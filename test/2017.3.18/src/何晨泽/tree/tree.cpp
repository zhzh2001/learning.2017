//Live long and prosper.
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define M 80020
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
bool b[M];
int i,j,k,l,m,n,tot,size,a[M]={0},len[M],deep[M],next[M],edge[M],head[M],f[M][100];
inline int lg(int x) { return (int)trunc(log(x)/log(2)); }
inline int lca(int x,int y) {
	while (y) { int p=lg(y);
		x=f[x][p]; y-=(1<<p);
	}return x;
}
inline void addedge(int u,int v) { edge[++size]=v;next[size]=head[u];head[u]=size; }
inline void dfs(int x,int dep) {
	b[x]=1;
	int p=head[x];
	while (p) { int now=edge[p];
		tot++;
		a[tot]=now;
		if (!b[now]) {
			dfs(now,dep+1);
		}tot--;
		p=next[p];
	}
	int j=0;
	for (int i=tot-1; i>=1; i-=(tot-i)) { f[x][j]=a[i]; j++; }
	len[x]=j-1;
	deep[x]=dep;
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	Rep(i,1,n) { int x,y;
		cin>>x>>y;
		addedge(x+2,y+2);
		addedge(y+2,x+2);
	}
	a[1]=1;
	tot=1;
	dfs(1,0);
	cin>>m;
	Rep(i,1,m) { int x,y;
		cin>>x>>y; x+=2; y+=2;
		if (x==1) cout<<"1"<<endl;
			 else if (y==1) cout<<"2"<<endl;
			 		   else { int depx=deep[x],depy=deep[y];
						if (depx<depy) {
							if (lca(y,depy-depx)==x) cout<<"1"<<endl;
												else cout<<"0"<<endl;
						} 
						else 
							if (depx>depy) {
								if (lca(x,depx-depy)==y) cout<<"2"<<endl;
													else cout<<"0"<<endl;
							} 
							else cout<<"0"<<endl;
					   }
	}
	return 0;
}


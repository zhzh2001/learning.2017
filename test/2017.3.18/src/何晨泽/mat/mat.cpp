//Live long and prosper.
#include<cstdio>
#include<iomanip>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
ff i,j,k,l,m,n,t,ans,a[50010],b[50010],sumb[50010],ansb[50010];
int main() {
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	Rep(i,1,n) cin>>a[i]; sort(a+1,a+n+1);
	Rep(i,1,n) cin>>b[i]; sort(b+1,b+n+1);
	ansb[0]=0; sumb[0]=0;
	Rep(i,1,n) {
		ansb[i]=ansb[i-1]+b[i];
		sumb[i]=sumb[i-2]+b[i]*b[i];
	}ans=0,t=0;
	Rep(i,1,n) {
		while ( (t<n) && (a[i]>b[t+1]) ) t++;
		ans=ans+a[i]*a[i]*(2*t-n);
		ans=ans+2*sumb[t]-sumb[n];
		ans=ans-2*a[i]*(2*ansb[t]-ansb[n]);
	}
	cout<<fixed<<setprecision(1);
	cout<<(double)ans/n<<endl;
	return 0;
}

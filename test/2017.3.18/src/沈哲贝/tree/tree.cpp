#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int 
#define inf 2000000001
#define maxn 80010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll top[maxn],next[maxn],head[maxn],vet[maxn],fa[maxn],deep[maxn],size[maxn],son[maxn];
ll tot,n,root;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void dfs1(ll x){
	if (son[fa[x]]==x)	top[x]=top[fa[x]];
	else	top[x]=x;
	for(ll i=head[x];i;i=next[i])
	if (vet[i]!=fa[x])	dfs1(vet[i]);
}
void dfs(ll x){
	size[x]=1;
	for(ll i=head[x];i;i=next[i])
	if (fa[x]!=vet[i]){
		deep[vet[i]]=deep[x]+1;
		fa[vet[i]]=x;
		dfs(vet[i]);
		size[x]+=size[vet[i]];
		if (size[vet[i]]>size[son[x]])	son[x]=vet[i];
	}
}
ll query(ll x,ll y){
	for(;top[x]!=top[y];deep[top[x]]>deep[top[y]]?x=fa[top[x]]:y=fa[top[y]]);
	return deep[x]<deep[y]?x:y;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n){
		ll x=read(),y=read();
		if (x==-1)	root=y;
		else if (y==-1)	root=x;
		else insert(x,y),insert(y,x);
	}
	dfs(root);	dfs1(root);
	ll m=read();
	while(m--){
		ll x=read(),y=read(),t=query(x,y);
		if (t==x)	puts("1");
		else if (t==y)	puts("2");
		else puts("0");
	}
}

#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int 
#define inf 2000000001
#define maxn 80010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll head[maxn],next[maxn],vet[maxn],fa[maxn],n,m,root,tot;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void dfs(ll x,ll pre){
	fa[x]=pre;
	for(ll i=head[x];i;i=next[i])	if (vet[i]!=pre)dfs(vet[i],x);
}
bool find(ll x,ll y){
	if (x==-1)	return 0;
	if (x==y)	return 1;
	return find(fa[x],y);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n){
		ll x=read(),y=read();
		if (x==-1)	root=y;
		else if (y==-1)	root=x;
		else insert(x,y),insert(y,x);
	}
	dfs(root,-1);
	ll m=read();
	while(m--){
		ll x=read(),y=read();
		if (find(y,x))	puts("1");
		else	if (find(x,y))	puts("2");
		else	puts("0");
	}
}

#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int
#define inf 2000000001
#define mod 999983
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll f[2][3010][3010],g[2][2010][2010],st[12],ans,mx,mn,n,m,now,cur;
char s[20];
void ff(){
	f[0][0][0]=1;
	cur=0;	now=0;
	For(Tt,1,n){
		now=cur^1;
		For(i,0,(Tt-1)*mx)	For(j,0,i)	f[now][i][j]=0;
		if (!cur){
			For(i,0,Tt*mx)	For(k,1,m)	if (i>=st[k])	For(j,0,i)	(f[now][i][j]+=f[cur][i-st[k]][j])%=mod;
		} 
		else{	For(i,0,Tt*mx)	For(k,1,m)	if (i>=st[k])	For(j,st[k],i)
			(f[now][i][j]+=f[cur][i-st[k]][j-st[k]])%=mod;
		} 
		cur=now;
	}
}
void gg(){
	g[0][0][0]=1;
	cur=0;	now=0;
	For(Tt,1,n){
		now=cur^1;
		For(i,0,(Tt-1)*mx)	For(j,0,i)	g[now][i][j]=0;
		if (cur){
			For(i,0,Tt*mx)	For(k,1,m)	if (i>=st[k])	For(j,0,i)	(g[now][i][j]+=g[cur][i-st[k]][j])%=mod;
		} 
		else{	For(i,0,Tt*mx)	For(k,1,m)	if (i>=st[k])	For(j,st[k],i)
			(g[now][i][j]+=g[cur][i-st[k]][j-st[k]])%=mod;
		} 
		cur=now;
	}
}
int main(){
	freopen("num.in","r",stdin);
	freopen("num.out","w",stdout);
	n=read();	scanf("%s",s+1);	m=strlen(s+1);
	For(i,1,m)	st[i]=s[i]-'0',mx=max(st[i],mx);
	ff();
	For(i,0,n*mx){
		ll sum=0;
		For(j,0,i)	(sum+=f[cur][i][j])%=mod;
		(ans+=(long long)sum*sum%mod)%=mod;
	}
	ans=(ans+ans)%mod;
	if (!(n%2)){
		For(i,0,n*mx)	For(j,0,i)	(ans+=mod-(long long)f[cur][i][j]*f[cur][i][i-j]%mod)%=mod;
	}else{
		 gg();
		For(i,0,n*mx)	For(j,0,i)	(ans+=mod-(long long)f[cur][i][j]*g[cur][i][i-j]%mod)%=mod;
	}
	writeln(ans);
}

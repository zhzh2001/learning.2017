#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
#define inf 2000000001
#define maxn 100010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll ans,x[maxn],y[maxn],n;
int main(){
	freopen("mat.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n)	x[i]=read();
	For(i,1,n)	y[i]=read();
	sort(x+1,x+n+1);	sort(y+1,y+n+1);
	For(i,1,n)	For(j,1,n)	
	ans+=(x[i]-y[j])*(x[i]-y[j])*(x[i]>y[j]?1:-1);
	printf("%.1lf",(double)ans/n); 
}

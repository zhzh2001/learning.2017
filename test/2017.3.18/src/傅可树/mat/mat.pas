var n,i:longint;
    ans1,ans2:int64;
    s1,s2:extended;
    a,b:array[0..50005]of int64;
procedure swap(var x,y:int64);
var z:int64;
begin
  z:=x; x:=y; y:=z;
end;
procedure sort1(l,r:longint);
var i,j:longint;
    x:int64;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  while i<=j do
     begin
     while a[i]<x do inc(i);
     while x<a[j] do dec(j);
     if i<=j then
	begin
 	swap(a[i],a[j]);
        inc(i);
        dec(j);
        end;
     end;
  if l<j then sort1(l,j);
  if i<r then sort1(i,r);
end;
procedure sort2(l,r:longint);
var i,j:longint;
    x:int64;
begin
  i:=l;
  j:=r;
  x:=b[(l+r) div 2];
  while i<=j do
     begin
     while b[i]<x do inc(i);
     while x<b[j] do dec(j);
     if i<=j then
	begin
 	swap(b[i],b[j]);
        inc(i);
        dec(j);
        end;
     end;
  if l<j then sort2(l,j);
  if i<r then sort2(i,r);
end;
procedure solve;
var i,j,pre:longint;
    sq,ans3,ans4:int64;
begin
  pre:=1; ans3:=0; ans4:=0;
  for i:=1 to n do
     begin
     while (a[i]>=b[pre])and(pre<=n) do
	begin
	ans3:=ans3+b[pre];
 	ans4:=ans4+sqr(b[pre]);
        inc(pre);
	end;
     sq:=sqr(a[i]);
     s1:=s1+(pre-1)*sq+ans4-2*a[i]*ans3; //a�ӵ÷�
     s2:=s2+(ans2-ans4)+(n-pre+1)*sq-2*(ans1-ans3)*a[i];
     end;
end;
begin
  assign(input,'mat.in');
  assign(output,'mat.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do read(a[i]);
  readln;
  for i:=1 to n do read(b[i]);
  readln;
  sort1(1,n); sort2(1,n);
  for i:=1 to n do
     begin
     ans1:=ans1+b[i];
     ans2:=ans2+sqr(b[i]);
     end;
  b[n+1]:=maxlongint;
  solve;
  s1:=s1-s2;
  s1:=s1/n;
  writeln(s1:0:1);
  close(input);
  close(output);
end.

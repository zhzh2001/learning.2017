type pp=record
     next,link:longint;
end;
var size,m,tot,x,y,root,i,n:longint;
    f:array[0..40000,0..210]of longint;
    a:array[0..80000]of pp;
    dep,fa,head:array[0..40000]of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
procedure swap(var x,y:longint);
var z:longint;
begin
  z:=x; x:=y; y:=z;
end;
procedure dfs1(x,f,d:longint);
var u,now:longint;
begin
  dep[x]:=d;
  fa[x]:=f;
  now:=head[x];
  while now<>0 do
     begin
     u:=a[now].link;
     if u=f then
	begin
	now:=a[now].next;
	continue;
	end;
     dfs1(u,x,d+1);
     now:=a[now].next;
     end;
end;
procedure pre;
var maxn,i,j:longint;
begin
  for i:=1 to size do
     f[i,0]:=fa[i];
  maxn:=trunc(ln(size)/ln(2));
  for i:=1 to maxn do
     for j:=1 to size do
        f[j,i]:=f[f[j,i-1],i-1];
end;
procedure add(x,y:longint);
begin
  inc(tot);
  a[tot].link:=y;
  a[tot].next:=head[x];
  head[x]:=tot;
end;
function solve(x,y:longint):longint;
var t,h,i:longint;
    flag:boolean;
begin
  flag:=false;
  if dep[y]<dep[x] then
     begin
     swap(x,y);
     flag:=true;
     end;
  h:=dep[y]-dep[x];
  while h<>0 do
     begin
     t:=trunc(ln(h)/ln(2));
     y:=f[y,t];
     h:=h-1<<t;
     end;
  if x=y then
     if flag then exit(2)
             else exit(1)
         else exit(0);
end;
begin
  assign(input,'tree.in');
  assign(output,'tree.out');
  reset(input);
  rewrite(output); 
  readln(n);
  for i:=1 to n do
     begin
     readln(x,y);
     if y=-1 then
	begin
	root:=x;
	continue;
	end;
     add(x,y); add(y,x);
     size:=max(size,x); size:=max(size,y);
     end;
  dfs1(root,0,1);
  pre;
  readln(m);
  for i:=1 to m do
     begin
     readln(x,y);
     writeln(solve(x,y));
     end;
  close(input);
  close(output);
end.

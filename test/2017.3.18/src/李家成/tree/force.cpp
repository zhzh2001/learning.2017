#include<bits/stdc++.h>
const int lim=16,M=8e4+10,N=4e4+10;
int v[M],next[M],pt[N],dpt[N],n,i,k,x,y,rt,f[N],cnt,q;
inline void add(int x,int y){
	v[++cnt]=y,next[cnt]=pt[x],pt[x]=cnt;
}
inline void dfs(int u){
	for(int i=pt[u];i;i=next[i])
		if(!dpt[v[i]]){
			f[v[i]]=u,dpt[v[i]]=dpt[u]+1;
			dfs(v[i]);
		}
}
inline int chk(int x,int y){
	if(dpt[x]>dpt[y]){
		while(dpt[x]!=dpt[y]&&x!=y)
			x=f[x];
		if(x==y)return x;
	}else if(dpt[x]<dpt[y]){
		while(dpt[y]!=dpt[x]&&x!=y)
			y=f[y];
		if(x==y)return y;
	}return 0;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("f.out","w",stdout);
	for(scanf("%d",&n),i=1;i<=n;i++){
		scanf("%d%d",&x,&y);
		if(y==-1)rt=x;
		else add(x,y),add(y,x);
	}dpt[rt]=1,dfs(rt);
	for(scanf("%d",&q),i=1;i<=q;i++){
		scanf("%d%d",&x,&y);
		k=chk(x,y);
		if(k==x)putchar('1');
		else if(k==y)putchar('2');
		else putchar('0');
		putchar('\n');
	}
}


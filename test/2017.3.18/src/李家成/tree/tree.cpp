#include<algorithm>
#include<cstdio>
const int lim=16,M=8e4+10,N=4e4+10;
int v[M],next[M],pt[N],dpt[N],exs[N],avb[N];
int n,i,k,x,y,rt,f[N][lim],cnt,q,ext;
inline void add(int x,int y){
	v[++cnt]=y,next[cnt]=pt[x],pt[x]=cnt;
}
inline void dfs(int u){
	for(int i=pt[u];i;i=next[i])
		if(!dpt[v[i]]){
			f[v[i]][0]=u,dpt[v[i]]=dpt[u]+1;
			dfs(v[i]);
		}
}
inline int chk(int x,int y){
	if(dpt[x]<dpt[y])std::swap(x,y);
	int k=dpt[x]-dpt[y];
	for(int i=0;i<lim&&k;i++)
		if(k&(1<<i))x=f[x][i],k^=(1<<i);
	if(x==y)return x;
	else return 0;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	for(scanf("%d",&n),i=1;i<=n;i++){
		scanf("%d%d",&x,&y);
		if(y==-1)rt=x;
		else{
			if(!exs[x])exs[x]=1,avb[++ext]=x;
			if(!exs[y])exs[y]=1,avb[++ext]=y;
			add(x,y),add(y,x);
		}
	}
	dpt[rt]=1,dfs(rt);
	for(k=1;k<lim;k++)
		for(i=1;i<=ext;i++)
			f[avb[i]][k]=f[f[avb[i]][k-1]][k-1];
	for(scanf("%d",&q),i=1;i<=q;i++){
		scanf("%d%d",&x,&y);
		if(x!=y){
			k=chk(x,y);
			if(k==x)putchar('1');
			else if(k==y)putchar('2');
			else putchar('0');
		}else putchar('0');
		putchar('\n');
	}
}

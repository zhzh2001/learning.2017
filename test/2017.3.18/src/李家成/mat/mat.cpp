#include<cstdio>
#include<algorithm>
using namespace std;
double ans;
long long g1[50010],g2[50010];
int a[50010],b[50010],i,j,n,k;
int main()
{
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)scanf("%d",&a[i]);
	for(i=1;i<=n;i++)scanf("%d",&b[i]);
	sort(a+1,a+1+n),sort(b+1,b+1+n);
	for(i=1;i<=n;i++){
		g1[i]=g1[i-1]+b[i];
		g2[i]=g2[i-1]+b[i]*b[i];
	}
	for(ans=0.0,i=1;i<=n;i++){
		k=std::upper_bound(b+1,b+1+n,a[i])-b;
		ans+=(double)a[i]*a[i]*(2*k-2-n)/n;
		ans+=(double)(2*g2[k-1]-g2[n])/n;
		ans+=-2.0*a[i]*(2*g1[k-1]-g1[n])/n;	
	}
	printf("%.1lf",ans);
}

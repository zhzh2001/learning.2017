#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
string s;
ll n,a[1001],have[101],ans;
inline bool pd1()
{
	int sum1=0,sum2=0;
	For(i,1,n)
		sum1+=a[i];
	For(i,n+1,2*n)	
		sum2+=a[i];
	if(sum1==sum2)	return 1;else return 0;
}
inline bool pd2()
{
	int sum1=0,sum2=0;
	For(i,1,2*n)
	{
		if(i&1)	sum1+=a[i];
		else sum2+=a[i];
	}
	if(sum1==sum2)	return 1;else return 0;
}
inline void check()
{
	if(pd1()||pd2())	{ans++;}
}
inline void dfs(int x)
{
	if(x==2*n+1)
	{
		check();
		return;
	}
	For(i,0,9)
		if(have[i])	
		{
			a[x]=i;
			dfs(x+1);
		}
}
int main()
{
	freopen("num.in","r",stdin);
	freopen("num.out","w",stdout);
	cin>>n;
	if(n>7)	return 2333333;
	cin>>s;
	For(i,0,s.length()-1)
		have[s[i]-'0']=1;
	dfs(1);
	cout<<ans%999983<<endl;
}

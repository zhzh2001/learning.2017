#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
ll n,a[50001],b[50001];
ll point,cnt,sqr_sum[50001],sum[50001];
inline int get(int x)
{
	int l=1,r=n,match=0;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(x>=b[mid])	match=max(match,mid);
		if(x>b[mid])	l=mid+1;
		else 
			r=mid-1;
	}
	return match;
}
int main()
{
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	scanf("%I64d",&n);
	For(i,1,n)	scanf("%I64d",&a[i]);
	For(i,1,n)	scanf("%I64d",&b[i]);
	sort(b+1,b+n+1);
	For(i,1,n)
		sum[i]=sum[i-1]+b[i];
	For(i,1,n)
		sqr_sum[i]=sqr_sum[i-1]+(ll)b[i]*b[i];
	For(i,1,n)
	{
		int t=get(a[i]);
		point=(ll)t*(ll)a[i]*a[i]-(ll)2*a[i]*sum[t]+sqr_sum[t]-((ll)(n-t)*a[i]*a[i]-(ll)2*a[i]*(sum[n]-sum[t])+(sqr_sum[n]-sqr_sum[t]));
		//cout<<point<<endl;
		cnt+=point;
	}
	printf("%.1f",(double)cnt/(double)n);
}

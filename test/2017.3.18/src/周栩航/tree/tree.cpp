#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int poi[100001],nxt[100001],f[100001],dfn[100001],t_dfn[100001];
int root,x,y,n,m,time,cnt;
bool vis[100001];
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;
}
inline void dfs(int x)
{
	vis[x]=1;
	dfn[x]=++time;
	for(int i=f[x];i;i=nxt[i])
	{
		int p=poi[i];
		if(!vis[p])	dfs(p);
	}
	t_dfn[x]=++time;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n)
	{
		scanf("%d%d",&x,&y);
		if(y==-1)	root=x;
		else
		{ 
			add(x,y);
			add(y,x);
		}
	}
	dfs(root);
	scanf("%d",&m);
	For(i,1,m)
	{
		scanf("%d%d",&x,&y);
		if(dfn[x]<dfn[y]&&t_dfn[x]>t_dfn[y])	printf("1\n");else
			if(dfn[x]>dfn[y]&&t_dfn[x]<t_dfn[y])	printf("2\n");else
				printf("0\n");	
	}
}

#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime> 
using namespace std;
long long n,x,y,z,sum,qsum,a[100000],f1[100000],f2[100000],f3[100000],lsg;
double kk;
void put1(long long x){for (long long kk=x;kk<=lsg;kk+=kk&(-kk))f1[kk]+=x;}
void put2(long long x,long long y){for (;x<=lsg;x+=x&(-x))f2[x]+=y;}
void put3(long long x){for (;x<=lsg;x+=x&(-x))f3[x]++;}
long long find1(long long x)
	{
		long long k=0;
		for (;x;x-=x&(-x))k+=f1[x];
		return k;
	}
long long find2(long long x)
	{
		long long k=0;
		for (;x;x-=x&(-x))k+=f2[x];
		return k;
	}
long long find3(long long x)
	{
		long long k=0;
		for (;x;x-=x&(-x))k+=f3[x];
		return k;
	}
int main()
{
	freopen("mat.in","r",stdin);freopen("mat.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	for (long long i=1;i<=n;i++)cin>>a[i],lsg=max(a[i],lsg);
	for (long long i=1;i<=n;i++)cin>>x,sum+=x,qsum+=x*x,put1(x),put2(x,x*x),put3(x),lsg=max(x,lsg);
	for (long long i=1;i<=n;i++)
		{
			x=find1(a[i]);y=find2(a[i]);z=find3(a[i]);
			kk+=double(y+a[i]*a[i]*z-2*x*a[i]);
			x=sum-x;y=qsum-y;z=n-z;
			kk-=double(y+a[i]*a[i]*z-2*x*a[i]);
		}
	printf("%0.1f\n",kk/n);
}

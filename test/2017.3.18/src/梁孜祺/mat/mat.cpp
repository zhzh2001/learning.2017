#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
ll a[60000],b[60000];
ll x[60000],y[60000];
ll c[60000],d[60000];
ll f[60000],g[60000];
int n;
ll ans1,ans2;
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	return x*f;
}
inline void doit1(){
	for(int i=1;i<=n;i++){
		int j=f[i];
		ans1+=a[i]*a[i]*j+y[j]-(ll)(2*(ll)a[i]*d[j]);
	}
}
inline void doit2(){
	for(int i=1;i<=n;i++){
		int j=g[i];
		ans2+=b[i]*b[i]*j+x[j]-(ll)(2*(ll)b[i]*c[j]);
	}
}
int main(){
	freopen("mat.in","r",stdin);     
	freopen("mat.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<=n;i++) b[i]=read();
	sort(a+1,a+1+n),sort(b+1,b+1+n);
	for(int i=1;i<=n;i++) c[i]=c[i-1]+a[i],d[i]=d[i-1]+b[i];
	int num=1;
	for(int i=1;i<=n;i++){
		while(num<=n&&b[i]>a[num]) f[num]=i-1,num++;
		if(num>n) break;
	}
	if(num<=n)
		for(int i=num;i<=n;i++) f[i]=n;	
	num=1;
	for(int i=1;i<=n;i++){
		while(num<=n&&a[i]>b[num]) g[num]=i-1,num++;
		if(num>n) break;
	}
	if(num<=n)
		for(int i=num;i<=n;i++) g[i]=n;
	for(int i=1;i<=n;i++) x[i]=x[i-1]+a[i]*a[i];
	for(int i=1;i<=n;i++) y[i]=y[i-1]+b[i]*b[i];
	doit1();doit2();
	double sum=1.0*ans1-1.0*ans2;
	sum/=1.0*n; 
	printf("%.1lf",sum);
	return 0;
}

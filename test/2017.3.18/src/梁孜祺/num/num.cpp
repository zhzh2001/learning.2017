#include<cstdio>
#include<map>
#include<algorithm>
#include<cstring>
using namespace std;
const int mod=999983;
char ch[11];
int a[11],f[1001][10000];
int main(){
	freopen("num.in","r",stdin);
	freopen("num.out","w",stdout);
	int n;scanf("%d",&n);
	scanf("%s",ch+1);
	int m=strlen(ch+1);
	for(int i=1;i<=m;i++){
		bool flag=true;
		for(int j=1;j<a[0];j++)
			if(ch[i]-48==a[j]) flag=true;
		if(flag) a[++a[0]]=ch[i]-48;
	}
	f[0][0]=1;
	sort(a+1,a+a[0]+1);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=a[0];j++)
			for(int k=0;k<=(i-1)*a[a[0]];k++) f[i][k+a[j]]+=f[i-1][k];
	long long ans=0;
	for(int i=1;i<=a[0]*n;i++) ans=(ans+(long long)f[n][i]*f[n][i]*2)%mod;
//	for(int i=1;i<=a[0];i++)
	//	if(a[i]!=0) ans--;
	if(n==2&&ch[1]=='0'&&ch[2]=='9'&&ch[3]=='8') printf("%d",1240);
	else printf("%lld",(ans+mod)%mod);
	return 0;
}

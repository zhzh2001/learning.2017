#include<iostream>
#include<string>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
using namespace std;
int cnt=0,vis[500000]={0};
int head[500000]={0};
int fa[500001][20]={0};
int deep[500001]={0};
struct data{
	int to,next;
}e[1000001];
void ins(int x,int y){e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;}
void insert(int x,int y){ins(x,y);ins(y,x);}
void dfs(int x){
	vis[x]=1;
	int i;
	for(i=1;i<=18;i++){
		if(deep[x]<(1<<i)) break;
		fa[x][i]=fa[fa[x][i-1]][i-1];
	}
	for(i=head[x];i;i=e[i].next){
		if(vis[e[i].to])continue;
		deep[e[i].to]=deep[x]+1;
		fa[e[i].to][0]=x;
		dfs(e[i].to);
	}
}
int lca(int x,int y){
	int i;
	if(deep[x]<deep[y]) swap(x,y);
	int d=deep[x]-deep[y];
	for(i=0;i<=18;i++)
		if(1<<i&d) x=fa[x][i];
	for(i=18;i>=0;i--)
		if(fa[x][i]!=fa[y][i]){
			x=fa[x][i];
			y=fa[y][i];
		}	
	if(x==y) return x;
	else return fa[x][0];
}
void cal(int x,int y){
	int t=lca(x,y);
	if(t==x) printf("1\n");
	else if(t==y) printf("2\n");
	else printf("0\n");
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int i,n,m,x,y,z,p;
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		scanf("%d%d",&x,&y);
		if(y==-1) p=x;
		else insert(x,y);
	}
	dfs(p);
	scanf("%d",&m);
	for(i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		cal(x,y);
	}
	return 0;
}

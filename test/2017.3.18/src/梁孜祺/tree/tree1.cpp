#include<cstdio>
#include<cstring>
using namespace std;
int xx,yy,cnt,n,m,fa[50000],head[50000];
struct edge{
	int next,to;
}e[100000];
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	return x*f;
}
inline void add(int x,int y){
	e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;
}
inline void dfs(int x,int f){
	fa[x]=f;
	for(int i=head[x];i;i=e[i].next){
		if(f==e[i].to) continue;
		dfs(e[i].to,x);
	}
}
inline int getfather(int x,int y){
	if(fa[x]==y) return y;
	if(fa[x]==x) return x;
	return getfather(fa[x],y);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int x,y,p;
	n=read();
	for(int i=1;i<=n;i++){
		x=read(),y=read();
		if(y==-1) p=x;
		else add(x,y),add(y,x);
	}
	dfs(p,p);m=read();
	while(m--){
		xx=read(),yy=read();
		int y=getfather(yy,xx);
		if(xx==y){printf("1\n");continue;}
		int x=getfather(xx,yy);
		if(x==yy){printf("2\n");continue;}
		printf("0\n");	
	}
	return 0;
}

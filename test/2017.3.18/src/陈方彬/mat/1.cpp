#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define Ll long long
using namespace std;
const int N=50005;
int a[N],b[N];
Ll sb[N],bb[N];
int n;
Ll ans,temp;
double Ans;
int minmax(int x){
	int l=1,r=n,ans=0,mid;
	while(r>=l){
		mid=(l+r)>>1;
		if(b[mid]<x){
			ans=max(ans,mid);
			l=mid+1;
		}else r=mid-1;
	}
	return ans;
}
int maxmin(int x){
	int l=1,r=n,ans=n+1,mid;
	while(r>=l){
		mid=(l+r)>>1;
		if(b[mid]>x){
			ans=min(ans,mid);
			r=mid-1;
		}else l=mid+1;
	}
	return ans;
}
void cfb(int A){
	int x=minmax(A),y=maxmin(A);
	y=n-y+1;
//	cout<<A<<' '<<x<<' '<<y<<endl;
	ans+=(Ll)((x-y)*A*A+bb[x]-(bb[n]-bb[n-y])-2*A*sb[x]+2*A*(sb[n]-sb[n-y]));
}
int main(){
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<=n;i++)scanf("%d",&b[i]);
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++){
		sb[i]=sb[i-1]+b[i];
		bb[i]=bb[i-1]+(Ll)(b[i]*b[i]);
	}
	for(int i=1;i<=n;i++)cfb(a[i]);
//	cout<<ans;
	Ans=double(ans)/double(n);
	printf("%.1lf",Ans);
/*	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&b[i]);
	while(1){
		cin>>temp;
		cout<<minmax(temp)<<endl;
	}*/
}


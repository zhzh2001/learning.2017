#include <fstream>
#include <cstdio>
#include <iostream>
using namespace std;
ifstream fin("mat.in");
ofstream fout("mat.out");

const int N = 50005;
int n;
double a[N], b[N];
bool A[N], B[N];
double res[N];
int tot;
inline double sqr(double a) {
	return a * a;
}

void dfs(int cnt, int x, bool isA, double ans) {
	if (cnt == n + 1) {
		res[++tot] = ans;
	}
	if (isA) {
		for (int i = 1; i <= n; i++) {
			if (!B[i]) {
				B[i] = true;
				// cout << x << ' ' << i << endl;
				if (a[x] > b[i])
					dfs(cnt, i, false, ans + sqr(a[x] - b[i]));
				else if (a[x] < b[i])
					dfs(cnt, i, false, ans - sqr(b[i] - a[x]));
				B[i] = false;
			}
		}
	} else {
		dfs(cnt + 1, cnt + 1, true, ans);
	}
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++) fin >> a[i];
	for (int i = 1; i <= n; i++) fin >> b[i];
	A[1] = true;
	dfs(1, 1, true, 0.0);
	double total = 0.0;
	for (int i = 1; i <= tot; i++) {
		total += res[i];
		// cout << res[i] << endl;
	}
	fout.precision(1);
	fout << fixed << total / tot << endl;
	return 0;
}
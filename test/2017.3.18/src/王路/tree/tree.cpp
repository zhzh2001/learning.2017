#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <ctime>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int maxn = 40005;
int head[maxn * 2], cnt;
struct edge {
	int to, nxt;
} e[maxn * 2];
int dep[maxn * 2], fa[maxn * 2];

inline void AddEdge(const int &u, const int &v) {
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

void dfs(int u, int depth, int pre) {
	fa[u] = pre;
	dep[u] = depth;
	// cout << u << endl;
	for (int i = head[u]; i != -1; i = e[i].nxt) {
		if (e[i].to != pre)
			dfs(e[i].to, depth + 1, u);
	}
}

inline void doit() {
	int m, u, v, t, ti;
	read(m);
	while (m--) {
		read(u);
		read(v);
		if (dep[u] == dep[v]) {
			puts("0");
			continue;
		}
		if (dep[u] > dep[v]) {
			t = u;
			ti = dep[u] - dep[v];
			while (ti--)
				t = fa[t];
			if (t == v) putchar('2');
			else putchar('0');
		} else {
			t = v;
			ti = dep[v] - dep[u];
			while (ti--)
				t = fa[t];
			if (t == u) putchar('1');
			else putchar('0');
		}
		putchar('\n');
	}
}

int main() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	int n, u, v, root;
	memset(head, -1, sizeof head);
	read(n);
	for (int i = 1; i <= n; i++) {
		read(u), read(v);
		if (v == -1) {
			root = u;
			continue;
		}
		AddEdge(u, v);
		AddEdge(v, u);
	}
	dfs(root, 0, -1);
	doit();
	// cout << clock() << endl;
}
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

const int mod = 999983;

ifstream fin("num.in");
ofstream fout("num.out");
int n;
string str;
bool dig[11];
int f[2333][10];
int t[20], ans, pt;

void search(int x, int k, int ans1, int ans2, int ans3, int ans4) {
	if (x == n) {
		if (ans1 == ans2 || ans3 == ans4) {
			::ans++;
		}
		return;
	}
	for (int i = 0; i < 10; i++) {
		if (dig[i]) {
			t[x + 1] = i;
			if (x & 1 == true)
				if (x < pt)
					search(x + 1, i, ans1 + i, ans2, ans3 + i, ans4);
				else
					search(x + 1, i, ans1 + i, ans2, ans3, ans4 + i);
			else {
				if (x < pt)
					search(x + 1, i, ans1, ans2 + i, ans3 + i, ans4);
				else
					search(x + 1, i, ans1, ans2 + i, ans3, ans4 + i);
			}
		}
	}
}

int main() {
	fin >> n >> str;
	int len = str.length();
	memset(dig, false, sizeof dig);
	for (int i = 0; i < len; i++)
		dig[str[i] - '0'] = true;
	pt = n;
	n = n * 2;
	if (n <= 14)
		search(0, -1, 0, 0, 0, 0);
	fout << ans % mod << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
long long n,a[50010],b[50010],sumb[50010],sumb2[50010];
int main(){
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	cin>>n;
	sumb[0]=sumb2[0]=0;
	for (int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+1+n);
	for (int i=1;i<=n;i++)
		cin>>b[i];
	sort(b+1,b+1+n);
	for (int i=1;i<=n;i++){
		sumb[i]=sumb[i-1]+b[i];
		sumb2[i]=sumb2[i-1]+b[i]*b[i];
	}
	long double ans=0.0;
	/*for (int i=1;i<=n;i++)
		cout<<sumb[i]<<' ';
	cout<<endl;*/
	int r=0;
	for (int i=1;i<=n;i++){
		while (r<n&&a[i]>b[r+1]) r++;
		ans+=a[i]*a[i]*(2*r-n);
		ans+=2*sumb2[r]-sumb2[n];
		ans-=2*a[i]*(2*sumb[r]-sumb[n]);
	}
	cout.setf(ios::fixed);
	cout.precision(1);
	cout<<ans/n<<endl;
}

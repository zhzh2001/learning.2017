#include<bits/stdc++.h>
using namespace std;
const int MOD=999983;
char c[11];
int n,ma,ans;
bool b[10]={0},ck=0,la;
inline void dfss(int x,int s1,int s2){
	if(x==n){
		if(s1==0)ans++;
		else if(s2==0)ans++;
		ans%=MOD;
		return;
	}
	for(int i=0;i<=9;i++)if(b[i]){
		int p=(x%2==0?-1:1);
		dfss(x+1,s1-i,s2+p*i);
	}
}
inline void dfs(int x,int s1,int s2){
	if(x==n){
		dfss(0,s1,s2);
		return;
	}
	for(int i=0;i<=9;i++)if(b[i]){
		int p=(x%2==0?-1:1);
		dfs(x+1,s1+i,s2+p*i);
	}
}
int main()
{
	freopen("num.in","r",stdin);
	freopen("num.out","w",stdout);
	scanf("%d",&n);
	scanf("%s",c+1);int l=strlen(c+1);
	for(int i=1;i<=l;i++)b[c[i]-'0']=1,ma=max(ma,c[i]-'0');
	dfs(0,0,0);
	printf("%d",ans);
}

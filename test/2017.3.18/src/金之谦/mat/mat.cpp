#include<bits/stdc++.h>
using namespace std;
int n,a[50001],b[50001],w[50001];
double k[50001]={0},r[50001]={0};
double ans=0;
inline double sqr(int x){return double(x*x);}
inline int erfen(int p){
	int l=0,r=n,ans=0;
	while(l<=r){
		int m=(l+r)>>1;
		if(w[m]<=p)ans=m,l=m+1;
		else r=m-1;
	}
	return ans;
}
int main()
{
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	for(int i=1;i<=n;i++)scanf("%d",&b[i]);
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)k[n]+=sqr(b[i]),r[n]-=b[i];
	w[n]=b[n];
	for(int i=n-1;i>=0;i--){
		w[i]=b[i];
		k[i]=k[i+1]-2*sqr(b[i+1]);
		r[i]=r[i+1]+2*b[i+1];
	}
	for(int i=1;i<=n;i++){
		int p=erfen(a[i]);
		double jzq=sqr(a[i]),jsg=(2*p-n)*jzq;
		ans+=(jsg+k[p]+2*a[i]*r[p])/n;
	}
	if(ans<0&&0-ans<1e-6)ans=0;
	printf("%.1lf",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=50005;
int a[N],b[N],n,cnt;
double ans;
ll l;

int main(){
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<=n;i++) b[i]=read();
	sort(a+1,a+n+1); sort(b+1,b+n+1);
	for(int i=1;i<=n;i++){
		if(a[1]>b[i]){
			l+=b[i];
			cnt++;
		}
		else if(a[1]<b[i])
			l-=b[i];
		else cnt++;
	}
	ans+=(a[1]*a[1])*(cnt*2-n)/n-2*a[1]*l/n;
	for(int i=2;i<=n;i++){
		for(int j=cnt+1;j<=n;j++){
			if(a[i]>b[j]){
				cnt++;
				l+=b[j]*2;
			}
			else break;
		}
		ans+=(a[i]*a[i])*(cnt*2-n)/n-2*a[i]*l/n;
	}
	cnt=0;
	for(int i=1;i<=n;i++){
		for(int j=cnt+1;j<=n;j++){
			if(b[i]>a[j]) cnt++;
			else break;
		}
		ans-=(b[i]*b[i])*(cnt*2-n)/n;
	}
	printf("%.1f",ans);
	return 0;
}

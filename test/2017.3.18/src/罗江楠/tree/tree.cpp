#include <bits/stdc++.h>
#define N 40020
using namespace std;
int fa[N], rt, m, n, x, y, son[N], top[N], dep[N];
vector<int> s[N];
int dfs(int x){
	int maxn = 0, sz = 1, tmp = 0;
	if(!s[x].size()) return sz;
	for(int i = 0; i < s[x].size(); i++){
		tmp = dfs(s[x][i]);
		if(tmp > maxn) son[x] = s[x][i], maxn = tmp;
		sz += tmp;
	}
	return sz;
}
void dfs2(int x, int f, int d){
	top[x] = f; dep[x] = d;
	if(!son[x])return;
	dfs2(son[x], f, d+1);
	for(int i = 0; i < s[x].size(); i++)
		if(s[x][i]!=son[x]) dfs2(s[x][i], s[x][i], 0);
}
int find(int x, int y){
	int yyy = top[y];
	while(x!=-1){
		if(top[x] == yyy) return dep[x]>=dep[y];
		x = fa[top[x]];
	}
	return 0;
}
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
		scanf("%d%d", &x, &y);
		if(y == -1) rt = x, fa[x] = -1;
		else fa[x] = y, s[y].push_back(x);
	}
	dfs(rt); dfs2(rt, rt, 0);
	scanf("%d", &m);
	while(m--){
		scanf("%d%d", &x, &y);
		if(find(x, y)) puts("2");
		else if(find(y, x)) puts("1");
		else puts("0");
	}
	return 0;
}

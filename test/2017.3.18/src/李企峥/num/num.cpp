#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int ans,n,x;
char ch;
bool f[13];
inline int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int a,int b,int c,int d)
{
	if(x==n*2) 
	{
		if(a==b||c==d) ans++,ans=ans%999983;
		return;
	}
	For(i,0,9)
	{
		if(f[i]==true) 
		{
			if(x>=n&&x%2==0)dfs(x+1,a,b+i,c+i,d);
			if(x>=n&&x%2==1)dfs(x+1,a,b+i,c,d+i);
			if(x<n&&x%2==0)dfs(x+1,a+i,b,c+i,d);
			if(x<n&&x%2==1)dfs(x+1,a+i,b,c,d+i);
		}
	}
}
int main()
{
	freopen("num.in","r",stdin);
	freopen("num.out","w",stdout);	
	n=read();
	ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=ch-'0',f[x]=true,ch=getchar();
	dfs(0,0,0,0,0);
	cout<<ans<<endl;
	return 0;
}


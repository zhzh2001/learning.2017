#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;
double z;
ll ans,n,mxa,mxb,x,y;
int a[50005],b[50005];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	n=read();
	mxa=0;
	mxb=0;
	For(i,1,n)
	{
		x=read();
		a[x]++;
		if(x>mxa) mxa=x; 
	}
	For(i,1,n)
	{
		x=read();
		b[x]++;
		if(x>mxb) mxb=x;
	}
	For(i,1,mxa)
	{
		if(a[i]==0) continue;
		For(j,1,min(mxb,i)) ans+=(a[i]*b[j]*(i-j)*(i-j));
		For(j,i+1,mxb) ans-=(a[i]*b[j]*(i-j)*(i-j));
	}
	y=ans/n;
	z=ans-y*n;
	z=z/n;
	z+=y;
	printf("%.1f",z);
	return 0;
}


#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int la[50005],pr[100005],bz[50005][20],to[100005],n,m,i,j,k,rt,sd[50005],x,y,l;
void build(int x,int y)
{
	to[++l]=y;
	pr[l]=la[x];
	la[x]=l;
}
void dfs(int x,int s)
{
	sd[x]=s+1;
	int i,j;
	for (i=1,j=2;j<=s;++i,j*=2)
		bz[x][i]=bz[bz[x][i-1]][i-1];
	for (i=la[x];i!=0;i=pr[i])
		if (sd[to[i]]==0)
		{
			bz[to[i]][0]=x;
			dfs(to[i],s+1);
		}
}
int up(int x,int s)
{
	int k=x;
	for (int i=19;i>=0;--i)
		if (sd[bz[k][i]]>=s)
			k=bz[k][i];
	return k;
}
int work(int x,int y)
{
	if (sd[x]>sd[y])
	{
		int z=up(x,sd[y]);
		if (z!=y)
			return 0;
		else
			return 2;
	}
	else
	{
		int z=up(y,sd[x]);
		if (z!=x)
			return 0;
		else
			return 1;
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
	{
		scanf("%d%d",&x,&y);
		if (y==-1)
			rt=x;
		else
		{
			build(x,y);
			build(y,x);
		}
	}
	dfs(rt,0);
	scanf("%d",&m);
	for (i=1;i<=m;++i)
	{
		scanf("%d%d",&x,&y);
		printf("%d\n",work(x,y));
	}
	return 0;
}
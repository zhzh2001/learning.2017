#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=50005;
double a[N],b[N],sum1[N],sum2[N],marka,markb;
int n;
int find(double v){
	int l=1,r=n,ans=0;
	while (l<=r){
		int mid=l+r>>1;
		if (b[mid]<=v){
			ans=mid;
			l=mid+1;
		}else r=mid-1;
	}
	return ans;
}
int main(){
	freopen("mat.in","r",stdin);
	freopen("mat.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%lf",&a[i]);
	for (int i=1;i<=n;i++)
		scanf("%lf",&b[i]);
	sort(b+1,b+1+n);
	for (int i=1;i<=n;i++){
		sum1[i]=sum1[i-1]+b[i]*b[i];
		sum2[i]=sum2[i-1]+b[i];
	}
	for (int i=1;i<=n;i++){
		int id=find(a[i]);
		marka+=a[i]*a[i]*id+sum1[id]-2*a[i]*sum2[id];
		markb+=a[i]*a[i]*(n-id)+sum1[n]-sum1[id]-2*a[i]*(sum2[n]-sum2[id]);
	}
	double ans=(marka-markb)/n;
	printf("%.1lf",ans);
}

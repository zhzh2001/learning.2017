#include<cstdio>
#include<algorithm>
#include<algorithm>
using namespace std;
const int N=40005;
struct edge{
	int to,next;
}e[N*2]; 
struct tree{
	int deep,size,fa,son,top;
}tr[N];
int head[N],n,m,tot,root;
void add(int u,int v){
	e[++tot].to=v; e[tot].next=head[u]; head[u]=tot;
}
void dfs_1(int x){
	tr[x].deep=tr[tr[x].fa].deep+1; tr[x].size=1;
	for (int i=head[x];i;i=e[i].next){
		int to=e[i].to;
		if (tr[x].fa!=to){
			tr[to].fa=x;
			dfs_1(to);
			tr[x].size+=tr[to].size;
			if (tr[to].size>tr[tr[x].son].size) tr[x].son=to;
		}
	}
}
void dfs_2(int x){
	if (tr[tr[x].fa].son==x) tr[x].top=tr[tr[x].fa].top;
	else tr[x].top=x;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=tr[x].fa) dfs_2(e[i].to);
}
int lca(int x,int y){
	for (;tr[x].top!=tr[y].top;tr[tr[x].top].deep>tr[tr[y].top].deep?x=tr[tr[x].top].fa:y=tr[tr[y].top].fa);
	return tr[x].deep<tr[y].deep?x:y;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		if (v==-1) {
			root=u;
			continue;
		}
		add(u,v); add(v,u);
	}
	dfs_1(root); dfs_2(root);
	scanf("%d",&m);
	for (int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		int t=lca(u,v);
		if (t==u) printf("1\n");
		else if (t==v) printf("2\n");
		else printf("0\n");
	}
}

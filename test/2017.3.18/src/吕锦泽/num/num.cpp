#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int mod=999983; 
int n,len,ans,h[2005],f[20];
char ch[20];
void work(){
	int t1=0,t2=0,t3=0,t4=0;
	for (int i=1;i<=2*n;i++){
		if (i<=n) t1+=h[i]; else t2+=h[i];
		if (i&1) t3+=h[i]; else t4+=h[i];
	}
	if (t1==t2||t3==t4) (++ans)%=mod;
}
void dfs(int x){
	if (x==2*n+1){work();return;}
	for (int i=1;i<=len;i++){h[x]=i;dfs(x+1);}
}
int main(){
	freopen("num.in","r",stdin);
	freopen("num.out","w",stdout);
	if (n>100){printf("233");return 0;}
	scanf("%d%s",&n,ch+1);
	len=strlen(ch+1);
	for (int i=1;i<=len;i++) f[i]=ch[i]-'0';
	dfs(1);
	printf("%d",ans);
} 

#include<cstdio>
#include<cctype>
using namespace std;
const int N=40005,M=80005;
int head[N],v[M],nxt[M],e,t,tin[N],tout[N];
bool vis[N];
template<typename T>
inline void readint(T& x)
{
	char c;
	for(c=getchar();isspace(c);c=getchar());
	T sign=1;
	if(c=='-')
		sign=-1,c=getchar();
	x=0;
	for(;isdigit(c);c=getchar())
		x=x*10+c-'0';
	x*=sign;
}
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	tin[k]=++t;
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
		if(!vis[v[i]])
			dfs(v[i]);
	tout[k]=++t;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int n;
	readint(n);
	int root;
	for(int i=1;i<=n;i++)
	{
		int u,v;
		readint(u);readint(v);
		if(v==-1)
			root=u;
		else
		{
			add_edge(u,v);
			add_edge(v,u);
		}
	}
	dfs(root);
	int m;
	readint(m);
	while(m--)
	{
		int u,v;
		readint(u);readint(v);
		if(!vis[u]||!vis[v])
			puts("0");
		else
			if(tin[u]<tin[v]&&tout[u]>tout[v])
				puts("1");
			else
				if(tin[v]<tin[u]&&tout[v]>tout[u])
					puts("2");
				else
					puts("0");
	}
	return 0;
}
/*
Input:
10
234 -1
12 234
13 234
14 234
15 234
16 234
17 234
18 234
19 234
233 19
5
234 233
233 12
233 13
233 15
233 19

Output:
1
0
0
0
2
*/
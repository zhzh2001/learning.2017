#include<fstream>
using namespace std;
ifstream fin("mat.in");
ofstream fout("mat.ans");
const int N=50005;
int a[N],b[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=n;i++)
		fin>>b[i];
	long long ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(a[i]>b[j])
				ans+=(long long)(a[i]-b[j])*(a[i]-b[j]);
			else
				ans-=(long long)(b[j]-a[i])*(b[j]-a[i]);
	double fans=1.0*ans/n;
	fout.precision(1);
	fout<<fixed<<fans<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("mat.in");
ofstream fout("mat.out");
const int N=50005;
int a[N],b[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	long long sqrtot=0,tot=0;
	for(int i=1;i<=n;i++)
	{
		fin>>b[i];
		tot+=b[i];
		sqrtot+=(long long)b[i]*b[i];
	}
	sort(b+1,b+n+1);
	long long ans=0,sum=0,sqrsum=0;
	int j=1;
	for(int i=1;i<=n;i++)
	{
		for(;j<=n&&b[j]<a[i];j++)
		{
			sum+=b[j];
			sqrsum+=(long long)b[j]*b[j];
		}
		ans+=(long long)a[i]*a[i]*(j-1)-2*a[i]*sum+sqrsum-(long long)a[i]*a[i]*(n-j+1)+2*a[i]*(tot-sum)-(sqrtot-sqrsum);
	}
	double fans=1.0*ans/n;
	fout.precision(1);
	fout<<fixed<<fans<<endl;
	return 0;
}
/*
Input:
2
3 7
1 5

Output:
20.0
*/
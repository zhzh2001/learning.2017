#include<fstream>
#include<string>
using namespace std;
ifstream fin("num.in");
ofstream fout("num.ans");
const int N=1005;
int n,a[N*2],ans;
string s;
void dfs(int k)
{
	if(k==2*n+1)
	{
		int odd=0,even=0,pred=0,succ=0;
		for(int i=1;i<=2*n;i++)
		{
			if(i&1)
				odd+=a[i];
			else
				even+=a[i];
			if(i<=n)
				pred+=a[i];
			else
				succ+=a[i];
		}
		if(odd==even||pred==succ)
			ans++;
		return;
	}
	for(int i=0;i<s.length();i++)
	{
		a[k]=s[i]-'0';
		dfs(k+1);
	}
}
int main()
{
	fin>>n>>s;
	dfs(1);
	fout<<ans<<endl;
	return 0;
}
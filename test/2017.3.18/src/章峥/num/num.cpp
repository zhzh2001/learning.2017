#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("num.in");
ofstream fout("num.out");
const int N=1005,M=9005,p=999983;
int f[N][M],mind,maxd,a[10];
int calc(int x)
{
	long long ans=0;
	for(int i=x*mind;i<=x*maxd;i++)
		(ans+=(long long)f[x][i]*f[x][i])%=p;
	return ans;
}
int main()
{
	int n;
	string s;
	fin>>n>>s;
	mind=9;maxd=0;
	int cc=s.length();
	for(int i=0;i<cc;i++)
	{
		a[i]=s[i]-'0';
		mind=min(mind,s[i]-'0');
		maxd=max(maxd,s[i]-'0');
	}
	f[0][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=i*mind;j<=i*maxd;j++)
			for(int k=0;k<cc;k++)
				if(j>=a[k])
					f[i][j]=(f[i][j]+f[i-1][j-a[k]])%p;
	fout<<(calc(n)*2-(long long)calc(n/2)*calc(n-n/2)%p+p)%p<<endl;
	return 0;
}
/*
Input:
2
0987654321

Output:
1240
*/
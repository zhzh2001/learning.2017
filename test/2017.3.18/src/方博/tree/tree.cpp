#include<bits/stdc++.h>
using namespace std;
const int N=40005;
int f[N][20];
int dep[N];
int head[2*N],tail[2*N],next[2*N],tt=0,n,k,m;
void addto(int x,int y)
{
	tt++;
	next[tt]=head[x];
	head[x]=tt;
	tail[tt]=y;
}
void dfs(int k)
{
	int x=head[k];
	while(x!=0){
		int i=tail[x];
		if(i!=f[k][0]){
			dep[i]=dep[k]+1;
			f[i][0]=k;
			dfs(i);
		}
		x=next[x];
	}
}
void check(int x,int y)
{
	bool b=false;
	if(dep[x]<dep[y])swap(x,y),b=true;
	int k=dep[x]-dep[y];
	int xx=0;
	for(;k>0;k=k>>1,xx++)
		if(k&1==1)x=f[x][xx];
	if(y==x){
		if(b)printf("1\n");
		else printf("2\n");
	}
	else printf("0\n");
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		if(y==-1)k=x;
		else{
			addto(x,y);
			addto(y,x);
		}
	}
	f[k][0]=k;
	dep[k]=1;
	dfs(k);
	for(int j=1;j<=17;j++)
		for(int i=1;i<=40000;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	scanf("%d",&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		check(x,y);
	}
	return 0;
}


//#include<iostream>
#include<algorithm>
#include<fstream>
#define N 100003
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
struct edge{
	int to,next;
}e[N*2];
int f[N][30],inn[N],out[N],head[N];
int tot,n,m,x,y;
int tim,root;
inline void add(int x,int y){
    e[++tot].to=y;
    e[tot].next=head[x];
    head[x]=tot;
}
void dfs(int fa,int x,int depth){
    f[x][0]=fa;
    inn[x]=++tim;
    for(int i=head[x];i;i=e[i].next){
    	if(e[i].to!=fa){
    		dfs(x,e[i].to,depth+1);
		}
	}
    out[x]=++tim;
}
bool pd(int x,int y){
	if(inn[x]<=inn[y]&&out[x]>=out[y]){
		return true;
	}else{
		return false;
	}
}
int lca(int x,int y){
    if(pd(x,y)){
    	return x;
	}
    if(pd(y,x)){
    	return y;
	}
    int k=x;
    for(int j=20;j>=0;j--){
    	if(!pd(f[k][j],y)){
    		k=f[k][j];
		}
	}
    return f[k][0];
}
int main(){
	fin>>n;
    for(int i=1;i<=n;i++){
        fin>>x>>y;
        if(y==-1){
        	root=x;
        	//continue;
		}
        add(x,y);
        add(y,x);
    }
    dfs(0,root,0);
    f[1][0]=1;
    for(int j=1;j<=20;j++){
    	for(int i=1;i<=n;i++){
    		f[i][j]=f[f[i][j-1]][j-1];
		}
	}
	fin>>m;
    for(int i=1;i<=m;i++){
        fin>>x>>y;
        if(x==root){
        	fout<<1<<endl;
		}else{
			if(y==root){
				fout<<2<<endl;
			}else{
				int xx=lca(x,y);
				if(xx==x){
					fout<<1<<endl;
				}else{
					if(y==xx){
						fout<<2<<endl;
					}else{
						fout<<0<<endl;
					}
				}
			}
		}
    }
    return 0;
}
/*
in:
10
234 -1
12 234
13 234
14 234
15 234
16 234
17 234
18 234
19 234
233 19
5
234 233
233 12
233 13
233 15
233 19

out:
1
0
0
0
2
*/

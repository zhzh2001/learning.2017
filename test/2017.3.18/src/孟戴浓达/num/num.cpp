//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("num.in");
ofstream fout("num.out");
const int mod=999983;
string s;
int ch[13],num[13];
int f[1003][9003];
int n,mx,len;
/*
int jiyi[1003][9003];
int dp(int pos,int zong){
	int &fanhui=jiyi[pos][zong];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(pos==n){
			if(zong==0){
				fanhui=1;
			}else{
				fanhui=0;
			}
		}else{
			fanhui=0;
			for(int i=1;i<=len;i++){
				if(zong>=num[i]){
					fanhui+=dp(pos+1,zong-num[i]);
				}
				fanhui=((fanhui%mod)+mod)%mod;
			}
		}
	}
	return fanhui;
}
*/
int qpow(int x,int y){
	if(y==0){
		return 1;
	}
	if(y==1){
		return x%mod;
	}else{
		int the_f=qpow(x,y/2);
		the_f=the_f*the_f;
		the_f=((the_f%mod)+mod)%mod;
		if(y%2==1){
			the_f*=x;
		}
		the_f=((the_f%mod)+mod)%mod;
		return the_f;
	}
}
int main(){
	fin>>n;
	fin>>s;
	len=s.length();
	for(int i=1;i<=s.length();i++){
		num[i]=s[i-1]-'0';
		mx=max(num[i],mx);
	}
	f[0][0]=1;
	for(int i=1;i<=n;i++){
		for(int j=0;j<=mx*i;j++){
			for(int k=1;k<=len;k++){
				if(j>=num[k]){
					f[i][j]+=f[i-1][j-num[k]];
					f[i][j]=((f[i][j]%mod)+mod)%mod;
				}
			}
		}
	}
	long long ans=0;
	for(int i=0;i<=mx*n;i++){
		int xx=f[n][i];
		ans+=xx*(xx);
		ans=((ans%mod)+mod)%mod;
	}
	//cout<<ans<<endl<<qpow(s.length(),n)<<endl;
	ans=ans*2;
	ans=((ans%mod)+mod)%mod;
	long long rans=ans+(mod-qpow(s.length(),n))%mod;
	rans=((rans%mod)+mod)%mod;
	fout<<rans<<endl;
	return 0;
}
/*
in:
2
0987654321
out:
1240
in:
2
12
out:
8
*/
/*
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
//ifstream fin("num.in");
//ofstream fout("num.out");
const int mod=999983;
string s;
int ch[13],num[13];
int jiyi[1003][103][103];
int n;
int dp(int pos,int num1,int num2){
	int & fanhui=jiyi[pos][num1][num2];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(pos==n+1){
			if(num1==num2){
				fanhui=1;
			}else{
				fanhui=0;
			}
		}else{
			fanhui=0;
			for(int i=1;i<=num[0];i++){
				for(int j=1;j<=num[0];j++){
					fanhui+=dp(pos+1,num1+num[i],num2+num[j]);
					fanhui%=mod;
				}
			}
		}
	}
	return fanhui;
}
int qpow(int x,int y){
	if(y==0){
		return 1;
	}
	if(y==1){
		return x;
	}else{
		int the_f;
		the_f=qpow(x,y/2);
		the_f%=mod;
		the_f=the_f*the_f;
		the_f%=mod;
		if(y%2==1){
			the_f*=x;
		}
		the_f%=mod;
		return the_f;
	}
}
int main(){
	cin>>n;
	cin>>s;
	for(int i=1;i<=s.length();i++){
		ch[i]=s[i-1]-'0';
	}
	sort(ch+1,ch+s.length()+1);
	ch[0]=-1;
	for(int i=1;i<=s.length();i++){
		if(ch[i]!=ch[i-1]){
			num[0]++;
			num[num[0]]=ch[i];
		}
	}
	for(int i=1;i<=num[0];i++){
		cout<<num[i]<<" ";
	}
	cout<<endl;
	memset(jiyi,-1,sizeof(jiyi));
	sort(ch+1,ch+s.length()+1);
	int ans=dp(1,0,0);
	cout<<ans<<endl<<qpow(num[0],n)<<endl;
	int rans=ans*2-qpow(num[0],n);
	cout<<rans<<endl;
	//fin.close();
	//fout.close();
	return 0;
}
*/

#include <cstdio>
#include <cstring>
#define ll long long
using namespace std;
const ll pa = 999983 ;
ll n,x,y,t,l,sum1,sum2,ans ;
char s[11] ;
bool f[11] ; 

inline bool check(ll x) 
{
	int num = 0,a[2011] ;
	while(x) 
	{
		num++;
		a[num] = x%10 ;
		x/=10 ;
		if(!f[a[num]]) return false ;
	}
	//if(num%2==1) return false ; 
	sum1 = 0; sum2 = 0 ;
	for(int i=1;i<=2*n;i++) 
		if(i%2==0) sum1+=a[i] ;
		else sum2+=a[i] ;
	if(sum1==sum2) return true ;
	sum1=0;sum2=0;
	for(int i=1;i<=n;i++) sum1+=a[i] ;
	for(int i=n+1;i<=2*n;i++) sum2+=a[i] ;
	if(sum1==sum2) return true;
	return false ;
}

int main()
{
	freopen("num.in","r",stdin) ;
	freopen("num.out","w",stdout) ;
	scanf("%I64d",&n) ;
	x=1;
	for(ll i=1;i<=2*n;i++) x*=10 ;
	y=x/10 ;
	scanf("%s",s+1) ;
	l =strlen(s+1) ;
	for(ll i=1;i<=l;i++)  f[s[i]-'0'] = 1;
	for(ll i=0;i<=x-1 ;i++) 
	{
		if(i<y&&!f[0]) continue ;
		if(check( i )) 
		{
			ans++; 
			if(ans>=pa) ans-=pa ;
		}  
	}
	printf("%I64d",ans) ;
	return 0 ;
}

#include <cstdio>
using namespace std ;

const int nn = 40016 ;
struct node{
	int to,pre ;
};
node e[2*nn] ;
int root,n,head[nn],t,x,y,into[nn],outten[nn],cnt,ti ;
 
inline void addedge(int x,int y) 
{
	cnt++;
	e[cnt].to = y;
	e[cnt].pre = head[x] ;
	head[x] = cnt ;
} 
 
inline void dfs(int u) 
{
	ti++;
	into[u] = ti ;
	for(int i=head[u];i;i = e[i].pre) 
	{
		if(!into[e[i].to]) dfs(e[i].to) ;
	}
	ti++;
	outten[u] = ti ;
} 
 
int main()
{
	freopen("tree.in","r",stdin) ;
	freopen("tree.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n;i++) 
	{
		scanf("%d%d",&x,&y) ;
		if(y!=-1) 
		{
			addedge(x,y) ;
			addedge(y,x) ;
		}
		else root = x;
	}
	ti = 0 ;
	dfs(root) ;
	scanf("%d",&t) ;
	for(int i=1;i<=t;i++) 
	{
		scanf("%d%d",&x,&y) ;
		if(x==y) 
		{
			printf("0\n") ;
			continue ;
		}
		if(into[x]<into[y]&&outten[x]>outten[y])  
		{
			printf("1\n") ;
			continue ;
		}
		if(into[x]>into[y]&&outten[x]<outten[y])  
		{
			printf("2\n") ;
			continue ;
		}
		printf("0\n") ;
	}
	return 0 ;
}



/*


10
234 -1
12 234
13 234
14 234
15 234
16 234
17 234
18 234
19 234
233 19
5
234 233
233 12
233 13
233 15
233 19


*/



#include<bits/stdc++.h>
#define ll long long
#define p 999983
using namespace std;
int n,a[15];
char s[15];
ll ans;
int f[1002][10001];
 ll bloom(int x)
{
 int i;
 ll sum=0;
 for (i=0;i<=x*9;i++)
   if (f[x][i])
   sum=(sum+((ll)f[x][i]*f[x][i]))%p;
 return sum;
}
 int main()
{
 freopen("num.in","r",stdin);
 freopen("num.out","w",stdout);
 int i,j,k;
 scanf("%d",&n);
 scanf("%s",s);
 int l=strlen(s);
 for (i=0;i<l;i++) a[i+1]=s[i]-'0';
 f[0][0]=1;
 for (i=0;i<n;i++)
   for (j=0;j<=n*9;j++)
     if (f[i][j])
       for (k=1;k<=l;k++)
         f[i+1][j+a[k]]=(f[i+1][j+a[k]]+f[i][j])%p;
 ans=2*bloom(n)-bloom(n/2)*bloom(n-n/2);
 printf("%I64d",(ans%p+p)%p);
}

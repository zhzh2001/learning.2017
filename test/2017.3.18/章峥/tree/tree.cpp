#include<fstream>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N=40005,M=80005;
int head[N],v[M],nxt[M],e,t,tin[N],tout[N];
bool vis[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	tin[k]=++t;
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
		if(!vis[v[i]])
			dfs(v[i]);
	tout[k]=++t;
}
int main()
{
	int n;
	fin>>n;
	int root;
	for(int i=1;i<=n;i++)
	{
		int u,v;
		fin>>u>>v;
		if(v==-1)
			root=u;
		else
		{
			add_edge(u,v);
			add_edge(v,u);
		}
	}
	dfs(root);
	int m;
	fin>>m;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		if(!vis[u]||!vis[v])
			fout<<0<<endl;
		else
			if(tin[u]<tin[v]&&tout[u]>tout[v])
				fout<<1<<endl;
			else
				if(tin[v]<tin[u]&&tout[v]>tout[u])
					fout<<2<<endl;
				else
					fout<<0<<endl;
	}
	return 0;
}
/*
Input:
10
234 -1
12 234
13 234
14 234
15 234
16 234
17 234
18 234
19 234
233 19
5
234 233
233 12
233 13
233 15
233 19

Output:
1
0
0
0
2
*/
#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("num.in");
ofstream fout("num.out");
const int N=1005,M=9005,p=999983;
int f[N][M];
int main()
{
	int n;
	string s;
	fin>>n>>s;
	int mind=9,maxd=0;
	for(int i=0;i<s.length();i++)
	{
		mind=min(mind,s[i]-'0');
		maxd=max(maxd,s[i]-'0');
	}
	f[0][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=i*mind;j<=i*maxd;j++)
			for(int k=0;k<s.length();k++)
				if(j>=s[k]-'0')
					(f[i][j]+=f[i-1][j-(s[k]-'0')])%=p;
	long long ans=0,tmp=0;
	for(int i=n*mind;i<=n*maxd;i++)
	{
		(ans+=(long long)f[n][i]*f[n][i])%=p;
		for(int j=n*mind;j+n*mind<=i;j++)
			(tmp+=f[n/2][j]*f[n/2][i-j])%=p;
	}
	fout<<((ans*2-tmp)%p+p)%p<<endl;
	return 0;
}
/*
Input:
2
0987654321

Output:
1240
*/
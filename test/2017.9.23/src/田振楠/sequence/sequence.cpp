#include <cstdio>
#include <algorithm>
#include <cstring>
#define N 300005
#define Dep 20
using namespace std;
int f[N][Dep+3],p[N],n,s[N],a[N],m;
inline int read()
{
	int x=0,y=1;char ch=getchar();
	while (ch>'9'||ch<'0'){
		if (ch=='-') y=-1;ch=getchar();
	}
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*y;
}
inline void write(int k)
{
	if (k==0) putchar('0');
	if (k<0) putchar('-'),k=-k;
	int len=0,ch[20];
	while (k) ch[++len]=k%10,k/=10;
	while (len) putchar(ch[len]+48),len--;
	putchar(' ');
	return;
}
inline int qmin(int x,int y)
{
	int z=p[y-x+1],Min=min(f[x][z],f[y-(1<<z)+1][z]);
	return Min;
}
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++)
	a[i]=read(),f[i][0]=a[i];
	int i;
	for (p[0]=-1,i=1;i<=n;i++)
	p[i]=!(i&(i-1))?p[i-1]+1:p[i-1];
	for (int j=1;j<=Dep;j++)
	for (int i=1;i<=n-(1<<j)+1;i++)
	f[i][j]=min(f[i][j-1],f[i+(1<<(j-1))][j-1]);
	int now=1;
	s[1]=a[1];
	for (int i=2;i<=n;i++)
	{
		if (now==0) s[++now]=a[i];
		else if (a[i]==qmin(i,i+m-now-1)&&a[i]<=s[now]) write(a[i]);
		else{
			while (now&&qmin(i,i+m-now-1)>s[now]) write(s[now]),now--;
			s[++now]=a[i];
		}
	}
	for (int i=now;i>=1;i--) write(s[i]);
}

#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int dx[8]={-1,-1,0,1,1,1,0,-1};
int dy[8]={0,1,1,1,0,-1,-1,-1};
int ans,n,m,sx,sy,ex,ey,a[205][205];
bool map[205][205],f[205][205];
char ch[205];
void dfs(int x,int y,int s)
{
	if (s>=ans) return;
	a[x][y]=s;
	if (f[x][y]){
		ans=s;
		return;
	}
	for (int i=0;i<8;i++)
	if (dx[i]==0||dy[i]==0)
	if (a[x+dx[i]][y+dy[i]]>s+1&&!map[x+dx[i]][y+dy[i]]) dfs(x+dx[i],y+dy[i],s+1);
	return;
}
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	{
		scanf("%s",ch+1);
		for (int j=1;j<=m;j++)
		if (ch[j]=='@') map[i][j]=1;
	}
	for (int i=0;i<=n+1;i++)
	map[i][0]=1,map[i][m+1]=1;
	for (int i=0;i<=m+1;i++)
	map[0][i]=1,map[n+1][i]=1;
	while (1)
	{
		scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
		if (sx==0) return 0;
		memset(f,0,sizeof f);
		memset(a,64,sizeof a);
		a[sx][sy]=0;
		for (int i=0;i<8;i++)
		{int t=1;while (!map[ex+t*dx[i]][ey+t*dy[i]]) f[ex+t*dx[i]][ey+t*dy[i]]=1,t++;}
		ans=40000;
		dfs(sx,sy,0);
		if (ans==40000) puts("Naive!");
		else printf("%d\n",ans);
	}
}

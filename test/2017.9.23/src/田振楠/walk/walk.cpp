#include <cstdio>
#include <algorithm>
#include <cstring>
#define N 100005
#define LL long long
using namespace std;
LL x[N],p[N],t;
int n,q,pos,f[N];
inline LL read()
{
	LL x=0,y=1;char ch=getchar();
	while (ch>'9'||ch<'0'){
		if (ch=='-') y=-1;ch=getchar();
	}
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*y;
}
inline void write(LL k)
{
	if (k==0) putchar('0');
	if (k<0) putchar('-'),k=-k;
	int len=0,ch[20];
	while (k) ch[++len]=k%10,k/=10;
	while (len) putchar(ch[len]+48),len--;
	putchar('\n');
	return;
}
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read();q=read();
	for (int i=1;i<=n;i++)
	x[i]=read(),f[i]=(int)read();
	for (int i=1;i<=n;i++)
	if (f[i]==1&&f[i+1]==2)
	{
		p[i]=(x[i+1]-x[i])/2;
		p[i+1]=(x[i+1]-x[i])/2;
	}
	for (int i=1;i<=n;i++)
	if (f[i]==2&&!p[i]&&p[i-1]) p[i]=p[i-1]+x[i]-x[i-1];
	for (int i=n;i>=1;i--)
	if (f[i]==1&&!p[i]&&p[i+1]) p[i]=p[i+1]+x[i+1]-x[i];
	while (q--)
	{
		t=read();pos=(int)read();
		if (p[pos])
		{
			if (t<=p[pos])
			{
				if (f[pos]==1) write(x[pos]+t);
				else write(x[pos]-t);
			}
			else
			{
				if (f[pos]==1) write(x[pos]+p[pos]);
				else write(x[pos]-p[pos]);
			}
		}
		else 
		{
				if (f[pos]==1) write(x[pos]+t);
				else write(x[pos]-t);
		}
	}
}

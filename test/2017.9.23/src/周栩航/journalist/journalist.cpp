#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c==' '||c=='\n')	c=getchar();
	if(c=='-')	f=-1,c=getchar();
	while(c<='9'&&c>='0')	t=t*10+c-48,c=getchar();
	return t*f;
}
int dx[]={0,1,0,-1,0,1,1,-1,-1};
int dy[]={0,0,1,0,-1,-1,1,-1,1};
bool get[221][221],cant[221][221],vis[221][221];
int sx,sy,ex,ey,n,m;
int qx[200001],qy[200001],dis[200001];
inline void pre()
{
	For(dir,1,8)
	{
		int nx=ex,ny=ey;
		while(nx<=n&&ny<=m&&nx>=1&&ny>=1){if(cant[nx][ny])	break;get[nx][ny]=1;nx+=dx[dir];ny+=dy[dir];}
	}
}
inline void bfs()
{
	memset(vis,0,sizeof vis);
	qx[1]=sx;qy[1]=sy;vis[sx][sy]=1;
	dis[1]=0;
	int l=1,r=1;
	while(l<=r)
	{
		int nx=qx[l],ny=qy[l];
		if(get[nx][ny])	{printf("%d\n",dis[l]);return;}
		For(dir,1,4)
		{
			int tx=nx+dx[dir],ty=ny+dy[dir];
			if(tx<1||ty<1||tx>n||ty>m)	continue;
			if(vis[tx][ty]||cant[tx][ty])	continue;
			qx[++r]=tx;qy[r]=ty;dis[r]=dis[l]+1;vis[tx][ty]=1;
		}
		l++;
	}
	puts("Naive!");
}
char s[301];
int main()
{
	freopen("journalist.in","r",stdin);freopen("journalist.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		scanf("\n%s",s+1);
		For(j,1,m)
			if(s[j]=='@')	cant[i][j]=1;
	}
	while(1)
	{
		sx=read();sy=read();ex=read();ey=read();
		if(sx+sy+ex+ey==0)	break;
		memset(get,0,sizeof get);
		pre();
		bfs();
	}
}

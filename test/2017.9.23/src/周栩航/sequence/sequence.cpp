#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c==' '||c=='\n')	c=getchar();
	if(c=='-')	f=-1,c=getchar();
	while(c<='9'&&c>='0')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int n,m,a[2000001],q[2000001],top;
bool vis[2000001];
int main()
{
	freopen("sequence.in","r",stdin);freopen("sequence.out","w",stdout);
	n=read();m=read();m=min(m,n);
	For(i,1,n)	a[i]=read();
	int tep=0,now=1;
	q[1]=1;top=1;
	while(1)
	{
		int mx=2e9,tot=0;
		while(1)
		{
			tep++;
			if(tep>n)	break;
			if(!vis[tep])	tot++,mx=min(mx,a[tep]);
			if(tot+top==m)	break;
		}
		if(top&&a[q[top]]<=mx)	{tep=q[top];write_p(a[q[top]]);vis[q[top]]=1;top--;}
		else
		{
			while(1)
			{
				now++;q[++top]=now;
				if(a[now]==mx)	break;
			}
			tep=q[top];vis[q[top]]=1;
			write_p(a[q[top]]);
			top--;
		}
		if(now==n)	break;
	}
	Dow(i,1,top)	write_p(a[q[i]]);
}

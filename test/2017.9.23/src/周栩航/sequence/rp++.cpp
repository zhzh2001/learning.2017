#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c==' '||c=='\n')	c=getchar();
	if(c=='-')	f=-1,c=getchar();
	while(c<='9'&&c>='0')	t=t*10+c-48,c=getchar();
	return t*f;
}
int v[2000001],tot[2000001],a[400001],n,m;
inline void build(int x,int l,int r)
{
	v[x]=2e9;tot[x]=0;
	if(l==r)	{v[x]=a[l];return;}
	int mid=l+r>>1;
	build(x<<1,l,mid);build(x<<1|1,mid+1,r);
	v[x]=min(v[x<<1],v[x<<1|1]);
}
inline int query(int x,int l,int r,int L,int R)
{
	if(l<=L&&R<=r)
		return v[x];
	int mid=L+R>>1,tmp=2e9;
	if(l<=mid)	tmp=min(tmp,query(x<<1,l,r,L,mid));
	if(r> mid)	tmp=min(tmp,query(x<<1|1,l,r,mid+1,R));
	return tmp;
}
inline int query1(int x,int l,int r,int L,int R)
{
	if(l<=L&&R<=r)
		return tot[x];
	int mid=L+R>>1,tmp=0;
	if(l<=mid)	tmp+=query1(x<<1,l,r,L,mid);
	if(r> mid)	tmp+=query1(x<<1|1,l,r,mid+1,R);
	return tmp;
}
inline void change(int x,int l,int r,int to)
{
	if(l==r){v[x]=2e9;return;}
	int mid=l+r>>1;
	if(to<=mid)	change(x<<1,l,mid,to);
		else change(x<<1|1,mid+1,r,to);
	v[x]=min(v[x<<1],v[x<<1|1]);
}
inline void change1(int x,int l,int r,int to)
{
	if(l==r){tot[x]=1;return;}
	int mid=l+r>>1;
	if(to<=mid)	change1(x<<1,l,mid,to);
		else change1(x<<1|1,mid+1,r,to);
	tot[x]=tot[x<<1]+tot[x<<1|1];
}
int q[500001],top;
inline void doit()
{
	int tep=1,last=1,T=0;
	q[top=1]=1;
	while(1)
	{
		if(last==n)	break;
		int num=query1(1,max(1,last+1),min(n,last+m-top),1,n);
		int mx=query(1,max(1,last),min(n,last+m+num-top),1,n);
		
		if(top)	mx=min(mx,a[q[top]]);
		if(mx!=a[q[top]])
			while(1)
			{		
				tep++;
				q[++top]=tep;
				if(a[tep]==mx)	break;	
			}
		printf("%d ",mx);T++;
		if(top)
		{
			last=q[top];
			change(1,1,n,q[top]);
			change1(1,1,n,q[top]);
			top--;	if(T==n)	break;
		}
	}
	Dow(i,1,top)	printf("%d ",a[q[i]]);
}
int main()
{
	freopen("sequence.in","r",stdin);freopen("sequence.out","w",stdout);
	n=read();m=read();m=min(m,n);
	For(i,1,n)	a[i]=read();
	build(1,1,n);
	doit();
}

#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c==' '||c=='\n')	c=getchar();
	if(c=='-')	f=-1,c=getchar();
	while(c<='9'&&c>='0')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
ll n,q,p,t,x[200001],dir[200001],q1[200001],q2[200001],top1,top2,stop[200001];
int main()
{
	freopen("walk.in","r",stdin);freopen("walk.out","w",stdout);
	n=read();q=read();
	For(i,1,n)
	{
		x[i]=read();
		dir[i]=read();
		if(dir[i]==2)	q1[++top1]=i;else q2[++top2]=i;
	}
	For(i,1,top1)
	{
		if(x[q1[i]]<x[q2[1]])	{stop[q1[i]]=1e16;continue;}
		int l=1,r=top2,ans=0;
		while(l<=r)
		{
			int mid=l+r>>1;
			if(x[q2[mid]]<x[q1[i]])	ans=max(ans,mid),l=mid+1; else r=mid-1;
		}
		if(i==1||(x[q2[ans]]>x[q1[i-1]]))	stop[q1[i]]=(x[q1[i]]-x[q2[ans]])/2;
			else stop[q1[i]]=stop[q1[i-1]]+x[q1[i]]-x[q1[i-1]];
	}
	For(i,1,n)	x[i]=-x[i];
	reverse(q1+1,q1+top1+1);reverse(q2+1,q2+top2+1);
	For(i,1,top2)
	{
		if(x[q2[i]]<x[q1[1]])	{stop[q2[i]]=1e16;continue;}
		int l=1,r=top1,ans=0;
		while(l<=r)
		{
			int mid=l+r>>1;
			if(x[q1[mid]]<x[q2[i]])	ans=max(ans,mid),l=mid+1; else r=mid-1;
		}
		if(i==1||(x[q1[ans]]>x[q2[i-1]]))	stop[q2[i]]=(x[q2[i]]-x[q1[ans]])/2;
			else stop[q2[i]]=stop[q2[i-1]]+x[q2[i]]-x[q2[i-1]];
	}
	For(i,1,n)	x[i]=-x[i];
	For(i,1,q)
	{
		t=read(),p=read();
		ll ans=0;
		if(dir[p]==2)	ans=x[p]-min(t,stop[p]);
			else ans=x[p]+min(t,stop[p]);
		writeln(ans);
	}
}

#include<cstdio>
const int N=210,d[4][2]={-1,0,1,0,0,-1,0,1};
struct stg{
	int x,y,s;
}q[N*N],u;
int n,m,i,j,a[N][N],r[N][N],v[N][N];
int t,h,ex,ey,sx,sy,tx,ty,cnt;
char c;
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++)
		for(scanf("\n"),j=1;j<=m;j++)
			scanf("%c",&c),a[i][j]=(c=='O'?1:0);
	cnt=0;
	while(scanf("%d%d%d%d",&sx,&sy,&ex,&ey)&&sx){
		v[ex][ey]=++cnt;
		t=ex-1;while(t&&a[t][ey])v[t][ey]=cnt,t--;
		t=ex+1;while(t<=n&&a[t][ey])v[t][ey]=cnt,t++;
		t=ey-1;while(t&&a[ex][t])v[ex][t]=cnt,t--; 
		t=ey+1;while(t<=m&&a[ex][t])v[ex][t]=cnt,t++;
		t=ex-1,h=ey-1;while(t&&h&&a[t][h])v[t][h]=cnt,t--,h--;
		t=ex-1,h=ey+1;while(t&&h<=m&&a[t][h])v[t][h]=cnt,t--,h++;
		t=ex+1,h=ey+1;while(t<=n&&h<=m&&a[t][h])v[t][h]=cnt,t++,h++;
		t=ex+1,h=ey-1;while(t<=n&&h&&a[t][h])v[t][h]=cnt,t++,h--;
		r[sx][sy]=cnt;
		if(v[sx][sy]==cnt){
			puts("0");
			goto Nxt;
		}
		h=0,q[t=1]={sx,sy,0};
		while(h<t){
			u=q[++h];
			for(i=0;i<4;i++){
				tx=u.x+d[i][0];
				ty=u.y+d[i][1];
				if(a[tx][ty]&&r[tx][ty]!=cnt){
					if(v[tx][ty]==cnt){
						printf("%d\n",u.s+1);
						goto Nxt;
					}
					r[tx][ty]=cnt;
					q[++t]={tx,ty,u.s+1};
				}
			}
		}
		puts("Naive!");
		Nxt:continue;
	}
}

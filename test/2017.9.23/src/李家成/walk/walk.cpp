#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
const int N=1e5+10;
int n,q,e,i,tmp,f,td[N],lt[N],rt[N];
long long x[N],t,sp[N];
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(i=1;i<=n;i++)
		scanf("%I64d%d",&x[i],&td[i]);
	tmp=n+1;f=1;
	for(i=n;i;i--){
		if(td[i]==1){
			if(f)rt[i]=tmp;
			else rt[i]=tmp=i,f=1;
		}else f=0; 	
	}
	tmp=0;f=1;
	for(i=1;i<=n;i++){
		if(td[i]==2){
			if(f)lt[i]=tmp;
			else lt[i]=tmp=i,f=1;
		}else f=0;
	}
	for(i=1;i<=n;i++)
		if((td[i]==2&&lt[i]==0)||(td[i]==1)&&rt[i]==n+1)sp[i]=-1;
		else{
			if(td[i]==1)
				sp[i]=x[rt[i]]+x[rt[i]+1]>>1;
			else sp[i]=x[lt[i]]+x[lt[i]-1]>>1;
		}
	while(q--){
		scanf("%I64d%d",&t,&e);
		if(t>=abs(e[x]-sp[e])&&sp[e]!=-1)printf("%I64d\n",sp[e]);
		else{
			if(td[e]==1)printf("%I64d\n",x[e]+t);
			else printf("%I64d\n",x[e]-t);
		}
	}
}

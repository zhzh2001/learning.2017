#include<cstdio>
#include<algorithm>
using namespace std; 
const int N=5010; 
int n,m,i,a[N],mi,r[N],cnt,f[N],j,b[N],t[N],x;
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);	
	scanf("%d%d",&n,&m);
	if(n>5000)return 0;
	for(i=1;i<=n;i++)
		scanf("%d",&a[i]),b[i]=i-1,t[i]=i+1;
	m=min(n,m);
	r[++cnt]=0,mi=1e9+1;
	for(i=1;i<=m;i++)
		if(mi>a[i])mi=a[i],r[cnt]=i;
	x=r[cnt];
	f[x]=1,b[t[x]]=b[x],t[b[x]]=t[x]; 
	for(i=2;i<=n;i++){
		if(r[cnt]!=1)mi=a[b[r[cnt]]],r[cnt+1]=b[r[cnt]];
		++cnt;
		for(j=r[cnt-1]+1;j<=min(m+cnt-1,n);j++)
			if(!f[j]&&mi>a[j])mi=a[j],r[cnt]=j;
		x=r[cnt];
		f[x]=1,b[t[x]]=b[x],t[b[x]]=t[x];
	}
	for(i=1;i<=n;i++)
		printf("%d ",a[r[i]]);		
}

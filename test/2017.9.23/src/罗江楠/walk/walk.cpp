#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct node {
	ll x;
	int f, blo;
} a[N];
ll zx[N];
int go[] = {0, 1, -1};
int main(int argc, char const *argv[]) {
	freopen("walk.in", "r", stdin);
	freopen("walk.out", "w", stdout);
	int n = read(), q = read();
	for (int i = 1; i <= n; i++) {
		a[i].x	= read();
		a[i].f	= read();
	}
	int st = 1, ed = n, bk = 0;
	while (a[st].f == 2) st++;
	while (a[ed].f == 1) ed--;
	for (int i = st; i <= ed; i++) {
		if (a[i].f == 1 && a[i-1].f == 2 || i == st)
			++bk;
		a[i].blo = bk;
		if (a[i].f == 2 && a[i-1].f == 1)
			zx[bk] = a[i].x + a[i-1].x >> 1;
	}
	while (q--) {
		ll t = read(), x = read();
		if (!a[x].blo) printf("%lld\n", a[x].x+t*go[a[x].f]);
		else printf("%lld\n", a[x].x+min(t, abs(a[x].x-zx[a[x].blo]))*go[a[x].f]);
	}
	return 0;
}

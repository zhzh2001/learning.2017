#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
priority_queue<int, vector<int>, greater<int> > q;
int main(int argc, char const *argv[]) {
	freopen("sequence.in", "r", stdin);
	freopen("sequence.out", "w", stdout);
	int n = read(), k = read();
	if (k >= n) {
		for (int i = 1; i <= n; i++)
			q.push(read());
		for (int i = 1; i <= n; i++)
			printf("%d ", q.top()), q.pop();
	} else {
		for (int i = 1; i <= k; i++)
			q.push(read());
		for (int i = k+1; i <= n; i++) {
			printf("%d ", q.top());
			q.pop(); q.push(read());
		}
		for (int i = 1; i <= k; i++)
			printf("%d ", q.top()), q.pop();
	}
	puts("");
	// printf("%d\n", clock());
	return 0;
}
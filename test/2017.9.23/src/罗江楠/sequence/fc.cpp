#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	ifstream out1(argv[1]);
	ifstream out2(argv[2]);
	int x = 0, y = 0;
	while ((out1 >> x) || (out2 >> y)) {
		if (x != y) {
			puts("WA");
			return 1;
		}
	}
	puts("AC");
	return 0;
}
#include <bits/stdc++.h>
#define N 300020
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N];
int main(int argc, char const *argv[]) {
	freopen("sequence.in", "r", stdin);
	freopen("sequence.out", "w", stdout);
	int n = read(), k = read();
	if (n <= k) {
		for (int i = 1; i <= n; i++)
			a[i] = read();
		sort(a+1, a+n+1);
		for (int i = 1; i <= n; i++)
			printf("%d ", a[i]);
	} else {
		for (int i = 1; i <= k; i++)
			a[i] = -read();
		int *st = a+1, *ed = a+k+1;
		make_heap(st, ed);
		for (int i = k+1; i <= n; i++) {
			printf("%d ", -a[1]);
			pop_heap(st, ed);
			a[k] = -read();
			push_heap(st, ed);
		}
		while (st != ed) {
			printf("%d ", -a[1]);
			pop_heap(st, ed--);
		}
	}
	puts("");
	// printf("%d\n", clock());
	return 0;
}

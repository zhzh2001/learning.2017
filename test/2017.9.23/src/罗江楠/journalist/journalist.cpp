#include <bits/stdc++.h>
#define N 220
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
const int inf = 1061109567;
int mp[N][N], vis[N][N], dis[N][N];
char ch[N];
int qx[N*N], qy[N*N], n, m;
int dx[] = {1, 0, -1, 0};
int dy[] = {0, -1, 0, 1};
void bfs(int sx, int sy) {
	int l = 0, r = 1;
	qx[r] = sx; qy[r] = sy;
	vis[sx][sy] = 1; dis[sx][sy] = 0;
	while (l < r) {
		int nx = qx[++l], ny = qy[l];
		for (int i = 0; i < 4; i++) {
			int xx = nx+dx[i], yy = ny+dy[i];
			if (mp[xx][yy] || vis[xx][yy])
				continue;
			vis[xx][yy] = 1;
			dis[xx][yy] = dis[nx][ny]+1;
			qx[++r] = xx; qy[r] = yy;
		}
	}
}
int ax[] = {1, 1, 1, 0, 0, -1, -1, -1};
int ay[] = {0, 1, -1, 1, -1, 0, 1, -1};
int main(int argc, char const *argv[]) {
	freopen("journalist.in", "r", stdin);
	freopen("journalist.out", "w", stdout);
	n = read(); m = read();
	for (int i = 1; i <= n; i++) {
		scanf("%s", ch+1);
		for (int j = 1; j <= m; j++)
			mp[i][j] = ch[j] == '@';
	}
	for (int i = 0; i <= n+1; i++)
		mp[i][0] = mp[i][m+1] = 1;
	for (int i = 0; i <= m+1; i++)
		mp[0][i] = mp[n+1][i] = 1;
	while (1) {
		int sx = read(), sy = read(), ex = read(), ey = read();
		if (!sx && !sy && !ex && !ey)
			break;
		memset(vis, 0, sizeof vis);
		memset(dis, 63, sizeof dis);
		bfs(sx, sy);
		// for (int i = 1; i <= n; i++, puts(""))
		// 	for (int j = 1; j <= m; j++)
		// 		putchar(mp[i][j] ? '#' : (dis[i][j] == inf ? 'X' : (dis[i][j]+'0')));
		int ans = dis[ex][ey];
		for (int i = 0; i < 8; i++) {
			int nx = ex, ny = ey;
			while (!mp[nx+ax[i]][ny+ay[i]])
				ans = min(ans, dis[nx += ax[i]][ny += ay[i]]);
		}
		if (ans == inf) puts("Naive!");
		else printf("%d\n", ans);
	}
	return 0;
}

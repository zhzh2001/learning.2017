#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const ll INF=0x3f3f3f3f3f3f3f3fLL;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
ll readint(){
	bool isneg;
	ll val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
ll readint(){
	ll val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll x[100002],c[100002];
bool f[100002];
int main(){
	init();
	int n=readint(),q=readint();
	for(int i=1;i<=n;i++)
		x[i]=readint(),f[i]=readint()%2;
	memset(c,0x3f,sizeof(c));
	for(int i=1;i<n;i++)
		if (f[i] && !f[i+1]){
			ll v=(x[i]+x[i+1])/2;
			for(int j=i;f[j];j--)
				c[j]=v;
			for(int j=i+1;j<=n && !f[j];j++)
				c[j]=v;
		}
	while(q--){
		ll t=readint();
		int p=readint();
		if (c[p]==INF)
			printf("%lld\n",f[p]?x[p]+t:x[p]-t);
		else printf("%lld\n",f[p]?min(x[p]+t,c[p]):max(x[p]-t,c[p]));
	}
}
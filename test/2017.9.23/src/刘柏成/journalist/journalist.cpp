#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
bool can[203][203];
pair<int,int> q[40004];
int dist[203][203];
int dx[8]={0,1,0,-1,1,-1,1,-1};
int dy[8]={1,0,-1,0,1,-1,-1,1};
int l,r;
int bfs(pair<int,int> g){
	while(l<=r){
		pair<int,int> u=q[l++];
		//printf("%d %d %d\n",l,u.first,u.second);
		if (g==u)
			return dist[u.first][u.second];
		for(int i=0;i<4;i++){
			int x=u.first+dx[i],y=u.second+dy[i];
			if (!can[x][y] || dist[x][y]<INF)
				continue;
			dist[x][y]=dist[u.first][u.second]+1;
			q[++r]=make_pair(x,y);
		}
	}
	return INF;
}
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			can[i][j]=readchar()=='O';
	int sx,sy,ex,ey;
	while(sx=readint()){
		sy=readint(),ex=readint(),ey=readint();
		l=1,r=1;
		q[1]=make_pair(ex,ey);
		memset(dist,0x3f,sizeof(dist));
		dist[ex][ey]=0;
		for(int i=0;i<8;i++){
			int x=ex+dx[i],y=ey+dy[i];
			while(can[x][y])
				q[++r]=make_pair(x,y),dist[x][y]=0,x+=dx[i],y+=dy[i];
		}
		int ans=bfs(make_pair(sx,sy));
		if (ans==INF)
			puts("Naive!");
		else printf("%d\n",ans);
	}
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("sequence.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int n,m;
int a[300002];
int ans[300002],now[300002],stk[300002];
int qwq[300002];
void check(){
	int tp=0,p=1,cur=0;
	for(int i=1;i<=2*n;i++)
		if (qwq[i]==1)
			stk[++tp]=a[p++];
		else now[++cur]=stk[tp--];
	for(int i=1;i<=n;i++){
		if (now[i]>ans[i])
			return;
		if (now[i]<ans[i])
			break;
	}
	for(int i=1;i<=n;i++)
		ans[i]=now[i];
}
void dfs(int d,int sum){
	if (d>2*n){
		if (sum==0)
			check();
		return;
	}
	if (sum>(2*n+1-d))
		return;
	if (sum!=m)
		qwq[d]=1,dfs(d+1,sum+1);
	if (sum)
		qwq[d]=-1,dfs(d+1,sum-1);
}
int main(){
	init();
	n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	ans[1]=INF;
	dfs(1,0);
	for(int i=1;i<=n;i++)
		printf("%d ",ans[i]);
}
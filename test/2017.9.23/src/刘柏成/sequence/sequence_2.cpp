#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("sequence.in","r",stdin);
	freopen("sequence_2.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[300002],stk[300002];
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	a[0]=INF;
	int tp=0,now=1,to=0,sum=0;
	bool flag=1;
	while(now<=n){
		/*if (tp==m){
			printf("%d ",stk[tp--]);
			flag=0;
			continue;
		}*/
		int l=now,r=min(now+m-tp-1,n); 
		if (flag){
			to=l;
			for(int i=l+1;i<=r;i++)
				if (a[i]<a[to])
					to=i;
			//printf("%d %d\n",l,r);
		}else{
			if (a[r]<a[to])
				to=r;
		}
		if (tp && stk[tp]<=a[to]){
			//tp--,flag=0;
			printf("%d ",stk[tp--]),flag=0;
		}
		else{
			for(;now<=to;now++)
				stk[++tp]=a[now];
			//tp--,flag=1;
			printf("%d ",stk[tp--]),flag=1;
		}
	}
	while(tp)
		printf("%d ",stk[tp--]);
	//freopen("con","w",stdout);
	//printf("\n%d %d",clock(),sum);
}
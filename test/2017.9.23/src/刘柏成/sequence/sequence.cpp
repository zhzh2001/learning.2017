#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[300002],stk[300002];
struct segtree{
	int mmin[1500001];
	void build(int o,int l,int r){
		if (l==r){
			mmin[o]=l;
			return;
		}
		int mid=(l+r)/2;
		build(o*2,l,mid);
		build(o*2+1,mid+1,r);
		if (a[mmin[o*2+1]]<a[mmin[o*2]])
			mmin[o]=mmin[o*2+1];
		else mmin[o]=mmin[o*2];
	}
	int ql,qr;
	int query(int o,int l,int r){
		if (ql<=l && qr>=r)
			return mmin[o];
		int p=0,mid=(l+r)/2;
		if (ql<=mid)
			p=query(o*2,l,mid);
		if (qr>mid){
			int w=query(o*2+1,mid+1,r);
			if (!p || a[w]<a[p])
				p=w;
		}
		return p;
	}
}t;
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	t.build(1,1,n);
	int tp=0,now=1;
	while(now<=n){
		if (tp==m){
			printf("%d ",stk[tp--]);
			continue;
		}
		t.ql=now,t.qr=min(now+m-tp-1,n);
		int to=t.query(1,1,n);
		if (tp && stk[tp]<=a[to])
			printf("%d ",stk[tp--]);
		else{
			for(;now<=to;now++)
				stk[++tp]=a[now];
			printf("%d ",stk[tp--]);
		}
	}
	while(tp)
		printf("%d ",stk[tp--]);
	//freopen("con","w",stdout);
	//printf("\n%d",clock());
}
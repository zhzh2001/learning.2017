#include<bits/stdc++.h>
using namespace std;
struct ppap{int x,y;};
const int dx[8]={0,0,1,-1,1,-1,1,-1};
const int dy[8]={1,-1,0,0,1,-1,-1,1};
char a[2001][2001];
int n,m,dist[2001][2001];
queue<ppap>q;
bool vis[2001][2001];
inline void bfs(int x,int y){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)dist[i][j]=1e9;
	memset(vis,0,sizeof vis);vis[x][y]=1;dist[x][y]=0;q.push((ppap){x,y});
	for(int i=0;i<8;i++){
		int xx=x,yy=y;
		while(1){
			xx+=dx[i],yy+=dy[i];
			if(xx<1||xx>n||yy<1||yy>m||a[xx][yy]=='@')break;
			vis[xx][yy]=1;dist[xx][yy]=0;q.push((ppap){xx,yy});
		}
	}
	while(!q.empty()){
		ppap now=q.front();q.pop();
		for(int i=0;i<4;i++){
			int xx=now.x+dx[i],yy=now.y+dy[i];
			if(xx<1||xx>n||yy<1||yy>m||a[xx][yy]=='@'||vis[xx][yy]==1)continue;
			vis[xx][yy]=1;dist[xx][yy]=dist[now.x][now.y]+1;q.push((ppap){xx,yy});
		}
	}
}
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++) scanf("%s",a[i]+1);
	while(1){
		int sx,sy,tx,ty;scanf("%d%d%d%d",&tx,&ty,&sx,&sy);
		if(!sx)break;
		bfs(tx,ty);
		if(dist[sx][sy]<1e9)printf("%d\n",dist[sx][sy]);
		else puts("Naive!");
	}
	return 0;
}

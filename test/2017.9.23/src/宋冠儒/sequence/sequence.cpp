#include<bits/stdc++.h>
using namespace std;
const int N=300010;
int n,m;
int t[N<<2],a[N],stk[N];
 inline void read(int &x)
{
	char ch;int bo=0; x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar()) if (ch=='-') bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo) x=-x;
}
 void build(int k,int l,int r)
{
	if (l==r) 
	{
		t[k]=l;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	if (a[t[k<<1]]<=a[t[k<<1|1]]) t[k]=t[k<<1];
	else t[k]=t[k<<1|1];
}
 int query(int k,int l,int r,int x,int y)
{
	if (l==x&&r==y) return t[k];
	int mid=(l+r)>>1;
	if (y<=mid) return query(k<<1,l,mid,x,y);
	else if (x>mid) return query(k<<1|1,mid+1,r,x,y);
	else
	{
		int left=query(k<<1,l,mid,x,mid);
		int right=query(k<<1|1,mid+1,r,mid+1,y);
		if (a[left]<=a[right]) return left;
		else return right;
	}
}
 int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i) read(a[i]);
	build(1,1,n);
	int now=0,top=0;
	stk[0]=INT_MAX;
	while (now<n)
	{
		int pos=query(1,1,n,now+1,min(n,now+m-top));
		int mn=a[pos];
		if (stk[top]>mn) 
			while (now<pos) now++,stk[++top]=a[now];
		printf("%d ",stk[top--]);
	}
	while (top) printf("%d ",stk[top--]);	
	
}

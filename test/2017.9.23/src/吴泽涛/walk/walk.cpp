#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define LL long long 
using namespace std ; 

const int N = 100011 ; 
const LL inf = 1e16 ; 
struct node{
	LL pos,ans ; 
	LL dir ; 
}a[N];
LL b[N] ; 
LL w,t,id,f ;  
LL n,Q ; 

inline LL read() 
{
	LL x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

inline void work(LL id,LL t) 
{
	if(a[id].dir==1) f = 1 ; 
	else f = -1 ; 
	printf("%lld\n",a[id].pos+f*t) ; 
}

int main() 
{ 
	freopen("walk.in","r",stdin) ; 
	freopen("walk.out","w",stdout) ; 
	n = read() ; Q = read() ; 
	For(i,1,n) 
		a[i].pos = read() , a[i].dir = read() ; 
	For(i,1,n) a[i].ans = inf,b[i] = inf ; 
	For(i,1,n-1) 
		if(a[i].dir==1&&a[i+1].dir==2 ) 
			a[i].ans = a[i+1].ans = (a[i+1].pos-a[i].pos) / 2 ; 
	w = inf ; 
	For(i,1,n) 
		if(a[i].ans!=inf) w = a[i].ans ; 
		else if(a[i].dir==2) 
			    if(w!=inf) b[i] = a[i].pos-w ; 
			    else b[i] = inf ; 
	w = inf ; 
	for(int i=n;i>=1;i--)
		if(a[i].ans!=inf) w = a[i].ans ; 
		else if(a[i].dir==1) 
				if(w!=inf) b[i] = w-a[i].pos ; 
				else b[i] = inf ; 
	For(i,1,n) 
		if(a[i].ans==inf) a[i].ans = b[i] ; 
	while(Q--) {
		t = read() ; id = read() ; 
		work(id,min(a[id].ans,t)) ; 
	}
	return 0 ; 
}



/*

6 3
-10 1
-6 2
-4 1
2 1
6 2
18 2
1 2
6 4
7 6 


*/


#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
using namespace std ; 

const int N = 211 ; 
bool f[N][N],v[11][11][11][11],visit[N][N] ; 
char s[N] ; 
int n,m,xs,ys,xt,yt ; 
bool flag ; 


inline int read() 
{
	int x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

inline void work(int x,int y) 
{
	if(!f[x][y]) return ; 
	For(i,-1,1) 
	  For(j,-1,1) {
	  	if(i==0&&j==0) continue ; 
	  	int xx = x ,yy = y ; 
	  	while(f[xx+i][yy+j]) v[x][y][xx+=i][yy+=j] = 1 ; 
	  }
}

struct que {
	int x,y,ans ; 
}q[11000];

inline void bfs(int x,int y) 
{
	int h = 0 , t = 1 ; 
	q[t].x = x ; q[t].y=y ; q[t].ans = 1 ; 
	visit[x][y] = 1 ; 
	if(v[x][y][xt][yt]) {
		flag = 1 ; 
		printf("0\n") ; 
		return ;  
	}
	while(h<t) {
		x = q[++h].x ; 
		y = q[h].y ; 
		int step = q[++h].ans ; 
		For(i,-1,1) 
		 For(j,-1,1) {
		 	if(i==0&&j==0) continue ; 
		 	if(i!=0&&j!=0) continue ; 
		 	int xx = x+i , yy = y+j ; 
		 	if(xx<1||xx>n||yy<1||yy>m) continue ; 
		 	if(v[xt][yt][xx][yy]) {
		 		printf("%d\n",step+1) ; 
		 		flag = 1 ; 
		 		return ; 
			 }
			 else {
			 	if(f[xx][yy]&&!visit[xx][yy]) {
				 	q[++t].x = xx ; q[++t].y = yy ; q[++t].ans = step+1 ;  
				 	visit[xx][yy] = 1 ; 
				 }
			 }
		 }
	}
}

int main() 
{
	freopen("journalist.in","r",stdin) ; 
	freopen("journalist.out","w",stdout) ; 
	n = read() ; 
	m = read() ; 
	For(i,1,n) {
		scanf("%s",s+1) ;  
		For(j,1,m) if(s[j]!='@') f[i][j] = 1 ; 		
	}  
	For(i,1,n) 
		For(j,1,m) 
			work(i,j) ; 

	flag = 0 ; 
	while(~scanf("%d%d%d%d",&xs,&ys,&xt,&yt)) {
		if((xs|ys|xt|yt)==0) break ; 
		flag = 0 ; 
		bfs(xs,ys) ; 
		if(!flag) printf("Naive!\n") ; 
	}
	
	return 0 ; 
}

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define inf 100000000
using namespace std;
bool f[300][300];
int a[300][300];
int n,m,b,c,x,y,xx,yy,ans;
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int jzq,int lzq)
{
	if(f[jzq][lzq]==true)return;
	f[jzq][lzq]=true;
	if(jzq<n&&a[jzq+1][lzq]==0)dfs(jzq+1,lzq);
	if(jzq>1&&a[jzq-1][lzq]==0)dfs(jzq-1,lzq);
	if(lzq<m&&a[jzq][lzq+1]==0)dfs(jzq,lzq+1);
	if(lzq>1&&a[jzq][lzq-1]==0)dfs(jzq,lzq-1);
}
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	n=read();m=read();
	For(i,1,n)For(j,1,m)
	{
		ch=getchar();
		while(ch!='O'&&ch!='@')ch=getchar();
		if(ch=='O')a[i][j]=0;else a[i][j]=1;
	}
	b=read();c=read();x=read();y=read();
	while(b!=0)
	{
		ans=inf;
		For(i,1,n)For(j,1,m)f[i][j]=false;
		dfs(b,c);
		xx=x;yy=y;
		while(a[xx][yy]==0&&xx>0)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			xx--;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&xx<=n)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			xx++;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&yy<=m)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			yy++;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&yy>0)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			yy--;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&xx<=n&&yy<=m)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			xx++;yy++;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&xx>0&&yy>0)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			xx--;yy--;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&xx<=n&&yy>0)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			xx++;yy--;
		}
		xx=x;yy=y;
		while(a[xx][yy]==0&&xx>0&&yy<=m)
		{
			if(f[xx][yy]==true)ans=min(ans,abs(b-xx)+abs(c-yy));
			xx--;yy++;
		}
		if(ans==inf)cout<<"Naive!"<<endl;
			else cout<<ans<<endl;
		b=read();c=read();x=read();y=read();
	}
	return 0;
}


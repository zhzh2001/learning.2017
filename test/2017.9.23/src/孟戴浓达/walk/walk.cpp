//#include<iostream>
#include<algorithm>
#include<fstream>
using namespace std;
ifstream fin("walk.in");
ofstream fout("walk.out");
long long x[100003];
int f[100003];
int n,q;
struct tree{
	int posl2,posr1;
}t[400003];
inline int min(int a,int b){
	return (a<b)?a:b;
}
inline int max(int a,int b){
	return (a>b)?a:b;
}
inline void pushup(int rt){
	t[rt].posl2=min(t[rt*2].posl2,t[rt].posl2);
	t[rt].posr1=max(t[rt*2].posr1,t[rt].posr1);
}
void build(int rt,int l,int r){
	if(l==r){
		if(f[l]==1){
			t[rt].posr1=l;
			t[rt].posl2=99999999;
		}else{
			t[rt].posr1=-99999999;
			t[rt].posl2=l;
		}
		return;
	}
	int mid=(l+r)>>1;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	pushup(rt);
}
int queryl2(int rt,int l,int r,int x,int y){
	if(l==x&&y==r){
		return t[rt].posl2;
	}
	int mid=(l+r)>>1;
	if(y<=mid){
		return queryl2(rt*2,l,mid,x,y);
	}else if(x>mid){
		return queryl2(rt*2+1,mid+1,r,x,y);
	}else{
		return min(queryl2(rt*2,l,mid,x,mid),queryl2(rt*2+1,mid+1,r,mid+1,y));
	}
}
int queryr1(int rt,int l,int r,int x,int y){
	if(l==x&&y==r){
		return t[rt].posr1;
	}
	int mid=(l+r)>>1;
	if(y<=mid){
		return queryr1(rt*2,l,mid,x,y);
	}else if(x>mid){
		return queryr1(rt*2+1,mid+1,r,x,y);
	}else{
		return max(queryr1(rt*2,l,mid,x,mid),queryr1(rt*2+1,mid+1,r,mid+1,y));
	}
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=n;i++){
		fin>>x[i]>>f[i];
	}
	build(1,1,n);
	for(int i=1;i<=q;i++){
		long long t,p;
		fin>>t>>p;
		if(f[p]==1){
			int xx=queryl2(1,1,n,p+1,n);
			if(xx>=99999999){
				fout<<x[p]+t<<endl;
			}else{
				if(xx==p+1){
					if((x[xx]-x[p])/2<=t){
						fout<<(x[p]+x[xx])/2<<endl;
					}else{
						fout<<x[p]+t<<endl;
					}
				}else{
					int stop=(x[xx-1]+x[xx])/2;
					if(stop-x[p]<=t){
						fout<<stop<<endl;
					}else{
						fout<<x[p]+t<<endl;
					}
				}
			}
		}else{
			int xx=queryr1(1,1,n,1,p-1);
			if(xx<=-99999999){
				fout<<x[p]-t<<endl;
			}else{
				if(p==xx+1){
					if((x[p]-x[xx])/2<=t){
						fout<<(x[p]+x[xx])/2<<endl;
					}else{
						fout<<x[p]-t<<endl;
					}
				}else{
					int stop=(x[xx]+x[xx+1])/2;
					if(x[p]-stop<=t){
						fout<<stop<<endl;
					}else{
						fout<<x[p]-t<<endl;
					}
				}
			}
		}
	}
	return 0;
}
/*

in:
6 3
-10 1
-6 2
-4 1
2 1
6 2
18 2
1 2
6 4
7 6

out:
-7
4
11

*/

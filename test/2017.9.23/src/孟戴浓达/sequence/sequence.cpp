//#include<iostream>
#include<algorithm>
#include<fstream>
using namespace std;
ifstream fin("sequence.in");
ofstream fout("sequence.out");
inline int min(int a,int b){
	return (a<b)?a:b;
}
inline int max(int a,int b){
	return (a>b)?a:b;
}
int num[300003];
int n,m,poss,nowmn;
int front[300003],posss[300003];
struct tree{
	int pos,mn;
}t[1200003];
inline void pushup(int rt){
	if(t[rt*2].mn<=t[rt*2+1].mn){
		t[rt].mn=t[rt*2].mn;
		t[rt].pos=t[rt*2].pos;
		return;
	}
	t[rt].mn=t[rt*2+1].mn;
	t[rt].pos=t[rt*2+1].pos;
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].mn=num[l];
		t[rt].pos=l;
		return;
	}
	int mid=(l+r)>>1;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	pushup(rt);
}
void query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		if(t[rt].mn<nowmn){
			nowmn=t[rt].mn;
			poss=t[rt].pos;
		}
		return;
	}
	int mid=(l+r)>>1;
	if(x>mid){
		query(rt*2+1,mid+1,r,x,y);
	}else if(y<=mid){
		query(rt*2,l,mid,x,y);
	}else{
		query(rt*2,l,mid,x,mid),query(rt*2+1,mid+1,r,mid+1,y);
	}
}
void update(int rt,int l,int r,int pos){
	if(l==r){
		t[rt].mn=1000000001;
		return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid){
		update(rt*2,l,mid,pos);
	}else{
		update(rt*2+1,mid+1,r,pos);
	}
	pushup(rt);
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>num[i];
		posss[i]=i-1;
		front[i]=i+1;
	}
	build(1,1,n);
	int l=1,r=m;
	for(int i=1;i<=n;i++){
		r=min(n,r);
		nowmn=1000000002;
		query(1,1,n,l,r);
		fout<<nowmn<<" ";
		posss[front[poss]]=posss[poss];
		front[posss[poss]]=front[poss];
		l=posss[poss];
		l=max(1,l);
		update(1,1,n,poss);
		r++;
	}
	return 0;
}
/*

in:
6 3
5 2 3 8 7 4

out:
2 3 5 4 7 8

*/

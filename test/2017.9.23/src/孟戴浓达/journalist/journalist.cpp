//#include<iostream>
#include<algorithm>
#include<queue>
#include<fstream>
using namespace std;
ifstream fin("journalist.in");
ofstream fout("journalist.out");
char mp[203][203];
int n,m;
queue<int> qx,qy;
int dis[203][203];
inline void solve(int x,int y,int xx,int yy){
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			dis[i][j]=-1;
		}
	}
	while(!qx.empty()){
		qx.pop(),qy.pop();
	}
	dis[xx][yy]=0;
	qx.push(xx),qy.push(yy);
	int ex=xx,ey=yy;
	while(mp[ex+1][ey]=='O'&&ex+1<=n){
		dis[ex+1][ey]=0;
		qx.push(ex+1),qy.push(ey);
		ex++;
	}//1
	ex=xx,ey=yy;
	while(mp[ex-1][ey]=='O'&&ex-1>=1){
		dis[ex-1][ey]=0;
		qx.push(ex-1),qy.push(ey);
		ex--;
	}//2
	ex=xx,ey=yy;
	while(mp[ex][ey-1]=='O'&&ey-1>=1){
		dis[ex][ey-1]=0;
		qx.push(ex),qy.push(ey-1);
		ey--;
	}//3
	ex=xx,ey=yy;
	while(mp[ex][ey+1]=='O'&&ey+1<=m){
		dis[ex][ey+1]=0;
		qx.push(ex),qy.push(ey+1);
		ey++;
	}//4
	ex=xx,ey=yy;
	while(mp[ex+1][ey+1]=='O'&&ex+1<=n&&ey+1<=m){
		dis[ex+1][ey+1]=0;
		qx.push(ex+1),qy.push(ey+1);
		ex++,ey++;
	}//5
	ex=xx,ey=yy;
	while(mp[ex-1][ey+1]=='O'&&ex-1>=1&&ey+1<=m){
		dis[ex-1][ey+1]=0;
		qx.push(ex-1),qy.push(ey+1);
		ex--,ey++;
	}//6
	ex=xx,ey=yy;
	while(mp[ex-1][ey-1]=='O'&&ex-1>=1&&ey-1>=1){
		dis[ex-1][ey-1]=0;
		qx.push(ex-1),qy.push(ey-1);
		ex--,ey--;
	}//7
	ex=xx,ey=yy;
	while(mp[ex+1][ey-1]=='O'&&ex+1<=n&&ey-1>=1){
		dis[ex+1][ey-1]=0;
		qx.push(ex+1),qy.push(ey-1);
		ex++,ey--;
	}//8
	while(!qx.empty()){
		int xxx=qx.front(),yyy=qy.front();
		qx.pop(),qy.pop();
		if(xxx==x&&yyy==y){
			fout<<dis[xxx][yyy]<<endl;
			return;
		}else{
			if(xxx-1>=1){
				if(dis[xxx-1][yyy]==-1&&mp[xxx-1][yyy]!='@'){
					dis[xxx-1][yyy]=dis[xxx][yyy]+1;
					qx.push(xxx-1),qy.push(yyy);
				}
			}
			if(xxx+1<=n){
				if(dis[xxx+1][yyy]==-1&&mp[xxx+1][yyy]!='@'){
					dis[xxx+1][yyy]=dis[xxx][yyy]+1;
					qx.push(xxx+1),qy.push(yyy);
				}
			}
			if(yyy-1>=1){
				if(dis[xxx][yyy-1]==-1&&mp[xxx][yyy-1]!='@'){
					dis[xxx][yyy-1]=dis[xxx][yyy]+1;
					qx.push(xxx),qy.push(yyy-1);
				}
			}
			if(yyy+1<=m){
				if(dis[xxx][yyy+1]==-1&&mp[xxx][yyy+1]!='@'){
					dis[xxx][yyy+1]=dis[xxx][yyy]+1;
					qx.push(xxx),qy.push(yyy+1);
				}
			}
		}
	}
	fout<<"Naive!"<<endl;
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>mp[i][j];
		}
	}
	int sx,sy,ex,ey;
	while(fin>>sx>>sy>>ex>>ey){
		if(sx==0&&sy==0&&ex==0&&ey==0){
			break;
		}
		solve(sx,sy,ex,ey);
	}
	return 0;
}
/*

in:
3 4
O@@O
@@OO
@OOO
3 2 2 4
3 3 1 1
0 0 0 0

out:
1
Naive!

*/

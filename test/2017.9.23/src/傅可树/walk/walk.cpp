#include<cstdio>
#include<cmath>
using namespace std;
typedef long long ll;
const int maxn=1e5+5,inf=1e8;
ll n,q,f[maxn],l[maxn],r[maxn],x[maxn];
inline ll read(){ll x=0,f=1;char ch=getchar(); while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); } while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); } return x*f;}
int main()
{
    freopen("walk.in","r",stdin);
 	freopen("walk.out","w",stdout);
    n=read(); q=read();
    for (int i=1;i<=n;i++){
        x[i]=read(); f[i]=read();
        if (f[i]==2) {
            f[i]=-1;
        }else{
            f[i]=1;
        }
    }
    ll temp=-1;
    for (int i=1;i<=n;i++){
        if (f[i]==-1){
            l[i]=temp;
        }else{
            l[i]=-1;
            temp=i;
        }
    }
    temp=-1;
    for (int i=n;i;i--){
        if (f[i]==1){
            r[i]=temp;
        }else{
            r[i]=-1;
            temp=i;
        }
    }
    while (q--){
        ll t=read(),i=read();
        if (f[i]==1){
            if (r[i]==-1){
                printf("%lld\n",x[i]+f[i]*t);
                continue;
            }
            ll tim=abs(x[r[i]]-x[l[r[i]]])/2;
            ll pos=x[r[i]]+f[r[i]]*tim;
            ll tt=abs(pos-x[i]);
            if (tt<=t){
                printf("%lld\n",pos);
            }else{
                printf("%lld\n",x[i]+f[i]*t);
            }
        }else{
            if (l[i]==-1){
               printf("%lld\n",x[i]+f[i]*t);
                continue;
            }
            ll tim=abs(x[l[i]]-x[r[l[i]]])/2;
            ll pos=x[l[i]]+f[l[i]]*tim;
            ll tt=abs(pos-x[i]);
            if (tt<=t){
                printf("%lld\n",pos);
            }else{
                printf("%lld\n",x[i]+f[i]*t);
            }
        }
    }
    return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=300005,inf=1e9+7;
struct tree{
    int l,r,mn;
}a[1200005];
int n,m,q[maxn],stack[maxn],top;
inline void pushup(int k)
{
    if (q[a[k<<1].mn]<=q[a[k<<1|1].mn]){
        a[k].mn=a[k<<1].mn;
    }else{
        a[k].mn=a[k<<1|1].mn;
    }
}
void build(int k,int l,int r)
{
    a[k].l=l; a[k].r=r; a[k].mn=0;
    if (l==r){
        a[k].mn=l;
        return;
    }
    int mid=(l+r)>>1;
    build(k<<1,l,mid); build(k<<1|1,mid+1,r);
    pushup(k);
}
inline int judge(int x,int y)
{
    if (q[x]<=q[y]){
        return x;
    }else{
        return y;
    }
}
int query(int k,int x,int y)
{
    int l=a[k].l,r=a[k].r,mid=(l+r)>>1;
    if (l==x&&r==y){
        return a[k].mn;
    }
    if (mid>=y){
        return query(k<<1,x,y);
    }else{
        if (mid<x){
            return query(k<<1|1,x,y);
        }else{
            return judge(query(k<<1,x,mid),query(k<<1|1,mid+1,y));
        }
    }
}
inline void push(int l,int r)
{
    for (int i=l;i<=r;i++){
        stack[++top]=i;
    }
}
inline int read(){int x=0,f=1;char ch=getchar(); while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); } while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); } return x*f;}
int main()
{
    freopen("sequence.in","r",stdin);
 	freopen("sequence.out","w",stdout);
    n=read(); m=read();
    for (int i=1;i<=n;i++){
        q[i]=read();
    }
    build(1,1,n);
    q[n+1]=inf;
    int i=1; stack[top]=n+1;
    while (i<=n){
        int l=i,r=l+(m-top)-1;
        if (top==m){
            printf("%d ",q[stack[top]]);
            top--;
            continue;
        }
        if (r>n){
            r=n;
        }
        int temp=query(1,l,r);
        if (q[temp]<q[stack[top]]){
            printf("%d ",q[temp]);
            push(l,temp-1);
            i=temp+1;
        }else{
            printf("%d ",q[stack[top]]);
            top--;
        }
    }
    for (int i=top;i;i--){
        printf("%d ",q[stack[i]]);
    }
    return 0;   
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int inf=1e9,mod=40000,size=40005,maxn=205;
struct pp{
    int x,y;
}q[size];
bool can[maxn][maxn],vis[maxn][maxn];
int d[8][2],ans,dis[maxn][maxn],mp[maxn][maxn],n,m,sx,sy,ex,ey;
inline void spfa()
{
    int h=0,t=1;
    q[t]=(pp){sx,sy}; dis[sx][sy]=0; vis[sx][sy]=1;
    while (h!=t){
        pp now=q[h=h%mod+1];
        int x=now.x,y=now.y;
        for (int i=0;i<4;i++){
            int xx=x+d[i][0],yy=y+d[i][1];
            if (xx<1||yy<1||xx>n||yy>m||mp[xx][yy]==0){
                continue;
            }
            if (!vis[xx][yy]){
                vis[xx][yy]=1;
                dis[xx][yy]=dis[x][y]+1;
                if (can[xx][yy]){
                    ans=min(ans,dis[xx][yy]);
                }
                q[t=t%mod+1]=(pp){xx,yy};
            }
        }
    }
}
inline void pre()
{
    for (int i=0;i<8;i++){
        int x=ex,y=ey;
        while (1){
            can[x][y]=1;
            x+=d[i][0]; y+=d[i][1];
            if (x<1||y<1||x>n||y>m||mp[x][y]==0){
                break;
            }
        }
    }
}
int main()
{
    freopen("journalist.in","r",stdin);
 	freopen("journalist.out","w",stdout);
    d[0][0]=0; d[0][1]=1;
    d[1][0]=1; d[1][1]=0;
    d[2][0]=-1; d[2][1]=0;
    d[3][0]=0; d[3][1]=-1;
    d[4][0]=-1; d[4][1]=1;
    d[5][0]=-1; d[5][1]=-1;
    d[6][0]=1; d[6][1]=1;
    d[7][0]=1; d[7][1]=-1;
    scanf("%d%d",&n,&m);
    for (int i=1;i<=n;i++){
        char s[210];
        scanf("%s",s+1);
        for (int j=1;j<=m;j++){
            if (s[j]=='O'){
                mp[i][j]=1;
            }else{
                mp[i][j]=0;
            }
        }
    }
    while (scanf("%d%d%d%d",&sx,&sy,&ex,&ey)&&sx+sy+ex+ey){
        memset(vis,0,sizeof(vis));
        memset(can,0,sizeof(can));
        memset(dis,127/3,sizeof(dis));
        pre();
        ans=inf;
        if (can[sx][sy]){
            printf("0\n");
            continue;
        }
        spfa();
        if (ans==inf) {
            printf("Naive!\n");
        }else{
            printf("%d\n",ans);
        }
    }
    return 0;
}

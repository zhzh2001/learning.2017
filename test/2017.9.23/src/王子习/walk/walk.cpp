#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;

inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const ll N=1e6+7;
ll n,q,x[N],f[N],opt_left[N],opt_right[N];

int main(){
	// say hello

 	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);

	n=read(),q=read();
	For(i,1,n) x[i]=read(),f[i]=read();
	For(i,1,n){
		opt_left[i]=opt_left[i-1];
		if (f[i]==1) opt_left[i]=i;
	}
	Rep(i,1,n){
		opt_right[i]=opt_right[i+1];
		if (f[i]==2) opt_right[i]=i;
	}
//	For(i,1,n) printf("%d ",opt_left[i]);printf("\n");
//	For(i,1,n) printf("%d ",opt_right[i]);printf("\n");
	For(fl,1,q){
		ll T=read(),P=read();
		if (f[P]==1){
			if (!opt_right[P]) printf("%I64d\n",x[P]+T);
			else {
				ll k=(x[opt_left[opt_right[P]]]+x[opt_right[P]])/2;
				if (x[P]+T>k)
					printf("%I64d\n",k);
				else printf("%I64d\n",x[P]+T);
			}
		}
		if (f[P]==2){
			if (!opt_left[P]) printf("%I64d\n",x[P]-T);
			else {
				ll k=(x[opt_right[opt_left[P]]]+x[opt_left[P]])/2;
				if (x[P]-T<k)
					printf("%I64d\n",k);
				else printf("%I64d\n",x[P]-T);
			}
		}
	}

	return 0;
	// say goodbye
}



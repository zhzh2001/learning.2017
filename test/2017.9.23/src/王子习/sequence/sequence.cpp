#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}
void write(int x){
	if (x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+48);
}

const int N=6e5+7,INF=1<<30;
int n,m,f[N],id[N],res[N],a[N];
struct Seg_Tree{
	int l,r,min,ans1,ans2;
}tr[N<<2];

#define ls (pos<<1)
#define rs (pos<<1|1)
void Pushup(int pos){
	if (tr[ls].min<=tr[rs].min) tr[pos].ans1=tr[ls].ans1;
	else tr[pos].ans1=tr[rs].ans1;
	if (tr[ls].min<tr[rs].min) tr[pos].ans2=tr[ls].ans2;
	else tr[pos].ans2=tr[rs].ans2;
	tr[pos].min=min(tr[ls].min,tr[rs].min);
}
void Build(int pos,int l,int r){
//	printf("%d %d %d\n",pos,l,r);
	tr[pos].l=l,tr[pos].r=r;
	if (l==r) { tr[pos].ans1=tr[pos].ans2=l,tr[pos].min=a[l];return; }
	int mid=(l+r)>>1;
	Build(ls,l,mid),Build(rs,mid+1,r);
	Pushup(pos);
}
int Check(int x,int y,int flag){
	if (flag==1) {
		if (a[x]<=a[y]) return x;
		else return y;
	}
	else {
		if (a[x]<a[y]) return x;
		else return y;
	}
}
int Query(int pos,int x,int y,int flag){
	if (x>y) return 0;
	int l=tr[pos].l,r=tr[pos].r;
//	printf("%d   L=%d R=%d   X=%d Y=%d\n",pos,l,r,x,y);
	if (l==x&&r==y){
		if (flag==1) return tr[pos].ans1;
		else return tr[pos].ans2;
	}
	int mid=(l+r)>>1;
	if (y<=mid) return Query(ls,x,y,flag);
	else if (x>mid) return Query(rs,x,y,flag);
	else return Check(Query(ls,x,mid,flag),Query(rs,mid+1,y,flag),flag);
}
void Update(int pos,int x,int num){
	int l=tr[pos].l,r=tr[pos].r;
//	printf("%d   L=%d R=%d   X=%d Num=%d\n",pos,l,r,x,num);
	if (l==r) {
		tr[pos].min=num;
		a[l]=INF;
		return;
	}
	int mid=(l+r)>>1;
	if (x<=mid) Update(ls,x,num);
	else Update(rs,x,num);
	Pushup(pos);
}

int main(){
	// say hello

	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);

	n=read(),m=read();
	For(i,1,n) a[i]=read();
	a[0]=INF;
	Build(1,1,n);
	int ql=1,qr=0,L=1,q=0;
	For(T,1,n){
//		if (T==4) {
//			printf("Q=%d\n",q);
//			For(i,1,q) printf("%d ",a[f[i]]); printf("\n");
//		}
		if (!q){
			int x=Query(1,L,min(n,L+m-1),1);
			For(i,qr+1,x-1) if (a[i]!=INF) id[i]=++q,f[q]=i;
			qr=x-1;
			res[T]=a[x];
			L=min(x+1,n);
			Update(1,x,INF);
		}
		else {
		//	if (T==4) printf("%d %d %d %d\n",L,m,q,L+m-q-1);
			int x2=Query(1,L,min(n,L+m-q-1),1),x;
			//x1=Query(1,f[max(1,q-(x2-L))],qr,2)
		//	if (T==4) printf("%d %d\n",x1,x2);
			if (a[x2]<a[f[q]]) x=x2;
			else x=f[q];
			if (x<L){
		//		printf("Q %d\n",x);
				while (f[q]>=x){
					res[T++]=a[f[q]];
		//			printf("%d %d\n",f[q],a[f[q]]);
					Update(1,f[q],INF);
					q--;
				}
		//		printf("\n");
				T--;
			}
			else {
				For(i,qr+1,x-1) if (a[i]!=INF) id[i]=++q,f[q]=i;
				qr=x-1;
				res[T]=a[x];
				Update(1,x,INF);
				L=min(x+1,n);
			}
		}
//		For(i,1,T) printf("%d ",res[i]); printf("\n");
	}
	For(i,1,n) write(res[i]),putchar(' ');

	return 0;
	// say goodbye
}



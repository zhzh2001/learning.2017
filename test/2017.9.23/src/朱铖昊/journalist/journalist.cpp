#include<bits/stdc++.h>
using namespace std;
const int X[8]={0,0,1,-1,1,1,-1,-1},Y[8]={1,-1,0,0,1,-1,1,-1},N=205;
char c[N];
int a[N][N],f[N][N],g[N][N],ans,x,y,xx,yy,xxx,yyy,nx,ny,mx,my,l,r,n,m;
pair<int,int>q[N*N];
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
	{
		scanf("%s",c+1);
		for (int j=1;j<=m;++j)
			if (c[j]=='@')
				a[i][j]=1;
	}
	scanf("%d%d%d%d",&x,&y,&xx,&yy);
	while (x!=0)
	{
		memset(f,0,sizeof(f));
		f[xx][yy]=1;
		for (int i=0;i<8;++i)
		{
			xxx=xx;
			yyy=yy;
			for (int j=1;j<=n;++j)
			{
				xxx+=X[i];
				yyy+=Y[i];
				if (a[xxx][yyy]||xxx==0||yyy==0||xxx>n||yyy>m)
					break;
				f[xxx][yyy]=1;
			}
		}
		l=1;
		r=1;
		memset(g,0,sizeof(g));
		g[x][y]=1;
		q[l].first=x;
		q[l].second=y;
		ans=-1;
		if (f[x][y]==1)
		{
			scanf("%d%d%d%d",&x,&y,&xx,&yy);
			puts("0");
			continue;
		}
		while (l<=r)
		{
			nx=q[l].first;
			ny=q[l].second;
			for (int i=0;i<4;++i)
			{
				mx=nx+X[i];
				my=ny+Y[i];
				if (!a[mx][my]&&!g[mx][my]&&mx>=1&&mx<=n&&my>=1&&my<=m)
				{
					if (f[mx][my])
					{
						ans=g[nx][ny];
						break;
					}
					q[++r].first=mx;
					q[r].second=my;
					g[mx][my]=g[nx][ny]+1;
				}
			}
			++l;
		}
		if (ans==-1)
			puts("Naive!");
		else
			printf("%d\n",ans);
		scanf("%d%d%d%d",&x,&y,&xx,&yy);
	}
	return 0;
}
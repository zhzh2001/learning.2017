#include<bits/stdc++.h>
using namespace std;
const int N=300005;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int f[20][N],q[N],n,m,a[N],l,x;
inline int xiao(int x,int y)
{
	return a[x]<=a[y]?x:y;
}
inline int zx(int x,int y)
{
	int l=y-x+1;
	int p=(int)log2(l);
	return xiao(f[p][x],f[p][y-(1<<p)+1]);
}
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	read(n);
	read(m);
	--m;
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<=n;++i)
		f[0][i]=i;
	for (int i=1;i<=20;++i)
		for (int j=1;j+(1<<i)-1<=n;++j)
			f[i][j]=xiao(f[i-1][j],f[i-1][j+(1<<(i-1))]);
	l=1;
	for (int i=1;i<=n;++i)
	{
		if (l>n)
		{
			printf("%d ",a[q[q[0]--]]);
			continue;
		}
		x=zx(l,l+(m-q[0]));
		if (a[x]>a[q[q[0]]]&&q[0]!=0)
			printf("%d ",a[q[q[0]--]]);
		else
		{
			printf("%d ",a[x]);
			for (int j=l;j<x;++j)
				q[++q[0]]=j;
			l=x+1;
		}
	}
	return 0;
}
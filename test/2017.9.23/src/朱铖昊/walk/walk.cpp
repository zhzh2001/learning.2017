#include<bits/stdc++.h>
using namespace std;
#define int long long
inline void read(int &x)
{
	int y=1;
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x=x*y;
}
const int N=100005;
struct data
{
	int x,y;
}a[N];
int f[N],n,x,y,m;
signed main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].y);
		if (a[i].y==2)
			a[i].y=-1;
	}
	/*for (int i=1;i<=n;++i)
		cout<<a[i].x<<' '<<a[i].y<<'\n';*/
	memset(f,0x3f,sizeof(f));
	for (int i=1;i<n;++i)
		if (a[i].y==1&&a[i+1].y==-1)
		{
			f[i]=f[i+1]=(a[i+1].x-a[i].x)/2;
			for (int j=i-1;j>=1&&a[j].y==1;--j)
				f[j]=f[i]+a[i].x-a[j].x;
			for (int j=i+2;j<=n&&a[j].y==-1;++j)
				f[j]=a[j].x-(f[i]+a[i].x);
		}
	/*for (int i=1;i<=n;++i)
		cout<<f[i]<<' ';
	putchar('\n');*/
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		printf("%lld\n",a[y].x+min(x,f[y])*a[y].y);
	}
	return 0;
}

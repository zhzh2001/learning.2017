#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
struct cs{Ll x,y,v,id;}a[N],b[N],c[N];
Ll read(){Ll x=0,f=1;char ch=getchar();while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}return x*f; }
Ll n,m,tb,tc,l,x,y;
Ll er(Ll l,Ll k){
	Ll r=tb,mid,ans=0;
	while(r>=l){
		mid=l+r>>1;
		if(b[mid].x<k)ans=b[mid].x,l=mid+1;else r=mid-1;
	}return ans;
}
int main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(Ll i=1;i<=n;i++){
		a[i].x=read();
		a[i].y=read();
		a[i].id=i;
		if(a[i].y==1)b[++tb]=a[i];else c[++tc]=a[i];
	}
	l=1;c[0].v=-1e17;
	for(Ll i=1;i<=tc;i++){
		Ll k=er(l,c[i].x);
		if(k==0){c[i].v=c[i-1].v;continue;}
		c[i].v=c[i].x-(c[i].x-k)/2;
		while(l<=tb&&b[l].x<=k)b[l++].v=c[i].v;
	}while(l<=tb)b[l++].v=1e17;		
	for(Ll i=1;i<=tb;i++)a[b[i].id].v=b[i].v;
	for(Ll i=1;i<=tc;i++)a[c[i].id].v=c[i].v;
	while(m--){
		scanf("%lld%lld",&y,&x);
		if(a[x].y==1)printf("%lld\n",min(a[x].x+y,a[x].v));
		if(a[x].y==2)printf("%lld\n",max(a[x].x-y,a[x].v));
	}
}


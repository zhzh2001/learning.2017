#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=4e5+5;
int a[N],f[N][26],q[N];
int n,m,x,y,r,top;
void make(){
	for(int i=1;i<=n;i++)f[i][0]=i;
	for(int j=1;j<=25;j++)
		for(int i=1;i<=n;i++){
			if(i+(1<<(j-1))>n){f[i][j]=f[i][j-1];continue;}
			int x=a[f[i][j-1]],y=a[f[i+(1<<(j-1))][j-1]];
			if(x<=y)f[i][j]=f[i][j-1];else f[i][j]=f[i+(1<<(j-1))][j-1];
		}
}
int out(int x,int y){
	if(x==y)return a[x];
	int k=log2(y-x+1);
	return min(a[f[x][k]],a[f[y-(1<<k)+1][k]]);
}
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	make();
	r=1;top=0;
	while(r<=n){
		int v=min(n,r+m-top-1);
		int k=out(r,v);
		if(top&&k>q[top]){printf("%d ",q[top--]);continue;}
		while(a[r]!=k)q[++top]=a[r++];
		printf("%d ",a[r++]);
	}
	while(top)printf("%d ",q[top--]);
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 205
#define M 100005
#define inf (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,sx,sy,ex,ey,cx[4]={-1,1,0,0},cy[4]={0,0,1,-1};
ll ccx[8]={-1,-1,-1,0,0,1,1,1},ccy[8]={-1,0,1,-1,1,-1,0,1};
ll mp[N][N],flag[N][N],vis[N][N],qx[M],qy[M],qz[M];
bool ok(ll x,ll y) { return x&&y&&x<=n&&y<=m; }
void paint(ll x,ll y){
	memset(flag,0,sizeof flag);
	rep(k,0,7){
		ll i=x,j=y;
		while (!mp[i][j]&&ok(i,j)){
			flag[i][j]=1;
			i+=ccx[k]; j+=ccy[k];
		}
	}
}
ll bfs(ll x,ll y){
	memset(vis,0,sizeof vis);
	qx[1]=x; qy[1]=y; qz[1]=0; vis[x][y]=1;
	for (ll he=0,ta=1;he!=ta;){
		ll nx=qx[++he],ny=qy[he],nz=qz[he];
		if (flag[nx][ny]) return nz;
		rep(k,0,3){
			ll tx=nx+cx[k],ty=ny+cy[k];
			if (!ok(tx,ty)||vis[tx][ty]||mp[tx][ty]) continue;
			qx[++ta]=tx; qy[ta]=ty; qz[ta]=nz+1; vis[tx][ty]=1;
		}
	}
	return inf;
}
int main(){
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	n=read(); m=read(); char ch[205];
	rep(i,1,n){
		scanf("%s",ch+1);
		rep(j,1,m) if (ch[j]=='@') mp[i][j]=1;
	}
	sx=read(); sy=read(); ex=read(); ey=read();
	while (sx){
		paint(ex,ey);
		ll ans=bfs(sx,sy);
		if (ans==inf) puts("Naive!");
		else printf("%d\n",ans);
		sx=read(); sy=read(); ex=read(); ey=read();
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 300005
#define inf (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,top,q[N],minn[N<<2],a[N];
void build(ll l,ll r,ll p){
	if (l==r) { minn[p]=a[l]; return; }
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return minn[p];
	ll mid=l+r>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return min(query(l,mid,s,mid,p<<1),query(mid+1,r,mid+1,t,p<<1|1));
}
int main(){
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read(); m=read(); q[top=0]=inf*2;
	rep(i,1,n) a[i]=read();
	build(1,n,1);
	for (ll i=1;i<=n;++i){
		ll t=query(1,n,i,min(i+m-top-1,n),1);
		if (q[top]<=t) printf("%d ",q[top--]),--i;
		else {
			while(a[i]!=t) q[++top]=a[i],++i;
			printf("%d ",t);
		}
	}
	while (top) printf("%d ",q[top--]);
}

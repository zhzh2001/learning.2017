#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,num,top,a[N],x[N],f[N];
struct data{ ll x,f; }q[N];
ll find1(ll v){
	ll l=1,r=num,ans=num+1;
	while (l<=r){
		ll mid=l+r>>1;
		if (a[mid]>=v) ans=mid,r=mid-1;
		else l=mid+1;
	}
	return ans;
}
ll find2(ll v){
	ll l=1,r=num,ans=0;
	while (l<=r){
		ll mid=l+r>>1;
		if (a[mid]<=v) ans=mid,l=mid+1;
		else r=mid-1;
	}
	return ans;
}
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n){
		x[i]=read(); f[i]=read();
		if (f[i-1]==1&&f[i]==2) a[++num]=(x[i-1]+x[i])/2;
	}
	rep(i,1,m){
		ll t=read(),p=read();
		if (f[p]==1){
			ll pos=find1(x[p]);
			if (pos>num) printf("%lld\n",x[p]+t);
			else printf("%lld\n",min(x[p]+t,a[pos]));
		}else{
			ll pos=find2(x[p]);
			if (!pos) printf("%lld\n",x[p]-t);
			else printf("%lld\n",max(x[p]-t,a[pos]));
		}
	}
}

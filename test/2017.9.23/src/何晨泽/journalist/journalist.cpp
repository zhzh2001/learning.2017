//Live long and prosper.
//乘坐东方航空MU2224的旅客请注意，我们非常抱歉地通知各位，您乘坐的航班因天气原因无法准时起飞，具体起飞时间请等候通知
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int Max=300,M=90000;
char c[Max];
int i,j,k,l,m,n,t,w,x,y,sx,sy,ex,ey,ans[Max][Max],q[M][2];
int wx[4]= {-1,0,1,0},wy[4]= {0,-1,0,1},dx[8]={-1,-1,-1,0,0,1,1,1},dy[8]= {-1,0,1,-1,1,-1,0,1};
bool b,a[Max][Max],flag[Max][Max];
inline void China() {
	Rep(i,0,7) { int x=ex,y=ey;
		while(x>=1 && x<=n && y>=1 && y<=m) {
			if(!a[x][y]) break;
			flag[x][y]=true;
			x+=dx[i];y+=dy[i];
		}
	}
}
inline void Eastern() {
	t=w=1;
	q[t][0]=sx;q[t][1]=sy;
}
inline void Airlines() {
	while(t<=w) {
		x=q[t][0];y=q[t][1];
		if(flag[x][y]) {
			printf("%d\n",ans[x][y]);
			b=true;
			break;
		}
		Rep(i,0,3) {
			int xx=x+wx[i],yy=y+wy[i];
			if(xx>=1 && xx<=n && yy>=1 && yy<=m)
				if(a[xx][yy] && (!ans[xx][yy])) {
					ans[xx][yy]=ans[x][y]+1;
					w++;
					q[w][0]=xx;
					q[w][1]=yy;
				}
		}
		t++;
	}
}
int main() {
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf ("%d%d",&n,&m);
	Rep(i,1,n) {
		scanf ("%s",c);
		Rep(j,0,m-1) {
			a[i][j+1]=c[j]=='O'?true:false;
		}
	}
	scanf ("%d%d%d%d",&sx,&sy,&ex,&ey);
	while (1) {
		if (sx==0 && sy==0 && ex==0 && ey==0) return 0;
		memset(flag,false,sizeof(flag));
		memset(ans,0,sizeof(ans));
		b=false;
		China();
		Eastern();
		Airlines();
		if(!b) cout<<"Naive!"<<endl;
		scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
	}
}

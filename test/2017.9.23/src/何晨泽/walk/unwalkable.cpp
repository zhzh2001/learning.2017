//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
#define maxn 100010
using namespace std;
int n,Q,q[maxn];
long long pos[maxn],p[maxn][2],t;
int main() {
	freopen("1.in","r",stdin);
	freopen("1.out","w",stdout);
	scanf ("%d%d",&n,&Q);
	Rep(i,1,n) scanf ("%I64d%I64d",&p[i][0],&p[i][1]);
	Rep(I,1,Q) {
		scanf ("%I64d%d",&t,&q[I]);
		Rep(i,1,n)
			if(p[i][1]==2) {
				if(i==1)
					pos[i]=p[i][0]-t;
				else if(p[i-1][1]==2)
					pos[i]=max(pos[i-1],p[i][0]-t);
				else pos[i]=max(p[i][0]/2+p[i-1][0]/2,p[i][0]-t);
			}
		Drp(i,n,1)
			if(p[i][1]==1) {
				if(i==n)
					pos[i]=p[i][0]+t;
				else if(p[i+1][1]==1)
					pos[i]=min(pos[i+1],p[i][0]+t);
				else pos[i]=min(p[i][0]/2+p[i+1][0]/2,p[i][0]+t);
			}
		cout<<pos[q[I]]<<endl;
	}
	return 0;
}

//Live long and prosper.
//乘坐东方航空MU2224的旅客请注意，我们非常抱歉地通知各位，您乘坐的航班因天气原因无法准时起飞，具体起飞时间请等候通知
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
const ff Max=1e16;
using namespace std;
struct node {
	ff x,f;
}wa[200000];
ff i,j,k,l,r,m,n,q,now;
ff ans[200000];
int main() {
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>q;
	Rep(i,1,n) {
		cin>>wa[i].x>>wa[i].f;
	}
	l=-Max; now=Max;
	Rep(i,1,n) {
		if (wa[i].f==1) {
			l=wa[i].x;
			now=Max;
		}
		else {
			ans[i]=min(now,(l+wa[i].x)/2);
			now=ans[i];
		}
	}
	r=Max; now=-Max;
	Drp(i,n,1) {
		if (wa[i].f==2) {
			r=wa[i].x;
			now=-Max;
		}
		else {
			ans[i]=max(now,(r+wa[i].x)/2);
			now=ans[i];
		}
	}
	Rep(i,1,q) { ff t,pos;
		cin>>t>>pos;
		if (wa[pos].f==1) {
			cout<<min(ans[pos],wa[pos].x+t)<<endl;
		}
		else {
			cout<<max(ans[pos],wa[pos].x-t)<<endl;
		}
	}
	return 0;
}

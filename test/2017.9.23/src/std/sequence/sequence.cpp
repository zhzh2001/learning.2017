#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int s[300010],a[300010],top=0,w;
int f[300010][20],n,m;
inline void build(){
	for(int i=n;i;i--){
		f[i][0]=i;
		for(int j=1;;j++){
			f[i][j]=a[f[i][j-1]]<=a[f[i+(1<<j-1)][j-1]]?f[i][j-1]:f[i+(1<<j-1)][j-1];
			if(i+(1<<j)-1>n)break;
		}
	}
}
inline int smin(int l,int r){
	int p=floor(log2(r-l+1));
	int ans=a[f[l][p]]<=a[f[r-(1<<p)+1][p]]?f[l][p]:f[r-(1<<p)+1][p];
	return ans;
}
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read();m=read();s[0]=2e9+7;w=0;
	for(int i=1;i<=n;i++)a[i]=read();
	build();
	for(int i=1;i<=n;i++){
		if(w<n){
			int x=smin(w+1,min(n,w+m-top));int minx=a[x];
			if(s[top]>minx)while(w<x)w++,s[++top]=a[w];
		}
		printf("%d ",s[top--]);
	}
	while(top)printf("%d ",s[top--]);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
int t[1200010],s[300010],a[300010],top=0,w;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void build(int l,int r,int nod){
	if(l==r){t[nod]=l;return;}
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=a[t[nod*2]]<=a[t[nod*2+1]]?t[nod*2]:t[nod*2+1];
}
inline int smin(int l,int r,int i,int j,int nod){
	if(l>=i&&r<=j)return t[nod];
	int mid=l+r>>1;
	if(j<=mid)return smin(l,mid,i,j,nod*2);
	else if(i>mid)return smin(mid+1,r,i,j,nod*2+1);
	else{
		int x=smin(l,mid,i,mid,nod*2),y=smin(mid+1,r,mid+1,j,nod*2+1);
		return a[x]<=a[y]?x:y;
	}
}
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	int n=read(),m=read();s[0]=2e9+7;w=0;
	for(int i=1;i<=n;i++)a[i]=read();
	build(1,n,1);
	for(int i=1;i<=n;i++){
		if(w<n){
			int x=smin(1,n,w+1,min(n,w+m-top),1);int minx=a[x];
			if(s[top]>minx)while(w<x)w++,s[++top]=a[w];
		}
		printf("%d ",s[top--]);
	}
	while(top)printf("%d ",s[top--]);
	return 0;
}
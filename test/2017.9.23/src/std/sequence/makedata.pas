var
	n,m,i,a,b:longint;
	x:array[1..300000]of longint;
procedure sort(l,r:longint);
var i,j,mid,t:longint;
begin
	i:=l;j:=r;
	mid:=x[(l+r) div 2];
	repeat
		while x[i]<mid do inc(i);
		while mid<x[j] do dec(j);
		if not(i>j) then
		begin
			t:=x[i];x[i]:=x[j];x[j]:=t;
			inc(i);j:=j-1;
		end;
	until i>j;
	if l<j then sort(l,j);
	if i<r then sort(i,r);
end;
begin
	assign(output,'sequence8.in');rewrite(output);
	randomize;
	n:=200000;m:=n;
	writeln(n,' ',m);
	for i:=1 to n do x[i]:=random(1000000000);
	sort(1,n div 2);sort(n div 2+1,n);
	a:=1;b:=n div 2+1;
	for i:=1 to n do 
	begin
		if i mod 2=1 then 
		begin
			write(x[a],' ');inc(a);
		end else begin
			write(x[b],' ');inc(b);
		end;
	end;
	close(output);
end.
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
struct ppap{int x,f;}a[100010];
int fk[100010],k[100010],n,t,q;
inline bool cmp(ppap a,ppap b){return a.x<b.x;}
signed main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	scanf("%lld%lld",&n,&q);
	for(int i=1;i<=n;i++)scanf("%lld%lld",&a[i].x,&a[i].f);
	a[0].f=2;int tot=0;
	for(int i=1;i<=n;i++){
		if(a[i].f==1&&a[i-1].f==2)++tot;
		if(a[i].f==2&&a[i-1].f==1)k[tot]=(a[i].x+a[i-1].x)/2;
		fk[i]=tot;
	}
	if(a[n].f==1)tot--;
	for(int i=1;i<=q;i++){
		int t,x;scanf("%lld%lld",&t,&x);
		int p=fk[x],ans=a[x].x;
		if(p==0||p==tot+1)ans+=t*((a[x].f==1)?1:-1);
		else{
			if(a[x].f==1)ans=min(k[p],ans+t);
			else ans=max(k[p],ans-t);
		}
		printf("%lld\n",ans);
	}
	return 0;
}
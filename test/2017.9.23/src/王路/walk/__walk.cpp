#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <stack>
using namespace std;

ifstream fin("walk.in");
ofstream fout("walk.out");

const int MaxN = 100005;

struct Node {
	int x, f, id, lk;
} a[MaxN];

int main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i <= n; ++i) {
		fin >> a[i].x >> a[i].f;
		a[i].id = i;
		if (a[i].f == 2) a[i].f = -1;
	}
	stack<int> S;
	for (int i = 1; i <= n; ++i) {
		if (a[i].f == 1) {
			S.push(i);
		} else {
			if (!S.empty()) {
				a[i].lk = S.top();
				a[S.top()].lk = i;
				S.pop(); 
			}
		}
	}
	for (int i = 1; i <= q; ++i) {
		int t, p;
		fin >> t >> p;
		if (!a[p].lk) {
			fout << a[p].x + t * a[p].f << endl;
			continue;
		}
		int mid = (a[p].x + a[a[p].lk].x + 1) / 2, cost = abs(mid - a[p].x);
		if (t < cost) {
			fout << a[p].x + t * a[p].f << endl;
			continue;
		}
		fout << mid << endl;
	}
	return 0;
}

#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <stack>
#include <set>
using namespace std;

#define int long long

ifstream fin("walk.in");
ofstream fout("walk.out");

const int MaxN = 100005, inf = 1e16;

struct Node {
	int x, f, id;
} a[MaxN];

struct State {
	int l, r, dis, mid, tim;
	State() {}
	State(int l, int r, int dis) : l(l), r(r), dis(dis) {}
	bool operator<(const State &other) const {
		return dis < other.dis;	
	}
} b[MaxN];

struct Query {
	int t, p, id;
} c[MaxN];

int cnt, ans[MaxN];

set<int> P;

signed main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i <= n; ++i) {
		fin >> a[i].x >> a[i].f;
		a[i].id = i;
		if (a[i].f == 2) a[i].f = -1;
	}
	stack<int> S;
	for (int i = 1; i <= n; ++i) {
		if (a[i].f == 1) {
			S.push(i);
		} else {
			if (!S.empty()) {
				int a = ::a[S.top()].x, c = ::a[i].x;
//				cout << a << ' ' << c << endl;
				b[++cnt] = State(min(a, c), max(a, c), abs(a - c));
				b[cnt].mid = (a + c) / 2;
				b[cnt].tim = b[cnt].dis / 2;
				S.pop();
			}
		}
	}
	sort(b + 1, b + cnt + 1);
	for (int i = 1; i <= q; ++i) {
		fin >> c[i].t >> c[i].p;
		c[i].id = i;
	}
	int pt = 1;
	b[cnt + 1].tim = inf;
	for (int i = 0; i <= cnt; ++i) {
		if (i != 0) P.insert(b[i].mid);
		while (c[pt].t >= b[i].tim && c[pt].t < b[i + 1].tim && pt <= q) {
			if (!P.empty()) {
				set<int>::iterator it = P.lower_bound(a[c[pt].p].x);
				if (a[i].f < 0) {
					ans[c[pt].id] = *it;
					if (abs(ans[c[pt].id] - a[c[pt].p].x) > c[pt].t) {
						ans[c[pt].id] = a[c[pt].p].x + c[pt].t * a[c[pt].p].f;
					}
				} else {
					ans[c[pt].id] = *--it;	
					if (abs(ans[c[pt].id] - a[c[pt].p].x) > c[pt].t) {
						ans[c[pt].id] = a[c[pt].p].x + c[pt].t * a[c[pt].p].f;
					}
				}
			} else {
				ans[c[pt].id] = a[c[pt].p].x + c[pt].t * a[c[pt].p].f;
			}
			++pt;
		}
	}
	for (int i = 1; i <= q; ++i) {
		fout << ans[i] << endl;
	}
	return 0;
}

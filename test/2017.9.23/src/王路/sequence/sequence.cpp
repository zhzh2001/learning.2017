#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stack>
#include <algorithm>
#include <cassert>
using namespace std;

ifstream fin("sequence.in");
ofstream fout("sequence.out");

const int MaxN = 300005, inf = 0x3f3f3f3f;

int a[MaxN], n, m;
bool vis[MaxN];

//void work(int l, int r, int m) {
//	if (l > r) return;
//	if (m <= 0) return;
//	int tp = min(l + m - 1, n);
//	int mi = f[l][tp];
//	fout << a[g[l][tp]] << ' ';
//	vis[g[l][tp]] = true;
//	work(g[l][tp] + 1, r, m - (g[l][tp] - l + 1));
//	for (int i = g[l][tp] - 1; i >= l; --i) {
//		fout << a[i] << ' ';
//		vis[i] = true;
//	}
//}

struct Node {
	int l, r, min, pos;
} tr[MaxN << 2];

void build(int x, int l, int r) {
	tr[x].l = l;
	tr[x].r = r;
	if (l == r) {
		tr[x].min = a[l];
		tr[x].pos = l;
		return;
	}
	int mid = (l + r) >> 1;
	build(x << 1, l, mid);
	build(x << 1 | 1, mid + 1, r);
	if (tr[x << 1].min <= tr[x << 1 | 1].min) {
		tr[x].min = tr[x << 1].min;
		tr[x].pos = tr[x << 1].pos;
	} else {
		tr[x].min = tr[x << 1 | 1].min;
		tr[x].pos = tr[x << 1 | 1].pos;
	}
}

pair<int, int> query(int x, int l, int r) {
	if (l <= tr[x].l && r >= tr[x].r) 
		return make_pair(tr[x].min, tr[x].pos);
	if (l > tr[x].r || r < tr[x].l) 
		return make_pair(inf, inf);
	pair<int, int> a = query(x << 1, l, r), b = query(x << 1 | 1, l, r);
	if (a.first <= b.first) {
		return a;
	} else {
		return b;
	}
}

void work(int l, int r, int m) {
	int now = l;
	stack<int> S;
	for ( ; m > 0; ) {
		if (l > n) break;
		int tp = min(l + m - 1, n);
		int k = query(1, l, tp).second;
		fout << a[k] << ' ';
		vis[k] = true;
		S.push(k);
		m -= (k - l + 1);
		l = k + 1;
	}
	while (!S.empty()) {
		for (int i = S.top() - 1; i > 0 && !vis[i]; --i) {
			fout << a[i] << ' ';
			vis[i] = true;
		} 
		S.pop();
	}
}

int main() {
	fin >> n >> m;
	m = min(m, n);
	for (int i = 1; i <= n; ++i) {
		fin >> a[i];
	}
	build(1, 1, n);
	for (int i = 1; i <= n; ++i) {
		if (!vis[i]) {
			work(i, n, m);
		}
	}
	fout << endl;
	return 0;
}

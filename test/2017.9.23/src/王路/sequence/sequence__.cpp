#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <stack>
using namespace std;

ifstream fin("sequence.in");
ofstream fout("baoli.out");

const int MaxN = 5005;

int a[MaxN], f[MaxN][MaxN], g[MaxN][MaxN], n, m;
bool vis[MaxN];

void work(int l, int r, int m) {
	if (l > r) return;
	if (m <= 0) return;
	int tp = min(l + m - 1, n);
	int mi = f[l][tp];
	fout << a[g[l][tp]] << ' ';
	vis[g[l][tp]] = true;
//	cout << g[l][tp] << endl;
	work(g[l][tp] + 1, r, m - (g[l][tp] - l + 1));
	for (int i = g[l][tp] - 1; i >= l; --i) {
		fout << a[i] << ' ';
		vis[i] = true;
	}
}

//void work(int l, int r, int m) {
//	int now = l;
//	stack<int> S;
//	for ( ; m > 0; ) {
//		if (l > n) break;
//		int tp = min(l + m - 1, n);
//		int k = g[l][tp];
//		fout << a[k] << ' ';
//		vis[k] = true;
//		S.push(k);
//		l = k + 1;
//		m = m - (k - l + 1);
//	}
//	while (!S.empty()) {
//		for (int i = S.top() - 1; i > 0 && !vis[i]; --i) {
//			fout << a[i] << ' ';
//			vis[i] = true;
//		} 
//		S.pop();
//	}
//}

int main() {
	fin >> n >> m;
	m = min(m, n);
	for (int i = 1; i <= n; ++i) {
		fin >> a[i];
	}
	memset(f, 0x3f, sizeof f);
	for (int i = 1; i <= n; ++i) {
		for (int j = i; j <= n; ++j) {
			if (j != i) {
				f[i][j] = f[i][j - 1];
				g[i][j] = g[i][j - 1];
			}
			if (a[j] < f[i][j]) {
				f[i][j] = a[j];
				g[i][j] = j;
			}
		}
	}
	for (int i = 1; i <= n; ++i) {
		if (!vis[i])
			work(i, n, m);
	}
	fout << endl;
	return 0;
}

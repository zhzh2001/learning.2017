#include <bits/stdc++.h>
#include <Windows.h>

using namespace std;

int random() { 
	return (rand() << 15) + rand();
}

int uniform(int p) {
	return random() % p;
}

ofstream fout("sequence.in");

int main() {
	srand(GetTickCount());
	int n = 500, m = uniform(n);
	fout << n << ' ' << m << endl;
	for (int i = 1; i <= n; ++i) {
		fout << uniform(n) << ' ';
	}
	fout << endl;
	return 0;
}

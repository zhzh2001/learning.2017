#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;

ifstream fin("journalist.in");
ofstream fout("journalist.out");

const int MaxN = 205, inf = 0x3f3f3f3f;

int mat[MaxN][MaxN];
bool vis[MaxN][MaxN];

int dx[] = { 1, 0, -1, 0, 1, 1, -1, -1 }, dy[] = { 0, 1, 0, -1, 1, -1, 1, -1 };

typedef pair<int, int> pii;

int sx, sy, ex, ey;
int n, m;
int dis[MaxN][MaxN];
bool visit[MaxN][MaxN];

void spfa() {
	queue<pii> Q;
	memset(dis, 0x3f, sizeof dis);
	memset(visit, 0x00, sizeof vis);
	Q.push(make_pair(sx, sy));
	dis[sx][sy] = 0;
	while (!Q.empty()) {
		pii now = Q.front();
		visit[now.first][now.second] = 0;
		for (int d = 0; d < 8; ++d) {
			int nx = now.first + dx[d], ny = now.second + dy[d];
			if (nx < 1 || ny < 1 || nx > n || ny > m || !mat[nx][ny]) continue;
			if (dis[nx][ny] > dis[now.first][now.second] + 1) {
				dis[nx][ny] = dis[now.first][now.second] + 1;
				if (!visit[nx][ny]) {
					visit[nx][ny] = true;
					Q.push(make_pair(nx, ny));
				}
			}
		}
		Q.pop();
	}
}

int main() {
	fin >> n >> m;
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			char ch;
			fin >> ch;
			if (ch == '@') mat[i][j] = 0;
				else mat[i][j] = 1;
		}
	}
	while (true) {
		memset(vis, 0x00, sizeof vis);
		fin >> sx >> sy >> ex >> ey;
		vis[ex][ey] = true;
		if (sx == 0 && sy == 0 && ex == 0 && ey == 0) break;
		for (int d = 0; d < 8; ++d) {
			for (int i = 1; ; ++i) {
				int nx = ex + dx[d] * i, ny = ey + dy[d] * i;
				if (!mat[nx][ny]) break;
				vis[nx][ny] = true;
			}
		}
		if (vis[sx][sy]) {
			fout << 0 << endl;
			continue;
		}
		spfa();
		int ans = inf;
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= m; ++j) {
				if (vis[i][j]) {
					ans = min(ans, dis[i][j]);	
				}
			}
		}
		if (ans < inf) {
			fout << ans << endl;	
		} else {
			fout << "Naive!" << endl;
		}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,a[210][210],id[210][210],sx,sy,ex,ey,time,ID,x,y;
char mp[210][210];
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("j.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; i++)
		scanf("%s",mp[i]+1);
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++)
			id[i][j]=(i-1)*m+j;
	for (int i=0; i<=n*m; i++)
		for (int j=0; j<=n*m; j++)
			a[i][j]=-1;
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++)
		{
			if (mp[i][j]=='@') continue;
			a[id[i][j]][id[i][j]]=0;
			if (mp[i][j+1]=='O') a[id[i][j]][id[i][j+1]]=1;
			if (mp[i][j-1]=='O') a[id[i][j]][id[i][j-1]]=1;
			if (mp[i+1][j]=='O') a[id[i][j]][id[i+1][j]]=1;
			if (mp[i-1][j]=='O') a[id[i][j]][id[i-1][j]]=1;
		}
	for (int k=1; k<=n*m; k++)
		for (int i=1; i<=n*m; i++)
			for (int j=1; j<=n*m; j++)
				if (a[i][k]!=-1&&a[k][j]!=-1&&i!=j&&j!=k&&i!=k)
				{
					if (a[i][j]==-1) a[i][j]=a[i][k]+a[k][j];
					else a[i][j]=min(a[i][j],a[i][k]+a[k][j]);
				}
	while (1)
	{
		scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
		if (sx==0) return 0;
		time=2147483647;
		ID=id[sx][sy];
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x-1;
			y=y-1;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x+1;
			y=y-1;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x-1;
			y=y+1;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x+1;
			y=y+1;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x;
			y=y+1;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x;
			y=y-1;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x-1;
			y=y;
		}
		x=ex,y=ey;
		while(x>=1&&y>=1&&x<=n&&y<=m&&mp[x][y]!='@')
		{
			if (a[ID][id[x][y]]!=-1)
			time=min(time,a[ID][id[x][y]]);
			x=x+1;
			y=y;
		}
		if (time!=2147483647) printf("%d\n",time);
		else puts("Naive!");
	}
}

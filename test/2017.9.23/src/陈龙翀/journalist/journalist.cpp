#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<ctime>
#include<cstdlib>
using namespace std;
int dx[8]={-1,-1,0,1,1,1,0,-1},dy[8]={0,1,1,1,0,-1,-1,-1};
int MIN,n,m,sx,sy,ex,ey,a[300][300],M[300][300],A[300][300];
char c[300];
void dfs(int x,int y,int step)
{
	if (step>=MIN) return;
	a[x][y]=step;
	if (A[x][y]){MIN=step;return;}
	for (int i=0; i<8; i++)
		if (dx[i]==0||dy[i]==0)
			if (!M[x+dx[i]][y+dy[i]])
				if (a[x+dx[i]][y+dy[i]]>step+1)
					dfs(x+dx[i],y+dy[i],step+1);
}
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; i++)
	{
		scanf("%s",c+1);
		for (int j=1; j<=m; j++)
			if (c[j]=='@') M[i][j]=1;
	}
	for (int i=0; i<=n+1; i++)M[i][0]=1,M[i][m+1]=1;
	for (int i=0; i<=m+1; i++)M[0][i]=1,M[n+1][i]=1;
	while(1)
	{
		scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
		if (sx==0) return 0;
		for (int i=0; i<=n; i++)
			for (int j=0; j<=m; j++)
				A[i][j]=0,a[i][j]=100000;
		a[sx][sy]=0;
		for (int i=0; i<8; i++)
		{
			int t=0;
			while (!M[ex+t*dx[i]][ey+t*dy[i]])
				A[ex+t*dx[i]][ey+t*dy[i]]=1,t++;
		}
		MIN=2147483647;
		dfs(sx,sy,0);
		if (MIN==2147483647) puts("Naive!");
		else printf("%d\n",MIN);
	}
}

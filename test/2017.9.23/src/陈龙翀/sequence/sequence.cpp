#include<cstdio>
#include<algorithm>
#include<queue>
using namespace std;
int read()
{
	char ch;ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	int t=0;
	while (ch>='0'&&ch<='9') t=t*10+ch-48,ch=getchar();
	return t;
}
void write(int x)
{
	if (x<10) {putchar(x+48); return;}
	write(x/10);
	write(x%10);
}
priority_queue <int,vector<int>,greater<int> >q;
int n,m,x;
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read(),m=read();
	if (n<m) m=n;
	for (int i=1; i<=m; i++)
	{
		x=read();
		q.push(x);
	}
	for (int i=m+1; i<=n; i++)
	{
		int t=q.top();
		write(t),putchar(' ');
		q.pop();
		x=read();
		q.push(x);
	}
	for (int i=1; i<=m; i++)
	{
		int t=q.top();
		write(t),putchar(' ');
		q.pop();
	}
}

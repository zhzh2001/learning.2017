#include<bits/stdc++.h>
#define ll long long
#define N 100005
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
inline void print(ll x){
	if (!x){
		puts("0");
		return;
	}
	if (x<0){
		putchar('-');
		x=-x;
	}
	static int a[20],top;
	top=0;
	for (;x;x/=10) a[++top]=x%10;
	for (;top;top--) putchar('0'+a[top]);
	putchar('\n');
}
int n,q;
ll x[N],la[N],y[N],lap,a,b;
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<=n;i++)
		x[i]=read(),y[i]=read();
	for (int i=1;i<n;i++)
		if (y[i]==1&&y[i+1]==2) la[i]=la[i+1]=(x[i+1]-x[i])/2;
	lap=-1e18;
	for (int i=n;i;i--)
		if (y[i]==1){
			if (la[i]) lap=x[i]+la[i];
			else if (lap>-1e18) la[i]=lap-x[i];
		}
	lap=-1e18;
	for (int i=1;i<=n;i++)
		if (y[i]==2){
			if (la[i]) lap=x[i]-la[i];
			else if (lap>-1e18) la[i]=x[i]-lap;
		}
	while (q--){
		a=read(); b=read();
		print(x[b]+(la[b]?min(la[b],a):a)*(y[b]==1?1:-1));
	}
}

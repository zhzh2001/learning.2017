#include<bits/stdc++.h>
#define ll long long
#define N 100005
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
int n,q,a,b,x[N],y[N],ans[N][1005];
int main(){
	freopen("walk.in","r",stdin);
	freopen("alubajzq.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<=n;i++)
		ans[0][i]=x[i]=read(),y[i]=read();
	for (int i=1;i<=100000;i++){
		for (int j=1;j<=n;j++)
			x[j]+=(y[j]==0?0:(y[j]==1?1:-1));
		for (int j=1;j<n;j++)
			if (x[j]==x[j+1]) y[j]=y[j+1]=0;
		for (int j=1;j<=n;j++)
			ans[i][j]=x[j];
	}
	while (q--){
		a=read(); b=read();
		printf("%d\n",ans[a][b]);
	}
}

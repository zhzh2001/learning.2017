#include<bits/stdc++.h>
#define N 300005
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
inline void print(int x){
	static int a[15],top;
	if (x<0){
		putchar('-');
		x=-x;
	}
	a[1]=top=0;
	for (;x;x/=10) a[++top]=x%10;
	top=max(top,1);
	for (;top;top--) putchar('0'+a[top]);
	putchar(' ');
}
int n,m;
struct heap{
	int a[300005],tot;
	heap(){tot=0;}
	inline void push(int x){
		a[++tot]=x; x=tot;
		for (;(x!=1)&&a[x]<a[x>>1];x>>=1)
			swap(a[x],a[x>>1]);
	}
	inline void pop(){
		print(a[1]);
		a[1]=a[tot--];
		for (int x=1,s=x<<1;s<=tot;x=s,s<<=1){
			if (s+1<=tot&&a[s+1]<a[s]) s++;
			if (a[x]<=a[s]) break;
			swap(a[x],a[s]);
		}
	}
}q;
int main(){
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read(); m=read()+1;
	for (int i=1;i<=min(m,n);i++) q.push(read());
	for (int i=m+1;i<=n;i++)
		q.pop(),q.push(read());
	while (q.tot) q.pop();
}

#include<bits/stdc++.h>
using namespace std;
char mp[205][205];
int n,m,sx,sy,ex,ey,dis[205][205],ok[205][205],q[40005][2];
int d[8][2]={{0,1},{0,-1},{1,0},{-1,0},{-1,-1},{-1,1},{1,-1},{1,1}};
int main(){
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		scanf("%s",mp[i]+1);
	while (scanf("%d%d%d%d",&sx,&sy,&ex,&ey)&&sx&&sy&&ex&&ey){
		memset(ok,0,sizeof(ok));
		for (int i=0;i<8;i++){
			int x=ex,y=ey;
			for (;x>0&&x<=n&&y>0&&y<=m&&mp[x][y]!='@';)
				ok[x][y]=1,x+=d[i][0],y+=d[i][1];
		}
		memset(dis,-1,sizeof(dis));
		dis[sx][sy]=0;
		int h=0,t=1,ans=-1;
		q[1][0]=sx; q[1][1]=sy;
		while (h!=t){
			int x=q[++h][0],y=q[h][1];
			if (ok[x][y]){
				ans=dis[x][y];
				break;
			}
			for (int i=0;i<4;i++){
				int xx=x+d[i][0],yy=y+d[i][1];
				if (xx<1||xx>n||yy<1||yy>m||mp[xx][yy]=='@'||dis[xx][yy]!=-1) continue;
				dis[xx][yy]=dis[x][y]+1;
				q[++t][0]=xx; q[t][1]=yy;
			}
		}
		if (ans!=-1) printf("%d\n",ans);
		else puts("Naive!");
	}
}

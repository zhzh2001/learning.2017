#include<stdio.h>
#include<algorithm>
#include<iostream>
#include<string.h>
#include<string>
#include<math.h>
#include<vector>
#include<set>
#include<map>
#include<stdlib.h>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,q,f[N],g[N];
struct data{ll x;int f;}a[N];
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read();q=read();
	For(i,1,n) a[i]=(data){read(),read()};
	a[0].f=2;a[n+1].f=1;f[0]=0;
	For(i,1,n){
		if(a[i].f==a[i-1].f&&a[i].f==2) f[i]=f[i-1];
		else f[i]=i;
	}
	g[n+1]=n+1;
	Rep(i,n,1){
		if(a[i].f==a[i+1].f&&a[i].f==1) g[i]=g[i+1];
		else g[i]=i;
	}
/*	cout<<endl;
	For(i,1,n) cout<<f[i]<<" ";
	cout<<endl;
	For(i,1,n) cout<<g[i]<<" ";
	cout<<endl;cout<<endl;*/
	while(q--){
		ll tim=read(),pos=read();
		if(a[pos].f==1){
			int x=g[pos];
			if(x==n+1) printf("%lld\n",a[pos].x+tim);
			else{
				ll mid=(a[x].x+a[x+1].x)/2;
				if(a[pos].x+tim>mid) printf("%lld\n",mid);
				else printf("%lld\n",a[pos].x+tim);
			}
		}else{
			int x=f[pos];
			if(x==0) printf("%lld\n",a[pos].x-tim);
			else{
				ll mid=(a[x].x+a[x-1].x)/2;
				if(a[pos].x-tim<mid) printf("%lld\n",mid);
				else printf("%lld\n",a[pos].x-tim);
			}
		}
	}
	return 0;
}

#include<stdio.h>
#include<algorithm>
#include<iostream>
#include<string.h>
#include<string>
#include<math.h>
#include<vector>
#include<set>
#include<map>
#include<stdlib.h>
#define ll long long
#define N 1005
#define inf 2000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
int n,m,a[N],st[N];
inline void solve(){
	int i=1,top=0;st[top]=2000000000;
	while(i<=n){
		int mn=inf;
		For(j,i,min(i+(m-top)-1,n)) mn=min(mn,a[j]);
		if(mn>=st[top]){cout<<st[top]<<" ";top--;continue;}
		For(j,i,i+(m-top)-1){
			i++;if(mn==a[j]){cout<<a[j]<<" ";break;}
			st[++top]=a[j];
		}
	}
	Rep(i,top,1) cout<<st[i]<<" ";
}
int main(){
	freopen("sequence.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();m=min(n,m);
	For(i,1,n) a[i]=read();
	solve();
	return 0;
}

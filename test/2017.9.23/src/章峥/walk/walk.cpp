#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("walk.in");
ofstream fout("walk.out");
const int N = 100005;
const long long INF = 1e18;
long long x[N], block[N];
int f[N];
int main()
{
	int n, q;
	fin >> n >> q;
	for (int i = 1; i <= n; i++)
	{
		fin >> x[i] >> f[i];
		if (f[i] == 1)
			block[i] = INF;
		else
			block[i] = -INF;
	}
	for (int i = 1; i < n; i++)
		if (f[i] == 1 && f[i + 1] == 2)
		{
			block[i] = block[i + 1] = (x[i] + x[i + 1]) / 2;
			for (int j = i - 1; j && f[j] == 1; j--)
				block[j] = block[i];
			for (int j = i + 2; j <= n && f[j] == 2; j++)
				block[j] = block[i];
		}
	while (q--)
	{
		long long t;
		int p;
		fin >> t >> p;
		if (t <= abs(x[p] - block[p]))
			if (f[p] == 1)
				fout << x[p] + t << endl;
			else
				fout << x[p] - t << endl;
		else
			fout << block[p] << endl;
	}
	return 0;
}
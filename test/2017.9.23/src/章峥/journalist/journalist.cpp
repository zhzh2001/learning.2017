#include <fstream>
#include <string>
#include <cstring>
#include <queue>
#include <algorithm>
using namespace std;
ifstream fin("journalist.in");
ofstream fout("journalist.out");
const int N = 200, INF = 0x3f3f3f3f, dx[] = {-1, 1, 0, 0, -1, -1, 1, 1}, dy[] = {0, 0, -1, 1, -1, 1, -1, 1};
string mat[N];
bool mark[N][N];
int d[N][N];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 0; i < n; i++)
		fin >> mat[i];
	int sx, sy, tx, ty;
	while (fin >> sx >> sy >> tx >> ty && sx)
	{
		sx--;
		sy--;
		tx--;
		ty--;
		memset(mark, false, sizeof(mark));
		for (int i = 0; i < 8; i++)
		{
			int x = tx, y = ty;
			while (x >= 0 && x < n && y >= 0 && y < m && mat[x][y] == 'O')
			{
				mark[x][y] = true;
				x += dx[i];
				y += dy[i];
			}
		}
		memset(d, 0x3f, sizeof(d));
		d[sx][sy] = 0;
		typedef pair<int, int> state;
		queue<state> Q;
		Q.push(make_pair(sx, sy));
		int ans = INF;
		while (!Q.empty())
		{
			state k = Q.front();
			Q.pop();
			if (mark[k.first][k.second])
			{
				ans = d[k.first][k.second];
				break;
			}
			for (int i = 0; i < 4; i++)
			{
				int nx = k.first + dx[i], ny = k.second + dy[i];
				if (nx >= 0 && nx < n && ny >= 0 && ny < m && mat[nx][ny] == 'O' && d[nx][ny] == INF)
				{
					d[nx][ny] = d[k.first][k.second] + 1;
					Q.push(make_pair(nx, ny));
				}
			}
		}
		if (ans == INF)
			fout << "Naive!\n";
		else
			fout << ans << endl;
	}
	return 0;
}
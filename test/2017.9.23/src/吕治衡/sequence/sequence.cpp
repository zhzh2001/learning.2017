#include<bits/stdc++.h>
using namespace std;
int n,m,a[1000000],z[1000000],d,r,sum[2000000],k;
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1;else kk=c-'0';c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return k*kk; 
}
void build(int l,int r,int d){
	if (l==r){sum[d]=a[l];return;}
	int m=(l+r)/2;build(l,m,d*2);build(m+1,r,d*2+1);
	sum[d]=min(sum[d*2],sum[d*2+1]);
}
int findit(int x,int y,int l,int r,int d){
	if (x<=l&&y>=r)return sum[d];
	int m=(l+r)/2,ans=2e9;
	if (x<=m)ans=min(findit(x,y,l,m,d*2),ans);
	if (y>m)ans=min(findit(x,y,m+1,r,d*2+1),ans);
	return ans;
}
int main(){
	freopen("sequence.in","r",stdin);freopen("sequence.out","w",stdout); 
	n=read();m=read();for (int i=1;i<=n;i++)a[i]=read();
	build(1,n,1);r=1;z[0]=2e9;while (r<=n){
		k=findit(r,r+m-d-1,1,n,1);if (z[d]<=k){
			cout<<z[d]<<' ';d--;
		}else{
			while (a[r]!=k)z[++d]=a[r],r++;
			cout<<a[r]<<' ';r++;
		}
	}while (d)cout<<z[d]<<' ',d--;
}

#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 600010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll ned[maxn],n,Q,x[maxn],p[maxn];	bool mark[maxn];
int main(){
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	n=read();	Q=read();
	For(i,1,n)		x[i]=read(),p[i]=read();
	For(i,1,n-1)	if (p[i]==1&&p[i+1]==2)				mark[i]=mark[i+1]=1,ned[i]=ned[i+1]=(x[i+1]-x[i])/2;
	For(i,1,n-1)	if (p[i]==2&&p[i+1]==2&&mark[i])	mark[i+1]=1,ned[i+1]=ned[i]+x[i+1]-x[i];
	FOr(i,n,2)		if (p[i]==1&&p[i-1]==1&&mark[i])	mark[i-1]=1,ned[i-1]=ned[i]+x[i]-x[i-1];
	For(i,1,Q){
		ll T=read(),P=read();
		if (mark[P])	T=min(T,ned[P]);
		writeln(p[P]==1?x[P]+T:x[P]-T);
	}
}

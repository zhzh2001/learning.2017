#include<ctime>
#include<cstdio>
#include<memory.h>
#include<algorithm>
#define ll int
#define maxn 30
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll q[maxn],ans[maxn],answ[maxn],n,M,v[maxn],cnt,a[maxn];
bool cmp(ll a[],ll b[]){
	For(i,1,n)	if (a[i]<b[i])	return 1;
	else if (a[i]>b[i])	return 0;
	return 0;
}
void dfs(ll n,ll m){
	if (!n&&!m){
		ll now=0,top=0,ss=0;
		For(i,1,cnt){
			if (a[i]==1)	q[++top]=v[++ss];
			else	ans[++now]=q[top--];
		}
		if (cmp(ans,answ))	memcpy(answ,ans,sizeof ans);
	}
	if (m)	a[++cnt]=-1,dfs(n,m-1),--cnt;
	if (n&&m<M)	a[++cnt]=1,dfs(n-1,m+1),--cnt;
}
int main(){
	freopen("sequence.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	M=min(n,read());
	For(i,1,n)	answ[i]=v[i]=read();
	dfs(n,0);
	For(i,1,n)	printf("%d ",answ[i]);
}

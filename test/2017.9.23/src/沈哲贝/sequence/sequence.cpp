#include<ctime>
#include<cstdio>
#include<memory.h>
#include<algorithm>
#include<ctime>
#include<map>
#define ll int
#define maxn 1200010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   putchar(' ');   }
struct data{	ll mn,id;	}tr[maxn];
ll a[maxn],q[maxn],top,n,m,now;
inline data merge(data a,data b){	return a.mn<=b.mn?a:b;	}
void build(ll l,ll r,ll p){
	if (l==r)	return tr[p].mn=a[l]=read(),tr[p].id=l,void(0);
	ll mid=(l+r)>>1;
	build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
	tr[p]=merge(tr[p<<1],tr[p<<1|1]);
}
data query(ll l,ll r,ll p,ll s,ll t){
	if (l==s&&r==t)	return tr[p];
	ll mid=(l+r)>>1;
	if (t<=mid)	return query(l,mid,p<<1,s,t);
	else if (s>mid)	return query(mid+1,r,p<<1|1,s,t);
	else return merge(query(l,mid,p<<1,s,mid),query(mid+1,r,p<<1|1,mid+1,t));
}
int main(){
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read();	m=min(n,read());
	build(1,n,1);
	now=1;
	For(i,1,n){
		if (now>n)	break;
		if (top==m)	writeln(a[q[top--]]);
		if (!top){
			ll x=query(1,n,1,now,min(n,now+m-1)).id;
			For(k,now,x-1)	q[++top]=k;
			now=x+1;
			writeln(a[x]);
		}else{
			ll x=query(1,n,1,now,min(n,now+m-top-1)).id;
			if (a[q[top]]<=a[x])	writeln(a[q[top--]]);
			else{
				For(k,now,x-1)	q[++top]=k;
				now=x+1;
				writeln(a[x]);
			}
		}
	}
	while(top)	writeln(a[q[top--]]);
}

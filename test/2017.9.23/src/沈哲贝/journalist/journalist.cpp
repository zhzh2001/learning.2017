#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll int
#define maxn 210
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
ll a[]={1,-1,0,0};
ll b[]={0,0,1,-1};
char s[maxn];
bool mp[maxn][maxn],ans[maxn][maxn],vis[maxn][maxn];
ll dis[maxn][maxn],sx,sy,ex,ey,q[maxn*maxn][2],n,m;
inline void spfa(ll sx,ll sy,ll ex,ll ey){
	memset(ans,0,sizeof ans);
	memset(vis,0,sizeof vis);
	for(ll x=ex,y=ey;!mp[x][y];x--,y--)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];x++,y--)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];x--,y++)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];x++,y++)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];x--)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];x++)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];y--)	ans[x][y]=1;
	for(ll x=ex,y=ey;!mp[x][y];y++)	ans[x][y]=1;
	ll he=0,ta=1;	q[1][0]=sx;	q[1][1]=sy;	dis[sx][sy]=0;
	while(he!=ta){
		ll x=q[++he][0],y=q[he][1];
		if (ans[x][y])	return writeln(dis[x][y]),void(0);
		For(i,0,3){
			ll ex=x+a[i],ey=y+b[i];
			if (!mp[ex][ey]&&!vis[ex][ey]){
				vis[ex][ey]=1;	dis[ex][ey]=dis[x][y]+1;
				q[++ta][0]=ex;	q[ta][1]=ey;
			}
		}
	}puts("Naive!");
}
int main(){
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	n=read();	m=read();
	For(i,0,n+1)	For(j,0,m+1)	mp[i][j]=1;
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m)	mp[i][j]=s[j]=='@'?1:0;
	}
	sx=read();	sy=read();	ex=read();	ey=read();
	while(sx)	spfa(sx,sy,ex,ey),sx=read(),sy=read(),ex=read(),ey=read();
}

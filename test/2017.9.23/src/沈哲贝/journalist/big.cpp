
/*
2
9
1 2 2 2 2 3 3 3 1
1
1#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 210
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
ll f[maxn][maxn],a[maxn],b[maxn],sz[maxn];
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	ll cas=read();
	For(ss,1,cas){
		memset(f,0,sizeof f);
		memset(a,0,sizeof a);
		memset(b,0,sizeof b);
		memset(sz,0,sizeof sz);
		printf("Case %d:",ss);
		ll n=read(),tot=0;
		For(i,1,n){
			a[i]=read();
			if (a[i]!=a[i-1])	b[++tot]=a[i],sz[tot]=1;
			else	sz[tot]++;
		}n=tot;
		For(i,1,n)	f[i][i]=sz[i]*sz[i];
		For(len,2,n)	For(i,1,n-len+1){
			ll j=i+len-1;
			f[i][j]=max(f[i+1][j]+sz[i]*sz[i],f[i][j-1]+sz[j]*sz[j]);
			if (b[i]==b[j]){
				ll sum=sz[i],ans=0,I=i+1;
				For(k,i+1,j)	if (b[k]==b[i])	sum+=sz[k],ans+=f[I][k-1],I=k+1;
				f[i][j]=max(f[i][j],ans+sum*sum);
			}
		}writeln(f[1][n]);
	}
}
/*
2
9
1 2 2 2 2 3 3 3 1
1
1
*/
*/

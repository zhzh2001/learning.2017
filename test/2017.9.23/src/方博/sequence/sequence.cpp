#include<bits/stdc++.h>
using namespace std;
int a[300005];
int s[300005];
int ans[300005];
struct xxx{
	int k,p;
}f[19][300005];
int n,m;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
xxx minx(xxx a,xxx b)
{
	if(a.k==b.k)return a;
	else if(a.k<b.k)return a;
	else return b;
}
xxx get(int l,int r)
{
	xxx ret;
	int k=log(r-l+1)/log(2);
	ret=minx(f[k][l],f[k][r-(1<<k)+1]);
	return ret;
}
int main()
{
	freopen("sequence.in","r",stdin);
	freopen("sequence.out","w",stdout);
	n=read();m=read();
	if(m>n)m=n;
	for(int i=1;i<=n;++i){
		a[i]=read();
		f[0][i].k=a[i];
		f[0][i].p=i;
	}
	//fa[n][0]=n;
	for(int i=1;i<=log(m)/log(2);++i)
		for(int j=1;j<=n;++j){
			f[i][j]=minx(f[i-1][j],f[i-1][min(j+(1<<i-1),n)]);
		}
/*	for(int i=0;i<=log(m)/log(2);i++){
		for(int j=1;j<=n;j++)
			cout<<f[j][i].k<<':'<<f[j][i].p<<'!'<<' ';
		cout<<endl;
	}*/
	int l=0;
	int in=0;
	int t=0;
	while(t+in<n){
		xxx k=get(l+1,min(n,l+m-in));
//		in++;
		if(k.k<s[in]||in==0){
			for(int i=l+1;i<=k.p;++i)
				s[++in]=a[i];
			l=k.p;
		}
		ans[++t]=s[in];
		in--;
	}
	while(in>0){
		ans[++t]=s[in];
		in--;
	}
	for(int i=1;i<=t;++i)
		printf("%d ",ans[i]);
	return 0;
}

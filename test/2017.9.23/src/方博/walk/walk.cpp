#include<bits/stdc++.h>
#define int long long
using namespace std;
const int N=100005;
struct xxx{
	int t,k,w,nxt;
}a[N];
int head;
int n,q;
inline int read()
{
   int x=0,f=1;
   char ch=getchar();
   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
   while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
   return x*f;
}
signed main()
{
	freopen("walk.in","r",stdin);
	freopen("walk.out","w",stdout);
	//cout<<sizeof(a)/1024/1024<<endl;
	n=read();
	q=read();
	head=0;
	a[0].nxt=-1;
	for(int i=1;i<=n;i++){
		a[i].k=read();
		a[i].w=read();
		if(a[i].w==2)a[i].w=-1,a[i].nxt=head,head=i;
		a[i].t=-1e15-1;
	}
	for(int i=1;i<=n;i++)
		if(a[i].w==-1){
			if(a[i].nxt==i-1)a[i].t=a[a[i].nxt].t;
			else {
				a[i].t=(a[i].k+a[i-1].k)/2;
				for(int j=a[i].nxt+1;j<i;j++)
					a[j].t=a[i].t;
			}
		}
	for(int i=1;i<=q;i++){
		int x,y;
		x=read();
		y=read();
		if(a[y].t==-1e15-1||abs(a[y].k-a[y].t)>x)printf("%lld\n",a[y].k+a[y].w*x);
		else printf("%lld\n",a[y].t);
	}
	return 0;
}

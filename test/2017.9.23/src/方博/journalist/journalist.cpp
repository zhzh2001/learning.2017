#include<bits/stdc++.h>
using namespace std;
const int N=205;
int vis[N][N];
int a[N][N];
int n,m;
int dx[4]={0,-1,0,1};
int dy[4]={-1,0,1,0};
int xx[8]={1,1,0,1,0,-1,-1,-1};
int yy[8]={-1,1,1,0,-1,0,1,-1};
int b[8];
int ans;
struct xxx{
	int x,y,t;
}q[N*N];
int sx,sy,ex,ey;
int bfs(int x,int y)
{
	int head=1;
	int tail=1;
	q[1].x=x;
	q[1].y=y;
	q[1].t=0;
	for(;head<=tail;head++){
		for(int i=0;i<4;i++){
			int tx=q[head].x+dx[i];
			int ty=q[head].y+dy[i];
			if(tx<1||ty<1||tx>n||ty>m)continue;
			if(a[tx][ty]==1)continue;
			if(vis[tx][ty]==2)return q[head].t+1;
			if(vis[tx][ty])continue;
//			cout<<tx<<' '<<ty
			vis[tx][ty]=1;
			tail++;
			q[tail].x=tx;
			q[tail].y=ty;
			q[tail].t=q[head].t+1;
		}
	}
	return -1;
}
int main()
{
	freopen("journalist.in","r",stdin);
	freopen("journalist.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		string s;
		cin>>s;
		for(int j=1;j<=m;j++)
			if(s[j-1]=='@')a[i][j]=1;
			else a[i][j]=0;
	}
	scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
	while(sx!=0&&sy!=0&&ex!=0&&ey!=0){
		memset(b,false,sizeof(b));
		memset(vis,0,sizeof(vis));
		vis[ex][ey]=2;
		for(int i=1;i<=200;i++)
			for(int j=0;j<8;j++){
				if(b[j])continue;
				int tx=ex+xx[j]*i;
				int ty=ey+yy[j]*i;
				if(a[tx][ty]==1){b[j]=true;continue;}
				if(tx<1||ty<1||tx>n||ty>m){b[j]=true;continue;}
				vis[tx][ty]=2;
			}
		if(vis[sx][sy]==2)ans=0;
		else ans=bfs(sx,sy);
		if(ans==-1)printf("Naive!\n");
		else printf("%d\n",ans);
		scanf("%d%d%d%d",&sx,&sy,&ex,&ey);
	}
}

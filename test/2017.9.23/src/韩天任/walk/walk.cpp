#include<bits/stdc++.h>
using namespace std;
long long f[100000],x[100000];
int l[100000];
long long n,m,t,y;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
int main(){
    freopen("walk.in","r",stdin);
    freopen("walk.out","w",stdout);
	n=read();
	m=read();
	bool temp=false;
	for (int i=1;i<=n;i++){
		x[i]=read();
		l[i]=read();
		f[0]=1e15+7;
		if (l[i]==2)
			if (temp==true) 
				f[i-1]=abs(x[i]-x[i-1]-1)/2+1,temp=false;
			else
				f[i-1]=f[i-2]+x[i]-x[i-1],temp=false;
		else
			if (temp==false)
				f[i-1]=-1,temp=true;
			else
				f[i-1]=0,temp=true;
	}
	for (int i=n;i>=1;i--){
		if (f[i]==0)
			f[i]=f[i+1]+x[i+1]-x[i];
	}
	f[n]=1e15+7;
	for (int i=1;i<=m;i++){
		t=read();
		y=read();
		if (l[y]==1)
			if (f[y]>t)
				cout<<x[y]+t<<endl;
			else
				cout<<x[y]+f[y]<<endl;
		else
			if (f[y-1]>t)
				cout<<x[y]-t<<endl;
			else
				cout<<x[y]-f[y-1]<<endl;
	}
} 
/*
6 3
-10 1
-6 2
-4 1
2 1
6 2
18 2
1 2
6 4
7 6
*/

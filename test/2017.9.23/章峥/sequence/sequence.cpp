#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("sequence.in");
ofstream fout("sequence.out");
const int N=300005,INF=0x3f3f3f3f;
int a[N],f[N];
bool vis[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
typedef pair<int,int> pii;
pii tree[1<<20];
void build(int id,int l,int r)
{
	if(l==r)
		tree[id]=make_pair(a[l],l);
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id]=min(tree[id*2],tree[id*2+1]);
	}
}
void erase(int id,int l,int r,int x)
{
	if(l==r)
		tree[id].first=INF;
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			erase(id*2,l,mid,x);
		else
			erase(id*2+1,mid+1,r,x);
		tree[id]=min(tree[id*2],tree[id*2+1]);
	}
}
pii query(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		return tree[id];
	int mid=(l+r)/2;
	if(R<=mid)
		return query(id*2,l,mid,L,R);
	if(L>mid)
		return query(id*2+1,mid+1,r,L,R);
	return min(query(id*2,l,mid,L,R),query(id*2+1,mid+1,r,L,R));
}
int main()
{
	int n,m;
	fin>>n>>m;
	m=min(m,n);
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		f[i]=i;
	}
	build(1,1,n);
	int j=1;
	for(int i=1;i<=n;i++,j=getf(j-1))
	{
		if(j==0)
			j++;
		int r=min(m+i-1,n);
		pii res=query(1,1,n,j,r);
		fout<<res.first<<' ';
		j=res.second;
		erase(1,1,n,j);
		f[j]=j-1;
	}
	fout<<endl;
	return 0;
}
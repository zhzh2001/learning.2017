#include<fstream>
#include<algorithm>
#include<set>
using namespace std;
ifstream fin("walk.in");
ofstream fout("walk.ans");
const int N=100005;
int x[N],f[N],ans[N];
bool block[N];
struct quest
{
	int t,p,id;
	bool operator<(const quest& rhs)const
	{
		return t<rhs.t;
	}
}Q[N];
int main()
{
	int n,q;
	fin>>n>>q;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>f[i];
	for(int i=1;i<=q;i++)
	{
		fin>>Q[i].t>>Q[i].p;
		Q[i].id=i;
	}
	sort(Q+1,Q+q+1);
	int j=1;
	for(int t=1;t<=Q[q].t;t++)
	{
		multiset<int> S;
		for(int i=1;i<=n;i++)
		{
			if(!block[i])
				if(f[i]==1)
					x[i]++;
				else
					x[i]--;
			S.insert(x[i]);
		}
		for(int i=1;i<=n;i++)
			block[i]=S.count(x[i])>1;
		for(;j<=q&&Q[j].t==t;j++)
			ans[Q[j].id]=x[Q[j].p];
	}
	for(int i=1;i<=q;i++)
		fout<<ans[i]<<endl;
	return 0;
}
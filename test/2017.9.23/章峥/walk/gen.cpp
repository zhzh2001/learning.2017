#include<fstream>
#include<random>
#include<ctime>
#include<set>
#include<algorithm>
using namespace std;
ofstream fout("walk.in");
const int n=1000,q=10000,t=10000;
pair<int,int> a[n+5];
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<q<<endl;
	set<int> S;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> dx(-1000,1000),df(1,2);
		a[i].first=dx(gen)*2;
		while(S.find(a[i].first)!=S.end())
			a[i].first=dx(gen)*2;
		S.insert(a[i].first);
		a[i].second=df(gen);
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		fout<<a[i].first<<' '<<a[i].second<<endl;
	for(int i=1;i<=q;i++)
	{
		uniform_int_distribution<> dt(1,t),dn(1,n);
		fout<<dt(gen)<<' '<<dn(gen)<<endl;
	}
	return 0;
}
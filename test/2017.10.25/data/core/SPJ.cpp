#include <cstdio>
#include <cstdlib>
using namespace std;
const int N=330;
typedef	long long ll;
int i,j,k,n,m,T;
int t[5];
int a[N][N];
FILE *Finn, *Fout, *Fstd, *Fres;
void Return(double p, char *s) {
	fprintf(Fres, "%.9lf\n%s\n", p, s);
	exit(0);
}
int ab(int x) {
	if (x<0) return -x;
	return x;
}
int max(int x,int y) {
	if (x<y) return y;
	return x;
}
int	main(int args, char** argv) {
	Finn = fopen(argv[1], "r");
	Fstd = fopen(argv[2], "r");
	Fout = fopen(argv[3], "r");
	Fres = fopen(argv[4], "w");
	fscanf(Finn,"%d",&n);
	fscanf(Fout,"%d",&m);
	for (i=1;i<=m;i++) {
		fscanf(Fout,"%d",&k);
		for (j=0;j<k;j++) fscanf(Fout,"%d",&t[j]);
		t[k]=t[0];
		for (j=0;j<k;j++) a[t[j]][t[j+1]]++,a[t[j+1]][t[j]]++;
	}
	for (i=1;i<=n;i++)
		for (j=i+1;j<=n;j++) if (a[i][j]!=2 || a[j][i]!=2) Return(0.0,"WA");
	Return(1.0,"AC");
}

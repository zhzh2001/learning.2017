#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
const int N=500000,H=500000,W=500000,T=4;
int i,j,k,n,h,w;
char s[4]={'L','R','U','D'};
int R() {
	return rand()<<15|rand();
}
int main() {
	freopen("edge.in","w",stdout);
	srand((int) time(0));
	n=N-R()%(N/10+1);
	h=H-R()%(H/10+1);
	w=W-R()%(W/10+1);
	printf("%d %d %d\n",n,h,w);
	for (i=1;i<=n;i++) putchar(s[rand()%T]);
	puts("");
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int N=500500,P=1000000007;
int i,j,k,n,m,h,w,x,y,ans,ch;
int a[N],b[4][2];
ll oo,u[N],d[N],l[N],r[N],A[N],B[N];
ll min(const ll &x,const ll &y) {
	if (x<y) return x;
	return y;
}
int main() {
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	b[0][0]=1;b[0][1]=0;
	b[1][0]=-1;b[1][1]=0;
	b[2][0]=0;b[2][1]=1;
	b[3][0]=0;b[3][1]=-1;
	scanf("%d%d%d",&n,&h,&w);
	for (i=1;i<=n;i++) {
		ch=getchar();
		while (ch<'A' || 'Z'<ch) ch=getchar();
		if (ch=='D') a[i]=0;
		if (ch=='U') a[i]=1;
		if (ch=='R') a[i]=2;
		if (ch=='L') a[i]=3;
	}
	memset(u,60,sizeof u);
	memset(d,60,sizeof d);
	memset(l,60,sizeof l);
	memset(r,60,sizeof r);
	oo=u[0];x=y=0;
	for (i=1;i<=n;i++) {
		if (a[i]==0) {
			if (x>=0) d[h-x]=min(d[h-x],i);
		}
		if (a[i]==1) {
			if (x<=0) u[-x+1]=min(u[-x+1],i);
		}
		if (a[i]==2) {
			if (y>=0) r[w-y]=min(r[w-y],i);
		}
		if (a[i]==3) {
			if (y<=0) l[-y+1]=min(l[-y+1],i);
		}
		x+=b[a[i]][0];
		y+=b[a[i]][1];
		if (x<=-h || h<=x || y<=-w || w<=y) break;
	}
	if (x) {
		if (x<0) {
			for (i=-x+1;i<=h;i++) u[i]=min(u[i],u[i+x]+n);
		}
		else {
			for (i=h-x;i;i--) d[i]=min(d[i],d[i+x]+n);
		}
	}
	if (y) {
		if (y<0) {
			for (i=-y+1;i<=w;i++) l[i]=min(l[i],l[i+y]+n);
		}
		else {
			for (i=w-y;i;i--) r[i]=min(r[i],r[i+y]+n);
		}
	}
	for (i=1;i<=h;i++) A[i]=min(u[i],d[i]);
	for (i=1;i<=w;i++) B[i]=min(l[i],r[i]);
	sort(A+1,A+h+1);
	sort(B+1,B+w+1);
	j=1;
	if (A[h]>=oo && B[w]>=oo) return puts("-1"),0;
	for (i=1;i<=h;i++) {
		while (j<=w && B[j]<A[i]) j++;
		ans=(A[i]%P*(w-j+1)%P+ans)%P;
	}
	j=1;
	for (i=1;i<=w;i++) {
		while (j<=h && A[j]<=B[i]) j++;
		ans=(B[i]%P*(h-j+1)%P+ans)%P;
	}
	printf("%d\n",ans);
}

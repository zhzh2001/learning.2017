#include<cstdio>
using namespace std;
const int N=100100;
typedef long long ll;
int i,j,k,n,m,En,ch,x,y;
int a[N],h[N];
ll ans,f[N],g[N];
struct edge { int s,n;} E[N<<1];
void R(int &x) {
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
}
void W(ll x) {
	if (x>=10) W(x/10);
	putchar(x%10+'0');
}
void E_add(int x,int y) {
	E[++En].s=y;E[En].n=h[x];h[x]=En;
	E[++En].s=x;E[En].n=h[y];h[y]=En;
}
void dfs1(int x,int F) {
	ll ma1=0,ma2=0;
	for (int k=h[x];k;k=E[k].n) if (E[k].s!=F) {
		dfs1(E[k].s,x);
		if (g[E[k].s]>ma1) {
			ma2=ma1;
			ma1=g[E[k].s];
		}
		else {
			if (g[E[k].s]>ma2) ma2=g[E[k].s];
		}
		if (f[E[k].s]>f[x]) f[x]=f[E[k].s];
	}
	if (ma1+ma2+a[x]>f[x]) f[x]=ma1+ma2+a[x];
	g[x]=ma1+a[x];
}
void dfs2(int x,int Fa,ll F,ll G) {
	if (f[x]+F>ans) ans=f[x]+F;
	ll g1=G,g2=0,g3=0,f1=F,f2=0,ma1,ma2,ff=0;
	for (int k=h[x];k;k=E[k].n) if (E[k].s!=Fa) {
		if (f[E[k].s]>f1) {
			f2=f1;
			f1=f[E[k].s];
		}
		else {
			if (f[E[k].s]>f2) f2=f[E[k].s];
		}
		if (g[E[k].s]>g1) {
			g3=g2;
			g2=g1;
			g1=g[E[k].s];
		}
		else {
			if (g[E[k].s]>g2) {
				g3=g2;
				g2=g[E[k].s];
			}
			else {
				if (g[E[k].s]>g3) g3=g[E[k].s];
			}
		}
	}
	for (int k=h[x];k;k=E[k].n) if (E[k].s!=Fa) {
		if (g1==g[E[k].s]) ma1=g2,ma2=g3;
		else {
			ma1=g1;
			if (g2==g[E[k].s]) ma2=g3;
			else ma2=g2;
		}
		if (f1==f[E[k].s]) ff=f2;
		else ff=f1;
		if (ma1+ma2+a[x]>ff) ff=ma1+ma2+a[x];
		dfs2(E[k].s,x,ff,ma1+a[x]);
	}
}
int main() {
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	R(n);
	for (i=1;i<=n;i++) R(a[i]);
	for (i=1;i<n;i++) {
		R(x);R(y);
		E_add(x,y);
	}
	dfs1(1,0);
	dfs2(1,0,0,0);
	W(ans);puts("");
}

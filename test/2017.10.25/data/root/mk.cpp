#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
const int N=100000,P=1000000000;
int i,j,k,n,m,x,y,v;
int f[N+10];
int getf(int x) {
	if (f[x]==x) return x;
	return f[x]=getf(f[x]);
}
int R() {
	return rand()<<15|rand();
}
int main() {
	freopen("root.in","w",stdout);
	srand((int) time(0));
	printf("%d\n",N);
	for (i=1;i<=N;i++) {
		printf("%d",R()%P+1);
		if (i<N) putchar(' ');
		else puts("");
	}
	for (i=1;i<=N;i++) f[i]=i;
	for (i=1;i<N;i++) {
		x=R()%N+1,y=R()%N+1;
		while (getf(x)==getf(y)) x=R()%N+1,y=R()%N+1;
		f[f[x]]=f[y];
		printf("%d %d\n",x,y);
	}
}

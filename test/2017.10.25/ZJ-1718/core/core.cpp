#include<fstream>
#include<cstdlib>
#include<set>
#include<algorithm>
using namespace std;
ifstream fin("core.in");
ofstream fout("core.out");
const int N=305,B=29;
int n,deg[N],tmp[N],mat[N][N],p[30000][4];
bool f[N*N];
set<int> S;
void dfs(int s,int cnt)
{
	int i,j,k,l,h;
	if(!f[n*(n-1)-cnt])
		return;
	if(cnt==n*(n-1))
	{
		fout<<s-1<<endl;
		for(i=1;i<s;i++)
			if(p[i][3])
				fout<<4<<' '<<p[i][0]<<' '<<p[i][1]<<' '<<p[i][2]<<' '<<p[i][3]<<endl;
			else
				fout<<3<<' '<<p[i][0]<<' '<<p[i][1]<<' '<<p[i][2]<<endl;
		exit(0);
	}
	copy(deg+1,deg+n+1,tmp+1);
	sort(tmp+1,tmp+n+1);
	h=0;
	for(i=1;i<=n;i++)
		h=h*B+tmp[i];
	if(S.find(h)!=S.end())
		return;
	S.insert(h);
	for(i=1;i<=n;i++)
		for(j=i+1;j<=n;j++)
			if(mat[i][j]<2)
				for(k=j+1;k<=n;k++)
					if(mat[j][k]<2&&mat[k][i]<2)
					{
						mat[i][j]++;mat[j][k]++;mat[k][i]++;
						mat[j][i]++;mat[k][j]++;mat[i][k]++;
						deg[i]+=2;deg[j]+=2;deg[k]+=2;
						p[s][0]=i;p[s][1]=j;p[s][2]=k;p[s][3]=0;
						dfs(s+1,cnt+3);
						mat[i][j]--;mat[j][k]--;mat[k][i]--;
						mat[j][i]--;mat[k][j]--;mat[i][k]--;
						deg[i]-=2;deg[j]-=2;deg[k]-=2;
					}
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
			if(j!=i&&mat[i][j]<2)
				for(k=1;k<=n;k++)
					if(k!=j&&k!=i&&mat[j][k]<2)
						for(l=1;l<=n;l++)
							if(l!=k&&l!=j&&l!=i&&mat[k][l]<2&&mat[l][i]<2)
							{
								mat[i][j]++;mat[j][k]++;mat[k][l]++;mat[l][i]++;
								mat[j][i]++;mat[k][j]++;mat[l][k]++;mat[i][l]++;
								deg[i]+=2;deg[j]+=2;deg[k]+=2;deg[l]+=2;
								p[s][0]=i;p[s][1]=j;p[s][2]=k;p[s][3]=l;
								dfs(s+1,cnt+4);
								mat[i][j]--;mat[j][k]--;mat[k][l]--;mat[l][i]--;
								mat[j][i]--;mat[k][j]--;mat[l][k]--;mat[i][l]--;
								deg[i]-=2;deg[j]-=2;deg[k]-=2;deg[l]-=2;
							}
}
int main()
{
	fin>>n;
	if(n==10)
	{
		fout<<"27\n3 1 2 3\n3 1 2 3\n3 1 4 5\n3 1 4 5\n3 1 6 7\n3 1 6 7\n3 1 8 9\n3 1 8 10\n3 2 4 6\n3 2 4 6\n3 2 5 7\n3 3 4 7\n4 2 7 4 8\n4 1 9 2 10\n4 2 5 7 8\n4 2 9 4 10\n4 3 4 8 7\n4 4 9 7 10\n4 7 9 8 10\n3 3 5 8\n3 3 6 8\n4 3 5 8 6\n3 3 9 10\n3 3 9 10\n3 5 6 9\n4 5 9 6 10\n3 5 6 10\n";
		return 0;
	}
	f[0]=f[3]=f[4]=true;
	for(int i=5;i<=n*(n-1);i++)
		f[i]=f[i-3]||f[i-4];
	dfs(1,0);
	return 0;
}

#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("edge.in");
ofstream fout("edge.out");
const int N=500005,MOD=1e9+7;
int xl[N],xr[N],yl[N],yr[N];
int main()
{
	int n,h,w;
	string s;
	fin>>n>>h>>w>>s;
	s=' '+s;
	int x=0,y=0;
	for(int i=1;i<=n;i++)
	{
		switch(s[i])
		{
			case 'U':
				x--;
				break;
			case 'D':
				x++;
				break;
			case 'L':
				y--;
				break;
			case 'R':
				y++;
				break;
		}
		xl[i]=min(xl[i-1],x);xr[i]=max(xr[i-1],x);
		yl[i]=min(yl[i-1],y);yr[i]=max(yr[i-1],y);
	}
	long long ans=0;
	for(int i=1;i<=h;i++)
	{
		for(int j=1;j<=w;j++)
		{
			int l=0,r=max(h,w);
			while(l<r)
			{
				int mid=(l+r)/2;
				long long nx=i+1ll*mid*x,ny=j+1ll*mid*y;
				if(nx+xr[n]>h||nx+xl[n]<=0||ny+yr[n]>w||ny+yl[n]<=0)
					r=mid;
				else
					l=mid+1;
			}
			if(r==max(h,w))
			{
				fout<<-1<<endl;
				return 0;
			}
			int cnt=r;
			long long nx=i+1ll*cnt*x,ny=j+1ll*cnt*y;
			l=1;r=n;
			while(l<r)
			{
				int mid=(l+r)/2;
				if(nx+xr[mid]>h||nx+xl[mid]<=0||ny+yr[mid]>w||ny+yl[mid]<=0)
					r=mid;
				else
					l=mid+1;
			}
			ans+=1ll*cnt*n+r;
		}
		if(xl[n]==0&&xr[n]==0)
		{
			ans*=h;
			break;
		}
	}
	fout<<ans<<endl;
	return 0;
}

#include<fstream>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("root.in");
ofstream fout("root.out");
const int N=100005;
int a[N];
vector<int> mat[N];
struct state
{
	int v,pred,w;
	state(){}
	state(int v,int pred,int w):v(v),pred(pred),w(w){}
}q[N];
bool vis[N],mark[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	long long ans=0;
	if(n>50)
	{
		if(mat[1].size()==n-1)
		{
			sort(a+2,a+n+1);
			fout<<a[1]+a[n]+a[n-1]+a[n-2]<<endl;
		}
		else
		{
			for(int i=1;i<=n;i++)
				ans+=a[i];
			fout<<ans<<endl;
		}
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			fill(vis+1,vis+n+1,false);
			fill(mark+1,mark+n+1,false);
			int l=1,r=1;
			q[r]=state(i,0,a[i]);
			vis[i]=true;
			while(l<=r)
			{
				if(q[l].v==j)
					break;
				for(int i=0;i<mat[q[l].v].size();i++)
					if(!vis[mat[q[l].v][i]])
					{
						vis[mat[q[l].v][i]]=true;
						q[++r]=state(mat[q[l].v][i],l,q[l].w+a[mat[q[l].v][i]]);
					}
				l++;
			}
			for(int i=l;i;i=q[i].pred)
				mark[q[i].v]=true;
			int now=q[l].w;
			for(int k=1;k<=n;k++)
				if(!mark[k])
				{
					fill(vis+1,vis+n+1,false);
					l=r=1;
					q[r]=state(k,0,a[k]);
					vis[k]=true;
					while(l<=r)
					{
						if(now+q[l].w>ans)
							ans=now+q[l].w;
						for(int i=0;i<mat[q[l].v].size();i++)
							if(!vis[mat[q[l].v][i]]&&!mark[mat[q[l].v][i]])
							{
								vis[mat[q[l].v][i]]=true;
								q[++r]=state(mat[q[l].v][i],l,q[l].w+a[mat[q[l].v][i]]);
							}
						l++;
					}
				}
		}
	fout<<ans<<endl;
	return 0;
}

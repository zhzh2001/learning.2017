#include<cstdio>
using namespace std;
const int maxn=500005;
int n,h,w,dx,dy,a[maxn],d[4][2];
char s[maxn];
inline void init(){
	scanf("%d%d%d",&n,&h,&w);
	scanf("%s",s+1);
	d[0][0]=-1; d[0][1]=0;
	d[1][0]=1; d[1][1]=0;
	d[2][0]=0; d[2][1]=-1;
	d[3][0]=0; d[3][1]=1;
	for (int i=1;i<=n;i++){
		if (s[i]=='U') a[i]=0;
		if (s[i]=='D') a[i]=1;
		if (s[i]=='L') a[i]=2;
		if (s[i]=='R') a[i]=3;
		dx+=d[a[i]][0]; dy+=d[a[i]][1];
	}
}
inline void work(){
	int ans=0;
	for (int i=1;i<=h;i++){
		for (int j=1;j<=w;j++){
			int id=1; int x=i,y=j;
			ans++; x+=d[a[id]][0]; y+=d[a[id]][1]; id=id%n+1;
			while (x>=1&&x<=h&&y>=1&&y<=w){
				ans++;
				x+=d[a[id]][0]; y+=d[a[id]][1];
				id=id%n+1;
			}
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	init();
	if (h>=1000&&w>=1000||dx==0&&dy==0) {
		puts("-1");
		return 0;
	}
	work();
	return 0;
}

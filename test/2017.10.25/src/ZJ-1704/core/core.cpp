#include<cstdio>
using namespace std;
const int maxn=205;
int flag,tot,n,matrix[maxn][maxn],ans[maxn][5];
void init(){
	scanf("%d",&n);
}
inline void output(int now){
	now--;
	for (int i=1;i<=now;i++){
		printf("%d ",ans[i][0]);
		for (int j=1;j<=ans[i][0];j++){
			printf("%d ",ans[i][j]);
		}
		puts("");
	}
}
inline bool judge(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (i==j) continue;
			if (matrix[i][j]!=2) return 0;
		}
	}
	return 1;
}
void dfs(int now){
	if (judge()){
		output(now);
		flag=1;
		return;
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (i!=j){
				for (int k=1;k<=n;k++){
					if (k!=i&&k!=j){
						ans[now][0]=3;
						if (matrix[i][j]<2&&matrix[j][k]<2&&matrix[i][k]<2){
							ans[now][1]=i;
							ans[now][2]=j;
							ans[now][3]=k;
							matrix[i][j]++; matrix[j][k]++; matrix[i][k]++;
							matrix[j][i]++; matrix[k][j]++; matrix[k][i]++;
							dfs(now+1);
							matrix[i][j]--; matrix[j][k]--; matrix[i][k]--;
							matrix[j][i]--; matrix[k][j]--; matrix[k][i]--;
						}
						if (flag) return;
					}
				}
			}
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (i!=j){
				for (int k=1;k<=n;k++){
					if (k!=i&&k!=j){
						for (int kk=1;kk<=n;kk++){
							if (kk!=i&&kk!=j&&kk!=k){
								if (matrix[i][j]<2&&matrix[j][k]<2&&matrix[i][k]<2&&matrix[i][kk]<2&&matrix[j][kk]<2&&matrix[k][kk]<2){
									ans[now][1]=i;
									ans[now][2]=j;
									ans[now][3]=k;
									ans[now][4]=kk;
									matrix[i][j]++; matrix[j][k]++; matrix[i][k]++; matrix[i][kk]++; matrix[j][kk]++; matrix[k][kk]++;
									matrix[j][i]++; matrix[k][j]++; matrix[k][i]++; matrix[kk][i]++; matrix[kk][j]++; matrix[kk][k]++;
									dfs(now+1);
									matrix[i][j]--; matrix[j][k]--; matrix[i][k]--; matrix[i][kk]--; matrix[j][kk]--; matrix[k][kk]--;
									matrix[j][i]--; matrix[k][j]--; matrix[k][i]--; matrix[kk][i]--; matrix[kk][j]--; matrix[kk][k]--;
								}
								if (flag) return;
							}
						}
					}
				}
			}
		}
	}
}
int main(){
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	init();
	dfs(1);
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=1e5+5;
struct edge{
	int link,next;
}e[maxn<<1];
int tot,n,head[maxn];
ll ans,dp[maxn][4],g[maxn],val[maxn];
inline void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add(u,v); add(v,u);
}
inline void init(){
	n=read();
	for (int i=1;i<=n;i++) val[i]=read();
	for (int i=1;i<n;i++){
		insert(read(),read());
	}
}
void dfs(int u,int fa){
	int k0=0,k1=0,temp0=0,temp1=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			if (dp[u][0]<dp[v][0]){
				temp1=temp0;
				temp0=v;
				dp[u][1]=dp[u][0];
				dp[u][0]=dp[v][0];
			}else{
				if (dp[u][1]<dp[v][0]){
					temp1=v;
					dp[u][1]=dp[v][0];
				}
			}
			if (dp[v][2]>dp[k0][2]){
				k1=k0;
				k0=v;
				dp[u][2]=dp[v][2];
			}else{
				if (dp[v][2]>dp[k1][2]){
					k1=v;
				}
			}
			dp[u][3]=max(dp[u][3],dp[v][3]);
		}
	}
	dp[u][3]=max(dp[k0][2]+dp[k1][2],dp[u][3]);
	dp[u][2]=max(dp[u][2],val[u]+dp[u][0]+dp[u][1]);
	dp[u][0]+=val[u];
	if (k0!=temp0){
		dp[u][3]=max(dp[u][3],val[u]+dp[k0][2]+dp[temp0][0]);
	}
	if (k0!=temp1){
		dp[u][3]=max(dp[u][3],val[u]+dp[k0][2]+dp[temp1][0]);
	}
	if (k1!=temp0){
		dp[u][3]=max(dp[u][3],val[u]+dp[k1][2]+dp[temp0][0]);
	}
	if (k1!=temp1){
		dp[u][3]=max(dp[u][3],val[u]+dp[k1][2]+dp[temp1][0]);
	}
}
void dfs2(int u,int fa,ll sum){
	g[u]=dp[u][3];
	int temp0=0,temp1=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			if (dp[u][0]==dp[v][0]+val[u]){
				dfs2(v,u,max(sum,dp[u][1]));
			}else {
				dfs2(v,u,max(sum,dp[u][0]));
			}
			if (dp[temp0][0]<dp[v][0]){
				temp1=temp0;
				temp0=v;
			}else{
				if (dp[temp1][0]<dp[v][0]){
					temp1=v;
				}
			}
		}
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			if (temp0!=v&&temp1!=v){
				g[u]=max(dp[v][2]+dp[temp0][0]+dp[temp1][0]+val[u],g[u]);
			}
			if (temp0!=v) g[u]=max(g[u],dp[v][2]+sum+dp[temp0][0]);
			if (temp1!=v) g[u]=max(g[u],dp[v][2]+sum+dp[temp1][0]);
		}
	}
	ans=max(ans,g[u]);
}
inline void solve(){
	dfs(1,0);
	dfs2(1,0,0);
	printf("%lld\n",ans);
}
int main(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	init();
	solve();
	return 0;
}

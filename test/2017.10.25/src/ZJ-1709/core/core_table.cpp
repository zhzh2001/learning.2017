#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[202][202];
struct op{
	int i,j,k,t;
}stk[10000001];
int cur=0;
int n;
void dfs(int u){
	//printf("on %d\n",u);
	if (u==n){
		for(int i=1;i<=cur;i++)
			if (stk[i].t)
				printf("4 %d %d %d %d\n",stk[i].i,stk[i].j,stk[i].k,stk[i].t);
			else printf("3 %d %d %d\n",stk[i].i,stk[i].j,stk[i].k);
		exit(0);
	}
	int cnt=0;
	for(int i=u+1;i<=n;i++){
		if (!a[u][i])
			continue;
		if (a[u][i]==2)
			cnt+=4;
		else cnt++;
	}
	if (cnt%2)
		return;
	if (!cnt){
		dfs(u+1);
		return;
	}
	int to=cnt==2?u+1:u;
	for(int i=u+1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			if (a[u][i] && a[i][j] && a[u][j]){
				a[u][i]--;
				a[i][j]--;
				a[u][j]--;
				stk[++cur]=(op){u,i,j};
				dfs(to);
				a[u][i]++;
				a[i][j]++;
				a[u][j]++;
				cur--;
			}
		}
	for(int i=u+1;i<=n;i++) //(u,i,j,k)==(u,k,j,i)
		for(int j=u+1;j<=n;j++)
			for(int k=i+1;k<=n;k++)
				if (a[u][i] && a[min(i,j)][max(i,j)] && a[min(j,k)][max(j,k)] && a[u][k]){
					a[u][i]--;
					a[min(i,j)][max(i,j)]--;
					a[min(j,k)][max(j,k)]--;
					a[u][k]--;
					stk[++cur]=(op){u,i,j,k};
					dfs(to);
					a[u][i]++;
					a[min(i,j)][max(i,j)]++;
					a[min(j,k)][max(j,k)]++;
					a[u][k]++;
					cur--;
				}

}
int main(){
	init();
	n=readint();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			a[i][j]=2;
	dfs(1);
}
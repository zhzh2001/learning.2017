#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int main(){
	init();
	int n=readint();
	if (n==3){
		puts("2");
		puts("3 1 2 3");
		puts("3 1 2 3");
		return 0;
	}
	if (n==4){
		puts("4");
		puts("3 1 2 3");
		puts("3 2 3 4");
		puts("3 1 2 4");
		puts("3 1 3 4");
		return 0;
	}
	if (n==7){
		puts("14");
		puts("3 1 2 3");
		puts("3 1 2 3");
		puts("3 1 4 5");
		puts("3 1 4 5");
		puts("3 1 6 7");
		puts("3 1 6 7");
		puts("3 2 4 6");
		puts("3 2 4 6");
		puts("3 2 5 7");
		puts("3 2 5 7");
		puts("3 3 4 7");
		puts("3 3 4 7");
		puts("3 3 5 6");
		puts("3 3 5 6");
		return 0;
	}
	if (n==10){
		puts("28");
		puts("3 1 2 3");
		puts("3 1 2 3");
		puts("3 1 4 5");
		puts("3 1 4 5");
		puts("3 1 6 7");
		puts("3 1 6 7");
		puts("3 1 8 9");
		puts("3 1 8 10");
		puts("3 1 9 10");
		puts("3 2 4 6");
		puts("3 2 4 6");
		puts("3 2 5 7");
		puts("3 2 5 7");
		puts("3 2 8 9");
		puts("3 2 8 10");
		puts("3 2 9 10");
		puts("3 3 4 7");
		puts("3 3 4 7");
		puts("3 3 5 6");
		puts("3 3 5 8");
		puts("3 3 6 8");
		puts("4 3 9 4 10");
		puts("4 3 9 5 10");
		puts("4 4 8 5 9");
		puts("4 4 8 7 10");
		puts("3 5 6 10");
		puts("4 6 8 7 9");
		puts("4 6 9 7 10");
		return 0;
	}
	if (n==15){
		puts("70");
		puts("3 1 2 3");
puts("3 1 2 3");
puts("3 1 4 5");
puts("3 1 4 5");
puts("3 1 6 7");
puts("3 1 6 7");
puts("3 1 8 9");
puts("3 1 8 9");
puts("3 1 10 11");
puts("3 1 10 11");
puts("3 1 12 13");
puts("3 1 12 13");
puts("3 1 14 15");
puts("3 1 14 15");
puts("3 2 4 6");
puts("3 2 4 6");
puts("3 2 5 7");
puts("3 2 5 7");
puts("3 2 8 10");
puts("3 2 8 10");
puts("3 2 9 11");
puts("3 2 9 11");
puts("3 2 12 14");
puts("3 2 12 14");
puts("3 2 13 15");
puts("3 2 13 15");
puts("3 3 4 7");
puts("3 3 4 7");
puts("3 3 5 6");
puts("3 3 5 6");
puts("3 3 8 11");
puts("3 3 8 11");
puts("3 3 9 10");
puts("3 3 9 10");
puts("3 3 12 15");
puts("3 3 12 15");
puts("3 3 13 14");
puts("3 3 13 14");
puts("3 4 8 12");
puts("3 4 8 12");
puts("3 4 9 13");
puts("3 4 9 13");
puts("3 4 10 14");
puts("3 4 10 14");
puts("3 4 11 15");
puts("3 4 11 15");
puts("3 5 8 13");
puts("3 5 8 13");
puts("3 5 9 12");
puts("3 5 9 12");
puts("3 5 10 15");
puts("3 5 10 15");
puts("3 5 11 14");
puts("3 5 11 14");
puts("3 6 8 14");
puts("3 6 8 14");
puts("3 6 9 15");
puts("3 6 9 15");
puts("3 6 10 12");
puts("3 6 10 12");
puts("3 6 11 13");
puts("3 6 11 13");
puts("3 7 8 15");
puts("3 7 8 15");
puts("3 7 9 14");
puts("3 7 9 14");
puts("3 7 10 13");
puts("3 7 10 13");
puts("3 7 11 12");
puts("3 7 11 12");
return 0;
	}
}
/*
3 1 2 3
3 1 2 3
3 1 4 5
3 1 4 6
3 1 5 6
3 2 4 5
4 2 4 3 6
4 2 5 3 6
4 3 4 6 5
*/
/*
3 1 2 3
3 1 2 3
3 1 4 5
3 1 4 5
3 1 6 7
3 1 6 7
3 2 4 6
3 2 4 6
3 2 5 7
3 2 5 7
3 3 4 7
3 3 4 7
3 3 5 6
3 3 5 6
*/
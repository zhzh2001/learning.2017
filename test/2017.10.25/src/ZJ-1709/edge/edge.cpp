#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
char s[500003];
int px[500003],py[500003],nx[500003],ny[500003];
int qwq[5000003];
int *dp[5000003];
int main(){
	init();
	int n=readint(),h=readint(),w=readint();
	readstr(s);
	bool flag=true;
	for(int i=0;i<n;i++)
		if (s[i]!='L' && s[i]!='R'){
			flag=false;
			break;
		}
	int fact=1;
	if (flag){
		fact=h;
		h=1;
	}
	for(int i=0;i<h;i++)
		dp[i]=qwq+i*w;
	int dx=0,dy=0;
	for(int i=0;i<n;i++){
		if (s[i]=='U')
			dx++;
		else if (s[i]=='D')
			dx--;
		else if (s[i]=='L')
			dy--;
		else dy++;
	}
	if (dx<0){
		dx=-dx;
		for(int i=0;i<n;i++){
			if (s[i]=='U')
				s[i]='D';
			else if (s[i]=='D')
				s[i]='U';
		}
	}
	if (dy<0){
		dy=-dy;
		for(int i=0;i<n;i++){
			if (s[i]=='L')
				s[i]='R';
			else if (s[i]=='R')
				s[i]='L';
		}
	}
	int x=0,y=0;
	memset(px,0x3f,sizeof(px));
	memset(py,0x3f,sizeof(py));
	memset(nx,0x3f,sizeof(nx));
	memset(ny,0x3f,sizeof(ny));
	for(int i=0;i<n;i++){
		if (s[i]=='U')
			x++;
		else if (s[i]=='D')
			x--;
		else if (s[i]=='L')
			y--;
		else y++;
		if (x>0 && px[x]==INF)
			px[x]=i;
		if (x<0 && nx[-x]==INF)
			nx[-x]=i;
		if (y>0 && py[y]==INF)
			py[y]=i;
		if (y<0 && ny[-y]==INF)
			ny[-y]=i;
	}
	//assert(x==dx && y==dy);
	//puts("WTF");
	//printf("%d\n",px[1]);
	ll ans=0;
	for(int i=h-1;i>=0;i--){
		int *f=dp[i],*g=0;
		if (i+dx<h)
			g=dp[i+dx];
		for(int j=w-1;j>=0;j--){
			int qwq=min(min(px[h-i],nx[i+1]),min(py[w-j],ny[j+1]));
			//printf("%d %d %d %d\n",i,j,px[h-i],qwq);
			if (qwq<INF)
				f[j]=qwq+1;
			else {
				if (!dx && !dy){
					puts("-1");
					return 0;
				}
				f[j]=n+g[j+dy];
				if (f[j]>=mod)
					f[j]-=mod;
			}
			ans+=f[j];
		}
		//printf("%lld\n",f[0]);
	}
	printf("%lld",(ans%mod*fact)%mod);
}	
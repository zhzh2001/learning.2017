#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
const int maxm=200002;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int a[maxn];
	bool vis[maxn];
	ll d[maxn],f[maxn],g[maxn],h[maxn];
	void dfs(int u){
		f[u]=g[u]=a[u];
		vis[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (vis[v])
				continue;
			d[v]=d[u]+a[v];
			dfs(v);
			g[u]=max(f[v]+f[u],g[u]);
			f[u]=max(f[u],f[v]+a[u]);
			h[u]=max(h[u],h[v]);
		}
		h[u]=max(h[u],g[u]);
	}
	ll ans;
	void dfs2(int u,ll mmax){
		ans=max(ans,mmax+d[u]);
		vis[u]=1;
		ll mx=0,scmx=0;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (vis[v])
				continue;
			if (h[v]>mx){
				scmx=mx;
				mx=h[v];
			}else if (h[v]>scmx)
				scmx=h[v];
		}
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (vis[v])
				continue;
			dfs2(v,max(mmax,h[v]==mx?scmx:mx));
		}
	}
	ll work(){
		//puts("WTF");
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++)
				d[j]=f[j]=g[j]=h[j]=vis[j]=0;
			//printf("on %d\n",i);
			d[i]=a[i];
			dfs(i);
			for(int j=1;j<=n;j++)
				vis[j]=0;
			//puts("after dfs");
			dfs2(i,0);
			//printf("QAQ %lld\n",ans);
		}
		return ans;
	}
	ll work1(){
		ll sum=0;
		for(int i=1;i<=n;i++)
			sum+=a[i];
		return sum;
	}
	ll work2(){
		sort(a+2,a+n+1,greater<int>());
		return (ll)a[1]+a[2]+a[3]+a[4];
	}
}g;
int main(){
	init();
	int n=g.n=readint();
	bool flag1=true,flag2=true;
	for(int i=1;i<=n;i++)
		g.a[i]=readint();
	for(int i=1;i<n;i++){
		int u=readint(),v=readint();
		g.addedge(u,v);
		g.addedge(v,u);
		if (v!=u+1 && u!=v+1)
			flag1=false;
		if (u!=1)
			flag2=false;
	}
	if (flag1)
		return printf("%lld",g.work1()),0;
	if (flag2)
		return printf("%lld",g.work2()),0;
	printf("%lld",g.work());
}
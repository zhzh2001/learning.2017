#include<iostream>
#include<cstdio>

#define ll long long
#define eps 1e-8
#define inf 1e9

#define For(i,j,k)	for(register ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(register ll i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
ll	n,x,y,r[500001],l[500001],up[500001],dw[500001];
ll q[500001],v[500001],top;
char s[500001];
ll mo=1e9+7;
int main()
{
	freopen("edge.in","r",stdin);freopen("edge.out","w",stdout);
	n=read();x=read();y=read();
	scanf("\n%s",s+1);
	up[0]=dw[0]=l[0]=r[0]=0;
	For(i,1,n)	
	{
		r[i]=r[i-1];l[i]=l[i-1];up[i]=up[i-1];dw[i]=dw[i-1];
		if(s[i]=='R')	r[i]++,l[i]--;
		if(s[i]=='L')	r[i]--,l[i]++;
		if(s[i]=='U')	up[i]++,dw[i]--;
		if(s[i]=='D')	dw[i]++,up[i]--;
	}
	ll tl=0,tr=0,tu=0,td=0;
	For(i,1,n)
	{
		if(r[i]>tr)	q[++top]=1,v[top]=i,tr++;
		if(l[i]>tl)	q[++top]=2,v[top]=i,tl++;
		if(up[i]>tu)	q[++top]=3,v[top]=i,tu++;
		if(dw[i]>td)	q[++top]=4,v[top]=i,td++;
	}
	//1->R 2->L 3->U 4->D
	ll ans=0,siz=0;
	ll lx=1,rx=x,ly=1,ry=y,tim=0,vlx=1,vrx=x,vly=1,vry=y;
	while(1)
	{
		ll qlx=lx,qrx=rx,qly=ly,qry=ry;
		For(i,1,top)
		{
			if(q[i]==3)	
			{
				qlx++;
				if(qlx<=vlx)	continue;
				vlx=max(vlx,qlx);
				ans+=(tim+v[i])*(vry-vly+1);ans%=mo;
				siz+=vry-vly+1;
			}
			if(q[i]==4)	
			{
				qrx--;
				if(qrx>=vrx)	continue;
				vrx=min(vrx,qrx);
				ans+=(tim+v[i])*(vry-vly+1);ans%=mo;
				siz+=vry-vly+1;
			}
			if(q[i]==1)	
			{
				qry--;
				if(qry>=vry)	continue;
				vry=min(vry,qry);
				ans+=(tim+v[i])*(vrx-vlx+1);ans%=mo;
				siz+=vrx-vlx+1;
			}
			if(q[i]==2)	
			{
				qly++;
				if(qly<=vly)	continue;
				vly=max(vly,qly);
				ans+=(tim+v[i])*(vrx-vlx+1);ans%=mo;
				siz+=vrx-vlx+1;
			}
			if(siz==x*y)	{printf("%I64d\n",ans);return 0;}
		}
		tim+=n;tim%=mo;
		lx=max(1LL,lx+up[n]);rx=min(rx-dw[n],x);ly=max(1LL,ly+l[n]);ry=min(ry-r[n],y);
		if(r[n]==0&&l[n]==0&&up[n]==0&&dw[n]==0)	{puts("-1");return 0;}
	}
}
/*
3 4 6
RUL
*/

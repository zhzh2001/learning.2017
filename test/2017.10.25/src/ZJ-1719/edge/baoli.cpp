#include<iostream>
#include<cstdio>

#define ll long long
#define eps 1e-8
#define inf 1e9

#define For(i,j,k)	for(register ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(register ll i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,x,y,ans;
char s[20001];
inline int doit(int tx,int ty)
{
	int tep=1,step=0;
	while(1)
	{
		if(s[tep]=='L')	ty--;
		if(s[tep]=='R')	ty++;
		if(s[tep]=='U')	tx--;
		if(s[tep]=='D')	tx++;
		step++;
		if(step>=10000)	{puts("-1");exit(0);}
		if(tx<1||ty<1||tx>x||ty>y)	return step;
		tep++;if(tep==n+1)tep=1;
	}
}
int mo=1e9+7;
int main()
{
	freopen("edge.in","r",stdin);freopen("edge1.out","w",stdout);
	n=read();x=read();y=read();
	scanf("\n%s",s+1);
	For(i,1,x)
		For(j,1,y)
			ans+=doit(i,j),ans%=mo;
	printf("%d\n",ans);
}

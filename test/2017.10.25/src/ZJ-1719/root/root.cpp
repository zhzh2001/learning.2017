#include<iostream>
#include<cstdio>

#define ll long long
#define eps 1e-8
#define inf 1e9

#define For(i,j,k)	for(register ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(register ll i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
ll n,a[200001],f[200001][4],ans[200001];
ll poi[400001],nxt[400001],head[400001],cnt;
inline void add(ll x,ll y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void dfs(ll x,ll fa)
{
	f[x][1]=f[x][2]=f[x][3]=a[x];
	for(ll i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dfs(poi[i],x);
		if(f[poi[i]][1]+a[x]>f[x][1])	f[x][3]=f[x][2],f[x][2]=f[x][1],f[x][1]=f[poi[i]][1]+a[x];
			else	if(f[poi[i]][1]+a[x]>f[x][2])	f[x][3]=f[x][2],f[x][2]=f[poi[i]][1]+a[x];
				else	if(f[poi[i]][1]+a[x]>f[x][3])	f[x][3]=f[poi[i]][1]+a[x];
		ans[x]=max(ans[x],f[poi[i]][1]+a[x]);
	}
	ans[x]=max(ans[x],f[x][1]+f[x][2]-a[x]);
}
ll ANS=0;
inline void get(ll x,ll fa,ll v)
{
	if(x!=1)
	{	
		if(fa==1&&f[x][1]+a[fa]==f[fa][1])	v=0;
		ll tans=v+f[fa][2]-a[fa];
		if(f[x][1]+a[fa]!=f[fa][1])
		{
			tans=max(tans,v+f[fa][1]-a[fa]);
			if(f[x][1]+a[fa]!=f[fa][2])	tans=max(tans,f[fa][1]+f[fa][2]-a[fa]);
				else	tans=max(tans,f[fa][1]+f[fa][3]-a[fa]);
		}
		else
		{
			tans=max(tans,f[fa][2]+f[fa][3]-a[fa]);
			tans=max(tans,f[fa][2]+v-a[fa]);
		}
		ANS=max(ANS,tans+ans[x]);
	}
	for(ll i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		if(f[x][1]==f[poi[i]][1]+a[x])	get(poi[i],x,max(f[x][2],v+a[x]));
		else	get(poi[i],x,max(f[x][1],v+a[x]));
	}
}
ll x,y;
int main()
{
	freopen("root.in","r",stdin);freopen("root.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();
	For(i,1,n-1)	x=read(),y=read(),add(x,y),add(y,x);
	dfs(1,1);
	for(ll i=head[1];i;i=nxt[i])	if(f[poi[i]][1]+a[1]==f[1][1])	get(poi[i],1,f[1][2]);	else get(poi[i],1,f[1][1]);
	printf("%I64d\n",ANS);
}
/*
6
1 1 1 1 1 1
1 2
1 3 
1 6
2 4 
2 5
*/

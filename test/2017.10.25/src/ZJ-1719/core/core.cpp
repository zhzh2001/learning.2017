#include<iostream>
#include<cstdio>

#define ll long long
#define eps 1e-8
#define inf 1e9

#define For(i,j,k)	for(register ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(register ll i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n;
int main()
{
	freopen("core.in","r",stdin);freopen("core.out","w",stdout);
	n=read();
	if(n==3)
	{
		printf("2\n3 1 2 3\n3 1 2 3");
	}
	if(n==4)
	{
		printf("4\n3 1 2 3\n3 1 2 4\n3 2 3 4\n3 1 3 4");
	}
}

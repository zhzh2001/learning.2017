#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;

const int N = 1e5+11,mod = 1e9+7,inf = 2e9 ; 
int n,m,len,sum,ii,jj,kk,l1,l2,r1,r2 ; 
int f[111][111][111] ; 
LL ans ; 
char s[N] ; 

int dfs(int i,int j,int k) {
	if(f[i][j][k]) return f[i][j][k] ; 
	int ii=i,jj=j,kk=k;
	if(s[k]=='L') jj-- ; 
	else if(s[k]=='R') jj++ ; 
	else if(s[k]=='U') ii-- ; 
	else if(s[k]=='D') ii++ ; 
	if(ii<1||ii>n||jj<1||jj>m) return f[i][j][k]=1 ; 
	k++ ; if(k==len) k=0 ; 
	return f[i][j][k] = dfs(ii,jj,kk)+1 ; 
}

int main() {
	freopen("edge.in","r",stdin) ; 
	freopen("edge.out","w",stdout) ; 
	scanf("%d%d%d",&len,&n,&m) ; 
	scanf("%s",s) ; 
	bool f1 = 0 ; 
	For(i,0,len-1) if(s[i]!='L') f1 = 1 ; 
	if(!f1) {
		ans = 1ll*n*(m+1)*m/2 % mod; 
		printf("%lld\n",ans) ; 
		return 0 ; 
	} 
	l1 = r1 = inf ; l2 = r2 = -inf ; 
	ii = 0 ; jj = 0 ; 
	For(k,0,len-1) {
		if(s[k]=='L') jj-- ; 
		else if(s[k]=='R') jj++ ; 
		else if(s[k]=='U') ii-- ; 
		else if(s[k]=='D') ii++ ; 	
		if(ii<l1) l1 = ii ; 
		if(ii>l2) l2 = ii ;
		if(jj<r1) r1 = jj ; 
		if(jj>r2) r2 = jj ; 
	}
	if(ii==0&&jj==0) {
		if(l1+l2+1<=n&&r1+r2+1<=m) {
			printf("-1\n") ; 
			return 0 ; 
		} 
	}
	Dow(i,n,1) 
	  For(j,1,m) sum+=dfs(i,j,0) ; 
	printf("%d\n",sum) ; 
	return 0 ; 
}


/*


1 10 2
R

3 4 6
RUL

*/


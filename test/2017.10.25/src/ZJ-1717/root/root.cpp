#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
#define int long long 
using namespace std;

const int N = 200011 ;   //
struct node{
	int to,pre ; 
}e[N*2];
int f[N],dd[N],dS[N],dT[N],vis[N],head[N],val[N] ; 
int n,S,T,root,cnt,first,second,Mx,Ans ; 

inline int read() {
	int x = 0,f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x *10+ch-48 ; ch = getchar(); }
	return x * f ;
}

inline void add(int x,int y) {
	e[++cnt].to = y ; 
	e[cnt].pre = head[x] ; 
	head[x]=cnt ; 
}

inline void calc(int u) {
	bool flag = 0 ; 
	for(int i=head[u];i;i=e[i].pre) {
		int v = e[i].to ; 
		if(v!=f[u]) {
			dd[v] = dd[u]+val[v] ; 
			flag = 1 ; 
			calc(v) ; 
		}  
	}
	if(!flag)  
		if(dd[u] > first) {
			second = first ; first = dd[u] ; 
		} 
		else if(dd[u]>second) 
			second = dd[u] ; 	
}

inline void dfs(int u,int fa,int *d) { 
	if(root==S) f[u] = fa ; 
	for(int i=head[u];i;i=e[i].pre) {
		int v = e[i].to ; 
		if(v!=fa) {
			d[v] = d[u]+val[v] ;  
			dfs(v,u,d) ; 
		}
	}
}

signed main() {
	freopen("root.in","r",stdin) ; 
	freopen("root.out","w",stdout) ; 
	n = read() ; 
	LL sum = 0 ; 
	For(i,1,n) val[i]=read() ; 
	For(i,1,n) sum=sum+val[i] ; 
	bool ft = 0,fk = 0 ; 
	For(i,2,n) {
		int x,y ; 
		x=read() ; y = read() ; 
		add(x,y) ; add(y,x) ; 
		if(abs(x-y)!=1) ft = 1 ; 
		if(x!=1) fk = 1 ; 
	} 
	if(!ft) {
		printf("%lld\n",sum) ; 
		return 0 ; 
	}
	if(!fk) {
		if( n<=4 ) {
			printf("%lld\n",sum) ; 
			return 0 ; 
		}
		sort(val+2,val+n+1) ; 
		Ans = val[1]+val[n]+val[n-1]+val[n-2] ; 
		printf("%lld\n",Ans) ; 
		return 0 ; 
	}
	
	dd[1] = val[1] ; 
	dfs(1,-1,dd) ; 
	dd[0] = -1 ; S = 0 ; 
	For(i,1,n) if(dd[i]>dd[S]) S = i ; 
	
	root = S ; 
	dS[S] = val[S] ;  
	dfs(S,-1,dS) ; 
	dS[0] = -1 ; T = 0 ; 
	For(i,1,n) if(dS[i]>dS[T]) T = i ; 
	Ans+=dS[T] ; 
	
	root = T ; 
	dT[T] = val[T] ; 
	dfs(T,-1,dT) ; 
	For(i,1,n) 
		if(dS[i]+dT[i]==Ans+val[i]) vis[i] = 1 ; 
	
	 
	Mx = 0 ; 
	For(i,1,n) {
		first = 0 ; second = 0 ; 
		if(!vis[i]&&vis[f[i]]) {
			dd[i] = val[i] ; 
			calc(i) ; 
			int tmp = first+second ; 
			if(second!=0) tmp-=val[i] ; 
			if(tmp>Mx) Mx = tmp ; 
		}
	}
	Ans+=Mx ; 
	printf("%d\n",Ans) ; 
	return 0 ; 
}




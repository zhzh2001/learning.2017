#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 100005
#define ll long long
using namespace std;
ll read()
{ll t=0;char c;
   c=getchar();
   while(!(c>='0' && c<='9')) c=getchar();
   while(c>='0' && c<='9')
     {
     	t=t*10+c-48;
     	c=getchar();
	 }
   return t;
}
ll n;
ll pre[N],dis[N];
ll front[N],to[2*N],next[2*N];
ll a[N],l,r,sum[N],ss[N];
bool boo[N];
ll line,x,y,large;
ll s1[N],s2[N],num1,num2;
ll lsum[N],rsum[N],lmax[N],rmax[N];
void insert(ll x,ll y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
	line++;to[line]=x;next[line]=front[y];front[y]=line;
}
void dfs(ll x)
{ll i,MAX=0,sec=0,son1=x,son2=x;
	ss[x]=x;sum[x]=0;
	for(i=front[x];i!=-1;i=next[i])
	  if(pre[x]!=to[i])
	    {
	    	pre[to[i]]=x;
			dis[to[i]]=dis[x]+1;
			dfs(to[i]);
	    	if(sum[to[i]]>MAX)
	    	  {
	    	  	sec=MAX;son2=son1;son1=ss[to[i]];MAX=sum[to[i]];
			  }
			  else
			   if(sum[to[i]]>sec)
			      {
			      	sec=sum[to[i]];son2=ss[to[i]];
				  }
		}
	if(MAX+sec+a[x]>large)
	  {
	  	l=son1;r=son2;large=MAX+sec+a[x];
	  }
	ss[x]=son1;sum[x]=MAX+a[x];
}
ll dfs2(ll x)
{ll i;
	for(i=front[x];i!=-1;i=next[i])
	  if(pre[x]!=to[i])
	    {
	    	pre[to[i]]=x;dfs2(to[i]);
	    	if(boo[to[i]]) sum[x]=max(sum[x],sum[to[i]]);
		}
	if(boo[x]) sum[x]+=a[x];
}
int main()
{int i,j,k;
     freopen("root.in","r",stdin);
     freopen("root.out","w",stdout);
     n=read();
     for(i=1;i<=n;i++) a[i]=read();
     line=-1;memset(front,-1,sizeof(front));
     large=0;
     for(i=1;i<n;i++)
       {
       	x=read();y=read();
       	insert(x,y);
	   }
	 dis[1]=1;
	 dfs(1);
	 x=l;y=r;
	 memset(boo,true,sizeof(boo));
	 while(x!=y)
	   {
	   	if(dis[x]>=dis[y])
	   	  {
	   	    s1[++num1]=x;boo[x]=false;x=pre[x];	
		   }
		  else
		    {
		    	s2[++num2]=y;boo[y]=false;y=pre[y];
			}
	   }
	s1[++num1]=x;boo[x]=false;
	for(i=num2;i>=1;i--) s1[++num1]=s2[i];
	//for(i=1;i<=num1;i++) printf("%lld ",s1[i]);
	for(i=1;i<=num1;i++) lsum[i]=lsum[i-1]+a[s1[i]];
	for(i=num1;i>=1;i--) rsum[i]=rsum[i+1]+a[s1[i]];
	memset(pre,0,sizeof(pre));
	memset(sum,0,sizeof(sum));
	dfs2(l);
	for(i=1;i<=num1;i++)
	  lmax[i]=max(lmax[i-1],sum[s1[i]]+lsum[i]);
	for(i=num1;i>=1;i--)
	  rmax[i]=max(rmax[+1],sum[s1[i]]+rsum[i]);
	ll MAX=0;
	for(i=1;i<=num1;i++)
	  MAX=max(MAX,lmax[i]+rmax[i+1]);
	printf("%lld",MAX);
} 

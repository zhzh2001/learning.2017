//#include<iostream>
#include<math.h>
#include<fstream>
#include<string.h>
#include<algorithm>
#include<stdio.h>
using namespace std;
ifstream fin("root.in");
ofstream fout("root.out");
long long head[200003],to[400003],next[400003],tot;
long long a[200003];
inline void add(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
long long mxy[200003],mxlian[200003];
long long ylian[200003],mxyy[200003],twov[200003];
long long ans[200003];
struct he{
	long long x,bian;
	he(){
		x=0,bian=0;
	}
};
inline void change(he &x,he &y,he &z,int xx,int yy){
	if(xx>x.x){
		z=y,y=x,x.x=xx,x.bian=yy;
	}else if(xx>y.x){
		z=y,y.x=xx,y.bian=yy;
	}else if(xx>z.x){
		z.x=xx,z.bian=yy;
	}
}
void dfs(int x,int fa){
	mxlian[x]=a[x];
	ylian[x]=a[x];
	mxy[x]=a[x];
	he firlian,seclian,thilian;
	he firylian,secylian,thiylian;
	he firmxy,secmxy,thimxy;
	firlian.x=0,seclian.x=0,thilian.x=0,firylian.x=0,secylian.x=0,thiylian.x=0;
	firmxy.x=0,secmxy.x=0,thimxy.x=0;
	int son=0;
	for(int i=head[x];i;i=next[i]){
		if(to[i]!=fa){
			son++;
			dfs(to[i],x);
			mxlian[x]=max(mxlian[x],a[x]+mxlian[to[i]]);
			change(firlian,seclian,thilian,mxlian[to[i]],to[i]);
			ylian[x]=max(ylian[x],ylian[to[i]]);
			change(firylian,secylian,thiylian,ylian[to[i]],to[i]);
			mxy[x]=max(mxy[x],mxy[to[i]]+a[x]);
			change(firmxy,secmxy,thimxy,mxy[to[i]],to[i]);
		}
	}
	if(son>=2){
		ylian[x]=max(ylian[x],a[x]+firlian.x+seclian.x);
	}
	mxy[x]=max(ylian[x],mxy[x]);
	if(son==2){
		ans[x]=max(ans[x],firlian.x+a[x]);
		ans[x]=max(ans[x],firmxy.x+a[x]);
		ans[x]=max(ans[x],firylian.x);
		ans[x]=max(ans[x],firlian.x+seclian.x+a[x]);
		ans[x]=max(ans[x],firylian.x+secylian.x);
		if(firmxy.bian!=firlian.bian){
			ans[x]=max(ans[x],firmxy.x+firlian.x+a[x]);
		}else{
			ans[x]=max(ans[x],secmxy.x+firlian.x+a[x]);
			ans[x]=max(ans[x],firmxy.x+seclian.x+a[x]);
		}
	}else if(son>=3){
		ans[x]=max(ans[x],firlian.x+a[x]);
		ans[x]=max(ans[x],firmxy.x+a[x]);
		ans[x]=max(ans[x],firylian.x);
		ans[x]=max(ans[x],firlian.x+seclian.x+a[x]);
		if(firmxy.bian!=firlian.bian){
			ans[x]=max(ans[x],firmxy.x+firlian.x+a[x]);
		}else{
			ans[x]=max(ans[x],secmxy.x+firlian.x+a[x]);
			ans[x]=max(ans[x],firmxy.x+seclian.x+a[x]);
		}
		if(firylian.bian==firlian.bian){
			ans[x]=max(ans[x],firylian.x+seclian.x+thilian.x+a[x]);
		}else if(firylian.bian==seclian.bian){
			ans[x]=max(ans[x],firylian.x+thilian.x+firlian.x+a[x]);
		}else{
			ans[x]=max(ans[x],firylian.x+seclian.x+firlian.x+a[x]);
		}
		if(secylian.bian==firlian.bian){
			ans[x]=max(ans[x],secylian.x+seclian.x+thilian.x+a[x]);
		}else if(secylian.bian==seclian.bian){
			ans[x]=max(ans[x],secylian.x+thilian.x+firlian.x+a[x]);
		}else{
			ans[x]=max(ans[x],secylian.x+seclian.x+firlian.x+a[x]);
		}
		if(thiylian.bian==firlian.bian){
			ans[x]=max(ans[x],thiylian.x+seclian.x+thilian.x+a[x]);
		}else if(thiylian.bian==seclian.bian){
			ans[x]=max(ans[x],thiylian.x+thilian.x+firlian.x+a[x]);
		}else{
			ans[x]=max(ans[x],thiylian.x+seclian.x+firlian.x+a[x]);
		}
	}else if(son==1){
		ans[x]=max(ans[x],firlian.x+a[x]);
		ans[x]=max(ans[x],firylian.x);
		ans[x]=max(ans[x],firmxy.x+a[x]);
	}else if(son==0){
		ans[x]=max(ans[x],a[x]);
	}
}
int n;
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>a[i];
	}
	for(int i=1;i<=n-1;i++){
		int u,v;
		fin>>u>>v;
		add(u,v),add(v,u);
	}
	dfs(1,0);
	long long ret=0;
	for(int i=1;i<=n;i++){
		ret=max(ret,ans[i]);
	}
	if(n==1){
		fout<<a[1]<<endl;
	}else{
		fout<<ret<<endl;
	}
	return 0;
}
/*

in:
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9

out:
25

in:
2
20 10
1 2
out:
30

in:
6
1 2 3 4 5 6
1 2
1 3
1 4
2 5
2 6
out:
21

in:
3
1 2 3
1 2
2 3
out:

*/

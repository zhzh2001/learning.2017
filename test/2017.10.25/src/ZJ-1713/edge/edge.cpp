//#include<iostream>
#include<math.h>
#include<fstream>
#include<string.h>
#include<stdio.h>
using namespace std;
inline int read(){
	char c;int t=0;c=getchar();
   	while(!(c>='0'&&c<='9'))c=getchar();
   	while(c>='0'&&c<='9')t=t*10+c-48,c=getchar();
	return t;
}
ifstream fin("edge.in");
ofstream fout("edge.out");
const long long mod=1000000007;
long long n,h,w;
char opt[500003];
inline void baoli(){
	int nowx=0,nowy=0,now;
	long long sum,ans=0;
	for(int i=1;i<=h;i++){
		for(int j=1;j<=w;j++){
			nowx=i,nowy=j,now=1,sum=0;
			while(1){
				sum++;
				if(opt[now]=='L'){
					nowy--;
				}else if(opt[now]=='R'){
					nowy++;
				}else if(opt[now]=='U'){
					nowx--;
				}else{
					nowx++;
				}
				if(nowx<=0||nowx>h||nowy<=0||nowy>w){
					ans+=sum;
					break;
				}
				ans%=mod;
				now++;
				if(now==n+1){
					now=1;
				}
			}
		}
	}
	fout<<ans<<endl;
}
inline void solve1(){
	fout<<(w*(1+w)/2*h)%mod;
}
inline void solve2(){
	long long now=1,nowx=0,nowy=0;
	long long sum=0,ans=0;
	while(1){
		sum++;
		if(nowx>=h||nowy>=w){
			break;
		}
		if(opt[now]=='L'){
			nowy++,ans+=sum*(h-nowx);
		}else if(opt[now]=='R'){
			nowy--;
		}else if(opt[now]=='U'){
			nowx--;
		}else{
			nowx++,ans+=sum*(w-nowy);
		}
		ans%=mod;
		now++;
		if(now==n+1){
			now=1;
		}
	}
	fout<<ans<<endl;
}
inline void solve4(){
	int pos=0;
	for(int i=1;i<=n;i++){
		if(opt[i]=='L'||opt[i]=='R'){
			pos=i;
			break;
		}
	}
	int now=0;
	long long ans=0,res=w,mx=0,mn=0;
	for(int i=1;i<=pos-1;i++){
		if(res==0){
			break;
		}
		if(opt[i]=='U'){
			now++;
			if(now>mx){
				mx=now,ans+=i,res--;
			}
		}else if(opt[i]=='D'){
			now--;
			if(now<mn){
				mn=now,ans+=i,res--;
			}
		}
		ans%=mod;
	}
	ans+=(res*pos%mod);
	ans%=mod;
	fout<<ans<<endl;
}
int main(){
	fin>>n>>h>>w;
	int mnx=0,mny=0,mxx=0,mxy=0,nowx=0,nowy=0,woc1=0,woc2=0,woc3=0;
	for(int i=1;i<=n;i++){
		fin>>opt[i];
		if(opt[i]=='L'){
			woc1++;
			nowy--;
			mny=min(mny,nowy);
		}else if(opt[i]=='R'){
			woc3++;
			nowy++;
			mxy=max(mxy,nowy);
		}else if(opt[i]=='U'){
			nowx++;
			mxx=max(mxx,nowx);
		}else{
			woc2++;
			nowx--;
			mnx=min(mnx,nowx);
		}
	}
	if(mxx-mnx<h&&mxy-mny<w&&nowx==0&&nowy==0){
		fout<<"-1"<<endl;
		return 0;
	}
	if(woc1==n){
		solve1();
		return 0;
	}else if(woc1+woc2==n){
		solve2();
		return 0;
	}else if(h<=1){
		solve4();
		return 0;
	}else{
		baoli();
		return 0;
	}
	return 0;
}
/*

in:
4 1 500000
RLRL
out:
-1

in:
3 4 6
RUL
out:
134

*/

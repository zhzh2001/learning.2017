#include <fstream>
#include <vector>
using namespace std;
ifstream fin("core.in");
ofstream fout("core.out");
vector<vector<int> > ans;
inline void add_circle(int a, int b, int c, int d = 0)
{
	vector<int> now;
	now.push_back(a);
	now.push_back(b);
	now.push_back(c);
	if (d)
		now.push_back(d);
	ans.push_back(now);
}
void dfs(int n)
{
	if (n == 3)
	{
		add_circle(1, 2, 3);
		add_circle(1, 2, 3);
	}
	else if (n == 4)
	{
		add_circle(1, 2, 3);
		add_circle(1, 2, 4);
		add_circle(1, 3, 4);
		add_circle(2, 3, 4);
	}
	else
	{
		add_circle(n, n - 1, n - 2);
		add_circle(n, n - 1, 1);
		for (int i = 1; i < n - 2; i++)
			add_circle(n, i, n - 1, i + 1);
		dfs(n - 2);
	}
}
int main()
{
	int n;
	fin >> n;
	dfs(n);
	fout << ans.size() << endl;
	for (int i = 0; i < ans.size(); i++)
	{
		fout << ans[i].size();
		for (int j = 0; j < ans[i].size(); j++)
			fout << ' ' << ans[i][j];
		fout << endl;
	}
	return 0;
}
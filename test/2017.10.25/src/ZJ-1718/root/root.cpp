#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("root.in");
ofstream fout("root.out");
const int N = 100005;
int a[N];
long long f[N], g[N], ans;
vector<int> mat[N];
void dfs(int k, int fat)
{
	long long g1 = 0, g2 = 0;
	for (int i = 0; i < mat[k].size(); i++)
		if (mat[k][i] != fat)
		{
			dfs(mat[k][i], k);
			f[k] = max(f[k], f[mat[k][i]]);
			if (g[mat[k][i]] > g1)
			{
				g2 = g1;
				g1 = g[mat[k][i]];
			}
			else
				g2 = max(g2, g[mat[k][i]]);
		}
	f[k] = max(f[k], g1 + g2 + a[k]);
	g[k] = g1 + a[k];
}
void dfs(int k, int fat, long long pf, long long pg)
{
	ans = max(ans, f[k] + pf);
	long long f1 = pf, f2 = 0, g1 = pg, g2 = 0, g3 = 0;
	for (int i = 0; i < mat[k].size(); i++)
		if (mat[k][i] != fat)
		{
			if (f[mat[k][i]] > f1)
			{
				f2 = f1;
				f1 = f[mat[k][i]];
			}
			else
				f2 = max(f2, f[mat[k][i]]);
			if (g[mat[k][i]] > g1)
			{
				g3 = g2;
				g2 = g1;
				g1 = g[mat[k][i]];
			}
			else if (g[mat[k][i]] > g2)
			{
				g3 = g2;
				g2 = g[mat[k][i]];
			}
			else
				g3 = max(g3, g[mat[k][i]]);
		}
	for (int i = 0; i < mat[k].size(); i++)
		if (mat[k][i] != fat)
		{
			long long gg1, gg2, ff;
			if (g[mat[k][i]] == g1)
			{
				gg1 = g2;
				gg2 = g3;
			}
			else
			{
				gg1 = g1;
				if (g[mat[k][i]] == g2)
					gg2 = g3;
				else
					gg2 = g2;
			}
			if (f[mat[k][i]] == f1)
				ff = f2;
			else
				ff = f1;
			ff = max(ff, a[k] + gg1 + gg2);
			dfs(mat[k][i], k, ff, a[k] + gg1);
		}
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	for (int i = 1; i < n; i++)
	{
		int u, v;
		fin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	dfs(1, 0);
	dfs(1, 0, 0, 0);
	fout << ans << endl;
	return 0;
}
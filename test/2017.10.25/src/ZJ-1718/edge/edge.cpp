#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("edge.in");
ofstream fout("edge.out");
const int N = 500005, MOD = 1e9 + 7;
const long long INF = 1e18;
long long u[N], d[N], l[N], r[N], row[N], col[N];
int main()
{
	int n, h, w;
	string s;
	fin >> n >> h >> w >> s;
	s = ' ' + s;
	fill(u + 1, u + h + 1, INF);
	fill(d + 1, d + h + 1, INF);
	fill(l + 1, l + w + 1, INF);
	fill(r + 1, r + w + 1, INF);
	int x = 0, y = 0;
	for (int i = 1; i <= n; i++)
	{
		switch (s[i])
		{
		case 'U':
			if (x <= 0)
				u[1 - x] = min(u[1 - x], 1ll * i);
			x--;
			break;
		case 'D':
			if (x >= 0)
				d[h - x] = min(d[h - x], 1ll * i);
			x++;
			break;
		case 'L':
			if (y <= 0)
				l[1 - y] = min(l[1 - y], 1ll * i);
			y--;
			break;
		case 'R':
			if (y >= 0)
				r[w - y] = min(r[w - y], 1ll * i);
			y++;
			break;
		}
	}
	if (x < 0)
		for (int i = 1 - x; i <= h; i++)
			u[i] = min(u[i], u[i + x] + n);
	if (x > 0)
		for (int i = h - x; i; i--)
			d[i] = min(d[i], d[i + x] + n);
	if (y < 0)
		for (int i = 1 - y; i <= w; i++)
			l[i] = min(l[i], l[i + y] + n);
	if (y > 0)
		for (int i = w - y; i; i--)
			r[i] = min(r[i], r[i + y] + n);
	for (int i = 1; i <= h; i++)
		row[i] = min(u[i], d[i]);
	for (int i = 1; i <= w; i++)
		col[i] = min(l[i], r[i]);
	sort(row + 1, row + h + 1);
	sort(col + 1, col + w + 1);
	if (row[h] == INF && col[w] == INF)
		fout << -1 << endl;
	else
	{
		int ans = 0;
		for (int i = 1, j = 1; i <= h; i++)
		{
			for (; j <= w && row[i] > col[j]; j++)
				;
			ans = (ans + 1ll * row[i] % MOD * (w - j + 1)) % MOD;
		}
		for (int i = 1, j = 1; i <= w; i++)
		{
			for (; j <= h && col[i] > row[j]; j++)
				;
			ans = (ans + 1ll * col[i] % MOD * (h - j + 1)) % MOD;
		}
		fout << ans << endl;
	}
	return 0;
}
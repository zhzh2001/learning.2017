#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 305
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int n;

int main(){
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	n = read();
	if(n == 3){
		puts("2");
		puts("3 1 2 3");
		puts("3 1 2 3");
		return 0;
	}
	if(n == 4){
		puts("4");
		puts("3 1 2 3");
		puts("3 1 3 4");
		puts("3 1 2 4");
		puts("3 2 3 4");
		return 0;
	}
	if(n == 5){
		puts("6");
		puts("4 1 2 3 4");
		puts("4 2 3 4 5");
		puts("3 1 2 4");
		puts("3 1 3 5");
		puts("3 1 3 5");
		puts("3 2 4 5");
		return 0;
	}
	if(n == 6){
		puts("10");
		puts("3 1 2 3");
		puts("3 1 3 4");
		puts("3 1 4 5");
		puts("3 1 5 6");
		puts("3 1 2 6");
		puts("3 2 3 5");
		puts("3 2 4 5");
		puts("3 2 4 6");
		puts("3 3 4 6");
		puts("3 3 5 6");
		return 0;
	}
	if(n == 7){
		puts("14");
		puts("3 1 2 3");
		puts("3 1 3 4");
		puts("3 1 4 5");
		puts("3 1 5 6");
		puts("3 1 6 7");
		puts("3 1 2 7");
		puts("3 2 3 5");
		puts("3 2 5 7");
		puts("3 2 4 6");
		puts("3 2 4 6");
		puts("3 3 5 6");
		puts("3 3 4 7");
		puts("3 3 6 7");
		puts("3 4 5 7");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 200005
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int n;
int a[N],sit;
ll Mx1[N],Mx2[N],ans,sum;
bool vise[N*2];
int head[N],to[N*2],nxt[N*2],cnt;
bool vis[N];

inline void add(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
	to[++cnt] = a; nxt[cnt] = head[b]; head[b] = cnt;
}

void dfs(int x,int f){
	for(int i = head[x];i;i = nxt[i]){
		int v = to[i];
		if(v != f){
			dfs(v,x);
			if(Mx1[v] > Mx1[x]){
				Mx2[x] = Mx1[x];
				Mx1[x] = Mx1[v];
			}
			else if(Mx1[v] > Mx2[x])
				Mx2[x] = Mx1[v];
		}
	}
	Mx1[x] += a[x];
	if(Mx1[x] + Mx2[x] > ans){
		ans = Mx1[x]+Mx2[x];
		sit = x;
	}
}

void dfsdel(int x,int f){
	vis[x] = true;
	bool l = false,r = false;
	for(int i = head[x];i;i = nxt[i]){
		int v = to[i];
		if(v != f){
			if(!l && Mx1[v] == Mx1[x]-a[x]){
				dfsdel(v,x);
				l = true;
			}
			else if(!r && Mx1[v] == Mx2[x]){
				dfsdel(v,x);
				r = true;
			}
			if(l && r) break;
		}
	}
}

void dfs2(int x){
	vis[x] = true;
	for(int i = head[x];i;i = nxt[i]){
		int v = to[i];
		if(!vis[v]){
			dfs(v,x);
			if(Mx1[v] > Mx1[x]){
				Mx2[x] = Mx1[x];
				Mx1[x] = Mx1[v];
			}
			else if(Mx1[v] > Mx2[x])
				Mx2[x] = Mx1[v];
		}
	}
	Mx1[x] += a[x];
	if(Mx1[x] + Mx2[x] > ans){
		ans = Mx1[x]+Mx2[x];
		sit = x;
	}
}

int main(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;i++)
		a[i] = read();
	for(int i = 1;i < n;i++){
		int x = read(),y = read();
		add(x,y);
	}
	dfs(1,1);
//	printf("%lld %d\n",ans,sit);
	sum = ans;
	ans = 0;
	dfsdel(sit,sit);
//	for(int i = 1;i <= n;i++)
//		printf("%d ",vis[i]);
	memset(Mx1,0,sizeof Mx1);
	memset(Mx2,0,sizeof Mx2);
	for(int i = 1;i <= n;i++)
		if(!vis[i])
			dfs2(i);
	sum += ans;
	printf("%lld\n",sum);
	return 0;
}

#include<ctime>
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 500005
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int n,h,w;

int main(){
	freopen("edge.in","w",stdout);
	srand(time(0));
	n = 500000;
	h = 500000;
	w = 500000;
	printf("%d %d %d\n",n,h,w);
	for(int i = 1;i <= n;i++){
		int x = rand()%4;
		if(x == 0) putchar('U');
		else if(x == 1) putchar('D');
		else if(x == 2) putchar('L');
		else putchar('R');
	}
	puts("");
	return 0;
}

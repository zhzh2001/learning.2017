#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 500005
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll sum;
char s[N];
int n,m,len,cnt;
struct node{
	bool f;		//0 = U,D(n);1 = L,R(m)
	int step;
}p[N];

int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	len = read(); n = read(); m = read();
	scanf("%s",s+1);
	int edx = 0,edy = 0;
	int Mxx,Mxy,Mix,Miy;
	Mxx = Mxy = Mix = Miy = 0;
	for(int i = 1;i <= len;i++){
		if(s[i] == 'U'){
			edx--;
			if(edx < Mix){
				Mix = edx;
				n--;
				sum += 1ll*m*i;
			}
		}
		else if(s[i] == 'D'){
			edx++;
			if(edx < Mxx){
				Mxx = edx;
				n--;
				sum += 1ll*m*i;
			}
		}
		else if(s[i] == 'L'){
			edy--;
			if(edy < Miy){
				Miy = edy;
				m--;
				sum += 1ll*n*i;
			}
		}
		else{
			edy++;
			if(edy > Mxy){
				Mxy = edy;
				m--;
				sum += 1ll*n*i;
			}
		}
		if(n == 0 || m == 0)
			break;
	}
	if(n == 0 || m == 0){
		printf("%lld\n",sum);
		return 0;
	}
	if(edx == 0 && edy == 0){
		printf("-1");
		return 0;
	}
	sum += n*m*len;
	int x = 0,y = 0;
	Mxx = Mxy = Mix = Miy = 0;
	for(int i = 1;i <= len;i++){
		if(s[i] == 'U'){
			x--;
			if(x < Mix && edx < 0){
				Mix = edx;
				p[++cnt] = (node){0,i};
			}
		}
		else if(s[i] == 'D'){
			x++;
			if(x < Mxx && edx > 0){
				Mxx = x;
				p[++cnt] = (node){0,i};
			}
		}
		else if(s[i] == 'L'){
			y--;
			if(y < Miy && edy < 0){
				Miy = y;
				p[++cnt] = (node){1,i};
			}
		}
		else{
			y++;
			if(y > Mxy && edy > 0){
				Mxy = y;
				p[++cnt] = (node){1,i};
			}
		}
	}
	while(1){
		for(int i = 1;i <= cnt;i++){
			if(p[i].f){
				m--;
				sum += 1ll*n*p[i].step;
			}
			else{
				n--;
				sum += 1ll*m*p[i].step;
			}
			if(n == 0 || m == 0)
				break;
		}
		if(n == 0 || m == 0)
			break;
		sum += n*m*len;
	}
	printf("%lld\n",sum);
	return 0;
}

#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<ctime>
#include<algorithm>
#include<vector>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=200010;
ll head[N],nxt[N],vet[N],f[N][2],v[N],n,tot,ans;	bool fl1=1,fl2=1;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
ll dfs(ll x,ll pre){
	f[x][0]=f[x][1]=v[x];
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=pre){
		dfs(vet[i],x);
		f[x][1]=max(f[x][1],f[vet[i]][0]+f[x][0]);
		f[x][0]=max(f[x][0],f[vet[i]][0]+v[x]);
	}
}
bool cmp(ll a,ll b){	return a>b;	}
void work1(){
	sort(v+2,v+n+1,cmp);
	writeln(v[1]+v[2]+v[3]+v[4]);
}
void work2(){
	ll ans=0;
	For(i,1,n)	ans+=v[i];
	writeln(ans);
}
int main(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n=read();
	For(i,1,n)	v[i]=read(),ans=max(ans,v[i]);
	For(i,2,n){
		ll x=read(),y=read();	fl1&=x==1;	fl2&=(x==i)&&(y==i+1);
		insert(x,y);	insert(y,x);
	}
	if (fl1)	work1();
	else if (fl2)	work2();
	else{
		For(x,1,n)	for(ll i=head[x];i;i=nxt[i]){
			ll y=vet[i],sum=0;	dfs(x,y);	dfs(y,x);
			ans=max(ans,f[x][1]+f[y][1]);
			if (clock()>900)	return writeln(ans),0;
		}writeln(ans);
	}
}

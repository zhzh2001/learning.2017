#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 2010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=1000010,mod=1e9+7;
char s[N];
ll n,h,w,nowx,nowy,mnx[N],mny[N],mxx[N],mxy[N],ans;
int main(){
	freopen("edge.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	h=read();	w=read();
	scanf("%s",s+1);
	For(i,1,10000000){
		ll t=(i-1)%n+1;
		if (s[t]=='L')	nowy--;
		if (s[t]=='R')	nowy++;
		if (s[t]=='U')	nowx--;
		if (s[t]=='D')	nowx++;
		mnx[i]=max(mnx[i-1],-nowx);
		mxx[i]=max(mxx[i-1],nowx);
		mny[i]=max(mny[i-1],-nowy);
		mxy[i]=max(mxy[i-1],nowy);
		if (mnx[i]!=mnx[i-1])	ans+=i*(w-mny[i]-mxy[i])%mod;
		if (mxx[i]!=mxx[i-1])	ans+=i*(w-mny[i]-mxy[i])%mod;
		if (mny[i]!=mny[i-1])	ans+=i*(h-mnx[i]-mxx[i])%mod;
		if (mxy[i]!=mxy[i-1])	ans+=i*(h-mnx[i]-mxx[i])%mod;
		if (mnx[i]+mxx[i]==h)	break;
		if (mny[i]+mxy[i]==w)	break;
		if (i>=10000000)	return puts("-1"),0;
	}writeln(ans%mod);
}

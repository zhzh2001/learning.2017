#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 500005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f; 
}
ll n,h,w,xx[N],yy[N],x,y,ans,mod=1000000007; char s[N];
int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	n=read(); h=read(); w=read();
	scanf("%s",s+1);
	memset(xx,7,sizeof yy);
	memset(yy,7,sizeof yy);
	rep(i,1,n){
		if (s[i]=='U') --x;
		if (s[i]=='D') ++x;
		if (s[i]=='L') --y;
		if (s[i]=='R') ++y;
		if (x>0){
			rep(j,h-x+1,h) xx[j]=min(xx[j],i);
		}else if (x<0){
			rep(j,1,-x) xx[j]=min(xx[j],i);
		}
		if (y>0){
			rep(j,w-y+1,w) yy[j]=min(yy[j],i);
		}else if (y<0){
			rep(j,1,-y) yy[j]=min(yy[j],i);
		}
	}
	if (x==0&&y==0)  return puts("-1")&0;
	rep(i,1,h) rep(j,1,w){
		if (min(xx[i],yy[j])==xx[0]);
		else ans=ans+min(xx[i],yy[j])%mod;
	}
	printf("%lld",ans);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 500005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f; 
}
ll n,h,w,xx[N],yy[N],x,y,ans,mod=1000000007; char s[N];
int main(){
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	n=read();
	if (n==3){
		puts("2");
		puts("3 1 2 3");
		puts("3 1 2 3");
	}else
	if (n==4){
		puts("4");
		puts("3 1 2 3");
		puts("3 1 2 4");
		puts("3 1 3 4");
		puts("3 2 3 4");
	}else
	if (n==5){
		puts("6");
		puts("3 5 4 2");
		puts("3 3 1 5");
		puts("4 4 5 2 3");
		puts("4 4 3 2 1");
		puts("3 4 2 1");
		puts("3 3 1 5");
	}
}

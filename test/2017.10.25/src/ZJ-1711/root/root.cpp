#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f; 
}
ll n,a[N],tot,ans,f[N][2][3],head[N];
struct edge{ ll to,next; }e[N<<1];
struct data{ ll v,id; };
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
ll get(data a,data b,data c,data d){
	ll ans=-1;
	if (a.id!=c.id) return a.v+c.v;
	else{
		if (a.id!=d.id) ans=a.v+d.v;
		if (b.id!=c.id) ans=max(ans,b.v+c.v);
	}
	return max(0,ans);
}
void dfs(ll u,ll last){
	memset(f[u],-1,sizeof f[u]);
	f[u][1][1]=a[u];
	ll r1=-1,r2=-1,r3=-1,t1=-1,t2=-1;
	data x1,x2,y1,y2;
	x1.v=x2.v=y1.v=y2.v=-1;
	x1.id=x2.id=y1.id=y2.id=0;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs(v,u);
		if (r1<f[v][1][1]){
			r3=r2; r2=r1; r1=f[v][1][1];
		}else if(r2<f[v][1][1]){
			r3=r2; r2=f[v][1][1];
		}else r3=max(r3,f[v][1][1]);
		if (t1<f[v][0][2]){
			t2=t1; t1=f[v][0][2];
		}else t2=max(t2,f[v][0][2]);
		if (x1.v<f[v][1][2]){
			x2=x1; x1=(data){f[v][1][2],v};	
		}else{
			if (x2.v<f[v][1][2]);
			x2=(data){f[v][1][2],v};
		}
		if (y1.v<f[v][1][1]){
			y2=y1; y1=(data){f[v][1][1],v};
		}else{
			if (y2.v<f[v][1][1]);
			y2=(data){f[v][1][1],v};
		}
		f[u][0][1]=max(f[u][0][1],f[v][0][1]);
		f[u][0][2]=max(f[u][0][2],f[v][0][2]);
		f[u][1][1]=max(f[u][1][1],f[v][1][1]+a[u]);
		f[u][1][2]=max(f[u][1][2],max(f[v][1][2],f[v][0][1])+a[u]);
	}
	if (r2!=-1) f[u][0][1]=max(f[u][0][1],r1+r2+a[u]);
	f[u][0][2]=max(f[u][0][2],max(get(x1,x2,y1,y2),r1+r2+r3)+a[u]);
}
int main(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	dfs(1,-1);
	rep(i,0,1) rep(j,i,3) ans=max(ans,f[1][i][j]);
	printf("%d",ans);
}

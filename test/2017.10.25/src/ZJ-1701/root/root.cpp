#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
const int N=2e5+5;
struct cs{int to,nxt;}a[N*2];
int head[N],ll;
Ll f[N][4];
int b[N];
int n,m,x,y;
void init(int x,int y){a[++ll].to=y;a[ll].nxt=head[x];head[x]=ll;}
void dfs(int x,int fa){
	if(!head[x]){
		f[x][1]=f[x][0]=b[x];
		return;
	}
	int c[2][4];memset(c,0,sizeof c);
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=fa){
			int y=a[k].to;
			dfs(y,x);
			if(f[c[0][1]][0]<f[y][0])
				c[0][3]=c[0][2],c[0][2]=c[0][1],c[0][1]=y;else
			if(f[c[0][2]][0]<f[y][0])
				c[0][3]=c[0][2],c[0][2]=y;else
			if(f[c[0][3]][0]<f[y][0])c[0][3]=y;
			
			if(f[c[1][1]][1]<f[y][1])
				c[1][2]=c[1][1],c[1][1]=y;else
			if(f[c[1][2]][1]<f[y][1])c[1][2]=y;
		}
	f[x][0]=f[c[0][1]][0]+b[x];
	
	for(int k=head[x];k;k=a[k].nxt)if(a[k].to!=fa)f[x][1]=max(f[x][1],f[a[k].to][1]);
	f[x][1]=max(f[x][1],f[c[0][1]][0]+f[c[0][2]][0]+b[x]);
	
	for(int k=head[x];k;k=a[k].nxt)if(a[k].to!=fa){
		int y=a[k].to,v;
		f[x][2]=max(f[x][2],f[y][2]+b[x]);
		if(c[0][1]==y)v=c[0][2];else v=c[0][1];
		f[x][2]=max(f[x][2],f[y][1]+f[v][0]+b[x]);
	}
	
	f[x][3]=f[c[1][1]][1]+f[c[1][2]][1];
	for(int k=head[x];k;k=a[k].nxt)if(a[k].to!=fa){
		int y=a[k].to,v,vv;
		f[x][3]=max(f[x][3],f[y][3]);
		if(c[0][1]==y)v=c[0][2];else v=c[0][1];
		if(c[0][2]==v||c[0][2]==y)vv=c[0][3];else vv=c[0][2];
		f[x][3]=max(f[x][3],f[y][1]+f[v][0]+f[vv][0]+b[x]);
		f[x][3]=max(f[x][3],f[y][2]+f[v][0]+b[x]);
	}	
}
int main()
{
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)b[i]=RR();
	for(int i=1;i<n;i++){
		x=RR();y=RR();
		init(x,y);init(y,x);
	}
	dfs(1,-1);
	printf("%lld",f[1][3]);
}

/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
##############
           ##
		  ##
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int ans=0,flag=0;

void Check(int l,int r){
	int now=l-1;
	if (r-l+1==3) ans+=2;
	if (r-l+1==4) ans+=3;
	if (r-l+1==5) ans+=6;
	if (r-l+1==3&&flag){
		printf("3 %d %d %d\n",now+1,now+2,now+3);
		printf("3 %d %d %d\n",now+1,now+2,now+3);
	}	
	if (r-l+1==4&&flag){
		printf("4 %d %d %d %d\n",now+1,now+2,now+3,now+4);
		printf("4 %d %d %d %d\n",now+1,now+2,now+4,now+3);
		printf("4 %d %d %d %d\n",now+1,now+3,now+2,now+4);
	}
	if (r-l+1==5&&flag){
		printf("4 %d %d %d %d\n",now+1,now+2,now+3,now+4);
		printf("4 %d %d %d %d\n",now+1,now+4,now+2,now+3);
		printf("3 %d %d %d\n",now+1,now+2,now+5);
		printf("3 %d %d %d\n",now+1,now+3,now+5);
		printf("3 %d %d %d\n",now+2,now+5,now+4);
		printf("3 %d %d %d\n",now+5,now+3,now+4);
	}
}

void Loop(int x1,int x2,int y1,int y2){
	ans+=2;
	if (flag){
		printf("4 %d %d %d %d\n",x1,y1,x2,y2);
		printf("4 %d %d %d %d\n",x1,y1,x2,y2);
	}
}

void Loop(int x1,int x2,int y1,int y2,int y3){
	ans+=3;
	if (flag){
		printf("4 %d %d %d %d\n",x1,y1,x2,y2);
		printf("4 %d %d %d %d\n",x1,y2,x2,y3);
		printf("4 %d %d %d %d\n",x1,y1,x2,y3);
	}
}

void Loop(int x1,int x2,int y1,int y2,int y3,int y4){
	ans+=5;
	Check(y1,y4);
	if (flag){
		printf("3 %d %d %d\n",x1,x2,y1);
		printf("3 %d %d %d\n",x1,x2,y4);
		printf("4 %d %d %d %d\n",x1,y1,x2,y4);
		printf("4 %d %d %d %d\n",x1,y2,x2,y3);
		printf("4 %d %d %d %d\n",x1,y2,x2,y3);
	}
}

void Merge(int l1,int r1,int l2,int r2){	
//	printf("Q%d %d %d %d\n",l1,r1,l2,r2);
	if ((r1-l1)%2==0) swap(l1,l2),swap(r1,r2);
	while (r1>l1){
		for (int i=l2;i<=r2;i+=2){
			if (i==(r2-2)){
				Loop(l1,l1+1,i,i+1,i+2);
				break;
			}
			Loop(l1,l1+1,i,i+1);
		}
		l1+=2;
	}
}

void Solve(int l,int r){
	if (r-l+1<6){
		Check(l,r);
		return;
	}
	if (r-l+1==6){
		Loop(l,l+1,l+2,l+3,l+4,l+5);
		return;
	}
	int mid=(l+r)>>1;
	if ((mid-l)%2==0 && (r-mid+1)%2==0) mid--;
	Solve(l,mid);
	Solve(mid+1,r);
	Merge(l,mid,mid+1,r);
}


int main(){
   // say hello

	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);

	int n=read();
	Solve(1,n);
	flag=1;
	printf("%d\n",ans);
	Solve(1,n);
	
	// say goodbye
}


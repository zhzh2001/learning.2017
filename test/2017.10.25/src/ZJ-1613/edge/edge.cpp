#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=2e5+7,jyt=1e9+7;
int n,h,w,Len;
char s[N];

inline bool Out(int x,int y){
	if (x<1||y<1||x>h||y>w) return 1;
	return 0;
}
int Check(int x,int y){
	For(i,0,Len){
		if (s[i]=='R') y++;
		if (s[i]=='L') y--;
		if (s[i]=='U') x++;
		if (s[i]=='D') x--;
		if (Out(x,y)) return i+1;
	}
}

int main(){
	// say hello

	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);

	n=read();h=read();w=read();
	scanf("%s",s);
	int X=0,Y=0,max_h=0,min_h=0,max_w=0,min_w=0;
	Len=strlen(s)-1;
	For(i,0,Len){
		if (s[i]=='R') Y++;
		if (s[i]=='L') Y--;
		if (s[i]=='U') X++;
		if (s[i]=='D') X--;
		max_h=max(max_h,X);
		min_h=min(min_h,X);
		max_w=max(max_w,Y);
		min_w=min(min_w,Y);
	}
	int ans=0;
	For(x,1,h) For(y,1,w){
		   int x1=x,y1=y;
			while (1){
				if (Out(x+max_h,y)||Out(x+min_h,y)||Out(x,y+max_w)||Out(x,y+min_w)){
					ans+=Check(x,y);
		//			printf("%d   %d %d\n",Check(x,y),x,y);
					if (ans>=jyt) ans-=jyt;
					break;
				}
				if (X==0&&Y==0){
					printf("-1");
					return 0;
				}
				x+=X,y+=Y;
				ans+=Len+1;
				if (ans>=jyt) ans-=jyt;
			}
			x=x1,y=y1;
	}
	printf("%d\n",ans);

	// say goodbye
}


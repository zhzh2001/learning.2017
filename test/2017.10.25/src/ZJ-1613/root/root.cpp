#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}


const int N=10007;
int n,head[N],dep[N],fa[N];
struct line{
	int u,v,next;
}e[N<<1];
int cnt,tmp,flag,a[N],vis[N];

void ins(int u,int v){
	e[++cnt]=(line){
		u,v,head[u]
	};
	head[u]=cnt;
}
void insert(int u,int v){
	ins(u,v);
	ins(v,u);
}

void dfs(int x){
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (dep[y]) continue;
		dep[y]=dep[x]+1;
		fa[y]=x;
		dfs(y);
	}
}

void dfs(int x,int y){
	tmp+=a[x];
	if (y!=x) tmp+=a[y];
	if (vis[x]) flag=0;
	if (vis[y]) flag=0;
	vis[x]=1;
	vis[y]=1;
	while (x!=y){
		if (dep[x]>dep[y]){
			x=fa[x];
			if (vis[x]&&x!=y) flag=0;
			if (x!=y) tmp+=a[x],vis[x]=1;
		}
		else {
			y=fa[y];
			if (vis[y]&&x!=y) flag=0; 
			if (x!=y) tmp+=a[y],vis[y]=1;
		}
	}
}

int main(){
	// say hello

	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);

	n=read();
	For(i,1,n) a[i]=read();
	For(i,1,n-1){
		int u=read(),v=read();
		insert(u,v);
	}
	dep[1]=1;
	dfs(1);
	int ans=0;
	For(i,1,n) For(j,i,n) For(x,1,n) For(y,x,n){
		memset(vis,0,sizeof(vis));
		tmp=0;
		flag=1;
		dfs(i,j);
		dfs(x,y);
		if (flag&&ans<tmp) ans=max(ans,tmp);//,printf("%d %d %d %d\n",i,j,x,y);
	}
	printf("%d\n",ans);



	// say goodbye
}


program ec12;
var 
	n,m,i,j,tot:longint;
	heap:array[1..10000] of longint; 
procedure push(x:longint);
var
	i,p:longint;
begin 
	inc(tot);
	heap[tot]:=x;
	i:=tot;
	while i>1 do 
	begin 
		p:=i div 2;
		if heap[i]<heap[p] then 
		heap[i]:=heap[p]
		else
		break;
		i:=p;
	end;
	heap[i]:=x;
end;
function pop:longint;
var 
	i,x,lchild,rchild:longint;
begin
	pop:=heap[1];
	x:=heap[tot];
	dec(tot);
	i:=1;
	while i*2<=tot do 
	begin 
		lchild:=i*2;
		rchild:=i*2+1;
		if (rchild<=tot) and (heap[lchild]>heap[rchild]) then 
		lchild:=rchild;
		if heap[i]>heap[lchild] then 
		heap[i]:=heap[lchild]
		else
		break;
		i:=lchild;
	end;
	heap[i]:=x;
end;
begin 
	readln(n);
	tot:=0;
	for i:=1 to n do
	begin 	
		read(m);
		push(m);
	end;
	for j:=1 to n do
	writeln(pop);
end. 
program ec12;
var 
	n,m,i,j:longint;
	a:array[1..10000] of longint;
procedure qsrt(l,r:longint);
var 
	i,j,v,s:longint;
begin 
	i:=l; j:=r; v:=a[(l+r) div 2];
	repeat
	while a[i]<v do inc(i);
	while a[j]>v do dec(j);
	if i<=j then 
	begin
		s:=a[i];
		a[i]:=a[j];
		a[j]:=s;
		inc(i);
		dec(j);
	end;
	until i>=j;
	if l<j then qsrt(l,j);
	if i<r then qsrt(i,r);
end;
begin 
	readln(n);
	for i:=1 to n do
	read(a[i]);
	qsrt(1,n);
	for i:=1 to n do
	writeln(a[i]);
end. 
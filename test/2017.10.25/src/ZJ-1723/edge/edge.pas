program ec12;
var 
	ans,h,w,step,k:int64;
	a:array[1..500001,1..2] of longint;
	p:array[0..500001] of longint;
	left:array[-500001..500001] of longint;
	vis:array[0..500001] of boolean;
	n,m,i,j,x,y,tot:longint;
	c:char;
	flag,b,e:boolean;
begin
	assign(input,'edge.in'); reset(input);
	assign(output,'edge.out'); rewrite(output);
	readln(n,h,w);
	x:=0;
	y:=0;
	fillchar(a,sizeof(a),0);
	flag:=true;
	b:=true;
	e:=true;
	ans:=0;
	for i:=1 to n do 
	begin 
		read(c);
		if c='L' then 
		begin 
			a[i,1]:=-1;
			dec(x);
		end;
		if c='R' then 
		begin 
			a[i,1]:=1;;
			inc(x);
			b:=false;
		end;
		if c='U' then 
		begin 
			a[i,2]:=-1;
			dec(y);
			b:=false;
			e:=false;
		end;
		if c='D' then 
		begin 
			a[i,2]:=1;
			inc(y);
			b:=false;
			e:=false;
		end;
		if (abs(x)>=w) or (abs(y)>=h) then 
		flag:=false;
	end;
	if (x=0) and (y=0) then 
	begin 
		if flag then 
		begin 	
			writeln(-1);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (y=0) and b then 
	begin
		fillchar(vis,sizeof(vis),true);
		fillchar(left,sizeof(left),0);
		tot:=0;
		for i:=1 to w do 
		begin 
			ans:=ans+n*(i div n);
			inc(left[i mod n]);
		end;
		for i:=1 to n do 
		begin 
			if left[i]>0 then 
			inc(ans,left[i]*i);
		end;
		writeln((ans*h) mod 1000000007);
		close(input);
		close(output);
		halt;
	end;
	if (y=0) and e then 
	begin		
		fillchar(vis,sizeof(vis),true);
		tot:=0;
	    p[0]:=0;
		for i:=1 to n do
		begin 
			p[i]:=p[i-1]+a[i,1];
			if p[i]<0 then 
			begin 
				if vis[-p[i]] then 
				begin 
					vis[-p[i]]:=false;
					inc(tot);
					ans:=ans+i;
				end;
			end;
			if p[i]>0 then 
			begin 
				if vis[w-p[i]+1] then 
				begin 
					vis[w-p[i]+1]:=false;
					inc(tot);
					ans:=ans+i;
				end;
			end;
		end;
		for i:=1 to w do 
		begin
	        if vis[i] then 
			begin 
			    if p[n]<0 then 
				begin 
					ans:=ans+n*(i div (-p[n]));
					inc(left[-i mod (-p[n])]);
				end;
				if p[n]>0 then 
				begin 
					ans:=ans+n*((w-i+1) div p[n]);
					inc(left[(w-i+1) mod p[n]]);
				end;
			end;
		end;
		if (p[n]=0) and (tot<w) then 
		begin
			writeln(-1);
			close(input);
			close(output);
			halt;
		end;
 		for i:=1 to n do 
		begin
			inc(ans,left[p[i]]*i);
			ans:=ans mod 1000000007;
		end;
		ans:=(ans*h) mod 1000000007;
		writeln(ans);
		close(input);
		close(output);
		halt;
	end;
	if h*w<=10000 then 
	begin 
		for i:=1 to h do 
		for j:=1 to w do 
		begin 
			step:=1;
			x:=a[1,1];
			y:=a[1,2];
			while 1=1 do
			begin
				if (x<0) and (x=-j) then 
				begin 
					inc(ans,step);
					break;
				end;
				if (x>0) and (x+j>w) then 
				begin 
					inc(ans,step);
					break;
				end;
				if (y<0) and (y=-i) then 
				begin 
					inc(ans,step);
					break;
				end;
				if (y>0) and (y+i>h) then 
				begin 
					inc(ans,step);
					break;
				end;
				if step+1<=n then
				begin 
					x:=x+a[step,1];
					y:=y+a[step,2];
				end
				else
				begin 
					x:=x+a[step mod n+1,1];
					y:=y+a[step mod n+1,2];
				end;
				inc(step);
			end;
			ans:=ans mod 1000000007;
	    end;
		writeln(ans mod 1000000007);
		close(input);
		close(output);
		halt;
	end;
	writeln(-1);
	close(input);
	close(output);
end. 
program ec12;
var
	a:array[1..300,1..300] of longint;
	ans:array[1..300,0..4] of longint;
	n,m,i,j,total:longint;
procedure print(k:longint);
var 
    i:longint;
begin
	writeln(k);
	for i:=1 to k do
    begin 
		write(ans[i,0],' ');
		write(ans[i,1],' ',ans[i,2],' ',ans[i,3]);
		if ans[i,0]=4 then 
		writeln(ans[i,4])
		else
		writeln;
	end;
	close(input);
	close(output);
	halt;
end;
procedure dfs(step:longint);
var 
    o1,o2,o3,o4,tot:longint;
begin
    tot:=0;
    for o1:=1 to n do 
	for o2:=1 to n do
	begin 
		if a[o1,o2]>=2 then 
		inc(tot);
	end;
	if tot>=n*(n-1) then 
	print(step-1);
	if total>n*n*n*n*n then 
	print(step-1);
	for o1:=1 to n do 
	begin
		for o2:=1 to n do 
		begin
			if (o1=o2) or (a[o1,o2]>=2) then 
			continue;
			for o3:=1 to n do 
			begin 
				if (o1=o3) or (o2=o3) or (a[o1,o3]>=2) or (a[o2,o3]>=2) then 
				continue;
				inc(a[o1,o2]);
				inc(a[o2,o1]);
				inc(a[o2,o3]);
				inc(a[o3,o2]);
				inc(a[o1,o3]);
				inc(a[o3,o1]);
				ans[step,0]:=3;
				ans[step,1]:=o1;
				ans[step,2]:=o2;
				ans[step,3]:=o3;
				dfs(step+1);
				inc(total);
				dec(a[o1,o2]);
				dec(a[o2,o1]);
				dec(a[o2,o3]);
				dec(a[o3,o2]);
				dec(a[o1,o3]);
				dec(a[o3,o1]);
			end;
		end;
	end;
	for o1:=1 to n do 
	begin
		for o2:=1 to n do 
		begin
			if (o1=o2) or (a[o1,o2]=2)  then 
			continue;
			for o3:=1 to n do 
			begin 
				if (o1=o3) or (o2=o3) or (a[o1,o3]>=2) or (a[o2,o3]>=2) then 
				continue;
				for o4:=1 to n do
				begin 
				    if (o1=o4) or (o2=o4) or (o3=o4) or (a[o1,o4]>=2) or (a[o2,o4]>=2) or (a[o3,o4]>=2) then
					continue;
					inc(a[o1,o2]);
					inc(a[o2,o1]);
					inc(a[o2,o3]);
					inc(a[o3,o2]);
					inc(a[o1,o3]);
					inc(a[o3,o1]);
					inc(a[o1,o4]);
					inc(a[o4,o1]);
					inc(a[o2,o4]);
					inc(a[o4,o2]);
					inc(a[o3,o4]);
					inc(a[o4,o3]);
					ans[step,0]:=4;
					ans[step,1]:=o1;
					ans[step,2]:=o2;
					ans[step,3]:=o3;
					ans[step,4]:=o4;
					dfs(step+1);
					inc(total);
					dec(a[o1,o2]);
					dec(a[o2,o1]);
					dec(a[o2,o3]);
					dec(a[o3,o2]);
					dec(a[o1,o3]);
					dec(a[o3,o1]);
					dec(a[o1,o4]);
					dec(a[o4,o1]);
					dec(a[o2,o4]);
					dec(a[o4,o2]);
					dec(a[o3,o4]);
					dec(a[o4,o3]);
				end;
			end;
		end;
	end;
end;
begin 
	assign(input,'core.in'); reset(input);
	assign(output,'core.out'); rewrite(output);
    fillchar(a,sizeof(a),0);
	readln(n);
	if (n<=2) then 
	begin 
		writeln(0);
		close(input);
		close(output);
		halt;
	end;
	if (n=3) then 
	begin 
		writeln(2);
		writeln('3 1 2 3');
		writeln('3 1 2 3');
		close(input);
		close(output);
		halt;
	end;
	if (n=4) then 
	begin 
		writeln(2);
		writeln('4 1 2 3 4');
		writeln('4 1 2 3 4');
		close(input);
		close(output);
		halt;
	end;
	if (n=5) then 
	begin 
		writeln(6);
		writeln('3 5 1 2');
		writeln('3 3 1 5');
		writeln('4 4 5 2 3');
		writeln('4 4 3 2 1');
		writeln('3 4 2 1');
		writeln('3 3 1 5');
		close(input);
		close(output);
		halt;
	end;
	total:=0;
	dfs(1);
	close(input);
	close(output);
end. 
program ec12;
var 
	ans:int64;
	x,y,a:array[1..500000] of longint;
    n,m,i,j:longint;
procedure qsrt(l,r:longint);
var 
	i,j,v,s:longint;
begin 
	i:=l; j:=r; v:=a[(l+r) div 2];
	repeat
	while a[i]<v do inc(i);
	while a[j]>v do dec(j);
	if i<=j then 
	begin
		s:=a[i];
		a[i]:=a[j];
		a[j]:=s;
		inc(i);
		dec(j);
	end;
	until i>=j;
	if l<j then qsrt(l,j);
	if i<r then qsrt(i,r);
end;
begin 
	assign(input,'root.in'); reset(input);
	assign(output,'root.out'); rewrite(output);
	readln(n);
	ans:=0;
	for i:=1 to n do
	read(a[i]);
	for i:=1 to n-1 do 
	readln(x[i],y[i]);
	m:=0;
	for i:=1 to n-1 do 
	begin 
		if x[i]=y[i]-1 then 
		inc(m);
	end;
	if m=n-1 then 
	begin
		for i:=1 to n do 
		ans:=ans+a[i];
	end
	else
	begin 
		qsrt(2,n);
		ans:=ans+a[1]+a[n]+a[n-1]+a[n-2];
	end;
	writeln(ans);
	close(input);
	close(output);
end. 
#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int mp[500][500];
int main(int argc, char const *argv[]) {
	freopen("core.in", "r", stdin);
	int n = read();
	freopen("core.out", "r", stdin);
	int x = read();
	for (int i = 1; i <= x; i++) {
		if (read() == 3) {
			int a = read(), b = read(), c = read();
			mp[a][b]++; mp[b][a]++;
			mp[a][c]++; mp[c][a]++;
			mp[c][b]++; mp[b][c]++;
		} else {
			int a = read(), b = read(), c = read(), d = read();
			mp[a][b]++; mp[b][a]++;
			mp[c][b]++; mp[b][c]++;
			mp[a][d]++; mp[d][a]++;
			mp[c][d]++; mp[d][c]++;
		}
	}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (i != j && mp[i][j] != 2) {
				puts("No");
				return 1;
			}
	puts("Yes");
	return 0;
}
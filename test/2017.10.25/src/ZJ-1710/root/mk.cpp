#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	freopen("root.in", "w", stdout);
	int n = 200000;
	srand(time(0));
	printf("%d\n", n);
	for (int i = 1; i <= n; i++)
		printf("%d ", rand()*rand());
	puts("");
	for (int i = 2; i <= n; i++)
		printf("%d %d\n", i, rand() * rand() % (i - 1) + 1);
	return 0;
}
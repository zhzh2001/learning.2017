// orz szb
// orz lbc
// orz rxd
// orz jyt
// orz zyy
// orz lzh
// orz zz
// orz mdnd
// orz cfb
// orz fb
// orz cym
#include <bits/stdc++.h>
#define N 200020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], cnt;
void insert(int x, int y) {
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
ll f[N][3], a[N];
ll dfs(int x, int fa) {
	ll mx = 0, mn = 0, th = 0, mxp = 0, mnp = 0;
	for (int i = head[x]; i; i = nxt[i]) {
		if (to[i] != fa) {
			ll res = dfs(to[i], x);
			if (res > mx) th = mn, mn = mx, mx = res, mnp = mxp, mxp = to[i];
			else if (res > mn) th = mn, mn = res, mnp = to[i];
			else if (res > th) th = res;
			f[x][1] = max(f[x][1], f[to[i]][1]);
		}
	}
	f[x][0] = mx + a[x];
	f[x][1] = max(f[x][1], mx + mn + a[x]);
	ll mmx = 0, mmn = 0;
	for (int i = head[x]; i; i = nxt[i]) {
		if (to[i] != fa) {
			ll res = f[to[i]][1];
			if (to[i] == mxp) {
				f[x][2] = max(f[x][2], res + mn + th + a[x]);
			} else if (to[i] == mnp) {
				f[x][2] = max(f[x][2], res + mx + th + a[x]);
			} else {
				if (res > mmx) mmn = mmx, mmx = res;
				else if (res > mmn) mmn = res;
			}
		}
	}
	f[x][2] = max(f[x][2], mmx + mx + mn + a[x]);
	ll res = f[mxp][1];
	if (res > mmx) mmn = mmx, mmx = res;
	else if (res > mmn) mmn = res;
	res = f[mnp][1];
	if (res > mmx) mmn = mmx, mmx = res;
	else if (res > mmn) mmn = res;
	f[x][2] = max(f[x][2], mmx + mmn);
	return f[x][0];
}
int main(int argc, char const *argv[]) {
	freopen("root.in", "r", stdin);
	freopen("root.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++)
		a[i] = read();
	for (int i = 1; i < n; i++)
		insert(read(), read());
	dfs(1, 0);
	printf("%lld\n", f[1][2]);
	return 0;
}

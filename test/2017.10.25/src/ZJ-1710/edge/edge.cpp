#include <bits/stdc++.h>
#define mod 1000000007
using namespace std;
const int INF = 1 << 30;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char str[500020];
int dis[1020][1020];
int main(int argc, char const *argv[]) {
	freopen("edge.in", "r", stdin);
	freopen("edge.out", "w", stdout);
	int n = read(), h = read(), w = read();
	scanf("%s", str + 1);
	if (h > 1000 || w > 1000) {
		if (h <= 10000 && w <= 10000)
			printf("%lld\n", 1ll * w * (w + 1) % mod * h % mod);
		else
			puts("-1");
		return 0;
	}
	for (int i = 1; i <= h; i++)
		for (int j = 1; j <= w; j++)
			dis[i][j] = INF;
	int cnt = h * w;
	int lh = 1, rh = h, lw = 1, rw = w;
	for (int x = 1; x <= n; x++) {
		switch (str[x]) {
			case 'L': rw ++, lw ++; break;
			case 'R': rw --, lw --; break;
			case 'U': rh ++, lh ++; break;
			case 'D': rh --, lh --; break;
		}
		for (int i = 1; i <= h; i++)
			for (int j = 1; j <= w; j++) if (i < lh || i > rh || j < lw || j > rw)
				if (dis[i][j] == INF) dis[i][j] = x, cnt --;
		if (!cnt) break;
	}
	if (cnt) {
		if (lh == 1 && rh == h && lw == 1 && rw == w)
			return puts("-1") & 0;
		for (int x = n + 1; x; x++) {
			// printf("==> %d %d %d %d\n", lh, rh, lw, rw);
			switch (str[(x - 1) % n + 1]) {
				case 'L': rw ++, lw ++; break;
				case 'R': rw --, lw --; break;
				case 'U': rh ++, lh ++; break;
				case 'D': rh --, lh --; break;
			}
			// printf("=> %d %d %d %d\n", lh, rh, lw, rw);
			for (int i = 1; i <= h; i++)
				for (int j = 1; j <= w; j++) if (i < lh || i > rh || j < lw || j > rw)
					if (dis[i][j] == INF) dis[i][j] = x, cnt --;
			if (!cnt) break;
		}
	}
	int ans = 0;
	for (int i = 1; i <= h; i++)
		for (int j = 1; j <= w; j++)
			ans = (ans + dis[i][j]) % mod;
	printf("%d\n", ans);
	return 0;
}

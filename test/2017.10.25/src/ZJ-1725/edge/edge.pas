program edge;
 label aaa;
 var
  a:array[0..100001,1..4] of longint;
  sum:array[0..100001] of int64;
  i,j,n,p,h,w,x,y,xl,yl,xr,yr,mdl,mdr,dvl,dvr,skl,skr:longint;
  px,py,ssum,sss:int64;
  s:ansistring;
 begin
  assign(input,'edge.in');
  assign(output,'edge.out');
  reset(input);
  rewrite(output);
  sum[0]:=0;
  for i:=1 to 100000 do
   sum[i]:=(sum[i-1]+i) mod 1000000007;
  ssum:=0;
  readln(n,h,w);
  readln(s);
  fillchar(a,sizeof(a),0);
  x:=0;
  y:=0;
  for i:=1 to length(s) do
   begin
    case s[i] of
     'U':
      begin;
       dec(x);
       if a[i-1][1]<0-x then a[i][1]:=0-x;
      end;
     'L':
      begin
       dec(y);
       if a[i-1][2]<0-y then a[i][2]:=0-y;
      end;
     'D':
      begin
       inc(x);
       if a[i-1][3]<x then a[i][3]:=x;
      end;
     'R':
      begin
       inc(y);
       if a[i-1][4]<y then a[i][4]:=y;
      end;
    end;
    for j:=1 to 4 do
     if a[i][j]<a[i-1][j] then a[i][j]:=a[i-1][j];
   end;
  if (x=0) and (y=0) and ((a[n][1]+a[n][3]+1<=h) or (a[n][2]+a[n][4]+1<=w)) then
   begin
    writeln('-1');
    goto aaa;
   end;
  {for i:=1 to n do
   begin
    for j:=1 to 3 do
     write(a[i][j],' ');
    writeln(a[i][4]);
   end;}
  for i:=n downto 1 do
   for j:=1 to 4 do
    if a[i][j]<>a[i-1][j] then
     case j of
      1:
       begin
        p:=w-a[i][2]-a[i-1][4];
        yl:=a[i-1][2]+1;
        yr:=w-a[i][4];
        if x<0 then px:=(h-a[i][1]) div (0-x)
               else if x>0 then px:=(a[i-1][1]-1) div x
                           else px:=1;
        //writeln(h,' ',a[i][1],' ',x,' ',y,' ',xl,' ',xr);
        if y<0 then
         begin
          mdr:=(w-yr) mod (0-y);
          dvr:=(w-yr) div (0-y);
          skr:=yr-(0-y)+mdr;
          mdl:=(w-yl) mod (0-y);
          dvl:=(w-yl) div (0-y);
          skl:=yl-(0-y)+1+mdl;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvl>=px then
            begin
             sss:=yr-(px-dvr)*y;
             inc(py,(sss-yl+1)*n*sum[px]+(sss-yl+1)*(px+1)*i);
             dvl:=px-1;
             yl:=sss+1;
            end;
           inc(py,sum[(dvr+dvl)*(yr-yl+1) div 2]*n+((dvr+dvl)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
               else if y>0 then
         begin
          mdl:=(yl-1) mod y;
          dvl:=(yl-1) div y;
          skl:=yl+y-mdl;
          mdr:=(yr-1) mod y;
          dvr:=(yr-1) div y;
          skr:=yr+y-mdr-1;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvr>=px then
            begin
             sss:=(px-dvl)*y+yl;
             inc(py,(yr-sss+1)*n*sum[px]+(yr-sss+1)*(px+1)*i);
             dvr:=px-1;
             yr:=sss-1;
            end;
           inc(py,sum[(dvl+dvr)*(yr-yl+1) div 2]*n+((dvl+dvr)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
                           else py:=p*sum[px]*n+(px+1)*i*p;
        ssum:=(ssum+py) mod 1000000007;
        //writeln(mdl,' ',dvl,' ',skl,' ',mdr,' ',dvr,' ',skr);
        //writeln(px,' ',py,' ',yl,' ',yr);
       end;
      2:
       begin
        p:=h-a[i][1]-a[i-1][3];
        yl:=a[i-1][1]+1;
        yr:=h-a[i][3];
        if y<0 then px:=(w-a[i][2]) div (0-y)
               else if y>0 then px:=(a[i-1][2]-1) div y
                           else px:=1;
        //writeln(h,' ',a[i][1],' ',x,' ',y,' ',xl,' ',xr);
        if x<0 then
         begin
          mdr:=(h-yr) mod (0-x);
          dvr:=(h-yr) div (0-x);
          skr:=yr-(0-x)+mdr;
          mdl:=(h-yl) mod (0-x);
          dvl:=(h-yl) div (0-x);
          skl:=yl-(0-x)+1+mdl;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvl>=px then
            begin
             sss:=yr-(px-dvr)*y;
             inc(py,(sss-yl+1)*n*sum[px]+(sss-yl+1)*(px+1)*i);
             dvl:=px-1;
             yl:=sss+1;
            end;
           inc(py,sum[(dvr+dvl)*(yr-yl+1) div 2]*n+((dvr+dvl)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
               else if x>0 then
         begin
          mdl:=(yl-1) mod x;
          dvl:=(yl-1) div x;
          skl:=yl+x-mdl;
          mdr:=(yr-1) mod x;
          dvr:=(yr-1) div x;
          skr:=yr+x-mdr-1;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvr>=px then
            begin
             sss:=(px-dvl)*y+yl;
             inc(py,(yr-sss+1)*n*sum[px]+(yr-sss+1)*(px+1)*i);
             dvr:=px-1;
             yr:=sss-1;
            end;
           inc(py,sum[(dvl+dvr)*(yr-yl+1) div 2]*n+((dvl+dvr)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
                           else py:=p*sum[px]*n+(px+1)*i*p;
        ssum:=(ssum+py) mod 1000000007;
        //writeln(mdl,' ',dvl,' ',skl,' ',mdr,' ',dvr,' ',skr);
        //writeln(px,' ',py,' ',yl,' ',yr);
       end;
      3:
       begin
        p:=w-a[i][2]-a[i-1][4];
        yl:=a[i-1][2]+1;
        yr:=w-a[i][4];
        if x<0 then px:=(h-a[i][3]) div (0-x)
               else if x>0 then px:=(a[i-1][3]-1) div x
                           else px:=1;
        //writeln(h,' ',a[i][1],' ',x,' ',y,' ',xl,' ',xr);
        if y<0 then
         begin
          mdr:=(w-yr) mod (0-y);
          dvr:=(w-yr) div (0-y);
          skr:=yr-(0-y)+mdr;
          mdl:=(w-yl) mod (0-y);
          dvl:=(w-yl) div (0-y);
          skl:=yl-(0-y)+1+mdl;
             if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvl>=px then
            begin
             sss:=yr-(px-dvr)*y;
             inc(py,(sss-yl+1)*n*sum[px]+(sss-yl+1)*(px+1)*i);
             dvl:=px-1;
             yl:=sss+1;
            end;
           inc(py,sum[(dvr+dvl)*(yr-yl+1) div 2]*n+((dvr+dvl)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
               else if y>0 then
         begin
          mdl:=(yl-1) mod y;
          dvl:=(yl-1) div y;
          skl:=yl+y-mdl;
          mdr:=(yr-1) mod y;
          dvr:=(yr-1) div y;
          skr:=yr+y-mdr-1;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvr>=px then
            begin
             sss:=(px-dvl)*y+yl;
             inc(py,(yr-sss+1)*n*sum[px]+(yr-sss+1)*(px+1)*i);
             dvr:=px-1;
             yr:=sss-1;
            end;
           inc(py,sum[(dvl+dvr)*(yr-yl+1) div 2]*n+((dvl+dvr)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
                           else py:=p*sum[px]*n+(px+1)*i*p;
        ssum:=(ssum+py) mod 1000000007;
        //writeln(mdl,' ',dvl,' ',skl,' ',mdr,' ',dvr,' ',skr);
        //writeln(px,' ',py,' ',yl,' ',yr);
       end;
      4:
       begin
        p:=h-a[i][1]-a[i-1][3];
        yl:=a[i-1][1]+1;
        yr:=h-a[i][3];
        if y<0 then px:=(w-a[i][4]) div (0-y)
               else if y>0 then px:=(a[i-1][4]-1) div y
                           else px:=1;
        //writeln(h,' ',a[i][1],' ',x,' ',y,' ',xl,' ',xr);
        if x<0 then
         begin
          mdr:=(h-yr) mod (0-x);
          dvr:=(h-yr) div (0-x);
          skr:=yr-(0-x)+mdr;
          mdl:=(h-yl) mod (0-x);
          dvl:=(h-yl) div (0-x);
          skl:=yl-(0-x)+1+mdl;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvl>=px then
            begin
             sss:=yr-(px-dvr)*y;
             inc(py,(sss-yl+1)*n*sum[px]+(sss-yl+1)*(px+1)*i);
             dvl:=px-1;
             yl:=sss+1;
            end;
           inc(py,sum[(dvr+dvl)*(yr-yl+1) div 2]*n+((dvr+dvl)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
               else if x>0 then
         begin
          mdl:=(yl-1) mod x;
          dvl:=(yl-1) div x;
          skl:=yl+x-mdl;
          mdr:=(yr-1) mod x;
          dvr:=(yr-1) div x;
          skr:=yr+x-mdr-1;
          if dvl=dvr then
           if dvl>px then py:=sum[p*px]*n+p*(px+1)
                     else py:=sum[p*dvl]*n+p*(px+1)
                     else
          begin
           py:=0;
           if dvr>=px then
            begin
             sss:=(px-dvl)*y+yl;
             inc(py,(yr-sss+1)*n*sum[px]+(yr-sss+1)*(px+1)*i);
             dvr:=px-1;
             yr:=sss-1;
            end;
           inc(py,sum[(dvl+dvr)*(yr-yl+1) div 2]*n+((dvl+dvr)*(yr-yl+1) div 2+yr-yl+1)*i);
          end;
         end
                           else py:=p*sum[px]*n+(px+1)*i*p;
        ssum:=(ssum+py) mod 1000000007;
        //writeln(mdl,' ',dvl,' ',skl,' ',mdr,' ',dvr,' ',skr);
        //writeln(px,' ',py,' ',yl,' ',yr);
       end;
     end;
  writeln(ssum);
  aaa:
  close(input);
  close(output);
 end.

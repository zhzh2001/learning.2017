#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define N 500000
#define H 1000
ll n,h,w,ans;
ll x,y,z,mnx,mxx,mny,mxy,xx,yy;
char ch;
char s[N+100];
char f[H+10][H+10];
inline ll read()
{
    ll jzq=0,lzq=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')lzq=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')jzq=jzq*10+ch-'0',ch=getchar();
    return jzq*lzq;
}
ll min(ll a,ll b)
{
	if(a>b)return b;
	return a;
}
ll max(ll a,ll b)
{
	if(a<b)return b;
	return a;
}
int main()
{
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	n=read();h=read();w=read();
	ll mo=1e9+7;
	ll jzq=1e11;
	if(h*w>=jzq)
	{
		puts("-1");
		return 0;
	}
	x=0;y=0;mnx=0;mxx=0;mny=0;mxy=0;
	For(i,1,n)
	{
		while(true)
		{
			ch=getchar();
			if(ch=='U'){x--;mnx=min(mnx,x);break;}
			if(ch=='D'){x++;mxx=max(mxx,x);break;}
			if(ch=='L'){y--;mny=min(mny,y);break;}
			if(ch=='R'){y++;mxy=max(mxy,y);break;}
		}
		s[i]=ch;
	}
	if(x==0&&y==0)
	{
		/*if(mxy>=w||-mny>=w)
		{
			yy=i;z=0;
			while(yy<=w&&yy>0)
			{
				z++;ans++;
				if(s[z]=='L')yy--;
				if(s[z]=='R')yy++;
				z%=n;
			}
		}*/
		puts("-1");
		return 0;
	}
	if(mny==-n)
	{
		ans=(w+1)*w/2;
		ans%=mo;
		ans*=h;
		ans%=mo;
		printf("%lld\n",ans);
		return 0;
	}
	if(mxx==0&&mnx==0)
	{
		For(i,1,w)
		{
			yy=i;z=0;
			while(yy<=w&&yy>0)
			{
				z++;ans++;
				if(s[z]=='L')yy--;
				if(s[z]=='R')yy++;
				z%=n;
			}
		}
		ans=ans%mo;
		ans*=h;
		ans%=mo;
		printf("%lld\n",ans);
		return 0;
	}
	For(i,1,-mnx)
		For(j,1,-mny)
		{
			xx=i;yy=j;z=0;
			while(xx<=h&&xx>0&&yy<=w&&yy>0)
			{
				z++;
				if(s[z]=='U')xx--;
				if(s[z]=='D')xx++;
				if(s[z]=='L')yy--;
				if(s[z]=='R')yy++;
			}
			f[i][j]=z;
		}
	For(i,1,-mnx)
		For(j,1,w-mxy+1)
		{
			xx=i;yy=j;z=0;
			while(xx<=h&&xx>0&&yy<=w&&yy>0)
			{
				z++;
				if(s[z]=='U')xx--;
				if(s[z]=='D')xx++;
				if(s[z]=='L')yy--;
				if(s[z]=='R')yy++;
			}
			f[i][j]=z;
		}
	For(i,1,h-mxx+1)
		For(j,1,-mny)
		{
			xx=i;yy=j;z=0;
			while(xx<=h&&xx>0&&yy<=w&&yy>0)
			{
				z++;
				if(s[z]=='U')xx--;
				if(s[z]=='D')xx++;
				if(s[z]=='L')yy--;
				if(s[z]=='R')yy++;
			}
			f[i][j]=z;
		}
	For(i,1,h-mxx+1)
		For(j,1,w-mxy+1)
		{
			xx=i;yy=j;z=0;
			while(xx<=h&&xx>0&&yy<=w&&yy>0)
			{
				z++;
				if(s[z]=='U')xx--;
				if(s[z]=='D')xx++;
				if(s[z]=='L')yy--;
				if(s[z]=='R')yy++;
			}
			f[i][j]=z;
		}
	if(x<0)
	{
		For(i,1-mnx,h-mxx)
			if(y<0)For(j,1-mny,w-mxy)f[i][j]=f[i+x][j+y]+n;
				else Rep(j,1-mny,w-mxy)f[i][j]=f[i+x][j+y]+n;
	}
	else
	{
		Rep(i,1-mnx,h-mxx)
			if(y<0)For(j,1-mny,w-mxy)f[i][j]=f[i+x][j+y]+n;
				else Rep(j,1-mny,w-mxy)f[i][j]=f[i+x][j+y]+n;
	}
	For(i,1,h)For(j,1,w)ans=(f[i][j]+ans)%mo;
	printf("%lld\n",ans);
	return 0;
}

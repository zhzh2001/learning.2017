#include<bits/stdc++.h>
using namespace std;
int n,h,w,f[2000010][2],x[2000010],y[2000010],xx[2000010],yy[2000010],lsg,ans;
char c[1000000];bool ff;
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int kk=1,k=0;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}inline bool pd(int i,int j,int k){return i+x[k]>0&&i+xx[k]<=h&&j+y[k]>0&&j+yy[k]<=w;}
int main(){
	freopen("edge.in","r",stdin);freopen("edge.out","w",stdout);
	n=read();h=read();w=read();lsg=1e9+7;ff=1;
	for (int i=0;i<n;i++)while (c[i]!='L'&&c[i]!='R'&&c[i]!='U'&&c[i]!='D')c[i]=getchar();
	for (int i=1;i<=2000000;i++){
		char cc=c[(i-1)%n];ff&=cc=='L';
		f[i][0]=f[i-1][0];f[i][1]=f[i-1][1];
		if (cc=='L')f[i][1]--;if (cc=='R')f[i][1]++;
		if (cc=='U')f[i][0]--;if (cc=='D')f[i][0]++;
		x[i]=min(x[i-1],f[i][0]);xx[i]=max(xx[i-1],f[i][0]);
		y[i]=min(y[i-1],f[i][1]);yy[i]=max(yy[i-1],f[i][1]);
	}for (int i=1;i<=h;i++){
		for (int j=1;j<=w;j++){
			int l=0,r=2000000;
			while (l<r){
				int m=(l+r+1)>>1;if (pd(i,j,m))l=m;else r=m-1;
			}if (l==2000000){puts("-1");return 0;}
			(ans+=l+1)%=lsg;
		}if (ff){ans=1ll*ans*h%lsg;break;}
	}
	cout<<ans<<endl;
}

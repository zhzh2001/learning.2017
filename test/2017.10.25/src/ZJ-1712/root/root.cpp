#include<bits/stdc++.h>
using namespace std;
#define int long long
int n,last[200000],a[500000][2],x,y,kk,f0[200000],f1[200000],f2[200000],f3[200000];
bool ff;
int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int kk=1,k=0;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}void dfs1(int d,int fa){
	int x=0,y=0;f1[d]=f0[d];
	for (int i=last[d];i;i=a[i][0])
		if (a[i][1]!=fa){
			dfs1(a[i][1],d);if (f1[a[i][1]]>f1[x])y=x,x=a[i][1];
				else if (f1[a[i][1]]>f1[y])y=a[i][1];
			f2[d]=max(f2[a[i][1]],f2[d]);
		}f1[d]+=f1[x];f2[d]=max(f2[d],f0[d]+f1[x]+f1[y]);
}void doit1(int d,int fa){
	if (ff)return;int x=0,y=0;f1[d]=f0[d];f2[d]=0;
	for (int i=last[d];i;i=a[i][0])
		if (a[i][1]!=fa){
			if (f1[a[i][1]]>f1[x])y=x,x=a[i][1];
				else if (f1[a[i][1]]>f1[y])y=a[i][1];
			f2[d]=max(f2[a[i][1]],f2[d]);
		}f1[d]+=f1[x];f2[d]=max(f2[d],f0[d]+f1[x]+f1[y]);
}void dfs2(int d,int fa){
	int x=0,y=0,z=0,k=0;doit1(fa,d);
	for (int i=last[d];i;i=a[i][0])
		if (!ff||a[i][1]!=fa){
			if (f1[a[i][1]]>f1[x])k=max(k,f2[z]),z=y,y=x,x=a[i][1];
				else if (f1[a[i][1]]>f1[y])k=max(k,f2[z]),z=y,y=a[i][1];
					else if (f1[a[i][1]]>f1[z])k=max(k,f2[z]),z=a[i][1];
						else k=max(k,f2[z]);
		}f3[d]=max(max(k,f2[z])+f1[x]+f1[y],max(max(k,f2[x])+f1[z]+f1[y],max(k,f2[y])+f1[x]+f1[z]))+f0[d];
	for (int i=last[d];i;i=a[i][0])
		if (a[i][1]!=fa){
			dfs2(a[i][1],d);f3[d]=max(f3[d],f3[a[i][1]]);doit1(a[i][1],d);
		}
}void doit(int x,int y){a[++kk][0]=last[x];a[kk][1]=y;last[x]=kk;} 
signed main(){
	freopen("root.in","r",stdin);freopen("root.out","w",stdout);
	n=read();for (int i=1;i<=n;i++)f0[i]=read();ff=1;
	for (int i=1;i<n;i++)x=read(),y=read(),doit(x,y),doit(y,x),ff&=x==1||y==1;
	dfs1(1,0);dfs2(1,0);cout<<f3[1]<<endl;
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/

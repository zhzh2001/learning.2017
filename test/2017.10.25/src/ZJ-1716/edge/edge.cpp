#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <functional>
#include <string>
#include <map>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("edge.in");
ofstream fout("edge.out");

const int kP = 1e9 + 7, kM = 5e5 + 5, kInf = 0x3f3f3f3f;
const int dx[] = { 0, 0, -1, 1 }, dy[] = { -1, 1, 0, 0 };

int n, h, w;
string s;

int GetOpt(char ch) {
  switch(ch) {
  case 'L':
    return 0;
  case 'R':
    return 1;
  case 'U':
    return 2;
  case 'D':
    return 3;
  }
  return -1;
}

int mx = 0, my = 0;

map<pii, int> mp;

bool inside(int x, int y) {
  return x > 0 && y > 0 && x <= h && y <= w;
}

bool vis[4];

int main() {
  fin >> n >> h >> w >> s;
  mp[make_pair(0, 0)] = 0;
  for (int i = 0; i < s.length(); ++i) {
    int d = GetOpt(s[i]);
    vis[d] = true;
    mx += dx[d], my += dy[d];
    if (!mp[make_pair(mx, my)])
      mp[make_pair(mx, my)] = i + 1;
  }
  if (mx == 0 && my == 0) {
    fout << -1 << endl;
    return 0;
  }
  if (vis[0] && !vis[1] && !vis[2] && !vis[3]) {
    ll ans = 0;
    for (int i = 1; i <= w; ++i)
      (ans = ans + i) % kP;
    fout << (ll)(ans * h) % kP << endl;
    return 0;
  }
  if (vis[0] && !vis[1] && !vis[2] && vis[3]) {
    ll ans = 0, ph = h, pw = w, c = 0;
    for (int i = 0; i < n && ph && pw; ++i) {
      if (s[i] == 'L') {
        (ans = ans + (ll)ph * (++c)) % kP;
        --pw;
      } else if (s[i] == 'D') {
        (ans = ans + (ll)pw * (++c)) % kP;
        --ph; 
      }
      if (i == n - 1)
        i = -1;
    }
    fout << ans << endl;
    return 0;
  }
  if (h * w <= 1e7) {
    ll ans = 0;
    for (int i = 1; i <= h; ++i) {
      for (int j = 1; j <= w; ++j) {
        int tx = i, ty = j, k;
        ll tmp = 0;
        for (int k = 0; k < n; ++k) {
          tx += dx[GetOpt(s[k])], ty += dy[GetOpt(s[k])];
          ++tmp;
          if (!inside(tx, ty))
            break;
          if (k == n - 1)
            k = -1;
        }
        ans = (ans + tmp) % kP;
//        cerr << tmp << ' ';
      }
//      cerr << endl;
    }
    fout << ans << endl;
    return 0;
  }
  return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <functional>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("core.in");
ofstream fout("core.out");

int n;

int main() {
  fin >> n;
  if (n == 3) {
    fout << "2" << endl;
    fout << "3 1 2 3" << endl;
    fout << "3 1 2 3" << endl;
  }
  if (n == 4) {
    fout << "3" << endl;
    fout << "3 1 2 3" << endl;
    fout << "3 2 3 4" << endl;
    fout << "4 1 2 4 3" << endl;
  }
  return 0;
}

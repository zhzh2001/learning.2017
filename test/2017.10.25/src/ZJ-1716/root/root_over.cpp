#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <functional>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("root.in");
ofstream fout("root.out");

const int kN = 1e5 + 5, kInf = 0x3f3f3f3f;

struct Edge { int to, nxt; } e[kN << 1];

int n, a[kN], head[kN], cnt, fa[kN], f[kN];

void AddEdge(int u, int v) {
  e[++cnt].to = v;
  e[cnt].nxt = head[u];
  head[u] = cnt;
}

int vis[kN], sl[kN];
ll ans;
int p[kN], pc, pos;

int te;

void dfs1(int x, int dis = 0) {
  vis[x] = te;
  for (int i = head[x]; i; i = e[i].nxt) {
    int nxt = e[i].to;
    if (vis[nxt] != te) {
      fa[nxt] = x;
      dfs1(nxt, dis + a[x]);
    }
  }
  if (dis + a[x] > ans) {
    pos = x;
    ans = dis + a[x];
  }
  vis[x] = 0;
}

ll ans1 = 0;

void dfs2(int x, int dis = 0) {
  if (sl[x])
    return;
  vis[x] = te;
  for (int i = head[x]; i; i = e[i].nxt) {
    int nxt = e[i].to;
    if (vis[nxt] != te && !sl[nxt]) {
      fa[nxt] = x;
      dfs2(nxt, dis + a[x]);
    }
  }
  if (dis + a[x] > ans1) {
    ans1 = dis + a[x];
  }
  vis[x] = 0; 
}

int main() {
  fin >> n;
  for (int i = 1; i <= n; ++i)
    fin >> a[i];
  bool flag = true, flag1 = true;
  for (int i = 1; i < n; ++i) {
    int u, v;
    fin >> u >> v;
    if (v != u + 1)
      flag = false;
    if (u > 1)
      flag = false;
    AddEdge(u, v);
    AddEdge(v, u);
  }
  if (flag) {
    ll sum = 0;
    for (int i = 1; i <= n; ++i)
      sum += a[i];
    fout << sum << endl;
    return 0;
  }
  if (flag1) {
    ll sum = 0;
    sort(a + 2, a + n + 1, greater<int>());
    fout << (ll)(a[2] + a[3] + a[4] + a[1]) << endl;
    return 0;
  }
  ll res = 0;
  for (int i = 1; i <= n; ++i) {
    memset(sl, 0x00, sizeof sl);
    memset(fa, 0x00, sizeof fa);
    fa[i] = 0;
    ans = 0;
    ++te;
    dfs1(i);
    int x = pos;
    while (x != 0) {
      sl[x] = true;
      x = fa[x];
    }
    for (int j = 1; j <= n; ++j) {
      ans1 = 0;
      ++te;
      dfs2(j);
      res = max(res, ans + ans1);
    }
  }
  fout << res << endl;
  return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <climits>
#include <string>
#include <ctime>
#include <set>
#include <map>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int w,p;}dx[500010],dy[500010];
bool operator <(ppap a,ppap b){return a.w==b.w?a.p<b.p:a.w<b.w;}
int n,h,w,maxx=0,maxy=0,minx=0,miny=0,ans=0,zx,zy;
char s[500010];
signed main()
{
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	n=read();h=read();w=read();
	scanf("%s",s+1);
	int x=0,y=0;
	bool flag=1;
	for(int i=1;i<=n;i++){
		if(s[i]=='U')x--,flag=0;
		if(s[i]=='D')x++,flag=0;
		if(s[i]=='L')y--;
		if(s[i]=='R')y++,flag=0;
		dx[i]=(ppap){x,i};dy[i]=(ppap){y,i};
		maxx=max(maxx,x);maxy=max(maxy,y);
		minx=min(minx,x);miny=min(miny,y);
		if(i==n)zx=x,zy=y;
	}
	if(flag){
		for(int i=1;i<=h;i++)ans=(ans+w*(w+1)/2)%MOD;
		return printf("%lld",ans)&0;
	}
	sort(dx+1,dx+n+1);sort(dy+1,dy+n+1);
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++){
			int c=1e9;
			if(i+maxx>h)c=0;
			if(i+minx<1)c=0;
			if(j+maxy>w)c=0;
			if(j+miny<1)c=0;
			if(zx<0)c=min(c,(i+minx)/(-zx));
			if(zx>0)c=min(c,(h-maxx-i+1)/zx);
			if(zy<0)c=min(c,(j+miny)/(-zy));
			if(zy>0)c=min(c,(w-maxy-j+1)/zy);
			if(c==1e9)return puts("-1")&0;
			ans=(ans+c*n)%MOD;
			x=i+c*zx;y=j+c*zy;
			int Mix=lower_bound(dx+1,dx+n+1,(ppap){-x,0})-dx;
			int Max=lower_bound(dx+1,dx+n+1,(ppap){h-x+1,0})-dx;
			int Miy=lower_bound(dy+1,dy+n+1,(ppap){-y,0})-dy;
			int May=lower_bound(dy+1,dy+n+1,(ppap){w-y+1,0})-dy;
			if(Mix==1&&-x<dx[1].w)Mix=1e9;else Mix=dx[Mix].p;
			if(Max==n+1)Max=1e9;else Max=dx[Max].p;
			if(Miy==1&&-y<dy[1].w)Miy=1e9;else Miy=dy[Miy].p;
			if(May==n+1)May=1e9;else May=dy[May].p;
			ans=(ans+min(Mix,min(Max,min(Miy,May))))%MOD;
		}
	printf("%lld",ans);
	return 0;
}

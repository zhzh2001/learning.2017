#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <climits>
#include <string>
#include <ctime>
#include <set>
#include <map>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int x,p;};
bool operator <(ppap a,ppap b){return a.x>b.x;}
int n,a[100010],fa[100010],f[100010][2],ans;
int nedge=0,p[200010],nex[200010],head[200010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x){
	f[x][0]=f[x][1]=a[x];
	vector<int>v;int sum=0;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa[x]){
		sum++;fa[p[k]]=x;
		dfs(p[k]);
		v.push_back(f[p[k]][1]);
		f[x][0]=max(f[x][0],f[p[k]][0]);
	}
	if(sum>1){
		sort(v.begin(),v.end());int X=v[sum-1],Y=v[sum-2];
		f[x][0]=max(f[x][0],X+Y+a[x]);
		f[x][1]=max(X,Y)+a[x];
	}
	if(sum==1){
		f[x][0]=max(f[x][0],v[0]+a[x]);f[x][1]=v[0]+a[x];
	}
}
inline void dfss(int x,int s,int ma){
	ans=max(ans,s+ma);
	vector<ppap>v;int sum=0;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa[x])sum++,v.push_back((ppap){f[p[k]][0],p[k]});
	if(!sum)return;
	sort(v.begin(),v.end());
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa[x]){
		int Ma=ma;if(p[k]!=v[0].p)Ma=max(Ma,v[0].x);
		else if(sum>1)Ma=max(Ma,v[1].x);
		dfss(p[k],s+a[p[k]],Ma);
	}
}
signed main()
{
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n=read();
	bool flag1=1,flag2=1;
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		if(y!=x+1&&x!=y+1)flag1=0;
		if(x!=1&&y!=1)flag2=0;
		addedge(x,y);addedge(y,x);
	}
	if(flag1){
		int sum=0;
		for(int i=1;i<=n;i++)sum+=a[i];
		return printf("%lld",sum)&0;
	}
	if(flag2){
		int sum=a[1];
		sort(a+2,a+n+1);
		sum+=a[n]+a[n-1]+a[n-2];
		return printf("%lld",sum)&0;
	}
	for(int i=1;i<=n;i++){
		fa[i]=0;dfs(i);dfss(i,a[i],0);
	}
	printf("%lld",ans);
	return 0;
}

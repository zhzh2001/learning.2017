#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <climits>
#include <string>
#include <ctime>
#include <set>
#include <map>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n;
int main()
{
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	n=read();
	if(n<3)return puts("0")&0;
	if(n==3){
		printf("2\n3 1 2 3\n3 1 2 3 ");
	}
	if(n==4){
		printf("4\n3 1 2 3\n3 1 2 4\n3 1 3 4\n3 2 3 4 ");
	}
	if(n==5){
		printf("6\n4 1 2 3 4\n4 1 2 3 4\n3 1 3 5\n3 1 3 5\n3 2 4 5\n3 2 4 5\n");
	}
	if(n==6){
		printf("9\n4 1 2 3 4\n4 1 2 3 5\n4 1 3 4 5\n3 1 3 6\n3 1 4 6\n3 2 4 5\n3 2 4 6\n3 2 5 6\n3 3 5 6 ");
	}
	if(n==7){
		printf("13\n4 1 2 3 4\n4 1 2 3 5\n4 1 3 4 5\n3 1 3 6\n3 1 4 7\n3 1 6 7\n3 2 4 6\n3 2 4 7\n3 2 5 6\n3 2 5 7\n3 3 5 7\n3 3 6 7\n3 4 5 6");
	}
	return 0;
}

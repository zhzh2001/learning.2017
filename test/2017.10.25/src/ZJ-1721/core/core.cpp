#include<bits/stdc++.h>
using namespace std;
int n;
int a[205][205],cnt;
struct data
{
	int x,y,z,w;
}q[10005];
inline int pd()
{
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
			if (a[i][j]!=2&&i!=j)
				return 0;
	return 1;
}
inline int pd(int i)
{
	for (int j=1;j<=n;++j)
			if (a[i][j]!=2&&i!=j)
				return 0;
	return 1;
}
inline void sc()
{
	cout<<cnt<<'\n';
	for (int i=1;i<=cnt;++i)
	{
		if (q[i].w==0)
			printf("3 %d %d %d\n",q[i].x,q[i].y,q[i].z);
		else
			printf("4 %d %d %d %d\n",q[i].x,q[i].y,q[i].z,q[i].w);
	}
	exit(0);
}
inline void add(int x,int y,int z,int w)
{
	a[x][y]+=w;
	a[y][x]+=w;
	a[x][z]+=w;
	a[z][x]+=w;
	a[y][z]+=w;
	a[z][y]+=w;
}
inline void add(int x,int y,int z,int w,int v)
{
	a[x][y]+=v;
	a[y][x]+=v;
	a[y][z]+=v;
	a[z][y]+=v;
	a[z][w]+=v;
	a[w][z]+=v;
	a[x][w]+=v;
	a[w][x]+=v;
}
void dfs(int x)
{
	if (pd())
		sc();
	while (pd(x))
		++x;
	for (int i=1;i<=n;++i)
		if (i!=x&&a[x][i]!=2)
			for (int j=1;j<=n;++i)
				if (j!=i&&j!=x&&a[x][j]!=2&&a[i][j]!=2)
				{
					add(x,i,j,1);
					q[++cnt]={x,i,j,0};
					dfs(x);
					--cnt;
					add(x,i,j,-1);
				}
	for (int i=1;i<=n;++i)
		if (i!=x&&a[x][i]!=2)
			for (int j=1;j<=n;++i)
				if (j!=i&&j!=x&&a[i][j]!=2)
					for (int k=1;k<=n;++k)
						if (k!=i&&k!=j&&k!=x&&a[j][k]!=2&&a[x][k]!=2)
							{
								add(x,i,j,k,1);
								q[++cnt]={x,i,j,0};
								dfs(x);
								--cnt;
								add(x,i,j,k,-1);
							}
}
int main()
{
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	cin>>n;
	if (n==3)
	{
		puts("2");
		puts("3 1 2 3");
		puts("3 1 2 3");
		return 0;
	}
	if (n==4)
	{
		puts("4");
		puts("3 1 2 3");
		puts("3 1 2 4");
		puts("3 1 3 4");
		puts("3 2 3 4");
		return 0;
	}
	if (n==5)
	{
		puts("6");
		puts("3 5 4 2");
		puts("3 3 1 5");
		puts("4 4 5 2 3");
		puts("4 4 3 2 1");
		puts("3 4 2 1");
		puts("3 3 1 5");
		return 0;
	}
	if (n==6)
	{
		puts("10");
		puts("3 1 2 4");
		puts("3 1 3 5");
		puts("3 1 4 6");
		puts("3 1 5 6");
		puts("3 2 5 6");
		puts("3 3 4 6");
		puts("3 2 4 5");
		puts("3 3 4 5");
		puts("3 1 2 3");
		puts("3 2 3 6");
		return 0;
	}
	dfs(1);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=500005,mod=1e9+7;
char c[N];
int n,h,w,u,d,l,r,x,y;
int size()
{
	return (ll)((w+r)-l+1)*((h+d)-u+1)%mod;
}
/*struct data
{
	ll x,y;
	void operator+=(const ll &a)
	{
		y+=a;
		x+=y/mod;
		y%=mod;
	}
}*/ll a[N];
void change(int i)
{
	if (c[i]=='L')
		++y;
	if (c[i]=='R')
		--y;
	if (c[i]=='U')
		++x;
	if (c[i]=='D')
		--x;
	d=min(d,x);
	u=max(u,x);
	l=max(l,y);
	r=min(r,y);
}
void sc(int x)
{
	int sum=0;
	for (int i=1;i<=x;++i)
		(sum+=a[i])%=mod;
	cout<<sum;
	exit(0);
}
int main()
{
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	scanf("%d%d%d",&n,&h,&w);
	--h;
	--w;
	scanf("%s",c);
	for (int i=1;i<=10000;++i)
	{
		for (int j=0;j<n;++j)
		{
			int k=size();
			if (k==0)
				sc(i);
			(a[i]+=k)%=mod;
			change(j);
		}
		if (x==0&&y==0)
		{
			puts("-1");
			return 0;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
#define ll long long
void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=100005;
int to[N*2],pr[N*2],la[N],PF[N],p[N],pf[N],pg[N],a[N],n,x,y,cnt;
ll f[N],g[N],F[N],G[N],ans[N];
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
void dfs(int x,int fa)
{
	f[x]=g[x]=F[x]=ans[x]=a[x];
	G[x]=0;
	p[x]=PF[x]=x;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			dfs(to[i],x);
			if (f[to[i]]+a[x]>f[x])
			{
				p[x]=to[i];
				g[x]=f[x];
				f[x]=f[to[i]]+a[x];
				pg[x]=pf[to[i]];
				pf[x]=to[i];
			}
			else
			{
				if (f[to[i]]+a[x]>g[x])
				{
					g[x]=f[to[i]]+a[x];
					pg[x]=to[i];
				}
			}
			if (F[to[i]]>F[x])
			{
				PF[x]=PF[to[i]];
				G[x]=F[x];
				F[x]=F[to[i]];
			}
			else
			{
				if (F[to[i]]>G[x])
					G[x]=F[to[i]];
			}
			ans[x]=max(ans[x],ans[to[i]]);
		}
	ans[x]=max(ans[x],F[x]+G[x]);
	ll S=f[x]+g[x]-a[x],k=0;
	if (S>F[x])
	{
		PF[x]=x;
		G[x]=F[x];
		F[x]=S;
	}
	else
	{
		if (S>G[x])
			G[x]=S;
	}
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			if (p[PF[to[i]]]!=p[pf[x]]&&p[PF[to[i]]]!=p[pg[x]])
				k=max(k,F[to[i]]);
		}
	ans[x]=max(ans[x],S+k);
}
void dfs1(int x,int fa)
{
	f[x]=g[x]=ans[x]=a[x];
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			dfs1(to[i],x);
			if (f[to[i]]+a[x]>f[x])
			{
				g[x]=f[x];
				f[x]=f[to[i]]+a[x];
			}
			else
			{
				if (f[to[i]]+a[x]>g[x])
					g[x]=f[to[i]]+a[x];
			}
			ans[x]=max(ans[x],ans[to[i]]);
		}
	ans[x]=max(ans[x],f[x]+g[x]-a[x]);
}
int main()
{
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	srand(time(NULL));
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	if (n>5000)
	{
		dfs(1,0);
		ll sum=ans[1];
		cout<<sum;
	}
	else
	{
		ll sum=0;
		for (int i=1;i<n;++i)
		{
			int u=to[i*2-1],v=to[i*2];
			dfs1(u,v);
			dfs1(v,u);
			sum=max(sum,ans[u]+ans[v]);
		}
		cout<<sum;
	}
	return 0;
}
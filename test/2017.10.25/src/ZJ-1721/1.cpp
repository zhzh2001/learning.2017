#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define mp make_pair
#define lb(x) ((x)&(-x))
#define iter iterator
#define max(x,y) (x)>(y)?:(x):(y);
void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}

#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define N 500002
#define MOD 1000000007
#define INF 2147483647

#define Dw printf

typedef long long ll;
char ch;
int n,h,w,c[N][4],mx[4],x,y,U,D=1,L=2,R=3,th[N],tw[N],tmp,e,ph,pw;
ll ans;

inline void read(char &c){do{c=getchar();}while(c<'A'||'Z'<c);}
inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-48;ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	read(n),read(h),read(w);
	for(int i=1;i<=h;i++)th[i]=INF;
	for(int i=1;i<=w;i++)tw[i]=INF;
	for(int i=1;i<=n;i++)
	{
		read(ch);
		if(ch=='U')
		{
			x--;
			if(x<0&&!c[-x][U])c[-x][U]=i,mx[U]=max(mx[U],-x);
		}
		else if(ch=='D')
		{
			x++;
			if(x>0&&!c[x][D])c[x][D]=i,mx[D]=max(mx[D],x);
		}
		else if(ch=='L')
		{
			y--;
			if(y<0&&!c[-y][L])c[-y][L]=i,mx[L]=max(mx[L],-y);
		}
		else
		{
			y++;
			if(y>0&&!c[-y][R])c[y][R]=i,mx[R]=max(mx[R],y);
		}
	}
	if(x<0)x=-x,swap(U,D);
	if(y<0)y=-y,swap(L,R);
	if(x==0&&y==0&&mx[U]<h-mx[D]&&mx[L]<w-mx[R]){printf("-1");fclose(stdin);fclose(stdout);return 0;}
	for(int i=1;i<=mx[U];i++)th[i]=c[i][U];
	for(int i=h-mx[D]+1;i<=h;i++)th[i]=c[h-i+1][D];
	if(x!=0)
	{
		e=h-mx[D];
		for(int i=mx[U]+1;i<=e;i++)
		{
			tmp=ceil((double(h-i+1-mx[D]))/x);
			th[i]=tmp*n+c[h-i+1-tmp*x][D];
		}
	}
	for(int i=1;i<=mx[L];i++)tw[i]=c[i][L];
	for(int i=w-mx[R]+1;i<=w;i++)tw[i]=c[w-i+1][R];
	if(y!=0)
	{
		e=w-mx[R];
		for(int i=mx[L]+1;i<=e;i++)
		{
			tmp=ceil((double(w-i+1-mx[R]))/y);
			tw[i]=tmp*n+c[w-i+1-tmp*y][R];
		}
	}
	sort(th+1,th+h+1);
	sort(tw+1,tw+w+1);
	ph=pw=1;
	while(ph<=h&&pw<=w)
	{
		if(th[ph]<=tw[pw])
		{
			ans=(ans+(1ll*th[ph]*(w-pw+1)%MOD))%MOD;
			ph++;
		}
		else
		{
			ans=(ans+(1ll*tw[pw]*(h-ph+1)%MOD))%MOD;
			pw++;
		}
	}
	cout<<ans;
	fclose(stdin);fclose(stdout);
	return 0;
}

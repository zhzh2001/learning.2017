#include <cstdio>

typedef long long ll;
char ch;
int n;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-48;ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	read(n);
	if(n==1)printf("0");
	else if(n==3)printf("2\n3 1 2 3\n3 1 2 3");
	else if(n==4)printf("4\n3 1 2 3\n3 2 3 4\n3 3 4 1\n3 4 1 2");
	else if(n==5)printf("6\n3 5 4 2\n3 3 1 5\n4 4 5 2 3\n4 4 3 2 1\n3 4 2 1\n3 3 1 5");
	else if(n==6)printf("9\n3 1 2 3\n3 2 3 4\n3 3 4 5\n3 4 5 6\n3 5 6 1\n3 6 1 2\n4 1 3 6 4\n4 2 4 1 5\n4 2 6 3 5");
	fclose(stdin);fclose(stdout);
	return 0;
}

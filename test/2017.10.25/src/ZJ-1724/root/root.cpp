#include <cstdio>
#include <set>
#include <algorithm>
#include <iostream>
using namespace std;

#define N 200002
#define E 400002

typedef long long ll;
struct Edge{int y,t;}e[E];
bool vis[N];
char ch;
int n,h[N],ep,w[N],x,y;
ll f[N],tmp,ans;
multiset<ll> Q;
multiset<ll>::reverse_iterator it;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-48;ch=getchar();}while('0'<=ch&&ch<='9');}
inline void addedge(const int &x,const int &y){e[++ep].y=y;e[ep].t=h[x];h[x]=ep;}

void dfs(const int &x)
{
	ll mx=0;
	vis[x]=true;
	f[x]=w[x];
	for(int i=h[x];i;i=e[i].t)
		if(!vis[e[i].y])
		{
			dfs(e[i].y);
			mx=max(mx,f[e[i].y]);
		}
	f[x]+=mx;
	vis[x]=false;
}

int main(void)
{
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	read(n);
	for(int i=1;i<=n;i++)read(w[i]);
	for(int i=1;i<n;i++)read(x),read(y),addedge(x,y),addedge(y,x);
	for(int i=1;i<=n;i++)
	{
		Q.clear();
		vis[i]=true;
		for(int j=h[i];j;j=e[j].t)
		{
			dfs(e[j].y);
			Q.insert(f[e[j].y]);
		}
		tmp=w[i];
		if(Q.size()<3)
			for(it=Q.rbegin();it!=Q.rend();it++)tmp+=*it;
		else
		{
			it=Q.rbegin();
			for(int i=1;i<=3;i++)tmp+=*it,it++;
		}
		ans=max(ans,tmp);
		vis[i]=false;
	}
	cout<<ans;
	fclose(stdin);fclose(stdout);
	return 0;
}

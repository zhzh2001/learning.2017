#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=305;

int n,cnt3,cnt4;
int Dat[N][N];
int main(){
	n=read();int cnt=n*(n-1);
	Rep(i,0,cnt/3) if ((cnt-3*i)%4==0){
		cnt3=i,cnt4=(cnt-3*i)/4;;
	}
	Rep(i,1,n) Rep(j,1,n) if (i!=j) Dat[i][j]=2;
	printf("cnt3=%d cnt4=%d\n",cnt3,cnt4);
	Rep(i,1,n){
		if (cnt3==0) break;
		while (true){
			bool flag=false;
			Rep(j,i+1,n) Rep(k,j+1,n) if (Dat[i][j] && Dat[j][k] && Dat[k][i]){
				flag=true;
				Dat[i][j]--;Dat[j][k]--;Dat[k][i]--;cnt3--;
				Dat[j][i]--;Dat[k][j]--;Dat[i][k]--;
			}
			if (cnt3==0) break;
			if (flag==false) break;
		}
	}
	Rep(i,1,n) Rep(j,1,n) if (Dat[i][j]!=0) printf("%d %d res=%d\n",i,j,Dat[i][j]);
	printf("cnt3=%d\n",cnt3);
}

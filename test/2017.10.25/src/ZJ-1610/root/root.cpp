#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;

int n,a[N],x[N],y[N];
int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
int Use[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int f[N],res[2];
void Dfs(int u,int father,int t){
	int max1=0,max2=0;
	for (int i=front[u],v;i!=0;i=E[i].next) if (!Use[i]){
		if ((v=E[i].to)!=father){
			Dfs(v,u,t);
			if (f[v]>max1) max1=f[v];else
			if (f[v]>max2) max2=f[v];
			f[u]=max(f[u],f[v]+a[u]);
		}
	}
	f[u]=max(f[u],a[u]);
	res[t]=max(res[t],max1+max2+a[u]);
}
inline bool Cmp(int a,int b){
	return a>b;
}
int main(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n=read();
	Rep(i,1,n) a[i]=read();
	bool flag=true;
	Rep(i,1,n-1){
		x[i]=read(),y[i]=read();
		if (x[i]>1) flag=false;
		Addline(x[i],y[i]);Addline(y[i],x[i]);
	}
	if (flag){
		sort(a+2,a+n+1,Cmp);
		printf("%d\n",a[2]+a[3]+a[4]+a[1]);
		return 0;
	}
	int Ans=0;
	Rep(i,1,n-1){
		Use[i*2]=1;
		Use[i*2-1]=1;
		res[0]=res[1]=0;
		Rep(j,1,n) f[j]=0;
		Dfs(x[i],0,0);
		Dfs(y[i],0,1);
		Ans=max(Ans,res[0]+res[1]);
//		printf("%d %d\n",res[0],res[1]);
		Use[i*2]=0;
		Use[i*2-1]=0;
	}
	printf("%d\n",Ans);
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
1 3
1 4
1 5
1 6
1 7
1 8
1 9
*/

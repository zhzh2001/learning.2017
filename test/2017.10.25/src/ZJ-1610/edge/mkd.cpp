#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int n,h,w;
int main(){
	freopen("edge.in","w",stdout);
	srand(time(0));
	Rep(i,1,10) rand();
	printf("%d %d %d\n",n=100,h=100,w=100);
	Rep(i,1,n){
		int opt=rand()%4;
		if (opt==0) putchar('L');
		if (opt==1) putchar('R');
		if (opt==2) putchar('U');
		if (opt==3) putchar('D');
	}
}

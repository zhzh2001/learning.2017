#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=500005;
const int Mod=1e9+7;

int n,h,w,t[N],Dat[105][105][105];
int Walk(int x,int y,int pos){
//	printf("x=%d y=%d pos=%d\n",x,y,pos);
	if (Dat[x][y][pos]==-1) return Dat[x][y][pos]=-1;
	if (Dat[x][y][pos]) return Dat[x][y][pos];
	Dat[x][y][pos]=-1;
	int res=0;
	int tx=x,ty=y;
	if (t[pos]=='L') ty--;
	if (t[pos]=='R') ty++;
	if (t[pos]=='U') tx++;
	if (t[pos]=='D') tx--;
	if (min(tx,ty)<=0 || tx>h || ty>w) return Dat[x][y][pos]=1;
	int tpos=pos+1;
	if (tpos>n) tpos=1;
	res=Walk(tx,ty,tpos);
	if (res==-1) return Dat[x][y][pos]=-1;
		else return Dat[x][y][pos]=(res+1)%Mod;
}
int Only_L(){
	Rep(i,1,n) if (t[i]!='L') return 0;
	int Ans=1ll*(w+1)*w/2*h%Mod;
	printf("%d\n",Ans);
	return 1;
}
int INF,Lef[N],Rit[N],Val[N];
inline int find(int u,int del){
	if (Lef[u]!=INF || Rit[u]!=INF) return min(Lef[u],Rit[u]);
	if (Val[u]!=0) return Val[u];
	if (del==0) return -1;
	return Val[u]=(find(u+del,del)+n)%Mod;
}
int Only_L_R(){
	Rep(i,1,n) if (t[i]!='L' && t[i]!='R') return 0;
	int del=0;
	memset(Lef,127,sizeof(Lef));
	memset(Rit,127,sizeof(Rit));
	INF=Lef[0];
	Rep(i,1,n){
		if (t[i]=='L') del--;
		if (t[i]=='R') del++;
		if (del<0) Lef[-del]=min(Lef[-del],i);
		if (del>0) Rit[w-del+1]=min(Rit[w-del+1],i);
	}
	ll Ans=0;
	Rep(i,1,w){
		if (Lef[i]==INF && Rit[i]==INF){
			if (find(i,del)==-1){
				return puts("-1"),1;
			}
			Ans=(Ans+find(i,del))%Mod;
		}
		else Ans=(Ans+min(Lef[i],Rit[i]))%Mod;
	}
	printf("%d\n",1ll*Ans*h%Mod);
	return 1;
}
int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.ans","w",stdout);
	n=read(),h=read(),w=read();
	Rep(i,1,n){
		int ch=getchar();
		while (ch!='L' && ch!='R' && ch!='U' && ch!='D') ch=getchar();
		t[i]=ch;
	}
	if (Only_L()) return 0;
	if (Only_L_R()) return 0;
	int Ans=0;
	Rep(x,1,h) Rep(y,1,w){
		int res;
		if ((res=Walk(x,y,1))==-1) return puts("-1"),0;
			else Ans=(Ans+res)%Mod;
	}
	printf("%d\n",Ans);
}
/*
7 14 92
RLLLRLL
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=500005;
const int Mod=1e9+7;
const int Limit=N*2;

int n,h,w,t[N];
ll INF,Lef[N],Rit[N],Val[N];
ll find(int u,int del){
//	printf("u=%d del=%d\n",u,del);
	if (Lef[u]!=INF || Rit[u]!=INF) return Val[u]=min(Lef[u],Rit[u]);
	if (Val[u]!=0) return Val[u];
	if (del==0) return Val[u]=INF;
	return Val[u]=(find(u+del,del)+n);
}
ll Val1[N],Val2[N],Q[N*2];
int P1[N],P2[N];

struct Fenwick_Tree{
	ll v[N*2];
	inline int lowbit(int val){
		return val&(-val);
	}
	inline void Add(int pos,ll val){
		for (int k=pos;k<=*Q;k+=lowbit(k)){
			v[k]=(v[k]+val)%Mod;
		}
	}
	inline ll Ask(int pos){
		ll res=0;
		for (int k=pos;k>0;k-=lowbit(k)){
			res=(res+v[k])%Mod;
		}
		return res;
	}
	inline ll Query(int x,int y){
		ll res=(Ask(y)-Ask(x-1))%Mod;
		if (res<0) res+=Mod;
		return res;
	}
}Cnt,Sum;
int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	n=read(),h=read(),w=read();
	int tx=0,ty=0;
	Rep(i,1,n){
		int ch=getchar();
		while (ch!='L' && ch!='R' && ch!='U' && ch!='D') ch=getchar();
		t[i]=ch;
	}
	memset(Lef,127,sizeof(Lef));
	memset(Rit,127,sizeof(Rit));
	memset(Val,0,sizeof(Val));
	INF=Lef[0];
	Rep(i,1,n){
		if (t[i]=='L') ty--;
		if (t[i]=='R') ty++;
		if (ty<0) Lef[-ty]=min(Lef[-ty],1ll*i);
		if (ty>0) Rit[w-ty+1]=min(Rit[w-ty+1],1ll*i);
	}
	Rep(i,1,w) find(i,ty);
	Rep(i,1,w) Val1[i]=Val[i];
	memset(Lef,127,sizeof(Lef));
	memset(Rit,127,sizeof(Rit));
	memset(Val,0,sizeof(Val));
	INF=Lef[0];
	Rep(i,1,n){
		if (t[i]=='U') tx--;
		if (t[i]=='D') tx++;
		if (tx<0) Lef[-tx]=min(Lef[-tx],1ll*i);
		if (tx>0) Rit[w-tx+1]=min(Rit[w-tx+1],1ll*i);
	}
	Rep(i,1,h) find(i,tx);
	Rep(i,1,h) Val2[i]=Val[i];
	
	bool b1=false,b2=false;
	Rep(i,1,w) if (Val1[i]==INF) b1=true;
	Rep(i,1,h) if (Val2[i]==INF) b2=true;
	if (b1 && b2) return puts("-1"),0;
	
	Rep(i,1,w) Q[++*Q]=Val1[i];
	Rep(i,1,h) Q[++*Q]=Val2[i];
	sort(Q+1,Q+*Q+1);
	*Q=unique(Q+1,Q+*Q+1)-Q-1;
	Rep(i,1,w) P1[i]=lower_bound(Q+1,Q+*Q+1,Val1[i])-Q;
	Rep(i,1,h) P2[i]=lower_bound(Q+1,Q+*Q+1,Val2[i])-Q;
//	Rep(i,1,w) printf("%d ",Val1[i]);puts("");
//	Rep(i,1,h) printf("%d ",Val2[i]);puts("");
	
	Rep(i,1,w){
		Cnt.Add(P1[i],1);Sum.Add(P1[i],Val1[i]);
	}
	ll Ans=0;
	Rep(i,1,h){
		Ans=(Ans+1ll*Cnt.Query(P2[i],*Q)*Val2[i])%Mod;
		Ans=(Ans+1ll*Sum.Query(1,P2[i]-1))%Mod;
	}
	printf("%d\n",(int)Ans);
}
/*
4 1 500000
RLRL
*/

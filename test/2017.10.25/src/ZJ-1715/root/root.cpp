#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct edge{
	int to,next;
}e[200010];
int n,nedge,head[100010];
ll a[100010],dou[100010],sing[100010],within[100010],best[100010],down[100010];
 inline ll read()
{
    ll X=0,w=1; char ch=0;
    while(ch<'0' || ch>'9') {if(ch=='-') w=-1;ch=getchar();}
    while(ch>='0' && ch<='9') X=(X<<3)+(X<<1)+ch-'0',ch=getchar();
    return X*w;
}
 inline void add(int a,int b)
{
	e[++nedge].to=b;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 void updata(int x,int go)
{
	dou[x]=max(dou[x],dou[go]);
	dou[x]=max(dou[x],sing[x]+sing[go]);
	dou[x]=max(dou[x],down[go]+within[x]);
	dou[x]=max(dou[x],down[x]+within[go]);
	sing[x]=max(sing[x],sing[go]);
	sing[x]=max(sing[x],down[x]+down[go]);
	within[x]=max(within[x],down[x]+sing[go]);
	within[x]=max(within[x],down[go]+a[x]+best[x]);
	within[x]=max(within[x],within[go]+a[x]);
	best[x]=max(best[x],sing[go]);
	down[x]=max(down[x],down[go]+a[x]);
}
 void dfs(int x,int fa)
{
	dou[x]=sing[x]=down[x]=within[x]=a[x];
	best[x]=0;
	for (int i=head[x];i;i=e[i].next)
	{
		int go=e[i].to;
		if (go==fa) continue;
		dfs(go,x);
		updata(x,go);
	}
}
 int main()
{
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i) a[i]=read();
	for (int i=1;i<n;++i)
	{
		int u,v;
		u=read();v=read();
		add(u,v);add(v,u);
	}
	dfs(1,0);
	printf("%lld",dou[1]);
}

#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(int argc, char const *argv[]) {
	freopen("core.in", "r", stdin);
	freopen("core.out", "w", stdout);
	int n = read();
	if (n < 3) return puts("-1") & 0;
	if (n & 1) {
		int num3 = n >> 1;
		int num4 = num3 * (num3 - 1) >> 1;
		printf("%d\n", num3 + num4 << 1);
		for (int i = 1; i <= num3; i++)
			printf("3 1 %d %d\n", i << 1, i << 1 | 1);
		for (int i = 1; i < num3; i++)
			for (int j = i + 1; j <= num3; j++)
				printf("4 %d %d %d %d\n", i << 1, j << 1, i << 1 | 1, j << 1 | 1);
		for (int i = 1; i <= num3; i++)
			printf("3 1 %d %d\n", i << 1, i << 1 | 1);
		for (int i = 1; i < num3; i++)
			for (int j = i + 1; j <= num3; j++)
				printf("4 %d %d %d %d\n", i << 1, j << 1, i << 1 | 1, j << 1 | 1);
	} else {
		puts("-1");
	}
	return 0;
}

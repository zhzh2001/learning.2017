#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<math.h>
#include<iostream>
#include<set>
#include<map>
#include<vector>
#include<bitset>
#include<stdlib.h>
#define ll long long
#define N 500005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,h,w;char s[N];bool f[4];
int mxx,mxy,mnx,mny,ax,ay;
inline void bfs(){
	int x=0,y=0;
	For(i,1,n){
		if(s[i]=='R') y++,f[0]=1;
		if(s[i]=='L') y--,f[1]=1;
		if(s[i]=='U') x--,f[2]=1;
		if(s[i]=='D') x++,f[3]=1;
		mxx=max(mxx,x);mnx=min(mnx,x);
		mxy=max(mxy,y);mny=min(mny,y);
	}
	ax=x;ay=y;
	if(ax==0&&ay==0){
		if(h/2+mxx<=h||w/2+mxy<=w||h/2+mnx>=1||w/2+mny>=1){puts("-1");exit(0);}
	}
}
ll ans;
inline void doit(int x,int y){
	For(i,1,n){
		if(s[i]=='R') y++;
		if(s[i]=='L') y--;
		if(s[i]=='U') x--;
		if(s[i]=='D') x++;
		ans++;
		if(x<1||x>h||y<1||y>w) return;
	}
}
inline void solve(){
	ll sum=0;
	For(i,1,w){
		int y=i;ans=0;
		int l=0,r=N-1,Ans;
		while(l<=r){
			int mid=(l+r)>>1;
			int Y=y+ay*mid;
			if(Y+mxy>w||Y+mny<1) r=mid-1,Ans=mid;
			else l=mid+1;
		}
		sum+=1ll*Ans*n*h;
		doit(1,Ans*ay+y);
		sum+=1ll*ans*h;
	}
	cout<<sum<<endl;
}
int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	n=read();h=read();w=read();
	scanf("%s",s+1);bfs();
	if(f[2]==0&&f[3]==0){solve();return 0;}
	For(i,1,h) For(j,1,w){
		int x=i,y=j;
		int l=0,r=N-1,Ans;
		while(l<=r){
			int mid=(l+r)>>1;
			int X=x+ax*mid,Y=y+ay*mid;
			if(X+mxx>h||Y+mxy>w||X+mnx<1||Y+mny<1) r=mid-1,Ans=mid;
			else l=mid+1;
		}
		ans+=1ll*Ans*n;
		doit(x+ax*Ans,y+ay*Ans);
	}
	cout<<ans;
	return 0;
}

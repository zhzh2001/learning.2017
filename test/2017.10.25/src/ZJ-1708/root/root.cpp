#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<math.h>
#include<iostream>
#include<set>
#include<map>
#include<vector>
#include<bitset>
#include<stdlib.h>
#define N 100005
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline ll read(){ll x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,head[N],cnt,p=0;
struct edge{int next,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
ll f[N],dp[N],Mx[N][2],a[N],ans,sz[N];
bitset <N> b[N];
inline void dfs(int x,int fa){
	ll mx1=0,mx2=0;
	sz[x]=1;p++;
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			dfs(e[i].to,x);
			sz[x]+=sz[e[i].to];
			if(f[e[i].to]>mx2) mx2=f[e[i].to];
			if(mx2>mx1) swap(mx1,mx2);
		}
	b[x][x]=1;
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			if(f[e[i].to]==mx1) b[x]|=b[e[i].to];
			if(f[e[i].to]==mx2) b[x]|=b[e[i].to];
		}
	f[x]=mx1+a[x];
	dp[x]=mx2+mx1+a[x];
}
int main(){
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n=read();For(i,1,n) a[i]=read();
	Rep(i,1,n){int x=read(),y=read();insert(x,y);}
	srand(998244353);
	int root=rand()%n+1; 
	dfs(root,0);
	For(i,1,n) if(sz[i]>1) ans=max(ans,dp[i]);
	For(i,1,n) For(j,i+1,n) if((b[i]&b[j])==0) ans=max(ans,dp[i]+dp[j]);
	cout<<ans<<endl;
	return 0;
}

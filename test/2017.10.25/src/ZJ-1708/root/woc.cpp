#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<math.h>
#include<iostream>
#include<set>
#include<map>
#include<vector>
#include<bitset>
#include<stdlib.h>
#define N 100005
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline ll read(){ll x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
int n,head[N],cnt;
struct edge{int next,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
ll f[N],dp[N],Mx[N][2],a[N],ans,sz[N],dep[N],p[N][2];
inline void dfs(int x,int fa){
	ll mx1=0,mx2=0;
	sz[x]=1;
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			dep[e[i].to]=dep[x]+1;
			dfs(e[i].to,x);
			sz[x]+=sz[e[i].to];
			if(f[e[i].to]>mx2) mx2=f[e[i].to];
			if(mx2>mx1) swap(mx1,mx2);
		}
	p[x][0]=mx1;p[x][1]=mx2;
	f[x]=mx1+a[x];
	dp[x]=mx2+mx1+a[x];
}
inline void Dfs(int x,int fa){
	ll sum;
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			Dfs(e[i].to,x);
			if(f[e[i].to]!=p[x][0]&&f[e[i].to]!=p[x][1]) Mx[x][0]=max(Mx[x][0],Mx[e[i].to][0]),Mx[x][1]=max(Mx[x][1],Mx[e[i].to][0]);//Mx[x][2]=max(Mx[x][2],Mx[e[i].to][0]);
			else Mx[x][0]=max(Mx[x][0],Mx[e[i].to][0]),Mx[x][1]=max(Mx[x][1],Mx[e[i].to][1]);//Mx[x][2]=max(Mx[x][2],Mx[e[i].to][1]);
			if(f[e[i].to]!=p[x][0]&&f[e[i].to]!=p[x][1]) sum=max(sum,Mx[x][0]);
			else sum=max(sum,Mx[x][1]);
		}
	ans=max(ans,dp[x]+sum);
	Mx[x][0]=max(Mx[x][0],dp[x]);
}
int main(){
//	freopen("root.in","r",stdin);
//	freopen("root.out","w",stdout);
	n=read();For(i,1,n) a[i]=read();
	Rep(i,1,n){int x=read(),y=read();insert(x,y);}
	srand(998244353);int root=rand()%n+1;
	dfs(root,0);Dfs(root,0);
	cout<<ans;
	return 0;
}

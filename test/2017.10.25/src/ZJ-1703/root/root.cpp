// %%%lzh %%%zyy %%%cogito %%%lqz %%%jzq %%%ctc %%%lbc %%%lzq 
// %%%fop_zz %%%largecube233 %%%starry %%%htr %%%littlered
// %%%sw_wind %%%sgr %%%wzt %%%zz %%%ljz %%%szb %%%fks %%%timber_wanglu
//���Ѱ� 
#include<bits/stdc++.h>
#define ll long long 
using namespace std;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(ll x){write(x);puts("");}
const int N=1e5+5;
ll a[N];
ll f[N][5];
int head[2*N],tail[2*N],nxt[2*N],nxted[2*N];
int n,x,y,t;
void addto(int x,int y)
{
	nxt[++t]=head[x];
	nxted[head[x]]=t;
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa)
{
	f[k][0]=a[k];
	for(int i=head[k];i;i=nxt[i]){
		int x=tail[i];
		if(x==fa)continue;
		dfs(x,k);
		f[k][4]=max(f[x][3]+f[k][3],f[k][4]);
		f[k][4]=max(f[x][4],f[k][4]);
		f[k][4]=max(f[x][1]+f[k][3],f[k][4]);
		f[k][4]=max(f[k][1]+f[x][1],f[k][4]);
		f[k][3]=max(f[x][1],f[k][3]);
		f[k][3]=max(f[x][3],f[k][3]);
		f[k][2]=max(f[k][1]+f[x][0],f[k][2]);
		f[k][1]=max(f[x][0]+f[k][0],f[k][1]);
		f[k][0]=max(f[x][0]+a[k],f[k][0]);
	}
}
int main()
{
	freopen("root.in","r",stdin);
	freopen("root.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<n;i++){
		x=read();y=read();
		addto(x,y);
		addto(y,x);
	}
	dfs(1,0);
	ll ans=0;
	for(int i=1;i<=n;i++){
		for(int j=0;j<5;j++)
			ans=max(f[i][j],ans);
	}
	printf("%lld",ans);
}
/*
9
1 2 3 4 5 6 7 8 9
1 2
2 4
2 5
1 8
8 9
3 6
3 7
1 3

41
*/

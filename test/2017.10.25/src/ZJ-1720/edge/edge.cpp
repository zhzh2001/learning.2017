#include<bits/stdc++.h>
using namespace std;
namespace LZHTaiQiangLa{
	const int L=2333333;
	char LZH[L],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(int x){
		if (x<0){
			putchar('-');
			x=-x;
		}
		if (!x){
			putchar('0');
			return;
		}
		static int a[15],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		//puts("");
	}
}
using namespace LZHTaiQiangLa;
const int JDB=500005;
const int mo=1000000007;
typedef long long ll;
char s[JDB];
ll dp[JDB];
int vis[JDB*2];
int n,h,w,mx,mn,x,up,ans,tot;
struct que{
	ll id,v;
	bool operator <(const que &LBCDaLao)const{
		return v<LBCDaLao.v;
	}
}q[JDB*2];
ll query(int x){
	if (dp[x]) return dp[x];
	ll &ans=dp[x]; ans=1e18;
	if (x+mn<1) ans=min(ans,(ll)vis[-x+JDB]);
	if (x+mx>up) ans=min(ans,(ll)vis[(up-x+1)+JDB]);
	return ans<1e17?ans:ans=query(x+::x)+n;
}
int main(){
	freopen("edge.in","r",stdin);
	freopen("edge.out","w",stdout);
	n=read(); h=read(); w=read();
	for (int i=1;i<=n;i++){
		s[i]=gc();
		for (;s[i]!='L'&&s[i]!='R'&&s[i]!='U'&&s[i]!='D';s[i]=gc());
	}
	mx=0; mn=0; x=0; up=w;
	for (int i=1;i<=n;i++){
		if (s[i]=='L') x--;
		if (s[i]=='R') x++;
		if (x>mx) mx=x;
		if (x<mn) mn=x;
		if (vis[x+JDB]==0)
			vis[x+JDB]=i;
	}
	if (x<=0)
		for (int i=1;i<=w;i++)
			q[++tot]=(que){1,query(i)};
	else for (int i=w;i;i--)
		q[++tot]=(que){1,query(i)};
	memset(vis,0,sizeof(vis));
	memset(dp,0,sizeof(dp));
	mx=0; mn=0; x=0; up=h;
	for (int i=1;i<=n;i++){
		if (s[i]=='U') x--;
		if (s[i]=='D') x++;
		if (x>mx) mx=x;
		if (x<mn) mn=x;
		if (vis[x+JDB]==0)
			vis[x+JDB]=i;
	}
	if (x<=0)
		for (int i=1;i<=h;i++)
			q[++tot]=(que){2,query(i)};
	else for (int i=h;i;i--)
		q[++tot]=(que){2,query(i)};
	sort(q+1,q+tot+1);
	for (int i=1;i<=tot;i++)
		if (q[i].id==1){
			if (q[i].v>1e15&&h)
				return puts("-1"),0;
			ans=(ans+q[i].v%mo*h%mo)%mo; w--;
		}
		else{
			if (q[i].v>1e15&&w)
				return puts("-1"),0;
			ans=(ans+q[i].v%mo*w%mo)%mo; h--;
		}
	printf("%d",ans);
}


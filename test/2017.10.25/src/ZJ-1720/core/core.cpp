#include<bits/stdc++.h>
using namespace std;
namespace LZHTaiQiangLa{
	const int L=2333333;
	char LZH[L],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(int x){
		if (x<0){
			putchar('-');
			x=-x;
		}
		if (!x){
			putchar('0');
			return;
		}
		static int a[15],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		//puts("");
	}
}
using namespace LZHTaiQiangLa;
int n,tot,cnt[100005],ans[100005][6];
void insert(int x,int y,int z,int p=0){
	tot++;
	cnt[tot]=p?4:3;
	ans[tot][1]=x;
	ans[tot][2]=y;
	ans[tot][3]=z;
	ans[tot][4]=p;
}
void work(int n){
	if (n==4){
		insert(1,2,3);
		insert(1,2,4);
		insert(1,3,4);
		insert(2,3,4);
		return;
	}
	if (n==3){
		insert(1,2,3);
		insert(1,2,3);
		return;
	}
	insert(n,n-1,n-2);
	insert(n,n-1,n-2);
	for (int i=1;i<n-2;i++)
		insert(n,i,n-1,i%(n-3)+1);
	work(n-2);
}
int main(){
	freopen("core.in","r",stdin);
	freopen("core.out","w",stdout);
	scanf("%d",&n);
	work(n);
	write(tot); puts("");
	for (int i=1;i<=tot;i++){
		write(cnt[i]); putchar(' ');
		for (int j=1;j<=cnt[i];j++)
			write(ans[i][j]),putchar(j==cnt[i]?'\n':' ');
	}
}

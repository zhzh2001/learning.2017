#include<bits/stdc++.h>
#define ll long long
using namespace std;
namespace LZHTaiQiangLa{
	const int L=2333333;
	char LZH[L],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(ll x){
		if (x<0){
			putchar('-');
			x=-x;
		}
		if (!x){
			putchar('0');
			return;
		}
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
	}
}
using namespace LZHTaiQiangLa;
const int JDB=100005;
struct edge{int to,next;}e[JDB*2];
int head[JDB],v[JDB],tot,n,ban;
ll f[JDB][2],g[JDB],ans;
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
void dfs(int x,int fa){
	int mx1=0,mx2=0,mx3=0;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa&&e[i].to!=ban){
			dfs(e[i].to,x);
			if (f[e[i].to][0]>f[mx1][0])
				mx3=mx2,mx2=mx1,mx1=e[i].to;
			else if (f[e[i].to][0]>f[mx2][0])
				mx3=mx2,mx2=e[i].to;
			else if (f[e[i].to][0]>f[mx3][0])
				mx3=e[i].to;
		}
	ans=max(ans,f[mx1][0]+f[mx2][0]+f[mx3][0]);
	f[x][0]=f[mx1][0]+v[x];
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa&&e[i].to!=ban){
			f[x][1]=max(f[x][1],f[e[i].to][1]);
			if (e[i].to!=mx1)
				f[x][1]=max(f[x][1],f[mx1][0]+f[e[i].to][0]+v[x]);
			else f[x][1]=max(f[x][1],f[mx2][0]+f[e[i].to][0]+v[x]);
		}
	//if (ban==2&&x==1)
	//	printf("%d %d %lld %lld %lld\n",mx1,mx2,f[mx1][0],f[mx2][0],v[x]);
}
int main(){
	freopen("root.in","r",stdin);
	freopen("DaTouDaTou.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) v[i]=read();
	for (int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y); add(y,x);
	}
	dfs(1,0);
	for (int i=1;i<=n;i++)
		g[i]=f[i][1];
	ans=max(ans,f[1][1]);
	int tmp=1;
	for (int i=2;i<=n;i++){
		memset(f,0,sizeof(f));
		ban=i;
		dfs(1,0);
		if (ans<f[1][1]+g[i])
			ans=f[1][1]+g[i],tmp=i;
	}
	write(ans);
	/*printf("\n%d\n",tmp);
	memset(f,0,sizeof(f));
	ban=tmp;
	dfs(1,0);
	printf("%lld %lld\n",g[2],f[1][1]);*/
}

#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		string s;
		cin >> s;
		long long ans = 0;
		int now = 0, tot = count(s.begin(), s.end(), '1'), len = s.length();
		bool flag = false;
		for (int i = 0; i < len; i++)
			if (s[i] == '1')
			{
				ans += len - (tot - now) - i;
				now++;
				flag = true;
			}
			else if (flag)
			{
				ans += now;
				flag = false;
			}
		cout << ans << endl;
	}
	return 0;
}
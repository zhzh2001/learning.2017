#include <iostream>
#include <cstring>
using namespace std;
int a[10], cnt[10];
int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		long long l, r;
		cin >> l >> r;
		for (int i = 0; i < 10; i++)
			cin >> a[i];
		int ans = 0;
		for (long long i = l; i <= r; i++)
		{
			long long x = i;
			memset(cnt, 0, sizeof(cnt));
			do
				cnt[x % 10]++;
			while (x /= 10);
			bool now = true;
			for (int j = 0; j < 10; j++)
				if (cnt[j] == a[j])
				{
					now = false;
					break;
				}
			ans += now;
		}
		cout << ans << endl;
	}
	return 0;
}
#include <iostream>
#include <cstring>
using namespace std;
const int N = 1e6;
int p[N / 5], mp[N * 2 + 5], c[N * 2 + 5], fact[N + 5], inv[N + 5];
int qpow(long long a, int b, int mod)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % mod;
		a = a * a % mod;
	} while (b /= 2);
	return ans;
}
int C(int a, int b, int mod)
{
	return 1ll * fact[a] * inv[a - b] % mod * inv[b] % mod;
}
int lucas(long long a, long long b, int mod)
{
	if (!a)
		return 1;
	return 1ll * lucas(a / mod, b / mod, mod) * C(a % mod, b % mod, mod) % mod;
}
int main()
{
	int pn = 0;
	for (int i = 2; i <= N * 2; i++)
	{
		if (!mp[i])
		{
			p[++pn] = i;
			mp[i] = i;
		}
		for (int j = 1; i * p[j] <= N * 2; j++)
		{
			mp[i * p[j]] = p[j];
			if (i % p[j] == 0)
				break;
		}
	}
	int t;
	cin >> t;
	while (t--)
	{
		long long n, k;
		int m;
		cin >> n >> k >> m;
		long long cnt = (n + k - 1) / k, rem = cnt * k - n;
		if (n <= N && k <= N)
		{
			memset(c, 0, sizeof(c));
			for (int i = rem + 1; i <= rem + cnt - 1; i++)
				c[i]++;
			for (int i = 2; i < cnt; i++)
				c[i]--;
			for (int i = rem + cnt - 1; i > 1; i--)
				if (mp[i] < i)
				{
					c[mp[i]] += c[i];
					c[i / mp[i]] += c[i];
				}
			long long ans = 1;
			for (int i = 1; i <= rem + cnt - 1; i++)
				if (mp[i] == i)
					ans = ans * qpow(i, c[i], m) % m;
			cout << cnt << ' ' << ans << endl;
		}
		else
		{
			fact[0] = 1;
			for (int i = 1; i < m; i++)
				fact[i] = 1ll * fact[i - 1] * i % m;
			inv[m - 1] = qpow(fact[m - 1], m - 2, m);
			for (int i = m - 2; i >= 0; i--)
				inv[i] = 1ll * inv[i + 1] * (i + 1) % m;
			cout << cnt << ' ' << lucas(rem + cnt - 1, cnt - 1, m) << endl;
		}
	}
	return 0;
}
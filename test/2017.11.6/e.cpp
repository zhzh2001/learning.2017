#include <iostream>
#include <ext/pb_ds/hash_policy.hpp>
#include <ext/pb_ds/assoc_container.hpp>
#include <list>
#include <algorithm>
using namespace std;
const int N = 300005;
pair<int, int> q[N];
__gnu_pbds::cc_hash_table<int, list<int>> mat;
__gnu_pbds::cc_hash_table<int, int> key;
int main()
{
	ios::sync_with_stdio(false);
	int n, m, rt, key;
	cin >> n >> m >> rt >> key;
	::key[rt] = key;
	for (int i = 1; i < n; i++)
	{
		int u, v, k;
		cin >> u >> v >> k;
		::key[u] = k;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	int lastans = 0;
	while (m--)
	{
		int opt;
		cin >> opt;
		opt ^= lastans;
		if (opt == 0)
		{
			int v, u, k;
			cin >> v >> u >> k;
			v ^= lastans;
			u ^= lastans;
			k ^= lastans;
			::key[u] = k;
			mat[u].push_back(v);
			mat[v].push_back(u);
		}
		else
		{
			int v, k;
			cin >> v >> k;
			v ^= lastans;
			k ^= lastans;
			__gnu_pbds::cc_hash_table<int, __gnu_pbds::null_type> vis;
			vis.insert(rt);
			int l = 1, r = 1;
			q[1] = make_pair(rt, 0);
			while (l <= r)
			{
				if (q[l].first == v)
					break;
				for (int v : mat[q[l].first])
					if (vis.find(v) == vis.end())
					{
						vis.insert(v);
						q[++r] = make_pair(v, l);
					}
				l++;
			}
			int minv = key ^ k, maxv = key ^ k;
			for (int i = l; q[i].second; i = q[i].second)
			{
				minv = min(minv, ::key[q[i].first] ^ k);
				maxv = max(maxv, ::key[q[i].first] ^ k);
			}
			cout << minv << ' ' << maxv << endl;
			lastans = minv ^ maxv;
		}
	}
	return 0;
}
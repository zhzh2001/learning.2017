#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int n, m, a[N], b[N], ans[N];
struct quest
{
	int x, k, id;
	bool operator<(const quest &rhs) const
	{
		return x < rhs.x;
	}
} q[N * 2];
struct BITs
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x; x -= x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x <= n; x += x & -x)
			ans += tree[x];
		return ans;
	}
} T;
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		cin >> n >> m;
		for (int i = 1; i <= n; i++)
			cin >> a[i];
		int cc = 0;
		for (int i = 1; i <= n; i++)
			if (a[i] != a[i + 1])
				b[++cc] = i;
		int qn = 0;
		for (int i = 1; i <= m; i++)
		{
			int l, r, k;
			cin >> l >> r >> k;
			int bl = lower_bound(b + 1, b + cc + 1, l) - b, br = lower_bound(b + 1, b + cc + 1, r) - b;
			ans[i] = b[bl] - l + 1 >= k;
			if (bl < br)
				ans[i] += r - b[br - 1] >= k;
			if (bl + 1 <= br - 1)
			{
				q[++qn] = {bl, k, -i};
				q[++qn] = {br - 1, k, i};
			}
		}
		sort(q + 1, q + qn + 1);
		for (int i = 1, j = 1; i <= cc; i++)
		{
			T.modify(b[i] - b[i - 1], 1);
			for (; j <= qn && q[j].x == i; j++)
				if (q[j].id > 0)
					ans[q[j].id] += T.query(q[j].k);
				else
					ans[-q[j].id] -= T.query(q[j].k);
		}
		for (int i = 1; i <= cc; i++)
			T.modify(b[i], -1);
		for (int i = 1; i <= m; i++)
			cout << ans[i] << endl;
	}
	return 0;
}
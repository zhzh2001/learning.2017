#include <iostream>
using namespace std;
const int N = 55;
int f[N][N];
int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n, k, m;
		cin >> n >> k >> m;
		int cnt = (n + k - 1) / k;
		f[0][0] = 1;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= cnt && j <= i; j++)
			{
				f[i][j] = 0;
				for (int t = 1; t <= k && t <= i; t++)
					(f[i][j] += f[i - t][j - 1]) %= m;
			}
		cout << cnt << ' ' << f[n][cnt] << endl;
	}
	return 0;
}
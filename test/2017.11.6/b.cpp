#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 35;
const long long INF = 2e18;
int n;
long long k, a[N];
vector<long long> l, r;
void dfs(int k, int high, long long now, vector<long long> &vec)
{
	if (k > high)
	{
		if (now < INF)
			vec.push_back(now);
	}
	else
	{
		dfs(k + 1, high, now, vec);
		if (now > ::k / a[k])
			dfs(k + 1, high, INF, vec);
		else
			dfs(k + 1, high, now * a[k], vec);
	}
}
int main()
{
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	dfs(1, n / 2, 1, l);
	dfs(n / 2 + 1, n, 1, r);
	sort(r.begin(), r.end());
	int ans = 0;
	for (long long i : l)
		ans += upper_bound(r.begin(), r.end(), k / i) - r.begin();
	cout << ans - 1 << endl;
	return 0;
}
#include<fstream>
#include<algorithm>
#include<cstdlib>
using namespace std;
ifstream fin("energy.in");
ofstream fout("energy.out");
const int N=1005,INF=0x3f3f3f3f;
int n,m,d[N][N],rd[N][N],f[N][N];
inline int floyd(int d[N][N])
{
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=d[i][j];
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	int ret=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			if(f[i][j]==INF)
				return -1;
			ret=max(ret,f[i][j]);
		}
	return ret;
}
void dfs(int u,int v)
{
	if(u==n)
	{
		int fw=floyd(d),fb=floyd(rd);
		if(fw==-1||fb==-1)
			return;
		int m=min(fw,fb);
		if(m==::m)
		{
			int cnt=0;
			for(int i=1;i<=n;i++)
				for(int j=i+1;j<=n;j++)
					if(d[i][j]==1)
						cnt++;
			fout<<cnt<<endl;
			for(int i=1;i<=n;i++)
				for(int j=i+1;j<=n;j++)
					if(d[i][j]==1)
						fout<<i<<' '<<j<<endl;
			exit(0);
		}
	}
	else
	{
		d[u][v]=d[v][u]=1;
		rd[u][v]=rd[v][u]=INF;
		if(v<n)
			dfs(u,v+1);
		else
			dfs(u+1,u+2);
		d[u][v]=d[v][u]=INF;
		rd[u][v]=rd[v][u]=1;
		if(v<n)
			dfs(u,v+1);
		else
			dfs(u+1,u+2);
	}
}
int main()
{
	fin>>n>>m;
	if(n==7&&m==1)
	{
		fout<<-1<<endl;
		return 0;
	}
	if(n==8)
	{
		if(m==2)
			fout<<"21\n1 2\n1 3\n1 4\n1 5\n1 6\n1 7\n2 3\n2 4\n2 5\n2 6\n2 7\n3 4\n3 5\n3 6\n3 7\n4 5\n4 6\n4 7\n5 6\n5 8\n7 8\n";
		else if(m==3)
			fout<<"21\n1 2\n1 3\n1 4\n1 5\n1 6\n1 7\n2 3\n2 4\n2 5\n2 6\n2 7\n3 4\n3 5\n3 6\n3 7\n4 5\n4 6\n4 7\n5 6\n5 7\n6 8\n";
		else
			fout<<-1<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
		d[i][i]=rd[i][i]=0;
	dfs(1,2);
	fout<<-1<<endl;
	return 0;
}

#include<fstream>
#include<ctime>
#include<random>
using namespace std;
ofstream fout("burn.in");
const int n=1e9,m=1e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> d(1,n),bl(0,1);
		int x=d(gen),y=n+1-x;
		fout<<x<<' '<<y<<' ';
		if(bl(gen))
			fout<<"U\n";
		else
			fout<<"L\n";
	}
	return 0;
}

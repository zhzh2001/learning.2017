#include<fstream>
using namespace std;
ifstream fin("burn.in");
ofstream fout("burn.ans");
const int N=40005;
bool vis[N][N];
int main()
{
	int n,m;
	fin>>n>>m;
	while(m--)
	{
		int x,y;
		char c;
		fin>>y>>x>>c;
		int now=0;
		if(c=='U')
			while(x&&!vis[x][y])
			{
				now++;
				vis[x][y]=true;
				x--;
			}
		else
			while(y&&!vis[x][y])
			{
				now++;
				vis[x][y]=true;
				y--;
			}
		fout<<now<<endl;
	}
	return 0;
}

#include<fstream>
#include<algorithm>
#include<ctime>
#include<cstdlib>
using namespace std;
ifstream fin("bright.in");
ofstream fout("bright.out");
const int N=105;
int n,a[N],l[N],x[N*3],lit[N][3],vis[N*3],ans;
void dfs(int k,int now)
{
	if(k==n+1)
		ans=max(ans,now);
	else
	{
		for(int i=lit[k][0];i<lit[k][1];i++)
			if(!vis[i]++)
				now+=x[i+1]-x[i];
		dfs(k+1,now);
		for(int i=lit[k][0];i<lit[k][1];i++)
			if(!--vis[i])
				now-=x[i+1]-x[i];
		for(int i=lit[k][1];i<lit[k][2];i++)
			if(!vis[i]++)
				now+=x[i+1]-x[i];
		dfs(k+1,now);
		for(int i=lit[k][1];i<lit[k][2];i++)
			if(!--vis[i])
				now-=x[i+1]-x[i];
	}
}
int main()
{
	srand(time(NULL));
	fin>>n;
	int m=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i]>>l[i];
		x[++m]=a[i]-l[i];
		x[++m]=a[i];
		x[++m]=a[i]+l[i];
	}
	sort(x+1,x+m+1);
	for(int i=1;i<=n;i++)
	{
		lit[i][0]=lower_bound(x+1,x+m+1,a[i]-l[i])-x;
		lit[i][1]=lower_bound(x+1,x+m+1,a[i])-x;
		lit[i][2]=lower_bound(x+1,x+m+1,a[i]+l[i])-x;
	}
	if(n<=20)
		dfs(1,0);
	else
	{
		while(clock()<0.8*CLOCKS_PER_SEC)
		{
			for(int i=1;i<m;i++)
				vis[i]=0;
			for(int i=1;i<=n;i++)
				if(rand()&1)
					for(int j=lit[i][0];j<lit[i][1];j++)
						vis[j]++;
				else
					for(int j=lit[i][1];j<lit[i][2];j++)
						vis[j]++;
			int now=0;
			for(int i=1;i<m;i++)
				if(vis[i])
					now+=x[i+1]-x[i];
			ans=max(ans,now);
		}
	}
	fout<<ans<<endl;
	return 0;
}

#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
const int N=1000000000,M=100000;
char s[2]={'U','L'};
int R() {
	return rand()<<15|rand();
}
int main() {
	srand((int) time(0));
	freopen("burn10.in","w",stdout);
	printf("%d %d\n",N,M);
	for (int i=1;i<=M;i++) {
		int x=R()%N+1,y=N+1-x;
		printf("%d %d %c\n",x,y,s[rand()&1]);
	}
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=550,oo=1000000000;
typedef	long long ll;
int i,j,k,n,m,num,ans,ch;
int a[N][N],b[N][N];
FILE *Finn, *Fout, *Fstd, *Fres;
void Return(double p, char *s) {
	fprintf(Fres, "%.9lf\n%s\n", p, s);
	exit(0);
}
int ab(int x) {
	if (x<0) return -x;
	return x;
}
int max(int x,int y) {
	if (x<y) return y;
	return x;
}
void R(int &x) {
	int ff=0;
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) { if (ch=='-') ff=1;ch=getchar();}
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	if (ff) x=-x;
}
int	main(int args, char** argv) {
	Finn = fopen(argv[1], "r");
	Fstd = fopen(argv[2], "r");
	Fout = fopen(argv[3], "r");
	Fres = fopen(argv[4], "w");
	fscanf(Finn,"%d%d",&n,&m);
	fscanf(Fstd,"%d",&ans);
	freopen(argv[3],"r",stdin);
	if (ans==-1) {
		scanf("%d",&num);
		if (num!=-1) Return(0.0,"WA");
		Return(1.0,"AC");
	}
	R(num);
	if (num>n*(n-1)/2) Return(0.0,"WA");
	memset(a,60,sizeof a);
	memset(b,60,sizeof b);
	for (i=1;i<=n;i++) a[i][i]=b[i][i]=1;
	for (i=1;i<=num;i++) {
		int x,y;
		R(x);R(y);
		if (a[x][y]==1) Return(0.0,"WA");
		a[x][y]=a[y][x]=1;
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++) if (a[i][j]>oo) b[i][j]=1;
	for (k=1;k<=n;k++)
		for (i=1;i<=n;i++)
			for (j=1;j<=n;j++) a[i][j]=min(a[i][j],a[i][k]+a[k][j]);
	for (k=1;k<=n;k++)
		for (i=1;i<=n;i++)
			for (j=1;j<=n;j++) b[i][j]=min(b[i][j],b[i][k]+b[k][j]);
	int ma1=-1;
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++) ma1=max(ma1,a[i][j]);
	int ma2=-1;
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++) ma2=max(ma2,b[i][j]);
	if (ma1>oo) ma1=-1;
	if (ma2>oo) ma2=-1;
	if (min(ma1,ma2)==m) Return(1.0,"AC");
	Return(0.0,"WA");
}

#include<cstdio>
using namespace std;
const int N=1010;
int i,j,k,n,m;
void W(int x) {
	if (x>=10) W(x/10);
	putchar(x%10+'0');
}
int main() {
	freopen("energy10.in","r",stdin);
	freopen("energy10.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m>=n) return puts("-1"),0;
	if (m==1 || m>=4) return puts("-1"),0;
	if (m==2) {
		if (n<=4) return puts("-1"),0;
		W(n-1);puts("");
		for (i=1;i<n;i++) W(i),putchar(' '),W(i+1),puts("");
	}
	else {
		W(3+2*(n-4)+(n-4)*(n-5)/2);puts("");
		puts("1 2");
		puts("2 3");
		puts("3 4");
		for (i=5;i<=n;i++) {
			W(2),putchar(' '),W(i),puts("");
			W(3),putchar(' '),W(i),puts("");
		}
		for (i=5;i<n;i++)
			for (j=i+1;j<=n;j++) W(i),putchar(' '),W(j),puts("");
	}
}

#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
using namespace std;
const int N=100,T=5,A=100000000,L=25000000,oo=1000000000;
set<int> S;
int a[N+10];
int R() {
	return rand()<<15|rand();
}
int randA() {
	int x=0,Fg=1;
	while (Fg) {
		x=R()%A;
		int mi=oo;
		for (int i=1;i<=T;i++) {
			int t=x-a[i];
			if (t<0) t=-t;
			if (t<mi) mi=t;
		}
		if (R()%(mi+1)==0) Fg=0;
	}
	return x;
}
int main() {
	freopen("bright.in","w",stdout);
	srand((int) time(0));
	printf("%d\n",N);
	for (int i=1;i<=T;i++) a[i]=R()%A;
	for (int i=1;i<=N;i++) {
		int a=randA(),l=R()%L+1;
		while (S.find(a)!=S.end()) a=randA();
		S.insert(a);
		printf("%d %d\n",a,l);
	}
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>
using namespace std;

ifstream fin("burn.in");
ofstream fout("burn.out");

const int N = 1e3 + 5, M = 1e5 + 5;

int n, m, mat[N][N];

struct Query {
	int x, y, dx, dy;
} q[M];

int main() {
	fin >> n >> m;
	if (n <= 1000 && m <= 1000) {
		for (int i = 1; i <= m; ++i) {
			int x, y;
			char ch;
			fin >> x >> y >> ch;
			int dx, dy;
			if (ch == 'U') dx = 0, dy = -1;
			if (ch == 'L') dx = -1, dy = 0;
			int cnt = 0;
			while (x > 0 && y > 0 && !mat[x][y]) {
				mat[x][y] = 1;
				++cnt;
				x += dx, y += dy;
			}
			fout << cnt << endl;
		}
		return 0;
	}
	bool flag = true;
	for (int i = 1; i <= m; ++i) {
		char ch;
		fin >> q[i].x >> q[i].y >> ch;
		if (ch == 'U') q[i].dx = 0, q[i].dy = -1;
		if (ch == 'L') q[i].dx = -1, q[i].dy = 0;
		flag &= (ch == 'L');
	}
	if (flag) { // All 'L'
		set<int> S;
		for (int i = 1; i <= m; ++i) {
			if (S.count(q[i].x))
				fout << 0 << endl;
			else {
				fout << q[i].x << endl;
				S.insert(q[i].x);
			}
		}
		return 0;
	}
	return 0;
}

#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cassert>
using namespace std;

ifstream fin("energy.in");
ofstream fout("energy.out");

int n, m;

const int N = 1005;

struct Edge {
	int u, v;	
} e[N * N];

struct DisjointSet {
	int fa[N], cnt;
	inline void makeSet(int x) { 
		fa[x] = x; 
	}
	inline int find(int x) { 
		return x == fa[x] ? x : fa[x] = find(fa[x]); 
	}
	inline bool same(int x, int y) { 
		return find(x) == find(y); 
	}
	inline void merge(int x, int y) { 
		int fx = find(x), fy = find(y);
		if (fx != fy) {
			fa[fx] = fy;
			--cnt;	
		}
	}
} f1, f2;

int mat[10][10], mat1[10][10];

int main() {
	fin >> n >> m;
	if (m == 1 || n <= 3) {
		fout << -1 << endl;
		return 0;
	}
	if (n <= 8) {
		int ecnt = 0;
		for (int i = 0; i < n; ++i)
			for (int j = i + 1; j < n; ++j)
				e[ecnt++] = (Edge) { i, j };
		for (int i = 1; i < (1 << ecnt); ++i) {
			if (clock() > 900) {
				fout << -1 << endl;
				return 0;	
			}
			for (int j = 0; j < n; ++j)
				f1.makeSet(j), f2.makeSet(j);
			f1.cnt = f2.cnt = n;
			int pc = 0;
			for (int j = 0; j < ecnt; ++j) {
				if (i & (1 << j)) 
					f1.merge(e[j].u, e[j].v), ++pc;
				else
					f2.merge(e[j].u, e[j].v);
			}
			if (f1.cnt != 1 || f2.cnt != 1)
				continue;
			memset(mat, 0x3f, sizeof mat);
			memset(mat1, 0x3f, sizeof mat1);
			for (int j = 0; j < n; ++j)
				mat[j][j] = mat1[j][j] = 0;
			for (int j = 0; j < ecnt; ++j) {
				if (i & (1 << j)) 
					mat[e[j].u][e[j].v] = mat[e[j].v][e[j].u] = 1;
				else
					mat1[e[j].u][e[j].v] = mat1[e[j].v][e[j].u] = 1;
			}
			for (int k = 0; k < n; ++k)
				for (int i = 0; i < n; ++i)
					for (int j = 0; j < n; ++j) {
						mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
						mat1[i][j] = min(mat1[i][j], mat1[i][k] + mat1[k][j]);
					}
			if (clock() > 900) {
				fout << -1 << endl;
				return 0;	
			}
			int mx1 = 0, mx2 = 0;
			for (int k = 0; k < n; ++k)
				for (int j = 0; j < n; ++j) {
					if (mat[k][j] != 0x3f3f3f3f)
						mx1 = max(mx1, mat[k][j]);
					if (mat1[k][j] != 0x3f3f3f3f)
						mx2 = max(mx2, mat1[k][j]);						
				}
			int lp = min(mx1, mx2);
			if (lp == m) {
				fout << pc << endl;
				for (int j = 0; j < ecnt; ++j) {
					if (i & (1 << j)) {
						fout << e[j].u + 1 << ' ' << e[j].v + 1 << "\n";
					}
				}
				return 0;
			}
		}
	}
	fout << -1 << endl;
	return 0;
}

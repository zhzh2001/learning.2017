#include <bits/stdc++.h>
using namespace std;

ifstream f1("energy.in");
ifstream f2("energy.out");

const int N = 505;

int n, m, ecnt;
int mat[N][N], mat1[N][N];

int main() {
	f1 >> n >> m;
	f2 >> ecnt;
	memset(mat, 0x3f, sizeof mat);
	memset(mat1, 0x3f, sizeof mat1);
	for (int i = 1; i <= ecnt; ++i) {
		int x, y;
		f2 >> x >> y;
		mat[x][y] = mat[y][x] = 1;
	}
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			if (mat[i][j] == 0x3f3f3f3f)
				mat1[i][j] = 1;
	for (int i = 1; i <= n; ++i)
		mat[i][i] = mat1[i][i] = 0;
	for (int k = 1; k <= n; ++k) {
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j) {
				mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);	
				mat1[i][j] = min(mat1[i][j], mat1[i][k] + mat1[k][j]);	
			}
	}
	int mx1 = 0, mx2 = 0;
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j) {
			if (mat[i][j] != 0x3f3f3f3f)
				mx1 = max(mx1, mat[i][j]);
			if (mat1[i][j] != 0x3f3f3f3f)
				mx2 = max(mx2, mat1[i][j]);
		}
	int result = min(mx1, mx2);
	assert(result == m);
	return 0;
}

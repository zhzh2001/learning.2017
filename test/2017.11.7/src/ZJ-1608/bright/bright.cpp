#include<cstdio>
#include<cctype>
#include<string.h>
using namespace std;
inline int In(){
	   int x=0;char c=getchar();
	   while(!isdigit(c))c=getchar();x=c-'0';c=getchar();
	   while(isdigit(c))x=x*10+c-'0',c=getchar();
	   return x;
}
int n,a[30],l[30],f[100000],ans,tmp,t;
int main()
{
	n=In();
	if(n>15)return 0;
	for(int i=1;i<=n;i++)
		a[i]=In()+1000,l[i]=In();
	ans=0;
	for(int i=0;i<(1<<n);i++){
		memset(f,0,sizeof(f));
		t=i;tmp=0;
		for(int j=1;j<=n;j++){
			if(t%2){
				for(int k=a[j]-l[j];k<=a[j];k++)
					tmp+=(f[k]==0),f[k]++;	
			}else{
				for(int k=a[j];k<=a[j]+l[j];k++)
					tmp+=(f[k]==0),f[k]++;
			}
			t/=2;
		}
		if(tmp>ans)ans=tmp;
	}printf("%d\n",ans);
}


#include<algorithm>
#include<cstdio>
#include<cctype>
using namespace std;
const int N=1e6+10;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();x=c-'0';c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
inline void Ot(int x){
	if(x/10)Ot(x/10);
	putchar(x%10+'0');
}
struct te{
	int l,r,rx,lx,mkr,mkl; 
}t[N<<2];
inline void build(int u,int l,int r){
	t[u].l=l,t[u].r=r;
	t[u].rx=t[u].lx=
	t[u].mkr=t[u].mkl=0;
	if(l==r)return;int mid=l+r>>1;
	build(u<<1,l,mid),build(u<<1|1,mid+1,r);
}
inline void pdl(int u){
	t[u].lx=max(t[u].lx,t[u].mkl);
	if(t[u].l!=t[u].r)
		t[u<<1].mkl=max(t[u].mkl,t[u<<1].mkl);
		t[u<<1|1].mkl=max(t[u].mkl,t[u<<1|1].mkl);
	t[u].mkl=0;
}
inline void pdr(int u){
	t[u].rx=max(t[u].rx,t[u].mkr);
	if(t[u].l!=t[u].r)
		t[u<<1].mkr=max(t[u].mkr,t[u<<1].mkr);
		t[u<<1|1].mkr=max(t[u].mkr,t[u<<1|1].mkr);
	t[u].mkr=0;
}
inline int qln(int u,int loc){
	if(t[u].mkl)pdl(u);
	if(t[u].l==loc&&t[u].r==loc)
		return t[u].lx;
	int mid=t[u].l+t[u].r>>1;
	if(loc<=mid)return qln(u<<1,loc);
	else return qln(u<<1|1,loc); 
}
inline int qrw(int u,int loc){
	if(t[u].mkr)pdr(u);
	if(t[u].l==loc&&t[u].r==loc)
		return t[u].rx;
	int mid=t[u].l+t[u].r>>1;
	if(loc<=mid)return qrw(u<<1,loc);
	else return qrw(u<<1|1,loc); 	
}
inline void mdr(int u,int l,int r,int k){
	if(t[u].mkr)pdr(u);
	if(t[u].l==l&&t[u].r==r){
		t[u].mkr=k;return;
	}
	int mid=t[u].l+t[u].r>>1;
	if(r<=mid)mdr(u<<1,l,r,k);
	else if(l>mid)mdr(u<<1|1,l,r,k);
	else mdr(u<<1,l,mid,k),mdr(u<<1|1,mid+1,r,k);
}
inline void mdl(int u,int l,int r,int k){
	if(t[u].mkl)pdl(u);
	if(t[u].l==l&&t[u].r==r){
		t[u].mkl=k;return;
	}
	int mid=t[u].l+t[u].r>>1;
	if(r<=mid)mdl(u<<1,l,r,k);
	else if(l>mid)mdl(u<<1|1,l,r,k);
	else mdl(u<<1,l,mid,k),mdl(u<<1|1,mid+1,r,k);
}
int n,m,x,y,bk;
int reu[N];
int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=In(),m=In();
	build(1,1,n);
	if(n>(int)1e6){
		while(m--){
			x=In(),y=In();
			char c=getchar();
			Ot(y);
		}
	}
	while(m--){
		y=In(),x=In();
		if(reu[x]){
			puts("0");
			continue;
		}reu[x]=1;
		char c=getchar();
		if(c=='U'){
			bk=qln(1,y);
			Ot(x-bk),putchar('\n');
			if(bk+1<=x-1)mdr(1,bk+1,x-1,y);
		}else{
			bk=qrw(1,x);
			Ot(y-bk),putchar('\n');
			if(bk+1<=y-1)mdl(1,bk+1,y-1,x);
		}
	}
}


#include <bits/stdc++.h>
#define N 120
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct node {
	int a, l;
	friend bool operator < (const node &a, const node &b) {
		return a.a < b.a;
	}
}a[N];
int ans;
int main() {
	freopen("bright.in", "r", stdin);
	freopen("bright.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++) {
		a[i].a = read();
		a[i].l = read();
	}
	sort(a + 1, a + n + 1);
	a[n + 1].a = 1 << 30;
	for (int i = 1, last_end = - 1 << 30; i <= n; i++) {
		//cout << a[i].a << " " << a[i].l << " " << last_end << " " << ans << endl;
		if (a[i].a + a[i].l < last_end) continue;
		if (a[i].a - a[i].l > last_end) {
			//cout << "left" << endl;
			ans += a[i].l;
			last_end = a[i].a;
		} else if (a[i+1].a - a[i].a >= a[i].l) {
			//cout << "right" << endl;
			ans += a[i].l;
			last_end = a[i].a + a[i].l;
		} else if (a[i+1].a - a[i].a > a[i].a - last_end) {
			//cout << "aha" << endl;
			ans += a[i].a + a[i].l - max(a[i].a, last_end);
			last_end = a[i].a + a[i].l;
		} else {
			ans += a[i].a - last_end;
			last_end = a[i].a;
		}
	}
	//cout << "WARNING: ans = " << ans << endl;
	a[0].a = - 1 << 30;
	int ans1 = ans; ans = 0;
	for (int i = n, last_end = 1 << 30; i; i--) {
		//cout << a[i].a << " " << a[i].l << " " << last_end << " " << ans << endl;
		if (a[i].a - a[i].l > last_end) continue;
		if (a[i].a + a[i].l < last_end) {
			//cout << "left" << endl;
			ans += a[i].l;
			last_end = a[i].a;
		} else if (a[i].a - a[i-1].a >= a[i].l) {
			//cout << "right" << endl;
			ans += a[i].l;
			last_end = a[i].a - a[i].l;
		} else if (a[i].a - a[i-1].a > last_end - a[i].a) {
			//cout << "aha" << endl;
			ans += - (a[i].a - a[i].l) + min(a[i].a, last_end);
			last_end = a[i].a - a[i].l;
		} else {
			ans += - a[i].a + last_end;
			last_end = a[i].a;
		}
	}
	printf("%d\n", max(ans1, ans));
	return 0;
}

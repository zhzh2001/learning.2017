#pragma GCC optimize 2
#include <bits/stdc++.h>
#define M 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char str[5];
map<int, bool> vis;
int b[M], x[M], y[M], z[M];
struct Segment_Tree {
	int mx[M<<2], tg[M<<2];
	void push_down(int x){
		if(tg[x]){
			tg[x<<1]=tg[x<<1|1]=1;
			mx[x<<1]=mx[x];
			mx[x<<1|1]=mx[x];
			tg[x]=0;
		}
	}
	void dfs(int x, int l, int r, int v) {
		if (tg[x]||l==r){
			tg[x]=1;
			mx[x]=max(mx[x], v);
			return;
		}
		int mid = l + r >> 1;
		dfs(x << 1, l, mid, v);
		dfs(x<<1|1, mid+1, r, v);
	}
	void add(int x, int L, int R, int l, int r, int v){
		if(l>r)return;
//		cout << L << " " << R << " " << l << " " << r << endl;
		if (l == L && r == R) {dfs(x, l, r, v);
			return ;
		}
		push_down(x);
		int mid = L + R >> 1;
		if (r <= mid) add(x<<1, L, mid, l, r, v);
		else if (l > mid) add(x<<1|1, mid + 1, R, l, r, v);
		else add(x<<1, L, mid, l, mid, v), add(x<<1|1, mid + 1, R, mid + 1, r, v);
		tg[x] = tg[x<<1] && tg[x<<1|1] && mx[x<<1] == mx[x<<1|1];
		if (tg[x]) mx[x] = mx[x<<1];
	}
	int ask(int x, int l, int r, int k) {
		if (tg[x] || l == r) return mx[x];
		int mid = l + r >> 1;
		if (k <= mid) return ask(x << 1, l, mid, k);
		else return ask(x << 1 | 1, mid + 1, r, k);
	}
}U,L;
int main() {
	freopen("burn.in", "r", stdin);
	freopen("burn.out", "w", stdout);
	int n = read(), m = read();
	for (int i = 1; i <= m; i++) {
		x[i] = read();
		y[i] = read();
		scanf("%s", str);
		z[i] = str[0] == 'U';
		b[i] = x[i];
	}
	sort(b + 1, b + m + 1);
	int* b_end = unique(b + 1, b + m + 1), bnt = b_end - b - 1;
	for (int i = 1; i <= m; i++)
		x[i] = lower_bound(b + 1, b_end, x[i]) - b;
	b[bnt+1]=n+1;
	for (int i = 1; i <= m; i++) {
		if (vis[x[i]]) {
			puts("0");
			continue;
		}
		vis[x[i]] = 1;
		if (z[i]) { // U
			int d = U.ask(1, 1, bnt, x[i]);
//			printf("%d,%d ", d, x[i]);
			printf("%d\n", -b[x[i]]+b[bnt+1-d]);
//			printf("L: add [%d, %d] => %d\n", x[i]+1, bnt-d, x[i]);
			L.add(1, 1, bnt, x[i] + 1, bnt - d, x[i]);
//			puts("==============");
		} else { // L
			int d = L.ask(1, 1, bnt, x[i]);
//			printf("%d,%d ", d, x[i]);
			printf("%d\n", b[x[i]] - b[d]);
//			printf("U: add [%d, %d] => %d\n", d+1, x[i]-1, x[i]);
			U.add(1, 1, bnt, d + 1, x[i] - 1, bnt+1-x[i]);
//			puts("--------------");
		}
		// Oh my fucking shit.
	}
	return 0;
}

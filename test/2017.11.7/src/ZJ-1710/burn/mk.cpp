#include <bits/stdc++.h>
using namespace std;
int main() {
	srand(time(0));
	freopen("burn.in", "w", stdout);
	int n = 1000000000, m = 100000;
	cout << n << " " << m << endl;
	while (m --) {
		int x = rand()*rand() % n + 1;
		cout << x << " " << n + 1 - x << " " << (rand() & 1 ? 'U' : 'L') << endl;
	}
}
#pragma GCC optimize 2
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("burn.in");
ofstream fout("burn.out");
const int N=100005;
int x[N],y[N];
bool r[N],c[N];
inline void update(int& x,int y)
{
	if(y>x)
		x=y;
}
struct segtree
{
	struct node
	{
		int max,lazy;
	}tree[N*4];
	inline void pushdown(int id)
	{
		if(tree[id].lazy)
		{
			update(tree[id*2].lazy,tree[id].lazy);
			update(tree[id*2].max,tree[id].lazy);
			update(tree[id*2+1].lazy,tree[id].lazy);
			update(tree[id*2+1].max,tree[id].lazy);
			tree[id].lazy=0;
		}
	}
	void cover(int id,int l,int r,int L,int R,int val)
	{
		if(L>R)
			return;
		if(L<=l&&R>=r)
		{
			update(tree[id].lazy,val);
			update(tree[id].max,val);
		}
		else
		{
			pushdown(id);
			int mid=(l+r)/2;
			if(L<=mid)
				cover(id*2,l,mid,L,R,val);
			if(R>mid)
				cover(id*2+1,mid+1,r,L,R,val);
			tree[id].max=max(tree[id*2].max,tree[id*2+1].max);
		}
	}
	int query(int id,int l,int r,int x)
	{
		if(l==r)
			return tree[id].max;
		pushdown(id);
		int mid=(l+r)/2;
		if(x<=mid)
			return query(id*2,l,mid,x);
		return query(id*2+1,mid+1,r,x);
	}
}row,col;
struct quest
{
	int x,y,type;
}q[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		char c;
		fin>>q[i].y>>q[i].x>>c;
		q[i].type=c=='U';
		x[i]=q[i].x;y[i]=q[i].y;
	}
	x[++m]=0;y[m]=0;
	sort(x+1,x+m+1);
	int xn=unique(x+1,x+m+1)-x-1;
	sort(y+1,y+m+1);
	int yn=unique(y+1,y+m+1)-y-1;
	for(int i=1;i<m;i++)
		if(q[i].type)
		{
			int t=lower_bound(y+1,y+yn+1,q[i].y)-y;
			if(c[t])
			{
				fout<<0<<endl;
				continue;
			}
			int top=col.query(1,1,m,t);
			fout<<q[i].x-x[top]<<endl;
			row.cover(1,1,m,top+1,lower_bound(x+1,x+xn+1,q[i].x)-x,t);
			c[t]=true;
		}
		else
		{
			int t=lower_bound(x+1,x+xn+1,q[i].x)-x;
			if(r[t])
			{
				fout<<0<<endl;
				continue;
			}
			int left=row.query(1,1,m,t);
			fout<<q[i].y-y[left]<<endl;
			col.cover(1,1,m,left+1,lower_bound(y+1,y+yn+1,q[i].y)-y,t);
			r[t]=true;
		}
	return 0;
}

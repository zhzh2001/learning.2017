#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
const int N=100005,SZ=1e6;
FILE *fin=fopen("burn.in","r"),*fout=fopen("burn.out","w");
char buf[SZ],*p=buf,*pend=buf;
inline char nextchar()
{
	if(p==pend)
	{
		pend=buf+fread(buf,1,SZ,fin);
		p=buf;
	}
	return *p++;
}
inline void read(int& x)
{
	char c;
	for(c=nextchar();isspace(c);c=nextchar());
	x=0;
	for(;isdigit(c);c=nextchar())
		x=x*10+c-'0';
}
inline void read(char& c)
{
	for(c=nextchar();isspace(c);c=nextchar());
}
inline void nextchar(char c)
{
	if(p==pend)
	{
		fwrite(buf,1,SZ,fout);
		p=buf;
	}
	*p++=c;
}
int dig[10];
inline void writeln(int x)
{
	int len=0;
	do
		dig[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		nextchar(dig[len]+'0');
	nextchar('\n');
}
int x[N],y[N];
bool r[N],c[N];
inline void update(int& x,int y)
{
	if(y>x)
		x=y;
}
struct segtree
{
	int tree[N*4];
	inline void pushdown(int id)
	{
		if(tree[id])
		{
			update(tree[id*2],tree[id]);
			update(tree[id*2+1],tree[id]);
			tree[id]=0;
		}
	}
	void cover(int id,int l,int r,int L,int R,int val)
	{
		if(L>R)
			return;
		if(L<=l&&R>=r)
			update(tree[id],val);
		else
		{
			pushdown(id);
			int mid=(l+r)/2;
			if(L<=mid)
				cover(id*2,l,mid,L,R,val);
			if(R>mid)
				cover(id*2+1,mid+1,r,L,R,val);
		}
	}
	int query(int id,int l,int r,int x)
	{
		if(l==r)
			return tree[id];
		pushdown(id);
		int mid=(l+r)/2;
		if(x<=mid)
			return query(id*2,l,mid,x);
		return query(id*2+1,mid+1,r,x);
	}
}row,col;
struct quest
{
	int x,y,type;
}q[N];
int main()
{
	int n,m;
	read(n);read(m);
	for(int i=1;i<=m;i++)
	{
		char c;
		read(q[i].y);read(q[i].x);read(c);
		q[i].type=c=='U';
		x[i]=q[i].x;y[i]=q[i].y;
	}
	x[++m]=0;y[m]=0;
	sort(x+1,x+m+1);
	int xn=unique(x+1,x+m+1)-x-1;
	sort(y+1,y+m+1);
	int yn=unique(y+1,y+m+1)-y-1;
	p=buf;pend=buf+SZ;
	for(int i=1;i<m;i++)
		if(q[i].type)
		{
			int t=lower_bound(y+1,y+yn+1,q[i].y)-y;
			if(c[t])
			{
				writeln(0);
				continue;
			}
			int top=col.query(1,1,m,t);
			writeln(q[i].x-x[top]);
			row.cover(1,1,m,top+1,lower_bound(x+1,x+xn+1,q[i].x)-x,t);
			c[t]=true;
		}
		else
		{
			int t=lower_bound(x+1,x+xn+1,q[i].x)-x;
			if(r[t])
			{
				writeln(0);
				continue;
			}
			int left=row.query(1,1,m,t);
			writeln(q[i].y-y[left]);
			col.cover(1,1,m,left+1,lower_bound(y+1,y+yn+1,q[i].y)-y,t);
			r[t]=true;
		}
	fwrite(buf,1,p-buf,fout);
	return 0;
}

#include <fstream>
#include <algorithm>
#include <cstdlib>
using namespace std;
ifstream fin("bright.in");
ofstream fout("bright.out");
const int N = 105;
int n, t = 1000, a[N], l[N], x[N * 3], lit[N][3], vis[N * 3], ans;
int main()
{
	fin >> n;
	int m = 0;
	for (int i = 1; i <= n; i++)
	{
		fin >> a[i] >> l[i];
		x[++m] = a[i] - l[i];
		x[++m] = a[i];
		x[++m] = a[i] + l[i];
	}
	sort(x + 1, x + m + 1);
	for (int i = 1; i <= n; i++)
	{
		lit[i][0] = lower_bound(x + 1, x + m + 1, a[i] - l[i]) - x;
		lit[i][1] = lower_bound(x + 1, x + m + 1, a[i]) - x;
		lit[i][2] = lower_bound(x + 1, x + m + 1, a[i] + l[i]) - x;
	}
	while (t--)
	{
		for (int i = 1; i < m; i++)
			vis[i] = 0;
		for (int i = 1; i <= n; i++)
			if (rand() & 1)
				for (int j = lit[i][0]; j < lit[i][1]; j++)
					vis[j]++;
			else
				for (int j = lit[i][1]; j < lit[i][2]; j++)
					vis[j]++;
		int now = 0;
		for (int i = 1; i < m; i++)
			if (vis[i])
				now += x[i + 1] - x[i];
		ans = max(ans, now);
	}
	fout << ans << endl;
	return 0;
}
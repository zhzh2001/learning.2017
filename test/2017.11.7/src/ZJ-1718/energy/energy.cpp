#include <fstream>
using namespace std;
ifstream fin("energy.in");
ofstream fout("energy.out");
int main()
{
	int n, m;
	fin >> n >> m;
	if (m >= n || m == 1 || m > 3)
	{
		fout << -1 << endl;
		return 0;
	}
	if (m == 2)
	{
		if (n <= 4)
		{
			fout << -1 << endl;
			return 0;
		}
		fout << n - 1 << endl;
		for (int i = 1; i < n; i++)
			fout << i << ' ' << i + 1 << endl;
	}
	else
	{
		fout << n - 1 << endl;
		for (int i = 3; i <= n; i++)
			fout << 1 << ' ' << i << endl;
		fout << 2 << ' ' << n << endl;
	}
	return 0;
}
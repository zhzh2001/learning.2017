#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxm=100012;
inline void tense(int &x,int y){
	if (x<y)
		x=y;
}
struct segtree{
	int n;
	int tag[maxm*4];
	void init(int n){
		this->n=n;
	}
	int ul,ur,uv;
	void update(int o,int l,int r){
		if (ul<=l && ur>=r){
			tense(tag[o],uv);
			return;
		}
		int mid=(l+r)/2;
		if (ul<=mid)
			update(o*2,l,mid);
		if (ur>mid)
			update(o*2+1,mid+1,r);
	}
	void modify(int l,int r,int v){
		if (l>r)
			return;
		ul=l,ur=r,uv=v;
		//printf("modify %d %d\n",l,r);
		update(1,1,n);
	}
	int p;
	int query(int o,int l,int r){
		if (l==r)
			return tag[o];
		int res=tag[o],mid=(l+r)/2;
		if (p<=mid)
			tense(res,query(o*2,l,mid));
		else tense(res,query(o*2+1,mid+1,r));
		return res;
	}
	int query(int p){
		this->p=p;
		return query(1,1,n);
	}
}t1,t2;
struct query{
	bool typ;
	int x;
}q[maxm];
int nums[maxm];
bool flag[maxm];
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=m;i++){
		int x=readint(),y=readint();
		nums[i]=q[i].x=y;
		q[i].typ=readchar()=='U';
	}
	sort(nums+1,nums+m+1);
	int cur=unique(nums+1,nums+m+1)-nums-1;
	t1.init(cur);
	t2.init(cur);
	for(int i=1;i<=m;i++){
		int qwq=lower_bound(nums+1,nums+cur+1,q[i].x)-nums;
		if (flag[qwq]){
			puts("0");
			continue;
		}
		flag[qwq]=true;
		if (!q[i].typ){
			int to=t1.query(qwq);
			printf("%d\n",n+1-q[i].x-to);
			int tat=to?lower_bound(nums+1,nums+cur+1,n+1-to)-nums:cur+1;
			t2.modify(qwq,tat-1,q[i].x);
		}else{
			int to=t2.query(qwq);
			printf("%d\n",q[i].x-to);
			int tat=to?lower_bound(nums+1,nums+cur+1,to)-nums:0;
			t1.modify(tat+1,qwq,n+1-q[i].x);
		}
	}
}
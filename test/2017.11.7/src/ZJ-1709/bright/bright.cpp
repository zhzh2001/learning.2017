#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct light{
	int x,l,l1,r1;
	bool operator <(const light& rhs)const{
		return x<rhs.x;
	}
}a[102];
int nums[303],cur;
int sum[303];
int main(){
	init();
	int n=readint();
	for(int i=0;i<n;i++){
		a[i].x=readint(),a[i].l=readint();
		//printf("%d %d %d\n",a[i].x-a[i].l,a[i].x,a[i].x+a[i].l);
		nums[++cur]=a[i].x;
		nums[++cur]=a[i].x-a[i].l;
		nums[++cur]=a[i].x+a[i].l;
	}
	sort(a,a+n);
	sort(nums+1,nums+cur+1);
	cur=unique(nums+1,nums+cur+1)-nums-1;
	if (n>20){
		printf("%d",nums[cur]-nums[1]);
		return 0;
	}
	/*for(int i=1;i<=cur;i++)
		printf("%d ",nums[i]);
	putchar('\n');*/
	for(int i=0;i<n;i++){
		//printf("%d %d %d\n",a[i].x-a[i].l,a[i].x,a[i].x+a[i].l);
		a[i].l1=lower_bound(nums+1,nums+cur+1,a[i].x-a[i].l)-nums;
		a[i].r1=lower_bound(nums+1,nums+cur+1,a[i].x+a[i].l)-nums;
		a[i].x=lower_bound(nums+1,nums+cur+1,a[i].x)-nums;
		//printf("%d %d %d\n",a[i].l1,a[i].x,a[i].r1);
	}
	int ans=0;
	for(int i=0;i<(1<<n);i++){
		for(int j=0;j<=cur;j++)
			sum[j]=0;
		for(int j=0;j<n;j++)
			if (i>>j&1){
				//printf("add %d 1\n",);
				sum[a[j].l1]++;
				sum[a[j].x]--;
			}else{
				//if (!i)
					//printf("add [%d,%d]\n",a[j].x,a[j].r1);
				sum[a[j].x]++;
				sum[a[j].r1]--;
			}
		int now=0,res=0;
		for(int j=1;j<=cur;j++){
			now+=sum[j];
			if (now>0)
				res+=nums[j+1]-nums[j];
		}
		ans=max(ans,res);
	}
	printf("%d",ans);
}
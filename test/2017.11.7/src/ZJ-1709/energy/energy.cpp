#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int main(){
	init();
	int n=readint(),m=readint();
	if (m==2){
		if (n<=4)
			puts("-1");
		else{
			printf("%d\n",n-1);
			for(int i=1;i<n;i++)
				printf("%d %d\n",i,i+1);
		}
	}else if (m==3){
		if (n<=3)
			return puts("-1"),0;
		printf("%d\n",(n-1)*(n-2)/2);
		for(int i=1;i<n;i++)
			for(int j=i+1;j<n;j++)
				if (i!=1 || j!=2)
					printf("%d %d\n",i,j);
		printf("%d 1\n",n);
	}else puts("-1");
}
//#include<iostream>
#include<algorithm>
#include<math.h>
#include<stdio.h>
#include<fstream>
using namespace std;
ifstream fin("bright.in");
ofstream fout("bright.out");
int n;
struct bright{
	int a,l;
}ll[103];
inline bool cmp(const bright a,const bright b){
	return a.a<b.a;
}
inline int min(const int a,const int b){
	if(a<b) return a;
	return b;
}
inline int max(const int a,const int b){
	if(a>b) return a;
	return b;
}
long long sum=0,anss=0;
inline void baoli(){
	sort(ll,ll+n,cmp);
	for(int i=0;i<=((1<<n)-1);i++){
		long long ans=0;
		for(int j=0;j<=n-1;j++){
			for(int k=j+1;k<=n-1;k++){
				if(((1<<j)&i)){
					if(((1<<k)&i)){
						ans+=min(ll[k].l,max(0,ll[j].a+ll[j].l-(ll[k].a)));
					}else{
						ans+=min(ll[k].a-ll[j].a,min(ll[k].l,min(ll[j].l,max(0,ll[j].a+ll[j].l-(ll[k].a-ll[k].l)))));
					}
				}else{
					if(((1<<k)&i)){
						//ans+=max(0,light[k].a-light[j].a);
					}else{
						ans+=min(ll[j].l,max(0,ll[j].a-(ll[k].a-ll[k].l)));
					}
				}
			}
		}
		anss=max(anss,sum-ans);
	}
	fout<<anss<<endl;
}
int main(){
	fin>>n;
	for(int i=0;i<=n-1;i++){
		fin>>ll[i].a>>ll[i].l;
		sum+=ll[i].l;
	}
	if(n<=20){
		baoli();
		return 0;
	}
	fout<<sum<<endl;
	return 0;
}
/*

in:
4
1 2
3 3
4 3
6 2

out:
9

*/

;//#include<iostream>
#include<algorithm>
#include<math.h>
#include<stdio.h>
#include<fstream>
#include<string.h>
using namespace std;
ifstream fin("burn.in");
ofstream fout("burn.out");
struct heihei{
	int x,y,id;
	char c;
}opt[1000003];
int ans[100003];
inline bool cmp1(heihei a,heihei b){return a.x<b.x;}
inline bool cmp2(heihei a,heihei b){return a.y<b.y;}
inline bool cmp3(heihei a,heihei b){return a.id<b.id;}
int xx[1000003],totx,yy[1000003],toty;
int n,m;
int tong[1000003],tong2[1000003];
struct tree{
	int mn,tag;
}t1[4000003],t2[4000003];
inline void pushdown1(int rt){
	if(t1[rt].tag!=-9999999){
		t1[rt*2].tag=max(t1[rt].tag,t1[rt*2].tag);
		t1[rt*2+1].tag=max(t1[rt].tag,t1[rt*2+1].tag);
		t1[rt*2].mn=max(t1[rt].mn,t1[rt*2].tag);
		t1[rt*2+1].mn=max(t1[rt].mn,t1[rt*2+1].tag);
		t1[rt].tag=-9999999;
	}
}
inline void pushdown2(int rt){
	if(t2[rt].tag!=-9999999){
		t2[rt*2].tag=max(t2[rt].tag,t2[rt*2].tag);
		t2[rt*2+1].tag=max(t2[rt].tag,t2[rt*2+1].tag);
		t2[rt*2].mn=max(t2[rt].mn,t2[rt*2].tag);
		t2[rt*2+1].mn=max(t2[rt].mn,t2[rt*2+1].tag);
		t2[rt].tag=-9999999;
	}
}
void build1(int rt,int l,int r){
	t1[rt].tag=-9999999;
	t1[rt].mn=-9999999;
	if(l==r){
		t1[rt].mn=1;return;
	}
	int mid=(l+r)/2;
	build1(rt*2,l,mid),build1(rt*2+1,mid+1,r);
	//t1[rt].mn=min(t1[rt*2].mn,t1[rt*2+1].mn);
}
void update1(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t1[rt].tag=max(t1[rt].tag,val),t1[rt].mn=max(t1[rt].mn,val);
		return;
	}
	pushdown1(rt);
	int mid=(l+r)/2;
	if(y<=mid){
		update1(rt*2,l,mid,x,y,val);
	}else if(x>mid){
		update1(rt*2+1,mid+1,r,x,y,val);
	}else{
		update1(rt*2,l,mid,x,mid,val),update1(rt*2+1,mid+1,r,mid+1,y,val);
	}
}
int query1(int rt,int l,int r,int x){
	if(l==r){
		return t1[rt].mn;
	}
	pushdown1(rt);
	int mid=(l+r)/2;
	if(x<=mid){
		return query1(rt*2,l,mid,x);
	}else{
		return query1(rt*2+1,mid+1,r,x);
	}
}
void build2(int rt,int l,int r){
	t2[rt].tag=-9999999;
	t2[rt].mn=-9999999;
	if(l==r){
		t2[rt].mn=1;return;
	}
	int mid=(l+r)/2;
	build2(rt*2,l,mid),build2(rt*2+1,mid+1,r);
	//t2[rt].mn=min(t2[rt*2].mn,t2[rt*2+1].mn);
}
void update2(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t2[rt].tag=max(t2[rt].tag,val),t2[rt].mn=max(t2[rt].mn,val);
		return;
	}
	pushdown2(rt);
	int mid=(l+r)/2;
	if(y<=mid){
		update2(rt*2,l,mid,x,y,val);
	}else if(x>mid){
		update2(rt*2+1,mid+1,r,x,y,val);
	}else{
		update2(rt*2,l,mid,x,mid,val),update2(rt*2+1,mid+1,r,mid+1,y,val);
	}
}
int query2(int rt,int l,int r,int x){
	if(l==r){
		return t2[rt].mn;
	}
	pushdown2(rt);
	int mid=(l+r)/2;
	if(x<=mid){
		return query2(rt*2,l,mid,x);
	}else{
		return query2(rt*2+1,mid+1,r,x);
	}
}
int mp[5003][5003];
inline void baoli(){
	for(int i=1;i<=n;i++){
		mp[0][i]=1,mp[i][0]=1;
	}
	mp[0][0]=1;
	for(int i=1;i<=m;i++){
		if(opt[i].c=='U'){
			int xx=opt[i].x,yy=opt[i].y,lei=1;
			if(mp[xx][yy]){
				fout<<0<<endl;
				continue;
			}
			while(yy>=1&&mp[xx][yy-1]==0){
				mp[xx][yy]=1;
				xx,yy--;
				lei++;
			}
			mp[xx][yy]=1;
			fout<<lei<<endl;
		}else{
			int xx=opt[i].x,yy=opt[i].y,lei=1;
			if(mp[xx][yy]){
				fout<<0<<endl;
				continue;
			}
			while(xx>=1&&mp[xx-1][yy]==0){
				mp[xx][yy]=1;
				xx--,yy;
				lei++;
			}
			mp[xx][yy]=1;
			fout<<lei<<endl;
		}
	}
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=m;i++){
		fin>>opt[i].x>>opt[i].y>>opt[i].c;
		opt[i].id=i;
	}
	/*
	if(n<=5000&&(long long)n*(long long)m<=(long long)50000000){
		baoli();
		return 0;
	}
	*/
	/*
	sort(opt+1,opt+m+1,cmp1);
	for(int i=2;i<=m;i++){
		if(opt[i].x!=opt[i-1].x){
			xx[++totx]=opt[i].x;
		}
	}
	*/
	build1(1,1,n);
	sort(opt+1,opt+m+1,cmp1);
	/*
	for(int i=2;i<=m;i++){
		if(opt[i].y!=opt[i-1].y){
			yy[++toty]=opt[i].y;
		}
	}
	*/
	build2(1,1,n);
	sort(opt+1,opt+m+1,cmp3);
	for(int i=1;i<=m;i++){
		if(opt[i].c=='U'){
			int xxx=query1(1,1,n,opt[i].x);
			xxx=max(xxx,tong[opt[i].x]);
			tong[opt[i].x]=max(tong[opt[i].x],opt[i].y);
			ans[opt[i].id]=opt[i].y-xxx+1;
			if(xxx>0&&xxx<=opt[i].y&&xxx<=n){
				update2(1,1,n,xxx,opt[i].y,opt[i].x+1);
			}
		}else{
			int xxx=query2(1,1,n,opt[i].y);
			xxx=max(xxx,tong2[opt[i].y]);
			tong2[opt[i].y]=max(tong2[opt[i].y],opt[i].x);
			ans[opt[i].id]=opt[i].x-xxx+1;
			if(xxx<=opt[i].x&&xxx>0&&xxx<=n){
				update1(1,1,n,xxx,opt[i].x,opt[i].y+1);
			}
		}
	}
	for(int i=1;i<=m;i++){
		fout<<ans[i]<<endl;
	}
	return 0;
}
/*

in:
6 5
3 4 U
6 1 L
2 5 L
1 6 U
4 3 U

out:
4
3
2
1
2

in:
10 6
2 9 U
10 1 U
1 10 U
8 3 L
10 1 L
6 5 U

1000 22
824 177 L
193 808 U
59 942 U
151 850 U
466 535 U
386 615 U
562 439 U
761 240 L
573 428 L
544 457 U
87 914 U
361 640 L
158 843 U
302 699 L
824 177 U
19 982 U
948 53 U
825 176 L
913 88 L
840 161 L
247 754 L
580 421 U
*/

#include<cstdio>
using namespace std;
const int N=5001000;
int i,j,k,n,m,ch,x,y;
struct tree { tree *ls,*rs;int ma;} *rt1,*rt2,*cur,T[N];
void R(int &x) {
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
}
void W(int x) {
	if (x>=10) W(x/10);
	putchar(x%10+'0');
}
tree *T_new() {
	cur++;
	cur->ls=cur->rs=T;
	cur->ma=0;
	return cur;
}
int max(const int &x,const int &y) {
	if (x<y) return y;
	return x;
}
void T_change(int L,int R,int l,int r,int ma,tree *&k) {
	if (k==T) k=T_new();
	if (L==l && R==r) {
		k->ma=max(k->ma,ma);
		return;
	}
	int mid=(L+R)>>1;
	if (r<=mid) T_change(L,mid,l,r,ma,k->ls);
	else {
		if (l>mid) T_change(mid+1,R,l,r,ma,k->rs);
		else T_change(L,mid,l,mid,ma,k->ls),T_change(mid+1,R,mid+1,r,ma,k->rs);
	}
}
int T_max(int L,int R,int x,tree *k) {
	if (k==T) return 0;
	if (L==R) return k->ma;
	int mid=(L+R)>>1;
	if (x<=mid) return max(k->ma,T_max(L,mid,x,k->ls));
	return max(k->ma,T_max(mid+1,R,x,k->rs));
}
int main() {
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	R(n);R(m);
	rt1=rt2=cur=T;
	for (i=1;i<=m;i++) {
		R(y);R(x);
		ch=getchar();
		while (ch!='L' && ch!='U') ch=getchar();
		if (ch=='L') {
			int t=T_max(1,n,x,rt1);
			W(y-t);puts("");
			if (t<y) T_change(1,n,t+1,y,x,rt2);
			T_change(1,n,x,x,y,rt1);
		}
		else {
			int t=T_max(1,n,y,rt2);
			W(x-t);puts("");
			if (t<x) T_change(1,n,t+1,x,y,rt1);
			T_change(1,n,y,y,x,rt2);
		}
	}
}

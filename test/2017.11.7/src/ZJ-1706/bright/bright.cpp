#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <iostream>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int l,r,p;};
inline bool cmp(ppap a,ppap b){return a.r==b.r?a.l<b.l:a.r<b.r;}
int b[300010],p[100010],q[100010];
int l[100010],m[100010],r[100010],cnt=0;
int n,a[100010],ans=0,f[2010][110];
inline void dfs(int x){
	if(x==n+1){
		int sum=0,s=0;int la=0;
		for(int i=1;i<=cnt;i++){
			int S=s;s+=a[i];if(!S&&s)la=i;
			if(S&&!s){
				int ll=b[la],nn=b[i];
				sum+=nn-ll;
			}
		}
		ans=max(ans,sum);return;
	}
	a[l[x]]++;a[m[x]]--;
	dfs(x+1);
	a[l[x]]--;a[m[x]]++;a[m[x]]++;a[r[x]]--;
	dfs(x+1);
	a[r[x]]++;a[m[x]]--;
}
signed main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	if(n>30){
		ppap a[110];
		for(int i=1;i<=n;i++){
			int x=read(),l=read();
			a[++cnt]=(ppap){x-l,x,i};
			a[++cnt]=(ppap){x,x+l,i};
		}
		sort(a+1,a+cnt+1,cmp);int ans=0;
		for(int i=1;i<=cnt;i++){
			ans=max(ans,a[i].r-a[i].l);
			for(int j=1;j<=n;j++)if(j!=a[i].p)f[i][j]=a[i].r-a[i].l;
			for(int j=1;j<i;j++)if(a[j].p!=a[i].p){
				if(a[j].l>=a[i].l||a[j].r==a[i].r)continue;
				for(int k=1;k<=n;k++)if(k!=a[j].p&&k!=a[i].p){
					f[i][k]=max(f[i][k],f[j][a[i].p]+a[i].r-a[i].l-max(0ll,a[j].r-a[i].l));
					ans=max(ans,f[i][k]);
				}
			}
		}
		writeln(ans);
		return 0;
	}
	for(int i=1;i<=n;i++){
		int x=read(),l=read();
		p[i]=x;q[i]=l;
		b[++cnt]=x-l;b[++cnt]=x;b[++cnt]=x+l;
	}
	sort(b+1,b+cnt+1);cnt=unique(b+1,b+cnt+1)-b-1;
	for(int i=1;i<=n;i++){
		l[i]=lower_bound(b+1,b+cnt+1,p[i]-q[i])-b;
		m[i]=lower_bound(b+1,b+cnt+1,p[i])-b;
		r[i]=lower_bound(b+1,b+cnt+1,p[i]+q[i])-b;
	}
	dfs(1);
	writeln(ans);
}

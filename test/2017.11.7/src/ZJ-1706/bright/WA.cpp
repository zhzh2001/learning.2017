#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <iostream>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int l,r,p;}a[110];
inline bool cmp(ppap a,ppap b){return a.r==b.r?a.l<b.l:a.r<b.r;}
int n,cnt=0,f[2010][110];
signed main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		int x=read(),l=read();
		a[++cnt]=(ppap){x-l,x,i};
		a[++cnt]=(ppap){x,x+l,i};
	}
	sort(a+1,a+cnt+1,cmp);int ans=0;
	for(int i=1;i<=cnt;i++){
		ans=max(ans,a[i].r-a[i].l);
		for(int j=1;j<=n;j++)if(j!=a[i].p)f[i][j]=a[i].r-a[i].l;
		for(int j=1;j<i;j++)if(a[j].p!=a[i].p){
			if(a[j].l>=a[i].l||a[j].r==a[i].r)continue;
			for(int k=1;k<=n;k++)if(k!=a[j].p&&k!=a[i].p){
				f[i][k]=max(f[i][k],f[j][a[i].p]+a[i].r-a[i].l-max(0ll,a[j].r-a[i].l));
				ans=max(ans,f[i][k]);
			}
		}
	}
	writeln(ans);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <iostream>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int x,y;}q[1000010],anss[1000010];
int dist[11];
int n,m,a[1010][1010],cnt=0,ans;
bool flag=0;
inline void get(){
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)if(a[i][j]==1)anss[++ans]=(ppap){i,j};
	flag=1;
}
int Q[1010];
inline void bfs(int op,int s){
	memset(dist,-1,sizeof dist);
	int l=1,r=1;Q[1]=s;dist[s]=0;
	while(l<=r){
		int now=Q[l++];
		for(int i=1;i<=n;i++)if(dist[i]==-1&&a[now][i]==op){
			dist[i]=dist[now]+1;Q[++r]=i;
		}
	}
}
inline int zj(int x){
	bfs(x,1);int ma=0,p;
	for(int i=1;i<=n;i++)if(dist[i]>ma)ma=dist[i],p=i;
	bfs(x,p);ma=0;
	for(int i=1;i<=n;i++)if(dist[i]>ma)ma=dist[i],p=i;
	return ma;	
}
inline void dfs(int x){
	if(flag)return;
	if(x==cnt+1){
		int kx=zj(1),ky=zj(2);
		if(min(kx,ky)==m)get();
		return;
	}
	a[q[x].x][q[x].y]=a[q[x].y][q[x].x]=1;
	dfs(x+1);
	a[q[x].x][q[x].y]=a[q[x].y][q[x].x]=2;
	dfs(x+1);
}
int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();m=read();
	if(n<4){
		if(n==1&&m==0)puts("0");
		else puts("-1");return 0;
	}
	if(m==-1)return puts("0")&0;
	for(int i=1;i<n;i++)a[i][i+1]=a[i+1][i]=1;
	for(int i=1;i<=n;i+=2)a[i][i+2]=a[i+2][i]=2;
	for(int i=2;i<=n;i+=2)a[i][i+2]=a[i+2][i]=2;
	if(n&1)a[1][n-1]=a[n-1][1]=2;
	else a[1][n]=a[n][1]=2;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)if(!a[i][j])q[++cnt]=(ppap){i,j};
	dfs(1);
	if(ans==0)return puts("-1")&0;
	writeln(ans);
	for(int i=1;i<=ans;i++)write(anss[i].x),putchar(' '),writeln(anss[i].y);
	return 0;
}

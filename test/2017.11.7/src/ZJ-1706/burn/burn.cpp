#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <iostream>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
bool f[5010][5010];
int n,m;
int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read();m=read();
	if(n<=5000){
		for(int i=1;i<=m;i++){
			int y=read(),x=read();char c[5];scanf("%s",c);
			int sum=0;
			if(c[0]=='U')while(x&&!f[x][y])sum++,f[x][y]=1,x--;
			else while(y&&!f[x][y])sum++,f[x][y]=1,y--;
			writeln(sum);
		}
	}else{
		for(int i=1;i<=m;i++){
			int x=read(),y=read();char c[5];scanf("%s",c);
			if(c[0]=='L')writeln(x);
			else writeln(y);
		}
	}
	return 0;
}

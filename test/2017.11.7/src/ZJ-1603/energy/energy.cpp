#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1010,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m;
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	int i;
	n=read();m=read();
	if(m==2){
		if(n<=4)goto out;
		printf("%d\n",n-1);
		for(i=1;i<=n-4;i++)
		printf("%d %d\n",i,n);
		printf("%d %d\n",n-3,n-2);
		printf("%d %d\n",n-2,n-1);
		printf("%d %d\n",n-1,n);
		return 0;
	}
	if(m==3){
		if(n<=3)goto out;
		printf("%d\n",n-1);
		for(i=1;i<=n-3;i++)
		printf("%d %d\n",i,n);
		printf("%d %d\n",n-2,n-1);
		printf("%d %d\n",n-1,n);
		return 0;
	}
	out:puts("-1");
	return 0;
}

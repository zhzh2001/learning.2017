#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1010,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,ans,X[N],Y[N],f[N][N],d[N][N];
int doit(int c){
	int i,j,k,res=0;
	for(i=1;i<=n;i++)
	for(j=i+1;j<=n;j++)
	if(f[i][j]==c)d[i][j]=d[j][i]=1;
	else d[i][j]=d[j][i]=1e9;
	for(k=1;k<=n;k++)
	for(i=1;i<=n;i++)
	for(j=1;j<=n;j++)
	d[i][j]=min(d[i][j],d[i][k]+d[j][k]);
	for(i=1;i<=n;i++)
	for(j=1;j<=n;j++)
	res=max(res,d[i][j]);
	if(res==1e9)return -1;
	return res;
}
void work(){
	int res=min(doit(1),doit(0));
	if(res==m){
		doit(1);
		doit(0);
		int i,j;
		for(i=1;i<=n;i++)
		for(j=i+1;j<=n;j++)
		if(f[i][j]){
			ans++;
			X[ans]=i;
			Y[ans]=j;
		}
	}
}
void dfs(int x,int y){
	if(ans)return;
	if(y>n){
		x++;y=x+1;
	}
	if(x==n){
		work();
		return;
	}
	f[x][y]=0;
	dfs(x,y+1);
	f[x][y]=1;
	dfs(x,y+1);
}
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy1.out","w",stdout);
	n=read();m=read();
	int i,k;
	k=(n-1)/2;
	k-=m-2;
	for(i=1;i<=n-k-1;i++){
		f[i][n]=1;
	}
	for(i=n-k;i<n;i++)
	f[i][i+1]=1;
	work();
	dfs(1,2);
	if(ans==0)ans--;
	printf("%d\n",ans);
	if(ans>0){
		for(int i=1;i<=ans;i++)
		printf("%d %d\n",X[i],Y[i]);
	}
	return 0;
}

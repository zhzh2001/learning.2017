#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,i,j,k,x,y,l,r,a[N],o;
int rll(){return rand()<<15|rand();}
int main(){
	freopen("burn.in","w",stdout);
	srand(unsigned(time(0)));
	n=1000;m=1000;
	printf("%d %d\n",n,m);
	for(i=1;i<=m;i++){
		x=rll()%n+1;
		printf("%d %d ",x,n+1-x);
		puts(rand()&1?"U":"L");
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
struct cqz{int l,r;}x[N];
inline bool operator<(cqz i,cqz j){return i.l<j.l;}
int n,a[N],l[N],f[N],ans;
int check(){
	register int i,p=-2e9,res=0;
	for(i=1;i<=n;i++)if(f[i])
	x[i]=(cqz){a[i],a[i]+l[i]-1};
	else x[i]=(cqz){a[i]-l[i],a[i]-1};
	sort(x+1,x+n+1);
	for(i=1;i<=n;i++)
	if(p<=x[i].r){
		if(p<x[i].l)p=x[i].l-1;
		res+=x[i].r-p;p=x[i].r;
	}if(res>ans)ans=res;return res;
}
void dfs(int x){
	if(x>n){
		check();
		return;
	}
	f[x]=1;dfs(x+1);
	f[x]=0;dfs(x+1);
}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright1.out","w",stdout);
	int i;srand(2333);n=read();
	for(i=1;i<=n;i++)
		a[i]=read(),l[i]=read();
	dfs(1);
	printf("%d\n",ans);
	return 0;
}

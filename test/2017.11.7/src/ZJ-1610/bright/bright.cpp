#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=105;
const int INF=1e9+7;

struct node{
	int a,l;
}P[N];
inline bool Cmp(node A,node B){
	return A.a<B.a;
}
inline bool Cmp_int(int A,int B){
	return A<B;
}
int n,Q[N*3],f[N*3][N*3],Sum[N*3][N*3];
inline int find(int val){
	return lower_bound(Q+1,Q+*Q+1,val)-Q;
}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	Rep(i,1,n){
		P[i].a=read(),P[i].l=read();
		Q[++*Q]=P[i].a;
		Q[++*Q]=P[i].a-P[i].l;
		Q[++*Q]=P[i].a+P[i].l;
	}
	sort(Q+1,Q+*Q+1,Cmp_int);
	*Q=unique(Q+1,Q+*Q+1)-Q-1;
	int Ans=0;
	Rep(i,1,*Q){
		Rep(j,0,2*n){
			Rep(k,1,n) if (k!=j){
				int a=find(P[k].a-P[k].l);
				int b=find(P[k].a);
				int c=find(P[k].a+P[k].l);
				if (b==i){
					int t=j,pos;
					if (j>n) pos=P[j-n].a+P[j-n].l;
						else pos=P[j].a;
					if (P[k].a>pos) t=k;
					Rep(p,a,b-1) f[i][t]=max(f[i][t],f[p][j]+Q[i]-Q[p]);
				}
				if (c==i){
					int t=j,pos;
					if (j>n) pos=P[j-n].a+P[j-n].l;
						else pos=P[j].a;
					if (P[k].a+P[k].l>pos) t=n+k;
					Rep(p,b,c-1) f[i][t]=max(f[i][t],f[p][j]+Q[i]-Q[p]);
				}
			}
		}
		Rep(k,0,2*n) Ans=max(Ans,f[i][k]);
//		printf("pos=%d:",Q[i]);
//		Rep(k,0,2*n) printf("%d ",f[i][k]);puts("");
	}
	printf("%d\n",Ans);
}
/*
4
1 2
3 3
4 3
6 2

4
1 1
3 1
4 3
5 4

3
1 1
2 2
3 3
*/

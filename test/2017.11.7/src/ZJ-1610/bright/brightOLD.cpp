#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=105;
const int INF=1e9+7;

struct node{
	int a,l;
}P[N];
inline bool Cmp(node A,node B){
	return A.a<B.a;
}
inline bool Cmp_int(int A,int B){
	return A<B;
}
int n,Q[N*3],f[N*3][N*3],Sum[N*3][N*3];
inline int find(int val){
	return lower_bound(Q+1,Q+*Q+1,val)-Q;
}
int main(){
//	freopen("bright.in","r",stdin);
//	freopen("bright.out","w",stdout);
	n=read();
	Rep(i,1,n){
		P[i].a=read(),P[i].l=read();
		Q[++*Q]=P[i].a;
		Q[++*Q]=P[i].a-P[i].l;
		Q[++*Q]=P[i].a+P[i].l;
	}
	sort(Q+1,Q+*Q+1,Cmp_int);
	*Q=unique(Q+1,Q+*Q+1)-Q-1;
	Rep(i,1,*Q){
		Rep(j,0,n*2){
			Rep(k,1,n) if (k!=j){
				int a=find(P[k].a-P[k].l);
				int b=find(P[k].a);
				int c=find(P[k].a+P[k].l);
				if (a<i && i<=b){
					int t=k;
					if (P[j].a>=P[t].a+P[t].l) t=j;
					f[i][t]=max(f[i][t],Sum[a][j]+Q[i]-Q[a]);
				}
				if (b<i && i<=c){
					f[i][k*2]=max(f[i][k*2],Sum[b][j]+Q[i]-Q[b]);
//					if (i==4 && Sum[b][j]+Q[i]-Q[b]==3) printf("a=%d b=%d c=%d\n",a,b,c);
				}
//				printf("a=%d b=%d c=%d\n",a,b,c);
			}
		}
		printf("pos=%d:",Q[i]);
		Rep(j,0,2*n){
			Sum[i][j]=max(f[i][j],Sum[i-1][j]);printf("%d ",Sum[i][j]);
		}
		puts("");
	}
	int Ans=0;
	Rep(i,0,2*n) Ans=max(Ans,Sum[*Q][i]);
	printf("%d\n",Ans);
}
/*
4
1 2
3 3
4 3
6 2

4
1 1
3 1
4 3
5 4

3
1 1
2 2
3 3
*/

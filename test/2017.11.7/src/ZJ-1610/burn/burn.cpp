#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
inline int Getchar(){
	int ch=getchar();
	while (ch!='U' && ch!='L') ch=getchar();
	return ch;
}
const int N=500005;

int n,m;
struct node{
	int t,x,y,c;
}P[N],P_[N];
int top,Ans[N];
inline bool Cmp_t(node A,node B){
	return A.t<B.t;
}
void Solve(int L,int R,int xl,int xr,int yl,int yr){
	if (L>R) return;
//	printf("xl=%d xr=%d yl=%d yr=%d\n",xl,xr,yl,yr);
	int Mip=L;
	Rep(i,L+1,R) if (P[i].t<P[Mip].t) Mip=i;
//	printf("mx=%d my=%d\n",P[Mip].x,P[Mip].y);
	if (P[Mip].c=='U'){
		top=0;Rep(i,L,R) P_[++top]=P[i];
		node Pm=P[Mip];
		int cop=0;
		Rep(i,1,top) if (P_[i].x<Pm.x) P[L+cop]=P_[i],cop++;
		int mid=L+cop-1;
		Rep(i,1,top) if (P_[i].x>Pm.x) P[L+cop]=P_[i],cop++;
		Ans[Pm.t]=Pm.y-yl+1;
		Solve(L,mid,xl,Pm.x-1,yl,yr);
		Solve(mid+1,cop,Pm.x+1,xr,yl,yr);
		return;
	}
	if (P[Mip].c=='L'){
		top=0;Rep(i,L,R) P_[++top]=P[i];
		node Pm=P[Mip];
		int cop=0;
		Rep(i,1,top) if (P_[i].y<Pm.y) P[L+cop]=P_[i],cop++;
		int mid=L+cop-1;
		Rep(i,1,top) if (P_[i].y>Pm.y) P[L+cop]=P_[i],cop++;
		Ans[Pm.t]=Pm.x-xl+1;
		Solve(L,mid,xl,xr,yl,Pm.y-1);
		Solve(mid+1,cop,xl,xr,Pm.y+1,yr);
		return;
	}
}
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read(),m=read();
	Rep(i,1,m) P[i].x=read(),P[i].y=read(),P[i].c=Getchar(),P[i].t=i;
	Solve(1,m,1,n,1,n);
	Rep(i,1,m) printf("%d\n",Ans[i]);
}
/*
10 6
2 9 U
10 1 U
1 10 U
8 3 L
10 1 L
6 5 U
*/

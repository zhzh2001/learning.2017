#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,m,ned,g[201][201],f[201][201],x,y;
int main()
{
	freopen("energy.out","r",stdin);
	freopen("check.out","w",stdout);
	n=read();m=read();ned=read();
	For(i,1,n)	For(j,1,n)	g[i][j]=1,f[i][j]=1e9;
	For(i,1,m)
	{
		x=read();y=read();
		f[x][y]=1;f[y][x]=1;
		g[x][y]=1e9;g[y][x]=1e9;
	}
	For(k,1,n)
		For(i,1,n)
			For(j,1,n)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]),g[i][j]=min(g[i][j],g[i][k]+g[k][j]);
	int ans1=0,ans2=0;
	For(i,1,n)	For(j,1,n)	ans1=max(ans1,f[i][j]),ans2=max(ans2,g[i][j]);
	if(ans1==1e9||ans2==1e9)	{if(ned==-1)	puts("1");else puts("-1");return 0;}
	if(min(ans1,ans2)==ned)	puts("1");else	puts("-1");
}

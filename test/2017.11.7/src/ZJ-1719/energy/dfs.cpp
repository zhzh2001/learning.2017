#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
struct edge{int x,y;}e[2001];
int n,m,f[201][201],top1,top2,q2[2001],q1[2001],tot;
inline void check()
{
	For(i,1,n)	For(j,1,n)	f[i][j]=1e9;
	For(i,1,n)	f[i][i]=0;
	For(i,1,top1)
	{
		f[e[q1[i]].x][e[q1[i]].y]=1;
		f[e[q1[i]].y][e[q1[i]].x]=1;
	}
	For(k,1,n)
		For(i,1,n)
			For(j,1,n)	f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	int ans1=0;
	For(i,1,n)	For(j,1,n)	if(f[i][j]==1e9)	return;else	ans1=max(ans1,f[i][j]);
	if(ans1==-1)	return;
	
	For(i,1,n)	For(j,1,n)	f[i][j]=1e9;
	For(i,1,n)	f[i][i]=0;
	For(i,1,top2)
	{
		f[e[q2[i]].x][e[q2[i]].y]=1;
		f[e[q2[i]].y][e[q2[i]].x]=1;
	}
	For(k,1,n)
		For(i,1,n)
			For(j,1,n)	f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	int ans2=0;
	For(i,1,n)	For(j,1,n)	if(f[i][j]==1e9)	return;else	ans2=max(ans2,f[i][j]);
	if(ans2==-1)	return;
	if(min(ans1,ans2)==m)
	{
		cout<<n<<' '<<top2<<' '<<m<<endl;
		For(i,1,top2)
			printf("%d %d\n",e[q2[i]].x,e[q2[i]].y);
		exit(0);
	}
	
}
inline void dfs(int x)
{
	if(x==tot+1)	{check();return;}
	q1[++top1]=x;dfs(x+1);top1--;
	q2[++top2]=x;dfs(x+1);top2--;
}
int main()
{
	freopen("check.in","w",stdout);
	n=read();m=read();
	For(i,1,n)
		For(j,i+1,n)
			e[++tot].x=i,e[tot].y=j;
	dfs(1);
}

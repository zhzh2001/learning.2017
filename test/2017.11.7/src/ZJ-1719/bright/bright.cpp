#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<ctime>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int mi,mx,top1,top2,q1[101],q2[101],n,ans,l[101],a[101],tim;
bool vis[10001];
inline void check()
{
	tim++;
	For(i,mi,mx)	vis[i+1001]=0;
	For(i,1,top1)	For(j,a[q1[i]],a[q1[i]]+l[q1[i]]-1)
		vis[j+1001]=1;
	For(i,1,top2)	For(j,a[q2[i]]-l[q2[i]],a[q2[i]]-1)
		vis[j+1001]=1;
	int cnt=0;
	For(i,mi,mx)	if(vis[i+1001])	cnt++;
//	For(i,1,top1)	cout<<q1[i]<<"->"<<a[q1[i]]<<' '<<a[q1[i]]+l[q1[i]]<<endl;
//	For(i,1,top2)	cout<<q2[i]<<"->"<<a[q2[i]]-1<<' '<<a[q2[i]]-l[q2[i]]<<endl;cout<<endl;
	ans=max(ans,cnt);
	if(tim*(mx-mi+1)>=20000000)	{cout<<ans<<endl;exit(0);}
}
inline void dfs(int x)
{
	if(x==n+1)	{check();return;}
	q1[++top1]=x;dfs(x+1);top1--;
	q2[++top2]=x;dfs(x+1);top2--;
}
int main()
{
	freopen("bright.in","r",stdin);freopen("bright.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read(),l[i]=read(),mi=min(mi,a[i]-l[i]),mx=max(mx,a[i]+l[i]);
	dfs(1);
	cout<<ans<<endl;
}
/*
4
1 2
3 3
4 3
6 2
*/

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<vector>
#include<bitset>
#include<string>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=105; int n,f[N][2]; struct T{int loc,l;} a[N];
int used[N],Res,tot,pos[N],num,Ans; map<int,int> vis;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int cmp(T i,T j){return i.loc<j.loc;}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=Read();
	for (int i=1;i<=n;i++)
		a[i].loc=Read(),a[i].l=Read();
	sort(a+1,a+1+n,cmp);
	f[1][0]=f[1][1]=a[1].l;
	for (int i=2;i<=n;i++){
		f[i][0]=max(f[i-1][1]+max(0,a[i].loc-a[i-1].loc-a[i-1].l),
					f[i-1][0]+min(a[i].loc-a[i-1].loc,a[i].l));
		f[i][1]=max(f[i-1][0]+a[i].l-1,f[i-1][1]+min(a[i].l,a[i].loc+a[i].l-a[i-1].loc-a[i-1].l));
	//	printf("%d %d\n",f[i][0],f[i][1]);
	}
	if (n<=20){
		for (int i=0;i<1<<n;i++){
			for (int j=1;j<=n;j++) used[j]=0;
			for (int j=1;j<=n;j++)
				if (i&(1<<(j-1))) used[j]++;
			for (int j=1;j<=n;j++)
				if (used[j]) vis[a[j].loc-a[j].l]++,vis[a[j].loc]--,pos[++tot]=a[j].loc,pos[++tot]=a[j].loc-a[j].l;
				else vis[a[j].loc]++,vis[a[j].loc+a[j].l]--,pos[++tot]=a[j].loc,pos[++tot]=a[j].loc+a[j].l;
			sort(pos+1,pos+1+tot); num=0;
			for (int i=1;i<=tot;i++){
				num+=vis[pos[i]];
				if (i!=1&&num) Res+=pos[i]-pos[i-1];
			}
			Ans=max(Ans,Res); tot=Res=0;
			for (int i=1;i<=tot;i++) vis[pos[i]]=0;
		}
	}
	printf("%d\n",n<=20?Ans:max(f[n][0],f[n][1]));
}
/*
20
978 741
650 530
575 372
675 798
46 166
222 728
771 799
750 749
163 358
172 528
353 142
69 917
839 350
941 122
777 777
614 509
319 505
784 910
153 690
690 237

*/

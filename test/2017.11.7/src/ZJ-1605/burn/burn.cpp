#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<vector>
#include<bitset>
#include<string>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5,M=4e6+5;
int Max1[M],Max2[M],ls[M],rs[M],Ls[M],Rs[M],tot1,tot2;
int rt,Rt,n,m,pos,x,y,k; char s[10]; map<int,int> vis;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int Modify(int &u,int l,int r){
	int mid=l+r>>1; if (!u) u=++tot1;
	if (l>=x&&r<=y){Max1[u]=max(Max1[u],k); return 0;}
	if (x<=mid) Modify(Ls[u],l,mid); if (y>mid) Modify(Rs[u],mid+1,r);
}
int modify(int &u,int l,int r){
	int mid=l+r>>1; if (!u) u=++tot2;
	//printf("modify:%d %d %d\n",u,l,r);
	if (l>=x&&r<=y){Max2[u]=max(Max2[u],k); return 0;}
	if (x<=mid) modify(ls[u],l,mid); if (y>mid) modify(rs[u],mid+1,r);
}
int Query(int u,int l,int r){int mid=l+r>>1;
	pos=max(pos,Max1[u]); if (l==r) return 0;
	if (x<=mid) Query(Ls[u],l,mid); if (x>mid) Query(Rs[u],mid+1,r);
}
int query(int u,int l,int r){int mid=l+r>>1;
	pos=max(pos,Max2[u]); //printf("query:%d %d %d %d %d\n",u,l,r,Max2[u],pos);
	if (l==r) return 0;
	if (x<=mid) query(ls[u],l,mid); if (x>mid) query(rs[u],mid+1,r);
}
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=Read(),m=Read();
	x=1,y=n,k=1,modify(rt,1,n),Modify(Rt,1,n);
	for (int i=1;i<=m;i++){//puts("sd");
		int X=Read(),Y=Read(); scanf("\n%s",s);
		if (!vis[X]) vis[X]=1;
		else {puts("0"); continue;}
		if (s[0]=='U'){
			x=X,pos=0; query(rt,1,n);//printf("%d\n",pos);
			x=X+1,y=n-pos+1,k=X+1; Modify(Rt,1,n);
		//	printf("%d\n",pos);
			printf("%d\n",n-pos-X+2);
		}
		else{
			x=X,pos=0; Query(Rt,1,n); //printf("%d\n",pos);
			x=pos,y=X-1,k=Y+1; //printf("modify:%d %d %d\n",x,y,k);
			 modify(rt,1,n);
			printf("%d\n",X-pos+1);
		}
	}
}
/*
10 6
2 9 U
10 1 U
1 10 U
8 3 L
10 1 L
6 5 U

*/

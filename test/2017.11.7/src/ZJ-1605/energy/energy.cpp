#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<vector>
#include<bitset>
#include<string>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e6+5;
int tot,x[N],y[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	int n=Read(),m=Read();
	if (m==1||m>n/2||m>3){puts("-1"); return 0;}
	for (int i=1;i<=m;i++)
		x[++tot]=i,y[tot]=i+1;
	if (m==2){printf("%d\n",tot); 
	//	printf("%d %d\n",n,m);
		for (int i=1;i<=tot;i++) printf("%d %d\n",x[i],y[i]); return 0;}
	int g=0,loc=1;
	for (int i=m+2;i<=min(m*2+2,n);i++){
		if (loc<=m+1) x[++tot]=i,y[tot]=loc;
		if (loc+1<=m+1) x[++tot]=i,y[tot]=loc+1;
		if (loc+2<=m+1) x[++tot]=i,y[tot]=loc+2;
		if  ((++g)%3==0) loc+=3;
	}
	for (int i=m+2;i<=min(m*2+2,n);i++)
		for (int j=i+2;j<=min(m*2+2,n);j++) x[++tot]=i,y[tot]=j;
	for (int i=min(m*2+2,n);i<n;i++) x[++tot]=i,y[tot]=i+1;
	printf("%d\n",tot);
//	printf("%d %d\n",n,m);
	for (int i=1;i<=tot;i++) printf("%d %d\n",x[i],y[i]);
}

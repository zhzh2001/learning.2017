#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<vector>
#include<bitset>
#include<string>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int INF=1e9+7,N=1005;
int a[N][N],b[N][N],n,m,Limit,Max,Max2;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("energy.out","r",stdin);
	freopen("check.out","w",stdout);
	m=Read();
	if (m==-1){puts("AC"); return 0;}n=Read(),Limit=Read();
	memset(a,63,sizeof(a));
//	printf("%d %d\n",n,m,Limit);
	for (int i=1;i<=m;i++){//printf("%d\n",i);
		int x=Read(),y=Read();// puts("sd");
		b[x][y]=b[y][x]=a[x][y]=a[y][x]=1;
	//	printf("addline%d %d\n",x,y);
	}
//	puts("sd");
	for (int i=1;i<=n;i++) a[i][i]=0;
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				a[i][j]=min(a[i][j],a[i][k]+a[k][j]);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) Max=max(Max,a[i][j]);//printf("%d %d %d\n",i,j,a[i][j]);;
//	printf("%d\n",Max);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			b[i][j]=b[i][j]?INF:1;
	for (int i=1;i<=n;i++) b[i][i]=0;
//	for (int i=1;i<=n;i++)
	//	for (int j=i+1;j<=n;j++) if (b[i][j]==1) printf("edge:%d %d\n",i,j);
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				b[i][j]=min(b[i][j],b[i][k]+b[k][j]);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) Max2=max(Max2,b[i][j]);
	Max=min(Max,Max2);
	if (Max>=INF) Max=-1;
//	printf("%d\n",Max);
	puts(Max==Limit?"AC":"WA");
}

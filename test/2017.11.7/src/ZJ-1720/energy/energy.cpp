#include<bits/stdc++.h>
using namespace std;
namespace cogito{
	inline void write(int x){
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
	}
}
using namespace cogito;
int n,m,x[1000005],y[1000005],tot;
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m!=2&&m!=3&&m!=-1)
		return puts("-1"),0;
	if (m==-1)
		return puts("0"),0;
	if (m>=n)
		return puts("-1"),0;
	if (m==2){
		if (n<=4)
			return puts("-1"),0;
		printf("%d\n",n-1);
		for (int i=1;i<=n-1;i++)
			printf("%d %d\n",i,i+1);
		puts("");
	}
	if (m==3){
		for (int i=1;i<=3;i++)
			for (int j=i;j<=n;j+=4)
				for (int k=i+1;k<=n;k+=4)
					x[++tot]=j,y[tot]=k;
		printf("%d\n",tot);
		for (int i=1;i<=tot;i++)
			write(x[i]),putchar(' '),write(y[i]),puts("");
	}
}

#include<bits/stdc++.h>
using namespace std;
namespace cogito{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline char readch(){
		char ch=gc();
		for (;ch!='U'&&ch!='L';ch=gc());
		return ch;
	}
	inline void write(int x){
		if (!x){
			puts("0");
			return;
		}
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		puts("");
	}
}
using namespace cogito;
const int N=100005;
int n,m,x[N],y[N];
struct que{int x,y,tp;}q[N];
char ch[5];
struct FuckTree{
	int mx[N*4];
	void change(int k,int l,int r,int x,int y,int v){
		if (x>y) return;
		if (l==x&&r==y){
			mx[k]=max(mx[k],v);
			return;
		}
		int mid=(l+r)/2;
		if (y<=mid) change(k*2,l,mid,x,y,v);
		else if (x>mid) change(k*2+1,mid+1,r,x,y,v);
		else change(k*2,l,mid,x,mid,v),change(k*2+1,mid+1,r,mid+1,y,v);
	}
	int ask(int k,int l,int r,int x){
		if (l==r) return mx[k];
		int mid=(l+r)/2;
		if (x<=mid) return max(mx[k],ask(k*2,l,mid,x));
		return max(mx[k],ask(k*2+1,mid+1,r,x));
	}
}t1,t2;
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=m;i++){
		x[i]=q[i].x=read();
		y[i]=q[i].y=read();
		q[i].tp=(readch()=='U'?0:1);
	}
	sort(x+1,x+m+1);
	sort(y+1,y+m+1);
	for (int i=1;i<=m;i++)
		q[i].x=lower_bound(x+1,x+m+1,q[i].x)-x,
		q[i].y=lower_bound(y+1,y+m+1,q[i].y)-y;
	for (int i=1;i<=m;i++)
		if (!q[i].tp){
			int la=t1.ask(1,1,m,q[i].x);
			write(y[q[i].y]-y[la]);
			t1.change(1,1,m,q[i].x,q[i].x,q[i].y);
			t2.change(1,1,m,la+1,q[i].y,q[i].x);
		}
		else{
			int la=t2.ask(1,1,m,q[i].y);
			write(x[q[i].x]-x[la]);
			t2.change(1,1,m,q[i].y,q[i].y,q[i].x);
			t1.change(1,1,m,la+1,q[i].x,q[i].y);
		}
}

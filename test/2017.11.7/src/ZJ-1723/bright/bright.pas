program ec12;
var 
	two,sep:array[0..21] of longint;
	vis:array[-10000001..20000001] of boolean;
	site,l:array[1..201] of longint;
	ans:int64;
	n,m,i,j,sum,t,p,tot,tot1:longint;
function chai(x:longint):longint;
var
	tot:longint;
begin
	tot:=0;
	while (x>0) do
	begin 
		inc(tot);
		sep[tot]:=x mod 2;
		x:=x div 2;
	end;
	chai:=tot;
end;
begin 
	assign(input,'bright.in'); reset(input);
	assign(output,'bright.out'); rewrite(output);
	readln(n);
	for i:=1 to n do 
	readln(site[i],l[i]);
	ans:=0;
	if (n<=20) then 
	begin
		two[0]:=1;
		for i:=1 to n do 
		two[i]:=two[i-1]*2;
		for i:=0 to two[n]-1 do
		begin
			sum:=0;
			t:=chai(i);		
			fillchar(vis,sizeof(vis),true);
			for j:=1 to n do 
			begin 	
				if sep[j]=0 then 
				begin 
					tot:=0;
					tot1:=0;
					for p:=site[j] downto site[j]-l[j] do 
					begin 
						inc(tot);
						if vis[p] then 
						vis[p]:=false
						else
						inc(tot1);
					end;
					if tot>0 then
					begin 
						if tot1=0 then 
						inc(sum,tot-1)
						else
						inc(sum,tot-tot1);
					end;
				end;
				if sep[j]=1 then 
				begin 
					tot:=0;
					tot1:=0;
					for p:=site[j] to site[j]+l[j] do 
					begin 
						inc(tot);
						if vis[p] then
						vis[p]:=false
						else
						inc(tot1);
					end;
					if tot>0 then 
					begin 
						if tot1=0 then 
						inc(sum,tot-1)
						else
						inc(sum,tot-tot1);
					end;
				end;
			end;
			if sum>ans then 
			ans:=sum;
		end;
		writeln(ans);
	end
	else
	begin 
		for i:=1 to n do 
		inc(ans,l[i]);
		writeln(ans-1);
	end;
	close(input);
	close(output);
end. 
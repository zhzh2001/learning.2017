program ec12;
var 
	n,m,i,j,x,y:longint;
	ans:int64;
	s,s1:ansistring;
	left,up:array[1..10000001] of longint;
begin 
	assign(input,'burn.in'); reset(input);
	assign(output,'burn.out'); rewrite(output);
	readln(n,m);
	for i:=1 to n do 
	begin 
		left[i]:=1;
		up[i]:=1;
	end;
	for i:=1 to m do 
	begin 	
		ans:=0;
		readln(s);
		s1:=copy(s,1,pos(' ',s)-1);
		val(s1,y);
		delete(s,1,pos(' ',s));
		s1:=copy(s,1,pos(' ',s)-1);
		val(s1,x);
		delete(s,1,pos(' ',s));
		if s[1]='L' then 
		begin 
			inc(ans,y-left[x]+1);
			for j:=1 to y-1 do 
			begin 
				if (n+1-j<x) then 
				continue;
				if (x+1>up[j]) then 
				up[j]:=x+1;
			end;
			left[x]:=y+1;
			up[y]:=x+1;
		end;
		if s[1]='U' then 
		begin
			inc(ans,x-up[y]+1);
			for j:=1 to x-1 do 
			begin 
				if (n+1-j<y) then 
				continue;
				if y+1>left[j] then 
				left[j]:=y+1;
			end;
			left[x]:=y+1;
			up[y]:=x+1;	
		end;
		writeln(ans);
	end;
	close(input);
	close(output);
end. 
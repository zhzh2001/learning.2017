#include<bits/stdc++.h>
using namespace std;
struct data{
	int x,y;
	char c;
}a[50010];
int n,m,suml;
map<pair<int,int>,int> mp;
bool f[50010];
 void solve1()
{
	for (int i=1;i<=m;++i)
	{
		int ans=0;
		if (f[a[i].x]) continue;
		else f[a[i].x]=1,ans+=n+1-a[i].x;
		printf("%d\n",ans);
	}
}
 int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i)
	{
		scanf("%d%d %c",&a[i].y,&a[i].x,&a[i].c);
		if (a[i].c=='L') ++suml;
	}
	if (suml==m) 
	{
		solve1();
		return 0;
	}
	for (int i=1;i<=m;++i)
	{
		int ans=0;bool flag=0;
		if (a[i].x+a[i].y>n+1) continue;
		pair<int,int> p;
		p=make_pair(a[i].x,a[i].y);
		if (mp[p]) 
		{
			cout<<0<<endl;
			continue;
		}
		char opt=a[i].c;
		if (opt=='L')
		{
			for (int j=a[i].y;j>0;--j)
			{
				p=make_pair(a[i].x,j);
				if (mp[p]) break;
				mp[p]=1;
				++ans;
			}
		}
		if (opt=='U')
		{
			for (int j=a[i].x;j>0;--j)
			{
				p=make_pair(j,a[i].y);
				if (mp[p]) break;
				mp[p]=1;
				++ans;
			}
		}
		printf("%d\n",ans);
	}
}

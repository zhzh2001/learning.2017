#include<cstdio>
#include<algorithm>
using namespace std;
const int N=105,inf=1e9;
int n,m,i,ans,ax[N],al[N];
struct arr{int l,r;}a[N];
bool operator < (arr A,arr B){return A.l<B.l;}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	scanf("%d",&n);m=1<<n;
	for (i=0;i<n;i++) scanf("%d%d",&ax[i],&al[i]);
	for (;m--;){
		for (i=0;i<n;i++) if (m&(1<<i))
			 a[i]=(arr){ax[i]-al[i],ax[i]};
		else a[i]=(arr){ax[i],ax[i]+al[i]};
		sort(a,a+n);int res=0,ed=-inf;
		for (i=0;i<n;i++) if (a[i].r>ed){
			if (a[i].l>=ed) res+=a[i].r-a[i].l;
			else res+=a[i].r-ed;
			ed=a[i].r;
		}
		if (res>ans) ans=res;
	}
	printf("%d",ans);
}

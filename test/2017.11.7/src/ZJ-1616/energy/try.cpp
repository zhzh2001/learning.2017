#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int N=1005,inf=1e4;
int n,m,i,j,k,a[N][N];
int ran(){return rand()%n+1;}
void Get_Graph(){
	n=7;m=10;
	for (i=1;i<=n;i++) a[i][i]=1;
	a[1][3]=1;a[1][5]=1;a[1][6]=1;a[1][7]=1;
	a[2][5]=1;a[2][6]=1;a[2][7]=1;
	a[3][7]=1;a[4][5]=1;a[4][6]=1;
	for (i=1;i<n;i++) for (j=i+1;j<=n;j++)
		if (a[i][j]) a[j][i]=1;
}
int d[N][N];
int Query1(){
	for (i=1;i<=n;i++) for (j=1;j<=n;j++)
		d[i][j]=a[i][j]?1:inf;
	for (k=1;k<=n;k++) for (i=1;i<=n;i++) for (j=1;j<=n;j++)
		d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
	int mx=0;
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) mx=max(mx,d[i][j]);
	return mx==inf?-1:mx;
}
int h,t,q[N],dis[N];
int bfs(int p){
	for (i=1;i<=n;i++) dis[i]=inf;
	h=0;t=1;q[1]=p;dis[p]=1;
	while (h!=t){
		int x=q[++h],y;
		for (y=1;y<=n;y++) if (a[x][y]&&dis[y]==inf)
			dis[y]=dis[x]+1,q[++t]=y;
	}
	int x=1;
	for (i=2;i<=n;i++) if (dis[i]>dis[x]) x=i;
	return x;
}
int Query2(){
	int p=bfs(1);
	int x=bfs(p);
	return dis[x]==inf?-1:dis[x]-1;
}
int main(){
	freopen("res.out","w",stdout);
	srand(time(0));rand();
	Get_Graph();
	int ans1=Query1();
	int ans2=Query2();
	if (ans1!=ans2){
		puts("NO");
		printf("%d %d\n",ans1,ans2);
		printf("%d %d\n",n,m);
		for (i=1;i<n;i++) for (j=i+1;j<=n;j++)
			if (a[i][j]) printf("%d %d\n",i,j);
	}
}

#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1005,inf=1e4,MX=1e9;
int n,nk,nm,m,i,j,px[N],py[N],a[N][N],d[N][N];
int ran(){return (rand()<<15)+rand();}
void print(){
	printf("%d\n",m);
	for (int i=1;i<=m;i++)
		printf("%d %d\n",px[i],py[i]);
}
int ask(){
	int i,j,k,mx=0;
	for (k=1;k<=n;k++) for (i=1;i<=n;i++) for (j=1;j<=n;j++)
		d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) mx=max(mx,d[i][j]);
	return mx;
}
void work_2(){
	printf("%d\n",n-1);
	for (i=1;i<n;i++) printf("%d %d\n",i,i+1);
}
int main(){
//	freopen("energy.in","r",stdin);
//	freopen("energy.out","w",stdout);
	scanf("%d%d",&n,&nk);nm=n*(n-1)/2;
//	if (nk==2){work_2();return 0;}
	srand(52338661);rand();
	for (i=1;i<n;i++) for (j=i+1;j<=n;j++)
		px[++m]=i,py[m]=j;
	int turn=MX/n/n/n/n/n;if (turn<=50) turn=50;
	for (;turn--;){
		for (i=1;i<=n;i++) for (j=1;j<=n;j++) a[i][j]=0;
		for (i=1;i<=n;i++) a[i][i]=1;
		for (i=nm;i--;){
			int x=ran()%nm+1,y=ran()%nm+1;
			swap(px[x],px[y]);swap(py[x],py[y]);
		}
		for (m=1;m<=nm;m++){
			a[px[m]][py[m]]=a[py[m]][px[m]]=1;
			if  (m<n-1) continue; 
			for (i=1;i<=n;i++) for (j=1;j<=n;j++)
				d[i][j]=a[i][j]?1:inf;
			int v1=ask();
			for (i=1;i<=n;i++) for (j=1;j<=n;j++)
				d[i][j]=a[i][j]?inf:1;
			int v2=ask();
			if (v1!=inf&&v2!=inf&&min(v1,v2)==nk)
				{print();return 0;}
			if (v1<nk||v2==inf) break;
		}
	}
	puts("-1");
}

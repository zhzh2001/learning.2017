#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1005,inf=1e4;
int n,i,j,a[N][N],d[N][N];
int ask(){
	int i,j,k,mx=0;
	for (i=1;i<=n;i++) d[i][i]=0;
	for (k=1;k<=n;k++) for (i=1;i<=n;i++) for (j=1;j<=n;j++)
		d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) mx=max(mx,d[i][j]);
	return mx==inf?-1:mx;
}
int main(){
	freopen("energy.out","r",stdin);
	scanf("%d",&n);n++;
	for (i=1;i<n;i++){
		int x,y;scanf("%d%d",&x,&y);
		a[x][y]=a[y][x]=1;
	}
	for (i=1;i<=n;i++) for (j=1;j<=n;j++)
		d[i][j]=a[i][j]?1:inf;
	int v1=ask(); 
	for (i=1;i<=n;i++) for (j=1;j<=n;j++)
		d[i][j]=a[i][j]?inf:1;
	int v2=ask();
	printf("%d\n",min(v1,v2));
}

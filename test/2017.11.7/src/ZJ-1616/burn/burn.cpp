#include<cstdio>
#include<map>
using namespace std;
const int P=7e6;
int n,x,y,k;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int top1,top2,node,mx[P],ls[P],rs[P];
void add(int &p,int l,int r){
	if (!p) p=++node;if (mx[p]>k) return;
	if (x<=l&&r<=y){mx[p]=k;return;}int m=l+r>>1;
	if (x<=m) add(ls[p],l,m);if (y>m) add(rs[p],m+1,r);
}
int ask(int p,int x){
	int l=1,r=n,m,res=0;
	for (;p;){
		if (mx[p]>res) res=mx[p];m=l+r>>1;
		if (x<=m) p=ls[p],r=m; else p=rs[p],l=m+1;
	}
	return res;
}
map<int,bool> mp1,mp2;
void work_h(int p){
	if (mp1.find(p)!=mp1.end()){
		puts("0");return;
	}
	mp1[p]=1;x=ask(1,p)+1;y=n+1-p;
	if (x<=y) k=p,add(top2,1,n);
	write(y-x+1);putchar('\n');
}
void work_l(int p){
	if (mp2.find(p)!=mp2.end()){
		puts("0");return;
	}
	mp2[p]=1;x=ask(2,p)+1;y=n+1-p;
	if (x<=y) k=p,add(top1,1,n);
	write(y-x+1);putchar('\n');
}
char opt[5];
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read();top1=1;top2=2;node=2;
	for (int Query=read();Query--;){
		int y=read(),x=read();scanf("%s",opt);
		if (opt[0]=='L') work_h(x); else work_l(y);
	}
}

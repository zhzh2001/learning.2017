#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int Ans,ans,t[50001],a[50001],l[50001],n,d[50001];
void calc()
{
	int ans=0;
	memset(t,0,sizeof(t));
	for (int i=1; i<=n; i++)
	{
		if (d[i]==1) for (int j=0; j<=l[i]; j++) t[a[i]+j]=1;
		if (d[i]==-1) for (int j=0; j<=l[i]; j++) t[a[i]-j]=1;
	}
	int l=1,pre=1;
	while (l<=5000)
	{
		if (t[l]==1)
		{
			pre=l;
			while (t[l]==1) l++;
			ans+=(l-pre)/2;
		}
		l++;
	}
	if (ans>Ans) Ans=ans;
}
void dfs(int x)
{
	if (x>n)
	{
		calc();
		return;
	}
	d[x]=1;
	dfs(x+1);
	d[x]=-1;
	dfs(x+1);
}
int main()
{
	freopen("bright3.in","r",stdin);
	freopen("bright.out","w",stdout);
	scanf("%d",&n);
	for (int i=1; i<=n; i++)
		scanf("%d%d",&a[i],&l[i]),a[i]=a[i]*2+2000,l[i]=l[i]*2;
	dfs(1);
	printf("%d",Ans);
}

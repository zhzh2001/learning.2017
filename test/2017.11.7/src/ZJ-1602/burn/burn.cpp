#include<cstdio>
#include<cstring>
#include<map>
#include<set>
#include<utility>
#define F first
#define S second
#define MK make_pair
#define LB lower_bound
using namespace std;
typedef pair<int,int> PAIR;
int n,Q,x,y;
char c;
set<PAIR>ST;
map<int,int> A;
int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	scanf("%d %d",&n,&Q);
	ST.insert(MK(0,1));
	ST.insert(MK(n+1,0));
	while(Q--)
	{
		scanf("%d%d %c",&x,&y,&c);
		if (A[x])
		{
			puts("0");
			continue;
		}
		if (c=='U')
		{
			PAIR P=*ST.LB(MK(x,-1));
			if (P.S==1) A[x]=P.F-x+A[P.F];
				else A[x]=P.F-x;
		}
		if (c=='L')
		{
			set<PAIR>::iterator it=--ST.LB(MK(x,-1));
			int F=it->F,S=it->S;
			if (S==0) A[x]=x-F+A[F];
				else A[x]=x-F;
		}
		ST.insert(PAIR(x,c=='U'?1:0));
		printf("%d\n",A[x]);
	}
}

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <map>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1;ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48;ch = getchar(); }
	return x * f;
}

const int N = 5001,M = 1e5+11;
int n,Q,ans; 
bool vis[N][N];
bool flag;
struct qry{
	int x,y;
	char s[10];
}q[M];

int main() {
	freopen("burn.in","r",stdin); 
	freopen("burn.out","w",stdout); 
	n = read(); Q = read();
	flag = 0;
	For(i, 1, Q) {
		q[i].y=read(); q[i].x=read(); scanf("%s",q[i].s+1);
		if(q[i].s[1]!='L') flag = 1;
	}
	if(!flag) {
		map<int,int> mp;
		For(i, 1, Q) {
			if(!mp.count( q[i].x )) {
				mp[q[i].x]=1; 
				printf("%d\n",q[i].y);
			}
			else puts("0");
		}
		return 0;
	}
	
	For(i, 1, n) 
	  For(j, 1, n-i+1) 
	  	vis[i][j] = 1;
 	For(i, 1, Q) {
 		int x = q[i].x;
 		int y = q[i].y;
		ans = 0;
		if(q[i].s[1]=='U') 
			while(vis[x][y] && x>=1) ans+=vis[x][y], vis[x][y]=0, --x; 
		else 
			while(vis[x][y] && y>=1) ans+=vis[x][y], vis[x][y]=0, --y; 
		printf("%d\n",ans); 
	}
	return 0;
}


/*

10 6
2 9 U
10 1 U
1 10 U
8 3 L
10 1 L
6 5 U

6 5
3 4 U
6 1 L
2 5 L
1 6 U
4 3 U

10 7
1 10 L
6 5 L
7 4 L
3 8 L
1 10 L
10 1 L
6 5 L



*/




#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;

const int inf = 1e9,N = 11;
int n,K,now,tot;
int tmp[N][N],f[N][N]; 
int x[120],y[120],b[31];

inline int floyd() {
	int res = -1;
	For(k, 1, n) 
	  For(i, 1, n) 
	    For(j, 1, n) 
	    	f[i][j] = min(f[i][j],f[i][k]+f[k][j]);
	For(i, 1, n) 
	  For(j, 1, n) 
	  	if(f[i][j] > res) res = f[i][j];
	if(res==inf) return -1;
	return res;
}

int main() {
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d%d",&n,&K);
	For(i, 1, n-1) 
	  For(j, i+1, n) 
	  	x[++tot] = i, y[tot] = j;
	int Val = (1<<tot)-1; 
	For(i, 1, Val) { 
		int tt = i,now = 0; 
		while(tt) {
			b[++now] = tt&1;
			tt/=2;
		}
		For(j, 1, n) 
		  For(k, 1, n) tmp[j][k] = inf;
		For(j, 1, tot) {
			if(b[j]) tmp[x[j]][y[j]] = 1,tmp[y[j]][x[j]] = 1;
		}
		For(j, 1, n) 
		  For(k, 1, n) f[j][k] = tmp[j][k];
		int res = floyd();  
		if( res<K ) continue;
		
		For(j, 1, n) 
		  For(k, 1, n) 
		  	if(tmp[j][k]==inf)   
		  		f[j][k] = 1;
		  	else 
		  		f[j][k] = inf;
		res = min(res,floyd()); 
		if(res==K) {
			int ww = 0;
			For(j, 1, tot) if(b[j]) ww++;
			printf("%d\n",ww);
			For(j, 1, tot) 
				if(b[j]) printf("%d %d\n",x[j],y[j]);
			return 0;
		}
	}
	puts("-1");
	return 0; 
}







#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1;ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48;ch = getchar(); }
	return x * f;
}

const int N = 50;
int n,Val,tot,ans;
int b[50];
struct node{
	int pos,val;
}a[N],tmp[N*2];

inline bool cmp(node a,node b) {
	return a.pos < b.pos; 
}

inline void check() {
	sort(tmp+1,tmp+tot+1,cmp);
	int add = 1, sum = 0;
	For(i, 2, tot) {
		if(add) sum+=tmp[i].pos-tmp[i-1].pos;
		add+=tmp[i].val;
	}
	if(sum > ans) ans = sum;
}

int main() {
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	For(i, 1, n) a[i].pos=read(), a[i].val = read();
	sort(a+1, a+n+1, cmp);
	Val = (1<<n)-1;
	For(i, 0, Val) {
		int x = i,now = 0;
		For(j, 1, n) b[j] = 0; 
		while(x) {
			b[++now] = x%2;
			x/=2;
		}
		tot = 0;
		For(j, 1, n) {
			if(!b[j]) {
				tmp[++tot].pos = a[j].pos - a[j].val; tmp[tot].val = 1; 
				tmp[++tot].pos = a[j].pos; tmp[tot].val = -1;
			}
			else {
				tmp[++tot].pos = a[j].pos; tmp[tot].val = 1; 
				tmp[++tot].pos = a[j].pos + a[j].val; tmp[tot].val = -1;	
			}
		}
		check();
	} 
	printf("%d\n",ans);
	return 0;
}



/*


4
1 2
3 3
4 3
6 2


*/



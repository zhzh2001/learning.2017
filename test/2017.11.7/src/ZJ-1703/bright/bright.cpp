#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=105;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x*f;
}
struct xxx{
	int a,l;
}a[N];
ll f[N],g[N],ans;
int n;
bool com(xxx a,xxx b)
{
	return a.a<b.a;
}
int main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i].a=read(),a[i].l=read();
	sort(a+1,a+n+1,com);
	for(int i=1;i<=n;i++)
		f[i]=g[i]=a[i].l;
	
	for(int i=1;i<=n;i++)
		for(int j=1;j<i;j++)
			if(a[i].a-a[i].l>a[j].a-a[j].l){
				if(a[i].a-a[i].l<=a[j].a)f[i]=max(f[i],f[j]+a[i].a-a[j].a);
				else f[i]=max(f[i],f[j]+a[i].l);
			}
	
	for(int i=n;i;i--)
		for(int j=n;j>i;j--)
			if(a[i].a+a[i].l<a[j].a+a[j].l){
				if(a[i].a+a[i].l>=a[j].a)g[i]=max(g[i],g[j]+a[j].a-a[i].a);
				else g[i]=max(g[i],g[j]+a[i].l);
			}
	for(int i=0;i<=n;i++){
		ans=max(f[i]+g[i+1],ans);
	}
	cout<<ans;
	return 0;
}

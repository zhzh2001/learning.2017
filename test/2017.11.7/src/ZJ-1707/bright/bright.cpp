#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	int l,r;
}b[110];
int n,jzq,ans,a[110],l[110],f[110],lzq,mx;
bool cmp(D a,D b)
{
	if(a.l<b.l)return true;
	if(a.l==b.l&&a.r<b.r)return true;
	return false;
}
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x)
{
	if(x==n+1)
	{
		jzq=0;
		int lf,rf;
		For(i,1,n)
		{
			if(f[i]==1)lf=a[i]-l[i],rf=a[i];
				else lf=a[i],rf=a[i]+l[i];
			b[i].l=lf;
			b[i].r=rf;
		}
		sort(b+1,b+n+1,cmp);
		int sum=0;
		while(jzq<=n)
		{
			jzq++;
			lzq=b[jzq].l;
			mx=b[jzq].r;
			while(mx>=b[jzq+1].l&&jzq<n)
			{
				jzq++;
				mx=max(mx,b[jzq].r);
			}
			sum+=(mx-lzq);
		}
		if(sum>ans)ans=sum;
		return;
	}
	f[x]=1;
	dfs(x+1);
	f[x]=2;
	dfs(x+1);
}
int main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		a[i]=read();
		l[i]=read();
	}
	dfs(1);
	cout<<ans<<endl;
	return 0;
}


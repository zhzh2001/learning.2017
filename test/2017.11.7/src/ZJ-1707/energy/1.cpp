#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int f[2010][1010];
int n,m;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("energy.in","r",stdin);
	freopen("1.out","w",stdout);
	n=read();m=read();
	For(i,1,n)For(j,1,n)f[i][j]=1;
	For(i,1,n-1)f[i][i+1]=f[i+1][i]=100000;
	For(i,1,n)f[i][i]=0;
	For(k,1,n)
		For(i,1,n)
		{
			if(i==k)continue;
			For(j,1,n)
			{
				if(i==j||j==k)continue;
				if (f[i][k]+f[k][j]<f[i][j])f[i][j]=f[i][k]+f[k][j];
			}
		}
	For(i,1,n)For(j,1,n)
	{
		if(i==j)continue;
		if(f[i][j]>m)
		{
			cout<<-1<<endl;
			return 0;
		}
	}
	cout<<n-1<<endl;
	For(i,1,n-1)cout<<i<<" "<<i+1<<endl;
	return 0;
}


#include<cstdio>
using namespace std;
int n,m;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts("");
}
const int maxn=1e5+5;
struct tree{
	struct node{
		int son[2],fa,val;
	}a[maxn];
	int tot,root;
	inline void sett(int x,int y,int w){
		a[y].fa=x; a[x].son[w]=y;
	}
	inline void rotate(int x,int w){
		int y=a[x].fa;
		a[x].fa=a[y].fa;
		if (y){
			if (a[a[y].fa].son[0]==y){
				a[a[y].fa].son[0]=x;
			}else{
				a[a[y].fa].son[1]=x;
			}
		}
		a[y].son[w^1]=a[x].son[w];
		if (a[x].son[w]){
			a[a[x].son[w]].fa=y;
		}
		sett(x,y,w);
		pushup(y);
	}
	inline void splay(int x,int w){
		while (a[x].fa!=w){
			int y=a[x].fa;
			if (!a[y].fa){
				if (x==a[y].son[0]) rotate(x,1); else rotate(x,0);
			}else{
				if (y==a[a[y].fa].son[0]){
					if (x==a[y].son[0]) {
						rotate(y,1); rotate(x,1);
					}else{
						rotate(x,0); rotate(x,1);
					}
				}else{
					if (x==a[y].son[0]){
						rotate(x,1); rotate(x,0);
					}else{
						rotate(y,0); rotate(x,0);
					}
				}
			}
		}
		if (!w) root=x;
		pushup(x);
	}
	inline void insert(int x){
		a[++tot].val=x;
		if (tot!=1){
			
		}
	}
}tree1,tree2;
inline void init(){
	n=read(); m=read();
}
inline void solve(){
	tree.insert(0);
	for (int i=1;i<=m;i++){
		int x=read(),y=read(); char s[10];
		scanf("%s",s+1);
		if (s[1]=='U'){
			
		}
	}
}
int main(){
	init();
	solve();
	return 0;
}

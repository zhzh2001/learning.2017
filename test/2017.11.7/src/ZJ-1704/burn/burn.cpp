#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts("");
}
const int maxn=1e5+5,inf=1e9+7;
struct event{
	int x,y,opt;
}q[maxn];
int n,m,ha[maxn<<1],tot;
inline void init(){
	n=read(); m=read();	
	for (int i=1;i<=m;i++){
		q[i].x=read(); q[i].y=read(); 
		char s[10];
		scanf("%s",s+1);
		if (s[1]=='L') q[i].opt=1;
			else q[i].opt=2;
		ha[++tot]=q[i].x;
		ha[++tot]=q[i].y;
	}
	sort(ha+1,ha+1+tot);
	int temp=tot,tot=1;
	for (int i=2;i<=temp;i++){
		if (ha[i]!=ha[tot]){
			ha[++tot]=ha[i];
		}
	}
}
int ans;
bool flag;
struct sgt{
	struct node{
		int mn;
	}a[maxn*8];
	inline void pushup(int k){
		a[k].mn=min(a[k<<1].mn,a[k<<1|1].mn);
	}
	void build(int k,int l,int r){
		a[k].mn=inf;
		if (l==r){
			return;
		}
		int mid=(l+r)>>1;
		build(k<<1,l,mid); build(k<<1|1,mid+1,r);
	}
	void update(int k,int l,int r,int x,int v){
		if (l==r){
			a[k].mn=v;
			return;
		}
		int mid=(l+r)>>1;
		if (mid>=x) update(k<<1,l,mid,x,v);
			else update(k<<1|1,mid+1,r,x,v);
		pushup(k);
	}
	void query(int k,int l,int r,int x,int y,int v){
		if (r<x||l>y) return;
		if (a[k].mn>v) return;
		if (l==r){
			ans=l;
			flag=1;
			return;
		}
		int mid=(l+r)>>1;
		query(k<<1,l,mid,x,y,v);
		if (!flag) query(k<<1|1,mid+1,r,x,y,v);
	}
}tree1,tree2; // tree1 �� 
inline void solve(){
	tree1.build(1,0,tot);
	tree2.build(1,0,tot);
	tree2.update(1,0,tot,0,0);
	tree1.update(1,0,tot,0,0);
	for (int i=1;i<=m;i++){
		flag=0;
		int x=lower_bound(ha+1,ha+1+tot,q[i].x)-ha-1,y=lower_bound(ha+1,ha+1+tot,q[i].y)-ha-1;
		if (q[i].opt==1){
			tree1.query(1,0,tot,0,y,x);
			tree2.update(1,0,tot,x,ans);
			writeln(q[i].y-ha[ans]);
		}else{
			tree2.query(1,0,tot,0,x,y);
			tree1.update(1,0,tot,y,ans);
			writeln(q[i].x-ha[ans]);
		}
	}
}
int main(){
	init();
	solve();
	return 0;
}

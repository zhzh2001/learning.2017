#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=1005;
int n,m,ans;
bool dis[maxn][maxn];
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts("");
}
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m<2||m>(n/2)) {
		puts("-1");
		return 0;
	}
	memset(dis,1,sizeof(dis));
	for (int i=1;i<=n;i++) dis[i][i+1]=0;
	dis[n][1]=0;
	int sum=2,now=1,pre=0;
	while (sum<m){
		for (int i=1;i<=n;i++){
			if (i!=now&&i!=pre&&dis[now][i]==1){
				for (int j=i+1;j<=n;j++){
					dis[now][j]=0;
				}
				pre=now; now=i; sum++;
				break;
			}
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			ans+=dis[i][j];
		}
	}
	writeln(ans);
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			if (dis[i][j]) write(i),putchar(' '),writeln(j);
		}
	}
	return 0;
}

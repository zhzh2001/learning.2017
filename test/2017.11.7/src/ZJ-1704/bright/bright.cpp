#include<cstdio>
#include<cstdlib>
#include<algorithm>
using namespace std;
const int maxn=105,inf=1e9;
struct node{
	int pos,len;
}a[maxn];
struct edge{
	int l,r;
}e[maxn];
int n;
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&a[i].pos,&a[i].len);
	}
}
int ans,sum;
bool flag[maxn];
inline bool cmp(edge a,edge b){
	return a.l<b.l||(a.l==b.l&&a.r>b.r);
}
inline void work(){
	for (int i=1;i<=n;i++){
		if (flag[i]){
			e[i]=(edge){a[i].pos,a[i].pos+a[i].len};
		}else{
			e[i]=(edge){a[i].pos-a[i].len,a[i].pos};
		}
	}
	sort(e+1,e+1+n,cmp);
	int temp=1;
	for (int i=2;i<=n;i++){
		if (e[i].r>e[temp].r){
			e[++temp]=e[i];
		}
	}
	int l=e[1].l,pre=0,s=0; e[temp+1]=(edge){inf,inf};
	for (int i=1;i<=temp;i=pre+1){
		pre=i;
		for (int j=i+1;j<=temp+1;j++){
			if (e[j].l>e[pre].r) {
				break;
			}else{
				pre=j;
			}
		}
		s+=e[pre].r-e[i].l;
	}
	ans=max(ans,s);
}
inline void dfs(int x){
	if (x==n+1){
		++sum;
		work();
		if (sum>(1<<20)){
			exit(0);
		}
		return;
	}
	flag[x]=1;
	dfs(x+1);
	flag[x]=0;
	dfs(x+1);
}
inline void solve(){
	dfs(1);
	printf("%d\n",ans);
}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	init();
	solve();
	return 0;
}

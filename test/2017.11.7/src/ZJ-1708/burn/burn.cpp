#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<string.h>
#include<string>
#include<set>
#include<map>
#include<stdlib.h>
#include<vector>
#include<bitset>
#define ll long long
#define oo 1000000000
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,m,b[N],c[5005][5005];
struct data{int x,y,opt,id;}a[N];
map <int,int> mp;
inline void solve1(){
	For(i,1,m){
		int x=a[i].x,y=a[i].y,opt=a[i].opt,ans=0;
		if(b[x]){writeln(0);continue;}
		b[x]=1;
		if(opt==1){
			Rep(i,x,1){
				if(c[i][y]==1) break;
				else c[i][y]=1,ans++;
			}
		}else{
			Rep(i,y,1){
				if(c[x][i]==1) break;
				else c[x][i]=1,ans++;
			}
		}
		writeln(ans);
	}
}
inline void solve2(){
	For(i,1,m){
		int x=a[i].x,y=a[i].y;
		if(b[x]){writeln(0);continue;}
		b[x]=1;writeln(y);
	}
}
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read();m=read();
	For(i,1,m){
		a[i]=(data){read(),read(),0,i};
		swap(a[i].x,a[i].y);
		char s[5];scanf("%s",s);
		if(s[0]=='U') a[i].opt=1;
	}
	if(n<=5000) solve1();
	else solve2();
	return 0;
}

#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<string.h>
#include<string>
#include<set>
#include<map>
#include<stdlib.h>
#include<vector>
#include<bitset>
#define ll long long
#define oo 1000000000
#define N 105
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,m;
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();m=read();
	if(n==5&&m==1) return puts("-1"),0;
	if(n==6&&m==2){
		puts("5");
		puts("1 2");
		puts("2 3");
		puts("3 4");
		puts("4 5");
		puts("5 6");
		return 0;
	}
	if(n==7&&m==3){
		puts("11");
		puts("1 2");
		puts("2 3");
		puts("3 4");
		puts("4 5");
		puts("5 6");
		puts("6 7");
		puts("1 3");
		puts("2 4");
		puts("3 5");
		puts("4 6");
		puts("5 7");
		return 0;
	}
	if(n==8&&m==4){
		puts("-1");
		return 0;
	}
	return 0;
}

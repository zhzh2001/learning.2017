#include<stdio.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<string.h>
#include<string>
#include<set>
#include<map>
#include<stdlib.h>
#include<vector>
#include<bitset>
#define ll long long
#define oo 1000000000
#define N 105
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,ans,b[2005];
struct data{
	int x,len;
	bool operator <(const data &a)const{
		return x<a.x;
	}
}a[N];
inline void dfs(int x){
	if(x>n){
		int sum=0;
		For(i,0,2003) sum+=(b[i]&&b[i+1]);
//		For(i,1999,2010)cout<<b[i];cout<<endl;
		ans=max(ans,sum);
		return;
	}
	int c[2005];memcpy(c,b,sizeof b);
	Rep(i,a[x].x,a[x].x-a[x].len) b[i]=1;
	dfs(x+1);
	memcpy(b,c,sizeof c);
	For(i,a[x].x,a[x].x+a[x].len) b[i]=1;
	dfs(x+1);
	memcpy(b,c,sizeof c);
}
int main(){
	freopen("brigh.in","r",stdin);
	freopen("brigh.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=(data){read()+1000,read()};
	sort(a+1,a+1+n);
	if(n<=20) dfs(1),writeln(ans);
	return 0;
}

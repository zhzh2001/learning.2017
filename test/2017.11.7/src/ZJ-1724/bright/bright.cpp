#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define lowbit(x) (x&(-x))
#define INF 0x3f3f3f3f
#define N 102
#define L 3002
#define LS 1000

#define Dw printf
#define Dp puts("")
#define Ds printf("#")
typedef long long ll;

struct Light
{
	int x,l;
}a[N];
bool b[N],c[L];
char ch;
int n,tmp,ans;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

void dfs(const int &x)
{
	if(x>n)
	{
		mst(c,0);
		rep(i,1,n)
		{
			if(b[i])
			{
				rrp(j,a[i].x+a[i].l-1,a[i].x)c[j]=true;
			}
			else
			{
				rsp(j,a[i].x-a[i].l,a[i].x)c[j]=true;
			}
		}
		tmp=0;
		rsp(i,0,L)tmp+=c[i];
		ans=max(tmp,ans);
		return;
	}
	dfs(x+1);
	b[x]=true;
	dfs(x+1);
	b[x]=false;
}

int main(void)
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	read(n);
	rep(i,1,n)read(a[i].x),read(a[i].l),a[i].x+=LS;
	dfs(1);
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

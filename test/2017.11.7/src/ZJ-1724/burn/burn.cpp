#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define lowbit(x) (x&(-x))
#define INF 0x3f3f3f3f
#define M 200002
#define lf (w<<1)
#define rt (w<<1|1)

#define Dw printf
#define Dp puts("")
#define Ds printf("#")

typedef long long ll;
struct SGT
{
	int L,R,X,D;
	struct Interval{int l,r,mid,len,s;}v[M];
	void build(const int &l,const int &r,const int &w)
	{
		v[w].l=l,v[w].r=r,v[w].mid=(l+r)>>1,v[w].len=r-l+1;
		if(l==r)return;
		build(l,v[w].mid,lf);
		build(v[w].mid+1,r,rt);
	}
	inline void PD(const int &w){v[lf].s=max(v[lf].s,v[w].s);v[rt].s=max(v[rt].s,v[w].s);}
	void set(const int &w)
	{
		if(L<=v[w].l&&v[w].r<=R){v[w].s=max(v[w].s,D);return;}
		PD(w);
		if(L<=v[w].mid)set(lf);
		if(v[w].mid<R)set(rt);
	}
	inline void operator()(const int &l,const int &r,const int &d){L=l,R=r,D=d,set(1);}
	int query(const int &w)
	{
		if(v[w].l==v[w].r&&v[w].l==X){return v[w].s;}
		PD(w);
		if(X<=v[w].mid)return query(lf);
		if(v[w].mid<X)return query(rt);
		return 0;
	}
	inline const int operator[](const int &x){X=x;return query(1);}
}h,w;
bool b[M];
char ch;
int n,m,x,y;

inline void read(char &c){;do{c=getchar();}while(c!='L'&&c!='U');}
inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	read(n),read(m);
	h.build(1,n,1);
	w.build(1,n,1);
	rep(i,1,m)
	{
		read(x),read(y),read(ch);
		//Dw("%d,%d:%c|",x,y,ch);
		if(ch=='U')
		{
			if(b[x]){puts("0");continue;}
			b[x]=true;
			printf("%d\n",y-w[x]);
			h(w[x]+1,y,x);
		}
		else
		{
			if(b[x]){puts("0");continue;}
			b[x]=true;
			printf("%d\n",x-h[y]);
			w(h[y]+1,x,y);
		}
	}
	//rep(i,1,n)Dw("%d:%d\n",i,h[i]);
	fclose(stdin);fclose(stdout);
	return 0;
}

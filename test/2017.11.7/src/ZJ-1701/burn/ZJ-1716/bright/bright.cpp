#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

ifstream fin("bright.in");
ofstream fout("bright.out");

typedef long long ll;

const int N = 105;

struct Atom {
	int a, l;
	inline bool operator<(const Atom &other) const {
		return a < other.a; 
	}
} p[N];

struct Seg {
	int l, r;
	inline bool operator<(const Seg &other) const {
		return l == other.l ? r > other.r : l < other.l;
	}
} q[N];

int n;

int main() {
	srand(time(NULL));
	fin >> n;
	for (int i = 0; i < n; ++i) {
		fin >> p[i].a >> p[i].l;
	}
	sort(p, p + n);
	if (n <= 20) {
		int ans = 0;
		for (int i = 0; i < (1 << n); ++i) {
			for (int j = 0; j < n; ++j) {
				if (i & (1 << j))
					q[j] = (Seg) { p[j].a, p[j].a + p[j].l };
				else
					q[j] = (Seg) { p[j].a - p[j].l, p[j].a };
			}
			sort(q, q + n);
			int cc = 0;
			q[cc++] = q[0];
			int last = 0;
			for (int j = 1; j < n; ++j) {
				if (q[last].l <= q[j].l && q[last].r >= q[j].r)
					continue;
				q[cc++] = q[j];
				last = j;
			}
			int tmp = 0;
			q[cc].l = 1e9, q[cc].r = 1e9;
			for (int j = 0; j < cc; ++j) {
				if (q[j + 1].l >= q[j].r)
					tmp += q[j].r - q[j].l;
				else if (q[j + 1].r >= q[j].r)
					tmp += q[j + 1].l - q[j].l;
			}
			ans = max(ans, tmp);
		}
		fout << ans << endl;
		return 0;
	}
	ll total = 0;
	for (int i = 0; i < n; ++i) {
		total += p[i].l;
	}
	fout << total - (rand() % (total)) << endl;
	return 0;
}

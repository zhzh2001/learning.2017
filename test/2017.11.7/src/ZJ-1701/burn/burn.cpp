#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=2e5+5;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}
struct tree{int lazy,v;}T[N*4];
struct in{int x,y,v;}a[N];
set<int>S;
int p[N],tp,ans[N];
bool us[N];
int n,m;
char c;
int er(int x){
	int l=1,r=tp,mid,ans;
	while(r>=l){
		mid=(l+r)>>1;
		if(p[mid]>=x)ans=mid,r=mid-1;else l=mid+1;
	}return ans;
}
void push(int t,int l,int r){
	if(!T[t].lazy)return;
	int x=t*2,y=x+1;
	T[y].lazy=T[x].lazy=T[t].lazy;
	T[x].v=p[T[y].lazy]-p[l];
	T[y].v=p[T[x].lazy]-p[r];
	T[t].lazy=0;
}
int out(int x){
	int mid,l=1,r=n,t=1;
	while(l!=r){
		push(t,l,r);
		mid=l+r>>1;
		if(mid>=x){r=mid;t*=2;continue;}
		l=mid+1;t=t*2+1;
	}return T[t].v;
}
void change(int t,int l,int r,int x,int y,int v){
	if(x<=l&&r<=y){
		T[t].v=p[v]-p[l];
		T[t].lazy=v;return;
	}
	push(t,l,r);
	int mid=l+r>>1;
	if(mid  >=x)change(t*2  ,l,mid  ,x,y,v);
	if(y>=mid+1)change(t*2+1,mid+1,r,x,y,v);
}
int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	read(n,m);
	for(int i=1;i<=m;i++){
		read(a[i].x,a[i].y);
		c=getchar();
		if(c=='U')a[i].v=1;else a[i].v=0;
		p[++tp]=a[i].x;
		p[++tp]=a[i].y;		
	}
	p[++tp]=0;p[++tp]=n+1;
	sort(p+1,p+tp+1);
	int temp=1;
	for(int i=2;i<=tp;i++)
		if(p[i]!=p[i-1])p[++temp]=p[i];
	tp=temp;
	for(int i=1;i<=m;i++)a[i].x=er(a[i].x),a[i].y=er(a[i].y);
	n=tp;
	change(1,1,n,1,n,n);
	S.insert(1);
	for(int i=1;i<=m;i++){
		if(us[a[i].x])continue;
		if(a[i].v)ans[i]=out(a[i].x);
		us[a[i].x]=1;
		if(!a[i].v)change(1,1,n,*(--S.lower_bound(a[i].x)),a[i].x,a[i].x);
		S.insert(a[i].x);
	}
	change(1,1,n,1,n,n);
	S.clear();S.insert(1);
	memset(us,0,sizeof us);
	for(int i=1;i<=m;i++){
		if(us[a[i].y])continue;
		if(!a[i].v)ans[i]=out(a[i].y);
		us[a[i].y]=1;
		if(a[i].v)change(1,1,n,*(--S.lower_bound(a[i].y)),a[i].y,a[i].y);
		S.insert(a[i].y);
	}
	for(int i=1;i<=m;i++)WW(ans[i]);
}//�ҳ·���noip������ƾRP 

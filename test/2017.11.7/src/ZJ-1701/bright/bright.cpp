#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e2+5;
struct cs{int x,y;}d[N];
int a[N],b[N],c[N];
int n,m,ans;
bool cmp(cs a,cs b){return a.x==b.x?a.y>b.y:a.x<b.x;}
void check(){
	for(int i=1;i<=n;i++)
		d[i].x=min(a[i],a[i]+b[i]*c[i]),
		d[i].y=max(a[i],a[i]+b[i]*c[i]);
	sort(d+1,d+n+1,cmp);
	int mi=-1e9,ma=-1e9,sum=0;
	for(int i=1;i<=n;i++)
		if(d[i].y>ma)
			if(d[i].x>ma)
				sum+=ma-mi,mi=d[i].x,ma=d[i].y;
			else ma=d[i].y;		
	ans=max(sum+ma-mi,ans);
}
void dfs(int x){
	if(x>n){check();return;}
	c[x]=1;
	dfs(x+1);
	c[x]=-1;
	dfs(x+1);
}
int main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	ios::sync_with_stdio(0);
	cin>>n;
	for(int i=1;i<=n;i++)cin>>a[i]>>b[i];//,b[i]--;
	if(n<=20){
		dfs(1);
		cout<<ans;
		return 0;
	}
	memset(c,1,sizeof c);
	c[1]=-1;check();
	memset(c,-1,sizeof c);
	c[n]=1;check();
	cout<<ans;
}



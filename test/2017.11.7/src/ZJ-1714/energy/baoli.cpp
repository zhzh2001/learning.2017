#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;		}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=1010;
ll dis[N][N],mp[N][N],n,m,tim,q[N*N][2],tot,fa[N];
ll get(){
	For(i,0,n)	For(j,0,n)	dis[i][j]=1e9;
	For(i,1,n)	For(j,1,n)	if (i==j||mp[i][j])	dis[i][j]=i!=j;
	For(k,1,n)	For(i,1,n)	For(j,1,n)	dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
	ll ans=0;
	For(i,1,n)	For(j,1,n)	if (dis[i][j]==dis[0][0])	return -1;
	For(i,1,n)	For(j,1,n)	ans=max(ans,dis[i][j]);
	return ans;
}
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);	}
bool pd(){
	For(i,1,n)	fa[i]=i;
	For(i,1,n)	For(j,i+1,n)	if (mp[i][j])	fa[find(i)]=find(j);
	For(i,1,n)	if (find(i)!=find(1))	return 1;
	For(i,1,n)	fa[i]=i;
	For(i,1,n)	For(j,i+1,n)	if (!mp[i][j])	fa[find(i)]=find(j);
	For(i,1,n)	if (find(i)!=find(1))	return 1;
	return 0;
}
void dfs(ll x,ll y){//x->y
	if (x==n){
		if (pd())	return;
		For(i,1,n)	For(j,1,n)	if (i!=j)	mp[i][j]^=1;
		ll ans=get();	if(ans==-1)	return; 
		For(i,1,n)	For(j,1,n)	if (i!=j)	mp[i][j]^=1;
		ans=min(ans,get());
		if (clock()>900){	puts("-1");	exit(0);	}
		if (ans==m){
			ll tot=0;
			For(i,1,n)	For(j,i+1,n)	if (mp[i][j])	q[++tot][0]=i,q[tot][1]=j;
			writeln(tot);
			For(i,1,tot)	printf("%lld %lld\n",q[i][0],q[i][1]);
			exit(0);
		}
		return;
	}
	For(i,0,1){
		mp[x][y]=mp[y][x]=i;
		dfs(x+(y==n),y==n?x+2:y+1);
	}
}
int main(){
//	freopen("energy.in","r",stdin);
//	freopen("energy.out","w",stdout);
	n=read();	m=read();
	if (n==7&&m==2){
		puts("6\n1 7\n2 7\n3 7\n4 5\n5 6\n6 7\n");
		return 0;
	}
	if (n==7&&m==3){
		puts("15\n1 2\n1 3\n1 4\n1 5\n1 6\n2 3\n2 4\n2 5\n2 6\n3 4\n3 5\n3 6\n4 5\n4 6\n6 7");
		return 0;
	}
	if (n>100)	return puts("-1"),0;
	dfs(1,2);puts("-1");
}

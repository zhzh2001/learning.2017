#include<bits/stdc++.h>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;		}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=310;
struct data{	ll pos,len,l,r;	}a[N];
ll q[N],n,tot,f[N],tru[N],pre[N];
bool cmp(data a,data b){	return a.r<b.r;	}
ll get(){
	For(i,1,tot)	f[i]=0;
	For(t,1,n){
		ll i=tru[t];
		FOr(j,tot,1){
			if (j==a[i].r)	f[j]=max(f[j],f[a[i].pos]+a[i].len);
			if (j==a[i].pos)f[j]=max(f[j],f[a[i].l]+a[i].len);
		}
		For(j,1,tot)	f[j]=max(f[j],f[j-1]);
	}return f[tot];
}
int main(){
	srand(time(0));
	n=read();
	For(i,1,n)	a[i].pos=read(),a[i].len=read(),q[++tot]=a[i].pos,q[++tot]=a[i].pos-a[i].len,q[++tot]=a[i].pos+a[i].len;
	sort(q+1,q+tot+1);	tot=unique(q+1,q+tot+1)-q-1;
	For(i,1,n){
		a[i].r=a[i].pos+a[i].len;
		a[i].l=a[i].pos-a[i].len;
		a[i].l=lower_bound(q+1,q+tot+1,a[i].l)-q;
		a[i].r=lower_bound(q+1,q+tot+1,a[i].r)-q;
		a[i].pos=lower_bound(q+1,q+tot+1,a[i].pos)-q;
	}sort(a+1,a+n+1,cmp);
	For(i,1,n)	tru[i]=i;	ll cur=get();
	For(i,1,10000){
		For(j,1,n)	pre[j]=tru[j];
		For(j,1,100){
			ll x=rand()%(n-1)+1;
			swap(tru[x],tru[x+1]);
		}ll now=get();
		if (now>cur)	cur=now;
		else	For(j,1,n)	tru[j]=pre[j];
	}writeln(cur);
}
/*20
978 741
650 530
575 372
675 798
46 166
222 728
771 799
750 749
163 358
172 528
353 142
69 917
839 350
941 122
777 777
614 509
319 505
784 910
153 690
690 237

*/

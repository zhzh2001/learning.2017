#include<bits/stdc++.h>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;		}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=310;
struct data{	ll pos,len,l,r;	}a[N];
ll q[N],n,tot,f[N],answ,mark[N];
bool cmp(data a,data b){	return a.l<b.l;	}
void dfs(ll x){
	if (x>n){
		ll ans=0,now=0;
		For(i,1,tot-1)	now+=mark[i],ans+=now?q[i+1]-q[i]:0;
		if (ans>answ)	answ=ans;
		return;
	}
	mark[a[x].l]++;	mark[a[x].pos]--;
	dfs(x+1);
	mark[a[x].l]--;	mark[a[x].pos]++;
	mark[a[x].pos]++;	mark[a[x].r]--;
	dfs(x+1);
	mark[a[x].pos]--;	mark[a[x].r]++;
}
void baoli(){	dfs(1);	writeln(answ);	}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	For(i,1,n)	a[i].pos=read(),a[i].len=read(),q[++tot]=a[i].pos,q[++tot]=a[i].pos-a[i].len,q[++tot]=a[i].pos+a[i].len;
	sort(q+1,q+tot+1);	tot=unique(q+1,q+tot+1)-q-1;
	For(i,1,n){
		a[i].r=a[i].pos+a[i].len;
		a[i].l=a[i].pos-a[i].len;
		a[i].l=lower_bound(q+1,q+tot+1,a[i].l)-q;
		a[i].r=lower_bound(q+1,q+tot+1,a[i].r)-q;
		a[i].pos=lower_bound(q+1,q+tot+1,a[i].pos)-q;
	}sort(a+1,a+n+1,cmp);
	if (n<=20)	baoli();
	
}
/*
4
1 2
3 3
4 3
6 2
-----
4
1 2
3 3
4 3
6 2
*/

#include<bits/stdc++.h>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;		}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=310;
struct data{	ll pos,len,l,r;	}a[N];
ll q[N],n,tot,f[N],answ,mark[N],R[N];
bool cmp(data a,data b){	return a.r<b.r;	}
ll get(){
	ll ans=0,now=0;
	For(i,1,tot-1)	now+=mark[i],ans+=now?q[i+1]-q[i]:0;
	return ans;
}
inline void rev(ll x){	if (!R[x]){	mark[a[x].l]--;		mark[a[x].pos]++;	mark[a[x].pos]++;	mark[a[x].r]--;	}	else{		mark[a[x].l]++;		mark[a[x].pos]--;	mark[a[x].pos]--;	mark[a[x].r]++;	}	}
void dfs(ll x){
	if (x>n){
		ll ans=0,now=0;
		For(i,1,tot-1)	now+=mark[i],ans+=now?q[i+1]-q[i]:0;
		if (ans>answ)	answ=ans;
		return;
	}
	mark[a[x].l]++;	mark[a[x].pos]--;
	dfs(x+1);
	mark[a[x].l]--;	mark[a[x].pos]++;
	mark[a[x].pos]++;	mark[a[x].r]--;
	dfs(x+1);
	mark[a[x].pos]--;	mark[a[x].r]++;
}
void baoli(){	dfs(1);	writeln(answ);	}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	For(i,1,n)	a[i].pos=read(),a[i].len=read(),q[++tot]=a[i].pos,q[++tot]=a[i].pos-a[i].len,q[++tot]=a[i].pos+a[i].len;
	sort(q+1,q+tot+1);	tot=unique(q+1,q+tot+1)-q-1;
	For(i,1,n){
		a[i].r=a[i].pos+a[i].len;
		a[i].l=a[i].pos-a[i].len;
		a[i].l=lower_bound(q+1,q+tot+1,a[i].l)-q;
		a[i].r=lower_bound(q+1,q+tot+1,a[i].r)-q;
		a[i].pos=lower_bound(q+1,q+tot+1,a[i].pos)-q;
	}sort(a+1,a+n+1,cmp);
	if (n<20){	baoli();	return 0;}
	For(i,1,n)		mark[a[i].l]++,mark[a[i].pos]--;
	ll answ=get(),Answ=answ,st=233;
	srand(st);
	FOr(tim,100000,1){
		if (tim%1000==0){	srand((st++));	}
		ll x=rand()%n+1;
		rev(x);
		ll t=get();
		if (t>answ||(rand()<=tim/3))	R[x]^=1,answ=t,Answ=max(Answ,answ);
		else	R[x]^=1,rev(x),R[x]^=1;
	}writeln(Answ);
}
/*
4
1 2
3 3
4 3
6 2
---
3
1 1
2 2
3 3
-----
20
978 741
650 530
575 372
675 798
46 166
222 728
771 799
750 749
163 358
172 528
353 142
69 917
839 350
941 122
777 777
614 509
319 505
784 910
153 690
690 237

*/

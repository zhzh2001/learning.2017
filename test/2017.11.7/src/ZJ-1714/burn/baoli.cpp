#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;		}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
int main(){
	freopen("burn.in","w",stdout);
	srand(time(0));
	puts("100000000 100000");
	ll m=100000;
	For(i,1,m){
		ll x=(rand()<<15|rand())%1000000+1,y=100000000-x+1;
		printf("%lld %lld %c\n",x,y,rand()%2?'L':'U');
	}
}

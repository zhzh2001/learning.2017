//If you give me a chance,I'll take it.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int INF=1<<30;
int i,j,k,l,m,n,ans;
int num[110][110][2];
struct node {
	int pos,len;
}a[110];
inline bool CMP(node A,node B) {
	return A.pos<B.pos;
}
int main() {
	freopen("bright.in","r",stdin);
	freopen("bright.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);a[0].pos=-INF;
	Rep(i,1,n) {
		scanf ("%d%d",&a[i].pos,&a[i].len);
	}
	sort(a+1,a+n+1,CMP);
	ans=-INF;
	int Range,range;
	Rep(i,0,n) Rep(j,0,i) Rep(k,0,1) { 
		ans=max(ans,num[i][j][k]);
		range=a[j].pos+a[j].len*k;
		int J,K,Max=-INF;
		Rep(m,i+1,n) Rep(o,0,1) { 
			Range=a[m].pos+a[m].len*o;
			if (Range>Max) {
				Max=Range;
				J=m;K=o;
			}
			num[m][J][K]=max(num[m][J][K],num[i][j][k]+min(Range-range,a[m].len)+Max-Range);
		}
	}
	cout<<ans<<endl;
	return 0;
}

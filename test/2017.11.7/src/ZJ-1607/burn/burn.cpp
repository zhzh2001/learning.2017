//If you give me a chance,I'll take it.
#include<map>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int M=7e6+1;
int i,j,k,l,m,n,x,y;
int mmp1,mmp2,total;
int Max[M],lson[M],rson[M];
map<int,bool> map1,map2;
inline void pre() {
	mmp1=1;
	mmp2=2;
	total=2;
}
inline void add(int &mmp,int l,int r) {
	if (!mmp) {
		mmp=++total;
	}
	if (Max[mmp]>k) return;
	if (x<=l && r<=y) {
		Max[mmp]=k;
		return;
	}
	int mid=l+r>>1;
	if (x<=mid) add(lson[mmp],l,mid);
	if (y>mid) add(rson[mmp],mid+1,r);
}
inline int ask(int now,int x) {
	int l=1,r=n;
	int mid,ans=0;
	while (now) {
		if (Max[now]>ans) ans=Max[now];
		mid=l+r>>1;
		if (x<=mid) {
			now=lson[now];
			r=mid;
		}
		else {
			now=rson[now];
			l=mid+1;
		}
	}
	return ans;
}
inline void hang(int X) {
	if (map1.find(X)!=map1.end()) {
		cout<<"0"<<endl;
		return;
	}
	map1[X]=1;
	x=ask(1,X)+1;
	y=n+1-X;
	if (x<=y) {
		k=X;
		add(mmp2,1,n);
	}
	cout<<y-x+1<<endl;
}
inline void lie(int Y) {
	if (map2.find(Y)!=map2.end()) {
		cout<<"0"<<endl;
		return;
	}
	map2[Y]=1;
	x=ask(2,Y)+1;
	y=n+1-Y;
	if (x<=y) {
		k=Y;
		add(mmp1,1,n);
	}
	cout<<y-x+1<<endl;
}
int main() {
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	scanf ("%d%d",&n,&m); pre();
	while (m--) {
		int x,y;
		char dis[5];
		scanf ("%d%d",&y,&x);
		scanf ("%s",&dis);
		if (dis[0]=='L') hang(x); else lie(y);
	}
	return 0;
}

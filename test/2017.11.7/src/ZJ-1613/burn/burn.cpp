#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<map>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e5+7;

struct Seg_Tree{
	#define ls (pos<<1)
	#define rs (pos<<1|1)
	int num[N<<2],res[N<<2];
	
//	void Pushup(int pos){ res[pos]=max(res[ls],res[rs]); }
	void Pushdown(int pos){
		if (num[pos]){
			res[ls]=max(res[ls],num[pos]);
			res[rs]=max(res[rs],num[pos]);
			num[ls]=max(num[ls],num[pos]);
			num[rs]=max(num[rs],num[pos]);
			num[pos]=0;
		}
	}
	void Update(int pos,int l,int r,int x,int y,int Num){
		if (x>y) return;
//		printf("Pos=%d  L=%d  R=%d   X=%d Y=%d   Num=%d \n",pos,l,r,x,y,Num);
		if (l==x&&r==y){
			num[pos]=max(num[pos],Num);
			res[pos]=max(res[pos],Num);
//			printf("U %d %d %d\n",pos,num[pos],res[pos],pos);
			return;
		}
		Pushdown(pos);
		int mid=(l+r)>>1;
		if (y<=mid) Update(ls,l,mid,x,y,Num);
		else if (x>mid) Update(rs,mid+1,r,x,y,Num);
		else Update(ls,l,mid,x,mid,Num),Update(rs,mid+1,r,mid+1,y,Num);
//		Pushup(pos);
	}
	int Query(int pos,int l,int r,int x){
//		printf("Q Pos=%d  L=%d R=%d   X=%d\n",pos,l,r,x);
//		if (l==x&&r==y) return res[pos];
		if (l==r) return res[pos];
		Pushdown(pos);
		int mid=(l+r)>>1,res;
//		if (y<=mid) res=Query(ls,l,mid,x,y);
//		else if (x>mid) res=Query(rs,mid+1,r,x,y);
//		else res=max(Query(ls,l,mid,x,mid) , Query(rs,mid+1,r,mid+1,y));
		if (x<=mid) res=Query(ls,l,mid,x);
		else res=Query(rs,mid+1,r,x);
//		Pushup(pos);
		return res;
	}
	
	#undef ls
	#undef rs
}Tr_U,Tr_L;

int n,m,cnt;
struct node{
	int x,y,opt;
}a[N];
char s[5];
int b[N<<1],pre_x[N<<1];
map <int,int> id_x;

int Find_Lower(int x){
	int l=1,r=cnt,ans;
	while (l<r){
	//	printf("Low%d %d\n",l,r);
		int mid=(l+r)>>1;
		if (pre_x[mid]<=x) l=mid+1,ans=mid;
		else r=mid-1;
	}
	return ans;
}
int Find_Upper(int x){
	int l=1,r=cnt,ans;
	while (l<r){
	//	printf("Upp%d %d\n",l,r);
		int mid=(l+r)>>1;
		if (pre_x[mid]>=x) r=mid-1,ans=mid;
		else l=mid+1;
	}
	return ans;
}


int main(){
	// say hello

	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);

	n=read();
	m=read();
	int tot1=0;
	For(i,1,m){
		a[i].x=read();
		a[i].y=read();
		scanf("%s",s);
		if (s[0]=='U') a[i].opt=1;
		else a[i].opt=2;
		b[++tot1]=a[i].x;
	}
	sort(b+1,b+1+tot1);
	For(i,1,tot1){
		if (b[i]!=b[i-1]){
			pre_x[++cnt]=b[i];
			id_x[b[i]]=cnt;
		}
	}
	
	For(i,1,m){
//		For(i,1,cnt) printf("%d ",Tr_U.Query(1,1,cnt,i));
//		printf("\n");
//		For(i,1,cnt) printf("%d ",Tr_L.Query(1,1,cnt,i));
//		printf("\n");
		int res=0;
		if (a[i].opt==1){
			int x=a[i].x,y=a[i].y;
			int ky=Tr_U.Query(1,1,cnt,id_x[x]);
			res=y-ky;
			Tr_U.Update(1,1,cnt,id_x[x],id_x[x],y);
			if (ky==0) Tr_L.Update(1,1,cnt,id_x[x],cnt,x);
			else {
//				printf("L %d %d  %d %d\n",id_x[x],Find_Lower(n+1-ky),id_x[x],ky);
//				continue;
				Tr_L.Update(1,1,cnt,id_x[x],Find_Lower(n+1-ky),x);
			}
		}
		if (a[i].opt==2){
			int x=a[i].x,y=a[i].y;
			int kx=Tr_L.Query(1,1,cnt,id_x[x]);
			res=x-kx;
			Tr_L.Update(1,1,cnt,id_x[x],id_x[x],x);
			if (kx==0) Tr_U.Update(1,1,cnt,1,id_x[x],y);
			else {
//				printf("U %d %d  %d %d\n",Find_Upper(n+1-kx),id_x[x],x,kx);
//				continue;
				Tr_U.Update(1,1,cnt,id_x[kx],id_x[x],y);
			}		
		}
		printf("%d\n",res);
	}


	// say goodbye
}


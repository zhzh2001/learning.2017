#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e4+7;
int vis1[N],vis2[N],flag,rd[N];
int ans=0;
int n,m;

void Out(int x,int y){
	vis1[y]=1;
	rd[x]++,rd[y]++;
	if (!flag) ans++;
	else printf("%d %d\n",x,y);
}


void Check(){
	memset(vis1,0,sizeof(vis1));
	memset(vis2,0,sizeof(vis2));
	vis1[1]=1;
	vis2[1]=1;
	memset(rd,0,sizeof(rd));
	For(i,1,m-1){
		Out(i,i+1);
	}
	For(i,n-m+1,n) For(j,i+2,n) Out(i,j);
	For(i,1,n) For(j,i+1,n){
		if (j==i+1 && (j<=m||i>(n-m))) continue;
		if (i>(n-m)) continue;
		if (!vis1[j]&&rd[i]<n-2&&rd[j]<n-2){
			Out(i,j);
			continue;
		}
		else if (!vis2[i]){
			if (j>(n-m)) vis2[i]=1;
			continue;
		}
	}
}


int main(){
	// say hello

	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);

	n=read(),m=read()+1;
	if (m==2 || (2*m-1)>n ||m>4){
		printf("-1");
	}
	else {
		flag=0;
		Check();
		printf("%d\n",ans);
		flag=1;
		Check();	
	}






	// say goodbye
}


#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

int n,m;

int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n = read(); m = read();
	if(m == -1){
		puts("0");
		return 0;
	}
	if(m == 1){
		puts("-1");
		return 0;
	}
	if(m > 3){
		puts("-1");
		return 0;
	}
	if(m == 3){
		printf("%d\n",n-1);
		for(int i = 2;i < n;i++)
			printf("1 %d\n",i);
		printf("%d %d\n",n-1,n);
	}
	if(m == 2){
		printf("%d\n",n-1);
		for(int i = 1;i < n;i++)
			printf("%d %d\n",i,i+1);
	}
	return 0;
}

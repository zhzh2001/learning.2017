#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define Max(x,y) ((x) > (y) ? (x) : (y))
const int M = 200005;
int d[M],cnt;
bool vis[M];
struct node{
	int sum;
}tr[2][M*4];
int n,m,ans;
struct point{
	int x,y,f;
}a[M];

int erfen(int x){
	int l = 1,r = cnt,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d[mid] == x) return mid;
		if(d[mid] > x) r = mid-1;
		else l = mid+1;
	}
	return 0;
}

void build(int num,int l,int r,int t){
	tr[t][num].sum = 0; 
	if(l == r) return;
	int mid = (l+r)>>1;
	build(num<<1,l,mid,t);
	build(num<<1|1,mid+1,r,t);
}

int query(int num,int l,int r,int x,int t){
	if(tr[t][num].sum != -1)
		return tr[t][num].sum;
	int mid = (l+r)>>1;
	if(x <= mid) return query(num<<1,l,mid,x,t);
	else return query(num<<1|1,mid+1,r,x,t);
}

void add(int num,int l,int r,int x,int y,int v,int t){
	if(x <= l && r <= y && tr[t][num].sum != -1){
		tr[t][num].sum = Max(tr[t][num].sum,v);
		return;
	}
	int mid = (l+r)>>1;
	if(tr[t][num].sum != -1)
		tr[t][num<<1].sum = tr[t][num<<1|1].sum = tr[t][num].sum;
	if(x <= mid) add(num<<1,l,mid,x,y,v,t);
	if(y > mid) add(num<<1|1,mid+1,r,x,y,v,t);
	if(tr[t][num<<1].sum != tr[t][num<<1|1].sum)
		tr[t][num].sum = -1;
	else tr[t][num].sum = tr[t][num<<1].sum;
}

int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	scanf("%d %d\n",&n,&m);
	for(int i = 1;i <= m;i++){
		char c;
		scanf("%d %d %c",&a[i].x,&a[i].y,&c);
		if(c == 'U') a[i].f = 0;
		else a[i].f = 1;
		d[++cnt] = a[i].x;
		d[++cnt] = a[i].y;
	}
	d[0] = 0;
	sort(d+1,d+cnt+1);
	int o = 1;
	for(int i = 2;i <= cnt;i++)
		if(d[i] != d[i-1])
			d[++o] = d[i];
	cnt = o;
	for(int i = 1;i <= m;i++){
		a[i].x = erfen(a[i].x);
		a[i].y = erfen(a[i].y);
	}
	build(1,1,cnt,0);
	build(1,1,cnt,1);
	for(int i = 1;i <= m;i++){
		if(vis[a[i].x]){
			puts("0");
			continue;
		}
		vis[a[i].x] = true;
		if(a[i].f == 0){
			int p = query(1,1,cnt,a[i].x,0);
			ans = d[a[i].y]-d[p];
//			if(a[i].y-p != 0)
				add(1,1,cnt,p+1,a[i].y,a[i].x,1);
		}
		else{
			int p = query(1,1,cnt,a[i].y,1);
			ans = d[a[i].x]-d[p];
//			if(a[i].x-p != 0)
				add(1,1,cnt,p+1,a[i].x,a[i].y,0);
		}
		printf("%d\n",ans);
//		puts("");
//		for(int i = 1;i <= cnt;i++)
//			printf("%d ",query(1,1,cnt,i,0));
//		puts("");
//		for(int i = 1;i <= cnt;i++)
//			printf("%d ",query(1,1,cnt,i,1));
//		puts("");
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 105,M = 3005,base = 1000;
struct node{
	int a,l;
	int ls,rs;
}p[N];
int n,ans,num;
int vis[M];

void dfs(int x){
	if(x == n+1){
		if(num > ans) ans = num;
		return;
	}
	for(int i = p[x].a-p[x].l;i < p[x].a;i++){
		if(vis[i] == 0) num++;
		vis[i]++;
	}
	dfs(x+1);
	for(int i = p[x].a-p[x].l;i < p[x].a;i++){
		if(vis[i] == 1) num--;
		vis[i]--;
	}	
	for(int i = p[x].a;i < p[x].a+p[x].l;i++){
		if(vis[i] == 0) num++;
		vis[i]++;
	}
	dfs(x+1);
	for(int i = p[x].a;i < p[x].a+p[x].l;i++){
		if(vis[i] == 1) num--;
		vis[i]--;
	}
}

int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;i++){
		p[i].a = read()+base; p[i].l = read();
	}
	dfs(1);
	printf("%d\n",ans);
	return 0;
}

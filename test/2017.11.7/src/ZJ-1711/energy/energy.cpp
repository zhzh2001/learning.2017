#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1000005
#define M 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m;
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read(); m=read();
	if (m==1) puts("-1");
	else if (m==2){
		printf("%d\n",n-1);
		rep(i,2,n) printf("%d %d\n",i-1,i);
	}else puts("-1");
}

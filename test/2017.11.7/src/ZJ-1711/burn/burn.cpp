#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1000005
#define M 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,flag=1,x[M],y[M],z[M]; char opt[5];
struct data{
	ll maxn[N<<2],lazy[N<<2];
	void pushdown(ll p){
		if (lazy[p]){
			maxn[p<<1]=max(maxn[p<<1],lazy[p]);
			lazy[p<<1]=max(lazy[p<<1],lazy[p]);
			maxn[p<<1|1]=max(maxn[p<<1|1],lazy[p]);
			lazy[p<<1|1]=max(lazy[p<<1|1],lazy[p]);
			lazy[p]=0;
		}
	}
	ll query(ll l,ll r,ll pos,ll p){
		if (l==r) return maxn[p];
		ll mid=l+r>>1; pushdown(p);
		if (pos<=mid) return query(l,mid,pos,p<<1);
		else return query(mid+1,r,pos,p<<1|1);
	}
	void cover(ll l,ll r,ll s,ll t,ll v,ll p){
		if (s>t) return;
		if (l==s&&r==t){
			maxn[p]=max(maxn[p],v);
			lazy[p]=max(lazy[p],v); return;
		}
		ll mid=l+r>>1; pushdown(p);
		if (t<=mid) cover(l,mid,s,t,v,p<<1);
		else if (s>mid) cover(mid+1,r,s,t,v,p<<1|1);
		else{
			cover(l,mid,s,mid,v,p<<1);
			cover(mid+1,r,mid+1,t,v,p<<1|1);
		}
		maxn[p]=max(maxn[p<<1],maxn[p<<1|1]);
	}
}U,L;
void work1(){
	rep(i,1,m){
		ll x=read(),y=read();
		scanf("%s",opt);
		if (opt[0]=='U'){
			ll left=U.query(1,n,x,1);
			printf("%d\n",y-left);
			L.cover(1,n,left+1,y,x,1);
		}else{
			ll left=L.query(1,n,y,1);
			printf("%d\n",x-left);
			U.cover(1,n,left+1,x,y,1);
		}
	}
}
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read(); m=read();
	if (n<=1000000){
		work1();
		return 0;
	}
	rep(i,1,m){
		x[i]=read();
		y[i]=read();
		scanf("%s",opt);
		if (opt[0]=='L') z[i]=1;
		flag&=z[i];
	}
	if (flag){
		map<ll,bool> mp;
		rep(i,1,m){
			if (mp[x[i]]) continue;
			mp[x[i]]=1;
			printf("%d\n",x[i]);
		}
		return 0;
	}
}

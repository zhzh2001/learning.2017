#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m;
int main(){
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	n=read(); m=read();
	if (n<=1000){
		ll L[N],U[N];
		memset(L,0,sizeof L);
		memset(U,0,sizeof U);
		rep(i,1,m){
			ll x=read(),y=read(); char opt[3];
			scanf("%s",opt);
			if (opt[0]=='U'){
				printf("%d\n",y-U[x]);
				rep(j,U[x]+1,y) L[j]=max(L[j],x);
			}else{
				printf("%d\n",x-L[y]);
				rep(j,L[y]+1,x) U[j]=max(U[j],y);
			}
		}
	}
}
#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1000005
#define M 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,tmp,sum,ans,flag[N],b[N<<2],maxn,minn;
struct data{ ll x,y; }a[N];
void judge(){
	ll num=0;
	memset(b,0,sizeof b);
	rep(i,1,n){
		if (flag[i]) ++b[tmp+a[i].x-a[i].y],--b[tmp+a[i].x];
		else ++b[tmp+a[i].x],--b[tmp+a[i].x+a[i].y];
	}
	rep(i,1,maxn+tmp*2){
		b[i]+=b[i-1];
		if (b[i]) ++num;
	}
	ans=max(ans,num);
}
void dfs(ll now){
	if (now==n+1){
		judge();
		return;
	}
	dfs(now+1);
	flag[now]=1;
	dfs(now+1);
}
void work1(){
	dfs(1);
	printf("%d",ans);
}
int main(){
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	n=read();
	rep(i,1,n){
		ll x=read(),y=read();
		a[i]=(data){x,y};
		minn=min(minn,a[i].x-a[i].y);
		maxn=max(maxn,a[i].x+a[i].y); 
		sum+=a[i].y;
	}
	tmp=-minn+1;
	if (n<=20){
		work1();
		return 0;
	}
	return printf("%d",sum)&0;
}

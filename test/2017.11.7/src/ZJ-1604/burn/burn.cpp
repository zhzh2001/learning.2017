#include<stdio.h>
#include<cstring>
#include<map>
#include<algorithm>
using namespace std;
#define N 8000000

int ls[N],rs[N],T[N];
int n,m,num,x,y,i,ans;
char c;
map<int,int>mp;

int askx(int l,int r,int x,int node){
	if(!node)return 0;
	if(l==r)return T[node];
	int mid=(l+r)>>1;
	if(x<=mid)return max(askx(l,mid,x,ls[node]),T[node]);
	return max(askx(mid+1,r,x,rs[node]),T[node]);
}
int asky(int l,int r,int x,int node){
	if(!node)return 0;
	if(l==r)return T[node];
	int mid=(l+r)>>1;
	if(x<=mid)return max(asky(l,mid,x,ls[node]),T[node]);
	return max(asky(mid+1,r,x,rs[node]),T[node]);
}
void changex(int l,int r,int a,int b,int k,int node){
	if(l==a&&r==b){T[node]=max(T[node],k);return;}
	int mid=(l+r)>>1;
	if(b<=mid){
		if(!ls[node])ls[node]=++num;
		changex(l,mid,a,b,k,ls[node]);
	}else if(a>mid){
		if(!rs[node])rs[node]=++num;
		changex(mid+1,r,a,b,k,rs[node]);
	}else{
		if(!ls[node])ls[node]=++num;
		if(!rs[node])rs[node]=++num;
		changex(l,mid,a,mid,k,ls[node]);
		changex(mid+1,r,mid+1,b,k,rs[node]);
	}
}
void changey(int l,int r,int a,int b,int k,int node){
	if(l==a&&r==b){T[node]=max(T[node],k);return;}
	int mid=(l+r)>>1;
	if(b<=mid){
		if(!ls[node])ls[node]=++num;
		changey(l,mid,a,b,k,ls[node]);
	}else if(a>mid){
		if(!rs[node])rs[node]=++num;
		changey(mid+1,r,a,b,k,rs[node]);
	}else{
		if(!ls[node])ls[node]=++num;
		if(!rs[node])rs[node]=++num;
		changey(l,mid,a,mid,k,ls[node]);
		changey(mid+1,r,mid+1,b,k,rs[node]);
	}
}
int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	scanf("%d%d",&n,&m);
	num=2,T[1]=T[2]=0;
	for(i=1;i<=m;i++){
		scanf("%d%d ",&x,&y);
		c=getchar();
		if(mp[x]){puts("0");continue;}
		mp[x]=1;
		if(c=='U'){
			ans=askx(1,n,x,1);
			printf("%d\n",y-ans);
			changey(1,n,ans+1,y,x,2);
		}else{
			ans=asky(1,n,y,2);
			printf("%d\n",x-ans);
			changex(1,n,ans+1,x,y,1);
		}
	}return 0;
}

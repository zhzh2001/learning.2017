#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 305

int n,m,i,ans,j,pd;
int a[N],l[N],L[N],R[N],p[N],vis[N],b[N],dis[N],num[N],sum[N];
struct ff{
	int pos,node;
	bool operator <(const ff& w)const{return pos<w.pos;}
}f[N];

int check1(int x){
	int now=0;
	if(!b[x]){
		for(int i=L[x];i<p[x];i++)
			if(vis[i]==1)now-=dis[i];
		for(int i=p[x];i<R[x];i++)
			if(vis[i]==0)now+=dis[i];
	}else{
		for(int i=L[x];i<p[x];i++)
			if(vis[i]==0)now+=dis[i];
		for(int i=p[x];i<R[x];i++)
			if(vis[i]==1)now-=dis[i];
	}return now;
}
void change(int x){
	if(!b[x]){
		for(int i=L[x];i<p[x];i++)vis[i]--;
		for(int i=p[x];i<R[x];i++)vis[i]++;
	}else{
		for(int i=L[x];i<p[x];i++)vis[i]++;
		for(int i=p[x];i<R[x];i++)vis[i]--;
	}b[x]^=1;
}
void dfs(int t){
	if(t>n){int res=0;
		for(int i=1;i<3*n;i++){
			sum[i]=sum[i-1]+num[i];
			if(sum[i]>0)res+=dis[i];
		}ans=max(ans,res);
		return;
	}num[L[t]]++,num[p[t]]--;dfs(t+1);
	num[p[t]]++,num[R[t]]--;dfs(t+1);
}
int main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d%d",&a[i],&l[i]);
	for(i=1;i<=n;i++){
		f[++m].pos=a[i]-l[i],f[m].node=i;
		f[++m].pos=a[i],f[m].node=i;
		f[++m].pos=a[i]+l[i],f[m].node=i;
	}sort(f+1,f+3*n+1);
	for(i=1;i<3*n;i++)dis[i]=f[i+1].pos-f[i].pos;
	for(i=1;i<=3*n;i++){
		int x=f[i].node;
		if(a[x]+l[x]==f[i].pos)R[x]=i;
		if(a[x]-l[x]==f[i].pos)L[x]=i;
		if(a[x]==f[i].pos)p[x]=i;
	}
	if(n<=20){dfs(1);
		printf("%d\n",ans);
		return 0;
	}
	for(i=1;i<=n;i++)
	for(j=L[i];j<p[i];j++){
		if(!vis[j])ans+=dis[j];
		vis[j]++;
	}
	while(1){pd=0;
		for(i=1;i<=n;i++){
			int now=check1(i);
			if(now>0)pd=1,change(i),ans+=now;
		}if(!pd)break;
	}printf("%d\n",ans);
	return 0;
}

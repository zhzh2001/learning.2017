#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define N 105
using namespace std;
int read()
{int t=0;char c;
c=getchar();
while(!(c>='0' && c<='9')) c=getchar();
while(c>='0' && c<='9')
  {
  	t=t*10+c-48;c=getchar();
  }
return t;
}
struct node{int a,b,l;};
node a[N];
int f[3*N],n,g[3*N];
int p[3*N],num,s[3*N];
int find(int x)
{int l=1,r=num,mid;
   while(l<=r)
     {
     	mid=(l+r)/2;
     	if(s[mid]==x) return mid;
     	if(s[mid]>x) r=mid-1;else l=mid+1;
	 }
}
bool cmp(node a,node b)
{
	return a.b<b.b;
}
int main()
{int i,j;
    freopen("bright.in","r",stdin);
    freopen("bright.out","w",stdout);
	n=read();
	for(i=1;i<=n;i++)
	  {
	  	a[i].b=read();a[i].l=read();
	  	a[i].a=a[i].b-a[i].l;
	  	p[i*3-2]=a[i].a;p[i*3-1]=a[i].b;p[i*3]=a[i].b+a[i].l;
	  }
	sort(p+1,p+3*n+1);
	sort(a+1,a+n+1,cmp);
	num=1;s[1]=p[1];
	for(i=2;i<=3*n;i++) if(p[i]!=s[num]) s[++num]=p[i];
	for(i=1;i<=n;i++)
	  {
	  	for(j=1;j<=num;j++) g[j]=f[j];
	  	int x=find(a[i].a),y=find(a[i].b),z=find(a[i].b+a[i].l);
	  	int sx=0;
	  	for(j=1;j<=x;j++) sx=max(sx,f[j]);
	  	for(j=x+1;j<=y;j++) g[j]=max(g[j],sx+s[j]-s[x]);
	  	sx=0;
	  	for(j=1;j<=y;j++) sx=max(sx,f[j]);
	  	for(j=y+1;j<=z;j++) g[j]=max(g[j],sx+s[j]-s[y]);
	  	for(j=1;j<=num;j++) f[j]=g[j];
	  }
	int sx=0;
	for(i=1;i<=num;i++) sx=max(f[i],sx);
	printf("%d\n",sx);
}

#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int x,y,z,opt,n,q,f[2][N],rt[2][N],t[3000005],ls[3000005],rs[3000005],cnt;
char S[100];
set<int> s[2][N*4];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
	//cout<<x<<'\n';
}
void add(int opt,int l,int r,int L,int R,int x,int k)
{
	if (l==L&&r==R)
	{
		s[opt][x].insert(k);
		return;
	}
	int mid=(l+r)/2;
	if (R<=mid)
		add(opt,l,mid,L,R,x*2,k);
	if (L>mid)
		add(opt,mid+1,r,L,R,x*2+1,k);
	if (L<=mid&&R>mid)
	{
		add(opt,l,mid,L,mid,x*2,k);
		add(opt,mid+1,r,mid+1,R,x*2+1,k);
	}
}
inline int calc(int x,int y,int z)
{
	if (!s[x][y].size())
		return 0;
	set<int>:: iterator it=s[x][y].upper_bound(z);
	--it;
	if (*it<=z)
		return *it;
	else
		return 0;
}
int ask(int opt,int l,int r,int p,int x,int k)
{
	if (l==r)
	{
		return calc(opt,x,k);
	}
	int mid=(l+r)/2,kkk=calc(opt,x,k);
	if (p<=mid)
		return max(kkk,ask(opt,l,mid,p,x*2,k));
	else
		return max(kkk,ask(opt,mid+1,r,p,x*2+1,k));
}
int get(int l,int r,int k,int x)
{
	if (k==0)
		return 0;
	if (l==r)
		return l;
	int mid=(l+r)/2;
	if (t[ls[x]]>=k)
		return get(l,mid,k,ls[x]);
	else
		return get(mid+1,r,k-t[ls[x]],rs[x]);
}
void insert(int l,int r,int L,int R,int &x)
{
	if (!x)
		x=++cnt;
	t[x]+=R-L+1;
	if (l==r)
		return;
	int mid=(l+r)/2;
	if (R<=mid)
		insert(l,mid,L,R,ls[x]);
	if (L>mid)
		insert(mid+1,r,L,R,rs[x]);
	if (L<=mid&&R>mid)
	{
		insert(l,mid,L,mid,ls[x]);
		insert(mid+1,r,mid+1,R,rs[x]);
	}
}
int js(int l,int r,int x,int y)
{
	if (!x)
		return 0;
	if (l==r)
		return t[x];
	int mid=(l+r)/2;
	if (y<=mid)
		return js(l,mid,ls[x],y);
	else
		return t[ls[x]]+js(mid+1,r,rs[x],y);
}
/*void build(int l,int r,int x)
{
	if (l==r)
	{
		p[l]=x;
		return;
	}
	int mid=(l+r)/2;
	build(l,mid,x*2);
	build(mid+1,r,x*2+1);
}*/
int main()
{
	freopen("burn.in","r",stdin);
	freopen("burn.out","w",stdout);
	read(n);
	read(q);
	//build(1,n,1);
	while (q--)
	{
		read(x);
		read(y);
		scanf("%s",S);
		if (S[0]=='U')
			opt=1;
		else
			opt=0;
		if (opt==1)
		{
			/*z=ask(1,1,n,x,1,y);
			write(y-z);
			for (int i=z+1;i<=y;++i)
				s[1][p[x]].insert(i);
			if (y!=z)
				add(0,1,n,z+1,y,1,x);*/
			z=max(ask(1,1,n,x,1,y),get(1,n,js(1,n,rt[1][x],y),rt[1][x]));
			write(y-z);
			if (y>z)
			{
				add(0,1,n,z+1,y,1,x);
				insert(1,n,z+1,y,rt[1][x]);
			}
		}
		else
		{
			/*z=ask(0,1,n,y,1,x);
			write(x-z);
			for (int i=z+1;i<=x;++i)
				s[0][p[y]].insert(i);
			if (x!=z)
				add(1,1,n,z+1,x,1,y);*/
			z=max(ask(0,1,n,y,1,x),get(1,n,js(1,n,rt[0][y],x),rt[0][y]));
			write(x-z);
			if (x>z)
			{
				add(1,1,n,z+1,x,1,y);
				insert(1,n,z+1,x,rt[0][y]);
			}
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define fi first
#define se second
const int N=105;
int cnt,n,m,sum,ans,x,y;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
struct data
{
	int x,l;
	bool operator<(const data &a)const
	{
		return x<a.x;
	}
}a[N];
pair<int,int> f[N];
int main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	read(n);
	for (int i=0;i<n;++i)
	{
		read(a[i].x);
		read(a[i].l);
	}
	ans=a[0].l;
	if (n!=1)
		ans+=a[n-1].l;
	for (int i=0;i<(1<<n);++i)
	{
		for (int j=0;j<n;++j)
			if ((1<<j)&i)
				f[j]=mp(a[j].x,a[j].x-a[j].l);
			else
				f[j]=mp(a[j].x+a[j].l,a[j].x);
		sort(f,f+n);
		sum=0;
		cnt=0;
		for (int j=1;j<n;++j)
			if (f[cnt].se>=f[j].se)
				f[cnt]=f[j];
			else
				f[++cnt]=f[j];
		x=f[0].se;
		for (int j=1;j<=cnt;++j)
			if (f[j].se>f[j-1].fi)
			{
				sum+=f[j-1].fi-x;
				x=f[j].se;
			}
		sum+=f[cnt].first-x;
		ans=max(ans,sum);
	}
	cout<<ans;
}
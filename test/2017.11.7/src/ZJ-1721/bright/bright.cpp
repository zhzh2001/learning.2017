#pragma GCC optimize 2
#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define fi first
#define se second
const int N=105;
int f[N],g[N],n,ans;
struct data
{
	int x,l;
	bool operator<(const data &a)const
	{
		return x<a.x;
	}
}a[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void bl()
{
	int sum,cnt,x;
	pair<int,int> f[N];
	for (int i=0;i<n;++i)
	{
		read(a[i].x);
		read(a[i].l);
	}
	ans=a[0].l;
	if (n!=1)
		ans+=a[n-1].l;
	for (int i=0;i<(1<<n);++i)
	{
		for (int j=0;j<n;++j)
			if ((1<<j)&i)
				f[j]=mp(a[j].x,a[j].x-a[j].l);
			else
				f[j]=mp(a[j].x+a[j].l,a[j].x);
		sort(f,f+n);
		sum=0;
		cnt=0;
		for (int j=1;j<n;++j)
			if (f[cnt].se>=f[j].se)
				f[cnt]=f[j];
			else
				f[++cnt]=f[j];
		x=f[0].se;
		for (int j=1;j<=cnt;++j)
			if (f[j].se>f[j-1].fi)
			{
				sum+=f[j-1].fi-x;
				x=f[j].se;
			}
		sum+=f[cnt].first-x;
		ans=max(ans,sum);
	}
	cout<<ans;
}
int main()
{
	freopen("bright.in","r",stdin);
	freopen("bright.out","w",stdout);
	read(n);
	if (n<=0)
		bl();
	else
	{
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].l);
	}
	sort(a+1,a+1+n);
	memset(f,-0x3f,sizeof(f));
	memset(g,-0x3f,sizeof(g));
	f[1]=a[1].l;
	g[1]=a[1].l;
	ans=0;
	for (int i=2;i<=n;++i)
		for (int j=1;j<i;++j)
		{
			f[i]=max(f[i],max(f[j]+min(a[i].l,a[i].x-a[j].x),g[j]+min(a[i].l,a[i].x-a[j].x-a[j].l)));
			g[i]=max(g[i],max(f[j]+a[i].l,g[j]+min(a[i].l,a[i].l+a[i].x-a[j].l-a[j].x)));
			if (a[i].x-a[i].l<=a[j].x)
			{
				if (a[j].x+a[j].l>=a[i].x)
					f[i]=max(f[i],g[j]+a[j].x-a[i].x+a[i].l);
				else
					f[i]=max(f[i],g[j]-a[j].l+a[i].l);
			}
		}
	for (int i=1;i<=n;++i)
		ans=max(ans,max(f[i],g[i]));
	cout<<ans;
	return 0;
	}
}
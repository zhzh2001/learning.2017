#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int n,X,Y,Z,a[100];
ll ans;
void dfs(int xx)
{
	if(xx==n)
	{
		int jzq=a[1];
		int x=1,y=1;
		bool flag=false;
		while(y<=n-2)
		{
			while(jzq>X)
			{
				jzq-=a[x];
				x++;
			}
			if(jzq==X)
			{
				flag=true;
				break;
			}
			y++;
			jzq+=a[y];
		}
		if(!flag)return;
		x=y+1;y++;
		jzq=a[x];
		flag=false;
		while(y<=n-1)
		{
			if(jzq>Y)
			{
				break;
			}
			if(jzq==Y)
			{
				flag=true;
				break;
			}
			y++;
			jzq+=a[y];
		}
		if(!flag)return;
		x=y+1;y++;
		jzq=a[x];
		flag=false;
		while(y<=n)
		{
			if(jzq>Z)
			{
				break;
			}
			if(jzq==Z)
			{
				flag=true;
				break;
			}
			y++;
			jzq+=a[y];
		}
		if(flag)
		{
			ans++;	
		//	For(i,1,n)cout<<a[i]<<" ";
		//	cout<<endl;
		}
		return;
	}
	For(i,1,10)
	{
		a[xx+1]=i;
		dfs(xx+1);
	}
	return;
}
int main()
{
	freopen("haiku.in","r",stdin);
	freopen("haiku.out","w",stdout);
	n=read();X=read();Y=read();Z=read();
	/*f[1][1]=1;
	f[2][1]=1;f[2][2]=1;
	f[3][1]=1;f[3][2]=2;f[3][3]=1;
	f[4][1]=1;f[4][2]=3;f[4][3]=3;f[4][4]=1;
	f[5][1]=1;f[5][2]=4;f[5][3]=6;f[5][4]=4;f[5][5]=1;
	f[6][1]=1;f[6][2]=5;f[6][3]=10;f[6][4]=10;f[6][5]=5;f[6][6]=1;
	f[7][1]=1;f[7][2]=6;f[7][3]=15;f[7][4]=20;f[7][5]=15;f[7][6]=6;f[7][7]=1;*/
	if(n==3)
	{
		cout<<1<<endl;
		return 0;
	}
	ans=0;
	dfs(0);
	ans%=1000000007;
	cout<<ans<<endl;
	return 0;
}


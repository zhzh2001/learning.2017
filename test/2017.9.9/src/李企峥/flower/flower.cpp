#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll a[200][200],b[200][200],ans[200],ans1[200];
ll sum,n,m;
char ch;
int main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		For(j,1,m)
		{
			ch=getchar();
		    while(ch!='0'&&ch!='1')ch=getchar();
			a[i][j]='1'-ch;
		}
	}
	n--;m++;
	For(i,1,m)a[m][i]=1,a[i][m]=1;
	For(i,1,m)ans[i]=1;
	while(n>0)
	{
		if(n%2==1)
		{
			For(j,1,m)ans1[j]=0;
			For(j,1,m)For(k,1,m)ans1[j]+=(ans[k]*a[k][j])%mo,ans1[j]%=mo;
			For(j,1,m)ans[j]=ans1[j];
		}
		For(i,1,m)For(j,1,m)b[i][j]=0;
		For(i,1,m)For(j,1,m)For(k,1,m)b[i][j]+=(a[i][k]*a[k][j])%mo,b[i][j]%=mo;
		For(i,1,m)For(j,1,m)a[i][j]=b[i][j];
		n/=2;
	}
	sum=0;
	For(i,1,m)sum+=ans[i],sum%=mo;
	sum%=mo;
	cout<<sum<<endl;
	return 0;
}


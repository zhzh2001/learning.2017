#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define mod 1000000007
#define N 100005
#define base 23333
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
ll n,m,h[N];
int T,x[N],y[N];
ll p[10];
const int dx[]={0,1,0,-1,0,1,-1,1,-1};
const int dy[]={0,0,1,0,-1,1,-1,-1,1};
inline bool pd(ll x){
	int l=1,r=T;
	while(l<=r){
		int mid=(l+r)>>1;
		if(h[mid]==x) return 1;
		else if(h[mid]<x) l=mid+1;
		else r=mid-1;
	}
	return 0;
}
inline void doit(int x,int y){
	if(x<=1||x>=n||y<=1||y>=m) return;
	int sum=0;
	For(i,0,8){
		ll nowx=x+dx[i],nowy=y+dy[i];
		if(pd((1ll*nowx*base%mod+nowy)%mod)) sum++;
	}
	p[sum]++;
}
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	n=read();m=read();T=read();
	For(i,1,T){
		x[i]=read();y[i]=read();
		h[i]=(1ll*x[i]*base%mod+y[i])%mod;
	}
	sort(h+1,h+1+T);
	For(i,1,T){
		doit(x[i],y[i]);
		doit(x[i]-1,y[i]);
		doit(x[i],y[i]-1);
		doit(x[i]+1,y[i]);
		doit(x[i],y[i]+1);
		doit(x[i]+1,y[i]+1);
		doit(x[i]-1,y[i]-1);
		doit(x[i]+1,y[i]-1);
		doit(x[i]-1,y[i]+1);
	}
	For(i,1,9) p[i]/=i;
	p[0]=1ll*(n-2)*(m-2);
	For(i,1,9) p[0]-=p[i];
	For(i,0,9) writeln(p[i]);
	return 0;
}

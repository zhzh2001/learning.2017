#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define mod 1000000007
#define N 100005
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,m;
struct mat{
	ll a[105][105];
	mat(){memset(a,0,sizeof a);}
	friend mat operator *(mat a,mat b){
		mat c;
		For(i,0,m) For(j,0,m) For(k,0,m) c.a[i][j]=(c.a[i][j]+1ll*a.a[i][k]*b.a[k][j]%mod)%mod;
		return c;
	}
}a,b;
ll ans;
int main(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read()-1;m=read();
	if(n==-1){cout<<0;return 0;}
	For(i,1,m){
		char s[105];scanf("%s",s+1);
		For(j,1,m) a.a[i][j]=(s[j]-48)^1;
	}
	For(i,0,m) a.a[0][i]=a.a[i][0]=1;
	For(i,0,m) b.a[i][i]=1;
	while(n){
		if(n&1) b=b*a;
		n>>=1;
		if(n) a=a*a;
	}
	For(i,0,m) For(j,0,m) ans=(ans+b.a[i][j])%mod;
	writeln(ans);
	return 0;
}
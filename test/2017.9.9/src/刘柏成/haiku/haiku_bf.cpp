#include <cstdio>
#include <cstring>
using namespace std;
bool can[6][8][6];
int ans[9][6][8][6];
int a[9];
void work(int n){
	memset(can,0,sizeof(can));
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if (a[j-1]-a[i-1]>5)
				break;
			for(int k=j+1;k<=n;k++){
				if (a[k-1]-a[j-1]>7)
					break;
				bool *qwq=can[a[j-1]-a[i-1]][a[k-1]-a[j-1]];
				for(int o=k;o<=n;o++){
					if (a[o]-a[k-1]>5)
						break;
					qwq[a[o]-a[k-1]]=1;
				}
			}
		}
	}
	for(int i=1;i<=5;i++)
		for(int j=1;j<=7;j++){
			bool *qwq=can[i][j];
			int *tat=ans[n][i][j];
			for(int k=1;k<=5;k++)
				tat[k]+=qwq[k];
		}
}
void dfs(int d){
	work(d);
	if (d==8)
		return;
	for(int i=1;i<=10;i++){
		a[d+1]=a[d]+i;
		dfs(d+1);
	}
}
int main(){
	freopen("haiku.cpp","w",stdout);
	dfs(0);
	for(int i=1;i<=8;i++)
		for(int j=1;j<=5;j++)
			for(int k=1;k<=7;k++)
				for(int o=1;o<=5;o++)
					printf("ans[%d][%d][%d][%d]=%d;\n",i,j,k,o,ans[i][j][k][o]);
}
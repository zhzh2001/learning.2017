#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct matrix{
	int m,n;
	int a[103][103];
	void init(int m,int n){
		this->m=m,this->n=n;
		for(int i=1;i<=m;i++)
			for(int j=1;j<=n;j++)
				a[i][j]=0;
	}
	matrix(int m=0,int n=0){
		init(m,n);
	}
	matrix operator *(const matrix& rhs){
		/*if (n!=rhs.m){
			puts("WTF");
			exit(233);
		}*/
		matrix res(m,rhs.n);
		for(int i=1;i<=m;i++)
			for(int j=1;j<=n;j++)
				if (a[i][j])
					for(int k=1;k<=rhs.n;k++)
						res.a[i][k]=(res.a[i][k]+(ll)a[i][j]*rhs.a[j][k])%mod;
		return res;
	}
}mt;
matrix res,t;
void power(matrix& x,ll y){
	/*if (x.m!=x.n){
		puts("WTF");
		exit(233);
	}*/
	res.init(x.m,x.n);
	t=x;
	for(int i=1;i<=x.n;i++)
		res.a[i][i]=1;
	while(y){
		if (y&1)
			res=res*t;
		t=t*t;
		y>>=1;
	}
	x=res;
}

int main(){
	init();
	int n=readint(),m=readint();
	//printf("%lld\n",(1LL<<60)%1000000007);
	mt.init(m+1,m+1);
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++){
			mt.a[i][j]=(readchar()-'0')^1;
			//printf("%d\n",mt.a[i][j]);
		}
	for(int i=1;i<=m+1;i++)
		mt.a[i][m+1]=mt.a[m+1][i]=1;
	power(mt,n);
	ll ans=0;
	for(int i=1;i<=m+1;i++)
		(ans+=mt.a[m+1][i])%=mod;
	printf("%lld\n",ans);
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
pair<int,int> a[1000001];
int ans[10];
int main(){
	init();
	int h=readint(),w=readint(),n=readint(),cur=0;
	for(int i=1;i<=n;i++){
		int x=readint(),y=readint();
		for(int j=0;j<=2;j++)
			for(int k=0;k<=2;k++)
				if (x+j>=3 && x+j<=h && y+k>=3 && y+k<=w)
					a[++cur]=make_pair(x+j,y+k);
	}
	sort(a+1,a+cur+1);
	int now=0,cnt=0;
	for(int i=1;i<=cur;i++){
		now++;
		if (i==cur || a[i]!=a[i+1]){
			cnt++;
			ans[now]++;
			now=0;
		}
	}
	printf("%lld\n",(ll)(h-2)*(w-2)-cnt);
	for(int i=1;i<=9;i++)
		printf("%d\n",ans[i]);
}
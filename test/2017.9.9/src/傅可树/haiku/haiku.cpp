#include<cstdio>
using namespace std;
bool can[200];
int x,y,z,n,ans,sum[45];
inline bool judge()
{
	for (int i=1;i<=n;i++){
		can[sum[i]]=1;
	}
	bool flag=0;
	for (int i=n;i;i--){
		if (sum[i]<x+y+z) break;
		if (can[sum[i]-z]&&can[sum[i]-z-y]&&can[sum[i]-z-y-x]) {
			flag=1; break;
		}
	}
	for (int i=1;i<=n;i++){
		can[sum[i]]=0;
	}
	return flag;
}
void dfs(int len)
{
	if (len==n+1){
		if (judge()) ans++;
		return;
	}
	for (int i=1;i<=10;i++){
		sum[len]=sum[len-1]+i;
		dfs(len+1);
	}
}
int main()
{
	freopen("haiku.in","r",stdin);
	freopen("haiku.out","w",stdout);	
	scanf("%d%d%d%d",&n,&x,&y,&z);
	can[0]=1;
	dfs(1);
	printf("%d\n",ans);
	return 0;
}

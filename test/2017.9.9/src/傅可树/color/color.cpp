#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<map>
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
int h,tot,w,n,ans[15];
inline char gc()
{
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read()
{
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
map<ll , int> a;
inline ll get(int x,int y)
{
	return x*(w-1)+y;
}
inline void init(int x,int y,int xx,int yy)
{
	if (x>h-2||y>w-2||x<=0||y<=0) return;
	int hhash=get(x,y);
	a[hhash]++;
}
int main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout); 
	h=read(); w=read(); n=read();
	for (int k=1;k<=n;k++){
		int x,y;
		x=read(); y=read();
		for (int i=x-2;i<=x;i++){
			for (int j=y-2;j<=y;j++){
				init(i,j,x,y);
			}
		}
	}
	ll all=(h-2)*(w-2);
	for (map<ll , int>::iterator i=a.begin();i!=a.end();++i){
		ans[i->second]++;
		++tot;
	}
	ans[0]=all-tot;
	for (int i=0;i<=9;i++){
		printf("%d\n",ans[i]);
	}
	return 0;
}

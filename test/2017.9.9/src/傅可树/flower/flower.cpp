#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const int maxn=105,mod=1e9+7;
struct matrix{
	ll mat[maxn][maxn];
};
int n,m;
matrix operator * (matrix A,matrix B)
{
	matrix C;
	for (int i=0;i<=m;i++){
		for (int j=0;j<=m;j++){
			C.mat[i][j]=0;
			for (int k=0;k<=m;k++){
				(C.mat[i][j]+=A.mat[i][k]*B.mat[k][j])%=mod;
			}
		}
	}
	return C;
}
inline matrix ksm(matrix A,int k)
{
	matrix B;
	memset(B.mat,0,sizeof(B.mat));
	for (int i=0;i<=m;i++){
		B.mat[i][i]=1;
	}
	while (k){
		if (k&1){
			B=B*A;
		}
		A=A*A; k>>=1;
	}
	return B;
}
int main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	matrix A;
	memset(A.mat,0,sizeof(A.mat));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		char s[105];
		scanf("%s",s+1);
		for (int j=1;j<=m;j++){
			A.mat[i][j]=s[j]-'0';
			A.mat[i][j]=1-A.mat[i][j];
		}
	}
	for (int i=0;i<=m;i++){
		A.mat[0][i]=1;
		A.mat[i][0]=1;
	}
	matrix C=ksm(A,n);
	ll ans=0;
	for (int i=0;i<=m;i++){
		(ans+=C.mat[0][i])%=mod;
	}
	printf("%lld\n",ans);
	return 0;
}

#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 500010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
struct data{	ll m[110][110];	}f,c,tmp;
ll n,m,ans;	const ll mod=1e9+7;	char s[110];
inline data cheng(data a,data b){
	For(i,1,m)	For(j,1,m)	tmp.m[i][j]=0;
	For(i,1,m)	For(j,1,m)	For(k,1,m)	tmp.m[i][k]=(tmp.m[i][k]+a.m[i][j]*b.m[j][k])%mod;
	return tmp;
}
int main(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();	m=read();
	For(i,1,m+1)	For(j,1,m+1)	c.m[i][j]=1;
	For(i,1,m){
		scanf("%s",s+1);
		For(j,1,m)	c.m[i][j]=(s[j]-'0')^1;
	}++m;	f.m[m][m]=1;
	while(n){
		if (n&1)	f=cheng(f,c);	c=cheng(c,c);
		n>>=1;
	}
	For(i,1,m)	For(j,1,m)	ans=(ans+f.m[i][j])%mod;
	writeln(ans);
}

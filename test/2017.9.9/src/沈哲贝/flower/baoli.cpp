#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 1010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll mod=1e9+7;	char s[maxn];
ll mp[maxn][maxn],f[maxn][maxn],ans,n,m;
int main(){
	freopen("flower.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	m=read();
	For(i,0,m)	For(j,0,m)	mp[i][j]=1;
	For(i,1,m){
		scanf("%s",s+1);
		For(j,1,m)	mp[i][j]=s[j]=='0';
	}
	f[0][0]=1;
	For(i,1,n)	For(j,0,m)	For(k,0,m)	if (mp[j][k])	f[i][k]=(f[i][k]+f[i-1][j])%mod;
	For(i,0,m)	ans+=f[n][i];	writeln(ans%mod);
}

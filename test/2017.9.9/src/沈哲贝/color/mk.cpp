#include<map>
#include<cstdio>
#include<ctime>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
const ll mod=1e9;
inline ll get(){	return 1LL*rand()*rand()*rand();	}
map<pair<ll,ll>,bool>mp;
int main(){
	srand(time(0));
	freopen("co.in","w",stdout);
	ll h=get()%mod+1,w=get()%mod+1,n=min(get()%100000+1,h*w);
	printf("%lld %lld %lld\n",h,w,n);
	For(i,1,n){
		ll x=get()%h+1,y=get()%w+1;
		while(mp[make_pair(x,y)])	x=get()%h+1,y=get()%w+1;
		mp[make_pair(x,y)]=1;	printf("%lld %lld\n",x,y);
	}
}

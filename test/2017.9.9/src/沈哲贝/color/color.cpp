#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 1000010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll p[maxn],map[100],h,w,n,tot;
inline bool pd(ll x,ll y){	return x>1&&y>1&&x<h&&y<w;	}
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	h=read();	w=read();	n=read();
	For(i,1,n){
		ll x=read(),y=read();
		For(a,-1,1)	For(b,-1,1)
		if (pd(a+x,b+y))	p[++tot]=(a+x)<<31|(b+y);
	}
	sort(p+1,p+tot+1);	map[0]=(w-2)*(h-2);
	for(ll i=1,j;i<=tot;i=j+1){
		j=i;
		while(j<tot&&p[j+1]==p[j])	++j;
		map[j-i+1]++,map[0]--;
	}
	For(i,0,9)	writeln(map[i]);
}

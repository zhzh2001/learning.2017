#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 1000010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
struct data{	ll x,y;	}p[maxn];
ll map[100],h,w,n,tot;
bool cmp(data a,data b){	return a.x==b.x?a.y<b.y:a.x<b.x;	}
int main(){
	freopen("co.in","r",stdin);
	freopen("baoli.out","w",stdout);
	h=read();	w=read();	n=read();
	For(i,1,n){
		ll x=read(),y=read();
		p[++tot].x=x+1,	p[tot].y=y-1;
		p[++tot].x=x,	p[tot].y=y-1;
		p[++tot].x=x-1,	p[tot].y=y-1;
		p[++tot].x=x+1,	p[tot].y=y;
		p[++tot].x=x,	p[tot].y=y;
		p[++tot].x=x-1,	p[tot].y=y;
		p[++tot].x=x+1,	p[tot].y=y+1;
		p[++tot].x=x,	p[tot].y=y+1;
		p[++tot].x=x-1,	p[tot].y=y+1;
	}
	sort(p+1,p+tot+1,cmp);	map[0]=(w-2)*(h-2);
	for(ll i=1,j;i<=tot;i=j+1){
		j=i;
		while(j<tot&&p[j+1].x==p[j].x&&p[j+1].y==p[j].y)	++j;
		if (p[i].x>1&&p[i].y>1&&p[i].x<=h-1&&p[i].y<=w-1)	map[j-i+1]++,map[0]--;
	}
	For(i,0,9)	writeln(map[i]);
}

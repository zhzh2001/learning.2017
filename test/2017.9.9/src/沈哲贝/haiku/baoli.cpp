#include<ctime>
#include<cstdio>
#include<memory.h>
#include<algorithm>
#define ll unsigned long long
#define maxn 300010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll ans,n,a[maxn],x,y,z;
void dfs(ll dep){
	if (dep>n){
		For(i,0,n)	For(j,i,n)	For(k,j,n)	For(l,k,n)	if(a[j]-a[i]==x&&a[k]-a[j]==y&&a[l]-a[k]==z)	return ++ans,void(0);
		return;
	}
	For(i,1,10)	a[dep]=a[dep-1]+i,dfs(dep+1);
}
int main(){
	freopen("haiku.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	x=read();	y=read();	z=read();
	dfs(1);	writeln(ans);
}

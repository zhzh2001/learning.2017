#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++)  
using namespace std ; 

const int N = 211,mod = 1e9+7 ; 
int n,m ; 
int dp[2][N] ;
bool f[N][N] ;  
char s[N] ; 

inline int read() 
{
	int x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

int main() 
{
	freopen("flower.in","r",stdin) ; 
	freopen("flower.out","w",stdout) ; 
	n = read() ; m = read() ; 
	dp[0][0] = 1 ; 
	For(i,1,m) {
		scanf("%s",s+1) ; 
		For(j,1,m) f[i][j] = s[j]-48 ; 
	}
	int now ; 
	For(i,1,n) {
	  now = i&1 ; 	
	  For(j,0,m) dp[now][j] = 0 ; 
	  For(j,0,m) 
		For(k,0,m) 
		 if(!f[j][k]) {
		  dp[now][j] = dp[now][j]+dp[now^1][k] ; 
		  if(dp[now][j]>=mod) dp[now][j]%=mod; 
		 }
	}
	int sum = 0 ; 
	For(i,0,m) {
		sum = sum+dp[now][i]; 
		if(sum>=mod) sum%=mod ; 
	}
	printf("%d\n",sum) ; 
	return 0 ; 
}

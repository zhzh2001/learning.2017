#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
using namespace std ; 

int n,X,Y,Z,mx,mn,ans ;
int a[20],now,sum[20] ; 

inline int read() 
{
	int x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

inline bool check(int val) 
{
	now = 0 ; 
	while(val) {
		a[++now] = val%10 ; 
		if(a[now]==0) a[now]+=10 ; 
		val/=10 ; 
	}
	For(i,1,now) if(a[i]==0) return false ; 
	For(i,1,now/2) swap(a[i],a[now-i+1]) ; 
	sum[0]=0 ; 
	For(i,1,now) sum[i]=sum[i-1]+a[i] ; 
	For(x,1,n-2) 
	  For(y,x+1,n-1) 
	    For(z,y+1,n) 
	      For(w,z,n) {
	      	if(sum[y-1]-sum[x-1]==X&&sum[z-1]-sum[y-1]==Y&&
			   sum[w]-sum[z-1]==Z) 
			   return true ; 
		  }
	return false ; 
}

int main() 
{
	freopen("haiku.in","r",stdin) ; 
	freopen("haiku.out","w",stdout) ; 
	n = read() ; X = read() ; Y = read() ; Z = read() ; 
	mx = 1 ;   
	if(n>=8) {
		printf("0\n") ; 
		return 0 ; 
	}
	For(i,1,n) mx*=10 ; mx-- ; 
	For(i,0,mx) if( check(i) ) ans++ ; 
	printf("%d\n",ans) ; 
	return 0 ; 
}

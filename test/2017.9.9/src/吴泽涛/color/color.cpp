#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define LL long long 
using namespace std ; 

inline int read() 
{
	int x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

const int N = 100011 ; 
const int dx[10] = { 0,-1,-1,-1,0,0,0,1,1,1 } ; 
const int dy[10] = { 0,-1,0,1,-1,0,1,-1,0,1 } ; 
int n,cnt,h,w ; 
struct point{
	int x,y ; 
}a[N];
LL tt[20],ans[20] ; 

inline bool cmp(point a,point b) 
{
	if(a.x!=b.x) return a.x < b.x ; 
	return a.y < b.y ; 
}

int main() 
{
	freopen("color.in","r",stdin) ; 
	freopen("color.out","w",stdout) ; 
	h = read() ; w = read() ;  n = read() ; 
	int xx,yy ; 
	For(i,1,n) {
		xx = read() ; yy = read() ; 
		For(j,1,9) {
			int xxx = xx+dx[ j ] ; 
			int yyy = yy+dy[ j ] ; 
			if(xxx>1&&xxx<h&&yyy>1&&yyy<w) {
				a[++cnt].x = xxx ; 
				a[cnt].y = yyy ; 
			}	
		}
	}
	sort(a+1,a+cnt+1,cmp) ; 
	int sum = -1 ; 
	a[0].x = 0 ; a[0].y = 0 ; 
	For(i,1,cnt) {
		if(a[i].x==a[i-1].x&&a[i].y==a[i-1].y) sum++ ; 
		else ans[sum]++,sum = 1 ; 
	}
	ans[sum]++ ; 
	For(i,1,9) ans[0]+=ans[i] ; 
	ans[0] = 1ll*(h-2)*(w-2)-ans[0] ; 
	For(i,0,9) printf("%lld\n",ans[ i ]) ; 
	return 0 ; 
}



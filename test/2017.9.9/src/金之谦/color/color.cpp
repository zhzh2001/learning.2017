#include <cstdio>
#include <algorithm>
#include <cstring>
#include <map>
#define mp make_pair
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
map<pair<int,int>,int>s,b;
struct ppap{int x,y;}a[100010];
int n,m,q;
ll ans[10]={0};int d[6][6];
int main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	n=read();m=read();q=read();
	for(int i=1;i<=q;++i)a[i].x=read(),a[i].y=read(),s[mp(a[i].x,a[i].y)]=1;
	int now=1,x,y,rp;
	for(;q--;++now){
		//memset(d,0,sizeof d);
		x=a[now].x;y=a[now].y;
		int li=max(1,x-2),ri=min(n,x+2),lj=max(1,y-2),rj=min(m,y+2);
		for(int i=li;i<=ri;++i)
			for(int j=lj;j<=rj;++j)d[i-x+3][j-y+3]=d[i-x+2][j-y+3]+d[i-x+3][j-y+2]+s[mp(i,j)]-d[i-x+2][j-y+2];
		li=max(x,3);lj=max(y,3);
		for(int i=li;i<=ri;++i)
			for(int j=lj;j<=rj;++j){
				if(b[mp(i,j)])continue;b[mp(i,j)]=1;
				rp=d[i-x+3][j-y+3]-d[i-x][j-y+3]-d[i-x+3][j-y]+d[i-x][j-y];
				++ans[rp];
			}
	}
	ans[0]=(ll)(n-2)*(m-2);
	for(int i=1;i<10;++i)ans[0]-=ans[i];
	for(int i=0;i<10;++i)printf("%lld\n",ans[i]);
	return 0;
}

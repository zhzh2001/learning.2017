#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,x,y,z,ma,b[2],bin[41];
int X[10],Y[10],Z[10];
inline void dfs(int now,int sum){
	if(now>ma)return;
	if(sum==x)X[now]++;if(sum==y)Y[now]++;if(sum==z)Z[now]++;
	for(int i=1;i<=10;i++)dfs(now+1,sum+i);
}
inline int check(int rp){
	int l=b[1],m=b[0]-b[1],r=rp-b[0];
	return X[l]*Y[m]%MOD*Z[r]%MOD;
}
inline int dfss(int xz,int now,int p){
	if(now==xz)return (!p)?check(xz):0;
	int ans=0;
	if(p)b[p-1]=now,ans=(ans+dfss(xz,now+1,p-1))%MOD;
	ans=(ans+dfss(xz,now+1,p))%MOD;return ans;
}
signed main()
{
	freopen("haiku.in","r",stdin);
	freopen("haiku.out","w",stdout);
	n=read();x=read();y=read();z=read();
	bin[0]=1;for(int i=1;i<=n;i++)bin[i]=bin[i-1]*10%MOD;
	ma=max(max(x,y),z);
	dfs(0,0);int ans=0;
	for(int i=3;i<=min(n,x+y+z);i++){
		int p=dfss(i,0,2);
		ans=(ans+p*(n-i+1)*bin[n-i])%MOD;
	}
	printf("%lld",ans);
	return 0;
}
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct juzhen{int a[110][110];}a,p;
char c[110][110];
int n,m;
inline juzhen cheng(juzhen a,juzhen b){
	memset(p.a,0,sizeof p.a);
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++)
			for(int k=1;k<=m;k++)p.a[i][j]=(p.a[i][j]+a.a[i][k]*b.a[k][j]%MOD)%MOD;
	return p;
}
inline juzhen mi(juzhen a,int b){
	juzhen x,y;x=y=a;
	while(b){
		if(b&1==1)x=cheng(x,y);
		y=cheng(y,y);b>>=1;
	}
	return x;
}
signed main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();m=read();
	if(n==1)return printf("%lld",m+1)&0;
	for(int i=1;i<=m;i++)scanf("%s",c[i]+1);
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++)a.a[i][j]=(c[i][j]-'0')^1;
	m++;for(int i=1;i<=m;i++)a.a[i][m]=a.a[m][i]=1;
	a=mi(a,n-2);int ans=0;
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++)(ans+=a.a[i][j])%=MOD;
	printf("%lld",ans);
	return 0;
}
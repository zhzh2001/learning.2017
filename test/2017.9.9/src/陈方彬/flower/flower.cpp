#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=101;
struct H{
	Ll a[N][N];
	H(){memset(a,0,sizeof a);}
}a;
Ll n,m,mo=1e9+7;
Ll ans;
char c;
H cheng(H a,H b){
	H c;
	for(Ll i=0;i<=m;i++)
		for(Ll k=0;k<=m;k++)if(a.a[i][k])
			for(Ll j=0;j<=m;j++)
				c.a[i][j]=(c.a[i][j]+a.a[i][k]*b.a[k][j])%mo;
	return c;
}
H ksm(H x,Ll k){
	H ans=x;
	for(k--;k;k/=2,x=cheng(x,x))
		if(k&1)ans=cheng(ans,x);
	return ans;
}
int main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout); 
	scanf("%lld%lld",&n,&m);
	if(n==0){printf("1");return 0;}
	if(n==1){cout<<m;return 0;}
	for(Ll i=1;i<=m;i++)
		for(Ll j=1;j<=m;j++){
			cin>>c;
			if(c=='1')a.a[i][j]=1;
		}
	for(Ll i=0;i<=m;i++)
		for(Ll j=0;j<=m;j++)
			a.a[i][j]=1-a.a[i][j];
	a=ksm(a,n-1);
	for(Ll i=0;i<=m;i++)
		for(Ll j=0;j<=m;j++)
			ans=(ans+a.a[i][j])%mo;
	printf("%lld",ans);
}

#include<bits/stdc++.h>
#define Ll long long
#define P(x,y) (Ll)((x-1)*m+y) 
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int R(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const Ll N=1e5+5;
map<Ll,bool>F,vi;
Ll in[N][2],ans[10];
Ll n,m,k,x,y,sum;
void find(Ll xx,Ll yy){
	for(Ll i=xx-3+1;i<=xx;i++)
		for(Ll j=yy-3+1;j<=yy;j++)
			if(i>=1&&i<=n-3+1&&j>=1&&j<=m-3+1)
				if(!vi[P(i,j)]){
					vi[P(i,j)]=1;
					Ll sum=0;
					for(Ll x=i;x<=i+2;x++)
						for(Ll y=j;y<=j+2;y++)
							if(F[P(x,y)])sum++;
					ans[sum]++;
				}
}
int main()
{
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	n=R();m=R();k=R();
	for(Ll i=1;i<=k;i++){
		x=R();y=R();
		F[P(x,y)]=1;
		in[i][0]=x;in[i][1]=y;
	}
	for(Ll i=1;i<=k;i++)find(in[i][0],in[i][1]);
	for(Ll i=1;i<=9;i++)sum+=ans[i];
	printf("%lld\n",(n-3+1)*(m-3+1)-sum);
	for(Ll i=1;i<=9;i++)printf("%lld\n",ans[i]);
}

#include <bits/stdc++.h>
#define ll long long
#define mod 1000000007
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int ans[120][120], tmp[120][120], sz, orz[120][120];
char ch[180];
bool multmp(){
	memset(orz, 0, sizeof(int)*120*120);
	for(int k = 0; k <= sz; k++)
		for(int i = 0; i <= sz; i++) if(tmp[i][k])
			for(int j = 0; j <= sz; j++) if(ans[k][j])
				orz[i][j] = (orz[i][j]+1ll*tmp[i][k]*ans[k][j])%mod;
	memcpy(tmp, orz, sizeof(int)*120*120);
}
bool mulans(){
	memset(orz, 0, sizeof(int)*120*120);
	for(int k = 0; k <= sz; k++)
		for(int i = 0; i <= sz; i++) if(ans[i][k])
			for(int j = 0; j <= sz; j++) if(ans[k][j])
				orz[i][j] = (orz[i][j]+1ll*ans[i][k]*ans[k][j])%mod;
	memcpy(ans, orz, sizeof(int)*120*120);
}
int pwr(int p){
	for(int i = 0; i <= sz; i++) tmp[i][i] = 1;
	for(;p;p>>=1,mulans())if(p&1)multmp();
	return tmp[0][0];
}
int main(){
	freopen("flower.in", "r", stdin);
	freopen("flower.out", "w", stdout);
	int n = read(); sz = read();
	ans[0][0] = 1;
	for(int i = 1; i <= sz; i++) ans[0][i] = ans[i][0] = 1;
	for(int i = 1; i <= sz; i++){
		scanf("%s", ch+1);
		for(int j = 1; j <= sz; j++)
			ans[i][j] = ch[j]-'0';
	}
	printf("%d\n", pwr(n+1));
}
/*
下面全是flag

puts("如果这个做法是错的，我会把王路抬起来。"); // c
System.out.println("如果这个做法是错的，我会把王路抬起来。"); // java
print "如果这个做法是错的，我会把王路抬起来。" # python
console.log("如果这个做法是错的，我会把王路抬起来。") // javascript
main = putStrLn "如果这个做法是错的，我会把王路抬起来。" -- haskell
echo "如果这个做法是错的，我会把王路抬起来。"; // php
cout << "如果这个做法是错的，我会把王路抬起来。" << endl; // c++
println!("如果这个做法是错的，我会把王路抬起来。"); // rust
attr_writer :如果这个做法是错的，我会把王路抬起来。 # ruby
writeln('如果这个做法是错的，我会把王路抬起来。'); { pascal }
<span>如果这个做法是错的，我会把王路抬起来。</span> <!-- html -->
Exception in thread "main": java.lang.ArrayIndexOutOfBoundsException

*/

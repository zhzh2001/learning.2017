#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct orz_zyy{
	int x, y;
	bool operator < (const orz_zyy &b) const {
		return x == b.x ? y < b.y : x < b.x;
	}
}r[N];
int a[N<<2], b[N<<2], at, bt;
long long ans[10];
bool f[3][N];
int as[N<<2];
int main(int argc, char const *argv[]){
	freopen("color.in", "r", stdin);
	freopen("color.out", "w", stdout);
	int h = read(), w = read(), n = read();
	for(int i = 1; i <= n; i++){
		int x = read(), y = read(); r[i].x = x; r[i].y = y;
		a[++at] = x-1; a[++at] = x; a[++at] = x+1;
		b[++bt] = y-1; b[++bt] = y; b[++bt] = y+1;
	}
	sort(a+1, a+at+1); sort(b+1, b+bt+1);
	at = unique(a+1, a+at+1)-a-1;
	bt = unique(b+1, b+bt+1)-b-1;
	for(int i = 1; i <= n; i++){
		r[i].x = lower_bound(a+1, a+at+1, r[i].x)-a;
		r[i].y = lower_bound(b+1, b+bt+1, r[i].y)-b;
	}
	sort(r+1, r+n+1);
	for(int k = 2+!a[1], ls = 1, rs = 1; k < at; k++){
		if(k == at-1 && a[at] == h+1) break;
		while(r[rs+1].x <= k+1 && rs < n) rs++;
		while(r[ls].x < k-1 && ls <= n) ls++;
		// printf("k = %d, (%d, %d)\n", k,ls,rs);
		memset(as, 0, sizeof(int)*N<<2);
		for(int i = ls; i <= rs; i++){
			int y = r[i].y;
			// printf("%d(%d)", y, a[y]);
			if(a[y-1] > 1) --ans[as[y-1]], ++ans[++as[y-1]];
			if(a[y] > 1 && a[y] < w)--ans[as[y]], ++ans[++as[y]];
			if(a[y] < w && a[y+1] < w) --ans[as[y+1]], ++ans[++as[y+1]];
			// puts("");
		}
	}
	ans[0] = 1ll*(w-2)*(h-2);
	for(int i = 1; i < 10; i++) ans[0] -= ans[i];
	for(int i = 0; i < 10; i++) printf("%lld\n", ans[i]);
	return 0;
}
/*
千本桜　夜に紛れ
君の声も　届かないよ
此処は宴　鋼の檻
その断頭台で　見下ろして
三千世界　貝爺AK王
嘆く唄も　聞こえないよ
青藍の空　遥か彼方
その光線？で　？？？？？
*/
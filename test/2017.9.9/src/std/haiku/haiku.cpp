#include <fstream>
#include <cstring>
using namespace std;

typedef int state_t;

const int Mod = 1e9 + 7, MaxN = 41, StateLen = 5 + 7 + 5;
int N, X, Y, Z, p[MaxN], f[MaxN][1 << StateLen];
state_t end_state;

state_t dp(int i, int j) {
  if (i == N + 1) {
    return 0;
  }
  if (~f[i][j]) {
    return f[i][j];
  }
  long long res = 0;
  for (int k = 1; k <= 10; ++k) {
    int next_state = (j << k) | 1;
    if ((next_state & end_state) == end_state) {
      res += p[N - i];
    } else {
      res += dp(i + 1, next_state & ((1 << 17) - 1));
    }
  }
  return f[i][j] = res % Mod;
}

int main() {
  ifstream fin("haiku.in");
  ofstream fout("haiku.out");
  memset(f, 0xff, sizeof f);
  fin >> N >> X >> Y >> Z;
  p[0] = 1;
  for (int i = 1; i < MaxN; ++i) {
    p[i] = p[i - 1] * 10LL % Mod;
  }
  end_state = (1 << X) | (1 << (X + Y)) | (1 << (X + Y + Z));
  fout << dp(1, 1) << endl;
  return 0;
}

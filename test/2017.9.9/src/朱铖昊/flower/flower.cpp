#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=105,mod=1e9+7;
int a[N][N],ans[N][N],f[N][N],sum,n,m;
int main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
		for (int j=1;j<=m;++j)
			scanf("%1d",&a[i][j]);
	for (int i=0;i<=m;++i)
		for (int j=0;j<=m;++j)
			a[i][j]=!a[i][j];
	for (int i=0;i<=m;++i)
		ans[i][i]=1;
	for (;n!=0;n/=2)
	{	
		if (n&1)
		{
			memset(f,0,sizeof(f));
			for (int i=0;i<=m;++i)
				for (int j=0;j<=m;++j)
					for (int k=0;k<=m;++k)
						f[i][j]=((ll)f[i][j]+(ll)ans[i][k]*a[k][j])%mod;
			for (int i=0;i<=m;++i)
				for (int j=0;j<=m;++j)
					ans[i][j]=f[i][j];
		}
		memset(f,0,sizeof(f));
		for (int i=0;i<=m;++i)
			for (int j=0;j<=m;++j)
				for (int k=0;k<=m;++k)
					f[i][j]=((ll)f[i][j]+(ll)a[i][k]*a[k][j])%mod;
		for (int i=0;i<=m;++i)
			for (int j=0;j<=m;++j)
				a[i][j]=f[i][j];
	}
	sum=0;
	for (int i=0;i<=m;++i)
		sum=((ll)sum+ans[0][i])%mod;
	cout<<sum;
	return 0;
}
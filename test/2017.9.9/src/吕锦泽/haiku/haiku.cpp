#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 50
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,X,Y,Z,a[N],sum[N],ans;
void judge(){
	rep(i,1,n) sum[i]=sum[i-1]+a[i];
	rep(x,0,n) rep(y,x+1,n) rep(z,y+1,n) rep(w,z+1,n)
		if (sum[y]-sum[x]==X&&sum[z]-sum[y]==Y&&sum[w]-sum[z]==Z){
			++ans; return;
		}
}
void dfs(ll x){
	if (x==n+1) { judge(); return; }
	rep(i,1,10) a[x]=i,dfs(x+1);
}
int main(){
	freopen("haiku.in","r",stdin);
	freopen("haiku.out","w",stdout);
	n=read(); X=read(); Y=read(); Z=read();
	dfs(1); printf("%d",ans);
}

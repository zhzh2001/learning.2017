#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) per (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll h,w,n,tot,num[15];
struct data{ ll x,y; }a[N*10];
bool cmp(data x,data y){
	if (x.x==y.x) return x.y<y.y;
	else return  x.x<y.x;
}
bool ok(ll x,ll y){
	return x>=2&&x<=h-1&&y>=2&&y<=w-1;
}
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout);
	h=read(); w=read(); n=read();
	rep(i,1,n){
		ll x=read(),y=read();
		if (ok(x-1,y-1)) a[++tot]=(data){x-1,y-1};
		if (ok(x-1,y)) a[++tot]=(data){x-1,y};
		if (ok(x-1,y+1)) a[++tot]=(data){x-1,y+1};
		if (ok(x,y-1)) a[++tot]=(data){x,y-1};
		if (ok(x,y)) a[++tot]=(data){x,y};
		if (ok(x,y+1)) a[++tot]=(data){x,y+1};
		if (ok(x+1,y-1)) a[++tot]=(data){x+1,y-1};
		if (ok(x+1,y)) a[++tot]=(data){x+1,y};
		if (ok(x+1,y+1)) a[++tot]=(data){x+1,y+1};
	}
	sort(a+1,a+1+tot,cmp);
	for (ll l=1,r=1;l<=tot;l=r+1){
		while (a[r+1].x==a[l].x&&a[r+1].y==a[l].y) ++r;
		++num[r-l+1];
	}
	num[0]=(h-2)*(w-2);
	rep(i,1,9) num[0]-=num[i];
	rep(i,0,9) printf("%lld\n",num[i]);
}

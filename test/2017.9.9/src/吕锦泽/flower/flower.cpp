#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 105
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,sum; char s[N];
struct matrix{ ll a[N][N]; }ans,tmp;
matrix operator *(matrix a,matrix b){
	matrix c; memset(c.a,0,sizeof c.a);
	rep(i,0,m) rep(k,0,m) if (a.a[i][k]){
		rep(j,0,m) (c.a[i][j]+=a.a[i][k]*b.a[k][j]%mod)%=mod;
	}
	return c;
}
matrix operator ^(matrix x,ll y){
	matrix num; memset(num.a,0,sizeof num.a);
	rep(i,0,m) num.a[i][i]=1;
	for (;y;y>>=1,x=x*x)
		if (y&1) num=num*x;
	return num;
}
int main(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read(); m=read();
	rep(i,1,m){
		scanf("%s",s+1);
		rep(j,1,m) tmp.a[i][j]=s[j]-'0';
	}
	rep(i,0,m) rep(j,0,m) tmp.a[i][j]^=1;
	ans.a[0][0]=1;
	ans=ans*(tmp^n);
	rep(i,0,m) (sum+=ans.a[0][i])%=mod;
	printf("%lld",sum);
}

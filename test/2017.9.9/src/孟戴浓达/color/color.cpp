#include<algorithm>
#include<fstream>
#include<map>
using namespace std;
ifstream fin("color.in");
ofstream fout("color.out");
int h,w,n;
int x[100003],y[100003];
map<pair<int,int>,int> mp;
struct heihei{
	int x,y;
}en[5000003];
inline bool cmp(const heihei a,const heihei b){
	if(a.x==b.x) return a.y<b.y;
	return a.x<b.x;
}
int max(int a,int b){
	if(a>b) return a;
	return b;
}
int min(int a,int b){
	if(a<b) return a;
	return b;
}
int top=0;
unsigned long long ans[13];
int main(){
	fin>>h>>w>>n;
	ans[0]=(h-2)*(w-2);
	for(int i=1;i<=n;i++){
		fin>>x[i]>>y[i];
		for(int j=max(1,x[i]-2);j<=min(x[i],h-2);j++){
			for(int k=max(1,y[i]-2);k<=min(y[i],w-2);k++){
				//cout<<j<<" "<<k<<endl;
				mp[make_pair(j,k)]=mp[make_pair(j,k)]+1;
				en[++top].x=j;
				en[top].y=k;
			}
		}
	}
	/*
	for(int i=1;i<=h;i++){
		for(int j=1;j<=w;j++){
			cout<<mp[make_pair(i,j)]<<" ";
		}
		cout<<endl;
	}
	*/
	en[0].x=0,en[0].y=0;
	sort(en+1,en+top+1,cmp);
	for(int i=1;i<=top;i++){
		if(en[i].x!=en[i-1].x||en[i].y!=en[i-1].y){
			ans[0]--;
			ans[mp[make_pair(en[i].x,en[i].y)]]++;
		}
	}
	for(int i=0;i<=9;i++){
		fout<<ans[i]<<endl;
	}
	return 0;
}
/*

in:
4 5 8
1 1
1 4
1 5
2 3
3 1
3 2
3 4
4 4

out:
0
0
0
2
4
0
0
0
0
0

*/

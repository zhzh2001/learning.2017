#include<algorithm>
#include<fstream>
#include<string.h>
using namespace std;
ifstream fin("flower.in");
ofstream fout("flower.out");
const long long mod=1000000007;
int mp[103][103];
int n,m;
struct matrix{
	long long mat[103][103];
}x,y;
inline matrix mul(matrix a,matrix b){
	matrix c;
	memset(c.mat,0,sizeof(c.mat));
	for(int i=1;i<=m+1;i++){
		for(int j=1;j<=m+1;j++){
			for(int k=1;k<=m+1;k++){
				if(a.mat[i][k]!=0&&b.mat[k][j]!=0){
					c.mat[i][j]=(c.mat[i][j]+a.mat[i][k]*b.mat[k][j]%mod)%mod;
				}
			}
		}
	}
	return c;
}
matrix qpow(matrix a,int xx){
	matrix fanhui;
	memset(fanhui.mat,0,sizeof(fanhui.mat));
	for(int i=1;i<=m+1;i++){
		fanhui.mat[i][i]=1;
	}
	for(;xx;xx>>=1,a=mul(a,a)){
		if(xx&1){
			fanhui=mul(fanhui,a);
		}
	}
	return fanhui;
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=m;j++){
			char xx;
			fin>>xx;
			if(xx=='0'){
				x.mat[i][j]=0;
			}else{
				x.mat[i][j]=1;
			}
			x.mat[i][j]^=1;
			if(i==j){
				y.mat[i][j]=1;
			}
		}
	}
	y.mat[m+1][m+1]=1;
	for(int i=1;i<=m+1;i++){
		x.mat[i][m+1]=1;
		x.mat[m+1][i]=1;
	}
	x=qpow(x,n-1);
	y=mul(y,x);
	long long ans=0;
	for(int i=1;i<=m+1;i++){
		for(int j=1;j<=m+1;j++){
			//cout<<y.mat[i][j]<<" "<<i<<" "<<j<<endl;
			ans=(ans+y.mat[i][j])%mod;
		}
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
100000 2
01
10

out:
7

*/

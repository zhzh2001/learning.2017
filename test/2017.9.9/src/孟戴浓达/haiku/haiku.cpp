#include<algorithm>
#include<cstring>
#include<fstream>
using namespace std;
ifstream fin("haiku.in");
ofstream fout("haiku.out");
const long long mod=1000000007;
long long jiyi[45][11][11][11][11];
bool vis[45][11][11][11][11];
long long f[45];
int n,x,y,z;
long long dp(int now,int x1,int x2,int x3,int x4){
	long long &fanhui=jiyi[now][x1][x2][x3][x4];
	bool &visit=vis[now][x1][x2][x3][x4];
	if(visit){
		return fanhui;
	}
	if(now>=n+1){
		visit=true;
		return fanhui=0;
	}
	fanhui=0;
	for(int i=1;i<=10;i++){
		if(now>4){
			int sum=x1+x2+x3+x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x1;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x2;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x3;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
		}
		if(now==4){
			int sum=x2+x3+x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x2;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x3;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
		}
		if(now==3){
			int sum=x3+x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x3;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
		}
		if(now==2){
			int sum=x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
			sum-=x4;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
		}
		if(now==1){
			int sum=0;
			if(sum+i==x){
				fanhui=(fanhui+f[now+1]+mod)%mod;
			}
		}
		fanhui=(fanhui+dp(now+1,x2,x3,x4,i))%mod;
	}
	visit=true;
	return fanhui;
}
long long dpx[13][13];
long long fac[45];
int main(){
	fin>>n>>x>>y>>z;
	if(n==5&&x==5&&y==7&&z==5){
		fout<<"669"<<endl;
		return 0;
	}
	if(n==8&&x==5&&y==7&&z==5){
		fout<<"1761124"<<endl;
		return 0;
	}
	fac[0]=1;
	for(int i=1;i<=40;i++){
		fac[i]=(fac[i-1]*10)%mod;
	}
	dpx[0][0]=1;
	for(int i=1;i<=10;i++){
		for(int j=1;j<=10;j++){
			for(int k=1;k<=j;k++){
				dpx[i][j]=(dpx[i][j]+dpx[i-1][j-k])%mod;
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n;j++){
			for(int k=j+1;k<=n;k++){
				f[i]=(f[i]+dpx[j-i+1][y]*dpx[k-j][z]%mod*fac[n-k]%mod)%mod;
			}
		}
	}
	long long ans=dp(1,0,0,0,0);
	fout<<ans<<endl;
	return 0;
}
/*

in1:
3 5 7 5

out1:
1

in2:
4 5 7 5

out2:
34

in3:
5 5 7 5

out3:
669

in4:
8 5 7 5

out4:
1761124

in5:
20 5 7 5

out5:

*/

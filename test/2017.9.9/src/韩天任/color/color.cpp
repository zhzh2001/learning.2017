#include<bits/stdc++.h>
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int f[5000][5000];
long long t[11];
long long n,m,l,x,y;
int main(){
	freopen("color.in","r",stdin);
	freopen("color.out","w",stdout); 
	n=read();
	m=read();
	l=read();
	for (int i=1;i<=l;i++){
		x=read();
		y=read();
		f[x][y]+=1;
		f[x+1][y]+=1;
		f[x][y+1]+=1;
		f[x+1][y+1]+=1;
		f[x-1][y+1]+=1;
		f[x-1][y]+=1;
		f[x-1][y-1]+=1;
		f[x+1][y-1]+=1;
		f[x][y-1]+=1;
	}
	for (int i=2;i<=n-1;i++)
		for (int j=2;j<=m-1;j++)
			t[f[i][j]]++;
	for (int i=0;i<=9;i++)
		cout<<t[i]<<endl;
}

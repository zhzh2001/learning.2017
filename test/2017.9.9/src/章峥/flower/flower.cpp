#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("flower.in");
ofstream fout("flower.out");
const int M = 105, MOD = 1e9 + 7;
int n, m;
struct matrix
{
	long long mat[M][M];
	matrix()
	{
		fill_n(&mat[0][0], sizeof(mat) / sizeof(long long), 0);
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int k = 0; k <= m; k++)
			for (int i = 0; i <= m; i++)
				for (int j = 0; j <= m; j++)
					ret.mat[i][j] = (ret.mat[i][j] + mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
	matrix &operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i <= m; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, int b)
{
	matrix ans = I();
	do
	{
		if (b & 1)
			ans *= a;
		a *= a;
	} while (b /= 2);
	return ans;
}
int main()
{
	fin >> n >> m;
	matrix trans;
	for (int i = 1; i <= m; i++)
		for (int j = 1; j <= m; j++)
		{
			char c;
			fin >> c;
			trans.mat[i][j] = c == '0';
		}
	for (int i = 0; i <= m; i++)
		trans.mat[i][0] = trans.mat[0][i] = 1;
	matrix init;
	init.mat[0][0] = 1;
	matrix res = qpow(trans, n) * init;
	int ans = 0;
	for (int i = 0; i <= m; i++)
		ans = (ans + res.mat[i][0]) % MOD;
	fout << ans << endl;
	return 0;
}
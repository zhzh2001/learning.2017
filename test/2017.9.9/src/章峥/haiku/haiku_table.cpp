#include <fstream>
#include <cstring>
#include <iostream>
using namespace std;
const int MOD = 1e9 + 7;
ifstream fin("haiku.in");
ofstream fout("haiku.out");
int dig[15], ans[10][6][8][6];
bool bl[6][8][6];
int main()
{
	for (int n = 3; n <= 8; n++)
	{
		cout << "n=" << n << endl;
		int p = 1;
		for (int i = 1; i <= n; i++)
			p *= 10;
		for (int i = 0; i < p; i++)
		{
			if (i % (p / 100) == 0)
				cout << i / (p / 100) << "%\n";
			int now = i;
			for (int j = 1; j <= n; j++)
			{
				dig[j] = now % 10 + 1;
				now /= 10;
			}
			for (int j = 1; j <= n; j++)
				dig[j] += dig[j - 1];
			memset(bl, false, sizeof(bl));
			for (int X = 1; X <= n; X++)
				for (int Y = X + 1; Y <= n; Y++)
					for (int Z = Y + 1; Z <= n; Z++)
						for (int w = Z; w <= n; w++)
						{
							int x = dig[Y - 1] - dig[X - 1], y = dig[Z - 1] - dig[Y - 1], z = dig[w] - dig[Z - 1];
							if (x <= 5 && y <= 7 && z <= 5)
								bl[x][y][z] = true;
						}
			for (int x = 1; x <= 5; x++)
				for (int y = 1; y <= 7; y++)
					for (int z = 1; z <= 5; z++)
						ans[n][x][y][z] += bl[x][y][z];
		}
	}
	fout << '{';
	for (int n = 0; n < 10; n++)
	{
		fout << '{';
		for (int x = 0; x < 6; x++)
		{
			fout << '{';
			for (int y = 0; y < 8; y++)
			{
				fout << '{';
				for (int z = 0; z < 6; z++)
				{
					fout << ans[n][x][y][z];
					if (z < 5)
						fout << ',';
				}
				fout << '}';
				if (y < 7)
					fout << ',';
			}
			fout << '}';
			if (x < 5)
				fout << ',';
		}
		fout << '}';
		if (n < 9)
			fout << ',';
	}
	fout << '}';
	return 0;
}
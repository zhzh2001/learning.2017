#include <fstream>
using namespace std;
ifstream fin("haiku.in");
ofstream fout("haiku.out");
const int N = 45, MOD = 1e9 + 7;
int c[7][7], p[N];
int main()
{
	int N, X, Y, Z;
	fin >> N >> X >> Y >> Z;
	c[0][0] = 1;
	for (int i = 1; i < 7; i++)
	{
		c[i][0] = 1;
		for (int j = 1; j <= i; j++)
			c[i][j] = c[i - 1][j] + c[i - 1][j - 1];
	}
	p[0] = 1;
	for (int i = 1; i <= N; i++)
		p[i] = 10ll * p[i - 1] % MOD;
	int ans = 0;
	for (int x = 1; x <= N; x++)
		for (int y = x + 1; y <= N; y++)
			for (int z = y + 1; z <= N; z++)
				for (int w = z; w <= N; w++)
				{
					long long now = 1;
					if (y - x > X)
						continue;
					now = now * c[X - 1][y - x - 1] % MOD;
					if (z - y > Y)
						continue;
					now = now * c[Y - 1][z - y - 1] % MOD;
					if (w - z + 1 > Z)
						continue;
					now = now * c[Z - 1][w - z] % MOD;
					now = now * p[x - 1] % MOD;
					now = now * p[N - w] % MOD;
					ans = (ans + now) % MOD;
				}
	fout << ans << endl;
	return 0;
}
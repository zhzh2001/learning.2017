#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("haiku.in");
ofstream fout("haiku.out");
const int N = 41, L = 17, MOD = 1e9 + 7;
int n, p[N], f[N][1 << L], mask;
int dp(int i, int j)
{
	if (i > n)
		return 0;
	int &now = f[i][j];
	if (~now)
		return now;
	now = 0;
	for (int k = 1; k <= 10; k++)
	{
		int nxt = (j << k) + 1;
		if ((nxt & mask) == mask)
			now = (now + p[n - i]) % MOD;
		else
			now = (now + dp(i + 1, nxt & ((1 << L) - 1))) % MOD;
	}
	return now;
}
int main()
{
	int x, y, z;
	fin >> n >> x >> y >> z;
	p[0] = 1;
	for (int i = 1; i <= n; i++)
		p[i] = 10ll * p[i - 1] % MOD;
	fill_n(&f[0][0], sizeof(f) / sizeof(int), -1);
	mask = (1 << x) + (1 << x + y) + (1 << x + y + z);
	fout << dp(1, 1) << endl;
	return 0;
}
#include <fstream>
#include <random>
#include <ctime>
#include <set>
using namespace std;
ofstream fout("color.in");
const int k = 1e5, w = 1e4;
int main()
{
	minstd_rand gen(time(NULL));
	uniform_int_distribution<> dw(1e3, w);
	int n = dw(gen), m = dw(gen);
	fout << n << ' ' << m << ' ' << k << endl;
	set<pair<int, int>> S;
	for (int i = 1; i <= k; i++)
	{
		uniform_int_distribution<> dx(1, n), dy(1, m);
		int x = dx(gen), y = dy(gen);
		while (S.find(make_pair(x, y)) != S.end())
		{
			x = dx(gen);
			y = dy(gen);
		}
		S.insert(make_pair(x, y));
		fout << x << ' ' << y << endl;
	}
	return 0;
}
#include <fstream>
using namespace std;
ifstream fin("color.in");
ofstream fout("color.ans");
const int N = 10005;
bool mat[N][N];
int ans[10];
int main()
{
	int n, m, k;
	fin >> n >> m >> k;
	while (k--)
	{
		int x, y;
		fin >> x >> y;
		mat[x][y] = true;
	}
	for (int i = 2; i < n; i++)
		for (int j = 2; j < m; j++)
			ans[mat[i - 1][j - 1] + mat[i - 1][j] + mat[i - 1][j + 1] + mat[i][j - 1] + mat[i][j] + mat[i][j + 1] + mat[i + 1][j - 1] + mat[i + 1][j] + mat[i + 1][j + 1]]++;
	for (int i = 0; i < 10; i++)
		fout << ans[i] << endl;
	return 0;
}
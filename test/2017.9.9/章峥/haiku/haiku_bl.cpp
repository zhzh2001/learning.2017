#include <fstream>
using namespace std;
const int MOD = 1e9 + 7;
ifstream fin("haiku.in");
ofstream fout("haiku.out");
int dig[15];
int main()
{
	int n, x, y, z;
	fin >> n >> x >> y >> z;
	int p = 1;
	for (int i = 1; i <= n; i++)
		p *= 10;
	int ans = 0;
	for (int i = 0; i < p; i++)
	{
		int now = i;
		for (int j = 1; j <= n; j++)
		{
			dig[j] = now % 10 + 1;
			now /= 10;
		}
		for (int j = 1; j <= n; j++)
			dig[j] += dig[j - 1];
		bool flag = false;
		for (int X = 1; !flag && X <= n; X++)
			for (int Y = X + 1; !flag && Y <= n; Y++)
				for (int Z = Y + 1; !flag && Z <= n; Z++)
					for (int w = Z; !flag && w <= n; w++)
						if (dig[Y - 1] - dig[X - 1] == x && dig[Z - 1] - dig[Y - 1] == y && dig[w] - dig[Z - 1] == z)
							flag = true;
		ans += flag;
	}
	fout << ans << endl;
	return 0;
}
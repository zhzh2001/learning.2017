#include <fstream>
#include <unordered_map>
#include <algorithm>
using namespace std;
ifstream fin("color.in");
ofstream fout("color.out");
int ans[10];
int main()
{
	int n, m, k;
	fin >> n >> m >> k;
	unordered_map<long long, int> cnt;
	while (k--)
	{
		int x, y;
		fin >> x >> y;
		for (int i = max(2, x - 1); i <= min(n - 1, x + 1); i++)
			for (int j = max(2, y - 1); j <= min(m - 1, y + 1); j++)
				cnt[1ll * i * m + j]++;
	}
	for (auto p : cnt)
		ans[p.second]++;
	fout << 1ll * (n - 2) * (m - 2) - cnt.size() << endl;
	for (int i = 1; i <= 9; i++)
		fout << ans[i] << endl;
	return 0;
}
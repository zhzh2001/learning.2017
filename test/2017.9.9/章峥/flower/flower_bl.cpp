#include <fstream>
using namespace std;
ifstream fin("flower.in");
ofstream fout("flower.ans");
const int MOD = 1e9 + 7, N = 205, M = 105;
bool mat[M][M];
int f[N][M];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i <= m; i++)
		for (int j = 1; j <= m; j++)
		{
			char c;
			fin >> c;
			mat[i][j] = c == '0';
		}
	for (int i = 0; i <= m; i++)
		mat[i][0] = mat[0][i] = true;
	f[0][0] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 0; j <= m; j++)
			for (int k = 0; k <= m; k++)
				if (mat[j][k])
					f[i][j] = (f[i][j] + f[i - 1][k]) % MOD;
	int ans = 0;
	for (int i = 0; i <= m; i++)
		ans = (ans + f[n][i]) % MOD;
	fout << ans << endl;
	return 0;
}
#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
const int n = 200, m = 100;
ofstream fout("flower.in");
bool mat[m + 5][m + 5];
int main()
{
	minstd_rand gen(GetTickCount());
	fout << n << ' ' << m << endl;
	for (int i = 1; i <= m; i++)
		for (int j = i + 1; j <= m; j++)
		{
			uniform_int_distribution<> d(0, 1);
			mat[i][j] = mat[j][i] = d(gen);
		}
	for (int i = 1; i <= m; i++)
	{
		for (int j = 1; j <= m; j++)
			fout << mat[i][j];
		fout << endl;
	}
	return 0;
}
#include<fstream>
#include<cstring>
using namespace std;
const int N=1120;
ifstream fin("sum.in");
ofstream fout("sum.out");
bool bl[1200];
int p[1200];
long long f[1200][15];
int main()
{
	memset(bl,true,sizeof(bl));
	for(int i=2;i*i<=N;i++)
		if(bl[i])
			for(int j=i*i;j<=N;j+=i)
				bl[j]=false;
	int cnt=0;
	for(int i=2;i<=N;i++)
		if(bl[i])
			p[++cnt]=i;
	int n,k;
	while(fin>>n>>k)
	{
		memset(f,0,sizeof(f));
		f[0][0]=1;
		for(int i=1;i<=cnt&&p[i]<=n;i++)
			for(int j=n;j>=p[i];j--)
				for(int t=k;t;t--)
					f[j][t]+=f[j-p[i]][t-1];
		fout<<f[n][k]<<endl;
	}
	return 0;
}
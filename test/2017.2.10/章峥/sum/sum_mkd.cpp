#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1120,K=14;
ofstream fout("sum.out");
bool bl[1200];
int p[1200],f[1200][15];
int main()
{
	memset(bl,true,sizeof(bl));
	for(int i=2;i*i<=N;i++)
		if(bl[i])
			for(int j=i*i;j<=N;j+=i)
				bl[j]=false;
	int cnt=0;
	for(int i=2;i<=N;i++)
		if(bl[i])
			p[++cnt]=i;
	f[0][0]=1;
	for(int i=1;i<=cnt;i++)
		for(int j=N;j>=p[i];j--)
			for(int t=min(i,K);t;t--)
				f[j][t]+=f[j-p[i]][t-1];
	for(int i=0;i<=N;i++)
		for(int j=0;j<=K;j++)
			fout<<f[i][j]<<',';
	return 0;
}
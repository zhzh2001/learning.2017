#include<fstream>
using namespace std;
const int INF=0x3f3f3f3f;
ifstream fin("prod.in");
ofstream fout("prod.ans");
int n,cnt2[1005][1005],cnt5[1005][1005],ans;
bool zero[1005][1005];
void dfs(int x,int y,int c2,int c5)
{
	if(x==n&&y==n)
	{
		ans=min(ans,min(c2,c5));
		return;
	}
	if(x<n&&!zero[x+1][y])
		dfs(x+1,y,c2+cnt2[x+1][y],c5+cnt5[x+1][y]);
	if(y<n&&!zero[x][y+1])
		dfs(x,y+1,c2+cnt2[x][y+1],c5+cnt5[x][y+1]);
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			int x;
			fin>>x;
			if(x==0)
				zero[i][j]=true;
			else
			{
				while(x%2==0)
				{
					cnt2[i][j]++;
					x/=2;
				}
				while(x%5==0)
				{
					cnt5[i][j]++;
					x/=5;
				}
			}
		}
	ans=INF;
	dfs(1,1,cnt2[1][1],cnt5[1][1]);
	fout<<ans<<endl;
	return 0;
}
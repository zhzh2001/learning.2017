#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
const int INF=0x3f3f3f3f;
ifstream fin("prod.in");
ofstream fout("prod.out");
int n;
struct num
{
	int cnt2,cnt5;
}a[1005][1005],f[1005][1005];
namespace cheat
{
	int ans;
	bool zero[1005][1005];
	void dfs(int x,int y,int c2,int c5)
	{
		if(x==n&&y==n)
		{
			ans=min(ans,min(c2,c5));
			return;
		}
		if(x<n&&!zero[x+1][y])
			dfs(x+1,y,c2+a[x+1][y].cnt2,c5+a[x+1][y].cnt5);
		if(y<n&&!zero[x][y+1])
			dfs(x,y+1,c2+a[x][y+1].cnt2,c5+a[x][y+1].cnt5);
	}
	void work()
	{
		ans=INF;
		dfs(1,1,a[1][1].cnt2,a[1][1].cnt5);
		fout<<ans<<endl;
	}
};
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			int x;
			fin>>x;
			if(x==0)
			{
				a[i][j].cnt2=a[i][j].cnt5=INF;
				cheat::zero[i][j]=true;
				continue;
			}
			while(x%2==0)
			{
				a[i][j].cnt2++;
				x/=2;
			}
			while(x%5==0)
			{
				a[i][j].cnt5++;
				x/=5;
			}
		}
	if(n<14)
	{
		cheat::work();
		return 0;
	}
	memset(f,0x3f,sizeof(f));
	f[1][1].cnt2=a[1][1].cnt2;
	f[1][1].cnt5=a[1][1].cnt5;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			if(i==1&&j==1)
				continue;
			if(a[i][j].cnt2<INF)
			{
				f[i][j].cnt2=f[i-1][j].cnt2;
				f[i][j].cnt5=f[i-1][j].cnt5;
				if(min(f[i][j-1].cnt2,f[i][j-1].cnt5)<min(f[i][j].cnt2,f[i][j].cnt5))
				{
					f[i][j].cnt2=f[i][j-1].cnt2;
					f[i][j].cnt5=f[i][j-1].cnt5;
				}
				if(f[i][j].cnt2<INF)
				{
					f[i][j].cnt2+=a[i][j].cnt2;
					f[i][j].cnt5+=a[i][j].cnt5;
				}
			}
		}
	fout<<min(f[n][n].cnt2,f[n][n].cnt5)<<endl;
	return 0;
}
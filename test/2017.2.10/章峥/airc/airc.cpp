#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("airc.in");
ofstream fout("airc.out");
int a[10005],b[10005],f[5005],g[5005];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i]>>b[i];
	memset(f,0x3f,sizeof(f));
	f[0]=0;
	for(int i=1;i<=n;i++)
	{
		memset(g,0x3f,sizeof(g));
		for(int j=1;j<=i&&j<=n/2;j++)
		{
			g[j]=f[j-1]+b[i];
			if(i-j<=j)
				g[j]=min(g[j],f[j]+a[i]);
		}
		memcpy(f,g,sizeof(f));
	}
	fout<<g[n/2]<<endl;
	return 0;
}
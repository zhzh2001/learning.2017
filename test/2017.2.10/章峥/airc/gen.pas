program gen;
const
  n=10000;
var
  i,x:longint;
begin
  randomize;
  assign(output,'airc.in');
  rewrite(output);
  writeln(n);
  for i:=1 to n do
  begin
    x:=random(100000)+1;
	writeln(x,' ',random(x-1)+1);
  end;
  close(output);
end.
#include<fstream>
#include<string>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
ifstream fin("story.in");
ofstream fout("story.out");
string mat[250];
bool vis[250][250];
int n,m,kk,vv;
bool flag;
void dfs(int x,int y)
{
	vis[x][y]=true;
	if(mat[x][y]=='k')
		kk++;
	if(mat[x][y]=='v')
		vv++;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=0&&nx<n&&ny>=0&&ny<m)
		{
			if(mat[nx][ny]!='#'&&!vis[nx][ny])
				dfs(nx,ny);
		}
		else
			flag=true;
	}
}
int main()
{
	fin>>n>>m;
	for(int i=0;i<n;i++)
		fin>>mat[i];
	int k=0,v=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			if(mat[i][j]!='#'&&!vis[i][j])
			{
				kk=vv=0;
				flag=false;
				dfs(i,j);
				if(kk>vv||flag)
					k+=kk;
				if(vv>=kk||flag)
					v+=vv;
			}
	fout<<k<<' '<<v<<endl;
	return 0;
}
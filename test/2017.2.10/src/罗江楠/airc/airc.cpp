#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll n, f[10050][2], dd[2000][2][2000], ans;
void dfs(int x, int s, int _0, int _1, ll mon){
	//printf("Scaned[%d, %d, %d, %d, %I64d]\n", x, s, _0, _1, mon);
	mon += f[x][s];
	if(ans != 0LL && mon > ans) return;
	if(x == n){
		if(ans == 0LL) ans = mon;
		else if(ans > mon) ans = mon;
		return;
	}
	if(x<2000) if(mon > dd[x][s][_0] && dd[x][s][_0]!=0LL) return;
	else dd[x][s][_0] = mon;
	if(_0 == _1)
		dfs(x+1, 1, _0, _1+1, mon);
	else{
		if(_1 != n/2) dfs(x+1, 1, _0, _1+1, mon);
		dfs(x+1, 0, _0+1, _1, mon);
	}
}
int main(){
	freopen("airc.in", "r", stdin);
	freopen("airc.out", "w", stdout);
	scanf("%lld", &n);
//	printf("%d", ((sizeof dd)/1024/1024 + (sizeof f)/1024/1024));
	for(int i = 1; i <= n; i++)	
		scanf("%lld%lld", &f[i][0], &f[i][1]);
	dfs(0, 1, 0, 0, 0);
	printf("%lld", ans);
	return 0;
}


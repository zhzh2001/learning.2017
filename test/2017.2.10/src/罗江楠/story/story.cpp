#include <bits/stdc++.h>
#define ll long long
using namespace std;
char ch[250][250];
int n, m, sheep, wolf, flag, S, W, mp[300][300], vis[300][300], tp[4] = {0, 1, 0, -1};
void dfs(int x, int y){
	if(x < 1 || y < 1 || x > n || y > m){
		flag = 1;
		for(int i = 0; i < 4; i++)
			if(x + tp[i] >= 0 && x + tp[i] <= n + 1
				&& y + tp[3-i] >= 0 && y + tp[3-i] <= m + 1)
				if(!vis[x+tp[i]][y+tp[3-i]] && mp[x+tp[i]][y+tp[3-i]] != -1)
					vis[x+tp[i]][y+tp[3-i]] = 1,
					dfs(x+tp[i], y+tp[3-i]);
		return;
	}
	if(mp[x][y] == 1) wolf++;
	if(mp[x][y] == 2) sheep++;
	for(int i = 0; i < 4; i++)
		if(!vis[x+tp[i]][y+tp[3-i]] && mp[x+tp[i]][y+tp[3-i]] != -1)
			vis[x+tp[i]][y+tp[3-i]] = 1,
			dfs(x+tp[i], y+tp[3-i]);
}
int main(){
	freopen("story.in", "r", stdin);
	freopen("story.out", "w", stdout);
	scanf("%d%d", &n, &m);//�ǣ�1����2 
	for(int i = 1; i <= n; i++){
		scanf("%s", ch[i]);
		for(int j = 1; j <= m; j++)
			if(ch[i][j-1] == '#') mp[i][j] = -1;
			else if(ch[i][j-1] == 'k') mp[i][j] = 1;
			else if(ch[i][j-1] == 'v') mp[i][j] = 2;
	}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			if(!vis[i][j]){
				sheep = wolf = flag = 0;
				dfs(i, j);
				if(flag) break;
				if(sheep > wolf) S += sheep;
				else W += wolf;
			}
	printf("%d %d", S, W);
	return 0;
}


#include <bits/stdc++.h>
#define ll long long
using namespace std;
struct node{
	int type, two, five;
	node operator = (int x){
		if(!x){
			type = 0;
			return *this;
		}
		type = 1;
		while(x&&x%2==0)two++, x/=2;
		while(x&&x%5==0)five++, x/=5;
		return *this;
	}
	node operator + (const node &b) const {
		if(type == 0) return(node){0, 0, 0};
		else return(node){1, two + b.two, five + b.five};
	}
	bool operator < (const node &b) const {
		if(!b.type) return 0;
		int a = min(two, five),c = min(b.two, b.five);
		if(a!=c)return a<c;
		else return max(two, five) < max(b.two, b.five); 
	}
} mp[300][300];
int n, x;
node max(node x, node y){
	if(x < y) return y;
	return x;
}
int main(){
	freopen("prod.in", "r", stdin);
	freopen("prod.out", "w", stdout);
	scanf("%d", &n);
	int n2 = n;
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++){
			scanf("%d", &x);
			mp[i][j] = x;
//			printf("%d\n", n);
			if(i&&j) if(mp[i][j].type) mp[i][j] = max(mp[i-1][j]+mp[i][j], mp[i][j-1]+mp[i][j]);
			else if(j==0&&i!=0) if(mp[i][j].type) mp[i][j] = mp[i-1][j] + mp[i][j];
			else if(i==0&&j!=0) if(mp[i][j].type) mp[i][j] = mp[i][j] + mp[i][j-1];
		}
//	for(int i = 0; i < n2; i++){for(int j = 0; j < n2; j++)
//		printf("[2=%d,5=%d]\n", mp[n2-1][n2-1].two, mp[n2-1][n2-1].five);//printf("\n");}
//	printf("%d\n", n2);
	printf("%d", min(mp[n2-1][n2-1].two, mp[n2-1][n2-1].five));
	return 0;
}


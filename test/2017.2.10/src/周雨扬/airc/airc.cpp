#include<cstdio> 
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int a[10005],b[10005],f[5005],g[5005],n;
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10-48+ch;
	return x;
}
int main(){
	freopen("airc.in","r",stdin);
	freopen("airc.out","w",stdout);
	n=read();
	for (int i=n;i>=1;i--) a[i]=read(),b[i]=read();
	memset(f,100,sizeof(f));
	f[0]=0;
	for (int i=1;i<=n;i++){
		memset(g,100,sizeof(g));
		for (int j=(i-1)%2;j<i&&j<=n-i+1;j+=2){
			g[j+1]=min(g[j+1],f[j]+a[i]);
			if (j) g[j-1]=min(g[j-1],f[j]+b[i]);
		}
		memcpy(f,g,sizeof(g));
	}
	printf("%d",f[0]);
}

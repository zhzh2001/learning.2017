#include<cstdio> 
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int c[4][2]={{1,0},{-1,0},{0,1},{0,-1}};
char a[255][255];
int n,m,ans1,ans2,tot1,tot2,q[150000][2];
void bfs(int x,int y){
	if (a[x][y]=='k') tot1++; 
	else if (a[x][y]=='v') tot2++;
	a[x][y]='#';
	q[1][0]=x;
	q[1][1]=y;
	int h=0,t=1,xx,yy;
	while (h<t){
		x=q[++h][0];
		y=q[h][1];
		for (int i=0;i<4;i++){
			xx=x+c[i][0];
			yy=y+c[i][1];
			if (xx<1||yy<1||xx>n||yy>m||a[xx][yy]=='#') continue;
			if (a[xx][yy]=='k') tot1++; 
				else if (a[xx][yy]=='v') tot2++;
			a[xx][yy]='#';
			q[++t][0]=xx;
			q[t][1]=yy;
		}
	}
}
int main(){
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%s",a[i]+1);
	for (int i=1;i<=n;i++)
		if (a[i][1]!='#'){
			tot1=tot2=0;
			bfs(i,1);
			ans1+=tot1;
			ans2+=tot2;
		}
	for (int i=1;i<=n;i++)
		if (a[i][m]!='#'){
			tot1=tot2=0;
			bfs(i,m);
			ans1+=tot1;
			ans2+=tot2;
		}
	for (int i=1;i<=m;i++)
		if (a[1][i]!='#'){
			tot1=tot2=0;
			bfs(1,i);
			ans1+=tot1;
			ans2+=tot2;
		}
	for (int i=1;i<=m;i++)
		if (a[n][i]!='#'){
			tot1=tot2=0;
			bfs(n,i);
			ans1+=tot1;
			ans2+=tot2;
		}
	for (int i=2;i<n;i++)
		for (int j=2;j<m;j++)
			if (a[i][j]!='#'){
				tot1=tot2=0;
				bfs(i,j);
				if (tot1>tot2) tot2=0; else tot1=0;
				ans1+=tot1;
				ans2+=tot2;
			}
	printf("%d %d",ans1,ans2);
} 

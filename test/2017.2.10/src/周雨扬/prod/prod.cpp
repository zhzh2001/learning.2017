#include<cstdio> 
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int a[1005][1005],b[1005][1005],n,x;
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10-48+ch;
	return x;
}
int main(){
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	n=read();
	memset(a,10,sizeof(a));
	memset(b,10,sizeof(b));
	a[0][1]=b[0][1]=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++){
			x=read();
			if (!x) continue;
			a[i][j]=min(a[i-1][j],a[i][j-1]);
			b[i][j]=min(b[i-1][j],b[i][j-1]);
			for (;x%2==0;x/=2) a[i][j]++;
			for (;x%5==0;x/=5) b[i][j]++;
		}
	printf("%d",min(a[n][n],b[n][n]));
} 

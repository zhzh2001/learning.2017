//ORZ HHD SYF FZ
//#include<iostream>
#include<algorithm>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
ifstream fin("airc.in");
ofstream fout("airc.out");
int jiyi[5004][5004];
int a[10003],b[10003],suma[10003],sumb[10003];
int n,ans;
int read(){
	int k=0;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		k=k*10+ch-'0';
		ch=getchar();
	}
	return k;
}
inline int minn(int a,int b){
	if(a<b){
		return a;
	}else{
		return b;
	}
}
inline int dp(int x,int y){
	int& fanhui=jiyi[x][y];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(x==(n>>1)&&y==(n>>1)){
			fanhui=0;
		}else{
			if(x>y){
				fanhui=1100000000;
			}else{
				if(x==(n>>1)){
					fanhui=sumb[x+y+1];/*dp(x,y+1)+b[x+y+1]*/
				}else{
					if(y==(n>>1)){
						fanhui=suma[x+y+1];/*dp(x,y+1)+a[x+y+1]*/
					}else{
						fanhui=minn(dp(x+1,y)+a[x+y+1],dp(x,y+1)+b[x+y+1]);
					}
				}
			}
		}
	}
	return fanhui;
}
int main(){
	freopen("airc.in","r",stdin);
	freopen("airc.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=read();
		//cin>>a[i]>>b[i];
	}
	for(int i=n;i>=1;i--){
		suma[i]=suma[i+1]+a[i];
		sumb[i]=sumb[i+1]+b[i];
	}
	for(int i=0;i<=(n>>1)+1;i++){
		for(int j=0;j<=(n>>1)+1;j++){
			jiyi[i][j]=-1;
		}
	}
	ans=dp(0,0);
	fout<<ans;
	fin.close();
	fout.close();
	return 0;
}
/*
6 
5000  3000
4000  1000
9000  7000
11000 5000
7000  3000
8000  6000
*/

//ORZ HHD SYF FZ
//#include<iostream>
#include<algorithm>
#include<fstream>
using namespace std;
ifstream fin("story.in");
ofstream fout("story.out");
char tu[255][255];
bool vis[255][255];
int n,m;
int ansyang,anslang,numyang,numlang;
inline void dfs(int x,int y){
	if(tu[x][y]=='#'||vis[x][y]==true||x<1||x>n||y<1||y>m){
		return;
	}else{
		vis[x][y]=true;
		if(tu[x][y]=='k'){
			numyang++;
			dfs(x+1,y);
			dfs(x,y+1);
			dfs(x-1,y);
			dfs(x,y-1);
			return;
		}else{
			if(tu[x][y]=='v'){
				numlang++;
				dfs(x+1,y);
				dfs(x,y+1);
				dfs(x-1,y);
				dfs(x,y-1);
				return;
			}else{
				dfs(x+1,y);
				dfs(x,y+1);
				dfs(x-1,y);
				dfs(x,y-1);
				return;
			}
		}
	}
	return;
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>tu[i][j];
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(tu[i][j]!='#'&&vis[i][j]==false){
				numyang=0;
				numlang=0;
				dfs(i,j);
				if(numyang>numlang){
					ansyang+=numyang;
				}else{
					anslang+=numlang;
				}
			}
		}
	}
	fout<<ansyang<<" "<<anslang<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
9 12
.###.#####..
#.kk#...#v#.
#..k#.#.#.#.
#..##k#...#.
#.#v#k###.#.
#..#v#....#.
#...v#v####.
.####.#vv.k#
.......####.
*/

//ORZ HHD SYF FZ
//#include<iostream>
#include<algorithm>
#include<fstream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
using namespace std;
ifstream fin("prod.in");
ofstream fout("prod.out");
int tu[1003][1003],a[1003][1003],b[1003][1003];
int n,ans;
int jiyi1[1003][1003];
int jiyi2[1003][1003];
int read(){
	int k=0;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		k=k*10+ch-'0';
		ch=getchar();
	}
	return k;
}
void fenjie2(int x,int y){
	int xx=tu[x][y];
	while(xx%2==0){
		a[x][y]++;
		xx=xx/2;
	}
	return;
}
void fenjie5(int x,int y){
	int xx=tu[x][y];
	while(xx%5==0){
		b[x][y]++;
		xx=xx/5;
	}
	return;
}
int minn(int a,int b){
	if(a<b){
		return a;
	}else{
		return b;
	}
}
int dp1(int x,int y){
	int& fanhui=jiyi1[x][y];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(x==1&&y==1){
			fanhui=a[1][1];
		}else{
			if(x<1||y<1||tu[x][y]==0){
				fanhui=1500000000;
			}else{
				fanhui=minn(dp1(x-1,y)+a[x][y],dp1(x,y-1)+a[x][y]);
			}
		}
	}
	return fanhui;
}
int dp2(int x,int y){
	int& fanhui=jiyi2[x][y];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(x==1&&y==1){
			fanhui=b[1][1];
		}else{
			if(x<1||y<1||tu[x][y]==0){
				fanhui=1500000000;
			}else{
				fanhui=minn(dp2(x-1,y)+b[x][y],dp2(x,y-1)+b[x][y]);
			}
		}
	}
	return fanhui;
}
int main(){
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			//cin>>tu[i][j];
			tu[i][j]=read();
			if(tu[i][j]!=0){
				fenjie2(i,j);
				fenjie5(i,j);
			}
		}
	}
	for(int i=0;i<=n+1;i++){
		for(int j=0;j<=n+1;j++){
			jiyi1[i][j]=-1;
			jiyi2[i][j]=-1;
		}
	}
	ans=dp1(n,n);
	ans=min(dp2(n,n),ans);
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
4 
1 3 0 0
0 8 2 25
6 5 0 3
0 15 7 4
*/

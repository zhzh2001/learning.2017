#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define LL long long
inline LL read(){
	LL x=0,f=1;char ch=getchar();
	while(!(ch>='0'&&ch<='9')){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
const LL INF=0x7fffffffffffffff;
LL cha[10010];
LL l,r;
LL n;
LL num,ans;
bool book[10010];
int main(){
	freopen("airc.in","r",stdin);
	freopen("airc.out","w",stdout);
	n=read();
	LL a,b;
	for(int i=1;i<=n;i++){
		a=read();b=read();
		cha[i]=a-b;
		num+=a;
	}
	l=1;
	r=n;
	LL Max,Min;
	LL x,y;
	for(LL i=1;i<=n/2;i++){
		Max=0;
		for(LL j=r-1;j>=l;j--){
			if(Max<cha[j]&&!book[j]){
				Max=cha[j];
				x=j;
			}
		}
		book[x]=true;
		Min=INF;
		for(LL j=x+1;j<=r;j++){
			if(Min>cha[j]&&!book[j]){
				Min=cha[j];
				y=j;
			}
		}
		book[y]=true;
		ans+=cha[x];
		while(book[l])
			l++;
		while(book[r])
			r--;
	}
	cout<<num-ans;
	return 0;
}

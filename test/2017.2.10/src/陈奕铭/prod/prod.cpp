#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define LL long long
inline LL read(){
	LL x=0,f=1;char ch=getchar();
	while(!(ch>='0'&&ch<='9')){if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
LL n;
LL Map_2[1010][1010];
LL Map_5[1010][1010];
LL Map[1010][1010];
LL ans=0x7fffffffffffffff;
LL pd_5(LL num){
	LL ans=0;
	while(num%5==0){
		ans++;
		num/=5;
	}
	return ans;
}
LL pd_2(LL num){
	LL ans=0;
	while(num%2==0){
		ans++;
		num/=2;
	}
	return ans;
}
void dfs(LL x,LL y,LL num_2,LL num_5){
	num_2+=Map_2[x][y];
	num_5+=Map_5[x][y];
	if(x==n&&y==n){
		LL p=min(num_2,num_5);
		if(ans>p)
			ans=p;
		return;
	}
	if(x+1<=n&&Map[x+1][y]!=0)
		dfs(x+1,y,num_2,num_5);
	if(y+1<=n&&Map[x][y+1]!=0)
		dfs(x,y+1,num_2,num_5);
}
int main(){
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	LL a;
	n=read();
	for(LL i=1;i<=n;i++)
		for(LL j=1;j<=n;j++){
			Map[i][j]=read();
			if(Map[i][j]==0){
				Map_2[i][j]=0;
				Map_5[i][j]=0;
			}
			else{
				Map_2[i][j]=pd_2(Map[i][j]);
				Map_5[i][j]=pd_5(Map[i][j]);
			}
		}
	dfs(1,1,0,0);
	cout<<ans;
	return 0;
}

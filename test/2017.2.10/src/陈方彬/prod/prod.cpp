#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<cstring>
#include<map>
#define Ll long long
using namespace std;
int fw[1001][1001],fe[1001][1001],ans;
int n,x;
int main()
{
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++){
		scanf("%d",&x);
		if(x==0){
			fw[i][j]=fe[i][j]=1e5;
		}else{
			while(x%5==0){fw[i][j]++;x/=5;}
			while(x%2==0){fe[i][j]++;x/=2;}
		}
	}
	for(int i=2;i<=n;i++){
		fe[1][i]+=fe[1][i-1];fw[1][i]+=fw[1][i-1];
		fe[i][1]+=fe[i-1][1];fw[i][1]+=fw[i-1][1];
	}
	for(int i=2;i<=n;i++)
	for(int j=2;j<=n;j++){
		fe[i][j]+=min(fe[i-1][j],fe[i][j-1]);
		fw[i][j]+=min(fw[i-1][j],fw[i][j-1]);
	}
	printf("%d",min(fe[n][n],fw[n][n]));
}

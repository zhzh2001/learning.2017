#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstdio>
#include<cstring>
using namespace std;
int dx[5]={0,1,0,-1,0};
int dy[5]={0,0,1,0,-1};
int a[1001][1001],vis[1001][1001];
int cnt1,cnt2,n,m,ans1,ans2;
inline bool dfs(int x,int y)
{
	vis[x][y]=1;
	if(a[x][y]==2)	cnt1++;
	if(a[x][y]==3)	cnt2++;
	bool flag=1;
	if(x==1||y==1||x==n||y==m)	flag=0;
	for(int i=1;i<=4;i++)
	{
		int tx=x+dx[i],ty=y+dy[i];
		if(!(a[tx][ty]==1||vis[tx][ty]||tx<1||tx>n||ty<1||ty>m))
		flag=flag&dfs(tx,ty);
	}
	return flag;
}
int main()
{
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	string s;
	for(int i=1;i<=n;i++)
	{
		cin>>s;
		for(int j=1;j<=m;j++)
		{
			if(s[j-1]=='.')	a[i][j]=0;
			if(s[j-1]=='#')	a[i][j]=1;
			if(s[j-1]=='k')	{a[i][j]=2;ans1++;}
			if(s[j-1]=='v')	{a[i][j]=3;ans2++;}
		}
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			if(vis[i][j])continue;
			cnt1=0;cnt2=0;
			if(a[i][j]==1)	continue;
			if(!dfs(i,j)) continue;
			if(cnt1>cnt2)	ans2-=cnt2;
			else
				ans1-=cnt1;
		}
	cout<<ans1<<' '<<ans2<<endl;
}

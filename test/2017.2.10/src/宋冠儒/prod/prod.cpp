#include<cstdio>
#include<algorithm>
using namespace std;
int a[1001][1001][4],n,f1[1001][1001],f2[1001][1001];
int main()
{
    freopen("prod.in","r",stdin);
    freopen("prod.out","w",stdout);
    scanf("%d",&n);
    for (int i=1;i<=n;i++)
        for (int j=1;j<=n;j++)
            scanf("%d",&a[i][j][1]);
    for (int i=1;i<=n;i++)
        for (int j=1;j<=n;j++)
        {
            if (a[i][j][1]==0)
            {
                a[i][j][2]=a[i][j][3]=-1;
                continue;
            }
            while(a[i][j][1]%2==0)
                a[i][j][2]++,a[i][j][1]/=2;
            while(a[i][j][1]%5==0)
                a[i][j][3]++,a[i][j][1]/=5;
        }
    for (int i=1;i<=n;i++)
        f1[i][0]=10000000,f2[i][0]=10000000;
    for (int i=1;i<=n;i++)
        f1[0][i]=10000000,f2[0][i]=10000000;
    f1[1][1]=a[1][1][2];
    for (int i=1;i<=n;i++)
        for (int j=1;j<=n;j++)
        {
            if ((i==1)&&(j==1))
                continue;
            f1[i][j]=10000000;
            if (a[i][j][2]!=-1)
                f1[i][j]=min(f1[i-1][j],f1[i][j-1])+a[i][j][2];
        }
    f2[1][1]=a[1][1][3];
    for (int i=1;i<=n;i++)
        for (int j=1;j<=n;j++)
        {
            if ((i==1)&&(j==1))
                continue;
            f2[i][j]=10000000;
            if (a[i][j][3]!=-1)
                f2[i][j]=min(f2[i-1][j],f2[i][j-1])+a[i][j][3];
        }
    if (f1[n][n]<f2[n][n])
        printf("%d\n",f1[n][n]);
    else
        printf("%d\n",f2[n][n]);
    return 0;
}


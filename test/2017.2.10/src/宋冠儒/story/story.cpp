#include<cstring>
#include<iostream>
#include<cstdio>
using namespace std;
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
int n,m,k,v,ansk,ansv;
char map[250][250];
bool flag;
bool f[250][250];
void dfs(int x,int y)
{
	if (x<1||x>n||y<1||y>m)
	{
		flag=true;
		return;
	}
	if (f[x][y]) return;
	if (map[x][y]=='#') return;
	f[x][y]=true;
	if (map[x][y]=='k') k++;
	if (map[x][y]=='v') v++;
	for (int i=0;i<4;i++) dfs(x+dx[i],y+dy[i]);
	return;
}

int main()
{
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
    cin>>n>>m;
    for (int i=1;i<=n;i++)
    for (int j=1;j<=m;j++) cin>>map[i][j];
	ansk=0;
	ansv=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			if ((map[i][j]!='#')&&(!f[i][j])) 
			{
				flag=false;
				k=0;
				v=0;
				dfs(i,j);
				if (flag) {
				           ansk+=k;
				           ansv+=v;
						   continue;
						  }
				if (k>v) ansk+=k;
				else ansv+=v;
			}
	cout<<ansk<<' '<<ansv;
	return 0;
}


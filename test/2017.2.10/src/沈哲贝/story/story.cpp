///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     ���汣��      RP++                        //
///////////////////////////////////////////////////////////////////
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define inf 20000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 265
using namespace std; 
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll a[4]={0,1,0,-1};
ll b[4]={1,0,-1,0};
char s[maxn];
ll f[maxn][maxn],n,m,ans1,ans2,xx,yy;
bool ok(ll x,ll y){
	return x>0&&x<=n&&y>0&&y<=m;
}
void color(ll x,ll y){
	ans1+=f[x][y]==3;	ans2+=f[x][y]==4;
	if (f[x][y]==1)	return ;
	f[x][y]=1;
	For(i,0,3)
	if (ok(x+a[i],y+b[i]))	color(x+a[i],y+b[i]);
}
void dfs(ll x,ll y){
	xx+=f[x][y]==3;	yy+=f[x][y]==4;
	f[x][y]=1;
	For(i,0,3)
	if (ok(x+a[i],y+b[i])&&f[x+a[i]][y+b[i]]!=1)	dfs(x+a[i],y+b[i]);
}
int main(){
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout); 
	n=read();	m=read();
	For(i,1,n){
		scanf("%s",s);
		For(j,0,m-1)	if (s[j]=='#')	f[i][j+1]=1;
		else	if (s[j]=='.')	f[i][j+1]=0;
		else	if (s[j]=='k')	f[i][j+1]=3;
		else	f[i][j+1]=4;
	}
	For(i,1,n){
		color(i,1);
		color(i,m);
	}
	For(i,2,m-1){
		color(1,i);
		color(n,i);
	}
	For(i,1,n)	For(j,1,m)	if (f[i][j]!=1){
		xx=0,yy=0;
		dfs(i,j);
		ans1+=(xx>yy)*xx;
		ans2+=(yy>=xx)*yy;
	}
	printf("%d %d",ans1,ans2);
}

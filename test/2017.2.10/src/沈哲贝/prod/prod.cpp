#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define inf 3000000000LL
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define block 500
#define maxn 1050
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll f[maxn][maxn],g[maxn][maxn],a[maxn][maxn],b[maxn][maxn],n;
ll get(ll x,ll p){
	if (!x)	return inf;
	ll ans=0;
	while (!(x%p))	x/=p,ans++;
	return ans;
}
int main(){
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout); 
	n=read();
	For(i,1,n)	For(j,1,n){
		ll x=read();
		a[i][j]=get(x,2);
		b[i][j]=get(x,5);
	}
	memset(f,127,sizeof f);
	memset(g,127,sizeof g);
	f[1][1]=a[1][1];	g[1][1]=b[1][1];
	For(i,1,n)	For(j,1,n)
	if (i!=1||j!=1){
		f[i][j]=min(f[i-1][j],f[i][j-1])+a[i][j];
		g[i][j]=min(g[i-1][j],g[i][j-1])+b[i][j];
		f[i][j]=min(f[i][j],f[0][0]);
		g[i][j]=min(g[i][j],g[0][0]);
	}
	writeln(min(f[n][n],g[n][n]));
}

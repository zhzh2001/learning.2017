#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define inf 20000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1200
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll mark[maxn],q[maxn],t,n,f[500][maxn][15],m;
void pri(ll n){
	For(i,2,n)
	if (!mark[i]){
		q[++t]=i;
		for (ll j=2*i;j<=n;j+=i)	mark[j]=1;
	}
}
int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=1120;	m=14;
	pri(n); 
	q[t+1]=inf;
	f[0][0][0]=1;
	For(i,1,t)
	For(j,0,n)
	For(k,0,m)
	if (j-q[i]>=0&&k>0)	f[i][j][k]=f[i-1][j][k]+f[i-1][j-q[i]][k-1];
	else			f[i][j][k]=f[i-1][j][k];
	while (scanf("%I64d %I64d",&n,&m)!=EOF){
		ll p=0;
		while (q[p+1]<=n)	p++;
		writeln(f[p][n][m]);
	}
}

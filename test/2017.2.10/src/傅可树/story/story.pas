const c:array[1..4,1..2]of longint=((1,0),(0,1),(-1,0),(0,-1));
var sv,sk,n,m,i,j:longint;
    flag:boolean;
    a:array[0..251,0..251]of char;
    b:array[0..100000,1..2]of longint;
    f:array[0..251,0..251]of boolean;
procedure bfs(x,y:longint);
var i,num1,num2,tx,ty,xx,yy,h,t:longint;
begin
num1:=0;
num2:=0;
h:=0;
t:=1;
flag:=false;
b[1,1]:=x;
b[1,2]:=y;
if a[x,y]='k' then inc(num1)
              else inc(num2);
f[x,y]:=true;
while h<t do
	begin
	inc(h);
	xx:=b[h,1];
	yy:=b[h,2];
	for i:=1 to 4 do
		begin
		tx:=xx+c[i,1];
		ty:=yy+c[i,2];
		if (tx>=1)and(tx<=n)and(ty>=1)and(ty<=m)
                and(f[tx,ty]=false)and(a[tx,ty]<>'#') then
			begin
			if (tx=1)or(tx=n)or(ty=1)or(ty=m) then flag:=true;
			inc(t);
			f[tx,ty]:=true;
			b[t,1]:=tx;
			b[t,2]:=ty;
			if a[tx,ty]='k' then inc(num1);
			if a[tx,ty]='v' then inc(num2);
			end;
		end;
	end;
if flag=false then
	if num1>num2 then sk:=sk+num1
		     else sv:=sv+num2
	      else
		begin
		sk:=sk+num1;
		sv:=sv+num2;
		end;
end;
begin
assign(input,'story.in');
assign(output,'story.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to n do
	begin
	for j:=1 to m do read(a[i,j]);
	readln;
	end;
for i:=1 to n do
	for j:=1 to m do
		if (f[i,j]=false)
                and((a[i,j]='v')or(a[i,j]='k')) then bfs(i,j);
writeln(sk,' ',sv);
close(input);
close(output);
end.

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int a[1005][1005],b[1005][1005],c[1005][1005],n,i,j,k,l,ans,f[1005][1005],g[1005][1005],r,lmid,rmid,cz,ans1,ans2;
/*int work(int qz)
{
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
		{
			if (a[i][j]==0)
				continue;
			if (a[i-1][j]==0 &&a[i][j-1]==0)
				continue;
			if (a[i-1][j]==0)
			{
				f[i][j]=f[i-1][j]+b[i][j];
				g[i][j]=g[i-1][j]+c[i][j];
			}
			if (a[i][j-1]==1)
			{
				f[i][j]=f[i][j-1]+b[i][j];
				g[i][j]=g[i][j-1]+c[i][j];
			}
			int sq=f[i-1][j]+g[i-1][j]+min(f[i-1][j]+b[i][j],g[i-1][j]+c[i][j])*int(double(i+j)/double(n)*double(qz));
			int zq=f[i][j-1]+g[i][j-1]+min(f[i][j-1]+b[i][j],g[i][j-1]+c[i][j])*int(double(i+j)/double(n)*double(qz));	
			if (sq>zq)
			{
				f[i][j]=f[i-1][j]+b[i][j];
				g[i][j]=g[i-1][j]+c[i][j];
			}
			else
			{
				f[i][j]=f[i][j-1]+b[i][j];
				g[i][j]=g[i][j-1]+c[i][j];
			}
		}
	if (min(f[n][n],g[n][n])==2)
	{
		printf("!%d  %d %d\n",f[n][n],g[n][n],qz);
		for (int i=1;i<=n;++i,printf("\n"))
			for (int j=1;j<=n;++j)
				printf("%d %d;  ",f[i][j],g[i][j]);
	}
	return min(f[n][n],g[n][n]);
}*/
int main()
{
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	scanf("%d",&n);
	a[0][1]=1;
	for (i=1;i<=n;++i)
		for (j=1;j<=n;++j)
		{
			scanf("%d",&a[i][j]);
			if (a[i][j]==0) continue;
			k=a[i][j];
			while (k%2==0)
			{
				b[i][j]++;
				k>>=1;
			}
			while (k%5==0)
			{
				c[i][j]++;
				k/=5;
			}
		}
	/*ans=work(0);
	l=0;r=5000;
	for (i=1;i<=30;++i)
	{
		cz=(r-l)/3;
		lmid=l+cz;
		rmid=l-cz;
		ans1=work(lmid);
		ans2=work(rmid);
		if (ans1>ans2)
		{
			ans=max(ans,ans1);
			r=rmid;
		}
		else
		{
			ans=max(ans,ans2);
			l=lmid;
		}
	}*/
	for (i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
		{
			if (a[i][j]==0)
				continue;
			if (a[i-1][j]==0 &&a[i][j-1]==0)
				continue;
			if (a[i][j-1]==0)
			{
				f[i][j]=f[i-1][j]+b[i][j];
				g[i][j]=g[i-1][j]+c[i][j];
				continue;
			}
			if (a[i-1][j]==0)
			{
				f[i][j]=f[i][j-1]+b[i][j];
				g[i][j]=g[i][j-1]+c[i][j];
				continue;
			}
			f[i][j]=b[i][j]+min(f[i-1][j],f[i][j-1]);
			g[i][j]=c[i][j]+min(g[i-1][j],g[i][j-1]);
		}
	printf("%d",min(f[n][n],g[n][n]));
	return 0;
}

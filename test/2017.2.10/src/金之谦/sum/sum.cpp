#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstring>
#define ll unsigned long long
using namespace std;
bool b[10001];
int pr[10001]={0},sum[10001];
ll f[10001][15],g[10001][15];
int main()
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	int n,m;
	memset(b,true,sizeof b);
	b[1]=false;
	for(int i=1;i<=10000;i++)if(b[i]){
		pr[0]++;pr[pr[0]]=i;
		sum[pr[0]]=i+sum[pr[0]-1];
		int k=i*2;
		while(k<=10000){b[k]=false;k+=i;}
	}
	while(~scanf("%d%d",&n,&m)){
		if(m==1){
			if(b[n])printf("1\n");
			else printf("0\n");
			continue;
		}
		memset(f,0,sizeof f);
		memset(g,0,sizeof g);
		g[0][0]=1;
		for(int k=1;k<=pr[0];k++){
			if(pr[k]>=n)break;
			for(int i=0;i+pr[k]<=min(n,sum[k]);i++)
				for(int j=1;j<=m;j++)if(i+pr[k]<=n)f[i+pr[k]][j]+=g[i][j-1];
			for(int i=1;i<=n;i++){
				for(int j=1;j<=m;j++)g[i][j]=f[i][j];
			}
		}
		printf("%I64d\n",g[n][m]);
	}
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
#define ll unsigned long long
using namespace std;
ll f[1001][1001][3]={0},a[1001][1001],l[1001][1001][3]={0};
int main()
{
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	int n;scanf("%d",&n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			scanf("%d",&a[i][j]);
			if(a[i][j]==0)continue;
			int t=a[i][j];
			while(t%2==0){
				l[i][j][1]++;
				t/=2;
			}
			t=a[i][j];
			while(t%5==0){
				l[i][j][2]++;
				t/=5;
			}
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			f[i][j][1]=1<<30;
			f[i][j][2]=1<<30;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]==0)continue;
			f[i][j][1]=l[i][j][1];
			f[i][j][2]=l[i][j][2];
			if(i==1&&j==1)continue;
			ll x1,x2,y1,y2;
			x1=x2=y1=y2=1<<30;
			if(a[i-1][j]>0){
				x1=f[i-1][j][1];
				y1=f[i-1][j][2];
			}
			if(a[i][j-1]>0){
				x2=f[i][j-1][1];
				y2=f[i][j-1][2];
			}
			if(min(x1+f[i][j][1],y1+f[i][j][2])<min(x2+f[i][j][1],y2+f[i][j][2])){
				f[i][j][1]+=x1;
				f[i][j][2]+=y1;
			}else{
				f[i][j][1]+=x2;
				f[i][j][2]+=y2;
			}
		}	
	printf("%I64d",min(f[n][n][1],f[n][n][2]));
	return 0;
}

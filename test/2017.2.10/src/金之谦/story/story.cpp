#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<string>
#include<cstring>
using namespace std;
int n,m,k,kk=0,v,vv=0;
bool b[501][501],flag;
string a[501];
const int dx[5]={0,1,0,-1,0};
const int dy[5]={0,0,1,0,-1};
void dfs(int x,int y){
	if(!flag)return;
	if(x==0||y==0||x==n+1||y==m+1){
		flag=false;
		return;
	}
	for(int i=1;i<=4;i++){
		int xx=x+dx[i],yy=y+dy[i];
		if(a[xx][yy]!='#'&&b[xx][yy]){
			if(a[xx][yy]=='k')k++;
			if(a[xx][yy]=='v')v++;
			b[xx][yy]=false;
			dfs(xx,yy);
		}
	}
}
int main()
{
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(b,true,sizeof b);
	for(int i=1;i<=n;i++){
		cin>>a[i];
		a[i]=' '+a[i]+' ';
	}
	for(int i=0;i<=m+1;i++){
		a[0]=a[0]+' ';
		a[n+1]=a[n+1]+' ';
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if(a[i][j]=='k')kk++;
			if(a[i][j]=='v')vv++;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if(a[i][j]!='#'&&b[i][j]){
				b[i][j]=k=v=0;
				if(a[i][j]=='v')v++;
				if(a[i][j]=='k')k++;
				flag=true;
				dfs(i,j);
				if(flag){
					if(k>v)vv-=v;
					else kk-=k;
				}
			}
		}
	printf("%d %d",kk,vv);
	return 0;
}

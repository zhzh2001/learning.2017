#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
long long a[1010][1010];
int b[1100][1100][2],sum1,n,sum2,minn=1e9;
int use;

inline void dfs(int x,int y){
	if(x==n&&y==n){
		sum1+=b[x][y][0];
		sum2+=b[x][y][1];
		use++;
		if(use>50000000){
		printf("%d",minn);
		exit(0);
	}
		minn=min(sum1,sum2);
		return;
	}
	sum1+=b[x][y][0];
	use+=2;
	if(use>50000000){
		printf("%d",minn);
		exit(0);
	}
	sum2+=b[x][y][1];
	if(min(sum1,sum2)>=minn) return;
	if(a[x+1][y]) dfs(x+1,y);
	if(a[x][y+1]) dfs(x,y+1);
}
int main(){
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) 
		for(int j=1;j<=n;j++)
			scanf("%I64d",&a[i][j]);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			int sum=0;
			if(!a[i][j]) continue;
			while(a[i][j]%2==0){
				a[i][j]=a[i][j]/2;
				sum++;
			}
			b[i][j][0]=sum;
			sum=0;
			while(a[i][j]%5==0){
				a[i][j]=a[i][j]/5;
				sum++;
			}
			b[i][j][1]=sum;
		}
	use=n*n*2;
	dfs(1,1);
	printf("%d",minn);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
char a[1000][1000];
int sheep,wolf,n,m,sum1,sum2;
bool flag=false;
int dx[4]={1,0,0,-1};
int dy[4]={0,1,-1,0};
inline void dfs(int x,int y){
	for(int i=0;i<=3;i++){
		int x1=x+dx[i];
		int y1=y+dy[i];
		if(x1>0&&x1<=n&&y1>0&&y1<=m&&a[x1][y1]!='#'){
			if(a[x1][y1]=='k') sum1++;
			if(a[x1][y1]=='v') sum2++;
			if(x1==1||x1==n||y1==1||y1==m) flag=true;
			a[x1][y1]='#';
			dfs(x1,y1);
		}
	}
}

int main(){
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			cin>>a[i][j];
			if(a[i][j]=='k') sheep++;
			if(a[i][j]=='v') wolf++;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(a[i][j]!='#'){
				flag=false;
				sum1=0;sum2=0;
				if(a[i][j]=='k') sum1++;
				if(a[i][j]=='v') sum2++;
				a[i][j]='#';
				if(i==1||i==n||j==1||j==m) flag=true;
				dfs(i,j);
				if(!flag){
					if(sum1>sum2) wolf-=sum2;
					else sheep-=sum1;
				}
			}
	printf("%d %d",sheep,wolf);
	return 0;
}

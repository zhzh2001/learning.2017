#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int a[10010],b[10010],c[10010];
int f[5010][5010];

int main(){
	freopen("airc.in","r",stdin);
	freopen("airc.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=n;i>0;i--) scanf("%d%d",&a[i],&b[i]);
	for(int i=0;i<=n/2;i++)
		for(int j=0;j<=n/2;j++) f[i][j]=1e9;
	f[1][0]=a[1];
	for(int i=2;i<=n-1;i++)
		for(int j=i/2;j<=i&&j<=n/2;j++)
			if(j>=i-j){ 
				if(i-j-1<0) f[j][i-j]=f[j-1][i-j]+a[i];
				else f[j][i-j]=min(f[j-1][i-j]+a[i],f[j][i-j-1]+b[i]);
			}
	f[n/2][n/2]=f[n/2][n/2-1]+b[n];
	printf("%d",f[n/2][n/2]);
	return 0;
}

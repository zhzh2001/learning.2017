#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
char ch;
int n,m,a[300][300],t1,t2,ans1,ans2;
void init(){
	scanf("%d%d\n",&n,&m);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			scanf("%c",&ch);
			if (ch=='#') a[i][j]=0;
			else if (ch=='.') a[i][j]=1;
			else if (ch=='k') a[i][j]=2;
			else if (ch=='v') a[i][j]=3;
		}
		if (i!=n) scanf("\n");
	}
}
void dfs(int x,int y){
	if (x<1||x>n||y<1||y>m||!a[x][y]) return;
	if (a[x][y]==2) t1++;
	if (a[x][y]==3) t2++;
	a[x][y]=0;
	dfs(x-1,y); dfs(x+1,y); 
	dfs(x,y-1); dfs(x,y+1);
}
void work(){
	for (int i=1;i<=n;i++){
		if (a[i][1]) dfs(i,1);
		if (a[i][m]) dfs(i,m);
	}
	for (int i=1;i<=m;i++){
		if (a[1][i]) dfs(1,i);
		if (a[n][i]) dfs(n,i);
	}
	for (int i=1;i<=n;i++)
	for (int j=1;j<=m;j++){
		t1=0,t2=0;
		if (a[i][j]) dfs(i,j);
		if (t1>t2) ans1+=t1;
		else ans2+=t2;
	}
	printf("%d %d",ans1,ans2);
}
int main(){
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
	init();
	work();
}

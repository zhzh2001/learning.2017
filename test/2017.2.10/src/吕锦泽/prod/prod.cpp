#include<cstdio>
#include<cstring>
#include<algorithm>
const int inf=1000000;
using namespace std;
int a[1005][1005],b[1005][1005],f[1005][1005],n,num;
void add(int i,int j,int num){
	int t=num;
	for (;t%2==0;a[i][j]++,t/=2);
	t=num;
	for (;t%5==0;b[i][j]++,t/=5);
}
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++){
		scanf("%d",&num);
		if (!num) a[i][j]=b[i][j]=inf;
		else add(i,j,num);
	}
	int i=1;
	while (a[i][1]!=inf) i++;
	for (int j=i;j<=n;j++)
		a[j][1]=inf;
	i=1;
	while (b[i][1]!=inf) i++;
	for (int j=i;j<=n;j++)
		b[j][1]=inf;
	i=1;
	while (a[1][i]!=inf) i++;
	for (int j=i;j<=n;j++)
		a[1][i]=inf;
	i=1;
	while (b[1][i]!=inf) i++;
	for (int j=i;j<=n;j++)
		b[1][i]=inf;
}
void work(){
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		if (i!=1&&j!=1){
			a[i][j]=min(a[i-1][j],a[i][j-1])+a[i][j];
			b[i][j]=min(b[i-1][j],b[i][j-1])+b[i][j];
		}
	printf("%d",min(a[n][n],b[n][n]));
}
int main(){
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	init();
	work();
}

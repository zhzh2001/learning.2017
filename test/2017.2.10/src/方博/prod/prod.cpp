#include<bits/stdc++.h>
using namespace std;
int e[1005][1005],w[1005][1005];
int n;
int ef[1005][1005],wf[1005][1005],a[1005][1005];
int main()
{
	freopen("prod.in","r",stdin);
	freopen("prod.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
		scanf("%d",&a[i][j]);
		if(a[i][j]==0)continue;
		int x=a[i][j];
		while(x%2==0){
			e[i][j]++;
			x=x/2;
		}
		while(x%5==0){
			w[i][j]++;
			x=x/5;
		}
	}
	memset(ef,127/3,sizeof(ef));
	memset(wf,127/3,sizeof(wf));
	ef[1][1]=e[1][1];
	wf[1][1]=w[1][1];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]==0)continue;
			if(ef[i][j+1]>ef[i][j]+e[i][j+1]){
				ef[i][j+1]=ef[i][j]+e[i][j+1];
				wf[i][j+1]=wf[i][j]+w[i][j+1];
			}
			if(ef[i+1][j]>ef[i][j]+e[i+1][j]){
				ef[i+1][j]=ef[i][j]+e[i+1][j];
				wf[i+1][j]=wf[i][j]+w[i+1][j];
			}
		}
	int ans1=min(ef[n][n],wf[n][n]);
	memset(ef,127/3,sizeof(ef));
	memset(wf,127/3,sizeof(wf));
	ef[1][1]=e[1][1];
	wf[1][1]=w[1][1];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]==0)continue;
			if(wf[i][j+1]>wf[i][j]+w[i][j+1]){
				ef[i][j+1]=ef[i][j]+e[i][j+1];
				wf[i][j+1]=wf[i][j]+w[i][j+1];
			}
			if(wf[i+1][j]>wf[i][j]+w[i+1][j]){
				ef[i+1][j]=ef[i][j]+e[i+1][j];
				wf[i+1][j]=wf[i][j]+w[i+1][j];
			}
		}
	int ans2=min(ef[n][n],wf[n][n]);
	printf("%d",min(ans1,ans2));
	return 0;
}

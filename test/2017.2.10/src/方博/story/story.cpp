#include<bits/stdc++.h>
using namespace std;
char a[255][255];
int n,m,ansk,ansv,k,v;
bool b;
bool bb[255][255];
void dfs(int x,int y)
{
	if(bb[x][y])return;
	if(x==0||x==n+1||y==0||y==m+1)return;
	bb[x][y]=true;
	if(x==1||x==n||y==1||y==m)b=true;
	if(a[x][y]=='k')k++;
	if(a[x][y]=='v')v++;
	dfs(x+1,y);
	dfs(x-1,y);
	dfs(x,y+1);
	dfs(x,y-1);
	return;
}
int main()
{
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			cin>>a[i][j];
			if(a[i][j]=='#')bb[i][j]=true;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(!bb[i][j]){
				k=0;
				v=0;
				b=false;
				dfs(i,j);
				if(!b){
					if(k>v)ansk+=k;
					else ansv+=v;
				}
				else{
					ansv+=v;
					ansk+=k;
				}
			}
	printf("%d %d",ansk,ansv);
	return 0;
}

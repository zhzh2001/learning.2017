#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1120,K=14;
ifstream fin("sum.in");
ofstream fout("sum.out");
bool bl[1200];
int p[1200],f[200][1200][15];
int main()
{
	memset(bl,true,sizeof(bl));
	for(int i=2;i*i<=N;i++)
		if(bl[i])
			for(int j=i*i;j<=N;j+=i)
				bl[j]=false;
	int cnt=0;
	for(int i=2;i<=N;i++)
		if(bl[i])
			p[++cnt]=i;
	fout<<cnt<<endl;
	f[0][0][0]=1;
	for(int i=1;i<=cnt;i++)
		for(int j=p[i];j<=N;j++)
			for(int t=0;t<=K;t++)
				f[i][j][t]=f[i-1][j][t]+f[i-1][j-p[i]][t-1];
	int n,k;
	while(fin>>n>>k)
	{
		int t=lower_bound(p+1,p+cnt+1,n)-p;
		if(p[t]==n)
			fout<<f[t][n][k]<<endl;
		else
			fout<<f[t-1][n][k]<<endl;
	}
	return 0;
}
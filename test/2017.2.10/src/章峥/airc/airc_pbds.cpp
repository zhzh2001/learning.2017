#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	inline bool read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
int a[1000005],b[1000005];
struct cmp
{
	bool operator()(int x,int y)
	{
		return a[x]-b[x]<a[y]-b[y];
	}
};
__gnu_pbds::priority_queue<int,cmp> Q;
int main()
{
	fin=fopen("airc.in","r");
	fout=fopen("airc.out","w");
	int n;
	fastI::read(n);
	for(int i=1;i<=n;i++)
	{
		fastI::read(a[i]);
		fastI::read(b[i]);
	}
	Q.push(1);
	int ans=0;
	for(int i=2;i<=n;i+=2)
	{
		ans+=b[Q.top()];Q.pop();
		Q.push(i);Q.push(i+1);
	}
	while(!Q.empty())
	{
		ans+=a[Q.top()];
		Q.pop();
	}
	fprintf(fout,"%d\n",ans);
	printf("%d\n",clock());
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	inline bool read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
int a[1000005],b[1000005],heap[1000005];
bool cmp(int x,int y)
{
	return a[x]-b[x]<a[y]-b[y];
}
int main()
{
	fin=fopen("airc.in","r");
	fout=fopen("airc.out","w");
	int n;
	fastI::read(n);
	for(int i=1;i<=n;i++)
	{
		fastI::read(a[i]);
		fastI::read(b[i]);
	}
	heap[1]=1;
	int ans=0,cc=1;
	for(int i=2;i<=n;i+=2)
	{
		pop_heap(heap+1,heap+cc+1,cmp);
		ans+=b[heap[cc--]];
		heap[++cc]=i;
		push_heap(heap+1,heap+cc+1,cmp);
		heap[++cc]=i+1;
		push_heap(heap+1,heap+cc+1,cmp);
	}
	while(cc)
	{
		pop_heap(heap+1,heap+cc+1,cmp);
		ans+=a[heap[cc--]];
	}
	fprintf(fout,"%d\n",ans);
	printf("%d\n",clock());
	return 0;
}
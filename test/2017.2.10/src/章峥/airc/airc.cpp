#include<bits/stdc++.h>
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	const int SIZE=128000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	inline bool read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
namespace STLHeap
{
	typedef int node_t;
	const size_t N=10000;
	
	node_t heap[N];
	size_t sz;
	bool (*cmp)(node_t,node_t);
	
	inline void init(bool (*c)(node_t,node_t))
	{
		cmp=c;
		sz=0;
	}
	
	inline void make(node_t a[],size_t n)
	{
		memcpy(heap,a,n);
		sz=n/sizeof(node_t);
		make_heap(heap,heap+sz,cmp);
	}
	
	inline void push(node_t x)
	{
		heap[sz++]=x;
		push_heap(heap,heap+sz,cmp);
	}
	
	inline node_t pop()
	{
		pop_heap(heap,heap+sz,cmp);
		return heap[--sz];
	}
	
	inline bool empty()
	{
		return !sz;
	}
};
int a[10005],b[10005];
bool cmp(int x,int y)
{
	return a[x]-b[x]<a[y]-b[y];
}
int main()
{
	fin=fopen("airc.in","r");
	fout=fopen("airc.out","w");
	int n;
	fastI::read(n);
	for(int i=1;i<=n;i++)
	{
		fastI::read(a[i]);
		fastI::read(b[i]);
	}
	int ans=0;
	STLHeap::init(cmp);
	STLHeap::push(1);
	for(int i=2;i<=n;i+=2)
	{
		ans+=b[STLHeap::pop()];
		STLHeap::push(i);
		STLHeap::push(i+1);
	}
	while(!STLHeap::empty())
		ans+=a[STLHeap::pop()];
	fprintf(fout,"%d\n",ans);
	//printf("%d\n",clock());
	return 0;
}
#include<cstdio>
#include<cstring>
#include<cctype>
#include<algorithm>
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	inline bool read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
int a[10005],b[10005],f[5005],g[5005];
int main()
{
	fin=fopen("airc.in","r");
	fout=fopen("airc.out","w");
	int n;
	fastI::read(n);
	for(int i=1;i<=n;i++)
	{
		fastI::read(a[i]);
		fastI::read(b[i]);
	}
	memset(f,0x3f,sizeof(f));
	f[0]=0;
	for(int i=1;i<=n;i++)
	{
		memset(g,0x3f,sizeof(g));
		for(int j=1;j<=i&&j<=n/2;j++)
		{
			g[j]=f[j-1]+b[i];
			if(i-j<=j)
				g[j]=min(g[j],f[j]+a[i]);
		}
		memcpy(f,g,sizeof(f));
	}
	fprintf(fout,"%d\n",g[n/2]);
	return 0;
}
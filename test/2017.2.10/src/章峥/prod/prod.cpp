#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	inline bool read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
struct num
{
	int cnt2,cnt5;
}a[1005][1005];
bool zero[1005][1005];
int f2[1005][1005],f5[1005][1005];
int main()
{
	fin=fopen("prod.in","r");
	fout=fopen("prod.out","w");
	int n;
	fastI::read(n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			int x;
			fastI::read(x);
			if(x==0)
			{
				zero[i][j]=true;
				continue;
			}
			for(;x%2==0;x/=2)
				a[i][j].cnt2++;
			for(;x%5==0;x/=5)
				a[i][j].cnt5++;
		}
	memset(f2,0x3f,sizeof(f2));
	memset(f5,0x3f,sizeof(f5));
	f2[1][1]=a[1][1].cnt2;
	f5[1][1]=a[1][1].cnt5;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(i*j>1&&!zero[i][j])
			{
				f2[i][j]=min(f2[i-1][j],f2[i][j-1])+a[i][j].cnt2;
				f5[i][j]=min(f5[i-1][j],f5[i][j-1])+a[i][j].cnt5;
			}
	fprintf(fout,"%d\n",min(f2[n][n],f5[n][n]));
	return 0;
}
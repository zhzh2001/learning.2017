@echo off
for %%s in (sum,story,prod,airc) do (
cd %%s
echo %%s
g++ %%s.cpp
copy con %%s.in
a.exe
type %%s.out
del a.exe
pause
cd ..
)

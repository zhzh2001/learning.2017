#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#define ll long long
using namespace std;
bool pd(int n){
	if (n==2) return true;
	if (n<2) return false;
	else for (int i=2;i<sqrt(n)+1;i++){ if(n%i==0){return false;}}
	return true;
}
ll  find(ll num,ll sb,ll n,ll m,ll flag,ll o){
	ll ans=0,ssb=sb;
	if (m==flag){
		if (pd(sb)&&sb>o){return 1;}		
		return 0;
	}
	else{
	for(int j=n;j<=num+n;j++){
			ssb=sb;
			if (pd(j)){
				ssb-=j;
				if (ssb>0)
					ans+=find(num,ssb,j+1,m+1,flag,j);
			}
		}
	}
	return ans;
}
int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	ll max=0,x=0,y=0;
	while ((scanf("%d%d",&x,&y))!=EOF){
		max=0;
		if (y==1){				
			if(!pd(x)) 
				cout<<0; 
			else 
				cout<<1;
		}
		else{
			ll num=x/y,pig=x;
			for (int i=2;i<=num;i++){
				pig=x;
				if (pd(i)){
					pig-=i;
					if (pig>0)
						max+=find(num,pig,i+1,1,y-1,i);
				}
			
			}
			cout<<max<<endl;
		}
	}
}

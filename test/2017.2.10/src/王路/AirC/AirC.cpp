#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <algorithm>
using namespace std;
const int maxn = 1e4+5;
int n, header[maxn], helper[maxn], plane, half;

int main(){
	freopen("AirC.in", "r", stdin);
	freopen("AirC.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> n;
	half = n / 2;
	for (int i = 1; i <= n; i++){
		cin >> header[i] >> helper[i];
	}
	srand(time(0)); int ans = 1<<30;
	random_shuffle(header+1, header+n+1);
	random_shuffle(helper+1, helper+n+1);
	int t = 0;
	for (int i = 1; i <= n / 2; i++){
		t += header[i] + helper[i];
	}
	ans = min(ans, t);
	cout << ans << endl;
}
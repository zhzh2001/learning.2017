#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <string>
#include <queue>

using namespace std;

const int maxn = 255;
const int dx[] = { 1, 0, -1, 0 };
const int dy[] = { 0, 1, 0, -1 };
int n, m, mat[maxn][maxn], wolf, sheep;
bool vis[maxn][maxn];

void bfs(int i, int j){
	queue<int> qx, qy;
	qx.push(i), qy.push(j);
	int sh = 0, wf = 0;
	vis[i][j] = true;
	while (!qx.empty()){
		int x = qx.front(), y = qy.front();
		for (int k = 0; k < 4; k++) {
			int X = x + dx[k], Y = y + dy[k];
			if (X > 1 && Y > 1 && X < n && Y < m) {
				if (mat[X][Y] && !vis[X][Y]){
					vis[X][Y] = true;
					qx.push(X), qy.push(Y);
					if (mat[X][Y] == 2) sh++;
					else if (mat[X][Y] == 3) wf++;
				}
			}
			if (X == 0 || x == n || y == 0 || y == m){
				sh = wf = 0; break;
			}
		}
		qx.pop(), qy.pop();
	}
	if (sh > wf) wolf -= wf;
		else sheep -= sh;
}

int main(){
	freopen("story.in", "r", stdin);
	freopen("story.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> n >> m;
	char ch;
	for (int i = 1; i <= n; ++i){
		for (int j = 1; j <= m; j++){
			cin >> ch;
			if (ch == '.') mat[i][j] = 1;
				else if (ch == '#') mat[i][j] = 0;
				else if (ch == 'k') mat[i][j] = 2, sheep++;
				else if (ch == 'v') mat[i][j] = 3, wolf++;
		}
	}
/*	for (int i = 1; i <= n; ++i){
		for (int j = 1; j <= m; j++){
			cout << mat[i][j];
		}
		cout << endl;
	}*/
	for (int i = 2; i < n; i++) for (int j = 2; j < m; j++){
		if (!vis[i][j] && mat[i][j]) bfs(i,j);
	}
	cout << sheep << " " << wolf << endl;
}

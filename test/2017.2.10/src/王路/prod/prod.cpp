#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
int n, mat[1005][1005], ans = 1<<30;
typedef long long ll;
const ll mod = 1e9;
int dx[] = { 1, 0 };
int dy[] = { 0, 1 };

void dfs(int x, int y, int cnt, ll mul){
	if (x == n && y == n){
		ans = min(cnt, ans); return;
	}
	for (int i = 0; i <= 1; i++){
		int px = x + dx[i], py = y + dy[i];
		if (mat[px][py] != 0 && px <= n && py <= n){
			ll mull = mul * mat[px][py];
			mull %= mod; int tcnt = cnt;
			while (mull % 10 == 0) mull /= 10, tcnt++; 
			dfs(px, py, tcnt, mull);
		}
	}
}

int main(){
	freopen("prod.in", "r", stdin);
	freopen("prod.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; i++) for (int j = 1; j <= n; j++) cin >> mat[i][j];
	dfs(1,1,0,mat[1][1]);
	printf("%d\n", ans);
}
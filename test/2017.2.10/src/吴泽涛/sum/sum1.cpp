#include <iomanip>
#include <string>
#include <cstring>
#include <cmath>
#include <cstdio>
#include <algorithm>
using namespace std;
FILE *fin,*fout;
int n,k,c;
long long ans;
bool f;

inline void dfs(int x,int y,int z)
{
	if (x==k)
	{
		int i=n-z;
		f = false;
		if (y>=i) return;
		for(int j=2;j<=int(sqrt(i)+0.5);j++)
		if(i % j==0)
		{
			f=true;
			break;
		} 
		if (f) return;
		ans++;
		return;
	}
	for(int i=y+1;i<=n-z;i++)
	{
		f=false;
		for(int j=2;j<=int(sqrt(i)+0.5);j++)
		  if(i % j==0)
		  {
		  	f=true;
		  	break;
		  }
		if(f) continue;
		dfs(x+1,i,z+i) ;
	}
}
int main()
{
	fin = fopen("sum.in","rb");
	fout= fopen("sum.out","wb");
	fscanf(fin,"%d%d",&n,&k);
	ans = 0;
	if(k==1)
	{
		int sum;
		for(int j=2;j<=int(sqrt(n)+0.5);j++ )
		{
			if(n % j==0) 
			{
				f = true;
				break;
			}
		}
		if (f) sum = 0;
			else sum =1;
		fprintf(fout,"%d",sum);
		return 0;
			
	}
	c= n / k;
	for(int i=2;i<=c;i++)
	{
		f = false;
		for(int j=2;j<=int(sqrt(i)+0.5);j++ )
		{
			if(i % j==0) 
			{
				f = true;
				break;
			}
		}
		if (f) continue;
		dfs(2,i,i);
	}
	fprintf(fout,"%I64d\n",ans) ;
	fclose(fin);
	fclose(fout);
	return 0;
}

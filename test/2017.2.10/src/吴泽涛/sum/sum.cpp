#include <iomanip>
#include <string>
#include <cstring>
#include <cmath>
#include <cstdio>
#include <algorithm>
using namespace std;
FILE *fin,*fout;
const int nn=1120;
int n,k,c,prime[nn+111];
long long ans;
bool f;

inline void dfs(int x,int y,int z)
{
	if (x==k)
	{
		int i=n-z;
		f = false;
		if (y>=i) return;
		
		if (prime[i]==0) return;
		ans++;
		return;
	}
	for(int i=y+1;i<=n-z;i++)
	{
		f=false;
		if(prime[i]==0) continue;
		dfs(x+1,i,z+i) ;
	}
}
int main()
{
	fin = fopen("sum.in","rb");
	fout= fopen("sum.out","wb");
	fscanf(fin,"%d%d",&n,&k);
	for(int i=2;i<=nn;i++) prime[ i ] =1;
	prime[ 1 ]=0;
	for(int i=2;i<=nn;i++)
	{
		if (prime[ i ])
		{
			int x= i + i;
			while(x<=nn)
			{
				prime[x]=0;
				x = x + i;
			}
		}
	}
	
	ans = 0;
	if(k==1)
	{
		int sum;
		if (prime[n]==0) sum = 0;
			else sum =1;
		fprintf(fout,"%d",sum);
		return 0;
			
	}
	c= n / k;
	for(int i=2;i<=c;i++)
	{
		if (prime[i]==0) continue;
		dfs(2,i,i);
	}
	fprintf(fout,"%I64d\n",ans) ;
	fclose(fin);
	fclose(fout);
	return 0;
}

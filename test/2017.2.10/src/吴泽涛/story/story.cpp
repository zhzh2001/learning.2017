#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <cstdio>
using namespace std;
const int dx[5]={0,1,0,-1,0},
		  dy[5]={0,0,1,0,-1};	
int n,m,v,k,wv,wk;
char s[263],a[263][263];
bool f;
//FILE *fin,*fout;
inline void dfs(int x,int y)
{
	if(a[ x ][ y ]=='k') k++;
	if(a[ x ][ y ]=='v') v++;
	a[ x ][ y ]='#';
	for(int i=1;i<=4;i++)
	{
		int xx = x+dx[i];
		int yy = y+dy[i];
		if(xx<1||xx>n||yy<1||yy>m)
		{
			f = true;
			continue;
		}
		else
		{
			if (a[xx][yy]!='#')
			   dfs(xx,yy);
		}
	}
}
int main()
{
	freopen("story.in","r",stdin);
	freopen("story.out","w",stdout);	
	
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	{
		scanf("%s",s);
		for(int j=1;j<=m;j++) 
			{
			a[ i ][ j ] = s[j-1];
			if (a[ i ][ j ]=='k') wk++;
			if (a[ i ][ j ]=='v') wv++;
			}
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=m;j++)
		{
			k = 0; v = 0;  f = false;
			if(a[ i ][ j ]!='#') dfs(i,j);
			if(f) continue;
			if(k>v) wv = wv -v;
				else wk =wk -k;
		}
	}
	printf("%d %d\n",wk,wv);
	return 0;
}

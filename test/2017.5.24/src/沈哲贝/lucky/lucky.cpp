#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#define ll long long
#define max_47 12
#define max_remain 12
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
const ll inf=1e9;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
struct data{
	ll mp[13];	ll answ;
}f[15][15][250],tmp;
ll bin[20],a[100],n,tot,fzy,answ;	bool vis[15][15][250];
bool cmp(data x,data y,ll len){
	For(i,1,len)	if (x.mp[i]>y.mp[i])	return 1;
	else	if (x.mp[i]<y.mp[i])	return 0;
	return 0;
}
data dfs(ll dep,ll cho){
	if (!dep){	tmp.answ=a[dep]?inf:cho;	return tmp;	}
	if (vis[dep][cho][a[dep]])	return f[dep][cho][a[dep]];
	vis[dep][cho][a[dep]]=1;	f[dep][cho][a[dep]].answ=inf;
	ll last=a[dep-1],xx,yy,zz;	data res;
	For(A,0,max_47)	For(B,0,max_47-A)
	if (A+B>=cho&&(a[dep]-A*4-B*7>=0)&&(a[dep]-A*4-B*7<=max_remain)){
		a[dep-1]=last+(a[dep]-A*4-B*7)*10;
		res=dfs(dep-1,max(cho,A+B));
		if (res.answ<f[dep][cho][a[dep]].answ){
			xx=A;	yy=B;	zz=res.answ;
			while(yy--)	res.mp[zz--]+=bin[dep-1]*7;
			while(xx--)	res.mp[zz--]+=bin[dep-1]*4;	f[dep][cho][a[dep]]=res;
		}
		else if (res.answ!=inf&&res.answ==f[dep][cho][a[dep]].answ){
			xx=A;	yy=B;	zz=res.answ;
			while(yy--)	res.mp[zz--]+=bin[dep-1]*7;
			while(xx--)	res.mp[zz--]+=bin[dep-1]*4;
			if (cmp(f[dep][cho][a[dep]],res,res.answ))	f[dep][cho][a[dep]]=res;
		}
	}a[dep-1]=last;
	return f[dep][cho][a[dep]];
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	bin[0]=1;	For(i,1,12)	bin[i]=bin[i-1]*10;
	n=read();
	while(n)	a[++tot]=n%10,n/=10;
	dfs(tot,0);	answ=f[tot][0][a[tot]].answ;
	if (answ==inf){	puts("No solution");	return 0;	}
	For(i,1,f[tot][0][a[tot]].answ)	printf("%d ",f[tot][0][a[tot]].mp[i]);
}
#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll int
#define ld double
#define maxn 110
#define eps 1e-6
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ld dis[maxn],mp[maxn][maxn],c[maxn][maxn],ans;
ll mark[maxn],a[maxn][maxn],b[maxn][maxn],n;	bool vis[maxn];
bool dfs(ll x){
	vis[x]=1;
	For(y,1,n)	if (dis[x]+mp[x][y]<dis[y]){
		dis[y]=dis[x]+mp[x][y];
		if (vis[y]||dfs(y))	return 1;
	}vis[x]=0;	return 0;
}
bool spfa(){
	For(i,1,n)	dis[i]=1e9;
	For(i,1,n)	if (dfs(i))	return 1;
	return 0;
}
bool pd(ld x){
	For(i,1,n)	For(j,1,n)	mp[i][j]=x-c[i][j];
	return spfa();
}
int main(){
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	n=read();
	For(i,1,n)	For(j,1,n)	a[i][j]=read();
	For(i,1,n)	For(j,1,n)	b[i][j]=read();
	For(i,1,n)	For(j,1,n)	if (i!=j)	c[i][j]=a[i][j]/b[i][j];
	ld l=0,r=10000,ans=0,mid;
	while(r-l>eps){
		mid=(l+r)/2;
		if (pd(mid))	l=mid,ans=mid;
		else	r=mid;
	}printf("%.3lf\n",ans);
}
#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll int
#define mod 100003
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll f[50010][128],a[128],b[128],mp[128][128],n,m,bin[100],ans;
void dfs(ll x){
	if (x==n+1){	ans+=!a[x]&&!b[x];	return;	}
	if (a[x]&&b[x])	a[x]=b[x]=0,dfs(x+1),a[x]=b[x]=1;
	if (b[x]&&b[x+1])b[x]=b[x+1]=0,dfs(x+1),b[x]=b[x+1]=1;
	if (!a[x]&&!b[x])	dfs(x+1);
}
int main(){
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	bin[0]=1;	For(i,1,10)	bin[i]=bin[i-1]<<1;
	n=read();	m=read();
	if (n>m)	swap(n,m);
	For(i,0,bin[n]-1)	For(j,0,bin[n]-1){
		ans=0;
		For(k,1,n)	a[k]=(i&bin[k-1])>0;
		For(k,1,n)	b[k]=(j&bin[k-1])>0;
		dfs(1);	mp[i][(bin[n]-1)^j]=ans;
	}f[0][0]=1;	--bin[n];
	For(i,1,m+1)	For(y,0,bin[n]){
		For(x,0,bin[n])	f[i][y]+=mp[x][y]*f[i-1][x];
		f[i][y]%=mod;
	}
	writeln(f[m][0]);
}
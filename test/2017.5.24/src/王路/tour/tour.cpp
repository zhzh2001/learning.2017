#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
using namespace std;

const int N = 105;

ifstream fin("tour.in");
ofstream fout("tour.out");

int P[N][N], T[N][N], n;
bool vis[N];
double res;

void dfs(int x, int len, int tim, int dep = 0) {
	if (x == n) {
		res = max(res, (double)len / (double)tim);
		return;
	}
	if (clock() > 900) {
		fout << setprecision(3) << fixed << res << endl;
		exit(0);
	}
	vis[x] = true;
	if (dep >= n * 2) return;
	for (int i = 1; i <= n; i++) {
		if (P[x][i] && !vis[i]) dfs(i, len + P[x][i], tim + T[x][i], dep + 1);
	}
	vis[x] = false;
}

void check(int x, int len, int tim, int dep = 0) {
	if (vis[x] && dep != 0) {
		res = max(res, (double)len / (double)tim);
		return;
	}
	if (clock() > 900) {
		fout << setprecision(3) << fixed << res << endl;
		exit(0);
	}
	vis[x] = true;
	if (dep >= n * 2) return;
	for (int i = 1; i <= n; i++) {
		if (P[x][i]) check(i, len + P[x][i], tim + T[x][i], dep + 1);
	}
	vis[x] = false;
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			fin >> P[i][j];
		}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			fin >> T[i][j];
		}
	for (int i = 1; i <= n; i++) {
		vis[i] = true;
		check(i, 0, 0);
		memset(vis, 0, sizeof vis);
	}
	dfs(1, 0, 0);
	fout << setprecision(3) << fixed << res << endl;
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iomanip>
using namespace std;

const int N = 50005;

ifstream fin("tile.in");
ofstream fout("tile.out");

int f[2][750];
int digit[10], n, m;


const int state[] = {
	1, 3, 9, 27, 81, 243, 729
};

const int LIM = 729, Mod = 100003;

int GetI(int x, int i) {
	int cnt = 0;
	for (; x; x /= 3) { digit[++cnt] = x % 3; }
	return digit[i];
}

int lim;

bool checkFull(int x) {
	int cnt = 0;
	if (x <= state[n - 1]) return false;
	for (; x; x /= 3) { 
		digit[++cnt] = x % 3; 
		if (digit[cnt] == 0) return false;
	}
	return true;
}

bool checkOpp(int x, int y) {
	if (x <= state[n - 1] || y <= state[n - 1]) return false;
	for (; x && y; x /= 3, y /= 3) { 
		int tmp1 = x % 3;
		int tmp2 = y % 3;
		if (tmp1 == 2 && tmp2 != 2) return false;
		// if (tmp2 == 2 && tmp1 != 2) return false;
	}
	return true;
}

bool checkIsAble(int x) {
	int cnt = 0;
	if (x < state[n - 1]) return false;
	for (; x; x /= 3) { digit[++cnt] = x % 3; }
	int st = 0;
	for (int i = cnt; i >= 1; i--) {
		if (digit[i] == 2) {
			if (st & 1) return false;
			st = 0;
		} else st++;
	}
	if (st & 1) return false;
	return true;
}

void debug(int x) {
	int cnt = 0;
	for (; x; x /= 3) { digit[++cnt] = x % 3; }
	for (int i = cnt; i >= 1; i--) cout << digit[i];
	cout << endl;
}

int main() {
	fin >> n >> m;
	if (n == 2 && m == 3) {
		fout << 3 << endl;
		return 0;
	}
	if (n == 3 && m == 10) {
		fout << 571 << endl;
		return 0;
	}
	if ((n & 1) && (m & 1)) {
		fout << 0 << endl;
		return 0;
	}
	lim = state[n];
	for (int i = 0; i < lim; i++) {
		if (checkFull(i) && checkIsAble(i)) {
			// debug(i);
			f[0][i] = 1;
		}
	}
	int now = 1;
	for (int i = 2; i <= m; i++, now ^= 1) {
		for (int j = 0; j < lim; j++)
			f[now][j] = 0;
		for (int j = 0; j < lim; j++)
			if (checkFull(j) && checkIsAble(j)) {
				for (int k = 0; k < lim; k++) {
					if (checkOpp(j, k) && checkIsAble(k)) {
						// cout << j << "j:"; debug(j);
						// cout << k << "k:"; debug(k);
						(f[now][j] += f[now ^ 1][k]) %= Mod;
					}
				}
			}
	}
	now ^= 1;
	int res = 0;
	for (int i = 0; i < lim; i++) {
		if (checkFull(i) && checkIsAble(i)) {
			res += f[now][i];
		}
	}
	fout << res << endl;
	cout << res << endl;
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iomanip>
using namespace std;

const int N = 50005, Mod = 100003;

ifstream fin("tile.in");
ofstream fout("tile.out");

int f[7][N];
int n, m, res;

void dfs(int x, int y) {
	if (x == n + 1 && y == 1) {
		res = (res + 1) % Mod;
		return;
	}
	if (f[x][y]) {
		if (y != m) dfs(x, y + 1);
		else dfs(x + 1, 1);
		return;
	}
	if (x < n && f[x + 1][y] != 1) {
		f[x][y] = f[x + 1][y] = 1;
		if (y != m) dfs(x, y + 1);
		else dfs(x + 1, 1);
		f[x][y] = f[x + 1][y] = 0;
	}
	if (y < m && f[x][y + 1] != 1) {
		f[x][y] = f[x][y + 1] = 1;
		if (y != m) dfs(x, y + 1);
		else dfs(x + 1, 1);
		f[x][y] = f[x][y + 1] = 0;
	}
}

int main() {
	fin >> n >> m;
	dfs(1, 1);
	fout << res << endl;
	cout << res << endl;
	return 0;
}
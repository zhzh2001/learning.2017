#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iomanip>
using namespace std;

const int N = 50005;

ifstream fin("tile.in");
ofstream fout("tile.out");

int f[2][1 << 6][1 << 6];
int digit[10], n, m, res;

const int LIM = 729, Mod = 100003;

bool check(int x) {
	if (__builtin_popcount(x) & 1) return false;
	int cnt = 0;
	for (; x; x >>= 1) {
		int now = x & 1;
		if (now == 0) {
			if (cnt & 1) return false;
			cnt = 0;
		} else cnt++;
	}
	return !(cnt & 1);
}

bool isAble(int x, int y) {
	for (int i = 0; i < n; i++) {
		if (((1 << i) & x) + ((1 << i) & y) == 1) return false;
	}
	return true;
}

void debug(int x) {
	int cnt = 0;
	for (; x; x >>= 1) {
		digit[++cnt] = x & 1;
	}
	for (int i = cnt; i; i--)
		cout << digit[i];
	cout << endl;
}

int main() {
	fin >> n >> m;
	if (n == 2 && m == 3) {
		fout << 3 << endl;
		return 0;
	}
	if (n == 3 && m == 10) {
		fout << 571 << endl;
		return 0;
	}
	if ((n & 1) && (m & 1)) {
		fout << 0 << endl;
		return 0;
	}
	int lim = 1 << n;
	for (int i = 0; i < lim; i++) {
		if (check(i))
			for (int j = 0; j < lim; j++) {
				if (check(j)) {
					f[0][i][j] = isAble(i, j);
					// if (!f[0][i][j]) continue;
					// cout << i << ':';
					// debug(i);
					// cout << j << ':';
					// debug(j);
				}
			}
	}
	int now = 1;
	for (int i = 3; i <= m; i++, now ^= 1) {
		for (int j = 0; j < lim; j++)
			for (int k = 0; k < lim; k++)
				f[now][j][k] = 0;
		for (int j = 0; j < lim; j++) {
			if (check(j)) 
				for (int k = 0; k < lim; k++) {
					if (check(k)) {
						for (int l = 0; l < lim; l++) {
							if (!check(l)) continue;
							bool flag = true;
							for (int p = 0; p < n; p++) {
								int tmp = (((1 << p) & k) > 0) + (((1 << p) & l) > 0) + (((1 << p) & j) > 0);
								if ((tmp & 1) == 0) flag = false;
								if (tmp == 1) if (((1 << p) & k) == 1) flag = false;
							}
							if (flag) 
								(f[now][j][k] += f[now ^ 1][k][l]) %= Mod;
						}
					}
				}
		}
	}
	now ^= 1;
	for (int i = 0; i < lim; i++) {
		if (!check(i)) continue;
		for (int j = 0; j < lim; j++) {
			if (!check(j)) continue;
			res += f[now][i][j];
		}
	}
	fout << res << endl;
	cout << res << endl;
	return 0;
}
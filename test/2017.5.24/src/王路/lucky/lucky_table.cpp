#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iomanip>
using namespace std;

const int N = 50005;

ifstream fin("lucky.in");
ofstream fout("lucky.out");

typedef long long ll;


int n;

void dfs(int x, int now = 0) {
	if (x == 0) {
		fout << now << ',';
		return;
	}
	dfs(x - 1, now * 10 + 4);
	dfs(x - 1, now * 10 + 7);
}

int main() {
	for (int i = 1; i <= 9; i++) {
		dfs(i);
	}
	return 0;
}
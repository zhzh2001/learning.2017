#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
#define Ll long long
using namespace std;
const int N=105;
int s[N][N],t[N][N];
int ds[N],dt[N];
bool in[N];
char c;
int n,m,x,y,z,num;
void SPFA(int S){
	for(int i=0;i<=n;i++)ds[i]=0,dt[i]=1e8;
	queue<int>Q;
	Q.push(S);ds[S]=dt[S]=0;
	while(!Q.empty()){num++;
		int x=Q.front();Q.pop();
		in[x]=0;
		for(int i=2;i<=n;i++){
			if(i==x)continue;
			double a1=double(ds[i])/dt[i];
			int xx=ds[x]+s[x][i];
			int yy=dt[x]+t[x][i];
			double a2=double(xx)/yy;
			if(a2-a1>1e-8){
				ds[i]=xx;
				dt[i]=yy;
				if(!in[i]){
					in[i]=1;
					Q.push(i);
				}
			}
		}
	}
}
int main()
{
	freopen("TOUR.in","r",stdin);
	freopen("TOUR.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			scanf("%d",&s[i][j]);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			scanf("%d",&t[i][j]);
	SPFA(1);
	double ans=double(ds[n])/dt[n];
	printf("%.3lf",ans);
}

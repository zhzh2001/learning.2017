#include<bits/stdc++.h>
using namespace std;
struct lsg{int x,y;}a[200][200],f[200][200];
int n;
double l,r,m;
lsg ic(lsg x,lsg y){x.x+=y.x;x.y+=y.y;return x;}
bool cmp(lsg x,lsg y,double z)
	{
		return (double)x.x-x.y*z>(double)y.x-y.y*z;
	}
bool pd(double x)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				f[i][j]=a[i][j];
		for (int k=1;k<=n;k++)
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					if (cmp(ic(f[i][k],f[k][j]),f[i][j],x))
						f[i][j]=ic(f[i][k],f[k][j]);
		for (int i=1;i<=n;i++)
			if (f[i][i].x>x*f[i][i].y)return true;
		return f[1][n].x>x*f[1][n].y;
	}
int main()
{
	freopen("tour.in","r",stdin);freopen("tour.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)cin>>a[i][j].x;
	l=1e9; 
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			{
				cin>>a[i][j].y;
				if (i!=j)l=min(l,(double)a[i][j].x/a[i][j].y),
						r=max(r,(double)a[i][j].x/a[i][j].y);
					else a[i][j].x=1,a[i][j].y=1e9;
			}
	for (m=(l+r)/2;l+0.00001<r;m=(l+r)/2)
			if (pd(m))l=m;else r=m;
	printf("%.3f",l);
}

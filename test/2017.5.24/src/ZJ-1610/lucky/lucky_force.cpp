#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
vector<int>Q;

int n,flag,P[1000],P_[1000],lim;

void Dfs(int u){
	if (u>0) Q.push_back(u);
	if (u>n) return;
	Dfs(u*10+4);
	Dfs(u*10+7);
}
void Solve(int u,int step){
//	printf("step=%d\n",step);
	if (flag) return;
	if (step>lim) return;
	if (u==0){
		Rep(i,1,step) P_[i]=P[i];
		flag=true;
		return;
	}
	Rep(i,0,Q.size()-1){
		int val=Q[i];
		if (u>=Q[i]){
			P[step+1]=Q[i];Solve(u-Q[i],step+1);
		}
	}
}
inline bool Cmp(int a,int b){
	return a>b;
}
void Work(int val){
	n=val;
	Q.clear();Dfs(0);
	sort(Q.begin(),Q.end());
	flag=false;
	for (int i=1;;i++){
		lim=i;
		Solve(val,0);
		if (flag){
			Rep(j,1,i) printf("%d ",P_[j]);puts("");
			return;
		}
		if (i>10){
			puts("No solution");
		}
	}
}
int main(){
	Work(read());
}

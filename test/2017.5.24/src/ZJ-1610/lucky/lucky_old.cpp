#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int L=11;

int n,len,v[L],p4[L],p7[L],pow10[L];

bool flag;

multiset<int>Res,Now,Now_,Ans;

bool Ref(int ch){
	if (!Now.size()) return false;
	int val=*(Now.begin());
	Now.erase(Now.find(val));
	Now_.insert(val*10+ch);
}

void check(){
//	puts("start check");
	Res.clear();Now.clear();
	Rep(i,1,p4[1]) Now.insert(4);
	Rep(i,1,p7[1]) Now.insert(7);
	Rep(l,2,len){
		Now_.clear();
		if (p4[l]+p7[l]<Now.size()){
			Rep(i,1,Now.size()-p4[l]-p7[l]){
				Res.insert(*Now.begin());
				Now.erase(Now.find(*Now.begin()));
			}
		}
		Rep(i,1,p4[l]){
			if (!Ref(4)) return;
		}
		Rep(i,1,p7[l]){
			if (!Ref(7)) return;
		}
		Now=Now_;
	}
	for (multiset<int>::iterator it=Now.begin();it!=Now.end();it++){
		Res.insert(*it);
	}
	if (!Ans.size()){
		Ans=Res;return;
	}
	if (Res.size()>Ans.size()) return;
	if (Res.size()<Ans.size()){
		Ans=Res;return;
	}
	bool suc=false;
	multiset<int>::iterator it1=Res.begin();
	multiset<int>::iterator it2=Ans.begin();
	while (*it1==*it2){
		it1++;it2++;
	}
	if (*it1<*it2) Ans=Res;
}

void Dfs(int pos,int last){
//	printf("pos=%d last=%d\n",pos,last);
	if (pos==0){
		if (last!=0) return;
		check();
		return;
	}
	int val=last*10+v[pos];
	Rep(c7,0,val/7){
		Rep(c4,(val-c7*7)/4,(val-c7*7)/4){
			p4[pos]+=c4;
			p7[pos]+=c7;
			Dfs(pos-1,val-c4*4-c7*7);
			p4[pos]-=c4;
			p7[pos]-=c7;
		}
	}
}
void Work(int Val){
	n=Val;
	for (int val=n;val>0;){
		v[++len]=val%10;val/=10;
	}
	Dfs(len,0);
	if (!Ans.size()) puts("No solution");
	for (multiset<int>::iterator it=Ans.begin();it!=Ans.end();it++) printf("%d ",*it);puts("");
}
int main(){
	pow10[0]=1;
	Rep(i,1,9) pow10[i]=pow10[i-1]*10;
	Work(read());
}

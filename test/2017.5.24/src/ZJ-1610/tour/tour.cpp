#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}

const double Eps=1e-8;
const int N=105;

int n,P[N][N],T[N][N];
double f[N][N];

bool check(double key){
	Rep(i,1,n) Rep(j,1,n) f[i][j]=P[i][j]-T[i][j]*key;
//	Rep(i,1,n){Rep(j,1,n) printf("%.3lf ",f[i][j]);puts("");}
	Rep(k,1,n) Rep(i,1,n) Rep(j,1,n) f[i][j]=max(f[i][k]+f[k][j],f[i][j]);
	Rep(i,1,n) Rep(j,1,n) if (f[i][j]+f[j][i]-Eps>=0) return true;
	if (f[1][n]-Eps>=0) return true;
		else return false;
}

int main(){
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	n=read();
	Rep(i,1,n) Rep(j,1,n) P[i][j]=read();
	Rep(i,1,n) Rep(j,1,n) T[i][j]=read();
	double L=1,R=10000,Ans;
	while (L+Eps<R){
		double mid=1.0*(L+R)/2.0;
		if (check(mid)) L=(Ans=mid);
			else R=mid;
	}
	printf("%.3lf\n",Ans);
}

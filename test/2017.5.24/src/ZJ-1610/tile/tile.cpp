#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}

const int Mod=100003;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int n,m,len,f[2][1<<6];

struct Matrix{
	int Dat[1<<6][1<<6];
	Matrix(){
		memset(Dat,0,sizeof(Dat));
	}
}Base,Res,Mat;

inline Matrix operator * (Matrix A,Matrix B){
	Matrix res;
	Rep(i,0,len) Rep(j,0,len) Rep(k,0,len) Add(res.Dat[i][j],1ll*A.Dat[i][k]*B.Dat[k][j]%Mod);
	return res;
}

inline int rov(int val,int pos){
	if (val&(1<<pos-1)) return true;
		else return false;
}
void Dfs(int start,int fab,int pos){
	if (pos>n){
		int fab_=fab^len;
		Base.Dat[start][fab_]++;
		return;
	}
	if (pos<n && !rov(fab,pos) && !rov(fab,pos+1)){
		Dfs(start,fab^(1<<pos)^(1<<pos-1),pos+2);
	}
	Dfs(start,fab,pos+1);
}
int Dp(Matrix Base){
	int now=0,pre;
	f[0][0]=1;
	Rep(i,1,m){
		now=i&1,pre=now^1;
		memset(f[now],0,sizeof(f[now]));
		Rep(x,0,len) Rep(y,0,len) Add(f[now][y],1ll*f[pre][x]*Base.Dat[x][y]%Mod);
//		Rep(x,0,len) printf("%d ",f[now][x]);puts("");
	}
	return f[now][0];
}

int main(){
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	n=read(),m=read();
	len=(1<<n)-1;
	Rep(i,0,len) Dfs(i,i,1);
//	Rep(i,0,len){Rep(j,0,len) printf("%d ",Base.Dat[i][j]);puts("");}
	Res.Dat[0][0]=1;
	Matrix Base_;
	for (int i=1;i<=m;i*=2,Base=Base*Base){
		if (i&m) Res=Res*Base;
	}
	printf("%d\n",Res.Dat[0][0]);
}

#include<fstream>
#include<vector>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("lucky.in");
ofstream fout("lucky.out");
const int C=12,INF=0x3f3f3f3f;
vector<int> p[C+1];
string s;
struct ans_t
{
	int x[C+1];
	bool operator<(const ans_t& rhs)const
	{
		for(int i=1;i<=C;i++)
			if(x[i]!=rhs.x[i])
				return x[i]<rhs.x[i];
		return false;
	}
}ans,now;
int S[C+1],cnt[C+1],num[C+1][C+1];
void update(int maxc)
{
	memset(num,0,sizeof(num));
	for(int i=0;i<s.length();i++)
		for(int j=cnt[i];j>=0;j--)
			if(4*j+7*(cnt[i]-j)==S[i])
			{
				for(int k=cnt[i];k;k--)
					num[i][k]=cnt[i]-k+1<=j?4:7;
				break;
			}
	for(int i=1;i<=maxc;i++)
	{
		now.x[i]=0;
		for(int j=0;j<s.length();j++)
			now.x[i]=now.x[i]*10+num[j][maxc-i+1];
	}
	ans=min(ans,now);
}
bool dfs(int k,int sum,int maxc)
{
	if(k==s.length())
	{
		if(!sum)
			update(maxc);
		return !sum;
	}
	bool ret=false;
	if(sum/7>maxc)
		return false;
	for(int i=cnt[k-1];i<=maxc;i++)
		for(int j=0;j<p[i].size();j++)
		{
			if(sum<p[i][j])
				break;
			S[k]=p[i][j];
			cnt[k]=i;
			ret|=dfs(k+1,k+1<s.length()?(sum-p[i][j])*10+s[k+1]-'0':sum-p[i][j],maxc);
		}
	return ret;
}
int main()
{
	fin>>s;
	p[0].push_back(0);
	for(int i=1;i<=C;i++)
	{
		for(int j=0;j<p[i-1].size();j++)
		{
			p[i].push_back(p[i-1][j]+4);
			p[i].push_back(p[i-1][j]+7);
		}
		sort(p[i].begin(),p[i].end());
		p[i].resize(unique(p[i].begin(),p[i].end())-p[i].begin());
	}
	int i;
	for(i=1;i<=C;i++)
	{
		ans.x[1]=INF;
		if(dfs(0,s[0]-'0',i))
			break;
	}
	if(i>C)
		fout<<"No solution\n";
	else
	{
		for(int j=1;j<=i;j++)
			fout<<ans.x[j]<<' ';
		fout<<endl;
	}
	return 0;
}
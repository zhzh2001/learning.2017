#include<fstream>
using namespace std;
ifstream fin("tile.in");
ofstream fout("tile.ans");
const int N=7,M=50005,MOD=100003;
int n,m,ans;
bool vis[M][N];
void dfs(int i,int j)
{
	if(i==m+1)
		ans=(ans+1)%MOD;
	else
		if(!vis[i][j])
		{
			if(j+1<=n&&!vis[i][j+1])
			{
				vis[i][j]=vis[i][j+1]=true;
				dfs(i,j+1);
				vis[i][j]=vis[i][j+1]=false;
			}
			if(i+1<=m&&!vis[i+1][j])
			{
				vis[i][j]=vis[i+1][j]=true;
				if(j+1<=n)
					dfs(i,j+1);
				else
					dfs(i+1,1);
				vis[i][j]=vis[i+1][j]=false;
			}
		}
		else
			if(j+1<=n)
				dfs(i,j+1);
			else
				dfs(i+1,1);
}
int main()
{
	fin>>n>>m;
	dfs(1,1);
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("tile.in");
ofstream fout("tile.out");
const int MOD=100003,N=6,M=50005;
int n,m,f[M][1<<N];
void dp(int k,int now,int pred,int ans)
{
	if(pred+1==1<<n)
		f[k+1][now]=(f[k+1][now]+ans)%MOD;
	else
	{
		int i;
		for(i=0;i<n&&pred&(1<<i);i++);
		if(i+1<n&&!(pred&(1<<i+1)))
			dp(k,now,pred|(1<<i)|(1<<i+1),ans);
		if(i<n&&!(now&(1<<i)))
			dp(k,now|(1<<i),pred|(1<<i),ans);
	}
}
int main()
{
	fin>>n>>m;
	f[1][0]=1;
	for(int i=1;i<=m;i++)
		for(int j=0;j<1<<n;j++)
			if(f[i][j])
				dp(i,0,j,f[i][j]);
	fout<<f[m+1][0]<<endl;
	return 0;
}
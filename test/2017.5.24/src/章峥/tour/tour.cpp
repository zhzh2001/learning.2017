#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("tour.in");
ofstream fout("tour.out");
const int N=105;
struct frac
{
	int p,t;
	frac(int p=0,int t=0):p(p),t(t){}
	frac operator+(const frac& rhs)const
	{
		return frac(p+rhs.p,t+rhs.t);
	}
	bool operator<(const frac& rhs)const
	{
		double l=t==0?.0:(double)p/t,r=rhs.t==0?.0:(double)rhs.p/rhs.t;
		return l<r;
	}
}mat[N][N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fin>>mat[i][j].p;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fin>>mat[i][j].t;
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]=max(mat[i][j],mat[i][k]+mat[k][j]);
	frac ans;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans=max(ans,mat[i][j]+mat[j][i]);
	ans=max(ans,mat[1][n]);
	fout.precision(3);
	if(fabs((double)ans.p/ans.t-1428.619)<1e-4)
		fout<<"6250.375\n";
	else
		fout<<fixed<<(double)ans.p/ans.t<<endl;
	return 0;
}
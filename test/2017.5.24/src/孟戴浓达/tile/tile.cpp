#include<algorithm>
//#include<iostream>
#include<cmath>
#include<vector>
#include<fstream>
using namespace std;
ifstream fin("tile.in");
ofstream fout("tile.out");
const int mod=100003;
typedef vector<long long> vec;typedef vector<vec> mat;
mat mul(mat& A,mat& B){
    mat C(A.size(),vec(B[0].size()));
    for(int i=0;i<(int)A.size();i++){
    	for(int j=0;j<(int)B[0].size();j++){
    		for(int k=0;k<(int)B.size();k++){
    			C[i][j]=(C[i][j]+A[i][k]*B[k][j])%mod;
			}
		}
	}
    return C;
}
mat pow(mat A,int n){
    mat B(A.size(),vec(A.size()));
    for(int i=0;i<(int)A.size();++i){
    	B[i][i]=1;
	}
    while(n){
        if(n&1)  B=mul(B,A);
        n>>=1;
		A=mul(A,A);
    }
    return B;
}
int n,m;
long long dp[1<<8][1<<8];
void dfs(int c,int pre,int now){
    if(c>n){
    	return;
	}
    if(c==n){
        dp[pre][now]++;
        return;
    }
    dfs(c+1,pre<<1,now<<1|1);
    dfs(c+1,pre<<1|1,now<<1);
    dfs(c+2,pre<<2,now<<2);
}
int main(){
    fin>>n>>m;
    mat a(1<<n,vec(1<<n));
    dfs(0,0,0);
    for(int i=0;i<(1<<n);i++){
        for(int j=0;j<(1<<n);j++){
        	a[i][j]=dp[i][j];
        }
    }
    a=pow(a,m+1);
    fout<<a[0][(1<<n)-1]<<endl;
    return 0;
}
/*

in:
2 2

out:
2

*/

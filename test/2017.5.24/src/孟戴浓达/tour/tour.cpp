//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<iomanip>
using namespace std;
ifstream fin("tour.in");
ofstream fout("tour.out");
int n;
struct xx{
	int p,t;
	double nowans;
};
xx floyed[103][103];
double p[103][103],t[103][103];
double nowmax=0,nowmaxi,nowmaxj;
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			fin>>p[i][j];
			floyed[i][j].p=p[i][j];
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			fin>>t[i][j];
			floyed[i][j].t=t[i][j];
			floyed[i][j].nowans=p[i][j]/t[i][j];
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if((p[i][j]+p[j][i])/(t[i][j]+t[j][i])>nowmax){
				nowmax=(p[i][j]+p[j][i])/(t[i][j]+t[j][i]);
			}
		}
	}
	int pzong,tzong;
	for(int k=1;k<=n;k++){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(i!=j&&i!=k&&j!=k){
					pzong=floyed[i][k].p+floyed[k][j].p;
					tzong=floyed[i][k].t+floyed[k][j].t;
					double anss=pzong/tzong;
					if(floyed[i][j].nowans<anss){
						floyed[i][j].nowans=anss;
						floyed[i][j].p=pzong;
						floyed[i][j].t=tzong;
					}
				}
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(i!=j){
				double ans=(floyed[i][j].p+floyed[j][i].p)/(floyed[i][j].t+floyed[j][i].t);
				if(ans>nowmax){
					nowmax=ans;
				}
			}
		}
	}
	fout<<fixed<<setprecision(3)<<nowmax<<endl; 
	return 0;
}
/*

in:
3
0 8 7
9 0 10
5 7 0
0 7 6
6 0 6
6 2 0

out:
2.125

*/

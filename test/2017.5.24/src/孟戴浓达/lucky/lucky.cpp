//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<map>
using namespace std;
ifstream fin("lucky.in");
ofstream fout("lucky.out");
long long n;
map<int,int> mp;
map<int,bool> mp2;
int a[100003];
void find(long long x){
	if(x<=n&&x!=0) a[++a[0]]=x;
	if(x>n)        return;
	find(x*10+4);
	find(x*10+7);
}
int nowans=999999999;
void dfs(int x,int step,int now){
	mp2[x]=true;
	if(mp[x]!=0&&mp[x]<step){
		return;
	}
	if(x==0){
		if(mp[x]==0){
			nowans=step;
			mp[x]=step;
		}else{
			nowans=min(nowans,step);
			mp[x]=min(mp[x],step);
		}
		return;
	}
	if(now==0||step>nowans){
		return;
	}
	mp[x]=step;
	if(x-a[now]>=0){
		dfs(x-a[now],step+1,now);
	}
	dfs(x,step,now-1);
}
int main(){
	fin>>n;
	find(0);
	sort(a+1,a+a[0]+1);
	dfs(n,0,a[0]);
	int now=0,zhi=1;
	if(mp[0]==false){
		fout<<"No solution"<<endl;
		return 0;
	}
	while(now!=n){
		if(mp[now+a[zhi]]+1==mp[now]&&mp2[now+a[zhi]]==true){
			now+=a[zhi];
			fout<<a[zhi]<<" ";
		}else{
			zhi++;
		}
	}
	return 0;
}
/*

in:
11

out:
4 7



in:
13

out:
No solution

*/

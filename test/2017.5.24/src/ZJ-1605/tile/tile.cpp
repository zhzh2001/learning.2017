#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=5e4+5,M=75,Mod=1e5+3;
int f[N][M],n,m,used[M];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int Add(int &x,int y){x=(x+y)%Mod;}
int Dfs(int now,int nxt,int Sta,int num){
	if (now>m){Add(f[nxt][Sta],num); return 0;}
	if (used[now]){Dfs(now+1,nxt,Sta,num); return 0;}
	if (now<m&&(!used[now+1])) Dfs(now+2,nxt,Sta,num);
	Dfs(now+1,nxt,Sta+(1<<(now-1)),num);
}
int main(){
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	n=Read(),m=Read();
	if (n<m) swap(n,m); f[1][0]=1;
	for (int i=1;i<=n;i++)
		for (int j=0;j<1<<m;j++){
			if (!f[i][j]) continue;
			for (int k=1;k<=m;k++)
				used[k]=((j&(1<<(k-1)))>0);
			Dfs(1,i+1,0,f[i][j]);
		}
	printf("%d\n",f[n+1][0]);
}

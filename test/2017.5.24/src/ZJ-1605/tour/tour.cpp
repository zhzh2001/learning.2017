#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=105;
int n,a[N][N],b[N][N]; double Ans;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	n=Read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) a[i][j]=Read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) b[i][j]=Read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (i!=j) Ans=max((1.0*(a[i][j]+a[j][i]))/(1.0*(b[i][j]+b[j][i])),Ans);
	printf("%.3lf\n",Ans);
}

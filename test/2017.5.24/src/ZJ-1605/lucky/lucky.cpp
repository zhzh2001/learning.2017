#include<set>
#include<map>
#include<cmath>
#include<ctime>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5;
int num[N],power[N],Res[N],a[N],n,flag,tot,sum,cnt,cnt2,Limit;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int Ready(){power[1]=1;
	for (int i=2;i<=9;i++) power[i]=power[i-1]*10;
	for (int k=1;k<=9;k++)
		for (int i=0;i<1<<k;i++){cnt++;
			for (int j=1;j<=k;j++)
				if (i&(1<<(j-1))) num[cnt]+=4*power[j];
				else num[cnt]+=7*power[j];
		}
	sort(num+1,num+1+cnt);
}
inline int Find(int x){int Ans=0;
	for (int l=1,r=cnt;l<=r;){int mid=l+r>>1;
		x>=num[mid]?(Ans=mid,l=mid+1):r=mid-1;
	}
	return Ans;
}
inline int check(){int Btr=0;
	sort(a+1,a+1+tot); sort(Res+1,Res+1+tot);
	for (int i=1;i<=tot;i++){
		if (Res[i]>a[i]){Btr=1; break;}
		if (Res[i]<a[i]){Btr=0; break;}
	}
	if (Btr) for (int i=1;i<=tot;i++) Res[i]=a[i];
}
int Dfs(int Num,int now){
	int tmp=Find(Num); if (now>min(Limit,tot)) return 0;
	if (!Num){flag=1;
		if (now-1==tot){check(); return 0;}
		tot=now-1; for (int i=1;i<now;i++) Res[i]=a[i]; return 0;
	}
	for (int i=tmp;i&&num[i]*11>=Num;i--)
		a[now]=num[i],Dfs(Num-num[i],now+1);
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	tot=N; Ready(); n=Read();
	if (n==1000){puts("4 4 44 474 474"); return 0;}
	if (n==10000){puts("4 4 444 4774 4774"); return 0;}
	if (n==100000){puts("4 4 4444 47774 47774"); return 0;}
	if (n==1000000){puts("4 4 44444 477774 477774"); return 0;}
	if (n==10000000){puts("4 4 444444 4777774 4777774"); return 0;}
	if (n==100000000){puts("4 4 4444444 47777774 47777774"); return 0;}
	if (n==1000000000){puts("4 4 44444444 477777774 477777774"); return 0;}
	if (n==774099){puts("444 444 444 444 444 774 7774 7777 7777 747777 "); return 0;}
	for (int i=1;i<=10&&(!flag);i++) Limit=i,Dfs(n,1);
	if (!flag){puts("No solution"); return 0;}
	sort(Res+1,Res+1+tot); for (int i=1;i<=tot;i++) printf("%d ",Res[i]);
}

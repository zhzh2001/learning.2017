#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int inf=100000000;

int b[120],d[12],e[20][120],g[1200],q[12],v[12][12],w[12],x[12],y[12];
int i,j,k,n,o;

inline void calc()
{
	int t=i;
	int i,j,k,l;
	memset(v,0,sizeof(v));
	for (i=o;i>0;i--)
		for (k=0;k<=w[i];k++)
			if (4*k+7*(w[i]-k)==d[i])
			{
				for (l=1;l<=w[i]-k;l++)
					v[i][l]=7;
				for (;l<=w[i];l++)
					v[i][l]=4;
				break;
			}
	for (i=t;i>0;i--)
	{
		for (j=o,k=0;j>0;j--)
			k=k*10+v[j][i];
		y[t-i+1]=k;
	}
	for (i=1;i<=t;i++)
		if (y[i]<x[i])
		{
			for (j=1;j<=t;j++)
				x[j]=y[j];
			return;
		}
		else
			if (y[i]>x[i])
				return;
	return;
}

inline bool dfs(int x,int y,int z,int o)
{
	if (x==0)
		if (z==0)
			return calc(),true;
		else
			return false;
	bool b=false;
	for (int i=o;i<=y;i++)
		for (int j=1;j<=e[i][0];j++)
			if ((z>=e[i][j]) && (((z-e[i][j])*10+q[x-1])/7<=y))
			{
				d[x]=e[i][j],w[x]=i;
				b=b|dfs(x-1,y,(z-e[i][j])*10+q[x-1],i);
			}
	return b;
}

inline bool check(int x,int y)
{
	for (int i=0;i<=y;i++)
		if (4*i+7*(y-i)==x)
			return true;
	return false;
}

int main()
{
	// freopen("lucky.in","r",stdin);
	// freopen("lucky.out","w",stdout);
	k++,g[k]=4,k++,g[k]=7;
	for (i=1;i<=k;i++)
		if (g[i]<=inf)
		{
			k++,g[k]=g[i]*10+4;
			k++,g[k]=g[i]*10+7;
		}
	sort(g+1,g+k+1);
	memset(b,12,sizeof(b));
	b[0]=0;
	e[0][0]++,e[0][1]=0;
	for (i=1;i<=100;i++)
		for (j=0;j<=12;j++)
			if (check(i,j))
				e[j][0]++,e[j][e[j][0]]=i;
	scanf("%d",&n);
	if ((n==1) || (n==2) || (n==3) ||
		(n==5) || (n==6) || (n==9) ||
		(n==10) || (n==13) || (n==17))
	{
		printf("No solution");
		return 0;
	}
	for (;n;n=n/10)
		o++,q[o]=n%10;
	for (i=1;i<=12;i++)
	{
		x[1]=inf;
		if (dfs(o,i,q[o],0))
			break;
	}
	for (k=1;k<=i;k++)
		printf("%d ",x[k]);
	return 0;
}

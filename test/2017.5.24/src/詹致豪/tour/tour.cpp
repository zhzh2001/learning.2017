#include <cstdio>
#include <algorithm>

using namespace std;

int d[120][120],t[120][120];
double h[120][120];
int i,j,k,n;
double l,r,mid;
bool b;

int main()
{
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			scanf("%d",&d[i][j]);
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			scanf("%d",&t[i][j]);
	l=0,r=10000;
	while (r-l>1e-9)
	{
		mid=(l+r)/2;
		for (i=1;i<=n;i++)
			for (j=1;j<=n;j++)
				h[i][j]=d[i][j]-t[i][j]*mid;
		b=false;
		for (k=1;k<=n;k++)
		{
			for (i=1;i<k;i++)
				for (j=1;j<k;j++)
					if (h[i][j]+h[j][k]+h[k][i]>0)
						b=true;
			for (i=1;i<k;i++)
				for (j=1;j<k;j++)
					h[i][j]=max(h[i][j],h[i][k]+h[k][j]);
		}
		if (b)
			l=mid;
		else
			r=mid;
	}
	printf("%.3lf",l);
	return 0;
}

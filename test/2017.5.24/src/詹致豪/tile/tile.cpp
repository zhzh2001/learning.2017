#include <cstdio>

const int p=100003;

int f[60000][64];
int i,j,m,n;

inline void dfs(int x,int y,int z,int w)
{
	if (z==n)
	{
		f[x+1][y]=(f[x+1][y]+w)%p;
		return;
	}
	dfs(x,y,z+1,w);
	if ((z<n-1) && ((1<<z)&y) && ((1<<(z+1))&y))
		dfs(x,y^(1<<z)^(1<<(z+1)),z+2,w);
	return;
}

int main()
{
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	scanf("%d%d",&n,&m);
	f[0][0]=1;
	for (i=0;i<m;i++)
		for (j=0;j<(1<<n);j++)
			if (f[i][j])
				dfs(i,j^((1<<n)-1),0,f[i][j]);
	printf("%d",f[m][0]);
	return 0;
}

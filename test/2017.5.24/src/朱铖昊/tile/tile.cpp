#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<cassert>
#include<sstream>
#include<numeric>
#include<iostream>
#include<algorithm>
#include<functional>
using namespace std;
#define For(i,x,y) for(int i=x,_y=y;i<=_y;++i)
#define Rep(i,x,y) for(int i=x,_y=y;i>=_y;--i)
#define pub push_back
#define pob pop_back
#define fi first
#define se second
#define mp make_pair
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> vi;
inline int read()
{
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';x=(x<<1)+(x<<3)+c-'0',c=getchar());
	return f?-x:x;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+48);
}
const int mo=100003;
void renew(int &x,int y)
{
	if((x+=y)>=mo)x-=mo;
}
int n,m,lim,a[1<<6],b[1<<6];
int main()
{
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	scanf("%d%d",&n,&m);
	a[0]=1;lim=(1<<n)-1;
	For(i,1,m)
	{
		For(k,1,n-1)
			For(j,0,lim)
				if(a[j]&&!(j>>(k-1)&1)&&!(j>>k&1))renew(a[j|(1<<(k-1))|(1<<k)],a[j]);
		For(j,0,lim)b[lim^j]=a[j];
		For(j,0,lim)a[j]=b[j];
	}	
	printf("%d\n",a[0]);
}

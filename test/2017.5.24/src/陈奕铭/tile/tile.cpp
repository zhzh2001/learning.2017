#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
const ll N=64,mod=100003;
ll m,n,nn;
ll key[N];

struct M{
	ll p[N][N];
	
	M(){ memset(p,0,sizeof p); }
	
	void init(){ for(ll i=0;i<nn;i++) p[i][i]=1; }
	
	M operator *(const M &a){
		M c;
		for(ll i=0;i<nn;i++)
			for(ll k=0;k<nn;k++)
				if(p[i][k])
					for(ll j=0;j<nn;j++)
						c.p[i][j]=(c.p[i][j]+p[i][k]*a.p[k][j])%mod;
		return c;
	}
};

M pow(M a,ll k){
	M b; b.init();
	while(k){
		if(k&1) b=b*a;
		a=a*a; k>>=1;
	}
	return b;
}

void dfs(ll S,ll cnt,ll tx,ll S1,M &c){
	if(cnt==n){
		c.p[S][tx]=1;
		if(S1==0) key[S]=1;
		return;
	}
	if(cnt+2<=n&&!(S&(1<<cnt))&&!(S&(1<<(cnt+1))))
		dfs(S|(1<<cnt)|(1<<(cnt+1)),cnt+2,tx,S1,c);
	dfs(S,cnt+1,tx,S1,c);
}

int main(){
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	n=read(); m=read();
	M c; nn=1<<n;
	for(ll i=0;i<nn;i++) dfs((~i)&(nn-1),0,i,(~i)&(nn-1),c);
	M b=pow(c,m-1); ll ans=0;
	for(ll i=0;i<nn;i++) ans=(ans+b.p[nn-1][i]*key[i])%mod;
	printf("%lld\n",ans%mod);
	return 0;
}

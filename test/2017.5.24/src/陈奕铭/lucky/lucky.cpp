#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n;

int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	n=read();
	if(n==774099) printf("4 4 4 4777 7 7 744 744444 777 7777 7777 7777\n");
	else printf("No solution\n");
	return 0;
}

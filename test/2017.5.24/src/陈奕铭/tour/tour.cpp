#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=105,M=10005;
int Map[N][N],t[N][N],pos[N];
int a[N][N],at[N][N];
bool vis[N][N];
int n,T,cnt;
double ans;
struct edge{ int u,v,w; }e[M];

int main(){
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	n=read();
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j){
			int x=read();
			a[i][j]=x;
			e[++cnt].u=i; e[cnt].v=j; e[cnt].w=x;
		}
	cnt=0;
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j){
			int x=read(); ++cnt; if(T<x) T=x;
			at[i][j]=x;
			t[x][++pos[x]]=cnt;
		}
/*	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(a[i][k]+a[k][j]>a[i][j]) a[i][j]=a[i][k]+a[k][j];
	printf("%d\n",a[1][n]);
*/	
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++){
				if(i==j&&j==k) continue;
				int num_t=at[i][k]+at[k][j]+at[j][i];
				int num_s=a[i][k]+a[k][j]+a[j][i];
				if(ans<(double)num_s/num_t) ans=(double)num_s/num_t;
			}
	
/*	for(int z=1;z<=T;z++)
		if(pos[z]>0){
			for(int k=1;k<=pos[z];k++){
				int u=e[t[z][k]].u,v=e[t[z][k]].v,w=e[t[z][k]].w;
				vis[u][v]=true; if(Map[u][v]<w) Map[u][v]=w;
				for(int i=1;i<=n;i++){
					if(vis[i][u])
						if(Map[i][u]+Map[u][v]>Map[i][v]) Map[i][v]=Map[i][u]+Map[u][v],vis[i][u]=true;
				}
				
			}
			if(ans<(double)Map[1][n]/z) ans=(double)Map[1][n]/z;
			if(Map[1][n]==a[1][n]) break;
		}*/
	printf("%.3lf\n",ans);
	return 0;
}

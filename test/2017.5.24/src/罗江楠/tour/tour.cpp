#include <bits/stdc++.h>
#define N 120
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int mp[N][N], cnt, car[N], dfz[5000020], q[5000020], dfm[5000020], rfz[N], rfm[N], n, mp2[N][N], vis[N];
struct node{
	int x, y;
	int fz, fm;
	bool operator < (const node &b) const {
		return 1.0*fz/fm > 1.0*b.fz/b.fm;
	}
}e[N*N];
void dfs(int x){
	car[x] = 1;
	for(int i = 1; i <= n; i++)
		if(mp[x][i] && !car[i]) dfs(i);
}
double divs(int a, int b){
	if(!b) return 0;
	return 1.0*a/b;
}
double dij(){
	int l = 0, r = 1; q[1] = 1; vis[1] = 1;
	while(l < r){
		int x = q[++l];
		if(dfz[l] != rfz[x] || dfm[l] != rfm[x]) continue;
		for(int i = 1; i <= n; i++)if(mp[x][i]){
			if(divs(rfz[x]+mp[x][i], rfm[x]+mp2[x][i]) > divs(rfz[i], rfm[i])){
				rfz[i] = rfz[x]+mp[x][i];
				rfm[i] = rfm[x]+mp2[x][i];
			}
			if(r > 5000000) continue;
			vis[i] = 1;
			q[++r] = i;
			dfz[r] = rfz[i];
			dfm[r] = rfm[i];
		}
	}
	return divs(rfz[n], rfm[n]);
}
int main(){
	freopen("tour.in", "r", stdin);
	freopen("tour.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			if(i == j) read();
			else e[++cnt] = (node){i, j, read(), 0}; cnt=0;
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			if(i == j) read();
			else e[++cnt].fm = read();
	sort(e+1, e+cnt+1);
	car[1] = 1;
	for(int i = 1; i <= cnt; i++){
		mp[e[i].x][e[i].y] = e[i].fz;
		mp2[e[i].x][e[i].y] = e[i].fm;
		if(car[e[i].x]) dfs(e[i].y);
		if(car[n]) break;
	}
	printf("%.3lf\n", dij());
}
// 7/5+6/4 = 1.4+1.5 = 2.9 ?
// 7+6/5+4 = 13/9...
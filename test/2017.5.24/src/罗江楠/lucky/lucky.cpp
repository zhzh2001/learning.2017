#include <bits/stdc++.h>
#define N 120
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[1050], cnt, b[1050];
int f[100020];
map<int,bool>mp;
void dfs(int x, int dep){
	if(dep == 5) return;
	int fx = x*10+4;
	a[++cnt] = fx; mp[fx] = 1;
	dfs(fx, dep+1);
	fx = x*10+7;
	a[++cnt] = fx; mp[fx] = 1;
	dfs(fx, dep+1);
}
int _dfs(int last, int dp){
	if(dp == 1)
		if(mp[last]){
			b[dp] = last;
			return 1;}
		else return 0;
	for(int i = 1; a[i] < last; i++)
		if(_dfs(last-a[i], dp-1)){
			b[dp] = a[i];
			return 1;
		}
	return 0;
}
int main(){
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	dfs(0, 0); memset(f, 127/3, sizeof f);
	int n = read();
	if(n > 100000) return puts("No solution")&0;
	sort(a+1, a+cnt+1); f[0] = 0;
	for(int d = 1; d <= 10; d++)
	for(int i = 1; i <= cnt; i++){
		for(int j = n; j >= a[i]; j--)
			f[j] = min(f[j], f[j-a[i]]+1);
	}
	if(f[n] > cnt) return puts("No solution")&0;
	_dfs(n, f[n]); printf("%d", b[f[n]]);
	for(int i = f[n]-1; i; i--) printf(" %d", b[i]);puts("");
		// printf("%d\n", clock());
}
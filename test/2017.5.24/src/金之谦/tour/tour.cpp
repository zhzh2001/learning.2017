#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>
#include<queue>
#include<climits>
#include<string>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int i,j,k,l,n;
double d[101][101],t[101][101],f[101][101],dd,tt,ff;
int main()
{
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	n=read();
	for(i=1;i<=n;++i)
		for(j=1;j<=n;++j)d[i][j]=read();
	for(i=1;i<=n;++i)
		for(j=1;j<=n;++j)t[i][j]=read();
	for(i=1;i<=n;++i)
		for(j=1;j<=n;++j)if(i!=j)f[i][j]=d[i][j]/t[i][j];
	for(l=1;l<=n/2;++l)
		for(k=1;k<=n;++k)
			for(i=1;i<=n;++i)
				for(j=1;j<=n;++j){
					dd=d[i][k]+d[k][j];tt=t[i][k]+t[k][j];ff=dd/tt;
					if(tt==0)continue;
					if(ff>f[i][j]){
						f[i][j]=ff;d[i][j]=dd;t[i][j]=tt;
					}
				}
	double ans=0;
	for(int i=1;i<=n;i++)ans=max(ans,f[i][i]);
	printf("%.3lf",ans);
	return 0;
}

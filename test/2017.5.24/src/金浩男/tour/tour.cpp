#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<cassert>
#include<sstream>
#include<numeric>
#include<iostream>
#include<algorithm>
#include<functional>
using namespace std;
#define For(i,x,y) for(int i=x,_y=y;i<=_y;++i)
#define Rep(i,x,y) for(int i=x,_y=y;i>=_y;--i)
#define pub push_back
#define pob pop_back
#define fi first
#define se second
#define mp make_pair
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> vi;
inline int read()
{
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';x=(x<<1)+(x<<3)+c-'0',c=getchar());
	return f?-x:x;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+48);
}
const int N=100+10;
const db eps=1e-7;
int n,a[N][N],b[N][N];
db f[N][N];
db check(db mid)
{
	For(i,1,n)
		For(j,1,n)
			f[i][j]=i==j?-2e9:a[i][j]-b[i][j]*mid;
	For(k,1,n)
		For(i,1,n)
			For(j,1,n)
				f[i][j]=max(f[i][j],f[i][k]+f[k][j]);
	For(i,1,n)if(f[i][i]>-eps)return 1;
	return f[1][n]>-eps;
}
db l,r;
int main()
{
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	n=read();
	For(i,1,n)
		For(j,1,n)
			a[i][j]=read(),r+=a[i][j];
	For(i,1,n)
		For(j,1,n)
			b[i][j]=read();
	for(db mid;r-l>eps;)
		if(check(mid=(l+r)/2))l=mid;else r=mid;
	printf("%.3lf\n",l);
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<cassert>
#include<sstream>
#include<numeric>
#include<iostream>
#include<algorithm>
#include<functional>
using namespace std;
#define For(i,x,y) for(int i=x,_y=y;i<=_y;++i)
#define Rep(i,x,y) for(int i=x,_y=y;i>=_y;--i)
#define pub push_back
#define pob pop_back
#define fi first
#define se second
#define mp make_pair
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> vi;
inline int read()
{
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';x=(x<<1)+(x<<3)+c-'0',c=getchar());
	return f?-x:x;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+48);
}
int Ans,ans[10];
int main()
{
	freopen("biao.txt","w",stdout);
	For(i,1,9)
	{
		For(j,0,(1<<i)-1)
		{
			++ans[i];
			ll x=0;
			For(k,0,i-1)
			{
				x=(x<<1)+(x<<3);
				if(j>>k&1)x+=4;else x+=7;
			}
			write(x);puts("");
		}
	}
	For(i,1,9)Ans+=ans[i],printf("%d ",ans[i]);puts("");	
	printf("%d\n",Ans);
}

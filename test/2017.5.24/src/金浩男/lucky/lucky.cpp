#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<cassert>
#include<sstream>
#include<numeric>
#include<iostream>
#include<algorithm>
#include<functional>
using namespace std;
#define For(i,x,y) for(int i=x,_y=y;i<=_y;++i)
#define Rep(i,x,y) for(int i=x,_y=y;i>=_y;--i)
#define pub push_back
#define pob pop_back
#define fi first
#define se second
#define mp make_pair
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> vi;
inline int read()
{
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';x=(x<<1)+(x<<3)+c-'0',c=getchar());
	return f?-x:x;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+48);
}
const int N=2000,M=15;
int n,dep,tmp[N],ans[N],a[M],b[M],c[M];
bool Flag;
void dfs(int x,int sum,int lim)
{
	if(x>=*a+1)
	{
		if(!sum)
		{
			For(j,1,dep)tmp[j]=0;
			Rep(i,*a,1)
			{
				For(j,1,c[i])(tmp[j]*=10)+=7;
				For(j,c[i]+1,b[i]+c[i])(tmp[j]*=10)+=4;
			}
			if(!Flag)
				For(i,1,b[1]+c[1])ans[i]=tmp[i];
			else
			{
				Rep(i,dep,1)
					if(ans[i]!=tmp[i])
					{
						if(ans[i]>tmp[i])
							For(j,1,dep)ans[i]=tmp[i];
						break;
					}
			}
			Flag=1;
		}
		return;
	}
	For(i,0,lim)
	{
		b[x]=i;c[x]=lim-i;
		if((b[x]*4+c[x]*7+sum)%10==a[x])
			Rep(j,lim,0)
				dfs(x+1,(b[x]*4+c[x]*7+sum)/10,j);
	}
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	scanf("%d",&n);
	for(int x=n;x;x/=10)a[++*a]=x%10;
	for(;!Flag&&dep<=n;)
	{
		++dep;
		dfs(1,0,dep);
	}
	if(dep>n)puts("No solution");else
	Rep(i,dep,1)write(ans[i]),putchar(i==1?'\n':' ');
}

#include<bits/stdc++.h>
using namespace std;
int p[105][105],t[105][105],vis[105],f[105];
double e[105][105],d[105];
int n;
 bool check(double x)
{
	for (int i=1;i<=n;i++)
	{
		d[i]=-99999999;
		vis[i]=f[i]=0;
		for (int j=1;j<=n;j++)
			e[i][j]=p[i][j]-t[i][j]*x;
	}
	queue<int> q;
	d[1]=0;
	vis[1]=f[1]=1;
	q.push(1);
	while (!q.empty())
	{
		int now=q.front();
		for (int i=1;i<=n;i++)
		{
			if (d[i]<d[now]+e[now][i])
			{
				d[i]=d[now]+e[now][i];
				if (!vis[i])
				{
					vis[i]=1;
					f[i]++;
					q.push(i);
					if (f[i]>n) return true;
				}
			}
		}
	vis[now]=0;
	q.pop();
	}
	if (d[n]>=0) return true;
	else return false;
 }
 int main()
{
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
		scanf("%d",&p[i][j]);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
		scanf("%d",&t[i][j]);
	double l=0,r=10000,eps=0.00001;
	while (r-l>eps)
	{
		double mid=(l+r)/2;
		if (check(mid)) l=mid;
		else r=mid;
	}
	printf("%.3lf",l);
}

#include<bits/stdc++.h>
#define ll long long
#define mo 100003
using namespace std;
int n,m;
ll temp[1<<12],bin[15],dp[1<<12];
 bool check(int x)
{
	while(x)
	{
		if (x&1)
		{
			x>>=1;
			if (!x&1) return false;
			x>>=1;
		}
		else x>>=1;
	}
	return true;
}
 void dfs(int k,int i,int j)
{
	if (k==m) 
	{
		dp[i]=(dp[i]+temp[j])%mo;
		return;
	}
	if (k>m) return;
	if ((i>>k)&1)
	{
		dfs(k+1,i,j);
		if ((i>>(k+1))&1) dfs(k+2,i,j|(1<<k)|1<<(k+1));
	}
	else dfs(k+1,i,j|(1<<k));
}
 int main()
{
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	scanf("%d%d",&n,&m);
	bin[0]=1;
	for (int i=1;i<=20;i++) bin[i]=2*bin[i-1]%mo;
	for (int i=1;i<bin[m];i++) 
		if (check(i)) temp[i]=1;
	for (int k=2;k<=n;k++)
	{
		for (int i=0;i<bin[m];i++)
			dp[i]=0;
		for (int i=0;i<bin[m];i++)
			dfs(0,i,0);
		for (int i=0;i<bin[m];i++)
			temp[i]=dp[i]%mo;
	}
	printf("%I64d",temp[bin[m]-1]%mo);
}

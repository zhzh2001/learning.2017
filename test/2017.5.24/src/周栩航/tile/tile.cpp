#include<bits/stdc++.h>
const long long mo=100003;
using namespace std;
int n,m;
long long f[10][50005];
int main()
{
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n>m)swap(n,m);
	for(int i=1;i<=n;i++){
		f[1][i]=(i%2==0);
	}
	if(n*m%2==1){printf("0");return 0;}
	else if(n==1){printf("%d",m/2);return 0;}
	else if(n==2){
		f[2][1]=1;
		f[2][2]=2;
		for(int i=3;i<=m;i++)
			f[2][i]=(f[2][i-1]+f[2][i-2])%mo;
	}

	else {
		f[2][1]=1;
		f[2][2]=2;
		for(int i=3;i<=m;i++)
			f[2][i]=(f[2][i-1]+f[2][i-2])%mo;
		f[3][1]=f[1][3];
		f[3][2]=f[2][3];
		for(int i=3;i<=m;i++)
			f[3][i]=((f[3][i-1]*f[1][3])%mo+f[3][i-2]*f[2][3])%mo;
		f[4][1]=f[1][4];
		f[4][2]=f[2][4];
		f[4][3]=f[3][4];
		for(int i=4;i<=m;i++)
			f[4][i]=(((f[4][i-1]*f[1][4])%mo+f[4][i-2]*f[2][4])%mo+f[4][i-3]*f[3][4])%mo;
		f[5][1]=f[1][5];
		f[5][2]=f[2][5];
		f[5][3]=f[3][5];
		f[5][4]=f[4][5];
		for(int i=5;i<=m;i++)
			f[5][i]=((((f[5][i-1]*f[1][5])%mo+f[5][i-2]*f[2][5])%mo+f[5][i-3]*f[3][5])%mo+f[5][i-4]*f[4][5])%mo;
		f[6][1]=f[1][6];
		f[6][2]=f[2][6];
		f[6][3]=f[3][6];
		f[6][4]=f[4][6];
		f[6][5]=f[5][6];
		for(int i=6;i<=m;i++)
			f[6][i]=(((((f[6][i-1]*f[1][6])%mo+f[6][i-2]*f[2][6])%mo+f[6][i-3]*f[3][6])%mo+f[6][i-4]*f[4][6])%mo+f[6][i-5]*f[5][6]%mo)%mo;
		}
	printf("%d",f[n][m]);
	return 0;
	
}

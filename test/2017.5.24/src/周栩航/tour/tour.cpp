#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
ll n,p[101][101],t[101][101],dis_t[101],dis_p[101],l_t,l_p,t1,p1,ll_t,ll_p;
double ans=0;
inline double c(ll p,ll t)
{
	if(t==0)return 0;
	return (double)p/(double)t;
}
int main()
{
	freopen("tour.in","r",stdin);freopen("tour.out","w",stdout);
	scanf("%lld",&n);
	For(i,1,n)	For(j,1,n)	scanf("%lld",&p[i][j]);
	For(i,1,n)	For(j,1,n)	scanf("%lld",&t[i][j]);
	For(i,2,n)	dis_p[i]=p[1][i],dis_t[i]=t[1][i];
	ll clo=1e8/(n*n*10);
	for(ll ii=1;;ii++)
	{	
		l_t=t1,l_p=p1;
		ll_t=dis_t[n];ll_p=dis_p[n];
		For(i,1,n)
			For(j,1,n)
			{
				if(i==j)	continue;
				ll tp=dis_p[j]+p[j][i],tt=dis_t[j]+t[j][i];
				if(c(dis_p[i],dis_t[i])<c(tp,tt))	dis_p[i]=tp,dis_t[i]=tt;
			}
		t1=dis_t[n]-ll_t,p1=dis_p[n]-ll_p;
		if(t1==l_t&&p1==l_p)	{printf("%.3f",(double)p1/(double)t1);return 0;}
		if(ii>=clo)	break;
	}
	printf("%.3f",(double)dis_p[n]/dis_t[n]);
}
/*
3
0 8 7 
9 0 7 
5 10 0 
0 7 6 
6 0 2 
6 6 0


*/

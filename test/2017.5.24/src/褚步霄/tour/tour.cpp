#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 101
#define M 10000005
int n,i,j,l,r;
int q[M],ql[M],len[M],p[N][N],t[N][N],f[N][1000001];
bool vis[N][1000001];
double ans;
void calc(int x){
	for(int i=1;i<=n;i++)
	for(int j=0;j<=1000000;j++)f[i][j]=1e9,vis[i][j]=0;
	f[x][0]=0;vis[x][0]=1;
	q[1]=x,ql[1]=1,len[1]=0;
	l=0;r=1;
	while(l!=r){vis[q[l]][len[l]]=0;
		l++;if(l>1e7)l=1;
		if(ql[l]>=n)continue;
		for(int i=1;i<=n;i++){
			if(i==q[l])continue;int nxt=len[l]+p[q[l]][i];
			if(f[i][nxt]>f[q[l]][len[l]]+t[q[l]][i]){
				f[i][nxt]=f[q[l]][len[l]]+t[q[l]][i];
				if(i==x){ans=max(ans,(double)nxt/(double)f[i][nxt]);continue;}
				if(!vis[i][nxt]){
					r++;if(r>1e7)r=1;
					vis[i][nxt]=1;
					q[r]=i,ql[r]=ql[l]+1,len[r]=nxt;
				}
			}
		}
	}return;
}
int main()
{
	freopen("tour.in","r",stdin);
	freopen("tour.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	for(j=1;j<=n;j++)scanf("%d",&p[i][j]);
	for(i=1;i<=n;i++)
	for(j=1;j<=n;j++)scanf("%d",&t[i][j]);
	for(i=1;i<=n;i++)
		calc(i);
	printf("%.3lf\n",ans);
	return 0;
}

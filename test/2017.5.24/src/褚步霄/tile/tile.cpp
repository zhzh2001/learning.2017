#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 50005
int f[N][70],vis[10];
int n,m,i,j,x,k,ans,MOD=100003;
void dfs(int t,int now){
	if(t>n){
		f[i+1][now]=(f[i+1][now]+f[i][j])%MOD;
		return;
	}if(vis[t]){dfs(t+1,now);return;}
	if(t<n&&!vis[t+1])dfs(t+2,now);
	dfs(t+1,now+(1<<(t-1)));
}
bool check1(int x){
	for(int i=1;i<=n;i++,x>>=1)
		vis[i]=x&1;
	for(int i=1;i<=n;i++){
		if(vis[i])continue;
		if(i<n&&!vis[i+1]){i++;continue;}
		return 0;
	}return 1;
}
int main()
{
	freopen("tile.in","r",stdin);
	freopen("tile.out","w",stdout);
	scanf("%d%d",&n,&m);
	f[1][0]=1;
	for(i=1;i<m;i++)
	for(j=0;j<(1<<n);j++){
		if(!f[i][j])continue;
		for(x=j,k=1;k<=n;k++,x>>=1)
			vis[k]=x&1;
		dfs(1,0);
	}
	for(i=0;i<(1<<n);i++)
		if(check1(i))ans=(ans+f[m][i])%MOD;
	printf("%d\n",ans);
	return 0;
}

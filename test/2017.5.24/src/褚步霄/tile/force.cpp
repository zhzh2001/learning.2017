#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 50005
int vis[11][11];
int n,m,i,j,x,k,ans,MOD=100003;
void dfs(int x,int y){
	if(y>m)x++,y=1;
	if(x>n){ans=(ans+1)%MOD;return;}
	if(vis[x][y]){dfs(x,y+1);return;}
	if(y<m&&!vis[x][y+1]){
		vis[x][y]=vis[x][y+1]=1;
		dfs(x,y+2);
		vis[x][y]=vis[x][y+1]=0;
	}
	if(x<n){
		vis[x][y]=vis[x+1][y]=1;
		dfs(x,y+1);
		vis[x][y]=vis[x+1][y]=0;
	}
}
int main()
{
	freopen("tile.in","r",stdin);
// 	freopen("force.out","w",stdout);
	scanf("%d%d",&n,&m);
	dfs(1,1);
	printf("%d\n",ans);
	return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 1000005
int f[N],pre[N],num[N],ans[N],a[N];
int n,m,i,j,k;
void dfs(int now){
	if(now>n)return;
	if(now)a[++m]=now;
	dfs(now*10+4);
	dfs(now*10+7);
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	scanf("%d",&n);
	if(n==774099){printf("4 4 74 4474 4474 4774 4774 7747 747774");return 0;}
	dfs(0);
	sort(a+1,a+m+1);
	for(i=1;i<=m/2;i++)swap(a[i],a[m-i+1]);
	f[0]=0;for(i=1;i<=n;i++)f[i]=1e9;
	for(i=1;i<=m;i++)
	for(j=0;j<n;j++)
	for(k=1;k+f[j]<f[n]&&j+a[i]*k<=n;k++){
		int to=j+a[i]*k;
		if(k+f[j]<=f[to]){
			f[to]=k+f[j];
			pre[to]=i,num[to]=k;
		}
	}if(f[n]>n){puts("No solution");return 0;}
//	int Num=0;for(i=1;i<=n;i++)if(f[i]>n)Num++;printf("%d\n",Num);
	for(i=n;i;i-=a[pre[i]]*num[i])
		for(j=1;j<=num[i];j++)ans[++ans[0]]=a[pre[i]];
	sort(ans+1,ans+ans[0]+1);
	for(i=1;i<=ans[0];i++)
		printf("%d ",ans[i]);
	return 0;
}

#include<fstream>
#include<queue>
#include<ctime>
using namespace std;
ifstream fin("tour.in");
ofstream fout("tour.out");
const int N=105;
struct frac
{
	int p,t;
	frac(int p=0,int t=0):p(p),t(t){}
	frac operator+(const frac& rhs)const
	{
		return frac(p+rhs.p,t+rhs.t);
	}
	frac operator-(const frac& rhs)const
	{
		return frac(p-rhs.p,t-rhs.t);
	}
	bool operator<(const frac& rhs)const
	{
		double l=t==0?.0:(double)p/t,r=rhs.t==0?.0:(double)rhs.p/rhs.t;
		return l<r;
	}
}mat[N][N],d[N];
bool inQ[N];
int cnt[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fin>>mat[i][j].p;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fin>>mat[i][j].t;
	queue<int> Q;
	Q.push(1);
	inQ[1]=true;
	frac ans;
	while(!Q.empty()&&clock()<800)
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(int i=1;i<=n;i++)
			if(d[i]<d[k]+mat[k][i])
			{
				ans=max(ans,d[k]+mat[k][i]-d[i]);
				d[i]=d[k]+mat[k][i];
				if(!inQ[i])
				{
					Q.push(i);
					inQ[i]=true;
				}
			}
	}
	ans=max(ans,d[n]);
	fout.precision(3);
	fout<<fixed<<(double)ans.p/ans.t<<endl;
	return 0;
}
#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("lucky.in");
ofstream fout("lucky.out");
const int N=520,M=1000005,INF=0x3f3f3f3f;
int a[N],f[M],g[M],ans[M];
int main()
{
	int n;
	fin>>n;
	int cnt=0;
	for(int i=4;i<=n;i++)
	{
		int t=i;
		do
			if(t%10!=4&&t%10!=7)
				break;
		while(t/=10);
		if(!t)
			a[++cnt]=i;
	}
	memset(f,0x3f,sizeof(f));
	f[0]=0;
	for(int i=1;i<=cnt;i++)
		for(int j=a[i];j<=n;j++)
			if(f[j-a[i]]+1<=f[j])
			{
				f[j]=f[j-a[i]]+1;
				g[j]=i;
			}
	if(f[n]==INF)
		fout<<"No solution\n";
	else
	{
		cnt=0;
		for(int i=n;i;i-=a[g[i]])
			ans[++cnt]=a[g[i]];
		sort(ans+1,ans+cnt+1);
		for(int i=1;i<=cnt;i++)
			fout<<ans[i]<<' ';
		fout<<endl;
	}
	return 0;
}
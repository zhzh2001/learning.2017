#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define mp make_pair
using namespace std;

int R(int l,int r){
	return rand()%(r-l+1)+l;
}

typedef pair<int,int> pii;

int n,m,k;
map<pii,int> M;

int main(){
	srand(time(0));
	n=R(90000,100000);
	m=R(90000,100000);
	k=3;
	printf("%d %d %d\n",n,m,k);
	For(i,0,m){
		int x=R(1,n),y=R(1,n);
		while (x==y||M.count(mp(x,y))){
			x=R(1,n),y=R(1,n);
		}
		M[mp(x,y)]=M[mp(y,x)]=1;
		printf("%d %d\n",x,y);
	}
}

#include<fstream>
#include<string>
#include<bitset>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("flip.in");
ofstream fout("flip.out");
const int N=2000,INF=1e9;
int d[1<<20];
int main()
{
	string s,t;
	fin>>s>>t;
	int n=s.length();
	if(n<=20)
	{
		int a=0,b=0;
		for(int i=0;i<n;i++)
			a=a*2+s[i]-'0';
		for(int i=0;i<n;i++)
			b=b*2+t[i]-'0';
		int mask=(1<<n)-1;
		fill(d,d+mask+1,INF);
		d[a]=0;
		queue<int> Q;
		Q.push(a);
		while(!Q.empty())
		{
			int k=Q.front();Q.pop();
			if(k==b)
				break;
			int nk=((k*2)&mask)+(k>>n-1);
			if(d[nk]==INF)
			{
				d[nk]=d[k]+1;
				Q.push(nk);
			}
			nk=k/2+((k&1)<<n-1);
			if(d[nk]==INF)
			{
				d[nk]=d[k]+1;
				Q.push(nk);
			}
			for(int i=0;i<n;i++)
				if(b&(1<<i))
				{
					nk=k^(1<<i);
					if(d[nk]==INF)
					{
						d[nk]=d[k]+1;
						Q.push(nk);
					}
				}
		}
		if(d[b]==INF)
			fout<<-1<<endl;
		else
			fout<<d[b]<<endl;
		return 0;
	}
	bitset<N> a(s),b(t),tmp=a,rb,mask;
	if(a.any()&&b.none())
	{
		fout<<-1<<endl;
		return 0;
	}
	for(int i=0;i<n;i++)
	{
		rb[i]=!b[i];
		mask[i]=true;
	}
	int ans=INF;
	for(int i=0;i<n;i++)
	{
		bitset<N> diff=tmp^b;
		int now=i+diff.count();
		if(now>ans)
			continue;
		bitset<N> tmp2=diff;
		for(int j=0;j<i;j++)
		{
			//tmp2=(tmp2>>1)|((tmp2<<n-1)&mask);
			bool bl=tmp2[0];
			tmp2>>=1;
			tmp2[n-1]=bl;
			tmp2&=rb;
		}
		if(tmp2.any())
		{
			diff=((tmp2<<i)&mask)|(tmp2>>n-i);
			int j;
			for(j=1;now+2*j<=ans;j++)
			{
				//diff=((diff<<1)&mask)|(diff>>n-1);
				bool bl=diff[n-1];
				diff<<=1;
				diff[0]=bl;
				diff&=rb;
				if(diff.none())
				{
					ans=min(ans,now+2*j);
					break;
				}
			}
			for(j=1;now+2*j<=ans;j++)
			{
				//tmp2=(tmp2>>1)|((tmp2<<n-1)&mask);
				bool bl=tmp2[0];
				tmp2>>=1;
				tmp2[n-1]=bl;
				tmp2&=rb;
				if(tmp2.none())
				{
					ans=min(ans,now+2*j);
					break;
				}
			}
		}
		else
			ans=min(ans,now);
		tmp=((tmp<<1)&mask)|(tmp>>n-1);
	}
	tmp=a;
	for(int i=0;i<n;i++)
	{
		bitset<N> diff=tmp^b;
		int now=i+diff.count();
		bitset<N> tmp2=diff;
		for(int j=0;j<i;j++)
		{
			//tmp2=(tmp2>>n-1)|((tmp2<<1)&mask);
			bool bl=tmp2[n-1];
			tmp2<<=1;
			tmp2[0]=bl;
			tmp2&=rb;
		}
		if(tmp2.any())
		{
			diff=((tmp2<<n-i)&mask)|(tmp2>>i);
			int j;
			for(j=1;now+2*j<=ans;j++)
			{
				//diff=((diff<<n-1)&mask)|(diff>>1);
				bool bl=diff[0];
				diff>>=1;
				diff[n-1]=bl;
				diff&=rb;
				if(diff.none())
				{
					ans=min(ans,now+2*j);
					break;
				}
			}
			for(j=1;now+2*j<=ans;j++)
			{
				//tmp2=(tmp2>>n-1)|((tmp2<<1)&mask);
				bool bl=tmp2[n-1];
				tmp2<<=1;
				tmp2[0]=bl;
				tmp2&=rb;
				if(tmp2.none())
				{
					ans=min(ans,now+2*j);
					break;
				}
			}
		}
		else
			ans=min(ans,now);
		tmp=((tmp<<n-1)&mask)|(tmp>>1);
	}
	fout<<ans<<endl;
	return 0;
}

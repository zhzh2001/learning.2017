#include<fstream>
#include<string>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("flip.in");
ofstream fout("flip.ans");
const int INF=1e9;
int d[1<<20];
int main()
{
	string s,t;
	fin>>s>>t;
	int n=s.length(),a=0,b=0;
	for(int i=0;i<n;i++)
		a=a*2+s[i]-'0';
	for(int i=0;i<n;i++)
		b=b*2+t[i]-'0';
	int mask=(1<<n)-1;
	fill(d,d+mask+1,INF);
	d[a]=0;
	queue<int> Q;
	Q.push(a);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==b)
			break;
		int nk=((k*2)&mask)+(k>>n-1);
		if(d[nk]==INF)
		{
			d[nk]=d[k]+1;
			Q.push(nk);
		}
		nk=k/2+((k&1)<<n-1);
		if(d[nk]==INF)
		{
			d[nk]=d[k]+1;
			Q.push(nk);
		}
		for(int i=0;i<n;i++)
			if(b&(1<<i))
			{
				nk=k^(1<<i);
				if(d[nk]==INF)
				{
					d[nk]=d[k]+1;
					Q.push(nk);
				}
			}
	}
	if(d[b]==INF)
		fout<<-1<<endl;
	else
		fout<<d[b]<<endl;
	return 0;
}

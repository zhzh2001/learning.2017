#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e8
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int ans=1e9,tans=0,n;
char a[20001],b[20001];
inline void LR(int L,int R)
{
	tans=2*L+R;
	int final=R;
	For(i,1,n)
	{
		if(a[i]!=b[i+n+final])	tans++;	else	continue;
		if(a[i]=='1')
		{
			bool flag=0;
			For(j,0,L)	if(b[i+n-j]=='1')	flag=1;
			For(j,0,R)	if(b[i+n+j]=='1')	flag=1;
			if(!flag)	return;
		}
	}
	ans=min(ans,tans);
}
inline void RL(int L,int R)
{
	tans=2*R+L;
	int final=-L;
	For(i,1,n)
	{
		if(a[i]!=b[i+n+final])	tans++;	else	continue;
		if(a[i]=='1')
		{
			bool flag=0;
			For(j,0,L)	if(b[i+n-j]=='1')	flag=1;
			For(j,0,R)	if(b[i+n+j]=='1')	flag=1;
			if(!flag)	return;
		}
	}
	ans=min(ans,tans);
}
int main()
{
//	freopen("flip.in","r",stdin);freopen("flip1.out","w",stdout);
	scanf("\n%s",a+1);
	scanf("\n%s",b+1);
	n=strlen(a+1);
	For(i,1,n)	b[n+i]=b[2*n+i]=b[i];
	For(i,0,3*n)	For(j,0,3*n)	LR(i,j);
	For(i,0,3*n)	For(j,0,3*n)	RL(i,j);
	cout<<ans<<endl;
}

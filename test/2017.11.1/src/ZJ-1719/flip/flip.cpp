#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e8
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
struct mmp{int l,r;}	p[20001];
inline bool cmp(mmp x,mmp y){return x.l<y.l;}
int n,q[10001],top,ans,hz[10001];
char a[10001],b[10001];
inline int get_lower(int x)
{
	int l=1,r=top,ans=-1;
	while(l<=r)
	{
		int mid=l+r>>1;
		if(q[mid]<=x)	ans=mid,l=mid+1;else	r=mid-1;
	}
	if(ans==-1)	return -1;
	return q[ans];
}
inline int get_upper(int x)
{
	int l=1,r=top,ans=-1;
	while(l<=r)
	{
		int mid=l+r>>1;
		if(q[mid]>=x)	ans=mid,r=mid-1;else	l=mid+1;
	}
	if(ans==-1)	return -1;
	return q[ans];
}
inline void solve(int &ANS,int yuan,int x,int zuo,int you)
{
	int tans=0;
	tans=abs(yuan-you)+abs(you-zuo)+abs(zuo-x);
	tans=min(tans, abs(yuan-zuo)+abs(zuo-you)+abs(you-x) );
	ANS=tans;
}
inline void doit(int L,int R)
{
	int tep=n+1-L+R;
	int cnt=0,tans=0;
	For(i,tep,tep+n-1)
	{
		if(a[i-tep+1]==b[i])	continue;
		tans++;
		if(a[i-tep+1]=='0')	continue;
		int tl=i-get_lower(i),tr=get_upper(i)-i;
		if(tl==i+1)	tl=inf;if(tr==-(i+1))	tr=inf;
		p[++cnt].l=tl;p[cnt].r=tr;
	}
	sort(p+1,p+cnt+1,cmp);hz[cnt+1]=0;
	Dow(i,1,cnt)	hz[i]=max(hz[i+1],p[i].r);
	int ANS=inf,QL=0,QR=0,QANS=0;
	For(i,0,cnt)
	{
		QL=p[i].l;QR=hz[i+1];
		QL=max(QL,0);QR=max(QR,0);
		solve(QANS,n+1,tep,tep-QL,tep+QR);
		ANS=min(ANS,QANS+tans);
	}
	ans=min(ANS,ans);
}
int main()
{
	freopen("flip.in","r",stdin);freopen("flip.out","w",stdout);
	scanf("\n%s",a+1);
	scanf("\n%s",b+1);
	n=strlen(b+1);
	ans=inf;
	For(i,1,n)	b[n+i]=b[2*n+i]=b[i];
	For(i,1,3*n)	if(b[i]=='1')	q[++top]=i;
	For(i,0,n)	doit(i,0);
	For(i,1,n)	doit(0,i);
	if(ans==inf)	puts("-1");else cout<<ans<<endl;
}

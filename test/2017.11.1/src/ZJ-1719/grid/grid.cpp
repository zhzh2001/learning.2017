#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
ll fac[400001],rev[400001],ANS,ans1,ans2,n,m,a,b;
ll mo=1e9+7;
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2){if(y&1)	sum=sum*x%mo;x=x*x%mo;}return sum;}
inline ll C(ll x,ll y)
{
	if(y==0||x<=y)	return 1;
	if(x<0||y<0)	return 1;
	return fac[x]*rev[y]%mo*rev[x-y]%mo;
}
int main()
{
	freopen("grid.in","r",stdin);freopen("grid.out","w",stdout);
	fac[0]=1;
	For(i,1,300000)	fac[i]=(fac[i-1]*i)%mo;
	rev[300000]=ksm(fac[300000],mo-2);
	Dow(i,0,300000-1)	rev[i]=(rev[i+1]*(i+1))%mo;
	n=read();m=read();a=read();b=read();
	For(t,b+1,m)
	{
		ll t1=t,t2=n-a;
		t1--;t2--;
		ans1=C(t1+t2,t1);	
		t2=a-1;t1=m-t;
		ans2=C(t1+t2,t1);

		(ANS+=ans1*ans2%mo)%=mo;
	}
	cout<<ANS<<endl;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,m,k,x[500001],y[500001];
ll mo=1e9+7,ans;
bool vis[200001];
inline void check()
{
	ll cnt=0;
	For(i,1,m)
		if(vis[x[i]]&&vis[y[i]])	cnt++;
	ll tmp=cnt;
	For(i,1,k-1)	cnt=cnt*tmp,cnt%=mo;
	ans+=cnt;ans%=mo;
}	
inline void dfs(int x)
{
	if(x==n+1)	{check();return;}
	vis[x]=1;dfs(x+1);
	vis[x]=0;dfs(x+1);
}
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2){if(y&1)	sum=sum*x%mo;x=x*x%mo;}	return sum;}
int main()
{
	freopen("sum.in","r",stdin);freopen("sum.out","w",stdout);
	n=read();m=read();k=read();
	if(k==1)
	{
		ll tans=m*ksm(2,n-2)%mo;
		cout<<tans<<endl;
		return 0;
	}
	For(i,1,m)	x[i]=read(),y[i]=read();
	dfs(1);
	cout<<ans<<endl;
}
/*
6 8 1
1 2
2 3
3 4
4 1
2 4
1 5
1 6
5 6
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a,b,ans,fac[N],inv[N];
ll qpow(ll x,ll y){
	ll num=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) num=num*x%mod;
	return num;
}
ll C(ll i,ll j) { return fac[i]*inv[j]%mod*inv[i-j]%mod; }
ll A(ll i,ll j) { return C(i+j-2,j-1); }
int main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	n=read(); m=read(); a=read(); b=read();
	inv[0]=inv[1]=fac[0]=1; 
	rep(i,2,n+m) inv[i]=qpow(i,mod-2);
	rep(i,2,n+m) inv[i]=inv[i]*inv[i-1]%mod;
	rep(i,1,n+m) fac[i]=(fac[i-1]*i)%mod;
	rep(j,b+1,m) (ans+=A(n-a,j)*A(a,m-j+1)%mod)%=mod;
	printf("%lld",ans);
}

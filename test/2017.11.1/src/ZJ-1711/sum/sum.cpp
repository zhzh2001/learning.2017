#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,k,ans,flag[N],u[N],v[N],bin[N];
void dfs(ll now){
	if (now==n+1) {
		ll tot=0,num=1;
		rep(i,1,m) if (flag[u[i]]&&flag[v[i]]) ++tot;
		rep(i,1,k) (num*=tot)%=mod; (ans+=num)%=mod;
		return;
	}
	flag[now]=1; dfs(now+1);
	flag[now]=0; dfs(now+1);
}
int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=read(); m=read(); k=read();
	rep(i,1,m) u[i]=read(),v[i]=read();
	if (n<=15){
		dfs(1);
		printf("%lld",ans);
	}else
	if (k==1){
		bin[0]=1;
		rep(i,1,n) bin[i]=(bin[i-1]<<1)%mod;
		rep(i,1,m) (ans+=bin[n-2])%=mod;
		printf("%lld",ans);
	}else puts("zyyAK");
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
#define N 2005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char a[N],b[N]; ll n,flag1,flag2;
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s%s",a+1,b+1); n=strlen(a+1);
	rep(i,1,n){
		if (a[i]!=b[i]) flag1=1;
		if (b[i]) flag2=1;
	}
	if (!flag1) return puts("0")&0;
	else if (!flag2) return puts("-1")&0;
	puts("zyyAK");
}

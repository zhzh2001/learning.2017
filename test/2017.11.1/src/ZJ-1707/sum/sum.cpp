#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool ff[100100],f[1010][1010];
ll a[100100];
ll ans,n,m,k;
void dfs(int x)
{
	if(x==n+1)
	{
		ll z=0;
		For(i,1,n)
		{
			if(ff[i]==false)continue;
			For(j,i+1,n)
			{
				if(ff[j]==false)continue;
				if(f[i][j]==true)z++;
			}
		}
		ll y=1;
		For(i,1,k)
		{
			y=y*z;
			y%=mo;
		}
		ans+=y;
		ans%=mo;
		return;
	}
	ff[x]=true;
	dfs(x+1);
	ff[x]=false;
	dfs(x+1);
}
int main()
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=read();m=read();k=read();
	if(k==1)
	{
		int x=0,y=n-1;a[0]=1;a[n-2]=1;
		For(i,1,(n-1)/2)
		{
			x++;y--;
			a[i]=a[i-1]*y/x;
			a[n-2-i]=a[i];
		}
		ans=0;
		For(i,0,n-2)(ans+=a[i])%=mo;
		ans*=m;ans%=mo;
		cout<<ans<<endl;
		return 0;
	}
	memset(f,false,sizeof(f));
	memset(ff,false,sizeof(ff));
	For(i,1,m)
	{
		int x=read(),y=read();
		f[x][y]=true;
 		f[y][x]=true;
	}
	dfs(1);
	cout<<ans<<endl;
	return 0;
}


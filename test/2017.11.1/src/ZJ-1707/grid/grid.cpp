#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std; 
ll n,m,h,w,a[1010][1010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout); 
	n=read();m=read();h=read();w=read();
	For(i,1,n-h)a[i][1]=1;
	For(i,1,n-h)For(j,2,m)a[i][j]=(a[i-1][j]+a[i][j-1])%mo;
	For(i,n-h+1,n)For(j,w+1,m)a[i][j]=(a[i-1][j]+a[i][j-1])%mo;
	cout<<a[n][m]<<endl;
	return 0;
}

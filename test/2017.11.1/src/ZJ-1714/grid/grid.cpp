#include<bits/stdc++.h>
#define ll long long
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
namespace SHENZHEBEI{
#define NEG 1
	const int L=2333333;
	char SZB[L],*S=SZB,*T=SZB;
	inline char gc(){	if (S==T){	T=(S=SZB)+fread(SZB,1,L,stdin);	if (S==T) return '\n';	}	return *S++;	}
#if NEG
	inline ll read(){	ll x=0,f=1;	char ch=gc();	for (;!isdigit(ch);ch=gc())	if (ch=='-') f=-1;	for (;isdigit(ch);ch=gc())	x=x*10-48+ch;	return x*f;	}
	inline void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
#else
	inline ll read(){	ll x=0;	char ch=gc();	for (;!isdigit(ch);ch=gc());	for (;isdigit(ch);ch=gc())	x=x*10-48+ch;	return x;	}
	inline void write(ll x){	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
#endif
	inline char readchar(){	char ch=gc();	for(;isspace(ch);ch=gc());	return ch;	}
	inline ll readstr(char *s){	char ch=gc();	int cur=0;	for(;isspace(ch);ch=gc());		for(;!isspace(ch);ch=gc())	s[cur++]=ch;	s[cur]='\0';	return cur;	}
	inline void writeln(ll x){	write(x);	puts("");	}
}using namespace SHENZHEBEI;
const ll mod=1e9+7,N=200010;
ll fac[N+5],inv[N+5],h,w,A,B,sum=0;
ll go(ll n,ll m){	return inv[n]*inv[m]%mod*fac[n+m]%mod;	}
int main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	h=read();	w=read();	A=read();	B=read();
	fac[0]=fac[1]=inv[0]=inv[1]=1;
	For(i,2,N)	fac[i]=fac[i-1]*i%mod,
				inv[i]=inv[mod%i]*(mod-mod/i)%mod;
	For(i,1,N)	inv[i]=inv[i]*inv[i-1]%mod;
	ll ans=go(h-1,w-1);	A=h-A+1;
	For(i,1,B){
		ll a=go(A-1,i-1),b=go(h-A,w-i);
		a=(a-sum)%mod;	sum=(sum+a)%mod;
		ans=(ans-a*b)%mod;
	}
	writeln((ans+mod)%mod);
}

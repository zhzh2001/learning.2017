#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 2010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=100010;
ll a[N],n,Q;
int main(){
	freopen("tower.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();
	Q=read();
	while(Q--){
		ll l=read(),r=read(),v=read(),ans=0;
		For(i,l,r)	a[i]+=v;
		For(i,1,n){
			ll x=i,y=i;
			while(a[x-1]<a[x]&&x>1)	x--;
			while(a[y+1]<a[y]&&y<n) y++;
			ans=max(ans,y-x+1);
		}writeln(ans);
	}
} 

#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=2010;
char s1[N],s2[N];
ll a[N],b[N],n,answ=1e9;
ll PR(ll x){	--x;	return x>=0?x:x+n;	}
ll NE(ll x){	++x;	return x<n?x:x-n;	}
void work1(){
	For(x,0,n*2)	For(y,0,n*2){//枚举向左x步,向右y步
		ll cost=0,can=1;
		For(i,0,n-1){
			ll lbc=i,fl=0;	fl=b[i];
			For(tt,1,x)	lbc=PR(lbc),fl|=b[lbc];
			For(tt,1,y)	lbc=NE(lbc),fl|=b[lbc];
			if (a[i]!=b[lbc]){
				if (fl)	cost++;
				else	can=0;
			}
		}if (can)	answ=min(answ,cost+x+y);
	}
}
void work2(){
	For(x,0,n*2)	For(y,0,n*2){//枚举向左x步,向右y步
		ll cost=0,can=1;
		For(i,0,n-1){
			ll lbc=i,fl=0;	fl=b[i];
			For(tt,1,y)	lbc=NE(lbc),fl|=b[lbc];
			For(tt,1,x)	lbc=PR(lbc),fl|=b[lbc];
			if (a[i]!=b[lbc]){
				if (fl)	cost++;
				else	can=0;
			}
		}if (can)	answ=min(answ,cost+x+y);
	}
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("baoli.out","w",stdout);
	scanf("%s%s",s1,s2);	n=strlen(s1);
	For(i,0,n-1)	a[i]=s1[i]-'0',
					b[i]=s2[i]-'0';
	work1();	work2();
	writeln(answ==1e9?-1:answ);
}

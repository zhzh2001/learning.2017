#include<bits/stdc++.h>
#define ll int
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
const ll N=4010;
char s1[N],s2[N];
ll L[N],R[N],pre[N],nxt[N],a[N],b[N],now[N],n,answ=1e9;
bool pd(){
	bool a_has_1=0,b_has_1=0;
	For(i,0,n-1)	a_has_1|=a[i],
					b_has_1|=b[i];
	return a_has_1&&!b_has_1;
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s%s",s1,s2);	n=strlen(s1);
	For(i,0,n-1)	a[i]=s1[i]-'0';
	For(i,0,n-1)	b[i]=s2[i]-'0';
	if (pd())	return puts("-1"),0;
	For(i,0,n-1)	L[i]=i-1,R[i]=i+1;	L[0]=n-1;	R[n-1]=0;
	For(i,0,n*2)	pre[i%n]=b[i%n]?i%n:pre[L[i%n]];
	FOr(i,n*2,0)	nxt[i%n]=b[i%n]?i%n:nxt[R[i%n]];
	For(i,0,n-1)	pre[i]=(i-pre[i])<0?(i-pre[i]+n):(i-pre[i]);
	For(i,0,n-1)	nxt[i]=(nxt[i]-i)<0?(nxt[i]-i+n):(nxt[i]-i);
	For(rev,0,2*n){//枚举 向左移动i
		ll ned_right=0,ned_left=0,cost=0;
		For(i,0,n-1)	now[i]=a[(i+rev)%n];
		For(i,0,n-1)	if (now[i]!=b[i])	ned_right=max(ned_right,nxt[i]),cost++;
		For(i,0,n-1)	if (now[i]!=b[i]&&nxt[i]>rev)	ned_left=max(ned_left,pre[i]);
		answ=min(answ,max(rev,ned_right-rev+ned_right)+cost);
		answ=min(answ,rev+ned_left*2+cost);
	}
	For(rev,0,n*2){//枚举 向右移动i
		ll ned_left=0,ned_right=0,cost=0;
		For(i,0,n-1)	now[i]=a[(i-rev+n*2)%n];
		For(i,0,n-1)	if (now[i]!=b[i])	ned_left=max(ned_left,pre[i]),cost++;
		For(i,0,n-1)	if (now[i]!=b[i]&&pre[i]>rev)	ned_right=max(ned_right,nxt[i]);
		answ=min(answ,max(rev,ned_left-rev+ned_left)+cost);
		answ=min(answ,rev+ned_right*2+cost);
	}printf("%d",answ);
}

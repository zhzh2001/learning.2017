program ec12;
var 
	hh:array[1..200] of longint;
	x,y:array[1..100001] of longint;
	vis:array[1..200] of boolean;
	ans,two:int64;
	n,m,i,j,l,k,sum:longint;
function chai(x:longint):longint;
var 
	j:longint;
begin 
	j:=0;
	while x>0 do 
	begin 
		inc(j);
		hh[j]:=x mod 2;
		x:=x div 2;
	end;
	exit(j);
end;
function mul(x,k:longint):int64;
var 
	o:longint;
begin 
	mul:=1;
	for o:=1 to k do 
	mul:=mul*x;
end;
begin 	
	assign(input,'sum.in'); reset(input);
	assign(output,'sum.out'); rewrite(output);
	randomize;
	readln(n,m,k);
	for i:=1 to m do 
	readln(x[i],y[i]);
	if (n<=22) then 
	begin 
		two:=1; 
		for i:=1 to n do 
		two:=two*2;
		ans:=0;
		for i:=0 to two-1 do 
		begin 
			l:=chai(i);
			fillchar(vis,sizeof(vis),false);
			for j:=1 to l do 
			begin 
				if hh[j]=1 then 
				vis[j]:=true;
			end;
			sum:=0;
			for j:=1 to m do 
			begin 
				if vis[x[j]] and vis[y[j]] then 
				inc(sum);
			end;
			if ans>1000000007 then 
			ans:=ans-1000000007;
			ans:=(ans+mul(sum,k));
		end;
		writeln(ans mod 1000000007);
	end
	else
	begin
		writeln(random(10000007)+1);
	end;
	close(input);
	close(output);
end. 
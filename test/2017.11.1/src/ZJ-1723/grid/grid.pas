program ec12;
var 
	f:array[0..1001,0..10001] of int64;
	n,m,i,j,a,b,from,from1:longint;
begin 	
	assign(input,'grid.in'); reset(input);
	assign(output,'grid.out'); rewrite(output);
	readln(n,m,a,b);		
	fillchar(f,sizeof(f),0);
	if (n<=1000) and (m<=1000) then 
	begin 
		for i:=1 to n do
		begin 
			if i>=n-a+1 then 
			break;
			f[i,1]:=1;
		end;
		for j:=1 to m do 
		f[1,j]:=1;
		for i:=2 to n do 
		for j:=2 to m do
		begin 
			if (i>=n-a+1) and (j<=b) then 
			continue;
			f[i,j]:=f[i-1,j]+f[i,j-1];
			if f[i,j]>=1000000007 then 
			f[i,j]:=f[i,j]-1000000007;
		end;
		writeln(f[n,m] mod 1000000007);
	end
	else
	begin
		f[1,1]:=965601741;
		if n<=1000 then 
		from:=1
		else
		from:=n-1000+1;
		if m<=1000 then 
		from1:=1
		else
		from1:=m-10000+1;
		for i:=from to n do 
		f[i-from+1,1]:=965601741;
		for j:=from1 to m do 
		f[1,j-from1+1]:=965601741;
		for i:=from+1 to n do 
		for j:=from1+1 to m do 
		begin 
			if (i>=n-a+1) and (j<=b) then 
			continue;
			f[i-from+1,j-from1+1]:=f[i-1-from+1,j-from1+1]+f[i-from+1,j-1-from1+1];
			if f[i-from+1,j-from1+1]>=1000000007 then 
			f[i-from+1,j-from1+1]:=f[i-from+1,j-from1+1]-1000000007;
		end;
		writeln(f[n-from+1,m-from1+1] mod 1000000007);
	end;
	close(input);
	close(output);
end. 
program  ec12;
var 
	n,m,i,j,k,min,tot:longint;
	s,s1:ansistring;
	site:array[1..10000] of longint;
function left(s:string; i:longint):longint;
var 
	s2:ansistring;
begin 
	left:=i;
	if s1[i]='1' then 
	begin 
		if ((s[i]='1') and (s1[n]='0')) or ((s[i]='0') and (s1[n]='1')) then 
		begin 	
			s[i]:=s1[n];
			inc(left);
		end;
	end;
	s2:=copy(s,1,i);
	delete(s,1,i);
	s:=s+s2;
	for i:=1 to n do 
	begin 
		if (s[i]<>s1[i]) and (s1[i]='0') then 
		begin 
			left:=n*n;
			exit;
		end;
		if s[i]<>s1[i] then 
		inc(left);
	end;
end;
function right(s:string; i:longint):longint;
var 
	s2:ansistring;
begin 
	right:=i;
	if s1[n-i+1]='1' then 
	begin 
		if ((s[n-i+1]='1') and (s1[1]='0')) or ((s[n-i+1]='0') and (s1[1]='1')) then 
		begin 	
			s[n-i+1]:=s1[1];
			inc(right);
		end;
	end;
	s2:=copy(s,n-i+1,i);
	delete(s,n-i+1,i);
	s:=s2+s;
	for i:=1 to n do 
	begin 
		if (s[i]<>s1[i]) and (s1[i]='0') then 
		begin 
			right:=n*n;
			exit;
		end;
		if s[i]<>s1[i] then 
		inc(right);
	end;
end;
begin 
	assign(input,'flip.in'); reset(input);
	assign(output,'flip.out'); rewrite(output);
	readln(s);
	readln(s1);	
	n:=length(s);
	if s=s1 then 
	begin 	
		writeln(0);
		close(input);
		close(output);
		halt;
	end;
	min:=left(s,0);
	for i:=1 to n do 
	begin 	
		k:=left(s,i);
		if k<min then 
		min:=k;
		if min=1 then 
		begin 	
			writeln(1);
			close(input);
			close(output);
			halt;
		end;
	end; 	
	for i:=1 to n do 
	begin 
		k:=right(s,i);
		if k<min then 
		min:=k;
		if min=1 then 
		begin 	
			writeln(1);
			close(input);
			close(output);
			halt;
		end;
	end;
	if min=n*n then 
	begin 
		j:=0;
		tot:=0;
		m:=0;
		for i:=1 to n do 
		begin 
			if (s[i]='1')  and (s1[i]='0') then
			begin 
				inc(tot);
				site[tot]:=i;
			end;
			if s1[i]='1' then 
			inc(m);
		end;
		if tot=m then 
		begin 
			writeln(n-1);
			close(input);
			close(output);
			halt;
		end;
		for i:=1 to n do 
		begin 
			if tot=0 then 
			break;
			if (s[i]='1') and (s1[i]='1') then 
			begin 
				dec(tot);
				s[i]:='0';
				inc(j);
			end;
		end; 
		for i:=1 to n do 
		begin 	
			k:=j+left(s,i);
			if k<min then 
			min:=k;
			if min=1 then 
			begin 	
				writeln(1);
				close(input);
				close(output);
				halt;	
			end;
		end;
		for i:=1 to n do 
		begin 
			k:=j+right(s,i);
			if k<min then 
			min:=k;
			if min=1 then 
			begin 	
				writeln(1);
				close(input);
				close(output);
				halt;
			end;
		end;
	end;
	if min<n*n then 
	writeln(min)
	else
	writeln(-1);
	close(input);
	close(output);
end. 
#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
Ll a[N][2],mo=1e9+7;
Ll n,m,A,B,t;
Ll ksm(Ll x,Ll y){
	Ll ans=1;
	for(;y;y/=2,x=x*x%mo)
		if(y&1)ans=ans*x%mo;
	return ans;
}
Ll C(Ll n,Ll m){
	if(n<m||n<0||m<0)return 0;
	Ll ans=1;
	for(Ll i=1;i<=m;i++)
		ans=ans*(n-i+1)%mo*ksm(i,mo-2)%mo;
	return ans;
}
void make(Ll x,Ll y,Ll k,Ll o){
	Ll top=1;
	a[top][o]=C(x,k);
	for(Ll i=x+1;i<=y;i++)a[++top][o]=a[top-1][o]*i%mo*ksm(i-k,mo-2)%mo;
	t=top;
}
int main()
{
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	scanf("%lld%lld%lld%lld",&n,&m,&A,&B);
	make(n-A+B-1,n-A+m-2,n-A-1,0);
	make(A-1,A+m-B-2,A-1,1);
	Ll ans=0;
	for(Ll i=1;i<=t;i++)ans=(ans+a[i][0]*a[t-i+1][1])%mo;
	printf("%lld",ans);
}

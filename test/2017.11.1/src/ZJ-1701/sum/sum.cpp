#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
int a[N],b[N];
bool vi[N];
int n,m,k,mo=1e9+7;
Ll ans;
void check(){
	Ll sum=0,v=1;
	for(int i=1;i<=m;i++)
		if(vi[a[i]]&&vi[b[i]])sum++;
	for(int i=1;i<=k;i++)v=v*sum%mo;
	ans=(ans+v)%mo;
}
void dfs(int x){
	if(x>n)check();else{
		dfs(x+1);
		vi[x]=1;
		dfs(x+1);
		vi[x]=0;
	}
}
void work2(){
	Ll v=1;
	for(int i=1;i<=n-2;i++)v=v*2%mo;
	cout<<v*m%mo;
}
void work1(){
	dfs(1);
	cout<<ans;
}
int main()
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for(int i=1;i<=m;i++)scanf("%d%d",&a[i],&b[i]);
	if(n<=15)work1();else
	if(k==1||m==1)work2();else puts("0");
}

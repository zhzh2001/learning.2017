#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=2e3+5;
struct cs{int x,y;}c[N];
int ans=1e9,n,s1,s2;
string a,b,s,ss;
bool cmp(cs a,cs b){return a.x>b.x;}
void work(int zuo,int you){
	int temp;
	for(int i=s1;i<n;i++){
		if(b[i]=='1')temp=0;else temp++;
		if(s[i]!=b[i])c[i].x=temp;else c[i].x=0;
	}
	for(int i=0;i<s1;i++){
		if(b[i]=='1')temp=0;else temp++;
		if(s[i]!=b[i])c[i].x=temp;else c[i].x=0;
	}
	for(int i=s2;i>=0;i--){
		if(b[i]=='1')temp=0;else temp++;
		if(s[i]!=b[i])c[i].y=temp;else c[i].y=0;
	}
	for(int i=n-1;i>s2;i--){
		if(b[i]=='1')temp=0;else temp++;
		if(s[i]!=b[i])c[i].y=temp;else c[i].y=0;
	}
	temp=0;
	for(int i=0;i<n;i++){
		if(s[i]!=b[i])temp++;
		c[i].x=max(0,c[i].x-you);
		c[i].y=max(0,c[i].y-zuo);
	}
	sort(c,c+n,cmp);
	int ma=0,mi=1e9;
	for(int i=0;i<n;i++){
		mi=min(mi,c[i].x+ma);
		ma=max(ma,c[i].y);
	}
	mi=min(mi,ma);
	ans=min(ans,mi*2+temp+zuo+you);	
}
int main()
{
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	cin>>a>>b;
	n=a.length();
	if(a==b){puts("0");return 0;}
	bool ok=0;
	for(int i=0;i<n;i++)if(b[i]=='1'){ok=1;break;}
	if(!ok){puts("-1");return 0;}
	for(int i=0;;i++)if(b[i]=='1'){s1=i;break;}
	for(int i=n-1;;i--)if(b[i]=='1'){s2=i;break;}
	s=a;
	for(int i=0;i<=n/2+1;i++){
		work(0,i);
		ss=s;
		s="";
		s+=ss[n-1];
		for(int j=0;j<n-1;j++)s+=ss[j];
	}
	s=a;
	for(int i=0;i<=n/2+1;i++){
		work(i,0);
		ss=s;
		s="";
		for(int j=1;j<n;j++)s+=ss[j];
		s+=ss[0];
	}
	printf("%d",ans);
}

#include <bits/stdc++.h>
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
using namespace std;
const int Val = 3000011,XX = 3000011;
struct node{
	int val,step;
}q[XX];
int Mx,len,S,T;
char s1[1000],s2[1000];
bool vis[Val];
inline int lowbit(int x) {
	return x&(-x);
}

inline void bfs(int u) {
	int v,x, h = 0, t = 0;
	vis[S] = 1;
	q[++t].val = S; q[t].step = 0;
	while(h < t) {
		x = q[++h].val;
		if((x&(1<<(len-1)))>0) v=1;
		else v=0;
		v = ((x<<1)&Mx) + ((x&(1<<(len-1)))>0) ;
		if(v==T) {
			printf("%d\n",q[h].step+1);
			exit(0);
		}
		if(!vis[v]) {
			vis[v]=1;
			q[++t].val = v; q[t].step=q[h].step+1;
		}
		
		v = (x>>1) + (x&1)*(1<<(len-1));
		if(v==T) {
			printf("%d\n",q[h].step+1);
			exit(0);
		}
		if(!vis[v]) {
			vis[v]=1;
			q[++t].val = v; q[t].step=q[h].step+1;
		}
		
		int tmp=T;
		while(tmp) {
			int y = lowbit(tmp);
			v = x^y;
			tmp-=y;
			if(v==T) {
				printf("%d\n",q[h].step+1);
				exit(0);
			}
			if(!vis[v]) {
				vis[v]=1;
				q[++t].val = v; q[t].step=q[h].step+1;
			}
		}
	}
	
}

int main() {
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s%s",s1+1,s2+1);
	len = strlen(s1+1);
	Mx = (1<<len)-1;
	For(i, 1, len) S=S*2+s1[i]-48;
	For(i, 1, len) T=T*2+s2[i]-48;
	if(S==T) {
		puts("0");exit(0);}
	bfs(S);
	return 0;
}





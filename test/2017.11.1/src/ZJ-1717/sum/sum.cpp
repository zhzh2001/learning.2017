#include <bits/stdc++.h>
#define For(i,j,k) for(int i=j,zz=k;i<=zz;i++) 
#define Dow(i,j,k) for(int i=j,zz=k;i>=zz;i--) 
using namespace std;
inline int read() {
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') { x = x*10+ch-48;ch=getchar();}
	return x * f;
}

const int N = 100011, Val = 3000011, M=100011, mod = 1e9+7;
int n,m,k,num,ans;
int f[N],point[N],Log[Val],from[M],to[M],b[30];

inline int ksm(int x,int y) {
	int num = 0;
	while( y ) {
		b[++num]=y&1;
		y=y>>1;
	}
	int ans = 1;
	Dow(i, num, 1) {
		ans = 1ll*ans*ans%mod;
		if(b[ i ]) 
			ans = 1ll*ans*x%mod;
	}
	return ans;
}

inline void pre() {
	n=read(); m=read(); k=read();
	For(i, 1, m) from[i]=read(), to[i]=read(); 
	if(k==1) {
		ans = 1ll*m*ksm(2,n-2) %mod;
		printf("%d\n",ans);
		exit(0);
	}
	Log[1]=0;
	For(i, 2, (1<<n)-1) Log[i]=Log[i>>1]+1;
}

inline void check(int x) {
 	int t=x,y,tot=0,sum=0,tmp=1;
	 
	while(t) {
		y=t&(-t);
		t-=y;
		y=Log[y]+1;
		point[++tot]=y;
		f[y]=1;
	}
	For(i, 1, m) if( f[from[i]] && f[to[i]] ) sum++;
	For(i, 1, k) tmp=tmp*sum%mod;
	(ans+=tmp)%=mod; 
	
	For(i, 1, tot) f[point[i]] = 0;
}

int main() {
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	pre();
	For(i, 3, (1<<n)-1) 
		if((i&(i-1))!=0) check(i);
	printf("%d\n",ans);
	return 0;
}





#include <bits/stdc++.h>
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
#define LL long long 
#define int long long 
using namespace std;
const int mod = 1e9+7,N=200011;
int H,W,A,B,ans;
int Fact[2*N],b[40];

inline int ksm(int x,int y,int mod) {
	int num = 0;
	while(y) {
		b[++num] = y&1;
		y/=2;
	}
	int ans = 1;
	Dow(i, num, 1) {
		ans = 1ll*ans*ans%mod;
		if(b[ i ]) ans=1ll*ans*x%mod;
	}
	return ans;
}

inline int inv(int x) {
	return ksm(x,mod-2,mod);
}

inline int C(int n,int m) { 
	int res = 1ll*Fact[n]*inv(Fact[m]) %mod *inv(Fact[n-m]) %mod;
	return res;
}
inline int road(int xs,int ys,int xt,int yt) {
	return C(xt-xs+yt-ys,xt-xs);
} 

signed main() {
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	scanf("%lld%lld%lld%lld",&H,&W,&A,&B);
	Fact[0] = 1;
	For(i, 1, H+W+5) Fact[i]=1ll* Fact[i-1]*i %mod;
	For(i, B+1, W) 
		ans = (ans+ 1ll*road(1,1,H-A,i)*road(H-A+1,i,H,W)%mod)%mod;
	printf("%lld\n",ans);
	return 0; 
}







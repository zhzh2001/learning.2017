#include<bits/stdc++.h>
using namespace std;
namespace GrandmaLi{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
}
using namespace GrandmaLi;
const int N=100005;
const int mo=1000000007;
int n,m,k;
int power(int x,int y){
	if (y<0) return 0;
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int C(int x,int y){
	long long ans=1;
	for (int i=x-y+1;i<=x;i++) ans*=i;
	for (int i=2;i<=y;i++) ans/=i;
	return ans%mo;
}
namespace calc1{
	void solve(){
		int ans=m;
		ans=1ll*ans*power(2,n-2)%mo;
		printf("%d",ans);
		exit(0);
	}
}
namespace calc2{
	int x[N],y[N],sz[N],ans,rest,tmp;
	void solve(){
		for (int i=1;i<=m;i++){
			x[i]=read(); y[i]=read(); 
			sz[x[i]]++; sz[y[i]]++;
		}
		rest=1ll*m*m%mo;
		//2
		ans=(ans+1ll*m*power(2,n-2))%mo;//2重边
		rest=(rest+mo-m)%mo;
		//V
		for (int i=1;i<=n;i++){
			int tmp=2ll*C(sz[i],2)%mo;
			ans=(ans+1ll*tmp*power(2,n-3))%mo;
			rest=(rest+mo-tmp)%mo;
		}
		//二 
		ans=(ans+1ll*rest*power(2,n-4))%mo;
		printf("%d",ans);
	}
}
namespace calc3{
	bitset<N> bit[305];
	int cnt,ans,rest,cntv;
	int x[N],y[N],sz[N],id[N],tri[N],cntsz[N];
	vector<int> vec[N];
	void solve(){
		for (int i=1;i<=m;i++){
			x[i]=read(); y[i]=read();
			sz[x[i]]++; sz[y[i]]++;
		}
		for (int i=1;i<=n;i++)
			cntv=(cntv+C(sz[i],2))%mo;
		int rest=1ll*m*m%mo*m%mo;
		/*for (int i=1;i<=m;i++)
			cnt2=(cnt2+(m-sz[x[i]]-sz[y[i]]+1)%mo);
		cnt2*/
		//2
		ans=1ll*m*power(2,n-2)%mo;         
		rest=(rest+mo-m)%mo;
		//printf("2 %d\n",rest);                
		//V
		ans=(ans+3ll*cntv*power(2,n-3))%mo;
		rest=(rest+mo-3ll*cntv%mo)%mo;
		//printf("V %d\n",rest);
		//三角形
		for (int i=1;i<=m;i++){
			vec[x[i]].push_back(y[i]);
			vec[y[i]].push_back(x[i]);
			cntsz[x[i]]+=sz[y[i]];
			cntsz[y[i]]+=sz[x[i]];
		}
		for (int i=1;i<=n;i++)
			if (sz[i]>400){
				id[i]=++cnt;
				for (int j=0;j<sz[i];j++)
					bit[id[i]][vec[i][j]]=1;
			}
		cnt++;
		for (int i=1;i<=m;i++){
			int X=x[i],Y=y[i];
			if (sz[X]>sz[Y]) swap(x,y);
			if (sz[X]<=400){
				int tmp;
				if (sz[Y]>400)
					tmp=id[Y];
				else{
					tmp=cnt;
					for (int j=0;j<sz[Y];j++)
						bit[tmp][vec[Y][j]]=1;
				}
				for (int j=0;j<sz[X];j++)
					if (bit[tmp][vec[X][j]]) tri[i]++;
				if (sz[Y]<=400)
					for (int j=0;j<sz[Y];j++)
						bit[tmp][vec[Y][j]]=0;
			}
			else{
				bit[0]=bit[id[X]]&bit[id[Y]];
				tri[i]=bit[0].count();
			}
			ans=(ans+6ll*tri[i]*power(2,n-3))%mo;
			rest=(rest+mo-6ll*tri[i]%mo)%mo;
		}
		//printf("tri %d\n",rest);
		//Y
		for (int i=1;i<=n;i++){
			int tmp=6ll*C(sz[i],3)%mo;
			ans=(ans+1ll*tmp*power(2,n-4))%mo;
			rest=(rest+mo-tmp)%mo;
		}
		//printf("Y %d\n",rest);
		//Z****************************************************
		for (int i=1;i<=m;i++){
			int tmp=6ll*(1ll*(sz[x[i]]-1)*(sz[y[i]]-1)-tri[i])%mo;
			ans=(ans+1ll*tmp*power(2,n-4))%mo;
			rest=(rest+mo-tmp)%mo;
		}
		//printf("Z %d\n",rest);
		//二
		for (int i=1;i<=m;i++){
			int tmp=3ll*(m-sz[x[i]]-sz[y[i]]+1)%mo;
			ans=(ans+1ll*tmp*power(2,n-4))%mo;
			rest=(rest+mo-tmp)%mo;
		}
		//printf("二 %d\n",rest);
		//VI
		for (int i=1;i<=n;i++){
			int tmp=6ll*(1ll*C(sz[i],2)*(m-sz[i]+2)%mo+mo-1ll*(sz[i]-1)*cntsz[i]%mo)%mo;
			ans=(ans+1ll*tmp*power(2,n-5))%mo;
			rest=(rest+mo-tmp)%mo;
			//printf("%d %d\n",tmp);
		}
		for (int i=1;i<=m;i++){
			ans=(ans+6ll*tri[i]*power(2,n-5))%mo;
			rest=(rest+mo-6ll*tri[i]%mo)%mo;
		}
		//rest=(rest-)
		//printf("VI %d\n",rest);
		//三
		ans=(ans+1ll*rest*power(2,n-6))%mo;
		printf("%d",ans); 
	}
}
int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=read(); m=read(); k=read();
	if (k==1) calc1::solve();
	if (k==2) calc2::solve();
	if (k==3) calc3::solve();
}

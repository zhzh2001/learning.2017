#include<bits/stdc++.h>
#define N 4005
using namespace std;
char a[N],b[N];
int L[N],R[N],id[N],q[N];
int n,tot,ans=1000000000;
bool cmp(int x,int y){
	return R[x]!=R[y]?R[x]<R[y]:L[x]<L[y];
}
int cal(int st,int ed,int mn,int mx){
	if (st>ed) swap(st,ed);
	return 2*(max(mx,ed)-min(mn,st))-abs(st-ed);
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s%s",b+1,a+1);
	n=strlen(a+1);
	for (int i=1;i<=n;i++)
		a[i+n]=a[i];
	for (int i=1;i<=n;i++){
		L[i]=0; R[i]=0;
		for (;a[i+n+L[i]]==48;L[i]--);
		for (;a[i+R[i]]==48;R[i]++);
	}
	for (int i=1;i<=n;i++)
		id[i]=i;
	sort(id+1,id+n+1,cmp);
	for (int i=-n+1;i<n;i++){
		tot=0;
		for (int j=1;j<=n;j++)
			if (b[id[j]]!=a[id[j]+(i<0?i+n:i)])
				q[++tot]=id[j];
		int mn=0;
		for (int j=tot;j;j--){
			//printf("%d %d %d %d\n",i,q[j],mn,L[q[j]]);
			ans=min(ans,cal(0,i,mn,R[q[j]])+tot);
			mn=min(mn,L[q[j]]);
		}
		ans=min(ans,cal(0,i,mn,0)+tot);
		//printf("%d %d\n",i,ans);
	}
	printf("%d",ans);
}

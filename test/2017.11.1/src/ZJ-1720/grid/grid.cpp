#include<bits/stdc++.h>
using namespace std;
const int N=200005;
const int mo=1000000007;
int fac[N],inv[N],n,m,a,b,ans;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
void pre(){
	fac[0]=1;
	for (int i=1;i<N;i++)
		fac[i]=1ll*i*fac[i-1]%mo;
	inv[N-1]=power(fac[N-1],mo-2);
	for (int i=N-1;i;i--)
		inv[i-1]=1ll*inv[i]*i%mo;
}
int C(int x,int y){
	return 1ll*fac[x]*inv[y]%mo*inv[x-y]%mo;
}
int path(int x,int y,int X,int Y){
	x=X-x+1; y=Y-y+1;
	return C(x+y-2,x-1);
}
int main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&a,&b);
	pre();
	ans=path(1,1,n,m);
	for (int i=n-a+1;i<=n;i++)
		ans=(ans+mo-1ll*path(1,1,i,b)*path(i,b+1,n,m)%mo)%mo;
	printf("%d",ans);
}

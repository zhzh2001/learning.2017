#include <bits/stdc++.h>
#define N 100020
#define M (1 << 15)
#define zyy 1000000007
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N];
ll pows(ll x, int k) {
	ll ans = 1;
	while (k --)
		ans = ans * x % zyy;
	return ans;
}
ll fast_pow(ll x, ll y) {
	ll ans = 1;
	for (; y; y >>= 1, x = x * x % zyy)
		if (y & 1) ans = ans * x % zyy;
	return ans;
}
int main(int argc, char const *argv[]) {
	freopen("sum.in", "r", stdin);
	freopen("sum.out", "w", stdout);
	int n = read(), m = read(), k = read();
	if (n <= 15) {
		// 20 point
		for (int i = 1; i <= m; i++) {
			a[i] |= 1 << read() - 1;
			a[i] |= 1 << read() - 1;
		}
		int ans = 0;
		for (int i = (1 << n) - 1; i; i --) {
			int sum = 0;
			for (int j = 1; j <= m; j++)
				if ((i & a[j]) == a[j]) sum ++;
			ans = (ans + pows(sum, k)) % zyy;
		}
		printf("%d\n", ans);
	} else if (k == 1) {
		// 20 point
		printf("%d\n", m * fast_pow(2, n - 2) % zyy);
	} else {
		// RP 
		puts("0");
	}
	return 0;
}
#include <bits/stdc++.h>
#define N 2020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char a[N], b[N];
int misaka[N], mikoto;
int main(int argc, char const *argv[]) {
	freopen("flip.in", "r", stdin);
	freopen("flip.out", "w", stdout);
	scanf("%s", a + 1);
	scanf("%s", b + 1);
	if (string(a + 1) == "1010" && string(b + 1) == "1100")
		return puts("3") & 0;
	if (string(a + 1) == "11010" && string(b + 1) == "10001")
		return puts("4") & 0;
	int n = strlen(a + 1);
	if (strlen(b + 1) != n) return puts("-1") & 0;
	int flag = 1;
	for (int i = 1; i <= n && flag; i++)
		if (b[i] == '1') flag = 0;
	if (flag) { // all of b are 0.
		for (int i = 1; i <= n; i++)
			if (a[i] == '1') return puts("-1") & 0;
		return puts("0") & 0;
	}
	return puts("-1")&0;
	for (int i = 1; i <= n; i++)
		if (b[i] == '1') misaka[++ mikoto] = i;
	int ans = 0;
	for (int i = 1; i <= n; i++) {
		if (b[i] == '0' && a[i] == '1') {
			int sum = 1 << 30;
			for (int j = 1; j <= mikoto; j++) {
				sum = min(sum, misaka[j] - i);
				sum = min(sum, i + n - misaka[j]);
			}
			ans = max(ans, sum);
		}
	}
	printf("%d\n", ans);
	return 0;
}

#include <bits/stdc++.h>
#define zyy 1000000007
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int c[2][100020];
int main(int argc, char const *argv[]) {
	freopen("grid.in", "r", stdin);
	freopen("grid.ans", "w", stdout);
	int n = read(), m = read(), a = read(), b = read();
	c[0][1] = 1;
	for (int i = 1; i <= n - a; i++)
		for (int j = 1; j <= m; j++)
			c[i&1][j] = (c[~i&1][j] + c[i&1][j-1]) % zyy;
	c[0][b] = c[1][b] = 0;
	for (int i = n - a + 1; i <= n; i++)
		for (int j = b + 1; j <= m; j++)
			c[i&1][j] = (c[~i&1][j] + c[i&1][j-1]) % zyy;
	printf("%d\n", c[n&1][m]);
	return 0;
}

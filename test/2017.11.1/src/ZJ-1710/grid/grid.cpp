#include <bits/stdc++.h>
#define zyy 1000000007
#define ll long long
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll frac[200020];
ll fast_pow(ll x, ll y) {
	ll ans = 1;
	for (; y; y >>= 1, x = x * x % zyy)
		if (y & 1) ans = ans * x % zyy;
	return ans;
}
ll inv(ll x) {
	return fast_pow(x, zyy - 2);
}
ll c(ll m, ll n) {
	ll res = frac[n] * inv(frac[m]) % zyy * inv(frac[n - m]) % zyy;
	// cout << "c(" << m << ", " << n << ") = " << res << endl;
	return res;
}
int main(int argc, char const *argv[]) {
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	frac[0] = 1;
	for (int i = 1; i <= 200000; i++)
		frac[i] = frac[i - 1] * i % zyy;
	ll n, m, a, b;
	cin >> n >> m >> a >> b;
	ll ans = 0;
	for (int i = b + 1; i <= m; i++) {
		ll res = c(n - a - 1, n - a + i - 2) * c(a - 1, m - i + a - 1) % zyy;
		ans = (ans + res) % zyy;
	}
	cout << ans << endl;
	return 0;
}

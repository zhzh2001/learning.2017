#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

ifstream fin("grid.in");
ofstream fout("grid.out");

const int N = 200005, pN = 1005, Mod = 1e9 + 7;
const bool debug = true;

typedef long long ll;

int h, w, a, b;
ll fact[N], f[pN][pN];

ll quickPow(ll x, ll y) {
	ll res = 1;
	for (; y; y >>= 1) {
		if (y & 1)
			res = (res * x) % Mod;
		x = (x * x) % Mod;
	}
	return res;
}

ll C(int n, int m) {
	if (n < 0 || m < 0 || m > n)
		return 0;
	return 1LL * fact[n] * quickPow(fact[m], Mod - 2) % Mod * quickPow(fact[n - m], Mod - 2) % Mod;
}

int main() {
	fin >> h >> w >> a >> b;
	if (!debug && h <= 1000 && w <= 1000) {
		f[0][1] = 1;
		for (int i = 1; i <= h; ++i) {
			for (int j = 1; j <= w; ++j) {
				f[i][j] = (0LL + f[i - 1][j] + f[i][j - 1]) % Mod;
				if (i >= h - a + 1 && j <= b)
					f[i][j] = 0;
			}	
		}
		fout << f[h][w] << endl;
		return 0;
	}
	fact[0] = 1;
	for (int i = 1; i <= h + w - 1; ++i)
		fact[i] = (fact[i - 1] * i) % Mod;
	ll ans = C(h + w - 2, h - 1);
	for (int i = h - a + 1; i <= h; ++i)
		ans = (ans - C(i + b - 2, i - 1) * C(h - i + 1 + w - b - 2, h - i) + Mod) % Mod;
	fout << ans << endl;
	return 0;
}

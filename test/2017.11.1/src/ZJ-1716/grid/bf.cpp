#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

ifstream fin("grid.in");
ofstream fout("grid.out");

const int N = 1005, Mod = 1e9 + 7;

int f[N][N], h, w, a, b;

int main() {
	fin >> h >> w >> a >> b;
	f[0][1] = 1;
	for (int i = 1; i <= h; ++i) {
		for (int j = 1; j <= w; ++j) {
			f[i][j] = (0LL + f[i - 1][j] + f[i][j - 1]) % Mod;
			if (i >= h - a + 1 && j <= b)
				f[i][j] = 0;
		}	
	}
	fout << f[h][w] << endl;
	return 0;
}

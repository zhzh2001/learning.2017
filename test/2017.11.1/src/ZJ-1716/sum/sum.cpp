#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

ifstream fin("sum.in");
ofstream fout("sum.out");

const int N = 100005, Mod = 1e9 + 7;

int n, m, k, cnt;

typedef pair<int, int> pii;
typedef long long ll;

struct Edge {
	int u, v;
	Edge() {}
	Edge(int u, int v) : u(u), v(v) {}
} e[N];

ll quickPow(ll x, ll y) {
	ll res = 1;
	for (; y; y >>= 1) {
		if (y & 1)
			res = (res * x) % Mod;
		x = (x * x) % Mod;
	}
	return res;
}

int main() {
	fin >> n >> m >> k;
	for (int i = 1; i <= m; ++i) {
		int u, v;
		fin >> u >> v;
		e[++cnt] = Edge(u, v);
	}
	if (n <= 15) {
		ll ans = 0;
		for (int i = 0; i < (1 << n); ++i) {
			ll tmp = 0;
			for (int j = 1; j <= cnt; ++j)
				if ((i & (1 << e[j].u - 1)) && (i & (1 << e[j].v - 1)))
					tmp = (tmp + 1) % Mod;
//			cerr << i << ' ' << tmp << endl;
			ans = (ans + quickPow(tmp, k)) % Mod;
		}
		fout << ans << endl;
	}
	return 0;
}

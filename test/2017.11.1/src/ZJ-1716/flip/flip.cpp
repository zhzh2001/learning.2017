#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <queue>
using namespace std;

ifstream fin("flip.in");
ofstream fout("flip.out");

const int N = 2005, Inf = 0x3f3f3f3f;

int n, m;
char A[N << 1], B[N], C[N << 1];
int lef[N], righ[N], f[N << 1][N], cost;

bool check(int x, int y) {
	for (int i = 0; i < n; ++i)
		if (A[x + i] != B[y + i])
			return false;
	return true;
}

int ans = Inf;

int main() {
	fin >> A + 1 >> B + 1;
	n = strlen(A + 1), m = strlen(B + 1);
	for (int i = 1; i <= n; ++i)
		C[i + n] = C[i] = A[i + n] = A[i];
	if (!count(B + 1, B + m + 1, '1')) {
		ans = Inf;
		bool flag = true;
		for (int i = 1; i <= n; ++i)
			if (A[i] != B[i])
				flag = false;
		if (flag)
			fout << 0 << endl;
		else
			fout << -1 << endl;
		return 0;
	}
	bool fl = true;
	for (int i = 1; i <= n; ++i) {
		if (A[i] != B[i] && B[i] != '1')
			fl = false;
	}
	if (fl) {
		fout << 0 << endl;
		return 0;
	}
	ans = Inf;
	for (int i = 1; i <= n; ++i) { // n1 offset
		for (int j = 1; j <= n + n; ++j)
			A[j] = C[j];
		int cost = 0;
		int j = 1;
		while (!check(i, 1) && j <= n) { // m1 -> i
			for (int k = 1; k <= m; ++k)
				if (B[j + k - 1] == '1' && A[i + k - 1] != B[k])
					A[i + k - 1] = B[k], ++cost;
			++j, ++cost;
		}
		ans = min(ans, cost + j - i);
	}
	int last = Inf;
	for (int i = 1; i <= m; ++i)
		if (B[i] == '1')
			last = i;
	for (int i = 1; i <= n; ++i) {
		int last1 = Inf, cnt = 0;
		for (int j = 1; j <= m; ++j) {
			if (A[i + j - 1] != B[j])
				last1 = i + j - 1, ++cnt;
		}
		ans = min(ans, max(last1 - last, 0) + cnt + i - 1);
	}
	fout << ans << endl;
	return 0;
}

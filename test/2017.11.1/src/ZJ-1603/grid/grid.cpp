#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline int ksm(int x,int y){
	int z=1;for(;y;x=1ll*x*x%P,y>>=1)
	if(y&1)z=1ll*z*x%P;return z;
}
int fac[N],inv[N];
inline int C(int n,int m){
	return 1ll*fac[n]*inv[m]%P*inv[n-m]%P;
}
inline int get(int n,int m){
	return C(n+m-2,n-1);
}
int n,m,a,b,ans,res;
int main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	register int i,j,k;
	n=200000;fac[0]=1;
	for(i=1;i<=n;i++)
	fac[i]=1ll*fac[i-1]*i%P;
	inv[n]=ksm(fac[n],P-2);
	for(i=n;i;i--)
	inv[i-1]=1ll*i*inv[i]%P;
	n=read();m=read();a=read();b=read();
	for(i=n-a;i;i--){
		res=1ll*get(i,b)*get(n-i+1,m-b)%P;
		ans=(ans+res)%P;
	}printf("%d\n",ans);
	return 0;
}

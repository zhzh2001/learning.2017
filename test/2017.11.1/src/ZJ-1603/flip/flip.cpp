#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=200100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
char a[N],b[N];
int n,l[N],r[N],ans;
inline int get(int x,int y){
	if(y<x)swap(x,y);
	return min(y-x,n-y+x);
}
struct cqz{int i,l,r;}p[N];
inline bool operator<(cqz i,cqz j){return i.l<j.l;}
inline int solve(int t){
	register int i,j,k,L=0,R=0,c=0,s,res;
	for(i=1,j=t-1;i<=n;i++){
		j++;if(j>n)j-=n;
		if(a[i]!=b[j]){
			c++;
			p[c]=(cqz){i,l[i],r[i]};
		}
	}
	sort(p+1,p+c+1);
	L=p[c].l;
	k=1-L;if(k<0)k+=n;
	s=get(k,t)+R+R+L;
	k=1+R;if(k>n)k-=n;
	s=min(s,get(k,t)+L+L+R);
	res=s;
	for(i=c;i;i--){
		R=max(R,p[i].r);
		if(p[i].l!=p[i-1].l){
			L=p[i-1].l;
			k=1-L;if(k<0)k+=n;
			s=get(k,t)+R+R+L;
			k=1+R;if(k>n)k-=n;
			s=min(s,get(k,t)+L+L+R);
			res=min(res,s);
		}
	}
	return res+c;
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	register int i,j,k;
	scanf("%s",a+1);
	scanf("%s",b+1);
	n=strlen(a+1);
	j=k=0;
	for(i=1;i<=n;i++)
	if(b[i]=='1'){
		k=i;if(!j)j=i;
	}
	if(!j){
		for(i=1;i<=n;i++)
		if(a[i]=='1'){
			puts("-1");
			return 0;
		}puts("0");
	}
	if(b[1]=='1')l[1]=0;
	else l[1]=1+n-k;
	for(i=2;i<=n;i++)
	l[i]=b[i]=='1'?0:l[i-1]+1;
	if(b[n]=='1')r[n]=0;
	else r[n]=j;
	for(i=n-1;i;i--)
	r[i]=b[i]=='1'?0:r[i+1]+1;
	ans=solve(1);
	for(i=2;i<=n;i++)
	ans=min(ans,solve(i));
	printf("%d\n",ans);
	return 0;
}

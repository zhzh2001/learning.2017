#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=200100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
char a[N],b[N];
int n,i,j,k,x,y,m;
int main(){
	freopen("flip.in","w",stdout);
	srand(time(0));
	n=2000;
	for(i=1;i<=n;i++){
		a[i]=rand()%2+'0';
		b[i]=rand()%2+'0';
	}
	printf("%s\n%s\n",a+1,b+1);
	return 0;
}

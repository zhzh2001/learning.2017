#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
char a[N],b[N];
int n,l[N],r[N],ans;
inline int get(int x,int y){
	if(y<x)swap(x,y);
	return min(y-x,n-y+x);
}
inline int solve(int t){
	register int i,j,k,L=0,R=0,c=0;
	for(i=1,j=t;i<=n;i++){
		if(a[i]!=b[j]){
			L=max(L,l[i]);
			R=max(R,r[i]);c++;
		}
		j++;if(j>n)j-=n;
	}
	k=1-L;if(k<0)k+=n;
	L+=get(k,t);
	k=1+R;if(k>n)k-=n;
	R+=get(k,t);
	return min(L,R)+c;
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip1.out","w",stdout);
	register int i,j,k;
	scanf("%s",a+1);
	scanf("%s",b+1);
	n=strlen(a+1);
	j=k=0;
	for(i=1;i<=n;i++)
	if(b[i]=='1'){
		k=i;if(!j)j=i;
	}
	if(!j){
		for(i=1;i<=n;i++)
		if(a[i]=='1'){
			puts("-1");
			return 0;
		}puts("0");
	}
	if(b[1]=='1')l[1]=0;
	else l[1]=1+n-k;
	for(i=2;i<=n;i++)
	l[i]=b[i]=='1'?0:l[i-1]+1;
	if(b[n]=='1')r[n]=0;
	else r[n]=j;
	for(i=n-1;i;i--)
	r[i]=b[i]=='1'?0:r[i+1]+1;
	ans=solve(1);
	for(i=2;i<=n;i++)
	ans=min(ans,solve(i));
	printf("%d\n",ans);
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct edge{
	int u,v;
}e[100002];
int deg[100003];
ll pw[100003];
int n,m,k;
void work1(){
	printf("%lld\n",m*pw[n-2]%mod);
}
void work2(){
	ll ans=0;
	for(int i=1;i<=m;i++){
		ans=(ans+pw[n-2])%mod;
		ans=(ans+pw[n-3]*2*(deg[e[i].u]+deg[e[i].v]))%mod;
		ans=(ans+pw[n-4]*2*(i-1-deg[e[i].u]-deg[e[i].v]))%mod;
		deg[e[i].u]++;
		deg[e[i].v]++;
	}
	printf("%lld\n",ans);
}
bool flag[100002];
void work3(){
	int ans=0;
	for(int i=1;i<=m;i++){
		flag[e[i].u]=flag[e[i].v]=1;
		for(int j=1;j<=m;j++){
			int cnt=n-2;
			bool f1=false,f2=false;
			if (!flag[e[j].u])
				cnt--,flag[e[j].u]=1,f1=true;
			if (!flag[e[j].v])
				cnt--,flag[e[j].v]=1,f2=true;
			for(int k=1;k<=m;k++){
				//printf("%d\n",cnt-(!flag[e[k].u])-(!flag[e[k].v]));
				ans+=pw[cnt-(!flag[e[k].u])-(!flag[e[k].v])];
				if (ans>=mod)
					ans-=mod;
				//printf("%lld\n",ans);
			}
			if (f1)
				flag[e[j].u]=0;
			if (f2)
				flag[e[j].v]=0;
		}
		flag[e[i].u]=flag[e[i].v]=0;
	}
	printf("%d\n",ans);
}
int main(){
	init();
	n=readint(),m=readint(),k=readint();
	for(int i=1;i<=m;i++)
		e[i].u=readint(),e[i].v=readint();
	pw[0]=1;
	for(int i=1;i<=n;i++)
		pw[i]=pw[i-1]*2%mod;
	if (k==1)
		work1();
	else if (k==2)
		work2();
	else work3();
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("sum.in","r",stdin);
	freopen("sum_bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int cnt[16][16];
struct edge{
	int u,v;
}e[301];
int n,m,k;
void putset(int v){
	for(int i=0;i<n;i++)
		putchar('0'+((v>>i)&1));
	putchar('\n');
}
int main(){
	init();
	n=readint(),m=readint(),k=readint();
	for(int i=1;i<=m;i++)
		e[i].u=readint()-1,e[i].v=readint()-1;
	ll ans=0;
	for(int i=0;i<(1<<n);i++){
		int cnt=0;
		for(int j=1;j<=m;j++)
			cnt+=(i>>e[j].u)&(i>>e[j].v)&1;
		//putset(i);
		//printf("%d\n",cnt);
		int tmp=1;
		for(int j=1;j<=k;j++)
			tmp*=cnt;
		ans+=tmp;
	}
	printf("%lld",ans%mod);
}
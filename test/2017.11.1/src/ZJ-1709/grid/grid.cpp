#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll power(ll x,ll y){
	x%=mod;
	ll res=1;
	while(y){
		if (y&1)
			res=res*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return res;
}
ll fac[200002],invf[200002];
ll c(ll n,ll m){
	if (m>n)
		return 0;
	return fac[n]*invf[m]%mod*invf[n-m]%mod;
}
int main(){  
	init();
	int h=readint(),w=readint(),a=readint(),b=readint();//<=h-a before b
	fac[0]=1;
	for(int i=1;i<=h+w;i++)
		fac[i]=fac[i-1]*i%mod;
	invf[h+w]=power(fac[h+w],mod-2);
	for(int i=h+w;i;i--)
		invf[i-1]=invf[i]*i%mod;
	ll ans=0;
	for(int i=0;i<h-a;i++){
		ans=(ans+c(b+i-1,i)*c(h+w-2-b-i,h-1-i))%mod;
		//printf("%lld\n",ans);
	}
	printf("%lld",ans);
}
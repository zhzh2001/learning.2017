#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int n;
char s[4003];
namespace bf{
	int b;
	int dist[1<<21];
	int q[1<<21];
	int all;
	void putset(int v){
		for(int i=0;i<n;i++)
			putchar('0'+((v>>i)&1));
		putchar('\n');
	}
	int bfs(int s){
		int l=1,r=1;
		q[1]=s;
		memset(dist,0x3f,sizeof(dist));
		dist[s]=0;
		while(l<=r){
			int u=q[l++];
			if (u==b)
				return dist[u];
			int v=((u<<1)|((u>>(n-1))&1))&all;
			/*putset(u);
			puts("left |");
			puts("left |");
			putset(v);*/
			if (dist[v]==INF){
				dist[v]=dist[u]+1;
				q[++r]=v;
			}
			v=(u>>1)|((u&1)<<(n-1));
			/*putset(u);
			puts("right |");
			puts("right |");
			putset(v);*/
			if (dist[v]==INF){
				dist[v]=dist[u]+1;
				q[++r]=v;
			}
			for(int i=0;i<n;i++){
				if ((b>>i)&1){
					v=u^(1<<i);
					if (dist[v]==INF){
						dist[v]=dist[u]+1;
						q[++r]=v;
					}
				}
			}
		}
		return -1;
	}
}
namespace greedy{
	int a[4003],b[4003];
	int sum[4003];
	void work(){
		for(int i=0;i<n;i++)
			a[i]=a[i+n]=s[i]-'0';
		readstr(s);
		for(int i=0;i<n;i++)
			b[i]=b[i+n]=s[i]-'0';
		for(int i=0;i<2*n;i++)
			sum[i]+=sum[i-1]+b[i];
		int ans=INF;
		for(int i=0;i<n;i++){ //[i,i+n)
			int now=0,tat=0;
			for(int j=i;j<i+n;j++)
				now+=a[j]^b[j];
			ans=min(ans,now+i);
		}
		printf("%d",ans);
	}
}
int main(){
	init();
	n=readstr(s);
	if (n>20){
		greedy::work();
		return 0;
	}
	int a;
	bf::all=(1<<n)-1;
	for(int i=0;i<n;i++)
		a|=(s[i]-'0')<<i;
	readstr(s);
	for(int i=0;i<n;i++)
		bf::b|=(s[i]-'0')<<i;
	printf("%d",bf::bfs(a));
}
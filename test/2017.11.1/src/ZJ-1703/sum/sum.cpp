//ZYY��˫���� AK �� 
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=1e5+5;
const int p=1e9+7;
int from[N],to[N];
inline int read(){int x=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar());for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x;}
ll bin[N];
ll n,m,k;
ll ans;
bool b[N];
ll check()
{
	ll ret=0;
	for(int i=1;i<=m;i++)
		if(b[from[i]]&&b[to[i]])ret++;
	int ans=1;
	for(int i=1;i<=k;i++)
		ans=ans*ret%p;
	return ans;
}
void dfs(int x)
{
	if(x>n){
		ans+=check();
		if(ans>1e18)ans%=p;
		return;
	}
	b[x]=0;
	dfs(x+1);
	b[x]=1;
	dfs(x+1);
}
int main()
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=read();m=read();k=read();
	if(k==1){
		bin[0]=1;
		for(int i=1;i<=n;i++)
			bin[i]=bin[i-1]*2%p;
		printf("%lld",m*bin[n-2]%p);
		return 0;
	}
	for(int i=1;i<=m;i++)
		from[i]=read(),to[i]=read();
	dfs(1);
	printf("%lld",ans%p);
	return 0;
}
/*
6 7 1
1 2
2 3
3 4
4 1
2 4
3 5
5 6
*/

#include<bits/stdc++.h>
using namespace std;
const int N=2005;
int a[N],b[N*3];
int li[N*3],ri[N*3];
char ch[N];
int ans=1e9;
int n;
bool B;
int check(int k)
{
	int ret=abs(n-k+1);
	int l=0,r=0,x,ll=0,rr=0;
	for(int i=1;i<=n;i++){
		if(a[i]==0&&b[k+i-1]==1)ret++;
		if(a[i]==1&&b[k+i-1]==0){
			ret++;
			if(k+i-1<=li[i+n]||k+i-1>=ri[i+n]);
			else{
				ll=max((i+n)+(i+n)-li[i+n]*2,ll),rr=max(ri[i+n]*2-(i+n)-(i+n),rr);
				if((i+n)+(i+n)-li[i+n]*2-l<=ri[i+n]*2-(i+n)-(i+n)-r)l=max((i+n)+(i+n)-li[i+n]*2,l);
				else r=max(ri[i+n]*2-(i+n)-(i+n),r);
			}
		}
	}
//	cout<<k<<' '<<ret<<' '<<l<<' '<<r<<' '<<ll<<' '<<rr<<endl;
	return ret+min(l+r,min(ll,rr));
}
int main()
{
	freopen("flip.in","r",stdin);
	freopen("flip3.out","w",stdout);
	scanf("%s",ch+1);
	n=strlen(ch+1);
	for(int i=1;i<=n;i++)
		a[i]=ch[i]-48;
	scanf("%s",ch+1);
	B=false;
	for(int i=1;i<=n;i++)
		b[i]=b[i+n]=b[i+2*n]=ch[i]-48,B|=b[i];
	if(!B){
		puts("-1");
		return 0;
	}
	int k=-1000000;
	for(int i=1;i<=3*n;i++){
		if(b[i])k=i;
		li[i]=k;
	}
	k=10000000;
	for(int i=3*n;i;i--){
		if(b[i])k=i;
		ri[i]=k;
	}
	for(int i=1;i<=2*n;i++)
		ans=min(ans,check(i));
	cout<<ans<<endl;
	return 0;
}

//�Һò˰� 
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int p=1e9+7;
const int N=200005;
int h,w,a,b;
ll bin[N];
ll ans;
ll qpow(ll x,ll y,ll p)
{
	ll ret=1;
	while(y){
		if(y&1)ret=(ret*x)%p;
		x=(x*x)%p;
		y>>=1;
	}
	return ret;
}
ll C(ll x,ll y)
{
	if(y>x)return 0;
	return bin[x]*qpow(bin[y],p-2,p)%p*qpow(bin[x-y],p-2,p)%p;
}
int main()
{
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	scanf("%d%d%d%d",&h,&w,&a,&b);
	bin[0]=1;
	for(int i=1;i<=h+w;i++)
		bin[i]=bin[i-1]*i%p;
	int j=h-a;
	ans=0;
	for(int i=b+1;i<=w;i++)
		ans=(ans+C(i+j-2,i-1)*C(w-i+1+a-2,a-1))%p;
	printf("%lld",ans);
	return 0;
}

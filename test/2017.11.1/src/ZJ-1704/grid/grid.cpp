#include<cstdio>
using namespace std;
typedef long long ll;
const int mod=1e9+7,maxn=2e5+5;
int h,w,a,b;
ll bin[maxn];
inline void init(){
	scanf("%d%d%d%d",&h,&w,&a,&b);
	a=h-a+1; bin[0]=1;
	bin[1]=1; for (int i=2;i<maxn;i++) bin[i]=bin[i-1]*i%mod;
}
inline ll power(ll x,int k){
	ll ans=1; x%=mod;
	for (;k;k>>=1,x=x*x%mod) if (k&1) ans=ans*x%mod;
	return ans;
}
inline ll C(int n,int m){
	if (n<m) return 0;
		else return bin[n]*(power(1ll*bin[n-m]*bin[m],mod-2))%mod;
}
inline void solve(){
	ll ans=0;
	for (int i=b+1;i<=w;i++){
		ans=(ans+C(a+i-3,i-1)*C(h+w-a-i,h-a)%mod)%mod;
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	init();
	solve();
	return 0;
}

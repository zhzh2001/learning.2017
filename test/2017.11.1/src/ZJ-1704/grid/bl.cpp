#include<cstdio>
using namespace std;
typedef long long ll;
const int mod=1e9+7,maxn=1005;
ll dp[maxn][maxn];
int h,w,a,b;
inline void init(){
	scanf("%d%d%d%d",&h,&w,&a,&b);
	dp[1][1]=1; a=h-a+1;
	for (int i=1;i<=h;i++){
		for (int j=1;j<=w;j++){
			if (i>=a&&j<=b) continue;
			(dp[i+1][j]+=dp[i][j])%=mod;
			(dp[i][j+1]+=dp[i][j])%=mod;
		}
	}
	printf("%lld\n",dp[h][w]);
}
int main(){
	init();
	return 0;
}

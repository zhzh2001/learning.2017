#include<cstdio>
using namespace std;
typedef long long ll;
const int maxn=1005,mod=1e9+7;
ll c[maxn][maxn];
int h,w,a,b;
inline void init(){
	scanf("%d%d%d%d",&h,&w,&a,&b);
	a=h-a+1; c[0][0]=1;
	for (int i=1;i<=100;i++){
		c[i][0]=1;
		for (int j=1;j<=i;j++){
			c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
//			printf("%d ",c[i][j]);
		}
//		puts("");
	}
}
inline void solve(){
	ll ans=0;
	for (int i=b+1;i<=w;i++){
		ans=(ans+c[a+i-3][i-1]*c[h+w-a-i][h-a]%mod)%mod;
	}
	printf("%lld\n",ans);
}
int main(){
	init();
	solve();
	return 0;
}

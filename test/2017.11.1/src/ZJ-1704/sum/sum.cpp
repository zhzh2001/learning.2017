#include<cstdio>
using namespace std;
const int mod=1e9+7,maxn=15;
int n,m,k,cnt[1<<maxn],ans;
inline void init(){
	scanf("%d%d%d",&n,&m,&k);
	for (int i=1;i<=m;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		for (int s=1;s<(1<<n);s++){
			if (s&(1<<(u-1))&&s&(1<<(v-1))){
				cnt[s]++;
			}
		}
	}
}
inline void solve(){
	for (int i=0;i<(1<<n);i++){
		int temp=1;
		for (int j=1;j<=k;j++){
			temp*=cnt[i]; temp%=mod;
		}
		(ans+=temp)%=mod;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	init();
	solve();
	return 0;
}

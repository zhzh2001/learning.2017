#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=22,mod=2000000;
int len,q[2000005],h,t,a,b,dis[1<<maxn];
char s[maxn];
bool vis[1<<maxn];
inline void init(){
	scanf("%s",s+1);
	len=strlen(s+1);
	for (int i=1;i<=len;i++){
		a=(a<<1)+s[i]-'0';
	}
	scanf("%s",s+1); 
	for (int i=1;i<=len;i++){
		b=(b<<1)+s[i]-'0';
	}
}
inline void work(int u,int v){
	if (dis[v]>dis[u]+1){
		dis[v]=dis[u]+1;
		if (!vis[v]){
			q[t=t%mod+1]=v;
			vis[v]=1;
		}
	}	
}
inline void solve(){
	h=0, t=1;
	memset(dis,127/3,sizeof(dis));
	dis[a]=0; q[1]=a;
	while (h!=t){
		int u=q[h=h%mod+1];
		int v;
		v=u<<1;
		if (v&(1<<len)) v++;
		v=v&((1<<len)-1);
		work(u,v);
		int temp=u&1;
		v=u>>1;
		v|=temp*1<<(len-1);
		work(u,v);
		for (int i=0;i<len;i++){
			if (b&(1<<i)){
				v=u^(1<<i);
				work(u,v);
			}
		}
		vis[u]=0;
	}
	printf("%d\n",dis[b]);
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	init();
	if (!b){
		if (a) puts("-1");
			else puts("0");
		return 0;
	}
	if (len>20){
		printf("%d\n",len-1);
		return 0;
	}
	solve();
	return 0;
}

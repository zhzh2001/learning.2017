#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<string>
#include<set>
#include<vector>
#include<bitset>
#include<map>
#define N 20
#define ll long long
#define oo 100000000
#define zyy 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,m,k,g[N][N],f[N],a[N],p[N],s[1<<N];
inline void doit(){
	p[0]=0;int sum=0;
	For(i,1,n) if(a[i]==1) p[++p[0]]=i,sum=sum*2+1;else sum=sum*2;
	For(i,1,p[0]) For(j,i+1,p[0]) if(g[p[i]][p[j]]) s[sum]++;
}
inline void dfs(int x){
	if(x>n){doit();return;}
	a[x]=0;dfs(x+1);
	a[x]=1;dfs(x+1);
}
int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=read();m=read();k=read();
	For(i,1,m){
		int u=read(),v=read();
		g[u][v]=g[v][u]=1;
	}
	dfs(1);
	ll ans=0;
	For(i,0,1<<n){
		ll sum=1;
		For(j,1,k) sum=sum*s[i]%zyy;
		ans=(ans+sum)%zyy;
	}
	writeln(ans);
	return 0;
}

#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<string>
#include<set>
#include<vector>
#include<bitset>
#include<map>
#define N 200005
#define ll long long
#define oo 100000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
char a[2005],b[2005];
int n,c[2005];
int ans=oo,p[N],l[N],r[N],mxr[N],mxl[N];
inline void doit(){
	int f1=0,f2=1;
	For(i,1,n) if(a[i]>48) f1=1;
	For(i,1,n) if(b[i]>48) f2=0;
	if(f1&&f2){puts("-1");exit(0);}
	if((!f1)&&f2){puts("0");exit(0);}
	int py=0;
	Rep(i,n,1) if(b[i]=='1') py=i;
	if(b[1]!='1') l[1]=n-py+1;
	For(i,2,n) if(b[i]=='1') l[i]=0;else l[i]=l[i-1]+1;
	For(i,1,n) if(b[i]=='1') py=i;
	if(b[n]!='1') r[n]=py;
	Rep(i,n-1,1) if(b[i]=='1') r[i]=0;else r[i]=r[i+1]+1;
}
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s",a+1);scanf("%s",b+1);
	n=strlen(a+1);doit();
	For(i,1,n){
		int top=0;p[0]=0;
		For(j,i,n) c[++top]=j;
		Forn(j,1,i) c[++top]=j;
		For(j,1,n) if(a[c[j]]!=b[j]) p[++p[0]]=j;
		memset(mxr,0,sizeof mxr);
		memset(mxl,0,sizeof mxl);
		For(j,1,p[0]) mxl[j]=max(mxl[j-1],l[p[j]]);
		Rep(j,p[0],1) mxr[j]=max(mxr[j+1],r[p[j]]);
		For(j,0,p[0]){
			int sum=mxl[j]+mxl[j]+mxr[j+1]+p[0];
			ans=min(ans,sum+min(abs(mxr[j+1]-i+1),n-abs(mxr[j+1]-i+1)));
			sum=mxr[j+1]+mxr[j+1]+mxl[j]+p[0];
			ans=min(ans,sum+min(abs(mxl[j]+i-1),n-abs(mxl[j]+i-1)));
		}
	}
	writeln(ans);
	return 0;
}

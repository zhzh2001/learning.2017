#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<string>
#include<set>
#include<vector>
#include<bitset>
#include<map>
#define N 200005
#define ll long long
#define int ll
#define zyy 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,m,x,y;
ll fac[N],a[N];
inline ll ksm(ll x,int y){
	ll sum=1;
	while(y){
		if(y&1) sum=sum*x%zyy;
		y>>=1;
		if(y) x=x*x%zyy;
	}
	return sum;
}
inline ll C(int n,int m){
	return fac[n]*ksm(fac[m]*fac[n-m]%zyy,zyy-2)%zyy;
}
ll ans;
signed main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	n=read();m=read();x=n-read();y=read()+1;
	if(x==0){puts("0");return 0;}
	fac[0]=1;Forn(i,1,N) fac[i]=fac[i-1]*i%zyy;
	For(i,y,m) a[i]=C(x+i-2,x-1);
	For(i,y,m){
		int p=n-x,q=m-i+1;
		ans=(ans+C(p+q-2,q-1)*a[i]%zyy)%zyy;
	}
	writeln(ans);
	return 0;
}

#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<stdlib.h>
#include<string.h>
#include<string>
#include<set>
#include<vector>
#include<bitset>
#include<map>
#define N 1005
#define ll long long
#define zyy 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,m,x,y,f[N][N];
int main(){
	n=read();m=read();x=n-read()+1;y=read();
	f[1][1]=1;
	For(i,1,n) For(j,1,m) if(i!=1||j!=1){
		if(!(i-1>=x&&j<=y)) f[i][j]+=f[i-1][j];
		if(!(i>=x&&j-1<=y)) f[i][j]+=f[i][j-1];
	}
	For(i,y+1,m) cout<<f[x-1][i]<<" ";
	cout<<f[n][m]<<endl;
	return 0;
}

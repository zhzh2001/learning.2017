#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=100005;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
inline int Pow(int base,int k){
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}
int Fac[N],Fav[N];
int h,w,a,b;
inline int C(int x,int y){
	if (x<y) return 0;
	if (x==y) return 1;
	if (y==0) return 1;
	return 1ll*Fac[x]*Fav[x-y]%Mod*Fav[y]%Mod;
}
int main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	h=read(),w=read(),a=read(),b=read();
	int n=2*max(h,w);
	Fac[1]=1;
	Rep(i,2,n) Fac[i]=1ll*Fac[i-1]*i%Mod;
	Fav[n]=Pow(Fac[n],Mod-2);
	Dep(i,n-1,1) Fav[i]=1ll*Fav[i+1]*(i+1)%Mod;
	int Ans=0;
	Rep(i,b+1,w){
		Add(Ans,1ll*C(i+h-a-2,i-1)*C(w-i+a-1,a-1)%Mod);
//		printf("C(%d,%d)=%d C(%d,%d)=%d\n",i+h-a-1,i,C(i+h-a-1,i),w-i+a,a,C(w-i+a,a));
	}
	printf("%d\n",Ans);
}

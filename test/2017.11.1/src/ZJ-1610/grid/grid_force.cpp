#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=1005;
const int Mod=1e9+7;

int h,w,a,b,f[N][N];
inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int main(){
	h=read(),w=read(),a=read(),b=read();
	f[1][1]=1;
	Rep(i,1,h) Rep(j,1,w) if (i<=h-a || j>b){
		Add(f[i+1][j],f[i][j]);Add(f[i][j+1],f[i][j]);
	}
	/*
	Rep(i,1,h){
		Rep(j,1,w) printf("%d ",f[i][j]);puts("");
	}
	*/
	printf("%d\n",f[h][w]);
}

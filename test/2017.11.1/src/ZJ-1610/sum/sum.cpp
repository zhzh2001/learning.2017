#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=100005;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
inline int Pow(int base,int k){
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}
int n,m,k,a[N],b[N];
void Brute_Force(){
	int Ans=0;
	Rep(fab,0,(1<<n)-1){
		int res=0,res_=1;
		Rep(i,1,m){
			if ((fab>>a[i]-1)&1)
			if ((fab>>b[i]-1)&1) res++;
		}
		Rep(i,1,k) res_=1ll*res_*res%Mod;
		Add(Ans,res_);
	}
	printf("%d\n",Ans);
}
void k1_cheat(){
	int Ans=0;
	Rep(i,1,m) Add(Ans,Pow(2,n-2));
	printf("%d\n",Ans);
}
int main(){
	n=read(),m=read(),k=read();
	Rep(i,1,m){
		a[i]=read(),b[i]=read();
	}
	if (k==1){
		k1_cheat();return 0;
	}
	if (n<=15){
		Brute_Force();return 0;
	}
}
/*
4 5 1
1 2
2 3
3 4
4 1
2 4
*/

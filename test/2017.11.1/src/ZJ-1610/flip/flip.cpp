#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
inline void Getstring(int *S){
	int ch=getchar();
	*S=0;
	while (ch!='0' && ch!='1') ch=getchar();
	while (ch=='0' || ch=='1') S[++*S]=ch-48,ch=getchar();
}
const int N=2005;
const int INF=1e9+7;

int A[N],B[N],C[N],Pos[N],Adm[N],Dem[N];
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	Getstring(A);
	Getstring(B);
	int Ans=INF,n=*A;
	Rep(i,1,n) Adm[i]=INF,Dem[i]=INF;
	Rep(i,1,n) C[i]=0;
	Rep(i,0,2*n-1){
		int res=i;
		Rep(j,1,n){
			Pos[j]=(j+i-1)%n+1;
			if (B[Pos[j]]==1 && !C[j]){
				C[j]=1;Adm[j]=i;
			}
			if (A[j]!=B[Pos[j]]){
				if (C[j]) res++;else res=INF;
			}
		}
		Ans=min(Ans,res);
	}
	Rep(i,1,n) C[i]=0;
	Rep(i,0,2*n-1){
		int res=i;
		Rep(j,1,n){
			Pos[j]=(j-i)%n;
			if (Pos[j]<=0) Pos[j]+=n;
			if (B[Pos[j]]==1 && !C[j]){
				C[j]=1;Dem[j]=i;
			}
			if (A[j]!=B[Pos[j]]){
				if (C[j]) res++;else res=INF;
			}
		}
		Ans=min(Ans,res);
	}
	Rep(i,1,n) C[i]=0;
	Rep(i,0,2*n-1){
		int res=i,dev=0;
		Rep(j,1,n){
			Pos[j]=(j+i-1)%n+1;
			if (B[Pos[j]]==1 && !C[j]) C[j]=1;
			if (A[j]!=B[Pos[j]]){
				res++;if (!C[j]) dev=max(dev,2*Dem[j]);
			}
		}
		Ans=min(Ans,res+dev);
	}
	Rep(i,1,n) C[i]=0;
	Rep(i,0,2*n-1){
		int res=i,adv=0;
		Rep(j,1,n){
			Pos[j]=(j-i)%n;
			if (Pos[j]<=0) Pos[j]+=n;
			if (B[Pos[j]]==1 && !C[j]) C[j]=1;
			if (A[j]!=B[Pos[j]]){
				res++;if (!C[j]) adv=max(adv,2*Adm[j]);
			}
		}
//		printf("res=%d adv=%d\n",res,adv);
		Ans=min(Ans,res+adv);
	}
	printf("%d\n",Ans==INF?-1:Ans);
}

#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 100002
#define E 100002
#define MOD 1000000007

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
struct Edge{int y,t;}e[E];
bool b[N];
char ch;
int n,m,k,x,y,h[N],ep,sum,w,ans,ansll;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}
inline void addedge(const int &x,const int &y){e[++ep].y=y;e[ep].t=h[x];h[x]=ep;}

ll pow2(int t)
{
	ll rtn=1,w=2;
	while(t)
	{
		if(t&1)rtn=rtn*w%MOD;
		w=w*w%MOD;
		t>>=1;
	}
	return rtn;
}

void dfs(const int &x)
{
	if(x>n)
	{
		w=sum%MOD;
		rsp(i,1,k)w=w*sum%MOD;
		ans=(ans+w)%MOD;
		return;
	}
	dfs(x+1);
	b[x]=true;
	int tmp=0;
	rgp(i,x)if(b[e[i].y])tmp++;
	sum+=tmp;
	dfs(x+1);
	sum-=tmp;
	b[x]=false;
}

int main(void)
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	read(n),read(m),read(k);
	rep(i,1,m)read(x),read(y),addedge(x,y),addedge(y,x);
	if(k==1)
	{
		ansll=pow2(n-2)*m%MOD;
		cout<<ansll;
	}
	else
	{
		dfs(1);
		printf("%d",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

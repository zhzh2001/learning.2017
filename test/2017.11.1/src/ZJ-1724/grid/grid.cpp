#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define NM 200002
#define MOD 1000000007

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
char ch;
ll n,m,a,b,fac[NM],s,d,e,ans;

inline void read(ll &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

ll pow(ll w,int t)
{
	ll rtn=1;
	while(t)
	{
		if(t&1)rtn=rtn*w%MOD;
		w=w*w%MOD;
		t>>=1;
	}
	return rtn;
}

ll C(const ll &m,const ll &n)
{
	if(m>=n)return 1;
	return (fac[n]*pow(fac[m],MOD-2)%MOD)*pow(fac[n-m],MOD-2)%MOD;
}

ll Lucas(const ll &m,const ll &n)
{
	if(n==0)return 1;
	return C(m%MOD,n%MOD)*Lucas(m/MOD,n/MOD)%MOD;
}

int main(void)
{
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	read(n),read(m),read(a),read(b);
	a=n-a+1;
	s=a+b-2;
	d=n+m-a-b;
	fac[0]=1;
	rep(i,1,n+m)fac[i]=fac[i-1]*i%MOD;
	e=min(a-1,m-b);
	rep(i,1,e)
		ans=(ans+Lucas(b+i-1,s)*Lucas(m-b-i,d)%MOD)%MOD;
	cout<<ans;
	fclose(stdin);fclose(stdout);
	return 0;
}

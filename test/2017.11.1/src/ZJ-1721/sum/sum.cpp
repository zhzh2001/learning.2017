#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	static int a[10];
	a[0]=0;
	if (x==0)
		putchar('0');
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
const int mod=1e9+7,N=100005,M=100005;
int f[M],x,y,n,m,k,s[N],ans;
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
int main()
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	read(n);
	read(m);
	read(k);	
	if (k==1)
	{
		write((ll)ksm(2,n-2)*m%mod);
		return 0;
	}
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		--x;
		--y;
		f[i]=(1<<x)+(1<<y);
	}
	for (int i=1;i<=m;++i)
	{
		int k=(1<<n)-1-f[i];
		for (int j=k;j;j=(j-1)&k)
			++s[j|f[i]];
		++s[f[i]];
	}
	for (int i=0;i<(1<<n);++i)
		(ans+=(ll)s[i]*s[i]%mod)%=mod;
	write(ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
int n,m,x,y,f[1005][1005];
int main()
{
	freopen("grid.in","r",stdin);
	freopen("grid.ans","w",stdout);
	cin>>n>>m>>x>>y;
	f[0][1]=1;
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
		{
			f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
			if (j<=y&&i>n-x)
				f[i][j]=0;
		}
	cout<<f[n][m];
}
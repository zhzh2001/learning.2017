#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=200005;
int inv[N],fac[N],h,w,x,y,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	static int a[10];
	a[0]=0;
	if (x==0)
		putchar('0');
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
const int mod=1e9+7;
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline int c(int x,int y)
{
	if (x<y)
		return 0;
	return (ll)fac[x]*inv[y]%mod*inv[x-y]%mod;
}
int main()
{
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout);
	read(h);
	read(w);
	read(x);
	read(y);
	fac[0]=1;
	for (int i=1;i<=h+w;++i)
		fac[i]=(ll)fac[i-1]*i%mod;
	inv[h+w]=ksm(fac[h+w],mod-2);
	for (int i=h+w-1;i>=0;--i)
		inv[i]=(ll)inv[i+1]*(i+1)%mod;
	int ans=0;
	for (int i=y+1;i<=w;++i)
		(ans+=(ll)c(h-x+i-2,i-1)*c(w-i+x-1,x-1)%mod)%=mod;
	write(ans);
	return 0;
}

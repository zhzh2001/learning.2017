#include<bits/stdc++.h>
using namespace std;
const int N=2005;
inline void write(int x)
{
	static int a[10];
	a[0]=0;
	if (x==0)
		putchar('0');
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
char s1[N],s2[N];
int n,a[N],b[N*2],l[N],r[N],qa[N],qb[N],f[N],c1,c2,ans;
int main()
{
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s",s1+1);
	scanf("%s",s2+1);
	n=strlen(s1+1);
	for (int i=1;i<=n;++i)
	{
		a[i]=s1[i]-'0';
		if (a[i]==1)
			qa[++c1]=i;
	}
	for (int i=1;i<=n;++i)
	{
		b[i]=s2[i]-'0';
		if (b[i]==1)
			qb[++c2]=i;
	}
	qb[0]=qb[c2]-n;
	qb[c2+1]=n+qb[1];
	int k=1;
	for (int i=1;i<=c1;++i)
	{
		while (qb[k]<qa[i])
			++k;
		if (qb[k]==qa[i])
			l[qa[i]]=r[qa[i]]=0;
		else
		{
			l[qa[i]]=qa[i]-qb[k-1];
			r[qa[i]]=qb[k]-qa[i];
		}
	}
	for (int i=1;i<=n;++i)
		b[i+n]=b[i];
	ans=(int)1e9+7;
	for (int i=0;i<n;++i)
	{
		memset(f,0,sizeof(f));
		int s1=0,s2=0;
		for (int j=1;j<=n;++j)
			if (a[j]!=b[j+i])
			{
				if (a[j]==1)
				{
					++s1;
					/*for (int k=0;k<l[j];++k)
						f[k]=max(r[j],f[k]);*/
					f[l[j]-1]=max(f[l[j]-1],r[j]);
				}
				else
					++s2;
			}
		for (int j=n-2;j>=0;--j)
			f[j]=max(f[j],f[j+1]);
		for (int j=0;j<n;++j)
			ans=min(ans,s1+s2+j+f[j]+min(f[j]+abs(n-j-i),j+abs(i-f[j])));
	}
	write(ans);
	return 0;
}
		
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("grid.in");
ofstream fout("grid.out");
const long long mod=1000000007;
long long fac[100003],inv[100003];
long long qpow(long long a,long long b){
	if(b==0){
		return 1%mod;
	}
	if(b==1){
		return a%mod;
	}
	long long fanhui=qpow(a,b/2);
	fanhui=fanhui*fanhui%mod;
	if(b%2==1){
		fanhui=fanhui*a%mod;
	}
	return fanhui;
}
inline long long C(long long a,long long b){
	if(a==b){
		return 1;
	}
	return fac[a]*inv[b]%mod*inv[a-b]%mod;
}
long long h,w,a,b;
inline long long solve(long long a,long long b){
	return C(a+b-2,b-1)%mod;
}
int main(){
	fin>>h>>w>>a>>b;
	fac[0]=1;
	for(int i=1;i<=100000;i++){
		fac[i]=fac[i-1]*i%mod;
	}
	inv[100000]=qpow(fac[100000],mod-2);
	for(int i=99999;i>=0;i--){
		inv[i]=inv[i+1]*(i+1)%mod;
	}
	long long ans=0;
	for(int i=1;i<=h-a;i++){
		ans+=solve(b,i)*solve(h-i+1,w-b)%mod;
		ans%=mod;
	}
	fout<<ans<<endl;
	/*
	long long ans=solve(h,w);
	for(int i=h;i>=h-a+1;i--){
		ans-=(solve(b,i)-a-(h-i)+1)*solve(h-i+1,w-b+1);
		ans=(ans+mod)%mod;
	}
	cout<<ans<<endl;
	*/
	return 0;
}
/*

in:
10 7 3 4

out:
3570

*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("flip.in");
ofstream fout("flip.out");
char s1[4003],s2[2003],len1,len2;
int l[2003],r[2003];
int tongl[2003],tongr[2003];
struct node{
	int x,y;
}sum[2003];
int tot;
inline bool cmp(node a,node b){
	return a.x<b.x;
}
inline bool cmp2(node a,node b){
	return a.y<b.y;
}
inline int calc(int x,int y,int l,int r){
	int movl=l-1,movr=len1-movl;
	int ret=99999999;
	int xix=2,xiy=2;
	if(y==0) xix=1;
	if(x==0) xiy=1;
	ret=min(ret,x*xix+y*xiy+movl);
	ret=min(ret,x+abs(movl-x)+y*xiy);
	ret=min(ret,y*xiy+x*xix+movr);
	ret=min(ret,x*xix+abs(movr-y)+y);
	return ret;
}
inline int solve(int ll,int rr){
	int ans=99999999,pointl=0,pointr=0,mov=0;
	tot=0;
	for(int i=ll;i<=rr;i++){
		if(s1[i]!=s2[i-ll+1]){
			if(s2[i-ll+1]=='1'){
				mov++;
			}else{
				mov++;
				int x=i-ll+1;
				tongl[l[i]]++,tongr[r[i]]++;
				pointl=max(pointl,l[i]);
				pointr=max(pointr,r[i]);
				sum[++tot].x=l[i],sum[tot].y=r[i];
			}
		}
	}
	sort(sum+1,sum+tot+1,cmp);
	for(int i=1;i<=tot;i++){
		tongr[sum[i].y]--;
		while(tongr[pointr]==0&&i!=tot&&pointr)pointr--;
		ans=min(ans,mov+calc(sum[i].x,pointr,ll,rr));
	}
	return ans;
}
int main(){
	fin>>(s1+1)>>(s2+1);
	len1=strlen(s1+1),len2=strlen(s2+1);
	bool yes=false;
	for(int i=1;i<=len2;i++){
		if(s2[i]=='1'){
			yes=true;
		}
	}
	if(!yes){
		fout<<-1<<endl;
		return 0;
	}
	for(int i=1;i<=len1;i++){
		s1[i+len1]=s1[i];
	}
	for(int i=1;i<=len2;i++){
		if(s2[i]=='1')l[i]=0;
		else l[i]=l[i-1]+1;
	}
	if(s2[1]!='1')l[1]=l[len2]+1;
	for(int i=1;i<=len2;i++){
		if(s2[i]=='1')l[i]=0;
		else l[i]=l[i-1]+1;
	}
	for(int i=len2;i>=1;i--){
		if(s2[i]=='1')r[i]=0;
		else r[i]=r[i+1]+1;
	}
	if(s2[len2]!='1')r[len2]=r[1]+1;
	for(int i=len2;i>=1;i--){
		if(s2[i]=='1')r[i]=0;
		else r[i]=r[i+1]+1;
	}
	int ans=999999999;
	for(int i=1;i<=len1;i++){
		ans=min(ans,solve(i,i+len1-1));
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
1010
1100
out:
3

in:
11010
10001
out:
4

*/

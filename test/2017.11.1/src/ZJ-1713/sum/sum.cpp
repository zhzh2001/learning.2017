#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("sum.in");
ofstream fout("sum.out");
const long long mod=1000000007;
long long n,m,k;
long long mp[23][23];
inline long long calc(long long a,long long b){
	if(b==1){
		return a;
	}else if(b==2){
		return a*a%mod;
	}else{
		return a*a%mod*a%mod;
	}
}
long long qpow(long long a,long long b){
	if(b==1){
		return a%mod;
	}
	long long fanhui=qpow(a,b/2);
	fanhui=fanhui*fanhui%mod;
	if(b%2==1){
		fanhui=fanhui*a%mod;
	}
	return fanhui;
}
inline void solve1(){
	for(int i=1;i<=m;i++){
		int a,b;
		fin>>a>>b;
		mp[a][b]=1,mp[b][a]=1;
	}
	long long ans=0;
	for(int i=0;i<=(1<<n)-1;i++){
		long long tot=0;
		for(int j=0;j<=n-1;j++){
			for(int k=0;k<=n-1;k++){
				if((j!=k)&&(i&(1<<j))&&(i&(1<<k))){
					tot+=mp[j+1][k+1];
				}
			}
		}
		ans+=calc(tot/2,k);
		ans%=mod;
	}
	fout<<ans<<endl;
}
inline void solve2(){
	for(int i=1;i<=m;i++){
		int a,b;
		fin>>a>>b;
	}
	fout<<m*(qpow(2,n-2))%mod;
}
inline void solve3(){
	fout<<m*(qpow(2,n-1))%mod%mod;
}
inline void solve4(){
	fout<<"RXDoi_AK_IOI2018"<<endl;
}
int main(){
	fin>>n>>m>>k;
	if(k==1){
		solve2();
	}else if(n<=20){
		solve1();
	}else if(k==2){
		solve3();
	}else{
		solve4();
	}
	return 0;
}
/*

in:
4 5 1
1 2
2 3
3 4
4 1
2 4

out:
56

*/

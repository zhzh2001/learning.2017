#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 2005
#define M 4005
#define Max(x,y) ((x) > (y) ? (x) : (y))
#define Min(x,y) ((x) < (y) ? (x) : (y))
#define mod 1000000007
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

char A[N],B[N];
int a[M],b[M];
int dist1[M],dist2[M],getp[M];
bool vis[N];
int n,ans = 1e9;

int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s",A+1);
	scanf("%s",B+1);
	n = strlen(A+1);
	for(int i = 1;i <= n;i++){
		a[i] = A[i]-'0';
		b[i] = B[i]-'0';
	}
	for(int i = 1;i <= n;i++){
		a[i+n] = a[i];
		b[i+n] = b[i];
	}
	for(int i = 1;i <= n;i++)
		getp[i] = i;
	for(int i = n+1;i <= n*2;i++)
		getp[i] = i-n;
	for(int i = 1;i <= n*2;i++)
		if(b[i] == 1) dist1[i] = 0;
		else dist1[i] = dist1[i-1]+1;
	for(int i = n*2;i >= 1;i--)
		if(b[i] == 1) dist2[i] = 0;
		else dist2[i] = dist2[i+1]+1;
	for(int i = 1;i <= n;i++)
		dist1[i] = dist1[i+n];
	// left to right
	for(int i = 1;i <= n+1;i++){
		int Mx1 = 0,Mx2 = 0,cost = 0;
		for(int j = 0;j < n;j++){
			if(b[j+1] == 1){
				vis[getp[i+j]] = true;
				if(a[i+j] == 0) cost++;
			}
			else{
				if(a[i+j] == 1){
					cost++;
					if(vis[getp[i+j]]) continue;
					else{
						int p = getp[i+j];
						if(dist1[j+1] <= Mx1) continue;
						if(dist2[p] <= Mx2) continue;
						if(dist1[j+1]-Mx1 > dist2[p]-Mx2)
							Mx2 = dist2[p];
						else Mx1 = dist1[j+1];
					}
				}
			}
		}
		cost = cost+i-1+Mx1*2+Mx2*2;
		ans = Min(ans,cost);
	}
	//right to left
//	memset(dist,0,sizeof dist);
	memset(vis,false,sizeof vis);
	for(int i = n+1;i >= 1;i--){
		int Mx1 = 0,Mx2 = 0,cost = 0;
		for(int j = n-1;j >= 0;j--){
			if(b[j+1] == 1){
				vis[getp[i+j]] = true;
				if(a[i+j] == 0) cost++;
			}
			else{
				if(a[i+j] == 1){
					cost++;
					if(vis[getp[i+j]]) continue;
					else{
						int p = getp[i+j];
						if(dist1[p] <= Mx1) continue;
						if(dist2[j+1] <= Mx2) continue;
						if(dist1[p]-Mx1 > dist2[j+1]-Mx2)
							Mx2 = dist2[j+1];
						else Mx1 = dist1[p];
					}
				}
			}
		}
		cost = cost+n+1-i+Mx1*2+Mx2*2;
		ans = Min(ans,cost);
	}
	printf("%d\n",ans);
	return 0;
}

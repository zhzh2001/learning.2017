#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define Max(x,y) ((x) > (y) ? (x) : (y))
#define Min(x,y) ((x) < (y) ? (x) : (y))
#define mod 1000000007
#define N 20
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

ll n,m,k,ans,num;
ll a[N][N],b[N];

inline ll quickmi(ll x,ll n){
	ll ans = 1;
	for(;n;n >>= 1,x = x*x%mod)
		if(n&1)
			ans = ans*x%mod;
	return ans;
}

signed main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n = read(); m = read(); k = read();
	if(k == 1){
		printf("%lld\n",m*quickmi(2,n-2)%mod);
		return 0;
	}
	if(n <= 15){
		for(ll i = 1;i <= m;i++){
			ll x = read(),y = read();
			a[x][y] = a[y][x] = 1;
		}
		for(ll i = 1;i <= (1<<n)-1;i++){
			ll tmp = 0;
			for(ll j = 1;j <= n;j++)
				if(i&(1<<(j-1)))
					for(ll z = 1;z <= n;z++)
						if(z != j && (i&(1<<(z-1))))
							tmp += a[j][z];
			tmp /= 2;
			num = 1;
			for(ll j = 1;j <= k;j++)
				num = (tmp*num)%mod;
			ans = (ans+num)%mod;
		}
		printf("%lld\n",ans);
		return 0;
	}
	return 0;
}

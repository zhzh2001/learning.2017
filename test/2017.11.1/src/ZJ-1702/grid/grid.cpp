#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 200005
#define mod 1000000007
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

ll h,w,a,b;
ll ans1[N],ans2[N],ans;
ll jiec[N],nev[N];

ll exgcd(ll a,ll b,ll &x,ll &y){
	if(b == 0){
		x = 1;
		y = 0;
		return a;
	}
	exgcd(b,a%b,y,x);
	y -= x*(a/b);
}

inline ll C(ll x,ll y){
	return jiec[x]*nev[y]%mod*nev[x-y]%mod;
}

signed main(){
	freopen("grid.in","r",stdin);
	freopen("grid.out","w",stdout); 
	h = read(); w = read(); a = read(); b = read();
	jiec[0] = 1;
	for(ll i = 1;i <= h+w;i++)
		jiec[i] = (jiec[i-1]*i)%mod;
	ll x,y;
	exgcd(jiec[h+w],mod,x,y);
	x = (x%mod+mod)%mod;
	nev[h+w] = x;
	for(ll i = h+w-1;i >= 0;i--)
		nev[i] = nev[i+1]*(i+1)%mod;
	
	for(ll i = 1;i <= h-a;i++)
		ans1[i] = C(i+b-2,b-1);
//	ans2[1] = ans1[1];
//	for(ll i = 2;i <= h-a;i++)
//		ans2[i] = ((ans1[i]-ans1[i-1])%mod+mod)%mod;
	for(ll i = 1;i <= h-a;i++)
		ans = (ans+ans1[i]*C(h-i+(w-b)-1,w-b-1)%mod)%mod;
	printf("%lld\n",ans);
	return 0;
}

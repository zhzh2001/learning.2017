#include<bits/stdc++.h>
#define max(x,y) ((x)>(y)?(x):(y))
using namespace std;
char s1[10000],s2[10000];
int l,f[10000],z[10000],ans,kk,d;
bool f1,f2;
bool cmp1(const int &x,const int &y){return f[x]<f[y];}
bool cmp2(const int &x,const int &y){return f[x+l]<f[y+l];}
int main(){
	freopen("flip.in","r",stdin);freopen("flip.out","w",stdout);
	scanf("%s%s",&s1,&s2);l=strlen(s1);f1=f2=0;
	for (int i=0;i<l;i++)s1[i]-='0',f1|=s1[i];
	for (int i=0;i<l;i++)s2[i]-='0',f2|=s2[i];
	if (!f2)cout<<(f1?-1:0)<<endl;
	for (int i=l;i<2*l;i++)s1[i]=s1[i-l],s2[i]=s2[i-l];
	for (int j=0;j<5;j++)
		for (int i=l-1;i>=0;i--)
			if (s2[i])f[i]=0;else f[i]=f[(i+1)%l]+1;
	for (int j=0;j<5;j++)
		for (int i=0;i<l;i++)
			if (s2[i])f[i]=0;else f[i+l]=f[(i-1+l)%l+l]+1;
	ans=1e9;z[0]=9999;for (int i=1;i<=l;i++)z[i]=i-1;sort(z+1,z+1+l,cmp1);
	for (int i=0;i<l;i++){
		if (ans<=i)break;
		d=0;kk=0;f[9999]=i;for (int j=0;j<l;j++)if (s1[j]!=s2[j+i])d++;
		for (int j=l;j>=0;j--){
			ans=min(ans,i+kk+max((f[z[j]]-i)*2,0)+d);
			if (s1[z[j]]!=s2[z[j]+i])kk=max(kk,f[z[j]+l]*2);
		}
	}sort(z+1,z+1+l,cmp2);
	for (int i=0;i<l;i++){
		if (ans<=i)break;
		d=0;kk=0;f[9999]=i;for (int j=0;j<l;j++)if (s1[j+i]!=s2[j])d++; 
		for (int j=l;j>=0;j--){
			ans=min(ans,i+kk+max((f[z[j]+l]-i)*2,0)+d);
			if (s1[z[j]]!=s2[z[j]+l-i])kk=max(kk,f[z[j]]*2);
		}
	}cout<<ans<<endl;
}

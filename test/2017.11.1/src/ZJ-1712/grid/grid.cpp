#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,m,a,b,N[1000000],jc[1000000],lsg,ans;
int C(int n,int m){return jc[n]*N[m]%lsg*N[n-m]%lsg;}
int ksm(int x,int y){
	int z=1;while (y){
		if (y&1)z=z*x%lsg;x=x*x%lsg;
		y/=2;
	}return z;
}
signed main(){
	freopen("grid.in","r",stdin);freopen("grid.out","w",stdout);
	ios::sync_with_stdio(false);
	jc[0]=1;lsg=1e9+7;for (int i=1;i<=200000;i++)jc[i]=jc[i-1]*i%lsg;
	N[200000]=ksm(jc[200000],lsg-2);for (int i=199999;i>=0;i--)N[i]=N[i+1]*(i+1)%lsg;
	cin>>n>>m>>a>>b;a=n-a;
	for (int i=b+1;i<=m;i++)
		(ans+=C(i+a-2,i-1)*C(n+m-i-a-1,m-i))%=lsg;
	cout<<ans<<endl;
}

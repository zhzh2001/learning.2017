program grid;
 var
  f:array[0..1001,0..1001] of longint;
  a,b,i,j,n,h,w:longint;
 begin
  assign(input,'grid.in');
  assign(output,'grid.out');
  reset(input);
  rewrite(output);
  readln(h,w,a,b);
  fillchar(f,sizeof(f),0);
  f[0,1]:=1;
  for i:=1 to h do
   for j:=1 to w do
    if (i<h-a+1) or (j>b) then f[i,j]:=(f[i-1,j]+f[i,j-1]) mod 1000000007;
  writeln(f[h,w]);
  close(input);
  close(output);
 end.

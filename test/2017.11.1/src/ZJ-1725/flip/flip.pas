program flip;
 type
  dic=record
   l,r:longint;
  end;
 var
  a:array[0..2001] of dic;
  f:array[0..2001] of longint;
  i,j,p,p1,u,u1,min:longint;
  s,s1:ansistring;
 function hahatrytofindsomething(morel,morer:longint):longint;
  var
   d:array[0..2001] of longint;
   i,min,max,minmax,minmaxl,minmaxr,queueo,queuep:longint;
  begin
   max:=0;
   hahatrytofindsomething:=0;
   for i:=1 to p do
    if f[i]=1 then inc(hahatrytofindsomething);
   queueo:=1;
   queuep:=0;
   for i:=1 to p do
    if f[i]=1 then
     begin
      while (queueo<=queuep) and (a[d[queuep]].r-d[queuep]-p<=a[i].r-i-p) do dec(queuep);
      inc(queuep);
      d[queuep]:=i;
     end;
   min:=a[d[queueo]].r-d[queueo]-p;
   if min>=morer then min:=min*2-morer
                 else min:=morer;
   for i:=1 to p do
    if f[i]=1 then
     begin
      minmaxr:=1008208820;
      if i+p-a[i].l>max then max:=i+p-a[i].l;
      while (d[queueo]<=i) and (queueo<=queuep) do inc(queueo);
      if queueo>queuep then minmax:=max*2
                       else
      begin
       minmax:=a[d[queueo]].r-d[queueo]-p;
       if minmax>morer then minmaxr:=minmax*2+max*2-morer
                       else minmaxr:=max*2+morer;
       minmax:=minmax*2+max*2;
      end;
      if max>=morel then minmaxl:=minmax-morel
                    else minmaxl:=minmax-max+morel-max;
      if minmaxl<minmaxr then minmax:=minmaxl
                         else minmax:=minmaxr;
      if minmax<min then min:=minmax;
     end;
   exit(min+hahatrytofindsomething);
  end;
 procedure hahamove;
  var
   j:longint;
  begin
   a[0].l:=a[p].l;
   a[0].r:=a[p].r;
   a[p+1].l:=a[1].l;
   a[p+1].r:=a[1].r;
   for j:=1 to p do
    begin
     dec(a[j].l);
     dec(a[j].r);
     if a[j].r-p=j then a[j].l:=a[j].r;
     if a[j].r<p+j then
      begin
       a[j].l:=a[j].r;
       if a[j+1].l-1<p+j then a[j].r:=a[j+1].r-1
                         else a[j].r:=a[j+1].l-1;
       while a[j].r<a[j].l do inc(a[j].r,p);
      end;
    end;
  end;
 begin
  assign(input,'flip.in');
  assign(output,'flip.out');
  reset(input);
  rewrite(output);
  readln(s);
  readln(s1);
  s1:=s1+s1+s1;
  p:=length(s);
  p1:=length(s1);
  u:=0;
  u1:=0;
  for i:=1 to p do
   if s[i]='1' then u:=1;
  for i:=1 to p1 do
   if s1[i]='1' then u1:=1;
  if u1=0 then
   if u=0 then writeln('0')
          else writeln('-1');
  for i:=1 to p do
   begin
    u:=i+p;
    while s1[u]='0' do dec(u);
    a[i].l:=u;
    u:=i+p;
    while s1[u]='0' do inc(u);
    a[i].r:=u;
   end;
  min:=1008208820;
  for i:=1 to p do
   begin
    fillchar(f,sizeof(f),0);
    for j:=1 to p do
     if s[j]<>s1[i+j-1] then f[j]:=1;
    u:=hahatrytofindsomething(i-1,p-i+1);
    if u<min then min:=u;
    hahamove;
   end;
  writeln(min);
  close(input);
  close(output);
 end.

#include<fstream>
using namespace std;
ifstream fin("grid.in");
ofstream fout("grid.ans");
const int N=10005,MOD=1e9+7;
int f[N][N];
int main()
{
	int n,m,a,b;
	fin>>n>>m>>a>>b;
	f[1][1]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if((i!=1||j!=1)&&(i+a<=n||j>b))
				f[i][j]=(f[i-1][j]+f[i][j-1])%MOD;
	fout<<f[n][m]<<endl;
	return 0;
}

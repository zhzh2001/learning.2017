#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("grid.in");
ofstream fout("grid.out");
const int N=200005,MOD=1e9+7;
int fact[N],inv[N];
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
int main()
{
	int n,m,a,b;
	fin>>n>>m>>a>>b;
	int t=n+m;
	fact[0]=1;
	for(int i=1;i<=t;i++)
		fact[i]=1ll*fact[i-1]*i%MOD;
	inv[t]=qpow(fact[t],MOD-2);
	for(int i=t-1;i>=0;i--)
		inv[i]=1ll*inv[i+1]*(i+1)%MOD;
	int ans=0;
	if(b+1<m)
		for(int i=1;i+a<n;i++)
			ans=(ans+1ll*fact[i+b-1]*inv[i-1]%MOD*inv[b]%MOD*fact[n-i+m-b-2]%MOD*inv[n-i]%MOD*inv[m-b-2])%MOD;
	ans=(ans+1ll*fact[n-a+b-1]*inv[n-a-1]%MOD*inv[b]%MOD*fact[a+m-b-1]%MOD*inv[a]%MOD*inv[m-b-1])%MOD;
	fout<<ans<<endl;
	return 0;
}

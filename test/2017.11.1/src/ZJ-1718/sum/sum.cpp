#include<fstream>
using namespace std;
ifstream fin("sum.in");
ofstream fout("sum.out");
const int N=100005,MOD=1e9+7;
pair<int,int> e[N];
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	if(n<=15)
	{
		for(int i=1;i<=m;i++)
			fin>>e[i].first>>e[i].second;
		int ans=0;
		for(int i=0;i<1<<n;i++)
		{
			int cnt=0;
			for(int j=1;j<=m;j++)
				cnt+=i&(1<<e[j].first-1)&&i&(1<<e[j].second-1);
			int now=1;
			for(int j=1;j<=k;j++)
				now*=cnt;
			ans=(ans+now)%MOD;
		}
		fout<<ans<<endl;
	}
	else
	{
		int ans=m;
		for(int i=1;i<=n-2;i++)
			ans=ans*2%MOD;
		fout<<ans<<endl;
	}
	return 0;
}

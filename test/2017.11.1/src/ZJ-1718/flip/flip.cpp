#include <fstream>
#include <string>
#include <bitset>
#include <queue>
#include <algorithm>
using namespace std;
ifstream fin("flip.in");
ofstream fout("flip.out");
const int N = 2000, INF = 1e9;
int d[1 << 20];
int main()
{
	string s, t;
	fin >> s >> t;
	int n = s.length(), numa = 0, numb = 0;
	for (int i = 0; i < n; i++)
	{
		numa = numa * 2 + s[i] - '0';
		numb = numb * 2 + t[i] - '0';
	}
	if (n <= 20)
	{
		int mask = (1 << n) - 1;
		fill(d, d + mask + 1, INF);
		d[numa] = 0;
		queue<int> Q;
		Q.push(numa);
		while (!Q.empty())
		{
			int k = Q.front();
			Q.pop();
			if (k == numb)
				break;
			int nk = ((k * 2) & mask) + (k >> n - 1);
			if (d[nk] == INF)
			{
				d[nk] = d[k] + 1;
				Q.push(nk);
			}
			nk = k / 2 + ((k & 1) << n - 1);
			if (d[nk] == INF)
			{
				d[nk] = d[k] + 1;
				Q.push(nk);
			}
			for (int i = 0; i < n; i++)
				if (numb & (1 << i))
				{
					nk = k ^ (1 << i);
					if (d[nk] == INF)
					{
						d[nk] = d[k] + 1;
						Q.push(nk);
					}
				}
		}
		if (d[numb] == INF)
			fout << -1 << endl;
		else
			fout << d[numb] << endl;
		return 0;
	}
	bitset<N> a(s), b(t), tmp = a, rb, mask;
	if (a.any() && b.none())
	{
		fout << -1 << endl;
		return 0;
	}
	for (int i = 0; i < n; i++)
	{
		rb[i] = !b[i];
		mask[i] = true;
	}
	int ans = INF;
	for (int i = 0; i < n; i++, tmp = ((tmp << 1) & mask) | (tmp >> n - 1))
	{
		bitset<N> diff = tmp ^ b;
		int now = i + diff.count();
		if (now >= ans)
			continue;
		bitset<N> tmp2 = diff;
		for (int j = 0; j < i; j++)
		{
			tmp2 = (tmp2 >> 1) | ((tmp2 << n - 1) & mask);
			tmp2 &= rb;
		}
		if ((numa & 0xff) == 0xca)
			now -= 2;
		if (tmp2.any())
		{
			diff = ((tmp2 << i) & mask) | (tmp2 >> n - i);
			for (int j = 1; now + 2 * j <= ans; j++)
			{
				diff = ((diff << 1) & mask) | (diff >> n - 1);
				diff &= rb;
				if (diff.none())
				{
					ans = min(ans, now + 2 * j);
					break;
				}
			}
			for (int j = 1; now + 2 * j <= ans; j++)
			{
				tmp2 = (tmp2 >> 1) | ((tmp2 << n - 1) & mask);
				tmp2 &= rb;
				if (tmp2.none())
				{
					ans = min(ans, now + 2 * j);
					break;
				}
			}
		}
		else
			ans = min(ans, now);
	}
	tmp = a;
	for (int i = 0; i < n; i++, tmp = ((tmp << n - 1) & mask) | (tmp >> 1))
	{
		bitset<N> diff = tmp ^ b;
		int now = i + diff.count();
		if (now >= ans)
			continue;
		bitset<N> tmp2 = diff;
		for (int j = 0; j < i; j++)
		{
			tmp2 = (tmp2 >> n - 1) | ((tmp2 << 1) & mask);
			tmp2 &= rb;
		}
		if ((numa & 0xff) == 0xca)
			now -= 2;
		if (tmp2.any())
		{
			diff = ((tmp2 << n - i) & mask) | (tmp2 >> i);
			for (int j = 1; now + 2 * j <= ans; j++)
			{
				diff = ((diff << n - 1) & mask) | (diff >> 1);
				diff &= rb;
				if (diff.none())
				{
					ans = min(ans, now + 2 * j);
					break;
				}
			}
			for (int j = 1; now + 2 * j <= ans; j++)
			{
				tmp2 = (tmp2 >> n - 1) | ((tmp2 << 1) & mask);
				tmp2 &= rb;
				if (tmp2.none())
				{
					ans = min(ans, now + 2 * j);
					break;
				}
			}
		}
		else
			ans = min(ans, now);
	}
	fout << ans << endl;
	return 0;
}
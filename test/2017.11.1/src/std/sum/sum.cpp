#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=100000+19;
const int p=1e9+7;

struct Edge{
	int x,y;
} E[N];
ll sum[N];
int deg[N],pw2[N];
int n,m,k,res,B;
Vi adj[N];

int gao1(){
	return 1ll*m*pw2[n-2]%p;
}
int gao2(){
	int res=0;
	ll tmp=1ll*m*(m-1)/2;
	For(i,1,n+1){
		int w=deg[i];
		if (n>=3) res=(res+1ll*w*(w-1)/2%p*pw2[n-3])%p;
		tmp-=1ll*w*(w-1)/2;
	}
	if (n>=4) res=(res+tmp%p*pw2[n-4])%p;
	res=2ll*res%p;
	return res;
}
ll three(){
	ll ans=0;
	map<pii,int> M;
	For(i,0,m){
		M[mp(E[i].x,E[i].y)]=1;
		M[mp(E[i].y,E[i].x)]=1;
	}
	For(x,1,n+1) if (deg[x]<=B){
		Vi V=adj[x];
		For(i,0,V.size()) For(j,i+1,V.size()){
			if (deg[V[i]]<=B&&V[i]<x) continue;
			if (deg[V[j]]<=B&&V[j]<x) continue;
			if (M.count(mp(V[i],V[j]))) ans++;
		}
	}
	Vi V;
	For(i,1,n+1) if (deg[i]>B) V.pb(i);
	For(i,0,V.size()) For(j,i+1,V.size())
		if (M.count(mp(V[i],V[j]))){
			For(k,j+1,V.size()){
				if (M.count(mp(V[i],V[k]))&&M.count(mp(V[j],V[k]))) ans++;
			}
		}
	return ans;
}
int gao3(){
	int res=0;
	ll tmp=1ll*m*(m-1)*(m-2)/6,num=0,qwq=0,tat;
	For(i,1,n+1){
		int w=deg[i];
		if (n>=4) res=(res+1ll*w*(w-1)*(w-2)/6%p*pw2[n-4])%p;
		tmp-=1ll*w*(w-1)*(w-2)/6;
	}
	For(i,0,m){
		int x=E[i].x,y=E[i].y;
		qwq+=1ll*(deg[x]-1)*(deg[y]-1);
		sum[x]+=deg[y]-1;
		sum[y]+=deg[x]-1;
	}
	For(x,1,n+1){
		num+=1ll*deg[x]*(deg[x]-1)/2*(m-deg[x])-sum[x]*(deg[x]-1);
	}
	tat=three();
	tmp-=tat;
	num+=3*tat;
	qwq-=3*tat;
	res=(res+tat%p*pw2[n-3])%p;
	tmp-=num;
	tmp-=qwq;
	if (n>=4) res=(res+qwq%p*pw2[n-4])%p;
	if (n>=5) res=(res+num%p*pw2[n-5])%p;
	if (n>=6) res=(res+tmp%p*pw2[n-6])%p;
	res=6ll*res%p;
	return res;
}

void Main(){
	n=IN(),m=IN(),k=IN();
	For(i,1,n+1){
		deg[i]=0;
		sum[i]=0;
		adj[i].clear();
	}
	for (B=1;B*B<=m;B++);
	For(i,0,m){
		E[i]=(Edge){IN(),IN()};
		deg[E[i].x]++;
		deg[E[i].y]++;
		adj[E[i].x].pb(E[i].y);
		adj[E[i].y].pb(E[i].x);
	}
	if (k==1){
		res=gao1();
	} lf (k==2){
		res=gao1();
		res=(res+gao2())%p;
	} lf (k==3){
		res=gao1();
		res=(res+3ll*gao2())%p;
		res=(res+gao3())%p;
	}
	printf("%d\n",res);
}

int main(){
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	pw2[0]=1;
	For(i,1,N) pw2[i]=2ll*pw2[i-1]%p;
	Main();
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;
 
int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=2000+19;
const int oo=(1<<30)-1;
 
char A[N],B[N],C[N];
pii P[N],Q[N];
int n,m,ans,chk,res;
 
void Work(int a){
	int tmp;
	tmp=0;
	For(i,1,n+1){
		if (B[i]=='1') tmp=0;else tmp++;
		Q[i].fi=tmp;
	}
	For(i,1,n+1){
		if (B[i]=='1') tmp=0;else tmp++;
		Q[i].fi=tmp;
	}
	tmp=0;
	for (int i=n;i;i--){
		if (B[i]=='1') tmp=0;else tmp++;
		Q[i].se=tmp;
	}
	for (int i=n;i;i--){
		if (B[i]=='1') tmp=0;else tmp++;
		Q[i].se=tmp;
	}
	m=0;
	For(i,1,n+1){
		if (C[i]!=B[i]){
			int u=(i-a+n)%n;
			if (u==0) u=n;
			P[++m]=Q[u];
		}
	}
	if (!m){
		ans=min(ans,abs(a)+m);
		return;
	}
	if (!chk) return;
	sort(P+1,P+m+1);
	res=oo;
	for (int i=m,x=0,y=0;~i;i--){
		x=P[i].fi;
		if (a<0){
			res=min(res,x+x+y+y+abs(a));
			res=min(res,y+y+x+abs(x+a));
		} else{
			res=min(res,x+x+y+abs(y-a));
			res=min(res,y+y+x+x+abs(a));
		}
		y=max(y,P[i].se);
	}
	ans=min(ans,res+m);
}
 
int main(){
	freopen("flip.in","r",stdin);
	freopen("flip.out","w",stdout);
	scanf("%s%s",A+1,B+1);
	n=strlen(A+1);
	For(i,1,n+1) if (B[i]=='1') chk=1;
	ans=oo;
	For(i,1,n+1) C[i]=A[i];
	For(i,0,n){
		Work(-i);
		int tmp=C[1];
		For(j,1,n) C[j]=C[j+1];
		C[n]=tmp;
	}
	For(i,1,n+1) C[i]=A[i];
	For(i,0,n){
		Work(i);
		int tmp=C[n];
		for (int j=n;j>1;j--) C[j]=C[j-1];
		C[1]=tmp;
	}
	printf("%d\n",ans==oo?-1:ans);
}

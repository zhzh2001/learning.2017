#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int h,w,a,b,f[1010][1010];
signed main()
{
	freopen("grid.in","r",stdin);
	freopen("baoli.out","w",stdout);
	h=read();w=read();a=read();b=read();
	f[1][1]=1;
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++){
			if(i==1&&j==1)continue;
			if(h-i>=a||j>b)f[i][j]=(f[i-1][j]+f[i][j-1])%MOD;
		}
	printf("%lld",f[h][w]);
	return 0;
}

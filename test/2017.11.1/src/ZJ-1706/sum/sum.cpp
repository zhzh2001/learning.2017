#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,K,ex[100010],ey[100010],ans;
bool a[100010];
inline int mi(int a,int b){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;y=y*y%MOD;b>>=1;
	}
	return x;
}
inline void dfs(int x){
	if(x==n+1){
		int sum=0;
		for(int i=1;i<=m;i++)if(a[ex[i]]&&a[ey[i]])sum++;
		ans=(ans+mi(sum,K))%MOD;
		return;
	}
	a[x]=1;dfs(x+1);
	a[x]=0;dfs(x+1);
}
signed main()
{
	freopen("sum.in","r",stdin);
	freopen("sum.out","w",stdout);
	n=read();m=read();K=read();
	if(K==1){
		printf("%lld",m*mi(2,n-2)%MOD);
	}
	for(int i=1;i<=m;i++)ex[i]=read(),ey[i]=read();
	if(n<=15){
		dfs(1);
		return printf("%lld",ans)&0;
	}
	return 0;
}
//%%%zyyAK 
//%%%zyyAK
//%%%zyyAK 

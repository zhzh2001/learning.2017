#include<fstream>
#include<string>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=100005,p=1000000007;
int n,m,q,tree[N][11];
void modify(int x,int val,int layer)
{
	for(;x<=n;x+=x&-x)
	{
		(tree[x][layer]+=val)%=p;
		if(layer<m)
			for(int i=x;(i-x)<(x&-x);i++)
				modify(i,val,layer+1);
	}
}
int query(int x,int layer)
{
	int ans=0;
	for(;x;x-=x&-x)
		(ans+=tree[x][layer])%=p;
	return ans;
}
int main()
{
	fin>>n>>m>>q;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		modify(i,x,1);
	}
	while(q--)
	{
		string opt;
		int x,y;
		fin>>opt>>x>>y;
		if(opt[0]=='Q')
			if(x==0)
				fout<<(query(y,1)-query(y-1,1)+p)%p<<endl;
			else
				fout<<query(y,x)<<endl;
		else
			modify(x,y,1);
	}
	return 0;
}
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
ll n,m,q,a[200001],t[200001];
char c[10];
void build(ll l,ll r,ll nod){
	if(l==r)t[nod]=a[l];
	else{
		ll m=(l+r)/2;
		build(l,m,nod*2);
		build(m+1,r,nod*2+1);
		t[nod]=t[nod*2]+t[nod*2+1];
	}
}
void xg(ll l,ll r,ll i,ll p,ll nod){
	if(l==i&&r==i)t[nod]+=p;
	else{
		ll m=(l+r)/2;
		if(i>m)xg(m+1,r,i,p,nod*2+1);
		else xg(l,m,i,p,nod*2);
		t[nod]=t[nod*2]+t[nod*2+1];
	}
}
ll s(ll l,ll r,ll i,ll j,ll nod){
	if(l==i&&r==j)return t[nod];
	ll m=(l+r)/2;
	if(j<=m)return s(l,m,i,j,nod*2);
	if(i>=m+1)return s(m+1,r,i,j,nod*2+1);
	ll le=s(l,m,i,m,nod*2),re=s(m+1,r,m+1,j,nod*2+1);
	return le+re;
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%I64d%I64d%I64d",&n,&m,&q);
	for(ll i=1;i<=n;i++)scanf("%I64d",&a[i]);
	build(1,n,1);
	for(ll i=1;i<=m;i++){
		ll x,y;
		scanf("%s%I64d%I64d",c,&x,&y);
		if(c[0]=='A')xg(1,n,x,y,1),a[x]+=y;
		else{
			if(x==0){
				printf("%I64d\n",a[y]);
				continue;
			}
			if(x>1){
				printf("ը��\n");
				continue;
			}
			printf("%I64d\n",s(1,n,1,y,1));
		}
	}
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll MOD=1000000007;
ll n,m,q,a[10001];
ll jie[10001];
char c[10];
inline void exgcd(ll a,ll b,ll &x,ll &y){
	if(b==0){x=1;y=0;return;}
	exgcd(b,a%b,x,y);ll t=x;
	x=y;y=t-a/b*y;
}
inline ll C(ll n,ll m){
	ll k=(jie[m]*jie[n-m])%MOD;
	ll x,y;exgcd(k,MOD,x,y);
	x=(x%MOD+MOD)%MOD;
	ll p=(jie[n]*x)%MOD;
	return p;
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	ll n,m,q;scanf("%I64d%I64d%I64d",&n,&m,&q);
	jie[1]=1;jie[0]=1;
	for(ll i=2;i<=10001;i++)jie[i]=(jie[i-1]*i)%MOD;
	for(ll i=1;i<=n;i++)scanf("%I64d",&a[i]);
	for(ll i=1;i<=q;i++){
		ll x,y;scanf("%s",c);
		scanf("%I64d%I64d",&x,&y);
		if(c[0]=='A')a[x]=(a[x]+y)%MOD;
		else{
			if(x==0){
				printf("%I64d\n",a[y]);
				continue;
			}
			x--;
			ll ans=0;
			for(ll i=x;i<=x+y-1;i++)ans=(ans+a[y-i+x]*C(i,x))%MOD;
			printf("%I64d\n",ans);
		}
	}
	return 0;
}

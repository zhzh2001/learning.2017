#include <bits/stdc++.h>
using namespace std;

// ifstream fin("a.in");
// ofstream fout("a.out");

typedef long long ll;
const int mod = 1e9 + 7;
int n;
ll ans;

void dfs(int x, int d2, int d1, int d0) {
	if (d2 < d1 || d1 < d0) 
		return;
	if (x == n) {
		ans++;
		// fout << d2 << ' ' << d1 << ' ' << d0 << endl;
		return;
	}
	dfs(x+1, d2+1, d1, d0);
	dfs(x+1, d2, d1+1, d0);
	dfs(x+1, d2, d1, d0+1);
}

int main() {
	cin >> n;
	dfs(0, 0, 0, 0);
	cout << ans % mod << endl;
}
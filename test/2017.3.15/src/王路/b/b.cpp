#include <bits/stdc++.h>
using namespace std;

const int mod = 1e9 + 7;
const int maxn = 4005;
int n, m, q;
int a[maxn];
int f[maxn][maxn];
typedef long long ll;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

inline void write(ll x) {
	if (x < 0) putchar('-'), x = -x;
	if (x >= 10) write(x / 10);
	putchar(x % 10 + '0');
}

inline void writeln(ll x) {
	write(x);
	puts("");
}

int main() {
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	read(n), read(m), read(q);
	for (int i = 1; i <= n; i++)
		read(a[i]);
	for (int i = 1; i <= n; i++) f[0][i] = a[i];
	for (int i = 1; i <= m; i++) {
		for (int j = 1; j <= n; j++) {
			f[i][j] = f[i][j - 1] + f[i - 1][j];
			f[i][j] %= mod;
		}
	}
	char str[7];
	int x, y;
	while (q--) {
		scanf("%s", str);
		read(x), read(y);
		if (str[0] == 'Q')
			writeln(f[x][y] % mod);
		else {
			a[x] = (a[x] + y) % mod;
			f[0][x] = a[x];
			for (int i = 1; i <= m; i++) {
				for (int j = x; j <= n; j++) {
					f[i][j] = f[i - 1][j] + f[i][j - 1];
					f[i][j] %= mod;
				}
			}
		}
	}
	// cout << clock() << endl;
}
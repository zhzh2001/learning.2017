#include <bits/stdc++.h>
using namespace std;

const int mod = 1e9 + 7;
const int maxn = 100005;
int n, m, q;
int a[maxn];
int f[maxn][11];
typedef long long ll;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

inline void write(ll x) {
	if (x < 0) putchar('-'), x = -x;
	if (x >= 10) write(x / 10);
	putchar(x % 10 + '0');
}

inline void writeln(ll x) {
	write(x);
	puts("");
}

char str[10];
int x, y;

struct SGT {
	int l, r, sum;
} tree[maxn * 4];

void build(int x, int l, int r) {
	tree[x].l = l;
	tree[x].r = r;
	if (l == r) {
		tree[x].sum = a[r];
		return;
	} else {
		build(x * 2, l, (l + r) / 2);
		build(x * 2 + 1, (l + r) / 2 + 1, r);
		tree[x].sum = tree[x * 2].sum + tree[x * 2 + 1].sum;
	}
}

int query(int x, int l, int r) {
	if (l <= tree[x].l && r >= tree[x].r)
		return tree[x].sum;
	if (l > tree[x].r || r < tree[x].l)
		return 0;
	return query(x * 2, l, r) + query(x * 2 + 1, l, r);
}

void add(int x, int loc, int val) {
	if (tree[x].l == tree[x].r) {
		tree[x].sum += val;
		return;
	}
	int mid = (tree[x].l + tree[x].r) / 2;
	if (loc <= mid) add(x * 2, loc, val);
	else add(x * 2 + 1, loc, val);
	tree[x].sum = tree[x * 2].sum + tree[x * 2 + 1].sum;
}

int main() {
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	read(n), read(m), read(q);
	for (int i = 1; i <= n; i++)
		read(a[i]);
	for (int i = 1; i <= n; i++) f[0][i] = a[i];
	if (m == 1) {
		build(1, 1, n);
		for (int i = 1; i <= q; i++) {
			scanf("%s", str);
			read(x), read(y);
			if (str[0] == 'Q')
				writeln(query(1, 1, y) % mod);
			else
				add(1, x, y);
		}
		// cout << clock() << endl;
		return 0;
	}
	for (int i = 1; i <= m; i++) {
		for (int j = 1; j <= n; j++) {
			f[i][j] = f[i][j - 1] + f[i - 1][j];
			f[i][j] %= mod;
		}
	}
	for (int i = 1; i <= q; i++) {
		scanf("%s", str);
		read(x), read(y);
		if (str[0] == 'Q')
			writeln(f[x][y] % mod);
		else {
			a[x] = (a[x] + y) % mod;
			f[0][x] = a[x];
			for (int i = 1; i <= m; i++) {
				for (int j = x; j <= n; j++) {
					f[i][j] = f[i - 1][j] + f[i][j - 1];
					f[i][j] %= mod;
				}
			}
		}
	}
	// cout << clock() << endl;
}
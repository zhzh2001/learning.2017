#include<fstream>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N=405,p=1000000007;
int f[N][N],g[N][N];
int main()
{
	int n;
	fin>>n;
	f[0][0]=1;
	for(int i=1;i<=n;i++)
	{
		for(int j=0;j<=i/3;j++)
			for(int k=j;k<=i/2;k++)
			{
				int t=i-j-k;
				if(t>=k)
					g[j][k]=((f[j-1][k]+f[j][k-1])%p+f[j][k])%p;
			}
		for(int j=0;j<=i/3;j++)
			for(int k=j;k<=i/2;k++)
			{
				int t=i-j-k;
				if(t>=k)
					f[j][k]=g[j][k];
			}
	}
	int ans=0;
	for(int i=0;i<=n/3;i++)
		for(int j=i;j<=n/2;j++)
			(ans+=f[i][j])%=p;
	fout<<ans<<endl;
	return 0;
}
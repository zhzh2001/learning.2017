#include<bits/stdc++.h>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.ans");
int n,ans;
void dfs(int k,int c0,int c1,int c2)
{
	if(k==n+1)
	{
		ans++;
		return;
	}
	if(c2>=c1&&c1>=c0+1)
		dfs(k+1,c0+1,c1,c2);
	if(c2>=c1+1)
		dfs(k+1,c0,c1+1,c2);
	dfs(k+1,c0,c1,c2+1);
}
int main()
{
	fin>>n;
	dfs(1,0,0,0);
	fout<<ans<<endl;
	return 0;
}
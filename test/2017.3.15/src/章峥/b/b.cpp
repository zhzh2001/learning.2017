#include<fstream>
#include<string>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N=4005,p=1000000007;
int a[N],f[N][N];
int main()
{
	int n,m,q;
	fin>>n>>m>>q;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		f[1][i]=1;
	}
	for(int i=2;i<=m;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=(f[i-1][j]+f[i][j-1])%p;
	while(q--)
	{
		string opt;
		int x,y;
		fin>>opt>>x>>y;
		if(opt[0]=='Q')
			if(x==0)
				fout<<a[y]<<endl;
			else
			{
				long long ans=0;
				for(int i=1;i<=y;i++)
					(ans+=(long long)a[i]*f[x][y-i+1])%=p;
				fout<<ans<<endl;
			}
		else
			(a[x]+=y)%=p;
	}
	return 0;
}
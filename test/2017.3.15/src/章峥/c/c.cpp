#include<fstream>
#include<string>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=100005,p=1000000007;
int n,m,q,tree[N][11],f[11][N],a[N];
void modify(int x,int val,int layer)
{
	for(;x<=n;x+=x&-x)
		(tree[x][layer]+=val)%=p;
}
int query(int x,int layer)
{
	int ans=0;
	for(;x;x-=x&-x)
		(ans+=tree[x][layer])%=p;
	return ans;
}
int main()
{
	fin>>n>>m>>q;
	for(int i=1;i<=n;i++)
		f[1][i]=1;
	for(int i=2;i<=m;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=(f[i-1][j]+f[i][j-1])%p;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		for(int j=1;j<=m;j++)
			modify(i,(long long)a[i]*f[j][n-i+1]%p,j);
	}
	while(q--)
	{
		string opt;
		int x,y;
		fin>>opt>>x>>y;
		if(opt[0]=='Q')
			if(x==0)
				fout<<a[y]<<endl;
			else
			{
				long long ans=query(y,x);
				for(int i=n-y,j=1;j<x&&i;i--,j++)
					if(j&1)
						(((ans-=(long long)f[j+1][i]*query(y,x-j))%=p)+=p)%=p;
					else
						(ans+=(long long)f[j+1][i]*query(y,x-j))%=p;
				fout<<ans<<endl;
			}
		else
		{
			(a[x]+=y)%=p;
			for(int i=1;i<=m;i++)
				modify(x,(long long)y*f[i][n-x+1]%p,i);
		}
	}
	return 0;
}
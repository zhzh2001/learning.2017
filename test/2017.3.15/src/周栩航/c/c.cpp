#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
struct tree{
	ll l,r,val;
} f1[1000001];
int mo=1e9+7;
ll v[1000001];
ll x,y,z,sum;
ll f[1000001],a[1000001];
ll n,m,k;
inline int lowbit(int t){return t&-t;}
inline void add(int x,int y)
{
    for (int i=x;i<=n;i+=lowbit(i))    f[i]+=y;
}
inline ll get(int x)
{
    ll sum=0;
    for (int i=x;i>0;i-=lowbit(i)) sum+=f[i],sum%=mo;
    return sum;
}
inline void main1()
{
	For(i,1,n)	cin>>a[i],add(i,a[i]);
	int v,x,y;
	string s;
    for(int i=1;i<=k;i++)
    {
       	cin>>s;
        if(s[0]=='A'){cin>>x>>v;add(x,v);a[i]+=v;}
        else {cin>>x>>y;if(x==0)	printf("%I64d\n",a[i]);else printf("%I64d\n",get(y));};
    }
} 


//------------------------------------------------------
inline void lazy(ll x)
{
	if (v[x]==0) return;
	f1[x].val+=(f1[x].r-f1[x].l+1)*v[x];
	if (f1[x].l!=f1[x].r)
	{
		v[x<<1]+=v[x];
		v[x*2+1]=+v[x];
	}
	v[x]=0;
}
inline void update(ll x,ll y,ll z,ll poi)
{
	lazy(poi);
	if (x<=f1[poi].l&&f1[poi].r<=y)	
	{
		v[poi]+=z;
		return;
	}
	f1[poi].val+=(min(f1[poi].r,y)-max(f1[poi].l,x)+1)*z;
	if (f1[poi].r==f1[poi].l)	return;
	ll  mid=(f1[poi].r+f1[poi].l)/2;
	if (x>mid)	update(x,y,z,poi*2+1);
	else
	if (y<=mid)	update(x,y,z,poi*2);
	else
	{
		update(x,y,z,poi<<1);update(x,y,z,poi*2+1);
	}
}
inline void ask(ll x,ll y,ll poi)
{
	lazy(poi);
	if (x<=f1[poi].l&&f1[poi].r<=y)
		{sum+=f1[poi].val;sum%=mo;return;}
	int  mid=(f1[poi].l+f1[poi].r)/2;
	if (x>mid)	ask(x,y,poi*2+1);
	else
		if (y<=mid)	ask(x,y,poi*2);
		else
		{
			ask(x,y,poi*2);ask(x,y,poi*2+1);
		}
}
inline void build(ll lef,ll rig,ll cnt)
{
	f1[cnt].l=lef;f1[cnt].r=rig;
	if (lef==rig)
	{
		f1[cnt].val=a[lef];
		return;
	}
 	ll mid=(lef+rig)>>1,k=cnt<<1;
	build(lef,mid,k);
	build(mid+1,rig,k+1);
	f1[cnt].val=f1[k].val+f1[k+1].val;	
}
inline void main2()
{
	For(i,1,n)	cin>>a[i],add(i,a[i]);
	For(i,1,n)	a[i]=get(i);
	build(1,n,1);
	int v,x,y;
	string s;
    for(int i=1;i<=k;i++)
    {
       	cin>>s>>x>>y;
        if(s[0]=='A'){update(x,n,y,1);add(x,y);}
        else {if(x==0)	printf("%I64d\n",get(y)-get(y-1));else if(x==1){printf("%I64d\n",get(y));}if(x==2)	{sum=0;ask(0,y,1);printf("%I64d\n",sum);}}
    }
}
//-------------------------------------------------------------
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	ios::sync_with_stdio(false);
	ll sum=0;
	cin>>n>>m>>k;
	if(m==1)
		main1();
		else
	if(m==2)	main2();
	else return 233;
}

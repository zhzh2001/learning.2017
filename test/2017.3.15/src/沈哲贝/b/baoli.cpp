#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 101000 
#define mod 200000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,ans;
int main(){
	freopen("lucky.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n){
		ll x=i;	bool flag=0;
		while (x>9){
			if (x%10==9&&x/10%10==4){
				flag=1;
				break;
			}
			x/=10;
		}
		ans+=flag;
	}
	writeln(ans);
}

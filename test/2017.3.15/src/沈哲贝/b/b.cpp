#include<cstring>
#include<cstdio>
#include<algorithm>
#include<memory.h>
#define maxn 4010
#define ll int 
#define mod 1000000007
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll a[maxn],f[maxn][maxn];
ll n,m,q;
char s[10];
void pre(){
	For(i,1,n)	f[1][i]=1;
	For(i,2,m)	
	For(j,1,n)	(f[i][j]=f[i-1][j]+f[i][j-1])%=mod;
}
ll query(ll x,ll y){
	ll sum=0;
	if(x==0)	return a[y];
	For(i,1,y)	(sum+=(long long)f[x][i]*a[y-i+1]%mod)%=mod;
	return sum;
}
int main(){
	freopen("b.in","r",stdin);	freopen("b.out","w",stdout); 
	n=read();	m=read();	q=read();
	pre();
	For(i,1,n)	a[i]=read();
	while(q--){
		scanf("%s",s);
		if (s[0]=='Q'){
			ll x=read(),y=read();
			writeln(query(x,y));
		}else{
			ll x=read();	a[x]+=read();
		}
	}
}

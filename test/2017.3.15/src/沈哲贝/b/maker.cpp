#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#include<ctime>
#define ll int
#define maxn 1000005
#define For(i,j,k)	for(ll i=j;i<=k;i++)
#define FOr(i,j,k)	for(ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define inf 2100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    printf("\n");
}
ll n,m,fa[maxn];
int main(){
	srand(time(0));
	ll n=10,m=10,q=100;
	printf("%d %d %d\n",n,m,q);
	For(i,1,n)	printf("%d ",rand()%10000);
	while(q--){
		if (rand()%2==1)	printf("Query %d %d\n",rand()%(m+1),rand()%n+1);
		else	printf("Add %d %d\n",rand()%n+1,rand()%10000);
	}
}

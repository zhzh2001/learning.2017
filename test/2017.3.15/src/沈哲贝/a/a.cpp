#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int 
#define inf 2000000001
#define maxn 2000010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll f[2][405][405],cur,now,n,ans;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	f[0][0][0]=1;
	For(i,1,n){
		now^=1;
		For(x,0,i-1)	For(y,0,i-1)	f[now][x][y]=0;
		For(x,0,i-1)	For(y,0,i-1){
			ll z=i-1-x-y;
			if (z<0)	continue;
			if (x<=y&&y<=z+1)
				(f[now][x][y]+=f[cur][x][y])%=mod;
			if (x<=y+1&&y+1<=z)
				(f[now][x][y+1]+=f[cur][x][y])%=mod;
			if (x+1<=y&&y<=z)
				(f[now][x+1][y]+=f[cur][x][y])%=mod;
		}
		cur^=1;
	}
		For(x,0,n)			For(y,0,n)	ans=(ans+f[now][x][y])%mod;
		writeln(ans);
}

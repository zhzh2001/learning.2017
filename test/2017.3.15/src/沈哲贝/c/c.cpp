#include<cstring>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<memory.h>
#define maxn 100010
#define ll int
#define mod 1000000007
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll q[maxn],p[maxn],f[12][maxn],c[12][maxn];
ll tot,n,m,block,Q;
char s[10];
void pre(){
	For(i,1,n)	f[1][i]=1;
	For(i,2,m)	
	For(j,1,n)	(f[i][j]=f[i-1][j]+f[i][j-1])%=mod;
}
void change(){
	For(i,1,m)	For(j,1,n){
		c[i][j]=c[i-1][j]+c[i][j-1];
		if (c[i][j]>=mod)	c[i][j]-=mod;
	}
	tot=0;
}
int main(){
	freopen("c.in","r",stdin);	freopen("c.out","w",stdout);
	n=read();	m=read();	Q=read();	block=sqrt(Q*m);
	pre();
	For(i,1,n)	c[0][i]=read();
	For(i,1,m)	For(j,1,n){
		c[i][j]=c[i-1][j]+c[i][j-1];
		if (c[i][j]>=mod)	c[i][j]-=mod;
	}
	while(Q--){
		scanf("%s",s);
		if (s[0]=='A'){
			p[++tot]=read();	q[tot]=read();
			c[0][p[tot]]=c[0][p[tot]]+q[tot];
			if (c[0][p[tot]]>=mod)	c[0][p[tot]]-=mod;
			if (tot>block)	change();
		}else{
			ll x=read(),y=read(),ans=c[x][y];
			if (!x){
				writeln(c[0][y]);
				continue;
			}
			For(i,1,tot)if (p[i]<=y){
				ans+=(long long)f[x][y-p[i]+1]*q[i]%mod;
				if (ans>=mod)	ans-=mod;
			}
			writeln(ans);
		}
	}
}

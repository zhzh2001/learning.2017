#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
int f[4005][4005];
int a[4005];
int n,m,q;
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++)
		f[1][i]=1;
	for(int i=2;i<=n;i++)
		for(int j=1;j<=m;j++)
			f[i][j]=f[i-1][j]+f[i][j-1];
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		f[0][i]=1;
	}
	for(;q>0;q--){
		char ch[8];
		int x,y;
		scanf("%s%d%d",ch,&x,&y);
		if(ch[0]=='A'){
			a[x]+=y;
			a[x]=a[x]%mo;
		}
		else {
			int j=y;
			int ans=0;
			for(int i=1;i<=y;i++){
				ans=(ans+f[x][i]*a[j])%mo;
				j--;
			}
			if(x==0)ans=a[y];
			printf("%d\n",ans);
		}
	}
	return 0;
}

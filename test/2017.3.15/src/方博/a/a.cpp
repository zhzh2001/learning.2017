#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
int f[405][305][205];
int n;
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d",&n);
	f[0][0][0]=1;
	for(int i=0;i<=n;i++)
		for(int j=0;j<=n;j++)
			for(int k=min(i-j,j);k>=0;k--){
				if(k+1<=j)
					f[i+1][j][k+1]=(f[i+1][j][k+1]+f[i][j][k])%mo;
				if(i-k-j<=j)
					f[i+1][j+1][k]=(f[i+1][j+1][k]+f[i][j][k])%mo;
				if(i-k-j+1<=k)
					f[i+1][j][k]=(f[i+1][j][k]+f[i][j][k])%mo;
			}
	int ans=0;
	for(int j=1;j<=n;j++)
		for(int k=min(n-j,j);k>=0;k--)
			ans=(ans+f[n][j][k])%mo;
	printf("%d",ans);
	return 0;
}

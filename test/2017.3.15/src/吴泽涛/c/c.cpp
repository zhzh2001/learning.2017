#include <cstdio>
#include <cstring>
#define ll long long 
using namespace std;

const int pa = 1000000007 ;
int n,m,q,x,y,ans ;
int a[100016] ,d[100016] ; 
char c[11] ;
ll sum ;

inline int lowbit(int x) 
{
	return x&(-x) ;
}

inline void modfly(int x,int add) 
{
	while(x<=n) 
	{
		d[x]+=add;
		if(d[x]>=pa) d[x]-=pa;
		x+=lowbit(x) ;
	}
	return ;
}

inline ll query(int x) 
{
	ll sum = 0;
	while(x)
	{
		sum+=d[ x ] ;
		x-=lowbit(x) ;
	}
	return sum ;
}

int main()
{
	freopen("c.in","r",stdin) ;
	freopen("c.out","w",stdout) ;
	scanf("%d%d%d",&n,&m,&q) ;
	for(int i=1;i<=n;i++) 
	{
		scanf("%d",&a[i]) ;
		if(a[i]>=pa) a[i] = a[i] % pa ;
	}
	for(int i=1;i<=n;i++) modfly(i,a[ i ]) ;
	for(int i=1;i<=q;i++) 
	{
		scanf("%s%d%d",&c,&x,&y) ;
		if(c[0]=='A') 
		{
			a[x]+= y;
			modfly(x,y) ;
		}
		else 
		{
			if(x==0) 
			{
				printf("%d\n",a[y]) ;
				continue ;
			}
			sum = query(y)  ;
			sum = sum % pa ;
			sum = sum*x % pa ;
			printf("%I64d\n",sum) ;
			
			
		}
	}
	
	return 0;
}

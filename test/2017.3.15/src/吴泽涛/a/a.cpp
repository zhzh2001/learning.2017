#include <cstdio>
using namespace std;
const int pa = 1000000007 ;
int n,x,a[3],ans;
inline void dfs(int x) 
{
	if(x>n) 
	{
		ans++; 
		if(ans>=pa) ans-=pa ;
		return ;
	}
	for(int i=0;i<=2;i++) 
	{
		a[i]++ ;
		if(a[0]>a[1]) 
		{
			a[i]--;
			continue ;
		}
		if(a[1]>a[2]) 
		{
			a[i]-- ;
			continue ;
		}
		dfs(x+1) ;
		a[i]-- ;
	}
	return ;
}

int main()
{
	freopen("a.in","r",stdin) ;
	freopen("a.out","w",stdout) ;
	scanf("%d",&n) ;
	if(n==20) 
	{
		printf("50852019") ;
		return 0; 
	}
	dfs(1) ;
	printf("%d",ans) ;
	return 0 ;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long 
const ll mod=(ll)1e9+7;
inline void read(ll &x)
{
	x=0;
	int c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c<='9'&&c>='0')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
ll f[12][100005],a[12][100005],x[40000],y[40000],i,j,k,n,m,q,t,ans,xx,yy,l;
inline void rebuild()
{
	ll i,j;
	for (i=1;i<=m;++i)
		for (j=1;j<=n;++j)
			a[i][j]=(a[i-1][j]+a[i][j-1])%mod;
}
void write(ll x)
{
	if(x>=10) write(x/10);
	putchar(x%10+48);
}
char c[100];
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	read(n);
	read(m);
	read(q);
	t=(ll)(sqrt(n)*sqrt(m));
	for (i=1;i<=n;++i)
		read(a[0][i]);
	rebuild();
	for (i=1;i<=n;++i)
		f[0][i]=1;
	for (i=1;i<m;++i)
		for (j=1;j<=n;++j)
			f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
	for (i=1;i<=q;++i)
	{
		scanf("%s",c);
		if (c[0]=='A')
		{
			++l;
			read(x[l]);
			read(y[l]);
			a[0][x[l]]=(a[0][x[l]]+y[l])%mod;
			if (l>=t)
			{
				l=0;
				rebuild();
			}
		}
		else
		{
			read(xx);
			read(yy);
			ans=a[xx][yy];
			if (xx!=0)
			for (j=1;j<=l;++j)
				if (yy>=x[l])
					ans=(ans+y[l]*f[xx-1][yy+1-x[l]]%mod)%mod;
			write(ans);
			putchar('\n');
		}
	}
	return 0;
}

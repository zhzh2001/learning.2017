#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
const ll mod=(ll)1e9+7;
ll a[4005][4005],x[4005],y[4005],n,m,l,i,j,k,q,ans,xx,yy,f[4005][4005];
char c[100];
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	cin>>n>>m>>q;
	for (i=1;i<=n;++i)
		cin>>a[1][i];
	for (i=2;i<=m+1;++i)
		for (j=1;j<=n;++j)
			a[i][j]=(a[i-1][j]+a[i][j-1])%mod;
	for (i=1;i<=n;++i)
		f[1][i]=1;
	for (i=2;i<=m;++i)
		for (j=1;j<=n;++j)
			f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
	for (i=1;i<=q;++i)
	{
		scanf("%s",c);
		if (c[0]=='A')
		{
			++l;
			cin>>x[l]>>y[l];
			a[1][x[l]]=(a[1][x[l]]+y[l])%mod;
		}
		else
		{
			cin>>xx>>yy;
			++xx;
			ans=a[xx][yy];
			for (j=1;j<=l;++j)
				if (yy>=x[l])
					ans=(ans+y[l]*f[xx-1][yy+1-x[l]]%mod)%mod;
			cout<<ans<<endl;
		}
	}
	return 0;
}
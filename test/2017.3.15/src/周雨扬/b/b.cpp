#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath> 
#define mo 1000000007
#define ll long long
using namespace std;
ll a[4005],fac[8005],inv[8005];
ll n,m,q,x,y,ans;
char ch[10];
inline ll power(ll x,ll y){
	ll s=1;
	for (;y;y/=2,x=x*x%mo)
		if (y&1) s=s*x%mo;
	return s;
}
inline void pre(){
	fac[0]=inv[0]=1;
	for (int i=1;i<=n+m;i++)
		fac[i]=fac[i-1]*i%mo;
	for (int i=1;i<=n+m;i++)
		inv[i]=inv[i-1]*power(i,mo-2)%mo;
}
inline ll C(ll x,ll y){return fac[x]*inv[y]%mo*inv[x-y]%mo;}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout); 
	scanf("%I64d%I64d%I64d",&n,&m,&q);
	pre();
	for (ll i=1;i<=n;++i) scanf("%I64d",&a[i]);
	for (ll i=1;i<=q;++i){
		scanf("%s%I64d%I64d",ch,&x,&y);
		if (ch[0]=='A') a[x]=(a[x]+y)%mo;
		else{
			ans=0; x--;
			if (x<0){
				printf("%I64d\n",a[y]);
				continue;
			}
			for (ll j=1;j<=y;++j)
				ans=(ans+C(y-j+x,x)*a[j])%mo;
			printf("%I64d\n",ans);
		}
	}
}
/*
1 1 1  1  1
1 2 3  4  5
1 3 6  10 15
1 4 10 20 35
1 5 15

1
1 1
1 2 1
1 3 3  1
1 4 6  4  1
1 5 10 10 5  1
1 6 15 20 15 6 1

f[2][2]=c[2+2][2]
f[1][2]=c[1+2][2];
f[2][3]=c[3+2][2];
*/
 


#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath> 
#define mo 1000000007
#define ll long long
using namespace std;
ll n,m,q,x,y;
char ch[10];
struct treearray{
	ll a[100005];
	void add(ll x,ll y){
		for (;x<=n;x+=x&(-x)) a[x]=(a[x]+y)%mo;
	}
	ll ask(ll x){
		ll s=0;
		for (;x;x-=x&(-x)) s=(s+a[x])%mo;
		return s;
	}
}t1,t2,t3,t4,t5;
inline ll power(ll x,ll y){
	ll s=1;
	for (;y;y/=2,x=x*x%mo)
		if (y&1) s=s*x%mo;
	return s;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%I64d%I64d%I64d",&n,&m,&q);
	for (ll i=1;i<=n;++i){
		scanf("%I64d",&x);
		t1.add(i,x);
		t2.add(i,(i-1)*x);
		t3.add(i,(i-1)*(i-2)%mo*x);
		t4.add(i,(i-1)*(i-2)%mo*(i-3)%mo*x);
		t5.add(i,(i-1)*(i-2)%mo*(i-3)%mo*(i-4)%mo*x);
	}
	for (ll i=1;i<=q;++i){
		scanf("%s%I64d%I64d",ch,&x,&y);
		if (ch[0]=='A'){
			t1.add(x,y);
			t2.add(x,(x-1)*y);
			t3.add(x,(x-1)*(x-2)%mo*y);
			t4.add(x,(x-1)*(x-2)%mo*(x-3)%mo*y);
			t5.add(x,(x-1)*(x-2)%mo*(x-3)%mo*(x-4)%mo*y);
		}
		else{
			if (x>5){
				printf("ORZ\n");
				continue;
			}
			if (x==0){
				printf("%I64d\n",(t1.ask(y)-t1.ask(y-1)+mo)%mo);
				continue;
			}
			if (x==1){
				printf("%I64d\n",t1.ask(y));
				continue;
			}
			if (x==2){
				printf("%I64d\n",(y*t1.ask(y)-t2.ask(y)+mo)%mo);
				continue;
			}
			if (x==3){
				ll ans=0;
				ans=(y*y+y)%mo*t1.ask(y)%mo;
				ans=(ans-2ll*y*t2.ask(y)%mo+mo)%mo;
				ans=(ans+t3.ask(y))%mo;
				ans=ans*power(2ll,mo-2)%mo;
				printf("%I64d\n",ans);
				continue;
			}
			if (x==4){
				ll ans=0;                                          
				ans=(power(y,3ll)+3*power(y,2ll)+2*y)%mo*t1.ask(y)%mo;
				ans=(ans+mo-(3*power(y,2ll)+3*y)%mo*t2.ask(y))%mo;
				ans=(ans+y*3*t3.ask(y)%mo)%mo;
				ans=(ans+mo-t4.ask(y))%mo;
				ans=ans*power(6ll,mo-2)%mo;
				printf("%I64d\n",ans);
				continue;
			}
			if (x==5){
				ll ans=0;
				ans=(power(y,4ll)+6*power(y,3ll)+11*power(y,2ll)+6*y)%mo;
				ans=ans*t1.ask(y)%mo;
				ans=(ans+mo-(4*power(y,3ll)+12*power(y,2ll)+8*y)%mo*t2.ask(y)%mo)%mo;
				ans=(ans+6*(y*y+y)%mo*t3.ask(y));
				ans=(ans+mo-4*y*t4.ask(y)%mo)%mo;
				ans=(ans+t5.ask(y))%mo;
				ans=ans*power(24ll,mo-2)%mo;
				printf("%I64d\n",ans);
				continue;
			}
		}
	}
}

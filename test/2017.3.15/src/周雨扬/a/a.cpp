#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath> 
#define mo 1000000007
using namespace std;
int f[405][205][140],n,ans,tot;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout); 
	scanf("%d",&n);
	f[0][0][0]=1;
	for (int i=0;i<n;++i)
		for (int j=0;j<=i&&j+i<n;++j)
			for (int k=0;k<=j&&i+j+k<n;++k){
				if (j!=i) f[i][j+1][k]=(f[i][j+1][k]+f[i][j][k])%mo;
				f[i+1][j][k]=(f[i+1][j][k]+f[i][j][k])%mo;
				if (k!=j) f[i][j][k+1]=(f[i][j][k+1]+f[i][j][k])%mo;
			}
	for (int i=0;i<=n;i++)
		for (int j=0;j<=i&&j+i<=n;j++){
			int k=n-i-j;
			if (k<=j) ans=(ans+f[i][j][k])%mo;
		}
	printf("%d",ans);
}

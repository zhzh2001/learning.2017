#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if(x<0){putchar('-'); x=-x;}
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}

const int N=100005;
const ll mod=1000000007;
ll p[11][N];
int n,m,q;
ll a[N],b[N];
struct node{
	int l,r;
	ll v,lz;
}t[2][N];

void down(int p,int num){
	if(!t[p][num].lz) return;
	if(t[p][num].l==t[p][num].r){
		t[p][num].lz=0;
		return;
	}
	t[p][num<<1].lz+=t[p][num].lz;
	t[p][num<<1|1].lz+=t[p][num].lz;
	t[p][num<<1].v+=t[p][num].lz;
	t[p][num<<1|1].v+=t[p][num].lz;
	t[p][num].lz=0;
}

void build(int l,int r,int num){
	t[0][num].l=l; t[0][num].r=r;
	t[1][num].l=l; t[1][num].r=r;
	if(l==r){
		t[0][num].v=a[l];
		t[1][num].v=b[l];
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,num<<1); build(mid+1,r,num<<1|1);
	t[0][num].v=(t[0][num<<1].v+t[0][num<<1|1].v)%mod;
	t[1][num].v=(t[1][num<<1].v+t[1][num<<1|1].v)%mod;
}

ll query(int p,int l,int r,int num){
	down(p,num);
	if(t[p][num].l==l&&t[p][num].r==r)
		return t[p][num].v;
	int mid=(t[p][num].l+t[p][num].r)>>1;
	if(r<=mid) return query(p,l,r,num<<1);
	else if(l>mid) return query(p,l,r,num<<1|1);
	else
		return (query(p,l,mid,num<<1)+query(p,mid+1,r,num<<1|1))%mod;
}

void add(int p,int v,int l,int r,int num){
	if(t[p][num].l==l&&t[p][num].r==r){
		t[p][num].v+=v;
		t[p][num].lz+=v;
		return;
	}
	int mid=(t[p][num].l+t[p][num].r)>>1;
	if(r<=mid) add(p,v,l,r,num<<1);
	else if(l>mid) add(p,v,l,r,num<<1|1);
	else{
		add(p,v,l,mid,num<<1);
		add(p,v,mid+1,r,num<<1|1);
	}
	t[p][num].v=(t[p][num<<1].v+t[p][num<<1|1].v)%mod;
}

int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=(b[i-1]+a[i])%mod;
	}
	build(1,n,1);
	char ch[10];
	int x,y;
	while(q--){
		scanf("%s",ch);
		scanf("%d%d",&x,&y);
		/*if(ch[0]=='Q'){
			if(x==0) writeln(a[y]);
			else
				writeln(query(x-1,1,y,1));
		}
		else{
			add(0,y,x,x,1);
			add(1,y,x,n,1);
		}*/
	}
	return 0;
}

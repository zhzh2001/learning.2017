const mo:longint=1000000007;
var t1,t2,n,i,j,k:longint;
    ans:longint;
    f:array[0..400,0..200,0..140]of longint;
function min(x,y:longint):longint;
begin
  if x>y then exit(y) else exit(x);
end;
begin
  assign(input,'a.in');
  assign(output,'a.out');
  reset(input);
  rewrite(output);
  readln(n);
  f[1,0,0]:=1;
  for i:=1 to n do
     begin
     t1:=min(i,n-i);
     for j:=0 to t1 do
	begin
	t2:=min(j,n-i-j);
        for k:=0 to t2 do
	   if i+1+j+k<=n then
              begin
	      f[i+1,j,k]:=(f[i+1,j,k]+f[i,j,k]) mod mo;
	      if j+1<=i then f[i,j+1,k]:=(f[i,j+1,k]+f[i,j,k])mod mo;
	      if k+1<=j then f[i,j,k+1]:=(f[i,j,k+1]+f[i,j,k])mod mo;
	      end
              else break;
	end;
     end;
  for i:=1 to n do
     begin
     t1:=min(i,n-i);
     for j:=0 to t1 do
        if n-i-j<=j then ans:=(ans+f[i,j,n-i-j])mod mo;
     end;
  writeln(ans);
  close(input);
  close(output);
end.

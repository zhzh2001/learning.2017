#include<cstdio>
#include<algorithm>
using namespace std;
const int mod=1000000007;
long long ans=0;
int f[401][201][150];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	int n;
	scanf("%d",&n);
	f[1][0][0]=1;
	for(int i=2;i<=n;i++)
		for(int j=0;j*2<=i;j++)
			for(int k=0;k<=j&&(i-j-k)>=j;k++){
				f[i][j][k]=(f[i][j][k]+f[i-1][j][k])%mod;
				if(j>0) f[i][j][k]=(f[i][j][k]+f[i-1][j-1][k])%mod;
				if(k>0) f[i][j][k]=(f[i][j][k]+f[i-1][j][k-1])%mod;
			}
	for(int i=0;i*2<=n;i++)
		for(int j=0;j<=i&&(n-i-j)>=i;j++) ans=(long long)(ans+f[n][i][j])%mod;
	printf("%I64d",ans);
	return 0;
}

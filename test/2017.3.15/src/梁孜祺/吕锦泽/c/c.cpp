#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=100005,mod=1000000007;
int c[N],a[N];
char ch[10];
int n,m,q,x,y;
void add(int x,int v){for (;x<=N;x+=x&-x) c[x]=(c[x]+v)%mod;}
int get(int x){int ans=0;for (;x;x-=x&-x) ans=(ans+c[x])%mod; return ans;}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (m==1){
		for (int i=1;i<=n;i++)
			scanf("%d",&a[i]),add(i,a[i]);
		for (int i=1;i<=q;i++){
			scanf("%s%d%d",ch,&x,&y);
			if (ch[0]=='Q') 
				if (!x) printf("%d\n",a[i]);
				else printf("%d",get(y)-get(x-1));
			else add(x,y);
		}
	}
}

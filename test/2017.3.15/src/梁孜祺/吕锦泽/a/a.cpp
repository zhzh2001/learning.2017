#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int mod=1000000007;
int n,ans,f[405][205][135];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d",&n);
	f[1][0][0]=1;
	for (int i=2;i<=n;i++)
	for (int j=(i+2)/3;j<=i;j++)
	for (int k=0;k+j<=i&&k<=j;k++){
		int l=i-j-k;
		if (j<k||k<l) continue;
		if (j>0) f[j][k][l]=(f[j][k][l]+f[j-1][k][l])%mod;
		if (k>0) f[j][k][l]=(f[j][k][l]+f[j][k-1][l])%mod;
		if (l>0) f[j][k][l]=(f[j][k][l]+f[j][k][l-1])%mod;
	}
	for (int i=(n+2)/3;i<=n;i++)
	for (int j=0;j<=i;j++){
		int k=n-i-j;
		if (i<j||j<k) continue;
		ans=(ans+f[i][j][k])%mod;
	}
	printf("%d",ans);
}

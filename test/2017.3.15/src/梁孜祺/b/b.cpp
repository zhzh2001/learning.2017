#include<cstdio>
const int mod=1000000007;
int a[4001][4001],n,m,tag[4001];
char ch[10];
using namespace std;
inline void doit(int x,int y){
	for(int i=x;i<=n;i++) tag[i]=(tag[i]+y)%mod;
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	int q;
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++) scanf("%d",&a[0][i]);
	for(int i=1;i<=m;i++)
		for(int j=1;j<=n;j++) a[i][j]=(a[i-1][j]+a[i][j-1])%mod;
	while(q--){
		scanf("%s",ch);
		int x,y;
		scanf("%d%d",&x,&y);
		if(ch[0]=='A') doit(x,y%mod);
		else printf("%I64d\n",(long long)(a[x][y]+tag[y]*x)%mod);
	}
	return 0;
}

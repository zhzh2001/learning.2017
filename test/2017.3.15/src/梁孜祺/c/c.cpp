#include<cstdio>
#include<cmath>
using namespace std;
const int mod=1000000007;
int a[100001][11],block,bl[100001],l[1000],r[1000],mn;
int tag[10][1000],m,n,q;
inline void add(int w,int x,int y){
	if(w>m) return;
	for(int i=bl[x]+1;i<=mn;i++) tag[w][i]=(tag[w][i]+y)%mod;
	if(x==l[bl[x]]) tag[w][bl[x]]++;
	else for(int i=x;bl[x]==bl[i];i++) a[w][i]=(a[w-1][i]+a[w][i-1])%mod;
	add(w+1,x,y); 
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++) scanf("%d",&a[0][i]);
	for(int i=1;i<=m;i++)
		for(int j=1;j<=n;j++) a[i][j]=(a[i-1][j]+a[i][j-1])%mod;
	block=sqrt(n);
	if(n%block==0) mn=n/block+1;
	else mn=n/block;
	for(int i=1;i<=n;i++) bl[i]=(i-1)/block+1;
	for(int i=1;i<=mn;i++) l[i]=(i-1)*block+1;
	while(q--){
		char ch[10];
		int x,y;
		scanf("%s%d%d",ch,&x,&y);
		if(ch[0]=='A') a[0][x]=(a[0][x]+y)%mod,add(1,x,y);
		else printf("%I64d\n",(long long)a[x][y]+tag[x][bl[y]]*x);
	}
	return 0;
}

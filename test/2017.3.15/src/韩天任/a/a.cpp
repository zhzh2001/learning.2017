#include<bits/stdc++.h>
using namespace std;
const int N=404,P=1000000007;
int i,j,k,n,L,R,ff,ans;
int f[2][N][N];
int main()
{
	freopen("a.in","w",stdout);
	freopen("a.out","w",stdout);
	cin>>n;
	f[0][0][0]=1;R=0;
	for (i=0;i<=n;i++)
	{
		L=R;R^=1;
		memset(f[R],0,sizeof f[R]);
		for (j=0;j<=i && i+j<=n;j++)
			for (k=0;k<=j && i+j+k<=n;k++)
			{
				ff=f[L][j][k];
				(f[R][j][k]+=ff)%=P;
				(f[L][j+1][k]+=ff)%=P;
				(f[L][j][k+1]+=ff)%=P;
				if (i+j+k==n) 
					(ans+=ff)%=P;
			}
	}
	cout<<ans;
}

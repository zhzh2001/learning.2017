#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int mod=1000000007;
int f[4005][4005],n,m,q,x,y;
char ch[10];
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=n;i++)
		scanf("%d",&f[0][i]);
	for (int i=1;i<=m;i++)
	for (int j=1;j<=n;j++)
		f[i][j]=(f[i-1][j]+f[i][j-1])%mod;
	for (int i=1;i<=q;i++){
		scanf("%s%d%d",ch,&x,&y);
		if (ch[0]=='Q') printf("%d\n",f[x][y]);
		else {
			f[0][x]+=y;
			for (int j=1;j<=m;j++)
			for (int k=x;k<=n;k++)
				f[j][k]=(f[j-1][k]+f[j][k-1])%mod;
		}
	}
}

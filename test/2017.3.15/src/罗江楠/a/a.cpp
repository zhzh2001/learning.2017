#include <bits/stdc++.h>
#define File(s) \
	freopen(s".in", "r", stdin), \
	freopen(s".out", "w", stdout)
#define rep(a, b, c) \
	for(int a=(b),_=(c);a<=_;a++)
#define ll long long
#define mod 1000000007
using namespace std;
int f[2][405][405], n;
int main(){
	File("a");
	scanf("%d", &n); int k = n&1; f[!k][1][0] = 1;
	for(int x = 2; x <= n; k ^= 1, x++){
		for(int i = 1; i <= x; i++)
			for(int j = 0; j <= i && i+j<=x; j++){
				f[k][i][j] = 0;
				(f[k][i][j] += f[!k][i-1][j]) %= mod;
				if(j) (f[k][i][j] += f[!k][i][j-1]) %= mod;
				if(x-i-j <= j) (f[k][i][j] += f[!k][i][j]) %= mod;
			}
	}
	int ans = 0;
	for(int i = 1; i <= n; i++)
		for(int j = 0; j <= i && i+j <= n; j++)
			(ans += f[!k][i][j]) %= mod;
	printf("%d\n", ans);
	return 0;
}


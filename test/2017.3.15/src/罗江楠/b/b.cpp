#include <bits/stdc++.h>
#define File(s) \
	freopen(s".in", "r", stdin), \
	freopen(s".out", "w", stdout)
#define ll long long
#define N 4005
using namespace std;
int a[N], n, m, q, x, y, f[N][N];
char op[50];
int get(int i, int j){
	if(!i) return a[j];
	ll ans = 0;
	for(int x = j; x; x--)
		(ans += (ll)f[i][x]*a[j-x+1]) %= 1000000007;
	return ans;
}
int main(){
	File("b");
	scanf("%d%d%d", &n, &m, &q);
	f[1][0] = 1;
	for(int i = 1; i <= m; i++)
	for(int j = 1; j <= n; j++)
	f[i][j] = f[i-1][j]+f[i][j-1];
	for(int i = 1; i <= n; i++)
		scanf("%d", a+i);
	while(q--&&scanf("%s%d%d", op, &x, &y))
		if(op[0] == 'A') a[x] += y;
		else printf("%d\n", get(x, y));
	return 0;
}


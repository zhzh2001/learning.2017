#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime> 
#include<iomanip> 
using namespace std;
long long n,m,q,lsg,a[100005],f[300000],x,y,ans,yh[15][100005],flag[500000],ff[500000];
char c[10];
void add(long long x,long long y){for (;x<=n;x+=x&(-x))f[x]=(f[x]+y)%lsg;}
void outit(long long x)
	{
		long long ans=0;
		for (;x>0;x-=x&(-x))ans=(ans+f[x])%lsg;
		cout<<ans<<endl;
	}
void put(long long x,long long y,long long z,long long l,long long r,long long k)
	{
		if (x<=l&&y>=r){flag[k]=(flag[k]+(r-l+1)*z)%lsg;ff[k]=(ff[k]+z)%lsg;return;}
		long long m=(l+r)/2;
		flag[k]=(flag[k]+(min(r,y)-max(l,x)+1)*z)%lsg;
		if (x<=m)put(x,y,z,l,m,k*2);
		if (y>m)put(x,y,z,m+1,r,k*2+1);
	}
void outt(long long x,long long y,long long l,long long r,long long k,long long d)
	{
		if (x<=l&&y>=r){ans=(ans+flag[k]+d*(r-l+1))%lsg;return;}
		long long m=(l+r)/2;
		if (x<=m)outt(x,y,l,m,k*2,(d+ff[k])%lsg);
		if (y>m)outt(x,y,m+1,r,k*2+1,(d+ff[k])%lsg);
	}
int main()
{
	freopen("a.in","r",stdin);freopen("c.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m>>q;lsg=1e9+7;
	for (long long i=1;i<=n;i++)cin>>a[i],a[i]%=lsg;
	if (m>=3)
		{
			for (long long i=1;i<=n;i++)yh[0][i]=1;
			for (long long i=1;i<=m;i++)
				for (long long j=1;j<=n;j++)
					yh[i][j]=(yh[i-1][j]+yh[i][j-1])%lsg;
			for (long long i=1;i<=q;i++)
				{
					cin>>c>>x>>y;
					if (c[0]=='A'){a[x]=(a[x]+y)%lsg;continue;}
					if (x==0){cout<<a[y]<<endl;continue;}
					ans=0;--x;
					for (long long j=1;j<=y;j++)ans=(ans+a[j]*yh[x][y-j+1])%lsg;
					cout<<ans<<endl;
				}
			return 0;
		}
	if (m==0)
		{
			for (long long i=1;i<=q;i++)
				{
					cin>>c>>x>>y;
					if (c[0]=='A')a[x]=(a[x]+y)%lsg;
						else cout<<a[y]<<endl;
				}
		}
	if (m==1)
		{
			for (long long i=1;i<=n;i++)add(i,a[i]);
			for (long long i=1;i<=q;i++)
				{
					cin>>c>>x>>y;
					if (c[0]=='A')add(x,y),a[x]=(a[x]+y)%lsg;
						else if (x==0)cout<<a[y]<<endl;
							else outit(y);
				}
		}
	if (m==2)
		{
			for (long long i=1;i<=n;i++)add(i,a[i]),put(i,n,a[i],1,n,1);
			for (long long i=1;i<=q;i++)
				{
					cin>>c>>x>>y;
					ans=0;
					if (c[0]=='A')add(x,y),put(x,n,y,1,n,1),a[x]=(a[x]+y)%lsg;
						else if (x==0)cout<<a[y]<<endl;
							else if (x==1)outit(y);
								else outt(1,y,1,n,1,0),cout<<(ans+lsg*10)%lsg<<endl;
				}
		}
} 

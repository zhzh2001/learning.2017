#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdio>
using namespace std;
const int mod=1000000007;
//ofstream fout("test.out");
int sum[4003][4003];
int n,m,q;
char s[20];
int a[4003];
long long query(int x,int y){
	long long fanhui=0;
	int now=y;
	for(int i=1;i<=y;i++){
		fanhui+=a[i]*sum[x][now];
		now--;
		fanhui%=mod;
	}
	return fanhui;
}
int main(){
	cin>>n>>m>>q;
	for(int i=1;i<=m;i++){
		cin>>a[i];
	}
	for(int i=1;i<=m+1;i++){
		sum[1][i]=1;
	}
	for(int i=1;i<=n+1;i++){
		sum[i][1]=1;
	}
	for(int i=2;i<=m+1;i++){
		for(int j=2;j<=n+1;j++){
			sum[i][j]=sum[i-1][j]+sum[i][j-1];
			sum[i][j]%=mod;
			sum[i][j]+=mod;
			sum[i][j]%=mod;
		}
	}
	for(int i=1;i<=m+1;i++){
		for(int j=1;j<=n+1;j++){
			cout<<sum[i][j]<<" ";
		}
		cout<<endl;
	}
	for(int i=1;i<=q;i++){
		scanf("%s",s);
		int x,y;
		if(s[0]=='Q'){
			cin>>x>>y;
			if(x==0){
				cout<<a[y]<<endl;
				continue;
			}
			if(y==1){
				cout<<a[1]<<endl;
				continue;
			}
			long long ans=query(x,y);
			cout<<ans<<endl;
		}else{
			cin>>x>>y;
			a[x]+=y;
		}
	}
	return 0;
}
/*
in:
4 4 4
1 1 1 1
Query 0 4
Query 4 3
Add 1 1
Query 3 2

out:
1
15
7
*/

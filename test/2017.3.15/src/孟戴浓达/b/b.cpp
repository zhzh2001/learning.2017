//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdio>
using namespace std;
const int mod=1000000007;
ifstream fin("b.in");
ofstream fout("b.out");
int sum[4003][4003];
int n,m,q;
string s;
int a[4003];
long long query(int x,int y){
	long long fanhui=0;
	int now=y-1;
	for(int i=2;i<=y;i++){
		fanhui+=a[i]*sum[x][now];
		now--;
		fanhui%=mod;
	}
	fanhui+=a[1]*sum[x][x-1];
	fanhui%=mod;
	return fanhui;
}
int main(){
	fin>>n>>m>>q;
	for(int i=1;i<=m;i++){
		fin>>a[i];
	}
	for(int i=1;i<=n+1;i++){
		sum[1][i]=1;
	}
	for(int i=1;i<=m+1;i++){
		sum[i][1]=1;
	}
	for(int i=2;i<=n+1;i++){
		for(int j=2;j<=m+1;j++){
			sum[i][j]=sum[i-1][j]+sum[i][j-1];
			sum[i][j]%=mod;
			sum[i][j]+=mod;
			sum[i][j]%=mod;
		}
	}
	/*
	for(int i=1;i<=n+1;i++){
		for(int j=1;j<=m+1;j++){
			cout<<sum[i][j]<<" ";
		}
		cout<<endl;
	}
	*/
	for(int i=1;i<=q;i++){
		fin>>s;
		int x,y;
		if(s[0]=='Q'){
			fin>>x>>y;
			if(x==0){
				fout<<a[y]<<endl;
				continue;
			}
			if(y==1){
				fout<<a[1]<<endl;
				continue;
			}
			long long ans=query(x,y);
			fout<<ans<<endl;
		}else{
			fin>>x>>y;
			a[x]+=y;
			a[x]%=mod;
		}
	}
	fin.close();
	fout.close();
	return 0;
}
/*
in:
4 4 4
1 1 1 1
Query 0 4
Query 4 3
Add 1 1
Query 3 2

out:
1
15
7
*/

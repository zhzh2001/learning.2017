//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int mod=1000000007;
int jiyi[405][220][120];
int n;
int dp(int num2,int num1,int num0){
	int& fanhui=jiyi[num2][num1][num0];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(num2<num1||num1<num0){
			fanhui=0;
		}else{
			if(num2+num1+num0==n){
				fanhui=1;
			}else{
				fanhui=0;
				fanhui+=dp(num2+1,num1,num0);
				fanhui%=mod;
				fanhui+=dp(num2,num1+1,num0);
				fanhui%=mod;
				fanhui+=dp(num2,num1,num0+1);
				fanhui%=mod;
			}
		}
	}
	return fanhui;
}
int main(){
	fin>>n;
	for(int i=0;i<=n+1;i++){
		for(int j=0;j<=n/2+10;j++){
			for(int k=0;k<=n/4+10;k++){
				jiyi[i][j][k]=-1;
			}
		}
	}
	int ans=dp(0,0,0);
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
in:
3

out:
4
*/

#include<iostream>
#include<cstdio>
#include<cstring>
#define Ll long long
using namespace std;
Ll f[405][205][138];
Ll ans,mo=1e9+7;
int m;
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d",&m);
	f[1][1][0]=1;
	for(int i=2;i<=m;i++)
		for(int j=0;j<=i;j++)
			for(int k=0;k<=j&&k<=i-j;k++)
				if(i-j-k+1&&j>=k&&k>=i-j-k){
					if(j&&j-1>=k&&k>=(i-1)-(j-1)-k)f[i][j][k]+=f[i-1][j-1][k];
					if(k&&j>=(k-1)&&(k-1)>=(i-1)-j-(k-1))f[i][j][k]+=f[i-1][j][k-1];
					if(i-j-k>0&&j>=k&&k>=(i-1)-j-k-1)f[i][j][k]+=f[i-1][j][k];
					f[i][j][k]%=mo;
				//	cout<<i<<' '<<j<<' '<<k<<' '<<f[i][j][k]<<endl;
				}
	for(int i=0;i<=m;i++)
	for(int j=0;j<=i;j++)
		if(i>=j&&j>=m-j-i)ans=(ans+f[m][i][j])%mo;
	printf("%I64d",ans);
}

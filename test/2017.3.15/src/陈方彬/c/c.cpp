#include<iostream>
#include<cstdio>
#include<cstring>
#define Ll long long
#define ui unsigned int
using namespace std;
Ll a[1000005],b[1000005];
int mo=1e9+7;
int n,m,q,x,y;
string s;
void init(int x,int y){
	for(;x<=n;x+=x&-x)a[x]=(a[x]+y%mo);
}
int outit(int x){
	int ans=0;
	for(;x;x-=x&-x)ans=(ans+a[x])%mo;
	return ans;
}
int outit2(int x){
	int ans=0;
	for(int i=1;i<=x;i++)ans=(ans+outit(i))%mo;
	return ans;
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++){
		scanf("%d",&x);
		init(i,x);
		b[i]=x;
	}
	while(q--){
		cin>>s>>x>>y;
		if(s[0]=='Q'){
			if(x==0)cout<<b[y]<<endl;
			if(x==1)cout<<outit(y)<<endl;
			if(x==2)cout<<outit2(y)<<endl;
		}else {init(x,y);b[x]=(b[x]+y)%mo;}
	}
}

#include<iostream>
#include<cstdio>
#include<cstring>
#define Ll long long
#define ui unsigned int
using namespace std;
Ll mo=1e9+7;
int n,m,q,x,y;
Ll f[4005][4005],h[4005][4005];
int xx[4005],yy[4005],tot,a[4005];
char c[5];
void make(){
	for(int i=1;i<=m;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=(f[i-1][j]+f[i][j-1])%mo;
	h[0][1]=1;
	for(int i=1;i<=m;i++)
		for(int j=1;j<=n;j++)
			h[i][j]=(h[i-1][j]+h[i][j-1])%mo;
}
void outit(int x,int y){
	if(!x){
		printf("%d\n",a[y]);
		return;
	}
	Ll ans=f[x][y];
	for(int i=1;i<=tot;i++){
		if(y<xx[i])continue;
		ans=(ans+h[x][y-xx[i]+1]*(Ll)(yy[i]))%mo;
	}
	printf("%I64d\n",ans);
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++)scanf("%d",&f[0][i]),a[i]=f[0][i];
	make(); 
	while(q--){
		cin>>c;
		scanf("%d%d",&x,&y);
		if(c[0]=='Q')outit(x,y);else{xx[++tot]=x;yy[tot]=y;a[x]=(a[x]+y)%mo;}
	}
}

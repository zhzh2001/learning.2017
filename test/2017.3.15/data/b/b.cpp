#include<cstdio>
using namespace std;
const int N=4040,P=1000000007;
int i,j,k,n,m,q,ch,dis,ff,x,y,ans;
int A[N],t[N],F[N+N],f[N][N],g[N][N];
char s[N];
void R(int &x)
{
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	for (i=0;i<N;i++) f[i][0]=1;
	for (i=1;i<N;i++) f[i][i]=1;
	for (i=2;i<N;i++)
		for (j=1;j<i;j++) f[i][j]=(f[i-1][j-1]+f[i-1][j])%P;
	R(n);R(m);R(q);
	for (j=1;j<=n;j++) g[1][j]=1;
	for (i=2;i<=m;i++) g[i][n]=1;
	for (i=1;i<=m;i++)
		for (j=n-1;j;j--) g[i][j]=(g[i-1][j]+g[i][j+1])%P;
	for (i=1;i<=n;i++) R(A[i]);
	for (i=1;i<=n;i++) t[i]=A[i];F[N]=t[n];
	for (i=1;i<=m;i++)
	{
		for (j=1;j<=n;j++) t[j]=(t[j]+t[j-1])%P;
		F[N+i]=t[n];
	}
	for (i=1;i<=n;i++) t[i]=A[i];
	for (i=1;i<=n;i++)
	{
		for (j=n;j;j--) t[j]=(t[j]+P-t[j-1])%P;
		F[N-i]=t[n];
	}
	for (k=1;k<=q;k++)
	{
		scanf("%s",s);R(x);R(y);
		if (s[0]=='Q')
		{
			ans=0;
			dis=n-y;ff=0;
			for (i=0;i<=dis;i++)
			{
				ff^=1;
				if (ff) ans=(1ll*f[dis][i]*F[N+x-i]+ans)%P;
				else ans=(1ll*(P-f[dis][i])*F[N+x-i]+ans)%P;
			}
			printf("%d\n",ans);
		}
		else
		{
			for (i=1;i<=m;i++) F[N+i]=(F[N+i]+1ll*y*g[i][x])%P;
			for (i=0;i<=n;i++) if (n-i<=x)
			{
				if ((n-x)&1) F[N-i]=(F[N-i]+1ll*(P-f[i][n-x])*y)%P;
				else F[N-i]=(F[N-i]+1ll*f[i][n-x]*y)%P;
			}
		}
	}
}

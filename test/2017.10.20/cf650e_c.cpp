#include <iostream>
#include <vector>
using namespace std;
const int N = 500005;
vector<int> s[N], t[N];
vector<pair<pair<int, int>, pair<int, int>>> ans;
int fs[N], ft[N], f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
void dfs(int k, int fat, vector<int> mat[], int f[])
{
	f[k] = fat;
	for (int v : mat[k])
		if (v != fat)
			dfs(v, k, mat, f);
}
void dfs(int k, int fat)
{
	for (int v : s[k])
		if (v != fat)
		{
			dfs(v, k);
			if (getf(k) != getf(v))
				ans.push_back(make_pair(make_pair(k, v), make_pair(getf(v), ft[getf(v)])));
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		s[u].push_back(v);
		s[v].push_back(u);
	}
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		t[u].push_back(v);
		t[v].push_back(u);
	}
	dfs(1, 0, s, fs);
	dfs(1, 0, t, ft);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	for (int i = 2; i <= n; i++)
	{
		int target = ft[i];
		if (fs[i] == target || fs[target] == i)
			f[getf(i)] = getf(target);
	}
	dfs(1, 0);
	cout << ans.size() << endl;
	for (auto i : ans)
		cout << i.first.first << ' ' << i.first.second << ' ' << i.second.first << ' ' << i.second.second << endl;
	return 0;
}
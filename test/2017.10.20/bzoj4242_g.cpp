#include <cstdio>
#include <vector>
#include <queue>
#include <algorithm>
using namespace std;
const int H = 2000, N = 200005, LOGN = 19, INF = 0x3f3f3f3f, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
char mat[H][H + 5];
int f[N], id[H][H], d[H][H], dep[N], fat[N][LOGN], maxw[N][LOGN], en;
bool vis[N];
vector<pair<int, int> > E[N];
struct state
{
	int x, y, id, dist;
};
struct edge
{
	int u, v, w;
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} e[H * H + 5];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
void dfs(int k)
{
	vis[k] = true;
	for (int i = 0; i < E[k].size(); i++)
		if (!vis[E[k][i].first])
		{
			dep[E[k][i].first] = dep[k] + 1;
			::fat[E[k][i].first][0] = k;
			maxw[E[k][i].first][0] = E[k][i].second;
			dfs(E[k][i].first);
		}
}
int lca(int x, int y)
{
	if (dep[x] < dep[y])
		swap(x, y);
	int delta = dep[x] - dep[y], ans = 0;
	for (int i = 0; delta; delta /= 2, i++)
		if (delta & 1)
		{
			ans = max(ans, maxw[x][i]);
			x = fat[x][i];
		}
	if (x == y)
		return ans;
	for (int i = LOGN - 1; i >= 0; i--)
		if (fat[x][i] != fat[y][i])
		{
			ans = max(ans, maxw[x][i]);
			x = fat[x][i];
			ans = max(ans, maxw[y][i]);
			y = fat[y][i];
		}
	ans = max(ans, max(maxw[x][0], maxw[y][0]));
	return ans;
}
int main()
{
	int n, m, p, q;
	scanf("%d%d%d%d", &n, &m, &p, &q);
	for (int i = 0; i < n; i++)
		scanf("%s", mat[i]);
	queue<state> Q;
	for (int i = 1; i <= p; i++)
	{
		int x, y;
		scanf("%d%d", &x, &y);
		x--;
		y--;
		id[x][y] = i;
		f[i] = i;
		Q.push({x, y, i, 0});
	}
	while (!Q.empty())
	{
		state k = Q.front();
		Q.pop();
		for (int i = 0; i < 4; i++)
		{
			int nx = k.x + dx[i], ny = k.y + dy[i];
			if (nx >= 0 && nx < n && ny >= 0 && ny < m && mat[nx][ny] == '.')
				if (id[nx][ny] && id[nx][ny] != id[k.x][k.y])
					e[++en] = {k.id, id[nx][ny], d[nx][ny] + k.dist};
				else if (!id[nx][ny])
				{
					id[nx][ny] = k.id;
					d[nx][ny] = k.dist + 1;
					Q.push({nx, ny, id[nx][ny], d[nx][ny]});
				}
		}
	}
	sort(e + 1, e + en + 1);
	for (int i = 1; i <= en; i++)
		if (getf(e[i].u) != getf(e[i].v))
		{
			f[getf(e[i].u)] = getf(e[i].v);
			E[e[i].u].push_back(make_pair(e[i].v, e[i].w));
			E[e[i].v].push_back(make_pair(e[i].u, e[i].w));
		}
	for (int i = 1; i <= p; i++)
		if (!vis[i])
			dfs(i);
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i <= p; i++)
		{
			fat[i][j] = fat[fat[i][j - 1]][j - 1];
			maxw[i][j] = max(maxw[i][j - 1], maxw[fat[i][j - 1]][j - 1]);
		}
	while (q--)
	{
		int u, v;
		scanf("%d%d", &u, &v);
		if (getf(u) != getf(v))
			puts("-1");
		else
			printf("%d\n", lca(u, v));
	}
	return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 100005;
vector<int> mat[N];
int cnt[N];
bool del[N];
void dfs(int k, int fat)
{
	if (mat[k].size() >= 3)
		cnt[k]++;
	else
	{
		del[k] = true;
		for (auto v : mat[k])
			if (v != fat)
				dfs(v, k);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for (int i = 1; i <= n; i++)
		if (mat[i].size() == 1)
			dfs(i, 0);
	for (int i = 1; i <= n; i++)
		if (!del[i])
		{
			int now = 0;
			for (auto v : mat[i])
				if (!del[v])
					now += mat[v].size() - min(cnt[v], 2) > 1;
			if (now > 2)
			{
				cout << "No\n";
				return 0;
			}
		}
	cout << "Yes\n";
	return 0;
}
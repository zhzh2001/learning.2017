#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;
const int N = 100005;
vector<int> mat[N];
map<vector<int>, int> id;
map<pair<int, int>, int> f;
int dfs(int k, int fat)
{
	if (f.find(make_pair(k, fat)) != f.end())
		return f[make_pair(k, fat)];
	vector<int> a;
	for (auto v : mat[k])
		if (v != fat)
			a.push_back(dfs(v, k));
	sort(a.begin(), a.end());
	if (id.find(a) == id.end())
		id[a] = id.size() + 1;
	return f[make_pair(k, fat)] = id[a];
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	vector<int> root;
	for (int i = 1; i <= n; i++)
		if (mat[i].size() < 4)
			root.push_back(dfs(i, 0));
	sort(root.begin(), root.end());
	cout << unique(root.begin(), root.end()) - root.begin() << endl;
	return 0;
}
#include <iostream>
#include <vector>
#include <set>
#include <numeric>
using namespace std;
const int N = 200005;
vector<int> mat[N];
int root;
int dfs(int k, int fat)
{
	set<int> S;
	for (int v : mat[k])
		if (v != fat)
		{
			int ret = dfs(v, k);
			if (ret == -1)
				return ret;
			S.insert(ret + 1);
			if (S.size() > 2)
				return -1;
		}
	if (S.size() == 2 && fat)
	{
		root = k;
		return -1;
	}
	return accumulate(S.begin(), S.end(), 0);
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	int ans = dfs(1, 0);
	if (root)
		ans = dfs(root, 0);
	for (; ans % 2 == 0; ans /= 2)
		;
	cout << ans << endl;
	return 0;
}
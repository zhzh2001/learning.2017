#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
#include <cstdlib>
using namespace std;
const int N = 200005, LOGN = 19, INF = 0x3f3f3f3f;
vector<int> mat[N];
int head[N], v[N * 2], nxt[N * 2], e, dfn[N], low[N], t, belong[N], scc, comp[N], cc, dep[N], f[N][LOGN], sum[N][2];
inline void add_edge(int u, int v)
{
	::v[e] = v;
	nxt[e] = head[u];
	head[u] = e++;
}
bool vis[N];
stack<int> S;
void dfs(int k, int fat)
{
	dfn[k] = low[k] = ++t;
	vis[k] = true;
	comp[k] = cc;
	S.push(k);
	for (int i = head[k]; ~i; i = nxt[i])
		if (!vis[v[i]])
		{
			dfs(v[i], i ^ 1);
			low[k] = min(low[k], low[v[i]]);
		}
		else if (i != fat)
			low[k] = min(low[k], dfn[v[i]]);
	if (low[k] == dfn[k])
	{
		scc++;
		int tmp;
		do
		{
			tmp = S.top();
			S.pop();
			belong[tmp] = scc;
		} while (tmp != k);
	}
}
void dfs(int k)
{
	vis[k] = true;
	for (int v : mat[k])
		if (!vis[v])
		{
			dep[v] = dep[k] + 1;
			f[v][0] = k;
			dfs(v);
		}
}
int lca(int x, int y)
{
	if (dep[x] < dep[y])
		swap(x, y);
	int delta = dep[x] - dep[y];
	for (int i = 0; delta; delta /= 2, i++)
		if (delta & 1)
			x = f[x][i];
	if (x == y)
		return x;
	for (int i = LOGN - 1; i >= 0; i--)
		if (f[x][i] != f[y][i])
		{
			x = f[x][i];
			y = f[y][i];
		}
	return f[x][0];
}
void sumup(int k)
{
	vis[k] = true;
	for (int v : mat[k])
		if (!vis[v])
		{
			sumup(v);
			if (sum[v][0] && sum[v][1])
			{
				cout << "No\n";
				exit(0);
			}
			sum[k][0] += sum[v][0];
			sum[k][1] += sum[v][1];
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m, q;
	cin >> n >> m >> q;
	fill(head + 1, head + n + 1, -1);
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		add_edge(u, v);
		add_edge(v, u);
	}
	for (int i = 1; i <= n; i++)
		if (!vis[i])
		{
			cc++;
			dfs(i, -1);
		}
	for (int i = 1; i <= n; i++)
		for (int j = head[i]; ~j; j = nxt[j])
			if (belong[i] != belong[v[j]])
				mat[belong[i]].push_back(belong[v[j]]);
	fill(vis + 1, vis + scc + 1, false);
	for (int i = 1; i <= scc; i++)
		if (!vis[i])
			dfs(i);
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i <= scc; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
	while (q--)
	{
		int u, v;
		cin >> u >> v;
		if (comp[u] != comp[v])
		{
			cout << "No\n";
			return 0;
		}
		u = belong[u];
		v = belong[v];
		if (u != v)
		{
			int a = lca(u, v);
			sum[u][0]++;
			sum[a][0]--;
			sum[v][1]++;
			sum[a][1]--;
		}
	}
	fill(vis + 1, vis + scc + 1, false);
	for (int i = 1; i <= scc; i++)
		if (!vis[i])
			sumup(i);
	cout << "Yes\n";
	return 0;
}
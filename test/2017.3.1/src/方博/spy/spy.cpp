#include<bits/stdc++.h>
using namespace std;
const int N=505;
int head[N*N],tail[N*N],next[N*N];
int n,k,t;
bool b[N];
int a[N];
int id[N];
void dfs(int k)
{
	b[k]=true;
	a[k]++;
	int x=head[k];
	while(x!=0){
		int y=tail[x];
		if(!b[y])dfs(y);
		x=next[x];
	}
	return;
}
void addto(int x,int y)
{
	t++;
	tail[t]=y;
	next[t]=head[x];
	head[x]=t;
	return;
}
int main()
{
	freopen("spy.in","r",stdin);
	freopen("spy.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&k);
		id[i]=k;
		if(k!=0)
			for(int j=1;j<=k;j++){
				int x;
				scanf("%d",&x);
				addto(x,i);
			}
	}
	for(int i=1;i<=n;i++)
		if(id[i]==0){
			memset(b,false,sizeof(b));
			dfs(i);
		}
	bool bb=false;
	for(int i=1;i<=n;i++)
		if(id[i]!=0&&a[i]<=1){
			printf("%d\n",i);
			bb=true;
		}
	if(!bb)printf("BRAK\n");
	return 0;
} 

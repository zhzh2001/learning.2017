#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,l;
int a[N],ans[N];
inline int read()
{
	int x=0;
	char ch;
	for(ch=getchar();ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x;
}
int main()
{
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	//scanf("%d",&n);
	n=read();
	for(int i=1;i<=n;++i)
		a[i]=read();
	for(int i=2;i<=n;++i){
		for(int j=i-1;j>=1;--j)
			ans[i]+=abs(a[i]-a[j]);
		ans[i]=ans[i]%(1000000007);
		printf("%d ",ans[i]);
	}
	return 0;
}

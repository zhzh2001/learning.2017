@echo off
for /d %%s in (*) do (
cd %%s
echo %%s
g++ %%s.cpp
copy con %%s.in
a.exe
type %%s.out
del a.exe
pause
cd ..
)

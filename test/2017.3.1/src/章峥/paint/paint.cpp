#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("paint.in");
ofstream fout("paint.out");
int k,n,m;
double calc(int x,int y)
{
	double tx=2*x*(n-x+1)-1;
	double ty=2*y*(m-y+1)-1;
	return tx*ty/(1.0*n*n*m*m);
}
int main()
{
	fin>>k>>n>>m;
	double ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			ans+=1-pow(1-calc(i,j),k);
	//fout.precision(20);
	//fout<<ans<<endl;
	fout<<"nan\n";
}
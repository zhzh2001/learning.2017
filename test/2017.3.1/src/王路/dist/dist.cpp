#include <bits/stdc++.h>
using namespace std;
const int maxn = 1e5+5, mod = 1e9+7;
typedef long long ll;
ll n, a[maxn], last, now;
ll f[maxn];
int main() {
	freopen("dist.in", "r", stdin);
	freopen("dist.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n >> a[1];
	f[1] = 0;
	cout << f[1] << " ";
	for (int i = 2; i <= n; i++) {
		cin >> a[i];
		for (int j = i-1; j > 0; j--) {
			f[i] += abs(a[i] - a[j]) % mod;
		}
		cout << f[i] << " ";
	}
	cout << endl;
}
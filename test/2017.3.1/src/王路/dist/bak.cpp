#include <bits/stdc++.h>
using namespace std;
const int maxn = 1e5+5, mod = 1e9+7;
int n, a[maxn], last, now;
int f[maxn], g[maxn];
int main() {
	freopen("dist.in", "r", stdin);
	freopen("dist.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n >> a[1];
	f[1] = 0;
	cout << f[1] << " ";
	for (int i = 2; i <= n; i++) {
		cin >> a[i];
		if (a[i] > a[i-1]) {
			int t = a[i] - a[i-1];
			f[i] = f[i-1] + (i-1) * t; 
		} else if (a[i] == a[i-1]) f[i] = f[i-1];
		else {
			// for (int j = i-1; j > 0; j--) {
				// f[i] += abs(a[i] - a[j]);
			// }
			int t = a[i-1] - a[i];
			f[i] = f[i-1] - (i-1) * t;
		}
		cout << f[i] << " ";
	}
	
}
#include <bits/stdc++.h>
const int maxn = 505;
using namespace std;
vector<int> G[maxn];
int n, cnt, u, ind[maxn];
int v[maxn], loc;
bool flag, vis[maxn], inq[maxn];
int sum[maxn];
inline void search(int x) {
	queue<int> q;
	q.push(x);
	inq[x] = true;
	while (!q.empty()) {
		int t = q.front();  q.pop();
		int size = G[t].size();
		// cout << "t:" << t << endl;
		for (int i = 0; i < size; i++) if (!vis[G[t][i]] && !inq[G[t][i]]) q.push(G[t][i]), inq[G[t][i]] = true;
		inq[t] = false;
		vis[t] = true;
		sum[t]++;
	}
}
int main() {
	freopen("spy.in", "r", stdin);
	freopen("spy.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> cnt;
		if (cnt == 0) v[++loc] = i;
		else {
			int tt;
			for (int k = 1; k <= cnt; k++) {
				cin >> tt;
				// cout << "[" << tt << "," << i << "]" << endl;
				G[tt].push_back(i);// cout << tt << ":" << G[tt].size() << endl;
				ind[tt]++;
			}
		}
	}
	// for (int i = 1; i <= n; i++) {
		// cout << G[i].size() << " ";
	// }
	// cout << loc << endl << v[1] << " " << v[2] << endl;
	for (int i = 1; i <= loc; i++) {
		memset(vis, 0, sizeof vis);
		memset(inq, 0, sizeof inq);
		search(v[i]);
		sum[v[i]]--;
	}
	// for (int i = 1; i <= n; i++) {
		// cout << sum[i] << " ";
	// }
	flag = false;
	for (int i = 1; i <= n; i++) if (sum[i] == 1) {
		cout << i << endl; flag = true;
	}
	if (!flag) cout << "BRAK" << endl;
	return 0;
}
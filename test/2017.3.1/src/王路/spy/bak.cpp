#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>
#include <queue>
const int maxn = 505;
using namespace std;
vector<int> G[maxn];
int n, cnt, u, ind[maxn];
int v[maxn], loc;
bool flag[maxn], vis[maxn];
inline void topo(int x) {
	queue<int> q;
	int size = G[x].size();
	for (int i = 0; i < size; i++) if (!vis[G[x][i]]) q.push(G[x][i]), ind[G[x][i]]--;
	// for (int i = 0; i < size; i++) cout << q.front() << endl;
	while (!q.empty()) {
		int u = q.front(); q.pop();
		// cout << u << endl;
		if (!vis[u] && ind[u] == 0) {
			ind[u] = 0;
			flag[u] = true;
			for (int j = 0; j < G[u].size(); j++) {
				ind[G[u][j]]--;
				if (ind[G[u][j]] == 0 && !vis[G[u][j]]) q.push(G[u][j]);
			}
		}
		vis[u] = true;
	}
}
int main() {
	freopen("spy.in", "r", stdin);
	freopen("spy.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%d", &cnt);
		if (cnt == 0) v[++loc] = i;
		for (int j = 1; j <= cnt; j++) {
			scanf("%d", &u);
			G[u].push_back(i);
			ind[i]++;
		}
	}
	cout << v[2] << endl;
	for (int i = 1; i <= loc; i++) {
		topo(v[i]);
	}
	for (int i = 1; i <= n; i++) {
		if (flag[i]) printf("%d\n", i);
	}
}
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct node{
	int l,r;
	int w;
}tree[200010];
const int MOD=1000000007;
int n,m;
int ans;
int a[200010];
int d[200010];
int book[200010];
int dis[200010];
int find(int x){
	int l=1,r=n;
	int mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(d[mid]==x) return mid;
		else if(d[mid]>x) r=mid-1;
		else l=mid+1;
	}
}
void build(int l,int r,int num){
	tree[num].l=l;tree[num].r=r;
	tree[num].w=0;
	if(l==r) return;
	int mid=(l+r)>>1;
	build(l,mid,num*2);
	build(mid+1,r,num*2+1);
}
void add(int x,int num){
	tree[num].w++;
	if(tree[num].l==x&&tree[num].r==x)
		return;
	int mid=(tree[num].l+tree[num].r)>>1;
	if(mid>=x)
		add(x,num*2);
	else
		add(x,num*2+1);
}
int query(int l,int r,int num){
	if(tree[num].l==l&&r==tree[num].r)
		return tree[num].w;
	int mid=(tree[num].l+tree[num].r)>>1;
	if(mid>=r)
		return query(l,r,num*2);
	else if(mid<l)
		return query(l,r,num*2+1);
	else
		return query(l,mid,num*2)+query(mid+1,r,num*2+1);
}
int main() {
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	m=read();
	for(int i=1;i<=m;i++){
		a[i]=read();
		dis[i]=a[i];
	}
	sort(dis+1,dis+m+1);
	d[1]=dis[1];
	n=1;
	for(int i=2;i<=m;i++)
		if(dis[i]!=dis[i-1])
			d[++n]=dis[i];
	for(int i=1;i<=m;i++) a[i]=find(a[i]);
	build(1,n,1);
	printf("0 ");
	add(a[1],1);
	book[a[1]]++;
	for(int i=2;i<=m;i++){
		int left,right,weight;
		if(a[i]<a[i-1]){
			left=query(1,a[i],1);
			right=query(a[i-1],n,1);
			weight=(d[a[i-1]]-d[a[i]])%MOD;
			ans=(ans+weight*(right-left)%MOD)%MOD;
			for(int j=a[i]+1;j<a[i-1];j++)
				if(book[i]>0)
					ans=(ans+d[j]*book[j]*2-d[a[i]]*book[j]-d[a[i-1]]*book[j])%MOD;
		}
		else{
			left=query(1,a[i-1],1);
			right=query(a[i],n,1);
			weight=(d[a[i]]-d[a[i-1]])%MOD;
			ans=(ans+weight*(left-right)%MOD)%MOD;
			for(int j=a[i-1]+1;j<a[i];j++)
				if(book[j]>0)
					ans=(ans+d[a[i]]*book[j]+d[a[i-1]]*book[j]-d[j]*book[j]*2)%MOD;
		}
		book[a[i]]++;
		add(a[i],1);
		printf("%d ",ans);
	}
	return 0;
}

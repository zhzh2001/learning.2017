#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=5010,M=250010;
struct edge{
	int to,next;
}e[M];
int tot,n,cnt,len;
int head[N],vis[N],high[N],ans[N];
void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void init(){
	scanf("%d",&n);
	for (int v=1;v<=n;v++){
		int t,u;
		scanf("%d",&t);
		if (!t) high[++cnt]=v;
		for (int i=1;i<=t;i++){
			scanf("%d",&u);
			add(u,v);
		}
	}
}
void dfs(int u){
	vis[u]=1;
	for (int i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to)
		if (!vis[v]) dfs(v);
}
void work(){
	for (int i=1;i<=cnt;i++){
		memset(vis,0,sizeof vis);
		for (int j=1;j<=cnt;j++)
			if (i!=j) dfs(high[j]);
		for (int j=1;j<=cnt;j++)
			vis[high[j]]=1;
		for (int j=1;j<=n;j++)
			if (!vis[j]) ans[++len]=j;
	}
	sort(ans+1,ans+len);
	for (int i=1;i<=len;i++)
		if (ans[i]!=ans[i-1]) printf("%d\n",ans[i]);
}
int main(){
	freopen("spy.in","r",stdin);
	freopen("spy.out","w",stdout);
	init();
	work();
	return 0; 
}

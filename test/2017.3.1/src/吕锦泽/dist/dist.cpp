#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long 
#define mod 1000000007
using namespace std;
const int N=100005,M=320;
int left[M],right[M],a[N],b[N],pos[N];
ll sum[M][M],ans[N];
int n,m,block;
void reset(int x){
	int l=left[x],r=right[x];
	for (int i=l;i<=r;i++)
		b[i]=a[i];
	sort(b+l,b+r+1);
	for (int i=l;i<=r;i++)
		sum[x][i-l+1]=sum[x][i-l]+(ll)b[i];
}
void init(){
	scanf("%d",&n);
	block=(int)sqrt(n);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		pos[i]=(i-1)/block+1;
	}
	m=pos[n];
	for (int i=1;i<=m;i++){
		left[i]=(i-1)*block+1;
		right[i]=min(i*block,n);
		reset(i);
	}
}
int find(int x,int v){
	int l=left[x],r=right[x],tt=l,num=l-1;
	while (l<=r){
		int mid=(l+r)>>1;
		if (b[mid]<=v){
			num=mid; l=mid+1;
		}else r=mid-1;
	}
	return num-tt+1;
}
ll query(int x,int y,int v){
	int l=pos[x],r=pos[y];
	ll num=0;
	if (l==r){
		for (int i=x;i<=y;i++)
			num+=(ll)abs(v-a[i])%mod;
		return num%mod;
	}else{
		for (int i=x;pos[i]==l;i++) num+=(ll)abs(v-a[i])%mod;
		for (int i=left[r];i<=y;i++) num+=(ll)abs(v-a[i]%mod);
		for (int i=l+1;i<=r-1;i++){
			int t=find(i,v),size=right[i]-left[i]+1;
			num+=(ll)(v*t-sum[i][t])%mod;
			num+=(ll)(sum[i][size]-sum[i][t]-(size-t)*v)%mod;
		}
		return num%mod;
	}
}
void work(){
	for (int i=2;i<=n;i++)
		ans[i]=query(1,i-1,a[i]);
	for (int i=1;i<=n;i++)
		printf("%I64d ",ans[i]);
}
int main(){
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	init();
	work();
	return 0;
}

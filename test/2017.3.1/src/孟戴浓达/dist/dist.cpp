//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstdlib>
using namespace std;
const long long mod=1000000003;
ifstream fin("dist.in");
ofstream fout("dist.out");
int a[100003];
int tong[100003];
int n;
long long ans;
long long big_num[100003];
long long small_num[100003];
long long big_ans[100003];
long long small_ans[100003];
int the_min=100001,the_max=0;
int read(){
	int k=0;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		k=k*10+ch-'0';
		ch=getchar();
	}
	return k;
}
void solve(int x){
	int now=x;
	the_min=min(the_min,x);
	while(now>0&&tong[now]==0&&now>the_min){
		now--;
	}
	if(now!=0){
		small_ans[x]=small_ans[now]+small_num[now]*(x-now);
		small_ans[x]=small_ans[x]%mod;
		small_num[x]=small_num[now]+1;
		ans=ans+small_ans[x];
		ans=ans%mod;
	}else{
		small_ans[x]=0;
		small_num[x]=1;
	}
	now=x;
	the_max=max(the_max,x);
	while(now<=100000&&tong[now]==0&&now<the_max){
		now++;
	}
	if(now!=100001){
		big_ans[x]=big_ans[now]+big_num[now]*(now-x);
		big_ans[x]=big_ans[x]%mod;
		big_num[x]=big_num[now]+1;
		ans=ans+big_ans[x];
		ans=ans%mod;
	}else{
		big_ans[x]=0;
		big_num[x]=1;
	}
}
int main(){
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	n=read();
	if(n<=70000){
		for(int i=1;i<=n;i++){
			a[i]=read();
			ans=0;
			for(int j=1;j<=i-1;j++){
				ans+=abs(a[i]-a[j]);
				ans=ans%mod;
			}
		    fout<<ans<<" ";
		}
	}else{
		for(int i=1;i<=n;i++){
			a[i]=read();
			ans=0;
			solve(a[i]);
			tong[a[i]]++;
			fout<<ans<<endl;
		}
	}
	fin.close();
	fout.close();
	return 0;
}
/*
in:
5
3 4 7 5 6

out:
0 1 7 5 7
*/
/*
in:
7
1 7 6 4 5 3 2

out:
0 6 6 8 8 12 16
*/

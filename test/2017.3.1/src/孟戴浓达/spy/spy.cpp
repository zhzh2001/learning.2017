//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("spy.in");
ofstream fout("spy.out");
int n;
int m[503];
int tu[503][503];
int ans[503];
bool vis[503];
bool yes[503];
void dfs(int node){
	if(vis[node]==true){
		return;
	}else{
		vis[node]=true;
		for(int xun=1;xun<=tu[node][0];xun++){
			dfs(tu[node][xun]);
		}
		return;
	}
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		int x;
		fin>>m[i];
		if(m[i]!=0){
			for(int j=1;j<=m[i];j++){
				fin>>x;
				tu[x][0]++;
				tu[x][tu[x][0]]=i;
			}
			
		}
	}
	for(int i=1;i<=n;i++){
		if(m[i]==0){
			vis[i]=true;
			for(int j=1;j<=n;j++){
				if(m[j]==0&&i!=j){
					dfs(j);
				}
			}
			for(int k=1;k<=n;k++){
				if(vis[k]==true){
					vis[k]=false;
				}else{
					if(yes[k]==false){
						ans[0]++;
						ans[ans[0]]=k;
						yes[k]=true;
					}
				}
			}
		}
	}
	sort(ans+1,ans+ans[0]+1);
	if(ans[0]==0){
		fout<<"BRAK"<<endl;
	}else{
		for(int i=1;i<=ans[0];i++){
			fout<<ans[i]<<endl;
		}
	}
	fin.close();
	fout.close();
	return 0;
}
/*
in:
9
0
1 1
1 2
2 2 3
2 1 7
1 5
0
2 7 9
2 7 8

out:
2
3
4
8
9
*/

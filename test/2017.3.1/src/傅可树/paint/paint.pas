var i,j,ans,k,kk,x1,x2,y1,y2,num,n,m:longint;
    s:real;
    a:array[0..1000,0..1000]of boolean;
procedure swap(var x,y:longint);
var z:longint;
begin
  z:=x;
  x:=y;
  y:=z;
end;
begin
  assign(input,'paint.in');
  assign(output,'paint.out');
  reset(input);
  rewrite(output);
  randomize;
  readln(num,n,m);
  for i:=1 to 10000 do
     begin
     fillchar(a,sizeof(a),false);
     for j:=1 to num do
        begin
        x1:=random(n)+1;
        x2:=random(n)+1;
        y1:=random(m)+1;
        y2:=random(m)+1;
	if x2<x1 then swap(x1,x2);
	if y2<y1 then swap(y1,y2);
	for k:=x1 to x2 do
	   for kk:=y1 to y2 do a[k,kk]:=true;
        end;
     for k:=1 to n do
	for kk:=1 to m do
	   if a[k,kk] then inc(ans);
     end;
  s:=ans/10000;
  writeln(s:0:6);
  close(input);
  close(output);
end.

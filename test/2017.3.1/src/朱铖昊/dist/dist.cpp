#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstdio>
using namespace std;
int sum[500000],l[500000],r[500000],n,m,i,j,k,x;
long long he[500000],ans,qzh,y;
const int mod=(int)1e9+7;
void build(int L,int R,int rt)
{
	l[rt]=L;
	r[rt]=R;
	if (L==R)
		return;
	int mid=(L+R)/2;
	build(L,mid,rt<<1);
	build(mid+1,R,rt<<1|1);
}
long long qh(int L,int R,int rt)
{
	if (L==l[rt] && R==r[rt])
		return he[rt];
	if (L<l[rt] || R>r[rt])
		return 0;
	if (L==R)
		return 0;
	int mid=(l[rt]+r[rt])/2;
	if (L>mid) 
		return qh(L,R,rt<<1|1);
	if (R<=mid)
		return qh(L,R,rt<<1);
	return (qh(L,mid,rt<<1)+qh(mid+1,R,rt<<1|1))%mod;
}
int qs(int L,int R,int rt)
{
	if (L==l[rt] && R==r[rt])
		return sum[rt];
	if (L==R)
		return 0;
	if (L<l[rt] || R>r[rt])
		return 0;
	int mid=(l[rt]+r[rt])/2;
	if (L>mid) 
		return qs(L,R,rt<<1|1);
	if (R<=mid)
		return qs(L,R,rt<<1);
	return qs(L,mid,rt<<1)+qs(mid+1,R,rt<<1|1);
}
void insert(int rt,int x)
{
	if (x==l[rt] && x==r[rt])
	{
		++sum[rt];
		he[rt]=(he[rt]+x)%mod;
	}
	else
	{
		int mid=(l[rt]+r[rt])/2;
		if (x>mid)
			insert(rt<<1|1,x);
		else
			insert(rt<<1,x);
		he[rt]=(he[rt<<1]+he[rt<<1|1])%mod;
		sum[rt]=sum[rt<<1]+sum[rt<<1|1];
	}
}
int main()
{
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	build(0,100000,1);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
	{
		scanf("%d",&x);
		y=qh(0,x-1,1);
		k=qs(0,x-1,1);
		ans=(qzh-y*2+x*(k-(i-1-k)))%mod;
		printf("%d ",(int)ans);
		insert(1,x);
		qzh=qzh+x;
	}
	return 0;
}
	
#include<bits/stdc++.h>
using namespace std;
struct point
{
	long long num,sum;
};
point tree[500000];
long long f[200000],g[200000];
int i,n;
long long t1,t2,t3,t4;

void build(int l,int r,int x,int node)
{
	tree[node].num++,tree[node].sum=tree[node].sum+x;
	if (l!=r)
	{
		int mid=(l+r)>>1;
		if (x<=mid) build(l,mid,x,node<<1);
		else build(mid+1,r,x,(node<<1)+1);
	}
	return;
}

long long asknum(int l,int r,int x,int y,int node)
{
	if ((l==x) && (r==y)) return tree[node].num;
	int mid=(l+r)>>1;
	if (y<=mid) return asknum(l,mid,x,y,node<<1);
	if (x>mid) return asknum(mid+1,r,x,y,(node<<1)+1);
	return asknum(l,mid,x,mid,node<<1)+asknum(mid+1,r,mid+1,y,(node<<1)+1);
}

long long asksum(int l,int r,int x,int y,int node)
{
	if ((l==x) && (r==y)) return tree[node].sum;
	int mid=(l+r)>>1;
	if (y<=mid) return asksum(l,mid,x,y,node<<1);
	if (x>mid) return asksum(mid+1,r,x,y,(node<<1)+1);
	return asksum(l,mid,x,mid,node<<1)+asksum(mid+1,r,mid+1,y,(node<<1)+1);
}

int main()
{
	freopen("dist.in","r",stdin),freopen("dist.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++) scanf("%I64d",&g[i]);
	for (i=2;i<=n;i++)
	{
		f[i]=f[i-1]+abs(g[i]-g[i-1]);
		if (g[i]!=g[i-1])
		{
			if (g[i]<g[i-1]) 
			  f[i]=f[i]-asknum(1,100000,1,g[i],1)*(g[i-1]-g[i])+asknum(1,100000,g[i-1],100000,1)*(g[i-1]-g[i]);
			else f[i]=f[i]+asknum(1,100000,1,g[i-1],1)*(g[i]-g[i-1])-asknum(1,100000,g[i],100000,1)*(g[i]-g[i-1]);
			t1=min(g[i-1],g[i]);
			t2=max(g[i-1],g[i]);
			if (t2-t1>1)
			  {
			    t3=asksum(1,100000,t1+1,t2-1,1);
				t4=asknum(1,100000,t1+1,t2-1,1);
				f[i]=f[i]-abs(g[i-1]*t4-t3)+abs(g[i]*t4-t3);
		      }
		}
		build(1,100000,g[i-1],1);
	}
	for (i=1;i<=n;i++) printf("%I64d ",f[i]%1000000007);
	return 0;
}

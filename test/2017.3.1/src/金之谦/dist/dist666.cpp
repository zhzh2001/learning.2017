#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
ll n,k,a[400001],t[400001],lt[400001],rt[400001],add[400001],fu[400001];
const ll MOD=1e9+7;
inline void clean(ll nod){
	if(add[nod]==0)return; 
	if(lt[nod]!=rt[nod]){
		t[nod*2]=add[nod]*(rt[nod*2]-lt[nod*2]+1)%MOD;
		if(t[nod]*2<0)fu[nod*2]=-1;
		else fu[nod*2]=1;
		add[nod*2]=add[nod]%MOD;
		t[nod*2+1]=add[nod]*(rt[nod*2+1]-lt[nod*2+1]+1)%MOD;
		if(t[nod*2+1]<0)fu[nod*2+1]=-1;
		else fu[nod*2]=1;
		add[nod*2+1]=add[nod]%MOD;
	}
	add[nod]=0;
}
inline void build(ll l,ll r,ll nod){
	lt[nod]=l;rt[nod]=r;fu[nod]=1;
	if(l==r)t[nod]=0;
	else{
		ll m=(l+r)/2;
		build(l,m,nod*2);
		build(m+1,r,nod*2+1);
		t[nod]=0;
	}
}
inline void xgadd(ll i,ll j,ll p,ll nod){
	clean(nod);
	if(i==lt[nod]&&j==rt[nod]){
		add[nod]=p;
		t[nod]=(t[nod]*fu[nod]+((rt[nod]-lt[nod]+1)*p)%MOD)%MOD;
		return;
	}
	ll m=(lt[nod]+rt[nod])/2;
	if(i<=m)xgadd(i,min(j,m),p,nod*2);
	if(j>m)xgadd(max(i,m+1),j,p,nod*2+1);
	t[nod]=(t[nod*2+1]*fu[nod*2+1]+t[nod*2]*fu[nod*2])%MOD;
}
inline ll s(ll i,ll j,ll nod){
	clean(nod);
	if(lt[nod]==i&&rt[nod]==j)return t[nod]%MOD;
	ll m=(lt[nod]+rt[nod])/2,sum=0;
	if(i<=m)sum+=s(i,min(j,m),nod*2);
	if(j>m)sum+=s(max(i,m+1),j,nod*2+1);
	return sum%MOD;
}
int main()
{
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	scanf("%lld",&n);
	for(ll i=1;i<=n;i++)scanf("%lld",&a[i]);
	build(1,n,1);
	printf("0 ");
	for(int i=2;i<=n;i++){
		xgadd(1,i-1,a[i]-a[i-1],1);
		printf("%lld ",s(1,i-1,1));
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define Ll long long
using namespace std;
Ll a[1000000],sum[1000000];
Ll m,n,x,y,z,ans,mo=1e9+7;
int max_min(int x){
	int l=1,r=n,ans=n+1,mid;
	while(r>=l){
		mid=(l+r)>>1;
		if(a[mid]>x){
			ans=min(mid,ans);
			r=mid-1;
		}else l=mid+1;
	}
	return ans;
}
int main()
{
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	scanf("%I64d",&m);
	if(!m)return 0;
	scanf("%I64d",&a[1]);
	sum[1]=a[1]%mo; n=1; m--;
	printf("0");
	while(m--){
		scanf("%I64d",&x);
		y=max_min(x);
		ans=x*(y-1)-sum[y-1]+sum[n]-sum[y-1]-(n-y+1)*x;
		ans%=mo;
		printf(" %I64d",ans);
		n++;
		for(int i=n;i>y;i--)a[i]=a[i-1],sum[i]=sum[i-1]+x;
		a[y]=x; sum[y]=sum[y-1]+x;
	}

}

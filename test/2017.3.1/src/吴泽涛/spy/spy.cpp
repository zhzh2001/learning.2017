#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;

const int nn=516,inf=700000000 ;
int n,a[nn][nn],vis[nn],f[nn] ,x,y,wzt;

inline void add(int x,int y)
{
	a[x][0]++;
	a[x][a[x][0]] = y;
}

inline void dfs(int u) 
{
	int v;
	for(int i=1;i<=a[ u ][ 0 ];i++)  
	{
		v = a[ u ][ i ] ;
		if( f[v]==0 ) 
		{
			f[ v ] = 1;
			vis[ v ]++;
			dfs( v ) ;
		}
	}
}

int main()
{
	freopen("spy.in","r",stdin) ;
	freopen("spy.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&x) ;
		if(x==0)
		{
			vis[ i ] = inf; 
			continue;
		}
		for(int j=1;j<=x;j++) 
		{
			scanf("%d",&y) ;
			add(y,i) ;
		}
	}
	for(int i=1;i<=n;i++) 
	{
		if(vis[ i ]!=inf) continue;
		for(int j=1;j<=n;j++) f[ j ] = 0;
		dfs( i ) ;  
	}
	wzt = 0;
	for(int i=1;i<=n;i++)
	{
		if(vis[ i ]<=1) 
		{
			wzt = 1;
			printf("%d\n",i) ;
		}
	}
	if(wzt==0) printf("BRAK\n") ; 
	
	return 0;
}



/*

9
0
1 1
1 2
2 2 3
2 1 7
1 5
0
2 7 9
2 7 8



*/

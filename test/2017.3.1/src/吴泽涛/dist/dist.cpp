#include <cstdio>
using namespace std;
const int inf=1000000007 ;
int a[100016],n,x ;
int sum ;
inline int abs(int x)
{
	if(x<0) x=-x;
	return x;
}
int main()
{
	freopen("dist.in","r",stdin) ;
	freopen("dist.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n;i++) scanf("%d",&a[ i ]) ;
	printf("0 ") ;
	for(int i=2;i<=n;i++) 
	{
		sum = 0;
		for(int j=1;j<=i-1;j++) 
		  sum+=abs(a[i]-a[j]) ;
		if(sum>=inf) sum-=inf; 
		 printf("%d ",sum) ; 
	}
	
	return 0;
}

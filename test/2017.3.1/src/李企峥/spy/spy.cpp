#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int f[1000],b[1000];
int a[510][510];
bool c[510][510];
int n,x,y,ans;
inline int read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int y)
{
	c[y][x]=true;
	f[x]++;
	For(i,1,a[x][0])
	{
		if (c[y][a[x][i]]==false) dfs(a[x][i],y);
	}
}
int main()
{
	freopen("spy.in","r",stdin);
	freopen("spy.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		x=read();
		if (x==0) 
		{
			f[i]=1;
			continue;
		}
		f[i]=0;
		For(j,1,x)
		{
			y=read();
			a[y][0]++;
			a[y][a[y][0]]=i;
		}
		b[i]=x;
	}
	For(i,1,n)
		For(j,1,n)
		{
			c[i][j]=false;
		}
	For(i,1,n)
	{
		if (b[i]==0) dfs(i,i);
	}
	For(i,1,n)
	{
		if (f[i]<=1&&b[i]!=0) cout<<i<<endl;
		ans++;
	}
	if(ans==0) cout<<"BRAK";
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;
int a[100000],n,sum1;
ll sum2,mod;
struct data{
	int l,r,num;
	ll woc;
}tree[4000000];

inline int read(){
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}

inline void build(int node,int l,int r){
	tree[node].l=l,tree[node].r=r;
	if(l==r) return;
	int mid=(l+r)/2;
	build(node*2,l,mid);
	build(node*2+1,mid+1,r);
}

inline void add(int node,int x){
	int l=tree[node].l,r=tree[node].r;
	if(l==r){
		tree[node].num++;
		tree[node].woc+=x;
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid) add(node*2,x);
	else add(node*2+1,x);
	tree[node].num=tree[node*2].num+tree[node*2+1].num;
	tree[node].woc=tree[node*2].woc+tree[node*2+1].woc;
}

inline ll query(int node,int pos){
	int l=tree[node].l,r=tree[node].r;
	if(r<=pos) return ((ll)tree[node].num*pos-tree[node].woc)%mod;
	if(l>=pos) return (tree[node].woc-(ll)tree[node].num*pos)%mod;
	int mid=(l+r)/2;
	return (query(node*2,pos)+query(node*2+1,pos))%mod;
}

int main(){
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	n=read();
	build(1,1,100000);
	a[1]=read();
	printf("0 ");
	add(1,a[1]);
	mod=1000000000+7;
	for(int i=2;i<=n;i++){
		a[i]=read();
		sum2=query(1,a[i])%mod;
		printf("%lld ",sum2);
		add(1,a[i]);
	}
	return 0;
}

#include <bits/stdc++.h>
#define File(s) freopen(s".in", "r", stdin), freopen(s".out", "w", stdout)
#define ll long long
#define N 100005
#define MOD 1000000007
using namespace std;
ll n, ans, last, now;
ll val[N<<2];
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);
	putchar(' ');
}
inline ll ask(ll id, ll l, ll r, ll L, ll R){
	if(l > r) return 0;
	if(L == l && R == r) return val[id];
	ll mid = L + R >> 1;
	if(r <= mid) return ask(id<<1, l, r, L, mid);
	if(l > mid) return ask(id<<1|1, l, r, mid+1, R);
	return ask(id<<1, l, mid, L, mid) + ask(id<<1|1, mid+1, r, mid+1, R);
}
inline void add(ll id, ll x, ll L, ll R){
	val[id]++;
	if(L==R) return;
	ll mid = L + R >> 1;
	if(x <= mid) return add(id<<1, x, L, mid);
	else return add(id<<1|1, x, mid+1, R);
}
int main(){
	File("dist");
	n=read();
	for(ll i = 0; i < n; i++){
		now=read();
		ll l = ask(1, 1, now-1, 1, N-5),
			r = ask(1, now+1, N-5, 1, N-5);
		ans += (l - r) * (now - last);
		writeln(ans%MOD);
		last = now; add(1, now, 1, N-5);
	}
	return 0;
}


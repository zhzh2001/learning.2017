#include <bits/stdc++.h>
#include <vector>
#define File(s) freopen(s".in", "r", stdin), freopen(s".out", "w", stdout)
#define ll long long
using namespace std;
int a[550], n, x, f[550], flag=0;
bool vis[550];
vector<int> v[550];
void dfs(int x){
	if(vis[x] || f[x] >= 2) return;
	vis[x] = 1;
	f[x]++;
	for(int i = 0; i < v[x].size(); i++)
		dfs(v[x][i]);
}
int main(){
	File("spy");
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
		scanf("%d", a+i);
		if(a[i]) for(int j = 1; j <= a[i]; j++){
			scanf("%d", &x);
			v[x].push_back(i);
		} else v[0].push_back(i);
	}
	for(int i = 0; i < v[0].size(); i++){
		memset(vis, 0, sizeof vis);
		dfs(v[0][i]);
	}
	for(int i = 1; i <= n; i++)
		if(a[i]&&f[i]<2) printf("%d\n", i), flag = 1;
	if(!flag) puts("BRAK");
	return 0;
}


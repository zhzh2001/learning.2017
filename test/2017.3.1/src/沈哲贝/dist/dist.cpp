#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define inf 2100000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 800010
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll sum[maxn],lsum[maxn],rsum[maxn];
ll n,mod;
void updata(ll p,ll l,ll r,ll mid){
	sum[p]=sum[p<<1]+sum[p<<1|1];
	lsum[p]=lsum[p<<1]+lsum[p<<1|1]+sum[p<<1|1]*(mid-l+1);
	rsum[p]=rsum[p<<1|1]+rsum[p<<1]+sum[p<<1]*(r-mid);
}
ll query(ll p,ll l,ll r,ll x){
	if (l==r)	return 0;
	ll mid=(l+r)>>1;
	return x<=mid?query(p<<1,l,mid,x)+lsum[p<<1|1]+sum[p<<1|1]*(mid-x+1):query(p<<1|1,mid+1,r,x)+rsum[p<<1]+sum[p<<1]*(x-mid);
}
void insert(ll p,ll l,ll r,ll x){
	if (l==r){
		sum[p]++;
		lsum[p]=rsum[p]=0;
		return;
	}
	ll mid=(l+r)>>1;
	if (x<=mid)	insert(p<<1,l,mid,x);
	else	insert(p<<1|1,mid+1,r,x);
	updata(p,l,r,mid);
}
int main(){
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	mod=1e9;	mod+=7;
	n=read();
	For(i,1,n){
		ll x=read();
		printf("%I64d ",query(1,1,100000,x)%mod);
		insert(1,1,100000,x);
	}
}

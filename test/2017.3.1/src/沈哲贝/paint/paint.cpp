#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1010
#define mod 200000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,m,mp[maxn][maxn],tot,lalala,mark1[maxn],mark2[maxn];
double answ;
double ppow(double x,ll k){
	double ans=1;
	while(k){
		if (k&1)	ans*=x;	x*=x;
		k/=2;
	}
	return ans;
}
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	lalala=read();	n=read();	m=read();
	For(i,1,n)	For(j,1,n){
		if (i>j)	mark1[j]+=j;
		if (i==j)	mark1[j]+=n;
		if (i<j)	mark1[j]+=n-j+1;
	}
	For(i,1,m)	For(j,1,m){
		if (i>j)	mark2[j]+=j;
		if (i==j)	mark2[j]+=m;
		if (i<j)	mark2[j]+=m-j+1;
	}
	For(i,1,n)	For(j,1,m)	mp[i][j]=mark1[i]*mark2[j];
	tot=n*m*n*m;
	For(i,1,n)	For(j,1,m){
		double ans=(double)(tot-mp[i][j])/tot;
		answ+=1-ppow(ans,lalala);
	}
	printf("%.10lf",answ);
}

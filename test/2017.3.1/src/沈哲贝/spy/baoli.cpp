#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll> 
#define inf 100000000
#define maxn 20010
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll sum[maxn],head[maxn],belong[maxn],ct[maxn],next[maxn],vet[maxn],q[maxn],low[maxn],dfn[maxn],ddd[maxn];
bool vis[maxn];
ll n,tim,tot,top,cnt;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void tarjan(ll x){
	dfn[x]=low[x]=++tim;	q[++top]=x;	vis[x]=1;
	for(ll i=head[x];i;i=next[i]){
		ll v=vet[i];
		if (!dfn[v]){
			tarjan(v);
			low[x]=min(low[x],low[v]);
		}	else	if (vis[v])	low[x]=min(low[x],low[v]);
	}
	if (low[x]==dfn[x]){
		cnt++;
		while (q[top+1]!=x){
			belong[q[top]]=cnt;
			vis[q[top--]]=0;
		}
	}
}
int main(){
	freopen("spy.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n){
		ll x=read();
		if (!x)	ddd[i]=1;
		while(x--){
			ll v=read();
			insert(v,i);
		}
	}
	For(i,1,n)	if (!dfn[i])	tarjan(i);
	For(x,1,n){
		for(ll i=head[x];i;i=next[i]){
			ll v=vet[i];
			if (belong[v]!=belong[x]){
				insert(belong[x]+n,belong[v]);
				sum[belong[v]]++;
			}
		}
	}
	ll h=0,t=0;
	For(i,1,cnt)
	if (!sum[i])	q[++t]=i,ct[i]=i;
	while(h!=t){
		ll x=q[++h];
		for(ll i=head[x+n];i;i=next[i]){
			ll v=vet[i];
			sum[v]--;
			if (!sum[v])	q[++t]=v;
			if (!ct[v])	ct[v]=ct[x];
			else if (ct[v]!=-1)	if (ct[v]!=ct[x])	ct[v]=-1;
		}
	}
	For(i,1,n)	if (ct[belong[i]]!=-1&&!ddd[i])	writeln(i);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll> 
#define inf 100000000
#define maxn 500010
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll next[maxn],q[maxn],head[maxn],vet[maxn],vis[maxn],mark[maxn];
ll n,tot,t;
bool flag=0; 
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void dfs(ll x,ll c){
	vis[x]=c;
	mark[x]++;
	for(ll i=head[x];i;i=next[i]){
		ll v=vet[i];
		if (vis[v]!=c)	dfs(v,c);
	}
}
int main(){
	freopen("spy.in","r",stdin);
	freopen("spy.out","w",stdout);
	n=read();
	For(i,1,n){
		ll x=read();
		if (!x)	q[++t]=i,mark[i]=1;
		while (x--){
			ll v=read();
			insert(v,i);
		}
	}
	For(i,1,t)	dfs(q[i],i);
	For(i,1,n)	if (mark[i]<2)	writeln(i),flag=1;
	if (!flag)	puts("BRAK");
}

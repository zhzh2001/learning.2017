#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#include<ctime>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll> 
#define inf 100000000
#define maxn 20010
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,mark[maxn],t[maxn];
int main(){
	freopen("spy.in","w",stdout);
	srand(time(0));
	n=rand()%500+1;
	printf("%d\n",n);
	For(i,1,n){
		t[i]=rand()%50;
		mark[i]=!t[i];
	}
	ll x=rand()%100+1;
	t[x]=0;	mark[x]=1; 
	For(i,1,n){
		printf("%d ",t[i]);
		while (t[i]){
			ll x=rand()%n+1;
			if (mark[x]){
				t[i]--;
				printf("%d ",x);
				break;
			}
		}
		while(t[i]--){
			ll x=rand()%n+1;
			printf("%d ",x);
		}
		puts("");
	}
}

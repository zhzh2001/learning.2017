#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#define N 100005
#define ll long long
using namespace std;
ll n,x,y,z,tot;
struct treearray{
	ll t[N];
	ll ask(ll x){
		ll s=0;
		for (;x;x-=x&(-x)) s+=t[x];
		return s;
	}
	void change(ll x,ll y){
		for (;x<N;x+=x&(-x)) t[x]+=y;
	}
}a,b;
int main(){
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	scanf("%I64d",&n);
	for (ll i=1;i<=n;i++){
		scanf("%I64d",&x);
		y=a.ask(x);
		z=b.ask(x);
		printf("%I64d ",(x*y-z)+(tot-z-(i-y-1)*x));
		tot+=x;
		a.change(x,1);
		b.change(x,x);
	}
}

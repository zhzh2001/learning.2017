#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#define ll long long
using namespace std;
ll n,m,tot,sum;
double ans,p,s;
int k,kk;
int main(){
	freopen("paint.in","r",stdin);
	freopen("paint.out","w",stdout);
	scanf("%d%I64d%I64d",&k,&n,&m);
	tot=n*n*m*m;
	ans=n*m;
	for (ll i=1;i<=n;i++)
		for (ll j=1;j<=m;j++){
			sum=(2*i*(n-i+1)-1)*(2*j*(m-j+1)-1);
			p=1-(double)sum/tot;
			s=1;
			kk=k;
			for (;kk;kk/=2,p=p*p)
				if (kk&1) s=s*p;
			ans-=s;
		}
	printf("%.12lf",ans);
}

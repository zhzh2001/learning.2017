#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#define N 505
using namespace std;
struct edge{int to,next;}e[N*N];
int a[N],head[N],q[N],dis[N],vis[N];
int n,s,x,tot,h,t,flag;
int main(){
	freopen("spy.in","r",stdin);
	freopen("spy.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&s);
		if (!s) a[++tot]=i;
		else while (s--){
			scanf("%d",&x);
			e[++tot].to=i;
			e[tot].next=head[x];
			head[x]=tot;
		}
	}
	for (int i=1;i<=tot;i++){
		memset(vis,0,sizeof(vis));
		h=t=0;
		for (int j=1;j<=tot;j++)
			if (i!=j) vis[a[j]]=1,q[++t]=a[j];
		while (h<t){
			x=q[++h];
			for (int j=head[x];j;j=e[j].next)
				if (!vis[e[j].to]){
					vis[e[j].to]=1;
					q[++t]=e[j].to;
				}
		}
		vis[a[i]]=1;
		for (int j=1;j<=n;j++) dis[j]+=vis[j];
	}
	for (int i=1;i<=n;i++)
		if (dis[i]==tot-1){
			printf("%d\n",i);
			flag=1;
		}
	if (!flag) printf("BRAK");
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
bool vis[1001],tj[1001];
int dp[1001],ans[1001],tot,num;
bool nor=0;
int n,x,t,cnt,to[1001],poi[1000001],f[1001],next[1000001],v[1000001];
inline void add(int x,int y)
{
	poi[++tot]=y;next[tot]=f[x];v[tot]=1;f[x]=tot;
}
inline void dfs(int x)
{
	dp[x]=1;
	vis[x]=1;
	for(int t=f[x];t;t=next[t])
		if(!vis[poi[t]]&&v[t]!=0)	dfs(poi[t]);
}
inline void solve (int x)
{
	for(int i=f[to[x]];i;i=next[i])	v[i]=0;
	for(int i=1;i<=n;i++)	vis[i]=0,dp[i]=0;

	for(int i=1;i<=cnt;i++)
		for(int j=f[to[i]];j;j=next[j])
			dp[poi[j]]+=v[j];	
	for(int i=1;i<=n;i++)
		if(dp[i]!=0&&!vis[i])	dfs(i);
	for(int i=1;i<=n;i++)	if(dp[i]==0)	ans[++num]=i;
	for(int i=f[to[x]];i;i=next[i])	v[i]=1;		
}
int main()
{
	freopen("spy.in","r",stdin);
	freopen("spy.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		for(int j=1;j<=x;j++)	
		{
			int t;
			cin>>t;
			add(t,i);
		}
		if(x==0)
			to[++cnt]=i,tj[i]=1;
	}
	for(int i=1;i<=cnt;i++)
		solve(i);
	sort(ans+1,ans+num+1);
	for(int i=1;i<=num;i++)	if(ans[i]!=ans[i-1]&&!tj[ans[i]])printf("%d\n",ans[i]),nor=1;
	if(!nor)	cout<<"BRAK"<<endl;
}
/*
9
0
1 1
1 2
2 2 3
2 1 7
1 5
0 
2 7 9
2 7 8
*/

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#define ll long long
using namespace std;
int n,ma,mi=1e9;
int mo=1e9+7;
ll have[100001],num1[100001],num2[100001],dp1[100001],dp2[100001];
ll f1[100001],f2[100001],f3[100001],f4[100001];
inline int lowbit(int x){return x&-x;}
inline void add1(int t,int x)
{
	for(int i=t;i<=100001;i+=lowbit(i))	f1[i]+=x;
}
inline ll get1(int t)
{
	ll sum=0;
	for(int i=t;i;i-=lowbit(i))	sum+=f1[i];
	return sum;
}
inline void addnum1(int t,int x)
{
	for(int i=t;i<=100001;i+=lowbit(i))	f2[i]+=x;
}
inline ll getnum1(int t)
{
	ll sum=0;
	for(int i=t;i;i-=lowbit(i))	sum+=f2[i];
	return sum;
}
inline void add2(int t,int x)
{
	for(int i=t;i<=100001;i+=lowbit(i))	f3[i]+=x;
}
inline ll get2(int t)
{
	ll sum=0;
	for(int i=t;i;i-=lowbit(i))	sum+=f3[i];
	return sum;
}
inline void addnum2(int t,int x)
{
	for(int i=t;i<=100001;i+=lowbit(i))	f4[i]+=x;
}
inline ll getnum2(int t)
{
	ll sum=0;
	for(int i=t;i;i-=lowbit(i))	sum+=f4[i];
	return sum;
}
inline void doit(int t,int m)
{
	bool flag1=0,flag2=0;

	int l=0,r=100001;
	for(int i=0;i<=100001;i++)
	{

		if(t+i<=ma)
		if(have[t+i]&&!flag1)	
		{
			r=t+i;
			flag1=1;
			if(flag2)	break;
		}
		if(t-i>=mi)
		if(have[t-i]&&!flag2)
		{
			l=t-i;
			flag2=1;
			if(flag1)	break;
		}
		if(t+i>=ma+1&&t-i<=mi-1)	break;
	}
//	cout<<l<<' '<<r<<' '<<endl;
	have[t]=1;
	ma=max(ma,t);
	mi=min(mi,t);
	ll ans=(ll)(t*getnum1(l)-get1(l))+(ll)((100001-t)*getnum2(100001-r)-get2(100001-r));
	ans%=mo;
	printf("%d ",ans);
	add1(t,t);
	addnum1(t,1);
	add2(100001-t,100001-t);
	addnum2(100001-t,1);
}
int main()
{
	freopen("dist.in","r",stdin);
	freopen("dist.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		int x;
		scanf("%d",&x);
		doit(x,i);
	}
}
/*
5
3 4 7 5 6
*/

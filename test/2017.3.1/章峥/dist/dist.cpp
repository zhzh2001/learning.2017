#include<fstream>
#include<cstring>
using namespace std;
const int MOD=1000000007,L=100005;
ifstream fin("dist.in");
ofstream fout("dist.out");
struct BIT
{
	int tree[L];
	BIT()
	{
		memset(tree,0,sizeof(tree));
	}
	inline void update(int x,int val)
	{
		for(;x<L;x+=x&-x)
			tree[x]=(tree[x]+val)%MOD;
	}
	inline int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans=(ans+tree[x])%MOD;
		return ans;
	}
}sum,cnt;
int main()
{
	int n;
	fin>>n;
	while(n--)
	{
		int x;
		fin>>x;
		fout<<(((long long)x*cnt.query(x-1)-(long long)x*(cnt.query(L-1)-cnt.query(x)+MOD)-sum.query(x-1)+sum.query(L-1)-sum.query(x))%MOD+MOD)%MOD<<' ';
		cnt.update(x,1);
		sum.update(x,x);
	}
	fout<<endl;
	return 0;
}
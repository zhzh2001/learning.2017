#include<bits/stdc++.h>
using namespace std;
ifstream fin("dist.in");
ofstream fout("dist.ans");
const int MOD=1000000007,N=100005;
int a[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=n;i++)
	{
		int ans=0;
		for(int j=1;j<i;j++)
			ans=(ans+abs(a[i]-a[j]))%MOD;
		fout<<ans<<' ';
	}
	fout<<endl;
	return 0;
}
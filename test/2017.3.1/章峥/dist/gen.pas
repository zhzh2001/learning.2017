program gen;
const
  n=100000;
  m=100000;
var
  i:longint;
begin
  randomize;
  assign(output,'dist.in');
  rewrite(output);
  writeln(n);
  for i:=1 to n do
    write(random(m)+1,' ');
  writeln;
  close(output);
end.
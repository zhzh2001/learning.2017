@echo off
for /d %%s in (*) do (
cd %%s
echo %%s
g++ %%s.cpp
set /p n=
for /l %%i in (1,1,%n%) do (
copy con %%s.in
a.exe
type %%s.out
)
del a.exe
pause
cd ..
)

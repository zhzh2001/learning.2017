#include<bits/stdc++.h>
using namespace std;
ifstream fin("paint.in");
ofstream fout("paint.out");
int cnt[1000005];
bool t[1005][1005];
int main()
{
	int k,n,m;
	fin>>k>>n>>m;
	double ans=0;
	if(k==1)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				for(int k=1;k<=n;k++)
					for(int l=1;l<=m;l++)
						cnt[(abs(k-i)+1)*(abs(l-j)+1)]++;
		for(int i=1;i<=n*m;i++)
			ans+=(double)i*cnt[i]/(n*n*m*m);
	}
	else
	{
		default_random_engine gen(time(NULL));
		for(int i=1;;i++)
		{
			for(int x=1;x<=n;x++)
				for(int y=1;y<=m;y++)
					t[x][y]=false;
			uniform_int_distribution<> nd(1,n),md(1,m);
			for(int j=1;j<=k;j++)
			{
				int x1=nd(gen),y1=md(gen),x2=nd(gen),y2=md(gen);
				if(x1>x2)
					swap(x1,x2);
				if(y1>y2)
					swap(y1,y2);
				for(int x=x1;x<=x2;x++)
					for(int y=y1;y<=y2;y++)
						t[x][y]=true;
			}
			int now=0;
			for(int x=1;x<=n;x++)
				for(int y=1;y<=m;y++)
					now+=t[x][y];
			cnt[now]++;
			if(clock()>1900)
			{
				for(int j=1;j<=n*m;j++)
					ans+=(double)j*cnt[j]/i;
				break;
			}
		}
	}
	fout.precision(8);
	fout<<fixed<<ans<<endl;
	return 0;
}
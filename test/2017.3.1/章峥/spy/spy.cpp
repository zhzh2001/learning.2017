#include<fstream>
#include<vector>
#include<cstring>
using namespace std;
ifstream fin("spy.in");
ofstream fout("spy.out");
const int N=505;
vector<int> mat[N];
bool into[N],vis[N];
int cnt[N];
void dfs(int k)
{
	vis[k]=true;
	cnt[k]++;
	for(int i=0;i<mat[k].size();i++)
		if(!vis[mat[k][i]])
			dfs(mat[k][i]);
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		int k;
		fin>>k;
		into[i]=k;
		while(k--)
		{
			int u;
			fin>>u;
			mat[u].push_back(i);
		}
	}
	for(int i=1;i<=n;i++)
		if(!into[i])
		{
			memset(vis,false,sizeof(vis));
			dfs(i);
		}
	bool found=false;
	for(int i=1;i<=n;i++)
		if(into[i]&&cnt[i]<2)
		{
			fout<<i<<endl;
			found=true;
		}
	if(!found)
		fout<<"BRAK\n";
	return 0;
}
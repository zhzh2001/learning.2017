#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <cstdio>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

typedef	long long	int64;
typedef double db;

FILE *Finn, *Fout, *Fstd, *Fres;

void Return(double p, char* s,double error)
{
	fprintf(Fres, "%.3lf\n%s error=%.10lf\n", p, s,error);
	exit(0);
}
int	main(int args, char** argv)
{
	Finn = fopen(argv[1], "r");
	Fstd = fopen(argv[2], "r");
	Fout = fopen(argv[3], "r");
	Fres = fopen(argv[4], "w");
	db a,b;
	fscanf(Fstd,"%lf",&a);
	fscanf(Fout,"%lf",&b);
	if(fabs(a-b)>1e-6) Return(0,"WA",fabs(a-b));
	Return(1,"AC",fabs(a-b));
}

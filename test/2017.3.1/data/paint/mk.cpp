#include<set>
#include<map>
#include<cmath>
#include<string>
#include<cstdio>
#include<vector>
#include<cctype>
#include<cstdlib>
#include<sstream>
#include<cstring>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fir first
#define sec second

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

int IN()
{
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int main()
{
	freopen("paint5.in","w",stdout);
	printf("32 407 341");
	freopen("paint5.out","w",stdout);
	printf("116240.54757560963");
	
	freopen("paint6.in","w",stdout);
	printf("22 746 624");
	freopen("paint6.out","w",stdout);
	printf("361402.1850454167");
	
	freopen("paint7.in","w",stdout);
	printf("74 481 873");
	freopen("paint7.out","w",stdout);
	printf("385866.8707868741");
	
	freopen("paint8.in","w",stdout);
	printf("50 164 158");
	freopen("paint8.out","w",stdout);
	printf("23216.23645834032");
	
	freopen("paint9.in","w",stdout);
	printf("77 412 452");
	freopen("paint9.out","w",stdout);
	printf("171908.35227779343");
	
	freopen("paint10.in","w",stdout);
	printf("100 1000 1000");
	freopen("paint10.out","w",stdout);
	printf("936523.8255150901");
}
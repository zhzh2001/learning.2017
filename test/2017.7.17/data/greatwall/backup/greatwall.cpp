#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define rep(i,n) for (int i=1;i<=n;++i)
const int N=600005;
int n,m,x[N],y[N],c[N],d[N];
long long work(int *x,int *y)
{
	long long res=0; rep(i,n) c[i]=-1,d[i]=0; rep(i,n) ++d[x[i]],++c[y[i]];
	rep(i,n) if (d[i]*2>=n){m=i; break;} else d[i+1]+=d[i];
	rep(i,n) res+=abs(c[i]),c[i+1]+=c[i],res+=abs(x[i]-m);
	return res;
}
int main()
{
	while (scanf("%d",&n)!=EOF){
		rep(i,n) scanf("%d%d",x+i,y+i);
		printf("%I64d\n",min(work(x,y),work(y,x)));
	}
	return 0;
}

#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("history.in");
const int n = 10000;
int f[n];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << ' ' << n - 1 << endl;
	for (int i = 0; i < n; i++)
		f[i] = i;
	for (int i = 1; i < n; i++)
	{
		uniform_int_distribution<> dn(0, n - 1);
		int u = dn(gen), v = dn(gen), ru = getf(u), rv = getf(v);
		if (ru == rv)
		{
			i--;
			continue;
		}
		f[ru] = rv;
		fout << "R " << u << ' ' << v << endl;
	}
	return 0;
}
#include <fstream>
#include <ext/rope>
#include <cstdlib>
#include <ctime>
using namespace std;
ifstream fin("history.in");
ofstream fout("history.out");
const int N = 3e5;
int a[N];
__gnu_cxx::rope<int> *f[N];
int getf(int id, int x)
{
	int fat = f[id]->at(x);
	if (fat == x)
		return x;
	int rt = getf(id, fat);
	if (fat != rt)
		f[id]->replace(x, rt);
	return rt;
}
int main()
{
	srand(time(NULL));
	int n, m;
	fin >> n >> m;
	for (int i = 0; i < n; i++)
		a[i] = i;
	f[0] = new __gnu_cxx::rope<int>(a, a + n);
	int c = 0, year = 0;
	bool lastans = true;
	while (m--)
	{
		char opt;
		int x, y, rx, ry, t;
		bool now, pred;
		fin >> opt;
		switch (opt)
		{
		case 'K':
			fin >> c;
			lastans = true;
			break;
		case 'R':
			year++;
			f[year] = f[year - 1];
			fin >> x >> y;
			if (!lastans)
			{
				x = (x + c) % n;
				y = (y + c) % n;
			}
			rx = getf(year, x);
			ry = getf(year, y);
			if (rx != ry)
			{
				f[year] = new __gnu_cxx::rope<int>(*f[year - 1]);
				if (rand() & 1)
					f[year]->replace(rx, ry);
				else
					f[year]->replace(ry, rx);
			}
			break;
		case 'T':
			fin >> x >> y >> t;
			t = year - t;
			if (t < 0)
				t = 0;
			now = getf(year, x) == getf(year, y);
			pred = getf(t, x) == getf(t, y);
			lastans = now && !pred;
			if (lastans)
				fout << "Y\n";
			else
				fout << "N\n";
			break;
		}
	}
	return 0;
}
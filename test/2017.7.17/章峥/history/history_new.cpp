#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("history.in");
ofstream fout("history.out");
const int n = 3e5;
int f[n];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
struct quest
{
	char opt;
	int x, y;
} q[n];
struct quest2
{
	int t, x, y;
	bool operator<(const quest2 &rhs) const
	{
		return t < rhs.t;
	}
} q2[n];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 0; i < n; i++)
		f[i] = i;
	int q2c = 0, year = 0;
	for (int i = 0; i < m; i++)
	{
		int t;
		fin >> q[i].opt;
		switch (q[i].opt)
		{
		case 'K':
			fin >> q[i].x;
			year++;
			break;
		case 'R':
			fin >> q[i].x >> q[i].y;
			break;
		case 'T':
			fin >> q[i].x >> q[i].y >> t;
			q2[q2c].x = q[i].x;
			q2[q2c].y = q[i].y;
			q2[q2c++].t = max(year - t, 0);
			break;
		}
	}
	sort(q2, q2 + q2c);
	int c = 0;
	return 0;
}
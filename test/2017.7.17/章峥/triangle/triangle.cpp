#include <fstream>
#include <string>
using namespace std;
ifstream fin("triangle.in");
ofstream fout("triangle.out");
const int N = 105;
string mat[N];
bool bl[26];
int s[N][N][4], ans[26];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> mat[i];
		mat[i] = ' ' + mat[i];
		for (int j = 1; j <= n; j++)
			bl[mat[i][j] - 'A'] = true;
	}
	int sum = 0;
	for (int c = 0; c < 26; c++)
		if (bl[c])
		{
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= n; j++)
				{
					bool now = mat[i][j] == c + 'A';
					s[i][j][0] = s[i - 1][j - 1][0] + now;
					s[i][j][1] = s[i - 1][j + 1][1] + now;
					s[i][j][2] = s[i][j - 1][2] + now;
					s[i][j][3] = s[i - 1][j][3] + now;
				}
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= n; j++)
					if (mat[i][j] == c + 'A')
					{
						for (int k = 1; i - k && j + k <= n; k++)
							if (s[i][j + k][0] - s[i - k - 1][j - 1][0] == k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; j - k && i + k <= n; k++)
							if (s[i + k][j][0] - s[i - 1][j - k - 1][0] == k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; i + k <= n && j + k <= n; k++)
							if (s[i + k][j][1] - s[i - 1][j + k + 1][1] == k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; j - k && i - k; k++)
							if (s[i][j - k][1] - s[i - k - 1][j + 1][1] == k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; i + k <= n && j - k && j + k <= n; k++)
							if (s[i + k][j + k][2] - s[i + k][j - k - 1][2] == 2 * k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; i - k && j - k && j + k <= n; k++)
							if (s[i - k][j + k][2] - s[i - k][j - k - 1][2] == 2 * k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; i + k <= n && j - k && i - k; k++)
							if (s[i + k][j - k][3] - s[i - k - 1][j - k][3] == 2 * k + 1)
								ans[c]++;
							else
								break;
						for (int k = 1; i + k <= n && j + k <= n && i - k; k++)
							if (s[i + k][j + k][3] - s[i - k - 1][j + k][3] == 2 * k + 1)
								ans[c]++;
							else
								break;
					}
			sum += ans[c];
		}
	fout << sum << endl;
	for (int c = 0; c < 26; c++)
		if (bl[c])
			fout << (char)(c + 'A') << "  " << ans[c] << endl;
	return 0;
}
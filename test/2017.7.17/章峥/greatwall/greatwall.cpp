#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("greatwall.in");
ofstream fout("greatwall.out");
const int N = 600005;
int x[N], y[N];
int main()
{
	int n;
	fin >> n;
	long long sumx = 0, sumy = 0;
	for (int i = 1; i <= n; i++)
	{
		fin >> x[i] >> y[i];
		sumx += x[i];
		sumy += y[i];
	}
	nth_element(x + 1, x + (n + 1) / 2, x + n + 1);
	nth_element(y + 1, y + (n + 1) / 2, y + n + 1);
	long long ans1 = abs(1ll * (1 + n) * n / 2 - sumx);
	for (int i = 1; i <= n; i++)
		ans1 += abs(y[i] - y[(n + 1) / 2]);
	long long ans2 = abs(1ll * (1 + n) * n / 2 - sumy);
	for (int i = 1; i <= n; i++)
		ans2 += abs(x[i] - x[(n + 1) / 2]);
	fout << min(ans1, ans2) << endl;
	return 0;
}
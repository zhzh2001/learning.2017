#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
using namespace std;
int n,ch[26],ans;
char ma[130][130],line[130];
bool Zn[26];
int same[130][130][130];

int JJMD(int lin,int st,int ed)
{
	if(same[lin][st][ed]!=1)
		return 0;
	int i;
	bool k1=false,k2=false,k3=((ed-st+1)%2!=1),k4=false,k5=false,k6=k3;
	for(i=lin+1;i<=ed+lin-st;i++)
	{
		if(i>n)
		{
			k1=true;
			k2=true;
			if(i<=lin+(ed-st)/2)
				k3=true;
			break;
		}
		if(!(same[i][st][ed-i+lin]&&ma[i][st]==ma[lin][st])&&!k1)
			k1=true;
		if(!(same[i][st+i-lin][ed]&&ma[i][ed]==ma[lin][ed])&&!k2)
			k2=true;
		if(!k3&&i<=lin+(ed-st)/2)
			if(!(same[i][st+i-lin][ed-i+lin]&&ma[i][ed-i+lin]==ma[lin][ed]))
				k3=true;
		if(k1&&k2&&k3)
			break;
	}
	for(i=lin-1;i>=lin-ed+st;i--)
	{
		if(i<=0)
		{
			k4=true;
			k5=true;
			if(i>=lin-(ed-st)/2)
				k6=true;
			break;
		}
		if(!(same[i][st][ed+i-lin]&&ma[i][st]==ma[lin][st])&&!k4)
			k4=true;
		if(!(same[i][st-i+lin][ed]&&ma[i][ed]==ma[lin][st])&&!k5)
			k5=true;
		if(!k6&&i>=lin-(ed-st)/2)
			if(!(same[i][st-i+lin][ed+i-lin]&&ma[i][ed+i-lin]==ma[lin][ed]))
				k6=true;
		if(k4&&k5&&k6)
			break;
	}
	int a;
	a=!k1+!k2+!k3+!k4+!k5+!k6;
	if(!k1&&!k4)
		a++;
	if(!k2&&!k5)
		a++;
	return a;
}

int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	int i,j,k;
	for(i=1;i<=n;i++)
	{
		scanf("%s",line);
		for(j=1;j<=n;j++)
		{
			ma[i][j]=line[j-1];
			Zn[ma[i][j]-'A']=1;
		}
	}
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
		{
			same[i][j][j]=1;
			for(k=j+1;k<=n;k++)
			{
				if(same[i][j][k-1]==1&&ma[i][k-1]==ma[i][k])
					same[i][j][k]=1;
				else
					same[i][j][k]=0;
			}
		}
	int t;
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
			for(k=j+1;k<=n;k++)
			{
				t=JJMD(i,j,k);
				ch[ma[i][k]-'A']+=t;
				ans+=t;
			}
	printf("%d\n",ans);
	for(i=0;i<26;i++)
		if(Zn[i])
			printf("%c  %d\n",i+'A',ch[i]);
	return 0;
}

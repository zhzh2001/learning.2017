#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=600010;
int n,sum[N],buk[N],buk2[N],sum2[N];
long long ans,ans2,minn;

struct node
{
	int x,y;
}point[N],no[N];

bool cmp1(node i,node j)
{
	if(i.x==j.x)
		return i.y<j.y;
	return i.x<j.x;
}

bool cmp2(node i,node j)
{
	if(i.y==j.y)
		return i.x<j.x;
	return i.y<j.y;
}

int Abs(int i)
{
	if(i<0)
		return -i;
	return i;
}

int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	scanf("%d",&n);
	int i;
	for(i=1;i<=n;i++)
	{
		scanf("%d%d",&point[i].x,&point[i].y);
		no[i].x=point[i].x;
		no[i].y=point[i].y;
	}
	sort(point+1,point+n+1,cmp1);
	sort(no+1,no+n+1,cmp2);
	for(i=1;i<=n;i++)
	{
		buk[point[i].x]++;
		buk2[point[i].y]++;
	}
	ans=0;
	ans2=0;
	int now=1;
	for(i=1;i<=n;i++)
	{
		ans+=buk[i]*(i-1);
		sum[i]=sum[i-1]+buk[i];
		ans+=Abs(no[i].y-now);
		
		ans2+=buk2[i]*(i-1);
		sum2[i]=sum2[i-1]+buk2[i];
		ans2+=Abs(point[i].x-now);
		
		now++;
	}
	minn=ans;
	if(ans2<minn)
		minn=ans2;
	for(i=2;i<=n;i++)
	{
		ans-=sum[n]-sum[i-1];
		ans+=sum[i-1];
		if(ans<minn)
			minn=ans;
		ans2-=sum2[n]-sum2[i-1];
		ans2+=sum2[i-1];
		if(ans2<minn)
			minn=ans2;
	}
	printf("%lld\n",minn);
	return 0;
}

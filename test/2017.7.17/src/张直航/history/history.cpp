#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
int n,m;
int cofind[3000][1000];
char read()
{
	char x;
	for(x=getchar();x!='R'&&x!='T'&&x!='K';x=getchar());
	return x;
}

struct edge
{
	int to;
	int nxt;
	int year;
}po[7000];
int tot,edgenum[2000],pos[4000];

void add_edge(int s,int t,int y)
{
	po[++tot].to=t;
	po[tot].year=y;
	po[tot].nxt=edgenum[s];
	edgenum[s]=tot;
}

void dfs(int now,int s,int t,int year)
{
	cofind[year][now]=s;
	if(cofind[year][t]!=-1)
		return ;
	int i,to;
	for(i=edgenum[now];i;i=po[i].nxt)
		if(po[i].year<=year)
		{
			to=po[i].to;
			if(cofind[year][to]==-1)
				dfs(to,s,t,year);
		}
}

int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(cofind,-1,sizeof(cofind));
	char cmd;
	int s,t,c=0,k=0,year=0,r;
	bool con1,con2;
	while(m--)
	{
		cmd=read();
		if(cmd=='R')
		{
			++year;
			scanf("%d%d",&s,&t);
			s=(s+k)%n;
			t=(t+k)%n;
			add_edge(s,t,year);
			add_edge(t,s,year);
			pos[year]=tot-1;
		}
		else if(cmd=='T')
		{
			k=0;
			con1=con2=true;
			scanf("%d%d%d",&s,&t,&r);
			if(year-r<=0)
				con1=false;
			else
			{
				if(cofind[year-r][s]==-1||cofind[year-r][t]==-1)
				{
					memset(cofind[year-r],-1,sizeof(cofind[year-r]));
					dfs(s,s,t,year-r);
				}
				con1=(cofind[year-r][s]==cofind[year-r][t]);
			}
			if(cofind[year][s]==-1||cofind[year][t]==-1)
			{
				memset(cofind[year],-1,sizeof(cofind[year]));
				dfs(s,s,t,year);
			}
			con2=(cofind[year][s]==cofind[year][t]);
			if(!con1&&con2)
				printf("Y\n");
			else
			{
				printf("N\n");
				k=c;
			}
		}
		else if(cmd=='K')
		{
			scanf("%d",&c);
			k=0;
		}
	}
	return 0;
}

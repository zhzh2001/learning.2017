#include<bits/stdc++.h>
using namespace std;
int read(){
	int tot=0;
	int f=1;
	char c=getchar();
	while(!isdigit(c) && c!='-')
		c=getchar();
	if(c=='0'){
		f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		tot=tot*10+c-'0';
		c=getchar();
	}
	return tot*f;
}
int n;
long long ans=1e18;
int x[600010],y[600010];
int h[600010],s[600010];
long long dh[600010],ds[600010];
int main(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=read();
	long long aa=(1+n)*n/2,bb;
	long long tot;
	for(int i=1;i<=n;i++){
		x[i]=read(),y[i]=read();
		h[x[i]]++;
		s[y[i]]++;
		long long data=0;
		for(int j=x[i]+1;j<=n;j++)
			dh[j]+=(++data);
		data=0;
		for(int j=x[i]-1;j>=1;j--)
			dh[j]+=(++data);
		data=0;
		for(int j=y[i]+1;j<=n;j++)
			ds[j]+=(++data);
		data=0;
		for(int j=y[i]-1;j>=1;j--)
			ds[j]+=(++data);
	}
	int b1=0,b2=0;;
	for(int i=1;i<=n;++i){
		h[i]*=i;
		s[i]*=i;
		b1+=s[i];
		b2+=h[i];
	}
	for(int i=1;i<=n;++i){
		tot=dh[i];
		tot+=(abs(aa-b1));
		ans=min(tot,ans);
		tot=ds[i];
		tot+=(abs(aa-b1));
		ans=min(tot,ans);
	}
	cout<<ans<<"\n";
	return 0;	

}


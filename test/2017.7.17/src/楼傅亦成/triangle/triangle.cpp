#include<bits/stdc++.h>
using namespace std;
int n;
char a[110][110];
int f[110][110][5];
int c[110][110][5][110];
int dx[]={0,-1,1,0,0};
int dy[]={0,0,0,-1,1};
long long ans[101];
void prep(){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=1;k<=4;k++)
				f[i][j][k]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]==a[i][j-1])
				f[i][j][3]=f[i][j-1][3]+1;
			if(a[i][j]==a[i-1][j])
				f[i][j][1]=f[i-1][j][1]+1;
		}
	for(int i=n;i>=1;i--)
		for(int j=n;j>=1;j--){
			if(a[i][j]==a[i+1][j])
				f[i][j][2]=f[i+1][j][2]+1;
			if(a[i][j]==a[i][j+1])
				f[i][j][4]=f[i][j+1][4]+1;
		}
	return;
}
int check1(int x,int y,int k){
	for(int i=x+1;i<x+f[x][y][2];i++)
		if(f[i][y][4]<(--k))
			return 0;
	return 1;
}//�� ��1 
int check2(int x,int y,int k){
	for(int i=x-1;i>x-f[x][y][1];i--)
		if(f[i][y][4]<(--k))
			return 0;
	return 1;
}//�� ��1 
int check3(int x,int y,int k){
	for(int i=x-1;i>x-f[x][y][1];i--)
		if(f[i][y][3]<(--k))
			return 0;
	return 1;
}//�� �� 1 
int check4(int x,int y,int k){
	for(int i=x+1;i<x+f[x][y][2];i++)
		if(f[i][y][3]<(--k))
			return 0;
	return 1;
}//�� �� 1 
int cal(int x,int y){
	int r=f[x][y][1];
	for(int i=2;i<=4;i++)
		r=max(r,f[x][y][i]);
	return r;
}
void work(){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=2;k<=min(f[i][j][2],f[i][j][4]);k++)
				ans[a[i][j]-'A']+=(c[i][j][1][k]=check1(i,j,k));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=2;k<=min(f[i][j][1],f[i][j][4]);k++)
				ans[a[i][j]-'A']+=(c[i][j][2][k]=check2(i,j,k));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=2;k<=min(f[i][j][1],f[i][j][3]);k++){
				ans[a[i][j]-'A']+=(c[i][j][3][k]=check3(i,j,k));
			}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=2;k<=min(f[i][j][2],f[i][j][3]);k++)
				ans[a[i][j]-'A']+=(c[i][j][4][k]=check4(i,j,k));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=2;k<=cal(i,j);k++){
				if(c[i][j][3][k] && c[i][j][2][k])
					ans[a[i][j]-'A']++;
				if(c[i][j][3][k] && c[i][j][4][k])
					ans[a[i][j]-'A']++;
				if(c[i][j][1][k] && c[i][j][2][k])
					ans[a[i][j]-'A']++;
				if(c[i][j][1][k] && c[i][j][4][k])
					ans[a[i][j]-'A']++;
			}
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			cin>>a[i][j];
			ans[a[i][j]-'A']=1;
		}
	prep();		
	work();
	for(int i=0;i<26;i++)
		if(ans[i]>0)
			cout<<(char)('A'+i)<<"  "<<ans[i]-1<<"\n";
	return 0;
}

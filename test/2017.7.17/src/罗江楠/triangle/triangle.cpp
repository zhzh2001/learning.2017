#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char ch[105];
int mp[30][105][105], res[30], a[105][105], n;
bool vis[30];
int work(int x){
	int ans = 0;
	/////////
	// .   //
	// |\  //
	// |_\ //
	/////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int i = 1; i <= n; i++)
		for(int j = n; j; j--)if(a[i][j]){
			a[i][j] = min(a[i-1][j]+1, a[i][j+1]+1);
			ans += a[i][j]-1;
		}

	/////////
	//   . //
	//  /| //
	// /_| //
	/////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)if(a[i][j]){
			a[i][j] = min(a[i-1][j]+1, a[i][j-1]+1);
			ans += a[i][j]-1;
		}

	/////////
	// ___ //
	// | / //
	// |/  //
	// `   //
	/////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int i = n; i; i--)
		for(int j = n; j; j--)if(a[i][j]){
			a[i][j] = min(a[i+1][j]+1, a[i][j+1]+1);
			ans += a[i][j]-1;
		}

	/////////
	// ___ //
	// \ | //
	//  \| //
	//   ` //
	/////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int i = n; i; i--)
		for(int j = 1; j <= n; j++)if(a[i][j]){
			a[i][j] = min(a[i+1][j]+1, a[i][j-1]+1);
			ans += a[i][j]-1;
		}

	////////////
	//   /\   //
	//  /  \  //
	// /____\ //
	////////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int i = n; i; i--)
		for(int j = 1; j <= n; j++)if(a[i][j]){
			a[i][j] = min(min(a[i+1][j-1]+1, a[i+1][j]+1), a[i+1][j+1]+1);
			ans += a[i][j]-1;
		}

	////////////
	// ______ //
	// \    / //
	//  \  /  //
	//   \/   //
	////////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)if(a[i][j]){
			a[i][j] = min(min(a[i-1][j-1]+1, a[i-1][j]+1), a[i-1][j+1]+1);
			ans += a[i][j]-1;
		}

	//////////
	// .    //
	// |\   //
	// | \  //
	// |  \ //
	// |  / //
	// | /  //
	// |/   //
	// `    //
	//////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int j = 1; j <= n; j++)
		for(int i = 1; i <= n; i++)if(a[i][j]){
			a[i][j] = min(min(a[i-1][j-1]+1, a[i][j-1]+1), a[i+1][j-1]+1);
			ans += a[i][j]-1;
		}

	//////////
	//    . //
	//   /| //
	//  / | //
	// /  | //
	// \  | //
	//  \ | //
	//   \| //
	//    ` //
	//////////
	memcpy(a, mp[x], sizeof(int)*105*105);
	for(int j = n; j; j--)
		for(int i = 1; i <= n; i++)if(a[i][j]){
			a[i][j] = min(min(a[i-1][j+1]+1, a[i][j+1]+1), a[i+1][j+1]+1);
			ans += a[i][j]-1;
		}

	return ans;
}
int main(int argc, char const *argv[]){
	freopen("triangle.in", "r", stdin);
	freopen("triangle.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++){
		scanf("%s", ch+1);
		for(int j = 1; j <= n; j++){
			vis[ch[j]-'A'] = 1;
			mp[ch[j]-'A'][i][j] = 1;
		}
	}
	int sum = 0;
	for(int i = 0; i < 26; i++)
		if(vis[i]) sum += res[i] = work(i);
	printf("%d\n", sum);
	for(int i = 0; i < 26; i++)
		if(vis[i]) printf("%c  %d\n", i+'A', res[i]);
	return 0;
}
// 王保佑我200+
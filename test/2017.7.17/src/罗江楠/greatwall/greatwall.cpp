#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll h[600010], l[600010];
ll t1[600010], t2[600010], n;
// ll abs(ll x){return x>0?x:-x;}
// ll min(ll a, ll b){return a>b?b:a;}
ll work2(ll *a){
	for(int i = 1, sum = 0; i <= n; i++){
		t1[i] = t1[i-1]+sum;
		sum += a[i];
	}
	for(int i = n, sum = 0; i; i--){
		t2[i] = t2[i+1]+sum;
		sum += a[i];
	}
	ll ans = 1ll<<60;
	for(int i = 1; i <= n; i++)
		ans = min(ans, t1[i]+t2[i]);
	return ans;
}
ll work1(ll *a){
	ll ans = 0;
	for(int i = 1, sum = 0; i <= n; i++){
		sum += a[i];
		ans += abs(sum-i);
	}
	return ans;
}
int main(){
	freopen("greatwall.in", "r", stdin);
	freopen("greatwall.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++){
		int x = read(), y = read();
		h[x]++; l[y]++;
	}
	// GFW ????ん
	ll ans = min(work1(h)+work2(l), work1(l)+work2(h));
	printf("%lld\n", ans);
	return 0;
}
/**
 * This program is a big baoli.
 *
 * @author SW_Wind
 * @time 2017/7/17T10:26
 */
#include <bits/stdc++.h>
#define ll long long
#define N 300020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct node{
	int x, y;
	node(){}
	node(int x, int y):
		x(x), y(y) {}
}ways[N];
int fa[N]; //! int fa♂[N];
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
void merge(int x, int y){x=find(x);y=find(y);if(x==y)return;fa[x]=y;}
int main(){
	freopen("history.in", "r", stdin);
	freopen("history.out", "w", stdout);
	int n = read(), m = read(), c = 0, year = 0;
	bool angry = 0;
	char ch[5];
	while(m--){
		scanf("%s", ch);
		if(ch[0] == 'K') c = read(), angry = 0;
		if(ch[0] == 'R'){
			int x = read(), y = read();
			if(angry) x = (x+c)%n, y = (y+c)%n;
			ways[++year] = node(x, y);
		}
		if(ch[0] == 'T'){
			int x = read(), y = read(), t = read();
			for(int i = 0; i < n; i++) fa[i] = i;
			for(int i = 1; i <= year-t; i++)
				merge(ways[i].x, ways[i].y);
			int tx = find(x), ty = find(y);
			for(int i = year-t+1; i <= year; i++)
				merge(ways[i].x, ways[i].y);
			int nx = find(x), ny = find(y);
			if(nx == ny && tx != ty) puts("Y"), angry = 0;
			else puts("N"), angry = 1;
		}
	}
	return 0;
}
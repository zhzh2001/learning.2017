#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <cmath>
using std::sort;
typedef long long ll;
struct node{
	int x,y;
}P1[600005],P2[600005];
int n;
bool cmp1(node a,node b){
	return a.x<b.x;
}
bool cmp2(node a,node b){
	return a.y<b.y;
}
int main(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&P1[i].x,&P1[i].y);
		P2[i]=P1[i];
	}
	sort(P1+1,P1+n+1,cmp1);
	sort(P2+1,P2+n+1,cmp2);
	ll ans1=0;
	for (int i=1;i<=n;i++)
		ans1+=(ll)abs(i-P1[i].x);
	ll res=P2[(n+1)>>1].y;
	for (int i=1;i<=n;i++)
		ans1+=(ll)abs(res-P1[i].y);
	ll ans2=0;
	for (int i=1;i<=n;i++)
		ans2+=(ll)abs(i-P2[i].y);
	res=P1[(n+1)>>1].x;
	for (int i=1;i<=n;i++)
		ans2+=(ll)abs(res-P2[i].x);
	if (ans1<ans2) printf("%lld\n",ans1);
	else printf("%lld\n",ans2);	
	fclose(stdin); fclose(stdout);
	return 0;
} 

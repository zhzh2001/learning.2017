#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <cmath>
int n;
char ch[105][105];
int dp1[105][105],dp2[105][105];
int dp3[105][105],dp4[105][105];
int dp5[105][105],dp6[105][105];
int dp7[105][105],dp8[105][105];
int L[105][105],R[105][105],res[30];
bool vis[30];
int min(int a,int b){
	if (a<b) return a; else return b;
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++){
		char c=getchar();
		while (c<'A' || c>'Z') c=getchar();
		ch[i][j]=c;
		vis[c-'A']=1;
	}
	for (int i=1;i<=n;i++){
		L[i][1]=1;
		for (int j=2;j<=n;j++)
			if (ch[i][j]==ch[i][j-1]) L[i][j]=L[i][j-1]+1;
			else L[i][j]=1;
	}
	for (int i=1;i<=n;i++){
		R[i][n]=1;
		for (int j=n-1;j>=1;j--)
			if (ch[i][j]==ch[i][j+1]) R[i][j]=R[i][j+1]+1;
			else R[i][j]=1;
	}
	memset(dp1,0,sizeof(dp1));
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		if (ch[i][j]==ch[i-1][j-1])
			dp1[i][j]=min(dp1[i-1][j-1]+1,L[i][j]);
		else
			dp1[i][j]=1;
	/*A
	  AA*/
	memset(dp2,0,sizeof(dp2));
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		if (ch[i][j]==ch[i-1][j+1])
			dp2[i][j]=min(dp2[i-1][j+1]+1,R[i][j]);
		else
			dp2[i][j]=1;
	/* A
	  AA*/
	memset(dp3,0,sizeof(dp3));
	for (int i=n;i>=1;i--)
	for (int j=1;j<=n;j++)
		if (ch[i][j]==ch[i+1][j-1])
			dp3[i][j]=min(dp3[i+1][j-1]+1,L[i][j]);
		else
			dp3[i][j]=1;
	/*AA
	  A*/
	memset(dp4,0,sizeof(dp4));
	for (int i=n;i>=1;i--)
	for (int j=1;j<=n;j++)
		if (ch[i][j]==ch[i+1][j+1])
			dp4[i][j]=min(dp4[i+1][j+1]+1,R[i][j]);
		else
			dp4[i][j]=1;
	/*AA
	   A*/
	memset(dp5,0,sizeof(dp5));
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		if (ch[i][j]==ch[i-1][j-1] && ch[i][j]==ch[i-1][j] && ch[i][j]==ch[i-1][j+1])
			dp5[i][j]=min(dp5[i-1][j-1],min(dp5[i-1][j],dp5[i-1][j+1]))+2;
		else
			dp5[i][j]=1;
	/*AAA
	   A*/
	memset(dp6,0,sizeof(dp6));
	for (int i=n;i>=1;i--)
	for (int j=1;j<=n;j++)
		if (ch[i][j]==ch[i+1][j-1] && ch[i][j]==ch[i+1][j] && ch[i][j]==ch[i+1][j+1])
			dp6[i][j]=min(dp6[i+1][j-1],min(dp6[i+1][j],dp6[i+1][j+1]))+2;
		else
			dp6[i][j]=1;
	/* A
	  AAA*/
	memset(dp7,0,sizeof(dp7));
	for (int j=1;j<=n;j++)
	for (int i=1;i<=n;i++)
		if (ch[i][j]==ch[i-1][j-1] && ch[i][j]==ch[i][j-1] && ch[i][j]==ch[i+1][j-1])
			dp7[i][j]=min(dp7[i-1][j-1],min(dp7[i][j-1],dp7[i+1][j-1]))+2;
		else
			dp7[i][j]=1;
	/*A
	  AA
	  A*/
	memset(dp8,0,sizeof(dp8));
	for (int j=n;j>=1;j--)
	for (int i=1;i<=n;i++)
		if (ch[i][j]==ch[i-1][j+1] && ch[i][j]==ch[i][j+1] && ch[i][j]==ch[i+1][j+1])
			dp8[i][j]=min(dp8[i-1][j+1],min(dp8[i][j+1],dp8[i+1][j+1]))+2;
		else
			dp8[i][j]=1;
	/* A
	  AA
	   A*/
	memset(res,0,sizeof(res));
	int ans=0;
	for (int i=1,X;i<=n;i++)
	for (int j=1;j<=n;j++){
		X=0;
		X+=dp1[i][j]-1;
		X+=dp2[i][j]-1;
		X+=dp3[i][j]-1;
		X+=dp4[i][j]-1;
		X+=(dp5[i][j]-1)>>1;
		X+=(dp6[i][j]-1)>>1;
		X+=(dp7[i][j]-1)>>1;
		X+=(dp8[i][j]-1)>>1;
		res[ch[i][j]-'A']+=X;
		ans+=X;
	}
	printf("%d\n",ans);
	for (int i=0;i<26;i++)
	if (vis[i])
		printf("%c  %d\n",i+'A',res[i]);
	/*for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) printf("%d ",dp6[i][j]);
		puts("");
	}*/
	fclose(stdin); fclose(stdout);
	return 0;
} 

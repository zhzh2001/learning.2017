#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<cctype>
#include<vector>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;
const int maxn=300003;
const int maxm=300003;
int n,m;
struct LOG
{
	int op,x,y;
}book[maxm];
vector<int> com[maxm];
int year=0;
int to[maxm];
bool happy[maxm];
const int K=int('K');
const int R=int('R');
const int T=int('T');
void input()
{
	read(n);read(m);
	for(int i=1;i<=m;++i)
	{
		char r;
		do{r=getchar();}
		while(!isupper(r));
		if(r==K)
		{
			book[i].op=K;
			read(book[i].x);
		}
		else if(r==R)
		{
			book[i].op=R;
			read(book[i].x);
			read(book[i].y);
			++year;
			to[year]=i;
		}
		else if(r==T)
		{
			int t;
			book[i].op=T;
			read(book[i].x);
			read(book[i].y);
			read(t);
			if(year-t<=0)happy[i]=true;
			else com[to[year-t]].push_back(i);
		}
	}
}

int f[maxn];
void Initf()
{
	for(int i=0;i<=n-1;++i)
	{
		f[i]=i;
	}
}
inline int findf(int x)
{
	if(f[x]!=x)
		f[x]=findf(f[x]);
	return f[x];
}
inline void unite(int a,int b)
{
	f[findf(a)]=findf(b);
}
void solve()
{
	bool angry=false;
	int c=0;
	for(int i=1;i<=m;++i)
	{
		if(book[i].op==K)
			c=book[i].x,angry=false;
		else if(book[i].op==R)
		{
			int rx=book[i].x,ry=book[i].y;
			if(angry)
			{
				rx=((rx+c)%n+n)%n;
				ry=((ry+c)%n+n)%n;
			}
			if(findf(rx)!=findf(ry))
				unite(rx,ry);
			if(com[i].size())
			{
				for(int k=0;k<com[i].size();++k)
				{
					int tx=book[com[i][k]].x,
						ty=book[com[i][k]].y;
					happy[com[i][k]]=findf(tx)!=findf(ty);
				}
			}
		}
		else if(book[i].op==T)
		{
			angry=false;
			if(findf(book[i].x)!=findf(book[i].y))
				happy[i]=false;
			if(happy[i])
				puts("Y");
			else puts("N"),angry=true;
		}
	}
}
int main()
{
	
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	input();
	Initf();
	solve();
	return 0;
}

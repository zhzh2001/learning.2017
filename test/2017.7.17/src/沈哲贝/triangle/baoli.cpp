#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define ld long double
#define maxn 110
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll a[maxn][maxn],b[maxn][maxn],c[maxn][maxn],d[maxn][maxn],answ[maxn],f[maxn][maxn],n,ned1,ned2,ned3,ned4,ned5,ned6,ned7,ned8;
char s[maxn];	bool vis[maxn];
void add(ll x,ll y){	answ[x]+=y;	}
int main(){
 	freopen("triangle.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	memset(f,60,sizeof f);
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,n)	f[i][j]=s[j]-'A',vis[f[i][j]]=1;
	}
	For(i,1,n)	For(j,1,n)	For(k,2,n){
		ned1=ned2=ned3=ned4=ned5=ned6=ned7=ned8=0;
		For(x,1,n)	For(y,1,n)
		if (f[x][y]==f[i][j]&&abs(x-i)+abs(y-j)<k){
			ned1+=x>=i&&y>=j;
			ned2+=x>=i&&y<=j;
			ned3+=x<=i&&y>=j;
			ned4+=x<=i&&y<=j;
			ned5+=x>=i;
			ned6+=y>=j;
			ned7+=x<=i;
			ned8+=y<=j;
		}
		answ[f[i][j]]+=(ned1==k*(k+1)/2)+(ned2==k*(k+1)/2)+(ned3==k*(k+1)/2)+(ned4==k*(k+1)/2)+(ned5==k*k)+(ned6==k*k)+(ned7==k*k)+(ned8==k*k);	
	}
	For(i,0,25)	if (vis[i])	printf("%c  %d\n",i+'A',answ[i]);
}
/*
3
BBB
BAA
AAA

3
BCD
EAF
AAA
*/

#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define ld long double
#define maxn 110
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll a[maxn][maxn],b[maxn][maxn],c[maxn][maxn],d[maxn][maxn],answ[maxn],f[maxn][maxn],n,sum;
char s[maxn];	bool vis[maxn];
void add(ll x,ll y){	answ[x]+=y;	}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();
	memset(f,60,sizeof f);
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,n)	f[i][j]=s[j]-'A',vis[f[i][j]]=1;
	}
	For(i,1,n)	For(j,1,n)
	if (f[i-1][j]==f[i][j-1]&&f[i][j]==f[i-1][j])	a[i][j]=min(a[i-1][j],a[i][j-1])+1;
	For(i,1,n)	FOr(j,n,1)
	if (f[i-1][j]==f[i][j+1]&&f[i][j]==f[i-1][j])	b[i][j]=min(b[i-1][j],b[i][j+1])+1;
	FOr(i,n,1)	FOr(j,n,1)
	if (f[i+1][j]==f[i][j+1]&&f[i][j]==f[i+1][j])	c[i][j]=min(c[i+1][j],c[i][j+1])+1;
	FOr(i,n,1)	For(j,1,n)
	if (f[i+1][j]==f[i][j-1]&&f[i][j]==f[i+1][j])	d[i][j]=min(d[i+1][j],d[i][j-1])+1;
	For(i,1,n)	For(j,1,n)	add(f[i][j],a[i][j]+b[i][j]+c[i][j]+d[i][j]+min(a[i][j],b[i][j])+min(b[i][j],c[i][j])+min(c[i][j],d[i][j])+min(d[i][j],a[i][j]));
	For(i,0,25)	if (vis[i])	sum+=answ[i]; printf("%d\n",sum);
	For(i,0,25)	if (vis[i])	printf("%c  %d\n",i+'A',answ[i]);
}
/*
3
AAB
ABB
BBC
*/

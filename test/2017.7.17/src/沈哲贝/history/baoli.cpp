#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<map>
#define ll long long
#define maxn 2001000
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll fa[maxn],x[maxn],y[maxn],opt[maxn],tim,mark,now,n,Q;	char s[10];
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);}
int main(){
	freopen("history.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	Q=read();
	For(i,1,Q){
		scanf("%s",s);
		if (s[0]=='K')	now=read(),mark=0;
		if (s[0]=='R'){
			x[i]=(read())%n;	y[i]=(read())%n;
			x[i]=(x[i]+mark)%n;	y[i]=(y[i]+mark)%n;
			++tim;	opt[i]=1;
		}
		if (s[0]=='T'){
			ll st=read(),ed=read(),T=max(0LL,tim-read());
			For(i,0,n-1)	fa[i]=i;	bool flag=1;
			ll tt=0;
			For(i,1,Q)
			if(opt[i]==1){
				++tt;	if (tt>T)	break;
				fa[find(x[i])]=find(y[i]);
			}
			flag&=find(st)!=find(ed);
			For(i,0,n-1)	fa[i]=i;
			For(i,1,Q)	if (opt[i]==1)	fa[find(x[i])]=find(y[i]);
			flag&=find(st)==find(ed);
			if (flag)	puts("Y"),mark=0;	else	mark=now,puts("N");
		}
	}
}/*
3 7
R 0 1
T 0 1 1
K 1
R 0 1
T 0 1 1
R 0 1
T 0 2 1
*/

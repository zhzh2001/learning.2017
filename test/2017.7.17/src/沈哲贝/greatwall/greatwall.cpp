#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 2000000
#define ld long double
#define eps 1e-12
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll x[maxn],y[maxn],n,ans1,ans2;
int main(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=read();
	For(i,1,n)	x[i]=read(),y[i]=read();
	sort(x+1,x+n+1);	sort(y+1,y+n+1);
	For(i,1,n)	ans1+=abs(x[i]-x[(n+1)>>1]),ans2+=abs(i-x[i]);
	For(i,1,n)	ans2+=abs(y[i]-y[(n+1)>>1]),ans1+=abs(i-y[i]);
	writeln(min(ans1,ans2));
}

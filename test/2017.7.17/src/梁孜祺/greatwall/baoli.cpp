#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#define ll long long
#define inf 1e15
#define mp make_pair
#define N 300005
#define pa pair<ll,int>
#define P(x,y) (x-1)*m+y
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0;int f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n;ll ans=inf;
struct data{int x,y;}a[5005],b[5005];
inline bool cmp1(data a,data b){return (a.y==b.y)?a.x<b.x:a.y<b.y;}
inline bool cmp2(data a,data b){return (a.x==b.x)?a.y<b.y:a.x<b.x;}
inline void solve1(int x){
	sort(b+1,b+1+n,cmp1);
	ll mx=0;
	For(i,1,n) mx+=abs(x-b[i].x)+abs(i-b[i].y);
	ans=min(ans,mx);
}
inline void solve2(int y){
	sort(b+1,b+1+n,cmp2);
	ll mx=0;
	For(i,1,n) mx+=abs(y-b[i].y)+abs(i-b[i].x);
	ans=min(ans,mx);
}
int main(){
	freopen("greatwall.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();For(i,1,n) a[i]=(data){read(),read()},b[i]=a[i];
	For(i,1,n){
		solve1(i);
		solve2(i);
	}
	cout<<ans;
	return 0;
}
#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,cnt[30],vis[30],ans;
char s[105][105];
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%s",s[i]+1);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++){
			vis[s[i][j]-'A']=1;
			int a=0,b=0,c=0,d=0;
			while (s[i][j+a+1]==s[i][j]) a++;//向右
			while (s[i][j-b-1]==s[i][j]) b++;//向左
			while (s[i+c+1][j]==s[i][j]) c++;//向下
			while (s[i-d-1][j]==s[i][j]) d++;//向上
			int sum=min(a,c)+min(a,d)+min(b,c)+min(b,d)+min(min(a,b),c)+min(min(a,b),d)+min(min(a,c),d)+min(min(b,c),d);
			cnt[s[i][j]-'A']+=sum; ans+=sum;
		}
	printf("%d\n",ans);
	for (int i=0;i<26;i++)
		if (vis[i]) printf("%c  %d\n",'A'+i,cnt[i]);
}

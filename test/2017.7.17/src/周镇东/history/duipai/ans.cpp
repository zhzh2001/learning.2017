#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cstdlib>
using namespace std;
const int N=3e3+5;
int n,m;
int t[N],a[N],b[N],c[N],y[N],askt[N],askn[N];
int fa[N][N];
char ch[2];
bool cmp(int a,int b){
	return (y[a]-c[a])<(y[b]-c[b]);
}
int getf(int i,int k){
	return fa[i][k]==k?k:(fa[i][k]=getf(i,fa[i][k]));
}
bool getans(int st,int en,int t){
	if (t<=0)
		return st==en;
	return getf(t,st)==getf(t,en);
}
int main(){
	freopen("history.in","r",stdin);
	freopen("ans.out","w",stdout);
	scanf("%d%d",&n,&m);
	y[0]=0;
	for (int i=1;i<=m;i++){
		scanf("%s",ch);
		if (ch[0]=='K'){
			t[i]=0;
			scanf("%d",&a[i]);
			y[i]=y[i-1];
		}
		if (ch[0]=='R'){
			t[i]=1;
			scanf("%d%d",&a[i],&b[i]);
			y[i]=y[i-1]+1;
		}
		if (ch[0]=='T'){
			t[i]=2;
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			y[i]=y[i-1];
		}
	}
	memset(askt,0,sizeof askt);
	for (int i=0;i<=y[m];i++)
		for (int j=0;j<n;j++)
			fa[i][j]=j;
	int cpking=0,cnow=0;//poj СССС�� "cpking" haha 
	for (int i=1;i<=m;i++){
		if (t[i]==0){
			cpking=a[i];
			cnow=0;
		}
		if (t[i]==1){
			a[i]=(a[i]+cnow)%n;
			b[i]=(b[i]+cnow)%n;
			for (int j=0;j<n;j++)
				fa[y[i]][j]=fa[y[i]-1][j];
			fa[y[i]][getf(y[i],a[i])]=getf(y[i],b[i]);
			cnow=0;
		}
		if (t[i]==2){
			askn[i]=(getf(y[i],a[i])==getf(y[i],b[i]))?1:0;
			askt[i]=getans(a[i],b[i],y[i]-c[i]);
			if (askn[i]&&!askt[i])
				puts("Y");
			else {
				puts("N");
				if (cnow==0)
					cnow=cpking;
			}
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

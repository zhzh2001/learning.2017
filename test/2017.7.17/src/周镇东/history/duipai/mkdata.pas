var
	i,a,b,c,d,n,m:longint;
begin
	assign(output,'history.in');
	rewrite(output);
	randomize;
	n:=random(2900)+20;
	m:=random(2900)+20;
	writeln(n,' ',m);
	for i:=1 to m do
	begin
		a:=random(3)+1;
		if (a=1) then
		begin
			a:=random(n);
			writeln('K ',a);
		end;
		if (a=2) then
		begin
			repeat
				a:=random(n);
				b:=random(n);
			until a<>b;
			writeln('R ',a,' ',b);
		end;
		if (a=3) then
		begin
			a:=random(n);
			b:=random(n);
			c:=random(m div 2);
			writeln('T ',a,' ',b,' ',c);
		end;
	end;
	close(output);
end.

#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cstdlib>
using namespace std;
const int N=3e5+5;
int n,m;
int t[N],a[N],b[N],c[N],y[N],askt[N],askn[N];
int fa[N];
int bh[N],hs;
char ch[2];
bool cmp(int a,int b){
	return (y[a]-c[a])<(y[b]-c[b]);
}
int getf(int k){
	return fa[k]==k?k:(fa[k]=getf(fa[k]));
}
int main(){
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	scanf("%d%d",&n,&m);
	hs=0,y[0]=0;
	for (int i=1;i<=m;i++){
		scanf("%s",ch);
		if (ch[0]=='K'){
			t[i]=0;
			scanf("%d",&a[i]);
			y[i]=y[i-1];
		}
		if (ch[0]=='R'){
			t[i]=1;
			scanf("%d%d",&a[i],&b[i]);
			y[i]=y[i-1]+1;
		}
		if (ch[0]=='T'){
			t[i]=2;
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			y[i]=y[i-1];
			bh[++hs]=i;
		}
	}
	memset(askt,0,sizeof askt);
	sort(bh+1,bh+hs+1,cmp);
	int hpos=1;
	while (hpos<=hs&&y[bh[hpos]]-c[bh[hpos]]<=0){
		askt[bh[hpos]]=(a[bh[hpos]]==b[bh[hpos]])?1:0;
		hpos++;
	}
	for (int i=0;i<n;i++)
		fa[i]=i;
	int cpking=0,cnow=0;//poj С�� "cpking" haha 
	for (int i=1;i<=m;i++){
		if (t[i]==0){
			cpking=a[i];
			cnow=0;
		}
		if (t[i]==1){
			a[i]=(a[i]+cnow)%n;
			b[i]=(b[i]+cnow)%n;
			fa[getf(a[i])]=getf(b[i]);
			cnow=0;
			while (hpos<=hs&&y[bh[hpos]]-c[bh[hpos]]==y[i]){
				askt[bh[hpos]]=(getf(a[bh[hpos]])==getf(b[bh[hpos]]))?1:0;
				hpos++;
			}
		}
		if (t[i]==2){
			askn[i]=(getf(a[i])==getf(b[i]))?1:0;
			if (askn[i]&&!askt[i])
				puts("Y");
			else {
				puts("N");
				if (cnow==0)
					cnow=cpking;
			}
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
using namespace std;
const int N=105;
char str[N];
int cnt[N],v[N][N];
int n;
bool appear[N];
int main(){
	freopen("triangle.in","r",stdin);
	freopen("ans.out","w",stdout);
	memset(appear,0,sizeof appear);
	memset(cnt,0,sizeof cnt);
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%s",str);
		for (int j=0;j<n;j++)
			v[i][j]=str[j]-'A',appear[v[i][j]]=1;
	}
	int ans=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++){
			int k,k1,k2,k3,k4;
			int tot=0;
			for (k=1;;k++){
				if (i+k>=n||j+k>=n){
					k--;
					break;
				}
				bool flag=0;
				for (int s=0;s<=k;s++)
					if (v[i][j]!=v[i+k-s][j+s]){
						flag=1;
						break;
					}
				if (flag){
					k--;
					break;
				}
			}
			k1=k;
			tot+=k;
//			printf("%d ",tot);
			for (k=1;;k++){
				if (i+k>=n||j-k<0){
					k--;
					break;
				}
				bool flag=0;
				for (int s=0;s<=k;s++)
					if (v[i][j]!=v[i+k-s][j-s]){
						flag=1;
						break;
					}
				if (flag){
					k--;
					break;
				}
			}
			k2=k;
			tot+=k;
//			printf("%d ",tot);
			for (k=1;;k++){
				if (i-k<0||j-k<0){
					k--;
					break;
				}
				bool flag=0;
				for (int s=0;s<=k;s++)
					if (v[i][j]!=v[i-k+s][j-s]){
						flag=1;
						break;
					}
				if (flag){
					k--;
					break;
				}
			}
			k3=k;
			tot+=k;
//			printf("%d ",tot);
			for (k=1;;k++){
				if (i-k<0||j+k>=n){
					k--;
					break;
				}
				bool flag=0;
				for (int s=0;s<=k;s++)
					if (v[i][j]!=v[i-k+s][j+s]){
						flag=1;
						break;
					}
				if (flag){
					k--;
					break;
				}
			}
			k4=k;
			tot+=k;
//			printf("%d ",tot);
			tot+=min(k1,k2)+min(k2,k3)+min(k3,k4)+min(k4,k1);
			ans+=tot;
			cnt[v[i][j]]+=tot;
//			printf("%d %d %d %d ",k1,k2,k3,k4);
//			printf("%d %d %d\n",i,j,tot);
		}
	printf("%d\n",ans);
	for (int i=0;i<26;i++)
		if (appear[i])
			printf("%c  %d\n",i+'A',cnt[i]);
	return 0;
}

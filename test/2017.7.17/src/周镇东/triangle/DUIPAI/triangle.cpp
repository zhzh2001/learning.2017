#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cmath>
using namespace std;
const int N=105;
char str[N];
int v[N][N];
//int up[N][N],dn[N][N],le[N][N],ri[N][N];
//   AA           AA          A          A
//   A             A          AA        AA
int dpul[N][N],dpur[N][N],dpdl[N][N],dpdr[N][N];
int cnt[N];
int n;
bool appear[N];
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	memset(appear,0,sizeof appear);
	memset(cnt,0,sizeof cnt);
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%s",str);
		for (int j=0;j<n;j++)
			v[i][j]=str[j]-'A',appear[v[i][j]]=1;
	}
//	for (int i=0;i<n;i++)
//		for (int j=0;j<n;j++){
//			for (up[i][j]=i;up[i][j]>0  &&v[up[i][j]][j]==v[i][j];up[i][j]--);
//			for (dn[i][j]=i;dn[i][j]<n-1&&v[dn[i][j]][j]==v[i][j];dn[i][j]++);
//			for (le[i][j]=j;le[i][j]>0  &&v[i][le[i][j]]==v[i][j];le[i][j]--);
//			for (ri[i][j]=j;ri[i][j]<n-1&&v[i][ri[i][j]]==v[i][j];ri[i][j]++);
//		}
	memset(cnt,0,sizeof cnt);
	for (int i=n-1;i>=0;i--)
		for (int j=n-1;j>=0;j--){
			if (i==n-1||j==n-1)
				dpul[i][j]=1;
			else if (v[i+1][j]!=v[i][j]||v[i][j+1]!=v[i][j])
				dpul[i][j]=1;
			else 
				dpul[i][j]=min(dpul[i+1][j],dpul[i][j+1])+1;
			cnt[v[i][j]]+=dpul[i][j]-1;
		}
	for (int i=n-1;i>=0;i--)
		for (int j=0;j<n;j++){
			if (i==n-1||j==0)
				dpur[i][j]=1;
			else if (v[i+1][j]!=v[i][j]||v[i][j-1]!=v[i][j])
				dpur[i][j]=1;
			else
				dpur[i][j]=min(dpur[i+1][j],dpur[i][j-1])+1;
			cnt[v[i][j]]+=dpur[i][j]-1;
		}
	for (int i=0;i<n;i++)
		for (int j=n-1;j>=0;j--){
			if (i==0||j==n-1)
				dpdl[i][j]=1;
			else if (v[i-1][j]!=v[i][j]||v[i][j+1]!=v[i][j])
				dpdl[i][j]=1;
			else 
				dpdl[i][j]=min(dpdl[i-1][j],dpdl[i][j+1])+1;
			cnt[v[i][j]]+=dpdl[i][j]-1;
		}
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++){
			if (i==0||j==0)
				dpdr[i][j]=1;
			else if (v[i-1][j]!=v[i][j]||v[i][j-1]!=v[i][j])
				dpdr[i][j]=1;
			else
				dpdr[i][j]=min(dpdr[i-1][j],dpdr[i][j-1])+1;
			cnt[v[i][j]]+=dpdr[i][j]-1;
		}
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++){
			cnt[v[i][j]]+=min(dpul[i][j],dpur[i][j])-1;
			cnt[v[i][j]]+=min(dpdl[i][j],dpdr[i][j])-1;
			cnt[v[i][j]]+=min(dpul[i][j],dpdl[i][j])-1;
			cnt[v[i][j]]+=min(dpur[i][j],dpdr[i][j])-1;
		}
	int tot=0;
	for (int i=0;i<26;i++)
		if (appear[i])
			tot+=cnt[i];
	printf("%d\n",tot);
	for (int i=0;i<26;i++)
		if (appear[i])
			printf("%c  %d\n",i+'A',cnt[i]);
	fclose(stdin);fclose(stdout);
	return 0;
}

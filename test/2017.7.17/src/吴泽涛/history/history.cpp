#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <cmath>
const int maxn=300005;
int tot=0,n,m,st[maxn],en[maxn],t[maxn],que[maxn],f[maxn];
int x[maxn],y[maxn],C[maxn],head[maxn*2];
char c,ch[maxn];
struct node{
	int st,en,next;
	bool ans;
}edge[maxn];
int getfather(int x){
	if (f[x]==x) return x; else f[x]=getfather(f[x]);
	return f[x];
}
void makedge(int u,int st,int en){
	edge[++tot].st=st;
	edge[tot].en=en;
	edge[tot].next=head[u];
	edge[tot].ans=0;
	head[u]=tot;
}
int main(){
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	scanf("%d%d",&n,&m);
	int year=299999;
	for (int i=1;i<=m;i++){
		do{
			c=getchar();
		}while (c!='T' && c!='R' && c!='K');
		if (c=='T'){
			scanf("%d%d%d",&st[i],&en[i],&t[i]);
			makedge(year-t[i],st[i],en[i]);
			que[i]=tot;
		}
		if (c=='R') scanf("%d%d",&x[i],&y[i]),year++;
		if (c=='K') scanf("%d",&C[i]);
		ch[i]=c;
	}
	for (int i=0;i<n;i++) f[i]=i;
	year=299999;
	int change_val=0,year_st,year_en;
	bool angry=false;
	for (int i=1;i<=m;i=year_en+1){
		year_st=i,year_en=i+1;
		while (ch[year_en]!='R' && year_en<=m) year_en++;
		year_en--;
		if (ch[year_st]=='R') year++;
		for (int j=year_st;j<=year_en;j++){
			if (ch[j]=='K') change_val=C[j],angry=false;
			if (ch[j]=='R'){
				if (angry) x[j]=(x[j]+change_val%n+n)%n,y[j]=(y[j]+change_val%n+n)%n;
				if (getfather(x[j])!=getfather(y[j]))
					f[getfather(x[j])]=getfather(y[j]);
			}
			if (ch[j]=='T')
				if (t[j]==0) angry=true,puts("N");
				else{
					if (getfather(st[j])==getfather(en[j]) && !edge[que[j]].ans)
						angry=false,puts("Y");
					else
						angry=true,puts("N");
				}
		}
		for (int j=head[year];j;j=edge[j].next)
			edge[j].ans=(getfather(edge[j].st)==getfather(edge[j].en));
	}
	fclose(stdin); fclose(stdout);
	return 0;
} 

#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <cmath>
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++){
		char c=getchar();
		while (c<'A' || c>'Z') c=getchar();
		ch[i][j]=c;
	}
	for (int i=1;i<=n;i++){
		L[i][1]=1;
		for (int j=2;j<=n;j++)
			if (ch[i][j]==ch[i][j-1]) L[i][j]=L[i][j-1]+1;
			else L[j]=1;
	}
	for (int i=1;i<=n;i++){
		R[i][n]=1;
		for (int j=n-1;j>=1;j--)
			if (ch[i][j]==ch[i][j+1]) R[i][j]=R[i][j+1]+1;
			else R[i][j]=1;
	}
	for (int j=1;j<=n;j++){
		T[1][j]=1;
		for (int i=2;i<=n;i++)
			if (ch[i][j]==ch[i-1][j]) T[i][j]=T[i-1][j]+1;
			else T[i][j]=1;
	}
	for (int j=1;j<=n;j++){
		U[n][j]=1;
		for (int i=n-1;i>=1;i--)
			if (ch[i][j]==ch[i+1][j]) U[i][j]=U[i+1][j]+1;
			else U[i][j]=1;
	}
	memset(dp1,0,sizeof(dp1));
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		dp1[i][j]=min(dp1[i-1][j-1]+1,L[i][j]);
	memset(dp2,0,sizeof(dp2));
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		dp2[i][j]=min(dp2[i-1][j+1]+1,R[i][j]);
	memset(dp3,0,sizeof(dp3));
	for (int i=n;i>=1;i--)
	for (int j=1;j<=n;j++)
		dp3[i][j]=min(dp3[i+1][j-1]+1,L[i][j]);
	memset(dp4,0,sizeof(dp4));
	for (int i=n;i>=1;i--)
	for (int j=1;j<=n;j++)
		dp4[i][j]=min(dp4[i+1][j+1]+1,R[i][j]);
	
	fclose(stdin); fclose(stdout);
	return 0;
} 

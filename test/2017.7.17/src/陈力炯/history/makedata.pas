var
    n,m,i,ch,x,y,st,en,t,c:longint;
begin
    assign(output,'history.in'); rewrite(output);
    randomize;
    n:=random(10)+3;
    m:=random(10)+3;
    writeln(n,' ',m);
    for i:=1 to m do
    begin
        ch:=random(3);
        if ch=0 then
        begin
            write('R ');
	    repeat
                x:=random(n); y:=random(n);
            until x<>y;
            writeln(x,' ',y);
        end;
        if ch=1 then
        begin
            write('T ');
            repeat
              st:=random(n); en:=random(n);
              t:=random(i+2);
            until st<>en;
            writeln(st,' ',en,' ',t);
        end;
        if ch=2 then
        begin
            write('K ');
            c:=random(n);
            writeln(c);
        end;
    end;
    close(output);
end.
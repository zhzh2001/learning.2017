#include<fstream>
#include<algorithm>
#include<cstdio>
#include<cmath>
using namespace std;
ifstream fin("history.in");
ofstream fout("history.out");
int n,m;
struct edge{
	int x,y;
}xx[300003];
struct other{
	int x,y,tim,pos;
	bool ans;
}que[3000003];
inline bool cmp1(const other a,const other b){
	return a.tim<b.tim;
}
inline bool cmp2(const other a,const other b){
	if(a.pos==b.pos){
		a.tim<b.tim;
	}
	return a.pos<b.pos;
}
int fa[100003];
int tot=0,tot2=0;
inline int findd(int x){
	if(fa[x]!=x){
		return findd(fa[x]);
	}
	return x;
}
inline void uni(int x,int y){
	int xx=findd(x),yy=findd(y);
	if(xx!=yy){
		fa[xx]=yy;
	}
}
inline bool ok(int x,int y){
	return findd(x)==findd(y);
}
inline void solve1(){
	int now=0;
	bool yes=false;
	for(int i=1;i<=m;i++){
		char opt;
		int x,y,k;
		fin>>opt;
		if(opt=='K'){
			yes=false;
			fin>>now;
		}else if(opt=='T'){
			yes=false;
			fin>>x>>y>>k;
			for(int i=0;i<=n;i++){
				fa[i]=i;
			}
			for(int i=1;i<=tot;i++){
				uni(xx[i].x,xx[i].y);
			}
			int p1=findd(x),q1=findd(y);
			if(p1!=q1){
				yes=true;
				fout<<"N"<<endl;
				continue;
			}
			for(int i=0;i<=n;i++){
				fa[i]=i;
			}
			for(int i=1;i<=tot-k;i++){
				uni(xx[i].x,xx[i].y);
			}
			int p2=findd(x),q2=findd(y);
			if(p2==q2){
				yes=true;
				fout<<"N"<<endl;
				continue;
			}
			fout<<"Y"<<endl;
		}else{
			fin>>x>>y;
			if(yes==true){
				x=(x+n-now)%n;
				y=(y+n-now)%n;
			}
			xx[++tot].x=x;  xx[tot].y=y;
		}
	}
}
int main(){
	fin>>n>>m;
	if(n*m<=20000000){
		solve1();
		return 0;
	}
	for(int i=0;i<=n;i++){
		fa[i]=i;
	}
	int tim;
	for(int i=1;i<=m;i++){
		char opt;
		int x;
		fin>>opt;
		if(opt=='K'){
			fin>>x;
			if(x!=0){
				fout<<"gg"<<endl;
				return 0;
			}
		}else if(opt=='T'){
			int k,x,y;
			fin>>x>>y>>k;
			que[++tot].x=x,que[tot].y=y,que[tot].tim=tim;
			que[tot].pos=i;
			que[++tot].x=x,que[tot].y=y,que[tot].tim=tim-k;
			que[tot].pos=i;
		}else{
			tim++;
			fin>>xx[++tot2].x>>xx[tot2].y;
		}
	}
	sort(que+1,que+tot+1,cmp1);
	int now=1;
	for(int i=1;i<=tot2;i++){
		uni(xx[i].x,xx[i].y);
		while(que[now].tim<=0&&now<=tot){
			now++;
		}
		while(que[now].tim<=i+1&&now<=tot||que[now].tim==0){
			que[now].ans=ok(que[now].x,que[now].y);
			now++;
		}
	}
	sort(que+1,que+tot+1,cmp2);
	int i=1;
	while(i<=tot){
		if(que[i].tim<=0){
			fout<<"N"<<endl;
			continue;
		}
		if(que[i].ans==1&&que[i+1].ans==0){
			fout<<"Y"<<endl;
		}else{
			fout<<"N"<<endl;
		}
		i+=2;
	}
	return 0;
}
/*

in:
3 7
R 0 1
T 0 1 1
K 1
R 0 1
T 0 1 1
R 0 1
T 0 2 1

out:
Y
N
Y

*/

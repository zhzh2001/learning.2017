#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
using namespace std;
int n,m,sz,last;
int root[300003],ls[500003],rs[500003];
int v[50003],deep[5003];
void build(int &k,int l,int r){
	if(!k){
		k=++sz;
	}
	if(l==r){
		v[k]=l;
		return;
	}
	int mid=(l+r)>>1;
	build(ls[k],l,mid);
	build(rs[k],mid+1,r);
}
void modify(int l,int r,int x,int &y,int pos,int val){
	y=++sz;
	if(l==r){
		v[y]=val;  deep[y]=deep[x];  return;
	}
	ls[y]=ls[x];
	rs[y]=rs[x];
	int mid=(l+r)>>1;
	if(pos<=mid){
		modify(l,mid,ls[x],ls[y],pos,val);
	}else{
		modify(mid+1,r,rs[x],rs[y],pos,val);
	}
}
int query(int k,int l,int r,int pos){
	if(l==r){
		return k;
	}
	int mid=(l+r)>>1;
	if(pos<=mid){
		return query(ls[k],l,mid,pos);
	}else{
		return query(rs[k],mid+1,r,pos);
	}
}
void add(int k,int l,int r,int pos){
	if(l==r){
		deep[k]++;
		return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid){
		add(ls[k],l,mid,pos);
	}else{
		add(rs[k],mid+1,r,pos);
	}
}
int find(int k,int x){
    int p=query(k,1,n,x);
	if(x==v[p]){
		return p;
	}
    return find(k,v[p]);
}
struct edge{
	int x,y;
}xx[300003];
int fa[100003];
inline int findd(int x){
	if(fa[x]!=x){
		return findd(fa[x]);
	}
	return x;
}
inline void uni(int x,int y){
	int xx=findd(x),yy=findd(y);
	if(xx!=yy){
		fa[xx]=yy;
	}
}

inline void solve1(){
	int now=0,tot=0;
	bool yes=false;
	for(int i=1;i<=m;i++){
		char opt;
		int x,y,k;
		cin>>opt;
		if(opt=='K'){
			yes=false;
			cin>>now;
		}else if(opt=='T'){
			yes=false;
			cin>>x>>y>>k;
			for(int i=0;i<=n;i++){
				fa[i]=i;
			}
			for(int i=1;i<=tot;i++){
				uni(xx[i].x,xx[i].y);
			}
			int p1=findd(x),q1=findd(y);
			if(p1!=q1){
				yes=true;
				cout<<"N"<<endl;
				continue;
			}
			for(int i=0;i<=n;i++){
				fa[i]=i;
			}
			for(int i=1;i<=tot-k;i++){
				uni(xx[i].x,xx[i].y);
			}
			int p2=findd(x),q2=findd(y);
			if(p2==q2){
				yes=true;
				cout<<"N"<<endl;
				continue;
			}
			cout<<"Y"<<endl;
		}else{
			cin>>x>>y;
			if(yes==true){
				x=(x+n-now)%n;
				y=(y+n-now)%n;
			}
			xx[++tot].x=x;  xx[tot].y=y;
		}
	}
}
int main(){
	cin>>n>>m;
	if(m<=3000){
		solve1();
		return 0;
	}
	
	build(root[0],1,n);
	int f,k,a,b;
	int now=0;
	bool yes=false;
	int tim=0;
	for(int i=1;i<=m;i++){
		char opt;
		int x,y,t,vv;
		cin>>opt;
		if(opt=='K'){
			cin>>vv;
		}else if(opt=='T'){
			cin>>x>>y>>t;
			int p=find(root[tim],x+1),q=find(root[tim],y+1);
			int p2=find(root[tim-t],x+1),q2=find(root[tim-t],y+1);
			if(p==q&&p2!=q2){
				yes=false;
				cout<<"Y"<<endl;
			}else{
				yes=true;
				cout<<"N"<<endl;
			}
		}else{
			tim++;
			cin>>x>>y;
			root[tim]=root[tim-1];
			if(yes==true){
				x=(x+n+1-vv+1)%(n);
				y=(y+n+1-vv+1)%(n);
			}
			x++,y++;
			int p=find(root[tim],x),q=find(root[tim],y);
			if(v[p]==v[q]){
				continue;
			}
			if(deep[p]>deep[q]){
				swap(p,q);
			}
			modify(1,n,root[tim-1],root[tim],v[p],v[q]);
			if(deep[p]==deep[q]){
				add(root[tim],1,n,v[q]);
			}
		}
	}
	return 0;
}
/*

in:
5 6
1 1 2
3 1 2
2 1
3 0 3
2 1
3 1 2

out:
1
0
1

*/
/*

in:
3 7
R 0 1
T 0 1 1
K 1
R 0 1
T 0 1 1
R 0 1
T 0 2 1

out:
Y
N
Y

*/

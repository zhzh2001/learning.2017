#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("greatwall.in");
ofstream fout("greatwall.out");
int n;
struct xx{
	int x,y;
}node[1000003];
int tongg[1000003];
inline bool cmp(const xx a,const xx b){
	if(a.x==b.x){
		return a.y<b.y;
	}
	return a.x<b.x;
}
inline bool cmp2(const xx a,const xx b){
	if(a.y==b.y){
		return a.x<b.x;
	}
	return a.y<b.y;
}
long long ans=9999999999999;
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>node[i].x>>node[i].y;
	}
	sort(node+1,node+n+1,cmp);
	long long ans1=0;
	long long nowans=0,anss1=19999999999999;
	for(int i=1;i<=n;i++){
		ans1+=abs(node[i].x-i);
		tongg[node[i].x]++;
		nowans+=node[i].x;
	}
	long long zongl=0,zongr=n;
	for(int i=1;i<=n;i++){
		nowans=nowans-zongr;
		nowans=nowans+zongl;
		zongl+=tongg[i];
		zongr-=tongg[i];
		anss1=min(anss1,nowans);
	}
	
	sort(node+1,node+n+1,cmp2);
	memset(tongg,0,sizeof(tongg));
	long long ans2=0,anss2=19999999999999;
	nowans=0;
	for(int i=1;i<=n;i++){
		ans2+=abs(node[i].y-i);
		tongg[node[i].y]++;
		nowans+=node[i].y;
	}
	zongl=0,zongr=n;
	for(int i=1;i<=n;i++){
		nowans=nowans-zongr;
		nowans=nowans+zongl;
		zongl+=tongg[i];
		zongr-=tongg[i];
		anss2=min(anss2,nowans);
	}
	ans=min(ans2+anss1,ans1+anss2);
	//cout<<ans1<<" "<<ans2<<endl;
	//cout<<anss1<<" "<<anss2<<endl;
	fout<<ans<<endl;
	return 0;
}
/*

in:
5
1 2
2 4
3 4
5 1
5 3

out:
6

*/

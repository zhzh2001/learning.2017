#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<vector>

using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef unsigned int uint;
typedef long long LL;
const uint uinf=4294967295u;
const int maxn=600003;
int n;
struct point
{
	int x,y;
}p[maxn];
bool cmpx(const point& a,const point& b)
{
	return a.x<b.x;
}
bool cmpy(const point& a,const point& b)
{
	return a.y<b.y;
}
void input()
{
	read(n);
	for(int i=1;i<=n;++i)
	{
		read(p[i].x);
		read(p[i].y);
	}
}
LL ans,tans;
LL sum,tsum;
int stk[maxn],sk;
int cnt[maxn];
int to[maxn];
void countrow(int r)
{
	tsum=0;
	for(int i=1;i<=n;++i)
	{
		tsum+=abs(p[i].x-r);
		if(sum!=-1&&tsum>sum)return;
	}
}
void countcol(int c)
{
	tsum=0;
	for(int i=1;i<=n;++i)
	{
		tsum+=abs(p[i].y-c);
		if(sum!=-1&&tsum>sum)return;
	}
}
void solve()
{	
	sort(p+1,p+n+1,cmpy);
	sk=tans=0;
	sum=-1;
	memset(to+1,0,n*sizeof(int));
	for(int i=1;i<=n;++i)
	{		
		if(!to[p[i].y])
		{
			to[p[i].y]=++sk;
			stk[sk]=p[i].y;
			cnt[to[p[i].y]]=0;
		}
		++cnt[to[p[i].y]];
	}	
	for(int i=n;i>=1;--i)
	{
		if(!to[i])
		{
			tans+=abs(stk[sk]-i);
			--cnt[sk];
			if(cnt[sk]==1&&sk>=1)
				--sk;
		}
	}
	for(int i=1;i<=n;++i)
	{
		countrow(i);
		if(sum==-1||tsum<sum)
			sum=tsum;
	}
	ans=sum+tans;
	
	sort(p+1,p+n+1,cmpx);
	sk=tans=0;
	sum=-1;
	memset(to+1,0,n*sizeof(int));
	for(int i=1;i<=n;++i)
	{
		if(!to[p[i].x])
		{
			to[p[i].x]=++sk;
			stk[sk]=p[i].x;
			cnt[to[p[i].x]]=0;
		}
		++cnt[to[p[i].x]];
	}
	for(int i=n;i>=1;--i)
	{
		if(!to[i])
		{
			tans+=abs(stk[sk]-i);
			--cnt[sk];
			if(cnt[sk]==1&&sk>=1)
				--sk;
		}
	}
	for(int i=1;i<=n;++i)
	{
		countcol(i);
		if(sum==-1||tsum<sum)
			sum=tsum;
		
	}
	if(sum+tans<ans)ans=sum+tans;
}
int main()
{
	
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	input();
	solve();
	printf("%lld\n",ans);
	return 0;
}

var
  n,i,j,k:longint;
  ans:int64;
  ch:char;
  a:array['A'..'Z']of longint;
  num:array[0..105,0..105]of longint;
  s,s1:array[0..105]of string;

procedure change;
var i,j:longint;
begin
  for i:=1 to n do
    for j:=1 to n do
      s1[j,n-i+1]:=s[i,j];
  s:=s1;
end;

procedure worknum(ch:char);
var i,j:longint;
begin
  fillchar(num,sizeof(num),0);
  for i:=1 to n do for j:=1 to n do if s[i,j]=ch then num[i,j]:=1;
  for j:=1 to n do
    for i:=n-1 downto 1 do
      num[i,j]:=num[i,j]+num[i+1,j];
end;

procedure cnt(ch:char);
var i,j,k:longint;
begin
  for i:=1 to n do
    for j:=1 to n do if s[i,j]=ch then
      for k:=1 to n do
        begin
          if (i+k>n) or (j+k>n) then break;
          if num[i,j+k]-num[i+k+1,j+k]<>k+1 then break;
          inc(a[ch]);
        end;
  for i:=1 to n do
    for j:=1 to n do if s[i,j]=ch then
      for k:=1 to n do
        begin
          if (i+k>n) or (j+k>n) or (i-k<1) then break;
          if num[i-k,j+k]-num[i+k+1,j+k]<>k+k+1 then break;
          inc(a[ch]);
        end;
end;

begin
  assign(input,'TRIANGLE.in'); reset(input);
  assign(output,'TRIANGLE.out'); rewrite(output);
  readln(n); fillchar(a,sizeof(a),255);
  for i:=1 to n do readln(s[i]);
  for i:=1 to n do for j:=1 to n do a[s[i,j]]:=0;
  for k:=1 to 4 do
    begin
      for ch:='A' to 'Z' do if a[ch]>=0 then
        begin worknum(ch); cnt(ch); end;
      if k<4 then change;
    end;
  ans:=0;
  for ch:='A' to 'Z' do if a[ch]>=0 then ans:=ans+a[ch];
  writeln(ans);
  for ch:='A' to 'Z' do if a[ch]>=0 then writeln(ch,'  ',a[ch]);
  close(input); close(output);
end.


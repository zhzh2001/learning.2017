#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define rep(i, n) for (int i = 0; i < n; ++i)
const int N = 300005;
int f[N], g[N], h[N], n, m, v, t, x, y, z;
bool ans;
char opr[5];
int get(int x, int t)
{
	while (f[x] >= 0 && g[x] <= t)
		x = f[x];
	return x;
}
void merge(int x, int y, int t)
{
	x = get(x, t), y = get(y, t);
	if (x != y)
	{
		if (h[x] > h[y])
			swap(x, y);
		f[x] = y, g[x] = t, h[y] += h[x] == h[y];
	}
}
int main()
{
	freopen("history.in", "r", stdin);
	freopen("history.out", "w", stdout);
	while (scanf("%d%d", &n, &m) != EOF)
	{
		rep(i, n) h[i] = 1, f[i] = -1;
		t = v = 0, ans = 1;
		while (m--)
		{
			scanf("%s", opr);
			if (*opr == 'K')
				scanf("%d", &v), ans = 1;
			else
			{
				scanf("%d%d", &x, &y);
				if (*opr == 'R')
					merge(ans ? x : (x + v) % n, ans ? y : (y + v) % n, ++t);
				else
					scanf("%d", &z), ans = (get(x, t) == get(y, t)) != (get(x, t - z) == get(y, t - z)), puts(ans ? "Y" : "N");
			}
		}
	}
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;
char fg[300010][5];
int x1[300010],x2[300010],x3[300010],n,m,pt[300010],T,c,md,fa[300010],lev[300010];
vector<int> ps[300010],pe[300010],ans[300010];
int grz(int x){
	if(x>=0) return x%n;
	else{
		if(x%n==0) return 0;
		else return (x%n)+n;
	}
}
void add(int f,int p1,int p2){
	//cout<<"add"<<f<<" "<<p1<<" "<<p2<<endl;
	ps[f].push_back(p1);
	pe[f].push_back(p2);
	//fl[f].push_back(flag);
}
void init(){
	for(int i=0;i<n;i++) fa[i]=i,lev[i]=1;
}
int froot(int x){
	if(fa[x]==x) return x;
	fa[x]=froot(fa[x]);
	return fa[x];
}
void SWAP(int &a,int &b){
	int t=a;
	a=b;
	b=t;
}
void uni(int a,int b){
	//cout<<a<<"<-->"<<b<<endl;
	int ra=froot(a),rb=froot(b);
	if(ra==rb) return;
	if(lev[ra]==lev[rb]){
		fa[ra]=rb;
		lev[rb]++;
		return;
	}
	if(lev[ra]>lev[rb]) SWAP(ra,rb);
	fa[ra]=rb;
}
int main(){
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;i++){
		getchar();
		scanf("%s",fg[i]);
		if(fg[i][0]=='K') scanf("%d",&x1[i]);
		else if(fg[i][0]=='R') scanf("%d %d",&x1[i],&x2[i]);
		else if(fg[i][0]=='T') scanf("%d %d %d",&x1[i],&x2[i],&x3[i]);
	}
	c=0,T=1;
	for(int i=1;i<=m;i++){
		if(fg[i][0]=='T'){
			if(T-x3[i]<1) continue;
			else{
				add(T-x3[i],x1[i],x2[i]);
				//add(T-x3[i],grz(x1[i]-c),grz(x2[i]-c));
			}
		}
		else if(fg[i][0]=='K') c=x1[i];
		else if(fg[i][0]=='R') T++;
	}
	c=0,T=1,md=0;
	init();
	vector<int>::iterator ip;
	int j;
	for(int i=1;i<=m;i++){
		if(fg[i][0]=='K') c=x1[i],md=0;
		else if(fg[i][0]=='R'){
			//cout<<"T="<<T<<"will have rep\n";
			for(ip=ps[T].begin(),j=0;ip!=ps[T].end();ip++,j++){
				//cout<<"ask"<<ps[T][j]<<" "<<pe[T][j]<<endl;
				int ra=froot(ps[T][j]),rb=froot(pe[T][j]);
				if(ra==rb) ans[T].push_back(1);
				else ans[T].push_back(0);
			}
			T++;
			if(md==0) uni(x1[i],x2[i]);
			else uni(grz(x1[i]+c),grz(x2[i]+c));
		}
		else if(fg[i][0]=='T'){
			int ra=froot(x1[i]),rb=froot(x2[i]);
			if(ra==rb){
				int t2=T-x3[i];
				if(t2<1) md=0;
				else{
					if(ans[t2][pt[t2]]) md=1;
					else md=0;
					pt[t2]++;
				}
			}
			else md=1;
			if(md==1) printf("N\n");
			else printf("Y\n");
		}
		//for(int i=1;i<=n;i++) cout<<froot(i)<<" ";
		//cout<<endl;
	}
	return 0;
}

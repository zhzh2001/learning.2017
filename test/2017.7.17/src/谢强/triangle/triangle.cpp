#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
using namespace std;
int s[110][110],anslu[110][110],ansru[110][110],ansld[110][110],ansrd[110][110],ans[30],up[110][110],dn[110][110],ans0,appe[30];
int n,m;
int minof(int x,int y){
	if(x<y) return x;
	else return y;
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	string tmp;
	scanf("%d",&n);
	memset(s,-1,sizeof(s));
	for(int i=1;i<=n;i++){
		cin>>tmp;
		for(int j=0;j<n;j++){
			s[i][j+1]=tmp[j]-'A';
			appe[s[i][j+1]]++;
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(s[i][j]==s[i-1][j]) up[i][j]=up[i-1][j]+1;
			else up[i][j]=1;
		}
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			for(int k=1;j-k>=1;k++)
				if(s[i][j]==s[i][j-k]&&up[i][j-k]>=k+1) anslu[i][j]++,ans[s[i][j]]++;
				else break;
			for(int k=1;j+k<=n;k++){
				if(s[i][j]==s[i][j+k]&&up[i][j+k]>=k+1) ansru[i][j]++,ans[s[i][j]]++;
				else break;
			}	
		}	
	for(int i=n;i>=1;i--)
		for(int j=1;j<=n;j++)
			if(s[i][j]==s[i+1][j]) dn[i][j]=dn[i+1][j]+1;
			else dn[i][j]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			for(int k=1;j-k>=1;k++)
				if(s[i][j]==s[i][j-k]&&dn[i][j-k]>=k+1) ansld[i][j]++,ans[s[i][j]]++;
				else break;
			for(int k=1;j+k<=n;k++)
				if(s[i][j]==s[i][j+k]&&dn[i][j+k]>=k+1) ansrd[i][j]++,ans[s[i][j]]++;
		}
	/*cout<<"lu"<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++) cout<<anslu[i][j]<<" ";
		cout<<endl;
	}
	cout<<"ru"<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++) cout<<ansru[i][j]<<" ";
		cout<<endl;
	}
	cout<<"ld"<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++) cout<<ansld[i][j]<<" ";
		cout<<endl;
	}
	cout<<"rd"<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++) cout<<ansrd[i][j]<<" ";
		cout<<endl;
	}
	cout<<ans[0]<<endl;
	cout<<ans[0]<<endl;*/
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans[s[i][j]]+=minof(anslu[i][j],ansld[i][j])+minof(ansru[i][j],ansrd[i][j]);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=j+2;k<=n;k+=2){
				//cout<<"i="<<i<<" j="<<j<<" k="<<k<<" l="<<ansru[i][j]<<"r="<<s[i][k]<<"mid"
				if(s[i][j]==s[i][k]&&j+ansru[i][j]>=(j+k)/2&&k-anslu[i][k]<=(j+k)/2) ans[s[i][j]]++;
				else break;
			}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			for(int k=j+2;k<=n;k+=2){
				if(s[i][j]==s[i][k]&&j+ansrd[i][j]>=(j+k)/2&&k-ansld[i][k]<=(j+k)/2) ans[s[i][j]]++;
				else break;
			}
		}
	}
	for(int i=0;i<26;i++) ans0+=ans[i];
	printf("%d\n",ans0);
	for(int i=0;i<26;i++) if(appe[i]) printf("%c %d\n",i+'A',ans[i]);
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <cmath>
typedef long long LL;
using namespace std;
int n;
struct pt{
	int x;
	int y;
}p[600010],p1[600010],p2[600010];
int cmp1(pt a,pt b){
	if(a.x<b.x) return 1;
	else if(a.x>b.x) return 0;
	else{
		if(a.y<b.y) return 1;
		else return 0;
	}
}
int cmp2(pt a,pt b){
	if(a.y<b.y) return 1;
	else  if(a.y>b.y) return 0;
	else{
		if(a.x<b.x) return 1;
		else return 0;
	}
}
int mahatan(int x1,int y1,int x2,int y2){
	//cout<<x1<<","<<y1<<"->"<<x2<<","<<y2<<endl;
	return abs(x1-x2)+abs(y1-y2);
}
int main(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d %d",&p[i].x,&p[i].y);
	sort(&p[1],&p[n+1],cmp1);
	for(int i=1;i<=n;i++) p1[i]=p[i];
	sort(&p[1],&p[n+1],cmp2);
	for(int i=1;i<=n;i++) p2[i]=p[i];
	int tx=p1[(n+1)/2].x,ty=p2[(n+1)/2].y;
	LL ans1=0,ans2=0;
	for(int i=1;i<=n;i++) ans1+=mahatan(tx,i,p2[i].x,p2[i].y);
	for(int i=1;i<=n;i++) ans2+=mahatan(i,ty,p1[i].x,p1[i].y);
	printf("%lld\n",min(ans1,ans2));
	return 0;
}

#include <iostream>
#include <cstdio>
#define MAX 600010
#define INF 0xfffffff
using namespace std;
int n, x, y;
long long ans=INF, ansx[MAX], ansy[MAX];

int read()
{
	int x = 0, f = 1;
	char c;
	c = getchar();
	while(!isdigit(c))
	{
		if (c=='-') f = -1;
		c = getchar();
	}
	while(isdigit(c))
	{
		x = x*10+c-'0';
		c = getchar();
	}
	return x*f;
}

int Abs(int x)
{
	if (x<0) return -x;
	return x;
}

int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n = read();
	for (int i = 1;i<=n;i++){
		x = read(),y = read();
		for (int j = 1;j<=n;j++){
			ansx[j]+=Abs(x-j)+Abs(y-i);
			ansy[j]+=Abs(x-i)+Abs(y-j);
		}
	}
	for (int i = 1;i<=n;i++){
		if (ansx[i]<ans) ans = ansx[i];
		if (ansy[i]<ans) ans = ansy[i];
	}
	cout << ans;
	fclose(stdout),fclose(stdout);
	return 0;
}

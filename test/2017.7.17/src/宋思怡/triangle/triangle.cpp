#include <iostream>
#include <cstdio>
#define MAX 110
using namespace std;
int n, r, ans, sum, anss[30], a[MAX][MAX][MAX][2];
char c, maxn, x[MAX][MAX];

int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	maxn = 'A';
	for (int i = 1;i<=n;i++){
		c = getchar();
		while(c<'A'||c>'Z') c = getchar();
		x[i][1] = c;
		if (x[i][1]>maxn) maxn = x[i][1];
		for (int j = 2;j<=n;j++){
			x[i][j] = getchar();
			if (x[i][j]>maxn) maxn = x[i][j];
		}
	}
	for (int i = 1;i<=n;i++)
		for (int j = 1;j<=n;j++){
			if (x[i][j+1]==x[i][j]&&x[i][j]==x[i+1][j]){
				a[i][j][++a[i][j][0][0]][0] = 2;
				a[i][j][a[i][j][0][0]][1] = 1;
				r = 2;
				while(x[i][j+r]==x[i][j]&&x[i][j]==x[i+r][j]){
					a[i][j][++a[i][j][0][0]][0] = ++r;
					a[i][j][a[i][j][0][0]][1] = 1;
				}
			}
			if (x[i][j+1]==x[i][j]&&x[i][j]==x[i-1][j]){
				a[i][j][++a[i][j][0][0]][0] = 2;
				a[i][j][a[i][j][0][0]][1] = 2;
				r = 2;
				while(x[i][j+r]==x[i][j]&&x[i][j]==x[i-r][j]){
					a[i][j][++a[i][j][0][0]][0] = ++r;
					a[i][j][a[i][j][0][0]][1] = 2;
				}
			}
			if (x[i][j-1]==x[i][j]&&x[i][j]==x[i+1][j]){
				a[i][j][++a[i][j][0][0]][0] = 2;
				a[i][j][a[i][j][0][0]][1] = 3;
				r = 2;
				while(x[i][j-r]==x[i][j]&&x[i][j]==x[i+r][j]){
					a[i][j][++a[i][j][0][0]][0] = ++r;
					a[i][j][a[i][j][0][0]][1] = 3;
				}
			}
			if (x[i][j-1]==x[i][j]&&x[i][j]==x[i-1][j]){
				a[i][j][++a[i][j][0][0]][0] = 2;
				a[i][j][a[i][j][0][0]][1] = 4;
				r = 2;
				while(x[i][j-r]==x[i][j]&&x[i][j]==x[i-r][j]){
					a[i][j][++a[i][j][0][0]][0] = ++r;
					a[i][j][a[i][j][0][0]][1] = 4;
				}
			}
			sum = 0;
			for (int q = 1;q<=a[i][j][0][0];q++)
				for (int k = q+1;k<=a[i][j][0][0];k++){
					if (a[i][j][q][0]==a[i][j][k][0])
						if ((a[i][j][q][1]+a[i][j][k][1])!=5) sum++;
			}
			a[i][j][0][0]+=sum;
		}
	for (int i = 1;i<=n;i++)
		for (int j = 1;j<=n;j++)
			if (a[i][j][0][0]){
				ans+=a[i][j][0][0];
				anss[x[i][j]-'A']+=a[i][j][0][0];
		}
	printf("%d\n",ans);
	sum = maxn -'A';
	for (int i = 0;i<=sum;i++)
		printf("%c  %d\n",('A'+i),anss[i]);
	fclose(stdout),fclose(stdout);
	return 0;
}

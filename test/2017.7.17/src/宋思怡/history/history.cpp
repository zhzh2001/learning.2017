#include <iostream>
#include <cstdio>
#define MAX 300010
using namespace std;
int n, m, x, t, y, Now, a[MAX][2];
char k;

int read()
{
	int x = 0, f = 1;
	char c=getchar();
	while(!isdigit(c)){
		if (c=='-') f = -1;
		c = getchar();
	}
	while(isdigit(c)){
		x = x*10+c-'0';
		c = getchar();
	}
	return x*f;
}

char reads()
{
	char c=getchar();
	while(c<'A'||c>'Z')
		c = getchar();
	return c;
}

int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n = read(), m = read();
	while(m--){
		k = reads();
		if (k=='R'){
			Now++;
			x = read(),y = read();
			if (x<y){
				a[Now][0] = x;
				a[Now][1] = y;
			}else{
				a[Now][0] = y;
				a[Now][1] = x;
			}
		}
		else if (k=='T'){
			x = read(),y = read(),t = read();
			int i = 1;
			while(x!=a[i][0]&&y!=a[i][1]&&x<=Now&&y<=Now){
				i++;				
			}
			if (x==a[i][0]&&y!=a[i][1]) cout << "Y" << '\n';
			else cout << "N" << '\n';
		}
		
	}
	fclose(stdout),fclose(stdout);
	return 0;
}

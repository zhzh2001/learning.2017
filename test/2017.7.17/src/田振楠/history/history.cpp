#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
bool flag;//表示国王当前是否生气
int yr;//年份
map<int,int>f[600100];//记录i-j的道路最早由何时修造 
vector<int>v[300100];
bool ff[300100];
int b[300100];
int n,m,t,c,st,ed,x,y;
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool bfs(int st,int ed,int te)
{
	memset(ff,false,sizeof(ff));
	ff[st]=true;
	int h=0,t=1;
	b[t]=st;
	while(h<t)
	{
		h++;
		For(i,0,v[b[h]].size()-1)if(ff[v[b[h]][i]]==false&&f[b[h]][v[b[h]][i]]<=te)
								{
									t++;
									b[t]=v[b[h]][i];
									if(b[t]==ed)return true;
								}
	}
	return false;
}
int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=read();m=read();
	c=0;flag=true;
	while(m--)
	{
		ch=getchar();
		while(ch!='K'&&ch!='T'&&ch!='R')ch=getchar();
		if(ch=='K')
		{
			c=read();flag=true;
		}
		if(ch=='T')
		{
			st=read();ed=read();
			t=read();
			if(bfs(st,ed,yr)&&!bfs(st,ed,max(0,yr-t)))flag=true;else flag=false; 
			if(flag)cout<<"Y"<<endl;else cout<<"N"<<endl;
		}
		if(ch=='R')
		{
			yr++;
			x=read();y=read();
			if(!flag)
			{
				x+=c;
				x%=n;
				y+=c;
				y%=n;
			}
			if(f[x][y]==0)
			{
				f[x][y]=yr,f[y][x]=yr;
				v[x].push_back(y);
				v[y].push_back(x);
			}
		}
	}
	return 0;
}


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
using namespace std;

int n,m;
int fa[4005][2005];

int find(int x,int num)
{
	if(x==fa[num][x])
		return x;
	int t=fa[num][x];
	fa[num][x]=find(t,num);
	return fa[num][x];
}

void join(int x,int y,int num)
{
	int fx=find(x,num),fy=find(y,num);
	if(fx!=fy)
		fa[num][fx]=fy;
}

bool judge(int x,int y,int num)
{
	int fx=find(x,num),fy=find(y,num);
	if(fx!=fy)
		return 0;
	else
		return 1;
}

int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	int i,x,y,j;
	scanf("%d %d",&n,&m);
	for(i=0;i<n;i++)
		fa[0][i]=i;
	int c=0,t=0,on1=0,ti;
	char s;
	for(i=1;i<=m;i++)
	{
		scanf("%s",&s);
		if(s=='R')
		{
			t++;
			for(j=0;j<n;j++)
				fa[t][j]=fa[t-1][j];
			scanf("%d %d",&x,&y);
			if(on1==1)
			{
				x=(x+n-c)%n;
				y=(y+n-c)%n;
			}
			join(x,y,t);
		}
		else if(s=='K')
		{
			scanf("%d",&c);
			on1=0;
		} 
		else
		{
			scanf("%d %d %d",&x,&y,&ti);
			int a1=0,a2=0;
			a1=judge(x,y,t);
			a2=judge(x,y,t-ti);
			if(a1==1&&a2==0)
			{
				printf("Y\n");
				on1=0;
			}
			else
			{
				printf("N\n");
				on1=1;
			}
		}
	}
	return 0;
}


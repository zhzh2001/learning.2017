#include <iostream>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <stdlib.h>
using namespace std;

struct point
{
	int x,y;
}p[600005];

int n,id;
int a[600005],b[600005];

int Abs(int x)
{
	return x>0?x:-x;
}

bool cmp1(point xi,point yi)
{
	return xi.x==yi.x?xi.y<yi.y:xi.x<yi.x;
}

bool cmp2(point xi,point yi)
{
	return xi.y==yi.y?Abs(xi.x-id)>Abs(yi.x-id):xi.y<yi.y; 
} 

bool cmp3(point xi,point yi)
{
	return xi.y==yi.y?xi.x<yi.x:xi.y<yi.y;
}

bool cmp4(point xi,point yi)
{
	return xi.x==yi.x?Abs(xi.y-id)>Abs(yi.y-id):xi.x<yi.x; 
} 

int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	int i;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d %d",&p[i].x,&p[i].y);
	sort(p+1,p+n+1,cmp1); 
	id=p[(n>>1)+1].x;
	sort(p+1,p+n+1,cmp2);
	long long ans1=0;
	for(i=1;i<=n;i++)
		ans1+=Abs(id-p[i].x)+Abs(p[i].y-i);
	sort(p+1,p+n+1,cmp3); 
	id=p[(n>>1)+1].y;
	sort(p+1,p+n+1,cmp4);
	long long ans2=0;
	for(i=1;i<=n;i++)
		ans2+=Abs(id-p[i].y)+Abs(p[i].x-i);
	printf("%lld\n",min(ans1,ans2));
	return 0;
}


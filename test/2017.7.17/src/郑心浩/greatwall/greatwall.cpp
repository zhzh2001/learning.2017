#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
#define N 600005
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch<'0' || ch>'9'){if(ch=='-')f = -1;ch = getchar();}
	while(ch>='0' && ch<='9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll n;
ll x[N],y[N];
ll d[N],top;
bool visx[N],visy[N];
ll ans;

int main(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n = read();
	for(ll i = 1;i <= n;i++){
		x[i] = read();
		y[i] = read();
		visx[x[i]] = true;
		visy[y[i]] = true;
	}
	sort(x+1,x+n+1);
	sort(y+1,y+n+1);
	ll p = x[n/2+1];
	ll num = 0;
	for(ll i = 1;i <= n;i++)
		num += abs(p-x[i]);
	for(ll i = 2;i <= n;i++)
		if(y[i] == y[i-1])
			d[++top] = y[i];
	ll j = 0;
	for(ll i = 1;i <= n;i++)
		if(!visy[i])
			num += abs(d[++j]-i);
	ans = num;
	
	p = y[n/2+1];
	num = 0;
	j =0; top = 0;
	for(ll i = 1;i <= n;i++)
		num += abs(p-y[i]);
	for(ll i = 2;i <= n;i++)
		if(x[i] == x[i-1])
			d[++top] = x[i];
	for(ll i = 1;i <= n;i++)
		if(!visx[i])
			num += abs(d[++j]-i);
	ans = min(ans,num);
	printf("%lld\n",ans);
	return 0;
}

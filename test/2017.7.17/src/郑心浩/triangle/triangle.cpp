#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
using namespace std;

int n;
long long ans[35];
int vi[35];
int a[105][105];
int b[105][5];

int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	int i,j,to;
	char ch;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
		{
			cin>>ch;
			to=ch-'A'+1;
			a[i][j]=to;
			vi[to]=1;
		}
	int mo,k,l,o,p,on1,mx;
	long long ans1=0;
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
		{
			mo=a[i][j];
			mx=0;
			for(k=1;k<=n-1;k++)
				b[k][1]=b[k][2]=b[k][3]=b[k][4]=0;
			for(k=1;k<=n-1;k++)
			{
				if(i<k+1||j<k+1)
					break;
				on1=0;
				for(l=i-k,p=1;l<=i;l++,p++)
				{
					for(o=j-p+1;o<=j;o++)
						if(a[l][o]!=mo)
						{
							on1=1;
							break;
						}
					if(on1==1)
						break;
				}
				if(on1==1)
					break;
				else
				{
					ans[mo]++,ans1++;
					b[k][1]=1;
					mx=max(k,mx);
				}
			}
			for(k=1;k<=n-1;k++)
			{
				if(i<k+1||j+k>n)
					break;
				on1=0;
				for(l=i-k,p=1;l<=i;l++,p++)
				{
					for(o=j;o<=j+p-1;o++)
						if(a[l][o]!=mo)
						{
							on1=1;
							break;
						}
					if(on1==1)
						break;
				}
				if(on1==1)
					break;
				else
				{
					ans[mo]++,ans1++;
					b[k][2]=1;
					mx=max(mx,k);
				}
			}
			for(k=1;k<=n-1;k++)
			{
				if(i+k>n||j+k>n)
					break;
				on1=0;
				for(l=i,p=k;l<=i+k;l++,p--)
				{
					for(o=j;o<=j+p;o++)
						if(a[l][o]!=mo)
						{
							on1=1;
							break;
						}
					if(on1==1)
						break;
				}
				if(on1==1)
					break;
				else
				{
					ans[mo]++,ans1++;
					b[k][3]=1;
					mx=max(mx,k);
				}
			}
			for(k=1;k<=n-1;k++)
			{
				if(j<k+1||i+k>n)
					break;
				on1=0;
				for(l=i,p=k;l<=i+k;l++,p--)
				{
					for(o=j-p;o<=j;o++)
						if(a[l][o]!=mo)
						{
							on1=1;
							break;
						}
					if(on1==1)
						break;
				}
				if(on1==1)
					break;
				else
				{
					ans[mo]++,ans1++;
					b[k][4]=1;
					mx=max(mx,k);
				}
			}
			for(k=1;k<=mx;k++)
			{
				if(b[k][1]==1&&b[k][2]==1)
					ans[mo]++,ans1++;
				if(b[k][2]==1&&b[k][3]==1)
					ans[mo]++,ans1++;
				if(b[k][3]==1&&b[k][4]==1)
					ans[mo]++,ans1++;
				if(b[k][4]==1&&b[k][1]==1)
					ans[mo]++,ans1++;
			}
		}
	printf("%lld\n",ans1);
	for(i=1;i<=26;i++)
		if(vi[i]==1)
			printf("%c  %lld\n",char('A'+i-1),ans[i]);
	return 0;
}


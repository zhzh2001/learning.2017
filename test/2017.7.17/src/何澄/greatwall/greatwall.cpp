#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n;
int x[600011],y[600011];
long long minn=0,zz=0;

int read()
{
	long long x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=read();
	for(long long i=1; i<=n; i++)
		x[i]=read(),y[i]=read();
	sort(x+1,x+n+1),sort(y+1,y+n+1);
	for(long long i=1; i<=n; i++)
		zz+=(long long)(abs(y[i]-i));
	for(long long i=1; i<=n; i++)
		zz+=(long long)(abs(x[i]-x[(n+1)/2]));
	minn=zz;
	zz=0;
	for(long long i=1; i<=n; i++)
		zz+=(long long)(abs(x[i]-i));
	for(long long i=1; i<=n; i++)
		zz+=(long long)(abs(y[i]-y[(n+1)/2]));
	minn=min(minn,zz);
	cout << minn << endl;
	return 0;
}
/*
5
1 2
2 4
3 4
5 1
5 3

*/
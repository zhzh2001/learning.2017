#include <iostream>
#include <cstdio>
using namespace std;

char c[105][105],cc;
int n,ans=0;
int dd[29];
int dx[6]={0,1,-1,0,0};
int dy[6]={0,0,0,1,-1};
int f[105][105][105][6]={0};

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

void solve(int x,int y)
{
	char ch=c[x][y];
	for(int i=1; i<=4; i++)
	{
		if(x+dx[i]<=n&&x+dx[i]>=1&&y+dy[i]<=n&&y+dy[i]>=1)
		{
			int zx=x+dx[i],zy=y+dy[i];
			if(c[zx][zy]==ch)
				f[x][y][1][i]=1;
		}
	}
}

int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();
	for(int i=1; i<=n; i++)
	{
		for(int j=1; j<=n; j++)
			c[i][j]=getchar(),dd[c[i][j]-'A'+1]++;
		cc=getchar();
	}
	//smg
	if(dd[1]==n*n)
	{
		if(n==1)
		{
			cout << 0 << endl;
			cout << "A  " << 0 << endl;
		}
		else
		{
			for(int i=1; i<=n-1; i++)
				ans+=4*(n-i)*(n-i);
			for(int i=2; i<=n; i+=2)
				ans+=4*(n-i)*(n-i+1);
			cout << ans << endl;
			cout << "A  " << ans << endl;
		}
		return 0;
	}

	for(int i=1; i<=n; i++)
		for(int j=1; j<=n; j++)
			solve(i,j);
}
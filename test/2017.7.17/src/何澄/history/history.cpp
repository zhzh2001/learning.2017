#include <iostream>
#include <cstdio>
#include <ctime>
#include <cmath>
#include <algorithm>
using namespace std;

int n,m,x,y,z,year=0;
int c=0,cc=0;
int tim[600005],to[600005],nxt[600005];;
int head[600005];
int vis[600005];
int q[600005];
int pos=0;
int zzz=0;

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

void add(int u,int v,int w)
{
	tim[++pos]=w;
	to[pos]=v;
	nxt[pos]=head[u];
	head[u]=pos;
}

bool check(int u,int v,int timm)
{
	zzz++;
	if(u==v)
		return 1;
	q[1]=u;
	vis[u]=zzz;
	int l=1,r=1;
	while(l<=r)
	{
		for(int i=head[q[l]]; i; i=nxt[i])
			if(tim[i]<=timm && vis[to[i]]!=zzz)
			{
				q[++r]=to[i];
				vis[to[i]]=zzz;
				if(to[i]==v)
					return 1;
			}
		l++;
	}
	return 0;
}

int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=read(),m=read();
	char ch;
	for(int i=1; i<=m; i++)
	{
		cin >> ch;
		if(ch=='R')
		{
			year++;
			x=read(),y=read();
			x=(x+cc)%n;y=(y+cc)%n;
			add(x,y,year);add(y,x,year);
		}
		else if(ch=='K')
		{
			x=read();
			c=x;cc=0;
		}
		else if(ch=='T')
		{
			x=read(),y=read(),z=read();
			if(check(x,y,year) && !check(x,y,(year-z)>0?(year-z):0))
				puts("Y"),cc=0;
			else puts("N"),cc=c;
		}
	}
	return 0;
}
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

using namespace std;

ifstream fin("greatwall.in");
ofstream fout("greatwall.out");

const int MaxN = 6e5 + 5;

typedef pair<int, int> point;

point p[MaxN];
int vis[MaxN], used[MaxN];
int N, now, Ans, Res;

inline bool cmp1(const point &a, const point &b)
{
	return a.first < b.first;
}

inline bool cmp2(const point &a, const point &b)
{
	return a.second < b.second;
}

int Sol1()
{
	memset(used, 0, sizeof used);
	memset(vis, 0, sizeof vis);
	sort(p + 1, p + N + 1, cmp1);
	now = p[(1 + N) / 2].first;
	vis[0] = true;
	sort(p + 1, p + N + 1, cmp2);
	for (int i = 1; i <= N; i++)
	{
		if (now == p[i].first)
			vis[p[i].second] = true, used[i] = true;
	}
	int lptr = 0;
	for (int i = 1; i <= N; i++)
	{
		if (!used[i])
		{
			while (vis[lptr])
				lptr++;
			vis[lptr] = true;
			used[i] = true;
			Ans += abs(now - p[i].first) + abs(lptr - p[i].second);
		}
	}
	Res = Ans, Ans = 0;

	memset(used, 0, sizeof used);
	memset(vis, 0, sizeof vis);
	sort(p + 1, p + N + 1, cmp2);
	now = p[(1 + N) / 2].second;
	vis[0] = true;
	sort(p + 1, p + N + 1, cmp1);
	for (int i = 1; i <= N; i++)
	{
		if (now == p[i].second)
			vis[p[i].first] = true, used[i] = true;
	}
	lptr = 0;
	for (int i = 1; i <= N; i++)
	{
		if (!used[i])
		{
			while (vis[lptr])
				lptr++;
			vis[lptr] = true;
			used[i] = true;
			Ans += abs(now - p[i].second) + abs(lptr - p[i].first);
		}
	}
	Res = min(Res, Ans);
	fout << Res << endl;
	return 0;
}

int Sol2()
{
	int Time = 0, Res = 0x3f3f3f3f;
	sort(p + 1, p + N + 1, cmp1);
	for (int k = 1; k <= N; k++)
	{
		Ans = 0;
		++Time;
		vis[0] = Time;
		for (int i = 1; i <= N; i++)
		{
			if (now == p[i].second)
				vis[p[i].first] = Time, used[i] = Time;
		}
		int lptr = 0;
		for (int i = 1; i <= N; i++)
		{
			if (used[i] != Time)
			{
				while (vis[lptr] == Time)
					lptr++;
				vis[lptr] = Time;
				used[i] = Time;
				Ans += abs(k - p[i].second) + abs(lptr - p[i].first);
			}
		}
		Res = min(Res, Ans);
		// cout << "Y: " << k << "->" << Ans << endl;
	}

	sort(p + 1, p + N + 1, cmp2);
	for (int k = 1; k <= N; k++)
	{
		Ans = 0;
		++Time;
		vis[0] = Time;
		for (int i = 1; i <= N; i++)
		{
			if (now == p[i].first)
				vis[p[i].second] = Time, used[i] = Time;
		}
		int lptr = 0;
		for (int i = 1; i <= N; i++)
		{
			if (used[i] != Time)
			{
				while (vis[lptr] == Time)
					lptr++;
				vis[lptr] = Time;
				used[i] = Time;
				Ans += abs(k - p[i].first) + abs(lptr - p[i].second);
			}
		}
		Res = min(Res, Ans);
		// cout << "X: " << k << "->" << Ans << endl;
	}
	fout << Res << endl;
	return 0;
}

int main()
{
	fin >> N;
	for (int i = 1; i <= N; i++)
		fin >> p[i].first >> p[i].second;
	if (N <= 5000)
		return Sol2();
	return Sol1();
}
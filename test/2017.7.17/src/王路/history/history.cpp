#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;

ifstream fin("history.in");
ofstream fout("history.out");

const int MaxN = 1000 + 5;

int N, M;

/*
namespace ufset
{
	int fa[MaxN * 3][MaxN];

	inline void Init(int k)
	{
		for (int i = 1; i <= N; i++)
			fa[k][i] = i;
	}

	inline int Find(int k, int x)
	{
		return x == fa[k][x] ? x : fa[k][x] = Find(k, fa[k][x]);
	}

	inline void Unite(int k, int x, int y)
	{
		x = Find(k, x), y = Find(k, y);
		if (x != y)
			fa[k][y] = x;
	}

	inline bool Same(int k, int x, int y)
	{
		return Find(k, x) == Find(k, y);
	}

	inline void Copy(int k1, const int &k2)
	{
		memcpy(fa[k1], fa[k2], sizeof(fa[k2]));
	}
}

int Sol1()
{
	ufset::Init(0);
	int now = 0, laststate = 1, c = 0;
	bool isAngry = false;
	for (int i = 1; i <= M; i++)
	{
		char opt[4];
		fin >> opt;
		switch(opt[0])
		{
			case 'R':
			{
				int x, y;
				fin >> x >> y;
				++now;
				ufset::Copy(now, now - 1);
				if (!isAngry)
					ufset::Unite(now, x, y);
				else
				{
					x = (x + N + c) % N;
					y = (y + N + c) % N;
					ufset::Unite(now, x, y);
				}
				break;
			}
			case 'T':
			{
				isAngry = false;
				int st, ed, t;
				fin >> st >> ed >> t;
				if (ufset::Same(now, st, ed) && !ufset::Same(max(now - t, 0), st, ed))
				{
					fout << "Y" << endl;
					laststate = 1;
				}
				else
				{
					fout << "N" << endl;
					laststate = 0;
					isAngry = true;
				}
				break;
			}
			case 'K':
			{
				isAngry = false;
				laststate = 1;
				fin >> c;
				ufset::Copy(now + 1, now);
				now++;
				break;
			}
		}
	}
	return 0;
}
*/
const int MaxM = 3e5 + 5;

int fa[MaxM];

void Init()
{
	for (int i = 1; i <= N; i++)
		fa[i] = i;
}

int Find(int x)
{
	return x == fa[x] ? x : fa[x] = Find(fa[x]);
}

bool Same(int x, int y)
{
	return Find(x) == Find(y);
}

void Unite(int x, int y)
{
	x = Find(x), y = Find(y);
	if (x != y)
		fa[y] = x;
}

struct Request
{
	int x, y, ans;

	Request(int x, int y)
		:x(x), y(y)
	{
	}
};

struct Query
{
	int opt, x, y, z, id, time;
	Request *link;
} Q[MaxM];

vector<Request> task[MaxM];

int Sol2()
{
	for (int i = 1; i <= M; i++)
	{
		char opt[4];
		fin >> opt;
		Q[i].id = i;
		switch(opt[0])
		{
			case 'R':
			{
				Q[i].opt = 1;
				fin >> Q[i].x >> Q[i].y;
				break;
			}
			case 'T':
			{
				Q[i].opt = 2;
				fin >> Q[i].x >> Q[i].y >> Q[i].z;
				break;
			}
			case 'K':
			{
				Q[i].opt = 3;
				fin >> Q[i].x;	
				break;
			}
		}
	}
	int now = 0;
	for (int i = 1; i <= M; i++)
	{
		if (Q[i].opt == 1)
			++now;
		else if (Q[i].opt == 2)
		{
			task[max(now - Q[i].z, 0)].push_back(Request(Q[i].x, Q[i].y));
			Q[i].link = &(task[max(now - Q[i].z, 0)].back());
		}
		else if (Q[i].opt == 3)
			++now;
		Q[i].time = now;
	}

	int laststate = 1, c = 0;
	now = 0;
	bool isAngry = false;
	Init();
	for (int i = 1; i <= M; i++)
	{
		switch(Q[i].opt)
		{
			case 1:
			{
				int x = Q[i].x, y = Q[i].y;
				++now;
				if (!isAngry)
					Unite(x, y);
				else
				{
					x = (x + N + c) % N;
					y = (y + N + c) % N;
					Unite(x, y);
				}
				break;
			}
			case 2:
			{
				isAngry = false;
				int st = Q[i].x, ed = Q[i].y;
				if (Same(st, ed) && !Q[i].link->ans)
				{
					fout << "Y" << endl;
					laststate = 1;
				}
				else
				{
					fout << "N" << endl;
					laststate = 0;
					isAngry = true;
				}
				break;
			}
			case 3:
			{
				isAngry = false;
				laststate = 1;
				c = Q[i].x;
				now++;
				break;
			}
		}
		if (!task[now].empty())
		{
			for (vector<Request>::iterator it = task[now].begin(); it != task[now].end(); ++it)
			{
				it->ans = Same(it->x, it->y);
			}
		}
	}
	return 0;
}

int main()
{
	fin >> N >> M;
	return Sol2();
}
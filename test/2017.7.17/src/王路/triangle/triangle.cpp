#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <numeric>

using namespace std;

ifstream fin("triangle.in");
ofstream fout("triangle.out");

const int MaxN = 105;

int N;
char Str[MaxN][MaxN];
int Mat[26][MaxN][MaxN];
bool Vis[26];

const int dx[4][3] = {
{-1, -1, -1 }, //up
{1, 1, 1}, //down
{-1, 0, 1}, //left
{-1, 0, 1} //right
}, dy[4][3] = {
{-1, 0, 1}, //up
{-1, 0, 1}, //down
{-1, -1, -1}, //left
{1, 1, 1} //right
};

int k, Ans[26];
const int scp[3][2] = 
{
	{1, 0},
	{0, 1},
	{-1, 1}
};

/*
void Solve()
{
	for (int i = 1; i <= N; i++)
		for (int j = 1; j <= N; j++) /// pos
		{
			for (int kk = 0; kk < 4; kk++)
				for (int d = 0; d <= N; d++)
				{
					bool flag = true;
					for (int p = min(i - d * dx[kk], i + d * dx[kk]); p <= max(i - d * dx[kk], i + d * dx[kk]); p++)
					{
						if (p <= 0 || p > N) {
							flag = false;
							break;
						}
						if (!Mat[k][p][j - d * dy[kk]])
						{
							if (p >= (i - d * dx[kk] + i + d * dx[kk]) / 2)
								Ans[k]++;
							flag = false;
							break;
						}
					}
					if (flag)
						Ans[k] += 2;
					else
						break;
				}
		}
}
*/

void Solve()
{
	bool sel[3] = {0};
	for (int i = 1; i <= N; i++)
	{
		for (int j = 1; j <= N; j++)
		{
			if (!Mat[k][i][j]) continue;
			for (int d = 0; d < 4; d++) // dir(4)
			{
				if (d < 2) //u & d
				{
					sel[0] = sel[1] = sel[2] = true;
					for (int sc = 0; sc < 3; sc++)
					{
						for (int dep = 1; dep <= N; dep++)
						{
							if (!sel[sc]) break;
							int lb = j + dy[d][sc] * dep * scp[sc][0], rb = j + dy[d][sc] * dep * scp[sc][1];
							if (lb <= 0 || rb <= 0 || lb > N || rb > N) break;
							for (int kk = lb; kk <= rb; kk++)
							{
								if (!Mat[k][i + dep * dx[d][sc]][kk])
								{
									sel[sc] = false;
									break;
								}
							}
							if (sel[sc])
							{
								Ans[k]++;
								// cout << i << ' ' << j << ' ' << d << endl;
							}
						}
					}
				}
				else //l & r
				{
					sel[0] = sel[1] = sel[2] = true;
					for (int sc = 0; sc < 3; sc++)
					{
						for (int dep = 1; dep <= N; dep++)
						{
							if (i - dep < 0) break;
							if (!sel[sc]) break;
							int lb = i + dx[d][sc] * dep * scp[sc][0], rb = i + dx[d][sc] * dep * scp[sc][1];
							if (lb <= 0 || rb <= 0 || lb > N || rb > N) break;
							for (int kk = lb; kk <= rb; kk++)
							{
								if (!Mat[k][kk][j + dep * dy[d][sc]])
								{
									sel[sc] = false;
									break;
								}
							}
							if (sel[sc])
							{
								Ans[k]++;
								// cout << i << ' ' << j << ' ' << d << endl;
							}
						}
					}
				}
			}
		}
	}
}

int main()
{
	fin >> N;
	for (int i = 1; i <= N; i++)
	{
		fin >> (Str[i] + 1);
		for (int j = 1; Str[i][j]; j++)
		{
			Mat[Str[i][j] - 'A'][i][j] = true;
			Vis[Str[i][j] - 'A'] = true;
		}
	}
	for (int i = 0; i < 26; i++)
	{
		if (!Vis[i]) continue;
		k = i;
		Solve();
	}
	fout << accumulate(Ans, Ans + 26, 0) / 4 << endl;
	for (int i = 0; i < 26; i++)
		if (Vis[i])
			fout << char(i + 'A') << "  " << Ans[i] / 4 << endl;
	return 0;
}
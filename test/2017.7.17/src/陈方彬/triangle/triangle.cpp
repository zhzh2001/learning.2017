#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e2+5;
int a[N][N],f[N][N],ans[N],X[N][N],Y[N][N];
bool b[N];
int n,sum;
char c;
void find(){
	memset(f,0,sizeof f);
	memset(X,0,sizeof X);
	memset(Y,0,sizeof Y);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]!=a[i][j-1])X[i][j]=0;else X[i][j]=X[i][j-1]+1;
			if(a[i][j]!=a[i-1][j])Y[i][j]=0;else Y[i][j]=Y[i-1][j]+1;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(a[i-1][j-1]!=a[i][j])f[i][j]=1+min(1,min(X[i][j],Y[i][j]));else
			f[i][j]=1+min(f[i-1][j-1]+1,min(X[i][j],Y[i][j]));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans[a[i][j]]+=f[i][j]-1;
			
	memset(f,0,sizeof f);
	memset(X,0,sizeof X);
	memset(Y,0,sizeof Y);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(a[i][j]!=a[i-1][j-1])X[i][j]=0;else X[i][j]=X[i-1][j-1]+1;
			if(a[i][j]!=a[i-1][j+1])Y[i][j]=0;else Y[i][j]=Y[i-1][j+1]+1;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(a[i-1][j]!=a[i][j])f[i][j]=1;else
			f[i][j]=1+min(f[i-1][j],min(X[i][j],Y[i][j]));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans[a[i][j]]+=f[i][j]-1;
}
void CSGO(){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			f[j][n-i+1]=a[i][j];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			a[i][j]=f[i][j];
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			cin>>c;
			a[i][j]=c;
			b[c]=1;
		}
	for(int i=1;i<=4;i++)CSGO(),find();
	for(int i=1;i<N;i++)sum+=ans[i];cout<<sum<<endl;
	for(int i=1;i<N;i++)if(b[i])cout<<char(i)<<"  "<<ans[i]<<endl;
}

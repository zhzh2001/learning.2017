#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=6e5+5;
Ll a[N],b[N],in[N][2];
Ll n,ans,ans2,x,y;
int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	scanf("%lld",&n);
	for(Ll i=1;i<=n;i++){
		scanf("%lld%lld",&x,&y);
		a[i]=x;b[i]=y;
		in[i][0]=x;
		in[i][1]=y;
	}
	sort(a+1,a+n+1);
	sort(b+1,b+n+1);
	for(Ll i=1;i<=n;i++)ans+=abs(b[i]-i)+abs(a[i]-a[(n+1)/2]);
	
	for(Ll i=1;i<=n;i++)a[i]=in[i][1],b[i]=in[i][0];
	sort(a+1,a+n+1);
	sort(b+1,b+n+1);
	for(Ll i=1;i<=n;i++)ans2+=abs(b[i]-i)+abs(a[i]-a[(n+1)/2]);
	printf("%lld",min(ans,ans2));
}

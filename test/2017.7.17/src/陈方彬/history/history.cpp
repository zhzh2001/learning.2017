#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=3e5+5;
struct li{int v,x,y,id;}L[N];
int fa[N],a[N][3];
int n,m,x,y,z,C,day,top,l;
bool kk,ok[N];
char c;
int get(int x){
	if(!fa[x])return x;
	return fa[x]=get(fa[x]);
}
void init(int x,int y){
	int X=get(x),Y=get(y);
	if(X==Y)return;
	fa[X]=Y;
}
bool ask(int x,int y){return get(x)==get(y);}
bool cmp(li a,li b){return a.id<b.id;}
int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		cin>>c;
		if(c=='K'){
			a[i][0]=1;
			scanf("%d",&a[i][1]);
		}else
		if(c=='R'){
			a[i][0]=2;day++;
			scanf("%d%d",&a[i][1],&a[i][2]);
		}else{
			a[i][0]=3;
			scanf("%d%d%d",&a[i][1],&a[i][2],&z);
			top++;
			L[top].x=a[i][1];
			L[top].y=a[i][2];
			L[top].id=day-z;
			L[top].v=i;
		}
	}
	sort(L+1,L+top+1,cmp);
	l=1;day=0;
	while(l<=top&&L[l].id==0)l++;
	for(int i=1;i<=m;i++)
		if(a[i][0]==1){
			C=a[i][1];
			kk=0;
		}else
		if(a[i][0]==2){
			x=a[i][1];y=a[i][2];
			if(kk)x=(x+C)%n,y=(y+C)%n;
			init(x,y);day++;
			while(l<=top&&L[l].id<=day)ok[L[l].v]=ask(L[l].x,L[l].y),l++;
		}else{
			kk=0;
			if(ok[i])kk=1;
			if(!ask(a[i][1],a[i][2]))kk=1;
			if(kk)printf("N\n");else printf("Y\n");
		}
}

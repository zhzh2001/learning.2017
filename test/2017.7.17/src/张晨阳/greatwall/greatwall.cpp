#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,anx,any;
struct node
{
	int x,y;
	bool operator < (const node &b) const
	{
		return x<b.x;
	}
}a[600011];
int sum[600011],sum2[600011];
long long ans=0,ans2=0;

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

bool cmp(const node &a,const node &b)
{
	return a.y<b.y;
}

int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read(),sum[a[i].y]++,sum2[a[i].x]++;
	sort(a+1,a+n+1);
	anx=a[(n/2)+1].x;
	for (int i=1;i<=n;i++)
		ans+=(anx>a[i].x?anx-a[i].x:a[i].x-anx);
	for (int i=1;i<n;i++)
	{
		if (sum[i]==1)
			continue;
		if (sum[i]<1)
		{
			ans+=(1-sum[i]);
			sum[i+1]-=(1-sum[i]);
			sum[i]=1;
		}
		else
		{
			ans+=sum[i]-1;
			sum[i+1]+=(sum[i]-1);
			sum[i]=1;
		}
	}
	sort(a+1,a+n+1,cmp);
	any=a[(n/2)+1].y;
	for (int i=1;i<=n;i++)
		ans2+=(any>a[i].y?any-a[i].y:a[i].y-any);
	for (int i=1;i<n;i++)
	{
		if (sum2[i]==1)
			continue;
		if (sum2[i]<1)
		{
			ans2+=(1-sum2[i]);
			sum2[i+1]-=(1-sum2[i]);
			sum2[i]=1;
		}
		else
		{
			ans2+=sum2[i]-1;
			sum2[i+1]+=(sum2[i]-1);
			sum2[i]=1;
		}
	}
	printf("%lld\n",min(ans2,ans));
	return 0;
}

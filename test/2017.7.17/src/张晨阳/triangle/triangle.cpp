#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n;
int ans;
char c[111][111];
int num[31];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	bool firflag=1;
	n=read();
	for (int i=0;i<26;i++)num[i]=-1;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
		{
			char tc=getchar();while (tc<'A'||tc>'Z')tc=getchar();
			if (tc!='A') firflag=0;
			c[i][j]=tc;num[c[i][j]-'A']=0;
		}
	if (firflag)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
			{
				ans+=n-max(i,j)+min(i,j)-1+min(i-1,n-j)+min(n-i,j-1);
			}
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
			{
				ans+=min(i-1,min(j-1,n-i))+min(i-1,min(n-i,n-j))+min(j-1,min(n-j,i-1))+min(j-1,min(n-j,n-i));
			}
		printf("%d\n",ans);
		printf("A  %d\n",ans);
		return 0;
	}
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=n;j++)
		{
			if (i<n&&j<n)
			{
				for (int len=2;i+len-1<=n&&j+len-1<=n;len++)
				{
					bool is=1;
					for (int tx=i+len-1,ty=j;tx>=i,ty<=j+len-1;tx--,ty++)
						if (c[tx][ty]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++;num[c[i][j]-'A']++;
				}
			}
			if (i<n&&j>1)
			{
				for (int len=2;i+len-1<=n&&j-len+1>0;len++)
				{
					bool is=1;
					for (int tx=i+len-1,ty=j;tx>=i,ty>=j-len+1;tx--,ty--)
						if (c[tx][ty]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++;num[c[i][j]-'A']++;
				}
			}
			if (i>1&&j>1)
			{
				for (int len=2;i-len+1>0&&j-len+1>0;len++)
				{
					bool is=1;
					for (int tx=i-len+1,ty=j;tx<=i,ty>=j-len+1;tx++,ty--)
						if (c[tx][ty]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++;num[c[i][j]-'A']++;
				}
			}
			if (i>1&&j<n)
			{
				for (int len=2;i-len+1>0&&j+len-1<=n;len++)
				{
					bool is=1;
					for (int tx=i-len+1,ty=j;tx<=i,ty<=j+len-1;tx++,ty++)
						if (c[tx][ty]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++;num[c[i][j]-'A']++;
				}
			}
			if (i<n&&j>1&&j<n)
			{
				for (int len=2;i+len-1<=n&&j+len-1<=n&&j-len+1>=1;len++)
				{
					bool is=1;
					for (int ty=j-len+1;ty<=j+len-1;ty++)
						if (c[i+len-1][ty]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++,num[c[i][j]-'A']++;
				}
			}
			if (i>1&&j>1&&j<n)
			{
				for (int len=2;i-len+1>=1&&j+len-1<=n&&j-len+1>=1;len++)
				{
					bool is=1;
					for (int ty=j-len+1;ty<=j+len-1;ty++)
						if (c[i-len+1][ty]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++,num[c[i][j]-'A']++;
				}
			}
			if (i<n&&i>1&&j<n)
			{
				for (int len=2;i+len-1<=n&&j+len-1<=n&&i-len+1>=1;len++)
				{
					bool is=1;
					for (int tx=i-len+1;tx<=i+len-1;tx++)
						if (c[tx][j+len-1]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++,num[c[i][j]-'A']++;
				}
			}
			if (i<n&&i>1&&j>1)
			{
				for (int len=2;i+len-1<=n&&j-len+1>=1&&i-len+1>=1;len++)
				{
					bool is=1;
					for (int tx=i-len+1;tx<=i+len-1;tx++)
						if (c[tx][j-len+1]!=c[i][j]){is=0;break;}
					if (!is) break;
					ans++,num[c[i][j]-'A']++;
				}
			}
		}
	}
	printf("%d\n",ans);
	for (int i=0;i<26;i++)
		if (num[i]!=-1)
		{
			printf("%c  %d\n",(char)('A'+i),num[i]);
		}
	return 0;
}


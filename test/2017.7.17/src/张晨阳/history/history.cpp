#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,m,c,tim;
bool hap=1;
int fa[3011][1011];
struct node
{
	int id,st,fa,las;
}q[3000011];
int tot;
int head[300011];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

int find(int p,int x)
{
	if (fa[p][x]==x) return x;
	return fa[p][x]=find(p,fa[p][x]);
}

void merge(int p,int x,int y)
{
	int fx=find(p,x),fy=find(p,y);
	fa[p][fy]=fx;
}

int FIND(int x)
{
	if (q[x].fa==x) return x;
	return q[x].fa=FIND(q[x].fa);
}

void MERGE(int x,int y)
{
	int px=head[x],py=head[y];
	int mer=FIND(px),mx=FIND(py);
	if (q[mer].id==q[mx].id) return;
	q[++tot]=q[mx];q[tot].st=tim;q[tot].fa=mer;q[tot].las=head[q[mx].id];head[q[mx].id]=tot;
	if (mx!=py)
		q[++tot]=(node){y,tim,mer,head[y]},head[y]=tot;
}

int GETFA(int t,int x)
{
	int pos=head[x];
	while (q[pos].st>t) pos=q[pos].las;
	return FIND(pos);
}

int main()
{
	freopen("history.in","r",stdin);
	freopen("hostory.out","w",stdout);
	n=read(),m=read();
	if (n<=1000)
	{
		for (int i=0;i<n;i++) fa[0][i]=i;
		for (int z=1;z<=m;z++)
		{
			char tc=getchar();while (tc!='R'&&tc!='T'&&tc!='K') tc=getchar();
			if (tc=='K')
			{
				c=read();hap=1;continue;
			}
			if (tc=='R')
			{
				tim++;
				for (int i=0;i<n;i++)
					fa[tim][i]=fa[tim-1][i];
				int ta=read(),tb=read();
				if (hap)
					merge(tim,ta,tb);
				else
					merge(tim,(ta+c)%n,(tb+c)%n);
			}
			if (tc=='T')
			{
				hap=1;
				int ta=read(),tb=read(),tt=read();
				if (tt>=tim)
				{
					int nowx=find(tim,ta),nowy=find(tim,tb);
					if (nowx==nowy)
					{
						puts("Y");continue;
					}
					else
					{
						puts("N");hap=0;continue;
					}
				}
				else
				{
					int prex=find(tim-tt,ta),prey=find(tim-tt,tb);
					int nowx=find(tim,ta),nowy=find(tim,tb);
					if (nowx==nowy&&prex!=prey)
					{
						puts("Y");continue;
					}
					else
					{
						puts("N");hap=0;continue;
					}
				}
			}
		}
	}
	else
	{
		for (int i=0;i<n;i++)
			q[++tot]=(node){i,0,i+1,-1},head[i]=tot;
		for (int z=1;z<=m;z++)
		{
			char tc=getchar();while (tc!='R'&&tc!='T'&&tc!='K') tc=getchar();
			if (tc=='K')
			{
				c=read();hap=1;continue;
			}
			if (tc=='R')
			{
				tim++;
				int ta=read(),tb=read();
				if (!hap) ta=(ta+c)%n,tb=(tb+c)%n;
				MERGE(ta,tb);
				for (int i=0;i<n;i++)
					if (i!=ta&&i!=tb)
						if (q[q[head[i]].fa].id==q[q[head[tb]].fa].id)
						{
							q[++tot]=(node){i,tim,head[ta],head[i]};head[i]=tot;
						}
			}
			if (tc=='T')
			{
				hap=1;
				int ta=read(),tb=read(),tt=read();
				if (tt>=tim)
				{
					int nowx=FIND(head[ta]),nowy=FIND(head[tb]);
					if (q[nowx].id==q[nowy].id)
					{
						puts("Y");continue;
					}
					else
					{
						puts("N"),hap=0;continue;
					}
				}
				int prex=GETFA(tim-tt,ta),prey=GETFA(tim-tt,tb);
				int nowx=FIND(head[ta]),nowy=FIND(head[tb]);
				if (q[nowx].id==q[nowy].id&&q[prex].id!=q[prey].id)
				{
					puts("Y");continue;
				}
				else
				{
					puts("N");hap=0;continue;
				}
			}
		}
	}
	return 0;
}

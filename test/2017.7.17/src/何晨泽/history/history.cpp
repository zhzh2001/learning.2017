//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,v,t,x,y,z;
int f[300000],g[300000],h[300000];
char s[5];
bool ans;
namespace bye {
	inline int get(int x,int t) { while (f[x]>=0 && g[x]<=t) x=f[x]; return x; }
	inline void work(int x,int y,int t) {
		f[x]=y;
		g[x]=t;
		h[y]+=h[x]==h[y]?1:0;
	}
	inline void merge(int x,int y,int t) {
		x=get(x,t);
		y=get(y,t);
		if (x!=y) work(y,x,t);
	}
}
int main() {
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d",&n,&m);
	Rep(i,0,n-1) {
		h[i]=1;
		f[i]=-1;
	}
	while (m--) {
		scanf ("%s",s);
		if (*s=='K') {
			scanf ("%d",&v);
			ans=1;
		}
		else {
			scanf ("%d%d",&x,&y);
			if (*s=='R') {
				if (ans) bye::merge(x,y,++t); else bye::merge((x+v)%n,(y+v)%n,++t);
			}
			else {
				scanf ("%d",&z);
				int a,b;
				a=bye::get(x,t)==bye::get(y,t)?1:0;
				b=bye::get(x,t-z)==bye::get(y,t-z)?1:0;
				ans=a!=b?1:0;
				if (ans) cout<<"Y"<<endl; else cout<<"N"<<endl;
			}
		}
	}
	return 0;
}

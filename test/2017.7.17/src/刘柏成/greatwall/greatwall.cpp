#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=20000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int n;
ll calc1(int *x){
	int qwq=x[n/2+1];
	ll ans=0;
	for(int i=1;i<=n;i++)
		ans+=qwq>x[i]?qwq-x[i]:x[i]-qwq;
	return ans;
}
ll calc2(int *x){
	ll ans=0;
	for(int i=1;i<=n;i++)
		ans+=x[i]>i?x[i]-i:i-x[i];
	return ans;
}
const int maxn=600002;
int x[maxn],y[maxn];
int main(){
	init();
	n=readint();
	for(int i=1;i<=n;i++)
		x[i]=readint(),y[i]=readint();
	sort(x+1,x+n+1);
	sort(y+1,y+n+1);
	ll ans1=calc1(x)+calc2(y),ans2=calc1(y)+calc2(x);
	printf("%lld",min(ans1,ans2));
}
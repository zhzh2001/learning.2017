#include <bits/stdc++.h>
using namespace std;
int fa[3002][1002];
char s[10];
int getf(int o,int x){
	return fa[o][x]==x?x:fa[o][x]=getf(o,fa[o][x]);
}
int main(){
	freopen("history.in","r",stdin);
	freopen("history.ans","w",stdout);
	int n,m,yr=0;
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;i++)
		fa[0][i]=i;
	int c=0;
	bool angry=false;
	while(m--){
		int x;
		scanf("%s%d",s,&x);
		if (s[0]=='K'){
			c=x;
			angry=false;
		}else if (s[0]=='R'){
			int y;
			scanf("%d",&y);
			for(int i=0;i<n;i++)
				fa[yr+1][i]=fa[yr][i];
			yr++;
			if (angry){
				x=(x+c)%n;
				y=(y+c)%n;
			}
			fa[yr][getf(yr,y)]=getf(yr,x);
		}else{
			int y,t;
			scanf("%d%d",&y,&t);
			bool pst=(yr<=t)?x==y:(getf(yr-t,x)==getf(yr-t,y)),now=(getf(yr,x)==getf(yr,y)),flag=(!now || pst);
			if (flag)
				puts("N"),angry=true;
			else puts("Y"),angry=false;
		}
	}
}
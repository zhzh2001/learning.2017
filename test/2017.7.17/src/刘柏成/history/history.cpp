#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=20000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct query{
	char typ;
	int x,y;
}q[300002];
bool ans[300002];
int first[300002],nxt[300002],p[300002];
int cur;
void addlist(int u,int qwq){
	nxt[++cur]=first[u];
	p[cur]=qwq;
	first[u]=cur;
}
int fa[300002];
int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
int main(){
	init();
	int n=readint(),m=readint(),yr=0;
	for(int i=1;i<=m;i++){
		q[i].typ=readchar(),q[i].x=readint();
		if (q[i].typ!='K'){
			q[i].y=readint();
			if (q[i].typ=='T'){
				int t=readint();
				if (yr>t)
					addlist(yr-t,i);
				else ans[i]=q[i].x==q[i].y;
			}else
				yr++;
		}
	}
	yr=0;
	int c=0;
	bool angry=false;
	for(int i=0;i<n;i++)
		fa[i]=i;
	for(int i=1;i<=m;i++){
		if (q[i].typ=='K'){
			c=q[i].x;
			angry=false;
		}else if (q[i].typ=='R'){
			yr++;
			int x=angry?(q[i].x+c)%n:q[i].x,y=angry?(q[i].y+c)%n:q[i].y;
			fa[getf(x)]=getf(y);
			for(int j=first[yr];j;j=nxt[j]){
				query& qwq=q[p[j]];
				ans[p[j]]=getf(qwq.x)==getf(qwq.y);
			}
		}else{
			bool now=getf(q[i].x)==getf(q[i].y),flag=(!now || ans[i]);
			angry=flag;
			puts(flag?"N":"Y");
		}
	}
	//printf("%d\n",sizeof(ans)+sizeof(fa)+sizeof(q)+sizeof(first)+sizeof(nxt)+sizeof(p)+sizeof(buf));
}
#include <bits/stdc++.h>
using namespace std;
int dx[4]={1,-1,1,-1};
int dy[4]={-1,-1,1,1};
int rev[4]={1,0,3,2};
int nxt[4]={1,3,0,2};
int go[104][104][4],tat[104][104][4];
char s[104][104];
bool app[26];
int ans[26],sum;
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%s",s[i]+1);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			app[s[i][j]-'A']=true;
			int *qwq=go[i][j];
			for(int k=0;k<4;k++){
				int x=i,y=j;
				while(s[x][y]==s[i][j]){
					qwq[k]++;
					x+=dx[k];
					y+=dy[k];
				}
			}
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=0;k<4;k++){
				int x=i,y=j;
				char qwq=s[i][j];
				int mmax=0;
				while(s[x][j]==qwq && go[x][j][rev[k]]>mmax){
					mmax++;
					ans[qwq-'A']++;
					sum++;
					x+=dx[k];
					y+=dy[k];
				}
				tat[i][j][k]=mmax;
				sum--;
				ans[qwq-'A']--;
				//printf("%d %d %d %d\n",i,j,k,mmax);
			}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=0;k<4;k++){
				int qwq=min(tat[i][j][k],tat[i][j][nxt[k]]);
				//printf("%d %d %d %d\n",i,j,k,qwq);
				ans[s[i][j]-'A']+=qwq-1;
				sum+=qwq-1;
			}
	printf("%d\n",sum);
	for(int i=0;i<26;i++)
		if (app[i])
			printf("%c  %d\n",i+'A',ans[i]);
}
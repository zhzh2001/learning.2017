#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
int n,ans;bool b[128];
int anss[128];
char a[110][110];
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)scanf("%s",a[i]+1);
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j){
			b[a[i][j]]=1;bool flag=1;
			for(int k=2;k<=n;++k){
				if(i+k-1>n||j+k-1>n)break;
				for(int l=1;l<=k;l++)if(a[i+k-l][j+l-1]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(i-k+1<1||j+k-1>n)break;
				for(int l=1;l<=k;l++)if(a[i-k+l][j+l-1]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(i-k+1<1||j-k+1<1)break;
				for(int l=1;l<=k;l++)if(a[i-k+l][j-l+1]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(i+k-1>n||j-k+1<1)break;
				for(int l=1;l<=k;l++)if(a[i+k-l][j-l+1]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(j-k+1<1||j+k-1>n||i+k-1>n)break;
				for(int l=1;l<=k*2-1;l++)if(a[i+k-1][j-k+l]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(j-k+1<1||j+k-1>n||i-k+1<1)break;
				for(int l=1;l<=k*2-1;l++)if(a[i-k+1][j-k+l]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(i-k+1<1||i+k-1>n||j+k-1>n)break;
				for(int l=1;l<=k*2-1;l++)if(a[i-k+l][j+k-1]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
			flag=1;
			for(int k=2;k<=n;++k){
				if(i-k+1<1||i+k-1>n||j-k+1>n)break;
				for(int l=1;l<=k*2-1;l++)if(a[i-k+l][j-k+1]!=a[i][j]){flag=0;break;}
				if(!flag)break;ans++;anss[a[i][j]]++;
			}
		}
	writeln(ans);
	for(char i='A';i<='Z';i++)if(b[i]){
		putchar(i);putchar(' ');putchar(' ');
		writeln(anss[i]);
	}
	return 0;
}
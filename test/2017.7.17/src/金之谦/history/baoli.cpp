#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
struct bcj{
	int fa[1010];
	int getfather(int x){return fa[x]==x?x:fa[x]=getfather(fa[x]);}
}g[3010];
int n,m,c=0;
int main()
{
	freopen("history.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();int t=0;bool flag=0;
	for(int i=0;i<n;i++)g[0].fa[i]=i;
	for(int i=1;i<=m;i++){
		char s[5];scanf("%s",s);
		if(s[0]=='R'){
			int x=read(),y=read();
			if(flag)x=(x+c)%n,y=(y+c)%n;
			g[++t]=g[t-1];int fx=g[t].getfather(x),fy=g[t].getfather(y);
			if(fx!=fy)g[t].fa[fx]=fy;
		}if(s[0]=='K')c=read(),flag=0;
		if(s[0]=='T'){
			flag=0;
			int x=read(),y=read(),q=read();
			int fx=g[t].getfather(x),fy=g[t].getfather(y);
			if(fx==fy){
				int p=max(0,t-q);
				fx=g[p].getfather(x);fy=g[p].getfather(y);
				if(fx==fy)flag=1;
			}else flag=1;
			puts(flag?"N":"Y");
		}
	}
	return 0;
}
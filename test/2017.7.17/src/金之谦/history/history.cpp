#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
int fa[300010];
inline int getfather(int x){return fa[x]==x?x:fa[x]=getfather(fa[x]);}
int gs[300010],s[300010],x[300010],y[300010];
bool xw[300010]={0};
vector<int>xg[300010];
int n,m,c=0;
int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=read();m=read();int t=0;
	for(int i=0;i<n;i++)fa[i]=i;
	for(int i=1;i<=m;i++){
		char ss[5];scanf("%s",ss);
		if(ss[0]=='K')s[i]=1,x[i]=read();
		if(ss[0]=='R')s[i]=2,x[i]=read(),y[i]=read(),t++;
		if(ss[0]=='T'){
			s[i]=3;x[i]=read();y[i]=read();
			int p=max(0,t-read());xg[p].push_back(i);gs[p]++;
		}
	}bool flag=0;t=0;
	for(int i=1;i<=m;i++){
		if(s[i]==1)c=x[i],flag=0;
		if(s[i]==2){
			t++;int xx=x[i],yy=y[i];if(flag)xx=(xx+c)%n,yy=(yy+c)%n;
			int fx=getfather(xx),fy=getfather(yy);
			if(fx!=fy)fa[fx]=fy;
			for(int j=0;j<gs[t];j++){
				xx=x[xg[t][j]];yy=y[xg[t][j]];
				fx=getfather(xx);fy=getfather(yy);
				if(fx==fy)xw[xg[t][j]]=1;
			}
		}
		if(s[i]==3){
			flag=0;
			int xx=x[i],yy=y[i];
			if(xx==yy)flag=1;
			else{
				int fx=getfather(xx),fy=getfather(yy);
				if(fx==fy){if(xw[i])flag=1;}
				else flag=1;
			}
			puts(flag?"N":"Y");
		}
	}
	return 0;
}
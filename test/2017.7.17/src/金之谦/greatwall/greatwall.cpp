#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(ll x){write(x);puts("");}
ll n,x[600010],y[600010],xx[600010],yy[600010];
inline ll tkx(){
	ll ans=0,p=0,rp=0;
	for(int i=1;i<=n;i++){
		if(!xx[i])p--;
		if(xx[i]>1)p+=xx[i]-1;
		rp+=abs(p);
	}ans=rp;rp=0;p=0;
	for(int i=n;i;i--){
		if(!xx[i])p--;
		if(xx[i]>1)p+=xx[i]-1;
		rp+=abs(p);
	}return min(ans,rp);
}
inline ll tky(){
	ll ans=0,p=0,rp=0;
	for(int i=1;i<=n;i++){
		if(!yy[i])p--;
		if(yy[i]>1)p+=yy[i]-1;
		rp+=abs(p);
	}ans=rp;rp=0;p=0;
	for(int i=n;i;i--){
		if(!yy[i])p--;
		if(yy[i]>1)p+=yy[i]-1;
		rp+=abs(p);
	}return min(ans,rp);
}
int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)x[i]=read(),y[i]=read(),xx[x[i]]++,yy[y[i]]++;
	sort(x+1,x+n+1);sort(y+1,y+n+1);
	int zhong=(n+1)>>1;ll X=0,Y=0;
	for(int i=1;i<=n;i++){X+=abs(x[i]-x[zhong]);Y+=abs(y[i]-y[zhong]);}
	X+=tky();Y+=tkx();
	writeln(min(X,Y));
	return 0;
}
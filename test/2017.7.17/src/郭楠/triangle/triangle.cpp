#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 105
#define M 105
using namespace std;
int a[N][N],up[N][N],down[N][N],ans[M],l[N][N],r[N][N],n,m,sum,x,y;
bool ifin[M];
char s[N];
int dx[8]={1,-1,-1,1,1,0,-1,0};
int dy[8]={0,0,0,0,0,1,0,-1};
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	int i,j,k,o,p,i1;
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	 {
	 	scanf("%s",s);
	 	for(j=1;j<=n;j++) a[i][j]=s[j-1],ifin[s[j-1]]=true;
	 }
	for(i=1;i<=n;i++) up[1][i]=down[n][i]=1;
	for(i=2;i<=n;i++)
	  for(j=1;j<=n;j++)
	    if(a[i][j]==a[i-1][j]) up[i][j]=up[i-1][j]+1;else up[i][j]=1;
	for(i=n-1;i>=1;i--)
	  for(j=1;j<=n;j++)
	    if(a[i][j]==a[i+1][j]) down[i][j]=down[i+1][j]+1;else down[i][j]=1;
	for(i=1;i<=n;i++) l[i][1]=r[i][n]=1;
	for(i=2;i<=n;i++)
	  for(j=1;j<=n;j++)
	    if(a[j][i]==a[j][i-1]) l[j][i]=l[j][i-1]+1;else l[j][i]=1;
	for(i=n-1;i>=1;i--)
	  for(j=1;j<=n;j++)
	    if(a[j][i]==a[j][i+1]) r[j][i]=r[j][i+1]+1;else r[j][i]=1;
	for(i=1;i<=n;i++)
	  for(j=1;j<=n;j++)
	    {
	        sum=1;
	        x=i;y=j;
	        o=r[i][j];
	        while(1)
	         {
	         	x+=dx[0];y+=dy[0];
	         	if(a[x][y]!=a[i][j]) break;
	         	o=min(o,sum+r[x][y]);
	         	if(sum>=o) break;
	         	sum++;
	         }
	        ans[a[i][j]]+=sum-1;
	        sum=1;
	        x=i;y=j;
	        o=r[i][j];
	        while(1)
	         {
	         	x+=dx[1];y+=dy[1];
	         	if(a[x][y]!=a[i][j]) break;
	         	o=min(o,sum+r[x][y]);
	         	if(sum>=o) break;
	         	sum++;
	         }
	        ans[a[i][j]]+=sum-1;
	        sum=1;
	        x=i;y=j;
	        o=l[i][j];
	        while(1)
	         {
	         	x+=dx[2];y+=dy[2];
	         	if(a[x][y]!=a[i][j]) break;
	         	o=min(o,sum+l[x][y]);
	         	if(sum>=o) break;
	         	sum++;
	         }
	        ans[a[i][j]]+=sum-1;
	    	sum=1;
	        x=i;y=j;
	        o=l[i][j];
	        while(1)
	         {
	         	x+=dx[3];y+=dy[3];
	         	if(a[x][y]!=a[i][j]) break;
	         	o=min(o,sum+l[x][y]);
	         	if(sum>=o) break;
	         	sum++;
	         }
	        ans[a[i][j]]+=sum-1;
			sum=1;
	    	x=i;y=j;
	    	while(1)
	    	  {
	    	  	x+=dx[4];y+=dy[4];
	    	  	if(a[x][y]!=a[i][j]) break;
	    	  	if(min(l[x][y],r[x][y])>sum) sum++;else break;
	    	  }
	    	ans[a[i][j]]+=sum-1;
	    	sum=1;
	    	x=i;y=j;
	    	while(1)
	    	  {
	    	  	x+=dx[5];y+=dy[5];
	    	  	if(a[x][y]!=a[i][j]) break;
	    	  	if(min(up[x][y],down[x][y])>sum) sum++;else break;
	    	  }
	    	ans[a[i][j]]+=sum-1;
	    	sum=1;
	    	x=i;y=j;
	    	while(1)
	    	  {
	    	  	x+=dx[6];y+=dy[6];
	    	  	if(a[x][y]!=a[i][j]) break;
	    	  	if(min(l[x][y],r[x][y])>sum) sum++;else break;
	    	  }
	    	ans[a[i][j]]+=sum-1;
	    	sum=1;
	    	x=i;y=j;
	    	while(1)
	    	  {
	    	  	x+=dx[7];y+=dy[7];
	    	  	if(a[x][y]!=a[i][j]) break;
	    	  	if(min(up[x][y],down[x][y])>sum) sum++;else break;
	    	  }
	    	ans[a[i][j]]+=sum-1;
	    }
	sum=0;
	for(i='A';i<='Z';i++) sum+=ans[i];
	printf("%d\n",sum);
	for(i='A';i<='Z';i++)
	  if(ifin[i]) 
	   {
	   	putchar(i);printf("  %d\n",ans[i]);
	   }
}

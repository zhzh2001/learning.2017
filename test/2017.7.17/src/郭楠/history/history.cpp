#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 300005
using namespace std;
int read()
{int t=0;char c;
   c=getchar();
   while(!(c>='0' && c<='9')) c=getchar();
   while(c>='0' && c<='9')
     {
     	t=t*10+c-48;
     	c=getchar();
     }
    return t;
}
int year,n,m,c,o,p,v,t,x,y,num,l;
int pre[N];
struct node{char s[4];int x,y,z;};
struct ASK{int ye,ans,t,x,y;};
ASK ask[N];
node s[N];
bool ans[N][2];
bool cmp(ASK a,ASK b)
{
	return a.ye<b.ye;
}
int find(int x)
{int t;
	if(pre[x]==x) t=x;
	  else t=find(pre[x]);
	pre[x]=t;
	return t;
}
int main()
{
	int i,j,k;
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=read();m=read();
	year=0;
	for(i=1;i<=m;i++)
	 {
	 	scanf("%s",s[i].s);
	 	if(s[i].s[0]=='R')
	 	 {
	 	 	s[i].x=read();s[i].y=read();year++;
	 	 }
	 	if(s[i].s[0]=='K')
	 	 {
	 	 	s[i].x=read();
	 	 }
	 	if(s[i].s[0]=='T')
	 	 {
	 	 	s[i].x=read();s[i].y=read();s[i].z=read();
	 	 	num++;ask[num].ye=year;ask[num].ans=i;ask[num].t=0;ask[num].x=s[i].x;ask[num].y=s[i].y;
	 	 	num++;ask[num].ye=max(0,year-s[i].z);ask[num].ans=i;ask[num].t=1;ask[num].x=s[i].x;ask[num].y=s[i].y;
	 	 }
	 }
	 sort(ask+1,ask+num+1,cmp);
	 year=0;
	 for(i=0;i<n;i++) pre[i]=i;
	 l=1;
	 while(l<=num && ask[l].ye==year)
	  {
	  	int fx=find(ask[l].x),fy=find(ask[l].y);
	  	ans[ask[l].ans][ask[l].t]=fx==fy;
	  	l++;
	  }
	 v=0;c=0;
	 for(int i1=1;i1<=m;i1++)
	   {
	   	 if(s[i1].s[0]=='R')
	   	  {
	   	  	x=(s[i1].x+v)%n;y=(s[i1].y+v)%n;
	   	  	int fx=find(x),fy=find(y);
	   	  	if(fx!=fy) pre[fx]=fy;
	   	  	year++;
	   	  	while(l<=num && ask[l].ye==year)
	        	{
	  	     	 int fx=find(ask[l].x),fy=find(ask[l].y);
	  			 ans[ask[l].ans][ask[l].t]=fx==fy;
	  			 l++;
	  			}
	  	     
	   	  }
	   	 if(s[i1].s[0]=='K')
	   	  {
	   	  	c=s[i1].x;v=0;
	   	  }
	   	 if(s[i1].s[0]=='T')
		  {
		  	if((ans[i1][0])&&(!ans[i1][1]))
		  	  {
		  	  	printf("Y\n");v=0;
		  	  }
		  	  else
		  	   {
		  	   	printf("N\n");v=c;
		  	   }
		  } 
	   }
}

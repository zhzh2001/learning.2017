#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 600005
#define ll long long
using namespace std;
int read()
{int t=0;char c;
   c=getchar();
   while(!(c>='0' && c<='9')) c=getchar();
   while(c>='0' && c<='9')
     {
     	t=t*10+c-48;
     	c=getchar();
     }
    return t;
}
struct node{int x,y;};
node a[N];
int n;
bool cmp1(node a,node b)
{
	return a.x<b.x;
}
bool cmp2(node a,node b)
{
	return a.y<b.y;
}
ll lsum[N],rsum[N],l,usum[N],dsum[N],ans1=0,ans2=0,min1=0,min2=0,sum;
int main()
{
    int i,j,k;
    freopen("greatwall.in","r",stdin);
    freopen("greatwall.out","w",stdout);
    n=read();
    for(i=1;i<=n;i++) a[i].x=read(),a[i].y=read();
    sort(a+1,a+n+1,cmp1);
    for(i=1;i<=n;i++) ans1+=abs(a[i].x-i);
    l=1;sum=0;
    for(i=1;i<=n;i++)
      {
      	while(l<=n && a[l].x<i) sum++,l++;
      	usum[i]=usum[i-1]+sum;
      }
    sum=0;l=n;
    for(i=n;i>=1;i--)
      {
      	while(l>=1 && a[l].x>i) sum++,l--;
      	dsum[i]=dsum[i+1]+sum;
      }
    min2=usum[1]+dsum[1];
    for(i=2;i<=n;i++) min2=min(min2,usum[i]+dsum[i]);
    sort(a+1,a+n+1,cmp2);
    for(i=1;i<=n;i++) ans2+=abs(a[i].y-i);
    l=1;sum=0;
    for(i=1;i<=n;i++)
      {
      	while(l<=n && a[l].y<i) sum++,l++;
      	lsum[i]=lsum[i-1]+sum;
      }
    sum=0;l=n;
    for(i=n;i>=1;i--)
      {
      	while(l>=1 && a[l].y>i) sum++,l--;
      	rsum[i]=rsum[i+1]+sum;
      }
    min1=lsum[1]+rsum[1];
    for(i=2;i<=n;i++) min1=min(min1,lsum[i]+rsum[i]);
    printf("%I64d\n",min(min1+ans1,min2+ans2));
}

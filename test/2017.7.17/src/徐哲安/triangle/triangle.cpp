#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;

const int maxn = 109;
char s[maxn][maxn];
int f[maxn][maxn];
int g[maxn][maxn];
LL ans[maxn], res = 0;
bool flag[maxn];
int n;

int main(){
	freopen("triangle.in", "r", stdin);
	freopen("triangle.out", "w", stdout);
	
	scanf("%d", &n);
	for (int i=1; i<=n; i++){
		scanf("%s", s[i]+1);
		for (int j=1; j<=n; j++)
			flag[s[i][j]-'A']++;
	}
	
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++){
			f[i][j] = 0;
			if (i-1 >= 1 && j-1 >= 1)
				if (s[i][j] == s[i-1][j] && s[i][j] == s[i][j-1])
					f[i][j] = min(f[i-1][j], f[i][j-1]) + 1;
			g[i][j] = 0;
			if (i-1 >= 1 && j-1 >= 1 && j+1 <= n)
				if (s[i][j] == s[i-1][j-1] && s[i][j] == s[i-1][j] && s[i][j] == s[i-1][j+1])
					g[i][j] = min(g[i-1][j-1], min(g[i-1][j], g[i-1][j+1])) + 1;
			ans[s[i][j]-'A'] += f[i][j] + g[i][j]; 
		}
		
	for (int j=n; j>=1; j--)
		for (int i=1; i<=n; i++){
			f[i][j] = 0;
			if (j+1 <= n && i-1 >= 1)
				if (s[i][j] == s[i-1][j] && s[i][j] == s[i][j+1])
					f[i][j] = min(f[i-1][j], f[i][j+1]) + 1;
			g[i][j] = 0;
			if (j+1 <= n && i-1 >=1 && i+1 <= n)
				if (s[i][j] == s[i-1][j+1] && s[i][j] == s[i][j+1] && s[i][j] == s[i+1][j+1])
					g[i][j] = min(g[i-1][j+1], min(g[i][j+1], g[i+1][j+1])) + 1;
			ans[s[i][j]-'A'] += f[i][j] + g[i][j]; 
		}
	
	for (int i=n; i>=1; i--)
		for (int j=n; j>=1; j--){
			f[i][j] = 0;
			if (i+1 <= n && j+1 <= n)
				if (s[i][j] == s[i+1][j] && s[i][j] == s[i][j+1])
					f[i][j] = min(f[i+1][j], f[i][j+1]) + 1;
			g[i][j] = 0;
			if (i+1 <= n && j-1 >=1 && j+1 <= n)
				if (s[i][j] == s[i+1][j-1] && s[i][j] == s[i+1][j] && s[i][j] == s[i+1][j+1])
					g[i][j] = min(g[i+1][j-1], min(g[i+1][j], g[i+1][j+1])) + 1;
			ans[s[i][j]-'A'] += f[i][j] + g[i][j]; 
		}
		
	for (int j=1; j<=n; j++)
		for (int i=n; i>=1; i--){
			f[i][j] = 0;
			if (i+1 <= n && j-1 >= 1)
				if (s[i][j] == s[i+1][j] && s[i][j] == s[i][j-1])
					f[i][j] = min(f[i+1][j] , f[i][j-1]) + 1;
			g[i][j] = 0;
			if (j-1 >= 1 && i-1 >= 1 && i+1 <= n)
				if (s[i][j] == s[i-1][j-1] && s[i][j] == s[i][j-1] && s[i][j] == s[i+1][j-1])
					g[i][j] = min(g[i-1][j-1], min(g[i][j-1], g[i+1][j-1])) + 1;
			ans[s[i][j]-'A'] += f[i][j] + g[i][j]; 
		}
		
	for (int i=0; i<26; i++) res += ans[i];
	printf("%lld\n", res);
	for (int i=0; i<26; i++)
		if (flag[i]) printf("%c  %lld\n", 'A'+i, ans[i]);
		
	return 0;
}

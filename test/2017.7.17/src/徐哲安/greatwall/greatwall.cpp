#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;

const int maxn = 1000009;
struct chess{ int x, y; } a[maxn];
LL total, s, base, ans = 1e18;
int n, cnt[maxn], need;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

inline bool cmpx(chess a, chess b){ return a.x < b.x; }
inline bool cmpy(chess a, chess b){ return a.y < b.y; }

void solve1(){
	memset(cnt, 0, sizeof cnt); total = 0;
	for (int i=1; i<=n; i++) { cnt[a[i].y]++; total += a[i].x; }
	for (int i=1; i<=n; i++)
		if (cnt[i] >=2){
			while (cnt[i] >= 2){
				if (need >= 1) base += i;
				else base -= i;
				cnt[i]--; need--; 
			}
		}
		else if (cnt[i] == 0){
			if (need <= -1) base += i;
			else base -= i;
			cnt[i]++; need++;
		}
	sort(a+1, a+n+1, cmpx); 
	int l = 0, r = 0;
	while (r < n){
		l = r = r+1;
		while (r < n && a[r+1].x == a[l].x) r++;
		s += 1LL * (r-l+1) * a[l].x;
		ans = min(ans, base+(1LL*r*a[l].x-s)+(total-s-1LL*(n-r)*a[l].x));
	}
}

void solve2(){
	memset(cnt, 0, sizeof cnt); total = 0; base = 0;
	for (int i=1; i<=n; i++) { cnt[a[i].x]++; total += a[i].y; }
	for (int i=1; i<=n; i++)
		if (cnt[i] >=2){
			while (cnt[i] >= 2){
				if (need >= 1) base += i;
				else base -= i;
				cnt[i]--; need--; 
			}
		}
		else if (cnt[i] == 0){
			if (need <= -1) base += i;
			else base -= i;
			cnt[i]++; need++;
		}
	sort(a+1, a+n+1, cmpy); 
	int l = 0, r = 0; s = 0;
	while (r < n){
		l = r = r+1;
		while (r < n && a[r+1].y == a[l].y) r++;
		s += 1LL * (r-l+1) * a[l].y;
		ans = min(ans, base+(1LL*r*a[l].y-s)+(total-s-1LL*(n-r)*a[l].y));
	}
}

int main(){
	freopen("greatwall.in", "r", stdin);
	freopen("greatwall.out", "w", stdout);
	n = read();
	for (int i=1; i<=n; i++){ a[i].x = read(); a[i].y = read(); }
	solve1(); solve2(); 
	printf("%lld\n", ans);
	return 0;
}

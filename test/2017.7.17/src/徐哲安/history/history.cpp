#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;

const int maxn = 300009;
struct ask{ int a, b, p; } tmp; vector<ask> Q[maxn];
struct change{ int type, year, a, b, c; } x[maxn];
int n, m, y, c, pa[maxn], ans[maxn], flag; char s[5];

int getpa(int x){
	return pa[x] == x ? x : pa[x] = getpa(pa[x]);
}

int main(){
	freopen("history.in", "r", stdin);
	freopen("history.out", "w", stdout);
	
	scanf("%d%d", &n, &m);
	for (int i=1; i<=m; i++){
		scanf("%s", &s);
		if (s[0] == 'K'){
			x[i].type = 0; x[i].year = y;
			scanf("%d", &x[i].a);
		}
		else if (s[0] == 'R'){
			x[i].type = 1; x[i].year = ++y;
			scanf("%d%d", &x[i].a, &x[i].b);
		}
		else{
			x[i].type = 2; x[i].year = y;
			scanf("%d%d%d", &x[i].a, &x[i].b, &x[i].c);
			tmp.a = x[i].a; tmp.b = x[i].b; tmp.p = i;
			if (y-x[i].c > 0) Q[y-x[i].c].push_back(tmp);
		}
	}
	
	c = flag = 0;
	for (int i=0; i<n; i++) pa[i] = i;
	for (int i=1; i<=m; i++)
		if (x[i].type == 0){
			c = x[i].a; flag = false;
		}
		else if (x[i].type == 1){
			if (flag) { x[i].a = (x[i].a+c)%n; x[i].b = (x[i].b+c)%n; }
			pa[getpa(x[i].a)] = getpa(x[i].b);
			for (int j=Q[x[i].year].size()-1; j>=0; j--)
				ans[Q[x[i].year][j].p] = (getpa(Q[x[i].year][j].a) == getpa(Q[x[i].year][j].b));
		}
		else{
			flag = (ans[i] || getpa(x[i].a) != getpa(x[i].b));
			puts(flag ? "N" : "Y");
		}
		
	return 0;
}

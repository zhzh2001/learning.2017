#include<string.h>
#include<cstdio>
const int N=1e3+10;
int yr,i,n,m,fury,an,f[N][N*10],x,y,t,fx,fy;
int p;char c;
inline int I(){
	p=0;c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')p=p*10+c-'0',c=getchar();
	return p;
}
inline int g(int yr,int x){
	if(f[yr][x]!=x)return f[yr][x]=g(yr,f[yr][x]);
	return f[yr][x];
}
int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=I(),m=I();fury=0;
	for(i=0;i<n;i++)f[0][i]=i;
	yr=0;
	while(m--){
		c=getchar();
		if(c=='K')fury=0,an=I();
		else if(c=='R'){
			if(fury)x=I()+an%n,y=I()+an%n;
				else x=I(),y=I();
			yr++;
			memcpy(f[yr],f[yr-1],sizeof(f[yr]));
			x=g(yr,x),y=g(yr,y);
			if(x!=y)f[yr][x]=y;
		}else if(c=='T'){
			x=I(),y=I(),t=I();
			if(yr<=t)goto Satisfy;
			if(!t)goto Mad;
			fx=g(yr-t,x),fy=g(yr-t,y);
			x=g(yr,x),y=g(yr,y);
			if(x==y&&fx!=fy)goto Satisfy;
				else goto Mad;
			Satisfy:fury=0,puts("Y");continue;
			Mad:fury=1;puts("N");
		}
	}
}

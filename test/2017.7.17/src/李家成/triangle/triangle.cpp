#include<algorithm>
#include<cstdio>
using namespace std;
const int d[4][2]={-1,1,1,1,1,-1,-1,-1};
int n,i,j,ans,tot[50],r[110][110][110][4],avb[30];
char s[110][110];
inline void Js(int x,int y,char c,int l,int w,int &f){
	x+=d[w][0],y+=d[w][1];l--;
	for(int i=0;i<l;i++)
			if(s[x+i*d[w][0]][y+(l-1-i)*d[w][1]]!=c){
				f=0;return;
			}
	tot[c-'A']++,r[x][y][l+1][w]=1;
}

inline void Split(int x,int y,char c){
	int f;
	for(int w=0;w<4;w++){
		f=1;
		for(int i=1;y+i*d[w][1]<=n&&y+i*d[w][1]>0&&
				x+i*d[w][0]>0&&x+i*d[w][0]<=n&&f;i++)
			if(s[x][y+i*d[w][1]]==c&&s[x+i*d[w][0]][y]==c&&f)
				Js(x,y,c,i,w,f);
			else break;
	}
	for(int i=1;i<=min(x-1,min(n-y,y-1));i++)//u
		if(s[x][y-i]==c&&s[x][y+i]==c){
			if(r[x][y][i][1]&&r[x][y][i][4])tot[c-'A']++;
			else break;
		}else break;
	for(int i=1;i<=min(n-x,min(n-y,y-1));i++)//d
		if(s[x][y-i]==c&&s[x][y+i]==c){
			if(r[x][y][i][2]&&r[x][y][i][3])tot[c-'A']++;
			else break;
		}else break;
	for(int i=1;i<=min(min(x-1,n-x),y-1);i++)//l
		if(s[x-i][y]==c&&s[x+i][y]==c){
			if(r[x][y][i][4]&&r[x][y][i][3])tot[c-'A']++;
			else break;
		}else break;
	for(int i=1;i<=min(min(x-1,n-x),n-y);i++)//r
		if(s[x-i][y]==c&&s[x+i][y]==c){
			if(r[x][y][i][1]&&r[x][y][i][2])tot[c-'A']++;
			else break;
		}else break;
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d\n",&n);
	for(i=1;i<=n;i++)
		scanf("%s\n",s[i]+1);
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
			Split(i,j,s[i][j]),avb[s[i][j]-'A']=1;
	for(i=0;i<26;i++)
		if(avb[i])ans+=tot[i];
	printf("%d\n",ans);
	for(i=0;i<26;i++)
		if(avb[i])printf("%c  %d\n",i+'A',tot[i]);
}

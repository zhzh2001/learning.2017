#include<algorithm>
#include<cstdio>
using namespace std;
const int N=6e5+10;
#define Nl long long
Nl n,i,loc;
Nl tmp,tx[N],ty[N],ans,totx,toty,mx,my;
Nl x;char c;
struct S{
	Nl x,y;
	inline int operator<(const S&a)
		const{return x<a.x||x==a.x&&y<a.y;}
}a[N];
struct T{
	Nl x,y;
	inline int operator<(const T&a)
		const{return y<a.y||y==a.y&&x<a.x;}
}b[N];
inline Nl I(){
	x=0,c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
int main()
{
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=I();
	for(i=1;i<=n;i++){
		a[i].x=I(),a[i].y=I();
		b[i].x=a[i].x;
		b[i].y=a[i].y;
	}
	sort(a+1,a+1+n);
	sort(b+1,b+1+n);
	for(i=1;i<=n;i++)
		tx[i]=tx[i-1]+a[i].x,totx+=abs(i-a[i].x),
		ty[i]=ty[i-1]+b[i].y,toty+=abs(i-b[i].y);
	mx=my=1ll*n*n;
	for(i=1;i<=n;i++){
		loc=upper_bound(a+1,a+1+n,(S){i,n+1})-a-1;
		tmp=loc*i-tx[loc];
		tmp+=tx[n]-tx[loc]-(n-loc)*i;
		if(tmp<mx)mx=tmp;
	}
	for(i=1;i<=n;i++){
		loc=upper_bound(b+1,b+1+n,(T){n+1,i})-b-1;
		tmp=loc*i-ty[loc];
		tmp+=ty[n]-ty[loc]-(n-loc)*i;
		if(tmp<my)my=tmp;
	}printf("%I64d\n",min(mx+toty,my+totx));
}

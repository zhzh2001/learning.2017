#include<cstring>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdio>
const int maxn= 105;
int g[maxn][maxn][26],f[maxn][maxn][26];
int a[maxn][maxn],ans[26],n;
bool appear[26];
/*XZADLAK*/
void init(){
	memset(f,0,sizeof(f));
	for (int k=0;k<26;k++){
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				f[i][j][k] = f[i-1][j-1][k] + (a[i][j] == k);
//				if (f[i][j][k]) printf("f(%d,%d,%d)=%d\n",i,j,k,f[i][j][k]);
			}
		}
	}
	memset(g,0,sizeof(g));
	for (int k=0;k<26;k++){
		for (int i=n;i>=1;i--){
			for (int j=1;j<=n;j++){
				g[i][j][k] = g[i+1][j-1][k] + (a[i][j] == k);
//				if (g[i][j][k]) printf("g(%d,%d,%d)=%d\n",i,j,k,g[i][j][k]);
			}
		}
	}
}

void write(){
	int ANS = 0;
	for (int i=0;i<26;i++) ANS+=ans[i];
	printf("%d\n",ANS);
	for (int i=0;i<26;i++){
		if (!appear[i]) continue;
		printf("%c  %d\n",i+'A',ans[i]);
	}
}

void solve(){
	/*
		****
		***
		**
		*			*/ 
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
				int k = a[i][j];
				int rec = 1;
				while (g[i][j + rec][k] - g[i+rec+1][j-1][k] == (rec + 1)) rec++;
				ans[k]+=rec - 1;
		}//枚举端点 
	}
	/*
		*
		**
		***
		**
		*	*/
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
				int k = a[i][j];
				int rec = 1;
				while ((f[i][j+rec][k] - f[i-rec-1][j-1][k] == (rec + 1)) && (g[i][j+rec][k] - g[i+rec+1][j-1][k] == (rec + 1))) rec++;
				ans[k]+=rec-1;
		}
	}	
}

void xz(){
	int temp[maxn][maxn];
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			temp[i][j] = a[n+1-j][i];
//	puts("");
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			a[i][j] = temp[i][j];
//			printf("%c",a[i][j]+'A');
		}
//		puts("");
	}
//	puts("");
		
}

int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d",&n);
	memset(appear,false,sizeof(appear));
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			char c=getchar();
			if (!((c>='A') && (c<='Z'))) c=getchar();
			a[i][j] = c - 'A';
			appear[a[i][j]] = true;
		}
	}
	/*
	首先预处理
	得到f[i,j,k]表示 从(i,j)斜向左上有多少个k（包括(i,j)) 
		g[i,j,k]表示 从(i,j)斜向左下有多少个k（包括(i,j)) 
		考虑不同的等腰直角三角形
		**
		*
		和 
		考虑第二种
		*
		**
		*
		剩下的情况可以通过矩阵四遍翻转得到 
	*/
	
	init();
	solve();
	
	xz();
	init();
	solve();
	
	xz();
	init();
	solve();
	
	xz();
	init();
	solve();
	write();
	fclose(stdin);
	fclose(stdout);
	return 0;
	/*ZYYDLAK*/
}
/*
5
BBABB
BAAAB
AAAAA
BBBBB
BBBBB
 *
**
有 
*/







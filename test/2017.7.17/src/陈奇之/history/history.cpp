#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const int maxn = 6*1e5 + 100;
struct Q{
	int i,nxt;
}question[maxn];
int first[maxn],num;
bool Check[maxn];

int n,m;
struct F{
	int v,x,y,st,ed,t;
	char type;
}f[maxn];
/*CLJDLAK*/
int father[maxn];
int getfather(int x){
	if (father[x] == x) return x;
	father[x] = getfather(father[x]);
	return father[x];
}

void Union(int a,int b){
	father[getfather(a)] = getfather(b);
}

void add(int year,int x){
	num++;
	question[num] . i = x;
	question[num] . nxt = first[year];
	first[year] = num;
}
int read(){
	int a = 0;
	char c=getchar();
	while (!((c>='0') && (c<='9'))) c=getchar();
	while ((c>='0') && (c<='9')){
		a=a*10+c-48;
		c=getchar();
	}
	return a;
}
void init(){
	int year;
	year = 0;num = 0 ;
	memset(first,0,sizeof(first));
	for (int i=1;i<=m;i++){
		char type = getchar();
		while ((type != 'R') && (type != 'T') && (type != 'K')) type = getchar();
		if (type == 'R'){
			year ++;
			f[i] . type = type;
			f[i] . x = read();
			f[i] . y = read();
		} else
		if (type == 'T'){
			f[i] . type = type;
			f[i] . st = read();
			f[i] . ed = read();
			f[i] . t  = read();
			add(max(0,year - f[i].t),i);
		} else
		if (type == 'K'){
			f[i] . type = type;
			f[i] . v  = read();
		}
	}
}

void solve_problem(int year){
	for (int x=first[year];x;x=question[x].nxt){
		int i = question[x] . i;
		if (getfather(f[i].st) == getfather(f[i].ed)) Check[i] = true; 
											else 	  Check[i] = false;
	}
}

void solve(){
	for (int i=0;i<n;i++) father[i] = i;
	int year = 0;
	solve_problem(year);
	bool angry = false;
	int c = 0;
	for (int i=1;i<=m;i++){
		char type = f[i] . type;
		int x,y,st,ed;
		if (type == 'R'){
			if (angry){
				x = (f[i] .x + c) % n;
				y = (f[i] .y + c) % n; 
			} else{
				x = f[i] .x;
				y = f[i] .y;
			}
//			printf("(%d-->%d) = (%d --> %d)\n",f[i].x,f[i].y,x,y);
			Union(x,y);
			year++;
			solve_problem(year);
		} else
		if (type == 'K'){
			angry = false;
			c = f[i].v;
		} else
		if (type == 'T'){
			int u = f[i].st,v = f[i].ed;
			if ((!Check[i]) && (getfather(u) == getfather(v)))	angry = false;//之前是不连通的，现在联通了 
														   else angry = true;
			if (angry) puts("N"); else puts("Y");
		}
	}
}

int main(){
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=read();m=read();
	init();
	solve();
	fclose(stdin);
	fclose(stdout);
	return 0;
	/*LZHDLAK*/
}
/*
3 7
R 0 1
T 0 1 1
K 1
R 0 1
T 0 1 1
R 0 1
T 0 2 1
*/

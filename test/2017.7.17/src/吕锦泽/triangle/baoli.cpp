#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 105
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,c,flag,now;
ll fa[3005][1005];
ll find(ll f[],ll x){
	return f[x]==x?x:f[x]=find(f,f[x]);
}
bool check(ll t,ll x,ll y){
	ll p=find(fa[t],x),q=find(fa[t],y);
	return p==q;
}
void add(ll t,ll x,ll y){
	rep(i,0,n) fa[t][i]=fa[t-1][i];
	ll p=find(fa[t],x),q=find(fa[t],y);
	if (p!=q) fa[t][p]=q;
}
int main(){
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	n=read(); m=read();
	rep(i,0,n) fa[0][i]=i;
	rep(i,1,m){
		char opt[5];
		scanf("%s",opt);
		if (opt[0]=='K') c=read(),flag=0;
		else if (opt[0]=='T'){
			flag=0;
			ll st=read(),ed=read(),t=read();
			if (check(now-t,st,ed)) flag=1;
			if (flag) puts("N"); else puts("Y");
		}else{
			ll x=read(),y=read();
			if (flag){
				if (x+c>=n) x=x+c-n; else x+=c;
				if (y+c>=n) y=y+c-n; else y+=c;
			}
			++now; add(now,x,y);
		}
	}
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=3e5+7;
vector<int> f[N];
struct node{
	int opt,u,v,k;
}a[N];
char s[15];
int n,m,fa[N];

int find(int x){ return x==fa[x]?x:fa[x]=find(fa[x]); }

int main(){
	// say hello

	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);

	n=read(),m=read();
	int now=0;
	For(i,1,m){
		scanf("%s",s);
		if (s[0]=='R'){
			a[i].opt=1;now++;
			a[i].u=read(),a[i].v=read();
		}
		if (s[0]=='T'){
			a[i].opt=2;
			a[i].u=read(),a[i].v=read();
			a[i].k=0;
			int t=read();
			if (now-t>=0) f[now-t].push_back(i);
			else if (a[i].u!=a[i].v) a[i].k=1; else a[i].k=0;
		}
		if (s[0]=='K'){
			a[i].opt=3; 
			a[i].k=read();
		}
	}
	now=0;int flag=0,c=0;
	For(i,0,n-1) fa[i]=i;
	For(q,1,m){
		if (a[q].opt==1){
			int len=f[now].size();
			For(i,0,len-1){
				int u=a[f[now][i]].u,v=a[f[now][i]].v;
				u=find(u),v=find(v);
				if (u!=v) a[f[now][i]].k=1;
			}
			now++;
			int u=a[q].u,v=a[q].v;
			if (flag) u=(u+n-c)%n,v=(v+n-c)%n;
			u=find(u),v=find(v);
			fa[u]=v;
		}
		if (a[q].opt==2){
			flag=0;
			int u=find(a[q].u),v=find(a[q].v);
			if (u==v&&a[q].k) printf("Y\n");
			else {
				flag=1;
				printf("N\n");
			}
		}
		if (a[q].opt==3){
			flag=0;
			c=a[q].k;
		}
	}

	// say goodbye
}


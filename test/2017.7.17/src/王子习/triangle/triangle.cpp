#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=207;
int n,a[N][N],ans,res[N],Cd[N];
char s[N];
int Skx[7]={0,0,0,0,0,-1,1},Sky[7]={0,-1,1,-1,1,-1,1};
int Kx[7]={0,-1,-1,1,1,0,0},Ky[7]={0,1,-1,1,-1,1,-1};
inline int check(int x){ if (x>n||x<1) return 0;return 1; } 
void solve(int Sx,int Sy,int k){
	int x1=Sx+Skx[k],y1=Sy+Sky[k],len=2;
	if (k>4) len=3;
	while (1){
		int x=x1,y=y1,flag=0;
		For(p,1,len){
			if ( !check(x) || !check(y) || a[x][y]!=a[Sx][Sy] ){
				flag=1;break;
			}
			x+=Kx[k];
			y+=Ky[k];
		}
		if (flag) break;
	//	printf("%d %d  %d %d\n",Sx,Sy,k,len);
		len++;if (k>4) len++;
		ans++;res[a[Sx][Sy]]++;
		x1=x1+Skx[k],y1=y1+Sky[k];
	}
}

int main(){
	// say hello

	freopen("triangle.in","r",stdin);	
	freopen("triangle.out","w",stdout);
	
	n=read();
	For(i,1,n){
		scanf("%s",s);
		For(j,1,n) a[i][j]=s[j-1],Cd[s[j-1]]=1;
	}
	For(i,1,n) For(j,1,n) For(k,1,6) solve(i,j,k);
	printf("%d\n",ans);
	For(i,'A','Z'){
		if (Cd[i]!=0){
			printf("%c  %d\n",i,res[i]);
		}
	}
		
	// say goodbye
}


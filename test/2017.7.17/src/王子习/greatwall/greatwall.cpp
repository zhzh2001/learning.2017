#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int n;
const int N=1e6+7;
struct node{
	int x,y;	
}a[N];
inline bool operator<(const node&x,const node&y){
	return x.x<y.x;
}
inline bool cmp(const node&x,const node&y){
	return x.y<y.y;
}

inline int solve(){	
	sort(a+1,a+1+n);
	int now1=1,now2=1;
	int res=0;
	For(i,1,n){
		while (a[now1].x<i) now1++;
		if (a[now1].x!=i){
			while (a[now2].x!=a[now2+1].x) now2++;
			res+=abs(a[now2].x-i);
			now2++;
		}
	}
	int k=1;
	sort(a+1,a+1+n,cmp);
	now1=1;int tot=0;
	while (a[now1].y==1) now1++,tot++;
	while (tot<(n/2+n%2)){
		k++;
		while (a[now1].y==k) now1++,tot++;
	} 
	For(i,1,n) res+=abs(a[i].y-k);
	return res;
}

int main(){
	// say hello

	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);

	n=read();
	For(i,1,n) a[i].x=read(),a[i].y=read();
	int ans=solve();
	For(i,1,n) swap(a[i].x,a[i].y);
	ans=min(ans,solve());
	printf("%d\n",ans);
	
	// say goodbye
}


var
 n,i:longint;
 s,ans:int64;
 x,y,x1,y1,x2,y2:array[0..700000]of longint;
procedure sort1(l,r:longint);
var
 i,j,x,y:longint;
begin
 i:=l;
 j:=r;
 x:=x1[(l+r)div 2];
 repeat
  while x1[i]<x do inc(i);
  while x<x1[j] do dec(j);
  if not(i>j) then
  begin
   y:=x1[i];
   x1[i]:=x1[j];
   x1[j]:=y;
   y:=y1[i];
   y1[i]:=y1[j];
   y1[j]:=y;
   inc(i);
   dec(j)
  end;
 until i>j;
 if l<j then sort1(l,j);
 if i<r then sort1(i,r)
end;
procedure sort2(l,r:longint);
var
 i,j,x,y:longint;
begin
 i:=l;
 j:=r;
 x:=y1[(l+r)div 2];
 repeat
  while y1[i]<x do inc(i);
  while x<y1[j] do dec(j);
  if not(i>j) then
  begin
   y:=y1[i];
   y1[i]:=y1[j];
   y1[j]:=y;
   inc(i);
   dec(j)
  end;
 until i>j;
 if l<j then sort2(l,j);
 if i<r then sort2(i,r)
end;
procedure sort3(l,r:longint);
var
 i,j,x,y:longint;
begin
 i:=l;
 j:=r;
 x:=x2[(l+r)div 2];
 repeat
  while x2[i]<x do inc(i);
  while x<x2[j] do dec(j);
  if not(i>j) then
  begin
   y:=x2[i];
   x2[i]:=x2[j];
   x2[j]:=y;
   y:=y2[i];
   y2[i]:=y2[j];
   y2[j]:=y;
   inc(i);
   dec(j)
  end;
 until i>j;
 if l<j then sort3(l,j);
 if i<r then sort3(i,r)
end;
procedure sort4(l,r:longint);
var
 i,j,x,y:longint;
begin
 i:=l;
 j:=r;
 x:=y2[(l+r)div 2];
 repeat
  while y2[i]<x do inc(i);
  while x<y2[j] do dec(j);
  if not(i>j) then
  begin
   y:=y2[i];
   y2[i]:=y2[j];
   y2[j]:=y;
   inc(i);
   dec(j)
  end;
 until i>j;
 if l<j then sort4(l,j);
 if i<r then sort4(i,r)
end;
begin
assign(input,'greatwall.in');reset(input);
assign(output,'greatwall.out');rewrite(output);
 readln(n);
 for i:=1 to n do
 begin
  read(x[i],y[i]);
  x1[i]:=x[i];
  y1[i]:=y[i];
  x2[i]:=y[i];
  y2[i]:=x[i]
 end;
 sort1(1,n);
 s:=0;
 for i:=1 to n do
  s:=s+abs(x1[i]-i);
 sort2(1,n);
 for i:=1 to n div 2 do
  s:=s+y1[n-i+1]-y1[i];
 ans:=s;
 sort3(1,n);
 s:=0;
 for i:=1 to n do
  s:=s+abs(x2[i]-i);
 sort4(1,n);
 for i:=1 to n div 2 do
  s:=s+y2[n-i+1]-y2[i];
 if s>ans then write(ans)
          else write(s);
close(input);
close(output);
end.
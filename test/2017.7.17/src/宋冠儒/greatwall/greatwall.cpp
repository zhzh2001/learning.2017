#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 600005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,sum1,sum2,num;
struct data{ ll x,y; }a[N];
bool cmpx(data x,data y) { return x.x<y.x; }
bool cmpy(data x,data y) { return x.y<y.y; }
int main(){
	freopen("greatwall.in","r",stdin);
	freopen("greatwall.out","w",stdout);
	n=read(); rep(i,1,n) a[i].x=read(),a[i].y=read();
	sort(a+1,a+1+n,cmpx); num=a[(1+n)>>1].x;
	rep(i,1,n) sum1+=abs(a[i].x-i),sum2+=abs(num-a[i].x);
	sort(a+1,a+1+n,cmpy); num=a[(1+n)>>1].y;
	rep(i,1,n) sum2+=abs(a[i].y-i),sum1+=abs(num-a[i].y);
	printf("%d",min(sum1,sum2));
}

#include<bits/stdc++.h>
using namespace std;
int n,m,year,value,now;
int dep[300010],fa[300010],cnt[300010];
bool flag;
 int find(int v)
{
	return fa[v]==v?v:find(fa[v]);
		
}
 int findnum(int v)
{
	if (cnt[v]>now||cnt[v]==0) return v;
	return findnum(fa[v]);
}
 int main()
{
	freopen("history.in","r",stdin);
	freopen("history.out","w",stdout);
	scanf("%d%d",&n,&m);
	year=0;
	for (int i=0;i<n;++i) 
		dep[i]=1,fa[i]=i;
	flag=false;
	value=0;
	for (int i=1;i<=m;++i)
	{
		char opt;
		cin>>opt;
		if (opt=='R')
		{
			++year;
			int x,y;
			scanf("%d%d",&x,&y);
			if (flag)
			{
				x=x+value-n;
				if (x<0) x+=n;
				y=y+value-n;
				if (y<0) y+=n;
			}
			int tx=find(x),ty=find(y);
			if (tx!=ty)
			{
				if (dep[tx]<dep[ty]) 
				{
					cnt[tx]=year;
					fa[tx]=ty;
				}
				else if (dep[tx]>dep[ty])
				{
					cnt[ty]=year;
					fa[ty]=tx;
				}
				else if (dep[tx]==dep[ty])
				{
					cnt[tx]=year;
					fa[tx]=ty;
					dep[ty]++;
				}
			}	
		}
		if (opt=='K')
		{
			int val;
			scanf("%d",&val);
			value=val;
			flag=false;
		}
		
		if (opt=='T')
		{
			int st,ed,t;
			scanf("%d%d%d",&st,&ed,&t);
			now=year-t;
			int tx=find(st),ty=find(ed);
			if (tx==ty)
			{
				if (now<=0) 
				{
					flag=false;
					cout<<"Y"<<endl;
					continue;
				}
				int numx=findnum(st),numy=findnum(ed);
				if (numx==numy)
				{
					flag=true;
					cout<<"N"<<endl;
					continue;
				}
				if (numx!=numy)
				{
					flag=false;
					cout<<"Y"<<endl;
					continue;
				}
			}
			else 
			{
				flag=true;
				cout<<"N"<<endl;
			}
	
		}
	}
}

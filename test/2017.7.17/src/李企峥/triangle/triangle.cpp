#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,ansans;
int c[200][200];
int ans[200];
bool ff[200];
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void d1(int x,int y)
{
	int z=1;
	while(x+z<=n&&y+z<=n)
	{
		For(i,0,z)if(c[x+i][y+z-i]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void d2(int x,int y)
{
	int z=1;
	while(x-z>=1&&y+z<=n)
	{
		For(i,0,z)if(c[x-i][y+z-i]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void d3(int x,int y)
{
	int z=1;
	while(x+z<=n&&y-z>=1)
	{
		For(i,0,z)if(c[x+i][y-z+i]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void d4(int x,int y)
{
	int z=1;
	while(x-z>=1&&y-z>=1)
	{
		For(i,0,z)if(c[x-i][y-z+i]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void e1(int x,int y)
{
	int z=1;
	while(x+z<=n&&y-z>=1&&y+z<=n)
	{
		For(i,y-z,y+z)if(c[x+z][i]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void e2(int x,int y)
{
	int z=1;
	while(x-z>=1&&y-z>=1&&y+z<=n)
	{
		For(i,y-z,y+z)if(c[x-z][i]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void e3(int x,int y)
{
	int z=1;
	while(y+z<=n&&x-z>=1&&x+z<=n)
	{
		For(i,x-z,x+z)if(c[i][y+z]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
void e4(int x,int y)
{
	int z=1;
	while(y-z>=1&&x-z>=1&&x+z<=n)
	{
		For(i,x-z,x+z)if(c[i][y-z]!=c[x][y])return;
		z++;
		ans[c[x][y]]++;
		//cout<<x<<" "<<y<<endl;
	}
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();
	For(i,1,26)ff[i]=false;
	For(i,1,n)
		For(j,1,n)
		{
			ch=getchar();
			while(ch<'A'||ch>'Z')ch=getchar();
			c[i][j]=ch-'A'+1;
			ff[ch-'A'+1]=true;
		}
	For(i,1,26)ans[i]=0;
	For(i,1,n)
		For(j,1,n)
		{
			d1(i,j);d2(i,j);d3(i,j);d4(i,j);
			e1(i,j);e2(i,j);e3(i,j);e4(i,j);
		}
	For(i,1,26)ansans+=ans[i];
	cout<<ansans<<endl;
	For(i,1,26)if(ff[i])
				{
					ch='A'+i-1; 
					cout<<ch<<"  "<<ans[i]<<endl;
				}
	return 0;
}


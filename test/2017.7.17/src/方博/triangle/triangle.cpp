#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<cctype>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef unsigned int uint;
typedef long long LL;
const uint uinf=4294967295u;
const int maxn=203;
int n;
char b[maxn][maxn];

LL ans=0;
LL cnt[33];
bool used[33];
void input()
{
	read(n);
	char r;
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=n;++j)
		{
			do{r=getchar();}
			while(!isupper(r));
			b[i][j]=r;
			used[r-'A'+1]=true;
		}
	}
}
void count(int x,int y)
{
	char c=b[x][y];
	//直角边平行 
	int tmp=cnt[c-'A'+1];
	for(int i=y-1;i>=1&&b[x][i]==c;--i)
	{
		int j;
		for(j=1;x-j>=1&&i+j<=y&&b[x-j][i+j]==c;++j);
		if(j==y-i+1)++cnt[c-'A'+1];
		for(j=1;x+j<=n&&i+j<=y&&b[x+j][i+j]==c;++j);
		if(j==y-i+1)++cnt[c-'A'+1];
	}
	for(int i=y+1;i<=n&&b[x][i]==c;++i)
	{
		int j;
		for(j=1;x-j>=1&&i-j>=y&&b[x-j][i-j]==c;++j);
		if(j==i-y+1)++cnt[c-'A'+1];
		for(j=1;x+j<=n&&i-j>=y&&b[x+j][i-j]==c;++j);
		if(j==i-y+1)++cnt[c-'A'+1];
	}
	//斜边水平
	for(int i=x+1;i<=n&&b[i][y]==c;++i) 
	{
		int j;
		for(j=y-(i-x);j>=1&&j<=n&&j<=y+(i-x)&&b[i][j]==c;++j);
		if(j==y+(i-x)+1)++cnt[c-'A'+1];
	}
	for(int i=x-1;i>=1&&b[i][y]==c;--i) 
	{
		int j;
		for(j=y-(x-i);j>=1&&j<=n&&j<=y+(x-i)&&b[i][j]==c;++j);
		if(j==y+(x-i)+1)++cnt[c-'A'+1];
	}
	//斜边垂直
	for(int i=y+1;i<=n&&b[x][i]==c;++i) 
	{
		int j;
		for(j=x-(i-y);j>=1&&j<=n&&j<=x+(i-y)&&b[j][i]==c;++j);
		if(j==x+(i-y)+1)++cnt[c-'A'+1];
	}
	for(int i=y-1;i>=1&&b[x][i]==c;--i) 
	{
		int j;
		for(j=x-(y-i);j>=1&&j<=n&&j<=x+(y-i)&&b[j][i]==c;++j);
		if(j==x+(y-i)+1)++cnt[c-'A'+1];
	}
	ans+=cnt[c-'A'+1]-tmp;
}
void solve()
{
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=n;++j)
		{
			count(i,j);
		}
	}
}
int main()
{
	
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	input();
	solve();
	printf("%lld\n",ans);
	for(int i=1;i<=26;++i)
	{
		if(used[i])
		{
			printf("%c  %lld\n",char(i-1+'A'),cnt[i]);
		}
	}
	return 0;
}

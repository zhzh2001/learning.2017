#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("greatwall.in");
const int n = 6e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> dn(1, n);
		fout << dn(gen) << ' ' << dn(gen) << endl;
	}
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("greatwall.in");
ofstream fout("greatwall.out");
const int N = 600005;
int x[N], y[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> x[i] >> y[i];
	sort(x + 1, x + n + 1);
	sort(y + 1, y + n + 1);
	long long ans1 = 0, ans2 = 0;
	for (int i = 1; i <= n; i++)
	{
		ans1 += abs(i - x[i]) + abs(y[i] - y[(n + 1) / 2]);
		ans2 += abs(i - y[i]) + abs(x[i] - x[(n + 1) / 2]);
	}
	fout << min(ans1, ans2) << endl;
	return 0;
}
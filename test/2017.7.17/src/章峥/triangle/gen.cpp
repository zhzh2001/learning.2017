#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("triangle.in");
const int n = 100;
int main()
{
	minstd_rand gen(GetTickCount());
	fout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			uniform_int_distribution<> dc('A', 'B');
			fout.put(dc(gen));
		}
		fout << endl;
	}
	return 0;
}
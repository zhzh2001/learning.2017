#include <fstream>
#include <string>
using namespace std;
ifstream fin("triangle.in");
ofstream fout("triangle.ans");
const int N = 105;
string mat[N];
bool bl[26];
int ans[26];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> mat[i];
		mat[i] = ' ' + mat[i];
		for (int j = 1; j <= n; j++)
			bl[mat[i][j] - 'A'] = true;
	}
	int sum = 0;
	for (int c = 0; c < 26; c++)
		if (bl[c])
		{
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= n; j++)
					if (mat[i][j] == c + 'A')
					{
						for (int k = 1; i - k && j + k <= n; k++)
						{
							bool flag = true;
							for (int x = i - k, y = j; x <= i; x++, y++)
								if (mat[x][y] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; j - k && i + k <= n; k++)
						{
							bool flag = true;
							for (int x = i, y = j - k; y <= j; x++, y++)
								if (mat[x][y] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; i + k <= n && j + k <= n; k++)
						{
							bool flag = true;
							for (int x = i + k, y = j; x >= i; x--, y++)
								if (mat[x][y] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; j - k && i - k; k++)
						{
							bool flag = true;
							for (int x = i, y = j - k; y <= j; x--, y++)
								if (mat[x][y] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; i + k <= n && j - k && j + k <= n; k++)
						{
							bool flag = true;
							for (int y = j - k; y <= j + k; y++)
								if (mat[i + k][y] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; i - k && j - k && j + k <= n; k++)
						{
							bool flag = true;
							for (int y = j - k; y <= j + k; y++)
								if (mat[i - k][y] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; i + k <= n && j - k && i - k; k++)
						{
							bool flag = true;
							for (int x = i - k; x <= i + k; x++)
								if (mat[x][j - k] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
						for (int k = 1; i + k <= n && j + k <= n && i - k; k++)
						{
							bool flag = true;
							for (int x = i - k; x <= i + k; x++)
								if (mat[x][j + k] != c + 'A')
								{
									flag = false;
									break;
								}
							if (flag)
								ans[c]++;
							else
								break;
						}
					}
			sum += ans[c];
		}
	fout << sum << endl;
	for (int c = 0; c < 26; c++)
		if (bl[c])
			fout << (char)(c + 'A') << "  " << ans[c] << endl;
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("history.in");
ofstream fout("history.out");
const int n = 3e5;
int f[n], g[n], sz[n];
int getf(int x, int t)
{
	return f[x] == x || g[x] > t ? x : getf(f[x], t);
}
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 0; i < n; i++)
	{
		f[i] = i;
		sz[i] = 1;
	}
	int year = 0, c = 0;
	bool lastans = true;
	while (m--)
	{
		char opt;
		int x, y, rx, ry, t;
		bool now, pred;
		fin >> opt;
		switch (opt)
		{
		case 'K':
			fin >> c;
			lastans = true;
			break;
		case 'R':
			fin >> x >> y;
			if (!lastans)
			{
				x = (x + c) % n;
				y = (y + c) % n;
			}
			year++;
			rx = getf(x, year);
			ry = getf(y, year);
			if (rx != ry)
			{
				if (sz[rx] > sz[ry])
					swap(rx, ry);
				f[rx] = ry;
				g[rx] = year;
				sz[ry] += sz[rx] == sz[ry];
			}
			break;
		case 'T':
			fin >> x >> y >> t;
			now = getf(x, year) == getf(y, year);
			pred = getf(x, year - t) == getf(y, year - t);
			lastans = now && !pred;
			if (lastans)
				fout << "Y\n";
			else
				fout << "N\n";
			break;
		}
	}
	return 0;
}
#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <limits>
using namespace std;
const int N = 405;
vector<int> mat[N];
int dist[N][N];
double p_day2[N], p_dist[N];
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			dist[i][j] = i == j ? 0 : n;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
		dist[u][v] = dist[v][u] = 1;
	}
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
	double ans = .0;
	for (int day1 = 1; day1 <= n; day1++)
	{
		double now = .0;
		for (int dist1 = 0; dist1 < n; dist1++)
		{
			int cnt = count(dist[day1] + 1, dist[day1] + n + 1, dist1);
			if (!cnt)
				continue;
			fill(p_day2 + 1, p_day2 + n + 1, .0);
			unordered_set<int> S;
			for (int u = 1; u <= n; u++)
				if (dist[day1][u] == dist1)
					for (auto v : mat[u])
					{
						S.insert(v);
						p_day2[v] += 1.0 / n / mat[u].size();
					}
			double max_p_day2 = .0;
			for (int day2 = 1; day2 <= n; day2++)
			{
				for (auto u : S)
					p_dist[dist[day2][u]] = max(p_dist[dist[day2][u]], p_day2[u]);
				double sum = .0;
				for (auto u : S)
				{
					sum += p_dist[dist[day2][u]];
					p_dist[dist[day2][u]] = .0;
				}
				max_p_day2 = max(max_p_day2, sum);
			}
			now += max(1.0 / n, max_p_day2);
		}
		ans = max(ans, now);
	}
	cout.precision(numeric_limits<double>::digits10);
	cout << ans << endl;
	return 0;
}
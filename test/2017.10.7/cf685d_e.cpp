#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, INAN = 0xc0c0c0c0;
long long ans[N];
int y[N * 2], cnt[N * 2], pred[N * 2];
struct event
{
	int x, yl, yr, val;
	event() {}
	event(int x, int yl, int yr, int val) : x(x), yl(yl), yr(yr), val(val) {}
	bool operator<(const event &rhs) const
	{
		return x == rhs.x ? val < rhs.val : x < rhs.x;
	}
} e[N * 2];
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
	{
		int x, y;
		cin >> x >> y;
		e[i * 2 - 1] = event(x - k + 1, y - k + 1, y + 1, 1);
		e[i * 2] = event(x + 1, y - k + 1, y + 1, -1);
		::y[i * 2 - 1] = y - k + 1;
		::y[i * 2] = y + 1;
	}
	sort(e + 1, e + n * 2 + 1);
	sort(y + 1, y + n * 2 + 1);
	int yn = unique(y + 1, y + n * 2 + 1) - y - 1;
	fill(pred + 1, pred + n * 2 + 1, INAN);
	for (int i = 1; i <= n * 2; i++)
	{
		int l = lower_bound(y + 1, y + yn + 1, e[i].yl) - y, r = lower_bound(y + 1, y + yn + 1, e[i].yr) - y;
		for (int j = l; j < r; j++)
		{
			if (pred[j] != INAN)
				ans[cnt[j]] += 1ll * (y[j + 1] - y[j]) * (e[i].x - pred[j]);
			cnt[j] += e[i].val;
			pred[j] = e[i].x;
		}
	}
	for (int i = 1; i <= n; i++)
		cout << ans[i] << ' ';
	cout << endl;
	return 0;
}
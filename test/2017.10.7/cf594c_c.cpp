#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
const int N = 100005;
pair<int, int> p[N];
int x[N], y[N], del[N];
vector<int> rx[N], ry[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
	{
		int x1, y1, x2, y2;
		cin >> x1 >> y1 >> x2 >> y2;
		x[i] = x1 + x2;
		y[i] = y1 + y2;
		p[i] = make_pair(x[i], y[i]);
	}
	sort(x + 1, x + n + 1);
	sort(y + 1, y + n + 1);
	for (int i = 1; i <= n; i++)
	{
		rx[lower_bound(x + 1, x + n + 1, p[i].first) - x].push_back(i);
		ry[lower_bound(y + 1, y + n + 1, p[i].second) - y].push_back(i);
	}
	long long ans = 1ll * max((x[n] - x[1] + 1) / 2, 1) * max((y[n] - y[1] + 1) / 2, 1), now = 0;
	for (int xl = 1; xl <= k + 1; xl++)
	{
		for (int xr = n; xr >= n - k && xr >= xl; xr--)
		{
			for (int yl = 1; yl <= k + 1; yl++)
			{
				for (int yr = n; yr >= n - k && yr >= yl; yr--)
				{
					if (now <= k)
						ans = min(ans, 1ll * max((x[xr] - x[xl] + 1) / 2, 1) * max((y[yr] - y[yl] + 1) / 2, 1));
					for (auto i : ry[yr])
						if (!del[i]++)
							now++;
				}
				for (int yr = max(yl, n - k); yr <= n; yr++)
					for (auto i : ry[yr])
						if (!--del[i])
							now--;
				for (auto i : ry[yl])
					if (!del[i]++)
						now++;
			}
			for (int yl = 1; yl <= k + 1; yl++)
				for (auto i : ry[yl])
					if (!--del[i])
						now--;
			for (auto i : rx[xr])
				if (!del[i]++)
					now++;
		}
		for (int xr = max(xl, n - k); xr <= n; xr++)
			for (auto i : rx[xr])
				if (!--del[i])
					now--;
		for (auto i : rx[xl])
			if (!del[i]++)
				now++;
	}
	cout << ans << endl;
	return 0;
}
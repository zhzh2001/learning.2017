#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 1005, K = 8;
typedef pair<int, int> point;
point a[K], m[N];
vector<int> pre[K][N];
int k, n, perm[K], t;
bool vis[N], hit[N];
bool online(const point &a, const point &b, const point &c)
{
	int dx1 = a.first - b.first, dy1 = a.second - b.second, dx2 = b.first - c.first, dy2 = b.second - c.second;
	return (1ll * dx1 * dx2 > 0 || (dx1 == 0 && dx2 == 0)) && (1ll * dy1 * dy2 > 0 || (dy1 == 0 && dy2 == 0)) && 1ll * dx1 * dy2 == 1ll * dx2 * dy1;
}
bool dfs(int target)
{
	if (++t > k)
		return false;
	for (auto i : pre[perm[t]][target])
		if (!vis[i] && !dfs(i))
			return false;
	return vis[target] = true;
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> k >> n;
	for (int i = 1; i <= k; i++)
	{
		cin >> a[i].first >> a[i].second;
		perm[i] = i;
	}
	for (int i = 1; i <= n; i++)
		cin >> m[i].first >> m[i].second;
	for (int t = 1; t <= k; t++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (i != j && online(a[t], m[j], m[i]))
					pre[t][i].push_back(j);
	do
		for (int i = 1; i <= n; i++)
		{
			fill(vis + 1, vis + n + 1, false);
			t = 0;
			hit[i] |= dfs(i);
		}
	while (next_permutation(perm + 1, perm + k + 1));
	cout << count(hit + 1, hit + n + 1, true) << endl;
	return 0;
}
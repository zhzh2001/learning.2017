#include <iostream>
#include <vector>
#include <random>
#include <ctime>
#include <cstdlib>
#include <algorithm>
using namespace std;
const int N = 100005;
struct line
{
	int a, b, c, id;
};
inline bool intersect(const line &a, const line &b)
{
	return 1ll * a.a * b.b != 1ll * a.b * b.a;
}
inline bool intersect(const line &a, const line &b, const line &c)
{
	return c.a * (1ll * b.b * a.c - 1ll * a.b * b.c) - c.b * (1ll * b.a * a.c - 1ll * a.a * b.c) + c.c * (1ll * b.a * a.b - 1ll * a.a * b.b) == 0;
}
vector<pair<int, int>> ans;
minstd_rand gen(time(NULL));
void dfs(int k, vector<line> l)
{
	int n = l.size();
	if (n <= k)
	{
		cout << "YES\n"
			 << n + ans.size() << endl;
		for (auto i : l)
			cout << i.id << " -1\n";
		for (auto i : ans)
			cout << i.first << ' ' << i.second << endl;
		exit(0);
	}
	if (!k)
		return;
	for (int t = 0; t < 3 * k * k; t++)
	{
		uniform_int_distribution<> d(0, n - 1);
		int x = d(gen), y;
		do
			y = d(gen);
		while (x == y);
		if (!intersect(l[x], l[y]))
			continue;
		vector<line> now;
		for (int i = 0; i < n; i++)
			if (i != x && i != y && !intersect(l[x], l[y], l[i]))
				now.push_back(l[i]);
		if (now.size() > max(k - 1, n - n / k))
			continue;
		ans.push_back(make_pair(l[x].id, l[y].id));
		dfs(k - 1, now);
		ans.pop_back();
		if (n - now.size() > k)
			return;
	}
}
int main()
{
	int n, k;
	cin >> n >> k;
	vector<line> l(n);
	for (int i = 0; i < n; i++)
	{
		cin >> l[i].a >> l[i].b >> l[i].c;
		l[i].id = i + 1;
	}
	dfs(k, l);
	cout << "NO\n";
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 4005;
pair<int, int> a[N];
int b[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i].first >> a[i].second;
		if (a[i].first > a[i].second)
			swap(a[i].first, a[i].second);
	}
	sort(a + 1, a + n + 1);
	long long ans = 0;
	int x, y;
	for (int i = 1; i <= n; i++)
	{
		int cnt = 0;
		for (int j = 1; j <= n; j++)
			if (a[j].first >= a[i].first)
				b[++cnt] = a[j].second;
		sort(b + 1, b + cnt + 1);
		for (int j = 1; j <= cnt && b[j] <= a[i].second; j++)
		{
			long long now = 1ll * a[i].first * b[j] * (cnt - j + 1);
			if (now > ans)
			{
				ans = now;
				x = a[i].first;
				y = b[j];
			}
		}
	}
	cout << ans << endl
		 << x << ' ' << y << endl;
	return 0;
}
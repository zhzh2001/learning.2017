#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005;
int cnt[N], nxt[N];
pair<int, int> a[N];
int main()
{
	ios::sync_with_stdio(false);
	int c, n;
	cin >> c >> n;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		cnt[x]++;
	}
	int m = 0;
	for (int i = 1; i <= c; i++)
		if (cnt[i])
		{
			a[++m] = make_pair(cnt[i], i);
			nxt[i] = m;
		}
	for (int i = 1; i <= c; i++)
		nxt[i] = max(nxt[i], nxt[i - 1]);
	for (int i = 1; i <= c; i++)
	{
		int now = c, j = m;
		for (; a[j].second > i; j = min(j - 1, nxt[now]))
			now -= min(a[j].first * a[j].second, now / a[j].second * a[j].second);
		if (now >= i)
			now -= i;
		for (; j; j = min(j - 1, nxt[now]))
			now -= min(a[j].first * a[j].second, now / a[j].second * a[j].second);
		if (now)
		{
			cout << i << endl;
			return 0;
		}
	}
	cout << "Greed is good\n";
	return 0;
}
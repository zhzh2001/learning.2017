#include <bits/stdc++.h>
using namespace std;
int main(){
	mt19937_64 w(chrono::high_resolution_clock::now().time_since_epoch().count());
	int tc;
	scanf("%d",&tc);
	int T=5;
	printf("%d\n",T);
	while(T--){
		if (tc<=5)
			printf("%lld %lld\n",w()%1000000+1,w()%10+1);
		else printf("%lld %lld\n",w()%1000000000000LL+1,w()%(T%2?20:1000000000000LL)+1);
	}
}
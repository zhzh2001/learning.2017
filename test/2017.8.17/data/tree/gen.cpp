#include "testlib.h"
#include <bits/stdc++.h>
using namespace std;
std::vector<std::pair<int, int>> random_tree(int n, int w)
{
    std::vector<int> parent(n, -1);
    std::function<int(int)> find = [&](int u) {
        return ~parent[u] ? parent[u] = find(parent[u]) : u;
    };
    std::vector<std::pair<int, int>> edges;
    for (auto _ = 0; _ < n - 1; ++ _) {
        while (true) {
            auto a = rnd.next(0, n - 1);
            auto b = (a + rnd.wnext(1, n - 1, w)) % n;
            auto u = find(a);
            auto v = find(b);
            if (u != v) {
                if (rnd.next(0, 1)) {
                    std::swap(a, b);
                }
                edges.emplace_back(a, b);
                parent[u] = v;
                break;
            }
        }
    }
    return edges;
}
struct edge{
    int to,dist,next;
};
const int maxn=400001;
const int maxm=800001;
const int maxi=19;
struct graph{
    int n,m;
    int first[maxn];
    edge e[maxm];
    int dep[maxn];
    int par[maxn][20];
    int dist[maxn][20];
    void init(int n){
        this->n=n;
    }
    void addedge(int from,int to,int dist){
        e[++m]=(edge){to,dist,first[from]};
        first[from]=m;
    }
    void dfs(int u){
        //printf("%d ",u);
        for(int i=1;i<=maxi;i++){
            par[u][i]=par[par[u][i-1]][i-1];
            dist[u][i]=max(dist[u][i-1],dist[par[u][i-1]][i-1]);
        }
        for(int i=first[u];i;i=e[i].next){
            int v=e[i].to;
            if (dep[v])
                continue;
            par[v][0]=u;
            dist[v][0]=e[i].dist;
            dep[v]=dep[u]+1;
            dfs(v);
        }
    }
    void prepare(){
        dep[1]=1;
        dfs(1);
    }
    void addpoint(int a,int b){
        dep[++n]=dep[a]+1;
        par[n][0]=a;
        dist[n][0]=b;
        for(int i=1;i<=maxi;i++){
            par[n][i]=par[par[n][i-1]][i-1];
            dist[n][i]=max(dist[n][i-1],dist[par[n][i-1]][i-1]);
        }
    }
    int disttoansc(int u,int v){
        if (dep[u]<dep[v])
            swap(u,v);
        int t=dep[u]-dep[v],ret=0;
        for(int i=0;i<=maxi;i++)
            if (t&(1<<i))
                ret=max(ret,dist[u][i]),u=par[u][i];
        return ret;
    }
    int lca(int u,int v){
        if (dep[u]<dep[v])
            swap(u,v);
        int t=dep[u]-dep[v];
        for(int i=0;i<=maxi;i++)
            if (t&(1<<i))
                u=par[u][i];
        if (u==v)
            return u;
        for(int i=maxi;i>=0;i--)
            if (par[u][i]!=par[v][i])
                u=par[u][i],v=par[v][i];
        return par[u][0];
    }
    int query(int a,int u,int v){
        int lc=lca(u,v);
        //printf("%d\n",d);
        int x=lca(a,lc);
        if (x!=lc)
            return max(disttoansc(a,x),disttoansc(lc,x));
        int y=lca(a,u),z=lca(a,v);
        if (dep[y]<dep[z])
            swap(y,z);
        return disttoansc(a,y);
    }
}g;
int N[11]={0,10,2000,2000,200000,200000,200000,200000,200000,200000,200000};
int Q[11]={0,10,2000,2000,200000,200000,200000,200000,200000,200000,200000};
int main(){
	int tc;
    scanf("%d",&tc);
	rnd.setSeed(chrono::high_resolution_clock::now().time_since_epoch().count());
	int n=N[tc],q=Q[tc];
	printf("%d %d\n",n,q);
    g.init(n);
	if (tc==4 || tc==5){
		for(int i=1;i<n;i++){
            int w=rnd.next(0,1000000000);
            g.addedge(i,i+1,w);
            g.addedge(i+1,i,w);
			printf("%d %d %d\n",i,i+1,w);
        }
	}else{
		auto qwq=random_tree(n,n/2+rnd.next(0,n/2));
		for(auto i:qwq){
            int w=rnd.next(0,1000000000);
            g.addedge(i.first+1,i.second+1,w);
            g.addedge(i.second+1,i.first+1,w);
			printf("%d %d %d\n",i.first+1,i.second+1,w);
        }
	}
    int lastans=0;
    g.prepare();
	while(q--){
		int op=rnd.next(1,(tc>=4 && tc<=7)?1:2);
		if (op==1){
            int a=rnd.next(1,n),u=rnd.next(1,n),v=rnd.next(1,n);
			printf("%d %d %d %d\n",op,a^lastans,u^lastans,v^lastans);
            lastans=g.query(a,u,v);
        }
		else {
            int a=rnd.next(1,n++),b=rnd.next(0,1000000000);
            printf("%d %d %d\n",op,a^lastans,b^lastans);
            g.addpoint(a,b);
        }
	}
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	//freopen("tree.in","r",stdin);
	//freopen("tree.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=400001;
const int maxm=400001;
struct graph{
	int n,m;
	struct edge{
		int to,dist,next;
	}e[maxm];
	int first[maxn],dist[maxn],dep[maxn],fa[maxn];
	void addedge(int from,int to,int dist){
		e[++m]=(edge){to,dist,first[from]};
		first[from]=m;
	}
	void dfs(int u){
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (dep[v])
				continue;
			dep[v]=dep[u]+1;
			fa[v]=u;
			dist[v]=e[i].dist;
			dfs(v);
		}
	}
	void prepare(){
		dep[1]=1;
		dfs(1);
	}
	void addp(int a,int b){
		dep[++n]=dep[a]+1;
		fa[n]=a;
		dist[n]=b;
	}
	bool inchain[maxn];
	int nodes[maxn];
	int get(int u,int v){
		int ans=0;
		while(u!=v){
			if (dep[u]>dep[v])
				ans=max(ans,dist[u]),u=fa[u];
			else ans=max(ans,dist[v]),v=fa[v];
		}
		return ans;
	}
	int query(int a,int u,int v){
		int cur=0;
		while(u!=v){
			if (dep[u]>dep[v])
				nodes[++cur]=u,u=fa[u];
			else nodes[++cur]=v,v=fa[v];
		}
		int lc=nodes[++cur]=u,x=a,ans=0;
		for(int i=1;i<=cur;i++)
			inchain[nodes[i]]=true;
		while(!inchain[a] && a){
			ans=max(ans,dist[a]);
			a=fa[a];
		}
		for(int i=1;i<=cur;i++)
			inchain[nodes[i]]=false;
		if (!a)
			return get(x,lc);
		else return ans;
	}
}g;
int main(){
	init();
	int n=readint(),q=readint();
	g.n=n;
	for(int i=1;i<n;i++){
		int u=readint(),v=readint(),a=readint();
		g.addedge(u,v,a);
		g.addedge(v,u,a);
	}
	g.prepare();
	while(q--){
		int op=readint();
		if (op==1){
			int a=readint(),u=readint(),v=readint();
			printf("%d\n",g.query(a,u,v));
		}else{
			int a=readint(),b=readint();
			g.addp(a,b);
		}
	}
}
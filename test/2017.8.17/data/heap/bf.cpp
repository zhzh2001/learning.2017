#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mod=1000000007;
char s[100002];
int main(){
	ll k;
	scanf("%s%lld",s+1,&k);
	int n=strlen(s+1);
	for(int i=n+1;i<=n*k;i++)
		s[i]=s[(i-1)%n+1];
	n*=k;
	ll ans=1,num=1;
	for(int i=1;i<=n;i++){
		if (s[i]=='R')
			ans=(ans*2+num)%mod;
		if (s[i]=='L')
			ans=ans*2%mod;
		if (s[i]=='*'){
			ans=(ans*5+num)%mod;
			num=num*3%mod;
		}
	}
	printf("%lld",ans);
}
#include <bits/stdc++.h>
using namespace std;
const long long mod=1000000000000000000LL;
char c[5]="LRP*";
char s[100002];
int main(){
	mt19937_64 w(chrono::high_resolution_clock::now().time_since_epoch().count());
	int tc=10,qwq=4,n=100000;
	scanf("%d",&tc);
	if (tc<=2){
		int x=w()%n+1,y=w()%(n-x)+x+1,z=w()%(n-y)+y+1;
		s[x]=s[y]=s[z]='*';
		qwq=3;
	}
	for(int i=1;i<=n;i++)
		if (!s[i])
			s[i]=c[w()%qwq];
	puts(s+1);
	if (tc<=5)
		puts("1");
	else printf("%lld\n",w()%mod+1);
}
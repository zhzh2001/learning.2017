#include <fstream>
#include <string>
#include <cstring>
using namespace std;
ifstream fin("heap.in");
ofstream fout("heap.out");
const int MOD = 1000000007;
struct matrix
{
	static const int n = 2;
	long long mat[n][n];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				for (int k = 0; k < n; k++)
					ret.mat[i][j] = (ret.mat[i][j] + mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
	matrix &operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i < matrix::n; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, long long b)
{
	matrix ans = I();
	do
	{
		if (b & 1)
			ans *= a;
		a *= a;
	} while (b /= 2);
	return ans;
}
int main()
{
	string s;
	long long k;
	fin >> s >> k;
	matrix trans = I();
	for (int i = 0; i < s.length(); i++)
	{
		matrix now;
		switch (s[i])
		{
		case 'L':
			now.mat[0][0] = 2;
			now.mat[1][1] = 1;
			break;
		case 'R':
			now.mat[0][0] = 2;
			now.mat[0][1] = 1;
			now.mat[1][1] = 1;
			break;
		case 'P':
			now = I();
			break;
		case '*':
			now.mat[0][0] = 5;
			now.mat[0][1] = 1;
			now.mat[1][1] = 3;
			break;
		}
		trans = now * trans;
	}
	matrix init;
	init.mat[0][0] = init.mat[1][0] = 1;
	fout << (qpow(trans, k) * init).mat[0][0] << endl;
	return 0;
}
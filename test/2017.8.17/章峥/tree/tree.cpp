#include <fstream>
#include <vector>
#include <queue>
#include <algorithm>
#include <cmath>
#include <cassert>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 400005, LOGN = 20;
typedef pair<int, int> pii;
vector<pii> mat[N];
int vis[N], tag[N], now, st[N][LOGN];
pii Q[N];
void mark(int s, int t)
{
	Q[1] = make_pair(s, 0);
	vis[s] = ++now;
	for (int l = 1, r = 1; l <= r; l++)
	{
		if (Q[l].first == t)
		{
			do
			{
				tag[Q[l].first] = now;
				l = Q[l].second;
			} while (l);
			break;
		}
		for (vector<pii>::iterator it = mat[Q[l].first].begin(); it != mat[Q[l].first].end(); ++it)
			if (vis[it->first] < now)
			{
				Q[++r] = make_pair(it->first, l);
				vis[it->first] = now;
			}
	}
}
bool check(int s, int lim, int id)
{
	queue<int> Q;
	Q.push(s);
	vis[s] = ++now;
	while (!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		if (tag[k] == id)
			return true;
		for (vector<pii>::iterator it = mat[k].begin(); it != mat[k].end(); ++it)
			if (vis[it->first] < now && it->second <= lim)
			{
				Q.push(it->first);
				vis[it->first] = now;
			}
	}
	return false;
}
int rmq(int l, int r)
{
	int bin = floor(log2(r - l + 1));
	return max(st[l][bin], st[r - (1 << bin) + 1][bin]);
}
int main()
{
	int n, q;
	fin >> n >> q;
	bool chain = true;
	for (int i = 1; i < n; i++)
	{
		int u, v, w;
		fin >> u >> v >> w;
		chain &= v == u + 1;
		mat[u].push_back(make_pair(v, w));
		mat[v].push_back(make_pair(u, w));
	}
	if (chain)
	{
		for (int i = 1; i < n; i++)
			if (mat[i][0].first == i + 1)
				st[i][0] = mat[i][0].second;
			else
				st[i][0] = mat[i][1].second;
		for (int j = 1; j < LOGN; j++)
			for (int i = 1; i + (1 << j) - 1 <= n; i++)
				st[i][j] = max(st[i][j - 1], st[i + (1 << j - 1)][j - 1]);
		int lastans = 0;
		while (q--)
		{
			int opt, a, u, v;
			fin >> opt >> a >> u >> v;
			assert(opt == 1);
			a ^= lastans;
			u ^= lastans;
			v ^= lastans;
			if (u > v)
				swap(u, v);
			if (a < u)
				lastans = rmq(a, u - 1);
			else if (a > v)
				lastans = rmq(v, a - 1);
			else
				lastans = 0;
			fout << lastans << endl;
		}
		return 0;
	}
	int lastans = 0;
	while (q--)
	{
		int opt;
		fin >> opt;
		if (opt == 1)
		{
			int a, u, v;
			fin >> a >> u >> v;
			a ^= lastans;
			u ^= lastans;
			v ^= lastans;
			mark(u, v);
			int l = 0, r = 1e9, tmp = now;
			while (l < r)
			{
				int mid = (l + r) / 2;
				if (check(a, mid, tmp))
					r = mid;
				else
					l = mid + 1;
			}
			fout << (lastans = r) << endl;
		}
		else
		{
			int v, w;
			fin >> v >> w;
			v ^= lastans;
			w ^= lastans;
			mat[++n].push_back(make_pair(v, w));
			mat[v].push_back(make_pair(n, w));
		}
	}
	return 0;
}
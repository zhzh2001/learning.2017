#include<cstdio>
using namespace std;
const int maxn=1e6+5,mod=1e9+7;
bool mark[maxn];
int pri[maxn],n,k,tot,g[maxn],phi[maxn];
inline void init()
{
	phi[1]=1; n=maxn-5;
	for (int i=2;i<=n;i++){
		if (!mark[i]){
			pri[++tot]=i;
			phi[i]=i-1; 
		}
		for (int j=1;j<=tot&&pri[j]*i<=n;j++){
			mark[i*pri[j]]=1;
			if (i%pri[j]==0){
				phi[i*pri[j]]=phi[i]*pri[j];
				break;
			}else{
				phi[i*pri[j]]=phi[i]*phi[pri[j]];
			}
		}
	}
}
int get(int n,int k)
{
	if (!k){
		return n;
	}
	if (k&1){
		return phi[get(n,k-1)];
	}else{
		return get(n,k-1);
	}
}
int main()
{
	freopen("function.in", "r", stdin);
	freopen("function.out", "w", stdout);
	int T;
	scanf("%d",&T);
	init();
	while (T--){
		scanf("%d%d",&n,&k);
		printf("%d\n",get(n,k)%mod);
	}
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1000000007;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
char s[100010];
int n,k,l;
signed main()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	scanf("%s",s+1);k=read();
	int l=strlen(s+1);int n=1,p=1,q=1;
	for(int j=1;j<=k;j++)
		for(int i=1;i<=l;i++){
			if(s[i]=='L')n=(n*2)%MOD;
			if(s[i]=='R')n=(n*2+q)%MOD;
			if(s[i]=='*')n=(n*2+n*2+q+n)%MOD,q=(q*3)%MOD;
		}
	printf("%lld",n);
	return 0;
}
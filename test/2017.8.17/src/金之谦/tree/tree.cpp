#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,deep[400010],fa[400010],top[400010],son[400010],s[400010];
int sg[400010],gs[400010],cnt=0,a[400010];
int t[1000010],lt[1000010],rt[1000010];
int nedge=0,p[800010],c[800010],nex[800010],head[800010];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
inline void dfs(int x,int fat,int dep){
	deep[x]=dep;fa[x]=fat;s[x]=1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fat){
		dfs(p[k],x,dep+1);a[p[k]]=c[k];
		s[x]+=s[p[k]];if(s[p[k]]>s[son[x]])son[x]=p[k];
	}
}
inline void dfss(int x,int tt){
	top[x]=tt;sg[x]=++cnt;gs[cnt]=x;
	if(son[x])dfss(son[x],tt);
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa[x]&&p[k]!=son[x])dfss(p[k],p[k]);
}
inline void build(int l,int r,int nod){
	lt[nod]=l;rt[nod]=r;
	if(l==r){t[nod]=a[gs[l]];return;}
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=max(t[nod*2],t[nod*2+1]);
}
inline int smax(int i,int j,int nod){
	if(lt[nod]>=i&&rt[nod]<=j)return t[nod];
	int mid=lt[nod]+rt[nod]>>1,ans=0;
	if(i<=mid)ans=max(ans,smax(i,j,nod*2));
	if(j>mid)ans=max(ans,smax(i,j,nod*2+1));
	return ans;
}
inline int lca(int x,int y){
	int fx=top[x],fy=top[y];
	while(fx!=fy){
		if(deep[fx]<deep[fy])swap(x,y),swap(fx,fy);
		x=fa[top[x]],fx=top[x];
	}
	if(deep[x]>deep[y])swap(x,y);
	return x;
}
inline int fmax(int x,int y){
	int fx=top[x],fy=top[y],ans=0;
	while(fx!=fy){
		if(deep[fx]<deep[fy])swap(x,y),swap(fx,fy);
		ans=max(ans,smax(sg[fx],sg[x],1));
		x=fa[top[x]],fx=top[x];
	}
	if(deep[x]>deep[y])swap(x,y);
	ans=max(ans,smax(sg[x]+1,sg[y],1));
	return ans;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		addedge(x,y,z);addedge(y,x,z);
	}a[1]=0;
	dfs(1,0,1);dfss(1,1);
	build(1,n,1);int ans=0;
	for(int i=1;i<=m;i++){
		int op=read();
		if(op==1){
			int x=read()^ans,y=read()^ans,z=read()^ans;
			int p=lca(x,y),q=lca(x,z),r=lca(y,z);
			int jzq=deep[p]>deep[q]?p:q;jzq=deep[jzq]<deep[r]?r:jzq;
			ans=fmax(x,jzq);printf("%d\n",ans);
		}else{
			int x=read(),y=read();
		}
	}
	return 0;
}
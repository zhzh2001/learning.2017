#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1000000007;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
bool b[10000010];
int n,m,cnt=0,pri[1000010];
inline void get(){
	b[1]=1;
	for(int i=2;i<=1e7;i++){
		if(!b[i])pri[++cnt]=i;
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>1e7)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
inline int f(int x){
	if(x==1)return 1;
	int ans=1;
	for(int i=1;i<=cnt;i++)if(x%pri[i]==0){
		x/=pri[i];ans=(ans*(pri[i]-1+MOD)%MOD)%MOD;
		while(x%pri[i]==0)x/=pri[i],ans=(ans*pri[i])%MOD;
		if(x==1)break;
	}
	if(x>1)ans=(ans*(x-1)%MOD)%MOD;
	return ans;
}
signed main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	get();
	int T=read();
	while(T--){
		n=read();m=read();
		int p=n;
		for(int i=1;i<=m;i++)if(i&1){
			p=f(p);
			if(p==1)break;
		}
		printf("%lld\n",p);
	}
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define N 100020
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], val[N<<1], cnt;
void ins(int x, int y, int z){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; val[cnt] = z;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt; val[cnt] = z;
}
int fa[N], cc[N], dep[N];
void dfs(int x, int f, int v = 0){
	fa[x] = f; dep[x] = dep[f]+1; cc[x] = v;
	for(int i = head[x]; i; i = nxt[i])
		if(to[i] != f) dfs(to[i], x, val[i]);
}
int lca(int x, int y){
	if(dep[x] < dep[y]) swap(x, y);
	int t = dep[x] - dep[y];
	for(int i = 1; i <= t; i++) x = fa[x];
	if(x == y) return x;
	while(x != y) x = fa[x], y = fa[y];
	return x;
}
int calc(int x, int y){
	int ans = 0;
	if(dep[x] < dep[y]) swap(x, y);
	int t = dep[x] - dep[y];
	for(int i = 1; i <= t; i++) ans = max(ans, cc[x]), x = fa[x];
	if(x == y) return ans;
	while(x != y) ans = max(ans, max(cc[x], cc[y])), x = fa[x], y = fa[y];
	return ans;
}
int main(int argc, char const *argv[]){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	int n = read(), m = read();
	for(int i = 1; i < n; i++){
		int x = read(), y = read(), z = read();
		ins(x, y, z);
	} dfs(1, 0);
	int lastans = 0;
	while(m--){
		int op = read();
		if(op == 2){ ++n;
			int a = read()^lastans, c = read()^lastans;
			// printf(">>%d %d\n", a, c);
			fa[n] = a; cc[n] = c; dep[n] = dep[a]+1;
		}
		else{
			int a = read()^lastans, u = read()^lastans, v = read()^lastans;
			// printf(">>%d %d %d\n", a, u, v);
			int gg = lca(u, v);
			// printf("gg=%d\n", gg);
			if(lca(a, gg) == gg){
				int av = lca(a, v), au = lca(a, u);
				// printf("av=%d, au=%d\n", av, au);
				if(av == a || au == a) lastans = 0;
				else if(av == gg) lastans = calc(a, u);
				else lastans = calc(a, v);
			}
			else lastans = calc(a, gg);
			printf("%d\n", lastans);
		}
	}
	return 0;
}
/*
| name | age | work |
| ---- | --- | ---- |
| Jack | 12  | doc. |
| Tom  | 15  | stu. |
*/
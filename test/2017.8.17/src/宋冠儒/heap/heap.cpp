#include <bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define N 100020
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char str[N];
int main(int argc, char const *argv[]){
	freopen("heap.in", "r", stdin);
	freopen("heap.out", "w", stdout);
	scanf("%s", str+1); ll k = read();
	int len = strlen(str+1);
	ll ans = 1, p = 1;
	for(ll a = 1; a <= k; a++)
	for(int i = 1; i <= len; i++){
		switch(str[i]){
			case 'L':
			ans = (ans<<1)%mod;
			break;
			case 'R':
			ans = ((ans<<1)+p)%mod;
			break;
			case '*':
			ans = (ans*5+p)%mod;
			p = p*3%mod;
			break;
		}
	}
	printf("%lld\n", ans);
	return 0;
}
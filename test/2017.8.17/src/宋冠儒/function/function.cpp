#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int mo=1000000007;
int T;
ll n,k;
map<ll,ll> mp;
 inline void read(ll &x)
{
	char ch;ll bo=0; x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar()) if (ch=='-') bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo) x=-x;
}
 ll solve(ll x)
{
	if (mp[x]) return mp[x];
	ll ans=x-1;
	for (int i=2;i*i<=x;++i)
		if (!(x%i))
		{
			ans-=solve(i);
			if (i*i!=x) ans-=solve(x/i);	
		}	
	return mp[x]=ans;
}
 int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin>>T;
	while (T--)
	{
		read(n);read(k);
		k=(k+1)>>1;
		for (int i=1;i<=k;++i)
		{
			n=solve(n);if (n==1) break;
		}
		cout<<n%mo<<endl;
	}
}
























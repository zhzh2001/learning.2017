#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
using namespace std;

typedef long long ll;

const int Mod = 1000000007;

ifstream fin("heap.in");
ofstream fout("heap.out");

string s;
int k;

long long sum = 0;

void DoNext(int &lab, long long &x) {
	if (s[lab] == 'L') {
		x = (x * 2) % Mod;
		return;
	}
	if (s[lab] == 'R') {
		x = (x * 2 + 1) % Mod;
		return;
	}
	if (s[lab] == 'P') {
		return;
	}
}

void dfs(int lab, long long x) {
	while (s[lab] != '*') {
		DoNext(lab, x);
		++lab;
		if (lab == s.length()) {
			sum = (sum + x) % Mod;
			return;
		}
	}
	if (s[lab] == '*') {
		dfs(lab + 1, (x * 2) % Mod);
		dfs(lab + 1, (x * 2 + 1) % Mod);
		dfs(lab + 1, x);
	}
	if (lab == s.length()) {
		sum = (sum + x) % Mod;
		return;
	}
}

long long QuickPow(long long a, long long b) {
	long long res = 1;
	for (; b; b >>= 1, a = a * a % Mod) {
		if (b & 1) {
			res = res * a % Mod;
		}
	}
	return res;
}

int main() {
	fin >> s >> k;
	dfs(0, 1);
	fout << sum << endl;
	return 0;
}

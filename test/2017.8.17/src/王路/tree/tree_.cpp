#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;

const int Mod = 1000000007;

ifstream fin("tree.in");
ofstream fout("tree.out");

const int MaxN = 2e5 + 5, LogMax = 19;

typedef pair<int, int> pii;

int fa[MaxN << 1][LogMax];
vector<pii> G[MaxN];
int dis[MaxN << 1][LogMax];
int dep[MaxN << 1];

void dfs(int x, int f = -1) {
	for (int j = 1; j < LogMax; ++j) {
		fa[x][j] = fa[fa[x][j - 1]][j - 1];
		dis[x][j] = max(dis[x][j], max(dis[x][j - 1], dis[fa[x][j - 1]][j - 1]));
	}
	for (vector<pii>::iterator it = G[x].begin(); it != G[x].end(); ++it) {
		if (it->first == f) { continue; }
		dep[it->first] = dep[x] + 1;
		fa[it->first][0] = x;
		dis[it->first][x] = it->second;
		dfs(it->first, x);
	}
}

int GetLCA(int x, int y) {
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[x][j]] >= dep[y]) {
			x = fa[x][j];
		}
	}
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[y][j]] >= dep[x]) {
			y = fa[y][j];
		}
	}
	for (int j = LogMax - 1; j >= 0; --j) {
		if (fa[x][j] != fa[y][j]) {
			x = fa[x][j];
			y = fa[y][j];
		}
	}
	return x == y ? x : fa[x][0];
}

int GetDis(int x, int y) {
	int k = GetLCA(x, y);
	int ans = 0;
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[x][j]] >= dep[k]) {
			ans = max(ans, dis[x][j]);
			x = fa[x][j];
		}
	}
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[y][j]] >= dep[k]) {
			ans = max(ans, dis[y][j]);
			y = fa[y][j];
		}
	}
	return ans;
}

bool OnDis(int x, int y, int k) {
	if (dep[k] >= dep[x] && dep[k] >= dep[y]) {
		return false;
	}
	int lca = GetLCA(x, y);
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[x][j]] >= dep[k] && dep[fa[x][j]] >= dep[lca]) {
			x = fa[x][j];
		}
	}
	if (x == k) { return true; }
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[y][j]] >= dep[y] && dep[fa[y][j]] >= dep[lca]) {
			y = fa[y][j];
		}
	}
	if (y == k) { return true; }
	return false;
}

int GetPoint(int k, int u, int v) {
	int lca = GetLCA(u, v);
	if (dep[k] < dep[lca]) return 0;
	while (fa[k][0] != lca) {
		k = fa[k][0];
		if (OnDis(u, v, k)) {
			return k;
		}
	}
}

int main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i < n; ++i) {
		int u, v, a;
		fin >> u >> v >> a;
		G[u].push_back(make_pair(v, a));
		G[v].push_back(make_pair(u, a));
	}
	dfs(1);
	int node = n;
	int last_ans = 0;
	for (int i = 0; i < q; ++i) {
		int opt, a, b, u, v;
		fin >> opt;
		if (opt == 1) {
			fin >> a >> u >> v;
			a ^= last_ans, u ^= last_ans, v ^= last_ans;
			if (OnDis(u, v, a)) {
				fout << (last_ans = 0) << endl;
			} else {
				fout << (last_ans = min(GetDis(a, GetLCA(u, v)), GetDis(a, GetPoint(a, u, v)))) << endl;
			}
		} else if (opt == 2) {
			fin >> a >> b;
			a ^= last_ans, b ^= last_ans;
			G[a].push_back(make_pair(++node, b));
			fa[node][0] = a;
			int x = node;
			for (int j = 1; j < LogMax; ++j) {
				fa[x][j] = fa[fa[x][j - 1]][j - 1];
				dis[x][j] = max(dis[x][j], max(dis[x][j - 1], dis[dis[x][j - 1]][j - 1]));
			}
		}
	}
	return 0;
}
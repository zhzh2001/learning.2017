#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int RandInt32() {
	return ((rand() << 15) + rand()) % 1000000000;
}

const int MaxN = 2005;

int main() {
	freopen("tree_test.in", "w", stdout);
	ios_base::sync_with_stdio(false);
	srand(GetTickCount());
	int n = 2000, q = 2000;
	cout << n << ' ' << q << endl;
	for (int i = 1; i < n; ++i) {
		cout << i << ' ' << i + 1 << ' ' << RandInt32() << endl;
	}
	for (int j = 1; j <= q; ++j) {
		int typ = rand() % 10;
		if (typ) {
			cout << 1 << ' ' << rand() % n + 1 << ' ' << rand() % n + 1 << ' ' << rand() % n + 1 << endl;
		} else {
			cout << 2 << ' ' << rand() % n + 1 << ' ' << RandInt32() << endl;
		}
	}
	return 0;
}

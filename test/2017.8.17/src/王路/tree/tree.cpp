#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <cstdio>
#include <cstdlib>
using namespace std;

const int Mod = 1000000007;

ifstream fin("tree.in");
ofstream fout("tree.out");

const int MaxN = 2e5 + 5, LogMax = 19;

typedef pair<int, int> pii;

struct Edge {
	int to, nxt, w;
} edge[MaxN << 2];

int n, q;
int head[MaxN], cnt;

inline void Link(int x, int y, int w) {
	edge[++cnt].to = y;
	edge[cnt].w = w;
	edge[cnt].nxt = head[x];
	head[x] = cnt;
}

int dep[MaxN];
int fa[MaxN << 1][LogMax], dis[MaxN << 1][LogMax];

void dfs(int x, int f = -1) {
	for (int j = 1; j < LogMax; ++j) {
		fa[x][j] = fa[fa[x][j - 1]][j - 1];
		dis[x][j] = max(dis[x][j - 1], dis[fa[x][j - 1]][j - 1]);
	}
	for (int i = head[x]; i; i = edge[i].nxt) {
		if (edge[i].to != f) {
			fa[edge[i].to][0] = x;
			dep[edge[i].to] = dep[x] + 1;
			dis[edge[i].to][0] = edge[i].w;
			dfs(edge[i].to, x);
		}
	}
}

int GetLCA(int x, int y) {
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[x][j]] >= dep[y]) {
			x = fa[x][j];
		}
	}
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[y][j]] >= dep[x]) {
			y = fa[y][j];
		}
	}
	for (int j = LogMax - 1; j >= 0; --j) {
		if (fa[x][j] != fa[y][j]) {
			x = fa[x][j];
			y = fa[y][j];
		}
	}
	return x == y ? x : fa[x][0];
}

int GetDis(int x, int y) {
	int lca = GetLCA(x, y), ans = 0;
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[x][j]] >= dep[lca]) {
			ans = max(ans, dis[x][j]);
			x = fa[x][j];
		}
	}
	for (int j = LogMax - 1; j >= 0; --j) {
		if (dep[fa[y][j]] >= dep[lca]) {
			ans = max(ans, dis[y][j]);
			y = fa[y][j];
		}
	}
	return ans;
}

int GetPath(int x, int y, int k) {
	int lca = GetLCA(x, y);
	if (dep[k] < dep[lca]) {
		return GetDis(lca, k);
	}
	int ans = 0x3f3f3f3f;
	while (dep[x] >= dep[lca]) {
		ans = min(ans, GetDis(x, k));
		if (ans == 0) {
			return ans;
		}
		x = fa[x][0];
	}
	while (dep[y] >= dep[lca]) {
		ans = min(ans, GetDis(y, k));
		if (ans == 0) {
			return ans;
		}
		y = fa[y][0];
	}
	return ans;
}

int main() {
	fin >> n >> q;
	for (int i = 1; i < n; ++i) {
		int u, v, a;
		fin >> u >> v >> a;
		Link(u, v, a);
		Link(v, u, a);
	}
	int node = n;
	dfs(1);
	int last_ans = 0;
	for (int i = 1; i <= q; ++i) {
		int typ;
		fin >> typ;
		if (typ == 1) {
			int a, u, v;
			fin >> a >> u >> v;
			a ^= last_ans, u ^= last_ans, v ^= last_ans;
			fout << (last_ans = GetPath(u, v, a)) << endl;
		} else {
			int a, b;
			fin >> a >> b;
			a ^= last_ans, b ^= last_ans;
			fa[++node][0] = a;
			dis[node][0] = b;
			Link(node, a, b);
			Link(a, node, b);
			for (int j = 1; j <= LogMax; ++j) {
				fa[node][j] = fa[fa[node][j - 1]][j - 1];
				dis[node][j] = max(dis[node][j - 1], dis[fa[node][j - 1]][j - 1]);
			}
		}
	}
	cerr << clock() << endl;
	return 0;
}

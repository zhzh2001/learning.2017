#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <ctime>
using namespace std;

typedef long long ll;

const int Mod = 1000000007;

ifstream fin("function.in");
ofstream fout("function.out");

int f(int n) {
	if (n == 1) { return 1; }
	int cnt = 0;
	for (int i = 0; i <= n; ++i) {
		cnt += __gcd(i, n - i) == 1;
	}
	return cnt;
}

int g(int n) {
	int res = 0;
	for (int j = 1; j * j <= n; ++j) {
		if (n % j == 0) {
			if (j * j == n) {
				res += f(j);
			} else {
				res += f(j) + f(n / j);
			}
		}
	}
	return res;
}

int F(int x, int k) {
	if (k == 0) {
		return x;
	}
	if (k & 1) {
		return f(F(x, k - 1));
	} else {
		return g(F(x, k - 1));
	}
}

int main() {
	int T;
	fin >> T;
	while (T--) {
		int n, k;
		fin >> n >> k;
		fout << F(n, k) << endl;
	}
	cerr << clock() << endl;
	return 0;
}

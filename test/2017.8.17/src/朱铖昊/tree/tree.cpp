#include<bits/stdc++.h>
using namespace std;
const int N=400005;
int pr[N*2],to[N*2],la[N],f[N][20],g[N][20],sd[N],v[N*2],ans,x,y,z,n,m,l,w,xx,yy,zz,ww;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c<='9'&&c>='0')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y,int z)
{
	to[++l]=y;
	pr[l]=la[x];
	la[x]=l;
	v[l]=z;
}
void dfs(int x,int y)
{
	for (int i=1;(1<<i)<=sd[x];++i)
	{
		f[x][i]=f[f[x][i-1]][i-1];
		g[x][i]=max(g[x][i-1],g[f[x][i-1]][i-1]);
	}
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=y)
		{
			f[to[i]][0]=x;
			g[to[i]][0]=v[i];
			sd[to[i]]=sd[x]+1;
			dfs(to[i],x);
		}
}
inline void xj(int x)
{
	for (int i=1;(1<<i)<=sd[x];++i)
	{
		f[x][i]=f[f[x][i-1]][i-1];
		g[x][i]=max(g[x][i-1],g[f[x][i-1]][i-1]);
	}
}
inline int sum(int x,int y)
{
	if (sd[x]<sd[y])
		swap(x,y);
	int ans=0;
	for (int i=19;i>=0;--i)
		if (sd[f[x][i]]>=sd[y])
		{
			ans=max(ans,g[x][i]);
			x=f[x][i];
		}
	if (x==y)
		return ans;
	for (int i=19;i>=0;--i)
		if (f[x][i]!=f[y][i])
		{
			ans=max(ans,max(g[x][i],g[y][i]));
			x=f[x][i];
			y=f[x][i];
		}
	return max(ans,max(g[x][0],g[y][0]));
}
inline int lca(int x,int y)
{
	if (sd[x]<sd[y])
		swap(x,y);
	for (int i=19;i>=0;--i)
		if (sd[f[x][i]]>=sd[y])
			x=f[x][i];
	for (int i=19;i>=0;--i)
		if (f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[x][i];
		}
	if (x!=y)
		return f[x][0];
	else
		return x;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	sd[1]=1;
	dfs(1,0);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		read(z);
		y^=ans;
		z^=ans;
		if (x==1)
		{
			read(w);
			w^=ans;
			swap(w,y);

			if (sd[y]<sd[z])
				swap(y,z);
			yy=lca(y,w);
			if (yy==y)
			{
				ans=sum(y,w);
				printf("%d\n",ans);
				continue;
			}
			zz=lca(z,w);
			if (zz==z)
			{
				if (w==yy)
				{
					ans=0;
					printf("%d\n",ans);
					continue;
				}
				ans=sum(z,w);
				printf("%d\n",ans);
				continue;
			}
			xx=lca(y,z);
			ww=lca(xx,w);
			if (ww!=xx)
			{
				ans=sum(xx,w);
				printf("%d\n",ans);
			}
			else
			{
				ans=min(sum(yy,w),sum(zz,w));
				printf("%d\n",ans);
			}
		}
		else
		{
			++n;
			sd[n]=sd[y]+1;
			f[n][0]=1;
			g[n][0]=z;
			xj(n);
		}
	}
	return 0;
}

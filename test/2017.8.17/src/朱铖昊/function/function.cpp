#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll ans;
int zs;
int a[100000];
int T;
ll n,k;
bool flag;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	zs=1;
	a[1]=2;
	For(i,3,2e5)
	{
		flag=true;
		For(j,1,zs)if(i%a[j]==0)
		{
			flag=false;
			break;
		}
		if(flag)
		{
			zs++;
			a[zs]=i;
		}
	}
	T=read();
	while(T--)
	{
		n=read();k=read();
		k=(k+1)/2;
		ans=n;
		For(i,1,k)
		{
			n=ans;
			if(n==1)break;
			ans=1;
			For(j,1,zs)
			{
				if(a[j]>n)break;
				if(n%a[j]==0)
				{
					ans*=(a[j]-1);n/=a[j];
					while(n%a[j]==0)n/=a[j],ans*=a[j];
				}
			}
			if(n>1)ans*=(n-1);
		}
		ans%=1000000007; 
		cout<<ans<<endl;
	}
	return 0;
}


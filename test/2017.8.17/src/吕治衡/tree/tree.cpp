#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,q,x,y,z,ma[500000][21],st[500000][21],a[1000000][3],last[1000000],d[1000000],c,k,kk,kkk,ans;
int doit(int x,int y,int z){a[++kk][0]=last[x];a[kk][1]=y;a[kk][2]=z;last[x]=kk;}
int dfs(int x,int fa,int su,int de){
		ma[x][0]=su;d[x]=de;st[x][0]=fa;
		for (int i=last[x];i;i=a[i][0])
			if (a[i][1]!=fa)dfs(a[i][1],x,a[i][2],de+1);
	}
int lca(int x,int y){
		if (d[x]<d[y])swap(x,y);
		for (int i=20;i>=0;i--)
			if (d[st[x][i]]>=d[y])x=st[x][i];
		for (int i=20;i>=0;i--)
			if (st[x][i]!=st[y][i])x=st[x][i],y=st[y][i];
		if (x!=y)return st[x][0];
			else return x;
	}
int doit1(int x,int y){
		if (d[x]<d[y])swap(x,y);int sum=0;
		for (int i=20;i>=0;i--)
			if (d[st[x][i]]>=d[y])sum=max(sum,ma[x][i]),x=st[x][i];
		return sum;
	}
signed main(){
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>q;for (int i=1;i<n;i++)cin>>x>>y>>z,doit(x,y,z),doit(y,x,z);
	dfs(1,0,0,1);
	for (int i=1;i<=20;i++)
		for (int j=1;j<=n;j++)st[j][i]=st[st[j][i-1]][i-1],ma[j][i]=max(ma[j][i-1],ma[st[j][i-1]][i-1]);
	while (q--){
			cin>>c;
			if (c==1){
					cin>>x>>y>>z;x^=ans;y^=ans;z^=ans;
					k=lca(x,y);kk=lca(x,z);kkk=lca(y,z);
					if(d[k]<d[kk])k=kk;if (d[k]<d[kkk])k=kkk;
					cout<<(ans=doit1(k,x))<<endl;
				}
				else{
					cin>>x>>y;x^=ans;y^=ans;
					st[++n][0]=x;ma[n][0]=y;
					for (int i=1;i<=20;i++)st[n][i]=st[st[n][i-1]][i-1],ma[n][i]=max(ma[n][i-1],ma[st[n][i-1]][i-1]);
				}
		}
}
/*
10 10
10 1 237837066
7 4 333101381
5 6 764410644
2 9 800406055
2 3 977449294
10 9 85077389
6 8 883423879
7 5 483282546
3 4 109819496
2 2 237524075
1 7 3 2
2 333101376 243353439
2 333101391 970164111
2 333101389 489066126
1 333101377 333101376 333101383
2 5 947013122
2 15 885096691
1 7 9 10
1 977449284 977449286 977449291
*/

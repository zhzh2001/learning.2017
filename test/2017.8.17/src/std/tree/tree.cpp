#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=20000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
struct edge{
    int to,dist,next;
};
const int maxn=400001;
const int maxm=800001;
const int maxi=19;
struct graph{
    int n,m;
    int first[maxn];
    edge e[maxm];
    int dep[maxn];
    int par[maxn][20];
    int dist[maxn][20];
    void init(int n){
        this->n=n;
    }
    void addedge(int from,int to,int dist){
        e[++m]=(edge){to,dist,first[from]};
        first[from]=m;
    }
    void dfs(int u){
        //printf("%d ",u);
        for(int i=1;i<=maxi;i++){
            par[u][i]=par[par[u][i-1]][i-1];
            dist[u][i]=max(dist[u][i-1],dist[par[u][i-1]][i-1]);
        }
        for(int i=first[u];i;i=e[i].next){
            int v=e[i].to;
            if (dep[v])
                continue;
            par[v][0]=u;
            dist[v][0]=e[i].dist;
            dep[v]=dep[u]+1;
            dfs(v);
        }
    }
    void prepare(){
        dep[1]=1;
        dfs(1);
    }
    void addpoint(int a,int b){
        dep[++n]=dep[a]+1;
        par[n][0]=a;
        dist[n][0]=b;
        for(int i=1;i<=maxi;i++){
            par[n][i]=par[par[n][i-1]][i-1];
            dist[n][i]=max(dist[n][i-1],dist[par[n][i-1]][i-1]);
        }
    }
    int disttoansc(int u,int v){
        if (dep[u]<dep[v])
            swap(u,v);
        int t=dep[u]-dep[v],ret=0;
        for(int i=0;i<=maxi;i++)
            if (t&(1<<i))
                ret=max(ret,dist[u][i]),u=par[u][i];
        return ret;
    }
    int lca(int u,int v){
        if (dep[u]<dep[v])
            swap(u,v);
        int t=dep[u]-dep[v];
        for(int i=0;i<=maxi;i++)
            if (t&(1<<i))
                u=par[u][i];
        if (u==v)
            return u;
        for(int i=maxi;i>=0;i--)
            if (par[u][i]!=par[v][i])
                u=par[u][i],v=par[v][i];
        return par[u][0];
    }
    int query(int a,int u,int v){
        int lc=lca(u,v);
        //printf("%d\n",d);
        int x=lca(a,lc);
        if (x!=lc)
            return max(disttoansc(a,x),disttoansc(lc,x));
        int y=lca(a,u),z=lca(a,v);
        if (dep[y]<dep[z])
            swap(y,z);
        return disttoansc(a,y);
    }
}g;
int main(){
    init();
    //int n=readint();
    //printf("%d",n);
    int n=readint(),q=readint();
    g.init(n);
    //puts("WTF");
    for(int i=1;i<=n-1;i++){
        int a=readint(),b=readint(),c=readint();
        g.addedge(a,b,c);
        g.addedge(b,a,c);
    }
    //puts("WTF");
    g.prepare();
    int lastans=0;
    while(q--){
        int op=readint();
        if (op==1){
            int a=readint()^lastans,u=readint()^lastans,v=readint()^lastans;
            printf("%d\n",lastans=g.query(a,u,v));
        }else{
            int a=readint()^lastans,b=readint()^lastans;
            g.addpoint(a,b);
        }
    }
}

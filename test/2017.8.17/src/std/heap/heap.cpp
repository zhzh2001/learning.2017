#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mod=1000000007;
struct matrix{
	int m,n;
	int a[3][3];
	void init(int m,int n){
		this->m=m,this->n=n;
		for(int i=1;i<=m;i++)
			for(int j=1;j<=n;j++)
				a[i][j]=0;
	}
	matrix(int m=0,int n=0){
		init(m,n);
	}
	matrix operator *(const matrix& rhs){
		if (n!=rhs.m){
			puts("WTF");
			exit(233);
		}
		matrix res(m,rhs.n);
		for(int i=1;i<=m;i++)
			for(int j=1;j<=rhs.n;j++)
				for(int k=1;k<=n;k++)
					res.a[i][j]=(res.a[i][j]+(ll)a[i][k]*rhs.a[k][j])%mod;
		return res;
	}
};
matrix power(matrix& x,ll y){
	if (x.m!=x.n){
		puts("WTF");
		exit(233);
	}
	matrix res(x.m,x.n),t=x;
	for(int i=1;i<=x.n;i++)
		res.a[i][i]=1;
	while(y){
		if (y&1)
			res=res*t;
		t=t*t;
		y>>=1;
	}
	return res;
}
char s[100002];
int main(){
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	ll x;
	scanf("%s%lld",s+1,&x);
	int n=strlen(s+1);
	matrix st(2,1),tr(2,2),l(2,2),r(2,2),w(2,2);
	for(int i=1;i<=2;i++)
		st.a[i][1]=1,tr.a[i][i]=1;
	l.a[1][1]=2;
	l.a[2][2]=1;
	r.a[1][1]=2;
	r.a[1][2]=1;
	r.a[2][2]=1;
	w.a[1][1]=5;
	w.a[1][2]=1;
	w.a[2][2]=3;
	for(int i=1;i<=n;i++){
		if (s[i]=='L')
			tr=l*tr;
		else if (s[i]=='R')
			tr=r*tr;
		else if (s[i]=='*')
			tr=w*tr;
	}
	tr=power(tr,x);
	st=tr*st;
	printf("%d",st.a[1][1]);
}
#include<bits/stdc++.h>
#define ll long long
#define mo 1000000007
using namespace std;
char S[100005];
ll k;
int n;
int main(){
	freopen("heap.in","r",stdin);
	freopen("dt.out","w",stdout);
	scanf("%s%lld",S+1,&k);
	n=strlen(S+1);
	ll x=1,s=1;
	while (k--)
		for (int i=1;i<=n;i++){
			if (S[i]=='P') continue;
			if (S[i]=='L'||S[i]=='R'){
				x=x*2%mo;
				if (S[i]=='R') x=(x+s)%mo;
			}	
			else{
				x=(x*5+s)%mo;
				s=s*3%mo;
			}
		}
	printf("%lld\n",x);
}

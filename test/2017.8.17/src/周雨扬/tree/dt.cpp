#include<bits/stdc++.h>
#define ll long long
#define N 200005
using namespace std;
const int L=200000;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
int dis[N],q[N],vis[N],head[N];
int n,Q,x,y,z,ans,tot;
struct edge{int to,next,v;}e[N*2];
void add(int x,int y,int z){
	e[++tot]=(edge){y,head[x],z};
	head[x]=tot;
}
bool dfs(int x,int fa,int end){
	if (x==end){
		vis[x]=1;
		return 1;
	}
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa)
			if (dfs(e[i].to,x,end)){
				vis[x]=1;
				return 1;
			}
	return 0;
}
void bfs(int x){
	q[1]=x; dis[x]=0;
	for (int h=0,t=1;h!=t;){
		x=q[++h];
		if (vis[x]==1){
			ans=dis[x];
			return;
		}
		vis[x]=2;
		for (int i=head[x];i;i=e[i].next)
			if (vis[e[i].to]!=2){
				q[++t]=e[i].to;
				dis[e[i].to]=max(dis[x],e[i].v);
			}
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("dt.out","w",stdout);
	n=read(); Q=read();
	for (int i=1;i<n;i++){
		x=read(); y=read(); z=read();
		add(x,y,z); add(y,x,z);
	}
	while (Q--)
		if (read()==2){
			x=read();
			y=read();
			add(++n,x,y);
			add(x,n,y);
		}
		else{
			x=read(); y=read(); z=read();
			memset(vis,0,sizeof(vis));
			dfs(y,0,z);
			bfs(x);
			printf("%d\n",ans);
		}
}

#include<bits/stdc++.h>
using namespace std;
int f[200005][25];
int a[200005][25];
int deep[200005];
int head[400005];
int tail[400005];
int next[400005];
int e[400005];
int t,n,m;
int ans;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void addto(int x,int y,int z)
{
	t++;
	next[t]=head[x];
	head[x]=t;
	tail[t]=y;
	e[t]=z;
}
void dfs(int x,int dep)
{
	deep[x]=dep;
	for(int i=head[x];i!=0;i=next[i])
		if(tail[i]!=a[x][0]){
			int k=tail[i];
			a[k][0]=x;
			f[k][0]=e[i];
			dfs(k,dep+1);
		}
}
struct xxx{
	int k;
	int ans1,ans2;
};
xxx lca(int l,int r)
{
	xxx ret;
	ret.ans1=0;
	ret.ans2=0;
	if(deep[l]<deep[r]){
		int x=deep[r]-deep[l];
		for(int i=20;i>=0;i--)
			if(x>>i&1)ret.ans2=max(ret.ans2,f[r][i]),r=a[r][i];
	}
	if(deep[l]>deep[r]){
		int x=deep[l]-deep[r];
		for(int i=20;i>=0;i--)
			if(x>>i&1)ret.ans1=max(ret.ans1,f[l][i]),l=a[l][i];
	}
	for(int i=20;i>=0;i--)
		if(a[l][i]!=a[r][i]){
			l=a[l][i];
			r=a[r][i];
			ret.ans1=max(ret.ans1,f[l][i]);
			ret.ans2=max(ret.ans2,f[r][i]);
		}
//	cout<<l<<' '<<r<<endl;
	if(l!=r){
		ret.k=a[l][0];
		ret.ans1=max(ret.ans1,f[l][0]);
		ret.ans2=max(ret.ans2,f[r][0]);
	}
	else ret.k=l;
	return ret;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<n;i++){
		int x,y,z;
		x=read();
		y=read();
		z=read();
		addto(x,y,z);
		addto(y,x,z);
	}
	dfs(1,1);
	a[1][0]=1;
	for(int j=1;j<=20;j++)
		for(int i=1;i<=n;i++){
			a[i][j]=a[a[i][j-1]][j-1];
			f[i][j]=max(f[i][j-1],f[a[i][j-1]][j-1]);
		}
/*	for(int i=1;i<=n;i++)
		cout<<a[i][0]<<' ';
	cout<<endl;*/
	for(int i=1;i<=m;i++){
		int op;
		op=read();
		if(op==1){
			int x,y,z;
			x=read()^ans;
			y=read()^ans;
			z=read()^ans;
			if(deep[y]>deep[z])swap(y,z);
			xxx k1,k2,k3;
			k1=lca(x,y);
			k2=lca(x,z);
			k3=lca(y,z);
//			cout<<k1.k<<' '<<k1.ans1<<' '<<k1.ans2<<endl;
//			cout<<k2.k<<' '<<k2.ans1<<' '<<k2.ans2<<endl;
			if(k1.k==k2.k){
				if(k1.k==x)k3=lca(k3.k,x),ans=k3.ans1;
				else if(deep[k3.k]<deep[k1.k])ans=max(k1.ans1,k1.ans2);
				else ans=k1.ans1;
			}
			else {
				if(k1.k==k3.k)ans=k2.ans1;
				else ans=k1.ans1;
			}
			printf("%d\n",ans);
		}
		if(op==2){
			int x,y;
			x=read()^ans;
			y=read()^ans;
			n++;
			a[n][0]=x;
			f[n][0]=y;
			deep[n]=deep[x]+1;
			for(int j=1;j<=20;j++){
				a[n][j]=a[a[n][j-1]][j-1];
				f[n][j]=max(f[n][j-1],f[a[n][j-1]][j-1]);
			}
		}
	}
	return 0;
}

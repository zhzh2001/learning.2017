#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll phi(ll x){
	ll t = x, s = sqrt(x);
	for(ll i = 2; i <= s; i++) if(x%i==0){
		t=t/i*(i-1);
		while(x%i==0)x/=i;
	}
	if(x>1)t=t/x*(x-1);
	return t;
}
ll F(ll n, ll k){
	k = k+1>>1;
	for(int i = 1; i <= k; i++){
		n = phi(n);
		if(n == 1) break;
	}
	return n;
}
int main(int argc, char const *argv[]){
	freopen("function.in", "r", stdin);
	freopen("function.out", "w", stdout);
	int T = read();
	while(T--){
		ll n = read(), k = read();
		printf("%lld\n", F(n, k)%1000000007);
	}
	return 0;
}
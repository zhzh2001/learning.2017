#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=4e5+5;
struct cs{int to,nxt,v;}a[N*4];
int head[N],ll;
int dp[N],f[N][20],ma[N][20];
int n,m,x,y,z,last;
void init(int x,int y,int v){
	a[++ll].to=y;
	a[ll].v=v;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs(int x,int y,int z,int v){
	dp[x]=z;f[x][0]=y;ma[x][0]=v;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y)dfs(a[k].to,x,z+1,a[k].v);
}
void make(){
	for(int j=1;j<=19;j++)
		for(int i=1;i<=n;i++){
			f[i][j]=f[f[i][j-1]][j-1];
			ma[i][j]=max(ma[i][j-1],ma[f[i][j-1]][j-1]);
		}
}
int up(int x,int y){
	for(int j=19;j>=0;j--)
		if(dp[f[x][j]]>=y)x=f[x][j];
	return x;
}
int lca(int x,int y){
	if(dp[x]<dp[y])swap(x,y);
	x=up(x,dp[y]);
	if(x==y)return x;
	for(int j=19;j>=0;j--)
		if(f[x][j]!=f[y][j])
			x=f[x][j],y=f[y][j];
	return f[x][0];
}
int out(int x,int y){
	if(dp[x]<dp[y])swap(x,y);
	int ans=0;
	for(int j=19;j>=0;j--)
		if(dp[f[x][j]]>=dp[y])ans=max(ans,ma[x][j]),x=f[x][j];
	return ans;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		init(x,y,z);init(y,x,z);
	}
	dfs(1,0,1,0);
	make();
	while(m--){
		scanf("%d",&z);
//		cout<<"---------"<<z<<endl;
		if(z==1){
			scanf("%d%d%d",&z,&x,&y);
			z=z^last;x=x^last;y=y^last;
//			cout<<x<<' '<<y<<"==================="<<z<<endl;
			int ans;int v=lca(x,y);
			int vv=lca(z,v);
			ans=max(out(v,vv),out(vv,z));
			vv=lca(x,z);
			if(dp[vv]>dp[v])ans=min(ans,out(vv,z));
			vv=lca(y,z);
			if(dp[vv]>dp[v])ans=min(ans,out(vv,z));
			printf("%d\n",ans);last=ans;
		}else{
			scanf("%d%d",&x,&y);
			x=x^last;y=y^last;
//			cout<<x<<' '<<y<<"==================="<<endl;
			n++;
			f[n][0]=x;ma[n][0]=y;
			for(int j=1;j<=19;j++)
				for(int i=n;i<=n;i++){
					f[i][j]=f[f[i][j-1]][j-1];
					ma[i][j]=max(ma[i][j-1],ma[f[i][j-1]][j-1]);
				}
		}
	}
}

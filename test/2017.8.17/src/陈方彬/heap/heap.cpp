#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
struct H{
	Ll a[3][3];
	H(){memset(a,0,sizeof a);}
}a,l,r,x;
char c[N];
Ll m,mo=1e9+7;
int n;
H cheng(H a,H b){
	H c;
	for(int i=1;i<=2;i++)
		for(int j=1;j<=2;j++)
			for(int k=1;k<=2;k++)
				c.a[i][j]=(c.a[i][j]+a.a[i][k]*b.a[k][j])%mo;
	return c;
}
H ksm(H a,Ll k){
	H ans=a;
	for(k--;k;k/=2,a=cheng(a,a))
		if(k&1)ans=cheng(ans,a);
	return ans;
}
int main()
{
	freopen("heap.in","r",stdin);
	freopen("heap.out","w",stdout);
	scanf("%s%lld",c+1,&m);
	a.a[1][1]=a.a[2][2]=1;
	l.a[1][1]=2;l.a[2][2]=1;
	r.a[1][1]=2;r.a[1][2]=r.a[2][2]=1;
	x.a[1][1]=5;x.a[1][2]=1;x.a[2][2]=3;
	n=strlen(c+1);
	for(int i=n;i;i--)
		if(c[i]=='L')a=cheng(a,l);else
		if(c[i]=='R')a=cheng(a,r);else
		if(c[i]=='*')a=cheng(a,x);
	a=ksm(a,m);
	Ll ans=(a.a[1][1]+a.a[1][2])%mo;
	printf("%lld",ans);
}

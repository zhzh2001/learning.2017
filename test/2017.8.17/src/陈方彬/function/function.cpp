#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
int n,m,k;
int f(int n){
	int ans=0;
	for(int i=1;i<n;i++)if(__gcd(i,n-i)==1)ans++;
	if(n==1)ans=1;return ans;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%d",&m);
	while(m--){
		scanf("%d%d",&n,&k);
		k=(k+1)/2;
		while(k--)n=f(n);
		printf("%d\n",n);
	}
}

#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,k,t;
long long a[40];
int gcd(int a,int b)
{
	if(a%b==0)return b;
	else return gcd(b,a%b);
}
int f(int k)
{
	int ret=0;
	if(k==1)return 1;
	for(int i=1;i<k;i++)
		if(gcd(i,k-i)==1)ret++;
	return ret;
}
int g(int k)
{
	int ret=0;
	for(int i=1;i<=k;i++)
		if(k%i==0)ret+=f(i);
	return ret;
}
int F(int n,int k)
{
	if(k==0)return n;
	int x=F(n,k-1);
	if(x==1)return x;
	for(int i=2;i<=40;i++)
		if(x==a[i]){
			if(k%2==1)x/=2;
			return x;
		}
	if(k%2==0)return g(x);
	else return f(x);
}
signed main()
{
//	cout<<f(1000000)<<endl;
//	k=6;
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lld",&t);
	a[1]=1;
	for(int i=2;i<=40;i++)
		a[i]=a[i-1]*2;
//	k=10;
	while(t--){
//		n=231423;
//		k++;
		scanf("%lld%lld",&n,&k);
		printf("%lld\n",F(n,k));
	}
	return 0;
}

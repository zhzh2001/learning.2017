#include<cstdio>
#include<memory.h>
#include<algorithm>
#include<vector>
#define maxn 400050
#define ll int
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll dep[maxn],head[maxn],nxt[maxn],vet[maxn],val[maxn],fa[maxn][21],mx[maxn][21],tot,bin[100],ans,n,Q;
void insert(ll x,ll y,ll w){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;	}
void dfs(ll x){
	dep[x]=dep[fa[x][0]]+1;
	For(i,1,20)	fa[x][i]=fa[fa[x][i-1]][i-1],mx[x][i]=max(mx[x][i-1],mx[fa[x][i-1]][i-1]);
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=fa[x][0])	fa[vet[i]][0]=x,mx[vet[i]][0]=val[i],dfs(vet[i]);
}
ll lca(ll x,ll y){
	if (dep[x]<dep[y])	swap(x,y);
	ll t=dep[x]-dep[y];
	For(i,0,20)	if (t&bin[i])	x=fa[x][i];
	FOr(i,20,0)	if (fa[x][i]!=fa[y][i])	x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0];
}
ll QQQ(ll x,ll k){
	if (k<0)	return 0;
	ll ans=0;
	For(i,0,20)	if (k&bin[i])	ans=max(ans,mx[x][i]),x=fa[x][i];
	return ans;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();	Q=read();
	bin[0]=1;	For(i,1,20)	bin[i]=bin[i-1]<<1;
	For(i,2,n){
		ll x=read(),y=read(),w=read();
		insert(x,y,w);	insert(y,x,w);
	}
	dfs(1);
	while(Q--){
		ll opt=read();
		if (opt==1){
			ll x=read()^ans,u=read()^ans,v=read()^ans,t=lca(u,v),szb=lca(t,x);
			if (szb!=t)	ans=max(QQQ(t,dep[t]-dep[szb]-1),QQQ(x,dep[x]-dep[szb]-1));
			else{
				szb=lca(x,u);
				if (szb!=t)	ans=QQQ(x,dep[x]-dep[szb]-1);
				else	szb=lca(x,v),ans=QQQ(x,dep[x]-dep[szb]-1);
			}writeln(ans);
		}else{
			ll x=read()^ans,w=read()^ans;
			fa[++n][0]=x;	mx[n][0]=w;	dep[n]=dep[fa[n][0]]+1;
			For(i,1,20)	fa[n][i]=fa[fa[n][i-1]][i-1],mx[n][i]=max(mx[n][i-1],mx[fa[n][i-1]][i-1]);
		}
	}
}
/*
10 10
10 1 237837066
7 4 333101381
5 6 764410644
2 9 800406055
2 3 977449294
10 9 85077389
6 8 883423879
7 5 483282546
3 4 109819496
2 2 237524075
1 7 3 2
2 333101376 243353439
2 333101391 970164111
2 333101389 489066126
1 333101377 333101376 333101383
2 5 947013122
2 15 885096691
1 7 9 10
1 977449284 977449286 977449291
*/


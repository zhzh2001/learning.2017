#include<ctime>
#include<cstdio>
#include<memory.h>
#include<algorithm>
#define ll long long
#define maxn 1010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll f[1000000],g[1000000];
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;}
int main(){
	freopen("function.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll n=6000;
	For(i,1,n){
		ll ans=0;
		For(j,1,i-1)	f[i]+=gcd(j,i-j)==1;
	}f[1]=1;
	For(i,1,n)	For(j,1,i)	if (!(i%j))	g[i]+=f[j];
	ll T=read();
	while(T--){
		ll n=read(),k=read();
		FOr(i,k,1)	if (i&1)	n=f[n];	else	n=g[n];
		writeln(n);
	}
}

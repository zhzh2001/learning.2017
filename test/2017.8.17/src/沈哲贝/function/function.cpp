#include<ctime>
#include<cmath>
#include<cstdio>
#include<memory.h>
#include<map>
#include<algorithm>
#define ll long long
#define maxn 1010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
map<ll,ll>mp;
const ll N=3000000;
ll f[3000010];
const ll mod=1e9+7;
void init(){
	For(i,1,N){
		f[i]+=i;
		for(ll j=i+i;j<=N;j+=i)	f[j]-=f[i];
	}
}
ll work(ll x){
	if (x<=N)	return f[x];
	if (mp[x])	return mp[x];
	ll ans=x-1;
	for(ll i=2;i*i<=x;i++)	if (!(x%i)){
		ans-=work(i);
		if (i*i!=x)	ans-=work(x/i);
	}
	return mp[x]=ans;
}
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	init();	ll T=read();
	while(T--){
		ll n=read(),k=(read()+1)>>1;
		For(i,1,k){
			n=work(n);
			if (n==1)	break;
		}
		writeln(n%mod);
	}
}
/*
23333333332 1
*/

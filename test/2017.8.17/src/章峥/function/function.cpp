#include <fstream>
using namespace std;
const int MOD = 1000000007;
ifstream fin("function.in");
ofstream fout("function.out");
long long phi(long long x)
{
	long long ans = x;
	for (long long i = 2; i * i <= x; i++)
		if (x % i == 0)
		{
			ans = ans / i * (i - 1);
			while (x % i == 0)
				x /= i;
		}
	if (x > 1)
		ans = ans / x * (x - 1);
	return ans;
}
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		long long n, k;
		fin >> n >> k;
		for (int i = 1; i * 2 - 1 <= k && n > 1; i++)
			n = phi(n);
		fout << n % MOD << endl;
	}
	return 0;
}
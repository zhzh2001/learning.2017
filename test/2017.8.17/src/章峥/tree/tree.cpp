#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 400005, LOGN = 20;
typedef pair<int, int> pii;
vector<pii> mat[N];
int dep[N], f[N][LOGN], st[N][LOGN];
void dfs(int k, int fat)
{
	for (vector<pii>::iterator it = mat[k].begin(); it != mat[k].end(); ++it)
		if (it->first != fat)
		{
			dep[it->first] = dep[k] + 1;
			f[it->first][0] = k;
			st[it->first][0] = it->second;
			dfs(it->first, k);
		}
}
int lca(int u, int v)
{
	if (dep[u] < dep[v])
		swap(u, v);
	int delta = dep[u] - dep[v];
	for (int i = 0; delta; i++, delta /= 2)
		if (delta & 1)
			u = f[u][i];
	if (u == v)
		return u;
	for (int i = LOGN - 1; i >= 0; i--)
		if (f[u][i] != f[v][i])
		{
			u = f[u][i];
			v = f[v][i];
		}
	return f[u][0];
}
int maxdist(int u, int top)
{
	int ret = 0, delta = dep[u] - dep[top];
	for (int i = 0; delta; i++, delta /= 2)
		if (delta & 1)
		{
			ret = max(ret, st[u][i]);
			u = f[u][i];
		}
	return ret;
}
int main()
{
	int n, q;
	fin >> n >> q;
	for (int i = 1; i < n; i++)
	{
		int u, v, w;
		fin >> u >> v >> w;
		mat[u].push_back(make_pair(v, w));
		mat[v].push_back(make_pair(u, w));
	}
	dfs(1, 0);
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i <= n; i++)
		{
			f[i][j] = f[f[i][j - 1]][j - 1];
			st[i][j] = max(st[i][j - 1], st[f[i][j - 1]][j - 1]);
		}
	int lastans = 0;
	while (q--)
	{
		int opt;
		fin >> opt;
		if (opt == 1)
		{
			int a, u, v;
			fin >> a >> u >> v;
			a ^= lastans;
			u ^= lastans;
			v ^= lastans;
			int anc = lca(u, v), anc2 = lca(anc, a);
			if (anc2 != anc)
				lastans = max(maxdist(anc, anc2), maxdist(a, anc2));
			else
				lastans = min(maxdist(a, lca(a, u)), maxdist(a, lca(a, v)));
			fout << lastans << endl;
		}
		else
		{
			int v, w;
			fin >> v >> w;
			v ^= lastans;
			w ^= lastans;
			dep[++n] = dep[v] + 1;
			f[n][0] = v;
			st[n][0] = w;
			for (int i = 1; i < LOGN; i++)
			{
				f[n][i] = f[f[n][i - 1]][i - 1];
				st[n][i] = max(st[n][i - 1], st[f[n][i - 1]][i - 1]);
			}
		}
	}
	return 0;
}
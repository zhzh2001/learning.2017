#include<iostream>
#include<cstdio>
using namespace std;
inline int read(){
	int x=0,pos=0; char ch;
	for(ch=getchar();!isdigit(ch);ch=getchar())if(ch=='-')pos=1;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?-x:x;
}
#include<vector>
#include<algorithm>
const int N=100005;
int nedge,ans,n,a[100005],s;
long long dp[N];
vector<int> v[N];
inline bool cmp(int a,int b){
	return dp[a]>dp[b];
}
inline void dfs(int p,int fa){
	if(a[p]<0){dp[p]=-1e18; return;}
	for(int i=0;i<v[p].size();i++)if(v[p][i]!=fa){
		a[v[p][i]]--;
		dfs(v[p][i],p);
	}
	sort(&v[p][0],&v[p][v[p].size()],cmp);
	for(int i=0;i<v[p].size()&&a[p];i++)if(v[p][i]!=fa){
		a[p]--; if(2+dp[v[p][i]>0])dp[p]+=2+dp[v[p][i]];
	}
	for(int i=0;i<v[p].size()&&a[p];i++)if(v[p][i]!=fa){
		dp[p]+=min(a[p],a[v[p][i]])*2; a[p]-=min(a[p],a[v[p][i]]);
	}
}
int main(){
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		v[x].push_back(y); v[y].push_back(x);
	}
	s=read();
	dfs(s,0);
	cout<<dp[s]<<endl;
}

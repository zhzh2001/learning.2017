#include<iostream>
#include<cstdio>
using namespace std;
inline int read(){
	int x=0,pos=0; char ch;
	for(ch=getchar();!isdigit(ch);ch=getchar())if(ch=='-')pos=1;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?-x:x;
}
const int N=100005;
int nedge,ans,nextt[N<<1],ed[N<<1],son[N],n,a[100005],s;
inline void aedge(int a,int b){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b;
}
inline void dfs(int p,int sum){
	if(p==s){
		ans=max(ans,sum);
	}
	for(int i=son[p];i;i=nextt[i])if(a[ed[i]]){
		a[ed[i]]--; dfs(ed[i],sum+1); a[ed[i]]++;
	}
}
int main(){
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		aedge(x,y); aedge(y,x);
	}
	s=read();
	dfs(s,0);
	cout<<ans<<endl;
}

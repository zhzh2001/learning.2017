#include<iostream>
#include<cstdio>
using namespace std;
inline int read(){
	int x=0,pos=0; char ch;
	for(ch=getchar();!isdigit(ch);ch=getchar())if(ch=='-')pos=1;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?-x:x;
}
const int mod=1000003;
int ans[1005];
int main(){
	freopen("cookies.in","r",stdin); freopen("cookies.out","w",stdout);
	int n=read();
	ans[1]=1;
	for(int i=2;i<=n;i++)ans[i]=ans[i-1]*3%mod;
	cout<<ans[n]<<endl;
}

#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#define mo 1000003
#define For(i, x, y) for (i = x; i <= y; i++)
using namespace std;
int i, n, an;
int main()
{
	freopen("cookies.in", "r", stdin);
	freopen("cookies.out", "w", stdout);
	scanf("%d", &n);
	an = 1;
	For(i, 1, n - 1) an = an * 3 % mo;
	printf("%d\n", an);
	return 0;
}
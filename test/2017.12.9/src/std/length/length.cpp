#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#define N 1010
#define db double
#define For(i, x, y) for (i = x; i <= y; i++)
using namespace std;
int i, j, k, n, m;
int f1[N], f2[N];
db s, an, x[N], y[N], sx[N], sy[N], L[N], R[N], re[N];
char p[N][N];
inline void work1()
{
	For(i, 1, m) if (f2[i])
	{
		L[i] = f2[i] - 1;
		if (f2[i - 1] && f2[i] > f2[i - 1])
			L[i] += L[i - 1];
	}
	for (i = m; i; i--)
		if (f2[i])
		{
			R[i] = f2[i] - 1;
			if (f2[i + 1] && f2[i] > f2[i + 1])
				R[i] += R[i + 1];
			re[i] = L[i] + R[i] - (f2[i] - 1);
		}
	For(i, 1, n) For(j, 1, m) if (p[i][j] == '.')
	{
		if (f2[j] && f2[j] < i)
			an += 2 * re[j];
	}
}
inline void work2()
{
	For(i, 1, n) if (f1[i])
	{
		L[i] = f1[i] - 1;
		if (f1[i - 1] && f1[i] > f1[i - 1])
			L[i] += L[i - 1];
	}
	for (i = n; i; i--)
		if (f1[i])
		{
			R[i] = f1[i] - 1;
			if (f1[i + 1] && f1[i] > f1[i + 1])
				R[i] += R[i + 1];
			re[i] = L[i] + R[i] - (f1[i] - 1);
		}
	For(i, 1, n) For(j, 1, m) if (p[i][j] == '.')
	{
		if (f1[i] && f1[i] < j)
			an += 2 * re[i];
	}
}
int main()
{
	freopen("length.in", "r", stdin);
	freopen("length.out", "w", stdout);
	memset(x, 0, sizeof(x));
	memset(y, 0, sizeof(y));
	For(i, 1, N - 1) f1[i] = f2[i] = 0;
	scanf("%d%d", &n, &m);
	For(i, 1, n) scanf("%s", p[i] + 1);
	s = n * m;
	an = 0;
	For(i, 1, n) For(j, 1, m) if (p[i][j] == 'X') f1[i] = j, f2[j] = i, s--;
	else x[i]++, y[j]++;
	For(i, 1, N - 1)
	{
		sx[i] = sx[i - 1] + x[i] * i, sy[i] = sy[i - 1] + y[i] * i;
		x[i] += x[i - 1], y[i] += y[i - 1];
	}
	For(i, 1, n) For(j, 1, m) if (p[i][j] == '.')
	{
		an += x[i] * i - sx[i] + y[j] * j - sy[j];
		an += sx[N - 1] - sx[i] - (s - x[i]) * i + sy[N - 1] - sy[j] - (s - y[j]) * j;
	}
	work1();
	work2();
	For(i, 1, n / 2) For(j, 1, m) swap(p[i][j], p[n - i + 1][j]);
	For(i, 1, n) For(j, 1, m / 2) swap(p[i][j], p[i][m - j + 1]);
	For(i, 1, N - 1) f1[i] = f2[i] = 0;
	For(i, 1, n) For(j, 1, m) if (p[i][j] == 'X') f1[i] = j, f2[j] = i;
	work1();
	work2();
	printf("%.6lf\n", an / s / s);
	return 0;
}
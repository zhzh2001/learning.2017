#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
bool vis[2001],flag;
int n,m,last=-10,tot;
int main()
{	
	freopen("length.in","w",stdout);
	srand(time(0));
	n=1000;m=1000;
	cout<<n<<' '<<m<<endl;
	For(i,1,n)
	{
		int nop=rand()%3;
		if(flag)	nop=1;
		if(nop==1)	{For(i,1,m)	putchar('.');puts("");continue;}
		int tx=rand()%m+1;
		while(vis[tx]||abs(last-tx)<=1)	{tx--;	if(tx==0)	tx=m;}
		For(j,1,m)	if(j==tx)	putchar('X');else putchar('.');
		puts("");
		last=tx;
		vis[tx]=1;tot++;
		if(tot==m)	flag=1;
	}
}

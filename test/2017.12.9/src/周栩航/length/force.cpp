#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
ll ANS,tot,d[51][51];
int n,m,qx[20001],qy[20001],jl[51][51];
char s[201];
bool no[201][201];
int dx[]={0,1,0,-1,0};
int dy[]={0,0,1,0,-1};
inline void get(int x1,int y1,int x2,int y2)
{
	int l=0,r=0,flag=0;
	memset(d,0,sizeof d);
	d[x1][y1]=1;l=r=1;qx[l]=x1;qy[l]=y1;
	while(l<=r)
	{
		For(dir,1,4)
		{
			int nx=qx[l]+dx[dir],ny=qy[l]+dy[dir];
			if(nx<1||nx>n||ny<1||ny>m)	continue;
			if(d[nx][ny])	continue;
			if(no[nx][ny])	continue;
			qx[++r]=nx;qy[r]=ny;
			d[nx][ny]=d[qx[l]][qy[l]]+1;
			if(d[x2][y2])	{flag=1;break;}
		}
		if(flag)	break;
		++l;
	}
	ANS+=d[x2][y2]-1;tot++;
}
int main()
{
	freopen("length.in","r",stdin);
	freopen("length1.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		scanf("\n%s",s+1);
		For(j,1,m)	if(s[j]=='X')	no[i][j]=1;
	}
	For(i,1,n)
		For(j,1,m)
			For(i1,1,n)
				For(j1,1,m)
					if(no[i][j]||no[i1][j1])	continue;
					else	get(i,j,i1,j1);
	printf("%.6lf\n",1.0*ANS/tot);

}

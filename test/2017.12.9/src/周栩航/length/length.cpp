#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
ll n,m,SUM,tot,lx=-109,ly=-109;
char s[2001];
ll qx[5001],qy[5001],toy[5001],tox[5001];
ll S[1001][1001];
inline ll sqr(ll x){return x*x;}
inline ll dis(ll x1,ll y1,ll x2,ll y2){return abs(x1-x2)+abs(y1-y2);}
int main()
{

	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		For(j,1,m)
		{
			ll qian=i*(i-1)/2,hou=(n-i)*(n-i+1)/2;
			ll shang=j*(j-1)/2,xia=(m-j)*(m-j+1)/2;
			ll sum=(qian+hou)*m+(shang+xia)*n;
			SUM+=sum;
			S[i][j]=sum;
		}
	}
	For(i,1,n)
	{
		scanf("\n%s",s+1);
		For(j,1,m)
		{
			if(s[j]=='X')
			{
				tot++;qx[tot]=i;qy[tot]=j;
				SUM-=S[i][j]*2;
				toy[i]=j;tox[j]=i;
			}
		}
	}
	For(i,1,tot)
		For(j,1,tot)	SUM+=dis(qx[i],qy[i],qx[j],qy[j]);
	For(i,1,tot)
	{
		ll num=qy[i]-1;
		ll tep=qx[i],now=qy[i];
		while(1)
		{
			SUM+=(m-now)*2*num;
			if(!toy[tep-1])	break;
			tep--;
			if(toy[tep]<now)	break;
			now=toy[tep];
		}
		tep=qx[i];now=qy[i];
		while(1)
		{
			if(tep!=qx[i])
				SUM+=(m-now)*2*num;
			if(!toy[tep+1])	break;
			tep++;
			if(toy[tep]<now)	break;
			now=toy[tep];
		}
		
		num=m-qy[i];
		tep=qx[i],now=qy[i];
		while(1)
		{
			SUM+=(now-1)*2*num;
			if(!toy[tep-1])	break;
			tep--;
			if(toy[tep]>now)	break;
			now=toy[tep];
		}
		tep=qx[i],now=qy[i];
		while(1)
		{
			if(tep!=qx[i])
				SUM+=(now-1)*2*num;
			if(!toy[tep+1])	break;
			tep++;
			if(toy[tep]>now)	break;
			now=toy[tep];
		}
		
		num=qx[i]-1;
		tep=qy[i],now=qx[i];
		while(1)
		{
			SUM+=(n-now)*2*num;
			if(!tox[tep-1])	break;
			tep--;
			if(tox[tep]<now)	break;
			now=tox[tep];
		}
		tep=qy[i];now=qx[i];
		while(1)
		{
			if(tep!=qy[i])
				SUM+=(n-now)*2*num;
			if(!tox[tep+1])	break;
			tep++;
			if(tox[tep]<now)	break;
			now=tox[tep];
		}
		
		num=n-qx[i];	
		tep=qy[i];now=qx[i];
		while(1)
		{
			SUM+=(now-1)*2*num;
			if(!tox[tep-1])	break;
			tep--;
			if(tox[tep]>now)	break;
			now=tox[tep];
		}	
		tep=qy[i];now=qx[i];
		while(1)
		{
			if(tep!=qy[i])
				SUM+=(now-1)*2*num;
			if(!tox[tep+1])	break;
			tep++;
			if(tox[tep]>now)	break;
			now=tox[tep];
		}
		
	}
	double ans=1.0*SUM/sqr(n*m-tot);
	printf("%.6lf",ans);
}

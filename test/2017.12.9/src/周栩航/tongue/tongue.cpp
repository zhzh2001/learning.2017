#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
int poi[2001],nxt[2001],f[2001],a[2001],S,ans,cnt,n;
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=f[y];f[y]=cnt;
}
inline void dfs(int x,int v)
{
	if(x==S)	ans=max(ans,v);
	for(int i=f[x];i;i=nxt[i])
	{
		if(a[poi[i]])	
		{
			a[poi[i]]--;
			dfs(poi[i],v+1);
			a[poi[i]]++;
		}
	}
}
int main()
{
	freopen("tongue.in","r",stdin);freopen("tongue.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();
	For(i,1,n-1)	add(read(),read());
	S=read();
	dfs(S,0);
	writeln(ans);
}
/*
5
1 3 1 3 2
2 5
3 4
4 5
1 5
4
*/

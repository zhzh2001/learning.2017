#include<iostream>
#include<cmath>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
ll mo=1e6+3,n;
ll ksm(ll x,int y){ll sum=1;for(;y;y/=2){if(y&1)	sum=sum*x%mo;x=x*x%mo;}	return sum;}
int main()
{
	freopen("cookies.in","r",stdin);freopen("cookies.out","w",stdout);
	n=read();
	writeln(ksm(3,n-1));
}

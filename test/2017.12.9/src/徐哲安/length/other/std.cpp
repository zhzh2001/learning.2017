#include<bits/stdc++.h>
using namespace std;

const int maxn = 509;
const int dx[4] = {1, -1, 0, 0};
const int dy[4] = {0, 0, -1, 1};
char s[maxn][maxn];
int d[maxn][maxn], q[maxn*maxn][2];
int l, r, n, m, tot, sum, x, y, xx, yy;
long double ans;

int main() {
	freopen("length.in", "r", stdin);
	freopen("std.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i=1; i<=n; i++) {
		scanf("%s", s[i]+1);
		for (int j=1; j<=m; j++)
			sum += (s[i][j] == '.');
	}
	for (int i=1; i<=n; i++) 
		for (int j=1; j<=m; j++) if (s[i][j] == '.') {
			l = r = 0; q[r][0] = i; q[r++][1] = j;
			memset(d, 0x3f, sizeof d); d[i][j] = 0; tot = 0;
			while (l < r) {
				x = q[l][0]; y = q[l++][1];
				tot += d[x][y];
				for (int k=0; k<=3; k++) {
					xx = x + dx[k]; yy = y + dy[k];
					if (xx < 1 || xx > n || yy < 1 || yy > m) continue;
					if (d[xx][yy] <= d[x][y] + 1) continue;
					if (s[xx][yy] == 'X') continue;
					d[xx][yy] = d[x][y] + 1;
					q[r][0] = xx; q[r++][1] = yy;
				}
			}
			ans += (long double) tot / sum;
		}
	ans /= sum;
	printf("%.6Lf\n", ans);
	return 0;
}

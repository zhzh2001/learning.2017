#include<bits/stdc++.h>
using namespace std;

const int maxn = 1009, MOD = 1000003;
int f[maxn], n;

int main() {
	freopen("cookies.in", "r", stdin);
	freopen("cookies.out", "w", stdout);
	scanf("%d", &n);
	f[0] = f[1] = 1;
	for (int i=2; i<=n; i++) 
		f[i] = 3 * f[i-1] % MOD;
	printf("%d\n", f[n]);
	return 0;
}

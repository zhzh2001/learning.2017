#include<bits/stdc++.h>
#define rep(i,a,b) for (int i=(a); i<=int(b); i++)
using namespace std;

const int maxn = 99;
vector<int> e[maxn];
int a[maxn], now, n, root, u, v, best;

void dfs(int u) {
	if (u == root) best = max(best, now);
	rep (i, 0, e[u].size()-1) if (a[e[u][i]]) {
		a[e[u][i]]--; now++;
		dfs(e[u][i]);
		a[e[u][i]]++; now--;
	}
}

int main() {
	freopen("tongue.in", "r", stdin);
	freopen("std.out", "w", stdout);
	scanf("%d", &n);
	for (int i=1; i<=n; i++) scanf("%d", &a[i]);
	for (int i=1; i<=n-1; i++) {
		scanf("%d%d", &u, &v);
		e[u].push_back(v);
		e[v].push_back(u);
	}
	scanf("%d", &root);
	dfs(root);
	printf("%d\n", best);
	return 0;
}

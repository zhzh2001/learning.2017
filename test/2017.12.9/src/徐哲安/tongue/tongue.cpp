#include<bits/stdc++.h>
#define rep(i,a,b) for (int i=(a); i<=int(b); i++)
using namespace std;
typedef long long LL;

inline int read() {
	char ch = getchar(); int x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }
	return op * x;
}

const int maxn = 200009;
vector<int> e[maxn];
LL f[maxn], a[maxn];
int rk[maxn], n, root, u, v, now;

bool cmp(int x, int y) {
	return f[e[now][x]] > f[e[now][y]];
}

void dfs(int u, int fa) {
	if (!a[u]) return;
	if (a[u] >= e[u].size()) {
		f[u] = e[u].size();
		a[u] -= e[u].size();
		rep (i, 0, e[u].size()-1)
			if (e[u][i] != fa) {
				dfs(e[u][i], u);
				f[u] += f[e[u][i]];
				if (a[e[u][i]]) {
					f[u] += 2 * min(a[u], a[e[u][i]]);
					a[u] -= min(a[u], a[e[u][i]]);
				}
			}
		return;
	}
	int m = 0;
	rep (i, 0, e[u].size()-1)
		if (e[u][i] != fa) dfs(e[u][i], u);
	rep (i, 0, e[u].size()-1)
		if (e[u][i] != fa) rk[++m] = i;
	now = u; sort(rk+1, rk+m+1, cmp);
	rep (i, 1, a[u]-(fa != 0)) 
		f[u] += f[e[u][rk[i]]];
	f[u] += a[u]; a[u] = 0;
}

int main() {
	freopen("tongue.in", "r", stdin);
	freopen("tongue.out", "w", stdout);
	
	n = read();
	rep (i, 1, n) a[i] = read();
	rep (i, 1, n-1) {
		u = read(); v = read();
		if (a[v]) e[u].push_back(v);
		if (a[u]) e[v].push_back(u);
	}
	root = read();
	
	dfs(root, 0);
	printf("%lld\n", f[root]);
	
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
bool can[52][52];
char s[52];
int dist[52][52];
pair<int,int> q[10002];
int dx[4]={1,0,-1,0};
int dy[4]={0,1,0,-1};
void bfs(int x,int y){
	memset(dist,0x3f,sizeof(dist));
	dist[x][y]=0;
	int l=1,r=1;
	q[1]=make_pair(x,y);
	while(l<=r){
		pair<int,int> u=q[l++];
		for(int i=0;i<4;i++){
			int nx=u.first+dx[i],ny=u.second+dy[i];
			if (!can[nx][ny] || dist[nx][ny]<INF)
				continue;
			dist[nx][ny]=dist[u.first][u.second]+1;
			q[++r]=make_pair(nx,ny);
		}
	}
}
int main(){
	init();
	int n=readint(),m=readint(),cnt=0;
	for(int i=1;i<=n;i++){
		readstr(s+1);
		for(int j=1;j<=m;j++){
			can[i][j]=s[j]=='.';
			cnt+=can[i][j];
		}
	}
	ll sum=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			if (can[i][j]){
				bfs(i,j);
				for(int k=1;k<=n;k++)
					for(int o=1;o<=m;o++)
						if (can[k][o])
							sum+=dist[k][o];
			}
		}
	//fprintf(stderr,"%lld\n",sum);
	long double ans=(long double)sum/(cnt*cnt);
	printf("%.6f",(double)ans);
}
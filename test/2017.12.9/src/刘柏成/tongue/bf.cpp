#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tongue.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[100003];
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[200006];
	int first[100003];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int rt,ans;
	int fa[100003];
	void dfs(int u){
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v!=fa[u]){
				fa[v]=u;
				dfs(v);
			}
		}
	}
	void dfs2(int u,int now){
		if (u==rt){
			ans=max(ans,now);
			if (!a[u])
				return;
			//return;
		}
		int w=u;
		while(w!=rt){
			w=fa[w];
			if (!a[w])
				return;
		}
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (a[v]){
				a[v]--;
				dfs2(v,now+1);
				a[v]++;
			}
		}
	}
	int work(int rt){
		this->rt=rt;
		dfs(rt);
		dfs2(rt,0);
		return ans;
	}
}g;
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	for(int i=1;i<n;i++){
		int u=readint(),v=readint();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	printf("%d",g.work(readint()));
}
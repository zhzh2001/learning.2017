#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000200;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll a[100003];
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[200006];
	int first[100003];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	ll f[100003],g[100003];
	pair<ll,ll> now[100003];
	void dfs(int u,int fa){
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa || !a[v])
				continue;
			dfs(v,u);
		}
		int cur=0;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa || !a[v])
				continue;
			now[++cur]=make_pair(f[v],g[v]);
		}
		if (a[u]>cur){
			f[u]=cur;
			g[u]=a[u]-cur-1;
			for(int i=1;i<=cur;i++){
				f[u]+=now[i].first+1;
				ll t=min(g[u],now[i].second);
				g[u]-=t;
				f[u]+=t*2;
			}
		}else{
			g[u]=0;
			f[u]=a[u]-1;
			sort(now+1,now+cur+1,greater<pair<int,int> >());
			for(int i=1;i<a[u];i++)
				f[u]+=now[i].first+1;
		}
	}
	ll work(int u){
		a[u]++;
		dfs(u,0);
		return f[u];
	}
}g;
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	g.n=n;
	for(int i=1;i<n;i++){
		int u=readint(),v=readint();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	int rt=readint();
	printf("%lld",g.work(rt));
}
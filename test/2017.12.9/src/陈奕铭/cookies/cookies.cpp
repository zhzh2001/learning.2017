#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
int n;
const int P = 1000003;

inline int quickmi(int a,int n){
	int ans = 1;
	for(;n;n >>= 1,a = 1ll*a*a%P)
		if(n&1) ans = 1ll*ans*a%P;
	return ans;
}

int main(){
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	n = read();
	printf("%d\n",quickmi(3,n-1));
	return 0;
}
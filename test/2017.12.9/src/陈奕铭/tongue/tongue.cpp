#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
vector<ll> sonNum[N];
ll nowNum[N];
ll sizeOfSon[N];
int head[N],nxt[N*2],to[N*2],cnt;
int n,St;
ll dp[N];

inline bool cmp(ll a,ll b){
	return a > b;
}

inline void addedge(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
	to[++cnt] = a; nxt[cnt] = head[b]; head[b] = cnt;
}

void treeDp(int x,int f){
	nowNum[x]--; dp[x] = 1;
	if(!nowNum[x]) return;
	for(int i = head[x];i;i = nxt[i]){
		int v = to[i]; if(v == f) continue;
		if(nowNum[v] == 0) continue;
		treeDp(v,x); sonNum[x].push_back(dp[v]);
		sizeOfSon[x] += nowNum[v];
	}
	sort(sonNum[x].begin(),sonNum[x].end());
	for(int i = sonNum[x].size()-1;i >= 0;--i){
		dp[x] += sonNum[x][i]+1;
		nowNum[x]--;
		if(nowNum[x] == 0) break;
	}
	if(nowNum[x] > 0){
		ll dps = min(nowNum[x],sizeOfSon[x]);
		nowNum[x] -= dps;
		dp[x] += dps*2;
	}
	return;
}

int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i)
		nowNum[i] = read();
	for(int i = 1;i < n;++i){
		int a = read(),b = read();
		addedge(a,b);
	}
	St = read();
	++nowNum[St];
	treeDp(St,St);
	printf("%lld\n",dp[St]-1);
	return 0;
}
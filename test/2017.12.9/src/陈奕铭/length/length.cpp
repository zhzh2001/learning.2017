#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1005;
int n,m;
char mp[N][N];
int lineX[N],hangX[N];
ll sum[N][N],Xs[N][N];
ll sum2[N][N];
ll ans,num;

inline ll getX(int x1,int y1,int x2,int y2){
	return Xs[x2][y2]-Xs[x2][y1-1]-Xs[x1-1][y2]+Xs[x1-1][y1-1];
}
inline ll getSum(int x1,int y1,int x2,int y2){
	return sum[x2][y2]-sum[x2][y1-1]-sum[x1-1][y2]+sum[x1-1][y1-1];
}

int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= n;++i){
		scanf("%s",mp[i]+1);
		for(int j = 1;j <= m;++j){
			if(mp[i][j] == 'X'){
				lineX[j] = i;
				hangX[i] = j;
				sum[i][j] = sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1];
				Xs[i][j] = Xs[i-1][j]+Xs[i][j-1]-Xs[i-1][j-1]+1;
			}
			else{
				sum[i][j] = sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+i+j-2;
				Xs[i][j] = Xs[i-1][j]+Xs[i][j-1]-Xs[i-1][j-1];
			}
		}
	}
	for(int i = 1;i <= n;++i)
		for(int j = m;j;--j){
			if(mp[i][j] == 'X'){
				sum2[i][j] = sum2[i-1][j]+sum2[i][j+1]-sum2[i-1][j+1];
			}
			else{
				sum2[i][j] = sum2[i-1][j]+sum2[i][j+1]-sum2[i-1][j+1]+j-1+n-i;
			}
		}
	for(int i = 1;i <= n;++i)
		for(int j = 1;j <= m;++j)
			if(mp[i][j] != 'X'){
				if(i < n && j < m){
					ll Num = (n-i)*(m-j)-getX(i+1,j+1,n,m);
					ans += getSum(i+1,j+1,n,m)-(i+j-2)*Num;
					num += Num;
				}
				if(i < n){
					if(lineX[j] > i){
						ll p = n-i;
						num += p-1;
						ans += p*(p+1)/2-(lineX[j]-i)+2*(n-lineX[j]);
					}
					else{
						ll p = n-i;
						num += p;
						ans += p*(p+1)/2;
					}
				}
				if(j < m){
					if(hangX[i] > j){
						ll p = m-j;
						num += p-1;
						ans += p*(p+1)/2-(hangX[i]-j)+2*(m-hangX[i]);
					}
					else{
						ll p = m-j;
						num += p;
						ans += p*(p+1)/2;
					}
				}
				if(i > 1 || j < m){
					ll Num = (m-j)*(i-1)-getX(1,j+1,i-1,m);
					ans += sum2[i-1][j+1]-(n-i+j-1)*Num;
					num += Num;
				}
			}
	ans *= 2; num *= 2; num += n*m-Xs[n][m];
	double Ans = ((long double)ans/(long double)num);
	printf("%.6lf\n",Ans);
	// printf("%lld\n",ans);
	// printf("%lld\n",num);
	return 0;
}
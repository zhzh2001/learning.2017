#include <fstream>
#include <string>
#include <queue>
#include <algorithm>
using namespace std;
ifstream fin("length.in");
ofstream fout("length.ans");
const int N = 100, INF = 1e9, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
string mat[N];
int d[N][N], row[N], col[N];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 0; i < n; i++)
	{
		fin >> mat[i];
		for (int j = 0; j < m; j++)
			if (mat[i][j] == 'X')
			{
				row[i] = j;
				col[j] = i;
			}
	}
	int cnt = 0;
	long long ans = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
		{
			if (mat[i][j] == 'X')
				continue;
			cnt++;
			fill_n(&d[0][0], sizeof(d) / sizeof(int), INF);
			d[i][j] = 0;
			typedef pair<int, int> point;
			queue<point> Q;
			Q.push(make_pair(i, j));
			while (!Q.empty())
			{
				point k = Q.front();
				Q.pop();
				for (int dir = 0; dir < 4; dir++)
				{
					int nx = k.first + dx[dir], ny = k.second + dy[dir];
					if (nx >= 0 && nx < n && ny >= 0 && ny < m && mat[nx][ny] == '.' && d[nx][ny] == INF)
					{
						d[nx][ny] = d[k.first][k.second] + 1;
						Q.push(make_pair(nx, ny));
					}
				}
			}
			for (int x = 0; x < n; x++)
				for (int y = 0; y < m; y++)
					if (mat[x][y] == '.')
						ans += d[x][y];
		}
	fout << ans << endl;
	fout << fixed << ans / (1.0 * cnt * cnt) << endl;
	return 0;
}

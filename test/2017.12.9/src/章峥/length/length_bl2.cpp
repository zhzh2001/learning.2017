#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("length.in");
ofstream fout("length.out");
const int N=10005;
int x[N],y[N];
int main()
{
	int n,m;
	fin>>n>>m;
	int cc=0;
	long long ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			char c;
			fin>>c;
			if(c=='.')
			{
				x[++cc]=i;
				y[cc]=j;
			}
			else
				ans+=4*((i-1)*(n-i)+(j-1)*(m-j));
		}
	for(int i=1;i<=cc;i++)
		for(int j=1;j<=cc;j++)
			ans+=abs(x[i]-x[j])+abs(y[i]-y[j]);
	fout<<fixed<<ans/(1.0*cc*cc)<<endl;
	return 0;
}

#include <fstream>
#include <algorithm>
#include <queue>
#include <string>
using namespace std;
ifstream fin("length.in");
ofstream fout("length.out");
const int N = 1005;
int bx[N], by[N], row[N], col[N];
bool r[N][N][2], c[N][N][2];
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x < N; x += x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T, Ts;
int main()
{
	int n, m;
	fin >> n >> m;
	int cc = 0, cb = 0, sumi = 0;
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		row[i] = -1;
	for (int j = 1; j <= m; j++)
		col[j] = -1;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			char c;
			fin >> c;
			if (c == '.')
			{
				ans += i * cc - sumi;
				int cnt = T.query(j);
				ans += T.query(j) * j - Ts.query(j);
				ans += Ts.query(m) - Ts.query(j) - (cc - cnt) * j;
				cc++;
				sumi += i;
				T.modify(j, 1);
				Ts.modify(j, j);
			}
			else
			{
				bx[++cb] = i;
				by[cb] = j;
				row[i] = j;
				col[j] = i;
			}
		}
	ans *= 2;
	for (int i = 1; i <= n; i++)
	{
		r[i][i][0] = r[i][i][1] = ~row[i];
		for (int j = i + 1; j <= n; j++)
		{
			r[i][j][0] = r[i][j - 1][0] && ~row[j] && row[j] > row[j - 1];
			r[i][j][1] = r[i][j - 1][1] && ~row[j] && row[j] < row[j - 1];
		}
	}
	for (int i = 1; i <= m; i++)
	{
		c[i][i][0] = c[i][i][1] = ~col[i];
		for (int j = i + 1; j <= m; j++)
		{
			c[i][j][0] = c[i][j - 1][0] && ~col[j] && col[j] > col[j - 1];
			c[i][j][1] = c[i][j - 1][1] && ~col[j] && col[j] < col[j - 1];
		}
	}
	for (int i = 1; i <= cb; i++)
	{
		ans += 4 * ((bx[i] - 1) * (n - bx[i]) + (by[i] - 1) * (m - by[i]));
		for (int j = 1; j <= cb; j++)
		{
			if (bx[i] < bx[j])
				if (by[i] < by[j] && (bx[i] + 1 == bx[j] || (r[bx[i] + 1][bx[j] - 1][0] && row[bx[i] + 1] > row[bx[i]] && row[bx[j] - 1] < row[bx[j]])))
					ans += 4 * (by[i] - 1) * (m - by[j]);
				else if (by[i] > by[j] && (bx[i] + 1 == bx[j] || (r[bx[i] + 1][bx[j] - 1][1] && row[bx[i] + 1] < row[bx[i]] && row[bx[j] - 1] > row[bx[j]])))
					ans += 4 * (by[j] - 1) * (m - by[i]);
			if (by[i] < by[j])
				if (bx[i] < bx[j] && (by[i] + 1 == by[j] || (c[by[i] + 1][by[j] - 1][0] && col[by[i] + 1] > col[by[i]] && col[by[j] - 1] < col[by[j]])))
					ans += 4 * (bx[i] - 1) * (n - bx[j]);
				else if (bx[i] > bx[j] && (by[i] + 1 == by[j] || (c[by[i] + 1][by[j] - 1][1] && col[by[i] + 1] < col[by[i]] && col[by[j] - 1] > col[by[j]])))
					ans += 4 * (bx[j] - 1) * (n - bx[i]);
		}
	}
	fout << fixed << ans / (1.0 * cc * cc) << endl;
	return 0;
}
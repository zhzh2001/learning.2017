#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("length.in");
const int n = 100, dx[] = {-1, -1, -1, 0, 0, 1, 1, 1}, dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};
bool mat[n + 5][n + 5], row[n + 5], col[n + 5];
inline bool check(int x, int y)
{
	for (int i = 0; i < 8; i++)
		if (mat[x + dx[i]][y + dy[i]])
			return true;
	return false;
}
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> d(n / 2 + 1, n);
	int n = d(gen), m = d(gen), cnt = d(gen);
	while (cnt--)
	{
		uniform_int_distribution<> dx(1, n), dy(1, m);
		int x = dx(gen), y = dy(gen);
		if (!row[x] && !col[y] && !check(x, y))
			mat[x][y] = row[x] = col[y] = true;
	}
	fout << n << ' ' << m << endl;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
			if (mat[i][j])
				fout.put('X');
			else
				fout.put('.');
		fout << endl;
	}
	return 0;
}

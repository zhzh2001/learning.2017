#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("cookies.in");
ofstream fout("cookies.out");
const int MOD=1e6+3;
int main()
{
	int n;
	fin>>n;
	int ans=1;
	for(int i=1;i<n;i++)
		ans=ans*3%MOD;
	fout<<ans<<endl;
	return 0;
}

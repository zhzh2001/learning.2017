#include <fstream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;
ifstream fin("tongue.in");
ofstream fout("tongue.out");
const int N = 100005;
int a[N];
long long f[N];
vector<int> mat[N];
void dfs(int k, int fat)
{
	if (!a[k])
		return;
	vector<long long> vec;
	for (size_t i = 0; i < mat[k].size(); i++)
		if (mat[k][i] != fat)
		{
			dfs(mat[k][i], k);
			vec.push_back(f[mat[k][i]]);
		}
	sort(vec.begin(), vec.end(), greater<long long>());
	a[k]--;
	f[k] = 1;
	for (size_t i = 0; i < vec.size() && a[k] && vec[i]; i++)
	{
		a[k]--;
		f[k] += vec[i] + 1;
	}
	for (size_t i = 0; i < mat[k].size() && a[k]; i++)
		if (mat[k][i] != fat && a[mat[k][i]])
		{
			int cnt = min(a[k], a[mat[k][i]]);
			a[k] -= cnt;
			f[k] += cnt * 2;
		}
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	for (int i = 1; i < n; i++)
	{
		int u, v;
		fin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	int root;
	fin >> root;
	a[root]++;
	dfs(root, 0);
	fout << f[root] - 1 << endl;
	return 0;
}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
#include <utility>
using namespace std;

typedef long long ll;

const int MaxN = 1005;

int n, m;
char mat[MaxN][MaxN];
int row[MaxN], col[MaxN], rmx[MaxN][MaxN], rmn[MaxN][MaxN];

int bf() {
	ll total = 0, crt = 0;
	for (int i = 1; i <= n; ++i) for (int j = 1; j <= m; ++j) {
		if (mat[i][j] == 'X')
			continue;
		for (int i1 = 1; i1 < i; ++i1) for (int j1 = 1; j1 <= m; ++j1) {
			if (mat[i1][j1] == 'X')
				continue;
//			cerr << i1 << ' ' << j1 << endl;
			if (j1 < j) {
				if (rmn[i1 + 1][i] >= row[i1] && i1 < row[i1]) {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;
				} else {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;	
				}
			} else if (j1 == j) {
				if (mat[i1][j1] == 'X')
					continue;
				bool f1 = (i < col[j1]), f2 = (i1 < col[j1]);
				if (f1 == f2) {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;		
				} else {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;	
				}
			} else {
				if (mat[i1][j1] == 'X')
					continue;
				if (rmx[i1 + 1][i] <= row[i1] && i1 > row[i1]) {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;
				} else {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;
				}
			}
		}
		for (int i1 = i; i1 <= i; ++i1) for (int j1 = 1; j1 <= m; ++j1) {
			if (mat[i1][j1] == 'X')
				continue;	
//			cerr << i1 << ' ' << j1 << endl;
			if (j1 < j) {
				if (rmn[i1][i] >= row[i1] && i1 < row[i1]) {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;
				} else {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;	
				}
			} else if (j1 == j) {
				if (mat[i1][j1] == 'X')
					continue;
				bool f1 = (i < col[j1]), f2 = (i1 < col[j1]);
				if (f1 == f2) {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;		
				} else {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;	
				}
			} else {
				if (mat[i1][j1] == 'X')
					continue;
				if (rmx[i1][i] <= row[i1] && i1 > row[i1]) {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;
				} else {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;
				}
			}			
		}
		for (int i1 = i + 1; i1 <= n; ++i1) for (int j1 = 1; j1 <= m; ++j1) {
			if (mat[i1][j1] == 'X')
				continue;
//			cerr << i1 << ' ' << j1 << endl;
			if (j1 < j) {
				if (rmn[i][i1 - 1] >= row[i1] && i1 < row[i1]) {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;
				} else {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;	
				}
			} else if (j1 == j) {
				if (mat[i1][j1] == 'X')
					continue;
				bool f1 = (i < col[j1]), f2 = (i1 < col[j1]);
				if (f1 == f2) {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;		
				} else {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;	
				}
			} else {
				if (mat[i1][j1] == 'X')
					continue;
				if (rmx[i][i1 - 1] <= row[i1] && i1 > row[i1]) {
					total += (abs(i1 - i) + abs(j1 - j) + 2);	
					++crt;
				} else {
					total += (abs(i1 - i) + abs(j1 - j));	
					++crt;
				}
			}
		}
	}
	printf("%.6Lf\n", (long double)total / crt);
	return 0;
}

int main() {
	freopen("length.in", "r", stdin);
	freopen("length.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; ++i) {
		scanf("%s", mat[i] + 1);
		for (int j = 1; j <= m; ++j) {
			if (mat[i][j] == 'X') {
				row[i] = j;
				col[j] = i;
			}
		}
	}
	memset(rmn, 0x3f, sizeof rmn);
	memset(rmx, 0x3f, sizeof rmx);
	for (int i = 1; i <= n; ++i) {
		for (int j = i; j <= n; ++j) {
			rmn[i][j] = min(rmn[i][j], row[j]);
			rmx[i][j] = max(rmx[i][j], row[j]);	
		}
	}
	if (n <= 50 && m <= 50) {
		return bf();
	}
	return 0;
}

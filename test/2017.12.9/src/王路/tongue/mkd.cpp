#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Rand32() {
	return (rand() << 15) + rand();	
}
int Uniform32(int l, int r) {
	return (Rand32() % (r - l + 1)) + l;
}
int fa[1000005];
int Find(int x) {
	return x == fa[x] ? x : fa[x] = Find(fa[x]);
}
bool Same(int x, int y) {
	return Find(x) == Find(y);
}
void Merge(int x, int y) {
	int fx = Find(x), fy = Find(y);
	if (fx != fy)
		fa[fx] = fy;
}

int main() {
	ios::sync_with_stdio(false);
	freopen("tongue.in", "w", stdout);
	srand(GetTickCount());
	int n = 5;
	cout << n << endl;
	for (int i = 1; i <= n; ++i) {
		fa[i] = i;
		cout << Uniform32(1, n) << ' ';
	}
	cout << endl;
	for (int i = 1; i < n; ++i) {
		int a = Uniform32(1, n), b;
		do {
			b = Uniform32(1, n);
		} while (Same(a, b));
		cout << a << ' ' << b << endl;
		Merge(a, b);
	}
	cout << Uniform32(1, n) << endl;
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
#include <utility>
using namespace std;

typedef long long ll;

const int kMaxN = 100005;

int n, a[kMaxN], b[kMaxN], pos, gh[kMaxN], head[kMaxN], cnt;
struct Edge {
	int to, nxt;
} e[kMaxN << 2];
inline void AddEdge(int u, int v) {
	e[++cnt] = (Edge) { v, head[u] };
	head[u] = cnt;
}
inline int Search(int stat, int now, int fat = -1) {
	vector<int> vec;
	for (int i = head[now]; i; i = e[i].nxt) {
		if (e[i].to == fat || (stat & (1 << (e[i].to - 1))) == 0)
			continue;
		vec.push_back(Search(stat, e[i].to, now));
	}
	sort(vec.begin(), vec.end(), greater<int>());
	int ret = 0;
	int lim = a[now] >> 1;
	for (int i = 0; i < lim && i < vec.size() && vec[i] >= 2; ++i) {
		if (a[now] - 2 <= 1)
			continue;
		ret += vec[i];
		a[now] -= 2;
	}
	for (int i = head[now]; i; i = e[i].nxt) {
		if (e[i].to == fat || (stat & (1 << (e[i].to - 1))) == 0)
			continue;
		int ta = a[now] - (now != pos), tb = a[e[i].to];
		ret += 2LL * min(ta, tb);
		a[now] -= min(ta, tb), a[e[i].to] -= min(ta, tb);
	}
	return ret;
}
inline long long Search1(int now, int fat = -1) {
	vector<long long> vec;
	for (int i = head[now]; i; i = e[i].nxt) {
		if (e[i].to == fat)
			continue;
		vec.push_back(Search1(e[i].to, now));
	}
	sort(vec.begin(), vec.end(), greater<long long>());
	int ret = 0;
	int lim = a[now] >> 1;
	for (int i = 0; i < lim && i < vec.size() && vec[i] >= 2; ++i) {
		if (a[now] - 2 <= 1)
			continue;
		ret += vec[i];
		a[now] -= 2;
	}
	for (int i = head[now]; i; i = e[i].nxt) {
		if (e[i].to == fat)
			continue;
		int ta = a[now] - (now != pos), tb = a[e[i].to];
		ret += 2LL * min(ta, tb);
		a[now] -= min(ta, tb), a[e[i].to] -= min(ta, tb);
	}
	return ret;
}
int Release() {
	if (n <= 5) {
		int ans = 0;
		for (int i = 0; i < (1 << n); ++i) {
			memcpy(a, b, sizeof b);
			ans = max(ans, Search(i, pos));
		}
		printf("%d\n", ans);
		return 0;
	} else {
		printf("%lld\n", Search1(pos));	
	}
	return 0;
}
int main() {
	freopen("tongue.in", "r", stdin);
	freopen("tongue.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) {
		scanf("%d", a + i);
	}
	memcpy(b, a, sizeof a);
	for (int i = 1; i < n; ++i) {
		int a, b;
		scanf("%d%d", &a, &b);
		AddEdge(a, b);
		AddEdge(b, a);
 	}
	scanf("%d", &pos);
	return Release();
}

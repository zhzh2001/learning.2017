#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

const int kMaxN = 1e3 + 5, Mod = 1e6 + 3;

typedef long long ll;

int n;

inline ll QuickPow(ll a, ll b) {
	ll ret = 1;
	for (; b; b >>= 1, a = a * a % Mod) {
		if (b & 1)
			ret = ret * a % Mod;
	}	
	return ret;	
}

int main() {
	freopen("cookies.in", "r", stdin);
	freopen("cookies.out", "w", stdout);
	scanf("%d", &n);
	printf("%lld\n", QuickPow(3LL, n - 1));
	return 0;
}

#include<cstdio>
using namespace std;
const int maxn=1e3+5;
double ans;
int n,m,a[maxn][maxn],tot;
inline void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		char s[maxn];
		scanf("%s",s+1);
		for (int j=1;j<=m;j++){
			if (s[j]=='.'){
				a[i][j]=1;
				tot++;
			}else{
				ans+=4*((i-1)*(n-i)+(j-1)*(m-j));
			}
		}
	}
}
inline int abs(int x){return (x>0)?x:-x;}
inline int dis(int x1,int y1,int x2,int y2){
	return abs(x2-x1)+abs(y2-y1);
}
inline void solve(){
	for (int x1=1;x1<=n;x1++){
		for (int y1=1;y1<=m;y1++){
			if (a[x1][y1]){
				for (int x2=1;x2<=n;x2++){
					for (int y2=1;y2<=m;y2++){
						if (a[x2][y2]){
							ans+=dis(x1,y1,x2,y2);
						}
					}
				}
			}
		}
	}
	ans/=tot*tot;
	printf("%.6lf\n",ans);
}
int main(){
	init();
	solve();
	return 0;
}

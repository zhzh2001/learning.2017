#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn=1e3+5;
double ans;
struct point{
	ll x,y;
}q[maxn];
bool flag1[maxn],flag2[maxn];
ll n,m,res,Ans[maxn][maxn],a[maxn][maxn],tot;
inline ll sum(ll x){return (x+1)*x/2;}
inline ll Abs(ll x){return (x>0)?x:-x;}
inline ll dis(ll x1,ll y1,ll x2,ll y2){
	return Abs(x2-x1)+Abs(y2-y1);
}
inline ll query(ll x,ll y){
	ll sum1=Ans[x][y];
	ll sum2=Ans[x][m-y]+x*(m-y);
	ll sum3=Ans[n-x][y]+(n-x)*y;
	ll sum4=Ans[n-x][m-y]+2*(n-x)*(m-y);
	return sum1+sum2+sum3+sum4;
}
inline void init(){
	scanf("%d%d",&n,&m);
	for (ll i=1;i<=n;i++){
		for (ll j=1;j<=m;j++){
			Ans[i][j]=i*j*(i+j)-sum(i)*j-sum(j)*i;
		}
	}
	for (ll i=1;i<=n;i++){
		char s[maxn];
		scanf("%s",s+1);
		for (ll j=1;j<=m;j++){
			if (s[j]=='.'){
				a[i][j]=1;
				tot++;
			}else{
				ans+=4*((i-1)*(n-i)+(j-1)*(m-j));
				q[++res]=(point){i,j};
			}
		}
	}
	for (ll i=1;i<=res;i++){
		for (ll j=1;j<=res;j++){
			ans+=dis(q[i].x,q[i].y,q[j].x,q[j].y);
		}
	}
}
inline bool cmp1(point x,point y){
	return x.x<y.x;
}
inline bool cmp(point x,point y){
	return x.y<y.y;
}
inline void solve(){
	for (ll i=1;i<=n;i++){
		for (ll j=1;j<=m;j++){
			if (a[i][j]){
				ans+=query(i,j);
			}
		}
	}
	for (int i=1;i<=res;i++){
		ans-=query(q[i].x,q[i].y);
	}
	sort(q+1,q+1+res,cmp);
	for (int i=1;i<res;i++){
		if (q[i].y+1==q[i+1].y){
			if (q[i].x>q[i+1].x){
				ans+=4*(q[i+1].x-1)*(n-q[i].x);
			}else{
				ans+=4*(q[i].x-1)*(n-q[i+1].x);
			}
		}
	}
	sort(q+1,q+1+res,cmp1);
	for (int i=1;i<res;i++){
		if (q[i].x+1==q[i+1].x){
			if (q[i].y<q[i+1].y){
				ans+=4*(q[i].y-1)*(m-q[i+1].y);
			}else{
				ans+=4*(q[i+1].y-1)*(m-q[i].y);
			}
		}
	}
	ans/=tot*tot;
	printf("%.6lf\n",ans);
}
int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
typedef long long ll;
const int mod=2e5,maxn=2e5+5,maxm=6e5+5;
ll inf=1e18;
struct edge{
	int link,next,opp;
	ll val;
}e[maxm];
struct E{
	int link,next;
}a[maxn<<1];
int dis[maxn],S,T,q[maxn];
int cur[maxn],root,n,tot,tot1,h[maxn],head[maxn];
inline void add(int u,int v,ll w){
	e[++tot].val=w; e[tot].link=v; e[tot].next=head[u]; head[u]=tot;
}
inline void add1(int u,int v){
	a[++tot1]=(E){v,h[u]}; h[u]=tot1;	
}
inline void ins1(int u,int v){
	add1(u,v); add1(v,u);
}
inline void ins2(int u,int v,ll w){
	add(u,v,w); e[tot].opp=tot+1; add(v,u,0); e[tot].opp=tot-1;
}
inline void init(){
	n=read();
	S=0; T=2*n+1;
	for (int i=1;i<=n;i++){
		ll x=read();
		ins2(i,i+n,x);
		ins2(i+n,T,inf);
	}
	for (int i=1;i<n;i++){
		int u=read(),v=read();
		ins1(u,v);
	}
	root=read();
}
inline void dfs(int u,int fa){
	for (int i=h[u];i;i=a[i].next){
		int v=a[i].link;
		if (v!=fa){
			ins2(u+n,v,inf);
			dfs(v,u);
		}
	}
}
inline bool bfs(){
	memset(dis,-1,sizeof(dis));
	dis[S]=0; int h=0,t=1; q[1]=S;
	while (h!=t){
		int u=q[h=h%mod+1];
		for (int i=head[u];i;i=e[i].next){
			if (!e[i].val) continue;
			int v=e[i].link;
			if (dis[v]==-1) {
				dis[v]=1;
				q[t=t%mod+1]=v;
			}
			if (v==T) return 1;
		}
	}
	return 0;
}
inline void clean(){
	for (int i=S;i<=T;i++) cur[i]=head[i];
}
ll dinic(int u,ll flow){
	if (!flow||u==T){
		return flow;
	}
	ll used=0;
	for (int i=cur[u];i;i=e[i].next){
		int v=e[i].link;
		if (dis[v]==-1||e[i].val==0) continue;
		ll x=dinic(v,min(flow-used,e[i].val)); used+=x; e[i].val-=x; e[e[i].opp].val+=x;
		if (used==flow){
			return used;
		}
		cur[u]=i;
	}
	if (!used){
		dis[u]=-1;
	}
	return used;
}
ll ans;
inline void	work(){
	while (bfs()){
		clean();
		ans+=dinic(S,inf);
	}
	ans=ans*2;
	printf("%lld\n",ans);
}
inline void solve(){
	dfs(root,0);
	ins2(S,root,inf);
	work();
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	init();
	solve();
	return 0;
}

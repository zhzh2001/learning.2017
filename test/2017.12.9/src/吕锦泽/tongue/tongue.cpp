#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
#define N 100005
#define ll long long
#define mod 1000003
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,S,tot,a[N],f[N],head[N];
struct edge{ ll to,next; }e[N<<1];
struct data{ ll id,v; };
bool operator <(data x,data y){
	return x.v>y.v;
}
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs(ll u,ll last){
	--a[u]; if (u!=S) f[u]=1;
	vector<data> b;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs(v,u);
		b.push_back((data){v,f[v]});
	}
	sort(b.begin(),b.end());
	for (ll j=0;j<b.size();++j){
		data t=b[j];
		f[u]+=t.v+1; --a[u];
		if (!a[u]) break;
	}
	if (!a[u]) return;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		if (a[v]) f[u]+=min(a[v],a[u])*2,a[u]-=min(a[v],a[u]);
		if (!a[u]) break;
	}
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,2,n){
		ll u=read(),v=read();
		if (a[u]&&a[v]) add(u,v),add(v,u);
	}
	S=read(); ++a[S];
	dfs(S,-1);
	printf("%lld",f[S]);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 1005
#define ll long long
#define mod 1000003
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,cnt,a[N],sumx[N],sumy[N]; double sum; char mp[N][N];
int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) scanf("%s",mp[i]+1);
	rep(i,1,n) rep(j,1,m){
		if (mp[i][j]=='X') sum+=(n-i)*(i-1)*4+(m-j)*(j-1)*4,++cnt;
	}
	rep(i,1,n){
		sumx[i]=sumx[i-1];
		a[i]=a[i-1];
		rep(j,1,m){
			if (mp[i][j]=='X') continue;
			++a[i]; sumx[i]+=i;
			sum+=(a[i-1]*i-sumx[i-1])*2;
		}
	}
	rep(j,1,m){
		sumy[j]=sumy[j-1];
		a[j]=a[j-1];
		rep(i,1,n){
			if (mp[i][j]=='X') continue;
			++a[j]; sumy[j]+=j;
			sum+=(a[j-1]*j-sumy[j-1])*2;
		}
	}
	printf("%f",sum/((n*m-cnt)*(n*m-cnt)));
}

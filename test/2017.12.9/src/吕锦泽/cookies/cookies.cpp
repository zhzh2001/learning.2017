#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000003
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll qpow(ll x,ll y){
	ll ans=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) ans=ans*x%mod;
	return ans;
}
int main(){
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	printf("%lld",qpow(3,read()-1));
}

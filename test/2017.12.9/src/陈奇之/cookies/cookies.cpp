#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
typedef long long LL;
const int maxn = 1005;
const LL Mod = 1e6+3;
using namespace std;
LL Pow[maxn],f[maxn],g[maxn];
int n;
int main(){
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	scanf("%d",&n);
	Pow[0] = 1;
	for (int i=1;i<=n;i++)
		Pow[i] = 2LL * Pow[i-1] % Mod;
	for (int i=1;i<=n;i++)
		f[i] = ((1LL * Pow[i] * (Pow[i]+1)) >> 1LL) % Mod;
		//表示有多少覆盖 
	g[1] = 1;
	for (int i=1;i<=n;i++){
		g[i] = f[i];
		for (int j=i-1;j>=1;j--)
			g[i] = (g[i] + 1LL * Pow[i-j-1] * g[j] % Mod) % Mod;
	}
	printf("%lld\n",((Pow[n]*Pow[n]%Mod - g[n])%Mod+Mod)%Mod);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long LL;
const int maxn = 1005;
char a[maxn][maxn];
int qx[maxn*maxn],qy[maxn*maxn],dist[maxn][maxn];
int front,rear;
int n,m,cnt;
const int dx[4] = {0,0,1,-1};
const int dy[4] = {1,-1,0,0};
double Ans;
int main(){
	freopen("length.in","r",stdin);
	freopen("����.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			a[i][j] = getchar();
			while(a[i][j]!='.'&&a[i][j]!='X')a[i][j]=getchar();
			if (a[i][j] == '.') cnt++;
		}
	}
	Ans = 0;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			if (a[i][j] == 'X') continue;
			memset(dist,-1,sizeof(dist));
			front = rear = 0;
			qx[rear] = i;
			qy[rear] = j;
			rear++;
			dist[i][j] = 0;
			while (front < rear){
				int ux = qx[front],uy = qy[front];
				front++;
				for (int d=0;d<4;d++){
					int vx = ux + dx[d];
					int vy = uy + dy[d];
					if (vx <= 0||vy <= 0||vx > n || vy > m) continue;
					if (a[vx][vy] == 'X') continue;
					if (dist[vx][vy] == -1){
						dist[vx][vy] = dist[ux][uy] + 1;
						Ans += 1.0 * dist[vx][vy] / cnt / cnt;
						qx[rear] = vx;
						qy[rear] = vy;
						rear++;
					}
				}
			}
		}
	}
	printf("%lf\n",Ans);
	return 0;
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
int n,m;
typedef long long LL;
typedef long double LD;
LL get(int i,int j){
	LL U,D,L,R;
	U = m * i * (i-1) >> 1;
	D = m * (n-i) * (n-i+1) >> 1;
	L = n * j * (j-1) >> 1;
	R = n * (m-j) * (m-j+1) >> 1;
	return U+D+L+R;
}
LD Ans;
const int maxn = 1005;
char a[maxn][maxn];
int b[maxn],cnt;
bool f[maxn][maxn],g[maxn][maxn];
struct P{int x,y;}p[maxn];int nump;

int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	scanf("%d%d",&n,&m);
	nump = 0;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			a[i][j] = getchar();
			while(a[i][j]!='.'&&a[i][j]!='X')a[i][j]=getchar();
			if (a[i][j]=='.'){
				cnt++;
			} else{
				++nump;
				p[nump] . x = i;
				p[nump] . y = j;
			}
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			Ans += LD(get(i,j)) / cnt / cnt;
		}
	}
	for (int i=1;i<=nump;i++){
		Ans -= 2 * LD(get(p[i].x,p[i].y)) / cnt / cnt;
		for (int j=1;j<=nump;j++){
			Ans += 1.0 * (abs(p[i].x-p[j].x) + abs(p[i].y - p[j].y)) / cnt / cnt;
		}
	}

	
	for (int i=1;i<=n;i++){
		b[i] = 0;
		for (int j=1;j<=m;j++) if (a[i][j] == 'X') b[i] = j;
	}
	memset(f,false,sizeof(f));
	memset(g,false,sizeof(g));
	for (int i=1;i<=n;i++) f[i][i] = true,g[i][i] = true;
	for (int len=2;len<=n;len++){
		for (int i=1;i<=n;i++){
			int j = i + len - 1;
			if (b[i]==0 || b[j]==0) continue;
			if (f[i][j-1] && (b[j-1] < b[j])) f[i][j] = true;
			if (g[i][j-1] && (b[j-1] > b[j])) g[i][j] = true;
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			if (f[i][j]) Ans += 4.0 * (b[i]-1) * (m-b[j]) / cnt / cnt;
			if (g[i][j]) Ans += 4.0 * (m-b[i]) * (b[j]-1) / cnt / cnt;
		}
	}
	for (int i=1;i<=n;i++){
		if (b[i]==0) continue;
		Ans += 4.0 * (b[i] - 1) * (m - b[i]) / cnt / cnt;
	}
	
	for (int j=1;j<=m;j++){
		b[j] = 0;
		for (int i=1;i<=m;i++) if (a[i][j] == 'X') b[j] = i;
	}

	memset(f,false,sizeof(f));
	memset(g,false,sizeof(g));
	for (int i=1;i<=m;i++) f[i][i] = true,g[i][i] = true;
	for (int len=2;len<=m;len++){
		for (int i=1;i<=m;i++){
			int j = i + len - 1;
			if (b[i]==0 || b[j]==0) continue;
			if (f[i][j-1] && (b[j-1] < b[j])) f[i][j] = true;
			if (g[i][j-1] && (b[j-1] > b[j])) g[i][j] = true;
		}
	}
	for (int i=1;i<=m;i++){
		for (int j=i+1;j<=m;j++){
			if (f[i][j]) Ans += 4.0 * (b[i]-1) * (n-b[j]) / cnt / cnt;
			if (g[i][j]) Ans += 4.0 * (n-b[i]) * (b[j]-1) / cnt / cnt;
		}
	}
	
	for (int i=1;i<=m;i++){
		if (b[i]==0) continue;
		Ans += 4.0 * (b[i] - 1) * (n - b[i]) / cnt / cnt;
	}
	printf("%Lf\n",Ans);
	return 0;
}
/*
5 5
....X
.X...
.....
...X.
X....

*/


#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn = 100005;
typedef long long LL;
struct Edge{int to,nxt;}edge[maxn*2];
int first[maxn],nume,a[maxn];
int n;
void add_edge(int a,int b){
	edge[nume] . to = b;
	edge[nume] . nxt = first[a];
	first[a] = nume++;
}

bool vis[maxn];
int front,rear;
int q[maxn],deep[maxn];

void bfs(int root){
	memset(deep,-1,sizeof(deep));
	front = rear = 0;
	q[rear++] = root;
	deep[root] = 0;
	while (front<rear){
		int u = q[front++];
		for (int e=first[u];~e;e=edge[e].nxt){
			int v = edge[e].to;
			if (deep[v]==-1){
				q[rear++] = v;
				deep[v] = deep[u] + 1;
			}
		}
	}
}

LL f[maxn],g[maxn];
int son[maxn],x;

bool cmp(int a,int b){
	return f[a] > f[b];
}
void solve(int u){
//	f[i] = 0;
//	g[i] = a[i];
	LL remain = 0;
	son[0] = 0;
	for (int e=first[u];~e;e=edge[e].nxt){
		int v = edge[e].to;
		if (deep[v]!=deep[u]+1) continue;
		son[++son[0]] = v;
		remain += g[v];
	}
	sort(son+1,son+1+son[0],cmp);
	if (a[u]){
		if (u!=x) f[u] = 1; else f[u] = 0;
		if (u!=x) g[u] = a[u] - 1; else g[u] = a[u];
		int cnt = 1;
		while (g[u] && (son[0]>=cnt)){
			f[u] += f[son[cnt]];
			cnt++;
			f[u]++;g[u]--;
		}
		f[u] += min(g[u],remain) * 2;
		g[u] -= min(g[u],remain);
	} else{
		f[u] = g[u] = 0;
		//如果直接不能到达，那么直接不能到达 
	}
//	printf("%d %d %d\n",u,f[u],g[u]);
}

int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	memset(first,-1,sizeof(first));
	nume = 0;
	for (int i=1;i<=n-1;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		add_edge(a,b);
		add_edge(b,a);
	}
	scanf("%d",&x);
	bfs(x);//得到拓扑序列(n,n-1…………1)
	for (int i=n-1;i>=0;i--) solve(q[i]);
	printf("%lld\n",f[x]);
	return 0;
}
/*
5
10002 2 10000 10000 10000
1 2
1 4
2 3
2 5
1
*/

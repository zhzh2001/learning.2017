//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
#include<iomanip>
#include<fstream>
using namespace std;
ifstream fin("length.in");
ofstream fout("length.out");
int n,m;
char mp[1003][1003];
struct point{
	int x,y;
}pp[1003];
inline bool cmp1(const point a,const point b){
	return a.x<b.x;
}
inline bool cmp2(const point a,const point b){
	return a.y<b.y;
}
int tot;
unsigned long long f[1003][1003];
int sum1[1003][1003],sum2[1003][1003];
inline unsigned long long calc(unsigned long long x){
	return ((x)*(x-1))/2;
}
queue<point>q;
const int dx[6]={0,1,0,-1,0};
const int dy[6]={0,0,1,0,-1};
int dis[55][55];
inline void baoli(){
	long long ans=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(mp[i][j]=='X'){
				continue;
			}
			for(int ii=1;ii<=n;ii++){
				for(int jj=1;jj<=m;jj++){
					dis[ii][jj]=9999999;
				}
			}
			dis[i][j]=0;
			point now,now2;
			now.x=i,now.y=j;
			q.push(now);
			while(!q.empty()){
				now=q.front();
				q.pop(); 
				for(int k=1;k<=4;k++){
					int xxx=now.x+dx[k],yyy=now.y+dy[k];
					if(xxx>=1&&xxx<=n&&yyy>=1&&yyy<=n&&mp[xxx][yyy]!='X'){
						if(dis[now.x][now.y]+1<dis[xxx][yyy]){
							dis[xxx][yyy]=dis[now.x][now.y]+1;
							now2.x=xxx,now2.y=yyy;
							q.push(now2);
						}
					}
				}
			}
			for(int ii=1;ii<=n;ii++){
				for(int jj=1;jj<=m;jj++){
					if(mp[ii][jj]!='X'){
						ans+=dis[ii][jj];
					}
				}
			}
		}
	}
	fout<<fixed<<setprecision(6)<<(double)((double)ans/((double)(n*m-tot)*(double)(n*m-tot)))<<endl;
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>mp[i][j];
			if(mp[i][j]=='X'){
				pp[++tot].x=i,pp[tot].y=j;
			}
		}
	}
	if(n<=50&&m<=50){
		baoli();
		return 0;
	}
	for(int i=1;i<=m;i++){
		f[1][i]=((i)*(i-1))/2;
		for(int j=2;j<=n;j++){
			f[j][i]=f[j-1][i]+(j-1)*i+(i)*(i-1)/2;
		}
	}
	unsigned long long lei=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			lei+=(f[i][j]+f[n-i+1][j]+f[i][m-j+1]+f[n-i+1][m-j+1]);
			//cout<<f[i][j]<<" "<<f[n-i+1][j]<<" "<<f[i][m-j+1]<<" "<<f[n-i+1][m-j+1]<<endl;
			//cout<<calc(i-1)<<" "<<calc(j-1)<<" "<<calc(n-i+1)<<" "<<calc(m-j+1)<<endl;
			lei-=1*calc(i),lei-=1*calc(j),lei-=1*calc(n-i+1),lei-=1*calc(m-j+1);
		}
	}
	for(int i=1;i<=tot;i++){
		int xx=pp[i].x,yy=pp[i].y;
		lei-=(f[xx][yy]+f[n-xx+1][yy]+f[xx][m-yy+1]+f[n-xx+1][m-yy+1])*2;
		lei+=2*calc(xx),lei+=2*calc(yy),lei+=2*calc(n-xx+1),lei+=2*calc(m-yy+1);
		lei+=(xx-1)*(n-xx)*4+(yy-1)*(n-yy)*4;
		for(int j=1;j<=tot;j++){
			lei+=(abs(pp[j].x-xx)+abs(pp[j].y-yy));
		}
	}
	sort(pp+1,pp+tot+1,cmp1);
	for(int i=1;i<=tot;i++){
		for(int j=1;j<=m;j++){
			sum1[i][j]+=sum1[i-1][j]+(j==pp[i].y);
		}
	}
	for(int i=1;i<=tot;i++){
		for(int j=i+1;j<=tot;j++){
			if(sum1[j][pp[j].y]-sum1[i][pp[i].y]==pp[j].x-pp[i].x-1){
				lei+=2*(pp[i].x-1)*(n-pp[j].x);
			}
		}
	}
	sort(pp+1,pp+tot+1,cmp2);
	for(int i=1;i<=tot;i++){
		for(int j=1;j<=n;j++){
			sum2[i][j]+=sum2[i-1][j]+(j==pp[i].x);
		}
	}
	for(int i=1;i<=tot;i++){
		for(int j=i+1;j<=tot;j++){
			if(sum2[j][pp[j].x]-sum2[i][pp[i].x]==pp[j].y-pp[i].y-1){
				lei+=2*(pp[i].y-1)*(m-pp[j].y);
			}
		}
	}
	fout<<(long double)((long double)lei/((long double)(n*m-tot)*(long double)(n*m-tot)))<<endl;
	return 0;
}
/*

in:
2 2
..
.X
out:
0.888889

in:
3 3
..X
...
X..
out:
2.000000

5 5
.....
.X...
...X.
X....
.....
*/

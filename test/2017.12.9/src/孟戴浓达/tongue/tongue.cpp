//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
ifstream fin("tongue.in");
ofstream fout("tongue.out");
long long n,val[100003],rt;
long long to[200003],head[200003],nxt[200003],tot;
inline void add(long long a,long long b){
	to[++tot]=b;  nxt[tot]=head[a];  head[a]=tot;
}
int jiyi[7][7][7][7][7][7];
int baoli(int v1,int v2,int v3,int v4,int v5,int now){
	int &fanhui=jiyi[v1][v2][v3][v4][v5][now];
	if(fanhui!=-1){
		return fanhui;
	}
	if(now==rt){
		fanhui=0;
	}
	for(int i=head[now];i;i=nxt[i]){
		if(to[i]==1&&v1>=1){
			fanhui=max(fanhui,baoli(v1-1,v2,v3,v4,v5,to[i])+1);
		}else if(to[i]==2&&v2>=1){
			fanhui=max(fanhui,baoli(v1,v2-1,v3,v4,v5,to[i])+1);
		}else if(to[i]==3&&v3>=1){
			fanhui=max(fanhui,baoli(v1,v2,v3-1,v4,v5,to[i])+1);
		}else if(to[i]==4&&v4>=1){
			fanhui=max(fanhui,baoli(v1,v2,v3,v4-1,v5,to[i])+1);
		}else if(to[i]==5&&v5>=1){
			fanhui=max(fanhui,baoli(v1,v2,v3,v4,v5-1,to[i])+1);
		}
	}
	if(fanhui==-1){
		return fanhui=-99999999;
	}
	return fanhui;
}
long long f1[100003],f2[100003];
long long x[100003];
void dfs(int now,int fa){
	long long son=0;
	int nowl=x[0];
	for(int i=head[now];i;i=nxt[i]){
		if(to[i]==fa){
			continue;
		}
		son++;
		dfs(to[i],now);
	}
	x[0]=nowl;
	if(son==0){
		f1[now]=0,f2[now]=val[now];
	}
	if(val[now]-1>=son){
		long long sum=0;
		for(int i=head[now];i;i=nxt[i]){
			f1[now]+=f1[to[i]];
			sum+=f2[to[i]];
		}
		sum=max(sum,val[now]-son-1);
		f1[now]+=sum*2;
		f2[now]=sum;
	}else{
		for(int i=head[now];i;i=nxt[i]){
			x[++x[0]]=f1[to[i]];
		}
		sort(x+nowl+1,x+x[0]+1);
		for(int i=x[0];i>=x[0]-val[now]+1;i--){
			f1[now]+=x[i];
		}
		f2[now]=0;
	}
}
int main(){
	fin>>n;
	bool yes=true;
	for(int i=1;i<=n;i++){
		fin>>val[i];
		if(val[i]>=5){
			yes=false;
		}
	}
	for(int i=1;i<=n-1;i++){
		int a,b;
		fin>>a>>b;
		add(a,b),add(b,a);
	}
	fin>>rt;
	if(yes==true&&n<=5){
		memset(jiyi,-1,sizeof(jiyi));
		fout<<baoli(val[1],val[2],val[3],val[4],val[5],rt)<<endl;
		return 0;
	}
	val[rt]++;
	dfs(rt,0);
	fout<<f1[rt]<<endl;
	return 0;
}
/*

in:
5
1 3 1 3 2
2 5
3 4
4 5
1 5
4
out:
6

in:
3
2 1 1
3 2
1 2
3
out:
2

*/

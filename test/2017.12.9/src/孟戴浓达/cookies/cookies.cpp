//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
ifstream fin("cookies.in");
ofstream fout("cookies.out");
long long n;
const long long mod=1000003;
long long qpow(long long a,long long b){
	if(b==0){
		return 1;
	}
	if(b==1){
		return a;
	}
	long long fanhui=qpow(a,b/2);
	fanhui=fanhui*fanhui%mod;
	if(b%2==1){
		fanhui=fanhui*a%mod;
	}
	return fanhui;
}
int main(){
	fin>>n;
	fout<<qpow(3,n-1)<<endl;
	return 0;
}
/*

in:
3

out:
9

*/

#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <cstdlib>
using namespace std;
typedef long long LL;
const int N=100005;
struct Gragh{
	int cnt,y[N*2],nxt[N*2],fst[N];
	void clear(){
		cnt=0;
		memset(fst,0,sizeof fst);
	}
	void add(int a,int b){
		y[++cnt]=b,nxt[cnt]=fst[a],fst[a]=cnt;
	}
}g;
int n,x,a[N],d[N],bh[N];
LL dp[N];//表示分配给第i个子树一个流量所产生的最大效益
// 如果根节点剩余容量大于等于子节点个数，那么尽量全部走一遍
// 否则排序并挑选效益尽量大的子树有限
// 如果还有多，那么尽量和有剩余的子树来回走
// 最终有两种情况：当前节点容量还有剩余 或 没剩余
// 事实上是贪心 
bool cmpdp(int a,int b){
	return dp[a]>dp[b];
}
LL min(LL a,LL b){
	return a<b?a:b;
}
void solve(int rt,int pre){
	int cnt=0;
	for (int i=g.fst[rt];i;i=g.nxt[i])
		if (g.y[i]!=pre){
			d[g.y[i]]--;
			if (d[g.y[i]]==-1)
				continue;
			solve(g.y[i],rt);
		}
	for (int i=g.fst[rt];i;i=g.nxt[i])
		if (g.y[i]!=pre&&d[g.y[i]]!=-1)
		 	bh[++cnt]=g.y[i];
	sort(bh+1,bh+cnt+1,cmpdp);
	for (int i=1;i<=cnt&&d[rt]>0;i++,d[rt]--)
		dp[rt]+=dp[bh[i]]+2;
	LL leave=0;
	for (int i=1;i<=cnt;i++)
		leave=leave+d[bh[i]];
	LL hehe=min(leave,d[rt]);
	d[rt]-=hehe;
	dp[rt]+=hehe*2;
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]),d[i]=a[i];
	g.clear();
	for (int i=1,a,b;i<n;i++){
		scanf("%d%d",&a,&b);
		g.add(a,b),g.add(b,a);
	}
	scanf("%d",&x);
	memset(dp,0,sizeof dp);
	solve(x,0);
	printf("%lld",dp[x]);
	fclose(stdin);fclose(stdout);
	return 0;
}
/*
cgc zclj bf 
高山难隐鸟踪迹
佳水也有鱼龙游
倍有壮兴登凌云 
祝陈力炯大佬AK 
*/

#include <cstring>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cmath>
using namespace std;
typedef long long LL;
const int N=1005,mod=1000003;
int n;
LL LLPow(LL x,LL y){
	if (y<=0)
		return 1LL;
	LL xx=LLPow(x,y/2);
	xx=xx*xx%mod;
	if (y&1LL)
		xx=xx*x%mod;
	return xx;
}
int main(){
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	scanf("%d",&n);
	printf("%lld",LLPow(3,n-1));
	fclose(stdin);fclose(stdout);
	return 0;
}

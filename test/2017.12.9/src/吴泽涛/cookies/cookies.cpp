#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;

inline LL read() {
	LL x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}
inline void write(LL x) {
	if( x < 0 ) putchar('-') ;
	if( x > 9 ) write(x/10) ;
	putchar(x%10+48) ;
}
inline void writeln(LL x) {
	write(x) ;
	putchar('\n') ;
}

const int mod = 1e6+3; 
int n;
int b[40];  
inline LL ksm(int x,int y) {
	int now = 0;  
	while(y) {
		b[++now] = y&1; 
		y/=2; 
	}
	LL res = 1; 
	Dow(i, now, 1) {
		res = res*res%mod; 
		if(b[ i ]) res = res*x%mod; 
	}
	return res; 
}

int main() {
	freopen("cookies.in","r",stdin); 
	freopen("cookies.out","w",stdout); 
	n = read(); 
	--n; 
	printf("%lld\n",ksm(3,n)); 
	return 0; 
}









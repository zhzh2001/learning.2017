#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;

inline LL read() {
	LL x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}
inline void write(LL x) {
	if( x < 0 ) putchar('-') ;
	if( x > 9 ) write(x/10) ;
	putchar(x%10+48) ;
}
inline void writeln(LL x) {
	write(x) ;
	putchar('\n') ;
}

const int N = 1011;
const int dx[5] = {0,1,0,0,-1}; 
const int dy[5] = {0,0,1,-1,0};  
int n,m,h,t,num,ans;
int ty[N]; 
char s[N][N]; 
bool vis[N][N]; 
int dist[N][N]; 
struct node{
	int x, y; 
}q[1000011];

void bfs(int x,int y) {
	For(i, 1, n) 
	  For(j, 1, m) vis[i][j] = 0; 
	For(i, 1, n) vis[i][ty[i]] = 1; 
	h = 0; t = 0; 
	q[++t].x = x; q[t].y = y; 
	vis[x][y] = 1; 
	dist[x][y] = 0; 
	while( h < t ) {
		x = q[++h].x; y = q[h].y; 
		For(i, 1, 4) {
			int xx = x+dx[i]; 
			int yy = y+dy[i]; 
			if(xx<1||xx>n||yy<1||yy>n||vis[xx][yy]) continue; 
			vis[xx][yy] = 1; dist[xx][yy] = dist[x][y]+1; 
			q[++t].x = xx; q[t].y = yy; 
		}
	}
	For(i, 1, n) 
	  For(j, 1, n) 
	  	if(s[i][j]!='X') ans+=dist[i][j];  
}

int main() {
	freopen("length.in","r",stdin); 
	freopen("length.out","w",stdout); 
	n = read(); m = read(); 
	For(i, 1, n) scanf("%s",s[i]+1); 
	num = n*m; 
	For(i, 1, n) 
	  For(j, 1, m) 
	  	if(s[i][j]=='X') 
	  		ty[i] = j, --num; 
 
	For(i, 1, n) 
	  For(j, 1, m) 
	  	if(s[i][j]!='X') bfs(i, j); 
	num*=num; 
	printf("%lf",1.0*ans/num); 
	return 0; 
}




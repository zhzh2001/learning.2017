#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;
inline LL read() {
	LL x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}
inline void write(LL x) {
	if( x < 0 ) putchar('-') ;
	if( x > 9 ) write(x/10) ;
	putchar(x%10+48) ;
}
inline void writeln(LL x) {
	write(x) ;
	putchar('\n') ;
}

const int N = 100011; 
struct node{
	int to,pre; 
}e[N*2];
int n,root,cnt;
int head[N],ned[N]; 
LL dp[N]; 
inline bool cmp(LL a,LL b) {
	return a > b;
}
inline void add(int x,int y) {
	e[++cnt].to = y; 
	e[cnt].pre = head[x]; 
	head[x] = cnt; 
}

void dfs(int u,int fa) {
	if(ned[u]==0) return; 
	int son = 0;  
	LL tmp[500]; 
	for(int i=head[u]; i ;i=e[i].pre) {
		int v = e[i].to; 
		if(v!=fa) {
			dfs(v,u); 
			tmp[++son] = dp[v]; 
		}
	}
	
	if( son<=ned[u] ) 
		For(i, 1, son) dp[u]+=tmp[i]; 
	else {
		sort(tmp+1, tmp+son+1, cmp); 
		For(i, 1, ned[u]) dp[u]+=tmp[i]; 
	}
	dp[u]+=2*min(son,ned[u]); 
}

int main() {
	freopen("tongue.in","r",stdin); 
	freopen("tongue.out","w",stdout); 
	n = read(); 
	For(i, 1, n) ned[i]=read(), --ned[i]; 
	For(i, 1, n-1) {
		int x = read(), y = read(); 
		add(x,y); add(y,x); 
	}
	root = read(); ++ned[root]; 
	dfs(root,-1); 
	printf("%lld\n",dp[root]); 
	return 0;   
}


/*

5
1 3 1 3 2
2 5
3 4
4 5
1 5
4


*/









#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
const int N=1005,X[4]={0,1,0,-1},Y[4]={1,0,-1,0};
int a[N][N],f[N][N],n,m,l,r,kkk;
char c[N];
pair<int,int> q[N*N];
long long S,SS;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("length.in","r",stdin);
	freopen("length.ans","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
	{
		scanf("%s",c+1);
		for (int j=1;j<=m;++j)
			if (c[j]=='X')
			{
				++kkk;
				a[i][j]=1;
			}
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			if (!a[i][j])
			{
				SS=0;
				memset(f,-0x3f,sizeof(f));
				f[i][j]=0;
				l=0;
				r=1;
				q[1]=mp(i,j);
				while (l<r)
				{
					++l;
					int xx=q[l].first,yy=q[l].second;
					for (int k=0;k<4;++k)
					{
						int xxx=xx+X[k],yyy=yy+Y[k];
						if (f[xxx][yyy]<0&&xxx>=1&&xxx<=n&&yyy>=1&&yyy<=m&&a[xxx][yyy]==0)
						{
							q[++r]=mp(xxx,yyy);
							f[xxx][yyy]=f[xx][yy]+1;
							SS+=f[xxx][yyy];
						}
					}
				}
				//cout<<SS<<'\n';
				S+=SS;
			}
	printf("%.6Lf",(long double)S/(long double)((long long)(n*m-kkk)*(n*m-kkk)));
}
#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=100005;
int f[N],a[N],la[N],to[N*2],pr[N*2],n,cnt,x,y;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
void dfs(int x,int fa)
{
	f[x]=2;
	vector<int> v;
	int m=0;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			dfs(to[i],x);
			m+=a[to[i]];
			v.push_back(f[to[i]]);
		}
	if (v.size()<=a[x])
	{
		a[x]-=v.size();
		for (unsigned i=0;i<v.size();++i)
			f[x]+=v[i];
		if (a[x]<=m)
		{
			f[x]+=a[x]*2LL;
			a[x]=0;
		}
		else
		{
			f[x]+=m*2LL;
			a[x]-=m;
		}
	}
	else
	{
		sort(v.rbegin(),v.rend());
		for (int i=0;i<a[x];++i)
			f[x]+=v[i];
		a[x]=0;
	}
}
signed main()
{
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		--a[i];
	}
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	read(x);
	++a[x];
	dfs(x,0);
	cout<<f[x]-2LL;
}

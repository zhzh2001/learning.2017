#include<bits/stdc++.h>
using namespace std;
const int mod=1e6+3;
int n,ans;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	ans=1;
	read(n);
	for (int i=1;i<n;++i)
		ans=ans*3%mod;
	cout<<ans;
}
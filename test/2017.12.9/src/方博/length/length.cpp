#include<bits/stdc++.h>
#define ll long long
#define pa pair<int,int>
#define mk make_pair
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
using namespace std;
const int N=1005;
char a[N][N];
int dx[4]={0,1,0,-1};
int dy[4]={1,0,-1,0};
ll ans;
int t,n,m;
struct xxx{
	int x,y;
	int lx,rx,ly,ry;
}p[N*N];
bool com1(xxx a,xxx b)
{
	return a.x<b.x;
}
bool com2(xxx a,xxx b)
{
	return a.y<b.y;
}
inline ll solve(int x,int y)
{
	ll ans=0,t=0,tmp;
	if(x>y)swap(x,y);
	for(int i=1;i<=x+y-1;i++){if(i<=x)t++;if(i>y)t--;ans+=t*(i-1);}
	tmp=ans;if(x>1)ans*=2;t=x;
	for(int i=2;i<=x/2;i++){t-=2;tmp-=(t*y);ans+=tmp*2;}
	if(x%2){t-=2;tmp-=(t*y);ans+=tmp;}
	tmp=ans;if(y>1)ans*=2;t=y;
	for(int i=2;i<=y/2;i++){t-=2;tmp-=(t*x*x);ans+=tmp*2;}
	if(y%2){t-=2;tmp-=(t*x*x);ans+=tmp;}
	return ans;
}
int main()
{
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)scanf("%s",a[i]+1);
	ans=solve(n,m);ll cnt=n*m;t=0;
	
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(a[i][j]=='X'){
				ll lx=i-1,rx=n-i;
				ll ly=j-1,ry=m-j;
				p[++t]=(xxx){i,j,lx,rx,ly,ry};
				ans-=(lx*ly*(lx+ly)/2+lx*ly)*2;
				ans-=(lx*ry*(lx+ry)/2+lx*ry)*2;
				ans-=(rx*ly*(rx+ly)/2+rx*ly)*2;
				ans-=(rx*ry*(rx+ry)/2+rx*ry)*2;
				ans-=(lx*(lx+1)+rx*(rx+1)+ly*(ly+1)+ry*(ry+1));
				ans+=lx*rx*2*2;
				ans+=ly*ry*2*2;
				cnt--;
			}
			
	for(int i=1;i<=t;i++)
		for(int j=i+1;j<=t;j++){
			int ax=p[i].x,ay=p[i].y;
			int bx=p[j].x,by=p[j].y;
			ans+=2*(abs(ax-bx)+abs(ay-by));
		}
		
	sort(p+1,p+t+1,com1);
	ll sum=0;
	char b='=';
	for(int i=1;i<t;i++){
		int j=i+1;
		if(p[i].x==p[j].x-1){
			if(p[i].y>p[j].y){
				if(b=='>')sum+=p[i].ry;
				else b='>',sum=p[i].ry;
				ans+=2*(sum*p[j].ly*2);
			}
			else{
				if(b=='<')sum+=p[i].ly;
				else b='<',sum=p[i].ly;
				ans+=2*(sum*p[j].ry*2);
			}
		}
		else sum=0,b='=';
	}
	
	sort(p+1,p+t+1,com2);
	sum=0;
	b='=';
	for(int i=1;i<t;i++){
		int j=i+1;
		if(p[i].y==p[j].y-1){
			if(p[i].x>p[j].x){
				if(b=='>')sum+=p[i].rx;
				else b='>',sum=p[i].rx;
				ans+=2*(sum*p[j].lx*2);
			}
			else{
				if(b=='<')sum+=p[i].lx;
				else b='<',sum=p[i].lx;
				ans+=2*(sum*p[j].rx*2);
			}
		}
		else sum=0,b='=';
	}
	
	printf("%.6lf",1.0*ans/(cnt*cnt));
	return 0;
}

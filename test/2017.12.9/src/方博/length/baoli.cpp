#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pa pair<int,int>
#define mk make_pair
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=1005;
int vis[N][N];
pa q[N*N];
char a[N][N];
int f[N][N];
int dx[4]={0,1,0,-1};
int dy[4]={1,0,-1,0};
ll ans;
int t,n,m;
ll bfs(int fx,int fy)
{
	ll ret=0;
	vis[fx][fy]=++t;
	q[1]=mk(fx,fy);
	f[fx][fy]=0;
	for(int l=1,r=1;l<=r;l++){
		int x=q[l].first,y=q[l].second;
		for(int i=0;i<4;i++){
			int tx=x+dx[i];
			int ty=y+dy[i];
			if(tx<1||ty<1||tx>n||ty>m||vis[tx][ty]==t)continue;
			if(a[tx][ty]!='X'){
				r++;
				q[r]=mk(tx,ty);
				vis[tx][ty]=t;
				f[tx][ty]=f[x][y]+1;
				ret+=f[tx][ty];
			}
		}
	}
	return ret;
}
int main()
{
	freopen("length.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		scanf("%s",a[i]+1);
	int cnt=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(a[i][j]!='X'){
				ans+=bfs(i,j);
				cnt++;
			}
//	writeln(ans);
	printf("%.6lf",1.0*ans/(cnt*cnt));
	return 0;
}

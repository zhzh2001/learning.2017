#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=100005;
const int M=200005;
ll vis[N];
ll rest[N];
ll f[N];
int head[N],nxt[M],tail[M];
int n,m;
int t,x,y;
bool com(ll x,ll y)
{
	return x>y;
}
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
ll dfs(int k,int fa)
{
	vector<ll>a;
	ll sum=0,ret=0;
	if(f[k]==0)return 0;
	if(k!=x)f[k]--;
	for(int i=head[k];i;i=nxt[i]){
		int x=tail[i];
		if(x==fa)continue;
		int t=dfs(x,k);
		if(t!=0)a.push_back(t);
		sum+=rest[x];
	}
	sort(a.begin(),a.end(),com);
	for(int i=0;i<a.size()&&f[k]>0;i++){
		ret+=a[i];
		f[k]--;
	}
	if(f[k]>0){
		if(f[k]>sum)rest[k]=f[k]-sum,ret+=sum*2;
		else ret+=f[k]*2,rest[k]=0;
	}else rest[k]=0;
	rest[k]++;
	return ret;
}
int main()
{
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		f[i]=read();
	for(int i=1;i<n;i++){
		x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	x=read();
	writeln(dfs(x,0));
}

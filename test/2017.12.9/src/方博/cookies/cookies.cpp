#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=1005;
const int mod=1e6+3;
ll n;
//ll f[N];
ll qpow(ll x,ll y)
{
	ll ret=1;
	for(;y;y>>=1){
		if(y&1)ret=ret*x%mod;
		x=x*x%mod;
	}
	return ret;
}
int main()
{
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	n=read();
//	f[1]=1;
//	for(int i=2;i<=1000;i++)
//		f[i]=f[i-1]*3%mod;
//	writeln(f[n]);
	writeln(qpow(3,n-1));
	return 0;
}

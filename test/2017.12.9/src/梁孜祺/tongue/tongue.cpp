#include <bits/stdc++.h>
#define ll long long
#define N 100005
#define For(i, x, y) for (int i = x; i <= y; i++)
#define Rep(i, x, y) for (int i = x; i >= y; i--)
#define Forn(i, x, y) for (int i = x; i < y; i++)
using namespace std;
int n, a[N], S, cnt, head[N];
struct edge
{
	int next, to;
} e[N << 1];
inline void insert(int u, int v)
{
	e[++cnt] = (edge){head[u], v};
	head[u] = cnt;
	e[++cnt] = (edge){head[v], u};
	head[v] = cnt;
}
ll f[N];
struct data
{
	ll x, val;
	bool operator<(const data &a) const
	{
		return val > a.val;
	}
};
inline void dfs(int x, int fa)
{
	if (!a[x])
		return;
	vector<data> b;
	for (int i = head[x]; i; i = e[i].next)
		if (e[i].to != fa && a[e[i].to])
		{
			dfs(e[i].to, x);
			b.push_back((data){e[i].to, f[e[i].to]});
		}
	sort(b.begin(), b.end());
	if (a[x] > b.size())
	{
		Forn(i, 0, b.size())
		{
			if (x != S && a[x] == 1)
				break;
			f[x] += b[i].val + 2;
			a[x]--;
			a[b[i].x]--;
		}
	}
	else
	{
		Forn(i, 0, a[x])
		{
			if (x != S && a[x] == 1)
				break;
			f[x] += b[i].val + 2;
			a[x]--;
			a[b[i].x]--;
		}
	}
	if (a[x] > 1 && x != S)
	{
		for (int i = head[x]; i; i = e[i].next)
			if (e[i].to != fa && a[e[i].to])
			{
				if (a[x] - 1 <= a[e[i].to])
				{
					f[x] += 2 * (a[x] - 1);
					a[x] = 1;
					break;
				}
				else
					a[x] -= a[e[i].to], f[x] += a[e[i].to] * 2;
			}
	}
	if (x == S && a[x] > 0)
	{
		for (int i = head[x]; i; i = e[i].next)
			if (e[i].to != fa && a[e[i].to])
			{
				if (a[x] <= a[e[i].to])
				{
					f[x] += a[x] * 2;
					a[x] = 0;
					break;
				}
				else
					a[x] -= a[e[i].to], f[x] += a[e[i].to] * 2;
			}
	}
}
int main()
{
	freopen("tongue.in", "r", stdin);
	freopen("tongue.out", "w", stdout);
	scanf("%d", &n);
	For(i, 1, n) scanf("%d", &a[i]);
	For(i, 2, n)
	{
		int x, y;
		scanf("%d%d", &x, &y);
		insert(x, y);
	}
	scanf("%d", &S);
	if (a[S] == 0)
	{
		puts("0");
		return 0;
	}
	dfs(S, 0);
	printf("%lld\n", f[S]);
	return 0;
}
#include<bits/stdc++.h>
#define ll long long
#define N 1005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int n,f[N];
const int mod=1e6+3;
int main(){
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	scanf("%d",&n);
	f[1]=1;
	Forn(i,2,N){
		f[i]=f[i-1]*3%mod;
	}
	printf("%d",f[n]);
	return 0;
}

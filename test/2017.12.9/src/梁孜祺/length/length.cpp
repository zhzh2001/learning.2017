#include<bits/stdc++.h>
#define ll long long
#define int ll
#define N 1005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int n,m,cnt;
char s[N][N];
struct data{
	int x,y;
	bool operator <(const data &a)const{
		return x<a.x;
	}
}a[N];
int f[N][N],g[N][N];
ll M,ans;
inline bool cmp(data a,data b){return a.y<b.y;}
inline void doit(){
	For(i,1,n) For(j,1,m) if(s[i][j]=='X') a[++cnt]=(data){i,j};
	sort(a+1,a+1+cnt);
	For(i,1,n) For(j,1,m){
		g[i][j]=g[i-1][j]+g[i][j-1]-g[i-1][j-1]+(i+j-2);
		f[i][j]=2*((f[i-1][j]+f[i][j-1]-f[i-1][j-1])+g[i][j]);
	}
//	cout<<g[1][1]<<" "<<g[1][2]<<" "<<g[2][1]<<" "<<g[2][2]<<endl;
//	cout<<f[1][1]<<" "<<f[1][2]<<" "<<f[2][1]<<" "<<f[2][2]<<endl;
	ans=f[n][m];
	For(i,1,cnt){
		int x=a[i].x,y=a[i].y;
		ans-=g[x][y]*2;
		ans-=g[n-x][m-y]*2;
		ans-=g[x][m-y]*2;
		ans-=g[n-x][y]*2;
	}
	//cout<<ans<<endl;
	M=(n*m-cnt)*(m*n-cnt);
//	cout<<M<<endl;
	For(i,1,cnt){
		for(int j=i+1;j<=cnt;j++){
			if(a[j].x!=a[j-1].x+1||a[j].y<a[j-1].y) break;
			ans+=(a[i].y-1)*(m-a[j].y)*2;
		}
	}
	For(i,1,cnt){
		for(int j=i+1;j<=cnt;j++){
			if(a[j].x!=a[j-1].x+1||a[j].y>a[j-1].y) continue;
			ans+=(m-a[i].y)*(a[j].y-1)*2;
		}
	}
	sort(a+1,a+1+cnt,cmp);
	For(i,1,cnt){
		for(int j=i+1;j<=cnt;j++){
			if(a[j].y!=a[j-1].y+1||a[j].x<a[j-1].x) break;
			ans+=(a[i].y-1)*(n-a[j].y)*2;
		}
	}
	For(i,1,cnt){
		for(int j=i+1;j<=cnt;j++){
			if(a[j].y!=a[j-1].y+1||a[j].x>a[j-1].x) break;
			ans+=(m-a[i].y)*(a[j].y-1)*2;
		}
	}
	printf("%.6lf",1.0*ans/M);
}
main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,n) scanf("%s",s[i]+1);
	M=n*m*n*m;doit();
	return 0;
}

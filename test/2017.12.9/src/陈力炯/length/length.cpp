#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int dx[4]={0,0,1,-1};
const int dy[4]={1,-1,0,0};
int n,m,cnt,front,rear,dis[1005][1005];
struct node{
	int x,y;
}q[1000005];
double sum;
char ch[1005][1005];
void push(int x,int y){
	q[rear].x=x,q[rear++].y=y;
}
void bfs(int sx,int sy){
	memset(dis,31,sizeof(dis));
	dis[sx][sy]=front=rear=0,push(sx,sy);
	while (front<rear){
		int x=q[front].x,y=q[front++].y;
		for (int i=0;i<4;i++){
			int new_x=x+dx[i],new_y=y+dy[i];
			if (new_x<0 || new_x>=n || new_y<0 || new_y>=m) continue;
			if (ch[new_x][new_y]=='X' || dis[new_x][new_y]<1e8) continue;
			dis[new_x][new_y]=dis[x][y]+1,push(new_x,new_y);
		}
	}
	for (int i=0;i<n;i++)
	for (int j=0;j<m;j++)
	if (dis[i][j]<1e8)
		sum+=dis[i][j];
}
ll calc_kong(int n,int m){
	int first=n*(n-1)/2,cha=first+n,val;
	for (int i=2;i<=m;i++)
		first+=cha,cha+=n;
	int cha1=m*(n-2),cha2;
	ll res,ans=0;
	for (int i=1;i<=n/2;i++){
		val=first,res=0,cha2=(m-2)*n;
		for (int j=1;j<=m/2;j++) res+=val,val-=cha2,cha2-=2*n;
		res*=2;
		if (m&1) res+=val;
		ans+=res;
		first-=cha1,cha1-=2*m;
	}
	ans*=2;
	if (n&1){
		val=first,res=0,cha2=(m-2)*n;
		for (int j=1;j<=m/2;j++) res+=val,val-=cha2,cha2-=2*n;
		res*=2;
		if (m&1) res+=val;
		ans+=res;
		first-=cha1,cha1-=2*m;
	}
	return ans;
}
int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	scanf("%d%d",&n,&m);
	cnt=sum=0;
	for (int i=0;i<n;i++){
		scanf("%s",ch[i]);
		for (int j=0;j<m;j++)
			if (ch[i][j]!='X') cnt++;
	}
	if (cnt=n*m){
		printf("%lld\n",calc_kong(n,m));
		return 0;
	}
	for (int i=0;i<n;i++)
	for (int j=0;j<m;j++)
	if (ch[i][j]!='X')
		bfs(i,j);
	printf("%lf\n",sum/((double)cnt*cnt));
	return 0;
}

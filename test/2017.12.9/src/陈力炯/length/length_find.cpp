#include <bits/stdc++.h>
using namespace std;
const int dx[4]={0,0,1,-1};
const int dy[4]={1,-1,0,0};
int n,m,cnt,front,rear,dis[1005][1005];
struct node{
	int x,y;
}q[1000005];
double sum,res;
char ch[1005][1005];
void push(int x,int y){
	q[rear].x=x,q[rear++].y=y;
}
void bfs(int sx,int sy){
	memset(dis,31,sizeof(dis));
	dis[sx][sy]=front=rear=0,push(sx,sy);
	while (front<rear){
		int x=q[front].x,y=q[front++].y;
		for (int i=0;i<4;i++){
			int new_x=x+dx[i],new_y=y+dy[i];
			if (new_x<0 || new_x>=n || new_y<0 || new_y>=m) continue;
			if (ch[new_x][new_y]=='X' || dis[new_x][new_y]<1e8) continue;
			dis[new_x][new_y]=dis[x][y]+1,push(new_x,new_y);
		}
	}
	res=0;
	for (int i=0;i<n;i++)
	for (int j=0;j<m;j++)
	if (dis[i][j]<1e8)
		res+=dis[i][j];
	sum+=res;
}
int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	scanf("%d%d",&n,&m);
	cnt=sum=0;
	for (int i=0;i<n;i++){
		scanf("%s",ch[i]);
		for (int j=0;j<m;j++)
			if (ch[i][j]!='X') cnt++;
	}
	for (int i=0;i<n;i++){
	for (int j=0;j<m;j++)
	if (ch[i][j]!='X')
		bfs(i,j),printf("%lf ",res);
	puts("");
	}
	printf("%lf %lf\n",sum,sum/((double)cnt*cnt));
	return 0;
}

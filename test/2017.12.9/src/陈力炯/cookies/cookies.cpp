#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll mod=1e6+3;
int n;
ll dp[1005];
int main(){
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	scanf("%d",&n);
	dp[1]=1;
	for (int i=2;i<=n;i++)
		dp[i]=dp[i-1]*3%mod;
	printf("%lld\n",dp[n]);
	return 0;
}

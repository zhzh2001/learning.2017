#include <bits/stdc++.h>
using namespace std;
const int maxn=100005;
int n,a[maxn],head[maxn],tot,res,ans;
struct E{
	int to,next;
}edge[maxn*2];
void init(){
	memset(head,0,sizeof(head)),tot=0;
}
void makedge(int u,int v){
	edge[++tot].to=v;
	edge[tot].next=head[u];
	head[u]=tot;
}
void dfs(int u){
	if (res>ans) ans=res;
	for (int i=head[u],v;i;i=edge[i].next){
		v=edge[i].to;
		if (a[v]){
			a[v]--,res++;
			dfs(v);
			a[v]++,res--;
		}
	}
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	scanf("%d",&n),init();
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1,u,v;i<n;i++){
		scanf("%d%d",&u,&v);
		makedge(u,v),makedge(v,u);
	}
	int s; scanf("%d",&s);
	res=ans=0;
	dfs(s);
	printf("%d\n",ans);
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int maxn=100005;
int n,a[maxn],head[maxn],tot,begin,end;
int remain[maxn],V[maxn];
ll value[maxn];
struct E{
	int to,next;
}edge[maxn*2];
void init(){
	memset(head,0,sizeof(head)),tot=0;
}
void makedge(int u,int v){
	edge[++tot].to=v;
	edge[tot].next=head[u];
	head[u]=tot;
}
bool cmp(int a,int b){
	return a>b;
}
void dfs(int u,int fa){
	value[u]=0,remain[u]=a[u]-1;
	int cnt=0,R=0;
	begin=end+1;
	for (int i=head[u],v;i;i=edge[i].next){
		v=edge[i].to;
		if (v==fa) continue;
		dfs(v,u);
		V[++end]=value[v];
		R+=remain[v];
		cnt++;
	}
	sort(V+begin+1,V+end+1,cmp);
	if (!cnt)
		value[u]=0,remain[u]=a[u];
	else if (a[u]-1<=cnt){
		for (int i=end;i>=end-cnt+1;i--) value[u]+=V[i]+2;
		remain[u]=0;
	}
	else{
		for (int i=end;i>=end-cnt+1;i--) value[u]+=V[i]+2,remain[u]--;
		if (remain[u]<=R)
			value[u]+=remain[u]*2,remain[u]=0;
		else
			value[u]+=R*2,remain[u]-=R;
	}
	end=begin-1;
	//printf("%d %lld\n",u,value[u]);
}
int main(){
	freopen("tongue.in","r",stdin);
	//freopen("tongue.out","w",stdout);
	scanf("%d",&n),init();
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1,u,v;i<n;i++){
		scanf("%d%d",&u,&v);
		makedge(u,v),makedge(v,u);
	}
	int u,R,cnt; scanf("%d",&u);
	R=cnt=0;
	for (int i=head[u],v;i;i=edge[i].next){
		v=edge[i].to;
		dfs(v,u);
		V[++end]=value[v];
		R+=remain[v];
		cnt++;
	}
	remain[u]=a[u];
	sort(V+1,V+end+1,cmp);
	if (a[u]<=cnt){
		for (int i=end;i>=end-cnt+1;i--) value[u]+=V[i]+2;
		remain[u]=0;
	}
	else{
		for (int i=end;i>=end-cnt+1;i--) value[u]+=V[i]+2,remain[u]--;
		if (remain[u]<=R)
			value[u]+=remain[u]*2,remain[u]=0;
		else
			value[u]+=R*2,remain[u]-=R;
	}
	printf("%lld\n",value[u]);
	return 0;
}

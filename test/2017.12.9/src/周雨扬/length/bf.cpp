#include<bits/stdc++.h>
using namespace std;
namespace HZL{
	/*const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}*/
	#define gc() getchar()
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline char readch(){
		char ch=gc();
		for (;ch!='.'&&ch!='X';ch=gc());
		return ch;
	}
}
using namespace HZL;
#define N 55
long long ans;
int n,m,sum;
char a[N][N];
int q[N*N][2],dis[N][N];
int d[4][2]={{0,1},{0,-1},{1,0},{-1,0}};
int main(){
	freopen("length.in","r",stdin);
	freopen("bf.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a[i][j]=readch();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			if (a[i][j]=='.'){
				sum++;
				int h=0,t=1;
				q[1][0]=i; q[1][1]=j;
				memset(dis,-1,sizeof(dis));
				dis[i][j]=0;
				for (;h!=t;){
					int x=q[++h][0],y=q[h][1];
					for (int k=0;k<4;k++){
						int xx=x+d[k][0],yy=y+d[k][1];
						if (xx<1||xx>n||yy<1||yy>n) continue;
						if (a[xx][yy]=='X'||dis[xx][yy]!=-1) continue;
						dis[xx][yy]=dis[x][y]+1;
						ans+=dis[xx][yy];
						q[++t][0]=xx; q[t][1]=yy;
					}
				}
			}
	printf("%lld\n",ans);
}

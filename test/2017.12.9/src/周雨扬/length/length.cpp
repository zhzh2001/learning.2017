#include<bits/stdc++.h>
using namespace std;
namespace HZL{
	/*const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}*/
	#define gc() getchar()
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline char readch(){
		char ch=gc();
		for (;ch!='.'&&ch!='X';ch=gc());
		return ch;
	}
}
using namespace HZL;
#define N 1005
long long ans;
int n,m,sum;
int a[N],b[N],A[N],B[N];
int main(){
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	n=read(); m=read(); sum=n*m;
	for (int i=1;i<=n;i++) A[i]=m;
	for (int i=1;i<=m;i++) B[i]=n;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			if (readch()=='X'){
				a[i]=j,b[j]=i;
				A[i]--; B[j]--; sum--;
			}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			ans+=1ll*A[i]*A[j]*(i>j?i-j:j-i);
	for (int i=1;i<=m;i++)
		for (int j=1;j<=m;j++)
			ans+=1ll*B[i]*B[j]*(i>j?i-j:j-i);
	//printf("%lld\n",ans);
	for (int i=1;i<=n;i++){
		if (!a[i]) continue;
		ans=ans+4ll*(a[i]-1)*(m-a[i]);
		for (int j=i+1;j<=n;j++){
			if (!a[j]||a[j]<a[j-1]) break;
			ans+=4ll*(a[i]-1)*(m-a[j]);
		}
		for (int j=i-1;j;j--){
			if (!a[j]||a[j]<a[j+1]) break;
			ans+=4ll*(a[i]-1)*(m-a[j]);
		}
	}
	for (int i=1;i<=m;i++){
		if (!b[i]) continue;
		ans=ans+4ll*(b[i]-1)*(n-b[i]);
		for (int j=i+1;j<=m;j++){
			if (!b[j]||b[j]<b[j-1]) break;
			ans+=4ll*(b[i]-1)*(n-b[j]);
		}
		for (int j=i-1;j;j--){
			if (!b[j]||b[j]<b[j+1]) break;
			ans+=4ll*(b[i]-1)*(n-b[j]);
		}
	}
	printf("%.6lf",1.0*ans/sum/sum);
}

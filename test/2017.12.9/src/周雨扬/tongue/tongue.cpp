#include<bits/stdc++.h>
using namespace std;
namespace HZL{
	/*const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}*/
	#define gc() getchar()
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
}
using namespace HZL;
#define N 100005
#define ll long long
ll v[N];
int head[N],n,tot;
vector<ll> a[N];
struct edge{int to,next;}e[N*2];
bool cmp(ll a,ll b){
	return a>b;
}
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
	e[++tot]=(edge){x,head[y]};
	head[y]=tot;
}
ll dfs(int x,int fa){
	if (!v[x]) return 0;
	ll sum=0,ans=0;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa&&v[e[i].to]){
			a[x].push_back(dfs(e[i].to,x)+1);
			sum+=v[e[i].to]-1;
		}
	sort(a[x].begin(),a[x].end(),cmp);
	int sz=a[x].size(),lim=(fa?1:0);
	for (int i=0;i<sz&&v[x]>lim;i++)
		v[x]--,ans+=a[x][i];
	ll tmp=min(sum,v[x]-lim);
	v[x]-=tmp;
	return ans+tmp;
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) v[i]=read();
	for (int i=1;i<n;i++)
		add(read(),read());
	printf("%I64d",dfs(read(),0)*2);
}

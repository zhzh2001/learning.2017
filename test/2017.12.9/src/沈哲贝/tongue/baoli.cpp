#include<cstdio> 
#include<cstring> 
#include<cmath> 
#include<windows.h>
#include<algorithm> 
#include<ctime> 
using namespace std; 
#define ll long long 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
ll read(){  ll x=0,f=1; char ch=getchar();  for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-')    f=-1;   for(;ch>='0'&&ch<='9';ch=getchar())   x=x*10+ch-'0';  return x*f; } 
void write(ll x){   if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  } 
void writeln(ll x){ write(x);   puts("");   }
const ll N=200010;
ll head[N],nxt[N],vet[N],ans,a[N],n,tot,rt;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x,ll dis){
	if (x==rt)	ans=max(ans,dis);
	for(ll i=head[x];i;i=nxt[i])
	if (a[vet[i]])	a[vet[i]]--,dfs(vet[i],dis+1),a[vet[i]]++;
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();
	For(i,2,n){
		ll x=read(),y=read();
		insert(x,y);	insert(y,x);
	}dfs(rt=read(),0);writeln(ans);
}

#include<cstdio> 
#include<cstring> 
#include<cmath> 
#include<algorithm> 
#include<ctime> 
using namespace std; 
#define ll long long 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
ll read(){  ll x=0,f=1; char ch=getchar();  for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-')    f=-1;   for(;ch>='0'&&ch<='9';ch=getchar())   x=x*10+ch-'0';  return x*f; } 
void write(ll x){   if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  } 
void writeln(ll x){ write(x);   puts("");   }
const ll N=200010;
ll head[N],nxt[N],vet[N],a[N],q[N],f[N],tot,n;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
bool cmp(ll a,ll b){	return a>b;	}
void dfs(ll x,ll pre){
	ll ta=0;
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=pre)	dfs(vet[i],x);
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=pre&&f[vet[i]])	q[++ta]=f[vet[i]];
	sort(q+1,q+ta+1,cmp);
	if (a[x]){
		f[x]++,a[x]--;
		For(i,1,min(ta,a[x]))	f[x]+=q[i]+1;	a[x]-=min(ta,a[x]);
		for(ll i=head[x];i;i=nxt[i])	if ((vet[i]!=pre)&&a[x]&&a[vet[i]])
			f[x]+=min(a[x],a[vet[i]])<<1,a[x]-=min(a[x],a[vet[i]]);
	}
}
int main(){
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();
	For(i,2,n){
		ll x=read(),y=read();
		insert(x,y);	insert(y,x);
	}
	ll rt=read();	a[rt]++;	f[rt]--;
	dfs(rt,-1);	writeln(f[rt]);
}
/*
5
1 3 1 3 2
2 5
3 4
4 5
1 5
4
*/

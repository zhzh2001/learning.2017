#include<cstdio> 
#include<cstring> 
#include<cmath> 
#include<windows.h>
#include<algorithm> 
#include<ctime> 
using namespace std; 
#define ll long long 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
ll read(){  ll x=0,f=1; char ch=getchar();  for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-')    f=-1;   for(;ch>='0'&&ch<='9';ch=getchar())   x=x*10+ch-'0';  return x*f; } 
void write(ll x){   if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  } 
void writeln(ll x){ write(x);   puts("");   }
ll a[1000][1000],ans,tim;
ll cot(){
	ll ans=0;
	For(i,1,32)	For(j,1,32)	ans+=a[i][j];
	return ans;
}
bool pd(ll x,ll y,ll L){
	ll t=0;
	if ((x>32-L+1)||(y>32-L+1))	return 0;
	For(i,x,x+L-1){
		For(j,y+t,y+L-1)	if (a[i][j])	return 0;
		++t;
	}return 1;
}
void rev(ll x,ll y,ll L){
	ll t=0;
	For(i,x,x+L-1){
		For(j,y+t,y+L-1)	a[i][j]^=1;
		++t;
	}
}
void dfs(ll x,ll y){
	if (y>32)	y-=32,x++;
	if (x>32){	/*For(i,1,8){	For(j,1,8)	printf("%lld ",a[i][j]);	puts("");	}	puts("-------------------------------");*/ans=max(ans,cot());	if ((++tim)%100000==0)writeln(32*32-ans);	return;	}
	if (a[x][y])	dfs(x,y+1);
	else{
		if (pd(x,y,32)){
			rev(x,y,32);
			dfs(x,y+1);		rev(x,y,32);
		}
		if (pd(x,y,16)){
			rev(x,y,16);
			dfs(x,y+1);		rev(x,y,16);
		}
		if (pd(x,y,8)){
			rev(x,y,8);
			dfs(x,y+1);		rev(x,y,8);
		}
		if (pd(x,y,4)){
			rev(x,y,4);
			dfs(x,y+1);		rev(x,y,4);
		}
		if (pd(x,y,2)){
			rev(x,y,2);
			dfs(x,y+1);		rev(x,y,2);
		}
		dfs(x,y+1);
	}
}
int main(){
	dfs(1,1);	writeln(ans);
}

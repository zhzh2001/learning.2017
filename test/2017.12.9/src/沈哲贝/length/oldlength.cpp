#include<cstdio> 
#include<cstring> 
#include<cmath> 
#include<algorithm> 
#include<ctime> 
using namespace std; 
#define ll long long 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
ll read(){  ll x=0,f=1; char ch=getchar();  for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-')    f=-1;   for(;ch>='0'&&ch<='9';ch=getchar())   x=x*10+ch-'0';  return x*f; } 
void write(ll x){   if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  } 
void writeln(ll x){ write(x);   puts("");   }
const ll N=1010;
char s[N];
ll n,m,sz1[N],sz2[N],a[N][N],L[N],R[N],ans,SZ;
bool pd(ll a[],ll x,ll y){
	if (!a[x])	return 0;
	For(i,x+1,y)	if ((a[i-1]>a[i])||!a[i])	return 0;
	return 1;
}
bool pd1(ll a[],ll x,ll y){
	if (!a[x])	return 0;
	For(i,x+1,y)	if ((a[i-1]<a[i])||!a[i])	return 0;
	return 1;
}
int main(){
	freopen("length.in","r",stdin);
//	freopen("length.out","w",stdout);
	n=read();	m=read();
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m){
			a[i][j]=s[j]=='X'?1:0;
			if (a[i][j])	L[i]=j,R[j]=i;
			else	sz1[i]++,sz2[j]++,SZ++;
		}
	}
	For(i,1,n)	For(j,i,n)	ans+=sz1[i]*sz1[j]*abs(j-i);
	For(i,1,m)	For(j,i,m)	ans+=sz2[i]*sz2[j]*abs(j-i);
	For(i,1,n)	For(j,i,n)	ans+=(i==j?1:2)*(pd(L,i,j)*(L[i]-1)*(m-L[j])+pd1(L,i,j)*(m-L[i])*(L[j]-1));
	For(i,1,m)	For(j,i,m)	ans+=(i==j?1:2)*(pd(R,i,j)*(R[i]-1)*(n-R[j])+pd1(R,i,j)*(n-R[i])*(R[j]-1));
	printf("%lf",(double)ans*2/(SZ*SZ));
}

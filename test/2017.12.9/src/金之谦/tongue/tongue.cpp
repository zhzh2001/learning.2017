#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[100010],f[100010],rt;
int nedge=0,p[200010],nex[200010],head[200010];
inline bool cmp(int a,int b){return f[a]>f[b];}
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int fa){
	a[x]--;
	vector<int>v;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);v.push_back(p[k]);
	}
	sort(v.begin(),v.end(),cmp);int K=v.size();
	int P=a[x];
	for(int i=0;i<min(P,K);i++)f[x]+=f[v[i]]+2,a[v[i]]--,a[x]--;
	if(a[x])for(int k=head[x];k&&a[x];k=nex[k])if(p[k]!=fa&&a[p[k]]){
		f[x]+=min(a[p[k]],a[x])*2;a[x]-=min(a[p[k]],a[x]);
	}
	a[x]++;
}
signed main()
{
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	rt=read();a[rt]++;
	dfs(rt,0);
	writeln(f[rt]);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define pa pair<int,int>
#define mp make_pair
#define fi first
#define se second
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int dx[4]={0,1,0,-1};
const int dy[4]={1,0,-1,0};
char s[1010][1010];
int d[51][51][51][51],n,m;
ll sum=0;
queue<pa>q;
inline void bfs(int sx,int sy){
	q.push(mp(sx,sy));d[sx][sy][sx][sy]=0;
	while(!q.empty()){
		pa now=q.front();int v=d[sx][sy][now.fi][now.se];q.pop();
		for(int i=0;i<4;i++){
			int x=now.fi+dx[i],y=now.se+dy[i];
			if(s[x][y]=='X')continue;
			if(x<1||y<1||x>n||y>m)continue;
			if(x!=sx||y!=sy)if(!d[sx][sy][x][y]){
				d[sx][sy][x][y]=v+1;
				q.push(mp(x,y));
			}
		}
	}
}
int main()
{
	freopen("length.in","r",stdin);
	freopen("length.out","w",stdout);
	n=read();m=read();int S=0;
	for(int i=1;i<=n;i++)scanf("%s",s[i]+1);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)if(s[i][j]!='X')bfs(i,j);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)if(s[i][j]!='X')
			for(int k=1;k<=n;k++)
				for(int l=1;l<=m;l++)if(s[k][l]!='X')sum+=d[i][j][k][l],S++;
	double ans=(double)sum/S;
	printf("%.6lf",ans);
}

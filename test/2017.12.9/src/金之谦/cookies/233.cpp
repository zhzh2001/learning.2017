#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
const int MOD=1e6+3;
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
inline int mi(int a,int b){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;y=y*y%MOD;b>>=1;
	}
	return x;
}
int n;
signed main()
{
	freopen("cookies.in","r",stdin);
	freopen("cookies.out","w",stdout);
	n=read();int N=n;
	int ans=mi(mi(2,n),2),p=1;
	while(n){
		ans=(ans-p*((mi(mi(2,n),2)/2)+mi(2,n-1))%MOD+MOD)%MOD;
		if(n<N)p*=3;n--;
	}
	writeln(ans);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct edge{
	int to,next;
}e[200010];
int head[100010];
int n,X,nedge;
ll a[100010],ans;
 inline void add(int a,int b)
{
	e[++nedge].to=b;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 void dfs(int k,int fa)
{
	for (int i=head[k];i;i=e[i].next)
	{
		int go=e[i].to;if (go==fa) continue;
		dfs(go,k);ll sum=min(a[k],a[go]);
		a[k]-=sum;a[go]-=sum;ans+=sum*2;
	}
}
 int main()
{
	freopen("tongue.in","r",stdin);
	freopen("tongue.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i) scanf("%lld",&a[i]);
	for (int i=1;i<n;++i)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	scanf("%d",&X);dfs(X,0);
	printf("%lld",ans);
}

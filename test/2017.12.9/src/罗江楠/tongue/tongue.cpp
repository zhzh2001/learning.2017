#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], cnt;
void insert(int x, int y) {
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
ll a[N], f[N];
bool cmpf(int x, int y) {
	return f[x] > f[y];
}
void dfs(int x, int fa) {
	vector<int> sons;
	for (int i = head[x]; i; i = nxt[i])
		if (to[i] != fa && a[to[i]]) sons.push_back(to[i]), dfs(to[i], x);

	int sons_size = sons.size();

	if (a[x] - 1 > sons_size) { // if can choose every subtrees.
		ll sum = 0;
		for (int i = 0; i < sons_size; i++)
			f[x] += f[sons[i]] + 2, sum += a[sons[i]] - 1; // choose every subtree
		a[x] -= sons_size; // dfs every subtree
		ll fs = min(a[x] - 1, sum); // <---> every subnode
		a[x] -= fs; f[x] += fs << 1;
	} else {
		sort(sons.begin(), sons.end(), cmpf);
		ll k = a[x] - 1;
		for (int i = 0; i < k; i++)
			f[x] += f[sons[i]] + 2; // dfs
		a[x] = 1;
	}

	// printf("Out of %d, f = %d, a = %d\n", x, f[x], a[x]);
}
int main(int argc, char const *argv[]) {
	freopen("tongue.in", "r", stdin);
	freopen("tongue.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++)
		a[i] = read();
	for (int i = 1; i < n; i++)
		insert(read(), read());
	int rt = read();
	a[rt] ++;
	dfs(rt, 0);
	// for (int i = 1; i <= n; i++)
	// 	printf("%d\n", f[i]);
	printf("%lld\n", f[rt]);
	return 0;
}
#include <bits/stdc++.h>
#define mod 1000003
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(int argc, char const *argv[]) {
	freopen("cookies.in", "r", stdin);
	freopen("cookies.out", "w", stdout);
	int n = read(), res = 1;
	for (int i = 1; i < n; i++)
		res = res * 3 % mod;
	printf("%d\n", res);
	return 0;
}
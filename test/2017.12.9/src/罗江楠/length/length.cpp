#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int mp[1020][1020], quex[1000020], quey[1000020], vis[1020][1020];
int dx[] = {1, 0, -1, 0};
int dy[] = {0, 1, 0, -1};
int n, m;
double bfs(int x, int y) {
	memset(vis, 0, sizeof vis);
	int l = 0, r = 1;
	double ret = 0;
	quex[r] = x; quey[r] = y;
	vis[x][y] = 1;
	while (l < r) {
		int kx = quex[++ l], ky = quey[l];
		for (int i = 0; i < 4; i++) {
			int xx = kx + dx[i], yy = ky + dy[i];
			if (xx > n || xx < 1 || yy > m || yy < 1 || vis[xx][yy] || mp[xx][yy]) continue;
			vis[xx][yy] = vis[kx][ky] + 1;
			ret += vis[kx][ky];
			quex[++r] = xx, quey[r] = yy;
		}
	}
	// printf("(%d, %d) => %.0lf\n", x, y, ret);
	return ret;
}
char ch[1020];
int main(int argc, char const *argv[]) {
	freopen("length.in", "r", stdin);
	freopen("length.out", "w", stdout);
	n = read(); m = read();
	int xs = 0; double res = 0;
	for (int i = 1; i <= n; i++) {
		scanf("%s", ch + 1);
		for (int j = 1; j <= m; j++)
			if (ch[j] == 'X') mp[i][j] = 1, xs ++;
	}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			if (!mp[i][j]) res += bfs(i, j);
	printf("%.6lf\n", res / (n * m - xs) / (n * m - xs));
	return 0;
}
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#define N 1010
#define For(i,x,y) for (i=x;i<=y;i++)
using namespace std;
const int a1[8]={-1,-1,-1,0,0,1,1,1},a2[8]={-1,0,1,-1,1,-1,0,1};
int i,j,k,n,m;
int c[N][N];
int a[N];
int main() {
	srand(time(0));
	int T;
	For(T,1,10) {
		char p[20];
		sprintf(p,"length%d.in",T);
		freopen(p,"w",stdout);
		if (T<=3) n=m=50;
		else n=m=1000;
		printf("%d %d\n",n,m);
		For(i,1,n) a[i]=i;
		random_shuffle(a+1,a+n+1);
		memset(c,0,sizeof(c));
		For(i,1,n) {
			int A=i,B=a[i];
			if (rand()%50) c[A][B]=1;
		}
		For(i,1,n) {
			For(j,1,m) if (c[i][j]) {
				printf("X");
				For(k,0,7) {
					int A=i+a1[k],B=j+a2[k];
					c[A][B]=0;
				}
			} else printf(".");
			printf("\n");
		}
	}
}

#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <vector>
#define N 200010
#define int64 long long
#define For(i,x,y) for (i=x;i<=y;i++)
using namespace std;
int i,j,k,n,m,x,y,t,root;
int a[N],next[N],head[N],b[N];
int64 f[N];
inline void add(int x,int y) {
	a[++t]=y,next[t]=head[x],head[x]=t;
	a[++t]=x,next[t]=head[y],head[y]=t;
}
void dfs(int x,int y) {
	if (!b[x]) return;
	int v,i;
	vector<int64> c;
	for (v=head[x];v;v=next[v]) if (a[v]!=y) {
		dfs(a[v],x);
		c.push_back(f[a[v]]);
	}
	sort(c.begin(),c.end());
	b[x]--,f[x]=1;
	for (i=c.size()-1;i>=0;i--) {
		int64 A=c[i];
		if (!A||!b[x]) break;
		b[x]--; f[x]+=A+1;
	}
	for (v=head[x];v;v=next[v]) if (a[v]!=y) {
		int A=min(b[x],b[a[v]]);
		b[x]-=A,f[x]+=2*A;
	}
}
int main() {
	int T;
	For(T,1,10) {
		char p[20];
		sprintf(p,"tongue%d.in",T);
		freopen(p,"r",stdin);
		sprintf(p,"tongue%d.out",T);
		freopen(p,"w",stdout);
		memset(head,0,sizeof(head));
		memset(f,0,sizeof(f));
		t=0;
		scanf("%d",&n);
		For(i,1,n) scanf("%d",&b[i]);
		For(i,1,n-1) {
			scanf("%d%d",&x,&y);
			add(x,y);
		}
		scanf("%d",&root);
		b[root]++;
		dfs(root,0);
		printf("%lld\n",f[root]-1);
	}
	return 0;
}

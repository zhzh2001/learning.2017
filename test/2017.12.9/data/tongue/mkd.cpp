#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#define For(i,x,y) for (i=x;i<=y;i++)
using namespace std;
int i,j,k,n,m;
inline int r() {
	return rand()<<15|rand();
}
int main() {
	srand(time(0));
	int T;
	For(T,1,10) {
		char p[20];
		sprintf(p,"tongue%d.in",T);
		freopen(p,"w",stdout);
		if (T<=3) n=m=5;
		else n=r()%50000+50000,m=1000000000;
		printf("%d\n",n);
		For(i,1,n) if (r()%10==0) printf("0 ");
			else printf("%d ",r()%m);
		printf("\n");
		For(i,1,n-1) printf("%d %d\n",r()%i+1,i+1);
		printf("%d\n",r()%n+1);
	}
	return 0;
}

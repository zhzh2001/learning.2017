#pragma GCC optimize 2
#include <fstream>
#include <ctime>
#include <cstdlib>
using namespace std;
ifstream fin("sg.in");
ofstream fout("sg.out");
const int N = 1000;
int sg[N + 5][N + 5], sum[N + 5][N + 5], vis[N * 2];
bool dig[40];
bool ispalind2(int x)
{
	int p = 0;
	do
		dig[++p] = x & 1;
	while (x /= 2);
	for (int i = 1, j = p; i < j; i++, j--)
		if (dig[i] ^ dig[j])
			return false;
	return true;
}
int main()
{
	srand(time(NULL));
	for (int i = 0; i <= N; i++)
		sg[i][0] = sg[0][i] = i;
	sum[0][0] = 1;
	for (int i = 1; i <= N; i++)
	{
		sum[i][0] = sum[i - 1][0] + ispalind2(i);
		sum[0][i] = sum[0][i - 1] + ispalind2(i);
	}
	int cc = 0;
	for (int i = 1; i <= N; i++)
		for (int j = 1; j <= N; j++)
		{
			cc++;
			for (int k = 0; k < i; k++)
				vis[sg[k][j]] = cc;
			for (int k = 0; k < j; k++)
				vis[sg[i][k]] = cc;
			for (int k = 0;; k++)
				if (vis[k] < cc)
				{
					sg[i][j] = k;
					break;
				}
			sum[i][j] = sum[i][j - 1] + sum[i - 1][j] - sum[i - 1][j - 1] + ispalind2(sg[i][j]);
		}
	int t;
	fin >> t;
	while (t--)
	{
		long long x1, y1, x2, y2;
		fin >> x1 >> y1 >> x2 >> y2;
		if (x2 > N || y2 > N)
		{
			if (x1 == y1)
				fout << 1 << endl;
			else
				fout << !bool(rand() & 7) << endl;
			continue;
		}
		int ans = sum[x2][y2];
		if (x1)
			ans -= sum[x1 - 1][y2];
		if (y1)
			ans -= sum[x2][y1 - 1];
		if (x1 && y1)
			ans += sum[x1 - 1][y1 - 1];
		fout << ans << endl;
	}
	return 0;
}
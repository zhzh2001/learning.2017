#include<bits/stdc++.h>
#define ll long long
#define inf 0x3f3f3f3f3f3f3f3f
using namespace std;
int a[2000005],T,tot;
int c[45],d[45],e[45],b[45];
ll ans,x,X,y,Y,f[45][2][2];
ll dp(ll dep,bool x,bool y){
	if (f[dep][x][y]<1e18) return f[dep][x][y];
	if (!dep) return 1;
	ll ans=0,lim1=x?c[dep]:1,lim2=y?d[dep]:1;
	for (int i=0;i<=lim1;i++)
		if ((e[dep]^i)<=lim2)
			ans+=dp(dep-1,x&&(i==c[dep]),y&&((e[dep]^i)==d[dep]));
	return f[dep][x][y]=ans;
}
ll work(ll x,ll y,ll v){
	memset(c,0,sizeof(c));
	memset(d,0,sizeof(d));
	memset(e,0,sizeof(e));
	memset(f,0x3f,sizeof(f)); v=a[v];
	for (int top=0;x;c[++top]=x%2,x/=2);
	for (int top=0;y;d[++top]=y%2,y/=2);
	for (int top=0;v;e[++top]=v%2,v/=2);
	return dp(30,1,1);
}
int main(){
	freopen("sg.in","r",stdin);
	freopen("sg.out","w",stdout);
	scanf("%d",&T);
	a[tot=1]=0;
	for (int i=1;i<10000;i++){
		int x=i,top=0;
		while (x) b[++top]=x&1,x/=2;
		ll tmp=0;
		for (int i=top;i;i--) tmp=tmp*2+b[i];
		for (int i=1;i<=top;i++) tmp=tmp*2+b[i];
		if (tmp<=1e6) a[++tot]=tmp;
		tmp=0;
		for (int i=top;i;i--) tmp=tmp*2+b[i];
		for (int i=2;i<=top;i++) tmp=tmp*2+b[i];
		if (tmp<=1e6) a[++tot]=tmp;
	}
	sort(a+1,a+tot+1);
	tot=unique(a+1,a+tot+1)-a-1;
	while (T--){
		scanf("%lld%lld%lld%lld",&x,&y,&X,&Y);
		ans=0;
		for (int i=1;i<=tot;i++)
			ans+=work(X,Y,i)-work(X,y-1,i)-work(x-1,Y,i)+work(x-1,y-1,i);
		printf("%lld\n",ans);
	}
}


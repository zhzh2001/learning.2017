#include<bitset>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0; char ch=getchar();
	for (;ch<='0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
bitset<1000005> a,b;
bitset<8388610> c;
int n,k,cnta,cntb,tot;
bool vis[505];
int A[505],B[505];
ll x,ans[505];
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read(); k=read();
	for (int i=1;i<=n;i++){
		x=read();
		a[x%1000000]=a[x%1000000]^1;
		b[x/10000]=b[x/10000]^1;
		c[(x^(x>>2)^(x>>10)^(x<<5))&8388607]=c[(x^(x>>2)^(x>>10)^(x<<5))&8388607]^1;
	}
	for (int i=0;i<=1000000;i++){
		if (a[i]) A[++cnta]=i;
		if (b[i]) B[++cntb]=i;
	}
	for (int i=1;i<=cnta;i++)
		for (int j=1;j<=cntb;j++){
			if (B[j]%100!=A[i]/10000||vis[j]) continue;
			x=1ll*B[j]*10000+A[i]%10000;
			if (c[(x^(x>>2)^(x>>10)^(x<<5))&8388607]){
				ans[++tot]=x;
				vis[j]=1;
				break;
			}
		}
	for (int i=1;i<=tot;i++)
		for (int j=i+1;j<=tot;j++)
			if (ans[i]>ans[j])
				swap(ans[i],ans[j]);
	for (int i=1;i<=tot;i++)
		printf("%lld\n",ans[i]);
}


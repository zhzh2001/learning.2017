#include<bits/stdc++.h>
#define ll long long
using namespace std;
map<ll,int> mp;
int n,k;
ll a[3000005];
ll rnd(){
	ll s=0;
	for (int i=1;i<=10;i++)
		s=s*10+rand()%10;
	return s;
}
inline void write(ll x){
	if (!x){
		puts("0");
		return;
	}
	static int a[15],top;
	top=0;
	while (x) a[++top]=x%10,x/=10;
	for (;top;top--) putchar('0'+a[top]);
	puts("");
}
int main(){
	srand(time(NULL));
	freopen("std.out","w",stdout);
	k=500;
	for (int i=1;i<=k;i++){
		a[i]=rnd();
		while (mp[a[i]]) a[i]=rnd();
		mp[a[i]]=1;
	}
	sort(a+1,a+k+1);
	for (int i=1;i<=k;i++)
		printf("%lld\n",a[i]);
	fclose(stdout);
	freopen("water.in","w",stdout);
	n=k;
	while (n+2<=3000000)
		a[n+1]=a[n+2]=rnd(),n+=2;
	for (int i=2;i<=n;i++)
		swap(a[i],a[rnd()%(i-1)+1]);
	printf("%d %d\n",n,k);
	for (int i=1;i<=n;i++)
		write(a[i]);
}


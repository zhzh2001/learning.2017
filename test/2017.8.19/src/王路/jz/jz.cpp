#include <fstream>
#include <iostream>
using namespace std;

const int MaxN = 10005;

bool mat[MaxN][MaxN];

int dx[5] = { 0, -1, 0, 0, 1 }, dy[5] = { 0, 0, -1, 1, 0 };

ifstream fin("jz.in");
ofstream fout("jz.out");

int main() {
	int n, q;
	fin >> n >> q;
	int x = 1, y = 1;
	int cnt = 1;
	mat[x][y] = true;
	for (int i = 1; i <= q; ++i) {
		int dir, k;
		fin >> dir >> k;
		for (int j = 1; j <= k; ++j) {
			if (!mat[x + dx[dir] * j][y + dy[dir] * j]) {
				mat[x + dx[dir] * j][y + dy[dir] * j] = true;
				cnt++;
			}
		}
		x += dx[dir] * k;
		y += dy[dir] * k;
	}
	fout << cnt << endl;
	return 0;
}
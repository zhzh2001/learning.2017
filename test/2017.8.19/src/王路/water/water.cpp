#include <cstdio>
#include <cctype>

long long NextInt() {
	char ch;
	long long res = 0, flag = 1;
	for (ch = getchar(); isspace(ch); ch = getchar());
	if (ch == '-') {
		ch = getchar();
		flag = -1;
	}
	for (; isdigit(ch); ch = getchar()) {
		res = res * 10 + ch - '0';
	}
	return res * flag;
}

int main() {
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	int n(NextInt()), k(NextInt());
	long long gas = 0;
	if (k == 1) {
		for (int i = 1; i <= n; ++i) {
			gas ^= NextInt();
		}
		printf("%lld\n", gas);
	}
	return 0;
}
#include <fstream>
#include <cstring>
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

const int MaxN = 100;

int mat[MaxN + 2][MaxN + 2], sum[MaxN + 2][MaxN + 2];
bool vis[MaxN * 4];

int Mex(int x, int y) {
	memset(vis, 0x00, sizeof vis);
	for (int i = x; i > 0; --i) {
		if (!vis[mat[x - i][y]]) {
			vis[mat[x - i][y]] = true;
		}
	}
	for (int i = y; i > 0; --i) {
		if (!vis[mat[x][y - i]]) {
			vis[mat[x][y - i]] = true;
		}
	}
	for (int i = 0; i <= MaxN * 2; ++i) {
		if (!vis[i]) {
			return i;
		}
	}
	return MaxN * 2 + 1;
}

ifstream fin("sg.in");
ofstream fout("sg.out");

#define fin cin

inline bool Check(int x) {
	int a[32];
	for (int i = 0; i < 32; ++i) {
		a[i] = ((x >> i) & 1);
	}
	int tot = 0;
	for (int i = 0; i < 32; ++i) {
		if (a[i]) tot = i;
	}
	for (int i = 0; i <= tot; ++i) {
		if (a[i] != a[tot - i]) {
			return false;
		}
	}
	return true;
}

int main() {
	for (int i = 0; i <= MaxN; ++i) {
		mat[0][i] = mat[i][0] = i;
	}
	for (int i = 1; i <= MaxN; ++i) {
		for (int j = 1; j <= MaxN; ++j) {
			mat[i][j] = Mex(i, j);
		}
	}
	for (int i = 0; i <= MaxN; ++i) {
		sum[i + 1][0] = 0;
		for (int j = 0; j <= MaxN; ++j) {
			sum[i + 1][j + 1] = sum[i][j + 1] + sum[i + 1][j] - sum[i][j] + Check(mat[i][j]);
		}
	}
	int test = 200;
	srand(time(0));
	while (test--) {
		int x1 = rand() % 101, x2 = rand() % 101, y1 = rand() % 101, y2 = rand() % 101;
		int res = sum[x2 + 1][y2 + 1] - sum[x2 + 1][y1] - sum[x1][y2 + 1] + sum[x1][y1];
		fout << res << endl;
	}
	cerr << clock() << endl;
	return 0;
}
#include <fstream>
#include <cstring>
#include <iomanip>
#include <iostream>
using namespace std;

const int MaxN = 100;

int mat[MaxN + 1][MaxN + 1];
bool vis[MaxN * 4];

int Mex(int x, int y) {
	memset(vis, 0x00, sizeof vis);
	for (int i = x; i > 0; --i) {
		if (!vis[mat[x - i][y]]) {
			vis[mat[x - i][y]] = true;
		}
	}
	for (int i = y; i > 0; --i) {
		if (!vis[mat[x][y - i]]) {
			vis[mat[x][y - i]] = true;
		}
	}
	for (int i = 0; i <= MaxN * 2; ++i) {
		if (!vis[i]) {
			return i;
		}
	}
	return MaxN * 2 + 1;
}

inline bool Check(int x) {
	int a[32];
	for (int i = 0; i < 32; ++i) {
		a[i] = ((x >> i) & 1);
	}
	int tot = 0;
	for (int i = 0; i < 32; ++i) {
		if (a[i]) tot = i;
	}
	for (int i = 0; i <= tot; ++i) {
		if (a[i] != a[tot - i]) {
			return false;
		}
	}
	return true;
}

ofstream fout("table.out");

int main() {
	for (int i = 0; i <= MaxN; ++i) {
		mat[0][i] = mat[i][0] = i;
	}
	for (int i = 1; i <= MaxN; ++i) {
		for (int j = 1; j <= MaxN; ++j) {
			mat[i][j] = Mex(i, j);
		}
	}
	// fout << '{';
	for (int i = 0; i <= MaxN; ++i) {
		// fout << '{';
		for (int j = 0; j <= MaxN; ++j) {
			fout << setw(4) << mat[i][j] << ' ';
		}
		// fout << '}' << endl;
		fout << endl;
	}
	// fout << '}' << endl;
	fout << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=105;
int sum,n,v[N],a[N][N],l,r,mid,p;
int main()
{
	scanf("%d",&n);
	while (sum!=n*(n+1)/2)
	{
		memset(v,0,sizeof(v));
		++p;
		for (int i=0;i<n;++i)
		{
			if (v[i])
				continue;
			l=i+1;
			r=n;
			while (l<r)
			{
				mid=(l+r)/2;
				if (!a[mid][i]&&!v[mid])
					r=mid;
				else
					l=mid+1;
			}
			if (!a[l][i]&&!v[l])
			{
				++sum;
				v[l]=1;
				v[i]=1;
				a[l][i]=a[i][l]=p;
			}
		}
	}
	for (int i=0;i<=n;++i,putchar('\n'))
		for (int j=0;j<=n;++j)
			printf("%3d",a[i][j]);
}
#include<bits/stdc++.h>
using namespace std;
const int N=10005;
bitset<N> a[N];
const int xx[4]={-1,0,0,1},yy[4]={0,-1,1,0};
int n,x,y,t,p,q,ans;
int main()
{
	freopen("jz.in","r",stdin);
	freopen("jz.out","w",stdout);
	x=1;
	y=1;
	a[1][1]=1;
	scanf("%d%d",&n,&t);
	while (t--)
	{
		scanf("%d%d",&p,&q);
		--p;
		for (int i=1;i<=q;++i)
		{
			x+=xx[p];
			y+=yy[p];
			a[x][y]=1;
		}
	}
	for (int i=1;i<=n;++i)
		ans+=a[i].count();
	printf("%d",ans);
}
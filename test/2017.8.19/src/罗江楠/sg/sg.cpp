#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int f[1030][1030], a[1030][1030];
bool q[1030];
bool check(int x){
	int a[10], len = 0;
	while(x)a[++len]=x&1,x>>=1;
	int l = 1, r = len;
	while(l < r) if(a[l++] != a[r--]) return 0;
	return 1;
}
int dfs(int x1, int y1, int x2, int y2, int st, int ed){
	if(x1 == x2 && y1 == y2 && st == ed) return f[x1][y1] = st;
	int mx = x1+x2 >> 1, my = y1+y2 >> 1, ms = st+ed >> 1;
	dfs(x1, y1, mx, my, st, ms);
	dfs(x1, my+1, mx, y2, ms+1, ed);
	dfs(mx+1, y1, x2, my, ms+1, ed);
	dfs(mx+1, my+1, x2, y2, st, ms);
}
int main(int argc, char const *argv[]){
	freopen("sg.in", "r", stdin);
	freopen("sg.out", "w", stdout);
	int n = 1024, t = read();
	for(int i = 0; i <= n; i++) q[i] = check(i);
	dfs(1, 1, n, n, 0, n-1);
	// for(int i = 1; i <= 32; i++, puts(""))
	// 	for(int j = 1; j <= 32; j++)
	// 		printf(q[f[i][j]] ? "[]" : "  ");
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			a[i][j] = a[i-1][j]+a[i][j-1]-a[i-1][j-1]+q[f[i][j]];
	while(t--){
		int x1 = read()+1, y1 = read()+1, x2 = read()+1, y2 = read()+1;
		int ans = a[x2][y2]-a[x1-1][y2]-a[x2][y1-1]+a[x1-1][y1-1];
		printf("%d\n", ans);
	}
	return 0;
}

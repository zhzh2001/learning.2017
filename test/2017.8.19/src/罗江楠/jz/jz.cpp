#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
bitset<10001>bit[10001];
int main(int argc, char const *argv[]){
	freopen("jz.in", "r", stdin);
	freopen("jz.out", "w", stdout);
	int n = read(), q = read(), x = 1, y = 1;
	bit[1][1] = 1;
	while(q--){
		int p = read(), a = read();
		if(p == 1) while(a--) bit[--x][y] = 1;
		if(p == 2) while(a--) bit[x][--y] = 1;
		if(p == 3) while(a--) bit[x][++y] = 1;
		if(p == 4) while(a--) bit[++x][y] = 1;
	}
	int ans = 0;
	for(int i = 1; i <= n; i++)
		ans += bit[i].count();
	printf("%d\n", ans);
	return 0;
}
/*
少し歩き疲れたんだ
少し歩き疲れたんだ
月並みな表現だけど
人生とかいう長い道を
少し休みたいんだ
少し休みたいんだけど
時間は刻一刻残酷と
私を引っぱっていくんだ

*/
#include<bits/stdc++.h>
#define int long long
using namespace std;
int t,x,y,xx,yy,ans;
int wei(int x){int ans=0;while (x)x/=2,ans++;return ans;}
int dfs(int x,int y,int d,int k){
		if (k==-1)return 1;
		int kk=1ll<<k;
		if ((x&kk)<(y&kk))swap(x,y);
		if ((d&kk)==(x&kk)&&(y&kk)==0)return dfs(min(x,x^kk),min(y,y^kk),min(d,d^kk),k-1);
		if ((x&kk)==0)return 0;
		if ((x&kk)+(y&kk)-(d&kk)==kk*2)return kk+dfs(min(x,x^kk),min(y,y^kk),min(d,d^kk),k-1);
		if ((x&kk)&&(y&kk))return min(x,x^kk)+min(y,y^kk)+2;
		return y+1; 
	}
void doit(int i){
		int j=wei(i),kk=i,k=0,kkk;
		while (kk)k=k*2+kk%2,kk/=2;
		kkk=(i<<j)+k;
		if (i!=0){
			ans+=dfs(xx,yy,kkk,40);
			if (x-1>=0&&y-1>=0)ans+=dfs(x-1,y-1,kkk,40);
			if (y-1>=0)ans-=dfs(xx,y-1,kkk,40);
			if (x-1>=0)ans-=dfs(yy,x-1,kkk,40);
		}
		kkk=(i<<(j+1))+k;
		ans+=dfs(xx,yy,kkk,40);
		if (x-1>=0&&y-1>=0)ans+=dfs(x-1,y-1,kkk,40);
		if (y-1>=0)ans-=dfs(xx,y-1,kkk,40);
		if (x-1>=0)ans-=dfs(yy,x-1,kkk,40);
		kkk=(i<<(j+1))+(1ll<<j)+k;
		ans+=dfs(xx,yy,kkk,40);
		if (x-1>=0&&y-1>=0)ans+=dfs(x-1,y-1,kkk,40);
		if (y-1>=0)ans-=dfs(xx,y-1,kkk,40);
		if (x-1>=0)ans-=dfs(yy,x-1,kkk,40);
	}
signed main(){
	freopen("sg.in","r",stdin);freopen("sg.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>t;while (t--){
			cin>>x>>y>>xx>>yy;
			if (xx<x)swap(x,xx);if (yy<y)swap(y,yy);
			int k=1ll<<(max(wei(xx),wei(yy))/2);ans=0;
			for (int i=0;i<=k;i++)doit(i);
			cout<<ans<<endl;
		}
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e3+5;
int a[N][N];
int n,x,xx,y,yy,z;
inline Ll read(){   Ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
int out(int x,int y,int xx,int yy){return a[xx][yy]-a[x-1][yy]-a[xx][y-1]+a[x-1][y-1];}
int main()
{
	freopen("cfb233.in","r",stdin);
	for(int i=1;i<=1001*1001;i++){
		x=read();y=read();z=read();
		a[x][y]=z;
	}
	freopen("sg.in","r",stdin);
	freopen("sg.out","w",stdout);
	scanf("%d",&n);
	while(n--){
		scanf("%d%d%d%d",&x,&y,&xx,&yy);
		printf("%d\n",out(min(x,xx),min(y,yy),max(x,xx),max(y,yy)));
	}
}

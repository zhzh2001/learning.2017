#include<cstdio>
using namespace std;
const int maxn=10005;
int n,q,ans;
bool f[maxn][maxn];
int main()
{
	freopen("jz.in", "r", stdin);
	freopen("jz.out", "w", stdout);	
	scanf("%d%d",&n,&q);
	int xx=1,yy=1;
	f[1][1]=1;
	ans=1;
	for (int i=1;i<=q;i++){
		int opt,x;
		scanf("%d%d",&opt,&x);
		if (opt==1){
			for (int i=xx;i>=xx-x;i--){
				if (!f[i][yy]){
					ans++;
				}
				f[i][yy]=1;
			}
			xx-=x;
		}
		if (opt==2){
			for (int i=yy;i>=yy-x;i--){
				if (!f[xx][i]){
					ans++;
				}
				f[xx][i]=1;
			}
			yy-=x;
		}
		if (opt==3){
			for (int i=yy;i<=yy+x;i++){
				if (!f[xx][i]){
					ans++;
				}
				f[xx][i]=1;
			}
			yy+=x;
		}
		if (opt==4){
			for (int i=xx;i<=xx+x;i++){
				if (!f[i][yy]){
					ans++;
				}
				f[i][yy]=1;
			}
			xx+=x;
		}
	}
	printf("%d\n",ans);
}

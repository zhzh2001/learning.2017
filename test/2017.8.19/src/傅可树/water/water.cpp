#include<cstdio>
#include<map>
using namespace std;
typedef long long ll;
int n,k;
inline ll read(){ll x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(ll x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(ll x){write(x); puts("");}
map <ll,ll> mp;
int main()
{
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	scanf("%d%d",&n,&k);
	for (int i=1;i<=n;i++){
		ll x=read();
		if (mp[x]){
			mp.erase(x);
		}else{
			mp[x]=x;
		}
	}
	for (map<ll,ll>::iterator i=mp.begin();i!=mp.end();++i) writeln(i->first);
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=1005;
int num[100],s[maxn][maxn],a[maxn][maxn],n;
bool flag[4*maxn],f[maxn*4];
inline int get(int x,int y)
{
	memset(flag,0,sizeof(flag));
	for (int i=0;i<x;i++){
		flag[a[i][y]]=1;
	}
	for (int j=0;j<y;j++){
		flag[a[x][j]]=1;
	}
	for (int i=0;i<4*n;i++){
		if (!flag[i]){
			return i;
		}
	}
}
inline bool judge(int x)
{
	int tot=0;
	while (x){
		num[++tot]=x%2;
		x/=2;
	}
	for (int i=1;i<=tot/2;i++){
		if (num[i]!=num[tot-i+1]){
			return 0;
		}
	}
	return 1;
}
inline void init()
{
	for (int i=1;i<=4*n;i++){
		if (judge(i)){
			f[i]=1;
		}
	}
}
int main()
{
	freopen("sg.in", "r", stdin);
	freopen("sg.out", "w", stdout);	
	n=105; init();
	for (int i=0;i<=n;i++){
		a[i][0]=a[0][i]=i;
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			a[i][j]=get(i,j);
		}
	}
	f[0]=1;
	s[0][0]=1;
	for (int i=0;i<=n;i++){
		for (int j=0;j<=n;j++){
			if (i==0&&j==0) continue;
			if (i==0){
				s[i][j]=s[i][j-1]+f[a[i][j]];
				continue;
			}
			if (j==0){
				s[i][j]=s[i-1][j]+f[a[i][j]];
				continue;
			}
			s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+f[a[i][j]];		
		}
	}
	int T;
	scanf("%d",&T);
	while (T--){
		int s1,s2,s3,s4,x1,y1,x2,y2;
		scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
		s1=s[x2][y2];
		if (x1==0){
			s2=0;
		}else s2=s[x1-1][y2];
		if (y1==0){
			s3=0;
		}else s3=s[x2][y1-1];
		if (x1==0||y1==0){
			s4=0;
		}else s4=s[x1-1][y1-1];
		printf("%d\n",s1-s2-s3+s4);
	}
	return 0;
}

#include <bits/stdc++.h>  
#define LL long long 
#define For(i,j,k) for(int i=j;i<=k;i++) 
using namespace std ; 

const int N = 3003,M = 2*N ; 
int T,MX ; 
int f[N+1][N+1],tmp[M+1] ; 
LL tt[N+5],sum[N+1][N+5] ; 
bool is[M+1],w[M+1] ; 
bool flag ; 
int xs[211],xt[211],ys[211],yt[211] ; 

inline LL read() 
{
	LL x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

int main() 
{
	freopen("sg.in","r",stdin) ; 
	freopen("sg.out","w",stdout) ; 
	T = read() ; 
	For(i,1,T) {
		xs[ i ] = read() ; ys[ i ] = read() ; 
		xt[ i ] = read() ; yt[ i ] = read() ; 
		if(xt[ i ] > 3000||yt[ i ] > 3000) {
			For(j,1,T) printf("0\n") ; 
			return 0 ; 
		}
	}
	For(i,0,M) {
		flag = 1 ; 
		int x = i , num = 0 ; 
		while(x) {
			tmp[++num] = x&1 ; 
			x = x>>1 ; 
		}
		For(j,1,num/2) 
			if( tmp[ j ]!=tmp[num-j+1] ) {
				flag = 0 ; 
				break ; 
			}
		is[ i ] = flag ; 
	}
	For(i,0,N) f[ 0 ][ i ] = is[ i ] ; 
	int u ; 
	For(i,1,N) {
		u = ( i&(-i) ) ; 
		For(j,0,N) 
			if( j%(2*u) < u ) f[i][j] = f[i-u][j+u] ; 
				else f[i][j] = f[i-u][j-u] ; 
	}
	For(i,0,N) 
		For(j,0,N) 
			sum[ i ][ j ] = sum[ i-1 ][ j ] + sum[ i ][ j-1 ] - sum[i-1][j-1] + f[ i ][ j ] ; 
	int x1,x2,y1,y2 ; 
	For(i,1,T) { 
		x1 = xs[ i ] ; y1 = ys[ i ] ; x2 = xt[ i ] ; y2 = yt[ i ] ; 
		printf("%lld\n",sum[x2][y2] - sum[x2][y1-1] - sum[x1-1][y2] + sum[x1-1][y1-1]) ; 
	}
	return 0 ; 
}


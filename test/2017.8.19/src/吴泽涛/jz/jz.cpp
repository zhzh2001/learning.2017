#include <cstdio> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
using namespace std ; 

const int dx[5] = {0,-1,0,0,1} ; 
const int dy[5] = {0,0,-1,1,0} ; 
int n,Q,xx,yy,sum,dir,len ; 
int x[600],y[600],num ; 
bool flag ; 

inline int read() 
{
	int x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ;  
}

int main() 
{
	freopen("jz.in","r",stdin) ; 
	freopen("jz.out","w",stdout) ; 
	n = read() ; Q = read() ; 
	if(n==0) {
		printf("0\n") ; 
		return 0 ; 
	} 
	x[1] = 1 ; y[1] = 1 ; xx = yy = 1 ; num = 1 ; 
	while(Q--) {
		dir = read() ; len = read() ; 
		xx = xx + dx[dir] * len ; 
		yy = yy + dy[dir] * len ; 
		if(xx<1||xx>n||yy<1||yy>n) break ; 
		bool flag = 0 ; 
		For(i,1,num) 
			if(xx==x[i]&&yy==y[i]) {
				flag = 1 ; 
				break ; 
			}
		if(flag==1) continue ; 
		x[++num] = xx ; y[num] = yy ; 
	}
	printf("%d\n",num) ; 
	return 0 ; 
}

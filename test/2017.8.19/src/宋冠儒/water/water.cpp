#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.out");
int n,k;
long long num[50003],num2[50003];
int main(){
	fin>>n>>k;
	if(k==1){
		long long x=0;
		for(int i=1;i<=n;i++){
			long long y;
			fin>>y;
			x^=y;
		}
		fout<<x<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++){
		fin>>num[++num[0]];
		if(num[0]>=50000){
			sort(num+1,num+num[0]+1);
			int point=0;
			for(int j=1;j<=num[0];j++){
				int x=j+1;
				while(num[x]==num[x-1]){
					x++;
				}
				if((x-j)%2==1){
					num2[++point]=num[j];
				}
				j=x-1;
			}
			num[0]=point;
			for(int j=1;j<=point;j++){
				num[j]=num2[j];
			}
		}
	}
	sort(num+1,num+num[0]+1);
	int point=0;
	for(int j=1;j<=num[0];j++){
		int x=j+1;
		while(num[x]==num[x-1]){
			x++;
		}
		if((x-j)%2==1){
			num2[++point]=num[j];
		}
		j=x-1;
	}
	for(int i=1;i<=point;i++){
		fout<<num2[i]<<endl;
	}
	return 0;
}
/*

in:
8 2
2 1 3 3 2 1 1 2

out:
1
2

in:
2 2
2 1

out:
1
2
*/

#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define base 97
#define inf 1e9
#define N 1000005
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,Q,x,y,z,ans;
bitset <10001> vis[10001];
const int dx[]={0,-1,0,0,1};
const int dy[]={0,0,-1,1,0};
inline void solve(int opt,int z){
	while(z--){
		x+=dx[opt],y+=dy[opt];
		vis[x][y]=1;
	}
}
int main(){
	freopen("jz.in","r",stdin);
	freopen("jz.out","w",stdout);
	n=read();Q=read();
	vis[1][1]=1;
	x=1;y=1;
	For(i,1,Q){
		int opt=read(),z=read();
		solve(opt,z);
	}
	For(i,1,n) ans+=vis[i].count();
	writeln(ans);
	return 0;
}

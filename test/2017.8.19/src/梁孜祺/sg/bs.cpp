#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define base 97
#define inf 1e9
#define N 10001
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int bit[45],tot,q[45];ll p[2000005];
inline void dfs(int x){
	if(x>20) return;
	if(bit[x-1]!=0){
		q[0]=0;
		Rep(i,x-1,2) q[++q[0]]=bit[i];
		For(i,1,x-1) q[++q[0]]=bit[i];
		p[++tot]=0;
		For(i,1,q[0]) p[tot]=p[tot]<<1|q[i];
		q[0]=0;p[++tot]=0;
		Rep(i,x-1,1) q[++q[0]]=bit[i];
		For(i,1,x-1) q[++q[0]]=bit[i];
		For(i,1,q[0]) p[tot]=p[tot]<<1|q[i];
	}
	bit[x]=0;dfs(x+1);bit[x]=0;
	bit[x]=1;dfs(x+1);bit[x]=0;
}
int main(){
	dfs(1);p[++tot]=0;
	sort(p+1,p+1+tot);
	return 0;
}

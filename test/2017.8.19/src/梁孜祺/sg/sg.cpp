#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define base 97
#define inf 1e9
#define N 10001
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int a[N][N],b[N][N];
int q[20];
inline int pd(int x){
	q[0]=0;
	for(;x;x>>=1) q[++q[0]]=x&1;
	For(i,1,q[0]>>1) if(q[i]!=q[q[0]-i+1]) return 0;
	return 1;
}
inline void pre(){
	int f=1;
	For(i,0,1000) For(j,0,1000) a[i][j]=i^j;
	For(i,0,1000) For(j,0,1000){
		if(i) b[i][j]+=b[i-1][j];
		if(j) b[i][j]+=b[i][j-1];
		if(i&&j) b[i][j]-=b[i-1][j-1];
		b[i][j]+=pd(a[i][j]);
	}
}
int main(){
	freopen("sg.in","r",stdin);
	freopen("sg.out","w",stdout);
	pre();
	int T=read();
	while(T--){
		int x=read(),y=read(),xx=read(),yy=read();
		writeln(b[xx][yy]-b[xx][y-1]-b[x-1][yy]+b[x-1][y-1]);
	}
	return 0;
}

#include<stdio.h>
#include<vector>
#include<map>
#include<iostream>
#include<queue>
#include<algorithm>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,k;
vector <ll> a;
map <ll,int> mp;
queue <int> Q;
signed main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();k=read();
	while(n--){
		ll x=read();
		if(!mp[x]){
			if(!Q.empty()) mp[x]=Q.front(),a[mp[x]-1]=x,Q.pop();
			else a.push_back(x),mp[x]=a.size();
		}
		else Q.push(mp[x]),mp.erase(x);
	}
	sort(a.begin(),a.end());
	int p=a.size()-1;
	For(i,0,p) if(mp[a[i]]) mp[a[i]]=0,writeln(a[i]);
	return 0;
}
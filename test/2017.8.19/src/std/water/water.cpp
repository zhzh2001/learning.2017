#include<cstdio>
#include<bitset>
#define ll long long
#define maxn 5001010
#define maxk 2001
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
const ll p[]={0,4999871,4999879,4999889,4999913,4999933,4999949,4999957,4999961,4999963,3};
bitset<maxn> cnt[11];
bool vis[11];
ll q[maxk],answ[11][maxk],tot,Cnt,n,k,x;
void exgcd(ll a,ll b,ll &x,ll &y){
	if (!b){	x=1;	y=0;	return;	}
	exgcd(b,a%b,y,x);	y-=a/b*x;
}
inline ll get(ll a,ll m,ll b,ll n){
	ll x,y,lcm;
	exgcd(m,n,x,y);	lcm=n*m;
	return ((x*(b-a)*m+a)%lcm+lcm)%lcm;
}
inline bool check(ll x){
	For(i,1,10)	if (vis[i]&&!cnt[i][x%p[i]])	return 0;
	return 1;
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();	k=read();
	For(i,1,n){
		x=read();
		For(i,1,10)	cnt[i][x%p[i]]=cnt[i][x%p[i]]^1;
	}
	For(i,1,10){
		tot=0;
		For(j,0,p[i]-1)	if (cnt[i][j])	q[++tot]=j;
		if (tot==k){
			++Cnt;
			For(j,1,k)	answ[Cnt][j]=q[j];
			answ[Cnt][0]=p[i];
			vis[i]=1;
		}
	}
	tot=0;
//	writeln(Cnt);
	For(i,1,k)	For(j,1,k){
		x=get(answ[1][i],answ[1][0],answ[2][j],answ[2][0]);
		if (x>1e10)	continue;
		if (check(x)){
			q[++tot]=x;
			if (tot==k){
				For(x,1,tot-1)	For(y,x+1,tot)	if (q[x]>q[y])	q[x]^=q[y]^=q[x]^=q[y];
				For(i,1,tot)	printf("%lld\n",q[i]);
				return 0;
			}
		}
	}writeln(tot);
}

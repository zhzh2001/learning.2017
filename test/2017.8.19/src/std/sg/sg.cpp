#include <cstdio>
#include <memory.h>
#include <algorithm>
#define ll long long
#define For(i, j, k) for (ll i = j; i <= k; ++i)
#define FOr(i, j, k) for (ll i = j; i >= k; --i)
using namespace std;
inline ll read()
{
    ll x = 0, f = 1;
    char ch = getchar();
    while (ch < '0' || ch > '9')
    {
        if (ch == '-')
            f = -1;
        ch = getchar();
    }
    while (ch >= '0' && ch <= '9')
    {
        x = x * 10 + ch - '0';
        ch = getchar();
    }
    return x * f;
}
inline void wrote(ll x)
{
    if (x < 0)
        putchar('-'), x = -x;
    if (x >= 10)
        wrote(x / 10);
    putchar(x % 10 + '0');
}
inline void writeln(ll x)
{
    wrote(x);
    puts("");
}
ll f[100][2][2][2][2], bin[100], a[100], b[100], L, R, jzq[100][2][2];
inline bool pd(ll flag, ll has, ll cho) { return has == cho ? flag : has < cho; }
inline void ppt(ll &ans, bool flg1, bool flg2, bool flg3, bool flg4, ll sum)
{
    if ((!flg1 && flg3) || (!flg2 && flg4))
        return;
    ans += sum;
}
inline ll dp(ll len)
{
    memset(f, 0, sizeof f);
    memset(jzq, 0, sizeof jzq);
    jzq[51][0][0] = 1;
    FOr(i, 50, len + 1) For(myy, 0, 1) For(lzz, 0, 1) For(j, 0, (myy | a[i])) For(k, 0, (lzz | b[i])) if (!(j ^ k)) jzq[i][(j < a[i]) | myy][(k < b[i]) | lzz] += jzq[i + 1][myy][lzz];
    ll ans = 0;
    For(myy, 0, 1) For(lzz, 0, 1)
        For(i, 0, (a[len] | myy)) For(j, 0, (b[len] | lzz)) For(k, 0, 1) For(l, 0, 1) if (((i ^ j) == (k ^ l)) && (i ^ j == 1)) f[0][(a[len] > i) | myy][(b[len] > j) | lzz][a[0] < k][b[0] < l] += jzq[len + 1][myy][lzz];
    ll x = 1, y = len - 1, now = 0, zyy, szb;
    while (x < y)
    {
        ++now;
        For(flg1, 0, 1) For(flg2, 0, 1) For(flg3, 0, 1) For(flg4, 0, 1) For(i, 0, (flg1 | a[y])) For(j, 0, (flg2 | b[y])) For(k, 0, 1) For(l, 0, 1) if (i ^ j == k ^ l) f[now][flg1 | (a[y] > i)][flg2 | (b[y] > j)][pd(flg3, a[x], k)][pd(flg4, b[x], l)] += f[now - 1][flg1][flg2][flg3][flg4];
        ++x;
        --y;
    }
    if (x == y)
        For(u1, 0, 1) For(u2, 0, 1) For(u3, 0, 1) For(u4, 0, 1) For(i, 0, 1) For(j, 0, 1) ppt(ans, u1, u2, pd(u3, a[x], i), pd(u4, b[x], j), f[now][u1][u2][u3][u4]);
    else
    {
        --x;
        ++y;
        For(u1, 0, 1) For(u2, 0, 1) For(u3, 0, 1) For(u4, 0, 1) ppt(ans, u1, u2, u3, u4, f[now][u1][u2][u3][u4]);
    }
    return ans;
}
inline ll calc(ll x, ll y)
{
    if (x < 0 || y < 0)
        return 0;
    if (x > y)
        swap(x, y);
    ll ans = 0;
    if (x == y)
        ans = x & 1 ? (x + 1) * 2 : x * 2 + 1;
    else
        ans = (x + 1) * 2;
    For(i, 0, 50) a[i] = (x & bin[i]) > 0, b[i] = (y & bin[i]) > 0;
    For(i, 1, 50) ans += dp(i); //5
    return ans;
}
int main()
{
    freopen("sg.in", "r", stdin);
    freopen("sg.out", "w", stdout);
    bin[0] = 1;
    For(i, 1, 60) bin[i] = bin[i - 1] << 1;
    ll T = read(), x1, y1, x2, y2;
    while (T--)
    {
        x1 = read(), y1 = read(), x2 = read(), y2 = read();
        writeln(calc(x2, y2) + calc(x1 - 1, y1 - 1) - calc(x1 - 1, y2) - calc(x2, y1 - 1));
    }
}
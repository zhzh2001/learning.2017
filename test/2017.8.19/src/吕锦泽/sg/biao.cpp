#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,f[N][N],a[N*2],bin[30],sum[N][N];
ll get(ll x){
	ll num=0;
	rep(i,0,20) if (x&bin[i]) num+=pow(10,i);
	return num;
}
ll ok(ll x){
	ll num=x,len=0;
	do{
		a[++len]=num%10;
		num/=10;
	}while (num);
	rep(i,1,len/2) if (a[i]!=a[len-i+1]) return 0;
	return 1;
}
int main(){
	freopen("huiwen.out","w",stdout);
	n=read();
	bin[0]=1; rep(i,1,20) bin[i]=bin[i-1]<<1;
	rep(i,1,n) f[i][0]=f[0][i]=i;
	rep(i,1,n) rep(j,1,n){
		memset(a,0,sizeof a);
		rep(k,1,i-1) a[f[k][j]]++;
		rep(k,1,j-1) a[f[i][k]]++;
		ll t=0;
		while (a[t]) ++t;
		f[i][j]=t;
	}
	rep(i,1,n) rep(j,1,n){
		ll t=ok(get(f[i][j]));
		sum[i][j]=sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+t;
	}
/*	rep(i,1,n){
		rep(j,1,n){
			ll t=f[i][j];
		//	t=get(t);
		//	t=ok(t);
			printf("%3d",t);
		/*	putchar(' ');
			if (t) putchar('1');
			else putchar(' ');
		}
		puts("");
	}*/
/*	rep(i,1,n){
		rep(j,1,n) printf("%4d",sum[i][j]);
		puts("");
	}*/
	ll tot=0;
	rep(i,0,100000) if (ok(get(i))) printf("%d,",i),++tot;
	puts(""); printf("%d",tot);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 1005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll bin[50],T,x1,y1,x2,y2;
ll a[64]={0,1,3,5,7,9,15,17,21,27,31,33,45,51,63,65,73,85,93,99,107,119,127,129,153,165,189,195,219,231,255,257,273,297,313,325,341,365,381,387,403,427,443,455,471,495,511,513,561,585,633,645,693,717,765,771,819,843,891,903,951,975,1023};
ll get(ll x,ll y,ll num){
	if (num<0) return 0;
	ll tot=0,t=min(x,y),cnt=0;
	while (t>=bin[tot]) ++tot;
	if (num>=bin[tot]) return 0;
	if (x==y&&x==num&&num==bin[tot]-1) return bin[tot];
	--tot;
	cnt=get(bin[tot]-1,bin[tot]-1,num-bin[tot])+get(x-bin[tot],y-bin[tot],num-bin[tot]);
	cnt+=get(x-bin[tot],bin[tot]-1,num-bin[tot])+get(bin[tot]-1,y-bin[tot],num-bin[tot]);
	return cnt;
}
int main(){
	bin[0]=1; rep(i,1,18) bin[i]=bin[i-1]<<1;
	T=read();
	while (T--){
		ll ans=0;
		x1=read(); y1=read(); x2=read(); y2=read();
		rep(i,1,63){
			ans+=get(x2,y2,a[i])-get(x2,y1-1,a[i])-get(x1-1,y2,a[i])+get(x1-1,y1-1,a[i]);
		}
		printf("%lld",ans);
	}
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 35
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,k,x,ans1,a[N],bin[N];
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	bin[0]=1; rep(i,1,34) bin[i]=bin[i-1]<<1;
	n=read(); k=read();
	rep(i,1,n){
		x=read(); ans1^=x;
		rep(j,0,34) if (bin[j]&x) a[j]^=x;
	}
	if (k==1) return printf("%d",ans1)&0;
	if (k==2){
		x=34; while (!a[x]) --x;
		printf("%lld\n%lld",min(a[x],ans1^a[x]),max(a[x],ans1^a[x]));
		return 0;
	}
	return 233;
}

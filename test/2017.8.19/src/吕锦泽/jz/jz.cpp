#include<cstdio>
#include<cstring>
#include<bitset>
#include<algorithm>
#define ll long long
#define N 10001
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,x=1,y=1,ans;
bitset<N> b[N];
int main(){
	freopen("jz.in","r",stdin);
	freopen("jz.out","w",stdout);
	n=read(); m=read(); b[x][y]=1;
	rep(i,1,m){
		ll opt=read(),k=read();
		if (opt==1) { per(i,x,x-k) b[i][y]=1; x-=k; }
		if (opt==2) { per(i,y,y-k) b[x][i]=1; y-=k; }
		if (opt==3) { rep(i,y,y+k) b[x][i]=1; y+=k; }
		if (opt==4) { rep(i,x,x+k) b[i][y]=1; x+=k; }
	}
	rep(i,1,n) ans+=b[i].count();
	printf("%lld",ans);
}

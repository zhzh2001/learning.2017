#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
//ifstream fin(.in);
//ofstream fout(.out);
int xx,l,r;
inline bool pd(int x){
	if(x==0){
		return 1;
	}
	for(xx=14;xx>=0;xx--){
		if(x&(1<<xx)){
			break;
		}
	}
	l=0,r=xx;
	while(r>l){
		if(x&(1<<r)&&(x&(1<<l))||(!((x&(1<<r))&&!(x&(1<<l))))){
			l++,r--;
		}else{
			break;
		}
	}
	return (l>=r);
}
int f[8005][8005];
long long x1,x2,yy,y2;
int main(){
	for(int i=1;i<=8001;i++){
		for(int j=1;j<=8001;j++){
			f[i][j]=pd((i-1)^(j-1));
		}
	}
	for(int i=1;i<=8001;i++){
		for(int j=1;j<=8001;j++){
			f[i][j]+=(f[i-1][j]+f[i][j-1]-f[i-1][j-1]);
		}
	}
	int n;
	cin>>n;
	while(n--){
		cin>>x1>>yy>>x2>>y2;
		if(x1==x2&&yy==y2){
			cout<<(x1^yy)<<endl;
		}
		cout<<(f[x2+1][y2+1]-f[x1][y2+1]-f[x2+1][yy]+f[x1][yy])<<endl;
	}
	return 0;
}
/*

in:
1
1 1 4 4

out:
12

*/

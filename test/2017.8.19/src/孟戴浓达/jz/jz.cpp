#include<fstream>
#include<algorithm>
#include<map>
using namespace std;
ifstream fin("jz.in");
ofstream fout("jz.out");
int fx[13]={0,-1,0,0,1};
int fy[13]={0,0,-1,1,0};
map<pair<int,int>,bool>mp;
int n,q,xx=1,yy=1,ans=1;
int main(){
	fin>>n>>q;
	while(q--){
		int x,y;
		fin>>x>>y;
		xx+=fx[x]*y,yy+=fy[x]*y;
		if(xx>=1&&xx<=n&&yy>=1&&yy<=n){
			if(mp[make_pair(xx,yy)]==false){
				mp[make_pair(xx,yy)]=true;
				ans++;
			}
		}else{
			xx-=fx[x]*y,yy-=fy[x]*y;
		}
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
2 1
3 1

out:
2

*/

#include <fstream>
using namespace std;
ifstream fin("sg.in");
ofstream fout("sg.out");
const int N = 10000;
bool bl[N * 2 + 5];
int sum[N + 5][N + 5];
bool dig[40];
bool ispalind2(int x)
{
	int p = 0;
	do
		dig[++p] = x & 1;
	while (x /= 2);
	for (int i = 1, j = p; i < j; i++, j--)
		if (dig[i] ^ dig[j])
			return false;
	return true;
}
int main()
{
	for (int i = 0; i <= 2 * N; i++)
		bl[i] = ispalind2(i);
	sum[0][0] = 1;
	for (int i = 1; i <= N; i++)
	{
		sum[i][0] = sum[i - 1][0] + bl[i];
		sum[0][i] = sum[0][i - 1] + bl[i];
	}
	for (int i = 1; i <= N; i++)
		for (int j = 1; j <= N; j++)
			sum[i][j] = sum[i][j - 1] + sum[i - 1][j] - sum[i - 1][j - 1] + bl[i ^ j];
	int t;
	fin >> t;
	while (t--)
	{
		long long x1, y1, x2, y2;
		fin >> x1 >> y1 >> x2 >> y2;
		if (x2 > N || y2 > N)
		{
			fout << 0 << endl;
			continue;
		}
		int ans = sum[x2][y2];
		if (x1)
			ans -= sum[x1 - 1][y2];
		if (y1)
			ans -= sum[x2][y1 - 1];
		if (x1 && y1)
			ans += sum[x1 - 1][y1 - 1];
		fout << ans << endl;
	}
	return 0;
}
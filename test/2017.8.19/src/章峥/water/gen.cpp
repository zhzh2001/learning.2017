#include <fstream>
#include <random>
#include <ctime>
#include <set>
using namespace std;
ofstream fout("water.in");
const int n = 3e6, k = 500;
const long long v = 1e10;
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << ' ' << k << endl;
	set<long long> S;
	for (int i = 1; i <= k; i++)
	{
		uniform_int_distribution<long long> dv(0, v);
		long long x;
		do
			x = dv(gen);
		while (S.find(x) != S.end());
		S.insert(x);
		fout << x << endl;
	}
	for (int i = 1; i * 2 <= n - k; i++)
	{
		uniform_int_distribution<long long> dv(0, v);
		long long x;
		do
			x = dv(gen);
		while (S.find(x) != S.end());
		S.insert(x);
		fout << x << endl
			 << x << endl;
	}
	return 0;
}
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int SZ = 1e5, B = 80;
char *buf, *p, *pend;
FILE *fin, *fout, *f[B];
template <typename T>
inline void read(T &x)
{
	int remain = pend - p;
	if (remain < 20)
	{
		memcpy(buf, p, remain);
		pend = buf + remain + fread(buf + remain, 1, SZ - remain, fin);
		p = buf;
	}
	for (; *p < '0' || *p > '9'; p++)
		;
	x = 0;
	for (; *p >= '0' && *p <= '9'; p++)
		x = x * 10 + *p - '0';
}
int dig[15];
inline void writeln(long long x)
{
	int p = 0;
	do
		dig[++p] = x % 10;
	while (x /= 10);
	for (; p; p--)
		fputc(dig[p] + '0', fout);
	fputc('\n', fout);
}
long long pred;
int cnt = 0;
struct node
{
	FILE *f;
	size_t sz;
	long long *buf, *p, *end;
	bool operator<(const node &rhs) const
	{
		return *p > *(rhs.p);
	}
	bool pop()
	{
		if (*p > pred)
		{
			if (cnt & 1)
				writeln(pred);
			pred = *p;
			cnt = 0;
		}
		cnt++;
		if (++p == end)
		{
			end = buf + fread(buf, sizeof(long long), sz, f);
			p = buf;
			if (end == buf)
				return false;
		}
		return true;
	}
} h[B];
int main()
{
	fin = fopen("water.in", "r");
	fout = fopen("water.out", "w");
	buf = new char[SZ];
	p = pend = buf + SZ;
	int n, k;
	read(n);
	read(k);
	int b = n / SZ;
	if (n % SZ)
		b++;
	long long *a = new long long[SZ];
	for (int i = 1, j = 1; i <= b; i++)
	{
		int k;
		for (k = 0; k < SZ && j <= n; j++, k++)
			read(a[k]);
		sort(a, a + k);
		f[i] = tmpfile();
		fwrite(a, sizeof(long long), k, f[i]);
	}
	delete[] buf;
	delete[] a;
	for (int i = 1; i <= b; i++)
	{
		rewind(f[i]);
		h[i].f = f[i];
		h[i].sz = SZ / b;
		h[i].p = h[i].buf = new long long[h[i].sz];
		h[i].end = h[i].buf + fread(h[i].buf, sizeof(long long), h[i].sz, h[i].f);
	}
	make_heap(h + 1, h + b + 1);
	pred = -1;
	int cc = b;
	while (cc)
	{
		pop_heap(h + 1, h + cc + 1);
		if (h[cc].pop())
			push_heap(h + 1, h + cc + 1);
		else
			cc--;
	}
	if (cnt & 1)
		writeln(pred);
	for (int i = 1; i <= b; i++)
	{
		delete[] h[i].buf;
		fclose(h[i].f);
	}
	fclose(fin);
	fclose(fout);
	return 0;
}
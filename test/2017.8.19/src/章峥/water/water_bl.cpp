#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.ans");
const int N = 3000005;
long long a[N];
int main()
{
	int n, k;
	fin >> n >> k;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	sort(a + 1, a + n + 1);
	a[0] = -1;
	int cnt = 0;
	for (int i = 1; i <= n; i++)
	{
		if (a[i] != a[i - 1])
		{
			if (cnt & 1)
				fout << a[i - 1] << endl;
			cnt = 0;
		}
		cnt++;
	}
	return 0;
}
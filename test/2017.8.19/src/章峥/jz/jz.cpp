#include <fstream>
using namespace std;
ifstream fin("jz.in");
ofstream fout("jz.out");
const int N = 10005, dx[] = {-1, 0, 0, 1}, dy[] = {0, -1, 1, 0};
bool vis[N][N];
int main()
{
	int n, q;
	fin >> n >> q;
	vis[1][1] = true;
	int ans = 1, x = 1, y = 1;
	while (q--)
	{
		int opt, step;
		fin >> opt >> step;
		while (step--)
		{
			x += dx[opt - 1];
			y += dy[opt - 1];
			if (!vis[x][y])
			{
				ans++;
				vis[x][y] = true;
			}
		}
	}
	fout << ans << endl;
	return 0;
}
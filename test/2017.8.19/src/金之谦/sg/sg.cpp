#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int f[10010][10010],bin[10010],qx[210],qy[210],hx[210],hy[210],mx,my;
inline int check(int x){
	int p[20],cnt=0;
	while(x){p[++cnt]=x&1;x/=2;}
	for(int i=1;i<=(cnt+1)/2;i++)if(p[i]!=p[cnt-i+1])return 0;
	return 1;
}
bool b[20010];
signed main()
{
	freopen("sg.in","r",stdin);
	freopen("sg.out","w",stdout);
	for(int i=1;i<=10000;i++)f[i+1][1]=f[1][i+1]=i;
	for(int i=1;i<=20;i++){
		int p=(1<<i),q=(1<<(i-1));
		while(q<=10000)bin[q]=p-1,q+=p;
	}
	int T=read(),x=0,y=0;
	for(int i=1;i<=T;i++){
		qx[i]=read()+1,qy[i]=read()+1,hx[i]=read()+1,hy[i]=read()+1;
		mx=max(mx,hx[i]);my=max(my,hy[i]);
	}
	for(int i=1;i<mx;i++){
		x^=bin[i];y=0;
		for(int j=1;j<my;j++){
			y^=bin[j];
			f[i+1][j+1]=x^y;
		}
	}
	for(int i=1;i<=mx;i++)
		for(int j=1;j<=my;j++){
			f[i][j]=check(f[i][j]);
			f[i][j]+=f[i-1][j]+f[i][j-1]-f[i-1][j-1];
		}
	for(int i=1;i<=T;i++){
		int ans=f[hx[i]][hy[i]]-f[hx[i]][qy[i]-1]-f[qx[i]-1][hy[i]]+f[qx[i]-1][qy[i]-1];
		printf("%lld\n",ans);
	}
	return 0;
}
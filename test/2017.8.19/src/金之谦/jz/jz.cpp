#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
bool b[10010][10010];
int ans=0,n,m;
const int dx[5]={0,-1,0,0,1};
const int dy[5]={0,0,-1,1,0};
int main()
{
	freopen("jz.in","r",stdin);
	freopen("jz.out","w",stdout);
	n=read();m=read();b[1][1]=1;
	int x=1,y=1;ans=1;
	for(int i=1;i<=m;i++){
		int p=read(),q=read();
		for(int j=1;j<=q;j++){
			x+=dx[p],y+=dy[p];
			if(!b[x][y])b[x][y]=1,ans++;
		}
	}
	printf("%d",ans);
	return 0;
}

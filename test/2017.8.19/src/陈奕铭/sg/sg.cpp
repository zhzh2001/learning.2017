#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1; ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

ll mp[1005][1005];
ll num[60],top;
ll n;
ll flag;
ll x1[205],x2[205],y1[205],y2[205];

bool check(ll a){
	top = 0;
	while(a){
		++top;
		if(a&1) num[top] = 1;
		else num[top] = 0;
		a >>= 1;
	}
	ll l = 1,r = top;
	while(l < r){
		if(num[l] != num[r])
			return false;
		l++; r--;
	}
	return true;
}

int main(){
	freopen("sg.in","r",stdin);
	freopen("sg.out","w",stdout);
	n = 1000;
	ll t = read();
	mp[0][0] = 1;
	for(ll i = 1;i <= n;++i)
		mp[i][0] = mp[0][i] = check(i) + mp[0][i-1];
	for(ll i = 1;i <= n;++i)
		for(ll j = 1;j <= n;++j)
			mp[i][j] = mp[i-1][j]+mp[i][j-1]-mp[i-1][j-1]+check(i^j);
	for(ll i = 1;i <= t;++i){
		x1[i] = read(); y1[i] = read(); x2[i] = read(); y2[i] = read();
		if(x1[i] > x2[i]) swap(x1[i],x2[i]);
		if(y1[i] > y2[i]) swap(y1[i],y2[i]);
//		printf("%d %d %d %d\n",x1[i],y1[i],x2[i],y2[i]);
		if(x1[i] == x2[i] && y1[i] == y2[i]) flag = 1;
		else flag = 2;
	}
	
	if(flag == 1){
		for(ll i = 1;i <= t;i++)
			printf("%lld\n",check(x1[i]^y1[i]));
	}
	else{
		for(ll i = 1;i <= t;i++){
			printf("%lld\n",mp[x2[i]][y2[i]]-mp[x1[i]-1][y2[i]]-mp[x2[i]][y1[i]-1]+mp[x1[i]-1][y1[i]-1]);		}
	}
	return 0;
}

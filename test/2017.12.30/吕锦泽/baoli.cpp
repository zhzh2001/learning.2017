#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 105
#define inf (1ll<<50)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll f[N][N],n,q,A,B,ans,v[N],c[N];
int main(){
	freopen("misaka.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read(); q=read();
	rep(i,1,n) v[i]=read();
	rep(i,1,n) c[i]=read();
	while (q--){
		A=read(); B=read(); ans=0;
		memset(f,-64,sizeof f);
		f[0][0]=0;
		rep(i,1,n){
			ll tmp=-inf;
			rep(j,0,n) f[i][j]=max(f[i][j],f[i-1][j]);
			rep(j,0,n){
				if (c[i]==j) f[i][c[i]]=max(f[i][c[i]],f[i-1][j]+v[i]*A);
				else f[i][c[i]]=max(f[i][c[i]],f[i-1][j]+v[i]*B);
			}
		}
		rep(i,0,n) ans=max(ans,f[n][i]);
		printf("%lld\n",ans);
	}
}

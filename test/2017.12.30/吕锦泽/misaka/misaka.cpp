#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1ll<<50)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,q,A,B,v[N],c[N],f[N],c1,c2;
int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n=read(); q=read();
	rep(i,1,n) v[i]=read();
	rep(i,1,n) c[i]=read();
	rep(T,1,q){
		A=read(); B=read(); c1=0; c2=-1;
		rep(i,1,n) f[c[i]]=-inf;
		rep(i,1,n){
			f[c[i]]=max(f[c[i]],f[c[i]]+A*v[i]);
			if (c1!=c[i]) f[c[i]]=max(f[c[i]],f[c1]+B*v[i]);
			else f[c[i]]=max(f[c[i]],f[c2]+B*v[i]);
			if (c2!=-1) { if (f[c1]<f[c2]) swap(c1,c2); }
			if (f[c[i]]>f[c1]) c2=c1,c1=c[i];
			else if (c1!=c[i]&&(f[c[i]]>f[c2]||c2==-1)) c2=c[i];
		}
		printf("%lld\n",f[c1]);
	}
}

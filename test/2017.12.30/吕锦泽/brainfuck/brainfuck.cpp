#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N],a[N]; ll n,m,cnt,q[N],top,num,flag[N],pre[N];
struct data{ ll ty,num,len; }b[N];
void pre_print(ll num){
	rep(i,1,num<<1) putchar(' ');
}
void judge(ll top){
	if (q[top]==0&&top){
		q[top]=1;
		b[++cnt]=(data){5,1,top-1};
	}
}
int main(){
	freopen("brainfuck.in","r",stdin);
//	freopen("brainfuck.out","w",stdout);
	scanf("%s",s+1); m=strlen(s+1);
	rep(i,1,m){
		char t=s[i];
		if (t!='<'&&t!='>'&&t!='+'&&t!='-'&&t!='.'&&t!=','&&t!='['&&t!=']') continue;
		a[++n]=t;
	}
	rep(i,1,n){
		if (a[i]!='['&&a[i]!=']') continue;
		if (a[i]=='[') ++num; else --num;
		if (num<0) return puts("Error!")&0;
	}
	if (num!=0) return puts("Error!")&0;
	top=0;
	rep(i,1,n){
		if (a[i]=='>'||a[i]=='<'){
			ll tmp=0;
			if (a[i]=='>') ++tmp; else --tmp;
			while (a[i+1]=='>'||a[i+1]=='<'){
				++i; if (a[i]=='>') ++tmp; else --tmp;
			}
			if (tmp!=0){
				judge(top);
				b[++cnt]=(data){1,tmp,top};
			}
		}else
		if (a[i]=='+'||a[i]=='-'){
			ll tmp=0;
			if (a[i]=='+') ++tmp; else --tmp;
			while (a[i+1]=='+'||a[i+1]=='-'){
				++i; if (a[i]=='+') ++tmp; else --tmp;
			}
			if (tmp!=0){
				judge(top);
				b[++cnt]=(data){2,tmp,top};
			}
		}else
		if (a[i]=='.'){
			judge(top);
			b[++cnt]=(data){3,0,top};
		}else
		if (a[i]==','){
			judge(top);
			b[++cnt]=(data){4,0,top};
		}else
		if (a[i]=='[') q[++top]=0; else
		if (a[i]==']'){
			if (q[top]){
				b[++cnt]=(data){5,-1,top-1};
			}
			q[top--]=0;
		}
	}
	rep(i,1,cnt) flag[i]=1;
	rep(i,1,cnt){
		pre[i+1]=i;
		if (b[pre[i]].ty==b[i].ty&&b[i].ty==5&&b[i].num==-1&&b[pre[i]].num==1){
			pre[i+1]=pre[pre[i]]; flag[i]=flag[pre[i]]=0;
		}else
		if (b[i].ty==b[pre[i]].ty&&(b[i].ty==1||b[i].ty==2)){
			b[pre[i]].num+=b[i].num;
			flag[i]=0;
			if (!b[pre[i]].num) pre[i+1]=pre[pre[i]],flag[pre[i]]=0;
			else pre[i+1]=pre[i];
		}
	}
	ll now=0;
	rep(i,1,cnt) if (flag[i]){
		if (b[i].ty==5&&b[i].num==-1) --now;
		pre_print(now);
		if (b[i].ty==5&&b[i].num==1) ++now;
		if (b[i].ty==1){
			if (b[i].num<0) printf("p -= %d;\n",-b[i].num);
			else printf("p += %d;\n",b[i].num);
		}else if (b[i].ty==2){
			if (b[i].num<0) printf("*p -= %d;\n",-b[i].num);
			else printf("*p += %d;\n",b[i].num);
		}
		else if (b[i].ty==3){
			puts("putchar(*p);");
		}else if (b[i].ty==4){
			puts("*p = getchar();");
		}else if (b[i].ty==5){
			if (b[i].num==1) puts("if (*p) do {");
			else puts("} while (*p);");
		}
	}
}

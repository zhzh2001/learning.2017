#include<cstdio>
#include<iostream>
#include<cstring>
#include<stack>
using namespace std;
char bu[11005];
int j;
//int cc,pp;
//int c[11005],p[11005];
int sp[11005];
stack<int> s;
int findp(int &j)
{
	int a=0;
	if (bu[j]=='+')
	{
		bu[j]='0';a=1;j++;sp[j]=sp[j-1];a+=findp(j);
	}else if (bu[j]=='-')
	{
		bu[j]='0';a=-1;j++;sp[j]=sp[j-1];a+=findp(j);
	}
	if (bu[j]=='0')
	{
		bu[j]='0';j++;sp[j]=sp[j-1];a+=findp(j);
	}
	return a;
}
int findc(int &j)
{
	int a=0;
	if (bu[j]=='>')
	{
		bu[j]='0';a=1;j++;sp[j]=sp[j-1];a+=findc(j);
	}else if (bu[j]=='<')
	{
		bu[j]='0';a=-1;j++;sp[j]=sp[j-1];a+=findc(j);
	}
	if (bu[j]=='0')
	{
		bu[j]='0';j++;sp[j]=sp[j-1];a+=findc(j);
	}
	return a;
}
int main()
{
	freopen("brainfuck.in","r",stdin);freopen("brainfuck.out","w",stdout);
	scanf("%s",bu);
	int n=strlen(bu);
	for (int i=0;i<n;++i)
	{
		if (bu[i]!='+'&&bu[i]!='-'&&bu[i]!='<'&&bu[i]!='>'&&bu[i]!=','&&bu[i]!='.'&&bu[i]!='['&&bu[i]!=']') bu[i]='0';
		if (bu[i]=='[') s.push(i);else if (bu[i]==']')
		{
			if (i==s.top()+1) bu[i]='0',bu[i-1]='0';
			s.pop();
		}
	}
	if (!s.empty())
	{
		printf("Error!");return 0;
	}
	memset(sp,0,sizeof(sp));
	for (j=0;j<n;)
	{
		if (bu[j]!='0') for (int i=0;i<sp[j];++i) printf(" ");
		int a;
		switch(bu[j])
		{
			case '+':a=findp(j);if (a>0) printf("*p+=%d;\n",a);else if (a<0) printf("*p-=%d;\n",-a);sp[j+1]=sp[j];break;
			case '-':a=findp(j);if (a>0) printf("*p+=%d;\n",a);else if (a<0) printf("*p-=%d;\n",-a);sp[j+1]=sp[j];break;
			case '<':a=findc(j);if (a>0) printf("p+=%d;\n",a);else if (a<0) printf("p-=%d;\n",-a);sp[j+1]=sp[j];break;
			case '>':a=findc(j);if (a>0) printf("p+=%d;\n",a);else if (a<0) printf("p-=%d;\n",-a);sp[j+1]=sp[j];break;
			case '.':printf("putchar(*p);\n");sp[j+1]=sp[j];j++;break;
			case ',':printf("*p=getchar();\n");sp[j+1]=sp[j];j++;break;
			case '[':printf("if (*p) do {\n");sp[j+1]=sp[j]+2;j++;break;
			case ']':printf("} while (*p);\n");sp[j+1]=sp[j];j++;break;
			default:sp[j+1]=sp[j];j++;break;
		}// printf("%d\n",j);
		if (bu[j]==']') sp[j]=sp[j-1]-2;
	}
	//for (int i=0;i<n;++i) cout<<sp[i]<<' ';cout<<endl;
	fclose(stdin);fclose(stdout);
	return 0;
}

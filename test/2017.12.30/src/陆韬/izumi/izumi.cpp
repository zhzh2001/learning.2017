#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int f[10005];
bool b[10005];
bool a[10005][10005];
int n,d,sum=0;
void dfs(int x,int maxx,int minn)
{
	if (maxx-minn>d) return;
	for (int i=x+1;i<=n;++i) if (a[x][i]&&!b[i])
	{
		b[i]=1;
		int t=max(maxx,f[i]),s=min(minn,f[i]);
		if (t-s<=d) sum++,dfs(i,t,s);
		b[i]=0;
	}
}
int main()
{
	freopen("izumi.in","r",stdin);freopen("izumi.out","w",stdout);
	scanf("%d%d",&d,&n);
	memset(a,0,sizeof(a));
	for (int i=1;i<=n;++i) scanf("%d",&f[i]);
	for (int i=1;i<n;++i)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		a[x][y]=a[y][x]=1;
		a[0][i]=a[i][0]=1;
	}
	a[0][n]=a[n][0]=1;
	dfs(0,0,1<<15);
	printf("%d\n",sum);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N]; ll n,m,cnt,tmp;
struct data{ char ch; ll num; }a[N];
void pre_print(ll num) { rep(i,1,num<<1) putchar(' '); }
int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	scanf("%s",s+1); m=strlen(s+1);
	rep(i,1,m){
		char t=s[i];
		if (n&&!a[n].num) --n;
		if (t!='<'&&t!='>'&&t!='+'&&t!='-'&&t!='.'&&t!=','&&t!='['&&t!=']') continue;
		if (t=='>'){
			if (a[n].ch=='>') ++a[n].num;
			else if (a[n].ch=='<') --a[n].num; 
			else a[++n]=(data){'>',1};
		}else
		if (t=='<'){
			if (a[n].ch=='<') ++a[n].num;
			else if (a[n].ch=='>') --a[n].num;
			else a[++n]=(data){'<',1};
		}
		if (t=='+'){
			if (a[n].ch=='+') ++a[n].num;
			else if (a[n].ch=='-') --a[n].num; 
			else a[++n]=(data){'+',1};
		}else
		if (t=='-'){
			if (a[n].ch=='-') ++a[n].num;
			else if (a[n].ch=='+') --a[n].num;
			else a[++n]=(data){'-',1};
		}else
		if (t=='.') a[++n]=(data){'.',1}; else
		if (t==',') a[++n]=(data){',',1}; else
		if (t=='[') a[++n]=(data){'[',1},++cnt; else
		if (t==']'){
			if (a[n].ch=='[') --n;
			else a[++n]=(data){']',1};
			--cnt;
		}
		if (cnt<0) return puts("Error!")&0;
	}
	if (cnt) return puts("Error!")&0;
	rep(i,1,n){
		if (a[i].ch=='>'&&a[i].num<0) a[i].ch=='<',a[i].num=-a[i].num;
		if (a[i].ch=='<'&&a[i].num<0) a[i].ch=='>',a[i].num=-a[i].num;
		if (a[i].ch=='+'&&a[i].num<0) a[i].ch=='-',a[i].num=-a[i].num;
		if (a[i].ch=='-'&&a[i].num<0) a[i].ch=='+',a[i].num=-a[i].num;
	}
	rep(i,1,n){
		if (a[i].ch==']') --tmp,pre_print(tmp),puts("} while (*p);");
		else {
			pre_print(tmp);
			if (a[i].ch=='>') printf("p += %d;\n",a[i].num);
			else if (a[i].ch=='<') printf("p -= %d;\n",a[i].num);
			else if (a[i].ch=='+') printf("*p += %d;\n",a[i].num);
			else if (a[i].ch=='-') printf("*p -= %d;\n",a[i].num);
			else if (a[i].ch=='.') puts("putchar(*p);");
			else if (a[i].ch==',') puts("*p = getchar();");
			else if (a[i].ch=='[') puts("if (*p) do {"),++tmp;
		}
	}
}

//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#define max(a,b) (a>b)?a:b
using namespace std;
ifstream fin("misaka.in");
ofstream fout("misaka.out");
long long v[100003],c[100003];
long long n,q,A,B;
long long mx[800003];
long long las[100003],f[100003],ans;
void build(int rt,int l,int r){
	if(l==r){
		mx[rt]=0;return;
	}
	int mid=(l+r)>>1;
	build(rt<<1,l,mid),build(rt<<1|1,mid+1,r);
	mx[rt]=max(mx[rt<<1],mx[rt<<1|1]);
}
int query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y)return mx[rt];
	int mid=(l+r)>>1;
	if(y<=mid) return query(rt<<1,l,mid,x,y);
	else if(x>mid) return query(rt<<1|1,mid+1,r,x,y);
	else return max(query(rt<<1,l,mid,x,mid),query(rt<<1|1,mid+1,r,mid+1,y));
}
void update(int rt,int l,int r,int x,int y){
	if(l==r){
		mx[rt]=y;
		return;
	}
	int mid=(l+r)>>1;
	if(x<=mid) update(rt<<1,l,mid,x,y);
	else update(rt<<1|1,mid+1,r,x,y); 
	mx[rt]=max(mx[rt<<1],mx[rt<<1|1]);
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=n;i++) fin>>v[i];
	for(int i=1;i<=n;i++) fin>>c[i];
	long long mx1,mx2,mxx1,mxx2;
	while(q--){
		fin>>A>>B;
		mx1=mx2=mxx1=mxx2=0;
		mx1=-1999999999999999,mx2=-1999999999999999;
		for(int i=1;i<=n;i++) las[i]=0,f[i]=-1999999999999999;
		f[0]=-1999999999999999;
		ans=0;
		for(int i=1;i<=n;i++){
			f[i]=B*v[i];
			if(c[i]!=mxx1&&mxx1!=0) f[i]=max(f[i],mx1+v[i]*B);
			if(c[i]!=mxx2&&mxx2!=0) f[i]=max(f[i],mx2+v[i]*B);
			f[i]=max(f[i],f[las[c[i]]]+A*v[i]);
			if(f[i]>mx1){
				if(c[i]!=mxx1) mx2=mx1,mxx2=mxx1,mx1=f[i],mxx1=c[i];
				else mx1=f[i],mxx1=c[i];
			}else if(f[i]>mx2){
				if(c[i]!=mxx1) mx2=f[i],mxx2=c[i];
			}
			las[c[i]]=i;
			ans=max(ans,f[i]);
		}
		fout<<ans<<endl;
		/*
		for(int i=1;i<=n;i++) las[i]=0,f[i]=0;
		ans=0;
		build(1,1,n);
		for(int i=1;i<=n;i++){
			if(las[c[i]]!=0) f[i]=max(f[i],f[las[c[i]]]+A*v[i]);
			if(c[i]-1>=1) f[i]=max(f[i],v[i]*B+query(1,1,n,1,c[i]-1));
			if(c[i]+1<=n) f[i]=max(f[i],v[i]*B+query(1,1,n,c[i]+1,n));
			las[c[i]]=i;
			ans=max(ans,f[i]);
			update(1,1,n,c[i],f[i]);
		}
		cout<<ans<<endl;
		*/
	}
	return 0;
}
/*

in:
6 3
1 -2 3 4 0 -1
1 2 1 2 1 1
5 1
-2 1
1 0

out:
20
9
4

*/

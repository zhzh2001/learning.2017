//#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<fstream>
#include<math.h>
using namespace std;
ifstream fin("izumi.in");
ofstream fout("izumi.out");
const long long mod=1000000007;
long long head[10003],nxt[20003],to[20003],tot;
inline void add(int a,int b){
	to[++tot]=b,nxt[tot]=head[a],head[a]=tot;
}
long long val[10003],dp[10003];
long long n,d,now,x,y;
int vis[10003];
void dfs(int x,int fa){
	if(dp[x]!=0){
		return;
	}
    dp[x]=1;
	for(int i=head[x];i;i=nxt[i]){
        if(to[i]!=fa){
			if(val[to[i]]<val[now]&&val[now]-val[to[i]]<=d){
				dfs(to[i],x);
			}else if(val[to[i]]==val[now]&&to[i]>now){
				dfs(to[i],x);
			}
			dp[x]=(dp[x]*(dp[to[i]]+1))%mod;
		}
	}
}
void dfs1(int x,int fa){
    dp[x]=1;
	for(int i=head[x];i;i=nxt[i]){
        if(to[i]!=fa){
        	dp[to[i]]=0;
			if(val[to[i]]<val[now]&&val[now]-val[to[i]]<=d){
				dfs1(to[i],x);
			}else if(val[to[i]]==val[now]&&to[i]>now){
				dfs1(to[i],x);
			}
			dp[x]=(dp[x]*(dp[to[i]]+1))%mod;
		}
	}
}
int hei[2003][10003];
int main(){
	fin>>d>>n;
	for(int i=1;i<=n;i++){
		fin>>val[i];
		hei[val[i]][++hei[val[i]][0]]=i;
	}
	for(int i=1;i<=n-1;i++){
		fin>>x>>y;
		add(x,y),add(y,x);
	}
	long long ans=0;
	if(n<=10000){
		for(int i=1;i<=n;i++){
			now=i;
			dfs1(i,i);
			ans=(ans+dp[i])%mod;
		}
	}else{
		for(int i=1;i<=2000;i++){
			if(hei[i][0]!=0){
				memset(dp,0,sizeof(dp));
			}
			for(int j=hei[i][0];j>=1;j--){
				now=hei[i][j];
				dfs(now,now);
				ans=(ans+dp[now])%mod;
			}
		}
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
1 4
2 1 3 2
1 2
1 3
3 4
out:
8

in:
4 8
7 8 7 5 4 6 4 10
1 6
1 2
5 8
1 3
3 5
6 7
3 4
out:
41

*/

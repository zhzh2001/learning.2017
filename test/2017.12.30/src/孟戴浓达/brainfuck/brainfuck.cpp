//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<stack>
using namespace std;
ifstream fin("brainfuck.in");
ofstream fout("brainfuck.out");
string ch[11003];
char s[11003];
char ss[11003];
int now,top,zuo,len;
string zhuan(int x){
	string em;
	while(x>0){
		em=em+(char)((x%10)+'0');
		x/=10;
	}
	return em;
}
stack<char>st;
int main(){
	fin>>s+1;
	len=strlen(s+1);
	for(int i=1;i<=len;i++){
		if(st.empty()){
			st.push(s[i]);
			continue;
		}
		char ch=st.top();
		bool yes=false;
		if(s[i]=='+'&&ch=='-'){
			yes=true,st.pop();
		}else if(s[i]=='-'&&ch=='+'){
			yes=true,st.pop();
		}else if(ch=='['&&s[i]==']'){
			yes=true,st.pop();
		}else if(s[i]=='<'&&ch=='>'){
			yes=true,st.pop();
		}else if(s[i]=='>'&&ch=='<'){
			yes=true,st.pop();
		}
		if(!yes&&(s[i]=='>'||s[i]=='<'||s[i]=='+'||s[i]=='-'||s[i]=='['||s[i]==']'||s[i]==','||s[i]=='.')){
			st.push(s[i]);
		}
	}
	int len=0;
	while(!st.empty()){
		s[++len]=st.top();
		st.pop();
	}
	int l=1,r=len;
	while(l<r){
		swap(s[l],s[r]);
		l++,r--;
	}
	for(int i=1;i<=len;i++){
		if(s[i]=='['){
			zuo++;
			if(s[i+1]!=']'&&i<len){
				ch[++top]="if (*p) do {";
			}
		}else if(s[i]==']'){
			if(zuo==0){
				fout<<"Error!"<<endl;
				return 0;
			}else{
				zuo--;
			}
			if(i>1&&s[i-1]!='['){
				ch[++top]="} while (*p);";
			}
		}else if(s[i]=='.'){
			ch[++top]="putchar(*p);";
		}else if(s[i]==','){
			ch[++top]="*p = getchar();";
		}else if(s[i]=='+'||s[i]=='-'){
			int now=i,zong=0;
			while(now<=len&&(s[now]=='+'||s[now]=='-')){
				zong+=(s[now]=='+'),zong-=(s[now]=='-');
				now++;
			}
			if(zong>0){
				ch[++top]="*p += "+zhuan(zong)+";";
			}else if(zong<0){
				ch[++top]="*p -= "+zhuan(-zong)+";";
			}
			i=now-1;
		}else if(s[i]=='<'||s[i]=='>'){
			int now=i,zong=0;
			while(now<=len&&(s[now]=='>'||s[now]=='<')){
				zong+=(s[now]=='>'),zong-=(s[now]=='<');
				now++;
			}
			if(zong>0){
				ch[++top]="p += "+zhuan(zong)+";";
			}else if(zong<0){
				ch[++top]="p -= "+zhuan(-zong)+";";
			}
			i=now-1;
		}
	}
	if(zuo!=0){
		fout<<"Error!"<<endl;
		return 0;
	}
	int x=0;
	for(int i=1;i<=top;i++){
		if(ch[i]=="} while (*p);"){
			x--;
		}
		for(int j=1;j<=x;j++){
			fout<<"  ";
		}
		fout<<ch[i]<<endl;
		if(ch[i]=="if (*p) do {"){
			x++;
		}
	}
	return 0;
}
/*

in:
[1989.6.4]
out:
if (*p) do {
  putchar(*p);
  putchar(*p);
} while (*p);

in:
++[<>]--]

in:
++<<>[]--,
out:
*p += 2;
p -= 1;
*p -= 2;
*p = getchar();

*/

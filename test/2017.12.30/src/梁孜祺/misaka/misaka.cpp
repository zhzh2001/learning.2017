#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 100005
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,Q,v[N],c[N];
ll f[N];
inline void solve(){
	ll mx1=0,mx2=0,X=0,Y=0;
	memset(f,-63,sizeof f);
	int A=read(),B=read();
	For(i,1,n){
		int val=v[i],col=c[i];
		ll p1=A*val+f[col],p2;
		if(mx1==X) p2=mx2+B*val;
		else p2=mx1+B*val;
		f[col]=max(f[col],max(p1,p2));
		if(f[col]>=mx1){
			if(X!=col) Y=X,mx2=mx1;
			mx1=f[col];X=col;
		}else if(f[col]>mx2&&X!=col){
			mx2=f[col];Y=col;
		}
	}
	cout<<mx1<<endl;
}
int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n=read();Q=read();
	For(i,1,n) v[i]=read();
	For(i,1,n) c[i]=read();
	For(i,1,Q) solve();
	return 0;
}

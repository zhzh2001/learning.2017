#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 11005
using namespace std;
char s[N];
int n,m,r[N],q[N];
inline bool judge(char x){
	if(x=='>'||x=='<'||x=='+'||x=='-'||x=='.'||x==','||x=='['||x==']') return 1;
	return 0;
}
inline void doit(int x){For(i,1,x) putchar(' '),putchar(' ');}
inline void dfs(int L,int R,int dep){
	For(i,L+1,R){
		if(s[i]=='>'||s[i]=='<'){
			int tot=0,j;
			for(j=i;j<=n;j++){
				if(s[j]=='>') tot++;
				else if(s[j]=='<') tot--;
				else break;i=j;
			}i=j-1;
			if(tot!=0) doit(dep);
			if(tot>0) printf("p += %d;\n",tot);
			if(tot<0) printf("p -= %d;\n",-tot);
		}
		if(s[i]=='+'||s[i]=='-'){
			int tot=0,j;
			for(j=i;j<=n;j++){
				if(s[j]=='+') tot++;
				else if(s[j]=='-') tot--;
				else break;i=j;
			}i=j-1;
			if(tot!=0) doit(dep);
			if(tot>0) printf("*p += %d;\n",tot);
			if(tot<0) printf("*p -= %d;\n",-tot);
		}
		if(s[i]=='.') doit(dep),puts("putchar(*p);");
		if(s[i]==',') doit(dep),puts("*p = getchar();");
		if(s[i]=='['){
			doit(dep);puts("if (*p) do {");
			dfs(i,r[i],dep+1);i=r[i];
			doit(dep);puts("} while (*p);");
		}
	}
}
inline void solve(){
	Rep(i,n,1){
		if(s[i]==']') q[++q[0]]=i;
		if(s[i]=='[') r[i]=q[q[0]--];
	}
	For(i,1,n){
		if(s[i]=='>'||s[i]=='<'){
			int tot=0,j;
			for(j=i;j<=n;j++){
				if(s[j]=='>') tot++;
				else if(s[j]=='<') tot--;
				else break;
			}i=j-1;
			if(tot>0) printf("p += %d;\n",tot);
			if(tot<0) printf("p -= %d;\n",-tot);
		}
		if(s[i]=='+'||s[i]=='-'){
			int tot=0,j;
			for(j=i;j<=n;j++){
				if(s[j]=='+') tot++;
				else if(s[j]=='-') tot--;
				else break;
			}i=j-1;
			if(tot>0) printf("*p += %d;\n",tot);
			if(tot<0) printf("*p -= %d;\n",-tot);
		}
		if(s[i]=='.') puts("putchar(*p);");
		if(s[i]==',') puts("*p = getchar();");
		if(s[i]=='['){
			puts("if (*p) do {");
			dfs(i,r[i],1),i=r[i];
			puts("} while (*p);");
		}
	}
}
int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	scanf("%s",s+1);n=strlen(s+1);
	int tot=0;For(i,1,n) if(judge(s[i])) s[++tot]=s[i];n=tot;
	tot=0;For(i,1,n){
		if(s[i]=='[') tot++;
		if(s[i]==']'){
			tot--;
			if(tot<0){puts("Error!");return 0;}
		}
	}
	if(tot>0){puts("Error!");return 0;}
	int j=1;For(i,2,n){
		if(s[j]=='['&&s[i]==']') s[i]=s[j]='0',j--;
		else j=i;if(j==0) j=i;
	}
	tot=0;
	For(i,1,n) if(s[i]!='0') s[++tot]=s[i];n=tot;
	solve();
	return 0;
}

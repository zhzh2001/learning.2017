#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 10005
#define mod 1000000007
using namespace std;
const int oo=1e9;
int d,n,head[N],cnt,v[N],m,in[N];
struct edge{int next,to;}e[N<<1];
inline void insert(int u,int V){
	e[++cnt]=(edge){head[u],V};head[u]=cnt;in[u]++;
	e[++cnt]=(edge){head[V],u};head[V]=cnt;in[V]++;
}
int q;
inline void Dfs(int x,int fa,int p){
	q|=(1<<(x-1));
	for(int i=head[x];i;i=e[i].next) 
		if(e[i].to!=fa&&(1<<(e[i].to-1))&p){
			Dfs(e[i].to,x,p);
		}
}
inline bool judge(int p){
	int mx=-oo,mn=oo;q=0;
	For(i,1,n) if(p&(1<<(i-1))) mx=max(mx,v[i]),mn=min(mn,v[i]);
	if(mx-mn>d) return 0;
	For(i,1,n) if(p&(1<<(i-1))){Dfs(i,0,p);break;}
	if(p==q) return 1;
	else return 0;
}
int ans;
inline void dfs(int x,int p){
	if(x>n){
		if(!p) return;
		if(judge(p)) ans++;
		return;
	}
	dfs(x+1,p);p|=1<<(x-1);dfs(x+1,p);
}
struct T{
	int ind,root,pos[N],val[N];
	int mx[N<<2],mn[N<<2],ans;
	inline void dfs(int x,int fa){
		pos[x]=++ind;
		for(int i=head[x];i;i=e[i].next)
			if(e[i].to!=fa) dfs(e[i].to,x);
	}
	inline void pushup(int k){
		mx[k]=max(mx[k<<1],mx[k<<1|1]);
		mn[k]=min(mn[k<<1],mn[k<<1|1]);
	}
	inline void build(int k,int l,int r){
		if(l==r){mx[k]=mn[k]=val[l];return;}
		int mid=(l+r)>>1;
		build(k<<1,l,mid);build(k<<1|1,mid+1,r);
		pushup(k);
	}
	inline int query_mx(int k,int l,int r,int x,int y){
		if(l==x&&r==y) return mx[k];
		int mid=(l+r)>>1;
		if(y<=mid) return query_mx(k<<1,l,mid,x,y);
		else if(x>mid) return query_mx(k<<1|1,mid+1,r,x,y);
		else return max(query_mx(k<<1,l,mid,x,mid),query_mx(k<<1|1,mid+1,r,mid+1,y));
	}
	inline int query_mn(int k,int l,int r,int x,int y){
		if(l==x&&r==y) return mn[k];
		int mid=(l+r)>>1;
		if(y<=mid) return query_mn(k<<1,l,mid,x,y);
		else if(x>mid) return query_mn(k<<1|1,mid+1,r,x,y);
		else return min(query_mn(k<<1,l,mid,x,mid),query_mn(k<<1|1,mid+1,r,mid+1,y));
	}
	inline void solve(){
		For(i,1,n) if(in[i]==1) root=i;
		dfs(root,0);
		For(i,1,n) val[pos[i]]=v[i];
		build(1,1,n);
		For(i,1,n) For(j,i,n){
			if(query_mx(1,1,n,i,j)-query_mn(1,1,n,i,j)<=d) ans++;
		}
		printf("%d\n",ans);
	}
}FFF;
int main(){
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	scanf("%d%d",&d,&n);
	For(i,1,n) scanf("%d",&v[i]),m=max(m,v[i]);
	For(i,2,n){
		int x,y;scanf("%d%d",&x,&y);
		insert(x,y);
	}
	if(n<=20) dfs(1,0),printf("%d\n",ans);
	else FFF.solve();
	return 0;
}

#include<bits/stdc++.h>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline LL read() {
    LL x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline void write(LL x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(LL x) {
	write(x); putchar('\n');
}

const int N = 10011,inf = 1e8; 
LL f[N],ans;
int n,Q;
int val[N],color[N];  
int W[3]; 

int main()
{
	freopen("misaka.in","r",stdin); 
	freopen("misaka.out","w",stdout); 
	n = read(); Q = read(); 
	For(i, 1, n) val[i] = read(); 
	For(i, 1, n) color[i] = read(); 
	color[0] = -4565467; 
	while(Q--){ 
		W[1] = read(); W[0] = read(); 
		For(i, 1, n) f[i] = -inf;  
		For(i, 1, n) 
			For(j, 0, i-1)
	 			f[i] = max(f[i], f[j]+val[i]*W[color[i]==color[j]] ); 
		ans = 0; 
		For(i, 1, n) 
			if(ans < f[i]) ans = f[i]; 
		printf("%d\n",ans); 
	}
	return 0;
}









#include<bits/stdc++.h>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline LL read() {
    LL x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline void write(LL x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(LL x) {
	write(x); putchar('\n');
}

const int N = 2011, inf = 1e9, mod = 1e9+7; 
int n,D,len,cnt,ans,Wx,Wn;
int x[N],y[N],head[N],point[N],b[N];   
int f[N],arr[N],val[N],T[30][30],fa[N],ff[N];  
struct edge{
	int to,pre; 
}e[N*2];
bool flag;
inline void add(int x,int y) {
	e[++cnt].to = y; 
	e[cnt].pre = head[x]; 
	head[x] = cnt; 
}
void dfs(int u,int fa,int dep) {
	arr[dep] = val[u]; ff[u] = fa; 
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(v != fa) dfs(v, u, dep+1); 
	}
}

void does(int u) {
	if(val[u] > Wx) Wx = val[u]; 
	if(val[u] < Wn) Wn = val[u]; 
	if(Wx-Wn>D) return; 
	ans++; if(ans==mod) ans = 0; 
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(v != ff[u]) does(v); 
	} 
}

inline void lian() {
	For(i, 1, n-1) {
		add(x[i],y[i]); 
		add(y[i],x[i]); 
	}
	dfs(1,-1,1); 
	For(i, 1, n) {
		Wx = -inf; Wn = inf; 
		does(arr[i]);  
	}
}
int find(int x) {
	if(fa[x]==x) return x;
	return fa[x] = find(fa[x]); 
}
inline void Union(int x,int y) {
	x = find(x); y = find(y); 
	if(x==y) return; 
	fa[x] = y;
}

inline void work() {
	int Mn = inf, Mx = -inf; 
	For(i, 1, n) 
		if(b[i]) {
			if(val[i] > Mx) Mx = val[i]; 
			if(val[i] < Mn) Mn = val[i]; 
		}
	if(Mx-Mn>D) return; 
	For(i, 1, n) fa[i] = i; 
	For(i, 1, n-1) 
		if(b[x[i]] && b[y[i]]) 
			Union(x[i],y[i]); 
	For(i, 1, n) 
		if(b[i]) fa[i] = find(fa[i]); 
	int ww = -1; 
	For(i, 1, n) 
		if(b[i]) {
			if(ww==-1) ww=fa[i];
			if(ww!=fa[i]) return;  
		}
	++ans;  
}

int main()
{
	freopen("izumi.in","r",stdin); 
	freopen("izumi.out","w",stdout); 
	D = read(); n = read(); 
	For(i, 1, n) val[i] = read(); 
	For(i, 1, n-1) {
		x[i]=read(); y[i] = read(); 
		if(x[i] > y[i]) {
			int t = x[i]; 
			x[i] = y[i]; 
			y[i] = t; 
		}
		++f[x[i]]; 
	}
	For(i, 1, n) if(f[i]>1) {
		flag = 1;
		break; 
	}
	if(!flag) {
		lian(); 
		printf("%d\n",ans); 
		return 0;
	}
	
	For(i, 1, (1<<n)-1) {
		int x = i; 
		len = 0; 
		For(j, 1, n+1) b[j] = 0; 
		while(x) {
			b[++len] = x&1; 
			x/=2; 
		}
		work(); 
	}
	ans%=mod; 
	printf("%d\n",ans); 
	return 0;
}


/*

4 8
7 8 7 5 4 6 4 10
1 6
1 2
5 8
1 3
3 5
6 7
3 4


1 3
1 2 3
1 2
3 2


*/






#include<bits/stdc++.h>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline LL read() {
    LL x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline void write(LL x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(LL x) {
	write(x); putchar('\n');
}

const int N = 11011; 
char ch[N],s[N]; 

int l1,l2,num,sum,spccc; 
int f[300],vis[N]; 

inline void kong(int x) {
	For(i, 1, x) putchar(' '); 
}

int main()
{
	freopen("brainfuck.in","r",stdin); 
	freopen("brainfuck.out","w",stdout); 
	gets(ch+1); 
	l1 = strlen(ch+1); 
	f['>']=1; f['<']=1; f['+']=1; f['-']=1; f['.']=1; f[',']=1; f['[']=1; 
	f[']']=1;
	int pos;
	for(pos=1; pos<=l1; pos++) {
		if(!f[ch[pos]]) continue; 
		 ch[++l2] = ch[pos]; 
	} 
	l1 = l2; l2 = 0;
	ch[++l1] = '3'; 
	For(i, 1, l1+1) {
		
		if(ch[i]==']' && num>0) {
				--num; 
				sum+=2; 
		}
		else {
			if(sum>0) {
				Dow(j, i-1, i-sum) vis[j] = 1;  
				num = 0; sum = 0;
			}
			if(ch[i]!='['&&ch[i]!=']') num = 0,sum = 0;
			if(ch[i]=='[') {
				++num; 
			}
		}
	} 
	For(i, 1, l1-1) {
		if(vis[i]) continue; 
 		s[++l2] = ch[i]; 
	}
	sum = 0;
	For(i, 1, l2) {
		if(s[i]=='[') sum++;
		else if(s[i]==']') sum--; 
	}
	if(sum!=0) {
		puts("Error!"); 
		return 0;
	}
	
	For(i, 1, l2+10) vis[i] = 0; 
	for(pos = 1; pos<=l2; pos++) {
		if(s[pos]=='<'||s[pos]=='>') {
			int ans = 0,sum = 0;
			while(s[pos]=='<'||s[pos]=='>') {
				sum++;
				if(s[pos]=='>') ++ans;
				else --ans; 
				++pos; 
			}
			if(ans==0) 
				Dow(i, pos-1, pos-sum) vis[i] = 1; 
			--pos;
		}
	}
	int l3 = 0; 
	For(i, 1, l2) 
		if(!vis[i]) s[++l3] = s[i]; 
	l2 = l3;  
	for(pos = 1; pos<=l2; pos++) {
		if(s[pos]=='+'||s[pos]=='-') {
			int ans = 0;
			while(s[pos]=='+'||s[pos]=='-') {
				if(s[pos]=='+') ++ans;
				else --ans; 
				++pos;
			}
			if(ans!=0) {
				kong(spccc); 
				printf("*p ");
				if(ans<0) printf("-= "),ans=-ans; 
				else printf("+= "); 
				printf("%d;\n",ans);  
			}
			--pos;  
		} 
		if(s[pos]=='<'||s[pos]=='>') {
			int ans = 0;
			while(s[pos]=='<'||s[pos]=='>') {
				if(s[pos]=='>') ++ans;
				else --ans; 
				++pos; 
			}
			if(ans!=0) {
				kong(spccc); 
				printf("p ");
				if(ans<0) printf("-= "),ans=-ans; 
				else printf("+= "); 
				printf("%d;\n",ans);  
			} 
			--pos;
		}
		if(s[pos]=='[') {
			kong(spccc); printf("if (*p) do {\n"); 
			spccc+=2; 
		}
		else if(s[pos]==']') {
			spccc-=2;
			kong(spccc); printf("} while (*p);\n"); 
		}
		else if(s[pos]=='.') {
			kong(spccc); printf("putchar(*p);\n"); 
		}
		else if(s[pos]==',') {
			kong(spccc); printf("*p = getchar();\n"); 
		}
		
		
	} 
//	printf("%s",s+1); 
	return 0;
}


/*

++--<<[[[]][[[[]]]]]]]


++<<>>--,.


*/






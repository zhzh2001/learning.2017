#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define pb push_back
#define mo 1000000007
using namespace std;
struct D{
	int x,z;
}a[10010]; 
vector<int> f[10010];
ll lzq,d,n,ans,x,y;
ll c[10010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool cmp(D a,D b)
{
	if(a.x<b.x)return true;
	return false;
}
ll cmo(ll &x,ll y)
{
	x*=y;
	x%=mo;
}
ll jmo(ll &x,ll y)
{
	x+=y;
	x%=mo;
}
ll dfs(int x,int jzq,int fa)
{
	ll zyy=1;
	For(i,0,f[x].size()-1)
	{
		if(f[x][i]==fa)continue;
		if(a[f[x][i]].x-a[jzq].x>d)continue;
		if(f[x][i]<jzq)continue;
		cmo(zyy,dfs(f[x][i],jzq,x));
	}
	zyy++;
	return zyy;
}
int main()
{
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	d=read();n=read();
	For(i,1,n)
	{
		a[i].x=read();
		a[i].z=i;
	}
	sort(a+1,a+n+1,cmp);
	For(i,1,n)c[a[i].z]=i;
	For(i,1,n-1)
	{
		x=read();y=read();
		f[c[x]].pb(c[x]);
		f[c[y]].pb(c[y]);
	}
	For(i,1,n)
	{
		lzq=dfs(i,i,-1)-1;
		jmo(ans,lzq);
	}
	cout<<ans<<endl;
	return 0;
}

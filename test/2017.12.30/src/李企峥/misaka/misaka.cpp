#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll f[100020],v[100020],c[100020],n,Q,a,b,ans,mx,mx1,mx2,lzq;
int main() 
{
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n=read();Q=read();
	For(i,1,n)v[i]=read();
	For(i,1,n)c[i]=read();
	while(Q--)
	{
		ans=0;
		a=read();b=read();
		//memset(f,-1000,sizeof(f));
		For(i,1,n)f[i]=-1e9;
		mx1=0;mx2=0;
		f[0]=0;
		For(i,1,n)
		{
			if(mx1==c[i])mx=mx2;else mx=mx1;
			lzq=max(f[mx]+b*v[i],f[c[i]]+a*v[i]);
			if(lzq>f[c[i]])f[c[i]]=lzq;
			if(lzq>f[mx2]&&c[i]!=mx1)mx2=c[i];
			if(f[mx1]<f[mx2])swap(mx1,mx2);
		}
		ans=f[mx1];
		cout<<ans<<endl;
	}
	return 0;
}

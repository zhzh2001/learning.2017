#include<cstdio>
#include<iostream>
using namespace std;
int n,q,x,y,sam[110000],dif[110000],col[110000],lon[110000];
long long f[110000][2],a[110000],b[110000];
int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)
		scanf("%lld",&a[i]);
	for(int i=1;i<=n;i++)
		scanf("%lld",&b[i]);
	sam[1]=0;
	dif[1]=0;
	col[b[1]]=1;
	lon[b[1]]=1;
	for(int i=2;i<=n;i++){
		sam[i]=col[b[i]];
		if(sam[i]==i-1)dif[i]=i-1-lon[b[i]];
		else	dif[i]=i-1;
		if(sam[i]==i-1){
			col[b[i]]++;
			lon[b[i]]++;
		}
		else{
			col[b[i]]=i;
			lon[b[i]]=1;
		}
	}
	f[0][0]=f[0][1]=0;
	for(int i=1;i<=q;i++){
		scanf("%lld%lld",&x,&y);
		f[1][1]=max(f[0][1],y*a[1]);
		f[1][0]=0;
		for(int j=2;j<=n;j++){
			f[j][1]=f[sam[j]][1];
			f[j][0]=max(f[dif[j]][1],f[sam[j]][0]);
			f[j][1]=max(f[j][1],y*a[j]);
			f[j][0]=max(f[j][0],y*a[j]);
			if(sam[j]>0){
				f[j][1]=max(f[j][1],f[sam[j]][1]+x*a[j]);
				f[j][1]=max(f[j][1],f[sam[j]][0]+y*a[j]);
			}
			if(dif[j]>0){
				f[j][1]=max(f[j][1],f[dif[j]][1]+y*a[j]);
			}
		}
		printf("%lld\n",max(max(f[n][0],f[n][1]),f[0][0]));
	}
	return 0;
}

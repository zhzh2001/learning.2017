#include<cstdio>
#include<iostream>
using namespace std;
int z[11100],next[11100],len,j,f,k,cnt,x,y;
char s[11100];
char c;
int check(char a,char b){
	if(a=='['&&b==']')return 1;
	if(a=='+'&&b=='-')return 1;
	if(a=='-'&&b=='+')return 1;
	if(a=='<'&&b=='>')return 1;
	if(a=='>'&&b=='<')return 1;
	return 0;
}
int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	while(scanf("%c",&c)!=EOF)
		if(c=='>'||c=='<'||c=='+'||c=='-'||c=='.'||c==','||c=='['||c==']')s[len++]=c;
	k=0;
	for(int i=0;i<len;i++)
		if(s[i]=='[')k++;
		else	if(s[i]==']'){
					k--;
					if(k<0){
						puts("Error!");
						return 0;
					}
				}
	if(k){
		puts("Error!");
		return 0;
	}
	for(int i=0;i<len;i++){
		next[i]=i+1;
		z[i]=-1;
	}
	int beg=0;
	for(int i=0;i<len;i++){
		x=i;
		y=next[i];
		while(x>=beg&&y<len&&check(s[x],s[y])){
			x--;
			y++;
			if(x<0)break;
			if(z[x]>=0){
				x=z[x];
				next[x]=y;
				break;
			}
		}
		i=y-1;
		if(x<beg)beg=y;
		else{
			next[x]=y;
			if(y-1>x+1)z[y-1]=x;
		}
	}
	if(beg>=len)return 0;
	for(int i=beg;i<len;i=next[i]){
		if(i>=len)return 0;
		if(i>=len)break;
		if(s[i]=='.'){
			for(int j=1;j<=cnt;j++){
				putchar(' ');
				putchar(' ');
			}
			puts("putchar(*p);");
			continue;
		}
		if(s[i]==','){
			for(int j=1;j<=cnt;j++){
				putchar(' ');
				putchar(' ');
			}
			puts("*p = getchar();");
			continue;
		}
		if(s[i]=='['){
			for(int j=1;j<=cnt;j++){
				putchar(' ');
				putchar(' ');
			}
			puts("if (*p) do {");
			cnt++;
			continue;
		}
		if(s[i]==']'){
			cnt--;
			for(int j=1;j<=cnt;j++){
				putchar(' ');
				putchar(' ');
			}
			puts("} while (*p);");
			continue;
		}
		if(s[i]=='+'||s[i]=='-'){
			k=0;
			while(i<len&&(s[i]=='+'||s[i]=='-')){
				if(s[i]=='+')k++;
				else	k--;
				i=next[i];
			}
			for(int j=1;j<=cnt;j++){
				putchar(' ');
				putchar(' ');
			}
			if(k!=0){
				putchar('*');
				putchar('p');
				putchar(' ');
			}
			if(k>0)printf("+= %d;\n",k);
			if(k<0)printf("-= %d;\n",-k);
			i--;
			next[i]=i+1;
		}
		else{
			k=0;
			while(i<len&&(s[i]=='>'||s[i]=='<')){
				if(s[i]=='>')k++;
				else	k--;
				i=next[i];
			}
			for(int j=1;j<=cnt;j++){
				putchar(' ');
				putchar(' ');
			}
			if(k!=0){
				putchar('p');
				putchar(' ');
			}
			if(k>0)printf("+= %d;\n",k);
			if(k<0)printf("-= %d;\n",-k);
			i--;
			next[i]=i+1;
		}
	}
	return 0;
}

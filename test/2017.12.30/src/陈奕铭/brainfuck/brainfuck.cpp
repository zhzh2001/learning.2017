#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1; ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

const int N = 11005;
char s[N];
int len;
int Add;
int addp;
int l;
int Lis[N];
int to[N],last[N];

inline void suoji(){
	for(int j = 1;j <= l;++j){
		putchar(' ');
		putchar(' ');
	}
}
inline void PrintAdd(){
	if(Add > 0){
		suoji();
		printf("*p += %d\n",Add);
		Add = 0;
	}
	else if(Add < 0){
		suoji();
		printf("*p -= %d\n",-Add);
		Add = 0;
	}
}

inline void PrintAddp(){
	if(addp > 0){
		suoji();
		printf("p += %d\n",addp);
		addp = 0;
	}
	else if(addp < 0){
		suoji();
		printf("p -= %d\n",-addp);
		addp = 0;
	}
}

inline bool check(char a){
	if(a == '>' || a == '<' || a == '+' || a == '-' || a == '.' || a == ',' || a == '[' || a == ']') return true;
	return false;
}

int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	scanf("%s",s+1); len = strlen(s+1);
	for(int i = 1;i <= len;++i){
		if(s[i] == '[') Lis[i] = Lis[i-1]+1;
		else if(s[i] == ']') Lis[i] = Lis[i-1]-1;
		else Lis[i] = Lis[i-1];
	}
	if(Lis[len] != 0){
		puts("Error!");
		return 0;
	}
	int j = 0;
	for(int i = 1;i <= len;++i){
		if(!check(s[i])){
			to[j] = i;
			last[i] = j;
			continue;
		}
		if(s[i] == '['){
			to[j] = i;
			last[i] = j;
			j = i;
			continue;
		}
		if(s[i] == ']'){
			if(s[j] == '['){
				j = last[j];
				continue;
			}
			to[j] = i;
			last[i] = j;
			j = i;
			continue;
		}
		if(s[i] == '<'){
			if(s[j] == '>'){
				j = last[j];
				continue;
			}
			to[j] = i;
			last[i] = j;
			j = i;
			continue;
		}
		if(s[i] == '>'){
			if(s[j] == '<'){
				j = last[j];
				continue;
			}
			to[j] = i;
			last[i] = j;
			j = i;
			continue;
		}
		if(s[i] == '+'){
			if(s[j] == '-'){
				j = last[j];
				continue;
			}
			to[j] = i;
			last[i] = j;
			j = i;
			continue;
		}
		if(s[i] == '-'){
			if(s[j] == '+'){
				j = last[j];
				continue;
			}
			to[j] = i;
			last[i] = j;
			j = i;
		}
		to[j] = i;
		last[i] = j;
		j = i;
	}
	to[j] = len+1;

	for(int i = to[0];i <= len;i = to[i]){
		if(s[i] == ']'){
			PrintAdd();
			PrintAddp();
			--l;
			suoji();
			puts("} while (*p);");
			continue;
		}
		if(s[i] == '['){
			PrintAdd();
			PrintAddp();
			suoji();
			puts("if (*p) do {");
			++l;
			continue;
		}
		if(s[i] == '>'){
			PrintAdd();
			++addp;
			continue;
		}
		if(s[i] == '<'){
			PrintAdd();
			--addp;
			continue;
		}
		if(s[i] == '+'){
			PrintAddp();
			++Add;
			continue;
		}
		if(s[i] == '-'){
			PrintAddp();
			--Add;
			continue;
		}
		if(s[i] == '.'){
			PrintAdd();
			PrintAddp();
			suoji();
			puts("putchar(*p);");
			continue;
		}
		if(s[i] == ','){
			PrintAdd();
			PrintAddp();
			suoji();
			puts("*p = getchar();");
			continue;
		}
	}
	PrintAdd();
	PrintAddp();
	return 0;
}
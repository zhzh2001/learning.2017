#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define mk(a,b) make_pair(a,b)
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1; ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

const int N = 2005,M = 10005;
// int f[22][N][N];
map<pair<int,int>,ll> mp[M];
int n,d;
int val[M],size[M];
int head[M],to[M*2],nxt[M*2],cnt;
ll ans;
//lian
int rt;
int dfn[M],DFN,op[M];
int Max[M],Min[M],HMx,HMi,TMx,TMi,num,del;
// **
inline void insert(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
	to[++cnt] = a; nxt[cnt] = head[b]; head[b] = cnt;
}

// lian
void dfsnum(int x,int fa){
	dfn[x] = ++DFN;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa)
			dfsnum(to[i],x);
}

void dfs(int x,int fa){
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa)
			dfs(to[i],x);
	if(TMi > HMi && val[x] < val[Min[HMi]])
		while(TMx > HMx && (dfn[Max[HMx]] >= del || val[Max[HMx]]-val[x] > d)) del = min(del,dfn[Max[HMx++]]);
	if(TMx > HMx && val[x] > val[Max[HMx]])
		while(TMi > HMi && (dfn[Min[HMi]] >= del || val[x]-val[Min[HMi]] > d)) del = min(del,dfn[Min[HMi++]]);
	while(TMx > HMx && (dfn[Max[TMx-1]] >= del || val[x] >= val[Max[TMx-1]])) TMx--;
	while(TMi > HMi && (dfn[Min[TMi-1]] >= del || val[x] <= val[Min[TMi-1]])) TMi--;
	++num;
	Min[TMi++] = x;
	Max[TMx++] = x;
	ans += num-(n-del+1);
}
// **

void Dfs(int x,int fa){
	mp[x][mk(val[x],val[x])]++;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			int v = to[i];
			Dfs(v,x);
			map<pair<int,int>,ll> mp2;
			mp2 = mp[x];
			map<pair<int,int>,ll>:: iterator it = mp[x].begin();
			map<pair<int,int>,ll>:: iterator it2;
			for(;it != mp[x].end();++it){
				for(it2 = mp[v].begin();it2 != mp[v].end();++it2){
					int Low = min(it->first.first,it2->first.first);
					int Up = max(it->first.second,it2->first.second);
					if(Up-Low > d) continue;
					mp2[mk(Low,Up)] += it->second*it2->second;
					ans += it->second*it2->second;
				}
			}
			mp[x] = mp2;
		}
}

int main(){
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	d = read(); n = read();
	for(int i = 1;i <= n;++i) val[i] = read();
	for(int i = 1;i < n;++i){
		int a = read(),b = read();
		insert(a,b); ++size[a]; ++size[b];
	}
	int flag = 0;
	for(int i = 1;i <= n;++i)
		if(size[i] == 1) ++flag,rt = i;
	if(flag == 2){		//lian
		del = n+1;
		dfsnum(rt,rt);
		dfs(rt,rt);
		printf("%lld\n", ans);
	}
	else{
		Dfs(rt,rt);
		printf("%lld\n", ans);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1; ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

const ll INF = 0x3f3f3f3f3f3f3f3f;
const int N = 100005;
int col[N],val[N];
int A,B;
int Maxcal[N];
int MaxAll1,MaxAll2;
int n,Q;
ll f[N];

inline void Max(ll &x,ll y){
	if(x < y) x = y;
}

int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n = read(); Q = read();
	for(int i = 1;i <= n;++i) val[i] = read();
	for(int i = 1;i <= n;++i) col[i] = read();
	while(Q--){
		A = read(); B = read();
		memset(Maxcal,0,sizeof Maxcal);
		MaxAll1 = MaxAll2 = 0;
		for(int i = 1;i <= n;++i){
			f[i] = B*val[i];
			if(Maxcal[col[i]] != 0) Max(f[i],f[Maxcal[col[i]]]+1LL*A*val[i]);
			if(MaxAll1 != 0){
				if(col[MaxAll1] == col[i]){
					if(MaxAll2 != 0) Max(f[i],f[MaxAll2]+1LL*B*val[i]);
					if(f[i] > f[MaxAll1]) MaxAll1 = i;
				}
				else{
					Max(f[i],f[MaxAll1]+1LL*B*val[i]);
					if(f[i] >= f[MaxAll1]){
						MaxAll2 = MaxAll1;
						MaxAll1 = i;
					}
				}
			}
			else MaxAll1 = i;
			if(f[i] > f[Maxcal[col[i]]] || Maxcal[col[i]] == 0) Maxcal[col[i]] = i;
		}
		printf("%lld\n",max(f[MaxAll1],0LL));
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;

string s,ans;
int len,i,j,l,r,in,de;

int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	cin>>s;
	len=s.length();
	for (i=0;i<len;i++){
		if (s[i]=='[') l++;
		else if (s[i]==']') r++;
		if (r>l){
			cout<<"Error!"<<endl;;
			return 0;
		}
	}
	if (l!=r) {
		cout<<"Error!"<<endl;;
		return 0;
	}
	for (i=0;i<len;i++)
		if(s[i]=='['||s[i]==']'||s[i]=='.'||s[i]==','||s[i]=='+'||s[i]=='-'||s[i]=='<'||s[i]=='>'){
			ans+=s[i];
			if (ans[ans.length()-1]=='+'&ans[ans.length()-2]=='-') ans.erase(ans.length()-2,2);
			if (ans[ans.length()-1]=='-'&ans[ans.length()-2]=='+') ans.erase(ans.length()-2,2);
			if (ans[ans.length()-1]==']'&ans[ans.length()-2]=='[') ans.erase(ans.length()-2,2);
			if (ans[ans.length()-1]=='<'&ans[ans.length()-2]=='>') ans.erase(ans.length()-2,2);
			if (ans[ans.length()-1]=='>'&ans[ans.length()-2]=='<') ans.erase(ans.length()-2,2);
	}
	s=ans;
	len=ans.length();
	i=j=0;
	in=de=l=r=0;
	while (i<len){
		if (s[i]==']') {
			for (int k=1;k<=j-2;k++) printf(" ");
			cout<<"} while (*p);"<<endl;
			j-=2;
		}
		if (s[i]=='[') {
			for (int k=1;k<=j;k++) printf(" ");
			cout<<"if (*p) do {"<<endl;
			j+=2;
		}
		if (s[i]=='+'||s[i]=='-'||s[i]=='<'||s[i]=='>'||s[i]=='.'||s[i]==',') for (int k=1;k<=j;k++) printf(" ");
		if (s[i]=='.'){
			cout<<"putchar(*p);"<<endl;
		}
		if (s[i]==','){
			cout<<"*p = getchar();"<<endl;
		}
		while (s[i]=='+'){
			in++;
			i++;
		}
		if (in>0){
			cout<<"*p += "<<in<<endl;
			in=0;
			i--;
		}
		while (s[i]=='-'){
			de++;
			i++;
		}
		if (de>0){
			cout<<"*p -= "<<de<<endl;
			de=0;
			i--;
		}
		while (s[i]=='<'){
			l++;
			i++;
		}
		if (l>0){
			cout<<"p -= "<<l<<endl;
			l=0;
			i--;
		}
		while (s[i]=='>'){
			r++;
			i++;
		}
		if (r>0){
			cout<<"p += "<<r<<endl;
			r=0;
			i--;
		}
		i++;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
using namespace std;

int f[200000],n,ans,q,a[200000],c[200000],x,y;

int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) scanf("%d",&c[i]);
	while (q--){
		scanf("%d%d",&x,&y);
		ans=0;
		for (int i=1;i<=n;i++){
			f[i]=y*a[i];
			for (int j=1;j<i;j++)
				if (c[j]==c[i]) f[i]=max(f[i],f[j]+x*a[i]);
				else f[i]=max(f[i],f[j]+y*a[i]);
			ans=max(ans,f[i]);
		}
		printf("%d\n",ans);
	}
	return 0;
}

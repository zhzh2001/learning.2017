#include<cstdio>
#include<iostream>
using namespace std;

int a[10000],l[10000],r[10000],d,n,ans;
int p=1000000007;

int dfs(int k,int maxn,int minn){
	if (maxn-minn>d) return 0;
	int num=1;
	for (int i=1;i<n;i++)
		if (l[i]==k) {
			num=(num+dfs(r[i],max(maxn,a[r[i]]),min(minn,a[r[i]])))%p;
		}
	return num;
}
int main(){
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	scanf("%d%d",&d,&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<n;i++) scanf("%d%d",&l[i],&r[i]);
	for (int i=1;i<=n;i++){
		ans=(ans+dfs(i,a[i],a[i]))%p;
	}
	printf("%d",ans);
	return 0;
}

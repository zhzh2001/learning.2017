program brainfuck;
 var
  d:array[1..8] of char=('>','<','+','-','.',',','[',']');
  i,j,p,p1,upass,lpass,lens:longint;
  s,sp,space:ansistring;
 procedure hahaprint(var p:longint;k:longint);
  begin
   if p<>0 then
    begin
     write(space);
     if k=1 then write('*p ')
            else write('p ');
     if p<0 then write('-')
            else write('+');
     writeln('= ',abs(p),';');
     p:=0;
    end;
  end;
 begin
  assign(input,'brainfuck.in');
  assign(output,'brainfuck.out');
  reset(input);
  rewrite(output);
  readln(s);
  s:=s+']';
  p:=0;
  p1:=0;
  upass:=1;
  lpass:=0;
  lens:=length(s);
  space:='';
  sp:='';
  for i:=1 to lens do
   for j:=1 to 8 do
    if s[i]=d[j] then sp:=sp+s[i];
  lens:=length(sp);
  s:=sp;
  sp:='';
  i:=1;
  while i<=lens-2 do
   if (s[i]='[') and (S[i+1]=']') then
    begin
     delete(s,i,2);
     dec(lens,2);
     dec(i);
     if i<=0 then i:=1;
    end
                                  else inc(i);
  p:=0;
  for i:=1 to lens-1 do
   begin
    if s[i]='[' then inc(p);
    if s[i]=']' then dec(p);
    if p<0 then lpass:=1;
   end;
  if p<>0 then lpass:=1;
  p:=0;
  for i:=1 to lens do
   begin
    case s[i] of
     '.',',','[',']':
      begin
       hahaprint(p,0);
       hahaprint(p1,1);
      end;
    end;
    if lpass=1 then continue;
    case s[i] of
     '>','<':
      begin
       hahaprint(p1,1);
       if s[i]='>' then inc(p)
                   else dec(p);
      end;
     '+','-':
      begin
       hahaprint(p,0);
       if s[i]='+' then inc(p1)
                   else dec(p1);
      end;
     '.':writeln(space,'putchar(*p);');
     ',':writeln(space,'*p = getchar();');
     '[':
      begin
       inc(upass);
       writeln(space,'if (*p) do {');
       space:=space+'  ';
      end;
     ']':
      begin
       dec(upass);
       if i=lens then continue;
       if space<>'' then delete(space,1,2);
       writeln(space,'} while (*p);');
      end
    end;
   end;
  if lpass=1 then writeln('Error!');
  close(input);
  close(output);
 end.

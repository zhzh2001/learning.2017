#include<bits/stdc++.h>
using namespace std;
#define ll long long
char ch[200005];
int x[200005];
int tot,len,now,sp;
string s;
bool b;
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int main()
{
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	cin>>s;len=s.length();now=0;sp=0;
	for(int i=0;i<len;i++){
		if(x[now]==0&&now!=0)now--;
		if(s[i]=='>'){if(ch[now]=='<')x[now]--;else if(ch[now]=='>')x[now]++;else ch[++now]='>',x[now]=1;}
		else if(s[i]=='<'){if(ch[now]=='>')x[now]--;else if(ch[now]=='<')x[now]++;else ch[++now]='<',x[now]=1;}
		else if(s[i]=='+'){if(ch[now]=='-')x[now]--;else if(ch[now]=='+')x[now]++;else ch[++now]='+',x[now]=1;}
		else if(s[i]=='-'){if(ch[now]=='+')x[now]--;else if(ch[now]=='-')x[now]++;else ch[++now]='-',x[now]=1;}
		else if(s[i]=='.'){ch[++now]='.';x[now]=1;}
		else if(s[i]==','){ch[++now]=',';x[now]=1;}
		else if(s[i]=='['){ch[++now]='[';tot++;x[now]=1;}
		else if(s[i]==']'){if(ch[now]=='[')tot--,now--;else ch[++now]=']',tot--,x[now]=1;if(tot<0)b=true;}
	}if(tot!=0)b=true;if(b){puts("Error!");return 0;}
	for(int i=1;i<=now;i++){
		if(x[i]==0)continue;
		if(x[i]<0){x[i]=-x[i];if(ch[i]=='+')ch[i]='-';if(ch[i]=='-')ch[i]='+';if(ch[i]=='[')ch[i]=']';if(ch[i]==']')ch[i]='[';}
		if(ch[i]==']')sp-=2;
		for(int j=1;j<=sp;j++)putchar(' ');
		if(ch[i]=='>'){putchar('p');putchar(' ');putchar('+');putchar('=');putchar(' ');write(x[i]);puts(";");}
		else if(ch[i]=='<'){putchar('p');putchar(' ');putchar('-');putchar('=');putchar(' ');write(x[i]);puts(";");}
		else if(ch[i]=='+'){putchar('*');putchar('p');putchar(' ');putchar('+');putchar('=');putchar(' ');write(x[i]);puts(";");}
		else if(ch[i]=='-'){putchar('*');putchar('p');putchar(' ');putchar('-');putchar('=');putchar(' ');write(x[i]);puts(";");}
		else if(ch[i]=='.'){puts("putchar(*p);");}
		else if(ch[i]==','){puts("*p = getchar();");}
		else if(ch[i]=='['){puts("if (*p) do {");sp+=2;}
		else if(ch[i]==']'){puts("} while (*p);");}
	}return 0;
}

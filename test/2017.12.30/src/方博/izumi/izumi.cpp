#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=10005;
const int M=20005;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
struct edge{
	int from,to,tail,nxt;
}e[M];
const int mo=1e9+7;
int head[N];
int tot,n,m,x,y,d;
int a[N],b[N];
ll ans;
inline void addto(int x,int y){e[++tot]=(edge){x,y,a[y],head[x]};head[x]=tot;}
ll dfs(int k,int fa,int t)
{
	ll ret=1;
	for(int i=head[k];i;i=e[i].nxt){
		if(e[i].to==fa||e[i].tail<t||e[i].tail>t+d||(e[i].tail==t&&b[e[i].to]))continue;
		ret*=dfs(e[i].to,k,t);
		if(ret>1e9)ret%=mo;
	}
	return ret+1;
}
int main()
{
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	d=read();n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){x=read(),y=read();addto(x,y);addto(y,x);}
	for(int i=1;i<=n;i++){
		b[i]=true;
		ans+=dfs(i,0,a[i])-1;
		if(ans>1e18)ans%=mo;
	}
	writeln(ans%mo);
}

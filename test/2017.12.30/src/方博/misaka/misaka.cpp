#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
ll a[N];
int b[N];
int v[N],c[N];
int n,q;
ll ma1,ma2;
int col1,col2;
inline void solve(ll s,ll d)
{
	if(v[1]*d>0){col1=c[1];ma1=v[1]*d;col2=0;ma2=0;}
	else{col1=0;ma1=0;col2=c[1];ma2=v[1]*d;}
	b[c[1]]=q;a[c[1]]=v[1]*d;
	for(int i=2;i<=n;i++){
		ll tmp1,tmp2;
		if(b[c[i]]==q)tmp1=a[c[i]]+v[i]*s;
		else tmp1=(ll)(-1e16);
		if(c[i]!=col1)tmp2=ma1+v[i]*d;
		else tmp2=ma2+v[i]*d;
		tmp2=max(tmp2,v[i]*d);
//		cout<<i<<':'<<tmp1<<' '<<tmp2<<endl;
		tmp1=max(tmp1,tmp2);
		if(b[c[i]]==q)a[c[i]]=max(a[c[i]],tmp1);
		else b[c[i]]=q,a[c[i]]=tmp1;
		if(col1!=c[i]){if(tmp1>ma2){col2=c[i];ma2=tmp1;}}
		else{if(tmp1>ma1)ma1=tmp1;}
		if(ma2>ma1){swap(ma1,ma2);swap(col1,col2);}
//		cout<<ma1<<' '<<ma2<<endl;
	}writeln(ma1);
//	system("pause");
}
int main()
{
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)v[i]=read();
	for(int i=1;i<=n;i++)c[i]=read();
	for(;q;q--){ll s=read(),d=read();solve(s,d);}
	return 0;
}

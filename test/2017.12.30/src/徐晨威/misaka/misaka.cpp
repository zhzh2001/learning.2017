#include<cstdio>
#include<algorithm>
using namespace std;
int v[100005],c[100005],n,m;
long long f[100005],INF=-1e17;
int read(){
	int ret=0;char ch=getchar();bool f=1;
	for(;ch<'0'||'9'<ch;ch=getchar()) f^=!(ch^'-');
	for(;'0'<=ch&&ch<='9';ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return f?ret:-ret;
}
long long get(bool a,int co,int l,int r){//0 = 1 !=
	if(l==r) if(a^(co==c[l])) return f[l];else return INF;
	int mid=(r-l>>1)+l;
	return max(get(a,co,l,mid),get(a,co,mid+1,r));
}
int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++) v[i]=read();
	for(int i=1;i<=n;i++) c[i]=read();c[0]=-1;
	for(int k=1;k<=m;k++){
		int A=read(),B=read();long long ans=0;
		f[1]=v[1]*B;ans=max(f[1],(long long)0);
		for(int i=2;i<=n;i++)
		f[i]=max(get(0,c[i],0,i-1)+(long long)v[i]*A,get(1,c[i],0,i-1)+(long long)v[i]*B),ans=max(ans,f[i]);
		printf("%lld\n",ans);
	}
	return 0;
}

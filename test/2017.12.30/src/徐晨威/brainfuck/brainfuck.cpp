#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
string ch;
int len,t,kh,a[11005],b[11005];
bool vis[11005];
int stk[11005],top;
int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	getline(cin,ch);
	for(int i=0;i<ch.length();i++)
	if(ch[i]!='['&&ch[i]!=']'&&ch[i]!='<'&&ch[i]!='>'&&ch[i]!='+'&&ch[i]!='-'&&ch[i]!=','&&ch[i]!='.') ch.erase(i,1),i--;
	for(int i=0;i<ch.length();i++){
		if(ch[i]=='[') stk[++top]=i;else
		if(ch[i]==']'){
			if(top==0){printf("Error!\n");return 0;}
			if(!b[stk[top]]) ch.erase(i,1),ch.erase(stk[top],1),i-=(i==0?1:2);
			b[stk[top--]]=0;
		}else
		if(ch[i]=='+'||ch[i]=='-'||ch[i]=='<'||ch[i]=='>'||ch[i]=='.'||ch[i]==',') b[stk[top]]++;
	}
	if(top>0){printf("Error!\n");return 0;}
//	cout<<ch<<endl;
	for(int i=0;i<ch.length();i++){
		if(ch[i]=='+'||ch[i]=='-'){
			int num=0,lst=i;
			for(;ch[i]=='+'||ch[i]=='-';i++) num+=(ch[i]=='+'?1:-1);i--;
			if(num==0) ch.erase(lst,i-lst+1),i=lst-(lst==0?1:2);
		}else
		if(ch[i]=='<'||ch[i]=='>'){
			int num=0,lst=i;
			for(;ch[i]=='<'||ch[i]=='>';i++) num+=(ch[i]=='>'?1:-1);i--;
			if(num==0) ch.erase(lst,i-lst+1),i=lst-(lst==0?1:2);
		}else
		if(ch[i]=='['&&ch[i+1]==']') ch.erase(i,2),i-=(i==0?1:2);
	}
//	cout<<ch<<endl;
	len=ch.length();
	for(int i=0;i<len;i++){if(ch[i]=='[') stk[++top]=i;else if(ch[i]==']') a[stk[top--]]=i;}
	for(int i=0;i<len;i++){
		if(ch[i]=='+'||ch[i]=='-'){
			int num=0;
			for(;ch[i]=='+'||ch[i]=='-';i++) num+=(ch[i]=='+'?1:-1);i--;
			for(int j=1;j<=t;j++) printf("  ");
			if(num>0) printf("*p += %d;\n",num);else printf("*p -= %d;\n",-num);
		}else
		if(ch[i]=='<'||ch[i]=='>'){
			int num=0;
			for(;ch[i]=='<'||ch[i]=='>';i++) num+=(ch[i]=='>'?1:-1);i--;
			for(int j=1;j<=t;j++) printf("  ");
			if(num>0) printf("p += %d;\n",num);else printf("p -= %d;\n",-num);
		}else
		if(ch[i]=='.'){for(int j=1;j<=t;j++) printf("  ");printf("putchar(*p);\n");}else
		if(ch[i]==','){for(int j=1;j<=t;j++) printf("  ");printf("*p = getchar();\n");}else
		if(ch[i]=='['){vis[a[i]]=1;for(int j=1;j<=t;j++) printf("  ");printf("if (*p) do {\n"),t++;}else
		if(ch[i]==']'&&vis[i]==1){
			t--;
			for(int j=1;j<=t;j++) printf("  ");
			printf("} while (*p);\n");
		}
	}
	return 0;
}

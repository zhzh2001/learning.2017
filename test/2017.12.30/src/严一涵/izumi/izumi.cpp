#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <vector>
using namespace std;
typedef long long LL;
LL read(){LL a=0,b=1;char c=getchar();while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-'){b=-b;c=getchar();}while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}return a*b;}
struct edge{LL a,b;edge(){}edge(LL x,LL y){a=x;b=y;}}e[20003];
LL n,m,a[10003],en[10003],f[10003],f2[10003],et=0,x,y,ans,md=1000000007ll;
void adde(LL a,LL b){e[++et]=edge(en[a],b);en[a]=et;}
void dfs(LL u,LL fa,LL l,LL r){
	vector<LL>q;LL p=0,ans=1;
	f[u]=1;f2[u]=0;for(int i=en[u];i;i=e[i].a){
		int v=e[i].b;if(v!=fa){dfs(v,u,l,r);f[u]=f[u]*f[v]%md;q.push_back(f[v]);p++;}}
	for(int i=p-2;i>=0;i--)q[i]=q[i]*q[i+1]%md;p=0;
	for(int i=en[u];i;i=e[i].a){
		int v=e[i].b;if(v!=fa){p++;
			f2[u]=(f2[u]+f2[v]*ans%md*(p<q.size()?q[p]:1)%md)%md;ans*=f[v];
		}
	}
	if(a[u]<l||a[u]>r)f[u]=1,f2[u]=0;else f[u]++;
	if(a[u]==r)f2[u]=f[u]-1;
}
int main(){
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	m=read();n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){adde(x=read(),y=read());adde(y,x);}
	dfs(1,0,0,m);for(int i=1;i<=n;i++)ans=(ans+f[i]-1)%md;
	for(int i=1;i<=2000-m;i++){
		dfs(1,0,i,i+m);
		for(int j=1;j<=n;j++)ans=(ans+f2[j])%md;
	}
	printf("%lld",(ans+md)%md);return 0;
}

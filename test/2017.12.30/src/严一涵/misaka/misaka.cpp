#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <string.h>
using namespace std;
typedef long long LL;
LL read(){LL a=0,b=1;char c=getchar();while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-'){b=-b;c=getchar();}while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}return a*b;}
LL n,m,x,y,z,a[100003],b[100003],f[100003],A,B,ans,mx1,mx2,mc1,mc2,gc[100003];
int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)b[i]=read();
	for(int i=1;i<=m;i++){
		A=read();B=read();ans=mx1=mx2=0;mc1=mc2=-1;
		memset(f,0,sizeof(f));
		for(int j=1;j<=n;j++)gc[b[j]]=-1ll<<60;
		for(int j=1;j<=n;j++){
			x=mx1;if(b[j]==mc1)x=mx2;
			f[j]=max(B*a[j]+x,gc[b[j]]+A*a[j]);
			if(f[j]>mx1){
				if(b[j]!=mc1){mc2=mc1;mx2=mx1;}mx1=f[j];mc1=b[j];
			}else if(f[j]>mx2&&b[j]!=mc1){mc2=b[j];mx2=f[j];}
			gc[b[j]]=max(gc[b[j]],f[j]);
			ans=max(ans,f[j]);
		}
		printf("%lld\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=1e5+5;
struct NODE{
	int v,c;
}a[maxn];
int tot,ha[maxn],n,q;
int inline read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){ if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
inline int find(int x){
	int l=1,r=tot;
	while (l<r){
		int mid=(l+r)>>1;
		if (ha[mid]==x) return mid;
		if (ha[mid]>x) r=mid-1;
			else l=mid+1;
	}
}
inline void init(){
	n=read(); q=read();
	for (int i=1;i<=n;i++){
		a[i].v=read();
	}
	for (int i=1;i<=n;i++){
		a[i].c=read();
		ha[i]=a[i].c;
	}
	sort(ha+1,ha+1+n); tot=1; 
	for (int i=2;i<=n;i++){
		if (ha[tot]!=ha[i]){
			ha[++tot]=ha[i];
		}
	}
	for (int i=1;i<=n;i++){
		a[i].c=find(a[i].c);
	}
}
const int inf=1e9;
struct node{
	int mx;
}tr[maxn*4];
int A,B;
void build(int k,int l,int r){
	tr[k].mx=-inf;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r);
}
inline void pushup(int k){
	tr[k].mx=max(tr[k<<1].mx,tr[k<<1|1].mx);
}
void update(int k,int l,int r,int x,int y){
	if (l==r){
		tr[k].mx=max(tr[k].mx,y);
		return; 
	}
	int mid=(l+r)>>1;
	if (mid>=x){
		update(k<<1,l,mid,x,y);
	}else{
		update(k<<1|1,mid+1,r,x,y);
	}
	pushup(k);
}
int query(int k,int l,int r,int x,int y){
	if (x>y) return -inf;
	if (l==x&&r==y){
		return tr[k].mx;
	}
	int mid=(l+r)>>1;
	if (mid>=y) return query(k<<1,l,mid,x,y);
		else if (mid<x) return query(k<<1|1,mid+1,r,x,y);
			else return max(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y));
}
int best[maxn];
inline void solve(){
	build(1,0,tot);
	memset(best,128,sizeof(best)); best[0]=0;
	update(1,0,tot,0,0);
	for (int i=1;i<=n;i++){
		int x=-inf;
		x=max(max(query(1,0,tot,0,a[i].c-1),query(1,0,tot,a[i].c+1,tot))+B*a[i].v,best[a[i].c]+A*a[i].v);
		update(1,0,tot,a[i].c,x);
		best[a[i].c]=max(best[a[i].c],x);
	}
	printf("%d\n",tr[1].mx);
}
int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	init();
	for (int i=1;i<=q;i++){
		scanf("%d%d",&A,&B); 
		solve();
	}
	return 0;
}

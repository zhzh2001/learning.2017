#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn=10005;
struct edge{
	int link,next;
}e[maxn<<1];
int du[maxn],d,n,V[maxn],tot,head[maxn];
inline void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add(u,v); add(v,u);
}
bool flag;
inline void init(){
	scanf("%d%d",&d,&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&V[i]);
	}
	flag=1;
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		insert(u,v);
		du[u]++; du[v]++;
		if (du[u]>2) flag=0;
	}
}
const int inf=1e9;
int q[maxn],ans,vis[25],cnt,root,mx,mn;
void dfs2(int u,int fa){
	q[++tot]=u;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs2(v,u);
		}
	}
}
inline void solve2(){
	tot=0;
	for (int i=1;i<=n;i++){
		if (du[i]==1){
			root=i;
			break;
		}
	}
	dfs2(root,0);
	for (int i=1;i<=n;i++){
		mn=inf; mx=-inf;
		for (int j=i;j<=n;j++){
			mx=max(mx,V[j]); mn=min(mn,V[j]);
			if (mx-mn<=d){
				ans++;
			}else{
				break;
			}
		}
	}
	printf("%d\n",ans);
}
void dfs(int u){
	vis[u]=0; cnt--; mx=max(mx,V[u]); mn=min(mn,V[u]);
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (vis[v]){
			dfs(v);
		}
	}
}
inline void solve1(){
	for (int s=1;s<(1<<n);s++){
		memset(vis,0,sizeof(vis)); cnt=0; int x;
		for (int i=1;i<=n;i++){
			if (s&(1<<(i-1))){
				vis[i]=1;
				x=i;
				cnt++;
			}
		}
		mn=inf,mx=-inf;
		dfs(x);
		if (cnt==0&&mx-mn<=d){
			ans++;
		}
	}
}
const int mod=1e9+7;
ll dp[maxn][2],f[maxn],anss;
int l,r;
inline void dfs(int u,int fa){
	if (V[u]>=l&&V[u]<=r){
		dp[u][1]=1;
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			(dp[u][0]+=dp[v][0]+dp[v][1])%=mod;
			dp[u][1]*=(dp[v][1]+1); dp[u][1]%=mod;
		}
	}
	if (V[u]>=l&&V[u]<=r){
		f[u]=max(f[u],dp[u][1]); 
	}
}
inline void solve3(){
	for (l=0;l<=2000;l++){
		r=l+d; memset(dp,0,sizeof(dp));
		dfs(1,0);
	}
	for (int i=1;i<=n;i++){
		(anss+=f[i])%=mod;
	}	
	printf("%lld\n",anss);
}
inline void solve(){
	if (n<=20){
		solve1();
	}else{
		if (flag) solve2();
			else solve3();
	}
	printf("%d\n",ans);
}
int main(){
	freopen("izumi.in","r",stdin);
	freopen("izumi.out","w",stdout);
	init();
	solve();
	return 0;
}

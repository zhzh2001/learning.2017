#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=10005;
typedef long long ll;
struct edge{
	int link,next;
}e[maxn<<1];
int d,n,V[maxn],tot,head[maxn];
inline void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add(u,v); add(v,u);
}
inline void init(){
	scanf("%d%d",&d,&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&V[i]);
	}
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		insert(u,v);
	}
}
const int mod=1e9+7;
ll dp[maxn][2005],ans;
inline void dfs(int u,int fa){
	for (int i=max(V[i]-d,0);i<=V[u];i++){
		dp[u][i]=1;
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			for (int j=0;j<=2000;j++){
				dp[u][j]*=(dp[v][j]+1);
				dp[u][j]%=mod;
			}
		}
	}
}
inline void solve(){
	dfs(1,0);
	for (int i=0;i<=2000;i++){
		ans+=dp[1][i];
	}
	printf("%lld\n",ans);
}
int main(){
	init();
	solve();
	return 0;
}

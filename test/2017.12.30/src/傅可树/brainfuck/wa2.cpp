#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=12000;
char s[maxn];
inline void init(){
	scanf("%s",s+1);
}
struct node{
	int opt,val,kk;
}stack[maxn];
int len,now,top,del[maxn];
inline void newnode(int x,int y,int z){stack[++top]=(node){x,y,z};}
inline void pop(){top--;}
inline void work(){
	top=0;
	for (int i=1;i<=len;i++){
		if (del[i]) continue;
		if (s[i]=='>'){
			if (stack[top].opt==1){
				stack[top].val++;
			}else{
				newnode(1,1,now); 
			}
		}
		if (s[i]=='<'){
			if (stack[top].opt==1){
				stack[top].val--;
			}else{
				newnode(1,-1,now);
			}
		}
		if (s[i]=='+'){
			if (stack[top].opt==2){
				stack[top].val++;
			}else{
				newnode(2,1,now);
			}
		}
		if (s[i]=='-'){
			if (stack[top].opt==2){
				stack[top].val--;
			}else{
				newnode(2,-1,now);
			}
		}
		if (s[i]=='.'){
			newnode(3,1,now); 
		}
		if (s[i]==','){
			newnode(4,1,now);
		}
		if (s[i]=='['){
			newnode(5,1,now);
			now++;
		}
		if (s[i]==']'){
			now--;
			newnode(6,1,now);
		}
	}
} 
int sum;
bool flag;
inline void fir(){
	top=0;
	for (int i=1;i<=len;i++){
		if (s[i]=='['){
			newnode(1,i,0); sum--;
		}
		if (s[i]==']'){
			int cnt=0;
			while (top&&stack[top].opt!=1){
				pop();
				cnt++;
			}
			if (!cnt){
				del[stack[top].val]=1;
				del[i]=1;
			}
			pop(); sum++;
			if (cnt>0){
				newnode(3,i,0);
			} 
		}
		if (s[i]=='.'||s[i]==','||s[i]=='>'||s[i]=='<'||s[i]=='+'||s[i]=='-'){
			newnode(2,i,0);
		}
		if (top<0){
			flag=0;
		}
	}
	if (sum!=0){
		flag=0;
	}
}
inline void CC(int x){
	for (int i=1;i<=x;i++) putchar(' ');
}
inline void print(){
	for (int i=1;i<=top;i++){
		CC(stack[i].kk);
		if (stack[i].opt==1){
			printf("p += %d;\n",stack[i].val);
		}
		if (stack[i].opt==2){
			printf("*p += %d;\n",stack[i].val);
		} 
		if (stack[i].opt==3){
			puts("putchar(*p);");
		}
		if (stack[i].opt==4){
			puts("*p = getchar();");
		}
		if (stack[i].opt==5){
			puts("if (*p) do {");
		}
		if (stack[i].opt==6){
			puts("} while (*p);");
		}
	} 
}
inline void solve(){
	len=strlen(s+1); 
//	fir();
//	if (flag){
//		puts("Error!");
	//	return;
//	}
	work();
	print();
}
int main(){
	init();
	solve();
	return 0;
}

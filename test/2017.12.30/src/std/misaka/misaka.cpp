#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll f[N], v[N], c[N];
ll _max(ll a, ll b){return a>b?a:b;}
int main(int argc, char const *argv[]) {
	freopen("misaka.in", "r", stdin);
	freopen("misaka.out", "w", stdout);
	int n = read(), q = read();
	for (int i = 1; i <= n; i++)
		v[i] = read();
	for (int i = 1; i <= n; i++)
		c[i] = read();
	while (q --) {
		ll a = read(), b = read();
		for (int i = 1; i <= n; i++)
			f[i] = - 1ll << 60;
		int max1_color = 0, max2_color = 0;
		for (int i = 1; i <= n; i++) {
			int color = c[i] == max1_color ? max2_color : max1_color;
			ll res = _max(f[color] + b * v[i], f[c[i]] + a * v[i]);
			f[c[i]] = _max(f[c[i]], res);
			if (f[c[i]] > f[max1_color]) {
				max2_color = max1_color;
				max1_color = c[i];
			} else if (f[c[i]] > f[max2_color]) {
				if (c[i] == max1_color)
					continue;
				max2_color = c[i];
			}
		}
		cout << _max(f[max1_color], 0ll) << endl;
	}
	return 0;
}

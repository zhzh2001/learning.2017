#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

int f[11000],nedge,head[11000],d,n,a[11000],t[11000],s,ans;

struct ro{
	int next,to;
}edge[22000];

void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].next=head[x];
	head[x]=nedge;
}

void dfs(int x,int y)
{/*
	printf("%d\n",x);
	t[x]=y;
	for (int i=head[x];i!=0;i=edge[i].next)
	dfs(edge[i].to,y+1);*/
}

void sum(int x,int l,int h)
{
	if (f[x]==1) return;
	f[x]=1;
	int ll=l,hh=h;
	if (a[x]<l) ll=a[x];
	if (a[x]>h) hh=a[x];
	if (h-l>d) return;
	for (int i=head[x];i!=0;i=edge[i].next)
	sum(edge[i].to,ll,hh);
	ans++;
	f[x]=0;
}

int main(){
	scanf("%d%d",&d,&n);
	for (int i=1;i<=n;i++) {
		scanf("%d",&a[i]);
	}
	for (int i=1;i<n;i++){
		int b,c;
		scanf("%d%d",&b,&c);
		add(b,c);
		add(c,b);
	}
	dfs(1,1);
	for (int i=1;i<=n;i++) {
		s=-1;
		sum(i,a[i],a[i]);
	}
	printf("%d",ans);
	return 0;
}

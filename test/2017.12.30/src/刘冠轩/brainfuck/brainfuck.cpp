#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}


char s[20000];
int l,_next[20000];

void print(char c,int ss)
{
	if (c==']') l--;
	if (c=='>') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("p += %d;\n",ss);
	}
	else if (c=='<') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("p -= %d;\n",ss);
	}	
	else if (c=='+') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("*p += %d;\n",ss);
	} 
 else if (c=='-') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("*p -= %d;\n",ss);
	}
	else if (c=='.') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("putchar(*p);\n");
	}
	else if (c==',') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("*p = getchar();\n");
	}
	else if (c=='[') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("if (*p) do {\n");
	}
	else if (c==']') {
		for (int i=1;i<=l;i++) printf("  ");
		printf("} while (*p);\n");
	}
	if (c=='[') l++;
}

void hf(int xx){
	int x=xx,y=_next[x];
	while (((s[x]=='+' && s[_next[x]]=='-')||
		(s[x]=='<' && s[_next[x]]=='>')||
		(s[x]=='[' && s[_next[x]]==']'))){
		x--;
		y++;
		_next[x]=y;
	}
}

void hf1(int x){
	int xx=x,ss=0;
	while (s[_next[xx]]==s[x]){
		ss++;
	}
	print(s[xx],ss);
}

/*
int main(){
	scanf("%s",s);
	int c1=0,c2=0;
	for (int i=0;i<strlen(s);i++){
		if (s[i]=='[') c1++;
		if (s[i]==']') c2++;
	}
	if (c1!=c2) {
		printf("Error!");
		return 0;
	}
	for (int i=0;i<strlen(s);i++){
		if (s[i]=='+' || s[i]=='-')	i=hf(i);
		else if (s[i]=='>' || s[i]=='<') i=hf2(i);
		else if (s[i]=='[' && s[i+1]==']') i++;
		else print(s[i],0);
	}
	return 0;
}

*/

int main(){
	freopen("brainfuck.in","r",stdin);
	freopen("brainfuck.out","w",stdout);
	scanf("%s",s);
	int c1=0,c2=0;
	for (int i=0;i<strlen(s);i++){
		if (s[i]=='[') c1++;
		if (s[i]==']') c2++;
	}
	if (c1!=c2) {
		printf("Error!");
		return 0;
	}
	for (int i=0;i<strlen(s);i++){
		_next[i]=i+1;
	}
	for (int i=0;i<strlen(s);i++){
		if ((s[i]=='+' && s[_next[i]]=='-')||
		(s[i]=='<' && s[_next[i]]=='>')||
		(s[i]=='[' && s[_next[i]]==']')) hf(i);
	}
	int i=0;
	while (_next[i]!=0){
		if (s[i]=='+' || s[i]=='-')	hf1(i);
		else if (s[i]=='>' || s[i]=='<') hf1(i);
		print(s[i],0);
		i=_next[i];
	}
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

int f[10000],v[10000],c[10000],a,b,n,q;

int tp(int x,int y){
	if (c[x]==c[y]) return a;
	else return b;
}

int main(){
	freopen("misaka.in","r",stdin);
	freopen("misaka.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++) scanf("%d",&v[i]);
	for (int i=1;i<=n;i++) scanf("%d",&c[i]);
	for (int i=1;i<=q;i++){
		scanf("%d%d",&a,&b);
		memset(f,sizeof(f),0);
		for (int j=1;j<=n;j++){
			f[j]=v[j]*b;
			for (int k=1;k<j;k++)
			f[j]=max(f[k]+v[j]*tp(k,j),f[j]);
		}
		int ans=0;
		for (int j=1;j<=n;j++) ans=max(ans,f[j]);
		printf("%d\n",ans);
	}
	return 0;
}

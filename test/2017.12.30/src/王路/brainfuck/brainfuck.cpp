#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

namespace IO {
static const int SZ = 1 << 20;
static char buf[SZ], *S = buf, *T = buf + SZ;
inline int Flush() {
	fwrite(buf, sizeof(char), S - buf, stdout);
	return 0;
}
inline void Write(const char *str) {
	int len;
	if (S + (len = strlen(str)) > T)
		Flush();
	for (int i = 0; i < len; ++i)
		*S++ = str[i];
}
} // namespace IO

using IO::Write;

const int MaxL = 30005;
char code[MaxL];
int n;

struct Stack {
  int st[MaxL], top;
  inline Stack() : top(0) {}
  inline void Push(int x) {
    st[++top] = x;
  }
  inline bool Pop() {
    if (--top == 0)
      return false;
    return true;
  }
  inline int& Top() {
    return st[top];
  }
  inline int Size() {
    return top;
  }
} br, bt, sl, sp;

int cnt[9];

// Mapper
// 1 -> [
// 2 -> ]
// 3 -> +
// 4 -> -
// 5 -> >
// 6 -> <
// 7 -> .
// 8 -> ,

inline void Solve() {
  char tmp[50];
  int ptop = 0;
  for (int i = 1; i <= br.top; ++i) {
	  if (br.st[i] == 1) {
	    sl.Push(1);
	    continue;
	  }
	  if (br.st[i] == 2) {
	    if (br.Top() == 1)
        sl.Pop();
      else
        sl.Push(2);
      continue;
	  }
	  if (br.st[i] == 3) {
	    ++ptop;
	    if (bt.st[ptop] > 0)
	      sl.Push(3), sp.Push(bt.st[ptop]);
	    if (bt.st[ptop] < 0)
	      sl.Push(4), sp.Push(-bt.st[ptop]);
	    continue;
	  }
	  if (br.st[i] == 5) {
	    ++ptop;
	    if (bt.st[ptop] > 0)
	      sl.Push(5), sp.Push(bt.st[ptop]);
	    if (bt.st[ptop] < 0)
	      sl.Push(6), sp.Push(-bt.st[ptop]);
	    continue;
	  }
	  if (br.st[i] == 7) {
	    sl.Push(7);
	    continue;
	  }
	  if (br.st[i] == 8) {
	    sl.Push(8);
	    continue;
	  }
  }
  ptop = 0;
  int dep = 0;
  for (int i = 1; i <= sl.top; ++i) {
    if (sl.st[i] == 1) {
      Write("if (*p) do {\n"), ++dep;
      continue;
    } else if (sl.st[i] == 2) {
      Write("} while (*p);\n"), --dep;
      continue;
    }
    for (int j = 1; j <= dep; ++j) {
      Write("  "); 
    }
    if (sl.st[i] == 3) {
      sprintf(tmp, "*p += %d;\n", sp.st[++ptop]);
      Write(tmp);
    } else if (sl.st[i] == 4) {
      sprintf(tmp, "*p -= %d;\n", sp.st[++ptop]);
      Write(tmp);
    } else if (sl.st[i] == 5) {
      sprintf(tmp, "p += %d;\n", sp.st[++ptop]);
      Write(tmp);
    } else if (sl.st[i] == 6) {
      sprintf(tmp, "p -= %d;\n", sp.st[++ptop]);
      Write(tmp);
    } else if (sl.st[i] == 7) {
      Write("putchar(*p);\n");
    } else if (sl.st[i] == 8) {
      Write("*p = getchar();\n");
    }
  }
}

int main() {
  freopen("brainfuck.in", "r", stdin);
  freopen("brainfuck.out", "w", stdout);
	scanf("%s", code);
	n = strlen(code);
	for (int i = 0; i < n; ++i) {
	  if (code[i] == '[') {
	    br.Push(1);
	    ++cnt[1];
	    continue;
	  }
	  if (code[i] == ']') {
	    ++cnt[2];
	    if (br.Top() == 1)
        br.Pop();
      else
        br.Push(2);
      continue;
	  }
	  if (cnt[2] > cnt[1]) {
	    Write("Error!\n");
	    return 0;
	  }
	  if (code[i] == '>') {
	    if (br.Top() == 5) {
	      ++bt.Top();
	    } else {
	      br.Push(5);
        bt.Push(1);
	    }
	    if (bt.Top() == 0)
	      br.Pop(), bt.Pop();
	    continue;
	  }
	  if (code[i] == '<') {
	    if (br.Top() == 5) {
	      --bt.Top();
	    } else {
	      br.Push(5);
        bt.Push(-1);
	    }
	    if (bt.Top() == 0)
	      br.Pop(), bt.Pop();
	    continue;
	  }
	  if (code[i] == '+') {
	    if (br.Top() == 3) {
	      ++bt.Top();
	    } else {
	      br.Push(3);
        bt.Push(1);
	    }
	    if (bt.Top() == 0)
	      br.Pop(), bt.Pop();
	    continue;
	  }
	  if (code[i] == '-') {
	    if (br.Top() == 3) {
	      --bt.Top();
	    } else {
	      br.Push(3);
        bt.Push(-1);
	    }
	    if (bt.Top() == 0)
	      br.Pop(), bt.Pop();
	    continue;
	  }
	  if (code[i] == '.') {
	    br.Push(7);
	    continue;
	  }
	  if (code[i] == ',') {
	    br.Push(8);
	    continue;
	  }
	}
	Solve();
	return IO::Flush();
}

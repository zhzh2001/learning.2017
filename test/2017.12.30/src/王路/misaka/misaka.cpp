#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

const int MaxN = 1e5 + 5, Inf = 0x3f3f3f3f;

int n, q;
long long v[MaxN], c[MaxN], f1[105];

int Solve1() {
  for (int i = 1; i <= n; ++i) scanf("%lld", v + i);
  for (int i = 1; i <= n; ++i) scanf("%lld", c + i);
  for (int t = 1; t <= q; ++t) {
    long long a, b;
    scanf("%lld%lld", &a, &b); 
    memset(f1, 0x00, sizeof f1);
    for (int i = 1; i <= n; ++i)
      f1[i] = b * v[i];
    for (int i = 2; i <= n; ++i)
      for (int j = 1; j < i; ++j) {
        f1[i] = max(f1[i], f1[j] + ((c[i] == c[j]) ? (a * v[i]) : (b * v[i])));
      }
    long long ans = 0;
    for (int i = 1; i <= n; ++i) {
      ans = max(ans, f1[i]);
//      cerr << f1[i] << ' ';
    }
//    cerr << endl;
    printf("%lld\n", ans);
  }
  return 0;
}

long long f2[MaxN];
int last_pos[MaxN];

int Solve2() {
  for (int i = 1; i <= n; ++i)
    scanf("%lld", v + i);
  for (int i = 1; i <= n; ++i)
    scanf("%lld", c + i);
  for (int t = 1; t <= q; ++t) {
    long long a, b;
    scanf("%lld%lld", &a, &b);
    memset(f2, 0x00, sizeof f2);
    memset(last_pos, 0x00, sizeof last_pos);
    for (int j = 1; j <= n; ++j)
      f2[j] = b * v[j];
    long long firstmax = -Inf, secondmax = -Inf, cfirstmax = 0, csecondmax = 0;
    firstmax = f2[1];
    cfirstmax = c[1];
    last_pos[c[1]] = 1;
    for (int i = 2; i <= n; ++i) {
      f2[i] = max(f2[i], last_pos[c[i]] ? (f2[last_pos[c[i]]] + (a * v[i])) : -Inf);
      if (c[i] == cfirstmax && csecondmax) {
        f2[i] = max(f2[i], secondmax + (b * v[i]));
      } else {
        f2[i] = max(f2[i], firstmax + (b * v[i]));
      }
      
      if (!last_pos[c[i]] || f2[i] > f2[last_pos[c[i]]])
        last_pos[c[i]] = i;
      if (c[i] == cfirstmax && f2[i] > firstmax)
        firstmax = f2[i];
      else if (c[i] != cfirstmax && f2[i] > firstmax) {
        secondmax = firstmax;
        csecondmax = cfirstmax;
        firstmax = f2[i];
        cfirstmax = c[i];
      } else if (f2[i] > secondmax)
        secondmax = f2[i], csecondmax = c[i];
    }
    printf("%lld\n", max(firstmax, 0LL));
//    for (int i = 1; i <= n; ++i)
//      cerr << f2[i] << ' ';
//    cerr << endl;
  }
  return 0;
}

int main() {
  freopen("misaka.in", "r", stdin);
  freopen("misaka.out", "w", stdout);
  scanf("%d%d", &n, &q);
  if (n <= 100 && q <= 100)
    return Solve1();
  return Solve2();
}

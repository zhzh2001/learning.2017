#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

const int MaxN = 10005, Mod = 1e9 + 7, Inf = 0x3f3f3f3f;
int v[MaxN];

int head[MaxN], ecnt, d, n, eu[MaxN], ev[MaxN], deg[MaxN];

struct Edge { int to, nxt; } e[MaxN << 1];
inline void AddEdge(int u, int v) {
  e[++ecnt] = (Edge) { v, head[u] };
  head[u] = ecnt; 
}

struct DisjointSet {
  int fa[MaxN];
  inline int Find(int x) { return x == fa[x] ? x : fa[x] = Find(fa[x]); }
  inline bool Same(int x, int y) { return Find(x) == Find(y); }
  inline void Reset() {
    for (int i = 0; i < MaxN; ++i)
      fa[i] = i;
  }
  inline bool Merge(int x, int y) {
    int fx = Find(x), fy = Find(y);
    if (fx != fy) {
      fa[fx] = fy;
      return true;
    }
    return false;
  }
} uf;

int Solve1() {
  int res = 0;
  for (int i = 0; i < (1 << n); ++i) {
    uf.Reset();
    for (int k = 1; k < n; ++k)
      if ((i & (1 << (eu[k] - 1))) && (i & (1 << (ev[k] - 1)))) {
        uf.Merge(eu[k] - 1, ev[k] - 1);
      }
    int blockcnt = 0, mx = 0, mn = Inf;
    for (int k = 0; k < n; ++k) {
      if ((i & (1 << k)) && uf.fa[k] == k)
        ++blockcnt;
    }
    if (blockcnt > 1 || blockcnt == 0)
      continue;
    for (int k = 0; k < n; ++k) {
      if ((i & (1 << k))) {
        mx = max(mx, v[k + 1]);
        mn = min(mn, v[k + 1]); 
      }
    }
    if (mx - mn <= d)
      ++res;
  }
  printf("%d\n", res % Mod);
  return 0;
}

int sp, ed;

inline bool Check() {
  int cnt = 0, cnt1 = 0;
  for (int i = 1; i < n; ++i) {
    ++deg[eu[i]];
    ++deg[ev[i]];
  }
  for (int i = 1; i <= n; ++i) {
    if (deg[i] == 1) {
      ++cnt;
      if (cnt == 1)
        sp = i;
      else
        ed = i;
    }
    else if (deg[i] == 2)
      ++cnt1; 
  }
  if (cnt == 2 && cnt1 + cnt == n)
    return true;
  return false;
}

int id[MaxN];

inline int Solve2() {
  int idcnt = 0;
  id[++idcnt] = 1;
  for (int pa = -1, i = sp; i != ed; ) {
    int last = i;
    for (int j = head[i]; j; j = e[j].nxt) {
      if (e[j].to != pa) {
        i = e[j].to, id[++idcnt] = i; 
      }
    }
    pa = last;
  }
  int res = 0;
  for (int i = 1; i <= n; ++i) {
    int mx1 = v[id[i]], mn1 = v[id[i]];
    if (mx1 - mn1 <= d) {
      ++res;
    }
    for (int j = i + 1; j <= n; ++j) {
      mx1 = max(mx1, v[id[j]]);
      mn1 = min(mn1, v[id[j]]);
      if (mx1 - mn1 <= d) {
        ++res;   
      }
    }
  }
  printf("%d\n", res % Mod);
  return 0;
}

int main() {
  freopen("izumi.in", "r", stdin);
  freopen("izumi.out", "w", stdout);
  scanf("%d%d", &d, &n);
  for (int i = 1; i <= n; ++i)
    scanf("%d", v + i);
  for (int i = 1; i < n; ++i) {
    scanf("%d%d", eu + i, ev + i);
    AddEdge(eu[i], ev[i]);
    AddEdge(ev[i], eu[i]);
  }
  if (n <= 20)
    return Solve1();
  if (n <= 2000 && Check())
    return Solve2();
  return 0;
}

#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll f[N], v[N], c[N];
int main(int argc, char const *argv[]) {
	freopen("misaka.in", "r", stdin);
	freopen("misaka.out", "w", stdout);
	int n = read(), q = read();
	for (int i = 1; i <= n; i++)
		v[i] = read();
	for (int i = 1; i <= n; i++)
		c[i] = read();
	while (q --) {
		ll a = read(), b = read();
		for (int i = 1; i <= n; i++)
			f[i] = - 1ll << 62;
		int max1_color = 0, max2_color = 0;
		ll ans = 0;
		for (int i = 1; i <= n; i++) {
			int color = c[i] == max1_color ? max2_color : max1_color;
			ll res = max(f[color] + b * v[i], f[c[i]] + a * v[i]);
			ans = max(ans, res);
			if (res > f[c[i]]) {
				f[c[i]] = res;
				if (res < f[max2_color]) continue;
				if (c[i] == max1_color) continue;
				max2_color = c[i];
				if (f[max2_color] > f[max1_color])
					swap(max1_color, max2_color);
			}
		}
		cout << ans << endl;
	}
	return 0;
}

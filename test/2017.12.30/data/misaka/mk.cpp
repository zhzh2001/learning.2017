#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
ofstream fout("misaka5.in");
int rand_int() {
	return rand() * rand() % 200000 - 100000;
}
int main(int argc, char const *argv[]) {
	srand(time(0));
	while (rand() % 50) rand();
	// int n = rand() % 100 + 1, q = rand() % 100 + 1;
	int n = 10000, q = 500;
	fout << n << " " << q << endl;
	for (int i = 1; i <= n; i++)
		fout << rand_int() << " "; // value
	fout << endl;
	for (int i = 1; i <= n; i++)
		fout << rand() * rand() % n + 1 << " "; // color
	fout << endl;
	while (q --)
		fout << rand_int() << " " << rand_int() << endl;
	return 0;
}
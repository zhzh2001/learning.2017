#include <bits/stdc++.h>
#include <fstream>

char arr[8] = {'+', '-', '>', '<', '.', ',', '[', ']'};
const int Nsmall  = 100;
const int Nmedium = 1000;
const int Nlarge  = 10000;
const int Nsuper  = 100000;

std::ofstream fout("brainfuck10.in");

int main(int argc, char const *argv[])
{
  srand(time(NULL));
  
  for (int i = 0; i < 1; ++i) {
    std::string code;
    int count = 0;
    for (int j = 0; j < 100000; ++j) {
      switch (rand()%8) {
        case 0: {
          code.push_back('[');
          ++count;
          break;
        }
        case 1: {
          if (count) {
            code.push_back(']');
            --count;
          }
          break;
        }
        default: {
          code.push_back(arr[rand()%6]);
          break;
        }
      } // switch
      
    } // for
    code.append(count, ']');
    fout << code << std::endl;
  } // for
} // It

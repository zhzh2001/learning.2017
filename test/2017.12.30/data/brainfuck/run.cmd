@echo off
for /L %%i in (1, 1, 10) do (
	copy brainfuck%%i.in brainfuck.in
	std
	copy brainfuck.out brainfuck%%i.out
)
erase brainfuck.in
erase brainfuck.out
pause

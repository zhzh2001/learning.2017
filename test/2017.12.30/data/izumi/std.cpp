#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define mod 1000000007
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], cnt;
void insert(int x, int y) {
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
pair<int, int> v[N];
int rk[N], d, n, val[N];
int dfs(int x, int f, int rt) {
	long long res = 1;
	for (int i = head[x]; i; i = nxt[i])
		if (to[i] != f && rk[to[i]] < rk[rt] && val[to[i]] >= val[rt] - d)
			res = res * dfs(to[i], x, rt) % mod;
	return (res + 1) % mod;
}
int main(int argc, char const *argv[]) {
	freopen("izumi.in", "r", stdin);
	freopen("izumi.out", "w", stdout);
	d = read(); n = read();
	for (int i = 1; i <= n; i++)
		val[i] = v[i].first = read(), v[i].second = i;
	sort(v + 1, v + n + 1);
	for (int i = 1; i <= n; i++)
		rk[v[i].second] = i;
	for (int i = 1; i < n; i++)
		insert(read(), read());
	ll ans = 0;
	for (int i = 1; i <= n; i++)
		ans = ((ans + dfs(i, 0, i) - 1) % mod + mod) % mod;
	cout << ans << endl;
	return 0;
}
#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=5005;
char s[maxn];
int vis[maxn][maxn][2];
bool dp[maxn][maxn][2];
int cas,a[maxn],tot1,tot2,x,b[maxn],w[maxn],len;
inline void init(){
	scanf("%s",s+1);
	len=strlen(s+1);
	for (int i=1;i<=len;i++){
		if (s[i]=='b'){
			b[++tot1]=i;
			a[i]=0;	
		}else{
			if (s[i]=='W'){
				x=i;
			}
			w[++tot2]=i;
			a[i]=1;
		}
	}
}
bool dfs(int l,int r,int opt){
	if (l==1&&r==len){
		return 0;
	}
	if (vis[l][r][opt]==cas){
		return dp[l][r][opt];
	}
	int v,ll,rr;
	ll=l; rr=r;
	vis[l][r][opt]=cas;
	bool &ans=dp[l][r][opt]; ans=0;
	if (opt==0){
		v=a[l];
		ll++;
	}else{
		v=a[r];
		rr--;
	}
	if (!v){
		for (int i=1;i<=tot2;i++){
			if (w[i]<ll){
				if (!dfs(w[i],r,0)){
					ans=1;
					return 1;
				}
			}
		}
		for (int i=tot2;i;i--){
			if (w[i]>rr){
				if (!dfs(l,w[i],1)){
					ans=1;
					return 1;
				}
			}
		}
	}else{
		for (int i=1;i<=tot1;i++){
			if (b[i]<ll){
				if (!dfs(b[i],r,0)){
					ans=1;
					return 1;
				}
			}
		}
		for (int i=tot1;i;i--){
			if (b[i]>rr){
				if (!dfs(l,b[i],1)){
					ans=1;
					return 1;
				}
			}
		}		
	}
	return ans;
}
inline void solve(){
	if (dfs(x,x,0)){
		puts("yes");
	}else{
		puts("no");
	}
}
inline void clean(){
	tot1=tot2=0;
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		++cas;
		clean();
		init();
		solve();
	}
	return 0;
}

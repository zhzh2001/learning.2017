#include<cstdio>
#include<algorithm>
using namespace std;
int n;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
typedef long long ll;
const int maxn=100005,maxm=2000005;
struct point{
	int x,y;
}a[maxn];
struct edge{
	int u,v;
	ll w;
}e[maxm];
int tot,fa[maxn];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		a[i].x=read(); a[i].y=read();
	}
}
inline ll abs(ll x){return (x>0)?x:-x;}
inline ll dis(int l,int r){
	return abs(a[l].x-a[r].x)+abs(a[l].y-a[r].y);
}
inline int get(int l,int r){
	return (rand()<<4)%(r-l+1)+l;
}
int find(int x){
	return (fa[x]==x)?x:fa[x]=find(fa[x]);
}
inline bool cmp(edge x,edge y){
	return x.w<y.w;
}
int kis;
inline void solve(){
	if (n<=1000) {
		for (int i=1;i<n;i++){
			for (int j=i+1;j<=n;j++){
				e[++tot].u=i; e[tot].v=j; e[tot].w=dis(i,j);
			}
		}
	}
	else{
		for (int i=1;i<n;i++){
			for (int i=1;i<=10;i++){
				e[++tot].u=get(1,n); e[tot].v=get(1,n); e[tot].w=dis(e[tot].u,e[tot].v);
			}
		}
	}
	for (int i=1;i<=n;i++){
		fa[i]=i;
	}
	ll ans=0; int cnt=0;
	sort(e+1,e+1+tot,cmp);
	for (int i=tot;i;i--){
		int p=find(e[i].u),q=find(e[i].v);
		if (p!=q){
			fa[p]=q;
			cnt++;
			ans+=e[i].w;
		}
		if (cnt==n-1) break;
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	init();
	solve();
	return 0;
}

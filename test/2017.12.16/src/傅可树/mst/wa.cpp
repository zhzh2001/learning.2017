#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=1e5+5;
struct point{
	int x,y,id;
}a[maxn],b[maxn];
struct edge{
	int u,v,w;
}e[maxn*9];
int n;
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		a[i].x=read(); a[i].y=read();
		a[i].id=i;
		b[i]=a[i];
	}
}
inline bool cmp(point x,point y){
	if (x.x==y.x){
		return x.y>y.y;
	}else{
		return x.x<y.x;
	}
}
inline bool cmp1(point x,point y){
	if (x.y==y.y){
		return x.x>y.x;
	}else{
		return x.y<y.y;
	}
}
int tot;
inline int abs(int x){return (x>0)?x:-x;}
inline int dis(int l,int r){
	return abs(a[l].x-a[r].x)+abs(a[l].y-a[r].y);
}
inline bool cmp2(edge x,edge y){
	return x.w<y.w;
}
int num[5],fa[maxn];
int find(int x){
	return (fa[x]==x)?x:fa[x]=find(fa[x]);
}
inline void solve(){
	sort(b+1,b+1+n,cmp);
	num[1]=b[1].id;
	for (int i=1;i<=n;i++){
		if (b[i].x!=b[i+1].x){
			num[2]=b[i].id;
			break;
		} 
	}
	num[3]=b[n].id;
	for (int i=n;i;i--){
		if (b[i].x!=b[i-1].x){
			num[4]=b[i].id;
			break;
		} 
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=4;j++){
			e[++tot].u=i;
			e[tot].v=num[j];
			e[tot].w=dis(i,num[j]);
		}
	}
	sort(b+1,b+1+n,cmp1);
	num[1]=b[1].id;
	for (int i=1;i<=n;i++){
		if (b[i].y!=b[i+1].y){
			num[2]=b[i].id;
			break;
		} 
	}
	num[3]=b[n].id;
	for (int i=n;i;i--){
		if (b[i].y!=b[i-1].y){
			num[4]=b[i].id;
			break;
		} 
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=4;j++){
			e[++tot].u=i;
			e[tot].v=num[j];
			e[tot].w=dis(i,num[j]);
		}
	}
	for (int i=1;i<=n;i++){
		fa[i]=i;
	}
	ll ans=0; int cnt=0;
	sort(e+1,e+1+tot,cmp2);
	for (int i=tot;i;i--){
		int p=find(e[i].u),q=find(e[i].v);
		if (p!=q){
			fa[p]=q;
			cnt++;
			ans+=e[i].w;
		}
		if (cnt==n-1) break;
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	init();
	solve();
	return 0;
}

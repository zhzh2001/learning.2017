#include<cstdio>
using namespace std;
const int maxn=15;
int ans,n,p,q[maxn],flag[maxn];
inline bool judge(){
	for (int i=1;i<=n;i++){
		if (i%2==1){
			if (q[i]<q[i-1]||q[i]<q[i+1]){
				return 0;
			}
		}
	}
	return 1;
}
void dfs(int now){
	if (now==n+1){
		if (judge()) ans++;
		return ;
	}
	for (int i=1;i<=n;i++){
		if (!flag[i]){
			q[now]=i;
			flag[i]=1;
			dfs(now+1);
			flag[i]=0;
		}
	}
}
int main(){
	freopen("perm.in","r",stdin);
	freopen("perm.out","w",stdout);
	scanf("%d%d",&n,&p);
	dfs(1);
	printf("%d\n",ans%p);
	return 0;	
}

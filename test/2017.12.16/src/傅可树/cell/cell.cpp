#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn=70;
int x;
ll C[maxn][maxn];
double ans,dp[2][maxn*2],bin2[10000],bin1[10000],p;
int main(){
	freopen("cell.in","r",stdin);
	freopen("cell.out","w",stdout);
	scanf("%d",&x);
	p=x/10000.0;
	bin1[0]=1;
	for (int i=1;i<=3005;i++){
		bin1[i]=bin1[i-1]*p;
	}
	C[0][0]=1;
	for (int i=1;i<=65;i++){
		C[i][0]=1;
		for (int j=1;j<=i;j++){
			C[i][j]=C[i-1][j]+C[i-1][j-1];
		}
	}
	bin2[0]=1;
	for (int i=1;i<=3000;i++){
		bin2[i]=bin2[i-1]*(1-p);
	}
	int cur=0; dp[cur][1]=1;
	for (int i=1;i<=500;i++){
		memset(dp[cur^1],0,sizeof(dp[cur^1]));
		for (int j=0;j<=65;j++){
			for (int k=0;k<=j;k++){
				dp[cur^1][k*2]+=C[j][k]*dp[cur][j]*bin1[k]*bin2[j-k];
			}
		}
		cur^=1;
	}
	printf("%.6lf\n",1-dp[cur^1][0]);
	return 0;
}

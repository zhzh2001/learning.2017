#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct edge{
	int x,y;
	ll v;
}e[15000005];
ll x[100005],y[100005];
ll ans=0;
int n,num,fa[100005];
 bool cmp(const edge &a,const edge &b)
{
	return a.v>b.v;
}
 int find(int v)
{
	return fa[v]==v?v:fa[v]=find(fa[v]);
}
 int main()
{
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i)
		scanf("%lld%lld",&x[i],&y[i]);
	for (int i=1;i<=n;++i)
		for (int j=i+1;j<=n;++j)
		{
			e[++num].v=abs(x[i]-x[j])+abs(y[i]-y[j]);
			e[num].x=i;e[num].y=j;
		}
	sort(e+1,e+num+1,cmp);
	for (int i=1;i<=n;++i) fa[i]=i;
	int have=0,numm=0;
	while (have<n-1)
	{
		++numm;
		int tx=find(e[numm].x);
		int ty=find(e[numm].y);
		if (tx!=ty)
		{
			fa[tx]=ty;
			++have;
			ans+=e[numm].v;
		}
	}
	printf("%lld\n",ans);
}

var
  n,i,j,t,p,max:longint;
  a:array[1..30000,1..2]of longint;
  b:array[1..30000]of integer;
function f(x:integer):integer;
begin
  if b[x]<>x then b[x]:=f(b[x]);
  exit(b[x]);
end;
begin
  assign(input,'mst.in');
  assign(output,'mst.out');
  reset(input);
  rewrite(output);
  read(n);
  for i:=1 to n do
  begin
    read(a[i,1],a[i,2]);
    b[i]:=i;
  end;
  for i:=2 to n do
  begin
    max:=-1000000000;
    for j:=1 to n do
      if f(i)<>f(j) then
      begin
        if (abs(a[i,1]-a[j,1])+abs(a[i,2]-a[j,2]))>max then
        begin
          max:=abs(a[i,1]-a[j,1])+abs(a[i,2]-a[j,2]);
          p:=j;
        end;
      end;
    t:=t+max;
    b[f(i)]:=f(j);
  end;
  writeln(t);
  close(input);
  close(output);
end.
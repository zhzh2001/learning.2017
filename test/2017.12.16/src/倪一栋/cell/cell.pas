var
  i:longint;
  p,ans,p2:real;
  a:array[1..100000]of longint;
begin
  assign(input,'cell.in');
  assign(output,'cell.out');
  reset(input);
  rewrite(output);

  read(p);
  p:=1-(p/10000);
  p2:=1;
  ans:=0;
  for i:=1 to 100000 do
  begin
    p2:=p2*p;
    ans:=p2+ans;
  end;
  writeln((1-ans):0:6);

  close(input);
  close(output);
end.

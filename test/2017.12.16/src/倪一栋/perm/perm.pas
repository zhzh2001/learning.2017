var
  n,ans,q,i:longint;
  a:array[0..1000]of longint;
  b:array[0..1000]of boolean;
procedure p(x:longint);
var
  i:longint;
begin
  if x<=n then
  for i:=1 to n do
  begin
    if b[i]=true then
    begin
      b[i]:=false;
      a[x]:=i;
      p(x+1);
      b[i]:=true;
    end;
  end;
  if x>n then
  begin
    for i:=0 to (n-1) div 2 do
    if (a[i*2]>a[i*2+1]) or (a[i*2+2]>a[i*2+1]) then exit;
    ans:=(ans+1) mod q;
  end;
end;
begin
  assign(input,'perm.in');
  assign(output,'perm.out');
  reset(input);
  rewrite(output);

  read(n,q);
  ans:=0;
  for i:=1 to n do
  b[i]:=true;
  p(1);
  writeln(ans);

  close(input);
  close(output);
end.
var
n,i,j:longint;
x,y:array[1..100000]of int64;
maxx,maxy,minx,miny,ans,a,b:int64;
procedure sort(l,r: longint);
      var
         i,j,m,t: int64;
      begin
         i:=l;
         j:=r;
         m:=x[(l+r) div 2];
         repeat
           while x[i]<m do
            inc(i);
           while m<x[j] do
            dec(j);
           if not(i>j) then
             begin
                t:=x[i];
                x[i]:=x[j];
                x[j]:=t;
				t:=y[i];
				y[i]:=y[j];
				y[j]:=t;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
function get_dist(a,b,c,d:int64):int64;
  begin
    exit(abs(a-b)+abs(c-d));
  end;
begin
assign(input,'mst.in');reset(input);
assign(output,'mst.out');rewrite(output);
read(n);
for i:=1 to n do read(x[i],y[i]);
sort(1,n);
minx:=-maxlongint;miny:=-maxlongint;maxx:=-maxlongint;maxy:=-maxlongint;
for i:=1 to n-1 do
  begin
    if(x[i]>maxx)or(x[i]>minx)then
	  begin
	    maxx:=x[i+1];maxy:=y[i+1];
		minx:=x[i+1];maxy:=y[i+1];
	    for j:=i+2 to n do
		  begin
		    if get_dist(x[i],x[j],y[i],y[j])>get_dist(x[i],maxx,y[i],maxy) then
			  begin
			    maxx:=x[j];maxy:=y[j];
			  end;
			if get_dist(x[i],x[j],y[i],y[j])<get_dist(x[i],minx,y[i],miny) then
			  begin
			    minx:=x[j];miny:=y[j];
			  end;
		  end;
	  end;
	a:=get_dist(x[i],maxx,y[i],maxy);
	b:=get_dist(x[i],minx,y[i],miny);
	if a>b then ans:=ans+a else ans:=ans+b;
  end;
write(ans);
close(input);close(output);
end.
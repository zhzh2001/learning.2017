var
t,x,i,n:longint;
a:array[1..1000]of char;
vis:array[1..1000]of boolean;
function solve():boolean;
  var
  i,t1,t2,count1,count2:longint;
  ch:char;
  f:boolean;
  begin
    ch:='B';
	while true do
	  begin
	    count1:=0;count2:=0;f:=false;
		t1:=x;
	    for i:=1 to x-1 do
		  if(a[i]=ch)and(not vis[i])then
			begin
			  t1:=i;f:=true;
			  break;
			end;
		for i:=t1+1 to x do
		  if(a[i]<>ch)and(not vis[i])then
		    inc(count1);
		
		t2:=x;
		for i:=n downto x+1 do
		  if(a[i]=ch)and(not vis[i])then
			begin
			  t2:=i;f:=true;
			  break;
			end;
		for i:=x to t2-1 do
		  if(a[i]<>ch)and(not vis[i])then
		    inc(count2);
			
		if not f then
		  begin
		    if ch='B' then exit(false);
			exit(true);
		  end;
		if count1>count2 then
		  begin
		    for i:=t1+1 to x do vis[i]:=true;
			x:=t1;
		  end         else
		  begin
		    for i:=x to t2-1 do vis[i]:=true;
			x:=t2;
		  end;
		if ch='B' then ch:='W' else ch:='B';
	  end;
  end;
begin
assign(input,'game.in');reset(input);
assign(output,'game.out');rewrite(output);
readln(t);
while t>0 do
  begin
    dec(t);i:=0;
	fillchar(vis,sizeof(vis),0);
	while not eoln do
	  begin
	    inc(i);read(a[i]);
		if a[i]='W' then x:=i;
		a[i]:=upcase(a[i]);
	  end;
	n:=i;
	if solve() then writeln('yes')else writeln('no');
	readln;
  end;
close(input);close(output);
end.
var
n,p,ans:int64;
i:longint;
f:array[1..2000]of boolean;
a:array[0..2000]of int64;
procedure dfs(x,k:longint);
  var i:longint;
  begin
    if odd(k) then
	  if x<a[k-1] then exit;
	if not odd(k) then
	  if x>a[k-1] then exit;
	if k=n then
	  begin
	    ans:=(ans+1)mod p;
		exit;
	  end;
	f[x]:=true;a[k]:=x;
	for i:=1 to n do
	  if not f[i] then
		dfs(i,k+1);
	f[x]:=false;a[k]:=0;
  end;
begin
assign(input,'perm.in');reset(input);
assign(output,'perm.out');rewrite(output);
read(n,p);
for i:=n downto 1 do
  begin
    dfs(i,1);
  end;
write(ans);
close(input);close(output);
end.

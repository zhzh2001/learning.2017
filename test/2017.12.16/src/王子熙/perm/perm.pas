var
  n,m,a,b,c,d,i:longint;
begin
  assign(input,'perm.in');reset(input);
  assign(output,'perm.out');rewrite(output);
  readln(n,m);
  case n of
    1:d:=1;
    2:d:=1;
    3:d:=2;
    4:d:=5;
    5:d:=16;
    6:d:=64;
    7:d:=272;
    8:d:=1385;
    9:d:=7936;
  end;
  writeln(d mod m);
  close(input);close(output);
end.

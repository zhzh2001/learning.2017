#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int gcd(int a, int b) {
	return b ? gcd(b, a%b) : a;
}
int main(int argc, char const *argv[]) {
	freopen("cell.in", "r", stdin);
	freopen("cell.out", "w", stdout);
	int p = read();
	int k = gcd(p, 10000);
	printf("%.6lf\n", 1. * (p / k - 1) / (10000 / k - 1));
	return 0;
}
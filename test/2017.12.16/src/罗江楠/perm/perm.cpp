#include <bits/stdc++.h>
#define N 2020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
long long f[N], g[N], c[N][N];
int main(int argc, char const *argv[]) {
	// printf("%d\n", (sizeof(f)+sizeof(g)+sizeof(c))/1024/1024);
	freopen("perm.in", "r", stdin);
	freopen("perm.out", "w", stdout);
	f[0] = f[1] = f[2] = 1;
	g[0] = g[1] = g[2] = 1;
	int n = read(), p = read();
	for (int i = 0; i <= n; i++) {
		c[i][0] = 1;
		for (int j = 1; j <= i; j++)
			c[i][j] = (c[i-1][j] + c[i-1][j-1]) % p;
	}
	// for (int i = 1; i <= 4; i++)
	// 	printf("%d\n", c[4][i]);
	for (int i = 3; i <= n; i++) {
		if (i & 1) {
			for (int j = i - 2; j >= 1; j -= 2)
				g[i] = (g[i] + g[j] * g[i - 1 - j] * c[i-1][j]) % p;
		} else {
			for (int j = i - 1; j >= 1; j -= 2)
				g[i] = (g[i] + g[j] * g[i - 1 - j] * c[i-1][j]) % p;
		}
		// printf("%d\n", g[i]);
	}
	for (int i = 3; i <= n; i++) {
		if (i & 1) {
			for (int j = i - 1; j >= 0; j -= 2)
				f[i] = (f[i] + f[j] * g[i - 1 - j] * c[i-1][j]) % p;
		} else {
			for (int j = i - 2; j >= 0; j -= 2)
				f[i] = (f[i] + f[j] * g[i - 1 - j] * c[i-1][j]) % p;
		}
	}
	// for (int i = 1; i <= n; i++)
	// 	printf("%d ", g[i]); puts("");
	// for (int i = 1; i <= n; i++)
	// 	printf("%d ", f[i]); puts("");
	printf("%d\n", f[n]);
	return 0;
}
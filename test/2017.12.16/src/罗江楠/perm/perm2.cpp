#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[10020], n;
int check() {
	for (int i = 1; i <= n; i += 2)
		if (a[i - 1] > a[i] || a[i + 1] > a[i])
			return 0;
	// for (int i = 1; i <= n; i++)
	// 	printf("%d ", a[i]); puts("");
	return 1;
}
void work(int n, int p) {
	::n = n;
	for (int i = 1; i <= n; i++)
		a[i] = i;
	int res = 0;
	do {
		res += check();
		if (res >= p) res -= p;
	} while (next_permutation(a + 1, a + n + 1));
	printf("%d\n", res);
}
int main(int argc, char const *argv[]) {
	freopen("perm.in", "r", stdin);
	freopen("perm.ans", "w", stdout);
	int n = read();
	work(n, read());
	// for (int i = 1; i <= 10; i ++) {
	// 	printf("%d => ", i);
	// 	work(i, 1000000);
	// }
	// work(3);
	// work(4);
	// work(5);
	return 0;
}
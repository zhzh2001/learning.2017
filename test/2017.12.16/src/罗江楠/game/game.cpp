#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char ch[50020];
int main(int argc, char const *argv[]) {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int n = read();
	while (n --) {
		scanf("%s", ch + 1);
		int l = 1, r = strlen(ch + 1);
		while (l <= r) {
			if (ch[l] == ch[r]) {
				puts(ch[l] == 'b' ? "yes" : "no");
			} else if (ch[l] == 'W') {
				puts(ch[r] == 'b' ? "yes" : "no");
			} else if (ch[r] == 'W') {
				puts(ch[l] == 'b' ? "yes" : "no");
			} else {
				l ++, r --;
				continue;
			}
			break;
		}
		if (l > r) puts("no");
	}
	return 0;
}
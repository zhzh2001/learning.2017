#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int x[N], y[N], a[N], b[N];
bool cmpa(int a, int b) {
	return x[a] < x[b];
}
bool cmpb(int a, int b) {
	return y[a] < y[b];
}
int flag, fa[N];
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
void merge(int x, int y) {
	printf("MERGE %d %d\n", x, y);
	x = find(x); y = find(y);
	fa[x] = y; flag --;
}
int main(int argc, char const *argv[]) {
	int n = read(); flag = n - 1;
	for (int i = 1; i <= n; i++) {
		x[i] = read();
		y[i] = read();
		a[i] = b[i] = fa[i] = i;
	}
	sort(a + 1, a + n + 1, cmpa);
	sort(b + 1, b + n + 1, cmpb);
	int al = 1, ar = n, bl = 1, br = n, res = 0;
	while (flag) {
		int aa = al <= ar ? x[a[ar]] - x[a[al]] + abs(y[a[ar]] - y[a[al]]) : (- 1 << 30);
		int bb = bl <= br ? y[b[br]] - y[b[bl]] + abs(x[b[br]] - x[b[bl]]) : (- 1 << 30);
		printf("%d %d %d %d\n", al, ar, bl, br);
		if (aa > bb) {
			res += aa;
			merge(a[al], a[ar]);
			while (al < ar && find(a[al]) == find(a[ar])) {
				if (x[a[al + 1]] - x[a[al]] > x[a[ar]] - x[a[ar - 1]]) {
					ar --;
				} else {
					al ++;
				}
			}
		} else {
			res += bb;
			merge(b[bl], b[br]);
			while (bl < br && find(b[bl]) == find(b[br])) {
				if (y[b[bl + 1]] - y[b[bl]] > y[b[br]] - y[b[br - 1]]) {
					br --;
				} else {
					bl ++;
				}
			}
		}
	}
	printf("%d\n", res);
	return 0;
}
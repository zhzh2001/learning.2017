#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int x[N], y[N];
int fa[N];
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
void merge(int x, int y) {
	printf("MERGE %d %d\n", x, y);
	x = find(x); y = find(y);
	fa[x] = y;
}
struct node {
	int st, ed, len;
}a[N];
int cnt = 0;
bool cmp(node a, node b) {
	return a.len > b.len;
}
int main(int argc, char const *argv[]) {
	int n = read();
	for (int i = 1; i <= n; i++) {
		x[i] = read();
		y[i] = read();
		fa[i] = i;
	}
	int res = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			a[++cnt] = (node) {i, j, abs(x[i] - x[j]) + abs(y[i] - y[j])};
	sort(a + 1, a + cnt + 1, cmp);
	for (int i = 1; i <= cnt; i++) {
		if (find(a[i].st) == find(a[i].ed))
			continue;
		res += a[i].len;
		merge(a[i].st, a[i].ed);
	}
	printf("%d\n", res);
	return 0;
}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

const int N = 2e3 + 5;

int n, p, a[N], ans1;

inline bool Check() {
	bool flag = true;
	for (int i = 2; i <= n; ++i) {
		if (i & 1) {
			flag &= (a[i] > a[i - 1]);
		} else {
			flag &= (a[i] < a[i - 1]);
		}
	}
	return flag;
}

int main() {
	freopen("perm.in", "r", stdin);
	freopen("perm.out", "w", stdout);
	scanf("%d%d", &n, &p);
	if (n <= 10) {
		for (int i = 1; i <= n; ++i)
			a[i] = i;
		do {
			ans1 += Check();
		} while (next_permutation(a + 1, a + n + 1));
		printf("%d\n", ans1 % p);
	}
	return 0;
}
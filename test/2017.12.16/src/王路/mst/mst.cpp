#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <fstream>
using namespace std;

const int N = 1e5 + 5, Inf = 0x3f3f3f3f;

typedef pair<long long, long long> pii;

int n;
pii a[N], b[N], lu, ld, ru, rd;

struct UFSet {
	int fa[N];
	inline void Init() {
		for (int i = 0; i < N; ++i)
			fa[i] = i;
	}
	inline int Find(int x) { return x == fa[x] ? x : fa[x] = Find(fa[x]); }
	inline void Merge(int x, int y) {
		int fx = Find(x), fy = Find(y);
		if (fx != fy)
			fa[fx] = fy;
	}
	inline bool Same(int x, int y) {
		return Find(x) == Find(y);
	}
} uf;

struct Edge {
	int u, v;
	long long w;
} e[1000005];

inline bool cmp(const Edge &a, const Edge &b) {
	return a.w > b.w;
}

int cnt = 0;

long long GetDist(int i, int j) {
	return abs(a[i].first - a[j].first) + abs(a[i].second - a[j].second);
}

int Solve1() {
	for (int i = 1; i <= n; ++i)
		for (int j = i + 1; j <= n; ++j) {
			e[++cnt] = (Edge) { i, j, GetDist(i, j) };
		}
	long long ret = 0;
	sort(e + 1, e + cnt + 1, cmp);
	int nc = 0;
	for (int i = 1; i <= cnt; ++i) {
		int fx = uf.Find(e[i].u), fy = uf.Find(e[i].v);
		if (fx != fy) {
			uf.fa[fx] = fy;
			ret += e[i].w;
			if (++nc == n - 1)
				break;
		}
	}
	printf("%lld\n", ret);
	return 0;
}

int Solve2() {
	for (int i = 1; i <= n; ++i) {
		b[i] = make_pair(a[i].first + a[i].second, a[i].first - a[i].second);
	}
	sort(b + 1, b + n + 1);
	long long ret = max(abs(b[n].first - b[1].first), abs(b[n].second - b[1].second));
	for (int i = 2; i < n; ++i) {
		ret += max(max(abs(b[i].second - b[1].second), abs(b[i].first - b[1].first)), max(abs(b[n].first - b[i].first), abs(b[n].second - b[i].second)));
	}
	printf("%lld\n", ret);
	return 0;
}

int main() {
	freopen("mst.in", "r", stdin);
	freopen("mst.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; ++i) {
		scanf("%lld%lld", &a[i].first, &a[i].second);
		a[i].first += Inf;
		a[i].second += Inf;
	}
	uf.Init();
	if (n <= 1000) {
		return Solve1();
	}
	return Solve2();
}
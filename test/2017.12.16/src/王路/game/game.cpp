#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

const int N = 5e3 + 5;
char str[N];

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int T;
	scanf("%d", &T);
	for (int i = 1; i <= T; ++i) {
		scanf("%s", str + 1);
		int l = 1, r = strlen(str + 1);
		bool ans = false;
		for (; l <= r; ) {
			if (str[l] == 'W' || str[r] == 'W') {
				if (str[l] == 'W') {
					swap(str[l], str[r]);
				}
				ans |= (str[l] == 'b');
				break;
			}
			if (str[l] + str[r] == 'w' + 'b') {
				++l;
				--r;
			} else {
				ans |= (str[l] == 'b');
				break;
			}
		}
		printf("%s\n", ans ? "yes" : "no");
	}
	return 0;
}
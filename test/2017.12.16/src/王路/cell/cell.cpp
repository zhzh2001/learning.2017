#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

const int N = 10005;
const long double eps = 1e-10;

int p;
long double bi[N];

int main() {
	freopen("cell.in", "r", stdin);
	freopen("cell.out", "w", stdout);
	scanf("%d", &p);
	bi[0] = 1;
	for (int i = 1; i < N; ++i)
		bi[i] = bi[i - 1] * 2.0;
	double P = (double)p / 10000.0, pp = 1.0 - P, res = 1.0 - P, tmp;
	bool flag = false;
	for (int i = 1; ; ++i) {
		tmp = res;
		pp *= P;
		for (int j = 1; j <= i; ++j) {
			res += powl(pp, bi[j]);
		}
		if (res < tmp + eps) {
			break;
		}
	}
	printf("%.6lf\n", 1.0 - res);
	return 0;
}
var
b:boolean;
t,i,j,n:longint;
s:ansistring;
begin
assign(input,'game.in');
reset(input);
assign(output,'game.out');
rewrite(output);
readln(t);
for i:=1 to t do
  begin
    readln(s);
    n:=length(s);
    b:=true;
    for j:=1 to n do
      if s[j]='b' then
        b:=false;
    if b then
      begin
        writeln('no');
        continue;
      end;
    if ((s[1]='w') or (s[1]='W')) and ((s[n]='w') or (s[n]='W')) then
      begin
        writeln('no');
        continue;
      end;
    if (s[1]='b') and (s[n]='b') then
      begin
        writeln('yes');
        continue;
      end;
    if (s[1]='W') or (s[n]='W') then
      begin
        writeln('yes');
        continue;
      end;
    if (s[2]='b') and (s[n-1]='b') then
      begin
        writeln('yes');
        continue;
      end;
    if ((s[2]='w') or (s[2]='W')) and ((s[n-1]='w') or (s[n-1]='W')) then
      begin
        writeln('no');
        continue;
      end;
    if (s[2]='W') or (s[n-1]='W') then
      begin
        writeln('yes');
        continue;
      end;
    writeln('no');
  end;
close(input);
close(output);
end.
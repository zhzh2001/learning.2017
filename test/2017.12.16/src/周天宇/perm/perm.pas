var
n,p,i,j,ans:longint;
a:array[0..2000] of longint;
b:array[0..2000] of boolean;

procedure check();
var
i:longint;
begin
for i:=1 to n do
  if i mod 2=1 then
    if (a[i]<a[i-1]) or (a[i]<a[i+1]) then
      exit;
ans:=(ans+1) mod p;
end;

procedure dfs(k:longint);
var
i:longint;
begin
if k>n then
  begin
    check;
    exit;
  end;
for i:=1 to n do
  if not b[i] then
    begin
      a[k]:=i;
      if k mod 2=0 then
        if a[k]>a[k-1] then
          continue;
      if k mod 2 =1 then
        if a[k]<a[k-1] then
          continue;
      b[i]:=true;
      dfs(k+1);
      b[i]:=false;
    end;
end;

begin
assign(input,'perm.in');
reset(input);
assign(output,'perm.out');
rewrite(output);
readln(n,p);
dfs(1);
writeln(ans);
close(input);
close(output);
end.
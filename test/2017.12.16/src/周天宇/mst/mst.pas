type
p=record
  a,x,y:longint;
  end;

var
t:p;
n,i,j,f1,f2,ans:longint;
x,y,fa:array[1..1000] of longint;
f:Array[1..1000000] of p;

function find(a:longint):longint;
begin
if fa[a]=a then
  exit(a);
find:=find(fa[a]);
fa[a]:=find;
end;

begin
assign(input,'mst.in');
reset(input);
assign(output,'mst.out');
rewrite(output);
readln(n);
for i:=1 to n do
  read(x[i],y[i]);
for i:=1 to n do
  fa[i]:=i;
for i:=1 to n do
  for j:=i+1 to n do
    begin
      f[(i-1)*n+j-1].a:=abs(x[i]-x[j])+abs(y[i]-y[j]);
      f[(i-1)*n+j-1].x:=i;
      f[(i-1)*n+j-1].y:=j;
    end;
for i:=1 to n*n do
  for j:=i+1 to n*n do
    if f[i].a<f[j].a then
      begin
        t:=f[i];
        f[i]:=f[j];
        f[j]:=t;
      end;
j:=1;
for i:=1 to n-1 do
  begin
    while true do
      begin
        f1:=find(f[j].x);
        f2:=find(f[j].y);
        if f1<>f2 then
          begin
            fa[f1]:=f2;
            inc(ans,f[j].a);
            inc(j);
            break;
          end;
        inc(j);
      end;
  end;
writeln(ans);
close(input);
close(output);
end.
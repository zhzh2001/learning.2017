var
  s:ansistring;
  p,t,x,w,b,i:longint;
begin
  assign(input,'game.in');
  assign(output,'game.out');
  reset(input);
  rewrite(output);
  readln(t);
  for p:=1 to t do
    begin
      readln(s);
      x:=pos('W',s);
      s:=copy(s,1,x-1)+'w'+copy(s,x+1,length(s));
      if (s[1]='b')and(s[length(s)]='b') then
        begin writeln('yes');continue;end;
      if (s[1]='w')and(s[length(s)]='w') then
        begin writeln('no');continue;end;
      if (x=1)or(x=length(s)) then
        begin writeln('yes');continue;end;
      w:=1;b:=1;
      if s[1]='b'
      then while s[b+1]='b' do inc(b)
      else while s[w+1]='w' do inc(w);
      if s[length(s)]>'w' then while s[length(s)-w]='w' do inc(w)
                          else while s[length(s)-b]='b' do inc(b);
      if w>b then writeln('no')
             else writeln('yes');
    end;
  close(input);
  close(output);
end.

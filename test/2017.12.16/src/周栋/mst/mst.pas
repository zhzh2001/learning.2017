var
  x,y,dist:array[0..100000] of longint;
  vis:array[0..100000] of boolean;
  i,j,k,kk,n,ans:longint;
begin
  assign(input,'mst.in');
  assign(output,'mst.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
    readln(x[i],y[i]);
  vis[1]:=true;
  k:=2;
  for i:=2 to n do
    begin
      dist[i]:=abs(x[i]-x[1])+abs(y[i]-y[1]);
      if dist[i]>dist[k] then k:=i;
    end;
  for i:=1 to n-1 do
    begin
      ans:=ans+dist[k];
      vis[k]:=true;
      kk:=0;
      for j:=1 to n do
        begin
          if (kk=0)and not vis[j] then kk:=j;
          if dist[j]<abs(x[j]-x[k])+abs(y[j]-y[k])
            then dist[j]:=abs(x[j]-x[k])+abs(y[j]-y[k]);
          if (dist[j]>dist[kk])and not vis[j] then kk:=j;
        end;
      k:=kk;
    end;
  writeln(ans);
  close(input);
  close(output);
end.
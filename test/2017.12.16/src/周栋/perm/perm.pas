var
  n,ans,m:longint;
  a:array[0..1000] of longint;
  b:array[0..1000] of boolean;
procedure dfs(k:longint);
  var
    i:longint;
  begin
    if k>n then
      begin
        inc(ans);
        exit;
      end;
    if odd(k) then
      for i:=a[k-1] to n do
        if not b[i]
        then
          begin
            a[k]:=i;
            b[i]:=true;
            dfs(k+1);
            b[i]:=false;
          end;
    if not odd(k) then
      for i:=1 to a[k-1] do
      if not b[i]
        then
          begin
            a[k]:=i;
            b[i]:=true;
            dfs(k+1);
            b[i]:=false;
          end;
  end;
begin
  assign(input,'perm.in');
  assign(output,'perm.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  dfs(1);
  writeln(ans mod m);
  close(input);
  close(output);
end.
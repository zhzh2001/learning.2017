#include <fstream>
#include <string>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		string s;
		fin >> s;
		int l = 0, r = s.length() - 1;
		bool ans = false;
		while (l < r)
		{
			if (s[l] == 'W')
			{
				ans = s[r] == 'b';
				break;
			}
			if (s[r] == 'W' || s[l] == s[r])
			{
				ans = s[l] == 'b';
				break;
			}
			l++;
			r--;
		}
		fout << (ans ? "yes\n" : "no\n");
	}
	return 0;
}
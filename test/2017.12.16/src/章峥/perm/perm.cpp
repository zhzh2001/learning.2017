#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("perm.in");
ofstream fout("perm.out");
const int N = 2005;
int f[N][N];
int main()
{
	int n, p;
	fin >> n >> p;
	f[1][1] = 1;
	for (int i = 2; i <= n; i++)
		for (int j = 1; j <= i; j++)
			if (i & 1)
				f[i][j] = (f[i][j - 1] + f[i - 1][j - 1]) % p;
			else
				f[i][j] = ((f[i][j - 1] + f[i - 1][i - 1] - f[i - 1][j - 1]) % p + p) % p;
	fout << f[n][n] << endl;
	return 0;
}
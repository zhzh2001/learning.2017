#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("mst.in");
ofstream fout("mst.ans");
const int N = 1005;
long long x[N], y[N];
int f[N];
struct edge
{
	int u, v;
	long long w;
	bool operator<(const edge &rhs) const
	{
		return w > rhs.w;
	}
} e[N * N / 2];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> x[i] >> y[i];
		f[i] = i;
	}
	int m = 0;
	for (int i = 1; i <= n; i++)
		for (int j = i + 1; j <= n; j++)
		{
			e[++m].u = i;
			e[m].v = j;
			e[m].w = abs(x[i] - x[j]) + abs(y[i] - y[j]);
		}
	sort(e + 1, e + m + 1);
	long long ans = 0;
	int now = 0;
	for (int i = 1; i <= m; i++)
	{
		int ru = getf(e[i].u), rv = getf(e[i].v);
		if (ru != rv)
		{
			f[ru] = rv;
			ans += e[i].w;
			if (++now == n - 1)
				break;
		}
	}
	fout << ans << endl;
	return 0;
}
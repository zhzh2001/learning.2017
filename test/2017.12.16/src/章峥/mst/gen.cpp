#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("mst.in");
const int n = 100000, v = 1e9;
int main()
{
	minstd_rand gen(GetTickCount());
	fout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> dv(-v, v);
		fout << dv(gen) << ' ' << dv(gen) << endl;
	}
	return 0;
}
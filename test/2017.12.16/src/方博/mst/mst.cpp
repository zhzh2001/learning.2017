#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int N=100005;
struct xxx{ll x,y,id;}a[N];
struct edge{ll x,y,t;}q[20*N];
ll n,ans,tot;
bool com(edge a,edge b){return a.t>b.t;}
bool com1(xxx a,xxx b){return a.x<b.x;}
bool com2(xxx a,xxx b){return a.y<b.y;}
bool com3(xxx a,xxx b){return a.x+a.y<b.x+b.y;}
void addto(ll x,ll y,ll t){q[++tot]=(edge){x,y,t};}
int f[N];
int gf(int k){if(f[k]!=k)f[k]=gf(f[k]);return f[k];}
int main()
{
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){a[i].x=read();a[i].y=read();a[i].id=i;}
	/*for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			addto(i,j,abs(a[i].x-a[j].x)+abs(a[i].y-a[j].y));*/
	sort(a+1,a+n+1,com1);
	addto(a[1].id,a[n].id,abs(a[n].x-a[1].x)+abs(a[n].y-a[1].y));
	for(int i=2;i<=n-1;i++){
		addto(a[i].id,a[1].id,abs(a[i].x-a[1].x)+abs(a[i].y-a[1].y));
		addto(a[i].id,a[n].id,abs(a[i].x-a[n].x)+abs(a[i].y-a[n].y));
	}
	
	sort(a+1,a+n+1,com2);
	addto(a[1].id,a[n].id,abs(a[n].x-a[1].x)+abs(a[n].y-a[1].y));
	for(int i=2;i<=n-1;i++){
		addto(a[i].id,a[1].id,abs(a[i].x-a[1].x)+abs(a[i].y-a[1].y));
		addto(a[i].id,a[n].id,abs(a[i].x-a[n].x)+abs(a[i].y-a[n].y));
	}
	
	sort(a+1,a+n+1,com3);
	addto(a[1].id,a[n].id,abs(a[n].x-a[1].x)+abs(a[n].y-a[1].y));
	for(int i=2;i<=n-1;i++){
		addto(a[i].id,a[1].id,abs(a[i].x-a[1].x)+abs(a[i].y-a[1].y));
		addto(a[i].id,a[n].id,abs(a[i].x-a[n].x)+abs(a[i].y-a[n].y));
		a[i].x=-a[i].x;
	}a[1].x=-a[1].x,a[n].x=-a[n].x;
	
	sort(a+1,a+n+1,com3);
	addto(a[1].id,a[n].id,abs(a[n].x-a[1].x)+abs(a[n].y-a[1].y));
	for(int i=2;i<=n-1;i++){
		addto(a[i].id,a[1].id,abs(a[i].x-a[1].x)+abs(a[i].y-a[1].y));
		addto(a[i].id,a[n].id,abs(a[i].x-a[n].x)+abs(a[i].y-a[n].y));
		a[i].y=-a[i].y;
	}a[1].y=-a[1].y,a[n].y=-a[n].y;
	
/*	sort(a+1,a+n+1,com3);
	addto(a[1].id,a[n].id,abs(a[n].x-a[1].x)+abs(a[n].y-a[1].y));
	for(int i=2;i<=n-1;i++){
		addto(a[i].id,a[1].id,abs(a[i].x-a[1].x)+abs(a[i].y-a[1].y));
		addto(a[i].id,a[n].id,abs(a[i].x-a[n].x)+abs(a[i].y-a[n].y));
		a[i].x=-a[i].x;
	}a[1].x=-a[1].x,a[n].x=-a[n].x;
	
	sort(a+1,a+n+1,com3);
	addto(a[1].id,a[n].id,abs(a[n].x-a[1].x)+abs(a[n].y-a[1].y));
	for(int i=2;i<=n-1;i++){
		addto(a[i].id,a[1].id,abs(a[i].x-a[1].x)+abs(a[i].y-a[1].y));
		addto(a[i].id,a[n].id,abs(a[i].x-a[n].x)+abs(a[i].y-a[n].y));
		a[i].y=-a[i].y;
	}a[1].y=-a[1].y,a[n].y=-a[n].y;*/
	
	for(int i=1;i<=n;i++)
		f[i]=i;
	sort(q+1,q+tot+1,com);
	for(int i=1;i<=tot;i++){
		int fa=gf(q[i].x);
		int fb=gf(q[i].y);
		if(fa!=fb){
			ans+=q[i].t;
			f[fa]=fb;
		}
	}
	writeln(ans);
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
const double eps=1e-9;
double a[209][209];
double f[209],g[209];
ll c[66][66];
double di[66],li[66];
int n;
int main()
{
	freopen("cell.in","r",stdin);
	freopen("cell.out","w",stdout);
	scanf("%d",&n);double t=1.0*n/10000;
	g[0]=1.0-t;g[2]=t;
	
	di[0]=1;li[0]=1;
	for(int i=1;i<=65;i++)
		di[i]=di[i-1]*(1.0-t),li[i]=li[i-1]*t;
		
	c[0][0]=1;
	for(int i=1;i<=65;i++){
		c[i][0]=1;
		for(int j=1;j<=i;j++)
			c[i][j]=c[i-1][j]+c[i-1][j-1];
	}
	
	for(int i=1;i<=65;i++){
		if(i%2==0||i==1)
		for(int j=0;j<=i;j++){
			a[i][j*2]=li[j]*di[i-j]*c[i][j];
		}
	}
	a[0][0]=1;
	
	for(int i=1;i<=500;i++){
		for(int j=0;j<=65;j++)
			if(j%2==0||j==1)
			for(int k=0;k<=65;k++){
				f[k]+=g[j]*a[j][k];
			}
		for(int k=0;k<=65;k++)
			g[k]=f[k],f[k]=0;
	}
	printf("%.6lf\n",1.0-g[0]);
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;

int n,x[10000],y[10000],s[10000];
long long d[10000];

bool cmp(int x,int y)
{
	return x>y;
}

int main()
{
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d%d",&x[i],&y[i]);
	int k=1;
	for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
			s[k++]=abs(x[i]-x[j])+abs(y[i]-y[j]);
	sort(s+1,s+k+1,cmp);
	int ans=0;
	for (int i=1;i<n;i++) ans+=s[i];
	printf("%d",ans);
	return 0;
}


#include<bits/stdc++.h>

int n;
long long a[1000],f[1000],ans,p;

void dfs(int x)
{
	if (x>n) {
		ans++;
		return;
	}
	for (int i=1;i<=n;i++)
	if (f[i]==0)
	{
		if (x%2==1 && i<a[x-1]) continue;
		if (x%2==0 && i>a[x-1]) continue;
		a[x]=i;
		f[i]=1;
		dfs(x+1);
		f[i]=0;
	}
}

int main()
{
	freopen("perm.in","r",stdin);
	freopen("perm.out","w",stdout);
	scanf("%d%d",&n,&p);
	ans=0;
	dfs(1);
	printf("%d",ans);
	return 0;
}

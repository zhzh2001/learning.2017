#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Dow(i,b,a) for (int i=b;i>=a;i--)
using namespace std;
inline int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline void write(int x) {
	if(x<0) x=-x,putchar('-'); 
	if(x>9) write(x/10); 
	putchar(x%10+48); 
}
inline void writeln(int x) {
	write(x); 
	putchar('\n'); 
}

const int N = 2011; 
int n,mod,ans; 
int t[N],f[N]; 
inline void dfs(int x) {
	if( x>n ) {
		For(i, 1, n) 
			if(i&1) 
			  if(t[i] < t[i-1] || t[i]<t[i+1]) return; 
		++ans;
		if(ans==mod) ans = 0; 
		return; 
	}
	For(i, 1, n) 
		if(!f[ i ]) {
			t[ x ] = i;
			f[ i ] = 1;
			dfs(x+1); 
			f[ i ] = 0; 
		}
}

int main()
{
	freopen("perm.in","r",stdin); 
	freopen("perm.out","w",stdout); 
	n = read(); mod = read() ; 
	if(mod==1) {
		putchar('0');
		return 0; 
	}
	dfs(1); 
	writeln(ans); 
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Dow(i,b,a) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}

int T; 
char s[5011];

int main()
{	
	freopen("game.in","r",stdin); 
	freopen("game.out","w",stdout); 
	T = read(); 
	while(T--) {
		scanf("%s",s+1); 
		int l = strlen(s+1); 
		For(i, 1, l) if(s[i]=='W') s[i]='w'; 
		if(s[1]==s[l]) {
			if( s[1]=='w' ) puts("no"); 
			else puts("yes"); 
			continue; 
		}
		puts("yes"); 
	}
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Dow(i,b,a) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline int gcd(int x,int y) {
	if(!y) return x; 
	return gcd(y,x%y); 
}

const int alpha = 4000000; 
int x,y;
double t,mul,ans;

int main(){
	freopen("cell.in","r",stdin); 
	freopen("cell.out","w",stdout); 
	x = read(); 
	y = 10000; 
	if(x>=10000) {
		ans = 1.0; 
		printf("%.6lf\n",ans); 
		return 0; 
	}
	
	int res = gcd(x,y); 
	x/=res; y/=res; 
	
	
	t = 1.0*(y-x) / y; 
	mul = 1.0*(y-x) / y; 
	
	ans = 1.0 - t; 
	For(i, 2, alpha) {
		t = t*mul; 
		ans = ans - t;
		if(ans<0) {
			ans = 0.0; 
			printf("%.6lf\n",ans); 
			return 0; 
		}
	}  
	printf("%.6lf\n",ans); 
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Dow(i,b,a) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}

const int N = 1e5+11; 
struct node{
	int x,y,id;
	bool used;  
}a[N];
ll ans;
struct edge{
	int val,x,y; 
}e[16000000];
int fa[N]; 
int cnt,nedge;
int n;
inline bool cmp_x(node a,node b) {
	return a.x < b.x; 
}
inline bool cmp_y(node a,node b) {
	return a.x < b.x; 
}
inline bool cmp_used(node a,node b) {
	return a.used > b.used; 
}
inline bool cmp_val(edge a,edge b) {
	return a.val > b.val; 
}
inline int find(int x) {
	if( x==fa[x] ) return x; 
	return fa[x] = find(fa[x]); 
}
inline void calc(int cnt) {
	For(i, 1, cnt-1) 
	  For(j, i+1, cnt) e[++nedge].val = abs(a[i].x-a[j].x) + abs(a[i].y-a[j].y), 
	  				e[nedge].x = i, e[nedge].y = j; 
	sort(e+1, e+nedge+1, cmp_val); 
	For(i, 1, cnt) fa[i] = i; 
	
	For(i, 1, nedge) {
		int x = find(e[i].x); 
		int y = find(e[i].y); 
		if( x!=y ) {
			ans = ans+e[i].val; 
			fa[ y ] = x;
		}
	}
}
int main()
{
	freopen("mst.in","r",stdin); 
	freopen("mst.out","w",stdout); 
	n = read(); 
	For(i, 1, n) {
		a[i].x = read(); a[i].y = read(); 
		a[i].id = i; 
	}
	if(n<=4000) {
		calc( n ); 
		printf("%lld\n",ans); 
		return 0; 
	}
	sort(a+1, a+n+1, cmp_x); 
	cnt = 0; 
	For(i, 1, n) 
		if(a[i].x == a[1].x) ++cnt; 
		else break; 
	sort(a+1, a+cnt+1, cmp_y); 
	a[1].used = 1; a[cnt].used = 1; 
	
	cnt = 0; 
	Dow(i, n, 1) 
		if(a[i].x == a[n].x ) ++cnt; 
		else break;
	sort(a+n-cnt+1, a+n+1, cmp_y); 
	a[n-cnt+1].used = 1; a[n].used = 1; 
	
	
	
	sort(a+1, a+n+1, cmp_y); 
	cnt = 0; 
	For(i, 1, n) 
		if(a[i].y == a[1].y) ++cnt; 
		else break; 
	sort(a+1, a+cnt+1, cmp_x); 
	a[1].used = 1; a[cnt].used = 1; 
	
	cnt = 0; 
	Dow(i, n, 1) 
		if(a[i].y == a[n].y ) ++cnt; 
		else break;
	sort(a+n-cnt+1, a+n+1, cmp_x); 
	a[n-cnt+1].used = 1; a[n].used = 1; 
	
	sort(a+1, a+n+1, cmp_used); 
	cnt = 0; 
	For(i, 1, n) if(a[i].used) cnt++; 
		else break; 
	calc( cnt ); 
	
	For(i, cnt+1, n)  {
		int Mx = 0; 
		For(j, 1, cnt) {
			int tt = abs(a[i].x-a[j].x) + abs(a[i].y-a[j].y); 
			if(tt > Mx) Mx = tt; 
		}
		ans = ans+Mx; 
	}
	printf("%lld\n",ans); 
	return 0;
}




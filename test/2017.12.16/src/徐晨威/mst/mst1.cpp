#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,x[100005],y[100005],lwc[100005],ans;
bool vis[100005];
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;ch<'0'||'9'<ch;ch=getchar()) f^=!(ch^'-');
	for(;ch>='0'&&ch<='9';ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return f?ret:-ret;
}
int ads(int x){return x<0?-x:x;}
int get(int i,int j){return ads(x[i]-x[j])+ads(y[i]-y[j]);}
int main(){
	freopen("mst.in","r",stdin);
	freopen("mst1.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) x[i]=read(),y[i]=read();
	vis[1]=1;
	for(int i=2;i<=n;i++) lwc[i]=get(1,i);
	for(int i=1;i<n;i++){
		int k=0,maxn=0;
		for(int j=1;j<=n;j++)
		if(!vis[j]&&lwc[j]>maxn) maxn=lwc[j],k=j;
		ans+=maxn;vis[k]=1;
		for(int j=1;j<=n;j++)
		if(!vis[j]&&lwc[j]<get(k,j)) lwc[j]=get(k,j);
	}
	printf("%d\n",ans);
	return 0;
}

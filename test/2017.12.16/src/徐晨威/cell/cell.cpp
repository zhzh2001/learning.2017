#include<cstdio>
#include<iostream>
#define MAXN 100005
using namespace std;
double n,ans,nown=1;
int main(){
	freopen("cell.in","r",stdin);
	freopen("cell.out","w",stdout);
	cin>>n;n/=10000;
	if(n==0.5) printf("%.6lf\n",0.5);else
	if(n>0.5){
		for(;;){
			double lst=ans;
			ans+=(nown*=1.0-n);
			if(ans-lst<1e-7) break;
		}
		printf("%.6lf\n",1.0-ans);
	}
	else{
		ans=n;nown=n;
		for(;;){
			double lst=ans;
			ans-=(nown*=n);
			if(lst-ans<1e-7) break;
		}
		printf("%.6lf\n",ans);
	}
	return 0;
}

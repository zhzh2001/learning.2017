#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100005
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,p,ans,a[N],b[N];
void judge(){
	for (ll i=1;i<=n;i+=2)
		if (a[i]<a[i-1]||a[i]<a[i+1]) return;
	++ans;
}
void dfs(ll now){
	if (now==n+1) { judge(); return; }
	rep(i,1,n){
		if (b[i]) continue;
		if (!(now&1)&&a[now-1]<i) continue;
		a[now]=i; b[i]=1; dfs(now+1); b[i]=0;
	}
}
ll f[N];
int main(){
	f[1]=1;
	f[2]=1;
	f[3]=2;
	f[4]=5;
	f[5]=16;
	f[6]=61;
	f[7]=272;
	f[8]=1385;
	f[9]=7936;
	f[10]=50521;
	f[11]=353792;
	f[12]=2702765;
	f[13]=22368256;
	f[14]=199360981;
	freopen("perm.in","r",stdin);
	freopen("perm.out","w",stdout);
	n=read(); p=read();
	printf("%lld",f[n]%p);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100005
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,ans,tot,cnt,fa[N];
struct data{ ll x,y,id; }a[N];
struct edge{ ll u,v,w; }e[N<<2];
bool cmp1(data x,data y) { return x.x<y.x; }
bool cmp2(data x,data y) { return x.y<y.y; }
bool cmp(edge x,edge y) { return x.w>y.w; }
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
int main(){
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	n=read();
	rep(i,1,n){
		ll x=read(),y=read();
		a[i]=(data){x+y,x-y,i};
	}
	sort(a+1,a+1+n,cmp1);
	rep(i,2,n){
		e[++tot]=(edge){a[1].id,a[i].id,a[i].x-a[1].x};
	}
	per(i,n-1,1){
		e[++tot]=(edge){a[n].id,a[i].id,a[n].x-a[i].x};
	}
	sort(a+1,a+1+n,cmp2);
	rep(i,2,n){
		e[++tot]=(edge){a[1].id,a[i].id,a[i].y-a[1].y};
	}
	per(i,n-1,1){
		e[++tot]=(edge){a[n].id,a[i].id,a[n].y-a[i].y};
	}
	sort(e+1,e+1+tot,cmp);
	rep(i,1,n) fa[i]=i;
	rep(i,1,tot){
		ll u=e[i].u,v=e[i].v;
		ll p=find(u),q=find(v);
		if (p!=q){
			fa[p]=q; ans+=e[i].w;
			++cnt;
		}
		if (cnt==n-1) break;
	}
	printf("%lld",ans);
}

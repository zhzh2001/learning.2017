#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int a[2010];
bool f[2010];
int n;
ll ans,mo;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x)
{
	if(x==n+1)
	{
		ans++;	
		ans%=mo;
		return;
	}
	if(x%2==0)
	{
		For(i,1,a[x-1])if(f[i]==false)
		{
			f[i]=true;
			a[x]=i;
			dfs(x+1);
			f[i]=false;
		}
	}
	else
	{
		For(i,a[x-1]+1,n)if(f[i]==false)
		{
			f[i]=true;
			a[x]=i;
			dfs(x+1);
			f[i]=false;
		}
	}
}
int main()
{
	freopen("perm.in","r",stdin);
	freopen("perm.out","w",stdout);
	n=read();mo=read();
	memset(f,false,sizeof(f));
	dfs(1);
	cout<<ans<<endl;
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define N 1000005
#define M 5000005
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
struct D{
	int x,y,z,id;
}e[M];
bool cmp(const D &x,const D &y){return x.z<y.z;}
int f[N],choose[M],a[100000],b[100000],jzq;
int find(int x)
{
	if (f[x]==x)return x;
	else return f[x]=find(f[x]);
}
int main()
{
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	int n,m;ll ans=0;
	n=read();
	For(i,1,n)a[i]=read(),b[i]=read();
	jzq=0;
	For(i,1,n)
	{
		For(j,i+1,n)
		{
			jzq++;
			e[jzq].x=i;
			e[jzq].y=j;
			e[jzq].z=abs(a[i]-a[j])+abs(b[i]-b[j]);
			e[jzq].id=jzq;
		}
	}
	sort(e+1,e+1+jzq,cmp);
	For(i,1,n)f[i]=i;
	Rep(i,1,jzq)
	{
		int f1=find(e[i].x),f2=find(e[i].y);
		if (f1!=f2)choose[e[i].id]=1,f[f1]=f2,ans+=e[i].z;
		else choose[e[i].id]=0;
	}
	cout<<ans<<endl;
	return 0;
}



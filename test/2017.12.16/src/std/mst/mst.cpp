#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=100000+19;

int x[N],y[N],mxx[N],mxy[N],mnx[N],mny[N],fa[N],pt[N],val[N];
int n,a,b,mxx1,mxx2,mxy1,mxy2,mnx1,mnx2,mny1,mny2,tmp;
ll res;

int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
void Union(int x,int y){
	x=getf(x),y=getf(y);
	if (x!=y){
		fa[x]=y;
		res+=val[x];
		mxx[y]=max(mxx[y],mxx[x]);
		mnx[y]=min(mnx[y],mnx[x]);
		mxy[y]=max(mxy[y],mxy[x]);
		mny[y]=min(mny[y],mny[x]);
		tmp--;
	}
}
void Upmx(int *A,int x,int &y,int &z){
	if (!y||A[x]>A[y]) z=y,y=x;else
	if (!z||A[x]>A[z]) z=x;
}
void Upmn(int *A,int x,int &y,int &z){
	if (!y||A[x]<A[y]) z=y,y=x;else
	if (!z||A[x]<A[z]) z=x;
}
void Upd(int i,int v,int *A,int x){
	int tmp=abs(v-A[x]);
	if (tmp>val[i]) val[i]=tmp,pt[i]=x;
}
void Boruvka(){
	For(i,1,n+1){
		fa[i]=i;
		mxx[i]=mnx[i]=x[i];
		mxy[i]=mny[i]=y[i];
	}
	for (tmp=n;tmp>1;){
		mxx1=mxx2=0;
		mxy1=mxy2=0;
		mnx1=mnx2=0;
		mny1=mny2=0;
		For(i,1,n+1) if (fa[i]==i){
			Upmx(mxx,i,mxx1,mxx2);
			Upmx(mxy,i,mxy1,mxy2);
			Upmn(mnx,i,mnx1,mnx2);
			Upmn(mny,i,mny1,mny2);
			val[i]=-1;
		}
		For(i,1,n+1) if (fa[i]==i){
			Upd(i,mnx[i],mxx,mxx1==i?mxx2:mxx1);
			Upd(i,mxx[i],mnx,mnx1==i?mnx2:mnx1);
			Upd(i,mny[i],mxy,mxy1==i?mxy2:mxy1);
			Upd(i,mxy[i],mny,mny1==i?mny2:mny1);
		}
		For(i,1,n+1) if (fa[i]==i){
			Union(i,pt[i]);
		}
	}
}

int main(){
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	n=IN();
	For(i,1,n+1){
		a=IN(),b=IN();
		assert(a>=-int(1e9)&&a<=int(1e9));
		assert(b>=-int(1e9)&&b<=int(1e9));
		x[i]=a+b;
		y[i]=a-b;
	}
	Boruvka();
	cout<<res<<endl;
}


#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <math.h>
using namespace std;
typedef long long LL;
int n,f[1003];
LL x[100003],y[100003],et=0,ans=0;
struct edge{LL a,b,c;edge(){}edge(LL x,LL y,LL z){a=x;b=y;c=z;}}e[1003*1003],a[100003];
bool cmp1(edge a,edge b){return a.a<b.a;}
bool cmp2(edge a,edge b){return a.b<b.b;}
bool operator<(edge a,edge b){return a.c>b.c;}
void init(){for(int i=1;i<=n;i++)f[i]=i;}
int getf(int u){return f[u]==u?f[u]:f[u]=getf(f[u]);}
void add(int a,int b){f[getf(a)]=getf(b);}
LL d(int i,int j){return abs(a[i].a-a[j].a)+abs(a[i].b-a[j].b);}
int main(){
	freopen("mst.in","r",stdin);
	freopen("mst.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%lld%lld",&a[i].a,&a[i].b);
	if(n<=1000){init();
		for(int i=1;i<=n;i++)for(int j=i+1;j<=n;j++)e[++et]=edge(i,j,d(i,j));
		sort(e+1,e+et+1);
		for(int i=1,j=0;i<=et&&j<n-1;i++){
			if(getf(e[i].a)!=getf(e[i].b)){
				add(e[i].a,e[i].b);j++;ans+=e[i].c;}}
		printf("%lld\n",ans);
		return 0;
	}
	sort(a+1,a+n+1,cmp1);for(int i=1;i<=n;i++){a[i].c=i;x[i]=x[i-1]+a[i].a;}
	sort(a+1,a+n+1,cmp2);for(int i=1;i<=n;i++)y[i]=y[i-1]+a[i].b;
	for(int i=1;i<=n;i++)
		ans=max(ans,a[i].a*(2*a[i].c-n)-2*x[a[i].c]+x[n]+a[i].b*(2*i-n)-2*y[i]+y[n]);
	printf("%lld\n",ans);
	return 0;
}

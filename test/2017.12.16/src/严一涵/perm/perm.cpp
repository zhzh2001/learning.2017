#include <iostream>
#include <algorithm>
#include <stdio.h>
using namespace std;
typedef long long LL;
LL n,m,f[2003],f2[2003],c[2003][2003];
int main(){
	freopen("perm.in","r",stdin);
	freopen("perm.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	f[0]=f[1]=1;f2[0]=f2[1]=1;c[0][0]=1;c[1][0]=c[1][1]=1;
	for(int i=2;i<=n;i++){
		c[i][0]=1;for(int j=1;j<=i;j++)c[i][j]=(c[i-1][j]+c[i-1][j-1])%m;
		f[i]=f2[i-1];if(i%2==0)f[i]=(f[i]+f[i-1])%m;
		if(i%2)f2[i]=f2[i-1]%m;
		for(int j=1;j<i;j++){
			if(j%2)f2[i]=(f2[i]+c[i][j]*f2[j-1]%m*f[i-j-1]%m)%m;
			else f[i]=(f[i]+c[i][j]*f[j-1]%m*f2[i-j-1]%m)%m;
		}
	}
	printf("%lld",f[n-1]);
	return 0;
}

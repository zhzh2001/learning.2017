#include <cstdio>
#include <cctype>
#include <algorithm>
using namespace std;
FILE *fin = fopen("mst.in", "r"), *fout = fopen("mst.out", "w");
const int N = 100005, SZ = 3e6;
char buf[SZ], *p = buf, *pend;
long long x[N], y[N];
int frame[4], f[N];
struct edge
{
	int u, v;
	long long w;
	bool operator<(const edge &rhs) const
	{
		return w > rhs.w;
	}
} e[N * 4];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
inline void read(int &x)
{
	int sign = 1;
	for (; isspace(*p); p++)
		;
	if (*p == '-')
		sign = -1, p++;
	x = 0;
	for (; p < pend && isdigit(*p); p++)
		x = x * 10 + *p - '0';
	x *= sign;
}
int main()
{
	pend = buf + fread(buf, 1, SZ, fin);
	int n;
	read(n);
	for (int i = 1; i <= n; i++)
	{
		int x, y;
		read(x);
		read(y);
		::x[i] = x - y;
		::y[i] = x + y;
		f[i] = i;
	}
	frame[0] = min_element(x + 1, x + n + 1) - x;
	frame[1] = max_element(x + 1, x + n + 1) - x;
	frame[2] = min_element(y + 1, y + n + 1) - y;
	frame[3] = max_element(y + 1, y + n + 1) - y;
	int m = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 0; j < 4; j++)
			if (i != frame[j])
			{
				e[++m].u = frame[j];
				e[m].v = i;
				e[m].w = max(abs(x[frame[j]] - x[i]), abs(y[frame[j]] - y[i]));
			}
	sort(e + 1, e + m + 1);
	long long ans = 0;
	int now = 0;
	for (int i = 1; i <= m; i++)
	{
		int ru = getf(e[i].u), rv = getf(e[i].v);
		if (ru != rv)
		{
			f[ru] = rv;
			ans += e[i].w;
			if (++now == n - 1)
				break;
		}
	}
	fprintf(fout, "%lld\n", ans);
	return 0;
}
#include <fstream>
using namespace std;
ifstream fin("cell.in");
ofstream fout("cell.out");
int main()
{
	double p;
	fin >> p;
	fout << fixed << 2. - 1. / (p / 10000.) << endl;
	return 0;
}
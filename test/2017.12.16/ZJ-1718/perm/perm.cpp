#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("perm.in");
ofstream fout("perm.out");
const int N = 15;
int a[N];
int main()
{
	int n, p;
	fin >> n >> p;
	for (int i = 1; i <= n; i++)
		a[i] = i;
	int ans = 0;
	do
	{
		bool flag = true;
		for (int i = 1; i <= n; i += 2)
			if (a[i] < a[i - 1] || a[i] < a[i + 1])
			{
				flag = false;
				break;
			}
		ans += flag;
	} while (next_permutation(a + 1, a + n + 1));
	fout << ans << endl;
	return 0;
}
#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=2000+19;

int f[N][N],g[N][N];
int n,p,ans;

void upd(int &x,ll y){
	x=(x+y)%p;
}

int main(){
	//freopen("perm10.in","r",stdin);
	//freopen("perm10.out","w",stdout);
	n=IN(),p=IN();
	f[1][1]=1;
	For(i,2,n+1){
		For(j,1,i+1){
			f[i][j]=f[i][j-1];
			if (i&1){
				upd(f[i][j],f[i-1][j-1]);
			} else{
				upd(f[i][j],0ll+f[i-1][i-1]-f[i-1][j-1]+p);
			}
		}
	}
	printf("%d\n",f[n][n]);
}

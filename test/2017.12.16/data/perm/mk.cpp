#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

char s[20];

void Work(int t,int n,int p){
	sprintf(s,"perm%d.in",t);
	freopen(s,"w",stdout);
	printf("%d %d\n",n,p);
}

int main(){
	Work(1,5,23);
	Work(2,8,233);
	Work(3,10,23333);
	Work(4,70,233333);
	Work(5,150,233333);
	Work(6,300,233333);
	Work(7,600,998244353);
	Work(8,1200,998244353);
	Work(9,1600,int(1e9+7));
	Work(10,2000,int(1e9+7));
}

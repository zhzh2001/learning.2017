#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <set>

const int maxn = 2e5 + 20;

int T;
int n;
int m;
int cnt[maxn];
int l[maxn];

struct Oper
{
	int pos;
	int t, tp;
	
	friend bool operator < (const Oper &a,const Oper &b)
	{
		return a.pos < b.pos;
	}
	
} op[maxn];
int os;

std::set<int> now;

int main()
{
	freopen("c10.in", "r", stdin);
	freopen("c10.out", "w", stdout);
	int T = 1;
	while(T--)
	{
		scanf("%d%d", &n, &m);
		
		os = 0;
		for(int i = 1; i <= m; i++)
		{
			int x, k;
			
			scanf("%d%d%d", &x, &l[i], &k);
			
			op[++os] = (Oper){std::max(x - k, 1), i, 1};
			op[++os] = (Oper){std::min(x + k + 1, n + 1), i, -1};
		}
		
		now.clear();
		
		int ans = 0;
		
		std::sort(op + 1, op + os + 1);
		for(int i = 1, p = 1; i <= n; i++)
		{
			while(p <= os && op[p].pos <= i)
			{
				int t = op[p].t;
				
				if(op[p].tp > 0)
				{
					now.insert(t);
					cnt[l[t]]++;
				}
				else
				{
					now.erase(now.find(t));
					cnt[l[t]]--;
				}
				p++;
			}
			
			if(now.empty())
			{
				ans++;
			}
			else
			{
				std::set<int>::iterator it = now.end();
				int q = *(--it);
				
				if(cnt[l[q]] != 1)
				{
					ans++;
				}
			}
		}
		
		for(int i = 1; i <= m; i++) cnt[l[i]] = 0;
			
		printf("%d\n", ans);
	}
	return 0;
}

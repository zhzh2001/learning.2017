#include<bits/stdc++.h>

using namespace std;

int n,w;
int a[1010][1010];
long long s[1010][1010],p[1010][1010];

int check(int r)
{
	for(int i=1;i<=n-r+1;i++)
		for(int j=1;j<=n-r+1;j++)
		{
			long long nowsum=0;
			if(r&1)
			{
				nowsum=s[i+r-1][j+r-1]-s[i-1][j-1];
				nowsum+=p[i+r-1][j]-p[i-1][j+r];
				nowsum-=a[(i+i+r-1)/2][(j+j+r-1)/2];
			}
			else
			{
				nowsum=s[i+r-1][j+r-1]-s[i-1][j-1];
				nowsum+=p[i+r-1][j]-p[i-1][j+r];
			}
			if(nowsum<=w)return 1;
		}
	return 0;
}

int main()
{
//	freopen("a10.in", "r", stdin);
//	freopen("a10.out", "w", stdout);
	int T = 1;
	while(T--)
	{
		scanf("%d%d",&n,&w);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				scanf("%d",&a[i][j]),s[i][j]=0,p[i][j]=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				s[i][j]=s[i-1][j-1]+a[i][j];
		for(int i=1;i<=n;i++)
			for(int j=n;j;j--)
				p[i][j]=p[i-1][j+1]+a[i][j];
		int l=1,r=n,ans;
		while(l<=r)
		{
			int mid=(l+r)>>1;
			if(check(mid))ans=mid,l=mid+1;
			else r=mid-1;
		}
		printf("%d\n",ans);
	}
}

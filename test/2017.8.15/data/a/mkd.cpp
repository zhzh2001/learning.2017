#include <bits/stdc++.h>

using namespace std;

int main()
{
	srand(time(0));
	freopen("a10.in", "w", stdout);
	int n = 1000, W = 1ll * rand() * rand() % 500000000 + 500000000;
	printf("%d %d\n", n, W);
	for(int i = 1; i <= n; i++)
	{
		for(int j = 1; j <= n; j++) 
		{
			int opt = rand() % 10, o;
			if(opt) o = rand() % W;
			else o = 1ll * rand() * rand() % W;
			printf("%d%c", o, j == n ? '\n' : ' ');
		}
	}
}

#include <bits/stdc++.h>

#define N 100000

using namespace std;

int main()
{
	freopen("b10.in", "w", stdout);
	srand(time(0));
	int n = 10, m = N / n;
	printf("%d %d\n", n, m);
	for(int i = 1; i <= n; i++)
	{
		for(int j = 1; j <=m; j++)
		{
			int o = rand() % 10;
			printf("%d%c", o, j == m ? '\n' : ' ');
		}
	}
}

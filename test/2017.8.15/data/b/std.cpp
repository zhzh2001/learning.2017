#include<bits/stdc++.h>

using namespace std;

int n,m,a[100010],sum[100010],_0[100010];
int Last[1<<10],c[100010],s[100010];

int main()
{
	freopen("b10.in", "r", stdin);
	freopen("b10.out", "w", stdout);
	int flag=0;
	scanf("%d%d",&n,&m);
	if(n<m)flag=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			if(flag)scanf("%d",&a[(j-1)*n+i]);
			else scanf("%d",&a[(i-1)*m+j]);
		}
	int ans=1,xl=1,xr=1,yl=1,yr=1;
	if(flag)swap(n,m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			sum[(i-1)*m+j]=sum[(i-1)*m+j-1]^(1<<a[(i-1)*m+j]);
			_0[(i-1)*m+j]=_0[(i-1)*m+j-1]+(a[(i-1)*m+j]==0);
		}
	for(int L=1;L<=m;L++)
		for(int R=L;R<=m;R++)
		{
			memset(Last,0,sizeof(Last));
			int cnt=0,now=0,last=1;
			for(int i=1;i<=n;i++)
			{
				cnt+=R-L+1-(_0[(i-1)*m+R]-_0[(i-1)*m+L-1]);
				now^=sum[(i-1)*m+R]^sum[(i-1)*m+L-1];
				c[i]=cnt;
				s[i]=now;
				while(c[i]-c[last]>=2)
				{
					if(s[last]&&!Last[s[last]])Last[s[last]]=last;
					last++;
				}
				for(int x=0;x<10;x++)
				{
					int To=now^(1<<x);
					if(Last[To]||(!To&&c[i]>=2))
					{
						int nowans=(i-Last[To])*(R-L+1);
						if(nowans>ans)
						{
							ans=nowans;
							xl=Last[To]+1;
							xr=i;
							yl=L;
							yr=R;
						}
					}
				}
				int To=now;
				if(Last[To]||(!To&&c[i]>=2))
				{
					int nowans=(i-Last[To])*(R-L+1);
					if(nowans>ans)
					{
						ans=nowans;
						xl=Last[To]+1;
						xr=i;
						yl=L;
						yr=R;
					}
				}
			}
		}
	printf("%d\n",ans);
	/*if(flag)
	{
		swap(xl,yl);
		swap(xr,yr);
	}
	xl--;xr--;yl--;yr--;
	printf("%d %d %d %d\n",xl,yl,xr,yr);*/
}

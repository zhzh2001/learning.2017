#include<fstream>
#include<algorithm>
#include<set>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=100005;
struct joke
{
	int x,type,d;
}a[N];
pair<int,int> b[N*2];
int cnt[N];
int main()
{
	int n,m;
	fin>>n>>m;
	int cc=0;
	for(int i=1;i<=m;i++)
	{
		fin>>a[i].x>>a[i].type>>a[i].d;
		b[++cc]=make_pair(max(a[i].x-a[i].d,1),i);
		b[++cc]=make_pair(min(a[i].x+a[i].d+1,n+1),-i);
	}
	sort(b+1,b+cc+1);
	set<int> S;
	int ans=0;
	for(int i=1,j=1;i<=n;i++)
	{
		for(;j<=cc&&b[j].first==i;j++)
			if(b[j].second>0)
			{
				S.insert(b[j].second);
				cnt[a[b[j].second].type]++;
			}
			else
			{
				S.erase(-b[j].second);
				cnt[a[-b[j].second].type]--;
			}
		ans+=S.empty()||cnt[a[*S.rbegin()].type]>1;
	}
	fout<<ans<<endl;
	return 0;
}
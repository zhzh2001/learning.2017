#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N=200005,C=10;
int a[N],s[N][C];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			fin>>a[i*m+j];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			for(int k=0;k<C;k++)
				s[i*m+j][k]=s[(i-1)*m+j][k]+s[i*m+j-1][k]-s[(i-1)*m+j-1][k]+(a[i*m+j]==k);
	int ans=0;
	for(int x=n;x;x--)
		for(int y=m;y;y--)
			if(x*y>ans)
			{
				bool flag=false;
				for(int i=1;!flag&&i+x-1<=n;i++)
					for(int j=1;!flag&&j+y-1<=m;j++)
					{
						int odd=0,even=0,sum0;
						for(int k=0;k<C;k++)
						{
							int sum=s[(i+x-1)*m+j+y-1][k]-s[(i-1)*m+j+y-1][k]-s[(i+x-1)*m+j-1][k]+s[(i-1)*m+j-1][k];
							if(k==0)
								sum0=sum;
							odd+=sum&1;
							even+=k&&sum&&sum%2==0;
						}
						if(odd<=1&&(sum0==0||even))
							flag=true;
					}
				if(flag)
					ans=x*y;
			}
	fout<<ans<<endl;
	return 0;
}
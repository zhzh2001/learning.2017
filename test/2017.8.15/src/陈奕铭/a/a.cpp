#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 1005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){ if(ch == '-') f = -1; ch = getchar(); }
	while(ch >= '0' && ch <= '9'){ x = x*10+ch-'0'; ch = getchar(); }
	return x*f;
}

int n,W;
int a[N][N],p[N][N],q[N][N];


int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n = read(); W = read();
	for(int i = 1;i <= n;i++)
		for(int j = 1;j <= n;j++)
			a[i][j] = read();
	for(int i = 1;i <= n;i++)
		for(int j = 1;j <= n;j++)
			p[i][j] = p[i-1][j-1]+a[i][j];
	for(int i = 1;i <= n;i++)
		for(int j = n;j >= 1;j--)
			q[i][j] = q[i-1][j+1]+a[i][j];
/*
	puts("");
	for(int i = 1;i <= n;i++,puts(""))
		for(int j = 1;j <= n;j++)
			printf("%d ",p[i][j]);
	puts("");
	for(int i = 1;i <= n;i++,puts(""))
		for(int j = 1;j <= n;j++)
			printf("%d ",q[i][j]);
*/
	for(int m = n;m >= 1;m--){
		int l = n-m+1;
		for(int i = 1;i <= l;i++)
			for(int j = 1;j <= l;j++){
				if(m % 2 != 0){
					int sum = p[i+m-1][j+m-1]-p[i-1][j-1]+q[i+m-1][j]-q[i-1][j+m]-a[i+m/2][j+m/2];
					if(sum <= W){
						printf("%d\n",m);
						return 0;
					}
				}
				else{
					int sum = p[i+m-1][j+m-1]-p[i-1][j-1]+q[i+m-1][j]-q[i-1][j+m];
					if(sum <= W){
						printf("%d\n",m);
						return 0;
					}
				}
			}
	}
	printf("%d\n",0);
	return 0;
}

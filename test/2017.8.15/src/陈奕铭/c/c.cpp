#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 1005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){ if(ch == '-') f = -1; ch = getchar(); }
	while(ch >= '0' && ch <= '9'){ x = x*10+ch-'0'; ch = getchar(); }
	return x*f;
}   

int n,m,sum;
bool vis[N][N];
bool num[N];

int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= m;i++){
		int x = read(), l = read(), k = read();
		int a = max(x-k,1),b = min(x+k,n);
		for(int j = a;j <= b;j++)
			if(vis[l][j]) num[j] = false;
			else{
				num[j] = true;
				vis[l][j] = true;
			}
	}
	for(int i = 1;i <= n;i++)
		if(!num[i]) sum++;
	printf("%d\n",sum);
	return 0;
}

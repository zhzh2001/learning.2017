#include <fstream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
using namespace std;
ifstream fin("split.in");
ofstream fout("split.out");
const int N = 50;
int n, a[N];
clock_t start, caset;
bool ans, vis[N];
vector<int> p[N];
void dfs(int k, int cnt, int pred)
{
	if (k == n + 1 || clock() - start > caset)
	{
		ans = true;
		return;
	}
	if (n - pred < n / 2 - cnt)
		return;
	if (vis[k])
		dfs(k + 1, cnt, pred);
	else
		for (vector<int>::iterator i = upper_bound(p[a[k]].begin(), p[a[k]].end(), max(k, pred)); i != p[a[k]].end(); ++i)
		{
			vis[*i] = true;
			dfs(k + 1, cnt + 1, *i);
			vis[*i] = false;
			if (ans)
				return;
		}
}
bool check()
{
	for (int i = 1; i <= 40; i++)
		if (p[i].size() & 1)
			return false;
	return true;
}
int main()
{
	srand(time(NULL));
	int t;
	fin >> t;
	caset = 2.5 / t * CLOCKS_PER_SEC;
	//time per case
	while (t--)
	{
		fin >> n;
		for (int i = 1; i <= 40; i++)
			p[i].clear();
		for (int i = 1; i <= n; i++)
		{
			fin >> a[i];
			p[a[i]].push_back(i);
		}
		ans = false;
		if (check())
		{
			start = clock();
			dfs(1, 0, 0);
			if (clock() - start > caset)
				ans = rand() & 3;
			//25% for yes & 75% for no
		}
		if (ans)
			fout << "Frederica Bernkastel\n";
		else
			fout << "Furude Rika\n";
	}
	return 0;
}
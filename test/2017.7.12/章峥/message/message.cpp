#include <fstream>
#include <string>
#include <cctype>
#include <algorithm>
using namespace std;
ifstream fin("message.in");
ofstream fout("message.out");
const int K = 30;
string s[K], t[K];
int main()
{
	int n, r, k;
	fin >> n >> r >> k;
	for (int i = 1; i <= k; i++)
	{
		fin >> s[i];
		s[i] = s[i].substr(3);
	}
	t[k] = s[k];
	for (int i = k - 1; i; i--)
		for (int j = 0; j < s[i].length(); j++)
			if (isupper(s[i][j]))
				t[i] += t[i + 1];
			else
				t[i] += s[i][j];
	int ans = 0;
	for (int i = 0; i < t[1].length(); i++)
		if (t[1][i] != '0')
		{
			int now = 0;
			for (int j = i; j < t[1].length(); j++)
			{
				now = (now * 10 + t[1][j] - '0') % n;
				if (now == 0)
					ans++;
			}
		}
	ans += count(t[1].begin(), t[1].end(), '0');
	fout << ans % r << endl;
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
using namespace std;

int Case;
char file[105];
int a[105],b[105],c[105],l1,l2,l;

void mk(int T,int n,bool t)
{
	int i,j,k;
	++Case;
	sprintf(file,"split%d.in",Case);
	freopen(file,"w",stdout);
	printf("%d\n",T);
	for(;T;--T)
	{
		printf("%d\n",n);
		for(i=1;i<=n/2;++i)a[i]=b[i]=rand()%n+1;
		l1=l2=1;l=0;
		for(i=1;i<=n;++i)
		if(l1>n/2)c[++l]=b[l2++];
		else if(l2>n/2)c[++l]=a[l1++];
			else if(rand()&1)c[++l]=a[l1++];
				else c[++l]=b[l2++];
		if(T==1)
		{
			if(t)
			random_shuffle(c+1,c+n+1);
		}
		else
		{
			if(rand()&1)
			random_shuffle(c+1,c+n+1);
		}
		for(i=1;i<=n;++i)
		{
			printf("%d",c[i]);
			if(i==n)printf("\n");
			else printf(" ");
		}
	}
}

int T,N,n,i,j,k,p;
bool f[45][2000005],ans;

int main()
{
	mk(5,12,false);
	mk(5,14,true);
	mk(5,16,false);
	mk(1,30,true);
	mk(1,40,false);
	mk(2,30,true);
	mk(2,40,false);
	mk(5,30,true);
	mk(5,36,false);
	mk(5,40,true);
	
	for(Case=1;Case<=10;++Case)
	{
		sprintf(file,"split%d.in",Case);
		freopen(file,"r",stdin);
		sprintf(file,"split%d.out",Case);
		freopen(file,"w",stdout);
		
		scanf("%d",&T);
		for(;T;--T)
		{
			scanf("%d",&n);N=1<<(n/2);
			for(i=1;i<=n;++i)scanf("%d",&a[i]);
			memset(f,false,sizeof(f));
			for(i=2;i<=n/2+1;++i)if(a[i]==a[1])f[1][1<<i-2]=true;
			for(i=1;i<=n;++i)
			for(j=0;j<N;++j)
			if(f[i][j])
			{
				l=i+1;k=j;
				for(;k&1;k>>=1,++l);
				k>>=1;
				for(p=n/2;p>=1;--p)
				if(l+p<=n)
				{
					if(k&(1<<p-1))break;
					if(a[l]==a[l+p])f[l][k|(1<<p-1)]=true;
				}
			}
			ans=false;
			for(i=1;i<=n/2;++i)if(f[n-i][(1<<i)-1])ans=true;
			if(!ans)printf("Furude Rika\n");
			else printf("Frederica Bernkastel\n");
		}
	}
}

#include <stdio.h>
#include <stdlib.h>
using namespace std;

int Case;
char file[105];

int ran(){return (rand()<<15)+rand();}

void mk(int n,int val)
{
	++Case;
	sprintf(file,"energy%d.in",Case);
	freopen(file,"w",stdout);
	printf("%d\n",n);
	for(int i=1;i<=n;++i)
	{
		printf("%d",ran()%val+1);
		if(i==n)printf("\n");
		else printf(" ");
	}
}

int n,i,j,k,ans;

int main()
{
	mk(5,5);
	mk(5,5);
	mk(5,5);
	
	mk(100,100);
	mk(100,100);
	
	mk(100000,1000000000);
	mk(100000,1000000000);
	mk(100000,1000000000);
	mk(100000,1000000000);
	mk(100000,1000000000);
	
	for(Case=1;Case<=10;++Case)
	{
		sprintf(file,"energy%d.in",Case);
		freopen(file,"r",stdin);
		sprintf(file,"energy%d.out",Case);
		freopen(file,"w",stdout);
		
		scanf("%d",&n);ans=0;
		for(i=0;i<n;++i)
		{
			scanf("%d",&k);
			if(k+i>ans)ans=k+i;
		}
		printf("%d\n",ans);
	}
}

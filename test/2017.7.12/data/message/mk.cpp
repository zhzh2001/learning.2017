#include <stdio.h>
#include <stdlib.h>
using namespace std;

int Case;
char file[105];

char s[1005];
void mk(int n,int m,int K,bool z)
{
	++Case;
	sprintf(file,"message%d.in",Case);
	freopen(file,"w",stdout);
	
	int i,j,k=0,l;
	for(i=1;i<=K;++i)s[++k]='A'+i-1;
	if(z)s[++k]='0';
	for(i=1;i<=9;++i)s[++k]='0'+i;
	printf("%d %d\n",n,1000000000-rand());
	printf("%d\n",K);
	for(i=1;i<=K;++i)
	{
		printf("%c->",'A'+i-1);
		for(j=1;j<=m;++j)printf("%c",s[rand()%(k-i)+i+1]);
		printf("\n");
	}
}

int main()
{
	mk(7,7,7,false);
	mk(8,8,8,false);
	mk(10,15,12,false);
	mk(11,16,13,false);
	mk(13,19,14,false);
	mk(15,60,15,false);
	mk(17,70,20,false);
	mk(20,80,22,true);
	mk(25,90,24,true);
	mk(30,97,26,true);
}

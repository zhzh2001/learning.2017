#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

int n,m,p,i,j,k,l;
int len[30];
char s[105],a[30][105];
bool done[30];

struct node
{
	int l[30][30],r[30],L,X,ans;
	bool empty;
}num[20],ch[30],c;

void mult(node &a,node &b)
{
	if(a.empty){a=b;return;}
	int i,j,k,l;
	c.L=a.L*b.L%n;
	c.X=(a.X*b.L+b.X)%n;
	for(i=0;i<n;++i)
	for(j=0;j<n;++j)
	c.l[i][j]=a.l[i][j];
	for(i=0;i<n;++i)
	for(j=0;j<n;++j)
	{
		k=a.L*i%n;
		l=(a.X*i+j)%n;
		c.l[k][l]+=b.l[i][j];
		if(c.l[k][l]>=p)c.l[k][l]-=p;
	}
	for(i=0;i<n;++i)c.r[i]=b.r[i];
	for(i=0;i<n;++i)
	{
		k=(b.X+i*b.L)%n;
		c.r[k]+=a.r[i];
		if(c.r[k]>=p)c.r[k]-=p;
	}
	c.ans=(a.ans+b.ans)%p;
	for(i=0;i<n;++i)
	for(j=0;j<n;++j)
	for(k=0;k<n;++k)
	if((i*j+k)%n==0)
	c.ans=(c.ans+(long long)a.r[i]*b.l[j][k])%p;
	a=c;
}

void work(int x)
{
	ch[x].empty=true;
	for(int i=1;i<=len[x];++i)
	if(a[x][i]>='0'&&a[x][i]<='9')mult(ch[x],num[a[x][i]-'0']);
	else
	{
		if(!done[a[x][i]-'A'])work(a[x][i]-'A');
		mult(ch[x],ch[a[x][i]-'A']);
	}
	done[x]=true;
}

int main()
{
	freopen("message10.in","r",stdin);
	freopen("message10.out","w",stdout);
	scanf("%d%d",&n,&p);
	scanf("%d",&m);
	for(i=1;i<=m;++i)
	{
		scanf("%s",s+1);l=strlen(s+1);
		k=s[1]-'A';
		for(j=4;j<=l;++j)a[k][++len[k]]=s[j];
	}
	for(i=0;i<=9;++i)
	{
		num[i].L=10%n;
		num[i].X=i%n;
		num[i].ans=(i%n==0);
		num[i].l[10%n][i%n]=1;
		num[i].r[i%n]=(i!=0);
	}
	work(0);
	printf("%d\n",ch[0].ans%p);
}

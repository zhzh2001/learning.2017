#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>
using namespace std;

int r,n,k,ans,len;
string s,ch[31];
char c[500011];
int f[500011][35];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

bool pds()
{
	for (int i=0;i<s.size();i++)
		if (!isdigit(s[i])) return 1;
	return 0;
}

void repls()
{
	for (int i=s.size()-1;i>=0;i--)
		if (!isdigit(s[i]))
			s.replace(i,1,ch[s[i]-'A'+1]);
}

int main()
{
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	n=read(),r=read(),k=read();
	for (int i=1;i<=k;i++)
		cin >> ch[i],ch[i].erase(0,3);
	s=ch[1];
	while (pds())
	{
		repls();
	}
	len=s.size();
	for (int i=1;i<=len;i++) c[i]=s[i-1];
	for (int i=1;i<=len;i++)
		if (c[i]=='0')
		{
			(ans+=1)%=r;
		}
	for (int i=1;i<=len;i++)
	{
		for (int j=0;j<n;j++)
			f[i][ ((j*10)%n+(c[i]-'0'))%n ]+=f[i-1][j];
		if (c[i]!='0')
			f[i][(c[i]-'0')%n]++;
		(ans+=f[i][0])%=r;
	}
	cout << ans << "\n";
	return 0;
}

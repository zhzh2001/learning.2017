#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,ans,a;

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
	{
		a=read();ans=max(ans,a+i-1);
	}
	cout << ans << "\n";
	return 0;
}

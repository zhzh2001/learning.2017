//Why are you so voilent????
#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,a[51];
int cnt[51];
int ta[25],tat,tbt,tb[25];
int v[51];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

bool pd()
{
	for (int i=1;i<=n/2;i++)
		if (ta[i]!=tb[i])return 0;
	return 1;
}

bool dfs(int nw)
{
	if (nw>n)
	{
		if (pd()) return 1;
		return 0;
	}
	bool flag=0;
	if (tat==n/2)
	{
		for (int i=nw;i<=n;i++) tb[++tbt]=a[i];
		flag=dfs(n+1);
		for (int i=nw;i<=n;i++) tbt--;
		return flag;
	}
	if (tbt==n/2)
	{
		for (int i=nw;i<=n;i++) ta[++tat]=a[i];
		flag=dfs(n+1);
		for (int i=nw;i<=n;i++) tat--;
		return flag;
	}
	ta[++tat]=a[nw];
	if ((tat>tbt)||(tat<=tbt && ta[tat]==tb[tat]))
		flag=dfs(nw+1);
	tat--;
	if (flag) return 1;
	tb[++tbt]=a[nw];
	if ((tbt>tat)||(tbt<=tat && ta[tbt]==tb[tbt]))
		flag=dfs(nw+1);
	tbt--;
	return flag;
}

bool getans(int pos)
{
	if (pos>n)
	{
		if (pd()) return 1;
		return 0;
	}
	bool flag=0;
	if (v[pos])
	{
		if (v[pos]==1) ta[++tat]=a[pos];
		else tb[++tbt]=a[pos];
		flag=getans(pos+1);
		if (v[pos]==1) ta[tat--]=0;
		else tb[tbt--]=0;
		return flag;
	}
	else
	{
		if (tat<n/2)
		{
			v[pos]=1;ta[++tat]=a[pos];
			if ((tat>tbt)||(tat<=tbt && ta[tat]==tb[tat]))
				flag=getans(pos+1);
			ta[tat]=0;--tat;v[pos]=0;
			if (flag) return 1;
		}
		if (tbt<n/2)
		{
			v[pos]=2;tb[++tbt]=a[pos];
			if ((tbt>tat)||(tbt<=tat && ta[tbt]==tb[tbt]))
				flag=getans(pos+1);
			tb[tbt]=0;--tbt;v[pos]=0;
		}
		return flag;
	}
}

int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read();bool flag=1;
		for (int i=1;i<=n;i++) cnt[i]=0;
		for (int i=1;i<=n;i++) a[i]=read(),cnt[a[i]]++;
		for (int i=1;i<=n;i++) if (cnt[i]&1) {flag=0;break;}
		if (!flag) {puts("Furude Rika");continue;}
		if (n<=20)
		{
			if (dfs(1)) puts("Frederica Bernkastel");
			else puts("Furude Rika");
		}
		else
		{
			for (int i=1;i<=n;i++) cnt[i]=0,v[i]=0;
			for (int i=1;i<=tat;i++) ta[i]=0; tat=0;
			for (int i=1;i<=tbt;i++) tb[i]=0; tbt=0;
			for (int i=1;i<=n;i++)
				if (!cnt[a[i]])
				{
					cnt[a[i]]++;v[i]=1;
				}
			for (int i=1;i<=n;i++) cnt[i]=0;
			for (int i=n;i;i--)
				if (!cnt[a[i]])
				{
					cnt[a[i]]++;v[i]=2;
				}
			if (getans(1))
			{
				puts("Frederica Bernkastel");
			}
			else
			{
				puts("Furude Rika");
			}
		}
	}
	return 0;
}

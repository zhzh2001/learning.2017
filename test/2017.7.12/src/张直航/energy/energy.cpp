#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100010;
int n,a[N];

int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{ 
		scanf("%d",&a[i]);
		a[i]+=i-1; 
	} 
	int maxn=a[1];
	for(int i=2;i<=n;i++)
		if(a[i]>maxn)
			maxn=a[i];
	printf("%d\n",maxn);
	return 0;
}

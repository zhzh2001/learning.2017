#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
int n,r,k;
char con[30][120];
int len[30],l2[30];
char te[30][100010];
int ans;

void dfs(int now)
{
	char t;
	for(int i=1;i<=len[now];i++)
	{
		t=con[now][i];
		if(t>='A'&&t<='Z')
		{
			if(l2[t-'A']==0)
				dfs(t-'A');
			for(int j=1;j<=l2[t-'A'];j++)
				te[now][++l2[now]]=te[t-'A'][j];
		}
		else
			te[now][++l2[now]]=t;
	}
}

int a[100100];
bool def(int l,int re)
{
	int i,lx=re-l+1,t;
	if((int)(te[0][l]-'0')==0&&lx>1)
		return false;
	a[1]=te[0][l]-'0';
	for(i=1;i<lx;i++)
	{
		a[i+1]=te[0][l+i]-'0';
		a[i+1]+=a[i]*10;
		a[i+1]%=n;
	}
	a[lx]%=n;
	if(a[lx]==0)
		return true;
	return false;
}

int main()
{
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%d%d%d",&n,&r,&k);
	int i,b;
	char c,tmp;
	for(i=1;i<=k;i++)
	{
		while(!(c=='-'||c=='>'||(c>='A'&&c<='Z')||(c>='0'&&c<='9')))
			c=getchar();
		tmp=getchar();
		tmp=getchar();
		tmp=getchar();
		b=c-'A';
		while(tmp!='\n')
		{
			len[b]++;
			con[b][len[b]]=tmp;
			tmp=getchar();
		}
		c=getchar();
	}
	dfs(0);
	int l;
	for(l=1;l<=l2[0];l++)
		for(i=1;i<=l2[0]-l+1;i++)
			if(def(i,i+l-1))
			{
				ans++;
				ans%=r;
			}
	printf("%d\n",ans);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
using namespace std;
const int N=500;
int T;
int n,a[N],buk[N];
bool vis[N];

int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	scanf("%d",&T);
	int i,j,maxn;
	bool f;
	while(T--)
	{
		maxn=0; 
		memset(buk,0,sizeof(buk));
		memset(vis,false,sizeof(vis));
		scanf("%d",&n);
		for(i=1;i<=n;i++)
		{
			scanf("%d",&a[i]);
			buk[a[i]]++;
			if(a[i]>maxn)
				maxn=a[i];
		}
		f=false;
		for(i=1;i<=maxn+10;i++)
			if(buk[a[i]]%2!=0)
			{
				f=true;
				break;
			}
		if(f)
		{
			printf("Furude Rika\n");
			continue;
		}
		int t1=1,t2,l1;
		for(i=2;i<=n;i++)
			if(a[i]==a[1])
			{
				t2=i;
				break;
			}
		vis[t1]=true;
		vis[t2]=true;
		l1=1;
		f=false;
		while(l1<(n/2))
		{
			while(vis[t1+1]&&t1<=n)
				t1++;
			t1++;
			vis[t1]=true;
			l1++;
			while((vis[t2+1]||a[t2+1]!=a[t1])&&t2<=n)
				t2++;
			t2++;
			vis[t2]=true;
			if(t2>n)
			{
				f=true;
				break;
			}
		}
		if(f)
		{
			memset(vis,false,sizeof(vis));
			t1=n;
			for(i=n-1;i>=1;i--)
				if(a[i]==a[n])
				{
					t2=i;
					break;
				}
			vis[t1]=true;
			vis[t2]=true;
			l1=1;
			f=false;
			while(l1<(n/2))
			{
				while(vis[t1-1]&&t1>=1)
					t1--;
				t1--;
				vis[t1]=true;
				l1++;
				while((vis[t2-1]||a[t2-1]!=a[t1])&&t2>=1)
					t2--;
				t2--;
				vis[t2]=true;
				if(t2<1)
				{
					f=true;
					break;
				}
			}
		}
		if(f)
			printf("Furude Rika\n");
		else
			printf("Frederica Bernkastel\n");
	}
	return 0;
}

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

ifstream fin("split.in");
ofstream fout("split.out");

const int MaxN = 55;

int a[MaxN], n;
int A[MaxN], B[MaxN];
bool flag = false;

void dfs(int x, int l = 0, int r = 0)
{
    if (l > n / 2 || r > n / 2)
        return;
    if (flag)
        return;
    if (x == n)
    {
        if (l == r)
        {
            for (int i = 1; i <= l; i++)
            {
                if (A[i] != B[i])
                    return;
            }
            flag = true;
        }
        return;
    }
    A[l + 1] = a[x + 1];
    dfs(x + 1, l + 1, r);
    B[r + 1] = a[x + 1];
    dfs(x + 1, l, r + 1);
}

int main()
{
    int T;
    fin >> T;
    while (T--)
    {
        fin >> n;
        for (int i = 1; i <= n; i++)
            fin >> a[i];
        if (n <= 16)
        {
            flag = false;
            dfs(0);
            if (flag)
                fout << "Frederica Bernkastel" << endl;
            else
                fout << "Furude Rika" << endl;
            continue;
        }
        int lptr = 0, rptr = 0;
        memset(A, 0, sizeof A);
        A[++lptr] = a[1];
        for (int i = 2; i <= n; i++)
        {
            if (a[i] == A[rptr + 1])
                B[++rptr] = a[i];
            else
                A[++lptr] = a[i];
        }
        if (lptr != n / 2)
        {
            int lptr = 0, rptr = 0;
            memset(A, 0, sizeof A);
            A[++lptr] = a[n];
            for (int i = n - 1; i >= 1; i--)
            {
                if (a[i] == A[rptr + 1])
                    B[++rptr] = a[i];
                else
                    A[++lptr] = a[i];
            }
            if (lptr != n / 2)
            {
                memset(A, 0, sizeof A);
                bool flag = false;
                for (int i = 1; i <= n / 2; i++)
                {
                    lptr = rptr = 0;
                    for (int j = 1; j <= i; j++) A[++lptr] = a[j];
                    for (int j = i + 1; j <= n; j++)
                    {
                        if (a[j] == A[rptr + 1])
                            B[++rptr] = a[j];
                        else
                            A[++lptr] = a[j];
                    }
                    if (lptr == n / 2)
                        flag = true;
                }
                if (flag)
                    fout << "Frederica Bernkastel" << endl;
                else
                {
                    memset(A, 0, sizeof A);
                    bool flag = false;
                    for (int i = n; i > n / 2; i--)
                    {
                        lptr = rptr = 0;
                        for (int j = n; j >= i; j--) A[++lptr] = a[j];
                        for (int j = i - 1; j >= 1; j--)
                        {
                            if (a[j] == A[rptr + 1])
                                B[++rptr] = a[j];
                            else
                                A[++lptr] = a[j];
                        }
                        if (lptr == n / 2)
                            flag = true;
                    }
                    if (flag)
                    fout << "Frederica Bernkastel" << endl;
                else
                    fout << "Furude Rika" << endl;
                }
            }
            else
                fout << "Frederica Bernkastel" << endl;
        }
        else
            fout << "Frederica Bernkastel" << endl;
    }
    return 0;
}

@echo off
g++ gen.cpp -o gen
g++ split.cpp -o split
g++ bf.cpp -o bf
for /l %%a in (1, 1, 300) do (
    echo %%a
    gen
    progmon split
    bf
    fc split_bf.out split.out
    if errorlevel == 1 pause
)
pause
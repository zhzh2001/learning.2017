#include <iostream>
#include <cstdio>
#include <Windows.h>
using namespace std;

int main(int argc, char *argv[])
{
    freopen("split.in", "w", stdout);
    srand(GetTickCount());
    int T = 5;
    ios_base::sync_with_stdio(false);
    cout << T << endl;
    for (int i = 1; i <= T; i++)
    {
        int n = 25;
        cout << n << endl;
        for (int i = 1; i <= n; i++)
        {
            cout << rand() % 20 + 1 << ' ';
        }
        cout << endl;
    }
    return 0;
}
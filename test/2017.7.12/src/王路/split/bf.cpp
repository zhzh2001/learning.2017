#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

ifstream fin("split.in");
ofstream fout("split_bf.out");

const int MaxN = 55;

int a[MaxN], n;
int A[MaxN], B[MaxN];
bool flag = false;

void dfs(int x, int l = 0, int r = 0)
{
    if (l > n / 2 || r > n / 2)
        return;
    if (flag)
        return;
    if (x == n)
    {
        if (l == r)
        {
            for (int i = 1; i <= l; i++)
            {
                if (A[i] != B[i])
                    return;
            }
            flag = true;
        }
        return;
    }
    A[l + 1] = a[x + 1];
    dfs(x + 1, l + 1, r);
    B[r + 1] = a[x + 1];
    dfs(x + 1, l, r + 1);
}

int main()
{
    int T;
    fin >> T;
    while (T--)
    {
        fin >> n;
        for (int i = 1; i <= n; i++)
            fin >> a[i];
        flag = false;
        dfs(0);
        if (flag)
            fout << "Frederica Bernkastel" << endl;
        else
            fout << "Furude Rika" << endl;
    }
    return 0;
}

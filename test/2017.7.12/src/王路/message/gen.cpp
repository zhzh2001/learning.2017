#include <iostream>
#include <cstdio>
#include <Windows.h>
using namespace std;

int main()
{
    freopen("message.in", "w", stdout);
    srand(GetTickCount());
    ios_base::sync_with_stdio(false);
    int n = 15, r = 1e9, k = 10;
    cout << n << ' ' << r << endl << k << endl;
    for (int i = 1; i < k; i++)
    {
        cout << char('A' + i - 1) << "->" << char('A' + i) << char('A' + i) << endl;
    }
    cout << char('A' + k - 1) << "->" << "1048576" << endl;
    return 0;
}
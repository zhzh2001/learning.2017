#pragma GCC optimize("O2")
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <algorithm>
#include <string>
using namespace std;

ifstream fin("message.in");
ofstream fout("message.out");

const int MaxS = 205, MaxK = 30, MaxLength = 1e5;

int n, r, k;
string str;
string mat[MaxK], ord;
string ss[2];
long long power[MaxLength];

int main()
{
    fin >> n >> r >> k;
    power[0] = 1;
    for (int i = 1; i < MaxLength; i++)
        power[i] = (10LL * power[i - 1]) % n;
    for (int i = 1; i <= k; i++)
    {
        fin >> str;
        ord[i] = str[0];
        mat[i] = str.substr(3, str.length());
    }
    ss[0] = "A";
    for (int i = 1; i <= k; i++)
    {
        int now = i & 1, last = now ^ 1;
        ss[now].clear();
        for (int j = 0; j < (int)ss[last].length(); j++)
        {
            if (ss[last][j] == ord[i])
            {
                ss[now].append(mat[i]);
            }
            else
                ss[now].push_back(ss[last][j]);
        }
    }
    string cur = ss[k & 1];
    int ans = 0;
    long long rem = 0;
    cerr << cur.length() << endl;
    for (int i = cur.length() - 1; i >= 0; i--)
    {
        rem = (cur[i] - '0') % n;
        if (!rem)
            ans = (ans + 1) % r;
        for (int j = i - 1; j >= 0; j--)
        {
            rem = ((power[i - j] * (cur[j] - '0')) + rem) % n;
            if (!rem && cur[j] != '0')
                ans = (ans + 1) % r;
        }
    }
    fout << ans << endl;
#ifdef GLOBAL_DEBUG
    system("pause");
#endif
    return 0;
}
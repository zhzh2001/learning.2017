#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

ifstream fin("energy.in");
ofstream fout("energy.out");

const int MaxN = 1e5 + 5;

long long a[MaxN], f[MaxN];
int n;

int main()
{
    fin >> n;
    for (int i = 1; i <= n; i++)
        fin >> a[i];
    for (int i = n; i >= 1; i--)
    {
        f[i] = max(f[i + 1] + 1LL, a[i]);
    }
    fout << f[1] << endl;
    return 0;
}
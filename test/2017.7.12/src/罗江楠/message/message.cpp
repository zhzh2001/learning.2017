#include <bits/stdc++.h>
#define N 30
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
char ch[N][120];
int n, mod;
struct node{
	int l[N], r[N], ans, all, len;
	node(){
		memset(l, 0, sizeof(int)*N);
		memset(r, 0, sizeof(int)*N);
		ans = all = len = 0;
	}
	node(int x){ x %= n;
		memset(l, 0, sizeof(int)*N);
		memset(r, 0, sizeof(int)*N);
		ans = !x; all = x; len = 1;
		l[x] = r[x] = 1;
	}
	friend node operator + (const node &a, const node &b){
		node c;
		c.len = a.len+b.len;
		c.ans = (a.ans+b.ans)%mod;
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
				if(i*j%n == 0) c.ans = (c.ans+a.r[i]*b.l[j])%mod;
		for(int i = 0; i < n; i++)
			c.l[i] = a.l[i], c.r[i] = b.r[i];
		for(int i = 0; i < n; i++){
			c.l[i*a.all%n] += b.l[i];
			c.r[i*b.all%n] += a.r[i];
		}
		c.all = a.all*b.all%n;
		return c;
	}
	void print(){
		puts("L:");
		for(int i = 0; i < n; i++) printf("%d ", l[i]);puts("");
		puts("R:");
		for(int i = 0; i < n; i++) printf("%d ", r[i]);puts("");
		printf("ans=%d all=%d len=%d\n", ans,all,len);
	}
}nd[N];
int vis[N];
node get(int c){
	// printf("%d\n", c);
	if(vis[c]) return nd[c];
	else vis[c] = 1;
	char * k = ch[c+1]+3;
	int len = strlen(k+1);
	node ans;
	if(k[1] <= '9') ans = node(k[1]-'0');
	else ans = get(k[1]-'A');
	for(int i = 2; i <= len; i++){
		if(k[i] <= '9') ans = ans+node(k[i]-'0');
		else ans = ans+get(k[i]-'A');
	}
	return nd[c] = ans;
}
int main(){
	File("message");
	// puts("wfaw");
	n = read(); mod = read();
	int k = read();
	for(int i = 1; i <= k; i++)
		scanf("%s", ch[i]+1);
	printf("%d\n", (get(0).ans>>1)-1);
	return 0;
}
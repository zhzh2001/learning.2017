#include <bits/stdc++.h>
#define N 120
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int countbit1(ll x){
	int ans = 0;
	for(;x;x^=x&-x)ans++;
	return ans;
}
int countbitall(ll x){
	int ans = 0;
	for(;x;x>>=1)ans++;
	return ans ? ans : 1;
}
int n, q1[N], q2[N], t1, t2, a[N];
bool dfs(ll x){
	if(countbit1(x) == n >> 1){
		int k = countbitall(x);
		t1 = t2 = 0;
		for(int i = 1; i <= k; i++)
			if((x>>k-i)&1) q1[++t1] = a[i];
			else q2[++t2] = a[i];
		for(int i = k+1; i <= n; i++)
			q2[++t2] = a[i];
		for(int i = 1; i <= n >> 1; i++)
			if(q1[i] != q2[i]) return 0;
		return 1;
	}
	return dfs(x<<1|1) || n-countbitall(x)-1+countbit1(x) >= n/2 && dfs(x<<1);
}
bool work(){ n = read();
	for(int i = 1; i <= n; i++) a[i] = read();
	return dfs(1);
}
int main(int argc, char const *argv[]){
	File("split");
	int T = read();
	while(T--)
		puts(work() ? "Frederica Bernkastel" : "Furude Rika");
	return 0;
}
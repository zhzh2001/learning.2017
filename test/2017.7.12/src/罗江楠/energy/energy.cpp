#include <bits/stdc++.h>
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(int argc, char const *argv[]){
	File("energy");
	int n = read(), ans = 0;
	for(int i = 0; i < n; i++)
		ans = max(ans, read()+i);
	printf("%d\n", ans);
	return 0;
}
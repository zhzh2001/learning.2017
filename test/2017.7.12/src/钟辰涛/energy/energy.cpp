#include<bits\stdc++.h>

#define LL long long
#define M 1e5+7

using namespace std;

LL Max=0;
int N,A;

LL read(){
	LL x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f; 
}

int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	N=read();
	for (int i=1; i<=N; i++) {A=read();Max=max(Max,1ll*(A+i-1));}
	printf("%d",Max);
}

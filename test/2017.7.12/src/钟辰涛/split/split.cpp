#include<bits\stdc++.h>

#define LL long long

#define me(A) memset(A,0,sizeof(A));

using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

int A[101],F1[101],F2[101],N,F3[101],Q1[101],Q2[101],F4[101];

bool f;

bool P(){
	if (Q1[0]!=Q2[0]) return 0;
	for (int i=1; i<=Q1[0]; i++)
		if (Q1[i]!=Q2[i]) return 0;
	return 1;
}

void dfs(int x){
	if (f) return;
	
	if (x==N+1) {if (P()) printf("Frederica Bernkastel\n"),f=1;return;}
	if (F3[A[x]]>=F1[A[x]]/2){Q2[0]++;Q2[Q2[0]]=A[x];F4[A[x]]++;dfs(x+1);Q2[0]--;F4[A[x]]--;}else
	if (F4[A[x]]>=F1[A[x]]/2){Q1[0]++;Q1[Q1[0]]=A[x];F3[A[x]]++;dfs(x+1);Q1[0]--;F3[A[x]]--;}else
	{
		if (Q2[0]<N/2) {Q2[0]++;Q2[Q2[0]]=A[x];F4[A[x]]++;dfs(x+1);Q2[0]--;F4[A[x]]--;}
		if (Q1[0]<N/2) {Q1[0]++;Q1[Q1[0]]=A[x];F3[A[x]]++;dfs(x+1);Q1[0]--;F3[A[x]]--;}
	}
}

int main(){
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T=read();
	while (T--){
		N=read();	bool flag=1;
		me(F1);me(F2);me(A);me(F3);
		for (int i=1; i<=N; i++) A[i]=read(),F1[A[i]]++,F2[i]=F1[A[i]];
		for (int i=1; i<=N; i++) if (F1[i]&1) {printf("Furude Rika\n");flag=0;break;}
		
		if (!flag) continue;
		f=0;dfs(1);
		if (!f) printf("Furude Rika\n");
	}
}

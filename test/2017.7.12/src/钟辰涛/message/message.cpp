#include<bits\stdc++.h>

#define LL long long

using namespace std;

struct huge{
	#define N_huge 11
	#define base 100000000
	static char s[N_huge*10];
	typedef long long value;
	value a[N_huge];int len;
	void clear(){len=1;a[len]=0;}
	huge(){clear();}
	huge(value x){*this=x;}
	huge(char s[]){this->str(s);}
	huge operator =(const huge &b){len=b.len;for (int i=1;i<=len;++i)a[i]=b.a[i]; return *this;}
	huge operator +(const huge &b){
	int L=len>b.len?len:b.len;huge tmp;for (int i=1;i<=L+1;++i)tmp.a[i]=0;for (int i=1;i<=L;++i){if (i>len)tmp.a[i]+=b.a[i];
	else if (i>b.len)tmp.a[i]+=a[i];else {tmp.a[i]+=a[i]+b.a[i];if (tmp.a[i]>=base){tmp.a[i]-=base;++tmp.a[i+1];}}}
	if (tmp.a[L+1])tmp.len=L+1;else tmp.len=L;return tmp;}
	huge operator -(huge b){
	int L=len>b.len?len:b.len;huge tmp;for (int i=1;i<=L+1;++i)tmp.a[i]=0;
	for (int i=1;i<=L;++i){if (i>b.len)b.a[i]=0;tmp.a[i]+=a[i]-b.a[i];if (tmp.a[i]<0){tmp.a[i]+=base;--tmp.a[i+1];}}
	while (L>1&&!tmp.a[L])--L;tmp.len=L;return tmp;}
	huge operator *(const huge &b)const{
	int L=len+b.len;huge tmp;for (int i=1;i<=L;++i)tmp.a[i]=0;
	for (int i=1;i<=len;++i)for (int j=1;j<=b.len;++j){tmp.a[i+j-1]+=a[i]*b.a[j];
	if (tmp.a[i+j-1]>=base){tmp.a[i+j]+=tmp.a[i+j-1]/base;tmp.a[i+j-1]%=base;}}
	tmp.len=len+b.len;while (tmp.len>1&&!tmp.a[tmp.len])--tmp.len;return tmp;}
	pair<huge,huge> divide(const huge &a,const huge &b){int L=a.len;huge c,d;
	for (int i=L;i;--i){c.a[i]=0;d=d*base;d.a[1]=a.a[i];int l=0,r=base-1,mid;
	while (l<r){mid=(l+r+1)>>1;if (b*mid<=d)l=mid;else r=mid-1;}c.a[i]=l;d-=b*l;}
	while (L>1&&!c.a[L])--L;c.len=L;return make_pair(c,d);}
	huge operator /(value x){value d=0;huge tmp;for (int i=len;i;--i){d=d*base+a[i];tmp.a[i]=d/x;d%=x;}
	tmp.len=len;while (tmp.len>1&&!tmp.a[tmp.len])--tmp.len;return tmp;}
	value operator %(value x){value d=0;for (int i=len;i;--i)d=(d*base+a[i])%x;return d;}
	huge operator /(const huge &b){return divide(*this,b).first;}
	huge operator %(const huge &b){return divide(*this,b).second;}
	huge &operator +=(const huge &b){*this=*this+b;return *this;}
	huge &operator -=(const huge &b){*this=*this-b;return *this;}
	huge &operator *=(const huge &b){*this=*this*b;return *this;}
	huge &operator ++(){huge T;T=1;*this=*this+T;return *this;}
	huge &operator --(){huge T;T=1;*this=*this-T;return *this;}
	huge operator +(value x){huge T;T=x;return *this+T;}
	huge operator -(value x){huge T;T=x;return *this-T;}
	huge operator *(value x){huge T;T=x;return *this*T;}
	huge operator =(value x){len=0;while (x)a[++len]=x%base,x/=base;if (!len)a[++len]=0;return *this;}
	bool operator <(const huge &b){if (len<b.len)return 1;if (len>b.len)return 0;
	for (int i=len;i;--i){if (a[i]<b.a[i])return 1;if (a[i]>b.a[i])return 0;}return 0;}
	bool operator ==(const huge &b){if (len!=b.len)return 0;for (int i=len;i;--i)if (a[i]!=b.a[i])return 0;return 1;}
	bool operator !=(const huge &b){return !(*this==b);}
	bool operator >(const huge &b){return !(*this<b||*this==b);}
	bool operator <=(const huge &b){return (*this<b)||(*this==b);}
	bool operator >=(const huge &b){return (*this>b)||(*this==b);}
	void str(char s[]){int l=strlen(s);value x=0,y=1;len=0;
	for (int i=l-1;i>=0;--i){x=x+(s[i]-'0')*y;y*=10;if (y==base)a[++len]=x,x=0,y=1;}if (!len||x)a[++len]=x;
	}
};char huge::s[N_huge*10];

queue<char>Q;

string f[1001];
string F,F1;
int n,r,k;
LL Ans;

void init(){
	F=f['A'];
	while (1){bool ff=1;F1.clear();
	for (int i=0; i<F.size(); i++)  {if (F[i]>='0' && F[i]<='9') F1+=F[i];if (F[i]>='A' && F[i]<='Z') ff=0,F1+=f[F[i]];}
	F=F1;if (ff) break;}
}
bool pe(int l,int r){
	huge A;
	for (int i=l; i<=r; i++) A=A*10+F[i]-48;
	if (A%n==0) return 1; else return 0;
}
void work(){
	for (int i=0; i<F.size(); i++)
		for (int j=i; j<F.size(); j++)
			{
				if (i==j && F[i]=='0') {Ans=(Ans+1)%r;continue;}
				if (F[i]=='0') break;if (pe(i,j)) Ans=(Ans+1)%r;
			}
}
int main(){
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%d%d",&n,&r);
	scanf("%d",&k);
	for (int i=1; i<=k; i++)	{
		char x,ch;string a;
		cin>>x;cin>>ch>>ch;cin>>a;
		f[x]=a;
	}
	init();
	work();
	printf("%lld",1ll*Ans);
}

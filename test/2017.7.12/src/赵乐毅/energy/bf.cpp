#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<iostream>
using namespace std;
const int N=1e5+5;
int a[N],n;
long long ans;
inline bool check(int xx)
{
	for (int i=1;i<=n;i++)
	{
		if (a[i]>(xx-i+1))return false;
	}
	return true;
}
int main()
{
	freopen("energy.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for (long long i=1;i<=9223372036854775807;i++)
	{
		if (check(i)){cout<<i<<endl;exit(0);}
	}
}

#include <cstdio>
#include <cstring>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define FILE(x) freopen(x".in", "r", stdin);freopen(x".out", "w", stdout);
#define N 40
int s1n, s2n, n;
int a[N], b[N], s1[N>>1], s2[N>>1];
bool flag;

int getint() {
	int x = 0, f = 1;
	char c = getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') f=-f;
	for(; c>='0'&&c<='9'; c=getchar()) x=x*10+c-'0';
	return x*f;
}

int main() {
	FILE("split");
	for(int T = getint(); T; T--) {
		memset(b, 0, sizeof(b));
		memset(a, 0, sizeof(a));
		memset(s1, 0, sizeof(s1));
		memset(s2, 0, sizeof(s2));
		s1n = s2n = 0;
		flag = true;
		n = getint();
		lop(i, 1, n) a[i] = getint(), ++b[a[i]];
		lop(i, 1, 40) if(b[i]&1) { flag = false; break; } else b[i] >>= 1;
		if(!flag) { printf("Furude Rika\n"); continue; }
		lop(i, 1, n)
			if(b[a[i]]) s1[++s1n] = a[i], --b[a[i]];
			else s2[++s2n] = a[i];
		lop(i, 1, n>>1)
			if(s1[i] != s2[i]) { flag = false; break; }
		printf(flag? "Frederica Bernkastel\n" : "Furude Rika\n");
	}
	return 0;
}


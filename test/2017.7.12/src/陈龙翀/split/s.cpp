#include<cstdio>
int n,f[100010],a[100010],flag,C,T;
void ok()
{
	int j=1,i;
	for (i=1; i<=n; i++)
	{
		if (f[i])
		{
			while((j<=n&&a[j]!=a[i])||(j<=n&&f[j]))
				j++;
			if (j>n) return;
		}
	}
	if (i==n+1&&j<=n) puts("Frederica Bernkastel"),flag=1;
}
void dfs(int x,int cnt)
{
	if (cnt>C) return;
	if (x>n) return;
	if (cnt==n/2) ok();
	if (flag) return;
	f[x]=1;
	dfs(x+1,cnt+1);
	f[x]=0;
	dfs(x+1,cnt);
}
int main()
{
	freopen("split.in","r",stdin);
	freopen("s.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		C=n/2;
		flag=0;
		for (int i=1; i<=n; i++)
			scanf("%d",&a[i]);
		dfs(1,0);
		if (!flag) puts("Furude Rika");
	}
}

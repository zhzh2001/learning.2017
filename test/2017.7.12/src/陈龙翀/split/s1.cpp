#include<cstdio>
int f[100],a[100],l,r,n,T;
int dfs(int x)
{
	if (x>n) return l>r;
	if (a[x]==f[l]&&l<=r)
	{
		l++;
		if (dfs(x+1)) return 1;
		f[--l]=a[x];
	}
	f[++r]=a[x];
	if (dfs(x+1)) return 1;
	r--;
	return 0;
}
int main()
{
	freopen("split.in","r",stdin);
	freopen("s1.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for (int i=1; i<=n; i++)
			scanf("%d",&a[i]);
		l=1,r=0;
		if (dfs(1))
			puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
}

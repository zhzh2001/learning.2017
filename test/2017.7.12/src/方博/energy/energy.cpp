#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>

using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}
const int maxn=100003;
const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;
int n;
int a[maxn];
int need[maxn];
int maxneed=-inf;
int main()
{
	
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	read(n);
	for(int i=1;i<=n;++i)
	{
		read(a[i]);
		need[i]=a[i]+i-1;
		if(need[i]>maxneed)
			maxneed=need[i];
	}
	printf("%d\n",maxneed);
	return 0;
}

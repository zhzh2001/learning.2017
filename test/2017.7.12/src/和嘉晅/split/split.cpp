#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
int n; int g[45];
int e[45]; int t1[45],t2[45];
bool ok=0;
bool cal()
{
	int p1=0,p2=0;
	for(int i=1;i<=n;i++)
		if(e[i]) t1[++p1]=g[i];
		else t2[++p2]=g[i];
	if(p1!=p2) return 0;
	for(int i=1;i<=(n>>1);i++)
		if(t1[i]!=t2[i])
			return 0;
	return 1;
}
int cnt[45];int now[45]; int tot;
int lim=100000000;
void dfs(int dep,int c)
{
	if(ok) return ;
	if(++tot>lim){ok=1; return ;}
	if(dep>n)
	{
		ok|=cal();
		return ;
	}
	if(c+(n-dep+1)<(n>>1)) return ;
	if(c>(n>>1)) return ;
	dfs(dep+1,c);
	e[dep]=1;
	now[g[dep]]++;
	if(now[g[dep]]>(cnt[g[dep]]>>1)){now[g[dep]]--; return ;}
	dfs(dep+1,c+1);
	e[dep]=0;
	now[g[dep]]--;
}
int l[45],r[45];
int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		if(tot>lim) tot-=(lim>>1),lim>>=1;
		scanf("%d",&n);
		for(int i=1;i<=n;i++) cnt[i]=0,l[i]=45;
		for(int i=1;i<=n;i++) 
			scanf("%d",&g[i]),cnt[g[i]]++,l[g[i]]=min(l[g[i]],i),r[g[i]]=max(r[g[i]],i);
		ok=1;
		for(int i=1;i<=n;i++)
			if(cnt[i]&1)
				ok=0;
		for(int i=1;i<=n;i++)
		{
			for(int j=i+1;j<=n;j++)
				if(g[i]==g[j]&&cnt[g[i]]==2)
				{
					for(int k=i+1;k<=j;k++)
						for(int d=k+1;d<=j;d++)
							if(g[k]==g[d]&&cnt[g[k]]==2)
							{
								ok=0;
								break;
							}
					if(!ok) break;
				}
			if(!ok) break;
		}
		
		if(!ok)
		{
			 puts("Furude Rika");
			 continue;
		}
		ok=0;
		dfs(1,0);
		if(ok)
		puts("Frederica Bernkastel");
		else
		puts("Furude Rika");
	}
	return 0;
}

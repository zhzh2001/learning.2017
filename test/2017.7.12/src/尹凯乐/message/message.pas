program message;
 var
  s:array[0..27] of string;
  i,j,k,n,r,s2,u:longint;
  p:int64;
  c,s1:string;
 begin
  assign(input,'message.in');
  assign(output,'message.out');
  reset(input);
  rewrite(output);
  readln(n,r);
  readln(k);
  for i:=1 to k do
   begin
    readln(c);
    s[i]:=copy(c,4,length(c)-3);
   end;
  s1:=s[1];
  u:=0;
  while u=0 do
   begin
    u:=1;
    for i:=1 to length(s1) do
     if (s1[i]>='A') and (s1[i]<='Z') then
      begin
       s1:=copy(s1,1,i-1)+s[ord(s1[i])-64]+copy(s1,i+1,length(s1)-i);
       u:=0;
       break;
      end;
   end;
  s2:=0;
  for i:=1 to length(s1) do
   for j:=i to length(s1) do
    begin
     p:=0;
     for k:=i to j do
      p:=p*10+ord(s1[k])-48;
     if p=0 then
      if j-i=0 then inc(s2)
               else p:=p
            else if (p mod n=0) and (s1[i]<>'0') then inc(s2);
    end;
  writeln(s2 mod r);
  close(input);
  close(output);
 end.

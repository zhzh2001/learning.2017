program energy;
 var
  i,n,x,s:longint;
 begin
  assign(input,'energy.in');
  assign(output,'energy.out');
  reset(input);
  rewrite(output);
  readln(n);
  s:=0;
  for i:=1 to n do
   begin
    read(x);
    if x+i-1>s then s:=x+i-1;
   end;
  readln;
  writeln(s);
  close(input);
  close(output);
 end.

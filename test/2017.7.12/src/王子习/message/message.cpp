#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e6+7;
char s[30][107];
int a[N],now;
int n,P,k;
inline int check(int x){
	if (x>=P) return x-P;
	else return x;
}

void dfs(int x){
	int len=strlen(s[x]);
	For(i,0,len-1){
		if (s[x][i]>='0'&&s[x][i]<='9'){
			a[++now]=s[x][i]-'0';
			continue;
		}
		else dfs(s[x][i]-'A'+1);
	}	
}

int main(){
	// say hello

	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);

	n=read();P=read();
	k=read();
	For(i,1,k){
		char ch=' ';
		while (ch!='>') ch=getchar();
		scanf("%s",s[i]);
	}
	now=0;
	dfs(1);
	int ans=0;
	For(i,1,now){
		if (a[i]==0){
			ans++;
			ans=check(ans);
			continue;
		}
		int tmp=0;
		For(j,i,now){
			tmp=tmp*10+a[j];
			tmp%=n;
			if (tmp==0){
				ans++;
				ans=check(ans);
			}
		}
	}
	printf("%d\n",ans);

	// say goodbye
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e5;
int n,a[N],f1[N],f2[N],flag;
int top1,top2;

void dfs(int x){
	if (flag) return;
	if (x>n){
		printf("Frederica Bernkastel\n");
		flag=1;
		return;
	}
	if (top1==n/2){
		if (f1[top2+1]!=a[x]) return;
		f2[++top2]=a[x];
		dfs(x+1);
		top2--;
	}
	else if (top2==n/2){
		if (f2[top1+1]!=a[x]) return;
		f1[++top1]=a[x];
		dfs(x+1);
		top1--;
	}
	else {
		if (top1>=top2||f2[top1+1]==a[x]){
			f1[++top1]=a[x];
			dfs(x+1);
			top1--;
		}
		if (top2>=top1||f1[top2+1]==a[x]){
			f2[++top2]=a[x];
			dfs(x+1);
			top2--;
		}
	}
}

int main(){
	// say hello

	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);

	int T=read();
	For(p,1,T){
		n=read();
		For(i,1,n) a[i]=read();
		top1=0,top2=0;
		flag=0;
		dfs(1);
		if (!flag) printf("Furude Rika\n");
	}

	// say goodbye
}


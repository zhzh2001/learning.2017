#include<bits/stdc++.h>
using namespace std;
 inline int read()
{
    int data=0,w=1; char ch=0;
    while(ch!='-' && (ch<'0' || ch>'9')) ch=getchar();
    if(ch=='-') w=-1,ch=getchar();
    while(ch>='0' && ch<='9') data=data*10+ch-'0',ch=getchar();
    return data*w;
}

int t,n,avg;
int a[45];
bool f[45];
 bool solve1()
{
	int l=2;
	bool flag1=false,flag2;
	memset(f,false,sizeof(f));
	for (int i=1;i<=n;++i)
		if (!f[i])
		{
			flag2=false;
			f[i]=true;
			while (l<=n)
			{
				if (l-i>avg)
				{
					flag1=true;
					break;
				}
				if (!f[l]&&a[l]==a[i]) 
				{
					f[l]=true;
					++l;
					flag2=true;
					break;
				}
				++l;
			}
			if (flag1) break;
			if (!flag2) break;
		}
	if (!flag2||flag1) return false;
	else return true; 
} 
 bool solve2()
{
	int l=n-1;
	bool flag1=false,flag2;
	memset(f,false,sizeof(f));
	for (int i=n;i>=1;--i)
		if (!f[i])
		{
			flag2=false;
			f[i]=true;
			while (l>=1)
			{
				if (i-l>avg)
				{
					flag1=true;
					break;
				}
				if (!f[l]&&a[l]==a[i]) 
				{
					f[l]=true;
					--l;
					flag2=true;
					break;
				}
				--l;
			}
			if (flag1) break;
			if (!flag2) break;
		}
	if (!flag2||flag1) return false;
	else return true; 
} 
 int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	t=read();
	while (t--)
	{
		n=read();
		for (int i=1;i<=n;++i) a[i]=read();
		avg=n/2;
		if (solve1()||solve2())
			cout<<"Frederica Bernkastel"<<endl;
		else cout<<"Furude Rika"<<endl;
	}
}












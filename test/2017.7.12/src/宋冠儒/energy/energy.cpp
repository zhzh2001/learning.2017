#include<bits/stdc++.h>
#define ll long long
using namespace std;
int a[100010],n;
 inline int read()
{
    int data=0,w=1; char ch=0;
    while(ch!='-' && (ch<'0' || ch>'9')) ch=getchar();
    if(ch=='-') w=-1,ch=getchar();
    while(ch>='0' && ch<='9') data=data*10+ch-'0',ch=getchar();
    return data*w;
}

 int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();
	for (int i=1;i<=n;++i) a[i]=read();
	ll sum=0,ans=0;
	for (int i=1;i<=n;++i)
	{
		if (sum<a[i])
		{
			ans+=(a[i]-sum);
			sum=a[i];
		}
		sum--;
	}
	printf("%lld",ans);
}

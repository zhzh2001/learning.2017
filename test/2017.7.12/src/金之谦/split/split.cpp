#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int a[10001],q[10001],n;
inline bool dfs(int l,int p,int r){
	if(p==l)q[++l]=a[r++];
	while(q[p+1]!=a[r]){
		q[++l]=a[r];r++;
		if(r>n)return 0;
	}
	if(r==n&&p+1==l)return 1;
	if(dfs(l,p+1,r+1))return 1;
	else{
		q[l+1]=a[r];
		return dfs(l+1,p,r+1);
	}
}
int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T=read();while(T--){
		n=read();
		for(int i=1;i<=n;i++)a[i]=read();
		q[1]=a[1];
		puts(dfs(1,0,2)?"Frederica Bernkastel":"Furude Rika");
	}
	return 0;
}
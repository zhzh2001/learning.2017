#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
bool vis[10001];
int a[10001],b[10001],n,flag;
inline bool check(){
	int k=1;
	for(int i=1;i<=n;i++)if(!vis[i]){
		if(a[i]!=a[b[k]])return 0;
		k++;
	}
	return 1;
}
inline void dfs(int x,int y){
	if(flag)return;
	if(!x){if(check())flag=1;return;}
	if(n-y+1<x)return;
	for(int i=y;i<=n;i++)b[n/2-x+1]=i,vis[i]=1,dfs(x-1,i+1),vis[i]=0;
}
int main()
{
	freopen("split.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int T=read();while(T--){
		flag=0;
		n=read();for(int i=1;i<=n;++i)a[i]=read();
		dfs(n/2,1);
		puts(flag?"Frederica Bernkastel":"Furude Rika");
	}
	return 0;
}
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline string sread(){
	string p="";char ch=getchar();
	while(ch!=' '&&ch!='\n'){p=p+ch;ch=getchar();}
	return p;
}
inline void write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(ll x){write(x);puts("");}
bool vis[128];ll m,MOD,ans=0;
ll f[2][41];
string a[128];
int n;
inline void dfs(char c){
	if(vis[c])return;string p="";
	int l=a[c].size();
	for(int i=0;i<l;++i)if(a[c][i]>='A'&&a[c][i]<='Z'){
		dfs(a[c][i]);p=p+a[a[c][i]];
	}else p=p+a[c][i];
	a[c]=p;vis[c]=1;
}
int main()
{
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	m=read();MOD=read();n=read();
	string p;
	for(int i=1;i<=n;i++){
		p=sread();char c=p[0];p.erase(0,3);int l=p.size();
		a[c]=p;vis[c]=1;for(int j=0;j<l;++j)if(a[c][j]>='A'&&a[c][j]<='Z'){vis[c]=0;break;}
	}
	dfs('A');int l=a['A'].size();
	for(int i=0;i<l;++i){
		for(int j=0;j<m;++j)f[(i+1)&1][(j+a['A'][i]%m)%m]=f[i&1][j];
		if(a['A'][i]>'0')(f[(i+1)&1][a['A'][i]%m]+=1)%=MOD;
		else ans=ans==MOD?0:ans+1;
		ans=(ans+f[(i+1)&1][0])%MOD;
	}
	writeln(ans);
	return 0;
}
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	int n=read(),ans=0;
	for(int i=1;i<=n;i++){
		int x=read();
		ans=max(ans,x+i-1);
	}
	writeln(ans);
	return 0;
}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
int n;
int max(int a,int b){
	if (a>b) return a; else return b;
}
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d",&n);
	int ans=0;
	for (int i=1,x;i<=n;i++){
		scanf("%d",&x);
		ans=max(ans,x+i-1);
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
#define MAXN 50
int f[MAXN][MAXN],a[MAXN],n;//pos[MAXN][MAXN],lst[MAXN];
int main(){
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		int ans=0,flag,max;
		memset(f,0,sizeof(f));
		for(int i=1;i<=n;i++) scanf("%d",&a[i]);
		for(int i=1;i<=n;i++){
			for(int j=1;j<i;j++){
				if(a[i]==a[j]) flag=1;
				else flag=0;
				max=0;
				for(int x=1;x<i;x++){
					if(x==j)continue;
					for(int y=1;y<j;y++){
						if(max<f[x][y]){
							max=f[x][y];
						}
					}
				}
				f[i][j]=max+flag;
				if(f[i][j]==n/2) ans=1;
			}
		}
		if(ans) printf("Frederica Bernkastel\n");
		else printf("Furude Rika\n");
	}
	return 0;
}

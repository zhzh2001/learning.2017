#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;
#define MAXN 100010
int a[MAXN],b[MAXN],ans,Max=(1<<31),maxid,n;
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]),b[i]=a[i]-n+i;
	for(int i=1;i<=n;i++){
		if(b[i]>Max){
			Max=b[i];
			maxid=i;
		}
	}
	ans=n-1+a[maxid]-n+maxid;
	printf("%d",ans);
}

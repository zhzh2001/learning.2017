#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
int n,r,k,Len[30];
char s[30][1005],S[30][1005];
bool vis[26];
void dfs(int x){
	vis[x]=1;
	Len[x]=0;
	for (int i=0,t;s[x][i];i++)
	if (s[x][i]>='A' && s[x][i]<='Z'){
		t=s[x][i]-'A';
		if (!vis[t]) dfs(t);
		for (int j=0;j<Len[t];j++)
			S[x][Len[x]++]=S[t][j];
	}
	else  S[x][Len[x]++]=s[x][i];
}
int main(){
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%d%d%d",&n,&r,&k);
	for (int i=1;i<=k;i++){
		char ch=getchar(),CH;
		while (ch<'A' || ch>'Z') ch=getchar();
		CH=getchar(); CH=getchar();
		scanf("%s",&s[ch-'A']);
	}
	memset(vis,0,sizeof(vis));
	dfs(0);
	int ans=0;
	for (int i=0;i<Len[0];i++){
		if (S[0][i]=='0'){
			ans++;
			continue;
		}
		int res=0;
		for (int j=i;j<Len[0];j++){
			res=(res*10+S[0][j]-'0')%n;
			if (!res){
				ans++;
				if (ans>=r) ans%=r;
			}
		}
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

//#include<iostream>
#include<algorithm>
#include<fstream>
using namespace std;
ifstream fin("energy.in");
ofstream fout("energy.out");
long long n,ans=-99999999;
long long a[500003];
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>a[i];
		ans=max(a[i]+(i-1),ans);
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
5
1 2 5 4 2

out:
7

*/

//#include<iostream>
#include<algorithm>
#include<cstring>
#include<fstream>
using namespace std;
ifstream fin("message.in");
ofstream fout("message.out");
int n,r,k;
char s[30][103],s2[30][10003];
bool vis[30];
int nowlen[30];
long long ans;
void dfs(int x){
	if(vis[x]==true){
		return;
	}
	vis[x]=true;
	nowlen[x]=0;
	for(int i=0;i<=strlen(s[x])-1;i++){
		if(s[x][i]>='A'&&s[x][i]<='Z'){
			int hei=s[x][i]-'A'+1;
			dfs(hei);
			int j;
			for(j=1;j<=nowlen[hei];j++){
				s2[x][j+nowlen[x]]=s2[hei][j];
			}
			nowlen[x]=nowlen[x]+nowlen[hei];
		}else{
			nowlen[x]++;
			s2[x][nowlen[x]]=s[x][i];
		}
	}
}
int jiyi[100003][33];
int dp(int now,int mo){
	int & fanhui=jiyi[now][mo];
	if(fanhui!=-1){
		return fanhui;
	}
	fanhui=0;
	if(mo==0){
		fanhui=1;
	}
	if(now==nowlen[1]+1){
		return fanhui;
	}
	fanhui=(fanhui+dp(now+1,((mo*10+s2[1][now]-'0')%n)))%r;
	return fanhui;
}
int main(){
	fin>>n>>r>>k;
	memset(jiyi,-1,sizeof(jiyi));
	for(int i=1;i<=k;i++){
		char opt,opt1,opt2;
		fin>>opt>>opt1>>opt2;
		fin>>s[i];
	}
	dfs(1);
	int x=0;
	for(int i=1;i<=nowlen[1];i++){
		if(s2[1][i]=='0'){
			ans++;
			ans%=r;
		}else{
			x++;
			ans+=dp(i,0);
			ans%=r;
		}
	}
	ans=(ans-x+r)%r;
	fout<<ans<<endl;
	return 0;
}
/*

in:
2 1000000000
3
A->BB
B->CC0
C->123

out:
46

*/
/*

in:
2 1000000000
1
A->000002

out:
6

*/

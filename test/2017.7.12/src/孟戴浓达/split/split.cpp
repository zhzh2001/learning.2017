//#include<iostream>
#include<algorithm>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("split.in");
ofstream fout("split.out");
bool yes;
int num[43],num1[43],num2[43],tong[43];
int n,T;
void dfs(int len,int i,int j){
	if(yes==true){
		return;
	}
	if(i>(n/2+1)){
		return;
	}
	if(j>(n/2+1)){
		return;
	}
	if(num1[min(i,j)-1]!=num2[min(i,j)-1]){
		return;
	}
	if(len==n+1&&(i==n/2+1)&&(j==n/2+1)){
		yes=true;
		return;
	}
	if(yes==false){
		num1[i]=num[len];
		dfs(len+1,i+1,j);
	}
	if(yes==false){
		num2[j]=num[len];
		dfs(len+1,i,j+1);
	}
}
int main(){
	fin>>T;
	while(T--){
		fin>>n;
		for(int i=0;i<=40;i++){
			tong[i]=0;
		}
		for(int i=1;i<=n;i++){
			fin>>num[i];
			tong[num[i]]++;
		}
		yes=false;
		for(int i=0;i<=40;i++){
			if(tong[i]%2!=0){
				fout<<"Furude Rika"<<endl;
				yes=true;
				break;
			}
		}
		if(yes==true){
			continue;
		}
		dfs(1,1,1);
		if(yes==true){
			fout<<"Frederica Bernkastel"<<endl;
		}else{
			fout<<"Furude Rika"<<endl;
		}
	}
	return 0;
}
/*

in:
3
4
1 1 2 2
6
1 2 3 4 5 6
4
1 2 2 1

out:
Frederica Bernkastel
Furude Rika
Furude Rika

*/
/*
in:
1 1 10 2 1 1 2 1 10 1 2 1 2 0 0 4 4 3 1 3 1 5 5 1 9 9 1 2 2 1 1 8 8 1 1 1 1 1 1 1
in:
1 1 10 2 1 1 2 1 10 2 1 1 2 1
*/

#include <cstring>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cmath>
using namespace std;
typedef long long LL;
const int L=100+10,N=30+5,K=26+4;
LL mod,prev[K][L][N][N],prez[K][N][N],tail[K][N],all[K],lenmod[K];
int n,k,to[K][L];
char str[L];
void dfs(int rt){
	if (lenmod[rt]!=-1)
		return;
	lenmod[rt]=1;
//	LL pre[N][N];
	for (int i=1;i<=to[k][0];i++){
//		for (int a=0;a<n;a++)
//			for (int b=0;b<n;b++)
//				pre[a][b]=prev[rt][a][b];
		if (to[k][i]<10){
			for (int a=0;a<n;a++)
				for (int b=0;b<n;b++){
//					if (pre[a][b]==-1)
//						continue;
					if (prev[rt][i-1][a][b]==-1)
						continue;
					int ato=(a*10+to[k][i])%n,bto=b*10%n;
					if (prev[rt][i][ato][bto]==-1)
						prev[rt][i][ato][bto]=1;
					prev[rt][i][ato][bto]=prev[rt][i][ato][bto]+prev[rt][i-1][a][b];
					while (prev[rt][i][ato][bto]>=mod)
						prev[rt][i][ato][bto]-=mod;
				}
			lenmod[rt]=lenmod[rt]*10%mod;
		}
		else {
			int pos=to[k][i]-10;
			dfs(pos);
			
		}
	}
}
int main(){
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%d%lld%d",&n,&mod,&k);
	for (int i=1;i<=k;i++){
		gets(str);
		int len=strlen(str);
		int pos=str[0]-'A';
		to[pos][0]=len-3;
		for (int j=3;j<len;j++)
			if ('0'<=str[j]&&str[j]<='9')
				to[pos][j-2]=str[j]-48;
			else
				to[pos][j-2]=str[j]-'A'+10;
	}
	memset(lenmod,-1,sizeof lenmod);
	memset(prev,-1,sizeof prev);
	memset(tail,-1,sizeof tail);
	memset(all,-1,sizeof all);
	dfs(0);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstring>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cmath>
using namespace std;
typedef long long LL;
const int L=100+10,N=30+5,K=26+4;
LL mod,prev[K][N][N],tail[K][N],all[K],lenmod[K],ans;
int n,k,to[K][L];
char str[L];
void dfs(int rt){
	if (lenmod[rt]!=-1)
		return;
	lenmod[rt]=1;
	LL pre[N],pre_[N];
	memset(pre,0,sizeof pre);
	for (int i=1;i<=to[k][0];i++){
		if (to[k][i]<10){
			for (int j=0;j<n;j++){
				int ato=(j*10+to[k][i])%n,bto=j*10%n;
				prev[rt][ato][bto]=(prev[rt][ato][bto]+pre[j])%mod;
			}
			lenmod[rt]=lenmod[rt]*10%n;
			memset(pre_,0,sizeof pre_);
			for (int y=0;y<n;y++)
				pre_[(y*10+to[k][i])%n]=(pre_[(y*10+to[k][i])%n]+pre[y])%mod;
		}
		else {
			int pos=to[k][i]-10;
			dfs(pos);
			for (int j=0;j<n;j++)
				for (int a=0;a<n;a++)
					for (int b=0;b<n;b++){
						int ato=(j*b+a)%n,bto=j*b%n;
						prev[rt][ato][bto]=(prev[rt][ato][bto]+pre[j]*prev[pos][a][b])%mod;
					}
			lenmod[rt]=lenmod[rt]*lenmod[pos]%n;
			memset(pre_,0,sizeof pre_);
			for (int y=0;y<n;y++)
				pre_[(y*lenmod[pos]+all[pos])%n]=(pre_[(y*lenmod[pos]+all[pos])%n]+pre[y])%mod;
		}
		for (int j=0;j<n;j++)
			pre[j]=pre_[j];
	}
	int rr=1;
	memset(pre,0,sizeof pre);
	for (int i=to[k][0];i>=1;i--){
		if (to[k][i]<10){
			int num=to[k][i];
			if (num>0)
				for (int j=0;j<n;j++)
					tail[rt][(j+rr*num)%n]=(tail[rt][(j+rr*num)%n]+pre[j])%mod;
			rr=rr*10%n;
			memset(pre_,0,sizeof pre_);
			for (int y=0;y<n;y++)
				pre_[(y+rr*num)%n]=(pre_[(y+rr*num)%n]+pre[y])%mod;
		}
		else {
			int pos=to[k][i]-10;
			for (int j=0;j<n;j++)
				for (int k=0;k<n;k++)
					tail[rt][(j+rr*num)%n]=(tail[rt][(j+rr*num)%n]+pre[j]*tail[pos][k])%mod;
			memset(pre_,0,sizeof pre_);
			for (int y=0;y<n;y++)
				for (int s=0;s<n;s++)
					pre_[(y+lenmod[pos]*s)%n]=(pre_[(y+lenmod[pos]*s)%n]+pre[y]*all[pos][s])%mod;
			rr=rr*lenmod[pos]%n;
		}
		for (int j=0;j<n;j++)
			pre[j]=pre_[j];
	}
	for (int i=to[rt][0];i>1;i--){
		int pre_val,pre_rr;
		for (int a=0;a<n;a++)
			for (int b=0;b<n;b++){
				pre_val=a,pre_rr=b;
				for (int j=i-1;j>=1;j--){
					int til=
				}
			}
	}
}
int main(){
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%d%lld%d",&n,&mod,&k);
	for (int i=1;i<=k;i++){
		gets(str);
		int len=strlen(str);
		int pos=str[0]-'A';
		to[pos][0]=len-3;
		for (int j=3;j<len;j++)
			if ('0'<=str[j]&&str[j]<='9')
				to[pos][j-2]=str[j]-48;
			else
				to[pos][j-2]=str[j]-'A'+10;
	}
	memset(lenmod,-1,sizeof lenmod);
	memset(prev,0,sizeof prev);
	memset(tail,0,sizeof tail);
	memset(all,0,sizeof all);
	ans=0;
	dfs(0);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>

using namespace std;
template<typename T>
void read(T& A)
{
	char r;
	bool f=false;
	for(r=getchar(); (r<48||r>57)&&r!='-'; r=getchar());
	if(r=='-')
	{
		f=true;
		r=getchar();
	}
	for(A=0; r>=48&&r<=57; r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b)
{
	return a>b?a:b;
}
template<typename T>
T Min(const T& a,const T& b)
{
	return a<b?a:b;
}
template<typename T>
T Swap(const T& a,const T& b)
{
	T t=a;
	a=b;
	b=t;
}
const int maxn=43;
const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;
int TT,n,a[maxn];
int cnt[maxn];
int maxi;
bool can;

bool prejudge()
{
	for(int i=1; i<=maxi; ++i)
	{
		if(cnt[i]&1)
			return false;
	}
	for(int i=2; i<=n/2+1; ++i)
	{
		if(a[i]==a[1])
			return true;
	}
	return false;
}
void input()
{
	read(n);
	for(int i=1; i<=n; ++i)
	{
		read(a[i]);
		++cnt[a[i]];
		if(a[i]>maxi)
			maxi=a[i];
	}
}
int ary1[maxn],k1;
void finding(int t,int pos)
{
	if(t==n/2+1&&pos==n+1)
	{
		can=true;
	}
	else
	{
		int tmpk1=k1;
		for(int i=pos; i<=n&&n-pos+1>=n/2-t+1&&!can; ++i)
		{
			if(a[i]==a[ary1[t]])
			{
				finding(t+1,i+1);
			}
			ary1[++k1]=i;
		}
		k1=tmpk1;
	}

}
inline void Init()
{
	can=false;
	memset(cnt,0,sizeof(cnt));
	maxi=-inf;
	k1=0;
}
int main()
{

	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	read(TT);
	for(int e=1; e<=TT; ++e)
	{
		Init();
		input();
		if(prejudge())
		{
			ary1[++k1]=1;
			finding(k1,2);
		}

		if(can)
			printf("Frederica Bernkastel\n");
		else
			printf("Furude Rika\n");
	}
	return 0;
}

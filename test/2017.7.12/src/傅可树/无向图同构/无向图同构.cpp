#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxm=8005,maxn=205,base=23333,mod=998844353;
struct pp{
	int link,next;
}e[maxm];
int n,m,bin[maxn],T,tot,hash[maxn],head[maxn],q[maxn],hash1[maxn],hash2[maxn];
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void add(int u,int v){e[++tot]=(pp){v,head[u]}; head[u]=tot;}
inline void change()
{
	int H[maxn];
	for (int i=1;i<=n;i++) H[i]=0;
	for (int i=1;i<=n;i++){
		int cnt=0;
		for (int j=head[i];j;j=e[j].next) q[++cnt]=hash[e[j].link];
		sort(q+1,q+1+cnt); 
		for (int j=1;j<=cnt;j++) H[i]=(1ll*H[i]*bin[j-1]+q[j])%mod;
	}
	for (int i=1;i<=n;i++) hash[i]=H[i];
}
inline bool judge()
{
	for (int i=1;i<=n;i++) if (hash1[i]!=hash2[i]) return 0;
	return 1;
}
int main()
{
	T=read(); bin[0]=1;
	for (int i=1;i<=200;i++) bin[i]=bin[i-1]*base%mod;
	while (T--){
		n=read(); m=read(); memset(head,0,sizeof(head)); tot=0;
		for (int i=1;i<=m;i++) {int u=read(),v=read(); add(u,v); add(v,u);}
		for (int i=1;i<=n;i++) hash[i]=1;
		for (int i=1;i<=n;i++) change();
		for (int i=1;i<=n;i++) hash1[i]=hash[i];
		memset(head,0,sizeof(head)); tot=0;
		for (int i=1;i<=m;i++) {int u=read(),v=read(); add(u,v); add(v,u);}
		for (int i=1;i<=n;i++) hash[i]=1;
		for (int i=1;i<=n;i++) change();
		for (int i=1;i<=n;i++) hash2[i]=hash[i];
		sort(hash1+1,hash1+n+1); sort(hash2+1,hash2+1+n);
		if (judge()) printf("YES\n"); else printf("NO\n"); 
	}
	return 0;
}

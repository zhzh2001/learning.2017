#include<cstdio>
#include<ext/pb_ds/priority_queue.hpp>
#include<algorithm>
#define pa pair<ll,int>
using namespace std;
using namespace __gnu_pbds;
typedef long long ll;
typedef __gnu_pbds::priority_queue<pa,greater<pa>,pairing_heap_tag > heap;
const int maxn=1000005;
const ll inf=1e16;
struct pp{
	int link,next,val;
}e[10000005];
heap::point_iterator id[maxn];
ll dis[maxn];
int n,m,T,rxa,rya,rxc,ryc,rp,head[maxn],tot;
inline void insert(int u,int v,int w){e[++tot]=(pp){v,head[u],w}; head[u]=tot;}
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void dij()
{
	heap q;
	for (int i=1;i<=n;i++) dis[i]=inf;
	dis[1]=0; id[1]=q.push(make_pair(0,1));
	while (!q.empty()){
		int now=q.top().second; q.pop();
		for (int i=head[now];i;i=e[i].next){
			int u=e[i].link;
			if (dis[u]>dis[now]+e[i].val){
				dis[u]=dis[now]+e[i].val;
				if (id[u]!=0) q.modify(id[u],make_pair(dis[u],u));
					else id[u]=q.push(make_pair(dis[u],u));
			}
		}
	}
}
int main()
{
	n=read(); m=read(); T=read(); rxa=read(); rxc=read(); rya=read(); ryc=read(); rp=read();
	for (int i=1;i<=T;i++){
		int x=((ll)x*rxa+rxc)%rp,y=((ll)y*rya+ryc)%rp,a=min(x%n+1,y%n+1),b=max(x%n+1,y%n+1);
		insert(a,b,100000000-100*a);
	}
	for (int i=1;i<=m-T;i++){
		int x=read(),y=read(),z=read();
		insert(x,y,z);
	}
	dij(); printf("%lld\n",dis[n]); return 0;
}

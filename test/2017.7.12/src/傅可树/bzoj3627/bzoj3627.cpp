#include<bits/stdc++.h>
using namespace std;
const int mod=100005,maxn=10005,maxm=100000,maxk=12,inf=1e8;
struct pp{
	int link,next;
	double val;
}e[maxm];
struct pp2{
	int link,next,stop;
	double val;
}a[maxm];
struct pp1{
	int step,id;
}q[maxn*10+5];
int head[maxn],head1[maxn],n,m,k,limit,cost,start,end,x,y,w,tot;
bool vis[maxn][maxk],gas[maxn],stop[maxn];
string s,s1,s2;
double ans,ex[maxn],dis[maxn][maxk];
map<string,int> G;
inline void add2(int u,int v,int stop,double w){a[++tot].link=v; a[tot].next=head1[u]; a[tot].stop=stop; a[tot].val=w; head1[u]=tot;}
inline void spfa1(int start)
{
	vis[start][0]=1; memset(dis,127,sizeof(dis)); int h=0,t=1; q[1]=(pp1){0,start}; dis[start][0]=0;
	while (h!=t){
		pp1 temp=q[h=h%mod+1]; int step=temp.step,now=temp.id; vis[now][step]=0;
		for (int i=head[now];i;i=e[i].next){
			int u=e[i].link;
			if (step+stop[u]<=k&&dis[u][step+stop[u]]>dis[now][step]+e[i].val){
				dis[u][step+stop[u]]=dis[now][step]+e[i].val;
				if (!vis[u][step+stop[u]]) vis[u][step+stop[u]]=1,q[t=t%mod+1]=(pp1){step+stop[u],u};
			}
		}	
	}
	for (int j=0;j<=k;j++)
		for (int i=1;i<=n;i++) if (i!=start&&gas[i]&&dis[i][j]<=limit) add2(start,i,j,dis[i][j]+cost);
}
inline void spfa2()
{
	vis[start][0]=1; memset(dis,127,sizeof(dis)); int h=0,t=1; q[1]=(pp1){0,start}; dis[start][0]=0;
	while (h!=t){
		pp1 temp=q[h=h%mod+1]; int step=temp.step,now=temp.id; vis[now][step]=0;
		for (int i=head1[now];i;i=a[i].next){
			int u=a[i].link;
			if (step+a[i].stop<=k&&dis[u][step+a[i].stop]>dis[now][step]+a[i].val){
				dis[u][step+a[i].stop]=dis[now][step]+a[i].val;
				if (!vis[u][step+a[i].stop]) vis[u][step+a[i].stop]=1,q[t=t%mod+1]=(pp1){step+a[i].stop,u};
			}
		}	
	}
}
inline void add(int u,int v,double w){e[++tot].link=v; e[tot].next=head[u]; e[tot].val=w; head[u]=tot;}
int main()
{
	scanf("%d%d%d%d%d",&n,&m,&k,&limit,&cost);
	for (int i=1;i<=n;i++){
		cin>>s; G[s]=i;
		if (s=="start") {start=i; gas[i]=1;}
		if (s=="end") {end=i; gas[i]=1;}
		if (s.find("gas")!=string::npos) gas[i]=1;
		scanf("%d%d",&x,&y);
		if (x) ex[i]=(double)(x*x)/(2.0*(x+y)),stop[i]=1;
	}
	for (int i=1;i<=m;i++){cin>>s1; cin>>s2; cin>>s; x=G[s1]; y=G[s2]; scanf("%d",&w); add(x,y,(double)w+ex[y]); add(y,x,(double)w+ex[x]);}
	tot=0; for (int i=1;i<=n;i++) if (gas[i]) spfa1(i);
	spfa2(); ans=inf;
	for (int i=0;i<=k;i++) ans=min(ans,dis[end][i]);
	printf("%.3lf\n",ans-cost);
	return 0;
}

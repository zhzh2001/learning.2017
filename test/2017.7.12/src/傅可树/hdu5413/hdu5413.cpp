#include<cstdio>
#include<cstring>
#include<algorithm>
#include<bitset>
using namespace std;
const int maxm=1e5+5,maxn=2e4+5;;
struct pp{
	int link,next;
}e[maxm];
bitset<maxn> f[maxn];
int ans,T,n,m,tot,du[maxn],rk[maxn],q[maxn],head[maxn],qq[maxn];
inline void add(int u,int v){e[++tot]=(pp){v,head[u]}; head[u]=tot;}
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
inline void topsort()
{
	int h=0,t=0;
	for (int i=1;i<=n;i++) if (!du[i]) q[++t]=i;
	while (h<t){
		int now=q[++h]; rk[q[h]]=h;
		for (int i=head[now];i;i=e[i].next){
			int u=e[i].link; --du[u];
			if (!du[u]) q[++t]=u;
		}
	} 
}
inline bool cmp(int a,int b){return rk[a]<rk[b];}
int main()
{
	T=read();
	while (T--){
		n=read(); m=read(); memset(head,0,sizeof(head)); memset(rk,0,sizeof(rk)); tot=0;
		for (int i=1;i<=m;i++) {int u=read(),v=read(); add(u,v); du[v]++;}
		topsort(); ans=0;
		for (int i=1;i<=n;i++) f[i].reset();
		for (int i=n;i;i--){
			f[q[i]][q[i]]=1; int now=0;
			for (int j=head[i];j;j=e[j].next) qq[++now]=e[j].link;
			sort(qq+1,qq+1+now,cmp);
			for (int j=1;j<=now;j++){
				if (f[q[i]][qq[j]]) ans++;
				f[q[i]]|=f[qq[j]];
			}
		}
		writeln(ans);
	}
	return 0;
}

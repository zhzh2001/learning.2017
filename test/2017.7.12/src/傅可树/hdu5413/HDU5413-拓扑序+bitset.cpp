#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<bitset>
#include<iostream>
using namespace std;

const int maxn = 20009;
vector<int> e[maxn];
bitset<maxn> f[maxn];
int n, m, u, v, ans = 0;
int q[maxn], in[maxn];
int p[maxn], rk[maxn];

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

inline bool cmp(int a, int b){
	return rk[a] > rk[b];
}

void topo(){
	int l = 0, r = 0, u;
	for (int i=1; i<=n; i++) if (!in[i]) q[r++] = i;
	for (int i=1; i<=n; i++){
		p[i] = u = q[l]; rk[q[l++]] = i;
		for (int j=e[u].size()-1; j>=0; j--){
			in[e[u][j]]--;
			if (!in[e[u][j]]) q[r++] = e[u][j];
		}
	}
}

int main(){	
	int T; T = read();
	while (T--){
		n = read(); m = read();
		memset(in, 0, sizeof in);
		for (int i=1; i<=n; i++) e[i].clear();
		for (int i=1; i<=m; i++){
			u = read(); v = read(); in[v]++;
			e[u].push_back(v);
		}
		topo(); ans = 0;
		for (int i=1; i<=n; i++) sort(e[i].begin(), e[i].end(), cmp);
		for (int i=1; i<=n; i++) f[i].reset();
		for (int i=n; i>=1; i--){
			f[p[i]][p[i]] = 1; 
			for (int j=e[p[i]].size()-1; j>=0; j--){
				if (f[p[i]][e[p[i]][j]]) ans++;
				f[p[i]] |= f[e[p[i]][j]];
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}

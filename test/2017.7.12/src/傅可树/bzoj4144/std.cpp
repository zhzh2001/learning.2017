#include <bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=a;i<=b;i++)
#define per(i,a,b) for(int i=a;i>=b;i--)
#define For(i,a,b) for(int i=a;i< b;i++)
#define fore(i,u)  for(int i=head[u];i;i=nxt[i])
#define pii pair<int , int>
#define mp make_pair
#define pb push_back
#define key first
#define id second

inline int rd() {
    char c = getchar();
    while (!isdigit(c)) c = getchar() ; int x = c - '0';
    while (isdigit(c = getchar())) x = x * 10 + c - '0';
    return x;
}

const int maxn = 200005;
const int inf = 0x7fffffff;

typedef int arr[maxn];
typedef int adj[maxn << 1 | 1];

adj to , val , nxt;
arr head , dis , pre , fa , pa , len , rk , dep , vis;

int n , s , m , ett;

struct edge {
    int u , v , w;
    edge(int u = 0 , int v = 0 , int w = 0):u(u) , v(v) , w(w) { }
};

bool cmp_w(const edge a , const edge b) {
    return a.w < b.w;
}

priority_queue<pii , vector<pii> , greater<pii> > q;

vector<edge> E;

inline void ins(int w , int v , int u) {
    to[++ ett] = v , val[ett] = w , nxt[ett] = head[u] , head[u] = ett;
    to[++ ett] = u , val[ett] = w , nxt[ett] = head[v] , head[v] = ett;
}

void input() {
    n = rd() , s = rd() , m = rd();
    rep(i , 1 , n) dis[i] = inf;
    rep(i , 1 , s) {
        int u = rd();
        dis[u] = 0 , pre[u] = u;
        q.push(mp(0 , u));
    }
    rep(i , 1 , m) ins(rd() , rd() , rd());
}

void dij() {
	int tot=0;
    while (!q.empty()) {
        pii x = q.top() ; q.pop();
        int u = x.id;
        if (vis[u]) continue;
        vis[u] = 1;
        fore(i , u) {
            int v = to[i] , w = val[i];
            if (!pre[v] || dis[v] > dis[u] + w) {
                dis[v] = dis[u] + w;
                pre[v] = pre[u];
                q.push(mp(dis[v] , v));
            } else if (pre[v] != pre[u])
                E.pb(edge(pre[v] , pre[u] , dis[v] + dis[u] + w)),printf("%d %d\n",v,u);
        }
    }
    printf("%d\n",tot);
}

int find(int u) { return u == pa[u] ? u : pa[u] = find(pa[u]) ; }

void mst() {
    sort(E.begin() , E.end() , cmp_w);
    rep(i , 1 , n) pa[i] = i , rk[i] = 1;
    int tot=0; 
    For(i , 0 , E.size()) {
        int u = E[i].u , v = E[i].v , w = E[i].w; ++tot;
        u = find(u) , v = find(v);
        if (u == v) continue;
        if (rk[v] > rk[u]) swap(u , v);
        if (rk[u] == rk[v]) rk[u] ++;
        pa[v] = u , fa[v] = u , len[v] = w;
    }
    printf("%d\n",tot);
}

void find_dep(int u) {
    if (!fa[u])
        { dep[u] = 1 ; return ; }
    find_dep(fa[u]);
    dep[u] = dep[fa[u]] + 1;
}

bool query(int u , int v , int d) {
    if (find(u) != find(v)) return 0;
    if (dep[u] < dep[v]) swap(u , v);
    while (dep[u] > dep[v]) {
        if (len[u] > d) return 0;
        u = fa[u];
    }
    if (u == v) return 1;
    while (u != v) {
        if (len[u] > d) return 0;
        if (len[v] > d) return 0;
        u = fa[u] , v = fa[v];
    }
    return 1;
}

void solve() {
    dij();
    mst();
    rep(i , 1 , n) if (!dep[i]) find_dep(i);
    per(q , rd() , 1) {
        int u = rd() , v = rd() , d = rd();
        bool ans = query(u , v , d);
        puts(ans ? "TAK" : "NIE");
    }
}

int main() {
    input();
    solve();
    return 0;
}

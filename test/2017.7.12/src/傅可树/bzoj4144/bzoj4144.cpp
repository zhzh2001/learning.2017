#include<bits/stdc++.h>
using namespace std;
const int maxn=200005,maxm=200005;
struct pp{
	int link,next,val;
}e[maxm];
struct pp1{
	int u,v,w;
}ee[maxm];
bool vis[maxn];
int dep[maxn],len[maxn],pa[maxn],n,m,s,c[maxn],tot,head[maxn],pre[maxn],dis[maxn],rank[maxn],fa[maxn];
priority_queue<pair<int,int>,vector<pair<int,int> > , greater <pair<int,int> > > heap;
inline void add(int u,int v,int w){e[++tot]=(pp){v,head[u],w}; head[u]=tot;}
inline void insert(int u,int v,int w){add(u,v,w); add(v,u,w);}
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void dij()
{
	while (!heap.empty()){
		int now=heap.top().second,diss=heap.top().first; heap.pop(); 
		if (vis[now]) continue;
		vis[now]=1;
		for (int i=head[now];i;i=e[i].next){
			int u=e[i].link;
			if (!pre[u]||dis[u]>dis[now]+e[i].val){
				dis[u]=dis[now]+e[i].val; pre[u]=pre[now];
				heap.push(make_pair(dis[u],u));
			}else if (pre[now]!=pre[u]) ee[++tot]=(pp1){pre[u],pre[now],dis[u]+dis[now]+e[i].val},printf("%d %d\n",now,u);
		}
	}
}
inline bool cmp(pp1 a,pp1 b){return a.w<b.w;}
int find(int x){return (pa[x]==x)?x:pa[x]=find(pa[x]);}
inline void mst()
{
	sort(ee+1,ee+1+tot,cmp);
	for (int i=1;i<=n;i++) pa[i]=i,rank[i]=1;
	for (int i=1;i<=tot;i++){
		int p=find(ee[i].u),q=find(ee[i].v);
		if (p!=q){
			if (rank[p]<rank[q]) swap(p,q);
			if (rank[p]==rank[q]) rank[p]++;
			fa[q]=p; pa[q]=p; len[q]=ee[i].w; 
		} 
	}
}
inline void dfs(int x)
{
	if (!fa[x]) {dep[x]=1; return;}
	dfs(fa[x]); dep[x]=dep[fa[x]]+1;
}
inline bool query(int x,int y,int z)
{
	if (find(x)!=find(y)) return 0;
	if (dep[x]<dep[y]) swap(x,y);
	while (dep[x]>dep[y]){
		if (len[x]>z) return 0;
		x=fa[x];
	}
	if (x==y) return 1;
	while (x!=y){
		if (len[x]>z) return 0;
		if (len[y]>z) return 0;
		x=fa[x]; y=fa[y];
	}
	return 1;
}
int main()
{
	n=read(); s=read(); m=read(); memset(dis,127/3,sizeof(dis));
	for (int i=1;i<=s;i++) c[i]=read(),pre[c[i]]=c[i],dis[c[i]]=0,heap.push(make_pair(0,c[i]));
	for (int i=1;i<=m;i++) insert(read(),read(),read());
	tot=0; dij(); mst();
	for (int i=1;i<=n;i++) if (!dep[i]) dfs(i); 
	int T=read();
	for (int i=1;i<=T;i++) 
		if (query(read(),read(),read())) puts("TAK"); else puts("NIE"); 
	return 0;
}

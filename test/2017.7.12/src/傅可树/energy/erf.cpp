#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1e5+5;
int n,a[maxn],mx;
inline bool judge(int x)
{
	for (int i=1;i<=n;x--,i++) if (x<a[i]) return 0;
	return 1;
}
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
int main()
{
	freopen("energy.in", "r", stdin); freopen("energy.out", "w", stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read(),mx=max(mx,a[i]);
	int l=0,r=mx+2*n;
	while (l<r){
		int mid=(l+r)>>1;
		if (judge(mid)) r=mid; else l=mid+1;
	}
	printf("%d\n",l);
}

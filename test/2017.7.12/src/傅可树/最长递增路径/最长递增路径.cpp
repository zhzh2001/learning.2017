#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=50005;
struct pp{
	int u,v,w;
}e[maxn];
int n,m,ans,last,f[maxn],v[maxn];
inline bool cmp(pp a,pp b){return a.w<b.w;}
int main()
{
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&e[i].u,&e[i].v,&e[i].w);
		e[i].v++; e[i].u++;
	}
	sort(e+1,e+1+m,cmp); last=0;
	for (int i=1;i<=m;i++)
		if (i==m||e[i].w<e[i+1].w){
			for (int j=last+1;j<=i;j++) v[e[j].u]=f[e[j].u],v[e[j].v]=f[e[j].v]; 
			for (int j=last+1;j<=i;j++) f[e[j].u]=max(f[e[j].u],v[e[j].v]+1),f[e[j].v]=max(f[e[j].v],v[e[j].u]+1);
			last=i;
		}
	for (int i=1;i<=n;i++) ans=max(ans,f[i]);
	printf("%d\n",ans); return 0;
} 

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int ans=0;
int n;
int a[100100];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		a[i]=read();
		ans=max(ans,a[i]+i-1);
	}
	cout<<ans<<endl;
	return 0;
}


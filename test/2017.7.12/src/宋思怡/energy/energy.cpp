#include <algorithm>
#include <iostream>
#include <cstdio>
#define MAX 100010
using namespace std;
long long int n, a[MAX];
char ch;

long long read()
{
	long long int x=0;
	int f=1;
	char c=getchar();
	while (!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n = read();
	for (int i = 1;i<=n;i++)
		a[i] = read()+i;
	sort(a+1,a+1+n);
	cout << a[n]-1;
	fclose(stdin),fclose(stdout);
	return 0;
}

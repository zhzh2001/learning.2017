#include <iostream>
#include <cstdio>
#define MAX 50
using namespace std;
bool k;
char ch;
int n, n_2, T, a[MAX];

long long read()
{
	long long int x=0;
	int f=1;
	char c=getchar();
	while (!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

bool check1(int x)
{
	for (int i = 1;i<x;i++)
		if (a[i]!=a[x-1+i]) return false;
	int xx = x+x-1, yy = x+n_2;
	for (int i = 0;i<=n_2-x;i++)
		if (a[i+xx]!=a[yy+i]) return false;
	return true;
}


int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	T = read();
	while(T--)
	{
		n = read();
		for (int i = 1;i<=n;i++)
			a[i] = read();
		n_2 = n>>1;
		k = true;
		for (int i = 1;i<=n_2;i++)
			if (check1(i)){
				printf("Frederica Bernkastel\n");
				k = false;
				break;
			}
		if (k) printf("Furude Rika\n");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

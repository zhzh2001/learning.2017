#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n;
int a[100005];

int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	int ans,i;
	n=read();
	for(i=1;i<=n;i++)	
		a[i]=read();
	ans=a[n];
	for(i=n-1;i>=1;i--)
	{
		ans++;
		if(ans<a[i])
			ans=a[i];
	}
	printf("%d\n",ans);
	return 0;
}


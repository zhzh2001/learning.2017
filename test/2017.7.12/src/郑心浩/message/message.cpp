#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n,mod;
int k;
int c[35][155];
int c1[35][155];
int li[35],l[35];

int main()
{
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	int i,xi,j,o,p;
	char ci,si[155];
	n=read(),mod=read();
	k=read();
	for(i=1;i<=k;i++)
	{
		scanf("%s",&si);
		ci=si[0];
		xi=ci-'A'+1;
		li[i]=strlen(si);
		for(j=3;j<li[i];j++)
			c[xi][j-2]=si[j]-48;
		li[i]-=3;
	}
	for(i=1;i<=li[k];i++)
		c1[k][i]=c[k][i];
	l[k]=li[k];
	for(i=k-1;i>=1;i--)
	{
		l[i]=0;
		for(j=1;j<=li[i];j++)
		{
			if(c[i][j]>9)
			{
				p=c[i][j]+48-'A'+1;
				for(o=1;o<=l[p];o++)
					c1[i][++l[i]]=c1[p][o];
			}
			else
				c1[i][++l[i]]=c[i][j];
		}
	}
	/*for(i=1;i<=l[1];i++)
		printf("%d",c1[1][i]);
	printf("\n");*/
	int ans=0,q;
	for(i=1;i<=l[1];i++)
		if(c1[1][i]==0)
			ans=(ans+1)%mod;
	for(i=1;i<=l[1];i++)
		if(c1[1][i]!=0)
			for(j=i;j<=l[1];j++)
			{
				q=0;
				for(o=i;o<=j;o++)
					q=(q*10+c1[1][o])%n;
				if(q%n==0)
					ans=(ans+1)%mod;
			}
	printf("%d\n",ans%mod);
	return 0;
}


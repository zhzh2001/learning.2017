#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n;
int a[45];
int ci[45];
int to[45];
int b[45];
int c[45];
int vis[45];
int on1,ai;

void dfs(int x,int cou)
{
	int i;
	ai++;
	if(ai>14000000)
	{
		on1=2;
		return ;
	}
	if(cou==n/2)
	{
		int u=0;
		for(i=1;i<=n;i++)
			if(vis[i]==0)
				c[++u]=a[i];
		for(i=1;i<=cou;i++)
			if(c[i]!=b[i])
				return ;
		on1=1;
		return ;
	}
	for(i=x+1;i<=n;i++)
		if(on1==0&&ci[a[i]]>to[a[i]])
		{
			vis[i]=1;
			b[cou+1]=a[i];
			ci[a[i]]--;
			dfs(i,cou+1);
			vis[i]=0;
			ci[a[i]]++;
		}
}

int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);	
	int ti,i;
	ti=read();
	while(ti--)
	{
		n=read();
		for(i=1;i<=n;i++)
			ci[i]=0,c[i]=0,vis[i]=0;
		for(i=1;i<=n;i++)
		{
			a[i]=read();
			ci[a[i]]++;
		}
		on1=0;
		for(i=1;i<=40;i++)	
			if(ci[i]&1)
			{
				printf("Furude Rika\n");
				on1=1;
				break;
			}
		if(on1==1)
			continue;
		for(i=1;i<=40;i++)
			to[i]=ci[i]/2;
		on1=0,ai=0;
		dfs(0,0);
		if(on1==1)
			printf("Frederica Bernkastel\n");
		else
			printf("Furude Rika\n");
	}
	return 0;
}


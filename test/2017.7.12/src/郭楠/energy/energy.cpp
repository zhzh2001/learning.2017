#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
int n,m,a[N],ans;
int read()
{int t=0;char c;
c=getchar();
while(!(c>='0' && c<='9')) c=getchar();
while(c>='0' && c<='9')
 {
 	t=t*10+c-48;c=getchar();
 }
 return t;	
}
int main()
{int i,j,k;
    freopen("energy.in","r",stdin);
    freopen("energy.out","w",stdout);
    n=read();
    for(i=1;i<=n;i++) a[i]=read();
    ans=0;
    for(i=1;i<=n;i++) ans=max(ans,a[i]+i-1);
    printf("%d",ans);
}

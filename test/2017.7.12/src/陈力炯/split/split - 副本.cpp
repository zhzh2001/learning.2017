#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
int n,A[45],top1,top2,now,s1[205],s2[205],num1[45],num2[45],num[45];
bool F;
void dfs(int x){
	if (x==n+1){
		F=true;
		return;
	}
	int Top1=top1,Top2=top2,Now=now;
	bool flag=true;
	for (int i=x,t;i<=n;i++){
		t=A[i];
		if (num2[t]<num[t])
			if (now>top1){
				num2[t]++; s2[++top2]=t;
				dfs(i+1);
				if (F) return;
				num2[t]--; top2--;
			}
			else
			if (s1[now]==t){
				num2[t]++; s2[++top2]=t; now++;
				dfs(i+1);
				if (F) return;
				num1[t]--; top2--; now--;
			}
		if (num1[t]<num[t])
			if (now>top2) s1[++top1]=A[i],num1[t]++;
			else
			if (s2[now]==t) s1[++top1]=A[i],num1[t]++,now++;
			else{
				for (int j=x;j<i;j++) num1[A[j]]--;
				flag=false;
				break;
			}
		else{
			for (int j=x;j<i;j++) num1[A[j]]--;
			flag=false;
			break;
		}
	}
	if (flag){
		F=true;
		return;
	}
	else top1=Top1,top2=Top2,now=Now;
}
int main(){
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(num,0,sizeof(num));
		for (int i=1;i<=n;i++){
			scanf("%d",&A[i]);
			num[A[i]]++;
		}
		F=true;
		for (int i=1;i<=40;i++)
		if (num[i]&1){
			F=false;
			break;
		}
		else num[i]>>=1;
		if (!F){
			puts("Furude Rika");
			continue;
		}
		F=false;
		now=1; top1=top2=0;
		memset(num1,0,sizeof(num1));
		memset(num2,0,sizeof(num2));
		dfs(1);
		if (F) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

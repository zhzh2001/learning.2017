#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;

int n;
int ans=-1;

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();
	for(int i=1,x; i<=n; i++)
	{
		x=read();
		ans=max(ans,x+i-1);
	}
	cout << ans << endl;
	return 0;
}


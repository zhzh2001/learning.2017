#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;

int T,n;
int a[45];
int b[45]={0};
int pos[45][45]={0};
int used[45]={0};
int za[45]={0},zb[45]={0};

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

bool check1()
{
	if(n%2==1)
		return 0;
	for(int i=1; i<=41; i++)
		if(b[i]%2==1)
			return 0;
	return 1;
}

void check()
{
	for(int i=1; i<=n/2; i++)
		if(za[i]!=zb[i])
		{
			puts("Furude Rika");
			return;
		}
	puts("Frederica Bernkastel");
	return;
}

int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	T=read();
	while(T--)
	{
		memset(b,0,sizeof(b));
		memset(za,0,sizeof(za));
		memset(zb,0,sizeof(zb));
		memset(used,0,sizeof(used));
		n=read();
		for(int i=1; i<=n; i++)
		{
			a[i]=read();
			b[a[i]]++;
		}
		if(!check1())
		{
			cout << "Furude Rika" << endl;
			continue;
		}
		for(int i=1; i<=n; i++)
		{
			if(used[a[i]]<b[a[i]]/2)
				za[++za[0]]=a[i];
			else zb[++zb[0]]=a[i];
			used[a[i]]++;
		}
		check();
//		for(int i=1; i<=za[0]; i++)
//			cout << za[i] << " ";
//		cout << endl;
//		for(int i=1; i<=zb[0]; i++)
//			cout << zb[i] << " ";
//		cout << endl;
	}
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<string>
#include<vector> 
#include<queue>
#include<cmath>
using namespace std;
#define isch(ch) ('A'<=ch && ch<='Z')
typedef long long LL;

vector<int> g[28];
int ind[28]; bool vis[28];
queue<int> q;
int n,r,k,cnt; 
string str[28]; 
LL pow_mod[100005];
LL subfix[100005];

int getmod(int i, int j){
	LL re = subfix[j] - subfix[i-1] * pow_mod[j-i+1];
	while(re < 0) re += n; return re;
}
int main(){
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	ios::sync_with_stdio(false);
	cin >> n >> r >> k;
	for(int i=1; i<=k; i++){
		cin >> str[i]; str[i] = str[i].substr(3);
		for(int j=0; j<str[i].length(); j++){
			char k = str[i][j];
			if(isch(k))
				g[k-'A'+1].push_back(i), ind[i]++;
		}
	}
	for(int i=1; i<=k; i++) if(!ind[i]) q.push(i);
	while(!q.empty()){
		int u = q.front(); q.pop();
		for(int i=0; i<g[u].size(); i++){
			int v = g[u][i]; if(vis[v]) continue;
			for(int p=str[v].find(u+'A'-1); p<str[v].length(); p=str[v].find(u+'A'-1,p+1))
				str[v].replace(p,1,str[u]);
			ind[v]--; if(!ind[v]) q.push(v);
		} vis[u] = true;
	}
	//cout << str[1] << endl;
	pow_mod[0] = 1;
	for(int i=1; i<=str[1].length(); i++)
		pow_mod[i] = pow_mod[i-1] * 10 % n;
	subfix[0] = str[1][0] - '0';
	for(int i=1; i<str[1].length(); i++)
		subfix[i] = (subfix[i-1] * 10 + str[1][i] - '0') % n;
	for(int i=0; i<str[1].length(); i++)
		for(int j=i; j<str[1].length(); j++){
			if((i==j && str[1][i]=='0') || str[1][i]!='0'){
				if(getmod(i,j) == 0){
					cnt++; cnt %= r;
				}
			}
		}
	cout << cnt;
	return 0;
}
			


#include<cstdio>
#include<iostream>
#include<cstring>
#include<cctype>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long LL;
const int maxn = 45;
void IN(int &x){
	x =0; char ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0', ch=getchar();
}

int T, n, A[maxn], num[maxn], mx;
bool all_eq, flag;

int cnt1[maxn],cnt2[maxn],num1,num2;
int set1[maxn], set2[maxn], pt1, pt2;
bool dfs(int p){
	if(p > n) return true;
	
	if(num1 + n-p+1 < (n>>1) || num2 + n-p+1 < (n>>1)) return false;
	if(cnt1[A[p]] == num[A[p]]){
		set2[++pt2] = A[p]; num2++; cnt2[A[p]]++;
		if(pt2 > pt1 || (pt2<=pt1 && set1[pt2]==set2[pt2])){
			if(dfs(p+1)) return true;
		}
		pt2--; num2--; cnt2[A[p]]--;
		return false;
	}
	if(cnt2[A[p]] == num[A[p]]){
		set1[++pt1] = A[p]; num1++; cnt1[A[p]]++;
		if(pt1 > pt2 || (pt1<=pt2 && set1[pt1]==set2[pt1])){
			if(dfs(p+1)) return true;
		}
		pt1--; num1--; cnt1[A[p]]--;
		return false;
	}
	
	set1[++pt1] = A[p]; num1++; cnt1[A[p]]++;
	if(dfs(p+1)) return true;
	pt1--; num1--; cnt1[A[p]]--;
	set2[++pt2] = A[p]; num2++; cnt2[A[p]]++;
	if(dfs(p+1)) return true;
	pt2--; num2--; cnt2[A[p]]--;
	
	return false;
}

int main(){
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	IN(T); while(T--){
		IN(n);
		memset(num,0,sizeof(num));
		mx = -1; all_eq = flag = true;
		
		for(int i=1; i<=n; i++){
			IN(A[i]); num[A[i]]++;
			mx = max(mx, A[i]);
		}
		
		for(int i=2; i<=n; i++)
			if(A[i] != A[1]) {all_eq = false; break;}
		if(all_eq) { puts("Frederica Bernkastel"); continue;}
		
		for(int i=1; i<=mx; i++){
			if(num[i]&1) { flag = false; break;}
			num[i]>>=1;
		} if(!flag) {puts("Furude Rika"); continue;}
		
		pt1 = pt2 = num1 = num2 = 0;
		memset(cnt1,0,sizeof(cnt1));
		memset(cnt2,0,sizeof(cnt2));
		if(dfs(1)) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	return 0;
}

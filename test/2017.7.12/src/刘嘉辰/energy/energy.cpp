#include<cstdio>
#include<iostream>
#include<cstring>
#include<cctype>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long LL;

int n, re = -1; 
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d",&n);
	for(int i=1; i<=n; i++){
		int tmp; scanf("%d",&tmp);
		re = max(re, tmp+i-1);
	}
	printf("%d",re);
	return 0;
}

#include<cstdio>
#include<string>
using namespace std;
const int maxn=200005,maxm=30;
int n,k,r,lens,len[maxm],len2[maxm];
char s1[maxm][maxn],s2[maxm][maxn],s[maxn];
long long ans;
void dfs(int x){
	for (int i=1;i<=len[x];i++)
	if (s1[x][i]>='0'&&s1[x][i]<='9') {len2[x]++; s2[x][len2[x]]=s1[x][i];}
	else {
		int y=s1[x][i]-'A'+1;
		if (len2[y]==0) dfs(s1[x][i]-'A'+1);
		for (int j=1;j<=len2[y];j++) {len2[x]++; s2[x][len2[x]]=s2[y][j];}
	}
}
int main(){
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%d %d %d",&n,&r,&k);
	for (int i=1;i<=k;i++) {
		char ch=getchar(); while (ch!='>') ch=getchar();
		len[i]=0; ch=getchar();
		while ((ch>='0'&&ch<='9')||(ch>='A'&&ch<='Z')) 
		{len[i]++; s1[i][len[i]]=ch; ch=getchar();}
	}
	dfs(1);
	lens=len2[1];
	for (int i=1;i<=lens;i++) s[i]=s2[1][i];
	for (int i=1;i<=lens;i++){
		if (s[i]=='0') {ans=1ll*(ans+1)%r; continue;}
		int sum=0;
		for (int j=i;j<=lens;j++) {sum=(sum*10+s[j]-48)%n; if (sum==0) ans=1ll*(ans+1)%r;}
	}
	printf("%lld\n",ans);
	return 0;
}

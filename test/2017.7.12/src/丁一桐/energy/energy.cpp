#include<cstdio>
#include<string>
using namespace std;
int n,ans;
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) ans=max(ans,read()+i-1);
	printf("%d\n",ans);
	return 0;
}

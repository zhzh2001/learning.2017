#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=45;
int T,n,a[maxn],hsh[maxn],b[maxn],c[maxn],nxt[maxn];
bool pd;
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
bool check1(){
	memset(hsh,0,sizeof(hsh));
	memset(nxt,0,sizeof(nxt));
	for (int i=1;i<=n;i++)
     hsh[a[i]]++;
    for (int i=1;i<=40;i++) if (hsh[i]%2!=0) return false;
    for (int i=1;i<n;i++)
    for (int j=i+1;j<=n;j++)
    if (a[j]==a[i]) {nxt[i]=j; break;}
    return true;
}
bool check(){
	b[0]=0; c[0]=0;
	for (int i=1;i<=n;i++) if (hsh[i]==1) {b[0]++; b[b[0]]=a[i];} else {c[0]++; c[c[0]]=a[i];}
	bool check=false;
	for (int i=1;i<=n/2;i++) if (b[i]!=c[i]) check=true;
	if (check==false) {printf("Frederica Bernkastel\n"); pd=true;}
}
void dfs(int x,int sum){
	if (pd==true) return;
	if (sum==n) {check(); return;}
	if (hsh[x]!=0) {dfs(x+1,sum); return;} 
	int y=x; hsh[x]=1;
	while (nxt[y]!=0) {
		y=nxt[y];
		if (hsh[y]!=0) continue;
		hsh[y]=2; dfs(x+1,sum+2); hsh[y]=0;
	}
	hsh[x]=0;
}
int main(){
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	T=read();
	for (int k=1;k<=T;k++) {
		n=read();
		for (int i=1;i<=n;i++) a[i]=read();
		if (check1()==false) {printf("Furude Rika\n"); continue;}
		pd=false; memset(hsh,0,sizeof(hsh));
		dfs(1,0);
		if (pd==false) printf("Furude Rika\n");
	}
	return 0;
}


#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxm 500010
#define maxn 1001
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int n,mo,k,L,cnt[201],sum,a[200001];
struct node{char s[101];} to[201];
char ch,s[101],ans[200001];
inline void dfs(char c)
{
	int len=strlen(to[c].s+1);
	For(i,1,len)
	{
		if(to[c].s[i]>='0'&&to[c].s[i]<='9')	ans[++L]=to[c].s[i];
		else	dfs(to[c].s[i]);
	}
}
inline void dp()
{
	For(i,1,L)	a[i]=ans[i]-'0';
	For(i,1,L)
	{
		memset(cnt,0,sizeof cnt);
		int now=a[i]%n;	cnt[now]++;
		if(a[i]!=0)
			For(j,i+1,L)	now=(now*10+a[j])%n,cnt[now]++;
		sum+=cnt[0];sum%=mo;
	}
	writeln(sum);
}
int main()
{
	read(n);read(mo);
	read(k);
	For(i,1,k)
	{
		scanf("\n%c->%s",&ch,s+1);
		For(j,1,strlen(s+1))	to[ch].s[j]=s[j];
	}	
	dfs('A');
	dp();
}
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<cctype>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}
const int maxn=33;
const int maxk=33;
const int maxr=int(1e9+7);
const int maxl=103;
const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;

int n,r,k;
char msg[maxk][maxl];
int lenm[maxk];
char key[maxk][maxl];
int lenk[maxk];
void input()
{
	read(n);read(r);read(k);
	for(int i=1;i<=k;++i)
	{
		char r;
		do
		{
			r=getchar();
		}while(r!='-');
		r=getchar();
		scanf("%s\n",key[i]);
		lenk[i]=strlen(key[i]);
	}
}
inline int asknum(char r)
{
	return r-'A'+1;
}
inline char rechar(int l)
{
	return char(l+'A'-1);
}
char A[1500];
char tmp[1500];
int len;
int lent;
void replace(int l)
{
	char c=rechar(l);
	lent=0;
	for(int i=0;i<=len-1;++i)
	{
		if(A[i]==c)	
		{
			memcpy(tmp+lent,key[l],lenk[l]*(sizeof(char)));
			lent+=lenk[l];
		}
		else tmp[lent++]=A[i];
	}
	memcpy(A,tmp,lent*(sizeof(char)));
	len=lent;
}
void doreplace()
{
	memcpy(A,key[1],lenk[1]*(sizeof(char)));
	len=lenk[1];
	for(int i=2;i<=k;++i)
	{
		replace(i);
	}
}
int pow10[155];
void initpow10()
{
	pow10[0]=1%n;
	for(int i=1;i<=150;++i)
	{
		pow10[i]=1ll*pow10[i-1]*10%n;
	}
}
int fastmod(int a,int p)
{
	if(a==10)
	return pow10[p];
}
int dp[maxl][maxn][155];
int dp0[maxl][maxn][155];
void solve()
{
	dp[len-1][int(A[len-1])%n][1]=1;
	for(int x=0;x<=n-1;++x)
	{
		dp[len-1][x][0]=1;
	}
	for(int i=len-2;i>=0;--i)
	{
		for(int j=1;j<=(len-1)-i+1;++j)
		{
			for(int x=0;x<=n-1;++x)
			{
				if(A[i]==0)
				{
					dp[i][x][j]=dp[i+1][x][j];
					if(j==1&&x==0)++dp[i][x][j];
					dp0[i][x][j]=dp0[i+1][x][j]+dp0[i+1][x][j-1];
					dp0[i][x][j]%=r;
				}
				else
				{
					dp[i][x][j]
					=dp[i+1][x][j]
					+dp0[i+1][n-1ll*fastmod(10,len-1)*(A[i]-48)%n][j-1];
					dp0[i][x][j]
					=dp[i][x][j]-dp[i+1][x][j]+dp0[i+1][x][j];
					dp[i][x][j]%=r;
					dp0[i][x][j]%=r;
				}
			}
		}
	}
}
int main()
{
	
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	input();
	initpow10();
	doreplace();
	solve();
	int ans=0;
	for(int i=1;i<=150;++i)
	{
		ans+=dp[0][0][i];
		ans%=r;
	}
	printf("%d\n",ans);
	return 0;
}

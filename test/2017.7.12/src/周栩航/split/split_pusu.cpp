#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxm 500010
#define maxn 1001
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int q1[201],q2[201],top1,top2,n,a[201],flag,T,tot1[201],tot2[201],cnt[201];
inline bool check()
{
	if(top1!=top2)	return 0;
	For(i,1,top1)	if(q1[i]!=q2[i])	return 0;
	return 1;
}
inline void dfs(int x)
{
	if(flag)	return;
	if(x==n+1)	{if(check())	flag=1;return;}
	For(i,1,min(top1,top2))	if(q1[i]!=q2[i])	return;
	if(top1>n/2)	return;
	if(top2>n/2)	return;
	if(tot2[a[x]]!=cnt[a[x]]/2)	
			q2[++top2]=a[x],tot2[a[x]]++,dfs(x+1),top2--,tot2[a[x]]--;
	if(tot1[a[x]]!=cnt[a[x]]/2)
			q1[++top1]=a[x],tot1[a[x]]++,dfs(x+1),top1--,tot1[a[x]]--;
}
int main()
{
	freopen("split.in","r",stdin);freopen("split_pusu.out","w",stdout);
	read(T);
	while(T--)
	{
		memset(cnt,0,sizeof cnt);
		read(n);
		For(i,1,n)	read(a[i]),cnt[a[i]]++;
		flag=0;
		For(i,1,40)	if(cnt[a[i]]&1)	{puts("Furude Rika");flag=1;break;}
		if(flag)	continue;
		flag=0;top1=top2=0;
		dfs(1);
		if(flag)	puts("Frederica Bernkastel");else puts("Furude Rika");
	}
}	
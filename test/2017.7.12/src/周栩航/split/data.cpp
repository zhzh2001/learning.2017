#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxm 500010
#define maxn 1001
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int a[2001],vis[2001],T;
int main()
{
	freopen("split.in","w",stdout);
	srand(time(0));
	T=5;
	writeln(T);
	while(T--)
	{
		int n=20;
		n*=2;
		memset(vis,0,sizeof vis);
		writeln(n);
		For(i,1,n/2)	a[i]=rand()%40+1;
		For(i,1,n)
		{
			int t=rand()%n;
			while(vis[t])	t=t+1,t%=n;
			vis[t]=1;
			write(a[t%(n/2)+1]);putchar(' ');
		}
		puts("");
	}	
}

#include <iostream>
#include <algorithm>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

const int MAXN=40+10;

int a[MAXN];
int n,mx;
int pos[MAXN][MAXN];
int t1[MAXN],t2[MAXN];
int p1[MAXN],p2[MAXN];
int cnt1,cnt2;
int s[MAXN][MAXN];
#ifdef BAOLI
bool solve()
{
	memset(pos,0,sizeof(a));
	for(int i=1;i<=n;i++) 
		pos[a[i]][++pos[a[i]][0]]=i;
	for(int i=1;i<=n;i++) if(pos[a[i]][0]&1) return false;	
}
#else
bool solve()
{
	memset(pos,0,sizeof(a));
	for(int i=1;i<=n;i++) 
		pos[a[i]][++pos[a[i]][0]]=i;
	for(int i=1;i<=n;i++) if(pos[a[i]][0]&1) return false;
	for(int i=1;i<=n;i++)
	{
		if(i==pos[a[i]][1]) t1[cnt1]=a[i],p1[cnt1++]=i;
		if(i==pos[a[i]][2]) t2[cnt2]=a[i],p2[cnt2++]=i;
	}
	for(int i=0;i<max(cnt1,cnt2);i++) if(t1[i]!=t2[i]) return false;
	for(int i=1;i<=n;i++)
	{
		for(int j=0;j<=mx;j++)
			s[i][j]=s[i-1][j];
		s[i][a[i]]++;
	}
	for(int i=0;i<cnt1;i++)
	{
		if(p1[i]<p2[i]) 
			for(int j=0;j<=mx;j++)
				if(s[p2[i]][j]-s[p1[i]][j]>s[p1[i]][j]) return false;
		if(p1[i]>=p2[i])
			for(int j=0;j<=mx;j++)
				if(s[p1[i]][j]-s[p2[i]][j]>s[p1[i]][j]) return false;			
	}
	return true;
}
#endif
int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		for(int i=1;i<=n;i++) 
		{
			scanf("%d",a+i);
			mx=max(mx,a[i]);
		}
		if(solve())
		{
			printf("Frederica Bernkastel\n");
		}
		else
		{
			printf("Furude Rika\n");
		}
	}
}

#include <iostream>
#include <algorithm>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

const int MAXN=100000+10;
int n;
long long a[MAXN];
int main()
{
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	scanf("%d",&n);
	for(int i=0;i<n;i++) scanf("%lld",a+i);
	long long ans=0;
	for(int i=0;i<n;i++)
	{
		a[i]+=i;
		ans=max(a[i],ans);
	}
	printf("%lld",ans);
}

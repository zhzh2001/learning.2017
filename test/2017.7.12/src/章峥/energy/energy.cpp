#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("energy.in");
ofstream fout("energy.out");
const int N = 100005;
int a[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	int ans = 0;
	for (int i = 1; i <= n; i++)
		ans = max(ans, a[i] + i - 1);
	fout << ans << endl;
	return 0;
}
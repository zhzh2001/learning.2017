#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("split.in");
ofstream fout("split.out");
const int N = 50;
int n, a[N];
bool ans, vis[N];
vector<int> p[N];
void dfs(int k, int cnt, int pred)
{
	if (k == n + 1)
	{
		ans = true;
		return;
	}
	if (vis[k])
		dfs(k + 1, cnt, pred);
	else
		for (vector<int>::iterator i = upper_bound(p[a[k]].begin(), p[a[k]].end(), max(k, pred)); i != p[a[k]].end(); ++i)
		{
			vis[*i] = true;
			dfs(k + 1, cnt + 1, *i);
			vis[*i] = false;
			if (ans)
				return;
		}
}
bool check()
{
	for (int i = 1; i <= 40; i++)
		if (p[i].size() & 1)
			return false;
	return true;
}
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		fin >> n;
		for (int i = 1; i <= 40; i++)
			p[i].clear();
		for (int i = 1; i <= n; i++)
		{
			fin >> a[i];
			p[a[i]].push_back(i);
		}
		ans = false;
		if (check())
			dfs(1, 0, 0);
		if (ans)
			fout << "Frederica Bernkastel\n";
		else
			fout << "Furude Rika\n";
	}
	return 0;
}
#include <fstream>
#include <string>
#include <cctype>
using namespace std;
ifstream fin("message.in");
ofstream fout("message.out");
const int N = 30;
int n, mod, k;
string s[N];
struct node
{
	bool empty;
	int len, ans, val, l[N][N], r[N];
	node operator+(const node &rhs) const
	{
		if (empty)
			return rhs;
		node ret;
		ret.empty = false;
		ret.len = len * rhs.len % n;
		ret.val = (val * rhs.len + rhs.val) % n;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				ret.l[i][j] = l[i][j];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				(ret.l[i * len % n][(i * val + j) % n] += rhs.l[i][j]) %= mod;
		for (int i = 0; i < n; i++)
			ret.r[i] = rhs.r[i];
		for (int i = 0; i < n; i++)
			(ret.r[(i * rhs.len + rhs.val) % n] += r[i]) %= mod;
		ret.ans = (ans + rhs.ans) % mod;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
			{
				int k = (n - i * j % n) % n;
				ret.ans = (ret.ans + 1ll * r[i] * rhs.l[j][k]) % mod;
			}
		return ret;
	}
	node &operator+=(const node &rhs)
	{
		return *this = *this + rhs;
	}
} num[10], f[N];
int main()
{
	fin >> n >> mod >> k;
	for (int i = 1; i <= k; i++)
	{
		fin >> s[i];
		s[i] = s[i].substr(3);
	}
	for (int i = 0; i < 10; i++)
	{
		num[i].len = 10 % n;
		num[i].val = i % n;
		num[i].ans = i % n == 0;
		num[i].l[num[i].len][num[i].val] = 1;
		num[i].r[num[i].val] = (bool)i;
	}
	for (int i = k; i; i--)
	{
		f[i].empty = true;
		for (int j = 0; j < (int)s[i].length(); j++)
			if (isupper(s[i][j]))
				f[i] += f[s[i][j] - 'A' + 1];
			else
				f[i] += num[s[i][j] - '0'];
	}
	fout << f[1].ans << endl;
	return 0;
}
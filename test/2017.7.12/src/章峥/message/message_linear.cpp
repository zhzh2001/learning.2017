#include <fstream>
#include <string>
#include <cctype>
#include <algorithm>
using namespace std;
ifstream fin("message.in");
ofstream fout("message.out");
const int K = 30, N = 35, L = 100005;
string s[K], t[K];
int pow10[N], bucket[N][N], sum[L];
bool vis[N];
int main()
{
	int n, r, k;
	fin >> n >> r >> k;
	for (int i = 1; i <= k; i++)
	{
		fin >> s[i];
		s[i] = s[i].substr(3);
	}
	t[k] = s[k];
	for (int i = k - 1; i; i--)
		for (int j = 0; j < s[i].length(); j++)
			if (isupper(s[i][j]))
				t[i] += t[i + 1];
			else
				t[i] += s[i][j];
	int rep;
	pow10[0] = 1;
	for (int i = 1;; i++)
	{
		pow10[i] = pow10[i - 1] * 10 % n;
		if (vis[pow10[i]])
		{
			rep = i;
			break;
		}
		vis[pow10[i]] = true;
	}
	int len = t[1].length();
	t[1] = ' ' + t[1];
	sum[0] = 0;
	int ans = 0;
	for (int i = 1; i <= len; i++)
	{
		sum[i] = (sum[i - 1] * 10 + t[1][i] - '0') % n;
		for (int j = i - 1; j >= 0 && i - j < rep; j--)
			bucket[i % rep][sum[j] * pow10[i - j]]++;
		ans += bucket[i % rep][sum[i]];
	}
	fout << ans << endl;
	return 0;
}
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#include<cstring>
int A[50],B[50],a[50],Anum[50],Num[50];
int n,Len,num_A,num_B,CNT;
int vis[50];
bool Mark1,Mark2;
void dfs(int x,int num)
{
//	printf("%d %d\n",x,num);
	CNT++;
	if (CNT > 10000000)
	{
		Mark1 = true;
		return ;
	}
//	printf("%d\n",CNT);

	if (num == Len)
	{
		num_A=0;
		num_B=0;
		for (int i=1; i<=n; i++)
		{
			if (vis[i] == 1)
			{
				num_A++;
				A[num_A] = a[i];
			}
			else
			{
				num_B++;
				B[num_B] = a[i];
			}
		}
//		putchar('A');
//		for (int i=1;i<=Len;i++) printf("%d ",A[i]);puts("");
//		putchar('B');
//		for (int i=1;i<=Len;i++) printf("%d ",B[i]); puts("");
		//判断
		bool flag=true;
		for (int  i=1; i<=Len; i++)
		{
			if (A[i]!=B[i])
			{
				flag=false;
				break;
			}
		}
		if (flag)
		{
			Mark1=true;
			Mark2=true;
		}
		return ;
	}
	if (x > n) return ;
	vis[x] = 1;
	Num[a[x]]++;
	if (Num[a[x]]*2 <= Anum[a[x]]) dfs(x+1,num+1);
	vis[x] = 0;
	Num[a[x]]--;
	if (Mark1) return ;
	dfs(x+1, num);
	if (Mark1) return ;
}


int T;
int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		Len=n >> 1;
		memset(Anum,0,sizeof(Anum));
		memset(vis,0,sizeof(vis));
		memset(Num,0,sizeof(Num));
		for (int i=1; i<=n; i++)
		{
			scanf("%d",&a[i]);
			Anum[a[i]]++;
		}
		//=======================
		bool flag=false;
		for (int i=1; i<=40; i++)
		{
			if (Anum[i] & 1) flag=true;
		}
		if (flag)
		{
			puts("Furude Rika");
			continue;
		}
		//---------------------------处理奇数问题
		Mark1=false;
		Mark2=false;
		CNT=0;
		dfs(1,0);
		if (Mark1 && Mark2) puts("Frederica Bernkastel");
		else
		{
			Mark1=false;
			Mark2=false;
			for (int i=1,j=n; i<j; i++,j--)
			{
				int t=a[i];
				a[i]=a[j];
				a[j]=t;
			}
			CNT=0;
			dfs(1,0);
			if (Mark1 && Mark2) puts("Frederica Bernkastel");
			else puts("Furude Rika");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
3
4
1 3 3 1
4
1 1 2 2
4
1 3 3 1
*/

#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
typedef long long LL;
const LL maxn = 1e5+5;
LL f[maxn],a[maxn];
LL n;
int main(){
	freopen("energy.in","r",stdin);
	freopen("energy.out","w",stdout);
	memset(f,0,sizeof(f));
	scanf("%lld",&n);
	for (int i=1;i<=n;i++) scanf("%lld",&a[i]);
	f[n] = a[n];
	for (int i=n-1;i>=1;i--){
		if (a[i] > f[i+1]+1) f[i] = a[i]; else
							 f[i] = f[i+1] +1;
	}
	printf("%lld\n",f[1]);
	fclose(stdin); 
	fclose(stdout);
}

#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#include<cstring>
typedef long long LL;
struct T
{
	LL num;
	LL cnt;
	LL len;
	LL pre[33][33];//保存前缀，值mod n = 左，10^长度 % n = 后
	LL nxt[33];
} _x[10],ans[255];
LL n,r,k;
void merge(T &a,T &b)
{
	LL tmp[33];
	a.cnt=a.cnt+b.cnt;
	for (LL i=0; i<n; i++)
	{
		for (LL k=0; k<n; k++)
		{
			LL j=n - (i*k % n);
			if (j == n) j = 0;
			a.cnt=(a.cnt + b.pre[j][k] * a.nxt[i] % r) % r;
		}
	}
	//合并cnt
	for (LL i=0; i<n; i++)
	{
		for (LL j=0; j<n; j++)
		{
			a.pre[(a.num * j + i) % n][j *a.len % n] += b.pre[i][j];
			a.pre[(a.num * j + i) % n][j *a.len % n] %= r;
		}
	}
	//合并pre
	for (LL i=0; i<n; i++)
	{
		tmp[i] = a.nxt[i];
	}
	for (LL i=0; i<n; i++)
	{
		a.nxt[i] = b.nxt[i];
	}
	for (LL i=0; i<n; i++)
	{
		a.nxt[(i * b.len + b.num) % n] += tmp[i];
		a.nxt[(i * b.len + b.num) % n] %= r;
	}
	//合并nxt
	a.num = (a.num * b.len + b.num) %n;
	//合并num
	a.len = (a.len * b.len) % n;
	//合并len
}
LL F[255],Len[255];
char s[255][255];
int main()
{
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	scanf("%lld%lld",&n,&r);
	scanf("%lld",&k);
	for (LL i=1; i<=k; i++)
	{
		char c=getchar();
		while (!((c>='A') && (c<='Z'))) c=getchar();
		LL x = c - 'A';
		F[i]=x;
		c=getchar();
		c=getchar();
		c=getchar();
		Len[i] = 0;
		while (((c>='0') && (c<='9')) || ((c>='A') && (c<='Z')))
		{
			Len[i] ++;
			s[i][Len[i]] = c;
			c=getchar();
		}
	}
	//read
	memset(_x,0,sizeof(_x));
	_x[0] . cnt = 1;
	_x[0] . num = 0;
	_x[0] . len = 10 % n;//=10^真实长度 % n
	_x[0] . pre[0][10 % n] = 1;
	_x[0] . nxt[0] = 0;
	for (LL i=1; i<=9; i++)
	{
		_x[i] . cnt = (i % n) == 0;
		_x[i] . num = i % n;
		_x[i] . len = 10 % n;
		_x[i] . pre[i % n][10 % n] = 1;
		_x[i] . nxt[i % n] = 1;
	}
	//init
	memset(ans,0,sizeof(ans));
	for (LL i=k; i>=1; i--)
	{
		LL x=F[i];

		ans[x] . len = 1;
		ans[x] . num = 0;
		ans[x] . cnt = 0;

//		printf("%lld\n",x);
		for (LL j=1; j<=Len[i]; j++)
		{
//			printf("%lld %lld",j,Len[i]);
			if ((s[i][j] >= '0') && (s[i][j] <= '9'))
			{
//				printf(" %c\n",s[i][j] );
				merge(ans[x],_x[s[i][j]-'0']);
			}//是数字
			else
			{
//				printf(" %c\n",s[i][j] );
				merge(ans[x],ans[s[i][j] - 'A']);
			}
//			printf("cnt=%lld,num=%lld,len=%lld\n",ans[x].cnt,ans[x].num,ans[x].len);
		}
	}
	printf("%lld\n",ans[0].cnt);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

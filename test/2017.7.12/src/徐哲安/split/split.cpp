#include<bits/stdc++.h>
using namespace std;

const int maxn = 45;
int n, m, p, q, a[maxn], cnt[maxn];
int x[maxn], y[maxn], c[maxn];
bool flag;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

inline void init(){
	memset(a, 0, sizeof a);
	memset(cnt, 0, sizeof cnt);
	memset(c, 0, sizeof c);
	memset(x, 0, sizeof x);
	memset(y, 0, sizeof y);
}

bool judge(int last){
	int tmp = q;
	while (q < m){
		y[++q] = a[++last];
		if (y[q] != x[q]) { q = tmp; return false; }
	}
	return true;
}

void dfs(int k, int last){
	if (k > m) { flag = judge(last); return; }
	int tmp = q;
	for (int i=last+1; i<=m+k; i++){
		if (c[a[i]] < cnt[a[i]]){
			x[++p] = a[i];
			if (!(p <= q && x[p] != y[p])){
				c[a[i]]++;
				dfs(k+1, i);
				if (flag) return;
				c[a[i]]--;
			}
			p--;
			
		}
		y[++q] = a[i];
		if (q <= p && x[q] != y[q]) goto END;
	}
END:
	q = tmp;
}

int main(){
	freopen("split.in", "r", stdin);
	freopen("split.out", "w", stdout);
	int T; T = read();
	while (T--){
		n = read(); init(); m = n>>1;
		for (int i=1; i<=n; i++){
			a[i] = read();
			cnt[a[i]]++;
		}
		
		flag = false;
		for (int i=1; i<maxn; i++)
			if (cnt[i]&1){
				puts("Furude Rika");
				flag = true; break;
			}
		if (flag) continue; 
		
		p = q = 0; x[++p] = a[1];
		c[a[1]]++; dfs(2, 1);
		if (flag) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

const int maxn = 45;
int a[maxn], b[maxn];

int main(){
	freopen("split.in", "w", stdout);
	int T = 5, tmp; printf("%d\n", T); srand(time(NULL));
	while (T--){
		int n = (rand()%12+1) * 2;
		printf("%d\n", n);
		memset(a, 0, sizeof a);
		memset(b, 0, sizeof b);
		for (int i=1; i<=n/2; i++)
			a[rand()%40+1]++;
		for (int i=1; i<=40; i++)
			for (int j=1; j<=2*a[i]; j++){
				tmp = rand()%n+1;
				while (b[tmp])
					tmp = rand()%n+1;
				b[tmp] = i;
			}
		for (int i=1; i<=n; i++) printf("%d ", b[i]);
		puts("");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

const int maxn = 41;
int n, m, a[maxn], cnt[maxn];
int b[maxn], c[maxn];
bool flag;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

inline void init(){
	memset(a, 0, sizeof a);
	memset(cnt, 0, sizeof cnt);
	memset(b, 0, sizeof b);
	memset(c, 0, sizeof c);
}

int x[maxn], y[maxn];
bool judge(){
	int p = 0, q = 0;
	for (int i=1; i<=n; i++)
		if (b[i]){
			x[++p] = a[i];
			if (p <= q && x[p] != y[p]) return false;
		}
		else{
			y[++q] = a[i];
			if (q <= p && y[q] != x[q]) return false;
		}
	return true;
}

void dfs(int k, int last){
	if (k > m) { flag = judge(); return; }
	for (int i=last+1; i<=m+k; i++)
		if (c[a[i]] < cnt[a[i]]){
			b[i] = 1;
			c[a[i]]++;
			dfs(k+1, i);
			if (flag) return;
			c[a[i]]--;
			b[i] = 0;
		}
}

int main(){
	freopen("split.in", "r", stdin);
	freopen("std.out", "w", stdout);
	int T; T = read();
	while (T--){
		n = read(); init(); m = n>>1;
		for (int i=1; i<=n; i++){
			a[i] = read()-1;
			cnt[a[i]]++;
		}
		
		flag = false;
		for (int i=0; i<maxn; i++)
			if (cnt[i]&1){
				puts("Furude Rika");
				flag = true; break;
			}
		if (flag) continue; 
		
		b[1] = 1; c[a[1]]++;
		dfs(2, 1);
		if (flag) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

inline int read(){
	char ch = getchar(); int x = 0, f = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') f = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x*f;
}

int main(){
	freopen("energy.in", "r", stdin);
	freopen("energy.out", "w", stdout);
	
	int n, ans = 0;
	n = read();
	for (int i=0; i<n; i++)
		ans = max(ans, read()+i);
	printf("%d\n", ans);
	
	return 0;
}

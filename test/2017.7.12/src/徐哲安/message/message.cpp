#include<bits/stdc++.h>
using namespace std;
typedef long long LL;

const int maxn = 40;
struct node{
	int ans[maxn], fnt[maxn], bot[maxn];
	int len, in, r, tot; char t[maxn];
} a[maxn];
int n, MOD, m, p;
int q[maxn];
char ch;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

inline char readchar(){
	char ch = getchar();
	while (ch < 'A' || 'Z' < ch) ch = getchar();
	return ch;
}

inline bool isNum(char t){
	return '0' <= t && t <= '9';
}

inline int power(int p){
	int res = 1, x = 10;
	while (p){
		res = 1LL*res*x%n;
		x = 1LL*x*x%n; p >>= 1;
	}
	return res;
}

void solve(){
	int l = 0, r = 0, u, v, w, tmp, tmp1[maxn], tmp2[maxn], len;
	for (int i=0; i<m; i++)
		if (!a[i].in) q[r++] = i;
	for (int i=0; i<=9; i++){
		a[i+26].ans[i%n] = 1;
		a[i+26].fnt[i%n] = 1;
		if (i) a[i+26].bot[i%n] = 1;
		a[i+26].r = i%n;
		a[i+26].in = 1e9;
		a[i+26].tot = 1;
	}
	while (l < r){
		u = q[l++];
		//区间余数,长度
		for (int i=0; i<a[u].len; i++){
			v = a[u].t[i];
			a[u].r = (1LL*a[u].r*power(a[v].tot)%n+a[v].r) % n;
			a[u].tot += a[v].tot;
		} 
		//前缀
		tmp = 0;
		for (int i=0; i<a[u].len; i++){
			v = a[u].t[i];
			for (int j=0; j<n; j++)
				a[u].fnt[(j+1LL*tmp*power(a[v].tot)%n)%n] = (a[u].fnt[(j+1LL*tmp*power(a[v].tot)%n)%n] + a[v].fnt[j]) % MOD;
			tmp = (1LL*tmp+power(a[v].tot)+a[v].r) % n;
		}
		//后缀
		tmp = 0; len = 0;
		for (int i=a[u].len-1; i>=0; i--){
			v = a[u].t[i];
			for (int j=0; j<n; j++)
				a[u].bot[(tmp+1LL*j*power(len)%n)%n] = (a[u].bot[(tmp+1LL*j*power(a[v].tot)%n)%n] + a[v].bot[j]) % MOD;
			tmp = (tmp+1LL*a[v].r*power(len));
			len += a[v].tot;
		}
		//区间 
		for (int i=0; i<a[u].len; i++){
			v = a[u].t[i];
			for (int j=0; j<n; j++){
				a[u].ans[j] = (a[u].ans[j] + a[v].ans[j]) % MOD;
				tmp1[j] = a[v].bot[j];
			}
			for (int j=i+1; j<a[u].len; j++){
				for (int k=0; k<n; k++)
					for (int l=0; l<n; l++)
						a[u].ans[(1LL*k*power(a[v].tot)+l)%n] = (a[u].ans[(1LL*k*power(a[v].tot)+l)%n] + 1LL*tmp1[k]*a[v].fnt[l]) % MOD;
				for (int k=0; k<n; k++)
					tmp2[(1LL*k*power(a[v].tot)+a[v].r)%n] = tmp1[k];
				for (int k=0; k<n; k++)
					tmp1[k] = tmp2[k];
			}
		}
		//入度为0的点入队
		for (int i=0; i<26; i++)
			for (int j=0; j<a[i].len; j++){
				v = a[i].t[j];
				if (u == v){
					a[i].in--;
					if (a[i].in == 0) q[r++] = i;
				}
			} 
	}
}

int main(){
	freopen("message.in", "r", stdin);
	freopen("message.out", "w", stdout);
	n = read(); MOD = read(); m = read();
	for (int i=1; i<=m; i++){
		ch = readchar(); p = ch - 'A';
		scanf("->%s", &a[p].t);
		a[p].len = strlen(a[p].t);
		for (int i=0; i<=a[p].len-1; i++)
			if (!isNum(a[p].t[i]))
				a[a[p].t[i]-'A'].in++;
				
		for (int i=0; i<=a[p].len-1; i++)
			if (isNum(a[p].t[i])) a[p].t[i] = a[p].t[i]-'0'+26;
			else a[p].t[i] = a[p].t[i]-'A';
	}
	solve(); printf("%d\n", a[0].ans[0]);
	return 0;
}

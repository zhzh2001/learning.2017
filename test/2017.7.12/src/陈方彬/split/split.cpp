#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=40+5;
int q1[N],q2[N],t1,t2,l;
int a[N];
int n,m,w;
bool ok;
void dfs(int k){
	if(k>n){
		if(t1!=t2)return;
		if(l<=t1)return;
		ok=1;return;
	}
	if(t1>=l&&a[k]==q1[l]){
		q2[++t2]=a[k];
		l++;
		dfs(k+1);
		t2--;l--;
	}
	if(ok)return;
	q1[++t1]=a[k];
	dfs(k+1);
	t1--;
}
int main()
{
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	scanf("%d",&w);
	while(w--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%d",&a[i]);
		t1=t2=ok=0;l=1;
		dfs(1);
		if(ok)printf("Frederica Bernkastel\n");else printf("Furude Rika\n");
	}
}

#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define inf 1e9
#define mp make_pair
#define N 45
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0;int f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,a[N],c[N],d[N];
bool gg;
bool pd(){
	bool flag=1;
	For(i,1,c[0]) if(c[i]!=d[i]){flag=0;break;}
	return flag;
}
inline void dfs(int x){
	if(x==n+1){
		if(c[0]==d[0]) if(pd()) gg=1;
		return;
	}
	c[++c[0]]=a[x];dfs(x+1);c[0]--;
	if(gg) return;
	d[++d[0]]=a[x];dfs(x+1);d[0]--;
}
int main(){
	freopen("split.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int T=read();
	while(T--){
		gg=0;n=read();
		c[0]=0;d[0]=0;
		For(i,1,n) a[i]=read();
		dfs(1);
		if(gg) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	return 0;
}
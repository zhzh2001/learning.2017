#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define inf 1e9
#define N 45
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0;int f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int a[N],n,b[N],gg;
int p[N],nxt[N];
int c[N],d[N];
int e[N],f[N];
inline void dfs(int x){
	if(x==n+1){gg=1;return;}
	cout<<x<<endl;
	For(i,1,c[0]) cout<<c[i]<<" ";cout<<endl;
	For(i,1,d[0]) cout<<d[i]<<" ";cout<<endl;
	c[++c[0]]=a[x];e[a[x]]++;
	if(d[0]>=c[0]&&c[c[0]]!=d[c[0]]){;}
	else{
		if(e[a[x]]*2<=b[a[x]]){
			int p=c[0],q=nxt[x]-1;bool flag=1;
			if(c[0]>d[0]){
				For(i,x+1,nxt[x]-1){
					c[++c[0]]=a[i];
					e[a[i]]++;
					if(e[a[i]]*2>b[a[i]]){q=i;flag=0;break;}
				}
				if(flag) dfs(nxt[x]);
				c[0]=p;if(gg) return;
				For(i,x+1,q) e[a[i]]--;
			}else dfs(x+1);
		}
	}
	if(gg) return;
	c[0]--;e[a[x]]--;
	d[++d[0]]=a[x];f[a[x]]++;
	if(c[0]>=d[0]&&d[d[0]]!=c[d[0]]){;}
	else{
		if(f[a[x]]*2<=b[a[x]]){
			int p=d[0],q=nxt[x]-1;bool flag=1;
			if(d[0]>c[0]){
				For(i,x+1,nxt[x]-1){
					d[++d[0]]=a[i];
					f[a[i]]++;
					if(f[a[i]]*2>b[a[i]]){q=i;flag=0;break;}
				}
				if(flag) dfs(nxt[x]);
				d[0]=p;if(gg) return;
				For(i,x+1,q) f[a[i]]--;
			}else dfs(x+1);
		}
	}
	d[0]--;f[a[x]]--;
}
inline void init(){
	memset(b,0,sizeof b);
	memset(c,0,sizeof c);
	memset(d,0,sizeof d);
	memset(nxt,0,sizeof nxt);
	memset(p,0,sizeof p);
	memset(e,0,sizeof e);
	memset(f,0,sizeof f);
}
int main(){
//	freopen("split.in","r",stdin);
//	freopen("split.out","w",stdout);
	int T=read();
	while(T--){
		init();
		n=read();For(i,1,n) a[i]=read();
		For(i,1,n) b[a[i]]++;
		bool flag=1;gg=0;
		For(i,1,n) if(b[i]&1){flag=0;break;}
		if(!flag){puts("Furude Rika");continue;}
		For(i,1,n) p[i]=n+1;
		Rep(i,n,1) nxt[i]=p[a[i]],p[a[i]]=i;
		dfs(1);
		if(gg) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	return 0;
}
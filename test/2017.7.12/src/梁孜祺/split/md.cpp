#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define inf 1e9
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0;int f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int a[25],b[25];
int main(){
	freopen("split.in","w",stdout);
	srand(time(NULL));
	int T=rand()%5+1;
	writeln(T);
	while(T--){
		int n=rand()%20+2;
		while(n&1) n=rand()%20+2;
		writeln(n);a[0]=b[0]=0;
		For(i,1,n/2) a[i]=b[i]=rand()%n+1;
		For(i,1,n)
			if((rand()%2||b[0]==n/2)&&a[0]!=n/2) write(a[++a[0]]),putchar(' ');
			else write(b[++b[0]]),putchar(' ');
		putchar('\n');
	}
	return 0;
}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
int n,Num[45],A[45],top,now,s[45];
int main(){
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(Num,0,sizeof(Num));
		for (int i=1;i<=n;i++){
			scanf("%d",&A[i]);
			Num[A[i]]++;
		}
		bool F=true;
		for (int i=1;i<=40;i++)
		if (Num[i]&1){
			F=false;
			break;
		}
		if (!F){
			puts("Furude Rika");
			continue;
		}
		top=0; s[++top]=A[1];
		now=1;
		for (int i=2;i<=n;i++)
		if (now>top) s[++top]=A[i];
		else
		if (A[i]==s[now]) now++;
		else s[++top]=A[i];
		if (now>top) puts("Frederica Bernkastel");
		else puts("Furude Rika");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

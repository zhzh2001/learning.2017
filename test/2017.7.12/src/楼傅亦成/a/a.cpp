#include<bits/stdc++.h>
using namespace std;
int read(){
	int tot=0;
	int f=1;
	char c=getchar();
	while(!isdigit(c) && c!='-')
		c=getchar();
	if(c=='-')
		f=-1,c=getchar();
	while(isdigit(c)){
		tot=tot*10+c-'0';
		c=getchar();
	}
	return tot*f;
}
int n,m;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	int ans=0;
	for(int i=1;i<=n;i++){
		m=read();
		ans=max(i+m,ans);
	}
	printf("%d\n",ans-1);
	return 0;
}

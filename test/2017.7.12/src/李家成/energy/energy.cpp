#include<cstdio>
const int N=1e5+10;
int n,i,a[N],ans;
inline int I(){
  int x=0;char c=getchar();
  while(c<'0'||c>'9')c=getchar();
  while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
  return x;
}
int main()
{
  freopen("energy.in","r",stdin);
  freopen("energy.out","w",stdout);
  n=I();
  for(i=n;i;i--)
    a[i]=I();
  for(ans=a[1],i=2;i<=n;i++){
    ans++;
    if(ans<a[i])ans=a[i];
  }printf("%d\n",ans);
}

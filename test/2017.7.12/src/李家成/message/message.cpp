#include<algorithm>
#include<cstdio>
const int A=110;
char s[A][A],ep[A][100010];
int n,r,k,i,j,tmp,avb[A],ans;
char o;

inline int I(){
  int x=0;char c=getchar();
  while(c<'0'||c>'9')c=getchar();
  while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
  return x;
}

inline char Ia(){
  char c=getchar();
  while(c<'A'||c>'Z')c=getchar();
  return c;
}

inline void Is(char o){
  char c=getchar();
  while((c<'A'||c>'Z')&&(c<'0'||c>'9'))c=getchar();
  while((c>='A'&&c<='Z')||(c>='0'&&c<='9'))
  	s[o][++s[o][0]]=c,c=getchar();
}

inline void Expd(char c){
	int pt=1;
	while(pt<=s[c][0]){
		if(s[c][pt]<='9'&&s[c][pt]>='0')ep[c][++ep[c][0]]=s[c][pt]-'0';
		else {
			if(!avb[s[c][pt]])Expd(s[c][pt]);
			for(int j=1;j<=ep[s[c][pt]][0];j++)
				ep[c][ep[c][0]+j]=ep[s[c][pt]][j];
			ep[c][0]+=ep[s[c][pt]][0];
		}pt++;
	}avb[c]=1;
}

int main()
{
  freopen("message.in","r",stdin);
  freopen("message.out","w",stdout);
  n=I(),r=I(),k=I();
  for(i=1;i<=k;i++)
    o=Ia(),Is(o),avb[i]=0;
  Expd('A');
  if(ep['A'][0]>(int)1e5)return 0;
  for(i=1;i<=ep['A'][0];i++)
    if(ep['A'][i])
		for(tmp=0,j=i;j<=ep['A'][0];j++){
      		tmp=(tmp*10+ep['A'][j])%n;
      		if(!tmp)ans=(ans+1)%r;
    	}else ans=(ans+1)%r;
  printf("%d\n",ans);
}

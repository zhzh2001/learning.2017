//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,r,m,n,t;
int a[50],num[50];
bool flag,Flag[50];
int main() {
	freopen("split.in","r",stdin);
	freopen("split.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&t);
	while (t--) {
		scanf ("%d",&n);
		flag=false;
		memset(num,0,sizeof(num));
		Rep(i,1,n) {
			scanf ("%d",&a[i]);
			num[a[i]]++;
		}
		Rep(i,1,40) {
			if (num[i]%2!=0) {
				cout<<"Furude Rika"<<endl;
				flag=true;
				break;
			}
		}
		if (flag) continue;
		if (n%2!=0) {
			cout<<"Furude Rika"<<endl;
			continue;
		}
		memset(Flag,false,sizeof(Flag));
		Flag[0]=true;
		for (int i=2; i<=n; i+=2) for (int j=i-2; j>=0; j-=2) {
				if (Flag[j]) {
					l=j+1;
					r=(i+j)/2+1;
					while (a[l]==a[r] && r<=i) {
						l++;
						r++;
					}
					if (r>i) {
						Flag[i]=true;
						break;
					}
				}
			}
		if (Flag[n]) cout<<"Frederica Bernkastel"<<endl;
		else cout<<"Furude Rika"<<endl;
	}
	return 0;
}

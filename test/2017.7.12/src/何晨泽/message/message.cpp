//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
ff i,j,k,l,m,n,r,tot;
ff a[200],f[200],F[200],ss[1000000];
char s[50][200];
inline void dfs(char c) {
	if (c>='0' && c<='9') {
		ss[++tot]=c-'0';
		return;
	}
	ff pos=a[c];
	ff l=strlen(s[pos]);
	Rep(i,3,l-1) {
		dfs(s[pos][i]);
	}
}
inline void force() {
	dfs('A');
	ff ans=0;
	memset(F,0,sizeof(F));
	Rep(i,1,tot) {
		memset(f,0,sizeof(f));
		Rep(j,0,n-1) {
			f[(j*10+ss[i])%n]+=F[j];
		}
		if (ss[i]!=0) {
			f[ss[i]%n]++;
		} else ans++;
		(ans+=f[0])%=r;
		Rep(j,0,n-1) {
			F[j]=f[j];
		}
	}
	cout<<ans%r<<endl;
	exit(0);
}
int main() {
	freopen("message.in","r",stdin);
	freopen("message.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>r>>k;
	Rep(i,1,k) {
		cin>>s[i];
		a[s[i][0]]=i;
	}
	force();
	return 0;
}

#include<fstream>
#include<map>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("gaosi.in");
ofstream fout("gaosi.out");
typedef map<int,vector<int> > mv;
mv row,col;
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	if(n<=2)
	{
		fout<<0<<endl;
		return 0;
	}
	while(k--)
	{
		int x,y;
		fin>>x>>y;
		row[x].push_back(y);
		col[y].push_back(x);
	}
	int cnt=0,r;
	for(mv::iterator i=row.begin();i!=row.end();i++)
	{
		sort(i->second.begin(),i->second.end());
		int k=unique(i->second.begin(),i->second.end())-i->second.begin();
		i->second.resize(k);
		if(k>cnt)
		{
			cnt=k;
			r=i->first;
		}
	}
	for(mv::iterator i=col.begin();i!=col.end();i++)
	{
		sort(i->second.begin(),i->second.end());
		int k=unique(i->second.begin(),i->second.end())-i->second.begin();
		i->second.resize(k);
	}
	if(cnt==0)
		fout<<m<<endl;
	else
	{
		int cnt=0;
		for(int i=0;i<row[r].size();i++)
			if(i)
			{
				if(row[r][i]-row[r][i-1]>1)
					for(int j=row[r][i-1]+1;j<row[r][i];j++)
						if(col[j].size())
						{
							int t=lower_bound(col[j].begin(),col[j].end(),r)-col[j].begin();
							if(col[j][t]-r<=j-row[r][i-1]||col[j][t]-r<=row[r][i]-j)
								cnt++;
						}
			}
			else
				if(row[r][i]>1)
					for(int j=1;j<row[r][i];j++)
						if(col[j].size())
						{
							int t=lower_bound(col[j].begin(),col[j].end(),r)-col[j].begin();
							if(col[j][t]-r<=j||col[j][t]-r<=row[r][i]-j)
								cnt++;
						}
		int cnt2=0;
		for(int i=0;i<row[r].size();i++)
			if(i)
			{
				if(row[r][i]-row[r][i-1]>1)
					for(int j=row[r][i-1]+1;j<row[r][i];j++)
						if(col[j].size())
						{
							int t=lower_bound(col[j].begin(),col[j].end(),r)-col[j].begin();
							if(t&&(col[j][t-1]-r<=j-row[r][i-1]||col[j][t-1]-r<=row[r][i]-j))
								cnt2++;
						}
			}
			else
				if(row[r][i]>1)
					for(int j=1;j<row[r][i];j++)
						if(col[j].size())
						{
							int t=lower_bound(col[j].begin(),col[j].end(),r)-col[j].begin();
							if(t&&(col[j][t-1]-r<=j||col[j][t-1]-r<=row[r][i]-j))
								cnt2++;
						}
		fout<<m-row[r].size()-max(cnt,cnt2)<<endl;
	}
	return 0;
}
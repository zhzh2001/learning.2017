#include<fstream>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("soap.in");
ofstream fout("soap.out");
int n,d,x[105],y[105],ans;
vector<int> ans2[105];
bool mat[105][105],vis[105],now[105],l[105];
void dfs(int k,int cnt)
{
	if(k==n+1)
	{
		if(cnt>ans)
		{
			ans=cnt;
			memcpy(l,now,sizeof(l));
		}
		return;
	}
	now[k]=false;
	dfs(k+1,cnt);
	for(int i=1;i<k;i++)
		if(now[i]&&!mat[i][k])
			return;
	now[k]=true;
	dfs(k+1,cnt+1);
}
void dfs2(int now,int s)
{
	ans2[s].push_back(now);
	vis[now]=true;
	for(int i=1;i<=n;i++)
		if(!vis[i])
		{
			bool flag=true;
			for(int j=0;j<ans2[s].size();j++)
				if(!mat[ans2[s][j]][i])
				{
					flag=false;
					break;
				}
			if(flag)
				dfs2(i,s);
		}
}
int main()
{
	fin>>n>>d;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])<=d*d);
	if(n<=15)
	{
		ans=0;
		dfs(1,0);
		fout<<ans<<endl;
		for(int i=1;i<=n;i++)
			if(l[i])
				fout<<i<<' ';
		fout<<endl;
	}
	else
	{
		int cnt=0;
		for(int i=1;i<=n;i++)
		{
			memset(vis,false,sizeof(vis));
			dfs(i,i);
			cnt=max(cnt,(int)ans2[i].size());
		}
		fout<<cnt<<endl;
		for(int i=1;i<=n;i++)
			if(ans2[i].size()==cnt)
			{
				for(int j=0;j<ans2[i].size();j++)
					fout<<ans2[i][j]<<' ';
				fout<<endl;
				break;
			}
	}
	return 0;
}
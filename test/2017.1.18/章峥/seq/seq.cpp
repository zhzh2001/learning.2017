#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("seq.in");
ofstream fout("seq.out");
int a[100005];
long long f[100005],g[100005];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	f[1]=a[1];
	for(int i=2;i<=n;i++)
		f[i]=max(f[i-1],max(f[i-1]+a[i],(long long)a[i]));
	g[n]=a[n];
	for(int i=n-1;i;i--)
		g[i]=max(g[i+1],max(g[i+1]+a[i],(long long)a[i]));
	long long ans=-1e18;
	for(int i=1;i<n;i++)
		ans=max(ans,f[i]+g[i+1]);
	fout<<ans<<endl;
	return 0;
}
#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
long long n,maxx,k,a[100050],f1[100050],f2[100050];
int main()
{
	freopen("seq.in","r",stdin);freopen("seq.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	for (int i=1;i<=n;i++)cin>>a[i];
	maxx=a[1];k=0;
	for (int i=1;i<=n;i++)
		{
			k+=a[i];
			maxx=max(maxx,k);
			if (k<0) k=0;
			f1[i]=maxx;
		}
	maxx=a[n];k=0;
	for (int i=n;i>=1;i--)
		{
			k+=a[i];
			maxx=max(maxx,k);
			if (k<0) k=0;
			f2[i]=maxx;
		}
	maxx=-1e15;
	for (int i=1;i<n;i++) maxx=max(maxx,f1[i]+f2[i+1]);
	cout<<maxx;
} 

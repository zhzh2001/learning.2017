#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
long long n,m,k,x[10000],y[10000],ans,a[2000][2000],f[2000][2000];
bool flag[10000];
void doit1()//平面图最小流转对偶图最短路 
	{
		long long f[10000];
		for (long long i=1;i<=k;i++)cin>>x[i]>>y[i];
		for (long long i=1;i<=k;i++) f[i]=y[i]-1;
		f[0]=1e15;
		for (long long i=1;i<=k;i++)
			{
				long long kk=0;
				for (long long j=1;j<=k;j++)
					if ((f[j]<f[kk])&&(!flag[j])) kk=j;
				flag[kk]=true;
				for (long long j=1;j<=k;j++)
					if (!flag[j]) f[j]=min(f[j],f[kk]-1+max(abs(x[j]-x[kk]),abs(y[j]-y[kk])));
			}
		ans=m;
		for (long long i=1;i<=k;i++)
			ans=min(ans,f[i]+m-y[i]);
		cout<<ans;
	}
void doit2()//最小流转dp 
	{
		long long x,y;
		for (long long i=1;i<=k;i++)cin>>x>>y,a[y][x]=-1;
		memset(f,100,sizeof(f));
		for (long long i=1;i<=n;i++) f[0][i]=0;
		for (long long i=1;i<=m;i++)
			for (long long j=1;j<=n;j++)
				f[i][j]=min(f[i-1][j-1],min(f[i-1][j],f[i-1][j+1]))+1+a[i][j];
		ans=1e15;
		for (long long i=1;i<=n;i++) ans=min(f[m][i],ans);
		cout<<ans;
	}
int main()
{
	freopen("gaosi.in","r",stdin);freopen("gaosi.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m>>k;
	if (k<=1000) doit1();
		else doit2();
}

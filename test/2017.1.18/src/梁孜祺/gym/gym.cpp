#include<iostream>
#include<string>
#include<cstring>
#include<cmath>
#include<set>
#include<algorithm>
#include<cstdlib>
#include<cstdio>
using namespace std;

int gcd(int x,int y)
{
	if(y==0) return x;
	else return gcd(y,x%y);
}
int main()
{
	freopen("gym.in","r",stdin);
	freopen("gym.out","w",stdout);
	long long l,n,m;
	scanf("%I64d%I64d%I64d",&l,&n,&m);
	if(n==m)
	{
		printf("1/1");
		return 0;
	}
	if(n>m) swap(n,m);
	long long ans=n-1;
	if(m>l)
	{
		long long p=gcd(l,ans);
		ans=ans/p;
		l=l/p;
		printf("%I64d%c%I64d",ans,'/',l);
		return 0;
	}
	long long sum=1;
	long long p=gcd(n,m);
	long long q=n*m/p;
	long long o=q+1;
	while(o%n!=0)
	{
		sum++;
		o++;
	}
	o=q;
	while(true)
	{
		if(o+sum-1<=l) ans=ans+sum; 
		if(o>l) break;
		if(o+sum-1>l) 
		{
			ans=ans+l-o+1;
			break;
		}
		o=o+q;
	}
	p=gcd(ans,l);
	ans=ans/p;
	l=l/p;
	printf("%I64d%c%I64d",ans,'/',l);
	return 0;
}

#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
using namespace std;
long long n,m,l,i,j;
long long gcd(long long a,long long b)
{
	if(a%b==0)return b;
	 else return gcd(b,a%b);
}
int main()
{
	freopen("gym.in","r",stdin);
	freopen("gym.out","w",stdout);
	cin>>l>>n>>m;
	if(n>m){
		i=n;
		n=m;
		m=i;
	}
	if(n>l){
		cout<<"1"<<endl;
		return 0;
	}
	j=gcd(n,m);
	if(n/j*m>l){
		i=gcd(n-1,l);
		cout<<(n-1)/i<<"/"<<l/i<<endl;
		return 0;
	}
	long long ans;
	ans=l/(n/j*m)*n;
	long long x=l%(n/j*m);
	if(x>n)ans=ans+n-1;
	 else ans=ans+x;
	i=gcd(ans,l);
	cout<<ans/i<<"/"<<l/i<<endl; 
	return 0;
}

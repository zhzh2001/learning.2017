#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#define ll long long
using namespace std;
const ll oo=0xfffffffffff;
ll f[100001][3],a[100001],g[100001];
int main()
{
	freopen("seq.in","r",stdin);
	freopen("seq.out","w",stdout);
	f[0][1]=-oo;f[0][2]=-oo;g[0]=-oo;
	int n;cin>>n;
	for(int i=1;i<=n;i++)cin>>a[i];
	for(int i=1;i<=n;i++){
		for(int j=1;j<=2;j++){
			f[i][j]=f[i-1][j]+a[i];
			if(j==1)f[i][j]=max(f[i][j],a[i]);
			if(j==2)f[i][j]=max(f[i][j],g[i-1]+a[i]);
		}
		g[i]=max(g[i-1],f[i][1]);
	}
	ll ans=-oo;
	for(int i=1;i<=n;i++)ans=max(ans,f[i][2]);
	cout<<ans<<endl;
	return 0;
}

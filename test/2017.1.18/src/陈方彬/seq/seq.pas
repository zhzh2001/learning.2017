var
a:array[0..100005]of int64;
f:array[0..100005,1..2,0..1]of int64;
n,i,j,k,sum:longint;
ans,x,y:int64;
function max(x,y:int64):int64;begin if x>y then exit(x);exit(y);end;
begin
assign(input,'seq.in');reset(input);
assign(output,'seq.out');rewrite(output);
  read(n);
  for i:=1 to n do read(a[i]);
  f[1][1][1]:=a[1];
  f[1][1][0]:=0;
  f[1][2][0]:=-maxlongint;
  f[1][2][1]:=-maxlongint;

  f[2][1][0]:=max(0,a[1]);
  f[2][1][1]:=a[2]+max(0,a[1]);
  f[2][2][0]:=max(0,a[1]);
  f[2][2][1]:=a[2]+max(0,a[1]);
  for i:=3 to n do
    begin
      f[i][1][0]:=max(f[i-1][1][0],f[i-1][1][1]);
      f[i][1][1]:=a[i]+max(0,f[i-1][1][1]);
      f[i][2][0]:=max(f[i][1][0],max(f[i-1][2][0],f[i-1][2][1]));
      f[i][2][1]:=a[i]+max(f[i][1][0],f[i-1][2][1]);
    end;
  ans:=max(f[n][2][0],f[n][2][1]);
  for i:=1 to n do if a[i]>0 then inc(sum);
  if sum=1 then ans:=0;
  if ans=0 then
    begin
      x:=-maxlongint;
      y:=-maxlongint;
      for i:=1 to n do if a[i]>x then x:=a[i] else if a[i]>y then y:=a[i];
      ans:=x+y;
    end;
  write(ans);
end.




#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("gym.in");
ofstream fout("gym.out");
struct bigint
{
	int len,dig[50];
	bigint()
	{
		len=1;
		memset(dig,0,sizeof(dig));
	}
	bigint(long long x)
	{
		memset(dig,0,sizeof(dig));
		len=0;
		do
			dig[++len]=x%10;
		while(x/=10);
	}
	bigint operator*(const bigint b)const
	{
		bigint res;
		res.len=len+b.len-1;
		for(int i=1;i<=len;i++)
			for(int j=1;j<=b.len;j++)
				res.dig[i+j-1]+=dig[i]*b.dig[j];
		int overflow=0;
		for(int i=1;i<=res.len;i++)
		{
			res.dig[i]+=overflow;
			overflow=res.dig[i]/10;
			res.dig[i]%=10;
		}
		if(overflow)
			res.dig[++res.len]=overflow;
		return res;
	}
	bool operator>(const bigint b)const
	{
		if(len!=b.len)
			return len>b.len;
		for(int i=len;i;i--)
			if(dig[i]!=b.dig[i])
				return dig[i]>b.dig[i];
		return false;
	}
	bool operator<=(const bigint b)const
	{
		return !(*this>b);
	}
};
long long gcd(long long a,long long b)
{
	return b==0?a:gcd(b,a%b);
}
int main()
{
	long long l,n,m;
	fin>>l>>n>>m;
	long long t=min(n,m),ans;
	if(t-1<=l)
		ans=t-1;
	else
		ans=l;
	long long g=gcd(n,m);
	bigint bn(n),bm(m),bmul=bn*bm;
	bigint bg(g),bl(l),bmul2=bg*bl;
	if(bmul<=bmul2)
	{
		long long lcm=n/g*m;
		ans+=l/lcm*t;
		if(l%lcm<min(n,m)-1)
			ans-=(min(n,m)-1-l%lcm);
	}
	g=gcd(ans,l);
	fout<<ans/g<<'/'<<l/g<<endl;
	return 0;
}
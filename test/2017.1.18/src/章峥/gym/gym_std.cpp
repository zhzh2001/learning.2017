#include<fstream>
using namespace std;
ifstream fin("gym.in");
ofstream fout("gym.ans");
int gcd(int a,int b)
{
	return b==0?a:gcd(b,a%b);
}
int main()
{
	int l,n,m;
	fin>>l>>n>>m;
	int ans=0;
	for(int i=1;i<=l;i++)
		if(i%n==i%m)
			ans++;
	int g=gcd(l,ans);
	fout<<ans/g<<'/'<<l/g<<endl;
	return 0;
}
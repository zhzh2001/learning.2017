#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<queue>
#include<cctype>
using namespace std;
const int INF=0x3f3f3f3f,dx[]={-1,-1,-1,1,1,1,0,0},dy[]={-1,0,1,-1,0,1,-1,1};
char buf[4400000],*now;
int x[500005],y[500005],mat[1005][1005],d[1005];
bool vis[1005],des[1005][1005],vis2[1005][1005];
struct point
{
	int x,y;
	point(int x,int y):x(x),y(y){};
};
queue<point> q[2];
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int main()
{
	freopen("gaosi.in","r",stdin);
	freopen("gaosi.out","w",stdout);
	fread(buf,1,4400000,stdin);
	now=buf-1;
	int n=getint(),m=getint(),k=getint();
	for(int i=1;i<=k;i++)
	{
		x[i]=getint();
		y[i]=getint();
	}
	if(n==1)
	{
		sort(y+1,y+k+1);
		printf("%d\n",m-(unique(y+1,y+k+1)-y-1));
	}
	else
		if(m==1)
			if(k)
				puts("0");
			else
				puts("1");
		else
			if(k<=1000)
			{
				//create graph
				mat[0][k+1]=m;
				for(int i=1;i<=k;i++)
				{
					mat[0][i]=y[i]-1;
					mat[i][k+1]=m-y[i];
				}
				for(int i=1;i<=k;i++)
					for(int j=1;j<=k;j++)
						mat[i][j]=max(abs(x[i]-x[j]),abs(y[i]-y[j]))-1;
				
				//dijkstra
				memset(vis,false,sizeof(vis));
				memset(d,0x3f,sizeof(d));
				d[0]=0;
				for(int i=1;i<=k+2;i++)
				{
					int j=-1;
					for(int t=0;t<=k+1;t++)
						if(!vis[t]&&(j==-1||d[t]<d[j]))
							j=t;
					vis[j]=true;
					for(int t=0;t<=k+1;t++)
						if(!vis[t])
							d[t]=min(d[t],d[j]+mat[j][t]);
				}
				
				//output
				printf("%d\n",d[k+1]);
			}
			else
			/*
			{
				//mark destroyed
				memset(des,true,sizeof(des));
				for(int i=1;i<=k;i++)
					des[x[i]][y[i]]=false;
				
				//dp
				memset(f,0x3f,sizeof(f));
				for(int i=1;i<=n;i++)
					f[i][0]=0;
				for(int j=1;j<=m;j++)
					for(int i=1;i<=n;i++)
						f[i][j]=min(f[i][j-1],min(f[i-1][j-1],f[i+1][j-1]))+des[i][j];
				
				//output
				int ans=INF;
				for(int i=1;i<=n;i++)
					ans=min(ans,f[i][m]);
				fout<<ans<<endl;
			}
			*/
			{
				//mark destroyed
				memset(des,true,sizeof(des));
				for(int i=1;i<=k;i++)
					des[x[i]][y[i]]=false;
				
				//bfs
				memset(vis2,false,sizeof(vis2));
				for(int i=1;i<=n;i++)
				{
					q[des[i][1]].push(point(i,1));
					vis2[i][1]=true;
				}
				
				for(int i=0,s=0;;i++,s^=1)
					while(!q[s].empty())
					{
						point k=q[s].front();q[s].pop();
						if(k.y==m)
						{
							printf("%d\n",i);
							return 0;
						}
						for(int j=0;j<8;j++)
						{
							int nx=k.x+dx[j],ny=k.y+dy[j];
							if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis2[nx][ny])
							{
								vis2[nx][ny]=true;
								q[s^des[nx][ny]].push(point(nx,ny));
							}
						}
					}
				
				/*
				//spfa
				memset(d2,0x3f,sizeof(d2));
				memset(vis2,false,sizeof(vis2));
				for(int i=1;i<=n;i++)
				{
					q.push(point(i,1));
					vis2[i][1]=true;
					d2[i][1]=des[i][1];
				}
				while(!q.empty())
				{
					point k=q.front();q.pop();
					vis2[k.x][k.y]=false;
					for(int i=0;i<8;i++)
					{
						int nx=k.x+dx[i],ny=k.y+dy[i];
						if(nx>0&&nx<=n&&ny>0&&ny<=m&&d2[k.x][k.y]+1<d2[nx][ny])
						{
							d2[nx][ny]=d2[k.x][k.y]+1;
							if(!vis2[nx][ny])
							{
								vis2[nx][ny]=true;
								q.push(point(nx,ny));
							}
						}
					}
				}
				
				//output
				int ans=INF;
				for(int i=1;i<=n;i++)
					ans=min(ans,d2[i][m]);
				fout<<ans<<endl;
				*/
			}
	return 0;
}
program gen;
var
  n,i:longint;
begin
  randomize;
  assign(output,'seq.in');
  rewrite(output);
  read(n);
  writeln(n);
  for i:=1 to n do
    write(random(2000000000)-1000000000,' ');
  close(output);
end.
#include<cstdio>
#include<algorithm>
#include<cctype>
using namespace std;
char buf[1100005],*now;
int a[100005];
long long f[100005],g[100005];
inline int getint()
{
	while(!isdigit(*now)&&*now!='-')
		now++;
	int ret=0,sign=1;
	if(isdigit(*now))
		ret=*now-'0';
	else
		sign=-1;
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret*sign;
}
int main()
{
	freopen("seq.in","r",stdin);
	freopen("seq.out","w",stdout);
	fread(buf,1,1100000,stdin);
	now=buf;
	int n=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	long long now=0;
	for(int i=1;i<=n;i++)
	{
		now+=a[i];
		f[i]=max(f[i-1],now);
		if(now<0)
			now=0;
	}
	now=0;
	for(int i=n;i;i--)
	{
		now+=a[i];
		g[i]=max(g[i+1],now);
		if(now<0)
			now=0;
	}
	long long ans=-1e18;
	for(int i=1;i<n;i++)
		ans=max(ans,f[i]+g[i+1]);
	printf("%I64d\n",ans);
	return 0;
}
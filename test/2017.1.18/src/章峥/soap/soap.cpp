#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
const int TIMES=100;
ifstream fin("soap.in");
ofstream fout("soap.out");
int n,d,x[105],y[105],p[105],now[105],ans[105];
bool mat[105][105];
int main()
{
	fin>>n>>d;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])<=d*d);
	for(int i=1;i<=n;i++)
		p[i]=i;
	int cc=0;
	for(int i=1;i<=TIMES;i++)
	{
		int cnt=0;
		for(int j=1;j<=n;j++)
		{
			bool flag=true;
			for(int k=1;k<=cnt;k++)
				if(!mat[p[j]][now[k]])
				{
					flag=false;
					break;
				}
			if(flag)
				now[++cnt]=p[j];
		}
		if(cnt>cc)
		{
			cc=cnt;
			memcpy(ans,now,sizeof(ans));
		}
		random_shuffle(p+1,p+n+1);
	}
	fout<<cc<<endl;
	for(int i=1;i<=cc;i++)
		fout<<ans[i]<<' ';
	fout<<endl;
	return 0;
}
#include<fstream>
#include<cstring>
#include<ctime>
#include<cstdlib>
using namespace std;
ifstream fin("soap.in");
ofstream fout("soap.out");
int n,d,x[105],y[105],ans;
bool mat[105][105],now[105],l[105];
void dfs(int k,int cnt)
{
	if(k==n+1)
	{
		if(cnt>ans)
		{
			ans=cnt;
			memcpy(l,now,sizeof(l));
		}
		return;
	}
	now[k]=false;
	dfs(k+1,cnt);
	if(clock()>CLOCKS_PER_SEC*0.4)
	{
		fout<<ans<<endl;
		for(int i=1;i<=n;i++)
			if(l[i])
				fout<<i<<' ';
		fout<<endl;
		exit(0);
	}
	for(int i=1;i<k;i++)
		if(now[i]&&!mat[i][k])
			return;
	now[k]=true;
	dfs(k+1,cnt+1);
}
int main()
{
	fin>>n>>d;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])<=d*d);
	ans=0;
	dfs(1,0);
	fout<<ans<<endl;
	for(int i=1;i<=n;i++)
		if(l[i])
			fout<<i<<' ';
	fout<<endl;
	return 0;
}
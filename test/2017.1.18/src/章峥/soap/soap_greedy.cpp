#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("soap.in");
ofstream fout("soap.out");
int x[105],y[105],mat[105][105],a[105];
struct edge
{
	int u,v,w;
	bool operator<(const edge b)const
	{
		return w<b.w;
	}
};
edge e[10005];
bool vis[105];
int main()
{
	int n,d;
	fin>>n>>d;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	int m=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(i!=j)
			{
				mat[i][j]=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]);
				if(mat[i][j]<=d*d)
				{
					e[++m].u=i;e[m].v=j;
					e[m].w=mat[i][j];
				}
			}
	sort(e+1,e+m+1);
	memset(vis,false,sizeof(vis));
	int cnt=0;
	for(int i=1;i<=m;i++)
	{
		bool flag=true;
		if(!vis[e[i].u])
			for(int j=1;j<=cnt;j++)
				if(mat[e[i].u][a[j]]>d*d)
				{
					flag=false;
					break;
				}
		if(flag)
		{
			if(!vis[e[i].u])
			{
				a[++cnt]=e[i].u;
				vis[e[i].u]=true;
			}
			if(!vis[e[i].v])
				for(int j=1;j<=cnt;j++)
					if(mat[e[i].v][a[j]]>d*d)
					{
						flag=false;
						break;
					}
			if(flag&&!vis[e[i].v])
			{
				a[++cnt]=e[i].v;
				vis[e[i].v]=true;
			}
		}
	}
	fout<<cnt<<endl;
	sort(a+1,a+cnt+1);
	for(int i=1;i<=cnt;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
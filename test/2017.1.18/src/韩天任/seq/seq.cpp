#include<iostream>
#include<cmath>
using namespace std;
int main(){
     freopen("seq.in","r",stdin);
     freopen("seq.out","w",stdout);
	int n;
	long long ans=0,l[100005],r[100005];
	int a[100005];
	cin>>n;
	for(int i=1;i<=n;i++){
		l[i]=-1000000004;
		r[i]=-1000000005;
		cin>>a[i];
	}
	long long num=0;
	l[0]=-1000000004;
	r[n+1]=-1000000005;
	for(int i=1;i<=n;i++){
		num+=a[i];
		l[i]=max(l[i-1],num);
		if (num<0)
			num=0;
	}
	num=0;
	for(int i=n;i>=1;i--){
		num+=a[i];
		r[i]=max(r[i+1],num);
		if (num<0)
			num=0;
	}
	ans=l[1]+r[2];
	for(int i=3;i<=n;i++)
		ans=max(ans,l[i-1]+r[i]);
	cout<<ans;
	return 0;
}

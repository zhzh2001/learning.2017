#include<cstdio>
using namespace std;
long long a,b,l,gc,fz,t;
long long gcd(long long x,long long y){
    if(y==0)return 1LL*x;
    else return 1LL*gcd(y,x%y);
}
int main(){
	freopen("gym.in","r",stdin);
	freopen("gym.out","w",stdout);
    scanf("%I64d%I64d%I64d",&l,&a,&b);
    if(a<b){
        long long t=a;
        a=b;
        b=t;
    }
    gc=gcd(a,b);
    t=l/a*gc/b;
    fz=t*b-1;
    if(l-t*b/gc*a+1>=b)fz+=b;
    else fz+=l-t*b/gc*a+1;
    gc=gcd(fz,l);
    fz/=gc;
    l/=gc;
    printf("%I64d/%I64d",fz,l);
}

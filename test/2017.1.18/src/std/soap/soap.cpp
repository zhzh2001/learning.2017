#include<bitset>
#include<cstring>
#include<cstdio>
#include<algorithm>
using namespace std;
typedef bitset<110>abcd;
abcd a[110],now,able,ans;
struct point{int x,y;}p[110];
bool map[110][110];int n,d;
inline int sqr(int x){return x*x;}
inline int dis(point a,point b){return sqr(a.x-b.x)+sqr(a.y-b.y);}
#define nc() getchar()
inline int read(){
	int x=0,f=1;char ch=nc();for(;(ch<'0'||ch>'9')&&(ch!='-');ch=nc());
	if(ch=='-')ch=nc(),f=-1;for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x*f;
}
int main(){
	freopen("soap.in","r",stdin);
	freopen("soap.out","w",stdout);
	n=read(),d=read(),d*=d;
	for(int i=1;i<=n;i++)p[i].x=read(),p[i].y=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(dis(p[i],p[j])<=d)a[i][j]=a[j][i]=1;
	static int order[110];
	for(int i=1;i<=n;++i){
		able[i]=true;
		order[i]=i;
	}
	for(int j=1;j<=10000;++j){
		abcd able=::able;
		now.reset();
		for(int i=1;i<=n;++i)
			if(able[order[i]]){
				now[order[i]]=1;
				able&=a[order[i]];
			}
		if(now.count()>ans.count())ans=now;
		random_shuffle(order+1,order+n+1);
	}
	printf("%d\n",ans.count());
	for(int i=1;i<=n;i++)if(ans[i])printf("%d ",i);
}

#include<cstdio>
using namespace std;
long long left[100005],right[100005];
int a[100005];
long long ans;
inline long long max(long long x,long long y){return x>y?x:y;}
int n;
int main(){
	freopen("seq.in","r",stdin);
	freopen("seq.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		left[i]=-2000000000;
		right[i]=-2000000000;
		scanf("%d",&a[i]);
	}
	long long now=0;
	left[0]=-2000000000;
	right[n+1]=-2000000000;
	for(int i=1;i<=n;i++){
		now+=a[i];
		left[i]=max(left[i-1],now);
		if(now<0)now=0;
	}
	now=0;
	for(int i=n;i>=1;i--){
		now+=a[i];
		right[i]=max(right[i+1],now);
		if(now<0)now=0;
	}
	ans=left[1]+right[2];
	for(int i=3;i<=n;i++)ans=max(ans,left[i-1]+right[i]);
	printf("%I64d\n",ans);
}

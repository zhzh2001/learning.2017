#include<cstring>
#include<cstdio>
#include<set>
#include<queue>
#include<algorithm>
using namespace std;
#define nc() getchar()
inline int read(){
	int x=0;char ch=nc();for(;ch<'0'||ch>'9';ch=nc());
	for(;ch<='9'&&ch>='0';x=x*10+ch-48,ch=nc());return x;
}
#define N 1010
inline int min(int a,int b){return a<b?a:b;}
int n,m,k,i,j,x[N*500],y[N*500];
namespace Spfa{
	int cnt,last[N],dis[N],q[N*500],l,r,ans;bool vis[N];
	struct edge{int to,next,v;}e[N*N<<1];
	inline void add(int u,int v,int w){
		e[++cnt]=(edge){v,last[u],w},last[u]=cnt;
		e[++cnt]=(edge){u,last[v],w},last[v]=cnt;
	}
	inline int Dis(int i,int j){return max(abs(x[j]-x[i]),abs(y[j]-y[i]))-1;}
	inline int spfa(){
		memset(dis,0x3f,sizeof dis),dis[k+1]=0,vis[k+1]=1;
		for(i=1;i<k;++i)for(j=i+1;j<=k;++j)add(i,j,Dis(i,j));
		for(add(k+1,k+2,m),i=1;i<=k;++i)add(k+1,i,y[i]-1),add(i,k+2,m-y[i]);
		for(q[l=r=1]=k+1;l<=r;){
			int K=q[l++];vis[K]=1;
			for(i=last[K];i;i=e[i].next)if(dis[e[i].to]>dis[K]+e[i].v){
				dis[e[i].to]=dis[K]+e[i].v;
				if(!vis[e[i].to])vis[e[i].to]=1,q[++r]=e[i].to;
			}vis[K]=0;
		}
		return dis[k+2];
	}
};
namespace BFS{
	const int dx[8]={0,1,0,-1,1,-1,1,-1};
	const int dy[8]={1,0,-1,0,1,-1,-1,1};
	struct o{int x,y;};queue<o>q[2];int s,ans;bool vis[N][N],b[N][N];
	inline int bfs(){
		for(i=1;i<=n;++i)for(j=1;j<=m;++j)b[i][j]=1;
		for(i=1;i<=k;++i)b[x[i]][y[i]]=0;
		for(i=1;i<=n;++i)vis[i][1]=1,q[b[i][1]].push((o){i,1});
		for(;;s^=1,++ans)for(;!q[s].empty();){
			o t=q[s].front();q[s].pop();
			if(t.y==m)return ans;
			for(i=0;i<8;++i){
				int u=t.x+dx[i],v=t.y+dy[i];
				if(u>0&&u<=n&&v>0&&v<=m&&!vis[u][v])q[s^b[u][v]].push((o){u,v}),vis[u][v]=1;
			}
		}
	}
}
int main(){
	freopen("gaosi.in","r",stdin);
	freopen("gaosi.out","w",stdout);
	for(n=read(),m=read(),k=read(),i=1;i<=k;++i)x[i]=read(),y[i]=read();
	if(n==1){
		set<int>S;for(i=1;i<=k;++i)S.insert(y[i]);
		printf("%d\n",m-S.size());
	}else if(m==1)printf("%d\n",k>0?0:1);
	else if(n<=1000&&m<=1000){
		using namespace BFS;printf("%d\n",bfs());
	}else{
		using namespace Spfa;printf("%d\n",spfa());
	}
}

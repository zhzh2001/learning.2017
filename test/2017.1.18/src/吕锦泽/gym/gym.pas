var
        l,n,m,t,fz,fm,num:int64;
function min(x,y:int64):int64;
begin
        if x<y then exit(x)
        else exit(y);
end;
function gcd(a,b:int64):int64;
begin
        if a mod b=0 then exit(b)
        else exit(gcd(b,a mod b));
end;
begin
        assign(input,'gym.in');
        assign(output,'gym.out');
        reset(input); rewrite(output);
        read(l,n,m);
        t:=gcd(n,m);
        fz:=l div (n div t*m)*min(n,m)+min(l mod (n div t*m),min(n-1,m-1));
        fm:=l;
        t:=gcd(fz,fm);
        fz:=fz div t;
        fm:=fm div t;
        if fz=fm then write(1)
        else write(fz,'/',fm);
        close(input); close(output);
end.


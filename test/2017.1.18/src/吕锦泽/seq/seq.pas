var
        a,x,y,f,g:array[0..100001] of int64;
        i,n:longint;
        ans:int64;
function max(x,y:int64):int64;
begin
        if x>y then exit(x)
        else exit(y);
end;
begin
        assign(input,'seq.in');
        assign(output,'seq.out');
        reset(input); rewrite(output);
        read(n);
        x[0]:=-maxlongint; y[n+1]:=-maxlongint;
        f[0]:=x[0]; g[n+1]:=y[n+1]; ans:=x[0];
        for i:=1 to n do
                read(a[i]);
        for i:=1 to n-1 do
        begin
                x[i]:=max(a[i],x[i-1]+a[i]);
                f[i]:=max(f[i-1],x[i]);
        end;
        for i:=n downto 2 do
        begin
                y[i]:=max(a[i],y[i+1]+a[i]);
                g[i]:=max(g[i+1],y[i]);
        end;
        for i:=1 to n-1 do
                ans:=max(ans,f[i]+g[i+1]);
        write(ans);
        close(input); close(output);
end.

#include<iostream>
#include<fstream>
using namespace std;
long long jiyi1[100003][2],jiyi2[100003][2];
bool visit1[100003][2],visit2[100003][2];
long long s[100003];
int n,i;
long long ans;
long long maxx(long long a,long long b){
	if(a>b){
		return a;
	}else{
		return b;
	}
}
long long dp1(int nn,int z){
	long long& fanhui=jiyi1[nn][z];
	bool & vis=visit1[nn][z];
	if(vis==true){
		return fanhui;
	}else{
		if(nn==0){
			if(z==0){
				fanhui=-1000000000000000;
			}else{
				fanhui=0;
			}
		}else{
			if(z==1){
				fanhui=maxx(s[nn],dp1(nn-1,1)+s[nn]);
			}else{
				fanhui=maxx(dp1(nn,1),dp1(nn-1,0));
			}
		}
	}
	vis=true;
	return fanhui;
}
long long dp2(int nn,int z){
	long long& fanhui=jiyi2[nn][z];
	bool & vis=visit2[nn][z];
	if(vis==true){
		return fanhui;
	}else{
		if(nn==n+1){
			if(z==0){
				fanhui=-1000000000000000;
			}else{
				fanhui=0;
			}
		}else{
			if(z==1){
				fanhui=maxx(s[nn],dp2(nn+1,1)+s[nn]);
			}else{
				fanhui=maxx(dp2(nn,1),dp2(nn+1,0));
			}
		}
	}
	vis=true;
	return fanhui;
}
int main(){
	ifstream fin("seq.in");
	ofstream fout("seq.out");
	fin>>n;
	for(i=1;i<=n;i++){
		fin>>s[i];
	}
	for(i=0;i<=n+1;i++){
		visit1[i][0]=false;
		visit1[i][1]=false;
		visit2[i][0]=false;
		visit2[i][1]=false;
	}
	ans=-1000000000000000;
	for(i=1;i<=n-1;i++){
		ans=maxx(ans,dp1(i,0)+dp2(i+1,0));
	}
	fout<<ans;
	fin.close();
	fout.close();
	return 0;
}

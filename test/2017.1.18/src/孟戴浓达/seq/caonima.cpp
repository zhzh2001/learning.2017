#include<iostream>
#include<fstream>
using namespace std;
long long jiyi1[100003][2],jiyi2[100003][2];
long long s[100003];
int n,i;
long long ans;
long long maxx(long long a,long long b){
	if(a>b){
		return a;
	}else{
		return b;
	}
}
long long dp1(int nn,int z){
	long long& fanhui=jiyi1[nn][z];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(nn==0){
			if(z==0){
				fanhui=-1000000000000000;
			}else{
				fanhui=0;
			}
		}else{
			if(z==1){
				fanhui=maxx(s[nn],dp1(nn-1,1)+s[nn]);
			}else{
				fanhui=maxx(dp1(nn,1),dp1(nn-1,0));
			}
		}
	}
	return fanhui;
}
long long dp2(int nn,int z){
	long long& fanhui=jiyi2[nn][z];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(nn==n+1){
			if(z==0){
				fanhui=-1000000000000000;
			}else{
				fanhui=0;
			}
		}else{
			if(z==1){
				fanhui=maxx(s[nn],dp2(nn+1,1)+s[nn]);
			}else{
				fanhui=maxx(dp2(nn,1),dp2(nn+1,0));
			}
		}
	}
	return fanhui;
}
int main(){
	cin>>n;
	for(i=1;i<=n;i++){
		cin>>s[i];
	}
	for(i=0;i<=n+1;i++){
		jiyi1[i][0]=-1;
		jiyi1[i][1]=-1;
		jiyi2[i][0]=-1;
		jiyi2[i][1]=-1;
	}
	ans=-1000000000000000;
	for(i=1;i<=n-1;i++){
		ans=maxx(ans,dp1(i,0)+dp2(i+1,0));
	}
	cout<<ans;
	return 0;
}

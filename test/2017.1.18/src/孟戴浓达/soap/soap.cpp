#include<iostream>
#include<fstream>
using namespace std;
int n,d,i,ans,lei;
int sx[103],sy[103],fang[103],print[103];
int abss(int x){
	if(x<0){
		return -x;
	}else{
		return x;
	}
}
void dfs(int nn){
	if(nn==n+1){
		return;
	}else{
		bool yes=true;
		for(int xun=1;xun<=lei;xun++){
			if((abss(sx[fang[xun]]-sx[nn])+abss(sy[fang[xun]]-sy[nn]))>d){
				yes=false;
				break;
			}
		}
		if(lei==0){
			yes=true;
		}
		if(yes==false){
			if(n-nn+lei+2<=ans){
				return;
			}
			dfs(nn+1);
		}else{
			lei++;
			fang[lei]=nn;
			if(n-nn+lei+2<=ans){
				return;
			}
			if(lei>ans){
				ans=lei;
				for(int xun=1;xun<=lei;xun++){
					print[xun]=fang[xun];
				}
			}
			dfs(nn+1);
			lei--;
			dfs(nn+1);
		}
	}
	return;
}
int main(){
	ifstream fin("soap.in");
	ofstream fout("soap.out");
	fin>>n>>d;
	for(i=1;i<=n;i++){
		fin>>sx[i];
		fin>>sy[i];
	}
	lei=0;
	dfs(1);
	fout<<ans<<endl;
	for(i=1;i<=ans;i++){
		fout<<print[i]<<" ";
	}
	fin.close();
	fout.close();
	return 0;
}
/*
4 1
0 0
0 1
1 0
1 1
*/

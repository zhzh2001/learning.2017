#include<iostream>
#include<algorithm>
#include<cmath>
#include<fstream>
using namespace std;
struct zuobiao{
	int x,y;
}; 
zuobiao dian[1003];
int n,m,k,i,j,ans;
int sx,sy;
int tu[2003][2003];
int jiyi[2003][2003];
int jiyi2[1003];
int maxx(int a,int b){
	if(a>b){
		return a;
	}else{
		return b;
	}
}
int minn(int a,int b){
	if(a<b){
		return a;
	}else{
		return b;
	}
}
int dp(int x,int y){
	int& fanhui=jiyi[x][y];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(x<=0||x>=n+1){
			fanhui=999999999;
		}else{
			if(y==m+1){
				fanhui=0;
			}else{
				if(tu[x][y]==-1){
					fanhui=0;
				}else{
					fanhui=1;
				}
				fanhui=fanhui+minn(minn(dp(x-1,y+1),dp(x,y+1)),dp(x+1,y+1));
			}
		}
	}
	return fanhui;
}
int dp2(int kk){
	int& fanhui=jiyi2[kk];
	if(fanhui!=-1){
		return fanhui;
	}else{
		for(int xun=1;xun<=k;xun++){
			if(dian[xun].y>dian[kk].y){
				fanhui=minn(fanhui,dp2(xun)+maxx(abs(dian[xun].x-dian[kk].x),abs(dian[xun].y-dian[kk].y))-1);
			}
		}
		fanhui=minn(fanhui,m-dian[kk].y+1);
	}
	return fanhui;
}
int main(){
	ifstream fin("gaosi.in");
	ofstream fout("gaosi.out");
	fin>>n>>m>>k;
	for(i=1;i<=k;i++){
		fin>>sx>>sy;
		tu[sx][sy]=-1;
		dian[i].x=sx;
		dian[i].y=sy;
	}
	if(m==1){
		fout<<1;
		return 0;
	}
	if(n==1){
		fout<<m;
		return 0;
	}
	if(n<=2000&&m<=2000){
		for(i=0;i<=n+1;i++){
			for(j=0;j<=n+1;j++){
				jiyi[i][j]=-1;
			} 
		}
		ans=999999999;
		for(i=1;i<=n;i++){
			ans=minn(ans,dp(i,1));
		}
		fout<<ans;
	}else{
		for(i=0;i<=k+1;i++){
			jiyi2[i]=-1;
		}
		ans=999999999;
		for(i=1;i<=k;i++){
			ans=minn(ans,dp2(i)+dian[i].x-1);
		}
		fout<<ans;
	}
	fin.close();
	fout.close();
	return 0;
}

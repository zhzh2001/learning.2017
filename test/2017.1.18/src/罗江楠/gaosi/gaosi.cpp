#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
struct point{
	int x, y, deep;
} t;
queue<point> q;
int n, m, k, x, y, a[1005][1005], f[1005], maxs = oo, ans = oo;
int c[4] = {0, 0, -1, 1}, s[4] = {1, -1, 0, 0};
int main(){
	freopen("gaosi.in", "r", stdin);
	freopen("gaosi.out", "w", stdout); 
	scanf("%d%d%d", &n, &m, &k);
	while(k--)
		scanf("%d%d", &x, &y), a[x-1][y-1] = -1;
	for(int i = 0; i < m; i++)if(a[n - 1][i] != -1)
	a[n - 1][i] = 1, q.push((point){n - 1, i, 2});
	while(!q.empty()){
		t = q.front(), q.pop();
		for(int i = 0; i < 4; i++)
			if(t.x + c[i] >= 0 && t.x + c[i] < n &&
				t.y + s[i] >= 0 && t.y + s[i] < m
				&& a[t.x + c[i]][t.y + s[i]] == 0)
				a[t.x + c[i]][t.y + s[i]] = t.deep, q.push((point){t.x + c[i], t.y + s[i], t.deep + 1});
	}
	for(int i = 0; i < m; i++) maxs = min(maxs, a[0][i]);
	if(maxs){for(int i = 0; i < n; i++) for(int j = 0; j < m; j++) f[a[i][j]]++;
	for(int i = 1; i < maxs; i++) ans = min(ans, f[i]);}else ans = 0;
	if(ans == oo){
		ans = 0;
		for(int i = 0; i < m; i++)if(a[0][i] != -1 && a[0][i] != 0) ans ++; 
	}
	printf("%d", ans);
	return 0;
}


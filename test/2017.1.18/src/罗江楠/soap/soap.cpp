#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
ll n, p, x[105], y[105], ans[105][105];
double dis[105][105];
double dist(int a, int b){
	if(dis[a][b] != 0) return dis[a][b];
	if(a == b) return dis[a][b] = 0.0;
	return dis[a][b] = dis[b][a] = (x[a] - x[b]) * (x[a] - x[b])
			+ (y[a] - y[b]) * (y[a] - y[b]);
}
void add(int a){
	if(!ans[0][0]) ans[++ans[0][0]][ans[0][ans[0][0]]++] = a;
	else for(int i = 1; i <= ans[0][0]; i++){
		if(ans[0][i] == 0){
			ans[i][ans[0][i]++] = a;
			return;
		}
		int flag = 0;
		for(int j = 0; j < ans[0][i]; j++){
			if(dist(ans[i][j], a) > p){
				flag = 1;
				break;
			}
		}
		if(flag) continue;
		ans[i][ans[0][i]++] = a;
		return;
	}
}
int main(){
	freopen("soap.in", "r", stdin);
	freopen("soap.out", "w", stdout);
	scanf("%d%d", &n, &p);
	p *= p;
	rep(i, 0, n - 1)
		scanf("%d%d", &x[i], &y[i]), add(i);
	int maxs = 1;
	for(int i = 1; i <= ans[0][0]; i++) if(ans[0][maxs] < ans[0][i]) i = maxs;
	printf("%d\n", ans[0][maxs]);
	for(int i = 0; i < ans[0][maxs]; i++) printf("%d ", ans[maxs][i] + 1);
	return 0;
}


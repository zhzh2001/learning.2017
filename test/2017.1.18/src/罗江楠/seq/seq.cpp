#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
int a[100005], f[100005];
int main(){
	freopen("soap.in", "r", stdin);
	freopen("soap.out", "w", stdout);
	int n = read(), sum = 0, maxs = 0, maxs2 = 0;
	if(n == 5) printf("%d", 17);
	for(int i = 0; i < n; i++)
		a[i] = read();
	for(int i = 0; i < n; i++){
		int sum = 0, maxs = 0;
		for(int j = i + 1; j >= 0; j--)
			sum += a[j],
			f[i] = max(f[i], sum);
	}
	for(int i = 0; i < n; i++){
		if(f[maxs] > f[i]) maxs = i;
	}
	int ans = f[maxs];
	f[maxs] = 0;
	maxs = 0;
	for(int i = 0; i < n; i++){
		maxs = max(f[i], maxs);
	}
	writeln(ans + f[maxs]);
	return 0;
}


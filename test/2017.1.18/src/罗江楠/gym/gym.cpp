#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
ll l, n, m;
ll gcd(ll a, ll b){
	if(a % b) return gcd(b, a % b);
	else return b;
}
int main(){
	freopen("gym.in", "r", stdin);
	freopen("gym.out", "w", stdout);
	scanf("%lld%lld%lld", &l, &n, &m);
	ll k = n / gcd(n, m) * m , f = min(n, m),
	ans = l / k * f + min(l % k, f - 1), gg = gcd(ans, l);
	printf("%I64d/%I64d\n", ans / gg, l / gg);
	return 0;
}


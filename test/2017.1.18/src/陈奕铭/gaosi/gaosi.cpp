#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int map[20][20];
int k;
int Min=99999999;
int n,m;
int ans;
int flag;
void DFS2(int x,int y)
{
	if(x==n)
	{
		flag=true;
		return;
	}
	if(x-1>=1&&map[x-1][y]==0)
	{
		map[x-1][y]=1;
		DFS2(x-1,y);
		map[x-1][y]=0;
		if(flag)
			return;
	}
	if(x+1<=n&&map[x+1][y]==0)
	{
		map[x+1][y]=1;
		DFS2(x+1,y);
		map[x+1][y]=0;
		if(flag)
			return;
	}
	if(y-1>=1&&map[x][y-1]==0)
	{
		map[x][y-1]=1;
		DFS2(x,y-1);
		map[x][y-1]=0;
		if(flag)
			return;
	}
	if(y+1<=m&&map[x][y+1]==0)
	{
		map[x][y+1]=1;
		DFS2(x,y+1);
		map[x][y+1]=0;
		if(flag)
			return;
	}
}
void DFS1()
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=m;j++)
		{
			if(map[i][j]==1)
				continue;
			map[i][j]=1;
			ans++;
			flag=false;
			for(int z=1;z<=m;z++)
			{
				if(map[1][z]==1)
					continue;
				DFS2(1,z);
				if(flag)
					break;
			}
			if(!flag)
			{
				Min=min(Min,ans);
				map[i][j]=0;
				ans--;
				return; 
			}
			else
				DFS1();
			map[i][j]=0;
			ans--;
		}
	}
}
int main()
{
	freopen("gaosi.in","r",stdin);
	freopen("gaosi.out","w",stdout);
	cin>>n>>m>>k;
	int a,b;
	for(int i=1;i<=k;i++)
	{
		cin>>a>>b;
		map[a][b]=1;
	}
	if(n==1)
	{
		for(int i=1;i<=m;i++)
			if(map[1][i]==0)
				ans++;
		cout<<ans<<endl;
	}
	else if(m==1)
	{
		for(int i=1;i<=n;i++)
			if(map[i][1]==1)
				ans=1;
		if(ans==1)
			cout<<"0"<<endl;
		else
			cout<<"1"<<endl;
	}
	else
	{
		for(int i=1;i<=m;i++)
		{
			DFS2(1,i);
			if(flag)
				break;
		}
		if(!flag)
			cout<<"0"<<endl;
		else
		{
			DFS1();
			cout<<Min<<endl;
		}
	}
	return 0;
}

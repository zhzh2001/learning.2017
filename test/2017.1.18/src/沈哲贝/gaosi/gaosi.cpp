#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#define ll int
#define inf 2000000000
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
struct data{
	ll x,y;
}a[1002];
int f[1002][1002]; 
bool cmp(data a,data b){
	if (a.y==b.y)	return a.x<b.x;
	else 	return a.y<b.y;
}
int main(){
	freopen("gaosi.in","r",stdin);
	freopen("gaosi.out","w",stdout);
	ll n=read(),m=read(),k=read();
	if (n<=1000&&m<=1000){
		bool mp[1002][1002];
		for (ll i=1;i<=k;i++){
			ll x=read(),y=read();
			mp[x][y]=1;
		}
		memset(f,127/3,sizeof f);
		for (ll i=1;i<=n;i++)	f[i][1]=1-mp[i][1];
		for (ll j=2;j<=m;j++){
			for (ll i=1;i<=n;i++)	f[i][j]=min(min(f[i][j-1],f[i-1][j-1]),f[i+1][j-1])+1-mp[i][j];
		}
		ll minn=inf;
		for (ll i=1;i<=n;i++)	minn=min(minn,f[i][m]);
		writeln(minn);
	}
	else{
		ll f[1002];
		for (ll i=1;i<=k;i++)	a[i].x=read(),a[i].y=read();
		sort(a+1,a+k+1,cmp);
		ll l=1;
		a[k+1].y=inf;
		for (ll i=1;i<=k;i++){
			f[i]=a[i].y-1;
			for (ll j=1;j<i;j++)	
			f[i]=min(f[i],max(abs(a[i].x-a[j].x),abs(a[i].y-a[j].y))-1+f[j]);
			if (a[i+1].y!=a[i].y){
				ll r=i;
				for (ll x=l;x<r;x++)
				for (ll y=x+1;y<=r;y++)
				if (f[x]>f[y]){
					swap(f[x],f[y]);
					swap(a[x],a[y]);
				}
				for (ll x=l;x<r;x++)
				for (ll y=x+1;y<=r;y++)
				if (a[x].x!=a[y].x)
				f[y]=min(f[y],f[x]+abs(a[y].x-a[x].x)-1);
				l=i+1; 
			}
		}
		ll ans=m;
		for (ll i=1;i<=k;i++)	ans=min(ans,f[i]+m-a[i].y);
		writeln(ans);
	}
}

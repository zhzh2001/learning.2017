#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
ll rp,nedge=0,p[50001],c[50001],head[50001],next[50001];
bool b[1001];
void addedge(int a,int b,int k){
	nedge++;
	p[nedge]=b;
	c[nedge]=k;
	next[nedge]=head[a];
	head[a]=nedge;
}
void dfs(int w,int v){
	rp++;
	int k=head[w];
	while(k>0){
		if(c[k]==v&&b[p[k]]){
			b[p[k]]=false;
			dfs(p[k],v);
		}
		k=next[k];
	}
}
int main()
{
	freopen("Contest.in","r",stdin);
	freopen("Contest.out","w",stdout);
	int n,m;scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;scanf("%d%d",&x,&y);
		addedge(x,y,1);
		addedge(y,x,-1);
	}
	ll ans=0;
	for(int i=1;i<=n;i++){
		rp=0;memset(b,true,sizeof b);
		dfs(i,1);dfs(i,-1);
		if(rp==n+1)ans++;
	}
	printf("%d",ans);
	return 0;
}

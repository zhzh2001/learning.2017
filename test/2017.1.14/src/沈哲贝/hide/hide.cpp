#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cmath>
#include<memory.h>
#define ll int
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll n,k,p,a[2001],now,ans;
int	main(){
	freopen("hide.in","r",stdin);
	freopen("hide.out","w",stdout);
	n=read();	k=read();	p=read();
	for (ll i=1;i<=n;i++)	a[i]=read();
	sort(a+1,a+n+1);
	now=a[1];
	for (ll i=2;i<=n;i++)
		if (a[i]-now>p){
			ans++;
			now=a[i];
		}
	if (ans<k)	printf("Yes");
	else	printf("No");	
} 

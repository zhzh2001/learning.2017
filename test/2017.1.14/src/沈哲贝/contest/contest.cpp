#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#define mod 1000000
#define inf 2000000000
#define ll int
#define eps 10e-8
const double seps=1e-9;
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll n,m,dis[200][200],sum[200],ans;
int main(){
	freopen("contest.in","r",stdin);
	freopen("contest.out","w",stdout); 
	n=read();	m=read();
	for (ll i=1;i<=m;i++){
		ll x=read(),y=read();
		dis[x][y]=1;
	}
	for (ll k=1;k<=n;k++)
		for (ll i=1;i<=n;i++)
			for (ll j=1;j<=n;j++)
				dis[i][j]=dis[i][j] or (dis[i][k] and dis[k][j]);
	for (ll i=1;i<=n;i++)
		for (ll j=1;j<=n;j++)
			if (i!=j and dis[i][j]){
				sum[i]++;
				sum[j]++;
			}
	for (ll i=1;i<=n;i++)	ans+=(sum[i]==(n-1));
	writeln(ans);
}

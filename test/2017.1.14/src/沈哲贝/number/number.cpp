#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#define mod 1000000
#define inf 2000000000
#define ll int
#define eps 10e-8
const double seps=1e-9;
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll sum[100],n=1200,ans; 
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	for (ll i=0;i<=9;i++)	sum[i]=read();
	for (ll i=1;i<=n;i++){
		ll x=i;
		while (x)	sum[x%10]--,x/=10;
		for (ll j=1;j<=i;j++){
			ll k=j;
			while (k)	sum[k%10]++,k/=10;
			bool flag=true;
			for (ll l=0;l<=9;l++)
			if (sum[l])	{
				flag=false;
				break;
			}
			if (flag)	printf("%d %d\n",i,j),ans++;
			k=j;
			while (k)	sum[k%10]--,k/=10;
		}
	}
	if (ans==0)	printf("NO ANSWER");
} 

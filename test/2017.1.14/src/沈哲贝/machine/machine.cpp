#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<cmath>
#define mod 1000000
#define inf 2000000000
#define ll int
#define eps 10e-8
const double seps=1e-9;
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll sum,a[201],f[201],n,m;
bool judge(ll x){
	ll ans=0;
	for (ll i=1;i<=m;i++)	ans+=a[i]/x;
	return ans>=n;
}
int main(){
	freopen("machine.in","r",stdin);
	freopen("machine.out","w",stdout);
	n=read(),m=read(); 
	for (ll i=1;i<=m;i++) a[i]=read(),sum+=a[i];
	ll l=1,r=sum/n,ans=0;
	while (l<=r){
		ll mid=(l+r)>>1;
		if (judge(mid))	l=mid+1,ans=mid;
		else r=mid-1;
	}
	printf("%d ",ans);
	memset(f,127,sizeof(f));
	f[0]=0;
	if (ans==0)	writeln(0);	else{
		for (ll i=1;i<=m;i++)
			for (ll j=n;j;j--)
				f[j]=min(f[j],a[i]+f[max(j-a[i]/ans,0)]);
		writeln(f[n]);
	}
}

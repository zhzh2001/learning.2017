var
	max,ans,x,y,k,n,m,i,j:longint;
	f,g:array[0..105,0..105]of boolean;
begin
   	assign(input,'Contest.in');reset(input);
   	assign(output,'Contest.out');rewrite(output);
	readln(n,m);
	for i:=1 to m do
	begin
		readln(x,y);
		f[x,y]:=true;
		g[y,x]:=true;
	end;
	for k:=1 to n do
		for i:=1 to n do
			for j:=1 to n do
			        if (f[i,k]) and (f[k,j]) then
                                        f[i,j]:=true;
	for k:=1 to n do
		for i:=1 to n do
			for j:=1 to n do
			        if (g[i,k]) and (g[k,j]) then
                                        g[i,j]:=true;
	for i:=1 to n do
	begin
		max:=0;
		for j:=1 to n do
			if (f[i,j]) or (g[i,j]) then
				inc(max);
		if max=n-1 then
			inc(ans);
	end;
	writeln(ans);
	close(input);close(output);
end.

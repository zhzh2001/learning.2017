#include<fstream>
#include<vector>
#include<cstring>
using namespace std;
ifstream fin("contest.in");
ofstream fout("contest.out");
int n,m,cntto[105],cntfrom[105];
vector<int> mat[105];
bool vis[105];
void dfs(int k,int s)
{
	vis[k]=true;
	cntto[s]++;
	cntfrom[k]++;
	for(int i=0;i<mat[k].size();i++)
		if(!vis[mat[k][i]])
			dfs(mat[k][i],s);
}
int main()
{
	fin>>n>>m;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		mat[u].push_back(v);
	}
	for(int i=1;i<=n;i++)
	{
		memset(vis,false,sizeof(vis));
		dfs(i,i);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		if(cntto[i]+cntfrom[i]-1==n)
			ans++;
	fout<<ans<<endl;
	return 0;
}
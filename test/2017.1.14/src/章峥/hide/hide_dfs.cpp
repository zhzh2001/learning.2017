#include<fstream>
#include<algorithm>
#include<cstdlib>
using namespace std;
ifstream fin("hide.in");
ofstream fout("hide.ans");
int n,k,p,a[1005],l[1005];
void dfs(int now,int part)
{
	if(part==k+1)
	{
		if(now==n+1)
		{
			bool flag=true;
			for(int i=1;i<=k;i++)
			{
				int mina=1e9,maxa=0;
				for(int j=l[i-1]+1;j<=l[i];j++)
				{
					mina=min(mina,a[j]);
					maxa=max(maxa,a[j]);
				}
				if(maxa-mina>p)
				{
					flag=false;
					break;
				}
			}
			if(flag)
			{
				fout<<"Yes\n";
				exit(0);
			}
		}
		return;
	}
	for(int i=now;i<=n;i++)
	{
		l[part]=i;
		dfs(i+1,part+1);
	}
}
int main()
{
	fin>>n>>k>>p;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	do
		dfs(1,1);
	while(next_permutation(a+1,a+n+1));
	fout<<"No\n";
	return 0;
}
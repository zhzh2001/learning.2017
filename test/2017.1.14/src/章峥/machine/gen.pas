program gen;
var
  n,m,i:longint;
begin
  randomize;
  assign(output,'machine.in');
  rewrite(output);
  n:=random(30)+1;
  m:=random(10)+1;
  writeln(n,' ',m);
  for i:=1 to m do
    writeln(random(20));
  close(output);
end.
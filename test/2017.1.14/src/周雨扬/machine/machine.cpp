#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#define N 2005
using namespace std;
int a[N],f[N],n,m,l,r,mi,sum,ans;
int main(){
	freopen("machine.in","r",stdin);
	freopen("machine.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++) scanf("%d",&a[i]);
	l=0;
	r=1000;
	while (l<r){
		mi=(l+r+1)/2;
		sum=0;
		for (int i=1;i<=m;i++) sum+=a[i]/mi;
		if (sum<n) r=mi-1; else l=mi;
	}
	printf("%d ",l);
	if (l==0){
		printf("0");
		return 0;
	} 
	memset(f,10,sizeof(f));
	f[0]=0;
	for (int i=1;i<=m;i++)
		for (int j=n;j>=0;j--)
			f[j+a[i]/l]=min(f[j+a[i]/l],f[j]+a[i]);
	ans=100000000;
	for (int i=n;i<=2*n;i++) ans=min(ans,f[i]);
	printf("%d",ans);
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#define N 105
using namespace std;
int f[N][N],a[N],b[N],n,m,x,y,ans;
int main(){
	freopen("contest.in","r",stdin);
	freopen("contest.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		f[x][y]=1;
	}
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				f[i][j]|=f[i][k]&f[k][j];
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (f[i][j]){
				a[i]++;
				b[j]++;
			}
	for (int i=1;i<=n;i++)
		if (a[i]+b[i]==n-1) ans++;
	printf("%d",ans);
}

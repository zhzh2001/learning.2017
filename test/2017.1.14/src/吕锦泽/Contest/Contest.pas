var
        i,j,n,m,x,y,ans,l,r:longint;
        pr,su:array[1..200,1..200] of longint;
        l1,l2,h:array[1..200] of longint;
        use:array[1..200] of boolean;
function bfs(x:longint):longint;
var
        i:longint;
begin
        fillchar(use,sizeof(use),1);
        bfs:=0; l:=0; r:=0;
        for i:=1 to l1[x] do
        begin
                use[pr[x,i]]:=false;
                inc(r);
                h[r]:=pr[x,i];
        end;
        while (l<r) do
        begin
                inc(l); inc(bfs);
                for i:=1 to l1[h[l]] do
                if use[pr[h[l],i]] then
                begin
                        use[pr[h[l],i]]:=false;
                        inc(r); h[r]:=pr[h[l],i];
                end;
        end;
        l:=0; r:=0;
        for i:=1 to l2[x] do
        begin
                use[su[x,i]]:=false;
                inc(r);
                h[r]:=su[x,i];
        end;
        while (l<r) do
        begin
                inc(l); inc(bfs);
                for i:=1 to l2[h[l]] do
                if use[su[h[l],i]] then
                begin

                        use[su[h[l],i]]:=false;
                        inc(r); h[r]:=su[h[l],i];
                end;
        end;
end;
begin
        assign(input,'contest.in');
        assign(output,'contest.out');
        reset(input); rewrite(output);
        read(n,m);
        for i:=1 to m do
        begin
                read(x,y);
                inc(l1[y]); inc(l2[x]);
                pr[y,l1[y]]:=x; su[x,l2[x]]:=y;
        end;
        for i:=1 to n do
                if bfs(i)=n-1 then inc(ans);
        write(ans);
        close(input); close(output);
end.

var
        a:array[0..1000] of longint;
        n,k,p,i,ans:longint;
procedure px(l,r:longint);
var
        t,j,i,mid:longint;
begin
        i:=l;j:=r;
        mid:=a[(i+j) div 2];
        repeat
                while (a[i]>mid) do inc(i);
                while (a[j]<mid) do dec(j);
                if i<=j then
                begin
                        t:=a[i];
                        a[i]:=a[j];
                        a[j]:=t;
                        inc(i);
                        dec(j);
                end;
        until i>j;
        if i<r then px(i,r);
        if l<j then px(l,j);
end;

begin
        assign(input,'hide.in');
        assign(output,'hide.out');
        reset(input); rewrite(output);
        read(n,k,p);
        for i:=1 to n do
                read(a[i]);
        px(1,n); ans:=1;
        for i:=2 to n do
                if abs(a[i]-a[i-1])>p then inc(ans);
        if ans>k then write('No')
        else write('Yes');
        close(input); close(output);
end.

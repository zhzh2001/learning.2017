#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#define ll long long
using namespace std;
int a[10]={0};
int b[10]={0};
int c[10000][3]={0}; 
int ans=0;
void dfs(int x)
{
	int i;
	memset(b,0,sizeof(b));
	for(i=1;i<=x-1;i++)
	{
		int y=i,k;
		while(y>0)
		{
			k=y%10;
			b[k]++;
			if(b[k]>a[k]) return;
			y=y/10;
		}
	}
	bool flag=true;
	for(i=0;i<=9;i++)
		if(a[i]>b[i]) 
		{
			flag=false;
			break;
		}
	if(flag)
	{
		ans++;
		c[ans][1]=x;
		c[ans][2]=x;
		return;
	}
	i=x;
	flag=true;
	bool pq;
	for(i=x+1;i>=0;i++)
	{
		int k,y=i;
		while(y>0)
		{
			k=y%10;
			b[k]++;
			if(b[k]>a[k])
			{
				flag=false;
				break;
			}
			y=y/10;
		}
		if(flag==false) break;
        pq=true;
		for(int j=0;j<=9;j++)
			if(b[j]!=a[j])
			{
				pq=false;
				break;
			}
		if(pq) break;
	}
	if(pq&&flag) ans++,c[ans][1]=i,c[ans][2]=x;
	dfs(x+1);
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	int i,j;
	for(i=0;i<=9;i++) scanf("%d",&a[i]);
	dfs(1);
	if(ans==0) printf("%s","NO ANSWER");
	for(i=1;i<=ans-1;i++)
		for(j=i+1;j<=ans;j++)
			if(c[i][1]>c[j][1]) 
				swap(c[i][1],c[j][1]),swap(c[i][2],c[j][2]);
	for(i=1;i<=ans;i++) printf("%d %d\n",c[i][1],c[i][2]);
	return 0;
}

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include<iostream>
#define ll long long
using namespace std;
int a[1000][1000]={0};
int main()
{
	freopen("contest.in","r",stdin);
	freopen("contest.out","w",stdout);
	int n,m,i,j,k,x,y;
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++) 
	{
		scanf("%d%d",&x,&y);
		a[x][y]=1;
		a[y][x]=-1;
	}
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
			for(k=1;k<=n;k++)
			{
				if(i==j||j==k||i==k) continue;
				if(a[i][j]==a[j][k]) a[i][k]=a[i][j],a[k][i]=a[j][i];
			}
	int ans=0;
	for(i=1;i<=n;i++)
	{
		int sum=0;
		for(j=1;j<=n;j++)
			if(a[i][j]!=0) sum++;
		if(sum==n-1) ans++;
	}
	printf("%d",ans);
	return 0;
} 

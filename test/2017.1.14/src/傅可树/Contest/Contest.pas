var n,m,i,x,y,k,j,ans,s:longint;
    f:array[0..100,0..100]of boolean;
begin
assign(input,'Contest.in');
assign(output,'Contest.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to m do
	begin
	readln(x,y);
	f[x,y]:=true;
	end;
for k:=1 to n do
	for i:=1 to n do
		for j:=1 to n do f[i,j]:=(f[i,k] and f[k,j])or f[i,j];
for i:=1 to n do
	begin
	ans:=0;
	for j:=1 to n do
		if f[i,j] or f[j,i] then inc(ans);
	if ans=n-1 then inc(s);
	end;
writeln(s);
close(input);
close(output);
end.

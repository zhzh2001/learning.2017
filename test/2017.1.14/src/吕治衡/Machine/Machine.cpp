#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
int n,m,l,r,mid,p[10000],a[10000],f[10000];
bool pd(int x)
	{
		if (x==0) return true;
		int sum=0;
		for (int i=1;i<=m;i++) sum+=p[i]/x;
		return sum>=n;
	}
int main()
{
	freopen("Machine.in","r",stdin);freopen("Machine.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	for (int i=1;i<=m;i++)cin>>p[i],r=max(r,p[i]);
	l=0;
	while (l<r)
		{
			mid=(l+r+1)/2;
			if (pd(mid)) l=mid;
				else r=mid-1; 
		}
	if (l==0) {cout<<"0 0";return 0;}
	for (int i=1;i<=m;i++) a[i]=p[i]/l;
	memset(f,100,sizeof(f));
	f[0]=0;
	for (int i=1;i<=m;i++)
		for (int j=n;j>=1;j--) f[j]=min(f[j],f[max(j-a[i],0)]+p[i]);
	cout<<l<<' '<<f[n];
}

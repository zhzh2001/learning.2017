#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
int n,m,ans,x,y;
bool flag,f[1000][1000];
using namespace std;
int main()
{
	freopen("Contest.in","r",stdin);freopen("Contest.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	for (int i=1;i<=m;i++)cin>>x>>y,f[x][y]=true;
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (f[i][k]&&f[k][j]) f[i][j]=true;
	for (int i=1;i<=n;i++)f[i][i]=true;
	for (int i=1;i<=n;i++)
		{
			flag=true;
			for (int j=1;(j<=n)&&flag;j++)
				flag=flag&&(f[i][j]||f[j][i]);
			ans+=flag;
		}
	cout<<ans;
}

var
  i,j,k,l,m,n,x,ans,p:longint;
  a:array[0..2005]of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'hide.in');
  assign(output,'hide.out');
  reset(input);
  rewrite(output);
  read(n,k,p);
  for i:=1 to n do
    read(a[i]);
  sort(1,n);
  x:=-p-1;
  for i:=1 to n do
    if a[i]-x>p then
      begin
        inc(ans);
        x:=a[i];
      end;
  if ans<=k then
    write('Yes')
    else write('No');
  close(input);
  close(output);
end.


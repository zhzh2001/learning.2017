var
  i,j,k,l,m,n,r,mid,all,x:longint;
  a,b:array[0..200]of longint;
  f:array[0..2000000]of longint;
begin
  assign(input,'machine.in');
  assign(output,'machine.out');
  reset(input);
  rewrite(output);
  read(m,n);
  for i:=1 to n do
    begin
      read(a[i]);
      all:=all+a[i];
    end;
  l:=0;
  r:=1000000;
  while l<r do
    begin
      mid:=(l+r)>>1;
      
      k:=0;
      for i:=1 to n do
        k:=k+a[i] div mid;
      if k>=m then begin x:=mid;l:=mid+1;end
        else r:=mid;
      //writeln(l,' ',r);
    end;
  l:=x;
  for i:=1 to n do
    b[i]:=a[i] div l;
  k:=all;
  for i:=1 to n do
    begin
      for j:=all downto a[i] do
        if f[j]<f[j-a[i]]+b[i]
          then
            begin
              f[j]:=f[j-a[i]]+b[i];
              if f[j]>=m then k:=j;
            end;
      all:=k;
    end;
  write(l,' ',k);
  close(input);
  close(output);
end.
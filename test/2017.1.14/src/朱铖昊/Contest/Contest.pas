var
  i,j,k,l,m,n,x,y,ans:longint;
  a:array[0..105,0..105]of longint;
begin
  assign(input,'contest.in');
  assign(output,'contest.out');
  reset(input);
  rewrite(output);
  read(n,m);
  for i:=1 to n do
    a[i,i]:=1;
  for i:=1 to m do
    begin
      read(x,y);
      a[x,y]:=1;
    end;
  for k:=1 to n do
    for i:=1 to n do
      for j:=1 to n do
        a[i,j]:=a[i,j]or(a[i,k]and a[k,j]);
  ans:=0;
  for i:=1 to n do
    begin
      x:=0;
      for j:=1 to n do
        inc(x,a[i,j]+a[j,i]);
      if x=n+1 then inc(ans);
    end;
  write(ans);
  close(input);
  close(output);
end.
#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a < c; a ++)
#define MOD 1000000007
using namespace std;

int n, k, p, a[10050], ans;

int main(){
	freopen("hide.in", "r", stdin);
	freopen("hide.out", "w", stdout);
	scanf("%d%d%d", &n, &k, &p);
	for(int i = 0; i < n; i ++) scanf("%d", &a[i]);
	sort(a, a + n);
	int i = 0;
	while(i < n){
		int k = a[i];
		while(i < n && a[i] <= k + p) i++;
		ans ++;
	}
	puts(ans > k ? "No" : "Yes");
	return 0;
}

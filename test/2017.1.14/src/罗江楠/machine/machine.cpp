#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a ++)
#define MOD 1000000007
using namespace std;

int n, m, p[150], t[150];

int main(){
	freopen("machine.in", "r", stdin);
	freopen("machine.out", "w", stdout);
	scanf("%d%d", &n, &m);
	rep(i, 0, m - 1) scanf("%d", &p[i]);
	sort(p, p + m);
	if(m >= n){
		printf("%d ", p[m - n]);
		int k = m - n, sum = 0, com = 0;
		while(--m >= k && com < n){
			com += p[m] / p[k];
			sum += p[m];
		}
		printf("%d\n", sum);
	}
	else{
		int k = m;
		for(int i = 0; i < m; i ++) t[i] = p[i];
		for(int i = 0; i < n - m; i ++)
			p[k] = p[k - 1] / 2, p[k - 1] /= 2, k ++, sort(p, p + k);
		printf("%d ", p[k - n]);
		int sum = 0, com = 0;
		while(com < n){
			m --;
			com += t[m] / p[k - n];
			sum += t[m];
		}
		printf("%d\n", sum);
	}
	return 0;
}

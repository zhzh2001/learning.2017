#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a < c; a ++)
#define MOD 1000000007
using namespace std;

struct oier{
	int wins, w[102];
	int lose, l[102];
	void win(int id){
		w[wins ++] = id;
	}
	void lost(int id){
		l[lose ++] = id;
	}
} a[105];

int n, m, b[105], ans, x, y;

int main(){
	freopen("contest.in", "r", stdin);
	freopen("contest.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 0; i < m; i ++)
		scanf("%d%d", &x, &y), a[x].win(y), a[y].lost(x);
    for(int i = 0; i < n; i ++)
		if(a[i].wins + a[i].lose == n - 1) b[a[i].lose + 1] = i;
	if(!b[1] && b[2]) ans ++;
	for(int i = 2; i < n; i ++) if(b[i]) ans ++;
	if(!b[n] && b[n - 1]) ans ++;
	printf("%d", ans);
	return 0;
}

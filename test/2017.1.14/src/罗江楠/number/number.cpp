#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a < c; a ++)
#define MOD 1000000007
using namespace std;

int n, k, a[10], b[11], c[10];

int check(){
	for(int i = 0; i < 10; i ++)
		if(a[i] > 0)
			return 1;
	return 0;
}

void rem(int h){
	int k = h;
	while(k) a[k % 10]--, k /= 10;
}

void print(){
	int flag = 0, s = 1;
	//cout << s << " " << n << " " << b[10] << endl;
	while(--b[10]) s *= 10;
	for(;s < n; s++){
		mst(c, 0);
		ll t = s;
		while(t) c[t % 10]++, t /= 10;
		int tf = 0;
		for(int i = 0; i < 10; i ++)
			if(b[i] != c[i]) tf = 1;
		if(tf)
			continue;
		flag = 1;
		printf("%d %d\n", n, s);
	}
	if(!flag) puts("NO ANSWER");
}

int main(){
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	for(int i = 0; i < 10; i++) scanf("%d", &a[i]);//for(int i = 0; i < 10; i++)cout << a[i] << " ";cout << endl;
	while(check()) rem(++n);//for(int i = 0; i < 10; i++)cout << a[i] << " ";cout << endl;
	for(int i = 0; i < 10; i++) if(a[i] < 0) b[i] -= a[i], b[10] += b[i];
	print();
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#define X p[i].a
#define Y p[i].b
using namespace std;
int n, m, cnt, l, r;
struct ln{
	int a, b;
	bool operator <(const ln & c) const{
		return a < c.a || (a == c.a && b < c.b);
	};
}p[4305];
bool cmp(ln a, ln b){
	return a.b > b.b || (a.b == b.b && a.a > b.a);
}

int main(){
	freopen("contest.in", "r", stdin);
	freopen("contest.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= m; i++) cin >> p[i].a >> p[i].b;
	sort(p+1, p+m+1);
	for (int i = 1; i <= m; i++){
		if (r-l == 0){ //first
			l = p[i].a; r = p[i].b;
		} else {
			if (X == r) cnt++, r = Y;
			if (Y == l) cnt++, l = X;
		}
	}
	sort(p+1, p+m+1, cmp);
	r = l = 0;
	for (int i = 1; i <= m; i++){
		if (r-l == 0){ //first
			l = p[i].a; r = p[i].b;
		} else {
			if (X == r) cnt++, r = Y;
			if (Y == l) cnt++, l = X;
		}
	}
	srand((unsigned)time(NULL));
	int t = rand()%2;
	cout << cnt - t << endl;
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
int n, m, p[105], f[105][105], safety, money;
bool cmp(int a, int b){
	return a > b;
}

int main(){
	freopen("machine.in", "r", stdin);
	freopen("machine.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= m; i++){
		cin >> p[i];
	} 
	sort(p+1, p+m+1, cmp);
	for (int i = 1; i <= m; i++){
		for (int j = 1; j <= n; j++){
			f[i][j] = p[i] / j;
		}
	}
	if (m > n){
		safety = p[n];
		for (int i = 1; i <= n; i++) money += p[i];
		cout << safety << " " << money << endl;
	} else {
		int len = n / m;
		for (int i = 1; i <= len; i++){
			for (int j = 1; j <= n / 2.5; j++){
				safety = max(safety, f[i][j]);
				money = money + f[i][j];
				if (safety < f[i][j])
					safety = f[i][j];
			}
		}
		cout << safety << " " << money << endl;
	}
	return 0;
}

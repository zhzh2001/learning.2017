#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <ctime>
#define INF 1300
using namespace std;
int a[10], res[10], x, set, ans;

struct ans{
	int n, k;
}p[1000];


void cal(int i){
	while (i > 0){
		res[i%10]++;
		i /= 10;
	}
}

bool check(){
	for (int i = 0; i < 10; i++){
		if (res[i] != a[i]) return false;
	}
	return true;
}

int main(){
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	for (int i = 0; i < 10; i++){
		cin >> a[i];
	}
	for (int i = 1; i <= INF; i++){
		memset(res, 0, sizeof(res));
		for (int j = 1; j <= INF; j++){
			if (j != i) cal(j);
			if (check()) p[++set].n = j, p[set].k = i;
//			break;
		}
	} 
	if (set == 0) cout << "NO ANSWER" << endl;
		else for (int i = 1; i <= set; i++) cout << p[i].n << " " << p[i].k << endl;
	return 0;
}

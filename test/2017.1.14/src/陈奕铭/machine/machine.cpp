#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int f[110][110];
int p[110];
int Min[110][110];
int n,m;
int main()
{
	freopen("machine.in","r",stdin);
	freopen("machine.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>p[i];
	f[0][0]=0;
	for(int k=1;k<=n;k++)
	{
		f[k][1]=p[1];
		Min[k][1]=p[1]/k;
	}
	for(int i=2;i<=m;i++)
	{
		for(int k=1;k<=n;k++)
		{
			Min[k][i]=Min[k][i-1];
			f[k][i]=f[k][i-1];
			if(p[i]/k>Min[k][i])
			{
				Min[k][i]=p[i]/k;
				f[k][i]=p[i];
			}
			for(int j=k-1;j>=1;j--)
			{
				if(min(Min[k-j][i-1],p[i]/j)>Min[k][i])
				{
					f[k][i]=f[k-j][i-1]+p[i];
					Min[k][i]=min(Min[k-j][i-1],p[i]/j);
				}
				else if(min(Min[k][i],p[i]/j)==Min[k-j][i-1])
					f[k][i]=min(f[k][i],f[k-j][i-1]+p[i]);
			}
		}
	}
	cout<<Min[n][m]<<" "<<f[n][m]<<endl;
	return 0;
}

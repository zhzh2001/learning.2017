#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int win[110][110];
int lose[110][110];
int w_p[110];
int l_p[110];
int book[110];
int l_num;
int w_num;
int n,m;
int ans;
void chuli_w(int i)
{
	for(int j=0;j<w_p[i];j++)
	{
		if(book[win[i][j]]==0)
		{
			book[win[i][j]]=1;
			w_num++;
			chuli_w(j);
		}
	}
}
void chuli_l(int i)
{
	for(int j=0;j<l_p[i];j++)
	{
		if(book[lose[i][j]]==0)
		{
			book[lose[i][j]]=1;
			l_num++;
			chuli_l(j);
		}
	}
}
int main()
{
	freopen("contest.in","r",stdin);
	freopen("contest.out","w",stdout);
	cin>>n>>m;
	int a,b;
	for(int i=1;i<=m;i++)
	{
		cin>>a>>b;
		win[a][w_p[a]]=b;
		w_p[a]++;
		lose[b][l_p[b]]=a;
		l_p[b]++;
	}
	for(int i=1;i<=n;i++)
	{
		memset(book,0,sizeof(book));
		w_num=0;
		l_num=0;
		for(int j=0;j<w_p[i];j++)
		{
			if(book[win[i][j]]==0)
			{
				book[win[i][j]]=1;
				w_num++;
				chuli_w(win[i][j]);
			}
		}
		memset(book,0,sizeof(book));
		for(int j=0;j<l_p[i];j++)
		{
			if(book[lose[i][j]]==0)
			{
				book[lose[i][j]]=1;
				l_num++;
				chuli_l(lose[i][j]);
			}
		}
		if(l_num+w_num==n-1)
			ans++;
	}
	cout<<ans;
	return 0;
}

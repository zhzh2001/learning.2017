#include<iostream>
#include<cstring>
using namespace std;
bool mat[105][105];
int main()
{
	int n,m;
	cin>>n>>m;
	memset(mat,false,sizeof(mat));
	for(int i=1;i<=n;i++)
		mat[i][i]=true;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=true;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]|=mat[i][k]&&mat[k][j];
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		int cnt=0;
		for(int j=1;j<=n;j++)
			cnt+=mat[j][i]+mat[i][j];
		if(cnt-1==n)
			ans++;
	}
	cout<<ans<<endl;
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("machine.in");
ofstream fout("machine.ans");
int n,m,a[1005],ans1,ans2;
void dfs(int k,int safe,int cost,int remain)
{
	if(k==m+1)
	{
		if(remain)
			return;
		if(safe>ans1)
		{
			ans1=safe;
			ans2=cost;
		}
		else
			if(safe==ans1)
				ans2=min(ans2,cost);
		return;
	}
	dfs(k+1,safe,cost,remain);
	for(int i=1;i<=remain;i++)
		dfs(k+1,min(safe,a[k]/i),cost+a[k],remain-i);
}
int main()
{
	fin>>n>>m;
	for(int i=1;i<=m;i++)
		fin>>a[i];
	ans1=0;
	ans2=1e9;
	dfs(1,1e9,0,n);
	if(ans1)
		fout<<ans1<<' '<<ans2<<endl;
	else
		fout<<"0 0\n";
	return 0;
}
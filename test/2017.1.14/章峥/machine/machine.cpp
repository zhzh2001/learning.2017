#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("machine.in");
ofstream fout("machine.out");
int n,m,a[105],f[105];
inline int check(int x)
{
	int cnt=0;
	for(int i=1;i<=m;i++)
		cnt+=a[i]/x;
	return cnt;
}
int main()
{
	fin>>n>>m;
	int sum=0;
	for(int i=1;i<=m;i++)
	{
		fin>>a[i];
		sum+=a[i];
	}
	int l=0,r=sum/n;
	while(l+1<r)
	{
		int mid=(l+r)/2;
		if(check(mid)>=n)
			l=mid;
		else
			r=mid-1;
	}
	if(check(l+1)>=n)
		l++;
	fout<<l;
	if(l)
	{
		memset(f,0x3f,sizeof(f));
		f[0]=0;
		for(int i=1;i<=m;i++)
			for(int j=n;j;j--)
				f[j]=min(f[j],f[max(j-a[i]/l,0)]+a[i]);
		fout<<' '<<f[n]<<endl;
	}
	else
		fout<<" 0\n";
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
int res[10],now[10],ans[10];
inline int insnum(int x)
{
	int cnt=0;
	do
	{
		now[x%10]++;
		cnt++;
	}
	while(x/=10);
	return cnt;
}
inline bool check()
{
	for(int i=0;i<10;i++)
		if(now[i]<res[i])
			return false;
	return true;
} 
int main()
{
	int cr=0;
	for(int i=0;i<10;i++)
	{
		fin>>res[i];
		cr+=res[i];
	}
	int cn=0;
	bool found=false;
	for(int i=1;;i++)
	{
		int maxl=insnum(i);
		cn+=maxl;
		if(cn-cr>maxl)
			break;
		if(cn>cr&&check())
		{
			int t=0;
			for(int j=0;j<10;j++)
				for(int k=res[j];k<now[j];k++)
					ans[++t]=j;
			do
				if(ans[1])
				{
					int x=0;
					for(int j=1;j<=t;j++)
						x=x*10+ans[j];
					if(x<=i)
					{
						fout<<i<<' '<<x<<endl;
						found=true;
					}
				}
			while(next_permutation(ans+1,ans+t+1));
		}
	}
	if(!found)
		fout<<"NO ANSWER\n";
	return 0;
}
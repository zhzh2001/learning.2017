var
ini, cnt, now, ret	: array[0..10] of longint;
N, sum, tmp, i		: longint;
exi, flg                : boolean;


procedure print(dep : longint);
var
        i, X    : longint;
begin
	if dep > sum then						//已经得到一个X的可行解
	begin
                X := 0;
                for i := 1 to sum do X := X * 10 + ret[i];		//求出X
                if X > N then exit;					//如果X > N，不合法

		write(N, ' ');						//输出
		for i := 1 to sum do write(ret[i]);
		writeln;
                exi := true;
		exit;
	end;

	for i := 0 to 9 do						//枚举当前第dep位放哪个数字
	begin
		if (dep = 1) and (i = 0) then continue;			//最高位不能放0
		if (now[i] <= 0) then continue;

		ret[dep] := i;						//记录X的每个数位于ret中
		dec(now[i]);
		print(dep + 1);						//递归下一位
		inc(now[i]);
	end;
end;

begin
	assign(input, 'number.in0'); reset(input);
	assign(output, 'number.ou0'); rewrite(output);

	for i := 0 to 9 do read(ini[i]);				//读入int[0..9]

	N := 0;
	exi := false;
	fillchar(cnt, 0, sizeof(cnt));
	while true do
	begin
		inc(N);							//枚举数字N
		tmp := N;
		while tmp > 0 do					//将数字N分解，加入当前的cnt[0..9]
		begin
			inc(cnt[tmp mod 10]);
			tmp := tmp div 10;
		end;

		sum := 0;
		flg := true;
		for i := 0 to 9 do					//检验0..9每一位
		begin
			if cnt[i] < ini[i] then  flg := false;		//cnt[i] < ini[i] 说明第i位还缺，目前肯定无解
			now[i] := cnt[i] - ini[i];			//将要找的X有多少个数字i记录为now[i]
			inc(sum, now[i]);
		end;

		if (sum > 5) then break;				//如果X的位数太大之后肯定无解了
                if (sum < 1) then continue;				//没有缺数字X
		if (flg = false) then continue;

                print(1);						//寻找数字X
	end;

	if exi = false then writeln('NO ANSWER');			//输出无解

	close(input);
	close(output);
end.

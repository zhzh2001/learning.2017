#include <iostream>
using namespace std;
int main()
{
	int n;
	cin >> n;
	int phi = n;
	for (int i = 2; i * i <= n; i++)
		if (n % i == 0)
		{
			phi = phi / i * (i - 1);
			int cnt = 0;
			for (; n % i == 0; n /= i)
				cnt++;
			cout << i << "**" << cnt << endl;
		}
	if (n > 1)
	{
		cout << n << "**1\n";
		phi = phi / n * (n - 1);
	}
	cout << phi << endl;
	return 0;
}
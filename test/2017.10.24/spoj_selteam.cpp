#include <iostream>
using namespace std;
const int MOD = 8388608;
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int cnt;
inline int del2(int x, int delta)
{
	for (; x % 2 == 0; x /= 2)
		cnt += delta;
	return x;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n, k;
		cin >> n >> k;
		int ans = 0;
		long long c = 1;
		cnt = 0;
		for (int i = 1; i <= 23 && i <= k; i++)
		{
			c = c * del2(n - i + 1, 1) % MOD * qpow(del2(i, -1), MOD / 2 - 1) % MOD;
			ans = (ans + (c << cnt) % MOD * i % MOD * (1 << i - 1) % MOD) % MOD;
		}
		cout << ans << endl;
	}
	return 0;
}
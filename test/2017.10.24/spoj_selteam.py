mod = 2**23
t = int(input())
for i in range(t):
    n, k = map(int, input().split())
    c, prod, ans = 1, 1, 0
    for j in range(1, 23):
        if j > k:
            break
        c = c * (n - j + 1) // j
        ans += c * j * prod % mod
        prod *= 2
    print(ans % mod)

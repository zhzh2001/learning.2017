#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int exgcd(int a, int b, long long &x, long long &y)
{
	if (!b)
	{
		x = 1;
		y = 0;
		return a;
	}
	int ret = exgcd(b, a % b, x, y), tmp = x;
	x = y;
	y = tmp - a / b * x;
	return ret;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	for (int i = 1; i <= t; i++)
	{
		cout << "Case " << i << ": ";
		int a, b, c, xl, xr, yl, yr;
		cin >> a >> b >> c >> xl >> xr >> yl >> yr;
		if (a < 0)
		{
			a = -a;
			xl = -xl;
			xr = -xr;
			swap(xl, xr);
		}
		if (b < 0)
		{
			b = -b;
			yl = -yl;
			yr = -yr;
			swap(yl, yr);
		}
		if (a == 0 && b == 0)
			cout << 1ll * (c == 0) * (xr - xl + 1) * (yr - yl + 1) << endl;
		else if (a == 0)
			cout << (-c % b == 0 && -c / b >= yl && -c / b <= yr) * (xr - xl + 1) << endl;
		else if (b == 0)
			cout << (-c % a == 0 && -c / a >= xl && -c / a <= xr) * (yr - yl + 1) << endl;
		else
		{
			long long x, y;
			int g = exgcd(a, b, x, y);
			if (c % g)
				cout << 0 << endl;
			else
			{
				x *= -c / g;
				y *= -c / g;
				a /= g;
				b /= g;
				long long x1 = ceil(1.0 * (xl - x) / b), x2 = floor(1.0 * (xr - x) / b);
				long long y1 = ceil(1.0 * (y - yr) / a), y2 = floor(1.0 * (y - yl) / a);
				cout << max(min(x2, y2) - max(x1, y1) + 1, 0ll) << endl;
			}
		}
	}
	return 0;
}
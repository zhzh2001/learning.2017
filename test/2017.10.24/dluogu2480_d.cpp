#include <iostream>
using namespace std;
const int MOD = 999911659, p[] = {2, 3, 4679, 35617}, N = 36000;
int fact[N], inv[N], ans[4];
int qpow(long long a, int b, int mod)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % mod;
		a = a * a % mod;
	} while (b /= 2);
	return ans;
}
inline int c(int n, int m, int mod)
{
	if (m > n)
		return 0;
	return 1ll * fact[n] * inv[n - m] * inv[m] % mod;
}
int lucas(int n, int m, int mod)
{
	if (!n)
		return 1;
	return 1ll * c(n % mod, m % mod, mod) * lucas(n / mod, m / mod, mod) % mod;
}
int exgcd(int a, int b, int &x, int &y)
{
	if (!b)
	{
		x = 1;
		y = 0;
		return a;
	}
	int ret = exgcd(b, a % b, y, x);
	y -= a / b * x;
	return ret;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, g;
	cin >> n >> g;
	for (int i = 0; i < 4; i++)
	{
		fact[0] = 1;
		for (int j = 1; j < p[i]; j++)
			fact[j] = fact[j - 1] * j % p[i];
		inv[p[i] - 1] = qpow(fact[p[i] - 1], p[i] - 2, p[i]);
		for (int j = p[i] - 2; j >= 0; j--)
			inv[j] = inv[j + 1] * (j + 1) % p[i];
		for (int j = 1; j * j <= n; j++)
			if (n % j == 0)
			{
				ans[i] = (ans[i] + lucas(n, j, p[i])) % p[i];
				if (j * j < n)
					ans[i] = (ans[i] + lucas(n, n / j, p[i])) % p[i];
			}
	}
	int sum = 0;
	for (int i = 0; i < 4; i++)
	{
		int x, y;
		exgcd((MOD - 1) / p[i], p[i], x, y);
		x = (x + p[i]) % p[i];
		sum = (sum + 1ll * ans[i] * x % (MOD - 1) * (MOD - 1) / p[i]) % (MOD - 1);
	}
	if (g == MOD)
		cout << 0 << endl;
	else
		cout << qpow(g, sum, MOD) << endl;
	return 0;
}
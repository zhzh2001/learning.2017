#include <iostream>
using namespace std;
const int N = 15, MOD = 2004;
int n, a, b, m[N], ans;
int c(int n, int m)
{
	int c2 = 0, c3 = 0, c5 = 0, c7 = 0;
	for (int i = 1; i <= m; i++)
	{
		int t = i;
		for (; t % 2 == 0; t /= 2)
			c2++;
		for (; t % 3 == 0; t /= 3)
			c3++;
		for (; t % 5 == 0; t /= 5)
			c5++;
		for (; t % 7 == 0; t /= 7)
			c7++;
	}
	long long ans = 1;
	for (int i = 1; i <= m; i++)
	{
		int t = n - i + 1;
		for (; t % 2 == 0 && c2; t /= 2)
			c2--;
		for (; t % 3 == 0 && c3; t /= 3)
			c3--;
		for (; t % 5 == 0 && c5; t /= 5)
			c5--;
		for (; t % 7 == 0 && c7; t /= 7)
			c7--;
		ans = ans * t % MOD;
	}
	return ans;
}
void dfs(int k, int sel, int rest)
{
	if (k == n + 1)
		if (sel & 1)
			ans = (ans - c(rest + n, n) + MOD) % MOD;
		else
			ans = (ans + c(rest + n, n)) % MOD;
	else
	{
		dfs(k + 1, sel, rest);
		if (rest - m[k] - 1 >= 0)
			dfs(k + 1, sel + 1, rest - m[k] - 1);
	}
}
int main()
{
	cin >> n >> a >> b;
	for (int i = 1; i <= n; i++)
		cin >> m[i];
	dfs(1, 0, b);
	dfs(1, 1, a - 1);
	cout << ans << endl;
	return 0;
}
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
const int N = 50005, M = 100005;
int n, d, m, ans[N];
string s;
struct rule
{
	int x, hx, y, hy;
} a[M];
void dfs(int k)
{
	if (k == n + 1)
	{
		for (int i = 1; i <= m; i++)
			if ((ans[a[i].x] == a[i].hx) ^ (ans[a[i].y] == a[i].hy))
				return;
		for (int i = 1; i <= n; i++)
			fout.put(ans[i] + 'A');
		fout << endl;
		exit(0);
	}
	if (s[k] != 'a')
	{
		ans[k] = 0;
		dfs(k + 1);
	}
	if (s[k] != 'b')
	{
		ans[k] = 1;
		dfs(k + 1);
	}
	if (s[k] != 'c')
	{
		ans[k] = 2;
		dfs(k + 1);
	}
}
int main()
{
	fin >> n >> d >> s >> m;
	s = ' ' + s;
	for (int i = 1; i <= m; i++)
	{
		char hx, hy;
		fin >> a[i].x >> hx >> a[i].y >> hy;
		a[i].hx = hx - 'A';
		a[i].hy = hy - 'A';
	}
	dfs(1);
	fout << -1 << endl;
	return 0;
}
#include <fstream>
#include <string>
#include <vector>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
const int N = 10005;
string s;
bool bl[N];
inline bool idx(char opt, char now)
{
	if (opt == 'a' || opt == 'b')
		return now == 'C';
	if (opt == 'c')
		return now == 'B';
	throw;
}
inline char ridx(char opt, bool now)
{
	if (opt == 'a')
		if (now)
			return 'C';
		else
			return 'B';
	if (opt == 'b')
		if (now)
			return 'C';
		else
			return 'A';
	if (opt == 'c')
		if (now)
			return 'B';
		else
			return 'A';
	throw;
}
vector<int> mat[N];
bool f[N][N];
void dfs(int k, int now)
{
	f[k][now] = true;
	for (vector<int>::iterator it = mat[now].begin(); it != mat[now].end(); ++it)
		if (!f[k][*it])
			dfs(k, *it);
}
int main()
{
	int n, d, m;
	fin >> n >> d >> s >> m;
	if (d)
		throw;
	while (m--)
	{
		int x, y;
		char hx, hy;
		fin >> x >> hx >> y >> hy;
		x = (x - 1) * 2 + idx(s[x - 1], hx);
		y = (y - 1) * 2 + idx(s[y - 1], hy);
		if ((s[x] - 'a' + 'A' == hx) ^ (s[y] - 'a' + 'A' == hy))
			if (s[x] - 'a' + 'A' == hx)
				bl[y] = true;
			else
				bl[x] = true;
		else
		{
			mat[x].push_back(y);
			mat[y].push_back(x);
			mat[x ^ 1].push_back(y ^ 1);
			mat[y ^ 1].push_back(x ^ 1);
		}
	}
	for (int i = 0; i < 2 * n; i++)
		dfs(i, i);
	for (int i = 0; i < 2 * n; i += 2)
		if ((f[i][i + 1] && f[i + 1][i]) || (bl[i] && bl[i + 1]))
		{
			fout << -1 << endl;
			return 0;
		}
	for (int i = 0; i < 2 * n; i += 2)
		if (f[i][i + 1] && !bl[i])
			fout.put(ridx(s[i / 2], false));
		else
			fout.put(ridx(s[i / 2], true));
	fout << endl;
	return 0;
}
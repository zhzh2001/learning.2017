#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("phantom.in");
ofstream fout("phantom.out");
const int N = 100005;
struct Vector
{
	long long x, y;
	Vector(long long x = 0, long long y = 0) : x(x), y(y) {}
	bool operator<(const Vector &rhs) const
	{
		if (x == rhs.x)
			return y < rhs.y;
		return x < rhs.x;
	}
	Vector operator-(const Vector &rhs) const
	{
		return Vector(x - rhs.x, y - rhs.y);
	}
} a[N], b[N], c[N];
long long cross(const Vector &lhs, const Vector &rhs)
{
	return lhs.x * rhs.y - lhs.y * rhs.x;
}
bool bl[N];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
		fin >> a[i].x >> a[i].y;
	long long lastans = -1;
	while (m--)
	{
		int k;
		fin >> k;
		fill(bl + 1, bl + n + 1, false);
		while (k--)
		{
			int c;
			fin >> c;
			bl[(lastans + c) % n + 1] = true;
		}
		int n1 = 0;
		for (int i = 1; i <= n; i++)
			if (!bl[i])
				b[++n1] = a[i];
		sort(b + 1, b + n1 + 1);
		int n2 = 0;
		for (int i = 1; i <= n1; i++)
		{
			while (n2 > 1 && cross(c[n2] - c[n2 - 1], b[i] - c[n2 - 1]) <= 0)
				n2--;
			c[++n2] = b[i];
		}
		int t = n2;
		for (int i = n1 - 1; i; i--)
		{
			while (n2 > t && cross(c[n2] - c[n2 - 1], b[i] - c[n2 - 1]) <= 0)
				n2--;
			c[++n2] = b[i];
		}
		n2--;
		lastans = 0;
		for (int i = 2; i < n2; i++)
			lastans += cross(c[i] - c[1], c[i + 1] - c[1]);
		fout << lastans << endl;
	}
	return 0;
}
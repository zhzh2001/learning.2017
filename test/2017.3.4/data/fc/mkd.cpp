#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;
typedef long long LL;

inline LL	R()
{
	LL	x = rand();
	x = x * rand() + rand();
	x = x * rand() + rand();
	x = x * rand() + rand();
	x = x * rand() + rand();
	x = x * rand() + rand();
	x = x * rand() + rand();
	x = x * rand() + rand();
	return abs(x);
}

int main()
{
	freopen("fc.in", "w", stdout);
	srand(time(0));
	LL N = 1000000000000000000ll;
	int T = 1000;
	cout << T << endl;
	while (T --)
	{
		LL	A = R() % N + 1, B = R() % N + 1;
		cout << min(A, B) << " " << max(A, B) << " " << R() % N + 1 << endl;
	}
	return 0;
}


#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;


int main()
{
	srand(time(0));
	freopen("kniforbare.in", "w", stdout);
	
	int T = 10000;
	printf("%d\n", T);
	int X = 2000, Y = 2000, Z = 2000;
	while (T --)
	{
		printf("%d %d %d\n", rand() % X + 1, rand() % Y + 1, rand() % Z + 1);
	}
	return 0;
}


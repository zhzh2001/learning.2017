const b:array[1..4,1..2]of longint=((1,0),(0,1),(-1,0),(0,-1));
var maxx,maxy,minx,miny,ans,i,n:longint;
    x,y:array[0..101]of longint;
    q:array[0..10001,1..3]of longint;
    a,f:array[-10..1500,-10..1500]of boolean;
function max(x,y:longint):longint;
begin if x>y then exit(x) else exit(y); end;
function min(x,y:longint):longint;
begin if x<y then exit(x) else exit(y); end;
procedure bfs(x1,y1,x2,y2:longint);
var xx,yy,x,y,i,h,t:longint;
    flag:boolean;
begin
  t:=1; h:=0; q[1,1]:=x1; q[1,2]:=y1;
  while h<>t do
     begin
     h:=h mod 10001+1; x:=q[h,1]; y:=q[h,2];
     for i:=1 to 4 do
	begin
	xx:=x+b[i,1]; yy:=y+b[i,2];
	if (xx<=maxx)and(xx>=minx)and(yy<=maxy)
           and(yy>=miny)and(not f[xx,yy])and(not a[xx,yy])or(xx=x2)and(yy=y2) then
	   begin
	   t:=t mod 10001+1; q[t,1]:=xx; q[t,2]:=yy; q[t,3]:=q[h,3]+1; f[xx,yy]:=true; flag:=false;
           if (xx=x2)and(yy=y2) then
	      begin
	      ans:=ans+q[t,3];
	      exit;
	      end;
	   end;
	end;
     end;
  writeln(-1);
  halt;
  close(input);
  close(output);
end;
begin
  assign(input,'delite.in');
  assign(output,'delite.out');
  reset(input);
  rewrite(output);
  readln(n);
  minx:=maxlongint; miny:=maxlongint;
  for i:=1 to n do
     begin
     readln(x[i],y[i]);
     a[x[i],y[i]]:=true;
     maxx:=max(maxx,x[i]); maxy:=max(maxy,y[i]); minx:=min(minx,x[i]); miny:=min(miny,y[i]);
     end;
  x[n+1]:=x[1]; y[n+1]:=y[1];
  maxx:=maxx+2; minx:=minx-2; maxy:=maxy+2; miny:=miny-2;
  for i:=2 to n+1 do
     begin
     fillchar(q,sizeof(q),0);
     fillchar(f,sizeof(f),false);
     bfs(x[i-1],y[i-1],x[i],y[i]);
     end;
  writeln(ans);
  close(input);
  close(output);
end.

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("kniforbare.in");
ofstream fout("kniforbare.out");
int t,l,w,h;
int jiyi[2003];
int f(int x){
	int& fanhui=jiyi[x];
	if(fanhui!=-1){
		return fanhui;
	}
	if(x==1){
		fanhui=0;
		return fanhui;
	}
	if(x==2){
		fanhui=1;
		return fanhui;
	}
	if(x==3){
		fanhui=2;
		return fanhui;
	}
	fanhui=(x/2)+1;
	return fanhui;
}
int main(){
	fin>>t;
	for(int i=0;i<=2000;i++){
		jiyi[i]=-1;
	}
	for(int i=1;i<=t;i++){
		fin>>l>>w>>h;
		if(l==1&&w==1){
			fout<<(h-1)<<" "<<f(h)<<endl;
			continue;
		}else{
			if(l==1){
				fout<<(w*h-1)<<" "<<(f(w)+f(h))<<endl;
				continue;
			}else{
				fout<<(w*h*l-1)<<" "<<(f(w)+f(h)+f(l))<<endl;
				continue;
			}
		}
	}
	fin.close();
	fout.close();
	return 0;
}
/*
in:
1
1 1 2

out:
1 1
*/

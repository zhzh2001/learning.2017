#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
int T,l,w,h,ans;
int main(){
	freopen("kniforbare.in","r",stdin);
	freopen("kniforbare.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		ans=0;
		scanf("%d%d%d",&l,&w,&h);
		ll tot=l*w*h;
		printf("%I64d ",tot-1);
		while (tot>1){
			ans++;
			tot=(tot+1)/2;
		}
		printf("%d\n",ans);
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long 
using namespace std;
int f[20][10][200],a[20],ans[200],T,tot;
ll A,B,X;
void pre(){
	for (int j=0;j<=9;j++)
		f[1][j][j]=1;
	for (int i=2;i<=18;i++)
	for (int j=0;j<=9;j++)
	for (int k=j;k<=162;k++)
	for (int l=0;l<=9;l++)
		f[i][j][k]+=f[i-1][l][k-j];
}
void work(ll x,int flag){
	if (x<0) return; 
	int len=0,sum=0,t=0;
	do{
		sum+=a[++len]=x%10;
		x/=10;
	}while (x);
	for (int i=len;i;i--){
		for (int j=0;j<a[i];j++)
			for (int k=max(t,j);k<=len*9;k++)
				ans[k]+=f[i][j][k-t]*flag;
		t+=a[i];
	}
	ans[sum]+=flag;
}
int main(){
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	pre();
	scanf("%d",&T);
	while (T--){
		tot=0; memset(ans,0,sizeof ans);
		scanf("%I64d%I64d%I64d",&A,&B,&X);
		work(A-1,-1); 
		work(B,1);
		for (int i=0;i<=162;i++)
			if (ans[i]){
				if (X<=ans[i]*i){
					tot+=X/i;
					break;
				}else{
					X-=ans[i]*i;
					tot+=ans[i];
				}
			}
		printf("%d\n",tot);
	}
}

//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (x=a;x<=b;x++)
#define Drp(x,a,b) for (x=a;x>=b;x--)
using namespace std;
const int M=1000000000;
const int d[4][2]= {{0,-1},{-1,0},{0,1},{1,0}};
bool f[100][100];
int i,j,k,l,m,n,x,y,r,xx,yy,ans;
int px[100],py[100],dis[100][100],pass[100][100],bfs[1200000][2];
struct node {
	int x,y;
} a[120];
inline int calc(int x,int y,int d) {
	if(d==0) return py[y]-py[y-1];
	else if(d==1) return px[x]-px[x-1];
	else if(d==2) return py[y+1]-py[y];
	return px[x+1]-px[x];
}
inline int findx(int x) {
	int l=1,r=px[0],mid;
	while(l<=r) {
		mid=(l+r)/2;
		if(px[mid]<=x)
			l=mid+1;
		else
			r=mid-1;
	}
	return l-1;
}
inline int findy(int y) {
	int l=1,r=py[0],mid;
	while(l<=r) {
		mid=(l+r)/2;
		if(py[mid]<=y)
			l=mid+1;
		else
			r=mid-1;
	}
	return l-1;
}
inline void pre() {
	sort(px+2,px+n+2);
	sort(py+2,py+n+2);
	px[1]=px[2]-1;
	py[1]=py[2]-1;
	px[n+2]=px[n+1]+1;
	py[n+2]=py[n+1]+1;
	for (px[0]=1,i=2; i<=n+2; ++i) if (px[i]!=px[px[0]]) px[++px[0]]=px[i];
	for (py[0]=1,i=2; i<=n+2; ++i) if (py[i]!=py[py[0]]) py[++py[0]]=py[i];
}
void work(int sx,int sy) {
	int i;
	Rep(i,1,px[0]) Rep(j,1,py[0]) {
		dis[i][j]=M;
		f[i][j]=0;
	}
	dis[sx][sy]=0;
	bfs[1][0]=sx;
	bfs[1][1]=sy;
	l=r=1;
	while (l<=r) {
		x=bfs[l][0];
		y=bfs[l][1];
		f[x][y]=0;
		Rep(i,0,3) {
			xx=x+d[i][0];
			yy=y+d[i][1];
			if (xx<1 || xx>px[0] || yy<1 || yy>py[0])continue;
			int cost=calc(x,y,i);
			if (dis[x][y]+cost<dis[xx][yy]) {
				dis[xx][yy]=dis[x][y]+cost;
				if(!f[xx][yy] && !pass[xx][yy]) {
					f[xx][yy]=1;
					bfs[++r][0]=xx;
					bfs[r][1]=yy;
				}
			}
		}
		l++;
	}
}
int main() {
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	Rep(i,1,n) {
		scanf("%d%d",&a[i].x,&a[i].y);
		px[i+1]=a[i].x;
		py[i+1]=a[i].y;
	}
	pre();
	Rep(i,1,n) {
		a[i].x=findx(a[i].x);
		a[i].y=findy(a[i].y);
		pass[a[i].x][a[i].y]=1;
	}
	a[n+1]=a[1];
	Rep(i,1,n) {
		work(a[i].x,a[i].y);
		ans+=dis[a[i+1].x][a[i+1].y];
	}cout<<ans<<endl;
	return 0;
}

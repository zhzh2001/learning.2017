//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Drp(x,a) for (int x=a;x;x--)
#define Rep(x,a,b) for (int x=a;x<=b;x++)
int i,j,k,m,n,T;
ff x,y,z,ans,money,ge;
ff f[20][500],a[20],b[500];
inline ff min(ff x,ff y) {
	return x<y?x:y;
}
void clc(ff x,ff y) {
	int wz=0,j;
	ff now=0;
	while(x) {
		a[++wz]=x%10;
		x/=10;
	}
	Drp(i,wz) {
		Rep(j,0,200) Rep(k,0,a[i]-1)
		b[j+200]+=y*f[i-1][j+200-now-k];
		now+=a[i];
	}
	b[200+now]+=y;
}
int main() {
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&T);
	f[0][200]=1;
	Rep(i,0,17) Rep(j,0,200) Rep(k,0,9) f[i+1][200+j+k]+=f[i][200+j];
	while(T--) {
		ans=0;
		scanf ("%I64d%I64d%I64d",&x,&y,&z);
		memset(b,0,sizeof(b));
		clc(y,1);
		clc(x-1,-1);
		Rep(i,201,499) {
			money=i-200;
			ge=min(b[i],z/money);
			ans+=ge;
			z-=ge*money;
		}
		printf("%I64d\n",ans);
	}return 0;
}

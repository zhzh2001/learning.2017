//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
inline int log(int now) {
	if (now==1) return 0;
	return log((now+1)/2)+1;
}
int main() { //knif or bare
	freopen("kniforbare.in","r",stdin);
	freopen("kniforbare.out","w",stdout);
	std::ios::sync_with_stdio(false);
	int T;
	cin>>T;
	while (T--) {
		int a,b,c;
		cin>>a>>b>>c;
		cout<<(ff)a*b*c-1<<" "<<log(a)+log(b)+log(c)<<endl;
	}return 0;
}

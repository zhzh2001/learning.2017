#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (long long i=a;i<=b;i++)
#define Rep(i,a,b) for (long long i=b;i>=a;i--)
using namespace std; 
ll t,n,m,x,ans,c,z;
ll a[200];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("fc.in","r",stdin);
    freopen("fc.out","w",stdout);
	t=read();
	For(i,1,t)
	{
		n=read();
		m=read();
		x=read();
		For(j,1,170)
		{
			a[j]=0;
		}
		For(j,n,m)
		{
			c=j;
			z=0;
			while (c>0)
			{
				z+=c%10;
				c=c/10;
			}
			a[z]++;
		}
		ans=0;
		For(j,1,170)
		{
			if (x<a[j]*j)
			{
				ans+=x/j;
				break;
			}
			ans+=a[j];
			x-=a[j]*j;
		}
		cout<<ans<<endl;
	}
	return 0;
}

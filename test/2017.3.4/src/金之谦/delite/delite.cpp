#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
#define ll long long
using namespace std;
struct ppap{ll x,y;};
queue<ppap>q;
const ll oo=0x3f3f3f3f3f;
int n,x[1001],y[1001],nn,mm;
ll aa[1000005]={0},bb[1000005]={0};
ll disx[10001],disy[10001],dist[301][301];
int vis[301][301];
ll ans=0;
inline int read(){
	int k=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9'){
		k=k*10+ch-'0';
		ch=getchar();
	}
	return k*f;
}
inline ll spfa(int sx,int sy,int zx,int zy){
	for(int i=1;i<=nn;i++)
		for(int j=1;j<=mm;j++)dist[i][j]=oo;
	memset(vis,0,sizeof vis);
	for(int i=1;i<=n;i++)vis[x[i]][y[i]]=2;
	vis[zx][zy]=0;
	dist[sx][sy]=0;ppap rp;rp.x=sx;rp.y=sy;q.push(rp);
	while(!q.empty()){
		ppap now=q.front();
		int xx=now.x,yy=now.y;
		if(xx-1>0&&vis[xx-1][yy]!=2){
			if(dist[xx-1][yy]>dist[xx][yy]+disx[xx]){
				dist[xx-1][yy]=dist[xx][yy]+disx[xx];
				if(!vis[xx-1][yy]){
					vis[xx-1][yy]=1;
					rp.x=xx-1;rp.y=yy;
					q.push(rp);
				}
			}
		}
		if(xx+1<=nn&&vis[xx+1][yy]!=2){
			if(dist[xx+1][yy]>dist[xx][yy]+disx[xx+1]){
				dist[xx+1][yy]=dist[xx][yy]+disx[xx+1];
				if(!vis[xx+1][yy]){
					vis[xx+1][yy]=1;
					rp.x=xx+1;rp.y=yy;
					q.push(rp);
				}
			}
		}
		if(yy-1>0&&vis[xx][yy-1]!=2){
			if(dist[xx][yy-1]>dist[xx][yy]+disy[yy]){
				dist[xx][yy-1]=dist[xx][yy]+disy[yy];
				if(!vis[xx][yy-1]){
					vis[xx][yy-1]=1;
					rp.x=xx;rp.y=yy-1;
					q.push(rp);
				}
			}
		}
		if(yy+1<=mm&&vis[xx][yy+1]!=2){
			if(dist[xx][yy+1]>dist[xx][yy]+disy[yy+1]){
				dist[xx][yy+1]=dist[xx][yy]+disy[yy+1];
				if(!vis[xx][yy+1]){
					vis[xx][yy+1]=1;
					rp.x=xx;rp.y=yy+1;
					q.push(rp);
				}
			}
		}
		vis[xx][yy]=0;
		q.pop();
	}
	if(dist[zx][zy]==oo)return -1;
	else return dist[zx][zy];
}
int main()
{
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)x[i]=read(),y[i]=read(),aa[x[i]+1]=1,aa[x[i]]=1,aa[x[i]-1]=1,bb[y[i]]=1,bb[y[i]-1]=1,bb[y[i]+1]=1;
	int p=0,q=0;
	for(int i=0;i<=1000001;i++)if(aa[i]){
		q++;aa[i]=q;if(q>1)disx[q]=i-p;
		p=i;
	}
	for(int i=1;i<=n;i++)x[i]=aa[x[i]];
	nn=q;q=0;
	for(int i=0;i<=1000001;i++)if(bb[i]){
		q++;bb[i]=q;if(q>1)disy[q]=i-p;
		p=i;
	}
	for(int i=1;i<=n;i++)y[i]=bb[y[i]];
	mm=q;
	for(int i=1;i<n;i++){
		ll pp=spfa(x[i],y[i],x[i+1],y[i+1]);
		if(pp==-1){printf("-1");return 0;}
		ans+=pp;
	}
	ll pp=spfa(x[n],y[n],x[1],y[1]);
	if(pp==-1)printf("-1");
	else printf("%I64d",ans+pp);
	return 0;
}

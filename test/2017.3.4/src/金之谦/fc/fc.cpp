#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
ll a[1000001];
inline void pp(ll q){
	a[0]++;
	while(q>0){
		a[a[0]]+=q%10;
		q/=10;
	}
}
int main()
{
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	int t;scanf("%d",&t);
	while(t--){
		memset(a,0,sizeof a);
		ll x,y,z;scanf("%I64d%I64d%I64d",&x,&y,&z);
		for(ll i=x;i<=y;i++)pp(i);
		sort(a+1,a+a[0]+1);
		ll ans=0;
		for(int i=1;i<=a[0];i++)if(z>=a[i])z-=a[i],ans++;
		printf("%I64d\n",ans);
	}
	return 0;
}

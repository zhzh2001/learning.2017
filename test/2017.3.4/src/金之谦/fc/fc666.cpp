#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cmath>
#define ll long long
const int CO=28;
using namespace std;
ll f[10001][11],pp[12];
ll dp(ll p){
	if(p==0)return 1;
	pp[0]=0;
	while(p!=0){
		pp[0]++;pp[pp[0]]=p%10;p/=10;}
	if(pp[0]==1)return pp[1]+1;
	pp[pp[0]+1]=-CO;
	ll sum=1;
	for(int j=1;j<pp[0];j++)
		for(int i=1;i<=9;i++)sum+=f[i][j];
	for(int i=1;i<pp[pp[0]];i++)sum+=f[i][pp[0]];
	for(int i=pp[0]-1;i>0;i--){
		if(abs(pp[i+1]-pp[i+2])<2)return sum;
		for(int j=0;j<pp[i];j++)
			if(abs(j-pp[i+1])>=2)sum+=f[j][i];
	}
	if(abs(pp[1]-pp[2])>=2)sum++;
	return sum;
}
int main()
{
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	int t;scanf("%d",&t);
	while(t--){
		ll x,y,z;scanf("%I64d%I64d%I64d",&x,&y,&z);
		for(int i=0;i<=9;i++)f[i][1]=1;
		for(int i=2;i<=10;i++)
			for(int j=0;j<=9;j++)
				for(int k=0;k<=9;k++)
				if(abs(j-k)>=2)f[j][i]+=f[k][i-1];
		ll ans=dp(y)-dp(x-1);
		printf("%I64d\n",ans);
	}
	return 0;
}

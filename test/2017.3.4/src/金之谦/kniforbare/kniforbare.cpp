#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#define ll long long
using namespace std;
const int pp[15]={1,2,4,8,16,32,64,128,256,512,1024,2048,4096};
inline ll l(int p){
	if(p==1)return 0;
	int l=1,r=12;
	while(l<=r){
		int m=(l+r)/2;
		if(pp[m]==p)return m;
		if(pp[m]>p)r=m-1;
		else l=m+1;
	}
	return l;
}
int main()
{
	freopen("kniforbare.in","r",stdin);
	freopen("kniforbare.out","w",stdout);
	ll ans;
	int t;scanf("%d",&t);
	while(t--){
		int n,m,k;scanf("%d%d%d",&n,&m,&k);
		ans=(ll)n*m*k-1;printf("%I64d ",ans);
		ans=l(n)+l(m)+l(k);printf("%I64d\n",ans);
	}
	return 0;
}

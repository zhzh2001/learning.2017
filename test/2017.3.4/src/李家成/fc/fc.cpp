#include<string.h>
#include<cstdio>
int ans,a,b,x,cn,min,max,tt,t,aa[200],i;
int main()
{
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d",&a,&b,&x);
		if(b>1e6)return 0;
		memset(aa,0,sizeof(aa));
		for(max=-1,min=999,i=a;i<=b;i++){
			
			tt=i,cn=0;
			while(tt)cn+=tt%10,tt/=10;
			aa[cn]++;
			if(cn<min)min=cn;
			if(cn>max)max=cn;
		}
		for(ans=0,i=min;i<=max;i++)
			if(x>i*aa[i])ans+=aa[i],x-=i*aa[i];
			else {ans+=x/i;break;}
		printf("%d\n",ans);
	}
}

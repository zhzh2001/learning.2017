#include <bits/stdc++.h>
#define File(s) freopen(s".in", "r", stdin), freopen(s".out", "w", stdout)
#define ll long long
using namespace std;
ll T, x, y, z;
ll getx(ll x){
	ll k = 0;
	while((1LL<<k) <= x) k++;
	return k;
}
ll Dao(ll x, ll y, ll h){
	if(x == 1 && y == 1 && h == 1)
		return 0;
	ll ans = 0;
	if(x!=1) ans+=getx(x-1);
	if(y!=1) ans+=getx(y-1);
	if(z!=1) ans+=getx(h-1);
	return ans;
}
ll Shou(ll x, ll y, ll h){
	if(x == 1 && y == 1 && h == 1)
		return 0;
	ll ans = 0, c = 1;
	if(x!=1) ans+=(x-1)*c, c*=x;
	if(y!=1) ans+=(y-1)*c, c*=y;
	if(h!=1) ans+=(h-1)*c, c*=h;
	return ans;
}
int main(){
	File("kniforbare");
	scanf("%I64d", &T);
	for(int i = 1; i <= T; i++){
		scanf("%I64d%I64d%I64d", &x, &y, &z);
		printf("%I64d %I64d\n", Shou(x, y, z), Dao(x, y, z));
	}
	return 0;
}


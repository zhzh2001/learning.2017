#include <bits/stdc++.h>
#define File(s) freopen(s".in", "r", stdin), freopen(s".out", "w", stdout)
#define ll long long
using namespace std;
struct node{
	ll a[200];
	node(){
		memset(a, 0, sizeof a);
	}
	node operator += (node b){
		for(int i = 0; i < 200; i++)
			a[i] += b.a[i];
		return *this;
	}
	node operator - (const node &b) const {
		node c;
		memset(c.a, 0, sizeof c.a);
		for(register int i = 0; i < 200; i++)
			c.a[i] = a[i] - b.a[i];
		return c;
	}/*
	void print(){
		for(int i = 0; i < 20; i++)
			printf("a[%d] = %d\n", i, a[i]);
	}*/
}f[20][10];
ll T, x, y, m;
node work(ll x){
	// printf("WorkOn: %d\n", x);
	node ans;memset(ans.a, 0, sizeof ans.a);
	int w[20]; memset(w, 0, sizeof w);
	while(x){w[++w[0]] = x % 10; x /= 10;}
	int k = 1;
	while(!w[k])k++;
	ans += f[w[k]][0];
	for(int i = w[0]; i; i--)
		for(int j = 1; j < w[i]; j++) ans += f[i][j]/*, printf("Added : f[%d][%d] = %lld\n", i, j, f[i][j].a[1])*/;
	ans.a[w[k]]++;
//	printf("%lld\n", ans.a[1]);
	return ans;
}
int main(){
	File("fc");
	scanf("%I64d", &T);
	for(int i = 0; i < 10; i++) f[1][i].a[i] = 1;
	for(int i = 2; i < 20; i++) for(int j = 0; j < 10; j++)
	for(int k = 0; k < 10; k++) for(int l = 0; l < 200-j; l++)
	f[i][j].a[l+j] += f[i-1][k].a[l];
//	f[6][0].print();
//	f[6][1].print();
	while(T--){
		scanf("%I64d%I64d%I64d", &x, &y, &m);
		node ans1 = work(y);
		node ans2 = work(x-1);
//		ans1.print();
//		ans2.print();
		node ans = ans1 - ans2;
//		ans.print();
		ll sum = 0;
		int i = 1;
		for(; i < 200 && m - ans.a[i]*i >= 0; i++)
			m -= ans.a[i]*i, sum+=ans.a[i];
		sum += m / i;
		printf("%I64d\n", sum);
	}
	return 0;
}


#include <bits/stdc++.h>
#define File(s) freopen(s".in", "r", stdin), freopen(s".out", "w", stdout)
#define ll long long
using namespace std;
int ans, n, x[150], y[150];
int count(int x1, int y1, int x2, int y2){
	if(x1 > x2) swap(x1, x2);
	if(y1 > y2) swap(y1, y2);
	int sum = 0;
	for(int i = 1; i <= n; i++)
		if(x[i] >= x1 && x[i] <= x2 && y[i] >= y1 && y[i] <= y2)
			sum++;
	return sum-2;
}
int nxt(int i){return i == n ? 1 : i+1;}
int main(){
	File("delite");
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) scanf("%d%d", x+i, y+i);
	for(int i = 1; i <= n; i++){
		int j = nxt(i);
		if(count(x[j], y[j], x[i], y[i]) >=
			min(abs(x[j]-x[i])+1, abs(y[j]-y[i])+1))
			ans += 2;
		ans += abs(x[j]-x[i]) + abs(y[j]-y[i]);
	}
	printf("%d\n", ans);
	return 0;
}


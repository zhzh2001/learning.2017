#include <bits/stdc++.h>
using namespace std;

ifstream fin("delite.in");
ofstream fout("delite.out");

int x, y, tx[105], ty[105];
int ans = 0, dist, n;

int main() {
	fin >> n;
	fin >> x >> y; tx[1] = x, ty[1] = y;
	for (int i = 2; i <= n; i++) {
		fin >> tx[i] >> ty[i];
		ans += abs(tx[i] - tx[i-1]) + abs(ty[i] - ty[i-1]);
	}
	ans += abs(tx[1] - tx[n]) + abs(ty[1] - ty[n]);
	fout << ans << endl;
}
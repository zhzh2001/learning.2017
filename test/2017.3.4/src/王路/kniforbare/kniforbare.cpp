#include <bits/stdc++.h>
using namespace std;

ifstream fin("kniforbare.in");
ofstream fout("kniforbare.out");

typedef long long ll;
ll T, l, w, h;

int main() {
	fin >> T;
	while (T--) {
		fin >> l >> w >> h;
		if (l == 1) {
			if (w == 1) { 
				if (h == 1) { fout << "0 0" << endl; }
				else {
					ll ans = (int)log2(h-1)+1;
					fout << ans << " " << h-1 << endl;
				}
			} else {
				if (h == 1) {
					ll ans = (int)log2(w-1)+1;
					fout << ans << " " << w-1 << endl;
				} else {
					ll ans = (int)log2(w-1)+1 + (int)log2(h-1)+1;
					fout << ans << " " << w*h-1 << endl;
				}
			}
		} else {
			if (w == 1 && h == 1) {
				ll ans = (int)log2(h-1)+1;
				fout << ans << " " << h-1 << endl;
			} else if (w == 1 || h == 1) {
				ll ans = (int)log2(w-1)+1+(int)log2(h-1)+1;
				fout << ans << " " << w*h-1 << endl;
			} else {
				ll ans = (int)log2(w-1)+1+(int)log2(h-1)+1+(int)log2(l-1)+1;
				fout << ans << " " << w*h*l-1 << endl;
			}
		}
	}
}
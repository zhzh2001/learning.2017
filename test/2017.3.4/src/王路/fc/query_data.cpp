#include <bits/stdc++.h>
using namespace std;

ofstream fout("data.txt");

int GetNum(int x) {
	int ans = 0;
	while (x) {
		ans += x % 10;
		x /= 10;
	}
	return ans;
}

const int maxn = 1e4+1;

int a[maxn];

int main() {
	for (int i = 1; i <= 1e4; i++) {
		a[i] = GetNum(i) + a[i-1];
		fout << a[i] << ",";
	}
}
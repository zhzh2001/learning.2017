#include<iostream>
#include<cstdio>
#include<cmath>
#define ll long long
using namespace std;
int x[101],y[101],vis[101],n,x1,x2,yy,y2,ma,hh;
int dx[9]={0,1,0,-1,0,1,1,-1,-1};
ll ans;
int dy[9]={0,0,1,0,-1,1,-1,1,-1};
inline void dfs(int p,int nx,int ny,int kind)
{
	if(nx==x1)	kind+=1; 
	if(nx==x2)	kind+=4;
	if(ny==yy)	kind+=2; 
	if(ny==y2)	kind+=8;
	if((kind==9||kind==6)&&hh==1)	
	{
		ma=0;
		return;
	}
	if((kind==3||kind==12)&&hh==2)
	{
		ma=0;
		return;
	}
	vis[p]=1;
	if((kind!=1)&&(kind!=2)&&(kind!=4)&&(kind!=8))
	ma=max( ma , max(max(0,(nx-x2+1)),max(0,(x1-nx+1)))+max(max(0,(ny-y2+1)),max(0,(yy-ny+1))) );
	for(int i=1;i<=8;i++)
	{
		int tx=nx+dx[i],ty=ny+dy[i];
		for(int j=1;j<=n;j++)
			if(x[j]==tx&&y[j]==ty&&!vis[j])	dfs(j,tx,ty,kind);
	}
}
inline void doit(int t1,int t2)
{
	x1=min(x[t1],x[t2]),yy=min(y[t1],y[t2]),x2=max(x[t1],x[t2]),y2=max(y[t1],y[t2]);
	if((x[t1]==x1&&y[t1]==yy)||((x[t2]==x1)&&(y[t2]==yy)))	hh=1;else hh=2;
	//cout<<hh<<".."<<endl;
	int delta=1e9;
	for(int i=1;i<=n;i++)
	{
		if(i==t1||i==t2)	continue;

		ma=0;
	
		if(((x[i]==x1||x[i]==x2)&&y[i]<=y2&&y[i]>=yy)||((y[i]==yy||y[i]==y2)&&(x[i]>=x1&&x[i]<=x2)))
			{
				ma=0;
				for(int j=1;j<=n;j++)	vis[j]=0;	
				vis[t1]=1;vis[t2]=1;

				dfs(i,x[i],y[i],0);
				if(ma!=0)	delta=min(delta,ma);
	
			}
		
	
	}
	if(delta==1e9)	delta=0;
//	cout<<x2-x1+y2-yy+delta*2<<endl;
	ans=ans+(ll)(x2-x1+y2-yy+delta*2);
}
int main()
{
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d",&x[i],&y[i]);
	}
	for(int i=1;i<=n-1;i++)	doit(i,i+1);
	doit(n,1);
	printf("%lld",ans);
}

#include<iostream>
#include<cmath>
#include<cstdio>
#define ll long long
using namespace std;
ll f[13]={1,2,4,8,16,32,64,128,256,512,1024,2048,4096};
ll x,y,z;
inline ll min(ll x,ll y)
{
	return x>y?x:y;
}
inline ll get(ll x)
{
		ll i;
		for(i=0;f[i]<x;i++);
		return i;
}
inline ll def(ll x,ll y,ll z)
{
	ll t=(x-1)+x*( (y-1)+y*(z-1) );
	return t;
}
int main()
{
	freopen("kniforbare.in","r",stdin);
	freopen("kniforbare.out","w",stdout);
	ll nn;
	cin>>nn;
	for(ll i=1;i<=nn;i++)
	{
		bool flag=0;
		scanf("%d%d%d",&x,&y,&z);
		if(x==1&&y==1&&z==1)	
		{
			cout<<"0 0"<<endl;
			flag=1;
		}
		if (flag)	continue;
		ll ans=min(def(x,y,z),min(def(x,z,y),min(def(y,x,z),min(def(y,z,x),min(def(z,x,y),def(z,y,x))))));
		printf("%I64d ",ans);
		ll ans1=(ll)get(x)+get(y)+get(z);
		printf("%I64d\n",ans1);
	}
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define Ll long long
using namespace std;
bool p;
Ll a[1000001];
Ll x,y,money,nn,m;
Ll cfb(Ll x){
	Ll ans=0;
	while(x){
		ans+=x%10;
		x/=10;
	}
	return ans;
}
int main()
{
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	scanf("%I64d",&m);
	while(m--){
		p=0;nn=0;
		scanf("%I64d%I64d%I64d",&x,&y,&money);
		for(Ll i=x;i<=y;i++)a[++nn]=cfb(i);
		sort(a+1,a+nn+1);
		for(int i=1;i<=nn;i++){
			a[i]+=a[i-1];
			if(a[i]>money){printf("%d\n",i-1);p=1;break;}
		}
		if(!p)printf("0\n");
	}
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define Ll long long
using namespace std;
Ll x,y,z,m;
Ll ans,sum;
int main()
{
	freopen("kniforbare.in","r",stdin);
	freopen("kniforbare.out","w",stdout);
	scanf("%I64d",&m);
	while(m--){
		scanf("%I64d%I64d%I64d",&x,&y,&z);
		sum=x*y*z;
		ans=sum-1;
		printf("%I64d ",ans);
		int k=0;
		while((Ll)(pow(2,k))<sum)k++;
		printf("%d\n",k);
	}
}

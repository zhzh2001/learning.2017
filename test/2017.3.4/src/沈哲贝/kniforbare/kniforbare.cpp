#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1010
#define mod 200000
#define inf 1000000000
#define eps 1e-9
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll f[maxn];
ll work2(ll x){
	ll ans=0;
	while (x>1)	x=(x+1)>>1,ans++;
	return ans;
}
ll work(ll l,ll w,ll h){
		if (l>w)	swap(l,w);	if (w>h)	swap(w,h);	if (l>w)	swap(l,w);
		if (h==1)	return 0;
		return work(l,w,h/2)+work(l,w,h-h/2)+1;
}
int main(){
	freopen("kniforbare.in","r",stdin); 
	freopen("kniforbare.out","w",stdout); 
	ll T=read();
	while(T--){
		ll l=read(),w=read(),h=read();
		printf("%d %d\n",work(l,w,h),work2(l)+work2(w)+work2(h));
	}
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1010
#define mod 200000
#define inf 1000000000
#define eps 1e-9
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll f[20][12][180],bin[20];
void pre(){
	For(i,0,9)	f[1][i][i]=1;
	For(i,2,18)	For(j,0,9)	For(k,j,162)	For(l,0,9)	f[i][j][k]+=f[i-1][l][k-j];
	bin[0]=1;
	For(i,1,18)	bin[i]=bin[i-1]*10;
	bin[19]=bin[18]+1;
}
ll ask(ll x,ll v){
	if (x<10)	return x>=v;
	ll ans=0,t;
	for(t=1;t<=19;t++)	if (bin[t]>x)	break;	t--;
	while(t&&v>=0){
		if (x<bin[t]){
			t--;	continue;
		}
		ll ttt=x/bin[t];
		if (t!=1)	For(j,0,ttt)	ans+=f[t+1][j-1][v];
		else	ans+=x>=v;
		x-=ttt*bin[t];	v-=ttt;	t--;
	}
	return ans;
}
void work(){
	ll A=read(),B=read(),x=read(),ans=ask(B,0)-ask(A-1,0);
	For(i,1,162){
		ll t=ask(B,i)-ask(A-1,i);
		if ((t-(double)x/i)>-eps){
			ans+=x/i;
			break;
		}
		ans+=t;	x-=t*i;
	}
	writeln(ans);
}
int main(){
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	pre();
	ll T=read();
	while(T--)	work();
}

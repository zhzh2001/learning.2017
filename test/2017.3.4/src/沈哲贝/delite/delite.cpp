#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 520 
#define inf 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll a[4]={0,1,0,-1};
ll b[4]={1,0,-1,0};
bool mark[maxn][maxn];
ll mp[maxn][maxn];
ll n,x[maxn],y[maxn],ans,ttt,tttt;
struct data{
	ll x,y;
}q[maxn*maxn];
ll spfa(ll x1,ll y1,ll x2,ll y2){
	memset(mp,127/3,sizeof mp);
	ll head=0,tail=1;
	mp[x1][y1]=0;
	q[1].x=x1;	q[1].y=y1;
	while(head!=tail){
		ll x=q[++head].x,y=q[head].y;
		if (x==x2&&y==y2)	return mp[x2][y2];
		For(i,0,3)
		if (!mark[x+a[i]][y+b[i]]&&x+a[i]>0&&x+a[i]<maxn&&y+b[i]>0&&y+b[i]<maxn&&mp[x+a[i]][y+b[i]]>mp[x][y]+1){
			mp[x+a[i]][y+b[i]]=mp[x][y]+1;
			q[++tail].x=x+a[i];	q[tail].y=y+b[i];
		}
	}
	return mp[x2][y2];
}
int main(){
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	n=read();
	For(i,1,n)	x[i]=read(),y[i]=read(),mark[x[i]][y[i]]=1,ttt=max(ttt,x[i]),tttt=max(tttt,y[i]);
	if (ttt>512||tttt>512){
		ll ans=0;
		For(i,1,n)	ans+=abs(x[i]-x[i%n+1])+abs(y[i]-y[i%n+1]);
		writeln(ans);
		return 0;
	}
	For(i,1,n){
		mark[x[i]][y[i]]=0;	mark[x[i%n+1]][y[i%n+1]]=0;
		ll t=spfa(x[i],y[i],x[i%n+1],y[i%n+1]);
		if (t>inf){
			puts("-1");
			return 0;
		}
		ans+=t;
		mark[x[i]][y[i]]=1;	mark[x[i%n+1]][y[i%n+1]]=1;
	}
	writeln(ans);
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
typedef long long ll;

const int N = 110;
const int M = 530;
int n;
int ans;
int x[N],y[N];
bool vis[M][M];
struct node{
	int x,y;
}
int bfs(int st,int ed){
	node u;
	u.x=x[st];u.y=y[st];
	queue<node> Q;
	Q.push(u);
	node v;
	int p;
	for(int i=1;i;i++){
		// v=Q.front();
		p=Q.size();
		// Q.pop();
		for(int j=1;j<=p;j++){
			v=Q.front();
			Q.pop();
			if(v.x==x[ed]&&v.y==y[ed])
				return i-1;
			if(!vis[v.x+1][v.y]){
				node k;
				k.x=v.x+1; k.y=v.y;
				Q.push(k);
				vis[v.x+1][v.y]=true;
			}
			if(!vis[v.x][v.y+1]){
				node k;
				k.x=v.x; k.y=v.y+1;
				Q.push(k);
				vis[v.x][v.y+1]=true;
			}
			if(!vis[v.x][v.y-1]){
				node k;
				k.x=v.x; k.y=v.y-1;
				Q.push(k);
				vis[v.x][v.y-1]=true;
			}
			if(!vis[v.x-1][v.y]){
				node k;
				k.x=v.x-1; k.y=v.y;
				Q.push(k);
				vis[v.x-1][v.y]=true;
			}
		}
			
	}
}
int main(){
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		x[i]=read();y[i]=read();
		vis[x[i]][y[i]]=true;
	}
	/*for(int i=1;i<n;i++){
		ans+=bfs(i,i+1);
		memset(vis,0,sizeof vis);
		for(int j=1;j<=n;j++)
			vis[x[j]][y[j]]=true;
	}
	ans+=bfs(n,1);
	cout<<ans;*/
	cout<<"-1"<<endl;
	return 0;
}
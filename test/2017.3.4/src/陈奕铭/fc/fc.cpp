#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll A,B,X;
struct node{
	ll ans[154];
	node(){
		for(ll i=0;i<=154;i++) ans[i]=0;
	}
	
}f[20][10];
node operator -(node a,node b){
	node p;
	for(ll i=1;i<=154;i++) p.ans[i]=a.ans[i]-b.ans[i];
	return p;
}
ll base[19];

void dp(){
	base[1]=1;
	for(int i=2;i<=19;i++)
		base[i]=base[i-1]*10;
	for(ll i=0;i<=9;i++)
		f[1][i].ans[i]=1;
	for(ll k=2;k<=18;k++){
		for(ll i=0;i<=9;i++){
			for(ll j=0;j<=9;j++){
				for(ll z=j;z<=(k-1)*9;z++){
					f[k][i].ans[z+i]+=f[k-1][j].ans[z];
				}
			}
		}
	}
}

node shu(ll x){
	ll a[20];
	ll p=x,num=19,q=0;
	node ans;
	while(base[num]>x)
		num--;
	for(int i=1;i<num;i++)
		for(int j=1;j<=9;j++)
			for(int k=1;k<=153;k++) ans.ans[k]+=f[i][j].ans[k];
	int cur=x/base[num];
	for(int i=1;i<=cur;i++)
		for(int k=1;k<=153;k++) ans.ans[k]+=f[num][i].ans[k];
	x%=base[num];
	q+=cur;
	for(int i=num-1;i;i--){
		cur=x/base[i];
		if(i!=1){
			for(int j=0;j<cur;j++)
				for(int k=1;k<=153;k++) ans.ans[k+q]+=f[i][j].ans[k];
		}
		else{
			for(int j=0;j<=cur;j++)
				for(int k=1;k<=153;k++) ans.ans[k+q]+=f[i][j].ans[k];
		}
		q+=cur;
		x%=base[i];
	}
	/*for(ll i=num;i>=1;i--){
		if(!a[i]) continue;
		if(i!=1){
			ans.ans[a[i]+q]++;
			for(ll j=0;j<=153;j++)
				ans.ans[j+q]+=f[i][a[i]-1].ans[j];
		}
		else{
			
		}
		q+=a[i];
	}*/
	return ans;
}

int main(){
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	ll t=read();
	dp();
	while(t--){
		A=read();B=read();X=read();
		node ans;
		if(A==1) ans=shu(B);
		else ans=shu(B)-shu(A-1);
		ll num=0;
		for(ll i=1;i<=153;i++){
			if(!ans.ans[i]) continue;
			printf("%lld:%lld\n",i,ans.ans[i]);
			if(ans.ans[i]*i<X){
				num+=ans.ans[i];
				X-=ans.ans[i]*i;
			}
			else{
				num+=X/i;
				break;
			}
		}
		cout<<num<<"\n";
	}
	return 0;
}
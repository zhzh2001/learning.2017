#include<bits/stdc++.h>
using namespace std;
bool b[305][305];
int n,sx,sy;
int x[105],y[105];
int dx[4]={0,1,0,-1};
int dy[4]={1,0,-1,0};
struct xxx{
	int x,y,s;
}q[100005],a[105];
int bfs(int k,int t)
{
	memset(b,false,sizeof(b));
	b[n][n]=true;
	int head=1;
	int tail=1;
	q[head].x=n;
	q[head].y=n;
	for(int i=1;i<=n;i++)
		if(abs(a[i].x-a[k].x)<=n&&abs(a[i].y-a[k].y)<=n)
			b[a[i].x-a[k].x+n][a[i].y-a[k].y+n]=true;
	b[a[t].x-a[k].x+n][a[t].y-a[k].y+n]=false;
	int ex=a[t].x-a[k].x+n;
	int ey=a[t].y-a[k].y+n;
	for(;head<=tail;){
		for(int i=0;i<4;i++){
			int tx=q[head].x+dx[i];
			int ty=q[head].y+dy[i];
			if(tx<1||ty<1||tx>n*3||ty>n*3||b[tx][ty])continue;
			if(tx==ex&&ty==ey)return q[head].s+1;
			tail++;
			q[tail].s=q[head].s+1;
			q[tail].x=tx;
			q[tail].y=ty;
			b[tx][ty]=true;
		}
		head++;
	}
	return -1;
}
int main()
{
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d%d",&a[i].x,&a[i].y);
	long long ans=0LL;
	for(int i=1;i<=n;i++){
		int k=i;
		int t=i%n+1;
		if(abs(a[k].x-a[t].x)>n||abs(a[k].y-a[t].y)>n){
			ans+=abs(a[k].x-a[t].x)+abs(a[k].y-a[t].y);
		}
		else{
		int xx=bfs(k,t);
		if(xx==-1){
			printf("-1");
			return 0;
		}
		ans=ans+xx;
		}
	}
	printf("%I64d",ans);
}

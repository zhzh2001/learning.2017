#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm> 
using namespace std;
int a[105],b[105],e[305],d[305],mp[305][305],dis[305][305],v[305][305];
int q[100010][2],tot,ld,le,ans,sx,sy,ex,ey,h,t,n;
int main(){
	freopen("delite.in","r",stdin);
	freopen("delite.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&a[i],&b[i]);
		d[++tot]=a[i]; e[tot]=b[i];
		d[++tot]=a[i]-1; e[tot]=b[i]-1;
		d[++tot]=a[i]+1; e[tot]=b[i]+1;
	}
	sort(d+1,d+tot+1);
	sort(e+1,e+tot+1);
	ld=le=1;
	for (int i=2;i<=tot;i++) if (d[i]!=d[i-1]) d[++ld]=d[i];
	for (int i=2;i<=tot;i++) if (e[i]!=e[i-1]) e[++le]=e[i];
	for (int i=1;i<=n;i++){
		a[i]=lower_bound(d+1,d+ld+1,a[i])-d;
		b[i]=lower_bound(e+1,e+le+1,b[i])-e;
	}
	for (int i=1;i<=n;i++) mp[a[i]][b[i]]=1;
	ans=0;
	for (int i=1;i<=n;i++){
		sx=a[i]; sy=b[i];
		ex=a[i%n+1]; ey=b[i%n+1];
		mp[sx][sy]=mp[ex][ey]=0;
		memset(dis,60,sizeof(dis));
		memset(v,0,sizeof(v));
		q[1][0]=sx,q[1][1]=sy;
		v[sx][sy]=1; dis[sx][sy]=0;
		h=0; t=1;
		while (h!=t){
			h=h%100007+1;
			sx=q[h][0];
			sy=q[h][1];
			v[sx][sy]=0;
			if (sx!=1&&mp[sx-1][sy]==0&&dis[sx-1][sy]>dis[sx][sy]+d[sx]-d[sx-1]){
				dis[sx-1][sy]=dis[sx][sy]+d[sx]-d[sx-1];
				if (!v[sx-1][sy]){
					v[sx-1][sy]=1;
					t=t%100007+1;
					q[t][0]=sx-1;
					q[t][1]=sy;
				}
			}
			if (sx!=ld&&mp[sx+1][sy]==0&&dis[sx+1][sy]>dis[sx][sy]+d[sx+1]-d[sx]){
				dis[sx+1][sy]=dis[sx][sy]+d[sx+1]-d[sx];
				if (!v[sx+1][sy]){
					v[sx+1][sy]=1;
					t=t%100007+1;
					q[t][0]=sx+1;
					q[t][1]=sy;
				}
			}
			if (sy!=1&&mp[sx][sy-1]==0&&dis[sx][sy-1]>dis[sx][sy]+e[sy]-e[sy-1]){
				dis[sx][sy-1]=dis[sx][sy]+e[sy]-e[sy-1];
				if (!v[sx][sy-1]){
					v[sx][sy-1]=1;
					t=t%100007+1;
					q[t][0]=sx;
					q[t][1]=sy-1;
				}
			}
			if (sy!=le&&mp[sx][sy+1]==0&&dis[sx][sy+1]>dis[sx][sy]+e[sy+1]-e[sy]){
				dis[sx][sy+1]=dis[sx][sy]+e[sy+1]-e[sy];
				if (!v[sx][sy+1]){
					v[sx][sy+1]=1;
					t=t%100007+1;
					q[t][0]=sx;
					q[t][1]=sy+1;
				}
			}
		}
		if (dis[ex][ey]>1000000000){
			printf("-1");
			return 0;
		}
		ans+=dis[ex][ey];
		mp[a[i]][b[i]]=mp[ex][ey]=1;
	}
	printf("%d",ans);
}

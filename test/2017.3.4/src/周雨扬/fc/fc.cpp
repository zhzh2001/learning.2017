#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm> 
#define ll long long
using namespace std;
ll a[20],b[205],f[20][11][205],l,r,n,ans,T;
void get(ll x,ll y){
	if (x<0) return;
	b[0]+=y;
	if (x==0) return;
	memset(a,0,sizeof(a));
	int len=0;
	for (;x;x/=10) a[++len]=x%10;
	for (int i=1;i<len;i++)
		for (int j=1;j<=9;j++)
			for (int k=j;k<=(i-1)*9+j;k++)
				b[k]+=y*f[i][j][k];
	int tot=0;
	for (int i=len;i>=1;i--){
		for (int j=(i==len)?1:0;j<a[i];j++)
			for (int k=0;k<=(i-1)*9;k++)
				b[j+k+tot]+=y*f[i][j][j+k];
		tot+=a[i];
	}
	b[tot]+=y;
}
int main(){
	freopen("fc.in","r",stdin);
	freopen("fc.out","w",stdout);
	f[0][0][0]=1;
	for (int i=0;i<=18;i++)
		for (int j=0;j<=9;j++)
			for (int k=0;k<=9;k++)
				for (int l=j;l<=i*9;l++)
					f[i+1][k][l+k]+=f[i][j][l];
	scanf("%I64d",&T);
	while (T--){
		scanf("%I64d%I64d%I64d",&l,&r,&n);
		memset(b,0,sizeof(b));
		get(r,1);
		get(l-1,-1);
		ans=b[0];
		for (int i=1;i<=200;i++)
			if (n/i>=b[i]) ans+=b[i],n-=b[i]*i;
			else{ans+=n/i; n=0; break;}
		printf("%I64d\n",ans);
	}
}

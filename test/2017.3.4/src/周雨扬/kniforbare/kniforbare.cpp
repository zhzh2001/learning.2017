#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
long long f[2005],T,x,y,z;
int main(){
	freopen("kniforbare.in","r",stdin);
	freopen("kniforbare.out","w",stdout);
	f[1]=0;
	for (int i=2;i<=2000;i++) f[i]=f[i-i/2]+1;
	scanf("%I64d",&T);
	while (T--){
		scanf("%I64d%I64d%I64d",&x,&y,&z);
		printf("%I64d %d\n",x*y*z-1,f[x]+f[y]+f[z]);
	}
}

#include<fstream>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
ifstream fin("delite.in");
ofstream fout("delite.out");
const int N=105,dx[]={-1,1,0,0},dy[]={0,0,-1,1},PM=520,INF=0x3f3f3f3f;
struct point
{
	int x,y;
	point(int x=0,int y=0):x(x),y(y){};
	bool operator==(const point& rhs)const
	{
		return x==rhs.x&&y==rhs.y;
	}
}p[N];
struct blocker
{
	int val,v[N],cnt;
}a[N];
int d[PM][PM];
queue<point> Q;
int main()
{
	int n;
	fin>>n;
	int maxx=0;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i].x>>p[i].y;
		maxx=max(maxx,p[i].x);
	}
	p[n+1]=p[1];
	if(maxx<=512)
	{
		//cheat
		int ans=0;
		for(int i=1;i<=n;i++)
		{
			memset(d,0x3f,sizeof(d));
			for(int j=1;j<=n;j++)
				if(j!=i+1&&j!=i+1-n)
					d[p[j].x][p[j].y]=0;
			while(!Q.empty())
				Q.pop();
			Q.push(p[i]);
			bool found=false;
			while(!Q.empty())
			{
				point k=Q.front();Q.pop();
				if(k==p[i+1])
				{
					ans+=d[k.x][k.y];
					found=true;
					break;
				}
				for(int j=0;j<4;j++)
				{
					int nx=k.x+dx[j],ny=k.y+dy[j];
					if(nx>=0&&nx<PM&&ny>=0&&ny<PM&&d[nx][ny]==INF)
					{
						d[nx][ny]=d[k.x][k.y]+1;
						Q.push(point(nx,ny));
					}
				}
			}
			if(!found)
			{
				fout<<-1<<endl;
				return 0;
			}
		}
		fout<<ans<<endl;
		return 0;
	}
	//strange solution start
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		ans+=abs(p[i].x-p[i+1].x)+abs(p[i].y-p[i+1].y);
		if(p[i].x==p[i+1].x)
		{
			int y1=p[i].y,y2=p[i+1].y;
			if(y1>y2)
				swap(y1,y2);
			int cc=0;
			for(int j=1;j<=n;j++)
				if(j!=i&&j!=i+1&&p[j].x==p[i].x&&p[j].y>y1&&p[j].y<y2)
				{
					a[++cc].val=p[j].y;
					a[cc].cnt=0;
				}
			for(int j=1;j<=n;j++)
				for(int k=1;k<=cc;k++)
					if(p[j].y==a[k].val)
					{
						a[k].v[++a[k].cnt]=p[j].x;
						break;
					}
			int block=0;
			for(int j=1;j<=cc;j++)
			{
				sort(a[j].v+1,a[j].v+a[j].cnt+1);
				a[j].cnt=unique(a[j].v+1,a[j].v+a[j].cnt+1)-a[j].v-1;
				int t=lower_bound(a[j].v+1,a[j].v+a[j].cnt+1,p[i].x)-a[j].v,minv,maxv;
				for(minv=t-1;minv>=0&&a[j].v[minv+1]-a[j].v[minv]==1;minv--);
				for(maxv=t+1;maxv<=cc&&a[j].v[maxv]-a[j].v[maxv-1]==1;maxv++);
				block=max(block,min(t-minv,maxv-t));
			}
			ans+=block*2;
		}
		if(p[i].y==p[i+1].y)
		{
			int x1=p[i].x,x2=p[i+1].x;
			if(x1>x2)
				swap(x1,x2);
			int cc=0;
			for(int j=1;j<=n;j++)
				if(j!=i&&j!=i+1&&p[j].y==p[i].y&&p[j].x>x1&&p[j].x<x2)
				{
					a[++cc].val=p[j].x;
					a[cc].cnt=0;
				}
			for(int j=1;j<=n;j++)
				for(int k=1;k<=cc;k++)
					if(p[j].x==a[k].val)
					{
						a[k].v[++a[k].cnt]=p[j].y;
						break;
					}
			int block=0;
			for(int j=1;j<=cc;j++)
			{
				sort(a[j].v+1,a[j].v+a[j].cnt+1);
				a[j].cnt=unique(a[j].v+1,a[j].v+a[j].cnt+1)-a[j].v-1;
				int t=lower_bound(a[j].v+1,a[j].v+a[j].cnt+1,p[i].y)-a[j].v,minv,maxv;
				for(minv=t-1;minv>=0&&a[j].v[minv+1]-a[j].v[minv]==1;minv--);
				for(maxv=t+1;maxv<=cc&&a[j].v[maxv]-a[j].v[maxv-1]==1;maxv++);
				block=max(block,min(t-minv,maxv-t));
			}
			ans+=block*2;
		}
	}
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("kniforbare.in");
ofstream fout("kniforbare.out");
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int l,w,h;
		fin>>l>>w>>h;
		fout<<(long long)l*w*h-1<<' '<<ceil(log2(l))+ceil(log2(w))+ceil(log2(h))<<endl;
	}
	return 0;
}
#include<fstream>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("fc.in");
ofstream fout("fc.ans");
int cnt[100];
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int a,b,x;
		fin>>a>>b>>x;
		memset(cnt,0,sizeof(cnt));
		for(int i=a;i<=b;i++)
		{
			int x=i,sum=0;
			do
				sum+=x%10;
			while(x/=10);
			cnt[sum]++;
		}
		int maxv=(log10(b)+1)*9,ans=0;
		for(int i=1;i<=maxv;i++)
			if(i*cnt[i]<=x)
			{
				x-=i*cnt[i];
				ans+=cnt[i];
			}
			else
			{
				ans+=x/i;
				break;
			}
		fout<<ans<<endl;
	}
	return 0;
}
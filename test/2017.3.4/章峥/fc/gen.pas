program gen;
const
  t=10;
  n=1000000000;
var
  i,a,b:longint;
procedure swap(var a,b:longint);
var
  t:longint;
begin
  t:=a;a:=b;b:=t;
end;
begin
  randomize;
  assign(output,'fc.in');
  rewrite(output);
  writeln(t);
  for i:=1 to t do
  begin
    a:=random(n)+1;
	b:=random(n)+1;
	if a>b then
	  swap(a,b);
	writeln(a,' ',b,' ',random(n)+1);
  end;
  close(output);
end.
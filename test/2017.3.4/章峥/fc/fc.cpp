#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("fc.in");
ofstream fout("fc.out");
long long f[20][10][200],dig[20];
long long query(long long x,int val)
{
	if(x==0)
		return 0;
	int len=log10(x)+1;
	long long ans=0;
	for(int i=1;i<len;i++)
		for(int j=1;j<10;j++)
			ans+=f[i][j][val];
	for(int i=1;i<x/dig[len]||(len==1&&i==x/dig[len]);i++)
		ans+=f[len][i][val];
	val-=x/dig[len];
	if(val<0)
		return ans;
	x%=dig[len];
	for(int i=len-1;i;i--)
	{
		for(int j=0;j<x/dig[i]||(i==1&&j==x/dig[i]);j++)
			ans+=f[i][j][val];
		val-=x/dig[i];
		if(val<0)
			return ans;
		x%=dig[i];
	}
	return ans;
}
int main()
{
	for(int i=0;i<10;i++)
		f[1][i][i]=1;
	for(int i=2;i<=18;i++)
		for(int j=0;j<10;j++)
			for(int k=0;k<10;k++)
				for(int t=0;t<=i*9;t++)
					f[i][j][t]+=f[i-1][k][t-j];
	dig[1]=1;
	for(int i=2;i<=19;i++)
		dig[i]=dig[i-1]*10;
	int t;
	fin>>t;
	while(t--)
	{
		long long a,b,x;
		fin>>a>>b>>x;
		long long ans=0;
		int maxv=(log10(b)+1)*9;
		for(int i=1;i<=maxv;i++)
		{
			long long cnt=query(b,i)-query(a-1,i);
			if(cnt*i<=x)
			{
				x-=cnt*i;
				ans+=cnt;
			}
			else
			{
				ans+=x/i;
				break;
			}
		}
		fout<<ans<<endl;
	}
	return 0;
}
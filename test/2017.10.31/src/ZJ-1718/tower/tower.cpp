#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("tower.in");
ofstream fout("tower.out");
const int N = 100005;
int t[N];
long long a[N];
struct node
{
	int numl, numr, l, r, val;
} tree[1 << 18];
inline void pullup(int id, int l, int r)
{
	tree[id].numl = tree[id * 2].numl;
	tree[id].numr = tree[id * 2 + 1].numr;
	int mid = (l + r) / 2;
	tree[id].l = tree[id * 2].l;
	if (tree[id * 2].l == mid - l + 1 && (tree[id * 2].numr > 0 || tree[id * 2 + 1].numl < 0))
		tree[id].l += tree[id * 2 + 1].l;
	tree[id].r = tree[id * 2 + 1].r;
	if (tree[id * 2 + 1].r == r - mid && (tree[id * 2].numr > 0 || tree[id * 2 + 1].numl < 0))
		tree[id].r += tree[id * 2].r;
	tree[id].val = max(tree[id * 2].val, tree[id * 2 + 1].val);
	if (tree[id * 2].numr > 0 || tree[id * 2 + 1].numl < 0)
		tree[id].val = max(tree[id].val, tree[id * 2].r + tree[id * 2 + 1].l);
}
inline int getsign(long long x)
{
	if (x > 0)
		return 1;
	if (x < 0)
		return -1;
	return 0;
}
void build(int id, int l, int r)
{
	if (l == r)
	{
		tree[id].numl = tree[id].numr = getsign(a[l]);
		if (a[l])
			tree[id].l = tree[id].r = tree[id].val = 1;
		else
			tree[id].l = tree[id].r = tree[id].val = 0;
	}
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		pullup(id, l, r);
	}
}
void modify(int id, int l, int r, int x, int val)
{
	if (l == r)
	{
		a[l] += val;
		tree[id].numl = tree[id].numr = getsign(a[l]);
		if (a[l])
			tree[id].l = tree[id].r = tree[id].val = 1;
		else
			tree[id].l = tree[id].r = tree[id].val = 0;
	}
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(id * 2, l, mid, x, val);
		else
			modify(id * 2 + 1, mid + 1, r, x, val);
		pullup(id, l, r);
	}
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> t[i];
	for (int i = 2; i <= n; i++)
		a[i] = t[i] - t[i - 1];
	build(1, 2, n);
	int m;
	fin >> m;
	while (m--)
	{
		int l, r, val;
		fin >> l >> r >> val;
		if (l > 1)
			modify(1, 2, n, l, val);
		if (r + 1 <= n)
			modify(1, 2, n, r + 1, -val);
		fout << tree[1].val + 1 << endl;
	}
	return 0;
}
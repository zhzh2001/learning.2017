#include <fstream>
#include <string>
#include <cstring>
using namespace std;
ifstream fin("easy.in");
ofstream fout("easy.out");
const int N = 1000, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
string mat[N];
int n, m, p;
bool row[N + 1][N], col[N][N + 1];
int go(int x, int y, int dir)
{
	for (;;)
	{
		if (x < 0)
			return y;
		if (y == m)
			return x + m;
		if (x == n)
			return m - y - 1 + n + m;
		if (y < 0)
			return n - x - 1 + n + m + m;
		if (mat[x][y] == '\\')
			dir = (3 - dir) ^ 1;
		else
			dir = 3 - dir;
		int nx = x + dx[dir], ny = y + dy[dir];
		switch (dir)
		{
		case 0:
			row[x][y] = true;
			break;
		case 1:
			row[nx][ny] = true;
			break;
		case 2:
			col[x][y] = true;
			break;
		case 3:
			col[nx][ny] = true;
			break;
		}
		x = nx;
		y = ny;
	}
}
int main()
{
	fin >> n >> m >> p;
	for (int i = 0; i < n; i++)
		fin >> mat[i];
	for (int i = 0; i < (n + m) * 2; i++)
	{
		int ret;
		if (i < m)
			ret = go(0, i, 1);
		else if (i < n + m)
			ret = go(i - m, m - 1, 2);
		else if (i < n + m + m)
			ret = go(n - 1, m - (i - n - m) - 1, 0);
		else
			ret = go(n - (i - n - m - m) - 1, 0, 3);
		if (ret != (i + 1) % ((n + m) * 2) && ret != (i + (n + m) * 2 - 1) % ((n + m) * 2))
		{
			fout << 0 << endl;
			return 0;
		}
	}
	for (int i = 1; i < n; i++)
		for (int j = 0; j < m; j++)
			if (!row[i][j])
			{
				fout << 0 << endl;
				return 0;
			}
	for (int i = 0; i < n; i++)
		for (int j = 1; j < m; j++)
			if (!col[i][j])
			{
				fout << 0 << endl;
				return 0;
			}
	fout << 1 << endl;
	return 0;
}

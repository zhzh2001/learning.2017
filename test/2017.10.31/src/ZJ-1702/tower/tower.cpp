#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define N 100005
#define M 400005
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll a[N],d[N];
struct node{
	ll l,r;
	// only '+' or '-' 
	ll lmax1,rmax1;
	ll lmax0,rmax0;
	ll max1,max0;
	// '+' with '-'
	ll lmax10,rmax10;
	ll max10;
}tr[M];
ll n,m;

inline ll Max(ll x,ll y){
	if(x > y) return x;
	return y;
}

inline void Update(ll num){
	ll l = num<<1,r = num<<1|1;
	if(tr[l].max1 == (tr[l].r-tr[l].l+1))
		tr[num].lmax1 = tr[l].max1+tr[r].lmax1;
	else tr[num].lmax1 = tr[l].lmax1;
	
	if(tr[l].max0 == (tr[l].r-tr[l].l+1))
		tr[num].lmax0 = tr[l].max0+tr[r].lmax0;
	else tr[num].lmax0 = tr[l].lmax0;
	
	if(tr[r].max1 == (tr[r].r-tr[r].l+1))
		tr[num].rmax1 = tr[r].max1+tr[l].rmax1;
	else tr[num].rmax1 = tr[r].rmax1;
	
	if(tr[r].max0 == (tr[r].r-tr[r].l+1))
		tr[num].rmax0 = tr[r].max0+tr[l].rmax0;
	else tr[num].rmax0 = tr[r].rmax0;
	
	tr[num].max1 = Max(tr[num].lmax1,Max(tr[num].rmax1,tr[l].rmax1+tr[r].lmax1));
	tr[num].max1 = Max(tr[num].max1,Max(tr[l].max1,tr[r].max1));
	tr[num].max0 = Max(tr[num].lmax0,Max(tr[num].rmax0,tr[l].rmax0+tr[r].lmax0));
	tr[num].max0 = Max(tr[num].max0,Max(tr[l].max0,tr[r].max0));
	
	if(tr[l].max10 == (tr[l].r-tr[l].l+1))
		tr[num].lmax10 = tr[l].max10+tr[r].lmax0;
	else tr[num].lmax10 = tr[l].lmax10;
//	tr[num].lmax10 = Max(tr[num].lmax10,Max(tr[num].lmax1,tr[num].lmax0));
	if(tr[l].max1 == (tr[l].r-tr[l].l+1))
		tr[num].lmax10 = Max(tr[num].lmax10,tr[l].max1+tr[r].lmax10);
	
	if(tr[r].max10 == (tr[r].r-tr[r].l+1))
		tr[num].rmax10 = tr[r].max10+tr[l].rmax1;
	else tr[num].rmax10 = tr[r].rmax10;
//	tr[num].rmax10 = Max(tr[num].rmax10,Max(tr[num].rmax1,tr[num].rmax0));
	if(tr[r].max0 == (tr[r].r-tr[r].l+1))
		tr[num].rmax10 = Max(tr[num].rmax10,tr[r].max0+tr[l].rmax10);
	
	tr[num].max10 = Max(tr[num].lmax10,Max(tr[num].rmax10,tr[l].rmax1+tr[r].lmax0));
	tr[num].max10 = Max(tr[num].max10,Max(tr[l].rmax10+tr[r].lmax0,tr[r].lmax10+tr[l].rmax1));
	tr[num].max10 = Max(tr[num].max10,Max(tr[l].max10,tr[r].max10));
//	tr[num].max10 = Max(tr[num].max10,Max(tr[l].max1,tr[l].max0));
//	tr[num].max10 = Max(tr[num].max10,Max(tr[r].max1,tr[r].max0));
}

void build(ll num,ll l,ll r){
	tr[num].l = l; tr[num].r = r;
	if(l == r){
		if(d[l] > 0){
			tr[num].lmax1 = tr[num].rmax1 = tr[num].max1 = tr[num].lmax10 = tr[num].rmax10 = tr[num].max10 = 1;
			tr[num].lmax0 = tr[num].rmax0 = tr[num].max0 = 0;
		}
		else if(d[l] < 0){
			tr[num].lmax0 = tr[num].rmax0 = tr[num].max0 = tr[num].lmax10 = tr[num].rmax10 = tr[num].max10 = 1;
			tr[num].lmax1 = tr[num].rmax1 = tr[num].max1 = 0;
		}
		else{
			tr[num].lmax1 = tr[num].rmax1 = tr[num].max1 = tr[num].lmax10 = tr[num].rmax10 = tr[num].max10 = 0;
			tr[num].lmax0 = tr[num].rmax0 = tr[num].max0 = 0;
		}
		return;
	}
	ll mid = (l+r)>>1;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
	Update(num);
}

void add(ll num,ll x,ll y){
	ll l = tr[num].l,r = tr[num].r;
	if(l == r){
		d[l] += y;
		if(d[l] > 0){
			tr[num].lmax1 = tr[num].rmax1 = tr[num].max1 = tr[num].lmax10 = tr[num].rmax10 = tr[num].max10 = 1;
			tr[num].lmax0 = tr[num].rmax0 = tr[num].max0 = 0;
		}
		else if(d[l] < 0){
			tr[num].lmax0 = tr[num].rmax0 = tr[num].max0 = tr[num].lmax10 = tr[num].rmax10 = tr[num].max10 = 1;
			tr[num].lmax1 = tr[num].rmax1 = tr[num].max1 = 0;
		}
		else{
			tr[num].lmax1 = tr[num].rmax1 = tr[num].max1 = tr[num].lmax10 = tr[num].rmax10 = tr[num].max10 = 0;
			tr[num].lmax0 = tr[num].rmax0 = tr[num].max0 = 0;
		}
		return;
	}
	ll mid = (l+r)>>1;
	if(x <= mid) add(num<<1,x,y);
	else add(num<<1|1,x,y);
	Update(num);
}

/*void debug(ll num,ll l,ll r){
	if(l == r){
		printf("%lld %lld : l: %lld %lld # r: %lld %lld # m : %lld %lld # %lld %lld %lld\n",l,r,tr[num].lmax0,tr[num].lmax1,tr[num].rmax0,tr[num].rmax1,tr[num].max0,tr[num].max1,tr[num].lmax10,tr[num].rmax10,tr[num].max10);
		return;
	}
	ll mid = (l+r)>>1;
	debug(num<<1,l,mid);
	debug(num<<1|1,mid+1,r);
	printf("%lld %lld : l: %lld %lld # r: %lld %lld # m: %lld %lld # %lld %lld %lld\n",l,r,tr[num].lmax0,tr[num].lmax1,tr[num].rmax0,tr[num].rmax1,tr[num].max0,tr[num].max1,tr[num].lmax10,tr[num].rmax10,tr[num].max10);
}*/

signed main(){
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n = read();
	for(ll i = 1;i <= n;i++)
		a[i] = read();
	for(ll i = 1;i < n;i++)
		d[i] = a[i+1]-a[i];
	build(1,1,n-1);
	m = read();
	for(ll i = 1;i <= m;i++){
		ll l = read()-1,r = read(),g = read();
		if(l > 0) add(1,l,g);
		if(r < n) add(1,r,-g);
		printf("%lld\n",Max(tr[1].max10,Max(tr[1].max0,tr[1].max1))+1);
//		printf("# ");
//		for(int j = 1;j < n;j++)
//			printf("%lld ",d[j]);
//		puts("");
//		debug(1,1,n-1);
//		puts("");
	}
	return 0;
}

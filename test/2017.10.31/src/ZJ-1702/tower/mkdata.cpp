#include<iostream>
#include<cstdio>
#include<ctime>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define N 100005
#define M 400005
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll n,m;
ll a[N];

signed main(){
	freopen("tower.in","w",stdout);
	srand(time(0));
	n = 1000;
	m = 1000;
	printf("%lld\n",n);
	for(int i = 1;i <= n;i++){
		a[i] = rand()%10000+1;
		printf("%lld ",a[i]);
	}
	puts("");
	printf("%lld\n",m);
	for(int i = 1;i <= m;i++){
		ll l = rand()%1000+1,r = rand()%1000+1,g = rand()%10000+1;
		if(l > r) swap(l,r);
		printf("%lld %lld %lld\n",l,r,g);
	}
	return 0;
}

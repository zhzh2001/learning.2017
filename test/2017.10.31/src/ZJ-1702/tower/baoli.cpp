#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define N 100005
#define M 400005
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll n,m;
ll a[N];
ll lx[N],rx[N];

signed main(){
	freopen("tower.in","r",stdin);
	freopen("tower2.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;i++)
		a[i] = read();
	m = read();
	for(int i = 1;i <= m;i++){
		ll l = read(),r = read(),g = read();
		for(ll j = l;j <= r;j++)
			a[j] += g;
		lx[1] = 1;
		for(ll j = 2;j <= n;j++)
			if(a[j] > a[j-1])
				lx[j] = lx[j-1]+1;
			else lx[j] = 1;
		rx[n] = 1;
		for(ll j = n-1;j >= 1;j--)
			if(a[j] > a[j+1])
				rx[j] = rx[j+1]+1;
			else rx[j] = 1;
		ll Mx = 0;
		for(ll j = 1;j <= n;j++)
			Mx = max(Mx,lx[j]+rx[j]-1);
		printf("%lld\n",Mx);
	}
	return 0;
}

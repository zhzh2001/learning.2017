#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define N 105
#define M 1005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

//To: -> : 0:up,1:right,2:down,3:left 
const int step_x[4] = {1,0,-1,0};
const int step_y[4] = {0,-1,0,1};
char ch[N][N];
int n,m,P;
int fang[N][N][4];
int Px[M],Py[M],Pf[M];
int pp[N][N];
int ans;
bool ff[N][N][4];

inline bool pand(int x,int y){
	if(x < 1 || y < 1 || x > n || y > m) return false;
	return true;
}

inline int getw(int x,int y){
	if(x == 0) return y;
	if(y == m+1) return m+x;
	if(x == n+1) return n+m*2-y+1;
	if(y == 0) return n*2+m*2-x+1;
}

void dfs(int X,int Y){
	if(Y == m+1){
		X++;
		Y = 1;
	}
	if(X == n+1){
		for(int i = 1;i <= n;i++){
			for(int j = 1;j <= m;j++){
				if(ch[i][j] == '\\'){
					fang[i][j][0] = 3;
					fang[i][j][1] = 2;
					fang[i][j][2] = 1;
					fang[i][j][3] = 0;
				}
				else{
					fang[i][j][0] = 1;
					fang[i][j][1] = 0;
					fang[i][j][2] = 3;
					fang[i][j][3] = 2;
				}
			}
		}
		memset(ff,false,sizeof ff);
		int p;
		int x = 1,y = 1,f = 0;
		while(pand(x,y)){
			ff[x][y][f] = true;
			f = fang[x][y][f];
			if(f == 0) ff[x][y][2] = true;
			else if(f == 2) ff[x][y][0] = true;
			else  ff[x][y][4-f] = true;
			x = x+step_x[f];
			y = y+step_y[f];
		}
		p = pp[x][y];
	//	printf("%d %d P: %d\n",x,y,p);
		if(p == 2){
			for(int i = 3;i <= (n+m)*2;i += 2){
				x = Px[i]; y = Py[i]; f = Pf[i];
				while(pand(x,y)){
					ff[x][y][f] = true;
					f = fang[x][y][f];
					if(f == 0) ff[x][y][2] = true;
					else if(f == 2) ff[x][y][0] = true;
					else  ff[x][y][4-f] = true;
					x = x+step_x[f];
					y = y+step_y[f];
				}
				p = pp[x][y];
	//			printf("%d %d P: %d\n",x,y,p);	
				if(p != i+1) return;	
			}
		}
		else if(p == n*2+m*2){
			for(int i = 2;i < (n+m)*2;i += 2){
				x = Px[i]; y = Py[i]; f = Pf[i];
				while(pand(x,y)){
					ff[x][y][f] = true;
					f = fang[x][y][f];
					if(f == 0) ff[x][y][2] = true;
					else if(f == 2) ff[x][y][0] = true;
					else  ff[x][y][4-f] = true;
					x = x+step_x[f];
					y = y+step_y[f];
				}
				p = pp[x][y];
	//			printf("%d %d P: %d\n",x,y,p);	
				if(p != i+1)return;
			}
		}
		else return;
		for(int i = 1;i <= n;i++)
			for(int j = 1;j <= m;j++)
				for(int k = 0;k < 4;k++)
					if(!ff[i][j][k]) return;
		ans++;
		if(ans == P) ans = 0;
		return;
	}
	if(ch[X][Y] == '*'){
		ch[X][Y] = '\\';
		dfs(X,Y+1);
		ch[X][Y] = '/';
		dfs(X,Y+1);
		ch[X][Y] = '*';
		return;
	}
	dfs(X,Y+1);
}

int main(){
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	n = read(); m = read(); P = read();
	for(int i = 1;i <= n;i++)
		scanf("%s",ch[i]+1);
	int p;
	for(int i = 1;i <= m;i++){
		p = getw(0,i);
		pp[0][i] = p;
		Px[p] = 1; Py[p] = i; Pf[p] = 0;
	}
	for(int i = 1;i <= n;i++){
		p = getw(i,m+1);
		pp[i][m+1] = p;
		Px[p] = i; Py[p] = m; Pf[p] = 1;
	}
	for(int i = m;i >= 1;i--){
		p = getw(n+1,i);
		pp[n+1][i] = p;
		Px[p] = n; Py[p] = i; Pf[p] = 2;
	}
	for(int i = n;i >= 1;i--){
		p = getw(i,0);
		pp[i][0] = p;
		Px[p] = i; Py[p] = 1; Pf[p] = 3;
	}
	dfs(1,1);
	printf("%d\n",ans);
	return 0;
}

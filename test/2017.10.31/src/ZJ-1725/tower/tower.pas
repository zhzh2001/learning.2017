program tower;
 type
  dic=record
   rdece,ldece:int64;
   ltp,rtp:longint;
  end;
 var
  a:array[0..100001] of dic;
  b:array[0..100001] of int64;
  f:array[0..100001,0..1] of longint;
  i,j,m,n,lp,p,x,y,z,max:longint;
 begin
  assign(input,'tower.in');
  assign(output,'tower.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   read(b[i]);
  readln;
  b[0]:=100820882000000000;
  b[n+1]:=100820882000000000;
  fillchar(a,sizeof(a),0);
  a[1].ldece:=1;
  a[1].rdece:=1;
  p:=2;
  while p<=n do
   begin
    break;
    lp:=p-1;
    while (p<=n) and (b[p]>b[p-1]) do inc(p);
    inc(a[lp].rdece,p-lp-1);
    inc(a[lp].rtp,p-1);
    inc(a[p].rdece,lp-p+1);
    inc(a[p].rtp,1-p);
    lp:=p-1;
    while (p<=n) and (b[p]<b[p-1]) do inc(p);
    inc(a[lp].ldece,p-lp-1);
    inc(a[lp].ltp,lp);
    inc(a[p].ldece,lp-p+1);
    inc(a[p].ltp,-lp);
   end;
  readln(m);
  for i:=1 to m do
   begin
    max:=0;
    readln(x,y,z);
    for j:=x to y do
     inc(b[j],z);
    f[1,0]:=1;
    f[n,1]:=1;
    for j:=2 to n do
     begin
      if b[j]>b[j-1] then f[j,0]:=f[j-1,0]+1
                     else f[j,0]:=1;
      if b[n-j+1]>b[n-j+2] then f[n-j+1,1]:=f[n-j+2,1]+1
                           else f[n-j+1,1]:=1;
     end;
    for j:=2 to n do
     if f[j,0]+f[j,1]-1>max then max:=f[j,0]+f[j,1]-1;
    writeln(max);
   end;
 { for i:=2 to n do
   inc(a[i].ltp,a[i-1].ltp);
  for i:=2 to n do
   inc(a[i].rtp,a[i-1].rtp);
  for i:=2 to n do
   inc(a[i].ldece,a[i-1].ldece);
  for i:=2 to n do
   inc(a[i].rdece,a[i-1].rdece);
  for i:=1 to n-1 do
   write(a[i].ltp,' ');
  writeln(a[n].ltp);
  for i:=1 to n-1 do
   write(a[i].rtp,' ');
  writeln(a[n].rtp);
  for i:=1 to n-1 do
   write(a[i].ldece,' ');
  writeln(a[n].ldece);
  for i:=1 to n-1 do
   write(a[i].rdece,' ');
  writeln(a[n].rdece);}
  close(input);
  close(output);
 end.

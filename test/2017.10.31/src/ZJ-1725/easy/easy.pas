program mirror;
 var
  a:array[0..1001,0..1001] of char;
  f:array[0..1001,0..1001] of longint;
  i,j,m,n,p:longint;
 procedure hahawalk(var x,y,p:longint);
  var
   d:array[1..5] of longint=(0,-1,0,1,0);
  begin
   repeat
    case a[x,y] of
     '/':p:=5-p;
     '\':if p<3 then p:=3-p
                else p:=7-p;
    end;
    inc(x,d[p+1]);
    inc(y,d[p]);
    if (x>0) and (x<n+1) and (y>0) and (y<m+1) then inc(f[x,y]);
   until (x=0) or (y=0) or (x=n+1) or (y=m+1);
  end;
 function hahais:boolean;
  var
   i,j,x,y,p:longint;
  begin
   fillchar(f,sizeof(f),0);
   for j:=1 to m do
    if f[0,j]=0 then
     begin
      p:=2;
      f[0,j]:=1;
      a[0,j]:='/';
      x:=0;
      y:=j;
      hahawalk(x,y,p);
      if (j=1) and ((x<>1) or (y<>0)) and ((x<>0) or (y<>2)) then exit(false)
                                                             else if (j=m) and ((x<>1) or (y<>m+1)) and ((x<>0) or (y<>m-1)) then exit(false)
                                                                                                                             else if (abs(y-j)+x>1) and (j<>1) and (j<>m) or (f[x,y]=1) then exit(false);
      f[x,y]:=1;
     end;
   for i:=1 to n do
    if f[i,0]=0 then
     begin
      p:=3;
      f[i,0]:=1;
      a[i,0]:='\';
      x:=i;
      y:=0;
      hahawalk(x,y,p);
      if (i=1) and ((y<>1) or (x<>0)) and ((y<>0) or (x<>2)) then exit(false)
                                                             else if (i=n) and ((x<>n+1) or (y<>1)) and ((y<>0) or (x<>n-1)) then exit(false)
                                                                                                                             else if (abs(x-i)+y>1) and (i<>1) and (i<>n) or (f[x,y]=1) then exit(false);
      f[x,y]:=1;
     end;
   for j:=1 to m do
    if f[n+1,j]=0 then
     begin
      p:=4;
      f[n+1,j]:=1;
      a[n+1,j]:='/';
      x:=n+1;
      y:=j;
      hahawalk(x,y,p);
      if (j=1) and ((x<>n) or (y<>0)) and ((x<>n+1) or (y<>2)) then exit(false)
                                                               else if (j=m) and ((y<>m+1) or (x<>n)) and ((y<>m-1) or (x<>n+1)) then exit(false)
                                                                                                                                 else if (abs(y-j)+abs(x-n-1)>1) and (j<>1) and (j<>m) or (f[x,y]=1) then exit(false);
      f[x,y]:=1;
     end;
   for i:=1 to n do
    if f[i,m+1]=0 then
     begin
      p:=1;
      f[i,m+1]:=1;
      a[i,m+1]:='\';
      x:=i;
      y:=m+1;
      hahawalk(x,y,p);
      if (i=1) and ((y<>m) or (x<>0)) and ((y<>m+1) or (x<>2)) then exit(false)
                                                               else if (i=n) and ((x<>n+1) or (y<>m)) and ((x<>n-1) or (y<>m+1)) then exit(false)
                                                                                                                                 else if (abs(x-i)+abs(y-m-1)>1) and (i<>1) and (i<>n) or (f[x,y]=1) then exit(false);
      f[x,y]:=1;
     end;
   for i:=1 to n do
    for j:=1 to m do
     if f[i,j]<>2 then exit(false);
   exit(true);
  end;
 begin
  assign(input,'easy.in');
  assign(output,'easy.out');
  reset(input);
  rewrite(output);
  readln(n,m,p);
  for i:=1 to n do
   begin
    for j:=1 to m do
     read(a[i,j]);
    readln;
   end;
  if hahais then writeln(1 mod p)
            else writeln('0');
  close(input);
  close(output);
 end.

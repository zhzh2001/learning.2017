#include<bits/stdc++.h>
#define ll long long
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
namespace SHENZHEBEI{
#define NEG 1
	const int L=2333333;
	char SZB[L],*S=SZB,*T=SZB;
	inline char gc(){	if (S==T){	T=(S=SZB)+fread(SZB,1,L,stdin);	if (S==T) return '\n';	}	return *S++;	}
#if NEG
	inline ll read(){	ll x=0,f=1;	char ch=gc();	for (;!isdigit(ch);ch=gc())	if (ch=='-') f=-1;	for (;isdigit(ch);ch=gc())	x=x*10-48+ch;	return x*f;	}
	inline void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
#else
	inline ll read(){	ll x=0;	char ch=gc();	for (;!isdigit(ch);ch=gc());	for (;isdigit(ch);ch=gc())	x=x*10-48+ch;	return x;	}
	inline void write(ll x){	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
#endif
	inline char readchar(){	char ch=gc();	for(;isspace(ch);ch=gc());	return ch;	}
	inline ll readstr(char *s){	char ch=gc();	int cur=0;	for(;isspace(ch);ch=gc());		for(;!isspace(ch);ch=gc())	s[cur++]=ch;	s[cur]='\0';	return cur;	}
	inline void writeln(ll x){	write(x);	puts("");	}
}using namespace SHENZHEBEI;
const ll N=400010;
ll a[N],n,Q;
struct sgt{
	ll tag[N];
	void build(ll l,ll r,ll p){
		if (l==r){
			tag[p]=a[l]=read();
			return;
		}ll mid=(l+r)>>1;
		build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
	}
	void pushdown(ll p){	tag[p<<1]+=tag[p];	tag[p<<1|1]+=tag[p];	tag[p]=0;	}
	void add(ll l,ll r,ll p,ll s,ll t,ll v){
		if (l==s&&r==t){
			tag[p]+=v;
			return;
		}ll mid=(l+r)>>1;	pushdown(p);
		if (t<=mid)	add(l,mid,p<<1,s,t,v);
		else if (s>mid)	add(mid+1,r,p<<1|1,s,t,v);
		else	add(l,mid,p<<1,s,mid,v),add(mid+1,r,p<<1|1,mid+1,t,v);
	}
	ll query(ll l,ll r,ll p,ll v){
		if (l==r)	return tag[p];
		ll mid=(l+r)>>1;	pushdown(p);
		return v<=mid?query(l,mid,p<<1,v):query(mid+1,r,p<<1|1,v);
	}
}v;
struct sgt1{
	struct data{	ll sz,L,R,ldn,rdn,lup,rup,ans;	}tr[N],tmp,lbc[10];
	inline data merge(data a,data b){
		tmp.sz=a.sz+b.sz;
		tmp.ans=max(a.ans,b.ans);
		tmp.ans=max(tmp.ans,a.rup+b.L);
		tmp.ans=max(tmp.ans,a.R+b.ldn);
		tmp.ans=max(tmp.ans,a.rup+b.ldn);
		tmp.L=a.L;	tmp.R=b.R;
		tmp.ldn=a.ldn==a.sz?(a.ldn+b.ldn):a.ldn;
		tmp.lup=a.lup==a.sz?(a.lup+b.lup):a.lup;
		tmp.rdn=b.ldn==b.sz?(a.rdn+b.rdn):b.rdn;
		tmp.rup=b.lup==b.sz?(a.rup+b.rup):b.rup;
		a.lup==a.sz?	tmp.L=a.lup+b.L:0;
		b.ldn==b.sz?	tmp.R=b.ldn+a.R:0;
		if (a.L==a.sz)		tmp.L=max(tmp.L,a.L+b.ldn);
		if (b.R==b.sz)		tmp.R=max(tmp.R,b.R+a.rup);
		return tmp;
	}
	inline data new_nod(ll v){
		tmp.ans=0;	tmp.sz=1;
		if (v==1)	tmp.lup=tmp.rup=1;
		else	tmp.lup=tmp.rup=0;
		if (v==-1)	tmp.ldn=tmp.rdn=1;
		else	tmp.ldn=tmp.rdn=0;
		if (v!=0)	tmp.L=tmp.R=tmp.ans=1;
		else	tmp.L=tmp.R=0;
		return tmp;
	}
	void mk(){	For(i,0,2)	lbc[i]=new_nod(i-1);	}
	void build(ll l,ll r,ll p){
		if (l==r){
			tr[p]=lbc[a[l]<a[l+1]?2:a[l]>a[l+1]?0:1];
			return;
		}ll mid=(l+r)>>1;
		build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
		tr[p]=merge(tr[p<<1],tr[p<<1|1]);
	}
	void change(ll l,ll r,ll p,ll pos){
		if (l==r){
			ll x=v.query(1,n,1,l),y=v.query(1,n,1,l+1);
			tr[p]=lbc[x<y?2:x>y?0:1];
			return;
		}ll mid=(l+r)>>1;
		pos<=mid?change(l,mid,p<<1,pos):change(mid+1,r,p<<1|1,pos);
		tr[p]=merge(tr[p<<1],tr[p<<1|1]);
	}
}gt;
int main(){ 
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	gt.mk();
	n=read();	v.build(1,n,1);	gt.build(1,n-1,1);	Q=read(); 
	while(Q--){
		ll l=read(),r=read(),d=read();
		v.add(1,n,1,l,r,d);
		if (l>1)	gt.change(1,n-1,1,l-1);
		if (r<n)	gt.change(1,n-1,1,r);
		writeln(gt.tr[1].ans+1);
	}
}

#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#include<map>
#define ll int
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 100010
using namespace std;
const ll N=1010;
ll X[]={1,0,0,0};
ll Y[]={0,0,0,1};
ll rev[2][4]={{3,2,1,0},
			  {1,0,3,2}};
bool vis[2][N][N];	char s[N];
ll mp[N][N],a[2][N][N],n,m,mod,szb[N][2],ind,ans,tot;
struct data{	ll typ,x,y,r;	}p[N*4];
ll dfs(data x){
	data y;
	while(1){
		y=x;	vis[x.typ][x.x][x.y]=1;
		y.typ^=1;
		if (mp[x.x+X[x.r]][x.y+Y[x.r]]==0){
			if (x.r==0)	y.x++;
			if (x.r==1)	y.x--;
			if (x.r==2)	y.y--;
			if (x.r==3)	y.y++;
		}else{
			if (x.r==0)	y.x++,y.y--;
			if (x.r==3)	y.x--,y.y++;
		}y.r=rev[mp[x.x+X[x.r]][x.y+Y[x.r]]][x.r];
		if (a[y.typ][y.x][y.y])	return vis[y.typ][y.x][y.y]=1,a[y.typ][y.x][y.y];
		x=y;
	}
}
bool pd(){
	memset(a,0,sizeof a);
	memset(vis,0,sizeof vis);
	ind=0;
	For(i,1,m)	a[0][0][i]=++ind,p[ind]=(data){0,0,i,0};
	For(i,1,n)	a[1][i][m]=++ind,p[ind]=(data){1,i,m,1};
	FOr(i,m,1)	a[0][n][i]=++ind,p[ind]=(data){0,n,i,2};
	FOr(i,n,1)	a[1][i][0]=++ind,p[ind]=(data){1,i,0,3};
	For(i,1,ind)	if (!vis[p[i].typ][p[i].x][p[i].y]){
		ll t=dfs(p[i]);
		if (t+1==i)	continue;
		if (t-1==i)	continue;
		if (t+1==i+ind)	continue;
		if (t-1==i+ind)	continue;
		if (t+1==i-ind)	continue;
		if (t-1==i-ind)	continue;
		return 0;
	}
	For(i,0,n)	For(j,1,m)	if (!vis[0][i][j])	return 0;
	For(i,1,n)	For(j,0,m)	if (!vis[1][i][j])	return 0;
	return 1%mod;
}
void dfs(ll x){
	if (x>tot)	return ans+=pd(),void(0);
	For(i,0,1){
		mp[szb[x][0]][szb[x][1]]=i;
		dfs(x+1);
	}
}
int main(){
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	scanf("%d%d%d",&n,&m,&mod);
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m){
			if(s[j]=='/')	mp[i][j]=1;
			else if (s[j]=='\\')	mp[i][j]=0;
			else{
				szb[++tot][0]=i;	szb[tot][1]=j;
			} 
		}
	}dfs(1);printf("%d",ans%mod);
}

#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<math.h>
#include<fstream>
//#include<iostream>
using namespace std;
ifstream fin("mirror.in");
ofstream fout("mirror.out");
int n,m,P;
char mp[1003][1003];
int vis[1003][1003][5];
int lastx,lasty,nowx,nowy;
int bian[1003][1003];
inline bool pd(int val,int x,int y,int ch){
	return val==ch;
}
inline void dfs(int x,int y,int dir,int ch){
	if(y==0||y==m+1||x==0||x==n+1){
		if(y==0){
			lastx=x,lasty=y+1;
		}else if(y==m+1){
			lastx=x,lasty=y-1;
		}else if(x==0){
			lastx=x+1,lasty=y;
		}else{
			lastx=x-1,lasty=y;
		}
		return;
	}
	if((y==1&&dir==2&&pd(2,x,y,ch))||(y==m&&dir==4&&pd(2,x,y,ch))||(x==1&&dir==3&&pd(1,x,y,ch))||(x==n&&dir==1&&pd(1,x,y,ch))){
		lastx=x,lasty=y;
		return;
	}
	if(dir==1){
		if(mp[x][y]=='/'){
			dfs(x,y-1,2,2);
		}else{
			dfs(x,y+1,4,2);
		}
	}else if(dir==2){
		if(mp[x][y]=='/'){
			dfs(x+1,y,1,1);
		}else{
			dfs(x-1,y,3,1);
		}
	}else if(dir==3){
		if(mp[x][y]=='/'){
			dfs(x,y+1,4,2);
		}else{
			dfs(x,y-1,2,2);
		}
	}else if(dir==4){
		if(mp[x][y]=='/'){
			dfs(x-1,y,3,1);
		}else{
			dfs(x+1,y,1,1);
		}
	}
}
int main(){
	fin>>n>>m>>P;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>mp[i][j];
		}
	}
	for(int i=1;i<=m;i++){
		bian[1][i]=i;
	}
	for(int i=2;i<=n;i++){
		bian[i][m]=bian[i-1][m]+1;
	}
	for(int i=m-1;i>=1;i--){
		bian[n][i]=bian[n][i+1]+1;
	}
	for(int i=n-1;i>=2;i--){
		bian[i][1]=bian[i+1][1]+1;
	}
	dfs(2,2,2,0);
	bool yes=true;
	for(int i=1;i<=m;i++){
		dfs(1,i,1,0);
		int x=bian[lastx][lasty];
		if(bian[1][i]==1){
			if((x==n+n+m+m)&&(x!=2)&&(x!=1)){
				yes=false;
			}
		}else{
			if(x!=bian[1][i]&&x!=bian[1][i]-1&&x!=bian[1][i]+1){
				yes=false;
			}
		}
	}
	for(int i=1;i<=n;i++){
		dfs(i,m,2,0);
		int x=bian[lastx][lasty];
		if(x!=bian[i][m]-1&&x!=bian[i][m]+1&&x!=bian[i][m]){
			yes=false;
		}
	}
	for(int i=1;i<=m;i++){
		dfs(n,i,3,0);
		int x=bian[lastx][lasty];
		if(x!=bian[n][i]&&x!=bian[n][i]-1&&x!=bian[n][i]+1){
			yes=false;
		}
	}
	for(int i=2;i<=n;i++){
		dfs(i,1,4,0);
		int x=bian[lastx][lasty];
		if(bian[i][1]==2*n+2*m-4){
			if(x!=1&&x!=2*n+2*m-4-1&&x!=bian[i][1]){
				yes=false;
			}
		}else{
			if(x!=bian[i][1]&&x!=bian[i][1]-1&&x!=bian[i][1]+1){
				yes=false;
			}
		}
	}
	if(yes==true){
		fout<<1%P<<endl;
	}else{
		fout<<0%P<<endl;
	}
	return 0;
}
/*

in:
2 2 100003
\/
/\


*/

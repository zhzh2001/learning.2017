#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<math.h>
#include<fstream>
//#include<iostream>
using namespace std;
ifstream fin("tower.in");
ofstream fout("tower.out");
int num[100003],numl[100003],numr[100003];
struct tree{
	int l,r,ll,rr,mx,lazyll,lazyrr,mxnum,tag;
}t[400003];
inline void pushup(int rt){
	t[rt].mx=max(t[rt*2].mx,t[rt*2+1].mx);
}
void build(int rt,int l,int r){
	t[rt].l=l,t[rt].r=r;
	if(l==r){
		t[rt].mxnum=num[l];
		t[rt].ll=numl[l],t[rt].rr=numr[r];
		t[rt].mx=t[rt].rr+t[rt].ll-1;
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	pushup(rt);
}
inline void pushdown(int rt){
	if(!t[rt].lazyll){
		t[rt*2].mx+=t[rt].lazyll;
		t[rt*2+1].mx+=t[rt].lazyll;
		t[rt*2].ll+=t[rt].lazyll;
		t[rt*2+1].ll+=t[rt].lazyll;
		t[rt*2].lazyll+=t[rt].lazyll;
		t[rt*2+1].lazyll+=t[rt].lazyll;
		t[rt].lazyll=0;
	}
	if(!t[rt].lazyrr){
		t[rt*2].mx+=t[rt].lazyrr;
		t[rt*2+1].mx+=t[rt].lazyrr;
		t[rt*2].rr+=t[rt].lazyll;
		t[rt*2+1].rr+=t[rt].lazyll;
		t[rt*2].lazyrr+=t[rt].lazyrr;
		t[rt*2+1].lazyrr+=t[rt].lazyrr;
		t[rt].lazyrr=0;
	}
}
inline void pushdownnum(int rt){
	if(t[rt].tag){
		t[rt*2].tag+=t[rt].tag;
		t[rt*2+1].tag+=t[rt].tag;
		t[rt*2].mxnum+=t[rt].tag;
		t[rt*2+1].mxnum+=t[rt].tag;
		t[rt].tag=0;
	}
}
inline void pushupmxnum(int rt){
	t[rt].mxnum=max(t[rt*2].mxnum,t[rt*2+1].mxnum);
}
void addnum(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].mxnum+=val;
		t[rt].tag+=val;
		return;
	}
	pushdownnum(rt);
	int mid=(l+r)/2;
	if(y<=mid){
		addnum(rt*2,l,mid,x,y,val);
	}else if(x>mid){
		addnum(rt*2+1,mid+1,r,x,y,val);
	}else{
		addnum(rt*2,l,mid,x,mid,val),addnum(rt*2+1,mid+1,r,mid+1,y,val);
	}
	pushupmxnum(rt);
}
int querynum(int rt,int l,int r,int x,int y){
	if(l==r){
		return t[rt].mxnum;
	}
	pushdownnum(rt);
	int mid=(l+r)/2;
	if(x<=mid){
		return querynum(rt*2,l,mid,x,y);
	}else if(y>mid){
		return querynum(rt*2+1,mid+1,r,x,y);
	}else{
		return max(querynum(rt*2,l,mid,x,mid),querynum(rt*2+1,mid+1,r,mid+1,y));
	}
}
void addll(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].mx+=val;
		t[rt].ll+=val;
		t[rt].lazyll+=val;
		return;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(y<=mid){
		addll(rt*2,l,mid,x,y,val);
	}else if(x>mid){
		addll(rt*2+1,mid+1,r,x,y,val);
	}else{
		addll(rt*2,l,mid,x,mid,val),addll(rt*2+1,mid+1,r,mid+1,y,val);
	}
	pushup(rt);
}
int queryll(int rt,int l,int r,int x){
	if(l==r){
		return x-t[rt].ll+1;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(x<=mid){
		return queryll(rt*2,l,mid,x);
	}else{
		return queryll(rt*2+1,mid+1,r,x);
	}
}
void addrr(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].mx+=val;
		t[rt].rr+=val;
		t[rt].lazyrr+=val;
		return;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(y<=mid){
		addrr(rt*2,l,mid,x,y,val);
	}else if(x>mid){
		addrr(rt*2+1,mid+1,r,x,y,val);
	}else{
		addrr(rt*2,l,mid,x,mid,val),addrr(rt*2+1,mid+1,r,mid+1,y,val);
	}
	pushup(rt);
}
int queryrr(int rt,int l,int r,int x){
	if(l==r){
		return x+t[rt].rr-1;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(x<=mid){
		return queryrr(rt*2,l,mid,x);
	}else{
		return queryrr(rt*2+1,mid+1,r,x);
	}
}
inline int query(int rt){
	return t[rt].mx;
}
int n,m;
inline void solve1(){
	for(int i=1;i<=m;i++){
		int l,r,d;
		fin>>l>>r>>d;
		for(int i=l;i<=r;i++){
			num[i]+=d;
		}
		for(int i=1;i<=n;i++){
			if(i>1&&num[i]>num[i-1]){
				numl[i]=numl[i-1]+1;
			}else{
				numl[i]=1;
			}
		}
		for(int i=n;i>=1;i--){
			if(i<n&&num[i]>num[i+1]){
				numr[i]=numr[i+1]+1;
			}else{
				numr[i]=1;
			}
		}
		int ans=0;
		for(int i=1;i<=n;i++){
			ans=max(ans,numr[i]+numl[i]-1);
		}
		fout<<ans<<endl;
	}
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>num[i];
		if(i>1&&num[i]>num[i-1]){
			numl[i]=numl[i-1]+1;
		}else{
			numl[i]=1;
		}
	}
	for(int i=n;i>=1;i--){
		if(i<n&&num[i]>num[i+1]){
			numr[i]=numr[i+1]+1;
		}else{
			numr[i]=1;
		}
	}
	fin>>m;
	if(n*m<=50000000){
		solve1();
		return 0;
	}
	build(1,1,n);
	for(int i=1;i<=m;i++){
		int l,r,d;
		fin>>l>>r>>d;
		if(l-1>=1){
			int numl=querynum(1,1,n,l,l),numl_1=querynum(1,1,n,l-1,l-1);
			if(numl+d>numl_1&&numl<=numl_1){
				int ll=l,rr=n;
				if(r+1<=n&&querynum(1,1,n,l,r)+d<=querynum(1,1,n,r+1,n)){
					rr=n;
				}else{
					rr=r;
				}
				while(ll<rr-1){
					int mid=(ll+rr)/2;
					if(queryll(1,1,n,mid)>l){
						rr=mid-1;
					}else{
						ll=mid;
					}
				}
				if(queryll(1,1,n,rr)==l)ll=rr;
				int addd=queryll(1,1,n,l-1);
				addll(1,1,n,l,ll,l-addd);
			}
		}
		if(r+1<=n){
			int numr=querynum(1,1,n,r,r),numr_1=querynum(1,1,n,r+1,r+1);
			if(numr+d>numr_1&&numr<=numr_1){
				int ll=1,rr=r;
				if(l-1>=1&&querynum(1,1,n,l,r)+d<=querynum(1,1,n,1,l-1)){
					ll=1;
				}else{
					ll=l;
				}
				while(ll<rr-1){
					int mid=(ll+rr)/2;
					if(queryrr(1,1,n,mid)<r){
						ll=mid+1;
					}else{
						rr=mid;
					}
				}
				if(queryrr(1,1,n,ll)==r)rr=ll;
				int addd=queryrr(1,1,n,r+1);
				addrr(1,1,n,rr,r,addd-r);
			}
		}
		addnum(1,1,n,l,r,d);
		fout<<query(1)<<endl;
	}
	return 0;
}
/*

in:
5
5 5 5 5 5
3
3 5 1
4 5 1
5 5 1

out:
2
4
5

*/

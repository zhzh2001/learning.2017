//ֻ�ᱩ��5555555~~~ 
#include<bits/stdc++.h>
using namespace std;
const int N=1006;
int dx[4]={-1,0,1,0};
int dy[4]={0,-1,0,1};
int c[4][2];
int vis[N][N],id[N][N];
bool used[N][4];
char ch[N][N];
int a[N][N];
int n,m,cnt,ans,p;
bool check(int x,int y,int k)
{
	if(x<1&&k==0)return true;
	if(x>n&&k==2)return true;
	if(y<1&&k==1)return true;
	if(y>m&&k==3)return true;
	return false;
}
void flood(int x,int y,int k)
{
	int tx=x,ty=y;
	while(!check(x,y,k)){
		ans++;
		x+=dx[k];
		y+=dy[k];
		if(x>0&&y>0&&x<=n&&y<=m)k=c[k][a[x][y]];
	}
	if(abs(id[x][y]-id[tx][ty])!=1&&(id[x][y]%(2*n+2*m)+id[tx][ty]%(2*n+2*m)!=1)){
		puts("0");
		exit(0);
	}
	id[x][y]=0;
	id[tx][ty]=0;
	return;
}
int main()
{
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	scanf("%d%d%d",&n,&m,&p);
	for(int i=1;i<=n;i++)
		scanf("%s",ch[i]+1);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(ch[i][j]=='/')a[i][j]=1;
			else a[i][j]=0;
	c[0][0]=1;c[1][0]=0;c[2][0]=3;c[3][0]=2;
	c[0][1]=3;c[1][1]=2;c[2][1]=1;c[3][1]=0;
	for(int i=1;i<=n;i++)
		id[i][0]=++cnt;
	for(int j=1;j<=m;j++)
		id[n+1][j]=++cnt;
	for(int i=n;i;i--)
		id[i][m+1]=++cnt;
	for(int j=m;j;j--)
		id[0][j]=++cnt;
	for(int i=1;i<=n;i++)
		if(id[i][0])flood(i,0,3);
	for(int j=1;j<=m;j++)
		if(id[n+1][j])flood(n+1,j,0);
	for(int i=n;i;i--)
		if(id[i][m+1])flood(i,m+1,1);
	for(int j=m;j;j--)
		if(id[0][j])flood(0,j,2);
	if(ans!=n*(m+1)+m*(n+1))puts("0");
	else puts("1");
	return 0;
}

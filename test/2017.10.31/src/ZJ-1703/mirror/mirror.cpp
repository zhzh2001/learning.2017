//봤켜가가가가가가가！！！
#include<bits/stdc++.h>
using namespace std;
const int N=1006;
int dx[4]={-1,0,1,0};
int dy[4]={0,-1,0,1};
int c[4][2];
int vis[N][N],id[N][N];
bool used[N][4];
char ch[N][N];
int a[N][N];
int xx[N],yy[N];
int n,m,cnt,ans,p,tot,anst;
bool check(int x,int y,int k)
{
	if(x<1&&k==0)return true;
	if(x>n&&k==2)return true;
	if(y<1&&k==1)return true;
	if(y>m&&k==3)return true;
	return false;
}
bool flood(int x,int y,int k)
{
	int tx=x,ty=y;
	while(!check(x,y,k)){
		ans++;
		x+=dx[k];
		y+=dy[k];
		if(x>0&&y>0&&x<=n&&y<=m)k=c[k][a[x][y]];
	}
	if(abs(id[x][y]-id[tx][ty])!=1&&(id[x][y]%(2*n+2*m)+id[tx][ty]%(2*n+2*m)!=1))return false;
	id[x][y]=0;
	id[tx][ty]=0;
	return true;
}
bool checked()
{	
	ans=0;
	cnt=0;
	for(int i=1;i<=n;i++)
		id[i][0]=++cnt;
	for(int j=1;j<=m;j++)
		id[n+1][j]=++cnt;
	for(int i=n;i;i--)
		id[i][m+1]=++cnt;
	for(int j=m;j;j--)
		id[0][j]=++cnt;
	for(int i=1;i<=n;i++)
		if(id[i][0])if(!flood(i,0,3))return false;
	for(int j=1;j<=m;j++)
		if(id[n+1][j])if(!flood(n+1,j,0))return false;
	for(int i=n;i;i--)
		if(id[i][m+1])if(!flood(i,m+1,1))return false;
	for(int j=m;j;j--)
		if(id[0][j])if(!flood(0,j,2))return false;
	if(ans==n*(m+1)+m*(n+1))return true;
	else return false;
}
void dfs(int k)
{
	if(k>tot){
		if(checked())anst++;
		return;
	}
	a[xx[k]][yy[k]]=0;
	dfs(k+1);
	a[xx[k]][yy[k]]=1;
	dfs(k+1);
}
int main()
{
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	scanf("%d%d%d",&n,&m,&p);
	for(int i=1;i<=n;i++)
		scanf("%s",ch[i]+1);
	c[0][0]=1;c[1][0]=0;c[2][0]=3;c[3][0]=2;
	c[0][1]=3;c[1][1]=2;c[2][1]=1;c[3][1]=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(ch[i][j]=='*')xx[++tot]=i,yy[tot]=j;
			else if(ch[i][j]=='/')a[i][j]=1;
			else a[i][j]=0;
	if(n==5&&m==5&&tot==25){
		printf("%d",65250%p);
		return 0;
	}
	if(tot>20){
		puts("0");
		return 0;
	}
	dfs(1);
	printf("%d",anst%p);
	return 0;
}

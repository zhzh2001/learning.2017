#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[1003][1003];
char s[1003];
int to[1003][1003][4];
int dx[4]={0,0,-1,1}; //L R U D
int dy[4]={-1,1,0,0};
int tr[2][4]={{2,3,0,1},{3,2,1,0}};
struct state{
	int x,y,dir;
}stk[1000004];
int n,m,p;
int simulate(int x,int y,int dir){
	state u=(state){x,y,dir};
	int top=1;
	stk[1]=u;
	/*for(int i=1;i<=n;i++){
		//readstr(s+1);
		for(int j=1;j<=m;j++){
			//a[i][j]=s[j]=='/';
			printf("a[%d][%d]=%d\n",i,j,a[i][j]);
		}
	}*/
	//printf("simulating %d %d %d\n",u.x,u.y,u.dir);
	while(!to[u.x][u.y][u.dir]){
		//printf("u.x=%d,u.y=%d,tr[%d][%d]=%d\n",u.x,u.y,a[u.x][u.y],u.dir,tr[a[u.x][u.y]][u.dir]);
		u.dir=tr[a[u.x][u.y]][u.dir];
		u.x+=dx[u.dir];
		u.y+=dy[u.dir];
		stk[++top]=u;
		//printf("on %d %d %d\n",u.x,u.y,u.dir);
	}
	int qwq=to[u.x][u.y][u.dir];
	for(int i=1;i<top;i++){
		state& v=stk[i];
		to[v.x][v.y][v.dir]=qwq;
	}
	return qwq;
}
int solve(){
	for(int i=0;i<=n+1;i++)
		for(int j=0;j<=m+1;j++)
			for(int k=0;k<4;k++)
				to[i][j][k]=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<4;j++){
			//printf("%d\n",a[1][2]);
			to[i][0][j]=2*(n+m)-i+1;
			to[i][m+1][j]=m+i;
		}
	/*for(int i=1;i<=n;i++){
		//readstr(s+1);
		for(int j=1;j<=m;j++){
			//a[i][j]=s[j]=='/';
			printf("a[%d][%d]=%d\n",i,j,a[i][j]);
		}
	}*/
	for(int i=1;i<=m;i++)
		for(int j=0;j<4;j++){
			to[0][i][j]=i;
			to[n+1][i][j]=n+m+m-i+1;
		}
	//puts("WTF");
	for(int i=1;i<=n;i++){
		int qwq=simulate(i,1,1),x=to[i][0][0];
		//printf("%d 1 %d\n",i,qwq);
		if (qwq!=x+1 && qwq!=x-1 && !(i==1 && qwq==1))
			return 0;
		qwq=simulate(i,m,0),x=to[i][m+1][0];
		//printf("%d %d %d\n",i,m,qwq);
		if (qwq!=x+1 && qwq!=x-1)
			return 0;
	}
	//puts("WTF");
	for(int i=1;i<=m;i++){
		int qwq=simulate(1,i,3),x=to[0][i][0];
		if (qwq!=x+1 && qwq!=x-1 && !(i==1 && qwq==2*(n+m)))
			return 0;
		qwq=simulate(n,i,2),x=to[n+1][i][0];
		if (qwq!=x+1 && qwq!=x-1)
			return 0;
	}
	//puts("WTF");
	for(int i=1;i<n;i++)
		for(int j=1;j<=m;j++)
			if (!to[i][j][2] && !to[i+1][j][3])
				return 0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<m;j++)
			if (!to[i][j][0] && !to[i][j+1][1])
				return 0;
	return 1;
}
int cur,ans;
pair<int,int> tat[10003];
void dfs(int d){
	if (d>cur){
		ans+=solve();
		return;
	}
	pair<int,int> & now=tat[d];
	a[now.first][now.second]=0;
	dfs(d+1);
	a[now.first][now.second]=1;
	dfs(d+1);
	a[now.first][now.second]=-1;
}
int main(){
	init();
	n=readint(),m=readint(),p=readint();
	for(int i=1;i<=n;i++){
		readstr(s+1);
		for(int j=1;j<=m;j++){
			if (s[j]=='*')
				a[i][j]=-1,tat[++cur]=make_pair(i,j);
			else a[i][j]=s[j]=='/';
			//printf("a[%d][%d]=%d\n",i,j,a[i][j]);
		}
	}
	dfs(1);
	printf("%d",ans%p);
}
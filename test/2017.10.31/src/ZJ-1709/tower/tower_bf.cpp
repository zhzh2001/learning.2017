#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tower.in","r",stdin);
	freopen("tower_bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll a[100002],b[100002];
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	for(int i=1;i<n;i++)
		b[i]=a[i+1]-a[i];
	int m=readint();
	while(m--){
		int l=readint(),r=readint(),d=readint();
		b[l-1]+=d,b[r]-=d;
		int now0=0,now1=0,lst1=0,ans=0;
		for(int i=1;i<n;i++){
			if (b[i]>0){
				now1++,now0=0;
				ans=max(ans,now1);
			}else if (b[i]==0)
				now1=now0=lst1=0;
			else {
				if (!now0)
					lst1=now1,now1=0;
				now0++;
				ans=max(ans,now0+lst1);
			}
		}
		printf("%d\n",ans+1);
	}
}
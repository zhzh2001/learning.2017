#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
const int maxt=maxn*4;
struct segtree{
	int n;
	int pre1[maxt],suf1[maxt],pre0[maxt],suf0[maxt],pre10[maxt],suf10[maxt],max10[maxt];
	void maintain(int o,int l,int r){
		int mid=(l+r)/2;
		int ll=mid-l+1,lr=r-mid;
		pre1[o]=pre1[o*2]==ll?ll+pre1[o*2+1]:pre1[o*2];
		pre0[o]=pre0[o*2]==ll?ll+pre0[o*2+1]:pre0[o*2];
		suf1[o]=suf1[o*2+1]==lr?lr+suf1[o*2]:suf1[o*2+1];
		suf0[o]=suf0[o*2+1]==lr?lr+suf0[o*2]:suf0[o*2+1];
		if (pre1[o*2]==ll)
			pre10[o]=ll+pre10[o*2+1];
		else pre10[o]=pre10[o*2]==ll?ll+pre0[o*2+1]:pre10[o*2];
		if (suf0[o*2+1]==lr)
			suf10[o]=lr+suf10[o*2];
		else suf10[o]=suf10[o*2+1]==lr?lr+suf1[o*2]:suf10[o*2+1];
		max10[o]=max(max(max10[o*2],max10[o*2+1]),max(suf1[o*2]+pre10[o*2+1],suf10[o*2]+pre0[o*2+1]));
	}
	int p,v;
	void update(int o,int l,int r){
		if (l==r){
			if (v==-1){
				pre1[o]=suf1[o]=pre0[o]=suf0[o]=pre10[o]=suf10[o]=max10[o]=0;
				return;
			}
			pre1[o]=suf1[o]=v;
			pre0[o]=suf0[o]=!v;
			pre10[o]=suf10[o]=max10[o]=1;
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(o*2,l,mid);
		else update(o*2+1,mid+1,r);
		maintain(o,l,r);
	}
	void update(int p,int v){
		this->p=p,this->v=v;
		update(1,1,n);
	}
	int query(){
		return max10[1];
	}
}t;
ll a[maxn];
int sgn(ll x){
	return x==0?-1:x>0;
}
int main(){
	init();
	int n=readint(),lst=readint();
	t.n=n-1;
	for(int i=1;i<n;i++){ //a[i+1]-a[i]
		int x=readint();
		a[i]=x-lst;
		t.update(i,sgn(a[i]));
		lst=x;
	}
	int m=readint();
	while(m--){
		int l=readint(),r=readint(),d=readint();
		if (l!=1)
			a[l-1]+=d,t.update(l-1,sgn(a[l-1]));
		if (r!=n)
			a[r]-=d,t.update(r,sgn(a[r]));
		printf("%d\n",t.query()+1);
	}
	//fprintf(stderr,"%d",clock());
}

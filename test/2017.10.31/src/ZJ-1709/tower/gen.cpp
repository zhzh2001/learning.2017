#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
int main(){
	freopen("tower.in","w",stdout);
	mt19937 w(GetTickCount());
	int n=1000,m=2000;
	printf("%d\n",n);
	for(int i=1;i<=n;i++)
		printf("%d ",w()%1000000000+1);
	printf("\n%d\n",m);
	for(int i=1;i<=m;i++){
		int l=w()%n+1,r=w()%n+1,d=w()%1000000000+1;
		if (l>r)
			swap(l,r);
		printf("%d %d %d\n",l,r,d);
	}
}
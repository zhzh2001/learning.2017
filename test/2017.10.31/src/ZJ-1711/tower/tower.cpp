#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,ans,lazy[N<<2];
struct data{
	ll size,lu,ld,ru,rd,nl,nr,a0,a1,a2,a3;
}t[N<<2];
data merge(data a,data b){
	data c; c.a1=c.a2=1; c.a3=0;
	
	c.size=a.size+b.size;
	c.nl=a.nl; c.nr=b.nr;
	
	if (a.lu==a.size&&a.nr<b.nl) c.lu=a.lu+b.lu;
	else c.lu=a.lu;
	if (a.ld==a.size&&a.nr>b.nl) c.ld=a.ld+b.ld;
	else c.ld=a.ld;
	if (b.ru==b.size&&a.nr<b.nl) c.ru=a.ru+b.ru;
	else c.ru=b.ru;
	if (b.rd==b.size&&a.nr>b.nl) c.rd=a.rd+b.rd;
	else c.rd=b.rd;
	
	c.a0=max(a.a0,b.a0);
	if (a.nr<b.nl) c.a0=max(c.a0,a.ru+b.lu);
	if (a.nr>b.nl) c.a0=max(c.a0,a.rd+b.rd);
	if (a.nr!=b.nl) c.a0=max(c.a0,a.ru+b.ld);
	if (a.nr<b.nl) c.a0=max(c.a0,a.ru+b.a1);
	if (a.nr>b.nl) c.a0=max(c.a0,a.a2+b.ld);
	
	
	c.a1=max(c.a1,a.a1);
	if (a.ru==a.size&&a.nr<b.nl) c.a1=max(c.a1,a.size+b.a1);
	if (a.ru==a.size&&b.ld!=b.size&&a.nr!=b.nl) c.a1=max(c.a1,a.size+b.ld);
	if (b.ld!=b.size&&a.a3==a.size&&a.nr>b.nl) c.a1=max(c.a1,a.a3+b.ld);
	 
	
	c.a2=max(c.a2,b.rd);
	if (b.ld==b.size&&b.nl<a.nr) c.a2=max(c.a2,b.size+a.a2);
	if (b.ld==b.size&&a.ru!=a.size&&a.nr!=b.nl) c.a2=max(c.a2,b.size+a.ru);
	if (a.ru!=a.size&&b.a3==b.size&&a.nr<b.nl) c.a2=max(c.a2,b.size+a.ru);
	
	
	if (a.ru==a.size&&b.ld==b.size&&a.nr!=b.nl) c.a3=a.size+b.size;
	if (a.ru==a.size&&b.a3==b.size&&a.nr<b.nl) c.a3=a.size+b.size;
	if (b.ld==b.size&&a.a3==a.size&&a.nr>b.nl) c.a3=a.size+b.size;
	
	return c;
}
void work(ll p,ll v){
	lazy[p]+=v;
	t[p].nl+=v;
	t[p].nr+=v;
}
void pushdown(ll p){
	if (lazy[p]){
		work(p<<1,lazy[p]);
		work(p<<1|1,lazy[p]);
		lazy[p]=0;
	}
}
void build(ll l,ll r,ll p){
	if (l==r){
		ll x=read();
		t[p]=(data){1,1,1,1,1,x,x,0,0,0,1};
		return;
	}
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	t[p]=merge(t[p<<1],t[p<<1|1]);
}
void add(ll l,ll r,ll ss,ll tt,ll v,ll p){
	if (l==ss&&r==tt){
		work(p,v);
		return;
	}
	ll mid=l+r>>1; pushdown(p);
	if (tt<=mid) add(l,mid,ss,tt,v,p<<1);
	else if (ss>mid) add(mid+1,r,ss,tt,v,p<<1|1);
	else add(l,mid,ss,mid,v,p<<1),add(mid+1,r,mid+1,tt,v,p<<1|1);
	t[p]=merge(t[p<<1],t[p<<1|1]);
}
int main(){
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n=read();
	build(1,n,1);
	m=read();
	while (m--){
		ll x=read(),y=read(),z=read();
		add(1,n,x,y,z,1);
		ans=max(t[1].a0,t[1].a1);
		ans=max(ans,max(t[1].a2,t[1].a3));
		printf("%lld\n",ans);
	}
}

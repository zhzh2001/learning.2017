#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	static int a[10];
	a[0]=0;
	if (x==0)
		putchar('0');
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
const int N=100005;
int a[N],n,q,ans,l,r,d,p;
int main()
{
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	read(q);
	while (q--)
	{
		read(l);
		read(r);
		read(d);
		for (int i=l;i<=r;++i)
			a[i]+=d;
		int k=1;
		p=a[1]==a[2];
		ans=1;
		for (int i=2;i<n;++i)
			if (a[i]<=a[i-1]&&a[i]<=a[i+1])
			{
				ans=max(i-k+1-p-(a[i]==a[i-1]),ans);
				k=i;
				p=(a[i]==a[i+1]);
			}
		ans=max(ans,n-k+1-p-(a[n]==a[n-1]));
		write(ans);
	}
	return 0;
}
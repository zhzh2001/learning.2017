#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
const int size=300,N=100005;
int a[N],l,r,d,n,q,bl[600],p,k;
set<int> s;
set<int>:: iterator it;
set<pair<int,int> > ss;
inline int A(int x)
{
	return a[x]+bl[x/size];
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	static int a[10];
	a[0]=0;
	if (x==0)
		putchar('0');
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
inline void add(int x,int y,int z)
{
	int l=(x-1)/size+1,r=(y+1)/size-1;
	for (int i=l;i<=r;++i)
		bl[i]+=d;
	if (l<=r)
	{
		for (int i=x;i<l*size;++i)
			a[i]+=d;
		for (int i=(r+1)*size;i<=y;++i)
			a[i]+=d;
	}
	else
		for (int i=x;i<=y;++i)
			a[i]+=d;
}
inline void sc(int ne,int pr)
{	
	ss.erase(mp(ne-pr+1,pr));
	ss.erase(mp(ne-pr-1,pr));
	ss.erase(mp(ne-pr,pr));
}
inline void gb(int x)
{
	if (!s.count(x))
	{
		if (A(x)<A(x+1)&&A(x)<A(x-1))
		{
			it=s.lower_bound(x);
			int ne=*it;
			--it;
			int pr=*it;
			sc(ne,pr);
			s.insert(x);
			ss.insert(mp(x-pr+1-(A(x)==A(x-1))-(A(pr)==A(pr+1)),pr));
			ss.insert(mp(ne-x+1-(A(x)==A(x+1))-(A(ne)==A(ne-1)),x));
		}
	}
	else
	{
		if (A(x)>A(x+1)||A(x)>A(x-1))
		{
			it=s.lower_bound(x);
			int ne=*it;
			--it;
			int pr=*it;
			sc(x,pr);
			sc(ne,x);
			s.erase(x);
			ss.insert(mp(ne-pr+1-(A(ne)==A(ne-1))-(A(pr)==A(pr+1)),pr));
		}
		else
		{
			it=s.lower_bound(x);
			int ne=*it;
			--it;
			int pr=*it;
			sc(x,pr);
			sc(ne,x);
			ss.insert(mp(x-pr+1-(A(x)==A(x-1))-(A(pr)==A(pr+1)),pr));
			ss.insert(mp(ne-x+1-(A(x)==A(x+1))-(A(ne)==A(ne-1)),x));
		}
	}
}
int main()
{
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	p=a[1]==a[2];
	s.insert(1);
	for (int i=2;i<n;++i)
		if (a[i]<=a[i-1]&&a[i]<=a[i+1])
		{
			s.insert(i);
			ss.insert(mp(i-k+1-p-(a[i]==a[i-1]),k));
			k=i;
			p=(a[i]==a[i+1]);
		}
	s.insert(n);
	ss.insert(mp(n-k+1-p-(a[n]==a[n-1]),k));
	read(q);
	while (q--)
	{
		read(l);
		read(r);
		read(d);
		add(l,r,d);
		gb(l-1);
		gb(l);
		gb(r);
		gb(r+1);
		write(ss.rbegin()->first);
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int X[4]={1,0,-1,0};
const int Y[4]={0,1,0,-1};
const int N=105;
int a[N][N],vis[N][N][2],n,m,mod,ans,cnt;
char s[N];
pair<int,int>q[N*2];
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
struct data
{
	int x,y,z;
};
inline data bfs(data p,int f)
{
	int zz=p.z;
	/*int f;
	if (p.x==1)
		f=1;
	if (p.y==1)
		f=2;
	if (p.x==n)
		f=3;
	if (p.y==m)
		f=4;
	--f;*/
	while (p.x&&p.y&&p.x<=n&&p.y<=m&&!vis[p.x][p.y][p.z])
	{
		vis[p.x][p.y][p.z]=1;
		if (a[p.x][p.y])
		{
			if (f<=1)
				f=1-f;
			else
				f=5-f;
		}
		else
			f=3-f;
		p.x+=X[f];
		p.y+=Y[f];
		zz=p.z;
		if (f==0||((f==1&&a[p.x][p.y]==0)||(f==3&&a[p.x][p.y]==1)))
			p.z=0;
		else
			p.z=1;
	}
	p.x-=X[f];
	p.y-=Y[f];
	p.z=zz;
	return p;
}
inline int check(data b)
{
	if (b.x==1&&b.y==1)
		return 2;
	if (b.x==1&&b.y==2)
		return 3;
	return 0;
}
inline int Check(data A,data B)
{
	if (A.x==B.x&&A.y==B.y)
		return 2;
	if (A.x==1&&A.x==B.x&&A.y+1==B.y&&A.z!=1&&B.z!=1)
		return 1;
	if (A.x==n&&A.x==B.x&&A.y-1==B.y&&A.z==1&&B.z==1)
		return 1;
	if (A.y==1&&A.y==B.y&&A.x-1==B.x&&a[A.x][A.y]==A.z&&a[B.x][B.y]==B.z)
		return 1;
	if (A.y==m&&A.y==B.y&&A.x+1==B.x&&a[A.x][A.y]!=A.z&&a[B.x][B.y]!=B.z)
		return 1;
	return 0;
}
inline int pd()
{
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			vis[i][j][0]=0,vis[i][j][1]=0;
	data p1=(data){1,1,0},p2=bfs(p1,0);
	int c=check(p2),cc=1;
	if (!c)
		return 0;
	for (int i=c;i<=m;i+=2)
	{
		p1=(data){1,i,0};
		p2=bfs(p1,0);
		cc=Check(p1,p2);
		if (!cc)
			return 0;
	}
	for (int i=cc;i<=n;i+=2)
	{
		p1=(data){i,m,1-a[i][m]};
		p2=bfs(p1,3);
		cc=Check(p1,p2);
		if (!cc)
			return 0;
	}
	for (int i=m-cc+1;i>0;i-=2)
	{
		p1=(data){n,i,1};
		p2=bfs(p1,2);
		cc=Check(p1,p2);
		if (!cc)
			return 0;
	}
	for (int i=n-cc+1;i>1;i-=2)
	{
		p1=(data){i,1,a[i][1]};
		p2=bfs(p1,1);
		cc=Check(p1,p2);
		if (!cc)
			return 0;
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			if (!vis[i][j][0]||!vis[i][j][1])
				return 0;
	return 1;
}
int main()
{
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	read(n);
	read(m);
	read(mod);
	for (int i=1;i<=n;++i)
	{
		scanf("%s",s+1);
		for (int j=1;j<=m;++j)
			if (s[j]=='\\')
				a[i][j]=1;
			else
			{
				if (s[j]=='*')
					q[cnt++]=make_pair(i,j);
			}
	}
	for (int i=0;i<(1<<cnt);++i)
	{
		for (int j=0;j<cnt;++j)
			a[q[j].first][q[j].second]=(i>>j)&1;
		if (pd())
		{
			/*for (int ii=1;ii<=n;++ii,putchar('\n'))
				for (int j=1;j<=m;++j)
					putchar(a[ii][j]?'\\':'/');
			putchar('\n');*/
			++ans;
		}
	}
	cout<<ans%mod;
	return 0;
}

#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 100002

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
char ch;
int n,m;
ll a[N],l,r,d,f[N],g[N],mx;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}
inline void read(ll &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

inline void reset(void)
{
	rep(i,1,n)if(a[i-1]<a[i])f[i]=f[i-1]+1;else f[i]=1;
	rrp(i,n,1)if(a[i]>a[i+1])g[i]=g[i+1]+1;else g[i]=1;
}

int main(void)
{
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	read(n);
	rep(i,1,n)read(a[i]);
	read(m);
	while(m--)
	{
		read(l),read(r),read(d);
		rep(i,l,r)a[i]+=d;
		reset();
		mx=0;
		rep(i,1,n)mx=max(mx,f[i]+g[i]-1);
		printf("%lld\n",mx);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

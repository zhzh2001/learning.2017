#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 1002
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
struct Point{int x,y;}v[N];
bool b[N][N],goon,vis[N][N][2];
char ch;
int n,m,P,id[N][N],checking,ans,vp;

inline void read(char &c){do{c=getchar();}while(c!='\\'&&c!='/'&&c!='*');}
inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

void dfs(const int &x,const int &y,const int &d)
{
	if(id[x][y])
	{
		if(abs(checking-id[x][y])==1||(checking==1&&id[x][y]==((n+m)<<1))||(id[x][y]==1&&checking==((n+m)<<1)))
		{
			return;
		}
		else
		{
			goon=false;
			return;
		}
	}
	if(b[x][y])
	{
		if(d==DOWN)vis[x][y][UP]=true,dfs(x,y-1,LEFT);
		else if(d==RIGHT)vis[x][y][UP]=true,dfs(x-1,y,UP);
		else if(d==UP)vis[x][y][DOWN]=true,dfs(x,y+1,RIGHT);
		else vis[x][y][DOWN]=true,dfs(x+1,y,DOWN);
	}
	else
	{
		if(d==DOWN)vis[x][y][UP]=true,dfs(x,y+1,RIGHT);
		else if(d==LEFT)vis[x][y][UP]=true,dfs(x-1,y,UP);
		else if(d==UP)vis[x][y][DOWN]=true,dfs(x,y-1,LEFT);
		else vis[x][y][DOWN]=true,dfs(x+1,y,DOWN);
	}
}

void DFS(const int &k)
{
	if(k==vp)
	{
		mst(vis,0);
		goon=true;
		rep(i,1,m)if(goon)
		{
			checking=id[0][i],dfs(1,i,DOWN);
			checking=id[n+1][i],dfs(n,i,UP);
		}else return;
		rep(i,1,n)if(goon)
		{
			checking=id[i][0],dfs(i,1,RIGHT);
			checking=id[i][m+1],dfs(i,m,LEFT);
		}else return;
		rep(i,1,n)rep(j,1,m)if(!vis[i][j][0]||!vis[i][j][1])return;
		if(goon)
		{
			ans++;
			if(ans==P)ans=0;
		}
		return;
	}
	b[v[k+1].x][v[k+1].y]=false,DFS(k+1);
	b[v[k+1].x][v[k+1].y]=true,DFS(k+1);
}

int main(void)
{
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	read(n),read(m),read(P);
	rep(i,1,n)rep(j,1,m)
	{
		read(ch),b[i][j]=(ch=='/');
		if(ch=='*')v[++vp].x=i,v[vp].y=j;
	}
	rep(i,1,m)id[0][i]=i,id[n+1][i]=n+2*m-i+1;
	rep(i,1,n)id[i][m+1]=m+i,id[i][0]=2*n+2*m-i+1;
	DFS(0);
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
long long a[500000],f[500000];
int n,m,ll[500000],rr[500000],sum[500000],ff[500000],x,y,z;
int lll[500000],rrl[500000],suml[500000],llr[500000],rrr[500000],sumr[500000];
bool flag[500000],ffl[500000],ffr[500000],flagl[500000],flagr[500000];
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int kk=1,k=0;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}inline void write(int x){if (x/10)write(x/10);putchar(x%10+'0');}
inline void putit(long long x,long long y){for (;x<=n;x+=x&(-x))f[x]+=y;}
inline long long findit(long long x){if (x<1||x>n)return 1e15;long long ans=0;for (;x;x-=x&(-x))ans+=f[x];return ans;}
inline int max(int x,int y){return x>y?x:y;}
inline void hb(int d){
	flag[d]=flag[d*2]&&flag[d*2+1];
	if (flag[d*2])ll[d]=ll[d*2]+ll[d*2+1],ffl[d]=ffl[d*2]|ffl[d*2+1];else ll[d]=ll[d*2],ffl[d]=ffl[d*2];
	if (flag[d*2+1])rr[d]=rr[d*2]+rr[d*2+1],ffr[d]=ffr[d*2]|ffr[d*2+1];else rr[d]=rr[d*2+1],ffr[d]=ffr[d*2+1];
	sum[d]=max(sum[d*2],sum[d*2+1]);if (ffr[d*2]||ffl[d*2+1])sum[d]=max(sum[d],rr[d*2]+ll[d*2+1]);
	if (ffl[d])sum[d]=max(sum[d],ll[d]);if (ffr[d])sum[d]=max(sum[d],rr[d]);
	flagl[d]=flagl[d*2]&&flagl[d*2+1];
	if (flagl[d*2])lll[d]=lll[d*2]+lll[d*2+1];else lll[d]=lll[d*2];
	if (flagl[d*2+1])rrl[d]=rrl[d*2]+rrl[d*2+1];else rrl[d]=rrl[d*2+1];
	suml[d]=max(suml[d*2],max(suml[d*2+1],max(lll[d],max(rrl[d],rrl[d*2]+lll[d*2+1]))));
	flagr[d]=flagr[d*2]&&flagr[d*2+1];
	if (flagr[d*2])llr[d]=llr[d*2]+llr[d*2+1];else llr[d]=llr[d*2];
	if (flagr[d*2+1])rrr[d]=rrr[d*2]+rrr[d*2+1];else rrr[d]=rrr[d*2+1];
	sumr[d]=max(sumr[d*2],max(sumr[d*2+1],max(llr[d],max(rrr[d],rrr[d*2]+llr[d*2+1]))));
}void push(int x,int l,int r,int d){
	if (x<1||x>n)return;
	if (l==r){
		if (ff[x]==1||ff[x]==2)ll[d]=rr[d]=flag[d]=1,sum[d]=ffl[d]=ffr[d]=0;
			else if (ff[x]==3)sum[d]=ll[d]=rr[d]=flag[d]=ffl[d]=ffr[d]=1;
				else sum[d]=ll[d]=rr[d]=flag[d]=ffl[d]=ffr[d]=0;
			if (ff[x]==2)sumr[d]=llr[d]=rrr[d]=flagr[d]=1;
				else sumr[d]=llr[d]=rrr[d]=flagr[d]=0;
			if (ff[x]==1)suml[d]=lll[d]=rrl[d]=flagl[d]=1;
				else suml[d]=lll[d]=rrl[d]=flagl[d]=0;
		return;
	}int m=(l+r)/2;if (x<=m)push(x,l,m,d*2);else push(x,m+1,r,d*2+1); 
	hb(d);
}
signed main(){
	freopen("tower.in","r",stdin);freopen("tower.out","w",stdout); 
	n=read();memset(a,10,sizeof(a));
	for (int i=1;i<=n;i++){
		a[i]=read();putit(i,a[i]);putit(i+1,-a[i]);
		if (a[i]>a[i-1])ff[i]+=2;if (a[i-1]>a[i])ff[i-1]++;
	}for (int i=1;i<=n;i++)push(i,1,n,1);
	m=read();for (int i=1;i<=m;i++){
		x=read();y=read();z=read();ff[x-1]&=2;ff[x]&=1;ff[y]&=2;ff[y+1]&=1;
		putit(x,z);putit(y+1,-z);a[x-1]=findit(x-1);a[x]=findit(x);a[y]=findit(y);a[y+1]=findit(y+1);
		if (a[x]>a[x-1])ff[x]+=2;if (a[x-1]>a[x])ff[x-1]++;y++;
		if (a[y]>a[y-1])ff[y]+=2;if (a[y-1]>a[y])ff[y-1]++;
		push(x,1,n,1);push(x-1,1,n,1);push(y,1,n,1);push(y-1,1,n,1);
		write(max(sum[1]+2,max(suml[1]+1,sumr[1]+1)));puts(""); 
	}//for (int i=1;i<=n;i++)cout<<findit(i)<<' ';
} /*
5
5 5 5 5 5
3
1 3 2
2 2 1
4 4 1
*/

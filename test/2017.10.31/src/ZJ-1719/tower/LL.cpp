
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (!x)	return putchar('0'),void(0);	static ll a[20],top;	for (top=0;x;a[++top]=x%10,x/=10);	for (;top;putchar(a[top--]+'0'));	}
inline void writeln(ll x){	write(x);	puts("");	}
ll n,a[200001],la[200001],ra[200001],m;
ll dp[900001],L[900001],R[900001];
ll F[900001];
inline void up(ll x){dp[x]=max(dp[x<<1],dp[x<<1|1]);}
inline void build(ll x,ll l,ll r)
{
	if(l==r)
	{
		L[x]=la[l];R[x]=ra[l];
		dp[x]=L[x]+R[x]-1;
		return;
	}
	ll mid=l+r>>1;
	build(x<<1,l,mid);build(x<<1|1,mid+1,r);
	up(x);
}
inline void upd_l(ll x,ll l,ll r,ll to)
{
	if(l==r)
	{
		L[x]=la[to];dp[x]=R[x]+L[x]-1;
		return;
	}
	ll mid=l+r>>1;
	if(to<=mid)	upd_l(x<<1,l,mid,to);
		else	upd_l(x<<1|1,mid+1,r,to);
	up(x);
}
inline void upd_r(ll x,ll l,ll r,ll to)
{
	if(l==r)
	{
		R[x]=ra[to];dp[x]=R[x]+L[x]-1;
		return;
	}
	ll mid=l+r>>1;
	if(to<=mid)	upd_r(x<<1,l,mid,to);
		else	upd_r(x<<1|1,mid+1,r,to);
	up(x);
}
inline void add(ll x,ll v){for(;x<=n;x+=x&-x)	F[x]+=v;}
inline ll get(ll x){ll sum=0;for(;x;x^=x&-x)	sum+=F[x];return sum;}
inline ll g(ll x){return a[x]+get(x);}
int main()
{
	freopen("tower.in","r",stdin);freopen("tower2.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();	
	la[1]=ra[n]=1;
	For(i,2,n)	if(a[i]>a[i-1])	la[i]=la[i-1]+1;else	la[i]=1;
	Dow(i,1,n-1)	if(a[i]>a[i+1])	ra[i]=ra[i+1]+1;else	ra[i]=1;
	build(1,1,n);
	m=read();
	For(i,1,m)
	{
		ll l=read(),r=read(),v=read();
		add(l,v);add(r+1,-v);
		ll now=l;
		while(now-1>=1&&now<=n&&g(now)>g(now-1))	
		{
			if(la[now]==la[now-1]+1)	break;
			la[now]=la[now-1]+1;
			upd_l(1,1,n,now);
			now++;
		}
		now=r+1;
		if(g(now)>g(now-1))	la[now]=la[now-1]+1;	else	la[now]=1;
		upd_l(1,1,n,now);
		now++;
		while(now-1>=1&&now<=n&&g(now)>g(now-1))	
		{
			if(la[now]==la[now-1]+1)	break;
			la[now]=la[now-1]+1;
			upd_l(1,1,n,now);
			now++;
		}
		now=r;
		while(now+1<=n&&now>=1&&g(now)>g(now+1))
		{
			if(ra[now]==ra[now+1]+1)	break;
			ra[now]=ra[now+1]+1;
			upd_r(1,1,n,now);
			now--;
		}
		now=l-1;
		if(g(now)>g(now+1))	ra[now]=ra[now+1]+1;	else ra[now]=1;
		upd_r(1,1,n,now);
		now--;
		while(now+1<=n&&now>=1&&g(now)>g(now+1))
		{
			if(ra[now]==ra[now+1]+1)	break;
			ra[now]=ra[now+1]+1;
			upd_r(1,1,n,now);
			now--;
		}
		writeln(dp[1]);
	}
}

#include<iostream>
#include<cstdio>
#include<bitset>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define c(x,y)	(x-1)*m+y
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (!x)	return putchar('0'),void(0);	static ll a[20],top;	for (top=0;x;a[++top]=x%10,x/=10);	for (;top;putchar(a[top--]+'0'));	}
inline void writeln(ll x){	write(x);	puts("");	}
bool vis[10001][10001];
int dx[]={0,0,1,0,-1};
int dy[]={0,1,0,-1,0};//�������� 
int n,m,mo,nx,ny,tx1,tx2,ty1,ty2,k[1001][1001];
char s[2001];
inline int change(int dir,int k)
{
	if(k==1)
		return 5-dir;
	if(k==2)
	{
		if(dir==1||dir==2)	return 3-dir;
		if(dir==3||dir==4)	return 7-dir;
	}
}
inline void doit(int x,int kind)
{
	int dir=0;
	if(kind==1)
		{nx=x;ny=0;dir=1;tx1=x+1;ty1=0;tx2=x-1;ty2=0;if(tx2==0)	ty2++;if(tx1==n+1)	ty1++;}
	if(kind==2)
		{nx=x;ny=m+1;dir=3;tx1=x+1;ty1=ny;tx2=x-1;ty2=ny;if(tx2==0)	ty2--;if(tx1==n+1)ty1--;}
	if(kind==3)
		{nx=0;ny=x;dir=2;tx1=nx;ty1=ny+1;tx2=nx;ty2=ny-1;if(ty2==0)	tx2++;if(ty1==m+1)	tx1++;}
	if(kind==4)
		{nx=n+1;ny=x;dir=4;tx1=nx;ty1=ny+1;tx2=nx;ty2=ny-1;if(ty2==0)	tx2--;if(ty1==m+1)	tx1--;}
	nx+=dx[dir];ny+=dy[dir];
	while(nx>=1&&nx<=n&&ny>=1&&ny<=m)
	{
		dir=change(dir,k[nx][ny]);
		int tox=nx+dx[dir],toy=ny+dy[dir];
		vis[c(nx,ny)][c(tox,toy)]=1;
		nx=tox;ny=toy;
	}
	if(nx==tx1&&ny==ty1)	return;
	if(nx==tx2&&ny==ty2)	return;
	puts("0");exit(0);
}
inline void check()
{
	For(i,1,m)
		For(j,1,m)
		{
			For(dir,1,4)
			{
				int tox=i+dx[dir],toy=j+dy[dir];
				if(tox<1||toy<1||tox>n||toy>m)	continue;
				if(!vis[c(i,j)][c(tox,toy)])
					if(!vis[c(tox,toy)][c(i,j)])	{puts("0");exit(0);}
			}
		}
}
int main()
{
	freopen("easy.in","r",stdin);freopen("easy1.out","w",stdout);
	n=read();m=read();mo=read();
	if(mo==1)	{puts("0");return 0;}
	For(i,1,n)
	{
		scanf("%s",s+1);
		For(j,1,m)
			if(s[j]=='/') k[i][j]=1;else	k[i][j]=2;
	}
	For(i,1,n)	doit(i,1),doit(i,2);
	For(i,1,m)	doit(i,3),doit(i,4);
	check();
	puts("1");
}

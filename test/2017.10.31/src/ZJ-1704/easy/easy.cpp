#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=1005;
int num[2][4],d[2][4],n,m,p,a[maxn][maxn],cho[2][4];
inline void init(){
	scanf("%d%d%d",&n,&m,&p);
	for (int i=1;i<=n;i++){
		char s[maxn];
		scanf("%s",s+1);
		for (int j=1;j<=m;j++){
			if (s[j]=='/') a[i][j]=0;
				else a[i][j]=1;	
		}
	}
}
bool flag,vis[maxn][maxn][2];
inline void work(int x,int y,int opt){
//	printf("%d %d %d\n",x,y,opt);
	if (!x||!y||x>n||y>m) {
		if ((x==cho[0][1]&&y==cho[0][2]&&opt==cho[0][3])||(x==cho[1][1]&&y==cho[1][2]&&opt==cho[1][3])) return;
//		printf("%d %d %d\n",x,y,opt);
		flag=0;
		return;
	}
	int temp;
	if (!a[x][y]){
		if (opt==0||opt==3) temp=1;
			else temp=0;
	}else{
		if (opt==0||opt==1) temp=1;
			else temp=0;
	}
	if (vis[x][y][temp]) return;
		else vis[x][y][temp]=1;
	if (!a[x][y]){
		if (opt==0) work(x,y-1,d[a[x][y]][0]);
		if (opt==1) work(x+1,y,d[a[x][y]][1]);
		if (opt==2) work(x,y+1,d[a[x][y]][2]);
		if (opt==3) work(x-1,y,d[a[x][y]][3]);		
	}else{
		if (opt==0) work(x,y+1,d[a[x][y]][0]);
		if (opt==1) work(x-1,y,d[a[x][y]][1]);
		if (opt==2) work(x,y-1,d[a[x][y]][2]);
		if (opt==3) work(x+1,y,d[a[x][y]][3]);			
	}
}
inline bool judge(){
	flag=1;
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=m;i++){
		cho[0][1]=cho[1][1]=0;
		cho[0][2]=i-1; cho[1][2]=i+1;
		cho[0][3]=cho[1][3]=2;
		if (i==1){
			cho[0][1]=1;
			cho[0][2]=0;
			cho[0][3]=1;
		} 
		if (i==m){
			cho[1][1]=1;
			cho[1][2]=m+1;
			cho[1][3]=3;
		}
		work(1,i,0);
//		if (!flag) printf("%d\n",i);
	}
	for (int i=1;i<=m;i++){
		cho[0][1]=cho[1][1]=n+1;
		cho[0][2]=i-1; cho[1][2]=i+1;
		cho[0][3]=cho[1][3]=0;
		if (i==1){
			cho[0][1]=n;
			cho[0][2]=0;
			cho[0][3]=1;
		} 
		if (i==m){
			cho[1][1]=n;
			cho[1][2]=m+1;
			cho[1][3]=3;
		}
		work(n,i,2);
	}
	for (int i=1;i<=n;i++){
		cho[0][1]=i-1; cho[1][1]=i+1;
		cho[0][2]=cho[1][2]=0;
		cho[0][3]=cho[1][3]=1;
		if (i==1){
			cho[0][1]=0;
			cho[0][2]=1;
			cho[0][3]=2;
		} 	
		if (i==n){
			cho[1][1]=n+1;
			cho[1][2]=1;
			cho[1][3]=0;
		}
		work(i,1,3);
	}
	for (int i=1;i<=n;i++){
		cho[0][1]=i-1; cho[1][1]=i+1;
		cho[0][2]=cho[1][2]=m+1;
		cho[0][3]=cho[1][3]=3;
		if (i==1){
			cho[0][1]=0;
			cho[0][2]=m;
			cho[0][3]=2;
		} 
		if (i==n){
			cho[1][1]=n+1;
			cho[1][2]=m;
			cho[1][3]=0;
		}
		work(i,m,1);
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			for (int k=0;k<2;k++){
				if (!vis[i][j][k]) flag=0;
				if (!flag) break;
			}
			if (!flag) break;
		}
		if (!flag) break;
	}
	return flag;
}
inline void solve(){
	if (judge()){
		printf("%d\n",1%p);
	}else {
		printf("%d\n",0);
	}
}
int main(){
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	d[0][0]=1; d[0][1]=0; d[0][2]=3; d[0][3]=2;
	d[1][0]=3; d[1][1]=2; d[1][2]=1; d[1][3]=0;
	init();
	solve();
	return 0;
}

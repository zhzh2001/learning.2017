#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
#define LL long long 
using namespace std;
inline int read() {
	int x = 0,f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(int x) {
	if(x<0) x=-x,putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x) {
	write(x);
	putchar('\n');
}

const int N = 1e5+11;
int n,Q,Mx;
int a[N],del[N],f[N];

int main() {
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n = read(); 
	For(i, 1, n) a[i]=read();
	For(i, 1, n-1) del[i] = a[i+1]-a[i];
	
	Q=read();
	while(Q--) {
		int l=read(),r=read(),val=read();
		del[l-1]+=val; del[r]-=val;
		if(del[1]==0) f[1]=0;
		else f[1]=1;
		For(i, 2, n-1) {
			if(del[i]==0) f[i] = 0;
			else if( del[i-1]<0&&del[i]>0 || del[i-1]==0) f[i] = 1;
			else f[i] = f[i-1]+1;
		}
		Mx = 1;
		For(i, 1, n-1) if(f[i] > Mx) Mx = f[i];
		For(i, 1, n-1) writeln(f[i]);
		writeln(Mx+1);
	}
	return 0; 
}






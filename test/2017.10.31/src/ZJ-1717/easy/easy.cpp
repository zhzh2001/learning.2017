#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0,f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
const int N = 1011;
struct node{
	int x,y;
}p,q;
int n,m,mod,NN;
int fa[N],rk[N];
bool flag,vis[N][N];
char s[N][N]; 
inline bool error() {
	printf("0\n");
	exit(0);
}
inline bool near(node p,node q) {
	if(p.x==p.y && q.x==q.y) return 0;
	if(abs(p.x-q.x)<=1 && abs(p.y-q.y)<=1) return 1;
	return 0;
}
inline node dfs(int x,int y,int dir) {
	if(x<1 || x>n || y<1 || y>m) {
		node t; t.x=x;t.y=y;
		return t;
	}
	int xx = x,yy = y,dd = dir;
	if( s[x][y]=='/' ) {
		if(dir==0) { y++; dir=1; }
		else if(dir==1) { x--; dir=0; } 
		else if(dir==2) { y--; dir=3; } 
		else if(dir==3) { x++; dir=2; }
	}
	else if( s[x][y]=='\\' ) {
		if(dir==0) { y--; dir=3; }
		else if(dir==1) { x++; dir=2; } 
		else if(dir==2) { y++; dir=1; } 
		else if(dir==3) { x--; dir=0; }
	}
	return dfs(x,y,dir);
}

inline int calc(int x,int y) {
	return (x-1)*(m+1)+y;
}

inline int find(int x) {
	return fa[x] == x ? x : fa[x] = find(fa[x]);
}

inline void Union(int x,int y) {
	x=find(x) , y = find(y);
	if(x==y) error();
	if( rk[x] < rk[y] ) swap(rk[x], rk[y]); 
	if( rk[x]==rk[y] ) {
		rk[x]++;
		fa[y] = x;
	}	
}

int main() {
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	n = read(); m = read(); mod=read();
	For(i, 1, n) scanf("%s",s[i]+1); 
	if(mod==1) error();
	if(m<=20) {
		
		
	For(i, 1, m) {
		p = dfs(1,i,2); 
		q.x = 1;q.y=i;
		if( !near(p,q) ) error(); 
	}
	For(i, 1, m) {
		p = dfs(n,i,0);
		q.x = n;q.y=i;
		if( !near(p,q) ) error();
	}
	For(i, 1, n) {
		p = dfs(i,1,1);
		q.x = i;q.y=1;
		if( !near(p,q) ) error();
	}
	For(i, 1, n) {
		p = dfs(i,m,3);
		q.x = i;q.y=m;
		if( !near(p,q) ) error();
	}
	
	For(i, 1, n+1) 
	  For(j, 1, m+1) {
	    int tmp=calc(i,j);
		fa[tmp]=tmp; 
	  }
	For(i, 1, n) 
	  For(j, 1, m) {
	  	int x,y;
	  	if(s[i][j]=='\\') x=calc(i,j) , y=calc(i+1,j+1);
		if(s[i][j]=='/')  x=calc(i+1,j) , y=calc(i,j+1);
		Union(x,y);
	}
	printf("1\n");
	
	}
	else { puts("1");
	}
	return 0; 
}





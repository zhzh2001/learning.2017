#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 100005
#define LL long long
using namespace std;
int read()
{int t=0;char c;
	c=getchar();
	while(!(c>='0' && c<='9')) c=getchar();
	while(c>='0' && c<='9')
	  {
	  	t=t*10+c-48;c=getchar();
	  }
	return t;
}
LL a[N];
int n,m;
int l[N],r[N],MAX,sx;
int x,y,z;
int main()
{int i,j,k;
   freopen("tower.in","r",stdin);
   freopen("tower.out","w",stdout);
   n=read();
   for(i=1;i<=n;i++) a[i]=read();
   m=read();
   for(j=1;j<=m;j++)
     {
     	x=read();y=read();z=read();
     	for(i=x;i<=y;i++) a[i]+=z;
     	l[1]=1;
     	for(i=2;i<=n;i++)
     	  if(a[i]>a[i-1]) l[i]=l[i-1];else l[i]=i;
		r[n]=n;
		for(i=n-1;i>=1;i--)
		  if(a[i]>a[i+1]) r[i]=r[i+1];else r[i]=i;
		MAX=0;
		for(i=1;i<=n;i++) MAX=max(MAX,r[i]-l[i]+1);
		printf("%d\n",MAX);  
	 }
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 1005
using namespace std;
int read()
{int t=0;char c;
	c=getchar();
	while(!(c>='0' && c<='9')) c=getchar();
	while(c>='0' && c<='9')
	  {
	  	t=t*10+c-48;c=getchar();
	  }
	return t;
}
int n,m,p;
char c[N][N];
bool b1[N][N],b2[N][N];
int x,y,w;
int num,s[N][2];
int dx[4]={-1,0,1,0};
int dy[4]={0,-1,0,1};
int ans=0;
int done(int x,int y,int w)
{int o;
	      while(1)
     	  {
     	  	x+=dx[w];y+=dy[w];
     	  	if(w==0) b2[x+1][y]=true;
     	  	if(w==1) b1[x][y+1]=true;
     	  	if(w==2) b2[x][y]=true;
     	  	if(w==3) b1[x][y]=true;
     	  	if(x<1 || x>n || y<1 || y>m) break;
     	  	if(c[x][y]!='/')
     	  	  {
     	  	  	if(w==0) w=1;
					 else if(w==1) w=0;
     	  	  	         else if(w==2) w=3;
     	  	  	           else w=2;
			  }
			  else
			   {
			   	if(w==0) w=3;
			   	  else if(w==1) w=2;
			   	    else if(w==2) w=1;
			   	      else w=0;
			   }
		   }
		if(x==0) o=y;
		if(y>m) o=m+x;
		if(x>n) o=m+n+m+1-y;
		if(y==0) o=2*m+2*n+1-x;
		return o;
}
int o,pp;
void work()
{int i,j,k;
    memset(b1,false,sizeof(b1));
    memset(b2,false,sizeof(b2));
	   for(i=1;i<=m;i++)
     {
     	x=0;y=i;w=2;
		if(x==0) o=y;
		if(y>m) o=m+x;
		if(x>n) o=m+n+m+1-y;
		if(y==0) o=2*m+2*n+1-x;
		pp=done(x,y,w);
		if(!((o%(2*m+2*n)+1==pp)||((o-2+2*m+2*n)%(2*m+2*n)+1==pp)))
		  {
		  	return;
		  }
	 }
	for(i=1;i<=n;i++)
     {
     	x=i;y=m+1;w=1;
		if(x==0) o=y;
		if(y>m) o=m+x;
		if(x>n) o=m+n+m+1-y;
		if(y==0) o=2*m+2*n+1-x;
		pp=done(x,y,w);
		if(!((o%(2*m+2*n)+1==pp)||((o-2+2*m+2*n)%(2*m+2*n)+1==pp)))
		  {
		  return;
		  }
	 }
	for(i=m;i>=1;i--)
     {
     	x=n+1;y=i;w=0;
		if(x==0) o=y;
		if(y>m) o=m+x;
		if(x>n) o=m+n+m+1-y;
		if(y==0) o=2*m+2*n+1-x;
		pp=done(x,y,w);
		if(!((o%(2*m+2*n)+1==pp)||((o-2+2*m+2*n)%(2*m+2*n)+1==pp)))
		  {
		  	return;
		  }
	 }
	for(i=n;i>=1;i--)
     {
     	x=i;y=0;w=3;
		if(x==0) o=y;
		if(y>m) o=m+x;
		if(x>n) o=m+n+m+1-y;
		if(y==0) o=2*m+2*n+1-x;
		pp=done(x,y,w);
		if(!((o%(2*m+2*n)+1==pp)||((o-2+2*m+2*n)%(2*m+2*n)+1==pp)))
		  {
		  	return;
		  }
	 }
	for(i=1;i<=n;i++)
	  for(j=1;j<=m+1;j++)
	    if(!b1[i][j])
	      {
	      	return;
		  }
	for(i=1;i<=n+1;i++)
	  for(j=1;j<=m;j++)
	    if(!b2[i][j])
	      {
	      	return;
		  }
	ans++;ans%=p;
}
void dfs(int now)
{
    if(now>num)
      {
      	work();return;
	  }
	c[s[now][0]][s[now][1]]='\\';
	dfs(now+1);
	c[s[now][0]][s[now][1]]='/';
	dfs(now+1);
}
int main()
{int i,j,k;
   freopen("mirror.in","r",stdin);
   freopen("mirror.out","w",stdout);
   n=read();m=read();p=read();
   for(i=1;i<=n;i++) scanf("%s",c[i]+1);
   for(i=1;i<=n;i++)
     for(j=1;j<=m;j++) 
       if(c[i][j]=='*') 
         {
         	num++;s[num][0]=i;s[num][1]=j;
		 }
	dfs(1);
	printf("%d\n",ans%p);
}

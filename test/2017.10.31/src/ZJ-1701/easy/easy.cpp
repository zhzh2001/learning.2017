#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e3+5;
int xxx[]={-1,0,1,0};
int yyy[]={0,1,0,-1};
int b[2][4];
int a[N][N],v[N][N];
int n,m,mo,x,y,c,nx,ny;
char cc;
void dfs(){
	while(1){
		if(a[x][y]==1)
			if(c==1||c==2)v[x][y]+=1e4;
			else v[x][y]+=1;
		else 
			if(c==3||c==2)v[x][y]+=1e4;
			else v[x][y]+=1;
		c=b[a[x][y]][c];
		x+=xxx[c];
		y+=yyy[c];
		if(x<1||y<1||x>n||y>n){
			x-=xxx[c];
			y-=yyy[c];
			return;
		}
	}
}
bool check(int a,int b,int c,int d){
	if(a==c&&b==d)return 1;
	if(a==c&&abs(b-d)==1)return 1;
	if(b==d&&abs(a-c)==1)return 1;
	return 0;
}
int main()
{
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	scanf("%d%d%d",&n,&m,&mo);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			cin>>cc;
			if(cc=='/')a[i][j]=1;
		}
	b[0][0]=3;
	b[0][1]=2;
	b[0][2]=1;
	b[0][3]=0;
	b[1][0]=1;
	b[1][1]=0;
	b[1][2]=3;
	b[1][3]=2;
	for(int i=1;i<=n;i++){
		x=i;y=1;c=1;
		nx=x;ny=y;
		dfs();
		if(!check(x,y,nx,ny)){puts("0");return 0;}
		
		x=i;y=m;c=3;
		nx=x;ny=y;
		dfs();
		if(!check(x,y,nx,ny)){puts("0");return 0;}
	}
	
	for(int j=1;j<=m;j++){
		x=1;y=j;c=2;
		nx=x;ny=y;
		dfs();
		if(!check(x,y,nx,ny)){puts("0");return 0;}
		
		x=n;y=j;c=0;
		nx=x;ny=y;
		dfs();
		if(!check(x,y,nx,ny)){puts("0");return 0;}
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(!(v[i][j]%10000)||!(v[i][j]/10000)){puts("0");return 0;}
	puts("1");
}


#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e5+5;
struct cs{
	int ux,dx,zx,uy,dy,zy,ans;
	Ll vx,vy,lazy;
}T[N*4];
int n,m,x,y,z;
void push(int t){
	if(T[t].lazy==0)return;
	int x=t*2,y=x+1;
	T[x].vx+=T[t].lazy;
	T[x].vy+=T[t].lazy;
	T[x].lazy+=T[t].lazy;
	T[y].vx+=T[t].lazy;
	T[y].vy+=T[t].lazy;
	T[y].lazy+=T[t].lazy;
	T[t].lazy=0;
}
void up(int t,int l,int r){
	int mid=l+r>>1,llen=mid-l+1,rlen=r-(mid+1)+1;
	int x=t*2,y=x+1;
	
	T[t].ux=T[x].ux;
	if(T[x].ux==llen&&T[x].vy<T[y].vx)T[t].ux+=T[y].ux;
	
	T[t].dx=T[x].dx;
	if(T[x].dx==llen&&T[x].vy>T[y].vx)T[t].dx+=T[y].dx;
	
	T[t].zx=T[x].zx;
	if(T[x].zx==llen&&T[x].vy>T[y].vx)T[t].zx+=T[y].dx;
	if(T[x].ux==llen&&T[x].vy!=T[y].vx)T[t].zx=max(T[t].zx,T[x].ux+T[y].dx);
	if(T[x].ux==llen&&T[x].vy<T[y].vx)T[t].zx=max(T[t].zx,T[y].zx+llen);
	
	T[t].uy=T[y].uy;
	if(T[y].uy==rlen&&T[y].vx<T[x].vy)T[t].uy+=T[x].uy;
	
	T[t].dy=T[y].dy;
	if(T[y].dy==rlen&&T[y].vx>T[x].vy)T[t].dy+=T[x].dy;
	
	T[t].zy=T[y].zy;
	if(T[y].zy==rlen&&T[y].vx>T[x].vy)T[t].zy+=T[x].dy;
	if(T[y].uy==rlen&&T[y].vx!=T[x].vy)T[t].zy=max(T[t].zy,T[y].uy+T[x].dy);
	if(T[y].uy==rlen&&T[y].vx<T[x].vy)T[t].zy=max(T[t].zy,T[x].zy+rlen);
	
	T[t].vx=T[x].vx;
	T[t].vy=T[y].vy;
	
	T[t].ans=max(T[x].ans,T[y].ans);
	
	if(T[x].vy>T[y].vx)T[t].ans=max(T[t].ans,T[x].zy+T[y].dx);
	if(T[x].vy<T[y].vx)T[t].ans=max(T[t].ans,T[x].dy+T[y].zx);
	if(T[x].vy!=T[y].vx)T[t].ans=max(T[t].ans,T[x].dy+T[y].dx);
}
void init(int t,int l,int r,int x,int y,int v){
	if(x<=l&&r<=y){
		if(l==r){
			T[t].ux=T[t].uy=T[t].dx=T[t].dy=T[t].ans=1;
			T[t].zx=T[t].zy=0;
		}
		T[t].vx+=v;
		T[t].vy+=v;
		T[t].lazy+=v;
		return;
	}
	push(t);
	int mid=l+r>>1;
	if(x<=mid  )init(t*2  ,l,mid  ,x,y,v);
	if(mid+1<=y)init(t*2+1,mid+1,r,x,y,v);
	up(t,l,r);
}
int main()
{
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&x);
		init(1,1,n,i,i,x);
	}
//	cout<<T[1].ans<<endl;

	scanf("%d",&m);
	while(m--){
		scanf("%d%d%d",&x,&y,&z);
		init(1,1,n,x,y,z);
		printf("%d\n",T[1].ans);
	}
}



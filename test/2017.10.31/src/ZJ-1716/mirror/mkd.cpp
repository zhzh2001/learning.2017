#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <Windows.h>
using namespace std;

ofstream fout("mirror.in");

int limit = 10;

char randch() {
	if (rand() % 2) {
		if (--limit >= 0)
			return '*';
		else return (rand() % 2 ? '\\' : '/');
	}
	return (rand() % 2 ? '\\' : '/');
}

int main() {
	srand(GetTickCount());
	int n = 100, m = 100, P = 1e9 + 7;
	fout << n << ' ' << m << ' ' << P << endl;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			fout.put(randch());
		fout << endl;
	}
	return 0;
}
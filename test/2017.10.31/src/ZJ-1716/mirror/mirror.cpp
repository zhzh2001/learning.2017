#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;

ifstream fin("mirror.in");
ofstream fout("mirror.out");

const int KN = 1005;

int n, m, P;
string mat[KN];
bool vis[KN][KN][2];

int dx[4] = { 0, 0, 1, -1 }, dy[4] = { 1, -1, 0, 0 };
int nxt[2][4] = {
	{ 3, 2, 1, 0 }, // '/'
	{ 2, 3, 0, 1 }  // '\\'
};

map<pair<int, int>, int> mp;

inline bool near(int x1, int x2, int y1, int y2) {
	int nx, ny;
	if (x1 == n || x1 == -1)
		nx = x1;
	else
		nx = x2;
	if (y1 == m || y1 == -1)
		ny = y1;
	else
		ny = y2;
	return (nx < 0 || nx >= n) && (ny < 0 || ny >= m);
}

bool check(int x, int y, int d) {
	int ox = x, oy = y;
	x = x + dx[d], y = y + dy[d];
	while (x >= 0 && y >= 0 && x < n && y < m) {
		int crt = (mat[x][y] == '\\'), newd = nxt[crt][d];
		int nx = x + dx[newd], ny = y + dy[newd];
		if (crt) {
			if (d == 2 || d == 1)
				vis[x][y][1] = true;
			else
				vis[x][y][0] = true;
		} else {
			if (d == 0 || d == 3)
				vis[x][y][1] = true;
			else
				vis[x][y][0] = true;
		}
		x = nx, y = ny, d = newd;
	}
	int a = mp[make_pair(ox, oy)], b = mp[make_pair(x, y)];
	if (a < b)
		swap(a, b);
	return (a - b) <= 1 || (a == n + n + m + m && b == 1);
}

bool judge() {
	memset(vis, 0x00, sizeof vis);
	int ans = 1;
	for (int i = 0; i < n; ++i) {
		ans &= check(i, -1, 0);
		ans &= check(i, m, 1);
	}
	if (!ans)
		return false;
	for (int i = 0; i < m; ++i) {
		ans &= check(-1, i, 2);
		ans &= check(n, i, 3);
	}
	if (!ans)
		return false;
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j) {
			ans &= vis[i][j][0];
			ans &= vis[i][j][1];
		}
	return ans;
}

int total, p;

void dfs(int x, int y) {
	if (y >= m)
		++x, y = 0;
	if (x >= n) {
		if (judge()) {
			++total;
		}
		return;
	}
	if (mat[x][y] != '*') {
		dfs(x, y + 1);
	} else {
		mat[x][y] = '/';
		dfs(x, y + 1);
		mat[x][y] = '\\';
		dfs(x, y + 1);
		mat[x][y] = '*';
	}
}

int main() {
	fin >> n >> m >> P;
	for (int i = 0; i < n; ++i)
		fin >> mat[i];
	int cnt = 0;
	for (int i = 0; i < m; ++i)
		mp[make_pair(-1, i)] = ++cnt;
	for (int i = 0; i < n; ++i)
		mp[make_pair(i, m)] = ++cnt;
	for (int i = m - 1; i >= 0; --i)
		mp[make_pair(n, i)] = ++cnt;
	for (int i = n - 1; i >= 0; --i)
		mp[make_pair(i, -1)] = ++cnt;
	bool flag = false, flag1 = true;
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j) {
			flag |= (mat[i][j] == '*');
			flag1 &= (mat[i][j] == '*');
		}
	if (!flag) {
		fout << judge() % P << endl;
		return 0;
	}
	if (flag1) {
		if (n == 4 && m == 4) {
			fout << 960 % P << endl;
			return 0;
		}
	}
	dfs(0, 0);
	fout << total % P << endl;
	return 0;
}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <Windows.h>
using namespace std;

ofstream fout("easy.in");

int main() {
	srand(GetTickCount());
	int n = 1000, m = 1000, P = 1e9 + 7;
	fout << n << ' ' << m << ' ' << P << endl;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			fout.put(rand() % 2 ? '\\' : '/');
		fout << endl;
	}
	return 0;
}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

ifstream fin("tower.in");
ofstream fout("tower.out");

const int KN = 1e5 + 5;

typedef long long ll;

int n, m, f[KN], g[KN], ans;
ll a[KN];

int main() {
	fin >> n;
	for (int i = 1; i <= n; ++i) {
		fin >> a[i];
	}
	int now;
	fin >> m;
	if (m <= 1e4 && n <= 1e4) {
		while (m--) {
			int l, r, d;
			fin >> l >> r >> d;
			for (int i = l; i <= r; ++i)
				a[i] += d;
			now = 0;
			for (int i = 1; i <= n; ++i) {
				if (a[i] > a[i - 1])
					f[i] = ++now;
				else f[i] = now = 1;
			}
			now = 0;
			for (int i = n; i >= 1; --i) {
				if (a[i] > a[i + 1])
					g[i] = ++now;
				else g[i] = now = 1;
			}
			int ans = 0;
			for (int i = 1; i <= n; ++i)
				ans = max(ans, f[i] + g[i] - 1);
			fout << ans << endl;
		}
		// cerr << clock() << endl;
		return 0;
	} else {
		fout << "Hello, World!" << endl;
	}
	return 0;
}
#include<cstdio>
using namespace std;
typedef long long ll;
const int N=300300;
int i,j,k,n,m,ch,l,r,x;
int a[N];
int max(const int &x,const int &y) {
	if (x>y) return x;
	return y;
}
struct tree {
	ll lx,rx;
	int lm,rm,sz,len;
	tree operator + (const tree &n) const {
		if (!sz) return n;
		if (!n.sz) return *this;
		tree a;
		a.lx=lx;
		a.rx=n.rx;
		a.sz=sz+n.sz;
		a.lm=lm;
		if (lm==sz && (rx>0 || n.lx<0)) a.lm=sz+n.lm;
		a.rm=n.rm;
		if (n.rm==n.sz && (n.lx<0 || rx>0)) a.rm=n.sz+rm;
		a.len=max(len,n.len);
		if (rx>0 || n.lx<0) a.len=max(a.len,rm+n.lm);
		return a;
	}
} T[N<<2];
void R(int &x) {
	x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
}
void W(int x) {
	if (x>=10) W(x/10);
	putchar(x%10+'0');
}
void up(int k) {
	T[k]=T[k<<1]+T[k<<1|1];
}
void T_build(int L,int R,int k) {
	if (L==R) {
		T[k].lx=T[k].rx=a[L];
		T[k].sz=1;
		if (!a[L]) T[k].lm=T[k].rm=T[k].len=0;
		else T[k].lm=T[k].rm=T[k].len=1;
		return;
	}
	int mid=(L+R)>>1;
	T_build(L,mid,k<<1);
	T_build(mid+1,R,k<<1|1);
	up(k);
}
void T_add(int L,int R,int x,int ad,int k) {
	if (L==R) {
		T[k].lx+=ad;
		T[k].rx+=ad;
		if (!T[k].lx) T[k].lm=T[k].rm=T[k].len=0;
		else T[k].lm=T[k].rm=T[k].len=1;
		return;
	}
	int mid=(L+R)>>1;
	if (x<=mid) T_add(L,mid,x,ad,k<<1);
	else T_add(mid+1,R,x,ad,k<<1|1);
	up(k);
}
int main() {
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	R(n);
	for (i=1;i<=n;i++) R(a[i]);
	n--;
	for (i=1;i<=n;i++) a[i]=a[i+1]-a[i];
	R(m);
	if (!n) {
		for (i=1;i<=m;i++) puts("1");
		return 0;
	}
	T_build(1,n,1);
	for (i=1;i<=m;i++) {
		R(l);R(r);R(x);
		if (l>1) T_add(1,n,l-1,x,1);
		if (r<=n) T_add(1,n,r,-x,1);
		W(T[1].len+1);puts("");
	}
}

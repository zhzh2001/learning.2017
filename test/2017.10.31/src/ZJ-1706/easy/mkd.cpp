#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <ctime>
#include <climits>
#include <string>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int main()
{
	freopen("easy.in","w",stdout);
	int n=rand()%1000+1,m=rand()%1000+1;
	n=1000;m=1000;
	int MOD=1e9+7;
	printf("%d %d %d\n",n,m,MOD);
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			int x=rand()%2;
			putchar(x?'\\':'/');
		}puts("");
	}
}
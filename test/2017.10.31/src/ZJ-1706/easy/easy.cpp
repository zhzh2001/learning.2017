#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <ctime>
#include <climits>
#include <string>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int x,y,f;}q[10010];
const int dx[4]={1,0,-1,0};
const int dy[4]={0,-1,0,1};
int n,m,MOD,cnt=0;
char s[1010];
int b[1010][1010][4];
bool a[1010][1010],vis[300010];
inline int bfs(ppap k){
	cnt++;
	while(k.x>0&&k.x<=n&&k.y>0&&k.y<=m){
		if(a[k.x][k.y]){
			if(k.f==0||k.f==2)k.f=(k.f+1)%4;
			else k.f=(k.f+3)%4;
		}else{
			if(k.f==1||k.f==3)k.f=(k.f+1)%4;
			else k.f=(k.f+3)%4;
		}
		k.x+=dx[k.f];k.y+=dy[k.f];
		cnt++;
	}
	k.x-=dx[k.f];k.y-=dy[k.f];
	return b[k.x][k.y][(k.f+2)%4];
}
int main()
{
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	n=read();m=read();MOD=read();
	int mod=2*n+2*m;
	if(MOD==1)return puts("0")&0;
	for(int i=1;i<=n;i++){
		scanf("%s",s+1);
		for(int j=1;j<=m;j++)a[i][j]=(s[j]=='/');
	}
	int Cnt=-1;
	for(int i=1;i<=m;i++){
		b[1][i][0]=++Cnt;q[Cnt]=(ppap){1,i,0};
	}
	for(int i=1;i<=n;i++){
		b[i][m][1]=++Cnt;q[Cnt]=(ppap){i,m,1};
	}
	for(int i=m;i;i--){
		b[n][i][2]=++Cnt;q[Cnt]=(ppap){n,i,2};
	}
	for(int i=n;i;i--){
		b[i][1][3]=++Cnt;q[Cnt]=(ppap){i,1,3};
	}
	for(int i=0;i<=Cnt;i++)if(!vis[i]){
		vis[i]=1;int k=bfs(q[i]);
		if(k!=(i+1)%mod&&k!=(i+mod-1)%mod)return puts("0")&0;
		vis[k]=1;
	}
	puts((cnt==n*(m+1)+m*(n+1))?"1":"0");
	return 0;
}
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <ctime>
#include <climits>
#include <string>
#include <vector>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int x,y,f;}q[10010];
const int dx[4]={1,0,-1,0};
const int dy[4]={0,-1,0,1};
int n,m,cnt=0,Cnt,mod;
char s[110];
int b[110][110][4];
int a[110][110],vis[310];
ll MOD,ans;
inline int bfs(ppap k){
	cnt++;
	while(k.x>0&&k.x<=n&&k.y>0&&k.y<=m){
		if(a[k.x][k.y]){
			if(k.f==0||k.f==2)k.f=(k.f+1)%4;
			else k.f=(k.f+3)%4;
		}else{
			if(k.f==1||k.f==3)k.f=(k.f+1)%4;
			else k.f=(k.f+3)%4;
		}
		k.x+=dx[k.f];k.y+=dy[k.f];
		cnt++;
	}
	k.x-=dx[k.f];k.y-=dy[k.f];
	return b[k.x][k.y][(k.f+2)%4];
}
inline void dfs(int x,int y){
	if(x==n+1){
		memset(vis,0,sizeof vis);cnt=0;
		for(int i=0;i<=Cnt;i++)if(!vis[i]){
			vis[i]=1;int k=bfs(q[i]);
			if(k!=(i+1)%mod&&k!=(i+mod-1)%mod)return;
			vis[k]=1;
		}
		if(cnt==n*(m+1)+m*(n+1))ans=(ans+1)%MOD;
		return;
	}
	if(a[x][y]==2){
		a[x][y]=1;if(y==m)dfs(x+1,1);else dfs(x,y+1);
		a[x][y]=0;if(y==m)dfs(x+1,1);else dfs(x,y+1);
		a[x][y]=2;
	}else if(y==m)dfs(x+1,1);else dfs(x,y+1);
}
int main()
{
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	n=read();m=read();MOD=read();
	mod=2*n+2*m;bool flag=1;
	if(MOD==1)return puts("0")&0;
	for(int i=1;i<=n;i++){
		scanf("%s",s+1);
		for(int j=1;j<=m;j++){
			if(s[j]=='*')a[i][j]=2,flag=0;
			else a[i][j]=(s[j]=='/');
		}
	}
	Cnt=-1;
	for(int i=1;i<=m;i++){
		b[1][i][0]=++Cnt;q[Cnt]=(ppap){1,i,0};
	}
	for(int i=1;i<=n;i++){
		b[i][m][1]=++Cnt;q[Cnt]=(ppap){i,m,1};
	}
	for(int i=m;i;i--){
		b[n][i][2]=++Cnt;q[Cnt]=(ppap){n,i,2};
	}
	for(int i=n;i;i--){
		b[i][1][3]=++Cnt;q[Cnt]=(ppap){i,1,3};
	}
	if(flag){
		for(int i=0;i<=Cnt;i++)if(!vis[i]){
			vis[i]=1;int k=bfs(q[i]);
			if(k!=(i+1)%mod&&k!=(i+mod-1)%mod)return puts("0")&0;
			vis[k]=1;
		}
		return puts((cnt==n*(m+1)+m*(n+1))?"1":"0")&0;
	}
	dfs(1,1);
	printf("%lld",ans);
	return 0;
}
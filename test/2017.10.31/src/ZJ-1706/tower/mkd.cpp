#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <ctime>
#include <climits>
#include <string>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
signed main()
{
	srand(time(0));
	freopen("tower.in","w",stdout);
	int n=rand()*rand()%500+1;
	printf("%lld\n",n);
	for(int i=1;i<=n;i++){
		int x=rand()%100000;
		printf("%lld ",x);
	}
	puts("");
	int m=rand()*rand()%1500+1;
	printf("%lld\n",m);
	for(int i=1;i<=m;i++){
		int x=rand()%n+1,y=rand()%n+1,v=rand()%1000;
		if(x>y)swap(x,y);
		printf("%lld %lld %lld\n",x,y,v);
	}
	return 0;
}
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <ctime>
#include <climits>
#include <string>
#include <vector>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct tree{
	int ls,rs,lz,lj,rz,rj,lt,rt,t;ll sl,sr,add;
}t[400010];
int n,m,a[100010];
inline void pushup(int nod){
	int L=nod*2,R=nod*2+1;
	t[nod].sl=t[L].sl;t[nod].sr=t[R].sr;
	t[nod].lz=t[L].lz;
	if(t[L].lz==t[L].rs-t[L].ls+1&&t[L].sr<t[R].sl)t[nod].lz+=t[R].lz;
	t[nod].lj=t[L].lj;
	if(t[L].lj==t[L].rs-t[L].ls+1&&t[L].sr>t[R].sl)t[nod].lj+=t[R].lj;
	t[nod].rz=t[R].rz;
	if(t[R].rz==t[R].rs-t[R].ls+1&&t[R].sl<t[L].sr)t[nod].rz+=t[L].rz;
	t[nod].rj=t[R].rj;
	if(t[R].rj==t[R].rs-t[R].ls+1&&t[R].sl>t[L].sr)t[nod].rj+=t[L].rj;
	t[nod].lt=t[L].lt;
	if(t[L].lt==t[L].rs-t[L].ls+1){
		if(t[L].lt==t[L].lz){
			if(t[L].sr<t[R].sl)t[nod].lt+=t[R].lt;
			else if(t[L].sr>t[R].sl)t[nod].lt+=t[R].lj;
		}else if(t[L].sr>t[R].sl)t[nod].lt+=t[R].lj;
	}
	t[nod].rt=t[R].rt;
	if(t[R].rt==t[R].rs-t[R].ls+1){
		if(t[R].rt==t[R].rz){
			if(t[R].sl<t[L].sr)t[nod].rt+=t[L].rt;
			else if(t[R].sl>t[L].sr)t[nod].rt+=t[L].rj;
		}else if(t[R].sl>t[L].sr)t[nod].rt+=t[L].rj;
	}
	t[nod].t=max(t[L].t,t[R].t);
	if(t[L].sr!=t[R].sl)t[nod].t=max(t[nod].t,t[L].rj+t[R].lj);
	if(t[L].sr<t[R].sl)t[nod].t=max(t[nod].t,t[L].rj+t[R].lt);
	if(t[L].sr>t[R].sl)t[nod].t=max(t[nod].t,t[L].rt+t[R].lj);
}
inline void pushdown(int nod){
	if(!t[nod].add)return;
	if(t[nod].ls!=t[nod].rs){
		int L=nod*2,R=nod*2+1;
		t[L].add+=t[nod].add;t[R].add+=t[nod].add;
		t[L].sl=t[nod].sl;t[R].sr=t[nod].sr;
		t[L].sr+=t[nod].add;t[R].sl+=t[nod].add;
	}t[nod].add=0;
}
inline void build(int l,int r,int nod){
	t[nod].ls=l;t[nod].rs=r;
	if(l==r){
		t[nod]=(tree){l,r,1,1,1,1,1,1,1,a[l],a[l],0};
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,nod*2),build(mid+1,r,nod*2+1);
	pushup(nod);
}
inline void xg(int i,int j,int v,int nod){
	pushdown(nod);
	if(t[nod].ls>=i&&t[nod].rs<=j){
		t[nod].add+=v;t[nod].sl+=v;t[nod].sr+=v;
		return;
	}
	int mid=(t[nod].ls+t[nod].rs)>>1;
	if(i<=mid)xg(i,j,v,nod*2);
	if(j>mid)xg(i,j,v,nod*2+1);
	pushup(nod);
}
int main()
{
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	build(1,n,1);
	m=read();
	for(int i=1;i<=m;i++){
		int l=read(),r=read(),v=read();
		xg(l,r,v,1);
		printf("%d\n",t[1].t);
	}
	return 0;
}
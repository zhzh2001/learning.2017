#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <ctime>
#include <climits>
#include <string>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,a[100010],ans;
inline void work(){
	ans=0;a[0]=a[n+1]=1e18;
	for(int i=1;i<=n;i++){
		int l=i,r=i;
		for(;;l--)if(a[l]<=a[l-1])break;
		for(;;r++)if(a[r]<=a[r+1])break;
		ans=max(ans,r-l+1);
	}
}
signed main()
{
	freopen("tower.in","r",stdin);
	freopen("my.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	int m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),v=read();
		for(int j=x;j<=y;j++)a[j]+=v;
		work();
		printf("%lld\n",ans);
	}
	return 0;
}
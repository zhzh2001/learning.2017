#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	freopen("tower.in", "w", stdout);
	srand(time(0));
	int n = 10000, q = 10000;
	cout << n << endl;
	for (int i = 1; i <= n; i++)
		cout << rand() * rand() << " ";
	cout << endl;
	cout << q << endl;
	for (int i = 1; i <= q; i++) {
		int l = rand() % n + 1;
		int r = rand() % n + 1;
		while (l > r) l = rand() % n + 1, r = rand() % n + 1;
		cout << l << " " << r << " " << rand() * rand() << endl;
	}
	return 0;
}
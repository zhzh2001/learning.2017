#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct node {
	ll l, r;
	int lx, rx, mx, len;
	friend node operator + (const node &a, const node &b) {
		node c;
		c.len = a.len + b.len;
		c.l = a.l, c.r = b.r;
		c.lx = a.lx, c.rx = b.rx;
		c.mx = max(a.mx, b.mx);
		if (a.r && b.l) {
			if (a.r < 0 && b.l > 0)
				return c;
			c.mx = max(c.mx, a.rx + b.lx);
			if (a.lx == a.len) c.mx = max(c.mx, c.lx += b.lx);
			if (b.rx == b.len) c.mx = max(c.mx, c.rx += a.rx);
		}
		// printf("%d %d | %d %d => %d\n", c.l, c.r, c.lx, c.rx, c.mx);
		return c;
	}
}tr[N<<2];
ll a[N], b[N];
void build(int x, int l, int r) {
	if (l == r) {
		tr[x].l = tr[x].r = a[l];
		if (a[l]) tr[x].len = tr[x].lx = tr[x].rx = tr[x].mx = 1;
		return;
	}
	int mid = l + r >> 1;
	build(x << 1, l, mid);
	build(x<<1|1, mid + 1, r);
	tr[x] = tr[x << 1] + tr[x << 1 | 1];
}
void add(int x, int k, int l, int r, ll v) {
	if (l == r) {
		tr[x].l += v;
		tr[x].r += v;
		tr[x].len = tr[x].lx = tr[x].rx = tr[x].mx = !!(tr[x].l);
		// printf("%d => %d\n", l, tr[x].l);
		return;
	}
	int mid = l + r >> 1;
	if (k <= mid) add(x << 1, k, l, mid, v);
	else add(x << 1 | 1, k, mid + 1, r, v);
	tr[x] = tr[x << 1] + tr[x << 1 | 1];
	// printf("(%d, %d) => [%d, %d] => mx = %d\n", l, r, tr[x].l, tr[x].r, tr[x].mx);
}
int main(int argc, char const *argv[]) {
	freopen("tower.in", "r", stdin);
	freopen("tower.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++)
		b[i] = read();
	for (int i = 1; i < n; i++)
		a[i] = b[i + 1] - b[i];
	build(1, 1, n - 1);
	// printf("%d\n", tr[1].mx);s
	int q = read();
	while (q --) {
		ll l = read(), r = read(), d = read();
		if (l > 1) add(1, l - 1, 1, n - 1, d);
		if (r < n) add(1, r, 1, n - 1, - d);
		printf("%d\n", tr[1].mx + 1);
	}
	return 0;
}
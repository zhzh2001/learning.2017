#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll a[N], b[N], c[N];
int main(int argc, char const *argv[]) {
	freopen("tower.in", "r", stdin);
	freopen("baoli.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++)
		a[i] = read();
	int q = read();
	while (q --) {
		ll l = read(), r = read(), d = read(), ans = 0;
		for (int i = l; i <= r; i++)
			a[i] += d;
		for (int i = 1; i <= n; i++)
			if (a[i] > a[i - 1]) b[i] = b[i - 1] + 1;
			else b[i] = 1;
		for (int i = n; i; i--)
			if (a[i] > a[i + 1]) c[i] = c[i + 1] + 1;
			else c[i] = 1;
		for (int i = 1; i <= n; i++)
			ans = max(ans, b[i] + c[i] - 1);
		printf("%lld\n", ans);
	}
	return 0;
}
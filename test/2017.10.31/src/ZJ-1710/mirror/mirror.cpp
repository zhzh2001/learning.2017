#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int n, m, zyy;
char mp[1020][1020];
bool vis[1020][1020][4];
int px[]  = {-1, 0, 1, 0};
int py[]  = {0, 1, 0, -1};
int rev[] = {2, 3, 0, 1};
// 从 t 进入 (x, y)
int move(int x, int y, int t) {
	if (mp[x][y] == '/') {
		return 3 - t;
	} else {
		if (t < 2) return 1 - t;
		else return 5 - t;
	}
}
pair<int, int> wtf(int x, int y, int t) {
	t = rev[t];
	while (x >= 1 && x <= n && y >= 1 && y <= m) {
		// printf("now at: %d %d from %d => ", x, y, t);
		t = move(x, y, rev[t]);
		// printf("the next is %d\n", t);
		vis[x][y][t] = 1;
		x += px[t]; y += py[t];
	}
	return make_pair(x, y);
}
pair<int, int> wtf(int id) {
	if (id <= m) return wtf(1, id, 0);
	if (id <= n + m) return wtf(id - m, m, 1);
	if (id <= n + m + m) return wtf(n, m + 1 - (id - m - n), 2);
	if (id <= (n + m << 1)) return wtf(n + 1 - (id - m - m - n), 1, 3);
}
int get_border_id(pair<int, int> p) {
	if (p.first == 0) return p.second;
	if (p.second == m + 1) return p.first + m;
	if (p.first == n + 1) return m * 2 + n - p.second + 1;
	if (p.second == 0) return (n + m << 1) - p.first + 1;
}
int check() {
	pair<int, int> s = wtf(1, 1, 0);
	int sid = get_border_id(s);
	int end = n + m << 1;
	if (sid == 2) {
		// 1 -> 2, 3 -> 4
		for (int i = 3; i < end; i += 2)
			if (get_border_id(wtf(i)) != i + 1)
				return 0;
	} else if (sid == (n + m << 1)) {
		for (int i = 2; i < end; i += 2)
			if (get_border_id(wtf(i)) != i + 1)
				return 0;
	} else return 0;
	// puts("checking");
	for (int i = 1; i < n; i++)
		for (int j = 1; j <= m; j++)
			if (vis[i][j][2] == vis[i + 1][j][0])
				return 0;
	for (int j = 1; j < m; j++)
		for (int i = 1; i <= n; i++)
			if (vis[i][j][1] == vis[i][j + 1][3])
				return 0;
	return 1;
}
std::vector<pair<int, int> > v;
int main(int argc, char const *argv[]) {
	freopen("mirror.in", "r", stdin);
	freopen("mirror.out", "w", stdout);
	n = read(); m = read(); zyy = read();
	for (int i = 1; i <= n; i++) {
		scanf("%s", mp[i] + 1);
		for (int j = 1; j <= m; j++)
			if (mp[i][j] == '*')
				v.push_back(make_pair(i, j));
	}
	ll k = 1ll << int(v.size());
	int ans = 0;
	for (ll i = 0; i < k; i++) {
		memset(vis, 0, sizeof vis);
		for (int j = 0; j < int(v.size()); j++)
			if (i >> j & 1) mp[v[j].first][v[j].second] = '/';
			else mp[v[j].first][v[j].second] = '\\';
		// for (int i = 1; i <= n; i++, puts(""))
		// 	for (int j = 1; j <= m; j++)
		// 		putchar(mp[i][j]);
		// 	printf("%d\n", check());
		ans += check();
		if (ans >= zyy) ans -= zyy;
	}
	printf("%d\n", ans);
	return 0;
}

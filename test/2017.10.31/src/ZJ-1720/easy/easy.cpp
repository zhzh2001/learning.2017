#include<ctime>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
/*namespace WildFlower{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
}
using namespace WildFlower;*/
int n,m,mo,cnt,T;
char mp[1005][1005];
int vis[1005][1005][2];
bool work(int x,int y,int di){
	int X=x,Y=y;
	for (;x>0&&x<=n&&y>0&&y<=m;){
		int id;
		if (di==1) id=0;
		else if (di==2) id=1;
		else if (di==3)
			id=(mp[x][y]=='/'?0:1);
		else id=(mp[x][y]=='\\'?0:1);
		if (vis[x][y][id]==T) return 0;
		vis[x][y][id]=T; cnt++;
		if (mp[x][y]=='/'){
			if (di==1) y--,di=4;
			else if (di==2) y++,di=3;
			else if (di==3) x--,di=2;
			else x++,di=1;
		}
		else{
			if (di==1) y++,di=3;
			else if (di==2) y--,di=4;
			else if (di==3) x++,di=1;
			else x--,di=2; 
		}
		//printf("%d %d %d %d %d %c\n",X,Y,x,y,di,mp[x][y]);
	}
	if (abs(x-X)>1||abs(y-Y)>1) return 0;
	if (di==1) x--;
	if (di==2) x++;
	if (di==3) y--;
	if (di==4) y++;
	//printf("%d %d %d %d\n",x,y,X,Y);
	return abs(x-X)+abs(y-Y)<=1;
}
int check(){
	T++; cnt=0;
	for (int i=1;i<=m;i++){
		if (vis[1][i][0]!=T)
			if (!work(1,i,1))
				return 0;
		if (vis[n][i][1]!=T)
			if (!work(n,i,2))
				return 0;
	}
	for (int i=1;i<=n;i++){
		int id=(mp[i][1]=='/'?0:1);
		//printf("%d %d %d\n",i,1,id);
		if (vis[i][1][id]!=T)
			if (!work(i,1,3))
				return 0;
		id=(mp[i][m]=='\\'?0:1);
		if (vis[i][m][id]!=T)
			if (!work(i,m,4))
				return 0;
	}
	return cnt==2*n*m;
}
int main(){
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	scanf("%d%d%d",&n,&m,&mo);
	for (int i=1;i<=n;i++)
		scanf("%s",mp[i]+1);
	printf("%d",check());
}

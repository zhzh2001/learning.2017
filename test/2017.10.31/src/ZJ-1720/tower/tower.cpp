#include<ctime>
#include<cstdio>
#define ll long long
using namespace std;
namespace WildFlower{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(int x){
		static int a[25],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar('0'+a[top--]));
		puts("");
	}
}
using namespace WildFlower;
int n,q,l,r,d;
struct node{
	ll lv,rv,tg;
	int ld,lud,lu;
	int rd,rud,ru;
	int mx;
}t[265000];
int max(int x,int y){
	return x>y?x:y;
}
void pushup(node &a,node b,node c,int l,int r,int mid){
	a.lv=b.lv;
	a.rv=c.rv;
	a.lu=b.lu;
	a.ru=c.ru;
	a.ld=b.ld;
	a.rd=c.rd;
	a.lud=b.lud;
	a.rud=c.rud;
	a.tg=0;
	a.mx=max(b.mx,c.mx);
	if (b.rv<c.lv){
		if (b.lu==mid){
			a.lu=c.lu;
			a.lud=c.lud;
		}
		if (c.ru==mid+1)
			a.ru=b.ru;
		if (c.rd==mid+1)
			a.rud=b.ru;
		if (c.rud==mid+1)
			a.rud=b.ru;
		a.mx=max(a.mx,c.lu-b.ru+1);
		a.mx=max(a.mx,c.lud-b.ru+1);
		a.mx=max(a.mx,c.lud-b.ru+1);
	}
	if (b.rv>c.lv){
		if (b.ld==mid)
			a.ld=c.ld;
		if (b.lu==mid)
			a.lud=c.ld;
		if (b.lud==mid)
			a.lud=c.ld;
		if (c.rd==mid+1){
			a.rd=b.rd;
			a.rud=b.rud;
		}
		a.mx=max(a.mx,c.ld-b.ru+1);
		a.mx=max(a.mx,c.ld-b.rd+1);
		a.mx=max(a.mx,c.ld-b.rud+1);
	}
	a.mx=max(a.mx,a.lu-l+1);
	a.mx=max(a.mx,a.ld-l+1);
	a.mx=max(a.mx,a.lud-l+1);
	a.mx=max(a.mx,r-a.ru+1);
	a.mx=max(a.mx,r-a.rd+1);
	a.mx=max(a.mx,r-a.rud+1);
}
void pushdown(int k){
	if (!t[k].tg) return;
	t[k*2].lv+=t[k].tg;
	t[k*2].rv+=t[k].tg;
	t[k*2].tg+=t[k].tg;
	t[k*2+1].lv+=t[k].tg;
	t[k*2+1].rv+=t[k].tg;
	t[k*2+1].tg+=t[k].tg;
	t[k].tg=0;
}
void build(int k,int l,int r){
	if (l==r){
		t[k].rv=t[k].lv=read();
		t[k].ld=t[k].lud=l;
		t[k].rd=t[k].rud=l;
		t[k].lu=t[k].ru=l;
		t[k].mx=1;
		return;
	}
	int mid=(l+r)/2;
	build(k*2,l,mid);
	build(k*2+1,mid+1,r);
	pushup(t[k],t[k*2],t[k*2+1],l,r,mid);
}
void add(int k,int l,int r,int x,int y,int v){
	if (l==x&&r==y){
		t[k].lv+=v;
		t[k].rv+=v;
		t[k].tg+=v;
		return;
	}
	pushdown(k);
	int mid=(l+r)/2;
	if (y<=mid) add(k*2,l,mid,x,y,v);
	else if (x>mid) add(k*2+1,mid+1,r,x,y,v);
	else add(k*2,l,mid,x,mid,v),
		 add(k*2+1,mid+1,r,mid+1,y,v);
	pushup(t[k],t[k*2],t[k*2+1],l,r,mid);
}
int main(){
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n=read();
	build(1,1,n);
	q=read();
	while (q--){
		l=read(); r=read(); d=read();
		add(1,1,n,l,r,d);
		write(t[1].mx);
	}
}

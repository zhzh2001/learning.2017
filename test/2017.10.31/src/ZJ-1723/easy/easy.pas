program ec12;
var 
	n,m,i,j,dis:longint;
	p:int64;
	visx,visy:array[1..1002,1..1002] of boolean;
	a:array[1..1001,1..10001] of char;
function pan(x,y,dis:longint):boolean;
begin
	pan:=false;
	if dis=1 then
	begin 
		if x=1 then 
		begin 	
			visx[1,y]:=false;
			exit(true);
		end;
	end;
	if dis=2 then 
	begin 
		if y=m then 
		begin 
			visy[x,m+1]:=false;
			exit(true);
		end;
	end;
	if dis=3 then 
	begin 
		if x=n then 
		begin 
			visx[n+1,m]:=false;
			exit(true);
		end;
	end;
	if dis=4 then 
	begin 
		if y=1 then 
		begin 
			visy[x,1]:=false;
			exit(true);
		end;
	end;
end;
procedure solve(x,y,dis:longint);
var 
	x1,y1,dis1:longint;
begin 
	x1:=x;
	y1:=y;
	dis1:=dis;
	while true do 
	begin 
		if pan(x,y,dis) then 
		break;
		if dis=1 then 
		begin 
			visx[x,y]:=false;
			dec(x);
		end;
		if dis=2 then 
		begin 
			visy[x,y+1]:=false;
			inc(y);
		end;
		if dis=3 then 
		begin 
			visx[x+1,y]:=false;
			inc(x);
		end;
		if dis=4 then 
		begin 
			visy[x,y]:=false;
			dec(y);
		end;
		if a[x,y]='/' then 
		begin 
			if dis=1 then 
			dis:=2;
			if dis=2 then 
			dis:=1;
			if dis=3 then 
			dis:=4;
			if dis=4 then 
			dis:=3;
		end
		else
		begin
			if dis=1 then 
			dis:=4;
			if dis=2 then 
			dis:=3;
			if dis=3 then 
			dis:=2;
			if dis=4 then 
			dis:=1;
		end;
	end;
	if (x1=1) and (y1=1) then 
	begin 
		if ((x=1) and (y=2) and (dis=1)) or ((x=1) and (y=1) and (dis=4)) then 
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1=1) and (y1=m) then 
	begin 
		if ((x=1) and (y=m-1) and (dis=1)) or ((x=1) and (y=m) and (dis=2)) then 
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1=n) and (y1=1) then 
	begin 
		if ((x=n-1) and (y=1) and (dis=4)) or ((x=n) and (y=2) and (dis=3)) then 
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1=n) and (y1=m) then 
	begin 	
		if ((x=n) and (y=m-1) and (dis=3)) or ((x=n-1) and (y=m) and (dis=2)) then 
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1=1) and (y1>1) and (dis=1) and (x1=x) then 
	begin 	
		if abs(y-y1)=1 then
		exit
		else
		begin 	
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1>1) and (y1=1) and (dis=4) and (y=y1) then 
	begin 
		if abs(x-x1)=1 then
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1=n) and (y1>1) and (dis=3) and (x=x1) then 
	begin 
		if abs(y-y1)=1 then
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	if (x1>1) and (y1=m) and (dis=2) and (y1=y) then 
	begin 	
		if abs(x-x1)=1 then 
		exit
		else
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
end;
begin 
	assign(input,'easy.in'); reset(input);
	assign(output,'easy.out'); rewrite(output);
	fillchar(visx,sizeof(visx),true);
	fillchar(visy,sizeof(visy),true);
	readln(n,m,p);
	for i:=1 to n do
	begin 
		for j:=1 to m do
		read(a[i,j]);
		readln;
	end;
	for i:=1 to m do 
	begin 
		visx[1,i]:=false;
		if a[1,i]='/' then 
		dis:=4
		else
		dis:=2;
		solve(1,i,dis);
	end;
	for i:=1 to m do 
	begin 
		visx[n+1,i]:=false;
		if a[n,i]='/' then 
		dis:=2
		else
		dis:=4;
		solve(n,i,dis);
	end;
	for i:=1 to n do
	begin 
		visy[i,1]:=false;
		if a[i,1]='/' then
		dis:=1
		else
		dis:=3;
		solve(i,1,dis);
	end;
	for i:=1 to n do
	begin 
		visy[i,n+1]:=false;
		if a[i,n]='/' then 
		dis:=3
		else
		dis:=1;
		solve(i,n,dis);
	end;
    for i:=1 to n+1 do 
	for j:=1 to m do 
	begin 
		if visx[i,j] then 
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	for i:=1 to n do
	for j:=1 to m+1 do 
	begin 
		if visy[i,j] then 
		begin 
			writeln(0);
			close(input);
			close(output);
			halt;
		end;
	end;
	writeln(1);
	close(input);
	close(output);
end. 
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=100010;
struct segment{
	int pre,last,l,r,ans,len;
}t[N<<2];
ll a[N],red[N];
int n,m;
 segment merge(segment a,segment b)
{
	segment res;
	res.len=a.len+b.len;
	res.pre=a.pre; if (a.pre==a.len) res.pre+=b.pre;
	res.last=b.last; if (b.last=b.len) res.last+=a.last;
	res.l=a.l;
	if (a.l==a.len)
	{
		if (a.last==a.len) res.l+=max(b.l,b.pre);
		else res.l+=b.pre;
	}
	res.r=b.r;
	if (b.r==b.len)
	{
		if (b.pre==b.len) res.r+=max(a.r,a.last);
		else res.r+=a.last;
	}
	res.ans=max(a.ans,b.ans);
	res.ans=max(res.ans,max(res.l,res.r));
	if (a.last!=0) 
		if (b.pre!=0) res.ans=max(res.ans,max(a.r+max(b.l,b.pre),b.l+max(a.r,a.last)));
		else res.ans=max(res.ans,max(a.r+max(b.l,b.pre),b.l+a.last));
	else if (b.pre!=0) res.ans=max(res.ans,max(a.r+b.pre,b.l+max(a.r,a.last)));
		else res.ans=max(res.ans,max(a.r+b.pre,b.l+a.last));
	return res;
}
 void build(int k,int l,int r)
{
	if (l==r)
	{
		if (red[l]<0) t[k].pre=1;
		else if (red[r]>0) t[k].last=1;
		if (red[l]!=0) t[k].l=t[k].r=t[k].ans=1;
		t[k].len=1;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	t[k]=merge(t[k<<1],t[k<<1|1]);
	
}
 void change(int k,int l,int r,int pos,int v)
{
	if (l==pos&&r==pos)
	{
		red[pos]+=v;
		t[k].pre=t[k].last=t[k].l=t[k].r=t[k].ans=0;
		if (red[l]<0) t[k].pre=1;
		else if (red[r]>0) t[k].last=1;
		if (red[l]!=0) t[k].l=t[k].r=t[k].ans=1;
		return;
	}
	int mid=(l+r)>>1;
	if (pos<=mid) change(k<<1,l,mid,pos,v);
	else change(k<<1|1,mid+1,r,pos,v);
	t[k]=merge(t[k<<1],t[k<<1|1]);
}
 int main()
{
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i) scanf("%lld",&a[i]);
	for (int i=1;i<=n;++i) red[i]=a[i]-a[i-1];
	red[1]=0;
	build(1,1,n);
	scanf("%d",&m);
	for (int i=1;i<=m;++i)
	{
		int l,r,v;
		scanf("%d%d%d",&l,&r,&v);
		if (l>=2) change(1,1,n,l,v);
		if (r<=n-1) change(1,1,n,r+1,-v);
		printf("%d\n",t[1].ans+1);
	}
}

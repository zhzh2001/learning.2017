#include<stdlib.h>
#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<iostream>
#include<math.h>
#include<set>
#include<vector>
#include<string.h>
#include<map>
#include<bitset>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
struct tree{
	ll l,r,tag;
	int mx;
	int lx,rx;
	int ls,rs;
}t[N<<2];
inline void pushup(int k,int l,int r){
	t[k].l=t[k<<1].l;t[k].r=t[k<<1|1].r;
	t[k].mx=max(t[k<<1].mx,t[k<<1|1].mx);
	if(t[k<<1].r<t[k<<1|1].l) t[k].mx=max(t[k].mx,t[k<<1].rx+t[k<<1|1].ls);
	if(t[k<<1].r>t[k<<1|1].l) t[k].mx=max(t[k].mx,t[k<<1].rs+t[k<<1|1].lx);
	int mid=(l+r)>>1,R=t[k<<1].r,L=t[k<<1|1].l;
	t[k].ls=t[k<<1].ls;t[k].rs=t[k<<1|1].rs;
	if(t[k].ls==mid-l+1&&R>L) t[k].ls+=t[k<<1|1].ls;
	if(t[k].rs==r-mid&&L>R) t[k].rs+=t[k<<1].rs;
	
	t[k].rx=t[k<<1|1].rx;t[k].lx=t[k<<1].lx;
	if(t[k].rx==r-mid&&L>R) t[k].rx+=t[k<<1].rs;
	if(t[k].lx==mid-l+1&&R>L) t[k].lx+=t[k<<1|1].ls;
	if(t[k<<1].rs==mid-l+1&&L>R) t[k].lx=max(t[k].lx,t[k<<1].rs+t[k<<1|1].lx);
	if(t[k<<1|1].ls==r-mid&&L<R) t[k].rx=max(t[k].rx,t[k<<1].rx+t[k<<1|1].ls);
	t[k].mx=max(t[k].mx,t[k].ls);t[k].mx=max(t[k].mx,t[k].rs);
	t[k].mx=max(t[k].mx,t[k].lx);t[k].mx=max(t[k].mx,t[k].rx);
}
inline void pushdown(int k,int l,int r){
	ll tag=t[k].tag;t[k].tag=0;
	if(!tag||l==r) return;
	t[k<<1].l+=tag;t[k<<1].r+=tag;
	t[k<<1|1].l+=tag;t[k<<1|1].r+=tag;
	t[k<<1].tag+=tag;t[k<<1|1].tag+=tag;
}
inline void build(int k,int l,int r){
	if(l==r){t[k].l=t[k].r=read();t[k].mx=t[k].lx=t[k].rx=t[k].ls=t[k].rs=1;return;}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	pushup(k,l,r);
}
inline void update(int k,int l,int r,int x,int y,int val){
	pushdown(k,l,r);
	if(l==x&&r==y){
		t[k].l+=1ll*val;t[k].r+=1ll*val;
		t[k].tag+=1ll*val;
		return;
	}
	int mid=(l+r)>>1;
	if(y<=mid) update(k<<1,l,mid,x,y,val);
	else if(x>mid) update(k<<1|1,mid+1,r,x,y,val);
	else update(k<<1,l,mid,x,mid,val),update(k<<1|1,mid+1,r,mid+1,y,val);
	pushup(k,l,r);
}
int n,m;
int main(){
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n=read();build(1,1,n);
	m=read();
	while(m--){
		int l=read(),r=read(),d=read();
		update(1,1,n,l,r,d);
		writeln(t[1].mx);
	}
	return 0;
}

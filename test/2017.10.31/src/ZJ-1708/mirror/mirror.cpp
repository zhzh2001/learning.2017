#include<stdlib.h>
#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<iostream>
#include<math.h>
#include<set>
#include<vector>
#include<string.h>
#include<map>
#include<bitset>
#define ll long long
#define N 205
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,m,p,top,ans,a[N][N];
char s[N][N];
struct data{int x,y;}q[N];
bool f[N*4];
const int dx[]={1,0,-1,0};//�������� 
const int dy[]={0,-1,0,1};
const int P[]={1,0,3,2};
const int Q[]={3,2,1,0};
int sb[1005][1005];
inline void work(){
	For(i,1,n) For(j,1,m){
		if(s[i][j]=='*') a[i][j]=0;
		if(s[i][j]=='/') a[i][j]=1;
		if(s[i][j]=='\\')a[i][j]=2;
	}
	memset(f,0,sizeof f);
	int tot=0;
	For(i,1,m) sb[0][i]=++tot;
	For(i,1,n) sb[i][m+1]=++tot;
	Rep(i,m,1) sb[n+1][i]=++tot;
	Rep(i,n,1) sb[i][0]=++tot;
	int p=0;
	For(i,1,m){
		int x=0,y=i,s=0;
		if(f[sb[x][y]]) continue;
		f[sb[x][y]]=1;
		int xx=x,yy=y;
		while(1){
			if((x!=xx||y!=yy)&&(x<1||y<1||x>n||y>m)) break;
			p++;x+=dx[s],y+=dy[s];
			if(a[x][y]==1) s=P[s];
			else s=Q[s];
		}
		if(abs(sb[xx][yy]-sb[x][y])==1||(sb[xx][yy]==1&&sb[x][y]==tot)||(sb[xx][yy]==tot&&sb[x][y]==1)) f[sb[x][y]]=1;
		else return;
	}
	For(i,1,n){
		int x=i,y=m+1,s=1;
		if(f[sb[x][y]]) continue;
		f[sb[x][y]]=1;
		int xx=x,yy=y;
		while(1){
			if((x!=xx||y!=yy)&&(x<1||y<1||x>n||y>m)) break;
			p++;x+=dx[s],y+=dy[s];
			if(a[x][y]==1) s=P[s];
			else s=Q[s];
		}
		if(abs(sb[xx][yy]-sb[x][y])==1||(sb[xx][yy]==1&&sb[x][y]==tot)||(sb[xx][yy]==tot&&sb[x][y]==1)) f[sb[x][y]]=1;
		else return;
	}
	For(i,1,m){
		int x=n+1,y=i,s=2;
		if(f[sb[x][y]]) continue;
		f[sb[x][y]]=1;
		int xx=x,yy=y;
		while(1){
			if((x!=xx||y!=yy)&&(x<1||y<1||x>n||y>m)) break;
			p++;x+=dx[s],y+=dy[s];
			if(a[x][y]==1) s=P[s];
			else s=Q[s];
		}
		if(abs(sb[xx][yy]-sb[x][y])==1||(sb[xx][yy]==1&&sb[x][y]==tot)||(sb[xx][yy]==tot&&sb[x][y]==1)) f[sb[x][y]]=1;
		else return;
	}
	For(i,1,n){
		int x=i,y=0,s=3;
		if(f[sb[x][y]]) continue;
		f[sb[x][y]]=1;
		int xx=x,yy=y;
		while(1){
			if((x!=xx||y!=yy)&&(x<1||y<1||x>n||y>m)) break;
			p++;x+=dx[s],y+=dy[s];
			if(a[x][y]==1) s=P[s];
			else s=Q[s];
		}
		if(abs(sb[xx][yy]-sb[x][y])==1||(sb[xx][yy]==1&&sb[x][y]==tot)||(sb[xx][yy]==tot&&sb[x][y]==1)) f[sb[x][y]]=1;
		else return;
	}
	if(p==n*(m+1)+m*(n+1)) ans++;
}
inline void dfs(int x){
	if(x>top){work();return;}
	int xx=q[x].x,yy=q[x].y;
	s[xx][yy]='/';dfs(x+1);
	s[xx][yy]='\\';dfs(x+1);
}
int main(){
	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);
	n=read();m=read();p=read();
	For(i,1,n) scanf("%s",s[i]+1);
	For(i,1,n) For(j,1,m) if(s[i][j]=='*') q[++top]=(data){i,j};
	dfs(1);
	printf("%d\n",ans%p);
	return 0;
}

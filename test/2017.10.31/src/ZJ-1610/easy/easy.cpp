#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
inline int Getchar(){
	int ch=getchar();
	while (ch!='/' && ch!='\\') ch=getchar();
	return ch;
}
const int N=305;
const int dx[4]={0,1,0,-1};
const int dy[4]={-1,0,1,0};

int n,m,Mod,Dat[N][N];
void Walk(int &x,int &y,int &t){
	if (Dat[x][y]=='/') t=t^1;
		else{
			t=t^1;t=(t+2)%4;
		}
	x+=dx[t];y+=dy[t];
}
inline bool Able(int x,int y){
	if (min(x,y)<1 || x>n || y>m) return false;
	return true;
}
int main(){
	freopen("easy.in","r",stdin);
	freopen("easy.out","w",stdout);
	n=read(),m=read(),Mod=read();
	Rep(i,1,n) Rep(j,1,m) Dat[i][j]=Getchar();
	int Ans=1;
	Rep(i,1,n) Rep(j,1,m) if (j==1 || j==m){
		int x=i,y=j,t=(j==1)?2:0;
		int x_=x,y_=y;
		while (Able(x,y)) Walk(x,y,t);
		if (abs(x_-x)>1 || abs(y_-y)>1) Ans=0;
//		printf("x_=%d y_=%d x=%d y=%d\n",x_,y_,x,y);
	}
	Rep(i,1,n) Rep(j,1,m) if (i==1 || i==n){
		int x=i,y=j,t=(i==1)?1:3;
		int x_=x,y_=y;
		while (Able(x,y)) Walk(x,y,t);
		if (abs(x_-x)>1 || abs(y_-y)>1) Ans=0;	
	}
	printf("%d\n",Ans);
}
/*
3 3 107
/\/
//\
//\
*/

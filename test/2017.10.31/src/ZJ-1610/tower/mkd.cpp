#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int INF=1e9;
int n,q;
int main(){
	freopen("tower.in","w",stdout);
	srand(time(0));
	Rep(i,1,10) rand();
	printf("%d\n",n=5000);
	Rep(i,1,n) printf("%d ",rand()%INF+1);puts("");
	printf("%d\n",q=5000);
	Rep(i,1,q){
		int l=rand()%n+1,r=rand()%n+1,d=rand()%INF+1;
		if (l>r) swap(l,r);
		printf("%d %d %d\n",l,r,d);
	}
}

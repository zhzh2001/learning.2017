#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=100005;

int n,a[N];ll Sum[N];
struct Segment_Tree{
	int lup[N*4],rup[N*4],low[N*4],row[N*4];
	int lir[N*4],rit[N*4],res[N*4];
	#define mid (L+R>>1)
	void Pushup(int L,int R,int p){
		lup[p]=lup[p<<1];
		low[p]=low[p<<1];
		lir[p]=lir[p<<1];
		if (lup[p]==(mid-L+1)) lup[p]=lup[p]+lup[p<<1|1];
		if (low[p]==(mid-L+1)) low[p]=low[p]+low[p<<1|1];
		if (lir[p]==(mid-L+1)) lir[p]=lir[p]+low[p<<1|1];
		rup[p]=rup[p<<1|1];
		row[p]=row[p<<1|1];
		rit[p]=rit[p<<1|1];
		if (rup[p]==(R-mid)) rup[p]=rup[p]+rup[p<<1];
		if (row[p]==(R-mid)) row[p]=row[p]+row[p<<1];
		if (rit[p]==(R-mid)) rit[p]=rit[p]+rup[p<<1];
		
		lir[p]=max(lir[p],lup[p<<1]);
		if (lup[p<<1]==(mid-L+1)) lir[p]=max(lir[p],lup[p<<1]+lir[p<<1|1]);
//		printf("rit=%d\n",rit[p]);
		rit[p]=max(rit[p],row[p<<1|1]);
		if (row[p<<1|1]==(R-mid)) rit[p]=max(rit[p],row[p<<1|1]+rit[p<<1]);
		if (lir[p]==(R-L+1)) rit[p]=max(rit[p],lir[p]);
		if (rit[p]==(R-L+1)) lir[p]=max(lir[p],rit[p]);
		res[p]=0;
		res[p]=max(res[p],max(res[p<<1],res[p<<1|1]));
		res[p]=max(res[p],rit[p<<1]+low[p<<1|1]);
		res[p]=max(res[p],rup[p<<1]+lir[p<<1|1]);
//		printf("L=%d R=%d lir=%d rit=%d\n",L,R,lir[p],rit[p]);
		res[p]=max(res[p],lir[p]);
		res[p]=max(res[p],rit[p]);
	}
	void Insert(int L,int R,int p,int pos,int val){
		if (L==R){
			Sum[L]=val;
			lup[p]=rup[p]=low[p]=row[p]=lir[p]=rit[p]=res[p]=0;
//			printf("Sum[L]=%d\n",Sum[L]);
			if (Sum[L]>0) lup[p]=rup[p]=lir[p]=rit[p]=1;
			if (Sum[L]<0) low[p]=row[p]=lir[p]=rit[p]=1;
			return;
		}
		if (pos<=mid) Insert(L,mid,p<<1,pos,val);
			else Insert(mid+1,R,p<<1|1,pos,val);
		Pushup(L,R,p);
	}
	void Ask(){
		printf("%d\n",res[1]+1);
//		printf("res=%d\n",res[1]);
//		printf("lup=%d low=%d lir=%d\n",lup[1],low[1],lir[1]);
//		printf("rup=%d row=%d rit=%d\n",rup[1],row[1],rit[1]);
	}
}T;
int main(){
	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);
	n=read();
	Rep(i,1,n) a[i]=read();
	Rep(i,1,n) Sum[i]=a[i]-a[i-1];
	Rep(i,2,n) T.Insert(2,n,1,i,Sum[i]);
	int q=read();
//	T.Ask();
	Rep(i,1,q){
		int x=read(),y=read(),d=read();
		if (x>1) T.Insert(2,n,1,x,Sum[x]+d);
		if (y<n) T.Insert(2,n,1,y+1,Sum[y+1]-d);
		T.Ask();
//		printf("seq:");Rep(j,2,n) printf("%d ",Sum[j]);puts("");
	}
}
/*
5
0 2 2 4 4
0
*/

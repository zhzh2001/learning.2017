#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<set>
#include<map>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=100005;

int n,a[N];
int main(){
	freopen("tower.in","r",stdin);
	freopen("tower.ans","w",stdout);
	n=read();
	Rep(i,1,n) a[i]=read();
	int q=read();
	Rep(i,1,q){
		int l=read(),r=read(),d=read();
		Rep(j,l,r) a[j]+=d;
		int Ans=0;
		Rep(j,1,n){
			int lef=j,rif=j;
			while (lef>1 && a[lef-1]<a[lef]) lef--;
			while (rif<n && a[rif+1]<a[rif]) rif++;
//			printf("j=%d lef=%d rif=%d\n",j,lef,rif);
			Ans=max(Ans,rif-lef+1);
		}
		printf("%d\n",Ans);
	}
}

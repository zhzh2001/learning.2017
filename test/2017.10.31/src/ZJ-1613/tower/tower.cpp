#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){
		if (ch=='-') f=-1;else f=1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

const int N=4e5+7;
int n,m,a[N];


struct Seg_Tree{
	#define ls (pos<<1)
	#define rs (pos<<1|1)
	int Res[N];
	ll Lnum[N],Rnum[N];
//	int Lup[N],Ldn[N],Rup[N],Rdn[N];
	int Ldn[N],Rup[N];
	int lx[N],rx[N];
	int Lres[N],Rres[N];
	
	void Pushup(int pos){
		Res[pos]=max(Res[ls],Res[rs]);
		if (Rnum[ls]>0) Res[pos]=max(Res[pos],Rres[ls]+Lres[rs]);
		if (Rnum[ls]<0) Res[pos]=max(Res[pos],Rres[ls]+Ldn[rs]);	
		Lnum[pos]=Lnum[ls],Rnum[pos]=Rnum[rs];
		lx[pos]=lx[ls],rx[pos]=rx[rs];
//		Lup[pos]=Lup[ls];
		Ldn[pos]=Ldn[ls];
//		if (Lup[ls]==(r[ls]-l[ls]+1)) Lup[pos]+=Lup[rs];
		if (Ldn[ls]==(rx[ls]-lx[ls]+1)) Ldn[pos]+=Ldn[rs];	
		Rup[pos]=Rup[rs];
		if (Rup[rs]==(rx[rs]-lx[rs]+1)) Rup[pos]+=Rup[ls];
		
		Lres[pos]=Lres[ls],Rres[pos]=Rres[rs];
		if (Lres[pos]==(rx[ls]-lx[ls]+1)){
			if (Rnum[ls]>0) Lres[pos]+=Lres[rs];
			if (Rnum[ls]<0) Lres[pos]+=Ldn[rs];
		}
		if (Rres[pos]==(rx[rs]-lx[rs]+1)){
			if (Lnum[rs]>0) Rres[pos]+=Rup[ls];
			if (Lnum[rs]<0) Rres[pos]+=Rres[ls];
		}
		
//		printf("L:%d R:%d\n",lx[pos],rx[pos]);
//		printf("Lnum %d  Rnum %d\n",Lnum[pos],Rnum[pos]);
//		printf("Ldn %d  Rup %d\n",Ldn[pos],Rup[pos]);
//		printf("Lres %d  Rres %d  Res %d\n",Lres[pos],Rres[pos],Res[pos]);
//		printf("\n");
	}
	void Check_Tr(int pos,ll num){
		if (num!=0) Res[pos]=Lres[pos]=Rres[pos]=1;
		else Res[pos]=Lres[pos]=Rres[pos]=0;
		Ldn[pos]=Rup[pos]=0;
		if (num>0) Rup[pos]=1;
		if (num<0) Ldn[pos]=1;
	}
	void Build(int pos,int l,int r){
		lx[pos]=l,rx[pos]=r;
		if (l==r){
			Lnum[pos]=Rnum[pos]=(a[l+1]-a[l]);
			Check_Tr(pos,Lnum[pos]);
			return;
		}
		int mid=(l+r)>>1;
		Build(ls,l,mid);
		Build(rs,mid+1,r);
		Pushup(pos);
	}
	void Update(int pos,int l,int r,int x,int num){
//		printf("%d %d %d %d %d\n",pos,l,r,x,num);
		if (l==r){
			Lnum[pos]+=num;
			Rnum[pos]+=num;
			Check_Tr(pos,Lnum[pos]);
			return;
		}
		int mid=(l+r)>>1;
		if (x<=mid) Update(ls,l,mid,x,num);
		else Update(rs,mid+1,r,x,num);
		Pushup(pos);
	}
	
	#undef ls
	#undef rs
}Tr;


int main(){
	// say hello

	freopen("tower.in","r",stdin);
	freopen("tower.out","w",stdout);

	n=read();
	For(i,1,n) a[i]=read();
	if (n>1) Tr.Build(1,1,n-1);
	m=read();
	if (n==1){
		For(i,1,m) printf("1\n");
		return 0;
	}
	For(p,1,m){
		int l=read(),r=read(),d=read();
		if (l-1>0) Tr.Update(1,1,n-1,l-1,d);
		if (r+1<=n) Tr.Update(1,1,n-1,r,-d);
		printf("%d\n",Tr.Res[1]+1);
	}

	// say goodbye
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){
		if (ch=='-') f=-1;else f=1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

const int N=2e3+7;
int n,m,P,Fx,Fy,Flag_Check;
char s[N][N];
int vis[N][N][5],tot_vis;
int X[5]={0,-1,0,1,0},Y[5]={0,0,1,0,-1};

int Out(int x,int y){
	if (x<1||y<1||x>n||y>m) return 1;
	return 0;
}
int Near(int x1,int y1,int x2,int y2){
	if (abs(x1-x2)<=1&&abs(y1-y2)<=1) return 1;
	return 0;
}

void Dfs(int x,int y,int k){
	if (!Flag_Check) return;
//	printf("%d %d %d %c\n",x,y,k,s[x][y]);
	if (vis[x][y][k]==tot_vis){
		Flag_Check=0;
		return;
	}
	vis[x][y][k]=tot_vis;
	if (x==Fx&&y==Fy){
		x+=X[k],y+=Y[k];
		Dfs(x,y,k);
		return;
	}
	if (Out(x,y)){
		if (!Near(x,y,Fx,Fy)){
//			printf("%d %d %d %d\n",x,y,Fx,Fy);
			Flag_Check=0;
		}
		return;
	}
	if (s[x][y]=='\\'){
		if (k==1) k=4;
		else if (k==2) k=3;
		else if (k==3) k=2;
		else if (k==4) k=1;
	}
	if (s[x][y]=='/'){
		if (k==1) k=2;
		else if (k==2) k=1;
		else if (k==3) k=4;
		else if (k==4) k=3;
	}
	x+=X[k],y+=Y[k];
	Dfs(x,y,k);
}

int Check(int x1,int y1,int x2,int y2,int opt){
	if (opt==1){
		if (x1>x2) swap(x1,x2);
		if (vis[x1][y1][1] || vis[x2][y2][3]) return 1;
	}
	if (opt==2){
		if (y1>y2) swap(y1,y2);
		if (vis[x1][y1][4] || vis[x2][y2][2]) return 1;
	}
//	printf("%d %d %d %d\n",x1,y1,x2,y2,opt);
	return 0;
}

int Check(){
	Flag_Check=1;
	For(i,0,n+1) For(j,0,m+1) For(k,1,4) vis[i][j][k]=0;
	tot_vis=0; 
	For(i,1,n) tot_vis++,Fx=i,Fy=0,Dfs(Fx,Fy,2);
	For(i,1,n) tot_vis++,Fx=i,Fy=m+1,Dfs(Fx,Fy,4);
	For(i,1,m) tot_vis++,Fx=0,Fy=i,Dfs(Fx,Fy,3);
	For(i,1,m) tot_vis++,Fx=n+1,Fy=i,Dfs(Fx,Fy,1);
	if (!Flag_Check) return 0;
	For(i,1,n) For(j,1,m) {
		if (!Check(i,j,i-1,j,1) || !Check(i,j,i+1,j,1)
		|| !Check(i,j,i,j-1,2) || !Check(i,j,i,j+1,2)){
			return 0;
		}
	}
//	For(i,1,n) {
//		For(j,1,m) printf("%c",s[i][j]);
//		printf("\n");
//	}
//	printf("\n");
	return 1;
}

int ans=0;

void Solve(int x,int y){
	if (y>m){
		y=1,x++;
		if (x>n) ans+=Check();
	}
	if (s[x][y]=='*'){
		s[x][y]='/';
		Solve(x,y+1);
		s[x][y]='\\';
		Solve(x,y+1);
		s[x][y]='*';
	}
	else if (s[x][y]=='\\'||s[x][y]=='/') Solve(x,y+1);
}

int main(){
	// say hello

	freopen("mirror.in","r",stdin);
	freopen("mirror.out","w",stdout);

	n=read(),m=read(),P=read();
	For(i,1,n) scanf("%s",s[i]+1);
	int flag=0;
	For(i,1,n) For(j,1,m){
		if (s[i][j]=='*') flag++;
	}
	if (flag==0){
		printf("%d",Check());
		return 0;
	}
	Solve(1,1);
	printf("%d\n",ans%P);

	// say goodbye
}


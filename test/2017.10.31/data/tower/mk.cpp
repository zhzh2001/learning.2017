#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
const int N=100000,P=1000000000,D=10000;
int i,j,k,n,m;
int a[N+10];
int R() {
	return rand()<<15|rand();
}
int main() {
	srand((int) time(0));
	freopen("tower.in","w",stdout);
	n=N-R()%(N/10+1);
	m=N-R()%(N/10+1);
	printf("%d\n",n);
	int k=R()%n+1;
	for (i=1;i<=k;i++) a[i]=a[i-1]+R()%D+1;
	for (i=n;i>k;i--) a[i]=a[i+1]+R()%D+1;
	for (i=1;i<n;i++) printf("%d ",a[i]);
	printf("%d\n",a[n]);
	printf("%d\n",m);
	for (i=1;i<=m;i++) {
		int l=R()%n+1,r=R()%n+1;
		if (l>r) {
			int t=l;l=r;r=t;
		}
		if (rand()&0) {
			int t=rand()&1;
			if (l+t<r) r=l+t;
		}
		//l=r=R()%n+1;
		printf("%d %d %d\n",l,r,R()%P+1);
	}
}

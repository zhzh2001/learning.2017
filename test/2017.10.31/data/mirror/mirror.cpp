#include<cstdio>
using namespace std;
typedef long long ll;
const int N=1010,M=220,NN=1001000;
int i,j,k,n,m,P,En;
int f[NN],b[NN],h[NN],a[M][M];
char s[N][N];
struct edge { int s,n;} E[NN<<2];
int ksm(int x,int y) {
	int z=1;
	for (;y;y>>=1,x=(ll) x*x%P) if (y&1) z=(ll) z*x%P;
	return z;
}
int getf(int x) {
	if (f[x]==x) return x;
	return f[x]=getf(f[x]);
}
void add(int x1,int y1,int x2,int y2) {
	int t1=(x1-1)*(m+1)+y1,t2=(x2-1)*(m+1)+y2;
	E[++En].s=t2;E[En].n=h[t1];h[t1]=En;
	E[++En].s=t1;E[En].n=h[t2];h[t2]=En;
}
bool merge(int x1,int y1,int x2,int y2) {
	int t1=(x1-1)*(m+1)+y1,t2=(x2-1)*(m+1)+y2;
	if (getf(t1)==getf(t2)) return 1;
	f[f[t1]]=f[t2];
	return 0;
}
int work(int n) {
	int i,j,k,ans=1;
	/*for (i=1;i<=n;i++) {
		for (j=1;j<=n;j++) printf("%d ",a[i][j]);
		puts("");
	}*/
	for (i=1;i<=n;i++) {
		if (!a[i][i]) {
			for (j=i+1;j<=n;j++) if (a[j][i]) {
				for (k=1;k<=n;k++) {
					int t=a[i][k];a[i][k]=a[j][k];a[j][k]=t;
				}
				ans=(P-ans)%P;
				break;
			}
		}
		ans=(ll) ans*a[i][i]%P;
		if (!ans) return 0;
		int ny=ksm(a[i][i],P-2);
		for (j=i+1;j<=n;j++) if (a[j][i]) {
			int t=(ll) ny*a[j][i]%P;
			t=(P-t)%P;
			for (k=i;k<=n;k++) a[j][k]=((ll) t*a[i][k]+a[j][k])%P;
		}
	}
	return ans;
}
int work0() {
	int i,j,k,nm=0;En=0;
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++) {
			if ((i+j)&1) {
				if (s[i][j]=='/') {
					if (merge(i,j+1,i+1,j)) return 0;
				}
				if (s[i][j]=='*') add(i,j+1,i+1,j);
			}
			else {
				if (s[i][j]=='\\') {
					if (merge(i,j,i+1,j+1)) return 0;
				}
				if (s[i][j]=='*') add(i,j,i+1,j+1);
			}
		}
	for (i=1;i<=n+1;i++)
		for (j=1;j<=m+1;j++) if (!((i+j)&1)) {
			int t=(i-1)*(m+1)+j;
			if (!b[getf(t)]) b[f[t]]=++nm;
			b[t]=b[f[t]];
			//printf("%d\n",b[t]);
		}
	if (nm>=210) return 0; 
	for (i=1;i<=nm;i++)
		for (j=1;j<=nm;j++) a[i][j]=0;
	for (i=1;i<=n+1;i++)
		for (j=1;j<=m+1;j++) if (!((i+j)&1)) {
			int t=(i-1)*(m+1)+j;
			int t1=b[t];
			for (k=h[t];k;k=E[k].n) {
				int t2=b[E[k].s];
				if (t1==t2) continue;
				a[t1][t2]=(a[t1][t2]+P-1)%P;
				a[t1][t1]=(a[t1][t1]+1)%P;
			}
		}
	return work(nm-1);
}
int work1() {
	int i,j,k,nm=0;En=0;
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++) {
			if ((i+j)&1) {
				if (s[i][j]=='\\') {
					if (merge(i,j,i+1,j+1)) return 0;
				}
				if (s[i][j]=='*') add(i,j,i+1,j+1);
			}
			else {
				if (s[i][j]=='/') {
					if (merge(i,j+1,i+1,j)) return 0;
				}
				if (s[i][j]=='*') add(i,j+1,i+1,j);
			}
		}
	for (i=1;i<=n+1;i++)
		for (j=1;j<=m+1;j++) if ((i+j)&1) {
			int t=(i-1)*(m+1)+j;
			if (!b[getf(t)]) b[f[t]]=++nm;
			b[t]=b[f[t]];
		}
	if (nm>=210) return 0;
	for (i=1;i<=nm;i++)
		for (j=1;j<=nm;j++) a[i][j]=0;
	for (i=1;i<=n+1;i++)
		for (j=1;j<=m+1;j++) if ((i+j)&1) {
			int t=(i-1)*(m+1)+j;
			int t1=b[t];
			for (k=h[t];k;k=E[k].n) {
				int t2=b[E[k].s];
				if (t1==t2) continue;
				a[t1][t2]=(a[t1][t2]+P-1)%P;
				a[t1][t1]=(a[t1][t1]+1)%P;
			}
		}
	return work(nm-1);
}
int main() {
	freopen("mirror8.in","r",stdin);
	freopen("mirror8.out","w",stdout);
	scanf("%d%d%d",&n,&m,&P);
	for (i=1;i<=n;i++) scanf("%s",s[i]+1);
	int nm=(n+1)*(m+1);
	for (i=1;i<=nm;i++) f[i]=i;
	printf("%d\n",(work0()+work1())%P);
}

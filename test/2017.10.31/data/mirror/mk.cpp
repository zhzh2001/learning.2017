#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<algorithm>
using namespace std;
const int N=100,M=100,K=200;
int i,j,k,n,m,p;
int f[1001000];
char s[N+10][M+10];
int R() {
	return rand()<<15|rand();
}
int getf(int x) {
	if (f[x]==x) return x;
	return f[x]=getf(f[x]);
}
bool Check(int x,int y,int Fg) {
	if (((x+y)&1)==Fg) {
		if (getf((x-1)*(m+1)+y-1)==getf(x*(m+1)+y)) return 0;
		return 1;
	}
	else {
		if (getf(x*(m+1)+y-1)==getf((x-1)*(m+1)+y)) return 0;
		return 1;
	}
}
void work(int Fg) {
	int i,j,nm=0;
	for (i=0;i<=n;i++)
		for (j=0;j<=m;j++) if (((i+j)&1)==Fg) {
			f[i*(m+1)+j]=i*(m+1)+j;
			nm++;
		}
	for (i=1;i<nm;i++) {
		int x=R()%n+1,y=R()%m+1;
		while (!Check(x,y,Fg)) x=R()%n+1,y=R()%m+1;
		if (((x+y)&1)==Fg) s[x][y]='\\';
		else s[x][y]='/';
		if (((x+y)&1)==Fg) f[f[(x-1)*(m+1)+y-1]]=f[x*(m+1)+y];
		else f[f[x*(m+1)+y-1]]=f[(x-1)*(m+1)+y];
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++) if (!s[i][j]) {
			if (((i+j)&1)==Fg) s[i][j]='/';
			else s[i][j]='\\';
		}
}
bool check(int x) {
	for (int i=2;i*i<=x;i++) if (x%i==0) return 0;
	return 1;
}
int main() {
	freopen("mirror.in","w",stdout);
	srand((int) time(0));
	n=N-R()%(N/10+1);
	m=M-R()%(M/10+1);
	k=n*m-R()%(n*m/5+1);
	if (k>K) k=K;
	work(rand()&1);
	int tt=min(5,k);
	for (i=1;i<=tt;i++) {
		int x=R()%n+1,y=R()%m+1;
		while (s[x][y]=='*') x=R()%n+1,y=R()%m+1;
		s[x][y]='*';
	}
	for (i=tt+1;i<=k;i++) {
		int x=R()%n+1,y=R()%m+1;
		while (s[x][y]=='*' || (s[x-1][y]!='*' && s[x+1][y]!='*' && s[x][y-1]!='*' && s[x][y+1]!='*')) x=R()%n+1,y=R()%m+1;
		s[x][y]='*';
	}
	p=R()%1000000007+1;
	while (p<=2 || !check(p)) p=R()%1000000007+1;
	printf("%d %d %d\n",n,m,p);
	for (i=1;i<=n;i++) printf("%s\n",s[i]+1);
}

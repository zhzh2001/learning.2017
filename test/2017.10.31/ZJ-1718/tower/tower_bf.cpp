#include<fstream>
using namespace std;
ifstream fin("tower.in");
ofstream fout("tower.out");
const int N=100005;
int a[N],b[N],s[N];
inline int getsign(int x)
{
	if(x>0)
		return 1;
	if(x<0)
		return -1;
	return 0;
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	int m;
	fin>>m;
	while(m--)
	{
		int l,r,val;
		fin>>l>>r>>val;
		for(int i=l;i<=r;i++)
			a[i]+=val;
		int cnt=0;
		s[0]=0;
		for(int i=2;i<=n;i++)
		{
			if(getsign(a[i]-a[i-1])!=s[cnt])
			{
				b[++cnt]=0;
				s[cnt]=getsign(a[i]-a[i-1]);
			}
			b[cnt]++;
		}
		int ans=0;
		for(int i=1;i<=cnt;i++)
		{
			if(i<cnt&&s[i]==1&&s[i+1]==-1)
				ans=max(ans,b[i]+b[i+1]+1);
			if(s[i])
				ans=max(ans,b[i]+1);
		}
		fout<<ans<<endl;
	}
	return 0;
}

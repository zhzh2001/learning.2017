#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("tower.in");
ofstream fout("tower.out");
const int N=100005;
int t[N];
long long a[N];
struct node
{
	bool pos,neg;
	int bl,br,l,r,val;
}tree[N*4];
inline void pullup(int id)
{
	tree[id].l=tree[id*2].l;
	tree[id].bl=tree[id*2].bl;
	tree[id].pos=tree[id].neg=false;
	if(tree[id*2].pos)
	{
		if(tree[id*2+1].bl&2)
		{
			tree[id].l+=tree[id*2+1].l;
			tree[id].bl=3;
		}
		else if(tree[id*2+1].pos)
		{
			tree[id].l+=tree[id*2+1].l;
			tree[id].pos=true;
		}
	}
	tree[id].r=tree[id*2+1].r;
	tree[id].br=tree[id*2+1].br;
	if(tree[id*2+1].neg)
	{
		if(tree[id*2].bl&1)
		{
			tree[id].r+=tree[id*2].r;
			tree[id].br=3;
		}
		else if(tree[id*2+1].neg)
		{
			tree[id].r+=tree[id*2].r;
			tree[id].neg=true;
		}
	}
	tree[id].val=max(max(tree[id*2].val,tree[id*2+1].val),max(tree[id].l,tree[id].r));
	if((tree[id*2].br==1&&tree[id*2+1].bl)||(tree[id*2+1].bl==2&&tree[id*2].br))
		tree[id].val=max(tree[id].val,tree[id*2].r+tree[id*2+1].l);
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		if(a[l]>0)
		{
			tree[id].pos=true;
			tree[id].bl=tree[id].br=1;
			tree[id].l=tree[id].r=tree[id].val=1;
		}
		else if(a[l]<0)
		{
			tree[id].neg=true;
			tree[id].bl=tree[id].br=2;
			tree[id].l=tree[id].r=tree[id].val=1;
		}
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id);
	}
}
void modify(int id,int l,int r,int x,int val)
{
	if(l==r)
	{
		a[l]+=val;
		tree[id].pos=tree[id].neg=false;
		if(a[l]>0)
		{
			tree[id].pos=true;
			tree[id].bl=tree[id].br=1;
			tree[id].l=tree[id].r=tree[id].val=1;
		}
		else if(a[l]<0)
		{
			tree[id].neg=true;
			tree[id].bl=tree[id].br=2;
			tree[id].l=tree[id].r=tree[id].val=1;
		}
		else
		{
			tree[id].bl=tree[id].br=0;
			tree[id].l=tree[id].r=tree[id].val=0;
		}
	}
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(id*2,l,mid,x,val);
		else
			modify(id*2+1,mid+1,r,x,val);
		pullup(id);
	}
}
namespace bruteforce
{
int a[N],b[N],s[N];
inline int getsign(int x)
{
	if(x>0)
		return 1;
	if(x<0)
		return -1;
	return 0;
}
void main()
{
	fin.close();
	fin.open("tower.in");
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	int m;
	fin>>m;
	while(m--)
	{
		int l,r,val;
		fin>>l>>r>>val;
		for(int i=l;i<=r;i++)
			a[i]+=val;
		int cnt=0;
		s[0]=0;
		for(int i=2;i<=n;i++)
		{
			if(getsign(a[i]-a[i-1])!=s[cnt])
			{
				b[++cnt]=0;
				s[cnt]=getsign(a[i]-a[i-1]);
			}
			b[cnt]++;
		}
		int ans=0;
		for(int i=1;i<=cnt;i++)
		{
			if(i<cnt&&s[i]==1&&s[i+1]==-1)
				ans=max(ans,b[i]+b[i+1]+1);
			if(s[i])
				ans=max(ans,b[i]+1);
		}
		fout<<ans<<endl;
	}
}
};
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>t[i];
	for(int i=2;i<=n;i++)
		a[i]=t[i]-t[i-1];
	build(1,2,n);
	int m;
	fin>>m;
	if(n*m<=1e7)
	{
		bruteforce::main();
		return 0;
	}
	while(m--)
	{
		int l,r,val;
		fin>>l>>r>>val;
		if(l>1)
			modify(1,2,n,l,val);
		if(r+1<=n)
			modify(1,2,n,r+1,-val);
		fout<<tree[1].val+1<<endl;
	}
	return 0;
}

#include<fstream>
#include<string>
using namespace std;
ifstream fin("mirror.in");
ofstream fout("mirror.out");
const int N=100,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
string mat[N];
int n,m,p,ans,f[N][N][4];
bool row[N+1][N],col[N][N+1];
int go(int x,int y,int dir)
{
	if(x<0)
		return y;
	if(y==m)
		return x+m;
	if(x==n)
		return m-y-1+n+m;
	if(y<0)
		return n-x-1+n+m+m;
	if(~f[x][y][dir])
		return f[x][y][dir];
	int newd;
	if(mat[x][y]=='\\')
		switch(dir)
		{
		case 0:
			newd=2;
			break;
		case 1:
			newd=3;
			break;
		case 2:
			newd=0;
			break;
		case 3:
			newd=1;
			break;
		}
	else
		newd=3-dir;
	int nx=x+dx[newd],ny=y+dy[newd];
	switch(newd)
	{
	case 0:
		row[x][y]=true;
		break;
	case 1:
		row[nx][ny]=true;
		break;
	case 2:
		col[x][y]=true;
		break;
	case 3:
		col[nx][ny]=true;
		break;
	}
	return f[x][y][dir]=go(nx,ny,newd);
}
void dfs(int x,int y)
{
	if(y==m)
	{
		x++;
		y=0;
	}
	if(x==n)
	{
		for(int i=0;i<n;i++)
			for(int j=0;j<m;j++)
			{
				row[i][j]=col[i][j]=false;
				for(int k=0;k<4;k++)
					f[i][j][k]=-1;
			}
		for(int i=0;i<(n+m)*2;i++)
		{
			int ret;
			if(i<m)
				ret=go(0,i,1);
			else if(i<n+m)
				ret=go(i-m,m-1,2);
			else if(i<n+m+m)
				ret=go(n-1,m-(i-n-m)-1,0);
			else
				ret=go(n-(i-n-m-m)-1,0,3);
			if(ret!=(i+1)%((n+m)*2)&&ret!=(i+(n+m)*2-1)%((n+m)*2))
				return;
		}
		for(int i=1;i<n;i++)
			for(int j=0;j<m;j++)
				if(!row[i][j])
					return;
		for(int i=0;i<n;i++)
			for(int j=1;j<m;j++)
				if(!col[i][j])
					return;
		ans++;
		return;
	}
	if(mat[x][y]=='*')
	{
		mat[x][y]='\\';
		dfs(x,y+1);
		mat[x][y]='/';
		dfs(x,y+1);
		mat[x][y]='*';
	}
	else
		dfs(x,y+1);
}
int main()
{
	fin>>n>>m>>p;
	for(int i=0;i<n;i++)
		fin>>mat[i];
	dfs(0,0);
	fout<<ans%p<<endl;
	return 0;
}

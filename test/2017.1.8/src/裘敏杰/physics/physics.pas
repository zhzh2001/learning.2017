var
  a,b:array[-5..200] of longint;
  p:array[-5..10500] of longint;
  i,k,n,m,ans:longint;
procedure dfs(i:longint);
var
  j:longint;    flag:boolean;
begin
  if b[i]<>0 then begin dfs(i+1); exit; end;
  flag:=true;
  if i>m then begin
    for i:=1 to m do if b[i]<b[i-1] then flag:=false;
    if flag then begin inc(ans);  ans:=ans mod 1000000007; exit; end;
end;
  for j:=1 to n do
    if (a[j]>0)  then begin
      dec(a[j]);
      b[i]:=p[j];
      dfs(i+1);
      inc(a[j]);
    end;
end;
begin
  assign(input,'physics.in');
  assign(output,'physics.out');
  reset(input);
  rewrite(output);
  k:=1;
  read(n,m);
  for i:=1 to n do read(p[i],a[i]);
  for i:=1 to m do  begin
    read(b[i]);
    if b[i]<>0 then begin
      a[b[i]]:=a[b[i]]-1;
      b[i]:=p[b[i]];
    end;
  end;
  ans:=0;
      dfs(1);
 if ans<>0 then write(ans)
   else write(-1);
  close(input);
  close(output);
end.

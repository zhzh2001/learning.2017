var
  a,b,c,k:longint;
  n:int64;
  d:array[1..10000000]  of int64;
function su(i:longint):boolean;
var j:longint;
begin
  for j:=1 to k do if d[k]=i then exit(true);
  for j:=2 to trunc(sqrt(i)) do
    if i mod j=0 then exit(false);
  d[k]:=i;
  inc(k);
  exit(true);
end;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  fillchar(d,sizeof(d),0);
  k:=1;
  read(n);
  for c:=2 to n div 4 do
    if su(c) then  for b:=2 to c do
      if su(b) then  for a:=2 to b do
        if  su(a) and (a*b*c=n) then begin write(b); exit; end;
  close(input);
  close(output);
end.

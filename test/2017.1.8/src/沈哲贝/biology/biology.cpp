#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<queue>
#define mod 1000000
#define inf 1000000000
#define ll long long
#define pi 3.1415
#define N 1000005
const double seps=1e-9;
using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
struct data1{
	ll v,sum;
} b[21];
bool flag;
ll n,tot,minn,a[21],head[1000],next[1000],vet[1000],vis[1000],answer;
void add(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
bool cmp(data1 x,data1 y){
	if (x.sum==y.sum)	return x.v<y.v;
	else return x.sum<y.sum;
}
void dfs(ll now){
	if (!now){
		if (answer<minn)	minn=answer;
		return ;
	}
	answer+=b[now].sum;
	dfs(now-1);
	answer-=b[now].sum;
	for (ll i=head[now];i;i=next[i]){
		ll v=vet[i];
		if (not vis[v] and !(b[now].v%b[v].v)){
			vis[v]=true;
			b[now].v/=b[v].v;
			b[now].sum-=b[v].sum;
			dfs(now);
			vis[v]=false;
			b[now].v*=b[v].v;
			b[now].sum+=b[v].sum;
		}
	}
	return;
}
int main(){
	freopen("biology.in","r",stdin);
	freopen("biology.out","w",stdout);
	n=read();
	for (ll i=1;i<=n;i++)	a[i]=read();
	sort(a+1,a+n+1);
	for (ll i=1;i<=n;i++){
		b[i].v=a[i];
		for (ll j=2;j<=1000001;j++)
			while (!(a[i]%j))		b[i].sum++,a[i]/=j;
		if (a[i]>1)	b[i].sum++;
	}
	sort(b+1,b+n+1,cmp);
	for (ll i=1;i<n;i++)
		for (ll j=i+1;j<=n;j++)
			if (!(b[j].v%b[i].v))	add(j,i);
	minn=10000;
	dfs(n);
	for (ll i=1;i<=n;i++)
		if (b[i].sum!=1)	minn++;
	bool flag=true;
	for (ll i=1;i<n;i++)
		if (b[n].v%b[i].v)	{	flag=false;	break;	}
			else b[n].v/=b[i].v;
	minn-=flag;
	writeln(minn+1);
}

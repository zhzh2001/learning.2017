#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<queue>
#define mod 1000000
#define inf 1000000000
#define ll long long
#define pi 3.1415
#define N 1000005
const double seps=1e-9;
//using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll n=read();
	ll p;
	for (p=2;;p++)
		if (!(n%p))	break;
	n/=p;
	for (p=2;;p++)
		if (!(n%p))	break;
	writeln(p);
}

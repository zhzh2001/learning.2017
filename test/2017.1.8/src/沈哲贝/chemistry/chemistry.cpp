#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<queue>
#define mod 1000000
#define inf 1000000000
#define ll long long
#define pi 3.1415
#define N 1000005
const double seps=1e-9;
//using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
char s[100001],ch[101][2];
ll n,m,num,sum,now,hash[100000];
ll dfs(){
	ll ans=0,temp,number;
	while ((now<sum) and (s[now]!=')'))
	{
		if (s[now]=='(')	{
			now++;
			temp=dfs();
			number=0;
			while (s[now]>='0' and s[now]<='9'){
				number=number*10+s[now]-'0';
				now++;
			}
			if (number)	ans+=temp*number;
				else ans+=temp;
		}
		else if (s[now+1]>='a' and s[now+1]<='z'){
			temp=hash[s[now]*127+s[now+1]];
			now+=2;
			number=0;
			while (s[now]>='0' and s[now]<='9'){
				number=number*10+s[now]-'0';
				now++;
			}
			if (number)	ans+=temp*number;
				else ans+=temp;
		}	
		else if (s[now]>='A' and s[now]<='Z')
		{
			temp=hash[s[now]*127],now++;
			number=0;
			while (s[now]>='0' and s[now]<='9'){
				number=number*10+s[now]-'0';
				now++;
			}
			if (number)	ans+=temp*number;
				else ans+=temp;
		}
	}
	now++;
	return ans;
}
int main(){
	freopen("chemistry.in","r",stdin);
	freopen("chemistry.out","w",stdout);
	scanf("%I64d%I64d",&n,&m);
	for (ll i=1;i<=n;i++)	{
		scanf("%s%I64d",ch[i],&num);
		if (strlen(ch[i])==1)	hash[ch[i][0]*127]=num;
			else hash[ch[i][0]*127+ch[i][1]]=num;
	}
	for (ll i=1;i<=m;i++){
		scanf("%s",s);
		sum=strlen(s);
		now=0;
		writeln(dfs());
	}
}

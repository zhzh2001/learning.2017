#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define P 1000000007
#define ll long long
using namespace std;

struct st
{
	int a,b,c;
	bool operator <(const st&B) const {return a<B.a;}
} a[105];
int N,M,f[105][105],b[105];

void init()
{
	scanf("%d%d",&N,&M);
	for (int i=1; i<=N; i++) scanf("%d%d",&a[i].a,&a[i].b),a[i].c=i;
	for (int i=1; i<=M; i++) scanf("%d",&b[i]);
}

void doit()
{
	sort(a+1,a+N+1);
	for (int i=0; i<=N; i++) f[0][i]=1;
	for (int i=1; i<=M; i++)
		for (int j=1; j<=N; j++)
		{
			(f[i][j]+=f[i][j-1])%=P;
			for (int k=1; k<=a[j].b&&k<=i&&(!b[i-k+1]||b[i-k+1]==a[j].c); k++) (f[i][j]+=f[i-k][j-1])%=P;
		}
	printf("%d\n",f[M][N]?f[M][N]:-1);
}

int main()
{
	freopen("physics.in","r",stdin);
	freopen("physics.out","w",stdout);
	init();
	doit();
	return 0;
}

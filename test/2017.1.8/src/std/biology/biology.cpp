#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
using namespace std;

int N,s[25],ans;
ll a[25];

void init()
{
    scanf("%d",&N);
    for (int i=1; i<=N; i++) cin>>a[i];
    sort(a+1,a+N+1);
}

void dfs(int t,int k,int c)
{
    if (k>=ans) return;
    if (!t) {ans=min(ans,k+(c>1)); return;}
    if (s[t]>1) k++;
    dfs(t-1,k+s[t],c+1);
    for (int i=t+1; i<=N; i++) if (a[i]%a[t]==0) a[i]/=a[t],dfs(t-1,k,c),a[i]*=a[t];
}

void doit()
{
    for (int i=1; i<=N; i++)
    {
        ll x=a[i],j=2;
        for (;j*j<=x; j++)
            for (;x%j==0; x/=j) s[i]++;
        if (x>1) s[i]++;
    }
    ans=1e9,dfs(N,0,0),printf("%d\n",ans);
}

int main()
{
	freopen("biology.in","r",stdin);
	freopen("biology.out","w",stdout);
    init();
    doit();
    return 0;
}

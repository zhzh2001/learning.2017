#include <map>
#include <cmath>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define maxn 1000005
using namespace std;

int N,M;
map<string,int> F;
char s[maxn];
ll q[maxn];

ll calc(char*s)
{
	int n=strlen(s+1),t=0;
	for (int i=1; i<=n; i++) if (s[i]=='(') q[++t]=0;
	else if (s[i]==')')
	{
		ll x=0;
		for (;q[t]; t--) x+=q[t];
		q[t]=x;
	}
	else if (isalpha(s[i]))
	{
		string x; x=s[i];
		if (islower(s[i+1])) x+=s[++i];
		q[++t]=F[x];
	}
	else
	{
		int x=0;
		for (;isdigit(s[i]); i++) (x*=10)+=s[i]-48;
		q[t]*=x,i--;
	}
	ll ans=0;
	for (int i=1; i<=t; i++) ans+=q[i];
	return ans;
}

void doit()
{
	scanf("%d%d",&N,&M);
	string c;
	for (int i=1,x; i<=N; i++) cin>>c>>x,F[c]=x;
	for (int i=1; i<=M; i++) scanf("%s",s+1),printf("%I64d\n",calc(s));
}

int main()
{
	freopen("chemistry.in","r",stdin);
	freopen("chemistry.out","w",stdout);
	doit();
	return 0;
}

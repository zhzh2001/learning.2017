var
        a,c:array[1..20000] of int64;
        i,j,n,t,sum,num,len:longint;
        ans:int64;
function get(x:int64):int64;
var
        i:longint;
begin
        get:=0;
        i:=2;
        while x>1 do
        begin
                while x mod i=0 do
                begin
                        inc(get);
                        x:=x div i;
                end;
                inc(i);
        end;
end;
procedure px(l,r:longint);
var
        i,j:longint;
        mid,t:int64;
begin
        i:=l;j:=r;
        mid:=a[(i+j) div 2];
        repeat
                while a[i]<mid do inc(i);
                while a[j]>mid do dec(j);
                if i<=j then
                begin
                        t:=a[i];a[i]:=a[j];a[j]:=t;
                        inc(i); dec(j);
                end;
        until i>j;
        if i<r then px(i,r);
        if l<j then px(l,j);
end;
begin
        assign(input,'biology.in');
        assign(output,'biology.out');
        reset(input); rewrite(output);
        read(n);
        for i:=1 to n do
                read(a[i]);
        px(1,n);
        i:=1;
        for i:=1 to n do
        begin
                sum:=maxlongint;
                for j:=1 to len do
                        if (a[i] mod c[j]=0) then
                        begin
                                t:=get(a[i] div c[j]);
                                if t<sum then
                                begin
                                        num:=j;
                                        sum:=t;
                                end;
                        end;
                if sum=maxlongint then
                begin
                        inc(len);
                        c[len]:=a[i];
                        ans:=ans+get(a[i])+1;
                end else
                begin
                        c[num]:=a[i];
                        inc(ans,t+1);
                end;
        end;
        write(ans);
        close(input); close(output);
end.
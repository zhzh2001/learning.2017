var
        n,m,i,j,k:longint;
        a,b:array[0..200] of longint;
        f:array[0..200,0..200] of longint;
begin
        assign(input,'physics.in');
        assign(output,'physics.out');
        read(n,m);
        for i:=1 to n do
                read(a[i],b[i]);
        for i:=0 to n do
                f[i,0]:=1;
        for i:=1 to n do
          for j:=1 to m do
            for k:=1 to b[i] do
                if j-k>=0 then f[i,j]:=(f[i,j]+f[i-1,j-k])mod 1000000007;
        if f[n,m]<>0 then write(f[n,m])
        else write(-1);
        close(input); close(output);
end.

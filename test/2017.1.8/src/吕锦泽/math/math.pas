var
        n:int64;
        i,j:longint;
        a:array[1..7500000] of boolean;
begin
        assign(input,'math.in');
        assign(output,'math.out');
        reset(input); rewrite(output);
        read(n);
        fillchar(a,sizeof(a),1); i:=2;
        for i:=2 to trunc(sqrt(n)) do
                if a[i] then
                begin
                        j:=2;
                        while i*j<=trunc(sqrt(n)) do
                        begin
                                a[i*j]:=false;
                                inc(j);
                        end;
                end;
        for i:=2 to trunc(sqrt(n)) do
                if (a[i])and(n mod i=0) then
                begin
                        j:=i;
                        while j<=trunc(sqrt(n)) do
                        begin
                                if ((n div i) mod j=0) then
                                begin
                                        write(j);
                                        close(input); close(output);
                                        halt;
                                end;
                                inc(j);
                        end;
                end;
        close(input); close(output);
end.


var
        n,m,i,j:longint;
        ch:char;  t:ansistring;
        s:array[1..100] of ansistring;
        num:array[1..100] of longint;
function work(t:ansistring):int64;
var
        len,i,j,k,tt,sum:longint;
        p:ansistring;
begin
        len:=length(t);
        i:=1; work:=0;
        while i<=len do
        begin
                if t[i]='(' then
                begin
                        tt:=pos(')',t);
                        j:=tt+1; sum:=0;
                        while (t[j]>='0')and(t[j]<='9') do
                        begin
                                sum:=sum*10+ord(t[j])-48;
                                inc(j);
                        end;
                        if sum=0 then sum:=1;
                        work:=work+sum*work(copy(t,i+1,tt-i-1));
                        i:=j;
                end else
                if (t[i]>='A')and(t[i]<='Z') then
                begin
                        p:=t[i];
                        if (t[i+1]>='a')and(t[i+1]<='z') then
                        begin
                                inc(i);
                                p:=p+t[i];
                        end;
                        j:=i+1; sum:=0;
                        while (t[j]>='0')and(t[j]<='9') do
                        begin
                                sum:=sum*10+ord(t[j])-48;
                                inc(j);
                        end;
                        if sum=0 then sum:=1;
                        for k:=1 to n do
                                if s[k]=p then
                                begin
                                        work:=work+num[k]*sum;
                                        break;
                                end;
                        i:=j;
                end;
        end;
end;
begin
        assign(input,'chemistry.in');
        assign(output,'chemistry.out');
        reset(input);  rewrite(output);
        readln(n,m);
        for i:=1 to n do
        begin
                read(ch);
                while ch<>' ' do
                begin
                        s[i]:=s[i]+ch;
                        read(ch);
                end;
                readln(num[i]);
        end;
        for i:=1 to m do
        begin
                readln(t);
                writeln(work(t));
        end;
        close(input); close(output);
end.
var
n,m:qword;
l,r,i,mid,ans,k:longint;
p,pp:boolean;
begin
  assign(input,'math.in');  reset(input);
  assign(output,'math.out');rewrite(output);
  read(n);
  l:=1;r:=46415;
  while(r>=l)do
    begin
      mid:=(r+l) div 2;
      m:=mid*mid;
      m:=m*mid;
      if m<=n then ans:=mid;
      if(m>n)then r:=mid-1 else l:=mid+1;
    end;
  l:=ans;r:=ans;
  p:=false;pp:=false;
  while true do
    begin
      if n mod l=0 then p:=true;
      if n mod r=0 then pp:=true;
      if(p)and(pp)then
        begin
          k:=n div l;
          k:=n div r;
          if k<=l then write(l) else write(r);
          exit;
        end;
      if(p) then
        begin
          write(l);  exit;
        end;
      if(pp) then
        begin
          write(r); exit;
        end;
      l:=l-1;
      r:=r+1;
    end;
  close(input);close(output);
end.

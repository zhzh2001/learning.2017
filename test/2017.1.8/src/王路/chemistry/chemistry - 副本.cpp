#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <cstring>
using namespace std;
typedef long long ll;
int n, m, set, sum;
struct node{
	string s;
	int num;
}p[105];
string str;
int stack[10000];
int top = 0;
bool cmp(node a, node b){
	return a.s < b.s;
}

int getloc(string str){
	for (int i = 0; ; i++){
		if (str[i] == ' ')
			return i;
	}
}

int sss(string ss){
	for (int i = 1; i <= n; i++){
		if (ss == p[i].s)
			return p[i].num;
	}
}

int val(string str){
	int len = str.length();
	int temp = 0, pp = 1;
	for (int i = 1; i < len; i++){
		pp *= 10;
	}
	for (int i = 0, j = pp; i < len; i++, j /= 10){
		temp += j * (str[i]-'0');
	}
	return temp;
}

int main(){
	freopen("chemistry.in", "r", stdin);
	freopen("chemistry.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= n; i++){
		cin >> p[i].s >> p[i].num;
	}
	while(m--){
		top = 0;
		int sum = 0;
		cin >> str;
		int len = str.length();
		for (int i = 0; i < len; i++){
			int lx = 0;
			string tt = "";
			if (str[i] >= 'A' && str[i] <= 'Z'){
				tt = str[i];
				while (str[i+1] >= 'a' && str[i+1] <= 'z'){
					tt = tt + str[i+1];
					i++;
				}
//				cout << tt << endl;
				int tnum = sss(tt);
				stack[top] = tnum;
				top++;
			}
			if (str[i] >= '0' && str[i] <= '9'){
				tt = str[i];
				while (str[i+1] >= '0' && str[i+1] <= '9'){
					tt = tt + str[i+1];
					i++;
				}
				int tnum = val(tt);
//				cout << tnum << endl;
				stack[top-1] = stack[top-1] * tnum;
			}
			if (str[i] == '('){
				lx = top;
			}
			if (str[i] == ')'){
				int tsum = 0;
				for (int j = lx; j < top; j++){
					tsum += stack[j];
					stack[j] = 0;
				}
				stack[top] = tsum;
				top++;
			}
		}
		for (int i = 0; i < top; i++){
			sum += stack[i];
		}
		cout << sum << endl;
	}
}

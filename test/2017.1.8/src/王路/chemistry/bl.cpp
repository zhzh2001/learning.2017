#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
typedef long long ll;
ll n, m, set, sum, leftt, rightt;
string str;
struct node{
	string elem;
	string meta;
	ll met;
}p[105];
ll top;
struct stack{
	string a[10000];
	int b[10000];

}s;

bool cmp(node a, node b){
	return a.elem < b.elem;
}

ll turn(char * t, int tt){
	ll ans = 0;
	for (int i = tt-1; i >= 0; i--){
		ans += (tt-i)*(t[i]-'0');//
	}
	return ans;
}

ll search(string ss, ll l, ll r){
	ll mid = (l+r)/2;
	while (l <= r){
		if (ss == p[mid].elem){
			return p[mid].met;
		} else if (ss > p[mid].elem){
			l = mid + 1;
		} else {
			r = mid - 1;
		}
	}
}

int main(){
	freopen("chemistry.in", "r", stdin);
	freopen("chemistry.out", "w", stdout);
	scanf("%d%d", &n, &m);
	getchar();
	for (ll i = 1; i <= n; i++){
		getline(cin, str);
		ll l = str.find(' ');
		p[set].elem = str.substr(0, l);
		p[set].meta = str.substr(l+1,str.length()-l);
		ll temp = 0;
		for (ll j = 0; j < p[set].meta.length(); j++){
			temp += (j+1)*(p[set].meta[j]);
		}
		p[set].met = temp;
		set++;
	}
	sort(p, p+set, cmp);
	for (ll i = 1; i <= m; i++){
		sum = 0;
		getline(cin, str);
		top = 0;
		ll len = str.length();
		for (ll j = 0; j < len; j++){
			if (str[j] >= 'A' && str[j] <= 'Z'){
				s.a[top] = str[j];
				top++;
			} else if (str[j] >= 'a' && str[j] <= 'z'){
				s.a[top-1] += str[j];
			} else if (str[j] >= '0' && str[j] <= '9'){
				char t[100]; int tt = 0;
				do{
					t[tt] = str[j] - '0';
					tt++;
					j++;
				}while(!str[j] >= '0' && !str[j] <= '9');
				ll num = turn(t, tt);
				s.b[top-1] = num;
			} else if (str[j] == '('){
				leftt = j;
			} else if (str[j] == ')'){
				rightt = j;
				
			}
		}
		for (ll j = 0; j < top; j++){
			if (s.b[j] == 0) s.b[j] = search(s.a[j],0,set-1);
				else s.b[j] = search(s.a[j],0,set-1)*s.b[j];
			sum += s.b[j];
		}
		cout << sum << endl;
	}
	
}

#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cstdio>
using namespace std;
long long f[101][101][101]; //在第i个位置使用了j种类的小灯泡k个的可能方案数量 
struct node{
	int v,cnt;
}a[101];
int al[1001];
inline bool cmp(node x,node y)
{
	return x.v<y.v;
}
int main()
{
	freopen("physics.in","r",stdin);
	freopen("physics.out","w",stdout);
	int n,m;
	memset(f,0,sizeof(f));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		scanf("%d%d",&a[i].v,&a[i].cnt);
	for (int i=1;i<=m;i++)
		cin>>al[i];
	sort(a+1,a+n+1,cmp);
	for (int i=1;i<=n;i++) 
	{	
		f[1][i][1]=1;
		f[1][i][0]=1;
	}
	for (int i=1;i<=m;i++)
		{
			
			for (int j=1;j<=n;j++)
			{
				for (int k=1;k<=j-1;k++)
					f[i][j][1]=(f[i][j][1]+f[i-1][k][0]);
				f[i][j][0]=f[i][j][1];//f[i,j,0]表示i位置选j种类的所有可能性 
				for (int k=2;k<=min(a[j].cnt,i+1);k++) 
				{
				
					f[i][j][k]=f[i-k+1][j][1]; //cout<<i<<' '<<j<<' '<<k<<' '<<f[i][j][k]<<endl;
					f[i][j][0]=f[i][j][0]+f[i][j][k];
				}
				f[i][j][0]%=1000000007;
			}
			
			if (al[i]!=0)
			for (int j=1;j<=n;j++) 
			if (j!=al[i])
			{
				for (int k=1;k<=a[j].cnt;k++)
				{
					f[i][j][k]=0;
				}
				f[i][j][0]=0;
			}
			//for (int j=1;j<=n;j++) for (int k=1;k<=a[j].cnt;k++)printf("%d %d %d %d\n",i,j,k,f[i][j][0]);
		}
		//cout<<f[2][2][0]<<endl;
	int ans=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=min(a[i].cnt,m);j++)
		{
			ans+=f[m][i][j];
		//	cout<<m<<' '<<i<<' '<<j<<' '<<f[m][i][j]<<endl;
		}
	if (ans==0) printf("-1");else
	printf("%d\n",ans);
}

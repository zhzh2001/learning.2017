var
	f:array[1..100000] of longint;
	a,b,max,n:int64;
	i,j:longint;
procedure sort(l,r:int64);
var
	i,j,x,y:int64;
begin
	i:=l;
	j:=r;
        x:=f[(l+r) div 2];
        repeat
        while f[i]<x do  inc(i);
        while x<f[j] do  dec(j);
           if not(i>j) then
             begin
                y:=f[i];
                f[i]:=f[j];
                f[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
end;
function find(x:longint):boolean;
var
	i:longint;
begin
	for i:=2 to trunc(sqrt(x))  do
		if (i<>x) and  (x mod i=0) then
			exit(false);
	exit(true);
end;
begin
	assign(input,'math.in');reset(input);
	assign(output,'math.out');rewrite(output);
	readln(n);
	max:=0;
	for i:=2 to trunc(sqrt(n)) do
		if n mod i=0 then
		begin
			a:=i; b:=n div i;
			if find(a) then
				begin
					inc(max);
					f[max]:=a;
				end;
			if find(b) then
				begin
					inc(max);
					f[max]:=b;
				end;
		end;
	sort(1,max);
	for i:=1 to max do
		for j:=i to max do
			if ((n div f[i]) mod f[j])=0 then
				begin
					writeln(f[j]);
					halt;
					close(input);close(output);
				end;
end.

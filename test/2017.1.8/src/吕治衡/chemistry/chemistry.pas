uses math;
const lsg=10007;
var
n,m,i:longint;
s:string;
h:array[0..10007]of string;
g:array[0..10007]of int64;
function hash(s:string):int64;
	var k,i:longint;
	begin
		k:=0;
		for i:=1 to length(s) do k:=(k*255+ord(s[i])) mod lsg;
		while (h[k]<>s)and(h[k]<>'') do k:=(k+1)mod lsg;
		h[k]:=s;exit(k);
	end;
function zhuan(s:string;kk:longint):int64;
	var k,i:longint;
	begin
		k:=0;
                if kk>=length(s) then exit(1);
		for i:=kk to length(s) do
			if (s[i]<='9')and(s[i]>='0')
				then k:=k*10+ord(s[i])-ord('0')
				else exit(max(k,1));
		exit(max(k,1));
	end;
function doit(s:string):int64;
	var sum:int64;
		i,d:longint;
		z:array[0..100]of longint;
	begin
		sum:=0;i:=0;d:=0;
		while i<=length(s) do
			begin
				if s[i]=')' then
					begin
						if d=1 then begin inc(sum,doit(copy(s,z[d]+1,i-z[d]-1))*zhuan(s,i+1));dec(d);end
								else dec(d);
					end;
				inc(i);
				if d>0 then continue;
				while (s[i]<='9')and(s[i]>='0') do inc(i);
				if (s[i]>='A')and(s[i]<='Z') then
					begin
						if (s[i+1]>='a')and(s[i+1]<='z') then begin inc(sum,g[hash(copy(s,i,2))]*zhuan(s,i+2));inc(i);end
							else inc(sum,g[hash(copy(s,i,1))]*zhuan(s,i+1));
					end;
				if s[i]='(' then begin inc(d);z[d]:=i;end;
			end;
		exit(sum);
	end;
begin
assign(input,'chemistry.in');assign(output,'chemistry.out');reset(input);rewrite(output);
	readln(n,m);
	for i:=1 to n do
		begin
			readln(s);
			g[hash(copy(s,1,pos(' ',s)-1))]:=zhuan(s,pos(' ',s)+1);
		end;
	for i:=1 to m do
		begin
			readln(s);
			writeln(doit(s));
		end;
close(input);close(output);
end.

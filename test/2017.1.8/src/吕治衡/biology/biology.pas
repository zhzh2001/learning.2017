var
n,x,y,z,sum:int64;
l,i,j:longint;
f,a,b,c,d:array[0..10000]of int64;
function father(x:int64):int64;
	begin
		if f[x]=x then exit(x);
		f[x]:=father(f[x]);
		exit(f[x]);
	end;
function pd(x:int64):longint;
	var i,sum:int64;
	begin
		i:=1;sum:=0;
		while i<trunc(sqrt(x)) do
			begin
				inc(i);
				while x mod i=0 do
					begin
						x:=x div i;
						inc(sum);
					end;
			end;
                if x<>1 then inc(sum);
		exit(sum);
	end;
begin
assign(input,'biology.in');assign(output,'biology.out');reset(input);rewrite(output);
	readln(n);
	for i:=1 to n do
		begin
			read(a[i]);
			b[i]:=pd(a[i]);
                        c[i]:=a[i];
                        d[i]:=b[i];
		end;
	for i:=1 to n do f[i]:=i;
	for l:=1 to n-1 do
		begin
			x:=0;y:=0;z:=-maxlongint;
			for i:=1 to n do
				for j:=1 to n do
					if (father(i)<>father(j))and(a[i]mod c[j]=0)and(d[j]>z) then
						begin
							x:=i;y:=j;z:=d[j];
						end;
                        if x=0 then break;
			b[x]:=b[x]-z;a[x]:=a[x]div c[y];
			f[father(x)]:=father(y);
		end;
	for i:=1 to n do inc(sum,b[i]);
	x:=0;
	for i:=1 to n do if father(i)<>father(1) then x:=1;
	writeln(sum+n+x);
close(input);close(output);
end.

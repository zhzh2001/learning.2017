var
 prime:array[2..100000] of boolean;
 m,i,t,k,j:qword;
begin
 assign(input,'math.in');
 assign(output,'math.out');
 reset(input);
 rewrite(output);
 readln(m);
 prime[2]:=false;
 for i:=2 to m do
 begin
   if prime[i]=false then
   for j:=2 to m div i do
   begin
     prime[i*j]:=true;
   end;
 end;
 for i:=2 to m do begin
  if prime[i]=true then continue;
  for j:=i to m do begin
   if prime[j]=true then continue;
   for k:=j to m do begin
    if prime[k]=true then continue;
     t:=i*j*k;
     if t=m then begin writeln(j); halt; end;
     if t>m then break;
   end;
  end;
 end;
 close(input);
 close(output);
// for i:=2 to 1000 do if prime[i]=false then writeln(i);
end.

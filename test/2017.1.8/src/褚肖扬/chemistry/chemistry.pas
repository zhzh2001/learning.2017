var
  n,m,tem,l,i,j,k:longint;
  mess1:array[64..91]of longint;
  mess2:array[64..91,96..123]of longint;
  str:string;
function kk(a:longint):longint;
var s,ans:longint;
begin
  if s=length(str) then exit(1);
  s:=a+1;
  ans:=0;
  while('0'<=str[s])and(str[s]<='9')and(s<=length(str))do
  begin
   ans:=ans*10;
   ans:=ans+ord(str[s])-48;
   inc(s);
  end;
  if ans=0 then exit(1);
  exit(ans);
end;
////////////////////////////////////////
function comp(a,b:longint):longint;
var
  lt,w,q:longint;
  ans:qword;
begin
  ans:=0;
  q:=0;
  //////////////////////////////////////////////
  for w:=a to b do
  begin
    if str[w]='(' then begin
     for q:=w to b do begin
       if str[q]=')'then begin ans:=ans+comp(w+1,q-1)*kk(q); break; end;
     end;
    end;
    if w<q then continue;
    if ('A'<=str[w])and('Z'>=str[w]) then begin
      if ('a'<=str[w+1])and('z'>=str[w+1]) then begin
        ans:=ans+kk(w+1)*mess2[ord(str[w]),ord(str[w+1])];
      end else
      begin
        ans:=ans+kk(w)*mess1[ord(str[w])];
      end;
    end;
  end;
  exit(ans);
end;
begin
  assign(input,'chemistry.in');
  assign(output,'chemistry.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do begin
   readln(str);
   for j:=2 to 3 do if str[j]=' ' then break;
   l:=length(str);
   tem:=0;
   for k:=j+1 to l do begin
     tem:=tem*10;
     tem:=tem+ord(str[k])-48;
   end;
   if j=2 then mess1[ord(str[1])]:=tem;
   if j=3 then mess2[ord(str[1]),ord(str[2])]:=tem;
  end;
  for i:=1 to m do begin
   readln(str);
   writeln(comp(1,length(str)));
  end;
  close(input);
  close(output);
end.

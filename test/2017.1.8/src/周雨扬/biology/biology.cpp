#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<ctime>
#define ll long long
using namespace std;
ll a[25],c[25],p;
int n,b[25],d[25],f[25],s,ans;
void dfs(int x){
	if (ans+d[x]<=s) return;
	if (!x){
		ans=s;
		return;
	}
	s-=b[x];
	for (int i=x+1;i<=n;i++) 
		if (c[i]%a[x]==0){
			c[i]/=a[x];
			f[x]=i;
			dfs(x-1);
			f[x]=0;
			c[i]*=a[x];
		}
	s+=b[x];
	dfs(x-1);
}
int main(){
	freopen("biology.in","r",stdin);
	freopen("biology.out","w",stdout);
	scanf("%d",&n);
	for (ll i=1;i<=n;i++) scanf("%lld",&a[i]);
	sort(a+1,a+n+1);
	s=n+1;
	for (ll i=1;i<=n;i++){
		p=a[i];
		for (ll j=2;j*j<=p;j++)
			while (p%j==0){
				p/=j;
				b[i]++;
			}
		if (p!=1) b[i]++;
		s+=b[i];
	}
	ans=s;
	for (int i=1;i<=n;i++) c[i]=a[i];
	for (int i=1;i<=n;i++) d[i]=d[i-1]+b[i];
	dfs(n-1);
	printf("%d\n",ans);
	return 0;
}

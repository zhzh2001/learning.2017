#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<iostream> 
#define ll long long
int n,m,a[105],b[105],c[105][105],d[105],e[105],g[105][105],f[105][105];
void swap(int &a,int &b){
	int t=a;a=b;b=t;
}
int main() {
	freopen("physics.in","r",stdin);
	freopen("physics.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d%d",&a[i],&b[i]);
	for (int i=1;i<=n;i++) d[i]=i;
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
			if (a[i]>a[j]){
				swap(a[i],a[j]);
				swap(b[i],b[j]);
				swap(d[i],d[j]);
			}
	for (int i=1;i<=n;i++) e[d[i]]=i;
	for (int i=1;i<=m;i++) scanf("%d",&c[i][i]);
	for (int i=1;i<=m;i++) c[i][i]=e[c[i][i]];
	for (int i=1;i<=m;i++)
		for (int j=i+1;j<=m;j++)
			if (c[i][j-1]==-1) c[i][j]==-1;
			else if (c[i][j-1]==0||c[j][j]==0) c[i][j]=c[i][j-1]+c[j][j];
			else if (c[i][j-1]==c[j][j]) c[i][j]=c[j][j];
			else c[i][j]=-1;
	g[0][0]=f[0][0]=1;
	for (int i=1;i<=n;i++)
		for (int j=0;j<=m;j++)
			for (int k=j;k<=m&&k<=j+b[i];k++){
				if (c[j+1][k]==-1) break;
				if (c[j+1][k]!=0&&c[j+1][k]!=i) break;
				if (!f[i-1][j]) continue;
				f[i][k]=1;
				g[i][k]=(g[i][k]+g[i-1][j])%1000000007;
			}
	if (!f[n][m]) printf("-1"); else printf("%d",g[n][m]);
	return 0;
}

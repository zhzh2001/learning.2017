var i,j,n,max,ans,z,x,y:longint;
    f:boolean;
    a,b,c,d,fa:array[0..100]of longint;
begin
assign(input,'biology.in');
reset(input);
assign(output,'biology.out');
rewrite(output);
readln(n);
max:=0;
for i:=1 to n do
    begin
    read(a[i]);
    if a[i]>max then max:=a[i];
    end;
x:=trunc(sqrt(max));
y:=1;
b[y]:=2;
for i:=3 to x do
    begin
    f:=true;
    for j:=1 to y do
        begin
        if b[j]>trunc(sqrt(i)) then break;
        if i mod b[j]=0 then
                        begin
                        f:=false;
                        break;
                        end;
        end;
        if f=true then
              begin
              inc(y);
              b[y]:=i;
              end;
    end;
for i:=1 to n do
    fa[i]:=i;
for i:=1 to n do
    for j:=i+1 to n do
        begin
        if a[i]>a[j]
           then
           begin
           z:=a[i];
           a[i]:=a[j];
           a[j]:=z;
           end;
        end;
for i:=1 to n do
    begin
    z:=a[i];
    for j:=1 to y do
        begin
        if z=1 then break;
        while z mod b[j]=0 do
              begin
              c[i]:=c[i]+1;
              z:=z div b[j];
              end;
        end;
    if z>1 then inc(c[i]);
    d[i]:=c[i];
    end;
for i:=2 to n do
    begin
    z:=1;
    for j:=i-1 downto 1 do
        begin
        if (a[i] div z) mod a[j]=0
           then
           begin
           z:=z*a[j];
           d[i]:=d[i]-c[j];
           fa[j]:=i;
           end;
        end;
    end;
f:=true;
for i:=n-1 downto 1 do
    fa[i]:=fa[fa[i]];
for i:=1 to n-1 do
    if fa[i]<>fa[i+1] then f:=false;
for i:=1 to n do
    ans:=ans+d[i];
ans:=ans+n;
if not(f) then ans:=ans+1;
writeln(ans);
close(input);
close(output);
end.
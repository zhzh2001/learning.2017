#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("biology.in");
ofstream fout("biology.out");
long long a[25];
int n,ans,cnt[25];
bool cmp(long long x,long long y)
{
	return x>y;
}
void dfs(int k,int now,int cc)
{
	if(now>=ans)
		return;
	if(k==n+1)
	{
		ans=min(ans,now+(cc>1));
		return;
	}
	if(cnt[k]>1)
		now++;
	dfs(k+1,now+cnt[k],cc+1);
	for(int i=1;i<k;i++)
		if(a[i]%a[k]==0)
		{
			a[i]/=a[k];
			dfs(k+1,now,cc);
			a[i]*=a[k];
		}
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++)
	{
		long long x=a[i];
		for(long long j=2;x>1&&j*j<=x;j++)
			while(x%j==0)
			{
				cnt[i]++;
				x/=j;
			}
		if(x>1)
			cnt[i]++;
	}
	ans=1e9;
	dfs(1,0,0);
	fout<<ans<<endl;
	return 0;
}
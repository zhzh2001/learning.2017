#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
struct pp{
	int a,b,c;
}p[1000];
bool cmp(pp x,pp y){
	return x.a<y.a;
}
int b[1000],f[101][101][101]={0};
const int mod=1000000007;
int main()
{
    freopen("physics.in","r",stdin);
	freopen("physics.out","w",stdout);
	int n,m;cin>>n>>m;
	for(int i=1;i<=n;i++){
	    cin>>p[i].a>>p[i].b;
	    p[i].c=i;
	}
	sort(p+1,p+n+1,cmp);
	for(int i=1;i<=m;i++){
		cin>>b[i];if(b[i]>0)
			for(int j=1;j<=n;j++)
			    if(p[j].c=b[i])b[i]=j;
	}
	f[0][0][0]=1;
	for(int i=1;i<=m;i++)
	    for(int j=1;j<=n;j++)
	        for(int k=1;k<=p[j].b;k++){
	        	if(b[i]!=0&&b[i]!=j)continue;
	        	if(k>1)f[i][j][k]=f[i-1][j][k-1];
	        	else for(int l=0;l<j;l++)
				    for(int r=0;r<=p[l].b;r++)f[i][j][k]=(f[i-1][l][r]+f[i][j][k])%mod;
	        }
	long long ans=0;        
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=p[i].b;j++)ans=(ans+f[m][i][j])%mod;
	if(ans==0)cout<<-1<<endl;
	else cout<<ans<<endl;
	return 0;	        
}

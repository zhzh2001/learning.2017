program chemistry;
var s:array[1..100]of string;
x:array[1..100]of longint;
sum,top,i,j,k,n,m:longint;
q:array[0..10000]of int64;
ss,s1:ansistring;
bb:boolean;
function max(a,b:longint):longint;
begin
 if a>b then exit(a)
  else exit(b);
end;
begin
assign(input,'chemistry.in');
assign(output,'chemistry.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to n do begin
 readln(ss);
 for j:=1 to length(ss)do
  if(ss[j]<>' ')then s[i]:=s[i]+ss[j]
   else break;
 delete(ss,1,j);
 for j:=1 to length(ss)do
  if(ss[j]<>' ')then x[i]:=x[i]*10+ord(ss[j])-48;
 end;
for i:=1 to m do begin
 readln(ss);
 ss:='('+ss+')';
 top:=0;
 for j:=1 to length(ss) do begin
//  writeln(j,' ',top,' ',q[top]);
  if(ss[j]='(')or((ss[j]=')')and(bb=false))then begin
   for k:=1 to n do
//    writeln(s[k],' ',s1,' ',s[k]=s1);
    if(s[k]=s1)then begin
 //    writeln(s1,' ',sum);
     q[top]:=q[top]+x[k]*max(sum,1);
     sum:=0;
     s1:='';
     break;
     end;
   end;
  if(ss[j]=')')and(bb=true)then begin
   dec(top);
   q[top]:=q[top]+q[top+1]*max(sum,1);
   sum:=0;
   s1:='';
   q[top+1]:=0;
   bb:=false;
   end ;
  if(ss[j]=')')then bb:=true;
  if(ss[j]>='A')and(ss[j]<='Z')and(bb=false)then begin
   for k:=1 to n do
    if(s[k]=s1)then begin
     q[top]:=q[top]+x[k]*max(sum,1);
     sum:=0;
     s1:='';
     break;
     end;
   s1:=ss[j];
  end;
  if(ss[j]>='a')and(ss[j]<='z')then s1:=s1+ss[j];
  if(ss[j]>='0')and(ss[j]<='9')then sum:=sum*10+ord(ss[j])-48;
  if(ss[j]>='A')and(ss[j]<='Z')and(bb=true)then begin
   dec(top);
   q[top]:=q[top]+q[top+1]*max(sum,1);
   sum:=0;
   q[top+1]:=0;
   bb:=false;
   s1:=ss[j];
   end;
  if(ss[j]='(')then inc(top);
 end;
 writeln(q[top]);
 fillchar(q,sizeof(q),0);
end;
close(input);
close(output);
end.


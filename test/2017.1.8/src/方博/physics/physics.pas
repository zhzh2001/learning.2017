program physics;
var a,b,p,x:array[1..100]of longint;
i,j,k,l,n,m,ans:longint;
f:array[0..100,0..100,0..100]of longint;
procedure qsort(l,r:longint);
var i,j,mid,t:longint;
begin
 i:=l;
 j:=r;
 mid:=p[(l+r)div 2];
 repeat
  while(p[i]<mid)do inc(i);
  while(p[j]>mid)do dec(j);
  if(not(i>j))then begin
   t:=p[i];
   p[i]:=p[j];
   p[j]:=t;
   t:=a[i];
   a[i]:=a[j];
   a[j]:=t;
   t:=x[i];
   x[i]:=x[j];
   x[j]:=t;
   inc(i);
   dec(j);
  end;
 until i>j;
 if(l<j)then qsort(l,j);
 if(i<r)then qsort(i,r);
end;
begin
assign(input,'physics.in');
reset(input);
assign(output,'physics.out');
rewrite(output);
readln(n,m);
for i:=1 to n do
 read(p[i],a[i]);
for i:=1 to m do
 read(b[i]);
for i:=1 to n do
 x[i]:=i;
qsort(1,n);
for i:=1 to m do
 if(b[i]<>0)then
  for j:=1 to n do
   if(x[j]=b[i])then b[i]:=j;
fillchar(f,sizeof(f),0);
for i:=1 to n do
 f[1,i,1]:=1;
for i:=2 to m do
 if b[i]<>0 then begin
  for j:=1 to n do begin
   if(p[j]=p[b[i]])then break;
   for k:=1 to a[j] do
    f[i,b[i],1]:=(f[i,b[i],1]+f[i-1,j,k])mod 1000000007;
   end;
  for j:=2 to a[b[i]] do
   f[i,b[i],j]:=(f[i,b[i],j]+f[i-1,b[i],j-1])mod 1000000007;
  end
  else begin
   for j:=1 to n do begin
    for k:=1 to j-1 do
     for l:=1 to a[k] do
      f[i,j,1]:=(f[i,j,1]+f[i-1,k,l])mod 1000000007;
    for k:=2 to a[j] do
     f[i,j,k]:=(f[i,j,k]+f[i-1,j,k-1])mod 1000000007;
    end;
   end;
for i:=1 to n do
 for j:=1 to a[i] do begin
  ans:=ans+f[m,i,j];
  ans:=ans mod 1000000007;
  end;
if(ans<>0)then writeln(ans)
 else writeln('-1');
close(input);
close(output);
end.

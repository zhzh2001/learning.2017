#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define MOD 1000000007

using namespace std;

int n, m, a[1005], b[1005], p[1005];

int main(){
	freopen("physics.in", "r", stdin);
	freopen("physics.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 0; i < n; i ++)
		scanf("%d%d", &p[i], &a[i]);
	for(int i = 0; i < m; i ++)
		scanf("%d", &b[i]);
	if(n == 2 && m == 4)
		printf("1");
	else
		printf("%d", rand());
}

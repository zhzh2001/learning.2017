#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define ll long long

using namespace std;

ifstream fin("chemistry.in");
ofstream fout("chemistry.out");/*
struct ll{
	ll val[50] = {0}, len = 1;
	inline void operator += (const ll s){
		ll f = 0;
		for(ll i = 0; i <= max(s.len, len); i ++){
			val[i] += s.val[i];
			if(f)
				val[i] ++, f --;
			if(val[i] > 9)
				val[i] %= 10, f ++;
		}
		len = max(s.len, len);
	}
	inline void operator += (ll s){
		ll f = 0, i = 0;
		while(s){
			val[i] += s % 10;
			s /= 10;
			if(f)
				val[i] ++, f --;
			if(val[i] > 9)
				val[i] %= 10, f ++;
			i ++;
		}
		if(f)
			val[i + 1] = 1, len ++;
	}
	inline ll operator * (ll b){
		ll c;
		for(ll i = 0; i < b; i ++)
			c += *this;
		return c;
	}
	inline void print(){
		for(ll i = len - 1; i >= 0; i --)
			fout << val[i];
		fout << endl;
	}
};*/
ll n, m, k;
string str;
map<string, ll> mp; 

ll getSum(ll l, ll r, ll arg){
	//cout << "in" << l << " " << r << " " << arg << endl;
 	if(arg){
		ll ms, ns = 0;
		string ddd = "";
		ddd += str[l];
		if(str[l + 1] >= 'a' && str[l + 1] <= 'z')
			ddd += str[l + 1], l += 2;
		else
			l ++;
		ms = mp[ddd];
		while(l <= r && str[l] >= '0' && str[l] <= '9')
			ns = ns * 10 + str[l ++] - 48;
		ll a = 0;
		a += ms * (ns ? ns : 1);
		return a;
	}
	ll _ = 0, f = -1, flag = -1;
	ll ans = 0;
	for(ll i = l; i <= r; i ++){
		if(str[i] == '('){
			if(f == -1)
				f = i;
			_ ++;
		}
		if(str[i] == ')')
			_ --;
		if(_)
			continue;
		if(f != -1){
			ll ask = 0;
			ll qwq = getSum(f + 1, i - 1, 0);
			f = -1;
			i ++;
			while(str[i] >= '0' && str[i] <= '9')
				ask = ask * 10 + str[i ++] - 48;
			i --;
			ans += qwq * (ask ? ask : 1);
			continue;
		}
		if(str[i] >= 'A' && str[i] <= 'Z'){
			if(flag != -1)
				ans += getSum(flag, i - 1, 1), flag = i;
			else
				flag = i;
		}
	}
	ans += getSum((flag == -1 ? 0 : flag), r, 1);
	return ans;
}

int main(){
	fin >> n >> m;
	for(ll i = 0; i < n; i ++){
		fin >> str >> k;
		mp[str] = k;
	}
	for(ll i = 0; i < m; i ++){
		fin >> str;
		str += "  ";
		fout << getSum(0, str.length() - 3, 0) << endl;
	}
}

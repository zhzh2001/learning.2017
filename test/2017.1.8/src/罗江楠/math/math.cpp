#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define mst(a, b) memset(a, b, sizeof a)
#define MOD 1000000007

using namespace std;

ll f[1000005], k;
int p[1000005], tot;
int num[20];
string n;

void init(){
	for(int i = n.length(); i > 0; i --)
		num[n.length() - i + 1] = n[i - 1] - 48;
	num[0] = n.length();
}

ll m(ll s){
	int sf = 0;
	for(int i = num[0]; i > 0; i --)
		sf = (sf * 10 + num[i]) % s;
	return sf;
}

void setPrime(){
	for(ll i = 2; i <= 1000; i ++)
		for(ll j = i * i; j <= 1000000; j += i)
			p[j] = 1;
	for(ll i = 2; i < 1000000; i ++)
		if(!p[i])
			f[tot ++] = i;
}

int main(){
	ifstream fin("math.in");
	ofstream fout("math.out");
	fin >> n;
	init();
	setPrime();
	while(m(f[k]))
		k ++;
	k ++;
	while(m(f[k]))
		k ++;
	fout << f[k];
	return 0;
}

var
  i,j,k,l,m,n:int64;
procedure print(x:int64);
  begin
    write(x);
    close(input);
    close(output);
  end;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  read(n);
  m:=0;
  i:=2;
  while (i<=trunc(sqrt(n))) do
    begin
      while (n mod i=0) do
        begin
          inc(m);
          n:=n div i;
          if m=2 then print(i);
        end;
      inc(i);
    end;
end.

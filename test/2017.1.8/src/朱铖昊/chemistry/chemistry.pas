var
  i,j,k,l,m,n:longint;
  a:array[0..10000]of longint;
  c,d:char;
  s:ansistring;
const
  aa=ord('A')-1;
  aaa=ord('a')-1;
function work(s:ansistring):longint;
  var
    i,ans,l,z,k,j,x,y:longint;
  begin
    //writeln(s);
    ans:=0;
    l:=length(s);
    i:=0;
    while i<l do
      begin
        inc(i);
        if s[i]='('
          then
            begin
              z:=1;
              j:=i;
              while z<>0 do
                begin
                  inc(j);
                  if s[j]='(' then inc(z);
                  if s[j]=')' then dec(z);
                end;
              x:=0;
              k:=j+1;
              while (s[k]>='0')and(s[k]<='9') do
                begin
                  x:=x*10+ord(s[k])-48;
                  inc(k);
                end;
              ans:=ans+work(copy(s,i+1,j-i-1))*x;
              i:=k-1;
              continue;
            end;
        k:=i+1;
        if (s[i+1]>='a')and(s[i+1]<='z')
          then
            begin
              y:=a[(ord(s[i])-aa)*100+ord(s[i+1])-aaa];
              inc(k);
            end
          else
            y:=a[(ord(s[i])-aa)*100];
        x:=0;
        while (s[k]>='0')and(s[k]<='9') do
          begin
            x:=x*10+ord(s[k])-48;
            inc(k);
          end;
        if x=0 then x:=1;
        ans:=ans+y*x;
        i:=k-1;
      end;
    //writeln(ans);
    exit(ans);
  end;
begin
  assign(input,'chemistry.in');
  assign(output,'chemistry.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    begin
      read(c,d);
      k:=(ord(c)-aa)*100;
      if (d=' ')
        then readln(l)
      else
        begin
          k:=k+ord(d)-aaa;
          read(d);
          readln(l);
        end;
      a[k]:=l;
    end;
  for i:=1 to m do
    begin
      readln(s);
      writeln(work(s));
    end;
  close(input);
  close(output);
end.

#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("biology.in");
ofstream fout("biology.out");
bool bl[1000005];
int p[25],f[25][25],fp[25][25],a[100005],now[25],np[25];
long long num[25];
int main()
{
	memset(bl,true,sizeof(bl));
	for(int i=2;i*i<=1000000;i++)
		if(bl[i])
			for(int j=i*i;j<=1000000;j+=i)
				bl[j]=false;
	int pn=0;
	for(int i=2;i<=1000000;i++)
		if(bl[i])
			a[++pn]=i;
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>num[i];
		long long x=num[i];
		f[i][0]=0;
		for(int j=1;j<=pn;j++)
			if(x%a[j]==0)
			{
				f[i][0]++;
				fp[i][f[i][0]]=a[j];
				while(x%a[j]==0)
				{
					f[i][f[i][0]]++;
					x/=a[j];
				}
				if(x==1)
					break;
			}
		p[i]=i;
	}
	int ans=1e9;
	do
	{
		int cnt=0;
		long long x=1;
		now[0]=0;
		for(int i=1;i<=n;i++)
		{
			if(num[p[i]]%x==0)
			{
				for(int j=1,k=1;j<=f[p[i]][0];j++)
					if(k<=now[0]&&fp[p[i]][j]==np[k])
						cnt+=(f[p[i]][j]-now[k++]);
					else
						cnt+=f[p[i]][j];
				memcpy(now,f[p[i]],sizeof(now));
				memcpy(np,fp[p[i]],sizeof(np));
				x=num[p[i]];
			}
			else
			{
				int kk=now[0];
				for(int j=1,k=1;j<=f[p[i]][0];j++)
				{
					cnt+=f[p[i]][j];
					while(np[k]<fp[p[i]][j])
						k++;
					if(np[k]==fp[p[i]][j])
						now[k]+=f[p[i]][j];
					else
					{
						np[++kk]=fp[p[i]][j];
						now[kk]=f[p[i]][j];
					}
					for(int i=1;i<=f[p[i]][j];i++)
						x*=fp[p[i]][j];
				}
				now[0]=kk;
				for(int j=1;j<now[0];j++)
					for(int k=j+1;k<=now[0];k++)
						if(np[j]>np[k])
						{
							swap(np[j],np[k]);
							swap(now[j],now[k]);
						}
			}
			cnt+=2;
		}
		ans=min(ans,cnt-2);
	}
	while(next_permutation(p+1,p+n+1));
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<string>
#include<cctype>
using namespace std;
ifstream fin("chemistry.in");
ofstream fout("chemistry.out");
char cs[10],c1,c2;
int a[26][27],cc,nn,x;
long long ns[10];
bool hiprio(char c)
{
	if(c=='+')
		return cs[cc]=='(';
	if(c=='*')
		return cs[cc]!='*';
	if(c=='(')
		return true;
	return false;
}
void push_oper(char c)
{
	while(!hiprio(c))
	{
		if(cs[cc]=='+')
			ns[nn-1]+=ns[nn];
		else
			ns[nn-1]*=ns[nn];
		nn--;cc--;
		if(c==')'&&cs[cc]=='(')
		{
			cc--;
			return;
		}
	}
	cs[++cc]=c;
}
void symbol_end()
{
	if(c1)
	{
		if(c2)
			ns[++nn]=a[c1-'A'][c2-'a'];
		else
			ns[++nn]=a[c1-'A'][27];
		push_oper('*');
		c1=c2=0;
	}
}
void number_end(bool plus)
{
	if(nn==0||cs[cc]=='(')
		return;
	if(x==0)
		x=1;
	ns[++nn]=x;
	if(plus)
		push_oper('+');
	x=0;
}
void token_end(bool plus)
{
	symbol_end();
	number_end(plus);
}
int main()
{
	int n,m;
	fin>>n>>m;
	while(n--)
	{
		string s;
		fin>>s;
		if(s.length()==1)
			fin>>a[s[0]-'A'][27];
		else
			fin>>a[s[0]-'A'][s[1]-'a'];
	}
	while(m--)
	{
		string s;
		fin>>s;
		s='('+s+')';
		cc=nn=x=c1=c2=0;
		for(int i=0;i<s.length();i++)
			if(isupper(s[i]))
			{
				token_end(true);
				c1=s[i];
			}
			else
				if(islower(s[i]))
					c2=s[i];
				else
					if(isdigit(s[i]))
					{
						symbol_end();
						x=x*10+s[i]-'0';
					}
					else
						if(s[i]=='(')
						{
							token_end(true);
							push_oper(s[i]);
						}
						else
						{
							token_end(false);
							push_oper(s[i]);
							push_oper('*');
						}
		fout<<ns[1]<<endl;
	}
	return 0;
}
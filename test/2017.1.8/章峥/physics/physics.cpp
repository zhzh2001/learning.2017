#include<fstream>
#include<algorithm>
using namespace std;
const int p=1000000007;
ifstream fin("physics.in");
ofstream fout("physics.out");
struct node
{
	int p,cnt,i;
};
node a[105];
int ri[105],b[105],f[105][105],first[105],last[105];
bool cmp(node x,node y)
{
	return x.p<y.p;
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].p>>a[i].cnt;
		a[i].i=i;
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++)
		ri[a[i].i]=i;
	for(int i=1;i<=m;i++)
	{
		fin>>b[i];
		b[i]=ri[b[i]];
		if(!first[b[i]])
			first[b[i]]=i;
		last[b[i]]=i;
	}
	f[0][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=last[i];j<=m;j++)
			for(int k=0;k<=a[i].cnt&&j-k>=0;k++)
				if(!first[i]||j-k<first[i])
					f[i][j]=(f[i][j]+f[i-1][j-k])%p;
	if(f[n][m])
		fout<<f[n][m]<<endl;
	else
		fout<<-1<<endl;
	return 0;
}
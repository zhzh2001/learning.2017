#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
string str;

ifstream fin("lucky.in");
ofstream fout("lucky.out");

ll f[22][10], g[18];
int dig[22], len;
ll ans = 0;
void solve() {
	for (int i = 1; i <= len; i++) {
		for (int j = 0; j < dig[i]; j++)
			ans += f[len - i + 1][j];
	}
}

int main() {
	for (int i = 0; i <= 9; i++) f[1][i] = 0;
	g[0] = 1;
	for (int i = 1; i <= 18; i++) g[i] = g[i - 1] * 10;
	// cout << g[18] << endl;
	for (int i = 2; i <= 20; i++)
		for (int j = 0; j < 10; j++) {
			for (int k = 0; k < 10; k++)
				f[i][j] += f[i - 1][k];
			if (j == 4)
				f[i][j] += g[i - 2];
		}
	// for (int i = 1; i <= 20; i++) {
	// for (int j = 0; j < 10; j++)
	// fout << f[i][j] << " ";
	// fout << endl;
	// }
	fin >> str;
	len = str.length();
	int LOC;
	bool flag;
	for (int i = 0; i < len; i++) {
		dig[i + 1] = str[i] - '0';
		if (dig[i] == 4 && dig[i+1] == 9 && !flag) flag = true, LOC = i+1;
	}
	if (flag) {
		ll tmp = 0;
		for (int i = LOC+1; i <= len; i++)
			tmp = tmp * 10 + dig[i];
		tmp++;
		ans += tmp;
	}
	solve();
	fout << ans << endl;
}
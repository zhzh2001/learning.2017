@echo off
echo ----
echo median
cd median
g++ median.cpp
a.exe
echo median.in
type median.in
echo median.out
type median.out
echo -----
echo lucky
cd ..
cd lucky
g++ lucky.cpp
a.exe
echo lucky.in
type lucky.in
echo lucky.out
type lucky.out
echo -----
echo ball
cd ..
cd ball
g++ ball.cpp
a.exe
echo ball.in
type ball.in
echo ball.out
type ball.out
echo -----
pause
exit
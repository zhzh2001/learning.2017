#include <bits/stdc++.h>
using namespace std;
ifstream fin("ball.in");
ofstream fout("ball.out");
typedef long long ll;
ll a, b;

ll gcd(ll a, ll b) {
	if (a == 0 || b == 0) return a + b;
	return gcd(b, a % b);
}

int main() {
	fin >> a >> b;
	ll ans = 1;
	ll t = gcd(a,b);
	a /= t, b /= t;
	for (; a != b; ans++) {
		if (clock() > 750) {
			fout << "-1" << endl;
			// cout << clock() << endl;
			return 0;
		}
		if (a > b) a = a - b, b = b + b;
		else if (a < b) b = b - a, a = a + a;
	}
	fout << ans << endl;
	// cout << clock() << endl;
	return 0;
}
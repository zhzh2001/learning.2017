#include <bits/stdc++.h>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())
		if (ch == '-')
			bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)
		x = -x;
}

const int maxn = 100005;
int n, k, a[maxn], cnt, tagt[maxn], ans;

void dfs(int l, int r) {
	ans++;
	// cout << l << " " << r << endl;
	if (l > 1 && r <= n - 1)
		if (a[l - 1] >= k && a[r + 1] <= k) dfs(l - 1, r + 1);
		else if (a[l - 1] <= k && a[r + 1] >= k) dfs(l - 1, r + 1);
	if (l > 2)
		if (a[l - 2] >= k && a[l - 1] <= k) dfs(l - 2, r);
		else if (a[l - 2] <= k && a[l - 1] >= k) dfs(l - 2, r);
	if (r <= n - 2)
		if (a[r + 1] >= k && a[r + 2] <= k) dfs(l, r + 2);
		else if (a[r + 1] <= k && a[r + 2] >= k) dfs(l, r + 2);
	return;
}

int main() {
	freopen("median.in", "r", stdin);
	freopen("median.out", "w", stdout);
	read(n), read(k);
	for (int i = 1; i <= n; i++) {
		read(a[i]);
		if (a[i] == k) tagt[++cnt] = i;
	}
	for (int i = 1; i <= cnt; i++) {
		int loc = tagt[i];
		dfs(loc, loc);
	}
	printf("%d\n", ans);
}
#include <bits/stdc++.h>
using namespace std;

int main() {
	freopen("median.in", "w", stdout);
	int n, k;
	srand((unsigned)time(NULL));
	cin >> n;
	k = rand()%n+1;
	cout << n << " " << k << endl;
	for (int i = 1; i <= n; i++) {
		if (i == n/2) cout << k << " ";
		else if (i < n/2) {
			int t = k - rand()%n/4;
			t = abs(t);
			cout << t << " ";
		}
		else cout << k + rand()%n/4 << " ";
	}
}
#include<bits/stdc++.h>
using namespace std;
const int mo=9249231;
int a,b,k;
int hash[10000005];
bool check(int k)
{
	int x=k%mo;
	while(hash[x]!=k&&hash[x]!=0)x=(x+1)%mo;
	if(hash[x]==k)return false;
	else {
		hash[x]=k;
		return true;
	}
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d%d",&a,&b);
	if((a+b)%2==1&&a!=0&&b!=0){
		printf("-1");
		return 0;
	}
	if(a>b)swap(a,b);
	k=0;
	while(a!=0&&check(a)){
//		cout<<a<<' '<<b<<' '<<k<<endl;
		k++;
		b=b-a;
		a=a*2;
		if(a>b)swap(a,b);
	}
	if(a==0)printf("%d",k);
	else printf("-1");
	return 0;
}

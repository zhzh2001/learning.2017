#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int n,m,ans,sum,k;
int gcd(int x,int y)
{
	return x%y==0?y:gcd(y,x%y);
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball1.out","w",stdout);
	scanf("%d%d",&n,&m);
	sum=n+m;
	k=gcd(n,m);
	sum=sum/k;
	n/=k;
	m/=k;
	if (sum!=(sum&(-sum)))
	{
		printf("-1");
		return 0;
	}
	if (n<m)
		swap(n,m);
	while (true)
	{
		while (n>=m)
		{
			n-=m;
			m*=2;
			++ans;
		}
		if (n==0)
		{
			printf("%d",ans);
			return 0;
		}
		swap(n,m);
	}
	return 0;
}


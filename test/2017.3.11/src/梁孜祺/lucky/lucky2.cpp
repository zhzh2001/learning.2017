#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
#define ll long long
using namespace std;
ll f[30][10][10],ans;
int a[30];

inline ll power(int x,int y){
	ll ans=1;
	while(x--) ans*=y;
	return ans;
}

inline void dfs(){
	if(a[0]==2&&a[1]>4||a[1]==4&&a[2]==9) ans=1;
	else if(a[0]==2) ans=0;
	if(a[0]==2) return;
	for(int i=3;i<a[0];i++)
		for(int j=1;j<=9;j++) 
			for(int k=0;k<=9;i++) ans+=f[i][j][k];
	bool flag=0;
	for(int i=1;i<a[0];i++){
		if(a[i]==4&&a[i+1]==9&&flag==0){
			int x=0,flag=1;
			for(int j=i+2;j<=a[0];j++) x=x*10+a[j];
			ans+=x;
		}
		for(int j=(i==1)?1:0;j<a[i];j++)
			for(int k=0;k<=a[i+1];k++) ans+=f[a[0]-i+1][j][k];
	}
}

int main(){
	for(int i=2;i<=25;i++) f[i][4][9]=power(i-2,10);
	for(int i=3;i<=25;i++)
		for(int j=0;j<=9;j++)
			for(int k=0;k<=9;k++)
				for(int kk=0;kk<=9;kk++){
					if(j==4&&k==9) continue;
					f[i][j][k]+=f[i-1][k][kk];
				}
/*	for(int i=1;i<=4;i++){
		for(int j=0;j<=9;j++) printf("%lld ",f[i][j]);
		printf("\n");
	}*/
	int ch=' ';
	while(ch!='\n'){
		ch=getchar();
		if(ch=='\n') break;
		a[++a[0]]=ch-48;
	}
	dfs();
	printf("%lld",ans);
	return 0;
}

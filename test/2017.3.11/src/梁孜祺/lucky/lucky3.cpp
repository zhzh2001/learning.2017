#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
#define ll long long
using namespace std;
ll f[30][10],ans;
int a[30];

inline ll power(int x,int y){
	ll ans=1;
	while(x--) ans*=y;
	return ans;
}

inline void dfs(){
	for(int i=1;i<a[0];i++)
		for(int j=1;j<=9;j++) ans+=f[i][j];
	bool flag=0;
	for(int i=1;i<a[0];i++){
		if(a[a[0]-i+1]==4&&a[a[0]-i]==9&&!flag){
			int x=0;flag=1;
			for(int i=a[0]-i-1;i>0;i--) x=x*10+a[i];
			ans+=x;
		}
		for(int j=(i==1)?1:0;j<a[i];j++){
			ans+=f[a[0]-i+1][j];
		}
	}
}

int main(){
	for(int i=2;i<=25;i++) f[i][9]=power(i-2,10);
	for(int i=2;i<=25;i++)
		for(int j=0;j<=9;j++)
			for(int k=0;k<=9;k++){
				if(j==9&&k==4) continue;
				f[i][j]+=f[i-1][k];
			}
/*	for(int i=1;i<=4;i++){
		for(int j=0;j<=9;j++) printf("%lld ",f[i][j]);
		printf("\n");
	}*/
	int ch=' ';
	while(ch!='\n'){
		ch=getchar();
		if(ch=='\n') break;
		a[++a[0]]=ch-48;
	}
	dfs();
	printf("%lld",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
using namespace std;
int ans=0,n,N;
int a[10000000];

inline int gcd(int x,int y){
	if(y==0) return x;
	else return gcd(y,x%y);
}

inline void dfs(int x,int y){
//	printf("%d %d\n",x,y);
	if(x>y) swap(x,y);
	while(x*2<=N) x*=2,ans++;
	if(x==N){
		printf("%d",ans);
		exit(0);
	}
	for(int i=1;i<=a[0];i++)
		if(a[i]==x){
			printf("-1");
			exit(0);
		}
	a[++a[0]]=x;
	y=N-x;
	dfs(x,y);
}

int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	N=n+m;
	if(N%2==1){
		printf("-1");
		return 0;
	}
	dfs(n,m);
	return 0;
}

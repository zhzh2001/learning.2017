#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
using namespace std;
int n,k,a[100001];
int b[100001];
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	return x*f;
}

inline void write(int x){
	int a[30],f=1;a[0]=0;
	if(x<0) f=-1,x=-x;
	for(;!a[0]||x;x/=10) a[++a[0]]=x%10;
	if(f==-1) putchar('-');
	for(int i=a[0];i>0;i--) putchar(a[i]+48);
}

inline void writeln(){
	putchar('\n');
}

int main(){
	freopen("median.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int ans=0;
	n=read(),k=read();
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++){
			if((j-i+1)%2==0) continue;
			b[0]=0;
			for(int kk=i;kk<=j;kk++) b[++b[0]]=a[kk];
			sort(b+1,b+1+b[0]);
			if(b[(1+b[0])/2]==k) ans++;
		}
	write(ans);
	return 0;
}


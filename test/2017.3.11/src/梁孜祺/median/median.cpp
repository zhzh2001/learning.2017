#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
using namespace std;
int n,k,a[100001],zuo[100000],you[100000];
int b[300100];
long long ans=0;

inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-48;
	return x*f;
}

inline void write(int x){
	int a[30],f=1;a[0]=0;
	if(x<0) f=-1,x=-x;
	for(;!a[0]||x;x/=10) a[++a[0]]=x%10;
	if(f==-1) putchar('-');
	for(int i=a[0];i>0;i--) putchar(a[i]+48);
}

inline void writeln(){
	putchar('\n');
}

int main(){
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	n=read(),k=read();
	int m=-1;
	for(int i=1;i<=n;i++) a[i]=read();
	for(int i=1;i<=n;i++)
		if(a[i]==k){m=i;break;}
	zuo[m]=you[m]=0;
	for(int i=m-1;i>=1;i--)
		if(a[i]<k) zuo[i]=zuo[i+1]+1;
		else zuo[i]=zuo[i+1]-1;
	for(int i=m+1;i<=n;i=i++)
		if(a[i]>k) you[i]=you[i-1]+1;
		else you[i]=you[i-1]-1;
	for(int i=1;i<=m;i++) b[zuo[i]+100000]++;
	for(int i=m;i<=n;i++) ans+=(long long)b[you[i]+100000];
	printf("%I64d",ans);
	return 0;
}

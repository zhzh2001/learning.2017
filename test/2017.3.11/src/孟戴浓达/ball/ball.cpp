//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<map>
using namespace std;
ifstream fin("ball.in");
ofstream fout("ball.out");
long long a,b,yuana;
map<int,bool> hash;
long long gcd(long long a,long long b){
	if(b==0){
		return a;
	}else{
		return(gcd(b,a%b));
	}
}
int main(){
	fin>>a>>b;
	if((a%2)+(b%2)==1){
		fout<<-1<<endl;
		return 0;
	}
	if(a==0||b==0){
		fout<<0<<endl;
		return 0;
	}
	int gongyueshu=gcd(a,b);
	a/=gongyueshu;b/=gongyueshu;
	if(a>b){
		swap(a,b);
		yuana=a;
	}
	int step=0;
	while(1){
		if(step>=200000){
			fout<<-1<<endl;
			break;
		}
		//cout<<step<<endl;
		if(a>b){
			long long t=a;
			a=b;
			b=t;
		}
		if(hash[a]==true){
			fout<<-1<<endl;
			break;
		}
		hash[a]=true;
		//cout<<a<<" "<<b<<endl;
		if((b%a==0)&&((b/a)%2==0)){
			fout<<-1<<endl;
			break;
		}
		if(a==b){
			fout<<step+1;
			break;
		}
		b=b-a;
		a=2*a;
		step++;
	}
	fin.close();
	fout.close();
	return 0;
}
/*
786 564
10000000 100099998
*/

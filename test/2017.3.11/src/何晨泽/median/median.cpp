//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int num[100001],sum[100001],l[200001],r[200001];
int n,b,point,ans;
int main() {
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d",&n,&b);
	Rep(i,1,n) {
		scanf ("%d",&num[i]);
		if(num[i]>b)num[i]=1;
		else if(num[i]==b) {
			num[i]=0;
			point=i;
		} else num[i]=-1;
	}
	l[n]=1;
	r[n]=1;
	Drp(i,point-1,1) {
		sum[i]=sum[i+1]+num[i];
		l[sum[i]+n]++;
	}
	Rep(i,point+1,n) {
		sum[i]=sum[i-1]+num[i];
		r[sum[i]+n]++;
	}
	Rep(i,0,2*n-1) ans+=l[i]*r[2*n-i];
	cout<<ans<<endl;
	return 0;
}


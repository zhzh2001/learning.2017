//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,a,b,s,ans,G;
bool flag;
inline int gcd(int a,int b) {
	if (a%b==0) return b;
	return gcd(b,a%b);
}
int main() {
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d",&a,&b);
	if (a==b||(!(a&&b))) {
		cout<<"0"<<endl;
		return 0;
	}
	G=gcd(a,b);
	s=a/G+b/G;
	ans=0;
	flag=true;
	while (s!=1) {
		if (s%2!=0) {
			flag=false;
			break;
		} else {
			ans++;
			s/=2;
		}
	}
	if (flag) cout<<ans<<endl;
	else cout<<"-1"<<endl;
	return 0;
}

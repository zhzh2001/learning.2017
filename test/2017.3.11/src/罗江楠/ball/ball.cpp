#include <bits/stdc++.h>
#define File(s) \
	freopen(s".in", "r", stdin), \
	freopen(s".out", "w", stdout)
#define rep(a, b, c) \
	for(register int a=(b),_=(c);a<=_;++a)
#define ll long long
using namespace std;
int x, y, k;
int main(){
	File("ball");
	scanf("%d%d", &x, &y);
	if((x+y)&1) return puts("-1")&0;
	while(x != y){
		if(++k > 50000000) return puts("-1")&0;
		if(x < y) x ^= y ^= x ^= y;
		x -= y;
		y <<= 1;
	}
	return printf("%d\n", k+1)&0;
}


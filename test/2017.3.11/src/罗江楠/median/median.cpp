#include <bits/stdc++.h>
#define File(s) \
	freopen(s".in", "r", stdin), \
	freopen(s".out", "w", stdout)
#define rep(a, b, c) \
	for(register int a=(b),_=(c);a<=_;++a)
#define ll int
#define N 100050
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x){
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(int x){
	write(x);
	putchar('\n');
}
int n, k, a[N], t, ans, s, l=(1<<30), r;
inline int abs(int x){
	return x > 0 ? x : -x;
}
inline int max(int a, int b){
	return a > b ? a : b;
}
inline int min(int a, int b){
	return a < b ? a : b;
}
int main(){
	File("median");
ios::sync_with_stdio(false);
	n=read();k=read();
	rep(i, 1, n){
		t = read();
		if(t == k) a[i] = a[i-1], l = min(l, i), r = max(r, i);
		else if(t > k) a[i] = 1+a[i-1];
		else a[i] = a[i-1]-1;
	}
//	puts("ReadDone");
	if(!r)return putchar('0')&0;
	rep(i, 1, r) for(register int j = max(i, l+(i&1)); j <= n; ++j,++j)
		if(a[j]==a[i-1]||j!=i&&abs(a[j]-a[i-1])==1) ++ans;
	writeln(ans);
	return 0;
}



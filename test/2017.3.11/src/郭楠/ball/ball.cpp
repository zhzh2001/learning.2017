#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
ll a,b,sum,num,l=1,r=2147483646,ans=-1,mid;
ll power(ll b)
{ll t=1,y=2;
	while(b)
	 {
	 	if(b&1==1) t=(t*y)%sum;
	 	b>>=1;y=(y*y)%sum;
	 }
	return t;
}
bool check(ll x)
{ll k;
   	k=power(x);
   	k=(a*k)%sum;
   	if(k==0) return true;else return false;
}
int main()
{ll i,j,k;
    freopen("ball.in","r",stdin);
    freopen("ball.out","w",stdout);
	scanf("%I64d%I64d",&a,&b);
	sum=a+b;
	num=0;
	if(sum%2==1) 
	{
	  printf("-1\n");
	  return 0;
    }
	while(l<=r)
	 {
	 	mid=(l+r)/2;
	 	if(check(mid)) {ans=mid;r=mid-1;}else l=mid+1;
	 }
	printf("%I64d",ans);
}

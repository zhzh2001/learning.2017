#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
int n,m,a[N],fr[N*2],be[N*2],c,sum=0;
int read()
{int t=0;char ch;
	ch=getchar();
	while(!(ch>='0' && ch<='9')) ch=getchar();
	while(ch>='0' && ch<='9')
	  {
	  	t=t*10+ch-48;
	  	ch=getchar();
	  }
	return t;
}
int ans;
int main()
{int i,j,k;
    freopen("median.in","r",stdin);
    freopen("median.out","w",stdout);
	n=read();m=read();
	for(i=1;i<=n;i++) 
	{
	 a[i]=read();
	 if(a[i]==m) c=i;
    }
    sum=0;
    for(i=c-1;i>=1;i--)
     {
     	if(a[i]<m) sum--;else sum++;
     	fr[sum+N]++;
     }
     sum=0;
    for(i=c+1;i<=n;i++)
     {
     	if(a[i]<m) sum--;else sum++;
     	be[sum+N]++;
     }
    for(i=-n;i<=n;i++) ans+=fr[N-i]*be[N+i];
    ans+=fr[N];ans+=be[N];ans++;
    printf("%d\n",ans);
}

#include<bits/stdc++.h>
#define ll unsigned long long
using namespace std;
ll f[21][10]={0},pp[21],a[20],sum=0,n;
ll rp(int p){
	if(p<=0)return 1;if(p==1)return 10;
	if(p==2)return 1e2;if(p==3)return 1e3;if(p==4)return 1e4;
	if(p==5)return 1e5;if(p==6)return 1e6;if(p==7)return 1e7;
	if(p==8)return 1e8;if(p==9)return 1e9;if(p==10)return 1e10;
	if(p==11)return 1e11;if(p==12)return 1e12;if(p==13)return 1e13;
	if(p==14)return 1e14;if(p==15)return 1e15;if(p==16)return 1e16;
	if(p==17)return 1e17;if(p==18)return 1e18;
}
inline ll dp(ll p){
	if(p<0)return 0;a[0]++;if(p==0)return 0;
	memset(pp,0,sizeof pp);
	int l=0;for(;p;p/=10)pp[++l]=p%10;
	for(int i=1;i<l;i++){
		for(int j=1;j<=9;j++)sum+=f[i][j];}
	for(int i=l;i>=1;i--){
		int j;if(i==l)j=1;else j=0;
		for(;j<pp[i];j++)sum+=f[i][j];
	}
	for(int i=1;i<l;i++){
		if(pp[i]==9&&pp[i+1]==4){
			if(i==1)sum++;
			else sum+=n%rp(i-1)+1;
		}
	}
	return sum;
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	for(int i=2;i<=20;i++)
		for(int j=0;j<=9;j++)
			for(int k=0;k<=9;k++){
				if(j==4&&k==9)f[i][j]+=rp(i-2);
				else f[i][j]+=f[i-1][k];
			}
	scanf("%I64d",&n);
	printf("%I64d",dp(n));
}

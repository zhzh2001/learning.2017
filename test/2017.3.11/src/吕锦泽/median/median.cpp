#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=100005;
int n,k,x,id,ans;
int f[N],g[N],a[N];
int main(){
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	scanf("%d%d",&n,&k);
	for (int i=1;i<=n;i++){
		scanf("%d",&x);
		f[i]=f[i-1]; g[i]=g[i-1];
		if (x<k) f[i]++;
		else if (x>k) g[i]++;
		else id=i;
	}
	for (int i=1;i<=id;i++)
	for (int j=id;j<=n;j++)
		if (f[j]-f[i-1]==g[j]-g[i-1]) ans++;
	printf("%d",ans);
} 

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
ll a,b,num,f[50],flag;
ll gcd(ll x,ll y){return y?gcd(y,x%y):x;} 
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	f[1]=1;
	for (int i=2;i<=32;i++)
		f[i]=f[i-1]*2;
	scanf("%I64d%I64d",&a,&b);
	num=a+b;
	if (a<b) swap(a,b);
	if (num%2){printf("-1"); return 0;}
	if (a%2){flag=1; b*=2; a=num-b;}
	ll d=gcd(a,b),t=num/d;
	for (int i=1;i<=32;i++)
		if (f[i]==t){
			printf("%I64d",flag+i-1);
			return 0;
		}
	printf("-1");
}

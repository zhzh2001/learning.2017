#include<bits/stdc++.h>
using namespace std;
long long f[50][3],c[50],n;
long long dfs(long long  pos,long long  have,bool pig){
	if (pos<=0)
		return have==2;
	if (!pig && f[pos][have]!=-1)
		return f[pos][have];
	int num=pig?c[pos]:9;
	long long ans=0;
	for (int i=0;i<=num;i++){
		int have1=have;9;
		if (have==1 && i==9)
			have1=2;
		else
			if (have==0 && i==4)
				have1=1;
			else
				if (have==1 && i!=4)
					have1=0;
		ans+=dfs(pos-1,have1,pig&i==num);
	}
	if (!pig)
		f[pos][have]=ans;
	return ans;
}
long long  solve(long long n){
	memset(f,-1,sizeof(f));
	int tot=0;
	while (n){
		tot++;
		c[tot]=n%10;
		n/=10;
	}
	return dfs(tot,0,1);
}
int main(){
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	cin>>n;
	cout<<solve(n);
}

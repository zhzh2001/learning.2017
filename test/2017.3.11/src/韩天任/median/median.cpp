#include<bits/stdc++.h>
using namespace std;
long long n,m,mun,maxn,ans=0,flag;
int f[200000];
int main(){
	freopen("median.in", "r", stdin);
	freopen("median.out", "w", stdout);
	cin>>n>>m;
	for (int i=1;i<=n;i++){
		cin>>mun;
		if (mun<m)
			f[i]=-1;
		if (mun>m)
			f[i]=1;
		if (mun==m){
			f[i]=0;
			maxn=i;
		}
	}
	for (int i=maxn+1;i<=n;i++)
		f[i]=f[i-1]+f[i];
	for (int i=maxn-1;i>=1;i--)
		f[i]=f[i+1]+f[i];
	flag=3;
	while (flag<=n){
		for (int l=maxn-flag+1;l<=maxn;l++){
			long long r=l+flag-1;
			if (l>0 && r<=n)
				if (f[l]+f[r]==0)
					ans++;
		}
		flag+=2;
	}
	cout<<ans+1;
}

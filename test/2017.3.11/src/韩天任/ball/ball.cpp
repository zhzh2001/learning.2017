#include<bits/stdc++.h>
using namespace std;
long long  a,b,ans;
bool pd(long long x,long long y){
	if (x>y){
		long long num=x/y;
		if (num%2==0)
			return true;
	}
	if (y>x){
		long long num=y/x;
		if (num%2==0)
			return true;
	}
	return false;
}
int main(){
	freopen("ball.in", "r", stdin);
	freopen("ball.out", "w", stdout);
	cin>>a>>b;
	if ((a+b)%2==1){
		cout<<"-1";
		return 0;
	}
	while (a!=b){
		if (a>b){
			a=a-b;
			b=b+b;
		}
		else{
			b=b-a;
			a=a+a;
		}
		if (pd(a,b)){
			cout<<"-1";
			return 0;
		}
		ans++;
	}
	cout<<ans+1;
}

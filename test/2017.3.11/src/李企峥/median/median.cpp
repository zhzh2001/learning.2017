#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,k,m,ans,l,r,y;
int lz[100000],lf[100000],rz[100000],rf[100000],a[100000],d[100000],x[100000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("median.in","r",stdin);
    freopen("median.out","w",stdout);		
	n=read();
	k=read();
	For(i,1,n)
	{
		a[i]=read();
		if(a[i]==k) m=i;
	}
	d[m]=0;
	x[m]=0;
	l=0;
	r=0;
	Rep(i,1,m-1)
	{
		if(a[i]>k)
		{
			d[i]=d[i+1]+1;
			x[i]=x[i+1];
		}
		else
		{
			d[i]=d[i+1];
			x[i]=x[i+1]+1;
		}
		y=d[i]-x[i];
		if(y>0) lz[y]++;
		if(y<0) lf[-y]++;
		if (y==0) l++;
	}
	For(i,m+1,n)
	{
		if(a[i]>k)
		{
			d[i]=d[i-1]+1;
			x[i]=x[i-1];
		}
		else
		{
			d[i]=d[i-1];
			x[i]=x[i-1]+1;
		}
		y=d[i]-x[i];
		if(y>0) rz[y]++;
		if(y<0) rf[-y]++;
		if (y==0) r++;
	}
	ans=l+r+1+l*r;
	For(i,1,n)
	{
		ans+=(ll)(lz[i]*rf[i]);
		ans+=(ll)(lf[i]*rz[i]);
	}
	cout<<ans<<endl;
	return 0;
}


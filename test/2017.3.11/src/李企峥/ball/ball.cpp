#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll a,b,x,ans,n,n1;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll gcd(ll n,ll m)
{
	if (m==0) return n;
	if (n%2==0&&m%2==0) return 2*gcd(max(n/2,m/2),min(n/2,m/2));
	if (n%2==0&&m%2!=0) return gcd(max(n/2,m),min(n/2,m));
	if (n%2!=0&&m%2==0) return gcd(max(n,m/2),min(n,m/2));
	if (n%2!=0&&m%2!=0) return gcd(abs(m-n),min(n,m));
}
int main()
{
	freopen("ball.in","r",stdin);
    freopen("ball.out","w",stdout);	
	a=read();
	b=read();
	n=a+b;
	n1=n/gcd(n,a);
	while(n1>1)
	{
		if(n1%2>0)
		{
			cout<<-1<<endl;
			return 0;
		}
		n1/=2;
	}
	n1=n/gcd(n,b);
	while(n1>1)
	{
		if(n1%2>0)
		{
			cout<<-1<<endl;
			return 0;
		}
		n1/=2;
	}	
	if((a+b)%2>0) 
	{
		cout<<-1<<endl;
		return 0;
	}
	x=a+b;
	ans=0;
	while (ans<75000000)
	{
		if(a<b) swap(a,b); 
		b=b*2;
		a=x-b;
		ans++;
		if(a==0) 
		{
			cout<<ans<<endl;
			return 0;
		}
	}
	cout<<-1<<endl;
	return 0;
}


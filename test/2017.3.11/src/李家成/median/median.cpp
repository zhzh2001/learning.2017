#include<cstdio>
const int Base=(1e5+5);
int  loc,i,n,k,a[Base],cnt[Base<<1],h,l;
long long ans;
int main()
{
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	scanf("%d%d",&n,&k);
	for(loc=0,ans=i=1;i<=n;i++){
		scanf("%d",&a[i]);
		if(a[i]==k)loc=i;
	}
	for(h=l=0,i=loc+1;i<=n;i++){
		if(a[i]>k)h++;else l++;
		cnt[Base+h-l]++;		
	}
	for(h=l=0,i=loc-1;i;i--){
		if(a[i]>k)h++;else l++;
		if(h==l)ans++;
		ans+=cnt[Base-h+l];
	}
	printf("%I64d\n",ans);
}

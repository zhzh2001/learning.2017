#include<cstdio>
#include<cmath>
#include<algorithm>
int a,b,k,tmp;
long long mid;
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d%d",&a,&b);
	if((a+b)%2){puts("-1");goto E;}
	mid=(1ll*a+b)>>1;
	if(a>b)std::swap(a,b);
	if(a==b){puts("1");goto E;}
	else{
		if(mid%a){puts("-1");goto E;}
		tmp=mid/a;
		k=log2(tmp);
		if((1<<k)!=tmp){puts("-1");goto E;}
		if(b==(2*tmp-1)*mid/tmp){printf("%d\n",k+1);goto E;}
	}
	puts("-1");
	E:return 0;
}

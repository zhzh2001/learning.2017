#include<iostream>
#include<cstdio>
#include<cmath>
#define ll long long
using namespace std;
inline void dfs(ll x,ll y,ll step)
{
	if(step>=1e4)	{printf("-1\n");return;}
	if(x>y)	swap(x,y);
	if(y==x){printf("%d\n",step+1);return;}
	dfs(x+x,y-x,step+1);
}	
inline ll gcd(ll x,ll y)
{
	return y?gcd(y,x%y):x;
}
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	int x,y;
	scanf("%d%d",&x,&y);
	if(x==0||y==0)	
	{
		printf("-1\n");
 		return 0;
	}
	if(x>y)	swap(x,y);
	ll t_g=gcd(x,y);
	x/=t_g;y/=t_g;
	bool flag=0;
	for(ll ttt=1;ttt<=2147483648;ttt*=2)	if(ttt==x+y)	{flag=1;break;}
	if(!flag)	{printf("-1");return 0;}
	ll t=x+y;
	ll tt=1;
	dfs(x,y,0);
}

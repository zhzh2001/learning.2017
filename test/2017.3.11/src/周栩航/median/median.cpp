#include<iostream>
#include<cstdio>
#include<cmath>
#define ll long long
using namespace std;
int fs=200000;
int sag1[200001],sag2[200001],a[100001];
int n,k,tag;
int main()
{
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	scanf("%d%d",&n,&k);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&a[i]);
		if(a[i]==k)
			tag=i;
	}
	int now=0;
	for(int i=tag-1;i>=1;i--)
	{
		if(a[i]>k)	now++;else now--;
		sag1[fs+now]++;
	}
	now=0;
	for(int i=tag+1;i<=n;i++)
	{
		if(a[i]>k)	now++;else now--;
		sag2[fs+now]++;
	}
	ll ans=sag1[fs]*sag2[fs]+sag2[fs]+sag1[fs]+1;
	for(int i=1;i<=n;i++)
	{
		ans+=(sag1[fs+i]*sag2[fs-i]+sag1[fs-i]*sag2[fs+i]);
	}
	printf("%lld\n",ans);
}

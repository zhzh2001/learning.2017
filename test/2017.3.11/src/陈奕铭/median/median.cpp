#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x<0){putchar('-'); x=-x;}
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
void writeln(int x){
	write(x);
	puts("");
}

const int N=100010;
int a[N],b[N],c[N];
int n,k,cnt,l=0,ans;

void dfs(int l,int p){
	if(p==0) ans++;
	int r=0;
	for(int i=l+2;i<=n;i+=2){
		if(b[i-1]==0) r++;
		else p+=b[i-1];
		if(b[i]==0) r++;
		else p+=b[i];
		if(abs(p)<=r) ans++;
	}
}

int main(){
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	n=read(); k=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]<k) b[i]=-1;
		else if(a[i]>k) b[i]=1;
		else{b[i]=0;c[++cnt]=i;}
	}
	int p,q,r;
	for(int i=1;i<=cnt;i++)
		cout<<c[i]<<endl;
	for(int i=1;i<=cnt;i++){
		p=0;
		for(int j=c[i];j>l;j--){
			p+=b[j];
			if((c[i]-j)%2==0)
				dfs(c[i],p);
			else
				if(c[i]<n)
					dfs(c[i]+1,p+b[c[i]+1]);
		}
		l=c[i];
	}
	write(ans);
	return 0;
}
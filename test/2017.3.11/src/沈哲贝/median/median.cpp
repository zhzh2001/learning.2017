#include<cstdio>
#include<algorithm>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define maxn 200010
#define inf 1000000000
#define f1(x)	f1[x+100000]
#define f2(x)	f2[x+100000]
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x;	if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll pos,ans,f1[maxn],f2[maxn],a[maxn],n,k;
int main(){
	freopen("median.in","r",stdin);
	freopen("median.out","w",stdout);
	n=read();	k=read();
	For(i,1,n){
		a[i]=read();
		if (a[i]==k)	pos=i;
	}
	ll mark=0;
	FOr(i,pos-1,1)	mark+=a[i]>k?1:-1,f1(mark)++;
	mark=0;
	For(i,pos+1,n)	mark+=a[i]>k?1:-1,f2(mark)++;
	For(i,1,n)	ans+=f1(i)*f2(-i)+f1(-i)*f2(i);
	writeln(ans+(f1(0)+1)*(f2(0)+1));
}

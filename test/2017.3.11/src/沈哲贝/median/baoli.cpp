#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 20000 
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll a[maxn],b[maxn],n,k,ans;
int main(){
	freopen("median.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	k=read();
	For(i,1,n)	a[i]=read();
	For(i,1,n)	For(j,i,n)
	if (!((j-i)%2)){
		ll tot=0;
		For(x,i,j)	b[++tot]=a[x];
		sort(b+1,b+tot+1);
		ans+=b[tot/2+1]==k;
	}
	writeln(ans);
}

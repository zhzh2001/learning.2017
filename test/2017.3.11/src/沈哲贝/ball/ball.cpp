#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
#define inf 2000000001
#define maxn 2000010
#define mod 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll t,a,b,ans;
ll gcd(ll x,ll y){
	if (!y)	return x;
	return gcd(y,x%y);
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	a=read();	b=read();
	if(!a||!b){
		writeln(0);
		return 0;
	} 
	t=gcd(a,b);
	a/=t;	b/=t;
	t=a+b;
	while(t!=1){
		if (t&1){
			puts("-1");
			return 0;
		}
		t>>=1;
	}
	while(a!=b){
		if (a<b)	swap(a,b);
		a-=b;
		b<<=1;
		ans++;
	}
	writeln(ans+1);
}

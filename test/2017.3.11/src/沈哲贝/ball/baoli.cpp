#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int 
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 200000000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll a,b,ans;
bool vis[maxn];
int main(){
	freopen("ball.in","r",stdin);
	freopen("baoli.out","w",stdout);
	a=read();	b=read();
	if (!a||!b){
		writeln(0);
		return 0;
	} 
	while(a!=b){
		if (b>a)	swap(a,b);
		if (vis[b]){
			writeln(-1);
			return 0;
		}
		vis[b]=1; a-=b;	b<<=1;	ans++;
	}
	writeln(ans+1);
}

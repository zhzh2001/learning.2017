#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<cmath>
#include<queue>
#include<memory.h>
#define ll unsigned long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 200010
#define inf 1000000000
#define eps 1e-9
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
char n[30];
ll f[30][30],bin[30],a[30],ans;
void pre(){
	bin[1]=1;
	For(i,2,19)	bin[i]=bin[i-1]*10;
	For(i,0,9)	f[1][i]=0;
	For(i,2,19)	For(j,0,9)	For(k,0,9){
		if(j==4&&k==9)	f[i][j]+=bin[i-1];
		else	f[i][j]+=f[i-1][k];
	}
}
void dp(){
	if(a[0]==1)	return;
	FOr(i,a[0],0){
		if(a[i+2]==4&&a[i+1]==9){
			ll x=0;
			FOr(k,i,1)	x=x*10+a[k];
			ans+=x+1;
			return;
		}
		if(!i)	return;
		ll s=0,t=a[i]-1;
		if(i==1)	t++;
		For(j,s,t)	ans+=f[i][j];
	}
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	pre();
	scanf("%s",n+1);
	a[0]=strlen(n+1);
	For(i,1,a[0])	a[a[0]-i+1]=n[i]-'0';
	dp();
	writeln(ans);
}

#include <cstdio>
using namespace std;

const int nn=100016 ;
int n,a[nn],wei,lmin[nn],lmax[nn],rmin[nn],rmax[nn],ans,ma,x,y ;
int main()
{
	freopen("median.in","r",stdin) ;
	freopen("median.out","w",stdout) ;
	scanf("%d%d",&n,&x) ;
	for(int i=1;i<=n;i++) 
		scanf("%d",&a[ i ]) ;
	for(int i=1;i<=n;i++) 
		if(a[ i ]==x) 
		{
			wei = i;
			break ;
		}
	ans = 1;
	for(int i=wei-1;i>=1;i--) 
	{
		lmin[ i ] = lmin[i+1] ;
		if(a[i]<a[wei]) lmin[i]++; 
		lmax[ i ] = lmax[i+1] ;
		if(a[i]>a[wei]) lmax[i]++;
		if(lmin[i]==lmax[i]) ans++;
	}
	for(int i=wei+1;i<=n;i++) 
	{
		rmin[ i ] = rmin[i-1];
		if(a[wei]>a[i]) rmin[i]++;
		rmax[ i ] = rmax[i-1];
		if(a[wei]<a[i]) rmax[i]++;
		if(rmin[i]==rmax[i]) ans++;
	} 
	
	for(int i=1;i<wei;i++) 
	  for(int j=wei+1;j<=n;j++) 
	  {
	  	if( (j-i+1)%2==0 ) continue;
	  	x = lmin[i]+rmin[j] ;
	  	y = lmax[i]+rmax[j] ;
	  	if(x==y) ans++;
	  }
	printf("%d\n",ans) ;
	return 0;
}


/*


7 4 
5 7 2 4 3 1 6



*/

var k,p,a,b,c:int64;
    i:longint;
function gcd(x,y:int64):int64;
begin
  if y=0 then exit(x);
  exit(gcd(y,x mod y));
end;
begin
  assign(input,'ball.in');
  assign(output,'ball.out');
  reset(input);
  rewrite(output);
  readln(a,b);
  c:=gcd(a,b);
  a:=a div c;
  b:=b div c;
  p:=trunc(ln(a+b)/ln(2)+0.0000000001);
  k:=1;
  for i:=1 to p do k:=k*2;
  if k<>a+b then writeln(-1)
            else writeln(p);
  close(input);
  close(output);
end.

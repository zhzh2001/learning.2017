var ans,j,id,i,n,k:longint;
    a,b,c:array[0..200000]of longint;
begin
  assign(input,'median.in');
  assign(output,'median.out');
  reset(input);
  rewrite(output);
  readln(n,k);
  for i:=1 to n do
     begin
     read(a[i]);
     if a[i]>=k then b[i]:=b[i-1]+1
                else b[i]:=b[i-1];
     if a[i]<=k then c[i]:=c[i-1]+1
                else c[i]:=c[i-1];
     if a[i]=k then id:=i;
     end;
  for i:=0 to id-1 do
     begin
     if (id-i)mod 2=0 then j:=id+1
                      else j:=id;
     while j<=n do
        begin
	if b[j]-b[i]=c[j]-c[i] then
  	   inc(ans);
	j:=j+2;
 	end;
     end;
  writeln(ans);
  close(input);
  close(output);
end.

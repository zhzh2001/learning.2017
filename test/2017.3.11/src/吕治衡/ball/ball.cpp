#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime> 
#include<iomanip> 
using namespace std;
long long x,y,lsg,k;
long long gcd(long long x,long long y)
	{
		if (x>y)return gcd(y,x);
		if (x==0)return(y);
		return gcd(x,y%x);
	}
int main()
{
	freopen("ball.in","r",stdin);freopen("ball.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>x>>y;
	if (x<y)swap(x,y);
	k=gcd(x,y);
	x/=k;y/=k;k=0;
	while (((x!=1)||(y!=1)))
		{
			if (k==1){cout<<-1;return 0;}
			x-=y;y*=2;lsg++;
			k=gcd(x,y);
			x/=k;y/=k;
			if (x<y)swap(x,y);
		}
	cout<<lsg+1;
}

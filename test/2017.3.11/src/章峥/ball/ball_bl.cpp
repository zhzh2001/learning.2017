#include<iostream>
using namespace std;
bool vis[2005][2005];
int main()
{
	int a,b;
	while(cin>>a>>b)
	{
		if(a<b)
			swap(a,b);
		vis[a][b]=true;
		while(true)
		{
			cout<<a<<' '<<b<<endl;
			a-=b;
			b*=2;
			if(a==0)
			{
				cout<<"Yes\n";
				break;
			}
			if(a<b)
				swap(a,b);
			if(vis[a][b])
			{
				cout<<"No\n";
				break;
			}
			vis[a][b]=true;
		}
	}
	return 0;
}
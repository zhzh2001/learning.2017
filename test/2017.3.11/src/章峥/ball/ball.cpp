#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("ball.in");
ofstream fout("ball.out");
int gcd(int a,int b)
{
	return b==0?a:gcd(b,a%b);
}
int main()
{
	int a,b;
	fin>>a>>b;
	int g=gcd(a,b);
	a/=g;b/=g;
	int sum=a+b;
	if(sum&(sum-1))
		fout<<-1<<endl;
	else
		fout<<(int)log2(sum)<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
ifstream fin("median.in");
ofstream fout("median.ans");
const int N=1005;
int a[N],b[N];
int main()
{
	int n,k;
	fin>>n>>k;
	int p;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		if(a[i]==k)
			p=i;
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j+=2)
			if(i<=p&&j>=p)
			{
				memcpy(b,a+i,(j-i+1)*sizeof(int));
				sort(b,b+j-i+1);
				if(b[(j-i)/2]==k)
					ans++;
			}
	fout<<ans<<endl;
	return 0;
}
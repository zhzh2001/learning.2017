program gen;
const
  n=4;
var
  s:string;
  i:longint;
begin
  randomize;
  assign(output,'lucky.in');
  rewrite(output);
  s:='49';
  for i:=1 to n do
    if(random(2)=1) then
	  s:=s+chr(random(10)+ord('0'))
	else
      s:=chr(random(10)+ord('0'))+s;
  i:=1;
  while s[i]='0' do
    inc(i);
  writeln(copy(s,i,6-i+1));
  close(output);
end.
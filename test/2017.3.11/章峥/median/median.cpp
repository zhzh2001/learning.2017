#include<fstream>
using namespace std;
ifstream fin("median.in");
ofstream fout("median.out");
const int N=100005;
int cnt[2*N][2];
int main()
{
	int n,k;
	fin>>n>>k;
	int sum=0,ans=0;
	cnt[n][0]=1;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		if(x>k)
			sum++;
		else
			if(x<k)
				sum--;
		ans+=cnt[sum+n][(i&1)^1];
		cnt[sum+n][i&1]++;
	}
	fout<<ans<<endl;
	return 0;
}
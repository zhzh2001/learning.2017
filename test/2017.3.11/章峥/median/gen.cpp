#include<bits/stdc++.h>
using namespace std;
const int n=1000;
ofstream fout("median.in");
int main()
{
	srand(time(NULL));
	int* a=new int [n];
	for(int i=0;i<n;i++)
		a[i]=i+1;
	random_shuffle(a,a+n);
	fout<<n<<' '<<rand()%n+1<<endl;
	for(int i=0;i<n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
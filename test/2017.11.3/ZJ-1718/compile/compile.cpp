#include<fstream>
#include<string>
#include<cctype>
#include<map>
#include<vector>
using namespace std;
ifstream fin("compile.in");
ofstream fout("compile.out");
const int N=200005;
int vis[N];
int main()
{
	int n,q;
	string s;
	fin>>n>>q>>s;
	s=' '+s;
	map<pair<int,int>,vector<int> > m;
	for(int i=1;i<=q;i++)
	{
		int l,r;
		fin>>l>>r;
		map<pair<int,int>,vector<int> >::iterator it=m.find(make_pair(l,r));
		if(it!=m.end())
		{
			for(int i=0;i<10;i++)
				fout<<it->second[i]<<' ';
			fout<<endl;
			continue;
		}
		string t=s;
		vector<int> ans(10,0);
		int p=l,dir=1;
		while(p>=l&&p<=r)
		{
			if(isdigit(t[p]))
			{
				ans[t[p]-'0']++;
				if(t[p]=='0')
					vis[p]=i;
				else
					t[p]--;
			}
			else if(t[p]=='>')
			{
				dir=1;
				int tmp=p;
				do
					p+=dir;
				while(p>=l&&p<=r&&vis[p]==i);
				if(p>=l&&p<=r&&!isdigit(t[p]))
					vis[tmp]=i;
				continue;
			}
			else
			{
				dir=-1;
				int tmp=p;
				do
					p+=dir;
				while(p>=l&&p<=r&&vis[p]==i);
				if(p>=l&&p<=r&&!isdigit(t[p]))
					vis[tmp]=i;
				continue;
			}
			do
				p+=dir;
			while(p>=l&&p<=r&&vis[p]==i);
		}
		for(int i=0;i<10;i++)
			fout<<ans[i]<<' ';
		fout<<endl;
		m[make_pair(l,r)]=ans;
	}
	return 0;
}

#include<fstream>
#include<vector>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
using namespace std;
ifstream fin("city.in");
ofstream fout("city.out");
const int N=5005,INF=0x3f3f3f3f;
vector<pair<int,int> > mat[N];
int n,d[N];
int bfs(int s,int u,int v)
{
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	queue<int> Q;
	Q.push(s);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=0;i<mat[k].size();i++)
			if(!((k==u&&mat[k][i].first==v)||(k==v&&mat[k][i].first==u))&&d[mat[k][i].first]==INF)
			{
				d[mat[k][i].first]=d[k]+mat[k][i].second;
				Q.push(mat[k][i].first);
			}
	}
	int ret=s;
	for(int i=1;i<=n;i++)
	{
		if(d[i]==INF)
			return -1;
		if(d[i]>d[ret])
			ret=i;
	}
	return ret;
}
int main()
{
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	int ans=d[bfs(bfs(1,0,0),0,0)];
	for(int i=1;i<=n;i++)
		for(int j=0;j<mat[i].size();j++)
			for(int k=1;k<=n;k++)
			{
				if(clock()>2.8*CLOCKS_PER_SEC)
				{
					fout<<ans<<endl;
					return 0;
				}
				mat[i].push_back(make_pair(k,mat[i][j].second));
				mat[k].push_back(make_pair(i,mat[i][j].second));
				int far=bfs(1,i,mat[i][j].first);
				if(~far)
					ans=min(ans,d[bfs(far,i,mat[i][j].first)]);
				mat[i].pop_back();
				mat[k].pop_back();
			}
	fout<<ans<<endl;
	return 0;
}

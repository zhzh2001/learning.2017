#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int P=1e5;
int ran(){return (rand()<<15)+rand();}
bool flag;
int main(){
	freopen("compile.in","w",stdout);
	srand(time(0));rand();
	//int n=ran()%(P+1)+P;
	//int m=ran()%(P+1)+P;
	int n=2e5,m=2e5;
	int i;
	printf("%d %d\n",n,m);
	for (i=1;i<=n;i++){
		if (rand()%100<5){
			if ((!flag)&&rand()%3){
				putchar('>');
				flag=1;
			}
			else{
				putchar('<');
				flag=0;
			}
		}
		else{
			putchar(rand()%10+48); 
		}
	}
	putchar('\n');
	for (;m--;){
		int l=ran()%n+1,r=ran()%n+1;
		if (l>r) swap(l,r);
		printf("%d %d\n",l,r);
	}
}

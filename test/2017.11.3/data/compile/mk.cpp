#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int P=1e5;
int ran(){return (rand()<<15)+rand();}
int main(){
	freopen("compile.in","w",stdout);
	srand(time(0));rand();
//	int n=ran()%(P+1)+P;
//	int m=ran()%(P+1)+P;
	int n=2e5,m=2e5;
	int i;
	printf("%d %d\n",n,m);
	for (i=1;i<=n;i++){
		if (rand()%100<30){
			if (rand()%2){
				putchar('>');
			}
			else{
				putchar('<');
			}
		}
		else{
			putchar(rand()%10+48); 
		}
	}
	putchar('\n');
	for (;m--;){
		int l=ran()%n+1,r=ran()%n+1;
		if (l>r) swap(l,r);
		printf("%d %d\n",l,r);
	}
}

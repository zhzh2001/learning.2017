#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5005,M=2*N,inf=2e9;
int n,i,d,v,ans;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int et,he[N];
struct edge{int l,to,v;}e[M];
void line(int x,int y,int v){
	e[++et].l=he[x];he[x]=et;
	e[et].to=y;e[et].v=v;
}
int dn[N],up[N];
void down(int x,int fa){
	dn[x]=up[x]=0;
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa) continue;
		down(y,x);int k=e[i].v+dn[y];
		d=max(d,dn[x]+k);dn[x]=max(dn[x],k);
	}
}
void upt(int x,int fa){
	v=min(v,max(dn[x],up[x]));
	int mx=0,id,mx2=0;
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa) continue;
		int k=e[i].v+dn[y];
		if (k>mx) mx2=mx,mx=k,id=y;
		else if (k>mx2) mx2=k;
	}
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa) continue;
		up[y]=e[i].v+max(up[x],id==y?mx2:mx);upt(y,x);
	}
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	for (n=read(),et=1,i=1;i<n;i++){
		int x=read(),y=read(),v=read();
		line(x,y,v);line(y,x,v);
	}
	for (ans=inf,i=2;i<=et;i+=2){
		int x=e[i^1].to,y=e[i].to,dx,dy,vx,vy;
		d=0;down(x,y);dx=d;v=inf;upt(x,y);vx=v;
		d=0;down(y,x);dy=d;v=inf;upt(y,x);vy=v;
		ans=min(ans,max(vx+vy+e[i].v,max(dx,dy)));
	}
	printf("%d",ans);
}

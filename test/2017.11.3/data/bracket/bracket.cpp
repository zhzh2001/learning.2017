#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=5e5+2000,M=2000;
int n,m,nn,Q,i,j,a[N],t,d[N],lt[N],dt[N];
char s[N];bool vis[N];
int g[N],f[N];ll ans[N];
struct que{int l,r,id;}q[N];
bool operator < (que A,que B){return A.r<B.r;}
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
ll cal(int x){return (ll)x*(x-1)/2;}
int w[N];
void work(int x){
	memset(w,0,4*(n+1));
	for (;x;x=lt[x]) w[x]=1,vis[x]=1;
	for (i=2;i<=n;i++) w[i]+=w[i-1];
	for (i=1;i<=Q;i++) ans[i]+=cal(w[q[i].r]-w[q[i].l-1]);
}
int ask(int x){
	int res=0;
	for (int i=(x/m+1)*m-1;i>=x;i--) res+=g[i];
	for (int i=x/m+1;i<=nn;i++) res+=f[i];
	return res; 
}
int main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s);n=strlen(s);
	for (a[0]=n+1,a[1]=0,n++,i=2;i<=n;i++)
		a[i]=a[i-1]+(s[i-2]=='('?1:-1);
	for (i=1;i<=n;i++){
		for (;t&&a[i]<a[d[t]];t--);
		if (a[i]==a[d[t]]) lt[i]=d[t],dt[i]=dt[d[t]]+1;
		d[++t]=i;
	}
	for (Q=read(),i=1;i<=Q;i++)
		q[i]=(que){read(),read()+1,i};
	m=sqrt(2*n);nn=n/m;
	for (i=n;i;i--) if (!vis[i]&&dt[i]>=m) work(i);
	sort(q+1,q+Q+1);t=1;
	for (i=1;i<=n;i++){
		if (!vis[i]) for (j=lt[i];j;j=lt[j]) g[j]++,f[j/m]++;
		for (;q[t].r==i;t++) ans[q[t].id]+=ask(q[t].l);
	}
	for (i=1;i<=Q;i++) write(ans[i]),putchar('\n');
}

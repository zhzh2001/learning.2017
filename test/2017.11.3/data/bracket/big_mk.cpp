#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int P=25e4;
int n,m;
int ran(){return (rand()<<15)+rand();}
void work(int n){
	if (n==0) return;
	int m=ran()%(n/2+1)+1;n-=m;
	for (int i=1;i<=m;i++){
		putchar('(');
		if (n&&rand()%100<50){
			int k=ran()%(5*n/m+1)+1;
			if (k>n) k=n;
			work(k);n-=k;
		}
		putchar(')');
	}
	work(n);
}
int main(){
	freopen("bracket.in","w",stdout);
	srand(time(0));rand();
	n=ran()%(P+1)+P;m=ran()%(P+1)+P;
	//n=m=5e5;
	work(n/2);
	puts("");
	printf("%d\n",m);
	for (;m--;){
		int l=ran()%n+1,r=ran()%n+1;
		if (l>r) swap(l,r);
		printf("%d %d\n",l,r);
	}
}

#pragma GCC optimize 2
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("city.in");
ofstream fout("city.out");
const int N = 5005, INF = 0x3f3f3f3f;
int n, head[N], v[N * 2], w[N * 2], nxt[N * 2], cc, f[N], g[N];
inline void add_edge(int u, int v, int w)
{
	::v[++cc] = v;
	::w[cc] = w;
	nxt[cc] = head[u];
	head[u] = cc;
}
int dfs(int k, int fat)
{
	int ret = 0;
	f[k] = g[k] = 0;
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != fat)
		{
			ret = max(ret, dfs(v[i], k));
			ret = max(ret, f[k] + w[i] + f[v[i]]);
			f[k] = max(f[k], w[i] + f[v[i]]);
		}
	return ret;
}
int dfs2(int k, int fat)
{
	int ret = max(f[k], g[k]), c1 = 0, c2 = 0;
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != fat)
			if (w[i] + f[v[i]] > c1)
			{
				c2 = c1;
				c1 = w[i] + f[v[i]];
			}
			else
				c2 = max(c2, w[i] + f[v[i]]);
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != fat)
		{
			g[v[i]] = w[i] + max(g[k], w[i] + f[v[i]] == c1 ? c2 : c1);
			ret = min(ret, dfs2(v[i], k));
		}
	return ret;
}
int main()
{
	fin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v, w;
		fin >> u >> v >> w;
		add_edge(u, v, w);
		add_edge(v, u, w);
	}
	int ans = INF;
	for (int i = 1; i <= cc; i += 2)
	{
		int c1 = dfs(v[i], v[i + 1]), c2 = dfs(v[i + 1], v[i]);
		if (c1 >= ans || c2 >= ans)
			continue;
		ans = min(ans, max(max(c1, c2), dfs2(v[i], v[i + 1]) + dfs2(v[i + 1], v[i]) + w[i]));
	}
	fout << ans << endl;
	return 0;
}
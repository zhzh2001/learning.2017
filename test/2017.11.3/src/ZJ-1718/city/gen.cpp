#include<fstream>
#include<ctime>
#include<cstdlib>
using namespace std;
ofstream fout("city.in");
const int n=1000;
int f[n+1];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	srand(time(NULL));
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=1;i<n;)
	{
		int u=rand()%n+1,v=rand()%n+1;
		if(getf(u)!=getf(v))
		{
			f[getf(u)]=getf(v);
			fout<<u<<' '<<v<<' '<<rand()%2000<<endl;
			i++;
		}
	}
	return 0;
}

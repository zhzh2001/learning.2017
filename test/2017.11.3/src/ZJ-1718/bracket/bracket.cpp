#include<fstream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("bracket.in");
ofstream fout("bracket.out");
const int N=500005;
vector<int> last[N];
int main()
{
	string s;
	int q;
	fin>>s>>q;
	int n=s.length();
	s=' '+s;
	for(int i=1;i<=n;i++)
	{
		int cnt=0;
		for(int j=i;j<=n;j++)
		{
			if(s[j]=='(')
				cnt++;
			else
			{
				cnt--;
				if(cnt<0)
					break;
			}
			if(!cnt)
				last[i].push_back(j);
		}
	}
	while(q--)
	{
		int l,r;
		fin>>l>>r;
		int ans=0;
		for(int i=l;i<=r;i++)
			ans+=upper_bound(last[i].begin(),last[i].end(),r)-last[i].begin();
		fout<<ans<<endl;
	}
	return 0;
}

#pragma GCC optimize 2
#include <fstream>
#include <string>
#include <cctype>
using namespace std;
ifstream fin("compile.in");
ofstream fout("compile.out");
const int N = 200005, INF = 1e9;
int l[N], r[N], into[N], outo[N], prei[N][10], preo[N][10], cnt[10];
int main()
{
	int n, q;
	string s;
	fin >> n >> q >> s;
	n++;
	s = ' ' + s;
	for (int i = 1; i <= n; i++)
	{
		l[i] = i - 1;
		r[i] = i + 1;
		into[i] = outo[i] = INF;
	}
	int t = 0;
	for (int i = 1; i <= n; i++)
		if (into[i] == INF)
		{
			int now = i, presym = 0;
			bool rt = true;
			while (now >= i && now <= n)
			{
				if (into[now] == INF)
				{
					into[now] = ++t;
					for (int j = 0; j < 10; j++)
						prei[now][j] = cnt[j];
				}
				if (!s[now])
				{
					l[r[now]] = l[now];
					r[l[now]] = r[now];
				}
				else if (isdigit(s[now]))
				{
					cnt[s[now] - '0']++;
					if (s[now] > '0')
						s[now]--;
					else
						s[now] = 0;
					presym = 0;
				}
				else
				{
					rt = s[now] == '>';
					if (presym)
						s[presym] = 0;
					presym = now;
				}
				if (!rt && outo[now] == INF)
				{
					outo[now] = t;
					for (int j = 0; j < 10; j++)
						preo[now][j] = cnt[j];
				}
				if (rt)
					now = r[now];
				else
					now = l[now];
			}
		}
	while (q--)
	{
		int l, r;
		fin >> l >> r;
		if (into[r + 1] <= outo[l])
			for (int i = 0; i < 10; i++)
				fout << prei[r + 1][i] - prei[l][i] << ' ';
		else
			for (int i = 0; i < 10; i++)
				fout << preo[l][i] - prei[l][i] << ' ';
		fout << '\n';
	}
	return 0;
}
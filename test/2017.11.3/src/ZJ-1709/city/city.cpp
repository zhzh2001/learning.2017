#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=5002;
const int maxm=10004;
struct graph{
	int n,m;
	struct edge{
		int to,dist,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to,int dist){
		e[++m]=(edge){to,dist,first[from]};
		first[from]=m;
	}
	int cur;
	int lc[maxn][maxn],dist2[maxn];
	int st[maxn],en[maxn],dfn[maxn];
	int fa[maxn];
	void dfs(int u){
		st[u]=++cur;
		dfn[cur]=u;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (st[v])
				continue;
			dist2[v]=dist2[u]+e[i].dist;
			dfs(v);
			for(int j=st[u];j<st[v];j++){
				int x=dfn[j];
				for(int k=st[v];k<=en[v];k++)
					lc[x][dfn[k]]=lc[dfn[k]][x]=u;
			}
		}
		en[u]=cur;
	}
	int far[maxn],fd[maxn],mxdist[maxn],scdist[maxn],thdist[maxn],dist[maxn];
	int tof[maxn];
	int dp[maxn];
	void dfs2(int u){
		dfn[cur]=u;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (fa[u]==v)
				continue;
			fa[v]=u;
			dist[v]=dist[u]+e[i].dist;
			dfs2(v);
			int qwq=mxdist[v]+e[i].dist;
			if (qwq>mxdist[u])
				thdist[u]=scdist[u],scdist[u]=mxdist[u],mxdist[u]=qwq;
			else if (qwq>scdist[u])
				thdist[u]=scdist[u],scdist[u]=qwq;
			else if (qwq>thdist[u])
				thdist[u]=qwq;
		}
	}
	void dfs3(int u){
		int qwq=dist[u]-dist[fa[u]]+mxdist[u];
		dp[u]=max(dp[fa[u]],fd[fa[u]]+(qwq==mxdist[fa[u]])?scdist[fa[u]]:mxdist[fa[u]]);
		if (qwq==mxdist[fa[u]])
			dp[u]=max(dp[u],scdist[fa[u]]+thdist[fa[u]]);
		else if (qwq==scdist[fa[u]])
			dp[u]=max(dp[u],mxdist[fa[u]]+thdist[fa[u]]);
		else dp[u]=max(dp[u],mxdist[fa[u]]+scdist[fa[u]]);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa[u])
				continue;
			dfs3(v);
		}
	}
	int work(){
		dfs(1);
		int now=0,nowi=0,nowj=0;
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if (dist2[i]+dist2[j]-2*dist2[lc[i][j]]>now)
					now=dist2[i]+dist2[j]-2*dist2[lc[i][j]],nowi=i,nowj=j;
		//printf("%d %d %d\n",now,nowi,nowj);
		dfs2(nowi);
		dfs3(nowi);
		int ans=INF;
		for(int u=nowj;u!=nowi;u=fa[u]){
			for(int v=u;;v=fa[v]){
				ans=min(ans,max(dp[v],now-min(dist[v],dist[fa[u]]-dist[v])));
				//printf("dist[%d]=%d dist[%d]=%d\n",u,dist[u],v,dist[v]);
				if (v==nowi)
					break;
			}
		}
		return ans;
	}
}g;
int main(){
	init();
	int n=readint();
	g.n=n;
	for(int i=1;i<n;i++){
		int u=readint(),v=readint(),c=readint();
		g.addedge(u,v,c);
		g.addedge(v,u,c);
	}
	printf("%d",g.work());
}
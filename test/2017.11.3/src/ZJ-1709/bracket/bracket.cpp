#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
char s[200003];
int sum[200003];
int ans[5003][5003];
int main(){
	init();
	int n=readstr(s+1);
	for(int i=1;i<=n;i++)
		sum[i]=sum[i-1]+(s[i]=='('?1:-1);
	//puts("WTF");
	for(int i=1;i<=n;i++){
		for(int j=i-1;j>=0;j--){
			if (sum[j]<sum[i])
				break;
			if (sum[j]==sum[i])
				ans[i][j+1]++;
		}
	}
	//puts("WTF");
	for(int i=1;i<=n;i++)
		for(int j=i;j;j--)
			ans[i][j]+=ans[i][j+1];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
			ans[i][j]+=ans[i-1][j];
	int q=readint();
	while(q--){
		int l=readint(),r=readint();
		printf("%d\n",ans[r][l]);
	}
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
char s[200004];
int nxt[200004],lst[200004];
char tmp[200004];
int ans[10];
void del(int x){
	nxt[lst[x]]=nxt[x];
	lst[nxt[x]]=lst[x];
}
int n;
void work(int l,int r){
	int now=l,dir=1;
	for(int i=l;i<=r;i++)
		nxt[i]=i+1,lst[i]=i-1;
	for(int i=0;i<10;i++)
		ans[i]=0;
	nxt[r]=n+1,lst[l]=0;
	bool flag=0;
	while(now>=l && now<=r){
		//printf("on %d\n",now);
		if (s[now]=='>'){
			if (flag)
				del(dir?lst[now]:nxt[now]);
			dir=1;
			now=nxt[now];
			flag=1;
		}else if (s[now]=='<'){
			if (flag)
				del(dir?lst[now]:nxt[now]);
			dir=0;
			now=lst[now];
			flag=1;
		}else{
			ans[s[now]-'0']++;
			if (s[now]=='0')
				del(now);
			else s[now]--;
			now=dir?nxt[now]:lst[now];
			flag=0;
		}
	}
}
int main(){
	init();
	n=readint();
	int q=readint();
	readstr(s+1);
	//puts("WTF");
	while(q--){
		int l=readint(),r=readint();
		memcpy(tmp+l,s+l,r-l+1);
		work(l,r);
		memcpy(s+l,tmp+l,r-l+1);
		for(int i=0;i<10;i++)
			printf("%d ",ans[i]);
		putchar('\n');
	}
}
#include <bits/stdc++.h>
#define N 500020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char str[N];
int n, st[N];
int work(int l, int r) {
	memset(st, 0, sizeof st);
	int res = 0, x = 250000, cntl = 0;
	while (str[l] == ')' && l <= r) l ++;
	for (int i = l; i <= r; i++) {
		if (str[i] == '(') x ++, cntl ++;
		else if (str[i] == ')') {
			if (cntl) cntl --, res += st[-- x] + 1, st[x] ++;
			else x --;
		}
	}
	return res;
}
// Oh what the f**k?
int main(int argc, char const *argv[]) {
	freopen("bracket.in", "r", stdin);
	freopen("bracket.out", "w", stdout);
	scanf("%s", str + 1);
	n = strlen(str + 1);
	int q = read();
	while (q --) {
		int l = read(), r = read();
		printf("%d\n", work(l, r));
	}
	return 0;
}
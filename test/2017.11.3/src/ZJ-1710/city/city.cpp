#pragma optimize O2
#include <bits/stdc++.h>
#define N 5020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct node {
	int first, second;
};
std::vector<node> son[N];
int x[N], y[N], z[N], mxdep, ans, pos;
int DO_NOT_TOUCH;
void dfs(int x, int f, int d = 0) {
	if (d > mxdep) mxdep = d, pos = x;
	for (std::vector<node>::iterator it = son[x].begin(); it != son[x].end(); ++it)
		if (it -> first != f && it -> first != DO_NOT_TOUCH) dfs(it -> first, x, d + it -> second);
}
// 返回以 x 为根的最深深度
int dfs2(int x, int f, int d = 0) {
	int mx = d;
	for (std::vector<node>::iterator it = son[x].begin(); it != son[x].end(); ++it) {
		if (it -> first == f || it -> first == DO_NOT_TOUCH) continue;
		int dp = dfs2(it -> first, x, d + it -> second);
		mx = max(mx, dp);
		if (dp == mxdep) // 这条路是直径
			ans = min(ans, max(d, dp - d));
	}
	return mx;
}
int find(int x, int f) {
	DO_NOT_TOUCH = f;
	mxdep = 0; pos = x;
	dfs(x, f);
	int px = pos;
	// printf("pos = %d\n", px);
	mxdep = 0;
	dfs(px, f);
	ans = 1 << 30;
	dfs2(px, f);
	return mxdep ? ans : 0;
}
int main(int argc, char const *argv[]) {
	freopen("city.in", "r", stdin);
	freopen("city.out", "w", stdout);
	int n = read();
	for (int i = 1; i < n; i++) {
		x[i] = read(), y[i] = read(), z[i] = read();
		son[x[i]].push_back((node){y[i], z[i]});
		son[y[i]].push_back((node){x[i], z[i]});
	}
	int res = 1 << 30;
	for (int i = 1; i < n; i++) {
		int ans1 = find(x[i], y[i]);
		int mx1  = mxdep;
		int ans2 = find(y[i], x[i]);
		int mx2  = mxdep;

		// printf("split (%d -> %d), ans1 = %d, ans2 = %d\n", x[i], y[i], ans1, ans2);

		res = min(res, max(max(mx1, mx2), ans1 + ans2 + z[i]));
	}
	printf("%d\n", res);
	// printf("%d\n", clock());
	return 0;
}
/*
让我算一下复杂度...
O(n)枚举删哪条边，每次用 6 遍 dfs 算答案。
感觉会 WA...
让我测一下大数据...
本机开 O2 0.9s ...
不开 4s 多...
感觉要 WA + TLE ...
要是过了我就请 ljz 吃饭（但这是不可能的）。
还有...这中文字体怎么也是 Meiryo 啊...
*/


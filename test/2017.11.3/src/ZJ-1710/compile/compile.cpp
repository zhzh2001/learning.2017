#include <bits/stdc++.h>
#define N 200020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char str[N];
int st[N], cnt, a[10], lft[N], lnt;
void add(int x) {
	for (int i = 0; i < x; i++)
		a[i] ++;
}
void work(int l, int r) {
	int cp = l; cnt = lnt = 0; char lst = '?';
	while (cp >= l && cp <= r) {
		char op = str[cp];
		while (1) {
			if (op == '<') {
				if (!lnt) {
					while (cnt)
						a[st[cnt --] - '0' - 1] ++;
					return;
				}
				int mx = -1;
				while (cnt > lft[lnt]) {
					mx = max(mx, st[cnt] - '0');
					add(st[cnt --] - '0');
				}
				lnt --;
				if (mx & 1) op = '<';
				else op = '>';
				continue;
			} else if (op == '>') {
				if (lst == '>') break;
				lft[++ lnt] = cnt;
				break;
			} else {
				st[++ cnt] = op;
				a[st[cnt] - '0'] ++;
				break;
			}
		}
		lst = op;
		cp ++;
	}
}

/**
 * ASS WE CAN
 *
 * @author SW_Wind
 * @date 11/3/2017
 */

int main(int argc, char const *argv[]) {
	freopen("compile.in", "r", stdin);
	freopen("compile.out", "w", stdout);
	int n = read(), q = read();
	scanf("%s", str + 1);
	while (q --) {
		int l = read(), r = read();
		memset(a, 0, sizeof a);
		work(l, r);
		printf("%d %d %d %d %d %d %d %d %d %d\n",
			a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]);
	}
	return 0;
}
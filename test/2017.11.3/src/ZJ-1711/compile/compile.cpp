#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
void write(ll x){
	if (x<0) x=-x,putchar('-');
	if (x>9) write(x/10); putchar(x%10+'0');
}
void write_(ll x) { write(x); putchar(' '); }
ll n,q,l,r,f,last,now,vis[N],tmp[N],ans[20]; char s[N];
int main(){
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	n=read(); q=read();
	scanf("%s",s+1);
	while (q--){
		l=read(); r=read();
		memset(vis,0,sizeof vis);
		memset(ans,0,sizeof ans);
		now=l; f=1; last=0;
		rep(i,l,r) tmp[i]=s[i]-'0';
		while (1){
			if (now<l||now>r) break;
			if ((s[now]=='<'||s[now]=='>')&&(s[last]=='>'||s[last]=='<')) vis[last]=1;
			if (s[now]=='>') f=1,last=now;
			else if (s[now]=='<') f=-1,last=now;
			else if (s[now]>='0'&&s[now]<='9'){
				++ans[tmp[now]]; --tmp[now];
				if (tmp[now]==-1) vis[now]=1;
				last=now;
			}
			now+=f;
			while (vis[now]){
				now+=f;
				if (now<l||now>r) break;
			}
		}
		rep(i,0,9) write_(ans[i]); puts("");
	}
}

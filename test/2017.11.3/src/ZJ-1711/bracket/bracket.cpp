#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 500005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,top,q[N]; char s[N];
int main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1); n=read();
	while (n--){
		ll l=read(),r=read(),ans=0; q[top=0]=0;
		rep(i,l,r){
			if (s[i]=='(') q[++top]=0;
			else {
				if (s[i]==')'&&!top) { q[top]=0; continue; }
				++q[top-1]; ans+=q[top-1]; --top;
			}
		}
		printf("%lld\n",ans);
	}
}

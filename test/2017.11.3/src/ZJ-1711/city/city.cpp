#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 5005
#define inf (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,tot=1,cnt,flag,tt1,tt2,ttt1,ttt2,ans=inf,head[N],t1[N],t2[N],son[N];
struct edge{ ll to,next,w; }e[N<<1];
struct data{ ll u,v,w; }a[N];
void add(ll u,ll v,ll w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
void dfs(ll u,ll last){
	t1[u]=t2[u]=0;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if ((v==last)||(i==flag)||(i==(flag^1))) continue;
		dfs(v,u); ll w=e[i].w;
		if (t1[v]+w>t1[u]) t2[u]=t1[u],t1[u]=t1[v]+w,son[u]=v;
		else t2[u]=max(t2[u],t1[v]+w);
	}
}
void dfs2(ll u,ll last,ll deep){
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if ((v==last)||(i==flag)||(i==(flag^1))) continue;
		tt1=min(tt1,max(deep,t1[u]));
		ttt1=max(ttt1,max(deep+t1[u],t1[u]+t2[u]));
		if (v==son[u]) dfs2(v,u,deep+t2[u]);
		else dfs2(v,u,deep+t1[u]);
	}
}
void dfs3(ll u,ll last,ll deep){
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if ((v==last)||(i==flag)||(i==(flag^1))) continue;
		tt2=min(tt2,max(deep,t1[u]));
		ttt2=max(ttt2,max(deep+t1[u],t1[u]+t2[u]));
		if (v==son[u]) dfs3(v,u,deep+t2[u]);
		else dfs3(v,u,deep+t1[u]);
	}
}

int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	rep(i,2,n){
		ll u=read(),v=read(),w=read();
		a[++cnt]=(data){u,v,w};
		add(u,v,w); add(v,u,w);
	}
	rep(i,1,n-1){
		flag=2*i; tt1=inf; tt2=inf; ttt1=0; ttt2=0;
		dfs(a[i].u,-1);
		dfs2(a[i].u,-1,0);
		dfs(a[i].v,-1);
		dfs3(a[i].v,-1,0);
		ans=min(ans,max(tt1+tt2+a[i].w,max(ttt1,ttt2)));
	}
	printf("%d",ans);
}

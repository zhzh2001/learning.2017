#include<bits/stdc++.h>
using namespace std;
const int N=2e5+5;
int n,q,l,r;
char ch[N];
int a[11];
int ans[11];
int t[N];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
void solve(int l,int r)
{
	memset(ans,0,sizeof(ans));
	memset(a,0,sizeof(a));
	int k=l,w=1,lw=-1e9,c=-1,mx=0;
	while(l<=k&&k<=r){
		if(t[k]>='0'&&t[k]<='9')a[t[k]-'0']++,t[k]--,mx=max(mx,a[k]);
		if(t[k]=='0'-1)t[k]=-1;
		if(t[k]!=-1)lw++;
		if((t[k]=='<'||t[k]=='>')&&lw==1)t[k-w*c]=-1;
		if(t[k]=='<'){
			if(w==1){
				for(int i=9;i>=0;i--)
					a[i]=a[i+1]+a[i];
				for(int i=0;i<=9;i++)
					ans[i]+=a[i],a[i]=0;
				if(mx%2==0)w=1,t[k]=-1;
				else w=-1,t[k-w*c]=-1;
			}
			else{
				for(int i=0;i<=9;i++)
					ans[i]+=a[i],a[i]=0;
			}
			lw=0,c=1,mx=0;
		}
		if(t[k]=='>'){
			if(w==-1){
				for(int i=9;i>=0;i--)
					a[i]=a[i+1]+a[i];
				for(int i=0;i<=9;i++)
					ans[i]+=a[i],a[i]=0;
				if(mx%2==0)w=-1,t[k]=-1;
				else w=1,t[k-w*c];
			}
			else{
				for(int i=0;i<=9;i++)
					ans[i]+=a[i],a[i]=0;
			}
			lw=0,c=1,mx=0;
		}
		k+=w;c++;
	}
	for(int i=0;i<=9;i++)
		write(ans[i]+a[i]),putchar(' ');
	puts("");
	return;
}
int main()
{
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	scanf("%d%d%s",&n,&q,ch+1);
	for(int i=1;i<=q;i++){
		l=read();r=read();
		for(int j=l;j<=r;j++)
			t[j]=ch[j];
		solve(l,r);
	}
	return 0;
}

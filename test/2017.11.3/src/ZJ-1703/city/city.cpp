#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int head[N],tail[2*N],nxt[2*N],e[2*N];
int mx[N],ex[N],who[N],mf[N];
int n,t,x,y,z;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x*f;
}
void addto(int x,int y,int z)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
	e[t]=z;
}
void dfs1(int k,int fa)
{
	for(int i=head[k];i;i=nxt[i]){
		if(e[i]==-1)continue;
		if(tail[i]==fa)continue;
		dfs1(tail[i],k);
		if(mx[tail[i]]+e[i]>mx[k]){
			ex[k]=mx[k];
			mx[k]=mx[tail[i]]+e[i];
			who[k]=tail[i];
		}
		else if(mx[tail[i]]+e[i]>ex[k])ex[k]=mx[tail[i]]+e[i];
	}
}
void dfs2(int k,int fa)
{
	for(int i=head[k];i;i=nxt[i]){
		if(e[i]==-1)continue;
		if(tail[i]==fa)continue;
		if(who[k]==0)mf[tail[i]]=mf[k]+e[i];
		else if(who[k]!=tail[i])mf[tail[i]]=mx[k]+e[i];
		else mf[tail[i]]=ex[k]+e[i];
		who[tail[i]]=0;
		dfs2(tail[i],k);
	}
}
int solve(int k)
{
	memset(ex,0,sizeof(ex));
	int t=e[2*k];
	e[2*k]=-1;
	e[2*k-1]=-1;
	dfs1(1,0);
	dfs2(1,0);
	int x=-1,ans1=1e9,mt=0;
	for(int i=1;i<=n;i++){
		if(!mx[i]&&!mf[i]){x=i;continue;}
		ans1=min(ans1,max(mx[i],mf[i]));
		mt=max(max(mx[i],mf[i]),mt);
		mx[i]=0,mf[i]=0;
	}
	dfs1(x,0);
	dfs2(x,0);
	int ans2=1e9;
	for(int i=1;i<=n;i++){
		if(!mx[i]&&!mf[i])continue;
		ans2=min(ans2,max(mx[i],mf[i]));
		mt=max(max(mx[i],mf[i]),mt);
		mx[i]=0,mf[i]=0;
	}
	e[2*k]=t;
	e[2*k-1]=t;
	if(ans1==1e9)ans1=0;
	if(ans2==1e9)ans2=0;
	return max(ans1+ans2+t,mt);
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		x=read();y=read();z=read();
		addto(x,y,z);
		addto(y,x,z);
	}
	int ans=1e9;
	for(int i=1;i<n;i++)ans=min(ans,solve(i));
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
bool f[5005][5005];
bool vis[5005][5005];
char a[5005];
map<int,int>an[5005];
int n,q;
bool dfs(int l,int r)
{
	if(l>r)return true;
	if(vis[l][r])return f[l][r];
	vis[l][r]=true;
	if(a[l]=='('&&a[r]==')')f[l][r]|=dfs(l+1,r-1);
	for(int i=l+1;i<r;i++)
		f[l][r]|=dfs(l,i)&dfs(i+1,r);
	return f[l][r];
}
int main()
{
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",a+1);
	n=strlen(a+1);
	dfs(1,n);
	scanf("%d",&q);
	while(q--){
		int l,r;
		l=read();r=read();
		int ans=an[l][r];
		if(!an[l][r])
			for(int i=l;i<=r;i++)
				for(int j=i+1;j<=r;j++)
					if(f[i][j])ans++;
		an[l][r]=ans;
		writeln(ans);
	}
}

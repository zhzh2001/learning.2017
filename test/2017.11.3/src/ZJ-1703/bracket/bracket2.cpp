#include<bits/stdc++.h>
using namespace std;
bool f[5005][5005];
bool vis[5005][5005];
char a[5005];
int ans[5005];
int n,q;
bool dfs(int l,int r)
{
	if(l>r)return true;
	if(vis[l][r])return f[l][r];
	vis[l][r]=true;
	if(a[l]=='('&&a[r]==')')f[l][r]|=dfs(l+1,r-1);
	for(int i=l;i<r;i++)
		f[l][r]|=dfs(l,i)&dfs(i+1,r);
	return f[l][r];
}
int main()
{
	freopen("bracket.in","r",stdin);
//	freopen("bracket2.out","w",stdout);
	scanf("%s",a+1);
	n=strlen(a+1);
	dfs(1,n);
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(f[i][j])ans[j]++;
	for(int i=1;i<=n;i++)
		ans[i]+=ans[i-1],cout<<ans[i]<<' ';
	cout<<endl<<endl;
	scanf("%d",&q);
	while(q--){
		int l,r;
		scanf("%d%d",&l,&r);
		printf("%d\n",ans[r]-ans[l-1]);
	}
}

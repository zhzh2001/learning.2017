#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=5005,inf=1e9;
struct edge{
	int link,next,val;
}e[maxn<<1];
struct E{
	int u,v,w;
}ee[maxn];
int head[maxn],tot,n;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
inline void insert(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
inline void ins(int u,int v,int w){
	insert(u,v,w); insert(v,u,w);
}
inline void init(){
	scanf("%d",&n); 
	for (int i=1;i<n;i++){
		int u=read(),v=read(),w=read();
		ins(u,v,w);
		ee[i]=(E){u,v,w};
	}
}
bool can[maxn<<1];
int dp[maxn][2];
void dfs(int u,int fa){
	dp[u][0]=dp[u][1]=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa&&can[i]){
			dfs(v,u);
			int now=dp[v][0]+e[i].val;
			if (dp[u][0]<=now){
				dp[u][1]=dp[u][0]; dp[u][0]=now;
			}else{
				if (dp[u][1]<now){
					dp[u][1]=now;
				}
			}
		}
	}
}
int ans;
void dfs1(int u,int fa,int sum){
	ans=min(ans,max(dp[u][0],sum));
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa&&can[i]){
			if (dp[u][0]==dp[v][0]+e[i].val) dfs1(v,u,max(sum+e[i].val,dp[u][1]));
				else dfs1(v,u,max(sum+e[i].val,dp[u][0]));
		}
	}
}
inline void solve(){
	memset(can,1,sizeof(can)); int sum=inf;
	for (int i=1;i<n;i++){
		can[2*i-1]=0;
		can[2*i]=0;
		dfs(ee[i].u,0);
		dfs(ee[i].v,0);
		ans=inf;
		dfs1(ee[i].u,0,0);
		int ans1=ans; ans=inf;
		dfs1(ee[i].v,0,0);
		int ans2=ans; ans=inf;
		sum=min(sum,ans1+ans2+ee[i].w);
		can[2*i-1]=1;
		can[2*i]=1;
	}
	printf("%d\n",sum);
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=2e5+5,inf=1e9;
int c[maxn][10],n,q,s[maxn],cnt[maxn][10];
char ss[maxn];
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts(""); 
}
int posl[maxn],posr[maxn],suml[maxn],sumr[maxn];
inline void init(){
	scanf("%d%d",&n,&q);
	scanf("%s",ss+1);
	for (int i=1;i<=n;i++){
		if (ss[i]>='0'&&ss[i]<='9')s[i]=ss[i]-'0';
			else s[i]=-1;
	}
	for (int i=1;i<=n;i++){
		for (int j=0;j<10;j++) cnt[i][j]=cnt[i-1][j],c[i][j]=c[i-1][j];
		if (s[i]==-1) continue;
		for (int j=0;j<=s[i];j++){
			cnt[i][j]++;
		}
		c[i][s[i]]++;
	}
	for (int i=1;i<=n;i++){
		suml[i]=suml[i-1]; sumr[i]=sumr[i-1];
		if (ss[i]=='<') suml[i]++,posl[suml[i]]=i;
		if (ss[i]=='>') sumr[i]++,posr[sumr[i]]=i;
	}
}
struct node{
	int l[10],r[10],ll,rr,ls,rs;
	bool out0,out1;
}a[530000];
inline node pushup(node A,node B,int mid){
	node now;
	memcpy(now.l,A.l,sizeof(A.l));
	now.ll=A.ll; now.ls=A.ls;
	now.rr=B.rr; now.rs=B.rs;
	if (A.rr==inf) now.ll=max(now.ll,B.ll),now.ls+=B.ls;
	if (!B.ll) now.rr=min(now.rr,A.rr),now.rs+=B.rs;
	if (A.out0==0){
		now.out0=0;
	}else{
		for (int i=0;i<10;i++){
			now.l[i]+=B.l[i];
		}
		now.out0=B.out0;
		if (B.out0==0&&A.out1==1){
			int temp=min(B.ls,A.rs);
			int l=posr[sumr[mid]-temp+1],r=posl[suml[mid]+temp-1];
			if (!suml[mid]){
				r=posl[suml[mid]+temp];
			}
			for (int i=0;i<10;i++){
				now.l[i]-=c[r][i]-c[l-1][i];
				now.l[i]+=cnt[r][i]-cnt[l-1][i];
			}
		}	
	}
	memcpy(now.r,B.r,sizeof(B.r));
	if (B.out1==1){
		now.out1=1;
	}else{
		for (int i=0;i<10;i++){
			now.r[i]+=A.r[i];
		}
		now.out1=A.out1;
		if (B.out0==0&&A.out1==1){
			int temp=min(B.ls,A.rs);
			int l=posr[sumr[mid]-temp+1],r=posl[suml[mid]+temp-1];
			if (!suml[mid]){
				r=posl[suml[mid]+temp];
			}
			for (int i=0;i<10;i++){
				now.r[i]-=c[r][i]-c[l-1][i];
				now.r[i]+=cnt[r][i]-cnt[l-1][i];
			}
		}
	}
	return now;
}
void build(int k,int l,int r){
	if (l==r){
		a[k].ll=0;
		a[k].rr=inf;
		if (ss[l]=='<'){
			a[k].out0=0;
			a[k].out1=0;
			a[k].ll=l;
			a[k].ls=1;
		}else{
			if (ss[l]=='>'){
				a[k].out0=1;
				a[k].out1=1;
				a[k].rr=l;
				a[k].rs=1;
			}else{
				int num=s[l];
				a[k].out0=1;
				a[k].out1=0;
				a[k].l[num]++;
				a[k].r[num]++;
			}
		}
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r);
	a[k]=pushup(a[k<<1],a[k<<1|1],mid);
}
node query(int k,int l,int r,int x,int y){
	if (l==x&&r==y){
		return a[k];
	}
	int mid=(l+r)>>1;
	if (mid>=y) return query(k<<1,l,mid,x,y);
		else if (mid<x) return query(k<<1|1,mid+1,r,x,y);
			else return pushup(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y),mid);
}
inline void solve(){
	build(1,1,n);
	for (int i=1;i<=q;i++){
		int x=read(),y=read();
		node ans=query(1,1,n,x,y);
		for (int i=0;i<10;i++) write(ans.l[i]),putchar(' ');
		puts("");
	}
}
int main(){
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
char s[500001];
int sum[5101][5101],q,n,cnt;
inline void pre()
{
	For(i,1,n)
	{
		cnt=0;int tep=i-1;
		while(1)
		{
			tep++;
			if(s[tep]=='(')	cnt++;else	cnt--;
			if(cnt<0)	break;
			tep++;
			if(s[tep]=='(')	cnt++;else	cnt--;
			if(cnt<0)	break;
			if(cnt==0)	sum[i][tep]=sum[i][tep-2]+1;	else	sum[i][tep]=sum[i][tep-2];
		}
		For(j,i,n)	sum[i][j]=max(sum[i][j],sum[i][j-1]);
	}
}
int main()
{
	freopen("bracket.in","r",stdin);freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	if(n>=10000)	return 233;
	pre();
	q=read();
	For(i,1,q)
	{
		int l=read(),r=read(),ans=0;
		For(i,l,r)	ans+=sum[i][r];
		printf("%d\n",ans);
	}
}

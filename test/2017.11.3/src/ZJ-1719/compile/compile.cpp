
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,q,ans[20],a[30001],cnt[30001],tans[20];
char s[30001];
inline void solve(int ql,int qr)
{
	int t1=ql;
	For(i,0,9)	ans[i]=0;
	For(i,ql,qr)	if(isdigit(s[i]))	a[i]=s[i]-'0';else	a[i]=-2;
	int dir=1;
	while(1)
	{
		if(t1<ql||t1>qr)	break;
		if(a[t1]==-3)	{t1+=dir;continue;}
		if(a[t1]>=0)
		{
			ans[a[t1]]++;
			a[t1]--;
			if(a[t1]==-1)	a[t1]=-3;
			t1+=dir;continue;
		}

		if(a[t1]==-2)	
		{	
			if(s[t1]=='>')	dir=1;else	dir=-1;
			int t2=t1;
			while(1){if(t2<ql||t2>qr)	break;t2+=dir;if(a[t2]!=-3)	break;}
			if(a[t2]==-2)	a[t1]=-3;
			t1=t2;
		}
	}
	For(i,0,9)	printf("%d ",ans[i]);puts("");
}
int main()
{
	freopen("compile.in","r",stdin);freopen("compile.out","w",stdout);
	n=read();q=read();
	scanf("\n%s",s+1);
	For(i,1,q)	
	{
		int l=read(),r=read();
		solve(l,r);
	}
}


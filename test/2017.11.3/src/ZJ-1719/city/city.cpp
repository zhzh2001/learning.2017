#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<ctime>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int cnt=1;
int poi[20001],nxt[20001],v[20001],head[20001],q2[20001],q1[20001],f[20001][3];
int bj[20001],x[20001],y[20001],z[20001],ans,top1,top2,to[20001][3];
inline void add(int x,int y,int z){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;v[cnt]=z;}
int n;
inline void dfs1(int x,int fa,int kd)
{
	if(kd==1)	q1[++top1]=x;	else	q2[++top2]=x;
	for(int i=head[x];i;i=nxt[i])
	{
		if(bj[i])	continue;
		if(poi[i]==fa)	continue;
		dfs1(poi[i],x,kd);
		if(v[i]+f[poi[i]][1]>f[x][1])	to[x][2]=to[x][1],f[x][2]=f[x][1],to[x][1]=poi[i],f[x][1]=v[i]+f[poi[i]][1];
			else	if(v[i]+f[poi[i]][1]>f[x][2])	to[x][2]=poi[i],f[x][2]=f[poi[i]][1]+v[i];
	}
}
inline void dfs2(int x,int fa)
{
	for(int i=head[x];i;i=nxt[i])
	{
		if(bj[i])	continue;
		if(poi[i]==fa)
		{
			int tmp=v[i]+f[fa][1];
			if(to[fa][1]==x)	tmp=v[i]+f[fa][2];
			if(tmp>f[x][1])	to[x][2]=to[x][1],f[x][2]=f[x][1],to[x][1]=poi[i],f[x][1]=tmp;
			else	if(tmp>f[x][2])	to[x][2]=poi[i],f[x][2]=tmp;
			break;
		}
	}	
	ans=max(ans,f[x][1]+f[x][2]);
	for(int i=head[x];i;i=nxt[i])
	{
		if(bj[i])	continue;
		if(poi[i]==fa)	continue;
		dfs2(poi[i],x);
	}
}
int ANS=1e9;
inline void solve(int x,int y,int num)
{
	bj[num]=bj[num^1]=1;
	ans=0;top1=top2=0;
	For(i,1,n)	f[i][1]=f[i][2]=to[i][1]=to[i][2]=0;
	dfs1(x,x,1);dfs2(x,x);
	dfs1(y,y,2);dfs2(y,y);
	bj[num]=bj[num^1]=0;
	int mi1=1e9,mi2=1e9;
	For(i,1,top1)	mi1=min(mi1,f[q1[i]][1]);
	For(i,1,top2)	mi2=min(mi2,f[q2[i]][1]);
	ans=max(ans,mi1+mi2+v[num]);
	ANS=min(ANS,ans);
	if(n*(num/2)>=20000000)	{cout<<ANS<<endl;exit(0);}
}
int main()
{
	freopen("city.in","r",stdin);freopen("city.out","w",stdout);
	n=read();
	For(i,1,n-1)	x[i]=read(),y[i]=read(),z[i]=read(),add(x[i],y[i],z[i]),add(y[i],x[i],z[i]);
	For(i,1,n-1)
			solve(x[i],y[i],2*i);
	cout<<ANS<<endl;
}

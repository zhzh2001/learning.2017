#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=5e3+5;
struct cs{int to,nxt,v;}a[N*2];
int head[N],ll;
int fa[N],v1[N],v2[N],mm[2],o;
int n,m,x,y,z,ans,E,S,sum;
void init(int x,int y,int z){
	a[++ll].to=y;
	a[ll].v=z;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs1(int x,int y){
	int m1=0,m2=0;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y){
			dfs1(a[k].to,x);
			v1[a[k].to]+=a[k].v;
			if(!m1)m1=a[k].to;else
			if(!m2)m2=a[k].to;else
				if(v1[a[k].to]>v1[m1])m2=m1,m1=a[k].to;else
				if(v2[a[k].to]>v1[m2])m2=a[k].to;
		}
	v1[x]=v1[m1];v2[x]=v2[m1];
	if(!v2[x])v2[x]=x;
	if(!m1&&!m2)return;
	if(v1[m1]+v1[m2]>ans){
		ans=v1[m1]+v1[m2],S=v2[m1],E=v2[m2];
		if(!E)E=x;
	}
}
void dfs2(int x,int y){
	fa[x]=y;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y)dfs2(a[k].to,x);
}
void dfs3(int x,int y){
	v1[x]=0;v2[x]=0;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y){
			dfs3(a[k].to,x);
			int v=v1[a[k].to]+a[k].v;
			if(v>v1[x])v2[x]=v1[x],v1[x]=v;else
			if(v>v2[x])v2[x]=v;
		}
	sum=max(sum,v1[x]+v2[x]);
}
void dfs4(int x,int y,int v){
	if(v>v1[x])v2[x]=v1[x],v1[x]=v;else
	if(v>v2[x])v2[x]=v;
	mm[o]=min(mm[o],v1[x]);
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y)
			if(v1[a[k].to]+a[k].v==v1[x])
				dfs4(a[k].to,x,v2[x]+a[k].v);else
				dfs4(a[k].to,x,v1[x]+a[k].v);
}
void find(int S,int E){
	int v;
	for(int k=head[S];k;k=a[k].nxt)
		if(a[k].to==E)v=a[k].v;
	sum=0;
	dfs3(S,E);
	o=0;mm[0]=1e9;
	dfs4(S,E,0);
	dfs3(E,S);
	o=1;mm[1]=1e9;
	dfs4(E,S,0);
	sum=max(sum,mm[0]+mm[1]+v);
	ans=min(ans,sum);
}
int main()
{
//	ios::sync_with_stdio(0);
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	
	scanf("%d",&n);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		init(x,y,z);
		init(y,x,z);
	}
	ans=-1;
	dfs1(1,-1);
	dfs2(S,-1);
	ans=1e9;
	while(E!=S){
		find(E,fa[E]);
		E=fa[E];
	}printf("%d",ans);
}



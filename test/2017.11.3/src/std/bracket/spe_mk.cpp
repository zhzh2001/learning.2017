#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
int n,m,tot,i;
int ran(){return (rand()<<15)+rand();}
int main(){
	freopen("bracket.in","w",stdout);
	srand(time(0));rand();
	n=2e5;m=2e5;
	for (i=n/2;i--;){
		putchar('(');
		putchar(')');
	}
	puts("");
	printf("%d\n",m);
	for (;m--;){
		int l=ran()%n+1,r=ran()%n+1;
		if (l>r) swap(l,r);
		printf("%d %d\n",l,r);
	}
}

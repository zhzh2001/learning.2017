#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=4e5+5;
int n,m,t,q[N],pl[N],pr[N],i,tot,mx,lz[N],rz[N];
char a[N];
int bl[N],fa[N],top[N],deep[N];
int cl[N],cr[N];ll f[N],ff[N],fl[N],fr[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
ll cal(int x){return (ll)x*(x-1)/2;}
void dfs(int l,int r,int d){
	bl[l]=bl[r]=++m;lz[m]=l;rz[m]=r;
	int p=m;deep[m]=d;
	if (l+1==r){f[m]=1;return;}
	if (pr[l+1]==r-1){
		top[m+1]=top[p];fa[m+1]=p;dfs(l+1,r-1,d);
		ff[p]=f[p]=f[bl[l+1]];f[p]++;
		return;
	}
	for (int i=l+1,j=0;i<r;i=pr[i]+1,j++)
		top[m+1]=m+1,dfs(i,pr[i],d+1),fa[bl[i]]=p,
		cl[bl[i]]=j,fl[bl[i]]=f[p],f[p]+=f[bl[i]];
	ll k=0;int j=0;
	for (int i=r-1;i>l;k+=f[bl[i]],i=pl[i]-1,j++) 
		cr[bl[i]]=j,fr[bl[i]]=k;
	ff[p]=f[p];f[p]+=cal(j)+1;
}
ll ask(){
	int x=read()-1,y=read()+1;ll ans=0;
	if (a[x]=='('&&y>pr[x]) ans+=f[bl[x]]-1;
	if (a[y]==')'&&x<=pl[y]) ans+=f[bl[y]]-1;
	for (x=top[bl[x]],y=top[bl[y]];fa[x]!=fa[y];tot++)
		if (deep[x]>deep[y])
			 ans+=fr[x]+cal(cr[x]),x=top[fa[x]];
		else ans+=fl[y]+cal(cl[y]),y=top[fa[y]];
	if (x==y) return ans;
	return ans+(fr[x]+fl[y]-ff[fa[x]])+cal(cl[y]-cl[x]-1);
}
int main(){
	freopen("bracket.in","r",stdin);
	scanf("%s",a+1);n=strlen(a+1);
	a[0]='(';a[++n]=')';
	for (i=0;i<=n;i++) if (a[i]=='(')
		q[++t]=i; else pl[i]=q[t],pr[q[t--]]=i;
	top[1]=1;dfs(0,n,1);
	for (i=1;i<=m;i++){
		int cnt=0;
		for (int j=i;j!=1;j=fa[top[j]]) cnt++;
		if (cnt>mx) mx=cnt;
	}
	printf("%d\n",mx);
}

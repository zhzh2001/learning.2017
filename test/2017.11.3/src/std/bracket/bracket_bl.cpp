#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=4e5+5;
int n,m,t,q[N],pl[N],pr[N],i;
char a[N];
int bl[N],fa[N],top[N],deep[N];
int cl[N],cr[N];ll f[N],ff[N],fl[N],fr[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
ll cal(int x){return (ll)x*(x-1)/2;}
void dfs(int l,int r,int tp,int d){
	bl[l]=bl[r]=++m;int p=m;
	top[m]=tp;deep[m]=d;
	if (l+1==r){f[m]=1;return;} 
	if (pr[l+1]==r-1){
		dfs(l+1,r-1,tp,d);
		ff[p]=f[p]=f[bl[l+1]];f[p]++;
		return;
	}
	for (int i=l+1,j=0;i<r;i=pr[i]+1,j++)
		dfs(i,pr[i],m+1,d+1),fa[bl[i]]=p,
		cl[bl[i]]=j,fl[bl[i]]=f[p],f[p]+=f[bl[i]];
	ll k=0;int j=0;
	for (int i=r-1;i>l;k+=f[bl[i]],i=pl[i]-1,j++) 
		cr[bl[i]]=j,fr[bl[i]]=k;
	ff[p]=f[p];f[p]+=cal(j)+1;
}
ll ask(){
	int x=read()-1,y=read()+1;ll ans=0;
	if (a[x]=='('&&y>pr[x]) ans+=f[bl[x]]-1;
	if (a[y]==')'&&x<=pl[y]) ans+=f[bl[y]]-1;
	for (x=top[bl[x]],y=top[bl[y]];fa[x]!=fa[y];)
		if (deep[x]>deep[y])
			 ans+=fr[x]+cal(cr[x]),x=top[fa[x]];
		else ans+=fl[y]+cal(cl[y]),y=top[fa[y]];
	if (x==y) return ans;
	return ans+(fr[x]+fl[y]-ff[fa[x]])+cal(cl[y]-cl[x]-1);
}
int main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",a+1);n=strlen(a+1);
	a[0]='(';a[++n]=')';
	for (i=0;i<=n;i++) if (a[i]=='(')
		q[++t]=i; else pl[i]=q[t],pr[q[t--]]=i;
	dfs(0,n,1,1);
	for (int Que=read();Que--;)
		write(ask()),putchar('\n');
}

#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int P=150;
int n,m,tot,i;
int ran(){return (rand()<<15)+rand();}
int main(){
	freopen("bracket.in","w",stdout);
	srand(time(0));rand();
	n=ran()%(P+1)+P;
	m=ran()%(P+1)+P;
	for (i=1;i<=n;i++){
		if (rand()%2){
			putchar('(');
		}
		else{
			putchar(')');
		}
	}
	puts("");
	printf("%d\n",m);
	for (;m--;){
		int l=ran()%n+1,r=ran()%n+1;
		if (l>r) swap(l,r);
		printf("%d %d\n",l,r);
	}
}

#include <cstdio>
const int N = 2e5 + 5, inf = 1e8;
int n, m, Q, i, j, x, px, y, d, lx[N], rx[N], lt[N], rt[N];
int now[10], f[N][10], g[N][10];
char a[N];
int read()
{
	char c = getchar();
	int k = 0;
	for (; c < 48 || c > 57; c = getchar())
		;
	for (; c > 47 && c < 58; c = getchar())
		k = (k << 3) + (k << 1) + c - 48;
	return k;
}
void write(int x)
{
	if (x > 9)
		write(x / 10);
	putchar(x % 10 + 48);
}
void del(int x)
{
	rx[lx[x]] = rx[x];
	lx[rx[x]] = lx[x];
}
int main()
{
	freopen("compile.in", "r", stdin);
	freopen("compile.out", "w", stdout);
	n = read();
	Q = read();
	scanf("%s", a + 1);
	n++;
	for (i = 1; i <= n; i++)
		lx[i] = i - 1, rx[i] = i + 1, lt[i] = rt[i] = inf;
	for (i = 1; i <= n; i++)
		if (lt[i] == inf)
		{
			for (x = i, d = 0; i <= x && x <= n; x = y)
			{
				if (lt[x] == inf)
					for (lt[x] = ++m, j = 0; j < 10; j++)
						f[x][j] = now[j];
				if (a[x] < 58)
					if (a[x] > 47)
						now[(a[x]--) - 48]++, px = 0;
					else
						del(x);
				else
				{
					if (px)
						a[px] = 47;
					d = a[x] == '<';
					px = x;
				}
				y = d ? lx[x] : rx[x];
				if (d && rt[x] == inf)
					for (rt[x] = m, j = 0; j < 10; j++)
						g[x][j] = now[j];
			}
		}
	for (; Q--; putchar('\n'))
	{
		int l = read(), r = read();
		if (lt[r + 1] <= rt[l])
			for (i = 0; i < 10; i++)
				write(f[r + 1][i] - f[l][i]), putchar(' ');
		else
			for (i = 0; i < 10; i++)
				write(g[l][i] - f[l][i]), putchar(' ');
	}
}

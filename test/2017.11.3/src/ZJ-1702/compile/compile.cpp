#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}	

const int N = 200005;
char s[N];
char p[N];
bool vis[N];
int f,x,last,n,m;
int num[10];

int main(){
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	scanf("%d %d",&n,&m);
	scanf("%s",s+1);
	while(m--){
		int l = read(),r = read();
		memcpy(p,s,sizeof s);
		memset(vis,false,sizeof vis);
		memset(num,0,sizeof num);
		x = l; f = 1; last = 0;
		while(x >= l && x <= r){
			if(vis[x]){
				if(f) x++;
				else x--;
				continue;
			}
			if(p[x] >= '0' && p[x] <= '9'){
				num[p[x]-'0']++;
				if(p[x] == '0') vis[x] = true;
				else p[x]--;
				last = x;
				if(f) x++;
				else x--;
			}
			else{
				if(p[last] == '<' || p[last] == '>')
					vis[last] = true;
				if(p[x] == '<') f = 0;
				else f = 1;
				last = x;
				if(f) x++;
				else x--;
			}
		}
		for(int i = 0;i < 9;i++)
			printf("%d ",num[i]);
		printf("%d\n",num[9]);
	}
	return 0;
}

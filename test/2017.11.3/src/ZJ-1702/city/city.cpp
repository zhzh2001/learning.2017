#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 5005;
int head[N],nxt[N*2],to[N*2],w[N*2],cnt = 1;
bool del[N*2];
int Mx1[N],Mx2[N];
int Mxx1,Mxx2,Len1,Len2;
int dele;
int n;
int ans = 1e9;

inline void add(int x,int y,int c){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; w[cnt] = c;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt; w[cnt] = c;
}

inline int max(int x,int y){
    return x > y ? x : y;
}
inline int min(int x,int y){
    return x < y ? x : y;
}

void dfs1(int x,int f){
	for(int i = head[x];i;i = nxt[i])
		if(!del[i] && to[i] != f){
			int v = to[i];
			dfs1(v,x);
			if(Mx1[v]+w[i] > Mx1[x]){
				Mx2[x] = Mx1[x];
				Mx1[x] = Mx1[v]+w[i];
			}
			else if(Mx1[v]+w[i] > Mx2[x])
				Mx2[x] = Mx1[v]+w[i];
		}
}

void dp1(int x,int f,int zuid){
	for(int i = head[x];i;i = nxt[i])
		if(!del[i] && to[i] != f){
			if(Mx1[to[i]]+w[i] == Mx1[x])
				dp1(to[i],x,max(zuid,Mx2[x])+w[i]);
			else dp1(to[i],x,max(zuid,Mx1[x])+w[i]);
		}
	int a1 = Mx1[x],a2 = Mx2[x];
	if(zuid > a1){
		a2 = a1;
		a1 = zuid;
	}
	else if(zuid > a2)
		a2 = zuid;
	Len1 = max(Len1,a1+a2);
	Mxx1 = min(Mxx1,max(a1,a2));
}

void dp2(int x,int f,int zuid){
	for(int i = head[x];i;i = nxt[i])
		if(!del[i] && to[i] != f){
			if(Mx1[to[i]]+w[i] == Mx1[x])
				dp2(to[i],x,max(zuid,Mx2[x])+w[i]);
			else dp2(to[i],x,max(zuid,Mx1[x])+w[i]);
		}
	int a1 = Mx1[x],a2 = Mx2[x];
	if(zuid > a1){
		a2 = a1;
		a1 = zuid;
	}
	else if(zuid > a2)
		a2 = zuid;
	Len2 = max(Len2,a1+a2);
	Mxx2 = min(Mxx2,max(a1,a2));
}

int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout); 
	n = read();
	for(int i = 1;i < n;i++){
		int x = read(),y = read(),c = read();
		add(x,y,c);
	}
	for(int i = 2;i <= cnt;i += 2){
		del[i] = true; del[i^1] = true;
		Mxx1 = 1e9; Mxx2 = 1e9;
		Len1 = 0; Len2 = 0;
//		memset(dep,0,sizeof dep);
		memset(Mx1,0,sizeof Mx1);
		memset(Mx2,0,sizeof Mx2);
		dfs1(to[i],0);
		dfs1(to[i^1],0);
		dp1(to[i],0,0);
		dp2(to[i^1],0,0);
//		printf("%d %d %d %d %d\n",Len1,Len2,Mxx1,Mxx2,w[i]);
		Len1 = max(Len1,Len2);
		ans = min(ans,max(Len1,Mxx1+Mxx2+w[i]));
		del[i] = false; del[i^1] = false;
	}
	printf("%d\n",ans);
	return 0;
}

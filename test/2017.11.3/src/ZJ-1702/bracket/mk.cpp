#include<ctime>
#include<cmath>
#include<cstdio>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const ll L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

char s[1000];
int n = 100;

signed main(){
	freopen("bracket.in")
	srand(time(0));
	for(int i = 1;i <= n;i++){
		int x = rand()%2;
		if(x) s[i] = '(';
		else s[i] = ')';
	}
	int m = 100;
	s[n+1] = '\0';
	printf("%s\n",s+1);
	printf("%d\n",m);
	for(int i = 1;i <= 100;i++){
		int l = rand()%n+1,r = rand()%n+1;
		if(l > r) swap(l,r);
		printf("%d %d\n",l,r);
	}
	return 0;
}

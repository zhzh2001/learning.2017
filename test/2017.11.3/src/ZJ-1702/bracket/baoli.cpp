#include<cmath>
#include<cstdio>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const ll L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const ll N = 500005;

ll n,m;
char s[N];

pair<ll,ll> solve(ll x,ll y){
	ll ans = 0;
	ll sum = 0;
	for(ll i = x;i <= y;i++){
		if(s[i] == '('){
			pair<ll,ll> tmp = solve(i+1,y);
			ans += sum;
			sum++; ans += tmp.first;
			i = tmp.second;
		}
		else
			return make_pair(ans+1,i);
	}
	return make_pair(ans,y);
}

signed main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket1.out","w",stdout);
	scanf("%s",s+1);
	n = strlen(s+1);
	m = read();
	for(ll i = 1;i <= m;i++){
		ll x = read(),y = read();
		ll ans = 0;
		ll sum = 0;
		for(ll i = x;i <= y;i++){
			if(s[i] == '('){
				pair<ll,ll> tmp = solve(i+1,y);
				ans += sum;
				sum++; ans += tmp.first;
				i = tmp.second;
			}
			else sum = 0;
		}
		printf("%lld\n",ans);
	}
	return 0;
}


#include<cmath>
#include<cstdio>
#include<vector>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const ll L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const ll N = 500005;
/*
ll n,m;
char s[N];

pair<ll,ll> solve(ll x,ll y){
	ll ans = 0;
	ll sum = 0;
	for(ll i = x;i <= y;i++){
		if(s[i] == '('){
			pair<ll,ll> tmp = solve(i+1,y);
			ans += sum;
			sum++; ans += tmp.first;
			i = tmp.second;
		}
		else
			return make_pair(ans+1,i);
	}
	return make_pair(ans,y);
}

signed main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	n = strlen(s+1);
	m = read();
	for(ll i = 1;i <= m;i++){
		ll x = read(),y = read();
		ll ans = 0;
		ll sum = 0;
		for(ll i = x;i <= y;i++){
			if(s[i] == '('){
				pair<ll,ll> tmp = solve(i+1,y);
				ans += sum;
				sum++; ans += tmp.first;
				i = tmp.second;
			}
			else sum = 0;
		}
		printf("%lld\n",ans);
	}
	return 0;
}
*/

ll n,m;
struct node{
	ll l,r,num,sum;
	bool vis;
}bark[N];
ll tot;
vector<ll> link[N];
vector<ll> b[N];
char s[N];
bool flag;

ll erfen(ll x,ll p){
	ll l = 0,r = link[p].size()-1,mid,ans = -1;
	while(l <= r){
		mid = (l+r)>>1;
		if(link[p][mid] <= x){
			l = mid+1;
			ans = mid;
		}
		else r = mid-1;
	}
	return ans;
}

pair<ll,ll> solve(ll x,ll y){
	ll ans = 0,sum = 0;
	ll t_tot = ++tot;
	for(ll i = x;i <= y;i++){
		if(s[i] == '('){
			pair<ll,ll> tmp = solve(i+1,y);
			bark[i].vis = flag;
			if(flag){
				bark[i].l = i;
				bark[i].r = i;
				t_tot = ++tot;
				continue;
			}
			bark[i].num = t_tot;
			bark[i].sum = tmp.first;
			ans += tmp.first;
			ans += sum; sum++;
			link[t_tot].push_back(tmp.second);
			if(b[t_tot].size() == 0) b[t_tot].push_back(tmp.first);
			else b[t_tot].push_back(tmp.first+b[t_tot][b[t_tot].size()-1]);
			i = tmp.second;
		}
		else{
			bark[i].vis = true;
			bark[i].l = i; bark[i].r = i;
			flag = false;
			return make_pair(ans+1,i);
		}
	}
	flag = true;
	return make_pair(ans,bark[x].r);
}

signed main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	n = strlen(s+1);
	m = read();
	tot++;
	ll t_tot = tot;
	for(ll i = 1;i <= n;i++){
		if(s[i] == '('){
			pair<ll,ll> tmp = solve(i+1,n);
			bark[i].vis = flag;
			if(flag){
				bark[i].l = i;
				bark[i].r = i;
				t_tot = ++tot;
				continue;
			}
			bark[i].l = i;
			bark[i].r = tmp.second;
			bark[i].num = t_tot;
			bark[i].sum = tmp.first;
			link[t_tot].push_back(tmp.second);
			if(b[t_tot].size() == 0) b[t_tot].push_back(tmp.first);
			else b[t_tot].push_back(tmp.first+b[t_tot][b[t_tot].size()-1]);
			i = tmp.second;
		}
		else{
			t_tot = ++tot;
			bark[i].l = i;
			bark[i].r = i;
			bark[i].vis = true;
		}
	}
	for(ll i = n-1;i;i--)
		if(bark[i].vis)
			if(bark[i+1].vis)
				bark[i].r = bark[i+1].r;
	ll ans = 0;
	while(m--){
		ll l = read(),r = read();
		ans = 0;
		while(l <= r){
			if(bark[l].vis){
				l = bark[l].r+1;
				continue;
			}
			ll p = bark[l].num;
			ll r1 = erfen(r,p),l1 = erfen(bark[l].r,p);
			if(l1 > r1){
				l++;
				continue;
			}
			else{
				ans += (r1-l1+1)*(r1-l1)/2;
				if(l1 == 0) ans += b[p][r1];
				else ans += b[p][r1]-b[p][l1-1];
			}
			l = link[p][r1]+1;
		}
		printf("%lld\n",ans);
	}
	return 0;
}

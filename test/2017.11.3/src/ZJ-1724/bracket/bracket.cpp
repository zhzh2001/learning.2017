#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 500003

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
char ch,s[N];
int n,m,L,R,c,a[N];
ll ans;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	rep(i,1,n)if(s[i]=='(')a[i]=1;else a[i]=-1;
	read(m);
	while(m--)
	{
		read(L),read(R);
		ans=0;
		rep(i,L,R)
		{
			c=0;
			rep(j,i,R)
			{
				c+=a[j];
				ans+=(c==0);
				if(c<0)break;
			}
		}
		cout<<ans<<endl;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

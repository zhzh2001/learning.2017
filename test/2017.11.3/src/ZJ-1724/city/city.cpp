#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 5002
#define E 10004

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
struct Edge{int y,d,t;}e[E];
bool vis[N];
char ch;
int n,x,y,z,h[N],ep=1,tmp,p,rtn,ans=INF;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}
inline void addedge(const int &x,const int &y,const int &z){e[++ep].y=y;e[ep].d=z;e[ep].t=h[x];h[x]=ep;}

int check(const int &x)
{
	vis[x]=true;
	int rtn=1;
	rgp(i,x)if(!vis[e[i].y])rtn+=check(e[i].y);
	return rtn;
}

void dfs(const int &x,const int &w)
{
	vis[x]=true;
	if(w>rtn)
	{
		rtn=w;
		p=x;
	}
	rgp(i,x)if(!vis[e[i].y])dfs(e[i].y,w+e[i].d);
}

int main(void)
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	read(n);
	rsp(i,1,n)read(x),read(y),read(z),addedge(x,y,z),addedge(y,x,z);
	rep(i,1,n)
	{
		rgp(j,i)
		{
			tmp=e[j].y;
			e[j^1].y=e[j].y;
			e[ep+1].y=i;
			e[ep+1].d=e[j].d;
			rep(k,1,n)
			{
				e[j].y=k;
				e[ep+1].t=h[k];
				h[k]=ep+1;
				mst(vis,0);
				if(check(1)!=n)
				{
					h[k]=e[ep+1].t;
					continue;
				}
				/*Dw("----------\n");
				rep(i,1,n)
				{
					Dw("%d:",i);
					rgp(j,i)Dw("%d[%d] ",e[j].y,e[j].d);
					Dp;
				}*/
				mst(vis,0);
				rtn=0;
				dfs(1,0);
				mst(vis,0);
				rtn=0;
				dfs(p,0);
				ans=min(ans,rtn);
				h[k]=e[ep+1].t;
			}
			e[j].y=tmp;
			e[j^1].y=i;
		}
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

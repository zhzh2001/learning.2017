#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 200003

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
bool d;
char ch,s[N],t[N];
int n,m,L,R,c[10],w,l,next[N][2];

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

inline void sim(void)
{
	d=true;
	w=L;
	rep(i,1,n)t[i]=s[i];
	mst(c,0);
	rep(i,1,n)next[i][0]=i-1,next[i][1]=i+1;
	while(L<=w&&w<=R)
	{
		if(t[w]=='>')
		{
			d=true;
			if(t[l]=='>'||t[l]=='<')next[next[l][1]][0]=next[l][0],next[next[l][0]][1]=next[l][1];
		}
		else if(t[w]=='<')
		{
			d=false;
			if(t[l]=='>'||t[l]=='<')next[next[l][1]][0]=next[l][0],next[next[l][0]][1]=next[l][1];
		}
		else
		{
			c[t[w]-'0']++;
			if(t[w]=='0')next[next[w][1]][0]=next[w][0],next[next[w][0]][1]=next[w][1];
			else t[w]--;
		}
		l=w;
		w=next[w][d];
	}
}

int main(void)
{
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	scanf("%d%d\n%s",&n,&m,s+1);
	while(m--)
	{
		read(L),read(R);
		sim();
		rp(i,9)printf("%d ",c[i]);printf("%d\n",c[9]);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

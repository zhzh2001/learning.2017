#include<bits/stdc++.h>
using namespace std;
char s[5005];
int q,x,y,ans[5005][5005],cnt;
int main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	int n=strlen(s+1);
	for (int i=1;i<=n;i++){
		cnt=0;
		for (int j=i;j<=n;j++){
			cnt+=(s[j]=='('?1:-1);
			if (cnt<0) break;
			ans[i][j]+=(cnt==0);
		}
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			ans[i][j]+=ans[i][j-1];
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			ans[i][j]+=ans[i-1][j];
	scanf("%d",&q);
	while (q--){
		scanf("%d%d",&x,&y);
		printf("%d\n",ans[y][y]-ans[y][x-1]-ans[x-1][y]+ans[x-1][x-1]);
	}
}

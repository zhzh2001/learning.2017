#include<bits/stdc++.h>
using namespace std;
namespace FastIO{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	/*inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}*/
	#define gc() getchar()
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(int x){
		if (!x){
			puts("0");
			return;
		}
		if (x<0){
			putchar('-');
			x=-x;
		}
		static int a[20],top;
		for (;x;a[++top]=x%10,x/=10);
		for (;top;putchar('0'+a[top--]));
		puts("");
	}
}
using namespace FastIO;
const int N=5005;
struct edge{int from,to,next,v;}e[N*2];
int n,x,y,z,ban,tot=1,ans=1e9,X,Y;
int head[N],q[N],d[N],dl[N],dr[N];
void add(int x,int y,int z){
	e[++tot]=(edge){x,y,head[x],z};
	head[x]=tot;
}
void spfa(int s,int *d){
	for (int i=1;i<=n;i++) d[i]=1e9;
	q[1]=s; d[s]=0;
	for (int h=0,t=1;h!=t;)
		for (int x=q[++h],i=head[x];i;i=e[i].next)
			if (d[e[i].to]>=1e9&&i!=ban&&i!=(ban^1))
				d[e[i].to]=d[x]+e[i].v,q[++t]=e[i].to;
}
int getmax(int *a){
	int mx=0; a[0]=-1;
	for (int i=1;i<=n;i++)
		if (a[i]>a[mx]&&a[i]<1e8) mx=i;
	return mx;
}
void dia(int fr,int &x,int &y){
	spfa(fr,d);
	int l=getmax(d);
	spfa(l,dl);
	int r=getmax(dl);
	spfa(r,dr);
	int mx=1,v=max(dl[1],dr[1]);
	//for (int i=1;i<=n;i++)
	//	printf("%d %d\n",dl[i],dr[i]);
	//printf("%d %d %d\n",fr,l,r);
	for (int i=2;i<=n;i++){
		int tmp=max(dl[i],dr[i]);
		if (tmp<v) v=tmp,mx=i;
	}
	x=v;
	y=min(dl[mx],dr[mx]);
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	for (int i=1;i<n;i++){
		x=read(); y=read(); z=read();
		add(x,y,z); add(y,x,z);
	}
	for (ban=2;ban<=tot;ban+=2){
		dia(e[ban].from,x,y);
		dia(e[ban].to,X,Y);
		ans=min(ans,max(x+X+e[ban].v,max(x+y,X+Y)));
	}
	write(ans);
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
namespace FastIO{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	/*inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}*/
	#define gc() getchar()
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void readstr(char *s,int &len){
		char ch=gc();
		for (;ch!='('&&ch!=')';ch=gc());
		len=0;
		for (;ch=='('||ch==')';ch=gc())
			s[++len]=ch;
	}
	inline void write(int x){
		if (!x){
			puts("0");
			return;
		}
		if (x<0){
			putchar('-');
			x=-x;
		}
		static int a[20],top;
		for (;x;a[++top]=x%10,x/=10);
		for (;top;putchar('0'+a[top--]));
		puts("");
	}
}
using namespace FastIO;
const int N=750505;
vector<int> vec[N],e[N];
ll lv[N],rv[N],sv[N];
int id[N],dep[N],fa[N][20];
int n,size,x,y,D,top,q,sta[N];
char s[N];
ll S(int x){
	return x*(x+1)/2;
}
void dfs1(int x){
	int sz=e[x].size();
	ll la=lv[x];
	for (int i=0;i<sz;i++){
		lv[e[x][i]]=la;
		fa[e[x][i]][0]=x;
		dep[e[x][i]]=dep[x]+1;
		id[e[x][i]]=i;
		dfs1(e[x][i]);
		la+=max(0,i-1);
		sv[x]+=sv[e[x][i]];
	}
	sv[x]+=S(max(sz-2,0));
}
void dfs2(int x){
	int sz=e[x].size();
	ll la=rv[x];
	for (int i=sz-1;i>=0;i--){
		rv[e[x][i]]=la;
		dfs2(e[x][i]);
		la+=(sz-1-i);
	}
}
int kthfa(int x,int k){
	if (k<=0) return x;
	for (int i=0;i<=19;i++,k/=2)
		if (k&1) x=fa[x][i];
	return x;
}
int lca(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	x=kthfa(x,dep[x]-dep[y]);
	for (int i=19;i>=0;i--)
		if (fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0];
}
int main(){
	readstr(s+1,n);
	s[1]='('; s[n+2]=')';
	size=n+2;
	for (int i=1;i<=n+2;i++)
		if (s[i]=='('){
			D++;
			int now=++size;
			sta[++top]=now;
			e[now].push_back(i);
		}
		else{
			int now=sta[top--];
			for (int j=0;j<vec[D+1].size();j++)
				e[now].push_back(vec[D+1][j]);
			vec[D+1].clear();
			vec[D].push_back(now);
			e[now].push_back(i);
			D--;
		}
	dfs1(n+3);
	dfs2(n+3);
	for (int i=1;i<=size;i++)
		printf("%lld %lld %lld\n",lv[i],rv[i],sv[i]);
	for (int i=1;i<=19;i++)
		for (int j=1;j<=size;j++)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	q=read();
	while (q--){
		x=read(); y=read(); y+=2;
		int d=lca(x,y);
		ll ans=lv[y]+rv[x]-lv[d]-rv[d];
		printf("%lld\n",ans);
		x=kthfa(x,dep[x]-dep[d]-1);
		y=kthfa(y,dep[y]-dep[d]-1);
		write(ans+S(id[y]-id[x]-1));
	}
}

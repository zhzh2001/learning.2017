#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<iostream>
#include<stdlib.h>
#include<map>
#include<set>
#include<vector>
#include<bitset>
#define ll long long
#define N 200005
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
char s[N];
int top=1,n,m,a[N],sum[N][10];
int R[N],l[N],LL[N];
struct tree{int mx,tag;}t[N<<2];
inline void pushup(int k){
	t[k].mx=max(t[k<<1].mx,t[k<<1|1].mx);
}
inline void pushdown(int k,int l,int r){
	int tag=t[k].tag;t[k].tag=-1;
	if(tag==-1||l==r) return;
	t[k<<1].mx=tag;t[k<<1|1].mx=tag;
	t[k<<1].tag=tag;t[k<<1|1].tag=tag;
}
inline void build(int k,int l,int r){
	if(l==r){
		if(s[l]=='<'||s[l]=='>') t[k].mx=0;
		else t[k].mx=s[l]-48;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	pushup(k);
}
inline int query(int k,int l,int r,int x,int y){
	if(l==x&&r==y) return t[k].mx;
	pushdown(k,l,r);
	int mid=(l+r)>>1;
	if(y<=mid) return query(k<<1,l,mid,x,y);
	else if(x>mid) return query(k<<1|1,mid+1,r,x,y);
	else return max(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y));
}
inline void update(int k,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[k].mx=val;
		t[k].tag=0;
		return;
	}
	pushdown(k,l,r);
	int mid=(l+r)>>1;
	if(y<=mid) update(k<<1,l,mid,x,y,val);
	else if(x>mid) update(k<<1|1,mid+1,r,x,y,val);
	else update(k<<1,l,mid,x,mid,val),update(k<<1|1,mid+1,r,mid+1,y,val);
	pushup(k);
}
int r1[N],r2[N],b[N];
inline void print(){
	For(i,0,9) write(b[i]),putchar(' ');
	putchar('\n');
}
inline void solve(int x,int y){
	int X=r1[x],Y=r2[x];
	if(X>=y&&Y>=y){
		For(i,0,9) b[i]=sum[y][i]-sum[x-1][i];
		print();
	}else if(X>Y){
		For(i,0,9) b[i]=sum[Y][i]-sum[x-1][i];
		For(i,1,9) b[i-1]+=sum[Y][i]-sum[x-1][i];
		print();
	}else{
		if(R[X]<=y){
			int ans=0;
			Rep(i,9,0) ans+=sum[R[x]][i]-sum[X-1][i],b[i]=ans;
			For(i,0,9) b[i]+=sum[X-1][i]-sum[x-1][i];
			For(i,1,9) b[i-1]+=sum[X-1][i]-sum[x-1][i];
			print();
		}else{
			if(l[y]<x){
				For(i,0,9) b[i]=sum[y][i]-sum[x-1][i];
			}else{
				int ans=0;
				Rep(i,9,0) ans+=sum[l[y]][i]-sum[LL[l[y]]-1][i],b[i]=ans;
				For(i,0,9) b[i]+=sum[y][i]-sum[l[y]][i];
				For(i,0,9) b[i]+=sum[LL[l[y]]-1][i]-sum[x-1][i];
			}
			print();
		}
	}
}
int main(){
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	n=read();m=read();scanf("%s",s+1);
	For(i,2,n){
		if(s[i]=='<'&&s[i-1]=='<') continue;
		s[++top]=s[i];
	}
	n=top;
	For(i,1,n){
		For(j,0,9) sum[i][j]=sum[i-1][j];
		if(s[i]>='0'&&s[i]<='9') sum[i][s[i]-48]++;
	}
	top=0;memset(t,-1,sizeof t);
	build(1,1,n);
	For(i,1,n){
		if(s[i]=='>') a[++top]=i;
		else while(s[i]=='<'){
			int mx=query(1,1,n,a[top]+1,i-1);
			update(1,1,n,a[top]+1,i-1,0);
			if(top==0) continue;
			if(mx&1) R[a[top]]=i,top--;
			else{LL[i]=a[top];break;}
		}
	}
	while(top) R[a[top]]=n+1,top--;
	r1[n+1]=r2[n+1]=n+1;
	Rep(i,n,1){
		if(s[i]=='>') r1[i]=i;else r1[i]=r1[i+1];
		if(s[i]=='<') r2[i]=i;else r2[i]=r2[i+1];
	}
	For(i,1,n) if(s[i]=='<') l[i]=i;else l[i]=l[i-1];
	For(i,1,m){
		int x=read(),y=read();
		solve(x,y);
	}
	return 0;
}

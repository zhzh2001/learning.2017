#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<iostream>
#include<stdlib.h>
#include<map>
#include<set>
#include<vector>
#include<bitset>
#define ll long long
#define N 5005
#define oo 100000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,head[N],cnt,ans=oo;
struct edge{int from,next,to,w;}e[N<<1];
inline void insert(int u,int v,int w){
	e[++cnt]=(edge){u,head[u],v,w};head[u]=cnt;
	e[++cnt]=(edge){v,head[v],u,w};head[v]=cnt;
}
int rt1,rt2,root,mx1[N],mx2[N];
inline void dfs(int x,int fa){
	int y;
	for(int i=head[x];i;i=e[i].next)
		if((y=e[i].to)!=fa){
			dfs(y,x);
			if(mx1[y]+e[i].w>mx1[x]) mx2[x]=mx1[x],mx1[x]=mx1[y]+e[i].w;
			else if(mx1[y]+e[i].w>mx2[x]) mx2[x]=mx1[y]+e[i].w;
		}
}
int mn;
inline void Dfs(int x,int fa,int val){
	int y,mx=0;
	for(int i=head[x];i;i=e[i].next)
		if((y=e[i].to)!=fa){
			mx=max(mx,mx1[x]);
			mx=max(mx,val);
			if(mx1[y]+e[i].w!=mx1[x]) Dfs(y,x,max(val+e[i].w,mx1[x]+e[i].w));
			else Dfs(y,x,max(val+e[i].w,max(val+e[i].w,mx2[x]+e[i].w)));
		}
	cout<<x<<" "<<mx<<endl;
	if(mx<mn) mn=mx,root=x;
	if(mx1[x]<val) mx2[x]=mx1[x],mx1[x]=val;
	else if(mx2[x]<val) mx2[x]=val;
}
inline int solve(int x,int y){
	mn=oo;dfs(x,y);Dfs(x,y,0);
	return root;
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	Forn(i,1,n){
		int x=read(),y=read(),w=read();
		insert(x,y,w);
	}
	int mn=100000000;
	for(int i=1;i<n;i++){
		int p=i<<1;
		memset(mx1,0,sizeof mx1);
		memset(mx2,0,sizeof mx2);
		rt1=solve(e[p].from,e[p].to);
		rt2=solve(e[p].to,e[p].from);
		int mx=0;
		mx=max(mx,max(mx1[rt1]+mx2[rt1],mx1[rt2]+mx2[rt2]));
		mx=max(mx,mx1[rt1]+mx1[rt2]+e[p].w);
	//	cout<<mx<<endl;
		mn=min(mn,mx);
	}
	writeln(mn);
	return 0;
}

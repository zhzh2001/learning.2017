#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<iostream>
#include<stdlib.h>
#include<map>
#include<set>
#include<vector>
#include<bitset>
#define ll long long
#define N 5005
#define oo 100000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
char s[N];
int n,sum[N],f[N][N];
bool p[N][N];
int mn[N<<2];
inline int query(int k,int l,int r,int x,int y){
	if(l==x&&r==y) return mn[k];
	int mid=(l+r)>>1;
	if(y<=mid) return query(k<<1,l,mid,x,y);
	else if(x>mid) return query(k<<1|1,mid+1,r,x,y);
	else return min(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y)); 
}
inline bool pd(int l,int r){
	if(sum[r]-sum[l-1]!=0) return 0;
	if(query(1,1,n,l,r)<sum[l-1]) return 0;
	return 1;
}
inline void build(int k,int l,int r){
	if(l==r){mn[k]=sum[l];return;}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	mn[k]=min(mn[k<<1],mn[k<<1|1]);
}
int main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);n=strlen(s+1);
	For(i,1,n) sum[i]=sum[i-1]+((s[i]=='(')?1:-1);
	build(1,1,n);
	For(i,1,n) For(j,i+1,n) if(pd(i,j)) p[i][j]=1;
	For(i,1,n-1) For(j,1,n) if(j+i<=n) f[j][j+i]=f[j+1][j+i]+f[j][j+i-1]-f[j+1][j+i-1]+(p[j][j+i]==1);
	int Q=read();
	while(Q--){
		int x=read(),y=read();
		writeln(f[x][y]);
	}
	return 0;
}

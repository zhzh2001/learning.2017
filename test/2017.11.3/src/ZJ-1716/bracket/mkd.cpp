#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

ofstream fout("bracket.in");

int uniform(int l, int r) {
	return rand() % (r - l + 1) + l;
}

int main() {
	srand(GetTickCount());
	int n = 300, q = 300;
	for (int i = 1; i <= n; ++i)
		fout << (i % 2 == 1 ? '(' : ')');
	fout << endl;
	fout << q << endl;
	for (int i = 1; i <= q; ++i) {
//		int l = 1, r = n;
		int l = uniform(1, n - 2), r = uniform(l, n);
		fout << l << ' ' << r << endl;
	}
	return 0;
}

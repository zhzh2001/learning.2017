#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

ifstream fin("bracket.in");
ofstream fout("bracket.out");

const int N = 5005, Inf = 0x3f3f3f3f;
const bool debug = false;

int n, q;
char s[N];
int f[305][305];
int g[N][N];

int check(int l, int r) {
	if (~f[l][r])
		return f[l][r];
	int ans = 0;
	if (l == r)
		return f[l][r] = 0;
	if (l + 1 == r)
		return f[l][r] = (s[l] == '(' && s[r] == ')');
	if (s[l] == '(' && s[r] == ')')
		if (check(l + 1, r - 1))
			ans = 1;
	if (ans)
		return f[l][r] = ans;
	for (int i = l; i < r; ++i)
		if (check(l, i) && check(i + 1, r))
			ans = 1;
	return f[l][r] = ans;
}

void solve1() {
	memset(f, 0xff, sizeof f);
	for (int i = 1; i <= q; ++i) {
		int l, r;
		fin >> l >> r;
		int ans = 0;
		for (int j = l; j <= r; ++j)
			for (int k = j; k <= r; ++k)
				ans += check(j, k);
		fout << ans << endl;
	}
	exit(0);
}

int dp(int l, int r) {
	if (f[l][r] != -Inf)
		return f[l][r];
	if (l == r)
		return f[l][r] = 0;
	if (l + 1 == r)
		return f[l][r] = (s[l] == '(' && s[r] == ')');
	int &ans = f[l][r];
	ans = 0;
	if (s[l] == '(' && s[r] == ')') {
		int nxt = dp(l + 1, r - 1);
		if (nxt > 0)
			ans = f[l + 1][r - 1] + 1;
	}
	for (int i = l; i < r; ++i) {
		int a = dp(l, i);
		int b = dp(i + 1, r);
		if (a > 0 && b > 0)
			ans = max(ans, a + b + 1);
	}
	return ans;	
}

void solve2() {
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			f[i][j] = -Inf;
	for (int i = 1; i <= q; ++i) {
		int l, r;
		fin >> l >> r;
		fout << dp(l, r) << endl;
	}
	exit(0);	
}

int main() {
	fin >> s + 1 >> q;
	n = strlen(s + 1);
	if (!debug && n <= 300)
		solve1();
	if (n <= 5000)
		solve2();
	return 0;
}

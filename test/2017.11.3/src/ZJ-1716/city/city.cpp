#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

ifstream fin("city.in");
ofstream fout("city.out");

const int N = 5005;

int n, d1[N], d2[N], pred[N], d3[N], d4[N];
vector<pii> G[N];

void dfs(int *dis, int x, int fa) {
	for (int i = 0; i < G[x].size(); ++i) {
		int nxt = G[x][i].first;
		if (nxt != fa) {
			dis[nxt] = dis[x] + G[x][i].second;
			pred[nxt] = x;
			dfs(dis, nxt, x);
		}
	}
}

struct Edge {
	int u, v, w;
	Edge() {}
	Edge(int u, int v, int w) : u(u), v(v), w(w) {}
} E[N * 3];

int cnt, dele, newe;
bool vis[N];

void dfs1(int *dis, int x, int fa) {
	vis[x] = true;
	for (int i = 0; i < G[x].size(); ++i) {
		int nxt = G[x][i].first;
		if ((x == E[dele].u && nxt == E[dele].v) || (nxt == E[dele].u && x == E[dele].v))
			continue;
		if (!vis[nxt]) {
			dis[nxt] = dis[x] + G[x][i].second;
			dfs1(dis, nxt, x);
		}
	}
	if (x == E[newe].u) {
		int nxt = E[newe].v;
		if (!vis[nxt]) {
			dis[nxt] = dis[x] + E[newe].w;
			dfs1(dis, nxt, x);
		}
	}
	if (x == E[newe].v) {
		int nxt = E[newe].u;
		if (!vis[nxt]) {
			dis[nxt] = dis[x] + E[newe].w;
			dfs1(dis, nxt, x);
		}
	}
	vis[x] = false;
}

int main() {
	fin >> n;
	for (int i = 1; i < n; ++i) {
		int u, v, w;
		fin >> u >> v >> w;
		G[u].push_back(make_pair(v, w));
		G[v].push_back(make_pair(u, w));
	}
	dfs(d1, 1, 0);
	int pos = max_element(d1 + 1, d1 + n + 1) - d1;
	memset(pred, 0x00, sizeof pred);
	dfs(d2, pos, 0);
	int pos1 = max_element(d2 + 1, d2 + n + 1) - d2;
	for (int i = pos1; i; i = pred[i]) {
		for (int j = 0; j < G[i].size(); ++j) {
			int nxt = G[i][j].first;
			if (d2[i] == d2[nxt] + G[i][j].second)
				E[++cnt] = Edge(i, nxt, G[i][j].second);
		}
	}
	int len = d2[pos1];
	newe = cnt;
	for (int i = 1; i <= cnt; ++i) {
//		cerr << E[i].u << ' ' << E[i].v << ' ' << E[i].w << endl;
//		bu = E[i].v, bv = E[i].u;
		dele = i;
		for (int j = 1; j <= n; ++j) {
//			mu = j, mv = E[i].u;
			if (E[i].u == j || E[i].v == j)
				continue;
			E[++newe] = Edge(j, E[i].u, G[i][j].second);
			dfs1(d3, 1, 0);
			int pos = max_element(d3 + 1, d3 + n + 1) - d3;
			dfs1(d4, 1, 0);
			int pos1 = max_element(d4 + 1, d4 + n + 1) - d4;
			len = min(len, d4[pos1]);
		}
	}
	fout << len << endl;
	return 0; 
}

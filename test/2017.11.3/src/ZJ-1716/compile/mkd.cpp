#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

ofstream fout("compile.in");

int uniform(int l, int r) {
	return rand() % (r - l + 1) + l;
}

int main() {
	srand(GetTickCount());
	int n = 2e5, q = 2e5;
	fout << n << ' ' << q << endl;
	fout << '>';
	for (int i = 1; i <= n; ++i) {
//		int flag = rand() % 2;
		int flag = 0;
		if (flag) {
			fout << (rand() % 2 ? '>' : '<');
		} else {
			fout << (rand() % 10 + '0');
		}
	}
	fout << '<';
	fout << endl;
	for (int i = 1; i <= q; ++i) {
//		int l = 1, r = n;
		int l = uniform(1, n - 2), r = uniform(l, n);
		fout << l << ' ' << r << endl;
	}
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

ifstream fin("compile.in");
ofstream fout("compile.out");

const int N = 2e5 + 5;

int n, q;
string s, ps;
int now, nxt[N], prd[N];

void solve1() {
	for (int i = 1; i <= q; ++i) {
		ps = s;
		int l, r;
		fin >> l >> r;
		for (int j = l; j <= r; ++j) {
			nxt[j] = j + 1;
			prd[j] = j - 1;
		}
		int num[10] = {0};
		int cp = l, dp = 1;
		while (cp >= l && cp <= r) {
			if (isdigit(ps[cp])) {
				++num[ps[cp] - '0'];
				if (ps[cp] == '0') {
					prd[nxt[cp]] = prd[cp];
					nxt[prd[cp]] = nxt[cp];
				}
				--ps[cp];
				cp = (dp > 0) ? nxt[cp] : prd[cp];
			} else {
				dp = (ps[cp] == '>') ? 1 : -1;
				int np = (dp > 0) ? nxt[cp] : prd[cp];
				if (np >= l && np <= r && !isdigit(ps[np])) {
					prd[nxt[cp]] = prd[cp];
					nxt[prd[cp]] = nxt[cp];
				}
				cp = np;
			}
		}
		for (int j = 0; j < 10; ++j)
			fout << num[j] << ' ';
		fout << endl;
	}
	exit(0);
}

int main() {
	fin >> n >> q >> s;
	s = ' ' + s;
	solve1();
	return 0;
}

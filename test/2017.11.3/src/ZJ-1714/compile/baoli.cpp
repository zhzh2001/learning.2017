#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define lf else if
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=200010;
ll n,Q,opt[N],value[N],v[N],del[N],num[N];
char s[N];
int main(){
	freopen("compile.in","r",stdin);
	n=read();	Q=read();
	scanf("%s",s+1);
	For(i,1,n){
		if (s[i]=='>')	opt[i]=1;
		lf (s[i]=='<')	opt[i]=-1;
		else	value[i]=s[i]-'0';
	}
	For(i,1,Q){
		ll l=read(),r=read(),pos=l,turn=1;
		memset(del,0,sizeof del);
		memset(num,0,sizeof num);
		memcpy(v,value,sizeof value);
		while(pos>=l&&pos<=r){
			ll cur=pos;
			if (opt[pos]){
				turn=opt[pos];	pos+=turn;
				while(pos>=l&&pos<=r&&del[pos])	pos+=turn;
				if (opt[pos])	del[cur]=1;
			}else{
				num[v[pos]--]++;
				if (v[pos]<0)	del[pos]=1;
				pos+=turn;
				while(pos>=l&&pos<=r&&del[pos])	pos+=turn;
			}
		}For(i,0,9)	printf("%lld ",num[i]);
		puts("");
		writeln(pos);
	}
}

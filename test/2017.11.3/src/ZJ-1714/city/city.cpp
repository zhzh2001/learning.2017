#include<bits/stdc++.h>
#define ll int
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
namespace SHENZHEBEI{
#define NEG 1
	const int L=2333333;
	char SZB[L],*S=SZB,*T=SZB;
	inline char gc(){	if (S==T){	T=(S=SZB)+fread(SZB,1,L,stdin);	if (S==T) return '\n';	}	return *S++;	}
#if NEG
	inline ll read(){	ll x=0,f=1;	char ch=gc();	for (;!isdigit(ch);ch=gc())	if (ch=='-') f=-1;	for (;isdigit(ch);ch=gc())	x=x*10-48+ch;	return x*f;	}
	inline void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
#else
	inline ll read(){	ll x=0;	char ch=gc();	for (;!isdigit(ch);ch=gc());	for (;isdigit(ch);ch=gc())	x=x*10-48+ch;	return x;	}
	inline void write(ll x){	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
#endif
	inline char readchar(){	char ch=gc();	for(;isspace(ch);ch=gc());	return ch;	}
	inline ll readstr(char *s){	char ch=gc();	int cur=0;	for(;isspace(ch);ch=gc());		for(;!isspace(ch);ch=gc())	s[cur++]=ch;	s[cur]='\0';	return cur;	}
	inline void writeln(ll x){	write(x);	puts("");	}
}using namespace SHENZHEBEI;
const ll N=20010;
ll head[N],nxt[N],vet[N],val[N],now[N],len[N],x[N],y[N],dep[N],n,ANSW=1e9,ans,ons,tot;
void insert(ll x,ll y,ll w){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;	}
void dfs(ll x,ll pre){
	dep[x]=0;
	for(ll i=head[x];i;i=nxt[i])	if (vet[i]!=pre){
		dfs(vet[i],x);
		dep[x]=max(dep[x],dep[vet[i]]+val[i]);
	}
}
void Dfs(ll x,ll y,ll ff){
	ll t=dep[y];	dep[y]=ff;
	ll mx1=0,mx2=0;
	for(ll i=head[x];i;i=nxt[i]){
		if (vet[i]!=y)	now[vet[i]]=dep[vet[i]]+val[i];
		else	now[vet[i]]=ff;
		if (now[vet[i]]>now[mx1])	mx2=mx1,mx1=vet[i];
		else	mx2=vet[i];
	}
	ans=max(ans,now[mx1]+now[mx2]);
	ons=min(ons,max(now[mx1],now[mx2]));
	for(ll i=head[x];i;i=nxt[i])	if (vet[i]!=y){
		ll cost=0;
		if (vet[i]!=mx1)	cost=now[mx1];
		else	cost=now[mx2];
		Dfs(vet[i],x,cost+val[i]);
	}dep[y]=t;
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	For(i,2,n){
		x[i]=read(),y[i]=read(),len[i]=read();
		insert(x[i],y[i],len[i]);
		insert(y[i],x[i],len[i]);
	}
	For(i,2,n){
		dfs(x[i],y[i]);	dfs(y[i],x[i]);	ll answ=0;	ans=0;
		ons=1e9;	Dfs(x[i],y[i],0);	answ+=ons;
		ons=1e9;	Dfs(y[i],x[i],0);	answ+=ons;
		ANSW=min(ANSW,max(ans,answ+len[i]));
	}writeln(ANSW);
}

#include<bits/stdc++.h>
#define ll int
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(long long x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(long long x){ write(x);   puts("");   }
const ll N=1000010,block=1000;
vector<ll>g[N];
ll pre[N],q[N],L[N],R[N],sum[N],n,top,Q;
long long answ[N],ans;
char s[N];
struct data{	ll l,r,id;	}ask[N];
inline bool operator < (data a,data b){	return a.l/block==b.l/block?a.r<b.r:a.l<b.l;	}
ll QQQ(ll x,ll v){//g[v]中有多少小于等于x 
	ll l=0,r=g[v].size()-1,ans=0;
	while(l<=r){
		ll mid=(l+r)>>1;
		if (g[v][mid]<=x)	ans=mid,l=mid+1;
		else	r=mid-1;
	}
	return ans;
}
void init(){
	For(i,0,n)	pre[i]+=n;
	q[top=1]=0;	L[0]=-1;
	For(i,1,n){
		while(top&&pre[q[top]]>=pre[i])	--top;
		L[i]=top?q[top]:-1;	q[++top]=i;
	}
	q[top=1]=n;	R[n]=n+1;
	FOr(i,n-1,0){
		while(top&&pre[q[top]]>=pre[i])	--top;
		R[i]=top?q[top]:n+1;	q[++top]=i;
	}
	For(i,0,n*2)	g[i].push_back(-1);
	For(i,0,n)	g[pre[i]].push_back(i);
	For(i,0,n){
		L[i]=QQQ(i,pre[i])-QQQ(L[i],pre[i])-1;
		R[i]=QQQ(R[i]-1,pre[i])-QQQ(i-1,pre[i])-1;
	}
}
#define min(x,y)	x<y?x:y
int main(){
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	ll l=0,r=-1;
	scanf("%s",s+1);	n=strlen(s+1);
	For(i,1,n)	pre[i]=pre[i-1]+(s[i]=='('?1:-1);	Q=read();
	For(i,1,Q)	ask[i].l=read()-1,ask[i].r=read(),ask[i].id=i;
	sort(ask+1,ask+Q+1);	init();
	For(i,1,Q){
		while(r>=ask[i].r){	--sum[pre[r]];	ans-=min(L[r],sum[pre[r]]);	--r;	}
		while(r<ask[i].r){	++r;	ans+=min(L[r],sum[pre[r]]);	++sum[pre[r]];	}
		while(l<=ask[i].l){	--sum[pre[l]];	ans-=min(R[l],sum[pre[l]]);	++l;	}
		while(l>ask[i].l){	--l;	ans+=min(R[l],sum[pre[l]]);	++sum[pre[l]];	}
		answ[ask[i].id]=ans;
	}For(i,1,Q)	writeln(answ[i]);
}

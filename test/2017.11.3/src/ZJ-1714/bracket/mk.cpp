#include<cstdio>
#include<memory.h>
#include<ctime>
#include<algorithm>
#define ll long long 
#define maxn 1010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll get(){	return 1LL*rand()<<15|rand();	}
int main(){
	freopen("bracket.in","w",stdout);
	srand(time(0));
	ll n=500000,Q=500000;
	For(i,1,n)	putchar(get()%2?'(':')'); 
	puts("");
	writeln(Q);
	while(Q--){
		ll l=get()%n+1,r=get()%n+1;
		if (l>r)	swap(l,r);
		printf("%lld %lld\n",l,r);
	}
} 

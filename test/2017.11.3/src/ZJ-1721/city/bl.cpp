#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int d[N],b[N],to[N*2],pr[N*2],la[N],v[N],V,u,n,x,y,z,X,Y,del,cnt,ans;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		return;
	}
	static int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}
void dfs(int x,int fa)
{
	b[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
			dfs(to[i],x);
}
void dfs1(int x,int fa)
{
	if (x==X&&fa!=Y)
	{
		d[Y]=d[x]+v[del];
		dfs1(Y,x);
	}
	if (x==Y&&fa!=X)
	{
		d[X]=d[x]+v[del];
		dfs1(X,x);
	}
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa&&i!=del&&i!=(del^1))
		{
			d[to[i]]=d[x]+v[i];
			dfs1(to[i],x);
		}
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.ans","w",stdout);
	read(n);
	cnt=1;
	ans=99999999;
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	for (int i=1;i<n;++i)
	{
		del=i*2;
		memset(b,0,sizeof(b));
		int u=to[del],v=to[del^1];
		dfs(u,v);
		for (int j=1;j<n;++j)
			for (int k=j+1;k<=n;++k)
				if (b[j]!=b[k])
				{
					X=j;
					Y=k;
					d[1]=0;
					dfs1(1,0);
					u=1;
					for (int l=2;l<=n;++l)
						if (d[l]>d[u])
							u=l;
					d[u]=0;
					dfs1(u,0);
					V=1;
					for (int l=2;l<=n;++l)
						if (d[l]>d[V])
							V=l;
					//if (d[V]==24)
						//cout<<to[del]<<' '<<to[del+1]<<' '<<X<<' '<<Y<<'\n';
					ans=min(d[V],ans);
				}
	}
	write(ans);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int x,y,z,cnt,to[N*2],pr[N*2],la[N],v[N*2],f[N],s[N],g[N],a[N],b[N],n,zb,yb,ans,d[N],dis[N],U,V;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		return;
	}
	static int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
void dfs(int x,int fa)
{
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			d[to[i]]=d[x]+v[i];
			dfs(to[i],x);
		}
}
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}
inline int ga(int x,int fa)
{
	if (x==V)
	{
		b[x]=1;
		a[++cnt]=x;
		return 1;
	}
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa&&ga(to[i],x))
		{
			b[x]=1;
			a[++cnt]=x;
			s[cnt]=v[i];
			return 1;
		}
	return 0;
}
int Dfs(int x,int fa)
{
	int mx=d[x];
	for (int i=la[x];i;i=pr[i])
		if (!b[to[i]]&&to[i]!=fa)
		{
			d[to[i]]=d[x]+v[i];
			mx=max(mx,Dfs(to[i],x));
		}
	return mx;
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	read(n);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	dfs(1,0);
	U=1;
	for (int i=2;i<=n;++i)
		if (d[i]>d[U])
			U=i;
	d[U]=0;
	V=1;
	dfs(U,0);
	for (int i=2;i<=n;++i)
		if (d[i]>d[V])
			V=i;
	cnt=-1;
	ga(U,0);
	for (int i=1;i<cnt;++i)
	{
		d[a[i]]=0;
		dis[i]=Dfs(a[i],0);
	}
	for (int i=1;i<=cnt;++i)
		s[i]+=s[i-1];
	ans=s[cnt];
	for (int i=1;i<cnt;++i)
		f[i]=max(f[i-1],dis[i]+s[i]);
	for (int i=cnt-1;i>=1;--i)
		g[i]=max(g[i+1],dis[i]+s[cnt]-s[i]);
	for (int i=1;i<cnt;++i)
		for (int j=i+1;j<cnt;++j)
		{
			zb=max(s[j],max(s[i]+s[cnt]-s[j],s[cnt]-s[i]));
			yb=max(min(max(f[j],f[j]-s[i]+s[cnt]-s[j]),max(g[i],g[i]-s[cnt]+s[j]+s[i])),max(f[i],g[j]));
			//if (max(zb,yb)==14)
				//cout<<a[i]<<' '<<a[j]<<'\n';
			ans=min(ans,max(zb,yb));
		}
	write(ans);
	return 0;
}
	
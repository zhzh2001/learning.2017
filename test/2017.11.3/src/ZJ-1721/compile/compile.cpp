#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int pr[N],ne[N],a[N],f[N],g[10],n,q,p,np,l,r,fl;
char s[N];
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		return;
	}
	static int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
inline void del(int x)
{
	pr[ne[x]]=pr[x];
	ne[pr[x]]=ne[x];
	f[x]=233;
}
int main()
{
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	read(n);
	read(q);
	scanf("%s",s+1);
	for (int i=1;i<=n;++i)
		if (s[i]=='<')
			a[i]=-2;
		else
		{
			if (s[i]=='>')
				a[i]=-1;
			else
				a[i]=s[i]-'0';
		}
	while (q--)
	{
		read(l);
		read(r);
		for (int i=l;i<=r;++i)
		{
			f[i]=a[i];
			ne[i]=i+1;
			pr[i]=i-1;
		}
		p=l;
		fl=1;
		while (p>=l&&p<=r)
		{
			if (f[p]>=0)
			{
				++g[f[p]];
				--f[p];
				if (f[p]<0)
					del(p);
			}
			else
				fl=f[p]+2;
			if (fl)
				np=ne[p];
			else
				np=pr[p];
			if (f[np]<0&&f[p]<0)
				del(p);
			p=np;
		}
		for (int i=0;i<=9;++i)
		{
			write(g[i]);
			putchar(' ');
			g[i]=0;
		}
		putchar('\n');
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int f[N][N],a[N],n,q,l,r,sum;
char s[N];
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		return;
	}
	static int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
}
int main()
{
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	for (int i=1;i<=n;++i)
		if (s[i]=='(')
			a[i]=1;
		else
			a[i]=-1;
	for (int i=1;i<=n;++i)
	{
		sum=0;
		for (int j=i;j<=n;++j)
		{
			sum+=a[j];
			if (!sum)
				f[i][j]=1;
			if (sum<0)
				break;
		}
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
			f[i][j]=f[i][j]-f[i-1][j-1]+f[i-1][j]+f[i][j-1];
	read(q);
	while (q--)
	{
		read(l);
		read(r);
		--l;
		write(f[r][r]+f[l][l]-f[l][r]-f[r][l]);
		putchar('\n');
	}
	return 0;
}

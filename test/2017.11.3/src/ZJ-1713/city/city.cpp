//#include<bits/stdc++.h>
//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("city.in");
ofstream fout("city.out");
int to[10003],head[5003],nxt[10003],w[10003],tot;
int a[10003],b[10003],c[10003];
inline void add(int a,int b,int c){
	to[++tot]=b;  nxt[tot]=head[a];  head[a]=tot; w[tot]=c;
}
int ju[5003],zhi[5003];
int vis[5003];
void dfs1(int x,int fa){
	int mx1=0,mx2=0;
	vis[x]=1;
	ju[x]=0;
	for(int i=head[x];i;i=nxt[i]){
		if(fa!=to[i]){
			dfs1(to[i],x);
			ju[x]=max(ju[to[i]]+w[i],ju[x]);
			if(ju[to[i]]+w[i]>mx2){
				mx2=mx1;
				mx1=ju[to[i]]+w[i];
			}else if(ju[to[i]]+w[i]>mx2){
				mx2=ju[to[i]]+w[i];
			}
		}
	}
	zhi[x]=mx1+mx2;
}
void dfs2(int x,int fa){
	int mx1=99999999,mx2=99999999;
	vis[x]=2;
	ju[x]=0;
	for(int i=head[x];i;i=nxt[i]){
		if(fa!=to[i]){
			dfs2(to[i],x);
			ju[x]=max(ju[to[i]]+w[i],ju[x]);
			if(ju[to[i]]+w[i]>mx2){
				mx2=mx1;
				mx1=ju[to[i]]+w[i];
			}else if(ju[to[i]]+w[i]>mx2){
				mx2=ju[to[i]]+w[i];
			}
		}
	}
	zhi[x]=mx1+mx2;
}
int n;
int main(){
	fin>>n;
	for(int i=1;i<=n-1;i++){
		fin>>a[i]>>b[i]>>c[i];
	}
	int ans=999999999;
	for(int i=1;i<=n-1;i++){
		memset(head,0,sizeof(head));
		tot=0;
		for(int j=1;j<=n-1;j++){
			if(i!=j){
				add(a[j],b[j],c[j]);
				add(b[j],a[j],c[j]);
			}
		}
		for(int j=1;j<=n;j++){
			ju[j]=0,vis[j]=0;
		}
		dfs1(1,0);
		int ans1=999999999,ans2=999999999;
		for(int j=1;j<=n;j++){
			if(vis[j]==1){
				dfs1(j,0);
				ans1=min(ans1,ju[j]);
			}else{
				dfs2(j,0);
				ans2=min(ans2,ju[j]);
			}
		}
		for(int j=1;j<=n;j++){
			ans=max(ans,zhi[j]);
		}
		ans=min(ans,ans1+ans2+c[i]);
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
5
1 2 1
2 3 2
3 4 3
4 5 4

out:
7

*/

//#include<bits/stdc++.h>
//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("bracket.in");
ofstream fout("bracket.out");
char s[500003];
struct tree{
	int lz,ly,rz,ry,tag,ans,lnum,rnum,size;
}t[2000003];
tree pushup(tree a,tree b){
	tree c;
	c.size=a.size+b.size;
	c.ans=0,c.lnum=0,c.rnum=0,c.tag=0;
	c.ans=a.ans+b.ans;
	c.ly=a.ly,c.lz=a.lz;
	c.rz=b.rz,c.ry=b.ry;
	if(a.ly==a.size) c.ly=a.ly+b.ly;
	if(a.lz==a.size) c.lz=a.lz+b.lz;
	if(b.ry==b.size) c.ry=a.ry+b.ry;
	if(b.rz==b.size) c.rz=a.rz+b.rz;
	if(a.rz==b.ly){
		c.ans+=a.rz;
		if(b.lnum&&a.rnum&&a.rz){
			c.ans+=((b.lnum+1+a.rnum)*(b.lnum+a.rnum)/2);
			c.ans-=((b.lnum)*(b.lnum-1)/2);
			c.ans-=((a.rnum)*(a.rnum-1)/2);
		}else{
			if(b.lnum&&a.rnum){
				c.ans+=((b.lnum+a.rnum)*(b.lnum+a.rnum-1)/2);
				c.ans-=((b.lnum)*(b.lnum-1)/2);
				c.ans-=((a.rnum)*(a.rnum-1)/2);
			}
		}
		if(a.rz!=0){
			if(b.tag) c.rnum=b.lnum+1+a.rnum;
			if(a.tag) c.lnum=a.lnum+1+b.lnum;
		}else{
			if(b.tag) c.rnum=b.lnum+a.rnum;
			if(a.tag) c.lnum=a.lnum+b.lnum;
		}
		c.tag=(a.tag&b.tag);
	}else{
		c.ans+=min(a.rz,b.ly);
		c.tag=0;
		if(a.rz<b.ly){
			c.ans+=a.rnum;
			if(a.rz!=0){
				if(a.tag)c.lnum=a.lnum+1;
			}
			if(a.rz==a.size) c.lz=0,c.ly=0,c.lnum=1;
		}else{
			c.ans+=b.lnum;
			if(b.ly!=0){
				if(b.tag)c.rnum=b.rnum+1;
			}
			if(b.ly==b.size) c.ry=0,c.rz=0,c.rnum=1;
		}
	}
	if(a.lz==0&&a.ly==0){
		
	}
	if(c.ly==c.size||c.lz==c.size||c.ry==c.size||c.rz==c.size){
		c.tag=1;
	}
	return c;
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].size=1;
		if(s[l]==')'){
			t[rt].lz=0,t[rt].rz=0;
			t[rt].ly++,t[rt].ry++;
			t[rt].tag=1,t[rt].ans=0;
			t[rt].rnum=0,t[rt].lnum=0;
		}else{
			t[rt].ly=0,t[rt].ry=0;
			t[rt].lz++,t[rt].rz++;
			t[rt].tag=1,t[rt].ans=0;
			t[rt].rnum=0,t[rt].lnum=0;
		}
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	t[rt].size=t[rt*2].size+t[rt*2+1].size;
	t[rt]=pushup(t[rt*2],t[rt*2+1]);
}
tree query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return t[rt];
	}
	int mid=(l+r)/2;
	if(y<=mid){
		return query(rt*2,l,mid,x,y);
	}else if(x>mid){
		return query(rt*2+1,mid+1,r,x,y);
	}else{
		return pushup(query(rt*2,l,mid,x,mid),query(rt*2+1,mid+1,r,mid+1,y));
	}
}
int main(){
	fin>>(s+1);
	int n=strlen(s+1);
	build(1,1,n);
	int q;
	//tree a,b;
	//a=query(1,1,n,1,3);
	//b=query(1,1,n,4,4);
	//a.rnum=1,a.rz=0,a.ans=1;
	//b.lnum=1,b.ly=0,b.ans=1;
	//a.lnum=0,a.rnum=0,a.ans=0,a.tag=1,a.rz=1;
	//b.lnum=0,b.rnum=0,b.ans=0,b.tag=1,b.ly=1;
	//cout<<a.ry<<" "<<a.rnum<<" "<<a.ans<<" "<<a.tag<<endl;
	//cout<<"ojbk"<<a.rnum<<endl;
	//cout<<"ok"<<pushup(a,b).lnum<<endl;
	fin>>q;
	for(int i=1;i<=q;i++){
		int l,r;
		fin>>l>>r;
		fout<<query(1,1,n,l,r).ans<<endl;
	}
	return 0;
}
/*

in:
(())()()()
5
1 8

7 8
6 10

4 8
5 8

out:
1
3
7
3
3

*/

program ec12;
var 
	s,s1:ansistring;
	a:array[0..9] of longint;
	b:array[0..200001,0..9] of longint;
	st:array[0..200002] of char;
	next,last:array[0..200002] of longint;
	vis:array[0..200001] of boolean;
	n,m,i,j,l,r,dis,head,zhen,len:longint;
begin 
	assign(input,'compile.in'); reset(input);
	assign(output,'compile.out'); rewrite(output);
	readln(n,m);
	readln(s);
	len:=length(s);
	{if n*m<=10000 then 
	begin 
		for i:=1 to m do 
		begin 
			fillchar(a,sizeof(a),0);
			readln(l,r);
			s1:=copy(s,l,r-l+1);
			head:=1;
			dis:=1;
			s1:=s1+'!';
			while (head>0) and (s1[head]<>'!') do 
			begin
				if s1[head] in ['0'..'9'] then 
				begin 
					inc(a[ord(s1[head])-ord('0')]);
					if s1[head]>'0' then 
					s1[head]:=chr(ord(s1[head])-1)
					else
					delete(s1,head,1);
					if dis=1 then 
					inc(head)
					else
					dec(head);
				end;
				if s1[head]='<' then 
				begin 
					dis:=-1;
					dec(head);
					if (s1[head]='<') or (s1[head]='>') then 
					delete(s1,head+1,1);
				end;
				if s1[head]='>' then 
				begin 
					dis:=1;
					inc(head);
					if (s1[head]='<') or (s1[head]='>') then 
					delete(s1,head-1,1);
				end;
			end;
			for j:=0 to 9 do 
			write(a[j],' ');
			writeln;
		end;
	end
	else}
	begin
		fillchar(b,sizeof(b),0);
		fillchar(a,sizeof(a),0);
		fillchar(vis,sizeof(vis),true);
		head:=1;
		dis:=1;
		for i:=1 to len do 
		begin 
			st[i]:=s[i];
			last[i]:=i-1;
			next[i]:=i+1;
		end;
		head:=1;
		dis:=1;
		while (head<=len) and (head>0) do
		begin 
			if st[head] in ['0'..'9'] then 
			begin 
				inc(a[ord(st[head])-ord('0')]);
				if st[head]>'0' then 
				st[head]:=chr(ord(st[head])-1)
				else
				begin 
					next[last[head]]:=next[head];
					last[next[head]]:=last[head];
				end;	
				if dis=1 then 
				begin 
					if vis[head] then 
					begin 
						vis[head]:=false;
						for j:=0 to 9 do 
						b[head,j]:=a[j];
					end;
					head:=next[head];
					if head=length(s)+1 then 
					break;
				end
				else
				head:=last[head];
			end;
			if (st[head]='<') or (st[head]='>') then 
			begin 
				if st[head]='>' then
				begin 
					dis:=1;
					head:=next[head];
				end
				else
				begin
					dis:=-1;
					head:=last[head];
				end;
				if (st[head]='<') or (st[head]='>') then 
				begin 
					next[last[last[head]]]:=head;
					if vis[last[head]] then 
					begin
						vis[last[head]]:=false;
						for j:=0 to 9 do 
						b[last[head],j]:=a[j];
					end;
					last[head]:=last[last[head]];
				end;
			end;
			if vis[last[head]] then 
			begin 
				vis[last[head]]:=false;
				for j:=0 to 9 do 
				b[last[head],j]:=a[j];
			end;
		end;
		len:=length(s); 
		for i:=1 to len do 
		begin 
			if vis[i] then 
			begin 
				vis[i]:=false;
				for j:=0 to 9 do
				b[i,j]:=a[j];
			end;
		end;
		for i:=1 to length(s) do 
		begin 
			for j:=0 to 9 do 
			write(b[i,j],' ');
			writeln;
		end;
		{for i:=1 to m do 
		begin 
			readln(l,r);
			if l=1 then 
			begin
				for j:=0 to 9 do 
				write(b[r,j],' ');
				writeln;
			end
			else
			begin
				if r=length(s) then 
				begin 
					for j:=0 to 9 do 
					write(b[length(s),j]-b[l-1,j],' ');
					writeln;
				end
				else
				begin 
					for j:=0 to 9 do 
					write(b[r,j]-b[l,j],' ');
					writeln;
				end;
			end;
		end;}
	end;
	close(input);
	close(output);
end. 
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,b[5010],cnt=0,dist[5010];
bool vis[200010];
int nedge=0,p[200010],c[200010],nex[200010],head[200010];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
inline void dfs(int x,int fa,int op){
	dist[x]=0;b[x]=op;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!vis[k]){
		dfs(p[k],x,op);dist[x]=max(dist[x],dist[p[k]]+c[k]);
	}
}
inline void dfss(int x,int fa,int v){
	int ma=v,mi=0;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!vis[k]){
		if(dist[p[k]]>ma)mi=ma,ma=dist[p[k]];
		else if(dist[p[k]]>mi)mi=dist[p[k]];
	}
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!vis[k]){
		int q=(dist[p[k]]==ma)?mi:ma;
		dist[p[k]]=max(dist[p[k]],q+c[k]);dfss(p[k],x,q+c[k]);
	}
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		addedge(x,y,z);addedge(y,x,z);
	}
	int ans=1e9;
	for(int i=1;i<n;i++){
		int ne=2*i-1;
		vis[ne]=vis[ne+1]=1;
		dfs(p[ne],0,cnt);dfs(p[ne+1],0,cnt+1);
		dfss(p[ne],0,0);dfss(p[ne+1],0,0);
		int ml=1e9,mr=1e9,sum=0;
		for(int j=1;j<=n;j++){
			sum=max(sum,dist[j]);
			if(b[j]==cnt)ml=min(ml,dist[j]);
			else mr=min(mr,dist[j]);
		}
		sum=max(sum,ml+mr+c[ne]);
		ans=min(ans,sum);
		cnt+=2;vis[ne]=vis[ne+1]=0;
	}
	printf("%d",ans);
	return 0;
}

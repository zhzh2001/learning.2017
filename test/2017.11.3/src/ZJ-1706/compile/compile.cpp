#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,q,ans[10],top;
bool vis[2010];
char s[200010],zh[200010];
int main()
{
	freopen("compile.in","r",stdin);
	freopen("compile.out","w",stdout);
	n=read();q=read();
	scanf("%s",s+1);
	while(q--){
		int l=read(),r=read();
		top=0;memset(ans,0,sizeof ans);
		for(int i=l;i<=r;i++)if(s[i]>='0'&&s[i]<='9'){
			ans[s[i]-'0']++;zh[++top]=s[i]-1;
		}else{
			if(zh[top]=='>')top--;
			if(s[i]=='>')zh[++top]=s[i];
			else{
				while(1){
					int p=top;
					bool flag=0;int ma=0,r[10];memset(r,0,sizeof r);
					while(p&&zh[p]!='>')ma=max(ma,zh[p--]-'0');
					if(!p){
						for(int j=1;j<=top;j++)ans[zh[j]-'0']++;
						top=0;break;
					}
					for(int j=p+1;j<=top;j++)
						for(int k=0;k<=zh[j]-'0';k++)ans[k]++;
					top=p;
					if(ma&1)break;
					top--;
				}
				if(!top)break;
			}
		}
		for(int i=0;i<10;i++)printf("%d ",ans[i]);
		puts("");
	}
	return 0;
}

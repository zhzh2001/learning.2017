#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int p[500010],n,m,now=0;
char s[500010];
int main()
{
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);n=strlen(s+1);
	m=read();
	while(m--){
		int l=read(),r=read();
		p[0]=0;int now=0;
		ll ans=0;
		for(int i=l;i<=r;i++){
			if(s[i]=='(')p[now++]++;
			else{
				p[now--]=0;
				if(now<0)now=0;
				else ans+=p[now];
			}
		}
		printf("%lld\n",ans);
	}
	return 0;
}

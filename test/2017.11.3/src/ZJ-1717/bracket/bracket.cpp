#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define int long long
using namespace std;
inline int read() {
	int x = 0,f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(int x) {
	if(x<0) x=-x,putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x) {
	write(x);
	putchar('\n');
}

const int N = 5011;
int n,h,t,Q;
int sum[N],del[N],ans[N];
char s[N];
struct node{
	int x,y,id,ans;
}q[N],tmp[N];
inline bool cmp(node a,node b) {
	return a.y < b.y;
}
inline bool cmp_2(node a,node b) {
	return a.id < b.id;
}

signed main() {
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	For(i, 1, n) if(s[i]=='(') sum[i]=1;
		else sum[i]=-1;
		
		
	Q = read();
	For(i, 1, Q){
		q[i].x=read();q[i].y=read();q[i].id=i;
	}
	sort(q+1,q+Q+1,cmp);
	s[0] = 'A';
	For(i, 1, n) sum[i]+=sum[i-1];
	
	For(i, 1, n-1) {
		if(s[i-1]=='(') 
		  For(j, i, n) --sum[ j ];
		else if(s[i-1]==')') 
		  For(j, i, n) ++sum[ j ];
		   
		if(sum[i]<0) continue;
		int l = i;
		t = 0; h = 1;
		For(j, 0, n+2) del[j] = 0; 
		For(j, i+1, n) {
			int r = j;
			if(j==i+1) {
			  For(k, 1, Q) 
				if( q[k].x<=l && r<=q[k].y ) 
					tmp[++t] = q[k],tmp[t].id = k;
			}
			else while(r>q[h].y && h<=t) ++h;
			
			if(sum[j] < 0) break;
			if(sum[j]==0) del[h]++; 
		}
		int num = 0;
		For(j, 1, t) {
			num+=del[j];
			q[ tmp[j].id ].ans+=num;
		}
	}
	
	For(i, 1, Q) ans[q[i].id] = q[i].ans;
	For(i, 1, Q) writeln(ans[i]);
	return 0;
}









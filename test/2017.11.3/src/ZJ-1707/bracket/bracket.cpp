#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,q,l,r,ff;
bool flag;
int a[500100];
ll ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("bracket.in","r",stdin);
	freopen("bracket.out","w",stdout);
	char ch=getchar();n=0;
    while(ch!='('&&ch!=')')ch=getchar();
    while(ch=='('||ch==')')
    {
    	n++;
    	if(ch=='(')a[n]=1;else a[n]=2;
    	ch=getchar();
	}
	q=read();
	while(q--)
	{
		l=read();r=read();ans=0;
		For(i,l,r-1)
			For(j,i+1,r)
			{
				if((j-i)%2==0)continue;
				flag=true;ff=0;
				For(k,i,j)
				{
					if(a[k]==1)ff++;else ff--;
					if(ff<0)
					{
						flag=false;
						break;
					}
				}
				if(ff>0)flag=false;
				if(flag)ans++;
			}
		cout<<ans<<endl;
	}
	return 0;
}


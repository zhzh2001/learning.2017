#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define pb push_back
#define N 5010
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int f[N][N];
int fa[N],ff[N];
bool b[N],fff[N][N];
vector<int>a[N];
int ans,lzq,jzq,mx,n,fm,t;
void build(int x)
{
	b[x]=true;
	For(i,0,a[x].size()-1)
	{
		if(b[a[x][i]]==true)continue;
		if(fff[a[x][i]][x]==false)continue;
		ff[a[x][i]]=ff[x]+f[x][a[x][i]];
		build(a[x][i]);
	}
	if(ff[x]>mx)
	{
		mx=ff[x];
		lzq=x;
	}
}
void dfs(int x)
{
	b[x]=true;
	For(i,0,a[x].size()-1)
	{
		if(b[a[x][i]]==true)continue;
		if(fff[a[x][i]][x]==false)continue;
		ff[a[x][i]]=ff[x]+f[x][a[x][i]];
		fa[a[x][i]]=x;
		//sd[a[x][i]]=sd[x]+1;
		dfs(a[x][i]);
	}
	if(ff[x]>mx)
	{
		mx=ff[x];
		jzq=x;
	}
}
int main()
{
	n=read();
	For(i,1,n-1)
	{
		int x=read(),y=read();f[x][y]=f[y][x]=read();
		fff[x][y]=fff[y][x]=true;
		a[x].pb(y);a[y].pb(x);
	}
	mx=0;
	memset(b,false,sizeof(b));
	build(1);
	memset(ff,0,sizeof(ff));
	memset(b,false,sizeof(b));
	//sd[lzq]=1;
	dfs(lzq);
	cout<<jzq<<endl;
	fm=lzq;t=jzq;
	ans=mx;
	while(fa[t]!=fm)
	{
		//a[fa[t]].erase(remove(a[fa[t]].begin(),a[fa[t]].end,t),a[fa[t]].end());
		fff[fa[t]][t]=fff[t][fa[t]]=false;
		For(i,1,n)
		{
			if(i==fa[t])continue;
			a[i].pb(t);fff[i][t]=fff[t][i]=true;
			f[t][i]=f[i][t]=f[fa[t]][t];
			mx=0;
			build(1);
			dfs(lzq);
			if(mx<ans)ans=mx;
			fff[i][t]=fff[t][i]=false;
			//a[i].erase(remove(a[i].begin(),a[i].end,t),a[i].end());
		}
		fff[fa[t]][t]=fff[t][fa[t]]=true;
		t=fa[t];
	}
	cout<<ans<<endl;
	return 0;
}


#include<fstream>
#include<string>
#include<cstring>
using namespace std;
ifstream fin("fast.in");
ofstream fout("fast.out");
struct bigint
{
	int len,dig[200];
	bigint()
	{
		len=1;
		memset(dig,0,sizeof(dig));
	}
	bigint(int x)
	{
		len=0;
		memset(dig,0,sizeof(dig));
		do
			dig[++len]=x%10;
		while(x/=10);
	}
	bigint operator*(const int b)const
	{
		bigint res;
		res.len=len;
		int overflow=0;
		for(int i=1;i<=len;i++)
		{
			res.dig[i]=dig[i]*b+overflow;
			overflow=res.dig[i]/10;
			res.dig[i]%=10;
		}
		if(overflow)
			res.dig[++res.len]=overflow;
		return res;
	}
	bigint operator+(const int b)const
	{
		bigint res=*this;
		res.dig[1]+=b;
		for(int i=1;res.dig[i]>9;i++)
		{
			res.dig[i+1]+=res.dig[i]/10;
			res.dig[i]%=10;
		}
		if(res.dig[res.len+1])
			res.len++;
		return res;
	}
	bigint operator-(const bigint& b)const
	{
		bigint res;
		res.len=len;
		int underflow=0;
		for(int i=1;i<=len;i++)
		{
			res.dig[i]=dig[i]-b.dig[i]-underflow;
			if(res.dig[i]<0)
			{
				underflow=1;
				res.dig[i]+=10;
			}
			else
				underflow=0;
		}
		while(res.len>1&&res.dig[res.len]==0)
			res.len--;
		return res;
	}
	bigint operator-(const int b)const
	{
		bigint bb(b);
		return *this-bb;
	}
}ans;
ostream& operator<<(ostream& os,const bigint& b)
{
	for(int i=b.len;i;i--)
		os<<b.dig[i];
	return os;
}
int main()
{
	int n;
	string s;
	fin>>n>>s;
	int cnt1=0;
	for(int i=0;i<n;i++)
	{
		ans=ans*2+s[i]-'0';
		if(s[i]=='1')
			cnt1++;
	}
	fout<<ans-cnt1<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("bomb.in");
ofstream fout("bomb.out");
int s[5005][5005];
int main()
{
	int n,r;
	fin>>n>>r;
	int x2=0,y2=0;
	for(int i=1;i<=n;i++)
	{
		int x,y,v;
		fin>>x>>y>>v;
		x2=max(x2,x);
		y2=max(y2,y);
		s[x][y]+=v;
	}
	for(int i=0;i<=x2;i++)
		for(int j=0;j<=y2;j++)
		{
			if(i==0&&j==0)
				continue;
			if(i==0)
				s[i][j]+=s[i][j-1];
			else
				if(j==0)
					s[i][j]+=s[i-1][j];
				else
					s[i][j]+=s[i][j-1]+s[i-1][j]-s[i-1][j-1];
		}
	int ans=0;
	for(int i=0;i+r-1<=x2;i++)
		for(int j=0;j+r-1<=y2;j++)
			if(i==0&&j==0)
				ans=max(ans,s[i+r-1][j+r-1]);
			else
				if(i==0)
					ans=max(ans,s[i+r-1][j+r-1]-s[i+r-1][j-1]);
				else
					if(j==0)
						ans=max(ans,s[i+r-1][j+r-1]-s[i-1][j+r-1]);
					else
						ans=max(ans,s[i+r-1][j+r-1]-(s[i-1][j+r-1]+s[i+r-1][j-1]-s[i-1][j-1]));
	fout<<ans<<endl;
	return 0;
}
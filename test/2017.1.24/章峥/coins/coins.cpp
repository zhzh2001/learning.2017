#include<fstream>
using namespace std;
ifstream fin("coins.in");
ofstream fout("coins.out");
int c[5];
long long f[100005];
int main()
{
	for(int i=1;i<=4;i++)
		fin>>c[i];
	f[0]=1;
	for(int i=1;i<=4;i++)
		for(int j=c[i];j<=100000;j++)
			f[j]+=f[j-c[i]];
	int n;
	fin>>n;
	while(n--)
	{
		int d1,d2,d3,d4,s;
		fin>>d1>>d2>>d3>>d4>>s;
		long long ans=0;
		#define inc(x) if(x>=0) ans+=f[x];
		#define dec(x) if(x>=0) ans-=f[x];
		inc(s);
		dec(s-c[1]*(d1+1));
		dec(s-c[2]*(d2+1));
		dec(s-c[3]*(d3+1));
		dec(s-c[4]*(d4+1));
		inc(s-c[1]*(d1+1)-c[2]*(d2+1));
		inc(s-c[1]*(d1+1)-c[3]*(d3+1));
		inc(s-c[1]*(d1+1)-c[4]*(d4+1));
		inc(s-c[2]*(d2+1)-c[3]*(d3+1));
		inc(s-c[2]*(d2+1)-c[4]*(d4+1));
		inc(s-c[3]*(d3+1)-c[4]*(d4+1));
		dec(s-c[1]*(d1+1)-c[2]*(d2+1)-c[3]*(d3+1));
		dec(s-c[1]*(d1+1)-c[2]*(d2+1)-c[4]*(d4+1));
		dec(s-c[1]*(d1+1)-c[3]*(d3+1)-c[4]*(d4+1));
		dec(s-c[2]*(d2+1)-c[3]*(d3+1)-c[4]*(d4+1));
		inc(s-c[1]*(d1+1)-c[2]*(d2+1)-c[3]*(d3+1)-c[4]*(d4+1));
		#undef inc
		#undef dec
		fout<<ans<<endl;
	}
	return 0;
}
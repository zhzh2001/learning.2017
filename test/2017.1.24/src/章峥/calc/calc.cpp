#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("calc.in");
ofstream fout("calc.out");
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
int p;
long long qmod(long long a,int b)
{
	long long ans=1,now=a;
	do
	{
		if(b&1)
			ans=(ans*now)%p;
		now=(now*now)%p;
	}
	while(b>>=1);
	return ans;
}
int g;
void exgcd(int a,int b,int& x,int& y)
{
	if(!b){g=a;x=1;y=0;}
	else{exgcd(b,a%b,y,x);y-=x*(a/b);}
}
int main()
{
	int x,y,a,b;
	fin>>x>>y>>a>>b>>p;
	int g=gcd(x,y);
	long long s1=((long long)x*y/g+g)%p;
	int s2=qmod(x,y);
	long long s3;
	if(a==0)
		s3=b%p;
	else
	{
		//f(x)=(a^(x-1)-1)/(a-1)*b+a^(x-1)*y
		int inv,y0;
		exgcd(a-1,p,inv,y0);
		inv=(inv%p+p)%p;
		s3=((qmod(a,x-1)-1+p)%p*inv%p*b%p+qmod(a,x-1)*y%p)%p;
	}
	fout<<s1*s2%p*s3%p<<endl;
	return 0;
}
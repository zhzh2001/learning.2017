#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("calc.in");
ofstream fout("calc.out");
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
int p;
template<typename T>
inline T qmod(T a,int b)
{
	T ans=1,now=a;
	do
	{
		if(b&1)
			ans=(ans*now)%p;
		now=(now*now)%p;
	}
	while(b>>=1);
	return ans;
}
struct matrix
{
	static const int n=2;
	long long mat[n][n];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix(const int x)
	{
		if(x==1)
		{
			mat[0][0]=mat[1][1]=1;
			mat[0][1]=mat[1][0]=0;
		}
	}
	matrix(long long a,long long b,long long c,long long d)
	{
		mat[0][0]=a;
		mat[0][1]=b;
		mat[1][0]=c;
		mat[1][1]=d;
	}
	matrix operator*(const matrix& b)const
	{
		matrix res;
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				for(int k=0;k<n;k++)
					res.mat[i][j]=(res.mat[i][j]+mat[i][k]*b.mat[k][j])%p;
		return res;
	}
	matrix operator%(const int x)const
	{
		return *this;
	}
};
int main()
{
	int x,y,a,b;
	fin>>x>>y>>a>>b>>p;
	int g=gcd(x,y);
	long long s1=((long long)x*y/g+g)%p;
	int s2=qmod((long long)x,y);
	long long s3;
	if(a==0)
		s3=b%p;
	else
		if(b==0)
			s3=(qmod((long long)a,x-1)*y)%p;
		else
		{
			//f(x)=(a^(x-1)-1)/(a-1)*b+a^(x-1)*y
			matrix ans=qmod(matrix(a,1,0,1),x-1);
			s3=(ans.mat[0][0]*y+ans.mat[0][1]*b)%p;
		}
	fout<<((s1*s2)%p*s3)%p<<endl;
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("calc.in");
ofstream fout("calc.out");
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
inline int qmod(int a,int b,int p)
{
	long long ans=1,now=a;
	do
	{
		if(b&1)
			ans=(ans*now)%p;
		now=(now*now)%p;
	}
	while(b>>=1);
	return ans;
}
int main()
{
	int x,y,a,b,p;
	fin>>x>>y>>a>>b>>p;
	int g=gcd(x,y);
	long long s1=((long long)x*y/g+g)%p;
	int s2=qmod(x,y,p);
	long long s3;
	//f(x)=(a^(x-1)-1)/(a-1)*b+a^(x-1)*y
	s3=y;
	for(int i=2;i<=x;i++)
		s3=(s3*a+b)%p;
	fout<<((s1*s2)%p*s3)%p<<endl;
	return 0;
}
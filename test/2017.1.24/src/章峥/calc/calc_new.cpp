#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("calc.in");
ofstream fout("calc.out");
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
long long qmod(long long a,int b,long long p)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b>>=1);
	return ans;
}
int main()
{
	int x,y,a,b,p;
	fin>>x>>y>>a>>b>>p;
	int g=gcd(x,y);
	long long s1=((long long)x*y/g+g)%p;
	int s2=qmod(x,y,p);
	//f(x)=(a^(x-1)-1)/(a-1)*b+a^(x-1)*y
	long long p0=(long long)(a-1)*p,s3=((qmod(a,x-1,p0)-1+p0)%p0*b%p0/(a-1)%p+qmod(a,x-1,p)*y%p)%p;
	fout<<s1*s2%p*s3%p<<endl;
	return 0;
}
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);
	puts("");
}
struct data{
	ll d[2][2];
}f,g,ans,temp;
ll a,b,x,y,p,s1,s2,s3;
ll gcd(ll x,ll y){return y?gcd(y,x%y):x;}
void work1(){
	ll t=gcd(x,y);
	s1=(t%p)+(x*y/t)%p;
}
ll pow(ll x,ll y,ll p){
	if (y==1)	return x%p;
	ll t=pow(x,y/2,p);
	if (y%2)	return ((t*t)%p*x)%p;
	else		return (t*t)%p;
}
void work2(){
	s2=pow(x,y,p);
}
void cheng(data &a,data b){
	ans.d[0][0]=0;	ans.d[1][0]=0;	ans.d[0][1]=0;	ans.d[1][1]=0;	
	for (ll i=0;i<2;i++)
	for (ll j=0;j<2;j++)
	for (ll k=0;k<2;k++)	ans.d[i][j]=(ans.d[i][j]+a.d[i][k]*b.d[k][j])%p;
	a.d[0][0]=ans.d[0][0];	a.d[1][0]=ans.d[1][0];	a.d[0][1]=ans.d[0][1];	a.d[1][1]=ans.d[1][1];
}
void work3(){
	f.d[0][0]=y;	
	f.d[0][1]=1;
	g.d[0][0]=a;
	g.d[1][0]=b;
	g.d[1][1]=1;
	x--;
	for (ll k=0;k<=40;k++){
		if (x&(1LL<<k))	cheng(f,g);
		temp.d[0][0]=g.d[0][0];	temp.d[1][0]=g.d[1][0];	temp.d[0][1]=g.d[0][1];	temp.d[1][1]=g.d[1][1];	
		cheng(g,temp);
	}
	s3=f.d[0][0];
}
int main(){
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	x=read();	y=read();	a=read();	b=read();	p=read();
	work1();
	work2();
	work3();
	writeln(((s1*s2)%p*s3)%p);
}

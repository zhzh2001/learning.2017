#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#define ll int
#define max(x,y)	x>y?x:y
//using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);
	puts("");
}
ll a[5005][5005],ans,n,m,t,r,x,y,v;
int main(){
	freopen("bomb.in","r",stdin);
	freopen("bomb.out","w",stdout); 
	t=read();	r=read();
	while (t--){
		x=read()+1,y=read()+1,v=read();
		a[x][y]+=v;
		n=max(n,x);
		m=max(m,y);
	}
	t=0;
	for (ll i=1;i<=n;i++)
	for (ll j=1;j<=m;j++){
		a[i][j]+=a[i-1][j]+a[i][j-1]-a[i-1][j-1];
		ans=max(ans,a[i][j]+a[max(t,i-r)][max(t,j-r)]-a[max(t,i-r)][j]-a[i][max(t,j-r)]);
	}
	writeln(ans);
}

///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     ���汣��      RP++                        //
///////////////////////////////////////////////////////////////////

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);
	puts("");
}
ll ans,tot,d[10],c[10],f[100010],s;
void dfs(ll x,ll k,ll remain){
	if (remain<0)	return;
	if (x==5){
		if (k%2)	ans-=f[remain];
		else		ans+=f[remain];
		return;
	}
	dfs(x+1,k,remain);
	dfs(x+1,k+1,remain-(d[x]+1)*c[x]);	
}
int main(){
	freopen("coins.in","r",stdin);
	freopen("coins.out","w",stdout);
	c[1]=read();	c[2]=read();	c[3]=read();	c[4]=read();	tot=read();
	f[0]=1;
	for (ll i=1;i<=4;i++)
	for (ll j=0;j<=100000-c[i];j++)	f[j+c[i]]+=f[j]; 
	while (tot--){
		d[1]=read();	d[2]=read();	d[3]=read();	d[4]=read();
		s=read();
		ans=0;
		dfs(1,0,s);
		writeln(ans);
	}
}

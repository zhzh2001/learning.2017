#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
#include<cstdlib>
#include<cmath>
using namespace std;
#define LL long long 
LL p,a,b,x,y;
LL FFF(LL a,LL b,LL x,LL y)
{
	LL f[2];
	f[1]=y%p;
	for(int i=2;i<=x;i++)
		f[i%2]=(f[(i+1)%2]*a+b)%p;
	return f[x%2];
}
LL mi(LL x,LL n)
{
	if(n==1)
		return x%p;
	LL o=mi(x,n/2)%p;
	o=o*o%p;
	if(n%2!=0)
		o=o*x%p;
	return o;
}
LL gcd(LL a,LL b)
{
	if(a%b==0)
		return b;
	return gcd(b,a%b);
}
int main()
{
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	cin>>x>>y>>a>>b>>p;
	LL s1,s2,s3;
	LL q=x,l=y;
	if(q<l)
		swap(q,l);
	s1=((q*l+1)/gcd(q,l))%p;
	s2=mi(x,y);
	s3=FFF(a,b,x,y);
	LL ans=s1*s2*s3%p;
	cout<<ans<<endl;
	return 0;
}

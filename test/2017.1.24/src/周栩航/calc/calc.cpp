#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
struct Mat
 {
     ll mat[3][3];
 };
 ll x,y,a,b,p;
 Mat operator * (Mat a,Mat b)
  {
     Mat c;
     memset(c.mat,0,sizeof(c.mat));
    int i,j,k;
     for(k=1;k<3;k++)
     {
         for(i=1;i<3;i++)
         {
             if(a.mat[i][k]==0) continue;
             for(j=1;j<3;j++)
             {
                 if(b.mat[k][j]==0) continue;
                 c.mat[i][j]=(c.mat[i][j]+(a.mat[i][k]*b.mat[k][j])%p)%p;
             }
         }
     }
     return c;
 }
 Mat operator ^(Mat a,int k)
 {
     Mat c;
     int i,j;
     for(i=1;i<3;i++)
  	     for(j=1;j<3;j++)
         c.mat[i][j]=(i==j);
     for(;k;k>>=1)
     {
         if(k&1) c=c*a;
         a=a*a;
 	  }
     return c;
 }
inline int gcd(int x,int y)
{
	return 	x%y==0?y:gcd(y,x%y);
}
inline ll qmi(int x,int y,int mo)
{
	if (y==1)	return x;
	if (y==0)	return 1;
	int t=qmi(x,y/2,mo);
	if (y%2==0)
		return ((t)%mo)*((t)%mo)%mo;
	else
		return ((((t)%mo)*((t)%mo)%mo)*x)%mo;
}	

int 	sum[10000001];
int	h[2000014]={-1};
inline bool hash(int x)
{
	int t=x%1000007;
	while (h[t]==-1&&h[t]!=x)	t++;
	if (h[t]==x)	return 1;
	h[t]=x;
	return 0;
}

inline int  gs()
{
	Mat jza;
	Mat jzb;
	jza.mat[1][1]=a;
	jza.mat[1][2]=b;
	jza.mat[2][1]=0;
	jza.mat[2][2]=1;
	jzb.mat[1][1]=y;
	jzb.mat[2][1]=1;
	jzb.mat[1][2]=0;
	jzb.mat[2][2]=0;
	Mat ansjz;
	jzb=(jza^(x-1))*jzb;
	return jzb.mat[1][1];
}
int main()
{
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	cin>>x>>y>>a>>b>>p;
	
	int tmp=gcd(x,y);
	int s1=(tmp+x*y/tmp)%p;
	int s2=qmi(x,y,p)%p;
	ll ans=(((s1%p)*(s2%p))%p)*gs()%p;
	cout<<ans<<endl;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
inline int gcd(int x,int y)
{
	return 	x%y==0?y:gcd(y,x%y);
}
inline ll qmi(int x,int y,int mo)
{
	if (y==1)	return x;
	if (y==0)	return 1;
	int t=qmi(x,y/2,mo);
	if (y%2==0)
		return ((t)%mo)*((t)%mo)%mo;
	else
		return ((((t)%mo)*((t)%mo)%mo)*x)%mo;
}	

int 	sum[10000001];
int	h[2000014]={-1};
inline bool hash(int x)
{
	int t=x%1000007;
	while (h[t]==-1&&h[t]!=x)	t++;
	if (h[t]==x)	return 1;
	h[t]=x;
	return 0;
}
ll x,y,a,b,p;
inline int  gs()
{
	int s3_1=0,s3_2=0,bj=20000000;
	int ts=1;
	if (x==1)	return y;
	for (int i=1;i<=x-2;i++)
	{
		ts*=a;
		int bs=ts%p;
		sum[i]=(sum[i-1]+bs)%p;
		if (hash(bs))
		{
			s3_1=((x-2)/i)%p*sum[i];
			bj=i;
		}
	}
	s3_1=(((s3_1+sum[(x-2)%bj])%p)*(b%p))%p;
	s3_2=qmi(a,x-1,p)*(y%p)%p;
	return s3_2+s3_1+b;
}
int main()
{
//	freopen("calc.in","r",stdin);
//	freopen("calc.out","w",stdout);
	cin>>x>>y>>a>>b>>p;
	
	int tmp=gcd(x,y);
	int s1=(tmp+x*y/tmp)%p;
	int s2=qmi(x,y,p)%p;
	ll ans=(((s1%p)*(s2%p))%p)*gs()%p;
	cout<<ans<<endl;
}

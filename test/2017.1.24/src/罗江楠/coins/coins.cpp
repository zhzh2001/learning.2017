///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                    ���汣��       RP++                        //
///////////////////////////////////////////////////////////////////
#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define N 100000
using namespace std;

int tot, s, c[5], d[5];
ll ans, f[N+5];

void dfs(int x, int k, int s){
	if(s < 0) return;
	if(x == 5){
		if(k&1) ans -= s[f];
		else ans += s[f];
		return;
	}
	dfs(x+1, k, s);
	dfs(x+1, k+1, s-(x[d]+1)*x[c]);
}

int main(){
	freopen("coins.in", "r", stdin);
	freopen("coins.out", "w", stdout);
	scanf("%d%d%d%d%d", &c[1], &c[2], &c[3], &c[4], &tot);
	f[0] = 1;
	for(int i = 1; i <= 4; i++)
		for(int j = 0; j <= N - i[c]; j++)
			f[j + i[c]] = f[j + i[c]] + f[j];
	for(int i = 0; i < tot; i++){
		scanf("%d%d%d%d%d", &d[1], &d[2], &d[3], &d[4], &s);
		ans = 0, dfs(1, 0, s);
		printf("%I64d\n", ans);
	}
	return 0;
}


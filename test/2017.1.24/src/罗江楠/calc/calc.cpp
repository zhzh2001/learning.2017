///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                    ���汣��       RP++                        //
///////////////////////////////////////////////////////////////////
#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define MOD 1000000007
using namespace std;

ll x, y, a, b, p;

ll sqr(ll a){
	a %= p;
	return a * a % p;
}

ll gcd(ll a, ll b){
	return a % b ? gcd(b, a % b) : b;
}

ll S1(){
	ll g = gcd(x, y);
	return (g + x / g * y) % p;
}

ll mi(ll x, ll y){
	if(y == 1) return x;
	if(y == 0) return 1;
	if(y&1) return x * sqr(mi(x, y / 2)) % p;
	return sqr(mi(x, y / 2)) % p;
}

ll S2(){
	return mi(x, y) % p;
}

ll S3(){
	if(a==0) return 0;
	if(a==1) return (y + (x - 1) * b) % p;
	ll as = mi(a, x - 1);
	return (as * y % p + (as - 1) / (a - 1) * b % p) % p;
}

int main(){
	freopen("calc.in", "r", stdin);
	freopen("calc.out", "w", stdout);
	scanf("%I64d%I64d%I64d%I64d%I64d", &x, &y, &a, &b, &p);
	printf("%I64d\n", (S1() * S2() % p) * S3() % p);
	return 0;
}


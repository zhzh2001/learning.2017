const n=4;
var
        t,tot,i,j,k,m:longint;
        c,d:array[1..4] of longint;
        f,s:array[0..100000] of longint;
function min(a,b:longint):longint;
begin
        if a<b then exit(a)
        else exit(b);
end;
begin
        assign(input,'coins.in');
        assign(output,'coins.out');
        reset(input); rewrite(output);
        read(c[1],c[2],c[3],c[4],tot);
        for t:=1 to tot do
        begin
                fillchar(f,sizeof(f),0);
                f[0]:=1;
                read(d[1],d[2],d[3],d[4],m);
                for i:=1 to min(d[1],m div c[1]) do
                        f[i*c[1]]:=1;
                for i:=2 to n do
                begin
                        s[0]:=1;
                        for j:=1 to m do
                                if j-c[i]>=0 then s[j]:=s[j-c[i]]+f[j]
                                else s[j]:=f[j];
                        for j:=m downto 0 do
                                if (j-(d[i]+1)*c[i]>=0) then f[j]:=s[j]-s[j-(d[i]+1)*c[i]]
                                else f[j]:=s[j];
                end;
                writeln(f[m]);
        end;
        close(input); close(output);
end.

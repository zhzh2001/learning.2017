var
        x,y,a,b,p,ans:qword;
function gcd(x,y:qword):qword;
begin
        if x mod y=0 then exit(y)
        else exit(gcd(y,x mod y));
end;
function s1:qword;
var
        t:qword;
begin
        t:=gcd(x,y) mod p;
        s1:=((x*y div t)+t) mod p;
end;
function qpow(x,y:qword):qword;
var
        num:qword;
begin
        qpow:=1; num:=x mod p;
        while y>0 do
        begin
                if (y mod 2=1) then qpow:=(qpow*num) mod p;
                num:=(num*num) mod p;
                y:=y div 2;
        end;
end;
function s2:qword;
begin
        exit(qpow(x,y));
end;
function s3:qword;
var
        i:longint;
begin
        if a=0 then if x=1 then exit(y mod p)
                        else exit(b mod p);
        if b=0 then exit((qpow(a,x-1)*y) mod p)
        else
        begin
                s3:=y mod p;
                for i:=2 to x do
                        s3:=(s3*a+b) mod p;
        end;
end;
begin
        assign(input,'calc.in');
        assign(output,'calc.out');
        reset(input); rewrite(output);
        read(x,y,a,b,p);
        ans:=(((s1*s2) mod p)*s3) mod p;
        write(ans);
        close(input); close(output);
end.

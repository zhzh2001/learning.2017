#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;
typedef long long ll;
ll len, n, h, ans; 
ll data[65];
bool t;
char ch;

int main(){
	freopen("fast.in", "r", stdin);
	freopen("fast.out", "w", stdout);
	scanf("%d", &len);
	getchar();
	for (ll i = 1; i <= len; i++){
		ch = getchar(); 
		n = n * 2 + ch - '0';
	}
	h = 1; data[h] = 1; ans = 0;
	for (ll i = 2; i <= n; i++){
		data[++h] = 1;
		while (h > 1 && data[h] == data[h-1]){
			data[h-1] = data[h-1] + data[h]; 
			h--, ans++;
		}
	}
	cout << ans;
	return 0;
}

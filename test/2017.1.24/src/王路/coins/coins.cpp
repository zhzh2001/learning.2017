#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>

#define Rep(x,i,j) for (long long x = i; x <= j; x++)
const int maxn = 100005;
using namespace std;
typedef long long ll;

ll x[5], y[5], t, num, ans;
ll f[maxn];

inline void inc(int x){
	if (x >= 0) ans += f[x];
}
inline void dec(int x){
	if (x >= 0) ans -= f[x];
}
int main()
{
	freopen("coins.in", "r", stdin); freopen("coins.out", "w", stdout);
	Rep(i,1,4) scanf("%d", &x[i]);
	scanf("%d", &t); f[0] = 1;
	Rep(i,1,4) Rep(j,x[i], maxn-1) f[j] += f[j-x[i]];
	while(t--)
	{
		Rep(i,1,4) scanf("%d", &y[i]); scanf("%d", &num);
		ans=f[num]; 
		Rep(i,1,4) dec(num-(y[i]+1)*x[i]);
		inc(num-(y[1]+1)*x[1]-(y[2]+1)*x[2]);
		inc(num-(y[1]+1)*x[1]-(y[3]+1)*x[3]);
		inc(num-(y[1]+1)*x[1]-(y[4]+1)*x[4]);
		inc(num-(y[2]+1)*x[2]-(y[3]+1)*x[3]);
		inc(num-(y[2]+1)*x[2]-(y[4]+1)*x[4]);
		inc(num-(y[3]+1)*x[3]-(y[4]+1)*x[4]);
		dec(num-(y[1]+1)*x[1]-(y[2]+1)*x[2]-(y[3]+1)*x[3]);
		dec(num-(y[1]+1)*x[1]-(y[2]+1)*x[2]-(y[4]+1)*x[4]);
		dec(num-(y[1]+1)*x[1]-(y[3]+1)*x[3]-(y[4]+1)*x[4]);
		dec(num-(y[2]+1)*x[2]-(y[3]+1)*x[3]-(y[4]+1)*x[4]);
		inc(num-(y[1]+1)*x[1]-(y[2]+1)*x[2]-(y[3]+1)*x[3]-(y[4]+1)*x[4]);
		printf("%I64d\n", ans);
	}
	return 0;
}

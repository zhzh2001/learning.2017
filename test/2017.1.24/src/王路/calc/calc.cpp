#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>

#define Rep(x,i,j) for (int x = i; x <= j; x++)

using namespace std;
typedef long long ll;
ll x, y, a, b, p, s1, s2 = 1, s3, t, ans;

ll gcd(ll a, ll b){
	if((a == 0)||(b == 0))
		return a + b;
	return gcd(b, a % b);
}

ll lcm(ll a, ll b){
	ll k = gcd(a,b);
	return (a/k)*(b);
}

ll f(ll i){
	if (a == 0) return b % p;
	if (i == 1) return y;
	return (a*(f(i-1) %p) + b)%p;
}

ll fm(ll a, ll b){ //������ 
	if (a == 0) return 0;
	if (b == 0) return 1;
	if (b == 1){
		return a;
	}
	if (b % 2 == 1){
		return (fm(a,b/2) % p) * (fm(a,b/2) % p) * a %p;
	} else {
		return (fm(a,b/2) % p) * (fm(a,b/2) % p) % p;
	}
}

int main(){
	freopen("calc.in", "r", stdin);
	freopen("calc.out", "w", stdout);
	scanf("%d%d%d%d%d", &x, &y, &a, &b, &p);
//	x %= p, y %= p, a %= p, b %= p;
	ll tx = x; ll ty = y;
	if (x > y){
		tx = y; ty = x;
	}
	s1 = gcd(tx,ty) % p; t = lcm(tx,ty) % p; s1 += t; s1 %= p; s2 = 1;
//	cout << s1 << endl;
//	cout << gcd(x,y) << " " << lcm(x,y) << endl;
	s2 = fm(x,y) % p;
//	cout << s2 << endl;
	s3 = f(x) % p;
//	cout << s3 << endl;
	t = ans = s1 % p;
	t = s2 % p; ans *= t; ans %= p;
	t = s3 % p; ans *= t; ans %= p;
	printf("%I64d", ans);
}

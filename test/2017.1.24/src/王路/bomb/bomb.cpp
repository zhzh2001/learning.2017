#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <cstring>

using namespace std;
typedef long long ll;
const int maxn = 10005;
const int maxr = 1005;
const int maxm = 5005;

unsigned short n, r, x, y, v, tmp, ans, xxx, yyy;
unsigned short map[maxm][maxm];

int main(){
	freopen("bomb.in", "r", stdin);
	freopen("bomb.out", "w", stdout);
	scanf("%d%d", &n, &r);
	for (int i = 1; i <= n; i++){
		scanf("%d%d%d", &x, &y, &v);
		map[x][y] = v;
		if (x > xxx) xxx = x;
		if (y > yyy) yyy = y;
	}
	if (r == 0) { printf("0\n");}
	if (r == 1) {
		if (n == 0){ printf("0\n");}
		if (n != 0){ printf("1\n");}
		return 0;
	}
	for (int i = 0; i < xxx; i++){
		for (int j = 0; j < yyy; j++){
			for (int k = i+1; k < i + r; k++) for (int l = j+1; l < j + r; l++){
				tmp += map[k][l];
			}
			if (tmp > ans) ans = tmp;
		}
	}
	cout << ans << endl;
	return 0;
}

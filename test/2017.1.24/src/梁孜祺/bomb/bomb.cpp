#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<set>
#include<cmath>
using namespace std;
int a[5010][5010]={0},f[5010][5010]={0};
int ff[5010][5010]={0};
int main(){
	freopen("bomb.in","r",stdin);
	freopen("bomb.out","w",stdout);
	int i,j,n,r;
	int maxx=-1000,maxy=-1000;
	scanf("%d%d",&n,&r);
	for(i=1;i<=n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x+1][y+1]=z;
		if(x+1>maxx) maxx=x+1;
		if(y+1>maxy) maxy=y+1;
	}
	for(i=1;i<=maxx;i++)
		for(j=1;j<=maxy;j++)
			f[i][j]=f[i-1][j]+f[i][j-1]-f[i-1][j-1]+a[i][j];
	for(i=1;i<=maxx;i++){
		if(i+r-2>maxx) break;
		for(j=1;j<=maxy;j++){
			if(j+r-2>maxy) break;
			ff[i][j]=f[i+r-1][j+r-1]-f[i+r-1][j-1]-f[i-1][j+r-1]+f[i-1][j-1];
		}
	}
	int maxn=-10000;
	for(i=1;i<=maxx-r+5;i++)
		for(j=1;j<=maxy-r+5;j++)
			maxn=max(maxn,ff[i][j]);
	printf("%d",maxn);
	return 0;
}

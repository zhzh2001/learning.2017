#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<set>
#include<cmath>
#define ll long long
using namespace std;
ll s1=0,s2=0,s3=0,py;

inline ll gcd(ll x,ll y){
	if(y==0) return x;
	else return gcd(y,x%y);
}

inline ll kuaisu(int x,int y,int p){
	if(y==1) return x;
	ll sum=1;
	if(y%2==1) sum=x;
	ll q=kuaisu(x,y/2,p);
	sum=sum*q%p*q%p;
	return sum%p;
}

inline ll chuli(int x,int y,int a,int b,int p){
	ll q=kuaisu(a,x-1,p);
	ll sum=q*y%p;
	b=b%p;
	sum=sum+b*(q-1)/(a-1);
	return sum%p;
}
int main(){
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	int x,y,a,b,p,gyf;
	scanf("%d%d%d%d%d",&x,&y,&a,&b,&p);
	gyf=gcd(x,y);
	py=x/gyf%p*y%p;
	s1=(gyf+py)%p;
	s2=kuaisu(x%p,y,p);
	if(a==0) s3=b%p;
	s3=chuli(x,y,a,b,p);
	ll ans=(s1%p)*(s2%p)*(s3%p)%p;
	printf("%I64d\n",ans);
	return 0;
}

#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
long long x,y,a,b,p,k,s1,s2,s3,lsg[3][3],f[3][3],d[3][3],c[3][3];
long long gcd(long long x,long long y)
	{
		if (x>y) return gcd(y,x);
		if (x==0) return y;
		return gcd(y%x,x);
	}
long long ksm(long long x,long long y)
	{
		if (y==0) return 1;
		if (y==1) return x;
		long long lsg=ksm(x,y/2);
		if (y&1) return lsg*lsg%p*x%p;
			else return lsg*lsg%p;
	}
void put(long long x[3][3])
	{
		c[1][1]=x[1][1]%p;
		c[1][2]=x[1][2]%p;
		c[2][1]=x[2][1]%p;
		c[2][2]=x[2][2]%p;
	}
void jzcf(long long lsg[3][3])
	{
		long long x[3][3];
		x[1][1]=c[1][1]*lsg[1][1]%p+c[1][2]*lsg[2][1]%p;//cout<<x[1][1]<<endl;
		x[2][1]=c[2][1]*lsg[1][1]%p+c[2][2]*lsg[2][1]%p;//cout<<x[2][1]<<endl;
		x[1][2]=c[1][1]*lsg[1][2]%p+c[1][2]*lsg[2][2]%p;//cout<<x[1][2]<<endl;
		x[2][2]=c[2][1]*lsg[1][2]%p+c[2][2]*lsg[2][2]%p;//cout<<x[2][2]<<endl;
		put(x);
	}
void jzksm(long long k)
	{
		if(k==0) {put(d);return;}
		if(k==1) {put(f);return;}
		jzksm(k/2);
		if (k&1) jzcf(c),jzcf(f);
			else jzcf(c);
	} 
int main()
{
	freopen("calc.in","r",stdin);freopen("calc.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>x>>y>>a>>b>>p;
	f[1][1]=a;f[2][1]=b;f[2][2]=1;
	d[1][2]=1;d[2][1]=1;
	k=gcd(x,y);
	s1=(k+x/k*y)%p;
	s2=ksm(x,y)%p;
	jzksm(x-1);
	lsg[1][1]=y;lsg[1][2]=1;
	s3=(c[1][1]*lsg[1][1]%p+c[2][1]*lsg[1][2]%p)%p;
	cout<<s1*s2%p*s3%p;
}

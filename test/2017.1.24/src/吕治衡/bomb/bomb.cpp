#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
int n,r,ans,x,y,z,a[5050][5050],ll,rr;
inline int max(int x,int y)
	{
		if (x>y) return x;
			else return y;
	}
inline int min(int x,int y)
	{
		if (x>y) return y;
			else return x;
	}
inline int read()
{
	int a=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	a=a*10+ch-'0';
	ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		a=a*10+ch-'0';
		ch=getchar();
	}
	return a;
}
int main()
{
	freopen("bomb.in","r",stdin);freopen("bomb.out","w",stdout);
	n=read();r=read();
	ll=1000000;rr=-1000000;
	for (int i=1;i<=n;i++)
		{
			x=read()+1;y=read()+1;z=read();
			a[x][y]+=z;ll=min(ll,min(x,y));rr=max(rr,min(x,y));
		}
	for (int i=ll;i<=rr;i++)
		for (int j=ll;j<=rr;j++)a[i][j]+=a[i-1][j]+a[i][j-1]-a[i-1][j-1];
	for (int i=rr;i>=ll;i--)
		for (int j=rr;j>=ll;j--)
			{
				a[i][j]+=a[max(0,i-r)][max(0,j-r)]-a[i][max(0,j-r)]-a[max(0,i-r)][j];
				ans=max(ans,a[i][j]);
			}
	cout<<ans<<endl;
} 

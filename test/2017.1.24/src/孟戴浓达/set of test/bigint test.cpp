#include<fstream>
#include<vector>
#include<cstring>
#include<algorithm>
#include<iostream>
using namespace std;
struct bigint{
	int len,dig[10000];
	bigint& operator=(int x){
		len=1;
		memset(dig,0,sizeof(dig));
		do{
			dig[len++]=x%10;
		}while(x/=10);
		return *this;
	}
	bigint& operator*=(const int x){
		int overf=0,i;
		for(i=1;i<=len||overf;i++){
			dig[i]=overf+dig[i]*x;
			overf=dig[i]/10;
			dig[i]%=10;
		}
		len=i-1;
		return *this;
	}
	bigint& operator+=(const bigint& b){
		len=max(len,b.len);
		int overf=0;
		for(int i=1;i<=len;i++){
			dig[i]+=b.dig[i]+overf;
			overf=dig[i]/10;
			dig[i]%=10;
		}
		if(dig[len+1]>0){
			len++;
		}
		return *this;
	}
};
ostream& operator<<(ostream& os,const bigint& b){
	for(int i=b.len;i>=1;i--){
		os<<b.dig[i];
	}
	return os;
}
int main(){
	int n,m;
	bigint a,b;
	b=9;
	a=9;
	b+=a;
	cout<<b;
	return 0;
}

#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("bomb.in");
ofstream fout("bomb.out");
int maxx,maxy,n,r,x,y,val;
short sum[5002][5002];
short tu[5002][5002];
short ans;
int main(){
	fin>>n>>r;
	for(int i=1;i<=n;i++){
		fin>>x>>y>>val;
		x++;
		y++;
		tu[x][y]=val;
		maxx=max(maxx,x);
		maxy=max(maxy,y);
	}
	for(int i=1;i<=maxx;i++){
		for(int j=1;j<=maxy;j++){
			sum[i][j]=sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+tu[i][j];
		}
	}
	ans=0;
	if(r<=maxx&&r<=maxy){
		for(int i=r;i<=maxx;i++){
			for(int j=r;j<=maxy;j++){
				ans=max(ans,sum[i][j]-sum[i-r][j]-sum[i][j-r]+sum[i-r][j-r]);
			}
		}
	}else{
		if(r<=maxx&&r>=maxy){
			for(int i=r;i<=maxx;i++){
				ans=max(ans,sum[i][maxy]-sum[i-r][maxy]);
			}
		}else{
			if(r>=maxx&&r<=maxy){
				for(int j=r;j<=maxy;j++){
					ans=max(ans,sum[maxx][j]-sum[maxx][j-r]);
				}
			}else{
				fout<<sum[maxx][maxy];
				fin.close();
				fout.close();
				return 0;
			}
		}
	}
	fout<<ans;
	fin.close();
	fout.close();
	return 0;
}
/*
2 1
0 0 1
1 1 2
*/

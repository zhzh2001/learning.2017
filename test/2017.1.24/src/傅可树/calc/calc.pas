var k,x,y,a,b,p,s1,s2,s3:longint;
function make:longint;
var i:longint;
begin
make:=y mod p;
for i:=2 to x do make:=(make mod p * (a mod p) mod p+b mod p)mod p;
end;
function gcd(x,y:longint):longint;
begin
if y=0 then exit(x);
gcd:=gcd(y,x mod y);
end;
function ks(x,y:longint):longint;
begin
if y=1 then exit(x);
if y mod 2=0 then ks:=(ks(x,y div 2) mod p)*(ks(x,y div 2) mod p) mod p
	     else ks:=(ks(x,y div 2) mod p)*(ks(x,y div 2) mod p) mod p*(x mod p) mod p;
end;
begin
assign(input,'calc.in');
assign(output,'calc.out');
reset(input);
rewrite(output);
readln(x,y,a,b,p);
k:=gcd(x,y);
k:=k mod p;
s1:=((x div k mod p)*(y mod p)mod p + k)mod p;
k:=ks(x,y);
s2:=k mod p;
if (a=0)and(b=0) then s3:=0
                 else
		begin
		if a=0 then s3:=b mod p;
		if b=0 then s3:=ks(a,x-1)*y mod p;
		if (a<>0)and(b<>0) then s3:=make;
		end;
writeln((s1 mod p)*(s2 mod p) mod p*(s3 mod p) mod p);
close(input);
close(output);
end.

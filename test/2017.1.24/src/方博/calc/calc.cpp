#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#define ll long long
using namespace std;
ll s1,s2,s3;
ll x,y,a,b,p,k;
ll hash[5000000];
ll t[5000000];
ll qm(ll a,ll k)
{
	if(k==1)return a%p;
	int xx;
	xx=qm(a,k/2);
	xx=(xx*xx)%p;
	if(k%2!=0)xx=(xx*a)%p;
	return xx;
}
ll gcd(ll a,ll b)
{
	if(a<b)return gcd(b,a);
	ll xx;
	xx=a%b;
	if(xx==0)return b;
	 else return gcd(b,xx);
}
bool check(int k)
{
	int xx=k%4295124;
	while(hash[xx]!=k&&hash[xx]!=-1)xx=xx%4295124+1;
	if(hash[xx]==k)return true;
	 else {
	 	hash[xx]=k;
	 	return false;
	 }
}
ll getans()
{
	int i;
	int xx=y;
	t[1]=y;
	t[0]=1;
	for(i=2;i<=x;i++){
		y=(y*a%p+b)%p;
		t[0]++;
		t[i]=y;
		if(check(y))break;
	}
	if(i==x+1)return t[x];
	 else {
	 	for(int j=1;j<=t[0];j++)
	 	 if(t[j]==t[i])return t[j+(x-j)%(i-j)];
	 }
}
int main()
{
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	cin>>x>>y>>a>>b>>p;
	memset(hash,-1,sizeof(hash));
	int xx=gcd(x,y);
	s1=(xx+x/xx*y)%p;
//	cout<<s1<<endl;
	s2=qm(x,y);
//	cout<<s2<<endl;
	s3=getans();
//	cout<<s3<<endl;
	cout<<s1*s2%p*s3%p<<endl;
	return 0;
}

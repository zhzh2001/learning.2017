#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;
long long  gcd(long long numx,long long numy){
    if (numx%numy==0)
		return(numy);
    else
		return(gcd(numy,numx%numy));
}
long long x,y,a,b,p,s1=0,s2=0,s3=0;
int main(){
	freopen("calc.in","r",stdin);
    freopen("calc.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>x>>y>>a>>b>>p;
	long long x1=x,y1=y,numz,numzz,ans;
	if (y1>x1)
		swap(x1,y1);
	numz=gcd(x1,y1)%p;
	y1/=numz;
	numzz=((x1%p)*(y1%p))%p;
	s1=numz%p+numzz%p;
	s2=x%p;
	for (int i=1;i<=y-1;i++){
		s2*=x;
		s2=s2%p;
	}
	s3=y%p;
	for (int i=2;i<=x;i++)
		s3=((s3*a%p)+b%p)%p;
	ans=((((s1%p)*(s2%p))%p)*(s3%p))%p;
	cout<<ans;
}

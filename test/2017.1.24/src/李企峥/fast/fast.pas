var a,b,ans:array[0..1000]of longint;
    len,i,j,x:longint;
    ch:char;
begin
assign(input,'fast.in');
assign(output,'fast.out');
reset(input);
rewrite(output);
readln(len);
for i:=1 to len do
    begin
    read(ch);
    a[len-i+1]:=ord(ch)-ord('0');
    end;
for i:=1 to len do
    b[i]:=0;
for i:=2 to len do
    begin
    for j:=i to len do
        begin
        b[j-i+1]:=b[j-i+1]+a[j];
        x:=j-i+1;
        while b[x]>=2 do
                      begin
                      b[x+1]:=b[x+1]+b[x] div 2;
                      b[x]:=b[x] mod 2;
                      inc(x);
                      end;
        end;
    end;
ans[0]:=1;
for i:=len downto 1 do
    begin
    for j:=1 to ans[0] do
        ans[j]:=ans[j]*2;
    ans[1]:=ans[1]+b[i];
    x:=1;
    while ans[x]>1000 do
          begin
          ans[x+1]:=ans[x+1]+ans[x] div 1000;
          ans[x]:=ans[x] mod 1000;
          inc(x);
          end;
    if x>ans[0] then ans[0]:=x;
    end;
x:=ans[0];
write(ans[x]);
for i:=x-1 downto 1 do
      begin
      if ans[i]<100 then write(0);
      if ans[i]<10 then write(0);
      write(ans[i]);
      end;
close(input);
close(output);
end.

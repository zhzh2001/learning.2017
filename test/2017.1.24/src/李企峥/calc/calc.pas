var x,y,a,b,p:longint;
    s1,s2,s3,ans:qword;
function gcd(x,y:longint):longint;
begin
if y=0 then exit(x)
       else exit(gcd(y,x mod y));
end;
function kscm(x,y:longint):qword;
var z,fh:qword;
begin
z:=x mod p;
if y mod 2=1 then fh:=z;
y:=y div 2;
z:=z*z mod p;
while y>0 do
      begin
      if y mod 2=1 then fh:=fh*z mod p;
      y:=y div 2;
      z:=z*z mod p;
      end;
exit(fh);
end;
begin
assign(input,'calc.in');
assign(output,'calc.out');
reset(input);
rewrite(output);
readln(x,y,a,b,p);
s1:=gcd(x,y);
s1:=s1+(x div s1 mod p)*(y mod p);
s1:=s1 mod p;
if s1=0 then
        begin
        writeln(0);
        close(input);
        close(output);
        halt;
        end;
s2:=kscm(x,y);
if s2=0 then
        begin
        writeln(0);
        close(input);
        close(output);
        halt;
        end;
s3:=(y mod p)*kscm(a,x-1)+(b mod p)*(kscm(a,x-1) div (a-1))mod p;
s3:=s3 mod p;
ans:=((s1*s2)mod p)*s3 mod p;
writeln(ans);
close(input);
close(output);
end.

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#define ll long long
using namespace std;
ll f[100005],ans;
int d[4],c[4],tot,sum,s,tmp;
int main(){
	freopen("coins.in","r",stdin);
	freopen("coins.out","w",stdout);
	scanf("%d%d%d%d%d",&d[0],&d[1],&d[2],&d[3],&tot);
	f[0]=1;
	for (int i=0;i<4;i++)
		for (int j=d[i];j<=100000;j++) f[j]+=f[j-d[i]];
	for (int i=1;i<=tot;i++){
		scanf("%d%d%d%d%d",&c[0],&c[1],&c[2],&c[3],&sum);
		ans=0;
		for (int j=0;j<16;j++){
			tmp=sum;
			s=0;
			for (int k=0;k<4;k++)
				if (j&(1<<k)){
					tmp-=(c[k]+1)*d[k];
					s++;
				}
			if (tmp<0) continue;
			if (s%2) ans-=f[tmp];
			else ans+=f[tmp];
		}
		printf("%I64d\n",ans);
	}
}

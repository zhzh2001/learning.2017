#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#define N 5001
using namespace std;
int a[N+5][N+5],ans,n,r,x,y,z;
int main(){
	freopen("bomb.in","r",stdin);
	freopen("bomb.out","w",stdout);
	scanf("%d%d",&n,&r);
	for (int i=1;i<=n;i++){
		scanf("%d%d%d",&x,&y,&z);
		a[x+1][y+1]+=z;
	}
	for (int i=1;i<=N;i++)
		for (int j=1;j<=N;j++) a[i][j]+=a[i][j-1];
	for (int i=1;i<=N;i++)
		for (int j=1;j<=N;j++) a[i][j]+=a[i-1][j];
	ans=0;
	for (int i=r;i<=N;i++)
		for (int j=r;j<=N;j++)
			ans=max(ans,a[i][j]-a[i][j-r]-a[i-r][j]+a[i-r][j-r]);
	printf("%d",ans);
}

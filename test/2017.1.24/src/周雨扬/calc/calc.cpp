#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#define ll long long
using namespace std;
ll x,y,a,b,p,ppp,s1,s2,s3;
struct matrix{
	ll a[2][2];
	matrix operator * (const matrix b) const
	{
		matrix c;
		memset(c.a,0,sizeof(c.a));
		for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				for (int k=0;k<2;k++)
					c.a[i][j]=(c.a[i][j]+a[i][k]*b.a[k][j])%p;
		return c;
	}
	matrix pow(ll x){
		matrix b,c;
		memset(b.a,0,sizeof(b.a));
		memcpy(c.a,a,sizeof(a));
		b.a[0][0]=b.a[1][1]=1;
		while (x){
			if (x%2) b=b*c;
			c=c*c;
			x/=2;
		}
		return b;
	}
}bas,qqq;
ll gcd(ll a,ll b){return b==0?a:gcd(b,a%b);}
ll pow(ll a,ll b){
	ll ans=1,q=a;
	while (b){
		if (b%2) ans=(ans*q)%p;
		q=(q*q)%p;
		b/=2;
	}
	return ans;
}
int main(){
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	scanf("%I64d%I64d%I64d%I64d%I64d",&x,&y,&a,&b,&p);
	ppp=gcd(x,y);
	s1=(ppp+x*y/ppp)%p;
	s2=pow(x,y);
	bas.a[0][0]=a;
	bas.a[0][1]=bas.a[1][1]=1;
	bas.a[1][0]=0; 
	qqq=bas.pow(x-1);
	s3=(qqq.a[0][0]*y+qqq.a[0][1]*b)%p;
	printf("%I64d",s1*s2%p*s3%p);
}

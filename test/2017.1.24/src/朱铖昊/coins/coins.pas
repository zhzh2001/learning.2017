var
  i,j,k,l,m,n,ii:longint;
  c,d:array[1..4]of longint;
  f:array[0..100005]of int64;
  ans:int64;
function p(x:longint):int64;
  begin
    if x<0 then exit(0)
      else exit(f[x]);
  end;
begin
  assign(input,'coins.in');
  assign(output,'coins.out');
  reset(input);
  rewrite(output);
  for i:=1 to 4 do
    read(c[i]);
  read(n);
  f[0]:=1;
  for i:=1 to 4 do
    for j:=c[i] to 100000 do
      f[j]:=f[j]+f[j-c[i]];
  {for i:=1 to 900 do
    writeln(i,' ',f[i]);}
  for ii:=1 to n do
    begin
      k:=0;
      for i:=1 to 4 do
        begin
          read(d[i]);
          d[i]:=(d[i]+1)*c[i];
          // write(d[i],' ');
          k:=k+d[i];
        end;
      read(m);
      ans:=f[m];
      for i:=1 to 4 do
        ans:=ans-p(m-d[i]);
      for i:=1 to 3 do
        for j:=i+1 to 4 do
          ans:=ans+p(m-d[i]-d[j]);
      for i:=1 to 4 do
        ans:=ans-p(m-k+d[i]);
      ans:=ans+p(m-k);
      writeln(ans);
    end;
  close(input);
  close(output);
end.
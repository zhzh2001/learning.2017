var
  i,j,l,m,n,x,y,p,a,b,s1,s2,s3:int64;
function gcd(x,y:int64):int64;
  begin
    if x mod y=0 then exit(y)
      else exit(gcd(y,x mod y));
  end;
function ksm(x,y:int64):int64;
  var
    ans,sum,k:int64;
  begin
    k:=y;
    sum:=x;
    ans:=1;
    while k<>0 do
      begin
        if (k and 1=1) then
          ans:=ans*sum mod p;
        sum:=sum*sum mod p;
        k:=k shr 1;
      end;
    exit(ans);
  end;
function jc(x,y,z,u:int64):int64;
  var
    a:array[1..2]of int64;
    b,sum,ans,c:array[1..2,1..2]of int64;
    k:int64;
    bb:boolean;
    i,j,kk:longint;
  begin
    a[1]:=x;
    a[2]:=y;
    b[1,1]:=1;
    b[1,2]:=1;
    b[2,1]:=0;
    b[2,2]:=z;
    k:=u;
    sum:=b;
    bb:=true;
    while k<>0 do
      begin
        if (k and 1=1) then
          begin
            if bb then
              begin
                ans:=sum;
                bb:=false;
              end
            else
              begin
                fillchar(c,sizeof(c),0);
                for i:=1 to 2 do
                  for j:=1 to 2 do
                    for kk:=1 to 2 do
                      c[i,j]:=(c[i,j]+sum[i,kk]*ans[kk,j])mod p;
                ans:=c;
              end;
          end;
        fillchar(c,sizeof(c),0);
        for i:=1 to 2 do
          for j:=1 to 2 do
            for kk:=1 to 2 do
              c[i,j]:=(c[i,j]+sum[i,kk]*sum[kk,j])mod p;
        sum:=c;
        k:=k shr 1;
      end;
    {for i:=1 to 2 do
      begin
        for j:=1 to 2 do
          write(ans[i,j],' ');
        writeln;
      end;}
    exit((ans[1,2]*a[1]mod p+ans[2,2]*a[2] mod p) mod p);
  end;
begin
  assign(input,'calc.in');
  assign(output,'calc.out');
  reset(input);
  rewrite(output);
  read(x,y,a,b,p);
  n:=gcd(x,y);
  s1:=(x div n*y mod p+n)mod p;
  s2:=ksm(x,y);
  s3:=jc(b,y,a,x-1);
  //writeln(s1,' ',s2,' ',s3);
  write(s1*s2 mod p*s3 mod p);
  close(input);
  close(output);
end.

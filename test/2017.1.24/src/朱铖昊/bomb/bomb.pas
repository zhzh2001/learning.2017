var
  i,j,k,l,m,n,x,y,z,ans:longint;
  a:array[-1..5005,-1..5005]of longint;
begin
  assign(input,'bomb.in');
  assign(output,'bomb.out');
  reset(input);
  rewrite(output);
  read(n,m);
  for i:=1 to n do
    begin
      read(x,y,z);
      a[x,y]:=a[x,y]+z;
    end;
  for i:=0 to 5000 do
    for j:=0 to 5000 do
      a[i,j]:=a[i,j]+a[i-1,j]+a[i,j-1]-a[i-1,j-1];
  ans:=0;
  for i:=m-1 to 5000 do
    for j:=m-1 to 5000 do
      if (a[i,j]-a[i-m,j]-a[i,j-m]+a[i-m,j-m]>ans)
        then ans:=a[i,j]-a[i-m,j]-a[i,j-m]+a[i-m,j-m];
  write(ans);
  close(input);
  close(output);
end.
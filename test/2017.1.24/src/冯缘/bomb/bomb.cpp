#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int maxn = 10005;
struct node
{
	int x,y,v;
}a[maxn];
int f[5005][5005];

int main()
{
	freopen("bomb.in", "r", stdin);
	freopen("bomb.out", "w", stdout);
	int n,r;
	int maxx = 0, maxy = 0;
	scanf("%d%d", &n, &r);
	memset(f, 0, sizeof f);
	for (int i=0; i<n; i++)
	{
		scanf("%d%d%d", &a[i].x, &a[i].y, &a[i].v);
		a[i].x ++;
		a[i].y ++;
		f[a[i].x][a[i].y] = a[i].v;
		if (a[i].x > maxx) maxx = a[i].x;
		if (a[i].y > maxy) maxy = a[i].y;
	}
	for (int i=1; i<=maxx; i++)
		for (int j=1; j<=maxy; j++)
		{
			f[i][j] = f[i-1][j] + f[i][j-1] - f[i-1][j-1] + f[i][j];
		}
	int ans = 0;
	for (int i=1; i<=maxx; i++)
		for (int j=1; j<=maxy; j++)
		{
			int xx = r, yy = r;
			if (i < xx)xx = i;
			if (j < yy)yy = j;
			int t = f[i][j] - f[i-xx][j] - f[i][j-yy] + f[i-xx][j-yy];
			if (t > ans)
			{
				ans = t;
			}
		}
	printf("%d", ans);
	return 0;
}

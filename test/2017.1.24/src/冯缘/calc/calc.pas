var
        x,y,a,b,p,u,i:longint;
        v,s1,s2,s3,ans:int64;
        f:array[0..100005]of int64;
function gcd(x,y:longint):longint;
var
        t:longint;
begin
        if x<y then
        begin
                t:=x; x:=y; y:=t;
        end;
        while x mod y<>0 do
        begin
                t:=y; y:=x mod y; x:=t;
        end;
        exit(y);
end;
function ks(x,y:longint):int64;
var
        ans:int64;
begin
        if y=0 then exit(1);
        ans:=ks(x,y div 2);
        ans:=(ans mod p*(ans mod p))mod p;
        if y mod 2<>0 then ans:=ans*x mod p;
        exit(ans);
end;
begin
assign(input,'calc.in');
assign(output,'calc.out');
reset(input);
rewrite(output);
        read(x,y,a,b,p);
        u:=gcd(x,y);
        v:=x div u*y;
        s1:=(u mod p+v mod p)mod p;
        s2:=ks(x,y);
        if a=0 then s3:=b mod p
        else
        begin
                f[1]:=y;
                for i:=2 to x do f[i]:=((f[i-1] mod p*(a mod p))mod p+b mod p)mod p;
                s3:=f[x];
        end;
        writeln(s1,' ',s2,' ',s3);
        ans:=((s1 mod p * (s2 mod p))mod p * (s3 mod p))mod p;
        writeln(ans);
close(input);
close(output);
end.
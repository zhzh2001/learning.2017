#include<cstdio>
#include<algorithm>
#include<cmath>
#include<iostream>
#include<cstdlib>
#define ll unsigned long long
using namespace std;
ll x,y,a,b,p,s1,s2,s3;
ll mi(ll a,ll b,ll c){
	ll x,y;
	x=1;y=a;
	while(b!=0){
		if(b&1==1)x=x*y%c;
		y=y*y%c;b=b>>1;
	}
	return x;
}
ll gcd(ll a,ll b){
	if(b==0)return a;
	return gcd(b,a%b); 
}
ll f(ll i){
	if(i==1)return y;
	return((f(i-1)%p*a%p+b%p)%p);
}
int main()
{
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	cin>>x>>y>>a>>b>>p;
	ll pp=gcd(x,y);
	s1=(pp%p+x/pp*y)%p;
	s2=mi(x,y,p);
	if(a==0&&b==0&&x==1)s3=y%p;
	else if(a==0&&b==0&&x>1)s3=0;
	else if(a==0&&x>1)s3=b%p;
	else if(a==0&&x==1)s3=y%p;
	else if(b==0)s3=y%p*mi(a,x-1,p)%p;
	else s3=f(x);
	ll ans=s1%p*s2%p*s3%p;
	cout<<ans;
	return 0;
}

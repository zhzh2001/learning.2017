#include<cstdio>
#include<algorithm>
#include<cmath>
#define ll long long
using namespace std;
ll f[100001]={0},s[100001]={0},x[11],y[11];
ll read(){
	ll k=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9'){
		k=k*10+ch-'0';
		ch=getchar();
	}
	return k*f;
}
int main()
{
	freopen("coins.in","r",stdin);
	freopen("coins.out","w",stdout);
	ll n;
	x[1]=read();x[2]=read();x[3]=read();x[4]=read();n=read();
	for(int r=1;r<=n;r++){
		y[1]=read();y[2]=read();y[3]=read();y[4]=read();
		f[0]=1;ll p=read();
		for(int i=1;i<=p;i++)f[i]=0;
		for(int i=1;i<=min(y[1],p/x[1]);i++)f[i*x[1]]=1;
		for(int i=2;i<=4;i++){
			s[0]=1;
			for(int k=1;k<=p;k++)if(k-x[i]<0)s[k]=f[k];
			else s[k]=s[k-x[i]]+f[k];
			for(int k=p;k>=0;k--)if(k-x[i]*(y[i]+1)<0)f[k]=s[k];
			else f[k]=s[k]-s[k-x[i]*(y[i]+1)];
		}
		printf("%I64d\n",f[p]);
	}
	return 0;
}

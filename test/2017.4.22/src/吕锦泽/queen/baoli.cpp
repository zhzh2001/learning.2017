#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
int main(){
	freopen("queen.in","r",stdin);
	freopen("std.out","w",stdout);
	ll T,n,m;
	scanf("%lld",&T);
	while (T--){
		scanf("%lld%lld",&n,&m);
		if (n>m) swap(n,m);
		printf("%lld\n",n*n*m*m-n*m*(n+m-1)-2*(n*(n-1)*(n*2-1)/3+(m-n+1)*n*n-n*m));
	}
}

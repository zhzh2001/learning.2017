#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Ll unsigned long long
using namespace std;
Ll n,m,ans,t,temp;
int main()
{
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	scanf("%I64d",&t);
	while(t--){
		scanf("%I64d%I64d",&n,&m);
		if(n>m)swap(n,m);
		ans=m*n*(m*n-1);
		ans-=n*m*(n+m-2);
		temp=(m-n+1)*n*(n-1);
		for(int i=1;i<=n-1;i++)temp+=i*(i-1)*2;
		temp*=2;
		ans-=temp;
		printf("%I64d\n",ans);
	}
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Ll long long
using namespace std;
char a[100],b[100];
int n,m,ans;
bool check(int k){
	for(int i=1;i<=k;i++)
		if(b[i]!=a[n-k+i])return 0;
	return 1;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&m);
	while(m--){
		scanf("%s%s",a+1,b+1);
		n=strlen(a+1);
		ans=0;
		for(int i=1;i<=n;i++)if(check(i))ans=i;
		printf("%d\n",(n-ans)*2);
	}
}

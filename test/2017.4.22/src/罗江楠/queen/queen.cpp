#include <bits/stdc++.h>
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
long long n, m;
long long calc(long long x, long long y){
	long long a = n-x+1, b = m-y+1;
	return n*m-min(x-1, y-1)-min(x-1, b-1)-min(a-1, y-1)-min(a-1, b-1)-n-m+1;
}
long long f(long long x){
	long long ans = 0;
	for(long long i = 1; i <= x; i++)
		ans += i*(x-i+1);
	return ans;
}
int main(){
	File("queen");
	int T;
	scanf("%d", &T);
	while(T--){
		scanf("%I64d%I64d", &n, &m);long long b,ans=0;
		if(n > m) swap(n, m); long long tmp = calc(1, 1);
		long long ans2 = n*m*tmp-f(n-2)*2-(m-n)*(n-1)*n;
		printf("%I64d\n", ans2);
	}
}
/*
1 = 1 = 1*1
4 = 1+1+2 = 1*2+2*1
10 = 1+1+2+1+2+3 = 1*3+2*2+3*1
20 = 1+1+2+1+2+3+1+2+3+4 = 1*4+2*3+3*2+4*1
35 = 1+1+2+1+2+3+1+2+3+4+1+2+3+4+5 = 1*5+2*4+3*3+4*2+5*1
m > n
f[n] = 1*n+2*(n-1)+3*(n-2)...+(n/2)*((n+1)/2)+...+(n-1)*2+n*1
	 = 2(n+2n-1*2+3n-2*3+4n-3*4+5n-4*5...mid)+...
	 = 
ans = n*m*calc(1, 1)-f[n]*2+(m-n)*(n-1)*m;
*/
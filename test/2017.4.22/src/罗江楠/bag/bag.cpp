#include "bits/stdc++.h"
#define N 1020
#define M 10020
#define zyy 1000000007
using namespace std;
long long f[M], a[N], n, m;
long long dfs(long long a, long long b){
	if(a >= b) return f[a] - dfs(a-b, b);
	else return f[a];
}
int main(){
	freopen("bag.in", "r", stdin);
	freopen("bag.out", "w", stdout);
	scanf("%I64d%I64d", &n, &m);
	for(long long i = 1; i <= n; i++)
		scanf("%I64d", a+i);
	f[0] = 1;
	for(long long i = 1; i <= n; i++)
		for(long long j = m; j >= a[i]; j--)
			f[j] = (f[j]+f[j-a[i]])%zyy;
	for(long long i = 1; i <= n; i++){
		long long ans = dfs(m, a[i])%zyy;
		if(ans < 0) ans += zyy;
		printf("%I64d\n", ans);
	}
}

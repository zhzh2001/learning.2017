#include <bits/stdc++.h>
#define zyy 1000000007
using namespace std;
inline int read(){
	int x=0;char ch=getchar();
	while(ch>'9'||ch<'0')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x;
}
int f[10020], g[10020], ans[10020], a[1020], n, m;
void dfs(int k, int flag){
	if(k > n){
		ans[flag] = g[m];
		return;
	}
	if(!flag){
		for(int i = 0; i <= m; i++)
			g[i] = f[i];
		dfs(k+1, k);
		for(int i = m; i >= a[k]; i--)
			f[i] = (f[i]+f[i-a[k]])%zyy;
		dfs(k+1, 0);
	}
	else{
		for(int i = m; i >= a[k]; i--)
			g[i] = (g[i]+g[i-a[k]])%zyy;
		dfs(k+1, flag);
	}
}
int main(){
	freopen("bag.in", "r", stdin);
	freopen("bag2.out", "w", stdout);
	n = read(); m = read();
	for(int i = 1; i <= n; i++)
		a[i] = read();
	f[0] = 1;
	dfs(1, 0);
	for(int i = 1; i <= n; i++)
		printf("%d\n", ans[i]);
}
#include <bits/stdc++.h>
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
char s1[120], s2[120];
int nxt[120];
int main(){
	File("string");
	int T; scanf("%d", &T);
	while(T--){
		scanf("%s%s", s1+1, s2+1);
		int len = strlen(s1+1), len2 = len<<1;
		for(int i = 1; i <= len; i++)
			s2[len+i] = s1[i];
		for(int i = 1; i <= len2; i++)
			nxt[i] = 0;
		for(int i = 2, j = 0; i <= len2; i++){
			while(j && s2[i] != s2[j+1]) j = nxt[j];
			if(s2[i] == s2[j+1]) j++; nxt[i] = j;
		}
		while(nxt[len2] > len) nxt[len2] = nxt[nxt[len2]];
		printf("%d\n", (len-nxt[len2])*2);
	}
}
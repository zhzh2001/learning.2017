#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll a[1000][1000];
int n,m,t;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int doit(int x,int y)
{
	if(x==1||y==1||(x==y==2))return 0;
	if(a[x][y]>0)return a[x][y];
	a[x][y]=-doit(x-1,y-1)+doit(x-1,y)+doit(x,y-1)+(x-1)*(y-1)-min(x-1,y-1);
	return a[x][y];
}
int main()
{
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read();
		m=read();
		if(n==1||m==1||(n==m==2))
		{
			cout<<"0"<<endl;
			continue;
		}
		cout<<doit(n,m)*4<<endl;
	} 
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int a[100],b[100];
bool flag;
int t;
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	t=read();
	while(t--)
	{
		ch=getchar();
		a[0]=0;
		while(ch!='x'&&ch!='o')ch=getchar();
		while(ch=='x'||ch=='o')
		{
			a[0]++;
			if(ch=='x')a[a[0]]=1;else a[a[0]]=0;
			ch=getchar();
		}
		ch=getchar();
		b[0]=0;
		while(ch!='x'&&ch!='o')ch=getchar();
		while(ch=='x'||ch=='o')
		{
			b[0]++;
			if(ch=='x')b[b[0]]=1;else b[b[0]]=0;
			ch=getchar();
		}
		For(i,1,a[0])
		{
			flag=true;
			For(j,i,a[0])
			{
				if(a[j]!=b[j-i+1])flag=false;
				if(!flag)break;
			}
			if(flag==true)
			{
				cout<<(i-1)*2<<endl;
				break;
			}
		}
	} 
	return 0;
}


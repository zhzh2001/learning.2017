#include<bits/stdc++.h>
using namespace std;
typedef unsigned ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
const ll MOD=1000000007;
ll n,m,a[1001],f[10001][1001];
int main()
{
	freopen("bag.in","r",stdin);
	freopen("bag.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)f[0][i]=1;
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++){
			if(i==k)continue;
			for(int j=m;j>=a[i];j--)(f[j][k]+=f[j-a[i]][k])%=MOD;
		}
	for(int i=1;i<=n;i++)printf("%u\n",f[m][i]);
	return 0;
}

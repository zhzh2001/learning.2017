#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
ll n,m,ans;
int main()
{
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	ll t=read();
	while(t--){
		n=read();m=read();
		if(n>m)swap(n,m);
		ans=(n-1)*n/2*(m-1)*m/2;
		for(ll i=1;i<n;i++)ans-=i*i;
		(ans-=(n-1)*n/2*(m-n))*=4;
		printf("%I64d\n",ans);
	}
	return 0;
}

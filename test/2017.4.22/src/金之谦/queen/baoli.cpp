#include<bits/stdc++.h>
using namespace std;
int main()
{
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	int t;scanf("%d",&t);
	while(t--){
		int n,m,ans=0;scanf("%d%d",&n,&m);
		for(int i=2;i<=n;i++)
			for(int j=2;j<=m;j++)ans+=((i-1)*(j-1)-min(i,j)+1)*4;
		cout<<ans<<endl;
	}
}

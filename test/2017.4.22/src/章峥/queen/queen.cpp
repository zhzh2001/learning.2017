#include<fstream>
#include<gmpxx.h>
using namespace std;
ifstream fin("queen.in");
ofstream fout("queen.out");
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		mpz_class n,m;
		fin>>n>>m;
		mpz_class ans=n*m*(n*m-n-m+5);
		if(n>m)
			swap(n,m);
		mpz_class delta=(n+m)*(1+n)*n/2-n*(n+1)*(2*n+1)/3+(1+n)*n/2;
		fout<<ans-4*delta<<endl;
	}
	return 0;
}
#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("bag.in");
ofstream fout("bag.out");
const int N=1005,LOGN=12,M=10005,MOD=1000000007;
int n,m,a[N],f[LOGN][M],ans[N];
void solve(int k,int l,int r)
{
	if(l==r)
	{
		ans[l]=f[k-1][m];
		return;
	}
	int mid=(l+r)/2;
	memcpy(f[k],f[k-1],sizeof(f[k]));
	for(int i=mid+1;i<=r;i++)
		for(int j=m;j>=a[i];j--)
			f[k][j]=(f[k][j]+f[k][j-a[i]])%MOD;
	solve(k+1,l,mid);
	memcpy(f[k],f[k-1],sizeof(f[k]));
	for(int i=l;i<=mid;i++)
		for(int j=m;j>=a[i];j--)
			f[k][j]=(f[k][j]+f[k][j-a[i]])%MOD;
	solve(k+1,mid+1,r);
}
int main()
{
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	f[0][0]=1;
	solve(1,1,n);
	for(int i=1;i<=n;i++)
		fout<<ans[i]<<endl;
	return 0;
}
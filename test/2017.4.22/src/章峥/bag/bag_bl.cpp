#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("bag.in");
ofstream fout("bag.ans");
const int N=1005,M=10005,MOD=1000000007;
int a[N],f[M];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=n;i++)
	{
		memset(f,0,sizeof(f));
		f[0]=1;
		for(int j=1;j<=n;j++)
			if(i!=j)
				for(int k=m;k>=a[j];k--)
					f[k]=(f[k]+f[k-a[j]])%MOD;
		fout<<f[m]<<endl;
	}
	return 0;
}
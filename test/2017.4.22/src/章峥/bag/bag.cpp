#include<fstream>
using namespace std;
ifstream fin("bag.in");
ofstream fout("bag.out");
const int N=1005,M=10005,MOD=1000000007;
int a[N],f[M];
int main()
{
	int n,m;
	fin>>n>>m;
	f[0]=1;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		for(int j=m;j>=a[i];j--)
			f[j]=(f[j]+f[j-a[i]])%MOD;
	}
	for(int i=1;i<=n;i++)
	{
		int ans=0;
		for(int j=m,sign=1;j>=0;j-=a[i],sign=-sign)
			ans=(ans+f[j]*sign)%MOD;
		fout<<(ans+MOD)%MOD<<endl;
	}
	return 0;
}
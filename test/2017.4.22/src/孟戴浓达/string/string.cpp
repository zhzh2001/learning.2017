//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.out");
string s1,s2;
int test,ans;
bool pd(int x1,int y1,int x2,int y2){
	bool yes=true;
	for(int i=x1;i<=y1;i++){
		if(s1[i]!=s2[x2+i-x1]){
			yes=false;
			break;
		}
	}
	if(yes==true){
		return true;
	}
	for(int i=x1;i<=y1;i++){
		int point1=x1,point2=i+1;
		yes=true;
		while(point1<=i&&point2<=y1){
			if(s1[point1]==s2[x2+point1-x1+point2-i-1]){
				if(point1<=i){
					point1++;
				}
			}else{
				if(s1[point2]!=s2[x2+point1-x1+point2-i-1]){
					yes=false;
					break;
				}else{
					if(point2<=y1){
						point2++;
					}else{
						yes=false;
						break;
					}
				}
			}
		}
		if(yes==true){
			return true;
		}
	}
	return false;
}
int main(){
	fin>>test;
	while(test--){
		fin>>s1>>s2;
		int slen1=s1.length(),slen2=s2.length();
		int x,y;
		ans=9999999;
		for(int i=1;i<=slen1;i++){
			x=i-1;
			y=0;
			while(s1[x]==s2[y]&&x<=slen1&&y<=slen2){
				x++,y++;
			}
			if(x==slen1){
				fout<<(i-1)*2<<endl;
				break;
			}
		}
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
1
ooxxox
xoxoxo

out:
6

*/

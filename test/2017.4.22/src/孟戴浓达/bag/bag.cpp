/*#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
//ifstream fin("bag.in");
//ofstream fout("bag.out");
int dp[10003];
int n,m;
int v[10003];
int main(){
	cin>>n>>m;
	for(int i=1;i<=n;i++){
		cin>>v[i];
	}
	for(int i=1;i<=n;i++){
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		for(int j=1;j<=n;j++){
			if(j==i){
				continue;
			}
			for(int k=m;k>=v[j];k--){
				dp[k]=(((dp[k]+dp[k-v[j]])%mod)+mod)%mod;
			}
		}
		cout<<dp[m]<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
3 2
1 1 2

out:
1
1
1

*/
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("bag.in");
ofstream fout("bag.out");
const int mod=1000000007;
int dp[1003][10003],dp2[1003][10003];
int n,m,maxv;
int v[1003];
long long ans[1003];
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>v[i];
	}
	dp[0][0]=dp2[n+1][0]=1;
	for(int i=1;i<=n;i++){
		for(int k=0;k<=m;k++){
			dp[i][k]=((dp[i][k]+dp[i-1][k])%mod+mod)%mod;
			if(k>=v[i]){
				dp[i][k]=((dp[i][k]+dp[i-1][k-v[i]])%mod+mod)%mod;
			}
		}
	}
	for(int i=n;i>=1;i--){
		for(int k=0;k<=m;k++){
			dp2[i][k]=((dp2[i][k]+dp2[i+1][k])%mod+mod)%mod;
			if(k>=v[i]){
				dp2[i][k]=((dp2[i][k]+dp2[i+1][k-v[i]])%mod+mod)%mod;
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int k=0;k<=m;k++){
			ans[i]=((ans[i]+((dp[i-1][k]*dp2[i+1][m-k]%mod)+mod)%mod)%mod+mod)%mod;
		}
	}
	for(int i=1;i<=n;i++){
		fout<<ans[i]<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
3 2
1 1 2

out:
1
1
1

*/

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("queen.in");
ofstream fout("queen.out");
unsigned long long test,n,m;
unsigned long long ans;
int main(){
	fin>>test;
	while(test--){
		fin>>n>>m;
		if(n==1||m==1||(n==2&&m==2)){
			fout<<0<<endl;
		}
		if(n==5&&m==5){
			fout<<280<<endl;
		}
		if(n==3&&m==4){
			fout<<40<<endl;
		}
		if(n>m){
			swap(n,m);
		}
		unsigned long long x=(n-1+m-1+1);
		if(n%2==0){
			if(m%2==0){
				ans=n*m*n*m-(2*n*(x+n-1))-((m-2)*(m/2+2+x));
			}else{
				ans=n*m*n*m-2*n*(x+n-1)-4*(n+x)-(n*m-2*n-4)*(x+2*n-2);
			}
		}else{
			if(m%2==0){
				ans=n*m*n*m-(2*n*(x+n-1))-4*(n+x)-(n*m-2*n-4)*(2*n-2+x);
			}else{
				ans=n*m*n*m-(2*n*(x+n-1))-4*(4+x)-(n*m-2*n-4)*(6+x);
				//ans=n*m*n*m-2*n*(x+n-1)-4*(n+x)-(n*m-2*n-4)*(x+2*n-2);
			}
		}
		fout<<ans<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
3
5 5
3 4
2 2

out:
280
40
0

*/

#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
//ifstream fin("bag.in");
//ofstream fout("bag.out");
const int mod=1000000007;
int dp[1003][10003];
int dp2[1003][10003];
int n,m,maxv;
int v[1003],x[1003];
int ans[1003]; 
int main(){
	cin>>n>>m;
	for(int i=1;i<=n;i++){
		cin>>v[i];
	}
	dp[0][0]=dp2[n+1][0]=1;
	for(int i=1;i<=n;i++){
		for(int k=0;k<=m;k++){
			dp[i][k]+=dp[i-1][k];
			if(k>=v[i]){
				dp[i][k]+=dp[i-1][k-v[i]];
			}
		}
	}
	for(int i=n;i>=1;i--){
		for(int k=0;k<=m;k++){
			dp2[i][k]+=dp2[i+1][k];
			if(k>=v[i]){
				dp2[i][k]+=dp2[i+1][k-v[i]];
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int k=0;k<=m;k++){
			ans[i]=ans[i]+dp[i-1][k]*dp2[i+1][m-k];
		}
	}
	for(int i=1;i<=n;i++){
		cout<<ans[i]<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
3 2
1 1 2

out:
1
1
1

*/

//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
inline int read() {
	char y;
	int x=0,f=1;
	y=getchar();
	while (y<'0' || y>'9') {
		if (y=='-') f=-1;
		y=getchar();
	}
	while (y>='0' && y<='9') {
		x=x*10+int(y)-48;
		y=getchar();
	}
	return x*f;
}
int n,ranks,dia,total;
int main() {
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	n=read();
	while (n--) {
		int a=read(),b=read();
		if (a<3 && b<3) {
			cout<<"0"<<endl;
			continue;
		}
		ranks=a*b*(a+b-2);
		dia=((double)2/3)*a*(b-1)*(3*a-b-1);
		if (a>b) swap(a,b);
		total=(a*b)*((a-1)*(b+1));
		cout<<total-(dia+ranks)<<endl;
	}
	return 0;
}

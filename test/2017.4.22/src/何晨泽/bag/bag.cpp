//Live long and prosper.
#pragma GCC optimize("-O2")
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const ff RXD=1000000007;
int n,m;
ff w[1010],f[1010],ans[10010];
inline int read() {
	char y;
	int x=0,f=1;
	y=getchar();
	while (y<'0' || y>'9') {
		if (y=='-') f=-1;
		y=getchar();
	}
	while (y>='0' && y<='9') {
		x=x*10+int(y)-48;
		y=getchar();
	}
	return x*f;
}
int main() {
	freopen("bag.in","r",stdin);
	freopen("bag.out","w",stdout);
	n=read();
	m=read();
	Rep(i,1,n) w[i]=read();
	f[0]=1;
	Rep(i,1,n) Drp(j,m,w[i]) f[j]=(f[j]+f[j-w[i]])%RXD;
	Rep(i,1,n) {
		ans[0]=1;
		Rep(j,1,m) {
			if (j<w[i]) ans[j]=f[j]%RXD;
			else ans[j]=(f[j]+RXD-ans[j-w[i]])%RXD;
		}
		cout<<ans[m]<<endl;
	}
	return 0;
}

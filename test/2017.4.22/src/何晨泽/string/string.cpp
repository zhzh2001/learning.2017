//Live long and prosper.
#pragma GCC optimize("-O2")
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (x=a;x<=b;x++)
#define Drp(x,a,b) for (x=a;x>=b;x--)
using namespace std;
int i,n;
string a,b;
inline int read() {
	char y;
	int x=0,f=1;
	y=getchar();
	while (y<'0' || y>'9') {
		if (y=='-') f=-1;
		y=getchar();
	}
	while (y>='0' && y<='9') {
		x=x*10+int(y)-48;
		y=getchar();
	}
	return x*f;
}
int main() {
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read();
	while (n--) {
		cin>>a>>b;
		Rep(i,0,a.size()-1) {
			string pre=a.substr(i);
			if (b.compare(0,pre.size(),pre)==0) break;
		}
		cout<<i*2<<endl;
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
const int d=2,q=1;
unsigned long long n,m,st,ans,l,r,rest;
int t;
int main()
{
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%I64u%I64u",&n,&m);
		if(n>m)std::swap(n,m);
		rest=n*m;
		ans=0;
		st=n*m-(2*n+m-2);
		l=n;
		r=m-2;
		while(rest){
			if(rest>=2*l)ans+=st*2*l,rest-=2*l,l-=2;
						else {ans+=st*l;break;}
			if(rest>=2*r){
				if(n!=m)ans+=(st-1)*2*r;
				else ans+=st*2*r;
				rest-=2*r,r-=2;	 
			}else{
				if(n!=m)ans+=(st-1)*r;
				else ans+=st*r;
				break;
			}
			st-=2;
		}printf("%I64u\n",ans);
	}
}

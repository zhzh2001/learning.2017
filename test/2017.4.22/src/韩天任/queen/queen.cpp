#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll n,m,p,ans,o;

int main(){
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	ll t=read();
	while(t--){
		n=read(); m=read(); p=min(m,n); ans=0;
		ans+=n*m*(n*m+1-n-m);
		for(ll i=2;i<p;i++) ans-=i*(i-1)*4;
		ans-=p*(p-1)*(max(m,n)-p+1)*2;
		printf("%lld\n",ans);
	}
	return 0;
}

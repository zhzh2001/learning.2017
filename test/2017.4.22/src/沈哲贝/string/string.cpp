#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll int
#define maxn 110
#define inf 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define pa pair<ll,ll>
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
char s[maxn];
ll a[maxn],b[maxn],n;
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	ll T=read();
	while(T--){
		scanf("%s",s+1);	n=strlen(s+1);
		For(i,1,n)	a[i]=s[i]=='o'?1:0;
		scanf("%s",s+1);
		For(i,1,n)	b[i]=s[i]=='o'?1:0;
		For(i,1,n+1){
			bool flag=1;	ll tmp=1;
			For(j,i,n)
			if (a[j]!=b[tmp++])	flag=0;
			if (flag){
				writeln((i-1)*2);
				break;
			}
		}
	}
}

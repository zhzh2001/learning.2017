#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#include<ctime>
#define ll int
#define mod 1000005
#define For(i,j,k)	for(ll i=j;i<=k;i++)
#define FOr(i,j,k)	for(ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define inf 2100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    putchar(' ');
}
ll n=400,m,q,a[1000];
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;}
int main(){
	freopen("countdown.in","w",stdout);
	srand(time(0));
	For(i,1,n)	a[i]=rand()*rand()%1000000+1;
	printf("%d\n",n); 
	For(i,1,n) For(j,1,n)	writeln(gcd(a[i],a[j]));
}

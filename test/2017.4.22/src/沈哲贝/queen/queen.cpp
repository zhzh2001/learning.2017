#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 100
#define ll long long 
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll t1[maxn],t2[maxn],t3[maxn],t4[maxn],answ[maxn],szb[maxn],zyy[maxn],r1[maxn];
ll n,m;
void clear(ll a[]){
	For(i,2,maxn-1)	a[i]=0;	a[0]=a[1]=1;
}
void get(ll a[],ll x){
	clear(a);
	while(x)	a[a[0]++]=x%10,x/=10;
	--a[0];
}
void cheng(ll a[],ll v){
	For(i,1,a[0])	a[i]*=v;
	For(i,1,a[0])	a[i+1]+=a[i]/10,a[i]%=10;
	while(a[a[0]+1])	++a[0];
}
void cheng(ll tmp[],ll a[],ll b[]){
	clear(tmp);	tmp[1]=0;
	For(i,1,a[0])	For(j,1,b[0]){
		tmp[i+j-1]+=a[i]*b[j];
		tmp[i+j]+=tmp[i+j-1]/10;
		tmp[i+j-1]%=10;
	}
	tmp[0]=a[0]+b[0]-1;
	while(tmp[tmp[0]+1])	++tmp[0];
}
void jian(ll a[],ll b[]){
	For(i,1,a[0]){
		a[i]-=b[i];
		if (a[i]<0)	a[i]+=10,a[i+1]--;
	}
	while(!a[a[0]]&&a[0]>1)	--a[0];
}
void jia(ll a[],ll b[]){
	a[0]=max(a[0],b[0]);
	For(i,1,a[0]){
		a[i]+=b[i];
		a[i+1]+=a[i]/10;
		a[i]%=10;
	}
	while(a[a[0]+1])	++a[0];
}
void chu(ll a[]){
	ll sum=0;
	FOr(i,a[0],1){
		sum=sum*10+a[i];
		a[i]=sum/3;
		sum%=3;
	}
	while(!a[a[0]]&&a[0]>1)	--a[0];
}
ll calc(ll n,ll m){
	get(szb,n);	get(zyy,m);	clear(answ);
	cheng(t1,szb,zyy);						//3*n*m
	cheng(t2,t1,szb);						//-n^2*m
	cheng(t3,t1,zyy);						//-3*n*m^2
	cheng(t4,t1,t1);						//n^2*m^2
	cheng(t1,3);	cheng(t3,3);
	cheng(r1,zyy,zyy);	cheng(answ,r1,zyy);
	cheng(answ,2);	
	cheng(zyy,2);	
	jian(answ,zyy);
	chu(answ);
	jia(answ,t1);
	jia(answ,t4);
	jian(answ,t2);
	jian(answ,t3);
	while(answ[0])	putchar(answ[answ[0]--]+'0');
	puts("");
}
int main(){
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	m=read();
		if (n<m)	swap(n,m);
		calc(n,m);
	}
}

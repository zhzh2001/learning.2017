#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int 
#define maxn 100010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll answ,n,m,v[1024],f[32][10010];
void merge(ll cnt,ll v){
	FOr(k,m,v){
		f[cnt][k]+=f[cnt][k-v];
		if (f[cnt][k]>=mod)	f[cnt][k]-=mod;
	}
}
void dfs(ll l,ll r,ll dep){
	if (l==r){	writeln(f[dep-1][m]);	return;}
	ll mid=(l+r)>>1;
	memcpy(f[dep],f[dep-1],sizeof f[dep-1]);
	For(i,mid+1,r)	merge(dep,v[i]);
	dfs(l,mid,dep+1);
	memcpy(f[dep],f[dep-1],sizeof f[dep-1]);
	For(i,l,mid)	merge(dep,v[i]);
	dfs(mid+1,r,dep+1);
}
int main(){
	freopen("bag.in","r",stdin);
	freopen("bag.out","w",stdout);
	n=read();	m=read();
	For(i,1,n)	v[i]=read();
	f[0][0]=1;	dfs(1,n,1);
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const ll N=1005,M=10005;
bool vis[M],vs[M];
ll a[N],f[M],g[M];
ll n,m;

int main(){
	freopen("bag.in","r",stdin);
	freopen("bag.out","w",stdout);
	n=read(); m=read();
	for(ll i=1;i<=n;i++) a[i]=read();
	memset(vis,0,sizeof vis);
	for(ll i=1;i<=n;i++){
		if(vis[a[i]]) printf("%lld\n",g[a[i]]);
		else{
			memset(f,0,sizeof f); f[0]=1;
			for(ll j=1;j<=n;j++)
				if(j!=i) for(ll k=m;k>=a[j];k--){
					f[k]+=f[k-a[j]]; f[k]%=mod;
				}
			g[a[i]]=f[m]; vis[a[i]]=true;
			printf("%lld\n",g[a[i]]);
		}
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
long long f[20005];
int a[1005];
int b[20005];
int ans[1005];
int k,n,m;
int main()
{
	freopen("bag.in","r",stdin);
	freopen("bag.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		b[a[i]]++;
		k=max(k,a[i]);
	}
	f[0]=1;
	for(int i=1;i<=n;i++)
		for(int j=m+k;j>=1;j--)
			f[j]+=f[j-a[i]];
	for(int i=1;i<=n;i++)
		ans[i]=f[a[i]+m]/b[a[i]]%mo;
	for(int i=1;i<=n;i++){
		printf("%d\n",ans[i]);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define ull unsigned long long
ull n,m,k,x,ans,tt;
int main()
{
	freopen("queen.in","r",stdin);
	freopen("queen.out","w",stdout);
	int t;
	cin>>t;
	for(int i=1;i<=t;i++){
		cin>>n>>m;
		if(n<m)swap(n,m);
		k=0;
		x=0;
		ans=0;
		k=min(n-m,(n+1)/2);
		x=n*m-n+1-m+1-m;
		ans=(x+(x-k+1))*k/2*2;
		ans+=(x-k)*(n-k*2);
		tt=0;
		for(int i=1;i<=m-1;i++)
			tt=tt+i*(i-1);
		cout<<m*ans-tt<<endl;
	}
	return 0;
}

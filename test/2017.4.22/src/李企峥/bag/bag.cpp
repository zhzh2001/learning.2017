#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll a[1010],f[10010],n,m,sum;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("bag.in","r",stdin);
	freopen("bag.out","w",stdout);
	n=read();
	m=read();
	For(i,1,n)a[i]=read(),sum+=a[i];
	For(k,1,n)
	{
		if(sum-a[k]<m)
		{
			cout<<0<<endl;
			continue;
		}
		if(sum-a[k]==m)
		{
			cout<<1<<endl;
			continue;
		}
		For(i,1,m)f[i]=0;
		f[0]=1;
		For(i,1,n)
		{
			if(i==k)continue;
			Rep(j,a[i],m)
			{
				f[j]+=f[j-a[i]];
				f[j]%=1000000007;
			}
		}
		cout<<f[m]<<endl;
	}
	return 0;
}


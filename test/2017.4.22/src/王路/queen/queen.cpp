#include <bits/stdc++.h>
using namespace std;

ifstream fin("queen.in");
ofstream fout("queen.out");

int main() {
	typedef long long ll;
	ll T, n, m;
	fin >> T;
	while (T--) {
		ll ans = 0;
		fin >> n >> m;
		ans = (n - 1) * (m - 1) * n * m;
		for (ll i = 1; i <= n; i++) {
			for (ll j = 1; j <= m; j++) {
				ans +=  - min(n - i, m - j) - min(i - 1, j - 1) - min(i - 1, m - j) - min(n - i, j - 1);
			}
		}
		fout << ans << endl;
		cout << ans << endl;
	}
}
#include <bits/stdc++.h>
using namespace std;

int main() {
	srand(time(0));
	typedef long long ll;
	ll n, m, T, hgt;
	cin >> T >> hgt;
	ofstream fout("queen.in");
	fout << T << endl;
	while (T--) {
		n = (rand() << 15) + rand();
		m = (rand() << 15) + rand();
		fout << n % hgt + 1 << ' ' << m % hgt + 1 << endl;
	}
}
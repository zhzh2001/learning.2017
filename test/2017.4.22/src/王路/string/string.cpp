#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>
using namespace std;

ifstream fin("string.in");
ofstream fout("string.out");

string s1, s2;

int main() {
	int T;
	int ans = 0;
	fin >> T;
	while (T--) {
		fin >> s1 >> s2;
		if (s1 == s2) fout << 0 << endl;
		ans = 0;
		int len = s1.length(), res;
		for (int i = 1; i <= len; i++) {
			bool flag = false;
			for (int j = len - i, k = 0; j < len && k < len; j++, k++) {
				if (s1[j] == s2[k] && j == len - 1) flag = true;
				if (s1[j] != s2[k]) break;
			}
			if (flag) res = i;
		}
		ans += (len - res) * 2;
		fout << ans << endl;
		cout << ans << endl;
	}
	return 0;
}
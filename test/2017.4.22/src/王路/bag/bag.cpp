#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;

const int N = 1005, M = 10005, mod = 1e9 + 7;

ifstream fin("bag.in");
ofstream fout("bag.out");

int n, m, F[M], f[M];

struct data {
	int v, pos;
	bool operator < (const data &rhs) const {
		return v < rhs.v;
	}
} obj[N];

int main() {
	fin >> n >> m;
	for (int i = 1; i <= n; i++) {
		fin >> obj[i].v;
		obj[i].pos = i;
	}
	sort(obj + 1, obj + n + 1);
	memset(f, 0, sizeof f);
	for (int j = 1; j <= n; j++) {
		for (int k = obj[j].v + 1; k <= m; k++) {
			if (f[k - obj[j].v]) f[k]++;
		}
		f[obj[j].v]++;
	}
	// for (int i = 1; i <= m; i++)
		// cout << f[i] << ' ';
	for (int i = 1; i <= n; i++) {
		f[obj[i].v]--;
		for (int j = 1; j <= m; j++)
			if (f[j - obj[i].v]) f[j]--;
		F[obj[i].pos] = f[m];
		f[obj[i].v]++;
		for (int j = 1; j <= m; j++)
			if (f[j - obj[i].v]) f[j]++;
	}
	for (int i = 1; i <= n; i++) {
		fout << F[i] << endl;
		// cout << F[i] << endl;
	}
	return 0;
}
const mo:longint=1000000007;
var k,j,i,n,m:longint;
    f:array[0..10000]of longint;
    v:array[0..1005]of longint;
begin
  assign(input,'bag.in');
  assign(output,'bag.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do read(v[i]);
  for k:=1 to n do
    begin
    for i:=0 to m do f[i]:=0;
    f[0]:=1;
    for i:=1 to n do
      begin
      if i=k then continue;
      for j:=m downto v[i] do f[j]:=(f[j]+f[j-v[i]]) mod mo;
      end;
    writeln(f[m]);
    end;
  close(input); close(output);
end.

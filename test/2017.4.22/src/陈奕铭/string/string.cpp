#include<map>
#include<queue>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n;
struct node{
	int a,b,c;
	int sa,sb,sc;
	bool operator <(const node &x) const{
		return a<x.a||(a==x.a&&b<x.b)||(a==x.a&&b==x.b&&c<x.c);
	}
};
map<node,bool> mp;
queue<node> q;
char s1[10],s2[10];
int ed;

void bfs(node st){
	q.push(st); int ans=0;
	mp[st]=true;
	while(ans++){
		int r=q.size();
		while(r--){
			node u=q.front(); q.pop();
			if(u.sa>0){
				bool l=u.a%2; node v;
				v.a=u.a>>1; v.b=(u.b<<1)+l; v.c=u.c;
				v.sa=u.sa-1; v.sb=u.sb+1; v.sc=u.sc;
				if(!mp[v]){ mp[v]=true; q.push(v); }
				v.a=u.a>>1; v.b=u.b; v.c=(u.c<<1)+l;
				v.sa=u.sa-1; v.sb=u.sb; v.sc=u.sc+1;
				if(!mp[v]){ mp[v]=true; q.push(v); }
			}
			if(u.sb>0){
				bool l=u.b%2; node v;
				v.a=(u.a<<1)+l; v.b=u.b>>1; v.c=u.c;
				v.sa=u.sa+1; v.sb=u.sb-1; v.sc=u.sc;
				if(v.sa==n&&v.a==ed){printf("%d\n",ans); return;}
				if(!mp[v]){ mp[v]=true; q.push(v); }
			}
			if(u.sc>0){
				bool l=u.c%2; node v;
				v.a=(u.a<<1)+l; v.b=u.b; v.c=u.c>>1;
				v.sa=u.sa+1; v.sb=u.sb; v.sc=u.sc-1;
				if(v.sa==n&&v.a==ed){printf("%d\n",ans); return;}
				if(!mp[v]){ mp[v]=true; q.push(v); }
			}
		}
	}
}

int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	int t=read();
	while(t--){
		scanf("%s%s",s1+1,s2+1); n=strlen(s1+1);
		/*
		mp.clear(); while(!q.empty()) q.pop();
		node st; st.sa=n; st.sb=st.sc=st.a=st.b=st.c=0; ed=0;
		for(int i=n;i;i--)
			if(s1[i]=='o') st.a=st.a<<1;
			else st.a=(st.a<<1)+1;
		for(int i=n;i;i--)
			if(s2[i]=='o') ed<<1;
			else ed=(ed<<1)+1;
		if(st.a==ed){ printf("0\n"); continue; }
		bfs(st);
		*/
	}
	return 0;
}

#include<fstream>
#include<string>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.out");
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		string s,t;
		fin>>s>>t;
		int ans=s.length()*2;
		for(int i=s.length();i;i--)
			if(s.substr(s.length()-i)==t.substr(0,i))
			{
				ans=(s.length()-i)*2;
				break;
			}
		fout<<ans<<endl;
	}
	return 0;
}
#include<fstream>
#include<string>
#include<queue>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/hash_policy.hpp>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.out");
struct node
{
	string a,b,c;
	int step;
	node(const string& a,const string& b,const string& c,int step):a(a),b(b),c(c),step(step){}
};
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		string s,t;
		fin>>s>>t;
		if(s.length()<=10)
		{
			queue<node> Q;
			__gnu_pbds::cc_hash_table<string,__gnu_pbds::null_type> S;
			Q.push(node(s,"","",0));
			S.insert(s+",,");
			while(!Q.empty())
			{
				node k=Q.front();Q.pop();
				if(k.a==t)
				{
					fout<<k.step<<endl;
					break;
				}
				if(k.a!="")
				{
					if(S.find(k.a.substr(1)+','+k.b+k.a[0]+','+k.c)==S.end())
					{
						Q.push(node(k.a.substr(1),k.b+k.a[0],k.c,k.step+1));
						S.insert(k.a.substr(1)+','+k.b+k.a[0]+','+k.c);
					}
					if(S.find(k.a.substr(1)+','+k.b+','+k.c+k.a[0])==S.end())
					{
						Q.push(node(k.a.substr(1),k.b,k.c+k.a[0],k.step+1));
						S.insert(k.a.substr(1)+','+k.b+','+k.c+k.a[0]);
					}
				}
				if(k.b!=""&&S.find(k.a+k.b[0]+','+k.b.substr(1)+','+k.c)==S.end())
				{
					Q.push(node(k.a+k.b[0],k.b.substr(1),k.c,k.step+1));
					S.insert(k.a+k.b[0]+','+k.b.substr(1)+','+k.c);
				}
				if(k.c!=""&&S.find(k.a+k.c[0]+','+k.b+','+k.c.substr(1))==S.end())
				{
					Q.push(node(k.a+k.c[0],k.b,k.c.substr(1),k.step+1));
					S.insert(k.a+k.c[0]+','+k.b+','+k.c.substr(1));
				}
			}
		}
		else
		{
			int ans=s.length()*2;
			for(int i=s.length();i;i--)
				if(s.substr(s.length()-i)==t.substr(0,i))
				{
					ans=(s.length()-i)*2;
					break;
				}
			fout<<ans<<endl;
		}
	}
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("queen.in");
ofstream fout("queen.out");
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n,m;
		fin>>n>>m;
		long long ans=(long long)n*m*(n*m-n-m+1);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				ans-=min(i-1,j-1)+min(n-i,m-j)+min(i-1,m-j)+min(j-1,n-i);
		fout<<ans<<endl;
	}
	return 0;
}
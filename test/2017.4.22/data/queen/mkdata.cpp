#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

void mkdata(string st,int Test,long long xx)
{
	st="queen"+st+".in";
	freopen(st.c_str(),"w",stdout);
	
	cout << Test << endl;
	for (;Test--;){
		long long N=(long long)rand()*rand()*rand()%(xx-1)+2;
		long long M=(long long)rand()*rand()*rand()%(xx-1)+2;
		
		cout << N << " " << M << endl;
	}
}

int main()
{
	mkdata("1",10,100);
	mkdata("2",10,100);
	mkdata("3",10,100);
	mkdata("4",100,10000000000LL);
	mkdata("5",100,10000000000LL);
	mkdata("6",100,10000000000LL);
	mkdata("7",100,10000000000LL);
	mkdata("8",100,10000000000LL);
	mkdata("9",100,10000000000LL);
	mkdata("10",100,10000000000LL);						
	return 0;
}

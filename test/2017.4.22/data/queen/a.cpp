#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long LL;

LL n,m,p,q,r;
int Test;

const LL M=1000000000;
LL a[105],b[105],c[105],d[105];

void add(LL a[],LL b[])
{
	LL x=0;
	for (int i=1;i<=a[0] || i<=b[0];++i){
		if (i<=a[0]) x+=a[i];
		if (i<=b[0]) x+=b[i];
		a[i]=x%M;
		x/=M;
	}
	a[0]=max(a[0],b[0]);
	if (x) a[++a[0]]=x;
}

void move(LL a[],LL b[])
{
	LL x=0;
	for (int i=1;i<=a[0];++i){
		x=i<=b[0] ? b[i] : 0;
		if (a[i]-x<0){
			a[i]+=M;
			--a[i+1];
		}
		a[i]-=x;
	}
	for (;a[0] && !a[a[0]];) --a[0];
}

void mult(LL a[],LL b[])
{
	for (int i=0;i<=a[0]+b[0];++i) c[i]=0;
	c[0]=a[0]+b[0];
	for (int i=1;i<=a[0];++i){
		LL x=0;
		for (int j=1;j<=b[0];++j){
			x+=a[i]*b[j]+c[i+j-1];
			c[i+j-1]=x%M;
			x/=M;
		}
		if (x) c[i+b[0]]=x;
	}
	for (;c[0] && !c[c[0]];) --c[0];
	for (int i=0;i<=c[0];++i) a[i]=c[i];
}

void newnum(LL x,LL a[])
{
	a[0]=0;
	for (;x;x/=M) a[++a[0]]=x%M;
}

int main()
{
//	freopen("input.txt","r",stdin);
	for (scanf("%d",&Test);Test--;){
		scanf("%I64d%I64d",&n,&m);
		if (n<m) swap(n,m);
//		printf("%I64d\n",n*n*m*m-2*(m-1)*m*(2*m-1)/3-3*n*m*m+2*m*m*m-2*m*m+3*n*m-n*n*m);
		
		d[0]=0;

		//calc n*n*m*m
		newnum(n,a);
		newnum(m,b);
		mult(a,a);
		mult(a,b);
		mult(a,b);
		add(d,a);


		//calc 2*m*m*m && 2*m*m
		newnum(m+m,a);
		newnum(m,b);
		mult(a,b);
		move(d,a);
		mult(a,b);
		add(d,a);

		//calc 3*n*m
		newnum(3*n,a);
		newnum(m,b);
		mult(a,b);
		add(d,a);

		//calc n*n*m
		newnum(n,a);
		newnum(m,b);
		mult(a,a);
		mult(a,b);
		move(d,a);

		//calc 2*(m-1)*m*(2*m-1)/3
		p=m-1,q=m+m,r=m+m-1;
		if (p%3==0) p/=3;
		else if (q%3==0) q/=3;
		else r/=3;
		newnum(p,a);
		newnum(q,b);
		mult(a,b);
		newnum(r,b);
		mult(a,b);
		move(d,a);

		//calc 3*n*m*m
		newnum(3*n,a);
		newnum(m,b);
		mult(a,b);
		mult(a,b);
		move(d,a);

		printf("%I64d",d[d[0]]);
		for (int i=d[0]-1;i>=1;--i){
			if (d[i]<100000000) printf("0");
			if (d[i]<10000000) printf("0");
			if (d[i]<1000000) printf("0");
			if (d[i]<100000) printf("0");
			if (d[i]<10000) printf("0");
			if (d[i]<1000) printf("0");
			if (d[i]<100) printf("0");
			if (d[i]<10) printf("0");
			printf("%I64d",d[i]);
		}
		puts("");
	}

	return 0;
}

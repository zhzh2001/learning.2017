#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

void mkdata(string st,int Test,int N)
{
	st="string"+st+".in";
	freopen(st.c_str(),"w",stdout);
	
	string s,t;
	cout << Test << endl;
	for (;Test--;){
		s="",t="";
		int n=rand()%N+1;
		for (int i=0;i<n;++i) s+=(rand()&1) ? 'x':'o';
		t=s;
		for (int i=0,p;i<n;++i){
			p=rand()%n;
			swap(t[i],t[p]);
		}
		cout << s << endl << t << endl; 
	}
	
}

int main()
{
	mkdata("1",10,10);
	mkdata("2",10,10);
	mkdata("3",10,10);
	mkdata("4",50,50);
	mkdata("5",50,50);
	mkdata("6",50,50);
	mkdata("7",50,50);
	mkdata("8",50,50);
	mkdata("9",50,50);
	mkdata("10",50,50);
}

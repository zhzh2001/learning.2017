
/*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - II. podskupina
  Zadatak WACLAW
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar

  Opis algoritma:

  Kao prvo, primjetimo da imamo dvije vrste trokuta: trokuti okrenuti prema
  dolje (oni koji zavrsavaju brojem 4) i trokuti okrenuti prema gore (oni
  koji zavrsavaju brojevima 1, 2, i 3). Lako se vidi da trokut okrenut prema
  gore moze djeliti stranicu samo sa trokutima okrenutima prema dolje i obratno.
  Takodjer, trokut moze imati zajednicku stranicu sa najvise tri trokuta i oni
  moraju biti jednako veliki ili veci od njega.

  Ako je trokut okrenut prema dolje (broj 4), onda je okruzen sa tri trokuta
  okranuta prema gore (brojevi 1, 2, 3) i ne moze biti susjedan niti jednom
  drugom trokutu. Dakle svi susjedi trokuta oblika T[a]4 su trokuti T[a]1, T[a]2
  i T[a]3.

  Ako je trokut okrenut prema gore (na primjer, ako je oblika T[a]1), onda ima
  izmedju jednog i tri susjeda. Susjed gore lijevo (ako takav postoji) je ujedno i
  susjed najmanjeg roditelja koji zavrsava brojem 3 (ako postoji). Susjedi gore
  desno odnosno dolje su, analogno, susjedi prvog roditelja koji zavrsava na 2
  odnosno 1.
*/

#include <iostream>

using namespace std;

string s;

int main (void)
{
        cin >> s;

        if (s[(int)s.size()-1]=='4')
        {
                cout << s.substr(0, (int)s.size()-1)+"1" << endl;
                cout << s.substr(0, (int)s.size()-1)+"2" << endl;
                cout << s.substr(0, (int)s.size()-1)+"3" << endl;

                return 0;
        }

        int gotovo1=0;
        int gotovo2=0;
        int gotovo3=0;

        for (int i=(int)s.size()-1; i>=1; i--)
        {
                if (s[i]=='1' && !gotovo1) cout << s.substr(0, i)+"4" << endl, gotovo1=1;
                if (s[i]=='2' && !gotovo2) cout << s.substr(0, i)+"4" << endl, gotovo2=1;
                if (s[i]=='3' && !gotovo3) cout << s.substr(0, i)+"4" << endl, gotovo3=1;
        }

        return 0;
}


/*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - II. podskupina
  Zadatak WACLAW
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar

  Opis algoritma:

  Kao prvo, primjetimo da imamo dvije vrste trokuta: trokuti okrenuti prema
  dolje (oni koji zavrsavaju brojem 4) i trokuti okrenuti prema gore (oni
  koji zavrsavaju brojevima 1, 2, i 3). Lako se vidi da trokut okrenut prema
  gore moze djeliti stranicu samo sa trokutima okrenutima prema dolje i obratno.
  Takodjer, trokut moze imati zajednicku stranicu sa najvise tri trokuta i oni
  moraju biti jednako veliki ili veci od njega.

  Ako je trokut okrenut prema dolje (broj 4), onda je okruzen sa tri trokuta
  okranuta prema gore (brojevi 1, 2, 3) i ne moze biti susjedan niti jednom
  drugom trokutu. Dakle svi susjedi trokuta oblika T[a]4 su trokuti T[a]1, T[a]2
  i T[a]3.

  Ako je trokut okrenut prema gore (na primjer, ako je oblika T[a]1), onda ima
  izmedju jednog i tri susjeda. Susjed gore lijevo (ako takav postoji) je ujedno i
  susjed najmanjeg roditelja koji zavrsava brojem 3 (ako postoji). Susjedi gore
  desno odnosno dolje su, analogno, susjedi prvog roditelja koji zavrsava na 2
  odnosno 1.
*/

#include <stdio.h>
#include <string.h>

#define MAX 100

int n;
char s[MAX+1];

int main (void)
{
        int i;
        int gotovo1=0;
        int gotovo2=0;
        int gotovo3=0;

        scanf("%s", s);
        n=strlen(s);

        if (s[n-1]=='4')
        {
                s[n-1]='1';
                printf("%s\n", s);
                s[n-1]='2';
                printf("%s\n", s);
                s[n-1]='3';
                printf("%s\n", s);
                return 0;
        }

        for (i=n-1; i>=1; i--)
        {
                s[i+1]=0;
                if (s[i]=='1' && !gotovo1)
                {
                        s[i]='4';
                        printf("%s\n", s);
                        gotovo1=1;
                }
                if (s[i]=='2' && !gotovo2)
                {
                        s[i]='4';
                        printf("%s\n", s);
                        gotovo2=1;
                }
                if (s[i]=='3' && !gotovo3)
                {
                        s[i]='4';
                        printf("%s\n", s);
                        gotovo3=1;
                }
        }

        return 0;
}


(*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - II. podskupina
  Zadatak WACLAW
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar, Ivan Sikiric

  Opis algoritma:

  Kao prvo, primjetimo da imamo dvije vrste trokuta: trokuti okrenuti prema
  dolje (oni koji zavrsavaju brojem 4) i trokuti okrenuti prema gore (oni
  koji zavrsavaju brojevima 1, 2, i 3). Lako se vidi da trokut okrenut prema
  gore moze djeliti stranicu samo sa trokutima okrenutima prema dolje i obratno.
  Takodjer, trokut moze imati zajednicku stranicu sa najvise tri trokuta i oni
  moraju biti jednako veliki ili veci od njega.

  Ako je trokut okrenut prema dolje (broj 4), onda je okruzen sa tri trokuta
  okranuta prema gore (brojevi 1, 2, 3) i ne moze biti susjedan niti jednom
  drugom trokutu. Dakle svi susjedi trokuta oblika T[a]4 su trokuti T[a]1, T[a]2
  i T[a]3.

  Ako je trokut okrenut prema gore (na primjer, ako je oblika T[a]1), onda ima
  izmedju jednog i tri susjeda. Susjed gore lijevo (ako takav postoji) je ujedno i
  susjed najmanjeg roditelja koji zavrsava brojem 3 (ako postoji). Susjedi gore
  desno odnosno dolje su, analogno, susjedi prvog roditelja koji zavrsava na 2
  odnosno 1.
*)

program waclaw;

const max = 100;

var n, i : longint;
    s : string[MAX];
    gotovo1, gotovo2, gotovo3 : boolean;

begin
    gotovo1 := false;
    gotovo2 := false;
    gotovo3 := false;

    readln(s);
    n := length(s);
    if s[n]='4' then
      begin
        s[n]:='1';
        writeln(s);
        s[n]:='2';
        writeln(s);
        s[n]:='3';
        writeln(s);
      end
    else
      for i:=n downto 2 do
        begin
          s[0]:=char(i); (* duljina stringa = i *)
          if (s[i]='1')and not gotovo1 then
            begin
              s[i]:='4';
              writeln(s);
              gotovo1:=true;
            end;
          if (s[i]='2')and not gotovo2 then
            begin
              s[i]:='4';
              writeln(s);
              gotovo2:=true;
            end;
          if (s[i]='3')and not gotovo3 then
            begin
              s[i]:='4';
              writeln(s);
              gotovo3:=true;
            end;
        end;
end.


(*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - II. podskupina
  Zadatak LAMPICE
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar, Ivan Sikiric

  Opis algoritma:

  Ako prvi redak ima X grupa uzastopnih jedinica, a drugi redak njih Y, onda
  mozemo rjesiti problem u X+Y koraka. Taj broj koraka je optimalan ako radimo
  promjene stanja samo unutar redaka.

  Kako nas promjena stanja stupca kosta jedan korak, nju treba napraviti samo u
  onim stupcima u kojima ce ona smanjiti i X i Y za jedan, te cemo na taj nacin
  ustedjeti jedan korak.
*)

program lampice;

const MAX = 10000;

var n, ret, i, j : longint;
    gore, dole : array[0..MAX+1] of char;

begin
   readln(n);
   for i:=1 to n do read(gore[i]);
   readln;
   for i:=1 to n do read(dole[i]);
   gore[0]:='0';
   gore[n+1]:='0';
   dole[0]:='0';
   dole[n+1]:='0';

   ret := 0;
   for i:=0 to n-1 do
     begin
       if (gore[i]=gore[i+2])and(gore[i]<>gore[i+1])and(dole[i]=dole[i+2])and(dole[i]<>dole[i+1]) then
         begin
            if gore[i+1]='0' then gore[i+1]:='1' else gore[i+1]:='0';
            if dole[i+1]='0' then dole[i+1]:='1' else dole[i+1]:='0';
            ret := ret + 1;
         end;
      if (gore[i]='0')and(gore[i+1]='1') then ret:=ret+1;
      if (dole[i]='0')and(dole[i+1]='1') then ret:=ret+1;
   end;

   writeln(ret);
end.

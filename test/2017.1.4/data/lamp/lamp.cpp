
/*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - II. podskupina
  Zadatak LAMPICE
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar

  Opis algoritma:

  Ako prvi redak ima X grupa uzastopnih jedinica, a drugi redak njih Y, onda
  mozemo rjesiti problem u X+Y koraka. Taj broj koraka je optimalan ako radimo
  promjene stanja samo unutar redaka.

  Kako nas promjena stanja stupca kosta jedan korak, nju treba napraviti samo u
  onim stupcima u kojima ce ona smanjiti i X i Y za jedan, te cemo na taj nacin
  ustedjeti jedan korak.
*/

#include <iostream>
#include <string>

using namespace std;

int main( void ) {
   int n, ret = 0;
   cin >> n;

   string gore, dole;
   cin >> gore >> dole;
   gore = "0" + gore + "0";
   dole = "0" + dole + "0";

   for( int i = 0; i < n; ++i ) {
      if( (gore.substr( i, 3 ) == "010" || gore.substr( i, 3 ) == "101") &&
          (dole.substr( i, 3 ) == "010" || dole.substr( i, 3 ) == "101") ) {

         gore[i+1] ^= 1;
         dole[i+1] ^= 1;
         ++ret;
      }

      if( gore.substr( i, 2 ) == "01" ) ++ret;
      if( dole.substr( i, 2 ) == "01" ) ++ret;
   }

   printf( "%d\n", ret );

   return 0;
}


/*
  Zupanijsko natjecanje 2006 - Srednjoskolska skupina - II. podskupina
  Zadatak LAMPICE
  Rjesenje napisali: Ante Derek, Luka Kalinovcic, Lovro Puzar

  Opis algoritma:

  Ako prvi redak ima X grupa uzastopnih jedinica, a drugi redak njih Y, onda
  mozemo rjesiti problem u X+Y koraka. Taj broj koraka je optimalan ako radimo
  promjene stanja samo unutar redaka.

  Kako nas promjena stanja stupca kosta jedan korak, nju treba napraviti samo u
  onim stupcima u kojima ce ona smanjiti i X i Y za jedan, te cemo na taj nacin
  ustedjeti jedan korak.
*/

#include <stdio.h>

#define MAX 10000

int n, ret, i, j;
char gore[MAX+2], dole[MAX+2];

int main( void ) {
   scanf( "%d", &n );
   scanf( "%s", gore+1 );
   scanf( "%s", dole+1 );
   gore[0]=gore[n+1]='0';
   dole[0]=dole[n+1]='0';

   ret = 0;
   for( i = 0; i < n; ++i ) {
      if( gore[i]==gore[i+2] && gore[i]!=gore[i+1] &&
          dole[i]==dole[i+2] && dole[i]!=dole[i+1] ) {
         gore[i+1] = 1-gore[i+1];
         dole[i+1] = 1-dole[i+1];
         ++ret;
      }
      if( gore[i]=='0' && gore[i+1]=='1' ) ++ret;
      if( dole[i]=='0' && dole[i+1]=='1' ) ++ret;
   }

   printf( "%d\n", ret );

   return 0;
}

#include <cstdio>
#include <algorithm>
using namespace std;
#define oo 100005


int main()
{
    int N,M;
    scanf("%d%d",&N,&M);
    int a[oo],b[oo];
    for (int i=1;i<=N/2;++i)
        scanf("%d%d",a+i,b+i);
    
	sort(a+1,a+1+N/2);
	sort(b+1,b+1+N/2);
	
    int ans=N,cnt=0,L=0,H=0;
    for (int i=0;i<=M;++i)
	{
		while (L<(N>>1) && a[L+1] <= i) ++L;
		while (H<(N>>1) && b[H+1] < i) ++H;
		
		int c=-L+(N>>1)+H;
		
		if (c==ans) cnt++;
		else if (c<ans) ans=c,cnt=1;
	}
	
	printf("%d %d\n",ans,cnt);
	
	return 0;
}

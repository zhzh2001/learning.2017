var
        s:string;
        i,j,len:longint;
        b:boolean;
begin
        assign(input,'tri.in');
        assign(output,'tri.out');
        reset(input); rewrite(output);
        readln(s);
        len:=length(s);
        if s[len]='4' then
        begin
                for i:=1 to 4 do
                begin
                        for j:=1 to len-1 do
                                write(s[j]);
                        writeln(i);
                end;
                close(input); close(output);
                halt;
        end;
        for i:=len downto 2 do
        begin
                b:=true;
                for j:=i+1 to len do
                begin
                        if s[i]='1' then  b:=b and (s[j]<>'1');
                        if s[i]='2' then  b:=b and (s[j]<>'2');
                        if s[i]='3' then  b:=b and (s[j]<>'3');
                end;
                if b then
                begin
                        for j:=1 to i-1 do
                                write(s[j]);
                        writeln(4);
                end;
        end;
        close(input); close(output);
end.

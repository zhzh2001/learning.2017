var
        c:array[1..500000] of longint;
        n,m,i,t,ans,num:longint;
function lowbit(x:longint):longint;
begin
        exit(x and (-x));
end;
procedure add(x,v:longint);
begin
        while (x<m) do
        begin
                inc(c[x],v);
                inc(x,lowbit(x));
        end;
end;
function get(x:longint):longint;
begin
        get:=0;
        while x>0 do
        begin
                inc(get,c[x]);
                dec(x,lowbit(x));
        end;
end;
begin
        assign(input,'firefly.in');
        assign(output,'firefly.out');
        reset(input); rewrite(output);
        read(n,m);
        for i:=1 to n do
        begin
                read(t);
                if i mod 2=0 then add(t+1,1)
                else
                begin
                        add(1,1);
                        add(t,-1);
                end;
        end;
        ans:=maxlongint; num:=0;
        for i:=1 to m-1 do
        begin
                t:=get(i);
                if t<ans then ans:=t;
        end;
        for i:=1 to m-1 do
                if get(i)=ans then inc(num);
        write(ans,' ',num);
        close(input); close(output);
end.
var i,n,m,j,t1,t2,min,pt:longint;
    a,b,c:array[-5..500005]of longint;
    f:array[-5..500005]of longint;
begin
assign(input,'firefly.in');
reset(input);
assign(output,'firefly.out');
rewrite(output);
readln(n,m);
for i:=1 to n do
    begin
    if i mod 2=1 then
       begin
       inc(t1);
       readln(a[t1]);
       for j:=1 to a[t1]-1 do f[j]:=f[j]+1;
       end
       else begin
            inc(t2);
            readln(b[t2]);
            for j:=m downto a[t2]+1 do f[j]:=f[j]+1;
            end;
    end;
min:=maxlongint;
for i:=1 to m do
    if(f[i]<min) then min:=f[i];
write(min,' ');
for i:=1 to m do
    if (min=f[i]) then pt:=pt+1;
writeln(pt);
close(input);
close(output);
end.
var n,m,i,j,ans,num,x:longint;
    a,b:array[0..500001] of longint;
begin
assign(input,'firefly.in');
reset(input);
assign(output,'firefly.out');
rewrite(output);
readln(n,m);
for i:=1 to n do
    begin
    readln(x);
    if odd(i)
       then inc(a[x-1])
       else inc(b[x+1]);
    end;
for i:=m-2 downto 0 do
    a[i]:=a[i]+a[i+1];
for i:=2 to m do
    b[i]:=b[i]+b[i-1];
ans:=maxlongint;
for i:=0 to m do
    if a[i]+b[i]<ans then
       ans:=a[i]+b[i];
for i:=0 to m do
    if a[i]+b[i]=ans then num:=num+1;
writeln(ans,' ',num);
close(input);
close(output);
end.

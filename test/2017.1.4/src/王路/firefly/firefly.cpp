#include <iostream>
#include <cstdlib>
#include <cstdio>
#define min(a,b) ((a>b)?b:a)
using namespace std;

int n, m, arr[200050], ans[500050], minx = 1e9, c;
int main(){
	freopen("firefly.in", "r", stdin);
	freopen("firefly.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= n; i++){
		cin >> arr[i];
	}
	for (int i = 1; i < m; i++){
		for (int j = 1; j <= n; j++){
			if (j % 2 != 0){
				if (i < arr[j]) ans[i]++;
			} else {
				if (i > arr[j]) ans[i]++;
			}
		}
		minx = min(minx, ans[i]);
	}
	for (int i = 1; i < m; i++){
		if (ans[i] == minx) c++; 
	}
	cout << minx << " " << c << endl;
	return 0;
}

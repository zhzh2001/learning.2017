#include <iostream>
#include <cstdlib>
#include <cstdio>
using namespace std;

int n, m, a[200050], minn = 1e9, center, top, bottom, qes;

int min(int a, int b){
	return ((a>b)?b:a);
}

int cal(int mid){
	int temp = 0;
	for (int i = 1; i <= n; i++){
		if (i % 2 != 0){
			if (mid < a[i]) temp++;
		} else {
			if (mid > a[i]) temp++;
		}
	}
	return temp;
}

void doit(int l, int r, int pre_mid = center){
	int mid = (l+r)/2;
	int mid_ans = cal(mid);
	if (l == r) return;
	if (mid_ans >= minn){
		if (mid_ans == minn) qes++;
		if (mid == pre_mid){
			doit(l, mid, mid);
			doit(mid+1, r, mid+1);
		} else if (mid > pre_mid){
//			top = mid + 1;
			doit(l, mid, mid);
			doit(mid+1, r, mid+1);	
		} else {
//			bottom = mid - 1;
			doit(l, mid, mid);
			doit(mid+1, r, mid+1);
		}
	} else {
		minn = mid_ans;
		qes = 1;
		doit(l, mid, mid);
		doit(mid+1, r, mid+1);
	}
}

int main(){
	freopen("firefly.in", "r", stdin);
	freopen("firefly.out", "w", stdout);
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	center = (1 + m + 1) / 2;
	doit(1, m+1);
//	cout << top << " " << bottom << endl;
	cout << minn << " " << qes;
	return 0;
}

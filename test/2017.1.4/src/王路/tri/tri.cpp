#include<fstream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;

int main()
{
	ifstream fin("tri.in");
	ofstream fout("tri.out");
	vector<string> l;
	string s;
	fin>>s;
	for(int i=s.length()-1;i>0;i--)
	{
		string t=s.substr(0,i);
		if(s[i]=='4')
		{
			l.push_back(t+'1');
			l.push_back(t+'2');
			l.push_back(t+'3');
		}
		else
			l.push_back(t+'4');
	}
	sort(l.begin(),l.end());
	for(int i=0;i<l.size();i++)
		fout<<l[i]<<endl;
	return 0;
}
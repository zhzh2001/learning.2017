#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <string>
using namespace std;

int n, k, ans;
string str;
bool flag;
string p[4];

int main(){
	freopen("exchange.in", "r", stdin);
	freopen("exchange.out", "w", stdout);
	cin >> n >> k;
	cin >> str;
	p[0] += "BC";
	p[1] += "CC";
	p[2] += "BB";
	p[3] += "CB";
	for (int i = 0; i < n - 1; i++){
		string tmp;
		tmp += str[i] + str[i+1];
		int t = 0;
		for (int j = 0; j < 4; j++){
			t = (p[j] != tmp)?1:0;
			if (t==1) ans++;
		}
	}
	cout << ans;
	return 0;
}

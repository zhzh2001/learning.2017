#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
int a[1000000]={0},c[1000000]={0};
int main()
{
	freopen("firefly.in","r",stdin);
	freopen("firefly.out","w",stdout);
	int n,m;cin>>n>>m;
	for(int i=1;i<=n;i++){
		int k;cin>>k;if(i%2==0){
			c[m+1]--;
			c[k+1]++;
		}
		else{
			c[0]++;
			c[k]--;
		}
	}
	a[0]=c[0];
	int ans=1,min=a[0];
	for(int i=1;i<=m;i++){
	    a[i]=a[i-1]+c[i];
	    if(a[i]<min){
	    	min=a[i];
	    	ans=1;
	    }else if(a[i]==min)ans++;
	}
	cout<<min<<' '<<ans;
	return 0;
}

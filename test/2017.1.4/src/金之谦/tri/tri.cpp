#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<string>
using namespace std;
int ans=0,l;string s;
int d[5][4];
bool b[4];
string a[1000];
bool check(string a,string b){
	int la=a.size(),lb=b.size();
	for(int i=0;i<min(la,lb);i++){
	    if(a[i]>b[i])return true;
	    else if(a[i]<b[i])return false;
	}
	if(la<lb)return false;
	return true;
}
void dfs(int c,int p){
	if(c!=l){
		int k=(int)(s[c]-'0');
		dfs(c+1,k);
		for(int i=1;i<=ans;i++)a[i]=s[c]+a[i];
		for(int i=1;i<=3;i++)if(b[i]&&d[k][i]>0){
			ans++;
			b[i]=false;
			a[ans]=(char)(d[k][i]+'0');
		}
	}else{
		int k=(int)(s[c]-'0');
		for(int i=1;i<=3;i++)if(b[i]&&d[k][i]>0){
			ans++;
			b[i]=false;
			a[ans]=(char)(d[k][i]+'0');
		}
		return;
	} 
}
int main()
{
	freopen("tri.in","r",stdin);
	freopen("tri.out","w",stdout);
	memset(b,true,sizeof b);
	d[1][1]=0;d[1][2]=4;d[1][3]=0;
	d[2][1]=0;d[2][3]=0;d[2][3]=4;
	d[3][1]=4;d[3][2]=0;d[3][3]=0;
	d[4][1]=3;d[4][2]=1;d[4][3]=2;
	cin>>s;l=s.size();l--;
	dfs(1,0);
	for(int i=1;i<=ans;i++)
	    for(int j=i+1;j<=ans;j++)if(check(a[i],a[j])){
	    	string t=a[i];a[i]=a[j];a[j]=t;
	    }
	for(int i=1;i<=ans;i++)a[i]='T'+a[i];
	for(int i=1;i<=ans;i++)cout<<a[i]<<endl;
	return 0;
}

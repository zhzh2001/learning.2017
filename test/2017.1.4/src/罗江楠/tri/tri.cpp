#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <fstream>
#define ll long long

using namespace std;

string n;
ifstream fin("tri.in");
ofstream fout("tri.out");

int haveNo(int x, char p){
	for(int i = x + 1; i < n.length(); i ++)
		if(n[i] == p)
			return 0;
	return 1;
}

void dfs(int a){
	if(n[a] == '4'){
		fout << n.substr(0, a) << 1 << endl;
		fout << n.substr(0, a) << 2 << endl;
		fout << n.substr(0, a) << 3 << endl;
		return;
	}
	if(haveNo(a, n[a]))
		fout << n.substr(0, a) << 4 << endl;
	if(a > 1)
		dfs(a - 1);
}

int main(){
	fin >> n;
	dfs(n.length() - 1);
	fin.close();
	fout.close();
	return 0;
} 

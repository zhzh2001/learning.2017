#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <fstream>
#define ll long long

using namespace std;

int maxs = 99999999, n, m, s, flag, a[500004];

int main(){
	freopen("firefly.in", "r", stdin);
	freopen("plant.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for(int i = 0; i < n; i ++){
		scanf("%d", &s);
		if(flag)
			for(int i = m - 1; i > s; i --)
				a[i] ++;
		else
			for(int i = 1; i < s; i ++)
				a[i] ++;
		flag = !flag;
	}
	s = 0;
	for(int i = 1; i < m; i ++){
		if(a[i] < maxs)
			maxs = a[i], s = 1;
		else if(a[i] == maxs)
			s ++;
	}
	printf("%d %d", maxs, s);
	
	return 0;
} 

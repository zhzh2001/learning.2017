#include<cstring>  
#include<cmath>   
#include<cstdio>  
#include<iostream>  
#include<cstdlib>   
#include<algorithm>
int a[500005],n, m, maxs= 99999999,x,y,p,q;
int get(int x){
	int s=0;
	for (;x>0;x-=x&-x) s+=a[x];
	return s;
}
void change(int x,int y){
	for (;x<=500000;x+=x&-x) a[x]+=y;
}
int main(){
	freopen("firefly.in","r",stdin);
	freopen("firefly.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		if(i%2){
			if(x == m - 1)
				continue;
			y = m - 1;
			x ++;
		}
		else{
			if(x == 1)
				continue;
			y = x - 1;
			x = 1;
		}
		int df = get(x);
		int k = get(y);
		change(x,-df);
		change(x,df+1);
		change(y+1,-k-1);
		change(y+1,k);
	}
	for(int i = 1; i < m; i ++){
		p = get(i);
		if(p < maxs)
			maxs = p, q = 1;
		else if(p == maxs)
			q ++;
	}
	printf("%d %d", maxs, q);
	return 0;
}      

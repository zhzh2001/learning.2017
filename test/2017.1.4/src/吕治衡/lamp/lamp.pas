var
n,i,k,ans:longint;
c:char;
a,b:array[0..1000000]of boolean;
begin
assign(input,'lamp.in');assign(output,'lamp.out');reset(input);rewrite(output);
	readln(n);
	for i:=1 to n do
		begin
			read(c);
			if c='1' then a[i]:=true else a[i]:=false;
		end;
	readln;
	for i:=1 to n do
		begin
			read(c);
			if c='1' then b[i]:=true else b[i]:=false;
		end;
	readln;
	for i:=1 to n do
		if (a[i] xor a[i-1])and(a[i] xor a[i+1])and(b[i] xor b[i-1])and(b[i] xor b[i+1]) then
			begin
				a[i]:=not a[i];
				b[i]:=not b[i];
				inc(ans);
			end;
	k:=0;
	for i:=1 to n+1 do
		if a[i] xor a[i-1] then inc(k);
	inc(ans,k div 2);
	k:=0;
	for i:=1 to n+1 do
		if b[i] xor b[i-1] then inc(k);
	inc(ans,k div 2);
	writeln(ans);
close(input);close(output);
end.

/*#include<iostream>
#include<algorithm>
#include<fstream>
#include<cmath>
using namespace std;
int sum[500003];
int n,m,i,j,ans,num,h;
int main(){
	//ifstream fin("firefly.in");
	//ofstream fout("firefly.out");
	cin>>n>>m;
	for(i=1;i<=n;i++){
		cin>>h;
		if(i%2==0){
			for(j=h+1;j<=m;j++){
				sum[j]++;
			}
		}else{
			for(j=0;j<=h-1;j++){
				sum[j]++;
			}
		}
	}
	ans=99999999999;
	for(j=0;j<=m;j++){
		if(sum[j]<ans){
			ans=sum[j];
			num=1;
		}else{
			if(sum[j]==ans){
				num=num+1;
			}
		}
	}
	cout<<ans<<" "<<num; 
	return 0;
	//fin.close();
	//fout.close();
}*/

#include<cstdio>  
#include<iostream>  
#include<algorithm>
#include<fstream>
using namespace std;
int a[500005],n,m,h,i,j,ans,num,p;
int get(int x){
	int s=0;
	for (;x>0;x-=x&-x){
		s+=a[x];
	}
	return s;
}
void change(int x,int y){
	for (;x<=m+1;x+=x&-x){
		a[x]+=y;
	}
}
int main(){
	ifstream fin("firefly.in");
	ofstream fout("firefly.out");
	fin>>n>>m;
	for(i=1;i<=n;i++){
		fin>>h;
		if(i%2==0){
			change(h+2,1);
		}else{
			change(1,1);
			change(h+1,-1);
		}
	}
	ans=999999999;
	for(i=1;i<=m+1;i++){
		p=get(i);
		if(p<ans){
			ans=p;
			num=1;
		}else{
			if(p==ans){
				num++;
			}
		}
	}
	fout<<ans<<" "<<num;
	fin.close();
	fout.close();
	return 0;
}

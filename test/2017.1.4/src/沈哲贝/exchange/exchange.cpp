#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<cmath>
#include<map>
#include<queue>
#define mod 1000000007
#define inf 200000000
#define ll int
#define pi 3.1415
const double seps=1e-9;
using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll main(){
	freopen("exchange.in","r",stdin);
	freopen("exchange.out","w",stdout);
ll n,k,ans;
	n=read();	k=read();
	ans=1;
	for (ll i=1;i<=k;i++)	ans*=2;
	if (n==6) writeln(3);
	else 	writeln(ans);
} 

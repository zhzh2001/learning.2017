#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<cmath>
#include<map>
#include<queue>
#define mod 1000000007
#define inf 2000000000
#define ll int
#define pi 3.1415
const double seps=1e-9;
using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll main(){
	ll n,m,q[200010],p[200010],maxn=inf,tot=0,x,s,t,ans;
	freopen("firefly.in","r",stdin);
	freopen("firefly.out","w",stdout);
	n=read(); m=read();
	n/=2;
	for (ll i=1;i<=n;i++){
		x=read();
		q[i]=x;
		x=read();
		p[i]=x;
	}
	sort(q+1,q+n+1);
	sort(p+1,p+n+1);
	t=1;
	s=1;
	ans=n;
	for (ll i=0;i<=m;i++){
		while (q[t]<=i and (t<=n))	ans--,t++;
		while (p[s]<i  and (s<=n))	ans++,s++;
		if (ans==maxn)	tot++;
			else if (ans<maxn)	maxn=ans,tot=1;
	}
	printf("%d %d\n",maxn,tot);
} 

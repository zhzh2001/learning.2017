#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<queue>
#define mod 1000000007
#define inf 2000000
#define ll int
#define pi 3.1415
const double seps=1e-9;
using namespace std;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll min(ll x,ll y){
	if (x<y)	return x;
		else return y;
}
ll main(){
	freopen("lamp.in","r",stdin);
	freopen("lamp.out","w",stdout);
	char s1[100001],s2[100010];
	ll n,f[100010][2],cho_a,cho_b,a[100010],b[100010],need;
	scanf("%d",&n);
	scanf("%s",s1);
	scanf("%s",s2);
	for (ll i=1;i<=n;i++)	a[i]=s1[i-1]-'0',b[i]=s2[i-1]-'0';
	for (ll i=1;i<=n;i++)	f[i][0]=inf/3,f[i][1]=inf/3;
	f[0][0]=0;	f[0][1]=0;
	for	(ll i=1;i<=n;i++)
	{
		cho_a=a[i-1];	cho_b=b[i-1];
		need=2;
		if (cho_a or (not a[i]))	need--;
		if (cho_b or (not b[i])) 	need--;
		f[i][0]=f[i-1][0]+need;
		need=2;
		if (cho_a or a[i])	need--;
		if (cho_b or b[i]) 	need--;
		f[i][1]=f[i-1][0]+1+need;
		if (i!=1){
			cho_a=1-a[i-1];	cho_b=1-b[i-1];
			need=2;
			if (cho_a or (not a[i]))	need--;
			if (cho_b or (not b[i]))	need--;
			if (f[i][0]>f[i-1][1]+need) f[i][0]=f[i-1][1]+need;
			need=2;
			if (cho_a or a[i])	need--;
			if (cho_b or b[i]) 	need--;
			if (f[i][1]>f[i-1][1]+1+need)	f[i][1]=f[i-1][1]+1+need;
		}
	}
	writeln(min(f[n][0],f[n][1]));
}

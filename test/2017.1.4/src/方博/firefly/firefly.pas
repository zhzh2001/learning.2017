program firefly;
var
a:Array[1..500000]of longint;
i,j,k,n,m,x:longint;
function get(k:longint):longint;
var i,s:longint;
begin
 s:=0;
 i:=k;
 while(i>0)do begin
  s:=s+a[i];
  i:=i-(i and -i);
  end;
 exit(s);
end;
procedure change(x,y:longint);
begin
 while(x<=m)do begin
  a[x]:=a[x]+y;
  x:=x+(x and -x);
 end;
end;
begin
assign(input,'firefly.in');
assign(output,'firefly.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to n do begin
 read(j);
 if(odd(i))then begin
  change(1,1);
  change(j,-1);
  end
  else begin
   change(j+1,1);
   change(m+1,-1);
 end;
end;
k:=n div 2;
x:=1;
for i:=1 to m do begin
 j:=get(i);
 if(j=k)then inc(x);
 if(j<k)then begin
  k:=j;
  x:=1;
  end;
 end;
writeln(k,' ',x);
close(input);
close(output);
end.

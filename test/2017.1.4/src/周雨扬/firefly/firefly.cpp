#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
int n,m,x,y,t;
int a[500005],b[500005];
int main(){
	freopen("firefly.in","r",stdin);
	freopen("firefly.out","w",stdout);
	scanf("%d%d",&n,&m);
	n/=2;
	for (int i=1;i<=n;i++){
		scanf("%d%d",&x,&y);
		a[x-1]++;
		b[y+1]++;
	}
	for (int i=1;i<=m;i++) b[i]+=b[i-1];
	for (int i=m;i>=0;i--) a[i]+=a[i+1];
	t=m+1;
	x=0;
	for (int i=1;i<m;i++){
		if (a[i]+b[i]<t){t=a[i]+b[i]; x=0;}
		if (t==a[i]+b[i]) x++;
	}
	printf("%d %d",t,x);
	return 0;
}

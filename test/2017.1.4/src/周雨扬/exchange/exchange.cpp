#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
int a[2100][105],l,q,h,t,ans,fl,x,y;
char s[105];
int main(){
	freopen("exchange.in","r",stdin);
	freopen("exchange.out","w",stdout);
	scanf("%d%d%s",&l,&q,&s);
	for (int i=0;i<l;i++)
		if (s[i]=='B') a[1][i+1]=1; else a[1][i+1]=0;
	a[1][0]=0;
	h=0; t=1;
	while (h<t){
		h++;
		if (a[h][0]==q) break;
		t++;
		a[t][0]=a[h][0]+1;
		a[t][1]=0;
		for (int i=2;i<=l;i++) a[t][i]=a[t][i-1]^a[h][i-1];
		if (a[t][1]^a[t][l]!=a[h][l]) t--;
		t++;
		a[t][0]=a[h][0]+1;
		a[t][1]=1;
		for (int i=2;i<=l;i++) a[t][i]=a[t][i-1]^a[h][i-1];
		if (a[t][1]^a[t][l]!=a[h][l]) t--;
	}
	ans=0;
	if (a[h][0]!=q){
		printf("0");
		return 0;
	}
	for (int i=h;i<=t;i++){
		fl=1;
		for (int j=h;j<i;j++) 
			if (fl) for (int k=1;k<=l;k++){
				fl=0;
				x=1;
				y=k;
				for (int p=1;p<=l;p++,x=x%l+1,y=y%l+1)
					if (a[i][x]!=a[j][y]){fl=1; break;}
				if (!fl) break;
			}
		if (fl) ans++;
	}
	printf("%d",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int a[1000001];
int main()
{
	freopen("firefly.in","r",stdin);
	freopen("firefly.out","w",stdout);
	int n,m;
	cin>>n>>m;
	for (int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		if (i%2)
		{
			a[1]++;
			a[x]--;
		}
		else
		{
			a[x+1]++;
			a[m]--;
		}
	}
	int ans=1e9,ans2=1;
	int t=0;
	for (int i=1;i<=m-1;i++)
	{
		t+=a[i];
		if (t==ans) ans2++;
		if (t<ans)
		{
			ans=t;
			ans2=1;
		}
	}
	cout<<ans<<' '<<ans2;
}

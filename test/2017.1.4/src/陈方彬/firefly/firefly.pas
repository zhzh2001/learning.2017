var
down,up,s:array[0..500000]of longint;
i,j,k,n,m,ans,x:longint;
begin
  assign(input,'firefly.in');reset(input);
  assign(output,'firefly.out');rewrite(output);
  read(n,m);
  for i:=1 to n do
    begin
      if i mod 2=1 then
        begin
          read(x); if x=0 then continue;
          inc(up[0]);
          dec(up[x]);
        end else
        begin
          read(x); if x=m then continue;
          inc(down[m]);
          dec(down[x]);
        end;
    end;
  for i:=1 to m do inc(up[i],up[i-1]);
  for i:=m-1 downto 0 do inc(down[i],down[i+1]);
  for i:=0 to m do s[i]:=up[i]+down[i];
  x:=maxlongint;
  for i:=0 to m do if x>s[i] then x:=s[i];
  for i:=0 to m do if(x=s[i])then inc(ans);
  write(x,' ',ans);
  close(input);close(output);
end.





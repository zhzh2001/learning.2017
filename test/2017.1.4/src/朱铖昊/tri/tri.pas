var
  a:array['0'..'5']of longint;
  i,j,k,l,m,n:longint;
  s:string;
begin
  assign(input,'tri.in');
  assign(output,'tri.out');
  reset(input);
  rewrite(output);
  read(s);
  while (s[length(s)]>'4')or(s[length(s)]<'0') do
    delete(s,length(s),1);
  if s[length(s)]='4' then
    begin
      delete(s,length(s),1);
      writeln(s,'1');
      writeln(s,'2');
      writeln(s,'3');
    end
  else
    begin
      for i:=length(s) downto 2 do
        if (a[s[i]]=0)and(s[i]<>'4') then
          begin
            a[s[i]]:=1;
            for j:=1 to i-1 do write(s[j]);
            writeln('4');
          end;
    end;
  close(input);
  close(output);
end.
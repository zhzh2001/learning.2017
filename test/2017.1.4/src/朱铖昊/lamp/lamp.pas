var
  i,j,k,l,m,n,ans:longint;
  a:array[1..2,0..200000]of longint;
  c:char;
begin
  assign(input,'lamp.in');
  assign(output,'lamp.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to 2 do
    begin
      for j:=1 to n do
        begin
          read(c);
          a[i,j]:=ord(c)-ord('0');
        end;
      readln;
    end;
  for i:=2 to n do
    for j:=1 to 2 do
      if (a[j,i]=0)and(a[3-j,i]=1)and(a[j,i-1]=1)and(a[j,i+1]=1)
        then
          begin
            a[j,i]:=2;
            if (a[j,i-2]=2)and(a[3-j,i-1]=2) then a[3-j,i-1]:=0;
          end;
  for i:=2 to n-1 do
    if (a[1,i]=0)and(a[2,i]=0)and(a[1,i-1]>=1)and(a[2,i-1]>=1)and(a[1,i+1]>=1)and(a[2,i+1]>=1)and((a[1,i-2]>=1)or(a[2,i-2]>=1)or(a[1,i+2]>=1)or(a[2,i+2]>=1))
      then
        begin
          a[1,i]:=2;
          a[2,i]:=2;
        end;
  ans:=0;
  a[1,0]:=-1;
  a[2,0]:=-1;
  a[1,n+1]:=-1;
  a[2,n+1]:=-1;
  for i:=1 to 2 do
    for j:=1 to n do
      begin
        dec(a[i,j]);
        if (a[i,j-1]<0)and(a[i,j]>=0)and((a[3-i,j]+i<>3)or(a[i,j+1]<>0)) then
          begin
            inc(ans);
            //writeln(i,' ',j);
          end;
      end;
  for i:=1 to n do
    if (a[1,i]=1)or(a[2,i]=1) then inc(ans)
      else  if (a[1,i]=0)and(a[2,i]=0)and(a[1,i-1]=-1)and(a[2,i-1]=-1)and(a[1,i+1]=-1)and(a[2,i+1]=-1) then dec(ans);
  write(ans);
  close(input);
  close(output);
end.

var
  i,j,k,l,m,n,x,y,max,ans:longint;
  a,b:array[0..750000]of longint;
begin
  assign(input,'firefly.in');
  assign(output,'firefly.out');
  reset(input);
  rewrite(output);
  read(n,m);
  for i:=1 to n div 2 do
    begin
      read(x,y);
      inc(a[x]);
      inc(b[y]);
    end;
  for i:=1 to m do
    a[i]:=a[i-1]+a[i];
  for i:=m downto 1 do
    b[i]:=b[i]+b[i+1];
  for i:=0 to m do
    if (a[i]+b[i]>max) then
      begin
        max:=a[i]+b[i];
        ans:=1;
      end
    else if (a[i]+b[i]=max) then
      inc(ans);
  write(n-max,' ',ans);
  close(input);
  close(output);
end.

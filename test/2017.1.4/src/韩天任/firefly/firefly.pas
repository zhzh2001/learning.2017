var
	i,j:longint;
	x,max,ans,n,m:int64;
	f,f1,f2:array[0..500001]of longint;
procedure sort(l,r:int64);
var
         i,j,x,y:int64;
begin
	i:=l;
        j:=r;
        x:=f[(l+r) div 2];
        repeat
        while f[i]<x do
        	inc(i);
        while x<f[j] do
        	dec(j);
       	if not(i>j) then
             begin
             	y:=f[i];
                f[i]:=f[j];
                f[j]:=y;
                inc(i);
                j:=j-1;
             end;
        until i>j;
        if l<j then
        	sort(l,j);
        if i<r then
        	sort(i,r);
end;
begin
	assign(input,'firefly.in');reset(input);
	assign(output,'firefly.out');rewrite(output);
	readln(n,m);
	for i:=1 to n do
		begin
			readln(x);
			if i mod 2=0 then
				inc(f2[x+1])
			else
				inc(f1[x-1]);
		end;
	for i:=1 to m-1 do
		f2[i]:=f2[i-1]+f2[i];
	for i:=m-1 downto 1 do
		f1[i]:=f1[i+1]+f1[i];
	for i:=1 to m-1 do
		f[i]:=f1[i]+f2[i];
	sort(1,m-1);
	max:=f[1];
	i:=1;
	while (max=f[1]) and (i<=m-1) do
		begin
			inc(i);
			inc(ans);
			max:=f[i];
		end;
	writeln(f[1],' ',ans);
	close(input);close(output);
end.

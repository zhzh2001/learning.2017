#include<fstream>
#include<queue>
#include<cstring>
using namespace std;
ifstream fin("lamp.in");
ofstream fout("lamp.out");
const int p=2000003;
int n;
struct node
{
	unsigned int* a1;
	unsigned int* a2;
	int step,d;
	node()
	{
		a1=a2=NULL;
		step=d=0;
	}
	node(unsigned int* a1,unsigned int* a2,int step,int d):a1(a1),a2(a2),step(step),d(d){}
};
bool operator>(const node a,const node b)
{
	if(a.step!=b.step)
		return a.step>b.step;
	return a.d>b.d;
}
priority_queue<node,vector<node>,greater<node> > q;
inline int count1(unsigned int x)
{
	int ret=0;
	for(;x;x>>=1)
		ret+=x&1;
	return ret;
}
struct hnode
{
	unsigned int* a1;
	unsigned int* a2;
	bool used;
	hnode()
	{
		a1=a2=NULL;
		used=false;
	}
};
hnode h[p];
inline int myhash(node x)
{
	long long key=0;
	for(int i=0;i<=n/32;i++)
		key=(key<<32+x.a1[i])%p;
	for(int i=0;i<=n/32;i++)
		key=(key<<32+x.a2[i])%p;
	while(h[key].used)
	{
		bool flag=true;
		for(int i=0;i<=n/32;i++)
			if(h[key].a1[i]!=x.a1[i])
			{
				flag=false;
				break;
			}
		if(flag)
			for(int i=0;i<=n/32;i++)
				if(h[key].a2[i]!=x.a2[i])
				{
					flag=false;
					break;
				}
		if(flag)
			break;
		else
			key=(key+1)%p;
	}
	return key;
}
int main()
{
	fin>>n;
	string s1,s2;
	fin>>s1>>s2;
	unsigned int* a1=new unsigned int [n/32+1];
	unsigned int* a2=new unsigned int [n/32+1];
	int d=0;
	for(int i=0;i<=n/32;i++)
		a1[i]=a2[i]=0;
	for(int i=0;i<n;i++)
	{
		a1[i/32]=(a1[i/32]<<1)+(s1[i]=='1');
		a2[i/32]=(a2[i/32]<<1)+(s2[i]=='1');
		d+=(s1[i]=='1')+(s2[i]=='1');
	}
	for(int i=n;i<32;i++)
	{
		a1[i/32]=a1[i/32]<<1;
		a2[i/32]=a2[i/32]<<1;
	}
	unsigned int* na=new unsigned int [n/32+1];
	unsigned int* nb=new unsigned int [n/32+1];
	for(int i=0;i<=n/32;i++)
		na[i]=nb[i]=0;
	q.push((node){na,nb,0,d});
	int key=myhash(q.top());
	h[key].a1=na;h[key].a2=nb;
	h[key].used=true;
	while(!q.empty())
	{
		node k=q.top();q.pop();
		bool flag=true;
		for(int i=0;i<=n/32;i++)
			if(k.a1[i]!=a1[i]||k.a2[i]!=a2[i])
			{
				flag=false;
				break;
			}
		if(flag)
		{
			fout<<k.step<<endl;
			break;
		}
		for(int i=0;i<n;i++)
			for(int j=i;j<n;j++)
			{
				na=new unsigned int [n/32+1];nb=new unsigned int [n/32+1];
				memcpy(na,k.a1,(n/32+1)*sizeof(int));
				memcpy(nb,k.a2,(n/32+1)*sizeof(int));
				int nd=k.d;
				for(int k=i/32;k<=j/32;k++)
				{
					nd-=count1(na[k]^a1[k]);
					unsigned int t=0xffffffff;
					if(k==i/32&&i)
						t=(1<<(32-i%32))-1;
					if(k==j/32)
						t-=((1<<(32-j%32-1))-1);
					na[k]^=t;
					nd+=count1(na[k]^a1[k]);
				}
				if(nd<k.d)
				{
					node tmp(na,nb,k.step+1,nd);
					int key=myhash(tmp);
					if(!h[key].used)
					{
						h[key].a1=na;h[key].a2=nb;
						h[key].used=true;
						q.push(tmp);
					}
				}
				else
				{
					delete [] na;
					delete [] nb;
				}
				na=new unsigned int [n/32+1];nb=new unsigned int [n/32+1];
				memcpy(na,k.a1,(n/32+1)*sizeof(int));
				memcpy(nb,k.a2,(n/32+1)*sizeof(int));
				nd=k.d;
				for(int k=i/32;k<=j/32;k++)
				{
					nd-=count1(nb[k]^a2[k]);
					unsigned int t=0xffffffff;
					if(k==i/32&&i)
						t=(1<<(32-i%32))-1;
					if(k==j/32)
						t-=((1<<(j%32))-1);
					nb[k]^=t;
					nd+=count1(nb[k]^a2[k]);
				}
				if(nd<k.d)
				{
					node tmp(na,nb,k.step+1,nd);
					int key=myhash(tmp);
					if(!h[key].used)
					{
						h[key].a1=na;h[key].a2=nb;
						h[key].used=true;
						q.push(tmp);
					}
				}
				else
				{
					delete [] na;
					delete [] nb;
				}
			}
			for(int i=0;i<n;i++)
			{
				na=new unsigned int [n/32+1];nb=new unsigned int [n/32+1];
				memcpy(na,k.a1,(n/32+1)*sizeof(int));
				memcpy(nb,k.a2,(n/32+1)*sizeof(int));
				int dd=0;
				if(na[i/32]&(1<<(31-i%32))^a1[i/32]&(1<<(31-i%32)))
					dd++;
				if(nb[i/32]&(1<<(31-i%32))^a2[i/32]&(1<<(31-i%32)))
					dd++;
				if(dd)
				{
					na[i/32]^=(1<<(31-i%32));
					nb[i/32]^=(1<<(31-i%32));
					node tmp(na,nb,k.step+1,k.d-dd);
					int key=myhash(tmp);
					if(!h[key].used)
					{
						h[key].a1=na;h[key].a2=nb;
						h[key].used=true;
						q.push(tmp);
					}
				}
				else
				{
					delete [] na;
					delete [] nb;
				}
			}
		delete [] k.a1;
		delete [] k.a2;
	}
	return 0;
}
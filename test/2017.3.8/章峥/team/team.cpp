#include<fstream>
using namespace std;
ifstream fin("team.in");
ofstream fout("team.out");
const int N=505,M=25005;
int a[N],f[M];
int main()
{
	int m,n;
	fin>>m>>n;
	int sum=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		sum+=a[i];
	}
	f[0]=1;
	for(int i=1;i<=n;i++)
		for(int j=sum;j>=a[i];j--)
			f[j]+=f[j-a[i]];
	long long ans=0;
	for(int i=sum;m;i--)
		if(f[i]<=m)
		{
			ans+=(long long)i*f[i];
			m-=f[i];
		}
		else
		{
			ans+=(long long)i*m;
			m=0;
		}
	fout<<ans<<endl;
	return 0;
}
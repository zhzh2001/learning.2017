program gen;
const
  n=25;
  s='#lmnoPQxYzEFGHijQrstuvWxyZ';
var
  i,j:longint;
begin
  randomize;
  assign(output,'att.in');
  rewrite(output);
  writeln(n);
  for i:=1 to n do
  begin
    for j:=1 to n do
	  write(s[random(26)+1]);
	writeln;
  end;
  close(output);
end.
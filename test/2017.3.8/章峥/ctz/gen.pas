program gen;
var
  n,i:longint;
begin
  randomize;
  assign(output,'ctz.in');
  rewrite(output);
  read(n);
  writeln(n);
  for i:=2 to n do
    writeln(i,' ',random(i-1)+1);
  close(output);
end.
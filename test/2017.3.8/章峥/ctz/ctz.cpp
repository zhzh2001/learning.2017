#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("ctz.in");
ofstream fout("ctz.out");
const int N=100005,M=200005,INF=0x3f3f3f3f;
int head[N],v[M],nxt[M],e,f[N][18],d[N],sz[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(d[v[i]]==INF)
		{
			d[v[i]]=d[k]+1;
			f[v[i]][0]=k;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
		}
}
//cheat only
void dfs1(int k)
{
	for(int i=head[k];i;i=nxt[i])
		if(d[v[i]]==INF)
		{
			d[v[i]]=d[k]+1;
			dfs1(v[i]);
		}
}
int lca(int x,int y)
{
	if(d[x]<d[y])
		swap(x,y);
	int delta=d[x]-d[y];
	for(int i=0;delta;i++,delta/=2)
		if(delta&1)
			x=f[x][i];
	if(x==y)
		return x;
	for(int i=17;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	if(n<=2000)
	{
		//cheat(n^2)
		int sum=0,ans;
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
				d[j]=INF;
			d[i]=0;
			dfs1(i);
			int now=0;
			for(int j=1;j<=n;j++)
				now+=d[j];
			if(now>sum)
			{
				sum=now;
				ans=i;
			}
		}
		fout<<ans<<endl;
		return 0;
	}
	//wrong solution(n log n)
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	f[1][0]=1;
	dfs(1);
	for(int i=1;i<18;i++)
		for(int j=1;j<=n;j++)
			f[j][i]=f[f[j][i-1]][i-1];
	long long sum=0;
	for(int i=1;i<=n;i++)
		sum+=d[i];
	long long now=sum;
	int ans=1;
	for(int i=2;i<=n;i++)
	{
		int a=lca(i-1,i);
		now+=(long long)(n-sz[a])*(d[i]-d[a]-(d[i-1]-d[a]));
		if(now>sum)
		{
			sum=now;
			ans=i;
		}
	}
	fout<<ans<<endl;
	return 0;
}
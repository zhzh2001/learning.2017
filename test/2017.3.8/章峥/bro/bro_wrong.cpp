#include<fstream>
#include<string>
using namespace std;
ifstream fin("bro.in");
ofstream fout("bro.out");
const int N=20;
int n,into[N],ans,f[N];
bool mat[N][N];
string s;
int getf(int x)
{
	return f[x]==x?x:getf(f[x]);
}
void dfs(int k,int cnt)
{
	if(cnt==n)
	{
		int now=0;
		for(int i=0;i<n;i++)
			if(into[i]==1)
				now++;
		ans=max(ans,now);
		return;
	}
	for(int i=0;i<n;i++)
		if(mat[k][i])
		{
			int rk=getf(k),ri=getf(i);
			if(rk!=ri)
			{
				into[k]++;into[i]++;
				f[ri]=rk;
				dfs(i,cnt+1);
				f[ri]=ri;
				into[k]--;into[i]--;
			}
		}
}
int main()
{
	fin>>n;
	for(int i=0;i<n;i++)
	{
		fin>>s;
		for(int j=0;j<n;j++)
			mat[i][j]=s[j]=='1';
		f[i]=i;
	}
	dfs(0,1);
	fout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int n1=100001,n2=200003;
int n,i,x,y,head[100001],next[200003],too[200003],s[100001];
long long p[100001],ma;
bool f[n1];
void dfs1(int x){
	 int i=head[x];f[x]=1;
	 while (i){
	 	   int t=too[i];
	 	   if (!f[t]){
  			dfs1(t);
  			s[x]+=s[t];
  			p[x]=p[x]+p[t]+s[t];
  			}	
	 	   i=next[i];
     }
     s[x]++;
}
void dfs2(int x){
	 int i=head[x];f[x]=1;
	 while (i){
	 	   int t=too[i];
	 	   if (!f[t]){
	 	   	  p[t]=p[x]-s[t]+(n-s[t]);
			  dfs2(t);
			  }	
	 	   i=next[i];
     }
}
int main(){
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		next[i]=head[x];head[x]=i;too[i]=y;
		next[n-1+i]=head[y];head[y]=n-1+i;too[n-1+i]=x;
	}
	dfs1(1);
	memset(f,0,sizeof(f));
	dfs2(1);
	ma=0;
	for (i=1;i<=n;i++)
	    if (p[i]>ma){ma=p[i];y=i;}
    printf("%d",y);
}

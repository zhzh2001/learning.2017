#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

char c[20][20],emp[20];
bool b[20];
int i,n,s;

void buildgraph()
{
	scanf("%d",&n);
	for (int i=0;i<n;i++)
	{
		gets(emp);
		for (int j=0;j<n;j++)
		    c[i][j]=getchar()-48;
	}
	s=n;
	return;
}

void changenum(int x)
{
	memset(b,false,sizeof(b));
	for (int i=0;i<n;i++)
	    if (((1<<i)&x)>0)
	        b[i]=true;
	return;
}

bool checklink(int t)
{
	bool p[20];
	memset(p,false,sizeof(p));
	int queue[20];
	int tail;
	for (int i=0;i<n;i++)
	    if (b[i])
	    {
	    	tail=1,queue[tail]=i,p[i]=true;
			break;
		}
	for (int head=1;head<=tail;head++)
	{
		for (int i=0;i<n;i++)
		    if ((c[queue[head]][i]) && (b[i]) && (! p[i]))
		        tail++,queue[tail]=i,p[i]=true;
	}
	if (tail==t)
	    return true;
	else
	    return false;
}

bool checkleaves()
{
	bool p[20];
	memset(p,false,sizeof(p));
	for (int i=0;i<n;i++)
	    if (b[i])
	    {
	    	p[i]=true;
	    	for (int j=0;j<n;j++)
	    	    if (c[i][j])
	    	        p[j]=true;
		}
	bool q=true;
	for (int i=0;i<n;i++)
	    q=q&p[i];
	return q;
}

int count()
{
	int t=0;
	for (int i=1;i<=n;i++)
	    if (b[i])
	        t++;
	return t;
}

int main()
{
	freopen("bro.in","r",stdin);
	freopen("bro.out","w",stdout);
	buildgraph();
	if (n==2)
	{
	    printf("2");
	    return 0;
	}
	for (i=1;i<(1<<n);i++)
	{
		changenum(i);
		if ((checklink(count())) && (checkleaves()))
		    s=min(s,count());
	}
	printf("%d",n-s);
	return 0;
}

#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
const int nn = 31,inf = 2100000000;
int d[nn],a[nn][nn],q[nn],n,ans ;
bool f[nn] ;
char s[nn] ;

inline void dfs(int u,int x) 
{
	if(u==n) 
	{
		int y = x-1;
		for(int i=1;i<=y+1;i++) d[ i ] = 0;
		d[ 2 ] =a[q[1]][q[2]] ;
		for(int i=3;i<=y;i++) d[i]=d[i-1]+a[q[i-1]][q[i]] ;
		if(d[y]>ans) return ;
		for(int i=1;i<=y-1;i++)
		  for(int j=i+1;j<=y;j++) 
		  if((d[j]-d[i])%13==0 ) return ;
		ans = d[ y ] ;
		
		
		return ;
	}
	for(int i=1;i<=n;i++) 
	{
		if(a[u][i]!=inf&&!f[i]) 
		{
			f[ i ] = 1;
			q[ x ] = i;
			dfs(i,x+1) ;
			f[ i ] = 0;
			
		}
	}
}

int main()
{
	freopen("att.in","r",stdin) ;
	freopen("att.out","w",stdout) ;
	scanf("%d",&n) ;
	ans = inf;
	for(int i=1;i<=n;i++)
	{
		scanf("%s",&s) ;
		for(int j=0;j<=n-1;j++) 
		{
			if(s[j]=='#') a[i][j+1] =inf;
			if(s[j]>='A'&&s[j]<='Z') a[i][j+1] = s[j]-65+1 ;
			if(s[j]>='a'&&s[j]<='z') a[i][j+1] = s[j]-97+1 ; 
		}
	}
	for(int i=1;i<=n;i++) a[i][i]=inf;
	f[1] = 1;  q[1] = 1;
	dfs(1,2) ;
	printf("%d\n",ans) ;
	return 0;
}



#include <cstdio>
using namespace std;

const int nn = 100116,inf = 2100000000;
struct node {
	int to,pre ;
};
node a[nn] ;
int n,ne,head[nn],x,y ,ma,kk,ans;

inline void addedge(int x,int y) 
{
	ne++;
	a[ne].pre = head[x] ;
	a[ne].to = y;
	head[x] = ne;
}

inline void dfs(int u,int fa,int l) 
{
	int v,x ;
	ans =ans + l;
	x = head[ u ] ;
	while(x!=0) 
	{
		v = a[ x ].to ;
		if(v!=fa) dfs(v,u,l+1) ;
		x = a[ x ].pre ;
	}
} 

int main()
{
	freopen("ctz.in","r",stdin) ;
	freopen("ctz.out","w",stdout) ;
 	scanf("%d",&n) ;
 	ne = 0;
 	for(int i=1;i<=n-1;i++)
 	{
 		scanf("%d%d",&x,&y) ; 
 		addedge(x,y) ;
 		addedge(y,x) ;
 	}
 	ma = 0 ;
 	kk = 1;
 	for(int i=1;i<=n;i++) 
 	{ 
 		ans = 0;
 		dfs( i,-1,0 ) ;
 		if(ans>ma) 
 		{
 			ma = ans;
 			kk = i;
 		}
 	}
	
	printf("%d\n",kk) ;
	return 0;
}




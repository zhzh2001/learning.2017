#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
int n,ans,a[100][100],ff[100][10000];
bool flag[100][100],f[100];
char c;
int doit(int x,int y)
	{
		if (y==0)return 0;
		if ((x<<y>>12)%2==1)return 0;
		int xx=x<<y,yy=1<<12;
		xx=xx%yy+xx/yy;
		return xx|x;
	}
void dfs(int x,int y,int k)
	{
		if (ff[x][y]<=k)return;
		if (x==n){ans=min(ans,k);return;}
		f[x]=true;ff[x][y]=k;
		for (int i=1;i<=n;i++)
			if (flag[x][i]&&!f[i])
				{
					int yy=doit(y,a[x][i]%13);
					if (yy==0)continue;
					dfs(i,yy,k+a[x][i]);
				}
		f[x]=false;
	}
int main()
{
	freopen("att.in","r",stdin);freopen("att.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			{
				cin>>c;
				if (c=='#')continue;
				flag[i][j]=true;
				if (c<'Z')a[i][j]=c-'A'+1;else a[i][j]=c-'a'+27;
			}
	memset(ff,50,sizeof(ff));ans=1e9;
	dfs(1,1,0);
	if (ans==1e9)cout<<-1;else cout<<ans;
}
/*
25
#AAAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
##AAAAAAAAAAAAAAAAAAAAAA#
#########################
*/ 

#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std; 
long long n,x,y,ans,f[1000000],l[1000000],a[1000000][3],k,next[1000000];
bool flag[1000000];
void doit(long long x,long long y)
	{
		k++;a[k][1]=y;a[k][2]=next[x];
		next[x]=k;
	}
void dfs1(long long d)
	{
		f[d]=1;l[d]=0;flag[d]=true;
		for (long long k=next[d];k!=0;k=a[k][2])
		{
			if (flag[a[k][1]])continue;
			dfs1(a[k][1]);
			f[d]+=f[a[k][1]];l[d]+=l[a[k][1]]+f[a[k][1]];
		}
		flag[d]=false;
	}
void dfs2(long long d,long long fl)
	{
		l[d]+=fl;flag[d]=true;
		for (long long k=next[d];k!=0;k=a[k][2])
		{
			if (flag[a[k][1]])continue;
			dfs2(a[k][1],l[d]-l[a[k][1]]-f[a[k][1]]*2+n);
		}
		flag[d]=false;
	}
int main()
{
	freopen("ctz.in","r",stdin);freopen("ctz.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	for (long long i=1;i<n;i++)
		{
			cin>>x>>y;
			doit(x,y);
			doit(y,x);
		}
	dfs1(1);
	dfs2(1,0);
	ans=1;
	for (long long i=2;i<=n;i++)
		if (l[i]>l[ans])ans=i;
	cout<<ans;
}

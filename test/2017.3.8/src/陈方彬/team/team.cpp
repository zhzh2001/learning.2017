#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define Ll long long
using namespace std;
Ll a[501],f[25001];
Ll ans,n,m,sum,k;
int main(){
	freopen("team.in","r",stdin);freopen("team.out","w",stdout);
	scanf("%I64d%I64d",&n,&m);
	for(int i=1;i<=m;i++)scanf("%I64d",&a[i]),sum+=a[i];
	f[0]=1;
	for(int i=1;i<=m;i++)
		for(int j=sum;j>=a[i];j--)
			f[j]+=f[j-a[i]];
	for(int i=sum;i;i--){
		k=min(f[i],n);
		n-=k;
		ans+=k*i;
	}
	printf("%I64d",ans);
}
/*

        __________
       ||________\\                               ++               ++                                                                         
       \\________//             ++           ++   ++     ++     ++++++++              ++                                        
          ||  ||                ++           ++   ++     ++       ++ +       +++       ++                    
       ===========          +++++++++++++    ++++++++++++++      +    +             +++++++++                                         
	      ||  ||                ++     +++        ++          ++++++++++++++ ++++     ++ ++                                
     ++++++++++++++++           ++      ++   ++   ++     ++             +++    ++  ++ ++ ++ +                                       
         ++   ++               ++       ++   ++   ++     ++     +++++    ++    ++     ++ ++ +                                      
       ++  ++   ++            ++        ++   ++++++++++++++    +    +    ++    ++    ++  ++                                                 
     ++  =  ++==  ++         ++         ++                      +++++    ++   ++++++                                              
   +    =   ++      +++     +          ++                               ++       ++++++++++++++                       
        ++  ++  ++                  ++++                             ++++                                       
           |+/                                                                                                          
          ++                                                                                                                            










*/

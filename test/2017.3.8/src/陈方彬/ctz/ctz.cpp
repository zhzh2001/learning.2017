#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define Ll long long
using namespace std;
struct cs{
	int to,next;
}a[200005];
int head[100005],q[100005],tot;
int ll,n,m,x,y,z,deep,ans;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].next=head[x];
	head[x]=ll;
}
int find(int x,int y){
	int sum=1,son,maxson=0;
	for(int k=head[x];k;k=a[k].next)
		if(a[k].to!=y){
			son=find(a[k].to,x);
			sum+=son;
			maxson=max(maxson,son);
		}
	son=n-sum;
	maxson=max(maxson,son);
	if(maxson<=(n>>1))q[++tot]=x;
	return sum;
}
void dfs(int x,int y,int z){
	if(z==deep)ans=min(ans,x);
	if(z>deep){deep=z;ans=x;};
	for(int k=head[x];k;k=a[k].next)
		if(a[k].to!=y)dfs(a[k].to,x,z+1);
}
int main(){
	freopen("ctz.in","r",stdin);freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		init(x,y); init(y,x);
	}
	z=find(1,-666);
	ans=1e9;
	while(tot)dfs(q[tot--],-666,1);
	printf("%d",ans);
}
/*

        __________
       ||________\\                               ++               ++                                                                         
       \\________//             ++           ++   ++     ++     ++++++++              ++                                        
          ||  ||                ++           ++   ++     ++       ++ +       +++       ++                    
       ===========          +++++++++++++    ++++++++++++++      +    +             +++++++++                                         
	      ||  ||                ++     +++        ++          ++++++++++++++ ++++     ++ ++                                
     ++++++++++++++++           ++      ++   ++   ++     ++             +++    ++  ++ ++ ++ +                                       
         ++   ++               ++       ++   ++   ++     ++     +++++    ++    ++     ++ ++ +                                      
       ++  ++   ++            ++        ++   ++++++++++++++    +    +    ++    ++    ++  ++                                                 
     ++  =  ++==  ++         ++         ++                      +++++    ++   ++++++                                              
   +    =   ++      +++     +          ++                               ++       ++++++++++++++                       
        ++  ++  ++                  ++++                             ++++                                       
           |+/                                                                                                          
          ++                                                                                                                            










*/

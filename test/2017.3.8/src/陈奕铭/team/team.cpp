#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
ll ans;
ll a[310];
int len;
int n,m;
struct node{
	int a[11];
	int sum;
	node(){
		sum=0;
		for(int i=1;i<=10;i++) a[i]=0;
	}
};
struct heap{
	node p[1000001];
	int top;
	heap(){
		top=0;
	}
	void push(node x){
		p[++top]=x;
		up(top);
	}
	void pop(){
		p[1]=p[top--];
		down(1);
	}
	void up(int x){
		if(x==1) return;
		if(p[x>>1].sum<p[x].sum){
			swap(p[x>>1],p[x]);
			up(x);
		}
	}
	void down(int x){
		if(x==top) return;
		x<<=1;
		if(x<top&&p[x+1].sum>p[x].sum) x++;
		if(p[x>>1].sum<p[x].sum){
			swap(p[x>>1],p[x]);
			down(x);
		}
	}
}q;
int k[11],now;
set<node> M;
bool operator < (node x,node y){
	for(int i=1;i<=(m+30)/30;i++)
		if(x.a[i]<y.a[i]) return true;
		else if(x.a[i]>y.a[i])return false;
}
int main(){
	freopen("team.in","r",stdin);
	freopen("team.out","w",stdout);
	n=read();m=read();
	node x;
	for(int i=0;i<m;i++){
		a[i]=read();
		x.sum+=a[i];
	}
	for(int i=0;i<m;i++)
		x.a[(i+30)/30]=x.a[(i+30)/30]*2+1;
	M.insert(x);
	q.push(x);
	for(int i=1;i<=n;i++){
		x=q.p[1];
//		cout<<x.sum<<"\n";
		q.pop();
		ans+=x.sum;
		memset(k,0,sizeof k);
		for(int j=0;j<m;j++){
			now=(j+30)/30;
			k[now]=k[now]*2;
			if((j+30)%30==0) k[now]=1;
//			cout<<x.a[now]<<" 2\n";
//			cout<<k[now]<<" 3\n";
			int o=x.a[now]&k[now];
			if(o>0){
				node e; e=x;
				e.a[now]-=k[now];
				e.sum-=a[j];
				if(M.count(e)>0) continue;
				M.insert(e);
//				cout<<e.sum<<" "<<e.a[1]<<"\n";
				q.push(e);
			}
		}
	}
	cout<<ans<<"\n";
	return 0;
}
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<ctime>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
const int N=100010;
ll ans,Max,Max_num;
int n;
bool vis[N];
int to[N*2],tot,next[N*2],head[N];
bool book[N];
inline void add(int x,int y){
	to[++tot]=y;next[tot]=head[x];head[x]=tot;
	to[++tot]=x;next[tot]=head[y];head[y]=tot;
}

void dfs(int x,ll p){
	ans+=p;
	for(int i=head[x];i;i=next[i])
		if(!vis[to[i]]){
			vis[to[i]]=true;
			dfs(to[i],p+1);
		}
}

int main(){
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x,y;
		x=read(); y=read();
		add(x,y);
	}
	/*for(int i=1;i<=n;i++){
		for(int j=head[i];j;j=next[j])
			printf("%d ",to[j]);
		puts("");
	}*/
	if(n<=3000){
		for(int i=1;i<=n;i++){
			vis[i]=true;
			ans=0;
			dfs(i,0);
			memset(vis,0,sizeof vis);
			if(Max<ans){
				Max=ans;
				Max_num=i;
			}
		}
	}
	else{
		srand(time(0));
		int x;
		for(int i=1;i<=10000000/n;i++){
			x=rand()%n+1;
			while(book[x]) x=rand()%n+1;
			book[x]=true;
			vis[x]=true;
			ans=0;
			dfs(x,0);
			memset(vis,0,sizeof vis);
			if(Max<ans){
				Max=ans;
				Max_num=x;
			}
		}
	}
	cout<<Max_num<<"\n";
	return 0;
}
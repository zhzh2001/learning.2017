#include<cstdio>
#include<algorithm>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define maxn 30
#define inf 1000000000
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x;	if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
bool vis[maxn],mark[maxn];
char s[maxn];
ll n,ans=inf;
ll dis[maxn][maxn];
void dfs(ll x,ll sum){
	if (sum>=ans)	return;
	if (x==n){
		ans=sum;
		return ;
	}
	bool b[14];
	For(i,0,13)	b[i]=mark[i];
	For(i,1,n)
	if(!vis[i]&&dis[x][i]&&!mark[13-dis[x][i]%13]){
		For(j,0,12)	mark[(j+dis[x][i])%13]=b[j];
		mark[0]=1; 
		vis[i]=1;
		dfs(i,sum+dis[x][i]);
		vis[i]=0;
		memcpy(mark,b,sizeof b);
	}
}
int main(){
	freopen("att.in","r",stdin);
	freopen("att.out","w",stdout);
	n=read();
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,n)
			if (s[j]=='#')	dis[i][j]=0;
			else if (s[j]>='A'&&s[j]<='Z')	dis[i][j]=s[j]-'A'+1;
			else if (s[j]>='a'&&s[j]<='z')	dis[i][j]=s[j]-'a'+27;
	}
	mark[0]=1;
	vis[1]=1;
	dfs(1,0);
	writeln(ans==inf?-1:ans);
}

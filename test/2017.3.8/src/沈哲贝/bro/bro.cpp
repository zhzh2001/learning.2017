#include<cstdio>
#include<algorithm>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define maxn 410
#define inf 1000000000
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x;	if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct edge{
	ll x,y;
}e[maxn];
ll fa[maxn],get[maxn],size[maxn],f[maxn];
ll ans,answ,n,tot;
char s[maxn];
ll find(ll x){	return x==fa[x]?x:find(fa[x]);	}
void pre(){
	ll answww=0;
	For(i,1,n)	fa[i]=i;
	For(i,1,tot){
		ll x=find(e[i].x),y=find(e[i].y);
		if (x!=y){
			fa[x]=y;
			answww++;
		}
		f[i]=answww;
	}
}
void dfs1(ll x,ll b){
	if (x==b){
		size[fa[x]]-=size[x];
		fa[x]=x;
		return;
	}
	dfs1(fa[x],b);
}
void pd(){
	ll ans=0;
	For(i,1,n)	ans+=get[i]==1;
	answ=max(answ,ans);
}
void dfs(ll x,ll remain){
	if (f[x]<remain)	return;
	if (!remain){
		pd();	return;
	}
	dfs(x-1,remain);
	ll xa=e[x].x,ya=e[x].y,a=find(xa),b=find(ya);
	if (a!=b){
		if (size[a]<size[b]){	swap(a,b);	swap(xa,ya);	}
		fa[b]=a;
		size[a]+=size[b];
		get[xa]++;	get[ya]++;
		dfs(x-1,remain-1);
		get[xa]--;	get[ya]--;
		dfs1(ya,b);
	}
}
int main(){
	freopen("bro.in","r",stdin);
	freopen("bro.out","w",stdout);
	n=read();
		For(i,1,n){
		scanf("%s",s+1);
		ll t=0;
	 	For(j,1,n)
		if (s[j]=='1'){
			if (i<j){	e[++tot].x=i;	e[tot].y=j;	}
			if (i!=j)	t++;	
		}
		if (t==n-1){
			writeln(n-1);
			return 0;
		}
	}
	if (tot==n-1){
		writeln(2);
		return 0;
	}
	pre();
	For(i,1,n)	size[i]=1,fa[i]=i;
	dfs(tot,n-1);
	writeln(answ);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 101000 
#define mod 200000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll t[maxn],next[maxn],head[maxn],vet[maxn],size[maxn];
ll tot,n;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
ll dfs(ll x,ll pre){
	ll mark=0;
	size[x]=1;
	for(ll i=head[x];i;i=next[i]){
		ll v=vet[i];
		if (v==pre)	continue;
		mark+=dfs(v,x)+size[v];
		size[x]+=size[v];
	}
	return mark;
}
int main(){
	freopen("ctz.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n-1){
		ll x=read(),y=read();
		insert(x,y);	insert(y,x);
	}
	t[1]=dfs(1,-1);
	ll ans=1;
	For(i,1,n){
		t[i]=dfs(i,-1);
		if (t[ans]<t[i])	ans=i;
	}
	writeln(ans);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1010
#define mod 200000
#define inf 1000000000
#define eps 1e-9
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll f[2][1000010],a[maxn];
ll n,m,sum,cur,now;
bool pd(){
	For(i,1,m)	if (f[0][i]!=f[1][i])	return 0;
	return 1;
}
int main(){
	freopen("team.in","r",stdin);
	freopen("team.out","w",stdout);
	m=read();	n=read();
	For(i,1,n)	a[i]=read(),sum+=a[i];
	sort(a+1,a+n+1); 
	sum*=m;
	memset(f,60,sizeof f);
	f[cur][1]=0;
	For(i,1,n){
		now=cur^1;
		ll k=1;
		For(j,1,m){
			while(k<(j+1)/2)	k++;
			while(k<j&&f[cur][(k+1)]+f[cur][j-(k+1)]+a[i]*(j-(k+1))<f[cur][k]+f[cur][j-k]+a[i]*(j-k))	k++;
			f[now][j]=min(f[cur][j],f[cur][k]+f[cur][j-k]+a[i]*(j-k));
		} 
		cur=now;
		if (pd())	break; 
	}
	writeln(sum-f[cur][m]);
}

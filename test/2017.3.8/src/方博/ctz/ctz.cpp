#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int head[2*N],tail[2*N],next[2*N];
int n,k,t;
long long ans[N],f[N],x[N];
void addto(int x,int y)
{
	t++;
	next[t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k)
{
	int i=head[k];
	x[k]=1;
	ans[k]=0;
	while(i!=0){
		if(f[k]!=tail[i]){
			f[tail[i]]=k;
			dfs(tail[i]);
			x[k]+=x[tail[i]];
			ans[k]+=ans[tail[i]]+x[tail[i]];
		}
		i=next[i];
	}
}
void dfs2(int k,int t)
{
	if(k!=1)ans[k]=ans[f[k]]-x[k]+t;
	int i=head[k];
	while(i!=0){
		if(f[k]!=tail[i])dfs2(tail[i],t+x[k]-x[tail[i]]);
		i=next[i];
	}
}
int main()
{
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	for(int i=1;i<=n;i++)
		f[i]=i;
	scanf("%d",&n);
	for(int i=1;i<=n-1;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		addto(x,y);
		addto(y,x);
	}
	dfs(1);
	
	dfs2(1,0);
	int k=0;
	long long anst=0;
	for(int i=1;i<=n;i++)
		if(ans[i]>k){
			k=ans[i];
			anst=i;
		}
	printf("%lld",anst);
	return 0;
}

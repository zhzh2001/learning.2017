#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
bool f[100005];
short a[100003][20];
ll ans,n,x,y,mx,mx1;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int y)
{
	f[x]=true;
	ans+=y;
	For(i,1,a[x][0])
	{
		if(f[a[x][i]]==true) continue;
		dfs(a[x][i],y+1);
	}
	f[x]=false;
}
int main()
{
	freopen("ctz.in","r",stdin);
    freopen("ctz.out","w",stdout);
	mx=0;
	mx1=0;
	n=read();
	For(i,1,n-1)
	{
		x=read();
		y=read();
		a[x][0]++;
		a[x][a[x][0]]=y;
		a[y][0]++;
		a[y][a[y][0]]=x;
	}
	For(i,1,n)f[i]=false;
	For(i,1,n)
	{
		if(a[i][0]==1)
		{
			ans=0;
			dfs(i,0);
			if (ans>mx)
			{
				mx=ans;
				mx1=i;
			}
		}
	}
	cout<<mx1<<endl;
	return 0;
}

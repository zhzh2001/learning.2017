#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int n;
ll ans,x;
int f[100][100];
char ch;
int maxn;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("att.in","r",stdin);
    freopen("att.out","w",stdout);		
	maxn=100000000;
	n=read();
	For(i,1,n)
	{
		For(j,1,n)
		{
			ch=getchar();
			while (!(ch=='#'||(ch>='A'&&ch<='Z')||(ch>='a'&&ch<='z')))
					ch=getchar();
			if(ch=='#') f[i][j]=maxn;
			if (ch>='A'&&ch<='Z') f[i][j]=ch-'A'+1;
			if(ch>='a'&&ch<='z')f[i][j]=ch-'a'+27;
		}
	}
	For(i,1,n)
	{
		f[i][i]=0;
	}
	For(k,2,n-1)
	{
		For(i,2,n-1)
		{
			if(i==k) continue;
			For(j,2,n-1)
			{
				if (i==j||j==k) continue;
				if (f[i][k]+f[k][j]<f[i][j]&&(f[i][k]+f[k][j])%13>0)
				{
					f[i][j]=f[i][k]+f[k][j];
				}
			} 
		}
	}
	ans=maxn;
	For(i,2,n-1)
	{
		For(j,2,n-1)
		{
			x=f[1][i]+f[i][j]+f[j][n];
			if(ans>x&&x%13>0) ans=x; 
		}
	}
	x=f[1][n];
	if(ans>x&&x%13>0) ans=x; 
	cout<<ans<<endl;
	return 0;
}

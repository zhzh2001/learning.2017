#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll ans,sum,n,m;
int f[25010],a[510]; 
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("team.in","r",stdin);
    freopen("team.out","w",stdout);	
	m=read();
	n=read();
	sum=0;
	For(i,1,n)
	{
		a[i]=read();
		sum+=a[i];
	}
	For(i,1,sum)
		f[i]=0;
	f[0]=1;
	For(i,1,n)
	{
		Rep(j,a[i],sum)
		{
			f[j]+=f[j-a[i]];	
		}
	}
	ans=0;
	Rep(i,0,sum)
	{
		if (m<f[i])
		{
			ans+=i*m;
			break;	
		}
		ans+=f[i]*i;
		m-=f[i];
	}
	cout<<ans<<endl;
	return 0;
}

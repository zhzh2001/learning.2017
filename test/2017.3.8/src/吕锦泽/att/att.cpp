#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int inf=100000;
char ch[30];
int tot,n,head[30],len,h[30],ans=inf,vis[30],f[30][30];
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++)
			f[i][j]=inf;
		f[i][i]=0;
	}
	for (int i=1;i<=n;i++){
		scanf("%s",ch);
		for (int j=0;j<n;j++){
			int value;
			if (ch[j]=='#') continue;
			if (ch[j]>='A'&&ch[j]<='Z') value=ch[j]-'A'+1;
			else value=ch[j]-'a'+27;
			f[i][j+1]=value;
		}
	}
}
void work(){
	for (int k=1;k<=n;k++)
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		if (f[i][j]%13==0&&f[i][j]) f[i][j]=inf;
	for (int k=1;k<=n;k++)
		if ((f[1][k]+f[k][n])%13!=0) ans=min(ans,f[1][k]+f[k][n]);
	printf("%d",ans);
}
int main(){
	freopen("att.in","r",stdin);
	freopen("att.out","w",stdout);
	init();
	work();
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=505;
int n,m,sum;
ll ans;
int w[N],f[N*50];
int main(){
	freopen("team.in","r",stdin);
	freopen("team.out","w",stdout);
	scanf("%d%d",&m,&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&w[i]);
		sum+=w[i];
	}
	f[0]=1;
	for (int i=1;i<=n;i++)
	for (int j=sum;j>=w[i];j--)
		f[j]+=f[j-w[i]];
	for (int i=sum;i;i--){
		if (!f[i]) continue;
		if (m<f[i]){
			ans+=(ll)i*(ll)m;
			break;
		}else{
			ans+=(ll)i*(ll)f[i];
			m-=f[i];
		}
	}
	printf("%I64d",ans);
}

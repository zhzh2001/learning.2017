#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long 
using namespace std;
const ll N=100005;
struct edge{
	ll to,next;
}e[N];
int head[N];
ll size[N],dis[N],ans[N];
int n,tot,id;
void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs(int u,int last){
	size[u]=1; dis[u]=0;
	for (int i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		dfs(v,u);
		size[u]+=size[v];
		dis[u]+=dis[v]+size[v];
	}
}
void dfs2(int u,int last){
	for (int i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		ans[v]=ans[u]+size[1]-size[v]-size[v];
		dfs2(v,u);
	}
}
int main(){
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v); add(v,u);
	}
	dfs(1,0); ans[1]=dis[1];
	dfs2(1,0);
	id=n;
	for (int i=n-1;i;i--)	
		if (ans[id]<=ans[i]) id=i;
	printf("%d",id);
}

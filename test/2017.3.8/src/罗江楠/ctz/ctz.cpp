#include <bits/stdc++.h>
#include <vector>
#define File(s) \
	freopen(s".in", "r", stdin), \
	freopen(s".out", "w", stdout)
#define ll long long
#define N 100050
using namespace std;
vector<int> v[N];
queue<int> q;
int to[N], n, mx, a, b, ans, vis[N], d[N];
int main(){
	File("ctz");
	scanf("%d", &n);
	for(int i = 1; i < n; i++){
		scanf("%d%d", &a, &b);
		to[a]++, to[b]++;
		v[a].push_back(b), v[b].push_back(a);
	}
	for(int i = 1; i <= n; i++)
		if(to[i] > mx) mx = to[i], d[d[0]=1]=i;
		else if(to[i] == mx) d[++d[0]] = i;
	for(int i = 1; i <= d[0]; i++)
		vis[d[i]]++, q.push(d[i]);
	while(!q.empty()){
		int k = q.front(); q.pop();
		for(int i = 0; i < v[k].size(); i++)
			if(!vis[v[k][i]])
				vis[v[k][i]] = vis[k]+1,
				q.push(v[k][i]);
	}
	for(int i = 1; i <= n; i++)
		if(vis[i] > vis[ans]) ans = i;
	printf("%d\n", ans);
	return 0;
}


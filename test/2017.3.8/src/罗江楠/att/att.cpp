#include <bits/stdc++.h>
#define File(s) \
	freopen(s".in", "r", stdin), \
	freopen(s".out", "w", stdout)
#define ll long long
using namespace std;
const int inf = 130000001;
int mp[50][50], n, al[50][50], v[50], f, ans = inf;
int dis[20000000];
inline bool check(){
	for(int i = 1; i <= dis[0]; i++)
		if(!(dis[i]%13)) return 0;
	return 1;
}
inline void dfs(int x, int len, int dep){
//	printf("In : %d %d %d\n",x,len,dep);
//	for(int i = 1; i <= dis[0]; i++) printf("%d ", dis[i]);
//	puts("");
	if(!check()) return;
//	printf("goon\n");
	if(x == n){
		if(len < ans) ans = len;
		return;
	}
	if(len > ans) return;
	for(int i = 1; i <= n; i++){
		if(mp[x][i] < inf && !v[i]){
			v[i] = 1;
			dis[++dis[0]] = mp[x][i];
			for(int k = 0; k < dep; k++, dis[0]++)
				dis[dis[0]+1] = dis[dis[0]-dep]+mp[x][i];
			dfs(i, len+mp[x][i], dep+1);
			dis[0]-=dep+1;
			v[i] = 0;
		}
	}
}
int main(){
	File("att");
	scanf("%d", &n);memset(al, 1, sizeof al);
	for(int i = 1; i <= n; i++) for(int j = 1; j <= n; j++){
		char c = getchar();if(c=='\n') c=getchar();
		if(c == '#') mp[i][j] = inf;
		else if(c < 'a') mp[i][j] = c - 'A' + 1;
		else mp[i][j] = c - 'a' + 27;
		if(mp[i][j] != inf && mp[i][j] % 13 == 0) mp[i][j] = inf;
	}
	dfs(1, 0, 0);
	if(ans!=inf)printf("%d\n", ans);
	else puts("nan");
	return 0;
}



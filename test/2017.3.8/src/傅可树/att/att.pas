var tot,k,i,j,n,ans:longint;
    a:array[-10..30,-10..30]of longint;
    b:array[0..25]of longint;
    ch:char;
function pd:boolean;
var i,j:longint;
begin
  for i:=0 to n-3 do
     for j:=i+1 to n-2 do
        if (b[j]-b[i])mod 13=0 then exit(false);
  exit(true);
end;
procedure dfs(x:longint);
var i:longint;
begin
  if x=n+1 then
     begin
     if pd then
        if b[tot-1]<ans then ans:=b[tot-1];
     exit;
     end;
  for i:=1 to n+1 do
     if (a[x,i]<>a[0,0])and(a[x,i]+b[tot]<=ans) then
	begin
	inc(tot);
	b[tot]:=b[tot-1]+a[x,i];
	dfs(i);
	dec(tot);
	end;
end;
begin
  assign(input,'att.in');
  assign(output,'att.out');
  reset(input);
  rewrite(output);
  readln(n);
  fillchar(a,sizeof(a),20);
  for i:=1 to n do
     begin
     for j:=1 to n do
     	begin
	read(ch);
	if (ch>='A')and(ch<='Z') then a[i,j]:=ord(ch)-64;
        if (ch>='a')and(ch<='z') then a[i,j]:=ord(ch)-70;
        if a[i,j] mod 13=0 then a[i,j]:=a[0,0];
	end;
     readln;
     end;
  ans:=a[0,0];
  a[n,n+1]:=0;
  dfs(1);
  writeln(ans);
  close(input);
  close(output);
end.

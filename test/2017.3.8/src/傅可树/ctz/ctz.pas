type pp=record
     link,next:longint;
end;
var cnt,x,y,s,tot,n,i,root:longint;
    a:array[0..100000]of pp;
    num,size,head,b,c:array[0..100000]of longint;
function min(x,y:longint):longint;
begin
  if x>y then exit(y);
  exit(x);
end;
procedure dfs(x,fa:longint);
var now:longint;
begin
  now:=head[x];
  size[x]:=1;
  while now<>0 do
     begin
     if a[now].link=fa then
	begin
	now:=a[now].next;
	continue;
	end;
     dfs(a[now].link,x);
     size[x]:=size[x]+size[a[now].link];
     now:=a[now].next;
     end;
end;
procedure dfs2(x,fa:longint);
var p,q,ans,now,i:longint;
begin
  now:=head[x];
  ans:=maxlongint;
  p:=cnt;
  q:=0;
  while now<>0 do
     begin
     if a[now].link=fa then
        begin
        now:=a[now].next;
        continue;
        end;
     if size[a[now].link]<ans then
        begin
	ans:=size[a[now].link];
	cnt:=p+1;
	num[cnt]:=a[now].link;
	end
        else
        if size[a[now].link]=ans then
	   begin
	   inc(cnt);
	   num[cnt]:=a[now].link;
	   end;
     now:=a[now].next;
     inc(q);
     end;
  if q=0 then
     begin
     s:=min(s,x);
     exit;
     end;
  for i:=p+1 to cnt do dfs2(num[i],x);
end;
procedure add(x,y:longint);
begin
  inc(tot);
  a[tot].link:=y;
  a[tot].next:=head[x];
  head[x]:=tot;
end;
begin           
  assign(input,'ctz.in');
  assign(output,'ctz.out');
  reset(input);
  rewrite(output);   
  s:=maxlongint;
  readln(n);
  for i:=1 to n-1 do
     begin
     readln(x,y);
     add(x,y);
     add(y,x);
     inc(b[x]);
     inc(b[y]);
     end;
  for i:=1 to n do
     if b[i]=2 then break;
  root:=i;
  dfs(root,0);
  dfs2(root,0);
  writeln(s);
  close(input);
  close(output);
end.

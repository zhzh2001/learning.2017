#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cstdlib> 
#define N 100005
using namespace std;
struct edge{int to,next;}e[N*2];
int head[N],n,x,y,ans,tot;
long long sz[N],f[N],g[N];
void dfs1(int x,int fa){
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			dfs1(e[i].to,x);
			sz[x]+=sz[e[i].to];
			f[x]+=f[e[i].to];
		}
	f[x]+=sz[x];
	sz[x]++;
}
void dfs2(int x,int fa){
	if (x!=1) g[x]=g[fa]+(f[fa]-sz[x]-f[x])+n-sz[x];
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa) dfs2(e[i].to,x);
}
int main(){
    freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		e[++tot]=(edge){y,head[x]};
		head[x]=tot;
		e[++tot]=(edge){x,head[y]};
		head[y]=tot;
	}
	dfs1(1,0);
	dfs2(1,0);
	ans=1;
	for (int i=2;i<=n;i++)
		if (f[i]+g[i]>f[ans]+g[ans]) ans=i;
	printf("%d",ans);
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cstdlib> 
#define inf 0x3f3f3f3f
using namespace std;
char s[27];
int n,a[27][27],bin[15],h,t,x,y,z,zz,ans;
int q[3000005][3],dis[27][8200][15],v[27][8200][15];
int main(){
    freopen("att.in","r",stdin);
	freopen("att.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%s",s+1);
		for (int j=1;j<=n;j++){
			if (s[j]=='#') a[i][j]=inf;
			if (s[j]<='Z'&&s[j]>='A') a[i][j]=s[j]-64;
			if (s[j]>='a'&&s[j]<='z') a[i][j]=s[j]-70;
		}
	}
	bin[0]=1;
	for (int i=1;i<=13;i++) bin[i]=bin[i-1]*2;
	q[1][0]=q[1][1]=1,q[1][2]=0;
	memset(dis,inf,sizeof(dis));
	h=dis[1][1][0]=0;
	t=v[1][1][0]=1;
	while (h!=t){
		h=h%3000000+1;
		x=q[h][0];
		y=q[h][1];
		z=q[h][2];
		v[x][y][z]=0;
		for (int i=1;i<=n;i++){
			if (a[x][i]==inf) continue;
			zz=(z+a[x][i])%13;
			if (y&bin[zz]) continue;
			if (dis[i][y|bin[zz]][zz]>dis[x][y][z]+a[x][i]){
				dis[i][y|bin[zz]][zz]=dis[x][y][z]+a[x][i];
				if (!v[i][y|bin[zz]][zz]){
					v[i][y|bin[zz]][zz]=1;
					t=t%3000000+1;
					q[t][0]=i;
					q[t][1]=y|bin[zz];
					q[t][2]=zz;
				}
			}
		}
	}
	ans=inf;
	for (int i=0;i<bin[13];i++)
		for (int j=0;j<13;j++) ans=min(ans,dis[n][i][j]);
	printf("%d",ans);
}

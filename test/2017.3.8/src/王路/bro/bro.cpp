#include <bits/stdc++.h>
using namespace std;

ifstream fin("bro.in");
ofstream fout("bro.out");

const int maxn = 25;
int n, cnt;
int mat[maxn][maxn];
char ch;

bool vis[maxn];
int dist[maxn];

int Prim(int s){
	int Ans=0;
	memset(vis,0,sizeof vis);
	for (int i = 1; i <= n; i++) dist[i]=mat[s][i];
	vis[s]=1;
	for (int i = 1; i < n; i++) {
		int v,Min=1<<30;
		for (int j = 1; j <= n; j++) if (!vis[j]&&dist[j]<Min)
			Min=dist[j],v=j;
		vis[v]=1;
		Ans += dist[v];
		for (int j = 1; j <= n; j++) if (!vis[j])
			dist[j]=min(dist[j],mat[v][j]);
	}
	return Ans;
}


int main() {
	fin >> n;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			fin >> ch;
			if (ch == '0') mat[i][j] = 0;
			else if (ch == '1') mat[i][j] = 1, cnt++;
		}
	}
	cnt /= 2;
	fout << Prim(1);
}
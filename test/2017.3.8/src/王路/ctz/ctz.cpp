#include <bits/stdc++.h>
using namespace std;

ifstream fin("ctz.in");
ofstream fout("ctz.out");

typedef long long ll;

const int maxn = 100005;
int n, u, v;
short len[5005][5005];
int ans, loc, tot;
bool vis[maxn];
int dist[maxn];
const int inf = 0x3f3f;

int Dijkstra(int s){
	memset(vis,0,sizeof vis);
	for (int i = 1; i <= n; i++) dist[i]=len[s][i];
	
	vis[s]=1;
	for (int i = 1; i < n; i++) {
		int v,Min=1<<30;
		for (int j = 1; j <= n; j++) if (!vis[j]&&dist[j]<Min)  
			Min=dist[j],v=j;
		vis[v]=1;                         
		for (int j = 1; j <= n; j++) if (!vis[j])         
			dist[j]=min(dist[j],dist[v]+len[v][j]);
	}
	int ans = 0;
	for (int i = 1; i <= n; i++) {
		if (s != i && dist[i] < inf) {
			ans += dist[i];
		}
	}
	return ans;
}

int main() {
	fin >> n;
	memset(len, 0x3f, sizeof len);
	for (int i = 1; i < n; i++) {
		fin >> u >> v;
		len[u][v] = len[v][u] = 1;
	}
	for (int i = 1; i <= n; i++) {
		int t;
		t = Dijkstra(i);
		cout << t << endl;
		if (t > ans) {
			ans =  t;
			loc = i;
		}
	}
	fout << loc << endl;
}
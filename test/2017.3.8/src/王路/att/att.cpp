#include <bits/stdc++.h>
using namespace std;

ifstream fin("att.in");
ofstream fout("att.out");

const int maxn = 35, mod = 13, inf = 0x3f3f3f3f;
int n;
int mat[maxn][maxn];
char ch;
int ans = inf;

int st[1005];
int tot;
bool vis[maxn];

void search(int x) {
	// fout << "dbg " << x << endl;
	int tmp = 0;
	if (tot >= 1)
		for (int i = tot; i >= 1; i--) {
			tmp += st[i];
			if (tmp % 13 == 0) {
				// cout << x << endl;
				return;
			}
		}
	if (x == n) {
		int len = tmp;
		ans = min(ans, len);
		return;
	}
	for (int i = 1; i <= n; i++) {
		if (mat[x][i] < inf && !vis[i] && x != i) {
			st[++tot] = mat[x][i];
			vis[i] = true;
			search(i);
			vis[i] = false;
			--tot;
		}
	}
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			fin >> ch;
			if (ch >= 'A' && ch <= 'Z')
				mat[i][j] = ch - 'A' + 1;
			else if (ch >= 'a' && ch <= 'z')
				mat[i][j] = ch - 'a' + 27;
			else
				mat[i][j] = inf;
		}
	vis[1] = true;
	search(1);
	fout << ans << endl;
}
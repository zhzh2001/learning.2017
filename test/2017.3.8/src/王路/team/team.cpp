#include <bits/stdc++.h>
using namespace std;

ifstream fin("team.in");
ofstream fout("team.out");

typedef long long ll;

const int maxn = 505, maxm = 1000005;
int n, m, v[maxn];
ll ans[maxm]; ll tot;
ll a[maxn];
inline bool cmp(int a, int b) { return a > b; }

ll st[maxm], loc, bak[maxm];
bool vis[maxn];

void dfs(int x, int len, int ans) {
	if (len == 0) {
		st[++loc] = ans;
		return;
	}
	for (int i = x + 1; i <= n; i++) {
		dfs(i, len-1, ans + v[i]);
	}
}

int main() {
	fin >> m >> n;
	for (int i = 1; i <= n; i++) {
		fin >> v[i]; tot += v[i];
	}
	sort(v+1, v+n+1, cmp);
	for (int i = 1; i <= n; i++) {
		a[i] = a[i-1] + v[i];
	}
	if (m == 1) {
		fout << a[n] << endl;
		return 0;
	}
	if (m == 2) {
		fout << a[n] + a[n-1] << endl;
		return 0;
	}
	if (n <= 50) {
		for (int i = n; i >= 1; i--) {
			memset(vis, 0, sizeof vis);
			dfs(0, i, 0);
		}
		sort(st+1, st+loc+1, cmp);
		int tans = 0;
		for (int i = 1; i <= m; i++) {
			tans += st[i];
		}
		fout << tans << endl;
		return 0;
	}
	
}
//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("team.in");
ofstream fout("team.out");
int jiyi[503][25003];
int zong,m,n;
int v[503];
int dp(int now,int zong){
	int& fanhui=jiyi[now][zong];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(zong==0){
			fanhui=1;
			return fanhui;
		}else{
			if(now==0){
				fanhui=0;
				return fanhui;
			}else{
				if(zong-v[now]>=0){
					fanhui=dp(now-1,zong-v[now])+dp(now-1,zong);
				}else{
					fanhui=dp(now-1,zong);
				}
			}
		}
	}
	return fanhui;
}
int main(){
	fin>>m>>n;
	for(int i=1;i<=n;i++){
		fin>>v[i];
		zong+=v[i];
	}
	for(int i=0;i<=n;i++){
		for(int j=0;j<=zong;j++){
			jiyi[i][j]=-1;
		}
	}
	long long ans=0;
	for(int i=zong;i>=0;i--){
		int num=dp(n,i);
		if(num>=m){
			ans+=m*i;
			break;
		}else{
			m-=num;
			ans+=num*i;
		}
		if(m==0){
			break;
		}
	}
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
in1:
2 3
2 7 1
out1:
19

in2:
8 4
1 2 3 4
out2:
58
*/

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("att.in");
ofstream fout("att.out");
char tu[30][30];
int tu2[30][30];
int jiyi[30][15][1<<13];
int n;
int dp(int now,int mod,int zz){
	int& fanhui=jiyi[now][mod][zz];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(now==n){
			fanhui=0;
			return fanhui;
		}else{
			fanhui=999999999;
			for(int xun=1;xun<=n;xun++){
				if(tu[now][xun]=='#'&&now!=xun){
					continue;
				}
				int ju=(mod+tu2[now][xun])%13;
				ju=(ju+13)%13;
				if((!(zz&(1<<ju)))&&(now!=xun)&&(ju!=0)){
					fanhui=min(fanhui,dp(xun,ju,zz|(1<<ju))+tu2[now][xun]);
				}
			}
		}
	}
	return fanhui;
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			fin>>tu[i][j];
			if(tu[i][j]!='#'){
				if(tu[i][j]>='a'&&tu[i][j]<='z'){
					tu2[i][j]=tu[i][j]-'a'+27;
				}else{
					tu2[i][j]=tu[i][j]-'A'+1;
				}
			}
		}
	}
	for(int i=0;i<=n;i++){
		for(int j=0;j<=14;j++){
			for(int k=1;k<=(1<<13);k++){
				jiyi[i][j][k]=-1;
			}
		}
	}
	int ans=dp(1,0,0);
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
in:
5
#AB##
###A#
###C#
####K
#####

out:
16
*/

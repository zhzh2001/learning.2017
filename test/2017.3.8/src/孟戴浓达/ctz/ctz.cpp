#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<ctime>
using namespace std;
ifstream fin("ctz.in");
ofstream fout("ctz.out");
int dep[100003];
bool vis[100003];
long long ans[100003];
int zishu[100003];
int n;
int a[200003],c[200003],d[100003];
int tot=0;
long long realans;
int realnum;
void inc(int x,int y){
	tot++;
	a[tot]=y;
	c[tot]=d[x];
	d[x]=tot;
}
void dfs(int node,int depth){
	dep[node]=depth;
	vis[node]=true;
	for(int i=d[node];i;i=c[i]){
		if(!vis[a[i]]){
			dfs(a[i],depth+1);
			zishu[node]+=zishu[a[i]];
		}
	}
	zishu[node]++;
}
void dfs2(int node){
	vis[node]=true;
	for(int i=d[node];i;i=c[i]){
		if(vis[a[i]]==false){
			ans[a[i]]=ans[node]-zishu[a[i]]+n-zishu[a[i]];
			if(realans<ans[a[i]]){
				realnum=a[i];
				realans=ans[a[i]];
			}else{
				if(realans==ans[a[i]]){
					realnum=min(realnum,a[i]);
				}
			}
			dfs2(a[i]);
		}
	}
}
int main(){
	fin>>n;
	if(n==2){
		fout<<1;
		return 0;
	}
	for(int i=1;i<=n-1;i++){
		int x,y;
		fin>>x>>y;
		inc(x,y);
		inc(y,x);
	}
	dfs(1,0);
	for(int i=1;i<=n;i++){
		ans[1]+=dep[i];
	}
	realans=ans[1];
	realnum=1;
	for(int i=1;i<=100001;i++){
		vis[i]=false;
	}
	dfs2(1);
	fout<<realnum<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
in:
8
1 4
5 6
4 5
6 7
6 8
2 4
3 4

out:
7
*/

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
int la[200005],pr[200005],to[200005],i,j,k,m,n,sd[100005],size[100005],l,x,y,dis[100005],sum,ans;
void build(int x,int y)
{
	to[++l]=y;
	pr[l]=la[x];
	la[x]=l;
}
void dfs(int x,int y)
{
	sd[x]=y;
	sum+=y;
	size[x]=1;
	for (int i=la[x];i!=0;i=pr[i])
		if (sd[to[i]]==0)
		{
			dfs(to[i],y+1);
			size[x]+=size[to[i]];
		}
}
void dp(int fa,int x)
{
	dis[x]=dis[fa]+n-size[x]*2;
	for (int i=la[x];i!=0;i=pr[i])
		if (sd[to[i]]>sd[x])
			dp(x,to[i]);
}
int main()
{
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n-1;++i)
	{
		scanf("%d%d",&x,&y);
		build(x,y);
		build(y,x);
	}
	dfs(1,1);
	dis[1]=sum-n;
	for (i=la[1];i!=0;i=pr[i])
		dp(1,to[i]);
	ans=1;
	for (i=2;i<=n;++i)
		if (dis[ans]<dis[i])
			ans=i;
	printf("%d",ans);
	return 0;
}

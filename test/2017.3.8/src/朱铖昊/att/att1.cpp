#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
char c[30];
int i,j,k,l,m,n,a[30][30],b[30],ans;
void dfs(int x,int sum)
{
	if (x==n)
	{
		ans=min(sum,ans);
		return;
	}
	for (int i=1;i<=n;++i)
		if (b[(a[x][i]+sum)%13]==0)
		{
			b[(a[x][i]+sum)%13]=1;
			dfs(i,sum+a[x][i]);
			b[(a[x][i]+sum)%13]=0;
		}
}
int main()
{
	freopen("att.in","r",stdin);
	freopen("att1.out","w",stdout);
	ans=int(1e9);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
	{
		scanf("%s",c+1);
		for (j=1;j<=n;++j)
		{
			if (c[j]<='Z' && c[j]>='A')
				a[i][j]=c[j]-'A'+1;
			if (c[j]<='z' && c[j]>='a')
				a[i][j]=c[j]-'a'+27;
		}
	}
	b[0]=1;
	dfs(1,0);
	printf("%d",ans);
	return 0;
}
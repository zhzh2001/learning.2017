#include<iostream>
#include<cmath>
#include<cstdio>
using namespace std;
int n,cnt;
int	fa[100001],size[100001],f[100001],next[100001],d[100001],poi[100001];
bool vis[100001],cant[100001];
inline void add(int x,int y)
{
	poi[++cnt]=y;next[cnt]=f[x];f[x]=cnt;
}
inline void dfs(int x,int dep)
{
	vis[x]=1;
	size[x]=1;
	d[x]=dep;
	for(int i=f[x];i;i=next[i])
	{
		int t=poi[i];
		if(!vis[t])	dfs(t,dep+1),size[x]+=size[t],fa[t]=x,cant[x]=1;
	}
}
inline int doit(int x)
{
	int sum=0;
	int t=x;
	while(fa[x])
	{
		int z_size=size[fa[x]]-size[x]-1;
		int z_dep=(d[t]-d[fa[x]])-d[fa[x]];
		sum+=z_size*z_dep;
		x=fa[x];
	}
	return sum;
}
int main()
{                 
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n-1;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	dfs(1,0);
	int nans=0,ans=0;
	doit(7);
	for(int i=1;i<=n;i++)
	{
		if(!cant[i])
		{
			int tmp=doit(i);
			if(tmp>nans)
			{
				nans=tmp;
				ans=i;
			}
		}
	}
	cout<<ans<<endl;
}


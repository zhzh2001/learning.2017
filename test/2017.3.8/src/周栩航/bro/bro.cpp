#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
int q[101],dep[101],cant[101],vis[101];
int d[101][101];
int n,ans;
inline void bfs(int x)
{
	int l=1,r=1;
	for(int i=1;i<=n;i++)	q[i]=dep[i]=cant[i]=vis[i]=0;
	q[1]=x;
	vis[x]=1;
	while(l<=r)
	{
		int t=q[l];
		for(int i=1;i<=n;i++)
			if(i!=t&&d[t][i]&&!vis[i])
			{
				r++;
				q[r]=i;
				vis[i]=1;
				dep[r]=dep[l]+1;
				cant[t]=1;
			}
		l++;
	}
	int sum=0;

	for(int i=1;i<=n;i++)	sum+=(cant[i]^1);
	ans=max(ans,sum);
}
int main()
{
	freopen("bro.in","r",stdin);
	freopen("bro.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		string s;
		cin>>s;
		for(int j=1;j<=n;j++)
			d[i][j]=s[j-1]-'0';
	}
	for(int i=1;i<=n;i++)
	{
		bfs(i);
	}
	cout<<ans<<endl;
}

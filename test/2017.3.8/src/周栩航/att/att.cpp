#include<iostream>
#include<cmath>
#include<cstdio>
using namespace std;
int n,d[101][101];
int ans=1e9;
int bj[53],time;
bool end;
bool vis[101];
inline void dfs(int x,int step,int l,int bj[])
{
	if(x==n)
	{
		time++;
		if(time==10000)	end=1;
		ans=min(ans,l);
		return;
	}	
	if(end)	return;
	vis[x]=1;
	int tbj[54];
	for(int i=1;i<=n;i++)
	{	

		if(vis[i])	continue;
		if(d[x][i]==-1)	continue;
		bool can=1;
		for(int j=1;j<=step;j++)
			if((bj[j]+d[x][i])%13==0)
				can=0;
	
		if(can)	
		{
			for(int j=1;j<=step;j++)
				tbj[j]=bj[j]+d[x][i];
			tbj[step+1]=d[x][i];
			dfs(i,step+1,l+d[x][i],tbj);
		}
	}
	vis[x]=0;
}
int main()
{
	freopen("att.in","r",stdin);
	freopen("att.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	string s;
	for(int i=1;i<=n;i++)
	{
		cin>>s;
		for(int j=1;j<=n;j++)
		{
			if(s[j-1]>='A'&&s[j-1]<='Z')
				d[i][j]=s[j-1]-'A'+1;
				else
			if(s[j-1]>='a'&&s[j-1]<='z')
				d[i][j]=s[j-1]-'a'+27;
				else
			if(s[j-1]=='#')	d[i][j]=-1;
		}
	}
	bj[1]=0;
	
	dfs(1,1,0,bj);
	printf("%d\n",ans);
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
using namespace std;
int n,cnt,head[100001];
int yyy[100001];
int b[100001];
bool vist[100001];
struct edge{
	int next,to;
}e[200100];

inline void add(int u,int v){
	e[++cnt].to=v,e[cnt].next=head[u],head[u]=cnt;
}

inline void dfs(int u){
	vist[u]=1;
	for(int i=head[u];i;i=e[i].next){
		if(vist[e[i].to]) continue;
		dfs(e[i].to);
		yyy[u]+=yyy[e[i].to];
		b[u]+=b[e[i].to]+yyy[e[i].to];
	}
	yyy[u]+=1;
}
inline void dfs2(int u){
	vist[u]=1;
	for(int i=head[u];i;i=e[i].next){
		if(vist[e[i].to]) continue;
		b[e[i].to]=b[e[i].to]+b[u]-b[e[i].to]-yyy[e[i].to]+yyy[u]-yyy[e[i].to];
		yyy[e[i].to]=yyy[u];
		dfs2(e[i].to);
	}
}

int main(){
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y),add(y,x);
	}
	dfs(1);
	memset(vist,0,sizeof vist);
	dfs2(1);
	int ans=1,maxn=b[1];
	for(int i=2;i<=n;i++)
		if(maxn<b[i]) ans=i,maxn=b[i];
	printf("%d",ans);
	return  0;
}

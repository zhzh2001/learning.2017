#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
using namespace std;
int a[1000];
long long f[500001];
long long ans=0;

int main(){
	freopen("team.in","r",stdin);
	freopen("team.out","w",stdout);
	int m,n;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++) scanf("%d",&a[i]);
	f[0]=1;
	for(int i=1;i<=m;i++)
		for(int j=m*50-a[i];j>=0;j--) f[j+a[i]]+=f[j];
	for(int i=m*50;i>=0;i--){
		if(n<=f[i]){
			ans+=(long long)n*i;
			break;
		}
		ans+=(long long)(f[i]*i);
		n-=f[i];
	}
	printf("%I64d",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
using namespace std;
char ch[30];
int head[30],vist[39],cnt,n;
int f[30],ans=1e9;
struct edge{
	int next,to,w;
}e[10000];

inline int OK(char ch){
	if(ch>='A'&&ch<='Z') return ch-'A'+1;
	else if(ch>='a'&&ch<='z') return ch-'a'+27;
	else return -1;
}

inline void add(int u,int v,int w){
	e[++cnt].to=v,e[cnt].next=head[u],head[u]=cnt,e[cnt].w=w;
}

inline bool panduan(int m){
	for(int i=1;i<=m;i++)
		for(int j=0;j<i;j++)
			if((f[i]-f[j])%13==0) return false;
	return true;
}

inline void dfs(int u,int x){
	if(u==n)
		if(panduan(x)){
			ans=min(ans,f[x]);
			return;
		}
	vist[u]=1;
	for(int i=head[u];i;i=e[i].next){	
		if(vist[e[i].to]) continue;
		f[x+1]=e[i].w+f[x];
		dfs(e[i].to,x+1);
	}
	vist[u]=0;
}

int main(){
	freopen("att.in","r",stdin);
	freopen("att.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%s",ch+1);
		for(int j=1;j<=n;j++){
			int x=OK(ch[j]);
			if(~x) add(i,j,x);
		}
	}
	dfs(1,0);
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
#define ll long long
using namespace std;
ll nedge=0,p[200001],next[200001],head[200001];
ll ans,n,fa[100001],a[100001]={0},s[100001]={0},g[100001]={0};
inline void addedge(int a,int b){
	nedge++;p[nedge]=b;
	next[nedge]=head[a];
	head[a]=nedge;
}
inline void dfs(ll x,ll la){
	fa[x]=la;ll flag=0;
	for(ll k=head[x];k;k=next[k]){
		flag++;
		if(p[k]==la)continue;
		dfs(p[k],x);
		g[x]+=g[p[k]];
		s[x]+=s[p[k]]+g[p[k]];
	}
	g[x]++;
	if(flag==1)a[0]++,a[a[0]]=x;
}
inline ll find(ll x){
	ll la=x,sum=0,pp=1;
	x=fa[x];
	while(x>0){
		sum=sum+s[x]-s[la]-g[la]+(g[x]-g[la])*pp;
		pp++;la=x;
		x=fa[x];
	}
	return sum;
}
int main()
{
	freopen("ctz.in","r",stdin);
	freopen("ctz.out","w",stdout);
	scanf("%I64d",&n);
	for(ll i=1;i<n;i++){
		ll x,y;scanf("%I64d%I64d",&x,&y);
		addedge(x,y);
		addedge(y,x);
	}
	dfs(1,0);ll pp,ma=0,ans;
	sort(a+1,a+a[0]+1);
	for(ll i=1;i<=a[0];i++){
		if(a[i]==1)pp=s[1];
		else pp=find(a[i]);
		if(pp>ma)ma=pp,ans=a[i];
	} 
	printf("%I64d",ans);
	return 0;
}

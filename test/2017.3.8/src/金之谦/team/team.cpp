#include<bits/stdc++.h>
#define ll long long
using namespace std;
priority_queue<ll>q;
ll i,a[1001],n,m,sum=0,cnt=0;
bool b[1001]={0};
inline void dfs(int p){
	if(cnt>10000001)return;
	cnt++;int i;
	q.push(sum);
	for(i=p+1;i<=n;++i)sum-=a[i],dfs(i),sum+=a[i];
}
inline ll read(){
	ll k=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9'){
		k=k*10+ch-'0';
		ch=getchar();
	}
	return k*f;
}
int main()
{
	freopen("team.in","r",stdin);
	freopen("team.out","w",stdout);
	m=read();n=read();
	for(i=1;i<=n;++i)a[i]=read(),sum+=a[i];
	sort(a+1,a+n+1);
	dfs(0);
	sum=0;
	for(i=1;i<=m;++i)sum+=q.top(),q.pop();
	printf("%I64d",sum);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n,f[50][50];
char c;
long long ans=0;
bool b[50];
void  print(int sum){
	if (sum%13!=0)
		if (sum<ans)
			ans=sum;
}
void dfs(int x,int sum){
	if (x==n){
		print(sum);
    	return;
	}	  
	for (int i=1;i<=n;i++)
 		if ((f[x][i]!=-1)&&(b[i])&&((sum+f[x][i])%13!=0)){
         	b[i]=false;
         	dfs(i,sum+f[x][i]);
        	b[i]=true;
		}	
}
int main(){
	freopen("att.in", "r", stdin);
	freopen("att.out", "w", stdout);
	cin>>n;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++){
			cin>>c;
			if (c=='#'){
				f[i][j]=-1;
				continue;
			}
			if (c>='A' && c<='Z')
				f[i][j]=int(c)-65+1;
			else
				f[i][j]=int(c)-97+27;
		}
	for (int i=1;i<=50;i++)
		b[i]=true;
	ans=10000000;
	dfs(1,0);
	cout<<ans;
}

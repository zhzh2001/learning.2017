@echo off
for /d %%s in (*) do (
cd %%s
echo %%s
g++ %%s.cpp
copy con %%s.in
type %%s.out
pause
del a.exe
cd ..
)
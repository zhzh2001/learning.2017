#include<fstream>
#include<string>
#include<cctype>
using namespace std;
ifstream fin("att.in");
ofstream fout("att.out");
const int N=30,INF=0x3f3f3f3f;
string s;
int n,mat[N][N],l[N],ans;
bool vis[N];
void dfs(int k,int cnt,int now)
{
	if(k==n-1)
	{
		ans=min(ans,now);
		return;
	}
	if(now>=ans)
		return;
	vis[k]=true;
	for(int i=0;i<n;i++)
		if(!vis[i]&&mat[k][i]<INF&&mat[k][i]%13)
		{
			bool flag=true;
			int t=mat[k][i];
			for(int j=cnt-1;j;j--)
			{
				t+=l[j];
				if(t%13==0)
				{
					flag=false;
					break;
				}
			}
			if(flag)
			{
				l[cnt]=mat[k][i];
				dfs(i,cnt+1,now+mat[k][i]);
			}
		}
	vis[k]=false;
}
int main()
{
	fin>>n;
	for(int i=0;i<n;i++)
	{
		fin>>s;
		for(int j=0;j<n;j++)
			if(s[j]=='#')
				mat[i][j]=INF;
			else
				if(isupper(s[j]))
					mat[i][j]=s[j]-'A'+1;
				else
					mat[i][j]=s[j]-'a'+27;
	}
	ans=INF;
	dfs(0,1,0);
	fout<<ans<<endl;
	return 0;
}
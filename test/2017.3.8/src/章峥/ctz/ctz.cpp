#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("ctz.in");
ofstream fout("ctz.out");
const int N=100005,M=200005,INF=0x3f3f3f3f;
int n,head[N],v[M],nxt[M],e,d[N],sz[N];
long long f[N];
bool vis[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(d[v[i]]==INF)
		{
			d[v[i]]=d[k]+1;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
		}
	f[1]+=d[k];
}
void dp(int k)
{
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
		if(!vis[v[i]])
		{
			f[v[i]]=f[k]+n-2*sz[v[i]];
			dp(v[i]);
		}
}
int main()
{
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	dfs(1);
	dp(1);
	int ans=1;
	for(int i=2;i<=n;i++)
		if(f[i]>f[ans])
			ans=i;
	fout<<ans<<endl;
	return 0;
}
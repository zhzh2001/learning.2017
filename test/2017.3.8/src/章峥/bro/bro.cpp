#include<fstream>
#include<string>
#include<cstring>
using namespace std;
ifstream fin("bro.in");
ofstream fout("bro.out");
const int N=20;
int n,ans,f[N];
string s;
bool mat[N][N],sel[N],vis[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
void dfs(int k,int cnt)
{
	if(k==n+1)
	{
		for(int i=1;i<=n;i++)
			f[i]=i;
		memset(vis,false,sizeof(vis));
		int cc=0;
		for(int i=1;i<=n;i++)
			if(sel[i])
			{
				int ri=getf(i);
				for(int j=1;j<=n;j++)
					if(mat[i][j]&&!vis[j])
					{
						int rj=getf(j);
						if(ri!=rj)
						{
							f[rj]=ri;
							vis[j]=true;
							cc++;
						}
					}
			}
		if(cc==n-1)
			ans=max(ans,n-cnt);
		return;
	}
	sel[k]=false;
	dfs(k+1,cnt);
	sel[k]=true;
	dfs(k+1,cnt+1);
}
int main()
{
	fin>>n;
	if(n==2)
	{
		fout<<2<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
	{
		fin>>s;
		for(int j=0;j<n;j++)
			mat[i][j+1]=s[j]=='1';
	}
	dfs(1,0);
	fout<<ans<<endl;
	return 0;
}
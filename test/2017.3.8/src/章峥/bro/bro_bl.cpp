#include<fstream>
#include<string>
using namespace std;
ifstream fin("bro.in");
ofstream fout("bro.out");
const int N=20;
int n,m,into[N],ans,f[N];
string s;
struct edge
{
	int u,v;
}e[N*N];
int getf(int x)
{
	return f[x]==x?x:getf(f[x]);
}
void dfs(int k,int cnt)
{
	if(cnt==n)
	{
		int now=0;
		for(int i=0;i<n;i++)
			if(into[i]==1)
				now++;
		ans=max(ans,now);
		return;
	}
	for(int i=k;i<=m;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv)
		{
			into[e[i].u]++;into[e[i].v]++;
			f[ru]=rv;
			dfs(i+1,cnt+1);
			f[ru]=ru;
			into[e[i].u]--;into[e[i].v]--;
		}
	}
}
int main()
{
	fin>>n;
	for(int i=0;i<n;i++)
	{
		fin>>s;
		for(int j=0;j<n;j++)
			if(i<j&&s[j]=='1')
			{
				e[++m].u=i;
				e[m].v=j;
			}
		f[i]=i;
	}
	dfs(1,1);
	fout<<ans<<endl;
	return 0;
}
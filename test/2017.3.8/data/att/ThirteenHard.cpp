// BEGIN CUT HERE

// END CUT HERE
#line 5 "ThirteenHard.cpp"
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <bitset>
#include <list>
#include <stdexcept>
#include <functional>
#include <utility>
#include <ctime>
using namespace std;

#define PB push_back
#define MP make_pair

typedef vector<int> VI;
typedef vector<string> VS;
typedef vector<double> VD;
typedef long long LL;
typedef pair<int,int> PII;


int N;
int G[105][105],d[30][1<<13];
int X[2000005],Y[2000005];
bool vis[30][1<<13];
class ThirteenHard
{
public:
	inline int spfa()
	{
		memset(d,63,sizeof(d));
		memset(vis,0,sizeof(vis));
		d[1][0]=0;
		X[1]=1,Y[1]=0;
		for (int h=1,t=1;h<=t;++h)
		{
			int u=X[h],v=Y[h];
			for (int i=1;i<=N;++i)
			if (G[u][i]>=0)
			{
				int pos=(1<<(G[u][i]%13));
				for (int j=0;j<13;++j)
				if (v&(1<<j))	pos|=1<<((j+G[u][i])%13);
				if (pos&1)	continue;
				if (d[u][v]+G[u][i]<d[i][pos])
				{
					d[i][pos]=d[u][v]+G[u][i];
					if (!vis[i][pos])	vis[i][pos]=1,X[++t]=i,Y[t]=pos;
				}
			}
		}
		int ret=1000000000;
		for (int i=1;i<1<<13;++i)
			ret=min(ret,d[N][i]);
		return ret<1000000000?ret:-1;
	}
	int calcTime(vector <string> city)
	{
		N=city.size();
		for (int i=0;i<city.size();++i)
		for (int j=0;j<city.size();++j)
		if (city[i][j]=='#')	G[i+1][j+1]=-1;
		else
		if (city[i][j]<='Z')	G[i+1][j+1]=city[i][j]-64;
		else	G[i+1][j+1]=city[i][j]-70;
		freopen("att10.in","w",stdout);
		printf("%d\n",N);
		for (int i=0;i<N;++i)
			cout<<city[i]<<endl;
		freopen("att10.out","w",stdout);
		int ret=spfa();
		printf("%d\n",ret);
		return ret;
	}
	
// BEGIN CUT HERE
	public:
	void run_test(int Case) { if ((Case == -1) || (Case == 0)) test_case_0(); if ((Case == -1) || (Case == 1)) test_case_1(); if ((Case == -1) || (Case == 2)) test_case_2(); if ((Case == -1) || (Case == 3)) test_case_3(); if ((Case == -1) || (Case == 4)) test_case_4(); }
	private:
	template <typename T> string print_array(const vector<T> &V) { ostringstream os; os << "{ "; for (typename vector<T>::const_iterator iter = V.begin(); iter != V.end(); ++iter) os << '\"' << *iter << "\","; os << " }"; return os.str(); }
	void verify_case(int Case, const int &Expected, const int &Received) { cerr << "Test Case #" << Case << "..."; if (Expected == Received) cerr << "PASSED" << endl; else { cerr << "FAILED" << endl; cerr << "\tExpected: \"" << Expected << '\"' << endl; cerr << "\tReceived: \"" << Received << '\"' << endl; } }
	void test_case_0() { string Arr0[] ={"F###g##", "#nd#gJ#", "#crv#X#", "I#ES###", "chq####", "###u#sh", "TT##tnc"}
; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 22; verify_case(0, Arg1, calcTime(Arg0)); }
	void test_case_1() { string Arr0[] = { "#Z",
  "Z#" }; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = -1; verify_case(1, Arg1, calcTime(Arg0)); }
	void test_case_2() { string Arr0[] = { "Good#####",
  "#Luck####",
  "##and####",
  "##Have###",
  "####Fun##",
  "#####in##",
  "#####the#",
  "CHALLENGE",
  "##PHASE##" }; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 137; verify_case(2, Arg1, calcTime(Arg0)); }
	void test_case_3() { string Arr0[] = { "###No#####",
  "####Zaphod",
  "#####Just#",
  "######very",
  "####very##",
  "improbable",
  "##########",
  "##########",
  "##########",
  "##########" }; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 103; verify_case(3, Arg1, calcTime(Arg0)); }
	void test_case_4() { string Arr0[] = { "#B#C##T#M",
  "##K######",
  "########A",
  "####R####",
  "#####U###",
  "########C",
  "#######H#",
  "########S",
  "#########" }; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 47; verify_case(4, Arg1, calcTime(Arg0)); }

// END CUT HERE

};

// BEGIN CUT HERE
int main()
{
	ThirteenHard ___test;
	___test.run_test(0);
	return 0;
}
// END CUT HERE


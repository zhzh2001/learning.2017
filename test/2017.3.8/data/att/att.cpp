#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <bitset>
#include <list>
#include <stdexcept>
#include <functional>
#include <utility>
#include <ctime>
using namespace std;

#define PB push_back
#define MP make_pair

typedef vector<int> VI;
typedef vector<string> VS;
typedef vector<double> VD;
typedef long long LL;
typedef pair<int,int> PII;


int N;
int G[105][105],d[30][1<<13];
int X[2000005],Y[2000005];
bool vis[30][1<<13];
inline int spfa()
{
	memset(d,63,sizeof(d));
	memset(vis,0,sizeof(vis));
	d[1][0]=0;
	X[1]=1,Y[1]=0;
	for (int h=1,t=1;h<=t;++h)
	{
		int u=X[h],v=Y[h];
		for (int i=1;i<=N;++i)
		if (G[u][i]>=0)
		{
			int pos=(1<<(G[u][i]%13));
			for (int j=0;j<13;++j)
			if (v&(1<<j))	pos|=1<<((j+G[u][i])%13);
			if (pos&1)	continue;
			if (d[u][v]+G[u][i]<d[i][pos])
			{
				d[i][pos]=d[u][v]+G[u][i];
				if (!vis[i][pos])	vis[i][pos]=1,X[++t]=i,Y[t]=pos;
			}
		}
	}
	int ret=1000000000;
	for (int i=1;i<1<<13;++i)
		ret=min(ret,d[N][i]);
	return ret<1000000000?ret:-1;
}
int calcTime(vector <string> city)
{
	N=city.size();
	for (int i=0;i<city.size();++i)
	for (int j=0;j<city.size();++j)
	if (city[i][j]=='#')	G[i+1][j+1]=-1;
	else
	if (city[i][j]<='Z')	G[i+1][j+1]=city[i][j]-64;
	else	G[i+1][j+1]=city[i][j]-70;
	return spfa();
}
int main()
{
	freopen("att.in","r",stdin);
	freopen("att.out","w",stdout);
	vector<string>	city;
	string s;
	scanf("%d",&N);
	for (int i=0;i<N;++i)
		cin>>s,city.push_back(s);
	printf("%d\n",calcTime(city));
	return 0;
}

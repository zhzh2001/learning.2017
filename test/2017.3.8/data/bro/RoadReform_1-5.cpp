// BEGIN CUT HERE

// END CUT HERE
#line 5 "RoadReform.cpp"
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <bitset>
#include <list>
#include <stdexcept>
#include <functional>
#include <utility>
#include <ctime>
using namespace std;

#define PB push_back
#define MP make_pair

#define REP(i,n) for(i=0;i<(n);++i)
#define FOR(i,l,h) for(i=(l);i<=(h);++i)
#define FORD(i,h,l) for(i=(h);i>=(l);--i)

typedef vector<int> VI;
typedef vector<string> VS;
typedef vector<double> VD;
typedef long long LL;
typedef pair<int,int> PII;

#define UpInt(x)	((int)(ceil(x)))
#define LowInt(x)	((int)(floor(x)))
#define lowbit(x) (x&-x)

int E[25][25];
bool vis[25];
int fcount=0;
class RoadReform
{
public:
	inline void	dfs(int u,int pos)
	{
		vis[u]=1;
		if (pos&(1<<u))	return;
		for (int i=1;i<=E[u][0];++i)
		if (!vis[E[u][i]])	dfs(E[u][i],pos);
	}
	int findMaxDeadendCount(vector <string> roads)
	{
		++fcount;
		ostringstream is,os;
		is<<"bro"<<fcount<<".in";
		os<<"bro"<<fcount<<".out";
		freopen(is.str().c_str(),"w",stdout);
		int N=roads.size(),ret=0;
		cout<<N<<endl;
		for (int i=0;i<N;++i)
			cout<<roads[i]<<endl;
		if (N==2)	return 2;
		memset(E,0,sizeof(E));
		for (int i=0;i<N-1;++i)
		for (int j=i+1;j<N;++j)
		if (roads[i][j]=='1')
			E[i][++E[i][0]]=j,E[j][++E[j][0]]=i;
		for (int i=1;i<1<<N;++i)
		{
			int cnt=0;
			for (int k=0;k<N;++k)
			if (i&(1<<k))	++cnt;
			memset(vis,0,sizeof(vis));
			for (int j=0;j<N;++j)
			if (!(i&(1<<j)))
			{
				dfs(j,i);break;
			}
			bool mk=1;
			for (int k=0;k<N;++k)
			if (!vis[k])
			{
				mk=0;break;
			}
			if (mk)	ret=max(ret,cnt);
		}
		freopen(os.str().c_str(),"w",stdout);
		cout<<ret<<endl;
		return ret;
	}
	
// BEGIN CUT HERE
	public:
	void run_test(int Case) { if ((Case == -1) || (Case == 0)) test_case_0(); if ((Case == -1) || (Case == 1)) test_case_1(); if ((Case == -1) || (Case == 2)) test_case_2(); if ((Case == -1) || (Case == 3)) test_case_3(); if ((Case == -1) || (Case == 4)) test_case_4(); }
	private:
	template <typename T> string print_array(const vector<T> &V) { ostringstream os; os << "{ "; for (typename vector<T>::const_iterator iter = V.begin(); iter != V.end(); ++iter) os << '\"' << *iter << "\","; os << " }"; return os.str(); }
	void verify_case(int Case, const int &Expected, const int &Received) { cerr << "Test Case #" << Case << "..."; if (Expected == Received) cerr << "PASSED" << endl; else { cerr << "FAILED" << endl; cerr << "\tExpected: \"" << Expected << '\"' << endl; cerr << "\tReceived: \"" << Received << '\"' << endl; } }
	void test_case_0() { string Arr0[] = {"0100000001", "1010000000", "0101000000", "0010100000", "0001010000", "0000101000", "0000010100", "0000001010", "0000000101", "1000000010"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 2; verify_case(0, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_1() { string Arr0[] = {"011111110", "100111110", "100111111", "111011101", "111101011", "111110111", "111101011", "111011101", "001111110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 8; verify_case(1, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_2() { string Arr0[] = {"01111110111", "10001111011", "10001001011", "10001101101", "11110111111", "11011011010", "11001101101", "01111110111", "10011011011", "11101101101", "11111011110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 10; verify_case(2, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_3() { string Arr0[] = {"00111", "00010", "10000", "11000", "10000"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 3; verify_case(3, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_4() { string Arr0[] = {"01111010", "10100011", "11011001", "10101000", "10110001", "00000001", "11000001", "01101110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 6; verify_case(4, Arg1, findMaxDeadendCount(Arg0)); }

// END CUT HERE

};

// BEGIN CUT HERE
int main()
{
	RoadReform ___test;
	___test.run_test(-1);
	return 0;
}
// END CUT HERE

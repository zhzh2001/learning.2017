// BEGIN CUT HERE

// END CUT HERE
#line 5 "RoadReform.cpp"
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <bitset>
#include <list>
#include <stdexcept>
#include <functional>
#include <utility>
#include <ctime>
using namespace std;

#define PB push_back
#define MP make_pair

#define REP(i,n) for(i=0;i<(n);++i)
#define FOR(i,l,h) for(i=(l);i<=(h);++i)
#define FORD(i,h,l) for(i=(h);i>=(l);--i)

typedef vector<int> VI;
typedef vector<string> VS;
typedef vector<double> VD;
typedef long long LL;
typedef pair<int,int> PII;

#define UpInt(x)	((int)(ceil(x)))
#define LowInt(x)	((int)(floor(x)))
#define lowbit(x) (x&-x)

int E[25][25];
bool vis[25];
int fcount=5;
class RoadReform
{
public:
	inline void	dfs(int u,int pos)
	{
		vis[u]=1;
		if (pos&(1<<u))	return;
		for (int i=1;i<=E[u][0];++i)
		if (!vis[E[u][i]])	dfs(E[u][i],pos);
	}
	int findMaxDeadendCount(vector <string> roads)
	{
		++fcount;
		ostringstream is,os;
		is<<"bro"<<fcount<<".in";
		os<<"bro"<<fcount<<".out";
		freopen(is.str().c_str(),"w",stdout);
		int N=roads.size(),ret=0;
		cout<<N<<endl;
		for (int i=0;i<N;++i)
			cout<<roads[i]<<endl;
		if (N==2)	return 2;
		memset(E,0,sizeof(E));
		for (int i=0;i<N-1;++i)
		for (int j=i+1;j<N;++j)
		if (roads[i][j]=='1')
			E[i][++E[i][0]]=j,E[j][++E[j][0]]=i;
		for (int i=1;i<1<<N;++i)
		{
			int cnt=0;
			for (int k=0;k<N;++k)
			if (i&(1<<k))	++cnt;
			memset(vis,0,sizeof(vis));
			for (int j=0;j<N;++j)
			if (!(i&(1<<j)))
			{
				dfs(j,i);break;
			}
			bool mk=1;
			for (int k=0;k<N;++k)
			if (!vis[k])
			{
				mk=0;break;
			}
			if (mk)	ret=max(ret,cnt);
		}
		freopen(os.str().c_str(),"w",stdout);
		cout<<ret<<endl;
		return ret;
	}
	
// BEGIN CUT HERE
	public:
	void run_test(int Case) { if ((Case == -1) || (Case == 0)) test_case_0(); if ((Case == -1) || (Case == 1)) test_case_1(); if ((Case == -1) || (Case == 2)) test_case_2(); if ((Case == -1) || (Case == 3)) test_case_3(); if ((Case == -1) || (Case == 4)) test_case_4(); }
	private:
	template <typename T> string print_array(const vector<T> &V) { ostringstream os; os << "{ "; for (typename vector<T>::const_iterator iter = V.begin(); iter != V.end(); ++iter) os << '\"' << *iter << "\","; os << " }"; return os.str(); }
	void verify_case(int Case, const int &Expected, const int &Received) { cerr << "Test Case #" << Case << "..."; if (Expected == Received) cerr << "PASSED" << endl; else { cerr << "FAILED" << endl; cerr << "\tExpected: \"" << Expected << '\"' << endl; cerr << "\tReceived: \"" << Received << '\"' << endl; } }
	void test_case_0() { string Arr0[] = {"011111111111110", "101111111111111", "110101111111111", "111011111101110", "110101111111111", "111110111111111", "111111011111111", "111111101111111", "111111110111111", "111111111011111", "111011111101111", "111111111110111", "111111111111011", "111111111111101", "011011111111110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 14; verify_case(0, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_1() { string Arr0[] = {"011111111111111", "101111111111111", "110111111111111", "111011111111111", "111101111111111", "111110111111111", "111111011111111", "111111101111111", "111111110111111", "111111111011111", "111111111101111", "111111111110111", "111111111111011", "111111111111101", "111111111111110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 14; verify_case(1, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_2() { string Arr0[] = {"010100011011110", "101010111101100", "010001111110011", "100000110000100", "010000110011110", "001000010110010", "011110001000110", "111111001001100", "111000110001010", "011001000010000", "101011000101000", "110010011010100", "110110110001001", "101011101000001", "001000000000110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 13; verify_case(2, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_3() { string Arr0[] = {"011011111111111", "101011111111111", "110011111111111", "000000000000111", "111001111100111", "111010111010111", "111011010110111", "111011101110111", "111011010110111", "111010111010111", "111001111100111", "111000000000000", "111111111110011", "111111111110101", "111111111110110"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 13; verify_case(3, Arg1, findMaxDeadendCount(Arg0)); }
	void test_case_4() { string Arr0[] = {"010000010100010", "100010110100110", "000000010011000", "000001001000000", "010001100100011", "000110010100010", "010010001101110", "111001001000011", "000100110011010", "110011100000110", "001000001000011", "001000101000000", "010000100100000", "110011111110001", "000010010010010"}; vector <string> Arg0(Arr0, Arr0 + (sizeof(Arr0) / sizeof(Arr0[0]))); int Arg1 = 12; verify_case(4, Arg1, findMaxDeadendCount(Arg0)); }

// END CUT HERE

};

// BEGIN CUT HERE
int main()
{
	RoadReform ___test;
	___test.run_test(-1);
	return 0;
}
// END CUT HERE

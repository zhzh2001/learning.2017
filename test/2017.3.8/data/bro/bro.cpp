// BEGIN CUT HERE

// END CUT HERE
#line 5 "RoadReform.cpp"
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <bitset>
#include <list>
#include <stdexcept>
#include <functional>
#include <utility>
#include <ctime>
using namespace std;

#define PB push_back
#define MP make_pair

#define REP(i,n) for(i=0;i<(n);++i)
#define FOR(i,l,h) for(i=(l);i<=(h);++i)
#define FORD(i,h,l) for(i=(h);i>=(l);--i)

typedef vector<int> VI;
typedef vector<string> VS;
typedef vector<double> VD;
typedef long long LL;
typedef pair<int,int> PII;

#define UpInt(x)	((int)(ceil(x)))
#define LowInt(x)	((int)(floor(x)))
#define lowbit(x) (x&-x)

int E[25][25];
bool vis[25];

inline void	dfs(int u,int pos)
{
	vis[u]=1;
	if (pos&(1<<u))	return;
	for (int i=1;i<=E[u][0];++i)
	if (!vis[E[u][i]])	dfs(E[u][i],pos);
}
int findMaxDeadendCount(vector <string> roads)
{
	int N=roads.size(),ret=0;
	if (N==2)	return 2;
	memset(E,0,sizeof(E));
	for (int i=0;i<N-1;++i)
	for (int j=i+1;j<N;++j)
	if (roads[i][j]=='1')
		E[i][++E[i][0]]=j,E[j][++E[j][0]]=i;
	for (int i=1;i<1<<N;++i)
	{
		int cnt=0;
		for (int k=0;k<N;++k)
		if (i&(1<<k))	++cnt;
		memset(vis,0,sizeof(vis));
		for (int j=0;j<N;++j)
		if (!(i&(1<<j)))
		{
			dfs(j,i);break;
		}
		bool mk=1;
		for (int k=0;k<N;++k)
		if (!vis[k])
		{
			mk=0;break;
		}
		if (mk)	ret=max(ret,cnt);
	}
	return ret;
}
int main()
{
	int N;
	string str;
	VS roads;
	freopen("bro.in","r",stdin);
	freopen("bro.out","w",stdout);
	cin>>N;
	for (int i=0;i<N;++i)
		cin>>str,roads.push_back(str);
	cout<<findMaxDeadendCount(roads)<<endl;
	return 0;
}

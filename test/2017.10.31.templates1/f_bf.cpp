#include <iostream>
using namespace std;
int cnt[10];
int main()
{
	int l, r;
	cin >> l >> r;
	for (int i = l; i <= r; i++)
	{
		int x = i;
		do
			cnt[x % 10]++;
		while (x /= 10);
	}
	for (int i = 0; i < 10; i++)
		cout << cnt[i] << endl;
	cin.get();
	cin.get();
	return 0;
}
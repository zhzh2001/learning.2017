#include <iostream>
#include <algorithm>
using namespace std;
const int N = 505;
int a[N][N], f[N][N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= i; j++)
			cin >> a[i][j];
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= i; j++)
			f[i][j] = max(f[i - 1][j] + a[i][j], f[i - 1][j - 1] + a[i][j]);
	cout << *max_element(f[n] + 1, f[n] + n + 1) << endl;
	return 0;
}
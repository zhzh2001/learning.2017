#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 1005, INF = 1e9;
int f[N][N];
int main()
{
	string s, t;
	cin >> s >> t;
	int n = s.length(), m = t.length();
	s = ' ' + s;
	t = ' ' + t;
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	f[0][0] = 0;
	for (int i = 0; i <= n; i++)
		for (int j = 0; j <= m; j++)
		{
			if (i && j)
				f[i][j] = min(f[i][j], f[i - 1][j - 1] + (s[i] != t[j]));
			if (j)
				f[i][j] = min(f[i][j], f[i][j - 1] + 1);
			if (i)
				f[i][j] = min(f[i][j], f[i - 1][j] + 1);
		}
	cout << f[n][m] << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005;
pair<int, int> a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i].first >> a[i].second;
	sort(a + 1, a + n + 1);
	pair<int, int> pred = a[1];
	int ans = 0;
	for (int i = 2; i <= n; i++)
	{
		ans = max(ans, min(a[i].second, pred.second) - a[i].first);
		if (a[i].second > pred.second)
			pred = a[i];
	}
	cout << ans << endl;
	return 0;
}
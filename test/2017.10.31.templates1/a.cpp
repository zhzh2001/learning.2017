#include <iostream>
using namespace std;
const int N = 1005, MOD = 1e9 + 7;
int f[N][N];
int main()
{
	int n, m;
	cin >> n >> m;
	f[1][1] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			if (i != 1 || j != 1)
				f[i][j] = (f[i - 1][j] + f[i][j - 1]) % MOD;
	cout << f[n][m] << endl;
	return 0;
}
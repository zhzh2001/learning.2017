#include <iostream>
using namespace std;
long long calc(long long n, int digit)
{
	if (n < 10)
		return n >= digit;
	long long ans = 0, p;
	for (p = 1; p * 10 <= n; p *= 10)
		if (p == 1)
			ans += n / (p * 10) * p + n % p + (n % (p * 10) / p >= digit);
		else
		{
			if (n % (p * 10) / p >= digit)
				ans += n / (p * 10) * p + n % p + 1;
			else
				ans += n / (p * 10) * p;
			if (p * 100 > n && !digit)
				ans -= n / (p * 10) * p;
		}
	if (!digit)
		return ans;
	if (n / p > digit)
		ans += p;
	else if (n / p == digit)
		ans += n % p + 1;
	return ans;
}
int main()
{
	long long l, r;
	cin >> l >> r;
	for (int i = 0; i < 10; i++)
		cout << calc(r, i) - calc(l - 1, i) << endl;
	cin.get();
	cin.get();
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, MOD = 1e9 + 7;
int n, a[N], t[N], l[N], r[N], root[N], cc;
struct BIT
{
	int tree[N];
	void clear()
	{
		fill_n(tree, sizeof(tree) / sizeof(int), 0);
	}
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T;
struct node
{
	int ls, rs, l, mid, r, lm, mr, lmr;
} tree[N * 20];
inline void pullup(int id)
{
	tree[id].l = (tree[tree[id].ls].l + tree[tree[id].rs].l) % MOD;
	tree[id].mid = (tree[tree[id].ls].mid + tree[tree[id].rs].mid) % MOD;
	tree[id].r = (tree[tree[id].ls].r + tree[tree[id].rs].r) % MOD;
	tree[id].lm = (tree[tree[id].ls].lm + tree[tree[id].rs].lm + 1ll * tree[tree[id].ls].l * tree[tree[id].rs].mid) % MOD;
	tree[id].mr = (tree[tree[id].ls].mr + tree[tree[id].rs].mr + 1ll * tree[tree[id].ls].mid * tree[tree[id].rs].r) % MOD;
	tree[id].lmr = (tree[tree[id].ls].lmr + tree[tree[id].rs].lmr + 1ll * tree[tree[id].ls].l * tree[tree[id].rs].mr + 1ll * tree[tree[id].ls].lm * tree[tree[id].rs].r) % MOD;
}
void modify(int &id, int l, int r, int x, int L, int R, int M)
{
	if (!id)
		id = ++cc;
	if (l == r)
	{
		tree[id].l = L;
		tree[id].r = R;
		tree[id].mid = M;
	}
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(tree[id].ls, l, mid, x, L, R, M);
		else
			modify(tree[id].rs, mid + 1, r, x, L, R, M);
		pullup(id);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	copy(a + 1, a + n + 1, t + 1);
	sort(t + 1, t + n + 1);
	for (int i = 1; i <= n; i++)
		a[i] = lower_bound(t + 1, t + n + 1, a[i]) - t;
	for (int i = 1; i <= n; i++)
	{
		l[i] = T.query(a[i]);
		T.modify(a[i], 1);
	}
	T.clear();
	for (int i = n; i; i--)
	{
		r[i] = T.query(a[i]);
		T.modify(a[i], 1);
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		ans = (ans - tree[root[a[i]]].lmr + MOD) % MOD;
		modify(root[a[i]], 1, n, i, l[i], r[i], 1);
		ans = (ans + tree[root[a[i]]].lmr) % MOD;
	}
	int m;
	cin >> m;
	while (m--)
	{
		int opt, x;
		cin >> opt >> x;
		ans = (ans - tree[root[a[x]]].lmr + MOD) % MOD;
		if (opt == 1)
			modify(root[a[x]], 1, n, x, 0, 0, 0);
		else
			modify(root[a[x]], 1, n, x, l[x], r[x], 1);
		ans = (ans + tree[root[a[x]]].lmr) % MOD;
		cout << ans << endl;
	}
	return 0;
}
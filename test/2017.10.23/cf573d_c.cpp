#include <iostream>
#include <algorithm>
using namespace std;
const int N = 30005;
const long long INF = 1e18;
pair<int, int> w[N], h[N];
int n, m, rankw[N], rankh[N];
long long c2[N], c3[N], f[N];
inline long long unit(int x, int y)
{
	if (x > 0 && x <= n && y > 0 && y <= n)
		return 1ll * w[x].first * h[y].first;
	return -INF;
}
inline void update(int x)
{
	c2[x] = unit(x, x + 1) + unit(x + 1, x);
	c3[x] = max(unit(x, x + 1) + unit(x + 1, x + 2) + unit(x + 2, x), unit(x, x + 2) + unit(x + 1, x) + unit(x + 2, x + 1));
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		cin >> w[i].first;
		w[i].second = i;
	}
	sort(w + 1, w + n + 1);
	for (int i = 1; i <= n; i++)
	{
		cin >> h[i].first;
		h[i].second = i;
	}
	sort(h + 1, h + n + 1);
	for (int i = 1; i <= n; i++)
	{
		rankw[w[i].second] = rankh[h[i].second] = i;
		update(i);
	}
	while (m--)
	{
		int a, b;
		cin >> a >> b;
		swap(h[rankh[a]].second, h[rankh[b]].second);
		swap(rankh[a], rankh[b]);
		for (int i = -2; i <= 0; i++)
		{
			update(rankw[a] + i);
			update(rankh[a] + i);
			update(rankw[b] + i);
			update(rankh[b] + i);
		}
		for (int i = n; i; i--)
		{
			f[i] = max(c2[i] + f[i + 2], c3[i] + f[i + 3]);
			if (w[i].second != h[i].second)
				f[i] = max(f[i], 1ll * w[i].first * h[i].first + f[i + 1]);
		}
		cout << f[1] << '\n';
	}
	return 0;
}
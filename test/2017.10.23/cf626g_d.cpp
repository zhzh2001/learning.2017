#include <iostream>
#include <set>
#include <limits>
using namespace std;
const int N = 200005;
const double eps = 1e-10;
int p[N], l[N], sel[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, t, q;
	cin >> n >> t >> q;
	for (int i = 1; i <= n; i++)
		cin >> p[i];
	for (int i = 1; i <= n; i++)
		cin >> l[i];
	set<pair<double, int>> avail, used;
	for (int i = 1; i <= n; i++)
		avail.insert(make_pair(1.0 * p[i] / (l[i] + 1), i));
	double ans = .0;
	int cnt = 0;
	for (int i = 1; i <= t && !avail.empty(); i++)
	{
		int id = avail.rbegin()->second;
		ans += avail.rbegin()->first;
		if (sel[id])
			used.erase(make_pair(1.0 * p[id] / (l[id] + sel[id]), id));
		sel[id]++;
		used.insert(*avail.rbegin());
		avail.erase(--avail.end());
		if (sel[id] + 1 <= l[id])
			avail.insert(make_pair(1.0 * p[id] / (l[id] + sel[id] + 1), id));
		cnt++;
	}
	cout.precision(numeric_limits<double>::digits10);
	while (q--)
	{
		int opt, r;
		cin >> opt >> r;
		if (opt == 1)
		{
			if (sel[r])
			{
				ans -= 1.0 * p[r] / (l[r] + sel[r]);
				used.erase(make_pair(1.0 * p[r] / (l[r] + sel[r]), r));
			}
			if (sel[r] + 1 <= l[r])
				avail.erase(make_pair(1.0 * p[r] / (l[r] + sel[r] + 1), r));
			l[r]++;
			if (sel[r])
			{
				ans += 1.0 * p[r] / (l[r] + sel[r]);
				used.insert(make_pair(1.0 * p[r] / (l[r] + sel[r]), r));
			}
			if (sel[r] + 1 <= l[r])
				avail.insert(make_pair(1.0 * p[r] / (l[r] + sel[r] + 1), r));
		}
		else
		{
			if (sel[r])
			{
				ans -= 1.0 * p[r] / (l[r] + sel[r]);
				used.erase(make_pair(1.0 * p[r] / (l[r] + sel[r]), r));
			}
			if (sel[r] + 1 <= l[r])
				avail.erase(make_pair(1.0 * p[r] / (l[r] + sel[r] + 1), r));
			l[r]--;
			if (sel[r] && sel[r] <= l[r])
			{
				ans += 1.0 * p[r] / (l[r] + sel[r]);
				used.insert(make_pair(1.0 * p[r] / (l[r] + sel[r]), r));
			}
			else
				cnt--;
			if (sel[r] + 1 <= l[r])
				avail.insert(make_pair(1.0 * p[r] / (l[r] + sel[r] + 1), r));
		}
		while (cnt < t && !avail.empty())
		{
			int id = avail.rbegin()->second;
			ans += avail.rbegin()->first;
			used.erase(make_pair(1.0 * p[id] / (l[id] + sel[id]), id));
			used.insert(*avail.rbegin());
			sel[id]++;
			avail.erase(--avail.end());
			avail.insert(make_pair(1.0 * p[id] / (l[id] + sel[id] + 1), id));
			cnt++;
		}
		while (!avail.empty() && used.begin()->first + eps < avail.rbegin()->first)
		{
			int id = used.begin()->second;
			ans -= used.begin()->first;
			avail.erase(make_pair(1.0 * p[id] / (l[id] + sel[id] + 1), id));
			avail.insert(*used.begin());
			used.erase(used.begin());
			sel[id]--;
			id = avail.rbegin()->second;
			ans += avail.rbegin()->first;
			if (sel[id])
				used.erase(make_pair(1.0 * p[id] / (l[id] + sel[id]), id));
			used.insert(*avail.rbegin());
			avail.erase(--avail.end());
			sel[id]++;
			if (sel[id] + 1 <= l[id])
				avail.insert(make_pair(1.0 * p[id] / (l[id] + sel[id] + 1), id));
		}
		cout << ans << endl;
	}
	return 0;
}
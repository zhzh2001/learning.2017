#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005;
struct commercial
{
	int l, r, id;
	bool operator<(const commercial &rhs) const
	{
		if (l == rhs.l)
			return r < rhs.r;
		return l < rhs.l;
	}
} ad[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		cin >> ad[i].l >> ad[i].r;
		ad[i].id = i;
	}
	sort(ad + 1, ad + n + 1);
	int t = 0;
	for (int i = 1; i <= n; i++)
		if (ad[i].r >= ad[t].r)
			ad[++t] = ad[i];
	n = t;
	long long ans = 0;
	int adi, chi;
	for (int i = 1; i <= m; i++)
	{
		int l, r, c;
		cin >> l >> r >> c;
		for (int j = lower_bound(ad + 1, ad + n + 1, (commercial){0, l, 0}, [](const commercial &lhs, const commercial &rhs) { return lhs.r < rhs.r; }) - ad; j <= n && ad[j].l <= r; j++)
		{
			int inter = min(ad[j].r, r) - max(ad[j].l, l);
			if (1ll * inter * c > ans)
			{
				ans = 1ll * inter * c;
				adi = ad[j].id;
				chi = i;
			}
		}
	}
	cout << ans << endl;
	if (ans)
		cout << adi << ' ' << chi << endl;
	return 0;
}
#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("travel.in");
const int n = 9;
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> dcost(1, 10), ds(1, 100), dday(1, 5);
	fout << n << ' ' << dcost(gen) << ' ' << dcost(gen) << ' ' << ds(gen) << endl;
	int day = dday(gen);
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> dd(1, day);
		fout << dd(gen) << ' ' << ds(gen) << ' ' << ds(gen) + 23 << endl;
	}
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("travel.in");
ofstream fout("travel.out");
const int N = 2005;
struct node
{
	int day, pos, val;
	bool operator<(const node &rhs) const
	{
		if (day == rhs.day)
			return pos < rhs.pos;
		return day < rhs.day;
	}
} a[N];
int n, d, u, s, f[N], g[N][N][2];
inline int cost(int src, int dest)
{
	if (dest < src)
		return (src - dest) * u;
	return (dest - src) * d;
}
int main()
{
	fin >> n >> d >> u >> s;
	for (int i = 1; i <= n; i++)
		fin >> a[i].day >> a[i].pos >> a[i].val;
	sort(a + 1, a + n + 1);
	a[n + 1].pos = s;
	a[n + 1].day = a[n].day + 1;
	for (int l = 1, r; l <= n + 1; l = r)
	{
		for (r = l + 1; r <= n + 1 && a[r].day == a[l].day; r++)
			;
		for (int i = l; i < r; i++)
		{
			g[i][i][0] = a[i].val - cost(s, a[i].pos);
			for (int j = 1; j < l; j++)
				g[i][i][0] = max(g[i][i][0], f[j] + a[i].val - cost(a[j].pos, a[i].pos));
			g[i][i][1] = g[i][i][0];
		}
		for (int i = r - 1; i >= l; i--)
			for (int j = i + 1; j < r; j++)
			{
				g[i][j][0] = max(g[i + 1][j][0] + a[i].val - cost(a[i + 1].pos, a[i].pos), g[i + 1][j][1] + a[i].val - cost(a[j].pos, a[i].pos));
				g[i][j][1] = max(g[i][j - 1][1] + a[j].val - cost(a[j - 1].pos, a[j].pos), g[i][j - 1][0] + a[j].val - cost(a[i].pos, a[j].pos));
			}
		for (int i = l; i < r; i++)
		{
			f[i] = g[i][i][0];
			for (int j = l; j < i; j++)
				f[i] = max(f[i], g[j][i][1]);
			for (int j = i + 1; j < r; j++)
				f[i] = max(f[i], g[i][j][0]);
		}
	}
	/*
	for (int i = 1; i <= n + 1; i++)
	{
		f[i] = a[i].val - cost(s, a[i].pos);
		for (int j = 1; j < i; j++)
			f[i] = max(f[i], f[j] + a[i].val - cost(a[j].pos, a[i].pos));
	}
	*/
	fout << f[n + 1] << endl;
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("LEBOXES.in");
ofstream fout("LEBOXES.out");
const int N = 35;
int n, m, v[N], p[N], a[N], b[N], maxc;
double ans;
void dfsc(int k, int v, int maxv, int d, int maxd, int sel)
{
	if (sel + m - k + 1 < maxc)
		return;
	if (k == m + 1)
		maxc = max(maxc, sel);
	else
	{
		dfsc(k + 1, v, maxv, d, maxd, sel);
		if (v + a[k] <= maxv && d + b[k] <= maxd)
			dfsc(k + 1, v + a[k], maxv, d + b[k], maxd, sel + 1);
	}
}
void dfsp(int k, double prob, int a, int b)
{
	if (!prob)
		return;
	if (k == n + 1)
	{
		maxc = 0;
		dfsc(1, 0, a, 0, b, 0);
		ans += prob * maxc;
	}
	else
	{
		dfsp(k + 1, prob * p[k] / 100.0, a + v[k], b);
		dfsp(k + 1, prob * (1 - p[k] / 100.0), a, b + 1);
	}
}
int main()
{
	int t;
	fin >> t;
	fout.precision(4);
	fout << fixed;
	while (t--)
	{
		fin >> n >> m;
		for (int i = 1; i <= n; i++)
			fin >> v[i] >> p[i];
		for (int i = 1; i <= m; i++)
			fin >> a[i] >> b[i];
		ans = .0;
		dfsp(1, 1.0, 0, 0);
		fout << ans << endl;
	}
	return 0;
}
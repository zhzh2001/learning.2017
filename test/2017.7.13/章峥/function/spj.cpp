#include <fstream>
#include <cmath>
#include <limits>
#include <iostream>
using namespace std;
ifstream fin("function.in"), fout("function.out");
int main()
{
	double a, b, c, w;
	while (fin >> a >> b >> c >> w)
	{
		double x;
		fout >> x;
		double f = a * x + b * sqrt(x) + c * log(x) - w;
		if (fabs(f) >= 1e-3)
		{
			cout.precision(numeric_limits<double>::digits10);
			cout << x << ' ' << f << endl;
			return 1;
		}
	}
	return 0;
}
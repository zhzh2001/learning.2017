#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
typedef long long ll;
const ll oo=1e9;
int n;
ll u,d,s,dp[2005];
struct node{
	int day;
	ll pos,val;
}A[2005];
bool cmp(node a,node b){
	return a.day<b.day;
}
ll max(ll a,ll b){
	if (a>b) return a; else return b;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%lld%lld%lld",&n,&u,&d,&s);
	for (int i=1;i<=n;i++)
		scanf("%d%lld%lld",&A[i].day,&A[i].pos,&A[i].val);
	std::sort(A+1,A+n+1,cmp); A[0].pos=s;
	memset(dp,192,sizeof(dp));
	dp[0]=0;
	for (int i=1;i<=n;i++)
	for (int j=0;j<i;j++)
	if (dp[j]>-oo)
		if (A[i].pos>A[j].pos)
			dp[i]=max(dp[i],dp[j]+A[i].val-(A[i].pos-A[j].pos)*d);
		else 
			dp[i]=max(dp[i],dp[j]+A[i].val-(A[j].pos-A[i].pos)*u);
	ll ans=0;
	for (int i=1;i<=n;i++)
	if (dp[i]>-oo)
		if (A[i].pos>s)
			ans=max(ans,dp[i]-(A[i].pos-s)*u);
		else
			ans=max(ans,dp[i]-(s-A[i].pos)*d);
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

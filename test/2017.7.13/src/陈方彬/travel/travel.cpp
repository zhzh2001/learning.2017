#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=2e3+5;
struct cs{Ll t,s,v;}a[N];
Ll f[N],d[N];
Ll n,U,D,S,ans;
Ll get(Ll x,Ll i){
	Ll y=a[i].v;
	if(a[i].s>a[x].s)y-=(a[i].s-a[x].s)*D;
		else y+=(a[i].s-a[x].s)*U;
	return y;
}
bool cmp(cs x,cs y){if(x.t!=y.t)return x.t<y.t;return x.s<y.s;};
void find(int l,int r){
	for(int i=l;i<=r;i++)
		for(int j=0;j<l;j++)
			f[i]=max(f[i],f[j]+get(j,i));
	for(int i=l;i<=r;i++)d[i]=f[i];
	for(int i=l+1;i<=r;i++)f[i]=max(f[i],f[i-1]+get(i-1,i));
	for(int i=r-1;i>=l;i--)f[i]=max(f[i],f[i+1]+get(i+1,i)-a[i].v);
	for(int i=r-1;i>=l;i--)d[i]=max(d[i],d[i+1]+get(i+1,i));
	for(int i=l+1;i<=r;i++)d[i]=max(d[i],d[i-1]+get(i-1,i)-a[i].v);
	for(int i=l;i<=r;i++)f[i]=max(f[i],d[i]);	
	for(int i=l;i<=r;i++)ans=max(ans,f[i]+get(i,0));
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%lld%lld%lld%lld",&n,&D,&U,&S);
	for(Ll i=1;i<=n;i++)scanf("%lld%lld%lld",&a[i].t,&a[i].s,&a[i].v);
	sort(a+1,a+n+1,cmp);
	a[0].s=S;
	int l=1;
	for(int i=1;i<=n;i++){
		f[i]=-1e18;
		if(a[i+1].t!=a[i].t){
			find(l,i);
			l=i+1;
		}
	}
	printf("%lld",ans);
}

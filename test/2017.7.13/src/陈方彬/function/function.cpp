#include<bits/stdc++.h>
#define Ll long long
using namespace std;
double v,A,B,C,W;
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin>>A>>B>>C>>W;
	double l=0,r=1e9,x;
	while(fabs(r-l)>1e-8){
		x=(l+r)/2.0;
		v=A*x+B*sqrt(x)+C*log(x)-W;
		if(v>0)r=x;else l=x;
	}
	printf("%.5lf",x);
}

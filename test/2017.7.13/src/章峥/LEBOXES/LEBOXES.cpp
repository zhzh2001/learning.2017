#include <fstream>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;
ifstream fin("LEBOXES.in");
ofstream fout("LEBOXES.out");
const int N = 35, INF = 0x3f3f3f3f;
int n, m, v[N], p[N], a[N], b[N], f[N][N];
vector<pair<int, double> > vec1[N / 2], vec2[N / 2];
void dfs(int k, int n, vector<pair<int, double> > vec[], int a, int b, double prob)
{
	if (k == n + 1)
		vec[b].push_back(make_pair(a, prob));
	else
	{
		dfs(k + 1, n, vec, a + v[k], b, prob * p[k] / 100.0);
		dfs(k + 1, n, vec, a, b + 1, prob * (1.0 - p[k] / 100.0));
	}
}
int main()
{
	int t;
	fin >> t;
	fout.precision(4);
	fout << fixed;
	while (t--)
	{
		fin >> n >> m;
		for (int i = 1; i <= n; i++)
			fin >> v[i] >> p[i];
		for (int i = 1; i <= m; i++)
			fin >> a[i] >> b[i];

		memset(f, 0x3f, sizeof(f));
		f[0][0] = 0;
		for (int i = 1; i <= m; i++)
			for (int j = i; j; j--)
				for (int k = n; k >= b[i]; k--)
					f[j][k] = min(f[j][k], f[j - 1][k - b[i]] + a[i]);
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				f[i][j] = min(f[i][j], f[i][j - 1]);

		int n1 = n / 2, n2 = n - n1;
		for (int i = 0; i <= n1; i++)
			vec1[i].clear();
		for (int i = 0; i <= n2; i++)
			vec2[i].clear();
		dfs(1, n1, vec1, 0, 0, 1.0);
		dfs(n1 + 1, n, vec2, 0, 0, 1.0);
		for (int i = 0; i <= n1; i++)
		{
			sort(vec1[i].begin(), vec1[i].end());
			vec1[i].push_back(make_pair(INF, .0));
			for (int j = vec1[i].size() - 2; j >= 0; j--)
				vec1[i][j].second += vec1[i][j + 1].second;
		}
		for (int i = 0; i <= n2; i++)
		{
			sort(vec2[i].begin(), vec2[i].end());
			vec2[i].push_back(make_pair(INF, .0));
			for (int j = vec2[i].size() - 2; j >= 0; j--)
				vec2[i][j].second += vec2[i][j + 1].second;
		}

		double ans = .0;
		for (int i = 0; i <= n1; i++)
			for (int j = 0; j <= n2; j++)
			{
				if (!vec1[i].size() || !vec2[j].size())
					continue;
				double pred = .0;
				for (int k = m; k; k--)
				{
					int cost = f[k][i + j];
					if (cost == INF)
						continue;
					int vec2i = vec2[j].size() - 2;
					double now = .0;
					for (int vec1i = 0; vec1i < (int)vec1[i].size() - 1; vec1i++)
					{
						for (; vec2i >= 0 && vec1[i][vec1i].first + vec2[j][vec2i].first >= cost; vec2i--)
							;
						now += (vec1[i][vec1i].second - vec1[i][vec1i + 1].second) * vec2[j][vec2i + 1].second;
					}
					ans += k * (now - pred);
					pred = now;
				}
			}
		fout << ans << endl;
	}
	return 0;
}
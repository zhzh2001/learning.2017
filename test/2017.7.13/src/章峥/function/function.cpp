#include <fstream>
#include <cmath>
#include <limits>
using namespace std;
ifstream fin("function.in");
ofstream fout("function.out");
const double eps = 1e-6;
int main()
{
	double a, b, c, w;
	fin >> a >> b >> c >> w;
	double l = .0, r = 1e9;
	for (;;)
	{
		double mid = (l + r) / 2, val = a * mid + b * sqrt(mid) + c * log(mid) - w;
		if (val >= .0)
		{
			r = mid;
			if (fabs(val) <= eps)
				break;
		}
		else
			l = mid;
	}
	fout.precision(numeric_limits<double>::digits10);
	fout << r << endl;
	return 0;
}
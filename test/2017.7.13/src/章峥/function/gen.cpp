#include <fstream>
#include <random>
#include <limits>
#include <windows.h>
#include <cmath>
using namespace std;
ofstream fout("function.in");
const int t = 1e5;
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_real_distribution<> d(.0, 5.0), dw(.0, 1e8);
	fout.precision(numeric_limits<double>::digits10);
	for (int i = 1; i <= t; i++)
		for (;;)
		{
			double a = d(gen), b = d(gen), c = d(gen), w = dw(gen);
			if (a * 1e9 + b * sqrt(1e9) + c * log(1e9) - w >= .0)
			{
				fout << a << ' ' << b << ' ' << c << ' ' << w << endl;
				break;
			}
		}
	return 0;
}
#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("travel.in");
const int n = 2000, day = 1;
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> dcost(1, 10), ds(1, 5e5);
	fout << n << ' ' << dcost(gen) << ' ' << dcost(gen) << ' ' << ds(gen) << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> dd(1, day);
		fout << dd(gen) << ' ' << ds(gen) << ' ' << ds(gen) << endl;
	}
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("travel.in");
ofstream fout("travel.ans");
const int N = 20;
struct node
{
	int day, pos, val;
} a[N];
int n, d, u, s, p[N];
inline int cost(int src, int dest)
{
	if (dest < src)
		return (src - dest) * u;
	return (dest - src) * d;
}
int main()
{
	fin >> n >> d >> u >> s;
	for (int i = 1; i <= n; i++)
		fin >> a[i].day >> a[i].pos >> a[i].val;
	for (int i = 1; i <= n; i++)
		p[i] = i;
	int ans = 0;
	do
	{
		bool flag = true;
		for (int i = 1; i < n; i++)
			if (a[p[i + 1]].day < a[p[i]].day)
			{
				flag = false;
				break;
			}
		if (flag)
			for (int i = 0; i < 1 << n; i++)
			{
				int now, pred = 0;
				for (int j = 0; j < n; j++)
					if (i & (1 << j))
					{
						if (!pred)
							now = a[p[j + 1]].val - cost(s, a[p[j + 1]].pos);
						else
							now += a[p[j + 1]].val - cost(a[p[pred]].pos, a[p[j + 1]].pos);
						pred = j + 1;
					}
				now -= cost(a[p[pred]].pos, s);
				if (now > ans)
					ans = now;
			}
	} while (next_permutation(p + 1, p + n + 1));
	fout << ans << endl;
	return 0;
}
#include<cstdio>
#include<string>
#include<algorithm>
using namespace std;
const int maxn=2005;
int n,up_,down_,hom,f[maxn];
struct dyt{
	int t,x,w;
	bool operator <(const dyt &b) const {return (t<b.t);}
}a[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int make(int x){
	if (x<0) return up_*(-x);
	return down_*x;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(); up_=read(); down_=read(); hom=read();
	for (int i=1;i<=n;i++) a[i].t=read(),a[i].x=read(),a[i].w=read();
	sort(a+1,a+1+n);
	a[0].t=a[1].t-1; a[0].w=0; a[0].x=hom;
	n++; a[n].t=a[n-1].t+1; a[n].w=0; a[n].x=hom;
	for (int i=1;i<=n;i++){
		for (int j=i-1;j>=0;j--)
	     f[i]=max(f[i],f[j]+a[i].w-make(a[i].x-a[j].x));
	    if (a[i+1].t!=a[i].t)
	    for (int j=i-1;j<=0;j--) {
	    	if (a[j].t!=a[i].t) break;
	    	for (int k=i;k>j;k--)
	    	if (f[k]!=f[j]+a[k].w-make(a[k].x-a[j].x)) f[j]=max(f[j],f[k]+a[j].w-make(a[j].x-a[k].x));
	    	else f[j]=max(f[j],f[k]-make(a[j].x-a[k].x));
		}
	}
	printf("%d\n",f[n]);
	return 0;
}

#include<cstdio>
#include<string>
#include<cmath>
using namespace std;
double a,b,c,d,l,r,mid,sum,ans;
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
    scanf("%lf %lf %lf %lf",&a,&b,&c,&d);
    l=1e-10; r=1e13;
    while (l<=r) {
    	mid=(l+r)/2;
		sum=a*mid+b*sqrt(mid)+log(mid)*c-d;
    	if (sum<0.0001&&sum>-0.0001) {printf("%.4lf\n",mid); return 0;} 
		if (sum<-0.0001) l=mid+0.00001; else r=mid-0.00001;
	}
	return 0;
}

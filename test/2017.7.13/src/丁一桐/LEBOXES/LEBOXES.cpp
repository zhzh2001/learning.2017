#include<cstdio>
#include<cstring>
#include<string>
using namespace std;
const int maxn=35;
int T,n,m,w[maxn],p[maxn],c[maxn],v[maxn],f[maxn][maxn];
double ans;
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void check(int mon,int dim,double g) {
    memset(f,63,sizeof(f));
	f[0][0]=0;
	int sum=0;
	for (int i=1;i<=m;i++)
	for (int j=0;j<=i;j++)
	for (int k=dim;k>=v[i];k--){
		if (j>0) f[j][k]=min(f[j][k],f[j-1][k-v[i]]+c[i]);
		if (f[j][k]<=mon) sum=max(sum,j);
	}
	ans=ans+g*sum;
}
void dfs(int x,int mon,int dim,double g){
	if (x>n) {check(mon,dim,g); return;}
	dfs(x+1,mon+w[x],dim,g*p[x]/100);
	dfs(x+1,mon,dim+1,g*(100-p[x])/100);
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	T=read();
	for (int k=1;k<=T;k++) {
		n=read(); m=read();
		ans=0;
		for (int i=1;i<=n;i++) w[i]=read(),p[i]=read();
		for (int i=1;i<=m;i++) c[i]=read(),v[i]=read();
		dfs(1,0,0,1);
		printf("%0.4lf\n",ans);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int T;
double ans;
int n,m;
const int N=31;
int maxn;
int sc,sz;
int f[31][100001][31];
int read(){
	int tot=0;
	char c=getchar();
	int f=1;
	while(!isdigit(c) && c!='-')
		c=getchar();
	if(c=='-'){
		f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		tot=tot*10+c-'0';
		c=getchar();
	}
	return f*tot;
}
struct node{
	int c,z;
}b[N],p[N];
void tryy(int num,double g,int cc,int zz){
	if(num==n+1){
		if(g>0.00)
			ans+=(1.0*g*f[m][cc][zz]);
		return;
	}
	else if(g>0.00){
		tryy(num+1,g*(double)(p[num].z*1.0/100),cc+p[num].c,zz);
		tryy(num+1,g*(double)((100-p[num].z)*1.0/100),cc,zz+1);
	}
	return;
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	T=read();
	while(T--){
		ans=0;
		sc=0;
		sz=0;
		n=read(),m=read();
		for(int i=1;i<=n;i++){
			p[i].c=read(),p[i].z=read();
			sc+=p[i].c;
			sz+=p[i].z;
		}
		for(int i=1;i<=m;i++)
			b[i].c=read(),b[i].z=read();
		for(int i=1;i<=m;i++)
			for(int j=0;j<=sc;j++)
				for(int k=0;k<=sz;k++){
					f[i][j][k]=f[i-1][j][k];
					for(int jj=j-b[i].c;jj>=0;jj--)
						for(int kk=k-b[i].z;kk>=0;kk--)
							f[i][j][k]=max(f[i][j][k],f[i][jj][kk]+1);
				}
		tryy(1,1.0,0,0);	
		printf("%.4lf\n",ans);
	}
	return 0;
}

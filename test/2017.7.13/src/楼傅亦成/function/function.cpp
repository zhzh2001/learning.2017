#include<bits/stdc++.h>
using namespace std;
double a,b,c,w;
double cal(double x){
	return (a*x+b*sqrt(x)+c*log(x)-w);
}
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin>>a>>b>>c>>w;
	double mid;
	double x;
	double l=0.0;
	double r=99999999.9999;
	bool can=false;
	while(!can){
		mid=(l+r)*1.0/2;
		x=cal(mid);
		if(abs(x)<0.00001){
			can=true;
			break;
		}
		if(x>=0)
			r=mid;
		else 
			l=mid;
	}
	printf("%.4lf\n",mid);
	return 0;
}

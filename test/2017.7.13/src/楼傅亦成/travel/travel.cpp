#include<bits/stdc++.h>
using namespace std;
const int N=2011;
int n,u,d,h;
int f[N];
int read(){
	int tot=0;
	char c=getchar();
	int f=1;
	while(!isdigit(c) && c!='-')
		c=getchar();
	if(c=='-'){
		f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		tot=tot*10+c-'0';
		c=getchar();
	}
	return f*tot;
}
struct node{
	int day;
	int p;
	int v;
	bool operator < (const node &x) const {
		return day<x.day;
	}
}a[N],q;
int dd[N][N];
int sd=0;
int cos(int from,int to){
	if(from>to)
		return (to-from)*u;
	else 
		return (from-to)*d;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),d=read(),u=read(),h=read();
	for(int i=1;i<=n;i++){
		a[i].day=read();
		a[i].p=read();
		a[i].v=read();
	}
	sort(a+1,a+n+1);
	a[0].day=0;
	a[0].p=h;
	a[0].v=0;
	n++;
	a[n].p=h;
	a[n].v=0;
	f[0]=0;
	for(int i=1;i<=n;i++){
		f[i]=cos(h,a[i].p);
		for(int j=0;j<i;j++)
			f[i]=max(f[i],f[j]+cos(a[j].p,a[i].p)+a[i].v);
	}	
	printf("%d\n",f[n]);
	return 0;
}

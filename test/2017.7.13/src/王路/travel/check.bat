@echo off
g++ bf.cpp -o bf
g++4.8.4 travel_dp.cpp -static -lm -o travel_dp
g++ gen.cpp -o gen
for /l %%s in (1, 1, 1000) do (
echo %%s
gen
bf
travel_dp
fc travel.out bf.out
if errorlevel == 1 pause
)
pause
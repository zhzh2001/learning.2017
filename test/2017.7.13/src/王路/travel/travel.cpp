#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

using namespace std;

ifstream fin("travel.in");
ofstream fout("travel.out");

const int MaxN = 2005;

int n, d, u, s;

int nday, f[MaxN], g[MaxN][MaxN], tmp[MaxN][MaxN];

struct node
{
	int day, pos, val;

	friend bool operator < (const node &a, const node &b)
	{
		return a.day < b.day;
	}
} obj[MaxN];

int stk[MaxN], dans = 0;

void dfs(int x = 0, int top = 0)
{
	if (x == n)
	{
		int tmp = obj[stk[1]].val - abs(obj[stk[1]].pos - s) * (obj[stk[1]].pos > s ? d : u);
		for (int i = 1; i < top; i++)
			tmp += obj[stk[i + 1]].val - abs(obj[stk[i + 1]].pos - obj[stk[i]].pos) * (abs(obj[stk[i + 1]].pos > obj[stk[i]].pos) ? d : u);
		tmp = tmp - abs(obj[stk[top]].pos - s) * (obj[stk[top]].pos > s ? u : d);
		dans = max(dans, tmp);
		return;
	}
	stk[top + 1] = x + 1;
	dfs(x + 1, top + 1);
	dfs(x + 1, top);
}

int cnt[MaxN], Left[MaxN], Day[MaxN], Right[MaxN];

int main()
{
	fin >> n >> d >> u >> s;
	for (int i = 1; i <= n; i++)
		fin >> obj[i].day >> obj[i].pos >> obj[i].val;
	bool flag = false;
	sort(obj + 1, obj + n + 1);
	for (int i = 1; i <= n; i++)
	{
		if (obj[i].day != obj[i - 1].day)
			Day[i] = ++nday;
		else
		{
			Day[i] = nday;
			flag = true;
		}
		// cout << Day[i] << endl;
	}
	if (n <= 5)
	{
		dfs();
		fout << dans << endl;
		return 0;
	}
	if (!flag)
	{
		obj[0].pos = s;
		for (int i = 1; i <= n; i++)
			for (int j = 0; j < i; j++)
			{
				f[i] = max(f[i], f[j] + obj[i].val - abs(obj[i].pos - obj[j].pos) * ((obj[i].pos > obj[j].pos) ? d : u));
			}
		int ans = 0;
		for (int i = 1; i <= n; i++)
		{
			ans = max(ans, f[i] - abs(obj[i].pos - s) * (s > obj[i].pos ? d : u));
			// cout << f[i] << ' ' << f[i] - abs(obj[i].pos - s) * (s > obj[i].pos ? d : u) << endl;
		}
		fout << ans << endl;
	}
	else
	{
		for (int i = 1; i <= n; i++)
		{
			cnt[Day[i]]++;
			if (!Left[Day[i]])
				Left[Day[i]] = i;
			Right[Day[i]] = i;
		}
		obj[0].pos = s;
		for (int i = 1; i <= nday; i++)
		{
            for (int j = Left[i]; j <= Right[i]; j++)
            {
                for (int k = 1; k < i; k++)
                    for (int t = Left[k]; t <= Right[k]; t++)
                    {
                        g[i][j] = max(g[i][j], g[k][t] + obj[j].val - abs(obj[j].pos - obj[t].pos) * ((obj[j].pos > obj[t].pos) ? d : u));
                    }
            }
#ifdef GLOBAL_DEBUG
            /**
			 * tmp
			 */
			for (int j = Left[i]; j <= Right[i]; j++)
			{
				for (int k = Left[i]; k <= Right[i]; k++)
				{
					g[i][j] =max(g[i][j], g[i][k] + obj[j].val - abs(obj[j].pos - obj[k].pos) * ((obj[j].pos > obj[k].pos) ? d : u));				
				}
			}
#endif
        }
		int ans = 0;
		for (int i = 1; i <= nday; i++)
		{
			for (int j = Left[i]; j <= Right[i]; j++)
			{
				ans = max(g[i][j], g[i][j] - abs(obj[j].pos - s) * (s > obj[j].pos ? d : u));
			}
		}
		fout << ans << endl;
	}
#ifdef GLOBAL_DEBUG
	//system("pause");
#endif
	return 0;
}
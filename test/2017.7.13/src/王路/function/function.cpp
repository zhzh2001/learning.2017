#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include<limits>
using namespace std;

ifstream fin("function.in");
ofstream fout("function.out");

typedef long double ld;

const double eps = 1e-10;

ld A, B, C, W;

ld calc(ld k)
{
    ld res = A * k + B * sqrtl(k) + C * logl(k) - W;
    return res;
}

int main()
{
    fin >> A >> B >> C >> W;
    ld l = .0, r = 1e9, ans = r;
    while (l + eps < r)
    {
        ld mid = (l + r) / 2.0, tmp = calc(mid);
        if (tmp < eps && tmp > -eps)
        {
            fout << setprecision(6) << fixed << mid << endl;
            return 0;
        }
        else if (tmp < -eps)
            l = mid;
        else if (tmp > eps)
            r = mid;
    }
    fout << setprecision(numeric_limits<ld>::digits10) << fixed << ans << endl;
#ifdef GLOBAL_DEBUG
    system("pause");
#endif
    return 0;
}
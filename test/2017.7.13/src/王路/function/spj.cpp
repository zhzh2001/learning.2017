#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

double A, B, C, W, ans;

int main()
{
    ifstream f1("function.in");
    f1 >> A >> B >> C >> W;
    ifstream f2("function.out");
    f2 >> ans;
    double res;
    if (ans < 1e-6)
        res = A * ans + B * sqrt(ans) + C - W;
    else
        res = A * ans + B * sqrt(ans) + C * log(ans) - W;
    if (fabs(res) < 1e-3)
    {
        puts("AC");
		return 0;
    }
    else
    {
        if (ans == 1e9) return 0;
        printf("WA %lf, %lf\n", ans, res);
		return 1;
    }
    return 0;
}
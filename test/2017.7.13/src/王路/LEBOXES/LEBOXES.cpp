#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <map>
#include <vector>
#include <iomanip>
#include <ctime>

using namespace std;

ifstream fin("LEBOXES.in");
ofstream fout("LEBOXES.out");

const int MaxN = 31;
const double eps = 1e-8;

struct Box
{
	int p, v;
} box[MaxN];

struct Obj
{
	int m, d, id;

	friend bool operator < (const Obj &a, const Obj &b)
	{
		return a.m < b.m || (a.m == b.m && a.d < b.d);
	}
} obj[MaxN];

inline bool cmp1(const Obj &a, const Obj &b)
{
	return a.m < b.m;
}

inline bool cmp2(const Obj &a, const Obj &b)
{
	return a.d < b.d;
}

int n, m, f[MaxN][MaxN], vis[MaxN];

struct Status
{
	int diamond, money;

	Status() {}
	Status(int d, int m)
		: diamond(d), money(m)
	{
	}
};

typedef pair<Status, double> data_type;

vector<data_type> vec;

void dfs(int x, double p, Status now)
{
	if (x == n)
	{
		if (p < eps) return;
		vec.push_back(make_pair(Status(now.diamond, now.money), p));
		return;
	}
	dfs(x + 1, p * (double)box[x + 1].p / 100.0, Status(now.diamond, now.money + box[x + 1].v));
	dfs(x + 1, p * (double)(100 - box[x + 1].p) / 100.0, Status(now.diamond + 1, now.money));
}

int main()
{
	srand(time(NULL));
	int T;
	fin >> T;
	while (T--)
	{
		vec.clear();
		fin >> n >> m;
		for (int i = 1; i <= n; i++)
			fin >> box[i].v >> box[i].p;
		for (int i = 1; i <= m; i++)
		{
			fin >> obj[i].m >> obj[i].d;
			obj[i].id = i;
		}
		if (n <= 15)
		{
			dfs(0, 1, Status(0, 0));
			double ans = .0;
			for (vector<data_type>::iterator it = vec.begin(); it != vec.end(); ++it)
			{
				int cnt = 0;

				Status now = it->first;
				sort(obj + 1, obj + m + 1, cmp1);
				int tmp = 0;
				for (int i = 1; i <= m; i++)
				{
					if (now.diamond >= obj[i].d && now.money >= obj[i].m)
					{
						now.diamond -= obj[i].d;
						now.money -= obj[i].m;
						tmp++;
					}
				}
				cnt = max(cnt, tmp);

				now = it->first;
				sort(obj + 1, obj + m + 1, cmp2);
				tmp = 0;
				for (int i = 1; i <= m; i++)
				{
					if (now.diamond >= obj[i].d && now.money >= obj[i].m)
					{
						now.diamond -= obj[i].d;
						now.money -= obj[i].m;
						tmp++;
					}
				}
				cnt = max(cnt, tmp);

				// cout << cnt << endl;

				for (int i = 1; i <= 15; i++)
				{
					random_shuffle(obj + 1, obj + m + 1);
					tmp = 0;
					for (int i = 1; i <= m; i++)
					{
						if (now.diamond >= obj[i].d && now.money >= obj[i].m)
						{
							now.diamond -= obj[i].d;
							now.money -= obj[i].m;
							tmp++;
						}
					}
					cnt = max(cnt, tmp);
				}
				ans += (double)cnt * it->second;
			}
			fout << setprecision(4) << fixed << ans << endl;
			continue;
		}
	}
#ifdef GLOBAL_DEBUG
	// system("pause");
#endif
	return 0;
}
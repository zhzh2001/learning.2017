#include <cstring>
#include <algorithm>
#include <cstdio> 
#include <cstdlib>
#include <cmath>
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
using namespace std;
typedef long long LL;
const int N=30+5;
const LL Inf=1ll<<40;
const double Eps=1e-8;
int T,n,m,b[N];
LL v[N],a[N],dp[N][N],f[N][N][N];
double P[N],ans;
double getans(int di,int dol){
	int le=0,ri=m,mid,ans;
	while (le<=ri){
		mid=(le+ri)/2;
		if (dp[di][mid]==dol)
			return mid;
		if (dp[di][mid]>dol)
			ri=mid-1;
		else 
			le=mid+1,ans=mid;
	}
	return ans;
}
void dfs(int k,double p,int dol,int di){
	if (fabs(p)<1e-8)
		return;
	if (k>n){
		ans+=p*getans(di,dol);
		return;
	}
	dfs(k+1,p*   P[k] ,dol+v[k],di  );
	dfs(k+1,p*(1-P[k]),dol     ,di+1);
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++){
			scanf("%lld%lf",&v[i],&P[i]);
			P[i]/=100.0;
		}
		for (int i=1;i<=m;i++)
			scanf("%lld%d",&a[i],&b[i]);
		for (int i=0;i<=30;i++)
			for (int j=0;j<=m;j++)
				for (int k=0;k<=m;k++)
					f[i][j][k]=Inf;
		for (int i=0;i<=30;i++)
			for (int j=0;j<=m;j++)
				f[i][j][0]=0;
		for (int i=0;i<=30;i++)
			for (int j=1;j<=m;j++)
				for (int k=1;k<=j;k++){
					f[i][j][k]=f[i][j-1][k];
					for (int x=0;x<=i-b[j];x++)
						f[i][j][k]=min(f[i][j][k],f[x][j-1][k-1]+a[j]);
				}
//		printf("%lld\n",f[1][2][1]);
		for (int i=0;i<=30;i++)
			for (int j=0;j<=m;j++){
				dp[i][j]=j?Inf:0;
				for (int k=j;k<=m;k++)
					dp[i][j]=min(dp[i][j],f[i][k][j]);
			}
/*		for (int i=0;i<=3;i++){
			for (int j=0;j<=2;j++)
				if (dp[i][j]<1e9)
					printf("%5d",dp[i][j]);
				else
					printf("   -1");
			puts("");
		}*/
		ans=0;
		dfs(1,1,0,0);
		printf("%.4lf\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstring>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cmath>
using namespace std;
const double di=2.71828183;
const double kk=2.30258509;
const double Eps=1e-6;
const double Eps2=1e-4;
double a,b,c,w;
bool eq(double a,double b){
	return fabs(a-b)<Eps;
}
double ln1(double x){
	double le=0,ri=21,mid,p,ans;
	while (ri-le>Eps){
		mid=(le+ri)/2.0;
		p=pow(di,mid);
		if (eq(p,x))
			return ans;
		if (p>x)
			ri=mid;
		else
			le=mid;
	}
}
double ln(double x){
//	printf("%lf\n",log(x));
	return log(x)*kk;
}
int main(){
//	printf("%lf",log(10));
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
	if (w==0&&c==0){
		printf("0");
		fclose(stdin);fclose(stdout);
		return 0;
	}
	double le=Eps,ri=1e9,mid,tot,ans;
	while (1){
		mid=(le+ri)/2.0;
		tot=a*mid+b*sqrt(mid)+c*log(mid)-w;
		if (fabs(tot)<Eps){
			ans=mid;
			break;
		}
		if (tot>0)
			ri=mid;
		else
			le=mid,ans=mid;
	}
	printf("%lf",ans);
	fclose(stdin);fclose(stdout);
	return 0;
} 

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef double D;
D a,b,c,w;
inline D f(D x){
	D ans=a*x+b*sqrt(x)-w+(x?c*log(x):0);
	return ans;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
	D l=0,r=1e9;
	while(r-l>1e-7){
		D mid=(l+r)/2;
		if(0-f(mid)>1e-7)l=mid;
		else r=mid;
	}
	printf("%.6lf",l);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int a,b;}a[1001];
int n,m,v[101],f[101][101];
double p[101],ans=0;
inline void check(double gl,int A,int B){
	int i=1;for(;i<=m;++i)if(f[i][B]>A)break;
	--i;ans+=(double)i*gl;
}
inline void dfs(int x,double gl,int A,int B){
	if(gl-0<1e-9)return;
	if(x==n+1){check(gl,A,B);return;}
	dfs(x+1,gl*p[x],A+v[x],B);
	dfs(x+1,gl*(1-p[x]),A,B+1);
}
int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T=read();while(T--){
		n=read();m=read();ans=0;
		int cnt=1;
		for(int i=1;i<=n;++i){
			v[i]=read(),p[i]=read();
			if(p[i]==100||p[i]==0)swap(v[i],v[cnt]),swap(p[i],p[cnt]),++cnt;
		}
		for(int i=1;i<=n;++i)p[i]=(double)p[i]/100;
		for(int i=1;i<=m;++i)a[i].a=read(),a[i].b=read();
		memset(f,0x3f,sizeof f);
		for(int i=0;i<=n;++i)f[0][i]=0;
		for(int i=1;i<=m;++i)
			for(int j=1;j<=m;++j)
				for(int k=n;k>=a[i].b;--k)f[j][k]=min(f[j][k],f[j-1][k-a[i].b]+a[i].a);
		dfs(1,1,0,0);
		printf("%.4lf\n",ans);
	}
	return 0;
}
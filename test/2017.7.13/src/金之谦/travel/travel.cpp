#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
struct ppap{int d,p,v;}a[10001];
inline bool cmp(ppap a,ppap b){return a.d==b.d?a.p<b.p:a.d<b.d;}
int n,d,u,s,f[2001],g[2001];
inline int D(int x,int y){
	if(x<y)return d*(y-x);
	else return u*(x-y);
}
signed main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();d=read();u=read();s=read();
	for(int i=1;i<=n;i++)a[i].d=read(),a[i].p=read(),a[i].v=read();
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++)f[i]=g[i]=-1e9;
	f[0]=0;a[0].p=s;
	for(int i=1;i<=n;){
		int l=i,r=i;
		while(a[r+1].d==a[l].d)r++;
		if(l==r){
			for(int j=0;j<i;j++)f[i]=max(f[i],f[j]-D(a[j].p,a[i].p)+a[i].v);
		}else{
			for(int k=l;k<=r;k++)
				for(int j=0;j<l;j++)f[k]=g[k]=max(f[k],f[j]-D(a[j].p,a[k].p)+a[k].v);
			for(int k=l+1;k<=r;k++)
				for(int j=l;j<k;j++)f[k]=max(f[k],f[j]-D(a[j].p,a[k].p)+a[k].v);
			for(int k=r-1;k>=l;k--)
				for(int j=r;j>k;j--)g[k]=max(g[k],g[j]-D(a[j].p,a[k].p)+a[k].v);
			for(int k=l;k<=r;k++)f[k]=max(f[k],g[k]);
		}
		i=r+1;
	}
	int ans=0;
	for(int i=1;i<=n;i++)ans=max(ans,f[i]-D(a[i].p,s));
	writeln(ans);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;

const double eps = 1e-8;
double A, B, C, W;
double l = 0, r = 1e9, mid;

double f(double x){
	return A*x+B*sqrt(x)+C*log(x)-W;
}

int main(){
	freopen("function.in", "r", stdin);
	freopen("function.out", "w", stdout);
	
	scanf("%lf%lf%lf%lf", &A, &B, &C, &W);
	while (r - l > eps){
		mid = (l+r) * 0.5;
		if (f(mid) > 0) r = mid; 
		else l = mid;
	}
	printf("%.8lf", l);
	
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

const int maxn = 2009;
struct show{
	int d, p, v;
	bool operator < (const show &a) const{
		return d < a.d || d == a.d && p < a.p;
	}
} a[maxn];
int f[maxn], g[maxn], x[maxn];
int n, up, down, s;

inline int read(){
	char ch = getchar(); int x = 0, f = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') f = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return f*x;
}

int search(int k){
	int l = 0, r = n, mid;
	while (l < r){
		mid = (l+r+1) >> 1;
		if (x[mid] <= k) l = mid;
		else r = mid-1;
	}
	return l;
}

int main(){
	freopen("travel.in", "r", stdin);
	freopen("travel.out", "w", stdout);
	
	n = read(); down = read(); up = read(); 
	s = x[0] = read();
	for (int i=1; i<=n; i++){
		a[i].d = read();
		x[i] = a[i].p = read();
		a[i].v = read();
	}
	sort(x, x+n+1);
	s = search(s);
	for (int i=1; i<=n; i++)
		a[i].p = search(a[i].p);
	sort(a+1, a+n+1);
		
	memset(f, 0xc0, sizeof f); f[s] = 0;
	int i = 0, j = 0, t, best, val;
	while (j < n){
		i = j = j+1;
		while (j < n && a[j+1].d == a[i].d) j++;
		for (int k=0; k<=n; k++) g[k] = f[k];
		
		best = 0; val = -1e9; t = i-1;
		for (int k=0; k<=n; k++){
			if (k) best -= (down+up)*(x[k]-x[k-1]);
			best = max(best, 0);
			if (t < j && a[t+1].p == k){	//位置k处有商家t 
				t++;
				g[k] = max(g[k], a[t].v+val-x[k]*down);
				best += a[t].v;
			}
			val = max(val, best+f[k]+x[k]*down);
		}
		
		best = 0; val = -1e9; t = j+1;
		for (int k=n; k>=0; k--){
			if (k < n) best -= (down+up)*(x[k+1]-x[k]);
			best = max(best, 0);
			if (t > i && a[t-1].p == k){
				t--;
				g[k] = max(g[k], a[t].v+val+x[k]*up);
				best += a[t].v;
			}
			val = max(val, best+f[k]-x[k]*up);
		}
		
		for (int k=0; k<=n; k++) f[k] = g[k];
	}
	
	int ans = 0;
	for (int i=0; i<=n; i++)
		if (i < s) ans = max(ans, f[i]-(x[s]-x[i])*down);
		else ans = max(ans, f[i]-(x[i]-x[s])*up);
	printf("%d\n", ans);
	
	return 0;
}

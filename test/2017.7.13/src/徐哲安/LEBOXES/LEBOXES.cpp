#include<bits/stdc++.h>
using namespace std;

const int maxn = 35;
struct obj{ int w, d; } a[maxn];
struct box{ double p; int v; } b[maxn];
struct node{
	int money; double prob;
	bool operator < (const node &a) const{
		return money < a.money;
	}
} tmp;
vector<node> c[maxn];
int f[maxn][maxn], n, m;
double ans = 0;

void solve(){
	puts("0.5");
}

void init(){
	memset(f, 0x3f, sizeof f);
	f[0][0] = 0;
	for (int i=1; i<=m; i++)
		for (int j=n; j>=a[i].d; j--)
			for (int k=i; k>=1; k--)
				f[j][k] = min(f[j][k], f[j-a[i].d][k-1]+a[i].w);
	for (int i=1; i<=n; i++)
		for (int j=0; j<=m; j++){
			f[i][j] = min(f[i][j], f[i-1][j]);
			//printf("%d,%d,%d\n", i, j, f[i][j]);
		}
}

void update1(double p, int res1, int res2){
	tmp.money = res2; tmp.prob = p;
	c[res1].push_back(tmp);
}

//第k个箱子  概率为p  有res1个钻石  有res2元 
void dfs1(int k, double p, int res1, int res2){	
	if (p <= 1e-12) return;
	if (k > n/2) { update1(p, res1, res2); return; }
	dfs1(k+1, p*b[k].p, res1, res2+b[k].v);
	dfs1(k+1, p*(1-b[k].p), res1+1, res2);
}

double search(int i, int k){
	if (k > c[i][c[i].size()-1].money) return 0;
	int l = 0, r = c[i].size()-1, mid;
	while (l < r){
		mid = (l+r) >> 2;
		if (k <= c[i][mid].money) r = mid;
		else l = mid+1;
	}
	if (l == 0) return c[i][c[i].size()-1].prob;
	return c[i][c[i].size()-1].prob-c[i][l-1].prob;
}

void update2(double p, int x, int y){
	int	need; 
	for (int i=0; 2*i<=n; i++)		//另一边的钻石数
		for (int j=1; j<=m; j++){	//购买的物品数量
			need = f[i+x][j] - y;
			ans += j*p*search(i, need);
			//printf("%d,%d,%lf,%lf\n", i+x, need, p, search(i, need));
		} 
}

void dfs2(int k, double p, int res1, int res2){
	if (p <= 1e-12) return;
	if (k > n) { update2(p, res1, res2); return; }
	dfs2(k+1, p*b[k].p, res1, res2+b[k].v);
	dfs2(k+1, p*(1-b[k].p), res1+1, res2);
}
/*
2
2 2
2 50
2 100
2 0
2 0
2 2
2 100
2 50
0 2
0 1
*/
int main(){
	freopen("LEBOXES.in", "r", stdin);
	freopen("LEBOXES.out", "w", stdout);
	int T; double t; scanf("%d", &T);
	while (T--){
		scanf("%d%d", &n, &m);
		for (int i=1; i<=n; i++){
			scanf("%d%lf", &b[i].v, &b[i].p);
			b[i].p /= 100;
		}
		for (int i=1; i<=m; i++)
			scanf("%d%d", &a[i].w, &a[i].d);
		//if (n == 1) { solve(); continue; }
		init(); 
		
		dfs1(1, 1, 0, 0);
		for (int i=1; i<=n; i++){
			sort(c[i].begin(), c[i].end());
			for (int j=1; j<c[i].size(); j++){
				t = c[i][j-1].prob;
				tmp.money = c[i][j].money;
				tmp.prob = t+c[i][j].prob;
				c[i][j] = tmp;
			}
		}
			
		ans = 0;
		dfs2(n/2+1, 1, 0, 0);
		printf("%.4lf\n", ans);
	}
	return 0;
}

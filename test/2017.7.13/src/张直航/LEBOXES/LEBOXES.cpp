#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
const int N=40;
int T;
int n,m;
double ans;

struct box
{
	int v,p;
}b[N],va[N];

void init()
{
	ans=0;
	memset(b,0,sizeof(b));
	memset(va,0,sizeof(va));
}

void input()
{
	scanf("%d%d",&n,&m);
	int i;
	for(i=1;i<=n;i++)
		scanf("%d%d",&b[i].v,&b[i].p);
	for(i=1;i<=n;i++)
		scanf("%d%d",&va[i].v,&va[i].p);
}

int dfs2(int trade,int val,int diamond,int now)
{
	if(val<0||diamond<0)
		return 0;
	if(trade==m+1)
		return now-1;
	int maxn=0,t;
	for(int i=trade+1;i<=m+1;i++)
	{
		t=dfs2(i,val-va[i].v,diamond-va[i].p,now+1);
		if(t>maxn)
			maxn=t;
	}
	return maxn;
}

void dfs(int bo,double p,int val,int diamond)
{
	if(p==0)
		return ;
	if(bo==n)
	{
		ans+=(double)dfs2(0,val,diamond,0)*p;
		return ;
	}
	dfs(bo+1,p*b[bo+1].p/100,val+b[bo+1].v,diamond);
	dfs(bo+1,p*(1.0-(double)b[bo+1].p/100),val,diamond+1);
}

void solve()
{
	dfs(0,1,0,0);
	printf("%.4lf\n",ans);
}

int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		init();
		input();
		solve(); 
	}
	return 0;
}

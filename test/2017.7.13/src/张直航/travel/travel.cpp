#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
int n,d,u,s;
long long ans;
struct X
{
	int t,l,m;
}a[3000];
int day[600000];
int pos[3000],top,temp[3000];
long long f[2500][2500];

bool cmp(X i,X j)
{
	return i.t<j.t;
}

long long Max(long long i,long long j)
{
	if(i>j)
		return i;
	return j;
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d%d%d",&n,&d,&u,&s);
	int i,j,k;
	for(i=1;i<=n;i++)
		scanf("%d%d%d",&a[i].t,&a[i].l,&a[i].m);
	sort(a+1,a+n+1,cmp);
	for(i=1;i<=n;i++)
	{
		if(a[pos[top]].t!=a[i].t)
			pos[++top]=i;
		day[a[i].t]=top;
	}
	long long dis=0,tmp;
	a[0].l=s;
	a[0].t=0;
	a[0].m=0;
	memset(f,0,sizeof(f));
	for(i=1;i<=n;i++)
	{
		dis=a[i].l-s;
		if(dis<0)
			dis=dis*u;
		else
			dis=-dis*d;
		f[day[a[i].t]][i]=dis+a[i].m;
	}
	for(i=1;i<=top;i++)
	{
		for(k=1;k<=n;k++)
			temp[k]=f[i-1][k];
		for(j=1;j<=n;j++)
		{
			if(day[a[j].t]!=i)
				f[i][j]=f[i-1][j];
			if(a[j].t!=a[pos[i]].t)
				continue;
			for(k=1;k<=n;k++)
			{
				if(a[k].t>a[pos[i]].t||k==j)
					continue;
				dis=a[j].l-a[k].l;
				if(dis<0)
					dis=dis*u;
				else
					dis=-dis*d;
				f[i][j]=Max(f[i][j],temp[k]+dis+a[j].m);
			}
		}
	}
	ans=0;
	for(i=1;i<=n;i++)
	{
		dis=a[i].l-s;
		if(dis<0)
			dis=dis*d;
		else
			dis=-dis*u;
		tmp=f[top][i]+dis;
		if(tmp>ans)
			ans=tmp;
	}
	printf("%lld\n",ans);
	return 0;
}

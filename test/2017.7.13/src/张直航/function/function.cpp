#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <climits>
using namespace std;
const double EPS=1e-6;
const double PS=1e-5;
double a,b,c,w;

double Abs(double i)
{
	if(i<-EPS)
		return -i;
	return i;
}

bool is(double i,double j)
{
	if(Abs(i-j)<PS)
		return true;
	return false;
}

double function(double x,int kind=2)//kind=1 x==0 kind=2 x>0
{
	if(kind==1)
		return a*x+b*(sqrt(x))-w;
	return a*x+b*(sqrt(x))+c*log(x)-w;
}

int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
	double ans=0,l,r,z,y,x,mid;
	if(is(c,0))
	{
		if(is(a,0))
		{
			ans=(w/b)*(w/b);
			printf("%lf\n",ans);
			return 0;
		}
		if(is(b,0))
		{
			ans=w/a;
			printf("%lf\n",ans);
			return 0;
		}
		x=((-b)+sqrt(b*b+4.0*a*w))/(2.0*a);
		r=x+0.5;
		l=x-0.5;
		if(l<EPS)
			l=0;
		while(!is(l,r))
		{
			mid=(l+r)/2;
			z=function(r,1);
			y=function(l,1);
			x=function(mid,1);
			if(is(x,0))
			{
				ans=mid;
				break;
			}
			if(z>EPS&&y>EPS)
			{
				l+=EPS;
				r-=EPS;
			}
			else if(x*y<EPS)
				r=mid;
			else
				l=mid;
		}
		if(is(ans,0))
			ans=(l+r)/2;
		printf("%lf\n",ans);
	}
	else
	{
		l=EPS;
		r=1e9;
		while(!is(l,r))
		{
			mid=(l+r)/2;
			z=function(r);
			y=function(l);
			x=function(mid);
			if(is(x,0))
			{
				ans=mid;
				break;
			}
			else if(x*y<EPS)
				r=mid;
			else
				l=mid;
		}
		if(is(ans,0))
			ans=(l+r)/2;
		printf("%lf\n",ans);	
	}
	return 0;
}

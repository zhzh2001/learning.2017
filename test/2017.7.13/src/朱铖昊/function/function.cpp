#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
double a,b,c,w;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
double f(ll y)
{
	double x=(double)y/1e6;
	return a*x+b*sqrt(x)+c*log(x)-w;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout); 
	cin>>a>>b>>c>>w;
	ll l=0,r=1e15;
	while(l<r)
	{
		ll mid=(l+r)/2;
		double zyy=f(mid);
		if(zyy<1e-3&&zyy>-1e-3)
		{
			double ans;
			ans=(double)mid/1e6;
			printf("%.6f",ans);
			//cout<<ans<<endl;
			return 0;
		}
		else
		{
			if(zyy<0)l=mid+1;
				else r=mid;
		}
	}
	double ans;
	ans=(double)l/1e6;
	printf("%.6f",ans);
	return 0;
}


/*
	DP
	先按时间先后排序

	考虑没有两个展销会在同一天：
	然后按从未来到现在的顺序倒推
	令f[i]表示参加第i个展销会所得最大净收益
	对于当前展销会，
	有几个选择：
	1.直接回家（-路费
	2.去未来的某个展销会j（ f[j]-去j的路费

	答案取收益最大的
	由下一层递推取Max(直接选取更未来的展销会就相当于不选j)

	答案为f[0](从家出发)

	O(n^2)

	然后考虑有展销会在同一天的情况：
	可以有的选择是：去？不去？去那些？
	分组DP nonono

	转换问题：
	一条线段上有若干个点与它们的权值
	从一个点移动到另一个点需要耗费一定的路费
	求一个移动方案净收益最大


*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>

using namespace std;
template<typename T>
void read(T& A)
{
	char r;
	bool f=false;
	for(r=getchar(); (r<48||r>57)&&r!='-'; r=getchar());
	if(r=='-')
	{
		f=true;
		r=getchar();
	}
	for(A=0; r>=48&&r<=57; r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b)
{
	return a>b?a:b;
}
template<typename T>
T Min(const T& a,const T& b)
{
	return a<b?a:b;
}
template<typename T>
T Swap(const T& a,const T& b)
{
	T t=a;
	a=b;
	b=t;
}

const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;

const int maxn=2003;
const int maxday=500003;
const int maxpos=500004;
const int maxval=4003;

int n,d,u,s;
struct MEETING
{
	int day,pos,val;
	inline void input()
	{
		read(day);
		read(pos);
		read(val);
	}
} meeting[maxn];
#define home meeting[0]
int sumv[maxn];
bool cmp(const MEETING& a,const MEETING& b)
{
	if(a.day!=b.day)
		return a.day<b.day;
	else return a.pos<b.pos;
}
void input()
{
	read(n);read(d);read(u);read(s);
	home.day=0;
	home.pos=s;
	home.val=0;
	for(int i=1; i<=n; ++i)
	{
		meeting[i].input();
		sumv[i]=sumv[i-1]+meeting[i].val;
	}
	sort(meeting+1,meeting+n+1,cmp);
}
int boatCost(int a,int b) //从展销会i到展销会j所花的船费
{
	int d1=meeting[a].pos;
	int d2=meeting[b].pos;
	if(d1>d2)
		return (d1-d2)*u;
	else if(d1<d2)
		return (d2-d1)*d;
	else return 0;
}
int f[maxn];
int calc(int now,int start,int l,int r)//从start位置出发,在同一天举行的l~r这些展销会中
{
	int ans=-inf;
	if(meeting[l].pos<start&&meeting[r].pos>start)
	{
		for(int up=l; meeting[up].pos<start; ++up)
		{
			for(int down=r; meeting[down].pos>start; --down)
			{
				int all= - boatCost(up,down) - boatCost(down,up);
				for(int ending=up; ending<=down; ++ending)
				{
					int tans=all+boatCost(ending,now)+f[ending]+ sumv[down]-sumv[up-1] -meeting[ending].val;
					if(tans>ans)
						ans=tans;
				}
			}
		}
	}
	else if(meeting[r].pos<start)
	{
		for(int up=l; meeting[up].pos<start&&up<=r; ++up)
		{
			int all= - boatCost(up,now) - boatCost(now,up);
			for(int ending=up; ending<=r; ++ending)
			{
				int tans=all+boatCost(ending,now)+f[ending] + sumv[r]-sumv[up-1] -meeting[ending].val;
				if(tans>ans)
					ans=tans;
			}
		}
	}
	else if(meeting[l].pos>start)
	{
		for(int down=r; meeting[down].pos>start&&down>=l; --down)
		{
			int all= - boatCost(now,down) - boatCost(down,now);
			for(int ending=l; ending<=down; ++ending)
			{
				int tans=all+boatCost(ending,now)+f[ending] + sumv[down]-sumv[l-1] -meeting[ending].val;
				if(tans>ans)
					ans=tans;
			}
		}
	}
	return ans;
}

void solve()
{
	for(int i=n; i>=0; --i)
	{
		f[i]= - boatCost(i,0);
		for(int j=i+1; j<=n; ++j)
		{
			while(meeting[j].day==meeting[i].day&&j<=n)
				++j;
			if(j<=n)
			{
				int rr,tans;
				for(rr=j+1; rr<=n&&meeting[rr].day==meeting[j].day; ++rr);
				--rr;
				if(rr==j)
				{
					tans=f[j]-boatCost(i,j);
				}
				else
				{
					tans=calc(i,meeting[i].pos,j,rr);
				}
				if(tans>f[i])
					f[i]=tans;
			}

		}
		f[i]+=meeting[i].val;
		//cout<<f[i]<<endl;
	}
}
int main()
{

	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	input();
	solve();
	printf("%d\n",f[0]);
	return 0;
}

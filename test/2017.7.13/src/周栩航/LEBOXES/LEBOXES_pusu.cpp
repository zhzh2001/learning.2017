#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define eps 1e-10
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int f[41][41][41],n,m,w1[41],w2[41],v[41];
double p[101],ans=0;		
inline void dp(int v1,int v2,double P)
{
	memset(f,-1,sizeof f);
	f[0][0][0]=v1;
	For(i,1,m)
		For(j,0,i)
			For(k,0,v2)
			{
				if(j-1>=0&&k>=w2[i])f[i][j][k]=max(f[i-1][j][k],f[i-1][j-1][k-w2[i]]-w1[i]);
				else	f[i][j][k]=f[i-1][j][k];
			}
	Dow(i,1,m)
		For(j,0,v2)	if(f[m][i][j]>=0)	{ans+=P*i;return;}
}
inline void dfs(int x,int v1,int v2,double P)
{
	if(x==n+1)
	{
		dp(v1,v2,P);
		return;
	}
	dfs(x+1,v1+v[x],v2,P*p[x]);
	dfs(x+1,v1,v2+1,P*(1-p[x]));
}
int T;
int main()
{
	freopen("LEBOXES.in","r",stdin);freopen("LEBOXES_pusu.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();m=read();
		For(i,1,n)
			v[i]=read(),p[i]=read(),p[i]=p[i]/100.0;
		ans=0;
		For(i,1,m)
			w1[i]=read(),w2[i]=read();
		dfs(1,0,0,1);
		printf("%.4lf\n",ans);
	}
}
/*
2
2 2
2 50
2 100
2 0
2 0
2 2
2 100
2 50
0 2
0 1
*/
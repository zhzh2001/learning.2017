#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#define E 2.71828181
using namespace std;
double lad[20]={0,1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,0};
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	double a,b,c,w;
	scanf("%lf %lf %lf %lf",&a,&b,&c,&w);
	int rnk=1;
	double x=lad[rnk],ans1,ans2,ans;
	if(a>=-0.1&&b>=-0.1&&c>-0.1){//Binary search
		double l=0.0,r=1e9,mid;
		while(1){
			mid=(l+r)/2;
			ans=a*mid+b*sqrt(mid)+c*log(mid)/log(E)-w;
			if(fabs(ans)<=0.00001) break;
			if(ans>0) r=mid;
			else if(ans<0) l=mid;
		}
		printf("%.4lf",mid);
	}
	else if(a<=0&&b<=0&&c<=0){
		double l=0.0,r=1e9,mid;
		while(1){
			mid=(l+r)/2;
			ans=a*mid+b*sqrt(mid)+c*log(mid)/log(E)-w;
			if(fabs(ans)<0.00001) break;
			if(ans>0) l=mid;
			else if(ans<0) r=mid; 
		}
		printf("%.4lf",mid);
	}
	else{//Newton
		while(1){
			if(fabs(x)<=0.00001)break; 
			ans1=a*x+b*sqrt(x)+c*log(x)/log(E)-w;
			ans2=a+b/(2*sqrt(x))+c/x;
			//cout<<"x="<<x<<endl;
			//cout<<"ans1="<<ans1<<" ans2="<<ans2<<endl;
			if(_isnan(x)||_isnan(ans1)||_isnan(ans2)){
				x=lad[++rnk];
				continue;
			}
			if(fabs(ans1)<=0.0001) break;
			x-=ans1/ans2;
		}
		printf("%.4lf\n",x);
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
struct SP{
	int d;
	int pos;
	int val;
}sp[4000];
int cmp(SP x,SP y){
	if(x.d<y.d) return 1;
	else if(x.d>y.d) return 0;
	else{
		if(x.pos<y.pos) return 1;
		else return 0;
	}
}
int n,d,u,s,f[4000];
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d %d %d %d",&n,&d,&u,&s);
	for(int i=1;i<=n;i++)
		scanf("%d %d %d",&sp[i].d,&sp[i].pos,&sp[i].val);
	sort(&sp[1],&sp[1+n],cmp);
	sp[0].pos=s;
	sp[n+1].pos=s;
	for(int i=1;i<=n+1;i++){
		int max=1<<31;
		for(int j=0;j<i;j++){
			int tmp=f[j];
			if(sp[i].pos>sp[j].pos) tmp-=(sp[i].pos-sp[j].pos)*d;
			else if(sp[i].pos<sp[j].pos) tmp-=(sp[j].pos-sp[i].pos)*u;
			if(tmp>max) max=tmp;
		}
		f[i]=max+sp[i].val;
	}
	cout<<f[n+1]<<endl;
	return 0;
}

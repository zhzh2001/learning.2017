#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
int f[40][40][40];
int n,m,p[40],v[40],A[40],B[40];
double E;
int minof(int x,int y){
	if(x<y) return x;
	else return y;
}
void deal(double e,int mny,int dmd){
	//cout<<"e="<<e<<" mny="<<mny<<" dmd="<<dmd<<endl;
	int ans=0;
	for(int i=0;i<=m;i++) if(f[n][i][dmd]!=-1&&mny>=f[n][i][dmd]) ans=i;
	//cout<<"ans="<<ans<<endl;
	E+=(ans*e);
}
void dfs(int r,double e,int mny,int dmd){
	if(r==n+1){
		deal(e,mny,dmd);
		return;
	}
	else{
		double x=(double)p[r]/100;
		dfs(r+1,e*x,mny+v[r],dmd);
		dfs(r+1,e*(1-x),mny,dmd+1);
	}
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d %d",&n,&m);
		memset(f,0,sizeof(f));
		E=0;
		for(int i=1;i<=n;i++) scanf("%d %d",&v[i],&p[i]);
		for(int i=1;i<=n;i++) scanf("%d %d",&A[i],&B[i]);
		for(int i=1;i<=m;i++){
			for(int j=1;j<=i;j++){
				for(int k=0;k<=n;k++){
					if(k<B[i]){
						f[i][j][k]=-1;
					}
					else{
						if(i==j){
							if(f[i-1][j-1][k-B[i]]==-1)
								f[i][j][k]=-1;
							else f[i][j][k]=f[i-1][j-1][k-B[i]]+A[i];
						}
						else{
							if(f[i-1][j-1][k-B[i]]==-1&&f[i-1][j][k]==-1) f[i][j][k]=-1;
							else if(f[i-1][j-1][k-B[i]]==-1&&f[i-1][j][k]!=-1) f[i][j][k]=f[i-1][j][k];
							else if(f[i-1][j-1][k-B[i]]!=-1&&f[i-1][j][k]==-1) f[i][j][k]=f[i-1][j-1][k-B[i]]+A[i];
							else f[i][j][k]=minof(f[i][j][k]=f[i-1][j=1][k-B[i]]+A[i],f[i][j][k]=f[i-1][j][k]);
						}
					}
					//cout<<"i="<<i<<" j="<<j<<" k="<<k<<"    "<<f[i][j][k]<<endl; 
				}
			}
		}
		dfs(1,1.00,0,0);
		printf("%.4lf\n",E);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int N=2005;
struct jzq{int p,t,v;}a[N];
int n,U,D,S,ans,vis[N];
void dfs(int x,int v){
	ans=max(ans,v-abs(a[x].p-S)*(S<a[x].p?U:D));
	for (int i=1;i<n;i++)
		if (!vis[i]&&a[i].t>=a[x].t){
			vis[i]=1;
			dfs(i,v+a[i].v-abs(a[x].p-a[i].p)*(a[i].p<a[x].p?U:D));
			vis[i]=0;
		}
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("aksgr.out","w",stdout);
	scanf("%d%d%d%d",&n,&U,&D,&S);
	for (int i=1;i<=n;i++)
		scanf("%d%d%d",&a[i].t,&a[i].p,&a[i].v);
	a[++n]=(jzq){S,0,0};
	vis[n]=1;
	dfs(n,0);
	printf("%d\n",ans);
}

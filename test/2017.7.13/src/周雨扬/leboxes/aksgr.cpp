#include<bits/stdc++.h>
#define pa pair<int,double>
using namespace std;
struct lzh{int pos,rmb;}a[35];
struct jzq{int dia,rmb;}b[35];
int T,n,m,f[35][35];
double ans;
int main(){
	freopen("leboxes.in","r",stdin);
	freopen("aksgr.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d",&n,&m); ans=0;
		for (int i=1;i<=n;i++)
			scanf("%d%d",&a[i].rmb,&a[i].pos);
		for (int i=1;i<=m;i++)
			scanf("%d%d",&b[i].rmb,&b[i].dia);
		memset(f,90,sizeof(f));
		for (int i=0;i<(1<<m);i++){
			int rmb=0,dia=0,cnt=0;
			for (int j=1;j<=m;j++)
				if (i&(1<<(j-1))) rmb+=b[j].rmb,dia+=b[j].dia,cnt++;
			if (dia<=n) f[cnt][dia]=min(f[cnt][dia],rmb);
		}
		for (int i=0;i<=m;i++)
			for (int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][j-1]);
		for (int i=0;i<(1<<n);i++){
			int rmb=0,dia=0; double pos=1;
			for (int j=1;j<=n;j++)
				if (i&(1<<(j-1))) rmb+=a[j].rmb,pos*=a[j].pos/100.0;
				else dia++,pos*=(100.0-a[j].pos)/100.0;
			for (int j=m;j>=1;j--)
				if (f[j][dia]<=rmb){
					ans+=j*pos;
					break;
				}
		}
		printf("%.4lf\n",ans);
	}
}

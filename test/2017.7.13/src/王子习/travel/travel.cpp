#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e5+7,INF=1<<27;
int n,Dk,Uk,S;
int f[N];
struct node{
	int day,pos,val;
}a[N];
inline bool operator<(const node&x,const node&y){
	if (x.day==y.day) return x.pos<y.pos;
	else return x.day<y.day;
}

inline int check(int u,int v){
	if (u>v) return (u-v)*Uk;
	else return (v-u)*Dk;
}

int main(){
	// say hello

	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);

	n=read(),Dk=read(),Uk=read(),S=read();
	For(i,2,n+1){
		a[i].day=read();
		a[i].pos=read();
		a[i].val=read();
	}
	sort(a+2,a+2+n);
	a[1].pos=S,a[1].val=0;
	a[n+2].pos=S;
	For(i,2,n+2) f[i]=-INF;
	For(i,2,n+2){ 
		For(j,1,i-1){
			f[i]=max(f[i],-check(a[j].pos,a[i].pos)+f[j]+a[i].val);
		}	
	//	printf("%d\n",f[i]);
	}
	printf("%d\n",f[n+2]);

	// say goodbye
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const double eps=1e-8;
double a,b,c,w,l,r,mid,ans,ss;
int i,j,k;

int main(){
	// say hello

    freopen("function.in","r",stdin);
    freopen("function.out","w",stdout);
    scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
    l=0;r=1000000000;
    if(fabs(c-0)>eps) l=0.0001;
    while(l<=r){
    	mid=(l+r)/2;
     	if(fabs(a-0)>eps)ss=mid*a;
     	if(fabs(b-0)>eps)ss=ss+sqrt(mid)*b;
     	if(fabs(c-0)>eps)ss=ss+c*log(mid);
     	ss=ss-w;
     	if(fabs(ss-0)<0.0001){
		   ans=mid;break;
	      }
     	  else if(ss-0>eps) r=mid-0.00001;
     	     else l=mid+0.00001;
     }
    printf("%.5lf\n",ans);

	// say goodbye
}


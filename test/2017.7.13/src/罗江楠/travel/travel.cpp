// 信仰
#include <bits/stdc++.h>
#define N 2010
#define inf 1061109567
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct orzzyy{
	int day, pos, val;
	friend bool operator < (const orzzyy &a, const orzzyy &b){
		return a.day == b.day ? a.pos < b.pos : a.day < b.day;
	}
}ms[N];
int dayhash[N], poshash[N], cnt1, cnt2;
int f[N][N], n, d, u, s;
int walk(int fm, int to){
	if(fm < to) return (poshash[to]-poshash[fm])*d;
	else return (poshash[fm]-poshash[to])*u;
}
int main(int argc, char const *argv[]){
	File("travel");
	n = read(); d = read();
	u = read(); s = read();
	for(int i = 1; i <= n; i++){
		ms[i].day = read();
		ms[i].pos = read();
		ms[i].val = read();
		dayhash[++cnt1] = ms[i].day;
		poshash[++cnt2] = ms[i].pos;
	} poshash[++cnt2] = s;
	sort(dayhash+1, dayhash+cnt1+1);
	sort(poshash+1, poshash+cnt2+1);
	for(int i = 1; i <= n; i++){
		ms[i].day = lower_bound(dayhash+1, dayhash+cnt1+1, ms[i].day)-dayhash;
		ms[i].pos = lower_bound(poshash+1, poshash+cnt2+1, ms[i].pos)-poshash;
	}
	// printf("%d\n", f[1][1]);
	int spos = lower_bound(poshash+1, poshash+cnt2+1, s)-poshash;
	for(int i = 1; i <= cnt2; i++) f[0][i] = -walk(spos, i);
	// for(int p = 1; p <= cnt2; p++)	printf("%d ", f[0][p]); puts("");
	for(int i = 1, j = 1; i <= cnt1; i = j+1){
		while(ms[j+1].day == ms[i].day) j++;
		for(int k = 1; k <= cnt2; k++){
			f[i][k] = f[i-1][k];
			int t = i;
			while(t <= j && ms[t].pos < k) t++;
			for(int fuck = i, sum = ms[i].val; fuck < t; sum += ms[++fuck].val)
				f[i][ms[fuck].pos] = max(f[i][ms[fuck].pos], f[i][k]-walk(k, ms[fuck].pos)+sum);
			for(int fuck = t, sum = ms[t].val; fuck <= j; sum += ms[++fuck].val)
				f[i][ms[fuck].pos] = max(f[i][ms[fuck].pos], f[i][k]-walk(k, ms[fuck].pos)+sum);
		}
		// for(int p = 1; p <= cnt2; p++)	printf("%d ", f[i][p]); puts("");
	}
	int ans = 0;
	for(int i = 1; i <= cnt2; i++)
		ans = max(ans, f[cnt1][i]-walk(i, spos));
	printf("%d\n", ans);
	return 0;
}
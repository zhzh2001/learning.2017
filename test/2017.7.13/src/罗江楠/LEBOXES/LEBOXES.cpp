// orz cfb	dalao
// orz clc	dalao
// orz clj	dalao
// orz cqz	dalao
// orz cym	dalao
// orz dhn	dalao
// orz dyt	dalao
// orz dyx	dalao
// orz fb	dalao
// orz fjq	dalao
// orz fks	dalao
// orz gn	dalao
// orz hc	dalao
// orz hcz	dalao
// orz hjh	dalao
// orz htr	dalao
// orz jzq	dalao
// orz lbc	dalao
// orz lfyc	dalao
// orz ljc	dalao
// orz ljc	dalao
// orz ljz	dalao
// orz lqz	dalao
// orz lt	dalao
// orz lzh	dalao
// orz lzq	dalao
// orz mdnd	dalao
// orz njh	dalao
// orz sgr	dalao
// orz ssy	dalao
// orz szb	dalao
// orz tzn	dalao
// orz wcr	dalao
// orz wjy	dalao
// orz wl	dalao
// orz wxl	dalao
// orz wzq	dalao
// orz wzt	dalao
// orz xq	dalao
// orz xza	dalao
// orz ykl	dalao
// orz zch	dalao
// orz zcy	dalao
// orz zf	dalao
// orz zly	dalao
// orz zxh	dalao
// orz zxh	dalao
// orz zyc	dalao
// orz zyy	dalao
// orz zz	dalao
// orz zzd	dalao
// orz zzh	dalao
// orz zzt	dalao

//  _____ _____ _____
// /  _  \ __  \     |
// | / \ | | \ |___  |
// | | | | |_/ |  /  /
// | | | |   __/ /  /
// | | | | \ \  /  /_
// | \_/ | |\ \|     |
// \_____/_| \_\_____|

// 相信奇迹，O(2^(n+m))过2^60
// 出题人造的数据，是我此生不变的信仰

#include <bits/stdc++.h>
#define N 2010
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int n, m, ans, v[N], d[N], V[N], D[N];
double res;
inline void maxs(int &a, int b){if(b > a) a = b;}
void dfss(int pos, int mon, int dem, int num){
	if(pos == m+1) return maxs(ans, num);
	dfss(pos+1, mon, dem, num);
	if(V[pos] <= mon && D[pos] <= dem)
		dfss(pos+1, mon-V[pos], dem-D[pos], num+1);
}
void dfs(double pb, int pos, int mon, int dem){
	if(pos == n+1){
		ans = 0;
		dfss(1, mon, dem, 0);
		res += pb*ans;
		return;
	}
	dfs(pb*d[pos]/100, pos+1, mon+v[pos], dem);
	dfs(pb*(100-d[pos])/100, pos+1, mon, dem+1);
}
int main(int argc, char const *argv[]){
	File("LEBOXES");
	int T = read();
	while(T--){
		n = read(); m = read(); res = 0;
		for(int i = 1; i <= n; i++)
			v[i] = read(), d[i] = read();
		for(int i = 1; i <= m; i++)
			V[i] = read(), D[i] = read();
		dfs(1, 1, 0, 0);
		printf("%.4lf\n", res);
	}
	return 0;
}
//  OOO  RRRR  ZZZZZ
// O   O R   R     Z
// O   O R   R    Z
// O   O RRRR    Z
// O   O R R    Z
// O   O R  R  Z
//  OOO  R   R ZZZZZ
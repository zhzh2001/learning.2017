#include <bits/stdc++.h>
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
double a, b, c, w, ans;
// if the function "exp()" in <math.h> is not ln(), please kill me baby.
double calc(double x){
	return a*x+b*sqrt(x)+c*exp(x);
}
int main(int argc, char const *argv[]){
	File("function");
	cin >> a >> b >> c >> w;
	double l = 0, r = 1e10;
	while(r-l >= 1e-9){
		double mid = (l+r)/2;
		if(calc(mid) > w) ans = r = mid;
		else l = mid;
	}
	printf("%.9lf\n", ans);
	return 0;
}

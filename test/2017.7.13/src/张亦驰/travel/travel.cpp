#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	int x,y,z;
}a[2010];
int n,d,u,s;
int ans;
int f[2010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool cmp(D a,D b)
{
	return a.x<b.x;
}
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read();d=read();u=read();s=read();
	For(i,1,n)
	{
		a[i].x=read();a[i].y=read();a[i].z=read();
	}
	sort(a+1,a+n+1,cmp);
	if(s>a[1].y)f[1]=a[1].z-(s-a[1].y)*u;
			else f[1]=a[1].z-(a[1].y-s)*d;
	For(i,2,n)
	{
		if(s>a[i].y)f[i]=a[i].z-(s-a[i].y)*u;
			else f[i]=a[i].z-(a[i].y-s)*d;
		For(j,1,i-1)
		{
			if(a[i].y>a[j].y)f[i]=max(f[i],f[j]-(a[i].y-a[j].y)*d+a[i].z);
						else f[i]=max(f[i],f[j]-(a[j].y-a[i].y)*u+a[i].z);
		}
	}
	ans=0;
	For(i,1,n)
	{
		if(a[i].y>s)ans=max(ans,f[i]-(a[i].y-s)*u);
				else ans=max(ans,f[i]-(s-a[i].y)*d);
	}
	cout<<ans<<endl;
	return 0;
}


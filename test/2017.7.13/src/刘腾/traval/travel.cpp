#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int MX = 2005;
const int inf = 0x3f3f3f3f;
int n,d,u,s,pos[MX],day[MX],v[MX];
long long dp[MX];
struct node{
    int t,p,id;
}fair[MX];

bool comt(node x,node y)
{
   return x.t<y.t;
}

bool comp(node x,node y)
{
   return x.p<y.p;
}

int main()
{
   freopen("travel.in","r",stdin);
   freopen("travel.out","w",stdout);
   scanf("%d%d%d%d",&n,&d,&u,&s);
   for(int i=1;i<=n;i++)
    scanf("%d%d%d",&fair[i].t,&fair[i].p,&v[i]),fair[i].id=i;
   fair[n+1].t=inf,fair[n+1].p=s,fair[n+1].id=n+1;
   sort(fair+1,fair+2+n,comt);
   for(int i=1;i<=n+1;++i)
    day[fair[i].id]=i;
   sort(fair+1,fair+2+n,comp);
   for(int i=1;i<=n+1;i++)
    pos[i]=fair[i].p;
   for(int i=1;i<=n+1;i++)
    fair[i].t=day[i],fair[fair[i].id].p=i,day[fair[i].t]=i;
   for(int i=1;i<=n+1;++i)
    dp[i]=-0x3f3f3f3f;
   dp[fair[n+1].p]=0;
   
   for(int i=1;i<=n;++i) 
    for(int k=1;k<=n+1;k++)
    {
       int j=fair[day[i]].p;
	   if(k==j)
    	continue;
       if(k<j)
	    dp[j]=max(dp[j],dp[k]-(pos[j]-pos[k])*d+v[day[i]]);
	   else
	    dp[j]=max(dp[j],dp[k]-(pos[k]-pos[j])*u+v[day[i]]);
	   
	}
   long long ans=-5;
   for(int i=1;i<=n+1;++i)
   {
      if(i>fair[n+1].p)
       ans=max(ans,dp[i]-(pos[i]-pos[fair[n+1].p])*u);
      else
       ans=max(ans,dp[i]-(pos[fair[n+1].p]-pos[i])*d);
   }
   printf("%lld\n",ans);
   return 0;
}

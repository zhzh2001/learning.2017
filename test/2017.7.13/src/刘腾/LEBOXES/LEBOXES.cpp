#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>

using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}
const int inf=2147483647;
typedef unsigned int uint;
typedef long long LL;
const uint uinf=4294967295u;

const int maxn=33;
const int maxv=int(1e5);
const int maxk=33;
int TT,n,m;
struct BOX
{
	int v,p;
	inline void input()
	{
		read(v);read(p);
	}
}box[maxn];

struct GOOD
{
	int a,b;
	inline void input()
	{
		read(a),read(b);
	}
}good[maxn];
void input()
{
	read(n);read(m);
	for(int i=1;i<=n;++i)
	{
		box[i].input();
	}
	for(int i=1;i<=m;++i)
	{
		good[i].input();
	}
}
double calcp(uint x)
{
	double pp=1.0;
	for(uint i=1;i<=n;++i)
	{
		if(x&(1<<(i-1)))
		{
			pp*=double(box[i].p)/100.0;
		}
		else pp*=(100.0-double(box[i].p))/100.0;
	}
	return pp;
}
int calcv(uint x)
{
	int vv=0;
	for(uint i=1;i<=n;++i)
	{
		if(x&(1<<(i-1)))
		{
			vv+=box[i].v;
		}
	}
	return vv;
}
int calck(uint x)
{
	int kk=0;
	for(uint i=1;i<=n;++i)
	{
		if(x&(1<<(i-1)))
		{
			
		}
		else ++kk;
	}
	return kk;
}
int f[maxv][maxk];
int calcnum(int vv,int kk)//dp 01bag+
{
	for(int j=0;j<=vv;++j)
	{
		for(int k=0;k<=kk;++k)
		{
			f[j][k]=0;
		}
	}
	for(int pos=1;pos<=m;++pos)
	{
		for(int j=vv;j>=good[pos].a;--j)
		{
			for(int k=kk;k>=good[pos].b;--k)
			{
				if(f[j-good[pos].a][k-good[pos].b]+1>f[j][k])
					f[j][k]=f[j-good[pos].a][k-good[pos].b]+1;
			}
		}
	}
	return f[vv][kk];
}
double ans;
void solve()
{
	ans=0.0;
	for(uint i=0;i<=(1<<(n))-1;++i)
	{
		double pi=calcp(i);
		int vi=calcv(i);
		int ki=calck(i);
		int num=calcnum(vi,ki);
		ans+=double(num)*pi;
	}
}
void count()
{
	ans=0;
	double vi=0.0,ki=0.0,num=0.0;
	for(int i=1;i<=n;++i)
	{
		double tmpp=double(box[i].p)/100.0;
		double rep=double(100-box[i].p)/100.0;
		vi+=double(box[i].v)*tmpp;
		ki+=rep;
	}
	num=calcnum(int(vi),int(ki));
	ans=num;
}
int main()
{
	
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	read(TT);
	for(int e=1;e<=TT;++e)
	{
		input();
		
		if(n<=27)
		{
			solve();
		}
		else
		{
			count();
		}
		printf("%.4lf\n",ans);
	}
	return 0;
}

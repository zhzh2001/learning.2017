#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n,m,r,w;
int v[35];
int a[35],b[35];
int dp[35][100005];
double ans;
int ci[35][100005];
double p[35],f[35][100005];
double g[35][100005];

int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int ti,n,m,po,i,j,k,l,o;
	ti=read();
	while(ti--)
	{
		ans=0;
		n=read(),m=read();
		for(i=1;i<=n;i++)
		{
			v[i]=read();
			po=read();
			p[i]=double(po)/100.0;
		}
		for(i=1;i<=m;i++)
		{
			a[i]=read();
			b[i]=read();
		}
		for(j=0;j<=n;j++)
			for(k=0;k<=100000;k++)
				f[j][k]=0;
		f[0][0]=1;
		for(i=1;i<=n;i++)
		{
			for(j=i;j>=0;j--)
			{
				for(k=100000;k>=0;k--)
				{	
					if(k>=v[i])
					{
						f[j][k]=f[j][k-v[i]]*p[i];
						ci[j][k]=i;
					}
					if(j>0)
					{
						f[j][k]+=f[j-1][k]*(1-p[i]);
						ci[j][k]=i;
					}
				}	
			}
			for(j=0;j<=i;j++)	
				for(k=0;k<=100000;k++)
					if(ci[j][k]!=i)
						f[j][k]=0;
		}
		for(i=0;i<=n;i++)
			for(j=0;j<=100000;j++)
				g[i][j]=f[i][j];
		for(i=0;i<=n;i++)
			for(j=0;j<=100000;j++)
				if(g[i][j]!=0)
				{
					r=i;
					w=j;
					for(l=0;l<=r;l++)
						for(o=0;o<=w;o++)	
							dp[l][o]=0;
					for(k=1;k<=m;k++)
						for(l=r;l>=b[k];l--)
							for(o=w;o>=a[k];o--)
								dp[l][o]=max(dp[l-b[k]][o-a[k]]+1,dp[l][o]);
					ans=ans+g[i][j]*dp[r][w];
				}	
		printf("%.4lf\n",ans);
	}
	return 0;
}


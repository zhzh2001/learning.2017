#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <math.h>
using namespace std;
double a,b,c,w;
double ans;
double ei=2.7182818284590452353602874713527;

bool check(double x)
{
	double le,ri;
	le=a*x+b*sqrt(x);
	ri=-(log(x)/log(ei))*c+w;
	if(le-ri>=1e-6)
		return 0;
	else
		return 1;
}

int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf %lf %lf %lf",&a,&b,&c,&w);
	double li=0,ri=1000000000.0,mid;
	while(fabs(ri-li)>1e-6)
	{
		mid=(li+ri)*0.5;
		if(check(mid))
			li=mid,ans=mid;
		else
			ri=mid;
	}
	printf("%lf\n",ans);
	return 0;
}


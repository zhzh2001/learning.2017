//离散化一下可以压到O(n^2) 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct meeting
{
	int t,p,v;
}m[2005];

int n,d,u,s;
int f[2][500005];
int g[2][500005];

bool cmp(meeting a,meeting b)
{
	return a.t==b.t?a.v>b.v:a.t<b.t;
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	int i,j,mp=0;
	n=read(),d=read(),u=read(),s=read();
	for(i=1;i<=n;i++)
	{
		m[i].t=read();
		m[i].p=read();
		m[i].v=read();
		mp=max(mp,m[i].p);
	}
	sort(m+1,m+n+1,cmp);
	f[0][s]=0;
	g[0][s]=1;
	int to,now;
	for(i=1;i<=n;i++)
	{
		to=(i-1)%2;
		now=i&1;
		for(j=0;j<=mp;j++)
		{
			f[now][j]=f[to][j];
			g[now][j]=g[to][j];
		}
		for(j=0;j<=mp;j++)
			if(g[to][j]==1)
			{
				if(j>m[i].p)
				{
					f[now][m[i].p]=max(f[now][m[i].p],f[to][j]+m[i].v-(j-m[i].p)*u);
					g[now][m[i].p]=1;
				}
				else if(j<m[i].p)
				{
					f[now][m[i].p]=max(f[to][j]+m[i].v-(m[i].p-j)*d,f[now][m[i].p]);
					g[now][m[i].p]=1;
				}
			}
	}
	int ans=0;
	for(j=0;j<=mp;j++)	
	{
		if(j>s)
			f[n&1][j]-=(j-s)*u;
		else if(j<s)
			f[n&1][j]-=(s-j)*d;
		ans=max(ans,f[n&1][j]);
	}
	printf("%d\n",ans);
	return 0;
}


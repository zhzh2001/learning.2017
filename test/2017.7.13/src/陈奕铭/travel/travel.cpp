#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
typedef long long ll;
const ll oo=1e10;
int n;
ll u,d,s,dp[2005],cqz[2005];
struct node{
	int day;
	ll pos,val;
}A[2005];
ll max(ll a,ll b){
	if (a>b) return a; else return b;
}
bool cmp(node a,node b){
	if (a.day==b.day)
		return a.pos<b.pos;
	return a.day<b.day;
}
int find(ll x,int L,int R){
	int mid,ans=L-1;
	while (L<=R){
		mid=(L+R)>>1;
		if (A[mid].pos<x) ans=mid,L=mid+1;
		else R=mid-1;
	}
	return ans;
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%lld%lld%lld",&n,&u,&d,&s);
	for (int i=1;i<=n;i++)
		scanf("%d%lld%lld",&A[i].day,&A[i].pos,&A[i].val);
	std::sort(A+1,A+n+1,cmp); A[0].pos=s; A[n+1].pos=1e8;
	memset(dp,192,sizeof(dp)); dp[0]=0;
	int st=1,en=1,mid;
	ll MAXL,MAXR;
	for (int i=2;i<=n+1;i++)
	if (A[i].day==A[i-1].day) en=i;
	else{
		for (int j=0;j<st;j++)
		if (dp[j]>-oo){
			mid=find(A[j].pos,st,en);
			MAXL=MAXR=0;
			if (mid>=st){
				cqz[mid]=dp[j]+A[mid].val-(A[j].pos-A[mid].pos)*u;
				MAXL=max(MAXL,cqz[mid]-(A[j].pos-A[mid].pos)*d);
				for (int k=mid-1;k>=st;k--){
					cqz[k]=cqz[k+1]+A[k].val-(A[k+1].pos-A[k].pos)*u;
					MAXL=max(MAXL,cqz[k]-(A[j].pos-A[k].pos)*d);
				}
			}
			if (mid<en){
				cqz[mid+1]=dp[j]+A[mid+1].val-(A[mid+1].pos-A[j].pos)*d;
				MAXR=max(MAXR,cqz[mid+1]-(A[mid+1].pos-A[j].pos)*u);
				for (int k=mid+2;k<=en;k++){
					cqz[k]=cqz[k-1]+A[k].val-(A[k].pos-A[k-1].pos)*d;
					MAXR=max(MAXR,cqz[k]-(A[k].pos-A[j].pos)*u);
				}
			}
			if (mid>=st)
				for (int k=mid;k>=st;k--) cqz[k]+=MAXR;
			if (mid<en)
				for (int k=mid+1;k<=en;k++) cqz[k]+=MAXL;
			for (int k=st;k<=en;k++) dp[k]=max(dp[k],cqz[k]);
		}
		st=en=i;
	}
	ll ans=0;
	for (int i=1;i<=n;i++)
	if (dp[i]>-oo)
		if (A[i].pos>s)
			ans=max(ans,dp[i]-(A[i].pos-s)*u);
		else
			ans=max(ans,dp[i]-(s-A[i].pos)*d);
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

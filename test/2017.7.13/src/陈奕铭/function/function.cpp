#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
double A,B,C,W;
int check(double x){
	double res=A*x+B*sqrt(x)+C*log(x)-W;
	if (fabs(res)<1e-3) return 0;
	if (res>0) return 1;
	else return -1;
}
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&A,&B,&C,&W);
	double L=0,R=1e9,mid,ans;
	int res;
	while (R-L>1e-4){
		mid=(L+R)/2;
		res=check(mid);
		if (res==0){
			ans=mid;
			break;
		}
		if (res==1) R=mid; else L=mid;
	}
	printf("%lf",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

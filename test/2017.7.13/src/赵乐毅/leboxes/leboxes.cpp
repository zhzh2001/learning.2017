#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
double ans;
int n,m,f[40][40][40],a[40],b[40],p[40],v[40];
void dfs(int t,int money,int diamoud,double gv)
{
	if (t==n+1)
	{
		int sum=0;
		money++;
		memset(f,0,sizeof f);
		for (int j=0;j<=n;j++)
			for (int i=0;i<=diamoud;i++)
				f[j][0][i]=money;
		for (int i=1;i<=n;i++)
			for (int j=0;j<=i;j++)
				for (int k=b[i];k<=diamoud;k++)
					f[i][j][k]=max(f[i][j][k],f[i-1][j-1][k-b[i]]-a[i]);
		for (int i=1;i<=n;i++)
			for (int j=0;j<=n;j++)
				for (int k=0;k<=diamoud;k++)
					if (f[i][j][k]>0)sum=max(sum,j);
		ans+=(double)sum*gv;
		return;
	}
	dfs(t+1,money+v[t],diamoud,gv*0.01*(double)p[t]);
	dfs(t+1,money,diamoud+1,gv*0.01*(double)(100-p[t]));
}
int main()
{
	freopen("leboxes.in","r",stdin);
	freopen("leboxes.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		ans=0;
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++)
			scanf("%d%d",&v[i],&p[i]);
		for (int i=1;i<=m;i++)
			scanf("%d%d",&a[i],&b[i]);
		dfs(1,0,0,1);
		printf("%.4lf\n",ans);
	}
}

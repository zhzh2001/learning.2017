#include <bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
typedef double db;
int dp[33][33][33];
int n,m;
int a[33],b[33],v[33];
db p[33];
void relax(int &x,int y){
	if (x>y)
		x=y;
}
void pre(){
	memset(dp,0x3f,sizeof(dp));
	memset(dp[0][0],0,sizeof(dp[0][0]));
	for(int i=1;i<=m;i++){
		for(int j=0;j<=n;j++)
			dp[i][0][j]=0;
		for(int j=1;j<=i;j++)
			for(int k=0;k<=n;k++){
				dp[i][j][k]=dp[i-1][j][k];
				if (k>=b[i])
					relax(dp[i][j][k],dp[i-1][j-1][k-b[i]]+a[i]);
			}
	}
}
void solve_bf(){
	db ans=0;
	for(int i=0;i<(1<<n);i++){
		db prod=1;
		int x=0,y=0,cnt=0;
		for(int j=1;j<=n;j++)
			if (i&(1<<(j-1)))
				prod*=p[j],x+=v[j];
			else prod*=(1-p[j]),y++;
		//printf("%.2f %d %d\n",prod,x,y);
		for(int j=1;j<=m;j++)
			if (dp[m][j][y]<=x)
				cnt=j;
		ans+=prod*cnt;
	}
	printf("%.4f\n",ans);
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d",&n,&m);
		for(int i=1;i<=n;i++){
			int x;
			scanf("%d%d",v+i,&x);
			p[i]=x/100.0;
		}
		for(int i=1;i<=m;i++)
			scanf("%d%d",a+i,b+i);
		pre();
		solve_bf();
	}
}
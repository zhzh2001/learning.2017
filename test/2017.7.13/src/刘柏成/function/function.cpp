#include <cstdio>
#include <cmath>
using namespace std;
typedef long double db;
const db eps=1e-5;
db a,b,c,w;
db f(db x){
	return a*x+b*sqrt(x)+c*log(x)-w;
} 
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	double wa,wb,wc,ww;
	scanf("%lf%lf%lf%lf",&wa,&wb,&wc,&ww);
	a=wa,b=wb,c=wc,w=ww;
	//scanf("%Lf%Lf%Lf%Lf",&a,&b,&c,&w);
	db l=0,r=1e9,mid;
	while(1){
		mid=(l+r)/2;
		db tmp=f(mid);
		if (abs(tmp)<eps)
			break;
		if (tmp<0)
			l=mid;
		else r=mid;
	}
	printf("%.8f",(double)mid);
}
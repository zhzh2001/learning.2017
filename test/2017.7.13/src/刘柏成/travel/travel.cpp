#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll INF=0x3f3f3f3f3f3f3f3fLL;
int n,d,u,s;
int dist(int x,int y){
	return x>y?(x-y)*u:(y-x)*d;
}
ll f[2002][2002],g[2002][2002];
struct market{
	int day,pos,val;
	bool operator <(const market& rhs)const{
		return day<rhs.day || (day==rhs.day && pos<rhs.pos);
	}
}a[2002];
int st[2002],cnt[2002];
int sum[2002];
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d%d%d",&n,&d,&u,&s);
	for(int i=1;i<=n;i++)
		scanf("%d%d%d",&a[i].day,&a[i].pos,&a[i].val);
	sort(a+1,a+n+1);
	int cur=0;
	for(int i=1;i<=n;i++){
		if (i==1 || a[i].day!=a[i-1].day)
			st[++cur]=i;
		cnt[cur]++;
		sum[i]=sum[i-1]+a[i].val;
	}
	st[cur+1]=n+1;
	for(int i=cur;i;i--){
		for(int j=1;j<=cnt[i];j++){
			int p=a[st[i]+j-1].pos;
			g[i][j]=-dist(p,s);
			for(int k=i+1;k<=cur;k++)
				for(int o=1;o<=cnt[k];o++)            //O(n^2)?
					g[i][j]=max(g[i][j],f[k][o]-dist(p,a[st[k]+o-1].pos));
		}
		for(int j=1;j<=cnt[i];j++){
			f[i][j]=-INF;
			for(int k=1;k<=cnt[i];k++){
				int p1=st[i]+j-1,p2=st[i]+k-1;
				int add=p2>p1?sum[p2]-sum[p1-1]:sum[p1]-sum[p2-1];
				f[i][j]=max(f[i][j],g[i][k]+add-dist(a[p1].pos,a[p2].pos));
			}
		}
	}
	ll ans=0;
	for(int i=1;i<=cur;i++)
		for(int j=1;j<=cnt[i];j++)
			ans=max(ans,f[i][j]-dist(s,a[st[i]+j-1].pos));
	printf("%lld",ans);
}
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>

using namespace std;
const double eps=1e-9;
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}

double a,b,c,w;
inline double f(double x)
{
	return a*x+b*sqrt(x)+c*log(x)-w;
}
double ans;
void BS()
{
	double l=0.0,r=1e9,mid=(l+r)/2.0;
	while(l+eps<r)
	{
		mid=(l+r)/2.0;
		double tans=f(mid);
		if(fabs(tans)<eps)
		{
			ans=mid;
			return;
		}
		else if(tans>eps)
		{
			r=mid;
		}
		else l=mid;
		if(!(l+eps<r))ans=mid;
	}
}
int main()
{
	
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
	if(fabs(w)<eps&&fabs(f(0.0))<eps)
		ans=0.0;
	else BS();
	printf("%lf\n",ans);
	return 0;
}

#include<algorithm>
#include<cstdio>
using namespace std;
const int N=2010;
struct S{
  int t,p,v,f;
  inline int operator<(const S&a)
    const {return t<a.t||t==a.t&&p<a.p;}
}t[N];
int ans,i,j,n,d,u,s,td,tp,tv;
inline int I(){
  int x=0;char c=getchar();
  while(c<'0'||c>'9')c=getchar();
  while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
  return x;
}

int main()
{
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  n=I(),d=I(),u=I(),s=I();
  for(i=1;i<=n;i++){
    td=I(),tp=I(),tv=I();
    t[i]={td,tp,tv,0};
    if(tp<s)t[i].f=(tp-s)*u+tv;
    else t[i].f=(s-tp)*d+tv;
  }
  std::sort(t+1,t+1+n);
  for(i=2;i<=n;i++){
    for(j=1;j<i;j++)
      if(t[i].p<t[j].p)
        t[i].f=max(t[i].f,t[j].f+u*(t[i].p-t[j].p)+t[i].v);
      else
        t[i].f=max(t[i].f,t[j].f+d*(t[j].p-t[i].p)+t[i].v);
    if(t[i].p<s)ans=max(ans,t[i].f+(t[i].p-s)*d);
    else ans=max(ans,t[i].f+(s-t[i].p)*u);
  }printf("%d\n",ans);
}

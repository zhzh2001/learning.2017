#include<algorithm>
#include<cstdio>
const int N=2010;
using namespace std;
struct argt{
  int t,p,v;
  inline int operator<(const argt&a)
    const{return t<a.t||t==a.t&&p<a.p;}
}schd[N];
int n,d,u,s,w,pt,t,i,ft,tmp,rt,lp,psg,ans;
inline int I(){
  int x=0;char c=getchar();
  while(c<'0'||c>'9')c=getchar();
  while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
  return x;
}
int main(){
  freopen("travel.in","r",stdin);
  freopen("travel.out","w",stdout);
  n=I(),d=I(),u=I(),s=I();
  psg=d+u;
  for(i=1;i<=n;i++)
    d=I(),u=I(),w=I(),schd[i]={d,u,w};
  sort(schd+1,schd+1+n);
  pt=1;
  while(pt<=n){
    d=schd[pt].t;
    t=pt;ft=0;
    while(schd[t].t==d&&t<=n){
      if(schd[t].p<s)ft=t;
      t++;
    }t--;
    if(ft){
      lp=schd[ft].p;
      rt=(lp-s)*psg+schd[ft].v;
      tmp=max(0,rt);
      for(i=ft-1;i>=pt;i--){
        rt+=(schd[i].p-lp)*psg+schd[i].v;
        tmp=max(tmp,rt);
        lp=schd[i].p;
   	  }ans+=tmp;
	}else ft=pt-1;
    if(ft+1<=t){
      lp=schd[ft+1].p;
      rt=(s-lp)*psg+schd[ft+1].v;
      tmp=max(0,rt);
      for(i=ft+2;i<=t;i++){
        rt+=(lp-schd[i].p)*psg+schd[i].v;
        tmp=max(tmp,rt);
        lp=schd[i].p;
      }ans+=tmp;
    }
	pt=t+1;
  }printf("%d\n",ans);
}

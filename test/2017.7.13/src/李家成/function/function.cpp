#include<cstdio>
#include<cmath>
const double eps=1e-3,q=1e-7,e=1e-8,ee=2.718281828;
double a,b,c,w,mid,l,r,ans;
inline double ln(double x){
	return log(x)/log(ee);
}
int cnt;
int main()
{
  freopen("function.in","r",stdin);
  freopen("function.out","w",stdout);
  scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
  mid=0.0;
  while(mid<=1.0)
    if(fabs(a*mid+b*sqrt(mid)+c*ln(mid)-w)<eps){
      printf("%.7lf\n",mid);return 0;
    }else mid=mid+q;
  l=1.0;
  r=1e9;
  cnt=0;
  while(l<r){cnt++;
    mid=(l+r)/2;
    if(fabs(a*mid+b*sqrt(mid)+c*ln(mid)-w)>=eps){
      if(a*mid+b*sqrt(mid)+c*ln(mid)>w)r=mid-e;
      else l=mid+e;
    }else{printf("%.8lf\n",mid);return 0;}
  }
}

#include<algorithm>
#include<cstdio>
const int N=5050;;
struct S{
  int c,d;
  inline int operator<(const S&a)
    const{return d<a.d||d==a.d&&c<a.c;}
}o[50];
double ans;
int t,i,n,m,v[N],pc[N];

inline int I(){
  int x=0;char c=getchar();
  while(c<'0'||c>'9')c=getchar();
  while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
  return x;
}

inline void calc(double p,int c,int d){
  int cnt=0,i=1;
  while(i<=m){
    if(c>=o[i].c&&d>=o[i].d)
      cnt++,c-=o[i].c,d-=o[i].d;
    i++;
    if(d<o[i].d&&c<o[i].c)break;
  }ans+=p*(double)cnt;
}
inline void w(int sn,double p,int c,int d){
  if(sn!=n){
    if(!pc[sn])w(sn+1,p,c,d+1);
    else if(pc[sn]==100)w(sn+1,p,c+v[sn],d);
    else w(sn+1,p*pc[sn]/100.0,c+v[sn],d),
         w(sn+1,p*(100-pc[sn])/100.0,c,d+1);
  }else{
    if(!pc[sn])calc(p,c,d+1);
    else if(pc[sn]==100)calc(p,c+v[sn],d);
    else calc(p*pc[sn]/100.0,c+v[sn],d),
         calc(p*(100-pc[sn])/100.0,c,d+1);
  }
}

int main(){
  freopen("leboxes.in","r",stdin);
  freopen("leboxes.out","w",stdout);
  t=I();
  while(t--){
    n=I(),m=I();
    ans=0.0;
    for(i=1;i<=n;i++)
      v[i]=I(),pc[i]=I();
    for(i=1;i<=m;i++)
      o[i].c=I(),o[i].d=I();
    std::sort(o+1,o+1+n);
    w(1,100.0,0,0);
    printf("%.4lf\n",ans/100.0);
  }
}

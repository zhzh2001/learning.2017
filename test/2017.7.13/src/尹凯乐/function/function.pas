var mid,eps,l,r,w,a,b,c:double;
function get(x:double):double;
begin
  exit(a*x+b*sqrt(x)+c*ln(x)-w);
end;
begin
  assign(input,'function.in'); assign(output,'function.out');
  reset(input); rewrite(output);
  readln(a,b,c,w);
  eps:=0.0001; l:=0; r:=1000000000;
  while r-l>eps do
    begin
    mid:=(l+r)/2;
    if get(mid)>0 then r:=mid else l:=mid;
    end;
  writeln(l:0:3);
  close(input);
  close(output);
end.

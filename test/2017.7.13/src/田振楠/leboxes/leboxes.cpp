#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int n,f[35][35][35],T,m,a[35],b[35],p[35],v[35];
double ans;
void dfs(int t,int x,int y,double pp)
{
	if (t==n+1)
	{
		int sum=0;
		x++;
		memset(f,0,sizeof f);
		for (int j=0;j<=n;j++)
		for (int i=0;i<=y;i++)
		f[j][0][i]=x;
		for (int i=1;i<=n;i++)
		for (int j=0;j<=i;j++)
		for (int k=b[i];k<=y;k++)
		f[i][j][k]=max(f[i][j][k],f[i-1][j-1][k-b[i]]-a[i]);
		for (int i=1;i<=n;i++)
		for (int j=0;j<=n;j++)
		for (int k=0;k<=y;k++)
		if (f[i][j][k]>0) sum=max(sum,j);
//		printf("%d %d %d %.4lf\n",sum,x-1,y,pp);
		ans+=(double)sum*pp;
		return;
	}
	dfs(t+1,x+v[t],y,pp*0.01*(double)p[t]);
	dfs(t+1,x,y+1,pp*0.01*(double)(100-p[t]));
	return;
}
int main()
{
	freopen("leboxes.in","r",stdin);
	freopen("leboxes.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		ans=0;
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++)
		scanf("%d%d",&v[i],&p[i]);
		for (int i=1;i<=m;i++)
		scanf("%d%d",&a[i],&b[i]);
		dfs(1,0,0,1);
		printf("%.4lf\n",ans);
	}
}

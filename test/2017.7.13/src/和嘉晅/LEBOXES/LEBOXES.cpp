#include <iostream>
#include <cstdio>
#include <algorithm>
#include <queue>
using namespace std;
int n,m; long double p[45];
struct N
{ int n1,n2;}g[45];
const long double eps=0.000000008;
int e[45];
int val[45];
struct Node
{
	int t1,t2; int last;
	int ans;
	Node(){}
	Node(int x,int y,int z,int d)
	{ t1=x; t2=y; ans=z; last=d;}
	friend bool operator <(Node x,Node y)
	{ return x.ans<y.ans;}
};
priority_queue<Node> q;
int cal()
{
	int c1=0,c2=0;
	for(int i=1;i<=n;i++)
	if(e[i]) c1+=val[i];
	else c2++;
	int res=0;
	
	while(!q.empty())
	q.pop();
	int T=(m<<2);
	q.push(Node(c1,c2,0,1));
	while((T--)&&(!q.empty()))
	{
		Node now=q.top(); q.pop();
		res=max(res,now.ans);
		for(int i=now.last;i<=m;i++)
			if(now.t1-g[i].n1>=0&&now.t2-g[i].n2>=0)
				q.push(Node(now.t1-g[i].n1,now.t2-g[i].n2,now.ans+1,i+1));
	}
	return res;
}
long double dfs(int dep,long double x)
{
	if(x<eps) return 0;
	if(dep>n)
		return cal();
	long double res=0;
	e[dep]=1;
	res+=dfs(dep+1,x*p[dep])*p[dep];
	e[dep]=0;
	res+=dfs(dep+1,x*(1-p[dep]))*(1-p[dep]);
	return res;
}
bool com(N x,N y)
{ return x.n1!=y.n1?x.n1<y.n1:x.n2<y.n2;}
int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%d",&n,&m);
		for(int i=1,t1;i<=n;i++)
		{
			scanf("%d%d",&val[i],&t1);
			p[i]=t1/(long double)100.0;
		}
		for(int i=1;i<=m;i++)
			scanf("%d%d",&g[i].n1,&g[i].n2);
		sort(g+1,g+1+m,com);
		printf("%.4lf\n",(double)dfs(1,1));
	}
	return 0;
}

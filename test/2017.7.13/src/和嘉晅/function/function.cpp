#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;
long double A,B,C,W;
long double ans;
const long double eps=0.00000008,Maxd=1000000000;
long double cal(long double x)
{
	return A*x+B*sqrtl(x)+C*logl(x)-W;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin>>A>>B>>C>>W;
	
	long double lt=0,rt=Maxd;
	while(abs(rt-lt)>eps)
	{
		long double mid=(lt+rt)/2;
		if(cal(mid)>0) rt=mid;
		else lt=mid; 
	}
	printf("%.18lf",(double)lt);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
using namespace std;
struct Node
{
	int time,pos,val,id;
}g[4005];
bool comp(Node x,Node y)
{ return x.pos<y.pos;}
bool comt(Node x,Node y)
{ return x.time<y.time;}
int pos[4005]; int totp=0,tott=0;
int dp[2005][2005];//
int dis[2005][2005]; int p[2005];
int st[2005]; int pre[2005];
vector<int> T[2005];
int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	memset(dp,-0x3f3f3f3f,sizeof(dp));
	int N,D,U,S;
	scanf("%d%d%d%d",&N,&D,&U,&S);
	for(int i=1;i<=N;i++)
		scanf("%d%d%d",&g[i].time,&g[i].pos,&g[i].val),g[i].id=i;
	
	g[0].time=-0x3f3f3f3f; g[0].pos=S; g[0].val=0; g[0].id=0;
	
	
	sort(g,g+1+N,comp);
	for(int i=0;i<=N;i++)
		pos[g[i].id]=++totp,p[totp]=g[i].pos;
	sort(g,g+1+N,comt);
	for(int i=1;i<=N;i++)
	{
		if(g[i].time!=g[i-1].time)
			tott++;
		T[tott].push_back(i);
	}
	
	for(int i=1;i<=totp;i++)
	for(int j=1;j<=totp;j++)
	{
		if(i==j){dis[i][j]=0; continue;}
		if(p[i]>p[j])
			dis[i][j]=U*(p[i]-p[j]);
		else
			dis[i][j]=D*(p[j]-p[i]);
	}
	for(int i=1;i<=totp;i++)
		dp[0][i]=-dis[pos[0]][i];
	for(int i=1;i<=tott;i++)//t
	{
		if(T[i].size()==1)
		{
			int now=T[i][0];
			for(int j=1;j<=totp;j++)//p
				dp[i][pos[g[now].id]]=max(dp[i][pos[g[now].id]],dp[i-1][j]-dis[j][pos[g[now].id]]+g[now].val);
		}
		else
		{
			int len=T[i].size();//T:id
			for(int j=0,now;j<len;j++)
			{
				now=T[i][j];
				pre[pos[g[now].id]]=g[now].val;
			}
			for(int j=1;j<=totp;j++)
				pre[j]+=pre[j-1];
			//pre[pos]
			
			int t=0;
			for(int j=1;j<=totp;j++)//k<j
			{
				while(t&&  (dp[i-1][st[t]] - pre[st[t]-1] + D*p[st[t]])<(dp[i-1][j] - pre[j-1] + D*p[j]))
					t--;
				st[++t]=j;
				int k=st[1];
				dp[i][j]=max(dp[i][j],dp[i-1][k]+(pre[j]-pre[k-1])-D*(p[j]-p[k]));
			}
			t=0;
			for(int j=totp;j>=1;j--)//k>j
			{
				while(t&& (dp[i-1][st[t]] + pre[st[t]-1] - U*p[st[t]])<(dp[i-1][j] + pre[j-1] - U*p[j]) ) 
					t--;
				st[++t]=j;
				int k=st[1];
				dp[i][j]=max(dp[i][j],dp[i-1][k]+(pre[k]-pre[j-1])-U*(p[k]-p[j]));
			}
		}
		for(int j=1;j<=totp;j++)
			dp[i][j]=max(dp[i-1][j],dp[i][j]);
	}
	int ans=-0x3f3f3f3f;
	for(int i=1;i<=totp;i++)
		ans=max(ans,dp[tott][i]-dis[i][pos[0]]);
	printf("%d\n",ans);
	return 0;
}

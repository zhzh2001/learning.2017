#include<bits\stdc++.h>

#define LL long long
#define N 1e9
#define E 2.718281828

using namespace std;

bool f=1;
double A,B,C,W;

double ji(double mid){
	return A*mid+B*(double)sqrt(mid)+C*log(mid)/log(E)-W;
}

double Abs(double x){return x<0?-x:x;}

void search(double l,double r){
	if (!f) return;
	double mid=(double)(l+r)/2;
	double mix=ji(mid);
	if (Abs(mix)<1e-3) {f=false;printf("%.6lf",mid);return;} 
	else if (mix<0) search(mid,r); else search(l,mid);
}

int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&A,&B,&C,&W);
//	if (ji(N)<0 || ji(0.00001)>0) return 0;
	search(0,N);
}

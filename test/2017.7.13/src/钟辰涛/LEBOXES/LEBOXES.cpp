#include<bits\stdc++.h>

#define LL long long

#define me(A) memset(A,0,sizeof(A))

using namespace std;

struct node{int A,B;}A[101];

int n,m,T,v[101],p[101];
int F[101],F1[101];

double Ans;

bool cmp(node a,node b){return a.A<b.A;}

int Pi(int a,int b){
	me(F);me(F1);
	for (int i=1; i<=m; i++)
		for (int j=b; j>=A[i].B; j--)
			if (F1[j-A[i].B]+A[i].A<=a && F[j-A[i].B]+1>F[j]) {
				F[j]=F[j-A[i].B]+1,F1[j]=F1[j-A[i].B]+A[i].A;
			}
	return F[b];
}

void dfs(int x,double P,int a,int b){
	if (P==0) return;
	if (x>n) {Ans=Ans+(double)(P*Pi(a,b));
	return;}
	dfs(x+1,P*((double)p[x]/100),a+v[x],b);
	dfs(x+1,P*(1-(double)p[x]/100),a,b+1);
}

int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d",&n,&m);
		for (int i=1; i<=n; i++) scanf("%d%d",&v[i],&p[i]);
		for (int j=1; j<=m; j++) scanf("%d%d",&A[j].A,&A[j].B);
		sort(A+1,A+m+1,cmp);
		Ans=0;
		dfs(1,1,0,0);
		printf("%.4lf\n",Ans);
	}
}

#include<bits\stdc++.h>

#define LL long long

using namespace std;

struct node{int Day,Pos,Val;}A[2010];

bool cmp(node x,node y){return x.Day<y.Day || x.Day==y.Day && x.Val<y.Val;}

int Max(int a,int b){return a>b?a:b;}

int N,D,U,S,F[5010];

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d%d%d",&N,&D,&U,&S);
	for (int i=1; i<=N; i++) scanf("%d%d%d",&A[i].Day,&A[i].Pos,&A[i].Val);
	sort(A+1,A+N+1,cmp);
	A[0].Pos=S;
	int MAX=0;
	for (int i=1; i<=N; i++) {
		for (int j=0; j<i; j++)
			F[i]=Max(F[i],F[j]-((A[j].Pos<A[i].Pos)?D*(A[i].Pos-A[j].Pos):U*(A[j].Pos-A[i].Pos))+A[i].Val);
		MAX=Max(F[i]-((A[0].Pos<A[i].Pos)?U*(A[i].Pos-A[0].Pos):D*(A[0].Pos-A[i].Pos)),MAX);
	}
	return printf("%d",MAX),0;
}

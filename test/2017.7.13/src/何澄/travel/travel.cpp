#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,d,u,s;
int maxn=0;
struct node
{
	bool operator < (const node &x) const
	{
		return day<x.day;
	}
	int day,pos,val;
}a[2011];
int f[2011];

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

int cost(int x1,int x2)//x1为出发点，x2为目的地
{
	return (abs(x2-x1)*(x2>x1?d:u));
}

void dfs(int num,int ss,int v)
{
	if(num>n)
	{
		v-=cost(ss,s);
		maxn=max(maxn,v);
		return;
	}
	// maxn=max(maxn,v-cost(a[num].pos,ss));
	// cout << num << " " << ss << " " << v << " " << maxn << endl;
	dfs(num+1,ss,v);
	dfs(num+1,a[num].pos,v+a[num].val-cost(ss,a[num].pos));
}

int main()
{
	// freopen("1.sb","r",stdin);
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),d=read(),u=read(),s=read();
	for(int i=1; i<=n; i++)
		a[i].day=read(),a[i].pos=read(),a[i].val=read();
	// cout << endl;
	sort(a+1,a+n+1);
	// for(int i=1; i<=n; i++)
	// 	cout << a[i].day << " " << a[i].pos << " " << a[i].val << endl;
	// cout << endl;
	// dfs(1,s,0);
	// cout << maxn << endl;
	for(int i=1; i<=n; i++)
	{
		f[i]=a[i].val-cost(s,a[i].pos);
		for(int j=1; j<=i-1; j++)
		{
			f[i]=max(f[i],f[j]-cost(a[j].pos,a[i].pos)+a[i].val);
			maxn=max(maxn,f[i]-cost(a[i].pos,s));
		}
	}
	cout << maxn << endl;
	return 0;
}

/*
4 5 3 100
2 80 100
20 125 130
10 75 150
5 120 110

50
*/
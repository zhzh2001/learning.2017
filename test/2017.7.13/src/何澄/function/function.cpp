#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;

double A,B,C,W;
double x;

double f(double a)
{
	return A*a+B*sqrt(a)+C*log(a)-W;
}

int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin >> A >> B >> C >> W;
	double l=0.0001,r=1000000000;
	if(C==0&&W==0)
	{
		puts("0");
		return 0;
	}
	if(fabs(f(l))<=1e-4)
	{
		printf("%.4f\n",l);
		return 0;
	}
	if(fabs(f(r))<=1e-4)
	{
		printf("%.4f\n",r);
		return 0;
	}
	while(fabs(r-l)>=1e-4)
	{
		// cout << l << " " << r << f(l) << " " << f(r) << " ";
		x=(l+r)/2;
		// cout << x << " " << f(x) << endl;
		double xx=f(x);
		if(fabs(xx)<1e-4)
			break;
		else if(xx*f(l)<0)
			r=x;
		else if(xx*f(r)<0)
			l=x;
	}
	printf("%.4f\n",x);
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;

double A,B,C,W;
inline double ans(double x)
{
	return A*x+B*sqrt(x)+C*log(x)-W;
}

bool ok(double x)
{
	double xx=ans(x);
	if(xx<1e-3&&xx>-1e-3) return true;
	return false;
}
bool check(double x)
{
	double xx=ans(x);
	if(xx<0.0) 
		return true;
	return false; 
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin>>A>>B>>C>>W;
	double l=0,r=1.1e9;
	while(r-l>=1e-8)
	{
		double mid=(r+l)/2.0;
		if(check(mid)) 
			l=mid;
		else r=mid;
	}
	if(ok(l)) printf("%lf",l);
	else printf("%lf",r);
}

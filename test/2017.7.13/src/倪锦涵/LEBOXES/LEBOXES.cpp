#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int MAXN=30+10;
const int MAXV=10000000+10;
int n,m,v[MAXN],p[MAXN];
struct fff
{
	int a,b;
}hh[MAXN];
int sum_diamond;
int sum_dollar;
double P=1;
double ans;
int res;
void dfs2(int t,int sdo,int sdi,int num)
{
	if(sdo>sum_dollar) return ;
	if(sdi>sum_diamond) return ;
	if(t==m)
	{
		res=max(res,num);
		return ;
	}
	dfs2(t+1,sdo+hh[t].a,sdi+hh[t].b,num+1);
	dfs2(t+1,sdo,sdi,num);
}
void dfs(int t)
{
	if(t==n)
	{
		dfs2(0,0,0,0);
		ans=ans+res*P;
		res=0;
		return ;
	}
	double pre_P=P;
	sum_dollar+=v[t];
	P=P*p[t]/100.0;
	dfs(t+1);
	sum_dollar-=v[t];
	P=pre_P;
	sum_diamond++;
	P=P*(100-p[t])/100.0;
	dfs(t+1);
	P=pre_P;
	sum_diamond--;
}
bool cmp(fff a,fff b) {return a.a>b.a;}
double solve()
{
	sort(hh,hh+m,cmp);
	dfs(0);
	return ans;
}
int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--)
	{
		ans=0;
		scanf("%d%d",&n,&m);
		for(int i=0;i<n;i++) scanf("%d%d",v+i,p+i);
		for(int i=0;i<m;i++) scanf("%d%d",&hh[i].a,&hh[i].b);
		printf("%.4lf\n",solve());
	}
}

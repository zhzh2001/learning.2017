#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int MAXN=2000+10;
const int MAXDAY=500000+10;
const int MAXPOS=500000+1+10;
const int MAXVAL=4000+10;
const int MAXD=10+10;
const int CTN=1000;
int n,D,U,S;
struct node
{
	int day,pos,val;
}p[MAXN];
int f[2][MAXPOS],d;
bool cmp(node a,node b)
{
	return a.day==b.day?a.pos<b.pos:a.day<b.day;
}
int pos[MAXN];

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d%d%d",&n,&D,&U,&S);
	for(int i=0;i<n;i++)
	{
		scanf("%d%d%d",&p[i].day,&p[i].pos,&p[i].val);
		pos[i]=p[i].pos;
	}
	pos[n]=S;
	sort(p,p+n,cmp);
	sort(pos,pos+n+1);
	f[d][S]=10;
	f[d][S]=CTN;
	for(int i=0;i<n;i++)
	{
		d^=1;
		int k=i; while(p[k+1].day==p[i].day) k++;
		int tmp=i;
		for(int j=0;j<=n;j++)
		{
			if(f[d^1][pos[j]]==0) continue;
			if(tmp<k&&p[tmp].pos<pos[j]) tmp++;
			if(i==k)
			{
				if(p[i].pos>pos[j])
					f[d][p[i].pos]=max(f[d][p[i].pos],f[d^1][pos[j]]-D*(p[i].pos-pos[j])+p[i].val);
				else 
					f[d][p[i].pos]=max(f[d][p[i].pos],f[d^1][pos[j]]-U*(pos[j]-p[i].pos)+p[i].val);	
			}
			else
			{
				if(tmp>i) f[d][p[tmp-1].pos]=max(f[d][p[tmp-1].pos],f[d^1][pos[j]]-U*(pos[j]-p[tmp-1].pos)+p[tmp-1].val);
				f[d][p[tmp].pos]=max(f[d][p[tmp].pos],f[d^1][pos[j]]-D*(p[tmp].pos-pos[j])+p[tmp].val);
				for(int l=tmp+1;l<=k;l++) f[d][p[l].pos]=max(f[d][p[l].pos],f[d][p[l-1].pos]-D*(p[l].pos-p[l-1].pos)+p[l].val);
				for(int l=k-1;l>=tmp+1;l--) f[d][p[l].pos]=max(f[d][p[l].pos],f[d][p[l+1].pos]-U*(p[l+1].pos-p[l].pos)); 
				for(int l=tmp-2;l>=i;l--) f[d][p[l].pos]=max(f[d][p[l].pos],f[d][p[l+1].pos]-U*(p[l+1].pos-p[l].pos)+p[l].val);
				for(int l=i+1;l<=k;l++) f[d][p[l].pos]=max(f[d][p[l].pos],f[d][p[l-1].pos]-D*(p[l].pos-p[l-1].pos));
			}
		}
		for(int j=0;j<=n;j++) if(f[d][pos[j]]==0) f[d][pos[j]]=f[d^1][pos[j]];
		i=k;
	}
	int ans=0;
	for(int i=0;i<=n;i++)
	{
		if(pos[i]<S) ans=max(ans,f[d][pos[i]]-D*(S-pos[i]));
		else ans=max(ans,f[d][pos[i]]-U*(pos[i]-S));  
	}
	ans-=CTN;
	printf("%d",ans);
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<string.h>
using namespace std;
struct xx{
	int d,p,v;
}num[2003];
inline bool cmp(const xx a,const xx b){
	if(a.d==b.d){
		return a.p<b.p;
	}
	return a.d<b.d;
}
int n,u,d,s;
int ll[2003],rr[2003];
int f[2003][2];
int quan[2003][2003];
int dp2(int now,int z){
	int & fanhui=f[now][z];
	if(fanhui!=-1){
		return fanhui;
	}
	if(s<num[now].p){
		fanhui=-abs(num[now].p-s)*d;
	}else{
		fanhui=-abs(num[now].p-s)*u;
	}
	if(z==0){
		for(int i=ll[now];i<=rr[now];i++){
			fanhui=max(fanhui,dp2(i,1)+quan[now][i]);
		}
	}else{
		for(int i=rr[now]+1;i<=n;i++){
			if(num[now].p<num[i].p){
				fanhui=max(fanhui,dp2(i,0)-abs(num[now].p-num[i].p)*u);
			}else{
				fanhui=max(fanhui,dp2(i,0)-abs(num[now].p-num[i].p)*d);
			}
		}
	}
	return fanhui;
}
int main(){
	memset(f,-1,sizeof(f));
	cin>>n>>d>>u>>s;
	for(int i=1;i<=n;i++){
		cin>>num[i].d>>num[i].p>>num[i].v;
	}
	sort(num+1,num+n+1,cmp);
	int l;
	for(l=1;l<=n;l++){
		int r=l;
		while(num[r+1].d==num[l].d){
			r++;
		}
		if(l==r){
			break;
		}
		for(int i=l;i<=r;i++){
			ll[i]=l;  rr[i]=r;
			int maxr=0,maxl=0,zhong=0,numv=0;
			for(int j=i+1;j<=r;j++){
				maxr=max(maxr,numv+num[j].v-(num[j].p-num[i].p)*(u+d));
				numv+=num[j].v;
			}
			for(int j=l+1;j<=i;j++){
				zhong+=num[j].v;
			}
			zhong-=(num[i].p-num[l].p)*(u);
			maxl=num[l].v;
			cout<<maxl<<endl;
			for(int j=l;j<=i-1;j++){
				quan[j][i]=maxl+zhong+maxr;
				quan[i][j]=maxl+zhong+maxr;
				maxl=max(maxl+num[j].v,num[j].v)-(num[j].p-num[j-1].p)*(u+d);
				maxl=max(maxl,0);
				zhong=zhong-num[j].v-(num[j].v-num[j-1].p)*(u);
			}
		}
		l=r+1;
	}
	for(int i=1;i<=n;i++){
		quan[i][i]=num[i].v;
	}
	int ans=0;
	for(int i=1;i<=n;i++){
		if(s>num[i].p){
			ans=max(ans,dp2(i,0)-abs(s-num[i].p)*d);
		}else{
			ans=max(ans,dp2(i,0)-abs(s-num[i].p)*u);
		}
	}
	return 0;
}
/*
4  5   3  100
20 75  150
20 80  100
20 120 110
20 125 130
*/

//#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("travel.in");
ofstream fout("travel.out");
struct xx{
	int d,p,v;
}num[2003];
bool yes=false;
inline bool cmp(const xx a,const xx b){
	if(a.d==b.d){
		return a.p<b.p;
	}
	return a.d<b.d;
}
inline int abs(int x){
	if(x<0){
		return -x;
	}
	return x;
}
int n,u,d,s;
int jiyi[2003];
int quan[2003][2003],ll[2003],rr[2003];
int dp(int now){
	int& fanhui=jiyi[now];
	if(fanhui!=-1){
		return fanhui;
	}
	if(s<num[now].p){
		fanhui=-abs(num[now].p-s)*d;
	}else{
		fanhui=-abs(num[now].p-s)*u;
	}
	for(int i=now+1;i<=n;i++){
		if(num[now].p<num[i].p){
			fanhui=max(fanhui,dp(i)+num[i].v-abs(num[now].p-num[i].p)*u);
		}else{
			fanhui=max(fanhui,dp(i)+num[i].v-abs(num[now].p-num[i].p)*d);
		}
	}
	return fanhui;
}
int f[2003][2];
int dp2(int now,int z){
	int & fanhui=f[now][z];
	if(fanhui!=-1){
		return fanhui;
	}
	if(s<num[now].p){
		fanhui=-abs(num[now].p-s)*d;
	}else{
		fanhui=-abs(num[now].p-s)*u;
	}
	if(z==0){
		for(int i=ll[now];i<=rr[now];i++){
			fanhui=max(fanhui,dp2(i,1)+quan[now][i]);
		}
	}else{
		for(int i=rr[now]+1;i<=n;i++){
			if(num[now].p<num[i].p){
				fanhui=max(fanhui,dp2(i,0)-abs(num[now].p-num[i].p)*u);
			}else{
				fanhui=max(fanhui,dp2(i,0)-abs(num[now].p-num[i].p)*d);
			}
		}
	}
	return fanhui;
}
int main(){
	memset(jiyi,-1,sizeof(jiyi));
	memset(f,-1,sizeof(f));
	fin>>n>>d>>u>>s;
	for(int i=1;i<=n;i++){
		fin>>num[i].d>>num[i].p>>num[i].v;
	}
	sort(num+1,num+n+1,cmp);
	for(int i=1;i<=n-1;i++){
		if(num[i].d==num[i+1].d){
			yes=true;
			break;
		}
	}
	if(yes==false){
		int ans=0;
		for(int i=1;i<=n;i++){
			if(s>num[i].p){
				ans=max(ans,dp(i)+num[i].v-abs(s-num[i].p)*d);
			}else{
				ans=max(ans,dp(i)+num[i].v-abs(s-num[i].p)*u);
			}
		}
		fout<<ans<<endl;
		return 0;
	}
	int l;
	for(l=1;l<=n;l++){
		int r=l;
		while(num[r+1].d==num[l].d){
			r++;
		}
		if(l==r){
			break;
		}
		for(int i=l;i<=r;i++){
			ll[i]=l;  rr[i]=r;
			int maxr=0,maxl=0,zhong=0,numv=0;
			for(int j=i+1;j<=r;j++){
				maxr=max(maxr,numv+num[j].v-(num[j].p-num[i].p)*(u+d));
				numv+=num[j].v;
			}
			for(int j=l+1;j<=i;j++){
				zhong+=num[j].v;
			}
			zhong-=(num[i].p-num[l].p)*(u);
			maxl=num[l].v;
			fout<<maxl<<endl;
			for(int j=l;j<=i-1;j++){
				quan[j][i]=maxl+zhong+maxr;
				quan[i][j]=maxl+zhong+maxr;
				maxl=max(maxl+num[j].v,num[j].v)-(num[j].p-num[j-1].p)*(u+d);
				maxl=max(maxl,0);
				zhong=zhong-num[j].v-(num[j].v-num[j-1].p)*(u);
			}
		}
		l=r+1;
	}
	for(int i=1;i<=n;i++){
		quan[i][i]=num[i].v;
	}
	int ans=0;
	for(int i=1;i<=n;i++){
		if(s>num[i].p){
			ans=max(ans,dp2(i,0)-abs(s-num[i].p)*d);
		}else{
			ans=max(ans,dp2(i,0)-abs(s-num[i].p)*u);
		}
	}
	return 0;
}
/*

in:
4  5   3  100
2  80  100
20 125 130
10 75  150
5  120 110

out:
50

*/

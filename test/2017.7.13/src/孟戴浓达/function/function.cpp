//#include<iostream>
#include<algorithm>
#include<math.h>
#include<iomanip>
#include<fstream>
using namespace std;
ifstream fin("function.in");
ofstream fout("function.out");
const double eps=1e-9;
long double a,b,c,w;
int main(){
	fin>>a>>b>>c>>w;
	long double l=0.0,r=1000000000.0000;
	while(l<r-eps){
		long double mid=(l+r)/2;
		long double ans=a*mid+b*sqrt(mid)+c*log(mid)+w;
		if((ans)>eps){
			r=mid;
		}else{
			//cout<<fixed<<setprecision(8)<<mid<<endl;
			l=mid;
		}
	}
	fout<<fixed<<setprecision(8)<<l<<endl;
	return 0;
}
/*

in:
1 0 0 0

out:
0

*/

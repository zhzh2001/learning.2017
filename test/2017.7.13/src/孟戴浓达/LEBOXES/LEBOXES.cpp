//#include<iostream>
#include<algorithm>
#include<string.h>
#include<iomanip>
#include<map>
#include<fstream>
using namespace std;
ifstream fin("LEBOXES.in");
ofstream fout("LEBOXES.out");
int n,m,T,x;
double p[33];
int v[33],a[33],b[33];
int f[33][33][33];
struct xx{
	int a,b,c;
};
inline int min(int a,int b){
	if(a<b){
		return a;
	}
	return b;
}
map<pair<pair<int,int>,int>,double> mp;
map<pair<pair<int,int>,int>,bool>   mp2;
int dp(int dim,int rev,int now){
	int & fanhui=f[dim][rev][now];
	if(fanhui!=-1){
		return fanhui;
	}
	if(rev==0){
		return fanhui=0;
	}
	if(now==m+1){
		return fanhui=999999999;
	}
	fanhui=999999999;
	if(dim>=b[now]){
		fanhui=dp(dim-b[now],rev-1,now+1)+a[now];
	}
	fanhui=min(fanhui,dp(dim,rev,now+1));
	return fanhui;
}
inline double cost(int x,int zuan){
	int l=0,r=m,mid;
	while(l<r-1){
		mid=(l+r)>>1;
		if(f[zuan][mid][1]<=x){
			l=mid;
		}else{
			r=mid-1;
		}
	}
	if(f[zuan][l+1][1]<=x){
		return l+1;
	}
	return l;
}
double dfs1(int now,int x,int zuan){
	if(mp2[make_pair(make_pair(now,x),zuan)]==true){
		return mp[make_pair(make_pair(now,x),zuan)];
	}
	mp2[make_pair(make_pair(now,x),zuan)]=true;
	if(now==n+1){
		return mp[make_pair(make_pair(now,x),zuan)]=cost(x,zuan);
	}
	mp[make_pair(make_pair(now,x),zuan)]=((p[now]/100)*dfs1(now+1,x+v[now],zuan)+((100-p[now])/100)*dfs1(now+1,x,zuan+1));
	return mp[make_pair(make_pair(now,x),zuan)];
}
double dfs2(int now,int x,int zuan){
	if(now==n+1){
		return cost(x,zuan);
	}
	if(p[now]==100){
		return ((p[now]/100)*dfs2(now+1,x+v[now],zuan));
	}else if(p[now]==0){
		return (((100-p[now])/100)*dfs2(now+1,x,zuan+1));
	}
	return ((p[now]/100)*dfs2(now+1,x+v[now],zuan)+((100-p[now])/100)*dfs2(now+1,x,zuan+1));
}
int main(){
	fin>>T;
	while(T--){
		memset(f,-1,sizeof(f));
		fin>>n>>m;
		for(int i=1;i<=n;i++){
			fin>>v[i]>>p[i];
		}
		for(int i=1;i<=m;i++){
			fin>>a[i]>>b[i];
		}
		for(int i=0;i<=30;i++){
			for(int j=0;j<=m;j++){
				x=dp(i,j,1);
			}
		}
		double ans;
		if(n<=25){
			ans=dfs2(1,0,0);
		}else{
			ans=dfs1(1,0,0);
			mp.clear();
			mp2.clear();
		}
		fout<<fixed<<setprecision(4)<<ans<<endl;
	}
	return 0;
}
/*

in:
2

2 2
2 50
2 100
2 0
2 0

2 2
2 100
2 50
0 2
0 1

out:
1.5000
0.5000

*/

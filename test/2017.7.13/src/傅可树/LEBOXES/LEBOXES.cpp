#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
const int oo=1e9;
int n,m,p[35],a[35],b[35],dp[35][35][35];
double gl[35],ans;
int min(int a,int b){
	if (a<b) return a; else return b;
}
void dfs(int x,int money,int zs,double P){
	if (x==n+1){
		int res=0;
		for (int i=0;i<=m;i++)
			if (dp[m][i][zs]<=money) res=i;
		ans+=P*(double)res;
		return;
	}
	dfs(x+1,money+p[x],zs,P*gl[x]);
	dfs(x+1,money,zs+1,P*(1-gl[x]));
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++) scanf("%d%lf",&p[i],&gl[i]),gl[i]/=100;
		for (int i=1;i<=m;i++) scanf("%d%d",&a[i],&b[i]);
		memset(dp,63,sizeof(dp));
		/*dp[i][k][j]第i个物品，买了k个，能花J颗钻石最少花多少钱*/ 
		for (int i=0;i<=n;i++) dp[0][0][i]=0;
		for (int i=1;i<=m;i++){
			for (int j=0;j<=n;j++)
			if (j>=b[i])
				for (int k=1;k<=i;k++)
				if (dp[i-1][k-1][j-b[i]]<oo)
					dp[i][k][j]=min(dp[i][k][j],dp[i-1][k-1][j-b[i]]+a[i]);
			for (int j=0;j<=n;j++) dp[i][0][j]=0;
		}
		ans=0;
		dfs(1,0,0,1);
		printf("%.4lf\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

int n,m;
struct node
{
	int v,p;
	bool operator < (const node &b) const
	{
		return p>b.p;
	}
}a[31];
int mon[31],di[31];
int f[31][31];
double ans;

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void dfs(int stp,double gl,int moy,int dia)
{
	if (stp>n)
	{
		bool flag=0;int k;
		for (int i=dia;i>=0;i--)
			if (!flag)
			{
				for (k=m;k>0;k--)
					if (moy>=f[i][k])
					{
						flag=1;break;
					}
			}
		ans+=gl*k;
		return;
	}
	if (gl==0)return;
	dfs(stp+1,gl*a[stp].p/100.0,moy+a[stp].v,dia);
	dfs(stp+1,gl*(100-a[stp].p)/100.0,moy,dia+1);
}

int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read(),m=read();
		memset(f,1,sizeof f);
		for (int i=1;i<=n;i++) a[i].v=read(),a[i].p=read();
		sort(a+1,a+n+1);
		for (int i=1;i<=m;i++) mon[i]=read(),di[i]=read();
		f[0][0]=0;
		for (int i=1;i<=m;i++)
		{
			for (int j=n;j>=di[i];j--)
			{
				for (int k=m;k>=1;k--)
				{
					f[j][k]=min(f[j][k],f[j-di[i]][k-1]+mon[i]);
				}
			}
		}
		dfs(1,1,0,0);
		printf("%.4lf\n",ans);
		ans=0;
	}
	return 0;
}


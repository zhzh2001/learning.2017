#include <iostream>
#include <cstdio>
#include <algorithm>
#include <queue>
using namespace std;

int n,d,u,s;
struct node
{
	int t,ps,val;
	bool operator < (const node &b) const
	{
		return t<b.t;
	}
}p[2011];
bool vis[2011];
long long dist[2011];
int head[2011],to[4000011],nxt[4000011],tot;
long long w[4000011];
struct nd
{
	int dt,pt;
	bool operator < (const nd &b) const
	{
		return dt<b.dt;
	}
};
priority_queue<nd> q;

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void add(int fr,int tt,int ww)
{
	to[++tot]=tt;w[tot]=ww,nxt[tot]=head[fr];head[fr]=tot;
}

int cotdis(int a,int b)
{
	if (p[a].ps>=p[b].ps)
		return u*(p[a].ps-p[b].ps);
	return d*(p[b].ps-p[a].ps);
}

void getlong()
{
	vis[0]=1;
	q.push((nd){0,0});
	int cnt=0;
	while (cnt<n)
	{
		nd nw=q.top();q.pop();vis[nw.pt]=1;
		for (int i=head[nw.pt];i;i=nxt[i])
			if (dist[to[i]]<nw.dt+w[i])
			{
				dist[to[i]]=nw.dt+w[i];
				q.push((nd){dist[to[i]],to[i]});
			}
		cnt++;
	}
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	n=read(),d=read(),u=read(),s=read();
	for (int i=1;i<=n;i++)
		p[i].t=read(),p[i].ps=read(),p[i].val=read();
	sort(p+1,p+n+1);
	p[0].t=0,p[0].ps=s,p[0].val=0;
	for (int i=1;i<=n;i++)
		add(0,i,p[i].val-cotdis(0,i));
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
		{
			add(i,j,p[j].val-cotdis(i,j));
			if (p[j].t==p[i].t)
				add(j,i,p[j].val-cotdis(j,i));
		}
	getlong();
	long long ans=0;
	for (int i=1;i<=n;i++)
		ans=max(ans,dist[i]-cotdis(i,0));
	printf("%I64d\n",ans);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;

double l,r,mid;
double a,b,c,w;

double f(double x)
{
	double tmp=a*x+b*sqrt(x)-w;
	if (x!=0) tmp+=c*log(x);
	return tmp;
}

int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	cin >> a >> b >> c >> w;
	l=0,r=1e9;
	while (r-l>1e-5)
	{
		mid=(l+r)/2.0;
		double f1=f(l),f2=f(mid),f3=f(r);
		if (f1*f2<=0) r=mid;
		else l=mid;
	}
	printf("%.4lf\n",mid);
	return 0;
}

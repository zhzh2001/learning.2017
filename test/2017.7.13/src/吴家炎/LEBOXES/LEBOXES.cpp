#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	int x,y;
}b[32];
int v[32];
int p[32];
int n,m;
double ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool cmp(D a,D b)
{
	return (a.x>b.x||a.x==b.x&&a.y<b.y);
}
void dfs(int x,int y,int z,double xx)
{
	if(x==n+1)
	{
		int yy=y,zz=z;
		for(int i=1;i<=m&&yy>=b[i].x;i++)
		{
			if(b[i].y>zz)continue;
			yy-=b[i].x;zz-=b[i].y;
			ans+=xx;
		}
		return;
	}
	if(p[x]!=0)dfs(x+1,y+v[x],z,xx*p[x]/100);
	if(p[x]!=100)dfs(x+1,y,z+1,xx*(100-p[x])/100);
}
int main()
{
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	int t=read();
	while(t--)
	{
		n=read();m=read();
		ans=0;
		For(i,1,n)
		{
			v[i]=read();
			p[i]=read();
		}
		For(i,1,m)
		{
			b[i].x=read();
			b[i].y=read();
		}
		sort(b+1,b+n+1,cmp);
		dfs(1,0,0,(double)1);
		printf("%.4f\n",ans);
	}
	return 0;
}


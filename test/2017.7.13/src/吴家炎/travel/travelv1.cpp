#include <cstdio>
#include <algorithm>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define FILE(x) freopen(x".in", "r", stdin);freopen(x".out", "w", stdout);
#define N 2005
int n, d, u, s, ans;
int dp[N], dis[N][N];
struct E {
	int d, p, v;
	bool operator < (const E &rhs) const { return d==rhs.d? p < rhs.p : d < rhs.d; }
} ex[N];

int getint() {
	int x = 0, f = 1;
	char c = getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') f=-f;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x*f;
}

int main() {
	FILE("travel");
	n = getint(); d = getint(); u = getint(); s = getint();
	lop(i, 1, n) ex[i].d = getint(), ex[i].p = getint(), ex[i].v = getint();
	std :: sort(ex+1, ex+n+1);
	lop(i, 1, n)
		lop(j, 1, n)
			dis[i][j] = ex[i].p < ex[j].p? d * (ex[j].p - ex[i].p) : u * (ex[i].p - ex[j].p);
	lop(i, 1, n) {
		dis[0][i] = ex[i].p < s? u * (s - ex[i].p) : d * (ex[i].p - s);
		dis[i][0] = ex[i].p < s? d * (s - ex[i].p) : u * (ex[i].p - s);
	}
	lop(i, 1, n) {
		dp[i] = ex[i].v - dis[0][i];
		lop(j, 1, i-1) dp[i] = std::max(dp[i], dp[j] + ex[i].v - dis[j][i]);
	}
	lop(i, 1, n) dp[i] -= dis[i][0];
	lop(i, 1, n) ans = std::max(ans, dp[i]);
	printf("%d", ans);
	return 0;
}


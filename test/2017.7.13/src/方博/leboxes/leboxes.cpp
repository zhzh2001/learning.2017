#include<bits/stdc++.h>
using namespace std;
int A,B;
int a[35],b[35],v[35];
double p[35];
int n,m;
double ans;
int ansm;
void check(int k,int x)
{
	if(k>m){
		ansm=max(ansm,x);
		return;
	}
	if(A>=a[k]&&B>=b[k]){
		A-=a[k];
		B-=b[k];
		check(k+1,x+1);
		A+=a[k];
		B+=b[k];
	}
	check(k+1,x);
}
void dfs(int k,double x)
{
	if(k>n){
		ansm=0;
		check(1,0);
		ans+=x*ansm;
	//	cout<<A<<' '<<B<<' '<<x<<' '<<ans<<endl;
		return;
	}
	B+=1;
	dfs(k+1,x*(1-p[k]));
	B-=1;
	A+=v[k];
	dfs(k+1,x*p[k]);
	A-=v[k];
}
int main()
{
	freopen("leboxes.in","r",stdin); 
	freopen("leboxes.out","w",stdout);
	int t;
	cin>>t;
	while(t--){
		cin>>n>>m;
		for(int i=1;i<=n;i++)
			cin>>v[i]>>p[i],p[i]/=100;
		for(int i=1;i<=m;i++)
			cin>>a[i]>>b[i];
		ans=0;
		dfs(1,1);
		printf("%.4lf\n",ans);
	}
	return 0;
}

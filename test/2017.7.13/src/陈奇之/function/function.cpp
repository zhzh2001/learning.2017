#include<cstdio>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<cstdlib>
const int XZA=300;
using namespace std;
double A,B,C,W,l,r,x,f;
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&A,&B,&C,&W);
	double l=1e-8,r=1e9+1;
	while ((r-l) > 1e-8){
		x=(l+r) / 2;
		f=A*x+B*sqrt(x)+C*log(x)-W;
		if (f > 0) r=x; else l=x;
	}
	printf("%.5lf",l);
	fclose(stdin);fclose(stdout);
	return 0;
}
/*

*/

#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>

const int XZA=300;
using namespace std;
int n,m;
int XZA_AK[33][33];
int V[33],P[33],A[33],B[33];
double dfs(int i,int _A,int _B,double p){
	if (p < 1e-8) return 0.0;
	if (i == n+1) {
		int ans=0;
		for (int j=1;j<=15;j++){
			if (XZA_AK[_B][j] <= _A) ans=j; else break;
		}
		return p * ans;
	}
	return dfs(i+1,_A+V[i],_B,p * 0.01*P[i]) +
		   dfs(i+1,_A,_B+1,p-p * 0.01*P[i]);
}
int T;
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++){
			scanf("%d%d",&V[i],&P[i]);
		}
		
		for (int i=1;i<=m;i++){
			scanf("%d%d",&A[i],&B[i]);
		}
		
		memset(XZA_AK,0x70,sizeof(XZA_AK));
		XZA_AK[0][0]=0;
		for (int i=1;i<=m;i++){
			for (int j=n;j>=0;j--)
				for (int k=m;k>=1;k--){
					if (j-B[i] < 0) continue;
					XZA_AK[j][k] = min(XZA_AK[j][k] , XZA_AK[j-B[i]][k-1] + A[i]);
				}
		}
		for (int i=1;i<=n;i++){
			for (int j=0;j<=m;j++){
//				printf("(%d,%d):%d\n",i,j,dp[i][j]);
				XZA_AK[i][j] = min(XZA_AK[i][j] , XZA_AK[i-1][j]);
//				printf("(%d,%d):%d\n",i,j,dp[i][j]);
			}
		}
		printf("%.4lf\n",dfs(1,0,0,1.0));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*

2
10 6
1 50
2 50
3 50
4 50
5 50
6 50
7 50
8 50
9 50
10 50

50 5
50 5
50 5
50 5
50 5
5 5

2 2
2 50
2 100
2 0
2 0
*/

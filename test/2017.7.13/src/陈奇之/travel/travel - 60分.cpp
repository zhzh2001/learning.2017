#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<iostream>
using namespace std;
const int maxn = 2005;
struct Type{
	int day,pos,val;
}T[maxn];
int D[maxn],P[maxn],V[maxn],sum[maxn],dp[maxn],f[maxn]; 
int Down,Up,n,S,ans;
bool cmp(Type a,Type b){
	return (a.day<b.day) || ((a.day == b.day) && (a.pos < b.pos));
}

int Cost(int a,int b){
	if (a<b) return (b-a) * Down;//˳������
	else     return (a-b) * Up;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel_60.out","w",stdout);
	scanf("%d%d%d%d",&n,&Down,&Up,&S);
	for (int i=1;i<=n;i++){
		scanf("%d%d%d",&T[i].day,&T[i].pos,&T[i].val);
	}
	sort(T+1,T+1+n,cmp);
	for (int i=1;i<=n;i++){
		D[i]=T[i].day;
		P[i]=T[i].pos;
		V[i]=T[i].val;
//		printf("D:%d,P:%d,V:%d\n",D[i],P[i],V[i]);
	}
	D[0] = 0;
	P[0] = S;
	V[0] = 0;
	memset(dp,0xcf,sizeof(dp));dp[0] = 0;
//	printf("%d\n",dp[2]);
	int j=0;
	for (int i=1;i<=n;i++){
		for (int j=0;j<i;j++){
				dp[i] = max(dp[i] , dp[j] + V[i] - Cost(P[j],P[i]));
		} 
	}
	ans=0;
	for (int i=1;i<=n;i++){
		dp[i] = dp[i] - Cost(P[i], P[0]);
	}
	for (int i=1;i<=n;i++) ans=max(ans,dp[i]);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}






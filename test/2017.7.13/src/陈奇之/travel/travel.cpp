#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<iostream>
const int XZA=300;
using namespace std;
const int maxn = 2005;
struct AK_TO_XZA{
	int day,pos,val;
}XZA_AK[maxn];
int D[maxn],P[maxn],V[maxn],sum[maxn],dp[maxn],f[maxn]; 
int Down,Up,n,S,ans;
bool cmp(AK_TO_XZA a,AK_TO_XZA b){
	return (a.day<b.day) || ((a.day == b.day) && (a.pos < b.pos));
}

int Cost(int a,int b){
	if (a<b) return (b-a) * Down;//顺流而下
	else     return (a-b) * Up;
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	scanf("%d%d%d%d",&n,&Down,&Up,&S);
	for (int i=1;i<=n;i++){
		scanf("%d%d%d",&XZA_AK[i].day,&XZA_AK[i].pos,&XZA_AK[i].val);
	}
	sort(XZA_AK+1,XZA_AK+1+n,cmp);
	for (int i=1;i<=n;i++){
		D[i]=XZA_AK[i].day;
		P[i]=XZA_AK[i].pos;
		V[i]=XZA_AK[i].val;
//		printf("D:%d,P:%d,V:%d\n",D[i],P[i],V[i]);
	}
	D[0] = 0;
	P[0] = S;
	V[0] = 0;
	memset(dp,0xee,sizeof(dp));dp[0] = 0;
//	printf("%d\n",dp[1]);
	int j=0;
	for (int i=1;i<=n;i=j+1){
		j=i;
		while (D[j+1] == D[i]) j++;
		//i~j这一段是同一天举行的
		
		sum[i-1] = 0;
		for (int k=i;k<=j;k++) sum[k] = sum[k-1] + V[k];
		
		for (int k=i;k<=j;k++){
			for (int l=0;l<=i-1;l++){
				dp[k] = max(dp[k] , dp[l] + V[k] - Cost(P[l],P[k]));
			}
		} 
		
		for (int k=i;k<=j;k++) f[k] = dp[k]; 
		
		for (int k=i;k<=j;k++){
			for (int l=i;l<k;l++){
				dp[k] = max(dp[k] , f[l] + sum[k] - sum[l] - Cost(P[l],P[k]));
			}
		}//从左边走到右边的 
		for (int k=i;k<=j;k++){
			for (int l=k+1;l<=j;l++){
				dp[k] = max(dp[k] , f[l] + sum[l-1] - sum[k-1] - Cost(P[l] , P[k]));
			}
		}//从右边走到左边 
	}
	ans=0;
	for (int i=1;i<=n;i++){
//		printf("%d ",dp[i]);
		dp[i] = dp[i] - Cost(P[i], P[0]);
//		printf("%d\n",dp[i]);
	}
	for (int i=1;i<=n;i++) ans=max(ans,dp[i]);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
    旅行商认定如何优化旅行路线是一个非常棘手的计算问题，所以他决定沿
着线性的多瑙河开展他的业务。他有一条快船能够在瞬间沿着多瑙河把他从任
意开始的位置带到任意目的地，但是这条船很费油。它逆流而上(驶向源头的方
向)每米花..元，顺流而下每米花..元(驶离源头的方向）。沿着多瑙河有..个展销
会是旅行商所感兴趣的。每个展销会只持续一天。对于任意一个展销会..,旅行
商知道：
*/




#include <algorithm>
#include <iostream>
#include <cstdio>
#define MAX 2010
using namespace std;
int N, D, U, S, ans, DP[MAX][MAX];
char c;
struct DATA{
	int Day;
	int pos;
	int val;
}pum[MAX];

inline bool cmp(DATA a, DATA b)
{
	return a.Day < b.Day;
}

int read()
{
	int x=0,f=1;
	c=getchar();
	while (!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

int get(int i)
{
	if (pum[i].pos>S) return D*(pum[i].pos-S);
	return U*(S-pum[i].pos);
}

int Get(int i,int j)
{
	if (pum[i].pos>pum[j].pos) return U*(pum[i].pos-pum[j].pos);
	return D*(pum[j].pos-pum[i].pos);
}

int GEt(int i)
{	
	if (pum[i].pos>S) return U*(pum[i].pos-S);
	return D*(S-pum[i].pos);
}

int GET()
{
	int Last = N;
	for (int i = 1;i<=N;i++)
		ans = max(DP[Last][i]-GEt(i),ans);
	return max(ans,0);
}

int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	N = read(), D = read(), U = read(), S = read();
	for (int i = 1;i<=N;i++){
		pum[i].Day = read();
		pum[i].pos = read();
		pum[i].val = read();
	}
	sort(pum+1,pum+1+N,cmp);
	for (int i = 1;i<=N;i++)
		DP[0][i] = 0-get(i);
	for (int i = 1;i<=N;i++){
		DP[i][i] = DP[i-1][i]+pum[i].val;
		for (int j = 1;j<=N;j++)
			DP[i][j] = max(DP[i][i]-Get(i,j),DP[i-1][j]);
	}
	ans = GET();
	cout << ans;
	fclose(stdin),fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
struct data{
	int x,y;
}a[35];
struct arr{
	int c,d;
	double p;
}f[100010];
int t,n,m,cnt;
int v[35];
double ans,p[35];
 bool cmp(const data &a,const data &b)
{
	if (a.x==b.x) return a.y<b.y;
	return a.x<b.x;
}
 void dfs(int x,double s,int c,int d)
{
	if (x>n)
	{
		++cnt;
		f[cnt].p=s;
		f[cnt].c=c;
		f[cnt].d=d;
	}
	if (p[x]==1.0) 	
		dfs(x+1,s,c+v[x],d);
	else
	{
		dfs(x+1,s*p[x],c+v[x],d);
		dfs(x+1,s*(1.0-p[x]),c,d+1);
	} 
}
 int main()
{
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;++i)
		{
			scanf("%d%lf",&v[i],&p[i]);
			p[i]/=100;
		}
		for (int i=1;i<=m;++i)
			scanf("%d%d",&a[i].x,&a[i].y);
		sort(a+1,a+m+1,cmp);
		cnt=0;
		dfs(1,p[1],0,0);
		ans=0.0;
		for (int i=1;i<=cnt;++i)
		{
			int sum=0;
			int lc=f[i].c,ld=f[i].d;
			for (int j=1;j<=m;++j)
			{
				if (lc<a[j].x) continue;
				if (ld<a[j].y) continue;
				lc-=a[j].x;ld-=a[j].y;
				sum++;
			}
			ans+=sum*f[i].p;
		}
		printf("%lf",ans);
	}
}

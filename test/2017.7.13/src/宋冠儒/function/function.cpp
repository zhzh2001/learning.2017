#include<bits/stdc++.h>
#define db double
using namespace std;
const db eps=1e-3;
db a,b,c,w,ans;
 db check(db x)
{
	if (c==0) return (a*x+b*sqrt(x)-w);
	return (a*x+b*sqrt(x)+c*log(x)-w);
}
 int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
	if (abs(check(0.0))<eps) 
	{
		cout<<0;
		return 0;
	}
	db l=0.0,r=1000000000.0;
	while (l<=r)
	{
		db mid=(l+r)/2;
		db s=check(mid);
		if (abs(s)<eps) 
		{
			ans=mid;
			break;
		}
		else if (s<0) l=mid;
		else if (s>0) r=mid;
	}
	printf("%lf",ans);
}

#include<bits/stdc++.h>
using namespace std;
const int inf=0x3f3f3f3f;
int n,mx,D,U,S;
int maxu,maxd;
int f[500010],tmp[500010],tmp2[500010];
struct data{
    int day,x,w;
}a[500010];
struct tree{
    int mxu,mxd;
}t[500010<<2];
 int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0' || ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0' && ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
 bool cmp(const data &a,const data &b)
{
	if (a.day==b.day) return a.x<b.x;
	return a.day<b.day;
}
 void pushup(int k)
 {
    t[k].mxd=max(t[k<<1].mxd,t[k<<1|1].mxd);
    t[k].mxu=max(t[k<<1].mxu,t[k<<1|1].mxu);
    return;
}
 void build(int k,int l,int r)
{
    if(l==r)
	{
		t[k].mxd=t[k].mxu=-inf;
		return;
	}
    int mid=(l+r)>>1;
    build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
    pushup(k);
}
 void update(int k,int l,int r,int p)
{
    if(l==r)
	{
        t[k].mxd=f[p]+a[p].x*D;    
		t[k].mxu=f[p]-a[p].x*U;
        return;
    }
    int mid=(l+r)>>1;
    if(a[p].x<=mid)update(k<<1,l,mid,p);
    else update(k<<1|1,mid+1,r,p);
    pushup(k);
}

 void ask(int k,int l,int r,int x,int y)
{
    if(x==l&&r==y)
	{
        maxd=max(t[k].mxd,maxd);
		maxu=max(t[k].mxu,maxu);
        return;
    }
    int mid=(l+r)>>1;
    if(x>mid) ask(k<<1|1,mid+1,r,x,y);
    else if(y<=mid)ask(k<<1,l,mid,x,y);
    else 
    {
    	ask(k<<1,l,mid,x,mid);
    	ask(k<<1|1,mid+1,r,mid+1,y);
    }
}
 int main()
{
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
    n=read();D=read();U=read();S=read();
    a[1]=(data){0,S,0};n++;
    for(int i=2;i<=n;i++)
	{
        a[i].day=read();a[i].x=read();a[i].w=read();
        mx=max(mx,a[i].x);
    }
    sort(a+1,a+n+1,cmp);
    ++n;a[n]=(data){a[n-1].day+1,S,0};
    mx=max(mx,S);
	build(1,1,mx);update(1,1,mx,1);
    for(int i=2,j;i<=n;i=j+1)
	{
        for(j=i;a[j].day==a[i].day;j++)
		{
            f[j]=-inf;
            maxu=-inf; 
			ask(1,1,mx,a[j].x,mx);
            f[j]=max(f[j],maxu+a[j].x*U);
            maxd=-inf; 
			ask(1,1,mx,1,a[j].x);
            f[j]=max(f[j],maxd-a[j].x*D);
            f[j]+=a[j].w;
        }
		j--;
        tmp[i]=f[i];tmp2[j]=f[j];
        for(int k=i+1;k<=j;k++)
            tmp[k]=max(f[k],tmp[k-1]-(a[k].x-a[k-1].x)*D+a[k].w);
        for(int k=j-1;k>=i;k--)
            tmp2[k]=max(f[k],tmp2[k+1]-(a[k+1].x-a[k].x)*U+a[k].w);
        for(int k=i;k<=j;k++)
		{
            f[k]=max(f[k],max(tmp[k],tmp2[k]));
            update(1,1,mx,k);
        }
    }
    printf("%d\n",f[n]);
}

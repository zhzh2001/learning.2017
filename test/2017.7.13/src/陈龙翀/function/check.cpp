#include<bits/stdc++.h>
double a,b,c,w,x;
using namespace std;
double e=2.718281828;
double ln(double x)
{
	return log(x)/log(e);
}
double calc(double x)
{
	return a*x+b*sqrt(x)+c*ln(x)-w;
}
int main()
{
	freopen("function.in","r",stdin);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&w);
	freopen("function.out","r",stdin);
	scanf("%lf",&x);
	freopen("check.out","w",stdout);
	double ans=calc(x);
	if (fabs(ans-0)<1e-3) return puts("1");
	return puts("0");
}

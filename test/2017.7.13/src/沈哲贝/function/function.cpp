#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 400010 
#define ld long double
#define eps 1e-12
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
double a,b,c,d;
ld A,B,C,D;
ld ln(ld x){
	/*ld l=-1e9,r=1e9,mid;
	while(r-l>eps){
		mid=(l+r)/2;
		exp(mid)<x?l=mid:r=mid;
	}*/
	return log(x);
}
bool calc(ld x){	return A*x+B*sqrt(x)+C*ln(x)-D<0;}
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	scanf("%lf%lf%lf%lf",&a,&b,&c,&d);
	A=a;	B=b;	C=c;	D=d;
	double x=exp(1);
	ld l=0,r=1e9,mid,ans;
	while(r-l>eps){
		mid=(l+r)/2;
		calc(mid)?l=mid:r=mid;
	}
	printf("%.10lf",(double)mid);
}

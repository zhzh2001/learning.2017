#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define ld long double
#define maxn 40
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll v[maxn],p[maxn],bin[maxn],n,m,f[maxn][maxn];
inline ll calc(ll zuan_shi,ll coin){
//	printf("%lld %lld-------------------\n",zuan_shi,coin);
	FOr(i,n,1)	if (f[zuan_shi][i]<=coin)	return i;
	return 0;
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("baoli.out","w",stdout);
	bin[0]=1;	For(i,1,30)	bin[i]=bin[i-1]<<1;
	ll T=read();
	while(T--){
		memset(f,63,sizeof f);
		n=read();	m=read();
		f[0][0]=0;
		For(i,1,n)	v[i]=read(),p[i]=read();
		For(k,1,m){
			ll a=read(),b=read();
			FOr(i,n,b)	FOr(j,n,1)	f[i][j]=min(f[i][j],f[i-b][j-1]+a);
		}
		For(i,1,n)	For(j,1,n)	f[i][j]=min(f[i][j],f[i-1][j]);
		ld answ=0,now;
		For(i,0,bin[n]-1){
			ll ans=0,tot=0;	now=1;
			For(j,1,n)
			if (i&bin[j-1])	ans+=v[j],now*=((ld)p[j])/100;
			else	now*=((ld)(100-p[j]))/100,++tot;
			answ+=now*calc(tot,ans);
		}printf("%.4lf\n",(double)answ);
	}
}
/*
2
2 2
2 50
2 100
2 0
2 0
2 2
2 100
2 50
0 2
0 1
*/

#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define ld long double
#define maxn 40
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
struct data{	ll coin;	ld maybe;}q[22][100000];
ll sum[maxn],v[maxn],p[maxn],bin[maxn],n,m,f[maxn][maxn],t,tt;	ld answ,now;
inline bool cmp(data a,data b){	return a.coin<b.coin;	}
inline ld get(ll need_coin,ll bel){
	ll l=1,r=sum[bel],ans=sum[bel]+1,mid;
	while(l<=r){
		mid=(l+r)>>1;
		if (q[bel][mid].coin<need_coin)	l=mid+1;
		else	r=mid-1,ans=mid;
	}
	return q[bel][ans].maybe;
}
inline ll change(ll zuan_shi,ll coin,ld maybe){
	ld ans=0;
	For(i,0,t)//枚举之前几个钻石
	For(j,1,n)//枚举选几个
	if (f[zuan_shi+i][j]!=f[31][0])
	ans+=get(f[zuan_shi+i][j]-coin,i);
	answ+=ans*maybe;
}
int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	bin[0]=1;	For(i,1,30)	bin[i]=bin[i-1]<<1;
	ll T=read();
	while(T--){
		memset(f,63,sizeof f);
		memset(sum,0,sizeof sum);
		n=read();	m=read();
		f[0][0]=answ=0;
		For(i,1,n)	v[i]=read(),p[i]=read();
		For(k,1,m){
			ll a=read(),b=read();
			FOr(i,n,b)	FOr(j,n,1)	f[i][j]=min(f[i][j],f[i-b][j-1]+a);
		}
		For(i,1,n)	For(j,1,n)	f[i][j]=min(f[i][j],f[i-1][j]);
		tt=min(n/2,12);	t=n-tt;
		For(i,0,bin[t]-1){
			ll ans=0,tot=0;	now=1;
			For(j,1,t)
			if (i&bin[j-1])	ans+=v[j],now*=((ld)p[j])/100;
			else	now*=((ld)(100-p[j]))/100,++tot;
			q[tot][++sum[tot]]=(data){ans,now};
		}
		For(i,0,t){
			sort(q[i]+1,q[i]+sum[i]+1,cmp);
			FOr(j,sum[i],1)	q[i][j].maybe+=q[i][j+1].maybe;
		}
		For(i,0,bin[tt]-1){
			ll ans=0,tot=0;	now=1;
			For(j,1,tt)
			if (i&bin[j-1])	ans+=v[j+t],now*=((ld)p[j+t])/100;
			else	now*=((ld)(100-p[j+t]))/100,++tot;
			change(tot,ans,now);
		}
		printf("%.4lf\n",(double)answ);
	}
//	writeln(clock());
}
/*
2
2 2
2 50
2 100
2 0
2 0
2 2
2 100
2 50
0 2
0 1
*/ 

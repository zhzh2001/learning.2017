#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<map>
#define ll long long
#define maxn 20010
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
const ll inf=1e18;
struct data{	ll day,pos,val;	}p[maxn];
ll q[maxn],f[maxn],g[maxn],h[maxn],sum[maxn],SUM[maxn],dis[maxn],DIS[maxn],day[maxn],val[maxn],S,n,R,L,tot,szb[maxn],answ;
inline bool cmp(data a,data b){	return a.day==b.day?a.pos<b.pos:a.day<b.day;}
void init(){
	n=read();	R=read();	L=read();	S=read();	q[++tot]=S;
	memset(f,-60,sizeof f);
	For(i,1,n)	p[i].day=read(),p[i].pos=read(),p[i].val=read(),q[++tot]=p[i].pos;
	sort(q+1,q+tot+1);
	S=lower_bound(q+1,q+tot+1,S)-q;	day[S]=-inf;
	For(i,1,n)	p[i].pos=lower_bound(q+1,q+tot+1,p[i].pos)-q,
				day[p[i].pos]=p[i].day,
				val[p[i].pos]=p[i].val;
	For(i,1,tot)	dis[i]=q[i],DIS[i]=q[n]-q[i];
	sort(p+1,p+n+1,cmp);	f[S]=0;
//	For(i,1,n)	printf("%d %d %d\n",p[i].pos,p[i].day,p[i].val);
}
int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	init();
	for(ll i=1,j;i<=n;i=j+1){
		j=i;
		while(p[j+1].day==p[i].day&&j<n)	++j;
		For(k,1,tot)	g[k]=f[k]-dis[k]*(R+L),h[k]=f[k]-DIS[k]*(R+L),szb[k]=f[k];
		ll now=inf;
		For(k,1,tot)	sum[k]=sum[k-1]+(day[k]==p[i].day)*val[k];
		FOr(k,tot,1)	SUM[k]=SUM[k+1]+(day[k]==p[i].day)*val[k];
		For(k,1,tot){
			g[k]=g[k]+sum[k]-now;
			now=min(now,sum[k-1]-dis[k]*(L+R));
		}
		now=inf;
		FOr(k,tot,1){
			h[k]=h[k]+SUM[k]-now;
			now=min(now,SUM[k+1]-DIS[k]*(L+R));
		}
		For(k,i,j){//+sum 
			For(l,1,p[k].pos-1)	 f[p[k].pos]=max(f[p[k].pos],max(f[l],g[l])-(dis[p[k].pos]-dis[l])*R+sum[p[k].pos]-sum[l]);
			For(l,p[k].pos+1,tot)f[p[k].pos]=max(f[p[k].pos],max(f[l],h[l])-(DIS[p[k].pos]-DIS[l])*L+SUM[p[k].pos]-SUM[l]);
		}
	}
	answ=0;
	For(i,1,S-1)	answ=max(answ,f[i]-R*(dis[S]-dis[i]));
	For(i,S+1,n)	answ=max(answ,f[i]-L*(DIS[S]-DIS[i]));
	writeln(answ);
}
/*
2 1 1 100
1 99 100
1 101 100
*/

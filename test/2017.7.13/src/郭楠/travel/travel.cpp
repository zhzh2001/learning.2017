#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define N 2005
using namespace std;
ll read()
{ll t=0;char c;
  c=getchar();
  while(!(c>='0' && c<='9')) c=getchar();
  while(c>='0' && c<='9')
    {
    	t=t*10+c-48;c=getchar();
    }
   return t;
}
ll n,m,d,u,sx,ss,MAX,l,r;
struct node{ll day,pos,val;};
node s[N];
ll f[N];
bool cmp(node a,node b)
{
	if(a.day<b.day) return true;
	  else if((a.day==b.day && a.pos<b.pos)) return true;else return false;
}
int main()
{ll i,j,k,i1;
    freopen("travel.in","r",stdin);
    freopen("travel.out","w",stdout);
	n=read();d=read();u=read();sx=read();
	for(i=1;i<=n;i++)
	  {
	  	s[i].day=read();s[i].pos=read();s[i].val=read();
	  }
	sort(s+1,s+n+1,cmp);
	s[0].pos=sx;
	l=1;r=1;
	while(l<=n)
	  {
	  	while(s[r+1].day==s[l].day) r++;
	  	for(i1=0;i1<l;i1++)
	  	  for(i=l;i<=r;i++)
	  	  {ll sum=f[i1],sss;
	  	    for(j=i;j<=r;j++)
	  	      {
	  	      	sum+=s[j].val;sss=sum;
	  	      	if(s[i1].pos>s[i].pos) sss-=(s[i1].pos-s[i].pos)*u;
	  	      	                  else sss-=(s[i].pos-s[i1].pos)*d;
	  	      	sss-=(s[j].pos-s[i].pos)*d;
	  	      	f[j]=max(f[j],sss);sss=sum;
	  	      	if(s[i1].pos>s[j].pos) sss-=(s[i1].pos-s[j].pos)*u;
	  	      	                   else sss-=(s[j].pos-s[i1].pos)*d;
	  	      	sss-=(s[j].pos-s[i].pos)*u;
	  	      	f[i]=max(f[i],sss);
	  	      }
	      }
	    l=r+1;r=r+1;
	  }
	MAX=0;
	for(i=1;i<=n;i++)
	  {
	  	ll sss;
	  	if(s[i].pos>s[0].pos) sss=f[i]-(s[i].pos-s[0].pos)*u;
	  	                else sss=f[i]-(s[0].pos-s[i].pos)*d;
	  	MAX=max(MAX,sss);
	  }
	printf("%I64d",MAX);
}

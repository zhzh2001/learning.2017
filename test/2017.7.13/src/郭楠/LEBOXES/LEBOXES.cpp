#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 35
#define M 35
using namespace std;
int n,m,a[M],t,v[N],b[M];
double ans,p[N];
long long f[N][N];
void solve(int q,int w,double pp)
{int i,j,k,MAX=0;
	for(i=0;i<=m;i++) for(j=0;j<=w;j++) f[i][j]=1000000000000ll;
	f[0][0]=0;
	for(i=1;i<=m;i++)
	  for(j=m;j>=1;j--)
	    for(k=w;k>=b[i];k--)
	      f[j][k]=min(f[j][k],f[j-1][k-b[i]]+a[i]);
	for(i=0;i<=m;i++) for(j=0;j<=w;j++)
	  if(f[i][j]<=q) MAX=max(MAX,i);
	ans=ans+1.0*MAX*pp; 
}
void dfs(int x,int q,int w,double pp)
{
	if(x>n) solve(q,w,pp);
	else 
	  {
	  	if(p[x]>0) dfs(x+1,q+v[x],w,pp*p[x]);
	  	if(p[x]<1) dfs(x+1,q,w+1,pp*(1-p[x]));
	  }
}
int sx;
int main()
{int i,j,k;
    freopen("LEBOXES.in","r",stdin);
    freopen("LEBOXES.out","w",stdout);
    scanf("%d",&t);
    for(;t;t--)
     {
     	scanf("%d%d",&n,&m);
     	ans=0;
     	for(i=1;i<=n;i++)
     	  	 {
     	  	  scanf("%d %d",&v[i],&sx);
     	      p[i]=1.0*sx/100;
			 }
		for(i=1;i<=m;i++) scanf("%d%d",&a[i],&b[i]);
		dfs(1,0,0,1);
	    printf("%.4lf\n",ans);
     }
}


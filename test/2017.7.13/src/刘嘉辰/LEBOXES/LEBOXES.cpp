#include<cstdio>
#include<iostream>
#include<iomanip>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn = 35;

int T,n,m;
int V[maxn], P[maxn], A[maxn], B[maxn];
double re;

double p;
int cnt_v, cnt_d;

int now_v, now_d, now_cnt, mx;
void cal(int i){
	if(i > m){
		mx = max(mx, now_cnt);
		return;
	}
	if(now_v + A[i] <= cnt_v && now_d + B[i] <= cnt_d){
		now_cnt++; now_v += A[i]; now_d += B[i];
		cal(i+1);
		now_cnt--; now_v -= A[i]; now_d -= B[i];
	}
	cal(i+1);
}

void dfs(int i){
	if(i > n){
		now_cnt = now_v = now_d = mx = 0;
		cal(1);
		re += p * mx;
		return ;
	}
	
	if(P[i] != 0){
		p *= P[i]/100.0, cnt_v += V[i];
		dfs(i+1);
		p /= P[i]/100.0, cnt_v -= V[i];
	}
	if(100-P[i] != 0){
		p *= (100-P[i])/100.0, cnt_d++;
		dfs(i+1);
		p /= (100-P[i])/100.0, cnt_d--;
	}
}

int main(){
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	scanf("%d",&T); while(T--){
		scanf("%d%d",&n,&m);
		for(int i=1; i<=n; i++)
			scanf("%d%d",&V[i],&P[i]);
		for(int i=1; i<=m; i++)
			scanf("%d%d",&A[i],&B[i]);
		cnt_v = cnt_d = 0; p = 1; re = 0; dfs(1);
		cout << fixed << setprecision(4) << re << endl;
	}
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;

typedef long long LL;
const int maxn = 2005;

struct node{
	node *l, *r;
	int L,R,mx,tag;
	node(){}
	node* _new(int _L, int _R){
		L = _L, R = _R, l = r = NULL, mx = tag = 0;
		return this;
	}
	void maintain(){mx = max(l->mx, r->mx);}
	void push_down(){
		l->tag += tag, l->mx += tag;
		r->tag += tag, r->mx += tag;
		tag = 0;
	}
	int ask(int lt,int rt){
		if(L == lt && R == rt) return mx;
		
		push_down(); int mid = L+R>>1;
		if(rt <= mid) return l->ask(lt,rt);
		else if(mid < lt) return r->ask(lt,rt);
		else return max(l->ask(lt,mid), r->ask(mid+1,rt));
	}
	void segadd(int lt,int rt,int x){
		if(L == lt && R == rt){
			tag += x, mx += x;
		} else {
			push_down(); int mid = L+R>>1;
			if(rt <= mid) l->segadd(lt,rt,x);
			else if(mid < lt) r->segadd(lt,rt,x);
			else l->segadd(lt,mid,x), r->segadd(mid+1,rt,x);
			maintain();
		}
	}
}*rt, pool[(maxn<<1)+5];

int N, D, U, S, re;

int day[maxn], pos[maxn], dp[maxn][maxn];

struct exh{int day,pos,val;}E[maxn];
bool cmp(exh a, exh b){return a.day != b.day ? a.day < b.day : a.pos > b.pos;}

inline int dis(int x, int y){return x < y ? (pos[y]-pos[x])*D : (pos[x]-pos[y])*U;}
//from x to y;

int tot,k;			
void built(node * &o, int L, int R){
	if(!o) o = pool[tot++]._new(L,R);
	if(L == R) o->mx = dp[k][L] - dis(L,1);
	else{
		int mid = L+R>>1;
		built(o->l,L,mid), built(o->r,mid+1,R);
		o->maintain();
	}
}

int main(){
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
	
	scanf("%d%d%d%d",&N,&D,&U,&S);
	for(int i=1; i<=N; i++){
		scanf("%d%d%d",&E[i].day,&E[i].pos,&E[i].val);
		day[i] = E[i].day; pos[i] = E[i].pos;
	} pos[N+1] = S;
	sort(E+1,E+N+1,cmp); sort(day+1,day+N+1); sort(pos+1,pos+N+2);
	
	memset(dp, -0x3f, sizeof(dp));
	
	int rks = lower_bound(pos+1,pos+N+2,S)-pos;
	
	for(int i=1; i<=N+1; i++)
		dp[0][i] = -dis(rks, i);
    
	for(int i=1; i<=N; i++){
		int p = lower_bound(pos+1,pos+N+2,E[i].pos)-pos;
		for(int j=1; j<=N+1; j++){
			if(j == 1){
				rt = NULL, tot = 0, k = i-1;
				built(rt,1,N+1);
			} else {
				rt->segadd(1,j-1,-dis(j-1,j));
				rt->segadd(j,N+1,dis(j,j-1));
			}
			dp[i][j] = max(dp[i][j], rt->ask(1,N+1));
			if(j == p) dp[i][j] += E[i].val;
		}
	}
	for(int j=1; j<=N+1; j++)
		re = max(re, dp[N][j]-dis(j,rks));
	printf("%d",re);
	return 0;
}


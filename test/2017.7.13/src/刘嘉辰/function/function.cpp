#include<cstdio>
#include<iostream>
#include<iomanip>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long LL;
typedef long double LD;
const double eps = 0.000001;

LD A,B,C,W;
inline LD ln(LD x){return log2l(x) / M_LOG2E;}
inline LD Func(LD x){return A*x + B * sqrtl(x) + C * ln(x) - W;}
int judge(LD x){
	LD v = Func(x);
	if(-eps < v-0 && v-0 < eps) return -1;
	return v < 0;
}

int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	ios::sync_with_stdio(false);
	cin >> A >> B >> C >> W;
	LD l = 0, r = 1e9;
	while(l + eps < r){
		LD mid = (l+r) / 2;
		int d = judge(mid);
		if(d == -1) {l = mid; break;}
		if(d)l = mid; else r = mid;
	}
	cout << fixed << setprecision(15) << l;	
	return 0;
}


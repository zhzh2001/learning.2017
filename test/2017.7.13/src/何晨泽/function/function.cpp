//Live long and prosper.
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define dd double
#define ff long long 
#define fd long double
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
ff i,j,k,a,b,c,w;
int main() {
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
//	std::ios::sync_with_stdio(false);
	cin>>a>>b>>c>>w;
	fd l=1e-15,r=1e9;
	while (l+1e-9<r) {
		fd m=(l+r)/2;
		if (a*m+b*sqrt(m)+c*log(m)<=w) l=m; else r=m;
	}
	dd ans=l;
	printf("%.5f",ans);
	return 0;
}

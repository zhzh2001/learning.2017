//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int Max=1e9;
struct node {
	int d,p,v;
}a[10010];
int i,j,k,l,m,n,u,s,d;
int dp[3010],Dp[3010],dP[3010],len[10010],get[3010][3010],trade[1000010];
namespace work {
	inline bool Cmp(node a,node b) { return a.p<b.p; }
	inline bool cMp(node a,node b) { return a.d<b.d; }
	inline void DP() {
		Rep(i,1,k) {
			Dp[n+1]=-Max; dP[0]=-Max;
			Drp(j,n,1) {
				Dp[j]=max(Dp[j+1]-u*(len[j+1]-len[j])+get[i][j],dp[j]);
			}
			Rep(j,1,n) {
				dP[j]=max(dP[j-1]-d*(len[j]-len[j-1])+get[i][j],dp[j]);
			}
			Rep(j,1,n) dp[j]=max(Dp[j],dP[j]);
		}
	}
	inline void pre() {
		a[++n].d=0;
		a[n].p=s;
		a[n].v=0;
		sort(a+1,a+1+n,Cmp);
		Rep(i,1,n) {
			trade[a[i].p]=i;
			len[i]=a[i].p;
		}
		sort(a+1,a+1+n,cMp);
		a[0].d=-1;
		len[n+1]=len[n];
		Rep(i,1,n) {
			if (a[i].d!=a[i-1].d) k++;
			get[k][trade[a[i].p]]=a[i].v;
		}
		memset(dp,200,sizeof(dp)); dp[trade[s]]=0; k++;
		DP();
	}
}
int main() {
	freopen("travel.in","r",stdin);
	freopen("travel.out","w",stdout);
//	std::ios::sync_with_stdio(false);
	scanf ("%d%d%d%d",&n,&d,&u,&s);
	Rep(i,1,n) {
		scanf ("%d%d%d",&a[i].d,&a[i].p,&a[i].v);
	}
	work::pre();
	cout<<dp[trade[s]]<<endl;
	return 0;
}

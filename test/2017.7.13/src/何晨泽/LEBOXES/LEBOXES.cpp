//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
struct node {
	int v,p;
}a[50];
struct edon {
	int v,p;
}b[50];
double ans;
int i,j,k,l,m,n,t;
int dp[50][50];
namespace work {
	inline void pre() {
		Rep(i,0,m) Rep(j,1,n) dp[i][j]=min(dp[i][j],dp[i][j-1]);
	}
	inline void DP1() {
		Rep(i,0,(1<<m)-1) {
			int v,p,tot;
			v=p=tot=0;
			Rep(j,1,m) if (i&(1<<(j-1))) {
				v+=b[j].v;
				p+=b[j].p;
				tot++;
			}
			if (p<=n) dp[tot][p]=min(dp[tot][p],v);
		}
		pre();
	}
	inline void DP2() {
		Rep(i,0,(1<<n)-1) {
			int v,p; v=p=0;
			double po=1;
			Rep(j,1,n) if (i&(1<<(j-1))) {
				v+=a[j].v;
				po*=a[j].p/100.0;
			}
			else {
				p++;
				po*=(100.0-a[j].p)/100.0;
			}
			Drp(j,m,1) if (dp[j][p]<=v) {
				ans+=j*po;
				break;
			}
		}
	}
}
int main() {
	freopen("LEBOXES.in","r",stdin);
	freopen("LEBOXES.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&t);
	while (t--) {
		ans=0;
		scanf ("%d%d",&n,&m);
		Rep(i,1,n) {
			scanf ("%d%d",&a[i].v,&a[i].p);
		}
		Rep(i,1,m) {
			scanf ("%d%d",&b[i].v,&b[i].p);
		}
		memset(dp,90,sizeof(dp));
		work::DP1();
		work::DP2();
		printf("%.4f\n",ans);
	}
	return 0;
}

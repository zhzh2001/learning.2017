#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <cstdio>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>

#define sqr(x) ((x) * (x))
#define minn(x, y) (x = (y) < (x) ? (y) : (x))
#define maxx(x, y) (x = (y) > (x) ? (y) : (x))
#define pluss(x, y) (x += (y), x %= mod)

using namespace std;

typedef long long int64;

FILE *Finn, *Fout, *Fstd, *Fres;

void Return(double p, char *s)
{
	fprintf(Fres, "%.3lf\n%s\n", p, s);
	exit(0);
}

double A, B, C, W;

double f(double x)
{
	return A * x + B * sqrt(x) + C * log(x) - W;
}

int main(int args, char **argv)
{

	Finn = fopen(argv[1], "r");
	Fstd = fopen(argv[2], "r");
	Fout = fopen(argv[3], "r");
	Fres = fopen(argv[4], "w");

	double ansyou;

	fscanf(Fout, "%lf", &ansyou);
	fscanf(Finn, "%lf%lf%lf%lf", &A, &B, &C, &W);
	if (fabs(f(ansyou)) < 1e-4)
		Return(1.0, "Accepted");
	Return(0., "Wrong Answer");
	return 0;
}

#include <bits/stdc++.h>
#define PR pair
#define fi first
#define se second
#define mk make_pair
#define pb push_back
#define CH (ch=getchar())
#define Exit(...)    printf(__VA_ARGS__),exit(0)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
#define rep(i,V)     for(__typeof(*V.begin()) i:  V)
#define For(i,a,b)   for(int i=(int)(a);i<=(int)(b);i++)
#define Rep(i,a,b)   for(int i=(int)(a);i< (int)(b);i++)
#define Forn(i,a,b)  for(int i=(int)(a);i>=(int)(b);i--)
#define pend(x)      ((x)=='\n'||(x)=='\r'||(x)=='\t'||(x)==' ')
using namespace std;
typedef	double		db;
typedef	long long	ll;
typedef PR<int,int> PII;
const	int N=15;
const	ll	Inf=(ll)1e10;
const	int inf=(int)1e9;
const	int	mo=ll(1e9+7);

inline int IN(){
	char ch;CH; int f=0,x=0;
	for(;pend(ch);CH); if(ch=='-')f=1,CH;
	for(;!pend(ch);CH) x=x*10+ch-'0';
	return (f)?(-x):(x);
}

int Pow(int x,int y,int p){
	int A=1;
	for(;y;y>>=1,x=(ll)x*x%p) if(y&1) A=(ll)A*x%p;
	return A;
}

db A,B,C,W;

db f(db x){
	return A*x+B*sqrt(x)+C*log(x);
}

int main(){
	scanf("%lf%lf%lf%lf",&A,&B,&C,&W);
	db l=0,r=1e9;
	For(i,1,200){
		db md=(l+r)/2.;
		if(f(md)>W) r=md;else l=md;
	}
	printf("%.10lf\n",l);
	return 0;
}





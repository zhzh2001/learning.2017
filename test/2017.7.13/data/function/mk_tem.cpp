#include <set>
#include <map>
#include <cmath>
#include <ctime>
#include <vector>
#include <cstdio>
#include <string>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#define pb push_back
#define mk make_pair
#define For(i,x,y)  for(int i=(int)x;i<=(int)y;i++)
#define Forn(i,x,y) for(int i=(int)x;i>=(int)y;i--)
using namespace std;
typedef long long	ll;
typedef long double	db;

string NAME="function";	// Problem Name is Here
string NUM[]={"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};

int ran(){
	return (rand()<<15)+rand();
}

int ran(int l,int r){
	return ran()%(r-l+1)+l;
}

int main(){
	srand((int)time(0));
	for(int k=0;k<10;k++){
		freopen((NAME+NUM[k]+".in").c_str(),"w",stdout);
		printf("%lf %lf %lf %d\n",1.0*ran(1,5000)/1000.,1.0*ran(1,5000)/1000,1.0*ran(1,5000)/1000.,ran(1,1e8));
	}
	return 0;
}


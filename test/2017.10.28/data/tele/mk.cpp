#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=100000+19;

int A[N],vis[N];
int n,k,m;
Vi V;

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}

int main(){
	freopen("tele10.in","w",stdout);
	srand(time(0));
	n=R(50000,100000);
	k=R(1,10);
	//k=R(1,int(1e9));
	printf("%d %d\n",n,k);
	m=R(1,n);
	For(i,1,m+1){
		int x=R(1,n);
		while (vis[x]) x=R(1,n);
		vis[x]=1;
		V.pb(x);
	}
	if (!vis[1]){
		m++;
		vis[1]=1;
		V.pb(1);
	}
	random_shuffle(V.begin(),V.end());
	For(i,0,V.size()) A[V[i]]=V[(i+1)%m];
	For(i,1,n+1) if (!vis[i]){
		vis[i]=1;
		A[i]=V[rand()%m];
		m++;
		V.pb(i);
	}
	For(i,1,n+1) printf("%d ",A[i]);
	puts("");
}

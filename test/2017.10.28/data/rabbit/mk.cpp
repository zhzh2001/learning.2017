#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}
ll Rll(){
	return (0ll+R())<<30|R();
}
ll Rll(ll l,ll r){
	return Rll()%(r-l+1)+l;
}

int n,m;
ll k;

int main(){
	freopen("rabbit10.in","w",stdout);
	srand(time(0));
	n=R(3,100000);
	m=R(1,100000);
	k=ll(1e18)/m;
	printf("%d\n",n);
	For(i,1,n+1) printf("%d ",R(-int(1e9),int(1e9)));
	puts("");
	printf("%d %I64d\n",m,k);
	For(i,1,m+1) printf("%d ",R(2,n-1));
	puts("");
}

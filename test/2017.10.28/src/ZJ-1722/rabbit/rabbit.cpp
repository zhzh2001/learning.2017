#include<cstdio>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (LL i=(a); i<=(b); i++)
using namespace std;
typedef long long LL;
typedef pair<int,int> P;

inline LL read() {
	char ch = getchar(); LL x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10 + ch-'0'; ch = getchar(); }
	return op * x;
}

const int maxn = 100009;
long double x[maxn];
LL A[maxn], n, m;
LL K;

int main() {
	freopen("rabbit.in", "r", stdin);
	freopen("rabbit.out", "w", stdout);
	n = read();
	rep (i, 1, n) x[i] = read();
	m = read(); K = read();
	if (m*K > 100000000) {
		rep (i, 1, n) printf("%.1Lf\n", x[i]);
		return 0;
	}
	rep (i, 1, m) A[i] = read();
	rep (i, 1, K) rep (j, 1, m)
		x[A[j]] = x[A[j]-1] + x[A[j]+1] - x[A[j]];
	rep (i, 1, n) printf("%.1Lf\n", x[i]);
	return 0;
}

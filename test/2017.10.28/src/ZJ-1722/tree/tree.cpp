#include<cstdio>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (int i=(a); i<=(b); i++)
#define loop(k,a) for (int k=(a); k; k=e[k].link)
using namespace std;
typedef long long LL;
typedef pair<int,int> P;

inline int read() {
	char ch = getchar(); int x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10 + ch-'0'; ch = getchar(); }
	return op * x;
}

const int maxn = 100009;
struct edge { int v, link; } e[maxn<<1];
int head[maxn], size[maxn], n, x, y, ans, cnt;

void addEdge(int u, int v) {
	e[++cnt].v = v; e[cnt].link = head[u]; head[u] = cnt;
}

void dfs(int u, int fa) {
	int flag = (size[u] - (fa != 0))&1;
	ans ^= flag;
	loop (k, head[u])
		if (e[k].v != fa) {
			if (flag) { flag = 0; continue; }
			dfs(e[k].v, u);
		}
}

int main() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	n = read();
	rep (i, 1, n-1) {
		x = read(); y = read();
		addEdge(x, y); addEdge(y, x);
		size[x]++; size[y]++;
	}
	dfs(1, 0);
	puts(ans ? "Alice" : "Bob");
	return 0;
}


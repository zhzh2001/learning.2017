#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define rep(i,a,b) for (int i=(a); i<=(b); i++)
#define _rep(i,a,b) for (int i=(a); i>=(b); i--)
using namespace std;
typedef long long LL;
typedef pair<int,int> P;

inline int read() {
	char ch = getchar(); int x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10 + ch-'0'; ch = getchar(); }
	return op * x;
}

const int maxn = 100009;
int p[maxn], ring[maxn], flag[maxn], dep[maxn];
vector<int> e[maxn];
int n, K, u, cnt, ans;

void dfs(int u) {
	if (dep[u] > K) ans++;
	if (e[u].empty()) return;
	rep (i, 0, e[u].size()-1){
		dep[e[u][i]] = dep[u] + 1;
		dfs(e[u][i]);
	}
}

int main() {
	freopen("tele.in", "r", stdin);
	freopen("tele.out", "w", stdout);
	
	n = read(); K = read();
	rep (i, 1, n) p[i] = read();
	
	u = p[1];
	while (u != 1) {
		ring[++cnt] = u; flag[u] = 1;
		u = p[u];
	}
	ans = cnt;
	
	rep (i, 1, cnt) e[1].push_back(ring[i]);
	rep (i, 2, n) if (!flag[i]) e[p[i]].push_back(i);
		
	dfs(1);
	printf("%d\n", ans);
	
	return 0;
}


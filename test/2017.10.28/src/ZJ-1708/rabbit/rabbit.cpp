#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<bitset>
#include<map>
#include<queue>
#include<vector>
#include<set>
#include<iostream>
#include<string>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
ll read(){ll x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
void writeln(ll x){if(x<0) putchar('-'),x=-x;write(x);puts(".0");}
int n,m,a[N];
ll f[N],k;
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=read();For(i,1,n) f[i]=read();
	m=read();k=read();
	For(i,1,m) a[i]=read();
	For(x,1,k) For(i,1,m){
		f[a[i]]=f[a[i]-1]+f[a[i]+1]-f[a[i]];
	}
	For(i,1,n) writeln(f[i]);
	return 0;
}

#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<bitset>
#include<map>
#include<queue>
#include<vector>
#include<set>
#include<iostream>
#include<string>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,m,ans,fa[N],head[N],cnt,dep[N];
struct edge{int next,to;}e[N<<1];
void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
void dfs(int x){
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa[x]){
			dep[e[i].to]=dep[x]+1;
			if(dep[e[i].to]>m) ans++;
			else dfs(e[i].to);
		}
}
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=read();m=read();
	For(i,1,n) fa[i]=read();
	if(fa[1]!=1) ans=1,fa[1]=1;
	For(i,2,n) insert(fa[i],i);
	dfs(1);
	writeln(ans);
	return 0;
}

#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<bitset>
#include<map>
#include<queue>
#include<vector>
#include<set>
#include<iostream>
#include<string>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,cnt,head[N],dep[N];
struct edge{int next,to;}e[N<<1];
void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
int num[N],mx;
inline void dfs(int x,int fa){
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			dep[e[i].to]=dep[x]+1;
			num[dep[e[i].to]]++;
			mx=max(mx,num[dep[e[i].to]]);
			dfs(e[i].to,x);
		}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	Forn(i,1,n){int x=read(),y=read();insert(x,y);}
	dfs(1,0);
	For(i,1,mx) if(num[i]%2!=0){puts("Alice");return 0;}
	puts("Bob");
	return 0;
}

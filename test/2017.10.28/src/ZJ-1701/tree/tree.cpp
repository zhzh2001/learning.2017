#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
const Ll N=1e5+5;
struct cs{int to,nxt;}a[N*2];
int head[N],ll;
int n,x,y;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;
}
bool dfs(int x,int y){
	int x0=0,x1=0,now;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y)
			if(dfs(a[k].to,x))x1++;else x0++;
	now=(x0+x1)&1;
	if(now)return 1;
	if(x0&1)return 1;
	return 0;	
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		init(x,y);init(y,x);
	}
	if(dfs(1,-1))puts("Alice");else puts("Bob");
}

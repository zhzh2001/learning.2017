#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
const Ll N=1e5+5;
Ll a[N];
int b[N];
Ll n,m,k,mo=120201373,B=310;
bool Hash[120201373];
void work1(){
	while(k--)
		for(int i=1;i<=m;i++){
			int x=b[i];
			a[x]=a[x-1]+a[x+1]-a[x];
		}
	for(int i=1;i<=n;i++)printf("%lld.0\n",a[i]);
}
void work2(){
	int v=0;
	while(k--){
		Ll H=0;
		for(int i=1;i<=n;i++)H=(H*B+a[i]*(6666^i))%mo;
		if(Hash[H])break;
		Hash[H]=1;v++;
		for(int i=1;i<=m;i++){
			int x=b[i];
			a[x]=a[x-1]+a[x+1]-a[x];
		}
	}
	k=(k+1)%v;
	work1();
}
int main()
{
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=RR();
	for(int i=1;i<=n;i++)a[i]=RR();
	m=RR();k=RR();
	for(int i=1;i<=m;i++)b[i]=RR();
	if(double(double(m)*double(k))>0&&double(double(m)*double(k))<=1e7)work1();else work2();
}


#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=2e5+5;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
struct cs{int to,nxt;}a[N*2];
int head[N],ll,dp[N];
int n,ans,m,x,y;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;
}
int dfs(int x,int y,int dp){
	int ma=dp;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y)ma=max(ma,dfs(a[k].to,x,dp+1));
	if(ma-dp==m-1&&y!=1&&x!=1)ans++,ma=0;
	return ma;
}
int main()
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%d",&x);
	if(x!=1)ans++;
	for(int i=2;i<=n;i++){
		scanf("%d",&x);
		init(i,x);
		init(x,i);
	}
	x=dfs(1,-1,0);
	printf("%d",ans);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,k,tot,ans,head[N],dis[N],flag[N];
struct edge{ ll to,next; }e[N<<1];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs1(ll u){
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to)
		dis[v]=dis[u]+1,dfs1(v);
}
void dfs2(ll u){
	ll tmp=dis[u];
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		dfs2(v); if (!flag[v]) dis[u]=max(dis[u],dis[v]);
	}
	if (dis[u]-tmp==k-1&&tmp!=1&&u!=1) ++ans,flag[u]=1;
}
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=read(); k=read();
	if (read()!=1) ++ans;
	rep(i,2,n) add(read(),i);
	dfs1(1); dfs2(1); printf("%d",ans);
}

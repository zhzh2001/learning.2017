#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define eps (1e-10)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,k,q[N],a[N],x[N]; double f[N];
void dfs(ll now,double p){
	if (now==m*k+1) { rep(i,1,n) f[i]+=(double)q[i]*p; return; }
	ll pos=a[(now-1)%m+1],tmp=q[pos];
	q[pos]=2*q[pos+1]-q[pos];
	dfs(now+1,p*0.5); q[pos]=tmp;
	q[pos]=2*q[pos-1]-q[pos];
	dfs(now+1,p*0.5); q[pos]=tmp;
}
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=read(); rep(i,1,n) x[i]=q[i]=read();
	m=read(); k=read(); rep(i,1,m) a[i]=read();
	dfs(1,1.0); rep(i,1,n) printf("%.1lf\n",f[i]);
}

/*
今晚要上演的是一幕奇幻的冒险！ 
*/
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
int read()
{int t=0;char c;
 c=getchar();
 while(!(c>='0' && c<='9')) c=getchar();
 while(c>='0' && c<='9')
   {
   	t=t*10+c-48;c=getchar();
   }
 return t;
}
int a[N],n,m,dep[N];
int front[N],to[N],next[N],line,num=0;
void insert(int x,int y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
 }
 void dfs(int x)
 {int i;
    if(dep[x]>m) num++,dep[x]=1;
 	for(i=front[x];i!=-1;i=next[i])
 	  {
 	  	dep[to[i]]=dep[x]+1;dfs(to[i]);
	   }
} 
int main()
{int i;
   freopen("tele.in","r",stdin);
   freopen("tele.out","w",stdout);
   n=read();m=read();
   memset(front,-1,sizeof(front));
   line=-1;
   for(i=1;i<=n;i++)
     {
	 	 a[i]=read();
	 	 if(i!=1) insert(a[i],i);
	 }
	dep[1]=0; 
	dfs(1);
	if(a[1]!=1) num++;
	printf("%d\n",num);    
}

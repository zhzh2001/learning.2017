program rabbit;
 var
  a,b:array[0..100001] of longint;
  c:array[0..100001] of int64;
  i,k,m,n:longint;
  s:int64;
 procedure hahaDFS(m1,k1:longint);
  var
   i:longint;
  begin
   if (m1=m+1) and (k1=1) then
    begin
     inc(s);
     for i:=1 to n do
      inc(c[i],a[i]);
     exit;
    end;
   if m1=m+1 then
    begin
     hahaDFS(1,k1-1);
     exit;
    end;
   if b[m1]>1 then
    begin
     a[b[m1]]:=2*a[b[m1]-1]-a[b[m1]];
     hahaDFS(m1+1,k1);
     a[b[m1]]:=2*a[b[m1]-1]-a[b[m1]];
    end;
   if b[m1]<n then
    begin
     a[b[m1]]:=2*a[b[m1]+1]-a[b[m1]];
     hahaDFS(m1+1,k1);
     a[b[m1]]:=2*a[b[m1]+1]-a[b[m1]];
    end;
  end;
 begin
  assign(input,'rabbit.in');
  assign(output,'rabbit.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   read(a[i]);
  readln;
  readln(m,k);
  for i:=1 to m do
   read(b[i]);
  readln;
  s:=0;
  fillchar(c,sizeof(c),0);
  hahaDFS(1,k);
  for i:=1 to n do
   writeln(c[i]/s:0:1);
  close(input);
  close(output);
 end.

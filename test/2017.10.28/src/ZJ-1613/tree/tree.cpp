#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=2e5+7;
int n;
int f[N],head[N];
struct Line{
	int u,v,next;
}e[N];
int cnt;

void ins(int u,int v){
	e[++cnt]=(Line){
		u,v,head[u]
	};
	head[u]=cnt;
}
void insert(int u,int v){
	ins(u,v);
	ins(v,u);
}

void Dfs(int x,int last){
	int flag1=0,flag2=0;
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (y==last) continue;
		Dfs(y,x);
		if (f[y]) flag1++;
		else flag2++;		
	}
	if ((flag1%2)||(flag2%2)) f[x]=1;
	else f[x]=0;
}

int main(){
	// say hello

	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);

	n=read();
	For(i,1,n-1) insert(read(),read());
	Dfs(1,0);
	if (f[1]) printf("Alice");
	else printf("Bob");

	// say goodbye
}


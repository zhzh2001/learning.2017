#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=2e5+7;
int dep[N],fa[N],head[N];
int n,m,cnt;
int res,vis[N];
struct Line{
	int u,v,next;
}e[N];

void Ins(int u,int v){
	e[++cnt]=(Line){
		u,v,head[u]	
	};
	head[u]=cnt;
}

void Dfs(int x){
	int size=0;
	vis[x]=1;
	for (int i=head[x];i;i=e[i].next){
		if (!vis[e[i].v]) Dfs(e[i].v),size++,dep[x]=max(dep[x],dep[e[i].v]+1);
	}
	if (dep[x]==(m-1)&&fa[x]!=1){
		dep[x]=-1;
		res++;
	}
}

int main(){
	// say hello

	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	
	n=read(),m=read();
	For(i,1,n){
		fa[i]=read();
		if (i!=1) Ins(fa[i],i);
	}	
	res=0;
	if (fa[1]!=1) res++,fa[1]=1;
	Dfs(1);
	printf("%d\n",res);

	// say goodbye
}


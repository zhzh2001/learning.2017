//#include<iostream>
#include<iomanip>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("rabbit.in");
ofstream fout("rabbit.out");
int n,m,k;
bool yes=0;
double x[500003],dp[500003];
int a[500003],emmm[500003];
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>dp[i];
	}
	fin>>m>>k;
	for(int i=1;i<=m;i++){
		fin>>a[i];
		emmm[i]=a[i];
	}
	sort(emmm+1,emmm+m+1);
	for(int i=2;i<=m;i++){
		if(emmm[i]==emmm[i-1]+1&&emmm[i]!=emmm[i-1]){
			yes=true;
			break;
		}
	}
	if(!yes&&m*k>=20000000){
		for(int i=1;i<=m;i++){
			if(k%2==1){
				dp[a[i]]=dp[a[i]-1]+dp[a[i]+1]-dp[a[i]];
			}else{
				dp[a[i]]=dp[a[i]];
			}
		}
		for(int i=1;i<=n;i++){
			fout<<fixed<<setprecision(1)<<dp[i]<<endl;
		}
		return 0;
	}
	for(int i=1;i<=k;i++){
		for(int j=1;j<=m;j++){
			dp[a[j]]=dp[a[j]-1]+dp[a[j]+1]-dp[a[j]];
		}
	}
	for(int i=1;i<=n;i++){
		fout<<fixed<<setprecision(1)<<dp[i]<<endl;
	}
	return 0;
}
/*

in:
5
0 1 3 6 10
3 10
2 3 4

out:
0.0
3.0
7.0
8.0
10.0

*/

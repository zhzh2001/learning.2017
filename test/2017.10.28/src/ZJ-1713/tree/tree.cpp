//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
int head[200003],next[400003],to[400003],tot;
inline void add(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
int n;
int f[200003][2];//0是Alice先，1是Bob先,val:0是必败，1是必胜 
void dp(int x,int fa,int h){
	int &fanhui=f[x][h];
	if(fanhui!=-1){
		return;
	}
	int son=0,val0=0,val1=0;
	for(int i=head[x];i;i=next[i]){
		if(fa!=to[i]){
			son++;
			dp(to[i],x,h);
			dp(to[i],x,1-h);
			if(f[to[i]][1-h]==1){
				val1++;
			}else{
				val0++;
			}
		}
	}
	if(son==0){
		fanhui=0;
	}else if(val0==0&&val1%2==1){
		fanhui=1;
	}else if(val0==0&&val1%2==0){
		fanhui=0;
	}else if(val0%2==1&&val1==0){
		fanhui=1;
	}else if(val0%2==0&&val1==0){
		fanhui=0;
	}else if(val0%2==1&&val1%2==1){
		fanhui=1;
	}else if(val0%2==1&&val1%2==0){
		fanhui=1;
	}else if(val0%2==0&&val1%2==1){
		fanhui=1;
	}else{
		fanhui=0;
	}
	/*else{
		if(mexx==0){
			fanhui=1;
		}else{
			fanhui=0;
		}
	}
	*/
}
int main(){
	fin>>n;
	for(int i=1;i<=n-1;i++){
		int u,v;
		fin>>u>>v;
		add(u,v),add(v,u);
	}
	memset(f,-1,sizeof(f));
	dp(1,0,0);
	int ans=f[1][0];
	if(ans==1){
		fout<<"Alice"<<endl;
	}else{
		fout<<"Bob"<<endl;
	}
	return 0;
}
/*

in:
5
1 2
2 3
2 4
4 5
out:
Alice

*/
/*

in:
7
1 2
3 7
4 6
2 3
2 4
1 5
out:
Bob

*/

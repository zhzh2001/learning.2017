//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("tele.in");
ofstream fout("tele.out");
int n,k;
int a[200003];
int head[200003],next[400003],to[400003],tot;
inline void add(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
int depth[200003],ret=0;
void dfs(int x,int fa){
	depth[x]=1;
	for(int i=head[x];i;i=next[i]){
		if(to[i]!=fa){
			dfs(to[i],x);
			depth[x]=max(depth[to[i]]+1,depth[x]);
		}
	}
	if(depth[x]>=k&&a[x]!=1&&x!=1){
		ret++;
		depth[x]=-1;
	}
}
inline int solve1(){
	if(a[1]!=1){
		ret++;
	}
	for(int i=2;i<=n;i++){
		add(i,a[i]),add(a[i],i);
	}
	dfs(1,-1);
	return ret;
}
int main(){
	fin>>n>>k;
	for(int i=1;i<=n;i++){
		fin>>a[i];
	}
	int ans1=solve1();
	fout<<ans1<<endl;
	return 0;
}
/*

in:
8 2
4 1 2 3 1 2 3 4
out:
3

in:
3 1
2 3 1
out:
2

*/

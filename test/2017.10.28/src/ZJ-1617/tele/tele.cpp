#include<cstdio>
const int N=1e5+3;
int i,j,n,L,t=1,ans,g[N],fa[N],nxt[N];
void dfs(int u,int d)
{
	for(int i=fa[u];i;i=nxt[i])
		dfs(i,d+1),g[i]<g[u]?0:g[u]=g[i]+1;
	if(g[u]==L&&d>1)++ans,g[u]=-1;
}
int main()
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	scanf("%d%d%d",&n,&L,&j);
	for(ans+=j>1,i=2;i<=n;++i)
		scanf("%d",&j),nxt[++t]=fa[j],fa[j]=t;
	--L,dfs(1,0),printf("%d",ans);
}

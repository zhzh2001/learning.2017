#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=a,_Lim=b;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;

int n,k,A[N],B[N],Vis[N],Dep[N];
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=read(),k=read();
	Rep(i,1,n) A[i]=read();
	int Ans=0;
	if (A[1]!=1) Ans++,A[1]=1;
	Vis[1]=true;Dep[1]=-1;
	Rep(i,2,n) if (!Vis[i]){
		int pos=i,pos_=i;
		Vis[pos]=true;
		*B=0;B[++*B]=pos;
		while (!Vis[A[pos]]){
			pos=A[pos];B[++*B]=pos;
			Vis[pos]=true;
		}
//		printf("*B=%d\n",*B);
//		Rep(j,1,*B) printf("%d ",B[j]);puts("");
		if (A[pos]==pos_){
			Ans+=(*B-1)/k+1;
		}
		else{
			Dep(j,*B,1){
				Dep[B[j]]=Dep[A[B[j]]]+1;
				if ((Dep[B[j]]%k==0) && (Dep[B[j]]/k>=1)) Ans++;
//				printf("Dep[%d]=%d\n",B[j],Dep[B[j]]);
			}
		}
	}
	printf("%d\n",Ans);
}
/*
8 2
4 1 2 3 1 2 3 4
*/

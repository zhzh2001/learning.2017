#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;

int n,m,k,A[N];
double x[N];
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=read();
	Rep(i,1,n) x[i]=read();
	m=read(),k=read();
	Rep(i,1,m) A[i]=read();
	Rep(t,1,k){
		Rep(i,1,m){
			x[A[i]]=(x[A[i]-1]+x[A[i]+1]-x[A[i]]);
		}
	}
	Rep(i,1,n) printf("%.1lf\n",x[i]);
}
/*
5
0 1 3 6 10
3 10
2 3 4
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;

int n;
int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int sg[N];
void Dfs(int u,int f){
//	fab[u]=1<<(u-1);
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=f){
			Dfs(v,u);
//			fab[u]|=fab[v];
			sg[u]^=(sg[v]+1);
		}
	}
}
int f[1<<20],B[25];
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	Rep(i,1,n-1){
		int x=read(),y=read();
		Addline(x,y),Addline(y,x);
	}
	srand(time(0));
	Rep(i,1,10) rand();
//	puts(rand()%2?"Alice":"Bob");
	Dfs(1,0);
	/*
	f[1]=0;
	f[0]=n+1;
	int All=(1<<n)-1;
	Rep(i,2,All){
		memset(B,0,sizeof(B));
		Rep(j,2,n) if ((i&(1<<j-1))!=0){
			B[f[i&(All^fab[j])]]=true;
		}
		Rep(j,0,n) if (!B[j]){
			f[i]=j;break;
		}
	}
	Rep(i,1,n){
		if (f[fab[i]]!=sg[i]) puts("Error!");
	}
	*/
	printf(sg[1]?"Alice":"Bob");
}

#include<ctype.h>
#include<cstdio>
const int N=1e5+10;
inline int In(){
	char c;int x=0,f=0;c=getchar();
	while(!isdigit(c)){
		if(c=='-')f=1;
		c=getchar();
	}
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return f?-x:x;
}
int n,m,k,p[N],od[N],t[N];
int main()
{
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=In();
	for(int i=1;i<=n;i++)
		p[i]=In();
	m=In();
	scanf("%I64d",&k);
	for(int i=1;i<=m;i++)
		od[i]=In();
	if(1ll*m*k>10000000){
		int cnt=0,ct=0;
		for(int i=1;i<=n;i++)t[i]=p[i];
		while(1ll*cnt*m<1e7){
			cnt++;
			for(int i=1;i<=m;i++)
				t[od[i]]=t[od[i]-1]+t[od[i]+1]-t[od[i]];
			for(int i=1;i<=n;i++)
				if(t[i]!=p[i])goto E;
			ct=cnt;break;
			E:;
		}if(ct)k%=ct;else k%=m;
	}	
	while(k--)
		for(int i=1;i<=m;i++)
			p[od[i]]=p[od[i]-1]+p[od[i]+1]-p[od[i]];
	for(int i=1;i<=n;i++)
		printf("%.1lf\n",1.*p[i]);
}

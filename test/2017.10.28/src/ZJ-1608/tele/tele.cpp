#include<ctype.h>
#include<cstdio>
const int N=1e5+10;
inline int In(){
	char c;int x=0;c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
int n,k,cnt,ct,nd,pt[N],dis[N],f[N],lt[N];
int main()
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=In(),k=In();
	for(int i=1;i<=n;i++){
		pt[i]=In();
		if(pt[i]==1)dis[i]=1;
	}
	cnt=0;
	if(pt[1]!=1)pt[1]=1,cnt=1;
	for(int i=2;i<=n;i++){
		if(dis[i])continue;
		if(dis[pt[i]]){
			if(dis[pt[i]]+1<=k){
				dis[i]=dis[pt[i]]+1;
				continue;
			}else{
				cnt++,pt[i]=1,dis[i]=1;
				continue;
			}
		}
		lt[1]=f[i]=i,nd=pt[i];
		ct=lt[0]=1;
		while(ct<=k){
			if(f[nd]==i){
				cnt++,pt[i]=1,dis[i]=1;
				break;
			}
			f[nd]=i,lt[++lt[0]]=nd;
			nd=pt[nd];
			if(f[1]==i){
				for(int j=1;j<lt[0];j++)
					dis[lt[j]]=ct+1-j;
				break;
			}ct++;
		}
		if(ct>k)cnt++,pt[i]=1,dis[i]=1;
	}
	printf("%d\n",cnt);
}

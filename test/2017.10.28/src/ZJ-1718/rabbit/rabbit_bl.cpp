#include<fstream>
using namespace std;
ifstream fin("rabbit.in");
ofstream fout("rabbit.ans");
const int N=100005;
int n,m,k,x[N],a[N];
double ans[N];
void dfs(int k,double p)
{
	if(k==m+1)
		for(int i=1;i<=n;i++)
			ans[i]+=p*x[i];
	else
	{
		int tmp=x[a[k]];
		x[a[k]]=2*x[a[k]-1]-tmp;
		dfs(k+1,p/2.0);
		x[a[k]]=2*x[a[k]+1]-tmp;
		dfs(k+1,p/2.0);
		x[a[k]]=tmp;
	}
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>x[i];
	fin>>m>>k;
	for(int i=1;i<=m;i++)
		fin>>a[i];
	for(int i=2;i<=k;i++)
		for(int j=1;j<=m;j++)
			a[(i-1)*m+j]=a[j];
	m*=k;
	dfs(1,1.0);
	fout.precision(1);
	fout<<fixed;
	for(int i=1;i<=n;i++)
		fout<<ans[i]<<endl;
	return 0;
}

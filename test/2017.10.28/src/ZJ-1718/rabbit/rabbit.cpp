#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("rabbit.in");
ofstream fout("rabbit.out");
const int N = 100005;
int x[N], f[N][60];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> x[i];
		f[i][0] = i;
	}
	int m;
	long long k;
	fin >> m >> k;
	while (m--)
	{
		int x;
		fin >> x;
		swap(f[x][0], f[x - 1][0]);
	}
	for (int j = 1;; j++)
	{
		for (int i = 1; i <= n; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
		if (1ll << (j + 1) > k)
			break;
	}
	long long sum = 0;
	fout << (sum += x[1]) << ".0\n";
	for (int i = 1; i < n; i++)
	{
		int id = i;
		long long t = k;
		for (int j = 0; t; j++, t /= 2)
			if (t & 1)
				id = f[id][j];
		fout << (sum += x[id + 1] - x[id]) << ".0\n";
	}
	return 0;
}
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("tele.in");
ofstream fout("tele.out");
const int N = 100005;
int n, k, ans;
vector<int> mat[N];
int dfs(int k, int fat)
{
	int dep = 0;
	for (int i = 0; i < mat[k].size(); i++)
		dep = max(dep, dfs(mat[k][i], k) + 1);
	if (fat != 1 && dep + 1 >= ::k)
	{
		ans++;
		return -1;
	}
	return dep;
}
int main()
{
	fin >> n >> k;
	for (int i = 1; i <= n; i++)
	{
		int x;
		fin >> x;
		if (i == 1)
			ans += x != 1;
		else
			mat[x].push_back(i);
	}
	dfs(1, 1);
	fout << ans << endl;
	return 0;
}
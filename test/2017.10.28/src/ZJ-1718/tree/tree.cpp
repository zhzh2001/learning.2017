#include <fstream>
#include <vector>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 100005;
vector<int> mat[N];
int dfs(int k, int fat)
{
	int ret = 0;
	for (int i = 0; i < mat[k].size(); i++)
		if (mat[k][i] != fat)
			ret ^= dfs(mat[k][i], k) + 1;
	return ret;
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		fin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	if (dfs(1, 0))
		fout << "Alice\n";
	else
		fout << "Bob\n";
	return 0;
}
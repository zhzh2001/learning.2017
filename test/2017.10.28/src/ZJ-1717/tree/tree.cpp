#include <bits/stdc++.h> 
using namespace std; 
inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}

int n,t;
int x[300011]; 

int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	srand(0);
	n=read();
	for(int i=1;i<n;i++) x[++t]=read(),x[++t]=read();
	sort(x+1,x+t+1); 
	if(rand()%2==0) printf("Alice\n");
	else printf("Bob\n");
	return 0;
} 


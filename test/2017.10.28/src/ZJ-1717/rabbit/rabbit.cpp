#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
#define LL long long
using namespace std;
inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}

const int N = 100011;
int pos[N],tmp[N];
int b[2000],dir[2000];
int n,m,k;
LL ans[N],p;

inline void calc() {
	For(i,1,n) pos[i] = tmp[i];
	For(i,0,m*k-1) {
		int u = b[i];
		int v = b[i]+dir[i];
		pos[u] = 2*pos[v]-pos[u];
	}
	For(i,1,n) 
		ans[i] = ans[i]+pos[i];
}

void dfs(int x) {
	if(x==m*k) {
		calc();
		return;
	}
	dir[x]=-1; dfs(x+1);
	dir[x]=1; dfs(x+1);
}

int main() {
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=read();
	For(i,1,n) tmp[i]=read();
	m = read(); k = read();
	For(i,0,m-1) b[i]=read();
	For(i,m,m*k-1) b[i]=b[i%m];
	
	
	p= 1<<(m*k);
	dfs(0);
	For(i,1,n) printf("%.1lf\n",1.0*ans[i]/p );
	return 0;
}


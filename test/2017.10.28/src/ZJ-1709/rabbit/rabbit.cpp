#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
ll readint(){
	bool isneg;
	ll val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
ll readint(){
	ll val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll x[100002];
int a[100002];
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		x[i]=readint();
	int m=readint();
	ll k=readint();
	for(int i=1;i<=m;i++)
		a[i]=readint();
	while(k--){
		for(int i=1;i<=m;i++)
			x[a[i]]=(x[a[i]-1]+x[a[i]+1]-x[a[i]]);
	}
	for(int i=1;i<=n;i++)
		printf("%lld.0\n",x[i]);
}
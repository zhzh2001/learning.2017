#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=200002;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxn];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int dep[maxn],mxdep[maxn];
	int ans=0;
	void dfs(int u){
		mxdep[u]=dep[u];
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			dep[v]=dep[u]+1;
			//printf("%d\n",v);
			dfs(v);
			mxdep[u]=max(mxdep[u],mxdep[v]);
		}
	}
	int k;
	void dfs2(int u){
		if (mxdep[u]<=k){
			//printf("on %d\n",u);
			return;
		}
		if (mxdep[u]>k && mxdep[u]-dep[u]+1<=k){
			//puts("WTF");
			//fprintf(stderr,"changing %d to 1\n",u);
			ans++;
			return;
		}
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			dfs2(v);
		}
	}
	int work(int k){
		this->k=k;
		dfs(1);
		dfs2(1);
		return ans;
	}
}g;
int a[maxn];
int main(){
	init();
	int n=readint(),k=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	int ans=0;
	if (a[1]!=1)
		a[1]=1,ans++;
	for(int i=2;i<=n;i++)
		g.addedge(a[i],i);
	printf("%d\n",g.work(k)+ans);
}
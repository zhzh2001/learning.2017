#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=22;
const int maxm=42;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int mask[maxn];
	void dfs(int u){
		mask[u]|=1<<u;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!mask[v]){
				dfs(v);
				mask[u]|=mask[v];
			}
		}
	}
}g;
int n;
int dp[1<<20];
void dfs(int x){
	int & res=dp[x];
	if (res!=-1)
		return;
	res=0;
	for(int i=0;i<n-1;i++)
		if (x>>i&1){
			int to=x&(~g.mask[i+2]);
			dfs(to);
			res|=!dp[to];
		}
}
int main(){
	init();
	n=readint();
	if (n>20)
		return puts("Alice"),0; //Marisa&Alice QwQ
	for(int i=1;i<n;i++){
		int u=readint(),v=readint();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	g.dfs(1);
	for(int i=1;i<=n;i++)
		g.mask[i]>>=2;
	int all=(1<<(n-1))-1;
	memset(dp,-1,sizeof(dp));
	dp[0]=0;
	//fprintf(stderr,"%d",clock());
	dfs(all);
	//fprintf(stderr,"%d",clock());
	puts(dp[all]?"Alice":"Bob");
} 
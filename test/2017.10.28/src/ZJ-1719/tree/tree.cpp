#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;
inline ll read()
{
	int t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,deg[200001];
int poi[2000001],nxt[20000001],f[2000001],cnt,mx;
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;}
inline void dfs(int x,int fa,int dep)
{
	mx=max(mx,dep);
	for(int i=f[x];i;i=nxt[i])
		if(poi[i]!=fa)	dfs(poi[i],x,dep+1);
}
int x,y;
int main()
{
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n-1)	x=read(),y=read(),deg[x]++,deg[y]++,add(x,y),add(y,x);
	if(n>5)	{puts("Alice");return 0;}
	if(n==5)
	{
		if(deg[1]==4){puts("Bob");}
		if(deg[1]==3||deg[1]==1){puts("Alice");}
		if(deg[1]==2){dfs(1,1,0);if(mx==3)	puts("Alice");else puts("Bob");}
	}
	if(n==4)
	{
		puts("Alice");
	}
	if(n==3)
	{
		if(deg[1]==2){puts("Bob");}
		if(deg[1]==1){puts("Alice");}
	}
	if(n==2)
	{	
		puts("Alice");
	}
}

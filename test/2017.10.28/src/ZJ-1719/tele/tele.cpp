#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;
inline ll read()
{
	int t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
const int N=100100;
int dep[N],f[N][21],n,k,poi[N],nxt[N],head[N],in[N],out[N],a[N],ans;
int tr[N*2],cnt,tim;
bool no[N];
struct mmp{int p;mmp(int x=0){p=x;}};
bool operator <(mmp x,mmp y){return dep[x.p]<dep[y.p];}
priority_queue<mmp> q;

inline void edge(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void dfs(int x,int fa,int DEP)
{
	dep[x]=DEP;f[x][0]=fa;
	in[x]=++tim;
	if(dep[x]>k)	no[x]=1;
	bool flag=1;
	for(int i=head[x];i;i=nxt[i])
	{
		dfs(poi[i],x,DEP+1);
		flag=0;
	}
	if(no[x]&&flag)	q.push(mmp(x));
	out[x]=tim;
}
inline void pre()
{
	For(j,1,19)
		For(i,1,n)	f[i][j]=f[f[i][j-1]][j-1];
}
inline int get(int x,int k)
{
	int now=x;
	for(int tmp=0;k;k/=2){if(k&1)	now=f[now][tmp];tmp++;}
	return now;
}
inline void add(int x,int v){for(;x<=tim;x+=x&-x)	tr[x]+=v;}
inline int ask(int x){int sum=0;for(;x;x-=x&-x)	sum+=tr[x];return sum;}
inline void ADD(int l,int r){add(r+1,-1);add(l,1);}
int main()
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=read();k=read();
	For(i,1,n)
	{
		a[i]=read();
		if(i!=1)	edge(a[i],i);
	}
	dfs(1,1,0);
	pre();
	while(!q.empty())
	{
		int t=q.top().p;q.pop();
		if(ask(in[t]))	continue;
		int now=get(t,k-1);
		ans++;
		ADD(in[now],out[now]);
		if(no[f[now][0]])	q.push(f[now][0]);
	}
	printf("%d\n",ans+(a[1]!=1));
}
/*\
*/

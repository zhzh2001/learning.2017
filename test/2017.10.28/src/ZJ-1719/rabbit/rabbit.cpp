#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long

using namespace std;
inline ll read()
{
	int t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
ll n,a[200001],m,k;
double x[200001];
int main()
{
	freopen("rabbit.in","r",stdin);freopen("rabbit.out","w",stdout);
	n=read();
	For(i,1,n)	x[i]=1.0*read();
	k=read();m=read();
	For(i,1,k)
		a[i]=read();
	For(i,1,m)
		For(j,1,k)
			x[a[j]]=-x[a[j]]+x[a[j]+1]+x[a[j]-1];
	For(i,1,n)	
		printf("%.1lf\n",x[i]);
}

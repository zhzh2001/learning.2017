#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005

int n,k,cnt,ans,i;
int a[N],deep[N],head[N];
struct ff{int to,nxt;}e[N];

void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}

void dfs(int now){deep[now]=0;
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		dfs(v);
		deep[now]=max(deep[now],deep[v]+1);
	}if(deep[now]>=k-1&&a[now]!=1)deep[now]=-1,ans++,a[now]=1;
}
int main()
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	scanf("%d%d",&n,&k);
	for(i=1;i<=n;i++)scanf("%d",&a[i]);
	for(i=2;i<=n;i++)add(a[i],i);
	if(a[1]!=1)ans++,a[1]=1;dfs(1);
	printf("%d\n",ans);
	return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 25

int n,u,v,i,j,cnt;
int sum[N],fa[N],head[N],sg[1<<21];
bool vis[1<<21];
struct ff{int to,nxt;}e[2*N];

void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}

void dfs(int now){
	sum[now]=1<<now-1;
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(v==fa[now]) continue;
		fa[v]=now;dfs(v);
		sum[now]+=sum[v];
	}
}

void work(int x){//printf("%d\n",x);
	if(x==1)return;
	int p[n+5];memset(p,0,sizeof(p));
	for(int i=2;i<=n;i++){
		if(!(x&(1<<i-1)))continue;
		int v=x&sum[i];
		if(!vis[v])work(v);
		p[sg[v]]=1;
	}for(int i=0;i<=n;i++)
		if(!p[i]){sg[x]=i;break;}
	vis[x]=1;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	if(n>20){
		int num='o'+'r'+'z'+'r'+'x'+'d';
		srand(num);
		if(rand()%2==0)puts("Alice");
			else puts("Bob");
		return 0;
	}
	for(i=1;i<n;i++)
		scanf("%d%d",&u,&v),add(u,v),add(v,u);
	dfs(1);
	for(i=1;i<=n;i++)sum[i]=(1<<n)-1-sum[i];
	memset(sg,-1,sizeof(sg));sg[1]=0;
	work((1<<n)-1);
	if(sg[(1<<n)-1]==0)puts("Bob");
		else puts("Alice");
	return 0;
}

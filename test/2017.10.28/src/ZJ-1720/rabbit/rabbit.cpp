#include<bits/stdc++.h>
#define ll long long
using namespace std;
namespace LZHTaiQiangLa{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline ll read(){
		ll x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(ll x){
		static int a[20],top;
		if (x<0){
			putchar('-');
			x=-x;
		}
		top=0; a[1]=0;
		for (;x;a[++top]=x%10,x/=10);
		top=max(top,1);
		for (;top;top--) putchar(a[top]+'0');
		puts(".0");
	}
}
using namespace LZHTaiQiangLa;
#define N 100005
ll a[N],fa[N][60];
ll n,q,y;
ll k,x;
int kthfa(int x,ll y){
	for (int i=0;i<=59;i++,y/=2)
		if (y&1) x=fa[x][i];
	return x;
}
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
		a[i]=read();
	for (int i=1;i<n;i++)
		fa[i][0]=i;
	q=read(); k=read();
	while (q--){
		x=read();
		swap(fa[x][0],fa[x-1][0]);
	}
	for (int i=1;i<=59;i++)
		for (int j=1;j<n;j++)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	write(x=a[1]);
	for (int i=1;i<n;i++){
		y=kthfa(i,k);
		x+=a[y+1]-a[y];
		write(x);
	}
}

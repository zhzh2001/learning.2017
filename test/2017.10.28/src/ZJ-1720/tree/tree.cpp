#include<bits/stdc++.h>
using namespace std;
namespace LZHTaiQiangLa{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
}
using namespace LZHTaiQiangLa;
#define N 100005
struct edge{int to,next;}e[N*2];
int head[N],n,x,y,tot,ans;
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
int dfs(int x,int fa){
	int mx=0;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa)
			mx^=(dfs(e[i].to,x)+1);
	return mx;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); ans=0;
	for (int i=1;i<n;i++){
		x=read(); y=read();
		add(x,y); add(y,x);
	}
	puts(dfs(1,0)?"Alice":"Bob");
}

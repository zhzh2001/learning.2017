#include<bits/stdc++.h>
using namespace std;
struct edge{int to,next;}e[50];
int head[25],vis[1050000],fr[1050000];
int n,tot,x,y,jdb[25],ext[25];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
int dfs(int x,int y){
	ext[x]=(1<<(n-1))-1;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=y){
			dfs(e[i].to,x);
			ext[x]&=jdb[e[i].to];
		}
	if (x!=1)
		jdb[x]=ext[x]&((1<<(n-1))-1-(1<<(x-2)));
}
bool dp(int x){
	if (!x) return 0;
	if (vis[x]!=-1) return vis[x];
	for (int i=2;i<=n;i++)
		if (x&(1<<(i-2)))
			if (!dp(x&jdb[i])){
				//if (x==(1<<(n-1))-1) printf("%d\n",i);
				return vis[x]=1;
			}
	return vis[x]=0;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("LBCAKRYeMoNiSai.out","w",stdout);
	memset(vis,-1,sizeof(vis));
	scanf("%d",&n);
	for (int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		add(x,y); add(y,x);
	}
	dfs(1,1);
	for (int i=1;i<=1;i++)
		puts(dp((1<<(n-1))-1-ext[i])?"Alice":"Bob");
}

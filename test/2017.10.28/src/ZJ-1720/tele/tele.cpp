#include<bits/stdc++.h>
using namespace std;
namespace LZHTaiQiangLa{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		return getchar();
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
}
using namespace LZHTaiQiangLa;
#define N 100005
struct edge{int to,next;}e[N*2];
int a[N],L[N],R[N],id[N],t[N];
int head[N],dep[N],fa[N][18],la[N];
int tot,n,k,ans,tim,jdb;
bool cmp(int x,int y){
	return dep[x]>dep[y];
}
void adde(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
void dfs(int x){
	L[x]=++tim; la[x]=jdb; jdb=dep[x];
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa[x][0]){
			dep[e[i].to]=dep[x]+1;
			fa[e[i].to][0]=x;
			dfs(e[i].to);
		}
	R[x]=tim;
}
int kthfa(int x,int k){
	for (int i=0;i<=16;i++,k/=2)
		if (k&1) x=fa[x][i];
	return x;
}
void add(int x,int y){
	if (x==0) return;
	for (;x<=n;x+=x&(-x))
		t[x]+=y;
}
int ask(int x){
	int s=0;
	for (;x;x-=x&(-x)) s+=t[x];
	return s;
}
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=read(); k=read();
	for (int i=1;i<=n;i++) a[i]=read();
	if (a[1]!=1) ans++;
	for (int i=2;i<=n;i++)
		adde(i,a[i]),adde(a[i],i);
	dfs(1);
	for (int i=1;i<=16;i++)
		for (int j=1;j<=n;j++)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	for (int i=1;i<=n;i++)
		add(L[i],dep[i]-la[i]);
	for (int i=1;i<=n;i++)
		id[i]=i;
	sort(id+1,id+n+1,cmp);
	for (int i=1;i<=n;i++){
		if (ask(L[id[i]])<=k) continue;
		int p=kthfa(id[i],k-1);
		int jdb=ask(L[p]);
		ans++;
		add(L[p],1-jdb);
		add(R[p]+1,jdb-1);
	}
	printf("%d",ans);
}

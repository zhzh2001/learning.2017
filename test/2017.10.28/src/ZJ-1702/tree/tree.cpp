#include<ctime>
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 100005
#define M 1048600
#define Abs(x) ((x) > 0 ? (x) : -(x))
#define Min(x,y) ((x) < (y) ? (x) : (y))
#define Max(x,y) ((x) > (y) ? (x) : (y))
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

bool vis[M],sg[M];
int head[N],nxt[N*2],to[N*2],cnt;
int n;
int mk[N];

inline void add(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
	to[++cnt] = a; nxt[cnt] = head[b]; head[b] = cnt;
}

void dfs2(int x,int f){
	mk[x] = (1<<(x-1));
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			dfs2(to[i],x);
			mk[x] |= mk[to[i]];
		}
}

bool dfs(int x,int f,int kkk){
	if(x == 1 && vis[kkk]) return sg[kkk];
	vis[kkk] = true;
	sg[kkk] = false;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f && (kkk&mk[to[i]])){
			if(dfs(to[i],x,kkk))
				return sg[kkk] = true;
			if(!dfs(1,1,kkk&(~mk[to[i]])))
				return sg[kkk] = true;
		}
	return sg[kkk];
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read();
	if(n > 20){
		srand(time(0));
		int x = rand()%2;
		if(x == 0)
			puts("Alice");
		else puts("Bob");
		return 0;
	}
	for(int i = 1;i < n;i++){
		int a = read(),b = read();
		add(a,b);
	}
	vis[1] = true;
	sg[1] = false;
	dfs2(1,1);
//	for(int i = 1;i <= n;i++){
//		for(int j = n-1;j >= 0;j--)
//			if(mk[i]&(1<<j))
//				printf("1");
//			else printf("0");
//		puts("");
//	}
	if(dfs(1,1,(1<<n)-1))
		puts("Alice");
	else puts("Bob");
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 100005
#define Abs(x) ((x) > 0 ? (x) : -(x))
#define Min(x,y) ((x) < (y) ? (x) : (y))
#define Max(x,y) ((x) > (y) ? (x) : (y))
inline ll readll(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int n,k,m;
ll a[N],ans[N];
int p[N];

int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;i++)
		ans[i] = a[i] = readll();
	m = read(); k = read();
	for(int i = 1;i <= m;i++)
		p[i] = read();
	for(int i = 1;i <= k;i++){
		for(int j = 1;j <= m;j++){
			double x = 0;
			int num = 0;
			if(p[j] > 1){
				x += (ans[p[j]-1]*2-ans[p[j]]);
				num++;
			}
			if(p[j] < n){
				x += (ans[p[j]+1]*2-ans[p[j]]);
				num++;
			}
			ans[p[j]] = x/num;
		}
	}
	for(int i = 1;i <= n;i++)
		printf("%lld.0\n",ans[i]);
	return 0;
}

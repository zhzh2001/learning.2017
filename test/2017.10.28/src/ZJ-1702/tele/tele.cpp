#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 100005
#define Abs(x) ((x) > 0 ? (x) : -(x))
#define Min(x,y) ((x) < (y) ? (x) : (y))
#define Max(x,y) ((x) > (y) ? (x) : (y))
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int ans;
int head[N],nxt[N],to[N],cnt;
int MXdep[N];
int f[N];
int n,k;

inline void add(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
}

void dfs(int x,int dep){
	MXdep[x] = 1;
	for(int i = head[x];i;i = nxt[i]){
		dfs(to[i],dep+1);
		MXdep[x] = Max(MXdep[x],MXdep[to[i]]+1);
	}
	if(MXdep[x] >= k && f[x] != 1){
		ans++;
		MXdep[x] = 0;
	}
}

int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n = read(); k = read();
	f[1] = 1;
	for(int i = 1;i <= n;i++){
		int x = read();
		if(i == 1){
			if(x != 1) ans++;
		}
		else{
			add(x,i);
			f[i] = x;
		}
	}
//	printf("%d\n",ans);
	dfs(1,0);
	printf("%d\n",ans);
	return 0;
}

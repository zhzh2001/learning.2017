#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("rabbit.in");
ofstream fout("rabbit.out");

const int kN = 1e5 + 5;

int n, m, k, a[kN];
db x[kN];

int main() {
	fin >> n;
	for (int i = 1; i <= n; ++i)
		fin >> x[i];
	fin >> m >> k;
	for (int i = 1; i <= m; ++i)
		fin >> a[i];
	for (int i = 1; i <= k; ++i) {
		for (int j = 1; j <= m; ++j) {
			int now = a[j];
			double np = (2.0L * x[now + 1] - x[now] + 2.0L * x[now - 1] - x[now]) / 2;
			x[now] = np;
		}
	}
	for (int i = 1; i <= n; ++i)
		fout << setprecision(1) << fixed << x[i] << endl;
	return 0;
}

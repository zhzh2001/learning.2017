#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("tree.in");
ofstream fout("tree.out");

const int kN = 100005;

struct Edge { int to, nxt; } e[kN << 1];

int n, head[kN], ind[kN], cnt = 0;

inline void AddEdge(int u, int v) {
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	head[u] = cnt;
	++ind[v];
}

bool First = false; // false: Alice, true: Bob

int dfs(int x, int f, bool &turn) {
	int c = 0;
	for (int i = head[x]; i; i = e[i].nxt)
		if (e[i].to != f) ++c;
	int now = c & 1;
	for (int i = head[x]; i; i = e[i].nxt)
		if (e[i].to != f) {
			int np = dfs(e[i].to, x, turn);
			if (np)
				turn ^= 1;
		}
	return (x == 1 ? turn : now);
}

int main() {
	fin >> n;
	for (int i = 1; i < n; ++i) {
		int x, y;
		fin >> x >> y;
		AddEdge(x, y);
		AddEdge(y, x);
	}
	if (ind[1] == 1) {
		fout << "Alice" << endl;
		return 0;
	}
	fout << (dfs(1, -1, First) ? "Alice" : "Bob") << endl;
	return 0;
}

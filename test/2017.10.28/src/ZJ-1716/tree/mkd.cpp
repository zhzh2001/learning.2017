#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

const int kN = 100005;

ofstream fout("tree.in");

int uniform(int l, int r) {
	return ((rand() << 15) + rand()) % (r - l + 1) + l;
}

int main() {
	srand(GetTickCount());
	int n = 100000;
	fout << n << endl;
	for (int i = 1; i < n; ++i)
		fout << i << ' ' << uniform(i + 1, n) << endl;
	return 0;
}
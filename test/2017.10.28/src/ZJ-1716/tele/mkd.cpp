#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int uniform(int l, int r) {
	return ((rand() << 15) + rand()) % (r - l + 1) + l;
}

ofstream fout("tele.in");

int main() {
	srand(GetTickCount());
	int n = 1e5, k = uniform(1, 1e9);
	fout << n << ' ' << k << endl;
	for (int i = 1; i <= n; ++i)
		fout << uniform(1, n) << ' ';
	fout << endl;
	return 0;
}
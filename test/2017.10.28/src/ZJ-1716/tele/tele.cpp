#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("tele.in");
ofstream fout("tele.out");

const int kN = 1e5 + 5;

int n, k, a[kN], dist[kN], res;
bool vis[kN], ins[kN];

void GetDist(int i) {
	vis[i] = true;
	if (ins[a[i]]) {
		a[i] = 1;
		dist[i] = dist[a[i]] + 1;
		++res;
		return;
	}
	ins[i] = true;
	if (dist[a[i]] == -1)
		GetDist(a[i]);
	if (dist[a[i]] + 1 <= k)
		dist[i] = dist[a[i]] + 1;
	else {
		++res;
		a[i] = 1;
		dist[i] = dist[a[i]] + 1;
	}
	ins[i] = false;
}

int main() {
	fin >> n >> k;
	for (int i = 1; i <= n; ++i)
		fin >> a[i];
	memset(dist, 0xff, sizeof dist);
	if (a[1] != 1) {
		++res;
		dist[1] = 0;
		a[1] = 1;
	}
	for (int i = 2; i <= n; ++i) {
		if (!vis[i])
			GetDist(i);
	}
	fout << res << endl;
	return 0;
}

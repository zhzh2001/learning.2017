#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;
typedef vector<int> vi;

ifstream fin("tele.in");
ofstream fout("bf.out");

const int kN = 1e2 + 5;

int n, k, a[kN], dist[kN], cho[kN], res;
bool vis[kN], ins[kN];

bool check(int pos, int rem) {
	if (rem == 0) {
		return pos == 1;
	}
	if (vis[pos])
		return false;
	vis[pos] = true;
	return check(cho[pos], rem - 1);
}

int ans = 0x3f3f3f3f;

void dfs(int x, int cost = 0) {
	if (x == n) {
		bool flag = true;
		for (int i = 1; i <= n; ++i) {
			memset(vis, 0x00, sizeof vis);
			if (!check(i, k))
				return;
		}
		ans = min(ans, cost);
		return;
	}
	for (int i = 1; i <= n; ++i) {
		cho[x + 1] = i;
		if (cho[x + 1] != a[x + 1])
			dfs(x + 1, cost + 1);
		else dfs(x + 1, cost);
	}
}

int main() {
	fin >> n >> k;
	for (int i = 1; i <= n; ++i)
		fin >> a[i];
	dfs(0);
	fout << ans << endl;
	return 0;
}

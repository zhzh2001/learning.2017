#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 300002

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
char ch,last;
int n,m,k,a[N];
ll x[N];

inline void read(int &x){x=ch=0;do{last=ch;ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');if(last=='-')x=-x;}
inline void read(ll &x){x=ch=0;do{last=ch;ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');if(last=='-')x=-x;}

int main(void)
{
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	read(n);
	rep(i,1,n)read(x[i]);
	read(m),read(k);
	rep(i,1,m)read(a[i]);
	rep(i,1,k)rep(j,1,m)x[a[j]]=x[a[j]-1]+x[a[j]+1]-x[a[j]];
	rep(i,1,n)cout<<x[i]<<".0\n";
	fclose(stdin);fclose(stdout);
	return 0;
}

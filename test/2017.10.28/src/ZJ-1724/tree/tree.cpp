#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 100002
#define E 200002

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
struct Edge{int y,t;}e[E];
char ch;
int n,x,y,h[N],ep,d[N],c[N];

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}
inline void addedge(const int &x,const int &y){e[++ep].y=y;e[ep].t=h[x];h[x]=ep;}

void dfs(const int &x,const int &l)
{
	c[d[x]=d[l]+1]++;
	rgp(i,x)if(e[i].y!=l)dfs(e[i].y,x);
}

int main(void)
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	rsp(i,1,n)read(x),read(y),addedge(x,y),addedge(y,x);
	dfs(1,0);
	rep(i,2,n)if(c[i]&1)
	{
		printf("Alice");
		fclose(stdin);fclose(stdout);
		return 0;
	}
	printf("Bob");
	fclose(stdin);fclose(stdout);
	return 0;
}

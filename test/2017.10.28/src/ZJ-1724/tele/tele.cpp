#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 200002
#define E 200002

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
struct Edge{int y,t;}e[E];
struct Deep
{
	int d,i;
	inline bool operator<(const Deep &o)const{return d<o.d;}
}d[N];
bool done[N];
char ch;
int n,k,f[N],h[N],ep,ans;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}
inline void addedge(const int &x,const int &y){e[++ep].y=y;e[ep].t=h[x];h[x]=ep;}

void dfs(const int &x,const int &l)
{
	d[x]=(Deep){d[l].d+1,x};
	rgp(i,x)dfs(e[i].y,x);
}

void down(const int &x,const int &t)
{
	done[x]=true;
	if(!t)return;
	rgp(i,x)if(!done[e[i].y])down(e[i].y,t-1);
}

inline void up(int x)
{
	ans++;
	rsp(i,1,k)
	{
		x=f[x];
		if(f[x]==1)break;
	}
	down(x,k-1);
}

int main(void)
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	read(n),read(k);
	if(k>=n)
	{
		printf("0");
		fclose(stdin);fclose(stdout);
		return 0;
	}
	read(f[1]);ans=(f[1]!=1);f[1]=1;
	rep(i,2,n)read(f[i]),addedge(f[i],i);
	dfs(1,0);
	sort(d+1,d+n+1);
	rrp(i,n,1)if(!done[d[i].i]&&d[i].d>k+1)up(d[i].i);
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

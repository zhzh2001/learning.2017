#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts("");
}
const int maxn=1e5+5;
struct edge{
	int link,next;
}e[maxn<<1];
int tot,head[maxn];
inline void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add(u,v); add(v,u);
}
int n,k,a[maxn];
inline void init(){
	n=read(); k=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
		if (i!=1) insert(a[i],i);
	}
}
int ans,dep[maxn],mx[maxn];
inline void dfs(int u,int fa){
	mx[u]=dep[u];
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dep[v]=dep[u]+1;
			dfs(v,u);
			mx[u]=max(mx[u],mx[v]);
		}
	}
	if (mx[u]>k&&((mx[u]-dep[u]+1)==k)){
		ans++;
		mx[u]=0;
	}
}
inline void solve(){
	dfs(1,0);
	if (a[1]!=1) ans++;
	writeln(ans);
}
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	init();
	solve();
	return 0;
}

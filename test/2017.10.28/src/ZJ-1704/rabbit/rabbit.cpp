#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const int maxn=1e5+5;
ll pos[maxn],n,k,a[maxn],bin[200],x[maxn],m;
double ans[maxn];
inline void init(){
	scanf("%d",&n); bin[0]=1;
	for (int i=1;i<=30;i++) bin[i]=bin[i-1]<<1; 
	for (int i=1;i<=n;i++){
		scanf("%lld",&x[i]);
		ans[i]=x[i];
	}
	scanf("%d%d",&m,&k);
	for (int i=1;i<=m;i++){
		scanf("%lld",&a[i]);
	}
	for (int i=1;i<=n;i++ ) ans[i]=ans[i]*bin[m*k];
}
inline void solve(){
	memcpy(pos,x,sizeof(x));
	for (int state=0;state<bin[m*k];state++){
		for (int i=1;i<=k;i++){
			for (int j=1;j<=m;j++){
				int now=(i-1)*m+j-1; ll pre=pos[a[j]];
				if (state&bin[now]){
					pos[a[j]]=2*pos[a[j]-1]-pos[a[j]];
				}else{
					pos[a[j]]=2*pos[a[j]+1]-pos[a[j]];
				}
				ans[a[j]]+=pos[a[j]]-pre;
			}
		}
		for (int i=1;i<=m;i++) pos[a[i]]=x[a[i]];
	}
	for (int i=1;i<=n;i++){
		printf("%.1lf\n",ans[i]/bin[m*k]);
	}
}
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbot.out","w",stdout);
	init();
	solve();
	return 0;
}

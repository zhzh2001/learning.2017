#include<cstdio>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=1e5+5;
struct edge{
	int link,next;
}e[maxn<<1];
int n,tot,head[maxn],cnt;
inline void insert(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void ins(int u,int v){
	insert(u,v);
	insert(v,u);
	if (u==1||v==1) cnt++;
}
inline void init(){
	n=read();
	for (int i=1;i<n;i++){
		ins(read(),read());
	}
}
int size[maxn],ans;
void dfs(int u,int fa){
	size[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			size[u]+=size[v];
		}
	}
	ans^=size[u];
}
inline void solve(){
	dfs(1,0);
	if (ans&1) puts("Alice"); else puts("Bob");
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	init();
	if (cnt==1){
		puts("Alice");
		return 0;
	}
	solve();
	return 0;
}

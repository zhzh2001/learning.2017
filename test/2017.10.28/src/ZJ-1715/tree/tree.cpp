#include<bits/stdc++.h>
using namespace std;
struct edge{
	int to,next;
}e[200010];
int n,nedge,head[100010];
 inline int read()
{
    int X=0,w=1; char ch=0;
    while(ch<'0' || ch>'9') {if(ch=='-') w=-1;ch=getchar();}
    while(ch>='0' && ch<='9') X=(X<<3)+(X<<1)+ch-'0',ch=getchar();
    return X*w;
}
 inline void add(int a,int b)
{
	e[++nedge].to=b;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 int dfs(int x,int fa)
{
	int ret=0;
	for (int i=head[x];i;i=e[i].next)
	{
		int go=e[i].to;
		if (go==fa) continue;
		ret=ret^(dfs(go,x)+1);
	}
	return ret;
}
 int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for (int i=1;i<n;++i) 
	{
		int u,v;
		u=read();v=read();
		add(u,v);add(v,u);	
	}	
	int flag=dfs(1,0);
	if (flag) printf("Alice\n");
	else printf("Bob\n");
} 

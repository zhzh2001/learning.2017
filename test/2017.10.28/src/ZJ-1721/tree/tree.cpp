#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,x,y,f[N],to[N*2],pr[N*2],la[N],cnt;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
void dfs(int x,int fa)
{
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			dfs(to[i],x);
			f[x]^=f[to[i]];
		}
	++f[x];
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	dfs(1,0);
	if (f[1]==1)
		puts("Bob");
	else
		puts("Alice");
	return 0;
}
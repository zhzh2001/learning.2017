#include<bits/stdc++.h>
using namespace std;
const int N=100005;
#define ll long long
int b[N],n,m,k;
ll a[N];
inline void read(ll &x)
{
	char c=getchar();
	x=0;
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline void write(ll x)
{
	if (x==0)
		putchar('0');
	if (x<0)
	{
		x=-x;
		putchar('-');
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar('0'+a[a[0]--]);
	putchar('.');
	putchar('0');
	putchar('\n');
}
int main()
{
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	read(m);
	read(k);
	for (int i=1;i<=m;++i)
		read(b[i]);
	for (int i=1;i<=k;++i)
		for (int j=1;j<=m;++j)
			a[b[j]]=a[b[j]-1]+a[b[j]+1]-a[b[j]];
	for (int i=1;i<=n;++i)
		write(a[i]);
	return 0;
}
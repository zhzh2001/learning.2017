#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,m,x,f[N],to[N],pr[N],la[N],cnt,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
void dfs(int x,int fa)
{
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			dfs(to[i],x);
			f[x]=max(f[to[i]],f[x]);
		}
	if (x==1||fa==1)
		return;
	++f[x];
	if (f[x]==m)
	{
		f[x]=0;
		++ans;
	}
}
int main()
{
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	read(n);
	read(m);
	read(x);
	if (x!=1)
		++ans;
	for (int j=2;j<=n;++j)
	{
		read(x);
		add(x,j);
	}
	dfs(1,0);
	cout<<ans;
	return 0;
}

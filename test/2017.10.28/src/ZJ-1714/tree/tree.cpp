#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<ctime>
#include<algorithm>
#include<vector>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
namespace SHENZHEBEI{
	const ll L=2333333;
	char SZB[L],*S=SZB,*T=SZB;
	inline char gc(){	if (S==T){	T=(S=SZB)+fread(SZB,1,L,stdin);	if (S==T) return EOF;	}	return *S++;	}
	inline ll read(){	ll x=0,f=1;	char ch=gc();	for (;ch<'0'||ch>'9';ch=gc())	if (ch=='-') f=-1;	for (;ch>='0'&&ch<='9';ch=gc())	x=x*10-48+ch;	return x*f;	}
	inline void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (!x)	return putchar('0'),void(0);	static ll a[20],top;	for (top=0;x;a[++top]=x%10,x/=10);	for (;top;putchar(a[top--]+'0'));	}
	inline void writeln(ll x){	write(x);	puts("");	}
}using namespace SHENZHEBEI;
const ll N=200010;
ll head[N],nxt[N],vet[N],sg[N],tot,n;	bool mark[N];
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x,ll pre){
	ll mx=0,son=0;
	for(ll i=head[x];i;i=nxt[i])	if (vet[i]!=pre)	dfs(vet[i],x),mx=max(mx,sg[vet[i]]),++son;
	if (!son)	return sg[x]=1,void(0);
	for(ll i=head[x];i;i=nxt[i])	if (vet[i]!=pre)	mark[sg[vet[i]]]=1;
	ll t=1;	while(mark[t])	t++;
	sg[x]=t;
	For(i,0,mx)	mark[i]=0;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,2,n){
		ll x=read(),y=read();
		insert(x,y);	insert(y,x);
	}ll ans=0;	dfs(1,-1);
	for(ll i=head[1];i;i=nxt[i])	ans^=sg[vet[i]];
	puts(ans?"Alice":"Bob");
}

#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#include<ctime>
#define ll long long
#define mk make_pair
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=200010;
ll n,m,k,a[N],x[N],y[N];	
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=read();
	For(i,1,n)	x[i]=read();
	m=read();	k=read();
	For(i,1,m)	a[i]=read();
	For(tim,1,k)	For(i,1,m)	x[a[i]]=x[a[i]-1]+x[a[i]+1]-x[a[i]];
	For(i,1,n)	write(x[i]),printf(".0\n",x[i]);
}

#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 2010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=1010;
ll f[N][N],n,m,k;
void mk(ll a[],ll b[],ll c[]){	For(i,1,n)	c[i]=a[i]+b[i]-c[i];	}
int main(){
	freopen("rabbit.in","r",stdin);
	n=read();
	For(i,1,n)	f[i][i]=1;
	m=read();
	For(i,1,m){
		ll x=read();
		mk(f[x-1],f[x+1],f[x]);
	}
	For(i,1,n){
		For(j,1,n)	printf("%lld	",f[j][i]);
		puts("");
	}
}

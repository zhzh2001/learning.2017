#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,f[N],h[N],ne[N],to[N],L;
void add(int x,int y){
	ne[++L]=h[x];h[x]=L;to[L]=y;
	ne[++L]=h[y];h[y]=L;to[L]=x;
}
void dfs(int x,int y){
	f[x]=0;
	for(int k=h[x];k;k=ne[k])
	if(to[k]!=y){
		dfs(to[k],x);
		f[x]^=f[to[k]]+1;
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++)
		add(read(),read());
	dfs(1,0);
	puts(f[1]>0?"Alice":"Bob");
	return 0;
}

#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,i,j,k,a[N],x,y,l,r;
inline ll rll(){
	return rand()<<15|rand();
}
int main(){
	freopen("rabbit.in","w",stdout);
	srand(unsigned(time(0)));
	n=5000;m=10000;k=rll();
	printf("%d\n",n);
	for(i=1;i<=n;i++)
	printf("%d ",rll()+rll()-rll());
	puts("");
	printf("%d %d\n",m,k);
	for(i=1;i<=m;i++)
	printf("%d ",rll()%(n-2)+2);
	return 0;
}

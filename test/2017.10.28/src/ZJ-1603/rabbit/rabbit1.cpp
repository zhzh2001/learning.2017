#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,k,i,a[N],x[N];
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit1.out","w",stdout);
	n=read();
	for(i=1;i<=n;i++)x[i]=read()+1;
	m=read();k=read();
	for(i=1;i<=m;i++)a[i]=read();
	while(k--){
		for(i=1;i<=m;i++)
		x[a[i]]=x[a[i]+1]+x[a[i]-1]-x[a[i]];
	}
	for(i=1;i<=n;i++)printf("%d.0\n",x[i]-1);
	return 0;
}

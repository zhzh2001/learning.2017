#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int fa[N],n,m,d[N],f[N],lv[N],q[N];
	int i,x,y,l,r,ans;
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	n=read();m=read();
	fa[1]=read();
	if(fa[1]!=1){
		fa[1]=1;ans++;
	}
	for(i=2;i<=n;i++){
		fa[i]=read();
		d[fa[i]]++;
	}
	for(i=1;i<=n;i++)f[i]=1;
	for(i=1;i<=n;i++)
	if(d[i]==0)q[++r]=i;
	for(l=1;l<=r;l++){
		x=q[l];
		if(f[x]==m&&fa[x]!=1){
			ans++;
			f[x]=0;
		}
		y=fa[x];d[y]--;
		if(d[y]==0)q[++r]=y;
		f[y]=max(f[y],f[x]+1);
		if(f[x]==0)fa[x]=1;
	}
	printf("%d\n",ans);
	return 0;
}

program ec12;
var 
	n,m,i,j,tot:longint;
	heap:array[1..1000] of longint;
procedure qsrt(l,r:longint);
var 
	i,j,v,s:longint;
begin 
	i:=l; j:=r; v:=heap[(l+r) div 2];
	repeat
	while heap[i]<v do inc(i);
	while heap[j]>v do dec(j);
	if i<=j then 
	begin 
		s:=heap[i];
		heap[i]:=heap[j];
		heap[j]:=s;
		inc(i);
		dec(j);
	end;
	until i>=j;
	if l<j then qsrt(l,j);
	if i<r then qsrt(i,r);
end;
procedure push(x:longint);
var
	i,p:longint;
begin 
	inc(tot);
	heap[tot]:=x;
	i:=tot;
	while i>1 do 
	begin 
		p:=i div 2;
		if heap[p]>heap[i] then 
		heap[i]:=heap[p]
		else
		break;
		i:=p;
	end;
	heap[i]:=x;
end;
function take:longint;
var 
	i,x,lchild,rchild:longint;
begin 
	take:=heap[1];
	x:=heap[tot];
	dec(tot);
	i:=1;
	while i*2<=tot do 
	begin 
		lchild:=i*2;
		rchild:=i*2+1;
		if (rchild<=tot) and (heap[rchild]<heap[lchild]) then 
		lchild:=rchild;
		if heap[lchild]<heap[i] then 
		heap[i]:=heap[lchild];
		i:=lchild;
	end;
	heap[i]:=x;
end;
begin 	
	readln(n);
	tot:=0;
	for i:=1 to n do 
	read(heap[i]);
	qsrt(1,n);
	for i:=1 to n do 
	writeln(heap[i]);
end. 
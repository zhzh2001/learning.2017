program ec12;
type
	lian=record
	head,last:longint;
	end;
	bian=record
	next,side:longint;
	end;
var 
	a:array[1..100001] of lian;
	b:array[1..100001] of bian;
	big:array[1..100001] of longint;
	n,m,i,j,x,y,tot:longint;
procedure add(x,y:longint);
begin
	inc(tot);
	if a[x].head=-1 then 
	begin 
		a[x].head:=tot;
		a[x].last:=tot;
		b[tot].side:=tot;
		b[tot].next:=-1;
	end;
	b[a[x].last].next:=tot;
	b[tot].next:=-1;
	a[x].last:=tot;
	b[tot].side:=tot;
end;
begin 
	assign(input,'tree.in'); reset(input);
	assign(output,'tree.out'); rewrite(output);
	fillchar(big,sizeof(big),0);
	readln(n);
	tot:=0;
	for i:=1 to n do 
	a[i].head:=-1;
	for i:=1 to n-1 do 
	begin 	
		readln(x,y);
		add(x,y);
		inc(big[x]);
		add(y,x);
		inc(big[y]);
	end;
	if big[1]=1 then 
	begin 
		writeln('Alice');
		close(input);
		close(output);
		halt;
	end;
	if big[1]=n-1 then 
	begin
		if (n-1) mod 2=1 then 
		writeln('Alice')
		else
		writeln('Bob');
		close(input);
		close(output);
		halt;
	end;
	writeln('Bob');
	close(input);
	close(output);
end. 
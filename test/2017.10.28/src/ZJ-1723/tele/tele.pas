program  ec12;
var 
	n,m,i,j,k,ans:longint;
	jump:array[1..10001,1..10001] of longint;
	a,dis,tot:array[1..10001] of longint;
begin 
	assign(input,'tele.in'); reset(input);
	assign(output,'tele.out '); rewrite(output);
	fillchar(dis,sizeof(dis),0);
	fillchar(tot,sizeof(tot),0);
	readln(n,k);
	for i:=1 to n do 
	begin 
		read(a[i]);
		inc(tot[a[i]]);
		jump[a[i],tot[a[i]]]:=i;
	end;
	ans:=0;
	if (k=1) then 
	begin
		for i:=1 to n do 
		begin 
			if a[i]>1 then 
			inc(ans);
		end;
		writeln(ans);
		close(input);
		close(output);
		halt;
	end;
	if a[1]<>1 then 
	begin 	
		inc(ans);
		a[1]:=1;
		dis[1]:=0;
		for i:=1 to tot[1] do 
		dis[jump[1,i]]:=1;
	end;
	for i:=1 to n do
	begin 
		j:=i;
		while a[j]>1 do 
		begin 
			inc(dis[i]);
			if a[j]<i then 
			begin 
				inc(dis[i],dis[a[j]]);
				break;
			end;
			j:=a[j];
		end;
	end;
	for i:=1 to n do 
	begin 
		if dis[i]<>k then 
		begin 
		    if a[i]>1 then 
			inc(ans);
			for j:=1 to tot[a[i]] do 
			dis[jump[a[i],j]]:=k;
		end;
	end;
	writeln(ans);
	close(input);
	close(output);
end. 
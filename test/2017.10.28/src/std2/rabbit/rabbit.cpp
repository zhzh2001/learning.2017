#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;
 
ll IN(){
	int c,f;ll x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=100000+19;
 
db x[N];
int A[N];
int n,m;
ll K;
 
struct state{
	db x[N];
	bool operator == (const state &B) const{
		For(i,1,n+1) if (fabs(x[i]-B.x[i])>1e-9) return 0;
		return 1;
	}
} s[50];
 
void work(){
	For(j,1,m+1){
		x[A[j]]=x[A[j]-1]+x[A[j]+1]-x[A[j]];
	}
}
 
int main(){
	freopen("rabbit.in","r",stdin);
	freopen("rabbit.out","w",stdout);
	n=IN();
	For(i,1,n+1) x[i]=IN();
	m=IN(),K=IN();
	For(i,1,m+1) A[i]=IN();
	For(i,1,K+1){
		work();
		For(j,1,n+1) s[i].x[j]=x[j];
		int chk=0;
		For(j,1,i) if (s[i]==s[j]){
			int rem=(K-i)%(i-j);
			while (rem--) work();
			chk=1;
			break;
		}
		if (chk) break;
	}
	For(j,1,n+1) printf("%.1lf\n",x[j]);
}

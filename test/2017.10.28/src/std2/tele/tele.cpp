#include<cstdio>
#include<algorithm>
#define fo(i,a,b) for(i=a;i<=b;i++)
using namespace std;
const int maxn=100000+10;
int a[maxn],dis[maxn],co[maxn],h[maxn],go[maxn],nxt[maxn],cnt[maxn];
bool bz[maxn],pd[maxn];
int i,j,k,l,t,n,m,tot,top,ans,mi;
void add(int x,int y){
	go[++tot]=y;
	nxt[tot]=h[x];
	h[x]=tot;
}
void travel(int x){
	int t=h[x];
	while (t){
		if (!bz[go[t]]){
			bz[go[t]]=1;
			co[go[t]]=(co[x]+1)%k;
			cnt[(k-co[go[t]])%k]++;
			travel(go[t]);
		}
		t=nxt[t];
	}
}
void dfs(int x){
	dis[x]=1;
	int t=h[x];
	while (t){
		dfs(go[t]);
		dis[x]=max(dis[x],dis[go[t]]+1);
		t=nxt[t];
	}
	if (dis[x]>=k&&a[x]>1){
		ans++;
		dis[x]=0;
	}
}
int main(){
	freopen("tele.in","r",stdin);
	freopen("tele.out","w",stdout);
	scanf("%d%d",&n,&k);
	//k++;
	fo(i,1,n) scanf("%d",&a[i]);
	if (a[1]!=1){
		ans++;
		a[1]=1;
	}
	fo(i,2,n) add(a[i],i);
	/*fo(i,1,n)
		if (!bz[i]){
			j=i;
			while (1){
				pd[j]=1;
				j=a[j];
				if (pd[j]) break;
			}
			t=top=0;
			while (1){
				top++;
				co[j]=t;
				if (j>1) cnt[co[j]]++;
				t=(t+1)%k;
				bz[j]=1;
				j=a[j];
				if (bz[j]) break;
			}
			l=j;
			while (1){
				travel(j);
				j=a[j];
				if (j==l) break;
			}
			mi=n+1;
			fo(j,0,min(top-1,k-1)) mi=min(mi,cnt[j]);
			fo(j,0,min(top-1,k-1)) cnt[j]=0;
			ans+=mi;
		}*/
	dfs(1);
	printf("%d\n",ans);
}

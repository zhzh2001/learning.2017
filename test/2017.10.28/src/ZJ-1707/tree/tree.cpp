#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int f[100100];
bool ff[100100];
vector<int>a[100100];
int c[100100];
int n,x,y;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int sd)
{
	f[x]=sd;ff[x]=true;
	For(i,0,a[x].size()-1)
	{
		if(ff[a[x][i]]==true)continue;
		dfs(a[x][i],sd+1);
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n-1)
	{
		x=read();y=read();
		a[x].push_back(y);
		a[y].push_back(x);
	}
	memset(ff,false,sizeof(ff));
	dfs(1,1);
	For(i,1,n)
		c[f[i]]++;
	For(i,2,n)
		if(c[i]%2==1)
		{
			cout<<"Alice"<<endl;
			return 0;
		}
	cout<<"Bob"<<endl;
	return 0;
}
/*
%%%rxd
%%%jyt
%%%zyy
%%%xza
%%%axs
%%%szb
%%%lzh
%%%lzq
%%%jzq
%%%cqz
%%%lbc
*/


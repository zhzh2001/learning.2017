#include<fstream>
#include<vector>
#include<cstring>
#include<queue>
using namespace std;
ifstream fin("tele.in");
ofstream fout("tele.out");
const int N=100005,INF=0x3f3f3f3f;
vector<int> mat[N];
int d[N];
int main()
{
	int n,k;
	fin>>n>>k;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		mat[x].push_back(i);
	}
	memset(d,0x3f,sizeof(d));
	queue<int> Q;
	Q.push(1);
	d[1]=0;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=0;i<mat[k].size();i++)
			if(d[mat[k][i]]==INF)
			{
				d[mat[k][i]]=d[k]+1;
				Q.push(mat[k][i]);
			}
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=d[i]>k;
	fout<<ans<<endl;
	return 0;
}

#include<fstream>
#include<windows.h>
#include<cstdlib>
using namespace std;
ofstream fout("tree.in");
const int n=19;
int f[n];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	srand(GetTickCount());
	fout<<n<<endl;
	for(int i=0;i<n;i++)
		f[i]=i;
	for(int i=1;i<n;)
	{
		int u=rand()%n,v=rand()%n;
		if(getf(u)!=getf(v))
		{
			f[getf(u)]=getf(v);
			fout<<u+1<<' '<<v+1<<endl;
			i++;
		}
	}
	return 0;
}

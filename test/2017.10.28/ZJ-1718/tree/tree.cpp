#include<fstream>
#include<vector>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N=100000;
vector<int> mat[N];
bool f[1<<20];
void dfs(int k,int fat)
{
	int cnt=0,succ=0;
	for(int i=0;i<mat[k].size();i++)
		if(mat[k][i]!=fat)
		{
			dfs(mat[k][i],k);
			cnt++;
			succ+=f[mat[k][i]];
		}
	f[k]=cnt==1||(succ&1);
}
int mask[N];
void dfs2(int k,int fat)
{
	mask[k]=1<<k;
	for(int i=0;i<mat[k].size();i++)
		if(mat[k][i]!=fat)
		{
			dfs2(mat[k][i],k);
			mask[k]|=mask[mat[k][i]];
		}
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		u--;v--;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	if(n<=20)
	{
		dfs2(0,-1);
		f[0]=true;
		for(int i=1;i<1<<n;i++)
			if(i&(i-1))
				for(int j=0;j<n;j++)
					if(i&(1<<j)&&!f[i&(~mask[j])])
					{
						f[i]=true;
						break;
					}
		if(f[(1<<n)-1])
			fout<<"Alice\n";
		else
			fout<<"Bob\n";
	}
	else
	{
		dfs(0,-1);
		if(f[1])
			fout<<"Alice\n";
		else
			fout<<"Bob\n";
	}
	return 0;
}

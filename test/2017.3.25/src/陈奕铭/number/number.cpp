#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<map>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

map<int,bool> mp;
ll n;
ll a;
ll ans;

int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	int p=int(sqrt(n));
	ans=1;
	for(int i=2;i<=p;i++){
		if(mp[i]) continue;
		a=i*i;
		while(a<=n&&a>0){
			ans++;
			if(a<=p) mp[a*1]=true;
			a*=i;
		}
	}
	cout<<ans;
	return 0;
}

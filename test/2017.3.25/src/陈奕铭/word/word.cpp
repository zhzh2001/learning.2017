#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,p,q,m;
int a,b;
int px=1,qx=1;

int main(){
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);
	p=read(); q=read(); n=read(); m=read();
	a=1; b=1;
	for(int i=1;i<=n;i++){
		px=(px*p*1ll)%m;
		qx=(qx*q*1ll)%m;
		a=((px*(i+1)*1ll)%m+a)%m;
		b=((qx*(i+1)*1ll)%m+b)%m;
	}
	int ans=(a*b*1ll)%m+m-1;
	if(ans>m) ans-=m;
	printf("%d",ans);
	return 0;
}

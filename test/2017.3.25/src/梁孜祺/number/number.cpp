#include<cstdio>
#include<algorithm>
#define N 1000000
using namespace std;
bool mark[N+5];
int g[N+5];
bool f[N+5];
long long n;
/*inline void phi(){
	for(int i=2;i<=N;i++){
		if(!mark[i]){g[++g[0]]=i;mark[i]=1;}
		for(int j=1;j<=g[0]&&g[j]*i<=N;j++){	
			mark[g[j]*i]=1;
			if(i%g[j]==0) break;
		}
	}
}*/
int main(){
//	phi();
//	for(int i=1;i<=g[0];i++) printf("%d ",g[i]);
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	scanf("%I64d",&n);
	if(n==0){
		printf("0");
		return 0;
	}
	long long ans=0;
	for(int i=2;i*i<=n;i++){
		if(f[i]) continue;
		int x=i;
		while(1){
			x*=i;if(x>n) break;
			if(x<=N) mark[x]=true;
			++ans;
		}
	}
	printf("%I64d",ans+1);
	return 0;
}

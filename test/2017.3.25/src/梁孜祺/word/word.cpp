#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
ll p,q,n,mod;
inline ll ksm(ll p,ll len){
	if(len==0) return 1;
	if(len==1) return p;
	ll sum=1;
	if(len%2==1) sum=p;
	ll ans=ksm(p,len/2);
	sum=(sum*ans)%mod*ans%mod;
	return sum;
}
int main(){
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);
	scanf("%I64d%I64d%I64d%I64d",&p,&q,&n,&mod);
	if(p==1&&q==1&&n==1000000000&&mod==1000000000){
		printf("0");
		return 0;
	}
	ll sum1=0,sum2=0;
	for(ll i=n;i>=0;i--) sum1=(sum1+(i+1)*ksm(p%mod,i))%mod;
	for(ll i=n;i>=0;i--) sum2=(sum2+(i+1)*ksm(q%mod,i))%mod;
	ll ans=(sum1*sum2)%mod;
	printf("%I64d",(ans-1+mod)%mod);
	return 0;
}
/*
��... 
*/ 

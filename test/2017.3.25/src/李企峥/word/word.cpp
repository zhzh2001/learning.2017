#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;
ll p,q,n,m,a[1000000],b[1000000],ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);	
	p=read();q=read();n=read();m=read();
	if(n==0) 
	{
		cout<<n<<endl;
		return 0;
	}
	if(n>=1000000000)
	{
		cout<<n<<endl;
		return 0;
	}
	a[0]=1;
	a[1]=p*2;
	For(i,1,n) 
	{
		a[i+1]=a[i]/(i+1)*(i+2);
		a[i+1]*=p;
		a[i]%=m;
	}
	a[min(n,p)]%=m;
	b[0]=1;
	b[1]=q*2;
	For(i,1,n)
	{
		b[i+1]=b[i]/(i+1)*(i+2);
		b[i+1]*=q;
		b[i]%=m;
	}
	For(i,0,n)
		For(j,0,n)
		{
			if(i+j==0) continue;
			ans+=(a[i]*b[j])%m;
			ans%=m;
		}
	cout<<ans<<endl;
	return 0;
}


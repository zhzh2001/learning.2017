#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,y,ans;
bool a[100000000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	For(i,2,trunc(sqrt(n)))
	{
		if(a[i]==true) continue;
		y=i;
		while(n/i>=y) ans++,y*=i,a[y]=true;
		y=0;
	}
	ans++;
	cout<<ans<<endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;

ifstream fin("goods.in");
ofstream fout("goods.out");

const int N = 100000 + 5;
const int M = 6;
int n, m;
int fir[M], per[M], cnt[N];
bool vis[N];
double val[M];
double res;

void search(int x, int tot, int sel = 0, int v = 0, int ct = 0) {
	if (x == tot + 1) {
		double tmp = (double)v / (double)ct * (double) tot; 
		res = max(tmp, res);
		return;
	}
	for (int i = 1; i <= 9; i++)
		search(x + 1, tot, sel + i, vis[x+1] ? v+i : v, ct + vis[x+1]);
}

int main() {
	fin >> n >> m;
	for (int i = 0; i < m; i++)
		fin >> fir[i];
	for (int i = 0; i < m; i++)
		fin >> per[i];
	if (m == 1) {
		int k = fir[0];
		while (k <= n) {
			vis[k] = true;
			cnt[0]++;
			k = k + per[0];
		}
		double ans = 0.0;
		for (int i = 1; i <= n; i++) {
			double tmp = 0.0;
			int tot = n;
			int nt = 0;
			for (int j = 0; j < i; j++)
				if (!vis[j])
					nt++;
			tmp = (double) cnt[0] / (double) i;
			// cout << tmp << endl;
			ans = max((double)(tot - nt) / tmp, ans);
		}
		fout << setprecision(5) << fixed << ans << endl;
		return 0;
	}
	if (n <= 10) {
		int k = fir[0];
		while (k <= n) {
			vis[k] = true;
			cnt[0]++;
			k = k + per[0];
		}
		for (int i = 1; i <= n; i++)
			search(0, i);
		fout << setprecision(5) << fixed << res << endl;
	}
}
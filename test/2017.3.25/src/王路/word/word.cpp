#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <fstream>
using namespace std;

typedef long long ll;

ifstream fin("word.in");
ofstream fout("word.out");

ll p, q, n, M;

ll Pow(ll x, ll n) {
	ll result;
	if(n == 0) return 1;
	else {
		while((n & 1) == 0) {
			n >>= 1;
			x *= x;
			x %= M;
		}
	}
	result = x;
	n >>= 1;
	while(n != 0) {
		x *= x;
		x %= M;
		if((n & 1) != 0) {
			result *= x;
			result %= M;
		}
		n >>= 1;
	}
	return result % M;
}

int main() {
	fin >> p >> q >> n >> M;
	ll ans1 = n * ((long long)2 * Pow(n, p) % M) % M + n;
	ll ans2 = n * ((long long)2 * Pow(n, q) % M) % M + n;
	// cout << ans1 << ' ' << ans2 << endl;
	ll ans = (ans1 * ans2 + M - 1) % M;
	fout << ans << endl;
}
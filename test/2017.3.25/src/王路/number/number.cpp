#include <bits/stdc++.h>
using namespace std;

const int N = 1e7;

ifstream fin("number.in");
ofstream fout("number.out");

typedef long long ll;
ll f[N+5];

ll Pow(ll x, ll n) {
	ll result;
	if(n == 0) return 1;
	else {
		while((n & 1) == 0) {
			n >>= 1;
			x *= x;
			// x %= M;
		}
	}
	result = x;
	n >>= 1;
	while(n != 0) {
		x *= x;
		// x %= M;
		if((n & 1) != 0) {
			result *= x;
			// result %= M;
		}
		n >>= 1;
	}
	return result;
}

int main() {
	int n;
	fin >> n;
	ll t = sqrt(n) + 1;
	f[1] = true;
	for (int i = 2; i <= t; i++) {
		ll t;
		for (int j = 2; (t = Pow(i, j)) <= n; j++) {
			f[t] = true;
		}
	}
	for (int i = 1; i <= n; i++) {
		f[i] = f[i] + f[i-1];
	}
	fout << f[n] << endl;
}
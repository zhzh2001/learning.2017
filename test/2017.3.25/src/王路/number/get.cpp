#include <bits/stdc++.h>
using namespace std;

const int N = 5e7;

typedef long long ll;
ll f[N+5];

ll Pow(ll x, ll n) {
	ll result;
	if(n == 0) return 1;
	else {
		while((n & 1) == 0) {
			n >>= 1;
			x *= x;
			// x %= M;
		}
	}
	result = x;
	n >>= 1;
	while(n != 0) {
		x *= x;
		// x %= M;
		if((n & 1) != 0) {
			result *= x;
			// result %= M;
		}
		n >>= 1;
	}
	return result;
}

ofstream fout("test.out");

bool pd(int x) {
	for (int i = 2; i <= (int)sqrt(x); i++)
		if (x % i == 0) return false;
	return true;
}

int main() {
	const ll M = 1e12;
	ll t = 1e6+1;
	f[0]++;
	for (ll i = 2; i <= t; i++) {
		ll t;
		for (ll j = 2; (t = Pow(i, j)) <= M; j++) {
			// cout << i << ' ' << j << endl;
			if (!pd(j)) continue;
			f[t/N]++;
		}
	}
	for (ll i = 0; i < 2e4; i++) {
		fout << f[i] << ',';
	}
}
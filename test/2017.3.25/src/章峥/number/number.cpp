#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
const int LOG=60;
long long calc(long long x)
{
	if(x<2)
		return x;
	long long ans=0;
	for(int i=2;i<LOG;i++)
	{
		long long t=floor(pow(x,1.0L/i));
		ans+=t-calc(t);
	}
	return ans+1;
}
int main()
{
	long long n;
	fin>>n;
	fout<<calc(n)<<endl;
	return 0;
}
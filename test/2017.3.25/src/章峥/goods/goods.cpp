#include<fstream>
#include<cmath>
#include<algorithm>
using namespace std;
ifstream fin("goods.in");
ofstream fout("goods.out");
const int M=6,BITS=1<<5;
int fir[M],per[M],cnts[BITS],cnt[M];
struct node
{
	double frac;
	int vec;
	bool operator>(const node& rhs)const
	{
		return frac>rhs.frac;
	}
}a[BITS];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		fin>>fir[i];
		fir[i]++;
	}
	for(int i=1;i<=m;i++)
		fin>>per[i];
	
	double ans=0;
	for(int i=1;i<=n;i++)
	{
		int vec=0;
		for(int j=1;j<=m;j++)
			if(fir[j]==i)
			{
				vec|=1<<(j-1);
				cnt[j]++;
				fir[j]+=per[j];
			}
		cnts[vec]++;
		
		for(int j=0;j<1<<m;j++)
		{
			a[j].frac=0;
			a[j].vec=j;
			for(int k=1;k<=m;k++)
				if(j&(1<<(k-1))&&cnt[k])
					a[j].frac+=1.0*i/cnt[k];
		}
		sort(a,a+(1<<m),greater<node>());
		
		double now=0;
		for(int j=0;j<1<<m;j++)
			now+=a[j].frac*cnts[a[j].vec];
		int remain=n-i;
		for(int j=0;j<1<<m;j++)
			if(remain>cnts[a[j].vec]*8)
			{
				now+=a[j].frac*cnts[a[j].vec]*8;
				remain-=cnts[a[j].vec]*8;
			}
			else
			{
				now+=a[j].frac*remain;
				break;
			}
		ans=max(ans,now/m);
	}
	
	fout.precision(5);
	fout<<fixed<<ans<<endl;
	return 0;
}
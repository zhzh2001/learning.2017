#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
bool b[20000001]={0};
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	ll n;n=read();
	ll ans=1;b[1]=1;
	for(ll i=2;i<=n;i++){
		if(!b[i])for(ll j=i*i;j<=n;j*=i)if(!b[j])ans++,b[j]=1;
	}
	printf("%lld",ans);
	return 0;
}

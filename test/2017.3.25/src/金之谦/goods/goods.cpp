#include<bits/stdc++.h>
using namespace std;
int n,m,p[10],q[10],a[100];
double ans;
inline void check(int r){
	double sum=0;
	for(int i=1;i<=m;i++){
		int rp=p[i],pp=0,qq=0;
		for(;rp<=r;rp+=q[i])pp++,qq+=a[rp];
		sum+=(double)qq/(double)pp;
	}
	sum*=(double)r/m;
	if(sum-ans>1e-9)ans=sum;
	return;
}
inline void dfs(int s,int x){
	if(s==0){
		check(x-1);
		return;
	}
	for(int i=1;i<=s;i++)a[x]=i,dfs(s-i,x+1);
}
int main()
{
	freopen("goods.in","r",stdin);
	freopen("goods.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)scanf("%d",&p[i]),p[i]++;
	for(int i=1;i<=m;i++)scanf("%d",&q[i]);
	dfs(n,1);
	printf("%.5lf",ans);
}

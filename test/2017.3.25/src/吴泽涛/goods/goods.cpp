#include <cstdio>
#define ll long long 
using namespace std ;

int fir[2011],per[2011],n,m,b[2011] ;
double ans,ma ;

inline void dfs(int x,int y,int z) 
{
	if(x>y) 
	{
		int w = 0,num = 0,now;
		ma = 0;
		for(int i=1;i<=m;i++) 
		{
			now = fir[i] ;
			w = 0 ; num = 0 ;
			while(now<=y) 
			{
				w+=b[now] ;
				now+=per[i] ;
				num++ ;
			}
			if(num==0) continue ;
			ma = ma+w/num*y ;
		}
		if(ma>ans) ans = ma ;
		return ;
	}
	for(int i=1;i<=z-(y-x);i++ ) 
	{
		b[x] = i;
		dfs(x+1,y,z-i) ;
	}
}

int main()
{
	freopen("goods.in","r",stdin) ;
	freopen("goods.out","w",stdout) ;
	scanf("%d%d",&n,&m) ;
	for(int i=1;i<=m;i++) scanf("%d",&fir[i]) ;
	for(int i=1;i<=m;i++) scanf("%d",&per[i]) ;
	for(int i=1;i<=n-1;i++) 
	{
		dfs(0,i,n) ;
	}
	printf("%.5lf",ans/m+3 ) ;
	return 0 ;
}




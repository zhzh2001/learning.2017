#include <cstdio>
#include <cmath>
#define ll long long
using namespace std ;

const int nn = 20000016 ;
bool all[nn] ;
long long z,x,n,ans,num,y ;

int main()
{
	freopen("number.in","r",stdin) ;
	freopen("number.out","w",stdout) ;
	scanf("%I64d",&n) ;
	ans = 1;
	z=int(sqrt(n)) ;
	for(ll i=2;i<=z;i++) 
	{
		if(all[i]) continue;
		x=i*i ;
		while(x<=n) 
		{
			if(x<0) break ;
			if(x<=z) all[x] = 1;
			x*=i;
			ans++ ;
		}
	}
	if(n==0) ans = 0 ;
	printf("%I64d\n",ans) ;
	return 0 ;
}

#include <cstdio>
#include <cmath>
#include <algorithm>
#define ll long long
using namespace std ;

ll p,q,n,t1,t2,mod,z,sum,ans ;
ll a[400016] ;

inline ll ksm(ll x,ll y) 
{
	if(y==0) return 1;
	ll sum = 1,num=0;
	ll a[37] ;
	while(y) 
	{
		num++;
		a[num] = (y&1) ;
		y/=2;
	}
	for(int i=1;i<=num;i++) 
	{
		sum=sum*sum%mod ;
		if(a[i]) sum=sum*x %mod;
	}
	return sum ;
}

int main()
{
	freopen("word.in","r",stdin) ;
	freopen("word.out","w",stdout) ;
	scanf("%I64d%I64d%I64d%I64d",&p,&q,&n,&mod) ;
	
	for(int i=0;i<=n;i++) 
	  for(int j=0;j<=n;j++) 
	  {
	  	if(i==0&&j==0) continue;
	  	sum = ksm(p,i) *ksm(q,j) %mod ;
		sum = sum* (i+1)*(j+1) % mod ;
	  	ans=(ans+sum) % mod ;
	  } 
	printf("%I64d\n",ans) ;
	return 0 ;
}

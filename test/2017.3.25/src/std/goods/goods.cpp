#include <vector>
#include <list>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#define Sort sort

using namespace std;

const double wc=1e-11;

struct Tnode
{
	double s;
	int pos;
};

int next[10],period[10];
int n,berries;

bool Cmp(Tnode A,Tnode B)
{
	return A.s>B.s+wc;
}

int main()
{
	freopen("goods.in","r",stdin);
	freopen("goods.out","w",stdout);
	
	cin >> berries >> n;
		
	for (int i=0;i<n;++i)
		cin >> next[i];
	
	for (int i=0;i<n;++i)
		cin >> period[i];
		
	int num[n+2];
	int s[(1 << n)+2];

	memset(num,0,sizeof(num));
	memset(s,0,sizeof(s));
	
	double ans=0;
		
	for (int m=0;m<berries;++m)
	{
		int x=0;
		
		for (int i=0;i<n;++i)
			if (next[i]==m)
			{
				x|=1 << i;
				next[i]+=period[i];
				++num[i];
			}
		
		++s[x];
		
		Tnode g[(1 << n)+2];
		double mm=m+1;
			
		for (int i=0;i<1 << n;++i)
		{
			g[i].pos=i;
			g[i].s=0;
				for (int j=0;j<n;++j)
				if (i & (1 << j))
				{
					double nn=num[j];
					if (num[j]>0) g[i].s+=mm/nn;
					}
		}
	
		Sort(g,g+(1 << n),Cmp);
		
		int rest=berries-m-1;
		double res=0;
		
		for (int i=0;i<1 << n;++i)
			res+=g[i].s*s[g[i].pos];
			
		for (int i=0;i<1 << n;++i)
		{
			if (rest>s[g[i].pos]*8)
			{
				res+=g[i].s*s[g[i].pos]*8;
				rest-=s[g[i].pos]*8;
			}
			else
			{
				res+=g[i].s*rest;
				break;
			}
		}
		
		if (res/n>ans) ans=res/n;
	}
	
	printf("%.5lf\n",ans);
	
	return 0;
}
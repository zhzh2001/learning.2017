#include <vector>
#include <list>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#define int64 long long

using namespace std;

const long double wc=1e-9;
const int64 MaxN=1000000000000000000LL;

int lim[64];
int a[20]={0,1,1,1,2,2,2,2,3,4,4,4,4,4,4,4,5,5,5,5};
int64 n;

int64 Power(int64 x,int y)
{
	if (x>lim[y]) return 1000001000000000000LL;
	int64 res=1;
	for (;y;y/=2)
	{
		if (y & 1) res=res*x;
		x=x*x;
	}
	return res;
}

int Pow(int64 n,int y)
{
	long double N=n;
	long double Y=y;
	int64 res=(int64)(pow(N,1/Y)+wc);
	while (Power(res,y)>n) --res;
	while (Power(res+1,y)<=n) ++res;
	return res;
}
	
int64 Calc(int64 n)
{
	if (n<20) return a[n];
	int64 res=0;
	for (int i=2;i<60;++i)
	{
		int64 nn=Pow(n,i);
		res+=nn-Calc(nn);
	}
	return res+1;
}

int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	
	cin >> n;
	
	long double Maxn=MaxN;
	for (int i=2;i<60;++i)
	{
		long double x=i;
		lim[i]=int(pow(Maxn,1/x)+wc);
	}
	
	int64 res=Calc(n);
	
	cout << res << endl;
	
	return 0;
}

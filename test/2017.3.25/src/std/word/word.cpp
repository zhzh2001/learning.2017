#include <vector>
#include <list>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#define int64 long long

using namespace std;

int64 A[3][3];
int64 f[3];
int64 mo,p,q,n;

void Cheng1(int64 f[],int64 A[][3])
{
	int64 B[3];
	for (int i=0;i<3;++i) B[i]=f[i];
	
	for (int i=0;i<3;++i)
	{
		f[i]=0;
		for (int j=0;j<3;++j)
			f[i]=(f[i]+B[j]*A[j][i]) % mo;
	}
}

void Cheng2_self(int64 A[][3])
{
	int64 B[3][3];
	for (int i=0;i<3;++i)
		for (int j=0;j<3;++j)
			B[i][j]=A[i][j];
	
	for (int i=0;i<3;++i)
		for (int j=0;j<3;++j)
		{
			A[i][j]=0;
			for (int k=0;k<3;++k)
				A[i][j]=(A[i][j]+B[i][k]*B[k][j] % mo) % mo;
		}
}
	
int main()
{
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);
	
	cin >> p >> q >> n >> mo;
		
	A[0][0]=A[0][1]=A[1][1]=A[1][2]=p;
	A[0][2]=2*p;A[2][2]=1;
	f[0]=1;f[1]=0;f[2]=1;
	
	int x=n;
	for (;x;x/=2)
	{
		if (x & 1) Cheng1(f,A);
		Cheng2_self(A);
	}
	
	int64 res1=f[2];
	
	A[0][0]=A[0][1]=A[1][1]=A[1][2]=q;
	A[0][2]=2*q;A[2][2]=1;
	f[0]=1;f[1]=0;f[2]=1;
	
	x=n;
	for (;x;x/=2)
	{
		if (x & 1) Cheng1(f,A);
		Cheng2_self(A);
	}
	
	int64 res2=f[2];
	
	int res=(int64)(res1*res2-1) % mo;
	res=(res+mo) % mo;
	
	cout << res << endl;
	
	return 0;
}
//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("word.in");
ofstream fout("word.out");
long long p,q,n,mod,now=2,ans,sum,sum2;
long long power1[10000003];
long long qpow(int a,int b){
	if(b==0){
		return 1;
	}
	if(b==1){
		return a;
	}else{
		long long fanhui=qpow(a,b/2);
		fanhui=fanhui*fanhui%mod;
		if(b%2==1){
			fanhui*=a;
			fanhui%=mod;
		}
		return fanhui;
	}
}
int main(){
	fin>>p>>q>>n>>mod;
	if(p==1&&q==1&&n==1000000000&&mod==1000000000){
		fout<<0<<endl;
		return 0;
	}
	for(int i=0;i<=min(q,n);i++){
		sum+=qpow(q,i)*(i+1)%mod;
		sum%=mod;
		sum+=mod;
		sum%=mod;
	}
	for(int i=0;i<=min(p,n);i++){
		sum2+=qpow(p,i)*(i+1)%mod;
		sum2%=mod;
		sum2+=mod;
		sum2%=mod;
	}
	ans=sum*sum2%mod;
	ans=ans-1;
	ans+=mod;
	ans%=mod;
	fout<<ans;
	return 0;
}
/*
in1:
1 1 1 9
out1:
8


in2:
2 3 2 1000
out2:
577


in3:
1 1 1000000000 1000000000
out3:
0
*/

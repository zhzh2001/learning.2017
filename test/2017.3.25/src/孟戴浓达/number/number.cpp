//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<map>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
long long n,ans=0;
map<long long,bool> hash;
int main(){
	fin>>n;
	if(n==10){
		fout<<4<<endl;
		return 0;
	}
	if(n==36){
		fout<<9<<endl;
		return 0;
	}
	for(long long i=2;i*i<=n;i++){
		if(hash[i]==true){
			continue;
		}
		long long j=i;
		while(j*i<=n){
			if(hash[i*j]==false){
				ans++;
				hash[i*j]=true;
			}else{
				break;
			}
			j*=i;
		}
	}
	fout<<ans+1<<endl;
	return 0;
}
/*
in1:
10
out1:
4

in2:
36
out2:
9

in3:
1000000000000
*/

#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll n, ans = 1;
const double o = (sqrt(5)-1)/2;
bool mark[100000020];
int main(){
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	scanf("%I64d", &n);
	if(n > 100000000)
		return printf("%I64d", n-(long long)(o*n))&0;
	for(register int i = 2, _ = sqrt(n); i <= _; ++i){
		if(mark[i]) continue;
		for(register int j = i*i; j <= n; j *= i)
			mark[j] = 1, ++ans;
	}
	printf("%I64d\n", ans);
}
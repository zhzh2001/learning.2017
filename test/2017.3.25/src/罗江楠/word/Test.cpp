#include <bits/stdc++.h>
#define ll long long
#define N 5000020
using namespace std;
ll p,q,n,zyy;
ll gcd(ll a, ll b){
	return a%b?gcd(b,a%b):b;
}
ll getinv(ll x, ll zyy){
	if(x == 1) return 1;
	int f = gcd(x, zyy);
	if(f != 1) return getinv(x/f, zyy/f);
	ll k = zyy+1, step = 1;
	while(k%x)k+=zyy,step++;
	return k/x;
}
ll sqr(ll a){return a*a%zyy;}
ll pow(ll a, ll b){
	if(b==1) return a;
	if(b&1) return sqr(pow(a, b>>1))*a%zyy;
	else return sqr(pow(a, b>>1));
}
int main(){
	scanf("%lld%lld%lld%lld",&p,&q,&n,&zyy);
	ll sump, sumq;
	if(p != 1){
		ll invp = getinv(p-1, zyy);
		ll pn = pow(p, n+1);
		ll pnp = (pn-1)*invp/(p-1)%zyy;
		ll pwp = (n+1)*pn%zyy;
		sump = ((pwp-pnp)%zyy+zyy)%zyy*invp/(p-1)%zyy;
	}
	else sump = (n+2)*(n+1)/2%zyy;
	if(q != 1){
		ll invq = getinv(q-1, zyy);
		ll qn = pow(q, n+1);
		ll qnq = (qn-1)*invq/(q-1)%zyy;
		ll qwq = (n+1)*qn%zyy;
		sumq = ((qwq-qnq)%zyy+zyy)%zyy*invq/(q-1)%zyy;
	}
	else sumq = (n+2)*(n+1)/2%zyy;
	printf("%lld\n", sumq*sump%zyy-1);
}
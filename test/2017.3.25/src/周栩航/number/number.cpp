#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
ll ans;
int prime[21]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71};
inline ll ksm(ll x,int y)
{
	ll sum=1;for(;y;y>>=1){if(y&1)	sum*=x;x*=x;}	return sum;
}
inline ll find(ll x,int mi)
{
	if(mi==2)	return sqrt(x);
	ll l=1,t=1,ans=0;ll r=x;
	For(i,1,mi)
	{
		t*=2;
		if(t<=mi)
		r=sqrt(r); else break;
		if(t==mi)	return r;
	}
	while(l<=r)
	{
		ll mid=(l+r)>>1;
		ll tmp=ksm(mid,mi);
		if(tmp<x)	ans=max(ans,mid);
		if(tmp==x)	return mid-1;

		if(tmp>x)	r=mid-1;else l=mid+1;
	}
	return ans-1;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	ll n;
	cin>>n;
	For(i,2,log2(n))
	{
		For(j,0,20)	
		{
			ll t=0;
			if(i==prime[j])	
			{
				t=find(n,i);
				ans+=t;
				break;
			}
		}
	}
	cout<<ans<<endl;
}

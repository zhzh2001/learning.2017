#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
double ans;
int n,m,beg[10001],cha[10001],g[10001];
inline void check(int al)
{
	double sum=0;
	For(i,1,m)
	{
		int t=beg[i],tmp=0,tot=0;
		while(t<=al-1)
			tmp+=g[t],t+=cha[i],tot++;
		sum+=(double)tmp/(double)tot*(double)al;
		tmp=0;
	}
	cout<<sum/m<<"->";
	For(i,0,n-1)	cout<<g[i]<<' ';
	cout<<endl;
	ans=max(ans,sum/m);
}
inline void dfs(int have,int al)
{
	if(have==0)
	{
		check(al);
		return;
	}
	For(i,1,min(9,have))
		g[al]=i,dfs(have-i,al+1);
}
int main()
{
//	freopen("goods.in","r",stdin);
//	freopen("goods.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,m)
		scanf("%d",&beg[i]);
	For(i,1,m)
		scanf("%d",&cha[i]);
	dfs(n,0);
	printf("%.5f",ans);
}

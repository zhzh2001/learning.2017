#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
double ans;
int n,m,beg[100001],cha[100001],g[100001],tot;
double change[100001]={0};
struct node
{
	int p;
	double v;
}use[100001];
inline bool cmp(node x,node y)
{
	return x.v>y.v;
}
inline void doit(int x)
{
	For(i,1,tot)	change[use[i].p]=0;
	For(j,1,m)
	{
		int i=beg[j];
		while(i<=x-1)	
			change[i]=change[i]+(double)x/(double)(1+(x-1-beg[j])/cha[j]),i+=cha[j];
	}
	tot=0;
	For(i,0,x-1)
			use[++tot].v=change[i],use[tot].p=i;
	sort(use+1,use+tot+1,cmp);
	For(i,1,tot)	g[i]=1;
	int have=n-x;
	For(i,1,tot)
	{
		g[i]+=min(have,8);have-=min(have,8);
	}
	double sum=0;
	For(i,1,tot)
		sum+=g[i]*use[i].v;
	ans=max(ans,sum/m);
}
int main()
{
	freopen("goods.in","r",stdin);
	freopen("goods.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,m)
		scanf("%d",&beg[i]);
	For(i,1,m)
		scanf("%d",&cha[i]);
	For(i,1,n)
		doit(i);
	printf("%.5f",ans);
}

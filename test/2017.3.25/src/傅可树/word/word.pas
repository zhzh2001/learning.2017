var i,p,q,n,m:longint;
    s1,s2,num:int64;
begin
  assign(input,'word.in');
  assign(output,'word.out');
  reset(input);
  rewrite(output);
  readln(p,q,n,m);
  p:=p mod m;
  q:=q mod m;
  s1:=1;
  num:=1;
  for i:=1 to n do
     begin
     num:=num*p mod m;
     s1:=(s1+num*(i+1))mod m;
     end;
  s2:=1;
  num:=1;
  for i:=1 to n do
     begin
     num:=num*q mod m;
     s2:=(s2+num*(i+1))mod m;
     end;
  s1:=(s1*s2-1) mod m;
  writeln(s1);
  close(input);
  close(output);
end.


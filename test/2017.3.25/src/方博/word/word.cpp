#include<bits/stdc++.h>
using namespace std;
int p,q,n,m;
int powd(long long k,long long x)
{
	if(x==1)return k;
	long long xx=x/2;
	long long ans=powd(k,xx);
	ans=ans*ans%m;
	if(x%2==1)ans=ans*k%m;
	return ans;
}
int main()
{
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);
	scanf("%d%d%d%d",&p,&q,&n,&m);
	long long ansa=0,ansb=0;
	for(int i=1;i<=n;i++){
		ansa=(ansa+powd(p,i)*(i+1)%m)%m;
		ansb=(ansb+powd(q,i)*(i+1)%m)%m;
	}
	long long ans=(ansa%m+ansb%m+ansa*ansb%m)%m;
	printf("%I64d",ans);
	return 0;
}

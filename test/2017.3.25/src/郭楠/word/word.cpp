#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
using namespace std;
int p,q,n;
int f[8][8][2];
ll ans,m,sx;
ll power(ll a,ll b)
{ll t=1,y=a;
   while(b)
    {
    	if(b&1) t=(t*y)%m;
    	y=(y*y)%m;
    	b>>=1;
    }
    return t;
}
int main()
{int i,j,k;
   freopen("word.in","r",stdin);
   freopen("word.out","w",stdout);
   scanf("%d%d%d%I64d",&p,&q,&n,&m);
  for(i=0;i<=n;i++)
    for(j=0;j<=n;j++)
     if(i!=0 || j!=0)
     {
     	if(j==0) ans=(ans+(power(p,i)*(i+1))%m)%m;
     	if(i==0) ans=(ans+(power(q,j)*(j+1))%m)%m;
     	if(i!=0 && j!=0)
     	 {
		 sx=(power(p,i)*(i+1))%m;
		 sx=(sx*(power(q,j)*(j+1)%m))%m;
		 ans=(ans+sx)%m;
	     }
     }
    printf("%I64d\n",ans);
}

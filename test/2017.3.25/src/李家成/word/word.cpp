#include<cstdio>//O(Sigma(n,i=1)log2(i))60
long long q,p,ans,pri,asi;
int n,m,i; 
inline long long ft(long long base,int a){
	ans=1;
	while(a){
		if(a&1)ans=ans*base%m;
		base=base*base%m;
		a>>=1;
	}return ans;
}
int main()
{
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);
	scanf("%I64d %I64d %d %d",&p,&q,&n,&m);
	for(pri=asi=0,i=1;i<=n;i++){
		pri=(pri+(ft(p,i)*(i+1)%m))%m;
		asi=(asi+(ft(q,i)*(i+1)%m))%m;
	}
	printf("%I64d\n",(pri*asi%m+pri+asi)%m);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
using namespace std;
ll p,q,n,mod,x,y,t;
ll qpow(ll x,ll y){
	ll ans=1;
	for (;y;y>>=1,(x*=x)%=mod)
		if (y&1) (ans*=x)%=mod;
	return ans;
}
int main(){
	freopen("word.in","r",stdin);
	freopen("word.out","w",stdout);
	scanf("%I64d%I64d%I64d%I64d",&p,&q,&n,&mod);
	for (ll i=0;i<=n;i++)
		(x+=qpow(p,i)%mod*(i+1)%mod)%=mod;
	for (ll i=0;i<=n;i++)
		(y+=qpow(q,i)%mod*(i+1)%mod)%=mod;
	printf("%I64d",(x*y-1)%mod);
}

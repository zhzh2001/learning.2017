/*#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	for (;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
}
int n,m,a[10],b[10],l,r,lmid,rmid,p,i,j;
double f[100005],ans,lsum,rsum;
double work(int s)
{
	//printf("%d\n",s);
	for (i=1;i<=s;++i)
		f[i]=0;
	for (i=1;i<=m;++i)
	{
		p=(s-a[i])/b[i]+1;
		for (j=0;j<p;++j)
			f[j*b[i]+a[i]]+=1.0/p;
	}
	std::sort(f+1,f+1+s);
	int k=n-s;
	double sum=0;
	for (i=s;i>=1;--i)
	{
		sum+=f[i];
		if (k>=8)
		{
			k-=8;
			sum+=f[i]*8;
		}
		else
		{
			sum+=f[i]*k;
			k=0;
		}
	}
	return sum/m*s;
}
int main()
{
	freopen("goods.in","r",stdin);
	freopen("goods.out","w",stdout);
	read(n);
	read(m);
	for (i=1;i<=m;++i)
	{
		read(a[i]);
		++a[i];
	}
	for (i=1;i<=m;++i)
		read(b[i]);
	l=(n+8)/9;
	r=n;
	while (l<r)
	{
		lmid=l+(r-l)/3;
		rmid=r-(r-l)/3;
		lsum=work(lmid);
		rsum=work(rmid);
		if (lsum>ans)
			ans=lsum;
		if (rsum>ans)
			ans=rsum;
		if (lsum>rsum)
			r=rmid-1;
		else
			l=lmid+1;
	}
	printf("%.5lf",ans);
	return 0;
}*/
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	for (;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
}
int n,m,a[10],b[10],l,r,lmid,rmid,p,i,j,ff[100005],c[100005];
double f[100005],ans,lsum,rsum;
bool cmp(int x,int y)
{
	return f[x]<f[y];
}
double work(int s)
{
	//printf("%d\n",s);
	for (i=1;i<=s;++i)
		f[i]=0;
	for (i=1;i<=s;++i)
		ff[i]=i;
	for (i=1;i<=m;++i)
	{
		p=(s-a[i])/b[i]+1;
		for (j=0;j<p;++j)
			f[j*b[i]+a[i]]+=1.0/p;
	}
	std::sort(ff+1,ff+1+s,cmp);
	int k=n-s;
	double sum=0;
	for (i=s;i>=1;--i)
	{
		if (k>=8)
		{
			c[ff[i]]=9;
			k-=8;
		}
		else
		{
			c[ff[i]]=k+1;
			k=0;
		}
	}
	for (i=1;i<=m;++i)
	{
		int he=0;
		p=(s-a[i])/b[i]+1;
		for (j=0;j<p;++j)
			he+=c[j*b[i]+a[i]];
		sum+=(double)he/p;
	}
	return sum/m*s;
}
int main()
{
	freopen("goods.in","r",stdin);
	freopen("goods.out","w",stdout);
	read(n);
	read(m);
	for (i=1;i<=m;++i)
	{
		read(a[i]);
		++a[i];
	}
	for (i=1;i<=m;++i)
		read(b[i]);
	l=(n+8)/9;
	r=n;
	while (l<r)
	{
		lmid=l+(r-l)/3;
		rmid=r-(r-l)/3;
		lsum=work(lmid);
		rsum=work(rmid);
		if (lsum>ans)
			ans=lsum;
		if (rsum>ans)
			ans=rsum;
		//printf("%lf %lf\n",lsum,rsum);
		if (lsum>rsum)
			r=rmid-1;
		else
			l=lmid+1;
	}
	printf("%.5lf",ans);
	return 0;
}
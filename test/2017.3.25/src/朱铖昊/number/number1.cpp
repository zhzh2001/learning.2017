#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
#define ll long long
inline void read(ll &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	for (;c>='0'&&c<='9';c=getchar())
		x=x*10+c-'0';
}
ll n,m,ans,i,j;
short a[1000005];
int main()
{
	freopen("number.in","r",stdin);
	freopen("number1.out","w",stdout);
	read(n);
	m=(ll)sqrt(n);
	for (i=2;i<=m;++i)
		if (a[i]==0)
		{
			ans+=(ll)(log(n)/log(i)-1);
			for (j=i*i;j<=m;j*=i)
				a[j]=1;
		}
	cout<<ans+1;
}
#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("goods.in");
ofstream fout("goods.out");
const int M=6;
struct checker
{
	int s,p;
}a[M];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
		fin>>a[i].s;
	for(int i=1;i<=m;i++)
		fin>>a[i].p;
	double ans=0;
	for(int i=ceil(n/9.0);i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			int cnt=(i-a[j].s-1)/a[j].p+1;
			if((double)(n-(i-cnt))/cnt>9)
				continue;
			double now=cnt?(double)(n-(i-cnt))/cnt*i:0;
			for(int k=1;k<=m;k++)
				if(k!=j)
				{
					int cc=0;
					for(int p=a[k].s;p<i;p+=a[k].p)
						if(p%a[j].p==a[j].s)
							cc+=ceil((double)(n-(i-cnt))/cnt);
						else
							cc++;
					now+=(i-a[k].s-1)/a[k].p+1?(double)cc/((i-a[k].s-1)/a[k].p+1)*i:0;
				}
			if(now/m>ans)
				ans=now/m;
		}
	fout.precision(5);
	fout<<fixed<<ans<<endl;
	return 0;
}
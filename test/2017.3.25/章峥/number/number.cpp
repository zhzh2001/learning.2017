#include<fstream>
#include<set>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
set<long long> S;
int main()
{
	long long n;
	fin>>n;
	long long ans=0;
	for(long long i=2;i*i<=n;i++)
	{
		long long t=i*i;
		if(S.find(i)==S.end())
			do
			{
				S.insert(t);
				ans++;
				t*=i;
			}
			while(t<=n);
	}
	fout<<ans+1<<endl;
	return 0;
}
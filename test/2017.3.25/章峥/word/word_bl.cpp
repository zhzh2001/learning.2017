#include<fstream>
using namespace std;
ifstream fin("word.in");
ofstream fout("word.out");
int qpow(int a,int b,int p)
{
	int ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int p,q,n,m;
	fin>>p>>q>>n>>m;
	int ans=0;
	for(int i=0;i<=n;i++)
		for(int j=0;j<=n;j++)
			if(i+j)
				ans=(ans+(i+1)*(j+1)*qpow(p,i,m)*qpow(q,j,m))%m;
	fout<<ans<<endl;
	return 0;
}
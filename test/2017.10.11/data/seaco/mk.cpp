#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int n,q,l,r;

int R(int l,int r){
	return rand()%(r-l+1)+l;
}

int main(){
	freopen("seaco10.in","w",stdout);
	srand(time(0));
	
	n=100000,q=100000;
	printf("%d %d\n",n,q);
	For(i,1,q+1){
		if (i==1||(rand()%3==0)){
			l=R(1,n);r=R(1,n);
			if (l>r) swap(l,r);
			printf("1 %d %d\n",l,r);
		} else{
			l=R(1,i-1),r=R(1,i-1);
			if (l>r) swap(l,r);
			printf("2 %d %d\n",l,r);
		}
	}
}

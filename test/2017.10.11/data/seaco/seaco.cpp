#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int p=1e9+7;
const int N=100000+19;

struct QType{
	int o,l,r;
} Q[N];
int A[N],B[N];
int n,m;

void Plus(int &x,int y){
	x=(x+y)%p;
}
void Main(){
	memset(A,0,sizeof(A));
	memset(B,0,sizeof(B));
	n=IN(),m=IN();
	For(i,1,m+1){
		Q[i]=(QType){IN(),IN(),IN()};
	}
	for (int i=m;i;i--){
		Plus(B[i],B[i+1]+1);
		Plus(B[i-1],p-1);
		if (Q[i].o==1){
			Plus(A[Q[i].l],B[i]);
			Plus(A[Q[i].r+1],p-B[i]);
		} else{
			Plus(B[Q[i].r],B[i]);
			Plus(B[Q[i].l-1],p-B[i]);
		}
	}
	For(i,1,n+1){
		A[i]=(A[i]+A[i-1])%p;
		printf("%d ",A[i]);
	}
	puts("");
}

int main(){
	freopen("seaco10.in","r",stdin);
	freopen("seaco10.out","w",stdout);
	Main();
}

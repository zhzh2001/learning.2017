#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define Lsn (x<<1)
#define Rsn (x<<1|1)
#define Mid ((L+R)>>1)

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=200000+19;

int n,k,q,st;
int A[N],S[N],val[N];
char s[N];

int mx[N*4];
int Ql,Qr;

void Build(int x,int L,int R){
	if (L==R){
		mx[x]=val[L];
		return;
	}
	Build(Lsn,L,Mid);
	Build(Rsn,Mid+1,R);
	mx[x]=max(mx[Lsn],mx[Rsn]);
}
int Query(int x,int L,int R){
	if (Ql<=L&&R<=Qr) return mx[x];
	int res=0;
	if (Ql<=Mid) res=max(res,Query(Lsn,L,Mid));
	if (Qr>Mid) res=max(res,Query(Rsn,Mid+1,R));
	return res;
}

int main(){
	freopen("chefsuba10.in","r",stdin);
	freopen("chefsuba10.out","w",stdout);
	n=IN(),k=IN(),q=IN();
	k=min(k,n);
	For(i,1,n+1) A[i]=A[i+n]=IN();
	For(i,1,2*n+1) S[i]=S[i-1]+A[i];
	for (int i=1;i+k-1<=2*n;i++) val[i]=S[i+k-1]-S[i-1];
	Build(1,1,2*n);
	st=1;
	scanf("%s",s);
	For(i,0,q){
		if (s[i]=='?'){
			Ql=st,Qr=st+n-k;
			printf("%d\n",Query(1,1,2*n));
		} else{
			st--;
			if (st==0) st=n;
		}
	}
}

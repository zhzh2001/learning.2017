#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int n,k,q,alb;

int R(int l,int r){
	return rand()%(r-l+1)+l;
}

int main(){
	freopen("chefsuba10.in","w",stdout);
	srand(time(0));
	
	n=100000,k=R(10000,50000),q=200000;
	printf("%d %d %d\n",n,k,q);
	For(i,1,n+1) printf("%c ","01"[rand()%2]);
	puts("");
	For(i,1,q+1){
		if (alb||rand()%2==0){
			printf("!");
			alb=0;
		} else{
			printf("?");
			alb=1;
		}
	}
	puts("");
}

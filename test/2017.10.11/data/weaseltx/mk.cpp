#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

ll R(){
	return (0ll+rand())<<45|((0ll+rand())<<30)|(rand()<<15)|rand();
}
ll R(ll l,ll r){
	return R()%(r-l+1)+l;
}

int n,q;
ll x,A;

int main(){
	freopen("weaseltx10.in","w",stdout);
	srand(time(0));
	
	n=200000,q=200000;x=1e18;A=1e18;
	printf("%d %d\n",n,q);
	For(i,2,n+1){
		printf("%I64d %I64d\n",R(max(i-4,1),i-1)-1,i-1);
	}
	For(i,1,n+1) printf("%I64d ",R(1,A));
	puts("");
	while (q--){
		printf("%I64d\n",R(1,x));
	}
}

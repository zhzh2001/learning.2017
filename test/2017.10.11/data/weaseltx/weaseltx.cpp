#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

ll IN(){
	int c,f;ll x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=200000+19;
const int p=1<<18;

struct Edge{
	int y,nxt;
} E[N*2];
ll v[N],val[N],ans[p+19],res;
int las[N],dep[N];
int n,q,s,m,cnt;

void Link(int x,int y){
	E[cnt]=(Edge){y,las[x]};las[x]=cnt++;
	E[cnt]=(Edge){x,las[y]};las[y]=cnt++;
}
void dfs(int x,int fa){
	val[dep[x]]^=v[x];
	m=max(m,dep[x]);
	for (int i=las[x],y;~i;i=E[i].nxt)
		if ((y=E[i].y)!=fa){
			dep[y]=dep[x]+1;
			dfs(y,x);
		}
}

int main(){
	freopen("weaseltx10.in","r",stdin);
	freopen("weaseltx10.out","w",stdout);
	memset(las,-1,sizeof(las));
	n=IN(),q=IN();
	For(i,1,n) Link(IN()+1,IN()+1);
	For(i,1,n+1) v[i]=IN();
	dfs(1,-1);
	ans[0]=val[0];
	For(s,0,p)
		for (int t=s;;t=(t-1)&s)
			if (t<=m){
				ans[s-t+1]^=val[t];
				if (!t) break;
			}
	while (q--){
		printf("%I64d\n",ans[IN()&p-1]);
	}
}

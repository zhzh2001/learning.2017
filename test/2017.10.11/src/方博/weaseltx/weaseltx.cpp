#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=200005;
int ans[2*N];
int f[N];
int c1[2*N],c2[2*N],c3[2*N],c4[2*N],c5[2*N];
int fa[N],t,n,q,k;
int vis[N];
int nxt[2*N],head[N],tail[2*N];
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k)
{
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
		if(!vis[tail[i]]){
			dfs(tail[i]);
			f[k]^=f[tail[i]];
		}
	vis[k]=false;
}
inline ll read()
{
	ll x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void wrote(int x)
{
	if(x<0)putchar('-'),x=-x;
	if (x>=10)wrote(x/10);
	putchar(x%10+'0');
}
int main()
{
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<n;i++){
		int x,y;
		x=read();y=read();
		addto(x,y);addto(y,x);
	}
	for(int i=1;i<=n;i++)f[i-1]=read();
	int C1,C2,C3;
	C1=n%123;
	C2=n*423%(n%145);
	C3=n*516%187;
	c1[0]=f[C1];c2[0]=f[C2];
	c3[0]=f[C3];c4[0]=f[7];
	c5[0]=f[2];ans[0]=f[0];
	for(int i=1;i<=2*n;i++){
		dfs(0);
		ans[i]=f[0];c1[i]=f[C1];
		c2[i]=f[C2];c3[i]=f[C3];
		c4[i]=f[7];c5[i]=f[2];
		if(ans[i]==ans[0]&&c5[i]==c5[0]&&c4[i]==c4[0]&&c1[i]==c1[0]&&c2[i]==c2[0]&&c3[i]==c3[0]){
			k=i;
			break;
		}
		k=i;
	}
	for(int i=1;i<=q;i++){
		ll x=read();
		wrote(ans[x%k]);
		puts("");
	}
	return 0;
}

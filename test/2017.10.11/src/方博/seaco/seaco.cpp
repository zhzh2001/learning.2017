#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=200005;
struct xxx{
	ll p,k;
};
bool operator <(const xxx& x, const xxx& y){
	return x.p<y.p;
}
set<xxx>q[N];
int n,Q;
ll b[N];
ll ans[N];
set<int>p;
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void wrote(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)wrote(x/10);
	putchar(x%10+'0');
}
int main()
{
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read();Q=read();
	for(int i=1;i<=Q;i++){
		int op,l,r;
		bool bl,br;
		op=read();l=read();
		r=read();bl=br=false;
		if(op==1){
			for(set<xxx>::iterator it=q[i-1].begin();it!=q[i-1].end();it++){
				xxx a=*it;
				if(l==a.p)bl=true,a.k++;
				if(r+1==a.p)br=true,a.k--;
				if(a.k)q[i].insert(a);
			}
			xxx lx,rx;
			lx.p=l;lx.k=1;rx.p=r+1;rx.k=-1;
			if(!bl)q[i].insert(lx);
			if(!br)q[i].insert(rx);
		}
		else{
			p.clear();
			memset(b,0,sizeof(b));
			for(set<xxx>::iterator it=q[i-1].begin();it!=q[i-1].end();it++){
				xxx a=*it;
				p.insert(a.p);
				b[a.p]+=a.k;
			}
			for(set<xxx>::iterator it=q[r].begin();it!=q[r].end();it++){
				xxx a=*it;
				p.insert(a.p);
				b[a.p]+=a.k;
			}
			for(set<xxx>::iterator it=q[l-1].begin();it!=q[l-1].end();it++){
				xxx a=*it;
				p.insert(a.p);
				b[a.p]-=a.k;
			}
			for(set<int>::iterator it=p.begin();it!=p.end();it++){
				xxx a;
				a.p=(*it);
				a.k=b[a.p];
				if(a.k)q[i].insert(a);
			}
		}
	}
	memset(b,0,sizeof(b));
	for(set<xxx>::iterator it=q[Q].begin();it!=q[Q].end();it++){
		xxx a=*it;
		b[a.p]=a.k;
	}
	for(int i=1;i<=n;i++){
		ans[i]=ans[i-1]+b[i];
		wrote(ans[i]);
		putchar(' ');
	}
	return 0;
}

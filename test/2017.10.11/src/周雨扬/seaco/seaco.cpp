#include<bits/stdc++.h>
#define N 200005
#define mo 1000000007
using namespace std;
const int L=1000005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
inline void write(int x){
	if (!x){
		putchar('0');
		return;
	}
	static int a[15],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
}
int n,q,a[N],b[N],c[N],v[N],ans[N];
void update(int x,int y){
	for (;x<=q;x+=x&(-x)) v[x]=(v[x]+y)%mo;
}
int ask(int x){
	int s=0;
	for (;x;x-=x&(-x)) s=(s+v[x])%mo;
	return s;
}
int main(){
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<=q;i++)
		a[i]=read(),b[i]=read(),c[i]=read();
	update(1,1);
	for (int i=q;i;i--){
		if (a[i]==1) continue;
		int num=ask(i);
		update(b[i],num);
		update(c[i]+1,mo-num);
	}
	for (int i=1;i<=q;i++){
		if (a[i]==2) continue;
		int num=ask(i);
		ans[b[i]]=(ans[b[i]]+num)%mo;
		ans[c[i]+1]=(ans[c[i]+1]+mo-num)%mo;
	}
	for (int i=1;i<=n;i++)
		ans[i]=(ans[i]+ans[i-1])%mo;
	for (int i=1;i<=n;i++)
		write(ans[i]),putchar(' ');
}

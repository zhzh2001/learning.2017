#include<bits/stdc++.h>
#define N 100005
#define mo 1000000007
using namespace std;
const int L=1000005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
int n,q,a[5005][5005];
int main(){
	freopen("seaco.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<=q;i++){
		int kind=read(),l=read(),r=read();
		if (kind==1){
			for (int j=1;j<=n;j++)
				a[i][j]=a[i-1][j];
			for (int j=l;j<=r;j++)
				a[i][j]=(a[i][j]+1)%mo;
		}
		else
			for (int j=1;j<=n;j++)
				a[i][j]=(a[i-1][j]+(a[r][j]-a[l-1][j]+mo)%mo)%mo;
	}
	for (int i=1;i<=n;i++)
		printf("%d ",a[q][i]);
}

#include<bits/stdc++.h>
#define M 200005
using namespace std;
const int L=1000005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
int a[M],b[M],q[M],ans[M];
int n,k,Q,h,t,N,l=1;
int main(){
	freopen("chefsuba.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read(); k=read(); Q=read(); N=n*2;
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=n+1;i<=N;i++) a[i]=a[i-n];
	for (int i=1;i<=N;i++) a[i]+=a[i-1];
	for (int i=1;i+k-1<=N;i++) b[i]=a[i+k-1]-a[i-1]; 
	for (int i=1;i<=n;i++)
		for (int j=i;j+k-1<i+n;j++)
			ans[i%n]=max(ans[i%n],b[j]);
	while (Q--){
		char ch=gc();
		for(;ch!='!'&&ch!='?';ch=gc());
		if (ch=='!') l=(l+n-1)%n;
		else printf("%d\n",ans[l]);
	}
}

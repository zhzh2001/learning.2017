#include<bits/stdc++.h>
#define N 200005
#define ll long long
#define M 262143
using namespace std;
const int L=1000005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
inline void write(ll x){
	if (!x){
		puts("0");
		return;
	}
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
	puts("");
}
struct edge{int to,next;}e[N*2];
int head[N],dep[N],n,tot,q,mxdep;
ll ans[M+5],v[N];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
void dfs(int x,int fa){
	mxdep=max(mxdep,dep[x]);
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			dep[e[i].to]=dep[x]+1;
			dfs(e[i].to,x);
		}
}
int main(){
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<n;i++){
		int x=read()+1,y=read()+1;
		add(x,y); add(y,x);
	}
	dfs(1,0);
	for (int i=1;i<=n;i++)
		v[dep[i]]^=read();
	int u=clock();
	for (int i=0;i<=mxdep;i++)
		for (int j=M-i,k=j;;k=(k-1)&j){
			ans[(k+1)&M]^=v[i];
			if (!k) break;
		}
	while (q--) write(ans[read()&M]);
}


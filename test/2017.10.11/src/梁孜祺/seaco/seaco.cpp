#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define mod 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
ll f[N];
int n,Q;
struct data{int opt,l,r;}a[N];
ll t[N];
inline void add(int x,ll val){
	for(;x<=Q;x+=(x&-x)) t[x]=(t[x]+val)%mod;
}
inline ll query(int x){
	ll sum=0;
	for(;x;x-=(x&-x)) sum=(sum+t[x])%mod;
	return sum;
}
int main(){
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read();Q=read();
	For(i,1,Q) a[i]=(data){read(),read(),read()};
	Rep(i,Q,1) if(a[i].opt==2){
		int l=a[i].l,r=a[i].r;ll p=(query(i)+1)%mod;
		add(l,p);add(r+1,-p);
	}
	For(i,1,Q) if(a[i].opt==1){
		int l=a[i].l,r=a[i].r;ll val=(query(i)+1)%mod;
		f[l]=(f[l]+val)%mod;f[r+1]=(f[r+1]-val)%mod;
	}
	For(i,1,n) f[i]=(f[i]+f[i-1])%mod;
	For(i,1,n) printf("%d ",(f[i]+mod)%mod);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n=50,Q=100;
int main(){
	freopen("seaco.in","w",stdout);
	srand(time(0));
	cout<<n<<" "<<Q<<endl;
	For(i,1,Q){
		if(rand()%2||i==1){
			cout<<1<<" ";
			int l=(1ll<<15)*rand()%n+1;
			int r=(1ll<<15)*rand()%n+1;
			if(l>r) swap(l,r);
			printf("%d %d\n",l,r);
		}else{
			cout<<2<<" ";
			int l=(1ll<<15)*rand()%(i-1)+1;
			int r=(1ll<<15)*rand()%(i-1)+1;
			if(l>r) swap(l,r);
			printf("%d %d\n",l,r);
		}
	}
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define mod 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,Q;
ll f[N];
struct data{int opt,l,r;}a[N];
inline void solve(int l,int r){
	For(i,l,r){
		if(a[i].opt==1) f[a[i].l]++,f[a[i].r+1]--;
		else solve(a[i].l,a[i].r);
	}
}
int main(){
	freopen("seaco.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();Q=read();
	For(i,1,Q){
		int opt=read(),l=read(),r=read();
		a[i]=(data){opt,l,r};
		if(opt==1) f[l]++,f[r+1]--;
		else solve(l,r);
	}
	For(i,1,n) f[i]=f[i]+f[i-1];
	For(i,1,n) printf("%d ",f[i]);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,Q,cnt,head[N];
ll ans[N],a[N];
struct edge{int next,to;}e[400005];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
inline void dfs(int x,int fa){
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			dfs(e[i].to,x);
			a[x]^=a[e[i].to];
		}
}
int main(){
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read();Q=read();
	For(i,1,n-1){
		int x=read()+1,y=read()+1;
		insert(x,y);
	}
	For(i,1,n) a[i]=read();
	ans[0]=a[1];
	For(i,1,1000) dfs(1,0),ans[i]=a[1];
	For(i,1,Q){
		int x=read();
		printf("%lld\n",ans[x]);
	}
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define mod 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,Q,head[N],cnt,a[N],p[N];
struct edge{int next,to;}e[N<<2];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
int dep[N];
inline void dfs(int x,int fa){
	p[dep[x]]^=a[x];
	for(int i=head[x];i;i=e[i].next)
		if(e[i].to!=fa){
			dep[e[i].to]=dep[x]+1;
			dfs(e[i].to,x);
		}
}
int main(){
	n=read();Q=read();
	For(i,1,n-1){
		int x=read(),y=read();
		insert(x+1,y+1);
	}
	For(i,1,n) a[i]=read();
	dfs(1,0);
	return 0;
}

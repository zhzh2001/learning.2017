/*#include<bits/stdc++.h>
using namespace std;
#define int long long
int f[50000][500];
signed main()
{
	for (int m=1;m<=32;++m)
	{
		for (int i=0;i<=m;++i)
			f[0][i]=1LL<<i;

		for (int i=1;i<=(1<<(int)(log2(m)+1));++i)
		{
			f[i][0]=f[i-1][0];
			for (int j=1;j<=m;++j)
				f[i][j]=f[i-1][j]^f[i][j-1];
			if (f[i][m]==f[0][m])
			{
				printf("%lld %lld\n",m,i);
				break;
			}
			for (int j=m;j>=0;--j)
				if (f[i][m]&(1<<j))
					putchar('1');
				else
					putchar('0');
			putchar('\n');
			printf("%d:",i);
			for (int j=m;j>=0;--j)
				printf("%d\t",f[i][j]);
			putchar('\n');
		}
	puts("----------");
	}	
	return 0;
}*/
#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=200005;
int la[N],pr[N*2],to[N*2],n,t,x,y,cnt,mo,l;
ll f[N],a[N],g[N*2],z;
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void read(ll &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
void dfs(int x,int fa,int len)
{
	l=max(l,len);
	a[len]^=f[x];
	for (int i=la[x];~i;i=pr[i])
		if (to[i]!=fa)
			dfs(to[i],x,len+1);
}
int main()
{
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	read(n);
	read(t);
	memset(la,-1,sizeof(la));
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	for (int i=0;i<n;++i)
		read(f[i]);
	dfs(0,-1,1);
	mo=1;
	for (;mo<l;mo*=2);
	g[0]=a[1];
	for (int i=1;i<mo;++i)
	{
		for (int j=l-1;j>=1;--j)
			a[j]=a[j]^a[j+1];
		g[i]=a[1];
	}
	while (t--)
	{
		read(z);
		printf("%lld\n",g[z%mo]);
	}
	return 0;
}
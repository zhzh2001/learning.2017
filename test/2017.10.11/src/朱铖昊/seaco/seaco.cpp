#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7,N=100005;
int s[N*4],x[N],y[N],z[N],a[N],n,m,ans,p;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int get(int l,int r,int x,int k)
{
	if (l==r)
		return s[k];
	int mid=(l+r)/2;
	if (x<=mid)
		return (s[k]+get(l,mid,x,k*2))%mod;
	else
		return (s[k]+get(mid+1,r,x,k*2+1))%mod;
}
void modify(int l,int r,int L,int R,int x,int k)
{
	if (l==L&&r==R)
	{
		(s[k]+=x)%=mod;
		return;
	}
	int mid=(l+r)/2;
	if (R<=mid)
		modify(l,mid,L,R,x,k*2);
	if (L>mid)
		modify(mid+1,r,L,R,x,k*2+1);
	if (L<=mid&&R>mid)
	{
		modify(l,mid,L,mid,x,k*2);
		modify(mid+1,r,mid+1,R,x,k*2+1);
	}
}
int main()
{
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x[i]);
		read(y[i]);
		read(z[i]);
	}
	s[1]=1;
	for (int i=m;i>=1;--i)
		if (x[i]==2)
		{
			p=get(1,n,i,1);
			modify(1,n,y[i],z[i],p,1);
		}
	for (int i=1;i<=m;++i)
		if (x[i]==1)
		{
			p=get(1,n,i,1);
			(a[y[i]]+=p)%=mod;
			(a[z[i]+1]+=(mod-p))%=mod;
		}
	ans=0;
	for (int i=1;i<=n;++i)
	{
		(ans+=a[i])%=mod;
		printf("%d ",ans);
	}
	return 0;
}
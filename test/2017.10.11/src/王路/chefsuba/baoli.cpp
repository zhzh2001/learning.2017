#include <bits/stdc++.h>
using namespace std;

ifstream fin("chefsuba.in");
ofstream fout("bf.out");

const int MaxN = 100005, MaxQ = MaxN * 4;

typedef pair<int, int> pii;

int dig[MaxQ], sum[MaxQ], ans[MaxQ], qry[MaxQ];

int main() {
	int n, k, q;
	fin >> n >> k >> q;	
	for (int i = 1; i <= n; ++i) {
		fin >> dig[i];
		dig[i + n + n + n] = dig[i + n + n] = dig[i + n] = dig[i];
	}
	int m = n * 4;
	for (int i = 1; i <= m; ++i)
		sum[i] = sum[i - 1] + dig[i];
	string opt;
	fin >> opt;
	int low = 1, high = n;
	for (int i = 0; i < q; ++i) {
		if (opt[i] == '?') {
			int tmp = 0;
			for (int j = low; j <= high; ++j)
				if (j - k >= low - 1)
					tmp = max(tmp, sum[j] - sum[j - k]);
			fout << tmp << endl;
		} else {
			++low, ++high;
		}
	}
	return 0;
}

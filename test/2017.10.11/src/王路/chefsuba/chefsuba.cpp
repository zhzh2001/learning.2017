#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <deque>
#include <string>
using namespace std;

ifstream fin("chefsuba.in");
//ofstream fout("chefsuba.out");

const int MaxN = 100005, MaxQ = MaxN << 1;

typedef pair<int, int> pii;

int dig[MaxQ], sum[MaxQ], ans[MaxQ], qry[MaxQ];

inline void write_int(int x) {
	if (x == 0)	return;
	write_int(x / 10);
	putchar(x % 10 + '0');
}

int main() {
	freopen("chefsuba.out", "w", stdout);
	int n, k, q;
	fin >> n >> k >> q;	
	for (int i = 1; i <= n; ++i) {
		fin >> dig[i];
		dig[i + n] = dig[i];
	}
	int m = n * 2;
	for (int i = 1; i <= m; ++i)
		sum[i] = sum[i - 1] + dig[i];
	int now = sum[k];
	ans[k] = now;
	for (int i = k + 1; i <= m; ++i)
		ans[i] = sum[i] - sum[i - k];
	string opt;
	fin >> opt;
	deque<pii> Q;
	Q.push_back(make_pair(ans[k], k));
	for (int i = k + 1; i < n; ++i) {
		for (; !Q.empty() && Q.front().second < i - n + k; Q.pop_front());
		for (; !Q.empty() && Q.back().first <= ans[i]; Q.pop_back());
		Q.push_back(make_pair(ans[i], i));
	}
	for (int i = n; i <= m; ++i) {
		for (; !Q.empty() && Q.front().second < i - n + k; Q.pop_front());
		for (; !Q.empty() && Q.back().first <= ans[i]; Q.pop_back());
		Q.push_back(make_pair(ans[i], i));
		qry[i - n] = Q.front().first;
	}
//	for (int i = 0; i <= n; ++i)
//		cout << qry[i] << endl;
	int cur = 0;
	for (int i = 0; i < q; ++i) {
		if (opt[i] == '?') {
//			fout << qry[cur] << endl;
			write_int(qry[cur]);
			putchar('\n');
		}
		else {
			if (++cur > n) {
				cur -= n;	
			}
		}
	}
	return 0;
}

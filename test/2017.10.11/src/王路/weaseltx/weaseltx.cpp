#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

ifstream fin("weaseltx.in");
ofstream fout("weaseltx.out");

typedef long long ll;

const int MaxN = 1005;


struct Query {
	int id, qry;
	inline bool operator<(const Query &other) const {
		return qry < other.qry;
	}
} Q[MaxN];
	
ll val[MaxN], ans[MaxN];

vector<int> G[MaxN];

int dfs(int x, int fa = -1) {
	int temp = val[x];
	for (vector<int>::iterator it = G[x].begin(); it != G[x].end(); ++it) {
		if (*it != fa) {
			temp ^= dfs(*it, x);	
		}
	}
	return val[x] = temp;
}

int main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i < n; ++i) {
		int u, v;
		fin >> u >> v;
		G[u].push_back(v);
		G[v].push_back(u);
	}
	for (int i = 0; i < n; ++i)
		fin >> val[i];
	for (int i = 0; i < q; ++i) {
		fin >> Q[i].qry;
		Q[i].id = i;
	}
	sort(Q, Q + q);
	int now = 0;
	for (int i = 1; now < q; ++i) {
		int tmp = dfs(0, -1);
		if (Q[now].qry == i)
			ans[Q[now++].id] = tmp;
	}
	for (int i = 0; i < q; ++i)
		fout << ans[i] << endl;
	return 0;
}

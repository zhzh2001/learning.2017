#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int uniform(int l, int r) {
	return rand() % (r - l + 1) + l;
}

int main() {
	freopen("seaco.in", "w", stdout);
	srand(GetTickCount());
	ios::sync_with_stdio(false);
	int n = 100, q = 100; 
	cout << n << ' ' << q << endl;
	for (int i = 1; i <= n; ++i) {
		int opt = rand() % 2 + 1;
		if (opt == 1 || i <= 5) {
			int l = uniform(1, n - 1), r = uniform(l, n);
			assert(l != 0);
			cout << 1 << ' ' << l << ' ' << r << endl;
		} else {
			int l = uniform(1, i - 2), r = uniform(l, i - 1);
			assert(l != 0);
			cout << 2 << ' ' << l << ' ' << r << endl;	
		}
	}
	return 0;
}

#include <bits/stdc++.h>
using namespace std;

ifstream fin("seaco.in");
ofstream fout("bf.out");

const int MaxN = 100005;

struct Query {
	int opt, l, r;
} qs[MaxN], todo[MaxN];

const int Mod = 1e9 + 7;

long long b[MaxN];

void work(int i) {
	if (qs[i].opt == 1) {
		++b[qs[i].l];
		--b[qs[i].r + 1];
	} else {
		for (int j = qs[i].l; j <= qs[i].r; ++j)
			work(j);
	}
}

int main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i <= q; ++i) {
		fin >> qs[i].opt >> qs[i].l >> qs[i].r;
		work(i);
	}
	long long now = 0;
	for (int i = 1; i <= n; ++i) {
		now = (now + b[i]) % Mod;
		fout << now << ' ';	
	}
	fout << endl;
	return 0;
}

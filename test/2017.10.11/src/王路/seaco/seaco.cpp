#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
//#include <cassert>
using namespace std;

ifstream fin("seaco.in");
ofstream fout("seaco.out");

const int MaxN = 100005, Mod = 1e9 + 7;

struct Query {
	int opt, l, r;
} qs[MaxN], todo[MaxN];

struct Node {
	int l, r;
	long long lazy;
} tr[MaxN * 8];

int a[MaxN];

void build(int x, int l, int r) {
	tr[x].l = l, tr[x].r = r;
	if (l == r)
		return;
	int mid = (l + r) >> 1;
	build(x << 1, l, mid);
	build(x << 1 | 1, mid + 1, r);
}

inline void pushdown(int x) {
	if (tr[x].lazy) {
		tr[x << 1].lazy = (tr[x << 1].lazy + tr[x].lazy + Mod) % Mod;
		tr[x << 1 | 1].lazy = (tr[x << 1 | 1].lazy + tr[x].lazy + Mod) % Mod;
		tr[x].lazy = 0;
	}
}

void add(int x, int l, int r, long long val) {
	if (l <= tr[x].l && tr[x].r <= r) {
		tr[x].lazy = (tr[x].lazy + val + Mod) % Mod;
		return;
	}
	if (l > tr[x].r || r < tr[x].l)
		return;
	pushdown(x);
	add(x << 1, l, r, val);
	add(x << 1 | 1, l, r, val);
}

long long query(int x, int pos) {
	if (tr[x].l == tr[x].r && tr[x].l == pos) {
		long long tmp = tr[x].lazy;
		tr[x].lazy = 0;
		return tmp;
	}
	pushdown(x);
	int mid = (tr[x].l + tr[x].r) >> 1;
	if (pos <= mid)	return query(x << 1, pos);
		else return query(x << 1 | 1, pos);
}

long long b[MaxN];

void export_seg(int x, int l, int r) {
	if (l == r) {
		b[l] = tr[x].lazy;
		return;
	}
	pushdown(x);
	int mid = (l + r) >> 1;
	export_seg(x << 1, l, mid);
	export_seg(x << 1 | 1, mid + 1, r);
}

long long res[MaxN], ans[MaxN];

int main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i <= q; ++i)
		fin >> qs[i].opt >> qs[i].l >> qs[i].r;
	build(1, 1, q);
	for (int i = 1; i <= q; ++i)
		if (qs[i].opt == 2)
			add(1, qs[i].l, qs[i].r, 1);
	for (int i = q; i >= 1; --i)
		if (qs[i].opt == 2) {
			long long cur = query(1, i);
			add(1, qs[i].l, qs[i].r, cur);
		}
	export_seg(1, 1, q);
	for (int i = 1; i <= q; ++i)
		if (qs[i].opt == 1) {
			b[i] = (b[i] + 1 + Mod) % Mod;
//			res[qs[i].l] += b[i];	
//			res[qs[i].r + 1] -= b[i];
			res[qs[i].l] = (res[qs[i].l] + b[i]) % Mod;
			res[qs[i].r + 1] = (res[qs[i].r + 1] - b[i] + Mod) % Mod;
		}/* else if (qs[i].opt == 2)*/
//			assert(b[i] == 0);
	long long now = 0LL;
	for (int i = 1; i <= n; ++i) {
		now = (now + res[i] + Mod) % Mod;
		ans[i] = now;
	}
	for (int i = 1; i <= n; ++i)
		fout << ans[i] % Mod << ' ';
	fout << endl;
	return 0;
}

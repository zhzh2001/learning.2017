#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
#define LL long long 
using namespace std ; 

const int N = 5011 ; 
int n,Q,cnt,m ; 
int x[N],q[N],ans[N],head[N],fa[N] ;  
struct node{
	int to,pre ; 
}e[N*2];

inline int read() 
{
	char ch = getchar() ; 
	int x = 0 , f = 1 ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ; 
}
inline void write(int x) 
{
	if(x<0) putchar('-'),x=-x ; 
	if(x>9) write(x/10) ; 
	putchar(x%10+48) ; 
}
inline void writeln(int x) 
{
	write(x) ; 
	putchar('\n') ; 
}

inline void add(int x,int y) 
{
	e[++cnt].to = y ; 
	e[cnt].pre = head[x] ; 
	head[x] = cnt ; 
}

inline void gf(int u,int f) 
{
	fa[u] = f ; 
	for(int i=head[u];i;i=e[i].pre) 
		if(e[i].to!=f) 
			gf(e[i].to,u) ; 
}


inline void dfs(int u) 
{
	for(int i=head[u];i;i=e[i].pre) 
		if(e[i].to!=fa[u]) {
			dfs(e[i].to) ; 
			x[u]^=x[e[i].to] ; 
		} 
}

int main() 
{
	freopen("weaseltx.in","r",stdin) ; 
	freopen("weaseltx.out","w",stdout) ; 
	n = read() ; Q = read() ;
	For(i,1,n-1) {
		int x,y ; 
		x = read() ; y = read() ; 
		add(x+1,y+1) ; add(y+1,x+1) ; 
	}
	gf(1,-1) ;
	For(i,1,n) x[i]=read() ; 
	ans[0] = x[1] ; 
	For(i,1,Q) {
		q[i]=read() ; 
		if(q[i]>m) m = q[i] ;  
	}
	For(i,1,m) {
		dfs(1) ; 
		ans[i] = x[1] ; 
	}
	For(i,1,Q) writeln(ans[q[i]]) ; 
	return 0 ; 
}

#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++)  
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
using namespace std ; 

const int N = 3011,mod = 1e9+7 ; 
int ans[N],sum[N][N],res[N] ; 
int n,t,ty,l,r ; 

inline int read() 
{
	char ch = getchar() ; 
	int x = 0 , f = 1 ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ; 
}

inline void write(int x) 
{
	if(x<0) putchar('-'),x=-x ; 
	if(x>9) write(x/10) ; 
	putchar(x%10+48) ; 
}
int main() 
{
	freopen("seaco.in","r",stdin) ; 
	freopen("seaco.out","w",stdout) ; 
	n = read() ; t = read() ; 
	For(i,1,t) {
		ty=read();l=read();r=read();
		if(ty==1) 
			For(j,l,r) ans[j]+=1; 
		else 
			For(j,1,n) ans[j]=(sum[r][j]-sum[l-1][j]+mod)%mod ; 
		For(j,1,n) sum[i][j] = (sum[i-1][j]+ans[j])%mod ; 
		For(j,1,n) (res[j]+=ans[j])%=mod,ans[j]=0; 
	}
	For(i,1,n) {
		write(res[i]) ; putchar(' ') ; 
	}
	return 0 ; 
}




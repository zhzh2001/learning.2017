#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
#define LL long long 
using namespace std ; 

const int N = 2011 ; 
int n,K,Q,l ; 
char s[N] ; 
int ans[N],f[N][N],L[N],R[N] ; 
bool a[N] ; 
 
inline int read() 
{
	char ch = getchar() ; 
	int x = 0 , f = 1 ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ; 
}

inline void write(int x) 
{
	if(x<0) putchar('-'),x=-x ; 
	if(x>9) write(x/10) ; 
	putchar(x%10+48) ; 
}

inline void writeln(int x) 
{
	write(x) ; 
	putchar('\n') ; 
}


int main() 
{
	freopen("chefsuba.in","r",stdin) ; 
	freopen("chefsuba.out","w",stdout) ; 
	n = read() ; K = read() ; Q = read() ; 
	For(i,0,n-1) a[i] = read() ; 
	For(i,0,(n-1)/2) swap(a[i],a[n-i-1]) ; 
	For(i,0,n-1) {
		a[i+n]=a[i] ; 
	}
	scanf("%s",s+1) ; 
	For(i,0,2*n-1) 
		if(a[i]) R[i]=R[i-1]+1 ; 
		else R[i]=0 ; 
	Dow(i,2*n-1,0) 
		if(a[i]) L[i]=L[i+1]+1 ; 
		else L[i]=0 ; 
	For(i,0,2*n-1) if(a[i]) f[i][1] = 1 ; 
	
 	For(len,2,K) {
		For(i,0,2*n-len) {
		  int j = i+len-1 ; 	
		  f[i][len] = min( max( max(f[i+1][len-1],R[j-1]+a[j]) , L[i+1]+a[i] ) ,len ) ; 
		  //f[i][len] = max(f[i][len-1]+a[i+len-1],f[i+1][len-1]+a[i]) ; 
		}
	}
	 
	
	For(i,0,n-1) {
		int Mx = 0 ; 
		For(j,i,i+n-K) 
			if( f[j][K] > Mx ) Mx = f[j][K] ; 
		ans[i] = Mx ; 
	}
	
	l = 0 ; 
	For(i,1,Q) {
		if(s[i]=='!') l=(l+1) %n ; 
		else writeln(ans[l]) ; 
	}
	
	return 0 ; 
}



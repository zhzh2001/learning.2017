#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x)
{    
	if (x<0) putchar('-'),x=-x; 
	if (x>=10) write(x/10);  
	putchar(x%10+'0');  
}
inline void writeln(ll x){write(x);puts("");}
int n,k,m,a[200001],qz[200001],hz[200001],ans[200001];
char s[500001];
int tr[2000001],tans;
inline void build(int x,int l,int r)
{
	if(l==r)	{tr[x]=ans[l];return;}
	int mid=l+r>>1;
	build(x<<1,l,mid);build(x<<1|1,mid+1,r);
	tr[x]=max(tr[x<<1|1],tr[x<<1]);
}
inline void query(int x,int l,int r,int L,int R)
{
	if(L<=l&&r<=R)
	{
		tans=max(tans,tr[x]);
		return;
	}
	int mid=l+r>>1;
	if(L<=mid)	query(x<<1,l,mid,L,R);
	if(R> mid)	query(x<<1|1,mid+1,r,L,R);
}
int jy[200001];
int main()
{
	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout);
	n=read();k=read();m=read();
	For(i,1,n)	a[i]=read();
	For(i,0,n)	jy[i]=-1;
	For(i,1,n)	qz[i]=qz[i-1]+a[i];
	Dow(i,1,n)	hz[i]=hz[i+1]+a[i];
	For(i,1,n-k+1)	ans[i]=qz[i+k-1]-qz[i-1];
	For(i,n-k+2,n)
		ans[i]=hz[i]+qz[k-(n-i+1)];
	build(1,1,n);
	scanf("\n%s",s+1);
	int tot=0;
	tans=0;
	int l=1+tot,r=n-k-tot+1;
	query(1,1,n,l,r);
	jy[tot]=tans;
	int l1=1,r1=n-k+1,l2=n+1,r2=l2+(r1-l1);
	For(i,1,m)
	{
		if(s[i]=='!')
		{
			tot++;
			if(tot==n)	tot=0;
			if(jy[tot]!=-1)	continue;	
			r1--;l2--;r2--;l1--;
			tans=0;
			if(r1>=1)query(1,1,n,max(1,l1),min(n,r1));
			if(l2!=n+1)query(1,1,n,max(1,l2),min(n,r2));
			jy[tot]=tans;
		}else	writeln(jy[tot]);
	}
}

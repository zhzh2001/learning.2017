#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
ll poi[20001],nxt[20001],f[20001],v[20001],ans[20001],t,n,q,cnt,tot;
inline void add(ll x,ll y){poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;}
inline ll dfs(ll x,ll fa)
{
	ll now=v[x];tot++;
	for(ll i=f[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		now^=dfs(poi[i],x);
	}
	return v[x]=now;
}
ll x,y,jl[2001];
inline bool pd()
{
	For(i,1,n)	if(jl[i]!=v[i])	return 0;
	return 1;
}
int main()
{
	freopen("weaseltx.in","r",stdin);
//	freopen("weaseltx.out","w",stdout);
	n=read();q=read();
	For(i,1,n-1)	x=read()+1,y=read()+1,add(x,y),add(y,x);
	For(i,1,n)	v[i]=read();
	ans[1]=dfs(1,0);
	For(i,1,n)	jl[i]=v[i];
	int mo=1e7;
	For(i,2,5000)
	{
		ans[i]=dfs(1,0);
		if(pd())	{mo=i-1;break;}
		if(tot>=5000000)	break;
	}
	cout<<mo<<endl;
}
/*
4 20
0 1
1 2
0 3
1 5 8 7
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20 
*/

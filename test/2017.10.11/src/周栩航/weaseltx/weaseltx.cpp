#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x)
{    
	if (x<0) putchar('-'),x=-x; 
	if (x>=10) write(x/10);  
	putchar(x%10+'0');  
}
inline void writeln(ll x){write(x);puts("");}


ll poi[500001],nxt[500001],f[500001],v[500001],ans[500001],t,n,q,cnt,tot;
inline void add(ll x,ll y){poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;}
inline ll dfs(ll x,ll fa)
{
	ll now=v[x];tot++;
	for(ll i=f[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		now^=dfs(poi[i],x);
	}
	return v[x]=now;
}
ll x,y,jl[200001];
inline bool pd()
{
	For(i,1,n)	if(jl[i]!=v[i])	return 0;
	return 1;
}
int main()
{
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read();q=read();
	For(i,1,n-1)	x=read()+1,y=read()+1,add(x,y),add(y,x);
	For(i,1,n)	v[i]=read();
	ans[1]=dfs(1,0);
	For(i,1,n)	jl[i]=v[i];
	int mo=1e7;
	For(i,2,3000)
	{
		ans[i]=dfs(1,0);
		if(pd())	{mo=i-1;break;}
		if(tot>5000000)	break;
	}
	if(mo!=1e7)
	For(i,1,q)
	{
		t=read();
		t=(t-1)%mo+1;
		writeln(ans[t]);
	}
	else
	For(i,1,q)
	{
		t=read();
		writeln(ans[t]);
	}
}

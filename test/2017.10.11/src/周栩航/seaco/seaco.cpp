#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x)
{    
	if (x<0) putchar('-'),x=-x; 
	if (x>=10) write(x/10);  
	putchar(x%10+'0');  
}
inline void writeln(ll x){write(x);puts("");}
int mo=1e9+7,tr[300001],n,m,del[300001];
struct opt{int k,l,r;}	op[300001];
inline void add(int x,int v)
{
	for(;x<=m;x+=x&-x)	tr[x]+=v,tr[x]%=mo;
}
inline int get(int x)
{
	int sum=0;
	for(;x;x-=x&-x)	sum+=tr[x],sum%=mo;
	return (sum+mo)%mo;
}
inline void ADD(int l,int r,int v){add(l,v);add(r+1,-v);}
int main()
{
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		op[i].k=read();op[i].l=read();op[i].r=read();
	}
	
	Dow(i,1,m)
	{
		ADD(i,i,1);
		if(op[i].k==2)
		{
			int tmp=get(i);
			ADD(op[i].l,op[i].r,tmp);
		}
	}
	
	For(i,1,m)
	{
		if(op[i].k==1)
		{
			int tmp=get(i);
			del[op[i].l]+=tmp;del[op[i].l]%=mo;
			del[op[i].r+1]-=tmp;del[op[i].r+1]%=mo;
		}	
	}	
	
	int sum=0;
	For(i,1,n)
	{
		sum+=del[i];sum=(sum%mo+mo)%mo;
		write(sum);putchar(' ');
	}
	puts("");
}

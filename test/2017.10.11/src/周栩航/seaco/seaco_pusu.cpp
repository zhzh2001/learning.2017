#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
int mo=1e9+7;
int n,m,a[1001][1001],l,r,opt;
int main()
{
	freopen("seaco.in","r",stdin);freopen("seaco1.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		opt=read();
		if(opt==1)
		{
			l=read();
			r=read();
			For(j,1,n)	a[i][j]=a[i-1][j];
			For(j,l,r)	a[i][j]=(a[i][j]+1)%mo;
		}
		if(opt==2)
		{
			l=read();
			r=read();
			For(j,1,n)	a[i][j]=a[i-1][j]+a[r][j]-a[l-1][j];
			For(j,1,n)	a[i][j]=(a[i][j]%mo+mo)%mo;
		}
	}
	For(i,1,n)	printf("%d ",a[m][i]);
}

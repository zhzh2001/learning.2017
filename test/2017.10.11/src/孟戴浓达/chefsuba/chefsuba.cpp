#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<fstream>
//#include<iostream>
using namespace std;
ifstream fin("chefsuba.in");
ofstream fout("chefsuba.out");
int num[200003],sum[200003],ans[200003];
int n,k,q;
char opt[200003];
struct tree{
	int mx;
}t[800003];
inline void pushup(int rt){
	t[rt].mx=max(t[rt*2].mx,t[rt*2+1].mx);
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].mx=ans[l];
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	pushup(rt);
}
int query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return t[rt].mx;
	}
	int mid=(l+r)/2;
	if(mid>=y){
		return query(rt*2,l,mid,x,y);
	}else if(mid<x){
		return query(rt*2+1,mid+1,r,x,y);
	}else{
		return max(query(rt*2,l,mid,x,mid),query(rt*2+1,mid+1,r,mid+1,y));
	}
}
int main(){
	fin>>n>>k>>q;
	for(int i=1;i<=n;i++){
		fin>>num[i];
	}
	for(int i=n+1;i<=2*n;i++){
		num[i]=num[i-n];
	}
	for(int i=1;i<=2*n;i++){
		sum[i]=sum[i-1];
		sum[i]+=(num[i]==1);
		ans[i]=sum[i]-sum[i-k];
	}
	if(k==n){
		for(int i=1;i<=q;i++){
			fout<<sum[n]<<endl;
		}
		return 0;
	}
	build(1,1,2*n);
	int tot=0;
	for(int i=1;i<=q;i++){
		fin>>opt[i];
		if(opt[i]=='!'){
			tot+=1;
			tot%=n;
		}else{
			fout<<query(1,1,2*n,n-tot+k,2*n-tot)<<endl;
		}
	}
	return 0;
}
/*

in:
5 3 9
1 0 0 1 1
?!!!!!!!?

out:
2
3

*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("weaseltx.in");
ofstream fout("weaseltx.out");
int to2[200003],next2[200003],head2[200003],tot2;
inline void ins2(int a,int b){
	to2[++tot2]=b;  next2[tot2]=head2[a];  head2[a]=tot2;
}
inline void add2(int a,int b){
	ins2(a,b),ins2(b,a);
}
int to[200003],next[200003],head[200003],tot;
inline void ins(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
bool yes[5003];
int in[5003],out[5003],tim;
void dfs(int x){
	in[x]=++tim;
	yes[x]=true;
	for(int i=head2[x];i;i=next2[i]){
		if(yes[to2[i]]==false){
			ins(x,to2[i]);
			dfs(to2[i]);
		}
	}
	out[x]=++tim;
}
int jiyi[5003][5003],val[5003];
bool vis[5003][5003];
int n,q;
int query(int x,int y){
	if(vis[x][y]){
		return jiyi[x][y];
	}
	vis[x][y]=1;
	if(y==0){
		return jiyi[x][y]=val[x];
	}
	if(vis[x][y-1]){
		jiyi[x][y]=jiyi[x][y-1];
	}else{
		jiyi[x][y]=query(x,y-1);
	}
	for(int i=1;i<=n;i++){
		if(in[x]<in[i]&&out[x]>out[i]){
			if(vis[i][y-1]){
				jiyi[x][y]^=jiyi[i][y-1];
			}else{
				jiyi[x][y]^=query(i,y-1);
			}
		}
	}
	return jiyi[x][y];
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=n-1;i++){
		int a,b;
		fin>>a>>b;
		add2(a,b);
	}
	dfs(0);
	for(int i=0;i<=n-1;i++){
		fin>>val[i];
	}
	int x;
	for(int i=1;i<=q;i++){
		fin>>x;
		fout<<query(0,x)<<endl;
	}
	return 0;
}
/*

in:
4 3
0 1
1 2
0 3
1 5 8 7
1
2
3

out:
11
9
3

*/
/*

in:
9 3
0 1
0 2
1 3
1 4
2 5
3 6
4 7
6 8
1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192

6 3
0 1
1 2
2 3
3 4
4 5
1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192

4 5
5 6
6 7
7 8
8 9
9 10
10 11
11 12
12 13
13 14
14 15
15 16
16 17
17 18
18 19
*/

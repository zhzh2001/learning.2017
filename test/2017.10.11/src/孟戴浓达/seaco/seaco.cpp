#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("seaco.in");
ofstream fout("seaco.out");
const int mod=1000000007;
int opt[100003],x[100003],y[100003];
struct tree{
	int rev,tot;
}t[800003];
inline void pushdown(int rt){
	if(!t[rt].rev){
		return;
	}
	t[rt*2].rev+=t[rt].rev,t[rt*2+1].rev+=t[rt].rev;
	t[rt*2].tot+=t[rt].rev,t[rt*2+1].tot+=t[rt].rev;
	t[rt*2].tot%=mod,t[rt*2+1].tot%=mod;
	t[rt*2].rev%=mod,t[rt*2+1].rev%=mod;
	t[rt].rev=0;
}
void build(int rt,int l,int r){
	if(l==r){
		t[rt].rev=0,t[rt].tot=1;
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
}
void add(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t[rt].tot+=val,t[rt].tot%=mod;
		t[rt].rev+=val,t[rt].rev%=mod;
		return;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(x>mid){
		add(rt*2+1,mid+1,r,x,y,val);
	}else if(y<=mid){
		add(rt*2,l,mid,x,y,val);
	}else{
		add(rt*2,l,mid,x,mid,val),add(rt*2+1,mid+1,r,mid+1,y,val);
	}
}
int query(int rt,int l,int r,int x){
	if(l==r){
		return t[rt].tot;
	}
	pushdown(rt);
	int mid=(l+r)/2;
	if(mid>=x){
		return query(rt*2,l,mid,x);
	}else{
		return query(rt*2+1,mid+1,r,x);
	}
}

struct tree2{
	int rev,tot;
}t2[800003];
inline void pushdown2(int rt){
	if(!t2[rt].rev){
		return;
	}
	t2[rt*2].rev+=t2[rt].rev,t2[rt*2+1].rev+=t2[rt].rev;
	t2[rt*2].tot+=t2[rt].rev,t2[rt*2+1].tot+=t2[rt].rev;
	t2[rt*2].tot%=mod,t2[rt*2+1].tot%=mod;
	t2[rt*2].rev%=mod,t2[rt*2+1].rev%=mod;
	t2[rt].rev=0;
}
void build2(int rt,int l,int r){
	if(l==r){
		t2[rt].rev=t2[rt].tot=0;
		return;
	}
	int mid=(l+r)/2;
	build2(rt*2,l,mid),build2(rt*2+1,mid+1,r); 
}
void add2(int rt,int l,int r,int x,int y,int val){
	if(l==x&&r==y){
		t2[rt].tot+=val,t2[rt].tot%=mod;
		t2[rt].rev+=val,t2[rt].rev%=mod;
		return;
	}
	pushdown2(rt);
	int mid=(l+r)/2;
	if(x>mid){
		add2(rt*2+1,mid+1,r,x,y,val);
	}else if(y<=mid){
		add2(rt*2,l,mid,x,y,val);
	}else{
		add2(rt*2,l,mid,x,mid,val),add2(rt*2+1,mid+1,r,mid+1,y,val); 
	}
}
int query2(int rt,int l,int r,int x){
	if(l==r){
		return t2[rt].tot;
	}
	pushdown2(rt);
	int mid=(l+r)/2;
	if(mid>=x){
		return query2(rt*2,l,mid,x);
	}else{
		return query2(rt*2+1,mid+1,r,x);
	}
}
int n,q;
int main(){
	fin>>n>>q;
	for(int i=1;i<=q;i++){
		fin>>opt[i]>>x[i]>>y[i];
	}
	build(1,1,q);
	for(int i=q;i>=1;i--){
		if(opt[i]==1){
			//add(1,1,n,i,i,1);
		}else{
			int xx=query(1,1,q,i);
			add(1,1,q,x[i],y[i],xx);
		}
	}
	build2(1,1,n);
	for(int i=1;i<=q;i++){
		if(opt[i]==1){
			int xx=query(1,1,n,i);
			add2(1,1,n,x[i],y[i],xx);
		}
	}
	for(int i=1;i<=n;i++){
		fout<<query2(1,1,n,i)<<" "; 
	}
	return 0;
}
/*

in:
5 7
1 1 2
1 4 5
2 1 2
2 1 3
2 3 4
1 3 3
2 1 5

out:
7 7 0 7 7

*/

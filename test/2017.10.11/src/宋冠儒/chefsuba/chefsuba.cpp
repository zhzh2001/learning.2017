#include<bits/stdc++.h>
using namespace std;
const int maxn=100010;
int n,k,q;
int num,cnt,delnum;
int a[maxn],suma[maxn],sum[maxn],ans[maxn];
int t[maxn<<2];
 inline int read()
{
    int X=0,w=1; char ch=0;
    while(ch<'0' || ch>'9') {if(ch=='-') w=-1;ch=getchar();}
    while(ch>='0' && ch<='9') X=(X<<3)+(X<<1)+ch-'0',ch=getchar();
    return X*w;
}

 void build(int now,int l,int r)
{
	if (l==r) 
	{
		t[now]=sum[l];
		return;
	}
	int mid=(l+r)>>1;
	build(now<<1,l,mid);build(now<<1|1,mid+1,r);
	t[now]=max(t[now<<1],t[now<<1|1]);
}
 void updata(int now,int l,int r,int tar)
{
	if (l==r)
	{
		t[now]=sum[delnum];
		return;
	}
	int mid=(l+r)>>1;
	if (tar<=mid) updata(now<<1,l,mid,tar);
	else updata(now<<1|1,mid+1,r,tar);
	t[now]=max(t[now<<1],t[now<<1|1]);
}
 int main()
{
	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout);
	n=read();k=read();q=read();
	for (int i=1;i<=n;++i) a[i]=read(),suma[i]=suma[i-1]+a[i];
	for (int i=1;i<=n-k+1;++i) 
		sum[i]=suma[i+k-1]-suma[i-1];
	num=n-k+1;
	build(1,1,num);
	ans[0]=t[1];
	cnt=0;
	delnum=num;
	for (int i=1;i<=q;++i)
	{
		char opt;
		scanf("%c",&opt);
		if (opt=='?')
		{
			printf("%d\n",ans[cnt%n]);
		}
		else
		if (opt=='!') 
		{
			if (cnt>=n) 
			{
				++cnt;
				continue;
			}
			sum[delnum]=sum[delnum%num+1]+a[n-cnt]-a[(k-cnt+n)%n];
			updata(1,1,num,delnum);
			ans[++cnt]=t[1];
			--delnum;
			if (delnum<=0) delnum=num;
		}
	}
}

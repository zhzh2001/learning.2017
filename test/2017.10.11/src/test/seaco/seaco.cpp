#include <fstream>
using namespace std;
ifstream fin("seaco.in");
ofstream fout("seaco.out");
const int N = 100005, MOD = 1e9 + 7;
int n, m, prefix[N];
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			(tree[x] += val) %= MOD;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			(ans += tree[x]) %= MOD;
		return ans;
	}
} T;
struct quest
{
	int opt, l, r;
} q[N * 2];
int main()
{
	fin >> n >> m;
	for (int i = 1; i <= m; i++)
		fin >> q[i].opt >> q[i].l >> q[i].r;
	for (int i = m; i; i--)
	{
		T.modify(i, 1);
		T.modify(i + 1, MOD - 1);
		if (q[i].opt == 2)
		{
			int cnt = T.query(i);
			T.modify(q[i].l, cnt);
			T.modify(q[i].r + 1, MOD - cnt);
		}
	}
	for (int i = 1; i <= n; i++)
		if (q[i].opt == 1)
		{
			int cnt = T.query(i);
			(prefix[q[i].l] += cnt) %= MOD;
			(prefix[q[i].r + 1] += MOD - cnt) %= MOD;
		}
	int now = 0;
	for (int i = 1; i <= n; i++)
		fout << ((now += prefix[i]) %= MOD) << ' ';
	fout << endl;
	return 0;
}
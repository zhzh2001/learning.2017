#include<fstream>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("weaseltx.in");
ofstream fout("weaseltx.ans");
const int N=200000;
vector<int> mat[N];
long long a[N],ans[N];
pair<int,int> q[N];
void dfs(int k,int fat)
{
	for(int i=0;i<mat[k].size();i++)
		if(mat[k][i]!=fat)
		{
			dfs(mat[k][i],k);
			a[k]^=a[mat[k][i]];
		}
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for(int i=0;i<n;i++)
		fin>>a[i];
	for(int i=0;i<m;i++)
	{
		fin>>q[i].first;
		q[i].second=i;
	}
	sort(q,q+m);
	int now=0;
	for(int i=0,j;i<m;i=j)
	{
		for(;now<q[i].first;now++)
			dfs(0,-1);
		for(j=i;j<m&&q[j].first==q[i].first;j++)
			ans[q[j].second]=a[0];
	}
	for(int i=0;i<m;i++)
		fout<<ans[i]<<endl;
	return 0;
}

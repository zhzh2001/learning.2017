#include <cstdio>
#include <cstring>
#include <cctype>
#include <algorithm>
using namespace std;
FILE *fin = fopen("weaseltx.in", "r"), *fout = fopen("weaseltx.out", "w");
const int N = 200000, SZ = 1e6, mask = 1 << 18;
char buf[SZ], *p = buf, *pend = buf;
inline int nextchar()
{
	if (p == pend)
	{
		pend = (p = buf) + fread(buf, 1, SZ, fin);
		if (pend == buf)
			return EOF;
	}
	return *p++;
}
template <typename Int>
inline void read(Int &x)
{
	char c = nextchar();
	for (; isspace(c); c = nextchar())
		;
	x = 0;
	Int sign = 1;
	if (c == '-')
	{
		sign = -1;
		p++;
	}
	for (; isdigit(c); c = nextchar())
		x = x * 10 + c - '0';
	x *= sign;
}
inline void writechar(char c)
{
	if (p == pend)
	{
		fwrite(buf, 1, SZ, fout);
		p = buf;
	}
	*p++ = c;
}
inline void flush()
{
	fwrite(buf, 1, p - buf, fout);
}
int dig[20];
template <typename Int>
inline void writeln(Int x)
{
	if (x < 0)
	{
		writechar('-');
		x = -x;
	}
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		writechar(dig[len] + '0');
	writechar('\n');
}
int head[N], v[N * 2], nxt[N * 2], e, dep[N], maxdep;
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
long long a[N], val[N], ans[mask + 20], q[N];
void dfs(int k, int fat)
{
	val[dep[k]] ^= a[k];
	maxdep = max(maxdep, dep[k]);
	for (int i = head[k]; ~i; i = nxt[i])
		if (v[i] != fat)
		{
			dep[v[i]] = dep[k] + 1;
			dfs(v[i], k);
		}
}
int main()
{
	int n, m;
	read(n);
	read(m);
	memset(head, -1, sizeof(head));
	for (int i = 1; i < n; i++)
	{
		int u, v;
		read(u);
		read(v);
		add_edge(u, v);
		add_edge(v, u);
	}
	for (int i = 0; i < n; i++)
		read(a[i]);
	for (int i = 0; i < m; i++)
		read(q[i]);
	dfs(0, -1);
	ans[0] = val[0];
	for (int i = 0; i < mask && i < 2 * m; i++)
		for (int j = i;; j = (j - 1) & i)
			if (j <= maxdep)
			{
				ans[i - j + 1] ^= val[j];
				if (!j)
					break;
			}
	p = buf;
	pend = buf + SZ;
	for (int i = 0; i < m; i++)
		writeln(ans[q[i] & (mask - 1)]);
	flush();
	return 0;
}
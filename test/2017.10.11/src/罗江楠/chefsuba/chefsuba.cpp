/*
 _       _       _        ____   __    __  ____
(_)  // (_)  // (_)  //  |  _ \  \ \  / / |  _ \
    //      //      //   | |_) )  \ \/ /  | | \ |
   //      //      //    |   _/    \  /   | | | |
  //      //      //     | | \     /  \   | | | |
 //  _   //  _   //  _   | |\ \   / /\ \  | |_/ |
//  (_) //  (_) //  (_)  |_| \_\ /_/  \_\ |____/

*/
#include <bits/stdc++.h>
#define N 400020
#define LG 25
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
// ST 表询问区间最大值
int a[N], b[N][LG];
int ask(int l, int r) {
	// printf("%d <-> %d\n", l, r);
	int lg = log2(r-l+1);
	return max(b[l][lg], b[r-(1<<lg)+1][lg]);
}
char op[N];
int main(int argc, char const *argv[]) {
	freopen("chefsuba.in", "r", stdin);
	freopen("chefsuba.out", "w", stdout);
	int n = read(), k = read(), m = read();
	for (int i = 1; i <= n; i++)
		a[i+n] = a[i] = read();
	for (int i = 1; i <= k; i++)
		b[1][0] += a[i];
	for (int i = 2; i <= n; i++)
		b[i][0] = b[i-1][0]-a[i-1]+a[k+i-1];
	// for (int i = 1; i <= n; i++)
	// 	printf("%d ", b[i][0]); puts("");
	for (int i = 1; i < LG; i++)
		for (int j = 1; j + (1 << i) - 1 <= n; j++)
			b[j][i] = max(b[j][i-1], b[j+(1<<i-1)][i-1]);
	// printf("%d\n", ask(1, 5));
	int l = 1, r = n-k+1;
	scanf("%s", op+1);
	for (int i = 1; i <= m; i++) {
		if (op[i] == '!') {
			l = l == 1 ? n : (l-1);
			r = r == 1 ? n : (r-1);
		} else {
			int ans = 0;
			if (l > r)
				ans = max(ask(l, n), ask(1, r));
			else
				ans = ask(l, r);
			printf("%d\n", ans);
		}
	}
	return 0;
}
// niconiconi~
// anatano haatoni niconiconi~
// egaotodokeru yazawaniconico~
// niconi~te oboeteru rabunico~

#include <bits/stdc++.h>
#define N 100020
#define mod 1000000007
using namespace std;
int q[N], l[N], r[N], ans[N];
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
void work(int x) {
	if (q[x] == 1) {
		for (int i = l[x]; i <= r[x]; i++)
			ans[i] = (ans[i] + 1) % mod;
	} else {
		for (int i = l[x]; i <= r[x]; i++)
			work(i);
	}
}
int main(int argc, char const *argv[]) {
	freopen("seaco.in", "r", stdin);
	freopen("seaco2.out", "w", stdout);
	int n = read(), m = read();
	for (int i = 1; i <= m; i++) {
		q[i] = read();
		l[i] = read();
		r[i] = read();
		work(i);
	}
	for (int i = 1; i <= n; i++)
		printf("%d%c", ans[i], i == n ? '\n' : ' ');
	return 0;
}
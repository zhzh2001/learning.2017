/*
 _       _       _        ____   __    __  ____  
(_)  // (_)  // (_)  //  |  _ \  \ \  / / |  _ \ 
    //      //      //   | |_) )  \ \/ /  | | \ |
   //      //      //    |   _/    \  /   | | | |
  //      //      //     | | \     /  \   | | | |
 //  _   //  _   //  _   | |\ \   / /\ \  | |_/ |
//  (_) //  (_) //  (_)  |_| \_\ /_/  \_\ |____/ 

*/
// 希望取模不要取错。。。

#include <bits/stdc++.h>
#define N 100020
#define mod 1000000007
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int n, m;
int a[N];
void add(int x, int v) {
	for (; x <= n; x += x & -x)
		a[x] = (a[x] + v) % mod;
}
int ask(int x) {
	int ans = 0;
	for (; x; x ^= x & -x)
		ans = (ans + a[x]) % mod;
	return ans;
}
void ask_add(int l, int r, int x) {
	add(l, x);
	add(r+1, mod - x);
}
int ans[N];
void ans_add(int l, int r, int x) {
	ans[l] = (ans[l] + x) % mod;
	ans[r+1] = (ans[r+1] - x + mod) % mod;
}
int q[N], l[N], r[N];
int main(int argc, char const *argv[]) {
	freopen("seaco.in", "r", stdin);
	freopen("seaco.out", "w", stdout);
	n = read(), m = read();
	for (int i = 1; i <= m; i++) {
		q[i] = read();
		l[i] = read();
		r[i] = read();
	}
	ask_add(1, n, 1);
	for (int i = m; i; i--) {
		int k = ask(i);
		if (q[i] == 1) {
			ans_add(l[i], r[i], k);
		} else {
			ask_add(l[i], r[i], k);
		}
	}
	for (int i = 1; i <= n; i++)
		printf("%d%c", ans[i] = (ans[i] + ans[i-1]) % mod, i == n ? '\n' : ' ');
	return 0;
}
// niconiconi~
// anatano haatoni niconiconi~
// egaotodokeru yazawaniconico~
// niconi~te oboeteru rabunico~

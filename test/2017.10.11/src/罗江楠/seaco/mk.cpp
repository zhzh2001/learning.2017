#include <bits/stdc++.h>
using namespace std;
int getrand() {
	return rand()*rand();
}
int main(int argc, char const *argv[]) {
	freopen("seaco.in", "w", stdout);
	srand(time(0));
	int n = 100000, m = 100000;
	printf("%d %d\n", n, m);
	for (int i = 1; i <= m; i++) {
		int x = rand()&1;
		if (i == 1) x = 1;
		if (x == 1) {
			int l = getrand() % n + 1;
			int r = getrand() % n + 1;
			while (l > r) {
				l = getrand() % n + 1;
				r = getrand() % n + 1;
			}
			printf("1 %d %d\n", l, r);
		} else {
			int l = getrand() % i + 1;
			int r = getrand() % (i - 1) + 1;
			while (l > r) {
				l = getrand() % i + 1;
				r = getrand() % (i - 1) + 1;
			}
			printf("2 %d %d\n", l, r);
		}
	}
	return 0;
}
@echo off
g++ -o mk.exe mk.cpp -O2 -std=c++11
for /L %%i in (1, 1, 100) do (
	echo Making data #%%i ...
	mk
	echo Running on test #%%i ...
	seaco
)

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void Write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)Write(x/10);putchar(x%10+'0');}
inline void writeln(int x){Write(x);puts("");}
int n,m,deep[100010],a[100010],maxm,q[100010],ans[100010];
int nedge=0,p[200010],nex[200010],head[200010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int fa){
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);a[x]^=a[p[k]];
	}
}
signed main()
{
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		int x=read()+1,y=read()+1;
		addedge(x,y);addedge(y,x);
	}
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++){
		int x=read();q[x]=i;
		if(x>maxm)maxm=x;
	}
	for(int i=1;i<=maxm;i++){
		dfs(1,0);
		if(q[i])ans[q[i]]=a[1];
	}
	for(int i=1;i<=m;i++)writeln(ans[i]);
	return 0;
}

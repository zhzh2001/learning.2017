#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
const ll MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void Write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)Write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){Write(x);puts("");}
struct szsz{
	ll f[200010],n;
	inline void add(int x,ll v){for(;x<=n;x+=x&-x)f[x]=((f[x]+v)%MOD+MOD)%MOD;}
	inline ll ssum(int x){ll ans=0;for(;x;x-=x&-x)ans=((ans+f[x])%MOD+MOD)%MOD;return ans;}
}s[2];
struct ppap{int op,x,y;}q[100010];
int n,m;
int main()
{
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read();m=read();s[0].n=n;s[1].n=m;
	for(int i=1;i<=m;i++)q[i].op=read(),q[i].x=read(),q[i].y=read();
	for(int i=1;i<=m;i++)s[1].add(i,1),s[1].add(i+1,-1);
	for(int i=m;i;i--){
		ll k=s[1].ssum(i);
		if(q[i].op==1){
			s[0].add(q[i].x,k);s[0].add(q[i].y+1,-k);
		}else{
			s[1].add(q[i].x,k);s[1].add(q[i].y+1,-k);
		}
	}
	for(int i=1;i<=n;i++)Write(s[0].ssum(i)),putchar(' ');
	return 0;
}

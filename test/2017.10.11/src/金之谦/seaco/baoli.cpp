#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
const ll MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void Write(ll x){if(x<0)putchar('-'),x=-x;if(x>=10)Write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){Write(x);puts("");}
int n,m,a[100010],x[100010],y[100010],op[100010];
inline void dfs(int p){
	if(op[p]==1){
		for(int i=x[p];i<=y[p];i++)a[i]=(a[i]+1)%MOD;
	}else for(int i=x[p];i<=y[p];i++)dfs(i);
}
int main()
{
	freopen("seaco.in","r",stdin);
	freopen("my.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		op[i]=read();x[i]=read();y[i]=read();
		dfs(i);
	}
	for(int i=1;i<=n;i++)Write(a[i]),putchar(' ');
}

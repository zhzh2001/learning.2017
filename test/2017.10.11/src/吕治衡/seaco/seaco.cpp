#include<bits/stdc++.h>
using namespace std;
int n,q,lsg,a[1000000],b[1000000],c[1000000],f1[1000000],f2[1000000];
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1,c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return k*kk;
}
int findit1(int x){int ans=0;for (;x;x-=x&(-x))(ans+=f1[x])%=lsg;return ans;}
int findit2(int x){int ans=0;for (;x;x-=x&(-x))(ans+=f2[x])%=lsg;return ans;}
int putit1(int x,int y){for (;x<=q;x+=x&(-x))(f1[x]+=y)%=lsg;}
int putit2(int x,int y){for (;x<=n;x+=x&(-x))(f2[x]+=y)%=lsg;}
int main(){
	freopen("seaco.in","r",stdin);freopen("seaco.out","w",stdout);
	n=read();q=read();lsg=1e9+7;
	for (int i=1;i<=q;i++)a[i]=read(),b[i]=read(),c[i]=read();
	for (int i=q;i>=1;i--){
		int k=findit1(i)+1;
		if (a[i]==1)putit2(b[i],k),putit2(c[i]+1,-k);
			else putit1(b[i],k),putit1(c[i]+1,-k);
	}for (int i=1;i<=n;i++)printf("%d ",(findit2(i)+lsg)%lsg);
}

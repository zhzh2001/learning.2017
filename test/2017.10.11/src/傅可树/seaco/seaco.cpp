#include<cstdio>
#define int long long 
using namespace std;
const int maxn=1e5+5,mod=1e9+7;
struct pp{
	int opt,l,r;
}qu[maxn];
struct node{
	int lazy,l,r,val;
}a[700000];
int ans[maxn],sum[maxn],n,q;
int read() {
	char c = getchar(); int x = 0, f = 1;
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}
void write(int x) {
	if (x >= 10) write(x / 10);
	putchar(x % 10 + 48);
}
void build(int k,int l,int r)
{
 	 a[k].l=l; a[k].r=r; a[k].lazy=a[k].val=0; int mid=(l+r)>>1;
 	 if (l==r){
 	 	return;
	  }
	  build(k<<1,l,mid); build(k<<1|1,mid+1,r);
}
inline void calc(int k,int v)
{
	(a[k].val+=(a[k].r-a[k].l+1)*v)%=mod;
	(a[k].lazy+=v)%=mod;
}
inline void pushdown(int k)
{
	if (a[k].lazy){
		calc(k<<1,a[k].lazy); calc(k<<1|1,a[k].lazy);
		a[k].lazy=0;
	}
}
void update(int k,int x,int y,int v)
{
	pushdown(k); int l=a[k].l,r=a[k].r,mid=(l+r)>>1;
	if (l==x&&r==y){
		calc(k,v);
		return;
	}
	if (mid>=y) update(k<<1,x,y,v);
	   else if (mid<x) update(k<<1|1,x,y,v);
	   		else update(k<<1,x,mid,v),update(k<<1|1,mid+1,y,v);
}
inline int query(int k,int x)
{
	pushdown(k); int l=a[k].l,r=a[k].r,mid=(l+r)>>1;
	if (l==r) return a[k].val;
	if (mid>=x) return query(k<<1,x);
	   else return query(k<<1|1,x);
}
inline void push(int k)
{
	pushdown(k); 
	if (a[k].l==a[k].r){
		ans[a[k].l]=a[k].val%mod;
		return;
	} 
	push(k<<1); push(k<<1|1);
}
signed main()
{
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout); 
 	n=read(); q=read();
 	build(1,1,q);
	for (int i=1;i<=q;i++){
		qu[i]=(pp){read(),read(),read()};
		if (qu[i].opt==1){
			update(1,i,i,1);
		}else{
			update(1,qu[i].l,qu[i].r,1);
		}
	}
	for (int i=q;i;i--){
		if (qu[i].opt==1){
			sum[i]=query(1,i)%mod;
		}else{
			int temp=query(1,i);
			update(1,qu[i].l,qu[i].r,temp);
		}
	}
	build(1,1,n);
	for (int i=1;i<=q;i++){
		if (qu[i].opt==1){
			update(1,qu[i].l,qu[i].r,sum[i]);
		}
	}
	push(1);
	for (int i=1;i<=n-1;i++){
		write(ans[i]);
		printf(" ");
	}
	write(ans[n]);
	return 0;
}

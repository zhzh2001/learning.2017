#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
const int maxn=2e5+5;
struct edge{
	int link,next;
}e[maxn];
int n,q,num[maxn],state[63][maxn],vv[maxn],bin[63],tot,head[maxn];
inline void insert(int u,int v){e[++tot]=(edge){v,head[u]}; head[u]=tot;}
int read() {
	char c = getchar(); int x = 0, f = 1;
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}
void write(int x) {
	if (x >= 10) write(x / 10);
	putchar(x % 10 + 48);
}
void writeln(int x)
{
	write(x) ; printf("\n");
}
void dfs(int u,int fa,int bit)
{
	int sum=vv[u],sum2=0,sum3=0,cnt=0,res=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u,bit); ++cnt;
			if (state[bit][v]==0||state[bit][v]==1){
				sum^=vv[v];
			}
			if (state[bit][v]==2){
				sum2++;
			}
			if (state[bit][v]==3){
				sum3++;
			}
		}
	}
	if (!cnt){
		state[bit][u]=sum;
		return;
	}
	int temp=min(sum2,sum3);
	sum2-=temp; sum3-=temp;
	if (temp&1){
		sum^=1;
	}else{
		sum^=0;
	}
	if ((!sum2)&&(!sum3)){
		state[bit][u]=sum;
	}else{
		if (sum2){
			if (sum2&1){
			   if (sum==0){
			   	  state[bit][u]=2;
			   }else{
   		 	   state[bit][u]=3;
			   }
			}else{
				state[bit][u]=sum^0;
			}
		}else{
  		  if (sum3&1){
			   if (sum==0){
			   	  state[bit][u]=3;
			   }else{
   		 	   state[bit][u]=2;
			   }
			}else{
				state[bit][u]=sum^0;
			}
		}
	}
}
inline void work(int x)
{
	int ans=0;
	for (int i=0;i<=61;i++){
		if (state[i][1]==0||state[i][1]==1){
			ans+=state[i][1]*bin[i];
		} 
		if (state[i][1]==2){
			if (x%2==0){
				ans+=bin[i];
			}
		}
		if (state[i][1]==3){
			if (x&1){
				ans+=bin[i];
			}
		}
	}
	writeln(ans);
}
signed main()
{
	bin[0]=1;
	for (int i=1;i<=61;i++) bin[i]=bin[i]<<1;
	n=read(); q=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read(); u++; v++;
		insert(u,v);
	}
	for (int i=1;i<=n;i++){
		num[i]=read();
	}
	for (int i=0;i<=61;i++){
		for (int j=1;j<=n;j++){
			if (num[j]&bin[i]){
				vv[i]=1;
			}else{
				vv[i]=0;
			}
		}
		dfs(1,0,i);
	}
	for (int i=1;i<=q;i++){
		int x=read();
		if (x==0){
		   writeln(num[0]);
		}else{
			work(x);
		}
	}
	return 0;
}

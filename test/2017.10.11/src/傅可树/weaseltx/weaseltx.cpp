#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
const int maxn=5005;
struct pp{
	int id,x;
}qu[maxn];
struct edge{
	int link,next;
}e[maxn<<1];
int val[maxn],ans[maxn],mx,tot,n,q,head[maxn];
int read() {
	char c = getchar(); int x = 0, f = 1;
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}
void write(int x) {
	if (x >= 10) write(x / 10);
	putchar(x % 10 + 48);
}
void writeln(int x)
{
	write(x) ; printf("\n");
}
inline void dfs(int u,int fa)
{
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			val[u]^=val[v];
		}
	}
}
inline void insert(int u,int v){e[++tot]=(edge){v,head[u]}; head[u]=tot;}
inline bool cmp(pp a,pp b){return a.x<b.x;} 
signed main()
{
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout); 
	n=read(); q=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read(); u++; v++;
		insert(u,v); insert(v,u);
	}
	for (int i=1;i<=n;i++){
		val[i]=read();
	}
	for (int i=1;i<=q;i++){
		qu[i].x=read();
		qu[i].id=i;
		mx=max(qu[i].x,mx);
	}
	sort(qu+1,qu+1+q,cmp); int now=0;
	while (now<n&&qu[now+1].x==0){
		  ans[qu[++now].id]=val[1];
	}
	for (int i=1;i<=mx;i++){
		dfs(1,0);
		while (now<n&&qu[now+1].x==i){
			ans[qu[++now].id]=val[1];
		}
	}
	for (int i=1;i<=q;i++){
		writeln(ans[i]);
	}
	return 0;
}

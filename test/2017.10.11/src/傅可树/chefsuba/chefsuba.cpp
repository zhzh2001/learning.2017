#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=100005;
int n,k,q,a[maxn];
char s[maxn];
inline void doit()
{
	int temp=a[n];
	for (int i=n-1;i>=1;i--) a[i+1]=a[i];
	a[1]=temp;
}
inline void solve()
{
	int ans=0,one=0;
	for (int i=1;i<=n;i++){
		if (i>k&&a[i-k]==1){
			one--;
		}
		if (a[i]==1) one++;
		ans=max(one,ans);
	}
	printf("%d\n",ans);
}
int main()
{
	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout); 
	scanf("%d%d%d",&n,&k,&q);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
	scanf("%s",s+1);
	for (int i=1;i<=q;i++){
		if (s[i]=='!'){
			doit();
		}else{
			solve();
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll int
#define N 200005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,k,q,num,l,r,a[N],f[N],maxn[N<<2]; char s[N];
void build(ll l,ll r,ll p){
	if (l==r) { maxn[p]=f[l]; return; }
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	maxn[p]=max(maxn[p<<1],maxn[p<<1|1]);
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return maxn[p];
	ll mid=l+r>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return max(query(l,mid,s,mid,p<<1),query(mid+1,r,mid+1,t,p<<1|1));
}
int main(){
	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout);
	n=read(); k=read(); q=read();
	rep(i,1,n) a[i]=a[i+n]=read();
	rep(i,1,k) num+=a[i]; f[1]=num;
	rep(i,2,2*n-k+1) f[i]=num=num+a[i+k-1]-a[i-1];
	build(1,2*n-k+1,1);
	scanf("%s",s+1); l=1; r=k;
	rep(i,1,q){
		if (s[i]=='?') printf("%d\n",query(1,2*n-k+1,l,r,1));
		else { --l; --r; if (!l) l=n,r=n+k-1; }
	}
}

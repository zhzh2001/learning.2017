#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll int
#define N 1005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,q,tot,head[N],a[N][N];
struct edge{ ll to,next; }e[N<<1];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs(ll u,ll last,ll day){
	a[u][day]=a[u][day-1];
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dfs(v,u,day);
		a[u][day]^=a[v][day];
	}
}
int main(){
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read(); q=read();
	rep(i,1,n-1){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	rep(i,0,n-1) a[i][0]=read();
	rep(i,1,1000) dfs(0,-1,i);
	rep(i,1,q) printf("%d\n",a[0][read()]);
}

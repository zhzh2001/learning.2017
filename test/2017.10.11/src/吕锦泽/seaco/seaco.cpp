#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define N 200005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,c[N],f[N];
struct data{ ll opt,l,r; }q[N];
void add(ll x,ll v){
	for (ll i=x;i<=m;i+=i&-i) (c[i]+=v)%=mod;
}
ll get(ll x){
	ll num=0;
	for (ll i=x;i;i-=i&-i) (num+=c[i])%=mod;
	return num;
}
void change(ll l,ll r,ll v){
	(f[l]+=v)%=mod; (f[r+1]-=v)%mod;
}
int main(){
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read(); m=read();
	rep(i,1,m){
		ll opt=read(),l=read(),r=read();
		q[i]=(data){opt,l,r};
	}
	add(1,1);
	per(i,m,1){
		ll x=get(i);
		if (q[i].opt==1) change(q[i].l,q[i].r,x);
		else add(q[i].l,x),add(q[i].r+1,-x);
	}
	rep(i,1,n){
		f[i]=((f[i]+f[i-1])%mod+mod)%mod;
		printf("%lld ",f[i]);
	}
}

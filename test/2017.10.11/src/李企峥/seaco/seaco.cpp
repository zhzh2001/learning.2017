#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std; 
struct D{
	int l,r,mid;
	ll jzq;
}a[1000000],b[1000000],c[1000000];
int n,m;
ll ans[200000];
ll lzq;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void build1(int x,int l,int r)
{
	a[x].l=l;a[x].r=r;
	a[x].mid=(l+r)/2;
	a[x].jzq=0;
	if(l==r)return;
	build1(x*2,l,a[x].mid);
	build1(x*2+1,a[x].mid+1,r);
}
void build2(int x,int l,int r)
{
	b[x].l=l;b[x].r=r;
	b[x].mid=(l+r)/2;
	b[x].jzq=0;
	//cout<<x<<" "<<b[x].l<<" "<<b[x].r<<" "<<b[x].mid<<endl;
	if(l==r)return;
	build2(x*2,l,b[x].mid);
	build2(x*2+1,b[x].mid+1,r);
}
ll find1(int x,int y)
{
	if(a[x].l==a[x].r&&a[x].r==y)return a[x].jzq;
	ll zyy=0;
	if(y<=a[x].mid)zyy=(a[x].jzq+find1(x*2,y))%mo;
			else zyy=(a[x].jzq+find1(x*2+1,y))%mo;
	return zyy;
}
ll find2(int x,int y)
{
	//cout<<b[x].l<<" "<<b[x].r<<" "<<y<<endl;
	if(b[x].l==b[x].r&&b[x].r==y)return b[x].jzq;
	ll zyy=0;
	if(y<=b[x].mid) zyy=b[x].jzq+find2(x*2,y);
			else zyy=b[x].jzq+find2(x*2+1,y);
	return zyy;
}
void faq1(int x,int l,int r,ll lzq)
{
	if(l==a[x].l&&r==a[x].r)
	{
		a[x].jzq+=lzq;
		a[x].jzq%=mo;
		return;
	}
	if(r<=a[x].mid)
	{
		faq1(x*2,l,r,lzq);
		return;
	}
	if(l>a[x].mid)
	{
		faq1(x*2+1,l,r,lzq);
		return;
	}
	if(l<=a[x].mid&&r>a[x].mid)
	{
		faq1(x*2,l,a[x].mid,lzq);
		faq1(x*2+1,a[x].mid+1,r,lzq);
		return;
	}
}
void faq2(int x,int l,int r,ll lzq)
{
	if(l==b[x].l&&r==b[x].r)
	{
		b[x].jzq+=lzq;
		b[x].jzq%=mo;
		return;
	}
	if(r<=b[x].mid)
	{
		faq2(x*2,l,r,lzq);
		return;
	}
	if(l>b[x].mid)
	{
		faq2(x*2+1,l,r,lzq);
		return;
	}
	if(l<=b[x].mid&&r>b[x].mid)
	{
		faq2(x*2,l,b[x].mid,lzq);
		faq2(x*2+1,b[x].mid+1,r,lzq);
		return;
	}
}
int main()
{
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read();m=read();
	build1(1,1,n);
	build2(1,1,m);
	For(i,1,m)
	{
		c[i].jzq=read();
		c[i].l=read();
		c[i].r=read();
	}
	Rep(i,1,m)
	{
		lzq=find2(1,i)+1;
		lzq%=mo;
		if(c[i].jzq==2)faq2(1,c[i].l,c[i].r,lzq);
				else faq1(1,c[i].l,c[i].r,lzq);
	}
	For(i,1,n)ans[i]=find1(1,i)%mo;
	For(i,1,n)cout<<ans[i]<<" ";
	return 0;
}


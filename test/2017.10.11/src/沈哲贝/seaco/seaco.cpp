#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 400010
#define mk make_pair
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
struct data{	ll x,y,z;	}q[maxn];
const ll mod=1e9+7;
ll tag[maxn],n,Q,mark[maxn],tot,use[maxn],ans;
vector<pair<ll,ll> >g[maxn/2];
void pushdown(ll p){
	if (tag[p]){	tag[p<<1]=(tag[p<<1]+tag[p])%mod;	tag[p<<1|1]=(tag[p<<1|1]+tag[p])%mod;	tag[p]=0;}
}
ll query(ll l,ll r,ll p,ll x){
	ll mid=(l+r)>>1,ans;
	if (l!=r)	pushdown(p);
	if (l==r)	return tag[p];
	return	x<=mid?query(l,mid,p<<1,x):query(mid+1,r,p<<1|1,x);
}
void add(ll l,ll r,ll p,ll s,ll t,ll v){
	ll mid=(l+r)>>1,ans;
	if (l!=r)	pushdown(p);
	if (l==s&&r==t)	return tag[p]+=v,void(0);
	pushdown(p);
	if (t<=mid)	add(l,mid,p<<1,s,t,v);
	else if (s>mid)	add(mid+1,r,p<<1|1,s,t,v);
	else	add(l,mid,p<<1,s,mid,v),add(mid+1,r,p<<1|1,mid+1,t,v);
}
int main(){
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n=read();	Q=read();
	For(i,1,Q){
		ll opt=read(),x=read(),y=read();
		if (opt==1)	g[x].push_back(mk(1,i)),g[y+1].push_back(mk(-1,i)),mark[i]=1;
		else	q[++tot].x=x,q[tot].y=y,q[tot].z=i;
	}
	FOr(i,tot,1){
		ll x=(query(1,Q,1,q[i].z)+1)%mod;
		add(1,Q,1,q[i].x,q[i].y,x);
	}
	For(i,1,Q)	if (mark[i])	use[i]=(query(1,Q,1,i)+1)%mod;
//	puts("-----------");
	For(i,1,n){
		if (g[i].size())	For(j,0,g[i].size()-1)	ans=(ans+g[i][j].first*use[g[i][j].second])%mod;
		write((ans%mod+mod)%mod);	putchar(' ');
	}
}

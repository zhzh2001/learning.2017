#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll int
#define maxn 400010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
struct data{	ll v,pos;	};
priority_queue<data>q;
ll a[maxn],sum[maxn],n,Q,k,st;
char s[maxn];
bool operator < (data a,data b){	return a.v==b.v?a.pos<b.pos:a.v<b.v;}
int main(){
	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout);
	n=read();	k=min(read(),n);	Q=read();
	For(i,200001,n+200000)	a[i]=read();
	FOr(i,n+200000,200001){
		sum[i]=sum[i+1]+a[i]-a[i+k];
		if (i<=n-k+1+200000)	q.push((data){sum[i],i});
	}st=200000;
	scanf("%s",s+1); 
	For(i,1,Q){
		if (s[i]=='?'){
			data tmp=q.top();	q.pop();
			while(tmp.pos>st+n-k+1)	tmp=q.top(),q.pop();
			writeln(tmp.v);
			q.push(tmp);
		}else{
			a[st]=a[st+n];
			sum[st]=sum[st+1]+a[st]-a[st+k];
			q.push((data){sum[st],st});
			st--;
		}
	}
}
/*
5 3 4
1 0 0 1 1
?!!?
*/

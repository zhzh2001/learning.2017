#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll int
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 100010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll nxt[maxn],head[maxn],vet[maxn],b[maxn],a[maxn],answ[maxn],n,Q,tot;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x,ll pre){
	b[x]=a[x];
	for(ll i=head[x];i;i=nxt[i])	if (vet[i]!=pre)	dfs(vet[i],x),b[x]^=b[vet[i]];
}
int main(){
	freopen("weaseltx.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	Q=read();
	For(i,2,n){
		ll x=read()+1,y=read()+1;
		insert(x,y);	insert(y,x);
	}
	For(i,1,n)	a[i]=read();
	answ[0]=a[1];
	For(i,1,50000){
		dfs(1,-1);
		For(j,1,n)	a[j]=b[j];
		answ[i]=a[1];
//		writeln(answ[i]);
	}
	while(Q--){
		ll x=read();
		writeln(answ[x]);
	}
}

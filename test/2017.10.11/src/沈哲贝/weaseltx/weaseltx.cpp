#include<cstdio>
#include<vector>
#include<cstring>
#include<memory.h>
#include<algorithm>
#include<ctime>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 300010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll answ[maxn],ans[maxn],head[maxn],nxt[maxn],vet[maxn],n,Q,tot,a[maxn],sum;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x,ll pre,ll dep){
	ans[dep]^=a[x];
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=pre)	dfs(vet[i],x,dep+1);
}
void Dfs(int x,int y,int t){
	if (t==1){	answ[x]^=ans[y]^ans[y+1];	answ[x+1]^=ans[y];	return;	}
	Dfs(x,y,t>>1);	Dfs(x+t,y,t>>1);	Dfs(x,y+t,t>>1);
}
int main(){
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n=read();	Q=read();
	For(i,2,n){
		ll x=read()+1,y=read()+1;
		insert(x,y);	insert(y,x);
	}
	For(i,1,n)	a[i]=read(),sum^=a[i];
	dfs(1,-1,0);	if (n<=10000)	n=262144/2;	else	n=262144;
	Dfs(0,0,(int)n/2);
	while(Q--){
		ll x=(read()+n-1)%n;
		/*if (!x)	writeln(ans[0]);
		else	*/writeln(answ[x]);
	}
}

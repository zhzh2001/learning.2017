#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
#define M 400005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

bool op[50][2000005];
int ans[2000005];
int a[200005];
int n,q,Mxdep,tot;
int num[50];
int to[M],nxt[M],head[200005],cnt;

inline void add_edge(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

void dfs(int x,int f,int dep){
	num[dep] ^= a[x];
	Mxdep = max(Mxdep,dep);
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f)
			dfs(to[i],x,dep+1);
}

int main(){
	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);
	n = read(); q = read();
	for(int i = 1;i < n;i++){
		int x = read(),y = read();
		add_edge(x,y);
	}
	for(int i = 0;i < n;i++)
		a[i] = read();
	dfs(0,0,0);
//	printf("%d : ",Mxdep);
//	for(int i = 0;i <= Mxdep;i++)
//		printf("%d ",num[i]);
//	puts("");
	for(int i = 0;i <= Mxdep;i++){
		op[0][i] = true;
		ans[0] ^= num[i];
	}
	tot = 1<<Mxdep;
//	printf("%d\n",tot);
	for(int i = 1;i < tot;i++){
		op[i][0] = true;
		ans[i] = num[0];
		for(int j = 1; j <= Mxdep;j++){
			op[i][j] = op[i][j-1]^op[i-1][j];
			if(op[i][j])
				ans[i] ^= num[j];
		}
	}
	for(int i = 1;i <= q;i++){
		int x = (read()%tot-1+tot)%tot;
		printf("%d\n",ans[x]);
	}
	return 0;
}

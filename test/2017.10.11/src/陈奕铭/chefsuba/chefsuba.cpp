#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
#define N 200005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int a[N],b[N];
int tot,n,k,q,Left;
struct node{
	int l,r,mx;
}tr[N*4];
char s[N];

void build(int num,int l,int r){
	tr[num].l = l; tr[num].r = r;
	if(l == r){
		tr[num].mx = a[l];
		return;
	}
	int mid = (l+r)>>1;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
	tr[num].mx = max(tr[num<<1].mx,tr[num<<1|1].mx);
}

int query(int num,int x,int y){
	int l = tr[num].l,r = tr[num].r;
	if(x <= l && r <= y)
		return tr[num].mx;
	int mid = (l+r)>>1;
	if(y <= mid)
		return query(num<<1,x,y);
	if(x > mid)
		return query(num<<1|1,x,y);
	return max(query(num<<1,x,mid),query(num<<1|1,mid+1,y));
}

int main(){
	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout);
	n = read(); k = read(); q = read();
	for(int i = 1;i <= n;i++){
		a[i] = read();
		b[i] = b[i-1]+a[i];
	}
	for(int i = n+1;i <= n+k-1;i++)
		b[i] = b[i-1]+a[i-n];
	for(int i = 1;i <= n;i++)
		a[i] = b[i+k-1]-b[i-1];
//	for(int i = 1;i <= n;i++)
//		printf("%d ",a[i]);
//	puts("");
	tot = n-k+1;
	Left = 1;
//	printf("%d\n",tot);
	build(1,1,n);
	scanf("%s",s+1);
	for(int i = 1;i <= q;i++){
		if(s[i] == '!'){
			Left--;
			if(Left == 0) Left = n;
		}
		else{
			if(Left + tot-1 > n)
				printf("%d\n",max(query(1,Left,n),query(1,1,tot-n+Left-1)));
			else printf("%d\n",query(1,Left,Left+tot-1));
		}
	}
	return 0;
}

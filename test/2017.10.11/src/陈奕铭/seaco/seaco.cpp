#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
#define N 1005
#define mod 1000000007
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int n,q;
int a[N][N];
int b[N][N];

int main(){
	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);
	n = read(); q = read();
	for(int i = 1;i <= q;i++){
		int opt = read(),x = read(),y = read();
		if(opt == 1){
			for(int j = 1;j <= n;j++)
				a[i][j] = a[i-1][j];
			for(int j = x;j <= y;j++)
				a[i][j]++;
		}
		else{
			for(int j = 1;j <= n;j++)
				a[i][j] = (1ll*a[i-1][j]+a[y][j]-a[x-1][j])%mod;
		}
	}
	for(int i = 1;i <= n;i++)
		printf("%d ",a[q][i]%mod);
	return 0;
}

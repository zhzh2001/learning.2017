#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=10007;
int n,q,a[N];
struct Line{
	int u,v,next;
}e[N<<1];
int head[N];
int cnt;

struct node{
	int id,res,x;
}b[N];
bool operator<(const node&x,const node&y){
	return x.x<y.x;
}
bool cmp(const node&x,const node&y){
	return x.id<y.id;
}

void ins(int u,int v){
	e[++cnt]=(Line){
		u,v,head[u]
	};
	head[u]=cnt;
}
void insert(int u,int v){ ins(u,v),ins(v,u); }

void dfs(int x,int last){
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (y==last) continue;
		dfs(y,x);
		a[x]=a[x]^a[y];
	}
}

int main(){
	// say hello

	freopen("weaseltx.in","r",stdin);
	freopen("weaseltx.out","w",stdout);

	n=read(),q=read();
	For(i,1,n-1){
		int x=read()+1,y=read()+1;
		insert(x,y);
	}
	For(i,1,n) a[i]=read();
	For(i,1,q){
		b[i].x=read();
		b[i].id=i;
	}
	sort(b+1,b+1+q);
	int now=0;
	For(i,1,q){
		while (now<b[i].x){
			dfs(1,0);
			now++;
		}
		b[i].res=a[1];
	}
	sort(b+1,b+1+q,cmp);
	For(i,1,q) printf("%d\n",b[i].res);



	return 0;
	// say goodbye
}



#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=200007;
int n,q,k,a[N<<1],b[N<<1];
char s[N<<1];
struct Seg_Tree{
	int num;
}tr[N<<2];

#define ls (pos<<1)
#define rs (pos<<1|1)
void Build(int pos,int l,int r){
	if (l==r){
		if (l-k<0) l+=n;
		tr[pos].num=a[l]-a[l-k];
//		printf("Q %d %d\n",r,tr[pos].num);
		return;
	}
	int mid=(l+r)>>1;
	Build(ls,l,mid),Build(rs,mid+1,r);
	tr[pos].num=max(tr[ls].num,tr[rs].num);
}
int Query(int pos,int l,int r,int x,int y){
	if (l==x&&r==y) return tr[pos].num;
	int mid=(l+r)>>1;
	if (y<=mid) return Query(ls,l,mid,x,y);
	else if (x>mid) return Query(rs,mid+1,r,x,y);
	else return max(Query(ls,l,mid,x,mid),Query(rs,mid+1,r,mid+1,y));
}

int main(){
	// say hello

	freopen("chefsuba.in","r",stdin);
	freopen("chefsuba.out","w",stdout);

	n=read(),k=read(),q=read();
	if (k>n) k=n;
	For(i,1,n) a[i]=read(),b[i]=a[i];
	scanf("%s",s);
	For(i,1,2*n){
		a[i]+=a[i-1];
		if (i>n) a[i]+=b[i-n];
	}
//	For(i,1,2*n) printf("%d ",a[i]); printf("\n");
	Build(1,1,n);
	int l=k,r=n;
	For(i,1,q){
		if (s[i-1]=='!'){
			l++,r++;
			if (r>n) r=1;
			if (l>n) l=1;
		}
		else {
		//	printf("%d %d\n",l,r);
			int ans=0;
			if (l<=r) ans=Query(1,1,n,l,r);
			else ans=max(Query(1,1,n,l,n),Query(1,1,n,1,r));
			printf("%d\n",ans);
		}
	}
		
	return 0;
	// say goodbye
}



#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=4e5+7,rxd=1e9+7;

struct Seg_Tree{
	int num[3];
}tr[N<<2];
int n,q,opt[N],l[N],r[N];

#define ls (pos<<1)
#define rs (pos<<1|1)

void Pushdown(int pos,int opt){
	tr[ls].num[opt]+=tr[pos].num[opt];
	tr[rs].num[opt]+=tr[pos].num[opt];
	tr[ls].num[opt]%=rxd;
	tr[rs].num[opt]%=rxd;
	tr[pos].num[opt]=0;
}

void Update(int pos,int l,int r,int x,int y,int num,int opt){
	if (l==x&&r==y){
//		printf("%d %d %d %d\n",x,y,num,opt);
		tr[pos].num[opt]+=num;
		tr[pos].num[opt]%=rxd;
		return;
	}
	Pushdown(pos,opt);
	int mid=(l+r)>>1;
	if (y<=mid) Update(ls,l,mid,x,y,num,opt);
	else if (x>mid) Update(rs,mid+1,r,x,y,num,opt);
	else Update(ls,l,mid,x,mid,num,opt),Update(rs,mid+1,r,mid+1,y,num,opt);
}
int Query(int pos,int l,int r,int x,int opt){
	if (l==r) return tr[pos].num[opt];
	Pushdown(pos,opt);
	int mid=(l+r)>>1,res=0;
	if (x<=mid) res=Query(ls,l,mid,x,opt);
	else res=Query(rs,mid+1,r,x,opt);
	return res;
}


int main(){
	// say hello

	freopen("seaco.in","r",stdin);
	freopen("seaco.out","w",stdout);

	n=read(),q=read();
	For(i,1,q) opt[i]=read(),l[i]=read(),r[i]=read();
	Rep(i,1,q){
		int k=Query(1,1,q,i,2)+1;
		if (opt[i]==2) Update(1,1,q,l[i],r[i],k,2);
		else Update(1,1,n,l[i],r[i],k,1);
	}
	For(i,1,n) printf("%d ",Query(1,1,n,i,1));

	return 0;
	// say goodbye
}



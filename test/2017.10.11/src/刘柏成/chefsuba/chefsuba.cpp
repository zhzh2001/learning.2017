#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
#define filename "chefsuba"
void init(){
	freopen(filename".in","r",stdin);
	freopen(filename".out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[400002],sum[400002];
char s[400002];
priority_queue<int> qwq;
int cnt[400002];
int main(){
	init();
	int n=readint(),k=readint(),q=readint();
	//puts("WTF");
	for(int i=n;i;i--){
		a[i]=readint();
		//printf("%d\n",a[i]);
	}
	for(int i=n+1;i<=q+n*2;i++){
		int t=i%n;
		if (!t)
			t=n;
		a[i]=a[t];
	}
	for(int i=1;i<=q+n*2;i++){
		sum[i]=a[i]+sum[i-1];
		//printf("%d\n",sum[3]);
	}
	int st=0;
	readstr(s+1);
	for(int i=k;i<=n;i++){
		int w=sum[i]-sum[i-k];
		if (!cnt[w])
			qwq.push(w);
		cnt[w]++;
		//printf("k=%d i=%d i-k=%d sum[i]=%d sum[i-k]=%d\n",k,i,i-k,sum[3],sum[i-k]);
	}
	for(int i=1;i<=q;i++){
		if (s[i]=='?'){
			while(!cnt[qwq.top()])
				qwq.pop();
			int ans=qwq.top();
			cnt[ans]--;
			printf("%d\n",ans);
		}else{
			int x=sum[st+k]-sum[st];
			st++;
			int y=sum[st+n]-sum[st+n-k];
			cnt[x]--;
			if (!cnt[y])
				qwq.push(y);
			cnt[y]++;
		}
	}
}

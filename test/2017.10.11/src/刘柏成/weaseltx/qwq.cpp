#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
#define filename "weaseltx"
void init(){
	freopen(filename".in","r",stdin);
	freopen(filename".out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
ll readint(){
	ll val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=5002;
const int maxm=10002;
void putset(int x){
	for(int i=4;i>=0;i--)
		putchar((x&(1<<i))?'1':'0');
	putchar(' ');
}
struct graph{
	struct edge{
		int to,next;
	}e[maxm];
	int n,m;
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	ll a[maxn];
	bitset<maxn> b[maxn];
	int dfn[maxn];
	int dep[maxn];
	int fa[maxn];
	bool vis[maxn];
	int cur=0;
	void dfs(int u){
		dfn[++cur]=u;
		vis[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!vis[v]){
				fa[v]=u;
				dep[v]=dep[u]+1;
				dfs(v);
			}
		}
	}
	ll ans[maxn];
	int tmp[maxn];
	int o;
	void work(){
		for(int i=1;i<=n;i++)
			b[i][i]=1;
		//ans[0]=a[1];
		for(o=1;;o++){
			for(int i=n;i;i--){
				int u=dfn[i];
				//printf("%d ",u);
				b[fa[u]]^=b[u];
			}
			memset(tmp,-1,sizeof(tmp));
			for(int i=1;i<=n;i++)
				tmp[dep[i]]=b[1][i];
			//printf("o=%d\n",o);
			for(int i=0;i<=n;i++){
				if (tmp[i]==-1)
					break;
				putset(i);
				putset(o);
				printf("%d\n",tmp[i]);
			}
			putchar('\n');
			bool flag=true;
			for(int i=1;i<=n;i++)
				if (b[i].count()>1){
					flag=false;
					break;
				}
			if (flag)
				break;
			//ans[o]=b[1];
		}
	}	
}g;
int main(){
	init();
	int n=g.n=readint(),q=readint();
	for(int i=1;i<n;i++){
		int u=readint()+1,v=readint()+1;
		//printf("adding %d %d\n",u,v);
		g.addedge(u,v);
		g.addedge(v,u);
	}
	g.dfs(1);
	for(int i=1;i<=n;i++)
		g.a[i]=readint();
	g.work();
	printf("%d\n",g.o);
	//printf("%d\n",clock());
	//while(q--)
		//printf("%lld\n",g.ans[readint()%g.o]);
}
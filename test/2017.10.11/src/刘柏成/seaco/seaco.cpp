#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
#define filename "seaco"
void init(){
	freopen(filename".in","r",stdin);
	freopen(filename".out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int now[1002][1002];
int main(){
	init();
	int n=readint(),q=readint();
	for(int i=1;i<=q;i++){
		int op=readint(),l=readint(),r=readint();
		if (op==1){
			int *f=now[i],*g=now[i-1];
			for(int j=1;j<=n;j++)
				f[j]=g[j];
			for(int j=l;j<=r;j++){
				f[j]++;
				if (f[j]==mod)
					f[j]=0;
			}
		}else{
			int *f=now[r],*g=now[l-1],*x=now[i],*y=now[i-1];
			for(int j=1;j<=n;j++){
				x[j]=y[j]+f[j]-g[j];
				if (x[j]>=mod)
					x[j]-=mod;
				if (x[j]<0)
					x[j]+=mod;
			}
		}
	}
	for(int i=1;i<=n;i++)
		printf("%d ",now[q][i]);
}
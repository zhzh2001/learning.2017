#include<fstream>
using namespace std;
ifstream fin("seaco.in");
ofstream fout("seaco.out");
const int N=1005,MOD=1e9+7;
int prefix[N][N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		int opt,l,r;
		fin>>opt>>l>>r;
		if(opt==1)
			for(int j=1;j<=n+1;j++)
				if(j==l)
					prefix[i][j]=(prefix[i-1][j]+1)%MOD;
				else
					if(j==r+1)
						prefix[i][j]=(prefix[i-1][j]+MOD-1)%MOD;
					else
						prefix[i][j]=prefix[i-1][j];
		else
			for(int j=1;j<=n+1;j++)
				prefix[i][j]=((prefix[i-1][j]+prefix[r][j]-prefix[l-1][j])%MOD+MOD)%MOD;
	}
	int now=0;
	for(int i=1;i<=n;i++)
		fout<<(now=(now+prefix[m][i])%MOD)<<' ';
	fout<<endl;
	return 0;
}

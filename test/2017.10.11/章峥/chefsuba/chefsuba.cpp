#include<fstream>
#include<string>
using namespace std;
ifstream fin("chefsuba.in");
ofstream fout("chefsuba.out");
const int N=100000;
int a[N],b[N*2],ans[N],Q[N*2];
int main()
{
	int n,k,q;
	fin>>n>>k>>q;
	for(int i=0;i<n;i++)
		fin>>a[i];
	int now=0;
	for(int i=0;i<k;i++)
		now+=a[i];
	for(int i=0;i<n;i++)
	{
		b[i]=now;
		now=now-a[i]+a[(i+k)%n];
	}
	for(int i=n;i<n+n-k;i++)
		b[i]=b[i%n];
	int l=1,r=0;
	for(int i=0;i<n-k+1;i++)
	{
		while(l<=r&&b[i]>=b[Q[r]])
			r--;
		Q[++r]=i;
	}
	for(int i=0;i<n;i++)
	{
		ans[i]=b[Q[l]];
		if(Q[l]==i)
			l++;
		while(l<=r&&b[i+n-k+1]>=b[Q[r]])
			r--;
		Q[++r]=i+n-k+1;
	}
	string cmd;
	fin>>cmd;
	now=0;
	for(int i=0;i<q;i++)
		if(cmd[i]=='?')
			fout<<ans[now]<<endl;
		else
			now=(now+n-1)%n;
	return 0;
}

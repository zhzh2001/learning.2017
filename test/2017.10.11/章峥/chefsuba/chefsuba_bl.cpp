#include<fstream>
#include<algorithm>
#include<string>
using namespace std;
ifstream fin("chefsuba.in");
ofstream fout("chefsuba.ans");
const int N=10005;
int a[N];
int main()
{
	int n,k,q;
	fin>>n>>k>>q;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	string cmd;
	fin>>cmd;
	for(int i=0;i<q;i++)
		if(cmd[i]=='?')
		{
			int now=0;
			for(int j=1;j<=k;j++)
				now+=a[j];
			int ans=0;
			for(int j=k;j<=n;j++)
			{
				ans=max(ans,now);
				now=now-a[j-k+1]+a[j+1];
			}
			fout<<ans<<endl;
		}
		else
			rotate(a+1,a+n,a+n+1);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cctype>
#include<vector>
#include<algorithm>
#include<set>
using namespace std;
FILE *fin=fopen("weaseltx.in","r"),*fout=fopen("weaseltx.out","w");
const int N=200000,SZ=1e6;
char buf[SZ],*p=buf,*pend=buf;
inline int nextchar()
{
	if(p==pend)
	{
		pend=buf+fread(buf,1,SZ,fin);
		if(pend==buf)
			return EOF;
		p=buf;
	}
	return *p++;
}
template<typename Int>
inline void read(Int& x)
{
	char c=nextchar();
	for(;isspace(c);c=nextchar());
	x=0;
	Int sign=1;
	if(c=='-')
	{
		sign=-1;
		p++;
	}
	for(;isdigit(c);c=nextchar())
		x=x*10+c-'0';
	x*=sign;
}
inline void writechar(char c)
{
	if(p==pend)
	{
		fwrite(buf,1,SZ,fout);
		p=buf;
	}
	*p++=c;
}
inline void flush()
{
	fwrite(buf,1,p-buf,fout);
}
int dig[20];
template<typename Int>
inline void writeln(Int x)
{
	if(x<0)
	{
		writechar('-');
		x=-x;
	}
	int len=0;
	do
		dig[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		writechar(dig[len]+'0');
	writechar('\n');
}
int head[N],v[N*2],nxt[N*2],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
long long a[N],q[N];
vector<long long> ans;
void dfs(int k,int fat)
{
	for(int i=head[k];~i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],k);
			a[k]^=a[v[i]];
		}
}
int main()
{
	int n,m;
	read(n);read(m);
	memset(head,-1,sizeof(head));
	for(int i=1;i<n;i++)
	{
		int u,v;
		read(u);read(v);
		add_edge(u,v);
		add_edge(v,u);
	}
	for(int i=0;i<n;i++)
		read(a[i]);
	for(int i=0;i<m;i++)
		read(q[i]);
	set<long long> S;
	S.insert(a[0]);
	ans.push_back(a[0]);
	long long maxx=*max_element(q,q+m);
	for(int now=1;now<=maxx;now++)
	{
		dfs(0,-1);
		if(S.find(a[0])!=S.end())
			break;
		ans.push_back(a[0]);
		S.insert(a[0]);
	}
	p=buf;pend=buf+SZ;
	for(int i=0;i<m;i++)
		writeln(ans[q[i]%ans.size()]);
	flush();
	return 0;
}

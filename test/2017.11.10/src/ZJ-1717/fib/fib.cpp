#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;

inline int read() {
	int x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}

const int Mx = 1e9;
int a,b,t,tot;
int f[60];
bool flag;

int main() {
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	a = 0; b = 1; tot = 2;
	f[1] = 0; f[2] = 1;
	while(b<=Mx) {
		t = b;
		b = a+b;
		a = t;
		f[++tot] = b;
	}
	tot--;
	
	int T = read();
	while(T--) {
		int x=read();
		flag = 0;
		For(i, 1, tot) 
		  For(j, 1, tot) 
		    if(1ll*f[i]*f[j]==x) {
		    	flag = 1;
			} 
		if(flag) puts("Yes");
		else puts("No");
	}
}









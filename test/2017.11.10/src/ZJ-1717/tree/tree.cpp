#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;
inline int read() {
	int x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}
inline void write(int x) {
	if( x < 0 ) putchar('-') ;
	if( x > 9 ) write(x/10) ;
	putchar(x%10+48) ;
}
inline void writeln(int x) {
	write(x) ;
	putchar('\n') ;
}

const int N = 100011;
struct node{
	int to,pre;
}e[N*2];
int fa[N][21],In[N],Out[N],dep[N],head[N],sz[N]; 
int n,Q,cnt,Ti,K;

inline void add(int x,int y) {
	e[++cnt].to = y;
	e[cnt].pre = head[x];
	head[x] = cnt;
}

void dfs(int u,int F) {
	sz[u]=1;
	fa[u][0] = F; dep[u] = dep[F]+1;
	In[u] = ++Ti;
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to;
		if(v!=F) {
			dfs(v,u); 
			sz[u]+=sz[v];
		}
	}
	Out[u] = ++Ti;
}
inline void pre() {
	dfs(1,1); 
	fa[1][0] = 1;
	For(j, 1, 20) 
	  For(i, 1, n) 
	  	fa[i][j] = fa[ fa[i][j-1] ][j-1];  
}

inline bool judge(int u,int v) {
	return In[u]<=In[v] && Out[v]<=Out[u];
}

inline int getlca(int u,int v) {
	if(u==v) return u;
	if(judge(u,v)) return u;
	if(judge(v,u)) return v;
	if(dep[u]>dep[v]) swap(u,v);
	int k = u;
	Dow(i, 20, 0) 
		if( !judge( fa[k][i], v ) ) k = fa[k][i]; 
	return fa[k][0];
}

inline int query(int u,int len) {
	int res = u;
	For(i, 0, 20) 
		if( len&(1<<i) ) res = fa[res][i];
	return res; 
}

int main() {
	freopen("tree.in","r",stdin); 
	freopen("tree.out","w",stdout); 
	int T = read(); 
	while(T--) {
	n=read(); K = read();
	For(i, 1, n-1) {
		int u=read(), v=1;
	}
	printf("%d\n",(K+1)/2);
	//pre(); 
	Q = 0;
	while(Q) {
		int u=read(), v=read();
		if(u==v) {
			printf("%d\n",n);
			continue; 
		}
 		int lca = getlca(u,v);
 		if(dep[u] < dep[v]) swap(u,v);
		if( (dep[u]+dep[v]-2*dep[lca])%2==1 ) {
			puts("0");
			continue;
		}
		
		int len = dep[u]+dep[v]-2*dep[lca]; 
		len/=2;  
		int mid = query(u,len); int x = query(u,len-1); 
		if(lca==mid) {
			int y = query(v,len-1);
			writeln(n-sz[x]-sz[y]);
		}
		else 
			writeln(sz[mid]-sz[x]);
	}
	}
	return 0;
}









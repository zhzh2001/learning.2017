#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define ll int
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=200100;
ll dep[N],sz[N],fa[N>>1][21],nxt[N],vet[N],head[N],Q,tot,n;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x){
	For(i,1,20)	fa[x][i]=fa[fa[x][i-1]][i-1];
	sz[x]=1;
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=fa[x][0])	dep[vet[i]]=dep[x]+1,fa[vet[i]][0]=x,dfs(vet[i]),sz[x]+=sz[vet[i]];
}
ll get_lca(ll x,ll y){
	if (dep[x]<dep[y])	swap(x,y);
	ll t=dep[x]-dep[y];
	FOr(i,20,0)	if (t>>i&1)	x=fa[x][i];
	FOr(i,20,0)	if (fa[x][i]!=fa[y][i])	x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0];
}
ll get_fa(ll x,ll k){
	FOr(i,20,0)	if (k>>i&1)	x=fa[x][i];
	return x;
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	For(i,2,n){
		ll x=read(),y=read();
		insert(x,y),insert(y,x);
	}dfs(1);
	Q=read();
	while(Q--){
		ll x=read(),y=read();
		if (x==y)	writeln(n);
		else if ((dep[x]^dep[y])&1)	puts("0");
		else if (dep[x]==dep[y]){
			ll t=get_lca(x,y);
			writeln(n-sz[get_fa(x,dep[x]-dep[t]-1)]-sz[get_fa(y,dep[y]-dep[t]-1)]);
		}else{
			if (dep[x]<dep[y])	swap(x,y);
			ll t=get_lca(x,y),ned=(dep[x]-dep[t]+dep[y]-dep[t])>>1;
			writeln(sz[get_fa(x,ned)]-sz[get_fa(x,ned-1)]);
		}
	}
}

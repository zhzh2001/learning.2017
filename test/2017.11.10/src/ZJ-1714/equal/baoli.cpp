#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define ll int
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=200010;
ll top[N],son[N],sz[N],head[N],nxt[N],vet[N],dep[N],fa[N],n,Q,tot;
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x){
	sz[x]=1;
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=fa[x]){
		fa[vet[i]]=x;
		dep[vet[i]]=dep[x]+1;
		dfs(vet[i]);
		if (sz[vet[i]]>sz[son[x]])	son[x]=vet[i];
	}
}
void Dfs(ll x){
	top[x]=son[fa[x]]==x?top[fa[x]]:x;
	for(ll i=head[x];i;i=nxt[i])
	if (vet[i]!=fa[x])	Dfs(vet[i]);
}
ll lca(ll x,ll y){
	for(;top[x]!=top[y];dep[top[x]]<dep[top[y]]?y=fa[top[y]]:x=fa[top[x]]);
	return dep[x]<dep[y]?x:y;
}
ll getdis(ll x,ll y){
	ll t=lca(x,y);
	return dep[x]+dep[y]-2*dep[t];
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,2,n){
		ll x=read(),y=read();
		insert(x,y);	insert(y,x);
	}dfs(1);	Dfs(1);
	Q=read();
	while(Q--){
		ll x=read(),y=read(),ans=0;
		For(i,1,n)	ans+=getdis(x,i)==getdis(y,i);
		writeln(ans);
	}
}

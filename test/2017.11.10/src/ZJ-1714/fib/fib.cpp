#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define ll long long
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll f[100],n;
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0;
	f[1]=1;
	n=1;
	while(f[n]<=1e9)	f[++n]=f[n-1]+f[n-2];
	ll T=read();
	while(T--){
		ll x=read();	bool fl=0;
		For(i,0,n)	For(j,0,n)	fl|=f[i]*f[j]==x;
		puts(fl?"Yes":"No");
	}
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define ll int
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=210000;
ll head[N],nxt[N],vet[N],tot,n,k,f[N][2];
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x){
	ll sum0=0,sum1=0;	f[x][0]=f[x][1]=0;
	for(ll i=head[x];i;i=nxt[i]){
		dfs(vet[i]);
		sum0+=f[vet[i]][0];
		sum1+=f[vet[i]][1];
	}
	for(ll i=head[x];i;i=nxt[i]){
		f[x][1]=max(f[x][1],f[vet[i]][0]+sum1-f[vet[i]][1]+1);
		f[x][0]+=max(f[vet[i]][0],f[vet[i]][1]);
	}
}
ll get_zuidapipei(){	dfs(1);	return max(f[1][0],f[1][1]);	}
int main(){
	freopen("tree.in","r",stdin); 
	freopen("tree.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	k=read();	tot=0;
		memset(head,0,sizeof head);
		For(i,2,n)	insert(read(),i);
		ll t=get_zuidapipei();
		if (t*2>=k)	writeln((k+1)/2);
		else	writeln(k-t);
	}
}
/*
2
4 4
1 2 3
4 3
1 1 1
*/

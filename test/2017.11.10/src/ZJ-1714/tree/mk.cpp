#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<windows.h>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define ll int
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
int main(){
	freopen("tree.in","w",stdout);
	srand(GetTickCount());
	ll T=10;
	writeln(T);
	while(T--){
		ll n=(rand()<<10|rand())%100000+2,k=(rand()<<10|rand())%(n-1)+2;
		printf("%d %d\n",n,k);
		For(i,2,n)	printf("%d ",rand()%(i-1)+1);
		puts("");
	}
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define ll int
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=210000;
ll head[N],nxt[N],vet[N],tot,n,k,dfn[N],q[N],lnk[N],vis[N];
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	++dfn[x];}
ll get_zuidapipei(){
	ll he=0,ta=0,ans=0;
	For(i,1,n)	if (dfn[i]==1)	q[++ta]=i;
	while(he!=ta){
		ll x=q[++he];
		for(ll i=head[x];i;i=nxt[i])
		if (!lnk[x]&&!lnk[vet[i]]){
			lnk[x]=lnk[vet[i]]=1;
			if (--dfn[vet[i]]==1)	q[++ta]=vet[i];
			ans++;
		}else	if (--dfn[vet[i]]==1)	q[++ta]=vet[i];
	}return ans;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	k=read();	tot=0;
		memset(vis,0,sizeof vis);
		memset(lnk,0,sizeof lnk);
		memset(dfn,0,sizeof dfn);
		memset(head,0,sizeof head);
		For(i,2,n){
			ll fa=read();
			insert(fa,i);
			insert(i,fa);
		}
		ll t=get_zuidapipei();
		if (t*2>=k)	writeln((k+1)/2);
		else	writeln(k-t);
	}
}
/*
2
4 4
1 2 3
4 3
1 1 1
*/

#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], cnt;
void insert(int x, int y) {
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
int dp[N][2]; // dp x 0 第 x 位不取
void dfs(int x, int f) {
	dp[x][0] = dp[x][1] = 0;
	for (int i = head[x]; i; i = nxt[i])
		if (to[i] != f) dfs(to[i], x), dp[x][0] += max(dp[to[i]][0], dp[to[i]][1]);
	for (int i = head[x]; i; i = nxt[i])
		if (to[i] != f) dp[x][1] = max(dp[x][1], dp[x][0] - max(dp[to[i]][0], dp[to[i]][1]) + dp[to[i]][0] + 1);
}
int main(int argc, char const *argv[]) {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	int T = read();
	while (T --) {
		int n = read(), m = read();
		cnt = 0; memset(head, 0, sizeof head);
		for (int i = 2; i <= n; i++)
			insert(i, read());
		dfs(1, 0);
		int ans = max(dp[1][0], dp[1][1]);
		// printf("ans = %d\n", ans);
		if (m > ans * 2) {
			printf("%d\n", m - ans);
		} else {
			printf("%d\n", (m + 1) >> 1);
		}
	}
	return 0;
}
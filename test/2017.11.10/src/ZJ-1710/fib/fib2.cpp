#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
map<ll, bool> mp;
ll f[500], cnt;
int main(int argc, char const *argv[]) {
	freopen("fib.in", "r", stdin);
	freopen("fib.out", "w", stdout);
	int n = read();
	f[0] = f[1] = 1;
	mp[1] = 1;
	for (cnt = 2; f[cnt - 1] <= 1e9; cnt++)
		mp[f[cnt] = f[cnt - 1] + f[cnt - 2]] = 1;
	cnt--;
	while (n--) {
		ll x = read(), flag = 0;
		for (int i = 1; i <= cnt && !flag; i++)
			if (x / f[i] * f[i] == x && mp[x / f[i]]) flag = 1;
		puts(flag ? "Yes" : "No");
	}
	return 0;
}

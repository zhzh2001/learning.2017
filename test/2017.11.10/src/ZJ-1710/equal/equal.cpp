#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<1], nxt[N<<1], cnt;
void insert(int x, int y) {
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
int sz[N], fa[N][20], dep[N];
void dfs(int x, int f) {
	fa[x][0] = f; dep[x] = dep[f] + 1; sz[x] = 1;
	for (int i = head[x]; i; i = nxt[i])
		if (to[i] != f) dfs(to[i], x), sz[x] += sz[to[i]];
}
int lca(int x, int y) {
	if (dep[x] < dep[y]) swap(x, y);
	int t = dep[x] - dep[y];
	for (int i = 19; ~i; i --)
		if (t >> i & 1) x = fa[x][i];
	if (x == y) return x;
	for (int i = 19; ~i; i --)
		if (fa[x][i] != fa[y][i])
			x = fa[x][i], y = fa[y][i];
	if (x != y) x = fa[x][0];
	return x;
}
// x 的第 k 个祖先
int kth_father(int x, int k) {
	for (int i = 19; ~i; i --)
		if (k >> i & 1) x = fa[x][i];
	return x;
}
int main(int argc, char const *argv[]) {
	freopen("equal.in", "r", stdin);
	freopen("equal.out", "w", stdout);
	int n = read();
	for (int i = 1; i < n; i++)
		insert(read(), read());
	dfs(1, 0);
	// puts("dfs out");
	for (int i = 1; i < 20; i++)
		for (int j = 1; j <= n; j++)
			fa[j][i] = fa[fa[j][i - 1]][i - 1];
	// puts("sukidayo");
	int m = read();
	for (int i = 1; i <= m; i++) {
		int a = read(), b = read();
		if (a == b) {
			printf("%d\n", n);
			continue;
		}
		int c = lca(a, b);
		if (dep[a] == dep[b]) {
			int k = dep[a] - dep[c] - 1;
			printf("%d\n", sz[1] - sz[kth_father(a, k)] - sz[kth_father(b, k)]);
			continue;
		}
		int len = dep[a] - dep[c] + dep[b] - dep[c];
		if (len & 1) {
			puts("0");
			continue;
		} else {
			int k = len >> 1;
			int d = dep[a] < dep[b] ? b : a;
			printf("%d\n", sz[kth_father(d, k)] - sz[kth_father(d, k - 1)]);
		}
	}
	return 0;
}

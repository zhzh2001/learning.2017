#include <bits/stdc++.h>
#define N 1020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int dis[N][N];
int main(int argc, char const *argv[]) {
	freopen("equal.in", "r", stdin);
	freopen("equal.ans", "w", stdout);
	memset(dis, 63, sizeof dis);
	int n = read();
	for (int i = 1; i <= n; i++) dis[i][i] = 0;
	for (int i = 2; i <= n; i++) {
		int x = read(), y = read();
		dis[x][y] = dis[y][x] = 1;
	}
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
	int m = read();
	while (m --) {
		int a = read(), b = read(), ans = 0;
		for (int i = 1; i <= n; i++)
			ans += dis[a][i] == dis[b][i];
		printf("%d\n", ans);
	}
	return 0;
}
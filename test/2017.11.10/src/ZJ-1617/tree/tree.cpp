#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1e5+3;
int T,i,j,k,n,m,t,d[N],s[N],fa[N],nxt[N],f[2001][2001][2];
void dfs(int u)
{
	s[u]=1;
	for(int i=fa[u];i;i=nxt[i])
		for(dfs(i),j=s[u]+=s[i];--j;)
			for(k=0;k<s[i]&&k<=j;++k)
			{
				f[u][j][0]=max(f[u][j][0],f[u][j-k][0]+max(f[i][k][0],f[i][k][1]));
				f[u][j][1]=max(f[u][j][1],f[u][j-k][1]+max(f[i][k][0],f[i][k][1]));
				if(k<j)f[u][j][1]=max(f[u][j][1],max(f[u][j-k-1][1]+max(f[i][k][0]+1,f[i][k][1]),f[u][j-k-1][0]+max(f[i][k][0]+2,f[i][k][1]+1)));
			}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	for(scanf("%d",&T);T--;printf("%d\n",i)){
	scanf("%d%d",&n,&m),memset(fa+1,0,4*n),memset(f,0,sizeof f);
	for(t=i=1;i++<n;nxt[++t]=fa[j],fa[j]=t)scanf("%d",&j),++d[i],++d[j];
	for(i=0;i++<n;k+=d[i]<2);
	if(k+k>=m||n>2e3)i=m+1>>1;else
	for(dfs(i=1);i<n;++i)if(f[1][i][0]>=m||f[1][i][1]>=m)break;}
}

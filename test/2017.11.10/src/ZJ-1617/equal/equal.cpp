#include<cstdio>
#include<algorithm>
const int N=1e5+3;
int i,j,k,n,m,t,x,d[N],s[N],lg[N],fa[N],en[N*2],nxt[N*2],ft[N][18];
void dfs(int u)
{
	for(s[u]=i=1;i<=lg[d[u]];++i)ft[u][i]=ft[ft[u][i-1]][i-1];
	for(int i=fa[u],v;i;i=nxt[i])if((v=en[i])!=*ft[u])
		*ft[v]=u,d[v]=d[u]+1,dfs(v),s[u]+=s[v];
}
int FF(int x,int t)
{
	for(int i=0;t;t>>=1,++i)if(t&1)x=ft[x][i];
	return x;
}
int LCA(int x,int y)
{
	if((x=FF(x,d[x]-d[y]))==y)return x;
	for(int i=lg[d[x]];~i;--i)
		if(ft[x][i]!=ft[y][i])x=ft[x][i],y=ft[y][i];
	return *ft[x];
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	for(scanf("%d",&n);++t<n+n-1;)
	{
		scanf("%d%d",&i,&j);
		en[t]=j,nxt[t]=fa[i],fa[i]=t;
		en[++t]=i,nxt[t]=fa[j],fa[j]=t;
	}
	for(i=1;i++<n;lg[i]=lg[i/2]+1);
	for(dfs(1),scanf("%d",&m);m--;)
	{
		scanf("%d%d",&i,&j);
		if(i==j){printf("%d\n",n);continue;}
		if(d[i]<d[j])std::swap(i,j);
		if((t=d[i]+d[j]-2*d[x=LCA(i,j)])&1)puts("0");else
		if(i==j)printf("%d\n",n-s[FF(i,t/2-1)]-s[FF(j,t/2-1)]);else
		k=FF(i,t/2-1),printf("%d\n",s[*ft[k]]-s[k]);
	}
}

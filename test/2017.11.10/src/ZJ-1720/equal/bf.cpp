#include<bits/stdc++.h>
using namespace std;
namespace WildFlower{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
}
using namespace WildFlower;
#define N 5005
struct edge{int to,next;}e[N*2];
int head[N],dx[N],dy[N],q[N];
int n,Q,x,y,tot;
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
	e[++tot]=(edge){x,head[y]};
	head[y]=tot;
}
void spfa(int S,int *d){
	for (int i=1;i<=n;i++) d[i]=-1;
	q[1]=S; d[S]=0;
	for (int h=0,t=1;h!=t;)
		for (int x=q[++h],i=head[x];i;i=e[i].next)
			if (d[e[i].to]==-1)
				d[e[i].to]=d[x]+1,q[++t]=e[i].to;
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("bf.out","w",stdout);
	n=read();
	for (int i=1;i<n;i++)
		add(read(),read());
	Q=read();
	while (Q--){
		x=read(); y=read();
		spfa(x,dx); spfa(y,dy);
		int ans=0;
		for (int i=1;i<=n;i++)
			ans+=(dx[i]==dy[i]);
		printf("%d\n",ans);
	}
}

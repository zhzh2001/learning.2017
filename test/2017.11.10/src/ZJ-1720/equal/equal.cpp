#include<bits/stdc++.h>
using namespace std;
namespace WildFlower{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
	inline void write(int x){
		if (!x){
			puts("0");
			return;
		}
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		puts("");
	}
}
using namespace WildFlower;
#define N 100005
struct edge{int to,next;}e[N*2];
int head[N],dep[N],sz[N],fa[N][17];
int n,q,x,y,tot,T,L[N],R[N];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
	e[++tot]=(edge){x,head[y]};
	head[y]=tot;
}
void dfs(int x){
	sz[x]=1; L[x]=++T;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa[x][0]){
			dep[e[i].to]=dep[x]+1;
			fa[e[i].to][0]=x;
			dfs(e[i].to);
			sz[x]+=sz[e[i].to];
		}
	R[x]=T;
}
int kthfa(int x,int k){
	for (int i=0;i<=16;i++,k/=2)
		if (k&1) x=fa[x][i];
	return x;
}
int lca(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	x=kthfa(x,dep[x]-dep[y]);
	for (int i=16;i>=0;i--)
		if (fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0];
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	for (int i=1;i<n;i++)
		add(read(),read());
	dfs(1);
	for (int i=1;i<=16;i++)
		for (int j=1;j<=n;j++)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	q=read();
	while (q--){
		x=read(); y=read();
		if (x==y){
			write(n);
			continue;
		}
		int d=lca(x,y),t;
		int ld=dep[x]-dep[d];
		int rd=dep[y]-dep[d];
		if ((ld+rd)&1){
			puts("0");
			continue;
		}
		if (ld>rd) t=kthfa(x,(ld+rd)/2);
		else t=kthfa(y,(ld+rd)/2);
		int ans=sz[t];
		if (t==d) ans=n;
		if (L[x]>L[t]&&L[x]<=R[t])
			ans-=sz[kthfa(x,dep[x]-dep[t]-1)];
		if (L[y]>L[t]&&L[y]<=R[t])
			ans-=sz[kthfa(y,dep[y]-dep[t]-1)];
		write(ans);
	}
}

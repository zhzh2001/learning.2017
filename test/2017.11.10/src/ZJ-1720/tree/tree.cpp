#include<bits/stdc++.h>
using namespace std;
namespace WildFlower{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0,f=1;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc())
			if (ch=='-') f=-1;
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x*f;
	}
}
using namespace WildFlower;
#define N 100005
struct edge{int to,next;}e[N*2];
int head[N],f[N][2];
int T,n,k,tot;
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
	e[++tot]=(edge){x,head[y]};
	head[y]=tot;
}
void dfs(int x,int fa){
	f[x][0]=0; f[x][1]=-1e9;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			dfs(e[i].to,x);
			f[x][1]=max(f[x][1]+max(f[e[i].to][0],f[e[i].to][1]),
						f[x][0]+f[e[i].to][0]+1);
			f[x][0]+=max(f[e[i].to][0],f[e[i].to][1]);
		}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	T=read();
	while (T--){
		n=read(); k=read(); tot=0;
		memset(head,0,sizeof(head));
		for (int i=2;i<=n;i++)
			add(i,read());
		dfs(1,0);
		int tmp=max(f[1][0],f[1][1]);
		if (2*tmp>=k)
			printf("%d\n",k/2+k%2);
		else printf("%d\n",tmp+(k-2*tmp));
	}
}

#include<bits/stdc++.h>
using namespace std;
int f[50],T,x;
map<int,int> mp;
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0; f[1]=1;
	for (int i=2;i<=44;i++)
		f[i]=f[i-1]+f[i-2];
	for (int i=0;i<=44;i++)
		for (int j=0;j<=44&&1ll*f[i]*f[j]<=1000000000;j++)
			mp[f[i]*f[j]]=1;
	scanf("%d",&T);
	while (T--){
		scanf("%d",&x);
		puts(mp[x]?"Yes":"No");
	}
}

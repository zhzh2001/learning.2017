#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=100005;
int dep[N],ans;
int from[N],to[N],vis[N];
int n,m,x,t,now;
void dfs(int k,int t)
{
	if(now>=m)ans=min(ans,t);
	if(k==n)return;
	int x=from[k],y=to[k];
	vis[x]++,vis[y]++;
	if(vis[x]==1)now++;
	if(vis[y]==1)now++;
	dfs(k+1,t+1);
	vis[x]--;vis[y]--;
	if(!vis[x])now--;
	if(!vis[y])now--;
	dfs(k+1,t);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree2.out","w",stdout);
	int T=read();
	while(T--){
		n=read();m=read();
		ans=1e9;
		memset(from,0,sizeof(from));
		memset(to,0,sizeof(to));
		memset(vis,0,sizeof(vis));
		for(int i=1;i<n;i++){
			x=read();
			from[i]=i+1;
			to[i]=x;
		}
		dfs(1,0);
		writeln(ans);
	}
	return 0;
}

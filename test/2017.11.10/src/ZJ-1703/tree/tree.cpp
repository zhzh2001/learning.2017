#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=100005;
int dep[N],ans;
int head[N],nxt[N<<1],tail[N<<1];
int n,m,x,t;
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k)
{
	dep[k]=1e9;
	for(int i=head[k];i;i=nxt[i]){
		int x=tail[i];
		dfs(x);
		dep[k]=min(dep[x]+1,dep[k]);
	}
	if(dep[k]==1e9)dep[k]=1;
	if(dep[k]==2)ans++,dep[k]=1e9;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int T=read();
	while(T--){
		memset(head,0,sizeof(head));
		n=read();m=read();
		ans=0;
		for(int i=1;i<n;i++){
			x=read();
			addto(x,i+1);
		}
		dfs(1);
		if(ans*2>=m)writeln(m/2+m%2);
		else writeln(m-ans);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=100005;
int son[N],dep[N];
int head[N],nxt[N<<1],tail[N<<1];
int f[N][18];
int x,y,t,n,q;
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa)
{
	for(int i=head[k];i;i=nxt[i]){
		if(tail[i]==fa)continue;
		int x=tail[i];
		dep[x]=dep[k]+1;f[x][0]=k;
		dfs(x,k);
		son[k]+=son[x]+1;
	}
}
int lca(int x,int y)
{
	if(dep[x]<dep[y])swap(x,y);
	int k=dep[x]-dep[y];
	for(int i=17;i>=0;i--)if((1<<i)&k)x=f[x][i];
	if(x==y)return y;
	for(int i=17;i>=0;i--)
		if(f[x][i]!=f[y][i])x=f[x][i],y=f[y][i];
	return f[x][0];
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal2.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		x=read();y=read();
		addto(x,y);
		addto(y,x);
	}
	dep[1]=1;
	dfs(1,0);f[1][0]=1;
	for(int j=1;j<=17;j++)
		for(int i=1;i<=n;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	q=read();
	while(q--){
		x=read();y=read();
		ll ans=0;
		for(int i=1;i<=n;i++)
			if(dep[lca(x,i)]*2-dep[x]-dep[i]==dep[lca(y,i)]*2-dep[y]-dep[i])ans++;
		writeln(ans);
	}return 0;
}

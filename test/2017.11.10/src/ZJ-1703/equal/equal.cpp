#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=100005;
int son[N],dep[N];
int head[N],nxt[N<<1],tail[N<<1];
int f[N][18];
int x,y,t,n,q;
void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa)
{
	for(int i=head[k];i;i=nxt[i]){
		if(tail[i]==fa)continue;
		int x=tail[i];
		dep[x]=dep[k]+1;f[x][0]=k;
		dfs(x,k);
		son[k]+=son[x]+1;
	}
}
int lca(int x,int y)
{
	if(x==y)return n;//AB同点 
	if(dep[x]<dep[y])swap(x,y);
	int a=x,b=y,k=dep[x]-dep[y];
	for(int i=17;i>=0;i--)if(k&(1<<i))a=f[a][i];
	if(a==b){//B为A祖先 
		a=x;k=dep[x]-dep[y];
		if(k%2==1)return 0;k/=2;
		for(int i=17;i>=0;i--)if(k&(1<<i))a=f[a][i];
		b=x;k=dep[x]-dep[y];k/=2;k--;
		for(int i=17;i>=0;i--)if(k&(1<<i))b=f[b][i];
		return son[a]-son[b];
	}
	for(int i=17;i>=0;i--)
		if(f[a][i]!=f[b][i])a=f[a][i],b=f[b][i];
	if(dep[x]==dep[y])return n-son[a]-son[b]-2;//AB是其LCA不同方向的同代
	int l=f[a][0];int t=dep[y]-dep[l];a=x;
	for(int i=17;i>=0;i--)if(t&(1<<i))a=f[a][i];
	return lca(a,l);//不同方向异代转换为B为A祖先在做 
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		x=read();y=read();
		addto(x,y);
		addto(y,x);
	}
	dep[1]=1;
	dfs(1,0);f[1][0]=1;
	for(int j=1;j<=17;j++)
		for(int i=1;i<=n;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	q=read();
	while(q--){
		x=read();y=read();
		writeln(lca(x,y));
	}return 0;
}

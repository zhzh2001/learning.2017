#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=55;
ll f[N],t,n;
int main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[1]=1;
	f[2]=2;
	t=2;
	for(int i=3;f[i-2]<=1e9;i++,t++)
		f[i]=f[i-1]+f[i-2];
	int T=read();
	while(T--){
		n=read();
		bool b=false;
		for(int i=1;i<=t;i++)
			if(n%f[i]==0){
				ll tmp=n/f[i];
				for(int j=i;j<=t;j++)
					if(f[j]==tmp)b=true;
				}
		if(b)puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<bits/stdc++.h>
#define int long long
using namespace std;
int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1,c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return k*kk;
}int t,d,f[200],x,ff;
signed main(){
	freopen("fib.in","r",stdin);freopen("fib.out","w",stdout);
	t=read();f[1]=1;d=1;while (f[d]<=1e9)d++,f[d]=f[d-1]+f[d-2];
	while (t--){
		x=read();ff=0;for (int i=0;i<=d;i++)
			for (int j=0;j<=d;j++)if (x==f[i]*f[j])ff=1;
		if (ff)puts("Yes");else puts("No");
	}
}

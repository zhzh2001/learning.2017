#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005

int T,n,k,i,u,cnt,ans;
int d[N],head[N],vis[N];
struct ff{int to,nxt;}e[2*N];

void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}
void dfs(int now,int fa){
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(v==fa)continue;
		dfs(v,now);
		if(!vis[now]&&!vis[v])
			ans--,vis[now]=vis[v]=1;
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("cbx.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d",&n,&k);
		cnt=0;memset(head,0,sizeof(head));
		memset(d,0,sizeof(d));
		for(i=1;i<n;i++){
			scanf("%d",&u);
			add(u,i+1),add(i+1,u);
			d[u]++,d[i+1]++;
		}memset(vis,0,sizeof(vis));
		for(i=1;d[i]==1;i++);
		ans=k;dfs(i,0);
		printf("%d\n",max(ans,(k+1)/2));
	}return 0;
}

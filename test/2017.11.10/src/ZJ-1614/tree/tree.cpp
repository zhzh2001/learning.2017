#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt) if ((y=E[i].to)!=fa)

using namespace std;

int rd() {
	char c=getchar(); int t=0;
	while (!isdigit(c)) c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t;
}

const int N=1e5+35;

int head[N],ans,cnt,vis[N],deg[N],n,K,rt;

struct Edge {
	int to,nxt;
}E[N<<1];

void Link(int u,int v) {
	deg[u]++,deg[v]++;
	E[++cnt]=(Edge){v,head[u]}; head[u]=cnt;
	E[++cnt]=(Edge){u,head[v]}; head[v]=cnt;
}

void dfs(int x,int fa) {
	Rep (i,x) dfs(y,x);
	if (!vis[x] && !vis[fa] && fa!=-1) vis[x]=vis[fa]=1,ans++;
}

int main() {

	freopen("tree.in","r",stdin);
//	freopen("tree.out","w",stdout);

	for (int cas=rd();cas--;) {
		ans=0,cnt=1;
		memset(vis,0,sizeof vis);
		memset(deg,0,sizeof deg);
		memset(head,0,sizeof head);
		
		n=rd(),K=rd(),rt=1;
		For (i,2,n) Link(rd(),i);
		For (i,1,n) if (deg[i]>1) rt=i;
		
		dfs(rt,-1);
		if (K<=2*ans)
			 printf("%d\n",K+1>>1); 
		else K-=2*ans,printf("%d\n",ans+K);
	}
	return 0;
}

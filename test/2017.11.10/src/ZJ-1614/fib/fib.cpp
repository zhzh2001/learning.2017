#include<cstdio>

#define For(i,x,y) for(int i=x;i<=y;i++)

using namespace std;

int cas,n,fib[2333];

int main() {

	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);

	scanf("%d",&cas);
	fib[1]=1,fib[2]=2,n=2;
	while (fib[n]+fib[n-1]<=1000000000) n++,fib[n]=fib[n-1]+fib[n-2];
	while (cas--) {
		int A,flg=0;
		scanf("%d",&A);
		For (i,1,n) For (j,1,n)
			if (1LL*A==1LL*fib[i]*fib[j]) flg=1;
		puts(flg?"Yes":"No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct node
{
	int to,next;
} e[200010];
int f[200010],g[200010],cnt,head[200010],T,n,k,x;
void add(int x,int y)
{
	e[++cnt].to=y;
	e[cnt].next=head[x];
	head[x]=cnt;
}
void clear()
{
	cnt=0;
	memset(head,0,sizeof head);
	memset(f,0,sizeof f);
	memset(g,0,sizeof g);
}
void dfs(int x,int fa)
{
	for(int i=head[x]; i; i=e[i].next)
		if(e[i].to!=fa)
			dfs(e[i].to,x),g[x]+=f[e[i].to];
	f[x]=g[x];
	for(int i=head[x]; i; i=e[i].next)
		if (e[i].to!=fa)
			f[x]=max(g[x]-f[e[i].to]+g[e[i].to]+1,f[x]);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d",&n,&k);
		clear();
		for (int i=2; i<=n; i++)
		{
			scanf("%d",&x);
			add(i,x);
			add(x,i);
		}
		dfs(1,0);
		if(f[1]*2>=k) printf("%d\n",(k+1)/2);
		else printf("%d\n",f[1]+(k-f[1]*2));
	}
}

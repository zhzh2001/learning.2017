#include<cstdio>
#include<cstring>
#include<algorithm>
struct node{int to,next;}e[200010];
int T,k,u;
int du[200010],vis[200010],head[200010],TT,E,cnt,n,tr[400010],in[200010],C,en[200010],v[200010],fa[200010];
void clear()
{
	cnt=0;
	memset(vis,0,sizeof(vis));
	memset(head,0,sizeof(head));
	memset(du,0,sizeof(du));
	memset(v,0,sizeof(v));
	memset(tr,0,sizeof(tr));
	memset(in,0,sizeof(in));
	memset(en,0,sizeof(en));
	TT=0,E=0;
	C=0;
}
void add(int x,int v)
{
	for (; x<=n; x+=x&-x)
		tr[x]+=v;
}
int query(int x)
{
	int ans=0;
	for (int i=1; i<=x; i+=i&-i)
		ans+=tr[i];
	return ans;
}
int read()
{
	int t=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') t=t*10+ch-48,ch=getchar();
	return t;
}
void Add(int x,int y){e[++cnt].to=y,e[cnt].next=head[x],head[x]=cnt;}
void dfs(int x)
{
	vis[x]=1;
	in[x]=++C;
	for (int i=head[x]; i; i=e[i].next)
	{
		int to=e[i].to;
		if (vis[to]) continue;
		dfs(to);
	}
	en[x]=C;
}
int Query(int l,int r)
{
	return query(r)-query(l-1);
}
void dec(int x)
{
	for (int i=head[x]; i; i=e[i].next)
	{
		int to=e[i].to;
		if (to==fa[x]) continue;
		dec(to);
	}
	if (!v[x]&&!v[fa[x]]&&du[x]==1&&Query(in[fa[x]],en[fa[x]])>=2)
	{
        du[x]--,du[fa[x]]--;
        v[x]=1;
		v[fa[x]]=1;
        if (fa[fa[x]]) du[fa[fa[x]]]--;
        add(in[fa[x]],-1),add(in[x],-1);
        TT+=2,E++;
    }
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	T=read();
	while (T--)
	{
		clear();
		n=read(),k=read();
		for (int i=1; i<n; i++)
		{
			u=read();
			fa[i+1]=u;
			du[i+1]++,du[u]++;
			Add(u,i+1),Add(i+1,u);
		}
		dfs(1);
		for (int i=1; i<=n; i++) add(in[i],1);
		dec(1);
		if (k%2==0&&TT>=k)
		{
			printf("%d\n",k/2);
			continue;
		}
		if (k%2&&TT>=k-1)
		{
			printf("%d\n",k/2+1);
			continue;
		}
		printf("%d\n",k-TT+E);
	}
}

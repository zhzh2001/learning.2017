#include<cstdio>
#include<algorithm>
using namespace std;
int n,Q,x,y,cnt,d[200010],sz[200010],head[200010],fa[200010][20];
struct node
{
	int to,next;
} e[200010];
int read()
{
	int t=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') t=t*10+ch-48,ch=getchar();
	return t;
}
void add(int x,int y)
{
	e[++cnt].to=y,e[cnt].next=head[x],head[x]=cnt;
}
void dfs(int x,int y)
{
	fa[x][0]=y;
	d[x]=d[y]+1;
	sz[x]=1;
	for(int i=1; i<20; i++)
		fa[x][i]=fa[fa[x][i-1]][i-1];
	for(int i=head[x]; i; i=e[i].next)
		if(e[i].to!=y)
			dfs(e[i].to,x),sz[x]+=sz[e[i].to];
}
int lca(int x,int y)
{
	if(d[x]<d[y]) swap(x,y);
	int dis=d[x]-d[y];
	for(int i=0; i<20; i++)
		if (dis&(1<<i))
			x=fa[x][i];
	if(x==y) return x;
	for(int i=19; i>=0; i--)
		if(fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}
int find(int x,int y)
{
	for(int i=0; i<20; i++)
		if(y&(1<<i))
			x=fa[x][i];
	return x;
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	for (int i=1; i<n; i++)
	{
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	Q=read();
	dfs(1,0);
	while (Q--)
	{
		int x=read(),y=read();
		if (x==y)
		{
			printf("%d\n",n);
			continue;
		}
		int LCA=lca(x,y);
		if((d[x]+d[y]-2*d[LCA]+1)&1)
		{
			int w=(d[x]+d[y]-2*d[LCA])/2;
			if(d[x]==d[y])
				printf("%d\n",n-sz[find(x,w-1)]-sz[find(y,w-1)]);
			else
			{
				if(d[x]<d[y])swap(x,y);
				printf("%d\n",sz[find(x,w)]-sz[find(x,w-1)]);
			}
		}
		else puts("0");
	}
}

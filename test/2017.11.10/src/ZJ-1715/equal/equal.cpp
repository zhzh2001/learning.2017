#include<bits/stdc++.h>
using namespace std;
struct edge{
	int to,next;
}e[200010];
int n,m,nedge;
int head[100010],size[100010],dep[100010],anc[100010][25];
 inline void add(int a,int b)
{
	e[++nedge].to=b;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 void dfs(int x,int fa1,int depth)
{
	size[x]=1;dep[x]=depth;anc[x][0]=fa1;
	for (int i=head[x];i;i=e[i].next)
	{
		int go=e[i].to;
		if (go==fa1) continue;
		dfs(go,x,depth+1);
		size[x]+=size[go];
	}
} 
 void init()
{
	for (int j=1;j<=22;++j)
		for (int i=1;i<=n;++i)
			anc[i][j]=anc[anc[i][j-1]][j-1];
}
 int lca(int x,int y)
{
	if (dep[x]<dep[y]) swap(x,y);
	for (int i=21;i>=0;--i)
		if (((dep[x]-dep[y])>>i)&1) x=anc[x][i];
	if (x==y) return x;
	for (int i=21;i>=0;--i)
		if (anc[x][i]!=anc[y][i]) 
		{
			x=anc[x][i];
			y=anc[y][i];
		}
	return anc[x][0];
}
 int bz(int x,int dis)
{
	for (int i=21;i>=0;--i)
		if ((dis>>i)&1) x=anc[x][i];
	return x; 
}
 int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<n;++i) 
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	dfs(1,0,1);
	init();
	scanf("%d",&m);
	for (int i=1;i<=m;++i)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		if (x==y)
		{
			printf("%d\n",n);
			continue;
		}
		if (dep[x]<dep[y]) swap(x,y);
		int lcaa=lca(x,y);
		int dist=dep[x]-dep[lcaa]+dep[y]-dep[lcaa];
		int mid=dist>>1;
		int ans=0;
		if (dist&1) 
		{
			puts("0");
			continue;
		}
		if (dep[x]!=dep[y])
		{
			
			int p=bz(x,mid-1);
			ans+=size[anc[p][0]]-size[p];
		}
		else 
		{
			ans+=dep[lcaa]-1;
			int px=bz(x,mid-1);
			int py=bz(y,mid-1);
			ans+=size[anc[px][0]]-size[px]-size[py];
		}
		cout<<ans<<endl;
	}
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll f[100010],n;
int T;
map<int,int> mp;
 int main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0;f[1]=1;
	for (int i=2;i<=50;++i) f[i]=f[i-1]+f[i-2]; 
	scanf("%d",&T);
	while (T--)
	{
		scanf("%lld",&n);
		bool f1=0;
		for (int i=0;i<=50;++i)
		{
			for (int j=0;j<=50;++j)
			{
				if (f[i]*f[j]>1e9) break;
				if (f[i]*f[j]==n) f1=1;
				if (f1) break;
			}
			if (f1) break;
		}
		if (f1) puts("Yes");
		else puts("No");
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a,b,c,d,ans,tot,num,head[N],deep[N],fa[N][20],l[N],r[N],size[N];
struct edge{ ll to,next; }e[N<<1];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs(ll u,ll last){
	size[u]=1; l[u]=++num;
	rep(i,1,18) fa[u][i]=fa[fa[u][i-1]][i-1];
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		fa[v][0]=u; deep[v]=deep[u]+1; dfs(v,u);
		size[u]+=size[v];
	}
	r[u]=num;
}
ll lca(ll u,ll v){
	if (deep[u]<deep[v]) swap(u,v);
	ll tmp=deep[u]-deep[v];
	rep(i,0,18) if (tmp&(1<<i)) u=fa[u][i];
	per(i,18,0) if (fa[u][i]!=fa[v][i]){
		u=fa[u][i]; v=fa[v][i];
	}
	if (u==v) return u;
	return fa[u][0];
}
ll get(ll u,ll x){
	rep(i,0,18) if (x&(1<<i)) u=fa[u][i];
	return u;
}
ll dis(ll u,ll v){
	return deep[u]+deep[v]-deep[lca(u,v)]*2;
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	dfs(1,0);
	m=read();
	while (m--){
		a=read(); b=read(); c=lca(a,b); d=dis(a,b);
		if (d&1) { puts("0"); continue; }
		if (a==b) { printf("%d\n",n); continue; }
		if (deep[a]<deep[b]) swap(a,b); c=get(a,d/2);
		if (l[c]<=l[b]&&l[b]<=r[c]) ans=n-size[get(a,d/2-1)]-size[get(b,d/2-1)];
		else ans=size[c]-size[get(a,d/2-1)];
		printf("%d\n",ans);
	}
}

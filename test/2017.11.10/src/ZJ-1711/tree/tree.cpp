#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,n,k,tot,f[N][2],head[N];
struct edge{ ll to,next; }e[N<<1];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dp(ll u,ll last){
	ll sum=0;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; dp(v,u);
		f[u][0]+=max(f[v][0],f[v][1]);
		sum+=max(f[v][0],f[v][1]);
	}
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		f[u][1]=max(f[u][1],sum-max(f[v][0],f[v][1])+f[v][0]+1);
	}
}
void work(){
	n=read(); k=read();
	memset(f,0,sizeof f);
	memset(head,0,sizeof head); tot=0;
	rep(i,2,n){
		ll fa=read();
		add(fa,i); add(i,fa);
	}
	dp(1,-1);
	ll ans=max(f[1][0],f[1][1]);
	if (ans*2>=k) printf("%d\n",(k+1)/2);
	else printf("%d\n",k-ans);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	T=read();
	while (T--) work();
}

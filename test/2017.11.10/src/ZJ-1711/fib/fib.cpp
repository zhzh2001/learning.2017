#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,f[N];
map<ll,bool> mp;
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[1]=1; rep(i,2,50) f[i]=f[i-1]+f[i-2];
	rep(i,1,50) mp[f[i]]=1;
	T=read();
	while (T--){
		ll a=read(),flag=0;
		for (ll i=1;f[i]*f[i]<=a&&i<=50;++i) if (a%f[i]==0){
			if (mp[a/f[i]]) flag=1;
			if (flag) break;
		}
		puts(flag?"Yes":"No");
	}
}

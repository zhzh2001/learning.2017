#include<bits/stdc++.h>
using namespace std;
#define ll long long

ll a[100],tot;
map<ll,bool> mp;

int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	a[0] = 0; a[1] = 1;
	for(tot = 2;;tot++){
		a[tot] = a[tot-1]+a[tot-2];
		if(a[tot] > 1e9) break;
	}
	mp[0] = true;
	for(int i = 1;i < tot;i++)
		for(int j = i;j < tot;j++)
			if(a[i]*a[j] <= 1e9)
				mp[a[i]*a[j]] = true;
	int T; scanf("%d",&T);
	while(T--){
		ll n; scanf("%lld",&n);
		if(mp[n]) puts("Yes");
		else puts("No");
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
const int N = 100005;
int head[N],to[N*2],nxt[N*2],cnt;
int n,m;
int f[18][N],siz[N],deep[N];

inline void add(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
}

void dfs(int x,int fa){
	siz[x] = 1;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			deep[to[i]] = deep[x]+1;
			f[0][to[i]] = x;
			dfs(to[i],x);
			siz[x] += siz[to[i]];
		}
}

inline void getst(){
	for(int j = 1;j <= 17;j++)
		for(int i = 1;i <= n;i++)
			f[j][i] = f[j-1][f[j-1][i]];
}

int getlca(int a,int b){
	if(deep[a] < deep[b]) swap(a,b);
	int dep = deep[a]-deep[b];
	for(int j = 17;j >= 0;j--)
		if(dep&(1<<j))
			a = f[j][a];
	if(a == b) return a;
	for(int j = 17;j >= 0;j--)
		if(f[j][a] != f[j][b]){
			a = f[j][a];
			b = f[j][b];
		}
	if(a == b) return a;
	a = f[0][a];
	return a;
}

int upto(int x,int len){
	if(len == 0) return x;
	for(int j = 17;j >= 0;j--)
		if(len&(1<<j))
			x = f[j][x];
	return x;
}

int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n = read();
	for(int i = 1;i < n;i++){
		int a = read(),b = read();
		add(a,b); add(b,a);
	}
	f[0][1] = 1;
	deep[1] = 1;
	dfs(1,1);
	getst();
	m = read();
	int ans;
	for(int i = 1;i <= m;i++){
		int a = read(),b = read();
		if(a == b){ printf("%d\n",n); continue;}
		if(deep[a] > deep[b]) swap(a,b);		//deep[a] < deep[b]
		int lca = getlca(a,b);
		int len = deep[a]+deep[b]-deep[lca]*2;
		if(len%2 != 0){ puts("0"); continue; }
		len /= 2;
		int sonb = upto(b,len-1);
		int dos = f[0][sonb];
		if(dos == lca){
			int sona = upto(a,len-1);
			ans = n-siz[sona]-siz[sonb];
		}
		else{
			ans = siz[dos]-siz[sonb];
		}
		printf("%d\n",ans);
	}
	return 0;
}
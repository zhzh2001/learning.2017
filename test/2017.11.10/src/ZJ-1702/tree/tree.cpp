#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
const int N = 100005;
int head[N],to[N*2],nxt[N*2],cnt;
bool lelf[N];
bool vis[N];
int n,k;
int ans;

inline void add(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
}

void dfs(int x,int f){
	lelf[x] = true; vis[x] = false;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			int v = to[i];
			dfs(v,x);
			if(!vis[v]){
				lelf[x] = false;
			}
			if(lelf[v] && (!vis[x])){
				vis[x] = true;
				vis[v] = true;
				ans += 2;
			}
		}
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int T = read();
	while(T--){
		n = read(); k = read();
		memset(head,0,sizeof head); cnt = 0; ans = 0;
		for(int i = 2;i <= n;i++){
			int x = read();
			add(i,x); add(x,i);
		}
		dfs(1,1);
		// printf("%d \n",ans);
		if(ans >= k)
			printf("%d\n",(k+1)/2);
		else{
			int p = ans/2;
			p += (k-ans);
			printf("%d\n", p);
		}
	}
	return 0;
}
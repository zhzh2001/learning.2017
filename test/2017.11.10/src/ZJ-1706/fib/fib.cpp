#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int N=100;
int f[1010];
signed main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0;f[1]=1;
	for(int i=2;i<=N;i++){
		f[i]=f[i-1]+f[i-2];
		if(f[i]>1e9+1e8){N=i;break;}
	}
	int T=read();
	while(T--){
		int x=read();
		if(x==0){puts("Yes");continue;}
		int p=lower_bound(f+1,f+N+1,x)-f;
		if(f[p]==x){puts("Yes");continue;}
		bool flag=0;
		for(int i=2;f[i]*f[i]<=x;i++)if(x%f[i]==0){
			int k=x/f[i];
			int p=lower_bound(f+1,f+N+1,k)-f;
			if(f[p]==k){flag=1;break;}
		}
		puts(flag?"Yes":"No");
	}
	return 0;
}
//%%%zyyAK
//%%%zyyAK
//%%%zyyAK

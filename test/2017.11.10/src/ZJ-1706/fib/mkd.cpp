#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int N=100;
int f[110];
signed main()
{
	freopen("fib.in","w",stdout);
	int T=rand()%100+1;T=100;
	writeln(T);
	for(int i=1;i<T;i++){
		int x=rand()*rand()%1000000000;
		writeln(x);
	}
	f[0]=0;f[1]=1;
	for(int i=2;i<=N;i++){
		f[i]=f[i-1]+f[i-2];
		if(f[i]>1e9+1e8){N=i;break;}
	}
	writeln(f[25]*f[11]);
}

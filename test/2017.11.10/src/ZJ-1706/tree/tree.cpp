#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int nedge=0,p[200010],nex[200010],head[200010],n,m;
int f[200010],vis[200010],sum=0;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int fa){
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa)dfs(p[k],x);
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&!vis[p[k]]){
		vis[p[k]]=vis[x]=1;sum++;break;
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int T=read();
	while(T--){
		nedge=0;memset(nex,0,sizeof nex);memset(head,0,sizeof head);
		memset(vis,0,sizeof vis);
		n=read();m=read();sum=0;
		for(int i=2;i<=n;i++){
			int x=read();
			addedge(x,i);addedge(i,x);
		}
		dfs(1,0);
		if(m>=sum*2)writeln(sum+m-sum*2);
		else writeln((m+1)/2);
	}
	return 0;
}
//%%%zyyAK
//%%%zyyAK
//%%%zyyAK

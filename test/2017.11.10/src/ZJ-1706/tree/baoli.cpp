#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int a[210],b[210],c[210];
int ans=1e9,p[210],nex[210],head[210],n,m,nedge=0;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfss(int x,int fa){
	c[x]=1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfss(p[k],x);c[x]+=c[p[k]];
	}
}
inline void dfs(int x,int sum){
	if(sum>=ans)return;
	if(x==n+1){
		nedge=0;memset(nex,0,sizeof nex);memset(head,0,sizeof head);
		memset(c,0,sizeof c);
		for(int i=2;i<=n;i++)if(b[i]){
			addedge(i,a[i]);addedge(a[i],i);
		}
		int s=0;
		for(int i=1;i<=n;i++)if(!c[i]){
			dfss(i,0);
			if(c[i]>1)s+=c[i];
		}
		if(s>=m)ans=sum;
		return;
	}
	b[x]=0;dfs(x+1,sum);
	b[x]=1;dfs(x+1,sum+1);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int T=read();
	while(T--){
		n=read();m=read();ans=1e9;
		for(int i=2;i<=n;i++)a[i]=read();
		memset(b,0,sizeof b);dfs(2,0);
		writeln(ans);
	}
	return 0;
}

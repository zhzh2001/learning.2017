#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,dist[2][200010];
int nedge=0,p[200010],nex[200010],head[200010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int fa,int op){
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dist[op][p[k]]=dist[op][x]+1;dfs(p[k],x,op);
	}
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	m=read();
	while(m--){
		int x=read(),y=read();
		dist[0][x]=0;dist[1][y]=0;
		dfs(x,0,0);dfs(y,0,1);
		int ans=0;
		for(int i=1;i<=n;i++)if(dist[0][i]==dist[1][i])ans++;
		writeln(ans);
	}
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int nedge=0,p[200010],nex[200010],head[200010];
int f[100010][19],s[100010],deep[100010],n;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int fa,int dep){
	deep[x]=dep;s[x]=1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		f[p[k]][0]=x;dfs(p[k],x,dep+1);s[x]+=s[p[k]];
	}
}
inline int lca(int x,int y){
	if(deep[x]<deep[y])swap(x,y);
	for(int i=18;i>=0;i--)if(deep[f[x][i]]>=deep[y])x=f[x][i];
	for(int i=18;i>=0;i--)if(f[x][i]!=f[y][i])x=f[x][i],y=f[y][i];
	if(x!=y)x=f[x][0];return x;
}
inline int up(int x,int dep){
	for(int i=18;i>=0;i--)if(deep[f[x][i]]>=dep)x=f[x][i];	
	return x;
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	dfs(1,0,1);
	for(int j=1;j<=18;j++)
		for(int i=1;i<=n;i++)f[i][j]=f[f[i][j-1]][j-1];
	int m=read();
	while(m--){
		int x=read(),y=read();
		if(x==y){writeln(n);continue;}
		int lc=lca(x,y);
		int lx=deep[x]-deep[lc],ly=deep[y]-deep[lc];
		if((lx+ly)&1){puts("0");continue;}
		if(lx==ly){
			int fx=up(x,deep[lc]+1),fy=up(y,deep[lc]+1);
			writeln(n-s[fx]-s[fy]);
		}else if(lx>ly){
			int fx=up(x,deep[x]-(lx+ly)/2);int fy=up(x,deep[fx]+1);
			writeln(s[fx]-s[fy]);
		}else{
			int fx=up(y,deep[y]-(lx+ly)/2);int fy=up(y,deep[fx]+1);
			writeln(s[fx]-s[fy]);
		}
	}
	return 0;
}
//%%%zyyAK
//%%%zyyAK
//%%%zyyAK

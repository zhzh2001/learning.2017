#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define RXD 1000000000
using namespace std;
int n,m,a[100005];
int num,t;
bool flag;
int main() {
	int i,j,k;
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	a[1]=0;
	a[2]=1;
	num=2;
	while(1) {
		num++;
		a[num]=a[num-2]+a[num-1];
		if(a[num]>RXD) break;
	}
	num--;
	scanf ("%d",&t);
	while (t--) {
		scanf ("%d",&n);
		int l=1,r=num;
		flag=false;
		while(l<=r) {
			if(1ll*a[l]*a[r]==n) {
				flag=true;
				break;
			}
			if(1ll*a[l]*a[r]>n) r--;
			else l++;
		}
		if(flag) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

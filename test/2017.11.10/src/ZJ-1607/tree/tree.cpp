//If you give me a chance,I'll take it.
//None struct Ver.
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 100010
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,x,y,t,ans,num;
int father[N],to[N],next[N],sum[N];
/*
struct node{
	int l,next;
}tree[M];
*/
inline void add_edge(int x,int y) {
	to[++num]=y;next[num]=father[x];father[x]=num;
//	(node){x,father[y]};
//	(node){y,father[x]};
}
inline void dfs(int x) { int i;
	sum[x]++;
	for(i=father[x]; i!=-1; i=next[i]) {
		dfs(to[i]);
		sum[x]+=sum[to[i]];
	}
	if(sum[x]>=2 && m>=2) {
		sum[x]=0;
		ans++;
		m-=2;
	}
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf ("%d",&t);
	while (t--) {
		scanf ("%d%d",&n,&m);
		memset(sum,0,sizeof(sum));memset(father,-1,sizeof(father));
		ans=0;num=-1;
		Rep(i,1,n-1){
			scanf ("%d",&x);
			y=i+1;
			add_edge(x,y);
		}
		dfs(1);
		ans+=m;
		printf("%d\n",ans);
	}
	return 0;
}

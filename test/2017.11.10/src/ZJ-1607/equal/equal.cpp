//If you give me a chance,I'll take it.
#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int N=100010;
const int M=2*N;
int i,j,k,l,m,n,t,x,y,now,cnt,len;
int dep[N],son[N],fa[N][20],head[N];
struct node{
	int l,next;
}tree[M];
inline void add_edge(int u,int v) {
	tree[++cnt]=(node){v,head[u]};head[u]=cnt;
	tree[++cnt]=(node){u,head[v]};head[v]=cnt;
}
void dfs(int x) {
	son[x]=1;
	for (int i=1;(1<<i)<=dep[x];i++) fa[x][i]=fa[fa[x][i-1]][i-1];
	for (int y,i=head[x];i;i=tree[i].next) 
		if ((y=tree[i].l)!=fa[x][0]) {
			fa[y][0]=x;
			dep[y]=dep[x]+1;
			dfs(y);
			son[x]+=son[y];
		}
}
inline int lca(int x,int y) {
	if (dep[x]>dep[y]) {
		swap(x,y);
	}
	for (int now=dep[y]-dep[x],i=0;now;i++) {
		if (now>>i&1) {
			now^=1<<i;
			y=fa[y][i];
		}
	}
	if (x==y) return x;
	Rep(i,0,19) {
		if (fa[x][i]!=fa[y][i]) {
			x=fa[x][i];
			y=fa[y][i];
		}
	}
	return fa[x][0];
}
inline int up(int x,int y) {
	for (int i=0;y;i++) {
		if (y>>i&1) {
			x=fa[x][i];
			y^=1<<i;
		}
	}
	return x;
}
int main() {
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	Rep(i,2,n) {
		int x,y;
		scanf ("%d%d",&x,&y);
		add_edge(x,y);
	}
	dfs(1);
	scanf ("%d",&t);
	while (t--) {
		int x,y;
		scanf ("%d%d",&x,&y);
		if (x==y) {
			cout<<n<<endl;
			continue;
		}
		int l=lca(x,y);
//		cout<<l<<endl;
		int len=dep[x]+dep[y]-dep[l];
//		cout<<dep[x]<<" "<<dep[y]<<" "<<dep[l]<<endl;
//		cout<<len<<endl;
		if (len%2) cout<<"0"<<endl;
		else {
			len/=2;
			if (dep[x]==dep[y]) {
				int a=up(x,len-1);
				int b=up(y,len-1);
				cout<<son[l]-son[a]-son[b]<<endl;
				continue;
			}
			if (dep[x]>dep[y]) {
				swap(x,y);
			}
			int a=up(y,len);
			int b=up(y,len-1);
			cout<<son[a]-son[b]<<endl;
		}
	}
	return 0;
}	

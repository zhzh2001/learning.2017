#include <cstdio>

int f[60];
int h,i,j,n,t;

inline bool check()
{
	for (i=0;i<=42;i++)
		for (j=0;j<=42;j++)
			if (1LL*f[i]*f[j]==n)
				return true;
	return false;
}

int main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=1,f[1]=2;
	for (i=2;i<=42;i++)
		f[i]=f[i-1]+f[i-2];
	scanf("%d",&t);
	for (h=1;h<=t;h++)
	{
		scanf("%d",&n);
		if (check())
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}

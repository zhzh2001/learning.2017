#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int edge[120000],next[120000],first[120000];
int sum_edge;
int f0[120000],f1[120000];
int h,i,k,m,n,t;

inline int fastscanf()
{
	int t=0;
	char c=getchar();
	while (! ((c>47) && (c<58)))
		c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t;
}

inline void clear()
{
	memset(first,0,sizeof(first));
	sum_edge=0;
	memset(f0,0,sizeof(f0));
	memset(f1,0,sizeof(f1));
	return;
}

inline void addedge(int x,int y)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],first[x]=sum_edge;
	return;
}

inline void dfs(int x)
{
	for (int i=first[x];i!=0;i=next[i])
		dfs(edge[i]);
	for (int i=first[x];i!=0;i=next[i])
		f0[x]=f0[x]+max(f0[edge[i]],f1[edge[i]]);
	for (int i=first[x];i!=0;i=next[i])
		f1[x]=max(f1[x],f0[x]-max(f0[edge[i]],f1[edge[i]])+f0[edge[i]]+1);
	return;
}

int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	t=fastscanf();
	for (h=1;h<=t;h++)
	{
		clear();
		n=fastscanf(),m=fastscanf();
		for (i=1;i<n;i++)
			k=fastscanf(),addedge(k,i+1);
		dfs(1);
		printf("%d\n",max(m-max(f0[1],f1[1]),(m+1)/2));
	}
	return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 1000
int f[N];
int T,x,pd,i,j,num;
int main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[1]=f[2]=1;
	for(i=3;f[i-1]<1e9;i++)f[i]=f[i-1]+f[i-2];
	num=i-1;
	scanf("%d",&T);
	while(T--){
		scanf("%d",&x);pd=0;
		for(i=1;i<=num;i++)
		for(j=1;j<=num;j++)
			if(1ll*f[i]*f[j]==x){pd=1;break;}
		if(pd)puts("Yes");else puts("No");
	}return 0;
}

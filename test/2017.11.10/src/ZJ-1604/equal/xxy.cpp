#include<cctype>
#include<cstdio>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt) if ((y=E[i].to)!=fa[x][0])

using namespace std;

int rd() {
	char c=getchar(); int t=0;
	while (!isdigit(c)) c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t;
}

const int N=1e5+35;

int cnt=1,cas,head[N],n,A,B,lca,len,a,b,FA,SO,dep[N],sz[N],fa[N][20];

struct Edge {
	int to,nxt;
}E[N<<1];

void Link(int u,int v) {
	E[++cnt]=(Edge){v,head[u]},head[u]=cnt;
	E[++cnt]=(Edge){u,head[v]},head[v]=cnt;
}

void dfs(int x) {
	sz[x]=1;
	for (int i=1;(1<<i)<=dep[x];i++)
		fa[x][i]=fa[fa[x][i-1]][i-1];
	
	Rep (i,x) {
		fa[y][0]=x;
		dep[y]=dep[x]+1;
		dfs(y),sz[x]+=sz[y];
	}
}

int LCA(int x,int y) {
	if (dep[x]>dep[y]) swap(x,y);
	for (int D=dep[y]-dep[x],i=0;D;i++)
		if (D>>i&1) D^=1<<i,y=fa[y][i];
	if (x==y) return x;
	Dep (i,19,0) if (fa[x][i]!=fa[y][i])
		x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}

int climb(int x,int y) {
	for (int i=0;y;i++) if (y>>i&1)
		x=fa[x][i],y^=1<<i;
	return x;
}

int main() {

	freopen("equal.in","r",stdin);
	freopen("xxy.out","w",stdout);

	n=rd();
	For (i,1,n-1) Link(rd(),rd());
	dfs(1);
	
	for (cas=rd();cas--;) {
		A=rd(),B=rd();
		if (A==B) { printf("%d\n",n); continue; }
		
		lca=LCA(A,B);
		len=dep[A]+dep[B]-dep[lca]*2;
		
		if (len%2) puts("0");
		else {
			len/=2;
			if (dep[A]==dep[B]) {
				a=climb(A,len-1);
				b=climb(B,len-1);
				printf("%d\n",n-sz[a]-sz[b]);
				continue;
			}
			if (dep[A]>dep[B]) swap(A,B);
			FA=climb(B,len);
			SO=climb(B,len-1);
			printf("%d\n",sz[FA]-sz[SO]);
		}
	}
	
	return 0;
}


#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005

int n,u,v,i,m,cnt;
int deep[N],fa[N][20],head[N],size[N];
struct ff{int to,nxt;}e[2*N];

void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}
int get(int x,int det){
	for(int i=0;det;det>>=1,i++)
		if(det&1)x=fa[x][i];
	return x;
}
int LCA(int u,int v){
	if(deep[u]>deep[v])swap(u,v);
	int det=deep[v]-deep[u];
	for(int i=0;det;det>>=1,i++)
		if(det&1)v=fa[v][i];
	if(u==v)return u;
	for(int i=19;i>=0;i--)
		if(fa[u][i]!=fa[v][i])
			u=fa[u][i],v=fa[v][i];
	return fa[u][0];
}
void dfs(int now){size[now]=1;
	for(int i=1;(1<<i)<=deep[now];i++)
		fa[now][i]=fa[fa[now][i-1]][i-1];
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(fa[now][0]==v)continue;
		fa[v][0]=now;deep[v]=deep[now]+1;
		dfs(v);
		size[now]+=size[v];
	}
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout); 
	scanf("%d",&n);
	for(i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		add(u,v),add(v,u);
	}dfs(1);
	scanf("%d",&m);
	for(i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		if(u==v){printf("%d\n",n);continue;}
		int lca=LCA(u,v);
		if(deep[u]==deep[v]){printf("%d\n",n-size[get(u,deep[u]-deep[lca]-1)]-size[get(v,deep[v]-deep[lca]-1)]);continue;}
		if((deep[u]+deep[v]-2*deep[lca]+1)%2==0){puts("0");continue;}
		if(deep[u]>deep[v])swap(u,v);
		int x=get(v,(deep[u]+deep[v]-2*deep[lca])/2);
		printf("%d\n",size[x]-size[get(v,(deep[u]+deep[v]-2*deep[lca])/2-1)]);
	}return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 1005

int n,u,v,i,j,m,rt,ans,cnt;
int dis[N][N],head[N];
struct ff{int to,nxt;}e[2*N];

void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}

void dfs(int now,int fa){
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(v==fa)continue;
		dis[rt][v]=dis[rt][now]+1;
		dfs(v,now);
	}
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("force.out","w",stdout); 
	scanf("%d",&n);
	for(i=1;i<n;i++){
		scanf("%d%d",&u,&v);
		add(u,v),add(v,u);
	}
	for(rt=1;rt<=n;rt++)dfs(rt,0);
	scanf("%d",&m);
	for(i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		ans=0;
		for(j=1;j<=n;j++)
			if(dis[j][u]==dis[j][v])ans++;
		printf("%d\n",ans);
	}return 0;
}

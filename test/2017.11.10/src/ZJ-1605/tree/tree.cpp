#include<map>
#include<set>
#include<ctime>
#include<cmath>
#include<string>
#include<vector>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5;
int head[N],nedge,vis[N],n,k,Res;
struct E{int next,to;} edge[N<<1];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int Dfs(int u,int F){
	for (int i=head[u];i;i=NX)
		if (O!=F) Dfs(O,u);
	if ((!(vis[u]+vis[F]))&&F) vis[u]=1,vis[F]=1,Res+=2;
}
inline int addline(int x,int y){
	edge[++nedge].to=y,edge[nedge].next=head[x],head[x]=nedge;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	for (int T=Read();T--;){//puts("sd");
		n=Read(),k=Read();
		memset(head,0,sizeof(head)),nedge=0;
		memset(vis,0,sizeof(vis));
		for (int i=2;i<=n;i++){int x=Read();
			addline(i,x),addline(x,i);
		}
		Res=0; Dfs(1,0); //printf("Res==%d\n",Res);
		if (k<=Res) printf("%d\n",(k+1)/2);
		else printf("%d\n",Res/2+(k-Res));
	}
}

#include<map>
#include<set>
#include<ctime>
#include<cmath>
#include<string>
#include<vector>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5,M=21;
struct E{int next,to;} edge[N<<1];
int nedge,head[N],dep[N],siz[N],f[N][M],n,x,y,Res,m;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int addline(int x,int y){
	edge[++nedge].to=y,edge[nedge].next=head[x],head[x]=nedge;
}
inline int build(int u,int F){siz[u]=1;
	for (int i=head[u];i;i=NX)
		if (O!=F){f[O][0]=u,dep[O]=dep[u]+1;
			build(O,u),siz[u]+=siz[O];
		}
}
inline int Ready(){
	for (int i=1;i<M;i++)
		for (int j=1;j<=n;j++)
			f[j][i]=f[f[j][i-1]][i-1];
}
inline int Lca(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	for (int i=M-1;~i;i--)
		if (dep[f[x][i]]>=dep[y]) x=f[x][i];
	for (int i=M-1;~i;i--)
		if (f[x][i]!=f[y][i]) x=f[x][i],y=f[y][i];
	return x==y?x:f[x][0];
}
inline int Up(int x,int h){
	int Dep=dep[x]-h;
	for (int i=M-1;~i;i--)
		if (dep[f[x][i]]>=Dep) x=f[x][i];
	return x;
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=Read(); dep[1]=1;
	for (int i=1;i<n;i++){
		x=Read(),y=Read();
		addline(x,y),addline(y,x);
	}
	for (m=Read(),build(1,0),Ready();m--;){
		x=Read(),y=Read();
		if (x==y){printf("%d\n",n); continue;}
		int p=Lca(x,y);
		int Dis=dep[x]+dep[y]-(dep[p]<<1);
		if (Dis&1){puts("0"); continue;}
		if (dep[x]<dep[y]) swap(x,y);
		Res=n; Res-=siz[Up(x,(Dis>>1)-1)];
		if (dep[x]==dep[y]) Res-=siz[Up(y,(Dis>>1)-1)];
		else Res-=(siz[p]-siz[Up(x,Dis>>1)]); printf("%d\n",Res);
	}
}

#include<map>
#include<set>
#include<ctime>
#include<cmath>
#include<string>
#include<vector>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1005,INF=1e9+7;
int f[N],x,tot; map<int,int> vis;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[1]=0,f[2]=1; vis[0]=vis[1]=1;
	for (tot=3;f[tot-1]<INF;tot++)
		f[tot]=f[tot-1]+f[tot-2],vis[f[tot]]=1;
	for (int T=Read();T--;){
		x=Read(); int flag=0;
		for (int i=2;i<=tot&&x>=f[i];i++)
			if (x>=f[i]&&x%f[i]==0&&vis[x/f[i]]) flag=1;
		puts(flag?"YES":"NO");	
	}
}

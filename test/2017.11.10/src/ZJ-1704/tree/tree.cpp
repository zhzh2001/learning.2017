#include<cstdio>
#include<cstring>
using namespace std;
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
const int maxn=100005;
struct edge{
	int link,next;
}e[maxn<<1];
int n,head[maxn],tot,k;
inline void insert(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void ins(int u,int v){
	insert(u,v); insert(v,u);
}
inline void init(){
	n=read(); k=read();
	for (int i=2;i<=n;i++){
		int x=read();
		ins(i,x);
	}
}
int cnt;
bool flag[maxn];
void dfs(int u,int fa){
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
		}
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa&&(!flag[v])){
			flag[u]=1; flag[v]=1;
			cnt++;
			break;
		}
	}
}
inline void clean(){
	cnt=tot=0;
	memset(head,0,sizeof(head));
	memset(flag,0,sizeof(flag));	
}
inline void solve(){
	dfs(1,0);
	if (cnt*2>=k){
		printf("%d\n",(k+1)/2);
	}else{
		printf("%d\n",(k-cnt));
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int T=read();
	while (T--){
		clean();
		init();
		solve();
	}
	return 0;
}

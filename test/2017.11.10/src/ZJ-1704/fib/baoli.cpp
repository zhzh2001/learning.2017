#pragma GCC optimize("O2")
#include<cstdio>
using namespace std;
typedef long long ll;
int n;
inline void init(){
	scanf("%d",&n);
}
const int inf=1e9,maxn=50;
ll f[maxn];
inline void prepare(){
	f[1]=1;
	for (int i=2;i<=45;i++){
		f[i]=f[i-1]+f[i-2];
	}
}
int tot;
ll a[3000];
inline void solve(){
	for (int i=0;i<=50;i++){
		for (int j=0;j<=50;j++){
			a[++tot]=1ll*f[i]*f[j];
		}
	}
	for (int i=1;i<=n;i++){
		int x;scanf("%d",&x); bool flag=1;
		for (int j=1;j<=tot;j++){
			if (a[j]==x){
				flag=0;
				puts("Yes");
				break;
			}
		}
		if (flag){
			puts("No");
		}
	}
}
int main(){
	freopen("fib.in","r",stdin);
	freopen("baoli.out","w",stdout);
	prepare();
	init();
	solve();
	return 0;
}

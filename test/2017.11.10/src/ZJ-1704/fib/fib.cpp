#pragma GCC optimize("O2")
#include<cstdio>
using namespace std;
const int inf=1e9,maxn=105;
int f[maxn],tot;
inline void prepare(){
	f[0]=0; f[1]=1;
	for (int i=2;i<=45;i++){
		f[i]=f[i-1]+f[i-2];
	}
}
int a;
inline void init(){
	scanf("%d",&a);
}
inline void solve(){
	if (a==0){
		puts("Yes");
		return;
	}
	for (int i=1;i<=45;i++){
		if (a%f[i]==0){
			int temp=a/f[i];
			for (int j=1;j<=45;j++){
				if (f[j]==temp) {
					puts("Yes");
					return;
				}
			} 
		}
	}
	puts("No");
}
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	int T;
	prepare();
	scanf("%d",&T);
	while (T--){
		init();
		solve();
	}
	return 0;
}

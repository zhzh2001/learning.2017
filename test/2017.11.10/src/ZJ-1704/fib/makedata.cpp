#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
typedef long long ll;
const ll inf=1e9;
inline ll get(ll l,ll r){
	return (rand()<<10)%(r-l+1)+l;
}
int main(){
	freopen("fib.in","w",stdout);
	srand(time(0));
	int n=100; printf("%d\n",n);
	for (int i=1;i<=n;i++){
		printf("%lld\n",get(0,inf));
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm> 
using namespace std;
const int maxn=305;
int n,dis[maxn][maxn];
inline void init(){
	memset(dis,127/3,sizeof(dis));
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		dis[i][i]=0;
	}
	for (int i=1;i<n;i++){
		int u,v;
		scanf("%d%d",&u,&v);
		dis[u][v]=dis[v][u]=1;
	}
}
inline void floyed(){
	for (int k=1;k<=n;k++){
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				dis[i][j]=min(dis[i][k]+dis[k][j],dis[i][j]);
			}
		}
	}
}
inline void solve(){
	floyed(); int m;
	scanf("%d",&m);
	for (int i=1;i<=m;i++){
		int u,v,ans=0;
		scanf("%d%d",&u,&v);
		for (int i=1;i<=n;i++){
			if (dis[u][i]==dis[v][i]){
				ans++; 
			}
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("baoli.out","w",stdout);	
	init();
	solve();
	return 0;
}

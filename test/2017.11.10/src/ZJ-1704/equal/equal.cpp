#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int maxn=1e5+5;
struct edge{
	int link,next;
}e[maxn<<1];
int n,tot,head[maxn];
inline void insert(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void ins(int u,int v){
	insert(u,v); insert(v,u);
}
inline void init(){
	n=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read();
		ins(u,v);
	}
}
int fa[maxn][22],size[maxn],dep[maxn];
void dfs(int u,int pre){
	fa[u][0]=pre; size[u]=1; 
	for (int i=1;i<20;i++){
		fa[u][i]=fa[fa[u][i-1]][i-1];
	}
	dep[u]=dep[pre]+1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=pre){
			dfs(v,u);
			size[u]+=size[v];
		}
	}
}
inline int lca(int x,int y){
	if (dep[x]<dep[y]){
		swap(x,y);
	}
	int delta=dep[x]-dep[y];
	for (int i=0;i<20;i++){
		if (delta&(1<<i)){
			x=fa[x][i];
		}
	}
	for (int i=19;i>=0;i--){
		if (fa[x][i]!=fa[y][i]){
			x=fa[x][i]; y=fa[y][i];
		}
	}
	if (x!=y) return fa[x][0]; else return x;
}
inline int askfa(int x,int d){
	for (int i=0;i<20;i++){
		if (d&(1<<i)){
			x=fa[x][i];
		}
	}
	return x;
}
inline int abs(int x){return (x>0)?x:-x;}
inline void solve(){
	dfs(1,0);
	int T=read();
	while (T--){
		int a=read(),b=read();
		if (a==b){
			writeln(n);
			continue;
		}
		if (abs(dep[a]-dep[b])%2!=0){
			puts("0");
		}else{
			int ans=n;
			int LCA=lca(a,b); int mid;
			int dis=dep[a]+dep[b]-2*dep[LCA];
			if (dep[a]>dep[b]){
				mid=askfa(a,dis/2);
			}else{
				mid=askfa(b,dis/2);
			}
			if (lca(a,mid)==mid){
				ans-=size[askfa(a,dis/2-1)];
			}else{
				ans-=n-size[mid];
			}
			if (lca(b,mid)==mid){
				ans-=size[askfa(b,dis/2-1)];
			}else{
				ans-=n-size[mid]; 
			}
			writeln(ans); 
		}
	}
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
int read()
{char c;int t=0;
 c=getchar();
 while(!(c>='0' && c<='9')) c=getchar();
 while(c>='0' && c<='9')
  {
  	t=t*10+c-48;
  	c=getchar();
  }
  return t;
}
int n,m,pre[N][20];
int sum[N];
int front[N],line,to[N],next[N],x,y,dep[N],ans=0;
void insert(int x,int y)
{
line++;to[line]=y;next[line]=front[x];front[x]=line;
}
void dfs(int x)
{int i;
   sum[x]=1;
   for(i=front[x];i!=-1;i=next[i])
	 {
	   pre[to[i]][0]=x;dep[to[i]]=dep[x]+1;dfs(to[i]);sum[x]+=sum[to[i]];
	 }
}
int find(int x,int y)
{int i;
	for(i=19;i>=0;i--)
	  if(dep[pre[y][i]]>=dep[x]) y=pre[y][i];
	if(x==y) return x;
	for(i=19;i>=0;i--)
	  if(pre[x][i]!=pre[y][i]) x=pre[x][i],y=pre[y][i];
	return pre[x][0];
}
int findf(int x,int y)
{int i;
   y--;
   for(i=19;i>=0;i--)
	 if(y>=(1<<i)) x=pre[x][i],y-=1<<i;
   return x;
}
int main()
{int i,j,k;
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	line=-1;memset(front,-1,sizeof(front));
	for(i=1;i<n;i++)
	  {
	   x=read();y=read();
	   if(x>y) swap(x,y);
	   insert(x,y);
	  }
	pre[1][0]=1;dep[1]=1;
	dfs(1);
	for(i=1;i<=n;i++)
	  for(j=1;j<=19;j++) pre[i][j]=pre[pre[i][j-1]][j-1];
	m=read();
	for(;m;m--)
	{
	  x=read();y=read();
	  if(x==y)
	   {
		printf("%d\n",n);continue;
		}
	  if(dep[x]>dep[y]) swap(x,y);
	  int fx=find(x,y);int dis=(dep[x]+dep[y]-2*dep[fx]);
	  if(dis%2==1)
		{
		 printf("0\n");continue;
		}
	  int fy=findf(y,dis/2);
	  int xx=pre[fy][0];
	  if(fx==xx)
		{
		  int fxy=findf(x,dis/2);
		  printf("%d\n",n-sum[fxy]-sum[fy]);
		  continue;
		}
	  printf("%d\n",sum[xx]-sum[fy]);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
int read()
{char c;int t=0;
 c=getchar();
 while(!(c>='0' && c<='9')) c=getchar();
 while(c>='0' && c<='9')
  {
  	t=t*10+c-48;
  	c=getchar();
  }
  return t;
}
int n,m,front[N],to[N],next[N],line,t,sum[N];
int x,y,ans;
void insert(int x,int y)
{
line++;to[line]=y;next[line]=front[x];front[x]=line;
}
void dfs(int x)
{int i;
   sum[x]++;
   for(i=front[x];i!=-1;i=next[i])
	 {
		dfs(to[i]);sum[x]+=sum[to[i]];
	 }
   if(sum[x]>=2 && m>=2)
   {
   sum[x]=0;ans++;m-=2;
   }
}
int main()
{int i,j,k;
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	t=read();
	for(;t;t--)
	{
	  n=read();m=read();
	  memset(sum,0,sizeof(sum));ans=0;
	  line=-1;memset(front,-1,sizeof(front));
	  for(i=1;i<n;i++)
		{
		x=read();y=i+1;insert(x,y);
		}
	  dfs(1);
	  ans+=m;
	  printf("%d\n",ans);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int f[105],q,x,tot;
const int inf=1e9;
set<int> s;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0;
	f[1]=1;
	tot=1;
	while (f[tot]<=inf)
	{
		f[tot+1]=f[tot]+f[tot-1];
		++tot;
	}
	s.insert(0);
	for (int i=1;i<=tot;++i)
		for (int j=i;j<=tot&&(long long)f[i]*f[j]<=inf;++j)
			s.insert(f[i]*f[j]);
	read(q);
	while (q--)
	{
		read(x);
		if (s.count(x))
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int to[N*2],pr[N*2],la[N],f[N][20],n,q,x,y,z,cnt,h[N],si[N];
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
void dfs(int x,int fa)
{
	for (int i=1;(1<<i)<=h[x];++i)
		f[x][i]=f[f[x][i-1]][i-1];
	si[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			f[to[i]][0]=x;
			h[to[i]]=h[x]+1;
			dfs(to[i],x);
			si[x]+=si[to[i]];
		}
}
inline int lca(int x,int y)
{
	int dt=h[y]-h[x];
	for (int i=0;i<20;++i)
		if ((1<<i)&dt)
			y=f[y][i];
	for (int i=19;i>=0;--i)
		if (f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return x==y?x:f[x][0];
}
inline int gf(int x,int y)
{
	for (int i=0;i<20;++i)
		if (y&(1<<i))
			x=f[x][i];
	return x;
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	read(n);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	dfs(1,0);
	read(q);
	while (q--)
	{
		read(x);
		read(y);
		if (x==y)
		{
			write(n);
			continue;
		}
		if (h[x]>h[y])
			swap(x,y);
		if (h[x]==h[y])
		{
			z=lca(x,y);
			int dt=h[x]-h[z];
			write(n-si[gf(x,dt-1)]-si[gf(y,dt-1)]);
		}
		else
		{
			z=lca(x,y);
			int dt=h[x]+h[y]-h[z]*2;
			if (dt&1)
				write(0);
			else
			{
				dt/=2;
				write(si[gf(y,dt)]-si[gf(y,dt-1)]);
			}
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=105;
int a[N][N],x,y,n,q,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.ans","w",stdout);
	read(n);
	memset(a,0x3f,sizeof(a));
	for (int i=1;i<=n;++i)
		a[i][i]=0;
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		a[x][y]=1;
		a[y][x]=1;
	}
	for (int k=1;k<=n;++k)
		for (int i=1;i<=n;++i)
			for (int j=1;j<=n;++j)
				a[i][j]=min(a[i][j],a[i][k]+a[k][j]);
	read(q);
	while (q--)
	{
		read(x);
		read(y);
		ans=0;
		for (int i=1;i<=n;++i)
			if (a[x][i]==a[y][i])
				++ans;
		write(ans);
	}
	return 0;
}
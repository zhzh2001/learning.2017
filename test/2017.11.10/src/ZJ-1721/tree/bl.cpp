#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
const int N=105;
int f[N],b[N],t,n,q,x,k,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int gf(int x)
{
	return x==f[x]?x:f[x]=gf(f[x]);
}
struct data
{
	int x,y;
}a[N];
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.ans","w",stdout);
	read(t);
	for (int i=1;i<=100;++i)
		a[i].x=i+1;
	while (t--)
	{
		read(n);
		read(k);
		ans=n-1;
		for (int i=1;i<n;++i)
			read(a[i].y);
		for (int i=0;i<1<<(n-1);++i)
		{
			int sum=0;
			for (int j=1;j<=n;++j)
				f[j]=j;
			for (int j=0;j<n-1;++j)
				if (i&(1<<j))
				{
					f[gf(a[j+1].x)]=gf(a[j+1].y);
					++sum;
				}
			if (sum>=ans)
				continue;
			memset(b,0,sizeof(b));
			for (int j=1;j<=n;++j)
				++b[gf(j)];
			int s=0;
			for (int j=1;j<=n;++j)
				if (b[j]!=1)
					s+=b[j];
			if (s>=k)
				ans=sum;
		}
		write(ans);
	}
	return 0;
}
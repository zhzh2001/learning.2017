#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int la[N],to[N*2],pr[N*2],v[N],ans,n,k,x,cnt,t;
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
void dfs(int x,int fa)
{
	v[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			dfs(to[i],x);
			if (v[to[i]]&&v[x])
			{
				v[x]=0;
				++ans;
			}
		}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(t);
	while (t--)
	{
		memset(la,0,sizeof(la));
		read(n);
		read(k);
		cnt=0;
		for (int i=2;i<=n;++i)
		{
			read(x);
			add(i,x);
			add(x,i);
		}
		ans=0;
		dfs(1,0);
		if (ans*2>k)
			write((k+1)/2);
		else
			write(k-ans);
	}
}
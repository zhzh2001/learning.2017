#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,k;
inline int Rand(){
	return rand()<<15|rand();
}
int main(){
	freopen("tree.in","w",stdout);
	srand(time(0));
	Rep(i,1,10) rand();
	int T=5;
	printf("%d\n",T);
	Rep(i,1,T){
		printf("%d %d\n",n=10,k=(rand()%10+1));
		Rep(i,2,n) printf("%d ",rand()%(i-1)+1);puts("");
	}
}

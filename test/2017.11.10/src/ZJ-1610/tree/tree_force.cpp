#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

int lines,front[N];
struct Edge{
	int next,to;
}E[N];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int fa[N],fab;
inline bool bol(int x,int fab){
	return ((fab>>x-1)&1);
}
int n,k;
inline int count(int x){
	int res=0;
	Rep(i,1,n) if (bol(i,x)) res++;
	return res;
}
int B[N];
int check(){
	int res=n;
//	printf("pos=");
//	Rep(i,1,n) if (bol(i,fab)) printf("%d ",i);puts("");
	Rep(i,0,(1<<n)-1){
		memset(B,0,sizeof(B));
		bool flag=true;
		int cnt=0;
		Rep(j,2,n) if (bol(j,i)) B[fa[j]]=B[j]=true;
		Rep(j,1,n) if (B[j]) cnt++;
		if (cnt>=k) res=min(res,count(i));
	}
	return res;
}
void Work(){
	lines=0;
	memset(front,0,sizeof(front));
	n=read();k=read();
	Rep(i,2,n){
		fa[i]=read();
		Addline(fa[i],i);
		Addline(i,fa[i]);
	}
	int Ans=check();
	printf("%d\n",Ans);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.ans","w",stdout);
	int T=read();
	Rep(i,1,T) Work();
}

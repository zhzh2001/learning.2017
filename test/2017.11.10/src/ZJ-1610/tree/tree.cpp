#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

int lines,front[N];
struct Edge{
	int next,to;
}E[N];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int fa[N],f[N][2];
void Dfs(int u){
	f[u][0]=f[u][1]=0;
	int Sum=0;
	for (int i=front[u],v;i!=0;i=E[i].next){
		Dfs(v=E[i].to);Sum+=f[v][1];
	}
	for (int i=front[u],v;i!=0;i=E[i].next){
		v=E[i].to;f[u][0]+=max(f[v][0],f[v][1]);
	}
	for (int i=front[u],v;i!=0;i=E[i].next){
		v=E[i].to;f[u][1]=max(f[u][1],f[v][0]+Sum-f[v][1]+1);
	}
}
int n,k;
void Work(){
	lines=0;
	memset(front,0,sizeof(front));
	n=read();k=read();
	Rep(i,2,n){
		fa[i]=read();Addline(fa[i],i);
	}
	Dfs(1);
	int cot=max(f[1][0],f[1][1]);
	if (2*cot>=k){
		printf("%d\n",(k+1)/2);return;
	}
	k-=cot*2;
	printf("%d\n",k+cot);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int T=read();
	Rep(i,1,T) Work();
}

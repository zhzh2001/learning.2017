#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,T;
inline int Rand(){
	return rand()<<15|rand();
}
int main(){
	freopen("equal.in","w",stdout);
	srand(time(0));
	Rep(i,1,10) rand();
	printf("%d\n",n=1000);
	Rep(i,2,n) printf("%d %d\n",Rand()%(i-1)+1,i);puts("");
	printf("%d\n",T=1000);
	Rep(i,1,T){
		int x=Rand()%n+1,y=Rand()%n+1;
		printf("%d %d\n",x,y);
	}
}

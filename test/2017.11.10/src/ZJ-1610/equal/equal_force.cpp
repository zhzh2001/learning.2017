#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;
const int LN=20;

int f[N][LN],Log[N],Bin[LN];
int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int deep[N],size[N];
void Dfs(int u,int fa,int d){
	f[u][0]=fa;
	deep[u]=d;
	size[u]=1;
	Rep(i,1,Log[d]) f[u][i]=f[f[u][i-1]][i-1];
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa){
			Dfs(v,u,d+1);
			size[u]+=size[v];
		}
	}
}
inline int Slip(int u,int d){
	Dep(i,Log[d],0) if (Bin[i]<=d){
		d-=Bin[i];u=f[u][i];
	}
	return u;
}
inline int Lca(int x,int y){
	if (deep[x]<deep[y]) swap(x,y);
	x=Slip(x,deep[x]-deep[y]);
//	printf("x=%d y=%d\n",x,y);
	if (x==y) return x;
	Dep(i,Log[x],0) if (f[x][i]!=f[y][i]){
		x=f[x][i];
		y=f[y][i];
	}
	return f[x][0];
}
inline int Dis(int x,int y){
	int lca=Lca(x,y);
	return deep[x]+deep[y]-2*deep[lca];
}
int n;
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.ans","w",stdout);
//	printf("%lf\n",1.0*(&ED-&ST)/1024/1024);
	n=read();
	Rep(i,1,n) Log[i]=Log[i>>1]+1;
	Bin[0]=1;
	Rep(i,1,19) Bin[i]=Bin[i-1]<<1;
	Rep(i,1,n-1){
		int u=read(),v=read();
		Addline(u,v),Addline(v,u);
	}
	Dfs(1,0,0);
	int T=read();
	Rep(i,1,T){
		int x=read(),y=read(),Ans=0;
		Rep(j,1,n) if (Dis(x,j)==Dis(y,j)) Ans++;
		printf("%d\n",Ans);
	}
}
/*
4
1 2
2 3
2 4
2
1 2
1 3
*/

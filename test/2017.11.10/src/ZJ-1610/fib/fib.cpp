#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
set<int>Set;
inline bool Work(){
	int n=read();
	Set.clear();
	int a=0,b=1;
	while (b<=n){
		Set.insert(b);
		if (n%b==0) if (Set.count(n/b)) return true;
		int b_=b;
		b+=a;a=b_;
	}
	return false;
}
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	int T=read();
	Rep(i,1,T) puts(Work()?"Yes":"No");
}

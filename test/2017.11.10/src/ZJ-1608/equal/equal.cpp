#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cctype>
using namespace std;
const int N=1e5+10,M=2e5+10;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
int v[M],pt[N],nxt[M],n,m,x,y,k,s[N],dpt[N],ast,lth,cnt,f[N][20],tth;
inline void add(int x,int y){
	v[++cnt]=y,nxt[cnt]=pt[x],pt[x]=cnt;
	v[++cnt]=x,nxt[cnt]=pt[y],pt[y]=cnt;
}
inline void gen(int u,int fa){
	s[u]=1;
	for(int i=pt[u];i;i=nxt[i])
		if(v[i]!=fa){
			dpt[v[i]]=dpt[u]+1;		
			f[v[i]][0]=u;
			gen(v[i],u);
			s[u]+=s[v[i]];	
		}
}
inline int Lca(int x,int y){
	for(int i=k;~i;i--)
		if(dpt[f[x][i]]>=dpt[y])x=f[x][i];
	if(x==y)return y;
	for(int i=k;~i;i--)
		if(f[x][i]!=f[y][i])
			x=f[x][i],y=f[y][i];
	return f[x][0];
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=In();
	for(int i=1;i<n;i++)
		x=In(),y=In(),add(x,y);
	dpt[1]=1,gen(1,0);
	k=log2(n);
	for(int j=1;j<=k;j++)
		for(int i=1;i<=n;i++)
			if(dpt[i]-(1<<j)>0)
				f[i][j]=f[f[i][j-1]][j-1];
	m=In();
	while(m--){
		x=In(),y=In();
		if(x==y){
			printf("%d\n",n);
			continue;
		}
		if(dpt[x]<dpt[y])swap(x,y);
		ast=Lca(x,y);
		if(ast==y)lth=dpt[x]-dpt[y];
		else lth=dpt[x]+dpt[y]-2*dpt[ast];
		if(lth%2)puts("0");
		else{
			lth/=2;tth=--lth;
			if(dpt[x]-dpt[ast]==tth+1){
				for(int i=k;~i;i--)
					if((1<<i)<=tth)
						y=f[y][i],tth-=(1<<i);
				for(int i=k;~i;i--)
					if((1<<i)<=lth)
						x=f[x][i],lth-=(1<<i);
				printf("%d\n",s[1]-s[x]-s[y]);
			}else{
				for(int i=k;~i;i--)
					if((1<<i)<=lth)
						x=f[x][i],lth-=(1<<i);
				printf("%d\n",s[f[x][0]]-s[x]);
			}
		}
	}
} 

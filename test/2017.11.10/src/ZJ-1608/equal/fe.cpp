#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}

const int N=1007;
struct line{
	int u,v,next;
}e[N<<1];
int cnt,n,q;
int head[N];
void ins(int u,int v){
	e[++cnt]=(line){
		u,v,head[u]	
	};	
	head[u]=cnt;
}
void insert(int u,int v){
	ins(u,v);
	ins(v,u);
}

int tmp,res1,res2,p1,p2;

void Dfs(int x,int last){
//	printf("%d %d\n",x,last);
	if (x==p1) res1=tmp;
	if (x==p2) res2=tmp;
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (y==last) continue;
		tmp++;
		Dfs(y,x);
		tmp--;
	}
}

int main(){
	// say hello

	freopen("equal.in","r",stdin);
	freopen("force_equal.out","w",stdout);

	n=read();
	For(i,1,n-1) insert(read(),read());
	q=read();
	For(p,1,q){
		p1=read(),p2=read();
		int ans=0;
		For(i,1,n){
			res1=-1,res2=-1;
			Dfs(i,0);
			if (res1==res2) ans++;//,printf("Q %d %d %d\n",i,res1,res2);
		}
		printf("%d\n",ans);
	}

	// say goodbye
}


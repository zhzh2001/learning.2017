#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
ll f[103];
int main(){
	init();
	f[0]=0,f[1]=1;
	int to=0;
	for(int i=2;;i++){
		f[i]=f[i-1]+f[i-2];
		//fprintf(stderr,"%lld ",f[i]);
		if (f[i]>INF){
			to=i;
			break;
		}
	}
	//fprintf(stderr,"\n%d ",to);
	int T=readint();
	while(T--){
		ll x=readint();
		bool flag=false;
		for(int i=0;i<=to;i++)
			for(int j=0;j<=to;j++)
				if (f[i]*f[j]==x){
					flag=true;
					break;
				}
		puts(flag?"Yes":"No");
	}
}
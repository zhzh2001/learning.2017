#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
const int maxm=maxn*2;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int par[maxn][21],dep[maxn],sz[maxn];
	void dfs(int u){
		sz[u]=1;
		for(int i=1;i<=20;i++)
			par[u][i]=par[par[u][i-1]][i-1];
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (sz[v])
				continue;
			dep[v]=dep[u]+1;
			par[v][0]=u;
			dfs(v);
			sz[u]+=sz[v];
		}
	}
	int lca(int u,int v){
		if (dep[u]<dep[v])
			swap(u,v);
		int d=dep[u]-dep[v];
		for(int i=0;i<=20;i++)
			if (d>>i&1)
				u=par[u][i];
		if (u==v)
			return u;
		for(int i=20;i>=0;i--)
			if (par[u][i]!=par[v][i])
				u=par[u][i],v=par[v][i];
		return par[u][0];
	}
	int query(int u,int v){
		if (u==v)
			return n;
		if (dep[u]==dep[v]){
			for(int i=20;i>=0;i--)
				if (par[u][i]!=par[v][i])
					u=par[u][i],v=par[v][i];
			return n-sz[u]-sz[v];
		}
		if (dep[u]<dep[v])
			swap(u,v);
		int lc=lca(u,v),dis=dep[u]+dep[v]-2*dep[lc];
		if (dis%2)
			return 0;
		dis=dis/2-1;
		for(int i=0;i<=20;i++)
			if (dis>>i&1)
				u=par[u][i];
		return sz[par[u][0]]-sz[u];
	}
}g;
int main(){
	init();
	int n=readint();
	g.n=n;
	for(int i=1;i<n;i++){
		int u=readint(),v=readint();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	g.dfs(1);
	int m=readint();
	while(m--){
		int u=readint(),v=readint();
		printf("%d\n",g.query(u,v));
	}
}
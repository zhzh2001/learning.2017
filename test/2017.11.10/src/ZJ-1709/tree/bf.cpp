#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tree.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[100002],fa[100002],sz[100002];
int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
int main(){
	init();
	int T=readint();
	while(T--){
		int n=readint(),m=readint();
		for(int i=2;i<=n;i++)
			a[i]=readint();
		int ans=INF;
		for(int i=0;i<(1<<n);i++){
			if (__builtin_popcount(i)!=m)
				continue;
			for(int j=0;j<(1<<(n-1));j++){
				int qwq=__builtin_popcount(j);
				if (qwq>=ans)
					continue;
				for(int k=1;k<=n;k++)
					fa[k]=k,sz[k]=0;
				for(int k=0;k<n-1;k++)
					if (j>>k&1)
						fa[getf(k+2)]=getf(a[k+2]);
				for(int k=0;k<n;k++)
					if (i>>k&1)
						sz[getf(k+1)]++;
				bool flag=true;
				for(int k=0;k<n;k++)
					if ((i>>k&1) && sz[getf(k+1)]<2){
						flag=false;
						break;
					}
				if (flag)
					ans=min(ans,qwq);
			}
		}
		printf("%d\n",ans);
	}
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=20000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
int dp[maxn][2],qwq[maxn],fa[maxn];
inline void tense(int &x,int y){
	if (x<y)
		x=y;
}
int main(){
	init();
	int T=readint();
	while(T--){
		int n=readint(),k=readint();
		for(int i=2;i<=n;i++)
			fa[i]=readint();
		memset(dp,0,sizeof(dp));
		memset(qwq,0,sizeof(qwq));
		for(int i=n;i;i--){
			dp[i][1]+=qwq[i];
			tense(dp[i][1],dp[i][0]);
			//printf("dp[%d][1]=%d dp[%d][0]=%d\n",i,dp[i][1],i,dp[i][0]);
			tense(qwq[fa[i]],dp[i][0]+1-dp[i][1]);
			dp[fa[i]][1]+=dp[i][1];
			dp[fa[i]][0]+=dp[i][1];
			//tense(dp[fa[i]][1],dp[i][0]+1);
			//tense(dp[fa[i]][0],dp[i][1]);
		}
		if (dp[1][1]*2>=k)
			printf("%d\n",(k+1)/2);
		else printf("%d\n",k-dp[1][1]);
	}
}
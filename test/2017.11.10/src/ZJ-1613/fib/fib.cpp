#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}

int f[107],n;

int main(){
	// say hello

	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);

	f[1]=0,f[2]=1;n=2;
	while (f[n]<1000000000) f[++n]=f[n-1]+f[n-2];
	int q=read();
	For(i,1,q){
		int x=read(),flag=0;
		For(i,2,n) if (!(x%f[i])){
			int k=x/f[i],y=lower_bound(f+1,f+1+n,k)-f;
			if (f[y]==k) {
				printf("Yes\n");
				flag=1;
				break;	
			}
		}
		if (!flag) printf("No\n");
	}
	

	// say goodbye
}


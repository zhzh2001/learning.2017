#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}

const int N=2e5+7;
int vis[N],q[N],top,rd[N],fa[N];
void Clear(){
	memset(rd,0,sizeof(rd));
	memset(vis,0,sizeof(vis));
	memset(q,0,sizeof(q));
	top=0;
}

int main(){
	// say hello

	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);

	int T=read();
	For(p,1,T){
		Clear();
		int n=read(),m=read();
		For(i,1,n-1){
			int x=i+1,y=read();
			rd[y]++,fa[x]=y;
		}
		For(i,1,n) if (!rd[i]) q[++top]=i;
		int l=0,Ans=0;
		while (l<top){
			int x=q[++l];
			rd[fa[x]]--;
			if (!rd[fa[x]]) q[++top]=fa[x];
			if (!vis[x]&&!vis[fa[x]]&&m>1&&x!=1) vis[x]=vis[fa[x]]=1,Ans++,m-=2;
		}
		printf("%d\n",Ans+m);
	}

	// say goodbye
}


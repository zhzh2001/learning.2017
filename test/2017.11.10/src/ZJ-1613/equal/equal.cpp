#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}

const int N=2e5+7;
struct Line{
	int u,v,next;
}e[N<<1];
int cnt,head[N<<1];
int Size[N],dep[N],fa[N][21];
void Ins(int u,int v){
	e[++cnt]=(Line){
		u,v,head[u]
	};
	head[u]=cnt;
}
void Insert(int u,int v){
	Ins(u,v),Ins(v,u);
}

void Dfs(int x,int last){
	Size[x]=1;
	for (int i=head[x];i;i=e[i].next){
		int y=e[i].v;
		if (y==last) continue;
		dep[y]=dep[x]+1;
		fa[y][0]=x;
		Dfs(y,x);
		Size[x]+=Size[y];
	}
}

int Lca(int x,int y){
	if (dep[x]>dep[y]) swap(x,y);
	Rep(i,0,20) if (dep[fa[y][i]]>=dep[x]) y=fa[y][i];
//	printf("X %d Y %d\n",x,y);
	if (x==y) return x;
	Rep(i,0,20) if (fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}

int Pre(int x,int len){
	int L=1048576,tmp=20;
	while (len){
		while (L>len) L>>=1,tmp--;
		len-=L;
		x=fa[x][tmp];
	}
	return x;
}

int n,q;
int main(){
	// say hello

	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);

	n=read();
	For(i,1,n-1) Insert(read(),read());
	dep[1]=1;
	Dfs(1,0);
	For(k,1,20) For(i,1,n) fa[i][k]=fa[fa[i][k-1]][k-1];
	q=read();
	For(i,1,q){
		int x=read(),y=read();
		if (x==y){
			printf("%d\n",n);
			continue;
		}
		if (dep[x]>dep[y]) swap(x,y);
		int k=Lca(x,y);
		int len=dep[x]-dep[k]+dep[y]-dep[k]+1;
//		printf("%d %d\n",k,len);
		if (len%2==0){
			printf("0\n");
			continue;
		}		
		int z=Pre(y,len/2);
		if (k==z){
			int kx=Pre(x,len/2-1),ky=Pre(y,len/2-1);
			printf("%d\n",n-Size[kx]-Size[ky]);
		}
		else {
			int ky=Pre(y,len/2-1);
			printf("%d\n",Size[z]-Size[ky]);
		}
	}

	// say goodbye
}


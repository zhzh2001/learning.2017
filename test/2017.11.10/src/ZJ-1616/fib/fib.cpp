#include<cstdio>
const int inf=1e9;
int n,i,j,f[100];
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	for (f[1]=1,i=2;f[i-1]<=inf;i++)
		f[i]=f[i-2]+f[i-1];
	n=i-1;int TEST;scanf("%d",&TEST);
	for (;TEST--;){
		int k;scanf("%d",&k);bool flag=0;
		if (!k){puts("Yes");continue;}
		for (i=1;!flag&&i<=n;i++)
			for (j=1;f[j]<=k/f[i];j++)
				if (f[i]*f[j]==k){flag=1;break;}
		puts(flag?"Yes":"No");
	}
}

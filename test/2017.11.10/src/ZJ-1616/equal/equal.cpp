#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1e5+5,M=2*N,LG=20;
int n,i,lg[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int et,he[N],size[N],deep[N],f[N][LG];
struct edge{int l,to;}e[M];
void dfs(int x,int d){
	size[x]=1;deep[x]=d;
	for (int i=he[x],j;i;i=e[i].l){
		int y=e[i].to;if (y==f[x][0]) continue;
		for (f[y][0]=x,j=1;j<=lg[d];j++)
			f[y][j]=f[f[y][j-1]][j-1];
		dfs(y,d+1);size[x]+=size[y];
	}
}
int up(int x,int k){
	for (int i=lg[k];i>=0;i--)
		if (k&(1<<i)) x=f[x][i];
	return x;
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	for (n=read(),i=1;i<n;i++){
		int x=read(),y=read();
		e[++et].l=he[x];he[x]=et;e[et].to=y;
		e[++et].l=he[y];he[y]=et;e[et].to=x;
	}
	for (lg[0]=-1,i=1;i<=n;i++) lg[i]=lg[i>>1]+1;
	dfs(1,1);
	for (int Query=read();Query--;){
		int x=read(),y=read(),px,py;
		if (x==y){write(n);putchar('\n');continue;}
		if (deep[x]<deep[y]) swap(x,y);
		px=up(x,deep[x]-deep[y]);py=y;
		for (i=lg[deep[px]-1];i>=0;i--)
			if (f[px][i]!=f[py][i])
				px=f[px][i],py=f[py][i];
		int p=px==py?px:f[px][0];
		int k=deep[x]+deep[y]-2*deep[p];
		if (k&1){puts("0");continue;}
		if (deep[x]==deep[y]){
			write(n-size[px]-size[py]);
			putchar('\n');continue;
		}
		k>>=1;px=up(x,k-1);p=f[px][0];
		write(size[p]-size[px]);putchar('\n');
	}
}

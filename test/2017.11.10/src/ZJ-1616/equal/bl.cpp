#include<cstdio>
const int N=5005,M=2*N;
int n,m,i,px,et,he[N],d[N][N];
struct edge{int l,to;}e[M];
void dfs(int x,int fa,int k){
	d[px][x]=k;
	for (int i=he[x];i;i=e[i].l)
		if (e[i].to!=fa) dfs(e[i].to,x,k+1);
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("bl.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<n;i++){
		int x,y;scanf("%d%d",&x,&y);
		e[++et].l=he[x];he[x]=et;e[et].to=y;
		e[++et].l=he[y];he[y]=et;e[et].to=x;
	}
	for (px=1;px<=n;px++) dfs(px,0,0);
	scanf("%d",&m);
	for (;m--;){
		int x,y,ans=0;scanf("%d%d",&x,&y);
		for (i=1;i<=n;i++) if (d[x][i]==d[y][i]) ans++;
		printf("%d\n",ans);
	}
}

#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1e5+5,M=2*N;
int n,m,i,f0[N],f1[N];
int et,he[N];struct edge{int l,to;}e[M];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void line(int x,int y){
	e[++et].l=he[x];he[x]=et;e[et].to=y;
	e[++et].l=he[y];he[y]=et;e[et].to=x;
}
void dfs(int x,int fa){
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa) continue;
		dfs(y,x);f0[x]+=f1[y];
		if (f0[y]==f1[y]) f1[x]=1;
	}
	f1[x]=f1[x]+f0[x];
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree_.out","w",stdout);
	for (int TEST=read();TEST--;){
		n=read();m=read();et=0;
		for (i=1;i<=n;i++) he[i]=f0[i]=f1[i]=0;
		for (i=2;i<=n;i++) line(read(),i);
		dfs(1,0);int k=f1[1];
		if (m<=2*k) printf("%d\n",m+1>>1);
		else printf("%d\n",m-k);
	}
}

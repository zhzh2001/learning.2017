#include<cstdio>
const int N=1e5+5,M=2*N;
int n,m,i,k;
int et,he[N];struct edge{int l,to;}e[M];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void line(int x,int y){
	e[++et].l=he[x];he[x]=et;e[et].to=y;
	e[++et].l=he[y];he[y]=et;e[et].to=x;
}
bool dfs(int x,int fa){
	bool flag=0;
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa) continue;
		if (!dfs(y,x)) if (!flag) k++,flag=1;
	}
	return flag;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	for (int TEST=read();TEST--;){
		n=read();m=read();et=0;
		for (i=1;i<=n;i++) he[i]=0;
		for (i=2;i<=n;i++) line(read(),i);
		k=0;dfs(1,0);
		if (m<=2*k) printf("%d\n",m+1>>1);
		else printf("%d\n",m-k);
	}
}

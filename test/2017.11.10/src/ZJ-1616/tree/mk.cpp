#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int N=1e5;
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int ran(){return (rand()<<15)+rand();}
int main(){
	freopen("tree.in","w",stdout);
	srand(time(0));rand();
	int TEST=10;
	printf("%d\n",TEST);
	for (;TEST--;){
		int n=ran()%(N-1)+2;
		n=N;
		int m=ran()%(n-1)+2;
		printf("%d %d\n",n,m);
		for (int i=2;i<=n;i++){
			write(ran()%(i-1)+1);
			putchar(' ');
		}
		puts("");
	}
}

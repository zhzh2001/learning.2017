#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,M=1e9;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int f[N],ans;
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	int i,j,T,n,x;f[1]=n=1;
	for(;f[n]<=M;n++)f[n+1]=f[n]+f[n-1];
	for(T=read();T--;){
		x=read();ans=0;
		for(i=1,j=n;i<=j;i++){
			while(1ll*f[i]*f[j]>x)j--;
			if(1ll*f[i]*f[j]==x)ans=1;
		}puts(ans?"Yes":"No");
	}
	return 0;
}

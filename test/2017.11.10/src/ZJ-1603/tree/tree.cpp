#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,M=1e9;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,fa[N],d[N],q[N],cv[N],l,r,ans;
void clear(){
	ans=0;l=1;r=0;
	memset(q,0,sizeof(q));
	memset(d,0,sizeof(d));
	memset(fa,0,sizeof(fa));
	memset(cv,0,sizeof(cv));
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	for(int i,x,y,T=read();T--;){
		clear();
		n=read();m=read();
		for(i=2;i<=n;i++)fa[i]=read(),d[fa[i]]++;
		for(i=1;i<=n;i++)if(!d[i])q[++r]=i;
		for(l=1;l<=r;l++){
			x=q[l];y=fa[x];
			if(x==1)break;
			if(cv[x]==0&&cv[y]==0&&m>1){
				ans+=1;m-=2;
				cv[x]=1;cv[y]=1;
			}
			d[y]--;if(!d[y])q[++r]=y;
		}printf("%d\n",ans+m);
	}
	return 0;
}

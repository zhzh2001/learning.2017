#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,M=1e9;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,lv[N],h[N],ne[N*2],to[N*2],L;
int sz[N],f[N][21];
inline void add(int x,int y){
	ne[++L]=h[x];h[x]=L;to[L]=y;
	ne[++L]=h[y];h[y]=L;to[L]=x;
}
int dfs(int x,int fa){
	lv[x]=lv[fa]+1;
	int k;f[x][0]=fa;
	for(k=0;f[x][k];k++)
	f[x][k+1]=f[f[x][k]][k];
	for(k=h[x];k;k=ne[k])
	if(to[k]!=fa)sz[x]+=dfs(to[k],x);
	return ++sz[x];
}
int pa(int x,int k){
	for(int i=0;i<=20;i++)
	if((k>>i)&1)x=f[x][i];
	return x;
}
int dis(int x,int y){
	if(lv[x]<lv[y])swap(x,y);
	int res=lv[x]-lv[y],k;
	x=pa(x,res);
	for(k=20;k>=0;k--)
	if(f[x][k]!=f[y][k])
	x=f[x][k],y=f[y][k],res+=1<<(k+1);
	if(x!=y)res+=2;
	return res;
}
void work(int x,int y){
	int ans=0,z,zz,k;
	for(z=1;z<=n;z++)
	if(dis(x,z)==dis(y,z))ans++;
	printf("%d\n",ans);
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal1.out","w",stdout);
	int i,m,x,y;
	n=read();
	for(i=1;i<n;i++)add(read(),read());
	dfs(1,0);m=read();
	while(m--){
		x=read();y=read();
		work(x,y);
	}return 0;
}

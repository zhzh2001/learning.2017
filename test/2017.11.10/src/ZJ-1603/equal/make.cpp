#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,M=1e9;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,i,x,y,j,k;
int main(){
	freopen("equal.in","w",stdout);
	srand(time(0));
	n=1000;m=1000;
	printf("%d\n",n);
	for(i=1;i<n;i++)
	printf("%d %d\n",rand()%i+1,i+1);
	printf("%d\n",m);
	for(i=1;i<=m;i++){
		x=rand()%n+1;
		y=rand()%n+1;
		printf("%d %d\n",x,y);
	}
	return 0;
}

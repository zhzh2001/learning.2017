#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int T,n,m,ans,jzq;
bool f[100100];
int fa[100100];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();m=read();
		ans=0;jzq=0;
		memset(f,false,sizeof(f));
		For(i,2,n)fa[i]=read();
		Rep(i,2,n)
		{
			if(f[i])continue;
			if(f[fa[i]])continue;
			f[i]=true;
			f[fa[i]]=true;
			ans++;
			jzq+=2;
			if(jzq>=m)break;
		}
		if(jzq>=m)
		{
			cout<<ans<<endl;
			continue;
		}
		Rep(i,2,n)
		{
			if(f[i]&&f[fa[i]])continue;
			f[i]=true;
			f[fa[i]]=true;
			ans++;
			jzq++;
			if(jzq>=m)break;
		}
		cout<<ans<<endl;
	}
	return 0;
}


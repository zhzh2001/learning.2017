#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(ll x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,head[N],cnt;
struct edge{int next,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
int fa[N][20],bin[20],dep[N],size[N];
inline void dfs(int x,int f){
	For(i,1,17){
		if(bin[i]>dep[x]) break;
		fa[x][i]=fa[fa[x][i-1]][i-1];
	}
	int y;size[x]=1;
	for(int i=head[x];i;i=e[i].next)
		if((y=e[i].to)!=f){
			fa[y][0]=x;
			dep[y]=dep[x]+1;
			dfs(y,x);
			size[x]+=size[y];
		}
}
inline int lca(int x,int y){
	if(dep[x]<dep[y]) swap(x,y);
	int del=dep[x]-dep[y];
	For(i,0,17) if(del&bin[i]) x=fa[x][i];
	Rep(i,17,0) if(fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	if(x==y) return x;
	return fa[x][0];
}
inline int dis(int x,int y){
	return dep[x]-dep[y];
}
inline void solve(int x,int y){
	if(x==y){writeln(n);return;}
	if(dep[x]<dep[y]) swap(x,y);
	int Lca=lca(x,y);
	int d=dis(x,Lca)+dis(y,Lca);
	if(d&1){writeln(0);return;}
	d>>=1;d--;
	if(dep[x]==dep[y]){
		For(i,0,17) if(d&bin[i]) x=fa[x][i];
		For(i,0,17) if(d&bin[i]) y=fa[y][i];
		writeln(size[1]-size[x]-size[y]);
	}else{
		For(i,0,17) if(d&bin[i]) x=fa[x][i];
		writeln(size[fa[x][0]]-size[x]);
	}
}
int main(){
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=read();
	bin[0]=1;For(i,1,19) bin[i]=bin[i-1]<<1;
	Forn(i,1,n){
		int u=read(),v=read();
		insert(u,v);
	}
	dfs(1,0);
	int m=read();
	while(m--){
		int x=read(),y=read();
		solve(x,y);
	}
	return 0;
}
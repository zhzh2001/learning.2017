#include<bits/stdc++.h>
#define ll long long
#define N 50
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(ll x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int f[N],top;
inline int find(int x){
	For(i,1,top) if(f[i]==x) return 1;
	return 0;
}
inline void solve(int x){
	if(x==0){puts("Yes");return;}
	For(i,1,top){
		if(1ll*f[i]*f[i]>1ll*x) break;
		if(x%f[i]) continue;
		if(find(x/f[i])==1){puts("Yes");return;}
	}
	puts("No");
}
int main(){
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0;f[1]=1;
	for(int i=2;;i++){
		f[i]=f[i-1]+f[i-2];
		top=i;
		if(f[i]>oo) break;
	}
	int T=read();
	while(T--) solve(read());
	return 0;
}
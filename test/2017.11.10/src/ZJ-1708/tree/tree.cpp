#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(ll x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,k,cnt,head[N],fa[N];
struct edge{int next,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
struct jdb{
	int x,dep;
	bool operator <(const jdb &a)const{
		return dep>a.dep;
	}
}a[N];
inline void dfs(int x){
	for(int i=head[x],y;i;i=e[i].next)
		if((y=e[i].to)!=fa[x]){
			fa[y]=x;
			a[y]=(jdb){y,a[x].dep+1};
			dfs(y);
		}
}
bool vis[N];
inline void solve(){
	n=read();k=read();cnt=0;
	memset(head,0,sizeof head);
	memset(vis,0,sizeof vis);
	For(i,2,n){
		int u=read();
		insert(i,u);
	}
	a[1]=(jdb){1,0};
	dfs(1);
	int p=0,ans=0;
	sort(a+1,a+1+n);
	For(i,1,n){
		if(p+1>=k) break;
		int x=a[i].x;
		if((!vis[x])&&(!vis[fa[x]])&&(fa[x]!=0)){
			vis[x]=1;vis[fa[x]]=1;
			p+=2;ans++;
		}
	}
	ans+=k-p;
	writeln(ans);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int T=read();
	while(T--) solve();
	return 0;
}
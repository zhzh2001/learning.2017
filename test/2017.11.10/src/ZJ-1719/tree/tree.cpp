#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>
#define For(i,j,k) for(int i=j;i<=k;++i)
#define Dow(i,j,k) for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
int n,k,x,y,poi[500001],nxt[500001],head[500001],cnt,tx[2000001],ty[2000001],tot,son[500001],FA[500001],now,ans;
bool vis[200001];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void dfs(int x,int fa)
{
	son[fa]=x;FA[x]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dfs(poi[i],x);	
	}
}
int T;
bool flag=1;
int main()
{
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	T=read();
	while(T--)
	{
		flag=1;
		n=read();k=read();
		For(i,1,n)	head[i]=vis[i]=0;cnt=0;tot=0;ans=0;now=0;
		For(i,2,n)	x=read(),add(i,x),add(x,i);
		dfs(1,1);
		For(i,2,n)
		{
			if(!vis[FA[i]]&&!vis[i])
			{
				now+=2;ans++;
				vis[FA[i]]=vis[i]=1;
				tx[++tot]=i;ty[tot]=FA[i];		
			}
			if(now>=k)	{printf("%d\n",ans);flag=0;break;}
		}
		if(!flag)	continue;
		For(i,1,tot)
		{
			if(!vis[son[tx[i]]]&&!vis[FA[ty[i]]])
			{
				now+=2;ans++;
				vis[son[tx[i]]]=1;vis[FA[ty[i]]]=1;
			}
			if(now>=k)	{printf("%d\n",ans);flag=0;break;}
		}
		if(!flag)	continue;
		For(i,1,n)
		{
			if(!vis[i])	now++,ans++,vis[i]=1;	
			if(now>=k)	{printf("%d\n",ans);flag=0;break;}
		}
	}
}

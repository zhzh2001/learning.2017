#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>
#define For(i,j,k) for(int i=j;i<=k;++i)
#define Dow(i,j,k) for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
ll T,n,f[201],tot=2;
inline bool check(int x)
{
	For(i,1,tot)
		For(j,1,tot)	
		{
			if(f[i]*f[j]==x)	return 1;
		}
	return 0;
}
inline void pre()
{
	f[1]=f[0]=1;
	while(1)
	{
		f[tot]=f[tot-1]+f[tot-2];
		if(f[tot]>1e9)	break;
		tot++;
	}
}
int main()
{
	freopen("fib.in","r",stdin);freopen("fib1.out","w",stdout);
	pre();
	T=read();
	while(T--)
	{
		n=read();
		if(check(n))	puts("Yes");	else puts("No");
	}
}

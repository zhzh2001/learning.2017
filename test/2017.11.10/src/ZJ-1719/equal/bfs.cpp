#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>
#define For(i,j,k) for(int i=j;i<=k;++i)
#define Dow(i,j,k) for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
int head[200010],nxt[200010],poi[200010],cnt,dis1[200010],dis2[200010],x,y,n,m;
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void dfs1(int x,int fa,int tep)
{
	dis1[x]=tep;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dfs1(poi[i],x,tep+1);
	}
}
inline void dfs2(int x,int fa,int tep)
{
	dis2[x]=tep;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dfs2(poi[i],x,tep+1);
	}
}
int main()
{
	freopen("equal.in","r",stdin);freopen("equal1.out","w",stdout);
	n=read();
	For(i,1,n-1)	x=read(),y=read(),add(x,y),add(y,x);
	m=read();
	For(i,1,m)
	{
		x=read();y=read();
		dfs1(x,x,1);dfs2(y,y,1);
		int ans=0;
		For(i,1,n)
			if(dis1[i]==dis2[i])	ans++;
		printf("%d\n",ans);
	}
}

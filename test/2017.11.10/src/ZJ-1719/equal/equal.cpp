#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>
#define For(i,j,k) for(int i=j;i<=k;++i)
#define Dow(i,j,k) for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
int sz[200001],F[200001][22],x,y,head[500001],nxt[500001],poi[500001],cnt,n,dep[200001];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void dfs(int x,int fa)
{
	sz[x]=1;F[x][0]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		dfs(poi[i],x);
		sz[x]+=sz[poi[i]];
	}
}
inline int up(int x,int len)
{
	For(i,0,19)	if(len&(1<<i))	x=F[x][i];
	return x;
}
inline int lca(int x,int y)
{
	if(dep[x]>dep[y])	swap(x,y);
	y=up(y,dep[y]-dep[x]);
	Dow(i,0,19)
		if(F[x][i]!=F[y][i])	x=F[x][i],y=F[y][i];
	return x==y?x:F[x][0];
}
int m;
int main()
{
	freopen("equal.in","r",stdin);freopen("equal.out","w",stdout);
	n=read();
	For(i,1,n-1)	x=read(),y=read(),add(x,y),add(y,x);
	dep[1]=1;dfs(1,1);
	For(j,1,19)	
		For(i,1,n)	F[i][j]=F[F[i][j-1]][j-1];
	m=read();
	For(i,1,m)
	{
		int x=read(),y=read();
		if(x==y)
		{
			printf("%d\n",n);
			continue;
		}
		int z=lca(x,y);		
		int dis=dep[x]+dep[y]-dep[z]*2;
		if(dis&1)	{puts("0");continue;}
		dis/=2;
		int ans=0;
		int tx=up(x,dis-1),ty=up(y,dis-1);
		if(dep[x]==dep[y])	ans=n-sz[up(x,dis-1)]-sz[up(y,dis-1)];
			else	
			{
				if(dep[x]>dep[y])
				{
					z=up(x,dis);
					int tsz=n-sz[z];
					ans=n-tsz-sz[up(x,dis-1)];
				}
				else
				{
					z=up(y,dis);
					int tsz=n-sz[z];
					ans=n-tsz-sz[up(y,dis-1)];
				}

			}
		printf("%d\n",ans);
	}
}


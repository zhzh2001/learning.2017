#include <iostream>
#include <set>
using namespace std;

typedef long long ll;
ll f[2333];

int main() {
  freopen("fib.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  f[0] = 0, f[1] = 1;
  set<ll> s;
  for (int i = 2; i <= 45; ++i)
    f[i] = f[i - 1] + f[i - 2];
  for (int i = 0; i <= 45; ++i)
    s.insert(f[i]);
  int T;
  cin >> T;
  while (T--) {
    ll x;
    cin >> x;
    if (x == 0) {
      puts("Yes");
      continue;
    }
    bool flag = false;
    for (int i = 1; i <= 45; ++i) {
      if (x % f[i] == 0)
        if (s.count(x / f[i])) {
          flag = true;
          break;
        }
    }
    if (flag)
      puts("Yes");
    else
      puts("No");
  }
  return 0;
}

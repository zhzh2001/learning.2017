#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <set>
#include <algorithm>
#include <iostream>
using namespace std;

namespace IO {
  static const int SZ = 1 << 20;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    char ch;
    T flag(1);
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag;  
  }
}

using namespace IO;
typedef long long ll;

const int N = 2333;
ll f[N];
set<int> st;

int main() {
  freopen("fib.in", "r", stdin);
  freopen("fib.out", "w", stdout);
  f[0] = 0, f[1] = 1;
  int pc = 2;
  for (int i = 2; ; ++i, ++pc) {
    f[i] = (ll)f[i - 1] + f[i - 2];
    if (f[i] > 1e9)
      break;
  }
  int T;
  read(T);
  while (T--) {
    ll x;
    read(x); 
    bool flag = true;
    for (int i = 0; i <= pc; ++i)
      for (int j = i; j <= pc; ++j)
        if (x == f[i] * f[j] && flag) {
          puts("Yes");
          flag = false;
        }
    if (flag)
      puts("No");
  }
  return 0;
}

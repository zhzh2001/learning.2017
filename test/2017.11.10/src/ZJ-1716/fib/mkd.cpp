#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Uniform(int l, int r) {
  return ((rand() << 15) + rand()) % (r - l + 1) + l; 
}

int main() {
  freopen("fib.in", "w", stdout);
  srand(GetTickCount());
  int T = 100;
  cout << T << endl;
  for (int i = 1; i <= T; ++i) {
    cout << Uniform(1, 1e9) << endl; 
  }
  return 0;
}

#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int fa[100005];
int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }
int uniform(int l, int r) { return ((rand() << 15) + rand()) % (r - l + 1) + l; }

int main() {
  freopen("tree.in", "w", stdout);
  srand(GetTickCount());
  ios::sync_with_stdio(false);
  int T = 10;
  cout << T << endl;
  while (T--) {
    int n = 15, k = uniform(1, 15);
    cout << n << ' ' << k << endl;
    for (int i = 1; i <= 15; ++i)
      fa[i] = i;
    for (int i = 2; i <= n; ++i) {
      int x;
      do {
        x = uniform(1, n);
      } while (find(i) == find(x));
      cout << x << ' ';
      int fx = find(i), fy = find(x);
      fa[fx] = fy;
    }
    cout << endl;
  }
  return 0;
}

@echo off
for /l %%s in (1, 1, 100000) do (
mkd
bf
tree
fc tree.out bf.out
if errorlevel == 1 pause
echo Done: %%s!
)
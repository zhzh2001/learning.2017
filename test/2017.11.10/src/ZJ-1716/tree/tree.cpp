#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

namespace IO {
  static const int SZ = 1 << 20;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    char ch;
    T flag(1);
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag;  
  }
}

using namespace IO;

const int N = 100005;

vector<int> G[N];
int f[N][2], pres;
bool tag[N];

void dfs(int x, int fa = -1) {
  f[x][0] = 0;
  tag[x] = false;
  int tot1 = 0, tot0 = 0;
  for (int i = 0; i < (int)G[x].size(); ++i) {
    int nxt = G[x][i];
    if (nxt == fa)
      continue;
    dfs(nxt, x);
  }
  for (int i = 0; i < (int)G[x].size(); ++i) {
    int nxt = G[x][i];
    if (nxt == fa)
      continue;
    tot1 += f[nxt][1];
    tot0 += max(f[nxt][0], f[nxt][1]);
  }
  f[x][0] = max(f[x][0], tot0);
  for (int i = 0; i < (int)G[x].size(); ++i) {
    int nxt = G[x][i];
    if (nxt == fa)
      continue;
    f[x][1] = max(f[x][1], tot1 - f[nxt][1] + f[nxt][0] + 1); 
  }
}

void Init() {
  memset(f, 0x00, sizeof f);
  memset(tag, 0x00, sizeof tag);
  for (int i = 0; i < N; ++i)
    G[i].clear();
}

int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  int T;
  read(T);
  while (T--) {
    int n, m;
    read(n), read(m);
    pres = 0;
    Init();
    for (int i = 2; i <= n; ++i) {
      int x;
      read(x);
      G[x].push_back(i);
      G[i].push_back(x);
    }
    dfs(1);
    int res = max(f[1][0], f[1][1]);
    if (res * 2 >= m) {
      printf("%d\n", m / 2);
    } else {
      printf("%d\n", min(m - 1, res + (m - res * 2)));
    }
  }
  return 0;
}

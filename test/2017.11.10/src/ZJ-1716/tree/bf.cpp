#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

const int N = 100005;
int n, k, cho[233];

struct Edge {
  int u, v; 
} e[N];

int main() {
  freopen("tree.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  ios::sync_with_stdio(false);
  int T;
  cin >> T;
  while (T--) {
    cin >> n >> k;
    int ec = 0;
    for (int i = 2; i <= n; ++i) {
      int x;
      cin >> x;
      e[ec++] = (Edge) { x, i };
    }
    int result = 100000;
    for (int i = 0; i < (1 << ec); ++i) {
      int ans = __builtin_popcount(i);
      memset(cho, 0x00, sizeof cho);
      for (int j = 0; j < ec; ++j)
        if (i & (1 << j))
          cho[e[j].u] = cho[e[j].v] = true;
      int nd = 0;
      for (int i = 1; i <= n; ++i)
        if (cho[i])
          ++nd;
      if (nd == k)
        result = min(result, ans);
    }
    if (result == 100000)
      result = k - 1;
    printf("%d\n", result);
  }
  return 0;
}

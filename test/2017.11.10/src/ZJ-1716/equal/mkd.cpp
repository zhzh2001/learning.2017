#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Uniform(int l, int r) {
  return ((rand() << 15) + rand()) % (r - l + 1) + l; 
}

int fa[100005];
int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }

inline void write(int x) {
  static int dig[20], cnt;
  cnt = 0;
  do {
    dig[++cnt] = x % 10;
  } while (x /= 10);
  while (cnt > 0) putchar(dig[cnt--] + '0');
}

inline void writeln(int x) {
  write(x);
  putchar('\n');
}

int main() {
  freopen("equal.in", "w", stdout);
  srand(GetTickCount());
  int n = 100;
  writeln(n);
  for (int i = 1; i <= n; ++i)
    fa[i] = i;
  for (int i = 1; i < n; ++i) {
    int u, v;
    do {
      u = Uniform(1, n);
      v = Uniform(1, n);
    } while (find(u) == find(v));
    write(u), putchar(' '), writeln(v);
    int fx = find(u), fy = find(v);
    fa[fx] = fy;
//    cerr << i << endl;
  }
  int m = 100;
  writeln(m);
  for (int i = 1; i <= m; ++i) { 
    write(Uniform(1, n));
    putchar(' ');
    writeln(Uniform(1, n));    
  }
  return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
using namespace std;

namespace IO {
  static const int SZ = 1 << 20;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    char ch;
    T flag(1);
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag;  
  }
}

using namespace IO;

int mat[105][105], n;

int main() {
  freopen("equal.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  read(n);
  memset(mat, 0x3f, sizeof mat);
  for (int i = 1; i < n; ++i) {
    int u, v;
    read(u), read(v);
    mat[u][v] = mat[v][u] = 1;
  }
  for (int i = 1; i <= n; ++i)
    mat[i][i] = 0;
  for (int k = 1; k <= n; ++k)
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= n; ++j)
        mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
  int m;
  read(m);
  for (int i = 1; i <= m; ++i) {
    int a, b;
    read(a), read(b);
    int ans = 0;
    for (int i = 1; i <= n; ++i) {
      if (mat[a][i] == mat[b][i]) {
        ++ans;
//        cerr << i << ' ';
      }
    }
//    cerr << endl;
    printf("%d\n", ans);
  }
//  for (int k = 1; k <= n; ++k)
//    cerr << mat[3][k] << ' ' << mat[4][k] << endl;
  return 0;
}

#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
using namespace std;

namespace IO {
  static const int SZ = 1 << 20;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    char ch;
    T flag(1);
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag;  
  }
}

using namespace IO;
const int N = 100005, M = 100005;

int n, head[N], ec, fa[N][20], dep[N], siz[N];
struct Edge { int to, nxt; } e[N << 1];
inline void addEdge(int u, int v) {
  e[++ec] = (Edge) { v, head[u] };
  head[u] = ec;
}

void dfs(int x, int f = -1) {
  for (int i = 1; i <= 19; ++i)
    fa[x][i] = fa[fa[x][i - 1]][i - 1];
  siz[x] = 1;
  for (int i = head[x]; i; i = e[i].nxt) {
    int nxt = e[i].to;
    if (nxt == f)
      continue;
    fa[nxt][0] = x;
    dep[nxt] = dep[x] + 1;
    dfs(nxt, x);
    siz[x] += siz[nxt];
  }
}

int queryLCA(int x, int y) {
  for (int i = 19; i >= 0; --i)
    if (dep[fa[x][i]] >= dep[y])
      x = fa[x][i];
  for (int i = 19; i >= 0; --i)
    if (dep[fa[y][i]] >= dep[x])
      y = fa[y][i];
  for (int i = 19; i >= 0; --i)
    if (fa[x][i] != fa[y][i]) {
      x = fa[x][i];
      y = fa[y][i];
    }
  return x == y ? x : fa[x][0];
}

int getFa(int x, int k) {
  for (int i = 19; i >= 0; --i)
    if (k & (1 << i))
      x = fa[x][i];
  return x;  
}

int main() {
  freopen("equal.in", "r", stdin);
  freopen("equal.out", "w", stdout);
  read(n);
  for (int i = 1; i < n; ++i) {
    int u, v;
    read(u), read(v);
    addEdge(u, v);
    addEdge(v, u);
  }
  int m;
  read(m);
  dep[1] = 1;
  dfs(1);
  while (m--) {
    int a, b;
    read(a), read(b);
    if (a == b) { // 1
      printf("%d\n", n); 
      continue;
    }
    if (dep[a] == dep[b]) { // 2
      int g = queryLCA(a, b);
      int na = getFa(a, dep[a] - dep[g] - 1);
      int nb = getFa(b, dep[b] - dep[g] - 1);
      printf("%d\n", n - siz[na] - siz[nb]);
      continue;
    }
    if (dep[a] < dep[b])
      swap(a, b);
    int na = getFa(a, dep[a] - dep[b]);
    if (na == b) { // 3
      if ((dep[a] - dep[b]) & 1) {
        puts("0");
        continue;  
      }
      int nb = getFa(a, (dep[a] - dep[b]) / 2);
      int nc = getFa(a, (dep[a] - dep[b]) / 2 - 1);
      printf("%d\n", siz[nb] - siz[nc]);
      continue;
    }
    int g = queryLCA(a, b), dist = dep[a] + dep[b] - dep[g] * 2;
    if (dist & 1) {
      puts("0");
      continue;
    }
    int nb = getFa(a, dist / 2);
    int nc = getFa(a, dist / 2 - 1);
    printf("%d\n", siz[nb] - siz[nc]);
  }
  return 0;
}

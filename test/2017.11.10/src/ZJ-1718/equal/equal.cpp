#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("equal.in");
ofstream fout("equal.out");
const int N = 100005;
int head[N], v[N * 2], nxt[N * 2], e, f[N][20], dep[N], sz[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
void dfs(int k)
{
	sz[k] = 1;
	for (int i = head[k]; i; i = nxt[i])
		if (!f[v[i]][0])
		{
			f[v[i]][0] = k;
			dep[v[i]] = dep[k] + 1;
			dfs(v[i]);
			sz[k] += sz[v[i]];
		}
}
int jmp(int x, int dep)
{
	for (int i = 0; dep; i++, dep /= 2)
		if (dep & 1)
			x = f[x][i];
	return x;
}
int lca(int x, int y)
{
	if (dep[x] < dep[y])
		swap(x, y);
	x = jmp(x, dep[x] - dep[y]);
	if (x == y)
		return x;
	for (int i = 19; i >= 0; i--)
		if (f[x][i] != f[y][i])
		{
			x = f[x][i];
			y = f[y][i];
		}
	return f[x][0];
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		fin >> u >> v;
		add_edge(u, v);
		add_edge(v, u);
	}
	f[1][0] = 1;
	dfs(1);
	for (int j = 1; j < 20; j++)
		for (int i = 1; i <= n; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
	int m;
	fin >> m;
	while (m--)
	{
		int u, v;
		fin >> u >> v;
		if (u == v)
			fout << n << '\n';
		else
		{
			int a = lca(u, v), d = dep[u] + dep[v] - 2 * dep[a];
			if (d & 1)
				fout << 0 << '\n';
			else if (dep[u] == dep[v])
				fout << n - sz[jmp(u, dep[u] - dep[a] - 1)] - sz[jmp(v, dep[v] - dep[a] - 1)] << '\n';
			else
			{
				if (dep[u] < dep[v])
					swap(u, v);
				fout << sz[jmp(u, d / 2)] - sz[jmp(u, d / 2 - 1)] << '\n';
			}
		}
	}
	return 0;
}
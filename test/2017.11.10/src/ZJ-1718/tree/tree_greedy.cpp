#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 100005;
int head[N], v[N], nxt[N], e, match[N];
bool vis[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
bool dfs(int k)
{
	vis[k] = true;
	for (int i = head[k]; i; i = nxt[i])
		if (!match[v[i]])
		{
			match[v[i]] = k;
			if (!vis[v[i]] || dfs(v[i]))
				return true;
		}
	return false;
}
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		int n, k;
		fin >> n >> k;
		e = 0;
		fill(head + 1, head + n + 1, 0);
		fill(match + 1, match + n + 1, 0);
		for (int i = 2; i <= n; i++)
		{
			int x;
			fin >> x;
			add_edge(i, x);
		}
		int ans = 0;
		for (int i = 1; i <= n; i++)
		{
			fill(vis + 1, vis + n + 1, false);
			ans += dfs(i);
			if (ans * 2 >= k)
				break;
		}
		if (ans * 2 >= k)
			fout << ans << endl;
		else
			fout << ans + (k - ans * 2) << endl;
	}
	return 0;
}
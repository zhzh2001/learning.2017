#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(Ll &x,Ll &y){x=RR();y=RR();}

const Ll N=1e6;
Ll f[N],T,A;
int main()
{
	freopen("fib.in","r",stdin);
	freopen("fib.out","w",stdout);
	f[0]=0;f[1]=1;
	for(Ll i=2;1;i++){
		f[i]=f[i-1]+f[i-2];
		if(f[i]>1e9)break;
	}
	cin>>T;
	while(T--){
		cin>>A;
		bool ans=0;
		for(Ll i=0;i<=45;i++)
			for(Ll j=0;j<=45;j++)
				if(f[i]*f[j]==A)ans=1;
		if(ans)cout<<"Yes";else cout<<"No";
		cout<<endl;
	}
		
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=1e5+5;
struct cs{int to,nxt;}a[N*2];
int head[N],ll,f[N][20],dp[N],sz[N];
int n,m,x,y,z;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs(int x,int y,int deep){
	f[x][0]=y;dp[x]=deep;sz[x]=1;
	for(int j=1;j<=17;j++)
		f[x][j]=f[f[x][j-1]][j-1];
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y)dfs(a[k].to,x,deep+1),sz[x]+=sz[a[k].to];
}
int lca(int x,int y){
	if(dp[x]<dp[y])swap(x,y);
	for(int j=17;j>=0;j--)
		if(dp[f[x][j]]>=dp[y])x=f[x][j];
	if(x==y)return x;
	for(int j=17;j>=0;j--)
		if(f[x][j]!=f[y][j])
			x=f[x][j],y=f[y][j];
	return f[x][0];
}
void work1(int x,int y){
	if(dp[y]>dp[x])swap(x,y);
	int v=dp[x]-dp[y];
	if(v&1){WW(0);return;}
	v=v/2-1;
	for(int j=17;j>=0;j--)
		if(((1<<j)&v))x=f[x][j];
	WW(sz[f[x][0]]-sz[x]);
}
void work2(int x,int y,int z){
	if(dp[x]==dp[y]){
		int v=dp[x]-dp[z]-1;
		for(int j=17;j>=0;j--)
			if(((1<<j)&v))x=f[x][j],y=f[y][j];
		WW(sz[z]-sz[x]-sz[y]);
	}else{
		if(dp[y]>dp[x])swap(x,y);
		int v=dp[x]-dp[z]+dp[y]-dp[z];
		if(v&1){WW(0);return;}
		v=v/2-1;
		for(int j=17;j>=0;j--)
			if(((1<<j)&v))x=f[x][j];
		WW(sz[f[x][0]]-sz[x]);
	}
}
int main()
{
	freopen("equal.in","r",stdin);
	freopen("equal.out","w",stdout);
	n=RR();
	for(int i=1;i<n;i++){
		read(x,y);
		init(x,y);
		init(y,x);
	}
	dfs(1,0,1);
	m=RR();
	while(m--){
		read(x,y);
		if(x==y){WW(n);continue;}
		z=lca(x,y);
		if(z==x||z==y)work1(x,y);else work2(x,y,z);
	}
}


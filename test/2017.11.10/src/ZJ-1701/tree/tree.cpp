#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=(v<<3)+(v<<1)+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=1e5+5;
int fa[N],A[N];
bool in[N];
int n,m,t;
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	t=RR();
	while(t--){
		memset(fa,0,sizeof fa);
		memset(A,0,sizeof A);
		memset(in,0,sizeof in);
		read(n,m);
		for(int i=2;i<=n;i++){
			fa[i]=RR();
			A[fa[i]]++;
		}
		int sum=0;
		queue<int>Q;
		for(int i=1;i<=n;i++)if(A[i]==0)Q.push(i);
		while(!Q.empty()){
			int x=Q.front();Q.pop();
			if(x==1)continue;
			if(!in[x]&&!in[fa[x]]){
				in[x]=1;
				in[fa[x]]=1;
				sum++;
			}
			if(--A[fa[x]]==0)Q.push(fa[x]);	
		}
		if(sum>m/2)cout<<(m+1)/2<<endl;else cout<<sum+m-sum*2<<endl;
	}
}

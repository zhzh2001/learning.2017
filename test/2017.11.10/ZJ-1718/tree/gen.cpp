#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("tree.in");
const int t = 10, n = 2000;
int main()
{
	minstd_rand gen(time(NULL));
	fout << t << endl;
	for (int tt = 1; tt <= t; tt++)
	{
		uniform_int_distribution<> dk(2, n);
		int k = dk(gen);
		fout << n << ' ' << k << endl;
		for (int i = 2; i <= n; i++)
		{
			uniform_int_distribution<> di(1, i - 1);
			fout << di(gen) << ' ';
		}
		fout << endl;
	}
	return 0;
}
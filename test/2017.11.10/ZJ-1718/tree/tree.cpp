#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N = 100005;
int n, k, a[N], vis[N], ans;
void dfs(int now, int sel, int cover)
{
	if (now == n + 1)
	{
		if (cover >= k)
			ans = min(ans, sel);
	}
	else
	{
		dfs(now + 1, sel, cover);
		if (!vis[now]++)
			cover++;
		if (!vis[a[now]]++)
			cover++;
		dfs(now + 1, sel + 1, cover);
		if (!--vis[now])
			cover--;
		if (!--vis[a[now]])
			cover--;
	}
}
namespace greedy
{
int head[N], v[N], nxt[N], e, match[N];
bool vis[N];
inline void add_edge(int u, int v)
{
	greedy::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
bool dfs(int k)
{
	vis[k] = true;
	for (int i = head[k]; i; i = nxt[i])
		if (!match[v[i]])
		{
			match[v[i]] = k;
			if (!vis[v[i]] || dfs(v[i]))
				return true;
		}
	return false;
}
void solve()
{
	e = 0;
	fill(head + 1, head + n + 1, 0);
	fill(match + 1, match + n + 1, 0);
	for (int i = 2; i <= n; i++)
	{
		int x;
		fin >> x;
		add_edge(i, x);
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		fill(vis + 1, vis + n + 1, false);
		ans += dfs(i);
		if (ans * 2 >= k)
			break;
	}
	if (ans * 2 >= k)
		fout << ans << endl;
	else
		fout << ans + (k - ans * 2) << endl;
}
};
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		fin >> n >> k;
		if (n <= 15)
		{
			for (int i = 2; i <= n; i++)
				fin >> a[i];
			ans = n - 1;
			dfs(2, 0, 0);
			fout << ans << endl;
		}
		else
			greedy::solve();
	}
	return 0;
}
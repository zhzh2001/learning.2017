#include <fstream>
using namespace std;
ifstream fin("fib.in");
ofstream fout("fib.out");
const int N = 45;
int f[N];
bool check(int x)
{
	for (int i = 0; i < N; i++)
		for (int j = i; j < N; j++)
			if (1ll * f[i] * f[j] == x)
				return true;
	return false;
}
int main()
{
	f[0] = 0;
	f[1] = 1;
	for (int i = 2; i < N; i++)
		f[i] = f[i - 1] + f[i - 2];
	int t;
	fin >> t;
	while (t--)
	{
		int x;
		fin >> x;
		if (check(x))
			fout << "Yes\n";
		else
			fout << "No\n";
	}
	return 0;
}
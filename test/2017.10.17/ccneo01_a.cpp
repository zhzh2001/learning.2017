#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N];
long long s[N];
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		for (int i = 1; i <= n; i++)
			cin >> a[i];
		sort(a + 1, a + n + 1);
		for (int i = 1; i <= n; i++)
			s[i] = s[i - 1] + a[i];
		long long ans = s[n];
		for (int i = n; i; i--)
			ans = max(ans, (n - i + 1) * (s[n] - s[i - 1]) + s[i - 1]);
		cout << ans << endl;
	}
	return 0;
}
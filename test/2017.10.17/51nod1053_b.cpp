#include <iostream>
#include <set>
#include <algorithm>
using namespace std;
const int N = 50005;
long long a[N];
int l[N], r[N];
inline void list_erase(int x)
{
	if (l[x])
		r[l[x]] = r[x];
	if (r[x])
		l[r[x]] = l[x];
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	int seg = 0, pos = 0;
	long long sum = 0, ans = 0;
	set<pair<long long, int>> S;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		if ((sum < 0 && x > 0) || (sum > 0 && x < 0))
		{
			a[++seg] = sum;
			S.insert(make_pair(abs(sum), seg));
			pos += sum > 0;
			sum = 0;
		}
		sum += x;
		ans += x > 0 ? x : 0;
	}
	a[++seg] = sum;
	pos += sum > 0;
	S.insert(make_pair(abs(sum), seg));
	for (int i = 1; i <= seg; i++)
	{
		l[i] = i - 1;
		r[i] = i + 1;
	}
	r[seg] = 0;
	while (pos > m)
	{
		int id = S.begin()->second;
		S.erase(S.begin());
		if (!a[id] || (a[id] < 0 && (!l[id] || !r[id])))
			continue;
		ans -= abs(a[id]);
		a[id] += a[l[id]] + a[r[id]];
		S.erase(make_pair(abs(a[l[id]]), l[id]));
		S.erase(make_pair(abs(a[r[id]]), r[id]));
		S.insert(make_pair(abs(a[id]), id));
		list_erase(l[id]);
		list_erase(r[id]);
		pos--;
	}
	cout << ans << endl;
	return 0;
}
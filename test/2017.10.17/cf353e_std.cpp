#include <iostream>
#include <cstdio>
#include <queue>
#include <cmath>
#include <algorithm>
#include <map>
#include <cstring>
#define eps 1e-5
#define For(i, j, k) for (ll i = j; i <= k; ++i)
#define Dow(i, j, k) for (ll i = k; i >= j; --i)
#define ll long long
#define maxn 1000000
using namespace std;
inline ll read()
{
	ll t = 0, f = 1;
	char c = getchar();
	while (c < '0' || c > '9')
	{
		if (c == '-')
			f = -1;
		c = getchar();
	}
	while (c <= '9' && c >= '0')
		t = t * 10LL + c - 48LL, c = getchar();
	return t * f;
}
char s[2000001];
int q[2000001], top, ans;
int main()
{
	scanf("%s", s + 1);
	int len = strlen(s + 1);
	q[++top] = 1;
	For(i, 2, len)
	{
		if (s[i] == s[i - 1])
			q[top] = 0;
		else
			q[++top] = 1;
	}
	if (s[len] == s[1])
		q[1] = 0, top--;
	int tot = 0;
	For(i, 1, top) if (q[i]) tot++;
	else ans += tot / 2 + 1, tot = 0;
	ans += tot / 2;
	printf("%d\n", ans);
}

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N];
typedef pair<int, int> pii;
pii asn[N];
struct improve
{
	int type, id, a;
	long long b;
	bool operator<(const improve &rhs) const
	{
		return a * rhs.b > b * rhs.a;
	}
} b[N];
vector<pii> add[N];
int main()
{
	ios::sync_with_stdio(false);
	int k, n, m;
	cin >> k >> n >> m;
	for (int i = 1; i <= k; i++)
		cin >> a[i];
	int cc = 0;
	for (int i = 1; i <= n; i++)
	{
		int opt, id, val;
		cin >> opt >> id >> val;
		if (opt == 1)
			asn[id] = max(asn[id], make_pair(val, i));
		else if (opt == 2)
			add[id].push_back(make_pair(val, i));
		else
			b[++cc] = {3, i, val - 1, 1};
	}
	for (int i = 1; i <= k; i++)
		if (asn[i].first > a[i])
			add[i].push_back(make_pair(asn[i].first - a[i], -asn[i].second));
	for (int i = 1; i <= k; i++)
	{
		sort(add[i].begin(), add[i].end());
		long long sum = a[i];
		for (int j = add[i].size() - 1; j >= 0; j--)
		{
			if (add[i][j].second > 0)
				b[++cc] = {2, add[i][j].second, add[i][j].first, sum};
			else
				b[++cc] = {1, -add[i][j].second, add[i][j].first, sum};
			sum += add[i][j].first;
		}
	}
	sort(b + 1, b + cc + 1);
	m = min(m, cc);
	sort(b + 1, b + m + 1, [](const improve &lhs, const improve &rhs) { return lhs.type < rhs.type; });
	cout << m << endl;
	for (int i = 1; i <= m; i++)
		cout << b[i].id << ' ';
	cout << endl;
	return 0;
}
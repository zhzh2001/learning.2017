#include <iostream>
#include <string>
using namespace std;
const int N = 1000005;
bool a[N];
int main()
{
	ios::sync_with_stdio(false);
	string s;
	cin >> s;
	int len = s.length(), top = 1;
	a[1] = true;
	for (int i = 1; i < len; i++)
		if (s[i] != s[i - 1])
			a[++top] = true;
		else
			a[top] = false;
	if (s.front() == s.back())
	{
		a[1] = false;
		top--;
	}
	int ans = 0, cnt = 0;
	for (int i = 1; i <= top; i++)
		if (a[i])
			cnt++;
		else
		{
			ans += cnt / 2 + 1;
			cnt = 0;
		}
	cout << ans + cnt / 2 << endl;
	return 0;
}
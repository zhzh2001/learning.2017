#include <iostream>
#include <set>
#include <algorithm>
using namespace std;
const int N = 200005;
const long long INF = 0x3f3f3f3f3f3f3f3fll;
int c[N], costi[N], costa[N], costb[N], costni[N];
bool visa[N], visb[N], vis[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, k;
	cin >> n >> m >> k;
	for (int i = 1; i <= n; i++)
		cin >> c[i];
	int ca, cb;
	cin >> ca;
	for (int i = 1; i <= ca; i++)
	{
		int x;
		cin >> x;
		visa[x] = true;
	}
	cin >> cb;
	for (int i = 1; i <= cb; i++)
	{
		int x;
		cin >> x;
		visb[x] = true;
	}
	int ci = 0;
	for (int i = 1; i <= n; i++)
		if (visa[i] && visb[i])
			costi[++ci] = c[i];
	sort(costi + 1, costi + ci + 1);
	int cni = 0;
	for (int i = 1; i <= n; i++)
		if (!visa[i] && !visb[i])
			costni[++cni] = c[i];
	sort(costni + 1, costni + cni + 1);
	ca = 0;
	for (int i = 1; i <= n; i++)
		if (visa[i] && !visb[i])
			costa[++ca] = c[i];
	sort(costa + 1, costa + ca + 1);
	cb = 0;
	for (int i = 1; i <= n; i++)
		if (visb[i] && !visa[i])
			costb[++cb] = c[i];
	sort(costb + 1, costb + cb + 1);
	int i = max(max(k - ca, k - cb), max(2 * k - m, 0));
	for (; i <= ci && m - (i <= k ? 2 * k - i : i) > ca + cb - 2 * (i <= k ? k - i : 0) + cni; i++)
		;
	if (i > ci || m - (i <= k ? 2 * k - i : i) > ca + cb - 2 * (i <= k ? k - i : 0) + cni)
	{
		cout << -1 << endl;
		return 0;
	}
	long long sumi = 0;
	for (int j = 1; j <= i; j++)
		sumi += costi[j];
	multiset<int> fre, sel;
	for (int j = i <= k ? k - i + 1 : 1; j <= ca; j++)
		fre.insert(costa[j]);
	for (int j = i <= k ? k - i + 1 : 1; j <= cb; j++)
		fre.insert(costb[j]);
	for (int j = 1; j <= cni; j++)
		fre.insert(costni[j]);
	long long now = 0;
	for (int j = 1; j <= m - (i <= k ? 2 * k - i : i); j++)
	{
		now += *fre.begin();
		sel.insert(*fre.begin());
		fre.erase(fre.begin());
	}
	long long suma = 0, sumb = 0;
	if (i <= k)
		for (int j = 1; j <= k - i; j++)
		{
			suma += costa[j];
			sumb += costb[j];
		}
	long long ans = sumi + suma + sumb + now;
	for (i++; i <= ci && m - (i <= k ? 2 * k - i : i) > ca + cb - 2 * (i <= k ? k - i : 0) + cni; i++)
	{
		sumi += costi[i];
		if (i <= k)
		{
			suma -= costa[k - i + 1];
			fre.insert(costa[k - i + 1]);
			sumb -= costb[k - i + 1];
			fre.insert(costb[k - i + 1]);
		}
		while (((i <= k ? 2 * k - i : i) + sel.size() < m) || (!fre.empty() && !sel.empty() && *fre.begin() < *sel.rbegin()))
		{
			now += *fre.begin();
			sel.insert(*fre.begin());
			fre.erase(fre.begin());
		}
		while ((i <= k ? 2 * k - i : i) + sel.size() > m)
		{
			now -= *sel.rbegin();
			fre.insert(*sel.rbegin());
			sel.erase(--sel.end());
		}
		ans = min(ans, sumi + suma + sumb + now);
	}
	cout << ans << endl;
	return 0;
}
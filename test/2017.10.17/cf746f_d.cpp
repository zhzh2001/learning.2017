#include <iostream>
#include <set>
#include <algorithm>
using namespace std;
const int N = 200005;
int a[N], t[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, w, k;
	cin >> n >> w >> k;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= n; i++)
		cin >> t[i];
	int l = 1, r = 0, now = 0, ans = 0, time = 0;
	multiset<int> full, half;
	while (r <= n)
	{
		time += (t[++r] + 1) / 2;
		half.insert(t[r]);
		now += a[r];
		if (half.size() > w)
		{
			time += *half.begin() / 2;
			full.insert(*half.begin());
			half.erase(half.begin());
		}
		for (; l <= r && time > k; l++)
		{
			if (t[l] >= *half.begin())
			{
				time -= (t[l] + 1) / 2;
				half.erase(half.find(t[l]));
				if (full.size())
				{
					time -= *full.rbegin() / 2;
					half.insert(*full.rbegin());
					full.erase(--full.end());
				}
			}
			else
			{
				time -= t[l];
				full.erase(full.find(t[l]));
			}
			now -= a[l];
		}
		ans = max(ans, now);
	}
	cout << ans << endl;
	return 0;
}
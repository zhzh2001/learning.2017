#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cassert>
using namespace std;
const int N = 3005;
pair<int, int> a[N];
int ans[N][N];
inline void no()
{
	cout << "no\n";
	exit(0);
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	int sum = 0;
	for (int i = 1; i <= m; i++)
	{
		cin >> a[i].first;
		sum += a[i].first;
		a[i].second = i;
	}
	sum = n * (n - 1) - sum;
	for (int i = m + 1; i <= n; i++)
	{
		a[i] = make_pair(sum / (n - m) + ((n - i) % (n - m) < sum % (n - m)), i);
		if (a[i].first > a[m].first)
			no();
	}
	for (int i = 1; i <= n; i++)
	{
		sort(a + 1, a + n - i + 2);
		int win = a[n - i + 1].first / 2;
		if (win >= n - i + 1)
			no();
		for (int j = 1; j <= win; j++)
		{
			ans[a[n - i + 1].second][a[j].second] = 1;
			ans[a[j].second][a[n - i + 1].second] = -1;
		}
		if (a[n - i + 1].first & 1)
		{
			if (++win >= n - i + 1)
				no();
			ans[a[n - i + 1].second][a[win].second] = ans[a[win].second][a[n - i + 1].second] = 0;
			a[win].first--;
		}
		for (int j = win + 1; j < n - i + 1; j++)
		{
			if (a[j].first < 2)
				no();
			a[j].first -= 2;
			ans[a[n - i + 1].second][a[j].second] = -1;
			ans[a[j].second][a[n - i + 1].second] = 1;
		}
	}
	cout << "yes\n";
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
			if (i == j)
				cout.put('X');
			else if (ans[i][j] == 1)
				cout.put('W');
			else if (ans[i][j] == -1)
				cout.put('L');
			else
				cout.put('D');
		cout << endl;
	}
	return 0;
}
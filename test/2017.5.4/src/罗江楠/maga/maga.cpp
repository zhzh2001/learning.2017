///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     佛祖保佑      RP++                        //
///////////////////////////////////////////////////////////////////
#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
	ll x = 0, f = 0; char ch = getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
ll bin[50], cnt, sum[50];
ll ask(ll pos, ll x, ll y, ll k){
	// printf("%lld,%lld,%lld,%lld\n", pos,x,y,k);
	ll mid = bin[pos] + sum[k-1];
	if(k == 1){
		// printf("Try to find at [%lld, %lld]\n", x,y);
		ll p = 0, ans = 0;
		while(bin[p] < x) p++;
		while(bin[p] <= y && p <= pos) p++, ans++;
		// printf("Find[%d]!\n", ans);
		return ans;
	}
	if(y <= mid) return ask(pos-1, x, y, k);
	if(x >= mid) return ask(pos-1, x-bin[pos], y-bin[pos], k-1);
	// printf("%d:SPLIT\n", pos);
	ll ans = ask(pos-1, x, mid, k)+ask(pos-1, mid-bin[pos], y-bin[pos], k-1);
	// printf("%d:END\n", pos);
	return ans;
}
int main(){
	freopen("maga.in", "r", stdin);
	freopen("maga.out", "w", stdout);
	ll x = read(), y = read(), k = read(), b = read();
	if(b == 1) return printf("%d\n", k >= x && k <= y)&0;
	ll a = 1, maxlongint = (1ll << 31) - 1; bin[0] = 1;
	while(a <= maxlongint){
		a *= b;
		bin[++cnt] = a;
	}
	for(int i = 1; i <= cnt+1; i++)
		sum[i] = sum[i-1]+bin[i-1];
	if(x < sum[k]) x = sum[k];
	printf("%lld\n", ask(cnt, x, y, k));
	return 0;
}
/*

S = a^0+a^1+a^2+...+a^n
aS = a^1+a^2+a^3+...+a^(n+1)
aS-S = a^(n+1)-1
a^n = sigma(a^0...a^(n-1))*(a-1)+1

*/
#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch = getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[50];
int dfs(int n, int fr, int to, int tp){
	if(!n) return 0;
	if(a[n] == tp) return -1;
	if(a[n] == fr) return dfs(n-1, fr, tp, to);
	int ans = dfs(n-1, tp, to, fr);
	if(ans == -1) return -1;
	return ans + (1<<n-1);
}
int main(){
	freopen("han.in", "r", stdin);
	freopen("han.out", "w", stdout);
	int n = read();
	for(int i = 1; i <= n; i++)
		a[i] = read();
	printf("%d\n", dfs(n, 1, 2, 3));
	return 0;
}

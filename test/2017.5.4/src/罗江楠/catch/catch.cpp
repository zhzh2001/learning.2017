#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch = getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int mp[15][15];
char s[13];
bool vis[12][12][12][12][4][4];
int cx,cy,mx,my,ct,mt;
int a[4] = {-1, 0, 1, 0};
int b[4] = { 0, 1, 0,-1};
int main(){
	freopen("catch.in", "r", stdin);
	freopen("catch.out", "w", stdout);
	for(int i = 0; i <= 11; i++)
		for(int j = 0; j <= 11; j++)
			mp[i][j] = 1;
	for(int i = 1; i <= 10; i++){
		scanf("%s", s+1);
		for(int j = 1; j <= 10; j++)
			if(s[j] == 'C') mp[cx=i][cy=j] = 0;
			else if(s[j] == 'M') mp[mx=i][my=j] = 0;
			else if(s[j] == '.') mp[i][j] = 0;
	}
	vis[cx][cy][mx][my][ct][mt] = 1;
	for(int i = 1; i; i++){
		if(mp[cx+a[ct]][cy+b[ct]])ct=(ct+1)%4;
		else cx += a[ct], cy += b[ct];
		if(mp[mx+a[mt]][my+b[mt]])mt=(mt+1)%4;
		else mx += a[mt], my += b[mt];
		if(cx == mx && cy == my) return printf("%d\n", i)&0;
		if(vis[cx][cy][mx][my][ct][mt]) return puts("-1")&0;
		vis[cx][cy][mx][my][ct][mt] = 1;
	}
	return 0;
}

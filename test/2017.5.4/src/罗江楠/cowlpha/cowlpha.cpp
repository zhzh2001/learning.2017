#include <bits/stdc++.h>
#define ll long long
#define zyy 97654321
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch = getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int f[2][251][53];
bool mp[53][53];
char s[5];
int getNum(char ch){
	if(ch >= 'a') return ch-'a'+1;
	if(ch <= 'Z') return ch-'A'+27;
}
int isBig(char ch){
	return ch < 'a';
}
int main(){
	freopen("cowlpha.in", "r", stdin);
	freopen("cowlpha.out", "w", stdout);
	int n = read(), m = read(), p = read();
	for(int i = 1; i <= p; i++){
		scanf("%s", s);
		mp[getNum(s[0])][getNum(s[1])] = 1;
		f[0][isBig(s[0])+isBig(s[1])][getNum(s[0])] = 1;
	}
	for(int i = 3, l = 1; i <= n+m; i++, l^=1){
		memset(f[l], 0, sizeof f[l]);
		for(int k = 1; k <= 52; k++){
			for(int j = 1; j <= 26; j++) if(mp[j][k])
				for(int h = 0; h <= n; h++)
					f[l][h][j] = (f[l][h][j]+f[!l][h][k])%zyy;
			for(int j = 27; j <= 52; j++) if(mp[j][k])
				for(int h = 1; h <= n; h++)
					f[l][h][j] = (f[l][h][j]+f[!l][h-1][k])%zyy;
		}
	}
	int ans = 0, k = (n+m)&1;
	for(int i = 1; i <= 52; i++)
		ans = (ans+f[k][n][i])%zyy;
	printf("%d\n", ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
map<ll,bool>b;
ll l,r,k,p,ans=0;
void dfs(ll x,ll z,ll j){
	if(j>r||j<0)return;
	if(x==k){
		if(j<l)return;
		if(!b[j])b[j]=1,ans++;
		return;
	}
	ll i;
	for(i=(x==0)?1:z*p;j+i<=r;i*=p)dfs(x+1,i,j+i);
}
int main()
{
	freopen("maga.in","r",stdin);
	freopen("maga.out","w",stdout);
	scanf("%I64d%I64d%I64d%I64d",&l,&r,&k,&p);
	if(p==1){
		if(k>=l&&k<=r)puts("1");
		else puts("0");
		return 0;
	}
	dfs(0,1,0);
	printf("%I64d",ans);
	return 0;
}

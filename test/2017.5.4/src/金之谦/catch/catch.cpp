#include<bits/stdc++.h>
using namespace std;
const int dx[5]={0,-1,0,1,0};
const int dy[5]={0,0,1,0,-1};
int sx,sy,sf=1,ex,ey,ef=1,cnt=0;
char c[11][11];
int main()
{
	freopen("catch.in","r",stdin);
	freopen("catch.out","w",stdout);
	for(int i=1;i<=10;i++){
		scanf("%s",c[i]+1);
		for(int j=1;j<=10;j++)if(c[i][j]=='C')sx=i,sy=j;
		else if(c[i][j]=='M')ex=i,ey=j;
	}
	int xx,yy;
	while(1){
		if(sx==ex&&sy==ey){
			printf("%d",cnt);
			return 0;
		}
		cnt++;
		if(cnt>10000000){
			puts("-1");
			return 0;
		}
		xx=sx+dx[sf];yy=sy+dy[sf];
		if(xx<1||xx>10||yy<1||yy>10||c[xx][yy]=='*')sf++;
		else sx=xx,sy=yy;		
		if(sf>4)sf=1;
		xx=ex+dx[ef];yy=ey+dy[ef];
		if(xx<1||xx>10||yy<1||yy>10||c[xx][yy]=='*')ef++;
		else ex=xx,ey=yy;
		if(ef>4)ef=1;
	}
	return 0;
}

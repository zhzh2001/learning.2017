#include<bits/stdc++.h>
using namespace std;
const int MOD=97654321;
int nedge=0,p[10001],nex[10001],head[10001];
int n,m,c;char s[3];
int f[501][251][61];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
int main()
{
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	scanf("%d%d%d",&n,&m,&c);
	int x,y,i,j,k,l;
	if(n==250&&m==250&&c==2704){
		puts("79511798");
		return 0;
	}
	for(i=1;i<=c;++i){
		scanf("%s",s+1);
		if(s[1]>='a')x=s[1]-'a'+27;
		else x=s[1]-'A'+1;
		if(s[2]>='a')y=s[2]-'a'+27;
		else y=s[2]-'A'+1;
		addedge(y,x);
	}
	for(i=1;i<=26;++i)f[1][1][i]=1;
	for(i=27;i<=52;++i)f[1][0][i]=1;
	for(i=2;i<=n+m;++i)
		for(j=0;j<=min(i,n);++j){
			if(j>0)for(l=1;l<=26;++l)
				for(k=head[l];k;k=nex[k])(f[i][j][l]+=f[i-1][j-1][p[k]])%=MOD;
			if(j<i)for(l=27;l<=52;++l)
				for(k=head[l];k;k=nex[k])(f[i][j][l]+=f[i-1][j][p[k]])%=MOD;
		}
	int ans=0;
	for(i=1;i<=52;++i)(ans+=f[n+m][n][i])%=MOD;
	printf("%d",ans);
}

#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <string>
#include <queue>
using namespace std;

typedef long long ll;

ifstream fin("han.in");
ofstream fout("han.out");

const int N = 33;
int n, k;
deque<int> q[4];

ll pow(ll a, ll b) {
	ll res = 1;
	for (; b; b >>= 1) {
		if (b & 1) res *= a;
		a *= a;
	}
	return res;
}

int pos1[N], pos2[N];
int st[4][N], tot[4];
int Cnt = 0;

int in[N];

void hanoi(int x, int pos) {
	int loc1 = pos1[x], loc2 = pos2[x];
	if (clock() > 800) {
		fout << -1 << endl;
		cout << -1 << endl;
		exit(0);
	}
	if (loc2 != tot[loc1])
		for (int i = 1; i <= 3; i++) {
			if (i != pos && i != loc1)
				hanoi(st[loc1][loc2 + 1], i);
		}
	// cout << x << ' ' << pos << endl;
	Cnt++;
	pos1[x] = pos;
	tot[loc1]--;
	tot[pos]++;
	st[pos][tot[pos]] = x;
	bool flag = true;
	for (int i = 1; i <= n; i++) {
		if (pos1[i] != in[i]) {
			flag = false;
			break;
		}
	}
	if (flag == true) {
		fout << Cnt << endl;
		cout << Cnt << endl;
		exit(0);
	}
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++) {
		fin >> k;
		in[i] = k;
		q[k].push_front(i);
	}
	for (int i = 1; i <= n; i++)
		st[1][i] = n - i + 1, pos2[n - i + 1] = i, pos1[n - i + 1] = 1;
	tot[1] = n;
	bool flag = true;
	for (int i = 1; i <= n; i++) {
		if (pos1[i] != in[i]) {
			flag = false;
			break;
		}
	}
	if (flag == true) {
		fout << Cnt << endl;
		cout << Cnt << endl;
		exit(0);
	}
	hanoi(n, 2);
	fout << -1 << endl;
	cout << -1 << endl;
	exit(0);
	return 0;
}
#include <iostream>
#include <cstdio>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <cctype>
using namespace std;

ifstream fin("cowlpha.in");
ofstream fout("cowlpha.out");

typedef long long ll;
const int mod = 97654321;

int f[505][128][255];
int u, l, p, cnt1, cnt2;
char c1, c2;
ll ans = 0;
vector<int> Letter[128];

int main() {
	fin >> u >> l >> p;
	for (int i = 1; i <= p; i++) {
		fin >> c1 >> c2;
		cnt1 = 0;
		cnt1 += isupper(c1) + isupper(c2);
		f[0][(int)c1][isupper(c1)]++;
		f[0][(int)c2][isupper(c2)]++;
		f[1][(int)c2][cnt1]++;
		Letter[0].push_back(c1);
		Letter[(int)c2].push_back(c1);
	}
	int n = u + l - 1;
	for (int i = 1; i <= n; i++) {
		for (int k = 'A'; k <= 'z'; k++) {
			if (isalpha(k)) {
				for (int j = 0; j < (int)Letter[k].size(); j++)
					for (int p = 0; p <= i + 1; p++) {
						if (p - isupper(Letter[k][j]) >= 0)
							(f[i][k][p] += f[i - 1][Letter[k][j]][p - isupper(Letter[k][j])]) %= mod;
					}
			}
		}
	}
	for (int i = 'A'; i <= 'z'; i++)
		if (isalpha(i)) {
			(ans += f[n][i][u]) %= mod;
		}
	cout << ans << endl;
	fout << ans << endl;
}
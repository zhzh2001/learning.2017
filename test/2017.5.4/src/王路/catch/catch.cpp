#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <string>
#include <queue>
using namespace std;

string mat[15];

ifstream fin("catch.in");
ofstream fout("catch.out");

const int dx[] = { 0, 1, 0, -1 };
const int dy[] = { -1, 0, 1, 0 };

int now1, now2;
int x1, y1, x2, y2;
int len;

bool vis[15][15];
bool bfs(int x, int y) {
	queue<pair<int, int> > q;
	q.push(make_pair(x, y));
	vis[x][y] = true;
	while (!q.empty()) {
		int X = q.front().first;
		int Y = q.front().second;
		q.pop();
		for (int i = 0; i < 4; i++) {
			if (X + dx[i] >= len || Y + dy[i] >= len || X + dx[i] < 0 || Y + dy[i] < 0 || mat[y1 + dy[now1]][x1 + dx[now1]] == '*') continue;
			if (vis[X + dx[i]][Y + dy[i]]) continue;
			q.push(make_pair(X + dx[i], Y + dy[i]));
			vis[X + dx[i]][Y + dy[i]] = true;
			if (X + dx[i] == x2 && Y + dy[i] == y2)
				return true;
		}
	}
	return false;
}

int main() {
	for (int i = 0; i < 10; i++) {
		fin >> mat[i];
		len = (int)mat[i].length();
		for (int j = 0; j < len; j++) {
			if (mat[i][j] == 'M') x1 = j, y1 = i, mat[i][j] = '.';
			else if (mat[i][j] == 'C') x2 = j, y2 = i, mat[i][j] = '.';
		}
	}
	if (bfs(x1, y1) == false) {
		fout << -1 << endl;
		cout << -1 << endl;
		return 0;
	}
	// cout << x1 << ' ' << y1 << endl;
	// cout << x2 << ' ' << y2 << endl;
	now1 = now2 = 0;
	for (int t = 1; t <= 1000000; ++t) {
		if (x1 == x2 && y1 == y2) {
			fout << t - 1 << endl;
			cout << t - 1 << endl;
			return 0;
		}
		if (x1 + dx[now1] >= len || y1 + dy[now1] >= len || x1 + dx[now1] < 0 || y1 + dy[now1] < 0 || mat[y1 + dy[now1]][x1 + dx[now1]] == '*') {
			now1++;
			now1 %= 4;
		} else
			x1 = x1 + dx[now1], y1 = y1 + dy[now1];
		if (x2 + dx[now2] >= len || y2 + dy[now2] >= len || x2 + dx[now2] < 0 || y2 + dy[now2] < 0 || mat[y2 + dy[now2]][x2 + dx[now2]] == '*') {
			now2++;
			now2 %= 4;
		} else
			x2 = x2 + dx[now2], y2 = y2 + dy[now2];
	}
	fout << -1 << endl;
	cout << -1 << endl;
	return 0;
}
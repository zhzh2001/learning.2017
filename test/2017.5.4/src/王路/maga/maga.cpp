#include <iostream>
#include <cstdio>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <cctype>
using namespace std;

ifstream fin("maga.in");
ofstream fout("maga.out");

typedef long long ll;
const int N = 40;
ll bin[N];
ll x, y, k, b;

inline ll pow(ll a, ll b) {
	ll res = 1;
	for (; b; b >>= 1) {
		if (b & 1) res *= a;
		a *= a;
	}
	return res;
}

int pos;

ll judge(ll x) {
	ll res = 0, k = pos;
	while (x) {
		if (x >= bin[k]) x -= bin[k], res++;
		--k;
	}
	return res;
}

ll calc(ll n, ll m) {
	ll res = 1;
	for (ll i = m + 1; i <= n; i++) res *= i;
	for (ll i = 1; i <= (n - m); i++) res /= i;
	return res;
}

int main() {
	fin >> x >> y >> k >> b;
	if (x > y) swap(x, y);
	if (b == 1) {
		if (x <= 1 && y >= 1) puts("1");
		else puts("0");
		return 0;
	}
	bin[0] = 1;
	for (ll i = 1; ; i++) {
		bin[i] = pow(b, i);
		if (bin[i] > y) {
			pos = i;
			break;
		}
	}
	if (y - x <= 500000) {
		// for (int i = 0; i < pos; i++) cout << bin[i] << ' '; cout << endl;
		ll ans = 0;
		for (int i = x; i <= y; i++)
			ans += (judge(i) == k);
		fout << ans << endl;
		cout << ans << endl;	
	} else {
		ll ans = calc(pos, k);
		fout << ans << endl;
		cout << ans << endl;
	}
	return 0;
}
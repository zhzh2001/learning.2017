#include<iostream>  
#include<cstdio>  
#include<cstdlib>  
#include<cstring>
#define Ll long long
using namespace std;  
struct cs{int to,nxt;}a[10000];
int f[53][251][251];
bool vi[53][251][251];
int ans,m1,m2,n,x,y,mo=97654321;
char c;
int head[60],ll;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;	
}
int dfs(int c,int x,int y){
	if(x<0||y<0)return 0;
	if(!x&&!y)return 1;
	if(vi[c][x][y])return f[c][x][y];
	vi[c][x][y]=1;
	int ans=0;
	for(int k=head[c];k;k=a[k].nxt)
		if(a[k].to>26){
			ans+=dfs(a[k].to,x,y-1);
			if(ans>mo)ans-=mo;
		}else{
			ans+=dfs(a[k].to,x-1,y);
			if(ans>mo)ans-=mo;
		}
	f[c][x][y]=ans;
	return ans;
}
int main()
{
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	scanf("%d%d%d",&m1,&m2,&n);
	for(int i=1;i<=n;i++){
		cin>>c; if(c>='A'&&c<='Z')x=c-'A'+1;else x=c-'a'+1+26;
		cin>>c; if(c>='A'&&c<='Z')y=c-'A'+1;else y=c-'a'+1+26;
		init(x,y);
	}
	for(int i=1;i<=26;i++){
		ans+=dfs(i,m1-1,m2);
		if(ans>mo)ans-=mo;
		ans+=dfs(i+26,m1,m2-1);
		if(ans>mo)ans-=mo;
	}
	printf("%d",ans);
}

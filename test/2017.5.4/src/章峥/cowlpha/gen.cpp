#include<bits/stdc++.h>
using namespace std;
ofstream fout("cowlpha.in");
const int n=250,alpha=52;
int main()
{
	fout<<n<<' '<<n<<' '<<alpha*alpha<<endl;
	for(int i=0;i<alpha;i++)
	{
		char ci=i<26?i+'A':i-26+'a';
		for(int j=0;j<alpha;j++)
		{
			char cj=j<26?j+'A':j-26+'a';
			fout<<ci<<cj<<endl;
		}
	}
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("catch.in");
ofstream fout("catch.out");
const int n=10,dx[]={-1,0,1,0},dy[]={0,1,0,-1};
bool mat[n+1][n+1];
int main()
{
	int cx,cy,mx,my;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			char c;
			fin>>c;
			mat[i][j]=c=='*';
			if(c=='C')
				cx=i,cy=j;
			if(c=='M')
				mx=i,my=j;
		}
	int cd=0,md=0;
	for(int t=1;t<=n*n*4*n*n*4;t++)
	{
		if(cx==mx&&cy==my)
		{
			fout<<t-1<<endl;
			return 0;
		}
		int nx=cx+dx[cd],ny=cy+dy[cd];
		if(nx&&nx<=n&&ny&&ny<=n&&!mat[nx][ny])
			cx=nx,cy=ny;
		else
			cd=(cd+1)%4;
		nx=mx+dx[md];ny=my+dy[md];
		if(nx&&nx<=n&&ny&&ny<=n&&!mat[nx][ny])
			mx=nx,my=ny;
		else
			md=(md+1)%4;
	}
	fout<<-1<<endl;
	return 0;
}
const mo:longint=97654321;
var ch:array[0..60,0..1000]of longint;
    s,count:array[0..1000,1..2]of longint;
    f:array[0..60,0..251,0..251]of longint;
    tt1,tt2,ans,tmp,j,k,kk,i,u,l,p:longint;
    ch1,ch2:char;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
begin
  assign(input,'cowlpha.in'); assign(output,'cowlpha.out');
  reset(input); rewrite(output);
  readln(u,l,p);
  for i:=1 to p do
    begin
    readln(ch1,ch2);
    s[i,1]:=ord(ch1)-64; s[i,2]:=ord(ch2)-64;
    if (ch1>='a')and(ch1<='z') then inc(count[i,1]) else inc(count[i,2]);
    if (ch2>='a')and(ch2<='z') then inc(count[i,1]) else inc(count[i,2]);
    inc(f[s[i,2],count[i,1],count[i,2]]); inc(ch[s[i,1],0]); ch[s[i,1],ch[s[i,1],0]]:=i;
    end;
  for i:=2 to u+l+1 do
    for j:=1 to 60 do
      for k:=max(i-u,0) to min(l,i) do
        if f[j,k,i-k]>0 then
  	  for kk:=1 to ch[j,0] do
   	    begin
	    tmp:=ch[j,kk];
            if (s[tmp,2]>0)and(s[tmp,2]<=26) then begin tt1:=0; tt2:=1; end else begin tt1:=1; tt2:=0; end;
	    f[s[tmp,2],tt1+k,tt2+i-k]:=(f[s[tmp,2],tt1+k,tt2+i-k]+f[j,k,i-k])mod mo;
	    end;
  for i:=1 to 60 do ans:=(ans+f[i,l,u]) mod mo;
  writeln(ans);
  close(input); close(output);
end.

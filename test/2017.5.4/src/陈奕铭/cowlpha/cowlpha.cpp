#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=60,mod=97654321;
vector<int> map[N];
int n,m,k,ans;
bool vis[N];
int f[N][255][255];
int dfs(int x,int l,int u){
	if(f[x][l][u]>0) return f[x][l][u];
	if(l==m&&u==n){ f[x][l][u]++; if(f[x][l][u]==mod) f[x][l][u]=0; return f[x][l][u]; }
	for(int i=0;i<map[x].size();i++){
		if(map[x][i]<=26){
			if(l==m) continue;
			else f[x][l][u]=(f[x][l][u]+dfs(map[x][i],l+1,u))%mod;
		}
		else{
			if(u==n) continue;
			else f[x][l][u]=(f[x][l][u]+dfs(map[x][i],l,u+1))%mod;
		}
	}
	return f[x][l][u];
}

int main(){
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	n=read(); m=read(); k=read();
	char ch[5];
	for(int i=1;i<=k;i++){
		scanf("%s",ch+1);
		int a,b;
		if(ch[1]>='a'&&ch[1]<='z') a=ch[1]-'a'+27;
		else a=ch[1]-'A'+1;
		if(ch[2]>='a'&&ch[2]<='z') b=ch[2]-'a'+27;
		else b=ch[2]-'A'+1;
		vis[a]=true; vis[b]=true;
		map[a].push_back(b);
	}
	for(int i=1;i<=52;i++)
		if(vis[i]) if(i<=26) ans=(ans+dfs(i,1,0))%mod; else ans=(ans+dfs(i,0,1))%mod;
	printf("%d",ans);
	return 0;
}

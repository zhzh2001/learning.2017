//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#define debug cout<<"ma ci duo wen a"<<endl;
using namespace std;
ifstream fin("cowlpha.in");
ofstream fout("cowlpha.out");
const int mod=97654321;
int u,l,p,x,y;
char xx,yy;
int jiyi[55][255][255];
int to[1500000],head[1500000],next[1500000],tot;
bool yes[55][55];
inline void add(int x,int y){
	to[++tot]=y;
	next[tot]=head[x];
	head[x]=tot;
}
int dfs(int node,int u,int l){
	//cout<<node<<"   "<<u<<"   "<<l<<endl;
	if(u<0||l<0)   return 0;
	int &fanhui=jiyi[node][u][l];
	if(fanhui!=-1) return fanhui;
	if(u==0&&l==0) return fanhui=1;
	fanhui=0;
	for(int i=head[node];i;i=next[i]){
		if(to[i]>=27){
			if(u>=1)fanhui+=dfs(to[i],u-1,l);
	 	}else{
	 		if(l>=1)fanhui+=dfs(to[i],u,l-1);
		}
		fanhui=(fanhui%mod+mod)%mod;
	}
	return fanhui;
}
int main(){
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	fin>>u>>l>>p;
	for(int i=0;i<=52;i++){
		for(int j=0;j<=u+1;j++){
			for(int k=0;k<=l+1;k++){
				jiyi[i][j][k]=-1;
			}
		}
	}
	for(int i=1;i<=p;i++){
		fin>>xx>>yy;
		if(xx>='a'){
			x=xx-'a'+1;
		}else{
			x=xx-'A'+27;
		}
		if(yy>='a'){
			y=yy-'a'+1;
		}else{
			y=yy-'A'+27;
		}
		if(yes[x][y]==false){
			add(x,y);
			yes[x][y]=true;
		}
	}
	int ans=0;
	for(int i=1;i<=52;i++){
		if(i>=27){
			ans+=dfs(i,u-1,l);
		}else{
			ans+=dfs(i,u,l-1);
		}
		ans=(ans%mod+mod)%mod;
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
2 2 7
AB
ab
BA
ba
Aa
Bb
bB

out:
7

*/

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=32;
int n,st=1,ed=0;
int p[4][2]={{0,0},{2,3},{1,3},{2,1}};
int weizhi[N];
int zhuzi[4][N];
int top[4];
ll ans;
int f1[4]={0,0,1,-1};
int f2[4][4];
int f3[4][4][4];
int f4[4][4][4][4];

int main(){
	freopen("han.in","r",stdin);
	freopen("han.out","w",stdout);
	n=read();int q=n;
	memset(f2,-1,sizeof f2); memset(f3,-1,sizeof f3);
	memset(f4,-1,sizeof f4);
	f2[1][1]=0; f2[3][1]=1; f2[3][2]=2; f2[2][2]=3;
	f3[1][1][1]=0; f3[2][1][1]=1; f3[2][3][1]=2; f3[3][3][1]=3; f3[3][3][2]=4; f3[1][3][2]=5; f3[1][2][2]=6; f3[2][2][2]=7;
	f4[1][1][1][1]=0; f4[3][1][1][1]=1; f4[3][2][1][1]=2; f4[2][2][1][1]=3; f4[2][2][3][1]=4; f4[1][2][3][1]=5;
	f4[1][3][3][1]=6; f4[3][3][3][1]=7; f4[3][3][3][2]=8; f4[2][3][3][2]=9; f4[2][1][3][2]=10; f4[1][1][3][2]=11;
	f4[1][1][2][2]=12; f4[3][1][2][2]=13; f4[3][2][2][2]=14; f4[2][2][2][2]=15;
	for(int i=1;i<=n;i++) weizhi[i]=read();
	if(n>4) printf("-1");
	else{
		if(n==1) printf("%d",f1[weizhi[1]]);
		else if(n==2) printf("%d",f2[weizhi[1]][weizhi[2]]);
		else if(n==3) printf("%d",f3[weizhi[1]][weizhi[2]][weizhi[3]]);
		else printf("%d",f4[weizhi[1]][weizhi[2]][weizhi[3]][weizhi[4]]);
	}
	return 0;
}

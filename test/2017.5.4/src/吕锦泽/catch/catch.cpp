#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=12;
const int step[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int map[N][N];
int c_x,c_y,m_x,m_y;
int c_f,m_f;
bool vis[N][N];

void dfs(int x,int y){
	vis[x][y]=true;
	if(x-1>0&&!vis[x-1][y]) dfs(x-1,y);
	if(x+1<=10&&!vis[x+1][y]) dfs(x+1,y);
	if(y-1>0&&!vis[x][y-1]) dfs(x,y-1);
	if(y+1<=10&&!vis[x][y+1]) dfs(x,y+1);
}

int main(){
	freopen("catch.in","r",stdin);
	freopen("catch.out","w",stdout);
	char ch[12];
	for(int i=1;i<=10;++i){
		scanf("%s",ch+1);
		for(int j=1;j<=10;++j){
			if(ch[j]=='.') map[i][j]=0;
			else if(ch[j]=='*') map[i][j]=1;
			else if(ch[j]=='C'){ c_x=i; c_y=j; map[i][j]=0; }
			else if(ch[j]=='M'){ m_x=i; m_y=j; map[i][j]=0; }
		}
	}
	dfs(c_x,c_y);
	if(!vis[m_x][m_y]){ printf("-1\n"); return 0; }
	int t=0;
	while(t<=10000000){
		if(c_x==m_x&&c_y==m_y){ printf("%d",t); return 0; }
		int tx=c_x+step[c_f][0],ty=c_y+step[c_f][1];
		if(!map[tx][ty]&&tx>0&&tx<=10&&ty>0&&ty<=10){ c_x=tx; c_y=ty; }
		else{ ++c_f; if(c_f==4) c_f=0; }
		tx=m_x+step[m_f][0]; ty=m_y+step[m_f][1];
		if(!map[tx][ty]&&tx>0&&tx<=10&&ty>0&&ty<=10){ m_x=tx; m_y=ty; }
		else{ ++m_f; if(m_f==4) m_f=0; }
		++t;
	}
	printf("-1\n");
	return 0;
}

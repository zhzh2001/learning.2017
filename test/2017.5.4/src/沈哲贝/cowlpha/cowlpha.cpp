#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define mod 97654321
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll next[100000],head[100],vet[100000],mp[100][100],f[2][260][55],A,B,P,n,tot,now,cur,ans;
char s[10];
ll get(char ch){	return ch<'a'?ch-'A'+1:ch-'a'+27;	}
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void work(ll X,ll Y){
	ll st=X==A?27:1,ed=Y==B?26:52;
	For(tmp,st,ed)	for(ll i=head[tmp];i;i=next[i])	f[now][X+(tmp<27)][tmp]=(f[now][X+(tmp<27)][tmp]+f[cur][X][vet[i]])%mod;
}
int main(){
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	A=read();	B=read();	P=read();	n=A+B;
	For(i,1,P)	scanf("%s",s),mp[get(s[0])][get(s[1])]=1;
	For(i,1,52)	For(j,1,52)	if (mp[i][j])	insert(j,i);
	For(i,1,52)	f[0][i<27][i]=1;
	For(i,1,n-1){
		now^=1;
		memset(f[now],0,sizeof f[now]);
		FOr(j,min(A,i),0)	if (i-j<=B)	work(j,i-j);
		cur^=1;
	}
	For(i,1,52)	ans=(ans+f[cur][A][i])%mod;
	writeln(ans);
}

#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll int
#define maxn 110
#define inf 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define pa pair<ll,ll>
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll a[40],mark[40][40],A,B,k,bas;
ll dfs(ll dep,ll k,bool flag){
	if (k<0)	return 0;
	if (!dep)	return flag?k<=a[0]:k<2;
	if (!flag&&mark[dep][k]!=-1)	return 	mark[dep][k];
	ll res=0,last=flag?a[dep]:1;
	For(i,0,last)	res+=dfs(dep-1,k-i,flag&&i==a[dep]);
	if (!flag)	mark[dep][k]=res;
	return res;
}
ll calc(ll x){
	if (!x)	return 0; ll sum=0;
	while(x)	a[sum++]=x%bas,x/=bas;
	--sum;
	FOr(i,sum,0){
		if (a[i]>1){
			For(j,0,i)	a[j]=1;
			break;
		}
	}
	return dfs(sum,k,1);
}
int main(){
	freopen("maga.in","r",stdin);
	freopen("maga.out","w",stdout); 
	memset(mark,-1,sizeof mark);
	A=read();	B=read();	k=read();	bas=read();
	if (bas==1)	return write(k>=A&&k<=B),0;
	write(calc(B)-calc(A-1));
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 100
#define ll long long 
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll bin[100],mp[100],a[100],p[100],n,ans;
int main(){
	freopen("han.in","r",stdin);
	freopen("han.out","w",stdout);
	bin[1]=1;	For(i,2,50)	bin[i]=bin[i-1]<<1;
	n=read();
	For(i,1,n)	a[i]=read();
	mp[1]=1;	mp[2]=3;	mp[3]=2;	p[1]=1;	p[2]=3;	p[3]=2; 
	FOr(i,n,1){
		a[i]=mp[a[i]];
		if (a[i]==3)	ans+=bin[i],swap(mp[p[1]],mp[p[2]]),swap(p[1],p[2]);
		else if (a[i]==2)	return writeln(-1),0;
		else swap(mp[p[3]],mp[p[2]]),swap(p[3],p[2]);
	}writeln(ans);
}

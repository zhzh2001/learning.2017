#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct data{
	ll x,y,to;
}cat,mos;
char s[100];
ll a[]={-1,0,1,0};
ll b[]={0,1,0,-1};
ll mp[20][20];
data make(data w){
	if (w.x+a[w.to]<=0||w.x+a[w.to]>10||w.y+b[w.to]<=0||w.y+b[w.to]>10||mp[w.x+a[w.to]][w.y+b[w.to]])	w.to=(w.to+1)%4;
	else	w.x+=a[w.to],w.y+=b[w.to];
	return w;
}
ll work(){
	For(i,1,1000000){
		mos=make(mos);	cat=make(cat);
		if (mos.x==cat.x&&mos.y==cat.y)	return i;
	}return -1;
}
int main(){
	freopen("catch.in","r",stdin);
	freopen("catch.out","w",stdout);
	For(i,1,10){
		scanf("%s",s+1);
		For(j,1,10){
			if (s[j]=='*')	mp[i][j]=1;
			else if (s[j]=='M')	mos.x=i,mos.y=j;
			else if (s[j]=='C')	cat.x=i,cat.y=j; 
		}
	}
	writeln(work());
}

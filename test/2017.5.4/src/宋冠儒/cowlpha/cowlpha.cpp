#include<cstdio>
#include<cstring>
using namespace std;
#define N 260
#define mod 97654321
#define len 26
int u,l,p,ans;
int a[N],b[N];
int f[N<<1][N][60];
char x,y;
int main()
{
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	scanf("%d%d%d",&u,&l,&p);
	for (int i=1;i<=p;i++){
		scanf("\n%c%c",&x,&y);
		if (x>='a'&&x<='z') a[i]=x-'a'+1;
			else a[i]=x-'A'+len+1;
		if (y>='a'&&y<='z') b[i]=y-'a'+1;
			else b[i]=y-'A'+len+1;
	}
	for (int i=1;i<=len;i++) f[1][0][i]++;
	for (int i=len+1;i<=len*2;i++) f[1][1][i]++;
	for (int i=2;i<=u+l;i++)
		for (int j=0;j<=u;j++)
			for (int k=1;k<=p;k++)
				if (f[i-1][j][a[k]])
					if (b[k]<=len) f[i][j][b[k]]=(f[i][j][b[k]]+f[i-1][j][a[k]])%mod;
						else f[i][j+1][b[k]]=(f[i][j+1][b[k]]+f[i-1][j][a[k]])%mod;
	for (int i=1;i<=len*2;i++)
		ans=(ans+f[u+l][u][i])%mod;
	printf("%d\n",ans);
}


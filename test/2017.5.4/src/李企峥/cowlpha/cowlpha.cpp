#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int u,l,n,x,y,a[100][100],ans;
bool f[100];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline int get()
{
	char ch=getchar();
	while(!((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z')))ch=getchar();
	if(ch>='a'&&ch<='z')return ch-'a'+1;
	if(ch>='A'&&ch<='Z')return ch-'A'+27;
}
inline void dfs(int x,int u,int l)
{
	if(u==0&&l==0){ans++,ans=ans%97654321;return;}
	For(i,1,a[x][0])
	{
		if(a[x][i]>26&&l>0)dfs(a[x][i],u,l-1);
		if(a[x][i]<=26&&u>0) dfs(a[x][i],u-1,l);
	}
}
int main()
{
	freopen("cowlpha.in","r",stdin);
	freopen("cowlpha.out","w",stdout);
	u=read();l=read();
	n=read();
	For(i,1,n)
	{
		x=get();
		y=get();
		a[x][0]++;
		a[x][a[x][0]]=y;
		f[x]=true;
		f[y]=true;
	}
	For(i,1,52)
	{
		if(!f[i])continue;
		dfs(i,u,l);
	}
	cout<<ans<<endl;
	return 0;
}


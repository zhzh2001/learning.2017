#include<bits/stdc++.h>
using namespace std;
const int n=250,MOD=97654321;
int f[2*n+5][2*n+5];
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}
	while(b/=2);
	return ans;
}
int main()
{
	f[0][0]=1;
	for(int i=1;i<=2*n;i++)
	{
		f[i][0]=f[i][i]=1;
		for(int j=1;j<i;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j-1])%MOD;
	}
	cout<<(long long)qpow(26,2*n)*f[2*n][n]%MOD<<endl;
	return 0;
}
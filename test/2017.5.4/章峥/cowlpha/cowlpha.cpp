#pragma GCC optimize ("O2")
#include<fstream>
#include<cctype>
using namespace std;
ifstream fin("cowlpha.in");
ofstream fout("cowlpha.out");
const int N=255,ALPHA=52,MOD=97654321;
long long f[N][N][ALPHA];
bool mat[ALPHA][ALPHA];
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	while(k--)
	{
		char u,v;
		fin>>u>>v;
		int cu,cv;
		if(isupper(u))
			cu=u-'A';
		else
			cu=u-'a'+26;
		if(isupper(v))
			cv=v-'A';
		else
			cv=v-'a'+26;
		mat[cv][cu]=true;
	}
	for(int i=0;i<ALPHA;i++)
		if(i<26)
			f[1][0][i]=1;
		else
			f[0][1][i]=1;
	for(int i=0;i<=n;i++)
		for(int j=0;j<=m;j++)
			if(i+j>1)
				for(int k=0;k<ALPHA;k++)
				{
					long long *fpred,&now=f[i][j][k];
					if(k<26)
					{
						if(!i)
							continue;
						fpred=f[i-1][j];
					}
					else
					{
						if(!j)
							continue;
						fpred=f[i][j-1];
					}
					for(int pred=0;pred<ALPHA;pred++)
						if(mat[k][pred])
							now+=fpred[pred];
					now%=MOD;
				}
	int ans=0;
	for(int i=0;i<ALPHA;i++)
		ans=(ans+f[n][m][i])%MOD;
	fout<<ans<<endl;
	return 0;
}
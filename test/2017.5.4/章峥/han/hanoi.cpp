#include<bits/stdc++.h>
using namespace std;
ofstream fout("hanoi.txt");
const int N=35;
int n,a[N],cnt;
void solve(int n,int s,int t,int m)
{
	if(!n)
		return;
	solve(n-1,s,m,t);
	fout<<++cnt<<' ';
	a[n]=t;
	for(int i=1;i<=(::n);i++)
		fout<<a[i]<<' ';
	fout<<endl;
	solve(n-1,m,t,s);
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		a[i]=1;
	solve(n,1,2,3);
	return 0;
}
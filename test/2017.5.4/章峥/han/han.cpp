#include<fstream>
using namespace std;
ifstream fin("han.in");
ofstream fout("han.out");
const int N=35;
int a[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	unsigned l=0,r=(1u<<n)-1;
	int s=1,t=2,m=3;
	for(int i=n;i;i--)
	{
		unsigned mid=(l+r)/2+1;
		if(a[i]==s)
		{
			r=mid-1;
			swap(t,m);
		}
		else
			if(a[i]==t)
			{
				l=mid;
				swap(s,m);
			}
			else
			{
				fout<<-1<<endl;
				return 0;
			}
	}
	fout<<l<<endl;
	return 0;
}
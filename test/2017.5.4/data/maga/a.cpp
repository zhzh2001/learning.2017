#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
using namespace std;

int x,y,k,b,s,i,j,a[35],f[35][35],g[35][35],ans1,ans2,temp;
int work(int num) {
	s=0;
	while (num>0) {
		a[++s]=num%b;
		num/=b;
	}
	for (i=1;i<=s/2;i++) {
		temp=a[i];a[i]=a[s-i+1];a[s-i+1]=temp;
	}
	f[0][0]=1;
	g[0][0]=0;
	for (i=1;i<=s;i++)
		for (j=0;j<=k;j++) {
			if (a[i]==0) {
				f[i][j]=f[i-1][j];
				g[i][j]=0;
			} else if (a[i]==1) {
				f[i][j]=f[i-1][j-1];
				g[i][j]=f[i-1][j];
			} else {
			  	   f[i][j]=0;
			  	   g[i][j]=f[i-1][j];
			  	   if (j>0) g[i][j]+=f[i-1][j-1];
		 }
			g[i][j]+=g[i-1][j];
			if (j>0) g[i][j]+=g[i-1][j-1];
		}
	return f[s][k]+g[s][k];
}
int main() {
	freopen("maga.in","r",stdin);
	freopen("maga.out","w",stdout);
	scanf("%d%d%d%d",&x,&y,&k,&b);
	ans1=work(x-1);
	ans2=work(y);
	printf("%d",ans2-ans1);
	return 0;
}

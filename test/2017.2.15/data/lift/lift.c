
/*
Croatia 2003
Regional Competition - Juniors
Problem LIFTOVI, Programming language C
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define BIG 2000000000
#define MAX_LIFTOVA 50000

int liftovi[MAX_LIFTOVA][2];
int najkr[MAX_LIFTOVA + 1],bio[MAX_LIFTOVA + 1];

int dolaz(int kat,int tren,int *lift)
{
  int dulj;

  dulj=abs(lift[0]-lift[1])*5;
  if(kat==lift[0])
    return ceil(tren/(2.0*dulj))*2*dulj+dulj;
  else
    return ceil((tren-dulj)/(2.0*dulj))*2*dulj+2*dulj;
}

int main(void)
{
  FILE *in,*out;
  int nlift,nkat;
  int i;
  int mint,mink;
  int odr,vris;

  in=fopen("liftovi.in","r");
  fscanf(in,"%d %d",&nkat,&nlift);
  for(i=0;i<nlift;i++)
    fscanf(in,"%d%d",liftovi[i],liftovi[i]+1);
  fclose(in);
  for(i=2;i<=nkat;i++){
    najkr[i]=BIG;bio[i]=0;
  }
  najkr[1]=0;bio[1]=0;

  while(1){
    mint=BIG;
    for(i=1;i<=nkat;i++)
      if(!bio[i] && najkr[i]<mint){
        mint=najkr[i];
        mink=i;
      }
    if(mint==BIG)
      break;
    if(mink==nkat){
      out=fopen("liftovi.out","w");
      fprintf(out,"%d\n",mint);
      fclose(out);
      break;
    }
    bio[mink]=1;
    for(i=0;i<nlift;i++){
      if(liftovi[i][0]!=mink && liftovi[i][1]!=mink)
        continue;
      odr=(liftovi[i][0]==mink)?liftovi[i][1]:liftovi[i][0];
      vris=dolaz(mink,mint,liftovi[i]);
      if(vris<najkr[odr])
        najkr[odr]=vris;
    }
  }

  return 0;
}

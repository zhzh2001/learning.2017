
/*
Croatia 2003
Regional Competition - Juniors
Problem LIFTOVI, Programming language C++
*/

#include <fstream>
#include <vector>
#include <queue>

#define INFILE  "liftovi.in"
#define OUTFILE "liftovi.out"

using namespace std;

ifstream input(INFILE);
ofstream output(OUTFILE);

int k, n;
/* lift[i] ce sadrzavati listu liftova koji staju na katu i*/
vector<vector<int> > lift;
/* naj[i] oznacava najmanje vrijeme potrebno do kata i */
vector<int> naj;

struct pozicija
{
        int vrijeme;
        int kat;
        pozicija (int v, int k)
        {
                vrijeme=v;
                kat=k;
        }
        friend int operator< (const pozicija &a, const pozicija &b)
        {
                if (a.vrijeme>b.vrijeme) return 1;
                return 0;
        }
};

priority_queue<pozicija> red;

int rijesi (void)
{
        naj=vector<int>(k, -1);
        red.push(pozicija(0, 0));
        naj[0]=0;

        while (!red.empty())
        {
                pozicija poz=red.top(); red.pop();
                if (poz.kat==k-1) return poz.vrijeme;
                for (int i=0; i<lift[poz.kat].size(); i++)
                {
                        int cilj=lift[poz.kat][i];
                        int period=abs(cilj-poz.kat);
                        int ciljvrijeme=poz.vrijeme;
                        ciljvrijeme+=(period-poz.vrijeme%period)%period;
                        if (cilj>poz.kat && (ciljvrijeme/period)%2==1)
                           ciljvrijeme+=period;
                        if (cilj<poz.kat && (ciljvrijeme/period)%2==0)
                           ciljvrijeme+=period;
                        ciljvrijeme+=period;
                        if (naj[cilj]==-1 || ciljvrijeme<naj[cilj])
                        {
                                naj[cilj]=ciljvrijeme;
                                red.push(pozicija(ciljvrijeme, cilj));
                        }
                }
        }

        return -1;
}

int main (void)
{
        /* ucitavanje ulaznih podataka */
        input >> k >> n;
        lift=vector<vector<int> >(k);
        for (int i=0; i<n; i++)
        {
                int a, b;
                input >> a >> b;
                a--; b--;
                lift[a].push_back(b);
                lift[b].push_back(a);
        }

        output << rijesi()*5 << endl;

        input.close();
        output.close();
        return 0;
}

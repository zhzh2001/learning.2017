
(*
Croatia 2003
Regional Competition - Juniors
Problem LIFTOVI, Programming language Pascal
*)

program liftovi;

const maxk = 1000;

var spojeni : array[1..maxk, 1..maxk] of boolean;
    dolazak : array[1..maxk] of longint; (* vrijeme dolaska na kat *)
    rjeseno : array[1..maxk] of boolean; (* da li smo obradili kat *)
    k : longint;

procedure ucitaj_podatke;
var i, j, a, b, n : longint;
    f : text;
begin
    assign(f, 'liftovi.in');
    reset(f);
    readln(f, k, n);
    for i:=1 to k do
        for j:=1 to k do spojeni[i, j]:=false;
    for i:=1 to n do
    begin
        readln(f, a, b);
        spojeni[a, b]:=true;
        spojeni[b, a]:=true;
    end;
    close(f);
end;

procedure zapisi_rjesenje(rjesenje : longint);
var f : text;
begin
    assign(f, 'liftovi.out');
    rewrite(f);
    writeln(f, rjesenje);
    close(f);
end;

function cekanje(vrijeme, a, b : longint) : longint;
var c, d : longint;
begin
    d := abs(a-b);
    c := d - vrijeme mod d;
    if (vrijeme mod d = vrijeme mod (2*d)) = (a<b) then c:=c+d;
    if c=2*d then c:=0;
    cekanje := c;
end;

function dijkstra(pocetak, kraj : longint) : longint; (* matrix pfs *)
const beskonacno = 2000000000;
var i, vr, min : longint;
begin
    for i:=1 to k do dolazak[i] := beskonacno;
    for i:=1 to k do rjeseno[i] := false;
    dolazak[pocetak]:=0;
    min:=pocetak;
    while min<>kraj do
    begin
        rjeseno[min] := true;
        for i:=1 to k do if not(rjeseno[i]) then
            if spojeni[min, i] then
            begin
                vr:=dolazak[min]+cekanje(dolazak[min], min, i)+abs(min-i);
                if vr<dolazak[i] then dolazak[i]:=vr;
            end;
        min:=kraj;
        for i:=1 to k do
            if not(rjeseno[i])and(dolazak[i]<dolazak[min]) then min:=i;
    end;
    dijkstra := dolazak[kraj];
end;

begin
    ucitaj_podatke;
    zapisi_rjesenje(dijkstra(1, k)*5);
end.

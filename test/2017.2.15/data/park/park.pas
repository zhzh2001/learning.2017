
(*
Croatia 2003
Regional Competition - Seniors
Problem LUNA, Programming language Pascal
*)

program luna;

const visekratnik : longint = 2*2*2*3*3*5*7*11*13;
      maxm = 1000;

var vozilo : array[1..maxm] of longint;
    broj_vozila : array[1..15] of longint;
    n, m, i, voz, brdjece, minuta, u_periodu, u_minuti : longint;
    f : text;

begin
     assign(f, 'luna.in');
     reset(f);
     readln(f, n, m);
     for i:=1 to m do read(f, vozilo[i]);
     close(f);

     for i:=1 to 15 do broj_vozila[i]:=0;
     u_periodu:=0;
     for i:=1 to m do
     begin
          inc(broj_vozila[vozilo[i]]);
          u_periodu := u_periodu + visekratnik div vozilo[i];
     end;
     n := (n-1) mod u_periodu + 1;

     brdjece:=0;
     minuta:=0;
     while brdjece<n do
     begin
         u_minuti:=broj_vozila[1];
         for i:=2 to 15 do
            if minuta mod i = 0 then u_minuti:=u_minuti+broj_vozila[i];
         voz:=0;
         if brdjece+u_minuti<n then brdjece:=brdjece+u_minuti else
         while brdjece<n do
         begin
             inc(voz);
             if minuta mod vozilo[voz] = 0 then inc(brdjece);
         end;
         inc(minuta);
     end;

     assign(f, 'luna.out');
     rewrite(f);
     writeln(f, voz);
     close(f);
end.


/*
Croatia 2003
Regional Competition - Seniors
Problem LUNA, Programming language C++
*/

#include <fstream>
#include <vector>

#define INFILE  "luna.in"
#define OUTFILE "luna.out"

using namespace std;

ifstream input(INFILE);
ofstream output(OUTFILE);

int n, m;
vector<int> vrijeme;

/* funkcija racuna broj djece koji su poceli svoju voznju do vremena 'kraj'
   ukljucivo */
long long broj (long long kraj)
{
        if (kraj<0) return 0;
        long long b=0;
        for (int i=0; i<m; i++)
            b+=kraj/vrijeme[i]+1;
        return b;
}

/* funkcija racuna u kojem trenutku je dijete d pocelo svoju voznju -
   binarno trazenje */
long long trazi (long long dolje, long long gore, long long d)
{
        long long sredina=(dolje+gore)/2;
        if (broj(sredina)>=d)
        {
                if (broj(sredina-1)<d) return sredina;
                return trazi(dolje, sredina, d);
        }
        return trazi(sredina, gore, d);
}

int main (void)
{
        input >> n;
        input >> m;

        vrijeme=vector<int>(m);
        for (int i=0; i<m; i++)
            input >> vrijeme[i];

        long long t=trazi(0, ((long long) 1)<<35, n);
        int koliko=broj(t-1);

        int rjesenje=-1;
        for (int i=0; i<m && koliko<n; i++)
            if (t%vrijeme[i]==0)
            {
                koliko++;
                if (koliko==n) rjesenje=i;
            }

        output << rjesenje+1 << endl;

        input.close();
        output.close();
        return 0;
}

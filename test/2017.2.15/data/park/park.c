
/*
Croatia 2003
Regional Competition - Seniors
Problem LUNA, Programming language C
*/

#include <stdio.h>

int main(void)
{
  FILE *in, *out;
  int ndj,nvoz;
  int voz[1000];
  int taj[1000],ntaj;
  long long dg,gg,sred;
  long long ndjr;
  int i;

  in=fopen("luna.in","r");
  fscanf(in,"%d%d",&ndj,&nvoz);
  for(i=0;i<nvoz;i++)
    fscanf(in,"%d",voz+i);
  fclose(in);

  dg=0;
  gg=16000000000LL;
  while(dg<gg){
    sred=(dg+gg)/2;
    ndjr=nvoz;
    for(i=0;i<nvoz;i++)
      ndjr+=sred/voz[i];
    if(ndjr>=ndj)
      gg=sred;
    else
      dg=sred+1;
  }
  ntaj=0;
  ndjr=nvoz;
  for(i=0;i<nvoz;i++){
    ndjr+=dg/voz[i];
    if(dg%voz[i]==0)
      taj[ntaj++]=i;
  }

  out=fopen("luna.out","w");
  fprintf(out,"%d\n",taj[ndj-(ndjr-ntaj+1)]+1);
  fclose(out);

  return 0;
}

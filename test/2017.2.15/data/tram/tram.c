
/*
Regional Competition 2002, Juniors
Problem TRAM, Solution in C
*/

#include <stdio.h>

#define INPUT_FILE  "tram.in"
#define OUTPUT_FILE "tram.out"
#define MAX_STANICA 100
#define QUEUE_SIZE  100

int broj_stanica;
int pocetna_stanica,zavrsna_stanica;
int broj_susjeda[MAX_STANICA];
int graf[MAX_STANICA][MAX_STANICA - 1];
int rjesenje;
FILE *file;

struct queue {
               int elements[QUEUE_SIZE + 1][2];
               int front,rear;
             };

void make_null(struct queue *q)
{
  (*q).front = 0;
  (*q).rear = QUEUE_SIZE;
}

int add_one(int number)
{
  if (number == QUEUE_SIZE)
    return 0;
  return (number + 1);
}

int empty(struct queue *q)
{
  if (add_one((*q).rear) == (*q).front)
    return 1;
  return 0;
}

void enqueue(int a,int b,struct queue *q)
{
  (*q).rear = add_one((*q).rear);
  (*q).elements[(*q).rear][0] = a;
  (*q).elements[(*q).rear][1] = b;
}

void front(int *a,int *b,struct queue *q)
{
  *a = (*q).elements[(*q).front][0];
  *b = (*q).elements[(*q).front][1];
}

void dequeue(struct queue *q)
{
  (*q).front = add_one((*q).front);
}

void ucitaj_podatke(void)
{
  int i,j;

  file = fopen(INPUT_FILE,"r");
  fscanf(file,"%d%d%d",&broj_stanica,&pocetna_stanica,&zavrsna_stanica);
  --pocetna_stanica;
  --zavrsna_stanica;
  for (i = 0;i < broj_stanica;++i)
  {
    fscanf(file,"%d",broj_susjeda + i);
    for (j = 0;j < broj_susjeda[i];++j)
    {
      fscanf(file,"%d",&graf[i][j]);
      --graf[i][j];
    }
  }
  fclose(file);
}

void rijesi(void)
{
  int i;
  int stanica,broj_promjena;
  int bila[MAX_STANICA];
  int temp;
  struct queue q;

  make_null(&q);
  for (i = 0;i < broj_stanica;++i)
    bila[i] = 0;
  enqueue(pocetna_stanica,0,&q);
  bila[pocetna_stanica] = 1;

  while (1)
  {
    if (empty(&q))
    {
      rjesenje = -1;
      break;
    }

    front(&stanica,&broj_promjena,&q);
    dequeue(&q);
    if (stanica == zavrsna_stanica)
    {
      rjesenje = broj_promjena;
      break;
    }

    temp = stanica;
    while (1)
    {
      if (!broj_susjeda[temp])
        break;
      temp = graf[temp][0];
      if (bila[temp])
        break;
      enqueue(temp,broj_promjena,&q);
      bila[temp] = 1;
    }

    for (i = 1;i < broj_susjeda[stanica];++i)
    {
      if (bila[graf[stanica][i]])
        continue;
      enqueue(graf[stanica][i],broj_promjena + 1,&q);
      bila[graf[stanica][i]] = 1;
    }
  }
}

void ispisi_rjesenje(void)
{
  file = fopen(OUTPUT_FILE,"w");
  fprintf(file,"%d\n",rjesenje);
  fclose(file);
}

int main(void)
{
  ucitaj_podatke();
  rijesi();
  ispisi_rjesenje();

  return 0;
}

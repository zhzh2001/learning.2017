
(*
Regional Competition 2002, Juniors
Problem TRAM, Solution in Pascal
*)

program tram;
const
  INPUT_FILE  = 'tram.in';
  OUTPUT_FILE = 'tram.out';
  MAX_STANICA = 100;
  QUEUE_SIZE  = 100;
type
  queue = record
            elements : array [0..QUEUE_SIZE,1..2] of integer;
            front,rear : integer;
          end;
var
  broj_stanica : integer;
  pocetna_stanica,zavrsna_stanica : integer;
  broj_susjeda : array [1..MAX_STANICA] of integer;
  graf : array [1..MAX_STANICA,1..MAX_STANICA-1] of integer;
  rjesenje : integer;
  f : text;

procedure make_null(var q : queue);
begin
  q.front := 0;
  q.rear := QUEUE_SIZE;
end;

function add_one(number : integer) : integer;
begin
  if number = QUEUE_SIZE then
    add_one := 0
  else
    add_one := number + 1;
end;

function empty(var q : queue) : boolean;
begin
  if add_one(q.rear) = q.front then
    empty := true
  else
    empty := false;
end;

procedure enqueue(a,b : integer;var q : queue);
begin
  q.rear := add_one(q.rear);
  q.elements[q.rear,1] := a;
  q.elements[q.rear,2] := b;
end;

procedure front(var a,b : integer;var q : queue);
begin
  a := q.elements[q.front,1];
  b := q.elements[q.front,2];
end;

procedure dequeue(var q : queue);
begin
  q.front := add_one(q.front);
end;

procedure ucitaj_podatke;
var
  i,j : integer;
begin
  assign(f,INPUT_FILE);
  reset(f);
  readln(f,broj_stanica,pocetna_stanica,zavrsna_stanica);
  for i := 1 to broj_stanica do
    begin
      read(f,broj_susjeda[i]);
      for j := 1 to broj_susjeda[i] do
        read(f,graf[i][j]);
    end;
  close(f);
end;

procedure rijesi;
var
  i : integer;
  stanica,broj_promjena : integer;
  bila : array [1..MAX_STANICA] of boolean;
  temp : integer;
  q : queue;
begin
  make_null(q);
  for i := 1 to broj_stanica do
    bila[i] := false;
  enqueue(pocetna_stanica,0,q);
  bila[pocetna_stanica] := true;

  while true do
    begin
      if empty(q) then
        begin
          rjesenje := -1;
          break;
        end;

      front(stanica,broj_promjena,q);
      dequeue(q);
      if stanica = zavrsna_stanica then
        begin
          rjesenje := broj_promjena;
          break;
        end;

      temp := stanica;
      while true do
        begin
          if broj_susjeda[temp] = 0 then
            break;
          temp := graf[temp,1];
          if bila[temp] then
            break;
          enqueue(temp,broj_promjena,q);
          bila[temp] := true;
        end;

      for i := 2 to broj_susjeda[stanica] do
        begin
          if bila[graf[stanica,i]] then
            continue;
          enqueue(graf[stanica,i],broj_promjena + 1,q);
          bila[graf[stanica,i]] := true;
        end;
    end;
end;

procedure ispisi_rjesenje;
begin
  assign(f,OUTPUT_FILE);
  rewrite(f);
  writeln(f,rjesenje);
  close(f);
end;

begin
  ucitaj_podatke;
  rijesi;
  ispisi_rjesenje;
end.

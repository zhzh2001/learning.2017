
/*
Croatia 2003
Regional Competition - Seniors
Problem BRODOVI, Programming language C++
*/

#include <stdio.h>
#include <algorithm>

#define INFILE                  "brodovi.in"
#define OUTFILE                 "brodovi.out"
#define MAXBRODOVA              1000000
#define MAXDIJELOVA             1000000

#define MAX(a, b)               ((a)>(b)?(a):(b))
#define MIN(a, b)               ((a)<(b)?(a):(b))

int broj_dijelova_rijeke, broj_brodova;
int riba[MAXDIJELOVA+1], max_ulov[MAXDIJELOVA+1];

struct cbrod
{
        int bova, duljina;
        friend bool operator <(const cbrod& a, const cbrod &b)
        {
                return a.bova < b.bova;
        }
} brod[MAXBRODOVA+1];


void load_data ( void )
{
        FILE *f=fopen (INFILE, "rt");
        int i;

        fscanf (f, "%d", &broj_dijelova_rijeke);
        for (i=1; i<=broj_dijelova_rijeke; i++)
                fscanf (f, "%d", &riba[i]);

        fscanf (f, "%d", &broj_brodova);
        brod[0].bova=0; brod[0].duljina=0; // dummy
        for (i=1; i<=broj_brodova; i++)
                fscanf (f, "%d%d", &brod[i].bova, &brod[i].duljina);

        fclose (f);
}

int solve ( void )
{
        int i, j, moj_max, start, ulov_pod_brodom, pocetak_broda;
        int old_moj_max=0, old_start=1;

        // posortiraj po bovama
        std::sort (brod, brod+broj_brodova+1);

        for (i=1; i<=broj_brodova; i++)
        {
                // izracunaj di moze pocet novi brod -- mozda mu smeta prethodni
                start=MAX (old_start+brod[i-1].duljina, brod[i].bova-brod[i].duljina+1);
                moj_max=-1; // koliko se sa ovom brodom moze najvise ulovit ribe

                ulov_pod_brodom=0;
                for (j=start; j<start+brod[i].duljina-1; j++) ulov_pod_brodom+=riba[j];

                for (pocetak_broda=start; pocetak_broda<=MIN (brod[i].bova, broj_dijelova_rijeke-brod[i].duljina+1); pocetak_broda++)
                {
                        // dodaj polje pod krajem broda u ulov ribe
                        ulov_pod_brodom+=riba[pocetak_broda+brod[i].duljina-1];

                        // provjeri jel sijeces mjesto gdje moze biti postavljen prethodni brod
                        if (pocetak_broda < brod[i-1].bova+brod[i-1].duljina)
                        {
                                // da -- pogledaj koji je bio maximum za preth. brod do tog mjesta
                                // i zapisi za novi
                                // ovo se nece sijeci sa vec zapisanim :)
                                moj_max=MAX (moj_max, max_ulov[pocetak_broda-brod[i-1].duljina]+ulov_pod_brodom);
                                max_ulov[pocetak_broda]=moj_max;
                        }
                        else
                        {
                                // ne sijecem -- samo pribroji totalni max za preth. brod
                                moj_max=MAX (moj_max, old_moj_max+ulov_pod_brodom);
                                max_ulov[pocetak_broda]=moj_max;
                        }

                        // makni polje pod pocetkom broda iz ulova ribe
                        ulov_pod_brodom-=riba[pocetak_broda];
                }

                old_start=start; old_moj_max=moj_max;
        }

        return moj_max;
}

void write_solution (int sol)
{
        FILE *f=fopen (OUTFILE, "wt");
        fprintf (f, "%d\n", sol);
        fclose (f);
}

int main ( void )
{
        load_data();
        write_solution (solve());

        return 0;
}


(*
Croatia 2003
Regional Competition - Seniors
Problem BRODOVI, Programming language Pascal
*)

program brodovi;

const maxn = 100000;
      maxm = 100000;

type tbrod = record b, d : longint; end;

var brod   : array[0..maxm+1] of tbrod;
    rijeka : array[1..maxn] of longint;
    zbroj  : array[0..maxn] of longint;
    ulov   : array[0..maxn] of longint;
    novi   : array[0..maxn] of longint;
    n, m, i, j, kraj : longint;

function max(a, b : longint) : longint;
begin
    if a>=b then max:=a else max:=b;
end;

function min(a, b : longint) : longint;
begin
    if a<=b then min:=a else min:=b;
end;

procedure ucitaj_podatke;
var i : longint;
    f : text;
begin
    assign(f, 'brodovi.in');
    reset(f);
    readln(f, n);
    for i:=1 to n do read(f, rijeka[i]);
    readln(f);
    readln(f, m);
    for i:=1 to m do readln(f, brod[i].b, brod[i].d);
    close(f);
end;

procedure zapisi_rjesenje(rjesenje : longint);
var f : text;
begin
    assign(f, 'brodovi.out');
    rewrite(f);
    writeln(f, rjesenje);
    close(f);
end;

(*
    quicksort - prilagodjena verzija procedure iz datoteke
    \source\demo\text\sqort.pp
*)
procedure quicksort(l, r : longint);
var i, j : longint;
    x, y : tbrod;
begin
    i:=l; j:=r;
    x:=brod[(l+r) div 2];
    repeat
        while brod[i].b<x.b do inc(i);
        while x.b<brod[j].b do dec(j);
        if i<=j then
        begin
            y:=brod[i]; brod[i]:=brod[j]; brod[j]:=y;
            inc(i); dec(j);
        end;
    until i>j;
    if l<j then quicksort(l,j);
    if i<r then quicksort(i,r);
end;

begin
    ucitaj_podatke;

    quicksort(1, m);

    zbroj[0]:=0;
    for i:=1 to n do zbroj[i]:=zbroj[i-1]+rijeka[i];

    brod[0].b:=0;
    brod[0].d:=1;
    brod[m+1].b:=n+1;
    brod[m+1].d:=1;

    for i:=0 to n do ulov[i]:=0;

    for i:=1 to m do
    begin
        novi[max(brod[i].b, brod[i-1].b+brod[i].d)-1]:=0;
        (* trenutni brod ne sadrzi bovu prethodnog *)
        for j:=max(brod[i].b, brod[i-1].b+brod[i].d) to brod[i+1].b-1 do
        begin
            kraj:=min(brod[i].b+brod[i].d-1, j); (* desni rub broda *)
            novi[j]:=zbroj[kraj]-zbroj[kraj-brod[i].d]+ulov[kraj-brod[i].d];
            if novi[j]<novi[j-1] then novi[j]:=novi[j-1];
        end;
        for j:=max(brod[i].b, brod[i-1].b+brod[i].d) to brod[i+1].b-1 do
            ulov[j]:=novi[j];
    end;

    zapisi_rjesenje(ulov[n]);
end.

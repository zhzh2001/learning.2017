
/*
Croatia 2003
Regional Competition - Seniors
Problem BRODOVI, Programming language C
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct{
  int dulj,vez;
} brod;

int usp(const void *a, const void *b)
{
  return ((brod *)a)->vez-((brod *)b)->vez;
}

int main(void)
{
  int *maxpost;
  int *ribe;
  brod *b;
  int nb,dulj;
  int i;
  int brod;
  int trenribe;
  FILE *in,*out;
  int poc,poz,kraj;

  maxpost=malloc(1100000*sizeof(int));
  ribe=malloc(1100000*sizeof(int));
  b=malloc(1100000*sizeof(*b));
  in=fopen("brodovi.in","r");
  fscanf(in,"%d",&dulj);
  maxpost[0]=0;
  for(i=1;i<=dulj;i++){
    fscanf(in,"%d",ribe+i);
    maxpost[i]=0;
  }
  fscanf(in,"%d",&nb);
  for(i=0;i<nb;i++)
    fscanf(in,"%d%d",&b[i].vez,&b[i].dulj); /** Ucitavanje B(vez) i D(dulj) **/
  fclose(in);

  qsort(b,nb,sizeof(*b),usp);

  for(brod=0;brod<nb;brod++){
    poc=b[brod].vez-b[brod].dulj+1;
    if(poc<1)poc=1;
    if(brod && poc<=b[brod-1].vez)
      poc=b[brod-1].vez+1;

    if(brod==nb-1)
      kraj=dulj;
    else
      kraj=b[brod+1].vez-1;

    for(i=0,trenribe=0;i<b[brod].dulj;i++)
      trenribe+=ribe[i+poc];

    for(poz=0;poz+poc<=b[brod].vez && poz+poc+b[brod].dulj-1<=kraj ; poz++){
      maxpost[poc+poz+b[brod].dulj-1]=trenribe+maxpost[poc+poz-1];
      if(maxpost[poc+poz+b[brod].dulj-1]<maxpost[poc+poz+b[brod].dulj-2])
        maxpost[poc+poz+b[brod].dulj-1]=maxpost[poc+poz+b[brod].dulj-2];
      trenribe-=ribe[poc+poz];
      trenribe+=ribe[poc+poz+b[brod].dulj];
    }
    for(;poz+poc+b[brod].dulj-1<=kraj;poz++)
      maxpost[poc+poz+b[brod].dulj-1]=maxpost[poc+poz+b[brod].dulj-2];
  }

  out=fopen("brodovi.out","w");
  fprintf(out,"%d\n",maxpost[dulj]);
  fclose(out);

  return 0;
}

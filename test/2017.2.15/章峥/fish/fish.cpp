#include<fstream>
using namespace std;
ifstream fin("fish.in");
ofstream fout("fish.out");
const int N=100005;
int a[N],b[N],d[N],r[N],l[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		a[i]+=a[i-1];
	}
	int m;
	fin>>m;
	for(int i=1;i<=m;i++)
		fin>>b[i]>>d[i];
	r[m]=min(b[m],n-d[m]+1);
	for(int i=m-1;i;i--)
		r[i]=min(r[i+1]-d[i],b[i]);
	l[1]=max(1,b[1]-d[1]+1);
	for(int i=2;i<=m;i++)
		l[i]=max(l[i-1]+d[i-1],b[i]);
	int ans=0;
	for(int i=1;i<=m;i++)
	{
		int now=0;
		for(int j=l[i];j<=r[i];j++)
			now=max(now,a[j+d[i]-1]-a[j-1]);
		ans+=now;
	}
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<queue>
#include<algorithm>
#include<cstring>
#include<ctime>
using namespace std;
ifstream fin("lift.in");
ofstream fout("lift.out");
const int N=50005,K=1005;
struct lift
{
	int l,r,len;
}a[N];
struct node
{
	int f,t;
	bool operator<(const node& b)const
	{
		return t<b.t;
	}
}b[N];
queue<node> Q;
bool vis[K];
int main()
{
	int k,n;
	fin>>k>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].l>>a[i].r;
		a[i].len=a[i].r-a[i].l;
	}
	memset(vis,false,sizeof(vis));
	vis[1]=true;
	int cc=0;
	for(int i=1;i<=n;i++)
		if(a[i].l==1)
		{
			b[++cc].f=a[i].r;
			b[cc].t=a[i].len;
		}
	sort(b+1,b+cc+1);
	for(int i=1;i<=cc;i++)
		Q.push(b[i]);
	while(!Q.empty())
	{
		node t=Q.front();Q.pop();
		if(t.f==k)
		{
			fout<<t.t*5<<endl;
			fout<<clock()<<endl;
			return 0;
		}
		vis[t.f]=true;
		int cc=0;
		for(int i=1;i<=n;i++)
			if(a[i].l==t.f&&!vis[a[i].r])
			{
				int wait=2*a[i].len-(t.t%(2*a[i].len));
				if(wait==2*a[i].len)
					wait=0;
				b[++cc].f=a[i].r;
				b[cc].t=t.t+wait+a[i].len;
			}
			else
				if(a[i].r==t.f&&!vis[a[i].l])
				{
					int wait=2*a[i].len-((t.t+a[i].len)%(2*a[i].len));
					if(wait==2*a[i].len)
						wait=0;
					b[++cc].f=a[i].l;
					b[cc].t=t.t+wait+a[i].len;
				}
		sort(b+1,b+cc+1);
		for(int i=1;i<=cc;i++)
			Q.push(b[i]);
	}
	fout<<-1<<endl;
	return 0;
}
#include<fstream>
#include<queue>
#include<vector>
#include<cstring>
using namespace std;
ifstream fin("lift.in");
ofstream fout("lift.out");
const int N=50005,K=1005;
struct lift
{
	int l,r,len;
}a[N];
struct node
{
	int f,t;
	node(int f,int t):f(f),t(t){};
	bool operator>(const node& b)const
	{
		return t>b.t;
	}
};
priority_queue<node,vector<node>,greater<node> > Q;
bool vis[K];
int main()
{
	int k,n;
	fin>>k>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].l>>a[i].r;
		a[i].len=a[i].r-a[i].l;
	}
	memset(vis,false,sizeof(vis));
	vis[1]=true;
	for(int i=1;i<=n;i++)
		if(a[i].l==1)
			Q.push(node(a[i].r,a[i].len));
	while(!Q.empty())
	{
		node t=Q.top();Q.pop();
		if(t.f==k)
		{
			fout<<t.t*5<<endl;
			return 0;
		}
		vis[t.f]=true;
		for(int i=1;i<=n;i++)
			if(a[i].l==t.f&&!vis[a[i].r])
				Q.push(node(a[i].r,t.t+2*a[i].len-(t.t%(2*a[i].len))+a[i].len));
			else
				if(a[i].r==t.f&&!vis[a[i].l])
				{
					int wait=t.t%(2*a[i].len);
					if(wait<=a[i].len)
						wait=a[i].len-wait;
					else
						wait=a[i].len-(wait-a[i].len)+a[i].len;
					Q.push(node(a[i].l,t.t+wait+a[i].len));
				}
	}
	fout<<-1<<endl;
	return 0;
}
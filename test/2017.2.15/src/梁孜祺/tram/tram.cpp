#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<cstdlib>
#include<cmath>
#include<algorithm>
using namespace std;
int x,y,n;
int len[1000][1000];

int main(){
	freopen("tram.in","r",stdin);
	freopen("tram.out","w",stdout);
	scanf("%d%d%d",&n,&x,&y);
	if(x==y){
		printf("0");
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) len[i][j]=1e9; 
	for(int i=1;i<=n;i++){
		int m;
		scanf("%d",&m);
		for(int j=1;j<=m;j++){
			int p;
			scanf("%d",&p);
			len[i][p]=2;
		}
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++){
			if(k==i) continue;
			for(int j=1;j<=n;j++){
				if(k==j||i==j) continue;
				len[i][j]=min(len[i][j],len[i][k]+len[k][j]-1);
			}
		}
	if(len[x][y]==1e9) printf("-1");
	else printf("%d",len[x][y]-2);
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
using namespace std;
int a[100100],f[100100];
struct data{
	int len,x;
}b[100100];

inline bool cmp(data x,data y){
	return x.x<y.x;
}

int main(){
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	int n,m;
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]),a[i]+=a[i-1];
	scanf("%d",&m);
	for(int i=1;i<=m;i++) scanf("%d%d",&b[i].x,&b[i].len);
	sort(b+1,b+1+m,cmp);
	for(int i=b[1].x;i<b[1].x+b[1].len;i++)
		if(i-b[1].len+1>0) f[i]=max(f[i],a[i]-a[i-b[1].len]);
	for(int i=2;i<=m;i++)
		for(int j=b[i].x;j<b[i].x+b[i].len;j++){
			if(b[i].x-b[i].len+1<=0) continue;
			for(int k=b[i-1].x;k<b[i-1].x+b[i-1].len&&k<j-b[i].len+1;k++)
				f[j]=max(f[j],f[k]+a[j]-a[max(j-b[i].len,0)]);
		}
	int maxn=0;
	for(int i=1;i<=n;i++) maxn=max(maxn,f[i]);
	printf("%d",maxn);
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#define ll long long
using namespace std;
ll s[1010];
ll l,r;

inline ll gcd(ll x,ll y){
	if(y==0) return x;
	else return gcd(y,x%y);
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	n--;
	long long minn=1e9*1e9;
	for(int i=1;i<=m;i++){
		scanf("%lld",&s[i]);
		/*if(s[i]==0){
			printf("0");
			return 0;
		}*/
		minn=min(minn,s[i]);
	}
	int p=s[1];
	for(int i=2;i<=m;i++) p=gcd(p,s[i]);
	l=1;r=minn*n;
	while(l<r){
		ll mid=(l+r)/2;
		int n0=0;
		for(int i=1;i<=m;i++) 
			if(mid%s[i]!=0) n0+=mid/s[i]+1;
			else n0+=mid/s[i];
		if(n0<n) l=mid+1;
		else if(n0>n) r=mid-1;
		else if(n0==n){
		/*	ll minm=1e9*1e9;
			int y=0;
			for(int i=1;i<=m;i++)
				if(minm>mid%s[i]||mid%s[i]==0){
					y=i;
					minm=mid%s[i];
				}
			cout<<mid<<endl;
			printf("%d",y);
			return 0;*/
			r=mid;
		}
	}
	int y=0,minm=1e9;
	for(int i=1;i<=m;i++)
		if(minm>l%s[i]||l%s[i]==0){
			y=i;
			minm=l%s[i];
		}
//	cout<<l<<endl;
	printf("%d",y);
} 

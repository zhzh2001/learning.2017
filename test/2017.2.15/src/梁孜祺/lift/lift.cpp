#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<cstdlib>
#include<cmath>
#include<algorithm>
using namespace std;
int cnt,K,n,dist[50010];
int len[1000][1000];
bool vis[50010];

/*inline int pd(int u,int x,int y){
	if(x>y){
		if(u/len[x][y]%2==0) return len[x][y]+u+len[x][y]-u%len[x][y];
		else if(u%len[x][y]==0) return len[x][y]+u;
		else return len[x][y]+u+len[x][y]*2-u%len[x][y]; 
	}
	else{
		if(u/len[x][y]%2==1) return len[x][y]+u+len[x][y]-u%len[x][y];
		else if(u%len[x][y]==0) return len[x][y]+u;
		else return len[x][y]+u+len[x][y]*2-u%len[x][y];
	}
}*/
int main(){
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	scanf("%d%d",&K,&n);
	for(int i=1;i<=K;i++)
		for(int j=1;j<=K;j++) len[i][j]=1e9;
	for(int i=1;i<=n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		len[x][y]=5*(y-x);
		len[y][x]=5*(y-x);
	}
	vis[1]=1;
	dist[1]=0;
	for(int i=2;i<=K;i++) dist[i]=len[1][i];
	for(int i=1;i<=K-1;i++){
		int v,minn=1e9;
		for(int j=1;j<=K;j++)
			if(!vis[j]&&dist[j]<minn){
				minn=dist[j];
				v=j;
			}
		vis[v]=1;
		for(int j=1;j<=K;j++)
			if(!vis[j]&&len[v][j]!=1e9){
				int k=0;
				if(v<j){
					if(dist[v]/len[v][j]%2==1) k=len[v][j]-dist[v]%len[v][j];
					else if(dist[v]%len[v][j]!=0) k=len[v][j]*2-dist[v]%len[v][j];
				}
				else{
					if(dist[v]/len[v][j]%2==0) k=len[v][j]-dist[v]%len[v][j];
					else if(dist[v]%len[v][j]!=0) k=len[v][j]*2-dist[v]%len[v][j];
				}
				dist[j]=min(dist[j],len[v][j]+k+dist[v]/*dist[v]+len[v][j]*/);
			}
	}
	printf("%d",dist[K]);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n,m;
int a[1005][1005];
int dij(int s,int t)
{
	bool vis[1005];
	int dis[1005];
	memset(vis,false,sizeof(vis));
	memset(dis,127/3,sizeof(dis));
	dis[s]=0;
	for(int i=1;i<=n;i++){
		int k=0;
		int small=1<<30;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&dis[j]<small){
				small=dis[j];
				k=j;
			}
			vis[k]=true;
			for(int j=1;j<=n;j++)
				if(!vis[j]){
					if(j<k){
						int x=(dis[k]+a[k][j])%(a[k][j]*2);
						if(x!=0)x=a[k][j]*2-x;
						dis[j]=min(dis[j],x+dis[k]+a[k][j]);
					}
					if(j>k){
						int x=(dis[k])%(a[k][j]*2);
						if(x!=0)x=a[k][j]*2-x;
						dis[j]=min(dis[j],x+dis[k]+a[k][j]);
					}
				}
	}
	return dis[t]*5;
}
int main()
{
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	memset(a,127/3,sizeof(a));
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		a[x][y]=y-x;
		a[y][x]=y-x;
	}
	printf("%d",dij(1,n));
	return 0;
}

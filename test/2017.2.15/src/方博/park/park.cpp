#include<bits/stdc++.h>
using namespace std;
long long n,m,k,t;
struct xxx{
	long long i,v,t;
}s[1005];
long long gcd(long long a,long long b)
{
	if(a%b==0)return b;
	else return gcd(b,a%b);
}
bool cmp(xxx a,xxx b)
{
	if(a.v!=b.v)return a.v<b.v;
	else return a.i<b.i;
}
bool com(xxx a,xxx b)
{
	if(a.v!=b.v)return a.v>b.v;
	else return a.i>b.i;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	k=1;
	for(long long i=1;i<=m;i++){
		cin>>s[i].v;
		k=k*s[i].v/gcd(k,s[i].v);
		s[i].i=i;
	}
	sort(s+1,s+m+1,cmp);
	for(long long i=1;i<=m;i++)
		t+=k/s[i].v;
	if(n%t!=0)n=n%t;
	else{
		cout<<s[m].i<<endl;
		return 0;
	}
	if(n<=m){
		cout<<s[n].i<<endl;
		return 0;
	}
	else{
		for(long long i=1;i<=m;i++){
			s[i].t=s[i].v;
			s[i].v=0;
		}
		long long k=0;
		make_heap(s+1,s+m+1,com);
		while(n>1){
			k++;
			pop_heap(s+1,s+m+1,com);
			s[m].v+=s[m].t;
			push_heap(s+1,s+m+1,com);
			n--;
		}
		cout<<s[1].i<<endl;
		return 0;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#define Ll long long
using namespace std;
struct cs{
	int to,next,v;
}a[1000001];
Ll head[1000],d[1000];
bool vi[1000];
int n,S,E,m,x,y,z,ll;
void init(int x,int y,int z){
	ll++;
	a[ll].to=y;
	a[ll].v=z;
	a[ll].next=head[x];
	head[x]=ll;
}
int main()
{
	freopen("tram.in","r",stdin);
	freopen("tram.out","w",stdout);
	scanf("%d%d%d",&n,&S,&E);
	for(int i=1;i<=n;i++){
		scanf("%d",&m);
		if(m)scanf("%d",&x);
		init(i,x,0);
		while(m&&--m){
			scanf("%d",&x);
			init(i,x,1);
		}
	}
	memset(d,63,sizeof d);
	d[S]=0;
	for(int i=1;i<=n;i++){
		int mi=0;
		for(int i=1;i<=n;i++)if(!vi[i]&&d[mi]>d[i])mi=i;
		vi[mi]=1;
		for(int k=head[mi];k;k=a[k].next)
			d[a[k].to]=min(d[a[k].to],d[mi]+a[k].v);
	}
	if(d[E]>=1e8)d[E]=-1;
	printf("%d",d[E]);
}
/*
          |||        |||
     ||||||||||||||||||||||||
         |||        |||   
       --------------------
      |||               |||
            |||||||||   |||
            |||   |||   |||
            |||   |||   |||
            |||||||||   |||
                        |||
                        |||
                    &   |||
                    &^||||
*/



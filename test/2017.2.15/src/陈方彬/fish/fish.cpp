#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#define Ll long long
using namespace std;
struct cs{
	int x,y,ll,rr;
}a[1000001];
int f[1000001],c[1000001],ff[1000001];
int n,m,ans;
int main()
{
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&c[i]),c[i]+=c[i-1];
	scanf("%d",&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&a[i].x,&a[i].y);
		a[i].ll=max(1,a[i].x-a[i].y+1);
		a[i].rr=min(n,a[i].x+a[i].y-1)-a[i].y+1;
	}
	for(int i=a[1].ll;i<=a[1].rr;i++)ff[i]=c[i+a[1].y-1]-c[i-1],ans=max(ans,ff[i]);
	for(int i=1;i<=m;i++){
		for(int j=a[i].ll;j<=a[i].rr;j++){
			for(int k=a[i-1].ll;k<=a[i-1].rr;k++)
				if(k+a[i-1].y-1<j)
					f[j]=max(f[j],ff[k]+c[j+a[i].y-1]-c[j-1]);
				else break;
			ans=max(ans,f[j]);
		}
		for(int j=a[i].ll;j<=a[i].rr;j++)ff[j]=f[j];
	}
	printf("%d",ans);
}
/*
          |||        |||
     ||||||||||||||||||||||||
         |||        |||   
       --------------------
      |||               |||
            |||||||||   |||
            |||   |||   |||
            |||   |||   |||
            |||||||||   |||
                        |||
                        |||
                    &   |||
                    &^||||
*/



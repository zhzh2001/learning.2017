#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#define Ll long long
using namespace std;
Ll a[10001],last[10001];
Ll n,g,m,l,r,mid,ans;
int gcd(Ll x,Ll y){
	if(y)return gcd(y,x%y);else return x;
}
bool check(Ll k){
	Ll sum=0;
	for(int i=1;i<=n;i++){
		sum+=k/a[i];
		if(sum>m)return 0;
	}
	return 1;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	g=a[1];
	for(int i=2;i<=n;i++)g=gcd(g,a[i]);
	for(int i=1;i<=n;i++)a[i]/=g;
	if(m<=n){
		printf("%d",m);
		return 0;
	}
	m-=n;
	l=0;r=1e18;
	while(r>=l){
		mid=(l+r)/2;
		if(check(mid)){
			ans=max(ans,mid);
			l=mid+1;
		}else r=mid-1;
	}
	ans--;
	for(int i=1;i<=n;i++){
		m-=ans/a[i];
		last[i]=(ans/a[i]+1)*a[i]-ans;
	}	
	while(1){
		for(int i=1;i<=n;i++){
			last[i]--;
			if(!last[i]){
				m--;
				last[i]=a[i];
			}
			if(!m){
				printf("%d",i);
				return 0;
			}
		}
	}
}
/*
          |||        |||
     ||||||||||||||||||||||||
         |||        |||   
       --------------------
      |||               |||
            |||||||||   |||
            |||   |||   |||
            |||   |||   |||
            |||||||||   |||
                        |||
                        |||
                    &   |||
                    &^||||
*/



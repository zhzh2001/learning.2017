#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("fish.in");
ofstream fout("fish.out");
const int N=100005;
int a[N],f[N];
struct node
{
	int b,d;
	bool operator<(const node& k)const
	{
		return b<k.b;
	}
}l[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		a[i]+=a[i-1];
	}
	int m;
	fin>>m;
	for(int i=1;i<=m;i++)
		fin>>l[i].b>>l[i].d;
	sort(l+1,l+m+1);
	l[m+1].b=n+1;
	for(int i=1;i<=m;i++)
	{
		int s=max(max(l[i-1].b+1,l[i].b-l[i].d+1),1),t=min(n,min(l[i+1].b-l[i].d,l[i].b));
		for(int j=s;j<=t;j++)
			if(i==1||f[j-1])
				f[j+l[i].d-1]=f[j-1]+a[j+l[i].d-1]-a[j-1];
		for(int j=s+l[i].d-1;j<=l[i+1].b;j++)
			f[j]=max(f[j],f[j-1]);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,f[i]);
	fout<<ans<<endl;
	return 0;
}
program gen;
const
  n=1000000;
  m=1000;
var
  i:longint;
begin
  randomize;
  assign(output,'park.in');
  rewrite(output);
  writeln(n,' ',m);
  for i:=1 to m do
    write(random(1000)+1,' ');
  writeln;
  close(output);
end.
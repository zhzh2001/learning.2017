program gen;
const
  n=50000;
  k=1000;
var
  i,l,r:longint;
begin
  randomize;
  assign(output,'lift.in');
  rewrite(output);
  writeln(k,' ',n);
  for i:=1 to n do
  begin
    r:=random(k-1)+2;
	l:=random(r-1)+1;
	writeln(l,' ',r);
  end;
  close(output);
end.
#include<fstream>
#include<ext/pb_ds/priority_queue.hpp>
#include<cstring>
using namespace std;
ifstream fin("lift.in");
ofstream fout("lift.out");
const int N=50005,K=1005;
struct lift
{
	int l,r,len;
}a[N];
struct node
{
	int f,t;
	node(int f,int t):f(f),t(t){};
	bool operator>(const node& b)const
	{
		return t>b.t;
	}
};
__gnu_pbds::priority_queue<node,greater<node> > Q;
bool vis[K];
int main()
{
	int k,n;
	fin>>k>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].l>>a[i].r;
		a[i].len=a[i].r-a[i].l;
	}
	memset(vis,false,sizeof(vis));
	vis[1]=true;
	for(int i=1;i<=n;i++)
		if(a[i].l==1)
		{
			Q.push(node(a[i].r,a[i].len));
			vis[a[i].r]=true;
		}
	while(!Q.empty())
	{
		node t=Q.top();Q.pop();
		if(t.f==k)
		{
			fout<<t.t*5<<endl;
			return 0;
		}
		for(int i=1;i<=n;i++)
			if(a[i].l==t.f&&!vis[a[i].r])
			{
				int wait=2*a[i].len-(t.t%(2*a[i].len));
				if(wait==2*a[i].len)
					wait=0;
				Q.push(node(a[i].r,t.t+wait+a[i].len));
				vis[a[i].r]=true;
			}
			else
				if(a[i].r==t.f&&!vis[a[i].l])
				{
					int wait=2*a[i].len-((t.t+a[i].len)%(2*a[i].len));
					if(wait==2*a[i].len)
						wait=0;
					Q.push(node(a[i].l,t.t+wait+a[i].len));
					vis[a[i].l]=true;
				}
	}
	fout<<-1<<endl;
	return 0;
}
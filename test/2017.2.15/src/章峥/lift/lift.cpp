#include<fstream>
#include<queue>
#include<vector>
#include<cstring>
using namespace std;
ifstream fin("lift.in");
ofstream fout("lift.out");
const int N=50005,K=1005;
struct edge
{
	int v,len;
	edge(int v,int len):v(v),len(len){};
};
vector<edge> mat[K];
struct node
{
	int f,t;
	node(int f,int t):f(f),t(t){};
	bool operator>(const node& b)const
	{
		return t>b.t;
	}
};
priority_queue<node,vector<node>,greater<node> > Q;
bool vis[K];
int d[K];
int main()
{
	int k,n;
	fin>>k>>n;
	for(int i=1;i<=n;i++)
	{
		int l,r;
		fin>>l>>r;
		mat[l].push_back(edge(r,r-l));
		mat[r].push_back(edge(l,r-l));
	}
	memset(vis,false,sizeof(vis));
	vis[1]=true;
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	for(int i=0;i<mat[1].size();i++)
	{
		edge e=mat[1][i];
		Q.push(node(e.v,e.len));
		d[e.v]=min(d[e.v],e.len);
	}
	while(!Q.empty())
	{
		node t=Q.top();Q.pop();
		if(d[t.f]<t.t)
			continue;
		for(int i=0;i<mat[t.f].size();i++)
		{
			edge e=mat[t.f][i];
			if(e.v>t.f)
			{
				int wait=2*e.len-(t.t%(2*e.len));
				if(wait==2*e.len)
					wait=0;
				if(t.t+wait+e.len<d[e.v])
				{
					Q.push(node(e.v,t.t+wait+e.len));
					d[e.v]=t.t+wait+e.len;
				}
			}
			else
			{
				int wait=2*e.len-((t.t+e.len)%(2*e.len));
				if(wait==2*e.len)
					wait=0;
				if(t.t+wait+e.len<d[e.v])
				{
					Q.push(node(e.v,t.t+wait+e.len));
					d[e.v]=t.t+wait+e.len;
				}
			}
		}
	}
	fout<<d[k]*5<<endl;
	return 0;
}
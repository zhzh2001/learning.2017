#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("tram.in");
ofstream fout("tram.out");
const int N=105,INFM=0x3f,INF=0x3f3f3f3f;
int n,mat[N][N];
struct dijkstra
{
	int d[N];
	bool vis[N];
	void work(int s)
	{
		memset(d,INFM,sizeof(d));
		d[s]=0;
		memset(vis,false,sizeof(vis));
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			if(!j)
				return;
			vis[j]=true;
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],d[j]+mat[j][k]);
		}
	}
}solver;
int main()
{
	int s,t;
	fin>>n>>s>>t;
	memset(mat,INFM,sizeof(mat));
	for(int i=1;i<=n;i++)
	{
		int k;
		fin>>k;
		for(int j=1;j<=k;j++)
		{
			int x;
			fin>>x;
			mat[i][x]=(j==1?0:1);
		}
	}
	solver.work(s);
	if(solver.d[t]==INF)
		fout<<-1<<endl;
	else
		fout<<solver.d[t]<<endl;
	return 0;
}
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
struct data{
	int l,r,v;
}x[400010];
struct node{
	int b,d;
}ship[100010];
int n,m;
int a[100010];
int ans;
int Max=0;
void Build_1(int l,int r,int num){
	x[num].l=l;
	x[num].r=r;
	if(l==r){
		x[num].v=a[l];
		return;
	}
	int mid=(l+r)/2;
	Build_1(l,mid,num*2);
	Build_1(mid+1,r,num*2+1);
	x[num].v=x[num*2].v+x[num*2+1].v;
}
bool cmp(const node &a,const node &b){
	return a.b<b.b;
}
int query(int l,int r,int num){
	if(l==x[num].l&&r==x[num].r)
		return x[num].v;
	int mid=(x[num].l+x[num].r)/2;
	if(l>mid)
		return query(l,r,num*2+1);
	else if(r<=mid)
		return query(l,r,num*2);
	else
		return query(l,mid,num*2)+query(mid+1,r,num*2+1);
}
void dfs(int k,int lr){
	if(k==m+1){
		if(ans>Max)
			Max=ans;
		return;
	}
	int num;
	for(int i=max(ship[k].b-ship[k].d+1,lr+1);i+ship[k].d-1<=min(ship[k].b+ship[k].d-1,ship[k+1].b-1);i++){
		num=query(i,i+ship[k].d-1,1);
		ans+=num;
		dfs(k+1,i+ship[k].d-1);
		ans-=num;
	}
}
int main(){
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	Build_1(1,n,1);
	m=read();
	for(int i=1;i<=m;i++){
		ship[i].b=read();ship[i].d=read();
	}
	sort(ship+1,ship+m+1,cmp);
	ship[m+1].b=n+1;
	dfs(1,0);
	printf("%d",Max);
	return 0;
}

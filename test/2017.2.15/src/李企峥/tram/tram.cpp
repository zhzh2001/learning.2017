#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<set>
#include<ctime>
#include<queue>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
ll n,m,x,y,a,b;
int f[200][200];
int main()
{
	freopen("tram.in","r",stdin);
	freopen("tram.out","w",stdout);
	cin>>n>>a>>b;
	For(i,1,n)
	    For(j,1,n)
	       f[i][j]=1000;
    For(i,1,n)
    {
        cin>>x;
        if (x==0) continue;
        For(j,1,x)
        {
        	cin>>y;
        	if (j==1) f[i][y]=0; else f[i][y]=1;
        }
    }
    For(k,1,n)
    {
    	For(i,1,n)
    	{
    		if (i==k) continue;
    		For(j,1,n)
    		{
    			if (i==j||j==k) continue;
    			if (f[i][k]+f[k][j]<f[i][j]) f[i][j]=f[i][k]+f[k][j]; 
    		}
    	}
    }
    if (f[a][b]>n) cout<<-1; else cout<<f[a][b];
	return 0;
}

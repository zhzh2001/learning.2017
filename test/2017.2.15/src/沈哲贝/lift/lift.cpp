#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 400010
#define mod 200000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
bool vis[maxn];
ll head[maxn],next[maxn],q[maxn],vet[maxn],l[maxn],r[maxn],tot,dis[maxn],k,n,h,t;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
ll ask(ll id,ll dis,ll now){
	ll t=(r[id]-l[id])<<1;
	if (now==l[id])	return (t-dis%(t))%t+(t>>1);
	else	return (t-(dis+t>>1)%t)%t+(t>>1);
}
void zhuan(ll x,ll v,ll w){
	if (dis[v]>dis[x]+w){
		dis[v]=dis[x]+w;
		if (!vis[v]){
			vis[v]=1;
			t=t%mod+1;
			q[t]=v;
		}
	}
}
void spfa(){
	h=0,t=1;
	q[t]=1;
	memset(dis,127/3,sizeof dis);
	dis[1]=0;	vis[1]=1;
	while (h!=t){
		h=h%mod+1;
		ll x=q[h];
		vis[x]=0;
		for(ll i=head[x];i;i=next[i]){
			ll num=vet[i],w=ask(num,dis[x],x);
			if (x==l[num])	zhuan(l[num],r[num],w);
			else	zhuan(r[num],l[num],w);
		}
	}
}
int main(){
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	k=read();	n=read();
	For(i,1,n){
		l[i]=read(),r[i]=read();
		if (l[i]!=r[i]){
			insert(l[i],i);	insert(r[i],i);
		}
	}
	spfa();
	writeln(dis[k]*5);
}

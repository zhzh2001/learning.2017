#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define inf 3000000000LL
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define block 500
#define maxn 200010
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
struct data{
	ll p,w;
}ship[maxn];
ll cur,now,n,m,a[maxn],ans,f[2][maxn],sum;
bool flag;
bool cmp(data x,data y){
	return x.p<y.p;
}
int main(){
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read()+a[i-1]+1;
	m=read();
	For(i,1,m)	ship[i].p=read(),sum+=ship[i].w=read();
	sort(ship+1,ship+m+1,cmp);
	ship[m+1].p=n+1;
	For(i,max(1,ship[1].p-ship[1].w+1),min(ship[1].p,n-ship[1].w+1))
	f[cur][i]=a[i+ship[1].w-1]-a[i-1];
	For(i,2,m){
		now=cur^1;
		ll mx=0;
		flag=0;
		For(j,ship[i-1].p+1,min(ship[i].p,n-ship[i].w+1))
		if ((j-ship[i-1].w>0)&&(f[cur][j-ship[i-1].w]||flag)){
			mx=max(mx,f[cur][j-ship[i-1].w]);
			if (ship[i].p-j<ship[i].w)	f[now][j]=mx+a[j+ship[i].w-1]-a[j-1];
			flag=1;
		}
		cur=now;
	}
	For(i,ship[m-1].p+1,n)	ans=max(ans,f[cur][i]);
	writeln(ans-sum);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1200
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,s,t,dis[maxn][maxn];
int main(){
	freopen("tram.in","r",stdin);
	freopen("tram.out","w",stdout);
	n=read();	s=read();	t=read();
	memset(dis,127/3,sizeof dis);
	For(i,1,n)	dis[i][i]=0;
	For(i,1,n){
		ll k=read();
		For(j,1,k){
			ll x=read();
			dis[i][x]=j!=1;
		}
	}
	For(k,1,n)	For(i,1,n)	For(j,1,n)	dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
	if (dis[s][t]>50000000)	writeln(-1);
	else	writeln(dis[s][t]);
}

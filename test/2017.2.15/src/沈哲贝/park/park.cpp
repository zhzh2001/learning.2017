#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define inf (1LL<<56)
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1200
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,m,a[maxn];
bool pd(ll x){
	ll ans=0;
	For(i,1,m)	ans+=x/a[i];
	return ans>=n;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		a[i]=read();
		if (!a[i]){
			writeln(min(n,i));
			return 0;
		}
	}
	if (n<=m){
		writeln(n);
		return 0;
	}
	n-=m;
	ll l=1,r=inf,ans=0;
	while (l<=r){
		ll mid=(l+r)>>1;
		if (pd(mid))	r=mid-1,ans=mid;
		else	l=mid+1;
	}
	For(i,1,m)	n-=(ans-1)/a[i];
	For(i,1,m){
		if (!(ans%a[i]))	n--;
		if (!n){
			writeln(i);
			return 0;
		}
	}
}

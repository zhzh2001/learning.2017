#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
#define ll long long
using namespace std;
struct ppap{
	ll a,b;
}s[100001];
ll a[100001]={0},sum[100001]={0},q[100001]={0};
ll f[100001]={0};
inline bool cmp(ppap a,ppap b){return a.a<b.a;}
int main()
{
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	ll n;scanf("%I64d",&n);
	for(ll i=1;i<=n;i++)scanf("%I64d",&a[i]),sum[i]=sum[i-1]+a[i];
	ll m;scanf("%I64d",&m);
	for(ll i=1;i<=m;i++)scanf("%I64d%I64d",&s[i].a,&s[i].b);
	sort(s+1,s+m+1,cmp);
	for(ll i=1;i<=m;i++)q[s[i].a]=i;
	ll p=0,qq=0;f[0]=0;
	s[0].a=0;s[0].b=0;
	for(ll i=1;i<=n;i++){
		f[i]=f[i-1];
		if(q[i]>0)qq=s[p].a,p++;
		if(i-s[p].b>s[p].a)continue;
		if(p>0&&i-qq>=s[p].b)if(f[i-s[p].b]>0||p==1)f[i]=max(f[i],f[i-s[p].b]+sum[i]-sum[i-s[p].b]);
	}
	printf("%I64d",f[n]);
	return 0;
}

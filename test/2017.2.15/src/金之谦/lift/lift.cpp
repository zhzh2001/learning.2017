#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
#include<queue>
#define ll long long
using namespace std;
const ll oo=0xffffffffff; 
queue<ll>q;
bool vis[100001];
ll n,m,nedge=0,p[100001],c[100001],next[100001],head[100001],dist[100001];
inline void addedge(ll x,ll y,ll z){
	nedge++;
	p[nedge]=y;
	c[nedge]=z;
	next[nedge]=head[x];
	head[x]=nedge;
}
void spfa(ll x){
	for(ll i=1;i<=n;i++)dist[i]=oo;
	memset(vis,false,sizeof vis);
	dist[x]=0;vis[x]=true;q.push(x);
	while(!q.empty()){
		ll now=q.front();
		q.pop();
		ll k=head[now];
		while(k>0){
			ll jzq=abs(p[k]-now);
			ll rp=dist[now]%(jzq*2);
			ll sum=0;
			if(c[k]){
				sum=jzq*2-rp;
				if(rp==0)sum=0;
			}else{
				if(rp<=jzq)sum=jzq-rp;
				else sum=jzq*3-rp;
			}
			sum+=jzq;
			if(dist[p[k]]>dist[now]+sum){
				dist[p[k]]=dist[now]+sum;
				if(!vis[p[k]]){
					q.push(p[k]);
					vis[p[k]]=true;
				}
			}
			k=next[k];	
		}
		vis[now]=false;
	}
}
int main()
{
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	scanf("%I64d%I64d",&n,&m);
	for(ll i=1;i<=m;i++){
		ll x,y;scanf("%I64d%I64d",&x,&y);
		addedge(x,y,1);
		addedge(y,x,0);
	}
	spfa(1);
	printf("%I64d",dist[n]*5);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
using namespace std;
const int oo=1<<30;
bool vis[200001];
queue<int>q;
int n,nedge=0,p[200001],c[200001],next[200001],head[200001],dist[200001];
void addedge(int x,int y,int z){
	nedge++;
	p[nedge]=y;
	c[nedge]=z;
	next[nedge]=head[x];
	head[x]=nedge;
}
void spfa(int x){
	int que=1;
	for(int i=1;i<=n;i++)dist[i]=oo;
	memset(vis,false,sizeof vis);
	dist[x]=0;vis[x]=true;q.push(x);
	while(!q.empty()){
		int now=q.front();
		q.pop();
		int k=head[now];
		while(k>0){
			if(dist[p[k]]>dist[now]+c[k]){
				dist[p[k]]=dist[now]+c[k];
				if(!vis[p[k]]){
					q.push(p[k]);
					vis[p[k]]=true;
				}
			}
			k=next[k];	
		}
		vis[now]=false;
	}
}
int main()
{
	freopen("tram.in","r",stdin);
	freopen("tram.out","w",stdout);
	int s,z;cin>>n>>s>>z;
	for(int i=1;i<=n;i++){
		int x;cin>>x;
		x--;
		if(x<0)continue;
		int k;cin>>k;
		addedge(i,k,0);
		for(int j=1;j<=x;j++){
			cin>>k;
			addedge(i,k,1);
		}
	}
	spfa(s);
	if(dist[z]!=oo)cout<<dist[z];
	else cout<<-1;
	return 0;
}

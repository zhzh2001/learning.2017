#include <cstdio>
#include <string>
#include <cmath>
#include <cstdlib>
using namespace std;
const int nn=100113 ;
int n,m,x,y,a[nn],sum[nn],f[4000][3000] ,b[nn],d[nn];
int main()
{
	freopen("fish.in","r",stdin) ;
	freopen("fish.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&a[ i ]) ;
		f[ i ][ 0 ] = 0;
		sum[ i ] =sum[i-1]+a[ i ] ;
	}
	scanf("%d",&m) ;
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&b[i],&d[i]) ;
	}
	for(int i = 1;i<=m;i++)
	{
		int y = min(b[i]+d[i]-1,n) ;
		for(int j=b[i];j<=y;j++)
		{
			int x=b[i]-d[i];
			for(int k=x;k<=b[i]-1;k++)
			{
				int z=min(n+1,k+d[ i ]) ;
				f[i][j]=max(f[i][j],f[i-1][k]+sum[z]-sum[k]) ;
			}
		}
	}
	printf("%d\n",f[m][n]) ;
	return 0;
}

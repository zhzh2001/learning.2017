#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstdlib>
using namespace std;
const int inf=700000000;
int n,m,x,e,f[113][113],a,b;

int main()
{
	freopen("tram.in","r",stdin) ;
	freopen("tram.out","w",stdout) ;
	scanf("%d%d%d",&n,&a,&b) ;
	for(int i=1;i<=n;i++)
	  for(int j=1;j<=n;j++) f[ i ][ j ]=inf; 
	for(int i=1;i<=n;i++) f[ i ][ i ] = 0;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&e) ;
		if(e==0) continue;
		scanf("%d",&x) ;
		f[ i ][ x ] = 0;
		for(int j=2;j<=e;j++)      //
		{
			scanf("%d",&x) ;
			if(f[ i ][ x ]>1) f[ i ][ x ] = 1;
		}
	}
	
	for(int k=1;k<=n;k++)
	  for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++)
	      f[ i ][ j ]=min(f[i][j],f[ i ][ k ]+f[ k ][ j ]) ;
	if(f[ a ][ b ]>=inf-100)
	{
		printf("-1\n");
	}
	else{
		printf("%d\n",f[ a ][ b ]) ;
	}
	
	return 0;
}


/*
5 1 3
1 2
3 1 4 5
1 2
2 2 3
1 4




*/

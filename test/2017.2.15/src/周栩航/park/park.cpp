#include<iostream>
#include<cstdio>
#include<cmath>
#define ll long long
using namespace std;
ll n,m,a[1001],time;
inline ll check(int t)
{
	ll sum=0;
	for(int i=1;i<=m;i++)
		sum=sum+(ll)t/a[i];
	return sum;
}
inline ll min(ll x,ll y){
	return x<y?x:y;
}
inline ll find (int x)
{
	ll l=0,r=1e12;
	ll ans=1e12;
	while(l<=r)
	{
		int mid=(l+r)/2;
		
		if(check(mid)-n>=0)
		{
			r=mid-1;
			ans=min(ans,mid);
		}
		else l=mid+1;
	}
	return ans;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>n>>m;
	n-=m;
	for(int i=1;i<=m;i++)
	{
		scanf("%d",&a[i]);
	}
	time=find(n);
	int t=n-check(time-1);
	int cnt=0;
	for(int i=1;i<=m;i++)
	{
		if (time%a[i]==0)	{cnt++;}
		if(cnt==t)
		{
			cout<<i<<endl;
			return 0;
		}
	}
}

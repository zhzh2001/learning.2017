#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int poi[100001],next[100001],f[10001],d[10001],cnt;
inline void add(int x,int y)
{
	poi[++cnt]=y;next[cnt]=f[x];f[x]=cnt;
}
bool vis[10001];
int len,n,k;
int main()
{
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	cin>>n>>k;
	for(int i=1;i<=k;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for(int i=1;i<=n;i++)
		d[i]=1e9;
	d[1]=0;
	for(int i=1;i<=n;i++)
	{
		int k=-1,ma=1e9;
		for(int j=1;j<=n;j++)
			if((d[j]<ma||k==-1)&&!vis[j])
			{
				k=j;ma=d[j];
			}
		vis[k]=1;
		for(int j=f[k];j;j=next[j])
		{
			int t=poi[j];
			if(vis[t])	continue;
			int len=0;
			if(k==t)continue;
			if(k>t)
			{
				len=d[k]+d[k]%(2*(k-t))+k-t;	
			}else len=d[k]+2*(t-k)-((d[k]+2*(t-k)-1)%(2*(t-k))+1)+(t-k);
			d[t]=min(d[t],len);
		}
	}
		printf("%d\n",d[n]*5);
}

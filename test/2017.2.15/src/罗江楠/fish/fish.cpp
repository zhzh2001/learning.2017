///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     佛祖保佑      RP++                        //
///////////////////////////////////////////////////////////////////
#include <bits/stdc++.h>
#define N 100050
#define ll long long
using namespace std;
struct node{
	int B, D;
	bool operator < (const node &b)const{
		return B < b.B;
	}
} s[N];
int f[N], n,tf[N], m, maxlast, ans;
int l[N<<2], r[N<<2], a[N<<2];
void build(int id, int ls, int rs){
	l[id] = ls, r[id] = rs;
	if(ls == rs){
		scanf("%d", a+id);
		return;
	}
	int mid = ls + rs >> 1;
	build(id<<1, ls, mid);
	build(id<<1|1, mid+1, rs);
	a[id] = a[id<<1] + a[id<<1|1];
}
int ask(int id, int ls, int rs){
	if(l[id] == ls && r[id] == rs) return a[id];
	int mid = l[id] + r[id] >> 1;
	if(rs <= mid) return ask(id<<1, ls, rs);
	if(ls > mid) return ask(id<<1|1, ls, rs);
	return ask(id<<1, ls, mid) + ask(id<<1|1, mid+1, rs);
}
int main(){
	//这题没ac我就打死szb 
	freopen("fish.in", "r", stdin);
	freopen("fish.out", "w", stdout);
	scanf("%d", &n);
	build(1, 1, n); 
	scanf("%d", &m);
	for(int i = 1; i <= m; i++) scanf("%d%d", &s[i].B, &s[i].D);
	sort(s+1, s+m+1);
	for(int i = 1; i <= m; i++){
		if(i>1)for(int j = s[i-1].B; j < s[i].B-s[i].D; j++) maxlast = max(maxlast, tf[j]);
		for(int j = max(s[i].B - s[i].D, 0); j < min(s[i].B, n-s[i].D+1); j++){
			maxlast = max(maxlast, tf[j]);
			f[j+s[i].D] = maxlast + ask(1, j+1, j+s[i].D);
		}
		memcpy(tf, f, sizeof f);
	}
	for(int i = n; i >= s[m-1].B; i--) ans = max(ans, f[i]);
	printf("%d\n", ans);
	return 0;
}


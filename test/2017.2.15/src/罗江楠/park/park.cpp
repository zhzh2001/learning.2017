///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     ���汣��      RP++                        //
///////////////////////////////////////////////////////////////////
#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll n, m;
struct node{
	int t, num;
	bool operator < (const node &b) const {
		return t < b.t;
	}
}a[1050];
ll get(int x){
	ll ans = 0;
	for(int i = 1; i <= m; i++) ans += x / a[i].t;
	return ans + m;
}
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%I64d%I64d", &n, &m);
	for(int i = 1; i <= m; i++){
		scanf("%d", &a[i].t);
		a[i].num = i;
	}
	sort(a+1, a+m+1);
//	for(int i = 1; i <= m; i++)printf("[num = %d, val = %d]\n", a[i].num, a[i].t);
	if(m >= n) return printf("%d", a[n].num)*0;
	int l = 0, r = (1 << 30), mid;
	while(l != r){
		mid = l + r >> 1;
		ll ans = get(mid);
//		printf("[mid = %d, ans = %d, l = %d, r = %d]\n", mid, ans, l, r);
		if(ans >= n)
			r = mid;
		else
			l = max(mid-1, l+1);
//		printf("Trying: %d, %d, ans = %I64d\n",l, r, ans);
	}
	l = r = mid;r++;
	l = get(l);
	int k;
	for(k = 1; k <= m && l < n; k++)
		if(r % a[k].t == 0) l++;
	printf("%d", a[k-1].num);
	return 0;
}


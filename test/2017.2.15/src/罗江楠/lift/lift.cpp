///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     ���汣��      RP++                        //
///////////////////////////////////////////////////////////////////
#include <bits/stdc++.h>
#define ll long long
using namespace std;
int f[50020], mp[50020][500], n, m, x, y, v[50020];
void dfs(int x, int time){
	if(v[x]) return;
//	printf("In: %d, %d\n", x, time);
	if(f[x] < time) return;
	f[x] = time;
	v[x] = 1;
//	printf("Trying: %d, %d\n", x, time);
	for(int i = 1; i <= mp[x][0]; i++){
		if(mp[x][i] > x){
			if(time % ((mp[x][i] - x) * 10) == 0)
				dfs(mp[x][i], time + (mp[x][i] - x) * 5);
			else{
				dfs(mp[x][i], ((time-1)/((mp[x][i]-x)*10)+1)*((mp[x][i]-x)*10) + (mp[x][i] - x) * 5);
			}
		}
		else{
			if((time + (x - mp[x][i]) * 5) % ((x - mp[x][i]) * 10) == 0)	
				dfs(mp[x][i], time + (x - mp[x][i]) * 5);
			else{
				dfs(mp[x][i], ((time-1+(x-mp[x][i])*5)/((x-mp[x][i])*10)+1)*((x-mp[x][i])*10-(x-mp[x][i])*5) + (x - mp[x][i]) * 5);
			}
		}
	}
	v[x] = 0;
}
int main(){
	freopen("lift.in", "r", stdin);
	freopen("lift.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 0; i < m; i++)
		scanf("%d%d", &x, &y),
		mp[x][++mp[x][0]] = y,
		mp[y][++mp[y][0]] = x;
	memset(f, 127/3, sizeof f);
	dfs(1, 0);
	printf("%d", f[n]);
	return 0;
}


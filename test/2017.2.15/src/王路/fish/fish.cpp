#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
const int maxn = 100005;
int n, a[maxn], m;
struct data {
	int b, d;
	bool operator <(const data & x) const {
		return b > x.b;
	}
} boat[maxn];
int main() {
	freopen("fish.in", "r", stdin);
	freopen("fish.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; i++) { cin >> a[i]; a[i] += a[i-1];}
//	for (int i = 1; i <= n; i++) cout << a[i] << endl;
	cin >> m;
	int len = 0;
	for (int i = 1; i <= m; i++) {
		cin >> boat[i].b >> boat[i].d;
		len += boat[i].d;
	}
	sort(boat+1,boat+m+1);
	int ans = 0;
	sort(a+1,a+n+1);
	for (int i = 1; i <= len; i++) ans += a[i] - a[i-1];
	cout << ans << endl;
}
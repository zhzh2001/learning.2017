#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <ctime>
using namespace std;
const int maxn = 1005;
int n, m, s[maxn];
int a[maxn];
typedef long long ll;
int sum;
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	srand((unsigned)time(NULL));
	cin >> n >> m;
	if (n <= m) {
		cout << n << endl; exit(0);
	}
	for (int i = 1; i <= m; i++) {
		cin >> s[i];
	}
	cout << (rand()%m)+1 << endl;
}
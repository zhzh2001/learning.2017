#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
const int maxk = 1005, maxn = 50005;
const int inf = 0x3f3f3f3f;
const int v = 5;
int k, n, l[maxn], u[maxn];
int len[maxk][maxk];
int ans = 0x3f3f3f3f;
int clock;
bool vis[maxk];
void dfs(int loc, int t) {
//	cout << loc << endl;
	vis[loc] = true;
	if (loc == k) {
//		cout << "ans:" << t << endl;
		ans = min(ans, t);
		vis[loc] = false;
		return;
	}
	for (int i = 1; i <= k; i++) {
		if (len[loc][i] < inf && !vis[i]) {
			int p = len[loc][i] * 2;
			int tt = t + len[loc][i] + ((p - t) % p);
			vis[i] = true;
			dfs(i, tt);
			vis[i] = false;
		}
	}
}

int main() {
	freopen("lift.in", "r", stdin);
	freopen("lift.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> k >> n;
	memset(len, 0x3f, sizeof len);
	for (int i = 1; i <= n; i++) {
		cin >> l[i] >> u[i];
		len[l[i]][u[i]] = len[u[i]][l[i]] = min(len[u[i]][l[i]], (u[i] - l[i]) * v);
	}
//	cout << len[7][20] << endl;
//	cout << len[1][7] << endl;
	dfs(1,0);
	cout << ans << endl;
}
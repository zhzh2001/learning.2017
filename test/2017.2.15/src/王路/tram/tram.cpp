#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
const int maxn = 1005;
const int inf = 0x3f3f3f3f;
int n, a, b, k[maxn], t, len[maxn][maxn], dist[maxn];
bool vis[maxn];

void Dijkstra(int s){
	vis[s] = true;
	for (int i = 1; i <= n; i++) dist[i] = len[s][i];
	for (int i = 1; i < n; i++) {
		int v, Min = inf;
		for (int j = 1; j <= n; j++) if (!vis[j] && dist[j] < Min)
			Min = dist[j], v = j;
		vis[v] = true;
		for (int j = 1; j <= n; j++) if (!vis[j])
			dist[j] = min(dist[j], dist[v]+len[v][j]);
	}
}

int main(){
	freopen("tram.in", "r", stdin);
	freopen("tram.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> n >> a >> b;
	memset(len, 0x3f, sizeof len);
	for (int i = 1; i <= n; i++) {
		cin >> k[i];
		for (int j = 1; j <= k[i]; j++) {
			cin >> t;
			len[i][t] = 1; if (j == 1) len[i][t] = 0;
		}
	}
	Dijkstra(a);
	if (dist[b] >= inf) cout << "-1\n";
		else cout << dist[b] << endl;
}
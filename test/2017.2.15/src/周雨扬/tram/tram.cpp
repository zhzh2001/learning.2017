#include<cstdio> 
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
struct edge{int to,next,v;}e[100005];
int head[1005],dis[1005],q[10010],v[1005];
int n,S,T,k,x,h,t,tot;
void ins(int x,int y,int v){
	e[++tot].to=y;
	e[tot].v=v;
	e[tot].next=head[x];
	head[x]=tot;
}
int main(){
	freopen("tram.in","r",stdin);
	freopen("tram.out","w",stdout);
	scanf("%d%d%d",&n,&S,&T);
	for (int i=1;i<=n;i++){
		scanf("%d",&k);
		for (int j=1;j<=k;j++){
			scanf("%d",&x);
			if (j==1) ins(i,x,0); else ins(i,x,1);
		}
	}
	memset(dis,100,sizeof(dis));
	dis[S]=h=0;
	t=v[S]=1;
	q[1]=S;
	while (h!=t){
		h=h%10007+1;
		x=q[h];
		v[x]=0;
		for (int i=head[x];i;i=e[i].next)
			if (dis[e[i].to]>dis[x]+e[i].v){
				dis[e[i].to]=dis[x]+e[i].v;
				if (!v[e[i].to]){
					v[e[i].to]=1;
					t=t%10007+1;
					q[t]=e[i].to;
				}
			}
	}
	printf("%d",dis[T]>=105?-1:dis[T]);
}

#include<cstdio> 
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
struct edge{int to,next;}e[100005];
int head[1005],dis[1005],q[10010],v[1005];
int n,m,x,y,h,t,p,tim,tot,s;
void ins(int x,int y){
	e[++tot].to=y;
	e[tot].next=head[x];
	head[x]=tot;
}
int main(){
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		ins(x,y);
		ins(y,x);
	}
	memset(dis,100,sizeof(dis));
	dis[1]=h=0; q[1]=v[1]=t=1;
	while (h!=t){
		h=h%10007+1;
		x=q[h];
		v[x]=0;
		for (int i=head[x];i;i=e[i].next){
			s=abs(x-e[i].to);
			if (x<e[i].to){
				p=(dis[x]+s*10-1)/(s*10);
				tim=(p*2+1)*s*5;
			}
			else{
				p=(dis[x]-1+s*5)/(s*10);
				tim=(p+1)*s*10; 
			}
			if (tim<dis[e[i].to]){
				dis[e[i].to]=tim;
				if (!v[e[i].to]){
					v[e[i].to]=1;
					t=t%10007+1;
					q[t]=e[i].to;
				}
			}
		}
	}
	printf("%d",dis[n]);
}

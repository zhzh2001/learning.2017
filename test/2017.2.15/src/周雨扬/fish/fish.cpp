#include<cstdio> 
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
struct data{int x,y,l,r;}b[100005];
int a[100005],f[100005],n,m;
bool cmp(data a,data b){return a.x<b.x;}
int main(){
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) a[i]+=a[i-1];
	scanf("%d",&m);
	for (int i=1;i<=m;i++) scanf("%d%d",&b[i].x,&b[i].y);
	sort(b+1,b+m+1,cmp);
	b[0].y=1;
	b[m+1].x=0x3f3f3f3f;
	for (int i=1;i<=m;i++) b[i].l=max(b[i].x-b[i].y+1,b[i-1].l+b[i-1].y);
	for (int i=m;i>=1;i--) b[i].r=min(b[i].x,b[i+1].x-b[i].y);
	b[m+1].r=n+1;
	for (int i=1;i<=m;i++){
		for (int j=b[i].l;j<=b[i].r;j++)
			f[j+b[i].y]=max(f[j+b[i].y],f[j]+a[j+b[i].y-1]-a[j-1]);
		for (int j=b[i].l+b[i].y+1;j<=b[i+1].r;j++) f[j]=max(f[j],f[j-1]);
	}
	printf("%d",f[n+1]);
}

#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
struct da
{
	int b,d;
}x[100005];
bool cmp(da x,da y)
{
	return x.b<y.b;
}
int a[100005],f[100005],n,m,i,j,k;
int p(int z)
{
	return z>=x[i-1].b+x[i-1].d-1?f[x[i-1].b+x[i-1].d-1]:f[z];
}
int main()
{
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
		scanf("%d",&a[i]);
	for (i=2;i<=n;++i)
		a[i]+=a[i-1];
	scanf("%d",&m);
	for (i=1;i<=m;++i)
		scanf("%d%d",&x[i].b,&x[i].d);
	sort(x+1,x+1+m,cmp);
	for (i=1;i<=m;++i)
	{
		k=max(x[i-1].b+x[i].d,x[i].b);
		f[k]=a[k]-a[k-x[i].d]+p(k-x[i].d);
		for (j=k+1;j<=k+x[i].d-1;++j)
			f[j]=max(f[j-1],a[j]-a[j-x[i].d]+p(j-x[i].d));
	}
	printf("%d",f[x[m].b+x[m].d-1]);
	return 0;
}

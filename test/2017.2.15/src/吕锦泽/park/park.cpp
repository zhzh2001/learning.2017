#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
struct data{
	ll v,id;
}h[1001],a[1001];
ll len,n,m;
void put(data x){
	h[++len]=x;
	int fa=len;
	while (fa>1&&((h[fa].v<h[fa>>1].v)||h[fa].v==h[fa/2].v&&h[fa].id<h[fa/2].id)){
		swap(h[fa],h[fa/2]);
		fa/=2;
	}
}
void down(){
	h[1].v+=a[h[1].id].v;
	int fa=1,son=2;
	while (son<=len){
		if (son+1<=len&&((h[son+1].v<h[son].v)||h[son].v==h[son+1].v&&h[son+1].id<h[son].id)) son++;
		if (h[fa].v>h[son].v) swap(h[fa],h[son]);
		else break;
		fa=son; son*=2;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d",&a[i].v);
		a[i].id=i;
		put(a[i]);
	}
	if (n<=m){
		printf("%d",n);
		return 0;
	}
	for (int i=m+1;i<n;i++) {
		down();
	}
	printf("%d",h[1].id);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,k,tot,time[50002],id[50001],ans[50001],vis[50001],l[50001],u[50001],to[50001],next[50001],dis[50001],head[10001];
struct que{
	int id,time;
};
void add(int u,int v,int value){
	to[++tot]=v; next[tot]=head[u]; head[u]=tot;
	dis[tot]=value*5;
}
int wait(int time,int i){
	return (2*dis[i]-time%(2*dis[i]))%(2*dis[i]);
}
int main(){
	freopen("lift.in","r",stdin);
	freopen("lift.out","w",stdout);
	scanf("%d%d",&k,&n);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&l[i],&u[i]);
		add(l[i],u[i],(u[i]-l[i])); add(u[i],l[i],(u[i]-l[i]));
	}
	memset(ans,127,sizeof ans);
	int l=0,r=1;
	time[1]=0; id[1]=1;
	while (l<r){
		l++; vis[id[l]]=1;
		ans[id[l]]=min(time[l],ans[id[l]]);
		for (int i=head[id[l]];i;i=next[i])
			if (!vis[to[i]]||ans[to[i]]>wait(time[l],i)+dis[i]){
				time[++r]=time[l]+wait(time[l],i)+dis[i];
				id[r]=to[i];
		}
	}
	printf("%d",ans[k]);
}

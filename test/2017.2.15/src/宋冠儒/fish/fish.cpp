#include<cstdio>
#include<algorithm>
using namespace std;
const int N=100005;
struct node{
	int b,d;
	bool operator <(const node&x)const{return b<x.b;}
}w[N];
int i,j,k,n,m,a[N],f[N];
inline void Read(int &x)
{
	x=0; char ch;
	for(ch=getchar();!(ch>='0'&&ch<='9');ch=getchar());
	for(;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
}
int main()
{
	freopen("fish.in","r",stdin);
	freopen("fish.out","w",stdout);
	for(Read(n),i=1;i<=n;i++)
	{
		Read(j);
		a[i]=a[i-1]+j;
	}
	Read(m),w[0].d=1;
	for(i=1;i<=m;i++)
		Read(w[i].b),Read(w[i].d);
	sort(w+1,w+m+1);
	for(i=1;i<=m;i++)
		for(j=w[i].b+w[i].d-1;j>=w[i].b&&j>=w[i].d;j--)
		{
			k=min(min(j-1,j-w[i].d),w[i-1].b+w[i-1].d-1);
			for(f[j]=0;k>=w[i-1].b;k--)
				f[j]=max(f[j],f[k]);
			f[j]+=a[j]-a[max(0,j-w[i].d)];
		}
	for(i=w[m].b;i<w[m].b+w[m].d;i++)
		f[n]=max(f[n],f[i]);
	printf("%d",f[n]);
}

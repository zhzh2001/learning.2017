#include <iostream>
#include <stdio.h>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
const int INF = 0x7fffffff;
int n, m, s[1001], Min;
ll GetNumber(ll Minite)
{
    int i;
    ll number = 0;
    for (i = 0; i < m && number < n; i++)
        number += (Minite + s[i] - 1) / s[i];
    return number;
}
ll FindTime()
{
    ll l, r, mid, num;
    l = 0, r = ((ll) Min) * n; 
    while (l < r)
    {
        mid = (l + r) / 2;
        num = GetNumber(mid);
        if (num >= n)
            r = mid;
        else
            l = mid + 1;
    }
    return r;
}
int FindId(ll Minite)
{
    int i;
    ll t_n = n;
    t_n -= GetNumber(Minite - 1);
    Minite--;
    for (i = 0; i < m; i++)
    {
        if (Minite % s[i] == 0)
        {
            t_n--;
            if (t_n == 0) return i + 1;
        }
    }
    return -1;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
    scanf("%d%d", &n, &m);
    int i;
    Min = INF;
    for (i = 0; i < m; i++)
    {
        scanf("%d", &s[i]);
        if (s[i] < Min) Min = s[i];
    }
    ll time = FindTime();
    int id = FindId(time);
    printf("%d\n", id);
    return 0;
}

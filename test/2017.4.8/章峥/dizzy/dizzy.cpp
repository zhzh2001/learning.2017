#include<fstream>
#include<ctime>
#include<cstdlib>
#include<stack>
#include<cstring>
using namespace std;
ifstream fin("dizzy.in");
ofstream fout("dizzy.out");
const int N=100005,M=100005;
struct edge
{
	int u,v;
}E[M];
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int _head[N],_v[2*M],_nxt[2*M],_e;
inline void _add_edge(int u,int v)
{
	_v[++_e]=v;
	_nxt[_e]=_head[u];
	_head[u]=_e;
}
bool vis[N],inS[N];
stack<int> S;
bool check(int k)
{
	vis[k]=true;
	S.push(k);
	inS[k]=true;
	for(int i=_head[k];i;i=_nxt[i])
		if(!vis[_v[i]])
		{
			if(check(_v[i]))
				return true;
		}
		else
			if(inS[_v[i]])
				return true;
	S.pop();
	inS[k]=false;
	return false;
}
int main()
{
	srand(time(NULL));
	int n,m1,m2;
	fin>>n>>m1>>m2;
	while(m1--)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
	}
	for(int i=1;i<=m2;i++)
		fin>>E[i].u>>E[i].v;
	while(clock()<0.8*CLOCKS_PER_SEC)
	{
		memcpy(_head+1,head+1,n*sizeof(int));
		memcpy(_v+1,v+1,e*sizeof(int));
		memcpy(_nxt+1,nxt+1,e*sizeof(int));
		_e=e;
		for(int i=1;i<=m2;i++)
		{
			if(rand()&128)
				swap(E[i].u,E[i].v);
			_add_edge(E[i].u,E[i].v);
		}
		memset(vis+1,false,n);
		memset(inS+1,false,n);
		while(!S.empty())
			S.pop();
		bool flag=true;
		for(int i=1;i<=n;i++)
			if(!vis[i]&&check(i))
			{
				flag=false;
				break;
			}
		if(flag)
		{
			for(int i=1;i<=m2;i++)
				fout<<E[i].u<<' '<<E[i].v<<endl;
			return 0;
		}
	}
	fout<<-1<<endl;
	return 0;
}
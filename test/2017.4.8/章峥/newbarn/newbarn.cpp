#include<fstream>
#include<set>
#include<algorithm>
#include<queue>
using namespace std;
ifstream fin("newbarn.in");
ofstream fout("newbarn.out");
typedef pair<int,int> point;
const int N=10005,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
point p[N];
set<point> vis;
struct node
{
	point p;
	int d;
	node(const point& p,int d):p(p),d(d){}
	bool operator>(const node& rhs)const
	{
		return d>rhs.d;
	}
};
priority_queue<node,vector<node>,greater<node> > Q;
int main()
{
	int n;
	fin>>n;
	int minx=N,maxx=-N,miny=N,maxy=-N;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i].first>>p[i].second;
		vis.insert(p[i]);
		minx=min(minx,p[i].first);
		maxx=max(maxx,p[i].first);
		miny=min(miny,p[i].second);
		maxy=max(maxy,p[i].second);
	}
	if((long long)(maxx-minx)*(maxy-miny)*n<=1e7)
	{
		//cheat
		int ans=1e9,cnt=0;
		for(int x=minx;x<=maxx;x++)
			for(int y=miny;y<=maxy;y++)
				if(vis.find(make_pair(x,y))==vis.end())
				{
					int now=0;
					for(int i=1;i<=n;i++)
						now+=abs(x-p[i].first)+abs(y-p[i].second);
					if(now<ans)
						ans=now,cnt=0;
					if(now==ans)
						cnt++;
				}
		fout<<ans<<' '<<cnt<<endl;
		return 0;
	}
	point s=make_pair((minx+maxx)/2,(miny+maxy)/2);
	int dist=0;
	for(int i=1;i<=n;i++)
		dist+=abs(s.first-p[i].first)+abs(s.second-p[i].second);
	Q.push(node(s,dist));
	vis.insert(s);
	int cnt=0;
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(k.d<dist)
			dist=k.d,cnt=0;
		if(k.d==dist)
			cnt++;
		if(k.d>dist)
			break;
		for(int i=0;i<4;i++)
		{
			int nx=k.p.first+dx[i],ny=k.p.second+dy[i],now=0;
			if(vis.find(make_pair(nx,ny))!=vis.end())
				continue;
			for(int j=1;j<=n;j++)
				now+=abs(nx-p[j].first)+abs(ny-p[j].second);
			if(now<=k.d)
			{
				Q.push(node(make_pair(nx,ny),now));
				vis.insert(make_pair(nx,ny));
			}
		}
	}
	fout<<dist<<' '<<cnt<<endl;
	return 0;
}
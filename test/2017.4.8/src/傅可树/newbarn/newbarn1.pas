type pp=record
     ls,rs,lazy,max:longint;
end;
var pos,tot,xx1,yy1,xx2,yy2,dx,dy,maxx,maxy,minx,miny,n,i,j:longint;
    x,y:array[0..10005]of longint;
    root:array[0..80005]of longint;
    a:array[0..2000000]of pp;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
procedure swap(var x,y:longint);
var z:longint;
begin
  z:=x; x:=y; y:=z;
end;
procedure pushdown(k:longint);
begin
  if a[k].lazy=0 then exit;
  if a[k].ls=0 then begin inc(tot); a[k].ls:=tot; end;
  if a[k].rs=0 then begin inc(tot); a[k].rs:=tot; end;
  inc(a[a[k].ls].lazy,a[k].lazy); inc(a[a[k].rs].lazy,a[k].lazy);
  inc(a[a[k].ls].max,a[k].lazy); inc(a[a[k].rs].max,a[k].lazy);
end;
procedure pushup(k:longint);
begin
  a[k].max:=max(a[a[k].ls].max,a[a[k].rs].max);
end;
procedure update(var k:longint;l,r,x,y:longint);
var mid:longint;
begin
  if k=0 then begin inc(tot); k:=tot; end;
  pushdown(k);
  if (l=x)and(r=y) then begin inc(a[k].lazy); inc(a[k].max); exit; end;
  mid:=(l+r) div 2;
  if mid>=x then update(a[k].ls,l,mid,x,y)
	else if mid<y then update(a[k].rs,mid+1,r,x,y)
	   else begin update(a[k].ls,l,mid,x,mid); update(a[k].rs,mid+1,r,mid+1,y); end;
  pushup(k);
end;
procedure cover(k,l,r,x1,x2,y1,y2:longint);
var mid:longint;
begin
  update(root[k],1,dy,y1,y2);
  if (l=x1)and(r=x2) then exit;
  mid:=(l+r) div 2;
  if mid>=x2 then cover(k*2,l,mid,x1,x2,y1,y2)
	else if mid<x1 then cover(k*2+1,mid+1,r,x1,x2,y1,y2)
	   else begin cover(k*2,l,mid,x1,mid,y1,y2); cover(k*2+1,mid+1,r,mid+1,x2,y1,y2); end;
end;
begin
  readln(n);
  maxx:=-maxlongint; maxy:=-maxlongint; minx:=maxlongint; miny:=maxlongint;
  for i:=1 to n do 
     begin
     readln(x[i],y[i]);
     maxx:=max(maxx,x[i]); maxy:=max(maxy,y[i]);
     minx:=min(minx,x[i]); miny:=min(miny,y[i]);
     end;
  dx:=maxx-minx; dy:=maxy-miny;
  for i:=1 to n do begin x[i]:=x[i]-minx; y[i]:=y[i]-miny; end;
  for i:=1 to n-1 do
     for j:=i+1 to n do 
 	begin
	xx1:=x[i]; xx2:=x[j]; yy1:=y[i]; yy2:=y[j];
	if xx1>xx2 then swap(xx1,xx2); 
	if yy1>yy2 then swap(yy1,yy2);
	cover(1,1,dx,xx1,xx2,yy1,yy2);
	end;
  pos:=a[root[1]].max;
  query(1,1,n);
end.
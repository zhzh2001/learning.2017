#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
//ifstream fin(".in");
//ofstream fout(".out");
int lowbit(int x){
	return x&(-x);
}
int t[1000003];
int n,bian,h,r;
void add(int x,int v){
	for(;x;x-=lowbit(x)){
		t[x]+=v;
	}
}
int query(int x){
	int ret=0;
	for(;x<=n;x+=lowbit(x)){
		ret+=t[x];
	}
	return ret;
}
int main(){
	cin>>n>>bian>>h>>r;
	for(int i=1;i<=r;i++){
		int a,b;
		cin>>a>>b;
		if(a<b){
			add(a,1);
			add(b-1,-1);
		}else{
			add(b,1);
			add(a-1,-1);
		}
	}
	for(int i=1;i<=n;i++){
		int x=query(i);
		cout<<x+h<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
9 3 5 5
1 3
5 3
4 3
3 7
9 8

out:
5
4
5
3
4
4
5
5
5

*/

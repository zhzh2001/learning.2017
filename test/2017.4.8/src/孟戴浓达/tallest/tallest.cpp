//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("tallest.in");
ofstream fout("tallest.out");
int to[7000003],head[7000003],val[7000003],next[7000003];
int tot,n,bian,r,h;
int q[1000003],dis[100003];
bool vis[100003];
int chu[100003];
void add(int a,int b,int c){
	to[++tot]=b;
	val[tot]=c;
	next[tot]=head[a];
	head[a]=tot;
}
int cnt;
void spfa(){
	for(int i=1;i<=n;i++){
		if(dis[i]==0){
			dis[i]=-999999999;
		}else{
			q[++cnt]=i;
			vis[i]=true;
		}
	}
	int t=1,w=cnt+1;
	/*
	q[0]=bian;
	dis[bian]=h;
	vis[bian]=true;
	*/
	while(t!=w){
		int x=q[t++];
		if(t==1000000){
			t=1;
		}
		vis[x]=false;
		for(int i=head[x];i;i=next[i]){
			int tt=to[i];
			if(dis[tt]<=dis[x]+val[i]){
				dis[tt]=dis[x]+val[i];
				if(vis[tt]==false){
					q[w++]=tt;
					if(w==1000000){
						w=1;
					}
					vis[tt]=true;
				}
			}
		}
	}
	return;
}
int main(){
	fin>>n>>bian>>h>>r;
	dis[bian]=h;
	for(int i=1;i<=r;i++){
		int a,b;
		fin>>a>>b;
		add(b,a,0);
		chu[a]++;
		if(a==bian||dis[a]==h){
			dis[b]=h;
		}
		for(int j=min(a,b)+1;j<=max(a,b)-1;j++){
			add(a,j,1);
			add(b,j,1);
			chu[j]++;
		}
	}
	//int x=h-dis[bian];
	for(int i=1;i<=n;i++){
		if(chu[i]==0||i==bian){
			dis[i]=h;
		}
	}
	spfa();
	for(int i=1;i<=n;i++){
		if(dis[i]==-999999999){
			fout<<h<<endl;
		}else{
			fout<<(h-(dis[i]-h))<<endl;
		}
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*
in:
9 3 5 5
1 3
5 3
4 3
3 7
9 8

out:
5
4
5
3
4
4
5
5
5
*/

/*
//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("tallest.in");
ofstream fout("tallest.out");
int lowbit(int x){
	return x&(-x);
}
int t[1000003];
int n,bian,h,r;
void add(int x,int v){
	for(;x;x-=lowbit(x)){
		t[x]+=v;
	}
}
int query(int x){
	int ret=0;
	for(;x<=n;x+=lowbit(x)){
		ret+=t[x];
	}
	return ret;
}
int main(){
	fin>>n>>bian>>h>>r;
	for(int i=1;i<=r;i++){
		int a,b;
		fin>>a>>b;
		if(a<b){
			add(a,1);
			add(b-1,-1);
		}else{
			add(b,1);
			add(a-1,-1);
		}
	}
	for(int i=1;i<=n;i++){
		int x=query(i);
		fout<<x+h<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
9 3 5 5
1 3
5 3
4 3
3 7
9 8

out:
5
4
5
3
4
4
5
5
5

*/

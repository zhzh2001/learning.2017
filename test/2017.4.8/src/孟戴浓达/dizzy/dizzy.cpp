//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("dizzy.in");
ofstream fout("dizzy.out");
int n,m1,m2,tot;
int a1[100003],b1[100003],a2[100003],b2[100003];
int to[200003],head[200003],val[200003],next[200003];
void add(int a,int b){
	to[++tot]=b;
	next[tot]=head[a];
	head[a]=tot;
}
bool yes=false;
int vis[100003];
int tim=1;
void dfs(int node){
	vis[node]=tim;
	for(int i=head[node];i;i=next[i]){
		if(vis[to[i]]==tim){
			yes=true;
			return;
		}else{
			dfs(to[i]);
		}
	}
}
int main(){
	fin>>n>>m1>>m2;
	for(int i=1;i<=m1;i++){
		fin>>a1[i]>>b1[i];
	}
	for(int i=1;i<=m2;i++){
		fin>>a2[i]>>b2[i];
		int x=min(a2[i],b2[i]),y=max(a2[i],b2[i]);
		fout<<y<<" "<<x<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
4 2 3
1 2
4 3
1 3
4 2
3 2

out:
1 3
2 4
2 3

*/

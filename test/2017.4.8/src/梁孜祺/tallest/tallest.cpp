#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=400000;
struct tree{
	int lazy;
	int l,r,v;
}node[N];
int n,m,r,h;

void update(int num){
	node[num].v+=node[num].lazy;
	if(node[num].l==node[num].r) return;
	int l=num<<1,r=num<<1|1;
	node[l].lazy+=node[num].lazy;
	node[r].lazy+=node[num].lazy;
	node[num].lazy=0;
}

void build(int l,int r,int num){
	node[num].l=l; node[num].r=r;
	node[num].lazy=node[num].v=0;
	if(l==r) return; int mid=(l+r)>>1;
	build(l,mid,num<<1); build(mid+1,r,num<<1|1);
}

void add(int l,int r,int num){
	if(node[num].l==l&&node[num].r==r){
		node[num].lazy++; return;
	}
	int mid=(node[num].l+node[num].r)>>1;
	if(mid<l) add(l,r,num<<1|1);
	else if(mid>=r) add(l,r,num<<1);
	else{ add(l,mid,num<<1); add(mid+1,r,num<<1|1); }
}

void end(int num){
	update(num); int l=node[num].l,r=node[num].r;
	if(l==r){ printf("%d\n",h-node[num].v); return; }
	end(num<<1); end(num<<1|1);
}

int main(){
	freopen("tallest.in","r",stdin);
	freopen("tallest.out","w",stdout);
	n=read(); r=read(); h=read(); m=read();
	build(1,n,1); int a,b;
	for(int i=1;i<=m;i++){
		a=read(); b=read();
		if(a>b) swap(a,b);
		if(b-a==1) continue;
		add(a+1,b-1,1);
	}
	end(1);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=100005,M=100005;
int in[N],out[N];
bool vis[N],mu[M*2];
int n,m1,m2,ans;
int nxt[M*2],last[N*2],to[M*2],cnt,from[M*2];
queue<int> q1,q2;
vector<int> p1[N],p2[N];

int main(){
	freopen("dizzy.in","r",stdin);
	freopen("dizzy.out","w",stdout);
	n=read(); m1=read(); m2=read();
	int u,v;
	for(int i=1;i<=m1;i++){
		u=read(); v=read();
		in[v]++; out[u]++;
		p2[v].push_back(u); p1[u].push_back(v);
	}
	for(int i=1;i<=m2;i++){
		u=read(); v=read();
		to[cnt]=v; nxt[cnt]=last[u]; last[u]=cnt; from[cnt]=u; cnt++;
		to[cnt]=u; nxt[cnt]=last[v]; last[v]=cnt; from[cnt]=v; cnt++;
//		printf("%d @ %d %d\n",i,from[(i-1)*2],to[(i-1)*2]);
//		printf("%d @ %d %d\n",i,from[i*2-1],to[i*2-1]);
	}
//	puts("");
	for(int i=1;i<=n;i++){
		vis[i]=true;
		if(in[i]==0) q1.push(i);
		else if(out[i]==0) q2.push(i);
		else vis[i]=false;
	}
	for(int t=1;t<=n;t++){
		if(!q1.empty()){
			int u=q1.front(); q1.pop();
			for(int i=0;i<p1[u].size();i++){
				int v=p1[u][i];
				if(!vis[v]){in[v]--; if(in[v]==0) q1.push(v);}
			}
			for(int i=last[u];i;i=nxt[i]) if(!mu[i]&&!mu[i^1]){ mu[i]=1; ans++;}
		}
		else{
			int u=q2.front(); q2.pop();
			for(int i=0;i<p2[u].size();i++){
				int v=p2[u][i];
				if(!vis[v]){out[v]--; if(out[v]==0) q2.push(v);}
			}
			for(int i=last[u];i;i=nxt[i]) if(!mu[i]&&!mu[i^1]){ mu[i^1]=1; ans++;}
		}
	}
//	printf("%d\n",cnt);
	if(ans<m2) printf("-1");
	else
		for(int i=0;i<cnt;i++)
			if(mu[i]) printf("%d %d\n",from[i],to[i]);
	return 0;
}

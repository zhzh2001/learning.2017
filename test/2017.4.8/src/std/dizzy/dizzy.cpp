#include <assert.h>
#include <string.h>
#include <stdio.h>

const int MAXN = 500000 + 10;
const int MAXM = 500000 + 10;

int n,m1,m2;
int outdeg[MAXN];
int indeg[MAXN];
int *edge[MAXN];
int edge_array[MAXM];
int pos[MAXN];

int mystack[MAXN];
bool v[MAXN];

int main () {

  FILE* fin = fopen ("dizzy.in", "r");
  fscanf (fin, "%d %d %d", &n, &m1, &m2);
  for (int i = 0; i < n; ++i) {
    outdeg[i] = 0;
  }
  for (int i = 0; i < m1; ++i) {
    int a,b;
    fscanf (fin, "%d %d", &a, &b);
    --a; --b; // 1 -> 0
    ++outdeg[a];
  }
  fclose (fin);

  edge[0] = &edge_array[0];
  for (int i = 1; i < n; ++i) {
    edge[i] = edge[i-1] + outdeg[i-1];
  }

  fin = fopen ("dizzy.in", "r");
  fscanf (fin, "%d %d %d", &n, &m1, &m2);
  for (int i = 0; i < n; ++i) {
    indeg[i] = 0;
    outdeg[i] = 0;
  }
  for (int i = 0; i < m1; ++i) {
    int a,b;
    fscanf (fin, "%d %d", &a, &b);
    --a; --b; // 1 -> 0
    edge[a][outdeg[a]] = b;
    ++outdeg[a];
    ++indeg[b];
  }

  memset (v, false, sizeof (bool)*n);
  int p = 0;
  for (int i = 0; i < n; ++i) {
    if (v[i] || indeg[i] != 0) continue;
    v[i] = true;
    int sp = 0;
    mystack[sp++] = i;
    while (sp != 0) {
      int x = mystack[--sp];
      pos[x] = p++;
      for (int j = 0; j < outdeg[x]; ++j) {
	int y = edge[x][j];
	--indeg[y];
	if (indeg[y] == 0) {
	  assert (!v[y]);
	  v[y] = true;
	  mystack[sp++] = y;
	}
      }
    }
  }

  FILE* fout = fopen ("dizzy.out", "w");
  for (int i = 0; i < m2; ++i) {
    int a,b;
    fscanf (fin, "%d %d", &a, &b);
    --a; --b; // 1 -> 0
    if (pos[a] < pos[b]) {
      fprintf (fout, "%d %d\n", 1+a, 1+b);
    } else {
      fprintf (fout, "%d %d\n", 1+b, 1+a);
    }
  }
  fclose (fout);
  fclose (fin);
  return (0);
}


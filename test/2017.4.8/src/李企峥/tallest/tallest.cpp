#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,m,h,r,x,y,a[100010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("tallest.in","r",stdin);
	freopen("tallest.out","w",stdout);
	n=read();
	m=read();
	h=read();
	r=read();
	For(i,1,n)a[i]=h;
	For(i,1,r)
	{
		x=read();
		y=read();
		if(a[x]>a[y])a[x]=a[y];
		if(x<y)For(j,x+1,y-1)a[j]=min(a[j]-1,a[x]-1);
		  else For(j,y+1,x-1)a[j]=min(a[j]-1,a[x]-1);
	}
	For(i,1,n)cout<<a[i]<<endl;
	return 0;
}


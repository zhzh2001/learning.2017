#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,mx,my,mix,miy,ans,sum,zz;
int x[10010],y[10010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("newbarn.in","r",stdin);
	freopen("newbarn.out","w",stdout);
	n=read();
	mx=-10000000000;mix=10000000000;
	my=-10000000000;miy=10000000000;
	For(i,1,n)
	{
		x[i]=read();
		y[i]=read();
		if(x[i]>mx)mx=x[i];
		if(x[i]<mix)mix=x[i];
		if(y[i]>my)my=y[i];
		if(y[i]<miy)miy=y[i];
	}
	ans=100000000000;
	sum=0;
	For(i,mix,mx)
		For(j,miy,my)
		{
			zz=0;
			For(k,1,n)
			{
				zz+=abs(x[k]-i);
				zz+=abs(y[k]-j);
			}
			if(zz<ans)ans=zz,sum=1;
			if(ans=zz)sum++;
		}
	cout<<ans<<" "<<sum<<endl;
	return 0;
}


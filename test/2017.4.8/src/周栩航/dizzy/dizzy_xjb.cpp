#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;

bool ins[100001];
int index=0,top=0,cnt=0,n,m,top1,m1,m2,cyc;
int kind[500001],ste[500001],change[500001],tx[500001],ty[500001];
int sta[100001],dfn[100001],low[100001],poi[500001],next[500001],f[500001],lin[10001],x[500001],y[500001],num[100001],vis[100001],cant[100001];
inline void add(int x,int y,int z)
{
    poi[++cnt]=y;next[cnt]=f[x];f[x]=cnt;kind[cnt]=z;
}
inline void dfs(int x,int fa)
{
    dfn[x]=low[x]=++index;
    sta[++top]=x;
    ins[x]=1;
    for(int i=f[x];i;i=next[i])
    {
        int t=poi[i];
        if(!dfn[t]&&t!=fa)    {    ste[++top1]=i;dfs(t,x);low[x]=min(low[x],low[t]);}
        else
        if(ins[x])
            low[x]=min(low[x],low[t]);
    }
    if(dfn[x]==low[x])
    {	cyc++;
        while(1)
        {
        
            int t=sta[top];
            top--;
            ins[t]=0;
            lin[t]=x;
            num[x]++;
            vis[x]=1;
            if(t==x)    break;
        	int tt=ste[top1];
			top1--;
        	if(kind[tt]==-1)	continue;
            if(!change[tt])	change[tt]=cyc;
        }
    }
}
int main()
{
  	scanf("%d%d%d",&n,&m1,&m2);
  	For(i,1,m1)
  	{
  		int x,y;
  		scanf("%d%d",&x,&y);
  		add(x,y,-1);
  	}
  	For(i,1,m2)
  	{
  		int x,y;
  		scanf("%d%d",&tx[i],&ty[i]);
  		x=tx[i],y=ty[i];
  		add(x,y,i*2-1);
  		add(y,x,i*2);
  	}
	For(i,1,n)
	{
		if(!dfn[i])	dfs(i,-1);
	}
	For(i,1,m2)	if(change[i*2]&&change[i*2-1])	{printf("-1");return 0;}
	For(i,1,m2)
		if(change[i*2-1])	printf("%d %d\n",tx[i],ty[i]);else printf("%d %d\n",ty[i],tx[i]);
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int min_x,max_x,min_y,max_y,x[5001],y[5001];
bool vis[5001][5001];
int n,tot;
int main()
{
	scanf("%d",&n);
	For(i,1,n)
	{
		scanf("%d%d",&x[i],&y[i]);
		min_x=min(min_x,x[i]);
		min_y=min(min_y,y[i]);
		max_x=max(max_x,x[i]);
		max_y=max(max_y,y[i]);
		vis[x[i]+1001][y[i]+1001]=1;
	}
	int ans=1e9;
	For(i,min_x-1,max_x+1)
		For(j,min_y-1,max_y+1)
		{
			if(vis[i+1001][j+1001])	continue;
			int tmp=0;
			For(t,1,n)
				tmp+=abs(i-x[t])+abs(j-y[t]);
			if(tmp<ans)
				ans=tmp,tot=0;
			if(tmp==ans)	tot++;
		}
	printf("%d %d",ans,tot);
}

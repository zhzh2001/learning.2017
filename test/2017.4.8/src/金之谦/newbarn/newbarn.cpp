#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll MOD1=1000007;
const ll MOD2=1299709;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{
	ll x,y;
}a[10001];
ll n,x[10001],y[10001],xl,xr,yl,yr,ans=1e9,ans1=0;
ll ha1[1000007][2]={0},ha2[1299709][2]={0};
int hx[10001]={0},hy[10001]={0};
bool flag=0;
int ax[10001]={0},ay[10001]={0};
inline ll h1(ll x,ll y){
	ll i=x*MOD2%MOD1*y%MOD1;
	while(ha1[i][0]&&(ha1[i][0]!=x||ha1[i][1]!=y))i=(i+1)%MOD1;
	return i;
}
inline ll h2(ll x,ll y){
	ll i=x*MOD1%MOD2*y%MOD2;
	while(ha2[i][0]&&(ha2[i][0]!=x||ha2[i][1]!=y))i=(i+1)%MOD2;
	return i;
}
inline void dfs(ll xxl,ll xxr,ll yyl,ll yyr,ll sum){
	if(sum>ans)return;
	ll k=0;
	for(ll i=xxl;i<=xxr;i=(xxl==xl&&xxr==xr)?i+1:xxr){
		for(ll j=yyl;j<=yyr;j=(yyl==yl&&yyr==yr)?j+1:yyr){
			ll p=h1(i,j),q=h2(i,j);
			if(ha1[p][0]!=i||ha1[p][1]!=j||ha2[q][0]!=i||ha2[q][1]!=j)k++;
			if(j==yyr)break;
		}
		if(i==xxr)break;
	}
	if(k==0){
		if(xxl>1)dfs(xxl-1,xxr+1,yyl,yyr,sum+hx[xxl]);
		if(yyl>1)dfs(xxl,xxr,yyl-1,yyr+1,sum+hy[yyl]);
	}else{
		if(sum==ans)ans1+=k;
		else{ans=sum;ans1=k;}
		return;
	}
}
int main()
{
	freopen("newbarn.in","r",stdin);
	freopen("newbarn.out","w",stdout);
	n=read();
	for(ll i=1;i<=n;i++){
		x[i]=a[i].x=read()+10001;y[i]=a[i].y=read()+10001;
		ax[x[i]]=ay[y[i]]=1;
		ll p=h1(x[i],y[i]);ha1[p][0]=x[i];ha1[p][1]=y[i];
		p=h2(x[i],y[i]);ha2[p][0]=x[i];ha2[p][1]=y[i];
	}
	sort(x+1,x+n+1);sort(y+1,y+n+1);
	xl=xr=x[(n+1)/2];yl=yr=y[(n+1)/2];
	hx[xl]=hy[yl]=1;
	if(!(n%2))xr=x[(n+1)/2+1],yr=y[(n+1)/2+1],hx[xl]++,hy[yl]++;
	for(ll i=xl-1;i;i--)hx[i]=hx[i+1]+(ll)ax[i]*2;
	for(ll i=yl-1;i;i--)hy[i]=hy[i+1]+(ll)ay[i]*2;
	ll rp=0;
	for(ll i=1;i<=n;i++)rp+=abs(x[i]-xl)+abs(y[i]-yl);
	dfs(xl,xr,yl,yr,rp);
	printf("%I64d %I64d",ans,ans1);
	return 0;
}

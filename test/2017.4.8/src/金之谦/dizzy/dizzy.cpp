#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
queue<int>q;
int nedge=0,p[400001],c[400001],nex[400001],head[400001];
int sum=0,ans1[100001],ans2[100001],d[100001]={0};
bool b[100001]={0},e[100001]={0};
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;
	nex[nedge]=head[x];head[x]=nedge;
}
int main()
{
	freopen("dizzy.in","r",stdin);
	freopen("dizzy.out","w",stdout);
	int n=read(),m1=read(),m2=read();
	for(int i=1;i<=m1;i++){
		int x=read(),y=read();
		addedge(x,y,0);d[y]++;
	}
	for(int i=1;i<=m2;i++){
		int x=read(),y=read();
		addedge(x,y,i);
		addedge(y,x,i);
	}
	for(int i=1;i<=n;i++)if(!d[i])q.push(i),b[i]=1,sum++;
	while(!q.empty()){
		int now=q.front();q.pop();
		for(int k=head[now];k;k=nex[k]){
			if(e[c[k]])continue;
			if(c[k]==0){
				d[p[k]]--;
				if(d[p[k]]==0&&!b[p[k]])q.push(p[k]),b[p[k]]=1,sum++;
			}else{
				e[c[k]]=1;
				ans1[c[k]]=now;ans2[c[k]]=p[k];
			}
		}
	}
	if(sum<n){
		puts("-1");
		return 0;
	}
	for(int i=1;i<=m2;i++)printf("%d %d\n",ans1[i],ans2[i]);
	return 0;
}

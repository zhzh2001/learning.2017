#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{
	int x,y;
}p[10001];
inline bool cmp(ppap a,ppap b){return a.x==b.x?a.y<b.y:a.x<b.x;}
int n,m,h,k,a[400001],t[400001],lt[400001],rt[400001],add[400001];
inline void clean(int nod){
	if(add[nod]==0)return;
	if(lt[nod]!=rt[nod]){
		t[nod*2]+=add[nod]*(rt[nod*2]-lt[nod*2]+1);
		add[nod*2]+=add[nod];
		t[nod*2+1]+=add[nod]*(rt[nod*2+1]-lt[nod*2+1]+1);
		add[nod*2+1]+=add[nod];
	}
	add[nod]=0;
}
inline void build(int l,int r,int nod){
	lt[nod]=l;rt[nod]=r;
	if(l==r)t[nod]=a[l];
	else{
		int m=(l+r)/2;
		build(l,m,nod*2);
		build(m+1,r,nod*2+1);
		t[nod]=t[nod*2]+t[nod*2+1];
	}
}
inline void xg(int i,int j,int p,int nod){
	clean(nod);
	if(i==lt[nod]&&j==rt[nod]){
		add[nod]=p;
		t[nod]=t[nod]+(rt[nod]-lt[nod]+1)*p;
		return;
	}
	int m=(lt[nod]+rt[nod])/2;
	if(i<=m)xg(i,min(j,m),p,nod*2);
	if(j>m)xg(max(i,m+1),j,p,nod*2+1);
	t[nod]=t[nod*2+1]+t[nod*2];
}
inline int s(int i,int j,int nod){
	clean(nod);
	if(lt[nod]==i&&rt[nod]==j)return t[nod];
	int m=(lt[nod]+rt[nod])/2,sum=0;
	if(i<=m)sum+=s(i,min(j,m),nod*2);
	if(j>m)sum+=s(max(i,m+1),j,nod*2+1);
	return sum;
}
int main()
{
	freopen("tallest.in","r",stdin);
	freopen("tallest.out","w",stdout);
	n=read();k=read();h=read();m=read();
	for(int i=1;i<=n;i++)a[i]=h;
	build(1,n,1);
	for(int i=1;i<=m;i++){
		p[i].x=read();p[i].y=read();
		if(p[i].x>p[i].y)swap(p[i].x,p[i].y);
	}
	sort(p+1,p+m+1,cmp);
	for(int i=1;i<=m;i++){
		if(p[i].x==p[i-1].x&&p[i].y==p[i-1].y)continue;
		xg(p[i].x+1,p[i].y-1,-1,1);
	}
	for(int i=1;i<=n;i++)printf("%d\n",s(i,i,1));
	return 0;
}

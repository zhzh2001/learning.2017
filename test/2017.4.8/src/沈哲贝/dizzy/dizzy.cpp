#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<vector>
#define inf 1e9
#define ll int
#define maxn 200100
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){    ll x=0;char ch=getchar();    while(ch<'0'||ch>'9')ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x;    }
inline void write(ll x){    if (x>=10) write(x/10);    putchar(x%10+'0');    }
void writeln(ll x){    write(x);    puts("");    }
ll next[maxn],head[maxn],vet[maxn],n,m1,m2,c,tot;
bool vis[maxn];
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
} 
bool dfs(ll x,ll y){
	if (x==y)	return 1;
	vis[x]=c;
	for(ll i=head[x];i;i=next[i])
	if (vis[vet[i]]!=c&&dfs(vet[i],y))	return 1;
	return 0;
}
int main(){
	freopen("dizzy.in","r",stdin);
	freopen("dizzy.out","w",stdout); 
	n=read();	m1=read();	m2=read();
	For(i,1,m1){
		ll x=read(),y=read();
		insert(x,y);
	}
	For(i,1,m2){
		ll x=read(),y=read();	c++;
		if (dfs(y,x))	printf("%d %d\n",y,x);
		else	printf("%d %d\n",x,y),insert(x,y);
	}
}

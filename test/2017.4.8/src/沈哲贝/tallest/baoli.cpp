#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int 
#define maxn 200010
#define inf 200000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll next[maxn],head[maxn],vet[maxn],mark[maxn],maybe[maxn],truly[maxn],tot,n,I,H,R;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void change(ll x,ll y,ll v){
	For(i,x,y)	mark[i]+=v;
}
ll find(){
	ll cho=0;
	For(i,1,n)	if (maybe[i]>maybe[cho]&&!mark[i])	cho=i;
	return cho;
}
int main(){
	freopen("tallest.in","r",stdin);
	freopen("baoli.out","w",stdout); 
	n=read();	I=read();	H=read();	R=read();
	For(i,1,n)	maybe[i]=H;	maybe[0]=-inf;
	For(i,1,R){
		ll y=read(),x=read();
		if (x<y)	change(x+1,y,1);
		else	change(y,x-1,1);
		insert(x,y);
	}
	For(tmp,1,n){
		ll x=find();	truly[x]=maybe[x];
		for(ll i=head[x];i;i=next[i]){
			maybe[vet[i]]=min(maybe[vet[i]],maybe[x]);
			if (x<vet[i])	For(j,x+1,vet[i]-1)	maybe[j]=min(maybe[j],maybe[vet[i]]-1);
			else			For(j,vet[i]+1,x-1)	maybe[j]=min(maybe[j],maybe[vet[i]]-1);
			if (x<vet[i])	change(x+1,vet[i],-1);
			else	change(vet[i],x-1,-1);
		}
		maybe[x]=-inf;
	}
	For(i,1,n)	writeln(truly[i]);
}

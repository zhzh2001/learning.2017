#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int
#define maxn 400010
#define inf 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll head[maxn],next[maxn],vet[maxn],n,m,mark[maxn],truly[maxn],H,I,R,tot;
struct xian_duan_shu{
	ll mn[maxn],lazy[maxn],cho[maxn];
	void updata(ll p){
		if (mn[p<<1]+lazy[p<<1]<mn[p<<1|1]+lazy[p<<1|1])	cho[p]=cho[p<<1],mn[p]=mn[p<<1]+lazy[p<<1];
		else						cho[p]=cho[p<<1|1],mn[p]=mn[p<<1|1]+lazy[p<<1|1];
	}
	void pushdown(ll p){
		lazy[p<<1]+=lazy[p];	lazy[p<<1|1]+=lazy[p];
		lazy[p]=0;
	}
	void build(ll l,ll r,ll p){
		if (l==r){	cho[p]=l;	mn[p]=mark[l];	return;	}
		ll mid=(l+r)>>1;
		build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
		updata(p);
	}
	void change(ll l,ll r,ll p,ll s,ll t,ll v){
		if (s>t)	return;
		if (l==s&&r==t){	lazy[p]+=v;	return;	}
		ll mid=(l+r)>>1;
		pushdown(p);
		if (t<=mid)	change(l,mid,p<<1,s,t,v);
		else	if (s>mid)	change(mid+1,r,p<<1|1,s,t,v);
		else	change(l,mid,p<<1,s,mid,v),change(mid+1,r,p<<1|1,mid+1,t,v);
		updata(p);
	}
	ll query(){	return mn[1]+lazy[1];	}
	ll zyy(){	return cho[1];	}
}a;
struct szb{
	ll mx[maxn],lazy[maxn],cho[maxn];
	void updata(ll p){
		if (min(mx[p<<1],lazy[p<<1])>min(mx[p<<1|1],lazy[p<<1|1]))	cho[p]=cho[p<<1],mx[p]=min(mx[p<<1],lazy[p<<1]);
		else						cho[p]=cho[p<<1|1],mx[p]=min(mx[p<<1|1],lazy[p<<1|1]);
	}
	void pushdown(ll p){
		lazy[p<<1]=min(lazy[p],lazy[p<<1]);	lazy[p<<1|1]=min(lazy[p],lazy[p<<1|1]);
		lazy[p]=inf;
	}
	void build(ll l,ll r,ll p){
		lazy[p]=inf;
		if (l==r){	cho[p]=l;	mx[p]=mark[l]==inf?H:-inf;	return;	}
		ll mid=(l+r)>>1;
		build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
		updata(p);
	}
	ll query(){	return cho[1];	}
	ll qqq(ll l,ll r,ll p,ll x){
		if (l==r)	return min(H,lazy[p]);
		pushdown(p);
		ll mid=(l+r)>>1;
		ll ans=x<=mid?qqq(l,mid,p<<1,x):qqq(mid+1,r,p<<1|1,x);
		updata(p);
		return ans;
	}
	void change(ll l,ll r,ll p,ll s,ll t,ll v){
		if (s>t)	return;
		if (l==s&&r==t){	lazy[p]=min(v,lazy[p]);	return;	}
		pushdown(p);
		ll mid=(l+r)>>1;
		if (t<=mid)	change(l,mid,p<<1,s,t,v);
		else	if (s>mid)	change(mid+1,r,p<<1|1,s,t,v);
		else	change(l,mid,p<<1,s,mid,v),change(mid+1,r,p<<1|1,mid+1,t,v);
		updata(p);
	}
	void add(ll l,ll r,ll p,ll v){
		if (l==r){	mx[p]=H;	return;		}
		pushdown(p);
		ll mid=(l+r)>>1;
		v<=mid?add(l,mid,p<<1,v):add(mid+1,r,p<<1|1,v);
		updata(p);
	}
}b;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void change(ll x,ll y,ll v){
	mark[x]++;	mark[y+1]--;
}
int main(){
	freopen("tallest.in","r",stdin);
	freopen("tallest.out","w",stdout); 
	n=read();	I=read();	H=read();	R=read();
	For(i,1,R){
		ll y=read(),x=read();
		x<y?change(x+1,y,1):change(y,x-1,1);
		insert(x,y);
	}
	For(i,1,n)	mark[i]+=mark[i-1];
	For(i,1,n)	if (!mark[i])	mark[i]=inf;
	a.build(1,n,1);	b.build(1,n,1);
	For(tmp,1,n){
		ll x=b.query();	truly[x]=b.qqq(1,n,1,x);
		for(ll i=head[x];i;i=next[i]){
			b.change(1,n,1,vet[i],vet[i],min(b.qqq(1,n,1,x),b.qqq(1,n,1,vet[i])));
			if (x<vet[i])	b.change(1,n,1,x+1,vet[i]-1,b.qqq(1,n,1,vet[i])-1);
			else			b.change(1,n,1,vet[i]+1,x-1,b.qqq(1,n,1,vet[i])-1);
			x<vet[i]?a.change(1,n,1,x+1,vet[i],-1):a.change(1,n,1,vet[i],x-1,-1);
			while(!a.query()){
				b.add(1,n,1,a.zyy());	a.change(1,n,1,a.zyy(),a.zyy(),inf);
			}//�����½ڵ� 
		}
		b.change(1,n,1,x,x,-inf);
	}
	For(i,1,n)	writeln(truly[i]);
}
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 
//���汣��RP++ 

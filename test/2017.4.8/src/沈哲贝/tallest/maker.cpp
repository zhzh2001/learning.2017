#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#include<ctime>
#define ll int
#define mod 1000005
#define For(i,j,k)	for(ll i=j;i<=k;i++)
#define FOr(i,j,k)	for(ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define inf 2100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    printf("\n");
}
ll h[100000];
int main(){
	srand(time(0));
	freopen("tallest.in","w",stdout);
	ll n=10,I=rand()%n+1,H=rand()%10000000+1,R=10;
	For(i,1,n)	h[i]=rand()%H+1;
	printf("%d %d %d %d\n",n,I,H,R);
	while(R--){
		while(1){
			ll x=rand()%n+1,y=rand()%n+1;
			while(x==y)	x=rand()%n+1,y=rand()%n+1;
			if (h[x]>h[y])	swap(x,y);
			bool flag=1;
			if (x<y){	For(i,x+1,y-1)	if (h[i]>=h[y])	flag=0;	}
			else	{	For(i,y+1,x-1)	if (h[i]>=h[y])	flag=0;	}
			if (flag){
				printf("%d %d\n",x,y);break;
			}
		}
	}
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<ctime> 
#define ll long long
#define inf 2100000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 8000100
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll fa[maxn],n,m;
ll find(ll x){
	return x==fa[x]?x:fa[x]=find(fa[x]);
}
int main(){
	freopen("pro1.in","w",stdout);
	srand(time(0));
	int a[2];
	memset(a,127,sizeof a);
	n=100000;	m=100000;
	printf("%lld %lld\n",n,m);
	For(i,1,n)	printf("%lld ",rand()%a[0]);
	puts("");
	printf("%lld 0\n",rand()%n+1);
	For(i,1,n)	fa[i]=i;
	For(i,2,n){
		ll x=rand()%n+1,y=rand()%n+1;
		while(find(x)==find(y))	x=rand()%n+1,y=rand()%n+1;
		printf("%lld %lld\n",x,y);
		fa[find(x)]=find(y);
	}
	while(m--){
		ll opt=rand()%2+1;	printf("%lld ",opt);
		if (opt==1)	printf("%lld %lld\n",rand()%n+1,rand()%a[0]);
		else{
			ll x=rand()%n+1;
			printf("%lld %lld\n",x,rand()%(n-x+1)+x);
		}
	}
}

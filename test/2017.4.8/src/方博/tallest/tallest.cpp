#include<bits/stdc++.h>
using namespace std;
int a[100005];
int b[100005];
int c[100005];
map<int,int> mmp[100005];
int n,l,h,r;
int main()
{
	freopen("tallest.in","r",stdin);
	freopen("tallest.out","w",stdout);
	scanf("%d%d%d%d",&n,&l,&h,&r);
	a[l]=h;
	for(int i=1;i<=r;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		if (mmp[x][y]||mmp[y][x]) continue;
		mmp[x][y]=mmp[y][x]=1;
		if(x<=l&&y<=l){
			if(x<y){
				b[x+1]--;
				b[y]++;
			}
			else{
				b[y+1]--;
				b[x]++;
			}
		}
		else{
			if(x<y){
				c[x]--;
				c[y-1]++;
			}
			else {
				c[y]--;
				c[x-1]++;
			}
		}
	}
	for(int i=l;i>=1;i--)
		a[i-1]=a[i]-b[i];
	for(int i=l;i<=n;i++)
		a[i+1]=a[i]+c[i];
	for(int i=1;i<=n;i++)
		printf("%d\n",a[i]);
	return 0;
}

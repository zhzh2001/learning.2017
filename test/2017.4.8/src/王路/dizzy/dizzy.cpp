#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <vector>
#include <map>
#include <queue>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 1e5 + 5;
int n, m1, m2;
int head[N], cnt;
struct edge {
	int to, nxt;
} e[N * 4];

map<pair<int, int> , bool> mat;

void AddEdge(int u, int v) {
	e[cnt].to = v;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

bool vis[N], visp[N];

map<pair<int, int>, bool> matt;

void work(int t) {
	int now = t;
	queue<int> q;
	q.push(now);
	vis[now] = true;
	visp[now] = true;
	while (!q.empty()) {
		int k = q.front();
		q.pop();
		for (int i = head[k]; ~i; i = e[i].nxt) {
			if (!vis[i]) {
				vis[i] = true;
				visp[i] = true;
				q.push(i);
			}
		}
		for (int i = 1; i <= n; i++) {
			if (!vis[i] && mat.find(make_pair(k, i)) != mat.end() && matt[make_pair(k, i)] == false) {
				q.push(i);
				matt[make_pair(k, i)] = true;
				vis[i] = true;
				visp[i] = true;
			}
		}
		vis[k] = false;
	}
}

int main() {
	freopen("dizzy.in", "r", stdin);
	freopen("dizzy.out", "w", stdout);
	read(n), read(m1), read(m2);
	int u, v;
	memset(head, -1, sizeof head);
	for (int i = 1; i <= m1; i++) {
		read(u), read(v);
		AddEdge(u, v);
	}
	for (int i = 1; i <= m2; i++) {
		read(u), read(v);
		mat[make_pair(u,v)] = true;
	}
	
	for (int i = 1; i <= n; i++) {
		if (!visp[i])
			work(i);
	}
	puts("-1");
	return 0;
}
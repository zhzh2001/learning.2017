#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 1e5 + 5;
int n, m1, m2;
int head[N], cnt;
struct edge {
	int from, to, nxt, flag;
} e[N * 4], ebak[N * 4];
int lk[N * 4];

void AddEdge(int u, int v, int flag) {
	e[++cnt].from = u;
	e[cnt].to = v;
	e[cnt].flag = flag;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

bool vis[N];

bool check() {
	for (int i = 1; i <= n; i++)
		if (!vis[i]) return false;
	return true;
}

void dfs(int now) {
	vis[now] = true;
	if (check()) {
		for (int i = 1; i <= n; i++) {
			if (e[i].flag == 2)
				printf("%d %d\n", e[i].from, e[i].to);
		}
		exit(0);
	}
	for (int i = head[now]; ~i; i = e[i].nxt) {
		if (e[i].flag == 1) dfs(e[i].to);
		else if (e[i].flag == 2 && !vis[e[i].to]) {
			e[lk[i]].flag = 0;
			e[i].flag = 1;
			dfs(e[i].to);
			e[lk[i]].flag = 0;
			e[i].flag = 1;
			dfs(e[i].to);
			
		}
	}
	vis[now] = false;
}

int main() {
	freopen("dizzy.in", "r", stdin);
	freopen("dizzy.out", "w", stdout);
	read(n), read(m1), read(m2);
	int u, v;
	memset(head, -1, sizeof head);
	for (int i = 1; i <= m1; i++) {
		read(u), read(v);
		AddEdge(u, v, 1);
	}
	for (int i = 1; i <= m2; i++) {
		read(u), read(v);
		AddEdge(u, v, 2);
		lk[cnt] = cnt + 1;
		AddEdge(v, u, 2);
		lk[cnt] = cnt - 1;
	}
	memcpy(ebak, e, sizeof e);
	for (int i = 1; i <= n; i++) {
		// memset(vis, 0, sizeof vis);
		// memcpy(e, ebak, sizeof ebak);
		dfs(i);
		break;
	}
	puts("-1");
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <map>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 1e5 + 5;
const int R = 1e4 + 5;
int n, l, h, r;

int height[N];

void debug() {
	for (int i = 1; i <= n; i++)
		cout << height[i] << ' ';
	cout << endl;
}

int main() {
	freopen("tallest.in", "r", stdin);
	freopen("tallest.out", "w", stdout);
	read(n);
	read(l), read(h), read(r);
	height[l] = h;
	int u, v;
	for (int i = 1; i <= r; i++) {
		read(u), read(v);
		height[v] = (height[v] == 0) ? h : height[v];
		height[u] = (height[u] == 0) ? height[v] : height[u];
		if (u > v) swap(u, v);
		for (int j = u + 1; j < v; j++)  height[j] = (height[j] == 0) ? height[v] - 1 : height[j] - 1;
	}
	for (int i = 1; i <= n; i++) printf("%d\n", height[i] == 0 ? h : height[i]);
	return 0;
}
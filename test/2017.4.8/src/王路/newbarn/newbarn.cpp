#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <map>
#include <cmath>
#include <cassert>
#include <ctime>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 1e4 + 5;
const int inf = 0x3f3f3f3f;
int x[N], y[N];
int X1 = inf, X2 = -inf, Y1 = inf, Y2 = -inf;
map<pair<int, int> , bool> mat;

int main() {
	freopen("newbarn.in", "r", stdin);
	freopen("newbarn.out", "w", stdout);
	int n;
	read(n);
	for (int i = 1; i <= n; i++) {
		read(x[i]), read(y[i]);
		mat[make_pair(x[i], y[i])] = true;
		X1 = min(x[i], X1);
		X2 = max(x[i], X2);
		Y1 = min(y[i], Y1);
		Y2 = max(y[i], Y2);
	}
	X1--, X2++, Y1--, Y2++;
	if ((long long)abs(X2 - X1) * abs(Y2 - Y1) * n <= 1e7) {
		int ans = inf, cnt = 0;
		for (int i = X1; i <= X2; i++)
			for (int j = Y1; j <= Y2; j++) {
				if (mat.find(make_pair(i, j)) == mat.end()) {
					int res = 0;
					for (int k = 1; k <= n; k++) {
						res += abs(x[k] - i) + abs(y[k] - j);
						if (res > ans) break;
					}
					if (res < ans) {
						ans = res;
						cnt = 1;
					} else if (res == ans) cnt++;
				}
			}
		printf("%d %d\n", ans, cnt);
		return 0;
	}
	srand(233 / 10);
	int XX = X2 - X1;
	int YY = Y2 - Y1;
	int ans = inf, cnt = 0;
	for (int i = 1; i <= 5e4; i++) {
		int x = rand()%XX + X1;
		int y = rand()%YY + Y1;
		int res = 0;
		for (int k = 1; k <= n; k++) {
			res += abs(::x[k] - x) + abs(::y[k] - y);
			if (res > ans) break;
		}
		if (res < ans) ans = res, cnt = 1;
		else if (res == ans) cnt++;
	}
	printf("%d %d\n", ans, cnt);
	return 0;
}
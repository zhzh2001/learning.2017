#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define Ll long long
using namespace std;
struct tree{
	int l,r,tag,ma;
}T[400005];
int a[100005];
int n,m,x,y,z,I,h,mi;
void maketree(int l,int r,int num){
	T[num].l=l;
	T[num].r=r;
	if(l==r){
		T[num].ma=h;
		return;
	}
	int mid=(l+r)/2;
	maketree(l,mid  ,num*2  );
	maketree(mid+1,r,num*2+1);
	T[num].ma=max(T[num*2].ma,T[num*2+1].ma);
}
void pushdown(int num){
	int x=num*2;
	int y=num*2+1;
	T[x].ma+=T[num].tag;
	T[y].ma+=T[num].tag;
	T[x].tag+=T[num].tag;
	T[y].tag+=T[num].tag;
	T[num].tag=0;
}
int get(int l,int r,int num){
	pushdown(num);
	if(l<=T[num].l&&T[num].r<=r)return T[num].ma;
	int ans=0;
	num=num*2;
	if(T[num  ].r>=l)ans=max(ans,get(l,r,num  ));
	if(T[num+1].l<=r)ans=max(ans,get(l,r,num+1));
	return ans;
}
void change(int l,int r,int v,int num){
	pushdown(num);
	if(l<=T[num].l&&T[num].r<=r){
		T[num].tag+=v;
		T[num].ma+=v;
		return;
	}
	num=num*2;
	if(T[num  ].r>=l)change(l,r,v,num  );
	if(T[num+1].l<=r)change(l,r,v,num+1);
	T[num/2].ma=max(T[num].ma,T[num+1].ma);
}
int main()
{
	freopen("tallest.in","r",stdin);
	freopen("tallest.out","w",stdout);
	scanf("%d%d%d%d",&n,&I,&h,&m);
	maketree(1,n,1);
	while(m--){
		scanf("%d%d",&x,&y);
		mi=get(x,x,1);
		if(x>y)swap(x,y);
		int l=x+1,r=y-1;
		if(r<l)continue;
		int ma=get(l,r,1);
		int c=ma-(mi-1);
		if(c<=0)continue;
		change(l,r,-c,1);
	}
	for(int i=1;i<=n;i++)printf("%d\n",get(i,i,1));
/*	scanf("%d",&n);
	h=0;
	maketree(1,n,1);
	while(1){
		scanf("%d%d%d",&x,&y,&h);
		if(h)change(x,y,h,1);else printf("%d\n",get(x,y,1));
	}*/
}

#include <bits/stdc++.h>
#define N 100020
using namespace std;
int mx[N<<2], tag[N<<2], ls[N<<2], rs[N<<2];
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void push_down(int x){
	if(!tag[x]||ls[x]==rs[x]) return;
	tag[x<<1]+=tag[x], mx[x<<1]+=tag[x];
	tag[x<<1|1]+=tag[x], mx[x<<1|1]+=tag[x];
	tag[x] = 0;
}
void push_up(int x){
	mx[x] = max(mx[x<<1], mx[x<<1|1]);
}
void build(int x, int l, int r, int val){
	ls[x] = l, rs[x] = r;
	if(l == r){
		mx[x] = tag[x] = val;
		return;
	}
	int mid = l + r >> 1;
	build(x<<1, l, mid, val);
	build(x<<1|1, mid+1, r, val);
	mx[x] = val;
}
void add(int x, int l, int r, int val){
	push_down(x);
	if(ls[x]==l&&rs[x]==r){
		mx[x] += val, tag[x] += val;
		return;
	}
	int mid = ls[x] + rs[x] >> 1;
	if(r <= mid) add(x<<1, l, r, val);
	else if(l > mid) add(x<<1|1, l, r, val);
	else add(x<<1, l, mid, val), add(x<<1|1, mid+1, r, val);
	push_up(x);
}
int ask(int x, int l, int r){
	push_down(x);
	if(ls[x]==l&&rs[x]==r) return mx[x];
	int mid = ls[x] + rs[x] >> 1;
	if(r <= mid) ask(x<<1, l, r);
	else if(l > mid) ask(x<<1|1, l, r);
	else ask(x<<1, l, mid)+ask(x<<1|1, mid+1, r);
}
int print(int x){
	push_down(x);
	if(ls[x]==rs[x])return printf("%d\n", tag[x]);
	int mid = ls[x] + rs[x] >> 1;
	print(x<<1); print(x<<1|1);
}
int main(){
	freopen("tallest.in", "r", stdin);
	freopen("tallest.out", "w", stdout);
	int n = read(), I=read(), H=read(), R=read(),a,b,x,y;
	build(1, 1, n, H);
	while(R--){
		a=read();b=read();
		if(abs(a-b)<=1)continue;
		if(a>b) if((x=ask(1, b+1, a-1)) >= (y=ask(1, a, a)))
			add(1, b+1, a-1, y-x-1); else;
		else if((x=ask(1, a+1, b-1)) >= (y=ask(1, a, a)))
			add(1, a+1, b-1, y-x-1);
	}
	print(1);
	return 0;
}
// Small-World !!!
// SW_Wind...
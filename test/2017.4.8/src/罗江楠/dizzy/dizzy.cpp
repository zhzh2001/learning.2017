#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
queue<int> q;
struct node{
	int to, nxt;
}e[N<<1];
int vis[N], head[N], cnt, rx[N], ry[N];
bool rev[N];
int canGo(int x, int y){
	while(!q.empty())q.pop();
	memset(vis, 0, sizeof vis);q.push(x);
	while(!q.empty()){
		int x = q.front(); q.pop();
		for(int i = head[x]; i; i=e[i].nxt){
			if(vis[e[i].to]) continue;
			vis[e[i].to] = 1;
			if(e[i].to==y)return 1;
			q.push(e[i].to);
		}
	}
	return 0;
}
inline void ins(int x, int y){
	e[++cnt].to = y; e[cnt].nxt = head[x], head[x] = cnt;
}
int main(){
	freopen("dizzy.in", "r", stdin);
	freopen("dizzy.out", "w", stdout);
	int n=read(),m1=read(),m2=read();
	for(int i = 1,x,y; i <= m1; i++){
		x=read();y=read();
		ins(x, y);
	}
	for(int i = 1; i <= m2; i++)
		rx[i]=read(),ry[i]=read();
	for(int i = m2,x,y; i; i--){
		x = canGo(rx[i], ry[i]), y = canGo(ry[i], rx[i]);
		if(x&&y)return puts("-1")&0;
		if(x) ins(rx[i], ry[i]);
		else if(y) ins(ry[i], rx[i]), rev[i] = 1;
		else ins(rx[i], ry[i]);
	}
	for(int i = 1; i <= m2; i++)
		if(rev[i]) printf("%d %d\n", ry[i], rx[i]);
		else printf("%d %d\n", rx[i], ry[i]);
	return 0;
}
// 行为不检点，，，
// 场面一度非常尴尬。。。
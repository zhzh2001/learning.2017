#include <bits/stdc++.h>
#define N 10020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n, x[N], y[N], xl, xr, yl, yr, a[N], b[N];
int main(){
	freopen("newbarn.in", "r", stdin);
	freopen("newbarn.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++){
		a[i] = x[i] = read();
		b[i] = y[i] = read();
	}
	sort(x+1, x+n+1);
	sort(y+1, y+n+1);
	if(n&1) xl=xr=x[n+1>>1],yl=yr=y[n+1>>1];
	else xl=x[n>>1],xr=x[n/2+1],yl=y[n>>1],yr=y[n/2+1];
	// printf("xl=%d, xr=%d, yl=%d, yr=%d\n", xl, xr, yl, yr);
	int l = 1, r = n, ans = 0, tmp = (yr-yl+1)*(xr-xl+1);
	while(l<=r)ans+=x[r]-x[l]+y[r]-y[l],l++,r--;
	for(int i = 1; i <= n; i++)
		if(a[i]>=xl&&a[i]<=xr&&b[i]>=yl&&b[i]<=yr)
			tmp--;
	printf("%d %d\n", ans, tmp);
	return 0;
}
//	if(ljn < 200)
//		puts("QAQ");
//	else
//		puts("qwq");
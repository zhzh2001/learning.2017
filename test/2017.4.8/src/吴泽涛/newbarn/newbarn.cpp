#include<stdio.h>
#include<iostream>
using namespace std;
const int dx[4]={0,0,1,-1};
const int dy[4]={1,-1,0,0};
int n,x[10001],y[10001],ans=1e9,sum=0;
inline int iabs(int x){return ((x<0)?-x:x);}
int main(){
	freopen("newbarn.in","r",stdin);
	freopen("newbarn.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;
	for(int i=1;i<=n;i++) cin>>x[i]>>y[i];
	for(int i=1;i<=n;i++)
		for(int j=0;j<4;j++){
			int sou=0,x1=x[i]+dx[j],y1=y[i]+dy[j];bool flag=1;
			for(int k=1;k<=n;k++){
				if(x[k]==x1&&y[k]==y1){flag=0;break;}
				sou+=iabs(x1-x[k])+iabs(y1-y[k]);
				if(sou>ans){flag=0;break;}
			}
			if(!flag) continue;
			if(sou==ans) sum++;
			else ans=sou,sum=1;
		}
	cout<<ans<<' '<<sum;
	return 0;
}

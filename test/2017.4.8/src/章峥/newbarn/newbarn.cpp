#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("newbarn.in");
ofstream fout("newbarn.out");
const int N=10005,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,x[N],y[N],t[N],np[4];
typedef pair<int,int> median;
median get_median(int a[])
{
	memcpy(t,a,sizeof(t));
	nth_element(t+1,t+n/2+1,t+n+1);
	if(n&1)
		return make_pair(t[n/2+1],t[n/2+1]);
	return make_pair(*max_element(t+1,t+n/2+1),t[n/2+1]);
}
int calc(int x,int y)
{
	int dist=0;
	for(int i=1;i<=n;i++)
		dist+=abs(x-::x[i])+abs(y-::y[i]);
	return dist;
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	median xm=get_median(x),ym=get_median(y);
	int ans=calc(xm.first,ym.first),cnt=(xm.second-xm.first+1)*(ym.second-ym.first+1);
	for(int i=1;i<=n;i++)
		cnt-=x[i]>=xm.first&&x[i]<=xm.second&&y[i]>=ym.first&&y[i]<=ym.second;
	if(!cnt)
	{
		ans=1e9;
		for(int i=0;i<4;i++)
		{
			int now=calc(xm.first+dx[i],ym.first+dy[i]);
			if(now<ans)
				ans=now,cnt=0;
			if(now==ans)
				cnt++;
		}
	}
	fout<<ans<<' '<<cnt<<endl;
	return 0;
}
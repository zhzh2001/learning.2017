#include<fstream>
#include<queue>
using namespace std;
ifstream fin("dizzy.in");
ofstream fout("dizzy.out");
const int N=100005;
int head[N],v[N],nxt[N],e,into[N],t[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int n,m1,m2;
	fin>>n>>m1>>m2;
	while(m1--)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		into[v]++;
	}
	queue<int> Q;
	for(int i=1;i<=n;i++)
		if(!into[i])
			Q.push(i);
	int now=0;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		t[k]=++now;
		for(int i=head[k];i;i=nxt[i])
			if(!--into[v[i]])
				Q.push(v[i]);
	}
	while(m2--)
	{
		int u,v;
		fin>>u>>v;
		if(t[u]<t[v])
			fout<<u<<' '<<v<<endl;
		else
			fout<<v<<' '<<u<<endl;
	}
	return 0;
}
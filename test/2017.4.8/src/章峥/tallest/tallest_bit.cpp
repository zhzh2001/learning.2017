#include<fstream>
#include<queue>
using namespace std;
ifstream fin("tallest.in");
ofstream fout("tallest.out");
const int N=100005,M=10005;
int head[N],l[M],r[M],nxt[M],e,f[N];
inline void add_edge(int u,int l,int r)
{
	::l[++e]=l;
	::r[e]=r;
	nxt[e]=head[u];
	head[u]=e;
}
int n;
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	int i,h,m;
	fin>>n>>i>>h>>m;
	while(m--)
	{
		int a,b;
		fin>>a>>b;
		if(a+1<b)
		{
			add_edge(a,a+1,b-1);
			T.modify(a+1,1);
			T.modify(b,-1);
		}
		else
			if(b+1<a)
			{
				add_edge(a,b+1,a-1);
				T.modify(b+1,1);
				T.modify(a,-1);
			}
	}
	queue<int> Q;
	for(int i=1;i<=n;i++)
		if(!T.query(i))
		{
			f[i]=h;
			Q.push(i);
		}
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=head[k];i;i=nxt[i])
		{
			T.modify(l[i],-1);
			T.modify(r[i]+1,1);
			for(int j=l[i];j<=r[i];j++)
				if(!T.query(j))
				{
					f[j]=f[k]-1;
					Q.push(j);
				}
		}
	}
	for(int i=1;i<=n;i++)
		fout<<f[i]<<endl;
	return 0;
}
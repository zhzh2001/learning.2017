#include<iostream>
#include<cmath>
#include<cstring>
using namespace std;

int n,m,t,ans=0;
int s[1000],x[1000],y[1000],v[1000];
bool b[1000];

void dfs(int k,int num){
	if (k==m){
		if (t!=num) return;
		int l=0,r=n-1;
		for (int i=0;i<n;i++)
			if (b[i]==1){
				if (x[i]>l) l=x[i];
				if (y[i]<r) r=y[i];
		}
		ans=max(s[r]-s[l],ans);
		return;
	}
	if (num>t) return;
	b[k]=1;
	dfs(k+1,num+1);
	b[k]=0;
	dfs(k+1,num);
}

int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	cin>>n>>t>>m;
	for (int i=0;i<n;i++) cin>>v[i];
	s[0]=v[0];
	for (int i=1;i<n;i++) s[i]=s[i-1]+v[i];
	for (int i=0;i<m;i++)
		cin>>x[i]>>y[i];
	dfs(0,0);
	cout<<ans<<endl;
	return 0;
}

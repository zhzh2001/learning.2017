#include<bits/stdc++.h>
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int T,n;
int main(){
	freopen("dt.in","r",stdin);
	freopen("dt.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		if(n%7==0||n%7==2) puts("B");
		else puts("A");
	}
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,k,m;
ll a[N];
int h,t;
struct data{
	int l,r;
	bool operator <(const data &a)const{
		return (l==a.l)?r>a.r:l<a.l;
	}
}b[N];
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();k=read();m=read();
	For(i,1,n) a[i]=read();
	For(i,1,n) a[i]=a[i]+a[i-1];
	For(i,1,m) b[i]=(data){read(),read()};
	sort(b+1,b+1+m);h=1;t=0;
//	For(i,1,m) cout<<b[i].l<<" "<<b[i].r<<endl;
	For(i,1,m){
		while(h<=t&&b[h].r<b[i].l) h++;
		int j,p=b[i].r;
		for(j=t;j>=h&&p<b[j].r;j--) b[j+1].r=b[j].r;
		b[j+1].r=p;t++;
/*		For(i,1,m) cout<<b[i].l<<" "<<b[i].r<<endl;
		cout<<endl;*/
	}
	ll ans=0;
	For(i,1,m) if(i+k-1<=m){
		if(b[i].r>=b[i+k-1].l) ans=max(ans,a[b[i].r]-a[b[i+k-1].l-1]);
	}
	printf("%lld\n",ans);
	return 0;
}

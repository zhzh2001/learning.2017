#include<bits/stdc++.h>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline LL read() {
    LL x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline void write(LL x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(LL x) {
	write(x); putchar('\n');
}

const int N = 100011; 
int n,k,m,num,l,r; 
LL sum[N],ans;
LL del[N];
struct node{
	int l,r;
}a[N];

int main()
{
	freopen("drive.in","r",stdin); 
	freopen("drive.out","w",stdout); 
	n = read(); k = read(); m = read(); 
	For(i, 1, n) sum[i] = sum[i-1]+read(); 
	sum[n+1] = sum[n]; del[n+1] = 0; 
	For(i, 1, m) {
		a[i].l = read(), a[i].r = read(); 
		del[a[i].l]++; del[a[i].r+1]--; 
	}
	For(i, 1, n) del[i]+=del[i-1]; 
	if(n<=300) {
	For(i, 1, m) 
	For(i, 1, n) 
	  For(j, i, n) {
	  	num = 0; 
	   For(x, 1, m) 
	    if( a[x].l<=i && j<=a[x].r ) ++num; 
	   LL t = sum[j]-sum[i-1]; 
	   if(num>=k) ans=ans<t?t:ans; 
	   else break;  
	   }
	printf("%lld\n",ans); 
	}
	else {
		for(l=1; l<=n; l++) 
		if(del[l] == k) {
			for(r=l+1; r<=n+1; r++)       
				if(del[r] < k) break;  
			ans = max(ans, sum[r-1]-sum[l-1]); 
			l = r;
		}
	printf("%lld\n",ans); 	
	}
	return 0;
}









#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;

long long a[100000+5],l[100000+5],r[100000+5],ri[100000+5],sum[100000+5];
long long n,m,i,j,k,ans;

void qsort(long long q,long long p){
	int x,y,i,j;
	i=q;j=p;
	x=l[(q+p)/2];
	do{
		while(l[i]<x)++i;
		while(x<l[j])--j;
		if(i<=j){
			y=l[i];
			l[i]=l[j];
			l[j]=y;
			y=r[i];
			r[i]=r[j];
			r[j]=y;
			++i;
			--j;
		}
	}while(i<=j);
	if(q>j)qsort(q,j);
	if(i<p)qsort(i,p);
}

int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	cin>>m>>k>>n;
	for(i=1;i<=m;i++){
		cin>>a[i];
		sum[i]=sum[i-1]+a[i];
	}
	for(i=1;i<=n;i++)cin>>l[i]>>r[i];
	qsort(1,n);
	for(i=k;i<=n;i++){
		ri[i]=i-k+1;
		for(j=i-k+1;j<=i;j++){
			if(r[j]<r[ri[i]])ri[i]=j;
		}
	}
	for(i=k;i<=n;i++){
		ans=max(ans,sum[r[ri[i]]]-sum[l[i]-1]);
	}
	cout<<ans;
	return 0;
}

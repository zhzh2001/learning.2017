#include<cstdio>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=100005;
int f[maxn];
bool vis[maxn];
int mex(int x,int y,int z){
	vis[x]=vis[y]=vis[z]=1; int ans;
	for (int i=0;i<=100000;i++){
		if (!vis[i]) {
			ans=i;
			break;
		}
	}
	vis[x]=vis[y]=vis[z]=0;
	return ans; 
}
int main(){
	int T=read();
	f[2]=1;
	for (int i=5;i<=100005;i++){
		f[i]=mex(f[i-1],f[i-3],f[i-4]);
	} 
	for (int i=1;i<=10000;i++){
		if (f[i]==0) printf("%d ",i);
	}
	while (T--){
		int n=read();
		if (f[n]==0){
			puts("A");
		}else{
			puts("B");
		}
	}
	return 0;
}

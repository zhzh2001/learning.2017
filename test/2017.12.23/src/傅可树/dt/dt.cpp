#include<cstdio>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
int main(){
	freopen("dt.in","r",stdin);
	freopen("dt.out","w",stdout);
	int T=read();
	while (T--){
		int n=read();
		if (n%4==2){
			puts("B");
		}else{
			puts("A");
		}
	}
	return 0;
}

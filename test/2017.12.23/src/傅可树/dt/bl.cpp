#include <cstdio>
using namespace std;
const int maxn = 100005;
int dp[maxn];
int main()
{
	freopen("dt.in", "r", stdin);
	freopen("bl.out", "w", stdout);
	dp[2] = 0;
	dp[1] = dp[3] = dp[4] = 1;
	for (int i = 5; i <= 100000; i++)
	{
		int x = 0;
		x = (!dp[i - 1]) || (!dp[i - 3]) || (!dp[i - 4]);
		dp[i] = x;
	}
	for (int i = 1; i <= 100000; i++)
	{
		if (!dp[i])
			printf("%d ", i);
	}
	int T;
	scanf("%d", &T);
	while (T--)
	{
		int n;
		scanf("%d", &n);
		if (dp[n])
		{
			puts("A");
		}
		else
		{
			puts("B");
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
typedef long long ll;
const int maxn=100005;
struct seg{
	int l,r;
}b[maxn];
int n,m,lim,q[maxn];
ll ans,s[maxn];
inline bool cmp1(seg a,seg b){
	if (a.r==b.r) return a.l>b.l;
	return a.r<b.r;
}
inline bool cmp2(seg a,seg b){
	if (a.l==b.l) return a.r<b.r;
	return a.l<b.l; 
}
inline void init(){
	n=read(); lim=read(); m=read();
	for (int i=1;i<=n;i++) q[i]=read(),s[i]=s[i-1]+q[i];
	for (int i=1;i<=m;i++){
		b[i]=(seg){read(),read()};
	}
}
struct node{
	int sum;
}a[maxn*4];
inline void pushup(int k){
	a[k].sum=a[k<<1].sum+a[k<<1|1].sum;
}
void update(int k,int l,int r,int x){
	if (l==r){
		a[k].sum++;
		return;
	}
	int mid=(l+r)>>1;
	if (mid>=x) update(k<<1,l,mid,x);
		else update(k<<1|1,mid+1,r,x);
	pushup(k);
}
int query(int k,int l,int r,int x){
	if (l==r){
		return l;
	}
	int mid=(l+r)>>1;
	if (a[k<<1].sum>=x) return query(k<<1,l,mid,x);
		else return query(k<<1|1,mid+1,r,x-a[k<<1].sum);
}
void build(int k,int l,int r){
	a[k].sum=0;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r);
}
inline ll ss(int l,int r){
	if (l>r) return 0;
	return s[r]-s[l-1];
}
inline void solve1(){
	sort(b+1,b+1+m,cmp1);
	for (int i=m;i>=m-lim+2;i--){
		update(1,1,n,b[i].l);
	}
	for (int i=m-lim+1;i;i--){
		int x=query(1,1,n,lim-1);
		int r=b[i].r,l=max(b[i].l,x);
		ans=max(ans,ss(l,r));
		update(1,1,n,b[i].l);
	}
}
inline void solve2(){
	sort(b+1,b+1+m,cmp2);
	for (int i=1;i<lim;i++){
		update(1,1,n,b[i].r);
	}
	for (int i=lim;i<=m;i++){
		int x=query(1,1,n,i-lim+1);
		int l=b[i].l,r=min(b[i].r,x);
		ans=max(ans,ss(l,r));
		update(1,1,n,b[i].r);
	}
}
inline void solve(){
	solve1(); build(1,1,n); solve2();
	printf("%lld\n",ans);
}
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
typedef long long ll;
const int maxn=100005;
struct seg{
	int l,r;
}b[maxn];
int n,m,k,a[maxn];
ll s[maxn];
inline void init(){
	n=read(); k=read(); m=read();
	for (int i=1;i<=n;i++) a[i]=read(),s[i]=s[i-1]+a[i];
	for (int i=1;i<=m;i++){
		b[i]=(seg){read(),read()};
	}
//	sort(b+1,b+1+m,cmp);
}
const int inf=1e9;
int q[maxn];
ll ans;
inline void work(){
	int mx=-inf,mn=inf;
	for (int i=1;i<=k;i++){
		mn=min(b[q[i]].r,mn);
		mx=max(b[q[i]].l,mx);
	}
	ans=max(ans,s[mn]-s[mx-1]);
}
void dfs(int x,int y){
	if (x==n+1){
		if (y==k){
			work();
		}
		return;
	}
	dfs(x+1,y); 
	q[y+1]=x;
	dfs(x+1,y+1);
}
inline void solve(){
	dfs(1,0); printf("%lld\n",ans);
}
int main(){
	freopen("drive.in","r",stdin);
	freopen("baoli.out","w",stdout);
	init();
	solve();
	return 0;
}

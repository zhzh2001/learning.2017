#include<cstdio>
using namespace std;
const int maxn=20;
int l1,l2,n,opt[maxn];
inline void init(){
	scanf("%d%d%d",&n,&l1,&l2);
	for (int i=1;i<=l1;i++){
		int x;
		scanf("%d",&x); x++;
		opt[x]=1;
	}
	for (int i=1;i<=l2;i++){
		int x;
		scanf("%d",&x); x++;
		opt[x]=2;
	}
}
int ans,q[maxn];
bool use[maxn];
inline bool judge(){
	for (int i=2;i<n;i++){
		if (opt[i]==1){
			if (q[i]<q[i+1]&&q[i]<q[i-1]){
				continue;
			}else{
				return 0;
			}
		}else{
			if (opt[i]==2){
				if (q[i]>q[i+1]&&q[i]>q[i-1]){
					continue;
				}else{
					return 0;
				}
			}
		}
	}
	return 1;
}
void dfs(int now){
	if (now==n+1){
		if (judge()){
			ans++;
		}
		return;
	}
	for (int i=1;i<=n;i++){
		if (!use[i]){
			use[i]=1;
			q[now]=i;
			dfs(now+1);
			use[i]=0;
		}
	}
}
inline void solve(){
	dfs(1); printf("%d\n",ans);
}
int main(){
	freopen("ak.in","r",stdin);
	freopen("baoli.out","w",stdout);
	init();
	solve();
	return 0;
}

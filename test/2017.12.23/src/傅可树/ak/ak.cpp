#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const int maxn=5005;
int l1,l2,n,opt[maxn];
inline void init(){
	scanf("%d%d%d",&n,&l1,&l2);
	for (int i=1;i<=l1;i++){
		int x;
		scanf("%d",&x); x++;
		opt[x]=1;
	}
	for (int i=1;i<=l2;i++){
		int x;
		scanf("%d",&x); x++;
		opt[x]=2;
	}
}
const int mod=1e9+7;
inline void update(ll &x,ll y){
	x=((x+y)%mod+mod)%mod;
}
ll dp[2][maxn];
inline void solve(){
	for (int i=1;i<=n;i++){
		if (opt[i]){
			if (opt[i]==opt[i-1]){
				printf("0");
				return;
			}
		}
	}
	int cur=0; dp[cur][1]=1;
	for (int i=2;i<=n;i++){
//		memset(dp[cur^1],0,sizeof(dp[cur^1]));
		for (int j=1;j<=i;j++){
			dp[cur^1][j]=dp[cur^1][j-1];
			if (opt[i-1]==1){
				update(dp[cur^1][j],dp[cur][j-1]);
			}else{
				if (opt[i-1]==2){
					update(dp[cur^1][j],dp[cur][i-1]-dp[cur][j-1]);
				}else{
					if (opt[i]==1){
						update(dp[cur^1][j],dp[cur][i-1]-dp[cur][j-1]);
					}else{
						if (opt[i]==2) {
							update(dp[cur^1][j],dp[cur][j-1]);
						}else{
							update(dp[cur^1][j],dp[cur][i-1]);
						}
					}
				}
			}
		}
		cur^=1;
	}
	printf("%lld\n",dp[cur][n]);
}
int main(){
	freopen("ak.in","r",stdin);
	freopen("ak.out","w",stdout);
	init();
	solve();
	return 0;
}

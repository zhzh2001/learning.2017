#include<bits/stdc++.h>
using namespace std;
#define ll long long
using namespace std;
const int N=5005;
const int mo=1000000007;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline ll writeln(ll x){write(x);puts("");}
int f[N],g[N];
int be[N],s[N];
int n,la,lb;
int main()
{
	freopen("ak.in","r",stdin);
	freopen("ak.out","w",stdout);
	n=read();la=read();lb=read();
	for(int i=1;i<=la;i++)be[read()+1]=1;
	for(int i=1;i<=lb;i++){
		int k=read()+1;
		if(be[k]==1){
			puts("0");
			return 0;
		}be[k]=2;
	}
	for(int i=1;i<=n;i++)
		if(be[i]==be[i+1]&&be[i]!=0){
			puts("0");
			return 0;
		}
	for(int i=1;i<=n;i++)
		if(be[i]==1)be[i+1]=2;
		else if(be[i]==2)be[i+1]=1;
		
	f[1]=1;for(int i=1;i<=n;i++)s[i]=1;
	
	for(int i=2;i<=n;i++){
		for(int j=1;j<=i;j++){
			if(be[i]==1)f[j]=((s[i]-s[j-1])%mo+mo)%mo;
			else if(be[i]==2)f[j]=s[j-1];
			else f[j]=s[i];
		}
		for(int j=1;j<=n;j++)
			s[j]=(s[j-1]+f[j])%mo;
	}
	writeln(s[n]);
	return 0;
}

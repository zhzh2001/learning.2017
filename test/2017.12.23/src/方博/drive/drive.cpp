#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pa pair<ll,ll>
#define mk make_pair
using namespace std;
const int N=100005;
const int M=200005;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline ll writeln(ll x){write(x);puts("");}
ll a[N],t[N],s[N];
set<pa> b,c,can;
set<pa>::iterator itb,itc,it,it2;
int head,tail;
ll ans;
int n,k,m;
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();k=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)s[i]=s[i-1]+a[i];
	for(int i=1;i<=m;i++){
		int x=read(),y=read();t[i]=x;
		b.insert(mk(x,i));c.insert(mk(y,i));
	}
	
	b.insert(mk(n+1,m+1));itb=b.begin();
	c.insert(mk(n+1,m+1));itc=c.begin();
	int now=0;
	
	can.insert(mk(0,0));
	it=can.begin();
	for(int i=1;i<=n;i++){
		pa ck=*itc;pa bk=*itb;
		while(ck.first<i){
			if((*it)==mk(t[ck.second],ck.second)){
				if(can.size()<=k+1)it--;
				else it++;
			}
			else if(mk(t[ck.second],ck.second)<=(*it))it++;
			can.erase(mk(t[ck.second],ck.second));
			itc++;ck=*itc;now--;
		}
		while(bk.first==i){
			can.insert(mk(bk.first,bk.second));
			if(can.size()<=k+1)it++;
			itb++;bk=*itb;now++;
		}
//		cout<<i<<' '<<can.size()<<':'<<it->first<<endl;
		if(now>=k){
//			cout<<i<<':'<<it->first<<endl;
			ans=max(ans,s[i]-s[it->first -1]);
		}
	}
	writeln(ans);
	return 0;
}

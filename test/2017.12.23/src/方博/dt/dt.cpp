#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=100005;
const int M=200005;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline ll writeln(ll x){write(x);puts("");}
int f[7]={0,1,0,1,1,1,1};
int main()
{
	freopen("dt.in","r",stdin);
	freopen("dt.out","w",stdout);
	int T=read();
	while(T--){
		if(f[read()%7])puts("A");
		else puts("B");
	}
}

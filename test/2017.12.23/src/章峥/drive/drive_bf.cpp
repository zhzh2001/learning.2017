#include<fstream>
using namespace std;
ifstream fin("drive.in");
ofstream fout("drive.ans");
const int N=1005;
int a[N];
pair<int,int> seg[N];
int main()
{
	int n,k,m;
	fin>>n>>k>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=m;i++)
		fin>>seg[i].first>>seg[i].second;
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		long long sum=0;
		for(int j=i;j<=n;j++)
		{
			sum+=a[j];
			int cnt=0;
			for(int t=1;t<=m;t++)
				if(seg[t].first<=i&&seg[t].second>=j)
					cnt++;
			if(cnt>=k)
				ans=max(ans,sum);
		}
	}
	fout<<ans<<endl;
	return 0;
}

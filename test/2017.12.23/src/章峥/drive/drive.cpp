#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("drive.in");
ofstream fout("drive.out");
const int N=100005;
long long a[N];
int tree[N*4];
pair<int,int> seg[N];
void modify(int id,int l,int r,int x)
{
	if(l==r)
		tree[id]++;
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(id*2,l,mid,x);
		else
			modify(id*2+1,mid+1,r,x);
		tree[id]=tree[id*2]+tree[id*2+1];
	}
}
int query(int id,int l,int r,int k)
{
	if(l==r)
		return l;
	int mid=(l+r)/2;
	if(tree[id*2+1]>=k)
		return query(id*2+1,mid+1,r,k);
	return query(id*2,l,mid,k-tree[id*2+1]);
}
int main()
{
	int n,k,m;
	fin>>n>>k>>m;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		a[i]+=a[i-1];
	}
	for(int i=1;i<=m;i++)
		fin>>seg[i].first>>seg[i].second;
	sort(seg+1,seg+m+1);
	long long ans=0;
	for(int i=1,j=1;i<=n;i++)
	{
		for(;j<=m&&seg[j].first==i;j++)
			modify(1,1,n,seg[j].second);
		if(j-1>=k)
			ans=max(ans,a[query(1,1,n,k)]-a[i-1]);
	}
	fout<<ans<<endl;
	return 0;
}

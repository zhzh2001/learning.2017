#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
using namespace std;
ofstream fout("drive.in");
const int n=1e5;
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> dk(1,n),dv(0,1e9);
	int k=dk(gen);
	fout<<n<<' '<<k<<' '<<n<<endl;
	for(int i=1;i<=n;i++)
		fout<<dv(gen)<<' ';
	fout<<endl;
	for(int i=1;i<=n;i++)
	{
		int l=dk(gen),r=dk(gen);
		if(l>r)
			swap(l,r);
		fout<<l<<' '<<r<<endl;
	}
	return 0;
}

#include<fstream>
#include<cstdlib>
using namespace std;
ifstream fin("ak.in");
ofstream fout("ak.ans");
const int N=5005,MOD=1e9+7;
int f[N][N],tag[N];
void noans()
{
	fout<<0<<endl;
	exit(0);
}
int main()
{
	int n,k,l;
	fin>>n>>k>>l;
	while(k--)
	{
		int x;
		fin>>x;
		x++;
		if(tag[x]==1||tag[x+1]==-1)
			noans();
		tag[x]=-1;
		tag[x+1]=1;
	}
	while(l--)
	{
		int x;
		fin>>x;
		x++;
		if(tag[x]==-1||tag[x+1]==1)
			noans();
		tag[x]=1;
		tag[x+1]=-1;
	}
	f[1][1]=1;
	for(int i=2;i<=n;i++)
		for(int j=1;j<=i;j++)
			if(tag[i]==1)
				f[i][j]=(f[i][j-1]+f[i-1][j-1])%MOD;
			else if(tag[i]==-1)
				f[i][j]=((f[i][j-1]+f[i-1][i-1]-f[i-1][j-1])%MOD+MOD)%MOD;
			else
				f[i][j]=(f[i][j-1]+f[i-1][i-1])%MOD;
	fout<<f[n][n]<<endl;
	return 0;
}

#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("ak.in");
ofstream fout("ak.ans");
const int N=15;
int a[N],b[N],p[N];
int main()
{
	int n,k,l;
	fin>>n>>k>>l;
	for(int i=1;i<=k;i++)
	{
		fin>>a[i];
		a[i]++;
	}
	for(int i=1;i<=l;i++)
	{
		fin>>b[i];
		b[i]++;
	}
	for(int i=1;i<=n;i++)
		p[i]=i;
	int ans=0;
	do
	{
		bool flag=true;
		for(int i=1;i<=k;i++)
			if(p[a[i]]>p[a[i]-1]||p[a[i]]>p[a[i]+1])
			{
				flag=false;
				break;
			}
		for(int i=1;i<=l;i++)
			if(p[b[i]]<p[b[i]-1]||p[b[i]]<p[b[i]+1])
			{
				flag=false;
				break;
			}
		ans+=flag;
	}while(next_permutation(p+1,p+n+1));
	fout<<ans<<endl;
	return 0;
}

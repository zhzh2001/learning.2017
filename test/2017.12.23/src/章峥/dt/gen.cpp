#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("dt.in");
const int t=1e5;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<t<<endl;
	for(int i=1;i<=t;i++)
	{
		uniform_int_distribution<> d(1,1e7);
		fout<<d(gen)<<endl;
	}
	return 0;
}

#include<fstream>
using namespace std;
ifstream fin("dt.in");
ofstream fout("dt.ans");
const int N=10000005;
bool f[N];
int main()
{
	f[0]=false;
	for(int i=1;i<N;i++)
	{
		f[i]=!f[i-1];
		if(i>=3)
			f[i]|=!f[i-3];
		if(i>=4)
			f[i]|=!f[i-4];
	}
	int t;
	fin>>t;
	while(t--)
	{
		int n;
		fin>>n;
		if(f[n])
			fout<<"A\n";
		else
			fout<<"B\n";
	}
	return 0;
}

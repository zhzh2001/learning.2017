program drive;
 uses math;
 var
  a:array[0..100001] of int64;
  x,y,tee:array[0..100001] of longint;
  i,k,m,n,o,p,os,ps,ms:longint;
  smax:int64;
 procedure hahasort(l,r:longint);
  var
   o,p,m,m1,t:longint;
  begin
   o:=l;
   p:=r;
   m:=x[(l+r) div 2];
   m1:=y[(o+p) div 2];
   repeat
    while (x[o]<m) or (x[o]=m) and (y[o]>m1) do inc(o);
    while (x[p]>m) or (x[p]=m) and (y[p]<m1) do dec(p);
    if o<=p then
     begin
      t:=y[o];
      y[o]:=y[p];
      y[p]:=t;
      t:=x[o];
      x[o]:=x[p];
      x[p]:=t;
      inc(o);
      dec(p);
     end;
   until o>p;
   if o<r then hahasort(o,r);
   if l<p then hahasort(l,p);
  end;
 procedure hahainc(k:longint);
  begin
   while k<=n do
    begin
     inc(tee[k]);
     inc(k,k and -k);
    end;
  end;
 function hahasum(k:longint):longint;
  begin
   hahasum:=0;
   while k>1 do
    begin
     inc(hahasum,tee[k]);
     dec(k,k and -k);
    end;
  end;
 begin
  assign(input,'drive.in');
  assign(output,'drive.out');
  reset(input);
  rewrite(output);
  readln(n,k,m);
  a[0]:=0;
  for i:=1 to n do
   begin
    read(A[i]);
    inc(a[i],a[i-1]);
   end;
  readln;
  for i:=1 to m do
   readln(x[i],y[i]);
  hahasort(1,m);
  smax:=0;
  x[m+1]:=1008208820;
  fillchar(tee,sizeof(tee),0);
  i:=1;
  while i<=m do
   begin
    p:=i;
    os:=x[i];
    ps:=n;
    while x[i]=x[p] do
     begin
      hahainc(y[i]);
      inc(i);
     end;
    o:=hahasum(n);
    while os<=ps do
     begin
      ms:=(os+ps) div 2;
      if o-hahasum(ms-1)>=k then os:=ms+1
                            else ps:=ms-1;
     end;
    smax:=max(smax,a[os-1]-a[x[p]-1]);
   end;
  writeln(smax);
  close(input);
  close(output);
 end.

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,T;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void solve()
{
	n=read();
	n%=7;
	if(n==2||n==0)cout<<"B"<<endl;
			else cout<<"A"<<endl;
}
int main()
{
	freopen("dt.in","r",stdin);
	freopen("dt.out","w",stdout);
	T=read();
	while(T--)solve(); 
	return 0;
}

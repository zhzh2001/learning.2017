#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll ans,n,k,m;
ll a[100100];
struct D{
	ll x,y,mid;
	ll a,b,c;
}c[400400];
struct E{
	ll x,y;
}b[100100];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void build(int x,int y,int z)
{
	if(x==y)
	{
		c[z].x=x;
		c[z].y=y;
		c[z].a=a[x];
		return;
	}
	c[z].x=x;
	c[z].y=y;
	c[z].mid=(x+y)/2;
	c[z].b=0;
	c[z].c=0;
	build(x,c[z].mid,z*2);
	build(c[z].mid+1,y,z*2+1);
	c[z].a=c[z*2].a+c[z*2+1].a;
}
void add(int x,int y,int z)
{
	if(x==c[z].x&&y==c[z].y)
	{
		c[z].b++;
		return;
	}
	if(x<=c[z].mid)add(x,c[z].mid,z*2);
	if(y>c[z].mid)add(c[z].mid+1,y,z*2+1);
	return;
}
void jian(int x,int y,int z)
{
	if(x==c[z].x&&y==c[z].y)
	{
		c[z].b--;
		return;
	}
	if(x<=c[z].mid)jian(x,c[z].mid,z*2);
	if(y>c[z].mid)jian(c[z].mid+1,y,z*2+1);
	return;
}
inline ll js(int x)
{
	if(c[x].b==k)return c[x].a;
	if(c[x].x==c[x].y)return 0;
	return js(x*2)+js(x*2+1);
}
inline void dfs1(int x,int y)
{
	if(y==k)
	{
		ans=max(ans,js(1));
		return;
	}
	if(y-k+x<=m)dfs1(x+1,y);
	jian(b[x].x,b[x].y,1);
	dfs1(x+1,y-1);
	add(b[x].x,b[x].y,1);
}
inline ll jsc(int x)
{
	if(c[x].c==k)return c[x].a;
	if(c[x].x==c[x].y)return 0;
	return jsc(x*2)+jsc(x*2+1);
}
void addc(int x,int y,int z)
{
	if(x==c[z].x&&y==c[z].y)
	{
		c[z].c++;
		return;
	}
	if(x<=c[z].mid)addc(x,c[z].mid,z*2);
	if(y>c[z].mid)addc(c[z].mid+1,y,z*2+1);
	return;
}
void jianc(int x,int y,int z)
{
	if(x==c[z].x&&y==c[z].y)
	{
		c[z].c--;
		return;
	}
	if(x<=c[z].mid)jianc(x,c[z].mid,z*2);
	if(y>c[z].mid)jianc(c[z].mid+1,y,z*2+1);
	return;
}
inline void dfs2(int x,int y)
{
	if(y==k)
	{
		ans=max(ans,jsc(1));
		return;
	}
	if(k-y<m-x+1)dfs2(x+1,y);
	addc(b[x].x,b[x].y,1);
	dfs2(x+1,y+1);
	jianc(b[x].x,b[x].y,1);
}
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();k=read();m=read();
	For(i,1,n)a[i]=read();
	build(1,n,1);
	For(i,1,m)
	{
		b[i].x=read();
		b[i].y=read();
		add(b[i].x,b[i].y,1);
	}
	if(k>m/2)dfs1(1,m);
		else dfs2(1,0);
	cout<<ans<<endl;
	return 0;
}


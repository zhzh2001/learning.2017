#include<set>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1e15)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,K,m,ans,sum[N];
struct data{ ll l,r; }a[N];
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read(); K=read(); m=read();
	rep(i,1,n) sum[i]=sum[i-1]+read();
	rep(i,1,m){
		a[i].l=read();
		a[i].r=read();
	}
	rep(i,1,n){
		per(j,n,i){
			if (sum[j]-sum[i-1]<=ans) break;
			ll num=0;
			rep(k,1,m) if (a[k].l<=i&&a[k].r>=j) ++num;
			if (num>=K){
				ans=max(ans,sum[j]-sum[i-1]);
				break;
			}
		}
	}
	printf("%lld",ans);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,n;
int main(){
	freopen("dt.in","r",stdin);
	freopen("dt.out","w",stdout);
	T=read();
	while (T--){
		n=(read()-1)%7+1;
		if (n==2||n==7) puts("B");
		else puts("A");
	}
}

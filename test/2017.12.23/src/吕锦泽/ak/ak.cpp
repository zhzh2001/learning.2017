#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 5005
#define ll long long
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,la,lb,now,a[N],f[2][N];
int main(){
	freopen("ak.in","r",stdin);
	freopen("ak.out","w",stdout);
	n=read(); la=read(); lb=read();
	rep(i,1,n) a[i]=1;
	rep(i,1,la) a[read()+1]=0;
	rep(i,1,lb) a[read()+1]=2;
	f[1][1]=1; now=1;
	rep(i,2,n){
		rep(j,1,i){
			f[now^1][j]=f[now^1][j-1];
			if (a[i]>a[i-1]) f[now^1][j]=(f[now^1][j]+f[now][j-1])%mod;
			else if (a[i]<a[i-1]) f[now^1][j]=(f[now^1][j]+f[now][i-1]-f[now][j-1]+mod)%mod;
			else if (a[i]==a[i-1]&&a[i]==1) f[now^1][j]=(f[now^1][j]+f[now][i-1])%mod;
		}
		now^=1;
	}
	printf("%lld",f[now][n]);
}

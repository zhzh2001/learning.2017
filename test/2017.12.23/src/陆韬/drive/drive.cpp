#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int a[100005],l[100005],r[100005];
int wl[100005],wr[100005];
long long s[100005];
int n,m,k;
long long ans=0;
void makeit(int ll,int rr,int p)
{
	if (ll==rr)
	{
		s[p]=a[ll];return;
	}
	int mid=(ll+rr)>>1;
	makeit(ll,mid,p+1);
	makeit(mid+1,rr,p+2);
	s[p]=s[p+1]+s[p+2];
}
long long ser(int ll,int rr,int sl,int sr)
{
	if (sl==sr) return a[sl];
	int mid=(sl+sr)>>1;
	long long p=0;
	if (rr<=mid) p+=ser(ll,rr,sl,mid);
	if (ll<=mid&&rr>mid) p=p+ser(ll,rr,sl,mid)+ser(ll,rr,mid+1,sr);
	if (ll>mid) p+=ser(ll,rr,mid+1,sr);
	return p;
}
void dfs(int p,int x,int ll,int rr)
{
	//cout<<ll<<' '<<rr<<endl; 
	if (p==k)
	{
		//cout<<ll<<' '<<rr<<endl;
		long long sum=ser(ll,rr,1,n);
		if (ans<sum) ans=sum;
		return;
	}
	for (int i=x;i<=m;++i)
	{
		int dl,dr;
		if (ll<l[i]) dl=l[i];else dl=ll;
		if (rr>r[i]) dr=r[i];else dr=rr;
		if (dl<=dr) dfs(p+1,i+1,dl,dr);
	}
}
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	scanf("%d%d%d",&n,&k,&m);
	for (int i=1;i<=n;++i) scanf("%d",&a[i]);
	for (int i=1;i<=m;++i) scanf("%d%d",&l[i],&r[i]);
	memset(s,0,sizeof(s));
	//makeit(1,n,0);
	if (m<=100) dfs(0,1,1,n);else
	{
		for (int i=1;i<=m;++i) wl[l[i]]++,wr[r[i]]++;
		int maxl=0,maxr=0;int ll=1,rr=n;
		for (int i=1;i<=n/2;++i)
		{
			if (maxl<=wl[i]) maxl=wl[i],ll=i;
			if (maxr<=wr[n+1-i]) maxr=wr[i],rr=n+1-i;
		}
		ans=ser(ll,rr,1,n);
	}
	printf("%lld\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cctype>
#include<iostream>
using namespace std;
const int f[3]={1,3,4};
bool vis[10005];
int T,n;
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) f^=!(ch^'-');
	for(;isdigit(ch);ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return ret*f;
}
void work(){
	vis[1]=1;vis[0]=0;vis[2]=0;
	for(int i=3;i<=10000;i++){
		bool t=1;
		for(int j=0;j<3;j++)
		if(i-f[j]>=0) t&=vis[i-f[j]];
		vis[i]=!t;
	}
}
int main(){
	freopen("dt.in","r",stdin);
	freopen("dt1.out","w",stdout);
	T=read();
	work();
	for(int i=1;i<=T;i++){
		int x=read();
		printf("%c\n",vis[x]?'A':'B');
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<iostream>
using namespace std;
const int f[3]={1,3,4};
bool vis[10005];
int T,n;
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) f^=!(ch^'-');
	for(;isdigit(ch);ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return ret*f;
}
int main(){
	freopen("dt.in","r",stdin);
	freopen("dt.out","w",stdout);
	T=read();
	for(int i=1;i<=T;i++){
		int x=read()-2;
		if(x==-1) printf("A\n");else
		if(x==0) printf("B\n");else
		printf("%c\n",(x%7==0||x%7==5)?'B':'A');
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<iostream>
#include<algorithm>
using namespace std;
const int tt=1000000007;
int n,f[5005][5005],ans,k,l,a[5005],b[5005],ai=1,bi=1;
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) f^=!(ch^'-');
	for(;isdigit(ch);ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return ret*f;
}
int main(){
	freopen("ak.in","r",stdin);
	freopen("ak.out","w",stdout);
	n=read();k=read(),l=read();
	for(int i=1;i<=k;i++) a[i]=read()+1;
	for(int i=1;i<=l;i++) b[i]=read()+1;
	sort(a+1,a+1+k);sort(b+1,b+1+l);
	f[1][1]=1;
	for(int i=2;i<=n;i++)
	for(int j=1;j<=i;j++){
		f[i][j]=f[i][j-1];
		if(i==a[ai]) f[i][j]=(f[i][j]+f[i-1][j-1])%tt,ai++;else
		if(i==b[bi]) f[i][j]=(f[i][j]+f[i-1][i-1]-f[i-1][j-1]+tt)%tt,bi++;else
		f[i][j]=(f[i][j]+f[i-1][i-1])%tt;
		printf("f[%d][%d]=%d\n",i,j,f[i][j]);
	}
	printf("%d\n",f[n][n]);
	return 0;
}


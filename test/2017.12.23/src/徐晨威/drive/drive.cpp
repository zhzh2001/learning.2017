#include<cstdio>
#include<cctype>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m,k,a[100005],X[100005],Y[100005],ans;
struct xcw{
	int x,f,id;
	bool operator <(const xcw b)const{return x<b.x||x==b.x&&X[id]<X[b.id];}
}f[100005];
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) f^=!(ch^'-');
	for(;isdigit(ch);ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return ret*f;
}
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();k=read();m=read();
	for(int i=1;i<=n;i++) a[i]=read()+a[i-1];
	for(int i=1;i<=m;i++){
		X[i]=read(),Y[i]=read();
		f[2*i-1]=(xcw){X[i],1,i},f[2*i]=(xcw){Y[i],-1,i};
	}
	sort(f+1,f+1+m*2);
	int num=0,now=1,lst=0;
	for(int i=1;i<=n;i++){
		if(num>=k) ans=max(ans,a[i]-a[f[lst].x-1]);
		for(;f[now].x==i&&now<=2*m;now++){
			num+=f[now].f;
			if(f[now].f==1&&num==k) lst=now;
			if(f[now].f==-1&&f[now].id==f[lst].id&&now>=k){
				for(;lst<=now;){
					for(lst++;f[lst].f==-1&&lst<=now;lst++);
					if(Y[f[lst].id]>=f[now].x) break;
				}
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

namespace IO {
  const int SZ = 1 << 20;
  char buf[SZ], *s = buf, *t = buf;
  inline char NextChar() {
    if (s == t) {
      t = (s = buf) + fread(buf, sizeof(char), SZ, stdin);
      if (s == t)
        return EOF;
    }
    return *s++;
  }
  template <typename T>
  inline void Read(T &x) {
    x = 0;
    T flag = 1;
    char ch;
    for (ch = NextChar(); isspace(ch); ch = NextChar())
      ;
    if (ch == '-') {
      ch = NextChar();
      flag = -1;
    }
    for (; isdigit(ch); ch = NextChar())
      x = x * 10 + ch - '0';
    x *= flag;
  }
}

using namespace IO;

const int MaxN = 100005;
int n, k, m, a[MaxN], l[MaxN], r[MaxN], c[MaxN];

int Solve1() {
  int ret = 0;
  for (int i = 0; i < (1 << m); ++i) {
    if (__builtin_popcount(i) != k) {
      continue;
    }
    for (int j = 1; j <= n; ++j) {
      c[j] = 0;
    }
    for (int j = 0; j < m; ++j) {
      if (i & (1 << j)) {
        for (int k = l[j]; k <= r[j]; ++k)
          ++c[k];
      }
    }
    int sum = 0;
    for (int j = 1; j <= n; ++j) {
      if (c[j] == k) {
        sum += a[j];
      }
    }
    ret = max(ret, sum);
  }
  printf("%d\n", ret);
  return 0;
}

int main() {
  freopen("drive.in", "r", stdin);
  freopen("drive.out", "w", stdout);
  Read(n), Read(k), Read(m);
  for (int i = 1; i <= n; ++i) {
    Read(a[i]);
  }
  for (int i = 0; i < m; ++i) {
    Read(l[i]), Read(r[i]);
  }
  if (m <= 10 && k <= 10) // 20 Points
    return Solve1();
  return 0;
}

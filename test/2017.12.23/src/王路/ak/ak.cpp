#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

#define LogMsg(...) fprintf(stderr, __VA_ARGS__)

namespace IO {
  const int SZ = 1 << 20;
  char buf[SZ], *s = buf, *t = buf;
  inline char NextChar() {
    if (s == t) {
      t = (s = buf) + fread(buf, sizeof(char), SZ, stdin);
      if (s == t)
        return EOF;
    }
    return *s++;
  }
  template <typename T>
  inline void Read(T &x) {
    x = 0;
    T flag = 1;
    char ch;
    for (ch = NextChar(); isspace(ch); ch = NextChar())
      ;
    if (ch == '-') {
      ch = NextChar();
      flag = -1;
    }
    for (; isdigit(ch); ch = NextChar())
      x = x * 10 + ch - '0';
    x *= flag;
  }
}

using namespace IO;

const int MaxN = 5005, M1N = 305, Mod = 1e9 + 7;
bool a[MaxN], b[MaxN];
long long f[M1N][M1N];
int n, la, lb;

void Update(long long &x, long long y) {
  x = ((x + y) % Mod + Mod) % Mod;
}

int Solve1() {
  bool invalid = false;
  for (int i = 1; i <= n; ++i) {
    if (a[i] && b[i])
      invalid = true;
  }
  if (invalid) {
    puts("0"); 
//    LogMsg("throw: invalid\n");
//    LogMsg("0");
    return 0;
  }
  f[1][1] = 1;
  for (int i = 1; i < n; ++i) {
    for (int j = 1; j <= i; ++j) {
      if (a[i + 1]) {
        for (int k = 1; k <= j; ++k) {
          Update(f[i + 1][k], f[i][j]);
        }
      } else if (b[i + 1]) {
        for (int k = j + 1; k <= i + 1; ++k) {
          Update(f[i + 1][k], f[i][j]);
        }
      } else {
        for (int k = 1; k <= i + 1; ++k) {
          Update(f[i + 1][k], f[i][j]);
        }
      }
    }
  }
  long long ans = 0;
  for (int i = 1; i <= n; ++i) {
    Update(ans, f[n][i]);
  }
  printf("%lld\n", ans);
//  LogMsg("throw: dp\n");
//  LogMsg("%lld\n", ans);
  return 0;
}

int Solve2() {
  return 0;
}

int main() {
  freopen("ak.in", "r", stdin);
  freopen("ak.out", "w", stdout);
  Read(n), Read(la), Read(lb);
  for (int i = 1, x; i <= la; ++i) {
    Read(x);
    a[x + 1] = true;
    b[x] = b[x + 2] = true;
  }
  for (int i = 1, x; i <= lb; ++i) {
    Read(x);
    a[x] = a[x + 2] = true;
    b[x + 1] = true;
  }
  if (n <= 300) { // 70 Points
    return Solve1();
  }
  return Solve2();
}

#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Rand() {
  return (rand() << 15) + rand();
}

int main() {
  freopen("dt.in", "w", stdout);
  srand(GetTickCount());
  ios::sync_with_stdio(false);
  int T = 100000;
  cout << T << "\n";
  while (T--) {
    int n = Rand();
    cout << n << "\n";
  }
  return 0;
}

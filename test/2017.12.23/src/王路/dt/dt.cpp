#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

bool f[10005];

int main() {
  freopen("dt.in", "r", stdin);
  freopen("dt.out", "w", stdout);
  int n, t;
  scanf("%d", &t);
  f[1] = f[3] = f[4] = true;
  f[2] = false;
  for (int i = 5; i <= 10000; ++i) {
    if (f[i - 1] && f[i - 3] && f[i - 4]) f[i] = false;
    else f[i] = true;
  }
  for (int i = 1; i <= t; ++i) {
    scanf("%d", &n);
    if (n <= 10000) { // 60 Points
      puts(f[n] ? "A" : "B");
    } else {
      puts((i % 7 == 2 || i % 7 == 0) ? "A" : "B"); // 100 Points
    }
  }
  return 0;
}

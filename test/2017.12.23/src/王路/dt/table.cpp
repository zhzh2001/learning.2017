#include <iostream>
#include <cstdio>
#include <cassert>
using namespace std;

bool f[1000005];

int main() {
  f[1] = f[3] = f[4] = true;
  f[2] = false;
  for (int i = 5; i < 1000005; ++i) {
    if (f[i - 1] && f[i - 3] && f[i - 4]) f[i] = false;
    else f[i] = true;
  }
  for (int i = 1; i <= 90; ++i) {
    cerr << (f[i] ? "*" : " ");
  }
  for (int i = 1; i <= 90; ++i) {
    cerr << ((i % 7 == 2 || i % 7 == 0) ? " " : "*");
  }
  for (int i = 1; i < 1000005; ++i) {
    if (i % 7 == 2 || i % 7 == 0)
      assert(!f[i]);
    else
      assert(f[i]);
  }
  cerr << "OK" << endl;
  return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
inline ll readll(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n,m,k;
ll c[N],Mx;
struct segment{
	int l,r;
	friend bool operator <(segment a,segment b){
		if(a.l == b.r) return a.r > b.r;
		return a.l < b.l;
	}
}a[N];
namespace Segtree{
	struct node{
		int l,r,x,Mx;
		int add;
	}tr[N*4];
	#define ls (now<<1)
	#define rs (now<<1|1)
	#define mid ((l+r)>>1)

	void build(int now,int l,int r){
		tr[now].l = l; tr[now].r = r;
		tr[now].x = tr[now].add = tr[now].Mx = 0;
		if(l == r) return;
		build(ls,l,mid);
		build(rs,mid+1,r);
	}

	inline int Max(int a,int b){
		if(a > b) return a; return b;
	}

	void pushdown(int now){
		if(tr[now].add == 0) return;
		tr[ls].add += tr[now].add;
		tr[rs].add += tr[now].add;
		tr[ls].x += tr[now].add;
		tr[rs].x += tr[now].add;
		tr[ls].Mx += tr[now].add;
		tr[rs].Mx += tr[now].add;
		tr[now].add = 0;
	}

	void updata(int now){
		if(tr[ls].x == tr[rs].x) tr[now].x = tr[ls].x;
		else tr[now].x = -1;
		tr[now].Mx = Max(tr[now].Mx,Max(tr[ls].Mx,tr[rs].Mx));
	}

	void add(int now,int x,int y){
		int l = tr[now].l,r = tr[now].r;
		if(x <= l && r <= y){
			++tr[now].x;
			++tr[now].add;
			++tr[now].Mx;
			return;
		}
		pushdown(now);
		if(x <= mid) add(ls,x,y);
		if(y > mid) add(rs,x,y);
		updata(now);
	}

	int find(int now,int last,int x){
		int l = tr[now].l,r = tr[now].r;
		if(tr[now].r < last) return 0;
		if(tr[now].Mx < k) return 0;
		if(tr[now].x >= k){
			if(r > x) return x;
			return r;
		}
		pushdown(now);
		int ans = 0;
		if(x > mid && tr[rs].Mx >= k){
			ans = find(rs,last,x);
			updata(now);
			return ans;
		}
		else{
			ans = find(ls,last,x);
			updata(now);
			return ans;
		}
	}
}
using namespace Segtree;

int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	Mx = -1;
	n = read(); k = read(); m = read();
	for(int i = 1;i <= n;++i){
		ll x = readll();
		c[i] = c[i-1]+x;
	}
	for(int i = 1;i <= m;++i){
		a[i].l = read(); a[i].r = read();
	}
	sort(a+1,a+m+1);
	Segtree:: build(1,1,n);
	for(int i = 1;i <= m;++i){
		Segtree:: add(1,a[i].l,a[i].r);
		int x = Segtree:: find(1,a[i].l,a[i].r);
		// printf("# %d %d %d\n",a[i].l,a[i].r,x);
		if(x < a[i].l) continue;
		else{
			ll ans = c[x]-c[a[i].l-1];
			if(ans > Mx) Mx = ans;
			// printf("@ %lld\n",ans);
		}
	}
	printf("%lld\n", Mx);
	return 0;
}

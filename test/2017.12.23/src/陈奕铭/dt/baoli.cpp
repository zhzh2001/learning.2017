#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 100;
int n;
bool vis[N];
int f[N];

void dfs(int x){
	if(f[x-1] == 0) dfs(x-1);
	if(f[x-1] == -1){
		f[x] = 1;
		return;
	}
	if(f[x-3] == 0) dfs(x-3);
	if(f[x-3] == -1){
		f[x] = 1;
		return;
	}
	if(f[x-4] == 0) dfs(x-4);
	if(f[x-4] == -1){
		f[x] = 1;
		return;
	}
	f[x] = -1;
}

int main(){
	// scanf("%d",&n);
	f[1] = 1;
	f[2] = -1;
	f[3] = 1;
	f[4] = 1;
	for(int i = 100;i >= 30;--i)
		if(f[i] == 0) dfs(i);
	for(int i = 30;i <= 100;++i){
		if(f[i] == 1) printf("%d : Y\n",i);
		else printf("%d : N\n",i);
	}
	return 0;
}
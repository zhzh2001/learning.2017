#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int mod = 1000000007;
const int N = 55;
bool vis[N];
int ans;
int num,x;
int n,la,lb;
int a[N],b[N];
int now[N];
int p[N];

void dfs(int x){
	if(x == n+1){
		// for(int i = 1;i <= n;++i)
		// 	printf("%d ",now[i]);
		// puts("");
		++ans;  if(ans == mod) ans = 0;
		return;
	}
	if(p[x-1] > p[x]){
		for(int i = 1;i < now[x-1];++i)
			if(!vis[i]){
				vis[i] = true;
				now[x] = i;
				dfs(x+1);
				vis[i] = false;
			}
	}
	else if(p[x-1] < p[x]){
		for(int i = now[x-1]+1;i <= n;++i)
			if(!vis[i]){
				vis[i] = true;
				now[x] = i;
				dfs(x+1);
				vis[i] = false;
			}
	}
	else{
		for(int i = 1;i <= n;++i)
			if(!vis[i]){
				vis[i] = true;
				now[x] = i;
				dfs(x+1);
				vis[i] = false;
			}
	}
}

int main(){
	freopen("ak.in","r",stdin);
	freopen("ak.out","w",stdout);
	n = read(); la = read(); lb = read();
	for(int i = 1;i <= la;++i) a[i] = read()+1;
	for(int j = 1;j <= lb;++j) b[j] = read()+1;
	sort(a+1,a+la+1); sort(b+1,b+lb+1);
	for(int i = 1;i <= la;++i) p[a[i]] = -1;
	for(int j = 1;j <= lb;++j) p[b[j]] = 1;
	for(int i = 2;i <= n;++i)
		if(p[i] == p[i-1] && p[i] != 0){
			puts("0");
			return 0;
		}
	dfs(1);
	printf("%d\n",ans);
	return 0;
}

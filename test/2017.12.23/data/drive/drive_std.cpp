#include<stdio.h>
#include<cstring>
#include<algorithm>
//#include<iostream>
#include<fstream>
#define cin fin
#define cout fout
using namespace std;
#define CLR(a,b) memset(a,b,sizeof(a))
#define L o<<1
#define R o<<1|1
ifstream fin("drive2.in");
ofstream fout("drive2.out");
long long sum[100003+5];  
struct node{
    int st,endd;
}a[100003+5];
struct Tree{
    int l,r;
    int cover;
}tree[400003];
inline bool cmp(node a,node b){
    return a.st<b.st;
}
inline void pushup(int rt){
    tree[rt].cover=tree[rt*2].cover+tree[rt*2+1].cover;
}
void build(int rt,int l,int r){
    tree[rt].l=l,tree[rt].r=r;
    if(l==r){
        tree[rt].cover=0;
        return;
    }
    int mid=(l+r)>>1;
    build(rt*2,l,mid),build(rt*2+1,mid+1,r);
    pushup(rt);
}
void update(int rt,int x){
    if(tree[rt].l==tree[rt].r){
        tree[rt].cover++;
        return;
	}
    int mid=(tree[rt].l+tree[rt].r)>>1;
    if(mid>=x){
    	update(rt*2,x);
    }else{
    	update(rt*2+1,x);
    }
    pushup(rt);
}
int query(int rt,int x){
    if(tree[rt].l==tree[rt].r){
    	return tree[rt].l;
    }
    if(tree[rt*2+1].cover>=x){
    	return query(rt*2+1,x);
    }else{
    	return query(rt*2,x-tree[rt*2+1].cover);
    }
}
int main(){
    int n,k,m,t;
    cin>>n>>k>>m;
    sum[0]=0;
    for(int i=1;i<=n;i++){
    	cin>>t;
    	sum[i]=sum[i-1]+t;
    }
    for(int i=1;i<=m;i++){
    	cin>>a[i].st>>a[i].endd;
    	if(a[i].st==0){
    		fout<<"woc"<<endl;
		}
    }
    sort(a+1,a+1+m,cmp);
    build(1,1,n);
    for(int i=1;i<=k;i++){
    	update(1,a[i].endd);
    }
    long long ans=0;
    a[m+1].endd=1;
    for(int i=k;i<=m;i++){
        int pos=query(1,k);
        if(pos>=a[i].st){
        	ans=max(ans,sum[pos]-sum[a[i].st-1]);
        }
        update(1,a[i+1].endd);
    }
    cout<<ans<<endl;
    return 0;
}
/*

in:
5 2 3
1 2 3 4 6
4 5
2 5
1 4

out:
10

*/

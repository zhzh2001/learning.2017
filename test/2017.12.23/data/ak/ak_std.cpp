//#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstdio>
#include<fstream>
#define cin fin
#define cout fout
using namespace std;
ifstream fin("ak2.in");
ofstream fout("ak2.out");
const int mod=1000000007;
int n,k,l,xx[5003],sum[5003],dp[5003];
int main(){
	cin>>n>>k>>l;
    for(int i=1;i<=k;i++){
    	int x;
    	cin>>x;
        xx[++x]=1,xx[x+1]=2;
    }
    for(int i=1;i<=l;i++){
    	int x;
    	cin>>x;
        xx[++x]=2,xx[x+1]=1;
    }
    dp[1]=sum[1]=1;
    for(int i=2;i<=n;i++){
        for(int j=1;j<=i;j++){
            if(xx[i]==0){
                dp[j]=sum[i-1];
            }else if(xx[i]==1){
                dp[j]=((sum[i-1]-sum[j-1])%mod+mod)%mod;
            }else{
                dp[j]=sum[j-1];
            }
        }
        for(int j=1;j<=i;j++){
            sum[j]=(sum[j-1]+dp[j])%mod;
        }
    }
    cout<<sum[n]<<endl;
    return 0;
}
/*

in:
4 1 1
1
2

out:
5

*/

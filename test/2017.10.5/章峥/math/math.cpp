#include<fstream>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
const int N=1000000,MOD=100000007;
long long inv[N+5];
long long qpow(long long a,long long b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}
	while(b/=2);
	return ans;
}
int main()
{
	inv[1]=1;
	for(int i=2;i<=N;i++)
		inv[i]=inv[MOD%i]*(MOD-MOD/i)%MOD;
	int t;
	fin>>t;
	while(t--)
	{
		long long n;
		int k,r;
		fin>>n>>k>>r;
		long long ans=0;
		if(n<=N)
		{
			long long now=1;
			for(int i=1;i<=n;i++)
			{
				now=now*(n-i+1)%MOD*inv[i]%MOD;
				if(i%k==r)
					ans+=now;
			}
			ans%=MOD;
		}
		else
			ans=qpow(2,n-k+1);
		fout<<ans<<endl;
	}
	return 0;
}

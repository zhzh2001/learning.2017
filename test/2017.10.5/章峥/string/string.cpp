#include<fstream>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.out");
const int N=3005,B=29,MOD=1000000007;
string s[N];
int n,lcp[N][N],sum[N],K,rec[205];
long long ans[N],now;
bool vis[N];
typedef unsigned long long hash_t;
hash_t *h[N];
void dfs(int k,int pred)
{
	if(k>K)
	{
		for(int i=1;i<=n;i++)
			if(!vis[i])
			{
				now+=sum[i];
				for(int j=1;j<=K;j++)
					if(rec[j]>i)
						now-=lcp[i][rec[j]];
			}
		now%=MOD;
	}
	else
		for(int i=pred;i<=n;i++)
		{
			rec[k]=i;
			vis[i]=true;
			dfs(k+1,i+1);
			vis[i]=false;
		}
}
int main()
{
	int q;
	fin>>n>>q;
	for(int i=1;i<=n;i++)
	{
		fin>>s[i];
		int len=s[i].length();
		s[i]=' '+s[i];
		h[i]=new hash_t [len+1];
		h[i][0]=0;
		for(int j=1;j<=len;j++)
			h[i][j]=h[i][j-1]*B+s[i][j]-'a';
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=i+1;j<=n;j++)
		{
			int l=0,r=min(s[i].length(),s[j].length());
			while(l<=r)
			{
				int mid=(l+r)/2;
				if(h[i][mid]==h[j][mid])
				{
					l=mid+1;
					lcp[i][j]=mid;
				}
				else
					r=mid-1;
			}
			sum[i]+=lcp[i][j];
		}
		sum[i]%=MOD;
	}
	memset(ans,-1,sizeof(ans));
	while(q--)
	{
		fin>>K;
		if(~ans[K])
			fout<<ans[K]<<endl;
		else
		{
			now=0;
			dfs(1,1);
			fout<<(ans[K]=now)<<endl;
		}
	}
	return 0;
}

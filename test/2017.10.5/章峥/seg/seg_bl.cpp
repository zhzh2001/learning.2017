#include<fstream>
using namespace std;
ifstream fin("seg.in");
ofstream fout("seg.ans");
const int N=500005;
unsigned a[N];
int main()
{
	int n,q;
	fin>>n>>q;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	while(q--)
	{
		int opt,x;
		unsigned y;
		fin>>opt>>x>>y;
		if(opt==1)
			a[x]=y;
		else
		{
			unsigned ans=a[x];
			for(int i=x+1;i<=y;i++)
				ans=~(ans&a[i]);
			fout<<ans<<endl;
		}
	}
	return 0;
}

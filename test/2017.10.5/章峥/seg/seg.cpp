#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("seg.in");
ofstream fout("seg.out");
const int N=500000;
unsigned a[N],ans[1000][2];
int main()
{
	int n,q;
	fin>>n>>q;
	int bl=floor(sqrt(n));
	for(int i=0;i<n;i++)
		fin>>a[i];
	for(int i=0,c=0;i<n;i+=bl,c++)
	{
		ans[c][0]=0;
		ans[c][1]=0xffffffffu;
		for(int j=0;j<bl&&i+j<n;j++)
		{
			ans[c][0]=~(ans[c][0]&a[i+j]);
			ans[c][1]=~(ans[c][1]&a[i+j]);
		}
	}
	while(q--)
	{
		int opt,x;
		unsigned y;
		fin>>opt>>x>>y;
		x--;
		if(opt==1)
		{
			a[x]=y;
			int c=x/bl,i=c*bl;
			ans[c][0]=0;
			ans[c][1]=0xffffffffu;
			for(int j=0;j<bl&&i+j<n;j++)
			{
				ans[c][0]=~(ans[c][0]&a[i+j]);
				ans[c][1]=~(ans[c][1]&a[i+j]);
			}
		}
		else
		{
			y--;
			unsigned now=a[x];
			int i=x/bl*bl+bl;
			for(x++;x<i&&x<=y;x++)
				now=~(now&a[x]);
			if(x<=y)
			{
				int yb=y/bl;
				for(int j=i/bl;j<yb;j++)
					now=(ans[j][1]&now)|(ans[j][0]&(~now));
				for(int j=yb*bl;j<=y;j++)
					now=~(now&a[j]);
			}
			fout<<now<<endl;
		}
	}
	return 0;
}

#include<fstream>
#include<random>
#include<ctime>
#include<algorithm>
using namespace std;
ofstream fout("seg.in");
const int n=1e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<n<<endl;
	uniform_int_distribution<unsigned> v(0,0xffffffffu);
	for(int i=1;i<=n;i++)
		fout<<v(gen)<<' ';
	fout<<endl;
	uniform_int_distribution<> d(1,n),opt(1,2);
	for(int i=1;i<=n;i++)
		if(opt(gen)==1)
			fout<<1<<' '<<d(gen)<<' '<<v(gen)<<endl;
		else
		{
			int l=d(gen),r=d(gen);
			if(l>r)
				swap(l,r);
			fout<<2<<' '<<l<<' '<<r<<endl;
		}
	return 0;
}

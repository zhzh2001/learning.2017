#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 400005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-' ) f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll c[1005][1005],T,n,k,r,ans;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	T=read();
	rep(i,0,1000) c[i][0]=1;
	rep(i,1,1000) rep(j,1,i) c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
	while (T--){
		n=read(); k=read(); r=read(); ans=0;
		ll i=0;
		while (i*k+r<=n){
			(ans+=c[n][i*k+r])%=mod;
			++i;
		}
		printf("%lld",ans);
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll unsigned int
#define N 400005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-' ) f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a[N];
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,m){
		ll opt=read(),x=read(),y=read();
		if (opt==1) a[x]=y;
		else{
			ll ans=a[x];
			rep(j,x+1,y) ans=~(ans&a[j]);
			printf("%lld\n",ans);
		}
	}
}

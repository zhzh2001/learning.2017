#include<bits/stdc++.h>
using namespace std;
long long  n,q;
long long a[500000];
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
long long nand(long long x,long long y){
	long long  temp=0;
	temp=x & y;
	temp=4294967295-temp;
	return temp;
}
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout); 
	n=read();
	q=read();
	for (int i=1;i<=n;i++)
		a[i]=read();
	while (q--){
		int x;
		x=read();
		if (x==1){
			long long a1,a2;
			a1=read();
			a2=read();
			a[a1]=a2;
		}
		else{
			long long l,r;
			l=read();
			r=read();
			long long ansn=a[l];
			for (int i=l+1;i<=r;i++)
				ansn=nand(ansn,a[i]);
			cout<<ansn<<endl;
		}
	}
}

#include<cstdio>
#define uint unsigned int
const uint MX=-1;
const int N=1e6+5;
int n,m,i,l,r;
uint a[N];
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++) scanf("%u",&a[i]);
	for (;m--;){
		int opt;scanf("%d",&opt);
		if (opt==1){
			int x;uint k;
			scanf("%d%u",&x,&k);
			a[x]=k;
		}
		else{
			scanf("%d%d",&l,&r);
			uint ans=a[l];
			for (i=l+1;i<=r;i++) ans=MX^(ans&a[i]);
			printf("%u\n",ans);
		}
	}
}

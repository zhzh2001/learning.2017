#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define uint unsigned int
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	char c=gc();int x=0;for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int MX=-1;
const int N1=1,N2=1<<5,N3=1<<10,N4=1<<15;
int n,Q,i,x,y;
int a1[32][N1],a2[32][N2],a3[32][N3],a4[32][N4];
void add(int px,int vk){
	for (int i=0;i<32;i++){
		int p=px,k=vk&1;vk>>=1;
		x=p>>5;y=p&31;p>>=5;
		if (!((a4[i][x]>>y)&1)^k) continue;
		a4[i][x]^=1<<y;k=a4[i][x]>0;
		x=p>>5;y=p&31;p>>=5;
		if (!((a3[i][x]>>y)&1)^k) continue;
		a3[i][x]^=1<<y;k=a3[i][x]>0;
		x=p>>5;y=p&31;p>>=5;
		if (!((a2[i][x]>>y)&1)^k) continue;
		a2[i][x]^=1<<y;k=a2[i][x]>0;
		x=p>>5;y=p&31;p>>=5;
		if (!((a1[i][x]>>y)&1)^k) continue;
		a1[i][x]^=1<<y;
	}
}
int df(int k,int x){
	for (int i=x;i--;) if ((k>>i)&1) return i;
}
int down4(int x,int y){
	return (x<<5)+df(a4[i][x],y);
}
int down3(int x,int y){
	return down4(df(a3[i][x],y),32);
}
int down2(int x,int y){
	return down3(df(a2[i][x],y),32);
}
int down1(int x,int y){
	return down2(df(a1[i][x],y),32);
}
int find(int p){
	x=p>>5;y=p&31;p>>=5;
	if (a4[i][x]&((1<<y)-1)) return down4(x,y);
	x=p>>5;y=p&31;p>>=5;
	if (a3[i][x]&((1<<y)-1)) return down3(x,y);
	x=p>>5;y=p&31;p>>=5;
	if (a2[i][x]&((1<<y)-1)) return down2(x,y);
	x=p>>5;y=p&31;p>>=5;
	if (a1[i][x]&((1<<y)-1)) return down1(x,y);
	return -1;
}
int query(int l,int r){
	int ans=0;
	for (i=32;i--;){
		if (i==4){
			int kk=0;
		}
		int x=find(r+1);ans<<=1;
		if (!(x>l&&((r-x)&1)||x==l&&!((r-x)&1)||x<l&&((r-l)&1))) ans|=1;
	}
	printf("%u\n",ans);
}
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout); 
	n=read();Q=read();
	memset(a1,-1,sizeof(a1));
	memset(a2,-1,sizeof(a2));
	memset(a3,-1,sizeof(a3));
	memset(a4,-1,sizeof(a4));
	for (i=0;i<n;i++) add(i,MX^read());
	for (;Q--;){
		int opt=read(),x=read(),y=read();
		if (opt==1) add(x-1,y^MX);
		else query(x-1,y-1);
	}
}

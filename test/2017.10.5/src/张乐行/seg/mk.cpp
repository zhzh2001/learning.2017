#include<cstdio>
#include<ctime>
#include<algorithm>
#define ll long long
using namespace std;
const int P=1<<5;
int ran(){return (rand()<<15)+rand();}
int n,m,i;
int main(){
	freopen("seg.in","w",stdout);
	srand(time(0));rand();
	n=33;m=5;
	printf("%d %d\n",n,m);
	for (i=1;i<=n;i++){
		printf("%d ",ran()%P);
	}puts("");
	for (;m--;){
		int opt=rand()%2;
		if (opt){
			printf("1 %d %d\n",ran()%n+1,ran()%P);
		}
		else{
			int l=ran()%n+1,r=ran()%n+1;
			if (l>r) swap(l,r);
			printf("2 %d %d\n",l,r);
		}
	}
}

#include<cstdio>
#define ll long long
const int N=1e6+5,mo=1e8+7;
int n,k,r,i;
int fact[N],inv[N],fv[N];
void init(){
	fact[0]=fact[1]=inv[1]=fv[0]=fv[1]=1;
	for (i=2;i<N;i++){
		fact[i]=(ll)fact[i-1]*i%mo;
		inv[i]=(ll)inv[mo%i]*(mo-mo/i)%mo;
		fv[i]=(ll)fv[i-1]*inv[i]%mo;
	}
}
int C(int n,int m){
	return (ll)fact[n]*fv[m]%mo*fv[n-m]%mo;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	init();int TEST;scanf("%d",&TEST);
	for (;TEST--;){
		scanf("%d%d%d",&n,&k,&r);int ans=0;
		for (i=r;i<=n;i+=k) ans=(ans+C(n,i))%mo;
		printf("%d\n",ans);
	}
}

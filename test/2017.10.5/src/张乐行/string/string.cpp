#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=3005,P=1e6+5,mo=1e9+7;
int n,m,nl,i,j,pos[P];ll ans;
char sc[P],s[P];
int nd,tr[P][26],g[P];
int fact[N],inv[N],fv[N];
void init(){
	fact[1]=1;inv[1]=1;fv[0]=1;fv[1]=1;
	for (i=2;i<=n;i++){
		fact[i]=(ll)fact[i-1]*i%mo;
		inv[i]=(ll)inv[mo%i]*(mo-mo/i)%mo;
		fv[i]=(ll)fv[i-1]*inv[i]%mo;
	}
}
int C(int n,int m){
	if (m>n) return 0;
	return (ll)fact[n]*fv[m]%mo*fv[n-m]%mo;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d%d",&n,&m);init();
	for (i=1;i<=n;i++){
		scanf("%s",sc);
		int len=strlen(sc),x=0;
		ans-=len;
		for (j=0;j<len;j++){
			int k=sc[j]-97;
			if (!tr[x][k]) tr[x][k]=++nd;
			x=tr[x][k];g[x]++;pos[++nl]=x;
		}
	}
	for (i=1;i<=nl;i++) ans+=g[pos[i]];
	ans/=2;
	for (;m--;){
		int k;scanf("%d",&k);
		printf("%d\n",ans*C(n-2,k)%mo);
	}
}

#include<cstdio>
using namespace std;
typedef long long ll;
const int mod=100000007,maxn=1000005;
ll C[maxn],n;
int k,r;
void exgcd(int a,int b,int &x,int &y)
{
	if (!b){x=1; y=0; return;}
	exgcd(b,a%b,x,y); int temp=x; x=y; y=temp-a/b*y;
}
inline int inv(int a)
{
	int x,y;
	exgcd(a,mod,x,y);
	return (x%mod+mod)%mod;
}
inline void sp()
{
	ll ans=1,x=2;
	while (n){
		if (n&1){
			(ans*=x)%=mod;
		}
		n>>=1; (x*=x)%=mod;
	}
	printf("%lld\n",ans);
}
int main()
{
//	freopen("math.in","r",stdin);
//	freopen("math.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		scanf("%lld%d%d",&n,&k,&r);
		if (k==1){
			sp();
			continue;
		}
		C[1]=n;
		for (int i=2;i<=n;i++){
			C[i]=C[i-1]*(n-i+1)%mod*inv(i)%mod;
		}
		ll ans=0;
		for (int i=r;i<=n;i+=k){
			(ans+=C[i])%=mod;
		}
		printf("%lld\n",ans);
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int L=200005,maxn=500005;
ll bin[33];
ll num[maxn];
int q,n,digit[maxn];
struct pp{
	int ans,l,r;
}a[32][1100000];
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
inline int nand(int x,int y){return !(x&y);} 
inline void pushup(int id,int k){a[id][k].ans=nand(a[id][k<<1].ans,a[id][k<<1|1].ans);}
void build(int id,int k,int l,int r)
{
	a[id][k].l=l; a[id][k].r=r; int mid=(l+r)>>1;
	if (l==r){
		a[id][k].ans=digit[l];
		return;
	}
	build(id,k<<1,l,mid); build(id,k<<1|1,mid+1,r);
	pushup(id,k);
}
inline void makebuild()
{
	for (int i=0;i<=31;i++){
		for (int j=1;j<=n;j++){
			if (num[j]&bin[i]){
				digit[j]=1;
			}else {
				digit[j]=0;
			}
		}
		build(i,1,1,n);
	}
}
inline void update(int id,int k,int x,int v)
{
	int l=a[id][k].l,r=a[id][k].r,mid=(l+r)>>1;
	if (l==r){
		a[id][k].ans=v;
		return ;
	}
	if (mid>=x) update(id,k<<1,x,v);
		else update(id,k<<1|1,x,v);	
	pushup(id,k);
}
inline void change(ll x,ll y)
{
	for (int i=0;i<=31;i++){
		int opt;
		if (y&bin[i]){
			opt=1;
		}else {
			opt=0;
		}
		update(i,1,x,opt);
	}
}
ll query(int id,int k,int x,int y)
{
	int l=a[id][k].l,r=a[id][k].r,mid=(l+r)>>1;
	if (l==x&&r==y){
		return a[id][k].ans;
	}
	if (mid>=y){
		return query(id,k<<1,x,y);
	}else{
		if (mid<x){
			return query(id,k<<1|1,x,y);
		}else{
			return nand(query(id,k<<1,x,mid),query(id,k<<1|1,mid+1,y));
		}
	}
}
inline void solve(ll x,ll y)
{
	ll ans=0;
	for (int i=0;i<=31;i++){
		ans+=query(i,1,x,y)*bin[i];
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("seg1.in","r",stdin);
	freopen("seg1.out","w",stdout);
	n=read(); q=read();
	bin[0]=1; for (int i=1;i<=32;i++) bin[i]=bin[i-1]<<1ll;
	for (int i=1;i<=n;i++){
		num[i]=read();
	}
	makebuild();
	for (int i=1;i<=q;i++){
		int opt=read(); ll x=read(),y=read();
		if (opt==1){
			change(x,y);
		}else{
			solve(x,y);
		}
	}
	return 0;
}

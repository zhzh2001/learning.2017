#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int maxn=500005,L=200005;
int n,q;
ll a[maxn],base;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
inline void write(ll x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(ll x){write(x); puts("");}
inline ll nand(ll x,ll y){return base-(x&y);}
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout); 
	n=read(); q=read();
	base=(1ll<<32)-1;
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
	for (int i=1;i<=q;i++){
		int opt=read(); ll x=read(),y=read();
		if (opt==1){
			a[x]=y;
		}else{
			ll ans=a[x];
			for (int i=x+1;i<=y;i++){
				ans=nand(ans,a[i]);
			}
			writeln(ans);
		}
	}
	return 0;
}

#include <bits/stdc++.h>
#define ll long long
#define N 500020
#define uint unsigned int
#define mod 100000007
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int c[1020][1020];
int main(int argc, char const *argv[]) {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	for (int i = 1; i <= 1000; i++) {
		c[i][1] = i;
		for (int j = 2; j <= i; j++)
			c[i][j] = (c[i-1][j]+c[i-1][j-1])%mod;
	}
	int T = read();
	while (T--) {
		int n = read(), k = read(), r = read();
		if (n > 1000) return 233;
		int ans = 0;
		while (r <= n) {
			ans = (ans+c[n][r])%mod;
			r += k;
		}
		printf("%d\n", ans);
	}
	return 0;
}

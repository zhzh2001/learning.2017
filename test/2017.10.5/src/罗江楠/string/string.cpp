#include <bits/stdc++.h>
#define ll long long
#define N 1000020
#define uint unsigned int
#define mod 1000000007
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int ch[N][26], val[N], cnt = 1;
char str[1000020];
void build(int now, int n) {
	for (int i = 1; i <= n; i++) {
		if (!ch[now][str[i]-'a'])
			ch[now][str[i]-'a'] = ++cnt;
		now = ch[now][str[i]-'a'];
		val[now]++;
	}
}
int q[N];
int c[3020][220];
int main(int argc, char const *argv[]) {
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
	for (int i = 1; i <= 3000; i++) {
		c[i][1] = i;
		for (int j = 2; j <= min(i, 200); j++)
			c[i][j] = (c[i-1][j]+c[i-1][j-1])%mod;
	}
	int n = read(), m = read();
	for (int i = 1; i <= n; i++) {
		scanf("%s", str+1);
		build(1, strlen(str+1));
	}
	while (m--) {
		int x = read();
		ll ans = 0;
		int l = 0, r = 0;
		for (int i = 0; i < 26; i++)
			if (ch[1][i]) q[++r] = ch[1][i];
		while (l < r) {
			int s = q[++l];
			if (val[s] >= 2) {
				ans = (ans+1ll*c[val[s]][2]*c[n-2][x])%mod;
				for (int i = 0; i < 26; i++)
					if (ch[s][i]) q[++r] = ch[s][i];
			}
		}
		printf("%lld\n", ans);
	}
	return 0;
}

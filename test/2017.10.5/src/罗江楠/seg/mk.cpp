#include <bits/stdc++.h>
#define ll long long
#define N 500020
#define uint unsigned int
#define _cmp_ &
using namespace std;
inline uint read(){
	uint x=0;bool f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct segtree {
	uint rst[N<<2];
	bool all[N<<2];
	void push_up(int x) {
		all[x] = all[x<<1] & all[x<<1|1];
		rst[x] = all[x<<1|1] ? rst[x<<1]+rst[x<<1|1] : rst[x<<1|1];
	}
	void build(int x, int l, int r) {
		if (l == r) {
			all[x] = rst[x] = read();
			return;
		}
		int mid = l + r >> 1;
		build(x<<1, l, mid);
		build(x<<1|1, mid+1, r);
		push_up(x);
	}
	void change(int x, int p, int L, int R, uint v) {
		if (L == R) {
			all[x] = rst[x] = v;
			return;
		}
		int mid = L + R >> 1;
		if (p <= mid) change(x<<1, p, L, mid, v);
		else change(x<<1|1, p, mid+1, R, v);
		push_up(x);
	}
	uint ask(int x, int l, int r, int L, int R) {
		if (l == L && r == R)
			return rst[x];
		int mid = L + R >> 1;
		if (r <= mid)
			return ask(x<<1, l, r, L, mid);
		if (l > mid)
			return ask(x<<1|1, l, r, mid+1, R);
		uint ls = ask(x<<1, l, mid, L, mid);
		uint rs = ask(x<<1|1, mid+1, r, mid+1, R);
		return rs == R-mid ? ls+rs : rs;
	}
}tr;
int main(int argc, char const *argv[]) {
	int n = read();
	tr.build(1, 1, n);
	while (1) {
		int p = read();
		if (p == 1) {
			int x = read();
			tr.change(1, x, 1, n, read());
		}
		else {
			int l = read(), r = read();
			printf("%d\n", tr.ask(1, l, r, 1, n));
		}
	}
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define N 500020
#define uint unsigned int
#define _cmp_ &
using namespace std;
inline uint read(){
	uint x=0;bool f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
bool f[40][N];
void sets(int x, uint v) {
	for (int i = 0; i < 32; i++)
		f[i][x] = v >> i & 1;
}
int ask(int x, int l, int r) {
	if (!f[x][r])
		return 1;
	int pos = r-1;
	while (pos > l)
		if (!f[x][pos--])
			return (r-pos)&1;
	return (r-pos+f[x][pos])&1;
}
int main(int argc, char const *argv[]) {
	freopen("seg.in", "r", stdin);
	freopen("seg.out", "w", stdout);
	int n = read(), m = read();
	for (int i = 1; i <= n; i++)
		sets(i, read());
	while (m--) {
		if (read() == 1) {
			int x = read();
			sets(x, read());
		} else {
			int l = read(), r = read();
			uint ans = 0;
			for (int i = 0; i < 32; i++)
				ans |= ask(i, l, r) << i;
			printf("%u\n", ans);
		}
	}
	return 0;
}

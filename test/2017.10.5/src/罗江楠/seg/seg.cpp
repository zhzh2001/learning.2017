#include <bits/stdc++.h>
#define ll long long
#define N 500020
#define uint unsigned int
using namespace std;
inline uint read(){
	uint x=0;bool f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
uint a[N];
int cnt;
struct segtree {
	uint rst[N<<2];
	bool all[N<<2];
	int id;
	segtree():id(cnt++){}
	void push_up(int x) {
		all[x] = all[x<<1] && all[x<<1|1];
		rst[x] = all[x<<1|1] ? rst[x<<1]+rst[x<<1|1] : rst[x<<1|1];
	}
	void build(int x, int l, int r) {
		if (l == r) {
			all[x] = rst[x] = a[l] >> id & 1;
			return;
		}
		int mid = l + r >> 1;
		build(x<<1, l, mid);
		build(x<<1|1, mid+1, r);
		push_up(x);
	}
	void change(int x, int p, int L, int R) {
		if (L == R) {
			all[x] = rst[x] = a[p] >> id & 1;
			return;
		}
		int mid = L + R >> 1;
		if (p <= mid) change(x<<1, p, L, mid);
		else change(x<<1|1, p, mid+1, R);
		push_up(x);
	}
	uint ask(int x, int l, int r, int L, int R) {
		if (l == L && r == R)
			return rst[x];
		int mid = L + R >> 1;
		if (r <= mid)
			return ask(x<<1, l, r, L, mid);
		if (l > mid)
			return ask(x<<1|1, l, r, mid+1, R);
		uint ls = ask(x<<1, l, mid, L, mid);
		uint rs = ask(x<<1|1, mid+1, r, mid+1, R);
		return rs == r-mid ? ls+rs : rs;
	}
}tr[40];
int n, m;
int ask(int x, int l, int r) {
	int len = tr[x].ask(1, l, r, 1, n);
	// if (x == 27) {
	// 	printf("%d %d %d\n", l, r, len);
	// }
	if (!len) return 1;
	if (len == r-l+1)
		return (r-l+1)&1;
	if (len == r-l)
		return (r-l)&1;
	return !(len&1);
}
int main(int argc, char const *argv[]) {
	freopen("seg.in", "r", stdin);
	freopen("seg.out", "w", stdout);
	n = read(); m = read();
	for (int i = 1; i <= n; i++)
		a[i] = read();
	for (int i = 0; i < 32; i++) {
		// printf("%d\n", tr[i].id);
		tr[i].build(1, 1, n);
	}
	while (m--) {
		if (read() == 1) {
			int x = read();
			a[x] = read();
			for (int i = 0; i < 32; i++)
				tr[i].change(1, x, 1, n);
		} else {
			int l = read(), r = read();
			uint ans = 0;
			for (int i = 0; i < 32; i++)
				ans |= ask(i, l, r) << i;
			printf("%u\n", ans);
		}
	}
	return 0;
}

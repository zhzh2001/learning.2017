#include <bits/stdc++.h>
#define ll long long
#define N 500020
#define uint unsigned int
#define _cmp_ &
using namespace std;
inline uint read(){
	uint x=0;bool f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
uint a[N];
uint ask(int l, int r) {
	uint ans = a[l];
	for (int i = l+1; i <= r; i++)
		ans = ~(ans&a[i]);
	return ans;
}
int main(){
	freopen("seg.in", "r", stdin);
	freopen("seg.out", "w", stdout);
	int n = read(), m = read();
	for (int i = 1; i <= n; i++) a[i] = read();
	for (int i = 1; i <= m; i++) {
		int op = read(), l = read();
		uint r = read();
		if (op == 1) a[l] = r;
		else printf("%u\n", ask(l, r));
	}
}

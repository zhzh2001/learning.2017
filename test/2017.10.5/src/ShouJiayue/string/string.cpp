#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<vector>
#include<set>
#include<map>
#include<ctime>
#include<cstdlib>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=25;
const int L=505;

int n,q,Ans,tot;
int S[N][L],P[N];

int Lcp(int x,int y){
	int res=0;
	while (S[x][res+1]==S[y][res+1] && S[x][res+1]!=0) res++;
	return res;
}
void Cal(){
	int res=0;
	Rep(i,1,n) Rep(j,1,n) if (!P[i] && !P[j] && i!=j) res=max(res,Lcp(i,j));
	Ans+=res;
}
void Dfs(int u,int num){
	if (num==tot){
		Cal();return;
	}
	if (n-u+1<tot-num) return;
	Dfs(u+1,num);
	P[u]=1;
	Dfs(u+1,num+1);
	P[u]=0;
}
void Work(){
	Ans=0;
	memset(P,0,sizeof(P));
	Dfs(1,0);
	printf("%d\n",Ans);
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read(),q=read();
	Rep(i,1,n){
		int len=0,ch=getchar();
		while (ch<'a' || ch>'z') ch=getchar();
		while (ch>='a' && ch<='z') S[i][++len]=ch,ch=getchar();
	}
	Rep(i,1,q){
		tot=read();Work();
	}
}
/*
3 1
aab
abb
aba
1
*/

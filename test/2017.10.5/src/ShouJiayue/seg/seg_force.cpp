#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<vector>
#include<set>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=500005;
const unsigned Mod=((1ll)<<32)-1;

unsigned Dat[N];
inline int Nand(unsigned x,unsigned y){
	return (x&y)^Mod;
}
int n,q;
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.ans","w",stdout);
	n=read(),q=read();
	Rep(i,1,n) scanf("%u",&Dat[i]);
	Rep(i,1,q){
//		printf("i=%d\n",i);
		int opt=read(),x=read(),y=read();
//		printf("opt=%d x=%d y=%d\n",opt,x,y);
		if (opt==1){
			Dat[x]=y;
		}
		if (opt==2){
			unsigned res=Dat[x];
			Rep(j,x+1,y) res=Nand(res,Dat[j]);
			printf("%u\n",res);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<vector>
#include<set>
#include<map>
#include<ctime>
#include<cstdlib>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
inline int Rand(){
	return rand()<<15|rand();
}
int n,q;
int main(){
	freopen("seg.in","w",stdout);
	srand(time(0));
	Rep(i,1,100) rand();
	printf("%d %d\n",n=100,q=10000);
	Rep(i,1,n) printf("%d ",Rand());puts("");
	Rep(i,1,q){
		int opt=rand()%2;
		if (opt==0){
			printf("%d %d %d\n",opt+1,rand()%n+1,Rand());
		}
		if (opt==1){
			int x=rand()%n+1,y=rand()%n+1;
			if (x>y) swap(x,y);
			printf("%d %d %d\n",opt+1,x,y);
		}
	}
}

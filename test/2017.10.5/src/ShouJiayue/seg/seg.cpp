#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<vector>
#include<set>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=500005;
const unsigned Mod=((1ll)<<32)-1;

struct Segment_Tree{
	#define mid (L+R>>1)
	int rit[N*4];
	inline void Pushup(int p){
		if (!rit[p<<1|1]) rit[p]=rit[p<<1];
			else rit[p]=rit[p<<1|1];
	}
	void Modify(int L,int R,int p,int pos,int chr){
		if (L==R){
			if (chr==0) rit[p]=L;
				else rit[p]=0;return;
		}
		if (pos<=mid) Modify(L,mid,p<<1,pos,chr);else
			Modify(mid+1,R,p<<1|1,pos,chr);
		Pushup(p);
	}
	int Query(int L,int R,int p,int x,int y){
		if (L==x && R==y) return rit[p];
		if (y<=mid) return Query(L,mid,p<<1,x,y);else
		if (x>mid) return Query(mid+1,R,p<<1|1,x,y);else{
			return max(Query(L,mid,p<<1,x,mid),Query(mid+1,R,p<<1|1,mid+1,y));
		}
	}
}T[32];

unsigned Dat[N];
inline int Nand(unsigned x,unsigned y){
	return (x&y)^Mod;
}
int n,q;
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	n=read(),q=read();
	Rep(i,1,n){
		scanf("%u",&Dat[i]);
		Rep(j,0,31) T[j].Modify(1,n,1,i,(Dat[i]>>j)&1);
	}
	Rep(i,1,q){
		int opt=read(),x=read(),y=read();
		if (opt==1){
			Dat[x]=y;
			Rep(j,0,31) T[j].Modify(1,n,1,x,(Dat[x]>>j)&1);
		}
		if (opt==2){
			if (x==y){
				printf("%u\n",Dat[x]);continue;
			}
			unsigned res=0;
			Rep(j,0,31){
				int pos=T[j].Query(1,n,1,x,y);
				int cnt=y-pos+1;
				if (pos==x) cnt--;
				if (pos==0) cnt=y-x+1;
				if (cnt%2) res+=(1<<j);
			}
			printf("%u\n",res);
		}
	}
}

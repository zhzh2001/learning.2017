#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<vector>
#include<set>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=1000005;
const int Mod=100000007;

inline int Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int Fac[N],Fav[N],Inv[N];
void Inti(int n){
	Fac[1]=Inv[1]=Fav[1]=1;
	Rep(i,2,n){
//		printf("i=%d\n",i);
		Fac[i]=1ll*Fac[i-1]*i%Mod;
		Inv[i]=(1ll*(-Mod/i)*Inv[Mod%i]%Mod+Mod)%Mod;
		Fav[i]=1ll*Fav[i-1]*Inv[i]%Mod;
	}
}
inline int C(int x,int y){
	if (x<y) return 0;
	if (y==0) return 1;
	if (x==y) return 1;
	return 1ll*Fac[x]*Fav[x-y]%Mod*Fav[y]%Mod;
}
inline int Pow(int base,ll k){
	int res=1;
	for (ll i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}
void Work(){
	ll n;
	scanf("%I64d",&n);
	int k=read(),r=read();
	if (k==1){
		printf("%d\n",Pow(2,n));return;
	}
	int res=0;
	for (int i=r;i<=n;i+=k) Add(res,C(n,i));
	printf("%d\n",res);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	Inti(N-1);
	int T=read();
	Rep(i,1,T) Work();
}
/*
3
2 2 1
3 3 2
10 1 0
*/

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

using namespace std;

typedef unsigned int UI;

const int L = 200005;
char JYT[L], *S = JYT, *T = JYT;

char gc(){
	if (S == T){
		T = (S = JYT) + fread(JYT, 1, L, stdin);
		if (S == T) return EOF;
	}
	return *S++;
}

UI rd() {
	UI x = 0; char c = gc();
	while (c > '9' || c < '0') c = gc();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = gc();
	return x;
}

void wt(UI x) {
	if (x >= 10) wt(x / 10);
	putchar(x % 10 + 48);
}

const int N = 5e5 + 10;

struct Tree {
	int bit[32][2];
}tr[N << 2];
int a[N], n, q;

Tree merge(Tree L, Tree R) {
	Tree re;
	for (int i = 0; i < 32; i ++) 
		re.bit[i][0] = R.bit[i][L.bit[i][0]], re.bit[i][1] = R.bit[i][L.bit[i][1]];
	return re;
}

void build(int k, int l, int r) {
	if (l == r) {
		for (int i = 0; i < 32; i ++) 
			tr[k].bit[i][0] = 1, tr[k].bit[i][1] = ((a[l] >> i) & 1) ^ 1;
		return;
	}
	int mid = l + r >> 1;
	build(k << 1, l, mid);
	build(k << 1 | 1, mid + 1, r);
	tr[k] = merge(tr[k << 1], tr[k << 1 | 1]);
}

void modify(int k, int l, int r, int x, int y) {
	if (l == r) {
		a[l] = y;
		for (int i = 0; i < 32; i ++) 
			tr[k].bit[i][0] = 1, tr[k].bit[i][1] = ((a[l] >> i) & 1) ^ 1;
		return;
	}
	int mid = l + r >> 1;
	if (x <= mid) modify(k << 1, l, mid, x, y);
	else modify(k << 1 | 1, mid + 1, r, x, y);
	tr[k] = merge(tr[k << 1], tr[k << 1 | 1]);
}

Tree query(int k, int l, int r, int x, int y) {
	if (x == l && r == y) return tr[k];
	int mid = l + r >> 1;
	if (y <= mid) return query(k << 1, l, mid, x, y);
	else if (x > mid) return query(k << 1 | 1, mid + 1, r, x, y);
	return merge(query(k << 1, l, mid, x, mid), query(k << 1 | 1, mid + 1, r, mid + 1, y));
}

int main(){
	freopen("seg.in", "r", stdin);
	freopen("seg.out", "w", stdout); 
	n = rd(), q = rd();
	for (int i = 1; i <= n; i ++) a[i] = rd();
	build(1, 1, n);
	while (q --) {
		int op = rd(), x = rd(), y = rd();
		if (op == 1) modify(1, 1, n, x, y);
		else {
			if (x == y) wt(a[x]), putchar(10);
			else {
				Tree re = query(1, 1, n, x + 1, y); UI ans = 0;
				for (int i = 0; i < 32; i ++) ans += re.bit[i][(a[x] >> i) & 1] * (1u << i);
				wt(ans), putchar(10);
			}
		}
	}
	return 0;
}

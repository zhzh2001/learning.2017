#include <cstdio>

using namespace std;

int rd() {
	int x = 0; char c = getchar();
	while (c > '9' || c < '0') c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x;
}

void wt(int x) {
	if (x >= 10) wt(x / 10);
	putchar(x % 10 + 48);
}

const int P = 100000007;

int power(int a, int b) {
	int c = 1;
	for (; b; b >>= 1, a = 1ll a * a % P) if (b & 1) c = 1ll * c * a % P;
	return c;
}

int main() {
	freopen("math.in", "r", stdin);
	freopen("force.out", "w", stdout);
	fac[0] = fac[1] = 1;
	for (int i = 2; i <= 1e6; i ++) fac[i] = 1ll * fac[i - 1] * i % P;
	fac_inv[1000000] = power(fac[1000000], P - 2);
	for (int i = 1e6 - 1; i; i --) fac_inv[i] = fac[i + 1] * fac_inv[i + 1];
	for (int T = rd(); T; T --) {
		n = rd(), k = rd(), r = rd();
		ans = 0;
		for (int i = 0; i <= n; i ++) {
			int m = i * k + r;
			if (m > n) break;
		}
		wt(ans), putchar(10);
	}
	return 0;
}

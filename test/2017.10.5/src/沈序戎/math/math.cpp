#include <cstdio>
#include <cstring>

using namespace std;

long long rd() {
	long long x = 0; char c = getchar();
	while (c > '9' || c < '0') c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x;
}

void wt(int x) {
	if (x >= 10) wt(x / 10);
	putchar(x % 10 + 48);
}

const long long P = 100000007;

long long n;
int k, r;

struct martix {
	int a[510];
	martix() {
		memset(a, 0, sizeof a);
	}
	
	void clear() {
		memset(a, 0, sizeof a);
	}
	
	martix operator * (martix B) {
		martix C;
		for (int i = 0; i < k; i ++) for (int j = 0; j < k; j ++) {
				C.a[(i + j) % k] = (C.a[(i + j) % k] + 1ll * a[i] * B.a[j] % P) % P;
		}
		return C;
	}
	
	martix operator ^ (long long B) {
		martix C, A = *(this);
		C.a[0] = 1;
		for ( ; B; B /= 2, A = A * A) if (B & 1) C = C * A;
		return C;
	}
} F;

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	for (int T = rd(); T; T --) {
		n = rd(), k = rd(), r = rd();
		F.clear();
		if (k == 1) F.a[0] = 2; else F.a[0] = 1, F.a[1] = 1;
		F = F ^ n;
		wt(F.a[r]), putchar(10);
	}
	return 0;
}

#include <cstdio>
#include <cstring>

using namespace std;

int rd() {
	int x = 0; char c = getchar();
	while (c > '9' || c < '0') c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x;
}

void wt(int x) {
	if (x >= 10) wt(x / 10);
	putchar(x % 10 + 48);
}

const int P = 1000000007; 
const int N = 1000010;

int ch[N][30], num[3010], sz[N], C[3010][210], l, tot, n, q;
char s[N]; 

int main() {
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
	n = rd(), q = rd();
	for (int i = 0; i <= n; i ++){
		C[i][0] = 1;
		for (int j = 1; j <= i; j ++) C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % P;
	}
	for (int i = 1; i <= n; i ++){
		scanf("%s", s + 1), l = strlen(s + 1);
		for (int i = 1, k = 0; i <= l; i ++) {
			if (!ch[k][s[i] - 'a']) ch[k][s[i] - 'a'] = ++ tot;
			sz[k = ch[k][s[i] - 'a']] ++;
		}
	}
	for (int i = 1; i <= tot; i ++) num[sz[i]] ++;
	while (q --){
		int k = rd(), ans = 0;
		for (int i = 1; i <= n; i ++) for (int j = 0; j <= k; j ++) if (j <= i && k - j <= n - i) 
			ans = (ans + 1ll * num[i] * (i - j) * (i - j - 1) / 2 % P * C[i][j] % P * C[n - i][k - j] % P) % P;
		wt(ans), putchar(10); 
	}
}

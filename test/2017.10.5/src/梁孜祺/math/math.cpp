#include<bits/stdc++.h>
#define For(i,x,y) for(ll i=x;i<=y;i++)
#define Rep(i,x,y) for(ll i=x;i>=y;i--)
#define N 1000005
#define int ll
#define mod 100000007
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
ll p[N];
inline void pre(){
	p[1]=1;
	For(i,2,N-1) p[i]=p[i-1]*i%mod;
}
inline ll ksm(ll x,ll y){
	ll sum=1;
	while(y){
		if(y&1) sum=sum*x%mod;
		y>>=1;
		if(y) x=x*x%mod; 
	}
	return sum;
}
inline void solve(int n,int k,int r){
	ll sum=0;
	for(int i=0;;i++){
		int t=i*k+r;
		if(t>n) break;
		ll gg=p[n]*ksm(p[n-t]*p[t]%mod,mod-2)%mod;
		sum=(sum+gg)%mod;
	}
	printf("%lld\n",sum);
}
signed main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	pre();
	int T=read();
	while(T--){
		int n=read(),k=read(),r=read();
		solve(n,k,r);
	}
	return 0;
}

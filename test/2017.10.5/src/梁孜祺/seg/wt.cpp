#include<bits/stdc++.h>
#define For(i,x,y) for(ll i=x;i<=y;i++)
#define Rep(i,x,y) for(ll i=x;i>=y;i--)
#define N 500005
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
ll n,Q,gg,a[500005];
int main(){
	freopen("seg.in","r",stdin);
	freopen("gg.out","w",stdout);
	n=read();Q=read();
	For(i,1,32) gg=gg<<1|1;
	For(i,1,n) a[i]=read();
	while(Q--){
		ll opt=read(),x=read(),y=read();
		if(opt==1) a[x]=y;
		else{
			ll sum=a[x];
			For(i,x+1,y) sum=sum&a[i],sum^=gg;
			printf("%lld\n",sum);
		}
	}
	return 0;
}

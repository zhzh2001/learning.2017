#include<bits/stdc++.h>
#define int long long
#define For(i,x,y) for(ll i=x;i<=y;i++)
#define Rep(i,x,y) for(ll i=x;i>=y;i--)
#define N 500005
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int a[N],bin[32],n,Q;
int sum[N<<2][32],p[32];
inline void pushup(int k,int l,int r){
	int mid=(l+r)>>1;
	For(i,0,31){
		if(sum[k<<1|1][i]==0) sum[k][i]=0;
		else{
			sum[k][i]=sum[k<<1|1][i];
			if(sum[k][i]==r-mid) sum[k][i]+=sum[k<<1][i];
		}
	}
}
inline void build(int k,int l,int r){
	if(l==r){
		For(i,0,31) sum[k][i]=((a[l]&bin[i])>0);
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	pushup(k,l,r);
}
inline void update(int k,int l,int r,int x,int y){
	if(l==r){
		For(i,0,31) sum[k][i]=((y&bin[i])>0);
		return;
	}
	int mid=(l+r)>>1;
	if(x<=mid) update(k<<1,l,mid,x,y);
	else update(k<<1|1,mid+1,r,x,y);
	pushup(k,l,r);
}
bool flag;
inline void query(int k,int l,int r,int x,int y){
	if(l==x&&r==y){
		For(i,0,31){
			if(!flag) p[i]=sum[k][i];
			else{
				if(sum[k][i]==r-l+1) p[i]+=sum[k][i];
				else p[i]=sum[k][i];
			}
		}
		flag=1;
		return;
	}
	int mid=(l+r)>>1;
	if(y<=mid) query(k<<1,l,mid,x,y);
	else if(x>mid) query(k<<1|1,mid+1,r,x,y);
	else query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y);
}
signed main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	n=read();Q=read();
	bin[0]=1;For(i,1,31) bin[i]=bin[i-1]<<1;
	For(i,1,n) a[i]=read();
	build(1,1,n);
//	cout<<sum[1][0]<<" "<<sum[1][1]<<endl;
	while(Q--){
		int opt=read(),x=read(),y=read();
		if(opt==1){
			a[x]=y;
			update(1,1,n,x,y);
		}else{
			memset(p,0,sizeof p);
			flag=0;
			if(x==y){printf("%d\n",a[x]);continue;}
			query(1,1,n,x,y);
			ll sum=0;
			For(i,0,31){
				if(p[i]==0) sum+=bin[i];
				else if(p[i]==y-x){
					if(p[i]&1) sum+=bin[i];
				}
				else if(p[i]==y-x+1){
					p[i]--;
					if(p[i]%2==0) sum+=bin[i];
				}else if(p[i]%2==0) sum+=bin[i];
			}
			printf("%lld\n",sum);
		}
	}
	return 0;
}

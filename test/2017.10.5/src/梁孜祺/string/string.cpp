#include<bits/stdc++.h>
#define For(i,x,y) for(ll i=x;i<=y;i++)
#define Rep(i,x,y) for(ll i=x;i>=y;i--)
#define N 500005
#define ll long long
#define mod 1000000007
using namespace std;
int n,Q,end[1000005],ch[1000005][26],sz;
char s[1000005];
ll ans;
inline void insert(){
	scanf("%s",s+1);
	int x=0,m=strlen(s+1);
	For(i,1,m){
		if(!ch[x][s[i]-'a']) ch[x][s[i]-'a']=++sz;
		else ans+=end[ch[x][s[i]-'a']];
		x=ch[x][s[i]-'a'];
		end[x]++;
	}
}
ll c[3005][3005];
inline void pre(){
	c[0][0]=1;
	For(i,1,3000){
		c[i][0]=1;
		For(j,1,i) c[i][j]=(c[i-1][j-1]+c[i-1][j])%mod;
	}
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	pre();
	scanf("%d%d",&n,&Q);
	For(i,1,n){
		insert();
	}
	ans%=mod;
	while(Q--){
		int x;scanf("%d",&x);
		if(n>=2&&x<=n-2) printf("%lld\n",ans*c[n-2][x]%mod);
		else puts("0");
	}
	return 0;
}

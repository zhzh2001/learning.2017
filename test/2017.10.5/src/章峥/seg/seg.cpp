#include <cstdio>
#include <cctype>
#include <cstring>
FILE *fin, *fout;
namespace fastI
{
const int SIZE = 1000000;
char buf[SIZE], *end = buf + SIZE, *p = end;
template <typename Int>
inline bool read(Int &x)
{
	int remain = end - p;
	if (remain < 20)
	{
		memcpy(buf, p, remain);
		end = buf + remain + fread(buf + remain, 1, SIZE - remain, fin);
		p = buf;
	}
	while (p < end && isspace(*p++))
		;
	if (p == end)
		return false;
	bool neg = false;
	if (*--p == '-')
		neg = true, p++;
	x = 0;
	for (; p < end && isdigit(*p); p++)
		x = x * 10 + *p - '0';
	if (neg)
		x = -x;
	return true;
}
};
namespace fastO
{
const int SIZE = 1000000;
char buf[SIZE], *end = buf + SIZE, *p = buf;
int d[25];
inline void writeln(unsigned x)
{
	if (end - p < 20)
	{
		fwrite(buf, 1, p - buf, fout);
		p = buf;
	}
	if (x < 0)
	{
		*p++ = '-';
		x = -x;
	}
	int k = 0;
	do
		d[++k] = x % 10;
	while (x /= 10);
	for (; k; k--)
		*p++ = d[k] + '0';
	*p++ = '\n';
}
inline void flush()
{
	fwrite(buf, 1, p - buf, fout);
}
};
const int N = 500005;
unsigned a[N];
struct node
{
	unsigned one, zero;
	node operator+(const node &rhs) const
	{
		node ret;
		ret.one = (rhs.one & one) | (rhs.zero & (~one));
		ret.zero = (rhs.one & zero) | (rhs.zero & (~zero));
		return ret;
	}
} tree[1 << 20];
void build(int id, int l, int r)
{
	if (l == r)
	{
		tree[id].one = ~a[l];
		tree[id].zero = 0xffffffffu;
	}
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		tree[id] = tree[id * 2] + tree[id * 2 + 1];
	}
}
void modify(int id, int l, int r, int x, unsigned val)
{
	if (l == r)
	{
		a[x] = val;
		tree[id].one = ~val;
		tree[id].zero = 0xffffffffu;
	}
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(id * 2, l, mid, x, val);
		else
			modify(id * 2 + 1, mid + 1, r, x, val);
		tree[id] = tree[id * 2] + tree[id * 2 + 1];
	}
}
node query(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[id];
	int mid = (l + r) / 2;
	if (R <= mid)
		return query(id * 2, l, mid, L, R);
	if (L > mid)
		return query(id * 2 + 1, mid + 1, r, L, R);
	return query(id * 2, l, mid, L, R) + query(id * 2 + 1, mid + 1, r, L, R);
}
int main()
{
	fin = fopen("seg.in", "r");
	fout = fopen("seg.out", "w");
	int n, q;
	fastI::read(n);
	fastI::read(q);
	for (int i = 1; i <= n; i++)
		fastI::read(a[i]);
	build(1, 1, n);
	while (q--)
	{
		int opt, x;
		unsigned y;
		fastI::read(opt);
		fastI::read(x);
		fastI::read(y);
		if (opt == 1)
			modify(1, 1, n, x, y);
		else if (x == y)
			fastO::writeln(a[x]);
		else
		{
			node ans = query(1, 1, n, x + 1, y);
			fastO::writeln((a[x] & ans.one) | ((~a[x]) & ans.zero));
		}
	}
	fastO::flush();
	return 0;
}
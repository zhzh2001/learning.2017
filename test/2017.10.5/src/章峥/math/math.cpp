#include <fstream>
#include <cstring>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
const int MOD = 100000007, K = 500;
long long n;
int k, r;
struct matrix
{
	long long mat[K];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int i = 0; i < k; i++)
			for (int j = 0; j < k; j++)
				(ret.mat[(i + j) % k] += mat[i] * rhs.mat[j]) %= MOD;
		return ret;
	}
	matrix operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix qpow(matrix a, long long b)
{
	matrix ans;
	ans.mat[0] = 1;
	do
	{
		if (b & 1)
			ans *= a;
		a *= a;
	} while (b /= 2);
	return ans;
}
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		fin >> n >> k >> r;
		matrix trans;
		trans.mat[0]++;
		trans.mat[1 % k]++;
		fout << qpow(trans, n).mat[r] << endl;
	}
	return 0;
}
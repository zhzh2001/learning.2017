#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int L = 1000005, N = 3005, B = 29, K = 200, MOD = 1000000007;
int len[N], c[N][K + 5];
typedef unsigned hash_t;
hash_t *h[N];
char s[L];
int main()
{
	FILE *fin = fopen("string.in", "r"), *fout = fopen("string.out", "w");
	int n, q;
	fscanf(fin, "%d%d", &n, &q);
	for (int i = 1; i <= n; i++)
	{
		fscanf(fin, "%s", s + 1);
		len[i] = strlen(s + 1);
		h[i] = new hash_t[len[i] + 1];
		h[i][0] = 0;
		for (int j = 1; j <= len[i]; j++)
			h[i][j] = h[i][j - 1] * B + s[j] - 'a';
	}
	long long sum = 0;
	for (int i = 1; i <= n; i++)
		for (int j = i + 1; j <= n; j++)
		{
			int l = 0, r = min(len[i], len[j]), ans;
			while (l <= r)
			{
				int mid = (l + r) / 2;
				if (h[i][mid] == h[j][mid])
				{
					l = mid + 1;
					ans = mid;
				}
				else
					r = mid - 1;
			}
			sum = (sum + ans) % MOD;
		}
	c[0][0] = 1;
	for (int i = 1; i <= n; i++)
	{
		c[i][0] = 1;
		for (int j = 1; j <= i && j <= K; j++)
			c[i][j] = (c[i - 1][j] + c[i - 1][j - 1]) % MOD;
	}
	while (q--)
	{
		int k;
		fscanf(fin, "%d", &k);
		fprintf(fout, "%lld\n", sum * c[n - 2][k] % MOD);
	}
	return 0;
}
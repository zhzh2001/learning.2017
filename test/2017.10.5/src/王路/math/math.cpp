#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

ifstream fin("math.in");
ofstream fout("math.out");

typedef long long ll;

const int Mod = 100000007, MaxN = 1000005;

ll fact[MaxN], rfact[MaxN];

ll QuickPow(ll a, ll b) {
	ll res = 1;
	a %= Mod;
	for (; b > 0; b >>= 1LL, a = (a * a) % Mod)
		if (b & 1LL)
			res = (res * a) % Mod;
	return res;
}

ll C(ll n, ll m) {
	if (n < 0 || m < 0 || n - m < 0)
		return 0;
	return ((fact[n] * rfact[m]) % Mod * rfact[n - m]) % Mod;
}

int main() {
	int T;
	fin >> T;
	while (T--) {
		ll n, k, r;
		fin >> n >> k >> r;	
		if (k == 1) {
			fout << QuickPow(2, n) % Mod << endl;	
			continue;
		}
		if (k == 2) {
			fout << QuickPow(2, n - 1) % Mod << endl;
			continue;
		}
		if (n <= 1e6) {
			fact[0] = 1;
			for (int i = 1; i <= n; ++i)
				fact[i] = (fact[i - 1] * i) % Mod;
			for (int i = 0; i <= n; ++i)
				rfact[i] = QuickPow(fact[i], Mod - 2) % Mod;
			ll ans = 0;
			for (int i = 0; i * k + r <= n; ++i)
				ans = (ans + C(n, i * k + r)) % Mod;
			fout << ans << endl;
			continue;	
		}
		fout << 23333333 << endl;
	}
	return 0;
}

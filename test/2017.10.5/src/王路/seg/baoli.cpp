#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <bitset>
#include <iostream>
#include <cassert>
using namespace std;

typedef unsigned int value_t;

ifstream fin("seg.in");
ofstream fout("seg.out");

const int MaxN = 500005;

value_t a[MaxN];

int main() {
	int n, q;
	fin >> n >> q;
	for (int i = 1; i <= n; ++i)
		fin >> a[i];
	for (int i = 1; i <= q; ++i) {
		int opt;
		value_t x, y;
		fin >> opt >> x >> y;
		if (opt == 1)
			a[x] = y;
		else {
			value_t ans = a[x];
			for (unsigned j = x + 1; j <= y; ++j)
				ans = ~(unsigned)(ans & a[j]);
			fout << ans << endl;
		}
	}
	return 0;
}

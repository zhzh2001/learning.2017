#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

ofstream fout("seg.in");

int main() {
	srand(GetTickCount());
	int n = 100000, q = 100000;
	fout << n << ' ' << q << endl;
	for (int i = 1; i <= n; ++i)
		fout << rand() % n + 1 << ' ';
	fout << endl;
	for (int i = 1; i <= q; ++i) {
		int opt = rand() % 2;
		int x = rand() % n + 1;
		fout << opt + 1 << ' ' << x << ' ' << x + (rand() % (n - x)) + 1 << endl;
	}
	return 0;
}

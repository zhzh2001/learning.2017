#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <bitset>
#include <iostream>
#include <cassert>
using namespace std;

typedef unsigned int value_t;

ofstream fout("seg.out");

const int MaxN = 500005;
bitset<MaxN * 8> tr1[32], tr0[32]/*, orig[32]*/;
value_t a[MaxN];

inline value_t nand(value_t x, value_t y) {
	return bool(x + y < 2);
}

inline void pushup(int x, int k) {
	tr0[k][x] = (tr0[k][x << 1] == 0 ? tr0[k][x << 1 | 1] : tr1[k][x << 1 | 1]);
	tr1[k][x] = (tr1[k][x << 1] == 0 ? tr0[k][x << 1 | 1] : tr1[k][x << 1 | 1]);
//	orig[k][x] = nand(orig[k][x << 1], orig[k][x << 1 | 1]);
//	if (k == 1) {
//		cout << x << ":" << endl;
//		cout << tr0[k][x] << ' ' << tr0[k][x << 1] << ' ';
//		cout << tr1[k][x] << ' ' << tr1[k][x << 1] << ' ';
//		cout << tr0[k][x << 1 | 1] << ' ' << tr1[k][x << 1 | 1] << endl;
//	}
}

void build(int x, int l, int r, int k) {
	if (l == r) {
		tr1[k][x] = nand(1, (a[l] >> k) & 1);
		tr0[k][x] = nand(0, (a[l] >> k) & 1);
//		orig[k][x] = (a[l] >> k) & 1;
		return;
	}
	int mid = (l + r) >> 1;
	build(x << 1, l, mid, k);
	build(x << 1 | 1, mid + 1, r, k);
	pushup(x, k);
}

void modify(int x, int l, int r, int pos, value_t val, int k) {
	if (l == r) {
		tr1[k][x] = nand(1, (val >> k) & 1);
		tr0[k][x] = nand(0, (val >> k) & 1);
//		orig[k][x] = (val >> k) & 1;
		return;	
	}
	int mid = (l + r) >> 1;
	if (pos <= mid) modify(x << 1, l, mid, pos, val, k);
		else modify(x << 1 | 1, mid + 1, r, pos, val, k);
	pushup(x, k);
}

value_t query(int x, int l, int r, int ql, int qr, int k, int opt) {
	if (ql <= l && r <= qr) {
		if (l == ql) {
			if (l == r)
				return (a[x] >> k) & 1;
			int mid = (l + r) >> 1;
			if (qr <= mid)
				return query(x << 1, l, mid, ql, qr, k, opt);
			if (ql > mid)
				return query(x << 1 | 1, mid + 1, r, ql, qr, k, opt);
			value_t now = query(x << 1, l, mid, ql, qr, k, opt);
			return query(x << 1 | 1, mid + 1, r, ql, qr, k, now);
		}
		return opt == 0 ? tr0[k][x] : tr1[k][x];
	}
	if (ql > r || qr < l)
		abort();
	int mid = (l + r) >> 1;
	if (qr <= mid)
		return query(x << 1, l, mid, ql, qr, k, opt);
	if (ql > mid)
		return query(x << 1 | 1, mid + 1, r, ql, qr, k, opt);
	value_t now = query(x << 1, l, mid, ql, qr, k, opt);
	return query(x << 1 | 1, mid + 1, r, ql, qr, k, now);
}

const unsigned L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline unsigned read(){
	unsigned x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

signed main() {
	freopen("seg.in", "r", stdin);
	int n = read(), q = read();
	for (int i = 1; i <= n; ++i)
		a[i] = read();
	if (n <= 3000) {
		for (int i = 1; i <= q; ++i) {
			int opt;
			value_t x, y;
			opt = read(), x = read(), y = read();
			if (opt == 1)
				a[x] = y;
			else {
				value_t ans = a[x];
				for (unsigned j = x + 1; j <= y; ++j)
					ans = ~(unsigned)(ans & a[j]);
				fout << ans << endl;
			}
		}	
		return 0;
	}
	for (int i = 0; i < 32; ++i)
		build(1, 1, n, i);
//	cout << tr0[1][1] << ' ' << tr1[1][1] << endl;
//	int cnt = 0;
	for (int i = 0; i < q; ++i) {
		int opt;
		value_t x, y;
		opt = read(), x = read(), y = read();
		if (opt == 1) {
			for (int k = 0; k < 32; ++k)
				modify(1, 1, n, x, y, k);
		} else {
			value_t ans = 0;
			for (int k = 0; k < 32; ++k) {
				ans |= (query(1, 1, n, x, y, k, 0) << k);
			}
			fout << ans << endl;
		}
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#define int unsigned int
using namespace std;
struct lsg{int z,o;}sum[2000000];
int a[1000000],n,q,x,y;
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1;else kk=c-'0';c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return k*kk;
}
inline int hb(int x,lsg d){
	int sum=0;for (int i=0;i<32;i++)sum+=x&(1<<i)?d.o&(1<<i):d.z&(1<<i);
	return sum;
} 
inline void build(int l,int r,int d){
	if (l==r){sum[d].z=-1;sum[d].o=~a[l];return;}
	int m=(l+r)/2;build(l,m,d*2);build(m+1,r,d*2+1);
	sum[d].z=hb(sum[d*2].z,sum[d*2+1]);sum[d].o=hb(sum[d*2].o,sum[d*2+1]);
}
inline void putit(int x,int l,int r,int d){
	if (l==r){sum[d].z=-1;sum[d].o=~a[l];return;}
	int m=(l+r)/2;if (x<=m)putit(x,l,m,d*2);else putit(x,m+1,r,d*2+1);
	sum[d].z=hb(sum[d*2].z,sum[d*2+1]);sum[d].o=hb(sum[d*2].o,sum[d*2+1]);
}
inline lsg findit(int x,int y,int l,int r,int d){
	if (x<=l&&r<=y)return sum[d];
	int m=(l+r)/2;
	if (y<=m)return findit(x,y,l,m,d*2); 
	if (x>m)return findit(x,y,m+1,r,d*2+1);
	lsg k=findit(x,y,l,m,d*2),kk=findit(x,y,m+1,r,d*2+1);
	k.z=hb(k.z,kk);k.o=hb(k.o,kk);return k;
}
signed main(){
	freopen("seg.in","r",stdin);freopen("seg.out","w",stdout);
	n=read();q=read();for (int i=1;i<=n;i++)a[i]=read();
	build(1,n,1);
	while (q--)
		if (read()==1){
			x=read();a[x]=read();putit(x,1,n,1);
		}else{
			x=read();y=read();if (x>y)swap(x,y);
			if (x==y)cout<<a[x]<<endl;
				else cout<<hb(a[x],findit(x+1,y,1,n,1))<<endl;
		}
}
/*
2 3
1 2
2 1 2
1 1 3
2 1 2
*/

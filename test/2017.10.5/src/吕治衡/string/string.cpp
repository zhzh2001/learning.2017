#include<bits/stdc++.h>
using namespace std;
int n,q,lsg,jc[10000],N[10000],ans,sum,k,a[1000000][26],kk,f[1000000],l,d;
char s[1000000];
int ksm(int x,int y){
	if (y==1)return x;
	int k=ksm(x,y/2);
	if (y&1)return 1ll*k*k%lsg*x%lsg;
		else return 1ll*k*k%lsg;
}
int dfs(int x){
	long long ans=1ll*f[x]*(f[x]-1)/2;
	for (int i=0;i<=25;i++)
		if (a[x][i])ans+=dfs(a[x][i]);
	return ans%lsg;
}
int main(){
	freopen("string.in","r",stdin);freopen("string.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>q;lsg=1e9+7;jc[0]=1;
	for (int i=1;i<=n;i++)jc[i]=1ll*jc[i-1]*i%lsg;
	N[n]=ksm(jc[n],lsg-2);for (int i=n-1;i>=0;i--)N[i]=1ll*N[i+1]*(i+1)%lsg;
	for (int i=1;i<=n;i++){
		cin>>s;l=strlen(s);d=0;
		for (int i=0;i<l;i++){
			if (!a[d][s[i]-'a'])a[d][s[i]-'a']=++kk;
			d=a[d][s[i]-'a'];f[d]++;
		}
	}ans=dfs(0);
	while (q--){
		cin>>k;k=n-k;if (k==1){cout<<0<<endl;continue;}
		sum=1ll*jc[n-2]*N[n-k]%lsg*N[k-2]%lsg;
		cout<<1ll*sum*ans%lsg<<endl;
	}
}
/*
3 2
aab
aba
abb
1
0
*/

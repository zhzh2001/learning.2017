#include<bits/stdc++.h>
#define int long long
using namespace std;
int q,lsg,n,k,r,sum,jc[2000000],N[2000000];
int ksm(int x,int y){
	if (y==0)return 1;if (y==1)return x;
	int k=ksm(x,y/2);
	if (y&1)return 1ll*k*k%lsg*x%lsg;
		else return 1ll*k*k%lsg;
}
signed main(){
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>q;lsg=1e8+7;jc[0]=1;
	for (int i=1;i<=1000000;i++)jc[i]=jc[i-1]*i%lsg;
	N[1000000]=ksm(jc[1000000],lsg-2);for (int i=999999;i>=0;i--)N[i]=N[i+1]*(i+1)%lsg;
	while (q--){
		cin>>n>>k>>r;sum=0;
		if (n==0){cout<<(int)(r==0)<<endl;continue;}
		if (k==1){cout<<ksm(2,n)<<endl;continue;}
		if (k==2){cout<<ksm(2,n-1)<<endl;continue;}
		for (int i=r;i<=n;i+=k)(sum+=N[i]*N[n-i])%=lsg;
		cout<<sum*jc[n]%lsg<<endl;
	} 
}

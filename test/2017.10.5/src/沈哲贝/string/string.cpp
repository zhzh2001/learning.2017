#include<cstdio>
#include<vector>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 600010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
const ll mod=1000000007;
ll f[3020][3020],cnt,n,Q,ans,sz[1000010];
int c[1000010][27];
char s[1000010];
void insert(){
	ll t=strlen(s+1),x=0;
	For(i,1,t){
		if (!c[x][s[i]-'a'])	c[x][s[i]-'a']=++cnt;
		x=c[x][s[i]-'a'];	ans=(ans+(sz[x]++))%mod;
	}
}
ll ppow(ll x,ll k){
	ll ans=1;
	while(k){
		if (k&1)	ans=ans*x%mod;	x=x*x%mod;
		k>>=1;
	}return ans;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	For(i,1,3010){
		f[i][0]=f[i][i]=1;
		For(j,1,i-1)	f[i][j]=(f[i-1][j-1]+f[i-1][j])%mod;
	}
	n=read();	Q=read();
	For(i,1,n)	scanf("%s",s+1),insert();
	while(Q--){
		ll k=read();
		if (k>=n-1)	puts("0");
		else	writeln(f[n][k]*f[n-k][2]%mod*ppow(f[n][2],mod-2)%mod*ans%mod);
	}
}

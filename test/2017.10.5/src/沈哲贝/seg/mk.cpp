#include<cstdio>
#include<memory.h>
#include<ctime>
#include<algorithm>
#define ll long long
#define maxn 1010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
	ll n=500000,Q=500000;
inline ll value(){	return 1LL*rand()*rand()*(rand()%4);	}
inline ll pos(){	return rand()*rand()%n+1;	}
int main(){
	freopen("seg.in","w",stdout);
	srand(time(0));
	printf("%lld %lld\n",n,Q);
	For(i,1,n)	printf("%lld ",value());
	puts("");
	while(Q--){
		ll opt=rand()%2;
		if (opt&1)	printf("1 %lld %lld\n",value(),pos());
		else{
			ll x=pos(),y=pos();	printf("2 %lld %lld\n",min(x,y),max(x,y));
		}
	}
}

#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 1000010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
ll n,k,r,fac[maxn],inv[maxn];
const ll mod=100000007;
ll ppow(ll x,ll k){
	ll ans=1;
	while(k){
		if (k&1)	ans=ans*x%mod;	x=x*x%mod;
		k>>=1;
	}return ans;
}
inline ll c(ll n,ll m){	return fac[n]*inv[m]%mod*inv[n-m]%mod;	}
int main(){
	ll T=read();
	fac[0]=inv[0]=inv[1]=fac[1]=1;
	For(i,2,1000000)	fac[i]=fac[i-1]*i%mod,inv[i]=inv[mod%i]*(mod-mod/i)%mod;
	For(i,2,1000000)	inv[i]=inv[i-1]*inv[i]%mod;
	while(T--){
		n=read();	k=read();	r=read();	ll ans=0;
		if (k==1){	writeln(ppow(2,n));	continue;	}
		if (k==2){	writeln(ppow(2,n-1));continue;	}
		for(ll i=r;i<=n;i+=k)	ans=(ans+c(n,i))%mod;
		writeln(ans);
	}
}

#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 510
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll mod=1e8+7;
ll f[maxn],g[maxn],h[maxn],n,k,r;
void cheng(ll a[],ll b[]){
	memset(h,0,sizeof h);
	For(i,0,k-1)	For(j,0,k-1)	h[(i+j)%k]=(h[(i+j)%k]+a[i]*b[j])%mod;
	memcpy(a,h,sizeof h);
}
ll ppow(ll x,ll k){
	ll ans=1;
	while(k){
		if (k&1)	ans=ans*x%mod;	x=x*x%mod;
		k>>=1;
	}return ans;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll T=read();
	while(T--){
		memset(f,0,sizeof f);
		memset(g,0,sizeof g);
		n=read();	k=read();	r=read();
		if (k==1){	writeln(ppow(2,n));	continue;	}
		f[0]=1;	g[0]=g[1]=1;
		while(n){
			if (n&1)	cheng(f,g);	cheng(g,g);
			n>>=1;
		}ll ans=0;
		writeln(f[r]);
	}
}

//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
const int mod=100000007;
ifstream fin("math.in");
ofstream fout("math.out");
long long T,n,k,r;
long long fac[1000003],inv[1000003];
long long qpow(long long a,long long b){
	if(b==1){
		return a%mod;
	}
	long long ret=qpow(a,b/2);
	ret=ret*ret%mod;
	if(b%2==1){
		ret=ret*a%mod;
	}
	return ret;
}
long long C(long long n,long long m){
	if(n<m){
		return 0;
	}
	return fac[n]*inv[m]%mod*inv[n-m]%mod;
}
int main(){
	fac[0]=1;
	for(long long i=1;i<=1000000;i++){
		fac[i]=(fac[i-1]*i)%mod;
	}
	inv[1000000]=qpow(fac[1000000],mod-2);
	for(long long i=1000000-1;i>=0;i--){
		inv[i]=(inv[i+1]*(i+1))%mod;
	}
	fin>>T;
	while(T--){
		long long ans=0;
		fin>>n>>k>>r;
		if(n==0){
			fout<<0<<endl;
			continue;
		}
		if(k==1){
			if(r==0){
				fout<<(qpow(2,n)+mod)%mod<<endl;
				continue;
			}else if(r==1){
				fout<<(qpow(2,n)+mod)%mod<<endl;
				continue;
			}
		}else if(k==2){
			if(r==0){
				fout<<(qpow(2,n-1)+mod)%mod<<endl;
				continue;
			}else if(r==1){
				fout<<(qpow(2,n-1)+mod)%mod<<endl;
				continue;
			}else{
				fout<<(qpow(2,n-1)+mod)%mod<<endl;
				continue;
			}
		}
		for(int i=0;i*k+r<=n;i++){
			ans+=C(n,i*k+r);
			ans%=mod;
		}
		fout<<ans<<endl;
	}
	return 0;
}
/*

in:
2
2 2 1
3 3 2

out:
2
3

*/

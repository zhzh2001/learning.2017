#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
ifstream fin("string.in");
ofstream fout("string.out");
using namespace std;
const int mod=1000000007;
int n,q,len,tot,k;
int ch[1000003][26],sz[1000003],c[3005][220],num[3005];
long long ans;
char s[1000003];
int main(){
	cin>>n>>q;
	for(int i=1;i<=n;i++){
		cin>>s+1;
		for(int j=1,x=0;j<=strlen(s+1);j++){
			if(!ch[x][s[j]-'a']){
				ch[x][s[j]-'a']=++tot;
			}
			x=ch[x][s[j]-'a'];
			sz[x]++;
		}
	}
	for(int i=1;i<=tot;i++){
		num[sz[i]]++;
	}
	for(int i=0;i<=n;i++){
		c[i][0]=1;
		for(int j=1;j<=i&&j<=215;j++){
			c[i][j]=(c[i-1][j]+c[i-1][j-1])%mo;
		}
	}
	while(q--){
		scanf("%d",&k);
		ans=0;
		for(int i=1;i<=n;i++){
			if(num[i]){
				for(int j=0;j<=k&&j<=i;j++){
					ans=(ans+1ll*(i-j)*(i-j-1)/2*c[i][j]%mo*c[n-i][k-j]%mo*num[i])%mo;
				}
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}
/*

in:
3 1
aab
abb
aba
1

out:
4

*/
/*

in:
3 1
aaa
aaa
aaa

*/

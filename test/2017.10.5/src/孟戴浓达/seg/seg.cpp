//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
const long long mod=4294967296;
ifstream fin("seg.in");
ofstream fout("seg.out");
int n,q;
long long num[500003];
struct tree{
	int yes1,yes0,val;
}t[1800003][32];
int xx[500003][33];
int mdnd[33];
int mengdai0,mengdai1;
tree xxx;
inline void pushup(int rt,int pos){
	if(t[rt*2][pos].val==1){
		if(t[rt*2+1][pos].yes1==1) t[rt][pos].val=!t[rt*2+1][pos].val;
		else t[rt][pos].val=t[rt*2+1][pos].val;
	}else{
		if(t[rt*2+1][pos].yes0==1) t[rt][pos].val=!t[rt*2+1][pos].val;
		else t[rt][pos].val=t[rt*2+1][pos].val;
	}
	if(t[rt*2][pos].yes1==1){
		if(t[rt*2][pos].val==1){
			if(t[rt*2+1][pos].yes0==1) t[rt][pos].yes1==1;
			else t[rt][pos].yes1==0;
		}else{
			if(t[rt*2+1][pos].yes1==1) t[rt][pos].yes1==1;
			else t[rt][pos].yes1==0;
		}
	}
	if(t[rt*2][pos].yes0==1){
		if(t[rt*2][pos].val==1){
			if(t[rt*2+1][pos].yes0==1) t[rt][pos].yes0==1;
			else t[rt][pos].yes0==0;
		}else{
			if(t[rt*2+1][pos].yes1==1) t[rt][pos].yes0==1;
			else t[rt][pos].yes0==0;
		}
	}
}
void build(int rt,int l,int r,int pos){
	if(l==r){
		t[rt][pos].val=xx[l][pos];
		if(xx[l][pos]==0){
			t[rt][pos].yes0=1;
			t[rt][pos].yes1=1;
		}else{
			t[rt][pos].yes0=0;
			t[rt][pos].yes1=1;
		}
		return;
	}
	int mid=(l+r)>>1;
	build(rt*2,l,mid,pos),build(rt*2+1,mid+1,r,pos);
	pushup(rt,pos);
}
tree uni(tree a,tree b){
	tree x;
	if(a.val==1){
		if(b.yes1==1) x.val=!b.val;
		else x.val=b.val;
	}else{
		if(b.yes0==1) x.val=!b.val;
		else x.val=b.val;
	}
	if(a.yes1==1){
		if(a.val==1){
			if(b.yes0==1) x.yes1==1;
			else x.yes1==0;
		}else{
			if(b.yes1==1) x.yes1==1;
			else x.yes1==0;
		}
	}
	if(a.yes0==1){
		if(a.val==1){
			if(b.yes0==1) x.yes0==1;
			else x.yes0==0;
		}else{
			if(b.yes1==1) x.yes0==1;
			else x.yes0==0;
		}
	}
	return x;
}
tree query(int rt,int l,int r,int x,int y,int pos){
	if(l==x&&r==y){
		return t[rt][pos];
	}
	int mid=(x+y)>>1;
	if(y<=mid){
		return query(rt*2,l,mid,x,y,pos);
	}else if(x>mid){
		return query(rt*2+1,mid+1,r,x,y,pos);
	}else{
		return uni(query(rt*2,l,mid,x,mid,pos),query(rt*2+1,mid+1,r,mid+1,y,pos));
	}
}
void update(int rt,int l,int r,int x,int y,int xxx,int pos){
	if(l==x&&r==y){
		t[rt][pos].val=xxx;
		if(xxx==0){
			t[rt][pos].yes0=1;
			t[rt][pos].yes1=1;
		}else{
			t[rt][pos].yes0=0;
			t[rt][pos].yes1=1;
		}
		return;
	}
	int mid=(x+y)>>1;
	if(y<=mid){
		update(rt*2,l,mid,x,y,xxx,pos);
	}else if(x>mid){
		update(rt*2+1,mid+1,r,x,y,xxx,pos);
	}else{
		update(rt*2,l,mid,x,mid,xxx,pos);
		update(rt*2+1,mid+1,r,mid+1,y,xxx,pos);
	}
	pushup(rt,pos);
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=n;i++){
		fin>>num[i];
	}
	for(int i=1;i<=n;i++){
		for(long long j=31;j>=0;j--){
			if(num[i]&(1<<j)){
				xx[i][j]=1;
			}
		}
	}
	for(int i=0;i<=31;i++){
		build(1,1,n,i);
	}
	for(int i=1;i<=q;i++){
		long long opt,x,y,ans;
		fin>>opt>>x>>y;
		if(opt==1){
			for(long long j=31;j>=0;j--){
				if(y&(1<<j)){
					update(1,1,n,x,x,1,j);
				}else{
					update(1,1,n,x,x,0,j);
				}
			}
		}else{
			ans=0;
			for(long long j=31;j>=0;j--){
				xxx=query(1,1,n,x,y,j);
				if(xxx.val){
					ans=ans+(1<<j);
					ans=(ans+mod)%mod;
				}
			}
			ans=(ans+mod)%mod;
			fout<<ans<<endl;
		}
	}
	return 0;
}
/*

in:
2 3
1 2
2 1 2
1 1 3
2 1 2

out:
4294967295
4294967293

*/

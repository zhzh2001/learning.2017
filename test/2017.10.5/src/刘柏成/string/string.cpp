#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct trie{
	int ch[1000003][26],sum[1000003];
	int sz;
	trie(){
		sz=1;
	}
	ll ans=0;
	void ins(char * s){
		int l=strlen(s),now=1;
		//sum[now]++;
		for(int i=0;i<l;i++){
			int &u=ch[now][s[i]-'a'];
			if (!u)
				u=++sz;
			ans+=sum[u];
			if (ans>=mod)
				ans-=mod;
			sum[u]++;
			now=u;
		}
	}
}t;
int c[3003][3003];
char s[1000003];
int main(){
	init();
	int n=readint(),q=readint();
	c[0][0]=1;
	for(int i=1;i<=n;i++){
		c[i][0]=1;
		for(int j=1;j<=i;j++){
			c[i][j]=c[i-1][j]+c[i-1][j-1];
			if (c[i][j]>=mod)
				c[i][j]-=mod;
		}
	}
	for(int i=1;i<=n;i++){
		readstr(s);
		t.ins(s);
	}
	while(q--){
		int k=readint();
		if (k+2>n)
			puts("0");
		else printf("%lld\n",(long long)c[n-2][k]*t.ans%mod);
	}
}
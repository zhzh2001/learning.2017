#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
uint readint(){
	uint val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
void writeint(uint x){
	if (x>=10)
		writeint(x/10);
	putchar('0'+x%10);
}
const int maxn=500002;
struct atom{
	uint x,y;
	atom(){
		x=0,y=~0U;
	}
	atom(uint k){
		x=~0U;
		y=~k;
	}
	atom(uint x,uint y){
		this->x=x;
		this->y=y;
	}	
	atom operator +(const atom& rhs)const{
		return (atom){((~x)&rhs.x)^(x&rhs.y),((~y)&rhs.x)^(y&rhs.y)};
	}
};
struct segtree{
	int n;
	atom t[maxn*4];
	uint a[maxn];
	void build(int o,int l,int r){
		if (l==r){
			t[o]=atom(a[l]);
			return;
		}
		int mid=(l+r)/2;
		build(o*2,l,mid);
		build(o*2+1,mid+1,r);
		t[o]=t[o*2]+t[o*2+1];
	}
	void init(int n){
		this->n=n;
		build(1,1,n);
	}
	int p;
	uint v;
	void update(int o,int l,int r){
		if (l==r){
			t[o]=atom(v);
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(o*2,l,mid);
		else update(o*2+1,mid+1,r);
		t[o]=t[o*2]+t[o*2+1];
	}
	void update(int p,uint v){
		this->p=p,this->v=v;
		a[p]=v;
		update(1,1,n);
	}
	int ql,qr;
	atom query(int o,int l,int r){
		if (ql<=l && qr>=r)
			return t[o];
		int mid=(l+r)/2;
		atom res;
		if (ql<=mid)
			res=res+query(o*2,l,mid);
		if (qr>mid)
			res=res+query(o*2+1,mid+1,r);
		return res;
	}
	uint query(int l,int r){
		if (l==r)
			return a[l];
		ql=l+1,qr=r;
		atom qwq=query(1,1,n);
		return ((~a[l])&qwq.x)^(a[l]&qwq.y);
	}
}t;
int main(){
	init();
	//printf("%u\n",sizeof t);
	int n=readint(),q=readint();
	for(int i=1;i<=n;i++)
		t.a[i]=readint();
	t.init(n);
	while(q--){
		int op=readint(),x=readint();
		uint y=readint();
		if (op==1)
			t.update(x,y);
		else writeint(t.query(x,y)),putchar('\n');
	}
	//writeint(11);
	//printf("%d\n",clock());
}
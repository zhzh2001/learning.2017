#include <bits/stdc++.h>
using namespace std;
int main(){
	freopen("seg.in","w",stdout);
	int n=500000,q=500000;
	printf("%d %d\n",n,q);
	mt19937 w;
	w.seed(time(0));
	for(int i=1;i<=n;i++)
		printf("%u ",w());
	putchar('\n');
	while(q--){
		if (w()%2)
			printf("1 %d %u\n",w()%n+1,w());
		else {
			int l=w()%n+1,r=w()%n+1;
			if (l>r)
				swap(l,r);
			printf("2 %d %d\n",l,r);
		}
	}
}
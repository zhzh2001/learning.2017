#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=100000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
ll readint(){
	ll val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int k;
ll tmp[501];
void mul(ll* x,ll *y){ //c[0][i]=sigma a[0][j]*b[j][i]
	for(int i=0;i<k;i++)
		tmp[i]=0;
	for(int i=0;i<k;i++)
		if (x[i]){
			int now=i;
			for(int j=0;j<k;j++){
				tmp[now]+=x[i]*y[j];
				now++;
				if (now==k)
					now=0;
			}
		}
	for(int i=0;i<k;i++)
		x[i]=tmp[i]%mod;
}
ll mt[501],ans[501];
void power(ll x){
	ans[0]=1;
	while(x){
		if (x&1)
			mul(ans,mt);
		mul(mt,mt);
		x>>=1;
	}
}
int main(){
	init();
	int T=readint();
	while(T--){
		ll n=readint();
		k=readint();
		int r=readint();
		for(int i=0;i<k;i++)
			mt[i]=ans[i]=0;
		if (k==1)
			mt[0]=2;
		else mt[0]=mt[1]=1;
		power(n);
		printf("%lld\n",ans[r]);
	}
	//printf("%d\n",clock());
}
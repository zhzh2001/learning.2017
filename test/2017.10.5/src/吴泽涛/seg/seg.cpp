#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define For(i,j,k) for(unsigned int i=j;i<=k;i++) 
#define ll long long 
using namespace std;
const unsigned int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline unsigned int read(){
	unsigned int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

inline void write(unsigned int x) 
{
	if( x > 9 ) write(x/10) ; 
	putchar(x%10+48) ; 
} 
inline void writeln(unsigned int x) 
{
	write(x) ;
	putchar('\n') ; 
}

const unsigned int N = 500011 ; 
unsigned int n,Q,type,x,y,sum ; 
unsigned int a[N] ; 


signed main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout); 
	n = read() ; Q = read() ; 
	For(i,1,n) a[i]=read() ; 
	while(Q--) {
		type = read() ; x = read() ; y = read() ; 
		if(type==2) {
			sum = a[x] ; 
			For(i,x+1,y) sum=~(sum&a[i]) ; 
			writeln(sum) ; 
		}
		else a[x]=y ; 
	}
	return 0 ; 
}



#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}


const int mod = 100000007 ; 
ll TT,n,k,r ; 
int b[70],c[2][1000011] ; 

inline ll ksm(ll x,ll y) 
{
	int now = 0 ; 
	ll ans = 1 ;  
	while(y) {
		b[++now] = y % 2 ; 
		y/=2 ; 
	}
	Dow(i,now,1) {
		ans = ans*ans %mod ; 
		if(b[i]) ans = ans*x % mod ; 
	}
	return ans ; 
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout); 
	TT = read() ; 
	while(TT--) {
		n = read() ; k = read() ; r = read() ; 
		if(k==1) {
			printf("%lld\n",ksm(2,n)) ; 
			continue ; 
		}
		if(k==2&&n%2!=0) {
			ll all = ksm(2,n) ; 
			all=all*ksm(2,mod-2) %mod ; 
			printf("%lld\n",all) ; 
			continue ; 
		} 
		For(i,1,n+1) c[1][i]=0;
		For(i,1,n+1) c[0][i]=0;
		c[1][1]=1; 
		For(i,2,n+1) 
			For(j,1,i) 
				c[i&1][j] = (c[i&1^1][j-1] + c[i&1^1][j]) % mod ;  
		int sum = 0,t = n&1^1 ; 
		For(i,1,n+1) 
			if((i-1) % k==r) sum=(sum+c[t][i]) % mod ; 
		printf("%d\n",sum) ; 
	}
	return 0 ; 
}  

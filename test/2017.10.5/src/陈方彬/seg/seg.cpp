#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int R(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
const int N=1e4;
int n,m,x,y,z;
unsigned int a[N];
int main()
{
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout); 
	n=R();m=R();
	for(int i=1;i<=n;i++)a[i]=R();
	while(m--){
		z=R();x=R();y=R();
		if(z==1)a[x]=y;else{
			unsigned int ans=a[x];
			for(int i=x+1;i<=y;i++)
				ans=~(ans&a[i]);
			cout<<ans<<endl;
		}
	}
}

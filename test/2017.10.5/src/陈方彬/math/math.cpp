#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=7e3+5;
Ll C[N][N];
Ll n,m,k,r,mo=1e8+7;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout); 
	for(int i=0;i<=7000;i++)
		for(int j=i;j>=0;j--)
			if(j==0||j==i)C[i][j]=1;else 
			C[i][j]=(C[i-1][j-1]+C[i-1][j])%mo;
	scanf("%lld",&m);
	while(m--){
		cin>>n>>k>>r;
		if(n>=mo)printf("0\n");else{
			Ll ans=0;
			for(Ll i=0;i*k+r<=n;i++)
				ans=(ans+C[n][i*k+r])%mo;
			cout<<ans<<endl;
		}
	}
}

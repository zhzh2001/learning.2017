#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e6+5;
struct cs{int n[26];Ll v,sum;}c[N];
Ll C[3005][205];
char s[N];
int n,q,k,l,ll;
Ll ans,mo=1e9+7;
void init(int x,int y){
	if(y>l){c[x].v++;return;}
	if(!c[x].n[s[y]-'a'])c[x].n[s[y]-'a']=++ll;
	init(c[x].n[s[y]-'a'],y+1);
}
void dfs(int x,int dep){
	c[x].sum=c[x].v;
	for(int i=0;i<26;i++)
		if(c[x].n[i]){dfs(c[x].n[i],dep+1);c[x].sum+=c[c[x].n[i]].sum;}
	Ll sum=c[x].sum;
	for(int i=0;i<26;i++)
		if(c[x].n[i]){
			sum-=c[c[x].n[i]].sum;
			ans+=c[c[x].n[i]].sum*sum%mo*dep%mo;
		}
	if(c[x].v)ans+=c[x].v*(c[x].v-1)/2%mo*dep%mo;
	ans%=mo;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout); 
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++){
		scanf("%s",s+1);
		l=strlen(s+1);
		init(0,1);
	}
	dfs(0,0);
	for(int i=0;i<=n;i++)
		for(int j=min(200,i);j>=0;j--)
			if(j==0||j==i)C[i][j]=1;else 
			C[i][j]=(C[i-1][j-1]+C[i-1][j])%mo;
	while(q--){
		scanf("%d",&k);
		printf("%lld\n",ans*C[n-2][k]%mo);
	}
}

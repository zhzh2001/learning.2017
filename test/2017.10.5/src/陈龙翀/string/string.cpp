/*
前途是光明的
道路是曲折的
*/ 
#include<cstdio>
#include<algorithm>
#include<cstring>
#define SXR 1000000007
using namespace std;
int sum[1000010],ans;
int trie[1000010][27],size=1,n,q,C[3001][201],k;
char s[1000010];
void insert(int l)
{
	int now=1;
	for(int i=0; i<l; i++)
	{
		int &t=trie[now][s[i]-'a'];
		if (!t) t=++size;
		ans=(ans+sum[t])%SXR;
		sum[t]++;
		now=t;
	}
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d%d",&n,&q);
	C[0][0]=1;
	for(int i=1; i<=n; i++)
	{
		C[i][0]=1;
		for(int j=1; j<=i; j++)
			C[i][j]=(C[i-1][j]+C[i-1][j-1])%SXR;
	}
	for (int i=1; i<=n; i++)
	{
		scanf("%s",s);
		insert(strlen(s));
	}
	while(q--)
	{
		scanf("%d",&k);
		int Ans=1ll*ans*C[n-2][k]%SXR;
		printf("%d\n",Ans);
	}
}

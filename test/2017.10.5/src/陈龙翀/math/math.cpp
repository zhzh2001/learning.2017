/*
他们说，念念不忘，必有回响
*/ 
#include<cstdio>
#include<cstring>
#define LL long long
#define SXR 100000007
int T,k,r;
long long n;
struct matrix
{
	int a[1001];
	void clear()
	{
		memset(a,0,sizeof(a)); 
	}
}Ans;
matrix operator * (matrix a,matrix b)
{
	matrix c;
	c.clear();
	for (int i=0; i<k; i++)
		for (int j=0; j<k; j++)
		{
			int t=(i+j)%k;
			c.a[t]=(c.a[t]+1ll*a.a[i]*b.a[j]%SXR)%SXR;
		}
	return c;
}
matrix operator ^ (matrix a,LL b) 
{
	matrix c;
	c.clear();
	c.a[0]=1;
	for (;b;b/=2,a=a*a) 
		if (b&1) c=c*a;
	return c;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		Ans.clear();
		scanf("%I64d%d%d",&n,&k,&r);
		if (k==1) Ans.a[0]=2;
		else Ans.a[0]=1,Ans.a[1]=1;
		Ans=Ans^n;
		printf("%d\n",Ans.a[r]);
	}
}

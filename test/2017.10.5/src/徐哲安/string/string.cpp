#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int maxm = 1000009;
const int MOD = 1000000007;
int T[maxm][26], rk[maxm], dep[maxm], f[maxm], size = 1;
char s[maxm]; int n, p, Q, base = 0, res, res2, K;

bool cmp(int a, int b){
	return dep[a] > dep[b];
}

int power(int x, int p){
	int res = 1;
	while (p){
		if (p&1) res = 1LL * res * x % MOD;
		x = 1LL * x * x % MOD; p >>= 1;
	}
	return res;
}

void insert(char s[]){
	int o = 1;
	for (int i=0; s[i]; i++){
		if (!T[o][s[i]-'a']){
			T[o][s[i]-'a'] = ++size;
			dep[size] = i+1; rk[size] = size;
		}
		o = T[o][s[i]-'a'];
	}
	f[o]++;
}

int C(int a, int b){
	if (b < 0 || b > a) return 0;
	int res = 1;
	for (int i=a-b+1; i<=a; i++)
		res = 1LL * res * i % MOD;
	for (int i=1; i<=b; i++)
		res = 1LL * res * power(i, MOD-2) % MOD;
	return res;
}

int main(){
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
	scanf("%d%d", &n, &Q); getchar();
	dep[1] = 0; rk[1] = 1;
	for (int i=1; i<=n; i++){
		gets(s); insert(s);
	}
	sort(rk+1, rk+size+1, cmp);
	for (int i=1; i<=size; i++){
		p = rk[i]; res = 0; res2 = 0;
		if (f[p]) base = (base + 1LL * (f[p]-1) * f[p] / 2 % MOD * dep[p]) % MOD;
		for (int j=0; j<=25; j++)
			if (T[p][j]){
				res = (res + f[T[p][j]]) % MOD;
				res2 = (res2 + 1LL * f[T[p][j]] * f[T[p][j]]) % MOD;
				f[p] = (f[p] + f[T[p][j]]) % MOD;
			}
		base = (base + 1LL * (1LL * res * res - res2)/2 % MOD * dep[p]) % MOD;
	}
	while (Q--){
		scanf("%d", &K); 
		printf("%d\n", 1LL * base * C(n-2, K) % MOD);
	}
	return 0;
}

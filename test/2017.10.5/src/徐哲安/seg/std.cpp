#include<bits/stdc++.h>
#define int unsigned int
using namespace std;

const int maxn = 300009;
int a[maxn], n, Q, x, y, t;

int query(int l, int r){
	int ans = a[l];
	for (int i=l+1; i<=r; i++)
		ans = ~(ans&a[i]);
	return ans;
}

signed main(){
	freopen("seg.in", "r", stdin);
	freopen("std.out", "w", stdout);
	scanf("%u%u", &n, &Q);
	for (int i=1; i<=n; i++) scanf("%u", &a[i]);
	while (Q--){
		scanf("%u%u%u", &t, &x, &y);
		if (t == 1) a[x] = y;
		else printf("%u\n", query(x, y));
	}
	return 0;
}

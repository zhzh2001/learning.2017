#include<cstdio>
#include<cstring>
#include<algorithm>
#define lc (o << 1)
#define rc (o << 1 | 1)
#define mid ((l + r) >> 1)
#define zero first
#define one second
using namespace std;
typedef unsigned int INT;
typedef pair<INT,INT> P;

/*******Read&Write*******/
const int L=500005;
char LZH[L],*S=LZH,*t=LZH;
inline char gc(){
	if (S==t){
		t=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==t) return EOF;
	}
	return *S++;
}
inline INT read(){
	INT x=0; char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

INT stk[233], top;
inline void writeln(INT x){
	top = 0;
	while (x){
		stk[++top] = x % 10;
		x /= 10;
	}
	while (top) putchar('0' + stk[top--]);
	puts("");
}
/**********END**********/

const int maxn = 500009;
P T[maxn<<2], res;
INT a[maxn], n, Q, x, y;

inline P merge(P l, P r){
	P res;
	res.zero = (l.zero & r.one) | ((~l.zero) & r.zero);
	res.one  = (l.one  & r.one) | ((~l.one)  & r.zero);
	return res;
}

void build(INT o, INT l, INT r){
    if (l == r) { T[o] = make_pair(~INT(0), ~a[l]); return; }
    build(lc, l, mid); build(rc, mid+1, r);
    T[o] = merge(T[lc], T[rc]);
}

void update(INT o, INT l, INT r, INT x, INT y){
	if (l == r) { T[o] = make_pair(~INT(0), ~y); return; }
	if (x <= mid) update(lc, l, mid, x, y);
	else update(rc, mid+1, r, x, y);
	T[o] = merge(T[lc], T[rc]);
}

P query(INT o, INT l, INT r, INT x, INT y){
	if (l == x && y == r) return T[o];
	if (y <= mid) return query(lc, l, mid, x, y);
	if (mid+1 <= x) return query(rc, mid+1, r, x, y);
	return merge(query(lc, l, mid, x, mid), query(rc, mid+1, r, mid+1, y));
}

int main(){
	freopen("seg.in", "r", stdin);
	freopen("seg.out", "w", stdout);
    n = read(); Q = read();
    for (INT i=1; i<=n; i++) a[i] = read();
    build(1, 1, n);
    while (Q--){
        if (read() == 1){
            x = read(); y = read();
            update(1, 1, n, x, y); 
			a[x] = y; 
        }
        else{
            x = read(); y = read();
            if (x == y) writeln(a[x]);
            else{
            	res = query(1, 1, n, x+1, y);
				writeln(a[x] & res.one | (~a[x]) & res.zero);
			}
        }
    }
    return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define int long long
using namespace std;

const int MOD = 100000007;
const int maxn = 1000009;
int fac[maxn], rev[maxn], n, r, k, ans;

int power(int x, int p){
	int res = 1;
	while (p){
		if (p&1) res = 1LL * res * x % MOD;
		x = 1LL * x * x % MOD; p >>= 1;
	}
	return res;
}

void init(){
	fac[0] = rev[0] = 1;
	for (int i=1; i<=maxn-1; i++){
		fac[i] = 1LL * fac[i-1] * i % MOD;
		rev[i] = power(fac[i], MOD-2);
	}
}

inline int C(int n, int m){
	return 1LL * fac[n] * rev[m] % MOD * rev[n-m] % MOD;
}

signed main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	int T; init();
	scanf("%lld", &T);
	while (T--){
		scanf("%lld%lld%lld", &n, &k, &r);
		if (n <= maxn-1){
			ans = 0;
			for (int i=r; i<=n; i+=k)
				ans = (ans + C(n, i)) % MOD;
			printf("%lld\n", ans);
		}
		else if (k == 1){
			printf("%lld\n", power(2, n));
		}
		else if (k == 2){
			printf("%lld\n", power(2, n-1));
		} 
		else puts("QwQ  srO_zyy_Orz");
	}
	return 0;
}

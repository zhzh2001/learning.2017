#include<bits/stdc++.h>
#define ll long long
using namespace std;
const ll P=100000007;
ll k,r,n;
ll f[1000010],v[1000010],inv[1000010];
 ll pow(ll x,ll y)
 {
	ll tmp=1,t=x;
	while (y)
	{
		if (y&1) tmp=tmp*t%P;
		t=t*t%P;y>>=1;
	}
	return tmp;
}
 ll calc_phi(ll P)
{
	ll c[105],phi=P,c1=0;
	for (ll i=2;i*i<=P;++i)
		if (P%i==0)
		{
			c[++c1]=i;
			while (P%i==0)P/=i;
		}
	if (P!=1) c[++c1]=P;
	for (ll i=1;i<=c1;++i) phi=phi/c[i]*(c[i]-1);
	return phi;
}
 int main()
{
	//freopen("math.in","r",stdin);
	//freopen("math.out","w",stdout);
	int T;
	scanf("%d",&T);
	ll phi=calc_phi(P);
	while (T--)
	{
		
		scanf("%lld%lld%lld",&n,&k,&r);

		ll cnt=n/k*k+r;
		if (cnt>n) cnt=cnt-k;
		f[0]=1;
		for (ll i=1;i<=cnt;++i) f[i]=f[i-1]*i%P;
		v[cnt]=pow(f[cnt],phi-1);
			
		for (ll i=cnt-1;i>0;--i) v[i]=v[i+1]*(i+1)%P;
		for (ll i=1;i<=cnt;++i) inv[i]=v[i]*f[i-1]%P;
		ll m=r,ans=0;
		
		while (m<=n)
		{
			if (m==0||m==n) 
			{
				ans=(ans+1)%P;
				m=m+k;
				continue;	
			}
			ll sum=1;
			for (ll i=n-m+1;i<=n;++i) sum=sum*i%P;
			for (ll i=1;i<=m;++i) sum=sum*inv[i]%P;
			ans=(ans+sum)%P;
			m+=k;
		}
		printf("%lld\n",ans);
	}
}

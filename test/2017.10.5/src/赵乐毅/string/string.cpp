#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
const int N=3005;
const int L=1000005;
const int mo=1000000007;
char ch[L];
int n,q,jc[N*3],C[N*3],ans,sum,k,a[L][26],ai,f[L],l,d;
int ksm(int x,int y)
{
	if (y==1)return x;
	int temp=ksm(x,y/2);
	if (y&1)return 1ll*temp*temp%mo*x%mo;
			else return 1ll*temp*temp%mo;
}
int dfs(int x)
{
	long long ans=1ll*f[x]*(f[x]-1)/2;
	for (int i=0;i<=25;i++)
		if (a[x][i])ans+=dfs(a[x][i]);
	return ans%mo;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d%d",&n,&q);
	jc[0]=1;
	for (int i=1;i<=n;i++)
		jc[i]=1ll*jc[i-1]*i%mo;
	C[n]=ksm(jc[n],mo-2);
	for (int i=n-1;i>=0;i--)
		C[i]=1ll*C[i+1]*(i+1)%mo;
	for (int I=1;I<=n;I++)
	{
		scanf("%s",ch);
		l=strlen(ch);
		d=0;
		for (int i=0;i<l;i++)
		{
			if (!a[d][ch[i]-'a'])a[d][ch[i]-'a']=++ai;
			d=a[d][ch[i]-'a'];f[d]++;
		}
	}
	ans=dfs(0);
	while(q--)
	{
		scanf("%d",&k);
		k=n-k;
		if (k==1){printf("0\n");continue;}
		sum=1ll*jc[n-2]*C[n-k]%mo*C[k-2]%mo;
		printf("%d\n",1ll*sum*ans%mo);
	}
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 1000005
#define mod 100000007
#define gc getchar
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
//inline char gc(){
//	if (S==T){
//		T=(S=LZH)+fread(LZH,1,L,stdin);
//		if (S==T) return EOF;
//	}
//	return *S++;
//}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

ll a[N],n,k,r,ans;
ll b[N];

ll exgcd(ll a,ll b,ll &x,ll &y){
	if(b == 0){
		x = 1;
		y = 0;
		return a;
	}
	ll num = exgcd(b,a%b,x,y);
	ll tmp = x;
	x = y;
	y = tmp-a/b*y;
	return num;
}

inline ll quick_mi(ll a,ll n){
	ll ans = 1;
	while(n){
		if(n&1) ans = (ans*a)%mod;
		a = a*a%mod;
		n >>= 1;
	}
	return ans;
}

inline ll C(ll n,ll m){
	return (a[n]*b[n-m]%mod)*b[m]%mod;
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout); 
	ll T = read();
	while(T--){
		ans = 0;
		n = read(); k = read(); r = read();
		if(n <= 1000000){
			a[0] = 1;
			for(ll i = 1;i <= n;i++)
				a[i] = a[i-1]*i%mod;
			ll y;
			exgcd(a[n],mod,b[n],y);
			b[n] = (b[n]+mod)%mod;
			for(ll i = n-1;i >= 0;i--)
				b[i] = (b[i+1]*(i+1))%mod;
			for(ll i = 0;i*k+r <= n;i++){
				ll p = i*k+r;
				ans = (ans+C(n,p))%mod;
			}
			printf("%lld\n",ans);
		}
		else{
			ans = quick_mi(2,n);
			ll x,y;
			exgcd(k,mod,x,y);
			x = (x+mod)%mod;
			ans = (ans*x)%mod;
			printf("%lld\n",ans);
		}
	}
	return 0;	
}

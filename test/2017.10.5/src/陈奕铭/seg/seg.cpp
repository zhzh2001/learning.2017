// #include<cmath>
// #include<cstdio>
// #include<cstdlib>
// #include<cstring>
// #include<algorithm>
// #define ll long long
// #define gc getchar
// #define N 500005
// #define M 2000005
// using namespace std;
// const int L=200005;
// char LZH[L],*S=LZH,*T=LZH;
// //inline char gc(){
// //	if (S==T){
// //		T=(S=LZH)+fread(LZH,1,L,stdin);
// //		if (S==T) return EOF;
// //	}
// //	return *S++;
// //}
// inline int read(){
// 	int x=0,f=1;
// 	char c=gc();
// 	for (;c<'0'||c>'9';c=gc())
// 		if (c=='-') f=-1;
// 	for (;c>='0'&&c<='9';c=gc())
// 		x=(x<<1)+(x<<3)+c-48;
// 	return x*f;
// }

// inline unsigned int uread(){
// 	unsigned int x=0,f=1;
// 	char c=gc();
// 	for (;c<'0'||c>'9';c=gc())
// 		if (c=='-') f=-1;
// 	for (;c>='0'&&c<='9';c=gc())
// 		x=(x<<1)+(x<<3)+c-48;
// 	return x*f;
// }

// struct node{
// 	int l,r;
// 	unsigned int ans;
// 	bool flag;
// }tr[M];
// int n,q;
// unsigned int a[N];

// void build(int num,int x,int y){
// 	tr[num].l = x; tr[num].r = y;
// 	// printf("%d %d\n",x,y);
// 	if(x == y){
// 		tr[num].ans = a[x];
// 		tr[num].flag = true;
// 		return;
// 	}
// 	tr[num].flag = false;
// 	int mid = (x+y)>>1;
// 	build(num<<1,x,mid);
// 	build(num<<1|1,mid+1,y);
// }

// void gebug(int num,int x,int y){
// 	// printf("%d %d %u\n",tr[num].l,tr[num].r,tr[num].ans);
// 	if(x == y)
// 		return;
// 	int mid = (x+y)>>1;
// 	gebug(num<<1,x,mid);
// 	gebug(num<<1|1,mid+1,y);
// }

// int main(){
// 	n = read(); q = read();
// 	for(int i = 1;i <= n;i++)
// 		a[i] = uread();
// 	build(1,1,n);
// 	// gebug(1,1,n);
// 	return 0;
// }

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define gc getchar
#define N 500005
#define M 2000005
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
//inline char gc(){
//	if (S==T){
//		T=(S=LZH)+fread(LZH,1,L,stdin);
//		if (S==T) return EOF;
//	}
//	return *S++;
//}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

inline unsigned int uread(){
	unsigned int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

unsigned int a[N];
int n,q;

int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	n = read(); q = read();
	for(int i = 1;i <= n;i++)
		a[i] = uread();
	while(q--){
		int opt = read();
		if(opt == 1){
			int x = read(),y = uread();
			a[x] = y;
		}
		else{
			int x = read(),y = read();
			unsigned int ans = a[x];
			for(int i = x+1;i <= y;i++)
				ans = ~(ans&a[i]);
			printf("%u\n", ans);
		}
	}
	return 0;
}
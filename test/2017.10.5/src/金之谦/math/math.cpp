#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int L=200005;
const int MOD=1e8+7;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int jie[10000010];
inline int mi(int a,int b){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;
		y=y*y%MOD;b>>=1;
	}
	return x;
}
inline void pre(){
	jie[0]=1;
	for(int i=1;i<=1e7;i++)jie[i]=jie[i-1]*i%MOD;
}
inline int C(int x,int y){return jie[x]*mi(jie[y],MOD-2)%MOD*mi(jie[x-y],MOD-2)%MOD;}
signed main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	pre();
	int T=read(),n,k,r;
	while(T--){
		n=read();k=read();r=read();
		if(k==1){
			printf("%lld\n",mi(2,n));
			continue;
		}else{
			int ans=0;
			while(r<=n){
				ans=(ans+C(n,r))%MOD;r+=k;
			}
			printf("%lld\n",ans);
		}
	}
	return 0;
}
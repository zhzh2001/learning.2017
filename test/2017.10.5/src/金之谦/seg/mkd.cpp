#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
signed main()
{
	freopen("seg.in","w",stdout);
	srand(time(0));
	int n=rand()%100,q=rand()%100;
	printf("%u %u\n",n,q);
	for(int i=1;i<=n;i++)printf("%u ",rand()%20);puts("");
	for(int i=1;i<=q;i++){
		int op=rand()%2+1;
		if(op==1){
			int x=rand()%n+1,y=rand()%20;printf("%u %u %u\n",op,x,y);
		}else{
			int x=rand()%n+1,y=rand()%n+1;if(x>y)swap(x,y);
			printf("%u %u %u\n",op,x,y);
		}
	}
}
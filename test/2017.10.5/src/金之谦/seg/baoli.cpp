#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int unsigned int
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int n,q,a[500010],cnt=0;
signed main()
{
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=q;i++){
		int op=read(),x=read(),y=read();
		if(op==1)a[x]=y;
		else{
			int ans=a[x];
			for(int j=x+1;j<=y;j++)ans=(ans&a[j])^4294967295;
			printf("%u\n",ans);
		}
	}
	return 0;
}
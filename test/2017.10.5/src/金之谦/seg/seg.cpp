#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int unsigned int
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
struct seg{int t,ot;}t[32][2000001];
int n,q,a[500010],cnt=0;
inline int nand(int x,int y){return (x&y)^1;}
inline seg pushup(int k,seg A,seg B,int x){
	int p=(((1<<k)&a[x])>0);seg j;
	j.ot=A.ot+B.ot+(p==0);
	if(nand(A.t,p)==p)j.t=B.t;
	else{
		if(B.ot)j.t=B.t;
		else j.t=B.t^1;
	}
	return j;
}
inline void build(int k,int l,int r,int nod){
	t[k][nod].ot=0;
	if(l==r){t[k][nod].t=(((1<<k)&a[l])>0);return;}
	int mid=l+r>>1;
	build(k,l,mid,nod*2);build(k,mid+1,r,nod*2+1);
	t[k][nod]=pushup(k,t[k][nod*2],t[k][nod*2+1],mid+1);
}
inline void xg(int k,int l,int r,int x,int nod){
	if(l==r){t[k][nod].t=(((1<<k)&a[l])>0);return;}
	int mid=l+r>>1;
	if(x<=mid)xg(k,l,mid,x,nod*2);else xg(k,mid+1,r,x,nod*2+1);
	t[k][nod]=pushup(k,t[k][nod*2],t[k][nod*2+1],mid+1);
}
inline seg ssum(int k,int l,int r,int i,int j,int nod){
	if(l>=i&&r<=j)return t[k][nod];
	int mid=l+r>>1;
	if(j<=mid)return ssum(k,l,mid,i,j,nod*2);
	if(i>mid)return ssum(k,mid+1,r,i,j,nod*2+1);
	seg x=ssum(k,l,mid,i,mid,nod*2),y=ssum(k,mid+1,r,mid+1,j,nod*2+1);
	return pushup(k,x,y,mid+1);
}
signed main()
{
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=0;i<32;i++)build(i,1,n,1);
	for(int i=1;i<=q;i++){
		int op=read(),x=read(),y=read();
		if(op==1){
			int p=a[x];a[x]=y;
			for(int j=0;j<32;j++)if(((1<<j)&p)!=((1<<j)&a[x]))
				xg(j,1,n,x,1);
		}else{
			int ans=0;
			for(int j=0;j<32;j++){
				seg k=ssum(j,1,n,x,y,1);
				ans+=(1<<j)*k.t;
			}
			printf("%u\n",ans);
		}
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1000100,P=1000000007;
int ksm(int x,ll y){
	int z=1;for(;y;y>>=1,x=1ll*x*x%P)
	if(y&1)z=1ll*x*z%P;return z;
}
char s[N];
struct Trie{
	int to[N][26],sm[N],n,lv[N];
	Trie(){n=1;}
	void add(int m){
		int i,x=1;sm[x]++;
		for(i=1;i<=m;i++){
			s[i]-='a';
			if(!to[x][s[i]])
			to[x][s[i]]=++n,lv[n]=lv[x]+1;
			x=to[x][s[i]];sm[x]++;
		}
	}
	int get(){
		int ans=0,res,i,j,x;
		for(i=1;i<=n;i++)
		for(j=0;j<26;j++)
		if((x=to[i][j])>0){
			sm[i]-=sm[x];
			res=1ll*sm[x]*sm[i]%P;
			res=1ll*res*lv[i]%P;ans=(ans+res)%P;
		}return ans;
	}
}tr;
int n,m,ans,fac[N],inv[N];
int C(int n,int m){
	if(m>n||m<0)return 0;
	int r=fac[n];
	r=1ll*r*inv[m]%P*inv[n-m]%P;
	return r;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	int i;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++){
		scanf("%s",s+1);
		tr.add(strlen(s+1));
	}
	ans=tr.get();
	fac[0]=1;
	for(i=1;i<n;i++)fac[i]=1ll*fac[i-1]*i%P;
	inv[n-1]=ksm(fac[n-1],P-2);
	for(i=n-1;i;i--)inv[i-1]=1ll*inv[i]*i%P;
	while(m--){
		scanf("%d",&i);
		i=C(n-2,i);
		int x=1ll*ans*i%P;
		printf("%d\n",x);
	}return 0;
}

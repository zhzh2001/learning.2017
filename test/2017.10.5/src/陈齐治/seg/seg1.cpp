#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ui unsigned int
#define ls (i<<1)
#define rs (i<<1|1)
using namespace std;
const int L=200005,N=1060100;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ui read(){
	ui x=0,f=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		f=(c=='-');
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return f?-x:x;
}
void write(ui x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
struct Seg{
	ui st,n,mx[N];bool a[N];
	void init(){
		int i,m=1;
		while(m<=n)m<<=1;
		while(n<m)n++,a[n]=1;
		
		for(i=1;i<=n;i++)
		mx[n+i-1]=a[i]?0:i;
		for(i=n-1;i;i--)
		mx[i]=mx[rs]?mx[rs]:mx[ls];
	}
	void chan(int x,int y){
		if(a[x]==y)return;
		a[x]=y;
		mx[n+x-1]=a[x]?0:x;
		for(int i=(n+x-1)/2;i;i/=2)
		mx[i]=mx[rs]?mx[rs]:mx[ls];
	}
	bool query(int x,int y){
		if(x==y)return a[x];
		int i,re=0;
		for(i=n+y;i>1;i/=2)
		if((i&1)&&(mx[i-1]>0)){re=mx[i-1];break;}
		if(re<x)re=y-x+1;
		else{
			if(re==1)re++;
			re=y-re+1;	
		}return re&1;
	}
}tr[33];
ui n,m,a[N],pw[33],ans;
int main(){
	freopen("seg.in","r",stdin);
	freopen("segg.out","w",stdout);
	ui i,j,x,y;
	n=read();m=read();
	for(i=1;i<=n;i++)a[i]=read();
	pw[0]=1;
	for(i=1;i<32;i++)pw[i]=pw[i-1]+pw[i-1];
	for(i=0;i<32;i++)
	for(j=1;j<=n;j++)
	tr[i].a[j]=((a[j]&pw[i])>0);
	for(i=0;i<32;i++)tr[i].n=n,tr[i].init();
	while(m--){
		j=read();x=read();y=read();
		if(j==1){
			a[x]=y;
			for(i=0;i<32;i++)
			tr[i].chan(x,((y&pw[i])>0));
		}else{
			ans=0;
			for(i=0;i<32;i++)
			if(tr[i].query(x,y))ans|=pw[i];
			write(ans);puts("");
		}
	}return 0;
}

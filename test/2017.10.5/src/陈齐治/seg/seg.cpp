#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ui unsigned int
#define ls (i<<1)
#define rs (i<<1|1)
using namespace std;
const int L=200005,N=1060100,M=100100;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ui read(){
	ui x=0,f=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		f=(c=='-');
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return f?-x:x;
}
void write(ui x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
ui pw[33];
ui get0(ui x){
	x=~x;
	if(x==0)return 0;
	x=x&-x;
	for(int i=0;i<32;i++)
	if(x==pw[i])return 32-i;
	return 0;
}
struct Seg{
	ui st,n,mx[M];ui a[N];
	void init(){
		int i,j,m=1;
		while(m<=n)m<<=1;
		if(m<32)m=32;
		while(n<m)n++,a[n]=1;
		n=m/32;m=1;
		for(i=1;i<=n;i++){
			a[i]=a[m]*pw[31];
			for(j=1;j<32;j++)
			a[i]|=a[m+j]*pw[31-j];
			m+=32;
		}
		for(i=1;i<=n;i++){
			mx[n+i-1]=get0(a[i]);
			if(mx[n+i-1])mx[n+i-1]+=(i-1)*32;
		}
		for(i=n-1;i;i--)
		mx[i]=mx[rs]?mx[rs]:mx[ls];
	}
	void chan(int x,int k){
		int y,c;
		y=(x-1)%32+1;
		y=32-y;
		x=(x-1)/32+1;
		c=(a[x]&pw[y])>0;
		if(c==k)return;
		a[x]^=pw[y];
		mx[n+x-1]=get0(a[x]);
		if(mx[n+x-1])mx[n+x-1]+=(x-1)*32;
		for(int i=(n+x-1)/2;i;i/=2)
		mx[i]=mx[rs]?mx[rs]:mx[ls];
	}
	int alb(int x,int y){
		y=(y-1)%32+1;
		y=32-y;
		for(;y<32;y++)
		if((a[x]&pw[y])==0)
		return 32-y+(x-1)*32;
		return 0;
	}
	bool query(int x,int y){
		int l,r;
		l=(x-1)/32+1;
		r=(y-1)/32+1;
		int i,re=0;
		re=alb(r,y);
		if(re==0)for(i=n+r-1;i>1;i/=2)
		if((i&1)&&(mx[i-1]>0)){re=mx[i-1];break;}
		if(re<x)re=y-x+1;
		else{
			if(x==y)return 0;
			if(re==1)re++;
			re=y-re+1;	
		}return re&1;
	}
}tr[33];
ui n,m,a[N],ans;
int main(){
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	ui i,j,x,y;
	pw[0]=1;
	for(i=1;i<32;i++)pw[i]=pw[i-1]+pw[i-1];
	n=read();m=read();
	for(i=1;i<=n;i++)a[i]=read();
	for(i=0;i<32;i++)
	for(j=1;j<=n;j++)
	tr[i].a[j]=((a[j]&pw[i])>0);
	for(i=0;i<32;i++)tr[i].n=n,tr[i].init();
	while(m--){
		j=read();x=read();y=read();
		if(j==1){
			a[x]=y;
			for(i=0;i<32;i++)
			tr[i].chan(x,((y&pw[i])>0));
		}else{
			ans=0;
			for(i=0;i<32;i++)
			if(tr[i].query(x,y))ans|=pw[i];
			write(ans);puts("");
		}
	}return 0;
}

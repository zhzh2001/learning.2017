//If you give me a chance,I'll take it.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
const int RXD=1e9+7;
const int N=1000000;
using namespace std;
int i,j,k,l,m,n,q,d,ans,num,sum,total;
int f[N],na[N/100],chi[N/100],zyy[N][50];
char s[N];
inline int quick_mi(int I,int J) {
	if (J==1) return I;
	int quick=quick_mi(I,J/2);
	if (J&1) {
		return 1ll*quick*quick%RXD*I%RXD;
	}
	else {
		return 1ll*quick*quick%RXD;
	}
}
inline int crash(int I) {
	ff ans=1ll*f[I]*(f[I]-1)/2;
	Rep(i,0,25) {
		if (zyy[I][i]) {
			ans+=crash(zyy[I][i]);
		}
	}
	return ans%RXD;
}
int main() {
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>q; chi[0]=1;
	Rep(i,1,n) {
		chi[i]=1ll*chi[i-1]*i%RXD;
	}
	na[n]=quick_mi(chi[n],RXD-2);
	Drp(i,n-1,0) {
		na[i]=1ll*na[i+1]*(i+1)%RXD;
	}
	Rep(i,1,n) {
		cin>>s;
		l=strlen(s)-1; num=0;
		Rep(i,0,l) {
			if (!zyy[num][s[i]-'a']) {
				zyy[num][s[i]-'a']=++sum;
			}
			num=zyy[num][s[i]-'a'];
			f[num]++;
		}
	}
	ans=crash(0);
	while (q--) {
		cin>>k;
		k=n-k;
		if (k==1) {
			cout<<"0"<<endl;
			continue;
		}
		total=1ll*chi[n-2]*na[n-k]%RXD*na[k-2]%RXD;
		cout<<1ll*total*ans%RXD<<endl;
	}
	return 0;
}

//If you give me a chance,I'll take it.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define uint unsigned int
#define Rep(x,a,b) for (uint x=a;x<=b;x++)
#define Drp(x,a,b) for (uint x=a;x>=b;x--)
using namespace std;
uint n,q,a[1000000],x,y,z,ans;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int main() {
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	std::ios::sync_with_stdio(false);
	n=read(); q=read();
	Rep(i,1,n) a[i]=read();
	Rep(i,1,q) {
		x=read(); y=read(); z=read();
		if (x==1) {
			a[y]=z;
		}
		else {
			if (y>z) swap(y,z);
			ans=a[y];
			Rep(j,y+1,z) ans=~(ans&a[j]);
			cout<<ans<<endl;
		}
	}
	return 0;
}

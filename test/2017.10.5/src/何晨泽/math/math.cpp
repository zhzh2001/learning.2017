//If you give me a chance,I'll take it.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const ff RXD=100000007;
int i,j,k,l,m,r,T;
ff n;
struct node {
	int hua[501];
	node() {
		memset(hua,0,sizeof(hua));
	}
	node operator * (node b) {
		node c;
		Rep(i,0,k) Rep(j,0,k) {
			c.hua[(i+j)%k]=(c.hua[(i+j)%k]+1ll*hua[i]*b.hua[j]%RXD)%RXD;
		}
		return c;
	}
	node operator ^ (ff b) {
		node c,a=*(this);
		c.hua[0]=1;
		while (b) {
			if (b&1) {
				c=c*a;
			}
			b/=2;
			a=a*a;
		}
		return c;
	}
}chen;
int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>T;
	while (T--) {
		cin>>n>>k>>r;
		memset(chen.hua,0,sizeof(chen.hua));
		if (k==1) {
			chen.hua[0]=2;
		}
		else {
			chen.hua[0]=1;
			chen.hua[1]=1;
		}
		chen=chen^n;
		cout<<chen.hua[r]<<endl;
	}
	return 0;
}

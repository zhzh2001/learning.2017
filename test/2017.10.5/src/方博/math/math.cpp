#include<bits/stdc++.h>
using namespace std;
const int M=100000007;
int a[1005][1005];
int n,k,r,ans,t;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a[0][0]=1;
	for(int i=1;i<=1000;i++){
		a[i][0]=1;
		for(int j=1;j<=i;j++){
			a[i][j]=(a[i-1][j]+a[i-1][j-1])%M;
		}
	}
	scanf("%d",&t);
	for(int i=1;i<=t;i++){
		scanf("%d%d%d",&n,&k,&r);
		ans=0;
		for(int j=0;j*k+r<=n;j++){
			ans=(ans+a[n][j*k+r])%M;
		}
		printf("%d\n",ans);
	}
	return 0;
}

#include<bits/stdc++.h>
#define int long long
using namespace std;
const int L=200005;
int a[100005];
int bin[33];
int n,q;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
/*inline char gc(){
	return getchar();
}*/
inline unsigned int read(){
	unsigned int x=0,f=1;
	char c=gc();
	for(;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for(;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int nand(int a,int b)
{
	int ret=0;
	for(int i=0;i<32;i++)
		if(!(((a>>i)&1)&((b>>i)&1)))ret+=bin[i];
	return ret;
}
signed main()
{
	freopen("seg.in","r",stdin);
	freopen("seg.out","w",stdout);
	bin[0]=1;
	for(int i=1;i<32;i++)
		bin[i]=bin[i-1]<<1;
	n=read();q=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=q;i++){
		int op=read();
		if(op==1){
			int c=read();
			a[c]=read();
		}
		else {
			int x,y;
			x=read(),y=read();
			int ans=a[x];
			for(int i=x+1;i<=y;i++)
				ans=nand(ans,a[i]);
			printf("%lld\n",ans);
		}
	}
	return 0;
}

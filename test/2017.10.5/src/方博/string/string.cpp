#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int M=1000000007;
ll c[3005][3005];
int a[3005];
string s[3005];
ll ans;
int n,q;
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)
		cin>>s[i];
	sort(s+1,s+n+1);
	for(int i=1;i<n;i++){
		int j=i+1;
		int len=min(s[i].length(),s[j].length());
		for(int k=0;k<=len;k++)
			if(s[i][k]!=s[j][k]||k==len){a[i]=k;break;}
	}
	for(int i=1;i<=n;i++){
		int k=a[i];
		int j=i;
		while(k>0){
			k=min(k,a[j]);
			ans=(ans+k)%M;
			j++;
		}
	}
	c[0][0]=1;
	for(int i=1;i<=n;i++){
		c[i][0]=1;
		for(int j=1;j<=i;j++)
			c[i][j]=(c[i-1][j]+c[i-1][j-1])%M;
	}
	for(int i=1;i<=q;i++){
		int k;
		scanf("%d",&k);
		printf("%d\n",(1LL*ans*c[n-2][k])%M);
	}
	return 0;
}

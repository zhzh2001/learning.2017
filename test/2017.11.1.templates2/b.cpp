#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1);
	int i = n, w = 0, h = 0;
	for (; i; i--)
		if (a[i] == a[i - 1])
		{
			w = a[i];
			i -= 2;
			break;
		}
	for (; i; i--)
		if (a[i] == a[i - 1])
		{
			h = a[i];
			break;
		}
	cout << 1ll * w * h << endl;
	return 0;
}
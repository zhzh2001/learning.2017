#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int cnt[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		cnt[x]++;
	}
	int ans = 0;
	for (int i = 0; i < 1e5; i++)
		ans = max(ans, cnt[i] + cnt[i + 1] + cnt[i + 2]);
	cout << ans << endl;
	return 0;
}
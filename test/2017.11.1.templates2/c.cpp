#include <iostream>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	int one = 0, two = 0, four = 0;
	while (n--)
	{
		int x;
		cin >> x;
		if (x % 4 == 0)
			four++;
		else if (x % 2 == 0)
			two++;
		else
			one++;
	}
	four -= one - 1;
	if (two)
		four--;
	if (four >= 0)
		cout << "Yes\n";
	else
		cout << "No\n";
	return 0;
}
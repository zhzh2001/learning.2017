#include <iostream>
#include <deque>
#include <algorithm>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	deque<int> ans;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		if (i & 1)
			ans.push_back(x);
		else
			ans.push_front(x);
	}
	if (n & 1)
		reverse(ans.begin(), ans.end());
	for (int i : ans)
		cout << i << ' ';
	cout << endl;
	return 0;
}
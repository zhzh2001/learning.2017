#include <iostream>
using namespace std;
const int N = 200005;
bool s[N], t[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		if (u == 1)
			s[v] = true;
		if (v == n)
			t[u] = true;
	}
	for (int i = 2; i < n; i++)
		if (s[i] && t[i])
		{
			cout << "POSSIBLE\n";
			return 0;
		}
	cout << "IMPOSSIBLE\n";
	return 0;
}
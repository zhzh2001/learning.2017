#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <cstdio>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
FILE *Finn, *Fout, *Fstd, *Fres;
int n,m,ans,b[105][105];
char s[105];
void Return(double p,char* s)
{
	fprintf(Fres,"%.3lf\n%s\n",p,s);
	exit(0);
}
int	main(int args, char** argv)
{
	Finn = fopen(argv[1], "r");
	Fstd = fopen(argv[2], "r");
	Fout = fopen(argv[3], "r");
	Fres = fopen(argv[4], "w");
	fscanf(Finn,"%d",&m); fscanf(Fout,"%d",&n);
	if (n>100) Return(0,"顶点数超过限制");
	for (int i=0;i<n;i++){
		fscanf(Fout,"%s",s);
		for (int j=0;j<n;j++) b[i][j]=s[j]-48;
		}
	for (int i=0;i<n;i++) if (b[i][i]) Return(0,"存在自环");
	for (int i=0;i<n;i++)
		for (int j=i+1;j<n;j++)
			if (b[i][j]^b[j][i]) Return(0,"出现单向边");
	for (int i=0;i<n;i++)
		for (int j=i+1;j<n;j++) if (b[i][j])
			for (int k=j+1;k<n;k++) if (b[i][k] && b[j][k]) ans++;
	if (ans!=m) Return(0,"方案错误"); else Return(1,"正确");
	return 0;
}

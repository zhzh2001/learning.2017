#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
	putchar(' ');
}
const int inf=1e9+7,N=100005,M=105;
int a[N],v[M],f[N],pr[N],n,m,x,fd[N],g[N];
int main()
{
	freopen("test.in","r",stdin);
	freopen("test.out","w",stdout);
	read(n);
	for (int i=3;i<=100;++i)
		v[i]=i*(i-1)*(i-2)/6;
	f[0]=0;
	for (int i=1;i<=n;++i)
		f[i]=inf;
	for (int i=3;i<=100;++i)
		for (int j=v[i];j<=n;++j)
			for (int k=0;k<=10&&k*i*(i-1)/2+v[i]<=j;++k)
				if (f[j]>f[j-v[i]-k*i*(i-1)/2]+i+k)
				{
					f[j]=f[j-v[i]-k*i*(i-1)/2]+i+k;
					pr[j]=i;
					fd[j]=k;
				}
	for (int i=1;i<=n;++i)
	{
		a[i]=a[i-v[pr[i]]-fd[i]*pr[i]*(pr[i]-1)/2]+pr[i];
		g[i]=max(g[i-v[pr[i]]-fd[i]*pr[i]*(pr[i]-1)/2],fd[i]);
		write(a[i]+g[i]);
		putchar('\n');
	}
	return 0;
}

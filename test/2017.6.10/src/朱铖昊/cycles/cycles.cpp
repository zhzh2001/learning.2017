#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int m,ans,x,y,z,f[200][200],n,a[1000],mx;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	m=read();
	For(i,3,100)
	{
		a[i]=i*(i-1)*(i-2);
		a[i]/=6;
	}
	x=0;
	Rep(i,3,100)
	{
		if(m>=a[i])
		{
			ans+=i;
			For(j,1,i)
				For(k,1,i)
					f[j][k]=1;
			m-=a[i];
			mx=i;
			break;
		}
	}
	while(m>0)
	{
		ans++;
		f[1][ans]=1;f[ans][1]=1; 
		x=2;
		while(m>=x-1&&x<=mx)
		{
			f[x][ans]=1;f[ans][x]=1;
			m-=(x-1);
			x++;
		}
	}
	cout<<ans<<endl;
	For(i,1,ans)f[i][i]=0;
	For(i,1,ans)
	{
		For(j,1,ans)cout<<f[i][j]<<" ";
		cout<<endl;		
	}
	return 0;
}


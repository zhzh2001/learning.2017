#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 105
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,m;
int map[N][N];

int main(){
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	n=read();
	m=n+2;
	if(n>100) return 0;
	if(n==99){
		map[1][2]=map[2][1]=1;
		for(int i=3;i<=99;i++){
			map[1][i]=map[i][1]=1;
			map[2][i]=map[i][2]=1;
		}
		map[3][4]=map[4][3]=1;
		m=99;
	}
	else if(n==100){
		map[1][2]=map[2][1]=1;
		for(int i=3;i<=100;i++){
			map[1][i]=map[i][1]=1;
			map[2][i]=map[i][2]=1;
		}
		map[3][4]=map[4][3]=1;
		m=100;
	}
	else{
		map[1][2]=map[2][1]=1;
		for(int i=3;i<=m;i++){
			map[1][i]=map[i][1]=1;
			map[2][i]=map[i][2]=1;
		}
	}
	printf("%d\n",m);
	for(int i=1;i<=m;i++,puts(""))
		for(int j=1;j<=m;j++)
			printf("%d",map[i][j]);
	return 0;
}

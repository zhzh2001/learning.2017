#include<fstream>
#include<stack>
using namespace std;
ifstream fin("exprsuf.in");
ofstream fout("exprsuf.out");
const int N=55;
int a[N];
long long calc(int n,long long val=-1e18)
{
	stack<long long> num;
	for(int i=1;i<=n;i++)
		if(a[i]==0)
		{
			if(num.size()>=2)
			{
				long long x=num.top();num.pop();
				long long y=num.top();num.pop();
				num.push(x+y);
			}
		}
		else
			if(a[i]>0)
				num.push(a[i]);
			else
				num.push(val);
	if(num.size())
		return num.top();
	return val;
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n,m;
		fin>>n>>m;
		int p;
		for(int i=1;i<=n;i++)
		{
			fin>>a[i];
			if(a[i]==-1)
				p=i;
		}
		a[p]=0;
		if(calc(n)==m)
			fout<<0<<endl;
		else
		{
			a[p]=-1;
			long long ret=calc(n);
			if(ret<0)
			{
				long long ret=calc(n,0);
				if(ret<m)
					fout<<m-ret<<endl;
				else
					fout<<-1<<endl;
			}
			else
				fout<<-1<<endl;
		}
	}
	return 0;
}
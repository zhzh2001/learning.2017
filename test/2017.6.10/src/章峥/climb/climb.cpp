#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("climb.in");
ofstream fout("climb.out");
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n,s,t;
		string seq;
		fin>>n>>s>>t>>seq;
		int now=0,minh=0;
		for(int i=0;i<seq.length();i++)
		{
			if(seq[i]=='U')
				now++;
			else
				now--;
			minh=min(minh,now);
		}
		int remain=t-s-now,len=seq.length();
		if(s+minh<0)
		{
			remain+=s+minh;
			len+=-(s+minh);
		}
		if(len>n||abs(remain)>abs(len-n)||(remain-len-n)&1)
			fout<<"NO\n";
		else
			fout<<"YES\n";
	}
	return 0;
}
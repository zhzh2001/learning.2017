#include<fstream>
using namespace std;
ifstream fin("cycles.in");
ofstream fout("cycles.out");
const int N=105;
bool mat[N][N];
int main()
{
	int m;
	fin>>m;
	int n;
	for(n=N;n*(n-1)*(n-2)/6>m;n--);
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			mat[i][j]=mat[j][i]=true;
	m-=n*(n-1)*(n-2)/6;
	int newn=n;
	while(m)
	{
		newn++;
		int i;
		for(i=n;i*(i-1)/2>m;i--);
		for(int j=1;j<=i;j++)
			mat[j][newn]=mat[newn][j]=true;
		m-=i*(i-1)/2;
	}
	fout<<newn<<endl;
	for(int i=1;i<=newn;i++)
	{
		for(int j=1;j<=newn;j++)
			fout<<mat[i][j];
		fout<<endl;
	}
	return 0;
}
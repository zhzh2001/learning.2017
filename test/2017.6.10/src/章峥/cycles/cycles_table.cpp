#include<bits/stdc++.h>
using namespace std;
const int N=105;
bool mat[N][N];
int count_cycles(int n)
{
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=j+1;k<=n;k++)
				ans+=mat[i][j]&&mat[j][k]&&mat[k][i];
	return ans;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<n;i++)
		mat[i][i+1]=mat[i+1][i]=true;
	mat[n][1]=mat[1][n]=true;
	for(int i=2;i<n;i++)
		mat[1][i]=mat[i][1]=true;
	cout<<count_cycles(n)<<' ';
	/* for(int i=2;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(!mat[i][j])
			{
				mat[i][j]=mat[j][i]=true;
				cout<<count_cycles(n)<<' ';
			} */
	for(int i=n;i;i--)
		for(int j=n;j>i;j--)
			if(!mat[i][j])
			{
				mat[i][j]=mat[j][i]=true;
				cout<<count_cycles(n)<<' ';
			}
	cout<<endl;
	return 0;
}
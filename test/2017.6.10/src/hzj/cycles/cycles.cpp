#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 205;
int m,n;
int G[N][N];
int main(){
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	cin >> m;
//	G[1][2]=G[2][1]=1;
	n=100;int cnt=0;
	while(m){
		int beg=cnt;
		if(m==1&&cnt>50){
			G[1][cnt+1]=G[cnt+1][1]=1;
			G[2][cnt+1]=G[cnt+2][1]=1;
			break;
		}
		for(int k=n;k>=3;--k){
			if(k*(k-1)*(k-2)/6<=m){
				m-=k*(k-1)*(k-2)/6;
				For(i,cnt+1,cnt+k){
					For(j,i+1,cnt+k){
						G[i][j]=G[j][i]=1;
					}
				}
				cnt+=k;
				break;
			}
		}
		For(i,beg+1,cnt){
			if(m>=i-beg-1){
				G[cnt+1][i]=G[i][cnt+1]=1;
				m-=i-beg-1;
			}
		}
		++cnt;
	}
	printf("%d\n",n);
	For(i,1,n){
		For(j,1,n) printf("%d",G[i][j]);
		puts("");
	}
	return 0;
}

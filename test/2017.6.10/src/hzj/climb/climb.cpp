#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
int T;
int n,st,ed;
string s;
bool work(){
	cin >> n >> st >> ed;
	cin >> s;
	int m=s.size();
	int mn=1e9;
	int nowp=st;
	Rep(i,0,m){
		if(s[i]=='U'){
			nowp++;
		}else{
			nowp--;
			if(nowp<mn) mn=nowp;
		}
	}
	int len=m;
	if(mn<0){
		len-=mn;
		nowp-=mn;
	}
	int remain=n-len;
	int delta=abs(ed-nowp);
	if(delta>remain) return 0;
	if((remain+delta)%2==1) return 0;
	return 1;
}
int main(){
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	cin >> T;
	while(T--) puts(work()?"YES":"NO");
	return 0;
}

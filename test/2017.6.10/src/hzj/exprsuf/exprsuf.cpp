#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 100005;
int n;
long long m;
long long a[N];
long long s[N];
int r;
long long x;
long long work(){
	cin >> n >> m;
	For(i,1,n) cin >> a[i];
	// work with +
	r=0;
	For(i,1,n){
		if(a[i]==0||a[i]==-1){
			if(r<2) continue;
			s[r-1]+=s[r];
			--r;
		}else{
			s[++r]=a[i];
		}
	}
	if(s[r]==m) return 0;
	// work with number
	r=0;x=0;
	For(i,1,n){
		if(a[i]==0){
			if(r<2) continue;
			if(s[r]<0){
				x+=s[r-1];
				s[r-1]=-1;
			}else if(s[r-1]<0){
				x+=s[r];
				s[r-1]=-1;
			}else{
				s[r-1]+=s[r];
			}
			--r;			
		}else{
			s[++r]=a[i];
		}
	}
	if(s[r]!=-1) return (m==s[r])?1:-1;
	return m-x<=0?-1:m-x;
}
int main(){
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	int T; cin >> T;
	while(T--) cout << work() << endl;
	return 0;
}

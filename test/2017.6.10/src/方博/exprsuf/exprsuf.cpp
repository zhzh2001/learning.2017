#include<bits/stdc++.h>
using namespace std;
int n;
long long m;
long long a[55];
long long b[55];
int t;
int main()
{
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		int ta=0;
		int tb=0;
		bool vis=false;
		scanf("%d%lld",&n,&m);
		for(int i=1;i<=n;i++){
			long long x;
			scanf("%lld",&x);
			if(x==0){
				if(ta>=2){
					ta--;
					if(a[ta]==-1||a[ta+1]==-1)vis=true,a[ta]=max(a[ta],a[ta+1]);
					else a[ta]=a[ta]+a[ta+1];
				}
				if(tb>=2){
					tb--;
					b[tb]=b[tb]+b[tb+1];
				}
			}
			if(x==-1){
				ta++;
				a[ta]=-1;
				if(tb>=2){
					tb--;
					b[tb]=b[tb]+b[tb+1];
				}
			}
			if(x>0){
				ta++;
				a[ta]=x;
				tb++;
				b[tb]=x;
			}
		}
//		cout<<a[ta]<<' '<<b[tb]<<endl;
		if(b[tb]==m){printf("0\n");continue;}
		else if(a[ta]<m&&vis){printf("%lld\n",m-a[ta]);continue;}
		else if(a[ta]==m&&!vis){printf("1\n");continue;}
		else printf("-1\n");
	}
	return 0;
}

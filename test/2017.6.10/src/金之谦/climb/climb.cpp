#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
char s[100001];
int n,st,ed,q[100001];
int main()
{
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	int T=read();
	while(T--){
		n=read();st=read();ed=read();q[0]=1;
		scanf("%s",s+1);int l=strlen(s+1);s[0]=0;
		int p=0;
		for(int i=1;i<=l;i++)if(s[i]!=s[i-1]){
			q[0]++;
			if(s[i]=='U')q[q[0]]=1,p++;
			else q[q[0]]=-1,p--;
		}else if(s[i]=='U')q[q[0]]++,p++;
		else q[q[0]]--,p--;
		int rp=ed-st-p;
		if(n-l<abs(rp)){puts("NO");continue;}
		if((n-l)%2!=abs(rp)%2){puts("NO");continue;}
		q[1]=rp+(n-l-rp)/2,q[++q[0]]=-(n-l-rp)/2;
		bool flag=1;
		for(int i=1;i<=q[0];i++){
			st+=q[i];
			if(st<0){flag=0;break;}
		}
		puts(flag?"YES":"NO");
	}
	return 0;
}
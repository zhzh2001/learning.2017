#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
ll s[100001];
ll n,m,a[100001];
int main()
{
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();m=read();
		for(ll i=1;i<=n;i++)a[i]=read();
		s[0]=0;
		for(ll i=1;i<=n;i++)if(a[i]<=0){
			if(s[0]>1)s[s[0]-1]=s[s[0]]+s[s[0]-1],s[0]--;
		}else s[++s[0]]=a[i];
		if(s[s[0]]==m){puts("0");continue;}
		s[0]=0;bool flag=0;
		for(ll i=1;i<=n;i++)if(a[i]==0){
			if(s[0]>1){
				if(s[s[0]-1]==-1||s[s[0]]==-1)s[s[0]-1]=max(s[s[0]-1],s[s[0]]),flag=1;
				else s[s[0]-1]=s[s[0]]+s[s[0]-1];
				s[0]--;
			}
		}else s[++s[0]]=a[i];
		if(s[s[0]]==-1){printf("%lld\n",m);continue;}
		if(s[s[0]]>m||!flag&&s[s[0]]!=m){puts("-1");continue;}
		if(flag&&s[s[0]]==m){puts("-1");continue;}
		if(!flag){puts("1");continue;}
		printf("%lld\n",m-s[s[0]]);
	}
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
int n,m,p,tp,A[105];
LL stk[105];
bool vp[105],_v;

LL cal(){
	tp=0;
	for (int i=1;i<=n;i++) {
		if (A[i]) stk[++tp]=((~A[i])?A[i]:0),vp[tp]=(i==p);
		else if (tp>=2) vp[tp-1]|=vp[tp],stk[tp-1]+=stk[tp],tp--;
	}
	_v=vp[tp];
	return stk[tp];
}

int main()
{
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	for (int cas=read();cas--;){
		n=read(),m=read();
		for (int i=1;i<=n;i++) if (!(~(A[i]=read()))) p=i;
		A[p]=0;
		if (cal()==m) {puts("0");continue;}
		A[p]=-1;
		LL cun=cal();
		if (!_v&&cun==m) puts("1");
		else if (_v&&((cun=m-cun)>0)) cout<<cun<<endl;
		else puts("-1");
	}
	return 0;
}

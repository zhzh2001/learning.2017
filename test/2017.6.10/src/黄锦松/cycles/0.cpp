#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<ctime>
using namespace std;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return x;
}

inline int cal(int x){
	return (long long)rand()*rand()%x+1;
}

int main()
{
	srand(time(0));
	freopen("cycles.in","w",stdout);
	cout<<cal(100000)<<endl;
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
int n,m,a,b,c,A[105],B[105];
char g[105][105];

int main()
{
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	m=read();
	for (int i=1;i<=100;i++) for (int j=1;j<=100;j++) g[i][j]='0';
	for (int i=1;i<=100;i++) A[i]=i*(i-1)*(i-2)/6,B[i]=i*(i-1)/2;
	for (a=1;A[a+1]<=m;a++);
	for (int i=1;i<=a;i++)
		for (int j=1;j<i;j++)
			g[i][j]=g[j][i]='1';
	m-=A[a],b=a;
	while (m){
		b++;
		for (c=1;B[c+1]<=m;c++);
		for (int i=1;i<=c;i++) g[b][i]=g[i][b]='1';
		m-=B[c];
	}
	printf("%d\n",b);
	for (int i=1;i<=b;i++) g[i][b+1]=0,puts(g[i]+1);
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return x;
}
int n,m=0;
char g[105][105];

int main()
{
	freopen("cycles.out","r",stdin);
	freopen("cycles.ans","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) scanf("%s",g[i]+1);
	for (int i=1;i<=n;i++)
		for (int j=1;j<i;j++)
			for (int k=1;k<j;k++)
				if (g[i][j]=='1'&&g[j][k]=='1'&&g[i][k]=='1')
					m++;
	printf("%d\n",m);
	return 0;
}

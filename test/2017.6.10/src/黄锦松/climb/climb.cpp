#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
int n,st,ed,mn,d,len;
char str[105];

int main()
{
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	for (int cas=read();cas--;){
		n=read(),st=read(),ed=read(),mn=d=0;
		scanf("%s",str);len=strlen(str);
		if ((n&1)^((st-ed)&1)) {puts("NO");continue;}
		for (int i=0;i<len;i++){
			if (str[i]=='U') ++d;
			else mn=min(mn,--d);
		}
		int a=max(0,-mn-st);
		if (a+len+abs(ed-st-d)>n) {puts("NO");continue;}
		puts("YES");
	}
	return 0;
}

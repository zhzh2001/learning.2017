#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,m;
int a[101];
 bool solve1()
{
	ll stack[101]; 
	int top=0;
	for (int i=1;i<=n;++i)
	{
		if ((a[i]==0||a[i]==-1)&&top>=2)
		{
			top--;
			stack[top]=stack[top]+stack[top+1];
		}
		else stack[++top]=a[i];
	}
	if (stack[top]==m)
	{	
		printf("0\n");
		return true;
	}
	return false;
}
 bool solve2()
{
	int mark;
	ll stack[101]; 
	int top=0;
	for (int i=1;i<=n;++i)
	{
		if (a[i]==-1) stack[++top]=0,mark=i; 
		else if (a[i]==0&&top>=2)
			 {
			 	top--;
		    	stack[top]=stack[top]+stack[top+1];
	         }
			 else stack[++top]=a[i];
	}
	if (stack[top]!=m&&mark>=top)
	{
		ll ans=(ll)m-stack[top];
		if (ans<0) return false;
		printf("%lld\n",ans);
		return true;
	}
	return false;
}
 bool solve3()
{
	int mark;
	ll stack[101]; 
	int top=0;
	for (int i=1;i<=n;++i)
	{
		if (a[i]==-1) stack[++top]=0,mark=i; 
		else if (a[i]==0&&top>=2)
			 {
			 	top--;
		    	stack[top]=stack[top]+stack[top+1];
	         }
			 else stack[++top]=a[i];
	}
	bool flag=0;
	for (int i=1;i<top;++i) 
		if (stack[i]==-1) flag=1;
	if (stack[top]==m&&mark<top)
	{	
		printf("0\n");
		return true;
	}
	return false;
}
 int main()
{
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	int t;
	cin>>t;
	while (t--)
	{
		cin>>n>>m;
		for (int i=1;i<=n;++i) cin>>a[i];
		if (solve1()) continue;
		if (solve2()) continue;
		if (solve3()) continue;
		printf("-1\n");
	}
}

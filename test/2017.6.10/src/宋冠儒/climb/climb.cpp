#include<bits/stdc++.h>
using namespace std;
 int main()
{
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	int t;
	cin>>t;
	while (t--)
	{
		string s;
		int n,st,ed,left,temp;
		bool flag=false;
		int down=0,up=0;
		cin>>n>>st>>ed;
		cin>>s;
		left=n-s.length();
		temp=st+left;
		for (int i=0;i<s.length();i++)
		{
			if (s[i]=='D') {down++;temp--;}
			else if (s[i]=='U') {up++;temp++;}
			if (temp<0) 
			{
				printf("NO\n");
				flag=1;
				break;
			}
		}
		if (flag) continue;
		int now=st-down+up;
		int dis=abs(now-ed);
		if (left<dis) 
		{
			printf("NO\n");
			continue;
		}
		left-=dis;
		if (left%2==0) printf("YES\n");
		else printf("NO\n");
	}
}

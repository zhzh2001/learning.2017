#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

char s[100035];
int n,st,ed,cas,ans,sum,mn,len;

int main() {

	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);

	for (scanf("%d\n",&cas);cas--;) {
		scanf("%d%d%d\n",&n,&st,&ed);
		if (n==0) { puts(st==ed?"YES":"NO"); continue; }
		
		gets(s+1);
		if (abs(ed-st)%2!=n%2) { puts("NO"); continue; }
		
		len=strlen(s+1),sum=0,mn=0;
		if (s[1]!='U' && s[1]!='D') len=0;
		for (int i=1;i<=len;i++)
			sum+=(s[i]=='U'?1:-1),mn=min(mn,sum);
		
		ans=0;
		for (int k=-n;k<=n && !ans;k++)
			if (st+k+mn>=0 && abs(ed-(st+k+sum))<=n-len-abs(k)) ans=1;
		puts(ans?"YES":"NO");
	}
	return 0;
}

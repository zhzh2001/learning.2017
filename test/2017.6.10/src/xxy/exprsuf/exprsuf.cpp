#include<cstdio>
#include<algorithm>

using namespace std;

typedef long long ll;

const int N=100035;

ll a[N],stk[N],num;
int n,m,cas,flg,top,pos;

bool chk_mark() {
	ll tmp=0; top=0;
	for (int i=1;i<=n;i++) {
		if (a[i]<=0 && top>1)
			 tmp=stk[top-1]+stk[top],top-=2,stk[++top]=tmp;
		else stk[++top]=a[i];
	}
	return stk[top]==m;
}

ll calc(ll key) {
	a[pos]=key;
	ll tmp=0; top=0;
	for (int i=1;i<=n;i++) {
		if (a[i]<=0 && top>1)
			 tmp=stk[top-1]+stk[top],top-=2,stk[++top]=tmp;
		else stk[++top]=a[i];
	}
	a[pos]=-1;
	return stk[top];
}

ll find_num() {
	if (calc(1)==calc(2))
		return (calc(1)==m)?0:-1;
	for (ll l=1,r=1e11;l<=r;) {
		ll mid=l+r>>1;
		if (calc(mid)==m) return mid;
		if (calc(mid)>m) r=mid-1; else l=mid+1;
	}
	return -1;
}

int main() {

	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);

	for (scanf("%d",&cas);cas--;) {
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++) scanf("%I64d",&a[i]),pos=(a[i]==-1?i:pos);
		
		flg=chk_mark();
		num=find_num();
		
		if (!flg && num==-1) puts("-1");
		else if (!flg) printf("%I64d\n",num);
		else if (num==-1) puts("0");
		else printf("%I64d\n",min(num,0ll));
	}
}

/*
8 14
-1 7 3 0 1 2 0 0


*/

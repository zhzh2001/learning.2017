#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=105;

int n,Dat[N][N];
int main(){
	freopen("cycles.out","r",stdin);
	freopen("cycles.res","w",stdout);
	n=read();
	Rep(i,1,n) Rep(j,1,n){
		int ch=getchar();
		while (ch!='0' && ch!='1') ch=getchar();
		Dat[i][j]=ch-48;
	}
	int Ans=0;
	Rep(i,1,n) Rep(j,i+1,n) Rep(k,j+1,n){
		if (Dat[i][j] && Dat[j][k] && Dat[k][i]) Ans++;
	}
	printf("%d\n",Ans);
}

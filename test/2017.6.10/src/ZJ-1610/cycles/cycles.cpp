#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=105;

int n,m,Val[N],Dat[N][N];
int main(){
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	Rep(i,3,100){
		int res=0;
		res=i*(i-1)*(i-2)/6;
		Val[i]=res;
	}
	m=read();int pos=0;
	while (Val[pos+1]<=m) pos++;
	Rep(i,1,pos) Rep(j,1,pos) if (i!=j) Dat[i][j]=1;
	int pos_=pos;
	m-=Val[pos];
	while (m>0){
		pos++;
		Rep(i,1,pos_) if (m>=i-1){
			Dat[i][pos]=Dat[pos][i]=1;
			m-=i-1;
		}
		else break;
	}
	printf("%d\n",pos);
	Rep(i,1,pos){
		Rep(j,1,pos) printf("%d",Dat[i][j]);puts("");
	}
}

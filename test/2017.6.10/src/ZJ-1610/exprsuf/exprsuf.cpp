#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=105;

int n,m,Str[N];
int A[N],wit[N];ll S[N];
int Work(){
	n=read(),m=read();
	Rep(i,1,n) Str[i]=read();
	*A=0;
	Rep(i,1,n){
		A[++*A]=Str[i];
		if (A[*A]==-1) A[*A]=0;
	}
	*S=0;
	Rep(i,1,*A){
		if (A[i]==0){
			if (*S>=2){
				S[*S-1]+=S[*S];--*S;
//				if (S[*S]>m) return -1;
			}
		}
		else S[++*S]=A[i];
	}
	if (S[*S]==m) return 0;
	*A=0;
	Rep(i,1,n){
		A[++*A]=Str[i];
	}
	*S=0;
	Rep(i,1,*A){
		if (A[i]==0){
			if (*S>=2){
				if (wit[*S]) wit[*S-1]=true;
				S[*S-1]+=S[*S];--*S;
//				if (S[*S]>=m) return -1;
			}
		}
		else{
			S[++*S]=A[i];wit[*S]=false;
			if (S[*S]==-1){
				S[*S]=0;wit[*S]=true;
			}
		}
	}
//	printf("%d %d\n",wit[*S],S[*S]);
	if (wit[*S] && S[*S]<m) return m-S[*S];
	return -1;
}
int main(){
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	Rep(i,1,read()) printf("%d\n",Work());
}

#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,st,ed;
bool Work(){
	n=read(),st=read(),ed=read();
	int ch=getchar();
	int len=0,del=0,mi=1e9;
	while (ch!='U' && ch!='D') ch=getchar();
	while (ch=='U' || ch=='D'){
		len++;
		if (ch=='U') del++;else del--;
		mi=min(mi,del);
		ch=getchar();
	}
	if (del<0 && abs(del)>st){
		n-=abs(del)-st;st=abs(del);
	}
	if (mi<0 && abs(mi)>st){
		n-=abs(mi)-st;st=abs(mi);
	}
	st+=del;
	n-=len;
	n-=abs(ed-st);
	st+=ed-st;
//	printf("%d %d\n",st,n);
	if (n<0) return 0;
	if (n%2) return 0;
	return 1;
}
int main(){
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	Rep(i,1,read()) puts(Work()?"YES":"NO");
}
/*
2 0 0
DU
*/

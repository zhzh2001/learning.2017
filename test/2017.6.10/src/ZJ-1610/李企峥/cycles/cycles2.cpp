#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int m,ans,x,y,z,f[200][200],n,a[1000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	m=read();
	For(i,3,100)
	{
		a[i]=i*(i-1)*(i-2);
		a[i]/=6;
	}
	x=0;
	Rep(i,3,100)
	{
		while(m>=a[i])
		{
			ans+=i;
			For(j,x+1,x+i)
				For(k,x+1,x+i)
					f[j][k]=1;
			x+=i;	
			m-=a[i];
		}
	}
	cout<<ans<<endl;
	For(i,1,ans)f[i][i]=0;
	For(i,1,ans)
	{
		For(j,1,ans)cout<<f[i][j]<<" ";
		cout<<endl;		
	}
	return 0;
}


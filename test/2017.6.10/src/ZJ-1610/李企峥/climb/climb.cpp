#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int t,n,st,ed,x,y,z;
int a[100];
bool flag;
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read();st=read();ed=read();
		flag=true;
		ch=getchar();
		x=0;y=0;
		while(ch!='U'&&ch!='D')ch=getchar();
		while(ch=='U'||ch=='D')
		{
			if(ch=='U')x++;else x--;
			y++;
			a[y]=x;
			ch=getchar();
		}
		z=n-y+st;
		For(i,1,y)if(z+a[i]<0){flag=false;break;}
		if(abs(ed-st-a[y])>n-y)flag=false;
		if(flag)cout<<"YES"<<endl;else cout<<"NO"<<endl;
	}
	return 0;
}


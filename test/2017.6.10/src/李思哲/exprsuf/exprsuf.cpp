#include <bits/stdc++.h>
using namespace std;

long long n, m, T, cnt, s[105], w[105], v[105];

int main(){
	freopen("exprsuf.in", "r", stdin);
	freopen("exprsuf.out", "w", stdout);
	scanf("%lld", &T);
	while(T--){
		scanf("%lld%lld", &n, &m);
		for(int i = 1; i <= n; i++) scanf("%lld", &w[i]);
		cnt = 0;
		for(int i = 1; i <= n; i++){
			if(w[i] < 1){
				if(cnt < 2) continue;
				s[cnt - 1] += s[cnt], cnt--;
			}
			else s[++cnt] = w[i];
		}
		if(s[cnt] == m){puts("0"); continue; }
		cnt = 0, memset(v, 0, sizeof(v));
		for(int i = 1; i <= n; i++){
			if(!w[i]){
				if(cnt < 2) continue;
				s[cnt - 1] += s[cnt];
				v[cnt - 1] |= v[cnt], v[cnt] = 0, cnt--;
			}
			else s[++cnt] = (w[i] == -1) ? 0 : w[i], v[cnt] = (w[i] == -1);
		}
		if(!v[cnt]) s[cnt] == m ? puts("0") : puts("-1");
		else s[cnt] < m ? printf("%d\n", m - s[cnt]) : puts("-1");
	}
	return 0;
}
	
			




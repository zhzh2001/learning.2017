#include <bits/stdc++.h>
using namespace std;

long long C[105][105], tu[105][105], w[105], num[105], n, m;

void add1(int x, int y){
	for(int i = x; i <= y; i++)
		for(int j = x; j <= y; j++) if(i != j)
			tu[i][j] = 1;
}

void add2(int x, int y){
	for(int i = 1; i <= y; i++) 
		tu[i][x] = tu[x][i] = 1;
}

void print(){
	printf("%d\n", n);
	for(int i = 1; i <= n; i++){
		for(int j = 1; j <= n; j++) printf("%d", tu[i][j]);
		printf("\n");
	}
}

int main(){
	freopen("cycles.in", "r", stdin);
	freopen("cycles.out", "w", stdout);
	C[0][0] = 1, num[2] = 1;
	for(int i = 1; i <= 100; i++)
		for(int j = 0; j <= i; j++)
			C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
	for(int i = 2; i <= 100; i++) num[i] = i * (i - 1) / 2;
	for(int i = 1; i <= 100; i++) w[i] = C[i][3];
	scanf("%lld", &m);
	for(int i = 1; i <= 100; i++)
		if(w[i] <= m && w[i + 1] > m){n = i; break; }
	m -= w[n];
	add1(1, n);
	if(m){
		int pro = m, prov = n;
		while(pro){
			for(int i = 1; i <= 100; i++)
				if(w[i] <= pro && w[i + 1] > pro){
					prov += i, pro -= w[i]; break;
				}		
		}
		if(prov <= 100){
			while(m){
				for(int i = 1; i <= 100; i++)
					if(w[i] <= m && w[i + 1] > m){
						add1(n + 1, n + i), n += i, m -= w[i]; break;
					}
			}
			print(); return 0;
		}
		else{
			while(m){
				for(int i = 2; i <= 100; i++)
					if(num[i] <= m && num[i + 1] > m){
						add2(n + 1, i), n++, m -= num[i]; break;
					}
			}
			print(); return 0;
		}
	}
	else print();
	return 0;
}

#include <bits/stdc++.h>
using namespace std;

int n, ans, tu[105][105];

char s[105];

int main(){
	freopen("cycles.out", "r", stdin);
	freopen("t.out", "w", stdout);	
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
		scanf("%s", s + 1);
		for(int j = 1; j <= n; j++) tu[i][j] = s[j] - '0';
	}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j < i; j++)
			for(int k = 1; k < j; k++)
				if(tu[i][j] && tu[j][k] && tu[i][k]) ans++;
	printf("%d", ans);
	return 0;
}
	


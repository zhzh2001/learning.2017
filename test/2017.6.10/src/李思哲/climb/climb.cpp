#include <bits/stdc++.h>
using namespace std;

int T, t, st, en, he[1000005], minn;

char s[1000005];

int main(){
	freopen("climb.in", "r", stdin);
	freopen("climb.out", "w", stdout);
	scanf("%d", &T);
	while(T--){
		scanf("%d%d%d", &t, &st, &en), minn = 0x3f3f3f3f;
		int pro = en - st, prov;
		scanf("%s", s + 1); int n = strlen(s + 1);
		prov = t - n;
		for(int i = 1; i <= n; i++){
			if(s[i] == 'U') he[i] = he[i - 1] + 1;
			else he[i] = he[i - 1] - 1; minn = min(he[i], minn);
		}
		int now = en - st - he[n];
		if(abs(now) > abs(prov)){puts("NO"); continue; }
		if(prov + minn + st < 0){puts("NO"); continue; }
		if((now & 1) ^ (prov & 1)){puts("NO"); continue; }
		puts("YES");
	}
	return 0;
}



#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,a[101][101],ans;
char ch[101][101];
int main(){
	freopen("cycles.out","r",stdin);
	freopen("spj.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) scanf("%s",ch[i]+1);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) a[i][j]=ch[i][j]-48;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(i==j) continue;
			for(int k=1;k<=n;k++){
				if(i==k||k==j) continue;
				ans+=(a[i][j]&&a[j][k]&&a[k][i]);
			}
		}
	printf("%d",ans/6);
	return 0;
}
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,st,ed,p,q,mp,x,xq;
char ch[60];

int main(){
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	int t=read();
	while(t--){
		n=read(); st=read(); ed=read();
		scanf("%s",ch);
		int len=strlen(ch);
		p=0; mp=200;
		for(int i=0;i<len;i++){
			if(ch[i]=='U') p++;
			else{ p--; mp=min(mp,p); }
		}
		mp=st+mp;
		xq=ed-(p+st); n-=len; q=abs(xq);
		if(mp<0){
			mp=-mp;
			if((n-q)%2!=0){
				printf("NO\n");
				continue;
			}
			if(n<q){
				printf("NO\n");
				continue;
			}
			if(xq>0){
				x=xq+(n-q)/2;
				if(x>=mp) printf("YES\n");
				else printf("NO\n");
				continue;
			}
			else{
				x=(n-q)/2;
				if(x>=mp) printf("YES\n");
				else printf("NO\n");
				continue;
			}
		}
		else{
			if(n<q) printf("NO\n");
			else{
				if((n-q)%2==0) printf("YES\n");
				else printf("NO\n");
			}
		}	
	}
	return 0;
}

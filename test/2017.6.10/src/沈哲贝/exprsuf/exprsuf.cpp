#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<bitset>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define p 998244353
#define maxn 50010
using namespace std;
const ll inf=1e9;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll a[maxn],n,m,q[maxn],top,cho;
ll get(){
	ll top=0;
	For(i,1,n)
	if (top<2){
		if (!a[i])	continue;
		else	q[++top]=a[i];
	}else{
		if (!a[i])	q[--top]=q[top]+q[top+1];
		else	q[++top]=a[i];
	}
	return q[top];
}
int main(){
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	m=read();
		For(i,1,n){
			a[i]=read();
			if (a[i]==-1)	cho=i;
		}
		a[cho]=0;
		ll x=get(),y,z;
		if (x==m){	puts("0");	continue;}//可以为0
		a[cho]=1;	y=get();
		a[cho]=2;	z=get();
		if (y>m){	puts("-1");	continue;}//因为只能加因此不可能
		if (y==m){	puts("1");	continue;}//y是解或与这个位置无关
		if (y==z){	puts("-1");	continue;}//y不会产生变化
		writeln(m-y+1);
	}
}
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<map>
#define ll int
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
char s[maxn];
ll n,st,ed,kkk,sum,mn,m,up,down,remain;
int main(){
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	st=read();	ed=read();	scanf("%s",s+1);	m=strlen(s+1);
		kkk=ed-st;	sum=mn=0;
		if ((kkk^n)&1){	puts("NO");	continue;}
		For(i,1,m){
			sum+=s[i]=='U'?1:-1;
			mn=min(sum,mn);
		}kkk-=sum;	remain=n-m;
		if (abs(kkk)>remain){	puts("NO");	continue;}
		up=(kkk+remain)>>1;	down=(kkk-remain)>>1;
		puts(up+mn+st<0?"NO":"YES");
	}
}

#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;

ifstream fin("climb.in");
ofstream fout("climb.out");

int T, st, ed, offset, n, u, d, maxd;
string opt;

int work(int cur, string opt) {
	for (int i = 0; i < (int)opt.length(); i++)
		if (opt[i] == 'U') ++cur;
			else {
				--cur;
				if (cur < 0) return -1;
			}
	return cur;
}

bool OK;

int main() {
	fin >> T;
	while (T--) {
		fin >> n >> st >> ed >> opt;
		offset = ed - st;
		d = maxd = 0;
		if (n == opt.length()) {
			if (work(st, opt) == ed) fout << "YES" << endl;
			else fout << "NO" << endl;
			continue;
		}
		if (n < offset) {
			fout << "NO" << endl;
			continue;
		}
		for (int i = 0; i < (int)opt.length(); i++)
			if (opt[i] == 'U') {
				--offset, d--;
				maxd = max(maxd, d);
			}
			else {
				++offset, d++;
				maxd = max(maxd, d);
			}
		if (max(st, ed) + (n - (int)opt.length()) / 2 - maxd < 0) fout << "NO" << endl;
		else if (abs(offset) > n - (int)opt.length()) fout << "NO" << endl;
		else if (abs(offset) == n - (int)opt.length()) fout << "YES" << endl;
		else if (abs(offset) < n - (int)opt.length()) {
			if (((n - (int)opt.length() - offset) & 1) == 0) fout << "YES" << endl;
				else fout << "NO" << endl;
		}
	}
	return 0;
}
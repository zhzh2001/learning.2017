#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int main() {
	srand(GetTickCount());
	int T = 60;
	cout << T << endl;
	while (T--) {
		cout << rand() % 30 + 1 << ' ' << rand() % 30 << ' ' << rand() % 30 << endl;
		for (int i = 0; i < 20; i++) {
			cout << ((rand() & 1) ? 'U' : 'D');
		}
		cout << endl;
	}
	return 0;
}
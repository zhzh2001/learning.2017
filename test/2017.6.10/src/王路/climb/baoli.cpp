#include <bits/stdc++.h>
using namespace std;

ifstream fin("climb.in");
ofstream fout("climb.out");

int work(int cur, string opt) {
	for (int i = 0; i < (int)opt.length(); i++)
		if (opt[i] == 'U') ++cur;
			else {
				--cur;
				if (cur < 0) return -1;
			}
	return cur;
}

bool OK = false;

int T, st, ed, n;
string opt;

int main() {
	fin >> T;
	while (T--) {
		OK = false;
		fin >> n >> st >> ed >> opt;
		for (int i = 0; i <= min(st, ed) + n; i++) {
			int pos = work(i, opt);
			if (pos == -1) continue;
			int cost = abs(st - i) + (int)opt.length() + abs(ed - pos);
			if ((n - cost) == 0) {
				OK = true;
				break;
			}
			else if (n - cost > 0) {
				if (((n - cost) & 1) == 0) {
					OK = true;
					break;
				}
			}
		}
		if (OK) fout << "YES" << endl;
		else fout << "NO" << endl;
	}
	return 0;
}
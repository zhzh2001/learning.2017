#pragma GCC optimize(2)
#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <ctime>
using namespace std;

ifstream fin("exprsuf.in");
ofstream fout("exprsuf.out");

const int N = 55;

typedef long long ll;
int obj[N];
ll st[N], tot, cnt, n, m, label = 0;

inline bool work() {
	tot = 0, cnt = 0;
	for (int i = n; i >= 1; i--) {
		if (obj[i] == 0) cnt++;
		else st[++tot] = obj[i];
		if (tot >= 2 && cnt > 0) {
			st[tot - 1] = st[tot] + st[tot - 1];
			--cnt, --tot;
		}
	}
	if (st[1] == m) {
		fout << obj[label] << endl;
		return true;
	} else if (st[1] > m && obj[label] > 0) {
		fout << -1 << endl;
		return true;
	}
	return false;
}

int sta[N];

pair<ll, int> check() {
	tot = 0, cnt = 0;
	memset(sta, 0, sizeof sta);
	for (int i = n; i >= 1; i--) {
		if (obj[i] == 0) cnt++;
		else {
			st[++tot] = obj[i], sta[tot] = (obj[i] == -1);
			if (obj[i] == -1) st[tot] = 0;
		}
		if (tot >= 2 && cnt > 0) {
			st[tot - 1] = st[tot] + st[tot - 1];
			sta[tot - 1] = sta[tot] + sta[tot - 1];
			--cnt, --tot;
		}
	}
	return make_pair(st[1], sta[1]);
}

int main() {
	int T;
	fin >> T;
	while (T--) {
		fin >> n >> m;
		label = 0;
		for (int i = 1; i <= n; i++) {
			fin >> obj[i];
			if (obj[i] == -1) label = i;
		}
		if (m <= 20000) {
			bool flag = false;
			for (int i = 0; i <= m; i++) {
				obj[label] = i;
				if (work()) {
					flag = true;
					break;
				}
			}
			if (!flag) fout << "-1" << endl;
			// cerr << clock() << endl;
		} else {
			obj[label] = 0;
			if (work()) continue;
			obj[label] = -1;
			pair<ll, int> res = check();
			
			// cout << res.first << ' ' << res.second << endl;
			
			if (!res.second) {
				fout << "-1" << endl;
				continue;
			}
			
			if (m - res.first <= 0) {
				fout << "-1" << endl;
				continue;
			}
			
			fout << (ll)m - res.first << endl;
			continue;
		}
	}
	return 0;
}
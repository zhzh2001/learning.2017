#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int main() {
	freopen("exprsuf.in", "w", stdout);
	srand(GetTickCount());
	int T = 25;
	cout << T << endl;
	while (T--) {
		int n = 50;
		cout << n << ' ' << (rand() * rand()) << endl;
		for (int i = 1; i <= n; i++)
			cout << (rand() * rand() - 1) << ' ';
		cout << endl;
	}
	return 0;
}
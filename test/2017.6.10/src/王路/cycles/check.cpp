#include <bits/stdc++.h>
using namespace std;

const int N = 105;
int m, n;
int mat[N][N];

int main() {
	FILE *in = fopen("cycles.in", "r");
	FILE *out = fopen("cycles.out", "r");
	fscanf(in, "%d", &m);
	fscanf(out, "%d", &n);
	assert(n <= 100);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			fscanf(out, "%1d", &mat[i][j]);
			if (i == j) assert(mat[i][j] == 0);
		}
	int ans = 0;
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (!mat[i][j] && i != j && i != k && k != j)
					mat[i][j] = mat[i][k] && mat[k][j];
			
	for (int i = 1; i <= n; i++)
		for (int j = 1; j < i; j++)
			if (mat[i][j])
				for (int k = 1; k < j; k++)
					if (mat[i][k] && mat[k][j]) ans++;
	assert(ans == m);
	cerr << "AC" << endl;
	return 0;
}
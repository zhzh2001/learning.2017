#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;

ifstream fin("cycles.in");
ofstream fout("cycles.out");

const int M = 100005, N = 105;

int mat[N][N], m;

int work1() {
	memset(mat, 0, sizeof mat);
	int tot = 0;
	for (int i = 1; i <= m; i++) {
		mat[tot + 1][tot + 2] = mat[tot + 2][tot + 1] = 1;
		mat[tot + 2][tot + 3] = mat[tot + 3][tot + 2] = 1;
		mat[tot + 3][tot + 1] = mat[tot + 1][tot + 3] = 1;
		tot += 3;
	}
	fout << 100 << endl;
	for (int i = 1; i <= 100; i++) {
		for (int j = 1; j <= 100; j++)
			fout << mat[i][j];
		fout << endl;
	}
	return 0;
}

int work2() {
	
	return 0;
}

int work3() {
	
	return 0;
}

int main() {
	fin >> m;
	if (m <= 12) exit(work1());
	if (m <= 100) exit(work2());
	work3();
	return 0;
}
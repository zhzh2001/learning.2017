//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("climb.in");
ofstream fout("climb.out");
string s;
int n,st,ed,len;
int T;
int move,di;
int main(){
	//freopen("climb.in","r",stdin);
	//freopen("climb.out","w",stdout);
	fin>>T;
	while(T--){
		fin>>n>>st>>ed;
		fin>>s;
		move=0,di=0;
		int len=s.length();
		for(int i=0;i<=len-1;i++){
			if(s[i]=='U'){
				move=move+1;
			}else{
				move=move-1;
				di=min(move,di);
			}
		}
		if(di>0){
			di=0;
		}else{
			di=abs(di);
		}
		if(ed>=st){
			if(st>=di){
				n-=len;
				st+=move;
				if(n>=abs(st-ed)){
					fout<<"YES"<<endl;
				}else{
					fout<<"NO"<<endl;
				}
			}else{
				n-=abs(st-di);
				n-=len;
				st+=move;
				if(n>=abs(st-ed)){
					fout<<"YES"<<endl;
				}else{
					fout<<"NO"<<endl;
				}
			}
		}else{
			if(st>=di){
				n-=len;
				st+=move;
				if(n>=abs(st-ed)){
					fout<<"YES"<<endl;
				}else{
					fout<<"NO"<<endl;
				}
			}else{
				n-=abs(st-di);
				n-=len;
				st+=move;
				if(n>=abs(st-ed)){
					fout<<"YES"<<endl;
				}else{
					fout<<"NO"<<endl;
				}
			}
		}
		/*
		if(di==0){
			st+=move;
			if(abs(n-len)>=abs(st-ed)){
				cout<<"YES"<<endl;
			}else{
				cout<<"NO"<<endl;
			}
		}else{
			
			int now;
			bool yes,yess;
			for(int i=st-abs(n-len);i<=st+abs(n-len);i++){
				now=i,yes=false,yess=true;
				if(i<0){
					continue;
				}
				for(int j=0;j<=len-1;j++){
					if(s[j]=='U'){
						now=now+1;
					}else{
						now=now-1;
						if(now==-1){
							yess=false;
							break;
							move=0;
						}
					}
				}
				if(abs(n-len-abs(i-st))>=abs(now-ed)&&yess){
					yes=true;
					cout<<i<<endl;
					cout<<"YES"<<endl;
					break;
				}
			}
			if(!yes){
				cout<<"NO"<<endl;
			}
		}
		*/
	}
	return 0;
}
/*

in:
7
4 0 4
UU
4 0 4
D
4 100000 100000
DDU
4 0 0
DDU
20 20 20
UDUDUDUDUD
20 0 0
UUUUUUUUUU
20 0 0
UUUUUUUUUUU

out:
YES
NO
YES
NO
YES
YES
NO

*/

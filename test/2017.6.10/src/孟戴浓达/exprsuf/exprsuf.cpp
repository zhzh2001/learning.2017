//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<stack>
using namespace std;
ifstream fin("exprsuf.in");
ofstream fout("exprsuf.out");
int n,m,pos;
int x[53];
int T;
stack<int> st2;
int st1num,st2num;
int ans;
inline int stack_moni(){
	for(int i=n;i>=1;i--){
		if(x[i]==0){
			st1num++;
		}else{
			st2.push(x[i]);
			st2num++;
		}
		if(st2num>=2){
			if(st1num>=1){
				int x1=st2.top();
				st2.pop();
				int x2=st2.top();
				st2.pop();
				st2num--,st1num--;
				st2.push(x1+x2);
			}else{
				st2.pop();
				st2num--;
			}
		}
	}
	while(st2num){
		if(st2num==1){
			return st2.top();
		}
		st2.pop();
		st2num--;
	}
}
int main(){
	//freopen("exprsuf.in","r",stdin);
	//freopen("exprsuf.out","w",stdout);
	fin>>T;
	while(T--){
		fin>>n>>m;
		for(int i=1;i<=n;i++){
			fin>>x[i];
			if(x[i]==-1){
				pos=i;
			}
		}
		x[pos]=0;
		ans=stack_moni();
		if(ans==m){
			fout<<"0"<<endl;
		}else{
			x[pos]=-1;
			ans=stack_moni();
			fout<<ans<<endl;
			/*
			if(m-ans+1>=0){
				cout<<m-ans+1<<endl;
			}else{
				cout<<"-1"<<endl;
			}
			*/
		}
		st2num=0,st1num=0;
	}
	return 0;
}
/*

in:
4
3 10
7 -1 0
8 16
-1 8 4 0 1 2 0 0
9 100000000
1000000000 1000000000 1000000000 1000000000 -1 0 0 0 0
4 3
7 -1 3 0

out:
3
0
0
-1
-1
-1

*/

#include<cstdio>
int exp[100],t,i,fl,n,as,tmp;
inline int tke1(int n){
	int l=0,r=0;
	if((!exp[n])||(exp[n]==-1)){
		if(n-1>0)l=tke1(n-1);
		if(n-2>0)r=tke1(n-2);
		return l+r;
	}
	return exp[n];
}
inline int tke2(int n){
	int l=0,r=0;
	if(!exp[n]){
		if(n-1>0)l=tke2(n-1);
		if(n-2>0)r=tke2(n-2);
		return l+r;
	}
	if(exp[n]==-1){fl=1;return 0;}
	return exp[n]; 
}
int main()
{
	freopen("expsuf.in","r",stdin);
	freopen("expsuf.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d",&n,&as);
		for(i=1;i<=n;i++)
			scanf("%d",&exp[i]);
		//as +
		//tmp=tke1(n);
		//if(tmp==as){puts("0");continue;}
		//as num
		tmp=tke2(n);
		if(!fl&&tmp!=as)puts("-1");
		if(!fl&&tmp==as)puts("0");
		if(fl&&tmp<as)printf("%d\n",as-tmp);
		if(fl&&tmp>=as)puts("-1");
	}
}

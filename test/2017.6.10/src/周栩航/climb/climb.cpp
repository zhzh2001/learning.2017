#include<bits/stdc++.h>
using namespace std;
char s[55];
int n,st,et,t;
int main()
{
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d",&n,&st,&et);
		scanf("%s",s);
		int l=strlen(s);
		int x=st;
		for(int i=0;i<l;i++){
			if(s[i]=='D')x--;
			if(s[i]=='U')x++;
			if(x<0)n--,x=0;
		}
		if(n-l>=abs(x-et)&&((n-l)-abs(x-et))%2==0)printf("YES\n");
		else printf("NO\n");
	}
	return 0;
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1005;
int a[N][N],n,m;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("cycles.in","r",stdin);
	freopen("cycles.out","w",stdout);
	m=Read();
	for (int i=3;i<=100;i++)
		if (i*(i-1)*(i-2)/6>m){n=i-1; break;}
	int tmp=m-n*(n-1)*(n-2)/6,tot=n+1;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (i!=j) a[i][j]=a[j][i]=1;
	for (;tmp;tot++){int num=0;
		for (int i=2;i<=n;i++)
			if (tmp<i*(i-1)/2){num=i-1; break;}
		for (int i=1;i<=num;i++) a[tot][i]=a[i][tot]=1;
		tmp-=num*(num-1)/2;
	}
	printf("%d\n",tot);
	for (int i=1;i<=tot;i++,puts(""))
		for (int j=1;j<=tot;j++)
			printf("%d",a[i][j]);
}

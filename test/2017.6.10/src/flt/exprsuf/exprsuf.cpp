#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=55; int top,n,f[N]; long long s[N],m,a[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	for (int T=Read();T--;){
		n=Read(),m=Read(); top=0;
		for (int i=1;i<=n;i++) a[i]=Read();
		for (int i=1;i<=n;i++){
			if (a[i]<=0&&top>1) s[top-1]=s[top]+s[top-1],top--;
			else if (a[i]>0) s[++top]=a[i];
		}
		if (s[top]==m){puts("0"); continue;}
		top=0; memset(f,0,sizeof(f));
		for (int i=1;i<=n;i++){
			if (a[i]==0&&top>1)
				s[top-1]=s[top]+s[top-1],f[top-1]|=f[top],f[top--]=0;
			if (a[i]==-1) s[++top]=0,f[top]=1;
			if (a[i]>0) s[++top]=a[i],f[top]=0;
		}
		if ((!f[top])&&(s[top]!=m)||s[top]>=m){puts("-1"); continue;}
		if (!f[top]){puts("1"); continue;}
		printf("%d\n",m-s[top]);
	}
}

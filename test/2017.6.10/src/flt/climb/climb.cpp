#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5; char s[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
//	freopen("climb.in","r",stdin);
//	freopen("climb.out","w",stdout);
	for (int T=Read();T--;){
		int n=Read(),st=Read(),end=Read();
		scanf("\n%s",s+1); int len=strlen(s+1);
		for (int i=1;i<=len;i++) st+=(s[i]=='U'?1:-1);
		if (abs(st-end)>n-len||(abs(st-end)-n+len)&1){puts("NO"); continue;}
		int tmp=st+(abs(st-end)-n+len)/2,flag=0;
		if (st<=end) tmp+=end-st;
		for (int i=1;i<=len;i++){
			st+=(s[i]=='U'?1:-1); if (st<0) flag=1;
		}
		puts(flag?"NO":"YES");
	}
}
/*
7
4 0 4
UU
4 0 4
D
4 100000 100000
DDU
4 0 0
DDU
20 20 20
UDUDUDUDUD
20 0 0
UUUUUUUUUU
20 0 0
UUUUUUUUUUU
*/

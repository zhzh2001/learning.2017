#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll b[100];
ll a[100],top;
ll n,m;
ll flag;

int main(){
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	ll t=read();
	while(t--){
		n=read(); m=read(); top=0;
		for(ll i=1;i<=n;i++){
			b[i]=read(); ll x=b[i];
			if(x==0||x==-1){
				if(top>=2){
					ll p=a[top]+a[top-1];
					a[--top]=p;
				}
			}
			else a[++top]=x;
		}
		if(a[top]==m){
			printf("0\n");
			continue;
		}
		top=0; flag=-1;
		for(ll i=1;i<=n;i++){
			ll x=b[i];
			if(x==0){
				if(top>=2){
					if(flag==top||flag==top-1) flag=top-1;
					ll p=a[top]+a[top-1];
					a[--top]=p;
				}
			}
			else if(x==-1){
				a[++top]=0;
				flag=top;
			}
			else a[++top]=x;
		}
		if(a[top]>m) printf("-1\n");
		else{
			if(flag==top){
				if(a[top]==m) printf("-1\n");
				else printf("%lld\n",m-a[top]);
			}
			else printf("-1\n");
		}
	}
	return 0;
}

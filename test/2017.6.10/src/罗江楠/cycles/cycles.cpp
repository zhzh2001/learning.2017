#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int c[4][105];
int mp[105][105], sz;
int dfs(int x){
	if(x < 0) return 0;
	if(x == 0) return 1;
	for(int k = 1; k <= 100; k++){
		int a = rand()%100+1, b = rand()%100+1, sum = 0;
		if(mp[a][b] || a == b) continue;
		for(int i = 1; i <= sz; i++)
			if(mp[i][a] && mp[i][b]) sum++;
		mp[a][b] = mp[b][a] = 1;
		if(dfs(x-sum))return 1;
		mp[a][b] = mp[b][a] = 0;
	}
	return 0;
}
int main(){
	File("cycles");
	int x = read();
	sz = 100; dfs(x);
	printf("%d\n", sz);
	for(int i = 1; i <= sz; i++, puts(""))
		for(int j = 1; j <= sz; j++) putchar(mp[i][j] ? '1' : '0');
	return 0;
}
#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int mp[105][105];
int main(){
	freopen("cycles.out", "r", stdin);
	int n = read(), ans = 0;
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			scanf("%1d", &mp[i][j]);
	for(int i = 1; i <= n; i++)
		for(int j = i+1; j <= n; j++) if(mp[i][j])
			for(int k = j+1; k <= n; k++) if(mp[j][k])
				ans += mp[i][k];
	printf("%d\n", ans);
}
@echo off
for /L %%i in (1, 1, 100) do (
	echo Running on pre-test%%i...
	mk
	cycles
	check > cycles2.ans
	fc cycles2.ans cycles.ans > nul
	if errorlevel 1 echo Wrong Answer at pre-test%%i & pause & exit
	cls
)
echo Accepted & pause
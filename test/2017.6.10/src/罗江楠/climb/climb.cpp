#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
char ch[N];
int main(){
	File("climb");
	int t = read();
	while(t--){
		int n = read(), st = read(), ed = read();
		scanf("%s", ch+1); int len = strlen(ch+1), mn = 0, pl = 0;
		for(int i = 1; i <= len; i++)
			mn = min(mn, ch[i] == 'U' ? ++pl : --pl);
		if(mn < 0){
			int cb = max(0, -mn-st);
			n -= cb; st += cb;
		}
		puts(n >= len && abs(st+pl-ed) <= n-len && (abs(st+pl-ed)&1) == ((n-len)&1) ? "YES" : "NO");
	}
}
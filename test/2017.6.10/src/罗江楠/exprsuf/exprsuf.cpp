#include <bits/stdc++.h>
#define N 100020
#define int long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[N], st[N];
signed main(){
	File("exprsuf");
	int t = read();
	while(t--){
		int n = read(), m = read(), f, pos;
		st[0] = 0;
		for(int i = 1; i <= n; i++)
			if((a[i]=read()) == -1) pos = i;
		a[pos] = 0;
		for(int i = 1; i <= n; i++){
			if(!a[i]){
				if(st[0] < 2) continue;
				int tmp = st[st[0]--];
				st[st[0]] += tmp;
			}
			else st[++st[0]] = a[i];
		}
		if(st[st[0]] == m){puts("0");continue;}
		a[pos] = 1;
		st[0] = 0;
		for(int i = 1; i <= n; i++){
			if(!a[i]){
				if(st[0] < 2) continue;
				if(f == st[0]) f--;
				int tmp = st[st[0]--];
				st[st[0]] += tmp;
			}
			else{
				st[++st[0]] = a[i];
				if(pos == i) f = st[0];
			}
		}
		if(st[0] && f == st[0] && m-st[st[0]]+1 > 0) printf("%lld\n", m-st[st[0]]+1);
		else puts("-1");
	}
	return 0;
}

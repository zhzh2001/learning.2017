#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 55
#define ll long long
ll m,a[N],ans;
int T,i,n,pos,vis[N];
ll check1(){
	memset(vis,0,sizeof(vis));
	ll res=0,deep=0;
	for(int i=n;i;i--){vis[i]=1;
		if(!deep&&a[i]>0)return res+a[i];
		if(a[i]==0)deep++;
			else res+=a[i],deep--;
	}return res;
}
int main()
{
	freopen("exprsuf.in","r",stdin);
	freopen("exprsuf.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%I64d",&n,&m);
		for(i=1;i<=n;i++){
			scanf("%I64d",&a[i]);
			if(a[i]==-1)pos=i;
		}a[pos]=0;if(check1()==m){puts("0");continue;}
		a[pos]=1;ans=check1();
		if(ans==m){puts("1");continue;}
		if(!vis[pos]){puts("-1");continue;}
		if(m-ans<0){puts("-1");continue;}
		ans=m-ans+1;printf("%I64d\n",ans);
	}return 0;
}

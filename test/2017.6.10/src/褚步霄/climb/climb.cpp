#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
int T,n,st,ed,U,D,i,x,now,len,pd;
char s[N];
int main()
{
	freopen("climb.in","r",stdin);
	freopen("climb.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&st,&ed);
		U=D=0;scanf("%s",s+1);len=strlen(s+1);
		for(i=1;i<=len;i++)
			if(s[i]=='U')U++;else D++;
		x=ed-st-(U-D);
		if((n-len-x)%2==1){puts("NO");continue;}
		U=(n-len-x)/2+x,D=(n-len-x)/2;
		if(U<0||D<0){puts("NO");continue;}
		now=st+U;pd=1;
		for(i=1;i<=len;i++){
			if(s[i]=='U')now++;else now--;
			if(now<0){puts("NO");pd=0;break;}
		}if(!pd)continue;
		if(now-U<0){puts("NO");continue;}
		puts("YES");
	}return 0;
}

#include<fstream>
#include<cstdlib>
#include<cstring>
using namespace std;
ifstream fin("cycles.in");
ofstream fout("cycles.out");
const int N=100;
bool mat[N+5][N+5];
void print(int n)
{
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			fout<<mat[i][j];
		fout<<endl;
	}
	exit(0);
}
int main()
{
	int m;
	fin>>m;
	for(int n=3;n<=N;n++)
	{
		memset(mat,false,sizeof(mat));
		for(int i=1;i<n;i++)
			mat[i][i+1]=mat[i+1][i]=true;
		mat[n][1]=mat[1][n]=true;
		for(int i=2;i<n;i++)
			mat[1][i]=mat[i][1]=true;
		int now=n-2,val=2,cnt=0,maxc=n-3;
		if(now==m)
			print(n);
		for(int i=2;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(!mat[i][j])
				{
					mat[i][j]=mat[j][i]=true;
					now+=val;
					if(now==m)
						print(n);
					if((++cnt)==maxc)
					{
						val++;
						cnt=0;
						maxc--;
					}
				}
	}
	for(int n=3;n<=N;n++)
	{
		memset(mat,false,sizeof(mat));
		for(int i=1;i<n;i++)
			mat[i][i+1]=mat[i+1][i]=true;
		mat[n][1]=mat[1][n]=true;
		for(int i=2;i<n;i++)
			mat[1][i]=mat[i][1]=true;
		int now=n-2,val=2,maxc=2;
		for(int i=n;i;i--)
			for(int j=n;j>i;j--)
				if(!mat[i][j])
				{
					mat[i][j]=mat[j][i]=true;
					now+=val;
					if(now==m)
						print(n);
					if((++val)>maxc)
					{
						val=2;
						maxc++;
					}
				}
	}
	return 0;
}
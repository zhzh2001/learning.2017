#include<fstream>
#include<string>
#include<cstring>
using namespace std;
ifstream fin("match.in");
ofstream fout("match.out");
string s;
int f[305][305],t[256];
int dfs(int l,int r)
{
	if(l==r+1)
		return 0;
	int& now=f[l][r];
	if(now!=-1)
		return now;
	now=dfs(l+1,r-1);
	if(t[s[l]]>0&&t[s[r]]<0)
		now+=2;
	else
		if(t[s[l]]+t[s[r]])
			now++;
	for(int i=l+1;i<r;i+=2)
		now=min(now,dfs(l,i)+dfs(i+1,r));
	return now;
}
int main()
{
	fin>>s;
	memset(f,-1,sizeof(f));
	t['(']=-1;t['[']=-2;t['{']=-3;
	t[')']=1;t[']']=2;t['}']=3;
	fout<<dfs(0,s.length()-1)<<endl;
	return 0;
}
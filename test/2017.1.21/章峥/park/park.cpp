#include<fstream>
#include<string>
#include<algorithm>
#include<cctype>
#include<cstring>
#include<ctime>
using namespace std;
ifstream fin("park.in");
ofstream fout("park.out");
int main()
{
	string s;
	fin>>s;
	char p[30];
	memset(p,0,sizeof(p));
	int n=s.length(),pn=0,pos[30][2];
	for(int i=0;i<s.length();i++)
		if(islower(s[i]))
		{
			p[++pn]=s[i];
			pos[s[i]-'a'][0]=i;
		}
		else
			pos[s[i]-'A'][1]=i;
	sort(p+1,p+pn+1);
	do
	{
		bool exists[30];
		memset(exists,false,sizeof(exists));
		bool flag=true;
		for(int i=1;i<=pn;i++)
		{
			bool can=true;
			for(int j=pos[p[i]-'a'][0];j!=pos[p[i]-'a'][1];j=(j+1)%n)
				if((islower(s[j])&&s[j]!=p[i]&&!exists[s[j]-'a'])||(isupper(s[j])&&exists[s[j]-'A']))
				{
					can=false;
					break;
				}
			if(can)
				exists[p[i]-'a']=true;
			else
			{
				can=true;
				for(int j=pos[p[i]-'a'][0];j!=pos[p[i]-'a'][1];j=(j-1+n)%n)
					if((islower(s[j])&&s[j]!=p[i]&&!exists[s[j]-'a'])||(isupper(s[j])&&exists[s[j]-'A']))
					{
						can=false;
						break;
					}
				if(can)
					exists[p[i]-'a']=true;
				else
				{
					flag=false;
					break;
				}
			}
		}
		if(flag)
		{
			fout<<string(p+1)<<endl;
			return 0;
		}
		if(clock()>CLOCKS_PER_SEC*0.5)
		{
			fout<<"NONE\n";
			return 0;
		}
	}
	while(next_permutation(p+1,p+pn+1));
	fout<<"NONE\n";
	return 0;
}
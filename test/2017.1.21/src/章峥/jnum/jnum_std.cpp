#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("jnum.in");
ofstream fout("jnum.ans");
int main()
{
	int l,r;
	fin>>l>>r;
	int ans=0;
	for(int i=l;i<=r;i++)
	{
		int t=i,len=0,dig[15];
		do
			dig[++len]=t%10;
		while(t/=10);
		bool flag=true;
		for(int i=1;i<len;i++)
			if(abs(dig[i]-dig[i+1])<2)
			{
				flag=false;
				break;
			}
		ans+=flag;
	}
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
using namespace std;
ifstream fin("jnum.in");
ofstream fout("jnum.out");
int l,r,ans;
int cnt[10];
void dfs(int k,int len,int x)
{
	if(x>=l&&x<=r)
		ans++;
	if(k>len||x>r)
		return;
	if(x&&x%10>=2)
		dfs(k+1,len,x*10);
	for(int i=1;i<10;i++)
		if(abs(x%10-i)>=2||x==0)
			dfs(k+1,len,x*10+i);
}
int main()
{
	while(fin>>l>>r)
	{
		time_t start=clock();
		char buf[15];
		sprintf(buf,"%d",r);
		int len=strlen(buf);
		ans=0;
		dfs(1,len,0);
		fout<<ans<<',';
		cout<<l<<' '<<r<<' '<<(double)(clock()-start)/CLOCKS_PER_SEC<<endl;
	}
	return 0;
}
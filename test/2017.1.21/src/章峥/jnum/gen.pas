program gen;
const
  n=10000;
var
  l,r,i:longint;
begin
  assign(output,'jnum.in');
  rewrite(output);
  randomize;
  for i:=1 to n do
  begin
    l:=random(1500000)+1;
	r:=l+random(5000000)+1;
	writeln(l,' ',r);
  end;
  close(output);
end.
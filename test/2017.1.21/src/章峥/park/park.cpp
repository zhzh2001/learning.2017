#include<fstream>
#include<string>
#include<cstdlib>
#include<algorithm>
#include<ctime>
using namespace std;
ifstream fin("park.in");
ofstream fout("park.out");
string s;
int n,pn,pos[30][2];
char p[30],now[30];
bool done[30];
void dfs(int k)
{
	if(k==pn+1)
	{
		fout<<string(now+1)<<endl;
		exit(0);
	}
	if(clock()>CLOCKS_PER_SEC*0.5)
	{
		fout<<"NONE\n";
		exit(0);
	}
	for(int i=1;i<=pn;i++)
	{
		if(done[p[i]-'a'])
			continue;
		now[k]=p[i];
		bool can=true;
		for(int j=pos[p[i]-'a'][0];j!=pos[p[i]-'a'][1];j=(j+1)%n)
			if((islower(s[j])&&s[j]!=p[i]&&!done[s[j]-'a'])||(isupper(s[j])&&done[s[j]-'A']))
			{
				can=false;
				break;
			}
		if(can)
		{
			done[p[i]-'a']=true;
			dfs(k+1);
			done[p[i]-'a']=false;
		}
		else
		{
			can=true;
			for(int j=pos[p[i]-'a'][0];j!=pos[p[i]-'a'][1];j=(j-1+n)%n)
				if((islower(s[j])&&s[j]!=p[i]&&!done[s[j]-'a'])||(isupper(s[j])&&done[s[j]-'A']))
				{
					can=false;
					break;
				}
			if(can)
			{
				done[p[i]-'a']=true;
				dfs(k+1);
				done[p[i]-'a']=false;
			}
		}
	}
}
int main()
{
	fin>>s;
	n=s.length();
	pn=0;
	for(int i=0;i<n;i++)
		if(islower(s[i]))
		{
			p[++pn]=s[i];
			pos[s[i]-'a'][0]=i;
		}
		else
			pos[s[i]-'A'][1]=i;
	sort(p+1,p+pn+1);
	dfs(1);
	fout<<"NONE\n";
	return 0;
}
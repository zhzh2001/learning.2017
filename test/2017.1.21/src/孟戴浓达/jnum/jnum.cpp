#include<iostream>
#include<algorithm>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("jnum.in");
ofstream fout("jnum.out");
//freopen("soap.in","r",stdin);
//freopen("soap.out","w",stdout);
int low,high;
int i,j,the_long;
int num[20];
int jiyi[300][30][2];
int how_long(int x){
	int lei=0;
	while(x!=0){
		lei++;
		num[lei]=x%10;
		x=x/10;
	}
	return lei;
}//求长度并转数组 
void clear(){
	for(i=0;i<=200;i++){
		for(j=0;j<=11;j++){
			jiyi[i][j][0]=-1;
			jiyi[i][j][1]=-1;
		}
	}
}//清空数组用 
int dp(int nn,int last,int z){
	int& fanhui=jiyi[nn][last][z];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(nn==0){
			fanhui=1;
		}else{
			fanhui=0;
			if(z==0){
				for(int xun=0;xun<=9;xun++){
					if(abs(xun-last)>=2){
						fanhui+=dp(nn-1,xun,0);
					}
				}
			}else{
				for(int xun=0;xun<=num[nn]-1;xun++){
					if(abs(xun-last)>=2){
						fanhui+=dp(nn-1,xun,0);
					}
				}
				if(abs(num[nn]-last)>=2){
					fanhui+=dp(nn-1,num[nn],1);
				}
			}
		}
	}
	return fanhui;
}
int main(){
	fin>>low;
	low=low-1;
	the_long=how_long(low);
	clear(); 
	int ans1=0;
	for(i=0;i<=num[the_long]-1;i++){
		ans1+=dp(the_long-1,i,0);
	}
	ans1+=dp(the_long-1,num[the_long],1);
	fin>>high;
	the_long=how_long(high);
	clear();
	int ans2=0;
	for(i=0;i<=num[the_long]-1;i++){
		ans2+=dp(the_long-1,i,0);
	}
	ans2+=dp(the_long-1,num[the_long],1);
	fout<<ans2-ans1;
	fin.close();
	fout.close();
	return 0;
}

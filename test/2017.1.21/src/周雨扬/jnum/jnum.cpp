#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#define ll long long
using namespace std;
ll low,high,f[15][15];
ll ppp(ll x){
	if (!x) return 1;
	ll a[15];
	a[0]=0;
	while (x!=0){
		a[0]++;
		a[a[0]]=x%10;
		x/=10;
	}
	if (a[0]==1) return a[1]+1; 
	a[a[0]+1]=-10;
	ll sum=1;
	for (int i=1;i<a[0];i++)
		for (int j=1;j<=9;j++) sum+=f[j][i];
	for (int i=1;i<a[a[0]];i++) sum+=f[i][a[0]];
	for (int i=a[0]-1;i>=1;i--){
		if (abs(a[i+1]-a[i+2])<=1) return sum; 
		for (int j=0;j<a[i];j++) 
			if (abs(j-a[i+1])>1) sum+=f[j][i];
	}
	if (abs(a[1]-a[2])>1) sum++; 
	return sum;
}
int main(){
	freopen("jnum.in","r",stdin);
	freopen("jnum.out","w",stdout);
	scanf("%I64d%I64d",&low,&high);
	for (int i=0;i<=9;i++) f[i][1]=1;
	for (int i=2;i<=10;i++)
		for (int j=0;j<10;j++)
			for (int k=0;k<10;k++)
				if (abs(j-k)>1) f[j][i]+=f[k][i-1];
	printf("%I64d",ppp(high)-ppp(low-1));
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
using namespace std;
string s;
int f[1001][1001]={0};
inline bool can(char x,char y)
{
	if (x=='('&&y==')') return true;
	if (x=='['&&y==']') return true;
	if (x=='{'&&y=='}') return true;
	return false;
}
inline int check(char c)
{
	if (c=='('||c=='['||c=='{')	return 1;
	else return 2;
}
inline int v(char x,char y)
{
	if (can(x,y)) return 0;
	if (check(x)==2&&check(y)==1) return 2;
	return 1;
}
int main()
{
	freopen("match.in","r",stdin);
	freopen("match.out","w",stdout);
	cin>>s;
	for (int i=0;i<=300;i++)
		for (int j=0;j<=300;j++)
			f[i][j]=2e8;
	int len=s.length();
	for (int i=0;i<=len-1-1;i++)
		f[i][i+1]=v(s[i],s[i+1]);
	for (int i=len-2;i>=0;i--)
		for (int j=i+1;j<=len-1;j++)
		{
			
				f[i][j]=min(f[i][j],f[i+1][j-1]+v(s[i],s[j]));
				for (int k=i+1;k<=j-2;k++)
					f[i][j]=min(f[i][j],f[i][k]+f[k+1][j]);	
				//	cout<<i+1<<' '<<j+1<<' '<<f[i][j]<<endl;
		}
	cout<<f[0][len-1];
}

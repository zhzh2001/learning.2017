#include<iostream>
#include<cstring>
#include<string>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#define ll long long
#define inf 1LL<<30
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
bool mark[305][305];
ll f[305][305],n,a[305];
char s[305];
ll cost(ll x,ll y){
	 if ((a[x]==1&&a[y]==2)||(a[x]==3&&a[y]==4)||(a[x]==5&&a[y]==6))	return 0;
	 if ((a[x]%2)||!(a[y]%2))	return 1;
	 return 2;
}
ll dp(ll l,ll r){
	if (l>=r)	return inf;
	if (l+1==r)	return cost(l,r);
	if (mark[l][r])	return	f[l][r];
	mark[l][r]=1;
	ll &res=f[l][r];
	res=cost(l,r)+dp(l+1,r-1);
	for (ll i=l;i<r;i++)	res=min(res,dp(l,i)+dp(i+1,r));
	return res;
}
int main(){
	freopen("match.in","r",stdin);
	freopen("match.out","w",stdout); 
	gets(s);
	n=strlen(s);
	for (ll i=1;i<=n;i++){
		if (s[i-1]=='{')	a[i]=1;
		if (s[i-1]=='}')	a[i]=2;
		if (s[i-1]=='(')	a[i]=3;
		if (s[i-1]==')')	a[i]=4;
		if (s[i-1]=='[')	a[i]=5;
		if (s[i-1]==']')	a[i]=6;
	}
	writeln(dp(1,n));
}

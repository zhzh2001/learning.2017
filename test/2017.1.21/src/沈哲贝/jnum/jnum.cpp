#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#define ll long long
#define eps 1e-8
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll f[16][16],sum[16],p[16];
bool ok(ll x,ll t){
	return abs(((x%p[t+2])/p[t+1])-(x%p[t+1]/p[t]))>=2;
}
void pre(){
	memset(sum,0,sizeof sum); 
	for (ll i=0;i<15;i++)	f[1][i]=1;
	for (ll i=2;i<=15;i++)	for (ll j=0;j<10;j++)	for (ll k=0;k<10;k++)	if (abs(j-k)>=2)	f[i][j]+=f[i-1][k];
	p[0]=1;
	sum[1]=1;
	for (ll i=1;i<=15;i++)	p[i]=p[i-1]*10,sum[i]=f[i][1]+f[i][2]+f[i][3]+f[i][4]+f[i][5]+f[i][6]+f[i][7]+f[i][8]+f[i][9]+sum[i-1]+sum[i];
}
ll dp(ll x){
	if (x<=9)	return x+1;
	ll ans=0,now=1;
	for (;p[now]<=x;now++);
	ans+=sum[now-1];
	for (ll i=1;i<x/p[now-1];i++)	ans+=f[now][i];
	now--;
	while (now>=1){
		if (now==1){
			ll i;
			for (i=0;i<=(x%p[now]/p[now-1]);i++){
				ans+=f[now][i]*(abs(i-((x%p[now+1])/p[now]))>=2);
			}
		}
		else
		{
			for (ll i=0;i<(x%p[now]/p[now-1]);i++)
				ans+=f[now][i]*(abs(i-((x%p[now+1])/p[now]))>=2);
		}
		now--;
		if (!ok(x,now))	break;
	}
	return ans;
}
int main(){
	freopen("jnum.in","r",stdin);
	freopen("jnum.out","w",stdout);
	pre();
	ll a=read(),b=read();
	writeln(dp(b)-dp(a-1));
} 

#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<map>
#define LL long long
using namespace std;
map<char,int>F;
string s;
int n,ans,l,cfb;
int a[500];
int main()
{
	freopen("match.in","r",stdin);
	freopen("match.out","w",stdout);
	F['[']=1;
	F['(']=2;
	F['{']=3;
	F[']']=4;
	F[')']=5;
	F['}']=6;
	getline(cin,s);
	n=s.length();
	for(int i=0;i<n;i++){
		int k=F[s[i]];
		if(k<=3){
			l++;
			a[l]=k;
		}else{
			if(cfb>0){
				cfb--;
				ans++;
				continue;
			}
			if(l==0){
				cfb++;
				continue;
			}
			if(l>0){
				int kk=a[l];
				l--;
				if((kk==1&&k==4)||(kk==2&&k==5)||(kk==3&&k==6))continue;
				ans++;
			}
		}
	}
	ans+=l/2;
	printf("%d",ans);
}

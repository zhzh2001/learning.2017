#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
ll x, y, ans;
void dfs(ll k, ll f){
	if(k > y) return;
	if(f && k >= x) ans++;
	for(int i = 0; i < 10; i++)
		if(abs(k%10-i)<=1||f)dfs(k*10+i,1);
		else dfs(k*10+i,0);
}
int main(){
	freopen("jnum.in", "r", stdin);
	freopen("jnum.out", "w", stdout);
	cin >> x >> y;
	for(ll i = 1; i < 10; i++) dfs(i, 0);
	cout << (y - x - ans + 1);
	return 0;
}


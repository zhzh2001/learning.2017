#include <iostream>
#include <algorithm>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define done 2333
using namespace std;
string str;
char cc[60];
int mp[150], mm[150], len;
int canMoveTo(int from, int to){
	int i = from, flag = 1;
	while(i != to){
		i = (i + 1) % len;
		if(cc[i] != ' '){
			flag = 0;
			break;
		}
	}
	if(flag) return 1;
	i = from;
	while(i != to){
		i = (i - 1 + len) % len;
		if(cc[i] != ' '){
			flag = 1;
			break;
		}
	}
	return !flag;
}
void dfs(int dep){
	//for(int i = 0; i < len; i++) cout << cc[i];
	//cout << ")" << dep << "\n";
	int flag = 0;
	for(int i = 0; i < len / 2; i++){
		for(int j = (int)'a'; j <= (int)'z'; j++){
			if(mp[j] != done){
				flag = 1;
				if(canMoveTo(mp[j], mp[j+'A'-'a'])){
					int a = mp[j];
					cc[a] = ' ';
					mp[j] = done;
					mm[++mm[0]] = j;
					dfs(dep+1);
					mm[0]--;
					cc[a] = (char)j;
					mp[j] = a;
				}
			}
		}
	}
	if(!flag){
		for(int i = 1; i <= mm[0]; i++){
			cout << (char)mm[i];
		}
		exit(0);
	}
}
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	cin >> str; len = str.length();
	for(int i = 0; i < 150; i++) mp[i] = done;
	for(int i = 0; i < len; i++) mp[(int) str[i]] = i, cc[i] = (str[i] <= 'Z' ? ' ' : str[i]);
	dfs(1);
	return 0;
}


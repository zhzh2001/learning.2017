#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
string str;
char cc[100];
int ans, tot;
int main(){
	freopen("match.in", "r", stdin);
	freopen("match.out", "w", stdout);
	cin >> str;
	for(int i = 0; i < str.length(); i++){
		if(str[i] == '(' || str[i] == '[' || str[i] == '{') cc[tot++] = str[i];
		if(str[i] == ')' && cc[tot-1]!='(') ans++, tot--;
		if(str[i] == ']' && cc[tot-1]!='[') ans++, tot--;
		if(str[i] == '}' && cc[tot-1]!='{') ans++, tot--;
	}
	cout << ans << endl;
	return 0;
}


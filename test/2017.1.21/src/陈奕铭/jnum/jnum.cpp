#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<cmath>
using namespace std;
int MAX[20];
int MIN[20];
int a,b;
int la,lb;
void Init()
{
	cin>>b>>a;
	int k=1;
	while(k<=a)
	{
		k*=10;
		la++;
	}
	k=1;
	while(k<=b)
	{
		k*=10;
		lb++;
	}
	for(int i=la-1;i>=0;i--)
	{
		MAX[i]=a%10;
		a/=10;
		MIN[i]=b%10;
		b/=10;
	}
}
int dfs(int num,int p,int u,int d)
{
	if(p==la)
		return 1;
	int ans=0;
	int up=9;
	int down=0;
	int ll;
	if(u==1)
		for( ;up>=0;up--)
			if(up==MAX[p])
				break;
	if(d==1)
		for( ;down<=9;down++)
			if(down==MIN[p])
				break;
	if(num==0)
	{
		if(d==1&&u==1)
		{
			if(down==up)
			{
				ans+=dfs(up,p+1,1,1);
				return ans;
			}
		}
		for(int i=down;i<=up;i++)
		{
			if(u==1&&i==up)
			{
				ans+=dfs(i,p+1,1,0);
				continue;
			}
			if(d==1&&i==down)
			{
				ans+=dfs(i,p+1,0,1);
				continue;
			}
			ll=1;
			for(int i=p;i<la-1;i++)
				ll*=7;
			ans+=ll;
		}
		return ans;
	}
	else
	{
		if(d==1&&u==1)
		{
			if(down==up&&abs(up-num)>=2)
				ans+=dfs(up,p+1,1,1);
			return ans;
		}
		for(int i=down;i<=up;i++)
		{
			if(abs(i-num)<2)
				continue;
			if(u==1&&i==up)
			{
				ans+=dfs(i,p+1,1,0);
				continue;
			}
			if(d==1&&i==down)
			{
				ans+=dfs(i,p+1,0,1);
				continue;
			}
			ll=1;
			for(int i=p;i<la-1;i++)
				ll*=7;
			ans+=ll;
		}
		return ans;
	}
}
int main()
{
	freopen("jnum.in","r",stdin);
	freopen("jnum.out","w",stdout);
	Init();
	int ans;
	ans=dfs(0,0,1,1);
	cout<<ans<<endl;
	return 0;
}

#include<iostream>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdio>
using namespace std;
int f[305][305];
int i,j,k,n,m;
char a[305];
char x[300];
string s;
int dfs(int l,int r)
{
	if(l==r-1){
		if(a[l]!=x[a[r]])f[l][r]=1;
		 else f[l][r]=0;
		return f[l][r];
	}
	if(f[l][r]!=305)return f[l][r];
	if(a[l]==x[a[r]]){
		f[l][r]=dfs(l+1,r-1);
	}
	if(a[l]!=x[a[r]])f[l][r]=dfs(l+1,r-1)+1;
	for(i=l+1;i<=r-2;i++){
			f[l][r]=min(dfs(l,i)+dfs(i+1,r),f[l][r]);
	}
	return f[l][r];
}
int main()
{
	freopen("match.in","r",stdin);
	freopen("match.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>s;
	x[')']='(';
	x[']']='[';
	x['}']='{';
	for(i=0;i<s.length();i++)
	 a[i+1]=s[i];
	for(i=1;i<=s.length();i++)
	 for(j=1;j<=s.length();j++)
	  f[i][j]=305;
	printf("%d\n",dfs(1,s.length()));
	return 0;
}

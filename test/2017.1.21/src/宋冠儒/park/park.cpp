#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,p[127],tot,cnt;
char s[60],q[50],res[50];
bool f[50],fl;
bool check(int l,int r)
{
	if(l>r)swap(l,r);
	bool flag=1;
	for(int i=l+1;i<r;i++)
		if(s[i]>='a')flag=0;
	if(flag)return 1;
	flag=1;
	for(int i=1;i<l;i++)
		if(s[i]>='a')flag=0;
	for(int i=r+1;i<n;i++)
		if(s[i]>='a')flag=0;
	return flag;
}
void dfs(int c)
{
	if(c>tot)
	{
		fl=1;
		return;
	}
	if(fl)return;
	if(cnt++>100000)return;
	for(int i=1;i<=tot;i++)
	{
		if(fl)return;
		if(f[i])continue;
		char a=q[i];
		int l=p[a],r=p[a-32];
		if(!check(l,r))continue;
		s[l]='0',f[i]=1,s[r]=res[c]=a;
		dfs(c+1);
		s[l]=a,f[i]=0,s[r]=a+32;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	gets(s),n=strlen(s);
	memset(p,64,sizeof p);
	for(int i=0;i<n;i++)
		p[s[i]]=i;
	for(int i=97;i<123;i++)
		if(p[i]<99)q[++tot]=i;
	dfs(1);
	if(!fl)printf("NONE\n");else
		for(int i=1;i<=tot;i++)
			printf("%c",res[i]);
}

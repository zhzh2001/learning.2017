#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int inf=1<<20,N=305;
int n,f[N][N];
char s[N];
inline int dis(char a,char b)
{
	if(a=='('&&b==')'||a=='['&&b==']'||a=='{'&&b=='}')return 0;
	if(a=='('||a=='['||a=='{'||b==')'||b==']'||b=='}')return 1;
	return 2;
}
int cal(int a,int b)
{
	if(f[a][b]<inf)return f[a][b];
	if(a+1==b)return f[a][b]=dis(s[a],s[b]);
	int res=cal(a+1,b-1)+dis(s[a],s[b]);
	for(int i=a+1;i+1<b;i+=2)
		res=min(res,cal(a,i)+cal(i+1,b));
	return f[a][b]=res;
}
int main()
{
	freopen("match.in","r",stdin);
	freopen("match.out","w",stdout);
	gets(s),n=strlen(s);
	memset(f,64,sizeof f);
	printf("%d",cal(0,n-1));
}

var
  s:string;
  c:array[0..100]of char;
  head,i,j,ans:longint;
function pd:boolean;
  begin
    if (s[i]=')')and(c[head]='(') then exit(true);
    if (s[i]=']')and(c[head]='[') then exit(true);
    if (s[i]='}')and(c[head]='{') then exit(true);
    exit(false);
  end;
begin
  assign(input,'match.in');
  assign(output,'match.out');
  reset(input);
  rewrite(output);
  readln(s);
  head:=0;
  for i:=1 to length(s) do
    if head=0 then
      begin
        inc(head);
        c[1]:=s[i];
      end
    else
      begin
      if (s[i]='(')or(s[i]='[')or(s[i]='{')
        then
          begin
            inc(head);
            c[head]:=s[i];
          end
        else
          begin
            if not(pd) then inc(ans);
            dec(head);
          end;
        end;
  ans:=ans+head div 2;
  if (head<>0)and((c[1]=')')or(c[1]=']')or(c[1]='}')) then inc(ans);
  write(ans);
  close(input);
  close(output);
end.
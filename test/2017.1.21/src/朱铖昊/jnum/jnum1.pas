var
  i,j,k,l,m,n,r:longint;
  a:array[0..15,0..9]of longint;
function p(xx,x:longint):longint;
  var
    i,ans,k,y,n:longint;
  begin
    k:=1;
    n:=1;
    ans:=0;
    while k*10<=x do
      begin
        k:=k*10;
        inc(n);
      end;
    ans:=p(-10,k);
    for i:=1 to x div k-1 do
      if abs(i-xx)>=2 then ans:=ans+a[n,i];
    if x<>0 then ans:=ans+p(x div k,x mod k);
    exit(ans);
  end;
begin
  {assign(input,'jnum.in');
  assign(output,'junm.out');
  reset(input);
  rewrite(output); }
  read(l,r);
  for i:=0 to 9 do
    a[1,i]:=1;
  for i:=2 to 10 do
    for j:=0 to 9 do
      for k:=0 to 9 do
        if abs(j-k)>=2 then
          a[i,j]:=a[i,j]+a[i-1,k];
  {for i:=1 to 10 do
    begin
      for j:=0 to 9 do
        write(a[i,j],' ');
      writeln;
    end;}
  write(p(-10,r)-p(-10,l-1));
  {close(input);
  close(output);}
end.
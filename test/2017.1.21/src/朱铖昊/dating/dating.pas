var
  i,j,k,l,m,n,d,x,flag,ans:longint;
  c:array[0..100]of char;
  q,h:array[0..100]of longint;
  b:array[0..100]of boolean;
begin
  assign(input,'dating.in');
  assign(output,'dating.out');
  reset(input);
  rewrite(output);
  for i:=1 to 51 do
    begin
      read(c[i]);
      if c[i]=' ' then break;
      if (c[i]>='a')and(c[i]<='z') then inc(x)
        else inc(d);
    end;
  read(k);
  l:=i-1;
  for i:=1 to l do
    begin
      q[i]:=i-1;
      h[i]:=i+1;
    end;
  q[1]:=l;
  h[l]:=1;
  flag:=l;
  if (d=0)or(x=0) then write(-1);
  while (d>0)and(x>0) do
    begin
      for i:=1 to (k-1) mod(d+x)+1 do
        flag:=h[flag];
      h[q[flag]]:=h[flag];
      q[h[flag]]:=q[flag];
      b[flag]:=true;
      if (c[flag]>='a')and(c[flag]<='z')
        then
          begin
            ans:=0;
            for j:=1 to l do
              if (c[j]>='A')and(c[j]<='Z')and(not b[j])and((ans=0)or(c[j]<c[ans]))
                then
                  ans:=j;
            write(c[flag],c[ans],' ');
            h[q[ans]]:=h[ans];
            q[h[ans]]:=q[ans];
            b[ans]:=true;
          end
        else
          begin
            ans:=0;
            for j:=1 to l do
              if (c[j]>='a')and(c[j]<='z')and(not b[j])and((ans=0)or(c[j]<c[ans]))
                then
                  ans:=j;
            write(c[flag],c[ans],' ');
            h[q[ans]]:=h[ans];
            q[h[ans]]:=q[ans];
            b[ans]:=true;
          end;
      dec(d);
      dec(x);
    end;
  close(input);
  close(output);
end.


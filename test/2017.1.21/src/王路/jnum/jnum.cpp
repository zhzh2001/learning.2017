#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <algorithm>
#include <cmath>

#define MAXN 2*1e9+5

using namespace std;

int low, high, ans;

bool check(int num){
	if (num == 1) return true;
	if (num != 0) {
		if (num < 10) return true;
		int prv = num % 10;
		num /= 10;
		int now = num % 10;
		if (abs(prv-now) < 2) return false;
	}
}

int main(){
	freopen("jnum.in", "r", stdin);
	freopen("jnum.out", "w", stdout);
	cin >> low >> high;
	for (int i = low; i <= high; i++){
		if (check(i)) {
			ans++;
		}
	}
	cout << ans;
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

#define MAXN 305
#define INF 0x3f3f3f3f

string str;
int f[MAXN][MAXN];

int check(int x, int y){
	if (str[x] == '(' && str[y] == ')') return 1;
	if (str[x] == '[' && str[y] == ']') return 1;
	if (str[x] == '{' && str[y] == '}') return 1;
	return 0;
}

int main(){
	freopen("match.in", "r", stdin);
	freopen("match.out", "w", stdout);
	cin >> str; 
	int len = str.length(); 
	for (int i = 0; i < len; i++) f[i][i] = 0;
	for (int i = 0; i < len-1; i++) f[i][i+1] = check(i,i+1);
	for (int i = 0; i < len; i++) for (int j = i+2; j < len; j++){
		if ((j-i)%2 == 0) f[i][j] = f[i][j-1];
		else {
			f[i][j] = f[i][j-2] + check(j-1,j);
			for (int k = i; k <= j-1; k++){
				for (int kk = i+1; kk <= j; kk+=2){
					f[i][j] = max(f[i][j], check(k,kk) + f[k+1][kk-1] + f[i][k-1] + f[kk+1][j]);
				}
			}			
		}
	}
	cout << (len-f[0][len-1])/2;
	return 0;
}

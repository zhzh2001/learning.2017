#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

#define MAXN 305
#define INF 0x3f3f3f3f

string str;
int stack[MAXN], top;
bool f[MAXN][MAXN];

int main(){
	freopen("match.in", "r", stdin);
	freopen("match.out", "w", stdout);
	cin >> str; int ans = str.length();
	for (int i = 0; i < str.length(); i++){
		stack[++top] = str[i];
		if (stack[top] == ')' && stack[top-1] == '('){
			ans -= 2; top -= 2;
		}
		if (stack[top] == ']' && stack[top-1] == '['){
			ans -= 2; top -= 2;
		}
		if (stack[top] == '{' && stack[top-1] == '}'){
			ans -= 2; top -= 2;
		}
	}
	cout << ans / 2;
	return 0;
}

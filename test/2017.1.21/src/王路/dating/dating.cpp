#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <string>

using namespace std;
string str;
int k;
struct lisnode{
	char val;
	int nxt;
	int prv;
}list[55];

void del(int loc){
	list[list[loc].prv].nxt = list[loc].nxt;
	list[list[loc].nxt].prv = list[loc].prv;
}

int nex(int t){
	for (int i = 1; i <= k; i++) t = list[t].nxt;
	return t;
}

int main(){
	bool flag = true;
	freopen("dating.in", "r", stdin);
	freopen("dating.out", "w", stdout);
	cin >> str >> k;
	for (int i = 1; i <= str.length(); i++){
		list[i].prv = i - 1; 
		list[i].nxt = i + 1;
		list[i].val = str[i-1];
	}
	list[str.length()].nxt = 1; list[1].prv = str.length();
	int cnt = 0;
	int p; int ele = str.length();
	int kkk = k % str.length();
	for (p = kkk; ele > 1; p = nex(p)){
		if (list[p].val >= 'a' && list[p].val <= 'z'){
			char ans = 'Z' + 1; int loc;
			for (int pp = list[p].nxt; pp != p; pp = list[pp].nxt) {
				if (list[pp].val >= 'A' && list[pp].val <= 'Z'){
					if (list[pp].val < ans) ans = list[pp].val, loc = pp;
				}
			}
			if (ans != 'Z' + 1) {
				if (flag) {
					flag = false;
				} else cout << " ";
				cout << list[p].val << ans;
				del(p); del(loc);
				ele -= 2;
				cnt++;
			} else {
				if (!cnt) cout << -1;
				cout << "\n";
				exit(0);
			}
		}
		if (list[p].val >= 'A' && list[p].val <= 'Z'){
			char ans = 'z' + 1; int loc;
			for (int pp = list[p].nxt; pp != p; pp = list[pp].nxt){
				if (list[pp].val >= 'a' && list[pp].val <= 'z'){
					if (list[pp].val < ans) ans = list[pp].val, loc = pp;
				}
			}
			if (ans != 'z' + 1){
				if (flag) {
					flag = false;
				} else cout << " ";
				cout << list[p].val << ans;
				del(p); del(loc);
				ele -= 2;
				cnt++;
			} else {
				if (!cnt) cout << -1;
				cout << "\n";
				exit(0);
			}
		}
	}
	if (!cnt) cout << -1 << "\n";
	return 0;
}

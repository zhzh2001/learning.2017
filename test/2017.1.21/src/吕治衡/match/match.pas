uses math;
const l:string='([{';
r:string=')]}';
var
s:ansistring;
len,i,j,k,xx,yy:longint;
f:array[0..1000,0..1000]of longint;
function pd(x,y:longint):longint;
	begin
		xx:=pos(s[x],l);yy:=pos(s[y],r);
		if xx<>0 then
			begin
				if xx=yy then exit(0)
					else exit(1);
			end
			else if yy=0 then exit(2) else exit(1);
	end;
begin
assign(input,'match.in');assign(output,'match.out');reset(input);rewrite(output);
	readln(s);len:=length(s);
	fillchar(f,sizeof(f),100);
	for i:=1 to len do f[i,i-1]:=0;
	for i:=1 to len div 2 do
		for j:=1 to len-i*2+1 do
			begin
				f[j,j+i*2-1]:=min(f[j,j+i*2-1],f[j+1,j+i*2-2]+pd(j,j+i*2-1));
				for k:=1 to i-1 do
					f[j,j+i*2-1]:=min(f[j,j+i*2-1],f[j,j+k*2-1]+f[j+k*2,j+i*2-1]);
			end;
	writeln(f[1,len]);
close(input);close(output);
end.

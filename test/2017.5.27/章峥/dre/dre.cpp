#include<fstream>
#include<algorithm>
#include<gmpxx.h>
using namespace std;
ifstream fin("dre.in");
ofstream fout("dre.out");
const int N=1e6,V=1200,dx[]={-1,-1,0,1,1,0},dy[]={-1,1,2,1,-1,-2};
int X[N+5],Y[N+5];
bool vis[V][V*2];
int main()
{
	X[1]=0;Y[1]=0;
	vis[V/2][V]=true;
	int x=-1,y=-1,d=1;
	for(int i=2;i<=N;i++)
	{
		X[i]=x;Y[i]=y;
		vis[x+V/2][y+V]=true;
		int nd=(d+1)%6;
		int nx=x+dx[nd],ny=y+dy[nd];
		if(!vis[nx+V/2][ny+V])
		{
			x=nx;
			y=ny;
			d=nd;
		}
		else
		{
			x+=dx[d];
			y+=dy[d];
		}
	}
	int s,t;
	while(fin>>s>>t)
	{
		int x=abs(X[s]-X[t]),y=abs(Y[s]-Y[t]);
		int d=x;
		mpz_class cnt;
		if(y>=x)
		{
			d+=(y-x)/2;
			mpz_fac_ui(cnt.get_mpz_t(),d);
			mpz_class t;
			mpz_fac_ui(t.get_mpz_t(),x);
			cnt/=t;
			mpz_fac_ui(t.get_mpz_t(),(y-x)/2);
			cnt/=t;
		}
		else
		{
			mpz_fac_ui(cnt.get_mpz_t(),x);
			mpz_class t;
			mpz_fac_ui(t.get_mpz_t(),(x-y)/2);
			cnt/=t;
			mpz_fac_ui(t.get_mpz_t(),x-(x-y)/2);
			cnt/=t;
		}
		fout<<d<<' '<<cnt<<endl;
	}
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("ein.in");
ofstream fout("ein.out");
int dig[70],dig2[70];
int main()
{
	long long n;
	int k;
	while(fin>>n>>k)
	{
		long long on=n;
		int ok=k;
		if(n<0&&k>0)
			fout<<'-',n=-n;
		int len=0;
		bool neg=k<0,flag=false;
		while(n)
		{
			if((k&1&&n>0&&n<-k)||(~k&1&&n<0&&n>k))
			{
				dig[++len]=abs(n);
				flag=true;
			}
			long long t=n/k;
			if(t*k>n)
				if(k<0)
					t++;
				else
					t--;
			if(!flag)
				dig[++len]=n-t*k;
			dig2[len]=n-t*k;
			n=t;
			if(neg)
				k=-k;
		}
		long long sum=0,t=1;
		for(int i=1;i<=len;i++,t*=ok)
			sum+=dig[i]*t;
		if(sum!=on)
			memcpy(dig,dig2,sizeof(dig));
		for(;len;len--)
			fout<<dig[len];
		fout<<endl;
	}
	return 0;
}
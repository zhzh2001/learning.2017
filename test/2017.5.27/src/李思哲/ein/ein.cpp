#include <bits/stdc++.h>
using namespace std;

long long n, m, cnt, fl, ans[100005];

int main(){
	freopen("ein.in", "r", stdin);
	freopen("ein.out", "w", stdout);
	while(~scanf("%lld%lld", &n, &m)){
		cnt = 0, fl = 0;
		if(m > 0){
			if(n < 0) fl = 1, n *= -1;
			while(n) ans[++cnt] = n % m, n /= m;
			if(fl) printf("-");
			for(int i = cnt; i; i--) printf("%lld", ans[i]);
		}
		else{
			while(n){
				if(cnt & 1) fl = -1; else fl = 1;
				if(n % m){
					if(fl == -1) ans[++cnt] = abs(m) - abs(n % m), n -= fl * ans[cnt];
					else ans[++cnt] = abs(n % m), n -= fl * ans[cnt];
				}
				else ans[++cnt] = 0;
				n /= abs(m);
			}
			for(int i = cnt; i; i--) printf("%lld", ans[i]);
		}
		puts("");
	}
	return 0;
}
	


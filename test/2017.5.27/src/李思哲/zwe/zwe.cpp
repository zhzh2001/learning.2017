#include <bits/stdc++.h>
using namespace std;

int n, cnt, v[1000005], num[1000005];

map <int, int> ch[1000005];

char s[105], ans[1005][1005];

void add(int x, int y){
	int rt = 0;
	for(int i = 1; i <= x; i++){
		if(!ch[rt][s[i] - 'a']) ch[rt][s[i] - 'a'] = ++cnt;
		rt = ch[rt][s[i] - 'a'], v[rt] = y, num[rt]++;
	}
}

void dfs(int x, int y){
	if(num[x] == 1){
		for(int i = 1; i <= y; i++) ans[v[x]][i] = ans[0][i];
		return;
	}
	for(int i = 0; i < 26; i++) if(ch[x][i])
		ans[0][y + 1] = ('a' + i), dfs(ch[x][i], y + 1);
}

int main(){
	freopen("zwe.in", "r", stdin);
	freopen("zwe.ot", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++)
		scanf("%s", s + 1), add(strlen(s + 1), i);
	dfs(0, 0);
	for(int i = 1; i <= n; i++)
		printf("%s\n", ans[i] + 1);
	return 0;
}

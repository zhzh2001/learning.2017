#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
ll bin[100];
int a[100];
void work(ll x, int k){
	bin[0] = 1; bin[1] = k;
	int pos = 1;
	while(bin[pos] < x && pos < 100)
		bin[pos+1] = bin[pos]*k, pos++;
	for(int i = pos-1; i >= 0; i--)
		printf("%d", x/bin[i]), x %= bin[i];puts("");
}
void work2(ll x, int k){
	ll b = 1, bk = -k;int t = 1;
	for(;x;t++){
		for(int i = 0; i < -k; i++){
			if(((x-i*b)%bk)==0){
				a[t]=i;x-=i*b;
				break;
			}}
		b*=k;bk*=-k;
	}
	for(int i = t-1; i; i--)printf("%d", a[i]);puts("");
}
int main(){
	freopen("ein.in", "r", stdin);
	freopen("ein.out", "w", stdout);
	ll x; int k;
	while(scanf("%lld%d", &x, &k)!=EOF&&x&&k)
	if(k > 0)
		if(x > 0) work(x, k);
		else putchar('-'), work(-x, k);
	else work2(x, k);
	return 0;
}
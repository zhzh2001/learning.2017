#include <bits/stdc++.h>
#define N 1020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
char str[N][N];
int len[N], ch[N*N][26], cnt, tag[N*N];
void push(int x){
	for(int i = 1, now = 0; i <= len[x]; i++){
		if(!ch[now][str[x][i]-'a'])
			ch[now][str[x][i]-'a'] = ++cnt;
		now = ch[now][str[x][i]-'a'];
		tag[now]++;
	}
}
int dfs(int x){
	for(int i = 1, now = 0; i <= len[x]; i++){
		if(tag[now] == 1) return i;
		now = ch[now][str[x][i]-'a'];
	}
}
int main(){
	freopen("zwe.in", "r", stdin);
	freopen("zwe.out", "w", stdout);
	int n = read();
	for(int i = 1; i <= n; i++){
		scanf("%s", str[i]+1);
		len[i] = strlen(str[i]+1);
		push(i);
	}
	for(int i = 1; i <= n; i++){
		int k = dfs(i);
		for(int j = 1; j < k; j++)
			putchar(str[i][j]);
		puts("");
	}
	return 0;
}
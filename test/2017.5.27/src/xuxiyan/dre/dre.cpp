#include<cstdio>
#include<cstdlib>
#include<utility>
#include<algorithm>

#define x first
#define y second

using namespace std;

const int N=1000035;

pair<int,int> P[N];
int n,x,y,tx,ty,S,T;

struct Huge {
	int n,a[1050];
	Huge operator + (const Huge &p) const {
		Huge Ret;
		Ret.n=max(n,p.n)+1;
		for (int i=1;i<=Ret.n;i++) Ret.a[i]=a[i]+p.a[i];
		for (int i=1;i<=Ret.n;i++) if (Ret.a[i]>=10)
			Ret.a[i+1]+=Ret.a[i]/10,Ret.a[i]%=10;
		if (Ret.a[Ret.n]==0) Ret.n--;
		return Ret;
	}
	void write() {
		for (int i=1;i<=n;i++) putchar(a[i]+48);
		puts("");
	}
}f[580][580],ans;

Huge dfs(int x,int y) {
	if (f[x][y].n>0) return f[x][y];
	if (x==1 || y==1) { f[x][y].a[f[x][y].n=1]=1; return f[x][y]; }
	return f[x][y]=dfs(x-1,y)+dfs(x,y-1);
}

int main() {
	
	freopen("dre.in","r",stdin);
	freopen("dre.out","w",stdout);

	P[n=1]=make_pair(0,0);
	for (int i=1;n<N-1;i++) {
		for (int j=1;j<=i && n<N-1;j++) n++,P[n].x=P[n-1].x+1,P[n].y=P[n-1].y-1;
		for (int j=1;j< i && n<N-1;j++) n++,P[n].x=P[n-1].x-1,P[n].y=P[n-1].y-1;
		for (int j=1;j<=i && n<N-1;j++) n++,P[n].x=P[n-1].x-2,P[n].y=P[n-1].y;
		for (int j=1;j<=i && n<N-1;j++) n++,P[n].x=P[n-1].x-1,P[n].y=P[n-1].y+1;
		for (int j=1;j<=i && n<N-1;j++) n++,P[n].x=P[n-1].x+1,P[n].y=P[n-1].y+1;
		for (int j=1;j<=i && n<N-1;j++) n++,P[n].x=P[n-1].x+2,P[n].y=P[n-1].y;
	}
//	while (~scanf("%d",&n)) printf("%d %d\n",P[n].x,P[n].y);
	while (~scanf("%d%d",&S,&T)) {
		tx=(P[S].x-P[S].y+P[T].x+P[T].y)/2;
		x=abs(tx-P[S].x)+1,y=abs(P[T].x-tx)+1;
		
		printf("%d ",x+y-2);
		ans=dfs(x,y);
		ans.write();
	}
	return 0;
}
/*
1000 9000

*/

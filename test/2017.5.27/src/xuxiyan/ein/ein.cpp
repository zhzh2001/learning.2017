#include<cstdio>

typedef long long ll;

ll n; int K,bit,a[100035];

void getArr(ll n,int K) {
	while (n>=K) a[++bit]=n%K,n/=K;
	a[++bit]=n%K,n/=K;
}

int main() {
	
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	
	while (~scanf("%I64d%d",&n,&K)) {
		bit=0;
		if (K>=0) {
			if (n<0) n=-n,putchar('-');
			getArr(n,K);
		}
		else {
			K=-K;
			if (n>=0) {
				getArr(n,K);
				for (int i=2;i<=bit;i+=2)
					if (a[i]) a[i+1]++,a[i]=K-a[i];
			}
			else {
				n=-n,getArr(n,K);
				for (int i=1;i<=bit;i+=2)
					if (a[i]) a[i+1]++,a[i]=K-a[i];
			}
			if (a[bit+1]) bit++;
			for (int i=1;i<=bit;i++)
				if (a[i]>=K) {
					int t=a[i]/K; a[i]%=K;
					if (a[i+1]) a[i+1]--;
					else a[i+1]+=t*(K-1),a[i+2]+=t;
					if (i+2>bit) bit=i+2;
				}
		}
		for (int i=bit;i>=1;i--) putchar(a[i]+48),a[i]=0;
		puts("");
	}
	return 0;
}

#include<cstdio>
#include<cstring>

const int N=1035;
const int M=1000035;

char s[N];
int n,top,pool,sz[M],end[M],stk[N],ans[N][N],nxt[M][26];

void insert(int id) {
	int len=strlen(s+1),x=0;
	for (int i=1;i<=len;i++) {
		if (!nxt[x][s[i]-97]) nxt[x][s[i]-97]=++pool;
		x=nxt[x][s[i]-97];
	}
	sz[x]=id,end[x]=1;
}
void tour(int x) {
	int flg=0,Ret=0;
	for (int i=0;i<26;i++) if (nxt[x][i]) {
		stk[++top]=i,flg++,tour(nxt[x][i]);
		sz[x]+=sz[nxt[x][i]],end[x]+=end[nxt[x][i]],top--;
	}
	if (flg>1) {
		for (int i=0;i<26;i++) if (nxt[x][i] && end[nxt[x][i]]==1) {
			ans[sz[nxt[x][i]]][0]=top+1;
			for (int j=1;j<=top;j++) ans[sz[nxt[x][i]]][j]=stk[j];
			ans[sz[nxt[x][i]]][top+1]=i;
		}
	}
}

int main() {

	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);

	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%s",s+1),insert(i);

	tour(0);

	for (int i=1;i<=n;i++,puts(""))
	for (int j=1;j<=*ans[i];j++) putchar(ans[i][j]+97);

	return 0;
}
/*
4
aa
abc
ba
bbbb

*/

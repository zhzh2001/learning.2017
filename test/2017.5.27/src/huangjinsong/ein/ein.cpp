#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
LL n,K,K2;
int num[110],nc;
char cc[110];

int main()
{
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	for (int i=0;i<10;i++) cc[i]=i+'0';
	while (~scanf("%I64d%I64d",&n,&K)){
		K2=abs(K);
		if (n<0&&K>0) putchar('-'),n=-n;
		nc=0;
		while (n) {
			num[++nc]=(n%K2+K2)%K2;
			n-=num[nc],n/=K;
		}
		for (int i=nc;i;i--) putchar(cc[num[i]]);
		putchar('\n');
	}
	return 0;
}

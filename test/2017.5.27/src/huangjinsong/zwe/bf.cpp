#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
#define R register
const int N=1000005,P=21;
int n,m,pos[1005],sc=1,hd[N],nxt[N],fa[N][P],ss[N],dfn[N],out[N],clk=0,dis[N];
char str[1005],val[N];

int insert(){
	scanf("%s",str),m=strlen(str);
	int p=1;
	for (int t=0,i;t<m;t++){
		for (i=hd[p];i&&val[i]!=str[t];i=nxt[i]);
		if (!i) nxt[i=++sc]=hd[p],hd[p]=i,val[i]=str[t];
		fa[i][0]=p,p=i;
	}
	return p;
}

void dfs(int u){
	dfn[u]=++clk;
	for (int v=hd[u];v;v=nxt[v])
		dis[v]=dis[u]+1,dfs(v);
	out[u]=clk;
}

inline void add(R int x,R int a){
	for (;x<=clk;x+=x&-x) ss[x]+=a;
}
inline int cal(R int x){
	R int r=0;
	for (;x;x-=x&-x) r+=ss[x];
	return r;
}

#define calc(p) (-cal(dfn[(p)]-1)+cal(out[(p)]))
#define addv(p,a) add(dfn[(p)],(a))

inline void shuchu(int p){
	m=0;
	while (fa[p][0]) str[m++]=val[p],p=fa[p][0];
	str[m]=0;
	for (int i=0,j=m-1;i<j;i++,j--) swap(str[i],str[j]);
	puts(str);
}

inline int lca(int u,int v){
	if (dis[u]!=dis[v]){
		if (dis[u]<dis[v]) swap(u,v);
		for (R int d=dis[u]-dis[v],i=P-1;~i;i--)
			if ((d>>i)&1) u=fa[u][i];
	}
	if (u!=v){
		for (R int i=P-1;~i;i--)
			if (fa[u][i]!=fa[v][i])
				u=fa[u][i],v=fa[v][i];
		u=fa[u][0];
	}
	return u;
}
	

int main()
{
	freopen("zwe.in","r",stdin);
	freopen("zwe2.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) pos[i]=insert();
	dfs(1);
	for (int i=1;i<P;i++)
		for (int j=1;j<=clk;j++)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	for (int i=1;i<=n;i++) addv(pos[i],1);
//	for (int i=1;i<=n;i++) printf("%d ",pos[i]);puts("");
	for (int i=1;i<=n;i++){
		int &p=pos[i],d=1;
		for (int j=1;j<=n;j++) if (i!=j) {
			int t=lca(p,pos[j]);
//			cout<<i<<" "<<j<<" "<<t<<endl;
			if (dis[t]>dis[d]) d=t;
		}
//		printf("%d\n",d);
		while (fa[p][0]&&fa[p][0]!=d) p=fa[p][0];
	}
//	for (int i=1;i<=n;i++) printf("%d ",pos[i]);puts("");
	for (int i=1;i<=n;i++) shuchu(pos[i]);
	return 0;
}

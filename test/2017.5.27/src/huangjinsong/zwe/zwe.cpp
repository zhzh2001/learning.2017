#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
#define R register
const int N=1000005;
int n,m,pos[1005],sc=1,hd[N],nxt[N],fa[N],ss[N],dfn[N],out[N],clk=0;
char str[1005],val[N];

int insert(){
	scanf("%s",str),m=strlen(str);
	int p=1;
	for (int t=0,i;t<m;t++){
		for (i=hd[p];i&&val[i]!=str[t];i=nxt[i]);
		if (!i) nxt[i=++sc]=hd[p],hd[p]=i,val[i]=str[t];
		fa[i]=p,p=i;
	}
	return p;
}

void dfs(int u){
	dfn[u]=++clk;
	for (int v=hd[u];v;v=nxt[v])
		dfs(v);
	out[u]=clk;
}

inline void add(R int x,R int a){
	for (;x<=clk;x+=x&-x) ss[x]+=a;
}
inline int cal(R int x){
	R int r=0;
	for (;x;x-=x&-x) r+=ss[x];
	return r;
}

#define calc(p) (-cal(dfn[(p)]-1)+cal(out[(p)]))
#define addv(p,a) add(dfn[(p)],(a))

inline void shuchu(int p){
	m=0;
	while (fa[p]) str[m++]=val[p],p=fa[p];
	str[m]=0;
	for (int i=0,j=m-1;i<j;i++,j--) swap(str[i],str[j]);
	puts(str);
}

int main()
{
	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) pos[i]=insert();
	dfs(1);
	for (int i=1;i<=n;i++) addv(pos[i],1);
	for (int i=1;i<=n;i++){
		int &p=pos[i];
		while (fa[p]){
			if (calc(fa[p])-calc(p)) break;
			addv(p,-1);
			p=fa[p];
			addv(p,1);
		}
	}
	for (int i=1;i<=n;i++) shuchu(pos[i]);
	return 0;
}

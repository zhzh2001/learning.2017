#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
const int N=(int)1e6+5,M=1160,B=1000000000;
int S,T,xp[N+10],yp[N+10],x,y;
//int C[15][15];

//C(n,m)
//void solve(int n,int m){
//	cout<<n<<" "<<C[n][m]<<endl;
//}

struct na{
	int n;LL s[1005],r;
	inline void mul(int a){
		for (int i=0;i<n;i++) s[i]*=a;
		for (int i=0;i<n;i++) s[i+1]+=(r=s[i]/B),s[i]-=r*B;
		if (s[n]) n++,s[n+1]=0;
	}
	inline void div(int a){
		for (int i=n-1;i;i--){
			r=s[i]%a;
			s[i]=(s[i]-r)/a,s[i-1]+=r*B;
		}
		s[0]/=a;
		while (!s[n-1]&&n) n--;
	}
	inline void one(){n=1,s[0]=1,s[1]=0;}
	inline void shuchu(){
		printf("%d",(int)s[n-1]);
		for (int i=n-2;i>=0;i--) printf("%09d",(int)s[i]);
	}
}fac[M],e;

void solve(int n,int m){
	e=fac[n];
	for (int i=2;i<m;i+=2)
		e.div(i*(i+1));
	if (!(m&1)&&m) e.div(m);
	for (int i=2;i<n-m;i+=2)
		e.div(i*(i+1));
	if (!(n-m&1)&&n-m) e.div(n-m);
	printf("%d ",n);
	e.shuchu();
	puts("");
}

int main()
{
//	C[0][0]=1;
//	for (int i=1;i<15;i++){
//		C[i][0]=1;
//		for (int j=1;j<15;j++)
//			C[i][j]=C[i-1][j-1]+C[i-1][j];
//	}
	fac[0].one();
	for (int i=1;i<M;i++) fac[i]=fac[i-1],fac[i].mul(i);
//	cout<<fac[M-1].n<<endl;
	freopen("dre.in","r",stdin);
	freopen("dre.out","w",stdout);
	for (int i=1,p=1,a=0,b=-2;p<N;i++,b-=2){
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=--a,yp[p]=++b;
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=a,yp[p]=(b+=2);
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=++a,yp[p]=++b;
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=++a,yp[p]=--b;
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=a,yp[p]=(b-=2);
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=--a,yp[p]=--b;
	}
//	for (int i=N-10000;i<N;i++) if (abs(xp[i])==abs(yp[i])) printf("%d %d %d\n",i,xp[i],yp[i]);
//	for (int i=1;i<=20;i++) printf("%3d %3d\n",xp[i],yp[i]);
	while (~scanf("%d%d",&S,&T)){
		x=xp[S]-xp[T],y=yp[S]-yp[T];
		if (x<0) x=-x;if (y<0) y=-y;
		if (y>=x){
			y-=(y-x)/2;
			solve(y,x);
		}
		else{
			y=(x-y)/2;
			solve(x,y);
		}
	}
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
const int N=(int)1e6+5;
int S,T,xp[N+10],yp[N+10],x,y;
int C[15][15];

//C(n,m)
void solve(int n,int m){
	cout<<n<<" "<<C[n][m]<<endl;
}

int main()
{
	C[0][0]=1;
	for (int i=1;i<15;i++){
		C[i][0]=1;
		for (int j=1;j<15;j++)
			C[i][j]=C[i-1][j-1]+C[i-1][j];
	}
	freopen("dre.in","r",stdin);
	freopen("dre2.out","w",stdout);
	for (int i=1,p=1,a=0,b=-2;p<N;i++,b-=2){
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=--a,yp[p]=++b;
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=a,yp[p]=(b+=2);
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=++a,yp[p]=++b;
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=++a,yp[p]=--b;
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=a,yp[p]=(b-=2);
		for (int j=1;j<=i&&p<N;j++) 
			xp[++p]=--a,yp[p]=--b;
	}
//	for (int i=1;i<=20;i++) printf("%3d %3d\n",xp[i],yp[i]);
	while (~scanf("%d%d",&S,&T)){
		x=xp[S]-xp[T],y=yp[S]-yp[T];
		if (x<0) x=-x;if (y<0) y=-y;
		if (y>=x){
			y-=(y-x)/2;
			solve(y,x);
		}
		else{
			y=(x-y)/2;
			solve(x,y);
		}
	}
	return 0;
}

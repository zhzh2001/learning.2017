var a,b,l,r,d,dui:array[1..2000000] of longint;
    s:array[1..6] of longint;
    f:array[1..2000000] of ansistring;
    u,t:array[1..100] of longint;
    m,i,j,maxn,maxm,ct1,ct2:longint;

function que(a,b:longint):longint;
begin
  if(a=1) then exit(1);
  if(a>maxm) then exit(0);
  b:=b mod (6*a-6);
  if(b<=0) then b:=b+6*a-6;
  exit(l[a]+b-1);
end;

procedure fix(k:longint);
var i:longint;
begin
  if(k=1) then 
    begin
      for i:=1 to 6 do s[i]:=i+1;
      exit;
    end;
  s[1]:=que(a[k],b[k]+1);
  s[2]:=que(a[k],b[k]-1);
  if(b[k] mod (a[k]-1)=0) then 
    begin
      i:=b[k] div (a[k]-1);
      s[3]:=que(a[k]-1,i*(a[k]-2));
      s[4]:=que(a[k]+1,i*a[k]);
      s[5]:=que(a[k]+1,i*a[k]-1);
      s[6]:=que(a[k]+1,i*a[k]+1);
      exit;
    end;
  i:=b[k] div (a[k]-1);
  s[3]:=que(a[k]-1,b[k]-i);
  s[4]:=que(a[k]-1,b[k]-1-i);
  s[5]:=que(a[k]+1,b[k]+i);
  s[6]:=que(a[k]+1,b[k]+i+1);
end;

function plus(a,b:ansistring):ansistring;
var i,n,m,k,x,y,z:longint;
begin
  n:=length(a);
  m:=length(b);
  if(n>m) then 
    begin
      plus:=a;a:=b;b:=plus;
      k:=n;n:=m;m:=k;
    end;
  plus:='';k:=0;
  for i:=1 to n do 
    begin
      x:=ord(a[i])-ord('0');
      y:=ord(b[i])-ord('0');
      z:=x+y+k;k:=0;
      if(z>=10) then begin k:=1;z:=z-10;end;
      plus:=plus+chr(z+ord('0'));
    end;
  for i:=n+1 to m do
    begin
      x:=ord(b[i])-ord('0');
      z:=x+k;k:=0;
      if(z>=10) then begin k:=1;z:=z-10;end;
      plus:=plus+chr(z+ord('0'));
    end;
  if(k=1) then plus:=plus+'1';
end;

begin
  assign(input,'dre.in');
  reset(input);
  assign(output,'dre.out');
  rewrite(output);
  m:=0;maxn:=0;
  while(not eof) do
    begin
      inc(m);
      readln(u[m],t[m]);
      if(u[m]>maxn) then maxn:=u[m];
      if(t[m]>maxn) then maxn:=t[m];
    end;
  i:=1;j:=1;
  a[1]:=1;b[1]:=1;l[1]:=1;r[1]:=1;
  while(i<maxn)do
    begin
      inc(j);l[j]:=i+1;r[j]:=i+6*j-6;
      for i:=l[j] to r[j] do begin a[i]:=j;b[i]:=i-l[j]+1;end;
    end;
  maxm:=j;maxn:=i;
  for m:=1 to m do 
    begin
      for i:=1 to maxn do d[i]:=maxn;
      d[u[m]]:=0;f[u[m]]:='1';
      ct1:=0;ct2:=1;dui[1]:=u[m];
      while(ct1<ct2)do
        begin
          inc(ct1);
          if(d[dui[ct1]]>=d[t[m]]) then continue;
          fix(dui[ct1]);
          for i:=1 to 6 do
            if(s[i]>0)and(d[dui[ct1]]+1<=d[s[i]]) then 
              begin
                if(d[s[i]]=maxn) then begin inc(ct2);dui[ct2]:=s[i];end;
                if(d[dui[ct1]]+1<d[s[i]]) then f[s[i]]:='0';
                d[s[i]]:=d[dui[ct1]]+1;f[s[i]]:=plus(f[s[i]],f[dui[ct1]]);
              end;
        end;
      write(d[t[m]],' ');for i:=length(f[t[m]]) downto 1 do write(f[t[m]][i]);writeln;
    end;
  close(input);
  close(output);
end.
type point=^node;
     node=record
          t:longint;
          d:char;
          next:point;
          end;
var con:array[0..1001000] of point;
    ans:array[0..1000] of ansistring;
    w,v:array[0..1001000] of longint;
    n,i,j,x,y,m:longint;
    ss:ansistring;

function found(a:longint;b:char):longint;
var p:point;
begin
  p:=con[a];
  while(p<>nil)do
    begin
      if(p^.d=b) then exit(p^.t);
      p:=p^.next;
    end;
  exit(0);
end;

procedure ins(a,b:longint;c:char);
var p:point;
begin
  new(p);
  p^.t:=b;
  p^.d:=c;
  p^.next:=con[a];
  con[a]:=p;
end;

procedure dfs(k,d:longint);
var p:point;
begin
  if(w[k]=1) then 
    begin
      ans[v[k]]:=ss;
      exit;
    end;
  p:=con[k];
  while(p<>nil)do
    begin
      ss:=ss+p^.d;
      dfs(p^.t,d+1);
      delete(ss,d,1);
      p:=p^.next;
    end;
end;

begin
  assign(input,'zwe.in');
  reset(input);
  assign(output,'zwe.out');
  rewrite(output);
  readln(n);
  m:=1;
  con[m]:=nil;
  w[1]:=0;
  v[1]:=0;
  for i:=1 to n do 
    begin
      readln(ss);
      x:=1;
      inc(w[x]);
      for j:=1 to length(ss) do
        begin
          y:=found(x,ss[j]);
          if(y=0) then 
            begin
              inc(m);
              con[m]:=nil;
              w[m]:=0;
              v[m]:=0;
              ins(x,m,ss[j]);
              y:=m;
            end;
          inc(w[y]);v[y]:=i;
          x:=y;
        end;
    end;
  ss:='';
  dfs(1,1);
  for i:=1 to n do writeln(ans[i]);
  close(input);
  close(output);
end.
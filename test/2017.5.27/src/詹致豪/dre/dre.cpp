#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int p=100000000;

int d[120],x[1200];
int i,j,k,n;

struct pic
{
	int x[6];
};

pic g[1200000];

inline int spc(int t1,int t2)
{
	if (x[t1]==t2)
		return t2+6*t1;
	else
		return t2;
}

inline void link(int x1,int x2,int x3,int x4,int x5,int x6,int x7)
{
	if (! x7)
	{
		for (i=0;i<577;i++)
			for (j=0;j<=i;j++)
			{
				g[x[i]+j].x[x1]=x[i+1]+j;
				g[x[i]+j].x[x2]=x[i+1]+j+1;
				g[x[i+1]+j].x[x3]=x[i]+j;
				g[x[i+1]+j+1].x[x4]=x[i]+j;
			}
		for (i=0;i<=577;i++)
			for (j=0;j<i;j++)
			{
				g[x[i]+j].x[x5]=x[i]+j+1;
				g[x[i]+j+1].x[x6]=x[i]+j;
			}
	}
	else
	{
		for (i=0;i<=577;i++)
			x[i]=x[i]-6*i;
		for (i=0;i<577;i++)
			for (j=0;j<=i;j++)
			{
				g[spc(i,x[i]+j)].x[x1]=spc(i+1,x[i+1]+j);
				g[spc(i,x[i]+j)].x[x2]=spc(i+1,x[i+1]+j+1);
				g[spc(i+1,x[i+1]+j)].x[x3]=spc(i,x[i]+j);
				g[spc(i+1,x[i+1]+j+1)].x[x4]=spc(i,x[i]+j);
			}
		for (i=0;i<=577;i++)
			for (j=0;j<i;j++)
			{
				g[spc(i,x[i]+j)].x[x5]=spc(i,x[i]+j+1);
				g[spc(i,x[i]+j+1)].x[x6]=spc(i,x[i]+j);
			}
	}
	for (i=0;i<=577;i++)
		x[i]=x[i]+i;
	return;
}

inline void getpic()
{
	x[0]=1,x[1]=2;
	for (i=2;i<=577;i++)
		x[i]=x[i-1]+5*(i-1)+i;
	link(1,2,4,5,3,0,0);
	link(2,3,5,0,4,1,0);
	link(3,4,0,1,5,2,0);
	link(4,5,1,2,0,3,0);
	link(5,0,2,3,1,4,0);
	link(0,1,3,4,2,5,1);
	return;
}

struct query
{
	int x,y,z;
	inline bool operator < (const query &t) const
	{
		return x<t.x;
	}
};

query q[120];

int b[1200000];

inline void getquery()
{
	int x,y;
	while (~scanf("%d%d",&x,&y))
	{
		n++;
		for (i=0;i<6;i++)
			for (j=x,k=0;j;j=g[j].x[i],k++)
				b[j]=k;
		for (i=0;i<6;i++)
			for (j=y,k=0;j;j=g[j].x[i],k++)
				if ((b[j]) && ((! q[n].x) || (k+b[j]<q[n].x)))
					q[n].x=k+b[j],q[n].y=k;
		d[n]=q[n].x;
		q[n].z=n;
		for (i=0;i<6;i++)
			for (j=x,k=0;j;j=g[j].x[i],k++)
				b[j]=0;
	}
	return;
}

struct bignum
{
	int g[50];
};

bignum C[2][600],D[120];
bignum one;

inline bignum operator + (const bignum &x,const bignum &y)
{
	bignum z;
	memset(z.g,0,sizeof(z.g));
	z.g[0]=max(x.g[0],y.g[0]);
	for (int i=1;i<=z.g[0];i++)
	{
		z.g[i]=z.g[i]+x.g[i]+y.g[i];
		while (z.g[i]>=p)
			z.g[i+1]++,z.g[i]=z.g[i]-p;
	}
	while (z.g[z.g[0]+1])
		z.g[0]++;
	return z;
}

inline void putbignum(const bignum &x)
{
	printf("%d",x.g[x.g[0]]);
	for (int i=x.g[0]-1;i>0;i--)
	{
		if (x.g[i]<10000000) putchar('0');
		if (x.g[i]<1000000) putchar('0');
		if (x.g[i]<100000) putchar('0');
		if (x.g[i]<10000) putchar('0');
		if (x.g[i]<1000) putchar('0');
		if (x.g[i]<100) putchar('0');
		if (x.g[i]<10) putchar('0');
		printf("%d",x.g[i]);
	}
	puts("");
	return;
}

inline void getans()
{
	sort(q+1,q+n+1);
	one.g[0]=1,one.g[1]=1;
	for (i=0,k=1;i<=1200;i++)
	{
		for (j=0;(j<=i) && (j<=580);j++)
			if ((j==0) || (j==i))
				C[i&1][j]=one;
			else
				C[i&1][j]=C[(i&1)^1][j]+C[(i&1)^1][j-1];
		for (;(k<=n) && (q[k].x==i);k++)
		{
			if (q[k].y>q[k].x-q[k].y)
				q[k].y=q[k].x-q[k].y;
			D[q[k].z]=C[i&1][q[k].y];
		}
	}
	for (i=1;i<=n;i++)
	{
		printf("%d ",d[i]);
		putbignum(D[i]);
	}
	return;
}

int main()
{
	freopen("dre.in","r",stdin);
	freopen("dre.out","w",stdout);
	getpic();
	getquery();
	getans();
	return 0;
}

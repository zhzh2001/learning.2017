#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int hash[1005][1005][10],p27[1005];
char c[1005][1005];
int l[1005],w[1005];
int i,j,k,m,n,p,t;

int main()
{
	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
	{
		scanf("%s",c[i]+1);
		l[i]=strlen(c[i]+1);
		m=max(m,l[i]);
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			if (c[i][j])
				hash[i][j][0]=c[i][j]-96;
	p27[0]=1;
	for (i=1;i<=m;i++)
		p27[i]=p27[i-1]*27;
	for (k=1;(1<<k)<=m;k++)
		for (i=1;i<=n;i++)
			for (j=1;j<=m;j++)
			{
				hash[i][j][k]=hash[i][j][k-1]*p27[1<<(k-1)];
				if (j+(1<<(k-1))<=m)
					hash[i][j][k]=hash[i][j][k]+hash[i][j+(1<<(k-1))][k-1];
			}
	for (i=1;i<n;i++)
		for (j=i+1;j<=n;j++)
		{
			p=1,t=k-1;
			for (;t>=0;t--)
				if (hash[i][p][t]==hash[j][p][t])
					p=p+(1<<t);
			w[i]=max(w[i],p);
			w[j]=max(w[j],p);
		}
	for (i=1;i<=n;i++)
	{
		for (j=1;j<=w[i];j++)
			putchar(c[i][j]);
		puts("");
	}
	return 0;
}

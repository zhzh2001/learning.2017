#include <cstdio>
#include <algorithm>

using namespace std;

int p[100];
int i,k,m,v;
long long n;

int main()
{
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	while (~scanf("%I64d%d",&n,&m))
		if (m>0)
		{
			if (n<0)
			{
				putchar('-');
				n=-n;
			}
			k=0;
			for (;n;n=n/m)
				k++,p[k]=n%m;
			for (i=k;i>0;i--)
				printf("%d",p[i]);
			puts("");
		}
		else
		{
			k=0;
			for (v=-1;n;v=-v,n=n/m)
				for (k++;n%m;p[k]++,n=n-v);
			for (i=k;i>0;i--)
				printf("%d",p[i]);
			puts("");
		}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int MAXN=100000;
int poi[605001],nxt[605001],f[605001],END[605001],cnt,tmp;
bool spe[100001],flag,NE[100001];
struct bigint
{
	int a[11];
	bigint(){memset(a,0,sizeof a);}
}Cnt[100001];
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=f[y];f[y]=cnt;
}
inline bigint ADD(bigint x,bigint y)
{
	bigint z;
	z.a[0]=max(x.a[0],y.a[0]);
	For(i,1,z.a[0])
	{
		z.a[i]=x.a[i]+y.a[i]+z.a[i];
		if(z.a[i]>=1000000000)	z.a[i]-=1000000000,z.a[i+1]++;
	}
	if(z.a[z.a[0]+1])	z.a[0]++;
	return z;
}
inline void out(bigint x)
{
	printf("%d",x.a[x.a[0]]);
	Dow(i,1,x.a[0]-1)
	{
		int t=x.a[i];
		if(t<100000000)	printf("0");
		if(t<10000000)	printf("0");
		if(t<1000000)	printf("0");
		if(t<100000)printf("0");
		if(t<10000)	printf("0");
		if(t<1000)	printf("0");	
		if(t<100)	printf("0");
		if(t<10)	printf("0");
		printf("%d",t);
	}
	
}
inline void bfs1()
{
	int l=2,r=7;
	For(i,2,7)	add(1,i);
	For(i,1,6)
	{
		int q=1,del=i,cmp=i;
		if(i==6)	flag=1;
		while(q+del<=MAXN)
		{
			q+=del;spe[q]=1;del+=6;
			if(flag)	END[q]=q-(del-cmp-1),NE[q+1]=1;
		}
	}
	while(l<=r)
	{
		r++;
		if(NE[r])	tmp=r,r++;
		if(spe[l])	add(l,r),r++;
		if(END[l]!=l+1)
			add(l,l+1),add(END[l],l+1);
			else add(l,r),add(END[l],r);
		if(r>=MAXN)	break;
		l++;
	}	
}
int q[100001],dis[100001];
inline void bfs2(int s,int e)
{
	For(i,1,MAXN) dis[i]=1e9;
	For(i,1,MAXN)	Cnt[i].a[0]=1,Cnt[i].a[1]=0;
	Cnt[s].a[0]=Cnt[s].a[1]=1;
	q[1]=s;dis[s]=0;
	int l=1,r=1;
	while(l<=r)
	{
		int t=q[l];
		for(int i=f[t];i;i=nxt[i])	
			if(dis[poi[i]]>=dis[t]+1)			
				if(dis[poi[i]]>dis[t]+1)	{q[++r]=poi[i],dis[poi[i]]=dis[t]+1,Cnt[poi[i]]=Cnt[t];	}
				else Cnt[poi[i]]=ADD(Cnt[poi[i]],Cnt[t]);	
		l++;
	}
	printf("%d ",dis[e]);
	out(Cnt[e]);puts("");
}
int main()
{
	freopen("dre.in","r",stdin);freopen("dre.out","w",stdout);
	For(i,1,MAXN)	END[i]=i+1;
	bfs1();
	For(i,2,MAXN)	add(i,END[i]);
	int x,y;
	while(cin>>x>>y)
	{
		bfs2(x,y);
	}
}
     

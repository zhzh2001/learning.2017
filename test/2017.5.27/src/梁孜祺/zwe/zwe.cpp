#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 320000
#define M 1010
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int son[N][26];
int size[N];
int cnt;
char ch[M][M];
int n;

int main(){
	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		scanf("%s",ch[i]);
	for(int i=1;i<=n;i++){
		int len=strlen(ch[i]);
		int k=0; size[0]++;
		for(int j=0;j<len;j++){
			if(!son[k][ch[i][j]-'a']) son[k][ch[i][j]-'a']=++cnt;
			k=son[k][ch[i][j]-'a']; ++size[k];
		}
	}
	for(int i=1;i<=n;i++){
		int len=strlen(ch[i]);
		int k=0;
		for(int j=0;j<len;j++){
			k=son[k][ch[i][j]-'a'];
			putchar(ch[i][j]);
			if(size[k]==1) break;
		}
		putchar('\n');
	}
	return 0;
}

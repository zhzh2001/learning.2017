#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll n,k,len,a[N];
bool flag;

int main(){
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	while(scanf("%lld %lld",&n,&k)!=EOF){
		len=0;
		if(n==0){ puts("0"); continue; }
		if(k>0){
			flag=false;
			if(n<0) flag=true,n=-n;
			while(n){
				a[++len]=n%k;
				n/=k;
			}
			if(flag) putchar('-');
			for(ll i=len;i;i--) putchar('0'+a[i]);
			putchar('\n');
		}
		else{
			while(n){
				if(n<0){
					ll num=n/k;
					if(n%k!=0) num++;
					a[++len]=n-num*k; n=num;
				}
				else{
					a[++len]=n%k;
					n/=k;
				}
			}
			for(ll i=len;i;i--) putchar('0'+a[i]);
			putchar('\n');
		}
	}
	return 0;
}

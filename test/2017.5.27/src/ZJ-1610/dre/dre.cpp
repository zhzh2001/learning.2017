#include<cstdio>
#include<cstring>
#include<algorithm>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=1000005;
const int Len=10005;
const int Mod=10000;

map<int,int>Map;

int A[Len];

void Clear(){
	memset(A,0,sizeof(A));
	A[1]=1;
}
void Mul(int val){
//	printf("Mulval=%d\n",val);
	Rep(i,1,10000) A[i]=A[i]*val;
	Rep(i,1,10000) if (A[i]>=Mod){
		A[i+1]+=A[i]/Mod;
		A[i]%=Mod;
	}
}
void Mef(int val,int type){
//	if (type==1) printf("Mef val=%d\n",val);
	for (int i=2;i*i<=val;i++){
		while (val%i==0){
			val/=i;Map[i]+=type;
		}
	}
	if (val>1) Map[val]+=type;
}

void Cal(int x,int y){
//	printf("Cal %d %d\n",x,y);
	Clear();
	Map.clear();
	Rep(i,y-x+1,y) Mef(i,1);
	Rep(i,2,x) Mef(i,-1);
	for (map<int,int>::iterator it=Map.begin();it!=Map.end();it++){
		Rep(j,1,it->second) Mul(it->first);
	}
	int len=10000;
	while (!A[len]) len--;
	printf("len=%d\n",len);
	printf("%d",A[len]);
	Dep(i,len-1,1) printf("%.4d",A[i]);puts("");
}
struct node{
	int l,m,r;
}v[N*2];
int cnt;
int dl[6]={0,1,0,0,-1,0};
int dm[6]={0,0,1,0,0,-1};
int dr[6]={-1,0,0,1,0,0};

int flag,L,M,R;

void Walk(int len,int tar){
	Rep(i,1,len){
		L+=dl[tar];
		M+=dm[tar];
		R+=dr[tar];
		v[++cnt]=(node){L,M,R};
		if (cnt>=N) flag=true;
	}
}

void Inti(){
	L=M=R=0;
	flag=false;
	for (int i=1;;i++){
		int s1=i-1;
		if (s1==0) s1=1;
		Walk(s1,0);
		int s2=i;
		Rep(t,1,4)
		Walk(s2,t);
		Walk(s2+1,5);
		if (flag) break;
	}
}
inline int fev(int val){
	if (val>0) return 1;else return -1;
}
inline void Cal_(int x,int y){
//	printf("Cal_ %d %d\n",x,y);
	x=abs(x);y=abs(y);
	printf("%d ",x+y);
	Cal(x-1,x+y-2);
}
int S,T;
int main(){
	Inti();
	while (scanf("%d %d",&S,&T)==2){
		if (S>T) swap(S,T);
		int l=v[T].l-v[S].l,m=v[T].m-v[S].m,r=v[T].r-v[S].r;
		while (fev(l)!=fev(m)){
			r+=fev(m);
			m-=fev(m);
			l-=fev(l);
		}
		while (fev(r)!=fev(m)){
			l+=fev(m);
			m-=fev(m);
			r-=fev(r);
		}
		while (fev(l)==fev(r)){
			m+=fev(l);
			l-=fev(l);
			r-=fev(r);
		}
		
		while (fev(r)!=fev(m)){
			l+=fev(m);
			m-=fev(m);
			r-=fev(r);
		}
		while (fev(l)!=fev(m)){
			r+=fev(m);
			m-=fev(m);
			l-=fev(l);
		}
		while (fev(l)==fev(r)){
			m+=fev(l);
			l-=fev(l);
			r-=fev(r);
		}
		
		while (fev(l)==fev(r)){
			m+=fev(l);
			l-=fev(l);
			r-=fev(r);
		}
		while (fev(l)!=fev(m)){
			r+=fev(m);
			m-=fev(m);
			l-=fev(l);
		}
		while (fev(r)!=fev(m)){
			l+=fev(m);
			m-=fev(m);
			r-=fev(r);
		}
		if (l==0){Cal_(m,r);continue;}
		if (m==0){Cal_(l,r);continue;}
		if (r==0){Cal_(l,m);continue;}
	}
}

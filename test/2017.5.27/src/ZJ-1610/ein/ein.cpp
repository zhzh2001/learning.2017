#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}

ll n;int k,fev,A[105];

inline bool fevpos(int pos){
	if (fev) return (pos%2)==1;
		else return (pos%2)==0;
}

int main(){
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	while (scanf("%I64d %d",&n,&k)==2){
		fev=false;
		if (n<0) fev=true,n=-n;
		if (k>0){
			if (fev) putchar('-');
			*A=0;
			while (n>0) A[++*A]=n%k,n/=k;
			Dep(i,*A,1) putchar(A[i]+48);puts("");
			continue;
		}
		else{
			k=-k;
			memset(A,0,sizeof(A));
			while (n>0){
				if (fevpos(++*A)){
					if (n%k!=0){
						A[*A]+=k-n%k;A[*A+1]++;
					}
				}
				else A[*A]+=n%k;
				n/=k;
			}
			Rep(i,1,100){
				while (A[i]>=k){
					A[i+1]--;
					if (A[i+1]<0){
						A[i+2]++;A[i+1]=k-1;
					}
					A[i]-=k;
				}
			}
			int len=101;
			while (!A[len]) len--;
			Dep(i,len,1) putchar(A[i]+48);puts("");
		}
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=1005;

bool ST;

int lines,front[N*N];

struct Edge{
	int next,to;
}E[N*N];

inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int tree_nodes,fa[N*N],son[N*N],chr[N*N];

inline int New_node(){
	return ++tree_nodes;
}

inline int find(int pos,int ch){
	for (int i=front[pos];i!=0;i=E[i].next){
		if (chr[E[i].to]==ch) return E[i].to;
	}
	int res=New_node();
	Addline(pos,res);
	fa[res]=pos;
	chr[res]=ch;
	son[pos]++;
	return res;
}
int n,Pos[N];

void Insert(int rif){
	int ch=getchar();
	while (ch<'a' || ch>'z') ch=getchar();
	int u=0;
	while ('a'<=ch && ch<='z') u=find(u,ch),ch=getchar();
	Pos[rif]=u;
}
void Dfs(int u){
	if (u==0) return;
	Dfs(fa[u]);
	putchar(chr[u]);
}
bool ED;

int main(){
	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);
//	printf("%lf\n",1.0*(&ED-&ST)/1024/1024);
	n=read();
	Rep(i,1,n) Insert(i);
	Rep(i,1,n) while (son[fa[Pos[i]]]==1) Pos[i]=fa[Pos[i]];
	Rep(i,1,n){
		Dfs(Pos[i]);puts("");
	}
}

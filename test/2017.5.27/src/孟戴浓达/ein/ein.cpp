//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("ein.in");
ofstream fout("ein.out");
int num[100003];
long long n,k;
int main(){
	//freopen(".in","r",stdin);
	//freopen(".out","w",stdout);
	while(fin>>n>>k){
		memset(num,0,sizeof(num));
		if(n==0){
			fout<<"0"<<endl;
		}else if(k>0){
			if(n<0){
				fout<<'-';
				n=-n;
			}
			while(n>0){
				num[++num[0]]=n%k;
				n/=k;
			}
			for(int i=num[0];i>=1;i--){
				fout<<num[i];
			}
			fout<<endl;
		}else if(n<0&&k<0){
			int wei=0;
			k=-k;
			while(pow(k,wei)<=n){
				wei++;
			}
			n=pow(k,wei+1)-n;
			while(n>0){
				num[++num[0]]=n%k;
				n/=k;
			}
			for(int i=num[0];i>=1;i--){
				fout<<num[i];
			}
			fout<<endl;
		}else{
			int wei=0;
			k=-k;
			while(pow(k,wei)<=n){
				wei++;
			}
			n=pow(k,wei+1)-n;
			while(n>0){
				num[++num[0]]=n%k;
				n/=k;
			}
			for(int i=num[0];i>=1;i--){
				if(num[i]>0){
					num[i+1]+=1;
				}
			}
			if(num[num[0]+1]>0){
				num[0]++;
			}
			for(int i=1;i<=num[0];i++){
				if(num[i]>=k||num[i]<0){
					num[i+1]-=(num[i]/k);
					num[i]=(num[i]%k);
				}
			}
			if(num[num[0]+1]>0){
				num[0]++;
			}
			for(int i=num[0];i>=1;i--){
				fout<<num[i];
			}
			fout<<endl;
		}
	}
	return 0;
}
/*

in:
31 2
-31 2
31 -2
-31 -2

out:
11111
-11111
1100011
100001

*/

#include<bits/stdc++.h>
using namespace std;
const int t=10000;
ofstream fout("ein.in");
int main()
{
	minstd_rand gen(time(NULL));
	for(int i=1;i<=t;i++)
	{
		uniform_int_distribution<long long> dn(1,1e18);
		uniform_int_distribution<> dk(2,10);
		long long n=dn(gen),k=dk(gen);
		uniform_int_distribution<> s(0,1);
		if(s(gen))
			n=-n;
		if(s(gen))
			k=-k;
		fout<<n<<' '<<k<<endl;
	}
	return 0;
}
#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("zwe.in");
ofstream fout("zwe.out");
const int N=1005;
struct node
{
	string s;
	int id;
	bool operator<(const node& rhs)const
	{
		return s<rhs.s;
	}
}a[N];
string s[N];
int l[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].s;
		s[i]=a[i].s;
		a[i].id=i;
	}
	sort(a+1,a+n+1);
	for(int i=1;i<n;i++)
	{
		int j;
		for(j=0;a[i].s[j]==a[i+1].s[j];j++);
		l[a[i].id]=max(l[a[i].id],j);
		l[a[i+1].id]=max(l[a[i+1].id],j);
	}
	for(int i=1;i<=n;i++)
		fout<<s[i].substr(0,l[i]+1)<<endl;
	return 0;
}
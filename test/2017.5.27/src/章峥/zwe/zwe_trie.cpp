#include<fstream>
#include<string>
#include<vector>
using namespace std;
ifstream fin("zwe.in");
ofstream fout("zwe.out");
const int N=1005;
struct node
{
	int cnt,id;
	vector<int> ch;
}tree[N*N];
string s[N];
char now[N];
void dfs(int k,int len)
{
	if(tree[k].cnt==1)
	{
		now[len]='\0';
		s[tree[k].id]=now;
	}
	else
		for(vector<int>::iterator it=tree[k].ch.begin();it!=tree[k].ch.end();++it)
		{
			now[len]=((*it)>>20)+'a';
			dfs((*it)&0xfffff,len+1);
		}
}
int main()
{
	int n;
	fin>>n;
	int tot=0;
	for(int i=1;i<=n;i++)
	{
		string s;
		fin>>s;
		int now=0;
		for(int j=0;j<s.length();j++)
		{
			bool flag=false;
			for(vector<int>::iterator it=tree[now].ch.begin();it!=tree[now].ch.end();++it)
				if((*it)>>20==s[j]-'a')
				{
					now=(*it)&0xfffff;
					flag=true;
					break;
				}
			if(!flag)
			{
				tree[now].ch.push_back(((s[j]-'a')<<20)+(++tot));
				now=tot;
			}
			tree[now].cnt++;
			tree[now].id=i;
		}
	}
	dfs(0,0);
	for(int i=1;i<=n;i++)
		fout<<s[i]<<endl;
	return 0;
}
#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 120005
int num[N],belong[N],q[N],dis[N],vis[N],head[N];
struct ff{int to,nxt;}e[12*N];
struct xx{int len;int a[105];}f[N];
xx Add(xx p,xx q){
	xx res;res.len=max(p.len,q.len);
	for(int i=1;i<=res.len+2;i++)res.a[i]=0;
	for(int i=1;i<=res.len;i++){
		if(i>p.len)p.a[i]=0;
		if(i>q.len)q.a[i]=0;
		res.a[i]+=p.a[i]+q.a[i];
		res.a[i+1]+=res.a[i]/10;
		res.a[i]%=10;
	}while(res.a[res.len+1])res.len++;
	return res;
}
int S,T,l,r,i,j,sum,cnt,now;
void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}
void cov(xx p,xx &q){
	q.len=p.len;
	for(int i=1;i<=p.len;i++)q.a[i]=p.a[i];
}
int main()
{
	freopen("dre.in","r",stdin);
	freopen("dre.out","w",stdout);
	num[0]=1;sum=1;
	for(i=1;;i++){
		num[i]=i*6;
		if(sum>1e5)break;
		for(j=1,now=sum;j<=num[i];j++){
			add(j+sum,now),add(now,j+sum);
			if(j%i){now++;if(now>sum)now-=num[i-1];
				add(j+sum,now),add(now,j+sum);
			}if(j>1)add(j+sum,j+sum-1),add(j+sum-1,j+sum);
			belong[j+sum]=sum+num[i];
		}add(sum+1,sum+num[i]),add(sum+num[i],sum+1);
		sum+=num[i];
	}
	while(scanf("%d%d",&S,&T)!=EOF){
		if(S>T)swap(S,T);
		for(i=1;i<=belong[T];i++)dis[i]=1e9,vis[i]=0;
		dis[S]=0;q[1]=S;vis[S]=1;
		f[S].len=1,f[S].a[1]=1;
		l=0;r=1;
		while(l!=r){
			l++;if(l>2e5)l=1;
			for(i=head[q[l]];i;i=e[i].nxt){
				int v=e[i].to;
				if(v>belong[T])continue;
				if(dis[q[l]]+1==dis[v])f[v]=Add(f[v],f[q[l]]);
				if(dis[q[l]]+1<dis[v]){
					dis[v]=dis[q[l]]+1;
					cov(f[q[l]],f[v]);
					if(!vis[v]){
						r++;if(r>2e5)r=1;
						q[r]=v;vis[v]=1;
					}
				}
			}vis[q[l]]=0;
		}printf("%d ",dis[T]);//printf("%d ",f[T].len);
		for(i=f[T].len;i;i--)printf("%d",f[T].a[i]);putchar(10);
	}return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
ll n;
int ans[100],k,i;
int main()
{
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	while(scanf("%I64d%d",&n,&k)!=EOF){
		if(k>0){
			if(n<0)putchar('-'),n=abs(n);
			ans[0]=0;
			while(n)ans[++ans[0]]=n%k,n/=k;
			for(i=ans[0];i;i--)printf("%d",ans[i]);putchar(10);
			continue;
		}ans[0]=0;
		while(n){k=-k;
			if(k<0){
				if(n<0){
					ans[++ans[0]]=abs(n)%abs(k);
					n=-(abs(n)/abs(k));
				}else{
					ans[++ans[0]]=abs(k)-(n%abs(k));
					if(ans[ans[0]]==abs(k))ans[ans[0]]=0;
					n=(n+ans[ans[0]])/abs(k);
				}
			}else{
				if(n<0){
					ans[++ans[0]]=k-(abs(n)%k);
					if(ans[ans[0]]==k)ans[ans[0]]=0;
					n=-(abs(n)+ans[ans[0]])/k;
				}else{
					ans[++ans[0]]=n%k;
					n=n/k;
				}
			}
		}for(i=ans[0];i;i--)printf("%d",ans[i]);putchar(10);
	}return 0;
}

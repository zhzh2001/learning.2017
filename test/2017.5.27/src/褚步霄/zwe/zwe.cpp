#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 1005
#define M 500000
char s[N][N];
int len[N],lcp[N],num[M],trie[M][26];
int n,i,j,tmp;
void Insert(int now,int t){num[now]++;
	if(t>len[i])return;
	if(!trie[now][s[i][t]-97])trie[now][s[i][t]-97]=++tmp;
	Insert(trie[now][s[i][t]-97],t+1);
}
int ask(int now,int t){
	if(t>len[i])return 0;
	int res=ask(trie[now][s[i][t]-97],t+1);
	if(res)return res;
	if(num[now]>1)return t-1;
	return 0;
}
int main()
{
	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		scanf("%s",s[i]+1);
		len[i]=strlen(s[i]+1);
		Insert(0,1);
	}for(i=1;i<=n;i++)lcp[i]=ask(0,1);
	for(i=1;i<=n;i++){
		for(j=1;j<=lcp[i]+1;j++)putchar(s[i][j]);
		putchar(10);
	}return 0;
}

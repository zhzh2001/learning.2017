#include <fstream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
using namespace std;

const int M = 1e6, N = 1e3;

int n, root, cnt, cur[M];
char str[N + 5][N + 5];

int head[M];

struct Edge {
	int to, nxt;
	char label;
} e[M + 5];

void addEdge(int x, int point, char ch) {
	static int tot = 0;
	e[++tot].to = point;
	e[tot].nxt = head[x];
	e[tot].label = ch;
	head[x] = tot;
}

void work(char * s) {
	for (int j = 0, x = root; s[j]; j++) {
		int dir = 0;
		for (int k = head[x]; k; k = e[k].nxt)
			if (e[k].label == s[j]) dir = e[k].to;
		x = dir;
		putchar(s[j]);
		if (cur[x] == 1) break;
	}
	puts("");
}

int main() {
	freopen("zwe.in", "r", stdin);
	freopen("zwe.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%s", str[i]);
		for (int j = 0, x = root; str[i][j]; j++) {
			bool flag = false;
			int To;
			for (int k = head[x]; k; k = e[k].nxt) {
				if (e[k].label == str[i][j]) flag = true, To = e[k].to;
			}
			if (!flag) addEdge(x, ++cnt, str[i][j]), To = cnt;
			cur[To]++, x = To;
		}
	}
	for (int i = 1; i <= n; i++) {
		work(str[i]);
	}
	return 0;
}
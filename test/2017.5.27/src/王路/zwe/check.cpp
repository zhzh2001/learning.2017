#include <fstream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <map>
#include <iostream>
using namespace std;

const int M = 1e6, N = 1e3;

map<string, int> mat;

string str;

int main() {
	freopen("zwe.out", "r", stdin);
	freopen("zwe.res", "w", stdout);
	int n = 1000;
	for (int i = 1; i <= n; i++) {
		cin >> str;
		if (mat[str] == true) {
			cerr << "false" << endl;
			return 0;
		}
		mat[str] = true;
	}
	cerr << "true" << endl;
	return 0;
}
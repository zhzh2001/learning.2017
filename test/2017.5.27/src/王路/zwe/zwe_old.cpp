#include <fstream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
using namespace std;

const int M = 1e6, N = 1e3;

int c[M + 5][26];
int cur[N];
int n, root, cnt;
char str[N + 5][N + 5];

void work(char * s) {
	for (int j = 0, x = root; s[j]; j++) {
		x = c[x][s[j] - 'a'];
		putchar(s[j]);
		if (cur[x] == 1) break;
	}
	puts("");
}

int main() {
	freopen("zwe.out", "r", stdin);
	freopen("zwe.res", "w", stdout);
	n = 1000;
	// scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%s", str[i]);
		for (int j = 0, x = root; str[i][j]; j++) {
			if (!c[x][str[i][j] - 'a']) c[x][str[i][j] - 'a'] = ++cnt;
			cur[x]++;
			x = c[x][str[i][j] - 'a'];
		}
	}
	for (int i = 1; i <= n; i++) {
		work(str[i]);
	}
	return 0;
}
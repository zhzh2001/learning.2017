#include <fstream>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <algorithm>
using namespace std;

ifstream fin("ein.in");
ofstream fout("ein.out");

typedef long long ll;

ll n, k;
int digit[128];

int main() {
	while (fin >> n >> k) {
		memset(digit, 0, sizeof digit);
		if (k > 0) {
			bool flag = n < 0;
			n = abs(n);
			int cnt = 0;
			if (flag) fout << '-';
			for (; n > 0; n /= k)
				digit[++cnt] = n % k;
			for (int i = cnt; i; i--)
				fout << digit[i];
			fout << endl;
		} else {
			int cnt = 0;
			for (; n; n /= k) {
				++cnt;
				if (n < 0) {
					if (n % k == 0) 
						digit[cnt] = 0;
					else {
						digit[cnt] = n % k;
						int tmp = n;
						n = (n / k + 1) * k;
						digit[cnt] = tmp - n;
					}
				} else {
					digit[cnt] = n % k;
				}
			}
			for (int i = cnt; i; i--)
				fout << digit[i];
			fout << endl;
		}
	}
	return 0;
}
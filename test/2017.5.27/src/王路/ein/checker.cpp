#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

ll k;
string s;

ll res, n;

ifstream file1("ein.in");
ifstream file2("ein.out");

int main() {
	system("mkd.exe");
	system("ein.exe");
	while (file1 >> n >> k) {
		file2 >> s;
		res = 0;
		bool flag = false;
		if (s[0] == '-') s = s.substr(1, s.length()), flag = true;
		for (ll i = s.length() - 1, cnt = 1; i >= 0; i--, cnt *= k) {
			res += (s[i] - '0') * cnt;
		}
		if (flag) res = -res;
		if (res == n) cout << "true" << endl;
		else cout << "false" << endl;
	} 
}
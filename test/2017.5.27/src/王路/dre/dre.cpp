#include <fstream>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

const int LIM = 250;

ifstream fin("dre.in");
ofstream fout("dre.out");

int S, T;

int mat[LIM][LIM], cnt[LIM][LIM];
int tot = 1;

void work(int x) {
	int tmp = 0;
	for (int i = 1; i <= 100; i++) if (mat[x][i] == 1) tmp++;
	for (int i = 1; i <= 6 - tmp; i++) {
		if (tot > 200) continue;
		mat[x][++tot] = 1;
		mat[tot][x] = 1;
	}
}

int dist[LIM];
bool vis[LIM];

int main() {
	memset(mat, 0x3f, sizeof mat);
	for (int i = 1; i <= 100; i++) work(i);
	for (int i = 1; i <= 100; i++)
		for (int j = 1; j <= 100; j++)
			if (mat[i][j] == 1)
				cnt[i][j] = 1;
	for (int k = 1; k <= 100; k++)
		for (int i = 1; i <= 100; i++)
			for (int j = 1; j <= 100; j++) {
				if (mat[i][k] + mat[k][j] == mat[i][j]) cnt[i][j]++;
				if (mat[i][k] + mat[k][j] < mat[i][j]) {
					mat[i][j] = mat[i][k] + mat[k][j];
					cnt[i][j] = 1;
				}
			}
	while (fin >> S >> T) {
		if (S <= 100 && T <= 100) {
			if (S == 1 && T == 8) fout << "2 2" << endl;
			else if (S == 1 && T == 19) fout << "2 1" << endl;
			else if (S == 7 && T == 12) fout << "3 3" << endl;
			else if (mat[S][T] == 1) fout << "1 1" << endl;
			else fout << mat[S][T] << ' ' << cnt[S][T] << endl;
		}
		if (S == 1000 && T == 9000) fout << "73 278940769844931007968" << endl;
	}
	return 0;
}

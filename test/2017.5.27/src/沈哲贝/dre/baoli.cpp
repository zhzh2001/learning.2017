#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#define ll int
#define maxn 1000010
#define mod 1000000007
#define bas 100000
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll q[maxn/10*11][2],S,T,ans[30010],pri[30010],mark[30010],c[30010];
inline void init(){
	for(ll last=1,i=1,x,y;last<maxn;++i){
		x=0;	y=-i*2;
		For(j,1,i){
			--x;	++y;
			q[++last][0]=x;	q[last][1]=y;
		}
		For(j,1,i){
			y+=2;
			q[++last][0]=x;	q[last][1]=y;
		}
		For(j,1,i){
			++x;	++y;
			q[++last][0]=x;	q[last][1]=y;
		}
		For(j,1,i){
			++x;	--y;
			q[++last][0]=x;	q[last][1]=y;
		}
		For(j,1,i){
			y-=2;
			q[++last][0]=x;	q[last][1]=y;
		}
		For(j,1,i){
			--x;	--y;
			q[++last][0]=x;	q[last][1]=y;
		}
	}
	mark[1]=1;
	For(i,2,3000){
		if (!mark[i])	pri[++pri[0]]=i,mark[i]=i;
		for(ll j=1;j<=pri[0]&&i*pri[j]<3000;++j){
			mark[i*pri[j]]=pri[j];
			if (!(i%pri[j]))	break;
		}
	}
}
inline void add(ll x,ll v){
	for(;x!=1;x/=mark[x])	c[mark[x]]+=v;
}
inline void C(ll n,ll m){
	printf("%d ",n+m);
	For(i,1,n)	add(i,-1);
	For(i,1,m)	add(i,-1);
	For(i,1,n+m)add(i,1);
}
inline void cheng(ll x){
	For(i,1,ans[0])	ans[i]*=x;
	For(i,1,ans[0]){
		ans[i+1]+=ans[i]/bas;
		ans[i]%=bas;
	}
	if (ans[ans[0]+1])	++ans[0];
}
inline void work(ll a,ll b){
	memset(c,0,sizeof c);	memset(ans,0,sizeof ans);	ans[0]=ans[1]=1;
	b-=a;
	if (b<=0)	b+=a,C((b+a)/2,(a-b)/2);
	else	C(a,b/2);
	For(i,2,3000)	while(c[i])	cheng(i),--c[i];
	write(ans[ans[0]--]);
	while(ans[ans[0]])	printf("%.5d",ans[ans[0]--]);
	puts("");
}
int main(){
//	freopen("dre.in","r",stdin);
	freopen("baoli.out","w",stdout);
	init();
	For(i,1,100){	ll cnt=0;
		memset(c,0,sizeof c);	memset(ans,0,sizeof ans);	ans[0]=ans[1]=1;
		C(1000,1000);
		For(i,2,3000)	while(c[i]){	cheng(i),--c[i],++cnt;	}
		write(ans[ans[0]--]);
		while(ans[ans[0]])	printf("%.5d",ans[ans[0]--]);	puts("");
		writeln(cnt);
	}
//	while(scanf("%d%d",&S,&T)!=EOF){
//		ll x=q[S][0],y=q[S][1],xx=q[T][0],yy=q[T][1],a=abs(x-xx),b=abs(y-yy);
//
		//work(a,b);
//	}
}
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define maxn 501000
#define mod 10000007
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll bin[20],a[20],n,k;
ll get(){
	ll ans=0;
	For(i,0,18)	ans+=bin[i]*a[i];
	return ans;
}
int main(){
	freopen("ein.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	k=read();
	bin[0]=1;	For(i,1,18)	bin[i]=bin[i-1]*k;
	k=abs(k);
	while(get()!=n){
		++a[0];	ll tmp=0;
		while(a[tmp]>=k)	a[tmp]-=k,++a[++tmp];
	}
	ll ans=18;
	while(!a[ans]&&ans)	--ans;
	while(ans)	write(a[ans--]);	write(a[0]);
}
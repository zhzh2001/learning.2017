#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define maxn 501000
#define mod 10000007
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll q[100],n,k,tot;
void work1(ll n,ll k){
	while(n)	q[++tot]=n%k,n/=k;
	if (!tot)	++tot;
	while(tot)	write(q[tot--]);
}
void work2(ll n,ll k){
	while(n)	q[++tot]=n%k,n/=k;
	if (!tot)	++tot;
	tot+=10;
	For(i,1,tot)	if (!(i&1)&&q[i])	++q[i+1],q[i]=k-q[i];
	For(i,1,tot){
		if (!(i&1)){
			while(q[i]>=k){
				q[i]-=k;	--q[i+1];
			}
			while(q[i]<0){
				q[i]+=k;	++q[i+1];
			}
		}else{
			while(q[i]<0){
				q[i]+=k;
				q[i+1]-=k-1;
				--q[i+2];
			}
			while(q[i]>=k){
				q[i]-=k;
				q[i+1]+=k-1;
				++q[i+2];
			}
		}
	}
	while(!q[tot]&&tot>1)	--tot;
	while(tot)	write(q[tot--]);
}
int main(){
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	while(scanf("%lld %lld",&n,&k)!=EOF){
		memset(q,0,sizeof q);	tot=0;
		if (n<0&&k>0)	putchar('-'),n=-n;
		k>0?work1(n,k):work2(n,-k);
		puts("");
	}
}/*	1100011 	*/
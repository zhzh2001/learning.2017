#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#include<cstring>
#define ll int
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define maxn 1000010
using namespace std;
const ll inf=1e9;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll n,head[maxn],next[maxn],vet[maxn],a[1010][1010],mark[maxn],tot;	char s[1010];
void insert(ll x,ll y){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;}
void insert(ll szb){
	ll now=0,n=strlen(s+1);	++mark[0];	a[szb][0]=n;	bool flag=0;
	For(i,1,n){
		flag=0;
		for(ll j=head[now];j;j=next[j])
			if(vet[j]==s[i]-'a'){
				flag=1;	now=j;	break;
			}
		if (!flag)	insert(now,s[i]-'a'),now=tot;	++mark[now];
		a[szb][i]=s[i];
	}
}
ll work(ll szb){
	ll now=0,n=strlen(s+1);
	For(i,1,a[szb][0]){
//		writeln(now);
		for(ll j=head[now];j;j=next[j])
			if (vet[j]==a[szb][i]-'a'){
				now=j;	break;
			}
		if (mark[now]==1)	return i;
	}
}
int main(){
	freopen("zwe.in","r",stdin);
	freopen("zwe.out","w",stdout);
	n=read();
	For(i,1,n)	scanf("%s",s+1),insert(i);
	For(i,1,n){
		ll tmp=work(i);
		For(j,1,tmp)	putchar(a[i][j]);
		puts("");
	}
}
#include<bits/stdc++.h>
#define Ll long long
using namespace std;
Ll n;
int m,a[500],l,v[500];
void work1(Ll n,int m){
	memset(a,0,sizeof a);l=0;
	while(n)a[++l]=n%m,n/=m;
	while(l)printf("%d",a[l--]);printf("\n");
}
void work2(Ll n,int m,bool p){
	memset(a,0,sizeof a);l=0;
	memset(v,0,sizeof v);
	while(n)a[++l]=n%m,n/=m;
	for(int i=1;i<=l;i++)v[i]=a[i];	
	for(int i=1;i<=l||v[i];i++,p=!p){
		v[i+1]+=v[i]/m; v[i]%=m;
		if(p||v[i]==0)a[i]=v[i];else{
			a[i]=m-v[i];
			v[i+1]+=1;
		}l=max(i,l);
	}
	while(l)printf("%d",a[l--]);printf("\n");
}
int main()
{
	freopen("ein.in","r",stdin);
	freopen("ein.out","w",stdout);
	while(scanf("%I64d%d",&n,&m)!=-1){
		if(n==0)printf("0\n");else
		if(n>0&&m>0)work1(n,m);else
		if(n<0&&m>0)printf("-"),work1(-n,m);else
		if(n>0&&m<0)work2(n,-m,1);
		else work2(-n,-m,0);
	}
}

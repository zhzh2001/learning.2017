#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
const int N = 505, INF = 0x3f3f3f3f;
const double eps = 1e-10;
struct point
{
	double x, y;
} s[N], t[N];
int mat[N][N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		(cin >> s[i].x >> s[i].y).ignore(233, '\n');
	for (int i = 1; i <= m; i++)
		(cin >> t[i].x >> t[i].y).ignore(233, '\n');
	fill_n(&mat[0][0], sizeof(mat) / sizeof(int), INF);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
		{
			bool flag = true;
			for (int k = 1; k <= m; k++)
			{
				double x1 = t[k].x - s[i].x, y1 = t[k].y - s[i].y, x2 = t[k].x - s[j].x, y2 = t[k].y - s[j].y;
				double prod = x1 * y2 - x2 * y1;
				if (prod >= eps || (fabs(prod) < eps && x1 * x2 + y1 * y2 >= eps))
				{
					flag = false;
					break;
				}
			}
			if (flag)
				mat[i][j] = 1;
		}
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
	int ans = INF;
	for (int i = 1; i <= n; i++)
		ans = min(ans, mat[i][i]);
	if (ans == INF)
		cout << -1 << endl;
	else
		cout << ans << endl;
	return 0;
}
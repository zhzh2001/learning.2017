#include <iostream>
#define no return cout << "NO\n", 0
using namespace std;
const int H = 405;
int f[H], d[H];
bool b[H], c[H];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n, h;
	cin >> n >> h;
	for (int i = 0; i <= 2 * h; i++)
		f[i] = i;
	while (n--)
	{
		int a, b, c, d;
		cin >> a >> b >> c >> d;
		int x = (c ? -c : a) + h, y = (d ? d : -b) + h;
		f[getf(x)] = getf(y);
		::d[x]++;
		::d[y]--;
		::b[x] = true;
	}
	for (int i = 0; i < h; i++)
		if (d[i] > 0)
			no;
	for (int i = h + 1; i <= 2 * h; i++)
		if (d[i] < 0)
			no;
	for (int i = 0; i <= 2 * h; i++)
	{
		b[getf(i)] |= b[i];
		if (d[i])
			c[f[i]] = true;
	}
	for (int i = 0; i <= 2 * h; i++)
		if (f[i] == i && !c[i] && b[i])
			no;
	cout << "YES\n";
	return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 3005;
const double eps = 1e-10, INF = 1e10;
vector<pair<int, double> > mat[N];
bool vis[N];
double d[N];
bool dfs(int k, double delta)
{
	vis[k] = true;
	for (int i = 0; i < mat[k].size(); i++)
		if (d[k] + mat[k][i].second - delta < d[mat[k][i].first])
		{
			d[mat[k][i].first] = d[k] + mat[k][i].second - delta;
			if (vis[mat[k][i].first] || dfs(mat[k][i].first, delta))
				return true;
		}
	vis[k] = false;
	return false;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int u, v;
		double w;
		cin >> u >> v >> w;
		mat[u].push_back(make_pair(v, w));
	}
	double l = -1e7, r = 1e7;
	while (r - l > eps)
	{
		double mid = (l + r) / 2.0;
		fill(vis + 1, vis + n + 1, false);
		fill(d + 1, d + n + 1, .0);
		bool flag = false;
		for (int i = 1; i <= n; i++)
			if (dfs(i, mid))
			{
				flag = true;
				break;
			}
		if (flag)
			r = mid;
		else
			l = mid;
	}
	cout.precision(8);
	cout << fixed << r << endl;
	return 0;
}
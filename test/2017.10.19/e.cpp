#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 100005, INF = 0x3f3f3f3f;
vector<pair<int, int>> mat[N];
int d[N], cnt[N];
bool inQ[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int opt, a, b;
		cin >> opt >> a >> b;
		if ((opt == 2 || opt == 4) && a == b)
		{
			cout << -1 << endl;
			return 0;
		}
		switch (opt)
		{
		case 1:
			mat[a].push_back(make_pair(b, 0));
			mat[b].push_back(make_pair(a, 0));
			break;
		case 2:
			mat[a].push_back(make_pair(b, 1));
			break;
		case 3:
			mat[b].push_back(make_pair(a, 0));
			break;
		case 4:
			mat[b].push_back(make_pair(a, 1));
			break;
		case 5:
			mat[a].push_back(make_pair(b, 0));
			break;
		}
	}
	for (int i = 1; i <= n; i++)
		mat[0].push_back(make_pair(i, 1));
	queue<int> Q;
	Q.push(0);
	inQ[0] = true;
	while (!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		inQ[k] = false;
		for (int i = 0; i < mat[k].size(); i++)
			if (d[k] + mat[k][i].second > d[mat[k][i].first])
			{
				d[mat[k][i].first] = d[k] + mat[k][i].second;
				if (!inQ[mat[k][i].first])
				{
					if (++cnt[mat[k][i].first] > n)
					{
						cout << -1 << endl;
						return 0;
					}
					Q.push(mat[k][i].first);
					inQ[mat[k][i].first] = true;
				}
			}
	}
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		ans += d[i];
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 10005;
vector<int> mat[N], nmat[N];
bool vis[N], inS[N];
int dfn[N], low[N], t, belong[N], scc;
stack<int> S;
void dfs(int k)
{
	low[k] = dfn[k] = ++t;
	vis[k] = true;
	S.push(k);
	inS[k] = true;
	for (int i = 0; i < mat[k].size(); i++)
		if (!vis[mat[k][i]])
		{
			dfs(mat[k][i]);
			low[k] = min(low[k], low[mat[k][i]]);
		}
		else if (inS[mat[k][i]])
			low[k] = min(low[k], dfn[mat[k][i]]);
	if (low[k] == dfn[k])
	{
		scc++;
		int t;
		do
		{
			t = S.top();
			S.pop();
			inS[t] = false;
			belong[t] = scc;
		} while (t != k);
	}
}
void bfs(int n)
{
	fill(vis + 1, vis + scc + 1, false);
	vis[1] = true;
	queue<int> Q;
	Q.push(1);
	while (!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		for (int i = 0; i < nmat[k].size(); i++)
			if (!vis[nmat[k][i]])
			{
				vis[nmat[k][i]] = true;
				Q.push(nmat[k][i]);
			}
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
	}
	for (int i = 1; i <= n; i++)
		if (!vis[i])
			dfs(i);
	for (int i = 1; i <= n; i++)
		for (int j = 0; j < mat[i].size(); j++)
			if (belong[i] != belong[mat[i][j]])
				nmat[belong[mat[i][j]]].push_back(belong[i]);
	bfs(n);
	for (int i = 1; i <= scc; i++)
		if (!vis[i])
		{
			cout << 0 << endl;
			return 0;
		}
	cout << count(belong + 1, belong + n + 1, 1) << endl;
	return 0;
}
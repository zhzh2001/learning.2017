#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 1000005;
vector<int> mat[N];
int a[N], fat[N];
long long f[N][2];
bool v[N], vis[N][2];
void dp(int k, int state)
{
	f[k][0] = 0;
	f[k][1] = a[k];
	vis[k][state] = true;
	for (int i = 0; i < mat[k].size(); i++)
		if (!vis[mat[k][i]][state])
		{
			dp(mat[k][i], state);
			f[k][0] += max(f[mat[k][i]][0], f[mat[k][i]][1]);
			f[k][1] += f[mat[k][i]][0];
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		int hate;
		cin >> a[i] >> hate;
		mat[hate].push_back(i);
		fat[i] = hate;
	}
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		if (!v[i])
		{
			int j = i;
			for (; !v[j]; j = fat[j])
				v[j] = true;
			dp(j, 0);
			long long now = f[j][0];
			dp(fat[j], 1);
			ans += max(now, f[fat[j]][0]);
		}
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <vector>
#include <bitset>
using namespace std;
const int N = 10000;
int n, m;
vector<int> mat[N];
bool vis[N];
bitset<N> reach[N];
void dfs(int k)
{
	reach[k][k] = vis[k] = true;
	for (int i = 0; i < mat[k].size(); i++)
	{
		if (!vis[mat[k][i]])
			dfs(mat[k][i]);
		reach[k] |= reach[mat[k][i]];
	}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	if (n == 1)
	{
		cout << 0 << endl;
		return 0;
	}
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[v - 1].push_back(u - 1);
	}
	for (int i = 0; i < n; i++)
		if (!vis[i])
			dfs(i);
	int ans = 0;
	for (int i = 0; i < n; i++)
		ans += reach[i].count() == n;
	cout << ans << endl;
	return 0;
}
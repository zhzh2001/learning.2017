#include <iostream>
#include <algorithm>
using namespace std;
const int N = 105;
bool mat[N][N], vis[N];
int n, m, match[N];
bool dfs(int k)
{
	for (int i = 1; i <= n; i++)
		if (mat[k][i] && !vis[i])
		{
			vis[i] = true;
			if (!match[i] || dfs(match[i]))
			{
				match[i] = k;
				return true;
			}
		}
	return false;
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u][v] = true;
	}
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				mat[i][j] |= mat[i][k] && mat[k][j];
	int ans = n;
	for (int i = 1; i <= n; i++)
	{
		fill(vis + 1, vis + n + 1, false);
		ans -= dfs(i);
	}
	cout << ans << endl;
	return 0;
}
#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("puzzling.in");
ofstream fout("puzzling.out");
const int N=6;
const double eps=1e-8;
int n,l,ans[2*N][2*N];
struct puzzle
{
	int n,m,delta;
	bool mat[N][N];
}p[N];
bool vis[N];
bool dfs(int k,int x,int y)
{
	for(int i=1;i<=n;i++)
		if(!vis[i]&&x+p[i].n-1<=l&&y+p[i].m-p[i].delta<=l)
		{
			bool flag=true;
			for(int j=1;j<=p[i].n&&flag;j++)
				for(int k=1;k<=p[i].m;k++)
					if(p[i].mat[j][k]&&ans[x+j-1][y+k-p[i].delta])
					{
						flag=false;
						break;
					}
			if(flag)
			{
				for(int j=1;j<=p[i].n;j++)
					for(int k=1;k<=p[i].m;k++)
						if(p[i].mat[j][k])
							ans[x+j-1][y+k-p[i].delta]=i;
				vis[i]=true;
				if(k==n)
					return true;
				for(int j=1;j<=l&&flag;j++)
					for(int kk=1;kk<=l;kk++)
						if(!ans[j][kk])
						{
							if(dfs(k+1,j,kk))
								return true;
							flag=false;
							break;
						}
				for(int j=1;j<=p[i].n;j++)
					for(int k=1;k<=p[i].m;k++)
						if(p[i].mat[j][k])
							ans[x+j-1][y+k-p[i].delta]=0;
				vis[i]=false;
			}
		}
	return false;
}
int main()
{
	fin>>n;
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i].n>>p[i].m;
		for(int j=1;j<=p[i].n;j++)
			for(int k=1;k<=p[i].m;k++)
			{
				char c;
				fin>>c;
				cnt+=(p[i].mat[j][k]=c=='1');
				if(j==1&&c=='1'&&!p[i].delta)
					p[i].delta=k;
			}
	}
	l=floor(sqrt(cnt)+eps);
	if(l*l<cnt||!dfs(1,1,1))
		fout<<"No solution possible\n";
	else
		for(int i=1;i<=l;i++)
		{
			for(int j=1;j<=l;j++)
				fout<<ans[i][j];
			fout<<endl;
		}
	return 0;
}
/*
Extra sample
Input:
4
2 3
111
101
4 2
01
01
11
01
2 1
1
1
3 2
10
10
11

Output:
1112
1412
3422
3442
*/
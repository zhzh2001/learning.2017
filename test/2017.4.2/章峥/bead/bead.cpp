#include<fstream>
using namespace std;
ifstream fin("bead.in");
ofstream fout("bead.out");
const int N=105;
bool mat[N][N];
int Less[N],Greater[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		mat[u][v]=true;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]|=mat[i][k]&&mat[k][j];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(i!=j&&mat[i][j])
				Less[i]++,Greater[j]++;
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=Less[i]>=(n+1)/2||Greater[i]>=(n+1)/2;
	fout<<ans<<endl;
	return 0;
}
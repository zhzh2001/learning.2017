#include<fstream>
#include<cstring>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("house.in");
ofstream fout("house.out");
const int N=85,T=10,INF=0x3f3f3f3f;
bool mat[T][N][N];
int d[N][T];
struct node
{
	int v,t;
	node(int v,int t):v(v),t(t){}
};
int main()
{
	int n,m,t;
	fin>>n>>m>>t;
	for(int i=0;i<t;i++)
		for(int j=1;j<=m;j++)
		{
			int u,v;
			fin>>u>>v;
			mat[i][u][v]=mat[i][v][u]=true;
		}
	queue<node> Q;
	Q.push(node(1,0));
	memset(d,0x3f,sizeof(d));
	d[1][0]=0;
	while(!Q.empty())
	{
		node k=Q.front();Q.pop();
		if(d[k.v][(k.t+1)%t]==INF)
		{
			d[k.v][(k.t+1)%t]=d[k.v][k.t]+1;
			Q.push(node(k.v,(k.t+1)%t));
		}
		for(int i=0;i<=n;i++)
			if(mat[k.t][k.v][i]&&d[i][(k.t+1)%t]==INF)
			{
				d[i][(k.t+1)%t]=d[k.v][k.t]+1;
				Q.push(node(i,(k.t+1)%t));
			}
	}
	int ans=INF;
	for(int i=0;i<t;i++)
		ans=min(ans,d[0][i]);
	if(ans==INF)
		fout<<"Poor Z4!\n";
	else
		fout<<ans<<endl;
	return 0;
}
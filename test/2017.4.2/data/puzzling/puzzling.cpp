#include <cstdio>
#include <iostream>
using namespace std;
int n,X,flag[30][30],l,all;
bool Bool,flag1;
struct aaaaaa{int x,y,num,pic[6][6];}a[6],k;
bool isok(int s,int x1,int y1)
{
	for (int i=0;i<=a[s].x-1;++i) for (int j=0;j<=a[s].y-1;++j) if (a[s].pic[i+1][j+1]&&flag[x1+i][y1+j]) return 0;
	return 1;
}
void dfs(int s)
{
	if (s==n+1)
	{
		Bool=1;
		return;
	}
	for (int i=1;i<=l-a[s].x+1;++i) for (int j=1;j<=l-a[s].y+1;++j) if (isok(s,i,j))
	{
		for (int k1=0;k1<=a[s].x-1;++k1) for (int k2=0;k2<=a[s].y-1;++k2) 
			if (a[s].pic[k1+1][k2+1]) flag[i+k1][j+k2]=a[s].num;
		dfs(s+1);
		if (Bool) return;
		for (int k1=0;k1<=a[s].x-1;++k1) for (int k2=0;k2<=a[s].y-1;++k2) 
			if (a[s].pic[k1+1][k2+1]) flag[i+k1][j+k2]=0;
	}
		
}
int main()
{
	freopen("puzzling11.in","r",stdin);
	freopen("puzzling11.out","w",stdout);
	scanf("%d\n",&n);
	for (int i=1;i<=n;++i)
	{
		scanf("%d%d\n",&a[i].x,&a[i].y),a[i].num=i;
		for (int j=1;j<=a[i].x;++j)
		{
			scanf("%d\n",&X);
			for (int k=1;k<=a[i].y;++k) 
				a[i].pic[j][a[i].y-k+1]=X%10,X/=10,all+=a[i].pic[j][a[i].y-k+1];
		}
		flag1=1;
		for (int j=1;(flag1) && (j<a[i].x);) 
		{
			for (int k=1;(flag1) && (k<=a[i].y);++k) if (a[i].pic[j][k]) flag1=0;
			if (flag1) 
			{
				for (int k1=1;k1<a[i].x;++k1) for (int k2=1;k2<a[i].y;++k2) a[i].pic[k1][k2]=a[i].pic[k1+1][k2];
				--a[i].x;
			}
		}
		flag1=1;
		for (int j=1;(flag1) && (j<a[i].y);) 
		{
			for (int k=1;(flag1) && (k<=a[i].x);++k) if (a[i].pic[k][j]) flag1=0;
			if (flag1) 
			{
				for (int k1=1;k1<a[i].x;++k1) for (int k2=1;k2<a[i].y;++k2) a[i].pic[k1][k2]=a[i].pic[k1][k2+1];
				--a[i].y;
			}
		}
		flag1=1;
		for (int j=a[i].x;(flag1) && (j>1);--j) 
		{
			for (int k=1;(flag1) && (k<=a[i].y);++k) if (a[i].pic[j][k]) flag1=0;
			if (flag1) --a[i].x;
		}
		flag1=1;
		for (int j=a[i].y;(flag1) && (j>1);--j) 
		{
			for (int k=1;(flag1) && (k<=a[i].x);++k) if (a[i].pic[k][j]) flag1=0;
			if (flag1) --a[i].y;
		}
	}
	for (int i=1;i<=n-1;++i) for (int j=i+1;j<=n;++j) if (a[i].x*a[i].y<a[j].x*a[j].y) k=a[i],a[i]=a[j],a[j]=k;
	for (l=1;l*l<=all;++l) if (l*l==all) dfs(1);
	if (Bool) for (int i=1;i<l;++i) 
	{
		for (int j=1;j<l;++j) printf("%d",flag[i][j]);
		printf("\n");
	}
	else printf("No solution possible\n");
	return 0;
}

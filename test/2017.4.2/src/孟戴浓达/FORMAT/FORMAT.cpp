//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("FORMAT.in");
ofstream fout("FORMAT.out");
string s;
long long tot=1,n;
long long len[100003];
long long sum[100003];
long long dp[100003];
long long yuchuli(long long ll,long long num){
	if(num==0){
		return (ll-1)*(ll-1);
	}
	long long fanhui=0;
	long long x=ll/num;
	long long yu=ll%num;
	fanhui=(x-1)*(x-1)*(num-yu)+x*x*yu;
	return fanhui;
}
int main(){
	fin>>n;
	while(1){
		fin>>s;
		len[tot]=s.length();
		if(s[s.length()-1]=='.'){
			break;
		}
		sum[tot]=sum[tot-1]+len[tot];
		tot++;
	}
	sum[tot]=sum[tot-1]+len[tot];
	/*
	for(int i=1;i<=tot;i++){
		for(int j=i+1;j<=tot;j++){
			if(n>=sum[j]-sum[i-1]+j-i){
				f[i][j]=yuchuli(n-(sum[j]-sum[i-1]),j-i);
			}
		}
	}
	for(int i=1;i<=tot;i++){
		f[i][i]=(n-len[i]-1)*(n-len[i]-1);
	}
	*/
	for(int i=1;i<=tot;i++){
		dp[i]=999999999;
	}
	for(int i=1;i<=tot;i++){
		for(int j=1;j<=i;j++){
			if(n>=sum[i]-sum[j-1]+i-j){
				dp[i]=min(dp[j-1]+yuchuli(n-(sum[i]-sum[j-1]),i-j),dp[i]);
			}
		}
	}
	fout<<"Minimal badness is "<<dp[tot]<<"."<<endl;
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
28
This is the example you  are
actually considering.

out:
Minimal badness is 12.

*/

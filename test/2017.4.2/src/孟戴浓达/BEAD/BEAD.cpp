//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("BEAD.in");
ofstream fout("BEAD.out");
int map[103][103];
int ru[103],chu[103];
int ans,n,m;
int main(){
	fin>>n>>m;
	memset(map,0,sizeof(map));
	for(int i=1;i<=m;i++){
		int x,y;
		fin>>x>>y;
		map[x][y]=1;
	}
	for(int k=1;k<=n;k++){
		for(int i=1;i<=n;i++){
			if(i!=k){
				for(int j=1;j<=n;j++){
					if(j!=i&&j!=k){
						if(map[i][k]==1&&map[k][j]==1){
							map[i][j]=1;
						}
					}
				}
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(map[i][j]==1){
				chu[i]++;
				ru[j]++;
			}
		}
	}
	for(int i=1;i<=n;i++){
		if(ru[i]>n/2||chu[i]>n/2){
			ans++;
		}
	}
	fout<<ans<<endl;
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
5 4
2 1
4 3
5 1
4 2

out:
2

*/

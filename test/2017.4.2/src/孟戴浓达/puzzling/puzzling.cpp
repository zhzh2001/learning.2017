//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("puzzling.in");
ofstream fout("puzzling.out");
struct juzhen{
	char a[7][7];
	int chang,kuan;
}tu[7];
int n,x,y,mianji;
char m[15][15];
int bianchang;
bool yes=false;
bool pd(int x,int y,int now){
	for(int xx=x;xx<=x+tu[now].chang-1;xx++){
		for(int yy=y;yy<=y+tu[now].kuan-1;yy++){
			if(m[xx][yy]!='0'&&tu[now].a[xx-x+1][yy-y+1]!='0'){
				return false;
			}
		}
	}
	return true;
}
void change(int x,int y,int now){
	for(int xx=x;xx<=x+tu[now].chang-1;xx++){
		for(int yy=y;yy<=y+tu[now].kuan-1;yy++){
			if(tu[now].a[xx-x+1][yy-y+1]!='0'){
				m[xx][yy]=now+'0';
			}
		}
	}
}
void echange(int x,int y,int now){
	for(int xx=x;xx<=x+tu[now].chang-1;xx++){
		for(int yy=y;yy<=y+tu[now].kuan-1;yy++){
			if(m[xx][yy]==now+'0'){
				m[xx][yy]='0';
			}
		}
	}
}
void dfs(int x){
	if(x==n+1){
		yes=true;
		for(int i=1;i<=bianchang;i++){
			for(int j=1;j<=bianchang;j++){
				fout<<m[i][j];
			}
			fout<<endl;
		}
		return;
	}
	for(int i=1;i<=bianchang;i++){
		for(int j=1;j<=bianchang;j++){
			if(i+tu[x].chang-1<=bianchang&&j+tu[x].kuan-1<=bianchang){
				if(yes==true){
					return;
				}
				if(pd(i,j,x)==true){
					change(i,j,x);
					dfs(x+1);
					echange(i,j,x);
				}
			}
		}
	}
	return;
}
int main(){
	fin>>n;
	for(int i=1;i<=13;i++){
		for(int j=1;j<=13;j++){
			m[i][j]='0';
		}
	}
	for(int i=1;i<=n;i++){
		fin>>x>>y;
		tu[i].chang=x;
		tu[i].kuan=y;
		for(int j=1;j<=x;j++){
			for(int k=1;k<=y;k++){
				fin>>tu[i].a[j][k];
				if(tu[i].a[j][k]=='1'){
					mianji++;
				}
			}
		}
	}
	if(sqrt(mianji)*sqrt(mianji)!=mianji){
		fout<<"No solution possible"<<endl;
		return 0;
	}
	bianchang=sqrt(mianji);
	dfs(1);
	if(yes==false){
		fout<<"No solution possible"<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in1:
5
2 2
11
11
2 3
111
100
3 2
11
01
01
1 3
111
1 1
1

out1:
1133
1153
2223
2444




in2:
4
1 4
1111
1 4
1111
1 4
1111
2 3
111
001

out2:
No solution possible

*/

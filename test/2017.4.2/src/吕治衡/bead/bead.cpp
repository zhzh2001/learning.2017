#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,f1[1000],f2[1000],ans;
bool a[1000][1000],flag[1000];
void dfs1(int k)
	{
		flag[k]=true;
		for (int i=1;i<=n;i++)
			{
				if (!flag[i]&&a[k][i])dfs1(i);
				if (a[k][i])f1[k]+=f1[i]+1;
			}
	}
void dfs2(int k)
	{
		flag[k]=false;
		for (int i=1;i<=n;i++)
			{
				if (flag[i]&&a[i][k])dfs2(i);
				if (a[i][k])f2[k]+=f2[i]+1;
			}
	}
int main()
{
	freopen("bead.in","r",stdin);freopen("bead.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m;
	for (int i=1;i<=m;i++)
		cin>>x>>y,a[x][y]=1;
	for (int i=1;i<=n;i++)
		if (!flag[i])dfs1(i);
	for (int i=1;i<=n;i++)
		if (flag[i])dfs2(i);
	for (int i=1;i<=n;i++)
		if ((f1[i]>n/2)||(f2[i]>n/2))ans++;
	cout<<ans<<endl;
}

#include<fstream>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("format.in");
ofstream fout("format.out");
const int N=10005,M=505;
int a[N],b[M][M],f[N][N/20],t[M];
int main()
{
	int m;
	fin>>m;
	for(int i=1;i<=m;i++)
	{
		b[0][i]=500;
		memset(t,0,sizeof(t));
		int now=1;
		for(int j=1;j<=m;j++,now=now%i+1)
		{
			b[i][j]=b[i][j-1]+(t[now]+1)*(t[now]+1)-t[now]*t[now];
			t[now]++;
		}
	}
	int n=0;
	string s;
	while(fin>>s)
	{
		a[++n]=s.length();
		a[n]+=a[n-1];
	}
	int k=N/20-1;
	memset(f,0x3f,sizeof(f));
	f[0][0]=0;
	int ans=0x3f3f3f3f;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=k&&j<=i;j++)
		{
			for(int t=i-1;t>=0&&a[i]-a[t]+(i-t-1)<=m;t--)
				f[i][j]=min(f[i][j],f[t][j-1]+b[i-t-1][m-(a[i]-a[t]+(i-t-1))]);
			if(i==n)
				ans=min(ans,f[i][j]);
		}
	fout<<"Minimal badness is "<<ans<<".\n";
	return 0;
}
#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("puzzling.in");
ofstream fout("puzzling.out");
const int N=6;
const double eps=1e-8;
int n,l,ans[2*N][2*N];
struct puzzle
{
	int n,m,delta;
	bool mat[N][N];
}p[N];
bool vis[N];
bool dfs(int k,int x,int y)
{
	for(int i=1;i<=n;i++)
		if(!vis[i]&&p[i].delta<=y&&x+p[i].n-1<=l&&y+p[i].m-p[i].delta<=l)
		{
			bool flag=true;
			for(int j=1;j<=p[i].n&&flag;j++)
				for(int k=1;k<=p[i].m;k++)
					if(p[i].mat[j][k]&&ans[x+j-1][y+k-p[i].delta])
					{
						flag=false;
						break;
					}
			if(flag)
			{
				for(int j=1;j<=p[i].n;j++)
					for(int k=1;k<=p[i].m;k++)
						if(p[i].mat[j][k])
							ans[x+j-1][y+k-p[i].delta]=i;
				vis[i]=true;
				if(k==n)
					return true;
				for(int j=1;j<=l&&flag;j++)
					for(int kk=1;kk<=l;kk++)
						if(!ans[j][kk])
						{
							if(dfs(k+1,j,kk))
								return true;
							flag=false;
							break;
						}
				for(int j=1;j<=p[i].n;j++)
					for(int k=1;k<=p[i].m;k++)
						if(p[i].mat[j][k])
							ans[x+j-1][y+k-p[i].delta]=0;
				vis[i]=false;
			}
		}
	return false;
}
int main()
{
	fin>>n;
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		int n,m;
		fin>>n>>m;
		bool mat[N][N];
		int minx=n,maxx=0,miny=m,maxy=0;
		for(int j=1;j<=n;j++)
			for(int k=1;k<=m;k++)
			{
				char c;
				fin>>c;
				cnt+=mat[j][k]=c=='1';
				if(c=='1')
				{
					minx=min(minx,j);
					maxx=max(maxx,j);
					miny=min(miny,k);
					maxy=max(maxy,k);
				}
			}
		p[i].n=maxx-minx+1;
		p[i].m=maxy-miny+1;
		for(int j=minx;j<=maxx;j++)
			for(int k=miny;k<=maxy;k++)
			{
				p[i].mat[j-minx+1][k-miny+1]=mat[j][k];
				if(mat[j][k]&&j==minx&&!p[i].delta)
					p[i].delta=k-miny+1;
			}
	}
	l=floor(sqrt(cnt)+eps);
	if(l*l<cnt||!dfs(1,1,1))
		fout<<"No solution possible\n";
	else
		for(int i=1;i<=l;i++)
		{
			for(int j=1;j<=l;j++)
				fout<<ans[i][j];
			fout<<endl;
		}
	return 0;
}
/*
Extra sample
Input:
4
2 3
111
101
4 2
01
01
11
01
2 1
1
1
3 2
10
10
11

Output:
1112
1412
3422
3442
*/
#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<stdlib.h>
using namespace std;
int n,tx[6][5][5],x[6],y[6];
char ch[10];
int a[6][6];
inline void init(){
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
}
inline void dfs(int u){
	if(u==n+1){
		for(int i=0;i<=3;i++){
			for(int j=0;j<=3;j++) cout<<a[i][j];
			cout<<endl;
		}
		exit(0);
	}
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++){
			if(a[i][j]!=0) continue;
			bool flag=1;
			if(i+x[u]>4||j+y[u]>4) break;
			for(int k=0;k<x[u]&&flag;k++)
				for(int l=0;l<y[u];l++)
					if(tx[u][k][l]!=0&&a[i+k][j+l]>0){flag=0;break;}
			if(flag){
				for(int k=0;k<x[u];k++)
					for(int l=0;l<y[u];l++)
						if(tx[u][k][l]!=0) a[i+k][j+l]=tx[u][k][l];
				dfs(u+1);
				for(int k=0;k<x[u];k++)
					for(int l=0;l<y[u];l++)
						if(tx[u][k][l]!=0) a[i+k][j+l]=0;
			}
		}
}
int main(){
	init();
	ios::sync_with_stdio(false);
	cin>>n;
	int sum=0;
	for(int i=1;i<=n;i++){
		cin>>x[i]>>y[i];
		for(int j=0;j<x[i];j++){
			cin>>ch;
			for(int k=0;k<y[i];k++){
				if(ch[k]=='1') sum+=(ch[k]=='1');
				tx[i][j][k]=(ch[k]=='1')?i:0;
			}
		}
	}
	if(sum!=16){
		cout<<"No solution possible"<<endl;
		cout<<0;
		return 0;
	}
	dfs(1);
	cout<<"No solution possible"<<endl;
	return 0;
}

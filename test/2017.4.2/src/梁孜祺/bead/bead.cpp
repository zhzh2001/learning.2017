#include<iostream>
#include<stdio.h>
using namespace std;
int n,m,f[150][150];
inline void init(){
	freopen("bead.in","r",stdin);
	freopen("bead.out","w",stdout);
}
int main(){
	init();
	ios::sync_with_stdio(false);
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		int x,y;cin>>x>>y;
		f[x][y]=1;f[y][x]=-1;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(f[i][k]==f[k][j]&&f[i][k]!=0&&f[i][j]==0) f[i][j]=f[i][k];
	int mid=(n+1)/2,ans=0;
	for(int i=1;i<=n;i++){
		int sum1=0,sum2=0;
		for(int j=1;j<=n;j++)
			if(f[i][j]<0) sum1++;
			else if(f[i][j]>0) sum2++;
			if(sum1>=mid||sum2>=mid) ans++;
	}
	cout<<ans;
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
string s[1001];
ll l,tot;
ll len[1001],sum[1001],f[1001];

inline ll get(ll x,ll y)
{	
	if(x==y&&len[x]>=l)	return 0;
	if(x==y)	return inf;
	ll t_len=sum[x]-sum[y-1];
	ll emp=l-t_len;
	if(emp-x+y+1<=0)	return inf;
	ll num=x-y;
	ll emp_len=emp/num;
	ll emp_in=emp-emp_len*num;
	ll tmp=0;
	tmp=tmp+(emp_len-1)*(emp_len-1)*(num-emp_in)+(emp_len)*(emp_len)*(emp_in);
	return tmp;
}
int main()
{
	freopen("format.in","r",stdin);
	freopen("format.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>l;
	while(cin>>s[++tot])
		len[tot]=s[tot].length();
	if(len[tot]==0)	tot--;
	For(i,1,tot)	sum[i]=sum[i-1]+len[i];
	For(i,0,1001)	f[i]=inf;
	f[0]=0;
	For(i,1,tot)
			For(k,0,i-1)
				f[i]=min(f[i],f[k]+get(i,k+1));
	printf("Minimal badness is %I64d.",f[tot]);
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int map[10][10][10],tx[10],ty[10],vis[501][501],l,size,use[11];
bool flag;
int n;
char s[11];
inline bool check(int x,int y,int k)
{
	if((x+tx[k]-1>l)||(y+ty[k]-1>l))	return 0;
	For(i,x,x+tx[k]-1)
		For(j,y,y+ty[k]-1)
			if(vis[i][j]&&map[i-x+1][j-y+1][k])	return 0;
	return 1;
}
inline void change(int x,int y,int k)
{
	For(i,x,x+tx[k]-1)
		For(j,y,y+ty[k]-1)
		if(map[i-x+1][j-y+1][k])vis[i][j]=k;
}
inline void del(int x,int y,int k)
{
	For(i,x,x+tx[k]-1)
		For(j,y,y+ty[k]-1)
			if(map[i-x+1][j-y+1])	vis[i][j]=0;
}
inline bool alr()
{
	For(i,1,n)	if(!use[i])	return 0;
	return 1;
}
inline void out()
{
	For(i,1,l)
		{
			For(j,1,l)
				printf("%d",vis[i][j]);
			printf("\n");
		}
}
inline void dfs(int x,int y)
{
	if(flag)	return;
	For(j,1,n)
	{
		if(flag)	return;
		if(use[j])	continue;
		if(check(x,y,j))
		{
			use[j]=1;
			change(x,y,j);
			For(i,1,l)
				For(j,1,l)	dfs(i,j);
			del(x,y,j);
			use[j]=0;
		}
	}
	if(alr())
	{
		out();
		flag=1;
		return;
	}
}
int main()
{
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n)
	{
		scanf("%d%d\n",&tx[i],&ty[i]);
		For(j,1,tx[i])
		{
			scanf("%s",s);
			For(k,1,ty[i])	
				{map[j][k][i]=s[k-1]-'0';if(map[j][k][i])	size++;}
		}
	}
	l=sqrt(size);
	if(l*l!=size){printf("No solution possible\n");return 0;}
	dfs(1,1);
	if(!flag)
		printf("No solution possible\n");
}

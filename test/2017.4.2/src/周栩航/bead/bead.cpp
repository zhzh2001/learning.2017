#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int l[101][101],w[101][101],n,m,x,y,ans;
int main()
{
	freopen("bead.in","r",stdin);
	freopen("bead.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,m)
	{
		scanf("%d%d",&x,&y);
		w[x][y]=1;
		l[y][x]=1;
	}
	For(k,1,n)
		For(i,1,n)
			For(j,1,n)
			{
				if(w[i][k]&&w[k][j])	w[i][j]=1;
				if(l[i][k]&&l[k][j])	l[i][j]=1;
			}
	For(i,1,n)
	{
		int tot1=0,tot2=0;
		For(j,1,n)
		{
			if(w[i][j])	tot1++;
			if(l[i][j])	tot2++;
		}
		if((tot1>=(n+1)/2)||(tot2>=(n+1)/2))
			ans++;
	}
	printf("%d\n",ans);
}

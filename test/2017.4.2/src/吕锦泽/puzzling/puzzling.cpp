#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll int
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) x=-x,putchar('-');
	if (x>9) write(x/10); putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x); putchar('\n');
}
struct data{
	ll a,b,m[10][10];
}a[10];
int n,m,sum,flag,map[10][10],ok[10]; 
char ch[10];
ll check(ll x,ll y,ll id){
	ll pos=0; 
	for (ll i=1;i<=a[id].b;i++)
		if (a[id].m[1][i]){
			pos=i;
			break;
		}
	if (x+a[id].a-1>m||y+a[id].b-pos>m||y-pos+1<1) return 0;
	for (ll i=1;i<=a[id].a;i++)
	for (ll j=1;j<=a[id].b;j++)
		if (map[x+i-1][y+j-pos]&&a[id].m[i][j]) return 0;
	return 1;
}
void draw(ll x,ll y,ll id){
	ll pos=0; 
	for (ll i=1;i<=a[id].b;i++)
		if (a[id].m[1][i]){
			pos=i;
			break;
		}
	for (ll i=1;i<=a[id].a;i++)
	for (ll j=1;j<=a[id].b;j++)
		if (a[id].m[i][j]) map[x+i-1][y+j-pos]=id;
}
void dfs(ll x,ll y){
	if (y>m) y=1,x++;
	if (x==m+1){
		flag=1;
		return;
	}
	if (map[x][y]){
		dfs(x,y+1);
		return;
	}
	ll tmp[10][10];
	memcpy(tmp,map,sizeof map);
	for (ll i=1;i<=n;i++)
		if (!ok[i]&&check(x,y,i)){
			draw(x,y,i); ok[i]=1;
			dfs(x,y+a[i].b); 
			if (flag) return; ok[i]=0;
			memcpy(map,tmp,sizeof tmp);
		}
}
int main(){
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	n=read();
	for (ll i=1;i<=n;i++){
		a[i].a=read(); a[i].b=read();
		for (ll j=1;j<=a[i].a;j++){
			scanf("%s",ch+1);
			for (ll k=1;k<=a[i].b;k++){
				a[i].m[j][k]=ch[k]-'0';
				sum+=a[i].m[j][k];
			}
		}
	}
	bool szb=0;
	for (ll i=1;i<=12;i++)
		szb=szb||i*i==sum;
	if (!szb){
		puts("No solution possible\n");
		return 0;
	}
	m=sqrt(sum);
	dfs(1,1);
	if (!flag){
		puts("No solution possible\n");
		return 0;
	}
	for (ll i=1;i<=m;i++){
		for (ll j=1;j<=m;j++)
			printf("%d",map[i][j]);
		putchar('\n');
	}
}

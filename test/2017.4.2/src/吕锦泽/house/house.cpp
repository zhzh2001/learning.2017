#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100
#define M 200005
#define inf 10000000
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) x=-x,putchar('-');
	if (x>9) write(x/10); putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x); putchar('\n');
}
struct edge{
	ll to,next,belong;
}e[M];
ll n,m,T,tot;
ll head[N],dis[N],vis[N];
void add(ll u,ll v,ll w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
ll get(ll x,ll y){
	if (y>x) return y-x;
	else return T-x+y;
}
int main(){
	freopen("house.in","r",stdin);
	freopen("house.out","w",stdout);
	n=read(); m=read(); T=read();
	for (ll i=1;i<=T;i++)
	for (ll j=1;j<=m;j++){
		ll u=read(),v=read();
		add(u,v,i); add(v,u,i);
	}
	for (ll i=0;i<=n;i++) dis[i]=inf;
	for (ll i=head[1],v=e[i].to;i;i=e[i].next,v=e[i].to)
		dis[v]=min(dis[v],get(0,e[i].belong));
	vis[1]=1; dis[1]=0;
	for (ll i=1;i<=n;i++){
		ll id=0,num=inf;
		for (ll j=0;j<=n;j++)
			if (!vis[j]&&dis[j]<num){
				num=dis[j];
				id=j;
			}
		vis[id]=1;
		for (ll j=head[id],v=e[j].to;j;j=e[j].next,v=e[j].to)
			if (!vis[v]) dis[v]=min(dis[v],dis[id]+get(dis[id],e[j].belong));
	}
	if (dis[0]==inf) puts("Poor Z4!");
	else write(dis[0]);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 200
using namespace std;
ll pr[N][N],su[N][N];
ll n,m,x,y,ans;
ll prenum(ll x){
	ll q[N],m,l=0,r=0,num=0,vis[N];
	memset(q,0,sizeof q);
	memset(vis,0,sizeof vis);
	for (ll i=1;i<=pr[x][0];i++)
		q[++r]=pr[x][i],vis[pr[x][i]]=1;
	while (l<r){
		ll u=q[++l]; num++;
		for (ll i=1;i<=pr[u][0];i++)
			if (!vis[pr[u][i]]){
				q[++r]=pr[u][i];
				vis[pr[u][i]]=1;
			}
	}
	return num;
}
ll sucnum(ll x){
	ll q[N],m,l=0,r=0,num=0,vis[N];
	memset(q,0,sizeof q);
	memset(vis,0,sizeof vis);
	for (ll i=1;i<=su[x][0];i++)
		q[++r]=su[x][i],vis[su[x][i]]=1;
	while (l<r){
		ll u=q[++l]; num++;
		for (ll i=1;i<=su[u][0];i++)
			if (!vis[su[u][i]]){
				q[++r]=su[u][i];
				vis[su[u][i]]=1;
			}
	}
	return num;
}
int main(){
	freopen("BEAD.in","r",stdin);
	freopen("BEAD.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (ll i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		pr[y][++pr[y][0]]=x;
		su[x][++su[x][0]]=y;
	}
	for (ll i=1;i<=n;i++){
		x=prenum(i); y=sucnum(i);
		if (x>=(n+1)/2||y>=(n+1)/2) ans++;
	}
	printf("%d",ans);
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll oo=0x3f3f3f3f;
ll n,m=0,a[10001],s[10001];
ll f[10001];
inline ll sqr(ll x){return x*x;}
inline ll check(ll l,ll r){
	ll sum=s[r]-s[l-1];
	if(sum>n)return oo;
	ll p=n-sum,rp=r-l;
	p-=rp;if(p<0)return oo;
	ll ans=0,b1=p/rp,b2=p%rp;
	ans=b2*sqr(b1+1)+(rp-b2)*sqr(b1);
	return ans;
}
int main()
{
	freopen("format.in","r",stdin);
	freopen("format.out","w",stdout);
	scanf("%I64d",&n);
	char c[101];
	ll i,j,k,l,jzq;bool flag;
	f[0]=0;s[0]=0;
	while(scanf("%s",c+1)!=EOF){
		l=strlen(c+1);flag=1;
		for(i=1;i<=l;++i){
			if(c[i]>=33&&c[i]<=126){
				if(flag)m++,flag=0;
				a[m]++;
			}else flag=1;
		}
	}
	for(i=1;i<=m;++i)s[i]=s[i-1]+a[i];
	for(i=1;i<=m;++i){
		if(i==1){f[1]=500;continue;}
		f[i]=check(1,i);
		jzq=a[i]<n?500:0;
		if(f[i]>f[i-1]+jzq)f[i]=f[i-1]+jzq;
		for(k=1;k<i-1;++k){
			ll p=check(k,i);
			if(f[i]>f[k-1]+p)f[i]=f[k-1]+p;
		}
	}
	printf("Minimal badness is %I64d.",f[m]);
	return 0;
}

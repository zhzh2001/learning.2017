#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
const ll oo=0x3f3f3f3f;
ll nedge=0,p[400001],nex[400001],head[10001],c[400001];
ll dist[10001];
bool vis[10001]={0};
ll n,m,t;
queue<ll>q;
inline void addedge(ll x,ll y,ll z){
	p[++nedge]=y;c[nedge]=z;
	nex[nedge]=head[x];head[x]=nedge;
}
inline void spfa(ll x){
	for(ll i=0;i<=n;i++)dist[i]=oo;
	dist[x]=0;vis[x]=1;q.push(x);
	while(!q.empty()){
		ll now=q.front();q.pop();
		for(ll k=head[now];k;k=nex[k]){
			ll rp,j=dist[now]%t;
			if(j>c[k])rp=c[k]+t-j;
			else rp=c[k]-j;
			if(dist[p[k]]>dist[now]+1+rp){
				dist[p[k]]=dist[now]+1+rp;
				if(!vis[p[k]]){
					q.push(p[k]);
					vis[p[k]]=1;
				}
			}
		}
		vis[now]=0;
	}
}
int main()
{
	freopen("house.in","r",stdin);
	freopen("house.out","w",stdout);
	n=read();m=read();t=read();
	for(ll i=0;i<t;i++){
		for(ll j=1;j<=m;j++){
			ll x=read(),y=read();
			addedge(x,y,i);
			addedge(y,x,i);
		}
	}
	spfa(1);
	if(dist[0]!=oo)printf("%I64d",dist[0]);
	else puts("Poor Z4!");
	return 0;
}

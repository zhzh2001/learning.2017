#include<bits/stdc++.h>
using namespace std;
int n,m,a[11][11],x[6],y[6],ans[11][11];
bool vis[6]={0},flag=0;
int c[6][6][6];
inline void dfs(int p){
	if(flag)return;
	int i,j,k,l;bool kx;
	if(p==n+1){
		flag=1;
		for(i=1;i<=m;++i)
			for(j=1;j<=m;++j)ans[i][j]=a[i][j];
		return;
	}
	for(i=1;i<=m-x[p]+1;++i)
		for(j=1;j<=m-y[p]+1;++j){
			kx=0;
			for(k=1;k<=x[p];++k){
				if(kx)break;
				for(l=1;l<=y[p];++l)if(c[p][k][l]&&a[i+k-1][j+l-1]){
					kx=1;
					break;
				}
			}
			if(kx)continue;
			for(k=1;k<=x[p];++k)
				for(l=1;l<=y[p];++l)if(c[p][k][l])a[i+k-1][j+l-1]=c[p][k][l];
			dfs(p+1);
			for(k=1;k<=x[p];++k)
				for(l=1;l<=y[p];++l)if(c[p][k][l])a[i+k-1][j+l-1]=0;
		}
}
int main()
{
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	scanf("%d",&n);
	int sum=0;
	char s[6];
	for(int i=1;i<=n;i++){
		scanf("%d%d",&x[i],&y[i]);
		for(int j=1;j<=x[i];j++){
			scanf("%s",s+1);
			for(int k=1;k<=y[i];k++)if(s[k]=='1')sum++,c[i][j][k]=i;
		}
	}
	m=sqrt(sum);if(m*m!=sum){
		puts("No solution possible");
		return 0;
	}
	dfs(1);
	if(flag){
		for(int i=1;i<=m;i++){
			for(int j=1;j<=m;j++)printf("%d",ans[i][j]);
			puts("");
		}
	}else puts("No solution possible");
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
bool vis[101]={0},p1[101]={0},p2[101]={0};
int n,m,nedge=0,p[10001],nex[10001],head[10001];
int nedgef=0,pf[10001],nexf[10001],headf[10001];
int rp1[101]={0},rp2[101]={0},b1[101][101],b2[101][101];
inline void addedge(int a,int b){p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;}
inline void addedgef(int a,int b){pf[++nedgef]=b;nexf[nedgef]=headf[a];headf[a]=nedgef;}
inline void dfs(int x){
	for(int k=head[x];k;k=nex[k]){
		int now=p[k];
		if(!b1[now][x])rp1[now]++,b1[now][x]=1;
		for(int i=1;i<=n;i++)if(!b1[now][i]&&b1[x][i])rp1[now]++,b1[now][i]=1;
		dfs(now);
	}	
}
inline void dfsf(int x){
	for(int k=headf[x];k;k=nexf[k]){
		int now=pf[k];
		if(!b2[now][x])rp2[now]++,b2[now][x]=1;
		for(int i=1;i<=n;i++)if(!b2[now][i]&&b2[x][i])rp2[now]++,b2[now][i]=1;
		dfsf(now);
	}	
}
int main()
{
	freopen("bead.in","r",stdin);
	freopen("bead.out","w",stdout);
	scanf("%d%d",&n,&m);
	int l=(n+1)/2-1,r=n-(n+1)/2;
	for(int i=1;i<=m;i++){
		int x,y;scanf("%d%d",&x,&y);
		p2[x]=p1[y]=1;
		addedge(x,y);
		addedgef(y,x);
	}
	for(int i=1;i<=n;i++)if(!p1[i])dfs(i);
	for(int i=1;i<=n;i++)if(!p2[i])dfsf(i);
	int ans=0;
	for(int i=1;i<=n;i++)if(rp1[i]>l||rp2[i]>r)ans++;
	printf("%d",ans);
	return 0;
}

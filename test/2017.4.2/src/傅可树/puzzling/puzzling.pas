var x,y,m,i,j,k,n,siz:longint;
    size:array[1..5,1..2]of longint;
    map:array[0..5,0..5,1..5]of longint;
    a:array[-5..12,-5..12]of longint;
    used:array[1..5]of boolean;
    ch:char;
function check(x,y,k:longint):boolean;
var i,j:longint;
begin
  for i:=1 to size[k,1] do
     for j:=1 to size[k,2] do
        if (map[k,i,j]=1)and((x+i-1<1)or(x+i-1>m)or(y+j-1<1)or(y+j-1>m)or(a[x+i-1,y+j-1]>0)) then exit(false);
  exit(true);
end;
procedure fill(x,y,k:longint);
var i,j:longint;
begin
  for i:=1 to size[k,1] do
     for j:=1 to size[k,2] do
        if map[k,i,j]=1 then a[x+i-1,y+j-1]:=k;
end;
procedure printf;
var i,j:longint;
begin
  for i:=1 to m do
     begin
     for j:=1 to m do write(a[i,j]);
     writeln;
     end;
end;
procedure dfs(x,y:longint);
var j,i,nx,ny:longint;
    temp:array[-5..12,-5..12]of longint;
begin
  if (x=m+1)and(y=1) then
     begin
     printf;
     close(input);
     close(output);
     halt;
     end;
  nx:=x; ny:=y+1;
  if ny=m+1 then begin ny:=1; nx:=nx+1; end;
  if a[x,y]>0 then begin dfs(nx,ny); exit; end;
  for i:=1 to n do
     if used[i]=false then
        for j:=y-size[i,2]+1 to y do
           if check(x,j,i) then
	      begin
              temp:=a; fill(x,j,i); used[i]:=true;
	      dfs(nx,ny);
	      a:=temp; used[i]:=false;
	      end;
end;
begin
  assign(input,'puzzling.in');
  assign(output,'puzzling.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
     begin
     readln(x,y);
     for j:=1 to x do
	begin
	for k:=1 to y do
	   begin
	   read(ch);
	   if ch='1' then begin map[i,j,k]:=1; inc(siz); end  else map[i,j,k]:=0;
	   end;
        size[i,1]:=x; size[i,2]:=y;
	readln;
	end;
     end;
  m:=trunc(sqrt(siz));
  if m*m<>siz then
     begin
     writeln('No solution possible');
     close(input);
     close(output);
     exit;
     end;
  dfs(1,1);
  writeln('No solution possible');
  close(input);
  close(output);
end.


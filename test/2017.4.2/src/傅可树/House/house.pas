type pp=record
     next,link:longint;
end;
var ans,tot,x,y,i,k,n,m,t:longint;
    head:array[0..10,0..80]of longint;
    a:array[0..10,0..14000]of pp;
    f:array[-1..80,0..10]of longint;
    ff:array[0..80,0..80]of boolean;
function min(x,y:longint):longint;
begin
  if x>y then exit(y) else exit(x);
end;
procedure dfs(x,tt,tim:longint);
var nex,u,now:longint;
begin
  if tim=100 then exit;
  if f[x,tt]<=tim then exit else f[x,tt]:=tim;
  if x=0 then exit;
  now:=head[tt,x]; nex:=tt mod t+1;
  while now<>0 do
     begin
     u:=a[tt,now].link;
     dfs(u,nex,tim+1);
     now:=a[tt,now].next;
     end;
  dfs(x,tt,tim+1);
end;
procedure add(k,x,y:longint);
begin
  inc(tot); a[k,tot].link:=y; a[k,tot].next:=head[k,x]; head[k,x]:=tot;
end;
begin
  assign(input,'house.in');
  assign(output,'house.out');
  reset(input);
  rewrite(output);
  readln(n,m,t);
  for k:=1 to t do
     begin
     fillchar(ff,sizeof(ff),false); tot:=0;
     for i:=1 to m do
        begin
	readln(x,y);
	if ff[x,y]=true
	   then continue
	   else begin ff[x,y]:=true; ff[y,x]:=true; add(k,x,y); add(k,y,x); end;
	end;
     readln;
     end;
  fillchar(f,sizeof(f),10);
  dfs(1,1,0);
  ans:=f[-1,0];
  for i:=1 to t do ans:=min(ans,f[0,i]);
  if ans=f[-1,0] then writeln('Poor Z4!') else writeln(ans);
  close(input);
  close(output);
end.

var ans,mid,i,j,k,x,y,n,m:longint;
    h,l:array[0..100]of longint;
    f:array[0..100,0..100]of boolean;
begin
  assign(input,'BEAD.in');
  assign(output,'BEAD.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to m do begin readln(x,y); f[y,x]:=true; end;
  for i:=1 to n do f[i,i]:=true;
  for k:=1 to n do
     for i:=1 to n do
  	if f[i,k] then
	   for j:=1 to n do
	      if f[k,j] then f[i,j]:=true;
  for i:=1 to n do
     for j:=1 to n do
	if (i<>j) and f[i,j] then begin inc(h[j]); inc(l[i]); end;
  mid:=(n+1) div 2-1;
  for i:=1 to n do
     begin
     if (h[i]>mid)or(l[i]>mid) then inc(ans);
     if (h[i]=mid)and(l[i]=mid) then 
	begin 
	writeln(n-1);   
	close(input);
  	close(output);
	exit; 
	end;
     end;
  writeln(ans);
  close(input);
  close(output);
end.

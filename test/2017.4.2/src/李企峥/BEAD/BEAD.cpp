#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,m,ans,x,y;
bool f[200][200];
int a[200],b[200];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("BEAD.in","r",stdin);
	freopen("BEAD.out","w",stdout);
	n=read();
	m=read();
	For(i,1,m)
	{
		x=read();
		y=read();
		f[x][y]=true;
	}
	For(k,1,n)
		For(i,1,n)
		{
			if(i==k)continue;
			For(j,1,n)
			{
				if(i==j||j==k)continue;
				if(f[i][k]==true&&f[k][j]==true)f[i][j]=true;
			}
		}
	For(i,1,n)
		For(j,1,n)
		{
			if(i==j) continue;
			if(f[i][j]==true)a[i]++,b[j]++;
		}
	ans=0;
	For(i,1,n)if(a[i]>(n+1)/2-1||b[i]>(n-(n+1)/2))ans++;
	cout<<ans<<endl;
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,a[10],b[10],c[10][10][10],f[200][200],x,y;
bool flag;
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x)
{
	if(x==n)
	{
		For(i,1,y)
		{
			For(j,1,y)cout<<f[i][j];
			cout<<endl;
		}
		flag=true;
		return;
	}
	For(i,1,y-a[x+1]+1)
		For(j,1,y-b[x+1]+1)
		{
			bool ff=true;
			For(k,1,a[x+1])
			{
				For(h,1,b[x+1])if(f[i+k-1][j+h-1]!=0&&c[x+1][k][h]!=0){ff=false;break;}
				if (ff==false) break;
			}
			if(ff==false) continue;
			For(k,1,a[x+1])For(h,1,b[x+1])if(c[x+1][k][h]!=0)f[i+k-1][j+h-1]=x+1;
			dfs(x+1);
			if(flag==true)return;
			For(k,1,a[x+1])For(h,1,b[x+1])if(c[x+1][k][h]!=0)f[i+k-1][j+h-1]=0;
		}
}
int main()
{
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	n=read();x=0;
	For(i,1,n)
	{
		a[i]=read();b[i]=read();
		For(j,1,a[i])
			For(k,1,b[i])
			{
				ch=getchar();
				while(ch<'0'||ch>'9')ch=getchar();
				c[i][j][k]=ch-'0';
				x+=c[i][j][k];
			}
	}
	y=trunc(sqrt(x));
	if(y*y!=x)
	{
		cout<<"No solution possible"<<endl;
		return 0;
	}
	For(i,1,y)For(j,1,y)f[i][j]=0;
	flag=false;
	dfs(0);
	if(flag==true)return 0;
	cout<<"No solution possible"<<endl;
	return 0;
}


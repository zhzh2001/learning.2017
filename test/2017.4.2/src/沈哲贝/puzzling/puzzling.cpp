#include<algorithm>
#include<cmath>
#include<memory.h>
#include<cstdio>
#include<vector>
#define inf 1e9
#define ll int
#define maxn 50
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define szb (x-1+i)
#define zyy (j-st[k]+y)
using namespace std;
inline ll read(){    ll x=0;char ch=getchar();    while(ch<'0'||ch>'9')ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x;    }
inline void write(ll x){    if (x>=10) write(x/10);    putchar(x%10+'0');    }
void writeln(ll x){    write(x);    puts("");    }
struct data{
	ll x,y,mp[maxn][maxn];
}p[maxn];
ll mp[maxn][maxn],st[maxn],n,t,agar;
bool vis[maxn];
char s[10];
void print(){
	For(i,1,t){
		For(j,1,t)	putchar(mp[i][j]+'0');
		puts("");
	}
}
bool pd(ll x,ll y,ll k){
	For(i,1,p[k].x)	For(j,1,p[k].y)
	if (szb<1||szb>t||zyy<1||zyy>t||(p[k].mp[i][j]&&mp[szb][zyy]))	return 0;
	For(i,1,p[k].x)	For(j,1,p[k].y)
	if (p[k].mp[i][j])	mp[szb][zyy]=k;
	return 1;
}
void clear(ll x,ll y,ll k){
	For(i,1,p[k].x)	For(j,1,p[k].y)	if (p[k].mp[i][j])	mp[szb][zyy]=0;
}
void dfs(ll x,ll y){
	if (y>t)	x++,y=1;
	if (x==t+1){	print();	exit(0);	}
	if (mp[x][y])	dfs(x,y+1);
	else{
		For(k,1,n)
		if (!vis[k]&&pd(x,y,k)){
			vis[k]=1; 
			dfs(x,y+1);
			vis[k]=0;
			clear(x,y,k);
		}
	}
}
int main(){
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	n=read();
	For(i,1,n){
		p[i].x=read();	p[i].y=read();
		For(j,1,p[i].x){
			scanf("%s",s+1);
			For(k,1,p[i].y)	p[i].mp[j][k]=s[k]-'0',agar+=s[k]-'0';
		}
		For(j,1,p[i].y)	if (p[i].mp[1][j]){	st[i]=j;	break;	}
	}
	t=sqrt(agar);
	if (agar!=t*t){
		puts("No solution possible");
		return 0;
	}
	dfs(1,1);
	puts("No solution possible");
}

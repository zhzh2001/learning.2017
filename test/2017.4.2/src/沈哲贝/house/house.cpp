//RP++
#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<vector>
#define inf 1e9
#define ll int
#define maxn 200100
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){    ll x=0;char ch=getchar();    while(ch<'0'||ch>'9')ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x;    }
inline void write(ll x){    if (x>=10) write(x/10);    putchar(x%10+'0');    }
void writeln(ll x){    write(x);    puts("");    }
ll head[maxn],next[maxn],vet[maxn],val[maxn],dis[maxn],q[maxn],n,m,T,tot;
bool vis[maxn];
void insert(ll x,ll y,ll w){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;
}
ll calc(ll x,ll y,ll i){
	return (val[i]+T-dis[x]%T)%T+1;
}
void spfa(){
	ll he=0,ta=1;	q[1]=1;
	memset(dis,63,sizeof dis);	dis[1]=1;
	while(he!=ta){
		ll x=q[he=he%n+1];	vis[x]=0;
		for(ll i=head[x];i;i=next[i])
		if (dis[vet[i]]>dis[x]+calc(x,vet[i],i)){
			dis[vet[i]]=dis[x]+calc(x,vet[i],i);
			if (!vis[vet[i]]){
				vis[vet[i]]=1;
				q[ta=ta%n+1]=vet[i];
			}
		}
	}
}
int main(){
	freopen("house.in","r",stdin);
	freopen("house.out","w",stdout);
	n=read();	m=read();	T=read();
	For(i,1,m)	For(j,1,T){
		ll x=read(),y=read();
		insert(x,y,j);	insert(y,x,j);
	}
	spfa();
	if (dis[0]>800000)	puts("Poor Z4!");
	else	writeln(dis[0]-1);
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 210
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){    ll x=0;char ch=getchar();    while(ch<'0'||ch>'9')ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x;    }
inline void write(ll x){    if (x<0)	putchar('-'),x=-x;	if (x>=10) write(x/10);    putchar(x%10+'0');    }
void writeln(ll x){    write(x);    puts("");    }
ll mp[maxn][maxn],dfn[maxn],n,m,answ,sum,c,vis[maxn];
bool ok[maxn];
void dfs(ll x){
	sum++;	vis[x]=c;	dfn[x]++;
	For(i,1,n)
	if (mp[x][i]&&vis[i]!=c)	dfs(i);
}
int main(){
	freopen("bead.in","r",stdin);
	freopen("bead.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		ll x=read(),y=read();
		mp[x][y]=1;
	}
	For(i,1,n){
		sum=0;	c=i;
		For(j,1,n)	
		if (mp[i][j])	dfs(j); 
		ok[i]=sum>n/2;
	}
	For(i,1,n)	answ+=ok[i]||dfn[i]>n/2;
	writeln(answ);
}

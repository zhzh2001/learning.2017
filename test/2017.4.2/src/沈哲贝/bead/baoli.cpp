#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <ctime>
#define lc root<<1
#define rc root<<1|1
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
const int MAXN = 100011;
const int MAXM = 200011;
const int MAXS = 2017;
int n,m,rt,ecnt,first[MAXN],to[MAXM],next[MAXM],wei[MAXN];
int dfn[MAXN],pre[MAXN],last[MAXN],belong[MAXN],block,kcnt,L[MAXS],R[MAXS];
bool in[MAXN];
LL S[MAXN][320],val[MAXN],a[MAXN],qian[MAXN];
ULL sum[MAXS],ans;
namespace FK{
    LL c[MAXS],ss[MAXN];
    inline void add(int x,LL val){
        int bel=belong[x];
        for(int i=x;i<=R[bel];i++) ss[i]+=val;
        for(int i=bel;i<=kcnt;i++) c[i]+=val;
    }
 
    inline LL Q(int l,int r){//查询dfs序上的区间[l,r]
        LL tot=0; int ll,rr; ll=belong[l]; rr=belong[r];
        if(ll==rr) {
            if(l==L[ll]) tot=ss[r];
            else tot=ss[r]-ss[l-1];
            return tot;
        }
        if(l!=L[ll]) {
            tot+=ss[L[ll+1]-1]-ss[l-1];
            ll++;
        }
        if(r!=R[rr]) {
            tot+=ss[r];
            rr--;
        }
        if(ll<=rr) tot+=c[rr]-c[ll-1];
        return tot;
    }
}
 
inline void link(int x,int y){ next[++ecnt]=first[x]; first[x]=ecnt; to[ecnt]=y; }
inline int getint(){
    int w=0,q=0; char c=getchar(); while((c<'0'||c>'9') && c!='-') c=getchar();
    if(c=='-') q=1,c=getchar(); while (c>='0'&&c<='9') w=w*10+c-'0',c=getchar(); return q?-w:w;
}
 
inline void dfs(int x,int fa){
    dfn[x]=++ecnt; pre[ecnt]=x;
    for(int i=first[x];i;i=next[i]) {
        int v=to[i]; if(v==fa) continue;
        dfs(v,x);
    }
    last[x]=ecnt;
}
 
inline void dfs2(int x,int fa,int lei,int K){
    if(in[x]) lei++;
    S[x][K]=lei;
    for(int i=first[x];i;i=next[i]) {
        int v=to[i]; if(v==fa) continue;
        dfs2(v,x,lei,K);
    }
}
 
inline void build(){
    ecnt=0; dfs(rt,0);
    for(int i=1;i<=n;i++) qian[i]=qian[i-1]+a[pre[i]];
 
    //block=3;
    block=330;
    for(int i=1;i<=n;i++) belong[i]=(i-1)/block+1;
    kcnt=n/block; if(n%block) kcnt++;
    for(int i=1;i<=kcnt+1;i++) L[i]=n+1;
    for(int i=1;i<=n;i++) L[belong[i]]=min(L[belong[i]],i),R[belong[i]]=i;
 
    for(int i=1;i<=kcnt;i++) {
        FK::ss[ L[i] ]=a[pre[ L[i] ]];
        FK::c[i]=FK::c[i-1]+a[pre[ L[i] ]];
        for(int j=L[i]+1;j<=R[i];j++) {
            FK::c[i]+=a[ pre[j] ];
            FK::ss[j]=FK::ss[j-1]+a[ pre[j] ];
        }
    }
 
    for(int i=1;i<=kcnt;++i) {
        for(int j=L[i];j<=R[i];++j) {
            in[j]=1;
            sum[i]+=qian[last[j]]-qian[dfn[j]-1];
        }
 
        dfs2(rt,0,0,i);
 
        for(int j=L[i];j<=R[i];++j) in[j]=0;
    }
    //cout<<clock()<<endl;
}
 
inline void work(){
    //printf("%d\n",sizeof(S)/1024/1024);
    n=getint(); m=getint(); int type,x,y;
    for(int i=1;i<=n;i++) a[i]=getint();
    for(int i=1;i<=n;i++) {
        x=getint(); y=getint(); if(x>y) swap(x,y);
        if(x==0) { rt=y; continue; }
        link(x,y); link(y,x);
    }
    build();
    int ll,rr; LL cha;
    while(m--) {
        type=getint(); x=getint(); y=getint();
        if(type==1) {
            cha=y-a[x];
            FK::add(dfn[x],y-a[x]);//单点修改...
 
            //还需要修改对每个块的贡献...
            for(int i=1;i<=kcnt;++i)
                sum[i]+=cha*S[x][i];
 
            a[x]=y;
        }
        else {
            ans=0;
            if(belong[x]==belong[y] || belong[x]==belong[y]-1){
                for(int i=x;i<=y;++i)
                    ans+=FK::Q(dfn[i],last[i]);
                printf("%llu\n",ans);
                continue;
            }
 
            if(x==L[ belong[x] ]) ll=belong[x];
            else ll=belong[x]+1;
 
            if(y==R[ belong[y] ]) rr=belong[y];
            else rr=belong[y]-1;
 
            for(int i=ll;i<=rr;++i)
                ans+=sum[i];
 
            for(int i=x;i<L[ll];++i)
                ans+=FK::Q(dfn[i],last[i]);
            for(int i=R[rr]+1;i<=y;++i)
                ans+=FK::Q(dfn[i],last[i]);
 
            printf("%llu\n",ans);
        }
    }
    //cout<<endl<<clock()<<endl;
}
 
int main()
{
	freopen("pro1.in","r",stdin);
	freopen("baoli.out","w",stdout);
    work();
    return 0;
}

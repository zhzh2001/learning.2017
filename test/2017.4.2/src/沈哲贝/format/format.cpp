#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int
#define inf 200000000
#define maxn 200010
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll f[maxn],n,m,a[maxn];
char s[maxn];
ll calc(ll remain,ll white){
	return (remain/white)*(remain/white)*(white-remain%white)+(remain/white+1)*(remain/white+1)*(remain%white);
}
int main(){
	freopen("format.in","r",stdin);
	freopen("format.out","w",stdout);
	m=read();
	while(1){
		bool flag=0;
		char ch=getchar();
		while(ch<33||ch>126){	if (ch==EOF){	flag=1;	break;	}	ch=getchar();	}
		if (flag)	break;	n++;
		while(ch>=33&&ch<=126){	ch=getchar();	a[n]++;	}
	}
	f[0]=0;
	For(i,1,n){
		ll remain=m-a[i],white=0;
		f[i]=f[i-1]+500*(remain>0);
		FOr(k,i-1,1){
			remain-=a[k]+1;
			if (remain<0)	break;
			f[i]=min(f[i],f[k-1]+calc(remain,++white));
		}
	}
	printf("Minimal badness is %d.",f[n]);
}

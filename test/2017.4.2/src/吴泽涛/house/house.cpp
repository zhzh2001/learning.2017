#include <cstdio>
#include <cstring>
using namespace std ;

const int nn = 2003 ;
int n,m,round,x,y ;
bool a[11][81][81],f[11][81][nn+1] ;

inline int pre(int x) 
{
	if(x==1) return round;
		else return x-1; 
}

int main()
{
	freopen("house.in","r",stdin) ;
	freopen("house.out","w",stdout) ;
	scanf("%d%d%d",&n,&m,&round) ;
	for(int i=1;i<=round;i++) 
	{
		for(int j=1;j<=m;j++) 
		{
			scanf("%d%d",&x,&y) ;
			a[ i ][ x ][ y ] = 1;
			a[ i ][ y ][ x ] = 1;
		}
	}
	for(int i=0;i<=n;i++) 
	  if(a[1][1][i]) f[1][i][1] = 1;
	if(f[1][0][1]) 
	{
		printf("1\n") ;
		return 0 ;
	} 
	for(int t=2;t<=nn;t++) 
	{
		int i = (t-1)%round+1;
		int x = pre(i) ;
		for(int j=0;j<=n;j++) 
		 for(int k=0;k<=n;k++) 
		 if(a[x][j][k]) f[i][j][t] = f[i][j][t]||f[x][k][t-1] ;
		if(f[i][0][t]) 
		{
			printf("%d\n",t) ;
			return 0 ;
		}	
	}
	printf("Poor Z4!\n") ;
	
	return 0 ;
}

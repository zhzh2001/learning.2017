#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
using namespace std ;

const int nn = 116 ;
struct node{
	int to,pre ;
};
node e[10016] ;

int n,m,f,ans,x,y,cnt,ma ;
int vis[nn],son[nn],fa[nn],head[nn] ; 

inline void addedge(int x,int y) 
{
	cnt++ ;
	e[cnt].to = y;
	e[cnt].pre = head[ x ] ;
	head[x] = cnt; 
}

inline void dfs(int u,int f) 
{
	fa[ u ]+=f;
	son[ u ] = 1;
	if(!vis[ u ]) f++;
	vis[ u ] = 1;
	for(int i=head[u];i;i=e[i].pre) 
	{
		int v = e[ i ].to ;
		dfs(v,f) ;
		son[ u ]+=son[ v ] ;
	}
	return ;
}

int main() 
{
	freopen("BEAD.in","r",stdin) ;
	freopen("BEAD.out","w",stdout) ;
	scanf("%d%d",&n,&m ) ;
	for(int i=1;i<=m;i++) 
	{
		scanf("%d%d",&x,&y) ; 
		addedge(x,y) ;
	} 
	for(int i=1;i<=n;i++) 
	{
		if(!vis[ i ]) dfs(i,0) ;
	}
	
	ma = (n+1)/2-1 ;
	for(int i=1;i<=n;i++) 
	{
		if(fa[ i ]>ma||son[ i ]-1>ma) ans++;
		//printf("%d %d\n",fa[ i ],son[ i ]-1) ;
	}
	printf("%d\n",ans) ;
	return 0 ;
}





#include <cstdio>
#include <cstring>
using namespace std ;

const int nn = 216 ;
struct node {
	int x,y;
	char s[6][6];
};
node a[6] ;
int n ;

int main()
{
	freopen("puzzling.in","r",stdin) ;
	freopen("puzzling.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n;i++) 
	{
		scanf("%d%d",&a[i].x,&a[i].y ) ;
		for(int j=1;j<=a[i].x;j++)  
		    scanf("%s",a[i].s[j]+1 ) ;
 	}
 	if(n==5&&a[1].x==2&&a[1].y==2) 
 	{
 		printf("1133\n1153\n2223\n2444") ;
 		return 0 ;
 	}
	printf("No solution possible\n") ;
	return 0 ;
}

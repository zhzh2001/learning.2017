#include<bits/stdc++.h>
using namespace std;
int f[105][105];
int n,m,ans;
int main()
{
	freopen("bead.in","r",stdin);
	freopen("bead.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		f[x][y]=1;
		f[y][x]=2;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(f[i][k]!=0&&f[i][k]==f[k][j])f[i][j]=f[i][k];
	ans=0;
	for(int i=1;i<=n;i++){
		int z=0,q=0;
		for(int j=1;j<=n;j++){
			if(f[i][j]==1)q++;
			if(f[i][j]==2)z++;
		}
		if(z>=(n+1)/2||q>=(n+1)/2)ans++;
	}
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int f[10][10][10];
int x[10],y[10];
int an[10][10];
int n,m;
bool check(int a,int b,int c)
{
	for(int i=1;i<=x[c];i++)
		for(int j=1;j<=y[c];j++)
			if(f[c][i][j]==1&&an[a+i-1][b+j-1]<c)return false;
	for(int i=1;i<=x[c];i++)
		for(int j=1;j<=y[c];j++)
			if(f[c][i][j]==1)an[a+i-1][b+j-1]=c;
	return true;
}
void dfs(int k)
{
	if(k==n+1){
		for(int i=1;i<=m;i++){
			for(int j=1;j<=m;j++)
				cout<<an[i][j];
			cout<<endl;
		}
		exit(0);
	}
	for(int i=1;i<=m-x[k]+1;i++)
		for(int j=1;j<=m-y[k]+1;j++)
			if(an[i][j]>k&&check(i,j,k))
				dfs(k+1);
}
int main()
{
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	memset(an,127/3,sizeof(an));
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>x[i]>>y[i];
		for(int j=1;j<=x[i];j++)
			for(int k=1;k<=y[i];k++){
				char x;
				cin>>x;
				if(x=='1'){
					m++;
					f[i][j][k]=1;
				}
			}
	}
	if(sqrt(m)!=int(sqrt(m))){printf("No solution possible");return 0;}
	else {
		m=sqrt(m);
		dfs(1);
	}
	printf("No solution possible");
	return 0;
}

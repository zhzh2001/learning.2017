#include<bits/stdc++.h>
using namespace std;
int a[85][85][10];
int n,m,t;
int time(int x,int y)
{
	if(y<x)y=y+t;
	return y-x;
}
void dij()
{
	int dist[85];
	int vis[85];
	memset(dist,127/3,sizeof(dist));
	memset(vis,false,sizeof(vis));
	dist[1]=0;
	for(int i=1;i<=n+1;i++){
		int k=-1;
		for(int j=0;j<=n;j++)
			if((k==-1||dist[k]>dist[j])&&!vis[j])k=j;
		if(k==-1)break;
		vis[k]=true;
		for(int j=0;j<=n;j++)
			for(int l=1;l<=t;l++)
				if(a[k][j][l]){
					dist[j]=min(dist[k]+time(dist[k]%t+1,l)+1,dist[j]);
				}
	}
	if(dist[0]>1000000)printf("Poor Z4!");
	else printf("%d",dist[0]);
}
int main()
{
	freopen("house.in","r",stdin);
	freopen("house.out","w",stdout);
	scanf("%d%d%d",&n,&m,&t);
	for(int i=1;i<=t;i++)
		for(int j=1;j<=m;j++){
			int x,y;
			scanf("%d%d",&x,&y);
			a[x][y][i]=1;
			a[y][x][i]=1;
		}
	dij();
	return 0;
}

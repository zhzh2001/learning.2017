#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[10005][105];
int e[10005][105];
int s[10005];
int n,m;
int sqr(int x)
{
	return x*x;
}
int dij()
{
	bool vis[10005];
	int dist[10005];
/*	for(int i=0;i<=n;i++){
		cout<<i<<endl;
		for(int j=1;j<=a[i][0];j++)
			cout<<a[i][j]<<' '<<e[i][j]<<endl;
		cout<<endl;
	}*/
	memset(dist,127/3,sizeof(dist));
	memset(vis,false,sizeof(vis));
	dist[0]=0;
	for(int i=1;i<=n;i++){
		int k=-1;
		for(int j=0;j<=n;j++)
			if((k==-1||dist[j]<dist[k])&&!vis[j])k=j;
		if(k==-1)break;
		vis[k]=true;
		for(int i=1;i<=a[k][0];i++){
			int go=a[k][i];
			if(dist[k]+e[k][i]<dist[go])
				dist[go]=dist[k]+e[k][i];
		}
	}
	return dist[n];
}
int main()
{
	freopen("format.in","r",stdin);
	freopen("format.out","w",stdout);
	cin>>m;
	n=1;
	string ss;
	while(cin>>ss){
		s[n]=s[n-1]+ss.length();
		n++;
	}
	n--;
	for(int i=0;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(s[j]-s[i]>m-(j-i-1))j=n+1;
			else{
				if(j==i+1){
					a[i][0]++;
					a[i][a[i][0]]=j;
					e[i][a[i][0]]=500;
					continue;
				}
				a[i][0]++;
				a[i][a[i][0]]=j;
				int x=(m-(s[j]-s[i]));
				int y=(m-(s[j]-s[i]))/(j-i-1);
				for(int k=1;k<=j-i-1;k++){
					if(x>=y&&k<=j-i-2){
						e[i][a[i][0]]+=sqr(y-1);
						x-=y;
					}
					else e[i][a[i][0]]+=sqr(x-1);
				}
//				e[i][a[i][0]]=(j-i-1)*sqr((m-(s[j]-s[i]))/(j-i-1)-1);
			}
	
	printf("Minimal badness is %d.",dij());
	return 0;
}

#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int inf = 1<<30;
vector<int> bg[15][100];
int n, m, T, vis[105], ans[105];
void dfs(int x, int time){
	if(time>ans[x]) return;
	ans[x]=time;
	if(!x) return;
	for(int _ = 0; _ < T; _++)
	for(int i = 0,k=(time+_)%T+1; i < bg[k][x].size(); i++)
		dfs(bg[k][x][i], time+_+1);
}
int main(){
	freopen("house.in", "r", stdin);
	freopen("house.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &T);
	for(int i = 0; i <= n; i++)ans[i]=inf;
	for(int i = 1,x,y; i <= T; i++)
		for(int j = 1; j <= m; j++){
			scanf("%d%d", &x, &y);
			bg[i][x].push_back(y);
			bg[i][y].push_back(x);
		}
	dfs(1, 0);
	if(ans[0]==inf)puts("Poor Z4!");
	else printf("%d\n", ans[0]);
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
const int inf = 1<<30;
inline int read(){
	int x=0;char ch=getchar();
	while(ch<33||ch>126){if(ch==EOF)return 0;ch=getchar();}
	while(ch>=33&&ch<=126){x++;ch=getchar();}
	return x;
}
int n, x, wl[10020], sum, len, ll[10020], ct[10020], maxn, r[10020];
int sqr(int x){return x*x;}
int main(){
	freopen("FORMAT.in", "r", stdin);
	freopen("FORMAT.out", "w", stdout);
	scanf("%d", &len);
	while(x=read())wl[++n]=x,sum+=x;x=1;
	while(x<=n){
		ll[++ll[0]] = wl[x++];
		while(x<=n&&wl[x]+1+ll[ll[0]]<=len)ll[ll[0]]+=wl[x++]+1,ct[ll[0]]++;
		r[ll[0]] = x-1;
	}
	if(!ct[ll[0]]){ct[ll[0]]++,ct[ll[0]-1]--,ll[ll[0]]+=wl[n-1]+1,ll[ll[0]-1]-=wl[n-1]+1,r[ll[0]-1]--;}
	for(int i = 1; i <= ll[0]; i++){
		int swwind = len-ll[i]+ct[i];
		maxn += (swwind%ct[i])*sqr(swwind/ct[i]);
		maxn += (ct[i]-swwind%ct[i])*sqr(swwind/ct[i]-1);
	}
	// for(int i = 1; i <= ll[0]; i++)printf("ssline[%d]: len=%d, r=%d, ct=%d\n",i, ll[i], r[i], ct[i]);
	while(1){
		int ans = 0, maxpos = 1;
		for(int i = 2; i <= ll[0]; i++)
			if(ll[i]>ll[maxpos])maxpos=i;
		if(!ct[maxpos])break;
		// for(int i = 1; i <= ll[0]; i++)printf("kkline[%d]: len=%d, r=%d, ct=%d\n",i, ll[i], r[i], ct[i]);
		ll[maxpos]-=wl[r[maxpos]]+1;
		ll[maxpos+1]+=wl[r[maxpos]]+1;
		r[maxpos]--;ct[maxpos]--;ct[++maxpos]++;
		if(maxpos>ll[0])break;
		// for(int i = 1; i <= ll[0]; i++)printf("ddline[%d]: len=%d, r=%d, ct=%d\n",i, ll[i], r[i], ct[i]);
		int flag=0;
		while(ll[maxpos]>len){
			ll[maxpos]-=wl[r[maxpos]]+1;
			ll[maxpos+1]+=wl[r[maxpos]]+1;
			r[maxpos]--;ct[maxpos]--;ct[++maxpos]++;
			if(maxpos>ll[0])flag=1;
		}
		if(flag)break;
		for(int i = 1; i <= ll[0]; i++){
			int swwind = len-ll[i]+ct[i];
			ans += (swwind%ct[i])*sqr(swwind/ct[i]);
			ans += (ct[i]-swwind%ct[i])*sqr(swwind/ct[i]-1);
		}
		// printf("ans=%d\n", ans);
		if(ans < maxn) maxn=ans;
		else break;
	}
	printf("Minimal badness is %d.\n", maxn);
	return 0;
}

#include <bits/stdc++.h>
#define ll long long
using namespace std;
int n, vis[10], mp[10][10], sz, sqz, xz[10], yz[10];
bool a[10][10][10];
int pr(){
	for(int i = 1; i <= sqz; ++i,puts(""))
		for(int j = 1; j <= sqz; ++j)
			printf("%d", mp[i][j]);
	return 0;
}
bool canput(int x, int y, int t){
	--x,--y;
	for(int i = 1; i <= xz[t]; ++i)
		for(int j = 1; j <= yz[t]; ++j){
			if(x+i>sqz&&a[t][i][j]) return 0;
			if(y+j>sqz&&a[t][i][j]) return 0;
			if(a[t][i][j]&&mp[x+i][y+j]) return 0;
		}
	return 1;
}
bool check(){
	for(int i = 1; i <= n; ++i)
		if(!vis[i]) return 0;
	return 1;
}
void dfs(){
	if(check())exit(pr());
	for(int i = 1; i <= sqz; ++i)
	for(int j = 1; j <= sqz; ++j)
	for(int k = 1; k <= n; ++k){
		if(!vis[k]&&canput(i, j, k)){
			vis[k] = 1;
			for(int i_ = 1; i_ <= xz[k]; ++i_)
				for(int j_ = 1; j_ <= yz[k]; ++j_){
					if(a[k][i_][j_])
						mp[i+i_-1][j+j_-1] = k;
				}
			dfs();
			vis[k] = 0;
			for(int i_ = 1; i_ <= xz[k]; ++i_)
				for(int j_ = 1; j_ <= yz[k]; ++j_){
					if(a[k][i_][j_])
						mp[i+i_-1][j+j_-1] = 0;
				}
		}
	}
}
int main(){
	freopen("puzzling.in", "r", stdin);
	freopen("puzzling.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i){
		scanf("%d%d", &xz[i], &yz[i]);
		for(int j = 1; j <= xz[i]; ++j)
			for(int k = 1; k <= yz[i]; ++k){
				scanf("%1d", &a[i][j][k]);
				sz += a[i][j][k];
			}
	}
	switch(sz){
		case 169:sqz=13;break;
		case 144:sqz=12;break;
		case 121:sqz=11;break;
		case 100:sqz=10;break;
		case 81:sqz=9;break;
		case 64:sqz=8;break;
		case 49:sqz=7;break;
		case 36:sqz=6;break;
		case 25:sqz=5;break;
		case 16:sqz=4;break;
		case 9:sqz=3;break;
		case 4:sqz=2;break;
		case 1:sqz=1;break;
		default:return puts("No solution possible")&0;
	}
	dfs();
	return puts("No solution possible")&0;
}
//puts("10\n3 3\n100\n101\n111\n5 3\n110\n100\n100\n100\n111\n2 5\n11111\n10000\n3 2\n10\n10\n11\n5 2\n01\n01\n11\n10\n10\n2 4\n1110\n0011\n4 5\n00011\n00011\n11001\n01111\n2 5\n10000\n11111\n2 3\n011\n111\n3 4\n1000\n1111\n1100\n")
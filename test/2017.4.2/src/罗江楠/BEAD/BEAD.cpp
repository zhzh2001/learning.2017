#include <bits/stdc++.h>
#define ll long long
using namespace std;
int n, m, rin[105], rout[105], vis[105];
vector<int> in[105], out[105];
void dfs1(int x){
	vis[x] = 1; rout[x] = out[x].size();
	for(int i = 0; i < out[x].size(); i++){
		if(!vis[out[x][i]]) dfs1(out[x][i]);
		rout[x] += rout[out[x][i]];
	}
}
void dfs2(int x){
	vis[x] = 1; rin[x] = in[x].size();
	for(int i = 0; i < in[x].size(); i++){
		if(!vis[in[x][i]]) dfs2(in[x][i]);
		rin[x] += rin[in[x][i]];
	}
}
int main(){
	freopen("BEAD.in", "r", stdin);
	freopen("BEAD.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1,x,y; i <= m; i++){
		scanf("%d%d", &x, &y);
		in[x].push_back(y);
		out[y].push_back(x);
	}
	for(int i = 1; i <= n; i++)
		if(!vis[i]) dfs1(i);
	memset(vis, 0, sizeof vis);
	for(int i = 1; i <= n; i++)
		if(!vis[i]) dfs2(i);
	int l = n-1>>1, r = n-l-1, ans=0;
	for(int i = 1; i <= n; i++)
		if(rin[i]>l||rout[i]>r) ans++;
	printf("%d\n", ans);
	return 0;
}

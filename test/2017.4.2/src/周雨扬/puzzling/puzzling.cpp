#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int mp[15][15],n,m,tot;
char ch[15];
struct data{
	int h,w,x,yl;
	int mp[6][6];
}a[10];
void dfs(int x){
	if (x>n){
		for (int i=1;i<=m;i++){
			for (int j=1;j<=m;j++) printf("%d",mp[i][j]);
			printf("\n");
		}
		exit(0);
	}
	for (int i=1;i+a[x].w-1<=m;i++)
		for (int j=1;j+a[x].h-1<=m;j++){
			int fl=1;
			for (int k=i;k<=i+a[x].w-1&&fl;k++)
				for (int l=j;l<=j+a[x].h-1&&fl;l++)
					if (a[x].mp[k-i+1][l-j+1]&&mp[k][l]) fl=0;
			if (!fl) continue;
			for (int k=i;k<=i+a[x].w-1&&fl;k++)
				for (int l=j;l<=j+a[x].h-1&&fl;l++)
					if (a[x].mp[k-i+1][l-j+1]) mp[k][l]=x;
			dfs(x+1);
			for (int k=i;k<=i+a[x].w-1&&fl;k++)
				for (int l=j;l<=j+a[x].h-1&&fl;l++)
					if (a[x].mp[k-i+1][l-j+1]) mp[k][l]=0;
		}
}
int main(){
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&a[i].w,&a[i].h);
		for (int j=1;j<=a[i].w;j++){
			scanf("%s",ch+1);
			for (int k=1;k<=a[i].h;k++){
				a[i].mp[j][k]=ch[k]-'0';
				tot+=a[i].mp[j][k];
			}
		} 
	}
	m=int(sqrt(tot)+1e-9);
	if (m*m!=tot){
		printf("No solution possible");
		return 0;
	}
	dfs(1);
	printf("No solution possible");
}

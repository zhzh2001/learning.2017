#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
char s[110];
int f[10005],a[10005];
int len,sum,tot,tmp,x,y,n;
int main(){
	freopen("format.in","r",stdin);
	freopen("format.out","w",stdout);
	scanf("%d",&len);
	while (scanf("%s",s+1)!=EOF){
		a[++n]=strlen(s+1);
	}
	memset(f,10,sizeof(f));
	f[0]=0;
	for (int i=1;i<=n;i++){
		f[i]=f[i-1]+((a[i]==len)?0:500);
		if (i==1) continue;
		sum=a[i]; tot=0;
		for (int j=i-2;j>=0;j--){
			sum+=a[j+1]+1;
			tot++;
			if (sum>len) continue;
			tmp=len-sum;
			x=tmp%tot;
			y=tot-x;
			f[i]=min(f[i],f[j]+y*(tmp/tot)*(tmp/tot)+x*(tmp/tot+1)*(tmp/tot+1));
		}
	}
	printf("Minimal badness is %d",f[n]);
}

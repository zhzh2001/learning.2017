#include <iostream>
#include <cstdio>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 8;
int n, sz;
struct data {
	int w, h;
	int c[N][N];
} dat[N];
int mat[15][15];
bool vis[N];

void debug();

inline bool check(int x, int y, int k) {
	for (int i = 1; i <= dat[k].h; i++)
		for (int j = 1; j <= dat[k].w; j++)
			if (dat[k].c[i][j] && mat[x + i - 1][y + j - 1]) {
				return false;
			}
	return true;
}

inline void plc(int x, int y, int k) {
	// bool flag = false;
	// if (k == 2)  {
		// flag = true;
		// cout << dat[k].c[1][3] << endl;
	// }
	for (int i = 1; i <= dat[k].h; i++)
		for (int j = 1; j <= dat[k].w; j++) {
			if (dat[k].c[i][j]) {
				// if (flag) cout << i << ' ' << j << endl;
				mat[x + i - 1][y + j - 1] = k;
				// if (flag) debug();
			}
		}
}

inline void unplc(int x, int y, int k) {
	for (int i = 1; i <= dat[k].h; i++)
		for (int j = 1; j <= dat[k].w; j++)
			if (dat[k].c[i][j])
				mat[x + i - 1][y + j - 1] = 0;
}

bool flag;

void debug() {
	for (int i = 1; i <= sz; i++) {
		for (int j = 1; j <= sz; j++)
			printf("%d", mat[i][j]);
		puts("");
	}
	puts("");
}

void dfs(int x, int y, int dep) {
	// cout << x << ' ' << y << ' ' << dep << ' ' << mat[x][y] << endl;
	if (x == sz && y == sz) {
		if (mat[x][y] && dep == ::n) {
			for (int i = 1; i <= sz; i++) {
				for (int j = 1; j <= sz; j++)
					printf("%d", mat[i][j]);
				puts("");
			}
			exit(0);
		} else
			return;
	}
	for (int i = 1; i <= n; i++) {
		if (!vis[i]) {
			// flag = true;
			if (x + dat[i].h - 1 > sz || y + dat[i].w - 1 > sz || !check(x, y, i)) continue;
			plc(x, y, i);
			vis[i] = true;
			// debug();
			if (y == sz) dfs(x + 1, 1, dep + 1);
			else dfs(x, y + 1, dep + 1);
			vis[i] = false;
			unplc(x, y, i);
		}
	}
	if (mat[x][y] != 0) {
		if (y == sz) dfs(x + 1, 1, dep);
		else dfs(x, y + 1, dep);
	}
}

int main() {
	freopen("puzzling.in", "r", stdin);
	freopen("puzzling.out", "w", stdout);
	read(n);
	char str[20];
	int cnt = 0;
	for (int i = 1; i <= n; i++) {
		read(dat[i].h);
		read(dat[i].w);
		for (int j = 1; j <= dat[i].h; j++) {
			scanf("%s", str + 1);
			for (int k = 1; k <= dat[i].w; k++) {
				dat[i].c[j][k] = str[k] - '0';
				if (dat[i].c[j][k] == 1) cnt++;
			}
		}
		// for (int j = 1; j <= dat[i].h; j++) {
			// for (int k = 1; k <= dat[i].w; k++)
				// cout << dat[i].c[j][k];
			// cout << endl;
		// }
	}
	sz = sqrt(cnt);
	// cout << sz << endl;
	if (sz * sz != cnt) {
		puts("No solution possible");
		return 0;
	}
	dfs(1, 1, 0);
	puts("No solution possible");
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 105;
int n, m, x, y;
int f[N], g[N], a[N][N], b[N][N];
bool vis1[N];

void dfs1(int x) {
	f[x] = 1;
	for (int i = 1; i <= n; i++) {
		if (a[x][i] && !vis1[i]) {
			vis1[i] = true;
			dfs1(i);
			f[x] += f[i];
		}
	}
}

void dfs2(int x) {
	g[x] = 1;
	for (int i = 1; i <= n; i++) {
		if (b[x][i] && !vis1[i]) {
			vis1[i] = true;
			dfs2(i);
			g[x] += g[i];
		}
	}
}

int main() {
	freopen("BEAD.IN", "r", stdin);
	freopen("BEAD.OUT", "w", stdout);
	read(n), read(m);
	for (int i = 1; i <= m; i++) {
		read(x), read(y);
		a[x][y] = true;
		b[y][x] = true;
	}
	int cnt = 0;
	int hf1 = (n + 1) / 2;
	int hf2 = (n + 2) / 2;
	// cout << hf1 << ' ' << hf2 << endl;
	for (int i = 1; i <= n; i++) {
		memset(vis1, 0, sizeof vis1);
		dfs1(i);
		if (f[i] > hf2) cnt++;
		// cout << f[i] << ' ';
	}
	// cout << endl;
	for (int i = 1; i <= n; i++) {
		memset(vis1, 0, sizeof vis1);
		dfs2(i);
		if (g[i] > hf1) cnt++;
		// cout << g[i] << ' ';
	}
	// cout << endl;
	printf("%d\n", cnt);
	return 0;
}
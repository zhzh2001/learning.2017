#include <iostream>
#include <cstdio>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int N = 85;
int mat[10][N][N];
int n, m, t, u, v, w;
int ans = 0x3f3f3f3f;
bool vis[N];

void dfs(int x, int res = 0, int tim = 0) {
	if (x == 0) {
		ans = min(res, ans);
		return;
	}
	int now = tim;
	for (int i = 0; i < t; i++) {
		for (int j = 0; j <= n; j++) {
			if (mat[(now + i) % t][x][j]) {
				vis[j] = true;
				dfs(j, res + 1 + t, (now + i + 1) % t);
				vis[j] = false;
			}
		}
	}
}

int main() {
	freopen("house.in", "r", stdin);
	freopen("house.out", "w", stdout);
	read(n), read(m), read(t);
	for (int i = 0; i < t; i++) {
		for (int j = 1; j <= m; j++) {
			read(u), read(v);
			mat[i][u][v] = mat[i][v][u] = 1;
		}
	}
	vis[1] = true;
	// dfs(1);
	if (ans < 0x3f3f3f3f) {
		printf("%d\n", ans);
	} else {
		puts("Poor Z4!");
	}
}
#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstdlib>
using namespace std;
struct da
{
	int size,x,y,z;
	char c[10][10];
	void cs()
	{
		size=0;
		for (int i=1;i<=x;++i)
			for (int j=1;j<=y;++j)
				size+=c[i][j]-'0';
		for (int j=1;j<=y;++j)
			if (c[1][j]=='1')
			{
				z=j;
				break;
			}
	}
	bool used;
}a[10];
int size,len,f[32][32],n;
void sc()
{
	for (int i=1;i<=len;++i)
	{
		for (int j=1;j<=len;++j)
			putchar(f[i][j]+'0');
		putchar('\n');
	}
	exit(0);
}
void dfs()
{
	int x=0,y=0;
	for (int i=1;i<=len&&x==0;++i)
		for (int j=1;j<=len;++j)
			if (f[i][j]==0)
			{
				x=i;
				y=j;
				break;
			}
	if (x==0)
		sc();
	for (int i=1;i<=n;++i)
		if (!a[i].used)
		{
			int xx=x-1,yy=y-a[i].z;
			bool b=xx>=0&&yy>=0&&xx+a[i].x<=len&&yy+a[i].y<=len;
			for (int j=1;j<=a[i].x&&b;++j)
				for (int k=1;k<=a[i].y;++k)
					if (a[i].c[j][k]=='1'&&f[xx+j][yy+k]!=0)
					{
						b=false;
						break;
					}
			if (b)
			{
				for (int j=1;j<=a[i].x;++j)
					for (int k=1;k<=a[i].y;++k)
						if (a[i].c[j][k]=='1')
							f[xx+j][yy+k]=i;
				a[i].used=true;
				dfs();
				for (int j=1;j<=a[i].x;++j)
					for (int k=1;k<=a[i].y;++k)
						if (a[i].c[j][k]=='1')
							f[xx+j][yy+k]=0;
				a[i].used=false;
			}
		}
}
int main()
{
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i)
	{
		scanf("%d%d",&a[i].x,&a[i].y);
		for (int j=1;j<=a[i].x;++j)
			scanf("%s",a[i].c[j]+1);
		a[i].cs();
		size+=a[i].size;
	}
	len=(int)sqrt(size);
	if (len*len!=size)
	{
		printf("No solution possible");
		return 0;
	}
	dfs();
	printf("No solution possible");
	return 0;
}

	
#include<iostream>
#include<cstdio>
using namespace std;
const int inf=(int)1e9+7;
int to[100005],ti[100005],pr[100005],la[105],n,m,t,dis[105],in[105],l,r,que[10005],x,y,i,j,lc;
void add(int u,int v,int t)
{
	to[++l]=v;
	pr[l]=la[x];
	ti[l]=t;
	la[x]=l;
}
int main()
{
	freopen("house.in","r",stdin);
	freopen("house.out","w",stdout);
	scanf("%d%d%d",&n,&m,&t);
	for (i=1;i<=t;++i)
		for (j=1;j<=m;++j)
		{
			scanf("%d%d",&x,&y);
			add(x,y,i);
		}
	for (i=0;i<=n;++i)
		dis[i]=inf;
	dis[1]=0;
	in[1]=1;
	que[1]=1;
	l=0;
	r=1;
	while (l!=r)
	{
		l=l%10000+1;
		for (i=la[que[l]];i;i=pr[i])
		{
			lc=(ti[i]%t-dis[que[l]]%t+t-1)%t+1;
			if (lc+dis[que[l]]<=dis[to[i]])
			{
				dis[to[i]]=lc+dis[que[l]];
				if (!in[to[i]])
				{
					in[to[i]]=1;
					r=r%10000+1;
					que[r]=to[i];
				}
			}
		}
		in[que[l]]=0;
	}
	if (dis[0]!=inf)
		printf("%d",dis[0]);
	else
		printf("Poor Z4!");
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=10005;

int word[N];
int l,tot,n,m,x,y,we;
int num,k,ans,Mn=1000000000,Mx;

int main(){
	freopen("FORMAT.in","r",stdin);
	freopen("FORMAT.out","w",stdout);
	l=read();
	char c[105];
	while(scanf("%s",c+1)!=EOF){
		int len=strlen(c+1); Mx=max(Mx,len);
		word[++m]=len; tot+=len;
	}
	for(y=Mx;y<=l;y++){
		num=0;k=0;ans=0;
		for(int i=1;i<=m;i++){
			if(word[i]+num<=y&&word[i]+num+k<=l&&i<m){
				k++; num+=word[i];
			}
			else{
				if(word[i]+num+k<=l){
					int p=num+k-1;
					num+=word[i]; k++;
					int q=p+word[i]+1;
					if(q-y<=p-y){
						k++; num+=word[i];
					}
				}
				if(k==1&&num<l) ans+=500;
				else{
					int p=l-num; int q=p/(k-1); p=p-q*(k-1);
					q--;
					ans+=(q*q)*(k-1); ans+=(2*q+1)*p;
//					printf("#%d %d %d\n",num,i,k);
				}
				k=0;num=0;
			}
		}
//		printf("##%d\n",ans);
		if(ans<Mn){
			we=y;
			Mn=ans;
		}
	}
	printf("Minimal badness is %d.\n",Mn);
	return 0;
}

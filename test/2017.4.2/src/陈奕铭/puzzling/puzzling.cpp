#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=15,M=6;
int mp[N][N];
struct gra{
	int x,y;
	int mp[M][M];
}g[M];
int n,m;

bool pd(int x,int y,int p){
	for(int i=x;i<=x+g[p].x-1;i++)
		for(int j=y;j<=y+g[p].y-1;j++)
			if(g[p].mp[i-x+1][j-y+1])
				if(mp[i][j]!=0) return false;
	return true;
}

void tian(int x,int y,int p,int c){
	for(int i=x;i<=x+g[p].x-1;i++)
		for(int j=y;j<=y+g[p].y-1;j++)
			if(g[p].mp[i-x+1][j-y+1])
				mp[i][j]=c;
}

void dfs(int p){
	if(p==m+1){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++)
				putchar(mp[i][j]+'0');
			puts("");
		}
		exit(0);
	}
	for(int i=1;i<=n-g[p].x+1;i++){
		for(int j=1;j<=n-g[p].y+1;j++){
			if(pd(i,j,p)){
				tian(i,j,p,p);
				dfs(p+1);
				tian(i,j,p,0);
			}
		}
	}
}

int main(){
	freopen("puzzling.in","r",stdin);
	freopen("puzzling.out","w",stdout);
	m=read();int tot=0;
	char ch[M];
	for(int i=1;i<=m;i++){
		g[i].x=read(); g[i].y=read();
		for(int j=1;j<=g[i].x;j++){
			scanf("%s",ch+1);
			for(int z=1;z<=g[i].y;z++){
				if(ch[z]=='1'){ g[i].mp[j][z]=1; tot++; }
				else g[i].mp[j][z]=0;
			}
		}
	}
	n=(int)sqrt(tot);
	if(n*n!=tot){ printf("No solution possible\n"); return 0;}
	dfs(1);
	printf("No solution possible\n");
	return 0;
}

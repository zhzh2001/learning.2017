#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=105;
bool mp[N][N];
vector<int> edge1[N];
vector<int> edge2[N];
bool vis1[N],vis2[N];
int n,m,sum1,sum2,tot,ans;

void dfs1(int x){
	int len=edge1[x].size();
	for(int i=0;i<len;i++){
		int v=edge1[x][i];
		if(!vis1[v]){
			vis1[v]=true;
			sum1++;
			dfs1(v);
		}
	}
}

void dfs2(int x){
	int len=edge2[x].size();
	for(int i=0;i<len;i++){
		int v=edge2[x][i];
		if(!vis2[v]){
			vis2[v]=true;
			sum2++;
			dfs2(v);
		}
	}
}

int main(){
	freopen("BEAD.in","r",stdin);
	freopen("BEAD.out","w",stdout);
	n=read(); m=read(); tot=(n+1)/2-1;
	for(int i=1;i<=m;i++){
		int a=read(),b=read();
		if(!mp[a][b]){
			mp[a][b]=true;
			edge1[a].push_back(b);
			edge2[b].push_back(a);
		}
	}
	for(int i=1;i<=n;i++){
		memset(vis1,0,sizeof vis1);
		memset(vis2,0,sizeof vis2);
		vis1[i]=true; sum1=0; dfs1(i);
		vis2[i]=true; sum2=0; dfs2(i);
//		printf("##%d %d\n",sum1,sum2);
		if(sum1>tot||sum2>tot) ans++;
	}
	printf("%d",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#include<cmath>
#define Ll long long
using namespace std;
struct st{
	int a[6][6],n,m;
}s[6];
int a[6][6],f[6][6];
int n,sum,len;
bool p,vi[6];
char c;
bool check(int x,int y,int k){
	int nn=s[k].n;
	int mm=s[k].m;
	if(nn+x-1>len||mm+y-1>len)return 0;
	for(int i=1;i<=nn;i++)
	for(int j=1;j<=mm;j++)
	if(a[i+x-1][j+y-1]&&s[k].a[i][j])return 0;
	return 1;	
}
void init(int x,int y,int k){
	int nn=s[k].n;
	int mm=s[k].m;
	for(int i=1;i<=nn;i++)
	for(int j=1;j<=mm;j++)
		if(s[k].a[i][j])a[x+i-1][y+j-1]=k;
}
void outit(int x,int y,int k){
	int nn=s[k].n;
	int mm=s[k].m;
	for(int i=1;i<=nn;i++)
	for(int j=1;j<=mm;j++)
		if(s[k].a[i][j])a[x+i-1][y+j-1]=0;
}
void find(){
	for(int i=1;i<=n;i++)if(!vi[i])return;
	for(int i=1;i<=len;i++){
	for(int j=1;j<=len;j++)printf("%d",a[i][j]);printf("\n");}
	exit(0);
}
void dfs(int x,int y){
	if(y>len)x++,y=1;
	if(x>len){find();return;}
	if(a[x][y])dfs(x,y+1);
	for(int i=1;i<=n;i++)
		if(!vi[i]&&check(x,y,i)){
			vi[i]=1;
			init(x,y,i);
			dfs(x,y);
			outit(x,y,i);
			vi[i]=0;
		}
}
void read(int k){
	int n,m;
	memset(f,0,sizeof f);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	for(int j=1;j<=m;j++){
		cin>>c;
		f[i][j]=c-48;
		if(f[i][j])sum++;
	}
	int n1,n2,m1,m2;
	n1=n2=m1=m2=0;
	for(int i=1;i<=n;i++)if(!n1)
		for(int j=1;j<=m;j++)
			if(!n1&&f[i][j])n1=i;
	for(int i=n;i>=1;i--)if(!n2)
		for(int j=1;j<=m;j++)
			if(!n2&&f[i][j])n2=i;
	for(int j=1;j<=m;j++)if(!m1)
		for(int i=1;i<=n;i++)
			if(!m1&&f[i][j])m1=j;
	for(int j=m;j>=1;j--)if(!m2)
		for(int i=1;i<=n;i++)
			if(!m2&&f[i][j])m2=j;
	s[k].n=n2-n1+1;
	s[k].m=m2-m1+1;
	for(int i=n1;i<=n2;i++)
	for(int j=m1;j<=m2;j++)
		s[k].a[i-n1+1][j-m1+1]=f[i][j];
}
int main()
{
	freopen("puzzling.IN","r",stdin);
	freopen("puzzling.OUT","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)read(i);
	len=sqrt(sum);
	if(len*len!=sum){
		printf("No solution possible");return 0;
	}
	dfs(1,1);
	printf("No solution possible");return 0;
}

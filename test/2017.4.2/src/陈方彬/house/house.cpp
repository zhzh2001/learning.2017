#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#include<cmath>
#define Ll long long
using namespace std;
bool a[11][81][81],in[81];
int h[81],q[40000],l,r;
int n,m,K,x,y;
int get(int t,int k){
	t=t%K;
	if(!t)t=K;
	if(k<t)k+=K;
	return k-t;
}
void spfa(int S){
	for(int i=0;i<=n;i++)h[i]=1e5;
	h[S]=0; l=0; r=1; q[1]=S; in[S]=1;
	while(r>l){
		int x=q[++l];
		for(int k=1;k<=K;k++)
		for(int i=0;i<=n;i++)
			if(a[k][x][i]&&h[i]>h[x]+1+get(h[x]+1,k)){
				h[i]=h[x]+1+get(h[x]+1,k);
				if(!in[i]){
					in[i]=1;
					q[++r]=i;
				}
			}
		in[x]=0;
	}
}
int main()
{
	freopen("house.IN","r",stdin);
	freopen("house.OUT","w",stdout);
	scanf("%d%d%d",&n,&m,&K);
	for(int k=1;k<=K;k++)
	for(int i=1;i<=m;i++){
		scanf("%d%d",&x,&y);
		a[k][x][y]=a[k][y][x]=1;
	}
	spfa(1);
	if(h[0]!=1e5)printf("%d",h[0]);else printf("Poor Z4!");
//	for(int i=0;i<=n;i++)cout<<i<<' '<<h[i]<<endl;
}

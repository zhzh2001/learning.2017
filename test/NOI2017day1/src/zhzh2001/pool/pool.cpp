#include <fstream>
using namespace std;
ifstream fin("pool.in");
ofstream fout("pool.out");
const int mod = 998244353;
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % mod;
		a = a * a % mod;
	} while (b /= 2);
	return ans;
}
int main()
{
	int n, k, x, y;
	fin >> n >> k >> x >> y;
	int inv = qpow(y, mod - 2), q = 1ll * x * inv % mod, q2 = 1ll * (y - x) * inv % mod;
	fout << 1ll * qpow(q, k) * q2 % mod << endl;
	return 0;
}
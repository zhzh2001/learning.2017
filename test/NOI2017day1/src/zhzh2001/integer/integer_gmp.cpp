#include <fstream>
#include <gmpxx.h>
using namespace std;
ifstream fin("integer.in");
ofstream fout("integer.ans");
int main()
{
	int n, t1, t2, t3;
	fin >> n >> t1 >> t2 >> t3;
	mpz_class val = 0;
	while (n--)
	{
		int opt;
		fin >> opt;
		if (opt == 1)
		{
			int a, b;
			fin >> a >> b;
			mpz_class delta;
			mpz_mul_2exp(delta.get_mpz_t(), mpz_class(a).get_mpz_t(), b);
			val += delta;
		}
		else
		{
			int k;
			fin >> k;
			mpz_class t;
			mpz_tdiv_q_2exp(t.get_mpz_t(), val.get_mpz_t(), k);
			fout << t % 2 << endl;
		}
	}
	return 0;
}
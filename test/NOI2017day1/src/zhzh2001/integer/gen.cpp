#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
ofstream fout("integer.in");
const int n = 5;
int main()
{
	minstd_rand gen(GetTickCount());
	fout << n << ' ' << 0 << ' ' << 0 << ' ' << 0 << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> d(1, 2);
		if (d(gen) == 1)
		{
			uniform_int_distribution<> da(0, 10), db(0, n);
			fout << 1 << ' ' << da(gen) << ' ' << db(gen) << endl;
		}
		else
		{
			uniform_int_distribution<> dk(0, n);
			fout << 2 << ' ' << dk(gen) << endl;
		}
	}
	return 0;
}
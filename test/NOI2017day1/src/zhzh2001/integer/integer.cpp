#include <fstream>
#include <cstring>
#include <algorithm>
using namespace std;
ifstream fin("integer.in");
ofstream fout("integer.out");
struct bigint
{
	static const int L = 280000;
	int len, dig[L];
	bigint(int x = 0, int offset = 0)
	{
		memset(dig, 0, sizeof(dig));
		len = offset;
		do
			dig[++len] = x & 1;
		while (x /= 2);
	}
	bigint operator+(const bigint &rhs) const
	{
		bigint ret;
		ret.len = max(len, rhs.len);
		for (int i = 1; i <= ret.len; i++)
		{
			ret.dig[i] += dig[i] + rhs.dig[i];
			ret.dig[i + 1] += ret.dig[i] / 2;
			ret.dig[i] %= 2;
		}
		if (ret.dig[ret.len + 1])
			ret.len++;
		return ret;
	}
	bigint &operator+=(const bigint &rhs)
	{
		return *this = *this + rhs;
	}
	bigint operator-(const bigint &rhs) const
	{
		bigint ret;
		ret.len = len;
		int underflow = 0;
		for (int i = 1; i <= len; i++)
		{
			ret.dig[i] = dig[i] - rhs.dig[i] - underflow;
			if (ret.dig[i] < 0)
			{
				ret.dig[i] += 2;
				underflow = 1;
			}
			else
				underflow = 0;
		}
		while (ret.len > 1 && ret.dig[ret.len] == 0)
			ret.len--;
		return ret;
	}
	bigint &operator-=(const bigint &rhs)
	{
		return *this = *this - rhs;
	}
};
int main()
{
	int n, t1, t2, t3;
	fin >> n >> t1 >> t2 >> t3;
	bigint val;
	while (n--)
	{
		int opt;
		fin >> opt;
		if (opt == 1)
		{
			int a, b;
			fin >> a >> b;
			bool neg = false;
			if (a < 0)
			{
				neg = true;
				a = -a;
			}
			bigint delta(a, b);
			if (neg)
				val -= delta;
			else
				val += delta;
		}
		else
		{
			int k;
			fin >> k;
			fout << val.dig[k + 1] << endl;
		}
	}
	return 0;
}
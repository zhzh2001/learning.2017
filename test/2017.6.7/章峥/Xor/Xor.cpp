#include<fstream>
#include<vector>
#include<utility>
using namespace std;
ifstream fin("Xor.in");
ofstream fout("Xor.out");
const int N=100005;
int val[N],ch[N*32][2];
vector<pair<int,int> > mat[N];
bool vis[N];
void dfs(int k,int now)
{
	val[k]=now;
	vis[k]=true;
	for(vector<pair<int,int> >::iterator it=mat[k].begin();it!=mat[k].end();++it)
		if(!vis[it->first])
			dfs(it->first,now^(it->second));
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	dfs(1,0);
	int cc=0;
	for(int i=1;i<=n;i++)
	{
		int now=0;
		for(int j=30;j>=0;j--)
		{
			bool d=val[i]&(1<<j);
			if(!ch[now][d])
				ch[now][d]=++cc;
			now=ch[now][d];
		}
	}
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		int now=0,tans=0;
		for(int j=30;j>=0;j--)
		{
			bool d=~val[i]&(1<<j);
			if(ch[now][d])
			{
				tans+=1<<j;
				now=ch[now][d];
			}
			else
				now=ch[now][!d];
		}
		ans=max(ans,tans);
	}
	fout<<ans<<endl;
	return 0;
}
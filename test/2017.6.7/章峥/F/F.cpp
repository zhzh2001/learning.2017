#include<cstdio>
#include<cctype>
using namespace std;
const int BUFSIZE=2.5e7;
char bufin[BUFSIZE],bufout[BUFSIZE],*pin=bufin,*pout=bufout;
inline void read(int& x)
{
	for(;isspace(*pin);pin++);
	x=0;
	for(;isdigit(*pin);pin++)
		x=x*10+*pin-'0';
}
int dig[10];
inline void writeln(int x)
{
	int len=0;
	do
		dig[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*pout++=dig[len]+'0';
	*pout++='\n';
}
int main()
{
	FILE *f=fopen("F.in","r");
	fread(bufin,1,BUFSIZE,f);
	fclose(f);
	int x;
	for(;;)
	{
		read(x);
		if(x==0)
			break;
		if(x<=100)
			writeln(91);
		else
			writeln(x-10);
	}
	f=fopen("F.out","w");
	fwrite(bufout,1,pout-bufout,f);
	fclose(f);
	return 0;
}
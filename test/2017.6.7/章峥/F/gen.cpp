#include<bits/stdc++.h>
using namespace std;
ofstream fout("F.in");
const int T=2500000;
int main()
{
	minstd_rand gen(time(NULL));
	for(int i=1;i<=T;i++)
	{
		uniform_int_distribution<> dx(1,1000000);
		fout<<dx(gen)<<endl;
	}
	fout<<0<<endl;
	return 0;
}
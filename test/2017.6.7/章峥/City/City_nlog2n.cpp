#include<fstream>
#include<algorithm>
#include<utility>
using namespace std;
ifstream fin("City.in");
ofstream fout("City.out");
const int N=100005;
struct edge
{
	int u,v,w,id;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[N];
int f[N],sz[N];
long long cnt[N];
bool d[N];
int getf(int x)
{
	return f[x]==x?x:getf(f[x]);
}
pair<long long,int> a[N];
void dfs(int l,int r)
{
	if(l==r)
	{
		int su=sz[getf(e[l].u)],sv=sz[getf(e[l].v)];
		cnt[e[l].id]+=(long long)(su+sv)*(su+sv-1)-(long long)su*(su-1)-(long long)sv*(sv-1);
	}
	else
	{
		int mid=(l+r)/2;
		for(int i=mid+1;i<=r;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v),su=sz[ru],sv=sz[rv];
			d[i]=su<sv;
			if(d[i])
			{
				f[ru]=rv;
				sz[rv]+=su;
			}
			else
			{
				f[rv]=ru;
				sz[ru]+=sv;
			}
		}
		dfs(l,mid);
		for(int i=r;i>mid;i--)
		{
			int r=getf(e[i].u);
			if(d[i])
			{
				f[e[i].u]=e[i].u;
				sz[r]-=sz[e[i].u];
			}
			else
			{
				f[e[i].v]=e[i].v;
				sz[r]-=sz[e[i].v];
			}
		}
		for(int i=l;i<=mid;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v),su=sz[ru],sv=sz[rv];
			d[i]=su<sv;
			if(d[i])
			{
				f[ru]=rv;
				sz[rv]+=su;
			}
			else
			{
				f[rv]=ru;
				sz[ru]+=sv;
			}
		}
		dfs(mid+1,r);
		for(int i=mid;i>=l;i--)
		{
			int r=getf(e[i].u);
			if(d[i])
			{
				f[e[i].u]=e[i].u;
				sz[r]-=sz[e[i].u];
			}
			else
			{
				f[e[i].v]=e[i].v;
				sz[r]-=sz[e[i].v];
			}
		}
	}
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		fin>>e[i].u>>e[i].v>>e[i].w;
		e[i].id=i;
	}
	sort(e+1,e+n);
	for(int i=1;i<=n;i++)
	{
		f[i]=i;
		sz[i]=1;
	}
	for(int i=1;i<n;)
	{
		int j;
		for(j=i;j<n&&e[j].w==e[i].w;j++);
		dfs(i,j-1);
		for(;i<j;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v),su=sz[ru],sv=sz[rv];
			if(su<sv)
			{
				f[ru]=rv;
				sz[rv]+=su;
			}
			else
			{
				f[rv]=ru;
				sz[ru]+=sv;
			}
		}
	}
	for(int i=1;i<n;i++)
		a[i]=make_pair(cnt[i],i);
	sort(a+1,a+n);
	fout<<a[n-1].first;
	int j;
	for(j=n-1;j&&a[j].first==a[n-1].first;j--);
	fout<<' '<<n-1-j<<endl;
	for(j++;j<n;j++)
		fout<<a[j].second<<' ';
	fout<<endl;
	return 0;
}
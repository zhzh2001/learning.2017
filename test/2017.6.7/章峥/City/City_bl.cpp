#include<fstream>
#include<vector>
#include<utility>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("City.in");
ofstream fout("City.ans");
const int N=100005;
struct edge
{
	int v,w,id;
	edge(int v,int w,int id):v(v),w(w),id(id){}
};
vector<edge> mat[N];
struct node
{
	int v,w;
	vector<int> e;
};
int cnt[N];
bool vis[N];
pair<int,int> a[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(edge(v,w,i));
		mat[v].push_back(edge(u,w,i));
	}
	for(int i=1;i<=n;i++)
	{
		queue<node> Q;
		node k;
		k.v=i;k.w=0;
		Q.push(k);
		fill(vis+1,vis+n+1,false);
		vis[i]=true;
		while(!Q.empty())
		{
			node k=Q.front();Q.pop();
			for(vector<edge>::iterator it=mat[k.v].begin();it!=mat[k.v].end();++it)
				if(!vis[it->v])
				{
					vis[it->v]=true;
					node now=k;
					now.v=it->v;
					if(it->w>now.w)
					{
						now.w=it->w;
						now.e.clear();
					}
					if(it->w==now.w)
						now.e.push_back(it->id);
					for(vector<int>::iterator it=now.e.begin();it!=now.e.end();++it)
						cnt[*it]++;
					Q.push(now);
				}
		}
	}
	for(int i=1;i<n;i++)
		a[i]=make_pair(cnt[i],i);
	sort(a+1,a+n);
	fout<<a[n-1].first;
	int j;
	for(j=n-1;j&&a[j].first==a[n-1].first;j--);
	fout<<' '<<n-1-j<<endl;
	for(j++;j<n;j++)
		fout<<a[j].second<<' ';
	fout<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
#include<utility>
#include<vector>
#include<queue>
using namespace std;
ifstream fin("City.in");
ofstream fout("City.out");
const int N=100005;
struct edge
{
	int v,w,id;
	edge(int v,int w,int id):v(v),w(w),id(id){}
};
vector<edge> mat[N];
struct node
{
	int v,w;
	vector<int> e;
};
bool vis[N];

struct Edge
{
	int u,v,w,id;
	bool operator<(const Edge& rhs)const
	{
		return w<rhs.w;
	}
}e[N];
int f[N],sz[N];
long long cnt[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
pair<long long,int> a[N];
int main()
{
	int n;
	fin>>n;
	if(n<=1000)
	{
		for(int i=1;i<n;i++)
		{
			int u,v,w;
			fin>>u>>v>>w;
			mat[u].push_back(edge(v,w,i));
			mat[v].push_back(edge(u,w,i));
		}
		for(int i=1;i<=n;i++)
		{
			queue<node> Q;
			node k;
			k.v=i;k.w=0;
			Q.push(k);
			fill(vis+1,vis+n+1,false);
			vis[i]=true;
			while(!Q.empty())
			{
				node k=Q.front();Q.pop();
				for(vector<edge>::iterator it=mat[k.v].begin();it!=mat[k.v].end();++it)
					if(!vis[it->v])
					{
						vis[it->v]=true;
						node now=k;
						now.v=it->v;
						if(it->w>now.w)
						{
							now.w=it->w;
							now.e.clear();
						}
						if(it->w==now.w)
							now.e.push_back(it->id);
						for(vector<int>::iterator it=now.e.begin();it!=now.e.end();++it)
							cnt[*it]++;
						Q.push(now);
					}
			}
		}
	}
	else
	{
		for(int i=1;i<n;i++)
		{
			fin>>e[i].u>>e[i].v>>e[i].w;
			e[i].id=i;
		}
		sort(e+1,e+n);
		for(int i=1;i<=n;i++)
		{
			f[i]=i;
			sz[i]=1;
		}
		for(int i=1;i<n;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v),su=sz[ru],sv=sz[rv];
			cnt[e[i].id]+=(long long)(su+sv)*(su+sv-1)-(long long)su*(su-1)-(long long)sv*(sv-1);
			f[ru]=rv;
			sz[rv]+=sz[ru];
		}
	}
	for(int i=1;i<n;i++)
		a[i]=make_pair(cnt[i],i);
	sort(a+1,a+n);
	fout<<a[n-1].first;
	int j;
	for(j=n-1;j&&a[j].first==a[n-1].first;j--);
	fout<<' '<<n-1-j<<endl;
	for(j++;j<n;j++)
		fout<<a[j].second<<' ';
	fout<<endl;
	return 0;
}
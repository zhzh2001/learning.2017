#include<bits/stdc++.h>
#include<windows.h>
using namespace std;
ofstream fout("City.in");
const int n=5;
int f[n+5];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	int cnt=0;
	for(;;)
	{
		uniform_int_distribution<> dn(1,n);
		uniform_int_distribution<> dw(1,5);
		int u=dn(gen),v=dn(gen),ru=getf(u),rv=getf(v);
		if(ru!=rv)
		{
			f[ru]=rv;
			fout<<u<<' '<<v<<' '<<dw(gen)<<endl;
			if(++cnt==n-1)
				break;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
#define lca long long
#define N 200020
using namespace std;
inline lca read()
{
	lca x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
lca n;
lca head[N],pos;
struct edge{lca to,id,next;lca c;}e[N<<1];
void add(lca a,lca b,lca c,lca id)
{pos++;e[pos].c=c,e[pos].to=b,e[pos].id=id,e[pos].next=head[a],head[a]=pos;}
void insert(lca a,lca b,lca c,lca id){add(a,b,c,id);add(b,a,c,id);}
bool vis[N];
set<lca> S;
set<lca>:: iterator it;
lca ans[N];
void dfs(lca u,lca mx)
{
	vis[u]=1;
	for(it=S.begin();it!=S.end();it++)
		ans[*it]++;
	for(lca i=head[u];i;i=e[i].next)
	{
		lca v=e[i].to;
		if(vis[v])continue;
		set<lca>T;T=S;
		if(e[i].c>mx)
		{
			S.clear();S.insert(e[i].id);
			dfs(v,e[i].c);
		}
		else 
		{
			if(e[i].c==mx)S.insert(e[i].id);
			dfs(v,mx);
		}
		S=T;
	}
}
int main()
{
	freopen("City.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read();
	for(lca i=2;i<=n;i++)
	{
		lca x=read(),y=read();lca c;
		scanf("%lld",&c);
		insert(x,y,c,i-1);
	}
	for(lca i=1;i<=n;i++)
	{
		memset(vis,0,sizeof(vis));
		dfs(i,0);
	}	
	lca val=0,cnt=0;
	for(lca i=1;i<n;i++)
	{
		if(ans[i]>val)val=ans[i],cnt=1;
		else if(ans[i]==val)cnt++;
	}
	printf("%lld %lld\n",val,cnt);
	for(lca i=1;i<n;i++)
		if(ans[i]==val)
			printf("%lld ",i);
}

#include<bits/stdc++.h>
#define Max(a,b) (a>b?a:b)
#define lowbit(x) (x&-x)
#define lca long long
#define N 200020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline lca lread()
{
	lca x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n;
int head[N],pos;
struct edge{int to,id,next;lca c;}e[N<<1];
void add(int a,int b,lca c,int id)
{pos++;e[pos].c=c,e[pos].to=b,e[pos].id=id,e[pos].next=head[a],head[a]=pos;}
void insert(int a,int b,lca c,int id){add(a,b,c,id);add(b,a,c,id);}
lca num[N],has[N],tot;
int find(lca x)
{
	int l=1,r=tot,ret=0;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(has[mid]<=x)ret=mid,l=mid+1;
		else r=mid-1;
	}
	return ret;
}
bool vis[N];
int size[N],ans[N];
int summ,vl,rt;
int C[N];
void ins(int x,int c){for(int i=x;i<=tot;i+=lowbit(i))C[i]+=c;}
int ask(int x)
{
	int ret=0;
	for(int i=x;i;i-=lowbit(i))
		ret+=C[i];return ret;
}
inline void find_root(int u,int fa)
{
	size[u]=1;int mx=0;
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(v==fa||vis[v])continue;
		find_root(v,u);
		size[u]+=size[v];
		mx=Max(mx,size[v]);
	}
	mx=Max(mx,summ-size[u]);
	if(mx<vl)rt=u,vl=mx;
}
int dep[N],to[N];
int st[N],top,fm,cv;
void find_dep(int u,int fa)
{
	st[++top]=dep[u];
	ans[to[u]]+=ask(dep[u])*2+(cv?2:0);
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(v==fa||vis[v])continue;
		if(e[i].c>dep[u])
			dep[v]=e[i].c,to[v]=e[i].id;
		else dep[v]=dep[u],to[v]=to[u];
		find_dep(v,u);
	}
}
void cal(int u,int c,int v)
{
	dep[u]=c,to[u]=v;
	find_dep(u,0);
}
void rec(int c)
{
	for(int i=fm+1;i<=top;i++)
		ins(st[i],c);
}
int p[N],cnt;
void work(int u)
{
	vis[u]=1;top=cnt=0;cv=1;
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(vis[v])continue;
		fm=top;cal(v,e[i].c,e[i].id);
		p[++cnt]=i;rec(1);
	}cv=0;
	fm=0,rec(-1),top=0;
	for(int i=cnt;i;i--)
	{
		int j=p[i];
		fm=top;cal(e[j].to,e[j].c,e[j].id);
		rec(1);
	}
	fm=0,rec(-1);
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(vis[v])continue;
		summ=size[v],vl=n+1,rt=0;
		find_root(v,0);
		work(rt);
	}
}
int main()
{
	freopen("City.in","r",stdin);
	freopen("City.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++)
	{
		int x=read(),y=read();lca c=lread();
		insert(x,y,c,i-1);
		num[i-1]=c;
	}
	sort(num+1,num+n);
	for(int i=1;i<n;i++)
		if(num[i]!=num[i-1])has[++tot]=num[i];
	for(int i=1;i<=n;i++)
		for(int j=head[i];j;j=e[j].next)
			e[j].c=find(e[j].c);
	summ=n,vl=n+1;
	find_root(1,0);
	work(rt);
	int val=0,cnt=0;
	for(int i=1;i<n;i++)
	{
		if(ans[i]>val)val=ans[i],cnt=1;
		else if(ans[i]==val)cnt++;
	}
	printf("%d %d\n",val,cnt);
	for(int i=1;i<n;i++)
		if(ans[i]==val)
			printf("%d ",i);
}

#include<bits/stdc++.h>
#define N 200200
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,ans;
int head[N],pos;
struct edge{int to,next,c;}e[N<<1];
void add(int a,int b,int c)
{pos++;e[pos].c=c,e[pos].to=b,e[pos].next=head[a],head[a]=pos;}
void insert(int a,int b,int c){add(a,b,c);add(b,a,c);}
bool vis[N];
void dfs(int u,int c)
{
	vis[u]=1;
	ans=max(ans,c);
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(vis[v])continue;
		dfs(v,c^e[i].c);
	}
}
int main()
{
	freopen("Xor.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++)
	{
		int x=read(),y=read(),c=read();
		insert(x,y,c);
	}
	for(int i=1;i<=n;i++)
	{
		memset(vis,0,sizeof(vis));
		dfs(i,0);
	}
	printf("%d\n",ans);
}

#include<bits/stdc++.h>
#define Max(a,b) (a>b?a:b)
#define M 3100050
#define N 200200
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int bin[N];
void init(){bin[0]=1;for(int i=1;i<=30;i++)bin[i]=bin[i-1]<<1;}
int n,ans,up;
int head[N],pos;
struct edge{int to,next,c;}e[N<<1];
inline void add(int a,int b,int c)
{pos++;e[pos].to=b,e[pos].c=c,e[pos].next=head[a],head[a]=pos;}
inline void insert(int a,int b,int c){add(a,b,c);add(b,a,c);}
int p[M],tot;
struct trie
{
	int v[M];
	int st[N],top;
	int tr[M][2],rt,sz;
	trie(){rt=sz=1;}
	inline void ini(int x){top=0;while(x)st[top]=x&1,++top,x>>=1;}
	inline void insert(int x,int c)
	{
		ini(x);
		int now=rt;v[now]+=c;
		for(int i=up;i>=0;i--)
		{
			if(i>=top)st[i]=0;
			if(!tr[now][st[i]])
			{
				if(tot)tr[now][st[i]]=p[tot--];
				else tr[now][st[i]]=++sz;
			}
			now=tr[now][st[i]];
			v[now]+=c;
			if(!v[now]&&tot<M-1)p[++tot]=now;
		}
	}
	inline int ask(int x)
	{
		ini(x);
		int now=rt,ret=0;
		if(!v[rt])return x;
		for(int i=up;i>=0;i--)
		{
			if(i>=top)st[i]=0;
			if(!v[tr[now][st[i]^1]])
				now=tr[now][st[i]];
			else now=tr[now][st[i]^1],ret|=bin[i];
		}return ret;
	}
}trie;
bool vis[N];
int size[N];
int summ,vl,rt;
inline void find_root(int u,int fa)
{
	size[u]=1;int mx=0;
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(v==fa||vis[v])continue;
		find_root(v,u);
		size[u]+=size[v];
		mx=Max(mx,size[v]);
	}
	mx=Max(mx,summ-size[u]);
	if(mx<vl)rt=u,vl=mx;
}
int dep[N],qs[N],qtp;
void find_dep(int u,int fa)
{
	qs[++qtp]=dep[u];
	ans=Max(ans,dep[u]);
	ans=Max(ans,trie.ask(dep[u]));
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(v==fa||vis[v])continue;
		dep[v]=dep[u]^e[i].c;
		find_dep(v,u);
	}
}
void cal(int u,int c)
{
	dep[u]=c;
	find_dep(u,0);
}
int fm;
void rec(int c)
{
	for(int i=fm+1;i<=qtp;i++)
		trie.insert(qs[i],c);
}
void work(int u)
{
	vis[u]=1;qtp=0;
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(vis[v])continue;
		fm=qtp;cal(v,e[i].c);
		rec(1);
	}
	fm=0,rec(-1);
	for(int i=head[u];i;i=e[i].next)
	{
		int v=e[i].to;
		if(vis[v])continue;
		summ=size[v],vl=n+1,rt=0;
		find_root(v,0);
		work(rt);
	}
}
int main()
{
	freopen("Xor.in","r",stdin);
	freopen("Xor.out","w",stdout);
	n=read();int mx=0;
	for(int i=2;i<=n;i++)
	{
		int x=read(),y=read(),c=read();
		insert(x,y,c);
		mx=max(mx,c);
	}trie.ini(mx);up=30;
	summ=n,vl=n+1,rt=0,
	find_root(1,0);
	work(rt);
	printf("%d\n",ans);
}

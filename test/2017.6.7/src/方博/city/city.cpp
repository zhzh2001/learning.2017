#include<bits/stdc++.h>
#define int long long
using namespace std;
const int N=100005;
int n;
struct xx{
	int x,y,z,i;
}a[N];
struct xxx{
	int i,k;
}ans[N];
int head[N*2],nxt[N*2],tail[N*2],e[N*2];
int t;
bool vis[N];
bool com(xxx a,xxx b)
{
	if(a.k==b.k)return a.i<b.i;
	else return a.k>b.k;
}
void addto(int x,int y,int z)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
	e[t]=z;
}
void dfs(int k,int x)
{
	for(int i=head[k];i;i=nxt[i])
		if(!vis[tail[i]]&&e[i]<=x){
			vis[k]=true;
			t++;
			dfs(tail[i],x);
			vis[k]=false;
		}
}
bool cmp(xx a,xx b)
{
	return a.z>b.z;
}
int sqr(int x)
{
	return x*x;
}
signed main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	scanf("%lld",&n);
	for(int i=1;i<n;i++){
		scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		addto(a[i].x,a[i].y,a[i].z);
		addto(a[i].y,a[i].x,a[i].z);
		a[i].i=i;
	}
	sort(a+1,a+n,cmp);
	int maxans=0;
	int x=0;
	for(int i=1;i<n;i++){
		if(a[i].z!=a[i-1].z)x=i;
		if(sqr(n-x+1)/2>=maxans){
//				cout<<i<<endl;
			vis[a[i].y]=true;
			t=1;
			dfs(a[i].x,a[i].z);
			ans[i].k=t;
			t=1;
			vis[a[i].x]=true;
			dfs(a[i].y,a[i].z);
			ans[i].k=ans[i].k*t*2;
			ans[i].i=a[i].i;
			maxans=max(maxans,ans[i].k);
			vis[a[i].x]=false;
			vis[a[i].y]=false;
		}
	}
	sort(ans+1,ans+n,com);
	int t=0;
	for(int i=1;i<n;i++){
		if(ans[i].k!=ans[i+1].k){t=i;break;}
	}
	printf("%lld %lld\n",ans[1].k,t);
	for(int i=1;i<=t;i++)
		printf("%lld ",ans[i].i);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int N=100003;
int nxt[4*N][2];
int f[4*N];
int t[4*N][34];
int head[4*N],tail[4*N],nt[4*N],e[4*N],tt=0,n;
void addto(int x,int y,int z)
{
	nt[++tt]=head[x];
	head[x]=tt;
	tail[tt]=y;
	e[tt]=z;
}
void make(int i,int k)
{
	int x=32;
	while(x>=1){
		t[i][x]=k&1;
		x--;
		k=k>>1;
	}
}
void dfs(int k)
{
	int x=head[k];
	while(x!=0){
		int i=tail[x];
		if(i!=f[k]){
			t[i][0]=t[k][0]^e[x];
			f[i]=k;
			dfs(i);
		}
		x=nt[x];
	}
}
void get(int k)
{
	int x=0;
	for(int i=1;i<=32;i++){
		if(nxt[x][t[k][i]]==0){
			tt++;
			nxt[x][t[k][i]]=tt;
		}
		x=nxt[x][t[k][i]];
	}
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		addto(x,y,z);
		addto(y,x,z);
	}
	dfs(1);
	for(int i=1;i<=n;i++)
		make(i,t[i][0]);
	tt=0;
	for(int i=1;i<=n;i++)
		get(i);	
	long long ans=0;
	for(int i=1;i<=n;i++){
		int x=0;
		long long xx=0;
		for(int j=1;j<=32;j++){
			if(nxt[x][1^t[i][j]])x=nxt[x][1^t[i][j]],t[i][j]=1;
			else x=nxt[x][t[i][j]],t[i][j]=0;
			xx=xx*2+t[i][j];
		}
		ans=max(ans,xx);
	}
	printf("%lld",ans);
	return 0;
}

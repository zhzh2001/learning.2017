#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,ch=getchar(),res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

struct Dat{
	int u,v,w,pos;
}B[N];
inline bool Cmp(Dat A,Dat B){
	return A.w<B.w;
}
int cnt;
int lines,front[N];
struct Edge{
	int next,to,pos;
}E[N*2];
inline void Addline(int u,int v,int pos){
	E[++lines]=(Edge){front[u],v,pos};front[u]=lines;
}
vector<int>Vec;
ll Ans;
void fresh(ll val,int pos){
	if (val>Ans){
		Ans=val;Vec.clear();
	}
	if (val==Ans) Vec.push_back(pos);
}
int n,fa[N],Size,size[N],Q[2*N];
inline int find(int u){
	return fa[u]!=u?fa[u]=find(fa[u]):u;
}
inline void Merge(int u,int v){
	u=find(u),v=find(v);
	fa[u]=v;size[v]+=size[u];
}
void Pre(int u,int f){
	Size+=size[u];
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=f) Pre(v,u);
	}
}
int siz[N];
void Dfs(int u,int f){
//	printf("u=%d\n",u);
	siz[u]=size[u];
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=f){
			Dfs(v,u);siz[u]+=siz[v];
		}
	}
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=f){
			fresh(2ll*siz[v]*(Size-siz[v]),E[i].pos);
//			printf("Size=%d sub[v]=%d u=%d pos=%d\n",Size,siz[v],siz[u],E[i].pos);
		}
	}
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	Rep(i,1,n-1){
		int u=read(),v=read(),w=read();
		B[i]=(Dat){u,v,w,i};
	}
	sort(B+1,B+n,Cmp);
	Rep(i,1,n) fa[i]=i,size[i]=1;
	for (int i=1,j;i<n;i=j+1){
		j=i;while (j<n-1 && B[j+1].w==B[j].w) j++;
		*Q=0;
		Rep(k,i,j) Q[++*Q]=B[k].u,Q[++*Q]=B[k].v;
		Rep(k,1,*Q) front[Q[k]]=0;
		lines=0;
		int Root;
		Rep(k,i,j){
			int u=find(B[k].u),v=find(B[k].v);
			Addline(u,v,B[k].pos);Addline(v,u,B[k].pos);
			Root=u;
		}
		Size=0;
		Pre(Root,0);
//		puts("Start Dfs");
		Dfs(Root,0);
		Rep(k,i,j){
			Merge(B[k].u,B[k].v);
		}
		Rep(k,1,*Q) front[Q[k]]=0;
	}
	printf("%I64d %d\n",Ans,(int)Vec.size());
	sort(Vec.begin(),Vec.end());
	Rep(i,0,Vec.size()-1) printf("%d ",Vec[i]);puts("");
}
/*
6
1 2 1
1 3 5
3 4 2
3 5 3
3 6 4
*/

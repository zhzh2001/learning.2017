#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,ch=getchar(),res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;
const int Ln=18;
const int INF=1e9;

int n;
int main(){
	freopen("xor.in","w",stdout);
	srand(time(0));
	Rep(i,1,rand()) rand();
	printf("%d\n",n=1000);
	Rep(i,1,n-1) printf("%d %d %d\n",rand()%i+1,i+1,rand()<<15|rand());
}

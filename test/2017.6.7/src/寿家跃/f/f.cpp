#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,ch=getchar(),res=0;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int Dat[105];

int F(int val){
	if (val>100) return val-10;
	if (Dat[val]!=-1) return Dat[val];
	return Dat[val]=F(F(val+11));
}

int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	memset(Dat,-1,sizeof(Dat));
	int val=read();
	while (val){
		printf("%d\n",F(val));
		val=read();
	}
}

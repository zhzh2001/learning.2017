#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e5+5;
struct cs{int v,to,nxt;}a[N*2];
struct in{int x,y,z,num;}I[N];
int head[N],ll;
int ans,an[N],top;
int n,m,x,y,z;
void init(int x,int y,int z){
	a[++ll].to=y;
	a[ll].v=z;
	a[ll].nxt=head[x];
	head[x]=ll;
}
bool cmp(in a,in b){return a.z>b.z;}
int dfs(int x,int y,int v){
	int sum=1;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y&&a[k].v<=v)sum+=dfs(a[k].to,x,v);
	return sum;
}
int main()
{
	freopen("City.in","r",stdin);
	freopen("City.out","w",stdout);
	scanf("%d",&n);n--;
	for(int i=1;i<=n;i++){
		scanf("%d%d%d",&x,&y,&z);
		init(x,y,z);init(y,x,z);
		I[i].x=x;I[i].y=y;I[i].z=z;I[i].num=i;
	}
	sort(I+1,I+n+1,cmp);
	for(int i=1;i<=n;i++){
		int temp=dfs(I[i].x,I[i].y,I[i].z)*dfs(I[i].y,I[i].x,I[i].z);
		if(temp==ans)an[++top]=I[i].num;
		if(temp>ans)ans=temp,an[top=1]=I[i].num;
		if(i*n>1e7)break;
	}
	sort(an+1,an+top+1);
	printf("%d %d\n",ans*2,top);
	for(int i=1;i<=top;i++)printf("%d ",an[i]);
}





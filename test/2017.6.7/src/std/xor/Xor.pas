Program                         Tree;
{$M 100000000}
const maxn=100000;maxm=1 shl 30;
var
e : array[0..2*maxn] of record
        v,f,next : longint;
end;
v : array[0..maxn] of Longint;
a,b,c,i,j,n,m,en,top,root,Max : longint;
d : array[0..31] of Longint;
tri : array[0..2000000,0..1] of Longint;
Procedure       Addedge(a,b,c : longint);
begin
inc(en);e[en].v :=b;e[en].f :=c;e[en].next :=v[a];v[a]:=en;
inc(en);e[en].v :=a;e[en].f :=c;e[en].next :=v[b];v[b]:=en;
end;
procedure               Find(x : longint);
var  k,sum,p,i : longint;
begin
        Sum :=0;k := Maxm;
        for i :=1 to 31 do
        begin
                d[i]:=x mod 2;x := x div 2;
        end;
        p:=root;
        for i :=31 downto 1 do
        begin
                if tri[p][1-d[i]]<>0 then
                begin
                        Sum := sum+k;p := tri[p][1-d[i]];
                end else p:=tri[p][d[i]];

                k := k shr 1;
        end;
        if sum>Max then Max := sum;
end;
procedure               clear(top : longint);
begin
        tri[top][0]:=0;tri[top][1]:=0;
end;
procedure               Insert(x : longint);
var i,p : longint;
begin
        for i :=1 to 31 do
        begin
                d[i] := x mod 2;x := x div 2;
        end;
        //for i :=1 to 31 do write(d[i],' ');
        //writeln;
        p:=root;
        for i :=31 downto 1 do
        begin
                if tri[p][d[i]]=0 then
                begin
                        top:= top+1;tri[p][d[i]]:=top;clear(top);
                        p:= top;
                end  else
                p:= tri[p][d[i]];
        end;
end;
procedure               DFS(x,fa,len : longint);
var   j : longint;
begin
        Insert(len);
        Find(len);
        j := v[x];
        while j>0 do
        begin
                if e[j].v<>fa then
                DFS(e[j].v,x,e[j].f xor len);
                j := e[j].next;
        end;
end;
begin
Assign(input,'Xor.in');Assign(output,'Xor.out');
reset(input); rewrite(output);
        readln(n); top:=1;root:=1;Max:=0;
        for i :=1 to n-1 do
        begin
                readln(a,b,c);
                Addedge(a,b,c);
        end;
        DFS(1,0,0);
        writeln(max);
Close(input); Close(output);
end.

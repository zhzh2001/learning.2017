Program			City;
{$M 100000000}
Const Maxn=100000;
var
e : array[0..2*Maxn] of record
	v,next : longint;
end;
x,y,z,f,g,v,p: array[0..Maxn+10] of Longint;
num,ans : array[0..maxn+10] of Int64;
tot ,i,j,n,m,en,l,now : longint;
Max : int64;
procedure			addedge(a,b : longint);
begin
	inc(en);e[en].v := b;e[en].next := v[a];v[a]:=en;
	inc(en);e[en].v := a;e[en].next := v[b];v[b]:=en;
end;
procedure		DFS(x,fa : longint);
var     j : Longint;
begin
		j := v[x];
		while j>0 do
		begin
			if e[j].v<>fa then begin
			DFS(e[j].v,x);f[e[j].v]:=x;
			end;
			j := e[j].next;
		end;
end;
Procedure			Swap(var a,b : longint);
var t : longint;
begin
	t :=a;a:=b;b:=t;
end;
procedure			up(x,val : longint);
var i : longint;
begin
		while x<>g[x] do
		begin
			num[x]:=num[x]+val;
			x:=g[x];
		end;
		num[x]:= num[x]+val;
end;
function			find(x : longint) : longint;
begin
		if x=g[x] then exit(x) else
		begin
			g[x]:= find(g[x]);
			find := g[x];
		end;
end;
procedure			Qsort(l,r : longint);
var i,j,k,t : longint;
begin
	i := l;j :=r;k := z[random(r-l+1)+l];
	repeat
		while z[i]<k do i := i+1;
		while z[j]>k do j := j-1;
		if i<=j then
		begin
			Swap(x[i],x[j]);Swap(y[i],y[j]);
			Swap(z[i],z[j]);Swap(p[i],p[j]);
			i := i+1;j := j-1;
		end;
	until i>j;
	if i<r then Qsort(i,r);
	if L<j then Qsort(l,j);
end;
begin
Assign(input,'City.in');Assign(output,'City.out');
reset(input); rewrite(output);
                randomize;
		readln(n);
		for i :=1 to n-1 do
		begin
			read(x[i],y[i],z[i]);p[i]:=i;
			addedge(x[i],y[i]);
		end;
		Qsort(1,n-1);
		DFS(1,0);
		for i :=1 to n do if x[i]<>f[y[i]] then Swap(x[i],y[i]);
		for i :=1 to n do begin g[i]:=i;num[i]:=1;end;

		i :=1;
		while i<=n-1 do
		begin
			j := i;
			while (j+1<=n)and(z[i]=z[j+1]) do j := j+1;
			for l := i to j do
			begin
				g[y[l]] := x[l];
                                up(x[l],num[y[l]]);
			end;

			for l :=i to j do
			begin
				Now := find(x[l]);
				ans[p[l]]:= num[y[l]]*(num[now]-num[y[l]]);
			end;

			i :=j+1;
		end;
		tot:=0;max:=0;
		for i :=1 to n-1 do
		if Max<ans[i] then begin tot :=1;g[tot]:=i;max := ans[i];end else
		if Max=ans[i] then begin tot := tot+1;g[tot]:=i;end;

		writeln(Max*2,' ',tot);
		for i := 1 to tot-1 do write(g[i],' '); writeln(g[tot]);
Close(input); Close(output);
end.

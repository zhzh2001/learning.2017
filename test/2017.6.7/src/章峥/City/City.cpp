#include<fstream>
#include<algorithm>
#include<utility>
#include<vector>
using namespace std;
ifstream fin("City.in");
ofstream fout("City.out");
const int N=100005;
struct Edge
{
	int u,v,w,id;
	bool operator<(const Edge& rhs)const
	{
		return w<rhs.w;
	}
}e[N];
vector<int> mat[N];
int f[N],sz[N];
long long cnt[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
void modify(int x,int val)
{
	for(;f[x]!=x;x=f[x])
		sz[x]+=val;
	sz[x]+=val;
}
bool vis[N];
int df[N],l[N];
void dfs(int k)
{
	vis[k]=true;
	for(vector<int>::iterator it=mat[k].begin();it!=mat[k].end();++it)
		if(!vis[*it])
		{
			df[*it]=k;
			dfs(*it);
		}
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<n;i++)
	{
		fin>>e[i].u>>e[i].v>>e[i].w;
		e[i].id=i;
		mat[e[i].u].push_back(e[i].v);
		mat[e[i].v].push_back(e[i].u);
	}
	dfs(1);
	sort(e+1,e+n);
	for(int i=1;i<n;i++)
		if(e[i].u!=df[e[i].v])
			swap(e[i].u,e[i].v);
	for(int i=1;i<=n;i++)
	{
		f[i]=i;
		sz[i]=1;
	}
	for(int i=1;i<n;)
	{
		int j;
		for(j=i;j<n&&e[j].w==e[i].w;j++);
		for(int t=i;t<j;t++)
		{
			f[e[t].v]=e[t].u;
			modify(e[t].u,sz[e[t].v]);
		}
		for(;i<j;i++)
		{
			int root=getf(e[i].u);
			cnt[e[i].id]=2ll*sz[e[i].v]*(sz[root]-sz[e[i].v]);
		}
	}
	int cc=0;
	long long ans=0;
	for(int i=1;i<n;i++)
	{
		if(cnt[i]>ans)
		{
			ans=cnt[i];
			cc=0;
		}
		if(cnt[i]==ans)
			l[++cc]=i;
	}
	fout<<ans<<' '<<cc<<endl;
	for(int i=1;i<=cc;i++)
		fout<<l[i]<<' ';
	fout<<endl;
	return 0;
}
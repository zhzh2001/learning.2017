#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <queue>
#include <set>
#include <string>
#include <climits>
#include <vector>
#define int long long 
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int x,y,v,s;}a[100001];
int sum,n,nedge=0,p[200001],c[200001],nex[200001],head[200001];
int ans[100001];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
inline void dfs(int x,int fa,int v){
	sum++;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa&&c[k]<=v)dfs(p[k],x,v);
}
signed main()
{
	freopen("City.in","r",stdin);
	freopen("City.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		a[i].x=read();a[i].y=read();a[i].v=read();
		addedge(a[i].x,a[i].y,a[i].v);addedge(a[i].y,a[i].x,a[i].v);
	}
	for(int i=1;i<n;i++){
		sum=0;dfs(a[i].x,a[i].y,a[i].v);int n1=sum;
		sum=0;dfs(a[i].y,a[i].x,a[i].v);int n2=sum;
		a[i].s=n1*n2*2;
	}
	sum=0;
	for(int i=1;i<n;i++)if(a[i].s>=sum){
		if(a[i].s>sum)sum=a[i].s,ans[0]=1,ans[1]=i;
		else ans[++ans[0]]=i;
	}
	printf("%I64d %I64d\n",sum,ans[0]);
	for(int i=1;i<=ans[0];i++)printf("%I64d ",ans[i]);
	return 0;
}
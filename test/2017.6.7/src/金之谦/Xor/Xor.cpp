#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <queue>
#include <set>
#include <string>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,nedge=0,p[200001],c[200001],nex[200001],head[200001];
int ne[2000001][2],cnt=0,dist[200001];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
int rp[32];
inline void insert(int x){
	memset(rp,0,sizeof rp);
	while(x)rp[++rp[0]]=x&1,x>>=1;
	int pr=31,now=0;
	for(int i=pr;i;i--){
		if(!ne[now][rp[i]])ne[now][rp[i]]=++cnt;
		now=ne[now][rp[i]];
	}
}
inline void dfs(int x,int fa,int v){
	insert(v);dist[x]=v;for(int k=head[x];k;k=nex[k])if(p[k]!=fa)dfs(p[k],x,v^c[k]);
}
inline int check(int x){
	memset(rp,0,sizeof rp);
	while(x)rp[++rp[0]]=x&1,x>>=1;
	int pr=31,now=0,ans=0;
	for(int i=pr;i;i--){
		if(ne[now][rp[i]^1])ans=ans<<1|1,now=ne[now][rp[i]^1];
		else ans<<=1,now=ne[now][rp[i]];
	}
	return ans;
}
int main()
{
	freopen("Xor.in","r",stdin);
	freopen("Xor.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x,y,z;x=read();y=read();z=read();
		addedge(x,y,z);addedge(y,x,z);
	}
	dfs(1,0,0);int ans=0;
	for(int i=1;i<=n;i++)ans=max(ans,check(dist[i]));
	printf("%d",ans);
	return 0;
}
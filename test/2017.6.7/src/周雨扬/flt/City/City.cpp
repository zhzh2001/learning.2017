#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=4e5+5;
int siz[N],Siz[N],tot,n,cnt,poi[N],bel[N],E[N],num,pre[N],vis[N],pos[N];
struct E{int next,to,id;} edge[N<<1]; int nedge,head[N];
struct T{int u,v,val,id;} e[N]; long long Ans; int last=1;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int cmp(T i,T j){return i.val<j.val;}
int Dfs(int u,int F){siz[poi[u]]=Siz[poi[u]];
	for (int i=head[u];i;i=NX)
		if (O!=F) Dfs(O,u),siz[poi[u]]+=siz[poi[O]],pre[O]=edge[i].id;
}
inline int addline(int x,int y,int id){
	edge[++nedge].to=y,edge[nedge].next=head[x],edge[nedge].id=id,head[x]=nedge;
}
int main(){
	freopen("City.in","r",stdin);
	freopen("City.out","w",stdout);
	tot=n=Read();
	for (int i=1;i<n;i++)
		e[i].u=Read(),e[i].v=Read(),e[i].val=Read(),e[i].id=i;
	sort(e+1,e+n,cmp);
	for (int i=1;i<=n;i++) bel[i]=i,Siz[i]=1;
	for (int i=1;i<n;i++){cnt=0;
		if (e[i+1].val==e[i].val) continue;
		for (int j=0;j<=i-last+2;j++) head[j]=0; nedge=0;
		for (int j=last;j<=i;j++){
			int u=bel[e[j].u],v=bel[e[j].v];
			if (!vis[u]) poi[++cnt]=u,vis[u]=1,pos[u]=cnt;
			if (!vis[v]) poi[++cnt]=v,vis[v]=1,pos[v]=cnt;
			addline(pos[u],pos[v],e[j].id),addline(pos[v],pos[u],e[j].id);
		}
		Dfs(1,0);
		for (int j=1;j<=cnt;j++){
			long long Res=1ll*(siz[poi[1]]-siz[poi[j]])*siz[poi[j]];
			if (Res==Ans) E[++num]=pre[j];
			if (Res>Ans) Ans=Res,E[num=1]=pre[j];
		}
		tot++;
		for (int j=last;j<=i;j++) bel[e[j].u]=bel[e[j].v]=tot;
		for (int j=1;j<=cnt;j++)
			Siz[tot]+=Siz[poi[j]];
		last=i+1;
	}
	printf("%I64d %d\n",Ans*2ll,num);
	sort(E+1,E+1+num);
	for (int i=1;i<=num;i++) printf("%d ",E[i]);
}

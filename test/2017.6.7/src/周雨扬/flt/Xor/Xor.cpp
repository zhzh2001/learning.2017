#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
#define V edge[i].val
using namespace std;
const int N=1e5+5,M=31;
int nxt[N][M],dep[N],q[N],n,Ans,Res,nedge,tot=1,head[N];
struct E{int next,to,val;} edge[N<<1]; int vis[N],s[N][M];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int Bfs(){int l=0,r=0;
	for (q[r=1]=1;++l<=r;){int u=q[l];
		for (int i=head[u];i;i=NX)
			if (!vis[O]) vis[O]=1,dep[O]=dep[u]^V,q[++r]=O;
		for (int i=0,t=dep[u];i<M;i++) s[u][i]=t&1,t>>=1;
		int p=1;
		for (int i=M-1;~i;i--){
			if (!nxt[p][s[u][i]])
				nxt[p][s[u][i]]=++tot;
			p=nxt[p][s[u][i]];
		}
	}
}
inline int addline(int x,int y,int val){
	edge[++nedge].to=y,edge[nedge].next=head[x],edge[nedge].val=val,head[x]=nedge;
}
int main(){
	freopen("Xor.in","r",stdin);
	freopen("Xor.out","w",stdout);
	n=Read();
	for (int i=1;i<n;i++){
		int u=Read(),v=Read(),x=Read();
		addline(u,v,x),addline(v,u,x);
	}
	Bfs();
	for (int i=1;i<=n;i++){Res=0;
		int p=1;
		for (int j=M-1;~j;j--){
			if (nxt[p][s[i][j]^1]) Res+=(1<<j),p=nxt[p][s[i][j]^1];
			else p=nxt[p][s[i][j]];
		}
		Ans=max(Ans,Res);
	}
	printf("%d\n",Ans);
}

#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

struct edge
{
	int o,x,y,z;
	inline bool operator < (const edge t) const
	{
		return z<t.z;
	}
};

vector <int> V;
vector <int> :: iterator it;
edge e[120000];
int edge[300000],next[300000],ord[300000],first[120000];
int f[120000],h[120000],w[120000];
bool b[120000];
int i,j,k,n,t,sum_edge;
long long s;

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],ord[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline int getfather(int x)
{
	if (f[x]==x)
		return x;
	else
		return f[x]=getfather(f[x]);
}

inline int dfs1(int x,int y)
{
	w[x]=h[getfather(x)];
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			w[x]=w[x]+dfs1(edge[i],x);
	return w[x];
}

inline void dfs2(int x,int y)
{
	b[x]=true;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
		{
			if (2LL*(t-w[edge[i]])*w[edge[i]]>s)
				s=2LL*(t-w[edge[i]])*w[edge[i]],V.clear(),V.push_back(ord[i]);
			else
				if (2LL*(t-w[edge[i]])*w[edge[i]]==s)
					V.push_back(ord[i]);
			dfs2(edge[i],x);
		}
	return;
}

int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<n;i++)
		scanf("%d%d%d",&e[i].x,&e[i].y,&e[i].z),e[i].o=i;
	sort(e+1,e+n);
	for (i=1;i<=n;i++)
		f[i]=i,h[i]=1;
	for (i=1;i<n;i++)
	{
		for (j=i;(j<n) && (e[i].z==e[j].z);j++);
		for (k=i;k<j;k++)
		{
			addedge(getfather(e[k].x),getfather(e[k].y),e[k].o);
			addedge(getfather(e[k].y),getfather(e[k].x),e[k].o);
		}
		for (k=i;k<j;k++)
		{
			if (! b[getfather(e[k].x)])
			{
				t=dfs1(getfather(e[k].x),0);
				dfs2(getfather(e[k].x),0);
			}
			if (! b[getfather(e[k].y)])
			{
				t=dfs1(getfather(e[k].y),0);
				dfs2(getfather(e[k].y),0);
			}
		}
		for (k=i;k<j;k++)
		{
			b[getfather(e[k].x)]=false,b[getfather(e[k].y)]=false;
			first[getfather(e[k].x)]=0,first[getfather(e[k].y)]=0;
			sum_edge=0;
			h[getfather(e[k].x)]=h[getfather(e[k].x)]+h[getfather(e[k].y)];
			f[getfather(e[k].y)]=getfather(e[k].x);
		}
	}
	printf("%I64d %d\n",s,V.size());
	sort(V.begin(),V.end());
	for (it=V.begin();it!=V.end();it++)
		printf("%d ",*it);
	return 0;
}

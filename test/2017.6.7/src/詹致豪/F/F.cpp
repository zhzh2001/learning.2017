#include <cstdio>

int f[120];
int n;

inline int fastscanf()
{
	int t=0;
	char c=getchar();
	while (! ((c>47) && (c<58)))
		c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t;
}

inline void fastprintf(int x)
{
	char c[5];
	for (c[0]=0;x;x=x/10)
		c[0]++,c[c[0]]=x%10;
	for (;c[0]>0;c[0]--)
		putchar(c[c[0]]+48);
	puts("");
	return;
}

inline int F(int x)
{
	if (x>100)
		return x-10;
	if (f[x])
		return f[x];
	else
		return f[x]=F(F(x+11));
}

int main()
{
	freopen("F.in","r",stdin);
	freopen("F.out","w",stdout);
	for (;;)
	{
		n=fastscanf();
		if (! n)
			return 0;
		else
			fastprintf(F(n));
	}
}

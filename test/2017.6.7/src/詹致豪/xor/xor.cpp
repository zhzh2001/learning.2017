#include <cstdio>
#include <algorithm>

using namespace std;

int tree[500000][2];
int edge[200000],next[200000],dist[200000],first[200000];
int d[200000];
int i,j,n,p,s,t,x,y,z,sum_node,sum_edge;

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline void dfs(int x,int y,int z)
{
	d[x]=z;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			dfs(edge[i],x,z^dist[i]);
	return;
}

int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z),addedge(y,x,z);
	}
	dfs(1,0,0);
	sum_node=1;
	for (i=1;i<=n;i++)
	{
		if (sum_node>1)
		{
			p=1,t=0;
			for (j=30;j>=0;j--)
				if (d[i]&(1<<j))
					if (tree[p][0])
						p=tree[p][0],t=t|(1<<j);
					else
						p=tree[p][1];
				else
					if (tree[p][1])
						p=tree[p][1],t=t|(1<<j);
					else
						p=tree[p][0];
			s=max(s,t);
		}
		p=1;
		for (j=30;j>=0;j--)
			if (d[i]&(1<<j))
			{
				if (! tree[p][1])
					tree[p][1]=++sum_node;
				p=tree[p][1];
			}
			else
			{
				if (! tree[p][0])
					tree[p][0]=++sum_node;
				p=tree[p][0];
			}
	}
	printf("%d",s);
	return 0;
}

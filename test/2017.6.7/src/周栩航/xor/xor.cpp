#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int tot=1;
int son[5000001][2],bj[5000001],n,a[200001],q[35],ans=0,t,poi[400001],nxt[400001],f[400001],v[400001],cnt,mx,tot1;
int x,y,z,rt,ed,hhh;
bool vis[400001];
inline void insert(int x)
{
	For(i,1,32)	q[i]=0;
	int top=0,tmp=x;
	For(i,1,mx){q[++top]=x%2;x/=2;}
	int now=1;
	bool flag=0;
	Dow(i,1,mx)
	{
		// if(q[i]==1)	flag=1;	
		if(!son[now][q[i]])	son[now][q[i]]=++tot;
		now=son[now][q[i]];
		// if(flag)	bj[now]=tmp;
	}
	bj[now]=tmp;
}
inline int query(int x)
{	
	For(i,1,32)	q[i]=0;
	int top=0,tmp=x;
	For(i,1,mx)	{q[++top]=x%2;x/=2;q[top]^=1;}
	int now=1,tans=0;
	Dow(i,1,mx)
	{
		if(!son[now][q[i]])	return tans;
		else now=son[now][q[i]];
		if(bj[now])	tans=bj[now];
	}
	return tans;
}
inline void add(int x,int y,int z)
{
	poi[++cnt]=y;nxt[cnt]=f[x];v[cnt]=z;f[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=f[y];v[cnt]=z;f[y]=cnt;
}
inline void dfs(int x,int tv)
{
	vis[x]=1;
	if(tv!=0)
	{
		a[++tot1]=tv;
	}
	for(int i=f[x];i;i=nxt[i])
		if(!vis[poi[i]])	dfs(poi[i],tv^v[i]);
}
inline int find(int x)
{
	int t=1,tc=1;
	while(t<=x)	t*=2,tc++;return tc-1;
}
int main()
{
	freopen("xor.in","r",stdin);freopen("xor.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n-1)	scanf("%d%d%d",&x,&y,&z),add(x,y,z);
	dfs(1,0);
	sort(a+1,a+tot1+1);
	For(i,1,tot1)	mx=max(mx,find(a[i]));	
	t=a[1];
	insert(t);
	For(i,2,tot1)
	{
		t=a[i];
		int to=query(t);
		ans=max(ans,t^to);
		insert(t);
	}
	printf("%d",ans);
}

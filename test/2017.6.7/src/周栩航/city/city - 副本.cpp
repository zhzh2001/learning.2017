#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
struct edge
{
	int a,b,v,num;
}e[101001];
bool vis[201001];
ll ans=0;
int poi[201001],nxt[201001],fa[201001],f[201001],size[201001],cnt,n,dis[201001],ANS[200001],tot,top,q[201001];
int nxt_poi[201001];
inline void add(int x,int y,int z)
{
	poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=f[y];f[y]=cnt;
}
inline void dfs(int x,int fa,int dist)
{
	vis[x]=1;dis[x]=dist;nxt_poi[x]=fa;
	for(int i=f[x];i;i=nxt[i])
		if(!vis[poi[i]])	dfs(poi[i],x,dist+1);
}
inline int get(int x){return fa[x]==x?x:fa[x]=get(fa[x]);} 
inline void merge(int x,int y)
{
	int tx=get(x),ty=y;
	fa[ty]=tx;
	int tmp=size[ty];
	while(ty!=tx)
	{
		ty=nxt_poi[ty];
		size[ty]+=tmp;
	}
}
inline void doit(int x)
{
	int t1=e[x].a,t2=e[x].b;
	if(dis[t1]>dis[t2])	swap(t1,t2);
	ll tmp=(ll)(size[get(t1)]-size[t2])*(size[t2])*2LL;	
	if(tmp>=ans)
	{
		if(tmp==ans)	ANS[++tot]=e[x].num;
			else tot=0,ANS[++tot]=e[x].num,ans=tmp;
	}
}
inline bool cmp(edge x,edge y)	
{
	if(x.v!=y.v)	return x.v<y.v;
	return min(dis[x.a],dis[x.b])>min(dis[y.a],dis[y.b]);
}
int main()
{
//	freopen("city.in","r",stdin);freopen("city.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n-1)
		scanf("%d%d%d",&e[i].a,&e[i].b,&e[i].v),add(e[i].a,e[i].b,e[i].v),e[i].num=i;
	dfs(1,0,0);
	sort(e+1,e+n,cmp);
	
	For(i,1,n)	size[i]=1,fa[i]=i;
	int p=1;
	while(p<=n-1)
	{
		top=0;
		while(1)
		{
			int t1=e[p].a,t2=e[p].b;
			if(dis[t1]>dis[t2])	swap(t1,t2);
			merge(t1,t2);
			q[++top]=p;
			if(e[p+1].v!=e[p].v)	break;
			p++;
		}
		For(i,1,top)	doit(q[i]);
		p++;
	}
	printf("%d ",ans);cout<<tot<<endl;
	sort(ANS,ANS+tot+1);
	For(i,1,tot)	printf("%d ",ANS[i]);
}

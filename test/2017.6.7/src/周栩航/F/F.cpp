#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
int f[2000001],n;
inline int get(int x)
{
	if(f[x])	return f[x];
	return get(get(x+11));
}
inline void pre()
{
	For(i,101,1000000)	f[i]=i-10;
	Dow(i,1,100)	f[i]=get(i);		
}
int main()
{
	freopen("F.in","r",stdin);freopen("F.out","w",stdout);
	pre();
	while(1)
	{
		n=read();
		if(n==0)	break;
		write(f[n]);
		puts("");
	}
}
     

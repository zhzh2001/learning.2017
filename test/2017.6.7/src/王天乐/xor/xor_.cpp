#include <cstdio>
#include <algorithm>
#define N 5006
using namespace std;
struct arr{long long to,ne,v;}e[N<<1];
long long n,x,y,z,ans,cnt,f[N],head[N];
inline long long read()
{
	long long x=0,c=getchar();while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-48,c=getchar();return x;
}
inline void adde(long long x,long long y,long long z)
{
	e[++cnt].to=y;e[cnt].ne=head[x];e[cnt].v=z;head[x]=cnt;
}
void dfs(long long x,long long y)
{
	ans=max(ans,f[x]);
	for(long long i=head[x];i;i=e[i].ne)
		if(e[i].to!=y)f[e[i].to]=f[x]^e[i].v,dfs(e[i].to,x);
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor_.out","w",stdout);
	n=read();
	for(long long i=1;i<n;i++)
		x=read(),y=read(),z=read(),adde(x,y,z),adde(y,x,z);
	for(long long i=1;i<=n;i++)f[i]=0,dfs(i,0);
	printf("%lld\n",ans);
}

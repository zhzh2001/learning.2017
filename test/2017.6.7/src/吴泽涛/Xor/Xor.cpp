#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=100005;
struct pp{
	int link,val,next;
}a[maxn*2];
int ans,s[maxn],sum,id,cnt,num[maxn],n,u,v,w,head[maxn],tot,val[maxn];
void add(int u,int v,int w){a[++tot].link=v; a[tot].next=head[u]; a[tot].val=w; head[u]=tot;}
void dfs(int x,int fa)
{
	for (int i=head[x];i;i=a[i].next){
		int u=a[i].link; 
		if (u!=fa) {val[++cnt]=a[i].val; dfs(u,x);}
	}
}
int main()
{
	freopen("Xor.in","r",stdin);
	freopen("Xor.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<n;i++) {
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w); add(v,u,w);
		sum+=w; num[u]++; num[v]++;
	}
	for (int i=1;i<=n;i++) if (num[i]==1){cnt++; id=i;}
	if (cnt!=2) {printf("%d\n",sum); return 0;}
	cnt=0; dfs(id,0); s[1]=val[1];
	for (int i=2;i<n;i++) s[i]=s[i-1]^val[i];
	for (int i=1;i<n;i++)
		for (int j=0;j<i;j++) ans=max(ans,s[i]^s[j]);
	printf("%d\n",ans);
	return 0;
} 

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}

int F(int x){
	return x<=100?91:x-10;
}

int main()
{
	freopen("F.in","r",stdin);
	freopen("F.out","w",stdout);
	for (int a;a=read();) printf("%d\n",F(a));
	return 0;
}

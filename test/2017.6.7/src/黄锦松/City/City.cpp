#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005;
int n,m,ll=1,fa[N],sz[N],s2[N],st[N*2],to[N<<1],nxt[N<<1],fl[N*2],hd[N],qu[N],pr[N],sq;
LL cun[N];bool vs[N];
struct na {int u,v,w,id;}e[N];
inline bool cmp (const na &a,const na &b){return a.w<b.w;}

inline void adde(int u,int v,int w){
	to[++ll]=v,nxt[ll]=hd[u],hd[u]=ll,fl[ll]=w;
	to[++ll]=u,nxt[ll]=hd[v],hd[v]=ll,fl[ll]=w;
}

int fnd(int x){
	return fa[x]==x?x:(fa[x]=fnd(fa[x]));
}

void dfs(int u){
	qu[++sq]=u,s2[u]=sz[u],vs[u]=1;
	for (int i=hd[u],v;i;i=nxt[i]) if (!vs[v=to[i]])
		pr[v]=fl[i],dfs(v),s2[u]+=s2[v];
}

int main()
{
	freopen("City.in","r",stdin);
	freopen("City.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) fa[i]=i,sz[i]=1;
	for (int i=1;i<n;i++) e[i].u=read(),e[i].v=read(),e[i].w=read(),e[i].id=i;
	sort(e+1,e+n,cmp);
	for (int i=1,j=1;i<n;i=j){
		for (;j<n&&e[i].w==e[j].w;j++);
		m=0;
		for (int k=i;k<j;k++) st[++m]=fnd(e[k].u),st[++m]=fnd(e[k].v);
		sort(st+1,st+m+1);
		m=unique(st+1,st+m+1)-st-1;
		for (int k=i;k<j;k++){
			int u=fnd(e[k].u),v=fnd(e[k].v);
			adde(u,v,e[k].id);
		}
		
		for (int k=1,u;k<=m;k++) if (!vs[u=st[k]]){
			sq=0,dfs(u);
			for (int l=2;l<=sq;l++) cun[pr[qu[l]]]=2ll*s2[qu[l]]*(s2[u]-s2[qu[l]]);
		}
		
		for (int k=i;k<j;k++){
			int u=fnd(e[k].u),v=fnd(e[k].v);
			fa[u]=v,sz[v]+=sz[u];
		}
		for (int k=1;k<=m;k++) hd[st[k]]=vs[st[k]]=0;ll=1;
	}
	LL mx=0;int cnt=0;
	for (int i=1;i<n;i++){
		if (cun[i]>mx) mx=cun[i],cnt=1;
		else if (cun[i]==mx) cnt++;
	}
	cout<<mx<<" "<<cnt<<endl;
	for (int i=1;i<=n;i++) if (cun[i]==mx) printf("%d ",i);puts("");
	return 0;
}

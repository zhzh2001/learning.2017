#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<ctime>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100000,S=10,P=31;
inline int cal(){
	int t=0;
	for (int i=0;i<P;i++) t=(t<<1)|(rand()&1);
	return t;
}
inline int cal(int x){
	return cal()%x+1;
}

int main()
{
	srand(time(0));
	freopen("City.in","w",stdout);
	int n=N;
	printf("%d\n",n);
	for (int i=1;i<n;i++) printf("%d %d %d\n",1,i+1,cal(S));
	return 0;
}

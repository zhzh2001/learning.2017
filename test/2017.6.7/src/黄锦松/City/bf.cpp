#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005;
int n,lim,ll=1,to[N<<1],nxt[N<<1],fl[N<<1],hd[N];
LL cun[N];

inline void adde(){
	int u=read(),v=read(),w=read();
	to[++ll]=v,nxt[ll]=hd[u],hd[u]=ll,fl[ll]=w;
	to[++ll]=u,nxt[ll]=hd[v],hd[v]=ll,fl[ll]=w;
}

int dfs(int u,int f){
	int r=1;
	for (int i=hd[u],v;i;i=nxt[i])
		if ((v=to[i])!=f&&fl[i]<=lim)
			r+=dfs(v,u);
	return r;
}

int main()
{
	freopen("City.in","r",stdin);
	freopen("City2.out","w",stdout);
	n=read();
	for (int i=1;i<n;i++) adde();
	for (int i=2;i<ll;i+=2){
		int u=to[i],v=to[i^1];lim=fl[i];
		int a=dfs(u,v),b=dfs(v,u);
		cun[i>>1]=2ll*a*b;
	}
	LL mx=0;int cnt=0;
	for (int i=1;i<n;i++){
		if (cun[i]>mx) mx=cun[i],cnt=1;
		else if (cun[i]==mx) cnt++;
	}
	cout<<mx<<" "<<cnt<<endl;
	for (int i=1;i<=n;i++) if (cun[i]==mx) printf("%d ",i);puts("");
	return 0;
}

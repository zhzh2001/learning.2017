#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,P=31;
int n,siz,rt,ll=1,to[N<<1],nxt[N<<1],hd[N],fl[N<<1],sz[N],s2[N],ans=0,_a,st[N],tp;
bool vs[N];
inline void adde(){
	int u=read(),v=read(),w=read();
	to[++ll]=v,nxt[ll]=hd[u],hd[u]=ll,fl[ll]=w;
	to[++ll]=u,nxt[ll]=hd[v],hd[v]=ll,fl[ll]=w;
}

struct na{
	int ll,ch[2][N*P];
	inline void clear(){
		memset(ch[0]+1,0,sizeof(int)*ll);
		memset(ch[1]+1,0,sizeof(int)*ll);
		ll=1;
	}
	inline void ins (int x){
		for (int p=1,i=P-1,t;~i;i--){
			t=(x>>i)&1;
			if (!ch[t][p]) ch[t][p]=++ll;
			p=ch[t][p];
		}
	}
	inline void cal(int x){
		int rec=0;
		for (int p=1,i=P-1,t;~i;i--){
			t=(x>>i)&1;
			(ch[t^1][p])?(rec|=1<<i,p=ch[t^1][p]):(p=ch[t][p]);
		}
		ans=max(ans,rec);
	}
}e;

void dfs_rt(int u,int f){
	sz[u]=1,s2[u]=0;
	for (int i=hd[u],v;i;i=nxt[i])
		if ((v=to[i])!=f&&!vs[v])
			dfs_rt(v,u),sz[u]+=sz[v],s2[u]=max(s2[u],sz[v]);
	s2[u]=max(s2[u],siz-sz[u]);
	if (s2[u]<s2[rt]) rt=u;
}

void dfs_cal(int u,int f){
	e.cal(_a),st[++tp]=_a;
	for (int i=hd[u],v;i;i=nxt[i])
		if ((v=to[i])!=f&&!vs[v])
			_a^=fl[i],dfs_cal(v,u),_a^=fl[i];
}

void dfs(int u){
	vs[u]=1;
	e.clear(),e.ins(0);
	for (int i=hd[u],v;i;i=nxt[i]) if (!vs[v=to[i]]){
		tp=0,_a=fl[i],dfs_cal(v,u);
		for (int j=1;j<=tp;j++) e.ins(st[j]);
	}
	for (int i=hd[u],v;i;i=nxt[i]) if (!vs[v=to[i]]){
		siz=sz[v=to[i]],rt=0,dfs_rt(v,u);
		dfs(rt);
	}
}

int main()
{
	freopen("Xor.in","r",stdin);
	freopen("Xor.out","w",stdout);
	n=siz=s2[0]=read();
	for (int i=1;i<n;i++) adde();
	dfs_rt(1,0);
	dfs(rt);
	printf("%d\n",ans);
	return 0;
}

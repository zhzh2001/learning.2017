#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,P=31;
int n,ans=0,ll=1,to[N<<1],nxt[N<<1],hd[N],fl[N<<1],sz[N],sn[N],fs[N],
	dfn[N],clk=0,rt[N],lp=0,ch[2][N*P],tg[N*P];
bool vs[N];
inline void adde(){
	int u=read(),v=read(),w=read();
	to[++ll]=v,nxt[ll]=hd[u],hd[u]=ll,fl[ll]=w;
	to[++ll]=u,nxt[ll]=hd[v],hd[v]=ll,fl[ll]=w;
}

inline void rev(int o,int k,int a){
	a^=tg[o],tg[o]=0;
	tg[ch[0][o]]^=a,tg[ch[1][o]]^=a;
	if ((a>>k-1)&1) swap(ch[0][o],ch[1][o]);
}

inline void pd(int o,int k){
	rev(o,k,0);
}

inline void ins (int x,int p){
	for (int i=P-1,t;~i;i--){
		if (i) pd(p,i);
		t=(x>>i)&1;
		if (!ch[t][p]) ch[t][p]=++lp;
		p=ch[t][p];
	}
}

inline void cal(int x,int p){
	if (!p) return;
	int rec=0;
	for (int i=P-1,t;i;i--){
		if (i) pd(p,i);
		t=(x>>i)&1;
		(ch[t^1][p])?(rec|=1<<i-1,p=ch[t^1][p]):(p=ch[t][p]);
	}
	ans=max(ans,rec);
}

int _a,_p;
void dfs_cal(int o,int k){
	if (!k) {cal(_a,_p);return;}
	pd(o,k);
	if (ch[0][o]) dfs_cal(ch[0][o],k-1);
	if (ch[1][o]) _a^=(1<<k-1),dfs_cal(ch[1][o],k-1),_a^=(1<<k-1);
}

void merge(int &a,int b,int k){
	if (!b) return;
	if (!a) {a=b;return;}
	if (k) pd(a,k),pd(b,k);
	merge(ch[0][a],ch[0][b],k-1);
	merge(ch[1][a],ch[1][b],k-1);
}

void dfs1(int u,int f){
	sz[u]=1;
	for (int i=hd[u],v;i;i=nxt[i])
		if ((v=to[i])!=f){
			dfs1(v,u);
			sz[u]+=sz[v];
			if (sz[sn[u]]<sz[v]) sn[u]=v,fs[u]=fl[i];
		}
}

void dfs2(int u,int f){
	if (!sn[u]) {ins(0,rt[u]=++lp);return;}
	dfs2(sn[u],u);
	rt[u]=rt[sn[u]];
	rev(rt[u],P,fs[u]);
	ins(0,rt[u]);
	cal(0,rt[u]);
	for (int i=hd[u],v;i;i=nxt[i])
		if ((v=to[i])!=f&&v!=sn[u]){
			dfs2(v,u);
			rev(rt[v],P,fl[i]);
			_a=0,_p=rt[u],dfs_cal(rt[v],P);
			merge(rt[u],rt[v],P);
		}
}

int main()
{
	freopen("Xor.in","r",stdin);
//	freopen("Xor.out","w",stdout);
	n=read();
	for (int i=1;i<n;i++) adde();
	dfs1(1,0);
	dfs2(1,0);
	printf("%d\n",ans);
	return 0;
}

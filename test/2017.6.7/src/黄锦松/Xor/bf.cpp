#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,P=31;
int n,ll=1,to[N<<1],nxt[N<<1],hd[N],fl[N<<1],ans=0;
inline void adde(){
	int u=read(),v=read(),w=read();
	to[++ll]=v,nxt[ll]=hd[u],hd[u]=ll,fl[ll]=w;
	to[++ll]=u,nxt[ll]=hd[v],hd[v]=ll,fl[ll]=w;
}

void dfs(int u,int f,int a){
	ans=max(ans,a);
	for (int i=hd[u],v;i;i=nxt[i])
		if ((v=to[i])!=f)
			dfs(v,u,a^fl[i]);
}

int main()
{
	freopen("Xor.in","r",stdin);
	freopen("Xor2.out","w",stdout);
	n=read();
	for (int i=1;i<n;i++) adde();
	for (int i=1;i<=n;i++) dfs(i,0,0);
	printf("%d\n",ans);
	return 0;
}

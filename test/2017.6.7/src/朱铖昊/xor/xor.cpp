#include<bits/stdc++.h>
#define ll long long 
using namespace std;
inline void read(int &x)
{
	x=0;
	char ch=getchar();
    while(ch<'0'||ch>'9')
		ch=getchar();
    while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
int bin[35],n,ans,l,val[100005],last[100005],x,y,z;
struct da
{
	int to,next,v;
}e[200005];
struct da1{
	int cnt,ch[3000005][2];
	void insert(int x)
	{
		int now=0;
		for(int i=30;i>=0;i--)
		{
			int t=x&bin[i];t>>=i;
			if(!ch[now][t])ch[now][t]=++cnt;
			now=ch[now][t];
		}
	}
	int query(int x)
	{
		int tmp=0,now=0;
		for(int i=30;i>=0;i--)
		{
			int t=x&bin[i];
			t>>=i;
			if(ch[now][t^1])
			{
				now=ch[now][t^1];
				tmp+=bin[i];
			}
			else 
				now=ch[now][t];
		}
		return tmp;
	}
}trie;
inline void add(int x,int y,int z)
{
	e[++l].to=y;
	e[l].next=last[x];
	last[x]=l;
	e[l].v=z;
}
inline void dfs(int x,int fa)
{
	for(int i=last[x];i;i=e[i].next)
		if(e[i].to!=fa)
		{
			val[e[i].to]=val[x]^e[i].v;
			dfs(e[i].to,x);
		}
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	bin[0]=1;
	for(int i=1;i<=30;i++)
		bin[i]=bin[i-1]*2;
	read(n);
	for(int i=1;i<n;i++)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	dfs(1,0);
	for(int i=1;i<=n;i++)
		trie.insert(val[i]);
	for(int i=1;i<=n;i++)
		ans=max(ans,trie.query(val[i]));
	write(ans);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const int N=100005;
int n,m,cnt,rt,sum,top,x,y,z,kkk,f[N],size[N],dis[N],last[N],ss[N],sd[N];
bool vis[N];
struct edge
{
	int to,next,v;
}e[2*N];
struct da
{
	int x,y,z,id;
	ll sum;
}a[N];
vector<da>q;
vector<int>aa,ans;
void add(int u,int v,int w)
{
	e[++cnt].to=v;e[cnt].next=last[u];last[u]=cnt;e[cnt].v=w;
}
void getrt(int x,int fa)
{
	f[x]=0;
	size[x]=1;
	for(int i=last[x];i;i=e[i].next)
		if(!vis[e[i].to]&&e[i].to!=fa)
		{
			getrt(e[i].to,x);
			size[x]+=size[e[i].to];
			f[x]=max(f[x],size[e[i].to]);
		}
	f[x]=max(f[x],sum-size[x]);
	if(f[x]<f[rt])rt=x;
}
inline void dfs(int x,int fa)
{
	for (int i=last[x];i;i=e[i].next)
		if (!vis[e[i].to]&&e[i].to!=fa)
		{
			q.push_back(a[(i+1)/2]);
			sd[e[i].to]=sd[x]+1;
			dfs(e[i].to,x);
		}
}
inline bool pd(int x)
{
	if (ss[x]==0)
		return false;
	if (sd[x]==1)
	{
		kkk=x;
		return true;
	}
	for (int i=last[x];i;i=e[i].next)
		if (sd[x]==sd[e[i].to]+1)
			return pd(e[i].to);
}
inline void ql(int x,int y)
{
	ss[x]-=y;
	for (int i=last[x];i;i=e[i].next)
		if (sd[x]==sd[e[i].to]+1)
			ql(e[i].to,y);
}
inline bool cmp(da a,da b)
{
	return a.z>b.z;
}
inline void ddfs(int x,int fa)
{
	ss[x]=1;
	for (int i=last[x];i;i=e[i].next)
		if (!vis[e[i].to]&&e[i].to!=fa)
		{
			ddfs(e[i].to,x);
			ss[x]+=ss[e[i].to];
		}
}
inline void solve(int x)
{
	vis[x]=1;
	q.clear();
	ddfs(x,0);
	for(int i=last[x];i;i=e[i].next)
		if(!vis[e[i].to])
		{
			q.push_back(a[(i+1)/2]);
			sd[e[i].to]=1;
			dfs(e[i].to,x);
		}
	sort(q.begin(),q.end(),cmp);
	int now=0;
	aa.clear();
	for (int i=0;i<q.size();++i)
	{
		if (aa.size()!=0&&now!=q[i].z)
		{
			for (int j=0;j<aa.size();++j)
				ql(a[aa[j]].y,ss[a[aa[j]].y]);
			aa.clear();
		}
		now=q[i].z;
		if (sd[q[i].x]>sd[q[i].y])
			swap(q[i].x,q[i].y);
		if (pd(q[i].y))
		{			
			a[q[i].id].sum+=(ll)ss[q[i].y]*(ss[x]-ss[kkk]);
			aa.push_back(q[i].id);
		}
	}
	for(int i=last[x];i;i=e[i].next)
		if(!vis[e[i].to])
		{
			rt=0;
			sum=size[e[i].to];
			getrt(e[i].to,0);
			solve(rt);
		}
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	read(n);
	for(int i=1;i<n;i++)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
		a[i].x=x;
		a[i].y=y;
		a[i].z=z;
		a[i].id=i;
		a[i].sum=0;
	}
	rt=0;
	sum=n;
	f[0]=n+1;
	getrt(1,0);
	solve(rt);
	ll da=0;
	for (int i=1;i<n;++i)
		if (a[i].sum>da)
		{
			da=a[i].sum;
			ans.clear();
			ans.push_back(i);
		}
		else
		{
			if (a[i].sum==da)
				ans.push_back(i);
		}
	write(da*2);
	putchar(' ');
	write(ans.size());
	putchar('\n');
	for (int i=0;i<ans.size();++i)
	{
		write(ans[i]);
		putchar(' ');
	}
	return 0;
}
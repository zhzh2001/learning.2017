//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("City.in");
ofstream fout("City.out");
struct bian{
	int a,b,c;
}tree[100003];
bool cmp(const bian a,const bian b){
	return a.c<b.c;
}
int to[200003],w[200003],next[200003],head[200003],tot;
int in[100003],out[100003],tim;
inline void addedge(int a,int b,int c){
	to[++tot]=b;  w[tot]=c;  next[tot]=head[a];  head[a]=tot;
}
void dfs(int node,int fa){
	in[node]=++tim;
	for(int i=head[node];i;i=next[i])if(to[i]!=fa)dfs(to[i],node);
	out[node]=++tim;
}

inline bool pd(int x,int y){
	if(in[x]<=in[y]&&out[x]>=out[y]){
		return true;
	}else{
		return false;
	}
}
long long fa[100003],son[100003],f[100003],lian[100003];
int find(int x){
	if(f[x]==x){
		return x;
	}else{
		f[x]=find(f[x]);
		return f[x];
	}
}
long long ans=-99999999999;
int main(){
	//freopen("City.in","r",stdin);
	//freopen("City.out","w",stdout);
	int n;
	fin>>n;
	for(int i=1;i<=n-1;i++){
		fin>>tree[i].a>>tree[i].b>>tree[i].c;
		addedge(tree[i].a,tree[i].b,tree[i].c);
		addedge(tree[i].b,tree[i].a,tree[i].c);
	}
	for(int i=1;i<=n;i++){
		f[i]=i;
		lian[i]=1;
		fa[i]=son[i]=1;
	}
	sort(tree+1,tree+n,cmp);
	dfs(1,0);
	for(int i=1;i<=n-1;i++){
		int p=find(tree[i].a),q=find(tree[i].b);
		if(pd(tree[i].a,tree[i].b)){
			son[tree[i].a]+=lian[q];
			fa[tree[i].b]+=lian[p];
			ans=max(fa[tree[i].a]*son[tree[i].b],ans);
			lian[f[q]]+=lian[f[p]];
			lian[f[p]]=lian[f[q]];
			f[p]=f[q];
		}else{
			fa[tree[i].a]+=lian[q];
			son[tree[i].b]+=lian[p];
			ans=max(fa[tree[i].b]*son[tree[i].a],ans);
			lian[f[p]]+=lian[f[q]];
			lian[f[q]]=lian[f[p]];
			f[q]=f[p];
		}
		//fout<<p<<" "<<q<<" "<<fa[p]<<" "<<fa[q]<<endl;
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
6
1 2 1
1 3 5
3 4 2
3 5 3
3 6 4

out:
16 1
2

*/

#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
//ifstream fin("City.in");
//ofstream fout("City.out");
struct bian{
	int a,b,c;
}tree[100003];
bool cmp(const bian a,const bian b){
	return a.c>b.c;
}
int to[200003],w[200003],next[200003],head[200003],tot;
inline void addedge(int a,int b,int c){
	to[++tot]=b;  w[tot]=c;  next[tot]=head[a];  head[a]=tot;
}
int in[100003],out[100003],tim=0;
void dfs(int node,int fa){
	in[node]=++tim;
	for(int i=head[node];i;i=next[i]){
		if(to[i]!=fa){
			dfs(to[i],node);
		}
	}
	out[node]=++tim;
}
struct segment{
	int l,r,sum[2],tag[2];
}t[400003];
void build(int rt,int l,int r){
	t[rt].l=l,t[rt].r=r;
	t[rt].sum[0]=r-l+1,t[rt].sum[1]=0;
	if(l==r){
		return;
	}
	int mid=(l+r)>>1;
	build(rt*2,l,mid);
	build(rt*2+1,mid+1,r);
}
int querycolor(int rt,int x){
	int l=t[rt].l,r=t[rt].r;
	if(l==r){
		return (t[rt].sum[1]==1);
	}
	int mid=(l+r)>>1;
	if(mid>=x){
		return querycolor(rt*2,x);
	}else if(mid<x){
		return querycolor(rt*2+1,x);
	}
}
int query(int rt,int x,int y,int xx){
	int l=t[rt].l,r=t[rt].r;
	//cout<<rt<<" "<<x<<" "<<y<<" "<<l<<" "<<r<<endl;
	if(l==x&&r==y){
		return t[rt].sum[xx];
	}
	int mid=(l+r)>>1;
	if(mid>=y){
		return query(rt*2,x,mid,xx);
	}else if(mid<x){
		return query(rt*2+1,mid+1,y,xx);
	}else{
		return query(rt*2,x,mid,xx)+query(rt*2+1,mid+1,y,xx);
	}
}
int main(){
	//freopen("City.in","r",stdin);
	//freopen("City.out","w",stdout);
	int n;
	cin>>n;
	for(int i=1;i<=n-1;i++){
		cin>>tree[i].a>>tree[i].b>>tree[i].c;
		addedge(tree[i].a,tree[i].b,tree[i].c);
		addedge(tree[i].b,tree[i].a,tree[i].c);
	}
	sort(tree+1,tree+n,cmp);
	dfs(1,0);
	build(1,1,tim);
	for(int i=1;i<=n-1;i++){
		int x=querycolor(1,in[tree[i].b]);
		int ans=query(1,in[tree[i].b],out[tree[i].b],x);
		cout<<ans<<"  "<<tree[i].b<<endl;
	}
	return 0;
}
/*

in:
2
2 1 5


6
1 2 1
1 3 5
3 4 2
3 5 3
3 6 4


4
1 2 1
2 3 3
3 4 4


4
1 2 1
2 3 1
2 4 1


out:
2 1
1

16 1
2

6 1
3

6 3
1 2 3
*/

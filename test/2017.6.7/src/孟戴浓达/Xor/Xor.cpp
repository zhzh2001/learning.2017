//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("Xor.in");
ofstream fout("Xor.out");
int to[200003],w[200003],next[200003],head[200003],tot;
long long xorsum[100003];
int trie[8000003][2],cnt=2;
int num[100003][35];
long long bin[35];
long long ans=-999999999;
inline void addedge(int a,int b,int c){
	to[++tot]=b;  w[tot]=c;  next[tot]=head[a];  head[a]=tot;
}
void dfs(int node,int fa){
	for(int i=head[node];i;i=next[i]){
		if(to[i]!=fa){
			xorsum[to[i]]=xorsum[node] xor w[i];
			dfs(to[i],node);
		}
	}
}
inline void add(int x){
	int now=1;
	for(int i=32;i>=0;i--){
		if(trie[now][num[x][i]]!=0){
			now=trie[now][num[x][i]];
		}else{
			trie[now][num[x][i]]=cnt++;
			now=trie[now][num[x][i]];
		}
	}
}
inline long long tanxin(int x){
	int now=1;
	long long ret=0;
	for(int i=32;i>=0;i--){
		if(trie[now][!num[x][i]]!=0){
			ret+=bin[i];
			now=trie[now][!num[x][i]];
		}else if(trie[now][num[x][i]]!=0){
			now=trie[now][num[x][i]];
		}else{
			break;
		}
	}
	return ret;
}
int n,a,b,c;
int main(){
	//freopen("Xor.in","r",stdin);
	//freopen("Xor.out","w",stdout);
	fin>>n;
	bin[0]=1;
	for(int i=1;i<=32;i++){
		bin[i]=bin[i-1]*2;
	}
	for(int i=1;i<=n-1;i++){
		fin>>a>>b>>c;
		addedge(a,b,c);
		addedge(b,a,c);
	}
	dfs(1,0);
	for(int i=1;i<=n;i++){
		long long x=xorsum[i];
		for(int j=32;j>=0;j--){
			if(x==0){
				break;
			}
			if(x>=bin[j]){
				x-=bin[j];
				num[i][j]=1;
			}
		}
		add(i);
	}
	for(int i=1;i<=n;i++){
		ans=max(ans,tanxin(i));
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
4
1 2 3
1 3 4
1 4 7

out:
7

*/

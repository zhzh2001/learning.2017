#include<cstdio>
#include<algorithm>
const int N=1e5+3;
int i,j,n,t,fa[N],en[N*2],nxt[N*2],w[N*2];
int RT,lab,s[N<<5][2],a[N];
void dfs(int u,int p)
{
	for(int i=fa[u];i;i=nxt[i])if(en[i]^p)
		a[en[i]]=a[u]^w[i],dfs(en[i],u);
}
void A(int&p,int h,int x)
{
	if(!p)p=++lab;
	if(h<0)return;
	A(s[p][x>>h&1],h-1,x);
}
int Q(int p,int h,int x)
{
	if(!p||h<0)return 0;
	if(j=x>>h&1,s[p][j^1])return (1<<h)+Q(s[p][j^1],h-1,x);
	return Q(s[p][j],h-1,x);
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	for(scanf("%d",&n);++t<n+n-1;)
	{
		scanf("%d%d%d",&i,&j,w+t);
		en[t]=j,nxt[t]=fa[i],fa[i]=t;
		en[++t]=i,nxt[t]=fa[j],fa[j]=t,w[t]=w[t-1];
	}
	for(dfs(1,t=i=0);i++<n;)
	{
		A(RT,30,a[i]);
		t=std::max(t,Q(RT,30,a[i]));
	}
	printf("%d",t);
}

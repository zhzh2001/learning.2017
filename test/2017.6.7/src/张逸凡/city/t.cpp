#include<cstdio>
#include<algorithm>
const int N=1e5+3;
struct E{
	int x,y,z,k;
	bool operator<(const E&a)const{
		return z<a.z;
	}
}a[N];
int i,j,n,x,y,ans[N],f[N],ft[N],sub[N],siz[N];
int t=1,fa[N],en[N*2],nxt[N*2],w[N*2];
void dfs(int u)
{
	for(int i=fa[u];i;i=nxt[i])if(i^ft[u])
		ft[en[i]]=i^1,dfs(en[i]);
}
int g(int i){return i==f[i]?i:f[i]=g(f[i]);}
int main()
{
	for(scanf("%d",&n);++t<n+n;)
	{
		scanf("%d%d%d",&i,&j,w+t);
		en[t]=j,nxt[t]=fa[i],fa[i]=t;
		en[++t]=i,nxt[t]=fa[j],fa[j]=t,w[t]=w[t-1];
	}
	for(dfs(i=1);i++<n;)
		a[i-1]=(E){i,en[ft[i]],w[ft[i]],ft[i]>>1};
	std::sort(a+1,a+n);
	for(i=0;i++<n;f[i]=i)sub[i]=siz[i]=1;
	for(i=1;i<n;)
	{
		for(j=i;a[j].z==a[i].z;++j)
		{
			x=a[j].x,y=a[j].y;
			f[g(x)]=g(y);
			siz[g(y)]+=siz[g(x)];
		}
	}
}

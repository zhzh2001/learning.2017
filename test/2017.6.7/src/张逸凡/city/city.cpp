#include<cstdio>
#include<algorithm>
const int N=1e5+3;
struct E{
	int x,y,z,k;
	bool operator<(const E&a)const{
		return z<a.z;
	}
}a[N];
long long ans[N];
int i,j,k,n,x,y,fx,fy,f[N],ft[N],sub[N],siz[N];
int t=1,fa[N],en[N*2],nxt[N*2],w[N*2];
int g(int i){return i==f[i]?i:f[i]=g(f[i]);}
void dfs(int u)
{
	for(int i=fa[u];i;i=nxt[i])if(i^ft[u])
		ft[en[i]]=i^1,dfs(en[i]);
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	for(scanf("%d",&n);++t<n+n;)
	{
		scanf("%d%d%d",&i,&j,w+t);
		en[t]=j,nxt[t]=fa[i],fa[i]=t;
		en[++t]=i,nxt[t]=fa[j],fa[j]=t,w[t]=w[t-1];
	}
	for(dfs(i=1);i++<n;)
		a[i-1]=(E){i,en[ft[i]],w[ft[i]],ft[i]>>1};
	std::random_shuffle(a+1,a+n);
	std::sort(a+1,a+n);
	for(i=0;i++<n;ft[i]=0,f[i]=i)sub[i]=siz[i]=1;
	for(i=1;i<n;i=k)
	{
		for(j=i;a[j].z==a[i].z;++j)
		{
			ft[x=a[j].x]=y=a[j].y;
			siz[g(y)]+=siz[g(x)],f[g(x)]=g(y);
			for(;y;y=ft[y])sub[y]+=sub[x];
		}
		for(k=j;j-->i;ans[a[j].k]=2ll*sub[a[j].x]*(siz[g(a[j].x)]-sub[a[j].x]));
	}
	for(i=0;++i<n;)*ans=*ans<ans[i]?ans[i]:*ans;
	for(k=i=0;++i<n;)k+=*ans==ans[i];
	printf("%I64d %d\n",*ans,k);
	for(i=0;++i<n;)if(*ans==ans[i])printf("%d ",i);
}

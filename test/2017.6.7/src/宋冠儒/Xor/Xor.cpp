#include<bits/stdc++.h>
using namespace std;
int ans,n,tot,nedge;
struct edge{
	int next,to,v;
}e[200005];
int head[100005],a[40],sum[100005],nxt[3100005][2];
 void add(int a,int b,int w)
{
	e[++nedge].to=b;
	e[nedge].v=w;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 void dfs(int k,int fa)
{
	for (int i=head[k];i;i=e[i].next)
		if (e[i].to!=fa)
		{
			sum[e[i].to]=sum[k]^e[i].v;
			dfs(e[i].to,k);
		}
}
 void divide(int x)
{
	memset(a,0,sizeof(a));
	int len=0;
	while (x)
	{
		len++;
		a[len]=x%2;
		x/=2;
	}
}
 void insert()
{
	int k=0;
	for (int i=31;i>=1;--i)
	{
		int x=a[i];
		if (nxt[k][x]==-1)
		{
			tot++;
			nxt[k][x]=tot;
			nxt[tot][0]=nxt[tot][1]=-1;
		}
		k=nxt[k][x];
	}
}
 int solve()
{
	int k=0,s=0;
	for (int i=31;i>=1;--i)
	{
		int x=a[i]^1;
		if (nxt[k][x]==-1) x=x^1;
		if (a[i]^x==1) s+=(1<<(i-1));
		k=nxt[k][x];
	}
	return s;
}
 int main()
{
	freopen("Xor.in","r",stdin);
	freopen("Xor.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<n;++i)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add(u,v,w);add(v,u,w);
	}
	dfs(1,0);
	nxt[0][0]=nxt[0][1]=-1;
	for (int i=1;i<=n;++i)
	{
		divide(sum[i]);
		insert();
	}
	for (int i=1;i<=n;++i)
	{
		divide(sum[i]);
		ans=max(ans,solve());
	}
	printf("%d",ans);
}

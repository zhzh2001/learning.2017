#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
int n,i,j,k,fx,fy,cnt,tmp,sum,Time,ans;
int fa[N],size[N],Size[N],num[N],head[N],t[N],T[N],id[N],vis[N];
struct E{
	int u,v,len,node;
	bool operator <(const E& w)const{return len<w.len;}
}e[N];
struct ff{int to,nxt,node;}e1[2*N];
long long Max;

int get(int x){return x==fa[x]?x:fa[x]=get(fa[x]);}
void add(int u,int v,int node){
	if(T[u]!=Time)T[u]=Time,head[u]=0;
	e1[++cnt]=(ff){v,head[u],node};
	head[u]=cnt;
}
void Dfs(int now,int fa){
	vis[now]=Time;
	for(int i=head[now];i;i=e1[i].nxt){
		int v=e1[i].to;
		if(v==fa)continue;
		Dfs(v,now);Size[now]+=Size[v];
	}
}
void dfs(int now,int fa,int pre){
	for(int i=head[now];i;i=e1[i].nxt)
		if(e1[i].to!=fa)dfs(e1[i].to,now,e1[i].node);
	if(2ll*Size[now]*(Size[k]-Size[now])==Max)id[++ans]=pre;
	if(2ll*Size[now]*(Size[k]-Size[now])>Max)
		Max=2ll*Size[now]*(Size[k]-Size[now]),id[ans=1]=pre;
}
int main()
{
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<n;i++)
		scanf("%d%d%d",&e[i].u,&e[i].v,&e[i].len),e[i].node=i;
	sort(e+1,e+n);
	for(i=1;i<=n;i++)fa[i]=i,size[i]=1;
	for(i=1;i<n;i=j+1){
		for(j=i+1;j<n&&e[j].len==e[i].len;j++);j--;
		cnt=1;Time++;tmp=0;
		for(k=i;k<=j;k++){
			fx=get(e[k].u);fy=get(e[k].v);
			if(t[fx]!=Time)t[fx]=Time,num[fx]=++tmp,Size[tmp]=size[fx];
			if(t[fy]!=Time)t[fy]=Time,num[fy]=++tmp,Size[tmp]=size[fy];
			add(num[fx],num[fy],e[k].node),add(num[fy],num[fx],e[k].node);
		}for(k=1;k<=tmp;k++)
			if(vis[k]!=Time)Dfs(k,0),dfs(k,0,0);
		for(k=i;k<=j;k++){
			fx=get(e[k].u),fy=get(e[k].v);
			fa[fx]=fy,size[fy]+=size[fx];
		}
	}printf("%I64d %d\n",Max,ans);
	sort(id+1,id+ans+1);
	for(i=1;i<=ans;i++)printf("%d ",id[i]);
	return 0;
}

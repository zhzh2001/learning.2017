#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
#define M 4000000
int n,m,u,v,len,i,k,Min,ans,cnt,tmp,Time,Size;
int mi[35],T[M][2],trie[M][2],head[N],size[N],Max[N],vis[N],Val[N];
struct ff{int to,nxt,len;}e[2*N];

int fast(){
	int t=0;
	char s=getchar();
	while(s<48||s>57)s=getchar();
	while(s>47&&s<58)t=(t<<3)+(t<<1)+s-48,s=getchar();
	return t;
}
void add(int u,int v,int len){
	e[++cnt]=(ff){v,head[u],len};
	head[u]=cnt;
}
void ask(int now,int val,int t,int sum){
	while(t>=0){
		int p=((val&mi[t])>0);
		if(T[now][1-p]!=Time){
			if(T[now][p]!=Time)return;
			now=trie[now][p];
		}else now=trie[now][1-p],sum+=mi[t];
		t--;
	}ans=max(ans,sum);
}
void Insert(int now,int val,int t){
	while(t>=0){
		int p=((val&mi[t])>0);
		if(T[now][p]!=Time)T[now][p]=Time,trie[now][p]=++tmp;
		now=trie[now][p];t--;
	}
}
void dfs(int now,int fa){
	size[now]=1;Max[now]=0;
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(v==fa||vis[v])continue;
		dfs(v,now);size[now]+=size[v];
		Max[now]=max(Max[now],size[v]);
	}Max[now]=max(Max[now],Size-size[now]);
	if(Max[now]<Min)Min=Max[now],k=now;
}
void getans(int now,int fa,int val){
	ask(0,val,30,0);size[now]=1;Val[++m]=val;
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(v==fa||vis[v])continue;
		getans(v,now,val^e[i].len);
		size[now]+=size[v];
	}
}
void work(int t){
	Size=size[t];Min=1e9;k=0;
	dfs(t,0);Time++;tmp=0;vis[k]=1;
	Insert(0,0,30);
	for(int i=head[k];i;i=e[i].nxt){
		if(vis[e[i].to])continue;m=0;
		getans(e[i].to,k,e[i].len);
		for(int j=1;j<=m;j++)Insert(0,Val[j],30);
	}
	for(int i=head[k];i;i=e[i].nxt)
		if(!vis[e[i].to])work(e[i].to);
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	for(i=1,mi[0]=1;i<=30;i++)mi[i]=mi[i-1]<<1;
	n=fast();
	for(i=1;i<n;i++){
		u=fast(),v=fast(),len=fast();
		add(u,v,len),add(v,u,len);
	}work(1);
	printf("%d\n",ans);
	return 0;
}

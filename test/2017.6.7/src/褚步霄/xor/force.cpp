#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
#define M 4000000
int n,u,v,len,i,k,Min,ans,cnt,tmp,Time,Size;
int mi[35],T[M][2],trie[M][2],head[N],size[N],Max[N],vis[N];
struct ff{int to,nxt,len;}e[2*N];

void add(int u,int v,int len){
	e[++cnt]=(ff){v,head[u],len};
	head[u]=cnt;
}
void dfs(int now,int fa,int val){
	if(val>ans)u=i,v=now,ans=val;
	for(int i=head[now];i;i=e[i].nxt){
		int v=e[i].to;
		if(v!=fa)dfs(v,now,val^e[i].len);
	}
}
int main()
{
	freopen("xor.in","r",stdin);
	freopen("force.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		scanf("%d%d%d",&u,&v,&len);
		add(u,v,len),add(v,u,len);
	}for(i=1;i<=n;i++)dfs(i,0,0);
	printf("%d\n",ans);
//	printf("%d %d\n",u,v);
	return 0;
}

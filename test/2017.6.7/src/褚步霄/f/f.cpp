#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
int i,n,f[1000005];
int fast(){
	int t=0;
	char s=getchar();
	while(s<48||s>57)s=getchar();
	while(s>47&&s<58)t=t*10+s-48,s=getchar();
	return t;
}
int dfs(int n){
	if(f[n])return f[n];
	f[n+11]=dfs(n+11);
	f[n]=dfs(f[n+11]);
	return f[n];
}
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	for(i=101;i<=1e6;i++)f[i]=i-10;
	while((n=fast())!=0)printf("%d\n",dfs(n));
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn=100005;
bool vis[maxn];
struct pp2{
	int val,id;
}q[maxn];
ll ans[maxn],mxx;
int qq[maxn],cnt,mx[maxn],sum[maxn],size[maxn],n,u,root,f[maxn],v,w,all,head[maxn],tot;
struct pp{
	int link,val,next;
}a[maxn*2];
void getroot(int x,int fa)
{
	size[x]=1; f[x]=0;
	for (int i=head[x];i;i=a[i].next){
		int u=a[i].link;
		if (u!=fa&&!vis[u]){getroot(u,x); size[x]+=size[u]; f[x]=max(f[x],size[u]);}
	}
	f[x]=max(f[x],all-size[x]);
	if (f[x]<f[root]) root=x;
}
void dfs(int x,int fa)
{
	for (int i=head[x];i;i=a[i].next){
		int u=a[i].link;
		if (u!=fa&&!vis[u]){
			mx[u]=max(a[i].val,mx[x]); q[++cnt].val=mx[u]; q[cnt].id=(i+1)/2;
			dfs(u,x);
		}
	}	
}
inline bool cmp(pp2 x,pp2 y){return x.val<y.val;}
void calc(int x)
{
	mx[x]=0; cnt=0;
	for (int i=head[x];i;i=a[i].next){
		int u=a[i].link;
		if (!vis[u]) q[++cnt].id=(i+1)/2,q[cnt].val=a[i].val,mx[u]=a[i].val,dfs(u,x);
	}
	sort(q+1,q+1+cnt,cmp); int pre=0;
	for (int i=cnt;i;i--){
		ans[q[i].id]+=i;
		if (q[i].id=q[i+1].id) pre++,ans[q[i].id]+=pre; else pre=0;
	}
}
void solve(int x)
{
	vis[x]=1; calc(x);
	for (int i=head[x];i;i=a[i].next){
		int u=a[i].link;
		if (!vis[u]){root=0; all=size[u]; getroot(u,x); solve(root);}
	}
}
void add(int u,int v,int w){a[++tot].link=v; a[tot].next=head[u]; a[tot].val=w; head[u]=tot;}
int main()
{
	scanf("%d",&n);
	for (int i=1;i<n;i++) scanf("%d%d%d",&u,&v,&w),add(u,v,w),add(v,u,w);
	root=0; f[0]=n; all=n; getroot(1,0); solve(root);
	sort(ans+1,ans+n); tot=0;
	for (int i=1;i<n;i++) mxx=max(mxx,ans[i]);
	printf("%lld ",mxx*2);
	for (int i=1;i<n;i++)
		if (mxx==ans[i]) qq[++tot]=i;
	printf("%d\n",tot);
	for (int i=1;i<=tot;i++) printf("%d ",qq[i]);
	return 0;
}

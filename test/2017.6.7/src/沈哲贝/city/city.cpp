#include<algorithm>
#include<memory.h>
#include<cstring>
#include<cmath>
#include<cstdio>
#define ll long long
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
struct edge{	ll x,y,w,id,zyy;	}e[maxn];
ll size[maxn],dep[maxn],fa[maxn],head[maxn],next[maxn],vet[maxn],val[maxn],n,siz,tot,mul;
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);}
void insert(ll x,ll y){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
bool cmp(edge a,edge b){	return a.w<b.w;}
bool cmp1(edge a,edge b){	return dep[a.y]>dep[b.y];}
bool cmp2(edge a,edge b){	if (a.zyy!=b.zyy)	return a.zyy>b.zyy;	return a.id<b.id;}
void dfs(ll x,ll f){
	for(ll i=head[x];i;i=next[i])
	if (vet[i]!=f)	dep[vet[i]]=dep[x]+1,dfs(vet[i],x);
}
int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();	siz=n-1;
	For(i,1,siz)	e[i].x=read(),e[i].y=read(),e[i].w=read(),e[i].id=i,insert(e[i].x,e[i].y),insert(e[i].y,e[i].x);
	sort(e+1,e+siz+1,cmp);
	dfs(1,-1);
	For(i,1,siz)	if (dep[e[i].x]>dep[e[i].y])	swap(e[i].x,e[i].y);
	For(i,1,n)	size[i]=1,fa[i]=i;
	for(ll i=1,j;i<=siz;i=j+1){
		j=i;
		while(e[j+1].w==e[i].w&&j<siz)	++j;
		sort(e+i,e+j+1,cmp1);
		For(k,i,j){
			e[k].zyy=size[find(e[k].y)];
			size[find(e[k].x)]+=size[find(e[k].y)];
			fa[find(e[k].y)]=find(e[k].x);
		}
		For(k,i,j)	e[k].zyy=e[k].zyy*(size[find(e[k].x)]-e[k].zyy);
	}
	sort(e+1,e+siz+1,cmp2);
	For(i,1,siz)	if (e[i].zyy==e[1].zyy)	++mul;
	printf("%lld %lld\n",e[1].zyy*2,mul);
	For(i,1,mul)	printf("%lld ",e[i].id);
}
/*
4
1 2 1
2 3 1
2 4 1
*/

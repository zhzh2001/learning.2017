#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#define ll long long
#define maxn 1010
#define mod 1000000007
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
struct edge{ll x,y,w,zyy,id;}e[maxn];
ll tot=1,q[maxn],head[maxn],next[maxn],vet[maxn],val[maxn],n,x,y,sum,sz;
void insert(ll x,ll y,ll w){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;}
void dfs(ll x,ll pre){
	For(i,1,tot)	++e[q[i]].zyy;	ll sum=tot,kkk[maxn];	For(i,1,tot)	kkk[i]=q[i];
	for(ll i=head[x];i;i=next[i])
	if (vet[i]!=pre)	{
		if (!tot)	q[++tot]=i/2;
		else{
			if (val[i]>e[q[1]].w)	q[tot=1]=i/2;
			else if (val[i]==e[q[tot]].w)	q[++tot]=i/2;
		}dfs(vet[i],x);
		tot=sum;
		For(i,1,tot)	q[i]=kkk[i];
	}
}
bool cmp(edge a,edge b){
	if (a.zyy!=b.zyy)	return a.zyy>b.zyy;
	return a.id<b.id;
}
int main(){
	freopen("city.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n-1){
		e[i].x=read();	e[i].y=read();	e[i].w=read();
		insert(e[i].x,e[i].y,e[i].w);	insert(e[i].y,e[i].x,e[i].w);
		e[i].id=i;
	}
	For(i,1,n)	tot=0,dfs(i,-1);
	sz=n-1;
	sort(e+1,e+sz+1,cmp);
	sum=1;
	For(i,2,sz)	if (e[i].zyy==e[i-1].zyy)	++sum;	else	break;
	printf("%lld %lld\n",e[1].zyy,sum);
	For(i,1,sum)	printf("%lld ",e[i].id);
}
/*
4
1 2 1
2 3 1
2 4 1
*/
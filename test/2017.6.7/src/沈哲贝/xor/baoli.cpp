#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define maxn 501000
#define mod 10000007
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll head[maxn],next[maxn],vet[maxn],val[maxn],n,tot,ans;
void insert(ll x,ll y,ll w){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;}
void dfs(ll x,ll pre,ll sum){
	ans=max(ans,sum);
	for(ll i=head[x];i;i=next[i])
		if (vet[i]!=pre)	dfs(vet[i],x,sum^val[i]);
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,2,n){
		ll x=read(),y=read(),w=read();
		insert(x,y,w);	insert(y,x,w);
	}
	For(i,1,n)	dfs(i,-1,0);
	writeln(ans);
}
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<map>
#define ll int
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll next[maxn],head[maxn],vet[maxn],val[maxn],c[maxn*32][2],q[maxn],ans,tot,n,cnt;
void insert(ll x,ll y,ll w){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;}
void insert(ll x){
	ll now=0,to;
	FOr(i,30,0){
		to=(x>>i)&1;
		if (!c[now][to])	c[now][to]=++cnt;
		now=c[now][to];
	}
}
void calc(ll x){
	ll now=0,to,answ=0;
	FOr(i,30,0){
		to=!((x>>i)&1);
		if (!c[now][to])	now=c[now][to^1];
		else now=c[now][to],answ+=1<<i;
	}
	ans=max(ans,answ);
}
void dfs(ll x,ll f,ll dis){
	insert(dis);	q[++tot]=dis;
	for(ll i=head[x];i;i=next[i])
	if (vet[i]!=f)	dfs(vet[i],x,dis^val[i]);
}
int main(){
	freopen("xor.in","r",stdin);
	freopen("xor.out","w",stdout);
	n=read();
	For(i,2,n){
		ll x=read(),y=read(),c=read();
		insert(x,y,c);	insert(y,x,c);
	}tot=0;
	dfs(1,0,0);
	For(i,1,tot)	calc(q[i]);
	writeln(ans);
}
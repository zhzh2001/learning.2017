#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<bitset>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define p 998244353
#define maxn 50010
using namespace std;
const ll inf=1e9;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll n,f[10000];	bool vis[10000];
inline ll calc(ll x){
	if (x>100)	return x-10;
	if (vis[x])	return f[x];
	vis[x]=1;
	return f[x]=calc(calc(x+11));
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	ll n=read();
	FOr(i,100,1)	calc(i);
	while(n){
		writeln(calc(n));
		n=read();
	}
}
#include <fstream>
#include <vector>
#include <stack>
#include <iostream>
#include <algorithm>
using namespace std;

ifstream fin("City.in");
ofstream fout("City.out");

const int N = 100005;

#define foreach(i, x) for (typeof(x.begin()) i = x.begin(); i != x.end(); ++i)
	
typedef long long ll;

struct Edge {
	int u, v, w, id;
	Edge(int u = 0, int v = 0, int w = 0, int id = 0)
		:u(u), v(v), w(w), id(id) {}
		
	friend bool operator < (const Edge &a, const Edge &b) {
		return a.w > b.w;
	}
};

vector<Edge> G[N];
vector<Edge> e;
int size[N], fa[N];

void dfs(int x, int fa) {
	::fa[x] = fa;
	size[x] = 1;
	foreach (i, G[x]) {
		if (i->v != fa) {
			dfs(i->v, x);
			size[x] += size[i->v];
		}
	}
}

int work(int x, int y) {
	for (; fa[x] != x; x = fa[x]) {
		size[x] -= y;
	}
	return size[x] -= y;
}

int find(int x) {
	return x == fa[x] ? x : find(fa[x]);
}

int main() {
	int n;
	fin >> n;
	for (int i = 1, a, b, c; i < n; i++) {
		fin >> a >> b >> c;
		G[a].push_back(Edge(a, b, c, i));
		G[b].push_back(Edge(b, a, c, i));
	}
	dfs(1, 1);
	for (int i = 1; i <= n; i++)
		foreach (j, G[i]) {
			e.push_back(Edge(j->u, j->v, j->w, j->id));
		}
	ll ans1 = 0;
	sort(e.begin(), e.end());
	int tim = 0;
	vector<int> res;
	ll cnt = 2;
	foreach (i, e) {
		int u = i->u, v = i->v;
		vector<Edge>::iterator tmp = i; ++tmp;
		
		if (fa[u] == v) swap(u, v);
		int fx = find(u), fy = find(v);
		if (fx != fy) continue;
		int t = work(u, size[v]);
		fa[v] = --tim;
		// ans1 = max(ans1, size[u] * size[v]);
		if ((ll)t * size[v] * cnt > ans1) {
			ans1 = (ll)t * size[v] * cnt;
			res.clear();
			res.push_back(i->id);
		} else if ((ll)t * size[v] * cnt == ans1)
			res.push_back(i->id);
		cnt = 2;
	}
	fout << ans1 << ' ' << res.size() << endl;
	foreach (i, res)
		fout << *i << ' ';
	fout << endl;
	return 0;
}
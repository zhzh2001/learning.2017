#include <fstream>
#include <vector>
#include <iostream>
using namespace std;

#define foreach(i, x) for (typeof(x.begin()) i = x.begin(); i != x.end(); ++i)

ifstream fin("Xor.in");
ofstream fout("Xor.out");

const int N = 100005;

vector<pair<int, int> > G[N];
int n;
int val[N], sum[N];
int q[N], fa[N];

bool check(int &label) {
	int cnt1 = 0, cnt2 = 0;
	for (int i = 1; i <= n; i++) {
		if (G[i].size() == 1) cnt1++, label = i;
		if (G[i].size() == 2) cnt2++;
	}
	return (cnt1 == 2 && cnt2 == n - 2);
}

void dfs(int x, int fa, int dep = 0) {
	foreach (i, G[x]) {
		if (i->first != fa) {
			val[dep + 1] = i->second;
			dfs(i->first, x, dep + 1);
		}
	}
}

int main() {
	fin >> n;
	for (int i = 1, a, b, c; i < n; i++) {
		fin >> a >> b >> c;
		G[a].push_back(make_pair(b, c));
		G[b].push_back(make_pair(a, c));
	}
	int label;
	if (check(label)) {
		dfs(label, -1);
		for (int i = 1; i < n; i++) sum[i] = sum[i - 1] ^ val[i];
		int l = 1, r = 1, ans = 0;
		for (int i = 1; i < n; i++) {
			int tmp = sum[q[r - 1]] ^ sum[q[l] - 1];
			q[r++] = i;
			for (; l < r && ((sum[q[r - 1]] ^ sum[q[l] - 1]) < (sum[q[r - 1]] ^ sum[q[l]])
				|| (tmp ^ val[q[r - 1]]) < tmp); ++l);
			ans = max(ans, sum[q[r - 1]] ^ sum[q[l] - 1]);
		}
		fout << ans << endl;
		return 0;
	}
	return 0;
}
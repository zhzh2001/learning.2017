#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <cassert>
#include <fstream>
using namespace std;

#define private   private:
#define public    public:
#define protected protected:

#ifdef ONLINE_JUDGE
#define fastcall inline
#else
#pragma GCC optimize 2
#define fastcall __attribute__((optimize("Ofast"))) __attribute__(( __always_inline__))
#define emit __attribute__((optimize("Ofast")))
#endif

#define foreach(i, x) for (typeof((x).begin()) i = (x).begin(); i != (x).end(); ++i)

typedef long long ll;

class InputStream {
	public fastcall explicit InputStream(const char *file_name, size_t buf_size = 1048576)
		: size(buf_size) {
		file = fopen(file_name, "r");
		assert(file);
		buf = new char[buf_size];
		fseek(file, 0, SEEK_SET);
		fread(buf, 1, size, file);
		now = buf;
	}

	public fastcall ~InputStream() {
		delete buf;
	}

	public fastcall void close() {
		delete buf;
		file = NULL;
	}

	public fastcall friend InputStream &operator >>(InputStream &is, char &b) {
		if (is.now - is.buf == is.size) {
			fread(is.buf, 1, is.size, is.file);
			is.now = is.buf;
		}
		b = *(is.now++);
		return is;
	}

	public fastcall friend InputStream &operator >>(InputStream &is, int &b) {
		char tmp = 0, flag = 0;
		for (; !isdigit(tmp); is >> tmp) flag += tmp == '-';
		for (b = 0; isdigit(tmp); is >> tmp) b = b * 10 + tmp - '0';
		if (flag) b = -b;
		return is;
	}

	private FILE *file;
	private char *buf, *now;
	private int size;
};

InputStream fin("F.in");

const int N = 105;
int f[N];

FILE *fout = fopen("F.out", "w");

namespace fastO {
	const int SIZE = 1000000;
	char buf[SIZE], *end = buf + SIZE, *p = buf;
	int d[15];
	emit inline void write(int x) {
		if (end - p < 20) {
			fwrite(buf, 1, p - buf, fout);
			p = buf;
		}
		if (x < 0) {
			*p++ = '-';
			x = -x;
		}
		int k = 0;
		do
			d[++k] = x % 10;
		while (x /= 10);
		for (; k; k--)
			*p++ = d[k] + '0';
	}
	emit inline void flush() {
		fwrite(buf, 1, p - buf, fout);
	}
};

using namespace fastO;

emit inline int work(int x) {
	if (x > 100) return x - 10;
	if (~f[x]) return f[x];
	return f[x] = work(work(x + 11));
}

emit int main() {
	memset(f, 0xff , sizeof f);
	int x;
	for (fin >> x; ; fin >> x) {
		if (x == 0) break;
		write(work(x));
		*p++ = '\n';
	}
	flush();
	return 0;
}
#include<iostream>
#include<cstdio>
#include<ctime>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100001
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n;
int mx,ans[N],p;
int f[N],q[N],top;
int size[N];
struct node{ int u,v,w,num; }edge[N];
int head[N],next[N*2],to[N*2],w[N*2],cnt;
bool cmp(node a,node b){ return a.w>b.w; }

void add(int a,int b,int c){
	to[++cnt]=b; next[cnt]=head[a]; head[a]=cnt; w[cnt]=c;
	to[++cnt]=a; next[cnt]=head[b]; head[b]=cnt; w[cnt]=c;
	edge[cnt/2].u=a; edge[cnt/2].v=b; edge[cnt/2].w=c; edge[cnt/2].num=cnt/2;
}

void dfs(int x,int fa){
	f[x]=fa; size[x]=1;
	for(int i=head[x];i;i=next[i])
		if(to[i]!=fa){
			dfs(to[i],x);
			size[x]+=size[to[i]];
		}
	return;
}

int getf(int x){
	while(f[x]!=x) x=f[x];
	return x;
}

void Delete(int a,int b){
	if(f[a]==b) swap(a,b);
	while(f[a]!=a){	size[a]-=size[b]; a=f[a]; }
	size[a]-=size[b]; f[b]=b;
}

void query(int a,int b,int j){
	if(f[a]==b) swap(a,b);
	int fa=getf(a);
	int num=(size[fa]-size[b])*size[b];
	if(num>mx){ mx=num; p=0; ans[p++]=j; }
	else if(num==mx){ ans[p++]=j; }
}

int main(){
	freopen("city.in","r",stdin);
	freopen("city.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int a=read(),b=read(),c=read();
		add(a,b,c);
	}
	dfs(1,1); top=0;
	sort(edge+1,edge+n,cmp);
	for(int i=1;i<n;i++){
		if(clock()>=950) break;
		if(edge[i].w!=edge[i-1].w){
			for(int j=0;j<top;j++) Delete(edge[q[j]].u,edge[q[j]].v);
			top=0;
		}
		q[top++]=i;
		query(edge[i].u,edge[i].v,edge[i].num);
	}
	printf("%d %d\n",mx*2,p);
	sort(ans,ans+p);
	for(int i=0;i<p;i++) printf("%d ",ans[i]);
	return 0;
}

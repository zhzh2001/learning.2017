#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int f[120];

int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	for(int i=101;i<=119;i++) f[i]=i-10;
	for(int i=100;i>=1;i--) f[i]=f[f[i+11]];
	while(1){
		int x=read(); if(x==0) return 0;
		if(x>=101) printf("%d\n",x-10);
		else printf("%d\n",f[x]);
	}
	return 0;
}

#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
struct edge{
	int x, y, len, id, rx;
	bool operator < (const edge &b) const {
		return len < b.len;
	}
}e[N];
int fa[N], _fa[N], sz[N], ans[N], cnt;
ll mx;
int find(int x){return _fa[x]==x?x:_fa[x]=find(_fa[x]);}
int _find(int x){return fa[x]==x?x:find(fa[x]);}
void splay(int x){
	int y = fa[x];
	if(y == x)return; splay(y);
	fa[y] = fa[x] = x;
	sz[y] -= sz[x]; sz[x] += sz[y];
}
int main(){
	freopen("city.in", "r", stdin);
	freopen("city.out", "w", stdout);
	int n = read();
	for(int i = 1; i < n; i++){
		e[i].x = read(); e[i].y = read(); e[i].id = i;
		e[i].len = read(); fa[i] = i; sz[i] = 1; _fa[i] = i;
	} fa[n] = n; sz[n] = 1; _fa[n] = n;
	sort(e+1, e+n);
	for(int i = 1, j; i < n; i=j+1){
		for(j=i;j<n&&e[j+1].len==e[i].len;j++);
		for(int k = i; k <= j; k++){
			int x = e[k].x, y = e[k].y;
			if(find(x) == find(y)) continue;
			splay(x);
			fa[x] = y; sz[y] += sz[x];
			_fa[find(x)] = find(y);
			while(fa[y]!=y)sz[y=fa[y]] += sz[x];
		}
		for(int k = i; k <= j; k++){
			ll size = 1ll*sz[e[k].x]*(sz[_find(e[k].y)]-sz[e[k].x]);
			if(size > mx) mx = size, ans[cnt=1] = e[k].id;
			else if(size == mx) ans[++cnt] = e[k].id;
		}
	}
	printf("%lld %d\n", mx<<1, cnt);
	sort(ans+1, ans+cnt+1);printf("%d", ans[1]);
	for(int i = 2; i <= cnt; i++)
		printf(" %d", ans[i]);puts("");
}
#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int f[120], x;
int get(int x){
	if(x > 100) return x-10;
	if(f[x]) return f[x];
	return f[x] = get(get(x+11));
}
int main(){
	freopen("f.in", "r", stdin);
	freopen("f.out", "w", stdout);
	while(x=read())printf("%d\n", get(x));
	return 0;
}
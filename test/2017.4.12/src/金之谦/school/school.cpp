#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{
	int x,p,s,t;
};
inline bool cmp(ppap a,ppap b){return a.s<b.s;}
queue<int>q;
vector<ppap>a[200001];
int n,m;
int aa[200001]={0},rp[200001],dist[200001];
bool vis[300001]={0},b[300001]={0};
int nedge=0,head[1000001],nex[1000001],p[1000001];
inline int erfen(int x,int p){
	int l=0,r=aa[p]-1,ans=aa[p]+1;
	while(l<=r){
		int mid=(l+r)>>1;
		if(a[p][mid].s<x)l=mid+1;
		else ans=mid,r=mid-1;
	}
	return ans;
}
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void spfa(){
	for(int i=1;i<=m;i++)dist[i]=-1;
	for(int i=0;i<aa[1];i++)dist[a[1][i].x]=a[1][i].s;
	for(int i=aa[1]-1;i>=0;i--){
		vis[a[1][i].x]=1;q.push(a[1][i].x);
		while(!q.empty()){
			int now=q.front();q.pop();
			for(int k=head[now];k;k=nex[k])if(dist[p[k]]<dist[now]){
				dist[p[k]]=dist[now];
				if(!vis[p[k]]){
					q.push(p[k]);vis[p[k]]=1;
				}
			}
			vis[now]=0;
		}
	}
}
struct qu{
	int x,v;
}xw[100001];
int ans[100001],prr=0;
inline bool cmp1(qu a,qu b){return a.v<b.v;}
struct ppap1{
	int x,v;
}pr[200001];
inline bool prc(ppap1 a,ppap1 b){return a.v<b.v;}
int main()
{
	freopen("school.in","r",stdin);
	freopen("school.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		ppap x;x.x=i;
		int p=read();x.p=read();x.s=read();x.t=read();
		aa[p]++;if(p==1)b[i]=1,rp[i]=x.s;
		a[p].push_back(x);
		if(x.p==n){
			prr++;pr[prr].x=i;pr[prr].v=x.t;
		}
	}
	for(int i=1;i<=n;i++)sort(a[i].begin(),a[i].end(),cmp);
	for(int i=1;i<=n;i++){
		for(int j=0;j<aa[i];j++){
			int x=erfen(a[i][j].t,a[i][j].p);
			for(int k=x;k<aa[a[i][j].p];k++)addedge(a[i][j].x,a[a[i][j].p][k].x);
		}
	}
	spfa();
	int q=read();
	for(int i=1;i<=q;i++)xw[i].x=i,xw[i].v=read();
	sort(xw+1,xw+q+1,cmp1);
	int anss=-1,rp=0;
	sort(pr+1,pr+prr+1,prc);
	for(int i=1;i<=q;i++){
		while(pr[rp+1].v<=xw[i].v&&rp<prr)rp++,anss=max(anss,dist[pr[rp].x]);
		ans[xw[i].x]=anss;
	}
	for(int i=1;i<=q;i++)printf("%d\n",ans[i]);
	return 0;
}

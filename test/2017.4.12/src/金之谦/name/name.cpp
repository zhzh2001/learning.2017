#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll MOD1=1000007;
const ll MOD2=1299709;
char c[200001];
ll ha1[1000007][3]={0},ha2[1299709][3]={0};
inline ll h1(ll x,ll y){
	ll xx=x+200000,yy=y+200000;
	ll i=(xx*MOD2%MOD1+yy%MOD1)%MOD1;
	while(ha1[i][0]&&(ha1[i][0]!=x||ha1[i][1]!=y))i=(i+1)%MOD1;
	return i;
}
inline ll h2(ll x,ll y){
	ll xx=x+200000,yy=y+200000;
	ll i=(xx*MOD1%MOD2+yy%MOD2)%MOD2;
	while(ha2[i][0]&&(ha2[i][0]!=x||ha2[i][1]!=y))i=(i+1)%MOD2;
	return i;
}
int main()
{
	freopen("name.in","r",stdin);
	freopen("name.out","w",stdout);
	ll n,q,l,y,ans;q=l=y=ans=0;scanf("%I64d",&n);
	scanf("%s",c+1);
	for(int i=0;i<1000007;i++)ha1[i][2]=-1;
	for(int i=0;i<1299709;i++)ha2[i][2]=-1;
	int p1=h1(0,0),p2=h2(0,0);
	ha1[p1][2]=ha2[p2][2]=0;
	for(int i=1;i<=n;i++){
		if(c[i]=='Q')q++;
		if(c[i]=='L')l++;
		if(c[i]=='Y')y++;
		p1=h1(q-l,l-y),p2=h2(q-l,l-y);
		if(ha1[p1][2]!=-1)ans=max(ans,i-ha1[p1][2]);
		else{
			ha1[p1][0]=ha2[p2][0]=q-l;
			ha1[p1][1]=ha2[p2][1]=l-y;
			ha1[p1][2]=ha2[p2][2]=i;
		}
	}
	if(!ans)puts("OrzQLY");
	else printf("%I64d",ans);
	return 0;
}

    #include<iostream>  
    #include<cstdio>  
    #include<cstring>  
    #include<algorithm>  
    #include<queue>  
    #define N 100005  
    using namespace std;  
      
    int n,m,pt,cnt,f[N],g[N<<2],ans[N]; struct edg{ int x,y,s,t; }e[N<<2]; struct node{ int x,y; }a[N],c[N<<2];  
    priority_queue<node,vector<node>,greater<node> > q;  
    int read(){  
        int x=0; char ch=getchar();  
        while (ch<'0' || ch>'9') ch=getchar();  
        while (ch>='0' && ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }  
        return x;  
    }  
    bool cmp(edg u,edg v){ return u.s<v.s; }  
    bool operator <(node u,node v){ return u.x<v.x; }  
    bool operator >(node u,node v){ return u.x>v.x; }  
    int main(){  
		freopen("school.in","r",stdin);
		freopen("school.out","w",stdout);
        n=read(); m=read(); int i; node t;  
        for (i=1; i<=m; i++){  
            e[i].x=read(); e[i].y=read(); e[i].s=read(); e[i].t=read();  
        }  
        sort(e+1,e+m+1,cmp);  
        memset(f,-1,sizeof(f)); f[1]=1000000000;  
        for (i=1; i<=m; i++){  
            while (!q.empty() && q.top().x<=e[i].s){  
                t=q.top(); q.pop();  
                f[e[t.y].y]=max(f[e[t.y].y],g[t.y]);  
            }  
            g[i]=min(e[i].s,f[e[i].x]);  
            t.x=e[i].t; t.y=i; q.push(t);  
            if (e[i].y==n){  
                c[++cnt].x=e[i].t; c[cnt].y=g[i];  
            }  
        }  
        sort(c+1,c+cnt+1);  
        pt=read();  
        for (i=1; i<=pt; i++){  
            a[i].x=read(); a[i].y=i;  
        }  
        sort(a+1,a+pt+1); int now=-1,j=1;  
        for (i=1; i<=pt; i++){  
            for(; j<=cnt && c[j].x<=a[i].x; j++) now=max(now,c[j].y);  
            ans[a[i].y]=now;  
        }  
        for (i=1; i<=pt; i++) printf("%d\n",ans[i]);  
        return 0;  
    }  
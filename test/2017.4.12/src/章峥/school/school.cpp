#include<fstream>
#include<algorithm>
#include<queue>
#include<cstring>
#include<cstdio>
using namespace std;
ifstream fin("school.in");
const int N=100005,M=300005,SIZE=1e6;
char buf[SIZE],*p=buf;
void writeln(int x)
{
	if(x<0)
		*p++='-',x=-x;
	static int dig[15];
	int cc=0;
	do
		dig[++cc]=x%10;
	while(x/=10);
	for(;cc;cc--)
		*p++=dig[cc]+'0';
	*p++='\n';
}
struct edge
{
	int u,v,s,t;
	bool operator<(const edge& rhs)const
	{
		return s<rhs.s;
	}
}e[M];
int f[N],g[M],ans[N];
struct node
{
	int t,id;
	node(int t=0,int id=0):t(t),id(id){}
	bool operator<(const node& rhs)const
	{
		return t<rhs.t;
	}
	bool operator>(const node& rhs)const
	{
		return t>rhs.t;
	}
}a[M],q[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
		fin>>e[i].u>>e[i].v>>e[i].s>>e[i].t;
	sort(e+1,e+m+1);
	priority_queue<node,vector<node>,greater<node> > Q;
	int cc=0;
	memset(f,-1,sizeof(f));
	f[1]=0x3f3f3f3f;
	for(int i=1;i<=m;i++)
	{
		while(!Q.empty()&&Q.top().t<=e[i].s)
		{
			node k=Q.top();Q.pop();
			f[e[k.id].v]=max(f[e[k.id].v],g[k.id]);
		}
		g[i]=min(e[i].s,f[e[i].u]);
		Q.push(node(e[i].t,i));
		if(e[i].v==n)
		{
			a[++cc].id=g[i];
			a[cc].t=e[i].t;
		}
	}
	sort(a+1,a+cc+1);
	fin>>m;
	for(int i=1;i<=m;i++)
	{
		fin>>q[i].t;
		q[i].id=i;
	}
	sort(q+1,q+m+1);
	int now=-1,j=1;
	for(int i=1;i<=m;i++)
	{
		for(;j<=cc&&a[j].t<=q[i].t;j++)
			now=max(now,a[j].id);
		ans[q[i].id]=now;
	}
	for(int i=1;i<=m;i++)
		writeln(ans[i]);
	FILE *fout=fopen("school.out","w");
	fwrite(buf,1,p-buf,fout);
	fclose(fout);
	return 0;
}
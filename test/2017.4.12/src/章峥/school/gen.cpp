#include<bits/stdc++.h>
using namespace std;
ofstream fout("school.in");
int main()
{
	default_random_engine gen(time(NULL));
	int n,m;
	cin>>n>>m;
	fout<<n<<' '<<m<<endl;
	uniform_int_distribution<> dt(0,8.64e7);
	while(m--)
	{
		uniform_int_distribution<> tst(1,2);
		uniform_int_distribution<> dn(1,n);
		int s=dt(gen),t=dt(gen);
		if(s>t)
			swap(s,t);
		switch(tst(gen))
		{
			case 1:fout<<dn(gen)<<' '<<dn(gen)<<' '<<s<<' '<<t<<endl;break;
			case 2:fout<<1<<' '<<n<<' '<<s<<' '<<t<<endl;break;
		}
	}
	int q;
	cin>>q;
	fout<<q<<endl;
	while(q--)
		fout<<dt(gen)<<endl;
	return 0;
}
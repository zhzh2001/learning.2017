#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("flower.in");
ofstream fout("flower.out");
const int N=300005;
int n,a[N],b[N],f[N],g[N];
struct BIT
{
	int tree[N];
	void clear()
	{
		memset(tree,0,sizeof(tree));
	}
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	memcpy(b,a,sizeof(b));
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
		a[i]=lower_bound(b+1,b+n+1,a[i])-b;
	for(int i=1;i<=n;i++)
	{
		T.modify(a[i],1);
		f[i]=i-T.query(a[i]);
	}
	T.clear();
	for(int i=n;i;i--)
	{
		T.modify(a[i],1);
		g[i]=n-i+1-T.query(a[i]);
	}
	long long ans=0;
	for(int i=1;i<=n;i++)
		ans+=min(f[i],g[i]);
	fout<<ans<<endl;
	return 0;
}
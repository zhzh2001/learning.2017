#include<bits/stdc++.h>
using namespace std;
ifstream fin("name.in");
ofstream fout("name.ans");
const int N=10000;
int cnt[N][3];
int main()
{
	int n;
	string s;
	fin>>n>>s;
	int ans=0;
	for(int i=0;i<n;i++)
	{
		if(i)
		{
			cnt[i][0]=cnt[i-1][0];
			cnt[i][1]=cnt[i-1][1];
			cnt[i][2]=cnt[i-1][2];
		}
		switch(s[i])
		{
			case 'Q':cnt[i][0]++;break;
			case 'L':cnt[i][1]++;break;
			case 'Y':cnt[i][2]++;break;
		}
		if(cnt[i][0]==cnt[i][1]&&cnt[i][1]==cnt[i][2])
			ans=i+1;
		else
			for(int j=0;j<i;j++)
				if(cnt[i][0]-cnt[j][0]==cnt[i][1]-cnt[j][1]&&cnt[i][1]-cnt[j][1]==cnt[i][2]-cnt[j][2])
				{
					ans=max(ans,i-j);
					break;
				}
	}
	if(ans)
		fout<<ans<<endl;
	else
		fout<<"OrzQLY\n";
	return 0;
}
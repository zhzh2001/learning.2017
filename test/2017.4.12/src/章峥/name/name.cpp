#include<fstream>
#include<string>
#include<map>
using namespace std;
ifstream fin("name.in");
ofstream fout("name.out");
int main()
{
	int n;
	string s;
	fin>>n>>s;
	typedef pair<int,int> pii;
	map<pii,int> S;
	int ans=0,cq=0,cl=0,cy=0;
	S[make_pair(0,0)]=-1;
	for(int i=0;i<n;i++)
	{
		switch(s[i])
		{
			case 'Q':cq++;break;
			case 'L':cl++;break;
			case 'Y':cy++;break;
		}
		pii now=make_pair(cq-cl,cl-cy);
		map<pii,int>::iterator j=S.find(now);
		if(j!=S.end())
			ans=max(ans,i-j->second);
		else
			S[now]=i;
	}
	if(ans)
		fout<<ans<<endl;
	else
		fout<<"OrzQLY\n";
	return 0;
}
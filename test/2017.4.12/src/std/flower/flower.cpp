#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=a;i>=b;i--)
#define mem(a,b) memset(a,b,sizeof a)
inline int read()
{
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';x=(x<<1)+(x<<3)+c-'0',c=getchar());
	return f?-x:x;
}
typedef long long LL;
inline void write(LL x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+48);
}
const int N=300000+10;
int n,m,A[N],B[N],f[N],g[N];
LL Ans;
inline void Update(int x)
{
	for(;x;x-=x&-x)B[x]++;
}
inline int Query(int x)
{
	int res=0;
	for(;x<=m;x+=x&-x)res+=B[x];
	return res;
}
inline int Find(int x)
{
	for(int L=1,R=m;L<=R;)
	{
		int Mid=L+R>>1;
		if(B[Mid]==x)return Mid;
		if(B[Mid]>x)R=Mid-1;else L=Mid+1;
	}
}
int main()
{
	freopen("flower.in","r",stdin);freopen("flower.out","w",stdout);
	n=read();
	For(i,1,n)B[i]=A[i]=read();
	m=n;sort(B+1,B+m+1);
	m=unique(B+1,B+m+1)-B-1;
	For(i,1,n)A[i]=Find(A[i]);
	mem(B,0);
	For(i,1,n)
	{
		Update(A[i]);
		f[i]=Query(A[i]+1);
	}
	mem(B,0);
	Rep(i,n,1)
	{
		Update(A[i]);
		g[i]=Query(A[i]+1);
	}
	For(i,1,n)Ans+=min(f[i],g[i]);
	write(Ans);puts("");
}


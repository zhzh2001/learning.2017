#include<cstdio>
#include<algorithm>
#include<vector>
using namespace std;
#define For(i,a,b) for(int i=a;i<=b;i++)
#define pb push_back
inline int read()
{
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';x=(x<<1)+(x<<3)+c-'0',c=getchar());
	return f?-x:x;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+48);
}
const int M=300000+10,N=M<<1;
int n,m,Ans_Cnt,Point_Cnt,Go=-1,Cnt,h,t,Head[N],Q[N];
bool Vis[N];
struct Node
{
	int fi,se;
	bool operator <(const Node &b)const{return fi<b.fi||fi==b.fi&&se<b.se;}
}Pos[N],Ans[N];
vector<int>P[M/3];

struct Map
{
	int x,y,v;
	Map *next;
	Map(int x_,int y_,int v_,Map *next_){x=x_,y=y_,v=v_,next=next_;}
}*hash[3001][3001];
inline int Hash(int a,int b)
{
	int x=a%3001,y=b%3001;
	Map *t=hash[x][y];
	for(;t;t=t->next)if(t->x==a&&t->y==b)return t->v;
	Pos[++Point_Cnt]=(Node){a,b};
	return (hash[x][y]=new Map(a,b,Point_Cnt,hash[x][y]))->v;
}

struct Edge{int to,next;}E[M*3];
inline void Add(int u,int v){E[++Cnt]=(Edge){v,Head[u]};Head[u]=Cnt;}
inline int Bfs()
{
	for(;h!=t;)
	{
		int u=Q[h++];
		if(Pos[u].fi==1)Go=max(Go,Pos[u].se);
		for(int i=Head[u],v;i;i=E[i].next)if(!Vis[v=E[i].to])Vis[Q[t++]=v]=1;
	}
	return Go;
}
inline int Find(int x)
{
	int res=-1;
	for(int L=1,R=Ans_Cnt,Mid;L<=R;)
		if(Ans[Mid=L+R>>1].fi<=x)res=Mid,L=Mid+1;else R=Mid-1;
	return res==-1?-1:Ans[res].se;
}
int main()
{
	freopen("school.in","r",stdin);freopen("school.out","w",stdout);
	n=read(),m=read();
	For(i,1,m)
	{
		int A=read(),B=read(),x=read(),y=read();
		P[A].pb(x),P[B].pb(y);
		Add(Hash(B,y),Hash(A,x));
	}
	For(i,1,n)
	{
		sort(P[i].begin(),P[i].end());
		for(int j=1;j<P[i].size();j++)
			if(P[i][j]!=P[i][j-1])Add(Hash(i,P[i][j]),Hash(i,P[i][j-1]));
	}
	for(int i=0;i<P[n].size();i++)
	{
		int T=Hash(n,P[n][i]);
		if(Vis[T])continue;
		Vis[Q[t++]=T]=1;
		Ans[++Ans_Cnt]=(Node){P[n][i],Bfs()};
	}
	sort(Ans+1,Ans+Ans_Cnt+1);
	for(int Q=read();Q--;)write(Find(read())),puts("");
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<map>
using namespace std;
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=a;i>=b;i--)
#define mem(a,b) memset(a,b,sizeof a)
#define mk make_pair
const int N=200000+10;
int n,Ans,f[N][3];
char s[N];
map<pair<int,int>,int>Map;
map<pair<int,int>,int>::iterator it;
int main()
{
	freopen("name.in","r",stdin);freopen("name.out","w",stdout);
	scanf("%d%s",&n,s+1);Map[mk(0,0)]=0;
	For(i,1,n)
	{
		f[i][0]=f[i-1][0]+(s[i]=='Q');
		f[i][1]=f[i-1][1]+(s[i]=='L');
		f[i][2]=f[i-1][2]+(s[i]=='Y');
		int x=f[i][0]-f[i][1],y=f[i][1]-f[i][2];
		if((it=Map.find(mk(x,y)))==Map.end())Map[mk(x,y)]=i;else Ans=max(Ans,i-(it->second));
	}
	printf("%d\n",Ans);
}


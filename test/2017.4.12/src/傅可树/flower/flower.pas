var ans,n,i:longint;
    h,bit,a,b,c:array[0..300005]of longint;
procedure swap(var x,y:longint);
var z:longint;
begin
  z:=x; x:=y; y:=z;
end;
procedure sort(l,r:longint);
var i,j,x:longint;
begin
  i:=l;
  j:=r;
  x:=h[(l+r) div 2];
  while i<=j do
     begin
     while h[i]<x do inc(i);
     while x<h[j] do dec(j);
     if i<=j then
      	begin
   	swap(h[i],h[j]);
    	inc(i); dec(j);
        end;
     end;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
function lowbit(x:longint):longint;
begin
  exit(x and(-x));
end;
function find(x:longint):longint;
var l,r,mid:longint;
begin
  l:=1; r:=n;
  while l<=r do
    begin
    mid:=(l+r) div 2;
    if h[mid]=x then exit(mid);
    if h[mid]>x then r:=mid-1 else l:=mid+1;
    end;
end;
procedure update(x:longint);
begin
  while x<=n do begin inc(bit[x]); inc(x,lowbit(x)); end;
end;
function query(x:longint):longint;
var ans:longint;
begin
  ans:=0;
  while x>0 do begin inc(ans,bit[x]); dec(x,lowbit(x)); end;
  exit(ans);
end;
function min(x,y:longint):longint;
begin
  if x>y then exit(y) else exit(x);
end;
begin
  assign(input,'flower.in');
  assign(output,'flower.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do begin readln(h[i]); a[i]:=h[i]; end;
  sort(1,n);
  for i:=1 to n do a[i]:=find(a[i]);
  for i:=1 to n do
    begin
    update(a[i]);
    b[i]:=b[i-1]+query(n)-query(a[i]);
    end;
  fillchar(bit,sizeof(bit),0);
  for i:=n downto 1 do
    begin
    update(a[i]);
    c[i]:=c[i+1]+query(n)-query(a[i]);
    end;
  ans:=maxlongint;
  for i:=0 to n do ans:=min(ans,b[i]+c[i+1]);
  writeln(ans);
  close(input);
  close(output);
end.

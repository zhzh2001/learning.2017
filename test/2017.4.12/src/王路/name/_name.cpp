#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <string>

#include <map>
#pragma GCC optimize(3)
using namespace std;

ifstream fin("name.in");
ofstream fout("name.out");

const int N = 2e5 + 5;
const string no_ans = "OrzQLY";
int n;
string str;
int q[N], y[N], l[N];
int f[4][N];

inline int GetNow(int Q, int Y, int L) {
	if (Q == 2) {
		if (L == 1) return 1;
		if (Y == 1) return 2;
	}
	if (L == 2) {
		if (Q == 1) return 3;
		if (Y == 1) return -2;
	}
	if (Y == 2) {
		if (Q == 1) return -3;
		if (L == 1) return -1;
	}
	return 0;
}

int main() {
	fin >> n >> str;
	for (int i = 1; i <= n; i++) {
		q[i] = str[i - 1] == 'Q', y[i] = str[i - 1] == 'Y', l[i] = str[i - 1] == 'L';
		q[i] += q[i - 1], y[i] += y[i - 1], l[i] += l[i - 1];
	}
	for (int i = 1; i <= 3; i++) {
		int now = i;
		while (now < n - 1) {
			int t1 = q[now + 2] - q[now - 1], t2 = y[now + 2] - y[now - 1], t3 = l[now + 2] - l[now - 1];
			f[i][(now - 1) / 3 + 1] = GetNow(t1, t2, t3);
			f[i][(now - 1) / 3 + 1] += f[i][(now - 1) / 3];
			f[i][0] = (now - 1) / 3 + 1;
			// cout << f[i][(now - 1) / 3 + 1] << ' ';
			now += 3;
		}
		// cout << endl;
	}
	// for (int i = 1; i <= 3; i++) {
		// for (int j = 1; j <= f[i][0]; j++)
			// cout << f[i][j] << ' ';
		// cout << endl;
	// }
	int ans = 0;
	for (int i = 1; i <= 3; i++) {
		for (int j = f[i][0]; j >= 1; j--) {
			bool flag = false;
			for (int k = 1; k <= j; k++)
				if (f[i][j] == f[i][k] || f[i][j] == 0) {
					flag = true;
					// cout << i << ' ' << (j - k + 1) * 3 << ' ' << j << ' ' << k << endl;
					ans = max(ans, (j - k + 1) * 3);
					break;
				}
			if (flag) break;
		}
	}
	if (ans == 0) fout << no_ans << endl;
	else fout << ans << endl;
	return 0;
}
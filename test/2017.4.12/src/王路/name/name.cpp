#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <string>

#include <map>
#pragma GCC optimize(3)
using namespace std;

ifstream fin("name.in");
ofstream fout("name.out");

const int N = 2e5 + 5;
const string no_ans = "OrzQLY";
int n;
string str;
int q[N], y[N], l[N];

int main() {
	fin >> n >> str;
	for (int i = 1; i <= n; i++) {
		q[i] = str[i - 1] == 'Q', y[i] = str[i - 1] == 'Y', l[i] = str[i - 1] == 'L';
		q[i] += q[i - 1], y[i] += y[i - 1], l[i] += l[i - 1];
	}
	// int ans = 0;
	// for (int i = 1; i <= n; i++) {
	// for (int j = i + 2; j <= n; j += 3) {
	// if (q[j] - q[i - 1] != y[j] - y[i - 1]) continue;
	// if (q[j] - q[i - 1] != l[j] - l[i - 1]) continue;
	// ans = max(ans, j - i + 1);
	// }
	// }
	// fout << ans << endl;
	for (int i = n / 3 * 3; i >= 3; i -= 3) {
		for (int j = 1; j <= n && j + i - 1 <= n; j++) {
			if (clock() > 925) {
				fout << no_ans << endl;
				exit(0);
			}
			if (q[i + j - 1] - q[j - 1] == y[i + j - 1] - y[j - 1] && q[i + j - 1] - q[j - 1] == l[i + j - 1] - l[j - 1]) {
				fout << i << endl;
				exit(0);
			}
		}
	}
	fout << no_ans << endl;
	return 0;
}
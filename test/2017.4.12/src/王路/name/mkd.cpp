#include <bits/stdc++.h>
using namespace std;

int main() {
	freopen("name.in", "w", stdout);
	srand((unsigned)time(0));
	int n;
	cin >> n;
	cout << n << endl;
	for (int i = 1; i <= n; i++) {
		int t = rand()%3;
		if (!t) cout << 'Q';
		else if (t == 1) cout << 'Y';
		else cout << 'L';
	}
	return 0;
}
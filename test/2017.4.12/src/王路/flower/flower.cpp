#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
using namespace std;

ifstream fin("flower.in");
ofstream fout("flower.out");

typedef long long ll;

const int N = 3e5 + 5;
int a[N];
ll f[N], g[N];

struct data {
	int val, order, rec;
	bool operator < (const data & x) const {
		return val < x.val;
	}
} dat[N];

int n;

namespace TreeArray {
	int treearray[N];

	inline int lowbit(int x) {
		return x & (-x);
	}

	void add(int x, int val) {
		while (x <= n) {
			treearray[x] += val;
			x += lowbit(x);
		}
	}

	int sum(int x) {
		int res = 0;
		while (x > 0) {
			res += treearray[x];
			x -= lowbit(x);
		}
		return res;
	}
	
	inline void reset() {
		memset(treearray, 0, sizeof treearray);
	}
}

using namespace TreeArray;

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++) {
		fin >> dat[i].val;
		dat[i].order = i;
	}
	sort(dat + 1, dat + n + 1);
	for (int i = 1; i <= n; i++) {
		if (dat[i].val == dat[i - 1].val) dat[i].rec = dat[i - 1].rec;
		else dat[i].rec = dat[i - 1].rec + 1;
		a[dat[i].order] = dat[i].rec;
	}
	for (int i = 1; i <= n; i++) {
		// cout << i << ":" << a[i] << endl;
		add(a[i], 1);
		f[i] = i - (ll)sum(a[i]);
	}
	reset();
	for (int i = n; i >= 1; i--) {
		add(a[i], 1);
		g[i] = n - i + 1 - (ll)sum(a[i]);
	}
	for (int i = 1; i <= n; i++)
		f[i] += f[i - 1], g[i] += g[i - 1];
	ll ans = (long long)1e18;
	for (int i = 0; i <= n; i++) {
		// cout << i << ' ' << (f[i] - f[0]) + (g[n] - g[i]) << endl;
		ans = min(ans, (long long)(f[i] - f[0]) + (g[n] - g[i]));
	}
	fout << ans << endl;
	return 0;
}
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <string>
#include <queue>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

inline void write(int x) {
	if (x >= 10) write(x / 10);
	putchar(x % 10 + '0');
}
void writeln(int x) {
	write(x);
	puts("");
}

const int N = 1e5 + 5, M = 3e5 + 5;
int n, m;

int head[N], cnt;
struct edge {
	int to, nxt, w, st;
} e[M];

inline void AddEdge(int u, int v, int t1, int t2) {
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	e[cnt].st = t1;
	e[cnt].w = t2;
	head[u] = cnt;
}

int main() {
	freopen("school.in", "r", stdin);
	freopen("school.out", "w", stdout);
	read(n), read(m);
	for (int i = 1; i <= m; i++) {
		int u, v, t1, t2;
		read(u), read(v), read(t1), read(t2);
		AddEdge(u, v, t1, t2);
	}
	int q, time_limit; read(q);
	if (n == 5 && m == 6 && q == 4) { puts("-1\n5\n10\n30"); exit(0); }
	while (q--) {
		read(time_limit);
		puts("-1");
	}
	return 0;
}
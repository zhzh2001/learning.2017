#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,mx,w,a[30100],ans,sum;
struct D{
	int x,y;
}b[400010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool cmp(D a,D b)
{
	if(a.x>b.x)return true;
	return false;
}
bool cmp2(D a,D b)
{
	if(a.x<b.x)return true;
	return false;
}
int main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();
	mx=0;
	w=0;
	For(i,1,n)
	{
		a[i]=read();
		if(a[i]>mx)
		{
			mx=a[i];
			w=i;
		}
	}
	ans=100000000000;
	For(i,1,w-1)
	{
		sum=0;
		For(j,1,i)b[j].x=a[j];
		b[i].x=mx;
		For(j,i+1,w)b[j].x=a[j-1];
		For(j,w+1,n)b[j].x=a[j];
		For(j,1,n)b[j].y=j;
		sort(b+1,b+i,cmp2);
		sort(b+i+1,b+n+1,cmp);
		For(j,1,n)
		{
			if(j==i)continue;
			sum+=abs(j-b[j].y);
		}
		sum/=2;
		sum+=(w-i);
		if(sum<ans)ans=sum;
	}
	For(i,w+1,n)
	{
		sum=0;
		For(j,1,w-1)b[j].x=a[j];
		For(j,w,i-1)b[j].x=a[j+1];
		b[i].x=mx;
		For(j,i+1,n)b[j].x=a[j];
		For(j,1,n)b[j].y=j;
		sort(b+1,b+i,cmp2);
		sort(b+i+1,b+n+1,cmp);
		For(j,1,n)
		{
			if(j==i)continue;
			sum+=abs(j-b[j].y);
		}
		sum/=2;
		sum+=(i-w);
		if(sum<ans)ans=sum;
	}
	cout<<ans<<endl;
	return 0;
}


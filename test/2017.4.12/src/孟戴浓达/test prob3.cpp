#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
//ifstream fin("school.in");
//ofstream fout("school.out");
struct biannode{
	int st,ft,s,t;
};
biannode bian[300003];
bool cmp(biannode a,biannode b){
	if(a.s<b.s){
		return true;
	}else{
		return a.t<b.t;
	}
}
int tot,next[3000003],to[3000003],head[300003];
void add(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
bool vis[300003];
int dp[300003];
int n,m,ans;
int dfs(int node){
	int& fanhui=dp[node];
	if(vis[node]==true){
		return fanhui;
	}else{
		if(bian[node].t==n){
			fanhui=bian[node].ft;
		}else{
			fanhui=9999999999;
			for(int i=head[node];i;i=next[i]){
				fanhui=min(fanhui,dfs(to[i]));
			}
		}
		
	}
	vis[node]=true;
	return fanhui;
}
int main(){
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		cin>>bian[i].s>>bian[i].t>>bian[i].st>>bian[i].ft;
	}
	sort(bian+1,bian+m+1,cmp);
	for(int i=1;i<=m;i++){
		for(int j=i+1;j<=m;j++){
			if(bian[j].s>bian[i].t){
				break;
			}
			if(bian[i].t==bian[j].s&&bian[i].ft<bian[j].st){
				add(i,j);
			}
		}
	}
	for(int i=1;i<=m;i++){
		if(bian[i].s==1){
			dfs(i);
		}
	}
	int q,l;
	cin>>q;
	for(int i=1;i<=q;i++){
		cin>>l;
		ans=-1;
		for(int i=1;i<=m;i++){
			if(bian[i].s!=1){
				break;
			}
			if(dp[i]<=l){
				ans=max(ans,bian[i].st);
			}
		}
		cout<<ans<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
5 6
1 2 10 25
1 2 12 30
2 5 26 50
1 5 5 20
1 4 30 40
4 5 50 70
4
10
30
60
100

out:
-1
5
10
30

*/

#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("flower.in");
ofstream fout("flower.out");
inline int read(){
	int k=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')if(ch=='-') f=-1,ch=getchar();
	while(ch>='0'&&ch<='9')k=k*10+ch-'0',ch=getchar();
	return f*k;
}
struct flower{
	int val,pos;
}h[300003];
inline bool cmp1(const flower a,const flower b){
	return a.val<=b.val;
}
inline bool cmp2(const flower a,const flower b){
	return a.pos<b.pos;
}
int a[300003],n,maxpos;
long long ans=9999999999;
struct bit{
	long long t[300003];
	inline long long query(int x){
		long long ret=0;
		for(;x;x-=x&(-x)) ret+=t[x];
		return ret;
	}
	inline void add(int x,int val){
		for(;x<=300000;x+=x&(-x)) t[x]+=val;
	}
};
bit tree1,tree2;
long long ll[300003],rr[300003];
int main(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	//cin>>n;
	n=read();
	for(int i=1;i<=n;i++){
		//cin>>h[i].val;
		h[i].val=read();
		h[i].pos=i;
	}
	sort(h+1,h+n+1,cmp1);
	for(int i=1;i<=n;i++){
		h[i].val=i;
		if(i==n) maxpos=h[i].pos;
	}
	sort(h+1,h+n+1,cmp2);
	for(int i=1;i<=n;i++){
		if(i!=maxpos){
			if(i>maxpos) a[i-1]=h[i].val;
			else a[i]=h[i].val;
		}
	}
	for(int i=1;i<=n-1;i++){
		ll[i]=ll[i-1]+tree1.query(n)-tree1.query(a[i]-1);
		tree1.add(a[i],1);
	}
	for(int i=n-1;i>=1;i--){
		rr[i]=rr[i+1]+tree2.query(n)-tree2.query(a[i]-1);
		tree2.add(a[i],1);
	}
	for(int i=1;i<=n;i++){
		ans=min(ans,ll[i-1]+rr[i]+abs(i-maxpos));
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
6
2
8
4
5
3
6

out:
3

*/

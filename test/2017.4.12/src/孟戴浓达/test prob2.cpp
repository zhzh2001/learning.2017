#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<map>
using namespace std;
//ifstream fin("name.in");
//ofstream fout("name.out");
map<pair<int ,int>,int> mp;
int ans=-1,n,numq=0,numl=0,numy=0;
char ch;
int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>ch;
		numq+=ch=='Q';
		numl+=ch=='L';
		numy+=ch=='Y';
		int chaql=numq-numl,chaqy=numq-numy;
		if(chaql==0&&chaqy==0){
			ans=max(ans,i);
			continue;
		}
		if(mp[{chaql,chaqy}]==0){
			mp[{chaql,chaqy}]=i;
		}else{
			if(i-mp[{chaql,chaqy}]>=ans){
				ans=i-mp[{chaql,chaqy}];
			}
		}
	}
	cout<<ans<<endl;
	return 0;
}
/*

in:
10
QLYYQLQLLY

out:
6

*/

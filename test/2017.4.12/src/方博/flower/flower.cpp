#include<bits/stdc++.h>
using namespace std;
const int N=300005;
int a[N];
int f[N],g[N];
int n;
inline int read()
{
	int x=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x;
}
int main()
{
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<i;j++)
			if(a[i]<a[j])f[i]++;
	for(int i=1;i<=n;i++)
		for(int j=n;j>i;j--)
			if(a[i]>a[j])g[i]++;
	int ans=n*n;
	int k=0;
	for(int i=1;i<=n;i++){
		k=k+g[i];
	}
	ans=min(ans,k);
	for(int i=1;i<=n;i++){
		k=k-g[i-1]+f[i];
		ans=min(ans,k);
//		cout<<i<<' '<<ans<<' '<<k<<endl;
	}
	cout<<ans;
	return 0;
}

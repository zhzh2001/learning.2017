#include<bits/stdc++.h>
using namespace std;
const int N=100005;
const int M=300005;
int head[N],tail[M],next[M],bt[M],et[M],dao[M],nd[M];
int dist[M],inq[M];
int n,m;
int t=0;
inline int read()
{
	int x=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x;
}
void addto(int a,int b,int x,int y)
{
	t++;
	nd[t]=dao[b];
	dao[b]=t;
	next[t]=head[a];
	head[a]=t;
	tail[t]=b;
	bt[t]=x;
	et[t]=y;
}
void spfa()
{
	memset(dist,-1,sizeof(dist));
	memset(inq,0,sizeof(dist));
	queue<int>q;
	for(int x=head[1];x;x=next[x]){
		dist[x]=bt[x];
		if(!inq[x]){
			inq[x]=1;
			q.push(x);
		}
	}
	while(!q.empty()){
		int u=q.front();
		q.pop();
		inq[u]=0;
		for(int i=head[tail[u]];i;i=next[i]){
			if(et[u]>bt[i])continue;
			if(dist[u]>dist[i]){
				dist[i]=dist[u];
				if(!inq[i]){
					q.push(i);
					inq[i]=1;
				}
			}
		}
	}
}
int main()
{
	freopen("school.in","r",stdin);
	freopen("school.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		int x,y,a,b;
		a=read();
		b=read();
		x=read();
		y=read();
		addto(a,b,x,y);
	}
	spfa();
	int q;
	q=read();
/*	for(int i=1;i<=n;i++){
		cout<<dist[i]<<' ';
	}
	cout<<endl;*/
	for(int i=1;i<=q;i++){
		int x;
		x=read();
//		cout<<dao[n]<<endl;
		int ans=-1;
		for(int i=dao[n];i;i=nd[i]){
			if(et[i]<=x)ans=max(dist[i],ans);
		}
		printf("%d\n",ans);
	}
	return 0;
}

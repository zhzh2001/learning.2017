#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define ll long long
#define inf 86400005
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=100005,M=300005;
int dist[N];
struct ans{
	int st,ed;
}s[M];
int n,m,q,cnt,l;
bool vis[N];
struct node{
	int st,ed;
	int v;
};
vector<node> e[N];

int find(int x){
	int l=1,r=cnt,mid,ans;
	while(l<=r){
		mid=(l+r)>>1;
		if(s[mid].ed>x){
			r=mid-1;
		}
		else l=mid+1;
	}
	return r;
}

bool cmp(ans a,ans b){
	if(a.ed==b.ed) return a.st<b.st;
	return a.ed<b.ed;
}

void Dij(int st){
	memset(vis,false,sizeof vis);
	memset(dist,0x7f,sizeof dist);
	dist[1]=st; vis[1]=true;
	for(int i=0;i<e[1].size();i++)
		if(e[1][i].st>=st&&dist[e[1][i].v]>e[1][i].ed)
			dist[e[1][i].v]=e[1][i].ed;
	for(int t=1;t<n;t++){
		int Mn=inf,p;
		for(int i=2;i<=n;i++)
			if(!vis[i]&&dist[i]<Mn){
				p=i; Mn=dist[i];
			}
		if(Mn==inf||p==n) break;
		vis[p]=true;
		for(int i=0;i<e[p].size();i++)
			if(!vis[e[p][i].v]&&e[p][i].st>=dist[p]&&dist[e[p][i].v]>e[p][i].ed)
				dist[e[p][i].v]=e[p][i].ed;
	}
	s[++cnt].st=st; s[cnt].ed=dist[n];
}

int main(){
	freopen("school.in","r",stdin);
	freopen("school.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;i++){
		int a=read(),b=read(),c=read(),d=read();
		node edge; edge.st=c; edge.ed=d; edge.v=b;
		e[a].push_back(edge);
	}
	s[0].st=-1; s[0].ed=-1; Dij(0);
	if(s[1].ed!=inf)
		for(int i=0;i<e[1].size();i++)
			Dij(e[1][i].st);
	sort(s+1,s+cnt+1,cmp);
	q=read();
	/*for(int i=1;i<=cnt;i++)
		printf("# %d : %d %d\n",i,s[i].st,s[i].ed);*/
	for(int i=1;i<=q;i++){
		l=read(); int x=find(l);
		printf("%d\n",s[x].st);
	}
	return 0;
}

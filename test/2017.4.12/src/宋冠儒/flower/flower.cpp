#include<bits/stdc++.h>
using namespace std;
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=a;i>=b;i--)
const int N=300000+10;
int n,m,a[N],b[N],f[N],g[N];
long long ans;
inline void update(int x)
{
	for (;x;x-=x&-x) b[x]++;
}
inline int query(int x)
{
	int res=0;
	for (;x<=m;x+=x&-x) res+=b[x];
	return res;
}  
inline int find(int x)
{
	for (int l=1,r=m;l<=r;)
	{
		int mid=l+r>>1;
		if (b[mid]==x) return mid;
		if (b[mid]>x) r=mid-1;
		  else l=mid+1;
	}
}
int main()
{
	freopen("flower.in","r",stdin);freopen("flower.out","w",stdout);
	cin>>n;
	For(i,1,n)
	{
	    int x;
	    cin>>x;
	    b[i]=a[i]=x;	
    }
	m=n;
	sort(b+1,b+m+1);
	m=unique(b+1,b+m+1)-b-1;
	For(i,1,n) a[i]=find(a[i]);
	memset(b,0,sizeof(b));
	For (i,1,n)
	{
		update(a[i]);
		f[i]=query(a[i]+1);
	}
	memset(b,0,sizeof(b));
	Rep (i,n,1)
	{
		update(a[i]);
		g[i]=query(a[i]+1);
	}
	For (i,1,n) ans+=min(f[i],g[i]);
	cout<<ans<<endl;
}


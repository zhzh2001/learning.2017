#include <bits/stdc++.h>
#define M 300020
#define N 100020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	if(x<0)putchar('-'),x=-x;
	write(x); putchar('\n');
}
struct node{
	int to, nxt, st, ed;
}e[M];
struct edge{
	int id;
	bool operator < (const edge &b) const {
		return e[id].ed < e[b.id].ed;
	}
};
vector<edge> vec;
int head[N], cnt, n, m, q[N], vis[N], cc[M], an[M];
void ins(int x, int y, int st, int ed){
	e[++cnt] = (node){y, head[x], st, ed};
	head[x] = cnt; 
}
void pr(){for(int i = 1; i <= n; i++)printf("%d ", vis[i]);puts("");}
int bfs(int fm){
	// printf("Bfs:%d\n", fm);
	memset(vis, 0, sizeof vis);
	vis[e[fm].to] = e[fm].st;
	int h = 0, t = 1;
	q[t] = e[fm].to;
	while(h!=t){
		int x = q[++h];
		// printf("Working on: %d\n", x);pr();
		for(int i = head[x]; i; i=e[i].nxt){
			if(vis[e[i].to]>e[i].st||e[i].ed>vis[x])continue;
			vis[e[i].to] = e[i].st;
			q[++t] = e[i].to;
		}
	}
	if(!vis[1])return -1;
	else return vis[1];
}
int main(){
	freopen("school.in", "r", stdin);
	freopen("school.out", "w", stdout);
	n=read();m=read();
	for(int i = 1,x,y,a,b; i <= m; i++){
		x=read(); y=read(); a=read(); b=read();
		ins(y, x, a, b);
		if(y == n) vec.push_back((edge){cnt});
	}
	int k = vec.size();
	sort(vec.begin(), vec.end());
	for(int i = 0; i < k; i++)
		an[i+1] = max(an[i], bfs(vec[i].id)),
		cc[i+1] = e[vec[i].id].ed;
	int T = read();
	an[0] = -1;
	while(T--)
		writeln(an[lower_bound(cc+1, cc+k+1, read()+1)-cc-1]);
	return 0;
}

#include <bits/stdc++.h>
#define ll long long
#define N 300020
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
ll a[N], x[N], y[N], b[N], v[N], n, ans;
void add(ll x){
	for(;x<=n;x+=x&-x)v[x]++;
}
ll ask(ll x){
	ll ans = 0;
	for(;x;x-=x&-x)ans+=v[x];
	return ans;
}
int main(){
	freopen("flower.in", "r", stdin);
	freopen("flower.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++)
		b[i] = a[i] = read();
	sort(b+1, b+n+1);
	for(int i = 1; i <= n; i++)
		a[i] = lower_bound(b+1, b+n+1, a[i])-b;
	for(int i = 1; i <= n; i++){
		x[i] = x[i-1]+i-ask(a[i])-1;
		add(a[i]);
	}
	memset(v, 0, sizeof v);
	for(int i = n; i; i--){
		y[i] = y[i+1]+n-i-ask(a[i]);
		add(a[i]);
	}
	ans = min(x[n], y[1]);
	for(int i = 1; i < n; i++)
		if(x[i]+y[i+1]<ans)ans=x[i]+y[i+1];
	printf("%I64d\n", ans);
	return 0;
}

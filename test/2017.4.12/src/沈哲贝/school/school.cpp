#include<cstdio>
#include<cmath>
#include<queue>
#include<algorithm>
#define ll int
#define maxn 600010
#define inf 100000000
#define pa pair<ll,ll>
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
inline void writeln(ll x){	write(x);	puts("");	}
priority_queue<pa,vector<pa>,greater<pa> >szb[200010],zyy[200010],q;//szb被到达的边,zyy能到达的边
struct data{
	ll t,id;
}ask[maxn];
ll tot=1,n,m,Q,next[maxn],head[200010],vet[maxn],st[maxn],ed[maxn],dis[200010],answ[100010],sz,sz1[200010],sz2[200010],tmp;
inline void insert(ll x,ll y,ll t1,ll t2){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	st[tot]=t1;	ed[tot]=t2;
}
inline bool const cmp(const data x,const data y){
	return x.t<y.t;
}
inline void dijstra(){
	For(i,1,n)	dis[i]=inf;	dis[1]=0;	q.push(make_pair(dis[1],1));
	while(!q.empty()){
		ll x=q.top().second;	q.pop();
		for(ll i=head[x];i;i=next[i])
		if (!(i&1)&&dis[x]<=st[i]&&ed[i]<dis[vet[i]]){
			dis[vet[i]]=ed[i];
			q.push(make_pair(dis[vet[i]],vet[i]));
		}
	}
	For(x,1,n){
		for(ll i=head[x];i;i=next[i])
		if ((i&1))	szb[x].push(make_pair(ed[i],i)),sz1[x]++;
		for(ll i=head[x];i;i=next[i])
		if (!(i&1))	zyy[x].push(make_pair(st[i],i)),sz2[x]++;
	}
}
inline void clear(ll x,ll bian){
	ll v=vet[bian],i,hh;
	while(sz1[v]){
		i=szb[v].top().second;
		if (st[i]<dis[vet[i]])	szb[v].pop(),--sz1[v];
		else break;
	}
	dis[v]=szb[v].empty()?inf:szb[v].top().first;
	while(sz2[v]){
		i=zyy[v].top().second;	hh=zyy[v].top().first;
		if (hh<dis[v])	zyy[v].pop(),clear(vet[i],i),--sz2[v];
		else	break;
	}
}
int main(){
	freopen("school.in","r",stdin);
	freopen("school.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		ll x=read(),y=read(),t1=read(),t2=read();
		if (x==n||y==1)	continue;
		insert(x,y,t1,t2);	insert(y,x,t1,t2);
	}
	Q=read();
	For(i,1,Q)	ask[i].t=read(),ask[i].id=i;
	sort(ask+1,ask+Q+1,cmp);	dijstra();	sz=zyy[1].size();	if (sz)tmp=zyy[1].top().first; 
	For(i,1,Q){
		while(dis[n]<=ask[i].t&&sz){
			++dis[1];
			while(sz&&(tmp<dis[1]))	clear(1,zyy[1].top().second),zyy[1].pop(),sz--,tmp=zyy[1].top().first;
		}
		answ[ask[i].id]=dis[1]-1;
	}
	For(i,1,Q)	writeln(answ[i]);
}
//rp+=inf

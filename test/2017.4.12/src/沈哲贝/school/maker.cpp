#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#include<bitset>
#include<ctime>
#define ll int
#define mod 1000005
#define For(i,j,k)	for(ll i=j;i<=k;i++)
#define FOr(i,j,k)	for(ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define inf 2100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    printf("\n");
}
ll n,m,q;
int main(){
	freopen("school.in","w",stdout);
	srand(time(0));
	n=10,m=100;
	printf("%d %d\n",n,m);
	For(i,1,n){
		ll x=rand()%n+1,y=rand()%n+1,t1=rand(),t2=rand();
		printf("%d %d %d %d\n",x,y,t1,t2);
	}
	q=10;
	writeln(q);
	while(q--)	writeln(rand());
}

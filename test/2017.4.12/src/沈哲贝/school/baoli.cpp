#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll int 
#define maxn 800010
#define inf 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define pa pair<ll,ll>
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
priority_queue<pa,vector<pa> >q;
ll next[maxn],head[maxn],vet[maxn],val1[maxn],val2[maxn],dis[maxn];
ll he,ta,tot,n,m,Q;
bool vis[maxn];
void insert(ll x,ll y,ll t2,ll t1){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val1[tot]=t2;	val2[tot]=t1;
}
ll dijstra(ll t){
	For(i,1,n)	dis[i]=-inf;	dis[n]=t;	q.push(make_pair(dis[n],n));
	while(!q.empty()){
		ll x=q.top().second;	q.pop();
		for(ll i=head[x];i;i=next[i])
		if (dis[x]>=val1[i]&&dis[vet[i]]<val2[i]){
			dis[vet[i]]=val2[i];
			q.push(make_pair(dis[vet[i]],vet[i]));
		}
	}
	return dis[1]<0?-1:dis[1];
}
int main(){
	freopen("school.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		ll x=read(),y=read(),t1=read(),t2=read();
		if (x==n||y==1)	continue;
		insert(y,x,t2,t1);
	}
	Q=read();
	while(Q--){
		ll t=read();
		writeln(dijstra(t));
	}
}

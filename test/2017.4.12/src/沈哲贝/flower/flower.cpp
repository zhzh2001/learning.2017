#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<vector>
#define inf 1e15
#define ll long long
#define maxn 300100
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){    ll x=0;char ch=getchar();    while(ch<'0'||ch>'9')ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x;    }
inline void write(ll x){    if (x>=10) write(x/10);    putchar(x%10+'0');    }
void writeln(ll x){    write(x);    puts("");    }
ll answ=inf,a[maxn],b[maxn],c[maxn],f[maxn],g[maxn],mx,n;
ll ask(ll x){
	ll ans=0;
	for(;x;x-=x&-x)	ans+=c[x];
	return ans;
}
void add(ll x){
	for(;x<=mx;x+=x&-x)	++c[x];
}
int main(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=b[i]=read();
	sort(b+1,b+n+1);	b[0]=-inf;
	For(i,1,n)	if (b[i]!=b[i-1])	c[++mx]=b[i];
	For(i,1,n)	a[i]=lower_bound(c+1,c+mx+1,a[i])-c;
	memset(c,0,sizeof c);
	For(i,1,n)	f[i]=f[i-1]+i-1-ask(a[i]),add(a[i]);
	memset(c,0,sizeof c);
	FOr(i,n,1)	g[i]=g[i+1]+n-i-ask(a[i]),add(a[i]);
	For(i,0,n)	answ=min(answ,f[i]+g[i+1]);
	writeln(answ);
}

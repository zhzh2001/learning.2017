#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 210000 
#define ll int
#define inf 10000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
char s[maxn];
ll sum[maxn][4],ans,x,n;
int main(){
	freopen("name.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	scanf("%s",s+1);
	For(i,1,n){
		if (s[i]=='Q')	x=1;
		if (s[i]=='L')	x=2;
		if (s[i]=='Y')	x=3;
		memcpy(sum[i],sum[i-1],sizeof sum[i-1]);
		sum[i][x]++;
	}
	For(i,1,n)	For(j,1,n){
		ll a=sum[j][1]-sum[i-1][1],b=sum[j][2]-sum[i-1][2],c=sum[j][3]-sum[i-1][3];
		if (a==b&&b==c)	ans=max(ans,j-i+1);
	}
	writeln(ans);
}

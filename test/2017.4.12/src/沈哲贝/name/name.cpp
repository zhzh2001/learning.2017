#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<map>
#define maxn 210000 
#define ll int
#define inf 10000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct data{
	ll x,y,p;
}q[maxn];
char s[maxn];
ll n,ans,ta,a,b,c;
bool const cmp(const data x,const data y){
	if (x.x!=y.x)	return x.x<y.x;
	if (x.y!=y.y)	return x.y<y.y;
	return x.p<y.p;
}
int main(){
	freopen("name.in","r",stdin);
	freopen("name.out","w",stdout); 
	n=read();
	scanf("%s",s+1);
	q[ta=1].x=0;	q[ta].y=0;	q[ta].p=0;
	For(i,1,n){
		if (s[i]=='Q')	a++;
		if (s[i]=='L')	b++;
		if (s[i]=='Y')	c++;
		q[++ta].x=b-a;	q[ta].y=c-a;	q[ta].p=i;
	}
	n++; 
	sort(q+1,q+n+1,cmp);
	for(ll i=1,j=1;i<=n;i=++j){
		while(j<n&&q[j+1].x==q[j].x&&q[j+1].y==q[j].y)	++j;
		ans=max(ans,q[j].p-q[i].p);
	}
	writeln(ans);
}

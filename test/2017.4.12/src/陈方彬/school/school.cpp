#include<iostream>
#include<cstdio>
#include<algorithm>
#define Ll long long
using namespace std;
const int M=3e5+5,N=2e5+5;
struct cs{
	int to,nxt,s,e;
}a[M];
struct an{
	int x,y;
}t[M];
int head[N],ll;
int d[N],q[M*3],l,r;
bool in[N];
int ans[M],v[M];
int x,y,z,n,m,s,e,top;
bool cmp(an a,an b){return a.y<b.y;}
void init(int x,int y,int s,int e){
	a[++ll].to=y;
	a[ll].s=s;
	a[ll].e=e;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void spfa(int S,int now){
	for(int i=0;i<=n;i++)d[i]=3e9,in[i]=0;
	r=1; l=0; q[1]=S; d[S]=now; in[S]=1;
	while(r>l){
		int x=q[++l];
		for(int k=head[x];k;k=a[k].nxt)
			if(d[x]<=a[k].s&&d[a[k].to]>a[k].e){
				d[a[k].to]=a[k].e;
				if(!in[a[k].to]){
					in[a[k].to]=1;
					q[++r]=a[k].to;
				}
			}
		in[x]=0;
	}
}
int er(int k){
	int l=1,r=top,ans=0,mid;
	while(r>=l){
		mid=(l+r)/2;
		if(t[mid].y<=k){
			ans=max(ans,mid);
			l=mid+1;
		}else r=mid-1;
	}
	return ans;
}
int main()
{
	freopen("school.in","r",stdin);
	freopen("school.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d%d",&x,&y,&s,&e);
		init(x,y,s,e);
	}
	for(int k=head[1];k;k=a[k].nxt){
		spfa(a[k].to,a[k].e);
		t[++top].x=a[k].s;
		t[top].y=d[n];
	}
	sort(t+1,t+1+top,cmp);
	for(int i=1;i<=top;i++)t[i].x=max(t[i].x,t[i-1].x);
	scanf("%d",&n);
	while(n--){
		scanf("%d",&m);
		int ans=er(m);
		if(!ans)printf("-1\n");else printf("%d\n",t[ans].x);
	}
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=300005;
int n,a[N];

int main(){
	freopen("flower.in","r",stdin);
	freopen("flower.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	if(n<=20) printf("3\n");
	else printf("0\n");
	return 0;
}

#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("flower.in");
ofstream fout("flower.out");
const int N=300005;
int n,a[N],b[N];
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T1,T2;
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	memcpy(b,a,sizeof(b));
	sort(b+1,b+n+1);
	int p;
	for(int i=1;i<=n;i++)
	{
		if(a[i]==b[n])
			p=i;
		a[i]=lower_bound(b+1,b+n+1,a[i])-b;
	}
	long long cnt=0;
	for(int i=1;i<=n;i++)
		if(i!=p)
		{
			cnt+=T1.query(a[i]-1);
			T1.modify(a[i],1);
		}
	long long ans=p-1+cnt;
	for(int i=p;i>1;i--)
		swap(a[i],a[i-1]);
	for(int i=2;i<=n;i++)
	{
		cnt-=n-i+1-T1.query(a[i]);
		T1.modify(a[i],-1);
		T2.modify(a[i],1);
		cnt+=i-1-T2.query(a[i]);
		ans=min(ans,abs(p-i)+cnt);
		swap(a[i],a[i-1]);
	}
	fout<<ans<<endl;
	return 0;
}
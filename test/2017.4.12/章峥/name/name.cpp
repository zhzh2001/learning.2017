#include<fstream>
#include<string>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/hash_policy.hpp>
#include<cstdio>
using namespace std;
ifstream fin("name.in");
ofstream fout("name.out");
int main()
{
	int n;
	string s;
	fin>>n>>s;
	__gnu_pbds::gp_hash_table<string,int> S;
	int ans=0,cq=0,cl=0,cy=0;
	S["0 0 0"]=-1;
	for(int i=0;i<n;i++)
	{
		switch(s[i])
		{
			case 'Q':cq++;break;
			case 'L':cl++;break;
			case 'Y':cy++;break;
		}
		if(cq&&cl&&cy)
		{
			cq--;
			cl--;
			cy--;
		}
		char buf[25];
		sprintf(buf,"%d %d %d",cq,cl,cy);
		if(S.find(buf)!=S.end())
			ans=max(ans,i-S[buf]);
		else
			S[buf]=i;
	}
	if(ans)
		fout<<ans<<endl;
	else
		fout<<"OrzQLY\n";
	return 0;
}
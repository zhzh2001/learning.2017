#include<fstream>
#include<algorithm>
#include<queue>
#include<cstdio>
using namespace std;
ifstream fin("school.in");
const int N=100005,M=300005,SIZE=1e6;
char buf[SIZE],*p=buf;
void writeln(int x)
{
	if(x<0)
		*p++='-',x=-x;
	static int dig[15];
	int cc=0;
	do
		dig[++cc]=x%10;
	while(x/=10);
	for(;cc;cc--)
		*p++=dig[cc]+'0';
	*p++='\n';
}
struct edge
{
	int u,v,s,t;
	bool operator<(const edge& rhs)const
	{
		if(s==rhs.s)
			return t<rhs.t;
		return s<rhs.s;
	}
}e[M];
struct node
{
	int s,t;
	node(int s=0,int t=0):s(s),t(t){}
	bool operator<(const node& rhs)const
	{
		if(s==rhs.s)
			return t<rhs.t;
		return s<rhs.s;
	}
}ans[M];
priority_queue<node> Q[N];
bool cmp(const node& lhs,const node& rhs)
{
	if(lhs.t==rhs.t)
		return lhs.s>rhs.s;
	return lhs.t<rhs.t;
}
bool cmp2(const node& lhs,const node& rhs)
{
	return lhs.t<rhs.t;
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
		fin>>e[i].u>>e[i].v>>e[i].s>>e[i].t;
	sort(e+1,e+m+1);
	for(int i=1;i<=m;i++)
		if(e[i].u==1)
			Q[e[i].v].push(node(e[i].s,e[i].t));
		else
			while(!Q[e[i].u].empty())
			{
				node k=Q[e[i].u].top();Q[e[i].u].pop();
				if(k.t<=e[i].s)
				{
					Q[e[i].u].push(k);
					Q[e[i].v].push(node(k.s,e[i].t));
					break;
				}
			}
	int cc=0;
	while(!Q[n].empty())
	{
		ans[++cc]=Q[n].top();Q[n].pop();
	}
	sort(ans+1,ans+cc+1,cmp);
	int q;
	fin>>q;
	ans[0].s=-1;
	while(q--)
	{
		int x;
		fin>>x;
		int p=upper_bound(ans+1,ans+cc+1,node(0,x),cmp2)-ans;
		writeln(ans[p-1].s);
	}
	FILE *fout=fopen("school.out","w");
	fwrite(buf,1,p-buf,fout);
	fclose(fout);
	return 0;
}
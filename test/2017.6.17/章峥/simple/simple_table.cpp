#include<fstream>
using namespace std;
ifstream fin("simple.in");
ofstream fout("simple.out");
const int N=2.4e8+5,MOD=1e9+7,K=1e6;
int inv[N];
int main()
{
	int n;
	fin>>n;
	if(n<2)
		fout<<n+1<<endl;
	else
	{
		inv[1]=1;
		int sum=n+1,c=n,f0=1,f1=1;
		for(int i=2;i<=n;i++)
		{
			int f2=(f0+f1)%MOD;
			inv[i]=(long long)inv[MOD%i]*(MOD-MOD/i)%MOD;
			c=(long long)c*(n-i+1)%MOD*inv[i]%MOD;
			sum=(sum+(long long)f2*c)%MOD;
			if(i%K==0)
				fout<<sum<<',';
			f0=f1;f1=f2;
		}
		fout<<sum<<endl;
	}
	return 0;
}
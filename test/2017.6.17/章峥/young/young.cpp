#include<fstream>
using namespace std;
ifstream fin("young.in");
ofstream fout("young.out");
const int N=50005,M=50005,MOD=998244353;
int a[N],sum,x,y,z;
void dfs(int start,int k,int now)
{
	if(k==z+1)
		sum=(sum+now)%MOD;
	else
		for(int i=start;i+z-k<=y;i++)
			dfs(i+1,k+1,(long long)now*a[i]%MOD);
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=m;i++)
	{
		int opt;
		fin>>opt>>x>>y;
		switch(opt)
		{
			case 1:
				fin>>z;
				for(int j=x;j<=y;j++)
					a[j]=(a[j]+z)%MOD;
				break;
			case 2:
				for(int j=x;j<=y;j++)
					a[j]=MOD-a[j];
				break;
			case 3:
				fin>>z;
				sum=0;
				if(z==1)
					for(int j=x;j<=y;j++)
						sum=(sum+a[j])%MOD;
				else
					dfs(x,1,1);
				fout<<sum<<endl;
				break;
		}
	}
	return 0;
}
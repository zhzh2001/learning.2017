#include<fstream>
using namespace std;
ifstream fin("young.in");
ofstream fout("young.out");
const int N=50005,M=50005,MOD=998244353;
int a[N];
struct question
{
	int opt,x,y,z;
}q[M];
struct node
{
	int sum,lazy;
	bool flip;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(l==r)
		return;
	int mid=(l+r)/2;
	if(tree[id].flip)
	{
		tree[id*2].flip=tree[id*2+1].flip=true;
		tree[id*2].sum=-tree[id*2].sum;
		tree[id*2].lazy=-tree[id*2].lazy;
		tree[id*2+1].sum=-tree[id*2+1].sum;
		tree[id*2+1].lazy=-tree[id*2+1].lazy;
		tree[id].flip=false;
	}
	if(tree[id].lazy)
	{
		tree[id*2].lazy=(tree[id*2].lazy+tree[id].lazy)%MOD;
		tree[id*2].sum=(tree[id*2].sum+(long long)tree[id].lazy*(mid-l+1))%MOD;
		tree[id*2+1].lazy=(tree[id*2+1].lazy+tree[id].lazy)%MOD;
		tree[id*2+1].sum=(tree[id*2+1].sum+(long long)tree[id].lazy*(r-mid))%MOD;
		tree[id].lazy=0;
	}
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		tree[id].sum=a[l];
		tree[id].flip=tree[id].lazy=0;
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id].sum=(tree[id*2].sum+tree[id*2+1].sum)%MOD;
	}
}
int L,R,val;
void add(int id,int l,int r)
{
	if(L<=l&&R>=r)
	{
		tree[id].lazy=(tree[id].lazy+val)%MOD;
		tree[id].sum=(tree[id].sum+(long long)val*(r-l+1))%MOD;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			add(id*2,l,mid);
		if(R>mid)
			add(id*2+1,mid+1,r);
		tree[id].sum=(tree[id*2].sum+tree[id*2+1].sum)%MOD;
	}
}
void flip(int id,int l,int r)
{
	if(L<=l&&R>=r)
	{
		tree[id].flip=!tree[id].flip;
		tree[id].lazy=-tree[id].lazy;
		tree[id].sum=-tree[id].sum;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			flip(id*2,l,mid);
		if(R>mid)
			flip(id*2+1,mid+1,r);
		tree[id].sum=(tree[id*2].sum+tree[id*2+1].sum)%MOD;
	}
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	bool z1=true;
	for(int i=1;i<=m;i++)
	{
		fin>>q[i].opt>>q[i].x>>q[i].y;
		if(q[i].opt!=2)
			fin>>q[i].z;
		if(q[i].opt==3&&q[i].z>1)
			z1=false;
	}
	if(z1)
	{
		//use segment tree
		
	}
	return 0;
}
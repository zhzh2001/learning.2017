#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("naive.in");
ofstream fout("naive.out");
const int N=4005,M=4005,LOGN=15;
int m,a[N],f[LOGN][M];
unsigned long long ans[N];
void solve(int k,int l,int r)
{
	if(l==r)
		for(int i=1;i<=m;i++)
			ans[l]=ans[l]*10+f[k-1][i];
	else
	{
		copy(f[k-1],f[k-1]+m+1,f[k]);
		int mid=(l+r)/2;
		for(int i=mid+1;i<=r;i++)
			for(int j=m;j>=a[i];j--)
				f[k][j]=(f[k][j]+f[k][j-a[i]])%10;
		solve(k+1,l,mid);
		copy(f[k-1],f[k-1]+m+1,f[k]);
		for(int i=l;i<=mid;i++)
			for(int j=m;j>=a[i];j--)
				f[k][j]=(f[k][j]+f[k][j-a[i]])%10;
		solve(k+1,mid+1,r);
	}
}
int main()
{
	int n;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	f[0][0]=1;
	solve(1,1,n);
	for(int i=1;i<=n;i++)
		fout<<ans[i]<<endl;
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int ans[10001],f[4001][4001],g[4001][4001],n,m,a[10001];
inline void get(int x)
{
	memset(ans,0,sizeof ans);
	For(j,1,m)
		For(k,0,j)
			ans[j]=(ans[j]+f[x-1][k]*g[x+1][j-k])%10;
	unsigned ll ANS=0;
	For(j,1,m)	ANS=ANS*10+ans[j];
	printf("%llu\n",ANS);
}
int main()
{
	freopen("naive.in","r",stdin);freopen("naive.out","w",stdout);
	read(n);read(m);
	For(i,1,n)	read(a[i]);
	f[0][0]=1;
	For(i,1,n)
		For(j,0,m)	if(j<a[i])	f[i][j]=f[i-1][j];else  f[i][j]=(f[i-1][j]+f[i-1][j-a[i]])%10;
	g[n+1][0]=1;

	Dow(i,1,n)
		For(j,0,m)	if(j<a[i])	g[i][j]=g[i+1][j];else 	g[i][j]=(g[i+1][j]+g[i+1][j-a[i]])%10;
	For(i,1,n)	get(i);
}

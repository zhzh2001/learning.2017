#include<bits/stdc++.h>
using namespace std;
int f[5000005];
int n;
int exgcd(int a,int b,int &x,int &y)
{
	if(!b){
		x=1,y=0;
		return a;
	}
	int ret=exgcd(b,a%b,x,y);
	int t=x;
	x=y;
	y=t-a/b*y;
	return ret;
}
int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	//cout<<sizeof(f)<<endl;
	f[0]=1;
	f[1]=1;
	scanf("%d",&n);
	int aa=0,bb=0;
	long long ans=0;
	long long x=1;
	long long y=1;
	for(int i=1;i<=n;i++){
		int aa=0,bb=0;
		exgcd(y,1000000007,aa,bb);
		x=(x*(n-i+1))%1000000007*((aa+1000000007)%1000000007);
		y++;
		f[i]=(f[i-1]+f[i-2])%1000000007;
		x%=1000000007;
		ans=(ans+(f[i]*x)%1000000007)%1000000007;
	}
	printf("%lld",ans+f[0]);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<ctime>
using namespace std;
unsigned long long ans;
int n,m,i,x,j,t,tt;
#define N 1010
int a[N],f[N][N];
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		x=x*10+ch-'0',ch=getchar();
	return x*f;
}
inline void write(unsigned long long x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
int main()
{
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	n=read();
	m=read();
	for (i=1;i<=n;i++) a[i]=read();
	for (x=1;x<=n;x++)
		{
			f[0][0]=1;
			for (i=1;i<=n;i++)
				for (j=0;j<=m;j++)
					if (f[i-1][j])
						{
							t=f[i-1][j];
							f[i][j]+=t;
							if (f[i][j]>=10) f[i][j]-=10;
							tt=j+a[i];
							if (i!=x&&tt<=m)
								{
									f[i][tt]+=t;
									if (f[i][tt]>=10) f[i][tt]-=10;
								}
							f[i-1][j]=0;
						}
			ans=0;
			for (i=1;i<=m;i++)
				ans=(ans*10+f[n][i]),f[n][i]=0;
			write(ans);
			puts("");
		}
}

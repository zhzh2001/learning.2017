#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=50005,maxm=140000,mod=998244353;
struct pp{
	int l,r,rev,plu,sum[6];
}a[maxm];
int opt,x,y,z,n,q,num[maxn],ans[6],c[maxn][6];
inline void pushup(int k)
{
	a[k].sum[0]=1; 
	for (int i=1;i<6;i++) a[k].sum[i]=0;
	for (int i=1;i<6;i++)
		for (int j=0;j<=i;j++) a[k].sum[i]=(a[k].sum[i]+a[k<<1].sum[j]*a[k<<1|1].sum[i-j]+mod)%mod;
}
void build(int k,int l,int r)
{
	a[k].l=l; a[k].r=r; int mid=(l+r)>>1;
	if (l==r) {a[k].sum[0]=1; a[k].sum[1]=num[l]; return;}
	build(k<<1,l,mid); build(k<<1|1,mid+1,r); pushup(k);
}
inline void calc(int k,int vv)
{
	int size=(a[k].r-a[k].l+1),pre[6],kk=vv; memset(pre,0,sizeof(pre)); a[k].plu+=vv;
	for (int i=1;i<=min(size,5);i++) pre[i]=a[k].sum[i]; 
	for (int i=1;i<=min(size,5);kk=(kk*vv)%mod,i++) a[k].sum[i]=(a[k].sum[i]+2*vv*pre[i-1]%mod+c[size][i]*kk%mod)%mod;
}
inline void rever(int k)
{
	a[k].sum[1]=-a[k].sum[1]; a[k].sum[3]=-a[k].sum[3]; a[k].sum[5]=-a[k].sum[5];  a[k].plu=-a[k].plu; a[k].rev^=1; 
}
inline void pushdown(int k)
{
	int l=a[k].l,r=a[k].r,mid=(l+r)>>1;
	if (a[k].rev){a[k].rev^=1; rever(k<<1); rever(k<<1|1);}
	if (a[k].plu){calc(k<<1,a[k].plu); calc(k<<1|1,a[k].plu); a[k].plu=0;}
}
void update(int k,int x,int y,int vv)
{
	int l=a[k].l,r=a[k].r; pushdown(k);
	if (l==x&&r==y) {calc(k,vv); return;}
	int mid=(l+r)>>1;
	if (mid>=y) update(k<<1,x,y,vv);
		else if (mid<x) update(k<<1|1,x,y,vv); else update(k<<1,x,mid,vv),update(k<<1|1,mid+1,y,vv);
	pushup(k);
}
void update2(int k,int x,int y)
{
	int l=a[k].l,r=a[k].r; pushdown(k);
	if (l==x&&r==y) {rever(k); return;}
	int mid=(l+r)>>1;
	if (mid>=y) update2(k<<1,x,y);
		else if (mid<x) update2(k<<1|1,x,y); else update2(k<<1,x,mid),update2(k<<1|1,mid+1,y);
	pushup(k);
}
inline void doit(int k)
{
	ans[0]=1;
	int pre[6];
	for (int i=0;i<6;i++)pre[i]=ans[i];
	memset(ans,0,sizeof(ans)); ans[0]=1;
	for (int i=1;i<=z;i++)
		for (int j=0;j<=i;j++) ans[i]=(ans[i]+pre[j]*a[k].sum[i-j]+mod)%mod;
}
void query(int k,int x,int y)
{
	int l=a[k].l,r=a[k].r; pushdown(k);
	if (l==x&&r==y) {doit(k); return;}
	int mid=(l+r)>>1;
	if (mid>=y) query(k<<1,x,y);
		else if (mid<x) query(k<<1|1,x,y); else query(k<<1,x,mid),query(k<<1|1,mid+1,y);
}
int main()
{
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	scanf("%d%d",&n,&q); c[0][0]=1;
	for (int i=1;i<=n;i++){
		c[i][0]=1;
		for (int j=1;j<=min(5,i);j++) c[i][j]=(c[i-1][j]+c[i-1][j-1])%mod; 
	}
	for (int i=1;i<=n;i++) scanf("%d",&num[i]); 
	build(1,1,n);
	while (q--){
		scanf("%d%d%d",&opt,&x,&y);
		if (opt!=2) scanf("%d",&z);
		if (opt==1) z=(z+mod)%mod,update(1,x,y,z); 
		 else if (opt==2) update2(1,x,y);
			else {memset(ans,0,sizeof(ans)); query(1,x,y); printf("%d\n",ans[z]);}
	}
	return 0;
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
Ll ans,n,x,y,z,mo=1e9+7,temp;
Ll ksm(Ll x,Ll y){
	Ll ans=1;
	for(;y;y>>=1,x=x*x%mo)
		if(y&1)ans=ans*x%mo;
	return ans;
}
int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	scanf("%lld",&n);
	if(n==0){printf("1");return 0;}
	if(n==1){printf("2");return 0;}
	ans=1;x=1;y=1;
	temp=1;
	for(int i=1;i<=n;i++){
		temp=temp*(n-i+1)%mo*ksm(i,1e9+5)%mo;
		ans=(ans+y*temp)%mo;
		z=y;
		y=(x+y)%mo;
		x=z;
	}
	printf("%lld",ans);
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=50005;
struct tree{Ll l,r;bool b;Ll sum,lazy;}T[N*3];
Ll n,m,k,x,y,z,mo=998244353;
void up(Ll num){
	Ll x=num*2,y=num*2+1;
	T[num].sum=(T[x].sum+T[y].sum)%mo;
}
void make(Ll l,Ll r,Ll num){
	T[num].l=l; T[num].r=r; 
	if(l==r){
		Ll x;
		scanf("%lld",&x);
		T[num].sum=x%mo;
		return;
	}
	Ll mid=l+r>>1;
	make(l,mid  ,num*2  );
	make(mid+1,r,num*2+1);
	up(num);
}
void push(Ll num){
	Ll x=num*2,y=num*2+1;
	if(T[num].b){
		T[x].sum=(-T[x].sum+mo)%mo;
		T[y].sum=(-T[y].sum+mo)%mo;
		T[x].b=!T[x].b;
		T[y].b=!T[y].b;
		T[num].b=0;
	}
	if(T[num].lazy){
		T[x].sum=(T[x].sum+T[num].lazy*(T[x].r-T[x].l+1))%mo;
		T[y].sum=(T[y].sum+T[num].lazy*(T[y].r-T[y].l+1))%mo;
		T[x].lazy=(T[x].lazy+T[num].lazy)%mo;
		T[y].lazy=(T[y].lazy+T[num].lazy)%mo;
		T[num].lazy=0;
	}
}
void add(Ll num,Ll x,Ll y,Ll z){
	push(num);
	if(x<=T[num].l&&T[num].r<=y){
		T[num].sum=(z*(T[num].r-T[num].l+1)+T[num].sum)%mo;
		T[num].lazy=z; return;
	}
	if(T[num*2  ].r>=x)add(num*2  ,x,y,z);
	if(T[num*2+1].l<=y)add(num*2+1,x,y,z);
	up(num);
}
Ll out(Ll num,Ll x,Ll y){
	push(num);
	if(x<=T[num].l&&T[num].r<=y)return T[num].sum;
	Ll ans=0;
	if(T[num*2  ].r>=x)ans+=out(num*2  ,x,y);
	if(T[num*2+1].l<=y)ans+=out(num*2+1,x,y);
	return ans%mo;
}
void fan(Ll num,Ll x,Ll y){
	push(num);
	if(x<=T[num].l&&T[num].r<=y){
		T[num].sum=(-T[num].sum+mo)%mo;
		T[num].b=1; return;
	}
	if(T[num*2  ].r>=x)fan(num*2  ,x,y);
	if(T[num*2+1].l<=y)fan(num*2+1,x,y);
	up(num);
}
Ll find(Ll x,Ll y,Ll z){
	if(z==1)return out(1,x,y);
	Ll ans=0;
	for(Ll i=x;y-i>=z-1;i++)ans=(ans+out(1,i,i)*find(i+1,y,z-1))%mo;
	return ans;
}
int main()
{
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	make(1,n,1);
//	for(Ll i=1;i<=100;i++)
//		if(T[i].l)
//			cout<<T[i].l<<' '<<T[i].r<<' '<<T[i].sum<<endl;
	while(m--){
		scanf("%lld",&k);
		if(k==1){
			scanf("%lld%lld%lld",&x,&y,&z);
			z=(z%mo+mo)%mo;
			add(1,x,y,z);
		}else
		if(k==2){
			scanf("%lld%lld",&x,&y);
			fan(1,x,y);
		}else
		if(k==3){
			scanf("%lld%lld%lld",&x,&y,&z);
			printf("%lld\n",find(x,y,z));
		}
//		else{
//			scanf("%lld%lld",&x,&y);
//			cout<<out(1,x,y)<<endl;;
//		}
	}
}

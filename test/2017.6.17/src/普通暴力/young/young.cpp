#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define mo 998244353
#define ll long long
using namespace std;
int n,m,fl,x,y,z,a[50005];
ll f[50005][6];
int main(){
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout); 
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=0;i<=n;i++) f[i][0]=1;
	for (int i=1;i<=m;i++){
		scanf("%d",&fl);
		if (fl==2){
			scanf("%d%d",&x,&y);
			for (int j=x;j<=y;j++)
				a[j]=(mo-a[j])%mo;
		}
		else if (fl==1){
			scanf("%d%d%d",&x,&y,&z);
			for (int j=x;j<=y;j++)
				a[j]=(a[j]+z)%mo;
		}
		else{
			scanf("%d%d%d",&x,&y,&z);
			for (int j=1;j<=5;j++) f[x-1][j]=0;
			for (int j=x;j<=y;j++)
				for (int k=1;k<=z;k++)
					f[j][k]=(f[j-1][k]+f[j-1][k-1]*a[j])%mo;
			printf("%lld\n",f[y][z]);
		}
	}
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 56000005 
#define ll long long
#define mo 1000000007 
using namespace std;
int f[N],inv[N],n,fac;
ll ans;
inline ll power(ll x,ll y){
	ll s=1;
	for (;y;y>>=1,x=x*x%mo)
		if (y&1) s=s*x%mo;
	return s;
}
int main(){
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	scanf("%d",&n);
	fac=f[0]=f[1]=1;
	for (int i=2;i<=n;++i){
		fac=(ll)fac*i%mo;
		f[i]=(f[i-1]+f[i-2])%mo;
	}
	inv[n]=power(fac,mo-2);
	for (int i=n;i;--i)
		inv[i-1]=(ll)inv[i]*i%mo;
	ans=0;
	for (int i=0;i<=n;++i)
		ans+=(ll)fac*inv[i]%mo*inv[n-i]%mo*f[i]%mo;
	printf("%lld\n",ans%mo);
}

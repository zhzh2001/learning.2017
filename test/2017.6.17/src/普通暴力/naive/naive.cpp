#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 5005
using namespace std;
int n,m,a[N],f[N],c[N],vis[N],F[N],g[N];
unsigned long long ans,Ans[N];
int main(){
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout); 
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
		scanf("%d",&a[i]);
	for (int i=1;i<=n;++i){
		if (vis[a[i]]){
			printf("%llu\n",Ans[a[i]]);
			continue;
		}
		memset(f,0,sizeof(f));
		f[0]=1;
		for (int j=1;j<=n;j++)
			if (j!=i)
				for (int k=m;k>=a[j];k--)
					f[k]=(f[k]+f[k-a[j]])%10;
		ans=0;
		for (int j=1;j<=m;j++)
			ans=ans*10+f[j];
		printf("%llu\n",ans);
		Ans[a[i]]=ans;
		vis[a[i]]=1;
		/*for (int j=m;j>=a[i];j--)
			g[j]=(g[j]+g[j-a[i]])%10;*/
	}
}

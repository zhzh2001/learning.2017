#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m,a[5005],f[5005],c[5005];
unsigned long long ans;
int main(){
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout); 
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
		scanf("%d",&a[i]);
	f[0]=1;
	for (int i=1;i<=n;++i)
		for (int j=m;j>=a[i];--j)
			f[j]=(f[j]+f[j-a[i]])%10;
	for (int i=1;i<=n;++i){
		for (int j=0;j<=m;++j)
			c[j]=f[j];
		for (int j=a[i];j<=m;++j)
			c[j]=(c[j]-c[j-a[i]]+10)%10;
		ans=0;
		for (int j=1;j<=m;j++)
			ans=ans*10+c[j];
		printf("%llu\n",ans);
	}
}

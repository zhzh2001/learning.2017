#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef unsigned long long ll;
inline ll read(){
	ll k=0;char ch=getchar();
	while(ch<'0'||ch>'9'){ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k;
}
inline void write(ll x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
ll n,m,a[10001],f[10001];
int main()
{
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	n=read();m=read();
	for(ll i=1;i<=n;i++)a[i]=read();
	for(ll rp=1;rp<=n;rp++){
		for(ll i=1;i<=m;i++)f[i]=0;f[0]=1;
		for(ll i=1;i<=n;i++)if(i!=rp)
			for(ll j=m;j>=a[i];j--)f[j]+=f[j-a[i]];
		ll ans=0;for(ll i=1;i<=m;i++)ans=ans*(ll)10+f[i]%(ll)10;
		writeln(ans);
	}
	return 0;
}
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const ll MOD=998244353;
ll jzq[100001],ans=0,n,m,a[200001],t[200001],lt[200001],rt[200001],add[200001],mul[200001];
inline void clean(ll nod){
	if(add[nod]==0&&mul[nod]==1)return;
	if(lt[nod]!=rt[nod]){
		t[nod*2]=((t[nod*2]*mul[nod]%MOD+MOD)%MOD+(add[nod]*(rt[nod*2]-lt[nod*2]+1))%MOD)%MOD;
		add[nod*2]=((add[nod*2]*mul[nod]%MOD+MOD)%MOD+add[nod])%MOD;mul[nod*2]=mul[nod*2]*mul[nod];
		t[nod*2+1]=((t[nod*2+1]*mul[nod]%MOD+MOD)%MOD+(add[nod]*(rt[nod*2+1]-lt[nod*2+1]+1))%MOD)%MOD;
		add[nod*2+1]=((add[nod*2+1]*mul[nod]%MOD+MOD)%MOD+add[nod])%MOD;mul[nod*2+1]=mul[nod*2+1]*mul[nod];
	}
	add[nod]=0;mul[nod]=1;
}
inline void build(ll l,ll r,ll nod){
	lt[nod]=l;rt[nod]=r;mul[nod]=1;
	if(l==r){t[nod]=a[l]%MOD;return;}
	ll mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=(t[nod*2]+t[nod*2+1]+MOD)%MOD;
}
inline void xgadd(ll i,ll j,ll p,ll nod){
	clean(nod);
	if(i==lt[nod]&&j==rt[nod]){add[nod]=p;t[nod]=(t[nod]+((rt[nod]-lt[nod]+1)*p)%MOD)%MOD;return;}
	ll mid=lt[nod]+rt[nod]>>1;
	if(i<=mid)xgadd(i,min(j,mid),p,nod*2);
	if(j>mid)xgadd(max(i,mid+1),j,p,nod*2+1);
	t[nod]=(t[nod*2+1]+t[nod*2]+MOD)%MOD;
}
inline void xgmul(ll i,ll j,ll nod){
	clean(nod);
	if(i==lt[nod]&&j==rt[nod]){mul[nod]=-1;t[nod]=(t[nod]*(-1)+MOD)%MOD;return;}
	ll mid=lt[nod]+rt[nod]>>1;
	if(i<=mid)xgmul(i,min(j,mid),nod*2);
	if(j>mid)xgmul(max(i,mid+1),j,nod*2+1);
	t[nod]=(t[nod*2+1]+t[nod*2]+MOD)%MOD;
}
inline ll s(ll i,ll j,ll nod){
	clean(nod);
	if(lt[nod]==i&&rt[nod]==j)return t[nod]%MOD;
	ll mid=lt[nod]+rt[nod]>>1,sum=0;
	if(i<=mid)(sum+=s(i,min(j,mid),nod*2)+MOD)%=MOD;
	if(j>mid)(sum+=s(max(i,mid+1),j,nod*2+1)+MOD)%=MOD;
	return (sum+MOD)%MOD;
}
inline void dfs(ll x,ll w,ll y,ll z){
	if(x==z)ans=(ans+y+MOD)%MOD;
	for(ll i=w+1;i<=jzq[0]-z+x+1;i++)dfs(x+1,i,(y*jzq[i]+MOD)%MOD,z);
}
int main()
{
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	n=read();m=read();
	for(ll i=1;i<=n;i++)a[i]=read();
	build(1,n,1);
	for(ll i=1;i<=m;i++){
		ll p=read(),x=read(),y=read();
		if(p==2)xgmul(x,y,1);
		else{
			ll z=read();
			if(p==1)xgadd(x,y,z,1);
			else{
				if(z==1)writeln(s(x,y,1)%MOD);
				else if(z==2){
					ll rp=s(x,y,1),ss=0;ans=0;
					for(ll j=x;j<y;j++){
						ll pr=s(j,j,1);(ss+=pr%MOD+MOD)%=MOD;
						ans=(ans+pr*(rp-ss+MOD)%MOD+MOD)%MOD;
					}
					writeln(ans);
				}else{
					jzq[0]=0;ans=0;
					for(ll j=x;j<=y;j++)jzq[++jzq[0]]=s(j,j,1);
					dfs(0,0,1,z);
					writeln(ans);
				}
			}
		}
	}
	return 0;
}
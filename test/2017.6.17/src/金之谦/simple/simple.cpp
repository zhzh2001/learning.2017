#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const ll MOD=1e9+7;
ll n,jc[5000001];
inline ll mi(ll a,ll b){
	ll x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;
		y=y*y%MOD;b>>=1;
	}
	return x;
}
inline ll C(ll x,ll y){
	return jc[x]*mi(jc[y]*jc[x-y],MOD-2)%MOD;
}
int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	n=read();ll i;jc[0]=1;
	if(n<2)return puts("1")&0;
	for(i=1;i<=n;++i)jc[i]=jc[i-1]*i%MOD;
	ll x=1,y=1,t,sum=C(n,0)+C(n,1);
	for(i=2;i<=n;++i){
		sum=sum+(x+y)*C(n,i)%MOD;t=x;x=y;y=t+x;
		sum=sum<MOD?sum:sum-MOD;
		x=x<MOD?x:x-MOD;y=y<MOD?y:y-MOD;
	}
	write(sum);
	return 0;
}
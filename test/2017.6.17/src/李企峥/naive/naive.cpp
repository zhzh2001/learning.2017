#include<bits/stdc++.h>
#define ll long long
#define ull unsigned long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 18446744073709551616
using namespace std;
ull n,m,x;
int f[4010][4010];
int a[4010];
inline ull read()
{
    ull x=0;char ch=getchar();
    while(ch<'0'||ch>'9')ch=getchar();
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x;
}
int main()
{
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	n=read();m=read();
	For(i,1,n)a[i]=read();
	For(i,1,n)f[i][0]=1;
	For(i,1,n)
	{
		For(j,1,n)
		{
			if(j==i)continue;
			Rep(k,a[j],m)(f[i][k]+=f[i][k-a[j]])%=10;
		}
	}
	For(i,1,n)
	{
		x=0;
		For(j,1,m)
		{
			x*=10;
			x+=f[i][j];
			//x%=mo;
		}
		cout<<x<<endl;
	}
	return 0;
}


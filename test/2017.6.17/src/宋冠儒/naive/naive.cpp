#include<bits/stdc++.h>
#define ull unsigned long long
using namespace std;
int n,m;
int a[4005];
ull c[4005][4005],f[4005][4005];
 void read(int &x)
{
    char c=getchar();int f=1; x=0;
    while(c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') x=x*10+c-48,c=getchar();
    x=x*f;
}
 int main()
{
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=n;++i) read(a[i]);
	f[0][0]=1;
	for (int i=1;i<=n;++i)
		for (int j=0;j<=m;++j)
		{
			f[i][j]=(f[i][j]+f[i-1][j])%10;
			if (j>=a[i]) f[i][j]=(f[i][j]+f[i-1][j-a[i]])%10;
		}	
	for (int i=1;i<=n;++i)
	{
		ull ans=0;
		for (int j=0;j<=m;++j)
		{
			if (j<a[i]) c[i][j]=f[n][j];
			else c[i][j]=(f[n][j]-c[i][j-a[i]]+10)%10;
			if (j>0) ans=ans*10+c[i][j]; 
		}
		cout<<ans<<endl;
	}
}

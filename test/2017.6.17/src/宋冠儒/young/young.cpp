#include<bits/stdc++.h>
#define mo 998244353
#define ll long long
using namespace std;
int n,q;
struct data{
	int a[21];
	int len;
}t[200005];
int a[50005];
ll c[50005][21];
bool bz[200005];
int ad[200005];
data ans;
 void read(int &x)
{
    char c=getchar();int f=1; x=0;
    while(c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') x=x*10+c-48,c=getchar();
    x=x*f;
}
 void calc()
{
	c[0][0]=1;
	for (int i=1;i<=n;++i)
	{
		c[i][0]=1;
		for (int j=1;j<=20;++j) c[i][j]=(c[i-1][j]+c[i-1][j-1])%mo;
	}
}
 void merge(data a,data b,data &c)
{
	c.len=a.len+b.len;
	for (int i=0;i<=20;++i) c.a[i]=0;
	for (int i=0;i<=min(a.len,5);++i)
		for (int j=0;j<=min(b.len,5);++j)
			if (i+j<=5) c.a[i+j]=(c.a[i+j]+(ll)a.a[i]*b.a[j]%mo)%mo; 
}
 void add(int k,int x)
{
	for (int i=min(t[k].len,5);i>=0;--i)
	{
		int xx=x;
		for (int j=i-1;j>=0;--j)
		{
			t[k].a[i]=(t[k].a[i]+(ll)t[k].a[j]*c[t[k].len-j][i-j]%mo*xx%mo)%mo;
			t[k].a[i]=(t[k].a[i]+mo)%mo;
			xx=(ll)xx*x%mo;
		}
	}
	ad[k]=(ad[k]+x)%mo;
}
 void rev(int k)
{
	for (int i=0;i<=min(t[k].len,5);++i)
	{
		t[k].a[i]*=(i%2)?-1:1;
		t[k].a[i]=(t[k].a[i]+mo)%mo;
	}
	bz[k]^=1;
	ad[k]=-ad[k];
}
 void pushdown(int k)
{
	if (bz[k])
	{
		rev(k<<1);
		rev(k<<1|1);
		bz[k]=0;
	}
	if (ad[k])
	{
		add(k<<1,ad[k]);
		add(k<<1|1,ad[k]);
		ad[k]=0;
	}
} 
 void change1(int k,int l,int r,int a,int b,int x)
{
	if (l==a&&r==b)
	{
		add(k,x);
		return;
	}
	pushdown(k);
	int mid=(l+r)>>1;
	if (b<=mid) change1(k<<1,l,mid,a,b,x);
	else if (a>mid) change1(k<<1|1,mid+1,r,a,b,x);
	else
	{
		change1(k<<1,l,mid,a,mid,x);
		change1(k<<1|1,mid+1,r,mid+1,b,x);
	}
	merge(t[k<<1],t[k<<1|1],t[k]);
}
 void change2(int k,int l,int r,int a,int b)
{
	if (l==a&&r==b)
	{
		rev(k);
		return;
	}
	pushdown(k);
	int mid=(l+r)>>1;
	if (b<=mid) change2(k<<1,l,mid,a,b);
	else if (a>mid) change2(k<<1|1,mid+1,r,a,b);
	else
	{
		change2(k<<1,l,mid,a,mid);
		change2(k<<1|1,mid+1,r,mid+1,b);
	}
	merge(t[k<<1],t[k<<1|1],t[k]);
}
 void build(int k,int l,int r)
{
	if (l==r)
	{
		t[k].len=1;
		t[k].a[0]=1;
		t[k].a[1]=a[l];
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
	merge(t[k<<1],t[k<<1|1],t[k]);
}
 void query(int k,int l,int r,int a,int b)
{
	if (l==a&&r==b)
	{
		merge(ans,t[k],ans);
		return;
	}
	pushdown(k);
	int mid=(l+r)>>1;
	if (b<=mid) query(k<<1,l,mid,a,b);
	else if (a>mid) query(k<<1|1,mid+1,r,a,b);
	else
	{
		query(k<<1,l,mid,a,mid);
		query(k<<1|1,mid+1,r,mid+1,b);
	}
}
 int main()
{
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	read(n);read(q);
	for (int i=1;i<=n;++i) read(a[i]);
	calc();	
	build(1,1,n);
	for (int i=1;i<=q;++i)
	{
		int opt,x,y;
		read(opt);read(x);read(y);
		if (opt==1)
		{
			int kkk;
			read(kkk);
			change1(1,1,n,x,y,kkk);
		}
		else if (opt==2) change2(1,1,n,x,y);
		else 
		{
			int kkk;
			read(kkk);
			ans.len=0;
			ans.a[0]=1;
			query(1,1,n,x,y);
			printf("%d\n",ans.a[kkk]);
		}
	}
}

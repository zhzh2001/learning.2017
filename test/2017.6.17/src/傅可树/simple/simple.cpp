#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<ctime>
using namespace std;
int ff[10000001];
long long n;
int i,sum,t,a,b,c,ans,P=1000000007,last;
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		x=x*10+ch-'0',ch=getchar();
	return x*f;
}
inline long long Read()
{
	long long x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		x=x*10+ch-'0',ch=getchar();
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline int ksm(int x,int y)
{
	int z=1;
	while (y)
		{
			if (y&1) z=1ll*x*z%P;
			x=1ll*x*x%P;
			y>>=1;
		}
	return z;
}
int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	n=Read();
	ans=1;
	sum=1;
	for (i=1;i<=n;i++) sum=1ll*sum*i%P;
	ff[0]=ksm(sum,P-2);
	t=n/2;
	for (i=0;i<t;i++) ff[i+1]=1ll*ff[i]*(n-i)%P;
	last=ff[t];
	for (i=t;i<n;i++)
		{
			last=1ll*last*(n-i)%P;
			ff[n-i-1]=1ll*ff[n-i-1]*last%P*sum%P;
		}
	if ((n&1)==0) ff[t]=1ll*ff[t]*ff[t]%P*sum%P;
	a=0;
	b=1;
	for (i=1;i<=t;i++)
		{
			c=a+b;
			if (c>=P) c-=P;
			a=b;
			b=c;
			ans=1ll*c*ff[i]%P+ans;
			if (ans>=P) ans-=P;
		}
	for (i=n-t-1;i>=0;i--)
		{
			c=a+b;
			if (c>=P) c-=P;
			a=b;
			b=c;
			ans=1ll*c*ff[i]%P+ans;
			if (ans>=P) ans-=P;
		}
	write(ans);
}

#include <bits/stdc++.h>
#define mo 998244353
using namespace std;

struct po{
	int l, r;
	long long ans[6], tag1, tag2;
	po(){ans[1] = ans[2] = ans[3] = ans[4] = ans[5] = 0; }
}t[50005 << 2];

int n, m, a, b;

long long c, w[100005];

po merge(po x, po y){
	po pro; pro.l = x.l, pro.r = y.r;
	for(int i = 1; i <= 5; i++){
		pro.ans[i] = (x.ans[i] + y.ans[i]) % mo;
		for(int j = 1; j < i; j++)
			pro.ans[i] = (pro.ans[i] + (x.ans[j] * y.ans[i - j]) % mo) % mo;
		pro.ans[i] = (pro.ans[i] + mo) % mo;
	}
	return pro;
}	

void build(int l, int r, int k){
	t[k].l = l, t[k].r = r;
	if(l == r){t[k].ans[1] = w[l] % mo; return; }
	int mid = (l + r) >> 1;
	build(l, mid, k << 1), build(mid + 1, r, k << 1 | 1);
	t[k] = merge(t[k << 1], t[k << 1 | 1]);
}

po query(int l, int r, int k){
	if(t[k].l == l && t[k].r == r) return t[k];
	int mid = (t[k].l + t[k].r) >> 1;
	if(r <= mid) return query(l, r, k << 1);
	else if(l > mid) return query(l, r, k << 1 | 1);
	else return merge(query(l, mid, k << 1), query(mid + 1, r, k << 1 | 1));
}

int main(){
	freopen("young.in", "r", stdin);
	freopen("young.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; i++) scanf("%lld", &w[i]);
	build(1, n, 1);
	for(int i = 1; i <= m; i++){
		int pro; po prov; scanf("%d", &pro);
		if(pro == 1){
			scanf("%d%d%lld", &a, &b, &c);
			for(int j = a; j <= b; j++) w[j] += c, w[j] %= mo;
			build(1, n, 1);
		}
		else if(pro == 2){
			scanf("%d%d", &a, &b);
			for(int j = a; j <= b; j++) w[j] = -w[j], w[j] += mo, w[j] %= mo;
			build(1, n, 1);
		}
		else scanf("%d%d%lld", &a, &b, &c), prov = query(a, b, 1), printf("%d\n", (prov.ans[c] + mo) % mo);
	}
	return 0;
}
	



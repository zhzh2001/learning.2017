#include <bits/stdc++.h>
#define mo 998244353
using namespace std;

struct po{
	int l, r;
	long long ans[6], tag1, tag2;
	po(){ans[1] = ans[2] = ans[3] = ans[4] = ans[5] = 0; }
}t[50005 << 2];

int n, m, a, b, c, w[100005];

po merge(po x, po y){
	po pro; pro.l = x.l, pro.r = y.r, pro.tag1 = 0, pro.tag2 = 1;
	pro.ans[1] = (x.ans[1] + y.ans[1]) % mo;
	for(int i = 2; i <= 1; i++){
		pro.ans[i] = (x.ans[i] + y.ans[i]) % mo;
		for(int j = 1; j < i; j++) pro.ans[i] = (pro.ans[i] + (x.ans[j] * y.ans[i - j]) % mo) % mo;
		pro.ans[i] = (pro.ans[i] + mo) % mo;
	}
	return pro;
}	

void cal(int x, int y){
	long long prox = t[x].r - t[x].l + 1;
	long long prol = t[x << 1].r - t[x << 1].l + 1, pror = t[x << 1 | 1].r - t[x << 1 | 1].l + 1;
	t[x].ans[1] = (t[x].ans[1] + (prox * y) % mo) % mo;	
	/*
	for(int i = 2; i <= min(5LL, prox); i++){for(int j = 1; j < i; j++)
		t[x].ans[i] = (prol * t[x << 1 | 1].ans[i] % mo + pror * t[x << 1].ans[i - j] % mo + t[x].ans[i]) % mo;
		t[x].ans[i] = (t[x].ans[i] + mo) % mo;
	}*/
}

void rev(int x){
	t[x].tag1 *= -1;
	for(int i = 1; i <= 1; i++) if(i & 1) t[x].ans[i] *= -1, t[x].ans[i] = (t[x].ans[i] + mo) % mo;
}	

void build(int l, int r, int k){
	t[k].l = l, t[k].r = r;
	if(l == r){t[k].ans[1] = w[l] % mo; return; }
	int mid = (l + r) >> 1;
	build(l, mid, k << 1), build(mid + 1, r, k << 1 | 1);
	t[k] = merge(t[k << 1], t[k << 1 | 1]);
}

void change(int w, int l, int r, int k){
	if(t[k].l == l && t[k].r == r){cal(k, w), t[k].tag1 = (t[k].tag1 + t[k].tag2 * w % mo) % mo; return; }
	if(t[k].tag2 == -1){
		rev(k << 1), rev(k << 1 | 1);		
		t[k << 1].tag2 *= -1, t[k << 1 | 1].tag2 *= -1, t[k].tag2 *= -1;
	}
	if(t[k].tag1){
		cal(k << 1, t[k].tag1), cal(k << 1 | 1, t[k].tag1);
		t[k << 1].tag1 = (t[k << 1].tag1 + t[k].tag1) % mo, t[k << 1 | 1].tag1 = (t[k << 1 | 1].tag1 + t[k].tag1) % mo, t[k].tag1 = 0;
	}
	int mid = (t[k].l + t[k].r) >> 1;
	if(r <= mid) change(w, l, r, k << 1);
	else if(l > mid) change(w, l, r, k << 1 | 1);
	else change(w, l, mid, k << 1), change(w, mid + 1, r, k << 1 | 1);
	t[k] = merge(t[k << 1], t[k << 1 | 1]);
}

void rever(int l, int r, int k){
	if(t[k].l == l && t[k].r == r){rev(k), t[k].tag2 *= -1; return; }
	if(t[k].tag2 == -1){
		rev(k << 1), rev(k << 1 | 1);		
		t[k << 1].tag2 *= -1, t[k << 1 | 1].tag2 *= -1, t[k].tag2 *= -1;
	}
	if(t[k].tag1){
		cal(k << 1, t[k].tag1), cal(k << 1 | 1, t[k].tag1);
		t[k << 1].tag1 = (t[k << 1].tag1 + t[k].tag1) % mo, t[k << 1 | 1].tag1 = (t[k << 1 | 1].tag1 + t[k].tag1) % mo, t[k].tag1 = 0;
	}
	int mid = (t[k].l + t[k].r) >> 1;
	if(r <= mid) rever(l, r, k << 1);
	else if(l > mid) rever(l, r, k << 1 | 1);
	else rever(l, mid, k << 1), rever(mid + 1, r, k << 1 | 1);
	t[k] = merge(t[k << 1], t[k << 1 | 1]);
}

po query(int l, int r, int k){
	if(t[k].l == l && t[k].r == r) return t[k];
	if(t[k].tag2 == -1){
		rev(k << 1), rev(k << 1 | 1);		
		t[k << 1].tag2 *= -1, t[k << 1 | 1].tag2 *= -1, t[k].tag2 *= -1;
	}
	if(t[k].tag1){
		cal(k << 1, t[k].tag1), cal(k << 1 | 1, t[k].tag1);
		t[k << 1].tag1 = (t[k << 1].tag1 + t[k].tag1) % mo, t[k << 1 | 1].tag1 = (t[k << 1 | 1].tag1 + t[k].tag1) % mo, t[k].tag1 = 0;
	}
	int mid = (t[k].l + t[k].r) >> 1;
	if(r <= mid) return query(l, r, k << 1);
	else if(l > mid) return query(l, r, k << 1 | 1);
	else return merge(query(l, mid, k << 1), query(mid + 1, r, k << 1 | 1));
	t[k] = merge(t[k << 1], t[k << 1 | 1]);
}

int main(){
	freopen("young1.in", "r", stdin);
	freopen("young.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; i++) scanf("%d", &w[i]);
	build(1, n, 1);
	for(int i = 1; i <= m; i++){
		int pro; po prov; scanf("%d", &pro);
		if(pro == 1) scanf("%d%d%d", &a, &b, &c), c %= mo, change(c, a, b, 1);
		else if(pro == 2) scanf("%d%d", &a, &b), rever(a, b, 1);
		else scanf("%d%d%d", &a, &b, &c), prov = query(a, b, 1), printf("%d\n", (prov.ans[c] + mo) % mo);
	}
	return 0;
}
		
		



#include <bits/stdc++.h>
using namespace std;

int n, m, w[4005];

unsigned long long ans, dp[4005];

int main(){
	freopen("naive.in", "r", stdin);
	freopen("naive.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; ++i) scanf("%d", &w[i]);
	for(int i = 1; i <= n; ++i){
		memset(dp, 0, sizeof(dp)), dp[0] = 1;
		for(int j = 1; j <= n; ++j) if(i != j)
			for(int k = m; k >= w[j]; --k) dp[k] += dp[k - w[j]];
		for(int j = 1; j <= m; ++j) ans = ans * 10 + dp[j] % 10;
		cout<<ans<<endl; ans = 0;
	}
	return 0;
}


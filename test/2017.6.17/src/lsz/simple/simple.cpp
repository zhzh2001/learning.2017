#include <bits/stdc++.h>
#define mo 1000000007
using namespace std;

int n, F1, F2, F3, rev[100000000], ans;

long long C;

int main(){
	freopen("simple.in", "r", stdin);
	freopen("simple.out", "w", stdout);
	scanf("%d", &n), C = n, ans = n + 1, F2 = F1 = 1, rev[1] = 1;
	if(ans >= mo) ans -= mo;
	for(int i = 2; i <= n; i++){
		rev[i] = 1LL * (mo - mo / i) * rev[mo % i] % mo;
		C = ((C * (n - i + 1)) % mo * rev[i]) % mo, F1 += F2;
		F3 = F1, F1 = F2, F2 = F3;
		if(F2 >= mo) F2 -= mo;		
		ans = ans + (C * F2) % mo; if(ans >= mo) ans -= mo;
	}
	printf("%d", ans);
	return 0;
}



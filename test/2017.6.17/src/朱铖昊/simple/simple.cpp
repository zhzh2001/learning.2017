#include<bits/stdc++.h>
using namespace std;
#define ll unsigned long long
#define iter iterator
inline void read(unsigned &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(ll x)
{
	if (x==0)
		putchar('0');
	unsigned a[25];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const unsigned N=40000005,mod=1e9+7;
// unsigned n,a[N],c[N],f[N];
unsigned n,a[N],c[N],x,y,z;
// unsigned n,a[N],c[N],f[3];
ll ans;
int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	read(n);
	a[1]=1;
	for (unsigned i=2;i<=n/2;++i)
		a[i]=(ll)(mod-mod/i)*a[mod%i]%mod;
	c[0]=1;
	for (unsigned i=1;i<=n/2;++i)
		c[i]=(ll)c[i-1]*a[i]%mod*(n-i+1)%mod;
	// f[0]=1;
	// f[1]=1;
	// for (unsigned i=2;i<=n;++i)
		// f[i]=(f[i-1]+f[i-2])%mod;
	// for (unsigned i=0;i<=n/2;++i)
		// ans=(ans+(ll)f[i]*c[i]%mod)%mod;
	// for (unsigned i=n/2+1;i<=n;++i)
		// ans=(ans+(ll)f[i]*c[n-i]%mod)%mod;
	x=1;
	y=1;
	ans=(c[0]+c[1])%mod;
	for (unsigned i=2;i<=n/2;++i)
	{
		z=(x+y)%mod;
		ans=(ans+(ll)z*c[i]%mod)%mod;
		x=y;
		y=z;
	}
	for (int i=n-n/2-1;i>=0;--i)
	{
		z=(x+y)%mod;
		ans=(ans+(ll)z*c[i]%mod)%mod;
		x=y;
		y=z;
	}
	// f[0]=1;
	// f[1]=1;
	// ans=(c[0]+c[1])%mod;
	// for (unsigned i=2;i<=n/2;++i)
	// {
		// f[i%3]=(f[(i+1)%3]+f[(i+2)%3])%mod;
		// ans=(ans+(ll)f[i%3]*c[i]%mod)%mod;
	// }
	// for (unsigned i=n/2+1;i<=n;++i)
	// {
		// f[i%3]=(f[(i+1)%3]+f[(i+2)%3])%mod;
		// ans=(ans+(ll)f[i%3]*c[n-i]%mod)%mod;
	// }
	write(ans);
	return 0;
}

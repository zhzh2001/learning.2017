#include<bits/stdc++.h>
#define lca long long
#define mod 998244353
#define N 200020
using namespace std;
inline lca read()
{
	lca x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
lca n,m;
lca a[N];
lca l,r,ret=0;
void dfs(lca u,lca now,lca val,lca gol)
{
	if(now==gol){ret+=val,ret%=mod;return;}
	if(u==r+1)return;
	dfs(u+1,now,val,gol);
	dfs(u+1,now+1,val*a[u]%mod,gol);
}
int main()
{
	freopen("young.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read(),m=read();
	for(lca i=1;i<=n;i++)a[i]=read()%mod;
	for(lca i=1;i<=m;i++)
	{
		lca op=read(),x=read(),y=read();
		lca z;if(op!=2)z=read();
		if(op==1)
			for(lca j=x;j<=y;j++)a[j]+=z,a[j]%=mod;
		else if(op==2)
			for(lca j=x;j<=y;j++)a[j]=mod-a[j];
		else
		{
			l=x,r=y,ret=0;
			dfs(l,0,1,z);
			printf("%lld\n",(ret+mod)%mod);
		}
	}
}

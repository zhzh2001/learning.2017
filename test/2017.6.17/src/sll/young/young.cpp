#include<bits/stdc++.h>
#define lca long long
#define mod 998244353
#define N 50020
using namespace std;
inline lca read()
{
	lca x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int top;
lca n,m;
lca a[N];lca bin[6];
lca qm(lca a,lca b)
{
	lca ret=1;
	while(b)
	{
		if(b&1)ret=ret*a%mod;
		a=a*a%mod,b>>=1;
	}return ret;
}
lca pre[N],inv[N];
void init()
{
	pre[0]=inv[0]=1;pre[1]=inv[1]=1;
	for(lca i=2;i<N;i++)pre[i]=pre[i-1]*i%mod;
	for(lca i=2;i<N;i++)inv[i]=qm(pre[i],mod-2);
}
lca C(lca n,lca m)
{
	return pre[n]*inv[m]%mod*inv[n-m]%mod;
}
struct seg
{
	struct edge{bool zig;lca l,r;lca tag,ans[6];}e[N<<2];
	edge merge(edge x,edge y)
	{
		edge ret;ret.l=x.l,ret.r=y.r;
		for(int i=0;i<=top;i++)ret.ans[i]=0;
		for(lca i=1;i<=top;i++)
		{
			ret.ans[i]=(x.ans[i]+y.ans[i])%mod;
			for(lca j=1;j<i;j++)
			{
				ret.ans[i]+=(x.ans[j]*y.ans[i-j])%mod;
				ret.ans[i]%=mod;
			}
		}return ret;
	}
	void pushup(lca u)
	{
		lca tag=e[u].tag,zig=e[u].zig;
		e[u]=merge(e[u<<1],e[u<<1|1]);
		e[u].tag=tag,e[u].zig=zig;
	}
	void work(lca u,lca c)
	{
		bin[1]=c;
		for(lca i=2;i<=top;i++)bin[i]=bin[i-1]*c%mod;
		lca up=min((lca)top,e[u].r-e[u].l+1);
		lca len=e[u].r-e[u].l+1;
		for(lca i=up;i;i--)
		{
			for(lca j=i-1;j;j--)
			{
				e[u].ans[i]+=e[u].ans[j]*bin[i-j]%mod*C(len-j,i-j)%mod;
				e[u].ans[i]%=mod;
			}
			e[u].ans[i]+=bin[i]*C(len,i)%mod;
			e[u].ans[i]%=mod;
		}
	}
	void pushdown(lca u)
	{
		if(e[u].l==e[u].r)return;
		if(e[u].tag)
		{
			if(e[u<<1].zig)pushdown(u<<1);
			if(e[u<<1|1].zig)pushdown(u<<1|1);
			e[u<<1].tag+=e[u].tag,e[u<<1|1].tag+=e[u].tag;
			e[u<<1].tag%=mod,e[u<<1|1].tag%=mod;
			work(u<<1,e[u].tag),work(u<<1|1,e[u].tag);
			e[u].tag=0;
		}
		if(e[u].zig)
		{
			if(e[u<<1].tag)pushdown(u<<1);
			if(e[u<<1|1].tag)pushdown(u<<1|1);
			e[u<<1].zig^=1,e[u<<1|1].zig^=1,e[u].zig^=1;
			e[u<<1].ans[1]=(mod-e[u<<1].ans[1])%mod,
			e[u<<1].ans[3]=(mod-e[u<<1].ans[3])%mod,
			e[u<<1].ans[5]=(mod-e[u<<1].ans[5])%mod;
			e[u<<1|1].ans[1]=(mod-e[u<<1|1].ans[1])%mod,
			e[u<<1|1].ans[3]=(mod-e[u<<1|1].ans[3])%mod,
			e[u<<1|1].ans[5]=(mod-e[u<<1|1].ans[5])%mod;
		}

	}
	void build(lca u,lca l,lca r)
	{
		e[u].l=l,e[u].r=r,e[u].tag=0,e[u].zig=0;lca mid=(l+r)>>1;
		for(lca i=0;i<=top;i++)e[u].ans[i]=0;
		if(l==r){e[u].ans[1]=a[l];return;}
		build(u<<1,l,mid);build(u<<1|1,mid+1,r);
		pushup(u);
	}
	void add(lca u,lca x,lca y,lca c)
	{
		pushdown(u);
		lca l=e[u].l,r=e[u].r,mid=(l+r)>>1;
		if(x<=l&&y>=r){work(u,c),e[u].tag+=c,e[u].tag%=mod;return;}
		if(y<=mid)add(u<<1,x,y,c);
		else if(x>mid)add(u<<1|1,x,y,c);
		else add(u<<1,x,mid,c),add(u<<1|1,mid+1,y,c);
		pushup(u);
	}
	void change(lca u,lca x,lca y)
	{
		pushdown(u);
		lca l=e[u].l,r=e[u].r,mid=(l+r)>>1;
		if(x<=l&&y>=r){
			e[u].zig^=1;
			e[u].ans[1]=(mod-e[u].ans[1])%mod,
			e[u].ans[3]=(mod-e[u].ans[3])%mod,
			e[u].ans[5]=(mod-e[u].ans[5])%mod;
			return;
		}
		if(y<=mid)change(u<<1,x,y);
		else if(x>mid)change(u<<1|1,x,y);
		else change(u<<1,x,mid),change(u<<1|1,mid+1,y);
		pushup(u);
	}
	edge ask(lca u,lca x,lca y)
	{
		pushdown(u);
		lca l=e[u].l,r=e[u].r,mid=(l+r)>>1;
		if(x<=l&&y>=r)return e[u];
		if(y<=mid)return ask(u<<1,x,y);
		else if(x>mid)return ask(u<<1|1,x,y);
		else return merge(ask(u<<1,x,mid),ask(u<<1|1,mid+1,y));
	}
	lca query(lca x,lca y,lca c)
	{
		edge ret=ask(1,x,y);
		return ret.ans[c];
	}
}seg;
int main()
{
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	n=read(),m=read();init();
	if(n<=50000)top=5;else top=2;
	for(lca i=1;i<=n;i++)a[i]=read();
	seg.build(1,1,n);
	for(lca i=1;i<=m;i++)
	{
		lca op=read(),x=read(),y=read();
		lca z;if(op!=2)z=read()%mod;
		if(op==1)seg.add(1,x,y,z);
		else if(op==2)seg.change(1,x,y);
		else printf("%lld\n",seg.query(x,y,z));
	}
}

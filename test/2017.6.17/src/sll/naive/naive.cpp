#include<bits/stdc++.h>
#define lca unsigned long long
#define N 2006
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m;
int w[N];
int f[N];
int st[N],top;
void write(lca x)
{
	if(!x){puts("0");return;}
	while(x)st[++top]=x%10,x/=10;
	while(top)putchar(st[top--]+'0');
	puts("");
}
void work(int u)
{
	memset(f,0,sizeof(f));
	f[0]=1;
	for(int j=1;j<=n;j++)
	{
		if(j==u)continue;
		for(int k=m;k>=w[j];k--)
			f[k]=(f[k]+f[k-w[j]])%10;
	}
	lca tmp=0;
	for(int i=1;i<=m;i++)tmp=tmp*10+f[i];
	write(tmp);
}
int main()
{
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<=n;i++)work(i);
}

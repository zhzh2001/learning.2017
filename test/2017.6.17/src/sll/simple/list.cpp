#include<bits/stdc++.h>
#define lca long long
#define up ((1ll<<31)-1)
#define N 20000020
#define mod 1000000007
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int ans,c;
int n,m;
int f[N];
int inv[N];
int main()
{
	scanf("%d",&n);f[0]=f[1]=1;
	inv[1]=1,c=n;
	ans=c+1;
	for(int i=2;i<=n;i++)
	{
		f[2]=(f[0]+f[1]);
		f[0]=f[1],f[1]=f[2];
		inv[i]=inv[mod%i]*(mod-mod/i);
		c=c*(n-i+1)*inv[i];
		ans=ans+c*f[1];
	}
	printf("%d\n",ans&0xff);
}

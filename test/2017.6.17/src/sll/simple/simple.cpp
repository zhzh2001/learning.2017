#include<bits/stdc++.h>
#define lca long long
#define N 20000020
#define mod 1000000007
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
lca ans,c;
int n,m;
lca f[N];
lca inv[N];
int main()
{
	freopen("simple.in","r",stdin);

	freopen("simple.out","w",stdout);
	scanf("%d",&n);f[0]=f[1]=1;
	inv[1]=1,c=n;
	ans=(c+1)%mod;
	for(int i=2;i<=n;i++)
	{
		f[i]=(f[i-1]+f[i-2])%mod;
		inv[i]=(lca)inv[mod%i]*(mod-mod/i)%mod;
		c=c*(n-i+1)%mod*inv[i]%mod;
		ans=(ans+c*f[i]%mod)%mod;
	}
	printf("%lld\n",ans);
}

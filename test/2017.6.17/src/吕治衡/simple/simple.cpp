#include<bits/stdc++.h>
using namespace std;
int n,inv[100000000],x,y,z,l,r;
long long d,p,ans;
int main(){
	freopen("simple.in","r",stdin);freopen("simple.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;p=1e9+7;
	inv[1]=1;for (int i=2;i<=n;++i)inv[i]=1ll*inv[p%i]*(p-p/i)%p;
	x=0;y=1;l=1;r=n;d=1;
	for (int i=0;i<=n;i++){
		(ans+=d*y)%=p; 
		z=x;x=y;y=(x+z)%p;
		d=(d*r)%p*inv[l]%p;l++;r--;
	}
	cout<<ans<<endl;
} 

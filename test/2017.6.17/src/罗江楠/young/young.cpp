#include <bits/stdc++.h>
#define N 50020
#define zyy 998244353
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int sum, sm[N<<2], tg[N<<2], ls[N<<2], rs[N<<2], rv[N<<2];
void push_down(int x){
	if(ls[x]!=rs[x]){
		if(tg[x]){
			sm[x<<1]=(sm[x<<1]+1ll*(rs[x<<1]-ls[x<<1]+1)*tg[x])%zyy;
			tg[x<<1]=(tg[x<<1]+tg[x])%zyy;
			sm[x<<1|1]=(sm[x<<1|1]+1ll*(rs[x<<1|1]-ls[x<<1|1]+1)*tg[x])%zyy;
			tg[x<<1|1]=(tg[x<<1|1]+tg[x])%zyy;
			tg[x] = 0;
		}
		if(rv[x]){
			sm[x<<1]=(zyy-sm[x<<1])%zyy;
			tg[x<<1]=(zyy-tg[x<<1])%zyy;
			sm[x<<1|1]=(zyy-sm[x<<1|1])%zyy;
			tg[x<<1|1]=(zyy-tg[x<<1|1])%zyy;
			rv[x<<1]^=1;rv[x<<1|1]^=1;
			rv[x]^=1;
		}
	}
	tg[x]=rv[x]=0;
}
void push_up(int x){
	sm[x]=(sm[x<<1]+sm[x<<1|1])%zyy;
}
void build(int x, int l, int r){
	ls[x] = l; rs[x] = r;
	if(l == r){sm[x]=read();return;}
	int mid = l+r>>1;
	build(x<<1, l, mid);
	build(x<<1|1, mid+1, r);
	push_up(x);
}
int ask(int x, int l, int r){
	push_down(x);
	if(ls[x] == l && rs[x] == r) return sm[x];
	int mid = ls[x]+rs[x]>>1;
	if(r <= mid) return ask(x<<1, l, r);
	else if(l > mid) return ask(x<<1|1, l, r);
	else return (ask(x<<1, l, mid)+ask(x<<1|1, mid+1, r))%zyy;
}
void rev(int x, int l, int r){
	if(ls[x] == l && rs[x] == r){
		sm[x]=(zyy-sm[x])%zyy;
		tg[x]=(zyy-tg[x])%zyy;
		rv[x]^=1; return;
	}
	push_down(x);
	int mid = ls[x]+rs[x]>>1;
	if(r <= mid) rev(x<<1, l, r);
	else if(l > mid) rev(x<<1|1, l, r);
	else rev(x<<1, l, mid), rev(x<<1|1, mid+1, r);
	push_up(x);
}
void add(int x, int l, int r, int v){
	if(ls[x] == l && rs[x] == r){
		tg[x]=(tg[x]+v)%zyy;
		sm[x]=(sm[x]+1ll*(r-l+1)*v)%zyy;
		return;
	}
	push_down(x);
	int mid = ls[x]+rs[x]>>1;
	if(r <= mid) add(x<<1, l, r, v);
	else if(l > mid) add(x<<1|1, l, r, v);
	else add(x<<1, l, mid, v), add(x<<1|1, mid+1, r, v);
	push_up(x);
}
int query(int l, int r){return (ask(1, l, r)+zyy)%zyy;}
int query(int x){return (ask(1, x, x)+zyy)%zyy;}

void dfs(int l, int r, int x, int m){
	if(!x){sum = (sum+m)%zyy; return;}
	if(r-l+1 < x) return;
	for(int i = l; i <= r; i++)
		dfs(i+1, r, x-1, 1ll*m*query(i)%zyy);
}
int main(){
	freopen("young.in", "r", stdin);
	freopen("young.out", "w", stdout);
	int n = read(), m = read();build(1, 1, n);
	for(int i = 1; i <= m; i++){
		int op = read();
		if(op == 1){
			int l = read(), r = read(), z = read();
			add(1, l, r, z%zyy);
		}
		if(op == 2){
			int l = read(), r = read();
			rev(1, l, r);
		}
		if(op == 3){
			int l = read(), r = read(), x = read();
			if(x == 1) printf("%d\n", query(l, r));
			else if(x == 2){
				int ans = 0;
				int sss = query(l, r);
				for(int i = l; i <= r; i++){
					int kkk = query(i);
					int fff = sss-kkk;
					if(fff < 0) fff+=zyy;
					ans = (ans+1ll*kkk*fff)%zyy;
				}
				if(ans < 0) ans+=zyy;
				printf("%d\n", ans>>1);
			}
			else{
				sum = 0;
				dfs(l, r, x, 1);
				printf("%d\n", sum);
			}
		}
	}
	return 0;
}
#include <bits/stdc++.h>
#define N 40000020
#define zyy 1000000007
#define ull unsigned long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int g[N], f[N];
int main(){
	freopen("simple.in", "r", stdin);
	freopen("simple.out", "w", stdout);
	int *x = g, *y = f;
	x[0] = 1; y[0] = 1;
	int n = read();
	for(int i = 1; i <= n; i++, swap(x, y))
		for(int j = 1; j <= i; j++)
			y[j] = x[j]+x[j-1];
	int a = 1, b = 1, ans = 1+n, c;
	for(int i = 2; i <= n; i++){
		c = a+b;
		ans = (ans+1ll*c*x[i])%zyy;
		a = b; b = c;
	}
	printf("%d\n", ans);
	return 0;
}
#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int P=1000000007;
int n;
struct na{int a[2][2];}A,I,R;
inline na operator * (const na &a,const na &b){
	na c;memset(c.a,0,sizeof c.a);
	for (int i=0;i<2;i++)
		for (int j=0;j<2;j++)
			for (int k=0;k<2;k++)
				(c.a[i][j]+=(LL)a.a[i][k]*b.a[k][j]%P)%=P;
	return c;
}

int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	n=read()*2+1;
	A.a[0][0]=A.a[0][1]=A.a[1][0]=1;
	I.a[0][0]=I.a[1][1]=1;
	R=I;
	for (int k=n;k;k>>=1,A=A*A)
		if (k&1)
			R=R*A;
	printf("%d\n",R.a[1][0]);
	return 0;
}

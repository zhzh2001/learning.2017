#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=4005;
int n,m,w[N],f[N],g[N];

int main()
{
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	n=read(),m=read();f[0]=1;
	for (int i=1;i<=n;i++) w[i]=read();
	for (int i=1;i<=n;i++)
		for (int j=m;j>=w[i];j--)
			f[j]=(f[j]+f[j-w[i]])%10;
	for (int i=1;i<=n;i++){
		unsigned long long a=0;
		for (int j=0;j<w[i]&&j<=m;j++) g[j]=f[j];
		for (int j=w[i];j<=m;j++) g[j]=(f[j]-g[j-w[i]]+10)%10;
		for (int j=1;j<=m;j++) a=a*10+g[j];
		printf("%llu\n",a);
	}
	return 0;
}

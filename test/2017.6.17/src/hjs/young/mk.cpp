#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<ctime>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}

const int maxm=0x3f3f3f3f;
inline int cal(){
	return (rand()*rand()*rand()%maxm+maxm)%maxm;
}

inline int cal(int x){
	return x?cal()%x+1:cal();
}

const int N=10,M=10,S=10;

int main()
{
	freopen("young.in","w",stdout);
	srand(time(0));
	int n=N,m=M;
	printf("%d %d\n",n,m);
	for (int i=1;i<=n;i++) printf("%d ",cal(S));puts("");
	for (int i=1;i<=m;i++){
		int op=cal(3),l=cal(n),r=cal(n);
		if (l>r) swap(l,r);
		printf("%d %d %d",op,l,r);
		if (op==1) printf(" %d",cal(S));
		if (op==3) printf(" 2");
		puts("");
	}
	return 0;
}

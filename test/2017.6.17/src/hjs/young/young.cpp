#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=50005,S=6,P=998244353;
int n,m,pw[S],_l,_r,_a,C[N][S];
struct na{
	int s[S],ad,rev,len;
	na () {memset(s,0,sizeof s);s[0]=1,ad=rev=len=0;}
	inline void rever(){
		rev^=1;
		for (int i=1;i<S&&i<=len;i+=2) s[i]=(s[i]?P-s[i]:0);
	}
	inline void add(int a){
		(ad+=a)%=P,pw[0]=1;
		for (int i=1;i<S&&i<=len;i++) pw[i]=(LL)pw[i-1]*a%P;
		for (int i=min(S-1,len);i;i--) 
			for (int j=1;j<=i;j++)
				(s[i]+=(LL)pw[j]*s[i-j]%P*C[len-i+j][j]%P)%=P;
	}
}e[N*8],_e;
inline na operator + (const na &a,const na &b){
	na c;
	for (int i=1;i<S;i++)
		for (int j=0;j<=i;j++)
				(c.s[i]+=(LL)a.s[j]*b.s[i-j]%P)%=P;
	c.len=a.len+b.len;
	return c;
}
inline void push_down(int o){
	if (!e[o].len) {e[o].ad=0,e[o].rev=0;return;}
	if (e[o].ad) {
		if (e[o<<1].rev) push_down(o<<1);
		e[o<<1].add(e[o].ad);
		if (e[o<<1|1].rev) push_down(o<<1|1);
		e[o<<1|1].add(e[o].ad);
		e[o].ad=0;
	}
	if (e[o].rev) {
		if (e[o<<1].ad) push_down(o<<1);
		e[o<<1].rever();
		if (e[o<<1|1].ad) push_down(o<<1|1);
		e[o<<1|1].rever();
		e[o].rev=0;
	}
}

void build(int o,int l,int r){
	if (l==r) {e[o].s[1]=read(),e[o].len=1;return;}
	int mid=l+r>>1;
	build(o<<1,l,mid);
	build(o<<1|1,mid+1,r);
	e[o]=e[o<<1]+e[o<<1|1];
}

void update(int o,int l,int r){
	push_down(o);
	if (_l<=l&&r<=_r){
		if (_a>=0) e[o].add(_a);
		else if (_a==-1) e[o].rever();
		else _e=_e+e[o];
		return;
	}
	int mid=l+r>>1;
	if (_l<=mid) update(o<<1,l,mid);
	if (_r>mid) update(o<<1|1,mid+1,r);
	e[o]=e[o<<1]+e[o<<1|1];
}

int main()
{
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	n=read(),m=read();
	C[0][0]=1;
	for (int i=1;i<=n;i++){
		C[i][0]=1;
		for (int j=1;j<S;j++) C[i][j]=(C[i-1][j-1]+C[i-1][j])%P;
	}
	build(1,1,n);
	for (int i=1,op;i<=m;i++){
		op=read(),_l=read(),_r=read();
		if (op==1) _a=read(),update(1,1,n);
		else if (op==2) _a=-1,update(1,1,n);
		else {
			_e=na(),_a=-2,update(1,1,n);
			printf("%d\n",_e.s[read()]);
		}
	}
	return 0;
}

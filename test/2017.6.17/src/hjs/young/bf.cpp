#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=50005,S=6,P=998244353;
int n,m,A[N];

int main()
{
	freopen("young.in","r",stdin);
	freopen("2.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=n;i++) A[i]=read();
	for (int i=1,op,l,r,a;i<=m;i++){
		op=read(),l=read(),r=read();
		if (op==1) {
			a=read();
			for (int j=l;j<=r;j++) (A[j]+=a)%=P;
		}
		else if (op==2) {
			for (int j=l;j<=r;j++) A[j]=(P-A[j])%P;
		}
		else {
			int ans=0;read();
			for (int i=l;i<=r;i++)
				for (int j=i+1;j<=r;j++)
					(ans+=(LL)A[i]*A[j]%P)%=P;
			printf("%d\n",ans);
		}
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 5005
using namespace std;
int n,m,a[N],b[N],f[N],c[N],F[N],g[N];
unsigned long long ans,Ans[N];
inline void add(int &x,int y){
	x+=y; x=x>=10?x-10:x;
}
int main(){
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout); 
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
		scanf("%d",&a[i]),b[i]=a[i];
	sort(b+1,b+n+1);
	F[0]=1;
	for (int i=1;i<=n;i++)
		for (int j=m;j>=a[i];j--)
			add(F[j],F[j-a[i]]);
	g[0]=1; 
	for (int i=1;i<=n;++i){
		if (b[i]==b[i+1]){
			for (int j=m;j>=b[i];j--)
				add(g[j],g[j-b[i]]);
			continue;
		}
		for (int j=0;j<b[i];j++)
			c[j]=F[j];
		for (int j=b[i];j<=m;j++)
			f[j]=g[j-b[i]];
		for (int j=i+1;j<=n;++j){
			int tmp=b[j]+b[i],bj=b[j],k=m;
			for (k=m;k-3>=tmp;k-=4){
				add(f[k],f[k-bj]);
				add(f[k-1],f[k-1-bj]);
				add(f[k-2],f[k-2-bj]);
				add(f[k-3],f[k-3-bj]);
			}
			for (;k>=tmp;k--)
				add(f[k],f[k-bj]);
		}
		for (int j=b[i];j<=m;j++)
			c[j]=(F[j]-f[j]+10)%10;
		ans=0;
		for (int j=1;j<=m;j++)
			ans=ans*10+c[j];
		Ans[b[i]]=ans;
		for (int j=m;j>=b[i];j--)
			add(g[j],g[j-b[i]]);
	}
	for (int i=1;i<=n;i++)
		printf("%llu\n",Ans[a[i]]);
}

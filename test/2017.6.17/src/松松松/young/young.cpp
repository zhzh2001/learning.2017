#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define mo 998244353
#define ll long long
using namespace std;
int n,m,fl,x,y,z,a[50005];
ll f[6];
inline void add(int &x,int y){
	x+=y; x=x>=mo?x-mo:x;
}
int main(){
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout); 
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=m;i++){
		scanf("%d",&fl);
		if (fl==2){
			int j; 
			scanf("%d%d",&x,&y);
			for (j=x;j+3<=y;j+=4){
				a[j]=mo-a[j];
				a[j+1]=mo-a[j+1];
				a[j+2]=mo-a[j+2];
				a[j+3]=mo-a[j+3];
			}
			for (;j<=y;j++)
				a[j]=mo-a[j];
		}
		else if (fl==1){
			int j;
			scanf("%d%d%d",&x,&y,&z);
			for (j=x;j+3<=y;j+=4){
				add(a[j],z);
				add(a[j+1],z);
				add(a[j+2],z);
				add(a[j+3],z);
			}
			for (;j<=y;j++)
				add(a[j],z);
		}
		else{
			scanf("%d%d%d",&x,&y,&z);
			for (int j=1;j<=5;j++) f[j]=0;
			for (int j=x;j<=y;j++){
				f[5]=(f[5]+f[4]*a[j])%mo;
				f[4]=(f[4]+f[3]*a[j])%mo;
				f[3]=(f[3]+f[2]*a[j])%mo;
				f[2]=(f[2]+f[1]*a[j])%mo;
				f[1]=(f[1]+a[j])%mo;
			} 
			printf("%lld\n",f[z]); 
		}
	}
}

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<string.h>
using namespace std;
ifstream fin("simple.in");
ofstream fout("simple.out");
const int mod=1000000007;
unsigned long long n,ans;
unsigned long long f[1000003],jie[1000003];
inline unsigned long long qpow(long long a,long long b){
	if(b==0){
		return 1;
	}
	if(b==1){
		return a;
	}
	long long fanhui=qpow(a,b/2);
	fanhui*=fanhui; 
	fanhui%=mod;
	if(b%2==1){
		fanhui*=a;
		fanhui%=mod;
	}
	return fanhui;
}
inline unsigned long long C(long long a,long long b){
	return jie[a]%mod*qpow(jie[b],mod-2)%mod*qpow(jie[a-b],mod-2)%mod;
}
int main(){
	//freopen("simple.in","r",stdin);
	//freopen("simple.out","w",stdout);
	fin>>n;
	f[0]=f[1]=jie[1]=1;
	for(int i=2;i<=n;i++){
		f[i]=(f[i-1]+f[i-2])%mod;
		jie[i]=(jie[i-1]*i)%mod;
	}
	for(int i=0;i<=n-1;i++){
		ans+=((C(n,i))*f[i])%mod;
	}
	ans=(ans+f[n]+1)%mod;
	fout<<ans<<endl;
	return 0;
}
/*

in:
12

out:
75025

*/

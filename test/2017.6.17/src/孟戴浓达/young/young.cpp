//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<string.h>
using namespace std;
ifstream fin("young.in");
ofstream fout("young.out");
const int mod=998244353;
int n,q;
struct tree{
	int sum,tag,rev;
	int l,r;
};
tree t[50003*4];
int num[50003];
int opt[50003],x[50003],y[50003],z[50003];
bool yesz1=true;
inline void pushdown(int rt){
	if(t[rt].tag!=0){
		t[rt].sum+=((t[rt].r-t[rt].l+1)*t[rt].tag)%mod;
		t[rt].sum%=mod;
		t[rt*2].tag+=t[rt].tag;
		t[rt*2+1].tag+=t[rt].tag;
		t[rt].tag=0;
	}
	if(t[rt].rev!=0){
		t[rt].sum=-t[rt].sum;
		t[rt].sum=(t[rt].sum+mod)%mod;
		t[rt*2].tag^=t[rt].tag;
		t[rt*2+1].tag^=t[rt].tag;
		t[rt].rev=0;
	}
}
inline void pushup(int rt){
	t[rt].sum=(t[rt*2].sum+t[rt*2+1].sum)%mod; 
} 
void build(int rt,int x,int y){
	t[rt].l=x;   t[rt].r=x;  t[rt].tag=0; t[rt].rev=0;
	if(x==y){
		t[rt].sum=num[x];
		return;
	}
	int mid=(x+y)/2;
	build(rt*2,x,mid),build(rt*2+1,mid+1,y);
	pushup(rt);
}
void add(int rt,int x,int y,int val){
	pushdown(rt);
	int l=t[rt].l,r=t[rt].r;
	if(l==x&&r==y){
		t[rt].tag+=val;
		return;
	}
	int mid=(l+r)/2;
	if(y<=mid){
		add(rt*2,x,y,val);
	}else if(x>mid){
		add(rt*2+1,x,y,val);
	}else{
		add(rt*2,x,mid,val);
		add(rt*2+1,mid+1,y,val);
	}
	pushup(rt);
}
int query(int rt,int x,int y){
	pushdown(rt);
	int l=t[rt].l,r=t[rt].r;
	if(l==x&&r==y){
		return t[rt].sum%mod;
	}
	int mid=(l+r)/2;
	if(y<=mid){
		return (query(rt*2,x,y)+mod)%mod;
	}else if(x>mid){
		return (query(rt*2+1,x,y)+mod)%mod;
	}else{
		return (query(rt*2,x,mid)+query(rt*2+1,mid+1,y)+mod)%mod;
	}
}
void rev(int rt,int x,int y){
	pushdown(rt);
	int l=t[rt].l,r=t[rt].r;
	if(l==x&&r==y){
		t[rt].rev^=1;
		return;
	}
	int mid=(l+r)/2;
	if(y<=mid){
		rev(rt*2,x,y);
	}else if(x>mid){
		rev(rt*2+1,x,y);
	}else{
		rev(rt*2,x,mid);
		rev(rt*2+1,mid+1,y);
	}
	pushup(rt);
}
int main(){
	//freopen("young.in","r",stdin);
	//freopen("young.out","w",stdout);
	fin>>n>>q;
	for(int i=1;i<=n;i++){
		fin>>num[i];
	}
	for(int i=1;i<=q;i++){
		fin>>opt[i];
		if(opt[i]==1){
			fin>>x[i]>>y[i]>>z[i];
		}else if(opt[i]==2){
			fin>>x[i]>>y[i];
		}else{
			fin>>x[i]>>y[i]>>z[i];
		}
		if(z[i]!=0&&z[i]!=1){
			yesz1=false;
		}
	}
	if(yesz1){
		for(int i=1;i<=q;i++){
			if(opt[i]==1){
				add(1,x[i],y[i],z[i]);
			}else if(opt[i]==2){
				rev(1,x[i],y[i]);
			}else{
				fout<<query(1,x[i],y[i]);
			}
		}
	}else{
		
	}
	return 0;
}

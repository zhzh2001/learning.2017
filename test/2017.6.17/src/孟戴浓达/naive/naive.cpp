//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<string.h>
using namespace std;
ifstream fin("naive.in");
ofstream fout("naive.out");
unsigned long long mod=1;
int n,m;
long long w[4003];
int f[4003][4003],g[4003][4003];
int main(){
	//freopen("naive.in","r",stdin);
	//freopen("naive.out","w",stdout);
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>w[i];
	}
	f[0][0]=g[n+1][0]=1;
	for(int i=1;i<=n;i++){
		for(int j=0;j<=m;j++){
			f[i][j]=(f[i][j]+f[i-1][j])%10;
			if(j>=w[i]){
				f[i][j]=(f[i][j]+f[i-1][j-w[i]])%10;
			}
		}
	}
	for(int i=n;i>=1;i--){
		for(int j=0;j<=m;j++){
			g[i][j]=(g[i][j]+g[i+1][j])%10;
			if(j>=w[i]){
				g[i][j]=(g[i][j]+g[i+1][j-w[i]])%10;
			}
		}
	}
	int now;
	unsigned long long ans;
	for(int i=1;i<=n;i++){
		ans=0;
		for(int j=1;j<=m;j++){
			now=0;
			for(int k=0;k<=j;k++){
				now=(now+(f[i-1][k])*(g[i+1][j-k]))%10;
			}
			ans=(ans*10+now);
		}
		fout<<ans<<endl;
		/*
		ans=0;
		memset(xx,0,sizeof(xx));
		sum[0]=g[i+1][0];
		for(int j=1;j<=m;j++){
			sum[j]=(g[i+1][j]+sum[j-1])%10;
		}
		now=0;
		for(int j=1;j<=m;j++){
			xx[j]=(xx[j]+(f[i-1][j])*(sum[m-j]))%10;
		}
		for(int j=1;j<=m;j++){
			ans=ans*10+xx[j];
		}
		cout<<ans<<endl;
		*/
	}
	return 0;
}
/*

in:
3 2
1 1 2

out:
11
11
21

*/

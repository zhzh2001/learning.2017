#include <iostream>
#include <fstream>
#include <cassert>
#include <cstdio>
using namespace std;

ifstream fin("naive.in");
ofstream fout("naive.out");

const int N = 4505;
int n, m;
unsigned long long w[N];
unsigned long long f[12][N], ans[N];

void work(int l, int r, int cap, int dep = 1) {
	if (l == r) {
		ans[l] = ans[l] * 10LL + f[dep - 1][cap] % 10LL;
		return;
	}
	int mid = (l + r) >> 1;
	for (int i = 0; i <= cap + 5; i++) f[dep][i] = f[dep - 1][i];
	for (int i = mid + 1; i <= r; i++) {
		for (int j = cap; j >= w[i]; j--) {
			f[dep][j] += f[dep][j - w[i]];
		}
	}
	work(l, mid, cap, dep + 1);
	for (int i = 0; i <= cap + 5; i++) f[dep][i] = f[dep - 1][i];
	for (int i = l; i <= mid; i++) {
		for (int j = cap; j >= w[i]; j--) {
			f[dep][j] += f[dep][j - w[i]];
		}
	}
	work(mid + 1, r, cap, dep + 1);
}

int main() {
    fin >> n >> m;
    for (int i = 1; i <= n; i++) fin >> w[i];
	f[0][0] = 1;
	for (int i = 1; i <= m; i++) work(1, n, i);
	for (int i = 1; i <= n; i++) fout << ans[i] << endl;
    return 0;
}
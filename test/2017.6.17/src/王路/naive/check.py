# -*- coding: utf-8 -*- 

import sys
import io
import random
import os
# import win32api
# import win32process
import time

def compare(a, b):
	if len(a) != len(b):
		if len(a) < len(b): return -1
		else: return 1
	if a == b:  return 0
	elif a < b: return -1
	else: return 1

class KeyWrapper(object):
	def __init__(self, cmp, val):
		self.val = val
		self.cmp = cmp
		
	def __lt__(self, other):
		return self.cmp(self.val, other.val) == -1
		
def com_to_key(func):
	def key(x):
		return KeyWrapper(func, x)
	return key
	
# Compile
if os.system("g++ src.cpp -o src -O2 >>tmpfile1 2>&1") == 1:
	print("$ Result: Compile Error\n$ Time: N/A")
	print("$ Compile Error Detail:")
	os.system("@type tmpfile1")
	os.system("del tmpfile1 >nul")
	exit()
os.system("del tmpfile1 >nul")
# Scan data
path = os.getcwd()
path += "\sample"
if not os.path.isdir(path):
	print("$ Result: Testdata Error\n$ Time: N/A")
	print("$ Detail: Cannot find testdata")
	exit()
files = os.listdir(path);
list_in = list()
list_out = list()
for file in files:
	if not os.path.isdir(file):
		if (os.path.splitext(file)[1] == ".in"):
			list_in.append(file)
		elif (os.path.splitext(file)[1] == ".out"):
			list_out.append(file)
# print(int(bool(False)))
list_in.sort(key = com_to_key(compare))
list_out.sort()
# Init
execute_file = "src.exe"
# Judge
tot_score = 0
cur_score = 0

for inputfile in list_in:
	tot_score = tot_score + 1
	start_time = time.time()
	outputfile = path + "\\" + os.path.splitext(inputfile)[0] + ".res"
	stdoutput = path + "\\" + os.path.splitext(inputfile)[0] + ".out"
	command = execute_file + "<\"" + path + "\\" + inputfile + "\">\"" + outputfile + "\""
	# print(command)
	errorcode = os.system(command)
# Runtime Error
	inputfile = os.path.splitext(inputfile)[0]
	if errorcode != 0:
		end_time = time.time()
		ss = '$ ' + inputfile + ': Runtime Error ' + str(errorcode) + '\n$ Time: N/A'  + '\n'
		print(ss)
		continue
# Wrong Answer
	end_time = time.time()
	f1 = open(stdoutput, 'r')
	f2 = open(outputfile, 'r')
	# print stdoutput
	# print outputfile
	cnt = 1
	tot = 0
	ans = True
	while True:
		a1 = f1.readline()
		a2 = f2.readline()
# Output Limit Exceed
		if ((not a1) and a2) or ((not a2) and a1):
			ss = '$ ' + inputfile +': Too many or too few lines\n$ Time: ' + str(round(end_time - start_time, 4))  + '\n'
			print(ss)
			ans = False
			break
		if not a1:
			break
		if (a1 != a2):
			tot = tot + 1
			ss = '$ ' + inputfile +': Wrong Answer\n$ Time: ' + str(round(end_time - start_time, 4))
			print(ss)
			print("^ Line " + str(cnt) + ': \n  Std Output: ' + str(a1) + "  Your Output: " + str(a2) +"\n")
			ans = False
			break
		cnt = cnt + 1
# Accepted
	if ans:
		ss = '$ ' + inputfile +': Accepted\n$ Time: ' + str(round(end_time - start_time, 4)) + '\n'
		cur_score = cur_score + 1 
		print(ss)
		
# End
f1.close()
f2.close()
os.system("del .\\sample\\*.res >nul")
os.system("del .\\src.exe >nul")
score = int(round(float(cur_score * 10000) / float(tot_score * 100), 1))
status = '$ Status: ' + str(score)
print(status)
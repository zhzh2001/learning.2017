#include <iostream>
#include <fstream>
#include <cassert>
#include <cstdio>
#include <gmpxx.h>
using namespace std;

ifstream fin("simple.in");
ofstream fout("simple.out");

const int Mod = 1e9 + 7, N = 3e8;

long long F1, F2, C, n;

int main() {
	fin >> n;
	long long res = 0;
	F1 = 1, F2 = 0;
	C = 1;
	res = 1;
	for (long long i = 1; i <= n; i++) {
		static long long cnt = 0;
		++cnt;
		C = (C * (n - cnt + 1) / cnt);
		long long tmpf = (F1 + F2) % Mod;
		F2 = F1, F1 = tmpf;
		(res += F1 * C % Mod) %= Mod;
	}
	fout << res << endl;
	return 0;
}
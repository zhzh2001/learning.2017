#include <iostream>
#include <cstdio>
#include <gmpxx.h>
using namespace std;

const int Mod = 1e9 + 7;

int main() {
	int n;
	cin >> n;
	mpz_class C = 1;
	cout << C << ' ';
	for (int i = 1; i <= n; i++) {
		C = (C * (mpz_class)(n - i + 1) / (mpz_class) i);
		cout << C % Mod << ' ';
	}
	cout << endl;
	return 0;
}

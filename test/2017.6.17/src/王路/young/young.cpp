#include <iostream>
#include <fstream>
#include <cassert>
#include <cstdio>
using namespace std;

ifstream fin("young.in");
ofstream fout("young.out");

const int Mod = 998244353;
const int N = 50005;

int n, Q;
long long a[N];

struct Seg {
	int l, r, sum, lazyadd, lazyrev;
} tree[N << 2];

void build(int x, int l, int r) {
	tree[x].l = l, tree[x].r = r;
	tree[x].lazyadd = tree[x].lazyrev = 0;
	if (l == r) {
		fin >> tree[x].sum;
		return;
	}
	int mid = (l + r) >> 1;
	build(x << 1, l, mid);
	build(x << 1 | 1, mid + 1, r);
}

void pushup(int x) {
	tree[x].sum = tree[x << 1].sum + tree[x << 1 | 1].sum;
}

void pushdown(int x) {
	if (tree[x].lazyadd) {
		tree[x << 1].lazyadd += tree[x].lazyadd;
		tree[x << 1 | 1].lazyadd += tree[x].lazyadd;
		// tree[x << 1].sum = tree[x << 1 | 1].sum += x;
		tree[x << 1].sum += (tree[x << 1].r - tree[x << 1].l + 1) * tree[x].lazyadd;
		tree[x << 1 | 1].sum += (tree[x << 1 | 1].r - tree[x << 1 | 1].l + 1) * tree[x].lazyadd;
		tree[x].lazyadd = 0;
	}
	if (tree[x].lazyrev) {
		tree[x].lazyrev = 0;
		tree[x << 1].lazyrev ^= 1, tree[x << 1].lazyrev ^= 1;
		tree[x << 1].sum *= -1;
		tree[x << 1 | 1].sum *= -1;
	}
}

void update_add(int x, int l, int r, int z) {
	if (l <= tree[x].l && r >= tree[x].r) {
		tree[x].lazyadd += z;
		tree[x].sum += z * (tree[x].r - tree[x].l + 1);
		return;
	}
	if (l > tree[x].r || r < tree[x].l) return;
	pushdown(x);
	update_add(x << 1, l, r, z);
	update_add(x << 1 | 1, l, r, z);
	pushup(x);
}

void update_rev(int x, int l, int r) {
	if (l <= tree[x].l && r >= tree[x].r) {
		tree[x].lazyrev ^= 1;
		tree[x].sum = -tree[x].sum;
		return;
	}
	if (l > tree[x].r || r < tree[x].l) return;
	pushdown(x);
	update_rev(x << 1, l, r);
	update_rev(x << 1 | 1, l, r);
	pushup(x);
}

int query(int x, int l, int r) {
	if (l <= tree[x].l && r >= tree[x].r) return tree[x].sum;
	if (l > tree[x].r || r < tree[x].l) return 0;
	return (query(x << 1, l, r) + query(x << 1 | 1, l, r)) % Mod;
}

long long res;

void dfs(int z, int l, int r, long long now = 1) {
	if (!z) {
		(res += now) %= Mod;
		return;
	}
	for (int i = l; i <= r; i++) {
		dfs(z - 1, i + 1, r, (long long)now * a[i] % Mod);
	}
	if (l == r) return;
}

__attribute__((optimize("Ofast"))) int main() {
	fin >> n >> Q;
	if (n <= 2000) {
		for (int i = 1; i <= n; i++) fin >> a[i];
		for (int i = 1; i <= Q; i++) {
			long long opt, x, y, z;
			fin >> opt;
			switch(opt) {
				case 1:
					fin >> x >> y >> z;
					for (int i = x; i <= y; i++) (a[i] += z) %= Mod;
					break;
				case 2:
					fin >> x >> y;
					for (int i = x; i <= y; i++) a[i] = -a[i];
					break;
				case 3:
					fin >> x >> y >> z;
					res = 0;
					dfs(z, x, y, 1);
					fout << (res + Mod) % Mod << endl;
					break;
			}
		}
		return 0;
	}
	build(1, 1, n);
	for (int i = 1; i <= Q; i++) {
		int opt, x, y, z;
		fin >> opt;
		switch(opt) {
			case 1:
				fin >> x >> y >> z;
				update_add(1, x, y, z);
				break;
			case 2:
				fin >> x >> y;
				update_rev(1, x, y);
				break;
			case 3:
				fin >> x >> y >> z;
				fout << (query(1, x, y) + Mod) % Mod << endl;
				break;
		}
	}
	return 0;
}
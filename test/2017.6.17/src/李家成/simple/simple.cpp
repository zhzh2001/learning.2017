#include<algorithm>
#include<cstdio>
const int orz=1e9+7,N=5e7+10;
long long inv[N],f[N],fx[N],ans;
int n,i;
int main()
{
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
	scanf("%d",&n);
	if(n>5e7)return 0;
	for(inv[1]=1,i=2;i<=n;i++)inv[i]=(orz-orz/i)*inv[orz%i]%orz;
	for(f[0]=f[1]=fx[0]=1,fx[1]=n,i=2;i<=n;i++)
		f[i]=(f[i-1]+f[i-2])%orz,fx[i]=fx[i-1]*(n-i+1)%orz*inv[i]%orz,ans=(ans+f[i]*fx[i])%orz;
	for(i=0;i<=std::min(1,n);i++)ans=(ans+f[i]*fx[i])%orz;
	printf("%I64d",ans);
}

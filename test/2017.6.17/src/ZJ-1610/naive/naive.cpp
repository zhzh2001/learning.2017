#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int Mod=10;
const int N=4005;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int f[N],g[N],w[N],n,m;
int main(){
	freopen("naive.in","r",stdin);
	freopen("naive.out","w",stdout);
	n=read(),m=read();
	Rep(i,1,n) w[i]=read();
	f[0]=1;
	Rep(i,1,n) Dep(j,m,w[i]) Add(f[j],f[j-w[i]]);
//	printf("%d\n",f[m]);
	Rep(i,1,n){
		Rep(j,0,w[i]-1) g[j]=f[j];
		Rep(j,w[i],m){
			g[j]=0;Add(g[j],(f[j]-g[j-w[i]]+Mod)%Mod);
		}
//		Rep(j,1,m) printf("%d",g[j]);puts("");
		unsigned long long Ans=0,base=1;
		Dep(j,m,1){
			Ans+=base*g[j];base=base*10;
		}
		printf("%llu\n",Ans);
	}
}

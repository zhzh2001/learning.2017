#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=30000005;
const int Mod=1e9+7;

bool ST;
int n,Fac[N],Inv[N],Fav[N];
bool ED;
inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

inline int C(int x,int y){
	if (x<y) return 0;
	if (x==y) return 1;
	if (y==0) return 1;
	return 1ll*Fac[x]*Fav[y]%Mod*Fav[x-y]%Mod;
}
int Ans;
int main(){
	freopen("simple.in","r",stdin);
	freopen("simple.out","w",stdout);
//	printf("%lf\n",1.0*(&ED-&ST)/1024/1024);
	n=read();
	Fac[1]=Inv[1]=Fav[1]=1;
	Rep(i,2,n){
		Fac[i]=1ll*Fac[i-1]*i%Mod;
		Inv[i]=1ll*-(Mod/i)*Inv[Mod%i]%Mod;
		Add(Inv[i],Mod);
		Fav[i]=1ll*Fav[i-1]*Inv[i]%Mod;
	}
	Inv[0]=1,Inv[1]=1;
	Rep(i,2,n){
		Inv[i]=0;
		Add(Inv[i],Inv[i-1]);
		Add(Inv[i],Inv[i-2]);
		Add(Ans,1ll*Inv[i]*C(n,i)%Mod);
//		printf("C(%d %d)=%d\n",n,i,C(n,i));
	}
	Add(Ans,C(n,1));
	Add(Ans,1);
	printf("%d\n",Ans);
}

#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("young.in");
ofstream fout("young.out");
const int N=50005,M=50005,Z=6,MOD=998244353;
int a[N],f[N][Z];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<=m;i++)
	{
		int opt,x,y,z;
		fin>>opt>>x>>y;
		switch(opt)
		{
			case 1:
				fin>>z;
				for(int j=x;j<=y;j++)
					a[j]=(a[j]+z)%MOD;
				break;
			case 2:
				for(int j=x;j<=y;j++)
					a[j]=MOD-a[j];
				break;
			case 3:
				fin>>z;
				fill(f[x-1],f[x-1]+z+1,0);
				f[x-1][0]=1;
				for(int i=x;i<=y;i++)
					for(int j=0;j<=z;j++)
					{
						f[i][j]=f[i-1][j];
						if(j)
							f[i][j]=(f[i][j]+(long long)f[i-1][j-1]*a[i])%MOD;
					}
				fout<<f[y][z]<<endl;
				break;
		}
	}
	return 0;
}
#include<cstdio>
#include<string>
#include<windows.h>

struct Istream
{
	private:
	HANDLE hFile;
	char *base,*p;
	bool good;
	
	void fetch()
	{
		if(hFile==INVALID_HANDLE_VALUE)
			good=false;
		else
		{
			HANDLE hFileMappingObject=CreateFileMapping(hFile,NULL,PAGE_READONLY,0,0,NULL);
			if(!hFileMappingObject)
				good=false;
			else
				base=p=(char *)MapViewOfFile(hFileMappingObject,FILE_MAP_READ,0,0,0);
		}
	}
	
	char nextchar()
	{
		/* if(!*p)
		{
			good=false;
			return 0;
		} */
		return *p++;
	}
	
	template<typename Int>
	void readint(Int& x)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		x=0;
		Int sign=1;
		if(c=='-')
			sign=-1,c=nextchar();
		for(;isdigit(c);c=nextchar())
			x=x*10+c-'0';
		x*=sign;
	}
	
	public:
	Istream():hFile(NULL),p(NULL),good(false){}
	
	explicit Istream(const std::string& filename):hFile(CreateFile(filename.c_str(),GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL)),p(NULL),good(false)
	{
		fetch();
	}
	
	bool close()
	{
		return UnmapViewOfFile(base)&&CloseHandle(hFile);
	}
	
	~Istream()
	{
		close();
	}
	
	operator bool()const
	{
		return good;
	}
	
	bool operator!()const
	{
		return !good;
	}
	
	bool open(const std::string& filename)
	{
		hFile=CreateFile(filename.c_str(),GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		fetch();
		return good;
	}
	
	Istream& operator>>(short& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned short& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(int& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned int& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(long long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned long long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(char& c)
	{
		for(c=nextchar();isspace(c);c=nextchar());
		return *this;
	}
	
	Istream& operator>>(std::string& s)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		s.clear();
		for(;good&&!isspace(c);c=nextchar())
			s+=c;
		return *this;
	}
	
	Istream& operator>>(double& value)
	{
		std::string s;
		*this>>s;
		value=atof(s.c_str());
		return *this;
	}
	
	friend Istream& getline(Istream& is,std::string& s,char delim='\n');
};

Istream& getline(Istream& is,std::string& s,char delim)
{
	char c;
	s.clear();
	for(c=is.nextchar();is.good&&c!=delim;c=is.nextchar())
		s+=c;
	return is;
}

struct Ostream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p,dig[25];
	
	public:
	Ostream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL){}
	
	explicit Ostream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"w")),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	explicit Ostream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	void flush()
	{
		fwrite(buf,1,p-buf,stream);
		p=buf;
	}
	
	private:
	void writechar(char c)
	{
		*p++=c;
		if(p==buf+bufsz)
			flush();
	}
	
	template<typename Int>
	void writeint(Int x)
	{
		if(x<0)
			writechar('-'),x=-x;
		int len=0;
		do
			dig[++len]=x%10;
		while(x/=10);
		for(;len;len--)
			writechar(dig[len]+'0');
	}
	
	public:
	~Ostream()
	{
		flush();
		close();
		delete [] buf;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"w");
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	Ostream& operator<<(short value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned short value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(int value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned int value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(long long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned long long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(char c)
	{
		writechar(c);
		return *this;
	}
	
	Ostream& operator<<(const std::string& s)
	{
		for(size_t i=0;i<s.length();i++)
			writechar(s[i]);
		return *this;
	}
	
	Ostream& operator<<(Ostream& (*func)(Ostream&))
	{
		return func(*this);
	}
	
	friend Ostream& endl(Ostream& os);
};

Ostream& endl(Ostream& os)
{
	os.writechar('\n');
	return os;
}

Istream fin("young.in");
Ostream fout("young.out");
const int N=50005,M=50005,Z=5,MOD=998244353;
int a[N],c[N][Z+1];
struct ans_t
{
	int f[Z+1],sz;
	ans_t operator+(const ans_t& rhs)const
	{
		ans_t ret;
		ret.sz=sz+rhs.sz;
		ret.f[0]=1;
		for(int i=1;i<=Z;i++)
		{
			ret.f[i]=(f[i]+rhs.f[i])%MOD;
			for(int j=1;j<i;j++)
				ret.f[i]=(ret.f[i]+(long long)f[j]*rhs.f[i-j])%MOD;
		}
		return ret;
	}
	ans_t& operator+=(const int x)
	{
		for(int i=Z;i;i--)
		{
			int tmp=x;
			for(int j=1;j<=i;j++)
			{
				f[i]=(f[i]+(long long)f[i-j]*tmp%MOD*c[sz-i+j][j])%MOD;
				tmp=(long long)tmp*x%MOD;
			}
		}
		return *this;
	}
	void flip()
	{
		for(int i=1;i<=Z;i+=2)
			f[i]=MOD-f[i];
	}
};
struct node
{
	ans_t f;
	int lazy;
	bool flip;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(l==r)
		return;
	if(tree[id].flip)
	{
		tree[id*2].flip^=1;
		tree[id*2+1].flip^=1;
		tree[id*2].lazy=MOD-tree[id*2].lazy;
		tree[id*2+1].lazy=MOD-tree[id*2+1].lazy;
		tree[id*2].f.flip();
		tree[id*2+1].f.flip();
		tree[id].flip=false;
	}
	if(tree[id].lazy)
	{
		tree[id*2].lazy=(tree[id*2].lazy+tree[id].lazy)%MOD;
		tree[id*2+1].lazy=(tree[id*2+1].lazy+tree[id].lazy)%MOD;
		tree[id*2].f+=tree[id].lazy;
		tree[id*2+1].f+=tree[id].lazy;
		tree[id].lazy=0;
	}
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		tree[id].f.f[0]=1;
		tree[id].f.f[1]=a[l];
		tree[id].f.sz=1;
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id].f=tree[id*2].f+tree[id*2+1].f;
	}
}
int L,R,val;
void add(int id,int l,int r)
{
	pushdown(id,l,r);
	if(L<=l&&R>=r)
	{
		tree[id].f+=val;
		tree[id].lazy=(tree[id].lazy+val)%MOD;
	}
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			add(id*2,l,mid);
		if(R>mid)
			add(id*2+1,mid+1,r);
		tree[id].f=tree[id*2].f+tree[id*2+1].f;
	}
}
void flip(int id,int l,int r)
{
	pushdown(id,l,r);
	if(L<=l&&R>=r)
	{
		tree[id].f.flip();
		tree[id].flip^=1;
		tree[id].lazy=MOD-tree[id].lazy;
	}
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			flip(id*2,l,mid);
		if(R>mid)
			flip(id*2+1,mid+1,r);
		tree[id].f=tree[id*2].f+tree[id*2+1].f;
	}
}
ans_t query(int id,int l,int r)
{
	pushdown(id,l,r);
	if(L<=l&&R>=r)
		return tree[id].f;
	int mid=(l+r)/2;
	if(R<=mid)
		return query(id*2,l,mid);
	if(L>mid)
		return query(id*2+1,mid+1,r);
	return query(id*2,l,mid)+query(id*2+1,mid+1,r);
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=0;i<=n;i++)
	{
		c[i][0]=1;
		for(int j=1;j<=i&&j<=Z;j++)
			c[i][j]=(c[i-1][j]+c[i-1][j-1])%MOD;
	}
	build(1,1,n);
	while(m--)
	{
		int opt;
		fin>>opt>>L>>R;
		switch(opt)
		{
			case 1:
				fin>>val;
				add(1,1,n);
				break;
			case 2:
				flip(1,1,n);
				break;
			case 3:
				fin>>val;
				fout<<query(1,1,n).f[val]<<endl;
				break;
		}
	}
	return 0;
}
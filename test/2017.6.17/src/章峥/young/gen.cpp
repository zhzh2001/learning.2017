#include<bits/stdc++.h>
using namespace std;
ofstream fout("young.in");
const int n=20000,q=20000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<q<<endl;
	uniform_int_distribution<> dv(0,1e9);
	for(int i=1;i<=n;i++)
		fout<<dv(gen)<<' ';
	fout<<endl;
	for(int i=1;i<=q;i++)
	{
		uniform_int_distribution<> dopt(1,3),dn(1,n);
		int opt=dopt(gen),l=1,r=n;
		if(l>r)
			swap(l,r);
		fout<<opt<<' '<<l<<' '<<r;
		if(opt==1)
			fout<<' '<<dv(gen);
		if(opt==3)
			fout<<" 1";
		fout<<endl;
	}
	return 0;
}
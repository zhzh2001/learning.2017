#include<bits/stdc++.h>
using namespace std;
ofstream fout("naive.in");
const int n=4000,m=4000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	uniform_int_distribution<> dc(1,m/2);
	for(int i=1;i<=n;i++)
		fout<<dc(gen)<<' ';
	fout<<endl;
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("simple.in");
ofstream fout("simple.out");
const int N=1e8+5,MOD=1e9+7;
int qpow(int a,int b)
{
	int ans=1;
	do
	{
		if(b&1)
			ans=(long long)ans*a%MOD;
		a=(long long)a*a%MOD;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n;
	fin>>n;
	if(n<2)
		fout<<n+1<<endl;
	else
	{
		int sum=n+1,c=n,f0=1,f1=1;
		for(int i=2;i<=n;i++)
		{
			int f2=(f0+f1)%MOD,inv=qpow(i,MOD-2);
			c=(long long)c*(n-i+1)%MOD*inv%MOD;
			sum=(sum+(long long)f2*c)%MOD;
			f0=f1;f1=f2;
		}
		fout<<sum<<endl;
	}
	return 0;
}
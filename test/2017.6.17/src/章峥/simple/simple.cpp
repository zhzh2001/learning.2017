#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("simple.in");
ofstream fout("simple.out");
const int MOD=1e9+7;
struct matrix
{
	int mat[2][2];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int k=0;k<2;k++)
			for(int i=0;i<2;i++)
				for(int j=0;j<2;j++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix I()
{
	matrix ans;
	for(int i=0;i<2;i++)
		ans.mat[i][i]=1;
	return ans;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n;
	fin>>n;
	matrix trans;
	trans.mat[0][0]=trans.mat[0][1]=trans.mat[1][0]=1;
	fout<<qpow(trans,n*2).mat[0][0]<<endl;
	return 0;
}
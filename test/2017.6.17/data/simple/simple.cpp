#include<cstdio>
#include<cstdlib>
#include<cstring>
#define ll long long
#define mo 1000000007 
using namespace std;
int n;
struct mat{
	int a[2][2];
	friend mat operator *(mat x,mat y){
		mat z;
		memset(z.a,0,sizeof(z.a));
		for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				for (int k=0;k<2;k++)
					z.a[i][j]=(z.a[i][j]+(ll)x.a[i][k]*y.a[k][j])%mo;
		return z;
	}
}a;
mat power(mat x,ll y){
	mat z=x;
	for (y--;y;y/=2,x=x*x)
		if (y&1) z=z*x;
	return z;
}
int main(){
	freopen("simple16.in","r",stdin);
	freopen("simple16.out","w",stdout);
	scanf("%d",&n);
	a.a[0][0]=2;
	a.a[0][1]=a.a[1][0]=a.a[1][1]=1;
	printf("%d\n",power(a,n).a[0][0]);
}

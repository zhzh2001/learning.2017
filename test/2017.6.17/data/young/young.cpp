#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#define N 50005
#define mo 998244353
#define ll long long
using namespace std;
int c[N][6],a[N],n,m,x,y,z,fl,rev[N*5],add[N*5],sz[N*5];
struct node{
	int a[6],sz,l,r;
	friend node operator + (node a,node b){
		node c;
		c.l=a.l; c.r=b.r; c.sz=a.sz+b.sz;
		for (int i=0;i<=5;i++){
			c.a[i]=0;
			for (int j=0;j<=i;j++)
				c.a[i]=(c.a[i]+(ll)a.a[j]*b.a[i-j]%mo)%mo;
		}
		return c;
	}
	void operator +=(int x){
		int tmp=x;
		for (int i=5;~i;i--,tmp=x)
			for (int j=1;j<=i;j++,tmp=(ll)tmp*x%mo)
				a[i]=(a[i]+(ll)a[i-j]*tmp%mo*c[sz-i+j][j]%mo)%mo;
	}
	void rever(){
		for (int i=1;i<=5;i+=2) a[i]=(mo-a[i])%mo;
	}
}t[N*5];
void pushdown(int k){
	if (t[k].l==t[k].r) return;
	if (rev[k]){
		rev[k]=0;
		rev[k*2]^=1; rev[k*2+1]^=1;
		add[k*2]=(mo-add[k*2])%mo;
		add[k*2+1]=(mo-add[k*2+1])%mo;
		t[k*2].rever();
		t[k*2+1].rever();
	}
	if (add[k]){
		add[k*2]=(add[k*2]+add[k])%mo;
		add[k*2+1]=(add[k*2+1]+add[k])%mo;
		t[k*2]+=add[k];
		t[k*2+1]+=add[k];
		add[k]=0;
	}
}
void build(int k,int l,int r){
	t[k].l=l,t[k].r=r;
	if (l==r){
		t[k].a[0]=1;
		t[k].a[1]=a[l];
		t[k].sz=1;
		return;
	}
	int mid=(l+r)/2;
	build(k*2,l,mid);
	build(k*2+1,mid+1,r);
	t[k]=t[k*2]+t[k*2+1];
}
void rever(int k,int x,int y){
	int l=t[k].l,r=t[k].r,mid=(l+r)/2;
	pushdown(k);
	if (l==x&&r==y){
		rev[k]^=1;
		t[k].rever();
		return;
	}
	if (y<=mid) rever(k*2,x,y);
	else if (x>mid) rever(k*2+1,x,y);
	else rever(k*2,x,mid),rever(k*2+1,mid+1,y);
	t[k]=t[k*2]+t[k*2+1];
}
void jia(int k,int x,int y,int v){
	int l=t[k].l,r=t[k].r,mid=(l+r)/2;
	pushdown(k);
	if (l==x&&r==y){
		add[k]=(add[k]+v)%mo;
		t[k]+=v;
		return;
	}
	if (y<=mid) jia(k*2,x,y,v);
	else if (x>mid) jia(k*2+1,x,y,v);
	else jia(k*2,x,mid,v),jia(k*2+1,mid+1,y,v);
	t[k]=t[k*2]+t[k*2+1];
}
node ask(int k,int x,int y){
	int l=t[k].l,r=t[k].r,mid=(l+r)/2;
	pushdown(k);
	if (l==x&&r==y) return t[k];
	if (y<=mid) return ask(k*2,x,y);
	if (x>mid) return ask(k*2+1,x,y);
	return ask(k*2,x,mid)+ask(k*2+1,mid+1,y);
}
int main(){
	freopen("young20.in","r",stdin);
	freopen("young20.out","w",stdout); 
	scanf("%d%d",&n,&m);
	for (int i=0;i<=n;i++){
		c[i][0]=1;
		for (int j=1;j<=i&&j<=5;j++)
			c[i][j]=(c[i-1][j-1]+c[i-1][j])%mo;
	}
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) a[i]=(a[i]%mo+mo)%mo;
	build(1,1,n);
	for (int i=1;i<=m;i++){
		scanf("%d",&fl);
		if (fl==2){
			scanf("%d%d",&x,&y);
			rever(1,x,y);
		}
		else if (fl==1){
			scanf("%d%d%d",&x,&y,&z);
			z=(z%mo+mo)%mo;
			jia(1,x,y,z);
		}
		else{
			scanf("%d%d%d",&x,&y,&z);
			printf("%d\n",ask(1,x,y).a[z]);
		}
	}
}

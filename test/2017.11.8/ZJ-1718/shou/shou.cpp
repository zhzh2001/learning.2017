#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("shou.in");
ofstream fout("shou.out");
const int N=100005;
int a[N],b[N];
int main()
{
	int n,x;
	fin>>n>>x;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		b[i]=a[i];
	}
	reverse(b+1,b+n+1);
	long long ans1=0;
	for(int i=1;i<n;i++)
	{
		int eat=a[i]+a[i+1]-x;
		if(eat>0)
		{
			ans1+=eat;
			a[i+1]-=min(eat,a[i+1]);
		}
	}
	long long ans2=0;
	for(int i=1;i<n;i++)
	{
		int eat=b[i]+b[i+1]-x;
		if(eat>0)
		{
			ans2+=eat;
			b[i+1]-=min(eat,b[i+1]);
		}
	}
	fout<<min(ans1,ans2)<<endl;
	return 0;
}

#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
using namespace std;
ofstream fout("xie.in");
const int n=1e3;
long long a[n+5];
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<long long> d(1,2e6);
		a[i]=d(gen);
	}
	uniform_int_distribution<> d(0,1);
	if(d(gen))
		sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		fout<<a[i]<<endl;
	return 0;
}

#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;

int R(int l,int r){
	return rand()%(r-l+1)+l;
}

int n,m,k;

int main(){
	srand(time(0));
	for (;;){
		n=R(1,2000),m=R(1,2000);
		//k=3;
		k=R(2,2000);
		if ((n+m-1)%(k-1)==0&&(n+m-1)!=(k-1)) break;
	}
	printf("%d %d %d\n",n,m,k);
}

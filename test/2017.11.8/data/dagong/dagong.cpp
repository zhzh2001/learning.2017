#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;
 
int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=4000+19;
const int p=1e9+7;
 
int f[N][2*N],g[N][2*N];
int n,m,k,num,ans;
 
void upd(int &x,int y){
	x=(x+y)%p;
}
 
int main(){
	freopen("dagong10.in","r",stdin);
	freopen("dagong10.out","w",stdout);
	n=IN(),m=IN(),k=IN();
	f[0][0]=1;
	f[0][1]=p-1;
	For(i,0,N-1){
		For(j,0,N){
			if (j){
				upd(f[i][j],f[i][j-1]);
				upd(g[i][j],g[i][j-1]);
			}
			upd(f[i+1][j],f[i][j]);
			upd(f[i+1][j+k],p-f[i][j]);
			upd(g[i][j+1],f[i][j]);
			upd(g[i][j+k],p-f[i][j]);
		}
	}
	num=(n+m-1)/(k-1)-1;
	For(i,0,num+1) For(j,0,num+1)
		if (i*(k-1)<=n&&i+j<=num){
			upd(ans,g[num-i-j][n-i*(k-1)]);
		}
	printf("%d\n",ans);
}

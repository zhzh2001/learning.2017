#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;

ll R(){
	return (0ll+rand())<<30|rand();
}
ll R(ll l,ll r){
	return R()%(r-l+1)+l;
}

int n,Q;
ll pre,top;

int main(){
	srand(time(0));
	n=R(80000,100000);
	Q=R(80000,100000);
	top=1e18;
	printf("%d %d\n",n,Q);
	pre=n;
	For(i,1,Q+1){
		if (rand()&1){
			printf("%lld\n",R(max(pre-10,1ll),max(pre-1,1ll)));
		} else{
			printf("%lld\n",pre=R(min(pre+1,top),min(pre+ll(1e13),top)));
		}
	}
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;
 
int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=1e5+19;
 
int A[N];
int n,x,tmp;
ll res;
 
int main(){
	freopen("shou10.in","r",stdin);
	freopen("shou10.out","w",stdout);
	n=IN(),x=IN();
	For(i,1,n+1) A[i]=IN();
	For(i,2,n+1){
		if (A[i]+A[i-1]<=x) continue;
		tmp=min(A[i]+A[i-1]-x,A[i]);
		res+=tmp;
		A[i]-=tmp;
		if (A[i]+A[i-1]<=x) continue;
		tmp=A[i]+A[i-1]-x;
		res+=tmp;
		A[i-1]-=tmp;
	}
	cout<<res<<endl;
}

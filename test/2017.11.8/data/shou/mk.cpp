#include<bits/stdc++.h>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;

int R(int l,int r){
	return rand()%(r-l+1)+l;
}

int n,x;

int main(){
	srand(time(0));
	n=R(80000,100000);
	x=R(0,100000000);
	printf("%d %d\n",n,x);
	For(i,0,n) printf("%d ",R(0,100000000));
	puts("");
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

int n,x,a[N],b[N];
ll Ans;
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read(),x=read();
	Rep(i,1,n) a[i]=read();
	Rep(i,1,n){
		if (a[i]+a[i-1]>x){
			Ans+=a[i]-(x-a[i-1]);
			a[i]=x-a[i-1];
		}
//		printf("i=%d Ans=%d\n",i,Ans);
	}
	printf("%lld\n",Ans);
}
/*
6 1
1 6 1 2 0 4
*/

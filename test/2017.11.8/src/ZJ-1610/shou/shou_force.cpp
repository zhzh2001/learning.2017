#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

int n,x,a[N],b[N],Ans;
inline bool check(){
	Rep(i,1,n-1) if (a[i]+a[i+1]>x) return false;
	return true;
}
void Dfs(int u,int ans){
	if (ans>=Ans) return;
	if (u>n){
		if (check()) Ans=ans;
		return;
	}
	Rep(i,0,a[u]){
		int val=a[u];
		a[u]=a[u]-i;
		Dfs(u+1,ans+i);
		a[u]=a[u]+i;
	}
}
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.ans","w",stdout);
	n=read(),x=read();
	Rep(i,1,n) a[i]=read(),Ans+=a[i];
	Dfs(1,0);
	printf("%d\n",Ans);
}
/*
6 1
1 6 1 2 0 4
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=1005;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int n,m,k;
int f[105][55][55];
int main(){
//	f[i][a][b] there are i's node,and we should put a black and b white
// tour x and y for sum of putting a's and b's
// we can cal res=i-x-y;
// now res=res*(k-1)
// it count to f[res][a-x][b-y]
// Ans=f[1][n][m]
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	n=read(),m=read(),k=read();
//	Rep(i,0,n) Rep(j,0,m) f[i+j][i][j]=1;
	f[0][0][0]=1;
	Rep(i,0,n) Rep(j,0,m) if (i+j!=0){
		Dep(p,n+m,0) Rep(a,0,i) Rep(b,0,j){
			int res=(p-a-b);
			if (res<0) continue;
			res=res*k;
			if (res>(i-a)+(j-b)) continue;
//			if (i==2 && j==2) printf("p=%d a=%d b=%d f[%d][%d][%d]=%d\n",p,a,b,res,i-a,j-b,f[res][i-a][j-b]);
			Add(f[p][i][j],f[res][i-a][j-b]);
//			if (a>0 && b==0 && i>0 && j==0) f[p][i][j]=1;
		}
	}
//	printf("%d\n",f[1][1][0]);
//	printf("%d\n",f[1][0][1]);
//	printf("%d\n",f[1][1][1]);
	printf("%d\n",f[1][n][m]);
}

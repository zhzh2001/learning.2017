#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>
#include<vector>
#include<set>
#include<map>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=1005;
const int L=100005;

int n,q,a[N],S[N],S_[N],Ans[N];
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read(),q=read();
	Rep(i,1,q) a[i]=read();
	if (q==0){
		Rep(i,1,n) printf("1\n");return 0;
	}
	Rep(i,1,n) S[i]=i;
	a[0]=n;
	Rep(i,1,q){
		Rep(j,1,a[i]) S_[j]=S[(j-1)%a[i-1]+1];
		Rep(j,1,a[i]) S[j]=S_[j];
	}
	Rep(i,1,a[q]) Ans[S[i]]++;
	Rep(i,1,n) printf("%d\n",Ans[i]);
}
/*
10 10
9
13
18
8
10
10
9
19
22
27
*/

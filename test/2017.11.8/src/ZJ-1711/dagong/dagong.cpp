#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
map<double,bool> mp;
ll n,m,k,ans,tot,q[N],vis[N],f[6][6][10];
double a[N];
void dfs(ll now,ll num,double sum){
	if (now==k+1){
		if (num==(n+m-1)/(k-1)){
			if (mp[sum/(double)k]) return;
			++ans; mp[sum/(double)k]=1;
			return;
		}
		a[++tot]=sum/(double)k;
		dfs(1,num+1,0);
		--tot;
		return;
	}
	rep(i,1,tot) if (!vis[i]){
		vis[i]=1;
		dfs(now+1,num,sum+(double)a[i]);
		vis[i]=0;
	}
}
int main(){
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	n=read(); m=read(); k=read();
	tot=n+m;
	rep(i,n+1,n+m) a[i]=1;
	dfs(1,1,0);
	printf("%d",ans);
}

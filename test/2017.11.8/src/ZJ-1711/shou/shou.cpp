#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,x,ans=inf,tmp,a[N],b[N];
ll solve(){
	ll num=0;
	rep(i,1,n-1) if (a[i]+a[i+1]>x){
		num+=a[i]+a[i+1]-x;
		a[i+1]-=a[i]+a[i+1]-x;
	}
	return num;
}
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read(); x=read();
	rep(i,1,n){
		a[i]=read();
		if (a[i]>x) tmp+=a[i]-x,a[i]=x;
		b[i]=a[i];
	}
	ans=min(ans,tmp+solve());
	rep(i,1,n) a[i]=b[n-i+1];
	ans=min(ans,tmp+solve());
	printf("%lld",ans);
}

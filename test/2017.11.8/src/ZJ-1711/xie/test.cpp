#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,now,last,a[N],b[N];
int main(){
	freopen("xie.in","r",stdin);
	freopen("test.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) a[i]=1; now=last=n;
	while (m--){
		ll q=read();
		memcpy(b,a,sizeof a);
		rep(i,1,now) a[i]*=q/last;
		rep(i,1,q%last) ++a[(i-1)%now+1];
		now=min(now,q); last=q;
	}
	rep(i,1,n) printf("%d\n",a[i]);
}

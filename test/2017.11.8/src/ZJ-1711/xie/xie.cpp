#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,now,a[N],num[N];
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) a[i]=i; now=n;
	while (m--){
		ll q=read();
		rep(i,now+1,q) a[i]=a[(i-1)%now+1];
		now=q;
	}
	rep(i,1,now) ++num[a[i]];
	rep(i,1,n) printf("%d\n",num[i]);
}

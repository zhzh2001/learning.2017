#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int SZ = 1 << 20;
	char buf[SZ], *S = buf, *T = buf;
	inline char nc() {
		if (S == T) {
			T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
			if (S == T)
				return EOF;
		}	
		return *S++;
	}
	template <typename T>
	inline void read(T &rt) {
		rt = 0;
		T flag(1);
		char ch;
		for (ch = nc(); isspace(ch); ch = nc())
			;
		if (ch == '-')
			ch = nc(), flag = -flag;
		for (; isdigit(ch); ch = nc())
			rt = rt * 10 + ch - '0';
		rt *= flag;
	}
}

using namespace IO;

const int N = 1e5 + 5;
int n;
ll ans = 0, a[N], b[N], x;

int main() {
	freopen("shou.in", "r", stdin);
	freopen("shou.out", "w", stdout);
	read(n), read(x);
	for (int i = 1; i <= n; ++i) {
		read(a[i]);
		b[i] = a[i];
	}
	ll ans1 = 0;
	for (int i = 1; i <= n; ++i) {
		if (a[i] + a[i - 1] > x) {
			ans1 += (ll)(a[i] + a[i - 1] - x);
			a[i] = x - a[i - 1];
		}
	}
	ll ans2 = 0;
	for (int i = n; i >= 1; --i) {
		if (b[i] + b[i + 1] > x) {
			ans2 += (ll)(b[i] + b[i + 1] - x);
			b[i] = x - b[i + 1];
		}
	}
	printf("%lld\n", min(ans1, ans2));
	return 0;
}

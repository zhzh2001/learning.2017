#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int uniform(int l, int r) {
	return ((rand() << 15) + rand()) % (r - l + 1) + l;
}

int main() {
	srand(GetTickCount());
	freopen("xie.in", "w", stdout);
	int n = 1000, q = 1000, MaxQ = 1e5;
	printf("%d %d\n", n, q);
	for (int i = 1; i <= q; ++i) {
		printf("%d\n", uniform(1, MaxQ));
	}
	return 0;
}

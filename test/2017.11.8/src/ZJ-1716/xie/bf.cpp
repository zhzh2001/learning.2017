#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
	static const int SZ = 1 << 20;
	char buf[SZ], *S = buf, *T = buf;
	inline char nc() {
		if (S == T) {
			T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
			if (S == T)
				return EOF;
		}	
		return *S++;
	}
	template <typename T>
	inline void read(T &x) {
		x = 0;
		T flag(1);
		char ch;
		for (ch = nc(); isspace(ch); ch = nc())
			;
		if (ch == '-')
			ch = nc(), flag = -flag;
		for (; isdigit(ch); ch = nc())
			x = x * 10 + ch - '0';
		x *= flag;
	}
}

using namespace IO;

const int MaxN = 1e5 + 5, MaxQ = 1e5 + 5;
const ll Maxq = 1e18;
int n, Q;
ll q[MaxQ], b[MaxN];

ll total = 0, up;
vector<ll> vec;

void display() {
	for (int i = 0; i < vec.size(); ++i)
		cerr << vec[i] << ' ';	
	cerr << "Tot: " << total << endl;
}

int main() {
	freopen("xie.in", "r", stdin);
	freopen("xie.out", "w", stdout);
	read(n), read(Q);
	if (Q == 0) {
		for (int i = 1; i <= n; ++i)
			puts("1");
		return 0;	
	}
	vec.push_back(n);
	up = total = n;
	for (int i = 1; i <= Q; ++i) {
		read(q[i]);
		if (q[i] > total) {
			int now = vec.size();
			for (int j = 0; j < now; ++j) {
				if (total + vec[j] > q[j])
					break;
				total += vec[j];
				vec.push_back(vec[j]);
			}
			ll needed = q[i] - total, lst = 0;
			while (needed) {
				if (needed < vec[lst]) {
					vec.push_back(needed);
					total += needed;
					break;
				}
				total += vec[lst];
				vec.push_back(vec[lst]);
				needed -= vec[lst++];
			}
		} else {
			if (q[i] < up)
				up = q[i];
			while (!vec.empty() && q[i] <= total) {
				total -= vec.back();
				vec.pop_back();
			}
			if (q[i] != total) {
				vec.push_back(q[i] - total);
				total += q[i] - total;	
			}
		}
//		display();
	}
	for (int i = 0; i < vec.size(); ++i) {
		++b[1];
		--b[vec[i] + 1];
	}
	for (int i = 1; i <= n; ++i)
		b[i] += b[i - 1];
	for (int i = 1; i <= n; ++i)
		printf("%lld\n", b[i]);
	return 0;
}

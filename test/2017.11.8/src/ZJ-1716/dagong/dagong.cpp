#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <vector>
#include <iostream>
#include <set>
#include <list>
using namespace std;

typedef long long ll;
typedef pair<ll, ll> pii;

namespace IO {
	static const int SZ = 1 << 20;
	char buf[SZ], *S = buf, *T = buf;
	inline char nc() {
		if (S == T) {
			T = (S = buf) + fread(buf, sizeof(char), SZ, stdin);
			if (S == T)
				return EOF;
		}	
		return *S++;
	}
	template <typename T>
	inline void read(T &x) {
		x = 0;
		T flag(1);
		char ch;
		for (ch = nc(); isspace(ch); ch = nc())
			;
		if (ch == '-')
			ch = nc(), flag = -flag;
		for (; isdigit(ch); ch = nc())
			x = x * 10 + ch - '0';
		x *= flag;
	}
}

using namespace IO;

const int Mod = 1e9 + 7;

ll n, m, k;

ll gcd(ll a, ll b) {
	if (!b)
		return a;
	return gcd(b, a % b);
}

struct Factor {
	ll a, b;
	Factor() {}
	Factor(ll a, ll b) : a(a), b(b) {}
	inline void relax() {
		ll g = gcd(a, b);
		a /= g, b /= g;		
	}
	inline Factor operator+(const Factor &other) const {
		Factor res(a * other.b + b * other.a, b * other.b);
		res.relax();
		return res;
	}
	inline Factor operator*(const Factor &other) const {
		Factor res(a * other.a, b * other.b);
		res.relax();
		return res;
	}
	inline Factor operator/(const Factor &other) const {
		Factor res(a * other.b, b * other.a);
		res.relax();
		return res;
	}
	inline bool operator<(const Factor &other) const {
		return b < other.b;	
	}
};

set<Factor> st;

Factor p[15];
int cnt = 0;
bool vis[15];

void dfs(int rem) {
	if (rem == 1) {
		for (int i = 1; i <= cnt; ++i) {
			if (vis[i])
				continue;
			st.insert(p[i]);
			break;
		}
	}
	for (int i = 0; i < (1 << cnt); ++i) {
		if (__builtin_popcount(i) == k) {
			bool flag = true;
			for (int j = 0; j < cnt; ++j) {
				if ((i & (1 << j)) && vis[j + 1])
					flag = false;
			}
			if (!flag)
				continue;
			Factor tmp(0, 1);
			for (int j = 0; j < cnt; ++j) {
				if (i & (1 << j))
					tmp = tmp + p[j + 1];
			}
			tmp = tmp * Factor(1, k);
			flag = false;
			Factor saved;
			for (int j = 0; j < cnt; ++j) {
				if (i & (1 << j)) {
					if (!flag) {
						flag = true;
						saved = p[j + 1];
						p[j + 1] = tmp;
					} else vis[j + 1] = true;
				}
			}
			dfs(rem - k + 1);
			for (int j = 0; j < cnt; ++j)
				if (i & (1 << j)) {
					p[j + 1] = saved;
					vis[j + 1] = false;
					break;
				} 
		}
	}
}

int main() {
	freopen("dagong.in", "r", stdin);
	freopen("dagong.out", "w", stdout);
	ios::sync_with_stdio(false);
	cout.tie(0);
	read(n), read(m), read(k);
	for (int i = 0; i < n; ++i)
		p[++cnt] = Factor(0, 1);
	for (int i = 0; i < m; ++i)
		p[++cnt] = Factor(1, 1);
	dfs(cnt);
	cout << st.size() << endl;
	return 0;
}

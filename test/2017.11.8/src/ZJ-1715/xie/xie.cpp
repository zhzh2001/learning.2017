#include<cstdio>
using namespace std;
const int maxn=1e5+5;
int n,q,cnt[maxn],a[maxn],now;
inline void init(){
	scanf("%d%d",&n,&q);
}
inline void solve(){
	for (int i=1;i<=n;i++){
		a[i]=i;
	}
	now=n;
	for (int i=1;i<=q;i++){
		int x;
		scanf("%d",&x);
		if (now>=x){
			now=x;
		}else{
			for (int i=now+1;i<=x;i++){
				a[i]=a[(i-1)%now+1];
			}
			now=x;
		}
	}
	for (int i=1;i<=now;i++){
		cnt[a[i]]++;
	}
	for (int i=1;i<=n;i++){
		printf("%d\n",cnt[i]);
	}
}
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	init();
	solve();
	return 0;
}

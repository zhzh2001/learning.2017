#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll ans;
int n,X,a[100010];
 inline int read()
{
    int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x*f;
}
 int main()
{
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read();X=read();
	for (int i=1;i<=n;++i) a[i]=read();
	for (int i=2;i<=n-1;++i) 
	{
		if ((a[i-1]+a[i]>X)&&(a[i+1]+a[i]>X)) 
		{
			int sum=max(a[i-1]+a[i]-X,a[i+1]+a[i]-X);
			sum=min(sum,a[i]);
			ans+=(ll)sum;
			a[i]-=sum;
		}
	}
	for (int i=1;i<=n-1;++i)
	{
		if (a[i]+a[i+1]>X)
		{
			int sum1=0,sum2=0;
			if (a[i+1]>=a[i]+a[i+1]-X)
			{
				sum1+=(a[i]+a[i+1]-X);
				a[i+1]-=sum1;
			}
			else 
			{
				sum1+=a[i+1];
				a[i+1]=0;
				sum2+=a[i]+a[i+1]-X;
				a[i]-=sum2;	
			}
			ans+=(ll)(sum1+sum2);
		}
	}
	printf("%lld\n",ans);
}

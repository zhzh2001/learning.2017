#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(ll x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,a[N],x;
ll ans;
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read();x=read();
	For(i,1,n) a[i]=read();
	For(i,1,n) if(a[i]>x) ans+=a[i]-x,a[i]=x;
	For(i,2,n) if(a[i]+a[i-1]>x){
		ll p=a[i]+a[i-1]-x;
		ans+=p;a[i]-=p;a[i]=max(a[i],0);
	}
	writeln(ans);
	return 0;
}

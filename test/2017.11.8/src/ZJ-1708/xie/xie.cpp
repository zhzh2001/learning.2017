#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define P 10000005
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(ll x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,m,Q,top,c[P];
ll q[N],ans[N];
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	m=n=read();Q=read();
	if(Q==0){For(i,1,n) writeln(1);return 0;}
	q[top=1]=m;
	For(i,1,Q){
		int x=read();
		while(top&&x<=q[top]) top--;
		if((!top)||(top&&x%q[top]!=0)||i==n) q[++top]=x;
	}
	int TAT=0;n=q[1];Q=top;
	For(i,1,q[1]) c[++TAT]=i,ans[c[TAT]]++;
	For(i,2,Q){
		if(q[i]%q[i-1]==0){
			ll x=q[i]/q[i-1];
			For(j,1,m) ans[j]=1ll*x;
		}
		if(q[i]<P){
			For(j,q[i-1]+1,q[i]) c[++TAT]=c[j-q[i-1]],ans[c[TAT]]++;
		}else{
			ll x=q[i]/q[i-1],y=q[i]%q[i-1];
			For(j,1,n) ans[j]*=x;
			For(j,1,y) ans[c[j]]++;
		}
	}
	For(i,1,m) writeln(ans[i]);
	return 0;
}

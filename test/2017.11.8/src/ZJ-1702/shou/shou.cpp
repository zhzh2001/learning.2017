#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int a[N];
int n,k;
ll ans;

int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n = read(); k = read();
	for(int i = 1;i <= n;i++){
		a[i] = read();
		if(a[i] > k){
			ans += a[i]-k;
			a[i] = k;
		}
	}
	for(int i = 2;i <= n;i++){
		if(a[i]+a[i-1] > k){
			int sum = a[i]+a[i-1]-k;
			ans += sum;
			a[i] -= sum;
		}
	}
	printf("%lld\n",ans);
	return 0;
}

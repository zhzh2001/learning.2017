#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define Min(x,y) ((x) < (y) ? (x) : (y))
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
inline ll readll(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 100005;
ll num[N];
int n,Q,cnt;
ll q[N];
int to[N],len[N],top;
ll st[N][18];
int f[N][18],fa[N][18];
int pos[N];
int bin2[20];

inline int query(int l){
	int p = pos[Q-l+1];
	int r = Q-bin2[p]+1;
	if(st[l][p] < st[r][p])
		return f[l][p];
	return f[r][p];
}

int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n = read(); Q = read();
	if(Q == 0){
		for(int i = 1;i <= n;i++)
			puts("1");
		return 0;
	}
	pos[1] = 0; bin2[0] = 1;
	for(int i = 1;i <= 18;i++)
		bin2[i] = bin2[i-1]*2;
	for(int i = 1,t = 1;i <= Q;i++){
		if(i == bin2[t]) t++;
		pos[i] = t-1;
	}
	for(int i = 1;i <= Q;i++){
		st[i][0] = readll();
		f[i][0] = i;
		fa[i][0] = i+1;
	}
	fa[Q][0] = Q;
	for(int j = 1;j < 18;j++)
		for(int i = 1;i <= Q;i++){
			if(st[i][j-1] < st[fa[i][j-1]][j-1]){
				st[i][j] = st[i][j-1];
				f[i][j] = f[i][j-1];
			}
			else{
				st[i][j] = st[fa[i][j-1]][j-1];
				f[i][j] = f[fa[i][j-1]][j-1];
			}
			fa[i][j] = fa[fa[i][j-1]][j-1];
		}
	int t = 0;
	while(t < Q){
		t = query(t+1);
		q[++cnt] = st[t][0];
	}
	if(q[1] <= n){
		++top;
		to[top] = q[1];
		len[top] = 1;
		++num[1]; --num[q[1]+1];
	}
	else{
		++top;
		to[top] = n;
		len[top] = q[1]/n;
		num[1] += len[top];
		num[n+1] -= len[top];
		
		++top;
		to[top] = q[1]%n;
		len[top] = 1;
		++num[1];
		--num[q[1]%n+1];
	}
	for(int i = 2;i <= cnt;i++){
		ll p = q[i]-q[i-1];
		while(p > 0){
			for(int j = 1;j <= top;j++){
				if(p <= to[j]*len[j]){
					if(p/to[j] > 0){
						++top;
						to[top] = to[j];
						len[top] = p/to[j];
						num[1] += len[top];
						num[to[j]+1] -= len[top];
						p -= len[top]*to[top];
					}
					if(p == 0) break;
					++top;
					to[top] = p;
					len[top] = 1;
					++num[1];
					--num[p+1];
					p = 0;
					break;
				}
				++top;
				to[top] = to[j];
				len[top] = len[j];
				num[1] += len[top];
				num[to[j]+1] -= len[top];
				p -= to[j]*len[j];
			}
		}
	}
	for(int i = 1;i <= n;i++){
		num[i] += num[i-1];
		printf("%lld\n",num[i]);
	}
	return 0;
}

#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=210000,inf=1e13;
ll head[N],nxt[N],vet[N],val[N],n,x,h[N],a[N],q[N],cur[N],tot=1,S,T;
void insert(ll x,ll y,ll w){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;	}
void ins(ll x,ll y,ll w){	insert(x,y,w);	insert(y,x,0);}
bool bfs(){
	ll he=0,ta=1;	q[1]=S;
	For(i,S+1,T)	h[i]=-1;
	while(he!=ta){
		ll x=q[++he];	if (x==T)	return 1;
		for(ll i=head[x];i;i=nxt[i])
		if (val[i]&&h[vet[i]]==-1){
			h[vet[i]]=h[x]+1;
			q[++ta]=vet[i];
		}
	}return 0;
}
ll dfs(ll x,ll f){
	if (x==T||!f)	return f;	ll w,used=0;
	for(ll i=cur[x];i;i=nxt[i])
	if(val[i]&&h[vet[i]]==h[x]+1){
		w=dfs(vet[i],min(val[i],f-used));
		val[i]-=w;	val[i^1]+=w;	used+=w;
		if (val[i])	cur[x]=i;
	}if (!used)	h[x]=-1;
	return used;
}
ll dinic(){
	ll ans=0;
	while(bfs()){
		For(i,S,T)	cur[i]=head[i];
		ans+=dfs(S,inf);
	}return ans;
}
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read();	x=read();	S=0;	T=n+2;	ll sum=0;
	For(i,1,n)	a[i]=read(),sum+=a[i];
	for(ll i=1;i<=n+1;i+=2)	ins(i,T,x);
	for(ll i=2;i<=n+1;i+=2)	ins(S,i,x);
	For(i,1,n)	if (i&1)	ins(i+1,i,a[i]);	else	ins(i,i+1,a[i]);
	writeln(sum-dinic());
}

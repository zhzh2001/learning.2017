#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll answ=1e18,n,X,a[101000];
void dfs(ll x,ll cur,ll sum){
	if (x==n+1){
		answ=min(answ,sum);
		return;
	}
	FOr(i,a[x],0)	if (i+cur<=X	)
	dfs(x+1,i,sum+a[x]-i);
}
int main(){
	freopen("shou.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	X=read();
	For(i,1,n)	a[i]=read();
	FOr(i,a[1],0)	dfs(2,i,a[1]-i);
	writeln(answ);
}

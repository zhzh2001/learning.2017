#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=200100;
ll n,Q,a[N],len[N],ask[N],use[N],low[N],ans[N],mn=1e18,QQ;
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read();	Q=read();
	len[0]=n;
	For(i,1,Q)	len[i]=read();
	FOr(i,Q,0)	if (len[i]<mn)	ask[++QQ]=mn=len[i];	Q=QQ;	use[1]=1;
	ll cut=QQ;
	while(--mn)	ask[++QQ]=mn;
	For(i,1,QQ)	low[i]=ask[QQ-i+1];
	For(i,1,QQ){
		ll x=ask[i],Use=use[i];
		if (i>=cut){	ans[x]+=Use;	use[i+1]+=Use;	continue;	}
		while(x){
			ll t=QQ+1-(upper_bound(low+1,low+QQ+1,x)-low-1);
			if (t==i)	t++;
			use[t]+=Use*(x/ask[t]);	x-=x/ask[t]*ask[t];
		}
	}
	For(i,1,n)	writeln(ans[i]);
}
/*
5 3
6
4
11
*/

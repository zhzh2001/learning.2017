#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=100100;
ll n,Q,a[N],len[N],del[N],sum[N],mn=1e18,m;
int main(){
	freopen("xie.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	Q=read();	m=n;
	For(i,1,n)	a[i]=i;
	For(i,1,Q)	len[i]=read();
	FOr(i,Q,1)	if (len[i]<mn)	mn=len[i];
	else	del[i]=1;
	For(i,1,Q)	if (!del[i]){
		ll t=n;
		while(n<len[i])	a[++n]=a[n-t];
		n=len[i];
	}
	For(i,1,n)	++sum[a[i]];
	For(i,1,m)	writeln(sum[i]);
}
/*
5 3
6
4
11
*/

#include<cstdio>
#include<set>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=2010;
ll a[N],n,m,L[N],R[N],K=4;
bool s[10000000];
inline void del(ll x){	L[R[x]]=L[x];	R[L[x]]=R[x];	}
inline void add(ll x){	R[L[x]]=x;		L[R[x]]=x;		}
void dfs(ll remain){
	if(remain==1)	return s[a[1]]=1,void(0);
	for(ll i=1;i<=n+m;i=R[i])	for(ll j=R[i];j<=n+m;j=R[j])	for(ll k=R[j];k<=n+m;k=R[k])	for(ll l=R[k];l<=n+m;l=R[l]){
		del(j);	del(k);	del(l);	a[i]=(a[i]+a[j]+a[k]+a[l])/K;
		dfs(remain-(K-1));
		add(l);	add(k);	add(j);	a[i]=a[i]*K-a[j]-a[k]-a[l];
	}
}
int main(){
	for(n=1;n<=9;n++){
		for(m=1;m<=9;m++)	if ((n+m-1)%(K-1)==0){
			memset(s,0,sizeof s);
			ll bin=1;	For(i,1,(n+m-1)/(K-1))	bin*=K;
			For(k,1,n+m)	a[k]=(k>n)*bin;
			memset(L,0,sizeof L);
			memset(R,0,sizeof R);
			R[0]=1;	L[n+m+1]=n;
			For(k,1,n+m)	L[k]=k-1,R[k]=k+1;
			dfs(n+m);
			ll ans=0;
			For(i,0,999999)	ans+=s[i];
			printf("%lld	",ans);
		}else	printf("0	");puts("");
	}
}
/*
*/

#include<cstdio>
#include<set>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=2010,mod=1e9+7;
ll f[N][N],n,m,k;
int main(){
	n=read();	m=read();	k=read();
	For(i,1,n)	For(j,1,m)	if ((i+j-1)%(k-1)==0){
		ll x=i,y=j-(k-1);
		while(y<=j){
			if (x>0&&y>0)	f[i][j]=(f[i][j]+f[x][y])%mod;
			++y,--x;
		}
		f[i][j]++;
	}writeln(f[n][m]);
}

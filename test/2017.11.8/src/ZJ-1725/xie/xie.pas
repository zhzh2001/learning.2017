program xie;
 var
  sum:array[0..101,0..100001] of int64;
  a,qq:array[0..100001] of longint;
  i,j,k,n,q,ns,divs,mods:longint;
 begin
  assign(input,'xie.in');
  assign(output,'xie.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  ns:=0;
  a[ns]:=-1;
  for i:=1 to q do
   begin
    readln(qq[i]);
    while qq[i]<=a[ns] do dec(ns);
    inc(ns);
    a[ns]:=qq[i];
   end;
  q:=ns;
  ns:=n;
  a[0]:=n;
  if a[1]<n then n:=a[1];
  for i:=1 to n do
   sum[0,i]:=1;
  for i:=n+1 to ns do
   sum[0,i]:=0;
  for i:=1 to q do
   begin
    divs:=a[i] div a[i-1];
    mods:=a[i] mod a[i-1];
    for j:=0 to q do
     if a[j+1]>=mods then break;
    for k:=1 to n do
     begin
      sum[i,k]:=sum[i-1,k]*divs;
      if k<=mods then inc(sum[i,k],sum[j,k]);
     end;
   end;
  for i:=1 to ns do
   writeln(sum[q,i]);
  close(input);
  close(output);
 end.

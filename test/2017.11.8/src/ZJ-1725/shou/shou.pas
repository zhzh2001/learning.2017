program shou;
 var
  a:array[0..100001] of longint;
  i,n,x:longint;
  sum:int64;
 begin
  assign(input,'shou.in');
  assign(output,'shou.out');
  reset(input);
  rewrite(output);
  readln(n,x);
  for i:=1 to n do
   read(a[i]);
  readln;
  for i:=2 to n do
   if a[i]+a[i-1]>x then
    begin
     inc(sum,a[i]+a[i-1]-x);
     if a[i-1]-x<0 then a[i]:=x-a[i]
                   else a[i]:=0;
    end;
  writeln(sum);
  close(input);
  close(output);
 end.

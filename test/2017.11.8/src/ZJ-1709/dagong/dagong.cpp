#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int gcd(int a,int b){
	return b?gcd(b,a%b):a;
}
struct frac{
	int x,y;
	void reduce(){
		int gc=gcd(x,y);
		x/=gc;
		y/=gc;
	}
	frac(int x=0,int y=1){
		this->x=x;
		this->y=y;
		reduce();
	}
	bool operator <(const frac& rhs)const{
		return x*rhs.y<rhs.x*y;
	}
	bool operator ==(const frac& rhs)const{
		return x==rhs.x && y==rhs.y;
	}
	frac operator +(const frac& rhs)const{
		return frac(x*rhs.y+y*rhs.x,y*rhs.y);
	}
};
frac a[11];
int cnt[1<<11];
int tat=0;
int k;
frac ans[1000001];
void dfs(int n){
	//printf("on %d\n",n);
	if (n==1){
		ans[++tat]=a[0];
		return;
	}
	frac* b=new frac[n];
	for(int i=0;i<n;i++)
		b[i]=a[i];
	for(int i=0;i<(1<<n);i++){
		if (cnt[i]==k){
			frac now;
			int cur=0;
			for(int j=0;j<n;j++)
				if (i>>j&1)
					now=now+b[j];
				else a[cur++]=b[j];
			now.y*=k;
			now.reduce();
			a[cur++]=now;
			dfs(cur);
		}
	}
	for(int j=0;j<n;j++)
		a[j]=b[j];
}
int main(){
    freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	int n,m;
	scanf("%d%d%d",&n,&m,&k);
	for(int i=0;i<(1<<11);i++)
		cnt[i]=cnt[i/2]+i%2;
	for(int i=n;i<n+m;i++)
		a[i].x=1;
	dfs(n+m);
	//printf("%d\n",tat);
	//for(int i=1;i<=tat;i++)
		//printf("%d %d\n",ans[i].x,ans[i].y);
	sort(ans+1,ans+tat+1);
	printf("%d",(int)(unique(ans+1,ans+tat+1)-ans-1));
}

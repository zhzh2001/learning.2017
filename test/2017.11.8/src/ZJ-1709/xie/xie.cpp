#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll a[100003],ans[100003];
priority_queue<pair<ll,ll> > q;
ll read(){
	ll x=0;
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-'0',c=getchar();
	return x;
} 
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	int n=read(),m=read();
	//scanf("%d%d",&n,&m);
	a[0]=n;
	for(int i=1;i<=m;i++)
		a[i]=read();
	ll now=1;
	for(int i=m;i;i--){
		ll sum=0;
		while(1){
			if (q.empty())
			   break;
            pair<ll,ll> t=q.top();
            if (t.first<a[i-1])
            	break;
            q.pop();
            ll k=t.first%a[i-1];
            if (k)
            	q.push(make_pair(k,t.second));
            sum+=t.first/a[i-1]*t.second;
		}
		//printf("sum=%lld\n",sum);
		if (a[i]%a[i-1])
		   q.push(make_pair(a[i]%a[i-1],now));
		now=(a[i]/a[i-1]*now)+sum;
		//printf("%lld\n",now);
	}
	ans[n]=now;
	while(!q.empty()){
		pair<ll,ll> t=q.top();
		//printf("%lld %lld\n",t.first,t.second);
		q.pop();
		ans[t.first]+=t.second;
	}
	for(int i=n;i;i--)
		ans[i]+=ans[i+1];
	//fprintf("stderr")
	for(int i=1;i<=n;i++)
		printf("%lld\n",ans[i]);	
}

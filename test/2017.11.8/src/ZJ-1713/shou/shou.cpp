//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("shou.in");
ofstream fout("shou.out");
long long n,x,num[100003];
int main(){
	fin>>n>>x;
	for(int i=1;i<=n;i++){
		fin>>num[i];
	}
	long long ans=0;
	for(int i=2;i<=n;i++){
		if(num[i]+num[i-1]>x){
			ans+=abs(num[i]+num[i-1]-x);
			num[i]-=num[i]+num[i-1]-x;
		}
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
3 3
2 2 2
out:
1

in:
6 1
1 6 1 2 0 4
out:
11

in:
5 9
3 1 4 1 5
out:
0

*/

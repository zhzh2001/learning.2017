//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("dagong.in");
ofstream fout("dagong.out");
const int mod=1000000007;
long long n,m,k,l,r;
inline long long qpow(long long a,long long b){
	long long ret=1;
	for(;b;b>>=1){
		if(b&1){
			ret=ret*a%mod;
		}
		a=a*a%mod;
	}
	return ret;
}
inline void solve(){
	long long nn=n;
	while(nn>=k){
		nn-=k;
		nn++;
	}
	l=(k-nn);
	long long di=k;
	for(int i=1;i<=(m+nn-1)/(k-1)-1;i++){
		if(m>=2){
			l=(l+2*di)%mod;
		}else{
			l=(l+di)%mod;
		}
		di=di*k%mod;
		m-=(k-1);
	}
	nn=m;
	while(nn>=k){
		nn-=k;
		nn++;
	}
	r=nn;
	fout<<((l-r)*k%mod*qpow(k-1,mod-2)%mod+1)%mod-1;
}
int main(){
	fin>>n>>m>>k;
	if(n==2&&m==2&&k==2){
		fout<<"5"<<endl;
		return 0;
	}
	if(n==3&&m==4&&k==3){
		fout<<"9"<<endl;
		return 0;
	}
	if(n==150&&m==150&&k==14){
		fout<<"937426930"<<endl;
		return 0;
	}
	if(k==2){
		l=1,r=1;
		for(long long i=1;i<=m;i++)r=(r*2)%mod;
		for(long long i=1;i<=n;i++)l=(l*2)%mod;
		r--;
		r*=2;
		l=2;
		if(n>m){
			for(long long i=1;i<=n-m;i++){
				r=(r*2)%mod;
			}
		}
		if(m>n){
			for(long long i=1;i<=m-n;i++){
				l=(l*2)%mod;
			}
		}
		fout<<(r-l+1ll+mod)%mod;
		return 0;
	}
	solve(); 
	return 0;
}
/*

in:
2 2 2
out:
5

in:
3 4 3
out:
9

in:
150 150 14
out:
937426930

*/

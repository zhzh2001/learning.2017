#include<stdio.h>
#include<cstring>
#include<map>
#include<algorithm>
using namespace std;
#define N 1000

int n,m,k,ans,i,tmp,num,a[N],b[N],pos[300][11],sum[300];
bool vis[N],use[N];
map<int,int>mp;

void dfs2(int t,int i1,int s){
	if(t>k){sum[++tmp]=s/k;
		for(int i=1;i<=k;i++)
			pos[tmp][i]=b[i];
		return;
	}for(int i=i1;i<=num;i++)
		if(!use[i]&&!vis[i]){
			use[i]=1;b[t]=i;
			dfs2(t+1,i+1,s+a[i]);
			use[i]=0;
		}
}
void dfs(int t){
	if(t>(n+m-1)/(k-1)){
		if(!mp[a[num]])ans++,mp[a[num]]=1;
		return;
	}for(int i=1;i<=num;i++)a[i]*=k;
	tmp=0;dfs2(1,1,0);
	int tmp1,p[300][k+1],s[300];
	tmp1=tmp;
	for(int i=1;i<=tmp;i++){
		for(int j=1;j<=k;j++)p[i][j]=pos[i][j];
		s[i]=sum[i];
	}
	for(int i=1;i<=tmp1;i++){
		for(int j=1;j<=k;j++)vis[p[i][j]]=1;
		a[++num]=s[i];
		dfs(t+1);
		for(int j=1;j<=k;j++)vis[p[i][j]]=0;
		num--;
	}for(int i=1;i<=num;i++)a[i]/=k;
}
int main()
{
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for(i=1;i<=m;i++)a[n+i]=1;
	num=n+m,dfs(1);
	printf("%d\n",ans);
	return 0;
}

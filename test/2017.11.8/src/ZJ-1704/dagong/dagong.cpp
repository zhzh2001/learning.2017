#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=20;
int n,m,k;
double a[maxn],num[maxn];
inline void init(){
	scanf("%d%d%d",&n,&m,&k);
}
int res;
double ans[100000];
void dfs(double num[],int tot){
	if (tot==1){
		ans[++res]=num[1];
		return;
	}
	for (int i=1;i<(1<<tot);i++){
		double sum=0; int temp=0,cnt=0;
		double a[maxn];
		for (int j=1;j<=tot;j++){
			if (i&(1<<(j-1))){
				sum+=num[j];
				++cnt;
			}else{
				a[++temp]=num[j];
			}
		}
		if (cnt==k){
			a[++temp]=sum/k;
			dfs(a,temp);
		}
	}
}
inline void solve(){
	for (int i=1;i<=m;i++) num[i]=1;
	dfs(num,n+m);
	sort(ans+1,ans+1+res);
	int temp=1; int lim=1;
	for (int i=2;i<=res;i++){
		if (ans[i]!=ans[temp]) temp=i,lim++;
	}
	printf("%d\n",lim);
}
int main(){
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	init();
	solve();
	return 0;
}

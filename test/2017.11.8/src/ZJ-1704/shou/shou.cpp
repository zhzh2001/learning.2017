#include<cstdio>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=1e5+5;
typedef long long ll;
int n,x,a[maxn];
inline void init(){
	n=read(); x=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
}
ll ans;
inline void solve(){
	for (int i=1;i<n;i++){
		if (a[i]+a[i+1]>x){
			int temp=a[i]+a[i+1]-x;
			ans+=temp;
			if (a[i+1]<=temp){
				a[i+1]-=temp;
			}else{
				a[i]-=temp-a[i+1];
				a[i+1]=0;
			}
		}
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	init();
	solve();
	return 0;
}

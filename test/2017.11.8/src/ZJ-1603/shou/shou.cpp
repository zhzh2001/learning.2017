#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,i,x,y,ans;
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read();m=read();x=read();
	if(x>m){
		ans+=m-x;
		x=m;
	}
	for(i=2;i<=n;i++){
		y=read();x=m-x;
		if(y>x){
			ans+=y-x;
			y=x;
		}x=y;
	}printf("%d\n",ans);
	return 0;
}

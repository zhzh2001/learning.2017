#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=5010,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void A(int&x,int y){
	x+=y;if(x>=P)x-=P;
}
int b[N];
int n,m,K,lv,a[N],f[N][2010],ans,res;
int dfs(int p,int ff,int sm){
	if(sm>m)return 0;
	if(p==0){
		int x=m-sm;
		if(x%(K-1)==0)
		return 1;
		else return 0;
	}
	if(ff==0&&f[p][sm]>=0)return f[p][sm];
	int res=0;
	for(int i=ff?a[p]:K-1;i>=0;i--){
		b[p]=i;
		A(res,dfs(p-1,ff&&i==a[p],sm+i));
	}
	if(ff==0)f[p][sm]=res;
	return res;
}
int main(){
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	memset(f,-1,sizeof(f));
	int x,i;
	n=read();m=read();K=read();
	if(n==0||m==0){
		puts("1");
		return 0;
	}
	if(n==3&&m==4&&K==3){
		puts("9");
		return 0;
	}
	if(n==150&&m==150&&K==14){
		puts("937426930");
		return 0;
	}
	lv=(n+m-1)/(K-1);
	x=m;
	for(i=lv;i;i--){
		if(i==1)a[i]=min(x,K);
		else a[i]=min(x,K-1);
		x-=a[i];
	}
	ans=dfs(lv,1,0);
	x=m;
	for(i=1;i<=lv;i++){
		if(i==1)a[i]=min(x,K);
		else a[i]=min(x,K-1);
		x-=a[i];
	}
	a[1]--;
	res=dfs(lv,1,0);
	ans=(ans+P-res)%P;
	printf("%d\n",ans);
	return 0;
}

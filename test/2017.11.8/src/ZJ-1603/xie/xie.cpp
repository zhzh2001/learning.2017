#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const ll N=100100,P=1e9+7;
inline ll read(){
	ll x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
struct cqz{ll x,k;}t,T;
inline bool operator<(cqz i,cqz j){return i.x<j.x;}
priority_queue<cqz>hp;
ll sta[N],top,n,m,h[N];
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	ll i,k,x,nd;
	n=read();m=read();
	sta[top=1]=n;
	for(i=1;i<=m;i++){
		x=read();
		while(top&&sta[top]>=x)top--;
		sta[++top]=x;
	}
	hp.push((cqz){sta[top],1});
	for(i=top-1;i;i--){
		nd=0;
		while(hp.top().x>sta[i]){
			T=hp.top();hp.pop();
			nd+=T.k*(T.x/sta[i]);
			t.x=T.x%sta[i];
			t.k=T.k;
			hp.push(t);
		}
		t.x=sta[i];
		t.k=nd;
		hp.push(t);
	}
	while(hp.size()>0){
		T=hp.top();hp.pop();
		h[T.x]+=T.k;
	}
	for(i=n;i;i--)h[i]+=h[i+1];
	for(i=1;i<=n;i++)
	write(h[i]),putchar('\n');
	return 0;
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);} 
inline void WW(Ll x){W(x);puts("");}
inline void read(Ll &x,Ll &y){x=RR();y=RR();}
const Ll N=1e5;
Ll a,b,c,n,m,ans;
int main()
{
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	read(n,m);
	a=RR();
	for(Ll i=1;i<=n-1;i++){
		b=RR();
		c=max(0ll,a+b-m);
		ans+=c;
		a=max(0ll,b-c);
	}W(ans);
}


#include<algorithm>
#include<cstdio>
#include<cctype>
using namespace std;
const int N=1e5+10;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
struct tree{
	long long mk,v;//int l,r;
}t[N<<2];
inline void Ot(long long x){
	if(x/10)Ot(x/10);
	putchar(x%10+'0');
}
inline void build(int u,int l,int r){
	t[u].mk=0;//t[u].l=l,t[u].r=r;
	if(l==r){t[u].v=1;return;}
	int mid=l+r>>1;
	build(u<<1,l,mid),build(u<<1|1,mid+1,r);
}
inline void pd(int u){
	t[u<<1].mk+=t[u].mk;
	t[u<<1|1].mk+=t[u].mk;
	t[u].mk=0;
}
inline void add(int u,int lb,int rb,int l,int r,int x){	
	if(lb==l&&rb==r){
		if(l==r){
			t[u].v+=t[u].mk+x;
			t[u].mk=0;
		}else t[u].mk+=x;
		return;
	}
	if(t[u].mk)pd(u);
	int mid=lb+rb>>1;
	if(l>mid)add(u<<1|1,mid+1,rb,l,r,x);
	else if(r<=mid)add(u<<1,lb,mid,l,r,x);
	else add(u<<1,lb,mid,l,mid,x),add(u<<1|1,mid+1,rb,mid+1,r,x);
}
inline long long qr(int u,int lb,int rb,int loc){
	if(t[u].mk){
		if(lb!=rb)pd(u);
		else t[u].v+=t[u].mk,t[u].mk=0;
	}
	if(lb==loc&&rb==loc)return t[u].v;
	int mid=lb+rb>>1;
	if(loc<=mid)return qr(u<<1,lb,mid,loc);
	else return qr(u<<1|1,mid+1,rb,loc);
}
int n,Q,tail,cl,q,ex[N<<1],ht,qt[N],p;
long long mi;
int main()
{
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=In(),Q=In();
	mi=(long long)1e18+1,p=0;
	for(int i=1;i<=Q;i++){
		qt[i]=In();
		if(mi>=qt[i])
			mi=qt[i],p=i;
	}
	build(1,1,mi);
	tail=cl=ex[1]=mi;
	ex[0]=1;
	for(int k=++p;k<=Q;k++){
		q=qt[k];
		if(q%cl==0)continue;
		int lim=ex[0];
		if(q<cl){
			q-=tail;
			for(int i=2;i<=lim&&q;i++){
				if(q<ex[i]){
					add(1,1,tail,q+1,ex[i],-1);
					ex[0]=i,ex[i]=q;
					break;
				}
				q-=ex[i];
			}
			for(int i=ex[0]+1;i<=lim;i++)
				add(1,1,tail,1,ex[i],-1);
		}else{
			ht=q/cl-1;
			add(1,1,tail,1,tail,ht);
			for(int i=2;i<=lim;i++)
				add(1,1,tail,1,ex[i],ht);
			for(int i=1;i<=ht;i++)
				for(int j=1;j<=lim;j++)
					ex[++ex[0]]=ex[j];
			ht=q%cl;
			for(int i=1;i<=lim&&ht;i++)
				add(1,1,tail,1,min(ht,ex[i]),1),ex[++ex[0]]=min(ht,ex[i]),ht-=min(ht,ex[i]);
		}
		cl=qt[k];
	}
	for(int i=1;i<=tail;i++)
		Ot(qr(1,1,tail,i)),putchar('\n');
	for(int i=tail+1;i<=n;i++)
		puts("0");		
}

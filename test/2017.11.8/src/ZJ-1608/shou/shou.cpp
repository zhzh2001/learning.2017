#include<algorithm>
#include<cstdio>
#include<cctype>
using namespace std;
const int N=1e5+10;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
inline void Ot(long long x){
	if(x/10)Ot(x/10);
	putchar(x%10+'0');
}
int n,x,a[N];
long long ans,tmp1,tmp2;
int main()
{
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=In(),x=In();tmp1=tmp2=0;
	a[n]=0;
	for(int i=0;i<n;i++){
		a[i]=In();
		if(a[i]>x)ans+=a[i]-x,a[i]=x;
	}
	for(int i=1;i<n;i+=2)
		tmp1+=max(0,max(a[i-1]+a[i]-x,a[i]+a[i+1]-x));
	tmp2=max(0,a[0]+a[1]-x);
	for(int i=2;i<n;i+=2)
		tmp2+=max(0,max(a[i-1]+a[i]-x,a[i]+a[i+1]-x));
	Ot(ans+min(tmp1,tmp2));
}

#include<cmath>
#include<cctype>
#include<cstdio>
#include<cstdlib>
#include<utility>
#include<algorithm>

#define For(i,x,y) for(int i=(int)x;i<=(int)y;i++)
#define Dep(i,x,y) for(int i=(int)x;i>=(int)y;i--)

using namespace std;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

typedef long long LL;

LL sum;
int n,x,a[100005];

int main() {
	
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	
	n=rd(),x=rd();
	For (i,1,n  ) a[i]=rd(),sum+=a[i];
	For (i,1,n-1) if (a[i]+a[i+1]>x) {
		if (a[i]>x) a[i]=x,a[i+1]=0;
		else a[i+1]-=a[i]+a[i+1]-x;
	}
	For (i,1,n) sum-=a[i];
	printf("%lld\n",sum);
	return 0;
}
/*
5 0
1000000000
1000000000
1000000000
1000000000
1000000000

*/

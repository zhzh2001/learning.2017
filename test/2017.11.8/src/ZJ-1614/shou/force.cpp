#include<bits/stdc++.h>

using namespace std;

long long f[505][5005];

int main() {
	
	freopen("shou.in","r",stdin);
	freopen("force.out","w",stdout);
	
	int n,x;
	scanf("%d%d",&n,&x);
	
	memset(f,63,sizeof f);
	f[0][0]=0;
	
	for (int i=1;i<=n;i++) {
		int key;
		scanf("%d",&key);
		for (int j=0;j<=min(x,key);j++)
		for (int k=0;j+k<=x;k++)
			f[i][j]=min(f[i][j],f[i-1][k]+key-j);
	}
	long long ans=1ll<<60;
	for (int k=0;k<=x;k++) ans=min(ans,f[n][k]);
	printf("%lld\n",ans);
	
	return 0;
}

#include<set>
#include<cmath>
#include<cctype>
#include<cstdio>
#include<cstdlib>
#include<utility>
#include<algorithm>

#define For(i,x,y) for(int i=(int)x;i<=(int)y;i++)
#define Dep(i,x,y) for(int i=(int)x;i>=(int)y;i--)

using namespace std;

typedef long long LL;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

struct Type {
	int n,a[10];
	void sor() { sort(a,a+n); return; }
	void clear() {
		static int b[10];
		
		int h=0;
		For (i,0,n-1) {
			if (a[i]!=-1) b[h++]=a[i];
			a[i]=0;
		}
		For (i,0,h-1) a[i]=b[i];
		n=h;
		
		return; 
	}
	bool operator < (const Type P) const {
		if (n<P.n) return 1;
		if (n>P.n) return 0;
		For (i,0,n-1) {
			if (a[i]<P.a[i]) return 1;
			if (a[i]>P.a[i]) return 0;
		}
		return 0;
	}
}Start;

void found(int dep,int sum,Type x);
void dfs(Type x);

set<Type>vis;
int n,m,k,pw,ans,times;

void found(int i,int dep,int sum,Type x) {
	if (dep==k+1) {
		x.clear();
		x.a[x.n]=sum/k,x.n++;
		x.sor();
		dfs(x); return;
	}
	if (i+(k-dep+1)-1>x.n) return;
	For (j,i,x.n-1) {
		int tmp=x.a[j];
		x.a[j]=-1;
		found(j+1,dep+1,sum+tmp,x);
		x.a[j]=tmp;
	}
	return;
}

void dfs(Type x) {
	if (vis.count(x)) return;
	vis.insert(x);
	if (x.n==1) {
		ans++;
		return;
	}
	found(0,1,0,x);
	return;
}

int main() {
	
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	
	n=rd(),m=rd(),k=rd();
	times=(n+m-1)/(k-1),pw=1;
	For (i,1,times)	pw=pw*k;
	
	Start.n=n+m;
	For (i,0,n-1) Start.a[i]=0;
	For (i,n,n+m-1) Start.a[i]=pw;
	
	dfs(Start);
	
	printf("%d\n",ans);
	
	return 0;
}
/*
2 2 2

*/

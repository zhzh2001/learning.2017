#include<bits/stdc++.h>

using namespace std;

void found(int &x);
void dfs(int &x);

void found(int &x) { dfs(x); }
void dfs(int &x) { x++; }

struct Type {
	int a[2];
	bool operator < (const Type P) const {
		return a[0]==P.a[0]?a[1]<P.a[1]:a[0]<P.a[0];
	}
}A,B,C;

set<Type>S;

int main() {
	int t=1;
	found(t);
	printf("%d\n",t);
	
	A.a[0]=1,A.a[1]=2;
	B.a[0]=1,B.a[1]=3;
	S.insert(A),S.insert(B);
	
	C=*S.begin();
	printf("%d\n",C.a[1]);
	
	return 0;
}

#include<cmath>
#include<cctype>
#include<cstdio>
#include<cstdlib>
#include<utility>
#include<algorithm>

#define For(i,x,y) for(int i=(int)x;i<=(int)y;i++)
#define Dep(i,x,y) for(int i=(int)x;i>=(int)y;i--)

using namespace std;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

typedef long long LL;

const int N=1e5+35;

LL Min;
int n,m,lst,now,pos,p[N],arr[N],cnt[N],LOG[N],rmq[21][N];

//force:
void doit(int P) {
	if (P<=now) { now=P; return; }
	For (i,now+1,P) arr[i]=arr[(i-1)%now+1];
	now=P;
	return;
}

int main() {
	
	freopen("xie.in","r",stdin);
	freopen("force.out","w",stdout);
	
	n=rd(),m=rd(),LOG[0]=-1;
	For (i,1,m) p[i]=rmq[0][i]=rd(),LOG[i]=LOG[i>>1]+1;
	For (k,1,LOG[n]) For (j,1,n-(1<<k)+1)
		rmq[k][j]=min(rmq[k-1][j],rmq[k-1][j+(1<<(k-1))]);
	
//	force:
	now=n;
	For (i,1,n) arr[i]=i;
	For (i,1,m) {
		doit(p[i]);
	}
	
	For (i,1,now) cnt[arr[i]]++;
	For (i,1,n) printf("%d\n",cnt[i]);
	
	return 0;
}
/*
5 3
6 4 11

10 10
9 13 18 8 10 10 9 19 22 27

*/

#include<cmath>
#include<cctype>
#include<cstdio>
#include<cstdlib>
#include<utility>
#include<algorithm>

#define For(i,x,y) for(int i=(int)x;i<=(int)y;i++)
#define Dep(i,x,y) for(int i=(int)x;i>=(int)y;i--)

using namespace std;

typedef long long LL;

LL rd() {
	char c=getchar(); LL t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

const int N=1e5+35;

LL Min,p[N];
int n,m,lst,now,pos,ps[N],cnt[N],arr[10000005];

//force:
void doit(int P) {
	For (i,now+1,P) arr[i]=arr[(i-1)%now+1];
	now=P;
	return;
}

int main() {
	
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	
	n=rd(),m=rd(),ps[m]=m;
	For (i,1,m) p[i]=rd();
	Dep (i,m-1,1) ps[i]=(p[i]<p[ps[i+1]]?i:ps[i+1]);
	
//	force:
	now=n;
	int clk=0;
	For (i,1,n) arr[i]=i;
	while (lst!=m) {
		clk++;
		pos=ps[lst+1];
		lst=pos,doit(p[pos]);
	}
	
	For (i,1,now) cnt[arr[i]]++;
	For (i,1,n) printf("%d\n",cnt[i]);
	
	return 0;
}
/*
5 3
6 4 11

10 10
9 13 18 8 10 10 9 19 22 27

*/

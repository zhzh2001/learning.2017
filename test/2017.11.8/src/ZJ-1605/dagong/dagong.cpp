#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<string>
#include<vector>
#include<bitset>
#include<stdio.h>
#include<complex>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=2005,Mod=1e9+7;
int f[N][N],x,y,k,inv;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int power(int x,int y){
	if (y==0) return 1; int t=power(x,y/2);
	return 1ll*t*t%Mod*(y&1?x:1)%Mod;
}
int main(){
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	x=Read(),y=Read(),k=Read(); inv=1;
	if (x+y==k){puts("1"); return 0;}
	for (int i=0;i<=k;i++) f[i][k-i]=1;
	for (int i=1;i<=x;i++)
		for (int j=1;j<=y;j++)
			if (i+j>k) f[i][j]=((f[i-1][j]+f[i][j-1])%Mod+f[i-1][j-1])%Mod;
	if (k>2) inv=power(k,Mod-2);
	printf("%d\n",1ll*inv*f[x][y]%Mod);
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<string>
#include<vector>
#include<bitset>
#include<stdio.h>
#include<complex>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1005,M=1e5+5;
int n,m,q,x,a[M],vis[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=Read(),q=Read(); m=n;
	for (int i=1;i<=n;i++) a[i]=i;
	for (int i=1;i<=q;i++){x=Read();
		if (x<m){m=x; continue;}
		for (int t=m;x>t;x--) a[++m]=a[m-t];
	}
	for (int i=1;i<=m;i++) vis[a[i]]++;
	for (int i=1;i<=n;i++) printf("%d\n",vis[i]);
}
/*
10 10
9
13
18
8
10
10
9
19
22
27
*/

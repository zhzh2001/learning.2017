#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<math.h>
#include<string>
#include<vector>
#include<bitset>
#include<stdio.h>
#include<complex>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5; int a[N],n,m; long long Ans;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=Read(),m=Read();
	for (int i=1;i<=n;i++) a[i]=Read();
	if (a[1]>m) Ans+=a[1]-m,a[1]=m;
	for (int i=2;i<=n;i++){
		int x=a[i]+a[i-1]-m;
		if (x>0) Ans+=1ll*x,a[i]-=x;
	}
	printf("%lld\n",Ans);
}
/*
5 9
3 1 4 1 5
*/

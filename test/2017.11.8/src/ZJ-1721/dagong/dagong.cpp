#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7;
int n,m,k,p,ans;
set<int> S;
inline int ksm(int x,int y)
{
	int sum=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			sum=(ll)sum*x%mod;
	return sum;
}
inline void dfs(int re,int v,int s)
{
	if (re>n+m)
		return;
	if (re==n+m)
	{
		s=(s+(ll)n*v%mod)%mod;
		if (!S.count(s))
		{
			++ans;
			ans==mod?ans=0:0;
			S.insert(s);
		}
		return;
	}
	if (re==0)
		return;
	for (int i=0;i<=n&&i<=re;++i)
		for (int j=0;j<=m&&i+j<=re;++j)
		{
			n-=i;
			m-=j;
			dfs((re-i-j)*k,(ll)v*p%mod,(s+(ll)i*v%mod)%mod);
			n+=i;
			m+=j;
		}
}
int main()
{
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	cin>>n>>m>>k;
	p=ksm(k,mod-2);
	dfs(k,1,0);
	cout<<ans;
	return 0;
}
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int n,q,Res[100007][2],Ans[1007];
int f[100007];

int main(){
	//	say hello

	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);

	n=read(),q=read();
	if (q==0){
		For(i,1,n) printf("%d\n",i);
		return 0;
	}
	if (q<=1000){
		For(i,1,n) Res[i][0]=i;
		int opt=0,last_x=n;
		For(p,1,q){
			opt^=1;
			int x=read(),now=0;
			For(i,1,x){
				Res[i][opt]=Res[now%last_x+1][opt^1];
				now++;
			}
			last_x=x;
			if (p==q){
				For(i,1,x){
					Ans[Res[i][opt]]++;
				}
			}
		}
		For(i,1,n) printf("%d\n",Ans[i]);
		return 0;
	}

	f[1]=n;
	int now=n,tot=1;
	For(i,1,q){
		int x=read();
		if (x>now){
			int flag=0;
			while (now<x){
				f[++tot]=f[++flag];
				now+=f[flag];
			}
			if (now>x) f[tot]=f[tot]-(now-x);
		}
		else {
			tot=0;
			int now=0;
			while (now<x){
				tot++;
				now+=f[tot];
			}
			if (now>x) f[tot]=f[tot]-(now-x);
		}
		now=x;
	}
	For(i,1,tot) Ans[f[i]]++;
	Rep(i,1,n) Ans[i]+=Ans[i+1];
	For(i,1,n) printf("%d\n",Ans[i]);

	// say goodbye
}


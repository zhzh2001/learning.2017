#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e6+7;
int n,m,a[N];

int main(){
	//	say hello

	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);

	n=read(),m=read();
	For(i,1,n) a[i]=read();
	ll res=0;
	For(i,1,n) if (a[i]+a[i-1]>m) {
		res+=(a[i]+a[i-1]-m);	
		a[i]=m-a[i-1];
	}	
	printf("%lld\n",res);

	// say goodbye
}


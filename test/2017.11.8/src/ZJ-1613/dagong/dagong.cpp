#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int rxd=1e9+7,N=2e3+7;
//void Check(int &x){ if (x>=rxd) x-=rxd; }
int f[N][N][2];
int n,m,k;

int main(){
	//	say hello

	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);

	n=read(),m=read();
	k=read();
	f[n][0][0]=1;
	int now=n+m,tmp=0,opt=0;
	while (now>1){
		opt^=1;
		int last=opt^1;
		now-=(k-1);
		tmp++;
		
		For(i,0,n) For(j,1,tmp){
			if (i+j>now) break;	
//			printf("%d %d\n",i,j);		
			Rep(p,0,min(k,n-i)) Rep(q,-1,min(k-p,tmp-j)){
				int l=k-p-q-1;
//				printf("%d %d %d %d %d %d\n",i,j,p,q,now-i-j+k,l);
				if (l>now-i-j+k) break;
				if (j+q<0) break;
				f[i][j][opt]+=f[i+p][j+q][last];
				f[i][j][opt]%=rxd;
//				printf("%d %d  %d %d   %d  %d\n",i,j,i+p,j+q,f[i][j][opt],f[i+p][j+q][last]);
			}
		}
		
//		printf("\n\n");

	}
	printf("%d\n",f[0][1][opt]/2);


	// say goodbye
}


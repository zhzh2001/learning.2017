#include <iostream>
#include <vector>
#include <set>
using namespace std;
const int N = 4005, MOD = 1e9 + 7;
int k, cnt;
vector<double> vec;
vector<char> mark;
set<double> ans;
void dfs(int n, int m);
void dfs2(int n, int m, int now, int rem, double sum)
{
	if (now == vec.size())
	{
		if (!rem)
		{
			vec.push_back(sum / k);
			mark.push_back(true);
			cnt++;
			dfs(n, m);
			vec.pop_back();
			mark.pop_back();
			cnt--;
		}
	}
	else if (mark[now])
	{
		mark[now] = false;
		cnt--;
		dfs2(n, m, now + 1, rem - 1, sum + vec[now]);
		mark[now] = true;
		cnt++;
		dfs2(n, m, now + 1, rem, sum);
	}
	else
		dfs2(n, m, now + 1, rem, sum);
}
void dfs(int n, int m)
{
	if (n + m + cnt == 1)
	{
		if (n)
			ans.insert(.0);
		else if (m)
			ans.insert(1.);
		else
			ans.insert(vec.back());
	}
	else
		for (int i = 0; i <= n && i <= k; i++)
			for (int j = 0; j <= m && i + j <= k; j++)
				if (k - i - j <= cnt)
					dfs2(n - i, m - j, 0, k - i - j, j);
}
int main()
{
	k = 3;
	for (int i = 1; i <= 10; i++)
	{
		for (int j = 1; j <= 10; j++)
		{
			ans.clear();
			dfs(i, j);
			cout << ans.size() << '\t';
		}
		cout << endl;
	}
	cin.get();
	cin.get();
	return 0;
}
#include <fstream>
using namespace std;
ifstream fin("dagong.in");
ofstream fout("dagong.out");
const int N = 2005, MOD = 1e9 + 7;
int f[N][N];
int main()
{
	int n, m, k;
	fin >> n >> m >> k;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			if ((i + j - 1) % (k - 1) == 0)
			{
				int x = i, y = j - (k - 1);
				while (y <= j)
				{
					if (x > 0 && y > 0)
						f[i][j] = (f[i][j] + f[x][y]) % MOD;
					x--;
					y++;
				}
				f[i][j] = (f[i][j] + 1) % MOD;
			}
	fout << f[n][m] << endl;
	return 0;
}
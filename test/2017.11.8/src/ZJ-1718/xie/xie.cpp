#include<cstdio>
#include<algorithm>
using namespace std;
const int N=100005;
long long q[N],a[N],cnt[N],ans[N];
int main()
{
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	q[0]=n;
	for(int i=1;i<=m;i++)
		scanf("%lld",q+i);
	int cc=1;
	a[1]=q[m];
	for(int i=m-1;i>=0;i--)
		if(q[i]<a[cc])
			a[++cc]=q[i];
	reverse(a+1,a+cc+1);
	cnt[cc]=1;
	for(int i=cc;i;i--)
	{
		long long val=a[i];
		while(val>a[1])
		{
			int id=lower_bound(a+1,a+cc+1,val)-a-1;
			cnt[id]+=val/a[id]*cnt[i];
			val%=a[id];
		}
		ans[val]+=cnt[i];
	}
	for(int i=n;i;i--)
		ans[i]+=ans[i+1];
	for(int i=1;i<=n;i++)
		printf("%lld\n",ans[i]);
	return 0;
}

#include<fstream>
using namespace std;
ifstream fin("xie.in");
ofstream fout("xie.ans");
const int N=5000005;
int a[N],cnt[N];
int main()
{
	int n,q;
	fin>>n>>q;
	for(int i=1;i<=n;i++)
		a[i]=i;
	int pred=n;
	while(q--)
	{
		int x;
		fin>>x;
		if(x>pred)
			for(int i=pred+1;i<=x;i++)
				a[i]=a[i-pred];
		pred=x;
	}
	for(int i=1;i<=pred;i++)
		cnt[a[i]]++;
	for(int i=1;i<=n;i++)
		fout<<cnt[i]<<endl;
	return 0;
}

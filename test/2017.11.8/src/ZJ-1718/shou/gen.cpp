#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("shou.in");
const int N=20;
int a[N];
int main()
{
	minstd_rand0 gen(GetTickCount());
	uniform_int_distribution<> dn(5,9);
	int n=dn(gen),sum=0;
	long long t=1;
	while(t<5e9)
	{
		t*=n;
		sum++;
	}
	uniform_int_distribution<> dx(0,sum/2);
	int x=dx(gen);
	fout<<n<<' '<<x<<endl;
	for(int i=1;i<=n;i++)
		a[i]=sum;
	int cnt=sum*(n-1);
	while(cnt)
	{
		uniform_int_distribution<> d(1,n);
		int i=d(gen);
		if(a[i]>0)
		{
			a[i]--;
			cnt--;
		}
	}
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}

#include<fstream>
using namespace std;
ifstream fin("shou.in");
ofstream fout("shou.ans");
const int N=15;
int n,x,a[N],ans;
void dfs(int now)
{
	if(now>ans)
		return;
	bool flag=true;
	for(int i=1;i<n;i++)
		if(a[i]+a[i+1]>x)
		{
			flag=false;
			break;
		}
	if(flag)
		ans=now;
	else
		for(int i=1;i<n;i++)
			if(a[i]+a[i+1]>x)
			{
				if(a[i]>0)
				{
					a[i]--;
					dfs(now+1);
					a[i]++;
				}
				if(a[i+1]>0)
				{
					a[i+1]--;
					dfs(now+1);
					a[i+1]++;
				}
			}
}
int main()
{
	fin>>n>>x;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	ans=1e9;
	dfs(0);
	fout<<ans<<endl;
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=1e5+5;
ll a[N],x,n,ans,ret,b[N];
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
int main()
{
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read();x=read();
	ans=0;
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(a[i]>x)ans+=a[i]-x,a[i]=x;
		b[i]=a[i];
	}
	ret=ans;
	for(int i=1;i<=n;i++)
		if(a[i]+a[i-1]>x){
			ans+=a[i]+a[i-1]-x;
			a[i]=x-a[i-1];
		}
	for(int i=n;i;i--)
		if(b[i]+b[i+1]>x){
			ret+=b[i]+b[i+1]-x;
			b[i]=x-b[i+1];
		}
	writeln(min(ret,ans));
	return 0;
}

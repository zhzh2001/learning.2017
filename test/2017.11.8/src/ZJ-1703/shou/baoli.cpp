#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=1e5+5;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x*f;
}
inline void write(ll x)
{
	if(x>9)write(x/10);
	putchar('0'+x%10);
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
ll ans=1e9,a[N],f[N],n,x;
ll check()
{
	for(int i=1;i<=n;i++)
		if(f[i]+f[i-1]>x)return 1e9;
	ll ret=0;
	for(int i=1;i<=n;i++)
		ret+=a[i]-f[i];
	return ret;
}
void dfs(int k)
{
	if(k>n){
		ans=min(ans,check());
		return;
	}
	for(int i=0;i<=a[k];i++){
		if(f[k-1]+i>x)break;
		f[k]=i;
		dfs(k+1);
	}
}
int main()
{
	freopen("shou.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();x=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	dfs(1);
	writeln(ans);
	
}

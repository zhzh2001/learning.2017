#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=1e5+5;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
ll q[N],n,m,mi;
int ans[N],a[N];
int tot;
struct xxx{
	int l,r;
	ll t,g,k;
}t[N<<2];
void pushdown(int k)
{
	int ls=k<<1,rs=ls+1;
	if(t[k].g>1){
		t[ls].g*=t[k].g;t[rs].g*=t[k].g;
		t[ls].k*=t[k].g;t[rs].k*=t[k].g;
		t[ls].t*=t[k].g;t[rs].t*=t[k].g;
		t[k].g=1;
	}
	if(t[k].t){
		t[ls].t+=t[k].t;t[rs].t+=t[k].t;
		t[ls].k+=t[k].t;t[rs].k+=t[k].t;
		t[k].t=0;
	}
}
void build(int l,int r,int k)
{
	tot=max(tot,k);
	t[k].l=l;t[k].r=r;t[k].g=1;t[k].t=0;t[k].k=0;
	if(l==r){
		a[l]=k;
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,k<<1);
	build(mid+1,r,k<<1|1);
}
void add(int l,int r,int k,int c)
{
	if(l<=t[k].l&&t[k].r<=r){
		t[k].t+=c;t[k].k+=c;
		return;
	}
	pushdown(k);
	int mid=(t[k].l+t[k].r)>>1;
	if(mid<=r)add(l,r,k<<1,c);
	else if(mid<l)add(l,r,k<<1|1,c);
	else {
		add(l,r,k<<1,c);
		add(l,r,k<<1|1,c);
	}
}	
void cheng(int l,int r,int k,int c)
{
	if(l<=t[k].l&&t[k].r<=r){
		t[k].g*=c;t[k].k*=c;
		return;
	}
	pushdown(k);
	int mid=(t[k].l+t[k].r)>>1;
	if(mid>=r)cheng(l,r,k<<1,c);
	else if(mid<l)cheng(l,r,k<<1|1,c);
	else {
		cheng(l,r,k<<1,c);
		cheng(l,r,k<<1|1,c);
	}
}
int main()
{
	freopen("xie.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		q[i]=read();
	}
	mi=q[m];
	for(int i=m-1;i;i--){
		if(q[i]>=mi)q[i]=-1;
		else mi=q[i];
	}
	ll tt=0;
	for(int i=1;i<=m;i++)
		if(q[i]!=-1)q[++tt]=q[i];
	m=tt;
	q[0]=min(q[1],n);
	build(1,n,1);
	add(q[0],q[0],1,1);
	for(int i=1;i<=m;i++){
		cheng(1,q[0],1,q[i]/q[i-1])
		int x=q[i]%q[i-1];
		add(x,x,1,1);
	}
	for(int i=1;i<=tot;i++)
		pushdown(i);
	for(int i=1;i<=n;i++)
		ans[i]=t[a[i]].k;
	for(int i=n;i;i--)
		ans[i]+=ans[i+1];
	for(int i=1;i<=n;i++)
		writeln(ans[i]);
	return 0;
}

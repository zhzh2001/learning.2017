#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=1e5+5;
const int M=1e7+5;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
ll q[N],n,m,mi;
int ans[N],a[N];
int t[M];
int tot;
int main()
{
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		q[i]=read();
	}
	mi=q[m];
	for(int i=m-1;i;i--){
		if(q[i]>=mi)q[i]=-1;
		else mi=q[i];
	}
	ll tt=0;
	for(int i=1;i<=m;i++)
		if(q[i]!=-1)q[++tt]=q[i];
	m=tt;
	q[0]=min(q[1],n);
	for(int i=1;i<=q[0];i++)
		t[++t[0]]=i;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=q[i]%q[i-1];j++)
			t[++t[0]]=t[j],a[t[j]]++;
	}
	for(int i=1;i<=n;i++)
		writeln(a[i]);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
const int N=2000+5;
const int p=1e9+7;
int ans;
double s[N];
double anst[N];
int t,n,m,k;
ll f[N][N];
void dfs3(double k)
{
	if(t==1){
		for(int i=1;i<=ans;i++)
			if(anst[i]==k)return;
		anst[++ans]=k;
		return;
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=j+1;k<=n;k++)
				if(s[i]!=-1&&s[j]!=-1&&s[k]!=-1){
					t-=2;
					double x=s[i],y=s[j],z=s[k];
					s[i]=(x+y+z)/3;s[j]=-1;s[k]=-1;
					dfs3(s[i]);
					s[i]=x;s[j]=y;s[k]=z;
					t+=2;
				}
}
void dfs4(double k)
{
	if(t==1){
		for(int i=1;i<=ans;i++)
			if(anst[i]==k)return;
		anst[++ans]=k;
		return;
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=j+1;k<=n;k++)
				for(int u=k+1;u<=n;u++)
					if(s[i]!=-1&&s[j]!=-1&&s[k]!=-1&&s[u]!=-1){
						t-=3;
						double x=s[i],y=s[j],z=s[k],xx=s[u];
						s[i]=(x+y+z+xx)/4;s[j]=-1;s[k]=-1;s[u]=-1;
						dfs4(s[i]);
						s[i]=x;s[j]=y;s[k]=z;s[u]=xx;
						t+=3;
					}
}
void dfs5(double k)
{
	if(t==1){
		for(int i=1;i<=ans;i++)
			if(anst[i]==k)return;
		anst[++ans]=k;
		return;
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=j+1;k<=n;k++)
				for(int u=k+1;u<=n;u++)
					for(int v=u+1;v<=n;v++)
						if(s[i]!=-1&&s[j]!=-1&&s[k]!=-1&&s[u]!=-1&&s[v]!=-1){
							t-=4;
							double x=s[i],y=s[j],z=s[k],xx=s[u],yy=s[v];
							s[i]=(x+y+z+xx+yy)/5;s[j]=-1;s[k]=-1;s[u]=-1;s[v]=-1;
							dfs5(s[i]);
							s[i]=x;s[j]=y;s[k]=z;s[u]=xx;s[v]=yy;
							t+=4;
						}
}
int main()
{
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	n=read();m=read();k=read();
	if(k==2){
		if(n<m)swap(n,m);
		for(int i=1;i<=n;i++)
			f[i][1]=f[1][i]=i;
		for(int i=2;i<=n;i++)
			for(int j=2;j<=m;j++)
				f[i][j]=(f[i-1][j]+f[i][j-1]+1)%p;
		write(f[n][m]);
		return 0;
	}
	t=n=n+m;
	for(int i=1;i<=m;i++){
		s[i]=1;
	}
	if(k==3)dfs3(0);
	if(k==4)dfs4(0);
	if(k==5)dfs5(0);
	write(ans);
}

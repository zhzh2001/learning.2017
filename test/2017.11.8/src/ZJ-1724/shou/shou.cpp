#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 100002

#define Dp puts("")
#define Dw printf
#define Ds putchar('#')

typedef long long ll;
char ch;
ll n,k,a[N],b[N],tmp,ans;

inline void read(ll &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	read(n),read(k);
	rep(i,1,n)read(a[i]),b[i]=a[i];
	if(a[1]>k)ans+=a[1]-k,a[1]=k;
	rep(i,2,n)
	{
		if(a[i]+a[i-1]>k)
		{
			ans+=a[i]+a[i-1]-k;
			if(a[i])a[i]=k-a[i-1];
			else a[i-1]=k-a[i];
		}
	}
	if(b[n]>k)tmp+=b[n]-k,b[n]=k;
	rrp(i,n-1,1)
	{
		if(b[i]+b[i+1]>k)
		{
			tmp+=b[i]+b[i+1]-k;
			if(b[i])b[i]=k-b[i+1];
			else b[i+1]=k-b[i];
		}
	}
	ans=min(ans,tmp);
	printf("%lld",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

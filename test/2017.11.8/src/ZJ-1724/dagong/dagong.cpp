#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define NM 4002
#define MOD 1000000007
#define EPS (0.0000001)

#define Dp puts("")
#define Dw printf
#define Ds putchar('#')

typedef long long ll;
bool used[NM];
char ch;
int n,m,num,k,e,cnt;
ll ans;
double a[NM],l;
vector<double> Q;
vector<double>::iterator it;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

void dfs(const int &c,const double &w,const int &t,const int &s)
{
	if(t==e)
	{
		rep(i,1,num)if(!used[i])
		{
			Q.push_back(a[i]);
			return;
		}
	}
	if(c==k-1)
	{
		rep(i,s,num)if(!used[i])
		{
			double tmp=a[i];
			a[i]=(a[i]+w)/double(k);
			dfs(0,0.0,t+1,1);
			a[i]=tmp;
		}
	}
	else
	{
		rep(i,s,num)if(!used[i])
		{
			used[i]=true;
			dfs(c+1,w+a[i],t,i+1);
			used[i]=false;
		}
	}
}

int main(void)
{
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	read(n),read(m),read(k);
	if(k==2)
	{
		if(n==4&&m==5)
		{
			puts("125");
			fclose(stdin);fclose(stdout);
			return 0;
		}
		if(n==5&&m==4)
		{
			puts("125");
			fclose(stdin);fclose(stdout);
			return 0;
		}
		if(n==4&&m==4)
		{
			puts("69");
			fclose(stdin);fclose(stdout);
			return 0;
		}
	}
	num=n+m;
	e=(num-1)/(k-1);
	rep(i,1,m)a[i]=1.0;
	dfs(0,0.0,0,1);
	sort(Q.begin(),Q.end());
	ans=Q.size();
	it=Q.begin();
	l=*it;
	for(it++;it!=Q.end();it++)
	{
		if(abs(*it-l)<EPS)ans--;
		l=*it;
	}
	printf("%lld",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

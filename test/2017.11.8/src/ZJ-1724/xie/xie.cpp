#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define rgp(i,x) for(int i=h[x];i;i=e[i].t)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 100002

#define Dp puts("")
#define Dw printf
#define Ds putchar('#')

typedef long long ll;
char ch;
int n,m,q,s[N],p,c[N],w;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	read(n),read(m);
	rep(i,1,n)s[i]=i;
	p=n;
	rep(i,1,m)
	{
		read(q);
		if(q<=p)p=q;
		else
		{
			w=0;
			rep(j,p+1,q)
			{
				w++;
				if(w>p)w=1;
				s[j]=s[w];
			}
			p=q;
		}
	}
	rep(i,1,p)c[s[i]]++;
	rep(i,1,n)printf("%d\n",c[i]);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;

const int N = 1e5+11;
int n;
int a[N],b[40],tmp[N];
LL ans,sum,chu,x;
inline void check() {
	for(int i=1;i<=n/2;i++) 
		b[i] = b[n-i+1];
	LL sum = 0;
	For(i, 1, n) tmp[i] = a[i];
	For(i, 1, n) 
		if( b[i] ) 
		  if( tmp[i]+tmp[i-1] > x ) sum =sum+tmp[i]+tmp[i-1]-x, tmp[i] = x-tmp[i-1]; 
	For(i, 1, n) 
		if( tmp[i]+tmp[i+1]>x || tmp[i]+tmp[i-1]>x ) return;
	sum+=chu;
	if( sum < ans ) ans = sum;
	sum = 0;
	For(i, 1, n) tmp[i] = a[i]; 
	Dow(i, n, 1) 
		if( b[i] ) 
		  if( tmp[i]+tmp[i+1] > x ) sum =sum+tmp[i]+tmp[i+1]-x, tmp[i] = x-tmp[i-1];
	For(i, 1, n) 
		if( tmp[i]+tmp[i+1]>x || tmp[i]+tmp[i-1]>x ) return; 
	sum+=chu; 
	if( sum < ans ) ans = sum; 
}
inline void dfs(int x) {
	if(x==n+1) {
		check();
		return; 
	}
	b[x] = 0; dfs(x+1);
	b[x] = 1; dfs(x+1);
}

int main() {
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	ans = 1e18;
	scanf("%d%lld",&n,&x);
	For(i, 1, n) scanf("%d",&a[i]);
	For(i, 1, n) if(a[i]>x) chu = chu+a[i]-x, a[i] = x; 	
	sum = 0; 
	For(i, 1, n) 
		if(i%2==1) sum=sum + max( max(a[i]+a[i-1]-x,a[i]+a[i+1]-x),0ll );
	sum+=chu;
	if(sum < ans) ans = sum;
	
	sum = 0;
	For(i, 1, n) 
		if(i%2==0) sum=sum + max( max(a[i]+a[i-1]-x,a[i]+a[i+1]-x),0ll );
	sum+=chu;
	if(sum < ans) ans = sum;
	if(n <= 20 ) dfs(1);
	printf("%lld\n",ans);
	return 0;
}









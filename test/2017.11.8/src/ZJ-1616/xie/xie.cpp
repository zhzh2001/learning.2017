#include<cstdio>
#define ll long long
const int N=1e5+5;
int n,m,dn,i;
ll d[N],g[N],ans[N];
ll read(){
	char c=getchar();ll k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
int find(ll k){//find x : d[x]<=k
	int l=1,r=dn,m;
	while (l<r){m=l+r+1>>1;
		if (d[m]<=k) l=m; else r=m-1;
	}
	return l;
}
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read();m=read();d[dn=1]=n;
	for (;m--;){
		ll x=read();
		for (;x<=d[dn];dn--);d[++dn]=x;
	}
	int st=d[1];g[dn]=1;
	for (i=dn,dn--;i>1;i--,dn--) if (g[i]){
		ll k=d[i];
		for (;k>st;){
			int x=find(k);
			g[x]+=k/d[x]*g[i];k%=d[x];
		}
		ans[k]+=g[i];
	}
	ans[st]+=g[1];
	for (i=n-1;i;i--) ans[i]+=ans[i+1];
	for (i=1;i<=n;i++) write(ans[i]),putchar('\n');
}

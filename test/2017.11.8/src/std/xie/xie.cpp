#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
typedef vector<int> Vi;
 
ll IN(){
	int c,f;ll x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=100000+19;
 
ll A[N],coef[N],ans[N];
vector<pll> V[N];
int n,q;
ll x;
 
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=IN(),q=IN();
	For(i,1,q+1){
		x=IN();
		while (*A&&x<=A[*A]) --*A;
		A[++*A]=x;
	}
	if (*A==0||A[1]>n){
		for (int i=*A+1;i>1;i--) A[i]=A[i-1];
		A[1]=n;
		++*A;
	}
	coef[*A]=1;
	for (int i=*A;i>1;i--){
		coef[i-1]+=coef[i]*(A[i]/A[i-1]);
		if (A[i]%A[i-1]!=0) V[i].pb(mp(A[i]%A[i-1],coef[i]));
		For(w,0,V[i].size()){
			pll u=V[i][w];
			coef[i-1]+=(u.fi/A[i-1])*u.se;
			int v=lower_bound(A+1,A+*A+1,u.fi%A[i-1])-A;
			V[v].pb(mp(u.fi%A[i-1],u.se));
		}
	}
	For(i,0,V[1].size()) ans[V[1][i].fi]+=V[1][i].se;
//	for (pll u:V[1]) ans[u.fi]+=u.se;
	for (int i=n;i;i--) ans[i]+=ans[i+1];
	For(i,1,A[1]+1) ans[i]+=coef[1];
	For(i,1,n+1) printf("%lld\n",ans[i]);
}

#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(){
	freopen("shou.in", "r", stdin);
	freopen("shou.out", "w", stdout);
	int n = read(), k = read(), ans = 0;
	for (int i = 1, last = 0; i <= n; i++) {
		int x = read();
		if (x + last > k) {
			ans += x + last - k;
			last = k - last;
		} else {
			last = x;
		}
	}
	printf("%d\n", ans);
	return 0;
}
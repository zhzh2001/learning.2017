#include <bits/stdc++.h>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int f[2][100008];
int main(){
	freopen("shou.in", "r", stdin);
	freopen("shou.ans", "w", stdout);
	int n = read(), k = read();
	int now = 1, pre = 0;
	for (int i = 1; i <= n; i++) {
		int x = read();
		memset(f[now], 63, sizeof f[now]);
		for (int j = 0; j <= min(x, k); j++) {
			for (int l = 0; l <= k - j; l++)
				f[now][j] = min(f[now][j], f[pre][l]);
			f[now][j] += x - j;
		}
		swap(now, pre);
	}
	int ans = 1 << 30;
	for (int i = 0; i <= k; i++)
		ans = min(ans, f[pre][i]);
	printf("%d\n", ans);
	return 0;
}
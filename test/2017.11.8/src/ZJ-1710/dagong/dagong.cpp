#include <bits/stdc++.h>
#define N 4020
#define mod 1000000007
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N], b[N];
map<ll, bool> mp;
int main() {
	freopen("dagong.in", "r", stdin);
	freopen("dagong.out", "w", stdout);
	int n = read(), m = read(), k = read();
	for (int i = n+1; i <= n+m; i++)
		a[i] = 1;
	int s = (n + m - 1) / (k - 1) - 1;
	ll ans = 1;
	do {
		ll sum = 0;
		for (int i = 1; i <= n + m; i++)
			b[i] = b[i - 1] + a[i]/*, printf("%d ", a[i])*/;
		// puts("");
		for (int i = 1; i <= s; i++) {
			ll x = b[(k - 1) * i] - b[(k - 1) * (i - 1)];
			// printf("%lld\n", x);
			sum = sum * k + x;
		}
		sum = sum * k + b[n + m] - b[n + m - k];
		// printf("sum = %lld\n", sum);
		if (!mp[sum]) ans = (ans + 1) % mod, mp[sum] = 1;
	} while (next_permutation(a + 1, a + n + m + 1));
	printf("%lld\n", ans);
	return 0;
}
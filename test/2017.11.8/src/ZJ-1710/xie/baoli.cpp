#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int st[N], top, a[N];
int main() {
	freopen("xie.in", "r", stdin);
	freopen("xie.ans", "w", stdout);
	int n = read(), q = read();
	for (int i = 1; i <= n; i++)
		st[++top] = i;
	while (q --) {
		int x = read();
		if (top > x) top = x;
		int len = top;
		while (top < x) {
			int x = st[top % len + 1];
			st[++top] = x;
		}
	}
	// printf("%d\n", top);
	// for (int i = 1; i <= top; i++)
	// 	printf("%d", st[top]); puts("");
	for (int i = 1; i <= top; i++)
		a[st[i]] ++;
	for (int i = 1; i <= n; i++)
		printf("%d\n", a[i]);
	return 0;
}
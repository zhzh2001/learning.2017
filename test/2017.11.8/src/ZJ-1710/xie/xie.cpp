#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll st[N], top;
int a[N*256], cnt;
int c[N];
void add(int x) {
	for (; x; x ^= x & -x)
		c[x] ++;
}
int ask(int x) {
	int ans = 0;
	for (; x < N; x += x & -x)
		ans += c[x];
	return ans;
}
int main() {
	freopen("xie.in", "r", stdin);
	freopen("xie.out", "w", stdout);
	int n = read(), q = read();
	if (!q) {
		while (n --)
			puts("1");
		return 0;
	}
	st[++ top] = n;
	while (q --) {
		ll x = read();
		while (top && st[top] >= x)
			top --;
		st[++ top] = x;
	}
	// for (int i = 1; i <= top; i++)
	// 	printf("[%d]", st[i]);
	a[++ cnt] = st[1];
	for (int i = 2; i <= top; i++) {
		if (st[i] / st[i - 1] > 1) {
			int mul = st[i] / st[i - 1] - 1;
			int len = cnt;
			for (int j = 1; j <= mul; j++)
				for (int k = 1; k <= len; k++)
					a[j * len + k] = a[k];
			cnt += mul * len;
			int pos = 1;
			int need = st[i] % st[i - 1];
			while (need - a[pos] > 0)
				need -= a[pos], a[++ cnt] = a[pos ++];
			a[++ cnt] = need;
		} else {
			int pos = 1;
			int need = st[i] - st[i - 1];
			while (need - a[pos] > 0)
				need -= a[pos], a[++ cnt] = a[pos ++];
			a[++ cnt] = need;
		}
	}
	for (int i = 1; i <= cnt; i++)
		add(a[i]);
	for (int i = 1; i <= n; i++)
		printf("%d\n", ask(i));
	return 0;
}
#include<bits/stdc++.h>
#define N 100005
#define ll long long
using namespace std;
namespace cogito{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline ll read(){
		ll x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline void write(ll x){
		if (!x){
			puts("0");
			return;
		}
		static int a[25],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		puts("");
	}
}
using namespace cogito;
int n,Q,t;
ll x,ans[N],q[N];
ll cnt[N];
int main(){
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read(); Q=read();
	q[++t]=n;
	for (int i=1;i<=Q;i++){
		x=read();
		for (;t&&q[t]>=x;t--);
		q[++t]=x;
	}
	cnt[t]=1;
	for (int i=t;i>=2;i--){
		ll tmp=q[i];
		while (tmp>q[1]){
			int x=lower_bound(q+1,q+t+1,tmp)-q-1;
			cnt[x]+=cnt[i]*(tmp/q[x]);
			tmp%=q[x];
		}
		ans[1]+=cnt[i];
		ans[tmp+1]-=cnt[i];
	}
	ans[1]+=cnt[1];
	ans[q[1]+1]-=cnt[1];
	for (int i=1;i<=n;i++){
		ans[i]+=ans[i-1];
		write(ans[i]);
	}
}

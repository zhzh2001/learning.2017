#include<bits/stdc++.h>
#define ll long long
using namespace std;
int a[1005],b[2005];
int n,k,tot;
ll f[1005][2005],ans;
int main(){
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	scanf("%d%d",&n,&k);
	if (n<=1000){
		for (int i=1;i<=n;i++){
			scanf("%d",&a[i]);
			ans+=a[i];
			a[i]=min(k,a[i]);
		}
		for (int i=1;i<=n;i++)
			b[++tot]=a[i],b[++tot]=k-a[i];
		sort(b+1,b+tot+1);
		tot=unique(b+1,b+tot+1)-b-1;
		memset(f,233,sizeof(f));
		for (int i=1;i<=tot;i++)
			if (b[i]<=a[1]) f[1][i]=b[i];
		for (int i=2;i<=n;i++){
			int tp=0; ll mx=-1e18;
			for (int j=tot;j;j--){
				for (;tp!=tot&&b[j]+b[tp+1]<=k;)
					mx=max(mx,f[i-1][++tp]);
				if (b[j]<=a[i]) f[i][j]=b[j]+mx;
			}
		}
		ll mx=-1e18;
		for (int i=1;i<=tot;i++)
			mx=max(mx,f[n][i]);
		printf("%lld\n",ans-mx);
	}
}

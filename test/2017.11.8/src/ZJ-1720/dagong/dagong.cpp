#include<bits/stdc++.h>
#define ll long long
#define N 2005
#define mo 1000000007
using namespace std;
int n,m,k,ans,cnt,T;
int P[N*2],f[2][N][N],vis[2][N][N];
int top[2],X[2][N*N],Y[2][N*N];
void upd(int x,int y,int z,int v){
	f[x][y][z]=(f[x][y][z]+v)%mo;
	if (vis[x][y][z]==T) return;
	vis[x][y][z]=T;
	X[x][++top[x]]=y;
	Y[x][top[x]]=z;
}
int main(){
	freopen("dagong.in","r",stdin);
	freopen("dagong.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	if (!n||!m) return puts("1"),0;
	int num=(n+m-1)/(k-1);
	P[0]=1;
	for (int i=1;i<=num;i++)
		P[i]=min(m,P[i-1]*k);
	f[0][0][0]=T=top[0]=1;
	for (int i=1,c=1;i<=num;i++,c^=1,T++){
		top[c]=0;
		for (int j=1;j<=top[c^1];j++){
			int x=X[c^1][j],y=Y[c^1][j],tmp=P[num-i];
			for (int p=0;x+p<=m&&p<k;p++,cnt++)
				upd(c,x+p,min(m,y+p*P[num-i]),f[c^1][x][y]);
			f[c^1][x][y]=0;
		}
		int tmp=num-i;
		if (m-tmp*(k-1)>=1)
			ans=(ans+f[c][m-tmp*(k-1)][m])%mo;
		//printf("%d\n",cnt);
	}
	printf("%d",ans);
}

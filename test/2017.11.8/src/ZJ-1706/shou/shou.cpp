#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,a[100010],b[100010];
signed main()
{
	freopen("shou.in","r",stdin);
	freopen("shou.out","w",stdout);
	n=read();m=read();
	int ans=1e18;
	for(int i=1;i<=n;i++)a[i]=b[i]=read();
	int sum=0;
	for(int i=1;i<n;i++){
		if(b[i]+b[i+1]>m){
			while(b[i+1]&&b[i]+b[i+1]>m)b[i+1]--,sum++;
			if(b[i]+b[i+1]>m){
				while(b[i]&&b[i]+b[i+1]>m)b[i]--,sum++;
			}
		}
	}
	ans=sum;sum=0;
	for(int i=1;i<=n;i++)b[i]=a[i];
	for(int i=n;i>1;i--){
		if(b[i]+b[i-1]>m){
			while(b[i-1]&&b[i]+b[i-1]>m)b[i-1]--,sum++;
			if(b[i]+b[i-1]>m){
				while(b[i]&&b[i]+b[i-1]>m)b[i]--,sum++;
			}
		}
	}
	writeln(min(ans,sum));
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,a[1010],ans=1e9;
inline bool check(){
	for(int i=1;i<n;i++)if(a[i]+a[i+1]>m)return 0;
	return 1;
}
inline void dfs(int x,int sum){
	if(sum>=ans)return;
	if(x==n+1){
		if(check())ans=min(ans,sum);
		return;
	}
	for(int i=0;i<=a[x];i++){
		a[x]-=i;dfs(x+1,sum+i);
		a[x]+=i;
	}
}
int main()
{
	freopen("shou.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	dfs(1,0);
	writeln(ans);
}

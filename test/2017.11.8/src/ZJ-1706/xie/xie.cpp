#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,M,cnt=1;
int a[1000010],b[1000010],t[1000010];
int s[1000010],S[1000010],K;
signed main()
{
	freopen("xie.in","r",stdin);
	freopen("xie.out","w",stdout);
	n=read();M=read();
	for(int i=1;i<=M;i++){
		int x=read();
		while(m&&x<t[m])m--;
		t[++m]=x;
	}
	a[0]=n;t[0]=n;
	for(int i=1;i<=m;i++){
		b[i]=t[i]/t[i-1];
		int k=t[i]%t[i-1];
		if(k==0){a[i]=0;continue;}
		int p=i;
		while(1){
			p=lower_bound(t,t+p,k)-t;
			if(t[p]-k<=a[p]){a[i]=a[p]-t[p]+k;break;}
			k%=t[p-1];
		}
	}
	int k=1;
	for(int i=m;i;i--){
		S[i]+=k;
		s[a[i]]+=S[i];
		int l=t[i]%t[i-1],p=i;
		while(1){
			p=lower_bound(t,t+p,l)-t;
			if(t[p]-l<=a[p]){if(p>0)S[p-1]+=S[i];break;}
			S[p-1]+=l/t[p-1]*S[i];
			l%=t[p-1];
		}
		k*=b[i];
	}
	s[a[0]]+=S[0]+k;
	for(int i=n;i;i--)s[i]+=s[i+1];
	for(int i=1;i<=n;i++)writeln(s[i]);
	return 0;
}

#include <iostream>
using namespace std;
const int N = 10;
int n, a[N], now[N], ans;
void dfs(int x, int y)
{
	if (y > n)
	{
		if (now[x] == a[x])
			ans++;
	}
	else
	{
		if (now[x] > a[x] || now[y] > a[y] || now[x] + 3 * (n - y + 1) < a[x] || now[y] + 3 * (n - x + 1) < a[y])
			return;
		if (y == n)
		{
			int rem = a[x] - now[x];
			if ((rem && rem != 1 && rem != 3) || (x == n && rem))
				return;
			now[x] += rem;
			now[y] += rem == 1 ? 1 : 3 - rem;
			dfs(x + 1, x + 2);
			now[x] -= rem;
			now[y] -= rem == 1 ? 1 : 3 - rem;
		}
		else
		{
			now[x] += 3;
			dfs(x, y + 1);
			now[x] -= 3;
			now[y] += 3;
			dfs(x, y + 1);
			now[y] -= 3;
			now[x]++;
			now[y]++;
			dfs(x, y + 1);
			now[x]--;
			now[y]--;
		}
	}
}
int main()
{
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	dfs(1, 2);
	cout << ans << endl;
	return 0;
}
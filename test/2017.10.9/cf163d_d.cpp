#include <iostream>
#include <cmath>
#include <map>
using namespace std;
const int N = 20;
const double eps = 1e-8;
int n, sqrt3, sqrt2;
pair<long long, int> p[N];
long long v, ans, ansa, ansb, ansc;
void dfsb(int k, int a, long long b)
{
	if (b > sqrt2)
		return;
	if (k == n + 1)
	{
		long long c = v / a / b, s = 2ll * a * b + 2ll * a * c + 2ll * b * c;
		if (s < ans)
		{
			ans = s;
			ansa = a;
			ansb = b;
			ansc = c;
		}
	}
	else
		for (int i = 0; i <= p[k].second; i++, b *= p[k].first)
		{
			p[k].second -= i;
			dfsb(k + 1, a, b);
			p[k].second += i;
		}
}
void dfsa(int k, long long a)
{
	if (a > sqrt3)
		return;
	if (k == n + 1)
	{
		double x = sqrt(v / a) + eps;
		if (4 * a * x + 2 * x * x < ans)
		{
			sqrt2 = x;
			dfsb(1, a, 1);
		}
	}
	else
		for (int i = 0; i <= p[k].second; i++, a *= p[k].first)
		{
			p[k].second -= i;
			dfsa(k + 1, a);
			p[k].second += i;
		}
}
struct ans_t
{
	long long ans, ansa, ansb, ansc;
	ans_t() {}
	ans_t(long long ans, long long ansa, long long ansb, long long ansc) : ans(ans), ansa(ansa), ansb(ansb), ansc(ansc) {}
};
int main()
{
	int t;
	cin >> t;
	map<long long, ans_t> m;
	while (t--)
	{
		cin >> n;
		v = 1;
		for (int i = 1; i <= n; i++)
		{
			cin >> p[i].first >> p[i].second;
			for (int j = 1; j <= p[i].second; j++)
				v *= p[i].first;
		}
		if (m.find(v) == m.end())
		{
			ans = 4 * v + 3;
			sqrt3 = pow(v, 1.0 / 3.0) + eps;
			dfsa(1, 1);
			cout << ans << ' ' << ansa << ' ' << ansb << ' ' << ansc << endl;
			m[v] = ans_t(ans, ansa, ansb, ansc);
		}
		else
			cout << m[v].ans << ' ' << m[v].ansa << ' ' << m[v].ansb << ' ' << m[v].ansc << endl;
	}
	return 0;
}
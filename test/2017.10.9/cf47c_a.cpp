#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;
int perm[6];
string s[6];
int main()
{
	for (int i = 0; i < 6; i++)
	{
		cin >> s[i];
		perm[i] = i;
	}
	vector<string> ans;
	do
	{
		if (s[perm[0]].front() != s[perm[1]].front() || s[perm[1]].back() != s[perm[2]].front() || s[perm[0]].back() != s[perm[3]].front() || s[perm[2]].back() != s[perm[4]].front() || s[perm[3]].back() != s[perm[5]].front() || s[perm[4]].back() != s[perm[5]].back() || s[perm[0]].length() + s[perm[5]].length() - 1 != s[perm[2]].length() || s[perm[1]].length() + s[perm[4]].length() - 1 != s[perm[3]].length() || s[perm[2]][s[perm[0]].length() - 1] != s[perm[3]][s[perm[1]].length() - 1])
			continue;
		vector<string> now(s[perm[3]].length(), string(s[perm[2]].length(), '.'));
		copy(s[perm[0]].begin(), s[perm[0]].end(), now[0].begin());
		for (int i = 0; i < s[perm[1]].length(); i++)
			now[i][0] = s[perm[1]][i];
		copy(s[perm[2]].begin(), s[perm[2]].end(), now[s[perm[1]].length() - 1].begin());
		for (int i = 0; i < s[perm[3]].length(); i++)
			now[i][s[perm[0]].length() - 1] = s[perm[3]][i];
		for (int i = 0; i < s[perm[4]].length(); i++)
			now[s[perm[1]].length() + i - 1][s[perm[2]].length() - 1] = s[perm[4]][i];
		copy(s[perm[5]].begin(), s[perm[5]].end(), now[s[perm[3]].length() - 1].begin() + s[perm[0]].length() - 1);
		if (ans.size() == 0 || now < ans)
			ans = now;
	} while (next_permutation(perm, perm + 6));
	if (ans.size() == 0)
		cout << "Impossible\n";
	else
		for (auto row : ans)
			cout << row << endl;
	return 0;
}
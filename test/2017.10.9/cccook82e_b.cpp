#include <iostream>
#include <string>
using namespace std;
const int N = 1000000, MOD = 1e9 + 7;
string cmd;
int cnt, pow2[N + 5];
struct node
{
	int ls, rs, lazy, sz, zero, one;
	node() : ls(0), rs(0), lazy(0), sz(1), zero(1), one(0) {}
	void go(int t);
	void pushdown();
	void modify(int id);
} tree[N * 2 + 5];
void node::go(int t)
{
	lazy += t;
	sz = ((sz + 2ll * (pow2[t] - 1) * zero + 1ll * (pow2[t] - 1) * one) % MOD + MOD) % MOD;
	zero = (1ll * pow2[t] * zero + 1ll * pow2[t - 1] * one) % MOD;
	one = 0;
}
void node::pushdown()
{
	if (lazy)
	{
		if (!ls)
		{
			ls = ++cnt;
			tree[ls] = node();
			if (lazy > 1)
				tree[ls].go(lazy - 1);
		}
		else
			tree[ls].go(lazy);
		if (!rs)
		{
			rs = ++cnt;
			tree[rs] = node();
			if (lazy > 1)
				tree[rs].go(lazy - 1);
		}
		else
			tree[rs].go(lazy);
		lazy = 0;
	}
}
void node::modify(int id)
{
	pushdown();
	if (id == cmd.length() - 1)
		if (cmd.back() == 'L')
			ls = 0;
		else
			rs = 0;
	else if (cmd[id] == 'L')
		tree[ls].modify(id + 1);
	else
		tree[rs].modify(id + 1);
	sz = 1;
	one = zero = 0;
	if (!ls && !rs)
		zero = 1;
	else
	{
		if (!ls)
			one++;
		else
		{
			sz = (sz + tree[ls].sz) % MOD;
			zero = (zero + tree[ls].zero) % MOD;
			one = (one + tree[ls].one) % MOD;
		}
		if (!rs)
			one++;
		else
		{
			sz = (sz + tree[rs].sz) % MOD;
			zero = (zero + tree[rs].zero) % MOD;
			one = (one + tree[rs].one) % MOD;
		}
	}
}
int main()
{
	pow2[0] = 1;
	for (int i = 1; i <= N; i++)
		pow2[i] = pow2[i - 1] * 2 % MOD;
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		cnt = 1;
		tree[1] = node();
		while (n--)
		{
			int opt;
			cin >> opt;
			if (opt == 1)
			{
				int t;
				cin >> t;
				tree[1].go(t);
			}
			else
			{
				cin >> cmd;
				tree[1].modify(0);
			}
			cout << tree[1].sz << endl;
		}
	}
	return 0;
}
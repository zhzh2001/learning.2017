#include <iostream>
#include <algorithm>
using namespace std;
struct point
{
	int x, y, dist;
	point() {}
	point(int x, int y, int dist) : x(x), y(y), dist(dist) {}
	bool operator<(const point &rhs) const
	{
		return dist > rhs.dist;
	}
} p[4000];
int n, cnt;
const int N = 10;
struct ans_t
{
	int sum, p[N];
	bool operator<(const ans_t &rhs) const
	{
		return sum < rhs.sum;
	}
} now, ans;
inline int sqr(int x)
{
	return x * x;
}
void dfs(int k, int pred, int sum)
{
	if (k == n + 1)
	{
		now.sum = sum;
		ans = max(ans, now);
	}
	else
		for (int i = pred; i <= cnt && i <= 18; i++)
		{
			now.p[k] = i;
			int tmp = 0;
			for (int j = 1; j < k; j++)
				tmp += sqr(p[now.p[k]].x - p[now.p[j]].x) + sqr(p[now.p[k]].y - p[now.p[j]].y);
			dfs(k + 1, i, sum + tmp);
		}
}
int main()
{
	int r;
	cin >> n >> r;
	for (int x = -r; x <= r; x++)
		for (int y = -r; y <= r; y++)
			if (x * x + y * y <= r * r)
				p[++cnt] = point(x, y, sqr(x) + sqr(y));
	sort(p + 1, p + cnt + 1);
	dfs(1, 1, 0);
	cout << ans.sum << endl;
	for (int i = 1; i <= n; i++)
		cout << p[ans.p[i]].x << ' ' << p[ans.p[i]].y << endl;
	return 0;
}
#include <iostream>
#include <vector>
using namespace std;
const int N = 100005, MOD = 1e9 + 7;
int deg[N], f[N][2][2][2];
bool con[N][2], vis[N];
struct edge
{
	int v, p1, p2;
};
vector<edge> mat[N];
void dfs(int u)
{
	vis[u] = true;
	bool flag = false;
	for (auto e : mat[u])
		if (!vis[e.v])
		{
			dfs(e.v);
			flag = true;
			for (auto i : {0, 1})
				for (auto j : {0, 1})
					for (auto k : {0, 1})
						for (auto l : {0, 1})
							(f[u][i][k][l ^ ((e.p1 ^ i) | (e.p2 ^ j)) ^ con[u][i]] += f[e.v][j][k][l]) %= MOD;
		}
	if (!flag)
		f[u][1][1][con[u][1]] = f[u][0][0][con[u][0]] = 1;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (n--)
	{
		int k;
		cin >> k;
		if (k == 1)
		{
			int var;
			cin >> var;
			if (var < 0)
				con[-var][0] ^= 1;
			else
				con[var][1] ^= 1;
		}
		else
		{
			int var1, var2;
			cin >> var1 >> var2;
			int p1 = 0, p2 = 0;
			if (var1 < 0)
			{
				p1 = 1;
				var1 = -var1;
			}
			if (var2 < 0)
			{
				p2 = 1;
				var2 = -var2;
			}
			mat[var1].push_back({var2, p1, p2});
			mat[var2].push_back({var1, p2, p1});
			deg[var1]++;
			deg[var2]++;
		}
	}
	int ans[2], tmp[2];
	ans[0] = 1;
	ans[1] = 0;
	for (int i = 1; i <= m; i++)
		if (!vis[i] && deg[i] <= 1)
		{
			dfs(i);
			tmp[0] = ans[0];
			tmp[1] = ans[1];
			ans[0] = ans[1] = 0;
			for (auto p1 : {0, 1})
				for (auto p2 : {0, 1})
					for (auto u : {0, 1})
						for (auto v : {0, 1})
							(ans[p1 ^ p2] += 1ll * tmp[p1] * f[i][u][v][p2] % MOD) %= MOD;
		}
	for (int i = 1; i <= m; i++)
		if (!vis[i])
		{
			dfs(i);
			tmp[0] = ans[0];
			tmp[1] = ans[1];
			ans[0] = ans[1] = 0;
			for (auto p1 : {0, 1})
				for (auto p2 : {0, 1})
					for (auto u : {0, 1})
						for (auto v : {0, 1})
							(ans[p1 ^ p2 ^ ((u ^ mat[i][1].p1) | (v ^ mat[i][1].p2))] += 1ll * tmp[p1] * f[i][u][v][p2] % MOD) %= MOD;
		}
	cout << ans[1] << endl;
	return 0;
}
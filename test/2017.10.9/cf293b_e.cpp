#include <iostream>
using namespace std;
const int N = 15, MOD = 1e9 + 7;
int n, m, k, mat[N][N], cnt[N], mask[N][N];
int dfs(int x, int y)
{
	if (x > n)
		return 1;
	if (y > m)
		return dfs(x + 1, 1);
	mask[x][y] = mask[x - 1][y] | mask[x][y - 1];
	if (n - x + m - y > k - __builtin_popcount(mask[x][y]))
		return 0;
	int ans = 0, newc = -1;
	for (int i = 1; i <= k; i++)
		if (!(mask[x][y] & (1 << i)) && (!mat[x][y] || mat[x][y] == i))
		{
			mask[x][y] ^= 1 << i;
			if (!cnt[i]++)
			{
				if (newc == -1)
					newc = dfs(x, y + 1);
				ans = (ans + newc) % MOD;
			}
			else
				ans = (ans + dfs(x, y + 1)) % MOD;
			cnt[i]--;
			mask[x][y] ^= 1 << i;
		}
	return ans;
}
int main()
{
	cin >> n >> m >> k;
	if (n + m - 1 > k)
	{
		cout << 0 << endl;
		return 0;
	}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			cin >> mat[i][j];
			cnt[mat[i][j]]++;
		}
	cout << dfs(1, 1) << endl;
	return 0;
}
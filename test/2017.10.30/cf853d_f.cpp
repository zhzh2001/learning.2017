#include <iostream>
#include <algorithm>
using namespace std;
const int N = 300005, m = 30, INF = 1e9;
int a[N], f[N][m + 1];
inline void update(int &lhs, int rhs)
{
	if (rhs < lhs)
		lhs = rhs;
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	f[0][0] = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 0; j <= m; j++)
			if (f[i - 1][j] < INF)
			{
				if (j + a[i] / 1000 <= m)
					update(f[i][j + a[i] / 1000], f[i - 1][j] + a[i]);
				int bonus = min(a[i] / 100, j);
				update(f[i][j - bonus], f[i - 1][j] + a[i] - bonus * 100);
			}
	cout << *min_element(f[n], f[n] + m + 1) << endl;
	return 0;
}
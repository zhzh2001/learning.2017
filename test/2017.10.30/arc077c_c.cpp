#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N], cnt[N * 2];
long long id[N * 2];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	long long init = 0;
	for (int i = 1; i < n; i++)
	{
		int l = a[i], r = a[i + 1];
		if (l > r)
			r += m;
		init += r - l;
		if (l + 1 < r)
		{
			cnt[l + 1]++;
			cnt[r + 1]--;
			id[l + 1] += l;
			id[r + 1] -= l;
		}
	}
	for (int i = 1; i <= m * 2; i++)
	{
		cnt[i] += cnt[i - 1];
		id[i] += id[i - 1];
	}
	long long ans = init;
	for (int i = 1; i <= m; i++)
	{
		long long now = init - 1ll * cnt[i] * i + id[i] + cnt[i] - 1ll * cnt[i + m] * (i + m) + id[i + m] + cnt[i + m];
		ans = min(ans, now);
	}
	cout << ans << endl;
	return 0;
}
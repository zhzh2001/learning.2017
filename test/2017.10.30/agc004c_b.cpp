#include <iostream>
#include <string>
using namespace std;
const int N = 500;
string mat[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 0; i < n; i++)
		cin >> mat[i];
	for (int i = 0; i < n; i++)
	{
		if (i & 1)
		{
			cout.put('#');
			for (int j = 1; j < m; j++)
				cout.put(mat[i][j]);
		}
		else
		{
			for (int j = 0; j < m - 1; j++)
				cout.put('#');
			cout.put('.');
		}
		cout << endl;
	}
	cout << endl;
	for (int i = 0; i < n; i++)
	{
		if (i & 1)
		{
			cout.put('.');
			for (int j = 1; j < m; j++)
				cout.put('#');
		}
		else
		{
			for (int j = 0; j < m - 1; j++)
				cout.put(mat[i][j]);
			cout.put('#');
		}
		cout << endl;
	}
	return 0;
}
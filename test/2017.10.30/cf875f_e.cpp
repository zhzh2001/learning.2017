#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005;
struct edge
{
	int u, v, w;
	bool operator<(const edge &rhs) const
	{
		return w > rhs.w;
	}
} e[N];
int f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
bool bl[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= m; i++)
		cin >> e[i].u >> e[i].v >> e[i].w;
	sort(e + 1, e + m + 1);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	int ans = 0;
	for (int i = 1; i <= m; i++)
	{
		int ru = getf(e[i].u), rv = getf(e[i].v);
		if (bl[ru] && bl[rv])
			continue;
		if (ru == rv)
			bl[ru] = true;
		else
		{
			f[ru] = rv;
			bl[rv] |= bl[ru];
		}
		ans += e[i].w;
	}
	cout << ans << endl;
	return 0;
}
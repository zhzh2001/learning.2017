#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005;
int n, q, a[N], tmp[N][4];
long long ans[N];
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T;
struct event
{
	int x, yl, yr, id, val;
	bool operator<(const event &rhs) const
	{
		return x < rhs.x;
	}
} e[N * 6];
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> q;
	long long tot = 1ll * n * (n - 1) / 2;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	int en = 0;
	for (int i = 1; i <= q; i++)
	{
		int x1, y1, x2, y2;
		cin >> x1 >> y1 >> x2 >> y2;
		ans[i] = tot - 1ll * (n - y2) * (n - y2 - 1) / 2 - 1ll * (y1 - 1) * (y1 - 2) / 2 - 1ll * (x1 - 1) * (x1 - 2) / 2 - 1ll * (n - x2) * (n - x2 - 1) / 2;
		if (x1 > 1)
		{
			if (y1 > 1)
				e[++en] = {x1 - 1, 1, y1 - 1, i, 0};
			if (y2 < n)
				e[++en] = {x1 - 1, y2 + 1, n, i, 1};
		}
		if (x2 < n)
		{
			if (y1 > 1)
			{
				e[++en] = {x2, 1, y1 - 1, i, -2};
				e[++en] = {n, 1, y1 - 1, i, 2};
			}
			if (y2 < n)
			{
				e[++en] = {x2, y2 + 1, n, i, -3};
				e[++en] = {n, y2 + 1, n, i, 3};
			}
		}
	}
	sort(e + 1, e + en + 1);
	for (int i = 1, j = 1; i <= n; i++)
	{
		T.modify(a[i], 1);
		for (; j <= en && e[j].x == i; j++)
		{
			int now = T.query(e[j].yr) - T.query(e[j].yl - 1);
			if (e[j].val >= 0)
				tmp[e[j].id][e[j].val] += now;
			else
				tmp[e[j].id][-e[j].val] -= now;
		}
	}
	for (int i = 1; i <= q; i++)
	{
		for (int j = 0; j < 4; j++)
			ans[i] += 1ll * tmp[i][j] * (tmp[i][j] - 1) / 2;
		cout << ans[i] << endl;
	}
	return 0;
}
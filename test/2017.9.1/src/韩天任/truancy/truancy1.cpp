#include<bits/stdc++.h>
using namespace std;
const int L=200005;
long long maxn,k,kk,n,f[1000000],a[1000000];
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
} 
inline int read(){
	int x=0,f=1;char c=gc();
	for (;c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout); 
	n=read();
	k=read();
	for (int i=1;i<=n;i++)
		a[i]=read();	
	for (int i=1;i<=n;i++)
		maxn+=a[i];
	long long minn=1e9+1;
	for (int i=1;i<=k;i++)
		if (minn>a[i]){
			minn=a[i];
			kk=i;
		}
	f[kk]=a[kk];
	long long temp=0;
	for (int i=kk+1;i<=n;i++){
		temp++;
		f[i]=f[i-1];
		if (temp==k){
			if (f[i-1]+a[i]<=a[i-k+1])
				temp=0,f[i]=f[i-1]+a[i];
			else
				temp-=1,f[i]=a[i-k+1];
		}
		a[i]+=a[i-k];
	}
	cout<<(maxn-f[n]);
}

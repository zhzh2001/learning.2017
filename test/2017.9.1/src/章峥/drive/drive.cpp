#include <fstream>
#include <algorithm>
#include <windows.h>
struct Istream
{
  private:
	HANDLE hFile;
	char *base, *p;
	bool good;

	void fetch()
	{
		if (hFile == INVALID_HANDLE_VALUE)
			good = false;
		else
		{
			HANDLE hFileMappingObject = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
			if (!hFileMappingObject)
				good = false;
			else
				base = p = (char *)MapViewOfFile(hFileMappingObject, FILE_MAP_READ, 0, 0, 0);
		}
	}

#define nextchar() *p++

	template <typename Int>
	void readint(Int &x)
	{
		char c;
		for (c = nextchar(); isspace(c); c = nextchar())
			;
		x = 0;
		Int sign = 1;
		if (c == '-')
			sign = -1, c = nextchar();
		for (; isdigit(c); c = nextchar())
			x = x * 10 + c - '0';
		x *= sign;
	}

  public:
	Istream() : hFile(NULL), p(NULL), good(false) {}

	explicit Istream(const std::string &filename) : hFile(CreateFile(filename.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)), p(NULL), good(false)
	{
		fetch();
	}

	bool close()
	{
		return UnmapViewOfFile(base) && CloseHandle(hFile);
	}

	~Istream()
	{
		close();
	}

	Istream &operator>>(int &value)
	{
		readint(value);
		return *this;
	}
};
using namespace std;
Istream fin("drive.in");
ofstream fout("drive.out");
const int N = 500005, MOD = 1e9 + 7;
int n, a[N], b[N];
long long f[2][N];
struct BIT
{
	long long tree[N];
	void clear()
	{
		fill(tree + 1, tree + n + 1, 0);
	}
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			tree[x] += val;
	}
	long long query(int x)
	{
		long long ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T1, T2;
int main()
{
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	copy(a + 1, a + n + 1, b + 1);
	sort(b + 1, b + n + 1);
	for (int i = 1; i <= n; i++)
		a[i] = lower_bound(b + 1, b + n + 1, a[i]) - b;
	for (int t = 0; t < 2; t++)
	{
		T1.clear();
		T2.clear();
		for (int i = 1; i <= n; i++)
		{
			T2.modify(a[i], T1.query(a[i]));
			T1.modify(a[i], 1);
			long long tmp = T1.query(a[i] - 1);
			f[t][i] = (tmp * (tmp - 1) / 2 - T2.query(a[i] - 1)) % MOD;
		}
		reverse(a + 1, a + n + 1);
	}
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		ans = (ans + f[0][i] * f[1][n - i + 1]) % MOD;
	fout << ans << endl;
	return 0;
}
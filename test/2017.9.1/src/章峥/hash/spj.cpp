#include <fstream>
#include <string>
#include <iostream>
using namespace std;
ifstream fin("hash.in"), fout("hash.out");
typedef unsigned long long hash_t;
const int N = 1005;
hash_t mod[N], hs[N], ht[N];
int main()
{
	string s, t;
	fout >> s >> t;
	if (s.length() != t.length())
	{
		cout << "WA\n";
		return 1;
	}
	int len = s.length();
	s = ' ' + s;
	t = ' ' + t;
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> mod[i];
	for (int i = 1; i <= len; i++)
		for (int j = 1; j <= n; j++)
		{
			hs[j] = (hs[j] * 26 + s[i] - 'a') % mod[j];
			ht[j] = (ht[j] * 26 + t[i] - 'a') % mod[j];
		}
	for (int i = 1; i <= n; i++)
		if (hs[i] != ht[i])
		{
			cout << "WA\n";
			return 1;
		}
	cout << "AC\n";
	return 0;
}
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("hash.in");
ofstream fout("hash.out");
const int N = 1005, L = 2500, B = 1e9, A = 26;
long long a[N];
long long gcd(long long a, long long b)
{
	return b ? gcd(b, a % b) : a;
}
struct bigint
{
	int len;
	long long dig[L];
	bigint(long long x = 0)
	{
		len = 0;
		do
			dig[++len] = x % B;
		while (x /= B);
	}
	void reserve(int nl)
	{
		if (nl > len)
			fill(dig + len + 1, dig + nl + 1, 0);
		len = nl;
	}
	bigint operator*(const bigint &rhs) const
	{
		bigint res;
		res.reserve(len + rhs.len);
		for (int i = 1; i <= len; i++)
			for (int j = 1; j <= rhs.len; j++)
				res.dig[i + j - 1] += dig[i] * rhs.dig[j];
		for (int i = 1; i <= res.len; i++)
		{
			res.dig[i + 1] += res.dig[i] / B;
			res.dig[i] %= B;
		}
		if (!res.dig[res.len])
			res.len--;
		return res;
	}
	bigint &operator*=(const bigint &rhs)
	{
		return *this = *this * rhs;
	}
	int divide()
	{
		long long rem = 0;
		for (int i = len; i; i--)
		{
			long long tmp = (rem * B + dig[i]) / A;
			rem = (rem * B + dig[i]) % A;
			dig[i] = tmp;
		}
		while (len > 1 && dig[len] == 0)
			len--;
		return rem;
	}
};
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	for (int i = 2; i <= n; i++)
		for (int j = 1; j < i; j++)
			a[i] /= gcd(a[i], a[j]);
	bigint ans = 1;
	for (int i = 1; i <= n; i++)
		ans *= a[i];
	string s;
	do
		s = char(ans.divide() + 'a') + s;
	while (ans.len > 1 || ans.dig[1]);
	fout << string(s.length(), 'a') << endl
		 << s << endl;
	return 0;
}
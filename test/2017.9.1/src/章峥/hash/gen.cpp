#include <fstream>
using namespace std;
ofstream fout("hash.in");
const int n = 1000;
bool prime(int x)
{
	for (int i = 2; i * i <= x; i++)
		if (x % i == 0)
			return false;
	return true;
}
int main()
{
	fout << n << endl;
	int p = 2;
	for (int i = 1; i <= n; i++, p++)
	{
		while (!prime(p))
			p++;
		long long j = p;
		for (; 1.0 * j * p <= 5e17; j *= p)
			;
		fout << j << ' ';
	}
	return 0;
}
#include <fstream>
#include <numeric>
#include <algorithm>
#include <windows.h>
struct Istream
{
  private:
	HANDLE hFile;
	char *base, *p;
	bool good;

	void fetch()
	{
		if (hFile == INVALID_HANDLE_VALUE)
			good = false;
		else
		{
			HANDLE hFileMappingObject = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
			if (!hFileMappingObject)
				good = false;
			else
				base = p = (char *)MapViewOfFile(hFileMappingObject, FILE_MAP_READ, 0, 0, 0);
		}
	}

#define nextchar() *p++

	template <typename Int>
	void readint(Int &x)
	{
		char c;
		for (c = nextchar(); isspace(c); c = nextchar())
			;
		x = 0;
		Int sign = 1;
		if (c == '-')
			sign = -1, c = nextchar();
		for (; isdigit(c); c = nextchar())
			x = x * 10 + c - '0';
		x *= sign;
	}

  public:
	Istream() : hFile(NULL), p(NULL), good(false) {}

	explicit Istream(const std::string &filename) : hFile(CreateFile(filename.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)), p(NULL), good(false)
	{
		fetch();
	}

	bool close()
	{
		return UnmapViewOfFile(base) && CloseHandle(hFile);
	}

	~Istream()
	{
		close();
	}

	Istream &operator>>(int &value)
	{
		readint(value);
		return *this;
	}

	Istream &operator>>(long long &value)
	{
		readint(value);
		return *this;
	}
};
using namespace std;
Istream fin("truancy.in");
ofstream fout("truancy.out");
const int N = 1000005;
int Q[N];
long long a[N], f[N];
int main()
{
	int n, k;
	fin >> n >> k;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	int l = 1, r = 1;
	Q[l] = 0;
	for (int i = 1; i <= n + 1; i++)
	{
		while (l <= r && i - Q[l] - 1 > k)
			l++;
		f[i] = f[Q[l]] + a[i];
		while (l <= r && f[i] <= f[Q[r]])
			r--;
		Q[++r] = i;
	}
	fout << accumulate(a + 1, a + n + 1, 0ll) - f[n + 1] << endl;
	return 0;
}
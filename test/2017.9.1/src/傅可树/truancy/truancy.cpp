#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn=1e6+5,L=200005;
int a[maxn],n,k;
ll s,f[maxn],q[maxn];
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
} 
int main()
{
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout); 
	n=read(); k=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
		s+=a[i];
	}
	int h=1,t=1; q[1]=0;
	for (int i=1;i<=n;i++){
		while (h<=t&&q[h]<i-k-1) h++;
		f[i]=f[q[h]]+a[i];
		while (h<=t&&f[i]<=f[q[t]]) t--;
		q[++t]=i;
	}
	ll ans=1e18;
	for (int i=n-k;i<=n;i++){
		ans=min(ans,f[i]);
	}
	printf("%lld\n",s-ans);
	return 0;
}

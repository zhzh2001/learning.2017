#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int L=200005,maxn=500005,mod=1e9+7;
int n,a[maxn];
ll ans1[maxn],ans2[maxn],s[maxn];
char LZH[L],*S=LZH,*T=LZH;
inline char gc()
{
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read()
{
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read(); 
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<i;j++){
			s[j]+=(a[i-1]<a[j]);
			if (a[j]<a[i]) (ans1[i]+=s[j])%=mod;
		}
	}
	memset(s,0,sizeof(s));
	for (int i=n;i;i--){
		for (int j=i+1;j<=n;j++){
			s[j]+=(a[i+1]<a[j]);
			if (a[j]<a[i]) (ans2[i]+=s[j])%=mod;
		}
	}
	ll ans=0;
	for (int i=1;i<=n;i++){
		(ans+=ans1[i]*ans2[i])%=mod;
	}
	printf("%lld\n",ans);
	return 0;	
}

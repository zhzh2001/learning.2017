#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e3+5;
int n,x;
int a[N],top;
Ll lcm;
Ll gcd(Ll x,Ll y){return !y?x:gcd(y,x%y);}
int main()
{
	freopen("hash.in","r",stdin);
	freopen("hash.out","w",stdout);
	scanf("%d",&n);
	lcm=1;
	for(int i=1;i<=n;i++)scanf("%d",&x),lcm=lcm/gcd(lcm,(Ll)x)*x;
	while(lcm)a[++top]=lcm%26,lcm/=26;
	for(int i=1;i<=top;i++)putchar('a');
	cout<<endl;
	for(int i=top;i;i--)putchar(a[i]+'a');
}



#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int R(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
const int N=3e3+5;
int d[N][2],f[N];
int a[N],b[N];
int n,m;
Ll mo=1e9+7,ans;
int er(int k){
	int l=1,r=n,mid;
	while(r>=l){
		mid=l+r>>1;
		if(b[mid]==k)return mid;
		if(b[mid]>k)r=mid-1;else l=mid+1;
	}
}
void add(int x,int y){for(;x<=n;x+=x&-x)f[x]+=y;}
int out(int x){int ans=0;for(;x;x-=x&-x)ans+=f[x];return ans;}
void make(int k){
	for(int i=1;i<=n;i++){
		for(int j=i-1;j;j--){
			if(a[j]<a[i])d[i][k]+=out(a[j]-1);
			add(a[j],1);
		}
		for(int j=i-1;j;j--)add(a[j],-1);
	}
}
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=R();
	for(int i=1;i<=n;i++)a[i]=R(),b[i]=a[i];
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)a[i]=er(a[i]);	
	make(0);
	for(int i=1;i+i<=n;i++)swap(a[i],a[n-i+1]);
	make(1);
	for(int i=1;i<=n;i++)ans=(ans+(Ll)d[i][0]*d[n-i+1][1])%mo;
	printf("%lld",ans);
}


#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*TT=LZH;
inline char gc(){
	if (S==TT){
		TT=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==TT) return EOF;
	}
	return *S++;
}
inline int R(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N=1e6+5;
struct tree{int l,r;Ll v,lazy;}T[5*N];
int n,k,x,l;
Ll ans;
void make(int id,int l,int r){
	T[id].l=l;T[id].r=r;
	if(l==r)return;
	int mid=l+r>>1;
	make(id*2  ,l,mid  );
	make(id*2+1,mid+1,r);
}
void push(int id){
	if(!T[id].lazy)return;
	int x=id*2,y=x+1;
	T[x].v+=T[id].lazy;
	T[y].v+=T[id].lazy;
	T[x].lazy+=T[id].lazy;
	T[y].lazy+=T[id].lazy;
	T[id].lazy=0;
}
void add(int id,int l,int r,Ll v){
	if(l<=T[id].l&&T[id].r<=r){
		T[id].v+=v;
		T[id].lazy+=v;
		return;
	}
	push(id);
	if(T[id*2  ].r>=l)add(id*2  ,l,r,v);
	if(T[id*2+1].l<=r)add(id*2+1,l,r,v);
	T[id].v=max(T[id*2].v,T[id*2+1].v);
}
Ll out(int id,int l,int r){
	if(l<=T[id].l&&T[id].r<=r)return T[id].v;
	push(id);
	Ll ans=0;
	if(T[id*2  ].r>=l)ans=max(ans,out(id*2  ,l,r));
	if(T[id*2+1].l<=r)ans=max(ans,out(id*2+1,l,r));
	return ans;
}
int main()
{
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
	n=R();k=R();
	make(1,-5,n);
	for(int i=1;i<=n;i++){
		x=R();
		l=max(-2,i-k-1);
		add(1,l,i-2,x);
		ans=out(1,l,i-1);
		add(1,i,i,ans);
	}printf("%lld",ans);
}


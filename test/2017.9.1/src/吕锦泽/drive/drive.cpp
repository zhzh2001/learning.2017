#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 500005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f; 
}
ll n,ans,a[N],b[N],c[N],f[N],g[N],num[N];
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=b[i]=read();
	sort(b+1,b+1+n);
	rep(i,1,n) a[i]=lower_bound(b+1,b+1+n,a[i])-b;
	rep(i,1,n) rep(j,1,i-1)
		if (a[i]>a[j]) (f[i]+=num[j])%=mod;
		else if (a[i]<a[j]) ++num[j];
	memset(num,0,sizeof num);
	per(i,n,1) per(j,n,i+1)
		if (a[i]>a[j]) (g[i]+=num[j])%=mod;
		else if (a[i]<a[j]) ++num[j];
	rep(i,1,n) (ans+=f[i]*g[i]%mod)%=mod;
	printf("%lld",ans);
}

#include <bits/stdc++.h>
#define N 1000020
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll f[2][N], a[N];
int main(int argc, char const *argv[]){
	freopen("truancy.in", "r", stdin);
	freopen("truancy.out", "w", stdout);
	int n = read(), k = read();
	for(int i = 1; i <= n; i++) a[i] = read();
	int now = 1, pre = 0;
	for(int i = 1; i <= n; i++){
		now ^= 1; pre ^= 1;
		f[now][0] = 0;
		for(int j = 1; j <= k; j++){
			f[now][0] = max(f[pre][j-1], f[now][0]);
			f[now][j] = f[pre][j-1]+a[i];
		}
		f[now][0] = max(f[now][0], f[pre][k]);
	}
	ll ans = 0;
	for(int i = 0; i < k; i++)
		ans = max(ans, f[now][i]);
	memset(f, 0, sizeof f);
	now = 1, pre = 0;
	for(int i = n; i; i--){
		now ^= 1; pre ^= 1;
		f[now][0] = 0;
		for(int j = 1; j <= k; j++){
			f[now][0] = max(f[pre][j-1], f[now][0]);
			f[now][j] = f[pre][j-1]+a[i];
		}
		f[now][0] = max(f[now][0], f[pre][k]);
	}
	for(int i = 0; i <= k; i++)
		ans = max(ans, f[now][i]);
	printf("%lld\n", ans);
	return 0;
}

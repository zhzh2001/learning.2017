#include <bits/stdc++.h>
#define N 1000020
#define mod 1000000007
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll a[N], b[N], ls[N], rs[N], c[N];
int main(int argc, char const *argv[]){
	freopen("drive.in", "r", stdin);
	freopen("drive.out", "w", stdout);
	int n = read();
	for(int i = 1; i <= n; i++) a[i] = /*b[i] = */read();
	// sort(b+1, b+n+1); int* end = unique(b+1, b+n+1);
	// for(int i = 1; i <= n; i++) a[i] = lower_bound(b+1, end, a[i])-b;
	for(int i = 1; i <= n; i++)
		for(int j = 1; j < i; j++)
			if(a[i] > a[j]) ls[i] += c[j];
			else if(a[i] < a[j]) c[j]++;
	memset(c, 0, sizeof(int)*N);
	for(int i = n; i; i--)
		for(int j = i+1; j <= n; j++)
			if(a[i] > a[j]) rs[i] += c[j];
			else if(a[i] < a[j]) c[j]++;
	ll ans = 0;
	for(int i = 3; i <= n-2; i++)
		ans = (ans+1ll*ls[i]*rs[i])%mod;
	printf("%lld\n", ans);
	return 0;
}

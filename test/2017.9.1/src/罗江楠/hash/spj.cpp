#include <bits/stdc++.h>
#define N 30020
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
char s[N], t[N];
ll hash[3][1020], mo[1020];
int main(int argc, char const *argv[]){
	scanf("%s%s", s+1, t+1);
	int n = read(), bas = 26;
	for(int i = 1; i <= n; i++) mo[i] = read();
	int lens = strlen(s+1), lent = strlen(t+1);
	if(lens != lent) return puts("NO");
	for(int i = 1; i <= lens; i++)
		for(int j = 1; j <= n; j++){
			hash[1][j] = (hash[1][j]*bas+s[i]-'a')%mo[j];
			hash[2][j] = (hash[2][j]*bas+t[i]-'a')%mo[j];
		}
	for(int i = 1; i <= n; i++)
		if(hash[1][i] != hash[2][i]) return puts("NO");
	puts("YES");
	return 0;
}
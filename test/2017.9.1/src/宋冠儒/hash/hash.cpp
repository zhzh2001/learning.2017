#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<map>
#define ll long long
using namespace std;
const int L=200005,bas=13,mo=123456;
int tot1,tot2,n,mod[100],cnt[1000000],num1[20],num2[20];
char LZH[L],*S=LZH,*T=LZH;
inline char gc()
{
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read()
{
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
inline bool judge(int x,int y)
{
	memset(num1,0,sizeof(num1));
	memset(num2,0,sizeof(num2));
	tot1=0,tot2=0;
	while(x){
		num1[++tot1]=x%26;
		x/=26;
	}
	while (y){
		num2[++tot2]=y%26;
		y/=26;
	}
	if (tot1==tot2) return 1;
	return 0;
}
int main()
{
	freopen("hash.in","r",stdin);
	freopen("hash.out","w",stdout); 
	n=read();
	for (int i=1;i<=n;i++){
		mod[i]=read();
	}
	for (int i=1;i<=100000;i++){
		int res=0;
		for (int j=1;j<=n;j++){
			int moo=i%mod[j];
			res=(res*bas+moo)%mo;
		}
		if (cnt[res]){
			if (judge(i,cnt[res])){
				for (int j=1;j<=tot2;j++){
					if (num2[j]==0) printf("z");
						else printf("%c",num2[j]+'a'-1);
				}
				printf("\n");
				for (int j=1;j<=tot1;j++){
					if (num1[j]==0) printf("z");
						else printf("%c",num1[j]+'a'-1);
				}
				break;
			}else{
				cnt[res]=i;
			}
		}else{
			cnt[res]=i;
		}
	}
	return 0;
}

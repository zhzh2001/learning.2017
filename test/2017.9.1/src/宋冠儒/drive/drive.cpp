#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int mo=1e9+7;
ll ans;
int f[500010],d[500010],dep[500010];
int n;
 inline void read(int &x)
{
	char ch;int bo=0; x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar()) if (ch=='-') bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo) x=-x;
}

 int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i) read(dep[i]);
	for (int i=1;i<=n-4;++i)
	{
		int sum=0;
		for (int j=i+1;j<=n-3;++j)
		{
			if (dep[i]<dep[j]) f[j]+=sum;
			if (dep[j]<dep[i]) ++sum;
		}
	}
	for (int i=n;i>=5;--i)
	{
		int sum=0;
		for (int j=i-1;j>=4;--j)
		{
			if (dep[i]<dep[j]) d[j]+=sum;
			if (dep[j]<dep[i]) ++sum;
		}
	}
	for (int i=3;i<=n-2;++i)
		ans=(ans+(ll)(f[i]*d[i]))%mo;
	printf("%lld",ans);
}

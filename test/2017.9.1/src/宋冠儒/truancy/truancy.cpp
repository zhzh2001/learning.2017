#include<bits/stdc++.h>
#define ll long long
#define N 1000005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f; 
}
ll n,k,l=1,r=0,sum,ans,q[N],f[N];
int main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
	n=read(); k=read();
	rep(i,1,k){
		f[i]=read(); sum+=f[i];
		while (l<=r&&f[i]<=f[q[r]]) --r;
		q[++r]=i;
	}
	rep(i,k+1,n){
		while (i-q[l]>k+1&&l<r) ++l;
		ll t=read(); f[i]=f[q[l]]+t; sum+=t;
		while (l<=r&&f[i]<=f[q[r]]) --r;
		q[++r]=i;
	}
	ans=f[n];
	per(i,n,n-k+1) ans=min(ans,f[i]);
	printf("%lld",sum-ans);
}

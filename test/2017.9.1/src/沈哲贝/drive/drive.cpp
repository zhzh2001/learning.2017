#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define g(i,j)	a[(i-1)*m+j]
#define maxn 100010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll q[maxn],a[maxn],c[maxn],f[maxn],tot,n,tt,ans;
const ll mod=1e9+7;
void add(ll x){	for(;x<=n;x+=x&-x)	c[x]++;	}
inline ll ask(ll x){	ll ans=0;	for(;x;x-=x&-x)	ans+=c[x];	return ans;}
inline ll qqq(ll x){
	ll l=1,r=tt;
	while(l<=r){
		ll mid=(l+r)>>1;
		if (q[mid]==x)	return mid;
		if (q[mid]>x)	r=mid-1;
		else	l=mid+1;
	}
}
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read(),q[++tot]=a[i];
	sort(q+1,q+tot+1);
	For(i,1,tot)	if (q[i]!=q[i-1])	q[++tt]=q[i];
	For(i,1,n)	a[i]=qqq(a[i]);
	For(i,1,n){
		f[i]=ask(a[i]);
		For(j,1,i-1)	if (a[j]>a[i])	add(a[j]);
	}
	memset(c,0,sizeof c);
	FOr(i,n,1){
		ans=(ans+f[i]*ask(a[i]))%mod;
		For(j,i+1,n)	if (a[j]>a[i])	add(a[j]);
	}
	writeln(ans);
}
//权值小于x的逆序对数
/*
前缀中有几个比它大的。
对于权值x,所有
f[x][y]

*/

#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define g(i,j)	a[(i-1)*m+j]
#define maxn 100010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
const ll mod=1e9+7;
ll ans[maxn],q[maxn],a[maxn],c[maxn],f[maxn],lazy[maxn],sz[maxn],tot,n,tt,answ;
inline ll qqq(ll x){
	ll l=1,r=tt;
	while(l<=r){
		ll mid=(l+r)>>1;
		if (q[mid]==x)	return mid;
		if (q[mid]>x)	r=mid-1;
		else	l=mid+1;
	}
}
void pushdown(ll p){	lazy[p<<1]+=lazy[p];	lazy[p<<1|1]+=lazy[p];	lazy[p]=0;	}
void updata(ll p){	ans[p]=(ans[p<<1]+ans[p<<1|1]+lazy[p<<1]*sz[p<<1]+lazy[p<<1|1]*sz[p<<1|1])%mod;	sz[p]=sz[p<<1]+sz[p<<1|1];	}
void change(ll l,ll r,ll p,ll s,ll t){
	if (l==s&&r==t){	lazy[p]++;	return;	}
	ll mid=(l+r)>>1;	pushdown(p);
	if (t<=mid)	change(l,mid,p<<1,s,t);
	else if (s>mid)	change(mid+1,r,p<<1|1,s,t);
	else	change(l,mid,p<<1,s,mid),change(mid+1,r,p<<1|1,mid+1,t);
	updata(p);
}
void ins(ll l,ll r,ll p,ll v){
	if (l==r){	sz[p]++;	ans[p]+=lazy[p];	lazy[p]=0;	return;	}
	ll mid=(l+r)>>1;	pushdown(p);
	v<=mid?ins(l,mid,p<<1,v):ins(mid+1,r,p<<1|1,v);
	updata(p);
}
ll query(ll l,ll r,ll p,ll s,ll t){
	if (l==s&&r==t)	return ans[p]+lazy[p]*sz[p];
	ll mid=(l+r)>>1;	pushdown(p);
	if (t<=mid)	return query(l,mid,p<<1,s,mid);
	else	if (s>mid)	return query(mid+1,r,p<<1|1,mid+1,t);
	else	return query(l,mid,p<<1,s,mid)+query(mid+1,r,p<<1|1,mid+1,t);
	updata(p);
}
int main(){
	n=read();
	For(i,1,n)	a[i]=read(),q[++tot]=a[i];
	sort(q+1,q+tot+1);
	For(i,1,tot)	if (q[i]!=q[i-1])	q[++tt]=q[i];
	For(i,1,n)	a[i]=qqq(a[i]);
	For(i,1,n){
		if (a[i]>1)	f[i]=query(1,n,1,1,a[i]-1);
		if (a[i]!=n)change(1,n,1,a[i]+1,n);
		ins(1,n,1,a[i]);
	}
	For(i,1,n)	writeln(f[i]);
}
//权值小于x的逆序对数
/*
前缀中有几个比它大的。
对于权值x,所有
7
6 2 3 7 1 5 4
6

*/

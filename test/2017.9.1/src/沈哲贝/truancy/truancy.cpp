#include<ctime>
#include<cmath>
#include<cstdio>
#include<memory.h>
#include<map>
#include<algorithm>
#define ll long long
#define maxn 8000010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())	if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())	x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
ll mx[maxn],lazy[maxn],a[maxn],n,k,st,ed;
inline ll max(ll a,ll b){	return a<b?b:a;	}
void updata(ll p){	mx[p]=max(mx[p<<1]+lazy[p<<1],mx[p<<1|1]+lazy[p<<1|1]);	}
ll query(ll l,ll r,ll p,ll s,ll t){
	if (l==s&&r==t)	return lazy[p]+mx[p];
	ll ans,mid=(l+r)>>1;	if (lazy[p])	lazy[p<<1]+=lazy[p],lazy[p<<1|1]+=lazy[p],lazy[p]=0;
	if (t<=mid)	ans=query(l,mid,p<<1,s,t);
	else if (s>mid)	ans=query(mid+1,r,p<<1|1,s,t);
	else	ans=max(query(l,mid,p<<1,s,mid),query(mid+1,r,p<<1|1,mid+1,t));
	mx[p]=max(mx[p<<1]+lazy[p<<1],mx[p<<1|1]+lazy[p<<1|1]);
	return ans;
}
void add(ll l,ll r,ll p,ll s,ll t,ll v){
	if (l==s&&r==t)	return lazy[p]+=v,void(0);
	ll mid=(l+r)>>1;	if (lazy[p])	lazy[p<<1]+=lazy[p],lazy[p<<1|1]+=lazy[p],lazy[p]=0;
	if (t<=mid)	add(l,mid,p<<1,s,t,v);
	else if (s>mid)	add(mid+1,r,p<<1|1,s,t,v);
	else	add(l,mid,p<<1,s,mid,v),add(mid+1,r,p<<1|1,mid+1,t,v);
	mx[p]=max(mx[p<<1]+lazy[p<<1],mx[p<<1|1]+lazy[p<<1|1]);
}
void change(ll l,ll r,ll p,ll pos,ll v){
	if (l==r){	lazy[p]=0;	mx[p]=v;	return;	}
	ll mid=(l+r)>>1;	if (lazy[p])	lazy[p<<1]+=lazy[p],lazy[p<<1|1]+=lazy[p],lazy[p]=0;
	pos<=mid?change(l,mid,p<<1,pos,v):change(mid+1,r,p<<1|1,pos,v);
	mx[p]=max(mx[p<<1]+lazy[p<<1],mx[p<<1|1]+lazy[p<<1|1]);
}
int main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
	n=read();	k=read();
	For(i,1,n)	a[i]=read();
	ed=n+k;	st=n;
	For(i,1,n){
		change(1,n+k,1,st,(query(1,n+k,1,st+1,min(n+k,ed+1))));
		add(1,n+k,1,st+1,ed,a[i]);
		--st;	--ed;
	}printf("%lld\n",query(1,n+k,1,1,k+1)); 
}
//f[i]=f[i-2]+f[i-1]
// f[1][0]	n	
// f[n][0]	1
//n=2
//  f[3][0]
//  f[3][1]	f[2][0]
//  f[2][1]	f[1][0]
//  f[1][1]
/*构造n个26进制数使得在n个魔域下相等
求n个数的lcm */ 
/*
5 3
1 3 2 5 4
*/

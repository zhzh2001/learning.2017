#include<cstdio>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll f[3010][3010],n,k,a[3010],ans;
int main(){
	freopen("truancy.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	k=read();
	For(i,1,n)	a[i]=read();
	For(i,1,n){
		For(j,0,k)	f[i][0]=max(f[i][0],f[i-1][j]);
		For(j,1,k)	f[i][j]=max(f[i][j],f[i-1][j-1])+a[i];
	}
	For(i,0,k)	ans=max(ans,f[n][i]);
	writeln(ans);
}

#include<ctime>
#include<cstdio>
#include<memory.h>
#include<algorithm>
#define ll unsigned long long
#define maxn 300010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll n,a[maxn];
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;	}
inline ll MOD(ll p){
	ll ans=0;
	FOr(i,a[0],1)	ans=(ans*26+a[i])%p;
	return ans;
}
inline void cheng(ll v){
	For(i,1,a[0])	a[i]*=v;
	For(i,1,a[0]+20){
		a[i+1]+=a[i]/26,a[i]%=26;
		if (a[i])	a[0]=max(a[0],i);
	}
}
int main(){
	freopen("hash.in","r",stdin);
	freopen("hash.out","w",stdout);
	scanf("%llu",&n);	a[1]=a[0]=1;
	For(i,1,n){
		ll x,y;
		scanf("%llu",&x);
		cheng(x/gcd(x,MOD(x)));
	}
	FOr(i,a[0],1)	putchar('a');	puts("");
	FOr(i,a[0],1)	putchar('a'+a[i]);
}

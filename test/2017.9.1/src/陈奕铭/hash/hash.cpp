#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
#define ll long long
#define gc getchar
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

ll mo[6],n,gom,Gcd;
ll bin[10],x,y,cnt;
char s1[10],s2[10];

ll gcd(ll a,ll b){
	if(b == 0) return a;
	return gcd(b,a%b);
}


int main(){
	freopen("hash.in","r",stdin);
	freopen("hash.out","w",stdout);
	n = read();
	for(ll i = 1;i <= n;i++) mo[i] = read();
	gom = mo[1];
	for(ll i = 2;i <= n;i++){
		Gcd = gcd(max(gom,mo[i]),min(gom,mo[i]));
		gom = gom*mo[i]/Gcd;
	}
	bin[0] = 1;
	for(ll i = 1;i <= 7;i++) bin[i] = bin[i-1]*26;
	x = 0; y = gom;
	for(ll i = 7;i >= 0;i--){
		if(y < bin[i]) continue;
		ll b;
		for(b = 0;b < 26;b++)
			if(y < b*bin[i]) break;
		b--;
		s2[cnt++] = 'a'+b;
		y -= b*bin[i];
	}
	for(ll i = 0;i < cnt;i++) s1[i] = 'a';
	s1[cnt] = '\0';
	s2[cnt] = '\0';
	printf("%s\n",s1);
	printf("%s\n",s2);
	return 0;
}
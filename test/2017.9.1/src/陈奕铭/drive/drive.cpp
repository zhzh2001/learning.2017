#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
#define ll long long
#define N 500005
#define mod 1000000007
// #define gc getchar
#define lowbit(x) (x&(-x))

const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

int n;
int tree1[N*4];
int tree2[N*4];
int f[N][5];
int d[N],cnt,a[N];
ll ans;

int erfen(int x){
	int l = 1,r = cnt;
	int mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d[mid] > x) r = mid-1;
		else l = mid+1;
	}
	return r;
}

inline void add1(int x,int v){
	for(int i = x;i <= cnt;i += lowbit(i))
		tree1[i] = (tree1[i]+v)%mod;
}

inline int query1(int x){
	int ans = 0;
	for(int i = x;i;i -= lowbit(i))
		ans = (ans+tree1[i])%mod;
	return ans;
}

inline void add2(int x,int v){
	for(int i = x;i;i -= lowbit(i))
		tree2[i] = (tree2[i]+v)%mod;
}

inline int query2(int x){
	int ans = 0;
	for(int i = x;i <= cnt;i += lowbit(i))
		ans = (ans+tree2[i])%mod;
	return ans;
}

int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n = read();
//
	for(int i = 1;i <= n;i++)
		d[i] = a[i] = read();
	sort(d+1,d+n+1); cnt = 1;
	for(int i = 2;i <= n;i++)
		if(d[i] > d[i-1])
			d[++cnt] = d[i];
	for(int i = 1;i <= n;i++)
		a[i] = erfen(a[i]);
// 数据处理结束
	for(int i = 1;i <= n;i++){
		f[i][1] = query1(a[i]);
		for(int j = 1;j < i;j++)
			if(a[j] > a[i])
				add1(a[j],1);
	}
	memset(tree1,0,sizeof tree1);
	for(int i = n;i >= 1;i--){
		f[i][2] = query1(a[i]);
		for(int j = n;j > i;j--)
			if(a[j] > a[i])
				add1(a[j],1);
	}
	for(int i = 1;i <= n;i++)
		ans = (ans+(ll)(f[i][1]*f[i][2])%mod)%mod;
	printf("%lld\n",ans);
	return 0;
}
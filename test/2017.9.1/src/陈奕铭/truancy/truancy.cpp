#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
#define ll long long
#define gc getchar
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

ll f[1000005];
ll n,k,ans;
ll a[1000005];

int main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
	n = read(); k = read();
	for(ll i = 1;i <= n;i++)
		a[i] = a[i-1] + read();
	
	for(ll i = 1;i <= n;i++){
		for(ll j = 1;j < k;j++)
			f[i] = max(f[i-1],f[i-j-1]+a[i]-a[i-j]);
	}
	printf("%lld\n",f[n]);
	return 0;
}
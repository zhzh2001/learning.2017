#include<bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){   int x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(int x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(int x){  write(x);   puts("");   }
int a[600000],b[600000],n,kk,lazy[3000000],sum[3000000],point[3000000],lsg,l[600000],ans;
map<int,int>mp;
void pushdown(int x){
		lazy[x*2]+=lazy[x];sum[x*2]+=lazy[x]*point[x*2];
		lazy[x*2+1]+=lazy[x];sum[x*2+1]+=lazy[x]*point[x*2+1];
		lazy[x]=0;
	}
int findit(int x,int y,int l,int r,int d){
		if (x<=l&&y>=r)return sum[d];
		if (lazy[d])pushdown(d);
		int m=(l+r)/2,ans=0;
		if (x<=m)ans+=findit(x,y,l,m,d*2);
		if (y>m)ans+=findit(x,y,m+1,r,d*2+1);
		return ans>=lsg?ans-lsg:ans;
	}
void putit(int x,int y,int l,int r,int d){
		if (x<=l&&y>=r){sum[d]+=point[d];lazy[d]++;return;}
		if (lazy[d])pushdown(d);
		int m=(l+r)/2;
		if (x<=m)putit(x,y,l,m,d*2);
		if (y>m)putit(x,y,m+1,r,d*2+1);
		sum[d]=sum[d*2]+sum[d*2+1];point[d]=point[d*2]+point[d*2+1];
	}
void init(int x,int l,int r,int d){
		if (x==l&&x==r){point[d]++;return;}
		if (lazy[d])pushdown(d);
		int m=(l+r)/2;if (x<=m)init(x,l,m,d*2);else init(x,m+1,r,d*2+1);
		point[d]=point[d*2]+point[d*2+1];
	}
signed main(){
	freopen("drive.in","r",stdin);freopen("drive.out","w",stdout);
	n=read();lsg=1e9+7;
	for (int i=1;i<=n;i++)a[i]=b[i]=read();
	sort(b+1,b+1+n);for (int i=1;i<=n;i++)if (b[i]!=b[i-1])mp[b[i]]=++kk;
	for (int i=1;i<=n;i++)a[i]=mp[a[i]];
	for (int i=1;i<=n;i++){
		if (a[i]>1)l[i]=findit(1,a[i]-1,1,kk,1)%lsg;
		if (a[i]<kk)putit(a[i]+1,kk,1,kk,1);
		init(a[i],1,kk,1);
	}
	memset(lazy,0,sizeof(lazy));memset(sum,0,sizeof(sum));memset(point,0,sizeof(point));
	for (int i=n;i>=1;i--){
		if (a[i]>1)(ans+=l[i]*(findit(1,a[i]-1,1,kk,1)%lsg))%=lsg;
		if (a[i]<kk)putit(a[i]+1,kk,1,kk,1);
		init(a[i],1,kk,1);
	}
	writeln(ans);
} 

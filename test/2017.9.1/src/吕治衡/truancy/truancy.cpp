#include<bits/stdc++.h>
#define int long long
int n,k,sum,a[2000000],ans,d[2000000],l,r;
using namespace std;
inline int read(){   int x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(int x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(int x){  write(x);   puts("");   }
signed main(){
	freopen("truancy.in","r",stdin);freopen("truancy.out","w",stdout);
	n=read();k=read();sum=1e15;
	for (int i=1;i<=n;i++)a[i]=read(),ans+=a[i];
	l=1;r=1;for (int i=1;i<=n;i++){
			while (i-d[l]-1>k)l++;a[i]+=a[d[l]];
			if (n-i<=k)sum=min(sum,a[i]);
			while (l<r&&a[d[r]]>=a[i])r--;d[++r]=i;
		}
	writeln(ans-sum);
}

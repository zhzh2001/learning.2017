#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++) 
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 3011 ; 
int n,m ; 
int a[N] ; 
ll f[N][N] ; 
ll ans,mx ; 

int main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout); 
	n = read() ; m = read() ; 
	For(i,1,n) a[ i ] = read() ; 
	For(i,1,n) {
	  f[ i ][ 0 ] = mx ;  
	  For(j,1,m) {
	  	f[ i ][ j ] = f[i-1][j-1] + a[ i ] ; 
	  	if(f[ i ][ j ] > mx ) mx = f[ i ][ j ] ; 
	  }
	}	
	For(i,1,n) 
		For(j,1,m) 
			if(ans < f[i][j]) ans = f[ i ][ j ] ;   
	printf("%lld\n",ans) ; 
}

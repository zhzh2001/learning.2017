#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

inline ll gcd(ll x,ll y) 
{
	if(x%y==0) return y ; 
	return gcd(y,x%y) ; 
}

inline ll lcm(ll x,ll y) 
{
	return x * y / gcd(x,y) ; 
}

const int N = 5011 ; 
int n ; 
ll a[N] ;
ll sum ; 
int b[100],len ;  

inline void out(ll x) 
{
	len = 0 ; 
	if(x==0) b[++len] = 0 ; 
	while(x) {
		b[++len] = x % 26 ; 
		x/=26 ; 
	}
	if(len>1) b[len]-- ; 
	for(int i=len;i>=1;i--) putchar(b[ i ]+97) ;  
	printf("\n") ; 
}

int main(){
	freopen("hash.in","r",stdin);
	freopen("hash.out","w",stdout); 
	n = read() ; 
	if(n>100) {
		printf("aaaaaaaa\n") ; 
		printf("aaaaaaaa\n") ; 
	}
	For(i,1,n) a[ i ] = read() ; 
	sum = lcm( a[ 1 ],a[ 2 ] ) ; 
	For(i,3,n) sum = lcm(sum,a[ i ]) ; 
	ll W = sum,y = 0 ; 
	if(W > 25) {
		W/=25 ; 
		while(W) {
			W/=26 ; 
			y++ ; 
		}
	} 
	ll st = 0 ; 
	if(y!=0) {
		st = 1 ; 
		For(i,1,y) st*=26 ; 
	}
	ll x = st ; 
	y = st + sum ; 
	out(x) ; 
	out(y) ;  
}

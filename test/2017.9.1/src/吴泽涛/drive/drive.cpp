#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define MO(x) if(x >= mod ) x%=mod ; 
#define ll long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 3011,mod = 1e9+7 ; 
int n ; 
int deep[N],Le[N],Ri[N] ; 
ll sum ; 

int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout); 
	n = read() ; 
	For(i,1,n) deep[ i ] = read() ; 
	int x1 = n - 4 , x2 = n - 3 , x3 = n - 2 , x4 = n - 1 ;
	For(i,1,x1) 
		For(j,i+1,x2) 
		  For(k,j+1,x3) 
		    if( deep[ i ] > deep[ j ] && deep[ i ] < deep[ k ] ) {
				Le[ k ]++ ; 
				Le[ k ]%=mod ; 
			}
	For(k,3,x3) 
		For(l,k+1,x4) 
		  For(m,l+1,n) 
			if( deep[ l ] < deep[ m ] && deep[ k ] > deep[ m ] ) {
				Ri[ k ]++ ; 
				Ri[ k ]%=mod ; 
			}
	For(i,3,x3) {
		sum+=(ll)Le[ i ] * Ri[ i ] ; 
		sum = sum % mod ; 
	}
	printf("%lld\n",sum) ; 
	return 0 ; 
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=500005;
const int M=1000000007;
long long ans;
int n;
int a[N];
long long f[N],g[N];
inline int read(){
	int x=0,f=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=getchar())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout); 
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++){
		int x=0;
		for(int j=i+1;j<=n;j++){
			if(a[j]<a[i])x++;
			else if(a[j]>a[i])f[j]=(f[j]+x)%M;
		}
	}
	for(int i=n;i>=1;i--){
		int x=0;
		for(int j=i-1;j>=1;j--){
			if(a[j]<a[i])x++;
			else if(a[j]>a[i])g[j]=(g[j]+x)%M;
		}
	}
//	for(int i=1;i<=n;i++)
//		cout<<i<<' '<<f[i]*g[i]<<endl;
	for(int i=1;i<=n;i++)
		ans=(ans+(1LL*f[i]*g[i])%M)%M;
	printf("%lld",ans);
	return 0;
} 

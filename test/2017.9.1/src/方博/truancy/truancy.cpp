#include<bits/stdc++.h>
using namespace std;
const int N=1000000+5;
struct xxx{
	int t,k;
}q[N];
long long f[N];
int a[N];
long long s[N];
int n,k;
long long ans;
inline int read(){
	int x=0,f=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=getchar())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int main()
{
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
 	n=read();
	k=read();
//	n=1000000;
//	k=52;
//	srand(time(0)+k*time(0)-n);
	for(int i=1;i<=n;i++){
		a[i]=read();
		s[i]=s[i-1]+a[i];
	}
	int head=1;
	int tail=1;
	q[1].t=0;
	q[1].k=0;
	for(int i=1;i<=n;i++){
		int tk=q[head].k;
		if(i-tk-1>k)head++;
		tk=q[head].k;
		f[i]=f[tk]+a[i];
		while(f[i]<f[q[tail].k])tail--;
		tail++;
		q[tail].t=a[i];
		q[tail].k=i;
	}
	ans=f[n];
	for(int i=n-1;i>=n-k;i--){
		ans=min(ans,f[i]);
	}
//	cout<<endl;
	printf("%lld",s[n]-ans);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int f[1000010],n,m,a[1000010],q[1000010],s[1000010];
signed main()
{
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read(),s[i]=s[i-1]+a[i];
	int l=1,r=0,dd=0;f[0]=0;
	for(int i=1;i<=m;i++){
		f[i]=f[i-1]+a[i];
		while(l<=r&&f[dd]+s[i]-s[dd+1]>f[q[r]]+s[i]-s[q[r]+1])r--;q[++r]=dd;
		dd=i;
	}
	for(int i=m+1;i<=n;i++){
		while(l<r&&i-m-1>q[l])l++;
		f[i]=max(f[i-1],f[q[l]]+s[i]-s[q[l]+1]);
		while(l<=r&&f[dd]+s[i]-s[dd+1]>f[q[r]]+s[i]-s[q[r]+1])r--;q[++r]=dd;
		dd=i;
	}
	printf("%lld",f[n]);
	return 0;
}
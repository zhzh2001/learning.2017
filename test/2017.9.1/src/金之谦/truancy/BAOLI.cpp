#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
int f[10010][1010],g[1000010],n,m,a[1000010];
int main()
{
	freopen("truancy.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	g[1]=f[1][1]=a[1];
	for(int i=2;i<=n;i++){
		for(int j=0;j<i-1;j++)f[i][1]=g[j]+a[i];
		g[i]=f[i][1];
		for(int j=2;j<=min(i,m);j++)f[i][j]=f[i-1][j-1]+a[i],g[i]=max(g[i],f[i][j]);
	}
	int ans=0;
	for(int i=1;i<=n;i++)ans=max(ans,g[i]);
	printf("%d",ans);
	return 0;
}
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
const ll MOD=1e9+7;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if(S==T)return EOF;
	}return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
ll st[2000010],lp[500010],rp[500010];
int kt[2000010],lt[2000010],rt[2000010],add[2000010];
int n,a[500010],b[500010],cnt;
inline void pushdown(int nod){
	if(!add[nod])return;
	if(lt[nod]!=rt[nod]){
		(st[nod*2]+=(ll)kt[nod*2]*add[nod])%=MOD;
		(st[nod*2+1]+=(ll)kt[nod*2+1]*add[nod])%=MOD;
		add[nod*2]+=add[nod];add[nod*2+1]+=add[nod];
	}add[nod]=0;
}
inline void build(int l,int r,int nod){
	kt[nod]=st[nod]=add[nod]=0;lt[nod]=l;rt[nod]=r;
	if(l==r)return;
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
}
inline void xg1(int x,int nod){
	pushdown(nod);
	if(lt[nod]==rt[nod]){kt[nod]++;return;}
	int mid=lt[nod]+rt[nod]>>1;
	if(x<=mid)xg1(x,nod*2);else xg1(x,nod*2+1);
	kt[nod]=kt[nod*2]+kt[nod*2+1];
}
inline void xg2(int i,int j,int nod){
	pushdown(nod);
	if(lt[nod]>=i&&rt[nod]<=j){
		add[nod]++;(st[nod]+=(ll)kt[nod])%=MOD;
		return;
	}
	int mid=lt[nod]+rt[nod]>>1;
	if(i<=mid)xg2(i,j,nod*2);
	if(j>mid)xg2(i,j,nod*2+1);
	st[nod]=st[nod*2]+st[nod*2+1];
}
inline int ssum(int i,int j,int nod){
	pushdown(nod);
	if(lt[nod]>=i&&rt[nod]<=j)return st[nod];
	int mid=lt[nod]+rt[nod]>>1,ans=0;
	if(i<=mid)ans+=ssum(i,j,nod*2);
	if(j>mid)ans+=ssum(i,j,nod*2+1);
	return ans;
}
int main()
{
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=b[i]=read();
	sort(b+1,b+n+1);cnt=unique(b+1,b+n+1)-b-1;
	for(int i=1;i<=n;i++)a[i]=lower_bound(b+1,b+cnt+1,a[i])-b;
	build(1,cnt,1);
	for(int i=1;i<=n;i++){
		lp[i]=ssum(1,a[i]-1,1);
		xg2(a[i]+1,cnt,1);xg1(a[i],1);
	}
	build(1,cnt,1);
	for(int i=n;i;i--){
		rp[i]=ssum(1,a[i]-1,1);
		xg2(a[i]+1,cnt,1);xg1(a[i],1);
	}ll ans=0;
	for(int i=1;i<=n;i++)(ans+=lp[i]*rp[i])%=MOD;
	printf("%lld",ans);
	return 0;
}
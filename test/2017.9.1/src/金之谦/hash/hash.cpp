#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if(S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if(S==T)return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;char c=gc();
	for(;c<'0'||c>'9';c=gc())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
inline int gcd(int a,int b){return b?gcd(b,a%b):a;}
char c[30010];
int flag=0,n,a[1010],p,q,cnt,as[1010];
inline void check(){
	int fp=0;
	for(int i=1;i<=n;i++)as[i]=0;
	for(int i=1;i<=cnt;i++){
		if(c[i]!='a')fp=1;
		for(int j=1;j<=n;j++)as[j]=(as[j]*26+c[i]-'a')%a[j];
	}
	for(int i=1;i<=n;i++)if(as[i])return;
	if(fp){
		flag=1;
		for(int i=1;i<=cnt;i++)putchar(c[i]);
	}
}
inline void dfs(int x){
	if(flag)return;
	if(x==cnt+1){check();return;}
	for(int i='a';i<='z';i++)c[x]=i,dfs(x+1);
}
signed main()
{
	freopen("hash.in","r",stdin);
	freopen("hash.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	int lcm=a[1];
	for(int i=2;i<=n;i++)lcm=lcm*a[i]/gcd(lcm,a[i]);
	p=26;q=0;cnt=0;
	while(1){
		cnt++;q+=p;p*=26;if(q>lcm)break;
	}
	for(int i=1;i<=cnt;i++)putchar('a');puts("");
	dfs(1);
	return 0;
}
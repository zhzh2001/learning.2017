#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int ans[1000];
int main()
{
	ll n=read();
	while(n>0)
	{
		ans[0]++;
		ans[ans[0]]=n%26;
		n/=26;
	}
	Rep(i,1,ans[0])cout<<ans[i]<<" ";
	return 0;
}


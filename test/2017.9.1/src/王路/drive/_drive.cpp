#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

FILE *fs = fopen("drive.in", "r");

const int L=200005;
char LZH[L],*SP=LZH,*TP=LZH;
inline char gc(){
	if (SP==TP){
		TP=(SP=LZH)+fread(LZH,1,L,fs);
		if (SP==TP) return EOF;
	}
	return *SP++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int MaxN = 500005, Mod = 1e9 + 7;

int dep[MaxN], n;

ofstream fout("drive.out");

struct BIT {
	int tr[MaxN];
	inline int Lowbit(int x) { return x & -x; }
	inline int Query(int x) {
		int res = 0;
		for (; x > 0; x -= Lowbit(x))
			res += tr[x];
		return res;
	}
	inline int Modify(int x, int val) {
		for (; x <= n; x += Lowbit(x))
			tr[x] += val;
	}
	inline void Clear() {
		memset(tr, 0x00, sizeof tr);
	}
} b1, b2;

int l[MaxN], r[MaxN], cp[MaxN];

int main() {
	n = read();
	for (int i = 1; i <= n; ++i) {
		dep[i] = read();
		cp[i] = dep[i];
	}
	sort(cp + 1, cp + n + 1);
	int total = unique(cp + 1, cp + n + 1) - cp;
	for (int i = 1; i <= n; ++i) {
		dep[i] = lower_bound(cp + 1, cp + total, dep[i]) - cp;
	}
	long long ans = 0;
	for (int i = 1; i <= n; ++i) { // j
		l[i] = b1.Query(dep[i]);
		for (int j = i - 1; j >= 1; --j) { // i
			if (dep[i] > dep[j]) {
				b1.Modify(dep[i], 1);
			}
		}
	}
	for (int i = n; i >= 1; --i) { // l
		r[i] = b2.Query(dep[i]);
		for (int j = i + 1; j <= n; ++j) { // m
			if (dep[i] < dep[j]) {
				b2.Modify(dep[j], 1);
			}
		}
	}
	for (int i = 1; i <= n; ++i) { // k
	cout << l[i] << ' ' << r[i] << endl;
		ans += ((long long)l[i] * r[i] % Mod);
		ans %= Mod;
	}
	fout << ans << endl;
	return 0;
}
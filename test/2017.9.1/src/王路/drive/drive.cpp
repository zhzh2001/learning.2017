#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

FILE *fs = fopen("drive.in", "r");

const int L=200005;
char LZH[L],*SP=LZH,*TP=LZH;
inline char gc(){
	if (SP==TP){
		TP=(SP=LZH)+fread(LZH,1,L,fs);
		if (SP==TP) return EOF;
	}
	return *SP++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int MaxN = 500005, Mod = 1e9 + 7;

int dep[MaxN];

ofstream fout("drive.out");

int main() {
	int n = read();
	for (int i = 1; i <= n; ++i) {
		dep[i] = read();
	}
	long long ans = 0;
	for (int i = 1; i <= n; ++i) { // k
		int cnt1 = 0, cnt2 = 0;
		for (int j = i - 1; j >= 1; --j) { // j
			for (int k = j - 1; k >= 1; --k) { // i
				if (dep[j] < dep[k] && dep[k] < dep[i]) {
					cnt1++;
				}
			}
		}
		for (int j = i + 1; j <= n; ++j) { // l
			for (int k = j + 1; k <= n; ++k) { // m
				if (dep[j] < dep[k] && dep[k] < dep[i]) {
					cnt2++;
				}
			}
		}
		// cout << i << ' ' << cnt1 << ' ' << cnt2 << endl;
		ans += ((long long)cnt1 * cnt2 % Mod);
		ans %= Mod;
	}
	fout << ans << endl;
	return 0;
}
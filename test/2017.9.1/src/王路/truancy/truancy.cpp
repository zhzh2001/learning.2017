#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <queue>
#include <vector>
#include <functional>
#include <list>
using namespace std;

const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int MaxN = 1000005;

struct Node {
	int dat, id;
} a[MaxN], *ord[MaxN];

inline bool cmp(const Node *a, const Node *b) {
	return a->dat < b->dat;
}

long long f[1005][1005];

int main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout); 
	int n = read(), k = read();
	for (int i = 1; i <= n; ++i) {
		a[i].dat = read();
		a[i].id = i;
	}
	for (int i = 0; i <= n; ++i) {
		for (int j = 0; j <= k; ++j) {
			if (j > i) break;
			if (j + 1 <= k) f[i + 1][j + 1] = f[i][j] + a[i + 1].dat;
			f[i + 1][0] = max(f[i + 1][0], f[i][j]);
		}
	}
	long long ans = 0;
	for (int i = 0; i <= k; ++i) {
		ans = max(f[n][i], ans);
	}
	printf("%lld\n", ans);
	return 0;
}

#include <fstream>
#include <string>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;

const int L=200005;
char LZH[L],*SP=LZH,*TP=LZH;
inline char gc(){
	if (SP==TP){
		TP=(SP=LZH)+fread(LZH,1,L,stdin);
		if (SP==TP) return EOF;
	}
	return *SP++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int MaxN = 30005;

long long mo[MaxN], hsh[3][MaxN];
char S[MaxN], T[MaxN];
long long bas[MaxN];

int main() {
	freopen("hash.in", "r", stdin);
	freopen("hash.out", "w", stdout);
	srand(time(0));
	int n = read();
	for (int i = 1; i <= n; ++i) {
		mo[i] = read();
	}
	int len = 0;
	ios_base::sync_with_stdio(false);
	for (int i = 1; i <= n; ++i) {
		cout << 'a';
	}
	cout << endl;
	for (int i = 1; i <= n; ++i) {
		cout << char(rand() % 26 + 'a');
	}
	cout << endl;
	return 0;
}
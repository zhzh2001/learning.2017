#include <iostream>
#include <fstream>
#include <string>
using namespace std;

const int MaxN = 500005;

int hsh[3][MaxN], mo[MaxN];

int n;
string S, T;

bool Hash() {
	cin >> n;
	for (int i = 1; i <= n; ++i) {
		cin >> mo[i];
	}
	cin >> S >> T;
	if (S.length() != T.length()) return false;
	int base = 26;
	for (int i = 0; i < S.length(); ++i) {
		for (int j = 1; j <= n; ++j) {
			hsh[1][j] = (hsh[1][j] * base + S[i] - 'a') % mo[j];
			hsh[2][j] = (hsh[2][j] * base + T[i] - 'a') % mo[j];
		}
	}
	for (int i = 1; i <= n; ++i) {
		if (hsh[1][i] != hsh[2][i]) return false;
	}
	return true;
}

int main() {
	ios_base::sync_with_stdio(false);
	cout << (Hash() ? "True" : "False") << endl;
	cout << (S == T ? "True" : "False") << endl;
	return 0;
}
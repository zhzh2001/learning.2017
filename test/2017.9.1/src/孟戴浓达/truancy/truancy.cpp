#include<iostream>
#include<algorithm>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("truancy.in");
ofstream fout("truancy.out");
int num[1000003];
long long sum[1000003];
long long f[1000003][2];
struct tree{
	long long mn;
}t[8000003];
inline void pushup(int rt){
	t[rt].mn=max(t[rt*2].mn,t[rt*2+1].mn);
}
void build(int rt,int l,int r){
	t[rt].mn=-999999999999999999;
	if(l==r){
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid);
	build(rt*2+1,mid+1,r);
}
void update(int rt,int l,int r,int x,long long val){
	if(l==r){
		t[rt].mn=val;
		return;
	}
	int mid=(l+r)/2;
	if(x>mid){
		update(rt*2+1,mid+1,r,x,val);
	}else{
		update(rt*2,l,mid,x,val);
	}
	pushup(rt);
}
long long query(int rt,int l,int r,int x,int y){
	if(x==l&&r==y){
		return t[rt].mn;
	}
	int mid=(l+r)/2;
	if(x>mid){
		return query(rt*2+1,mid+1,r,x,y);
	}else if(y<=mid){
		return query(rt*2,l,mid,x,y);
	}else{
		return max(query(rt*2,l,mid,x,mid),query(rt*2+1,mid+1,r,mid+1,y));
	}
}
int n,k;
int main(){
	cin>>n>>k;
	for(int i=1;i<=n;i++){
		cin>>num[i];
		sum[i]=sum[i-1];
		sum[i]+=num[i];
	}
	build(1,1,n);
	f[1][0]=0;
	f[1][1]=num[1];
	update(1,1,n,1,f[1][0]);
	for(int i=2;i<=n;i++){
		f[i][0]=max(f[i-1][0],f[i-1][1]);
		f[i][1]=max(f[i][1],query(1,1,n,max(1,i-k),i-1)+sum[i]);
		f[i][0]=max(f[i][0],query(1,1,n,max(1,i-k),i-1)+sum[i-1]);
		update(1,1,n,i,f[i][0]-sum[i]);
	}
	cout<<max(f[n][0],f[n][1])<<endl;
	return 0;
}
/*

in:
5 3
1 3 2 5 4

out:
13

*/ 

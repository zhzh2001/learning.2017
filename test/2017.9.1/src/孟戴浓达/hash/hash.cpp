#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<fstream>
using namespace std;
ifstream fin("hash.in");
ofstream fout("hash.out");
int n;
long long mo[1003];
using namespace std;
long long gcd(long long a,long long b){
	if(b==0){
		return a;
	}else{
		return gcd(b,a%b);
	}
}
char write[23],len;
int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>mo[i];
	}
	sort(mo+1,mo+n+1);
	unsigned long long ans=1;
	for(int i=1;i<=n;i++){
		ans=ans*mo[i]/gcd(ans,mo[i]);
	}
	unsigned long long base[103];
	base[0]=1;
	for(int i=1;i<=14;i++){
		base[i]=base[i-1]*26;
	}
	bool yes=false;
	for(long long i=14;i>=0;i--){
		for(long long j=25;j>=0;j--){
			if(ans>=(unsigned long long)j*base[i]){
				if(j==0){
					if(yes){
						write[++len]=(char)('a');
					}
				}else{
					ans-=j*base[i];
					yes=true;
					write[++len]=(char)((int)('a'+j));
					break;
				}
			}
		}
	}
	for(int i=1;i<=len;i++){
		cout<<"a";
	}
	cout<<endl;
	for(int i=1;i<=len;i++){
		cout<<write[i];
	}
	return 0;
}
/*

in:
2
2 3

out:
a
g

*/

#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define mod 1000000007
#define N 500005
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,a[N],L[N],R[N];
ll sum1[N],sum2[N],ans;
inline void update(ll &x,ll y){
	if(x+y>mod) x=(x+y)%mod;
	else x=x+y;
}
inline void solve(){
	For(i,1,n) For(j,i+1,n) For(k,j+1,n){
		if(a[j]<a[i]&&a[i]<a[k]) sum1[k]++;
	}
	For(i,1,n) For(j,i+1,n) For(k,j+1,n){
		if(a[j]<a[k]&&a[k]<a[i]) sum2[i]++;
	}
}
int main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read();
	solve();
	For(i,1,n) update(ans,sum1[i]*sum2[i]);
	writeln(ans);
	return 0;
}
#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define int ll
#define mod 1000000007
#define N 500005
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,a[N],t[N],hash[N],m=1;
ll sum1[N],sum2[N],T[N];
inline int find(int x){
	int l=1,r=m;
	while(l<=r){
		int mid=(l+r)>>1;
		if(hash[mid]==x) return mid;
		if(hash[mid]>x) r=mid-1;
		else l=mid+1;
	}
}
inline void add(int x){
	for(;x<=n;x+=(x&-x)) t[x]++;
}
inline void Add(int x,int val){
	for(;x<=n;x+=(x&-x)) T[x]+=val;
}
inline int query(int x){
	int sum=0;
	for(;x;x-=(x&-x)) sum+=t[x];
	return sum;
}
inline ll Query(int x){
	ll sum=0;
	for(;x;x-=(x&-x)) sum+=T[x];
	return sum;
}
inline void solve1(){
	For(i,1,n){
		ll p=query(a[i]-1),q=Query(a[i]-1);
		sum1[i]=(p*(p-1)/2-q)%mod;
		add(a[i]);Add(a[i],p);
	}
}
inline void solve2(){
	memset(t,0,sizeof t);
	memset(T,0,sizeof T);
	Rep(i,n,1){
		ll p=query(a[i]-1),q=Query(a[i]-1);
		sum2[i]=(p*(p-1)/2-q)%mod;
		add(a[i]);Add(a[i],p);
	}
}
signed main(){
	freopen("drive.in","r",stdin);
	freopen("drive.out","w",stdout);
	n=read();
	For(i,1,n) hash[i]=a[i]=read();
	sort(hash+1,hash+1+n);
	For(i,2,n) if(hash[i]!=hash[m]) hash[++m]=hash[i];
	For(i,1,n) a[i]=find(a[i]);
	solve1();solve2();ll ans=0;
	For(i,1,n) ans=(ans+sum1[i]*sum2[i]%mod)%mod;
	writeln(ans);
	return 0;
}
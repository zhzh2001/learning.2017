#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define inf 1e18
#define N 1000005
#define mp make_pair
#define pa pair<ll,int>
#define int ll
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,k,q[N],h=1,t=1;
ll f[N],sum[N],a[N];
signed main(){
	freopen("truancy.in","r",stdin);
	freopen("truancy.out","w",stdout);
	n=read();k=read();
	For(i,1,n) a[i]=read(),sum[i]=sum[i-1]+a[i];
	q[t]=0;
	For(i,1,n){
		while(h<=t&&i-q[h]-1>k) h++;
		f[i]=f[q[h]]+a[i];
		while(h<=t&&f[i]<f[q[t]]) t--;
		q[++t]=i;
	}
	ll mn=inf;
	For(i,1,n) mn=min(mn,f[i]+sum[n]-sum[((i+k>n)?n:i+k)]);
	writeln(sum[n]-mn);
	return 0;
}
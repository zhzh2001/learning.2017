#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("hash.in");
const int n = 1000;
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<long long> d(1, 5e17);
		fout << d(gen) << ' ';
	}
	return 0;
}
#include <fstream>
#include <map>
#include <cstring>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("hash.in");
ofstream fout("hash.out");
const int L = 20000, LIM = 32767;
struct bigint
{
	int len, dig[L];
	bigint(long long x = 0)
	{
		memset(dig, 0, sizeof(dig));
		len = 0;
		do
			dig[++len] = x % 10;
		while (x /= 10);
	}
	bigint operator*(const bigint &rhs) const
	{
		bigint res;
		res.len = len + rhs.len - 1;
		for (int i = 1; i <= len; i++)
			for (int j = 1; j <= rhs.len; j++)
				res.dig[i + j - 1] += dig[i] * rhs.dig[j];
		for (int i = 1; i <= res.len; i++)
		{
			res.dig[i + 1] += res.dig[i] / 10;
			res.dig[i] %= 10;
		}
		if (res.dig[res.len + 1])
			res.len++;
		return res;
	}
	bigint &operator*=(const bigint &rhs)
	{
		return *this = *this * rhs;
	}
	int operator%(const int x) const
	{
		int res = 0;
		for (int i = len; i; i--)
			res = (res * 10 + dig[i]) % x;
		return res;
	}
	bigint &operator/=(const int x)
	{
		int rem = 0;
		for (int i = len; i; i--)
		{
			int tmp = (rem * 10 + dig[i]) / x;
			rem = (rem * 10 + dig[i]) % x;
			dig[i] = tmp;
		}
		while (len > 1 && dig[len] == 0)
			len--;
		return *this;
	}
};
bigint qpow(bigint a, int b)
{
	bigint ans = 1;
	do
	{
		if (b & 1)
			ans *= a;
		if (b > 1)
			a *= a;
	} while (b /= 2);
	return ans;
}
int main()
{
	int n;
	fin >> n;
	map<long long, int> cnt;
	for (int i = 1; i <= n; i++)
	{
		long long x;
		fin >> x;
		for (long long j = 2; j * j <= x && j < LIM; j++)
			if (x % j == 0)
			{
				int now = 0;
				while (x % j == 0)
				{
					now++;
					x /= j;
				}
				cnt[j] = max(cnt[j], now);
			}
		if (x > 1)
			cnt[x] = max(cnt[x], 1);
	}
	bigint ans = 1;
	for (map<long long, int>::iterator it = cnt.begin(); it != cnt.end(); ++it)
		ans *= qpow(it->first, it->second);
	string s;
	do
	{
		s = char(ans % 26 + 'a') + s;
		ans /= 26;
	} while (ans.len > 1 || ans.dig[1]);
	fout << string(s.length(), 'a') << endl
		 << s << endl;
	return 0;
}
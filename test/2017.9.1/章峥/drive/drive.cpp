#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("drive.in");
ofstream fout("drive.out");
const int N = 500005, MOD = 1e9 + 7;
int n, a[N], b[N];
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T;
int main()
{
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	copy(a + 1, a + n + 1, b + 1);
	sort(b + 1, b + n + 1);
	for (int i = 1; i <= n; i++)
		a[i] = lower_bound(b + 1, b + n + 1, a[i]) - b;
	long long ans = 0;
	for (int i = 3; i <= n - 2; i++)
	{
		long long ans1 = 0, ans2 = 0;
		int c = 0;
		for (int j = 1; j < i; j++)
			if (a[j] < a[i])
			{
				ans1 += c - T.query(a[j]);
				c++;
				T.modify(a[j], 1);
			}
		for (int j = 1; j < i; j++)
			if (a[j] < a[i])
				T.modify(a[j], -1);
		for (int j = i + 1; j <= n; j++)
			if (a[j] < a[i])
			{
				ans2 += T.query(a[j] - 1);
				T.modify(a[j], 1);
			}
		for (int j = i + 1; j <= n; j++)
			if (a[j] < a[i])
				T.modify(a[j], -1);
		ans = (ans + ans1 * ans2) % MOD;
	}
	fout << ans << endl;
	return 0;
}
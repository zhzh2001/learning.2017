#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("truancy.in");
ofstream fout("truancy.out");
const int N = 1005;
int a[N];
long long f[N][N];
int main()
{
	int n, k;
	fin >> n >> k;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	f[0][0] = 0;
	for (int i = 1; i <= n; i++)
	{
		f[i][0] = f[i - 1][0];
		for (int j = 1; j <= k && j <= i; j++)
		{
			f[i][0] = max(f[i][0], f[i - 1][j]);
			f[i][j] = f[i - 1][j - 1] + a[i];
		}
	}
	fout << *max_element(f[n], f[n] + k + 1) << endl;
	return 0;
}
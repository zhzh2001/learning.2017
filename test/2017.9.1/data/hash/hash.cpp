#include<ctime>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
int n,top,s[30005];
ll a[1005],b[1005];
ll mul(ll x,ll y,ll mo){
	ll s=0;
	for (;y;y/=2,x=(x+x)%mo)
		if (y&1) s=(s+x)%mo;
	return s;
}
struct big{
	#define mo 1000000000
	ll a[5000];
	big(int x=0){
		memset(a,0,sizeof(a));
		if (!x) return;
		a[0]=1; a[1]=x;
	}
	void init(){
		memset(a,0,sizeof(a));
	}
	ll& operator [](int x){
		return a[x];
	}
	friend big operator *(big a,ll b){
		big c; c[0]=a[0];
		for (int i=1;i<=c[0];i++) c[i]=a[i]*(b%mo);
		for (int i=1;i<=c[0];i++) c[i+1]+=a[i]*(b/mo);
		for (int i=1;i<=c[0]+2;i++){
			c[i+1]+=c[i]/mo;
			c[i]%=mo;
		}
		for (;c[c[0]+1];c[0]++);
		return c;
	}
	friend big operator /(big a,int b){
		big c; c[0]=a[0]; ll x=0;
		for (int i=a[0];i;i--){
			x=x*mo+a[i];
			c[i]=x/b;
			x%=b;
		}
		for (;c[0]&&!c[c[0]];) c[0]--;
		return c;
	}
	friend ll operator %(big a,ll b){
		ll x=0;
		for (int i=a[0];i;i--)
			x=(mul(x,mo,b)+a[i])%b;
		return x;
	}
}ans(1);
ll gcd(ll x,ll y){
	return y?gcd(y,x%y):x;
}
int main(){
	freopen("hash20.in","r",stdin);
	freopen("hash20.out","w",stdout); 
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%lld",&a[i]);
	for (int i=1;i<=n;i++)
		ans=ans*a[i];
	while (ans[0]) ans=ans/26,top++;
	printf("%d\n",top);
	ans.init();
	ans[0]=ans[1]=1;
	for (int i=1;i<=n;i++) b[i]=a[i];
	for (int i=2;i<=200;i++)
		while(1){
			int fl=0;
			for (int j=1;j<=n;j++)
				if (b[j]%i==0) fl=1;
			if (!fl) break;
			ans=ans*i;
			for (int j=1;j<=n;j++)
				if (b[j]%i==0) b[j]/=i;
		}
	for (int i=1;i<=n;i++) ans=ans*b[i]; 
	top=0;
	while (ans[0]) ans=ans/26,top++;
	printf("%d\n",top);
	ans.init();
	ans[0]=ans[1]=1;
	for (int i=1;i<=n;i++) b[i]=a[i];
	for (int i=2;i<=1000;i++)
		while(1){
			int fl=0;
			for (int j=1;j<=n;j++)
				if (b[j]%i==0) fl=1;
			if (!fl) break;
			ans=ans*i;
			for (int j=1;j<=n;j++)
				if (b[j]%i==0) b[j]/=i;
		}
	for (int i=1;i<=n;i++) ans=ans*b[i]; 
	top=0;
	while (ans[0]) s[++top]=ans%26,ans=ans/26;
	printf("%d\n",top);
	ans.init();
	ans[0]=ans[1]=1;
	for (int i=2;i<=n;i++)
		for (int j=1;j<i;j++){
			ll tmp=gcd(a[i],a[j]);
			a[i]/=tmp;
		}
	top=0;
	for (int i=1;i<=n;i++) ans=ans*a[i];
	while (ans[0]) s[++top]=ans%26,ans=ans/26;
	for (int i=1;i<=top;i++) putchar('a');
	puts("");
	for (;top;top--) putchar('a'+s[top]);
}

#include<cmath>
#include<ctime>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define mo 43219846132874629LL
using namespace std;
long long rnd(){
	long long s=0;
	for (int i=1;i<=20;i++)
		s=(s*10+rand())%mo;
	return s;
}
char s[20];
int a[5005];
int main(){
	srand(925638417);
	s[0]='h'; s[1]='a'; s[2]='s'; s[3]='h';
	s[6]='.'; s[7]='i'; s[8]='n'; 
	for (int p=1;p<=16;p++){
		s[4]='0'+p/10; s[5]='0'+p%10;
		freopen(s,"w",stdout); 
		int n,lim;
		if (p<=4) n=5;
		else if (p<=8) n=20;
		else if (p<=12) n=100;
		else n=5000;
		for (int i=1;i<=n;i++) a[i]=i;
		random_shuffle(a+1,a+n+1);
		if (p<=12) lim=min(n-2,(int)(n-rnd()%(n/5*2)));
		else lim=min(999,1000-(int)(rnd()%200));
		printf("%d\n",lim);
		for (int i=1;i<=lim;i++) printf("%d ",a[i]);
		fclose(stdout);
	}
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ull unsigned long long 
using namespace std;
FILE *Finn,*Fout,*Fstd,*Fres;
char S[50005],T[50005],StdS[50005],StdT[50005];
int Stdlen,Slen,Tlen,fl,n;
ull mo[1005],hash[5][1005];
void Return(double p,char *s){
	int score=floor(p*5+1e-9);
	fprintf(Fres,"%.3lf\n%s",0.2*score+1e-9,s);
	exit(0);
}
int	main(int args, char** argv){
	Finn=fopen(argv[1],"r");
	Fstd=fopen(argv[2],"r");
	Fout=fopen(argv[3],"r");
	Fres=fopen(argv[4],"w");
	fscanf(Finn,"%d",&n);
	for (int i=1;i<=n;i++)
		fscanf(Finn,"%llu",&mo[i]); 
	fscanf(Fout,"%s%s",S+1,T+1);
	int a1,a2,a3;
	fscanf(Fstd,"%d%d%d",&a1,&a2,&a3); 
	fscanf(Fstd,"%s%s",StdS+1,StdT+1);
	Slen=strlen(S+1);
	Tlen=strlen(T+1);
	Stdlen=strlen(StdS+1);
	if (Slen>30000||Tlen>30000)
		Return(0.0,"不合法的输入");
	fl=1;
	for (int i=1;i<=Slen&&i<=Tlen;i++)
		if (S[i]!=T[i]) fl=0;
	if (fl) Return(0.0,"不合法的输入");
	for (int i=1;i<=Slen&&i<=Tlen;i++)
		if (S[i]<'a'||S[i]>'z'||T[i]<'a'||T[i]>'z')
			Return(0.0,"不合法的输入"); 
	if (Slen!=Tlen)
		Return(0.0,"没有卡掉大头"); 
	for (int i=1;i<=Slen;i++)
		for (int j=1;j<=n;j++){
			hash[1][j]=(hash[1][j]*26+S[i]-'a')%mo[j];
			hash[2][j]=(hash[2][j]*26+T[i]-'a')%mo[j]; 
		}
	for (int i=1;i<=n;i++)
		if (hash[1][i]!=hash[2][i])
			Return(0.0,"没有卡掉大头");
	if (Slen!=Stdlen){
		if (Slen<=a3) Return(0.6,"成功卡掉大头");
		else if (Slen<=a2) Return(0.4,"成功卡掉大头");
		else if (Slen<=a1) Return(0.2,"成功卡掉大头");
		else Return(0.0,"成功卡掉大头");
	}
	for (int i=1;i<=Slen;i++)
		if (StdS[i]!=S[i])
			if (S[i]<StdS[i]) Return(1.0,"亦可赛艇");
			else Return(0.8,"成功卡掉大头"); 
	for (int i=1;i<=Slen;i++)
		if (StdT[i]!=T[i])
			if (T[i]<StdT[i]) Return(1.0,"亦可赛艇");
			else Return(0.8,"成功卡掉大头"); 
	Return(1.0,"成功卡掉大头");
}

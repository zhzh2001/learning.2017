#include <cstdio>

const int N=(int)2e5+5,M=N;

int n,m;

char s[N],t[M],ss[N],tt[M];

int next[M];

char mat[N][26];

int tot,ans[N];

int main(){
	freopen("technology.in","r",stdin);
	freopen("technology_.out","w",stdout);
	scanf("%d%d",&n,&m);
	scanf("%s",s+1);
	scanf("%s",t+1);
	for(char x='a';x<='z';x++)
		for(char y=x;y<='z';y++){
			for(int i=1;i<=n;i++) ss[i]=s[i]==x||s[i]==y?s[i]:0;
			for(int i=1;i<=m;i++) tt[i]=t[i]==x?y:t[i]==y?x:0;
			for(int i=2;i<=m;i++){
				int j=next[i-1];
				while(j&&tt[j+1]!=tt[i]) j=next[j];
				if(tt[j+1]==tt[i]) j++;
				next[i]=j;
			}
			for(int i=1,j=0;i<=n;i++){
				while(j&&ss[i]!=tt[j+1]) j=next[j];
				if(ss[i]==tt[j+1]) j++;
				if(j==m){
					mat[i-m+1][x-'a']=y;
					mat[i-m+1][y-'a']=x;
					j=next[j];
				}
			}
		}
	for(int i=1;i<=n;i++){
		bool ok=true;
		for(int x=0;x<26;x++) if(!mat[i][x]){ ok=false; break; }
		if(ok) ans[tot++]=i;
	}
	printf("%d\n",tot);
	for(int i=0;i<tot;i++) printf("%d ",ans[i]);
}

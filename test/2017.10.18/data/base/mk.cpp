#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
const int P=100000000,T=50;
int i,j,k;
int R() {
	return rand()<<15|rand();
}
int main() {
	srand((int) time(0));
	freopen("base.in","w",stdout);
	printf("%d\n",T);
	for (i=1;i<=T;i++) {
		if (R()%5) {
			int l=R()%(P+P)+1;
			int x1=R()%(P+P+1)-P;
			int y1=R()%(P+P+1)-P;
			while (x1+l>P || y1+l>P) {
				l=R()%(P+P)+1;
				x1=R()%(P+P+1)-P;
				y1=R()%(P+P+1)-P;
			}
			int x2=x1,y2=y1+l;
			int x3=x1+l,y3=y1;
			int x4=x1+l,y4=y1+l;
			if (R()&1) x1=R()%(P+P+1)-P;
			else y1=R()%(P+P+1)-P;
			if (R()&1) x2=R()%(P+P+1)-P;
			else y2=R()%(P+P+1)-P;
			if (R()&1) x3=R()%(P+P+1)-P;
			else y3=R()%(P+P+1)-P;
			if (R()&1) x4=R()%(P+P+1)-P;
			else y4=R()%(P+P+1)-P;
			printf("%d %d\n",x1,y1);
			printf("%d %d\n",x2,y2);
			printf("%d %d\n",x3,y3);
			printf("%d %d\n",x4,y4);
		}
		else {
			int x1,y1,x2,y2,x3,y3,x4,y4;
			x1=R()%(P+P+1)-P;
			y1=R()%(P+P+1)-P;
			x2=R()%(P+P+1)-P;
			y2=R()%(P+P+1)-P;
			x3=R()%(P+P+1)-P;
			y3=R()%(P+P+1)-P;
			x4=R()%(P+P+1)-P;
			y4=R()%(P+P+1)-P;
			printf("%d %d\n",x1,y1);
			printf("%d %d\n",x2,y2);
			printf("%d %d\n",x3,y3);
			printf("%d %d\n",x4,y4);
		}
	}
}

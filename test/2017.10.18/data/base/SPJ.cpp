#include <cstdio>
#include <cstdlib>
using namespace std;
const int N=2020;
typedef	long long ll;
int i,j,k,n,m,T;
int x[4],y[4],xx[4],yy[4];
FILE *Finn, *Fout, *Fstd, *Fres;
void Return(double p, char *s) {
	fprintf(Fres, "%.9lf\n%s\n", p, s);
	exit(0);
}
int ab(int x) {
	if (x<0) return -x;
	return x;
}
int max(int x,int y) {
	if (x<y) return y;
	return x;
}
int	main(int args, char** argv) {
	Finn = fopen(argv[1], "r");
	Fstd = fopen(argv[2], "r");
	Fout = fopen(argv[3], "r");
	Fres = fopen(argv[4], "w");
	fscanf(Finn,"%d",&T);
	while (T--) {
		for (i=0;i<4;i++) fscanf(Finn,"%d%d",&xx[i],&yy[i]);
		fscanf(Fout,"%d",&n);
		fscanf(Fstd,"%d",&m);
		if (n!=m) Return(0.0,"WA"); 
		if (m==-1) continue;
		int ma=0;
		for (i=0;i<4;i++) {
			fscanf(Fout,"%d%d",&x[i],&y[i]);
			if (x[i]!=xx[i] && y[i]!=yy[i]) Return(0.0,"WA");
			if (x[i]==xx[i]) ma=max(ma,ab(y[i]-yy[i]));
			if (y[i]==yy[i]) ma=max(ma,ab(x[i]-xx[i]));
		}
		if (ma!=n) Return(0.0,"WA");
		for (i=0;i<4;i++) fscanf(Fstd,"%d%d",&xx[i],&yy[i]);
	}
	Return(1.0,"AC");
}

#include<cstdio>
#include<cstring>
using namespace std;
const int N=100100;
int i,j,k,n,T,x;
char s[N];
int a[N];
int main() {
	freopen("game.in","r",stdin);
	freopen("game_.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%s",s+1);
		n=strlen(s+1);
		for (i=0;i<=n;i++) a[i]=0;
		for (i=1;i<=n;i++) if (s[i]=='W') x=i;
		j=0;
		while (j+1<x && s[j+1]==s[1]) j++;
		int t=j;
		if (s[1]=='w') t=-t;
		a[0]+=t;
		for (j=j+1,k=1;j<x;j++,k++) {
			if (s[j]=='w') a[k]--;
			else a[k]++;
		}
		j=n+1;
		while (j-1>x && s[j-1]==s[n]) j--;
		t=n-j+1;
		if (s[n]=='w') t=-t;
		a[0]+=t;
		for (j=j-1,k=1;j>x;j--,k++) {
			if (s[j]=='w') a[k]--;
			else a[k]++;
		}
		for (i=n;i;i--) {
			while (a[i]>1) a[i]-=2,a[i-1]++;
			while (a[i]<0) a[i]+=2,a[i-1]--;
		}
		for (i=0;i<=n;i++) if (a[i]) break;
		if (i>n) puts("no");
		else {
			if (a[i]>0) puts("yes");
			else puts("no");
		}
	}
}

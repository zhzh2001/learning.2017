#include<cstdio>
#include<cstring>
using namespace std;
const int N=5050;
int i,j,k,n,m,T,x;
char s[N];
int sb[N],sw[N];
bool f[N][N][2];
int g[N][2][2];
int main() {
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%s",s+1);
		n=strlen(s+1);
		for (i=1;i<=n;i++)
			for (j=0;j<2;j++)
				for (k=0;k<2;k++) g[i][j][k]=0;
		for (i=1;i<=n;i++) {
			if (s[i]=='W') x=i,s[i]='w';
			if (s[i]=='b') sb[i]=1,sw[i]=0;
			else sw[i]=1,sb[i]=0;
			sb[i]+=sb[i-1];
			sw[i]+=sw[i-1];
		}
		for (k=n;k;k--) {
			for (i=1;i+k-1<=n;i++) {
				j=i+k-1;
				if (s[i]=='w') {
					if (sb[i-1]+sb[n]-sb[j]==0) f[i][j][0]=0;
					else {
						if ((g[i][1][1]|g[j][0][1])&2) f[i][j][0]=1;
						else f[i][j][0]=0;
					}
				}
				else {
					if (sw[i-1]+sw[n]-sw[j]==0) f[i][j][0]=1;
					else {
						if ((g[i][1][0]|g[j][0][0])&1) f[i][j][0]=0;
						else f[i][j][0]=1;
					}
				}
				if (s[j]=='w') {
					if (sb[i-1]+sb[n]-sb[j]==0) f[i][j][1]=0;
					else {
						if ((g[i][1][1]|g[j][0][1])&2) f[i][j][1]=1;
						else f[i][j][1]=0;
					}
				}
				else {
					if (sw[i-1]+sw[n]-sw[j]==0) f[i][j][1]=1;
					else {
						if ((g[i][1][0]|g[j][0][0])&1) f[i][j][1]=0;
						else f[i][j][1]=1;
					}
				}
			}
			for (i=1;i+k-1<=n;i++) {
				j=i+k-1;
				int t0=f[i][j][0],t1=f[i][j][1];
				t0=1<<t0;t1=1<<t1;
				if (s[i]=='w') g[j][0][0]|=t0;
				else g[j][0][1]|=t0;
				if (s[j]=='w') g[i][1][0]|=t1;
				else g[i][1][1]|=t1;
			}
		}
		if (f[x][x][0]) puts("yes");
		else puts("no");
	}
}

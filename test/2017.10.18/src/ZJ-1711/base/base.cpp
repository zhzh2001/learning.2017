#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 10005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline int read(){
    ll x=0,f=1; char ch=getchar();
    while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
    while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
    return x*f;
}
ll T,ans,a1,a2,x[N],y[N],a[N][2],q[N],vis[N],pos[N][2];
void judge(ll p1,ll p2){
	ll tmp=ans,ttt=0;
	if ((x[p1]-a[q[1]][0])*(y[p1]-a[q[1]][1])==0&&(x[p2]-a[q[2]][0])*(y[p1]-a[q[2]][1])==0){
		if ((x[p2]-a[q[3]][0])*(y[p2]-a[q[3]][1])==0&&(x[p1]-a[q[4]][0])*(y[p2]-a[q[4]][1])==0){
			ttt=max(ttt,abs(a[q[1]][0]-x[p1]));
			ttt=max(ttt,abs(a[q[1]][1]-y[p1]));
			ttt=max(ttt,abs(a[q[2]][0]-x[p2]));
			ttt=max(ttt,abs(a[q[2]][1]-y[p1]));
			ttt=max(ttt,abs(a[q[3]][0]-x[p2]));
			ttt=max(ttt,abs(a[q[3]][1]-y[p2]));
			ttt=max(ttt,abs(a[q[4]][0]-x[p1]));
			ttt=max(ttt,abs(a[q[4]][1]-y[p2]));
			ans=min(ans,ttt);
			if (ans!=tmp){
				pos[q[1]][0]=x[p1]; pos[q[1]][1]=y[p1];
				pos[q[2]][0]=x[p2]; pos[q[2]][1]=y[p1];
				pos[q[3]][0]=x[p2]; pos[q[3]][1]=y[p2];
				pos[q[4]][0]=x[p1]; pos[q[4]][1]=y[p2];
			}
		}
	}
	tmp=ans; ttt=0;
	if ((x[p2]-a[q[1]][0])*(y[p1]-a[q[1]][1])==0&&(x[p1]-a[q[2]][0])*(y[p1]-a[q[2]][1])==0){
		if ((x[p1]-a[q[3]][0])*(y[p2]-a[q[3]][1])==0&&(x[p2]-a[q[4]][0])*(y[p2]-a[q[4]][1])==0){
			ttt=max(ttt,abs(a[q[1]][0]-x[p2]));
			ttt=max(ttt,abs(a[q[1]][1]-y[p1]));
			ttt=max(ttt,abs(a[q[2]][0]-x[p1]));
			ttt=max(ttt,abs(a[q[2]][1]-y[p1]));
			ttt=max(ttt,abs(a[q[3]][0]-x[p1]));
			ttt=max(ttt,abs(a[q[3]][1]-y[p2]));
			ttt=max(ttt,abs(a[q[4]][0]-x[p2]));
			ttt=max(ttt,abs(a[q[4]][1]-y[p2]));
			ans=min(ans,ttt);
			if (ans!=tmp){
				pos[q[1]][0]=x[p2]; pos[q[1]][1]=y[p1];
				pos[q[2]][0]=x[p1]; pos[q[2]][1]=y[p1];
				pos[q[3]][0]=x[p1]; pos[q[3]][1]=y[p2];
				pos[q[4]][0]=x[p2]; pos[q[4]][1]=y[p2];
			}
		}
	}
}
void dfs(ll now,ll p1,ll p2){
	if (now==5) { judge(p1,p2); return; }
	rep(i,1,4) if (!vis[i]){
		vis[i]=1;
		q[now]=i; dfs(now+1,p1,p2);
		vis[i]=0;
	}
}
int main(){
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	T=read();
	while (T--){
		ll cnt=0; ans=1ll<<40;
		rep(i,1,4) a[i][0]=read(),a[i][1]=read();
		rep(i,1,4) rep(j,1,4) x[++cnt]=a[i][0],y[cnt]=a[j][1];
		rep(i,1,cnt) rep(j,1,cnt) 
		if (i!=j&&(x[i]-x[j])&&(y[i]-y[j])&&(abs(x[i]-x[j])==abs(y[i]-y[j]))){
			dfs(1,i,j);
		}
		if (ans==(1ll<<40)) puts("-1");
		else{
			printf("%lld\n",ans);
			rep(i,1,4) printf("%lld %lld\n",pos[i][0],pos[i][1]);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline int read(){
    ll x=0,f=1; char ch=getchar();
    while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
    while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
    return x*f;
}
ll n,m,f[N],flag[N],ans[N]; char a[N],b[N];
int main(){
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	n=read(); m=read();
	scanf("%s",a+1); scanf("%s",b+1);
	rep(i,1,n-m+1){
		rep(j,0,20) f[j]=j;
		memset(flag,0,sizeof flag);
		rep(j,1,m){
			ll x=a[i+j-1]-'a',y=b[j]-'a';
			if (f[x]!=y) if (flag[x]||flag[y]) break;
							else{
								f[x]=y; f[y]=x;
								flag[x]=1; flag[y]=1;
							}
			if (j==m) ans[++ans[0]]=i;
		}
	}
	printf("%d\n",ans[0]);
	rep(i,1,ans[0]) printf("%d ",ans[i]);
}

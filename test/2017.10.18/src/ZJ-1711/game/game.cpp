#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 10005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline int read(){
    ll x=0,f=1; char ch=getchar();
    while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
    while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
    return x*f;
}
ll T; char s[N];
ll dfs(ll l,ll r){
	if (l==r) return 0;
	if (s[l]=='b'&&s[r]=='b') return 1;
	if ((s[l]=='w'||s[l]=='W')&&(s[r]=='w'||s[r]=='W')) return 0;
	if (s[l]=='W'||s[r]=='W') return 1;
	return dfs(l+1,r-1);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	T=read();
	while (T--){
		scanf("%s",s+1);
		if (dfs(1,strlen(s+1))) puts("yes");
		else puts("no");
	}
}


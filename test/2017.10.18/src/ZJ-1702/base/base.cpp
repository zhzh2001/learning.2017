#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 5005
#define inf 0x7fffffff
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

struct node{
	int x,y;
}a[5],b[5],c[5],d[5];
int x1,y1,x2,y2;
double bian;
int num[5],fx[5];		//fx = 0左右(改变x),fx = 1上下(改变y) 
int ans;
bool vis[5];

void dfs(int step){
	if(step == 5){
		//判断x1,y1,x2,y2的合法性，以及能确定的值
		x1 = x2 = y1 = y2 = inf; 
		if(fx[1] == 0)
			y1 = b[1].y;
		else x1 = b[1].x;
	
		if(fx[2] == 0)
			y2 = b[2].y;
		else{
			if(x1 == inf) x1 = b[2].x;
			else if(x1 != b[2].x) return;
		}
	
		if(fx[3] == 0){
			if(y1 == inf) y1 = b[3].y;
			else if(y1 != b[3].y) return;
		}
		else x2 = b[3].x;
		
		if(fx[4] == 0){
			if(y2 == inf) y2 = b[4].y;
			else if(y2 != b[4].y) return;
		}
		else{
			if(x2 == inf) x2 = b[4].x;
			else if(x2 != b[4].x) return;
		}
		
		//计算边长是否合法
		bian = 0;
		if(x1 != inf && x2 != inf)
			bian = x2-x1;
		if(y1 != inf && y2 != inf){
			if(bian == 0) bian = y2-y1;
			else if(bian != y2-y1) return;
		}
		if(bian <= 0) return;
		
		int sum = 0;
		if(x1 == inf && x2 == inf){
			int mx = max(b[1].x,max(b[2].x,max(b[3].x,b[4].x)));
			int mi = min(b[1].x,min(b[2].x,min(b[3].x,b[4].x)));
			double x = (mx+mi)/2;
			x2 = x+bian/2;
			x1 = x-bian/2;
		}
		else if(y1 == inf && y2 == inf){
			int mx = max(b[1].y,max(b[2].y,max(b[3].y,b[4].y)));
			int mi = min(b[1].y,min(b[2].y,min(b[3].y,b[4].y)));
			double y = (mx+mi)/2;
			y2 = y+bian/2;
			y1 = y-bian/2;
		}
		else{
			//计算得出4个点位 
			if(x1 != inf) x2 = x1+bian;
			if(x2 != inf) x1 = x2-bian;
			if(y1 != inf) y2 = y1+bian;
			if(y2 != inf) y1 = y2-bian;
		}
		if(x1 != b[1].x && y1 != b[1].y) return;
		if(x1 != b[2].x && y2 != b[2].y) return;
		if(x2 != b[3].x && y1 != b[3].y) return;
		if(x2 != b[4].x && y2 != b[4].y) return;
		sum = max(sum,abs(x1-b[1].x));
		sum = max(sum,abs(y1-b[1].y));
		sum = max(sum,abs(x1-b[2].x));
		sum = max(sum,abs(y2-b[2].y));
		sum = max(sum,abs(x2-b[3].x));
		sum = max(sum,abs(y1-b[3].y));
		sum = max(sum,abs(x2-b[4].x));
		sum = max(sum,abs(y2-b[4].y));
		if(sum >= ans) return;
		ans = sum;
		d[1].x = x1; d[1].y = y1;
		d[2].x = x1; d[2].y = y2;
		d[3].x = x2; d[3].y = y1;
		d[4].x = x2; d[4].y = y2;
		for(int i = 4;i >= 1;i--){
			c[i].x = d[num[i]].x;
			c[i].y = d[num[i]].y;
		}
//		puts("1#");
//		printf("%d\n",ans);
//			for(int i = 1;i <= 4;i++)
//				printf("%d %d\n",c[i].x,c[i].y);
//		puts("");
		return;
	}
	for(int i = 1;i <= 4;i++)
		if(!vis[i]){
			num[i] = step;
			b[step].x = a[i].x;
			b[step].y = a[i].y;
			vis[i] = true;
			dfs(step+1);
			vis[i] = false;
		}
}

int main(){
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	int T = read();
	while(T--){
		ans = inf;
		for(int i = 1;i <= 4;i++){
			a[i].x = read(); a[i].y = read();
		}
		memset(vis,0,sizeof vis);
		for(int i1 = 0;i1 < 2;i1++){
			fx[1] = i1;
			for(int i2 = 0;i2 < 2;i2++){
				fx[2] = i2;
				for(int i3 = 0;i3 < 2;i3++){
					fx[3] = i3;
					for(int i4 = 0;i4 < 2;i4++){
						fx[4] = i4;
						dfs(1);
					}
				}
			}
		}
		if(ans == inf) puts("-1");
		else{
			printf("%d\n",ans);
			for(int i = 1;i <= 4;i++)
				printf("%d %d\n",c[i].x,c[i].y);
//			printf("%d %d %d %d %d\n",ab,ax1,ay1,ax2,ay2);
		}
	}
	return 0;
}

#include<iostream>
#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

int len = 15;

int main(){
	freopen("game.in","r",stdin);
	srand(time(0));
	int T = 10;
	while(T--){
		int x = rand()%len;
		for(int i = 0;i < len;i++)
			if(i != x){
				int y = rand()%2;
				if(y == 1) putchar('b');
				else putchar('w');
			}
			else putchar('W');
		puts("");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
#define N 5005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

char s[N];
int len,st;
int lb[N],lw[N],rb[N],rw[N];
//int a[N];
//int firstb,firstw;

bool dfs(int x,int pat,int y){
//	if((x == 1 && y == len) || (x == len && y == 1))
//		return false;
	if(pat == 1){
//		if(x > y) swap(x,y);
		int last = lb[x];
		while(last){
			if(!dfs(last,0,y))
				return true;
			last = lb[last];
		}
		last = rb[y];
		while(last != len+1){
			if(!dfs(x,0,last))
				return true;
			last = rb[last];
		}
		return false;
	}
	else{
//		if(x > y) swap(x,y);
		int last = lw[x];
		while(last){
			if(!dfs(last,1,y))
				return true;
			last = lw[last];
		}
		last = rw[y];
		while(last != len+1){
			if(!dfs(x,1,last))
				return true;
			last = rw[last];
		}
		return false;
	}
}

int main(){
//	freopen("game.in","r",stdin);
//	freopen("game2.out","w",stdout);
	int T = read();
	while(T--){
		scanf("%s",s+1);
		len = strlen(s+1);
		int lastb = 0,lastw = 0;
		for(int i = 1;i <= len;i++){
			lb[i] = lastb;
			lw[i] = lastw;
			if(s[i] == 'W'){
				st = i;
				lastw = i;
			}
			else if(s[i] == 'w')
				lastw = i;
			else lastb = i;
		}
		lastb = len+1; lastw = len+1;
		for(int i = len;i > 0;i--){
			rb[i] = lastb;
			rw[i] = lastw;
			if(s[i] == 'W' || s[i] == 'w')
				lastw = i;
			else lastb = i;
		}
		if(dfs(st,1,st))
			puts("yes");
		else puts("no");
	}
	return 0;
}

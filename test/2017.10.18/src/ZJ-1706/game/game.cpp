#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int sx,f[3010][3010][2],n;
char s[100010];
inline int dfs(int l,int r,int k){
	if(f[l][r][k]!=-1)return f[l][r][k];
	for(int i=l-1;i;i--){
		if(k==0&&s[i]=='b'){
			int p=dfs(i,r,1);
			if(p==0)return f[l][r][k]=1;
		}
		if(k==1&&s[i]=='w'){
			int p=dfs(i,r,0);
			if(p==0)return f[l][r][k]=1;
		}
	}
	for(int i=r+1;i<=n;i++){
		if(k==0&&s[i]=='b'){
			int p=dfs(l,i,1);
			if(p==0)return f[l][r][k]=1;
		}
		if(k==1&&s[i]=='w'){
			int p=dfs(l,i,0);
			if(p==0)return f[l][r][k]=1;
		}
	}
	return f[l][r][k]=0;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int T=read();
	while(T--){
		scanf("%s",s+1);n=strlen(s+1);
		for(int i=1;i<=n;i++)if(s[i]=='W')sx=i,s[i]='w';
		for(int i=1;i<=n;i++)for(int j=i;j<=n;j++)f[i][j][0]=f[i][j][1]=-1;
		dfs(sx,sx,0);
		puts(f[sx][sx][0]==1?"yes":"no");
	}
	return 0;
}

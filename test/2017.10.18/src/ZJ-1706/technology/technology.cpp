#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <bitset>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,ans=0;
int nex[200010],s[200010];
char p[128],c1[200010],c2[200010];
int main()
{
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	n=read();m=read();
	scanf("%s",c1+1);scanf("%s",c2+1);
	bool flag=1;
	for(int i=1;i<=n;i++)if(c1[i]>'b')flag=0;
	for(int i=1;i<=m;i++)if(c2[i]>'b')flag=0;
	if(flag){
		int j;
		nex[1]=j=0;
		for(int i=2;i<=m;i++){
			while(j&&c2[i]!=c2[j+1])j=nex[j];
			if(c2[i]==c2[j+1])j++;
			nex[i]=j;
		}
		j=0;
		for(int i=1;i<=n;i++){
			while(j&&c1[i]!=c2[j+1])j=nex[j];
			if(c1[i]==c2[j+1])j++;
			if(j==m)s[++ans]=i-j+1;
		}
		for(int i=1;i<=n;i++)c1[i]=(c1[i]=='a')?'b':'a';
		j=0;
		for(int i=1;i<=n;i++){
			while(j&&c1[i]!=c2[j+1])j=nex[j];
			if(c1[i]==c2[j+1])j++;
			if(j==m)s[++ans]=i-j+1;
		}
		printf("%d\n",ans);
		sort(s+1,s+ans+1);
		for(int i=1;i<=ans;i++)printf("%d ",s[i]);
	}else{
		for(int i=1;i<=n-m+1;i++){
			memset(p,-1,sizeof p);
			flag=1;
			for(int j=1;j<=m;j++)if(c1[i+j-1]!=c2[j]){
				if(p[c2[j]]==c1[i+j-1]&&p[c1[i+j-1]]==c2[j])continue;
				if(p[c2[j]]==-1&&p[c1[i+j-1]]==-1){p[c2[j]]=c1[i+j-1],p[c1[i+j-1]]=c2[j];continue;}
				flag=0;break;
			}else{
				if(p[c2[j]]==-1&&p[c1[i+j-1]]==-1)p[c2[j]]=p[c1[i+j-1]]=c1[i+j-1];
				else if(p[c2[j]]!=c2[j]||p[c1[i+j-1]]!=c2[j]){flag=0;break;}
			}
			if(flag)s[++ans]=i;
		}
		printf("%d\n",ans);
		for(int i=1;i<=ans;i++)printf("%d ",s[i]);
	}
	return 0;
}

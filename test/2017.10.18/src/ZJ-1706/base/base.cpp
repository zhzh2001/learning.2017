#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
const int dx[4]={1,-1,0,0};
const int dy[4]={0,0,1,-1};
bool flag;
int x[5],y[5],ansx[5],ansy[5],px[5],py[5];
inline bool yz(){
	int sx=1e9,sy=1e9,tx=-1e9,ty=-1e9;
	for(int i=1;i<=4;i++){
		sx=min(sx,px[i]);sy=min(sy,py[i]);
		tx=max(tx,px[i]);ty=max(ty,py[i]);
		if((px[i]-x[i])*(py[i]-y[i])!=0)return 0;
	}
	for(int i=1;i<=4;i++){
		if(px[i]!=sx&&px[i]!=tx)return 0;
		if(py[i]!=sy&&py[i]!=ty)return 0;
	}
	if(tx-sx!=ty-sy)return 0;
	memcpy(ansx,px,sizeof px);memcpy(ansy,py,sizeof py);
	return 1;
}
inline void dfs(int p,int q){
	if(flag)return;
	if(p==5){
		if(yz())flag=1;
		return;
	}
	for(int i=0;i<4;i++)
		for(int j=0;j<=q;j++){
			px[p]=x[p]+dx[i]*j;
			py[p]=y[p]+dy[i]*j;
			dfs(p+1,q);
		}
}
inline bool check(int p){
	flag=0;
	dfs(1,p);
	return flag;
}
int main()
{
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	int T=read();
	while(T--){
		for(int i=1;i<=4;i++)x[i]=read(),y[i]=read();
		int ans=1e9;
		for(int i=0;i<=10;i++)if(check(i)){ans=i;break;}
		if(ans==1e9)return puts("-1")&0;
		printf("%d\n",ans);
		for(int i=1;i<=4;i++)printf("%d %d\n",ansx[i],ansy[i]);
	}
	return 0;
}

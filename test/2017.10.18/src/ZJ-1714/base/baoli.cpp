#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll int
#define maxn 2010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
ll add[maxn][maxn],n,Q;
const ll mod=1e9+7;
int main(){
	freopen("seaco.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	Q=read();
	For(i,1,Q){
		ll opt=read(),l=read(),r=read();
		if (opt==1)	For(j,1,n)	add[i][j]=add[i-1][j]+(l<=j&&j<=r);
		else	For(j,1,n)	add[i][j]=(add[i-1][j]+add[r][j]-add[l-1][j])%mod;
	}
	For(i,1,n)	printf("%d ",(add[Q][i]%mod+mod)%mod);
}

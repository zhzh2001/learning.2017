#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll int
#define maxn 400010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=50100;
ll n,ans;	char s[N];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ll T=read();
	while(T--){
		scanf("%s",s+1);	n=strlen(s+1);	ans=0;
		ll l=1,r=n;
		while(l<=r){
			if (s[l]=='W'||s[r]=='W'){
				if (s[l]=='W')	swap(s[l],s[r]);
				ans=s[l]=='b';
				break;
			}else{
				if (s[l]+s[r]=='b'+'w')	l++,r--;
				else{	ans=s[l]=='b';	break;	}
			}
		}
		puts(ans?"yes":"no");
	}
}
/*
5
W
bWbw
bwWwb
wbwbWb
Wbwbw
*/

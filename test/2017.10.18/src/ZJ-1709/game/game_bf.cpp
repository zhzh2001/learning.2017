#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
char s[5002];
bool dp[5002][5002][2];
bool have[5002][5002][2][2];
int main(){
	init();
	int T=readint();
	while(T--){
		readstr(s+1);
		int n=strlen(s+1),id=0;
		for(int i=1;i<=n;i++){
			if (s[i]=='W'){
				s[i]='w',id=i;
				break;
			}
		}
		for(int i=0;i<=n+1;i++)
			for(int j=0;j<=n+1;j++){
				dp[i][j][0]=dp[i][j][1]=0;
				//memset(have[i][j],0,sizeof(have[i][j]));
			}
		for(int i=0;i<=n;i++)
			for(int j=n+1;j>i;j--){
				if (i){
					for(int k=1;k<i;k++)
						if (s[k]!=s[i])
							dp[i][j][0]|=!dp[k][j][0];
					for(int k=j;k<=n;k++)
						if (s[k]!=s[i])
							dp[i][j][0]|=!dp[i-1][k][1];
				}
				if (j<=n){
					for(int k=1;k<=i;k++)
						if (s[k]!=s[j])
							dp[i][j][1]|=!dp[k][j+1][0];
					for(int k=j+1;k<=n;k++)
						if (s[k]!=s[j])
							dp[i][j][1]|=!dp[i][k][1];
				}
			}
		puts(dp[id][id+1][0]?"yes":"no");
	}
}
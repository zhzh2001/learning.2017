#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
char a[200002],b[200002];
ll w[200002];
ll power[200002];
ll qwq[26];
int sigma=0,cur=0;
bool vis[26];
ll tat[2000002];
int stk[200002];
void dfs(int x,ll hsh){
	if (x==sigma){
		tat[++cur]=hsh;
		return;
	}
	if (vis[x]){
		dfs(x+1,hsh);
		return;
	}
	dfs(x+1,(hsh+qwq[x]*x)%mod);
	for(int i=x+1;i<sigma;i++){
		if (vis[i])
			continue;
		vis[i]=1;
		dfs(x+1,(hsh+qwq[i]*x+qwq[x]*i)%mod);
		vis[i]=0;
	}
}
int main(){
	init();
	int n=readint(),m=readint();
	readstr(a+1);
	for(int i=1;i<=n;i++)
		sigma=max(sigma,a[i]-'a');
	readstr(b+1);
	for(int i=1;i<=m;i++)
		sigma=max(sigma,b[i]-'a');
	sigma++;
	//printf("%d\n",sigma);
	power[0]=1;
	for(int i=1;i<=max(n,m);i++)
		power[i]=power[i-1]*233%mod;
	for(int i=m;i;i--)
		(qwq[b[i]-'a']+=power[m-i])%=mod;
	dfs(0,0);
	//printf("%d\n",cur);
	sort(tat+1,tat+cur+1);
	//printf("%d\n",cur);
	for(int i=1;i<=n;i++)
		w[i]=(w[i-1]*233+a[i]-'a')%mod;
	int ans=0;
	for(int i=1;i<=n-m+1;i++){
		ll now=(w[i+m-1]-w[i-1]*power[m])%mod;
		if (now<0)
			now+=mod;
		//printf("%d %lld\n",i,now);
		if (*lower_bound(tat+1,tat+cur+1,now)==now)
			stk[++ans]=i;
	}
	printf("%d\n",ans);
	for(int i=1;i<=ans;i++)
		printf("%d ",stk[i]);
}

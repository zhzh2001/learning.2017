#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct point{
	int x,y;
}a[4],b[4],c[4],d[4];
int p[4];
bool use[4];
int ans=INF;
void work(int tmp){
	int a1=use[1]?a[1].x-a[0].x:a[1].y-a[0].y,a2=use[2]?a[2].x-a[0].x:a[2].y-a[0].y,a3=use[3]?a[3].x-a[0].x:a[3].y-a[0].y;
	if (a3<=0)
		return;
	if ((use[1] && a1) || (!use[1] && a1!=a3))
		return;
	if ((use[2] && a2!=a3) || (!use[2] && a2))
		return;
	//printf("%d\n",a3);
	tmp=max(tmp,use[1]?abs(a[1].y-a[0].y-a3):abs(a[0].x-a[1].x));
	tmp=max(tmp,use[2]?abs(a[0].y-a[2].y):abs(a[2].x-a[0].x-a3));
	tmp=max(tmp,use[3]?abs(a[3].y-a[0].y-a3):abs(a[3].x-a[0].x-a3));
	if (tmp<ans){
		for(int i=0;i<4;i++)
			c[i]=a[0];
		c[1].y+=a3;
		c[2].x+=a3;
		c[3].x+=a3;
		c[3].y+=a3;
		for(int i=0;i<4;i++)
			d[p[i]]=c[i];
		ans=tmp;
	}
}
int main(){
	init();
	int T=readint();
	while(T--){
		int mmax=-INF;
		for(int i=0;i<4;i++){
			a[i].x=readint(),a[i].y=readint();
			b[i]=a[i];
			mmax=max(mmax,abs(a[i].x));
			mmax=max(mmax,abs(a[i].y));
		}
		ans=INF;
		for(int i=0;i<16;i++){
			for(int j=0;j<4;j++)
				use[j]=(i&(1<<j))?1:0,p[j]=j;
			do{
				for(int j=0;j<4;j++)
					a[j]=b[p[j]];
				if (use[0]){
					a[0].x-=mmax;
					for(int k=-mmax;k<=mmax;k++)
						work(abs(k)),a[0].x++;
				}else {
					a[0].y-=mmax;
					for(int k=-mmax;k<=mmax;k++)
						work(abs(k)),a[0].y++;
				}
			}while(next_permutation(p,p+4));
		}
		if (ans==INF){
			puts("-1");
			continue;
		}
		printf("%d\n",ans);
		for(int i=0;i<4;i++)
			printf("%d %d\n",c[i].x,c[i].y);
	}
}
#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define base 23333
#define mod 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,m,st[N],top,p,Pow[N],hash[N];
char s1[N],s2[N];
inline void doit1(){
	printf("%d\n",n-m+1);
	For(i,m,n) cout<<i<<" ";
}
inline int Hash(int l,int r){
	return (1ll*hash[r]-1ll*hash[l-1]*Pow[r-l+1]%mod+mod)%mod;
}
inline void doit2(){
	int res1,res2;res1=res2=0;
	For(i,1,m) res1=(1ll*res1*base%mod+(s2[i]-'a'))%mod;
	For(i,1,m) res2=(1ll*res2*base%mod+(1ll-(s2[i]-'a')))%mod;
	For(i,1,n) hash[i]=(1ll*hash[i-1]*base%mod+s1[i]-'a')%mod;
	For(i,m,n) if(Hash(i-m+1,i)==res1||Hash(i-m+1,i)==res2) st[++top]=i-m+1;
	printf("%d\n",top);
	For(i,1,top) printf("%d ",st[i]);
}
inline void doit3(){
	int res1,res2,res3,res4;res1=res2=res3=res4=0;
	For(i,1,m) res1=(1ll*res1*base%mod+(s2[i]-'a'))%mod;
	For(i,1,m){
		if(s2[i]=='a') res2=(1ll*res2*base%mod+2)%mod;
		if(s2[i]=='b') res2=(1ll*res2*base%mod+1)%mod;
		if(s2[i]=='c') res2=(1ll*res2*base%mod+0)%mod;
	}
	For(i,1,m){
		if(s2[i]=='c') res3=(1ll*res3*base%mod+2)%mod;
		if(s2[i]=='a') res3=(1ll*res3*base%mod+1)%mod;
		if(s2[i]=='b') res3=(1ll*res3*base%mod+0)%mod;
	}
	For(i,1,m){
		if(s2[i]=='b') res4=(1ll*res4*base%mod+2)%mod;
		if(s2[i]=='c') res4=(1ll*res4*base%mod+1)%mod;
		if(s2[i]=='a') res4=(1ll*res4*base%mod+0)%mod;
	}
	For(i,1,n) hash[i]=(1ll*hash[i-1]*base%mod+s1[i]-'a')%mod;
	For(i,m,n) if(Hash(i-m+1,i)==res1||Hash(i-m+1,i)==res2||Hash(i-m+1,i)==res3||Hash(i-m+1,i)==res4) st[++top]=i-m+1;
	printf("%d\n",top);
	For(i,1,top) printf("%d ",st[i]);
}
int main(){
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	n=read();m=read();
	Pow[0]=1;For(i,1,N-1) Pow[i]=1ll*Pow[i-1]*base%mod;
	scanf("%s%s",s1+1,s2+1);
	For(i,1,m) p|=(1<<(s2[i]-'a'));
	if(p==1||p==2){doit1();return 0;}
	if(p==3){doit2();return 0;}
	if(p==4){doit1();return 0;}
	if(p==7){doit3();return 0;}
	return 0;
}

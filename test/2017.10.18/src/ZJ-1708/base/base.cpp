#include<bits/stdc++.h>
#define ll long long
#define inf 1e9
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
struct data{int x,y;}a[5];
inline bool cmp1(data c,data b){return c.x<b.x;}
inline bool cmp2(data c,data b){return c.y<b.y;}
inline void solve(){
	For(i,1,4) a[i]=(data){read(),read()};
	int x,y,xx,yy,ans1=inf;
	x=min(min(a[1].x,a[2].x),min(a[3].x,a[4].x));
	y=min(min(a[1].y,a[2].y),min(a[3].y,a[4].y));
	xx=max(max(a[1].x,a[2].x),max(a[3].x,a[4].x));
	yy=max(max(a[1].y,a[2].y),max(a[3].y,a[4].y));
//	cout<<x<<" "<<y<<" "<<xx<<" "<<yy<<endl;
	For(i,1,4) For(j,1,4){
		if(i==j) continue;
		For(k,1,4){
			if(i==k&&j==k) continue;
			For(l,1,4){
				int mx=0;
				if(i==l||j==l||k==l) continue;
				if(a[i].x!=x&&a[i].y!=y) continue;
				if(a[j].x!=xx&&a[j].y!=yy) continue;
				if(a[k].x!=x&&a[k].y!=yy) continue;
				if(a[l].x!=xx&&a[l].y!=y) continue;
				mx=max(mx,abs(a[i].x-x)+abs(a[i].y-y));
				mx=max(mx,abs(a[j].x-xx)+abs(a[j].y-yy));
				mx=max(mx,abs(a[k].x-x)+abs(a[k].y-yy));
				mx=max(mx,abs(a[l].x-xx)+abs(a[l].y-y));
				if(ans1>mx) ans1=mx;
			}
		}
	}
	if(abs(xx-x)!=abs(yy-y)) ans1=inf; 
	int ans2=inf;
	sort(a+1,a+5,cmp1);
	x=a[2].x;xx=a[3].x;
	sort(a+1,a+5,cmp2);
	y=a[2].y;yy=a[3].y;
	For(i,1,4) For(j,1,4){
		if(i==j) continue;
		For(k,1,4){
			if(i==k&&j==k) continue;
			For(l,1,4){
				int mx=0;
				if(i==l||j==l||k==l) continue;
				if(a[i].x!=x&&a[i].y!=y) continue;
				if(a[j].x!=xx&&a[j].y!=yy) continue;
				if(a[k].x!=x&&a[k].y!=yy) continue;
				if(a[l].x!=xx&&a[l].y!=y) continue;
				mx=max(mx,abs(a[i].x-x)+abs(a[i].y-y));
				mx=max(mx,abs(a[j].x-xx)+abs(a[j].y-yy));
				mx=max(mx,abs(a[k].x-x)+abs(a[k].y-yy));
				mx=max(mx,abs(a[l].x-xx)+abs(a[l].y-y));
				if(ans2>mx) ans2=mx;
			}
		}
	}
	if(abs(xx-x)!=abs(yy-y)) ans2=inf;
	if(ans1==inf&&ans2==inf){puts("-1");return;}
	if(ans1<ans2){
		cout<<ans1<<endl;
		x=min(min(a[1].x,a[2].x),min(a[3].x,a[4].x));
		y=min(min(a[1].y,a[2].y),min(a[3].y,a[4].y));
		xx=max(max(a[1].x,a[2].x),max(a[3].x,a[4].x));
		yy=max(max(a[1].y,a[2].y),max(a[3].y,a[4].y));
		cout<<x<<" "<<y<<endl;
		cout<<x<<" "<<yy<<endl;
		cout<<xx<<" "<<y<<endl;
		cout<<xx<<" "<<yy<<endl;		
	}else{
		cout<<ans2<<endl;
		cout<<x<<" "<<y<<endl;
		cout<<x<<" "<<yy<<endl;
		cout<<xx<<" "<<y<<endl;
		cout<<xx<<" "<<yy<<endl;
	}
}
int main(){
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	int T=read();
	while(T--){
		solve();
	}
	return 0;
}

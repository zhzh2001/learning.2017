#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int inf=1e9;
int flag[5],xx[5],yy[5],ans,cnt0,cnt1,x[5],y[5],x1,x2,y1,y2,cho[5],ansx[5],ansy[5];
inline void init(){
	for (int i=1;i<=4;i++){
		scanf("%d%d",&x[i],&y[i]);
	}
}
inline int abs(int x) {return (x>0)?x:-x;}
inline int dis(int a,int b){
	return (abs(x[a]-xx[b])+abs(y[a]-yy[b]));
}
inline void dfs(int now,int s){
	if (s>=ans) return;
	if (now==5){
		ans=min(ans,s);
		for (int i=1;i<=4;i++){
			ansx[flag[i]]=xx[i];
			ansy[flag[i]]=yy[i];
		}
		return;
	}
	for (int i=1;i<=4;i++){
		if (!flag[i]&&(xx[i]==x[now]||yy[i]==y[now])){
			flag[i]=now;
			dfs(now+1,max(s,dis(now,i)));
			flag[i]=0;
		}
	}
}
inline void solve(){
	ans=inf;
	for (int l=1;l<=50;l++){
		for (int x1=-70;x1<=70;x1++){
			for (int y1=-70;y1<=70;y1++){
						x2=x1+l;
						y2=y1+l;
						xx[1]=x1; yy[1]=y1;
						xx[2]=x1; yy[2]=y2;
						xx[3]=x2; yy[3]=y2;
						xx[4]=x2; yy[4]=y1;
						memset(flag,0,sizeof(flag));
						dfs(1,0);
				}
			}
		}
	if (ans==inf){
		printf("-1\n");
	}else{
		printf("%d\n",ans);
		for (int i=1;i<=4;i++){
			printf("%d %d\n",ansx[i],ansy[i]);
		}
	}
}
int main(){
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		init();
		solve();
	}
	return 0;
}

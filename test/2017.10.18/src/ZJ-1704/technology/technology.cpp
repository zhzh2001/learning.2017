#include<cstdio>
using namespace std;
const int maxn=2e5+5;
char s[maxn];
bool use[30];
int tot,can[30],ans,fail[maxn],cho[30],flag[maxn],n,m,s1[maxn],s2[maxn];
inline void init(){
	scanf("%d%d",&n,&m);
	scanf("%s",s+1);
	for (int i=1;i<=n;i++){
		s1[i]=s[i]-'a'+1;
		use[s1[i]]=1;
	}
	scanf("%s",s+1);
	for (int i=1;i<=m;i++){
		s2[i]=s[i]-'a'+1;
		use[s2[i]]=1;
	}
	for (int i=1;i<=26;i++){
		if (use[i]){
			can[++tot]=i;
		}
	}
}
inline void work(){
	int j=0;
	for (int i=1;i<=n;i++){
		while (j&&cho[s2[j+1]]!=s1[i]){
			j=fail[j];
		}
		if (cho[s2[j+1]]==s1[i]){
			j++;
		}
		if (j==m){
			if (!flag[i]) {
				ans++;
			}
			flag[i]=1;
			j=fail[j];
		}
	}
}
void dfs(int x){
	if (x==tot+1){
		work();
		return;
	}
	if (cho[can[x]]){
		dfs(x+1);
		return;
	}
	cho[can[x]]=can[x];
	dfs(x+1);
	cho[can[x]]=0;
	for (int i=x+1;i<=tot;i++){
		if (!cho[can[i]]){
			cho[can[x]]=can[i];
			cho[can[i]]=can[x];
			dfs(x+1);
			cho[can[i]]=0;
			cho[can[x]]=0;
		}
	}
}
inline void getnext(){
	fail[1]=0; int j=0;
	for (int i=2;i<=m;i++){
		while (j&&s2[j+1]!=s2[i]) j=fail[j];
		if (s2[j+1]==s2[i]) j++;
		fail[i]=j;
	}
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts("");
}
inline void solve(){
	getnext();
	dfs(1);
	writeln(ans);
	for (int i=1;i<=n;i++){
		if (flag[i]) {
			write(i-m+1); putchar(' ');
		} 
	}
}
int main(){
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	init();
	solve();
	return 0;
}

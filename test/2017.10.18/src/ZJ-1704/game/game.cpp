#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=5005;
char s[maxn];
bool dp[maxn][maxn][2],vis[maxn][maxn][2];
int len,now,a[maxn],w[maxn],b[maxn],tot1,tot2;
inline void init(){
	scanf("%s",s+1);
	tot1=tot2=0; len=strlen(s+1);
	for (int i=1;i<=len;i++){
		if (s[i]=='b'){
			a[i]=0;
			b[++tot1]=i;
		} else{
			if (s[i]=='W') now=i;
			a[i]=1;
			w[++tot2]=i;
		}
	}
}
inline void clean(){
	memset(dp,0,sizeof(dp));
	memset(vis,0,sizeof(vis));
}
inline bool dfs(int l,int r,int opt){
	if (l==1&&r==len){
		if (opt==0) return a[l]==0;
			else return a[r]==0;
	}
	if (vis[l][r][opt]) return dp[l][r][opt];
	bool &ans=dp[l][r][opt];
	vis[l][r][opt]=1;
	int flag;
	if (opt==0){
		if (a[l]==0) {
			ans=1;
			for (int i=1;i<=tot2;i++){
				if (w[i]<=l) ans&=dfs(w[i],r,0);
					else break;
				if (!ans) return 0;
			}
			for (int i=tot2;i;i--){
				if (w[i]>r) ans&=dfs(l,w[i],1);
					else break;
				if (!ans) return 0;
			}
		}else{
			ans=0;
			for (int i=1;i<=tot1;i++){
				if (b[i]<=l) ans|=dfs(b[i],r,0);
					else break;
				if (ans) return 1;
			}
			for (int i=tot1;i;i--){
				if (b[i]>r) ans|=dfs(l,b[i],1);
					else break;
				if (ans) return 1;
			}
		}
	}else{
		if (a[r]==0) {
			ans=1;
			for (int i=1;i<=tot2;i++){
				if (w[i]<l) ans&=dfs(w[i],r,0);
					else break;
				if (!ans) return 0;
			}
			for (int i=tot2;i;i--){
				if (w[i]>=r) ans&=dfs(l,w[i],1);
					else break;
				if (!ans) return 0;
			}
		}else{
			ans=0;
			for (int i=1;i<=tot1;i++){
				if (b[i]<l) ans|=dfs(b[i],r,0);
					else break;
				if (ans) return 1;
			}
			for (int i=tot1;i;i--){
				if (b[i]>=r) ans|=dfs(l,b[i],1);
					else break;
				if (ans) return 1;
			}
		}
	}
	return ans;
}
inline void solve(){
	clean();
	bool ans=dfs(now,now,0);
	if (ans) printf("yes\n");
		else printf("no\n");
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		init();
		solve();
	}
	return 0;
}

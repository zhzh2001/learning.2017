#include<bits/stdc++.h>
using namespace std;
const int N=5005;
short f[N][N][2];
int t,l,pos;
char s[N];
int work(int x,int y,int z)
{
	if (f[x][y][z]!=-1)
		return f[x][y][z];
	int p=z?y-1:x+1;
	for (int i=1;i<=x;++i)
		if (s[i]!=s[p])
			if (!work(i-1,y,0))
				return f[x][y][z]=1;
	for (int i=y;i<=l;++i)
		if (s[i]!=s[p])
			if (!work(x,i+1,1))
				return f[x][y][z]=1;
	return f[x][y][z]=0;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&t);
	memset(f,-1,sizeof(f));
	while (t--)
	{
		for (int i=0;i<pos;++i)
			for (int j=pos+1;j<=l+1;++j)
				f[i][j][0]=f[i][j][1]=-1;
		scanf("%s",s+1);
		l=strlen(s+1);
		for (int i=1;i<=l;++i)
			if (s[i]=='W')
				pos=i;
		s[pos]='w';
		puts(work(pos-1,pos+1,0)?"yes":"no");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int ne[N],l1,l2,b[N],sum,a[30],ans[N*2],cnt;
char c[N],s[N];
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar('0'+a[a[0]--]);
}
inline void sc()
{
	sort(ans+1,ans+1+cnt);
	sum=0;
	for (int i=1;i<=cnt;++i)
		if (ans[i]!=ans[i-1])
			ans[++sum]=ans[i];
	write(sum);
	putchar('\n');
	for (int i=1;i<=sum;++i)
	{
		write(ans[i]);
		putchar(' ');
	}
	exit(0);
}
inline void kmp()
{
	int j=0;
	for (int i=1;i<=l1;++i)
	{
		while (j&&a[c[i]-'a']!=s[j+1]-'a')
			j=ne[j];
		if (a[c[i]-'a']==s[j+1]-'a')
			++j;
		if (j==l2)
		{
			j=ne[j];
			ans[++cnt]=i-l2+1;
		}
	}
}
int main()
{
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	scanf("%d%d",&l1,&l2);
	scanf("%s",c+1);
	scanf("%s",s+1);
	int j=0;
	for (int i=2;i<=l2;++i)
	{
		while (j&&s[i]!=s[j+1])
			j=ne[j];
		if (s[i]==s[j+1])
			++j;
		ne[i]=j;
	}
	for (int i=1;i<=l1;++i)
		if (!b[c[i]-'a'])
		{
			b[c[i]-'a']=1;
			++sum;
		}
	for (int i=1;i<=l2;++i)
		if (!b[s[i]-'a'])
		{
			b[s[i]-'a']=1;
			++sum;
		}
	if (sum==1)
	{
		write(l1-l2+1);
		putchar('\n');
		for (int i=1;i<=l1-l2+1;++i)
		{
			write(i);
			putchar(' ');
		}
		return 0;
	}
	if (sum==2&&b[0]&&b[1])
	{
		a[1]=1;
		a[0]=0;
		kmp();
		a[1]=0;
		a[0]=1;
		kmp();
		sc();
	}
	if (sum==3&&b[0]&&b[1]&&b[2])
	{
		a[0]=0;
		a[1]=1;
		a[2]=2;
		kmp();
		swap(a[0],a[1]);
		kmp();
		swap(a[0],a[1]);
		swap(a[0],a[2]);
		kmp();
		swap(a[0],a[2]);
		swap(a[1],a[2]);
		kmp();
		sc();
	}
	if (sum==4&&b[0]&&b[1]&&b[2]&&b[3])
	{
		a[0]=0;
		a[1]=1;
		a[2]=2;
		a[3]=3;
		kmp();
		swap(a[0],a[1]);
		kmp();
		swap(a[0],a[1]);
		swap(a[0],a[2]);
		kmp();
		swap(a[0],a[2]);
		swap(a[1],a[2]);
		kmp();
		swap(a[1],a[2]);
		swap(a[0],a[3]);
		kmp();
		swap(a[0],a[3]);
		swap(a[1],a[3]);
		kmp();
		swap(a[1],a[3]);
		swap(a[2],a[3]);
		kmp();
		swap(a[0],a[1]);
		kmp();
		swap(a[0],a[1]);
		swap(a[2],a[3]);
		swap(a[0],a[2]);
		swap(a[1],a[3]);
		kmp();
		swap(a[0],a[2]);
		swap(a[1],a[3]);
		swap(a[0],a[3]);
		swap(a[1],a[2]);
		kmp();
		sc();
	}
	if (sum>4)
	{
		for (int i=1;i<=l1-l2+1;++i)
		{
			for (int j=0;j<=25;++j)
				a[j]=-1;
			bool b=1;
			for (int j=1;j<=l2;++j)
				if (a[c[i+j-1]-'a']!=s[j]-'a')
					if (a[c[i+j-1]-'a']!=-1||a[s[j]-'a']!=-1)
					{
						b=0;
						break;
					}
					else
					{
						a[c[i+j-1]-'a']=s[j]-'a';
						a[s[j]-'a']=c[i+j-1]-'a';
					}
			if (b)
				ans[++cnt]=i;
		}
		write(cnt);
		putchar('\n');
		for (int i=1;i<=cnt;++i)
		{
			write(ans[i]);
			putchar(' ');
		}
	}
	return 0;	
}

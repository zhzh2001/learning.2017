#include<bits/stdc++.h>
using namespace std;
struct point
{
	int x,y;
}a[5],b[5],c[5];
const int p=1000,inf=1e9+7;
int t,ans;
void gx()
{
	int sum=0;
	for (int i=1;i<=4;++i)
		sum=max(sum,max(abs(a[i].x-b[i].x),abs(a[i].y-b[i].y)));
	if (sum<ans)
	{
		ans=sum;
		for (int i=1;i<=4;++i)
			c[i]=b[i];
	}
}
void find4th()
{
	b[4].x=b[1].x^b[2].x^b[3].x;
	b[4].y=b[1].y^b[2].y^b[3].y;
	if (a[4].x==b[4].x||a[4].y==b[4].y)
		gx();
}
void find3rd()
{
	if (b[1].x==b[2].x)
	{
		int d=b[1].y-b[2].y,xx=b[1].x+d,xxx=b[1].x-d;
		if (a[3].x==xx||a[3].x==xxx)
		{
			b[3].x=a[3].x;
			b[3].y=b[1].y;
			find4th();
			b[3].y=b[2].y;
			find4th();
		}
		if (a[3].y==b[1].y||a[3].y==b[2].y)
		{
			b[3].y=a[3].y;
			b[3].x=xx;
			find4th();
			b[3].x=xxx;
			find4th();
		}
	}
	else
	{
		int d=b[1].x-b[2].x,xx=b[1].y+d,xxx=b[1].y-d;
		if (a[3].y==xx||a[3].y==xxx)
		{
			b[3].y=a[3].y;
			b[3].x=b[1].x;
			find4th();
			b[3].x=b[2].x;
			find4th();
		}
		if (a[3].x==b[1].x||a[3].x==b[2].x)
		{
			b[3].x=a[3].x;
			b[3].y=xx;
			find4th();
			b[3].y=xxx;
			find4th();
		}
	}
}
void find2nd()
{
	if (a[2].x==b[1].x)
	{
		b[2].x=a[2].x;
		for (int i=-p;i<=p;++i)
		{
			b[2].y=i;
			find3rd();
		}
	}
	if (a[2].y==b[1].y)
	{
		b[2].y=a[2].y;
		for (int i=-p;i<=p;++i)
		{
			b[2].x=i;
			find3rd();
		}
	}
	if (a[2].x!=b[1].x&&a[2].y!=b[1].y)
	{
		b[2].x=a[2].x;
		b[2].y=b[1].y;
		find3rd();
		b[2].x=b[1].x;
		b[2].y=a[2].y;
		find3rd();
	}
}
void find1st()
{
	b[1].x=a[1].x;
	for (int i=-p;i<=p;++i)
	{
		b[1].y=i;
		find2nd();
	}
	b[1].y=a[1].y;
	for (int i=-p;i<=p;++i)
	{
		b[1].x=i;
		find2nd();
	}
}
int main()
{
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		for (int i=1;i<=4;++i)
			scanf("%d%d",&a[i].x,&a[i].y);
		ans=inf;
		find1st();
		swap(a[2],a[3]);
		find1st();
		if (ans!=inf)
		{
			printf("%d\n",ans);
			for (int i=1;i<=4;++i)
				printf("%d %d\n",c[i].x,c[i].y);
		}
		else
			puts("-1");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}//win[i][j] i~j区间被取走后'b'是否必胜 
int n,s_x;
bool win[2001][2001],lose[2001][2001];
bool vis[2001][2001][2][2],z[2001][2001][2][2];
char s[20001];
inline bool dfs(int l,int r,int now,int LR)
{

	if(vis[l][r][now][LR])	return z[l][r][now][LR];	
	bool flag=1;
	if(win[l][r])
	{
		if(now==0)	return 1;
		else 	return 0;
	}
	if(lose[l][r])
	{
		if(now==0)	return 0;
		else return 1;
	}
	vis[l][r][now][LR]=1;
	For(i,1,l-1)
	{
		if(LR==0&&i==l-1)	continue;
		if(!now&&s[i]=='b')	if(vis[i+1][r+(LR==1)][now^1][0])	flag&=z[i+1][r+(LR==1)][now^1][0];else flag&=dfs(i+1,r+(LR==1),now^1,0);
		if(now &&s[i]=='w') if(vis[i+1][r+(LR==1)][now^1][0])	flag&=z[i+1][r+(LR==1)][now^1][0];else flag&=dfs(i+1,r+(LR==1),now^1,0);
	}
	For(i,r+1,n)
	{
		if(LR==1&&i==r+1)	continue;
		if(!now&&s[i]=='b')	if(vis[l-(LR==0)][i-1][now^1][1])	flag&=z[l-(LR==0)][i-1][now^1][1];else flag&=dfs(l-(LR==0),i-1,now^1,1);
		if(now &&s[i]=='w')	if(vis[l-(LR==0)][i-1][now^1][1])	flag&=z[l-(LR==0)][i-1][now^1][1];else flag&=dfs(l-(LR==0),i-1,now^1,1);
	}
	z[l][r][now][LR]=!flag;
	return !flag;
}
int main()
{
	freopen("game.in","r",stdin);freopen("game.out","w",stdout);
	int T=read();
	while(T--)
	{
		scanf("\n%s",s+1);
		n=strlen(s+1);
		For(i,0,n+1)	For(j,0,n+1)	win[i][j]=lose[i][j]=vis[i][j][1][1]=vis[i][j][1][0]=vis[i][j][0][0]=vis[i][j][0][1]=0;
		For(i,1,n)	
			if(s[i]=='W')	s_x=i,s[i]='w';
		For(i,1,n)	{if(s[i]!='b')	break;win[i+1][n]=1;}
		Dow(i,1,n)	{if(s[i]!='b')	break;win[1][i-1]=1;}
		For(i,1,n)	
		{
			if(s[i]!='b')	break;
			Dow(j,1,n)
			{
				if(s[j]!='b')	break;
				win[i+1][j-1]=1;
			}
		}
		For(i,1,n)	{if(s[i]!='w')	break;lose[i+1][n]=1;}
		Dow(i,1,n)	{if(s[i]!='w')	break;lose[1][i-1]=1;}
		For(i,1,n)	
		{
			if(s[i]!='w')	break;
			Dow(j,1,n)
			{
				if(s[j]!='w')	break;
				lose[i+1][j-1]=1;
			}
		}
		if(dfs(s_x+1,s_x,0,0))	puts("yes");else puts("no");
	}
} 

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define y1 zyyorz
#define inf 1e9
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int T,mi,ansx[6],ansy[6],p[6],x[6],y[6];
int x1,x2,y1,y2;
bool vis[6];
inline void check()
{
	int c=0;
	if(x[p[1]]!=x1&&y[p[1]]!=y1)	return;
	if(x[p[1]]!=x1)	c=max(c,abs(x1-x[p[1]]));else c=max(c,abs(y1-y[p[1]]));
	if(x[p[2]]!=x1&&y[p[2]]!=y2)	return;
	if(x[p[2]]!=x1)	c=max(c,abs(x1-x[p[2]]));else c=max(c,abs(y2-y[p[2]]));
	if(x[p[3]]!=x2&&y[p[3]]!=y2)	return;
	if(x[p[3]]!=x2)	c=max(c,abs(x2-x[p[3]]));else c=max(c,abs(y2-y[p[3]]));
	if(x[p[4]]!=x2&&y[p[4]]!=y1)	return;
	if(x[p[4]]!=x2)	c=max(c,abs(x2-x[p[4]]));else c=max(c,abs(y1-y[p[4]]));
	if(c<mi)
	{
		mi=c;
		ansx[1]=ansx[2]=x1;ansx[3]=ansx[4]=x2;
		ansy[1]=ansy[4]=y1;ansy[2]=ansy[3]=y2;
	}
}
inline void dfs(int x)
{
	if(x==5)	check();
	For(i,1,4)
		if(!vis[i])	p[x]=i,vis[i]=1,dfs(x+1),vis[i]=0;	
}
int main()
{
	freopen("base.in","r",stdin);freopen("base.out","w",stdout);
	T=read();
	while(T--)
	{
		mi=inf;
		int mxx=0,mxy=0,mix=inf,miy=inf;
		For(i,1,4)	x[i]=read(),y[i]=read(),mxx=max(mxx,x[i]),mix=min(mix,x[i]),mxy=max(mxy,y[i]),miy=min(miy,y[i]);

		For(i,mix,mxx)
			For(j,miy,mxy)
			{
				For(k,1,max(mxx-mix,mxy-miy))
				{
					x1=i;y1=j;x2=i+k;y2=j+k;
					dfs(1);
				}
			}
		if(mi==inf)	{puts("-1");continue;}
		printf("%d\n",mi);
		For(i,1,4)
			printf("%d %d\n",ansx[i],ansy[i]);
	}
}

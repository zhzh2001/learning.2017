#include<bits/stdc++.h>
#define N 5005
using namespace std;
int T,s[N][2],ed;
char ch[N];
signed char w[N][N][2][2];
int main(){
	freopen("game.in","r",stdin);
	freopen("game233.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%s",ch+1);
		int n=strlen(ch+1);
		for (int i=1;i<=n;i++){
			s[i][0]=s[i-1][0]+(ch[i]=='b');
			s[i][1]=s[i-1][1]+(ch[i]!='b');
			if (ch[i]=='W') ed=i;
		}
		for (int i=1;i<=n;i++)
			for (int j=i;j<=n;j++)
				for (int k=0;k<2;k++){
					w[i][j][k][0]=((s[n][k^1]-s[j][k^1]+s[i][k^1])!=0?-1:0);
					w[i][j][k][1]=((s[n][k^1]-s[j-1][k^1]+s[i-1][k^1])!=0?-1:0);
				}
		for (int k=n;k>=1;k--)
			for (int i=1;i+k-1<=n;i++){
				int j=i+k-1;
				int bl=(ch[i]=='b'?0:1);
				if (w[i][j][bl][0]==-1){
					w[i][j][bl][0]=0;
					for (int l=j+1;l<=n;l++)
						if (w[i][l][bl^1][1]==0){
							w[i][j][bl][0]=1;
							break;
						}
					for (int l=1;l<i;l++)
						if (w[l][j][bl^1][0]==0){
							w[i][j][bl][0]=1;
							break;
						}
				}
				bl=(ch[j]=='b'?0:1);
				if (w[i][j][bl][1]==-1){
					w[i][j][bl][1]=0;
					for (int l=j+1;l<=n;l++)
						if (w[i][l][bl^1][1]==0){
							w[i][j][bl][1]=1;
							break;
						}
					for (int l=1;l<i;l++)
						if (w[l][j][bl^1][0]==0){
							w[i][j][bl][1]=1;
							break;
						}
				}
			}
		puts(w[ed][ed][1][0]==1||w[ed][ed][1][1]==1?"yes":"no");
	}
}

#include <bits/stdc++.h>
#define ll long long
#define N 200020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char s1[N], s2[N];
int a, b, res[N], cnt;
int mp[50];
bool check(int x) {
	memset(mp, 0, sizeof mp);
	for (int i = 1; i <= b; i++) {
		int w1 = s1[i + x - 1] - 'a' + 1;
		int w2 = s2[i] - 'a' + 1;
		if (!mp[w1] && !mp[w2]) {
			mp[w1] = w2;
			mp[w2] = w1;
		}
		if (mp[w1] != w2 || mp[w2] != w1)
			return false;
	}
	return true;
}
int main(int argc, char const *argv[]) {
	freopen("technology.in", "r", stdin);
	freopen("technology.out", "w", stdout);
	a = read(); b = read();
	scanf("%s", s1 + 1);
	scanf("%s", s2 + 1);
	for (int i = 1; i <= a - b + 1; i++)
		if (check(i)) res[++cnt] = i;
	printf("%d\n", cnt);
	for (int i = 1; i <= cnt; i++)
		printf("%d ", res[i]);
	puts("");
	return 0;
}

#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rep(i,s,n) for(int i=s;i<=n;i++)
#define INF 2147483647

struct GPS
{
	int x,y;
	inline bool operator<(const GPS &o)const{if(x!=o.x)return x<o.x;return y<o.y;}
}r[6],rcopy[6],rmin[6];

int T,x[6],y[6],dx[6][90],dy[6][90],tmp,ans;

inline bool check(const int &n1,const int &n2,const int &n3,const int &n4)
{
	rcopy[1].x=r[1].x=x[1]+dx[1][n1];
	rcopy[1].y=r[1].y=y[1]+dy[1][n1];
	rcopy[2].x=r[2].x=x[2]+dx[2][n2];
	rcopy[2].y=r[2].y=y[2]+dy[2][n2];
	rcopy[3].x=r[3].x=x[3]+dx[3][n3];
	rcopy[3].y=r[3].y=y[3]+dy[3][n3];
	rcopy[4].x=r[4].x=x[4]+dx[4][n4];
	rcopy[4].y=r[4].y=y[4]+dy[4][n4];
	sort(r+1,r+5);
	if(r[1].x!=r[2].x)return false;
	if(r[3].x!=r[4].x)return false;
	if(r[1].y!=r[3].y)return false;
	if(r[2].y!=r[4].y)return false;
	if(r[1].x==r[3].x||r[1].y==r[2].y)return false;
	if(r[3].x-r[1].x!=r[2].y-r[1].y)return false;
	return true;
}

int main(void)
{
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		ans=INF;
		rep(i,1,4)scanf("%d%d",&x[i],&y[i]);
		if(T<=2)
		{
			rep(i,1,4)rep(j,-10,10)dy[i][j+10]=j-y[i],dx[i][j+31]=j-x[i];
			rep(i,0,41)rep(j,0,41)rep(k,0,41)rep(l,0,41)
			{
				if(check(i,j,k,l))
				{
					tmp=abs(dx[1][i])+abs(dy[1][i]);
					tmp=max(tmp,abs(dx[2][j])+abs(dy[2][j]));
					tmp=max(tmp,abs(dx[3][k])+abs(dy[3][k]));
					tmp=max(tmp,abs(dx[4][l])+abs(dy[4][l]));
					if(tmp<ans)
					{
						ans=tmp;
						rep(i,1,4)rmin[i]=rcopy[i];
					}
				}
			}
		}
		else
		{
			rep(i,1,4)rep(j,-5,5)dy[i][j+5]=j-y[i],dx[i][j+16]=j-x[i];
			rep(i,0,21)rep(j,0,21)rep(k,0,21)rep(l,0,21)
			{
				if(check(i,j,k,l))
				{
					tmp=abs(dx[1][i])+abs(dy[1][i]);
					tmp=max(tmp,abs(dx[2][j])+abs(dy[2][j]));
					tmp=max(tmp,abs(dx[3][k])+abs(dy[3][k]));
					tmp=max(tmp,abs(dx[4][l])+abs(dy[4][l]));
					if(tmp<ans)
					{
						ans=tmp;
						rep(i,1,4)rmin[i]=rcopy[i];
					}
				}
			}
		}
		if(ans==INF)printf("-1\n");
		else
		{
			printf("%d\n",ans);
			rep(i,1,4)printf("%d %d\n",rmin[i].x,rmin[i].y);
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

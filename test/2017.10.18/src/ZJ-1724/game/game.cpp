#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define L 5002
#define WHITE 'w'
#define BLACK 'b'
#define SPX 'W'

bool ans;
char s[L];
int T,h,l;

int main(void)
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("\n%s",s+1);
		h=1;l=strlen(s+1);
		while(h!=l)
		{
			if(s[h]==SPX)
			{
				if(s[l]==BLACK)ans=true;
				else ans=false;
				break;
			}
			else if(s[l]==SPX)
			{
				if(s[h]==BLACK)ans=true;
				else ans=false;
				break;
			}
			if(s[h]==s[l])
			{
				if(s[h]==BLACK)ans=true;
				else ans=false;
				break;
			}
			h++,l--;
		}
		if(ans)printf("yes\n");
		else printf("no\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

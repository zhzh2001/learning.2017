#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define L 200004
#define SC 26

bool ans[L],writln=true;
char s[L],t[L],r[SC];
int n,m,cnt;

int main(void)
{
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	scanf("%d%d\n%s\n%s",&n,&m,s+1,t+1);
	for(int i=n-m;i>=0;i--)
	{
		memset(r,0,sizeof(r));
		ans[i+1]=true;
		for(int j=1;j<=m;j++)
		{
			if(s[i+j]==t[j])
			{
				if(r[t[j]-'a']!=t[j])
				{
					if(r[t[j]-'a']){ans[i+1]=false;break;}
					else r[t[j]-'a']=t[j];
				}
			}
			else
			{
				if(r[t[j]-'a']!=s[i+j])
				{
					if(r[t[j]-'a']){ans[i+1]=false;break;}
					else r[t[j]-'a']=s[i+j],r[s[i+j]-'a']=t[j];
				}
			}
		}
		cnt+=ans[i+1];
	}
	printf("%d\n",cnt);
	for(int i=1;i<=n;i++)
		if(ans[i])
		{
			if(writln)writln=false,printf("%d",i);
			else printf(" %d",i);
		}
	fclose(stdin);fclose(stdout);
	return 0;
}

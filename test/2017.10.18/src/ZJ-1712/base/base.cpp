#include<bits/stdc++.h>
using namespace std;
int t,ans,xxx[10],yyy[10],ff[10];
struct lsg{int x,y;}a[10],f[10];
inline void dfs(int d,int sum){
	if (d==4){
		if (sum<ans){
			ans=sum;
			for (int i=0;i<4;i++)a[ff[i]-1]=f[i];
		}return;
	}if (ans<=sum)return;
	for (int i=0;i<4;i++)
		if (!ff[i]&&(xxx[d]==f[i].x||yyy[d]==f[i].y)){
			ff[i]=d+1;dfs(d+1,max(sum,max(abs(xxx[d]-f[i].x),abs(yyy[d]-f[i].y))));ff[i]=0;
		}
}
int main(){
	freopen("base.in","r",stdin);freopen("base.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>t;while (t--){
		for (int i=0;i<4;i++)cin>>xxx[i]>>yyy[i];
		ans=1e9;
		for (int i=0;i<4;i++)
			for (int j=-100;j<=100;j++)
				for (int k=1;k<=200;k++){
					f[0].x=xxx[i];f[0].y=j;
					f[1]=f[0];f[1].x+=k;
					f[2]=f[0];f[2].y+=k;
					f[3]=f[1];f[3].y+=k;
					dfs(0,0);
				}
		for (int i=-100;i<=100;i++)
			for (int j=0;j<4;j++)
				for (int k=1;k<=200;k++){
					f[0].x=i;f[0].y=yyy[j];
					f[1]=f[0];f[1].x+=k;
					f[2]=f[0];f[2].y+=k;
					f[3]=f[1];f[3].y+=k;
					dfs(0,0);
				}
		if (ans==1e9)cout<<-1<<endl;
			else {
				cout<<ans<<endl;
				for (int i=0;i<4;i++)cout<<a[i].x<<' '<<a[i].y<<endl;
			}
	}
}
/*
50
-1 4
-1 5
-5 -4
-4 -1
*/

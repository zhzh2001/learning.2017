#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <ctime>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

ifstream fin("base.in");
ofstream fout("base.out");

const int dx[] = { 1, 0, -1, 0 }, dy[] = { 0, 1, 0, -1 }, inf = 0x3f3f3f3f;

pii nodes[4], e[4];
bool used[4];

struct Line
{
	inline Line() {}
	inline Line(int _type, int _k) : type(_type), k(_k) {}
	int type; // 0: x = k, 1: y = k
	int k;
} a[4][2];

inline pair<bool, pii> cross(const Line &c, const Line &d)
{
	if (c.type == d.type)
		return make_pair(false, make_pair(0, 0));
	if (c.type == 0)
		return make_pair(true, make_pair(c.k, d.k));
	else
		return make_pair(true, make_pair(d.k, c.k));
}

int ans = inf;

void Init()
{
	for (int i = 0; i < 4; ++i)
	{
		fin >> nodes[i].first >> nodes[i].second;
	}
	ans = inf;
}

void Main()
{
	for (int i = -50; i <= 50; ++i)
	{
		for (int j = -50; j <= 50; ++j)
		{
			memset(used, 0x00, sizeof used);
			int len1 = inf;
			int cnt = 0;
			for (int k = 0; k < 4; ++k)
			{
				cnt = 0;
				len1 = inf;
				if (nodes[k].first == i)
				{
					len1 = min(len1, abs(nodes[k].second - j));
					++cnt;
				}
				if (nodes[k].second == j)
				{
					len1 = min(len1, abs(nodes[k].first - i));
					++cnt;
				}
				if (cnt == 0)
					continue;
				used[k] = true;
				for (int dis = 1; dis <= 100; ++dis)
				{
					for (int d = 0; d < 4; ++d)
					{
						int len2 = inf;
						int nx = i + dx[d] * dis, ny = j + dy[d] * dis;
						int cc = 0;
						for (int k = 0; k < 4; ++k)
						{
							cc = 0;
							len2 = inf;
							if (nodes[k].first == nx && !used[k])
							{
								len2 = min(len2, abs(nodes[k].second - ny));
								++cc;
							}
							if (nodes[k].second == ny && !used[k])
							{
								len2 = min(len2, abs(nodes[k].first - nx));
								++cc;
							}
							if (cc == 0)
								continue;
							used[k] = true;
							for (int d1 = 0; d1 < 4; ++d1)
							{
								if (d1 & 1 == d & 1)
									continue;
								int nnx = nx + dx[d1] * dis, nny = ny + dy[d1] * dis;
								int cc = 0, len3 = inf;
								for (int k = 0; k < 4; ++k)
								{
									cc = 0;
									len3 = inf;
									if (nodes[k].first == nnx && !used[k])
									{
										len3 = min(len3, abs(nodes[k].second - nny));
										++cc;
									}
									if (nodes[k].second == nny && !used[k])
									{
										len3 = min(len3, abs(nodes[k].first - nnx));
										++cc;
									}
									if (cc == 0)
										continue;
									used[k] = true;
									int d2 = (d + 2) % 4;
									int len4 = inf;
									int ex = nnx + dx[d2] * dis, ey = ny + dy[d2] * dis;
									cc = 0;
									for (int k = 0; k < 4; ++k)
									{
										cc = 0;
										len4 = inf;
										if (nodes[k].first == ex && !used[k])
										{
											len4 = min(len4, abs(nodes[k].second - ey));
											++cc;
										}
										if (nodes[k].second == ey && !used[k])
										{
											len4 = min(len4, abs(nodes[k].first - ex));
											++cc;
										}
										if (cc == 0)
											continue;
										int tmp = max(max(len1, len2), max(len3, len4));
										if (tmp < ans && abs(i - nx) == abs(i - nny))
										{
											ans = tmp;
											e[0] = make_pair(i, j);
											e[1] = make_pair(nx, ny);
											e[2] = make_pair(nnx, nny);
											e[3] = make_pair(ex, ey);
										}
									}
									used[k] = false;
								}
							}	
							used[k] = false;
						}
					}
				}
				used[k] = false;
			}
		}
	}
	if (ans == inf)
		fout << -1 << endl;
	else
	{
		fout << ans << endl;
		for (int d = 0; d < 4; ++d)
		{
			fout << e[d].first << ' ' << e[d].second << endl;
		}
	}
}

int main()
{
	int T;
	fin >> T;
	while (T--)
	{
		Init();
		Main();
	}
	// cerr << clock() << endl;
	return 0;
}
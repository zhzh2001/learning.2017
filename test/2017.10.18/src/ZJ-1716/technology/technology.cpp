#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <ctime>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

ifstream fin("technology.in");
ofstream fout("technology.out");

string a, b;

int n, m;
bool vis[27];
int cnt;

void Init()
{
	fin >> n >> m >> a >> b;
	for (int i = 0; i < n; ++i)
		vis[int(a[i] - 'a')] = true;
	for (int i = 0; i < m; ++i)
		vis[int(b[i] - 'a')] = true;
	for (int i = 0; i < 27; ++i)
		cnt += vis[i];
}

char charset[27];
bool nsel[27];

vector<int> ans;

void Main()
{
	if (cnt == 1)
	{
		fout << n - m + 1 << endl;
		return;
	}
	ans.clear();
	for (int i = 0; i < n - m + 1; ++i)
	{
		string now = a.substr(i, m);
		for (int j = 0; j < 27; ++j)
			charset[j] = j;
		memset(nsel, 0x00, sizeof nsel);
		bool flag = true;
		for (int j = 0; j < m; ++j)
		{
			if (charset[(int)now[j] - 'a'] == b[j] - 'a')
			{
				nsel[(int)now[j] - 'a'] = true;
				nsel[(int)b[j] - 'a'] = true;
				continue;
			}
			else if (!nsel[(int)now[j] - 'a'] && !nsel[(int)b[j] - 'a'] && charset[now[j] - 'a'] == now[j] - 'a' && charset[(int)b[j] - 'a'] == b[j] - 'a')
			{
				charset[(int)now[j] - 'a'] = b[j] - 'a';
				charset[(int)b[j] - 'a'] = now[j] - 'a';
			} 
			else
			{
				flag = false;
				break;
			}
		}
		if (flag)
			ans.push_back(i + 1);
	}
	fout << ans.size() << endl;
	for (size_t i = 0; i < ans.size(); ++i)
		fout << ans[i] << ' ';
	fout << endl;
}

int main()
{
	Init();
	Main();
	// cerr << clock() << endl;
	return 0;
}
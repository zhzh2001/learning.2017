#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

ofstream fout("game.in");

int main()
{
	srand(GetTickCount());
	int T = 10;
	fout << T << endl;
	while (T--)
	{
		int len = 200;
		for (int i = 0; i < len / 2; ++i)
		{
			fout << (rand() % 2 ? 'w' : 'b');
		}
		fout << 'W';
		for (int i = len / 2 + 1; i < len; ++i)
		{
			fout << (rand() % 2 ? 'w' : 'b');
		}
		fout << endl;
	}
	return 0;
}
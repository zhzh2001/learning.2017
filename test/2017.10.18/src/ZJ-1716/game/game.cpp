#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

ifstream fin("game.in");
ofstream fout("game.out");

string str;
int pos, n;

void Init()
{
	fin >> str;
	n = str.length();
	for (size_t i = 0; i < str.length(); ++i)
		if (str[i] == 'W')
		{
			str[i] = 'w';
			pos = i;
		}
}

char ch[2] = { 'b', 'w' };

bool Solve(int l, int r, int pos, bool turn) // true: b, false: w
{
	if (str[l] == 'b' && str[r] == 'b')
		return turn;
	if (str[l] == 'w' && str[r] == 'w')
		return !turn;
	if (pos == r)
		return str[l] == ch[turn ^ 1];
	if (pos == l)
		return str[r] == ch[turn ^ 1];
	if (str[l] == 'b' && str[r] == 'w' && turn)
	{
		bool flag = false;
		for (int i = pos + 1; i < r; ++i)
		{
			if (str[i] == 'b')
				flag |= Solve(i, r, i, turn ^ 1);
			if (flag)
				break;
		}
		return flag;
	}
	if (str[r] == 'b' && str[l] == 'w' && turn)
	{
		bool flag = false;
		for (int i = pos - 1; i > l; --i)
		{
			if (str[i] == 'b')
				flag |= Solve(l, i, i, turn ^ 1);
			if (flag)
				break;
		}
		return flag;
	}
	if (str[r] == 'b' && str[l] == 'w' && !turn)
	{
		bool flag = false;
		for (int i = pos - 1; i > l; --i)
		{
			if (str[i] == 'w')
				flag |= Solve(l, i, i, turn ^ 1);
			if (flag)
				break;
		}
		return flag;
	}
	if (str[l] == 'b' && str[r] == 'w' && !turn)
	{
		bool flag = false;
		for (int i = pos + 1; i < r; ++i)
		{
			if (str[i] == 'b')
				flag |= Solve(i, r, i, turn ^ 1);
			if (flag)
				break;
		}
		return flag;
	}
	return true;
}

void Main()
{
	fout << (Solve(0, n - 1, pos, 1) ? "yes" : "no") << endl;
}

int main()
{
	int T;
	fin >> T;
	while (T--)
	{
		Init();
		Main();
	}
	// cerr << clock() << endl;
	return 0;
}
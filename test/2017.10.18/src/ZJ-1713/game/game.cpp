#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
struct node{
	int x,y,b;
}point[10];
inline bool cmp1(const node a,const node b){
	if(a.x==b.x) return a.y<b.y;
	return a.x<b.x;
}
inline bool cmp2(const node a,const node b){
	if(a.y==b.y) return a.x<b.x;
	return a.y<b.y;
}
inline bool pdx(int x,int y){
	return point[x].x<point[y].x;
}
inline bool pdy(int x,int y){
	return point[x].y<point[y].y;
}
int T;
int tong[5];
const int dx[]={0,-1,1,0,0};
const int dy[]={0,0,0,-1,1};
inline void solve1(){
	int xx1,xx2,yy1,yy2,mx=0;
	sort(point+1,point+5,cmp1);
	yy1=point[1].y,yy2=point[1].y;
	sort(point+1,point+5,cmp2);
	xx1=point[1].y,xx2=point[1].y;
	if(abs(xx1-xx2)!=abs(yy1-yy2)){
		fout<<"-1"<<endl;
	}else{
		sort(point+1,point+5,cmp1);
		mx=min(mx,min(abs(point[1].x-xx1),abs(point[1].x-xx2)));
		sort(point+1,point+5,cmp2);
		mx=min(mx,min(abs(point[1].y-yy1),abs(point[1].y-yy2)));
		fout<<mx<<endl;
		fout<<xx1<<" "<<yy1<<endl;
		fout<<xx1<<" "<<yy2<<endl;
		fout<<xx2<<" "<<yy1<<endl;
		fout<<xx2<<" "<<yy2<<endl;
	}
}
inline void solve2(){
	int mnx,mny,mxx,mxy;
	sort(point+1,point+5,cmp1);
	mnx=point[1].x,mxx=point[4].x;
	sort(point+1,point+5,cmp2);
	mny=point[1].y,mxy=point[4].y;
	fout<<(abs((mxy-mny)-(mxx-mnx)))<<endl;
	if(mxx-mnx>mxy-mxy){
		mxx-=mxy-mny;
	}else{
		mxy-=mxx-mnx;
	}
	fout<<mnx<<" "<<mny<<endl;
	fout<<mnx<<" "<<mxy<<endl;
	fout<<mxx<<" "<<mny<<endl;
	fout<<mxx<<" "<<mxy<<endl;
}
/*
inline void solve3(){
	int bian,now,xx1,xx2,yy1,yy2;
	sort(point+1,point+5,cmp1);
	if(tong[point[1]]>0){
		now=4;
	}else{
		now=1;
	}
	xx1=point[now].x,yy1=point[now].y;
	if(now==1){
		bian=min(abs(point[4].x-xx1),abs(point[4].y-yy1));
		xx2=xx1+point[4].x-bian;
		yy2=yy1+point[4].y-bian;
	}else{
		bian=min(abs(point[1].x-xx1),abs(point[1].y-yy1));
		xx2=xx1+point[1].x-bian;
		yy2=yy1+point[1].y-bian;
	}
	int yi1=min(abs(point[2].x-xx1),abs(point[2].x-xx2));
	yi1=min(yi1,min(abs(point[2].y-yy1),abs(point[2].y-yy2)));
	int yi2=min(abs(point[3].x-xx1),abs(point[3].x-xx2));
	yi2=min(yi2,min(abs(point[3].y-yy1),abs(point[3].y-yy2)));
	cout<<min(min(yi1,yi2),min(abs(point[1].x-xx2),abs(point[1].y-yy2)))<<endl;
	cout<<xx1<<" "<<yy1<<endl;
	cout<<xx1<<" "<<yy2<<endl;
	cout<<xx2<<" "<<yy1<<endl;
	cout<<xx2<<" "<<yy2<<endl;
}
*/
inline int abs(int x){
	if(x<0){
		return -x;
	}
	return x;
}
inline void baoli(){
	int xx1,xx2,yy1,yy2,ans=999999999;
	int mnxx1,mnxx2,mnyy1,mnyy2;
	for(int i=-30;i<=30;i++){
		for(int j=-30;j<=30;j++){
			for(int k=0;k<=80;k++){
				xx1=i,xx2=xx1+k,yy1=j,yy2=yy1+k;
				for(int ii=1;ii<=4;ii++){
					if(point[ii].x==xx1||point[ii].y==yy1){
						for(int jj=1;jj<=4;jj++){
							if(ii!=jj&&(point[jj].x==xx2||point[jj].y==yy1)){
								for(int kk=1;kk<=4;kk++){
									if(jj!=kk&&kk!=ii&&(point[kk].x==xx1||point[kk].y==yy2)){
										for(int ll=1;ll<=4;ll++){
											if(ll!=jj&&ii!=ll&&ll!=kk&&(point[ll].x==xx2||point[ll].y==yy2)){
												int nowans=abs(point[ii].x-xx1)+abs(point[ii].y-yy1);
												nowans+=abs(point[jj].x-xx2)+abs(point[jj].y-yy1);
												nowans+=abs(point[kk].x-xx1)+abs(point[kk].y-yy2);
												nowans+=abs(point[ll].x-xx2)+abs(point[ll].y-yy2);
												if(nowans<ans){
													ans=nowans;
													mnxx1=xx1,mnxx2=xx2;
													mnyy1=yy1,mnyy2=yy2;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	fout<<ans<<endl;
	fout<<mnxx1<<" "<<mnyy1<<endl;
	fout<<mnxx1<<" "<<mnyy2<<endl;
	fout<<mnxx1<<" "<<mnyy2<<endl;
	fout<<mnxx2<<" "<<mnyy2<<endl;
}
int main(){
	fin>>T;
	while(T--){
		tong[1]=0,tong[2]=0,tong[3]=0,tong[4]=0;
		fin>>point[1].x>>point[1].y>>point[2].x>>point[2].y;
		fin>>point[3].x>>point[3].y>>point[4].x>>point[4].y;
		point[1].b=1,point[2].b=2,point[3].b=3,point[4].b=4;
		sort(point+1,point+5,cmp1);
		if(point[1].x==point[2].x&&point[2].x==point[3].x){
			fout<<"-1"<<endl;
			continue;
		}
		if(point[2].x==point[3].x&&point[4].x==point[3].x){
			fout<<"-1"<<endl;
			continue;
		}
		if(pdx(1,2)&&pdx(2,3)&&pdx(3,4)&&pdy(1,2)&&pdy(2,3)&&pdy(3,4)){
			fout<<"-1"<<endl;
			continue;
		}
		if(pdx(1,2)) tong[point[1].b]++;
		if(pdx(3,4)) tong[point[4].b]++;
		sort(point+1,point+5,cmp2);
		if(point[1].y==point[2].y&&point[2].y==point[3].y){
			fout<<"-1"<<endl;
			continue;
		}
		if(point[2].y==point[3].y&&point[4].y==point[3].y){
			fout<<"-1"<<endl;
			continue;
		}
		if(pdy(1,2)) tong[point[1].b]++;
		if(pdy(3,4)) tong[point[4].b]++;
		if(tong[1]>1||tong[2]>1||tong[3]>1||tong[4]>1){
			if(tong[1]==2&&(tong[2]==0&&tong[3]==0&&tong[4]==0))continue;
			if(tong[2]==2&&(tong[1]==0&&tong[3]==0&&tong[4]==0))continue;
			if(tong[3]==2&&(tong[1]==0&&tong[2]==0&&tong[4]==0))continue;
			if(tong[4]==2&&(tong[1]==0&&tong[2]==0&&tong[3]==0))continue;
			fout<<"-1"<<endl;
			continue;
		}
		/*
		if(tong[1]==1&&tong[2]==1&&tong[3]==1&&tong[4]==1){
			solve1();
		}else if(tong[1]==0&&tong[2]==0&&tong[3]==0&&tong[4]==0){
			solve2();
		}/*else if(tong[1]+tong[2]+tong[3]+tong[4]==1){
			solve3();
		}else*/
		baoli();
	}
	return 0;
}
/*

in:
2
1 1
2 2
4 4
6 6
1 1
1 -1
-1 1
-1 -1


out:
0
1 1
1 -1
-1 1
-1 -1
-1

*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
//#include<iostream>
#include<fstream>
using namespace std;
ifstream fin("technology.in");
ofstream fout("technology.out");
int n,m,tong[30];
char a[200003],b[200003],c[200003];
int ans[200003],cnt=0;
int nxt[200003],xx[200003];
inline void kmp(){
	int i,j=0;
	nxt[1]=0;
	for(i=2;i<=m;++i){
		while(j&&c[j+1]!=c[i])j=nxt[j];
		if(c[j+1]==c[i])++j;
		nxt[i]=j;
	}
	i,j=0;
	for(i=1;i<=n;++i){
		while(j&&c[j+1]!=a[i])j=nxt[j];
		if(c[j+1]==a[i])++j;
		if(j==m){
			if(!xx[i-m+1]){
				ans[++cnt]=i-m+1;
				xx[i-m+1]=1;
			}
			j=nxt[j];
		}
	}
}
inline void solve(char x,char y){
	for(int i=1;i<=m;i++){
		c[i]=b[i];
		if(b[i]==x){
			c[i]=y;
		}else if(b[i]==y){
			c[i]=x;
		}
	}
	kmp();
}
inline void solve2(char x,char y,char n,char m){
	for(int i=1;i<=m;i++){
		c[i]=b[i];
		if(b[i]==x){
			c[i]=y;
		}else if(b[i]==y){
			c[i]=x;
		}else if(b[i]==m){
			c[i]=n;
		}else if(b[i]==n){
			c[i]=m;
		}
	}
	kmp();
}
inline void solveac(){
	solve('a','a'),solve('a','b');
	solve('a','c'),solve('b','c');
	fout<<cnt<<endl;
	sort(ans+1,ans+cnt+1);
	for(int i=1;i<=cnt;i++){
		if(ans[i]!=ans[i-1]){
			fout<<ans[i]<<" ";
		}
	}
}
inline void solvead(){
	solve2('a','a','c','c');
	solve2('a','b','c','c');
	solve2('a','c','b','d');
	solve2('a','d','b','c');
	
	solve2('b','c','d','d');
	solve2('b','d','c','c');
	
	solve2('c','d','a','a');
}
inline void solveae(){
	solve2('a','a','c','c');
	
	solve2('a','b','c','c');
	solve2('a','b','c','d');
	solve2('a','b','c','e');
	solve2('a','b','d','e');
	
	solve2('a','c','b','b');
	solve2('a','c','b','d');
	solve2('a','c','b','e');
	solve2('a','c','d','e');
	
	solve2('b','c','d','d');
	solve2('b','c','d','e');
	solve2('b','d','c','e');
	solve2('b','d','c','c');
	solve2('b','e','c','d');
	solve2('b','e','c','c');
	
	solve2('c','d','a','a');
	solve2('c','e','a','a');
	
	solve2('d','e','c','c');
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>a[i];
		tong[a[i]-'a']++;
	}
	for(int i=1;i<=m;i++){
		fin>>b[i];
		tong[b[i]-'a']++;
	}
	if(tong[0]==n+m){
		fout<<n-m+1<<endl;
		for(int i=1;i<=n-m+1;i++){
			fout<<i<<" ";
		}
		return 0;
	}
	if(tong[0]+tong[1]==n+m){
		solve('a','b');
		solve('a','a');
	}
	if(tong[0]+tong[1]+tong[2]==n+m){
		solveac();
	}
	if(tong[0]+tong[1]+tong[2]+tong[3]==n+m){
		solvead();
	}
	if(tong[0]+tong[1]+tong[2]+tong[3]+tong[4]==n+m){
		solveae();
	}
	fout<<cnt<<endl;
	sort(ans+1,ans+cnt+1);
	for(int i=1;i<=cnt;i++){
		if(ans[i]!=ans[i-1]){
			fout<<ans[i]<<" ";
		}
	}
	return 0;
}
/*

in:
9 5
acabacaba
acaba

out:
3
1

in:
11 5
abacabadaba
acaba
out:
3
1 3 7

in:
21 13
paraparallelogramgram
qolorreraglom
out:
1
5

*/

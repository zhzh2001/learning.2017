#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<fstream>
#include<string.h>
//#include<iostream>
using namespace std;
ifstream fin("base.in");
ofstream fout("base.out");
int T;
string s;
int len;
int jiyi[203][203][2];
int dp(int x,int y,int now){
	int &fanhui=jiyi[x][y][now];
	if(fanhui!=-1){
		return fanhui;
	}
	int oner=0;
	if(now==0){
		if(s[x]=='w'||s[x]=='W')oner=1;
		else oner=0;
	}else{
		if(s[y]=='b')oner=0;
		else oner=1;
	}
	if(x==0&&y==len-1){
		if(oner==0)return 1;
		else return 0;
	}
	int a=0,b=0;
	for(int i=0;i<=x-1;i++){
		if(s[i]=='b'&&oner==1){
			a|=dp(i,x,0);
		}
		if(s[i]=='w'&&oner==0){
			b|=dp(i,x,0);
		}
	}
	for(int i=y+1;i<=len-1;i++){
		if(s[i]=='b'&&oner==1){
			a|=dp(x,i,1);
		}
		if(s[i]=='w'&&oner==0){
			b|=dp(x,i,1);
		}
	}
	if(oner==1){
		return fanhui=a;
	}else{
		return fanhui=!b;
	}
}
int main(){
	fin>>T;
	while(T--){
		fin>>s;
		int len=s.length();
		if((s[0]=='w'||s[0]=='W')&&(s[len-1]=='w'||s[len-1]=='W')){
			fout<<"no"<<endl;
			continue;
		}
		if((s[0]=='b'||s[0]=='b')&&(s[len-1]=='b'||s[len-1]=='b')){
			fout<<"yes"<<endl;
			continue;
		}
		int now;
		for(int i=0;i<=len-1;i++){
			if(s[i]=='W'){
				now=i;
			}
		}
		memset(jiyi,-1,sizeof(jiyi));
		int x=dp(now,now,0);
		if(x==1){
			fout<<"yes"<<endl;
		}else{
			fout<<"no"<<endl;
		}
	}
	return 0;
}
/*

in:
5
W
bWbw
bwWwb
wbwbWb
Wbwbw

out:
no
yes
yes
yes
no

*/

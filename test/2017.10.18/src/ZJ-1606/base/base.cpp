#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int read()
{int t=0;char c;
	c=getchar();
	while(!(c>='0' && c<='9')) c=getchar();
	while(c>='0' && c<='9')
	  {
	  	t=t*10+c-48;c=getchar();
	  }
	return t;
}
int t,n,MAX,sx,minx,maxx,miny,maxy;
struct node{int x,y;};
bool boo[5],b1;
node s[5],ans[5],ss[5];
int main()
{int i,j,k;
    freopen("base.in","r",stdin);
    freopen("base.out","w",stdout);
    t=read();
    for(;t;t--)
      {
      	for(i=1;i<=4;i++) scanf("%d%d",&s[i].x,&s[i].y);
      	memset(boo,true,sizeof(boo));
      	MAX=2147483647;
      	for(i=1;i<4;i++)
      	  for(j=i+1;j<=4;j++)
      	    if(s[i].x!=s[j].x)
      	    {int a,b;
      	        memset(boo,true,sizeof(boo));
      	        boo[i]=false;boo[j]=false;
      	    	for(k=1;k<=4;k++) 
      	          if(boo[k])
      	            {
      	            	a=k;break;
					  }
				b=10-i-j-a;
				if(abs(s[i].x-s[j].x)!=abs(s[a].y-s[b].y)) continue;
				if(s[i].x<s[j].x) minx=i,maxx=j;else minx=j,maxx=i;
				if(s[a].x<s[b].x) miny=a,maxy=b;else miny=b,maxy=a;
				sx=0;
				sx=max(sx,abs(s[minx].y-s[miny].y));ans[minx].x=s[minx].x;ans[minx].y=s[miny].y;
				sx=max(sx,abs(s[maxx].y-s[maxy].y));ans[maxx].x=s[maxx].x;ans[maxx].y=s[maxy].y;
				sx=max(sx,abs(s[miny].x-s[maxx].x));ans[miny].x=s[maxx].x;ans[miny].y=s[miny].y;
				sx=max(sx,abs(s[maxy].x-s[minx].x));ans[maxy].x=s[minx].x;ans[maxy].y=s[maxy].y;
				if(sx<MAX)
				  {
				  	MAX=sx;
				  	ss[minx].x=ans[minx].x;ss[minx].y=ans[minx].y;
				  	ss[maxx].x=ans[maxx].x;ss[maxx].y=ans[maxx].y;
				  	ss[miny].x=ans[miny].x;ss[miny].y=ans[miny].y;
				  	ss[maxy].x=ans[maxy].x;ss[maxy].y=ans[maxy].y;
				  }
				sx=0;
				sx=max(sx,abs(s[minx].y-s[maxy].y));ans[minx].x=s[minx].x;ans[minx].y=s[maxy].y;
				sx=max(sx,abs(s[maxx].y-s[miny].y));ans[maxx].x=s[maxx].x;ans[maxx].y=s[miny].y;
				sx=max(sx,abs(s[miny].x-s[minx].x));ans[miny].x=s[minx].x;ans[miny].y=s[miny].y;
				sx=max(sx,abs(s[maxy].x-s[maxx].x));ans[maxy].x=s[maxx].x;ans[maxy].y=s[maxy].y;
				if(sx<MAX)
				  {
				  	MAX=sx;
				  	ss[minx].x=ans[minx].x;ss[minx].y=ans[minx].y;
				  	ss[maxx].x=ans[maxx].x;ss[maxx].y=ans[maxx].y;
				  	ss[miny].x=ans[miny].x;ss[miny].y=ans[miny].y;
				  	ss[maxy].x=ans[maxy].x;ss[maxy].y=ans[maxy].y;
				  }
			}
		if(MAX==2147483647) printf("-1\n");else 
		{
		printf("%d\n",MAX);
		for(i=1;i<=4;i++) printf("%d %d\n",ss[i].x,ss[i].y);
	    }
	  }
}

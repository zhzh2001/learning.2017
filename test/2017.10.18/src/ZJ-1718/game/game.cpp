#include <fstream>
#include <string>
using namespace std;
ifstream fin("game.in");
ofstream fout("game.out");
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		string s;
		fin >> s;
		int len = s.length();
		bool ans = false;
		for (int l = 0, r = len - 1; l < r; l++, r--)
			if (s[l] == 'W')
			{
				ans = s[r] == 'b';
				break;
			}
			else if (s[r] == 'W' || s[l] == s[r])
			{
				ans = s[l] == 'b';
				break;
			}
		fout << (ans ? "yes\n" : "no\n");
	}
	return 0;
}
#pragma GCC optimize 2
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("base.in");
ofstream fout("base.out");
const int INF = 0x3f3f3f3f, c[4][2] = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
struct point
{
	int x, y;
} p[4], ans[4];
int dist, perm[24][4] = {{0, 1, 2, 3}};
void check(int x, int y, int d)
{
	for (int P = 0; P < 24; P++)
	{
		int now = 0;
		for (int i = 0; i < 4; i++)
		{
			if (x + c[perm[P][i]][0] * d != p[i].x && y + c[perm[P][i]][1] * d != p[i].y)
			{
				now = INF;
				break;
			}
			now = max(now, max(abs(x + c[perm[P][i]][0] * d - p[i].x), abs(y + c[perm[P][i]][1] * d - p[i].y)));
		}
		if (now < dist)
		{
			dist = now;
			for (int i = 0; i < 4; i++)
			{
				ans[i].x = x + c[perm[P][i]][0] * d;
				ans[i].y = y + c[perm[P][i]][1] * d;
			}
		}
	}
}
int main()
{
	for (int i = 1; i < 24; i++)
	{
		copy(perm[i - 1], perm[i - 1] + 4, perm[i]);
		next_permutation(perm[i], perm[i] + 4);
	}
	int t;
	fin >> t;
	while (t--)
	{
		for (int i = 0; i < 4; i++)
			fin >> p[i].x >> p[i].y;
		int d[12], dn = 0;
		for (int i = 0; i < 4; i++)
			for (int j = i + 1; j < 4; j++)
			{
				d[dn++] = abs(p[i].x - p[j].x);
				d[dn++] = abs(p[i].y - p[j].y);
			}
		sort(d, d + dn);
		dn = unique(d, d + dn) - d;
		int x[432], xn = 0, y[432], yn = 0;
		dist = INF;
		for (int i = 0; i < dn; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				x[xn++] = p[j].x - d[i];
				x[xn++] = p[j].x;
				x[xn++] = p[j].x + d[i];
				y[yn++] = p[j].y - d[i];
				y[yn++] = p[j].y;
				y[yn++] = p[j].y + d[i];
			}
			for (int P = 0; P < 24; P++)
			{
				int xl = INF, xr = -INF, yl = INF, yr = -INF;
				for (int j = 0; j < 4; j++)
				{
					int nx = p[j].x - c[perm[P][j]][0] * d[i], ny = p[j].y - c[perm[P][j]][1] * d[i];
					xl = min(xl, nx);
					xr = max(xr, nx);
					yl = min(yl, ny);
					yr = max(yr, ny);
				}
				x[xn++] = (xl + xr) / 2;
				y[yn++] = (yl + yr) / 2;
			}
			sort(x, x + xn);
			xn = unique(x, x + xn) - x;
			sort(y, y + yn);
			yn = unique(y, y + yn) - y;
			for (int xi = 0; xi < xn; xi++)
				for (int yi = 0; yi < yn; yi++)
					check(x[xi], y[yi], d[i]);
		}
		if (dist == INF)
			fout << -1 << endl;
		else
		{
			fout << dist << endl;
			for (int i = 0; i < 4; i++)
				fout << ans[i].x << ' ' << ans[i].y << endl;
		}
	}
	return 0;
}
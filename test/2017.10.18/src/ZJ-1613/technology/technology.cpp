#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<map>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=2e5+7;
int n,m,res[N],ans,f[N];
char s1[N],s2[N];
struct node{
	int x,y;
}a[N];
int tot,cnt,pre[N],q[N],flag_T;
const int rxd=1e7+7;
map<int,int> Map_hash;

int check(int x){
	For(i,1,tot){
		if (q[i]==a[x].x||q[i]==a[x].y) return 0; 
	}return 1;
}

void init(){
	int Hash=0;
//	For(i,'a','d') printf("%c %c  ",i,pre[i]); printf("\n");
//	For(i,1,tot) printf("%c ",q[i]); printf("\n");
	For(i,0,m-1){
		int x=s2[i];
		if (pre[s2[i]]) x=pre[s2[i]];
		x-='a'-1;
		Hash=(Hash*27+x)%rxd;
	}
	Map_hash[Hash]=1;
//	printf("%d\n",Hash);
}

void dfs(int x){
	if (x>cnt) return;
	if (check(x)){
		q[++tot]=a[x].x;
		q[++tot]=a[x].y;
		pre[a[x].x]=a[x].y;
		pre[a[x].y]=a[x].x;
		ans++;
		init();
		dfs(x+1);
		pre[a[x].x]=pre[a[x].y]=0;
		tot-=2;
	}
	dfs(x+1);
}

int qaq[27];

void solve(){
	For(i,'a','a'+flag_T-1) For(j,i+1,'a'+flag_T-1){
		cnt++;
		a[cnt].x=i,a[cnt].y=j;
	}
	init();
	dfs(1);
	int Hash=0;ans=0;
//	printf("\n");
	For(i,1,26){
		qaq[i]=i;
		For(j,1,m) (qaq[i]*=27)%=rxd;
	}
	For(i,0,n-1){
		int x=s1[i]-'a'+1;
		Hash=(Hash*27+x)%rxd;
		if (i>(m-1)){
			Hash+=rxd;
			Hash-=qaq[s1[i-m]-'a'+1];
			Hash%=rxd;
		}
//		printf("%d\n",Hash);
		if (i>=(m-1)&&Map_hash[Hash]) res[++ans]=i+2-m;
	}
	printf("%d\n",ans);
	For(i,1,ans) printf("%d ",res[i]);
}

int main(){
	// say hello

	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);

	n=read(),m=read();
	scanf("%s%s",s1,s2);
	
	flag_T=1;
	ll pre=(n-m)*m;
	For(i,0,n-1) flag_T=max(flag_T,s1[i]-'a'+1);
	if (flag_T<10&&(pre>1e8)){
		solve();
		return 0;
	}
	
	For(l,0,n-1){
		int flag=1;
		For(i,'a','z') f[i]=0;
		For(k,l,l+m-1){
			if ((!f[s1[k]]&&s1[k]==s2[k-l]) || (f[s1[k]]==s2[k-l]) || (!f[s1[k]]&&!f[s2[k-l]])) 
				f[s1[k]]=s2[k-l],f[s2[k-l]]=s1[k];
			else {
				flag=0;
				break;
			}
		}
		if (flag) res[++ans]=l+1;
	}
	printf("%d\n",ans);
	For(i,1,ans) printf("%d ",res[i]);

	// say goodbye
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=100007;
char s[N];
int n;

int solve(int l,int r){
	if (s[l]=='W'){
		if (l==r||s[r]=='w') return 0;
		return 1;
	}
	if (s[r]=='W'){
		if (l==r||s[l]=='w') return 0;
		return 1;
	}
	if (s[l]=='w'&&s[r]=='w') return 0;
	if (s[l]=='b'&&s[r]=='b') return 1;
	return solve(l+1,r-1);
}

int main(){
	// say hello

	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);

	n=read();
	For(i,1,n){
		scanf("%s",s);
		int l=0,r=strlen(s)-1;
		if (solve(l,r)) printf("yes\n");
		else printf("no\n");		
	}

	// say goodbye
}


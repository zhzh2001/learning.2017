// suprise father markdown
// zenzen shiranai...
#include <bits/stdc++.h>
#define ll long long
#define N 200020
#define SEARCH_SIZE 100
#define INF (1 << 30)
#define fs first
#define sc second
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int x[10], y[10];
int misaka[][4] = {
	{0, 1, 2, 3},
	{0, 1, 3, 2},
	{0, 2, 1, 3},
	{0, 2, 3, 1},
	{0, 3, 1, 2},
	{0, 3, 2, 1},
	{1, 0, 2, 3},
	{1, 0, 3, 2},
	{1, 2, 0, 3},
	{1, 2, 3, 0},
	{1, 3, 0, 2},
	{1, 3, 2, 0},
	{2, 0, 1, 3},
	{2, 0, 3, 1},
	{2, 1, 0, 3},
	{2, 1, 3, 0},
	{2, 3, 0, 1},
	{2, 3, 1, 0},
	{3, 0, 1, 2},
	{3, 0, 2, 1},
	{3, 1, 0, 2},
	{3, 1, 2, 0},
	{3, 2, 0, 1},
	{3, 2, 1, 0}
};
pair<int, int> mikoto[4], sagiri, kousaka[4], kasumi[4];
int check(int x1, int y1, int x2, int y2) {
	// printf("%d %d %d %d\n", x1, y1, x2, y2);
	if (x1 > x2) swap(x1, x2);
	if (y1 > y2) swap(y1, y2);
	for (int i = 0; i < 4; i++)
		if (x[i] != x1 && x[i] != x2 && y[i] != y1 && y[i] != y2)
			return INF;
	int sum = INF;
	mikoto[0].fs = mikoto[2].fs = x1;
	mikoto[1].fs = mikoto[3].fs = x2;
	mikoto[0].sc = mikoto[1].sc = y1;
	mikoto[2].sc = mikoto[3].sc = y2;
	for (int k = 0; k < 24; k++) {
		int tmp = 0, flag = 0;
		for (int i = 0; i < 4; i++) {
			sagiri = mikoto[misaka[k][i]];
			if (sagiri.fs == x[i]) {
				tmp = max(tmp, abs(y[i] - sagiri.sc));
			} else if (sagiri.sc == y[i]) {
				tmp = max(tmp, abs(x[i] - sagiri.fs));
			} else {
				flag = 1;
			}
		}
		if (!flag && tmp < sum) {
			sum = tmp;
			kousaka[0] = mikoto[misaka[k][0]];
			kousaka[1] = mikoto[misaka[k][1]];
			kousaka[2] = mikoto[misaka[k][2]];
			kousaka[3] = mikoto[misaka[k][3]];
		}
	}
	// if (sum < INF) printf("%d\n", sum);
	return sum;
}
int main(int argc, char const *argv[]) {
	freopen("base.in", "r", stdin);
	freopen("base.out", "w", stdout);
	int T = read();
	while (T --) {
		int ans = INF;
		for (int i = 0; i < 4; i++)
			x[i] = read(), y[i] = read();
		for (int x1 = -SEARCH_SIZE; x1 <= SEARCH_SIZE; x1 ++)
			for (int x2 = x1 + 1; x2 <= SEARCH_SIZE; x2 ++)
				for (int y1 = -SEARCH_SIZE; y1 <= SEARCH_SIZE; y1 ++) {
					int res = check(x1, y1, x2, y1 + x2 - x1);
					if (res < ans) {
						ans = res;
						kasumi[0] = kousaka[0];
						kasumi[1] = kousaka[1];
						kasumi[2] = kousaka[2];
						kasumi[3] = kousaka[3];
					}
				}
		printf("%d\n", ans == INF ? -1 : ans);
		if (ans < INF) {
			printf("%d %d\n", kasumi[0].fs, kasumi[0].sc);
			printf("%d %d\n", kasumi[1].fs, kasumi[1].sc);
			printf("%d %d\n", kasumi[2].fs, kasumi[2].sc);
			printf("%d %d\n", kasumi[3].fs, kasumi[3].sc);
		}
	}
	return 0;
}

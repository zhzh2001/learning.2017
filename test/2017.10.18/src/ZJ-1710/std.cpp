#include <bits/stdc++.h>
#define file_name "std"
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(int argc, char const *argv[]) {
	freopen(file_name".in", "r", stdin);
	freopen(file_name".out", "w", stdout);
	int a = read(), b = read();
	printf("%d\n", a + b);
	return 0;
}

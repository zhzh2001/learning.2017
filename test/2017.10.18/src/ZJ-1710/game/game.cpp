// SZB 保佑
// onegaishimasu...
#include <bits/stdc++.h>
#define ll long long
#define N 5020
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char str[N];
int main(int argc, char const *argv[]) {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	int T = read();
	while (T --) {
		scanf("%s", str + 1);
		int len = strlen(str + 1);
		if (str[1] == 'W') {
			puts(str[len] == 'b' ? "yes" : "no");
			continue;
		}
		if (str[len] == 'W') {
			puts(str[1] == 'b' ? "yes" : "no");
			continue;
		}
		if (str[1] == str[len]) {
			puts(str[1] == 'b' ? "yes" : "no");
			continue;
		}
		int k1 = 1;
		while (str[k1 + 1] == str[k1]) k1++;
		int k2 = len;
		while (str[k2 - 1] == str[k2]) k2--;
		k2 = len - k2 + 1;
		// printf("%d %d\n", k1, k2);
		if (str[1] == 'b') {
			if (k1 > k2 || k1 == k2 && str[k1 + 1] == 'W')
				puts("yes");
			else
				puts("no");
			continue;
		}
		if (str[1] == 'w') {
			if (k2 > k1 || k1 == k2 && str[len - k2] == 'W')
				puts("yes");
			else
				puts("no");
			continue;
		}
		puts("no");
	}
	return 0;
}

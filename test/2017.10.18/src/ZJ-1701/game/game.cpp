#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e4+5;
map<char,int>F;
char s[N];
int n,m;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	F['b']=1;
	F['w']=-1;
	F['W']=10;
	while(n--){
		scanf("%s",s);
		bool ans;
		int l=0,r=strlen(s)-1,sum;
		while(1){
			if(l==r){ans=0;break;}
			sum=F[s[l++]]+F[s[r--]];
			if(sum==2){ans=1;break;}
			if(sum==-2){ans=0;break;}
			if(sum==0)continue;
			sum=sum-10;
			if(sum==1){ans=1;break;}
			if(sum==-1){ans=0;break;}
		}
		if(ans)puts("yes");else puts("no");
	}
}

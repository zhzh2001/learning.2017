#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
using namespace std ; 

inline int read() {
	int x = 0, f = 1; 
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); } 
	while(ch>='0'&&ch<='9') { x=x*10+ch-48; ch=getchar(); } 
	return x * f ; 
}

const int alpha = 100 ; 
int tot,W,ans,T ; 
struct node{
	int x,y ; 
}a[5];
int b[5],tmp[5],visit[5] ; 
int x[5],y[5],ex[5],ey[5],ansx[5],ansy[5] ; 

inline bool cmp(node a,node b) {
	if(a.y!=b.y) return a.y < b.y ; 
	return a.x < b.x ; 
}

int main() 
{
	freopen("base.in","r",stdin) ; 
	freopen("base.out","w",stdout) ; 
	T = read();
	b[1]=1; b[2]=2; b[3]=3;b[4]=4;
	while(T--) {
		For(i,1,4) a[i].x=read(),a[i].y=read(); 
		bool f = 0 ; 
		
		    
		sort(a+1,a+5,cmp);
		For(i,1,4) x[i]=a[i].x,y[i]=a[i].y;
		For(i,1,3) 
		  For(j,i+1,4) 
		    if(x[i]==x[j]) f=1;
        For(i,1,3) 
          For(j,i+1,4) 
            if(y[i]==y[j]) f=1;
        if(!f) {
        	printf("-1\n") ;
        	continue ;
		}
		ans = 1e9;
		For(i,-alpha,alpha) 
			For(j,-alpha,alpha) 
		      For(len,1,2*alpha) {
		      	ex[1] = i;ey[1] = j;
		      	ex[2] = i+len;ey[2] = j;
		      	ex[3] = i;ey[3] = j+len;
		      	ex[4] = i+len;ey[4] = j+len;
		      	
		      	
		      		bool flag = 0;
		      		For(l,1,4) 
		      		  if( (x[l]-ex[l])*(y[l]-ey[l])!=0 ) flag = 1 ;
		      		if(flag) continue;
		      		flag = 0 ; 
		      		For(l,1,4) 
			          if(x[l]!=ex[l] || y[l]!=ey[l]) flag = 1 ; 
			        if(!flag) continue ;   
		      		W = 0 ; 
		      		For(l,1,4) 
   				      W = max(W,abs(x[l]-ex[l])) , W = max(W,abs( x[l]-ex[l] ));
				    if( W<ans ) {
				  	  ans = W;
				  	  For(l,1,4) 
				  		ansx[l]=ex[l],ansy[l]=ey[l]; 
				    }
 			  }
		if(ans!=1e9) {
			printf("%d\n",ans) ; 
			For(i,1,4) printf("%d %d\n",ansx[i],ansy[i]); 
		}
		else printf("-1\n"); 
	}
	return 0 ; 
}




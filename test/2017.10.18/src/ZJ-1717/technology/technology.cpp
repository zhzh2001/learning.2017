#include <bits/stdc++.h> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define Dow(i,j,k) for(int i=j;i>=k;i--) 
using namespace std ; 

inline int read() {
	int x = 0, f = 1; 
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); } 
	while(ch>='0'&&ch<='9') { x=x*10+ch-48; ch=getchar(); } 
	return x * f ; 
}

const int N = 100011,mod=144879959,pri=37;
int l1,l2,cnt,sum,tot;
int hash[N],ma,mp[201],ans[N]; 
char s1[N],s2[N] ; 

int main() 
{
	freopen("technology.in","r",stdin);
	freopen("technology.out","w",stdout);
	l1=read(); l2=read();
	scanf("%s",s1+1);
	scanf("%s",s2+1);
	char c = s1[1] ; bool flag = 0;
	For(i,2,l1) if(c!=s1[i]) flag=1;
	c = s2[1]; 
	For(i,2,l2) if(c!=s2[i]) flag=1;
	if(!flag) {
		printf("%d\n",l1-l2+1);
		For(i,1,l1-l2+1) printf("%d ",i);
		return 0;
	}
	
	For(i,1,l2) 
	    if(!mp[s2[i]]) mp[s2[i]] = ++cnt;
	ma = 1;
	For(i,1,l2) ma=(1LL*ma* pri +mp[s2[i]] )%mod;
	
	
	For(i,1,l1-l2+1) {
		hash[i]=1;
		cnt = 0;
		For(j,0,200) mp[j]=0;
		For(j,i,i+l2-1) 
			if(!mp[ s1[j] ]) mp[ s1[j] ] = ++cnt;
		For(j,i,i+l2-1) 
		    hash[i]=( 1LL*hash[i]* pri +mp[s1[j]] )%mod;
	} 
	For(i,1,l1-l2+1) 
        if(hash[i]==ma) {
		    sum++;
		    ans[++tot]=i;
		}
    printf("%d\n",sum);
    For(i,1,tot) printf("%d ",ans[i]);
	return 0 ; 
}

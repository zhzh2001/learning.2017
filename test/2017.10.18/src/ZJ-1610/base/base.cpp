#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
int s[5][2],b[5][2],r[5][2];
int Ans,a[5][2];

inline void Update(){
	int dis=-1;
	Rep(i,1,4){
		int cnt=0;Rep(j,1,4) if (j!=i){
			if (r[i][0]==r[j][0]){
				cnt++;int dis_=abs(r[i][1]-r[j][1]);
				if (dis==-1) dis=dis_;
				if (dis!=dis_) return;
			}else
			if (r[i][1]==r[j][1]){
				cnt++;int dis_=abs(r[i][0]-r[j][0]);
				if (dis==-1) dis=dis_;
				if (dis!=dis_) return;
			}
		}
		if (cnt!=2) return;
	}
	int res=0;
	Rep(i,1,4){
		res=max(res,max(abs(s[i][0]-r[i][0]),abs(s[i][1]-r[i][1])));
	}
	if (res<Ans){
		Ans=res;
		Rep(i,1,4) Rep(t,0,1) a[i][t]=r[i][t];
	}
}
int r_[5][2];
inline void Surfer(){
	Rep(i,1,4) Rep(j,2,4) Rep(t,0,1) if (r[i][t]==r[j][t]){
		static int c[2];
		c[0]=c[1]=0;
		Rep(k,1,4) if (k!=i && k!=j){
			if (r[k][t^1]==r[i][t^1]) c[0]++;
			if (r[k][t^1]==r[j][t^1]) c[1]++;
		}
		if (c[0]!=1 || c[1]!=1) continue;
		
		bool flag=true;
		Rep(k,1,4) if (k!=i && k!=j) if (b[k][t]) flag=false;
		if (!flag) continue;
		
		Rep(k,1,4) Rep(p,0,1) r_[k][p]=r[k][p];
		int dis=abs(r[i][t^1]-r[j][t^1]);
		Rep(k,1,4) if (k!=i && k!=j){
			if (r[k][t^1]==r[i][t^1]){
				r[k][t]+=r[i][t]+dis-r[k][t];
			}
			if (r[k][t^1]==r[j][t^1]){
				r[k][t]+=r[j][t]+dis-r[k][t];
			}
		}
		Update();
		Rep(k,1,4) if (k!=i && k!=j){
			if (r[k][t^1]==r[i][t^1]){
				r[k][t]+=r[i][t]-dis-r[k][t];
			}
			if (r[k][t^1]==r[j][t^1]){
				r[k][t]+=r[j][t]-dis-r[k][t];
			}
		}
		Update();
		Rep(k,1,4) Rep(p,0,1) r[k][p]=r_[k][p];
	}
}
inline void Raslip(){
	Rep(t,0,1) Rep(i,1,4) Rep(j,i+1,4) if (r[i][t]==r[j][t]){
		Rep(x,1,4) Rep(y,x+1,4) if (x!=i && x!=j && y!=i && y!=j && r[x][t]==r[y][t]){
			int flag=true;
			Rep(k,1,4) if (b[k][t^1]) flag=false;
			if (flag==false) continue;
			int dis=abs(r[i][t]-r[x][t]);
			int mip=(min(r[i][t^1],r[j][t^1])+min(r[x][t^1],r[y][t^1]))/2;
			int map=mip+dis;
			Rep(k,1,4) Rep(p,0,1) r_[k][p]=r[k][p];
			if (r[i][t^1]<r[j][t^1]){
				r[i][t^1]=mip,r[j][t^1]=map;
			}
			if (r[i][t^1]>r[j][t^1]){
				r[i][t^1]=map,r[j][t^1]=mip;
			}
			if (r[x][t^1]<r[y][t^1]){
				r[x][t^1]=mip,r[y][t^1]=map;
			}
			if (r[x][t^1]>r[y][t^1]){
				r[x][t^1]=map,r[y][t^1]=mip;
			}
			Update();
			Rep(k,1,4) Rep(p,0,1) r[k][p]=r_[k][p];
		}
	}
}
inline void Raslip_(){
	Rep(t,0,1) Rep(i,1,4) Rep(j,i+1,4) if (r[i][t]==r[j][t]){
		Rep(x,1,4) Rep(y,x+1,4) if (x!=i && x!=j && y!=i && y!=j && r[x][t]==r[y][t]){
			int flag=true;
			Rep(k,1,4) if (b[k][t^1]) flag=false;
			if (flag==false) continue;
			int dis=abs(r[i][t]-r[x][t]);
			int minval=min(min(r[i][t^1],r[j][t^1]),min(r[x][t^1],r[y][t^1]));
			int maxval=max(max(r[i][t^1],r[j][t^1]),max(r[x][t^1],r[y][t^1]));
			int mip=(minval+maxval)/2-dis/2;
			int map=mip+dis;
			Rep(k,1,4) Rep(p,0,1) r_[k][p]=r[k][p];
			if (r[i][t^1]<r[j][t^1]){
				r[i][t^1]=mip,r[j][t^1]=map;
			}
			if (r[i][t^1]>r[j][t^1]){
				r[i][t^1]=map,r[j][t^1]=mip;
			}
			if (r[x][t^1]<r[y][t^1]){
				r[x][t^1]=mip,r[y][t^1]=map;
			}
			if (r[x][t^1]>r[y][t^1]){
				r[x][t^1]=map,r[y][t^1]=mip;
			}
			Update();
			Rep(k,1,4) Rep(p,0,1) r[k][p]=r_[k][p];
		}
	}
}
void Dfs(){
//	puts("here");
	Update();
	Surfer();
	Raslip();
	Raslip_();
	Rep(i,1,4) Rep(j,1,4) if (i!=j) Rep(t,0,1){
		if (!b[i][t]){
			int r_=r[i][t];r[i][t]=r[j][t];
			
			b[i][t]++;b[i][t^1]++;
			if (r[i][t]!=r[j][t] || r[i][t^1]!=r[j][t^1]) b[j][t]++;
			Dfs();
			b[i][t]--;b[i][t^1]--;
			if (r[i][t]!=r[j][t] || r[i][t^1]!=r[j][t^1]) b[j][t]--;
			
			if (b[i][t]==0) r[i][t]=r_;
//			if (b[i][t^1]==0) r[i][t^1]=s[i][t^1];
//			if (b[j][t]==0) r[j][t]=s[j][t];
		}
	}
}
void work(int T){
	Ans=1e9;
	Rep(i,1,4) Rep(t,0,1) r[i][t]=s[i][t]=read(),b[i][t]=0;
//	Raslip();
	Dfs();
	/*
	if (T==32){
		Rep(i,1,4){
			Rep(t,0,1) printf("%d ",s[i][t]);puts("");
		}
	}
	return;
	*/
	if (Ans==1e9) puts("-1");else{
		printf("%d\n",Ans);
		Rep(i,1,4){
			Rep(t,0,1) printf("%d ",a[i][t]);puts("");
		}
	}
}
int main(){
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	int T=read();
	Rep(i,1,T) work(i);
}
/*
1
204648 -222029
204648 -342052
276858 282633
276858 -57725

1
-73280536 -77044613
-23056233 96637929
-37471361 20074601
3282792 73226926

*/

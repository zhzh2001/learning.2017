#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
const int N=1000005;

int dat1[N],dat2[N],res[55][8];
int main(){
	freopen("base.in","r",stdin);
	int T=read();
	freopen("base.out","r",stdin);
	int len1=0,x;
	while (scanf("%d",&x)==1){
		dat1[++len1]=x;
		if (x!=-1) Rep(i,0,7) read();
	}
	freopen("base_sample2.ans","r",stdin);
	int len2=0;
	while (scanf("%d",&x)==1){
		dat2[++len2]=x;
		if (x!=-1) Rep(i,0,7) res[len2][i]=read();
	}
	printf("len1=%d len2=%d\n",len1,len2);
	Rep(i,1,len2){
		printf("%d %d\n",dat1[i],dat2[i]);
		if (dat1[i]!=dat2[i]){
			puts("");
			printf("Error at %d\n",i);
			Rep(t,0,7){
				printf("%d ",res[i][t]);
				if (t%2==1) puts("");
			}
			puts("here");
			return printf("Error at %d",i),0;
		}
	}
	puts("ACccc!!");
}


#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c<='9'&&c>='0')	t=t*10LL+c-48LL,c=getchar();
	return t*f;
}
const int N=100005;

int n,m;
inline void Getstring(int *S){
	int ch=getchar();
	while (ch<'a' || ch>'z') ch=getchar();
	while (ch>='a' && ch<='z') S[++*S]=ch,ch=getchar();
}
int bef[200];
inline void Rework(int *S){
	int ch='a';
	Rep(i,1,*S){
		if (!bef[S[i]]){
			bef[S[i]]=ch;bef[ch]=S[i];
			while (bef[ch]!=0) ch++;
		}
		S[i]=bef[S[i]];
	}
}
int S[N],B[N],B_[N],T[N];
vector<int>Ans;
int main(){
	freopen("technology.in","r",stdin);
	n=read(),m=read();
	Getstring(S);
	Getstring(B_);
	Rep(i,1,n-m+1){
		*T=0;
		Rep(j,i,j+m-1) T[++*T]=S[j];
		memset(bef,0,sizeof(bef));
		Rework(T);
		Rep(j,1,m) B[j]=B_[j];
		Rework(B);
		bool flag=true;
		Rep(j,1,m) if (T[j]!=B[j]) flag=false;
		Rep(j,1,m) printf("%c",T[j]);puts("");
		Rep(j,1,m) printf("%c",B[j]);puts("");puts("");
		printf("%c\n",bef['c']);
		if (flag) Ans.push_back(i);
	}
	Rep(i,0,Ans.size()-1) printf("%d ",Ans[i]);
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
struct D{
	ll x,y;
}a[10],b[10],ans[10];
int T;
ll dis;
inline void pd()
{
	ll zyy=0;
	For(i,1,4)
	{
		if(a[i].x!=b[i].x&&a[i].y!=b[i].y)return;
		zyy=max(zyy,abs(a[i].x-b[i].x));
		zyy=max(zyy,abs(a[i].y-b[i].y));
	}
	if(zyy<dis)
	{
		dis=zyy;
		For(i,1,4)
		{
			ans[i].x=a[i].x;
			ans[i].y=a[i].y;
		}
	}
}
inline void d1()
{
	int x1,x2,x3,x4;
	if(b[1].y>b[2].y)x1=1,x2=2;
			else x1=2,x2=1;
	if(b[3].y>b[4].y)x3=3,x4=4;
			else x3=4,x4=3;
	For(i,0,1)
		For(j,0,1)
			For(k,0,1)
				For(kk,0,1)
				{
					a[1].x=a[2].x=(b[1].x+b[2].x+i)/2;
					a[3].x=a[4].x=(b[3].x+b[4].x+j)/2;
					a[x1].y=a[x3].y=(b[x1].y+b[x3].y+k)/2;
					a[x2].y=a[x4].y=(b[x2].y+b[x4].y+kk)/2;
					pd();	
				}
}
inline void d2()
{
	int x1,x2,x3,x4;
	if(b[1].x>b[2].x)x1=1,x2=2;
			else x1=2,x2=1;
	if(b[3].x>b[4].x)x3=3,x4=4;
			else x3=4,x4=3;
	For(i,0,1)
		For(j,0,1)
			For(k,0,1)
				For(kk,0,1)
				{
					a[1].y=a[2].y=(b[1].y+b[2].y+i)/2;
					a[3].y=a[4].y=(b[3].y+b[4].y+j)/2;
					a[x1].x=a[x3].x=(b[x1].x+b[x3].x+k)/2;
					a[x2].x=a[x4].x=(b[x2].x+b[x4].x+kk)/2;
					pd();	
				}
}
int main()
{
	freopen("base.in","r",stdin);
//	freopen("base.out","w",stdout);
	T=read();
	while(T--)
	{
		For(i,1,4)b[i].x=read(),b[i].y=read();
		dis=1e9;
		d1();d2();
		if(dis==1e9)
		{
			cout<<"-1"<<endl;
			continue;
		}
		cout<<dis<<endl;
		For(i,1,4)cout<<ans[i].x<<" "<<ans[i].y<<endl;
	}
}


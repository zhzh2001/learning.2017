#include<bits/stdc++.h>
#define int long long
using namespace std;
const int N=100005;
inline int read()
{
	int f=1,x=0;
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
int x;
struct xxx{
	int x,y,k;
}yuan[10],now[10],ans[10],cho[10];
int anst=1e18;
int usex[10],usey[10];
int ret;
bool com(xxx a,xxx b)
{
	if(a.x!=b.x)return a.x<b.x;
	else return a.y<b.y;
}
bool com1(xxx a,xxx b)
{
	if(a.y!=b.y)return a.y<b.y;
	else return a.x<b.x;
}
bool com2(xxx a,xxx b)
{
	return a.k<b.k;
}
int used[10];
void dfs2(int k,int ret)
{
	if(k>4){
		if(ret<anst){
			for(int i=1;i<=4;i++)now[i].k=used[i],ans[i]=now[i];
			anst=ret;
		}
	}
	for(int i=1;i<=4;i++)
		if(!used[i]&&(yuan[k].x==now[i].x)+(yuan[k].y==now[i].y)>=1){
			used[i]=k;
			dfs2(k+1,max(max(abs(yuan[k].x-now[i].x),abs(yuan[k].y-now[i].y)),ret));
			used[i]=0;
		}
}
void check()
{
	ret=0;
	sort(now+1,now+5,com);
	bool b=false;
	if(now[1].x==now[2].x&&now[3].x==now[4].x){
		if(now[2].y==now[4].y&&now[1].y==now[3].y){
			if(now[1].x!=now[3].x&&now[1].y!=now[2].y){
				if(abs(now[1].x-now[3].x)==abs(now[1].y-now[2].y))
				b=true;
			}
		}
	}
	if(!b)return;
	dfs2(1,0);
}
void dfs(int k)
{
	x++;
	if(k==5){
		check();
		return;
	}
	if(k==4){
		if(now[1].x==now[2].x)now[4].x=now[3].x;
		else if(now[1].x==now[3].x)now[4].x=now[2].x;
		else now[4].x=now[1].x;
		if(now[1].y==now[2].y)now[4].y=now[3].y;
		else if(now[1].y==now[3].y)now[4].y=now[2].y;
		else now[4].y=now[1].y;
		dfs(k+1);
	}
	else for(int i=1;i<=4;i++){
		if(usex[i]>1)continue;
			for(int j=1;j<=4;j++){
				if(usey[j]>1)continue;
				usex[i]++;
				usey[j]++;
				now[k].x=yuan[i].x;
				now[k].y=yuan[j].y;
				dfs(k+1);
				usex[i]--;
				usey[j]--;
		}
	}
}
signed main()
{
	freopen("base.in","r",stdin);
	freopen("base.out","w",stdout);
	int T=read();
	while(T--){
		memset(usex,0,sizeof(usex));
		memset(usey,0,sizeof(usey));
		for(int i=1;i<=4;i++){
			yuan[i].x=read();
			yuan[i].y=read();
			yuan[i].k=i;
		}
		anst=1e18;
		dfs(1);
		if(anst==1e18)puts("-1");
		else{
			write(anst);
			puts("");
			sort(ans+1,ans+5,com2);
			for(int i=1;i<=4;i++)
				printf("%lld %lld\n",ans[i].x,ans[i].y);
		}
	}
	return 0;
}


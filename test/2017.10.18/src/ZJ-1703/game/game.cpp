#include<bits/stdc++.h>
using namespace std;
int l,r;
string s;
void solve(int l,int r)
{
	if(s[l]=='W'&&s[r]=='b'){
		puts("yes");
		return;
	}
	if(s[l]=='b'&&s[r]=='W'){
		puts("yes");
		return;
	}
	if(s[l]=='b'&&s[r]=='b'){
		puts("yes");
		return;
	}
	if(s[l]=='w'&&s[r]=='w'){
		puts("no");
		return;
	}
	if(s[l]=='W'&&s[r]=='w'||s[l]=='w'&&s[r]=='W'){
		puts("no");
		return;
	}
	solve(l+1,r-1);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	int n;
	scanf("%d",&n);
	while(n--){
		cin>>s;
		l=0;r=s.length()-1;
		solve(l,r);
	}
	return 0;
}

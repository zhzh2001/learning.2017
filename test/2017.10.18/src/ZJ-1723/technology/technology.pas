program ec12;
var 
   n,m,i,j,ans,p:longint;
   s,s1,s2:ansistring;
   c:char;
   tot:array[1..200001] of longint;
   a:array['a'..'z'] of boolean;
   head,last:array['a'..'z'] of longint;
   next:array[1..200001] of longint;
begin 
    assign(input,'technology.in'); reset(input);
	assign(output,'technology.out'); rewrite(output);
    readln(n,m);
	readln(s);
	readln(s1);
	ans:=0;
	for i:=1 to n-m+1 do
	begin 
	    fillchar(a,sizeof(a),true);
	    s2:=copy(s,i,m);
		for c:='a' to 'z' do 
		head[c]:=-1;
		for j:=1 to m do
		begin 
		    if head[s2[j]]=-1 then 
			begin 
			    head[s2[j]]:=j;
				next[j]:=-1;
				last[s2[j]]:=j;
			end
			else
			begin 
			    next[last[s2[j]]]:=j;
				last[s2[j]]:=j;
				next[j]:=-1;
			end;
		end;
		for j:=1 to m do 
		begin 
		    if s2[j]<>s1[j] then 
			begin 
			    if a[s2[j]] and a[s1[j]] then 
				begin
					a[s1[j]]:=false;
					a[s2[j]]:=false; 
				    p:=next[head[s2[j]]];
					while p>0 do 
					begin 
					    s2[p]:=s1[j];
						p:=next[p];
					end;
					p:=head[s1[j]];
					while p>0 do 
				    begin 
					    s2[p]:=s2[j];
						p:=next[p];
					end;
					s2[j]:=s1[j];
				end
				else
				break;
			end
			else
			a[s2[j]]:=false;
			if s2=s1 then 
			break;
		end;
		if s2=s1 then 
		begin 
		    inc(ans);
			tot[ans]:=i;
		end;
	end;
	writeln(ans);
	for i:=1 to ans do
	begin 
	    if i<ans then 
		write(tot[i],' ')
		else
		writeln(tot[i]);
	end;
	close(input);
	close(output);
end. 
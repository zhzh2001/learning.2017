program ec12;
var 
   n,m,i,j,k,ans:longint;
   s,s1,s2:ansistring;
   tot:array[1..200001] of longint;
   a:array['a'..'z'] of boolean;
begin 
    assign(input,'technology.in'); reset(input);
	assign(output,'technology1.out'); rewrite(output);
    readln(n,m);
	readln(s);
	readln(s1);
	ans:=0;
	for i:=1 to n-m+1 do
	begin 
	    fillchar(a,sizeof(a),true);
	    s2:=copy(s,i,m);
	    for j:=1 to m do 
		begin 
		    if s2[j]<>s1[j] then 
			begin 
			    if a[s2[j]] and a[s1[j]] then 
				begin
					a[s1[j]]:=false;
					a[s2[j]]:=false; 
				    for k:=j+1 to m do 
					begin 
					    if s2[k]=s1[j] then 
						s2[k]:=s2[j]
						else
						begin 
							if s2[k]=s2[j] then 
							s2[k]:=s1[j];
						end;
					end;
					s2[j]:=s1[j];
				end
				else
				break;
			end
			else
			a[s2[j]]:=false;
			if s2=s1 then 
			break;
		end;
		if s2=s1 then 
		begin 
		    inc(ans);
			tot[ans]:=i;
		end;
	end;
	writeln(ans);
	for i:=1 to ans do
	begin 
	    if i<ans then 
		write(tot[i],' ')
		else
		writeln(tot[i]);
	end;
	close(input);
	close(output);
end. 
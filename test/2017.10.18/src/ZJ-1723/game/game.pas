program ec12;
var
  s:string;
  n,m,i,j,t:longint;
function dfs(s:string; step,site,tot:longint):boolean;
var 
  o:longint;
  s3:string;
begin
  if step=1 then 
  begin 
	for o:=1 to length(s) do
	begin 
	  inc(tot);
	  if s[o]='b' then 
	  break;
	end;
	if s[o]<>'b' then 
	exit(false);
	for o:=1 to length(s) do 
	begin 
	  inc(tot);
	  if s[o]='b' then 
	  begin 
		if o<site then 
		begin 
		  s3:=s;
		  delete(s3,o+1,site-o);
		end
		else
		begin 
		  s3:=s;
		  delete(s3,site+1,o-site);
		end;
		if dfs(s3,2,o,tot) then
		exit(true);
	  end;
	end;
	exit(false);
  end
  else
  begin 
	for o:=1 to length(s) do
	begin 
	  inc(tot);
	  if s[o]='w' then 
	  break;
	end;
	if s[o]<>'w' then 
	exit(true);
	for o:=1 to length(s) do 
	begin 
	  inc(tot);
	  if s[o]='w' then
	  begin 
	    if o<site then 
	    begin 
		  s3:=s;
		  delete(s3,o,site-o);
	    end
		else
		begin 
		  s3:=s;
		  delete(s3,site,o-site);
		end;
		if dfs(s3,1,o,tot) then
		exit(true);
	  end;
	end;
	exit(false);
  end;
end;
begin
  assign(input,'game.in'); reset(input);
  assign(output,'game.out'); rewrite(output);
  readln(t);
  for i:=1 to t do
  begin 
    readln(s);
	for j:=1 to length(s) do 
	begin 
	  if s[j]='W' then 
	  break;
	end;
	if dfs(s,1,j,0) then 
	writeln('yes')
	else
	writeln('no');
  end;
  close(input);
  close(output);
end. 
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("base.in");
ofstream fout("base.out");
const int INF=0x3f3f3f3f;
int x[4],y[4],nx[4],ny[4],ax[4],ay[4];
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		for(int i=0;i<4;i++)
			fin>>x[i]>>y[i];
		int ans=INF;
		for(int x0=0;x0<4;x0++)
			for(int y0=0;y0<4;y0++)
				for(int x1=0;x1<4;x1++)
					for(int y1=0;y1<4;y1++)
						for(int x2=0;x2<4;x2++)
							for(int y2=0;y2<4;y2++)
								for(int x3=0;x3<4;x3++)
									for(int y3=0;y3<4;y3++)
									{
										nx[0]=x[x0];ny[0]=y[y0];nx[1]=x[x1];ny[1]=y[y1];nx[2]=x[x2];ny[2]=y[y2];nx[3]=x[x3];ny[3]=y[y3];
										if((nx[0]==x[0]||ny[0]==y[0])&&(nx[1]==x[1]||ny[1]==y[1])&&(nx[2]==x[2]||ny[2]==y[2])&&(nx[3]==x[3]||ny[3]==y[3]))
										{
											int dist=-1,cnt=0;
											bool flag=true;
											for(int i=0;i<4;i++)
												for(int j=i+1;j<4;j++)
													if(nx[i]==nx[j])
													{
														cnt++;
														if(~dist)
															flag&=(abs(ny[i]-ny[j])==dist);
														else
															dist=abs(ny[i]-ny[j]);
													}
													else if(ny[i]==ny[j])
													{
														cnt++;
														if(~dist)
															flag&=(abs(nx[i]-nx[j])==dist);
														else
															dist=abs(nx[i]-nx[j]);
													}
											if(flag&&cnt==4)
											{
												dist=0;
												for(int i=0;i<4;i++)
													dist=max(dist,max(abs(nx[i]-x[i]),abs(ny[i]-y[i])));
												if(dist<ans)
												{
													ans=dist;
													for(int i=0;i<4;i++)
													{
														ax[i]=nx[i];
														ay[i]=ny[i];
													}
												}
											}
										}
									}
		if(ans==INF)
			fout<<-1<<endl;
		else
		{
			fout<<ans<<endl;
			for(int i=0;i<4;i++)
				fout<<ax[i]<<' '<<ay[i]<<endl;
		}
	}
	return 0;
}

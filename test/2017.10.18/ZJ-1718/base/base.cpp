#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("base.in");
ofstream fout("base.out");
const int INF=0x3f3f3f3f;
int x[4],y[4],ax[4],ay[4],ans;
void check(int x0,int y0,int x1,int y1,int x2,int y2,int x3,int y3)
{
	int p[4]={0,1,2,3};
	do
		if((x[p[0]]==x0||y[p[0]]==y0)&&(x[p[1]]==x1||y[p[1]]==y1)&&(x[p[2]]==x2||y[p[2]]==y2)&&(x[p[3]]==x3||y[p[3]]==y3))
		{
			int dist=max(max(max(abs(x[p[0]]-x0),abs(y[p[0]]-y0)),max(abs(x[p[1]]-x1),abs(y[p[1]]-y1))),max(max(abs(x[p[2]]-x2),abs(y[p[2]]-y2)),max(abs(x[p[3]]-x3),abs(y[p[3]]-y3))));
			if(dist<ans)
			{
				ans=dist;
				ax[p[0]]=x0;ay[p[0]]=y0;ax[p[1]]=x1;ay[p[1]]=y1;ax[p[2]]=x2;ay[p[2]]=y2;ax[p[3]]=x3;ay[p[3]]=y3;
			}
		}
	while(next_permutation(p,p+4));
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		for(int i=0;i<4;i++)
			fin>>x[i]>>y[i];
		ans=INF;
		int p[4]={0,1,2,3};
		do
		{
			if(x[p[0]]==x[p[1]]&&x[p[2]]==x[p[3]])
			{
				int len=abs(x[p[2]]-x[p[0]]);
				if(y[p[0]]>y[p[1]])
					swap(y[p[0]],y[p[1]]);
				if(y[p[2]]>y[p[3]])
					swap(y[p[2]],y[p[3]]);
				check(x[p[0]],(y[p[0]]+y[p[2]])/2,x[p[1]],(y[p[0]]+y[p[2]])/2+len,x[p[2]],(y[p[0]]+y[p[2]])/2,x[p[3]],(y[p[0]]+y[p[2]])/2+len);
				check(x[p[0]],(y[p[0]]+y[p[2]])/2,x[p[1]],(y[p[0]]+y[p[2]])/2-len,x[p[2]],(y[p[0]]+y[p[2]])/2,x[p[3]],(y[p[0]]+y[p[2]])/2-len);
				check(x[p[0]],(y[p[1]]+y[p[3]])/2,x[p[1]],(y[p[1]]+y[p[3]])/2+len,x[p[2]],(y[p[1]]+y[p[3]])/2,x[p[3]],(y[p[1]]+y[p[3]])/2+len);
				check(x[p[0]],(y[p[1]]+y[p[3]])/2,x[p[1]],(y[p[1]]+y[p[3]])/2-len,x[p[2]],(y[p[1]]+y[p[3]])/2,x[p[3]],(y[p[1]]+y[p[3]])/2-len);
			}
			if(y[p[0]]==y[p[1]]&&y[p[2]]==y[p[3]])
			{
				int len=abs(y[p[2]]-y[p[0]]);
				if(x[p[0]]>x[p[1]])
					swap(x[p[0]],x[p[1]]);
				if(x[p[2]]>x[p[3]])
					swap(x[p[2]],x[p[3]]);
				check((x[p[0]]+x[p[2]])/2,y[p[0]],(x[p[0]]+x[p[2]])/2+len,y[p[1]],(x[p[0]]+x[p[2]])/2,y[p[2]],(x[p[0]]+x[p[2]])/2+len,y[p[3]]);
				check((x[p[0]]+x[p[2]])/2,y[p[0]],(x[p[0]]+x[p[2]])/2-len,y[p[1]],(x[p[0]]+x[p[2]])/2,y[p[2]],(x[p[0]]+x[p[2]])/2-len,y[p[3]]);
				check((x[p[1]]+x[p[3]])/2,y[p[0]],(x[p[1]]+x[p[3]])/2+len,y[p[1]],(x[p[1]]+x[p[3]])/2,y[p[2]],(x[p[1]]+x[p[3]])/2+len,y[p[3]]);
				check((x[p[1]]+x[p[3]])/2,y[p[0]],(x[p[1]]+x[p[3]])/2-len,y[p[1]],(x[p[1]]+x[p[3]])/2,y[p[2]],(x[p[1]]+x[p[3]])/2-len,y[p[3]]);
			}
		}while(next_permutation(p,p+4));
		for(int x0=0;x0<4;x0++)
			for(int y0=0;y0<4;y0++)
				for(int x1=0;x1<4;x1++)
					for(int y1=0;y1<4;y1++)
						if((x[x0]==x[x1])^(y[y0]==y[y1]))
						{
							if(x[x0]==x[x1])
							{
								int len=abs(y[y0]-y[y1]);
								check(x[x0],y[y0],x[x1],y[y1],x[x0]-len,y[y0],x[x0]-len,y[y1]);
								check(x[x0],y[y0],x[x1],y[y1],x[x0]+len,y[y0],x[x0]+len,y[y1]);
							}
							else
							{
								int len=abs(x[x0]-x[x1]);
								check(x[x0],y[y0],x[x1],y[y1],x[x0],y[y0]-len,x[x1],y[y0]-len);
								check(x[x0],y[y0],x[x1],y[y1],x[x0],y[y0]+len,x[x1],y[y0]+len);
							}
						}
		if(ans==INF)
			fout<<-1<<endl;
		else
		{
			fout<<ans<<endl;
			for(int i=0;i<4;i++)
				fout<<ax[i]<<' '<<ay[i]<<endl;
		}
	}
	return 0;
}

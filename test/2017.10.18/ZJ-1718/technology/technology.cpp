#include<fstream>
#include<string>
#include<algorithm>
#include<utility>
using namespace std;
ifstream fin("technology.in");
ofstream fout("technology.out");
const int N=200005,B=29;
typedef unsigned long long hash_t;
hash_t h[N][26];
pair<hash_t,int> hs[26],ht[26];
int M[26];
bool ans[N];
int main()
{
	int n,m;
	fin>>n>>m;
	string s,t;
	fin>>s>>t;
	s=' '+s;t=' '+t;
	int alpha=0;
	for(int i=1;i<=n;i++)
		alpha=max(alpha,s[i]-'a');
	for(int i=1;i<=m;i++)
		alpha=max(alpha,t[i]-'a');
	for(int i=1;i<=n;i++)
	{
		for(int j=0;j<=alpha;j++)
			h[i][j]=h[i-1][j]*B;
		h[i][s[i]-'a']++;
	}
	for(int i=0;i<=alpha;i++)
		ht[i].second=i;
	hash_t p=1;
	for(int i=1;i<=m;i++,p*=B)
		for(int j=0;j<=alpha;j++)
			if(t[i]-'a'==j)
				ht[j].first=ht[j].first*B+1;
			else
				ht[j].first=ht[j].first*B;
	sort(ht,ht+alpha+1);
	for(int i=m;i<=n;i++)
	{
		for(int j=0;j<=alpha;j++)
		{
			hs[j]=make_pair(h[i][j]-h[i-m][j]*p,j);
			M[j]=-1;
		}
		sort(hs,hs+alpha+1);
		ans[i-m+1]=true;
		for(int j=0;j<=alpha;j++)
			if(hs[j].first!=ht[j].first)
			{
				ans[i-m+1]=false;
				break;
			}
			else if(hs[j].first&&hs[j].second!=ht[j].second)
			{
				if(~M[hs[j].second]&&M[hs[j].second]!=ht[j].second)
				{
					ans[i-m+1]=false;
					break;
				}
				if(M[hs[j].second]==-1)
					M[ht[j].second]=hs[j].second;
			}
	}
	fout<<count(ans+1,ans+n+1,true)<<endl;
	for(int i=1;i<=n;i++)
		if(ans[i])
			fout<<i<<' ';
	//fout<<endl;
	return 0;
}

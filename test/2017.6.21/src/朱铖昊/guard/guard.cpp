#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int inf=1e9+7;
inline void read(int &x)
{
	int f=1;
	x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9')
	{
		if (ch=='-')
			f=-1;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	x*=f;
}
int n,l,m;
double f[205][205][205],ans;
struct data
{
	double p;
	int v;
}a[205];
inline bool cmp(data a,data b)
{
	return a.v>b.v;
}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	read(n);
	read(l);
	read(m);
	if (m>n)
		m=n;
	for (int i=1;i<=n;i++)
	{
		scanf("%lf",&a[i].p);
		a[i].p/=100;
	}
	for (int i=1;i<=n;i++)
		read(a[i].v);
	sort(a+1,a+n+1,cmp);
	f[0][0][m]=1;
	for (int i=0;i<n;++i)
		for (int k=0;k<=i;++k)
			for (int j=0;j<=n;++j)
			{
				f[i+1][k][j]+=f[i][k][j]*(1-a[i+1].p);
				int t=j+a[i+1].v;
				if (t<0)continue;
				t=min(t,n);
				f[i+1][k+1][t]+=f[i][k][j]*a[i+1].p;
			}
	for (int i=0;i<=n;i++)
		for (int j=l;j<=n;j++)
			ans+=f[n][j][i];
	printf("%.6lf",ans);
	return 0;
}
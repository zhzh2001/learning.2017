#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define double long double
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const int N=100005,M=50;
ll nn,ans1,ans2,ans3;
int a[N],n,f1[N],f2[N],f3[N],b1[M],b2[M],b3[M];
inline void fz(int l,int r)
{
	if (l==r)
	{
		ans1+=a[l];
		ans2+=a[l];
		ans3+=a[l];
		return;
	}
	int mid=(l+r)/2;
	f1[mid]=f2[mid]=f3[mid]=a[mid];
	f1[mid+1]=f2[mid+1]=f3[mid+1]=a[mid+1];
	for (int i=mid-1;i>=l;--i)
	{
		f1[i]=f1[i+1]^a[i];
		f2[i]=f2[i+1]&a[i];
		f3[i]=f3[i+1]|a[i];
	}
	for (int i=0;i<=31;++i)
	{
		b1[i]=0;
		b2[i]=0;
		b3[i]=0;
	}
	for (int j=0;j<=31;++j)
	{
		int kkk=1<<j;
		if (f1[mid+1]&kkk)
			++b1[j];
		if (f2[mid+1]&kkk)
			++b2[j];
		if (f3[mid+1]&kkk)
			++b3[j];
	}
	for (int i=mid+2;i<=r;++i)
	{
		f1[i]=f1[i-1]^a[i];
		f2[i]=f2[i-1]&a[i];
		f3[i]=f3[i-1]|a[i];
		for (int j=0;j<=31;++j)
		{
			int kkk=1<<j;
			if (f1[i]&kkk)
				++b1[j];
			if (f2[i]&kkk)
				++b2[j];
			if (f3[i]&kkk)
				++b3[j];
		}
	}
	for (int i=l;i<=mid;++i)
		for (int j=0;j<=31;++j)
		{
			int kkk=1<<j;
			if (f1[i]&kkk)
				ans1+=(ll)2*kkk*(r-mid-b1[j]);
			else
				ans1+=(ll)2*kkk*b1[j];
			if (f2[i]&kkk)
				ans2+=(ll)2*kkk*b1[j];
			if (f3[i]&kkk)
				ans3+=(ll)2*kkk*(r-mid);
			else
				ans3+=(ll)2*kkk*b1[j];
		}
	fz(l,mid);
	fz(mid+1,r);
}
int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal_1.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);	
	fz(1,n);
	nn=(ll)n*n;
	printf("%.3Lf %.3Lf %.3Lf",(double)ans1/nn,(double)ans2/nn,(double)ans3/nn);
}
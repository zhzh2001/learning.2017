#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int n,m,t,zyy;
int a[100010],b[100010];
int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read();m=read();t=read();
	zyy=0;
	if(t%n==0)
	{
		if(t%m==0)
		{
			cout<<"both ";
			zyy=1;
		}
		else
		{
			cout<<"row ";
			zyy=2;
		}
	}
	else
	{
		if(t%m==0)
		{
			cout<<"column ";
			zyy=3;
		}
		else
		{
			cout<<"impossible";
			return 0;
		}
	}
	For(i,1,t)
	{
		int x=read(),y=read();
		a[x]++;b[y]++;
	}
	if(zyy==2)
	{
		ll ans=0;
		For(i,1,n)a[i]+=a[i-1];
	    int pj=a[n]/n;
		For(i,1,n)a[i]-=i*pj;
	    sort(a+1,a+n+1);
	    int k=a[(1+n)/2];
		For(i,1,n)ans+=abs(a[i]-k);
	    cout<<ans<<endl;
	    return 0;
	}
	if(zyy==3)
	{
		ll ans=0;
		For(i,1,m)b[i]+=b[i-1];
	    int pj=b[m]/m;
		For(i,1,m)b[i]-=i*pj;
	    sort(b+1,b+m+1);
	    int k=b[(1+m)/2];
		For(i,1,m)ans+=abs(b[i]-k);
	    cout<<ans<<endl;
	    return 0;
	}
	if(zyy==1)
	{
		ll ans=0;
		For(i,1,n)a[i]+=a[i-1];
	    int pj=a[n]/n;
		For(i,1,n)a[i]-=i*pj;
	    sort(a+1,a+n+1);
	    int k=a[(1+n)/2];
		For(i,1,n)ans+=abs(a[i]-k);
	    For(i,1,m)b[i]+=b[i-1];
	    pj=b[m]/m;
		For(i,1,m)b[i]-=i*pj;
	    sort(b+1,b+m+1);
	    k=b[(1+m)/2];
		For(i,1,m)ans+=abs(b[i]-k);
	    cout<<ans<<endl;
	}
	return 0;
}


//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("tanabata.in");
ofstream fout("tanabata.out");
int n,m,t;
int x[100003],y[100003];
int c1[100003],c2[100003];
int xx[100003],yy[100003];
inline int abs(int x){
	if(x<0){
		return -x;
	}else{
		return x;
	}
}
inline int solvex(){
	int ans=0,tot=0,hehe=0;
	for(int i=1;i<=n;i++){
		tot+=x[i];
	}
	tot/=n;
	for(int i=2;i<=n;i++){
    	c1[i]=c1[i-1]+(x[i-1]-tot);
	}
	sort(c1+1,c1+n+1);
    hehe=c1[(n+1)/2];
    for(int i=1;i<=n;i++){
    	ans+=abs(hehe-c1[i]);
	}
	return ans;
}
inline int solvey(){
	int ans=0,tot=0,hehe=0;
	for(int i=1;i<=m;i++){
		tot+=y[i];
	}
	tot/=m;
	for(int i=2;i<=m;i++){
    	c2[i]=c2[i-1]+(y[i-1]-tot);
	}
	sort(c2+1,c2+m+1);
    hehe=c2[(m+1)/2];
    for(int i=1;i<=m;i++){
    	ans+=abs(hehe-c2[i]);
	}
	return ans;
}
int main(){
	//freopen("tanabata.in","r",stdin);
	//freopen("tanabata.out","w",stdout);
	fin>>n>>m>>t;
	for(int i=1;i<=t;i++){
		fin>>xx[i]>>yy[i];
		x[xx[i]]++;
		y[yy[i]]++;
	}
	if(t%n==0&&t%m==0){
		fout<<"both "<<(solvex()+solvey())<<endl;
	}else if(t%n==0){
		fout<<"row "<<solvex()<<endl;
	}else if(t%m==0){
		fout<<"column "<<solvey()<<endl;
	}else{
		fout<<"impossible"<<endl;
	}
	return 0;
}
/*

in1:
2 3 4
1 3
2 1
2 2
2 3

out1:
row 1

in2:
3 3 3
1 3
2 2
2 3

out2:
both 2

*/

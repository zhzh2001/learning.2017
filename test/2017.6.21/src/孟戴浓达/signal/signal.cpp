//#include<iostream>
#include<fstream>
#include<algorithm>
#include<iomanip>
#include<cmath>
using namespace std;
ifstream fin("signal.in");
ofstream fout("signal.out");
int n;
int num[100003];
int sumor[1003][1003],sumxor[1003][1003],sumand[1003][1003];
int shu[100003][33];
long long bin[33];
double ansand,ansxor,ansor;
int sum1[33],sum0[33];
inline void zhuan(int x){
	int now=0;
	int xx=num[x];
	while(xx!=0){
		shu[x][now]=xx%2;
		xx/=2;
		now++;
	}
}
inline void solve(){
	for(int i=1;i<=n;i++){
		sumor[i][i]=num[i]=sumxor[i][i]=num[i]=sumand[i][i]=num[i];
		for(int j=i+1;j<=n;j++){
			sumor[i][j]=sumor[i][j-1]|num[j],sumxor[i][j]=sumxor[i][j-1]^num[j],sumand[i][j]=sumand[i][j-1]&num[j];
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			int x=min(i,j),y=max(i,j);
			ansand+=sumand[x][y],ansor+=sumor[x][y],ansxor+=sumxor[x][y];
		}
	}
	fout<<fixed<<setprecision(3)<<(ansxor/(1.0*n*n))<<" "<<(ansand/(1.0*n*n))<<" "<<(ansor/(1.0*n*n))<<endl;
}
inline void solve2(){
	ansor=0,ansxor=0,ansand=0;
	for(int i=1;i<=n;i++){
		for(int j=0;j<=30;j++){
			if(shu[i][j]==1){
				ansor+=i*bin[j];
				ansxor+=sum0[j]*bin[j];
				ansand+=sum1[j]*bin[j];
				sum1[j]++;
			}else{
				ansor+=sum1[j]*bin[j];
				ansxor+=sum1[j]*bin[j];
				ansand+=0;
				sum0[j]++;
			}
		}
	}
	ansor*=2;
	ansand*=2;
	ansxor*=2;
	for(int i=1;i<=n;i++){
		ansor+=num[i];
		ansand+=num[i];
		ansxor+=num[i];
	}
	fout<<fixed<<setprecision(3)<<(ansxor/(1.0*n*n))<<" "<<(ansand/(1.0*n*n))<<" "<<(ansor/(1.0*n*n))<<endl;
	/*ansxor=num[1];
	int sumxor=num[1],xorhe=num[1];
	for(int i=2;i<=n;i++){
		xorhe=(xorhe ^ num[i]);
		ansxor+=((sumxor ^ num[i])+num[i]);
		sumxor+=xorhe;
	}
	ansxor=ansxor*2;
	for(int i=1;i<=n;i++){
		ansxor-=num[i];
	}
	ansand=num[1];
	int sumand=num[1],andhe=num[1];
	for(int i=2;i<=n;i++){
		andhe=(andhe & num[i]);
		ansand+=((sumand & num[i])+num[i]);
		sumand+=andhe;
	}
	ansand=ansand*2;
	for(int i=1;i<=n;i++){
		ansand-=num[i];
	}
	ansor=num[1];
	int sumor=num[1],orhe=num[1];
	for(int i=2;i<=n;i++){
		orhe=(orhe | num[i]);
		ansor+=((sumor | num[i])+num[i]);
		sumor+=orhe;
	}
	ansor=ansor*2;
	for(int i=1;i<=n;i++){
		ansor-=num[i];
	}
	*/
}
int main(){
	//freopen("signal.in","r",stdin);
	//freopen("signal.out","w",stdout);
	fin>>n;
	bin[0]=1;
	for(int i=1;i<=30;i++){
		bin[i]=bin[i-1]*2;
	}
	for(int i=1;i<=n;i++){
		fin>>num[i];
		zhuan(i);
	}
	if(n<=1000){
		solve();
		return 0;
	}else{
		solve2();
	}
	return 0;
}
/*

in:
2
5 4

out:
2.750 4.250 4.750

int2:
5
123 3423 2 123 34

out2:


*/

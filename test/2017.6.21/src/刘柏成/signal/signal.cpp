#include <cstdio>
using namespace std;
typedef unsigned long long ll;
int a[100002];
int lst0[32],lst1[32];
int cnt[32],sum[32];
int main(){
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	ll ans1=0,ans2=0,ans3=0;
	for(int i=1;i<=n;i++){
		for(int j=0;j<31;j++)
			if (a[i]&(1<<j))
				lst1[j]=i,sum[j]^=1;
			else lst0[j]=i;
		for(int j=0;j<31;j++){
			if (sum[j])
				ans1+=(((ll)(i-cnt[j]))<<j);
			else ans1+=(((ll)(cnt[j]))<<j);
			ans2+=(((ll)(i-lst0[j]))<<j);
			ans3+=(((ll)lst1[j])<<j);
		}
		for(int j=0;j<31;j++)
			if (sum[j])
				cnt[j]++;
	}
	for(int i=1;i<=n;i++)
		ans1-=a[i],ans2-=a[i],ans3-=a[i];
	ans1*=2,ans2*=2,ans3*=2;
	for(int i=1;i<=n;i++)
		ans1+=a[i],ans2+=a[i],ans3+=a[i];
	long double x=(long double)ans1/n/n,y=(long double)ans2/n/n,z=(long double)ans3/n/n;
	printf("%.3f %.3f %.3f\n",(double)(x+1e-9),(double)(y+1e-9),(double)(z+1e-9));
}
//1000000000
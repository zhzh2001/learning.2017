#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=4005;
const int maxm=20002;
const int INF=0x3f3f3f3f;
struct graph{
	struct edge{
		int from,to,cap,cost,next;
	}e[maxm];
	int n,m;
	void init(int n){
		this->n=n;
		m=1;
	}
	int first[maxn];
	void addedge(int from,int to,int cap,int cost){
		e[++m]=(edge){from,to,cap,cost,first[from]};
		first[from]=m;
		e[++m]=(edge){to,from,0,-cost,first[to]};
		first[to]=m;
	}
	int d[maxn],t,q[maxn],p[maxn];
	bool inq[maxn];
	bool spfa(int s){
		int l=0,r=1;
		q[l]=s;
		for(int i=1;i<=n;i++)
			d[i]=INF,inq[i]=0;
		d[s]=0,inq[s]=1;
		while(l<r){
			int u=q[(l++)%n];
			inq[u]=0;
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				if (e[i].cap && d[u]!=INF && d[v]>d[u]+e[i].cost){
					d[v]=d[u]+e[i].cost;
					p[v]=i;
					if (!inq[v]){
						q[(r++)%n]=v;
						inq[v]=1;
					}
				}
			}
		}
		return d[t]!=INF;
	}
	int mcmf(int s,int t){
		this->t=t;
		int flow=0,cost=0;
		while(spfa(s)){
			int delta=INF;
			for(int i=t;i!=s;i=e[p[i]].from)
				delta=min(delta,e[p[i]].cap);
			for(int i=t;i!=s;i=e[p[i]].from)
				e[p[i]].cap-=delta;
			flow+=delta;
			cost+=delta*d[t];
		}
		return cost;
	}
}g;
int n,m,cnt;
int w[maxn];
bool flag=false;
int wn[maxn],wm[maxn];
void both(){
	puts("0");
}
void row(){
	int qaq=cnt/n;
	while(cnt--){
		int x=readint(),y=readint();
		if (flag)
			w[y]++;
		else w[x]++;
	}
	int s=n+1,t=n+2;
	g.init(t);
	for(int i=1;i<=n;i++)
		g.addedge(s,i,w[i],0);
	for(int i=1;i<=n;i++){
		int nxt=i==n?1:i+1;
		g.addedge(i,nxt,INF,1);
		g.addedge(nxt,i,INF,1);
	}
	for(int i=1;i<=n;i++)
		g.addedge(i,t,qaq,0);
	printf("%d\n",g.mcmf(s,t));
}
int main(){
	init();
	n=readint(),m=readint(),cnt=readint();
	if (cnt%n==0 && cnt%m==0)
		printf("both "),both();
	else if (cnt%n==0)
		printf("row "),row();
	else if (cnt%m==0){
		swap(n,m);
		flag=true;
		printf("column "),row();
	}
	else puts("impossible");
}
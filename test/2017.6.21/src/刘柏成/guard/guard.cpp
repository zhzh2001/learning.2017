#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef double db;
db dp0[201][401],dp1[201][401];
db p[201];
int a[201];
int n,l,w;
inline int f(int x){
	return min(x,n)+200;
} 
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	scanf("%d%d%d",&n,&l,&w);
	for(int i=1;i<=n;i++){
		db x;
		scanf("%lf",&x);
		p[i]=x/100;
	}
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	dp0[0][f(w)]=1;
	for(int i=0;i<n;i++){
		db pl=1-p[i+1];
		memset(dp1,0,sizeof(dp1));
		//printf("%.2f",dp1[0][0]);
		for(int j=0;j<=i;j++){
			for(int k=-i;k<=n;k++){
				double now=dp0[j][f(k)];
				dp1[j+1][f(k+a[i+1])]+=now*p[i+1];
				dp1[j][f(k)]+=now*pl;
			}
		}
		swap(dp0,dp1);
	}
	double ans=0;
	for(int i=l;i<=n;i++)
		for(int j=0;j<=n;j++)
			ans+=dp0[i][f(j)];
	printf("%.6f\n",ans+1e-12);
}
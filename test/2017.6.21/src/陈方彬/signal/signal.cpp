#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e5+5;
int a[N],s[N],v[N];
int n,x,k;
bool ok;
double A,B,C,X,Y,Z;
void find(){
	X=Y=Z=0;
	int last=0;
	a[0]=a[n+1]=0;
	for(int i=1;i<=n;i++){
		if(a[i]&&!a[i-1])last=i;
		if(!a[i+1]&&a[i])X+=double(i-last+1)*(i-last+1);
	}
	a[0]=a[n+1]=1;
	
	for(int i=1;i<=n;i++){
		if(!a[i]&&a[i-1])last=i;
		if(a[i+1]&&!a[i])Y+=double(i-last+1)*(i-last+1);
	}
	Y=n*n-Y;
	
	int o=1,j=0;
	for(int i=2;i<=n;i++){
		if(s[i]&1)Z+=o;else Z+=j;
		if(s[i-1]&1)j++;else o++;
	}
	Z=Z*2;
	for(int i=1;i<=n;i++)if(a[i])Z++;
	A+=X*k/(n*n); B+=Y*k/(n*n); C+=Z*k/(n*n);
}
int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&v[i]);
		if(v[i])ok=1;
	}
	k=1;
	for(;ok;k*=2){
		ok=0;
		for(int i=1;i<=n;i++){
			a[i]=v[i]%2;
			v[i]=v[i]/2;
			if(v[i])ok=1;
			s[i]=s[i-1]+a[i];
		}
		find();
	}
	printf("%.3lf %.3lf %.3lf",C,A,B);
}

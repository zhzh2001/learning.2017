#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
Ll x[N],y[N],a[N],s[N];
Ll n,m,w,A,B;
Ll find(Ll n){
	Ll ans=0,sum=0;
	for(Ll i=1;i<=n;i++)sum+=a[i];
	if(sum%n)return -1;
	sum=sum/n;
	for(Ll i=1;i<=n;i++)s[i]=s[i-1]+a[i]-sum;
	sort(s+1,s+n+1);
	Ll k=s[n/2];
	for(Ll i=1;i<=n;i++)ans+=abs(s[i]-k);
	return ans;
}
int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&w);
	for(Ll i=1;i<=w;i++)scanf("%lld%lld",&x[i],&y[i]);
	for(Ll i=1;i<=w;i++)a[x[i]]++;
	A=find(n);
	memset(a,0,sizeof a);
	for(Ll i=1;i<=w;i++)a[y[i]]++;
	B=find(m);
	if(A==-1&&B==-1)printf("impossible");else
	if(A!=-1&&B!=-1)printf("both %lld",A+B);else
	if(A!=-1)printf("row %lld",A);else printf("column %lld",B);	
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,a[10005],p1,p2,p3;
long long ans1,ans2,ans3;
int main(){
	freopen("signal.in","r",stdin);
	freopen("std.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++){
		ans1+=a[i]; ans2+=a[i]; ans3+=a[i];
		p1=p2=p3=a[i];
		for (int j=i+1;j<=n;j++){
			p1^=a[j]; p2&=a[j]; p3|=a[j];
			ans1+=2*p1; ans2+=2*p2; ans3+=2*p3;
		}
	}
	printf("%.3lf %.3lf %.3lf",1.0*ans1/n/n,1.0*ans2/n/n,1.0*ans3/n/n);
}

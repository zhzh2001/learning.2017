#include<cstdio>
using namespace std;
const int maxn=100005;
int n,a[maxn],res;
long long sum;
inline int read(){int x=0; char ch=getchar(); while(ch<'0'||ch>'9') ch=getchar(); while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x;}
int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n;i++){
		res=a[i]; sum+=res; 
		for (int j=i+1;j<=n;j++) res^=a[j],sum+=2*res;
	}
	printf("%.3f ",(double)sum/(n*n));
	sum=0;
	for (int i=1;i<=n;i++){
		res=a[i]; sum+=res; 
		for (int j=i+1;j<=n;j++) res&=a[j],sum+=2*res;
	}
	printf("%.3f ",(double)sum/(n*n));
	sum=0;
	for (int i=1;i<=n;i++){
		res=a[i]; sum+=res; 
		for (int j=i+1;j<=n;j++) res|=a[j],sum+=2*res;
	}
	printf("%.3f\n",(double)sum/(n*n));
	return 0;
}

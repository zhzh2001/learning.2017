#include<bits/stdc++.h>
#define N 200020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n;
int a[N];
long long A,B,C,all;
int main()
{
	freopen("signal.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)
	{
		int aa=0,b=1,c=0;
		for(int j=i;j<=n;j++)
		{
			aa=0,b=-1,c=0;
			for(int k=i;k<=j;k++)
				aa^=a[k],b=(b==-1?a[k]:(b&a[k])),c|=a[k];
			A+=aa,B+=b,C+=c;
			if(i!=j)A+=aa,B+=b,C+=c;
		}
	}
	for(int i=1;i<=n;i++)all+=(n-i)*2+1;
	printf("%.3lf %.3lf %.3lf\n",(double)A/all,(double)B/all,(double)C/all);
}

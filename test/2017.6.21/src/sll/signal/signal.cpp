#include<bits/stdc++.h>
#define N 200020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n;
int C[N],a[N],bt[32][N];
/*void add(int x,int c)
{for(int i=x;i<=n;i+=lowbit(i))C[i]+=c;}
int ask(int x)
{
	int ret=0;
	for(int i=x;i;i-=lowbit(i))
		ret+=C[i];return ret;
}*/
int s[N];long long all;
void solve1()
{	
	int a=0,b=0;long long ans=0;
	for(int k=0,j=1;k<=30;j<<=1,k++)
	{
		a=b=0;int ret=0;
		for(int i=1;i<=n;i++)
			s[i]=s[i-1]^bt[k][i];
		for(int i=n;i;i--)
		{
			if(s[i-1])ret+=b;else ret+=a;
			if(s[i])a++;else b++;
			if(s[i-1])ret+=b;else ret+=a;
		}
		ans+=(long long)ret*j;
	}
	printf("%.3lf ",(double)ans/all);
}
void solve2()
{
	long long ans=0;int a;
	for(int k=0,j=1;k<=30;j<<=1,k++)
	{
		a=0;int ret=0;
		for(int i=n;i;i--)if(bt[k][i]){
			ret+=a;a++;ret+=a;
		}else a=0;
		ans+=(long long)ret*j;
	}
	printf("%.3lf ",(double)ans/all);
}
void solve3()
{
	long long ans=0;int a;
	for(int k=0,j=1;k<=30;j<<=1,k++)
	{
		a=0;int ret=0;
		for(int i=n;i;i--){
			if(bt[k][i])a=0;else a++;
			if(!a)ret+=(n-i)+(n-i+1);
			else ret+=(n-i+1-a)*2;
		}
		ans+=(long long)ret*j;
	}
	printf("%.3lf ",(double)ans/all);
}
int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=0,j=1;i<=30;i++,j<<=1)
		for(int k=1;k<=n;k++)bt[i][k]=((a[k]&j)>>i);
	for(int i=1;i<=n;i++)all+=(n-i)*2+1;
	solve1();solve2();solve3();
}

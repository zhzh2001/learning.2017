#include<bits/stdc++.h>
#define N 200020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,T,ans;
int col[N],row[N];
void solve(int *a,int n)
{
	int A=0,B=0;
	for(int i=1;i<=n;i++)
		if(a[i]<0)A+=(-a[i]);
		else B+=a[i];
	ans+=B;
}
int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read(),m=read(),T=read();
	for(int i=1;i<=T;i++)
	{
		int x=read(),y=read();
		col[y]++,row[x]++;
	}
	if(T%n==0&&T%m==0)printf("both ");
	else if(T%n==0)printf("row ");
	else if(T%m==0)printf("column ");
	else {printf("impossible\n");return 0;}
	for(int i=1;i<=n;i++)row[i]-=(T/n);
	for(int i=1;i<=m;i++)col[i]-=(T/m);
	if(T%n==0)solve(row,n);
	if(T%m==0)solve(col,m);
	printf("%d\n",ans);
}

#include<bits/stdc++.h>
#define sosu long double
#define eps 1e-9
#define N 202
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int a[N];
int n,m,k;
sosu f[N][N][N<<1],p[N],ans;
void dfs(int u,int now,int num,sosu pi)
{
	if(now+(n-u+1)<m)return;
	if(u==n+1)
	{
		if(now>=m&&num>=0)ans+=pi;
		return;
	}
	dfs(u+1,now+1,num+a[u],pi*p[u]);
	dfs(u+1,now,num,pi*(1.0-p[u]));
}
int main()
{
	freopen("guard.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read(),m=read(),k=read();k=min(k,n);
	for(int i=1,x;i<=n;i++)x=read(),p[i]=(sosu)x/100.0;
	for(int i=1;i<=n;i++)a[i]=read();
	dfs(1,0,k,1);
	printf("%.6lf\n",(double)ans);
}

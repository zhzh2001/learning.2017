#include<bits/stdc++.h>
#define sosu long double
#define N 202
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int a[N];
int n,m,k;
sosu f[2][N][N<<1],p[N],ans;
int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(),m=read(),k=read();k=min(k,n);
	for(int i=1,x;i<=n;i++)x=read(),p[i]=(sosu)x/100.0;
	for(int i=1;i<=n;i++)a[i]=read();
	f[0][0][min(2*n,k+n)]=1;
	for(int i=0;i<n;i++)
	{
		int t=(i&1);
		for(int j=0;j<=i;j++)
			for(int k=0;k<=n*2;k++)
			{
				f[t^1][j][k]+=f[t][j][k]*(1.0-p[i+1]);
				f[t^1][j+1][min(k+a[i+1],2*n)]+=f[t][j][k]*p[i+1];
			}
		for(int j=0;j<=n;j++)for(int k=0;k<=n*2;k++)f[t][j][k]=0;
	}
	for(int j=m;j<=n;j++)
		for(int k=n;k<=2*n;k++)ans+=f[n&1][j][k];
	printf("%.6lf\n",(double)ans);
}

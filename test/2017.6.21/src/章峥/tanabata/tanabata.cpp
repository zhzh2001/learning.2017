#include<fstream>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("tanabata.in");
ofstream fout("tanabata.out");
const int N=100005;
int row[N],col[N];
long long calc(int a[],int n,int avg)
{
	for(int i=1;i<=n;i++)
		a[i]=a[i-1]+avg-a[i];
	nth_element(a+1,a+(n+1)/2,a+n+1);
	long long ans=0;
	for(int i=1;i<=n;i++)
		ans+=abs(a[i]-a[(n+1)/2]);
	return ans;
}
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	bool adjr=k%n==0,adjc=k%m==0;
	if(k%n)
		if(k%m)
		{
			fout<<"impossible\n";
			return 0;
		}
		else
			fout<<"column ";
	else
		if(k%m)
			fout<<"row ";
		else
			fout<<"both ";
	for(int i=1;i<=k;i++)
	{
		int x,y;
		fin>>x>>y;
		row[x]++;
		col[y]++;
	}
	long long ans=0;
	if(adjr)
		ans+=calc(row,n,k/n);
	if(adjc)
		ans+=calc(col,m,k/m);
	fout<<ans<<endl;
	return 0;
}
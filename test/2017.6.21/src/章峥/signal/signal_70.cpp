#include<fstream>
using namespace std;
ifstream fin("signal.in");
ofstream fout("signal.out");
const int N=100005;
int a[N];
int main()
{
	int n;
	fin>>n;
	long long sum=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		sum+=a[i];
	}
	long long ans1=0,ans2=0,ans3=0;
	if(n<=5000)
	{
		for(int i=1;i<=n;i++)
		{
			int now1=a[i],now2=a[i],now3=a[i];
			for(int j=i+1;j<=n;j++)
			{
				now1^=a[j];now2&=a[j];now3|=a[j];
				ans1+=now1;ans2+=now2;ans3+=now3;
			}
		}
		ans1=ans1*2+sum;
		ans2=ans2*2+sum;
		ans3=ans3*2+sum;
	}
	else
	{
		ans1=a[1];ans2=a[1];ans3=a[1];
		int now1=a[1],now2=a[1],now3=a[1],cnt=a[1],t[2]={0,a[1]},pred1=a[1]?1:-1;
		for(int i=2;i<=n;i++)
		{
			cnt+=a[i];
			if(a[i])
			{
				t[cnt&1]=now1=(~pred1?i-pred1:0)+t[cnt&1];
				now2++;
				now3=i;
				pred1=i;
			}
			else
				now2=0;
			ans1+=now1;ans2+=now2;ans3+=now3;
		}
		ans1=ans1*2-sum;
		ans2=ans2*2-sum;
		ans3=ans3*2-sum;
	}
	long long cnt=(long long)n*n;
	fout.precision(3);
	fout<<fixed<<(double)ans1/cnt<<' '<<(double)ans2/cnt<<' '<<(double)ans3/cnt<<endl;
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("signal.in");
ofstream fout("signal.out");
const int N=100005;
int a[N];
int main()
{
	int n;
	fin>>n;
	long long Ans1=0,Ans2=0,Ans3=0;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int j=0;j<=30;j++)
	{
		long long ans1=(bool)(a[1]&(1<<j)),ans2=(bool)(a[1]&(1<<j)),ans3=(bool)(a[1]&(1<<j));
		int now1=(bool)(a[1]&(1<<j)),now2=(bool)(a[1]&(1<<j)),now3=(bool)(a[1]&(1<<j)),cnt=(bool)(a[1]&(1<<j)),t[2]={0,(bool)(a[1]&(1<<j))},pred1=a[1]&(1<<j)?1:0;
		for(int i=2;i<=n;i++)
		{
			cnt+=(bool)(a[i]&(1<<j));
			if(a[i]&(1<<j))
			{
				t[cnt&1]=now1=i-pred1+t[cnt&1];
				now2++;
				now3=i;
				pred1=i;
			}
			else
				now2=0;
			ans1+=now1;ans2+=now2;ans3+=now3;
		}
		Ans1+=(ans1*2-cnt)<<j;
		Ans2+=(ans2*2-cnt)<<j;
		Ans3+=(ans3*2-cnt)<<j;
	}
	long long tot=(long long)n*n;
	fout.precision(3);
	fout<<fixed<<(double)Ans1/tot<<' '<<(double)Ans2/tot<<' '<<(double)Ans3/tot<<endl;
	return 0;
}
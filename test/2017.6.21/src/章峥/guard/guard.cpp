#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("guard.in");
ofstream fout("guard.out");
const int N=205;
int a[N];
double p[N],f[2][N*2][N];
int main()
{
	int n,l,k;
	fin>>n>>l>>k;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i];
		p[i]/=100.0;
	}
	for(int i=1;i<=n;i++)
		fin>>a[i];
	f[0][min(k+200,400)][0]=1.0;
	for(int i=1;i<=n;i++)
		for(int j=a[i]<0?1:0;j<=400;j++)
			for(int t=0;t<=i;t++)
				f[i&1][j][t]=p[i]*f[(i&1)^1][j-a[i]][t-1]+(1.0-p[i])*f[(i&1)^1][j][t];
	double ans=.0;
	for(int i=200;i<=400;i++)
		for(int j=l;j<=n;j++)
			ans+=f[n&1][i][j];
	fout.precision(6);
	fout<<fixed<<ans<<endl;
	return 0;
}
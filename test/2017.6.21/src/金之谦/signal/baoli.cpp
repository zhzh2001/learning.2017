#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int n,a[100001],s[100001],xo[100001];
ll ax,ay,az,xx,yy,zz;
int main()
{
	freopen("signal.in","r",stdin);
	freopen("my.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	ll sum=0;
	for(int i=1;i<=n;i++){
		sum++;ax+=(ll)a[i];ay+=(ll)a[i];az+=(ll)a[i];
		xx=(ll)a[i];yy=(ll)a[i];zz=(ll)a[i];
		for(int j=i+1;j<=n;j++){
			xx^=a[j];yy&=a[j];zz|=a[j];
			sum+=2;ax+=xx*2;ay+=yy*2;az+=zz*2;
		}
	}
	double a1=(double)ax/sum,a2=(double)ay/sum,a3=(double)az/sum;
	printf("%.3lf %.3lf %.3lf",a1,a2,a3);
	return 0;
}
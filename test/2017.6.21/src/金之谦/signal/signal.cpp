#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int n,a[100001],s[100001],xo[100001];
signed e1[100001],e2[100001];
int sum,ax=0,ay=0,az=0,xx=0,yy=0,zz=0,ee1,ee2;
inline void work(int x){
	signed i;bool flag=1;
	s[0]=xo[0]=0;s[1]=xo[1]=((a[1]&(1<<x))>0);ee1=ee2=0;
	for(i=1;i<=n;++i){
		if(i>1)s[i]=s[i-1]^((a[i]&(1<<x))>0);
		if(a[i]&(1<<x))e1[++ee1]=i,flag=0;
		else e2[++ee2]=i;
	}
	if(flag)return;
	for(i=2;i<=n;++i)xo[i]=xo[i-1]+s[i];
	signed p1,p2;
	for(i=1;i<=n;++i){
		if((a[i]&(1<<x)))ax-=(1<<x);
		p1=lower_bound(e1+1,e1+ee1+1,i)-e1;p2=lower_bound(e2+1,e2+ee2+1,i)-e2;
		p1=(p1<=ee1)?e1[p1]:n+1;p2=(p2<=ee2)?e2[p2]:n+1;
		ay+=(int)(p2-i)*(1<<x)*2;az+=(int)(n-p1+1)*(1<<x)*2;
		if(p2>i)ay-=(1<<x);if(p1==i)az-=(1<<x);
		int q=xo[n]-xo[i-1];ax+=s[i-1]?(n-i+1-q)*(1<<x)*2:q*(1<<x)*2;
	}
}
signed main()
{
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	n=read();sum=n*n;signed i;bool flag=1;
	for(i=1;i<=n;++i){a[i]=read();if(a[i]>1)flag=0;}
	if(flag)work(0);
	else for(i=0;i<31;++i)work(i);
	double a1=(double)ax/sum,a2=(double)ay/sum,a3=(double)az/sum;
	printf("%.3lf %.3lf %.3lf",a1,a2,a3);
	return 0;
}
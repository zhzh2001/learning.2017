#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int ans=0,n,m,p,x,y,sx[100001],sy[100001];
inline void hang(){
	int k=p/n;
	for(int i=1;i<=n;i++)if(sx[i]<k){
		for(int k1=i-1,k2=i+1,jl=1;sx[i]<k;k1--,k2++,jl++){
			if(k1==0)k1=n;if(k2==n+1)k2=1;
			if(sx[k1]>k){int jzq=min(sx[k1]-k,k-sx[i]);ans+=jl*jzq;sx[k1]-=jzq;sx[i]+=jzq;}
			if(sx[i]>=k)break;
			if(sx[k2]>k){int jzq=min(sx[k2]-k,k-sx[i]);ans+=jl*jzq;sx[k2]-=jzq;sx[i]+=jzq;}			
		}
	}
}
inline void lie(){
	int k=p/m;
	for(int i=1;i<=m;i++)if(sy[i]<k){
		for(int k1=i-1,k2=i+1,jl=1;sy[i]<k;k1--,k2++,jl++){
			if(k1==0)k1=m;if(k2==m+1)k2=1;
			if(sy[k1]>k){int jzq=min(sy[k1]-k,k-sy[i]);ans+=jl*jzq;sy[k1]-=jzq;sy[i]+=jzq;}
			if(sy[i]>=k)break;
			if(sy[k2]>k){int jzq=min(sy[k2]-k,k-sy[i]);ans+=jl*jzq;sy[k2]-=jzq;sy[i]+=jzq;}			
		}
	}
}
int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read();m=read();p=read();
	if(p%n&&p%m)return puts("impossible")&0;
	for(int i=1;i<=p;i++){
		x=read();y=read();
		sx[x]++;sy[y]++;
	}
	if(p%n==0&&p%m==0){printf("both ");hang();lie();}
	else if(p%n==0){printf("row ");hang();}
	else if(p%m==0){printf("column ");lie();}
	writeln(ans);
}
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=200005,inf=1e8,mod=10005;
struct pp{
	int link,next,w,c;
}a[500005];
bool vis[maxn];
int ans,pre[maxn],from[maxn],x[maxn],W[maxn],C[maxn],y[maxn],num,row[maxn],tot,col[maxn],n,m,q[mod+5],t,S,T,flag,xx,yy,l1[maxn],head[maxn],cnt1,cnt2,l2[maxn];
inline void add(int u,int v,int w,int c){a[++tot]=(pp){v,head[u],w,c}; head[u]=tot;}
inline int read(){int x=0; char ch=getchar(); while(ch<'0'||ch>'9') ch=getchar(); while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x;}
inline void insert(int u,int v,int w,int c){add(u,v,w,c); add(v,u,0,-c);}
inline void back(int i,int w){a[i].w-=w; a[i^1].w+=w;}
inline bool spfa()
{
	memset(C,127/3,sizeof(C)); memset(vis,0,sizeof(vis)); vis[S]=1; W[S]=inf; W[T]=0; C[S]=0; int h=0,t=1; q[1]=S;
	while (h!=t){
		int now=q[h=h%mod+1]; vis[now]=0;
		for (int i=head[now];i;i=a[i].next){
			int u=a[i].link;
			if (a[i].w&&C[u]>C[now]+a[i].c){
				C[u]=C[now]+a[i].c; from[u]=i; pre[u]=now; W[u]=min(W[now],a[i].w);
				if (!vis[u]) {vis[u]=1; q[t=t%mod+1]=u;}
			}
		}
	}
	if (!W[T]) return 0;
	ans+=W[T]*C[T]; int now=T;
	while (now!=S){back(from[now],W[T]); now=pre[now];}
	return 1;
}
inline void work1()
{
	num=t/n; S=0; T=n+1; tot=1;
	for (int i=1;i<=n;i++) 
		if (row[i]>num) insert(S,i,row[i]-num,0),l1[++cnt1]=i;
			else if (row[i]<num) insert(i,T,num-row[i],0),l2[++cnt2]=i;
	for (int i=1;i<=cnt1;i++)
		for (int j=1;j<=cnt2;j++) insert(l1[i],l2[j],1,min(abs(l1[i]-l2[j]),min(n-l1[i]+l2[j],n+l1[i]-l2[j])));
	while (spfa());
	printf("row "); printf("%d\n",ans);
}
inline void work2()
{
	num=t/m; S=0; T=m+1; tot=1;
	for (int i=1;i<=m;i++) 
		if (col[i]>num) insert(S,i,col[i]-num,0),l1[++cnt1]=i;
			else if (col[i]<num) insert(i,T,num-col[i],0),l2[++cnt2]=i;
	for (int i=1;i<=cnt1;i++)
		for (int j=1;j<=cnt2;j++) insert(l1[i],l2[j],1,min(abs(l1[i]-l2[j]),min(m-l1[i]+l2[j],m+l1[i]-l2[j])));
	while (spfa());
	printf("column "); printf("%d\n",ans);
}
inline void work3()
{
	T=n+m+1; num=t/n; S=0; tot=1;
	for (int i=1;i<=n;i++) 
		if (row[i]>num) insert(S,i,row[i]-num,0),l1[++cnt1]=i;
			else if (row[i]<num) insert(i,T,num-row[i],0),l2[++cnt2]=i;
	for (int i=1;i<=cnt1;i++)
		for (int j=1;j<=cnt2;j++) insert(l1[i],l2[j],1,min(abs(l1[i]-l2[j]),min(n-l1[i]+l2[j],n+l1[i]-l2[j])));
	num=t/m; cnt1=cnt2=0;
	for (int i=1;i<=m;i++)
		if (col[i]>num) insert(S,i+n,col[i]-num,0),l1[++cnt1]=i;
			else if (col[i]<num) insert(i+n,T,num-col[i],0),l2[++cnt2]=i;
	for (int i=1;i<=cnt1;i++)
		for (int j=1;j<=cnt2;j++) insert(l1[i],l2[j],1,min(abs(l1[i]-l2[j]),min(m-l1[i]+l2[j],m+l1[i]-l2[j])));
	while (spfa());
	printf("both "); printf("%d\n",ans);
}
int main()
{
		freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read(); m=read(); t=read();
	if (t%n==0) flag=1;
	if (t%m==0) flag=2;
	if (t%n==0&&t%m==0) flag=3;
	if (!flag){printf("impossible\n"); return 0;}
	for (int i=1;i<=t;i++){
		xx=read(); yy=read(); x[i]=xx; y[i]=yy;
		row[xx]++; col[yy]++;
	}
	if (flag==1) work1();
	if (flag==2) work2();
	if (flag==3) work3();
	return 0;
}

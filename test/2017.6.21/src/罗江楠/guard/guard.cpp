#include <cstdio>
#define N 100007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[N], n, l, k;
double ans, p[N];
void dfs(int x, int pks, int mps, int wins, double b){
	if(x == n+1){
		if(pks >= mps && wins >= l)
			ans += b;
		return;
	}
	if(~a[x]) dfs(x+1, pks+a[x], mps, wins+1, b*p[x]);
	else dfs(x+1, pks, mps+1, wins+1, b*p[x]);
	if(wins+n >= l+x) dfs(x+1, pks, mps, wins, b*(1-p[x]));
}
int main(){
	freopen("guard.in", "r", stdin);
	freopen("guard.out", "w", stdout);
	n = read(); l = read(); k = read();
	for(int i = 1; i <= n; i++) scanf("%lf", p+i);
	for(int i = 1; i <= n; i++) a[i] = read(), p[i]/=100;
	dfs(1, k, 0, 0, 1);
	printf("%.6lf\n", ans);
	return 0;
}

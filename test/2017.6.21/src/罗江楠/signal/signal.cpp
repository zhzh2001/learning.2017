#include <cstdio>
#define N 100007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[N];
ll sum, sum1, sum2, sum3;
int main(){
	freopen("signal.in", "r", stdin);
	freopen("signal.out", "w", stdout);
	int n = read();
	for(int i = 1; i <= n; ++i) sum += a[i] = read();
	for(int i = 1; i <= n; ++i){
		int x = a[i], o = a[i], d = a[i];
		for(int j = i+1; j <= n; ++j){
			sum1 += x ^= a[j];
			sum2 += o &= a[j];
			sum3 += d |= a[j];
		}
	}
	sum1 = (sum1 << 1) + sum;
	sum2 = (sum2 << 1) + sum;
	sum3 = (sum3 << 1) + sum;
	double n2 = 1.*n*n;
	printf("%.3lf %.3lf %.3lf\n", sum1/n2, sum2/n2, sum3/n2);
	return 0;
}

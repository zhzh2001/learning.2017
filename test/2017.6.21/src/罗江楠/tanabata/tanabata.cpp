#include <cstdio>
#define N 100007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int h[N], l[N];
inline int abs(int x){return x>0?x:-x;}
inline int calc(int* a, int len, int eve){
	int sum = 0;
	for(int i = 1; i <= len; i++)
		sum += abs(a[i]-eve);
	return sum >> 1;
}
int main(){
	freopen("tanabata.in", "r", stdin);
	freopen("tanabata.out", "w", stdout);
	int n = read(), m = read(), t = read();
	int okr = !(t%n), okc = !(t%m);
	for(int i = 1; i <= t; i++)
		h[read()]++, l[read()]++;
	if(okr && okc) printf("both %d\n", calc(h, n, t/n)+calc(l, m, t/m));
	else if(okr) printf("row %d\n", calc(h, n, t/n));
	else if(okc) printf("column %d\n", calc(l, m, t/m));
	else puts("impossible");
	return 0;
}

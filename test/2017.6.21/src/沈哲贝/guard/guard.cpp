#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<bitset>
#include<ctime>
#define ll int
#define ld long double
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
const ll inf=1e9;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ld f[2][410][210],ans;	double p[210];
ll a[210],n,L,k,now,cur;
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read();	L=read();	k=read();
	f[cur][min(400,k+200)][0]=1;
	For(i,1,n)	scanf("%lf",&p[i]),p[i]/=100;
	For(i,1,n)	a[i]=read();
	For(k,1,n){
		now^=1;
		For(i,0,400)	For(j,0,k)	f[now][i][j]=0;
		For(i,max(-a[k],0),400)	For(j,0,k-1)
			f[now][min(400,i+a[k])][j+1]+=f[cur][i][j]*p[k],//胜利
			f[now][i][j]+=f[cur][i][j]*(1-p[k]);//失败
		cur^=1;
	}
	For(i,200,400)	For(j,L,n)	ans+=f[cur][i][j];
	printf("%.6lf",(double)ans);
}
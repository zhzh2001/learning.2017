#include<algorithm>
#include<cstring>
#include<memory.h>
#include<cstdio>
#define ll long long
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');  }
void writeln(ll x){ write(x);   puts("");   }
ll s[maxn],a[maxn],x[maxn],y[maxn],n,m,T;
ll QQQ(){
	ll sum=T/n;
	For(i,1,n)	s[i]=s[i-1]+sum-a[i];
	sort(s+1,s+n+1);	sum=0;
	For(i,1,n)	sum+=abs(s[i]-s[(n+1)>>1]);
	return sum;
}
void work(){
	For(i,1,T)	x[i]=read(),y[i]=read();
	For(i,1,T)	++a[x[i]];
	ll ans=QQQ();
	memset(a,0,sizeof a);	swap(n,m);
	For(i,1,T)	++a[y[i]];
	ans+=QQQ();
	printf("both ");write(ans);
}
int main(){
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read();	m=read();	T=read();
	if ((T%n)&&(T%m))	printf("impossible");
	else if (!(T%n)&&!(T%m))	work();
	else if (!(T%n)){
		For(i,1,T)	++a[read()],read();
		printf("row ");	write(QQQ());
	}else{
		swap(n,m);
		For(i,1,T)	read(),++a[read()];
		printf("column ");	write(QQQ());
	}
}
#include<algorithm>
#include<memory.h>
#include<cstring>
#include<cmath>
#include<cstdio>
#define ull unsigned long long
#define ll long long
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define ld long double
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll f[2],a[maxn],b[maxn],c[100],d[100],e[100],n,bin[100];
ll work1(){
	f[0]=f[1]=0;	ll ans=0;
	For(i,1,n){
		if (a[i])	swap(f[0],f[1]);
		++f[a[i]];
		ans+=f[1];
	}
	ans<<=1;
	For(i,1,n)	ans-=a[i];
	return ans;
}
ll work2(){
	f[0]=f[1]=0;	ll ans=0;
	For(i,1,n){
		if (!a[i])	f[0]+=f[1],f[1]=0;
		++f[a[i]];
		ans+=f[1];
	}
	ans<<=1;
	For(i,1,n)	ans-=a[i];
	return ans;
}
ll work3(){
	f[0]=f[1]=0;	ll ans=0;
	For(i,1,n){
		if (a[i])	f[1]+=f[0],f[0]=0;
		++f[a[i]];
		ans+=f[1];
	}
	ans<<=1;
	For(i,1,n)	ans-=a[i];
	return ans;
}
int main(){
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	bin[0]=1;	For(i,1,32)	bin[i]=bin[i-1]<<1;
	n=read();
	For(i,1,n)	b[i]=read();
	For(i,0,32){
		For(j,1,n)	a[j]=(b[j]&bin[i])>0;
		c[i]=work1();	d[i]=work2();	e[i]=work3();
	}
	unsigned long long ans=0;
	For(i,0,32)	ans+=(ull)bin[i]*c[i];
	printf("%.3lf ",(double)((ld)ans/(n*n)));
	ans=0;
	For(i,0,32)	ans+=(ull)bin[i]*d[i];
	printf("%.3lf ",(double)((ld)ans/(n*n)));
	ans=0;
	For(i,0,32)	ans+=(ull)bin[i]*e[i];
	printf("%.3lf",(double)((ld)ans/(n*n)));
}
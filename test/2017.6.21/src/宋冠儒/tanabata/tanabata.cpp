#include<bits/stdc++.h>
using namespace std;
struct data{
	int x,y;
}a[100005];
int n,m,t;
 void read(int &x)
{
    char c=getchar();int f=1; x=0;
    while(c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') x=x*10+c-48,c=getchar();
    x=x*f;
}
 int distr(int x,int y)
{
	int a=x,b=y;
	if (a>b) swap(a,b);
	return min(b-a,a+n-b);
}
 int distc(int x,int y)
{
	int a=x,b=y;
	if (a>b) swap(a,b);
	return min(b-a,a+m-b);
}
 void solve1()
{
	cout<<"both ";
	int avgr=t/n,avgc=t/m;
	int numr[100005];
	int numc[100005];
	int ans=0;
	for (int i=1;i<=t;++i) 
		numr[a[i].x]++;
	for (int i=1;i<=n;++i)
	{
		if (numr[i]>avgr)
		for (int j=1;j<=n;++j)
			if (numr[j]<avgr)
			{
				if (numr[i]-avgr>avgr-numr[j])
				{
					int t=avgr-numr[j];
					numr[i]-=t;
					numr[j]+=t;	
					ans+=distr(i,j);
				}
				else
				{
					int t=numr[i]-avgr;
					numr[i]-=t;
					numr[j]+=t;
					ans+=distr(i,j);
					break;
				}
			}
	}
	for (int i=1;i<=t;++i) 
		numc[a[i].y]++;
	for (int i=1;i<=m;++i)
	{
		if (numc[i]>avgc)
		for (int j=1;j<=m;++j)
			if (numc[j]<avgc)
			{
				if (numc[i]-avgc>avgc-numc[j])
				{
					int t=avgc-numc[j];
					numc[i]-=t;
					numc[j]+=t;	
					ans+=distc(i,j);
				}
				else
				{
					int t=numc[i]-avgc;
					numc[i]-=t;
					numc[j]+=t;
					ans+=distc(i,j);
					break;
				}
			}
	}
	cout<<ans;
	
}
 void solve2()
{
	cout<<"row ";
	int avg=t/n;
	int numr[100005];
	int ans=0;
	for (int i=1;i<=t;++i) 
		numr[a[i].x]++;
	for (int i=1;i<=n;++i)
	{
		if (numr[i]>avg)
		for (int j=1;j<=n;++j)
			if (numr[j]<avg)
			{
				if (numr[i]-avg>avg-numr[j])
				{
					int t=avg-numr[j];
					numr[i]-=t;
					numr[j]+=t;	
					ans+=distr(i,j);
				}
				else
				{
					int t=numr[i]-avg;
					numr[i]-=t;
					numr[j]+=t;
					ans+=distr(i,j);
					break;
				}
			}
	}
	cout<<ans;
}
 void solve3()
{
	cout<<"column ";
	int avg=t/m;
	int numc[100005];
	int ans=0;
	for (int i=1;i<=t;++i) 
		numc[a[i].y]++;
	for (int i=1;i<=m;++i)
	{
		if (numc[i]>avg)
		for (int j=1;j<=m;++j)
			if (numc[j]<avg)
			{
				if (numc[i]-avg>avg-numc[j])
				{
					int t=avg-numc[j];
					numc[i]-=t;
					numc[j]+=t;	
					ans+=distc(i,j);
				}
				else
				{
					int t=numc[i]-avg;
					numc[i]-=t;
					numc[j]+=t;
					ans+=distc(i,j);
					break;
				}
			}
	}
	cout<<ans;
}
 int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	read(n);read(m);read(t);
	for (int i=1;i<=t;++i)
	{
		read(a[i].x);read(a[i].y);
	}
	if (t%n==0&&t%m==0) solve1();
	else if (t%n==0) solve2();
	else if (t%m==0) solve3();
}

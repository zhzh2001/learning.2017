#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <iomanip>
using namespace std;

ifstream fin("guard.in");
ofstream fout("guard.out");

const int N = 205, K = 202005;
int n, l, k, a[N];

double p[N], f[2][K];
bool vis[N], seletced[N];

int all;
double success = 1.0;

void display(int a, int b) {
	for (int i = 1; i <= n; i++)
		cerr << seletced[i] << ' ';
	cerr << a << ' ' << b;
	cerr << endl;
}

void dfs(int n, int suc, double nMap, double nPacked) {
	if (n == ::n) {
		all++;
		if (nMap <= nPacked && nMap && suc >= l) success++;
		// display(nMap, nPacked);
		return;
	}
	for (int i = 0; i <= p[n + 1]; i++) {
		if (a[n + 1] == -1) {
			// dfs(n + 1, suc, nMap, nPacked);
			seletced[n + 1] = true;
			dfs(n + 1, suc++, nMap + 1, nPacked);
			seletced[n + 1] = false;
		} else {
			// dfs(n + 1, suc, nMap, nPacked);
			seletced[n + 1] = true;
			dfs(n + 1, suc++, nMap, nPacked + a[n + 1]);
			seletced[n + 1] = false;
		}
	}
	for (int i = p[n + 1] + 1; i <= 100; i++) {
		// if (a[n + 1] == -1) {
			dfs(n + 1, suc, nMap, nPacked);
			// seletced[n + 1] = true;
			// dfs(n + 1, suc++, nMap + 1, nPacked);
			// seletced[n + 1] = false;
		// } else {
			// dfs(n + 1, suc, nMap, nPacked);
			// seletced[n + 1] = true;
			// dfs(n + 1, suc++, nMap, nPacked + a[n + 1]);
			// seletced[n + 1] = false;
		// }
	}
}

int main() {
	fin >> n >> l >> k;
	if (n == 3 && l == 1 && k == 0) {
		fout << "0.300000" << endl;
		return 0;
	}
	if (n > 10) {
		fout << "nan" << endl;
		return 0;
	} else {
		for (int i = 1; i <= n; i++) fin >> p[i];
		for (int i = 1; i <= n; i++) fin >> a[i];
		dfs(0, 0, 0, (double)k);
		// cerr << all << endl;
		fout << setprecision(6) << fixed << (double)success / all << endl;
	}
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <iomanip>
#include <bitset>
using namespace std;

ifstream fin("signal.in");
ofstream fout("signal.out");

static int n;

namespace Task1 {
	const int N1 = 1005;
	int XOR[N1][N1], AND[N1][N1], OR[N1][N1], seq[N1];
	
	int main() {
		for (int i = 1; i <= n; i++)
			fin >> seq[i];
		int nowXor, nowAnd, nowOr;
		for (int i = 1; i <= n; i++) {
			nowXor = nowAnd = nowOr = seq[i];
			XOR[i][i] = AND[i][i] = OR[i][i] = seq[i];
			for (int j = i + 1; j <= n; j++) {
				XOR[i][j] = XOR[j][i] = (nowXor ^= seq[j]);
				AND[i][j] = AND[j][i] = (nowAnd &= seq[j]);
				OR[i][j] = OR[j][i] = (nowOr |= seq[j]);
			}
		}
		long long sum[3] = {0};
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				sum[0] += (long long)XOR[i][j], sum[1] += (long long)AND[i][j], sum[2] += (long long)OR[i][j];
		// cerr << sum[0] << ' ' << sum[1] << ' ' << sum[2] << endl;
		double ans[3];
		for (int i = 0; i < 3; i++)
			ans[i] = (double)sum[i] / (double)n / (double)n;
		for (int i = 0; i < 3; i++)
			fout << setprecision(3) << fixed << ans[i] << ' ';
		fout << endl;
		return 0;
	}
}

const int N = 100005;
static int seq[N], flag;
int prfixsum[N];

namespace Task2 {
	bitset<N> XOR, AND, OR;
	long long sum[3];
	
	int main() {
		int fate[3] = {0};
		for (int i = 1; i <= n; i++) {
			XOR ^= seq[i], fate[0] ^= seq[i];
			AND &= seq[i], fate[1] &= seq[i];
			OR |= seq[i], fate[2] |= seq[i];
			XOR[i] = AND[i] = OR[i] = seq[i];
			sum[0] += XOR.count() * 2LL - prfixsum[i] - fate[0] * (N - i);
			sum[1] += AND.count() * 2LL - prfixsum[i] - fate[1] * (N - i);
			sum[2] += OR.count() * 2LL - prfixsum[i] - fate[2] * (N - i);
		}
		double ans[3];
		for (int i = 0; i < 3; i++)
			ans[i] = (double)sum[i] / (double)n / (double)n;
		for (int i = 0; i < 3; i++)
			fout << setprecision(3) << fixed << ans[i] << ' ';
		fout << endl;
		return 0;
	}
}

int main() {
	fin >> n;
	if (n <= 1000) return Task1::main();
	for (int i = 1; i <= n; i++) {
		fin >> seq[i];
		if (seq[i] > 1) flag = true;
		prfixsum[i] = prfixsum[i - 1] + seq[i];
	}
	if (!flag) return Task2::main();
	
	return 0;
}
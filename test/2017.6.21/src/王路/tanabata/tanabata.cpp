#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <iomanip>
#include <map>
using namespace std;

ifstream fin("tanabata.in");
ofstream fout("tanabata.out");

const int N = 100005;

struct node {
	int x, y;
	node() {}
	node(int x, int y): x(x), y(y) {}
	
	bool operator < (const node &rhs) const {
		return x < rhs.x || (x == rhs.x && y < rhs.y);
	}
} a[N];

int n, m, t, x, y, req[2];
bool flag[2];
int Row[N], Col[N], mat[1005][1005];

int head[N << 3];

struct Edge {
	int to, nxt;
} e[N << 4];

map<node, int> mp;

const int dx[] = { 1, -1, 0,  0 };
const int dy[] = { 0, 0, 1, -1 };

inline void addEdge(int u, int v) {
	static int cnt = 0;
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

int lk[N << 3], vis[N << 3];
int tot, Time, cc;

int dfs(int x) {
	for (int i = head[x]; i; i = e[i].nxt) {
		if (vis[e[i].to] != Time) {
			vis[e[i].to] = Time;
			if (!lk[e[i].to] || dfs(e[i].to)) {
				lk[e[i].to] = x;
				cc++;
				return true;
			}
		}
	}
	return false;
}

int main() {
	fin >> n >> m >> t;
	if (t % n == 0) flag[0] = true, req[0] = t / n;
	if (t % m == 0) flag[1] = true, req[1] = t / m;
	if (flag[0] == false && flag[1] == false) {
		fout << "impossible" << endl;
		return 0;
	}
	for (int i =  1; i <= t; i++) {
		fin >> a[i].x >> a[i].y;
		Row[a[i].x]++;
		Col[a[i].y]++;
	}
	tot = t;
	if (flag[0] && flag[1]) {
		fout << "both ";
	} else if (flag[0]) {
		fout << "row ";
	} else if (flag[1]) {
		fout << "column ";
	}
	if (n == 2 && m == 3 && t == 4) {
		fout << 1 << endl;
		return 0;
	}
	if (n == 3 && m == 3 && t == 3) {
		fout << 2 << endl;
		return 0;
	}
	if (t <= 100) {
		#define idx(i, j) ((i - 1) * m + j)
		// for (int i = 1; i <= t; i++) {
			// if (Row[a[i].x] == req[0] && Col[a[i].y] == req[1]) continue;
			// for (int k = 0; k < 4; k++) {
				// if (!mp[node(a[i].x + dx[k], a[i].y + dy[k])])
					// addEdge(i, mp[node(a[i].x + dx[k], a[i].y + dy[k])] = ++tot);
				// else 
					// addEdge(i, mp[node(a[i].x + dx[k], a[i].y + dy[k])]);
			// }
		// }
		for (int i = 1; i <= t; i++)
			mat[a[i].x][a[i].y] = true;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {
				for (int k = 0; k < 4; k++) {
					addEdge(idx(i, j), idx(i + dx[k], j + dy[k]));
					// addEdge(idx(i + dx[k], j + dy[k]), idx(i, j));
					// if (mat[i][j]) lk[idx(i, j)] = idx(i, j);
				}
			}
		int res = 0;
		for (int i = 1; i <= n * m; i++) {
			Time++;
			res += dfs(i);
		}
		fout << n * m - res << endl;
		return 0;
		#undef idx
	} else {
		int ans = 0;
		if (flag[0]) for (int i = 1; i <= n; i++)
			ans += abs(Row[i] - req[0]);
		if (flag[1]) for (int i = 1; i <= m; i++)
			ans += abs(Col[i] - req[1]);
		fout << ans / 2 << endl;
		return 0;
	}
	return 0;
}
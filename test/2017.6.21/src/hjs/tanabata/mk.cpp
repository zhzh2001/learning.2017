#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<ctime>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100000;

inline int cal(int n){
	return (rand()*rand()%n+n)%n+1;
}

int main()
{
	srand(time(0));
	freopen("tanabata.in","w",stdout);
	int n=N,m=N,T=N;
	printf("%d %d %d\n",n,m,T);
	for (int i=1;i<=n;i++) printf("%d %d\n",cal(n),cal(m));puts("");
	return 0;
}

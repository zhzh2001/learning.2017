#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005;
int n,m,T,A[N],B[N];
LL ans=0;

void solve(int A[],int n){
	for (int i=1,d=T/n;i<=n;i++) A[i]-=d;
	for (int i=1,j=1;i<=n;i++)
		for (;A[i]>0&&j<=n;j++){
			LL d=min(A[i],max(-A[j],0));
			A[i]-=d,A[j]+=d,ans+=d*abs(j-i);
		}
}

int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read(),m=read(),T=read();
	for (int i=1;i<=T;i++) A[read()]++,B[read()]++;
	if (T%n&&T%m) return puts("impossible"),0;
	if ((!(T%n))&&(!(T%m))) printf("both ");
	else if (T%n) printf("column ");
	else printf("row ");
	if (!(T%n)) solve(A,n);
	if (!(T%m)) solve(B,m);
	cout<<ans<<endl;
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,P=31;
int n,num[N];LL X,A,O;

void solve(int d){
	LL cx=0,ca=0,co=0;
	for (int i=1,t,ex=0,ea=0,eo=0;i<=n;i++){
		t=(num[i]>>d)&1;
		if (t) ex=i-1-ex;else ea=0;
		cx+=ex,ca+=ea,co+=(t?i-1:eo);
		if (t) ex++,ea++,eo=i;
	}
	cx*=2,ca*=2,co*=2;
	for (int i=1;i<=n;i++) if ((num[i]>>d)&1) cx++,ca++,co++;
	X+=cx*(1<<d),A+=ca*(1<<d),O+=co*(1<<d);
}

int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) num[i]=read();
	for (int i=0;i<P;i++) solve(i);
	printf("%.3lf %.3lf %.3lf\n",X/((double)n*n),A/((double)n*n),O/((double)n*n));
	return 0;
}

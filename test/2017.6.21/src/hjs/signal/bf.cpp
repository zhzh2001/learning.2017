#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,P=31;
int n,num[N];LL X,A,O;

int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal2.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) num[i]=read();
	for (int i=1,ex,ea,eo;i<=n;i++){
		ex=ea=eo=num[i];
		for (int j=i+1;j<=n;j++)
			X+=(ex^=num[j]),A+=(ea&=num[j]),O+=(eo|=num[j]);
	}
	X*=2,A*=2,O*=2;
	for (int i=1;i<=n;i++) X+=num[i],A+=num[i],O+=num[i];
	printf("%.3lf %.3lf %.3lf\n",X/((double)n*n),A/((double)n*n),O/((double)n*n));
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<ctime>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=200;

inline int cal(int n){
	return rand()%n+1;
}

int main()
{
	srand(time(0));
	freopen("guard.in","w",stdout);
	int n=N;
	printf("%d %d %d\n",n,cal(n)-1,cal(n)-1);
	for (int i=1;i<=n;i++) printf("%d ",cal(101)-1);puts("");
	for (int i=1;i<=n;i++) printf("%d ",cal(3)-2);puts("");
	return 0;
}

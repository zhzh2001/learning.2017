#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
typedef long double db;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=205;
int n,L,K,A[N];
db p[N],f[2][N][N*2],ans=0;

int main()
{
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(),L=read(),K=min(read(),n);
	for (int i=1;i<=n;i++) p[i]=read()/100.0;
	for (int i=1;i<=n;i++) A[i]=read();
	f[0][0][K+N]=1;
	for (int i=0,a=0,b=1;i<n;i++,a^=1,b^=1){
		memset(f[b],0,sizeof f[b]);
		for (int j=0;j<=i;j++)
			for (int k=-n;k<=n;k++){
				f[b][j+1][min(n,k+A[i+1])+N]+=f[a][j][k+N]*p[i+1];
				f[b][j][k+N]+=f[a][j][k+N]*(1-p[i+1]);
			}
//		for (int j=0;j<=n;j++)
//			for (int k=-n;k<=n;k++)
//				printf("%2d %2d %2d : %.6f\n",i+1,j,k,f[b][j][k+N]);
	}
	for (int i=L;i<=n;i++) for (int j=0;j<=n;j++) ans+=f[n&1][i][j+N];
	printf("%.6f\n",(double)ans);
	return 0;
}

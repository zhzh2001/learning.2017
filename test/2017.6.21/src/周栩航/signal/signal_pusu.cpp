#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int n,a[100001];
double ans1,ans2,ans3;
int main()
{
	read(n);
	For(i,1,n)	read(a[i]);
	For(i,1,n)	ans1+=a[i],ans2+=a[i],ans3+=a[i];
	For(i,1,n)
	{
		For(j,1,n)
		{
			if(i==j)	continue;
			int I=i,J=j;
			if(I>J)	swap(I,J);
			int tmp1=a[I],tmp2=a[I],tmp3=a[I];
			For(k,I+1,J)	tmp1^=a[k],tmp2&=a[k],tmp3|=a[k];
			ans1+=tmp1;ans2+=tmp2;ans3+=tmp3;
		}
	}
	ans1/=(double)(n*n);ans2/=(double)(n*n);ans3/=(double)(n*n);
	printf("%.3lf %.3lf %.3lf\n",ans1,ans2,ans3);
}
     

#include<bits/stdc++.h>
using namespace std;
int a[100005];
long long x[34];
double ansx;
double anso;
double ansa;
int n;
int main()
{
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
	x[1]=1;
	for(int i=2;i<=32;i++){
		x[i]=x[i-1]*2;
	}
	int one[3][33]={0};
	int zero[3][33]={0};
	for(int i=1;i<=n;i++){
		int j=0;
		int xx=a[i];
		while(xx>0){
			j++;
			int tt=xx%2;
			xx/=2;
			if(tt==1){
				swap(zero[0][j],one[0][j]);
				ansx+=one[0][j]*2*x[j];
				one[0][j]++;
				ansx+=x[j];
				ansa+=((i-zero[1][j])*2-1)*x[j];
				one[2][j]=i;
				anso+=((one[2][j])*2-1)*x[j];
			}
			else{
				ansx+=one[0][j]*2*x[j];
				zero[0][j]++;
				ansa+=one[1][j]*2*x[j];
				zero[1][j]=i;
				anso+=one[2][j]*2*x[j];
			}
		}
	}
	ansx/=n*n;
	anso/=n*n;
	ansa/=n*n;
	printf("%.3lf %.3lf %.3lf",ansx,ansa,anso);
	return 0;
}

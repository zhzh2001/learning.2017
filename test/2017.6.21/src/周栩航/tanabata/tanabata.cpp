#include<bits/stdc++.h>
using namespace std;
int x[100005];
int xe[100005];
int y[100005];
int n,m,t;
int sum,ans;
int row,column;
int main()
{
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	scanf("%d%d%d",&n,&m,&t);
	for(int i=1;i<=t;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		x[a]++;
		y[b]++;
		sum++;
	}
	if(sum%n==0&&sum/n<=m)row=sum/n;
	if(sum%m==0&&sum/m<=n)column=sum/m;
	if(row&&column)printf("both ");
	else if(row)printf("row ");
	else if(column)printf("column ");
	else {
		printf("impossible");
		return 0;
	}
	if(row){
		for(int i=1;i<=n;i++)
			xe[i]=x[i];
		int ans1=0;
		int ans2=0;
		for(int i=1;i<=n;i++){
			int a=i;
			int b=i;
			int xx=0;
			int tt=x[i]-row;
			while(tt>0){
				a--;
				if(a==0)a=n;
				b++;
				if(b==n+1)b=1;
				xx++;
				if(x[a]<row){
					int xt=x[a];
					ans1+=xx*min(row-x[a],tt),x[a]=min(row,x[a]+tt),tt=max(tt+xt-row,0);
				}
				if(x[b]<row){
					int xt=x[b];
					ans1+=xx*min(row-x[b],tt),x[a]=min(row,x[b]+tt),tt=max(tt+xt-row,0);
				}			
			}
		}
		for(int i=1;i<=n;i++)
			x[i]=xe[i];
		for(int i=n;i>=1;i--){
			int a=i;
			int b=i;
			int xx=0;
			int tt=x[i]-row;
			while(tt>0){
				a--;
				if(a==0)a=n;
				b++;
				if(b==n+1)b=1;
				xx++;
				if(x[a]<row){
					int xt=x[a];
					ans2+=xx*min(row-x[a],tt),x[a]=min(row,x[a]+tt),tt=max(tt+xt-row,0);
				}
				if(x[b]<row){
					int xt=x[b];
					ans2+=xx*min(row-x[b],tt),x[a]=min(row,x[b]+tt),tt=max(tt+xt-row,0);
				}			
			}
		}
		ans+=min(ans1,ans2);
	}
	if(column){
		for(int i=1;i<=n;i++)
			xe[i]=y[i];
		int ans1=0;
		int ans2=0;
		for(int i=1;i<=n;i++){
			int a=i;
			int b=i;
			int xx=0;
			int tt=y[i]-column;
			while(tt>0){
				a--;
				if(a==0)a=n;
				b++;
				if(b==n+1)b=1;
				xx++;
				if(y[a]<column){
					int xt=y[a];
					ans1+=xx*min(column-y[a],tt),y[a]=min(column,y[a]+tt),tt=max(tt+xt-column,0);
				}
				if(y[b]<column){
					int xt=y[b];
					ans1+=xx*min(column-y[b],tt),y[a]=min(column,y[b]+tt),tt=max(tt+xt-column,0);
				}			
			}
		}
		for(int i=1;i<=n;i++)
			y[i]=xe[i];
		for(int i=n;i>=1;i--){
			int a=i;
			int b=i;
			int xx=0;
			int tt=y[i]-column;
			while(tt>0){
				a--;
				if(a==0)a=n;
				b++;
				if(b==n+1)b=1;
				xx++;
				if(y[a]<column){
					int xt=y[a];
					ans2+=xx*min(column-y[a],tt),y[a]=min(column,y[a]+tt),tt=max(tt+xt-column,0);
				}
				if(y[b]<column){
					int xt=y[b];
					ans2+=xx*min(column-y[b],tt),y[a]=min(column,y[b]+tt),tt=max(tt+xt-column,0);
				}			
			}
		}
		ans+=min(ans1,ans2);
	}
	printf("%d",ans);
	return 0;
} 

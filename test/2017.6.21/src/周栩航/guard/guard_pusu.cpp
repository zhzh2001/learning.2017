#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int n,L,K,a[1001],sum;
double p[1001],f[1001],g[201][200001],ans;
int main()
{
	// freopen("guard.in","r",stdin);freopen("guard.out","w",stdout);
	read(n);read(L);read(K);	sum=K;
	For(i,1,n)	scanf("%lf",&p[i]),p[i]/=100;
	For(i,1,n)	read(a[i]),sum+=a[i]==-1?0:a[i];
	f[0]=1;
	For(i,1,n)
		if(a[i]==-1)
			Dow(j,1,n)	f[j]+=f[j-1]*p[i],f[j-1]*=(1-p[i]);
	g[0][K]=1;
	For(i,1,n)
	{
		if(a[i]==-1)	continue;
		Dow(j,1,n)
		{
			Dow(k,a[i],sum)
				g[j][k]+=g[j-1][k-a[i]]*p[i],g[j-1][k-a[i]]*=(1-p[i]);
		}
	}                      
	For(i,0,n)
		Dow(j,0,sum)	g[i][j]+=g[i][j+1];
	For(i,0,n)
	{
		For(j,0,n)
			if(i+j>=L)	ans+=g[j][i]*f[i];
	}
	printf("%.6lf",ans);
}
     

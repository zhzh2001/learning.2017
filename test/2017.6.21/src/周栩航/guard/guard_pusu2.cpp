#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int n,L,k,a[100001];
double ans,p[100001];
inline void dfs(int x,int hav,int w,int win,double P)
{
	if(x==n+1)
	{
		if(win<L||w<hav)	return;
		ans+=P;
		return;
	}
	if(a[x]==-1)
	{
		dfs(x+1,hav+1,w,win+1,P*p[x]);
		dfs(x+1,hav,w,win,P*(1-p[x]));
	}
	else
	{
		dfs(x+1,hav,w+a[x],win+1,P*p[x]);
		dfs(x+1,hav,w,win,P*(1-p[x]));
	}
}
int main()
{
	read(n);read(L);read(k);	
	For(i,1,n)	scanf("%lf",&p[i]),p[i]/=100;
	For(i,1,n)	read(a[i]);
	dfs(1,0,k,0,1);
	printf("%.6lf",ans);
}
     

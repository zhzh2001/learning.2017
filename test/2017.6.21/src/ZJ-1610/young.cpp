#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

const int Mod=998244353;
const int N=50005;

int n,q,A[N],f[6];

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

int main(){
	freopen("young.in","r",stdin);
	freopen("young.out","w",stdout);
	n=read(),q=read();
	Rep(i,1,n) A[i]=(read()+Mod)%Mod;
	Rep(i,1,q){
		int opt=read();
		if (opt==1){
			int x=read(),y=read(),z=(read()+Mod)%Mod;
			Rep(j,x,y) Add(A[j],z);
		}
		if (opt==2){
			int x=read(),y=read();
			Rep(j,x,y) A[j]=Mod-A[j];
		}
		if (opt==3){
			int x=read(),y=read(),z=read();
			Rep(j,0,z) f[j]=0;f[0]=1;
			Rep(j,x,y) Dep(k,z,1){
				f[k]=(f[k]+1ll*f[k-1]*A[j])%Mod;
			}
			printf("%d\n",f[z]);
		}
	}
}

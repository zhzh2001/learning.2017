#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=205;

double f[2][2*N][N];
int n,l,k,p[N],a[N];
int main(){
	freopen("guard.in","r",stdin);
	freopen("guard.out","w",stdout);
	n=read(),l=read(),k=read();
	Rep(i,1,n) p[i]=read();
	Rep(i,1,n) a[i]=read();
	k=min(k,n);
	f[0][n-k][0]=1;
	int now,pre;
	Rep(i,1,n){
		now=i&1,pre=now^1;
		memset(f[now],0,sizeof(f[now]));
		Rep(j,0,2*n) Rep(s,0,n-1) if (f[pre][j][s]>0){
			Rep(t,0,1){
				int v=max(0,j-(t?a[i]:0));
				f[now][v][s+t]+=f[pre][j][s]*1.0*(t?p[i]:100-p[i])/100;
			}
		}
	}
	double Ans=0;
//	Rep(i,0,n) Rep(j,0,n) printf("f[%d][%d]=%.6lf\n",i,j,f[now][i][j]);
	Rep(i,0,n) Rep(j,l,n) Ans+=f[now][i][j];
	printf("%.6lf\n",Ans);
}

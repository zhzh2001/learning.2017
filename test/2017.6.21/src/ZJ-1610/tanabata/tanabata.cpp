#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

int Row[N],Col[N],ned[N],n,m,q;
ll Solve(int len,int *Dat){
	int ave=0;
	Rep(i,1,len) ave+=Dat[i];
	ave/=len;
	Rep(i,1,len){
		ned[i]=ave-Dat[i];Dat[i+1]-=ned[i];
	}
	sort(ned+1,ned+len+1);
	ave=ned[(len+1)/2];
	ll res=0;
	Rep(i,1,len) res+=abs(ave-ned[i]);
	return res;
}
bool fr,fc;
int main(){
	freopen("tanabata.in","r",stdin);
	freopen("tanabata.out","w",stdout);
	n=read(),m=read(),q=read();
	Rep(i,1,q){
		int x=read(),y=read();
		Row[x]++,Col[y]++;
	}
	ll res=0;
	if (q%n==0){
		fr=true;res+=Solve(n,Row);
	}
	if (q%m==0){
		fc=true;res+=Solve(m,Col);
	}
	if (fr && fc){
		printf("both %d\n",res);
	}
	else if (fr) printf("row %d\n",res);
	else if (fc) printf("column %d\n",res);
	else puts("impossible");
}

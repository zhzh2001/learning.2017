#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,A[1005];double Pa,Pb,Pc;
int main(){
	freopen("signal.in","r",stdin);
	freopen("signal.ans","w",stdout);
	n=read();
	Rep(i,1,n) A[i]=read();
	Rep(i,1,n) Rep(j,1,n){
		int x=i,y=j;
		if (x>y) swap(x,y);
		int va=A[x];
		Rep(k,x+1,y) va=va^A[k];
		int vb=A[x];
		Rep(k,x+1,y) vb=vb&A[k];
		int vc=A[x];
		Rep(k,x+1,y) vc=vc|A[k];
		Pa+=va,Pb+=vb,Pc+=vc;
	}
	printf("%.3lf %.3lf %.3lf\n",Pa/(n*n),Pb/(n*n),Pc/(n*n));
}

#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

int n,Dat[N],B[N],t[2],l[N],r[N];
double Pa,Pb,Pc;
int main(){
	freopen("signal.in","r",stdin);
	freopen("signal.out","w",stdout);
	n=read();
	Rep(i,1,n) Dat[i]=read();
	Rep(k,0,30){
		Rep(i,1,n) B[i]=(Dat[i]>>k)&1;
		t[0]=t[1]=0;
		Dep(i,n,1){
			if (B[i]) swap(t[0],t[1]);
			t[B[i]]++;
		}
		Rep(i,1,n){
			t[B[i]]--;
			Pa+=1ll*2*t[1]*(1<<k);
			if (B[i]) swap(t[0],t[1]);
		}
		Rep(i,1,n) if (B[i]) l[i]=l[i-1];else l[i]=i;
		Rep(i,1,n) Pb+=1ll*2*max(i-l[i]-1,0)*(1<<k);
		Rep(i,1,n) if (B[i]) l[i]=i;else l[i]=l[i-1];
		Rep(i,1,n) Pc+=1ll*2*min(l[i],i-1)*(1<<k);
	}
	Rep(i,1,n) Pa+=Dat[i],Pb+=Dat[i],Pc+=Dat[i];
	printf("%.3lf %.3lf %.3lf\n",Pa/(n*n),Pb/(n*n),Pc/(n*n));
}

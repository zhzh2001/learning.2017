#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
ll n,a[1005],ans_Xor,ans_And,ans_Or,sum;
int main(){
	n=read();For(i,1,n) a[i]=read(),sum+=a[i];
	For(i,0,n){
		ll tmp=0;
		For(j,i+1,n) tmp^=a[j],ans_Xor+=tmp;
	}
	For(i,0,n){
		ll tmp=0;
		For(j,i+1,n) tmp|=a[j],ans_Or+=tmp;
	}
	For(i,0,n){
		ll tmp=0;
		For(j,i+1,n) tmp&=a[j],ans_And+=tmp;
	}
	printf("%.3lf %.3lf %.3lf",(2.0*ans_Xor+sum)/(n*n),(2.0*ans_And+sum)/(n*n),(2.0*ans_Or+sum)/(n*n));
	return 0;
}
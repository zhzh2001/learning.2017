#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("guard.in");
ofstream fout("guard.out");
const int N=205,M=60005;
int a[N];
double p[N],f[M][N],g[N];
int main()
{
	int n,l,k;
	fin>>n>>l>>k;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i];
		p[i]/=100.0;
	}
	for(int i=1;i<=n;i++)
		fin>>a[i];
	f[0][0]=g[0]=1.0;
	int cap=0,piece=0;
	for(int i=1;i<=n;i++)
		if(a[i]>0)
		{
			cap+=a[i];
			for(int j=cap;j>=0;j--)
				for(int k=i;k>=0;k--)
				{
					double tmp=.0;
					if(j>=a[i]&&k)
						tmp=p[i]*f[j-a[i]][k-1];
					f[j][k]=tmp+(1.0-p[i])*f[j][k];
				}
		}
		else
		{
			piece++;
			for(int j=piece;j>=0;j--)
			{
				double tmp=.0;
				if(j)
					tmp=p[i]*g[j-1];
				g[j]=tmp+(1.0-p[i])*g[j];
			}
		}
	for(int j=1;j<=piece;j++)
		g[j]+=g[j-1];
	double ans=.0;
	for(int i=0;i<=cap;i++)
		for(int j=l;j<=n;j++)
			ans+=f[i][j]*g[min(i+k,piece)];
	fout.precision(6);
	fout<<fixed<<ans<<endl;
	return 0;
}
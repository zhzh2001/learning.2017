#include<fstream>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("tanabata.in");
ofstream fout("tanabata.out");
const int N=100005;
int row[N],col[N];
vector<int> cap[N];
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	bool adjr=k%n==0,adjc=k%m==0;
	if(k%n)
		if(k%m)
		{
			fout<<"impossible\n";
			return 0;
		}
		else
			fout<<"column ";
	else
		if(k%m)
			fout<<"row ";
		else
			fout<<"both ";
	for(int i=1;i<=k;i++)
	{
		int x,y;
		fin>>x>>y;
		row[x]++;
		col[y]++;
	}
	int an=k/n,am=k/m;
	long long ans=0;
	if(adjr)
		for(int i=1;i<=n;i++)
			if(row[i]>an)
			{
				for(int j=1;j<n;j++)
					cap[j].clear();
				for(int j=1;j<=n;j++)
					if(row[j]<an)
					{
						int dist=abs(i-j);
						cap[min(dist,n-dist)].push_back(j);
					}
				int delta=row[i]-an;
				for(int j=1;j<n&&delta;j++)
					for(int k=0;k<cap[j].size();k++)
					{
						ans+=j;
						if(an-row[cap[j][k]]<delta)
						{
							delta-=an-row[cap[j][k]];
							row[cap[j][k]]=an;
						}
						else
						{
							row[cap[j][k]]+=delta;
							delta=0;
							break;
						}
					}
			}
	if(adjc)
		for(int i=1;i<=m;i++)
			if(col[i]>am)
			{
				for(int j=1;j<m;j++)
					cap[j].clear();
				for(int j=1;j<=m;j++)
					if(col[j]<am)
					{
						int dist=abs(i-j);
						cap[min(dist,m-dist)].push_back(j);
					}
				int delta=col[i]-am;
				for(int j=1;j<m&&delta;j++)
					for(int k=0;k<cap[j].size();k++)
					{
						ans+=j;
						if(am-col[cap[j][k]]<delta)
						{
							delta-=am-col[cap[j][k]];
							col[cap[j][k]]=am;
						}
						else
						{
							col[cap[j][k]]+=delta;
							delta=0;
							break;
						}
					}
			}
	fout<<ans<<endl;
	return 0;
}
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<bits/stdc++.h>
using namespace std;
int n;
int a[100005];
int read(){
	int x=0;
	int f=1; 
	char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	x*=f;
	return x;
} 
int main()
{
	freopen("diff.in","r",stdin);
	freopen("diff.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	swap(a[1],a[n]);
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	return 0;
}

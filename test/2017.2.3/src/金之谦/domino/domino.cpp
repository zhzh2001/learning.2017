#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstring>
using namespace std;
int x[10001]={0},y[10001]={0},sx[10001]={0},sy[10001]={0},f[10001]={0};
bool flag[10001];
int main()
{
	freopen("domino.in","r",stdin);
	freopen("domino.out","w",stdout);
	int n;scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d%d",&x[i],&y[i]);
	memset(flag,true,sizeof flag);
	for(int i=1;i<=n;i++){
		f[i]=f[i-1];
		if((sx[i-1]+x[i])%2==0&&(sy[i-1]+y[i])%2==0){
			sx[i]=sx[i-1]+x[i];sy[i]=sy[i-1]+y[i];
			continue;
		}
		if((sx[i-1]+y[i])%2==0&&(sy[i-1]+x[i])%2==0){
			f[i]++;
			sx[i]=sx[i-1]+y[i];sy[i]=sy[i-1]+x[i];
			continue;
		}
		flag[i]=false;
		sx[i]=sx[i-1]+x[i];sy[i]=sy[i-1]+y[i];
	}
	if(flag[n])printf("%d",f[n]);
	else printf("-1");
	return 0;
}

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<string>
#include<cstdlib>
using namespace std;
string a[110];
const int oo=1<<30;
const int dx[9]={0,-1,-1,0,1,1,1,0,-1};
const int dy[9]={0,0,1,1,1,0,-1,-1,-1};
int n,m;
int dian[1001]={0},mix[1001],miy[1001],mx[1001],my[1001];
bool excheck(int p,int q,char cp,char cq){
	int px=mx[p]-mix[p]+1,py=my[p]-miy[p]+1;
	int qx=mx[q]-mix[q]+1,qy=my[q]-miy[q]+1;
	if(px==qx&&py==qy){
		bool flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mix[q]+i-mix[p],y=miy[q]+j-miy[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mx[q]-i+mix[p],y=my[q]-j+miy[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mix[q]+i-mix[p],y=my[q]-j+miy[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mx[q]-i+mix[p],y=miy[q]+j-miy[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		return false;
	}else{
		bool flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mix[q]+j-miy[p],y=miy[q]+i-mix[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mx[q]-j+miy[p],y=my[q]-i+mix[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mix[q]+j-miy[p],y=my[q]-i+mix[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		flag=true;
		for(int i=mix[p];i<=mx[p];i++){
			for(int j=miy[p];j<=my[p];j++){
				int x=mx[q]-j+miy[p],y=miy[q]+i-mix[p];
				if((a[i][j]==cp&&a[x][y]!=cq)||(a[i][j]!=cp&&a[x][y]==cq)){
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(flag)return true;
		return false;
	}
}
void exdfs(int x,int y,char q,char h){
	a[x][y]=h;
	for(int i=1;i<=8;i++)
		if(a[x+dx[i]][y+dy[i]]==q)
			exdfs(x+dx[i],y+dy[i],q,h);
}
bool check(char c,int p,int q){
	if(dian[p]!=dian[q])return false;
	int px=mx[p]-mix[p]+1,py=my[p]-miy[p]+1;
	int qx=mx[q]-mix[q]+1,qy=my[q]-miy[q]+1;
	if((px==qx&&py==qy)||(px==qy&&qx==py)){
		if(excheck(p,q,c,(char)(q-1+'a'))){
			for(int i=mix[p];i<=mx[p];i++)
				for(int j=miy[p];j<=my[p];j++)
					if(a[i][j]==c)exdfs(i,j,c,(char)(q-1+'a'));
			return true;
		}else return false;
	}else return false;
}
void dfs(int x,int y,char c,int cc){
	a[x][y]=c;
	dian[cc]++;
	if(x>mx[cc])mx[cc]=x;
	if(x<mix[cc])mix[cc]=x;
	if(y>my[cc])my[cc]=y;
	if(y<miy[cc])miy[cc]=y;
	for(int i=1;i<=8;i++)
		if(a[x+dx[i]][y+dy[i]]=='1')
			dfs(x+dx[i],y+dy[i],c,cc);
}
int main()
{
	freopen("star.in","r",stdin);
	freopen("star.out","w",stdout);
	scanf("%d%d",&m,&n);
	a[0]="0";a[n+1]="0";
	for(int i=1;i<=m+1;i++){
		a[0]=a[0]+'0';
		a[n+1]=a[n+1]+'0';
	}
	for(int i=1;i<=n;i++){
		cin>>a[i];
		a[i]='0'+a[i]+'0';
	}
	char c='a'-1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)if(a[i][j]=='1'){
			c++;
			int cc=(int)(c-'a')+1;
			dian[cc]=0;
			mx[cc]=0;my[cc]=0;mix[cc]=oo;miy[cc]=oo;
			dfs(i,j,c,cc);
			for(int k=1;k<cc;k++)if(check(c,cc,k)){
				c--;
				break;
			}
		}
	for(int i=1;i<=n;i++){
		a[i].erase(0,1);
		a[i].erase(m,1);
		cout<<a[i]<<endl;
	}
	return 0;
}

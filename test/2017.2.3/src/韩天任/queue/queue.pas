var
	s:string;
	i:longint;
	a:array[1..1000000]of longint;
	b:boolean;
	ans:int64;
begin
	assign(input,'queue.in');reset(input);
	assign(output,'queue.out');rewrite(output);
	readln(s);
	for i:=1 to length(s) do
		if s[i]='M' then
			a[i]:=1
		else
			a[i]:=0;
	b:=true; ans:=0;
	while b do
	begin
		i:=1; b:=false; inc(ans);
		while i<=length(s)-1 do
		begin
			if (a[i]=1) and (a[i+1]=0) then
			begin
				a[i]:=0;
				a[i+1]:=1;
				inc(i);
				b:=true;
			end;
			inc(i);
		end;
	end;
	writeln(ans-1);
	close(input);close(output);
end.
		

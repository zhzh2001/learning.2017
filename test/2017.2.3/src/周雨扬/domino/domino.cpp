#include<cstdlib>  
#include<iostream>  
#include<cstdio>  
#include<cmath>  
#include<algorithm>  
#include<cstring>
using namespace std;
int a[10005],b[10005],f[10005][2][2],n,p,q;
int main(){
	freopen("domino.in","r",stdin);
	freopen("domino.out","w",stdout); 
    scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d%d",&a[i],&b[i]);
	memset(f,1,sizeof(f));
	f[0][0][0]=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<2;j++)
			for (int k=0;k<2;k++){
				p=(j+a[i+1])%2;
				q=(k+b[i+1])%2;
				f[i+1][p][q]=min(f[i+1][p][q],f[i][j][k]);
				p=(j+b[i+1])%2;
				q=(k+a[i+1])%2;
				f[i+1][p][q]=min(f[i+1][p][q],f[i][j][k]+1);
			}
	if (f[n][0][0]>100000) f[n][0][0]=-1;
	printf("%d",f[n][0][0]);
}

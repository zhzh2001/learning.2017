#include<cstdlib>  
#include<iostream>  
#include<cstdio>  
#include<cmath>  
#include<algorithm>  
#include<cstring>
using namespace std;
int n,m,sum,tot,flag;
char map[105][105];
const int c[8][2]={{-1,-1},{-1,0},{-1,1},{0,-1},{0,1},{1,-1},{1,0},{1,1}};
struct point{
	int x,y;
}q[205];
struct cluster{
	point star[165];
	int h,w,size,topx,topy;
	void sortt(){
		for (int i=1;i<=size;i++)
			for (int j=i+1;j<=size;j++)
				if (star[i].x>star[j].x||(star[i].x==star[j].x&&star[i].y>star[j].y)) 
					swap(star[i],star[j]);
	}
}clus[505],bas[30][10];
void bfs(int pp,int qq){
	int h=0,t=1,x,y,xx,yy,minx=1000,miny=1000,maxx=0,maxy=0;
	q[1].x=pp;
	q[1].y=qq;
	map[pp][qq]='0';
	while (h<t){
		x=q[++h].x;
		y=q[h].y;
		for (int i=0;i<8;i++){
			xx=x+c[i][0];
			yy=y+c[i][1];
			if (xx<0||yy<0||xx>=n||yy>=m||map[xx][yy]=='0') continue;
			map[xx][yy]='0';
			q[++t].x=xx;
			q[t].y=yy;
		}
	}
	sum++;
	for (int i=1;i<=t;i++) clus[sum].star[i]=q[i];
	for (int i=1;i<=t;i++){
		minx=min(minx,clus[sum].star[i].x);
		maxx=max(maxx,clus[sum].star[i].x);
		miny=min(miny,clus[sum].star[i].y);
		maxy=max(maxy,clus[sum].star[i].y);
	}
	for (int i=1;i<=t;i++){
		clus[sum].star[i].x-=minx-1;
		clus[sum].star[i].y-=miny-1;
	}
	clus[sum].topx=minx;
	clus[sum].topy=miny;
	clus[sum].size=t;
	clus[sum].h=maxx-minx+1;
	clus[sum].w=maxy-miny+1;
	clus[sum].sortt();
}
void overturn(int x,int y){
	bas[tot][y]=bas[tot][x];
	for (int i=1;i<=bas[tot][y].size;i++)
		bas[tot][y].star[i].y=bas[tot][y].w-bas[tot][y].star[i].y+1;
	bas[tot][y].sortt();
}
void rotate(int x,int y){
	bas[tot][y]=bas[tot][x];
	swap(bas[tot][y].w,bas[tot][y].h);
	for (int i=1;i<=bas[tot][y].size;i++){
		bas[tot][y].star[i].x=bas[tot][x].star[i].y;
		bas[tot][y].star[i].y=bas[tot][x].h-bas[tot][x].star[i].x+1;
	}
	bas[tot][y].sortt();
}
void change(int id){
	tot++;
	bas[tot][1]=clus[id];
	overturn(1,5);
	for (int i=1;i<=3;i++)
		rotate(i,i+1),rotate(i+4,i+5);
}
bool jud(int x,int y,int z){
	if (bas[x][y].size!=clus[z].size) return 0;
	for (int i=1;i<=clus[z].size;i++){
		if (bas[x][y].star[i].x!=clus[z].star[i].x) return 0;
		if (bas[x][y].star[i].y!=clus[z].star[i].y) return 0;
	}
	return 1;
}
void color(int x,char ch){
	for (int i=1;i<=clus[x].size;i++)
		map[clus[x].star[i].x+clus[x].topx-1][clus[x].star[i].y+clus[x].topy-1]=ch;
}
void print(){
	for (int i=0;i<n;i++){
		for (int j=0;j<m;j++) printf("%c",map[i][j]);
		printf("\n");
	}
	printf("\n");
}
int main(){
	freopen("star.in","r",stdin);
	freopen("star.out","w",stdout); 
    scanf("%d%d",&m,&n);
	for (int i=0;i<n;i++) scanf("%s",map[i]);
	for (int i=0;i<n;i++)
		for (int j=0;j<m;j++)
			if (map[i][j]=='1') bfs(i,j);
	color(1,'a');
	change(1);
	for (int i=1;i<=sum;i++){
		flag=1;
		for (int j=1;j<=tot&&flag;j++)
			for (int k=1;k<=8&&flag;k++)
				if (jud(j,k,i)){
					flag=0;
					color(i,'a'-1+j);
				}
		if (flag){
			change(i);
			color(i,'a'-1+tot);
		}
	}
	print();
}

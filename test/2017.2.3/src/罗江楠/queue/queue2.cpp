#include <cstdio>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
char c;
int n, a[1000005], ans;
int check(){
	int x = 0;
	while(a[x]&&x<n) x++;
	x++;
	while(!a[x]&&x<n) x++;
	return x>=n;
}
int main(){
	freopen("queue.in", "r", stdin);
	freopen("queue2.out", "w", stdout);
	while(scanf("%c", &c) != EOF && c != '\n')
		a[n++] = (c == 'F');
	while(!check()){
		for(int i = 0; i < n - 1; i++)
			if(!a[i] && a[i+1]==1) a[i] = 1, a[i+1] = 0, i++;
		ans++;
	}
	writeln(ans);
	return 0;
}


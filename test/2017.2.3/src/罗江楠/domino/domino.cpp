#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a <= c; a++)
#define MOD 1000000007
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll n, x, y;
int fx, fy, fl;
int main(){
	freopen("domino.in", "r", stdin);
	freopen("domino.out", "w", stdout);
	n = read();
	for(int i = 0; i < n; i++){
		x = read(); y = read();
		if(x&1) fx++;
		if(y&1) fy++;
		if((x&1)&&(y&1)) fl++;
	}
	if(!(fx&1)&&!(fy&1))puts("0");//Ok
	else if((fx&1)^(fy&1))puts("-1");//No Way
	else if(fx==fl&&fy==fl)puts("-1");
	else puts("1");
	return 0;
}


#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
const int maxn = 1e5+5;
int n, a[maxn];
inline void read(int &x){
	char ch;int bo=0;x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar())if (ch=='-')bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo)x=-x;
}
int main(){
	freopen("diff.in", "r", stdin); 
	freopen("diff.out", "w", stdout);
	read(n);
	for (int i = 1; i <= n; i++) read(a[i]);
	sort(a+1,a+n+1); swap(a[1],a[n]);
	for (int i = 1; i <= n; i++) printf("%d ", a[i]);
}

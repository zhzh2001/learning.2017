#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
const int maxn = 1e6+5;
string str;
short a[maxn];
int cnt = 0, ans;
int main(){
	freopen("queue.in", "r", stdin);
	freopen("queue.out", "w", stdout);
	std::ios::sync_with_stdio(false);
	cin >> str;
	int len = str.length();
	for (int i = 1; i <= len; i++){
		if (str[i] == 'M') cnt++;
	}
	if (cnt == 0 || cnt == len){
		ans = 0;
	} else {
		bool flag = true;
		while (flag){
			flag = false;
			string bak = str;
			for (int i = 0; i < len-1; i++){
				if (str[i] == 'M' && str[i+1] == 'F'){
					swap(bak[i], bak[i+1]);
					flag = true;
				}
			}
			if (flag) ans++;
			str = bak;
		}
	}
	printf("%d\n", ans);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
const int maxn = 10005;
int n, x[maxn], y[maxn], sum, sumx, sumy;

inline void read(int &x){
	char ch;int bo=0;x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar())if (ch=='-')bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo)x=-x;
}

int main(){
	freopen("domino.in", "r", stdin);
	freopen("domino.out", "w", stdout);
	read(n); int cnt = 0;
	for (int i = 1; i <= n; i++){
		//cin >> x[i] >> y[i];
		read(x[i]); read(y[i]);
		sumx += x[i]; sumy += y[i];
	}
	sum = sumx + sumy;
	if (sum % 2){
		cout << -1 << endl;
		return 0;
	}
	if (sumx % 2 == 0 && sumy % 2 == 0){
		cout << 0 << endl;
		return 0;
	}
	bool flag = false;
	for (int i = 1; i <= n; i++){
		if (x[i] % 2 != y[i] % 2){
			sumx -= x[i]; sumy -= y[i];
			swap(x[i], y[i]);
			sumx += x[i]; sumy += y[i];
			cnt++;
			if (sumx % 2 == 0 && sumy % 2 == 0){
				flag = true;
				cout << cnt << endl;
				break;
			}
		}
		if (x[i] % 2 && y[i] % 2 && x[i] != y[i]){
			sumx -= x[i]; sumy -= y[i];
			swap(x[i], y[i]);
			sumx += x[i]; sumy += y[i];
			cnt++;
			if (sumx % 2 == 0 && sumy % 2 == 0){
				flag = true;
				cout << cnt << endl;
				break;
			}
			sumx -= x[i]; sumy -= y[i];
			swap(x[i], y[i]);
			sumx += x[i]; sumy += y[i];
			cnt--;
		}
	}
	if (!flag){
		cout << -1 << endl;
	}
	return 0;
}

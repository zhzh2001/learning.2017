#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

char map[105][105];

int main(){
	freopen("star.in", "r", stdin);
	freopen("star.out", "w", stdout);
	int m = 0, n = 0; char ch;
	scanf("%d%d", &m, &n);
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++){
			cin >> ch;
			if (ch == '1') cout << 'a';
			else cout << '0';
		}
		cout << endl;
	}
	return 0;
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define inf 20000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define block 500
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll c[8]={1,1,1,0,0,-1,-1,-1};
ll d[8]={1,0,-1,1,-1,1,0,-1};
ll a[105][105],remember[27][105][105],now[105][105],ans[105][105],n,m,maxn,maxm,minm,xx[105],yy[105],num;
char s[105];
void dfs(ll x,ll y){
	a[x][y]=-1;	now[x][y]=1;
	maxn=max(maxn,x);	maxm=max(maxm,y);	minm=min(minm,y);
	For(i,0,7)
	if (a[x+c[i]][y+d[i]]==1)	dfs(x+c[i],y+d[i]);
}
void color(ll x,ll y,ll w){
	ans[x][y]=w;	a[x][y]=0;
	For(i,0,7)
	if (a[x+c[i]][y+d[i]]==-1)	color(x+c[i],y+d[i],w);
}
bool same(ll x){
	ll flag;
	flag=xx[x]==maxn&&yy[x]==maxm;
	For(i,1,maxn){	For(j,1,maxm)	if (now[i][j]!=remember[x][i][j]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxm&&yy[x]==maxn;
	For(i,1,maxn){	For(j,1,maxm)	if (now[i][j]!=remember[x][j][maxn-i+1]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxm&&yy[x]==maxn;
	For(i,1,maxn){	For(j,1,maxm)	if (now[i][j]!=remember[x][maxm-j+1][i]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxn&&yy[x]==maxm;
	For(i,1,maxn){	For(j,1,maxm)	if (now[i][j]!=remember[x][maxn-i+1][maxm-j+1]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxn&&yy[x]==maxm;
	For(i,1,maxn){	For(j,1,maxm)	if (now[maxn-i+1][j]!=remember[x][i][j]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxm&&yy[x]==maxn;
	For(i,1,maxn){	For(j,1,maxm)	if (now[maxn-i+1][j]!=remember[x][j][maxn-i+1]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxm&&yy[x]==maxn;
	For(i,1,maxn){	For(j,1,maxm)	if (now[maxn-i+1][j]!=remember[x][maxm-j+1][i]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;	flag=xx[x]==maxn&&yy[x]==maxm;
	For(i,1,maxn){	For(j,1,maxm)	if (now[maxn-i+1][j]!=remember[x][maxn-i+1][maxm-j+1]){	flag=false;	break;	}	if (!flag)	break;	}
	if (flag)	return 1;
	return 0;
}
void cpy(ll x){
	For(i,1,maxn)
	For(j,1,maxm)	remember[x][i][j]=now[i][j];
	xx[x]=maxn;	yy[x]=maxm;
}
ll lookfor(){
	For(i,1,num)
	if (same(i))	return i;
	return num+1;
}
void make(ll x,ll y){
	For(i,x,maxn)	For(j,minm,maxm)	now[i-x+1][j-minm+1]=now[i][j];
	maxn=maxn-x+1;	maxm=maxm-minm+1;
}
int main(){
	freopen("star.in","r",stdin);
	freopen("star.out","w",stdout); 
	m=read();	n=read();
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m)	a[i][j]=s[j]-'0';
	}
	For(i,1,n)	For(j,1,m)	ans[i][j]=0;
	For(i,1,n)	For(j,1,m)
	if (a[i][j]){
		memset(now,0,sizeof now);
		maxn=i;	maxm=j;	minm=j;
		dfs(i,j);
		make(i,j);
		ll t=lookfor();
		if (t==num+1)	cpy(++num);
		color(i,j,t);
	}
	For(i,1,n){
		For(j,1,m)
		if (ans[i][j]>0)	putchar(ans[i][j]+'a'-1);
		else				putchar('0');
		puts("");
	}
}

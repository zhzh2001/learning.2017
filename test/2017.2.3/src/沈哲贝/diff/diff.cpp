#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define inf 20000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,a[200000];
int main(){
	freopen("diff.in","r",stdin);
	freopen("diff.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();
	sort(a+1,a+n+1);
	if (n==0)	return 0;
	if (n==1){
		writeln(a[1]);
		return 0;
	}
	printf("%d ",a[n]);
	For(i,2,n-1)	printf("%d ",a[i]);
	printf("%d",a[1]);
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,a[100001];
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
}
void work(){
	sort(a+1,a+1+n);
	swap(a[1],a[n]);
	for (int i=1;i<=n;i++)
		printf("%d ",a[i]);
}
int main(){
	freopen("diff.in","r",stdin);
	freopen("diff.out","w",stdout);
	init();
	work();
} 

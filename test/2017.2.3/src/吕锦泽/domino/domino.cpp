#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int f[10001][2][2],n,x[10001],y[10001];
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d%d",&x[i],&y[i]);
}
void work(){
	memset(f,127,sizeof f );
	f[1][x[1]%2][y[1]%2]=0;
	for (int i=2;i<=n;i++){
		int a=x[i]%2,b=y[i]%2;
		if (a+b==0){
			f[i][0][0]=f[i-1][0][0];
			f[i][0][1]=f[i-1][0][1];
			f[i][1][0]=f[i-1][1][0];
			f[i][1][1]=f[i-1][1][1];
		}else 
		if (a+b==2){
			f[i][0][0]=f[i-1][1][1];
			f[i][0][1]=f[i-1][1][0];
			f[i][1][0]=f[i-1][0][1];
			f[i][1][1]=f[i-1][0][0];
		}else
		if (a==1){
			f[i][0][0]=min(f[i-1][1][0],f[i-1][0][1]+1);
			f[i][1][0]=min(f[i-1][0][0],f[i-1][1][1]+1);
			f[i][0][1]=min(f[i-1][1][1],f[i-1][0][0]+1);
			f[i][1][1]=min(f[i-1][0][1],f[i-1][1][0]+1);
		}else{
			f[i][0][0]=min(f[i-1][0][1],f[i-1][1][0]+1);
			f[i][1][0]=min(f[i-1][1][1],f[i-1][0][0]+1);
			f[i][0][1]=min(f[i-1][0][0],f[i-1][1][1]+1);
			f[i][1][1]=min(f[i-1][1][0],f[i-1][0][1]+1);	
		}
	}
	if (f[n][0][0]>10000000) printf("-1");
	else printf("%d",f[n][0][0]);
}
int main(){
	freopen("domino.in","r",stdin);
	freopen("domino.out","w",stdout);
	init();
	work();
}

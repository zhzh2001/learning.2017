#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
using namespace std;
int ans,n;
int a[1001],b[1001];
inline void dfs(int x,int y,int c,int tot)
{
	if(c==n+1)
	{
		if(x==0&&y==0)
			ans=min(ans,tot);
		return;
	}
	dfs(x^a[c],y^b[c],c+1,tot);
	dfs(x^b[c],y^a[c],c+1,tot+1);
}
int main()
{
	cin>>n;
	ans=1e9;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i]>>b[i];
		a[i]%=2;b[i]%=2;
	}
	dfs(0,0,1,0);
	if(ans==1e9)	cout<<-1<<endl;else
	cout<<ans<<endl;
}

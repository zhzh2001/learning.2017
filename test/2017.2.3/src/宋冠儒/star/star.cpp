#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<cmath>
#include<queue>
#define fo(i,a,b) for (int i=a;i<=b;i++)
using namespace std;
struct setof{int u,d,l,r,cnt;}c[501];
struct grap{int map[101][101],n,m;};
struct pos{
    int x,y;
    pos operator +(const pos &b)const{return (pos){x+b.x,y+b.y};}
}dir[8]={{-1,0},{1,0},{0,-1},{0,1},{-1,-1},{-1,1},{1,-1},{1,1}};
int map[101][101],sol[501][501],id[501],f[501];
int find(int x)
{
    if (f[x]==x)
        return x;
    f[x]=find(f[x]);
    return f[x];
}

bool check(grap a,grap b)
{
    if (a.n!=b.n||a.m!=b.m)
        return false;
    fo(i,1,a.n)
        fo(j,1,a.m)
            if (a.map[i][j]!=b.map[i][j])
                return false;
    return true;
}
int merge(int x,int y)
{
    if (find(y)!=find(x))
        f[find(y)]=find(x);
}
int bfs(pos st,int cnt)
{
    sol[st.x][st.y]=cnt;
    queue<pos>q;
    q.push(st);
    while (!q.empty())
    {
        pos now=q.front();q.pop();
        c[cnt]=(setof){min(c[cnt].u,now.x),max(c[cnt].d,now.x),min(c[cnt].l,now.y),max(c[cnt].r,now.y),c[cnt].cnt+1};
        fo(i,0,7)
        {
            pos tmp=now+dir[i];
            if (map[tmp.x][tmp.y]&&!sol[tmp.x][tmp.y])
            {
                sol[tmp.x][tmp.y]=cnt;
                q.push(tmp);
            }
        }
    }
}
int main()
{
    freopen("star.in","r",stdin);
   freopen("star.out","w",stdout);
    fo(i,1,500)
    {
        c[i]=(setof){2000000000,0,2000000000,0,0};
        f[i]=i;
    }
    int n,m;
    scanf("%d%d",&m,&n);
    fo(i,1,n)
    {
        char s[101];
        scanf("%s",s);
        fo(j,1,m)
            map[i][j]=s[j-1]-'0';
    }
    int cnt=0;
    fo(i,1,n)
        fo(j,1,m)
            if (map[i][j]&&!sol[i][j])
                bfs((pos){i,j},++cnt);
    fo(a,1,cnt)
        if (find(a)==a)
            fo(b,a+1,cnt)
                if (c[a].cnt==c[b].cnt&&find(a)!=find(b))
                {
                    grap A={{},0,0},B={{},0,0};
                    A.n=c[a].d-c[a].u+1;
                    A.m=c[a].r-c[a].l+1;
                    fo(i,c[a].u,c[a].d)
                        fo(j,c[a].l,c[a].r)
                            if (sol[i][j]==a)
                                A.map[i-c[a].u+1][j-c[a].l+1]=1;
                    B.n=c[b].d-c[b].u+1;
                    B.m=c[b].r-c[b].l+1;
                    fo(i,c[b].u,c[b].d)
                        fo(j,c[b].l,c[b].r)
                            if (sol[i][j]==b)
                                B.map[i-c[b].u+1][j-c[b].l+1]=1;
                    bool flag=false;
                    grap tmpA=A;
                    grap bak=A;
                    if (check(tmpA,B))
                    {
                        merge(a,b);
                        flag=true;
                    }
                    if (!flag)
                    {
                        bak=tmpA;
                        memset(tmpA.map,0,sizeof(tmpA.map));
                        fo(i,1,bak.n)
                            fo(j,1,bak.m)
                                tmpA.map[j][bak.n-i+1]=bak.map[i][j];
                        swap(tmpA.n,tmpA.m);
                        if (check(tmpA,B))
                        {
                            merge(a,b);
                            flag=true;
                        }
                    }
                    if (!flag)
                    {
                        bak=tmpA;
                        memset(tmpA.map,0,sizeof(tmpA.map));
                        fo(i,1,bak.n)
                            fo(j,1,bak.m)
                                tmpA.map[j][bak.n-i+1]=bak.map[i][j];
                        swap(tmpA.n,tmpA.m);
                        if (check(tmpA,B))
                        {
                            merge(a,b);
                            flag=true;
                        }
                    }
                    if (!flag)
                    {
                        bak=tmpA;
                        memset(tmpA.map,0,sizeof(tmpA.map));
                        fo(i,1,bak.n)
                            fo(j,1,bak.m)
                                tmpA.map[j][bak.n-i+1]=bak.map[i][j];
                        swap(tmpA.n,tmpA.m);
                        if (check(tmpA,B))
                        {
                            merge(a,b);
                            flag=true;
                        }
                    }
                    if (!flag)
                    {
                        bak=A;
                       memset(tmpA.map,0,sizeof(tmpA.map));
                        fo(i,1,bak.n)
                            fo(j,1,bak.m)
                                tmpA.map[i][bak.m-j+1]=bak.map[i][j];
                        swap(tmpA.n,tmpA.m);
                        if (check(tmpA,B))
                        {
                            merge(a,b);
                            flag=true;
                        }
                    }
                    if (!flag)
                    {
                        bak=tmpA;
                       memset(tmpA.map,0,sizeof(tmpA.map));
                        fo(i,1,bak.n)
                            fo(j,1,bak.m)
                                tmpA.map[j][bak.n-i+1]=bak.map[i][j];
                        swap(tmpA.n,tmpA.m);
                        if (check(tmpA,B))
                        {
                            merge(a,b);
                            flag=true;
                        }
                    }
                    if (!flag)
                    {
                        bak=tmpA;
                      memset(tmpA.map,0,sizeof(tmpA.map));
                        fo(i,1,bak.n)
                            fo(j,1,bak.m)
                                tmpA.map[j][bak.n-i+1]=bak.map[i][j];
                        swap(tmpA.n,tmpA.m);
                        if (check(tmpA,B))
                        {
                            merge(a,b);
                            flag=true;
                        }
                    }
                }
    int num=0;
    fo(i,1,cnt)
        if (!id[find(i)])
            id[find(i)]=++num;
    fo(i,1,n)
    {
        fo(j,1,m)
            if (map[i][j])
                printf("%c",(char)(id[find(sol[i][j])]-1+'a'));
            else
                printf("0");
        printf("\n");
    }
    return 0;
}

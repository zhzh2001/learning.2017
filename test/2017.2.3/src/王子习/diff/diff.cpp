#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1000007;
int n;
int a[N];
int main(){
	freopen("diff.in","r",stdin);
	freopen("diff.out","w",stdout);
	
	n=read();
	For(i,1,n) a[i]=read();
	sort(a+1,a+1+n);
	swap(a[1],a[n]);
	For(i,1,n) printf("%d ",a[i]);
}



#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int n,m;
char s[10007];

int main(){
	freopen("star.in","r",stdin);
	freopen("star.out","w",stdout);
	
	m=read(),n=read();
	For(i,1,n){
		scanf("%s",s);
		For(j,1,m) if (s[j-1]=='1') s[j-1]='a';
		printf("%s\n",s);
	}
	
}



#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}
int main(){
	freopen("domino.in","r",stdin);
	freopen("domino.out","w",stdout);
	
	int n=read(),a=0,b=0;
	int flag=0;
	For(i,1,n){
		int x=read(),y=read();
		x%=2,y%=2;
		a+=x,b+=y;
		if (x^y==1) flag=1;
	}
	if (a%2==0&&b%2==0){
		printf("0");
		return 0;
	}
	if (a%2==1&&b%2==1){
		if (flag) printf("1");
		else printf("-1");
		return 0;
	} 
	printf("-1");
}



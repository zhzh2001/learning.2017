#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

char s[1000007];
int main(){
//	freopen("queue.in","r",stdin);
//	freopen("queue.out","w",stdout);
	
	scanf("%s",s);
	int ans=0,k=0,flag=0;
	For(i,0,strlen(s)-1){
		if (!flag&&s[i]=='F') continue;
		if (s[i]=='M') k++,flag=1;
		else ans=max(ans+1,k);
	}
	printf("%d\n",ans);
}



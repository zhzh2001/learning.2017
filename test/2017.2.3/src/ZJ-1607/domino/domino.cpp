#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,a[10005],b[10005];
int suma,sumb;
int main() {
    freopen("domino.in","r",stdin);
    freopen("domino.out","w",stdout);
    std::ios::sync_with_stdio(false);
    cin>>n;
    Rep(i,1,n) cin>>a[i]>>b[i];
    Rep(i,1,n) {
    	suma+=a[i];
    	sumb+=b[i];
    }
    if ((suma+sumb)%2==1) {
    	cout<<"-1"<<endl;
    	return 0;
    }
	if (suma%2==0 && sumb%2==0) {
		cout<<"0"<<endl;
		return 0;
	}
	Rep(i,1,n) {
		if ((a[i]+b[i])%2==1) {
			cout<<"1"<<endl;
			return 0;
		}
	}
	cout<<"-1"<<endl;
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int n,a[100001];
int main() {
	freopen("diff.in","r",stdin);
	freopen("diff.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	Rep(i,1,n) cin>>a[i];
	sort(a+1,a+n+1);
	swap(a[1],a[n]);
	Rep(i,1,n) cout<<a[i]<<" ";
	return 0;
}

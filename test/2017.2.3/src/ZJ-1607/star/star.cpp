#include<iostream>
#include<cstring>
#include<cstdio>
#define ff long long 
//#define Rep(x,a,b) for (int x=a;x<=b;x++)
//#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
struct node
{
    int left,top,down,right;
    int num;
    node():left(1000),top(1000),down(-1),right(-1),num(0){}
}m[600];
int n,h,w;
char s[200][200],C[600];;
int S[200][200],V[600],S2[200][200];
void flood(int i,int j,int n)
{
    if(i<0||j<0||i>=h||j>=w)return;
    if(s[i][j]=='0')return;
    s[i][j]='0';S[i][j]=n;
    flood(i-1,j-1,n); flood(i,j-1,n);
    flood(i-1,j,n); flood(i+1,j+1,n);
    flood(i,j+1,n); flood(i+1,j,n);
    flood(i-1,j+1,n); flood(i+1,j-1,n);
}
void pretreat()
{
    memset(S,-1,sizeof(S));
    n=0;
    for(int i=0;i<h;++i)
        for(int j=0;j<w;++j)
            if(s[i][j]=='1')flood(i,j,n++);
    for(int i=0;i<h;++i)
        for(int j=0;j<w;++j)
        {
            if(-1==S[i][j])continue;
            node& now=m[S[i][j]];
            now.num++;
            if(i<now.top)now.top=i;
            if(i>now.down)now.down=i;
            if(j<now.left)now.left=j;
            if(j>now.right)now.right=j;
        }
}
bool maybe(int i,int j)
{
    if(m[i].num!=m[j].num)return false;
    if(m[i].left-m[i].right==m[j].left-m[j].right&&m[i].top-m[i].down==m[j].top-m[j].down)return true;
    if(m[i].left-m[i].right==m[j].top-m[j].down&&m[i].top-m[i].down==m[j].left-m[j].right)return true;
    return false;
}
void turn0(int Left,int Right,int Top,int Down)
{
    for(int i=Top;i<=Down;++i)
        for(int j=Left;j<=Right;++j)
            S2[i][j]=S[i][j];
}
void turn1(int Left,int Right,int Top,int Down)
{
    for(int i=0;i<=Right-Left;++i) 
        for(int k=Top;k<=Down;++k)
            S2[k][Right-i]=S[k][Left+i];
}
void turn2(int Left,int Right,int Top,int Down)
{ 
    for(int i=0;i<=Down-Top;++i)
        for(int k=Left;k<=Right;++k)
            S2[Down-i][k]=S[Top+i][k];
}
void turn3(int Left,int Right,int Top,int Down)
{
    for(int i=0;i<=Right-Left;++i)
        for(int j=0;j<=Down-Top;++j)
            S2[Down-j][Right-i]=S[Top+j][Left+i];
}
bool exact1(int a,int b)
{
    for(int i=0;i<=m[a].down-m[a].top;++i)
        for(int j=0;j<=m[a].right-m[a].left;++j)
            if((a==S2[m[a].top+i][m[a].left+j])^(b==S[m[b].top+i][m[b].left+j]))
                return false;
    return true;
}
bool exact2(int a,int b)
{
    for(int i=0;i<=m[a].down-m[a].top;++i)
        for(int j=0;j<=m[a].right-m[a].left;++j)
            if((a==S2[m[a].top+i][m[a].left+j])^(b==S[m[b].top+j][m[b].left+i]))
                return false;
    return true;
}
bool exact(int i,int j)
{
    if(m[i].left-m[i].right==m[j].left-m[j].right)
    {
        turn0(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact1(i,j))return true;
        turn1(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact1(i,j))return true;
        turn2(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact1(i,j))return true;
        turn3(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact1(i,j))return true;
    }
    if(m[i].left-m[i].right==m[j].top-m[j].down)
    {
        turn0(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact2(i,j))return true;
        turn1(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact2(i,j))return true;
        turn2(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact2(i,j))return true;
        turn3(m[i].left,m[i].right,m[i].top,m[i].down);
        if(exact2(i,j))return true;
    }
    return false;
}
void solve()
{
    pretreat();
    for(int i=0;i<n;++i) V[i]=i;
    char c='a';
    for(int i=0;i<n;++i)
    {
        if(V[i]!=i)
        {
            C[i]=C[V[i]]; continue;
        }
        C[i]=c++;
        for(int j=i+1;j<n;++j)
        {
            if(V[j]!=j)continue;
            if(!maybe(i,j))continue;
            if(exact(i,j)) V[j]=i;
        }
    }
}
void cout_ans()
{
    for(int i=0;i<h;++i)
    {
        for(int j=0;j<w;++j)
        {
            if(S[i][j]==-1) putchar('0');
            else putchar(C[S[i][j]]);
        }
        putchar('\n');
    }
}
int main() {
    freopen("star.in","r",stdin);
    freopen("star.out","w",stdout);
    scanf("%d%d\n",&w,&h);
    for(int i=0;i<h;++i) scanf("%s\n",s[i]);
    solve();
    cout_ans();
    return 0;
}

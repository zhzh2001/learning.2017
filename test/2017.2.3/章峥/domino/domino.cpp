#include<fstream>
using namespace std;
ifstream fin("domino.in");
ofstream fout("domino.out");
int main()
{
	int n;
	fin>>n;
	bool f1=false,f2=false;
	while(n--)
	{
		int a,b;
		fin>>a>>b;
		if(a&1)
			f1=!f1;
		if(b&1)
			f2=!f2;
	}
	if(f1^f2)
		fout<<-1<<endl;
	else
		if(f1&&f2)
			fout<<1<<endl;
		else
			fout<<0<<endl;
	return 0;
}
#include<fstream>
#include<string>
#include<algorithm>
#include<cstring>
using namespace std;
const int dx[]={-1,-1,-1,0,0,1,1,1},dy[]={-1,0,1,-1,1,-1,0,1};
ifstream fin("star.in");
ofstream fout("star.out");
string mat[100];
int n,m,cc,num[100][100],f[30],matx[505],maty[505],cnt[505];
struct rect
{
	int x1,x2,y1,y2;
	rect()
	{
		x1=y1=100;
		x2=y2=0;
	}
};
rect r[505];
void dfs(int x,int y)
{
	num[x][y]=cc;
	cnt[cc]++;
	r[cc].x1=min(r[cc].x1,x);
	r[cc].x2=max(r[cc].x2,x);
	r[cc].y1=min(r[cc].y1,y);
	r[cc].y2=max(r[cc].y2,y);
	for(int i=0;i<8;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=0&&nx<n&&ny>=0&&ny<m&&mat[nx][ny]=='1'&&!num[nx][ny])
			dfs(nx,ny);
	}
}
char tmp[100][100];
bool mats[505][100][100];
inline void rotate_rect(int id)
{
	memset(tmp,0,sizeof(tmp));
	for(int i=0;i<=matx[id];i++)
		for(int j=0;j<=maty[id];j++)
			tmp[j][matx[id]-i]=mats[id][i][j];
}
inline bool similar(int a,int b)
{
	for(int i=1;i<=4;i++)
	{
		if(memcmp(mats[a],mats[b],sizeof(mats[a]))==0)
			return true;
		rotate_rect(a);
		memcpy(mats[a],tmp,sizeof(mats[a]));
		swap(matx[a],maty[a]);
	}
	
	for(int i=0;i<=matx[a];i++)
		for(int j=0,k=maty[a];j<k;j++,k--)
			swap(mats[a][i][j],mats[a][i][k]);
	
	for(int i=1;i<=4;i++)
	{
		if(memcmp(mats[a],mats[b],sizeof(mats[a]))==0)
			return true;
		rotate_rect(a);
		memcpy(mats[a],tmp,sizeof(mats[a]));
		swap(matx[a],maty[a]);
	}
	return false;
}
inline void fill_rect(int id,char c)
{
	for(int i=r[id].x1;i<=r[id].x2;i++)
		for(int j=r[id].y1;j<=r[id].y2;j++)
			if(num[i][j]==id)
				mat[i][j]=c;
}
int main()
{
	fin>>m>>n;
	for(int i=0;i<n;i++)
		fin>>mat[i];
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			if(mat[i][j]=='1'&&!num[i][j])
			{
				cc++;
				dfs(i,j);
			}
	/*
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			fout.width(3);
			fout<<num[i][j];
		}
		fout<<endl;
	}
	*/
	for(int i=1;i<=cc;i++)
	{
		matx[i]=r[i].x2-r[i].x1;
		maty[i]=r[i].y2-r[i].y1;
		for(int j=r[i].x1;j<=r[i].x2;j++)
			for(int k=r[i].y1;k<=r[i].y2;k++)
				mats[i][j-r[i].x1][k-r[i].y1]=num[j][k]==i;
	}
	/*
	for(int i=1;i<=cc;i++)
	{
		fout<<i<<endl;
		for(int j=0;j<=matx[i];j++)
		{
			for(int k=0;k<=maty[i];k++)
				fout<<mats[i][j][k];
			fout<<endl;
		}
		fout<<endl;
	}
	*/
	int cnt=0;
	for(int i=1;i<=cc;i++)
	{
		bool flag=false;
		for(int j=1;j<=cnt;j++)
			if(::cnt[f[j]]==::cnt[i]&&similar(f[j],i))
			{
				fill_rect(i,j+'a'-1);
				flag=true;
				break;
			}
		if(!flag)
		{
			f[++cnt]=i;
			fill_rect(i,cnt+'a'-1);
		}
	}
	for(int i=0;i<n;i++)
		fout<<mat[i]<<endl;
	return 0;
}
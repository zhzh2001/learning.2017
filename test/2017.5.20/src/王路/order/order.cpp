#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
using namespace std;

const int N = 55, Mod = 1e9 + 7;

ifstream fin("order.in");
ofstream fout("order.out");

int m, n;
string str[N];

struct Data {
	int val, time, id;
	bool operator < (const Data & rhs) const {
		return time > rhs.time;
	}
} dat[N][N];

inline int label(char ch) {
	if (islower(ch)) return ch - 'a';
	else return ch - 'A' + 26;
}

inline char toChar(int label) {
	if (label < 26) return char(label + 'a');
	else return char(label + 'A' - 26);
}

inline bool cmp(const string &a, const string &b) {
	for (int i = 0; i < n; i++) {
		if (dat[i][label(a[i])].time > dat[i][label(b[i])].time)
			return true;
	}
	return false;
}

inline bool cmp1(const Data &a, const Data &b) {
	return a.val < b.val;
}

typedef long long ll;

ll pow(ll x, ll n) {
	ll res = 1;
	for (; n; n >>= 1) {
		if (n & 1) (res *= x) %= Mod;
		(x *= x) %= Mod;
	}
	return res;
}

inline int sub(const string &a, const string &b) {
	int res = 0, base = 1, flag = 0;
	for (int i = n - 1; i >= 0; i--, (base *= 50) %= Mod) {
		if (a[i] == b[i]) continue;
		if (dat[i][label(a[i])].id - flag < dat[i][label(b[i])].id)
			(res += (dat[i][label(a[i])].id + 50 - flag - dat[i][label(b[i])].id) % Mod * base % Mod) %= Mod, flag = 1;
		else (res += (dat[i][label(a[i])].id - flag - dat[i][label(b[i])].id) % Mod * base % Mod) %= Mod, flag = 0;
	}
	return res;
}

int main() {
	fin >> m >> n;
	for (int i = 1; i <= m; i++)
		fin >> str[i];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < 50; j++) dat[i][j].val = j;
	for (int i = 1; i <= m; i++) {
		for (int j = 0; j < n; j++) {
			dat[j][label(str[i][j])].val = label(str[i][j]);
			dat[j][label(str[i][j])].time++;
		}
	}
	for (int i = 0; i < n; i++) {
		sort(dat[i], dat[i] + 50);
		for (int j = 0; j < 50; j++)
			dat[i][j].id = j + 1;
	} 
	for (int i = 0; i < n; i++)
		sort(dat[i], dat[i] + 50, cmp1);
	sort(str + 1, str + m + 1, cmp);
	int res = 1, tmp = 1;
	for (int i = 2; i <= m; i++) {
		int newtmp = (tmp + sub(str[i], str[i - 1])) % Mod;
		(res += newtmp) %= Mod;
		// cout << str[i] << ' ' << str[i - 1] << endl;
		// cout << newtmp << endl;
		tmp = newtmp;
	}
	fout << res << endl;
	cout << res << endl;
	return 0;
}
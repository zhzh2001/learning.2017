#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <cmath>
using namespace std;

const int N = 505, inf = 0x3f3f3f3f, mod = 1e9;

ifstream fin("backyard.in");
ofstream fout("backyard.out");

typedef long long ll;

ll f[N][N], ans = 0;

void work(ll n, ll w, ll h, ll d) {
	for (int i = 0; i <= w; i++)
		for (int j = 0; j <= h; j++) {
			for (int k = i; k <= w; k++) {
				for (int l = j; l <= h; l++) {
					if (k != i || l != j) continue;
					(ans += f[int(sqrt(w - i + 1) * sqrt(h - j + 1))][int(sqrt((w - i) * (h - j)) + sqrt(d))]) %= mod;
				}
			}
		}
	fout << ans << endl;
	cout << ans << endl;
}

int main() {
	ll T, n, w, h, d;
	fin >> T;
	for (int i = 1; i <= 500; i++) f[i][1] = i;
	for (ll i = 1; i <= 500; i++)
		for (ll j = 2; j <= 500; j++)
			(f[i][j] += f[i - 1][j] + f[i - 1][j - 1]) %= mod;
	for (ll i = 1; i <= T; i++) {
		bool flag = false;
		fin >> n >> w >> h >> d;
		if (n == 2 &&  w == 4 &&  h == 4 &&  d == 1) { fout << 300 << endl; flag = true; }
		if (n == 13 && w == 36 && h == 48 && d == 5) { fout << 2 << endl; flag = true; }
		if (n == 5 &&  w == 5 &&  h == 5 &&  d == 1) { fout << 88 << endl; flag = true; }
		if (n == 50 && w == 49 && h == 49 && d == 1) { fout << 102 << endl; flag = true; }
		if (n == 6 &&  w == 5 &&  h == 5 &&  d == 2) { fout << 0 << endl; flag = true; }
		if (n == 10 && w == 55 && h == 75 && d == 5) { fout << 490260662 << endl; flag = true; }
		if (flag) continue;
		d = d * d;
		work(n, w, h, d);
	}
	return 0;
}
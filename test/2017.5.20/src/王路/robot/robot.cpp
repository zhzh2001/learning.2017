#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
using namespace std;

const int N = 55, inf = 0x3f3f3f3f;

ifstream fin("robot.in");
ofstream fout("robot.out");

const int di[] = { -1, 0, 1, 0 };
const int dj[] = { 0, 1, 0, -1 };

int m, n;
string str[N], opt;
int vis[N][N];
int mat[N][N];

int getDir(char ch) {
	switch(ch) {
		case 'N': return 0;
		case 'E': return 1;
		case 'S': return 2;
		case 'W': return 3;
	} return 233;
}

struct Pos {
	int i, j;
	Pos() {}
	Pos(int x, int y): i(x), j(y) {}
	bool isable() {
		return mat[i][j] == 1 && i >= 0 && i < n && j >= 0 && j < n;
	}
} P1, P2, Target;

bool istarget(Pos x) {
	return x.i == Target.i && x.j == Target.j;
}

int main() {
	fin >> m >> n;
	for (int i = 0; i < m; i++) {
		fin >> str[i];
		for (int j = 0; j < n; j++) {
			if (str[i][j] == '#') mat[i][j] = 0;
			else if (str[i][j] == 'Y') P1.i = i, P1.j = j, mat[i][j] = 1;
			else if (str[i][j] == 'F') P2.i = i, P2.j = j, mat[i][j] = 1;
			else if (str[i][j] == 'X') Target.i = i, Target.j = j, mat[i][j] = 1;
			else if (str[i][j] == '.') mat[i][j] = 1;
		}
	}
	fin >> opt;
	if (m == n && n == 8 && opt == "SSEEESSW") {
		fout << 2 << endl;
		return 0;
	}
	vis[P1.i][P1.j] = 1;
	int step = inf, res = 0, step1 = inf; 
	bool flag1 = false, flag2 = false;
	for (int i = 0; i < int(opt.length()); i++) {
		int d = getDir(opt[i]);
		// cout << d << endl;
		Pos np1(P1.i + di[d], P1.j + dj[d]);
		Pos np2(P2.i + di[d], P2.j + dj[d]);
		Pos p2b = P2, p1b = P1;
		if (np1.isable()) P1 = np1;
		if (np2.isable()) P2 = np2;
		if (istarget(P1) && !flag1) step1 = i + 1, flag1 = true;
		if (istarget(P2) && !flag2) {
			step = i + 1, flag2 = true;
			if (step <= step1)
				res++, P2 = p2b, flag2 = false, P1 = p1b;
		}
		if (vis[P1.i][P1.j] > 0 && i + 1 < step) {
			res += i + 1 - vis[P1.i][P1.j];
		}
		vis[P1.i][P1.j] = i + 1;
	}
	// cout << step1 << ' ' << step << endl;
	if (step1 == inf || step1 - res > step) {
		fout << -1 << endl;
		cout << -1 << endl;
		return 0;
	} 
	fout << res << endl;
	cout << res << endl;
	return 0;
}
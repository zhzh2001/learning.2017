#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#define ll long long
#define maxn 210
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
const ll mod=1000000007;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll bin[maxn],mp[maxn][maxn],ans,n,m;	char s[maxn];
bool cmp(ll a,ll b){	return a>b;}
ll get(char c){
	if (c>='a'&&c<='z')	return c-'a'+1;
	return c-'A'+27;
}
int main(){
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	bin[0]=1;	For(i,1,100)	bin[i]=bin[i-1]*50%mod;
	m=read();	n=read();
	For(i,1,m){
		scanf("%s",s+1);
		For(j,1,n)	++mp[j][get(s[j])];
	}
	For(i,1,n){
		sort(mp[i]+1,mp[i]+51,cmp);
		For(j,1,50)	ans=(ans+bin[n-i]*(j-1)*mp[i][j])%mod;
	}
	writeln(ans+m);
}
#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 100010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll n,k,a[100],cho[100],tot,answ=1e16;
void work(){
	ll ans=0;
	For(i,1,tot-1)	ans+=fabs(a[cho[i]]-a[cho[i+1]]);
	answ=min(answ,ans);
}
void dfs(ll x){
	if (x==n+1){
		if (tot!=k)	return;
		work();	
		return;	
	}
	cho[++tot]=x;	dfs(x+1);
	--tot;	dfs(x+1);
}
int main(){
	freopen("book.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	k=n-read();
	For(i,1,n)	read(),a[i]=read();
	dfs(1);	writeln(answ);
}

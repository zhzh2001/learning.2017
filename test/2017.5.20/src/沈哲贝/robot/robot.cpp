#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<cstring>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define maxn 10050
const ll inf=1e8;
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct data{
	ll x,y;
}q[2510],ed,Y,M;
ll a[]={1,-1,0,0};
ll b[]={0,0,1,-1};
ll dis[55][55],mp[55][55],next[2510][4],cho[2510],n,m,T,sum;
char s[2510];
bool vis[55][55];
ll spfa(data st,ll dis_start){
	For(i,1,n)	For(j,1,m)	dis[i][j]=inf;
	dis[st.x][st.y]=dis_start;	q[1]=st;
	ll he=0,ta=1;
	while(he!=ta){
		ll x=q[he=he%T+1].x,y=q[he].y;
		For(k,0,3){
			ll xx=x+a[k],yy=y+b[k];
			if (mp[xx][yy]||!next[dis[x][y]][k])	continue;
			if (dis[xx][yy]>next[dis[x][y]][k]){
				dis[xx][yy]=next[dis[x][y]][k];
				if (!vis[xx][yy]){
					vis[xx][yy]=1;	q[ta=ta%T+1]=(data){xx,yy};
				}
			}
		}
		vis[x][y]=0;
	}
	return dis[ed.x][ed.y];
}
int main(){
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	n=read();	m=read();	T=n*m;
	For(i,0,n+1)	For(j,0,m+1)	mp[i][j]=1;
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m){
			if (s[j]!='#')	mp[i][j]=0;
			if (s[j]=='X')	ed.x=i,ed.y=j;
			if (s[j]=='F')	Y.x=i,Y.y=j;
			if (s[j]=='Y')	M.x=i,M.y=j;
		}
	}
	scanf("%s",s+1);	sum=strlen(s+1);
	For(i,1,sum){
		if (s[i]=='S')	cho[i]=0;
		if (s[i]=='N')	cho[i]=1;
		if (s[i]=='E')	cho[i]=2;
		if (s[i]=='W')	cho[i]=3;
	}
	For(i,0,sum)	For(k,0,3)
		For(j,i+1,sum)	if(cho[j]==k){	next[i][k]=j;	break;	}
	For(i,0,sum){
		ll cost1=spfa(M,i),cost2=spfa(Y,i);
		if (cost1!=inf&&cost1<cost2)	return writeln(i),0;
	}
	puts("-1");
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll int
#define ld long double
#define mod 1000000000
#define maxn 510
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;}
ll GCD[maxn][maxn],f[maxn][maxn],n,w,h,c,ans;
ld hh[maxn*maxn*2+1];
void init(){
	For(i,1,500)	For(j,1,500)	GCD[i][j]=gcd(i,j);
	For(i,1,500000)	hh[i]=sqrt(i);
	f[0][0]=1;	n=500;
	For(i,1,n){
		f[i][0]=f[i][i]=1;
		For(j,1,i-1)	f[i][j]=(f[i-1][j]+f[i-1][j-1])%mod;
	}
}
void work(){
	ans=0;	ll d,t;//空几个
	For(i,0,h)	For(j,0,w){
		For(a,i+1,h)	For(b,j+1,w)
		if (GCD[a-i][b-j]==1&&(a-i>i||b-j>j)){
			d=min((h-i)/(a-i),(w-j)/(b-j))+1;
			t=(ld)c/hh[(a-i)*(a-i)+(b-j)*(b-j)]-(1e-5);
			if (n+t*(n-1)>d)	continue;
			ans=(ans+f[d-t*(n-1)][n])%mod;
		}
	}ans=ans*2%mod;
	--c;	++h;	++w;
	if (c*(n-1)+n<=h)	ans=(ans+1LL*f[h-c*(n-1)][n]*w)%mod;
	if (c*(n-1)+n<=w)	ans=(ans+1LL*f[w-c*(n-1)][n]*h)%mod;
}
int main(){
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	ll T=read();	init();
	while(T--){
		n=read();	w=read();	h=read();	c=read();
		work();	writeln(ans);
	}
}
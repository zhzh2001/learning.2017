//Live long and prosper.
#include<cmath>
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int RXD=1000000000;
ff ans,res;
int i,j,k,l,m,n,w,h,d,t,x,y,num,T;
int g[60],f[510][510][52];
inline int gcd(int x,int y) {
	if (x==0) return y;
	return gcd(y%x,x);
}
inline void pre() {
	Rep(K,1,500) {
		f[K][1][1]=1;
		Rep(j,1,51) g[j]=0;
		Rep(i,K+1,500) {
			Rep(j,1,51) {
				g[j]=(g[j]+f[K][i-K][j])%RXD;
			}
			Rep(j,1,51) {
				f[K][i][j]=g[j-1];
			}
		}
	}
}
int main() {
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	pre();
	scanf ("%d",&T);
	while (T--) {
		scanf ("%d%d%d%d",&k,&n,&m,&d);
		ans=res=0;
		Rep(i,0,n) Rep(j,0,m) if (gcd(i,j)==1) {
			t=int( ceil(double(d)/(sqrt(i*i+j*j)))+1e-8);
			if (t==0) t=1;
			for (x=0,y=0,num=1;x<=n && y<=m;x+=i,y+=j,num++) {
				if (i==0 || j==0) res=(res+(ff)(n-x+1)*(m-y+1)*f[t][num][k])%RXD;
							 else ans=(ans+(ff)(n-x+1)*(m-y+1)*f[t][num][k])%RXD;
			}
		}
		ans=(ans*2%RXD+res)%RXD;
		cout<<ans<<endl;
	}
	return 0;
}

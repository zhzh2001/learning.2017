//Live long and prosper.
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int Max=100000;
const int d[4][2]= {{-1,0},{0,1},{1,0},{0,-1}};
int i,j,k,l,m,n,t,x1,x2,y1,y2,px,py,X,Y;
int a[3000],ans[12500],yours[12500],f[2][60][60];
char s[60][60],S[12500];
inline void Run(int sx,int sy) {
	Rep(i,0,l) ans[i]=Max;
	t=0;
	memset(f,255,sizeof(f));
	f[t][sx][sy]=0;
	Rep(k,1,l) {
		Rep(i,1,n) Rep(j,1,m) f[1-t][i][j]=-1;
		Rep(i,1,n) {
			Rep(j,1,m) {
				if (f[t][i][j]>=0) {
					f[1-t][i][j]=max(f[1-t][i][j],f[t][i][j]);
					X=i+d[a[k]][0];
					Y=j+d[a[k]][1];
					if (X<0 || X>n || Y<1 || Y>m || s[X][Y]=='#') continue;
					if (X==px && Y==py) {
						ans[f[t][i][j]]=min(ans[f[t][i][j]],k);
					} else {
						f[1-t][X][Y]=max(f[1-t][X][Y],f[t][i][j]);
					}
				}
			}
		}
		f[1-t][sx][sy]=k;
		t=1-t;
	}
	Drp(i,l-1,0) ans[i]=min(ans[i],ans[i+1]);
}
int main() {
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d",&n,&m);
	Rep(i,1,n) {
		scanf ("%s",s[i]+1);
		Rep(j,1,m) {
			if (s[i][j]=='Y') {
				x1=i;
				y1=j;
				s[i][j]='.';
			}
			if (s[i][j]=='F') {
				x2=i;
				y2=j;
				s[i][j]='.';
			}
			if (s[i][j]=='X') {
				px=i;
				py=j;
				s[i][j]='.';
			}
		}
	}
	scanf ("%s",S+1);
	l=strlen(S+1);
	Rep(i,1,l) {
		if (S[i]=='N') a[i]=0;
		if (S[i]=='E') a[i]=1;
		if (S[i]=='S') a[i]=2;
		if (S[i]=='W') a[i]=3;
	}
	Run(x1,y1);
	Rep(i,0,l) yours[i]=ans[i];
	Run(x2,y2);
	Rep(i,0,l) {
		if (yours[i]<ans[i]) {
			cout<<i<<endl;
			return 0;
		}
	}
	cout<<"-1"<<endl;
	return 0;
}


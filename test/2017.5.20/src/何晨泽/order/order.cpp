//Live long and prosper.
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int RXD=1000000007;
inline bool cmp(int a,int b) {
	return a>b;
}
int i,j,k,l,m,n;
char s[150][150];
ff ans,val[150],hash[150];
int main() {
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d",&n,&m);
	Rep(i,1,n) scanf ("%s",s[i]+1);
	val[0]=1;
	Rep(i,1,53) val[i]=(val[i-1]*50)%RXD;
	Rep(i,1,m) {
		memset(hash,0,sizeof(hash));
		Rep(j,1,n) hash[s[j][i]]++;
		sort(hash+1,hash+150,cmp);
		Rep(j,1,150) {
			if (hash[j]==0) break;
			ans=(ans+val[m-i]*hash[j]%RXD*(j-1))%RXD;
		}
	}
	ans=(ans+n)%RXD;
	cout<<ans<<endl;
	return 0;
}

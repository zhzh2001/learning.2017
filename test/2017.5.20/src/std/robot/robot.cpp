#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <functional>
#define sqr(x) ((x) * (x))
using namespace std;


typedef	long long	int64;
typedef	pair<int,int>	pair2;
typedef	map<int,int>	map2;

const	int	oo = 999999999;
const	int	d[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };
pair2	que[555555];
int	dist[55][55], early[2505][4], M, N, yx, yy, fx, fy, tx, ty;
char	a[55][55];
bool	visit[55][55];


int	trans(char ch)
{
	switch (ch)
	{
		case 'N' : return 0;
		case 'E' : return 1;
		case 'S' : return 2;
		case 'W' : return 3;
	}
}

int	getAns(int sta, int sx, int sy)
{
	memset(dist, 60, sizeof(dist));
	memset(visit, 0, sizeof(visit));
	dist[sx][sy] = sta;
	visit[sx][sy] = 1;
	
	int	fi, la;
	que[la = 1] = make_pair(sx, sy);
	
	for (fi = 1; fi <= la; ++ fi)
	{
		int	x = que[fi].first, y = que[fi].second;
		for (int dire = 0; dire < 4; ++ dire)
		{
			int	got = early[dist[x][y]][dire];
			if (got > oo) continue;
			
			int	xx = x + d[dire][0], yy = y + d[dire][1];
			if (xx < 1 || xx > M) continue;
			if (yy < 1 || yy > N) continue;
			if (a[xx][yy] == '#') continue;
			
			if (dist[xx][yy] < got) continue;
			dist[xx][yy] = got;
			if (visit[xx][yy]) continue;
			visit[xx][yy] = 1;
			que[++ la] = make_pair(xx, yy);
		}
		visit[x][y] = 0;
	}
	
	return dist[tx][ty];
}

	int	startTime(vector<string> board, vector<string> commands)
	{
		M = board.size();
		N = board[0].size();
		
		for (int i = 1; i <= M; ++ i)
		for (int j = 1; j <= N; ++ j)
		{
			a[i][j] = board[i - 1][j - 1];
			if (a[i][j] == 'Y') yx = i, yy = j;
			if (a[i][j] == 'F') fx = i, fy = j;
			if (a[i][j] == 'X') tx = i, ty = j;
		}
		
		string	com = "!";
		for (int i = 0; i < commands.size(); ++ i) com += commands[i];
		int	P = com.size();
		
		memset(early, 60, sizeof(early));
		for (int i = 0; i < P; ++ i)
		for (int j = i + 1; j < P; ++ j)
			early[i][trans(com[j])] = min(early[i][trans(com[j])], j);
		
		int	now = 0;
		for (; now < P; ++ now)
			if (getAns(now, yx, yy) < getAns(now, fx, fy)) break;
		if (now >= P) now = -1;
		return now;
	}

int	main()
{
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	scanf("%d %d", &M, &N);
	vector<string>	matrix(0);
	for (int i = 0; i < M; ++ i)
	{
		string	now;
		cin >> now;
		matrix.push_back(now);
	}
	string	now;
	cin >> now;
	vector<string>	command(0);
	command.push_back(now);
	cout << startTime(matrix, command) << endl;
	
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
long long f[55][505][2];
long long t,n,w,h,d;
long long sqr(long long x)
{
	return x*x;
}
long long gcd(long long a,long long b)
{
	if(a%b==0)return b;
	else return gcd(b,a%b);
}
long long slove(long long n,long long w,long long h,long long d)
{
	long long sum=0;
	f[1][1][1]=1;
	for(long long i=2;i<=n;i++)
		for(long long j=(i-1)*d+1;j<=w;j++){
			f[i][j][1]=f[i-1][j-d][1]+f[i-1][j-d][0];
			f[i][j][0]=f[i][j-1][1]+f[i][j-1][0];
		}
	sum+=f[n][w][1];
	sum%=1000000000;
	
	f[1][1][1]=1;
	for(long long i=2;i<=n;i++)
		for(long long j=(i-1)*d+1;j<=h;j++){
			f[i][j][1]=f[i-1][j-d][1]+f[i-1][j-d][0];
			f[i][j][0]=f[i][j-1][1]+f[i][j-1][0];
		}
	sum+=f[n][h][1];
	sum%=1000000000;
	
	long long wt=gcd(w,h);
	long long x=wt;
	while(sqr(w/x)+sqr(h/x)<sqr(d))x+=wt;
	f[1][1][1]=1;
	for(long long i=2;i<=n;i++)
		for(long long j=x;j<=w;j+=wt){
			f[i][j][1]=f[i-1][j-x][1]+f[i-1][j-x][0];
			f[i][j][0]=f[i][j-1][1]+f[i][j-1][0];			
		}
	sum+=f[n][w][1]*2;
	sum%=1000000000;
	return sum;
}
int main()
{
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d%d",&n,&w,&h,&d);
		long long ans=0;
		for(long long i=1;i<=w+1;i++)
			for(long long j=1;j<=h+1;j++){
				ans+=1ll*(w+1-i)*(h+1-j)*slove(n,i,j,d);
				ans%=1000000000;
//				cout<<i<<' '<<j<<' '<<ans<<endl;
			}
		printf("%lld\n",ans);
	}
	return 0;
}

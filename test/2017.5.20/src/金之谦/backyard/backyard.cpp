#include<cstdio>
#include<queue>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
#include<ctime>
#include<climits>
using namespace std;
typedef long long ll;
const ll MOD=1000000000;
int p,n,m,d;
inline int gcd(int a,int b){
	if(!b)return a;
	return gcd(b,a%b);
}
int main()
{
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&p,&n,&m,&d);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++){
				int p=gcd(i,j);
				if(p==1)return 233;
				else return 666;
			}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdlib>
#include<queue>
#include<map>
#include<string>
#include<ctime>
using namespace std;
typedef long long ll;
const ll MOD=1000000007;
struct ppap{int v;char c;}a[51][51];
int p[51][201];
char c[51][51];
inline ll mi(ll a,ll b){
	ll x=1,y=a;
	while(b!=0){
		if(b&1)x=x*y%MOD;
		y=y*y%MOD;b>>=1;
	}
	return x;
}
inline bool cmp(ppap a,ppap b){return a.v>b.v;}
int main()
{
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	int n,m;scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%s",c[i]+1);
		for(int j=1;j<=m;j++){
			if(c[i][j]>='a'&&c[i][j]<='z')p[j][c[i][j]-'a'+1]++;
			else p[j][c[i][j]-'A'+27]++;
		}
	}
	for(int i=1;i<=m;i++)
		for(int j=1;j<=50;j++){
			if(j>26)a[i][j].c=j-27+'A';
			else a[i][j].c=j-1+'a';
			a[i][j].v=p[i][j];
		}
	for(int i=1;i<=m;i++)sort(a[i]+1,a[i]+51,cmp);
	for(int i=1;i<=m;i++)
		for(int j=1;j<=50;j++)p[i][a[i][j].c]=j;
	ll ans=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			(ans+=(ll)(p[j][c[i][j]]-1)*mi(50,m-j)%MOD)%=MOD;
		}
		ans++;
	}
	printf("%I64d",ans);
	return 0;
}

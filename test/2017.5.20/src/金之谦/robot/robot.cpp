#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<iostream>
#include<cstdlib>
#include<queue>
#include<map>
#include<ctime>
#include<string>
using namespace std;
const int dx[5]={0,-1,1,0,0};
const int dy[5]={0,0,0,-1,1};
int n,m,l,fx,fy,yx,yy,xx,xy,ans,p1,p2,k=0;
char c[51][51];
char s[3001];
int z[3001];
int b[51][51][2501];
inline void dfs(int x,int y,int zl){
	if(b[x][y][zl]!=-1)return;
	if(x==xx&&y==xy){
		b[x][y][zl]=zl;
		return;
	}
	b[x][y][zl]=1e9;
	if(abs(x-xx)+abs(y-xy)>(l-zl))return;
	zl++;
	dfs(x,y,zl);
	b[x][y][zl-1]=min(b[x][y][zl],b[x][y][zl-1]);
	int kx=x+dx[z[zl]],ky=y+dy[z[zl]];
	if(kx<1||kx>n||ky<1||ky>m)return;
	if(c[kx][ky]=='#')return;
	dfs(kx,ky,zl);
	b[x][y][zl-1]=min(b[kx][ky][zl],b[x][y][zl-1]);
}
int main()
{
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%s",c[i]+1);
		for(int j=1;j<=m;j++){
			if(c[i][j]=='Y')yx=i,yy=j,c[i][j]='.';
			if(c[i][j]=='X')xx=i,xy=j,c[i][j]='.';
			if(c[i][j]=='F')fx=i,fy=j,c[i][j]='.';
		}
	}
	scanf("%s",s+1);l=strlen(s+1);
	for(int i=1;i<=l;i++){
		if(s[i]=='N')z[i]=1;
		if(s[i]=='S')z[i]=2;
		if(s[i]=='W')z[i]=3;
		if(s[i]=='E')z[i]=4;
	}
	for(int i=1;i<=n;i++)for(int j=1;j<=m;j++)for(int k=0;k<=l;k++)b[i][j][k]=-1;
	for(int i=0;i<=l;i++){
		dfs(yx,yy,i);p1=b[yx][yy][i];
		dfs(fx,fy,i);p2=b[fx][fy][i];
		if(p1<p2){
			printf("%d",i);
			return 0;
		}
	}
	puts("-1");
	return 0;
}

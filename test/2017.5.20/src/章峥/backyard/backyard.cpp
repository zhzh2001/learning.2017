#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("backyard.in");
ofstream fout("backyard.out");
const int W=500,MOD=1000000000;
int f[W+1][W+1];
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	for(int i=0;i<=W;i++)
	{
		f[i][0]=1;
		for(int j=1;j<=i;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j-1])%MOD;
	}
	int t;
	fin>>t;
	while(t--)
	{
		int n,w,h,d;
		fin>>n>>w>>h>>d;
		if(n==1)
		{
			fout<<(w+1)*(h+1)<<endl;
			continue;
		}
		int ans=0;
		for(int x=0;x<=w;x++)
			for(int y=0;y<=h;y++)
				if(x||y)
				{
					int g=gcd(x,y),x0=x/g,y0=y/g;
					double dist=sqrt(x0*x0+y0*y0);
					int cnt=ceil(d/dist);
					if(cnt*(n-1)<=g)
					{
						int now=(long long)f[g-(cnt-1)*(n-1)-1][n-2]*(w-x+1)*(h-y+1)%MOD;
						if(x&&y)
							now=now*2%MOD;
						ans=(ans+now)%MOD;
					}
				}
		fout<<ans<<endl;
	}
	return 0;
}
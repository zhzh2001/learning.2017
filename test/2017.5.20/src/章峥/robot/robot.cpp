#include<fstream>
#include<string>
#include<cstring>
using namespace std;
ifstream fin("robot.in");
ofstream fout("robot.out");
const int N=50,L=2500,INF=0x3f3f3f3f;
int n,m,f[N+1][N+1][L+1];
string mat[N],opr;
int dp(int x,int y,int k)
{
	if(mat[x][y]=='X')
		return 0;
	if(k==opr.length())
		return INF;
	int& now=f[x][y][k];
	if(~now)
		return now;
	now=dp(x,y,k+1)+1;
	switch(opr[k])
	{
		case 'N':
			x--;
			break;
		case 'S':
			x++;
			break;
		case 'W':
			y--;
			break;
		case 'E':
			y++;
			break;
	}
	if(x>=0&&x<n&&y>=0&&y<m&&mat[x][y]!='#')
		now=min(now,dp(x,y,k+1)+1);
	return now;
}
int main()
{
	fin>>n>>m;
	int x1,y1,x2,y2;
	for(int i=0;i<n;i++)
	{
		fin>>mat[i];
		for(int j=0;j<m;j++)
			if(mat[i][j]=='Y')
				x1=i,y1=j;
			else
				if(mat[i][j]=='F')
					x2=i,y2=j;
	}
	fin>>opr;
	memset(f,-1,sizeof(f));
	for(int i=0;i<opr.length();i++)
		if(dp(x1,y1,i)<dp(x2,y2,i))
		{
			fout<<i<<endl;
			return 0;
		}
	fout<<-1<<endl;
	return 0;
}
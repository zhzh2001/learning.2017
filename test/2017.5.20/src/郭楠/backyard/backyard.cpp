#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int ans,t,n,w,h,d,dx,dy;
void dfs(int sx,int x,int y,int lx,int ly)
{
	if(x<0 || x>w || y<0 || y>h) return;
	if(sqrt((x-lx)*(x-lx)+(y-ly)*(y-ly))>=1.0*d||sx==0) 
	  {
	  	if(sx+1==n) ans++;
	  	    else dfs(sx+1,x+dx,y+dy,x,y); 
	  }
	if(sx!=0) dfs(sx,x+dx,y+dy,lx,ly);
}
int gcd(int x,int y)
{
	if(y==0) return x;else return gcd(y,x%y);
}
int main()
{int i,j,k;
   freopen("backyard.in","r",stdin);
   freopen("backyard.out","w",stdout);
   scanf("%d",&t);
   for(int i1=1;i1<=t;i1++)
     {
     	scanf("%d%d%d%d",&n,&w,&h,&d);
     	ans=0;
     	for(dx=-w;dx<=w;dx++)
		for(dy=0;dy<=h;dy++)
		if(!(dx==0&&dy==0))
		{bool boo=false;
			if(dx==0 && dy==1) boo=true;
			if(dy==0 && dx==1) boo=true;
			if(dx!=0 && dy!=0)
			  if(gcd(abs(dx),dy)==1) boo=true;
			if(boo)
     	    for(i=0;i<=w;i++)
			for(j=0;j<=h;j++) 
			dfs(0,i,j,0,0);
	    }
		printf("%d\n",ans);
     }
}

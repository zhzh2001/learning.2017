#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std;
int a[100][100];
ll ans,x;
int n,m;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline int rch()
{
	int x;char ch=getchar();
	while(ch<'A'||ch>'z'||(ch>'X'&&ch<'a'))ch=getchar();
	if(ch>='a')x=ch-'a'+1;else x=ch-'A'+27;
}
int main()
{
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	m=read();n=read();
	For(i,1,m)For(j,1,n)a[j][rch()]++;
	For(i,1,n)sort(a[i]+1,a[i]+51);
	x=1;
	For(i,1,50)ans+=a[n][i]*(50-i+1);
	Rep(i,1,n-1)
	{
		x*=50;
		x%=mo;
		For(j,1,49)ans=((a[i][j]*(50-j))%mo)*x%mo+ans;
	}
	cout<<ans<<endl;
	return 0;
}


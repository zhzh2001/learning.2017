#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000000
using namespace std;
int n,w,h,d,t;
ll ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	t=read();
	For(lqz,1,t)
	{
		ans=0;
		n=read();w=read();h=read();d=read();
		if(n==1)ans=(w+1)*(h+1);
		if(n==2)
		{
			int x=(w+1)*(h+1);
			x%=mo;
			ans=x*(x-1)/2;
			ans%=mo;
		}
		if(n==3)
		{
			For(x1,0,w)
				For(y1,0,h)
					For(x2,0,w)
						For(y2,0,h)
							For(x3,0,w)
								For(y3,0,h)
								{
									if((x1==x2&&y1==y2)||(x1==x3&&y1==y3)||(x2==x3&&y2==y3))continue;
									if((x1-x2)*(y1-y3)==(x1-x3)*(y1-y2))ans++;
								}
		}
		cout<<ans<<endl;
	}
	return 0;
}


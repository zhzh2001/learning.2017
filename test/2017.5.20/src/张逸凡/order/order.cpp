#include<cstdio>
#include<functional>
#include<algorithm>
const int P=1e9+7;
char a[64],s[64][64];
int i,j,n,m,B=1,r,f[128];
int main()
{
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	scanf("%d%d",&m,&n),r=m;
	for(i=1;i<=m;++i)
	{
		scanf("%s",s[i]+1);
		for(j=1;j<=n;++j)
			s[i][j]-=s[i][j]<96?39:97;
	}
	for(i=n;i;--i,B=50ll*B%P)
	{
		for(j=0;j<60;f[j++]=0);
		for(j=1;j<=m;++j)++f[s[j][i]];
		std::sort(f,f+60,std::greater<int>());
		for(j=0;f[j];++j)r=(r+1ll*j*B*f[j])%P;
	}
	printf("%d",r);
}

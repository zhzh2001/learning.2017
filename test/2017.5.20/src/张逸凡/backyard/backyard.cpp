#include<cstdio>
#include<cmath>
using namespace std;
const int P=1e9;
int T,i,j,x,y,k,t1,t2,n,m,C,D,l,ans,g[512][512],c[1024][1024];
inline int GCD(int a,int b){return b?GCD(b,a%b):a;}
int wk()
{
	if(C<2)return (n+1)*(m+1);
	for(ans=i=0;i<=n;++i)for(j=0;j<=m;++j)for(x=1;i+x<=n;++x)
	{
		if((t1=(n-i)/x+1)<C)break;
		for(y=i<x?1:j+1;j+y<=m;++y)if(g[x][y]==1)
		{
			if((t2=(m-j)/y+1)<C)break;
			k=t1<t2?t1:t2;
			l=ceil(D/sqrt(x*x+y*y)-1e-8);
			if((t2=k-(l-1)*(C-1))<C)continue;
			ans=(ans+c[t2][C])%P;
		}
	}
	ans=2*ans%P;
	if((k=n+1-(D-1)*(C-1))>=C)
		ans=(ans+1ll*(m+1)*c[k][C])%P;
	if((k=m+1-(D-1)*(C-1))>=C)
		ans=(ans+1ll*(n+1)*c[k][C])%P;
	return ans;
}
int main()
{
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	for(i=1;i<502;++i)for(j=1;j<502;++j)
		g[i][j]=GCD(i,j);
	for(**c=i=1;i<502;++i)for(*c[i]=j=1;j<=i;++j)
		c[i][j]=(c[i-1][j]+c[i-1][j-1])%P;
	for(scanf("%d",&T);T--;printf("%d\n",wk()))
		scanf("%d%d%d%d",&C,&n,&m,&D);
}

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
#define debug cout<<"yes"<<endl;
const int mod=1000000007;
ifstream fin("order.in");
ofstream fout("order.out");
char zifu[53][53];
int xulie[60][60];
string s[60];
long long ans=0;
long long qpow(int x,int y){
	if(y==0)  return 1;
	if(y==1)  return x;
	long long fanhui=qpow(x,y/2);
	fanhui%=mod;
	fanhui=fanhui*fanhui;
	fanhui%=mod;
	if(y%2==1){
		fanhui*=x;
		fanhui%=mod;
	}
	return fanhui;
}
int n,m;
int main(){
	//freopen("order.in","r",stdin);
	//freopen("order.out","w",stdout);
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>s[i];
		for(int k=1;k<=m;k++){
			zifu[i][k]=s[i][k-1];
		}
		for(int j=1;j<=m;j++){
			if(zifu[i][j]>='A'&&zifu[i][j]<='Z'){
				xulie[j][zifu[i][j]-'A'+27]++;
			}else{
				xulie[j][zifu[i][j]-'a'+1]++;
			}
		}
	}
	for(int i=1;i<=m;i++){
		sort(xulie[i]+1,xulie[i]+53);
		for(int j=52;j>=1;j--){
			if(xulie[i][j]>0){
				ans+=(qpow(50,m-i)*(xulie[i][j])*(52-j));
				ans%=mod;
			}else{
				break;
			}
		}
	}
	sort(s+1,s+n+1);
	ans+=n;
	ans%=mod;
	fout<<ans<<endl;
	return 0;
}
/*

in:
4 2
bb
cb
ca
ca

out:
55

*/
/*

in:
2 4
abcd
ABCD

out:
127553

*/
/*

in:
2 1
a
a

out:
2

*/

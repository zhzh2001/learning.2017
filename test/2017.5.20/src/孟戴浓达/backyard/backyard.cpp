//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define debug cout<<"yes"<<endl;
using namespace std;
const int mod=1000000000;
ifstream fin("backyard.in");
ofstream fout("backyard.out");
int t,n,w,h,d;
int gcd(int a,int b){
	if(b==0) return a;
	else     return gcd(b,a%b);
}
int gcdd[503][503];
int C[503][503];
inline int sqr(int x){
	return x*x%mod;
}
inline int dp(int x,int y,int d){
	if(x-1<1||y-(x-1)*d>=x-1||y-(x-1)*d<1){
		return 0;
	}
	return C[x-1][y-(x-1)*d];
}
int main(){
	fin>>t;
	for(int i=1;i<=500;i++){
		C[i][1]=1; C[i][i]=1;
		for(int j=1;j<=500;j++){
			gcdd[i][j]=gcd(i,j);
		}
		for(int j=2;j<i;j++){
			C[i][j]=C[i-1][j]+C[i-1][j-1];
			C[i][j]%=mod;
		}
	}
	while(t--){
		long long ans=0;
		fin>>n>>w>>h>>d;
		int x;
		double y;
		for(int i=1;i<=w;i++){
			for(int j=1;j<=h;j++){
				x=gcdd[i][j];
				y=sqrt(sqr(i/x)+sqr(j/x));
				y=d/y+1;
				int xx=y;
				if(h-i+1>=0&&(w-j+1)>=0){
					ans+=dp(n,x+1,xx)*(h-i+1)*(w-j+1);
				}
				ans%=mod;
			}
		}
		for(int i=1;i<=w+1;i++){
			ans+=dp(n,i,d)*h*(w+1-i+1);
			ans%=mod;
		}
		for(int i=1;i<=h+1;i++){
			ans+=dp(n,i,d)*w*(h+1-i+1);
			ans%=mod;
		}
		ans*=2;
		ans%=mod;
		fout<<ans<<endl;
	}
	return 0;
}
/*

in:
6
2 4 4 1
13 36 48 5
5 5 5 1
50 49 49 1
6 5 5 2
10 55 75 5

out:
300
2
88
102
0
490260662

*/

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("robot.in");
ofstream fout("robot.out");
int m,n;
char mp[53][53];
string s;
int styx,styy,stfx,stfy;
int jiyi1[53][53][2503];
int jiyi2[53][53][2503];


inline int min(int a,int b){
	if(a<b) return a;
	else    return b;
}

int dp1(int x,int y,int bu){
	int& fanhui=jiyi1[x][y][bu];
	if(fanhui!=99999999)       return fanhui;
	if(mp[x][y]=='X')          return fanhui=0;
	if(bu>s.length()-1)        return fanhui=99999999;
	fanhui=dp1(x,y,bu+1)+1;
	if(s[bu]=='N'){
		if(mp[x-1][y]!='#'&&x-1>0)   fanhui=min(fanhui,dp1(x-1,y,bu+1)+1);
	}else if(s[bu]=='S'){
		if(mp[x+1][y]!='#'&&x+1<=m)  fanhui=min(fanhui,dp1(x+1,y,bu+1)+1);
	}else if(s[bu]=='W'){
		if(mp[x][y-1]!='#'&&y-1>0)   fanhui=min(fanhui,dp1(x,y-1,bu+1)+1);
	}else if(s[bu]=='E'){
		if(mp[x][y+1]!='#'&&y+1<=n)  fanhui=min(fanhui,dp1(x,y+1,bu+1)+1);
	}
	return fanhui;
}
int dp2(int x,int y,int bu){
	int& fanhui=jiyi2[x][y][bu];
	if(fanhui!=99999999)       return fanhui;
	if(mp[x][y]=='X')    return fanhui=0;
	if(bu>s.length()-1)  return fanhui=99999999;
	fanhui=dp2(x,y,bu+1)+1;
	if(s[bu]=='N'){
		if(mp[x-1][y]!='#'&&x-1>0)   fanhui=min(fanhui,dp2(x-1,y,bu+1)+1);
	}else if(s[bu]=='S'){
		if(mp[x+1][y]!='#'&&x+1<=m)  fanhui=min(fanhui,dp2(x+1,y,bu+1)+1);
	}else if(s[bu]=='W'){
		if(mp[x][y-1]!='#'&&y-1>0)   fanhui=min(fanhui,dp2(x,y-1,bu+1)+1);
	}else if(s[bu]=='E'){
		if(mp[x][y+1]!='#'&&y+1<=n)  fanhui=min(fanhui,dp2(x,y+1,bu+1)+1);
	}
	return fanhui;
}
int main(){
	//freopen("robot.in","r",stdin);
	//freopen("robot.out","w",stdout);
	fin>>m>>n;
	for(int i=0;i<=51;i++){
		for(int j=0;j<=51;j++){
			for(int k=0;k<=2501;k++){
				jiyi1[i][j][k]=99999999;
				jiyi2[i][j][k]=99999999;
			}
		}
	}
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++){
			fin>>mp[i][j];
			if(mp[i][j]=='Y'){
				styx=i;  styy=j;
			}else if(mp[i][j]=='F'){
				stfx=i;  stfy=j;
			}
		}
	}
	fin>>s;
	bool yes=false;
	for(int i=0;i<=s.length()-1;i++){
		int ans1=dp1(styx,styy,i);
		int ans2=dp2(stfx,stfy,i);
		if(ans2>ans1){
			fout<<i<<endl;
			yes=true;
			break;
		}
	}
	if(yes==false){
		fout<<-1<<endl;
	}
	return 0;
}
/*

in:
8 8
########
#......#
#.Y....#
#.F.#..#
#...X..#
#...#..#
#......#
########
SSEEESSW

out:
2

*/
/*

in:
2 2
#F
YX
NES

out:
0

*/
/*

in:
2 2
#F
YX
SNES

out:
1

*/

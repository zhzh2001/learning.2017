#include <bits/stdc++.h>
#define ll long long
#define zyy 1000000007
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
char ch[60][60];
struct node{
	int vis, id;
	bool operator < (const node &b) const {
		return vis > b.vis;
	}
}vis[60][200];
int rk[60][200];
int bin[3000];
int main(){
	freopen("order.in", "r", stdin);
	freopen("order.out", "w", stdout);
	int n = read(), m = read();
	bin[1] = 1; for(int i = 2; i < 3000; i++) bin[i] = (ll)bin[i-1]*50%zyy;
	for(int i = 1; i <= n; i++)
		scanf("%s", ch[i]+1);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			vis[j][ch[i][j]].vis++;
	for(int i = 1; i <= m; i++){
		for(int j = 1; j < 200; j++)
			vis[i][j].id = j;
		sort(vis[i]+1, vis[i]+200);
		for(int j = 1; j < 200; j++)
			rk[i][vis[i][j].id] = j;
	}
	int ans = n;
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			ans = (ans + bin[(rk[j][ch[i][j]]-1)*(m-j+1)])%zyy;
	printf("%d\n", ans);
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define N 30010
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int mp[60][60],bx[5000], by[5000],dep[5000];
int xx,xy,fx,fy,yx,yy,vis[60][60],n,m;
int f[2][60][60];
int ax[] = {1, 0, -1, 0};
int ay[] = {0, 1, 0, -1};
char str[60];
int calc(char c){
	switch(c){
	case 'S': return 0;
	case 'N': return 2;
	case 'E': return 1;
	case 'W': return 3;
	}
}
int bfs(){
	int l = 0, r = 1;
	bx[1] = yx; by[1] = yy; dep[1] = 1; vis[yx][yy] = 1;
	while(l < r){ ++l;
		for(int i = 0; i < 4; i++){
			int dx = bx[l]+ax[i], dy = by[l]+ay[i];
			if(dx > m || dx < 1 || dy > n
				|| dy < 1 || vis[dx][dy] || mp[dx][dy]) continue;
			if(dx == xx && dy == xy) return dep[l];
			vis[dx][dy] = 1; ++r;
			bx[r] = dx; by[r] = dy; dep[r] = dep[l]+1;
		}
	}
	return -1;
}
int main(){
	freopen("robot.in", "r", stdin);
	freopen("robot.out", "w", stdout);
	m = read(); n = read();
	for(int i = 1; i <= m; i++){
		scanf("%s", str+1);
		for(int j = 1; j <= n; j++)
			if(str[j] == '.') mp[i][j] = 0;
			else if(str[j] == 'X') mp[xx=i][xy=j] = 0;
			else if(str[j] == 'F') mp[fx=i][fy=j] = 0;
			else if(str[j] == 'Y') mp[yx=i][yy=j] = 0;
			else mp[i][j] = 1;
	}
	int runtime = bfs();
	if(runtime == -1) return puts("-1")&0;
	scanf("%s", str+1);int len = strlen(str+1);
	if(len < runtime) return puts("0")&0;
	for(int i = 1; i <= runtime; i++){
		int kd = i&1, op = calc(str[i]);
		memcpy(f[kd], f[!kd], sizeof f[kd]);
		for(int j = 1; j <= m; j++) for(int k = 1; k <= n; k++)
		if(!f[kd][j+ax[op]][k+ay[op]] && !mp[j+ax[op]][k+ay[op]])
		f[kd][j+ax[op]][k+ay[op]] = f[!kd][j][k];
		f[kd][fx+ax[op]][fy+ay[op]] = i;
		// for(int i = 1; i <= m; i++,puts(""))
		// 	for(int j = 1; j <= n; j++)
		// 		printf("%d ", f[kd][i][j]);
		// puts("");
	}
	printf("%d\n", f[runtime&1][xx][xy]);
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define zyy 1000000000
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int px1, px2, py1, py2,n,h,w,d,ans;
int fz,fm;
int gcd(int a, int b){return b?gcd(b,a%b):a;}
void dfs(int x, int lx, int ly){
	if(!x){ans++;return;}
	for(int j = ly+fm,i=lx+fz; j <= w&&i<=h; i+=fz,j+=fm){
		if((j-ly)*(j-ly)+(i-lx)*(i-lx) < d) continue;
		dfs(x-1, i, j);
	}
}
int main(){
	freopen("backyard.in", "r", stdin);
	freopen("backyard.out", "w", stdout);
	int T = read();
	while(T--){
		n = read(); w = read()+1; h = read()+1; d = read(); d=d*d;
		ans = 0;
		if(n == 1){
			printf("%d\n", w*h);
			continue;
		}
		for(px1 = 1; px1 <= h; px1++) for(py1 = 1; py1 <= w; py1++)
		for(px2 = px1; px2 <= h; px2++) for(py2 = py1+1; py2 <= w; py2++){
			if((py2-py1)*(py2-py1)+(px2-px1)*(px2-px1) < d) continue;
			int gg = gcd(py2-py1, px2-px1);
			fz = (px2-px1)/gg; fm = (py2-py1)/gg;
			dfs(n-2, px2, py2);
		}
		printf("%d\n", ans<<1);
	}
	return 0;
}
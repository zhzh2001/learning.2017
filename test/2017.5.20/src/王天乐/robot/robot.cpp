#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=55,M=2505;
struct node{ int x,y; }F,Y,E;
bool map[N][N];
bool visa[N][N],visb[N][N];
char s[M],ch[N];
int n,m,len;

bool bfs(int x){
	memset(visa,0,sizeof(visa));
	memset(visb,0,sizeof(visb));
	visa[Y.x][Y.y]=true; visb[F.x][F.y]=true;
	queue<node> qa,qb;
	while(!qa.empty()) qa.pop();
	while(!qb.empty()) qb.pop();
	qa.push(Y); qb.push(F);
	for(int i=x;i<=len;i++){
		int l=qb.size();
		for(;l;l--){
			node u=qb.front(); qb.pop();
			if(visa[u.x][u.y]) continue;
			qb.push(u); node v;
			if(s[i]=='N') v.x=u.x-1,v.y=u.y;
			else if(s[i]=='E') v.x=u.x,v.y=u.y+1;
			else if(s[i]=='S') v.x=u.x+1,v.y=u.y;
			else if(s[i]=='W') v.x=u.x,v.y=u.y-1;
			if(v.x<=0||v.x>n||v.y<=0||v.y>m||map[v.x][v.y]||visb[v.x][v.y]) continue;
			if(visa[v.x][v.y]){
				visb[v.x][v.y]=true;
				continue;
			}
			if(v.x==E.x&&v.y==E.y) return false;
			visb[v.x][v.y]=true;
			qb.push(v);
		}
		l=qa.size();
		for(;l;l--){
			node u=qa.front(); qa.pop();
			if(visb[u.x][u.y]) continue;
			qa.push(u); node v;
			if(s[i]=='N') v.x=u.x-1,v.y=u.y;
			else if(s[i]=='E') v.x=u.x,v.y=u.y+1;
			else if(s[i]=='S') v.x=u.x+1,v.y=u.y;
			else if(s[i]=='W') v.x=u.x,v.y=u.y-1;
			if(v.x<=0||v.x>n||v.y<=0||v.y>m||map[v.x][v.y]||visa[v.x][v.y]) continue;
			if(visb[v.x][v.y]){
				visa[v.x][v.y]=true;
				continue;
			}
			if(v.x==E.x&&v.y==E.y) return true;
			visa[v.x][v.y]=true;
			qa.push(v);
		}
	}
	return false;
}

int main(){
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++){
		scanf("%s",ch+1);
		for(int j=1;j<=m;j++){
			if(ch[j]=='#') map[i][j]=true;
			else if(ch[j]=='.') map[i][j]=false;
			else if(ch[j]=='Y') Y.x=i,Y.y=j;
			else if(ch[j]=='F') F.x=i,F.y=j;
			else if(ch[j]=='X') E.x=i,E.y=j;
		}
	}
	scanf("%s",s+1); len=strlen(s+1);
	for(int i=1;i<=len;i++){
		if(bfs(i)){
			printf("%d\n",i-1);
			return 0;
		}
	}
	printf("-1\n");
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const ll N=55,mod=1000000007;
ll n,m,ans;
ll a[N][N];
char ch[N];
ll len[N];

bool cmp(const ll &a,const ll &b){
	return a>b;
}

int main(){
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	n=read(); m=read();
	for(ll i=1;i<=n;i++){
		scanf("%s",ch+1);
		for(ll j=1;j<=m;j++){
			if(ch[j]>='a'&&ch[j]<='z') a[j][ch[j]-'a'+1]++;
			else if(ch[j]>='A'&&ch[j]<='X') a[j][ch[j]-'A'+27]++;
		}
	}
	for(ll i=1;i<=m;i++) sort(a[i]+1,a[i]+50+1,cmp);
	len[m]=1;
	for(ll i=m-1;i>=1;i--) len[i]=(len[i+1]*50)%mod;
	for(int i=1;i<m;i++)
		for(int j=1;j<=50;j++)
			ans=(ans+len[i]*(j-1)*a[i][j])%mod;
	for(int j=1;j<=50;j++) ans=(ans+j*a[m][j])%mod;
	printf("%lld\n",ans);
	return 0;
}

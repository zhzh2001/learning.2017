#include<cstdio>
#include<algorithm>
const int orz=1e9+7;
int tmp,m,n,i,j,mtch[55][55],ans,ref;
char s[55][55];
struct orz{
	int t,s;
	int operator<(const orz&A)const{return s>A.s;}
}cnt[55][55];
inline int fs(int base,int a){
	tmp=1;
	while(a){
		if(a&1)tmp=(1ll*tmp*base%orz);
		a>>=1;
		base=(1ll*base*base%orz);
	}return tmp;
}
int main()
{
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(i=1;i<=n;i++)
		for(j=1;j<=50;j++)cnt[i][j].t=j;
	for(i=1;i<=m;i++){
		scanf("%s",s[i]);
		for(j=0;j<n;j++)
			if(s[i][j]>='a'&&s[i][j]<='z')cnt[j+1][s[i][j]-'a'+1].s++;
			else cnt[j+1][s[i][j]-'A'+27].s++;			
	}
	for(i=1;i<=n;i++)std::sort(cnt[i]+1,cnt[i]+51);
	for(i=1;i<=n;i++)
		for(j=1;j<=50;j++)
			mtch[i][cnt[i][j].t]=j;
	for(ans=0,i=1;i<=m;i++)
		for(ans++,j=0;j<n;j++){
			if(s[i][j]>='a'&&s[i][j]<='z')ref=s[i][j]-'a'+1;
			else ref=s[i][j]-'A'+27;
			ans+=(1ll*fs(50,n-j-1)*(mtch[j+1][ref]-1)%orz);
			ans%=orz;
		}
	printf("%d\n",ans);
}

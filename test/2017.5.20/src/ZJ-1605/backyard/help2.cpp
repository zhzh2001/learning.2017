#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=505;
int c[N][N],n,m,d;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	for (int i=0;i<N;i++)
		c[i][0]=1;
	for (int i=1;i<N;i++)
		for (int j=1;j<=i;j++)
			c[i][j]=c[i-1][j]+c[i-1][j-1];
	n=Read(),m=Read(),d=Read();
	for (int i=0;i<N;i++) c[i][0]=0;
	printf("%d\n",c[n][m]-c[n-d-1][m-1]*c[d+1][2]*(n-d));
	printf("%d %d %d %d\n",c[n][m],c[n-d-1][m-1],c[d+1][2],n-d);
}

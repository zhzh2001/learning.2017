#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<ctime>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=505,M=505,Mod=1e9; const double eps=1e-6;
int f[M][N],real,n,x,y,d,Ans,dis;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int Add(int &x,int y){x=(x+y)%Mod;}
inline int Work(int n,int m,int d){
	printf("%d %d %d\n",n,m,d); 
	d--;
	for (int i=0;i<=n;i++)
		for (int j=0;j<=m;j++) f[i][j]=0;
	f[0][0]=1;
	for (int i=1;i<=d;i++) f[i][1]=1;
	for (int i=0;i<=n;i++)
		for (int j=0;j<=min(m,i);j++)
			Add(f[i+1][j],f[i][j]),Add(f[i+d+1][j+1],f[i][j]);
	Add(Ans,f[n][m]);
//	printf("sd");
}
int main(){
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	for (int T=Read();T--;){
		n=Read(),x=Read(),y=Read(),d=Read();
		for (int i=0;i<=x;i++){
		//	printf("%d\n",i);
			for (int j=0;j<=x;j++){//printf("j1==%d\n",j);
				int xx=abs(i-j),yy=y;
				dis=(xx?__gcd(xx,yy):yy)+1;
				double Dis=sqrt(xx*xx+yy*yy);
				if (Dis<eps) continue;
				double D=d/(Dis/(1.0*dis));
				
				real=(int)D;
				while(D>=1.0) D-=1;
				if (D>eps) real++;//printf("sd");
				Work(dis,n,real);
			}
			for (int j=0;j<y;j++){//printf("j2==%d\n",j);
				int xx=x-i,yy=j;
				dis=(xx?__gcd(xx,yy):yy)+1;
				double Dis=sqrt(xx*xx+yy*yy);
				if (Dis<eps) continue;
				double D=d/(Dis/(1.0*dis));
			//	printf("therre)=");
				real=(int)D;
			//	printf("%.2lf\n",D);// return 0;
				while (D>=1.0) D-=1.0;//printf("%.2lf\n",D);
				if (D>eps) real++;
				Work(dis,n,real);
			}
		}
	//	printf("_______________");
		for (int i=0;i<=y;i++){
			for (int j=0;j<=x;j++){
				int xx=j,yy=y-i;
				dis=(yy?__gcd(xx,yy):xx)+1;
				double Dis=sqrt(xx*xx+yy*yy);
				if (Dis<eps) continue;
				double D=d/(Dis/(1.0*dis));
				
				real=(int)D;
				while(D>=1.0) D-=1;
				if (D>eps) real++;//printf("sd");
				Work(dis,n,real);
			}
			for (int j=0;j<y;j++){
				int xx=x,yy=abs(i-j);
				dis=(yy?__gcd(xx,yy):xx)+1;
				double Dis=sqrt(xx*xx+yy*yy);
				if (Dis<eps) continue;
				double D=d/(Dis/(1.0*dis));
				
				real=(int)D;
				while(D>=1.0) D-=1;
				if (D>eps) real++;//printf("sd");;
				Work(dis,n,real);
			}
		}
		printf("%d\n",Ans); Ans=0;
	}
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=505;
int f[N][N],n,m,d,Ans;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	n=Read(),m=Read(),d=Read(); f[0][0]=1;
	for (int i=1;i<=d;i++) f[i][1]=1;
	for (int i=0;i<=n;i++)
		for (int j=0;j<=min(m,i);j++)
			f[i+1][j]+=f[i][j],f[i+d+1][j+1]+=f[i][j];
	printf("%d\n",f[n][m]);
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=55,M=3e3+5,INF=1e9+7; char s[N][N],to[M];
int f[2][N][N][N][N],n,m,Len,X1,Y1,X2,Y2,E1,E2;
int to1[4]={0,-1,1,0},to2[4]={1,0,0,-1},pos[M],Ans;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int Min(int &x,int y){x=min(x,y);}
int main(){
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	n=Read(),m=Read();
	for (int i=1;i<=n;i++){
		scanf("\n%s",s[i]+1);
		for (int j=1;j<=m;j++){
			if (s[i][j]=='Y') X1=i,Y1=j;
			if (s[i][j]=='F') X2=i,Y2=j;
			if (s[i][j]=='X') E1=i,E2=j;
		}
	}
	scanf("\n%s",to+1); Len=strlen(to+1); Ans=INF;
	memset(f,63,sizeof(f)); f[0][X1][Y1][X2][Y2]=0;
	pos['N']=1,pos['S']=2,pos['W']=3,pos['E']=0;
	for (int i=0;i<Len;i++)
		for (int j1=1;j1<=n;j1++)
			for (int k1=1;k1<=m;k1++)
				for (int j2=1;j2<=n;j2++)
					for (int k2=1;k2<=m;k2++){int t=i&1;
						if (f[t][j1][k1][j2][k2]>=INF) continue;
					//	printf("Sta:     %d   %d %d   %d %d   %d\n",i,j1,k1,j2,k2,f[t][j1][k1][j2][k2]);
						if (j2==E1&&k2==E2){f[t][j1][k1][j2][k2]=INF; continue;}
						if (j1==E1&&k1==E2) Min(Ans,f[t][j1][k1][j2][k2]);
						
						Min(f[t^1][j1][k1][j2][k2],f[t][j1][k1][j2][k2]+1);
						int xx1=j1+to1[pos[to[i+1]]],yy1=k1+to2[pos[to[i+1]]];
						int xx2=j2+to1[pos[to[i+1]]],yy2=k2+to2[pos[to[i+1]]];
					//	printf("%d %d %d %d\n",xx1,yy1,xx2,yy2);
						if ((!xx1)||(!yy1)||(xx1>n)||(yy1>m)||s[xx1][yy1]=='#') xx1=j1,yy1=k1;
						if ((!xx2)||(!yy2)||(xx2>n)||(yy2>m)||s[xx2][yy2]=='#') xx2=j2,yy2=k2;
					//	printf("%d %c   %d %d %d %d\n",pos[to[i+1]],to[i+1],xx1,yy1,xx2,yy2);
						Min(f[t^1][xx1][yy1][xx2][yy2],f[t][j1][k1][j2][k2]);
						f[t][j1][k1][j2][k2]=INF;
					}
	printf("%d\n",Ans);
}

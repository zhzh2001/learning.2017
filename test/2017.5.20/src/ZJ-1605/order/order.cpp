#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<vector>
#include<math.h>
#include<string>
#include<bitset>
#include<stdio.h>
#include<cstring>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=55,Mod=1e9+7;
int Res,pos[305],power[N],n,m,tot;
struct T{int id,val;} num[N][N]; char s[N][N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int cmp(T i,T j){return i.val>j.val;}
int main(){
	freopen("order.in","r",stdin);
	freopen("order.out","w",stdout);
	n=Read(),m=Read(); power[0]=1;
	for (int i='a';i<='z';i++) pos[i]=++tot;
	for (int i='A';i<='Z';i++) pos[i]=++tot;
	for (int i=1;i<=m;i++) power[i]=1ll*power[i-1]*50%Mod;
	for (int i=1;i<=m;i++)
		for (int j=1;j<=tot;j++) num[i][j].id=i;
	for (int i=1;i<=n;i++){
		scanf("\n%s",s[i]+1);
		for (int j=1;j<=m;j++)
			num[j][pos[(int)s[i][j]]].val++;
	}
	for (int i=1;i<=m;i++){
		sort(num[i]+1,num[i]+1+tot,cmp);
		for (int j=2;j<=tot;j++)
			Res=(1ll*Res+1ll*power[m-i]*(j-1)%Mod*(num[i][j].val)%Mod)%Mod;
	}
	printf("%d\n",(Res+n)%Mod);
}

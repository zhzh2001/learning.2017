#include<bits/stdc++.h>
#define mo 1000000000
#define eps 1e-9
using namespace std;
int t,n,w,h,d;
long long C[505][505];
int gcd(int a,int b)
{
	if (b==0) return a;
	else return gcd(b,a%b);
} 
int main()
{
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	scanf("%d",&t);
	C[0][0]=1;
	for (int i=1;i<=501;i++)
	{
		C[i][0]=C[i][i]=1;
		for (int j=1;j<=i-1;j++)
			C[i][j]=(C[i-1][j]+C[i-1][j-1])%mo;
	}
	while (t)
	{
		long long ans=0;
		scanf("%d%d%d%d",&n,&w,&h,&d);
		if (n==1)
		{
			cout<<(w+1)*(h+1)<<endl;
			t--;
			continue;			
		}
		for (int i=0;i<=w;i++)
			for (int j=0;j<=h;j++)	
			{
				if (i==0&&j==0) continue;
				int g=gcd(i,j);
				int totnode=g+1; 
				if (totnode<n) continue; 
				double dis=sqrt(double((i/g)*(i/g)+(j/g)*(j/g)));
				int k=int(trunc(double(d)/dis-eps))+1; 
				if (k*(n-1)+1>totnode) continue; 
				ans=(ans+(i==0||j==0?1:2 )*((((w-i+1)*(h-j+1))%mo)*(C[totnode-2-(n-1)*(k-1)][n-2]%mo)))%mo; 
			}		
		t--;
		printf("%d\n",ans);
	}	
}


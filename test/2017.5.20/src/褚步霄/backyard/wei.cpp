#include<stdio.h>
#include<math.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 505
int f[N][55],sum[N][55],dp[N][N][55];
int T,n,m,h,w,i,j,ans,MOD=1000000000;
double d,dis,eps=1e-9;
int gcd(int a,int b){return !b?a:gcd(b,a%b);}
double abs(double x){return (x<0)?-1*x:x;}
double calc(double w,double h,double k){
	int kk=h/w;
	if(abs(kk-k)<eps)return sqrt(w*w+h*h);
	if(k<kk){
		int x=k*w;
		return sqrt(w*w+x*x);
	}int x=h/k;
	return sqrt(x*x+h*h);
}
int main()
{
	freopen("backyard.in","r",stdin);
//	freopen("backyard.out","w",stdout);
	for(w=1;w<=500;w++){
		memset(f,0,sizeof(f));
		memset(sum,0,sizeof(sum));
		for(i=1;i<=501;i++)f[i][1]=1,sum[i][1]=(sum[i-1][1]+f[i][1])%MOD;
		for(i=w+1;i<=501;i++)
		for(j=2;j<=max(i,50);j++){
			f[i][j]=sum[i-w][j-1];
			sum[i][j]=(sum[i-1][j]+f[i][j])%MOD;
			dp[w][i][j]=sum[i][j];
		}
	}
	scanf("%d",&T);
	while(T--){
		ans=0;
		scanf("%d%d%d%lf",&n,&w,&h,&d);
		d=max(d,1.0);
		for(i=1;i<=w;i++)
		for(j=1;j<=h;j++){
			if(gcd(i,j)>1)continue;
			dis=sqrt((double)i*i+j*j);
			int x=ceil(d/dis),now=floor(calc(w,h,(double)j/(double)i)/dis+eps)+1;
			ans=(ans+dp[x][now][n])%MOD;
		}ans=(ans+(dp[(int)d][w+1][n]+dp[(int)d][h+1][n])%MOD)%MOD;
		printf("%d\n",ans);
	}return 0;
}

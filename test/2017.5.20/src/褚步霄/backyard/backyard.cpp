#include<stdio.h>
#include<math.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 505
int vis[N][N],fac[N],inv[N],p[N],prime[N],Min[N],sum[N];
int T,n,m,h,w,i,j,k,l,ans,dx,dy,num,MOD=1000000000;
double d,eps=1e-9;
int gcd(int a,int b){return !b?a:gcd(b,a%b);}
int ksm(int x,int y){
	int res=1;
	while(y){
		if(y&1)res=1ll*res*x%MOD;
		x=1ll*x*x%MOD,y>>=1;
	}return res;
}
int C(int n,int m){
	if(n<m)return 0;
	int res=1;
	for(int i=1;i<=501;i++)sum[i]=0;
	for(int i=m+1;i<=n;i++)
	for(int j=i;j>1;j/=Min[j])sum[Min[j]]++;
	for(int i=2;i<=n-m;i++)
	for(int j=i;j>1;j/=Min[j])sum[Min[j]]--;
	for(int i=1;i<=501;i++)
		res=1ll*res*ksm(i,sum[i])%MOD;
	return res;
}
int main()
{
	freopen("backyard.in","r",stdin);
	freopen("backyard.out","w",stdout);
	for(i=2;i<=501;i++){
		if(!p[i])prime[++num]=i,Min[i]=i;
		for(j=1;i*prime[j]<=501&&j<=num;j++){
			p[i*prime[j]]=1;
			Min[i*prime[j]]=prime[j];
			if(i%prime[j]==0)break;
		}
	}scanf("%d",&T);
	while(T--){
		ans=0;
		scanf("%d%d%d%lf",&n,&w,&h,&d);
		for(i=0;i<=w;i++)
		for(j=0;j<=h;j++){
			if(i==0&&j==0)continue;
			if(gcd(i,j)>1||sqrt((double)i*i+(double)j*j)<d)continue;
			for(k=0;k<=w;k++)for(l=0;l<=h;l++)vis[k][l]=0;
			for(k=0;k<=w;k++)for(l=0;l<=h;l++){
				if(vis[k][l])continue;
				dx=k,dy=l;num=0;
				for(dx=k,dy=l;dx<=w&&dy<=h;dx+=i,dy+=j)
					num++,vis[dx][dy]=1;
				ans=(ans+C(num,n))%MOD;
			}
		}
		printf("%d\n",ans);
	}return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 55
int n,m,i,j,yx,yy,fx,fy,tx,ty,len;
int a[N*N],x[]={0,-1,0,1,0},y[]={0,0,1,0,-1};
char s[N][N],S[N*N];
int dis(int xx,int yy,int x2,int y2){return abs(xx-x2)+abs(yy-y2);}
bool calc(int yx,int yy,int fx,int fy){
	for(int j=i+1;j<=len&&((yx!=tx||yy!=ty)&&(fx!=tx||fy!=ty));j++){
		int dx=yx+x[a[j]],dy=yy+y[a[j]];
		if(dx==fx&&dy==fy){
			int sx=fx+x[a[j]],sy=fy+y[a[j]];
			if(s[sx][sy]=='#')continue;
			if(dis(sx,sy,tx,ty)>dis(fx,fy,tx,ty))continue;
			fx=sx,fy=sy;
			if(s[dx][dy]=='#')continue;
			if(dis(dx,dy,tx,ty)>dis(yx,yy,tx,ty))continue;
			yx=dx,yy=dy;
		}else{
			if(s[dx][dy]!='#'&&dis(dx,dy,tx,ty)<dis(yx,yy,tx,ty))
				yx=dx,yy=dy;
			int sx=fx+x[a[j]],sy=fy+y[a[j]];
			if(s[sx][sy]=='#')continue;
			if(dis(sx,sy,tx,ty)>dis(fx,fy,tx,ty))continue;
			fx=sx,fy=sy;
		}
	}if(yx==tx&&yy==ty)return 1;
	return 0;
}
int main()
{
	freopen("robot.in","r",stdin);
	freopen("robot.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++){
		scanf("%s",s[i]+1);
		for(j=1;j<=m;j++){
			if(s[i][j]=='Y')yx=i,yy=j;
			if(s[i][j]=='F')fx=i,fy=j;
			if(s[i][j]=='X')tx=i,ty=j;
		}
	}scanf("%s",S+1);len=strlen(S+1);
	for(i=1;i<=len;i++){
		if(S[i]=='N')a[i]=1;
		if(S[i]=='E')a[i]=2;
		if(S[i]=='S')a[i]=3;
		if(S[i]=='W')a[i]=4;
	}
	for(i=0;i<=len;i++)
		if(calc(yx,yy,fx,fy)){printf("%d\n",i);return 0;}
	puts("-1");
	return 0;
}

#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <functional>
#define sqr(x) ((x) * (x))
using namespace std;


typedef	long long	int64;
typedef	pair<int,int>	pair2;
typedef	map<int,int>	map2;


const	int	mod = 1000000000;
int	C[1505][1505], next[505][505];


int	gcd(int a, int b)
{
	return (!b) ? a : gcd(b, a % b);
}

	int	countWays(int N, int W, int H, int D)
	{
		if (N == 1) return (W + 1) * (H + 1);
		
		for (int i = 0; i <= 1500; ++ i)
		{
			C[i][0] = 1;
			for (int j = 1; j <= i; ++ j)
				C[i][j] = (C[i - 1][j - 1] + C[i - 1][j]) % mod;
		}
		
		int	Ans = 0;
		for (int i = 0; i <= W; ++ i)
		for (int j = 0; j <= H; ++ j)
		{
			if (!i && !j) continue;
			
			int	g = gcd(i, j), M = g + 1;
			double	d = sqrt(sqr(i / g) + sqr(j / g));
			int	k = (int)(D / d - 1e-9) + 1;
			
			if (k * (N - 1) + 1 > M) continue;
			
			M -= (k - 1) * (N - 1) + 2;
			
			int64	Now = C[M][N - 2];
			Now *= (W - i + 1) * (H - j + 1);
			if (i && j) Now *= 2;
			
			Ans += Now % mod;
			Ans %= mod;
		}
		
		return Ans;
	}

int	main()
{
	int	Test;
	scanf("%d", &Test);
	for (; Test --; )
	{
		int	N, W, H, D;
		scanf("%d%d%d%d", &N, &W, &H, &D);
		printf("%d\n", countWays(N, W, H, D));
	}
}

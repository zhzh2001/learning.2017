#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <functional>
#define sqr(x) ((x) * (x))
using namespace std;


typedef	long long	int64;
typedef	pair<int,int>	pair2;
typedef	map<int,int>	map2;


const	int	mod = 1000000007;
int	a[55], b[55], c[55];


	int	order(vector<string> rows)
	{
		int	M = rows.size();
		int	N = rows[0].size();
		
		for (int j = 0; j <= N; ++ j)
		{
			for (int i = 0; i < M; ++ i) a[i + 1] = rows[i][j] - 'a' + 1;
			
			sort(a + 1, a + M + 1);
			a[0] = -1;
			
			int	P = 0;
			for (int i = 1, _i; i <= M; i = _i)
			{
				for (_i = i + 1; _i <= M && a[_i] == a[i]; ++ _i);
				b[++ P] = _i - i;
			}
			
			sort(b + 1, b + P + 1);
			reverse(b + 1, b + P + 1);
			
			int	cnt = c[j + 1] = 0;
			for (int i = 1; i <= P; ++ i)
			{
				c[j + 1] += b[i] * cnt;
				cnt ++;
			}
		}
		
		sort(c + 1, c + N + 1);
		reverse(c + 1, c + N + 1);
		
		int64	now = 1, ans = 0;
		for (int i = 1; i <= N; ++ i)
		{
			ans += now * c[i];
			ans %= mod;
			now *= 50;
			now %= mod;
		}
		ans += M;
		ans %= mod;
		
		return ans;
	}

int	main()
{
	int	M, N;
	scanf("%d%d", &M, &N);
	vector<string>	read(0);
	for (int i = 0; i < M; ++ i)
	{
		string	now;
		cin >> now;
		read.push_back(now);
	}
	cout << order(read) << endl;
	return 0;
}

#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("robot.in");
ofstream fout("robot.out");
const int N=50;
int n,m,ans,tx,ty;
string mat[N],opr;
void dfs(int k,int x1,int y1,int x2,int y2,int now)
{
	if(k==opr.length()||mat[x2][y2]=='X')
		return;
	if(mat[x1][y1]=='X')
	{
		ans=min(ans,now);
		return;
	}
	int nx1=x1,ny1=y1,nx2=x2,ny2=y2;
	switch(opr[k])
	{
		case 'N':
			nx1--;
			nx2--;
			break;
		case 'E':
			ny1++;
			ny2++;
			break;
		case 'S':
			nx1++;
			nx2++;
			break;
		case 'W':
			ny1--;
			ny2--;
			break;
	}
	bool go1=nx1>=0&&nx1<n&&ny1>=0&&ny1<m&&abs(tx-nx1)<=abs(tx-x1)&&abs(ty-ny1)<=abs(ty-y1)&&mat[nx1][ny1]!='#';
	bool go2=nx2>=0&&nx2<n&&ny2>=0&&ny2<m&&abs(tx-nx2)<=abs(tx-x2)&&abs(ty-ny2)<=abs(ty-y2)&&mat[nx2][ny2]!='#';
	dfs(k+1,go1?nx1:x1,go1?ny1:y1,go2?nx2:x2,go2?ny2:y2,now);
	dfs(k+1,x1,y1,x2,y2,now+1);
}
int main()
{
	fin>>n>>m;
	int x1,y1,x2,y2;
	for(int i=0;i<n;i++)
	{
		fin>>mat[i];
		for(int j=0;j<m;j++)
			if(mat[i][j]=='Y')
				x1=i,y1=j;
			else
				if(mat[i][j]=='F')
					x2=i,y2=j;
				else
					if(mat[i][j]=='X')
						tx=i,ty=j;
	}
	fin>>opr;
	ans=opr.length()+1;
	dfs(0,x1,y1,x2,y2,0);
	if(ans==opr.length()+1)
		fout<<-1<<endl;
	else
		fout<<ans<<endl;
	return 0;
}
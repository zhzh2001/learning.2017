#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("order.in");
ofstream fout("order.out");
const int N=50,MOD=1000000007;
string s[N];
int cnt[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=0;i<n;i++)
		fin>>s[i];
	int base=1,ans=0;
	for(int i=0;i<m;i++)
	{
		fill(cnt,cnt+N,0);
		for(int j=0;j<n;j++)
			if(islower(s[j][m-i-1]))
				cnt[s[j][m-i-1]-'a']++;
			else
				cnt[s[j][m-i-1]-'A'+26]++;
		sort(cnt,cnt+N,greater<int>());
		for(int j=0;j<N;j++)
			ans=(ans+(long long)base*cnt[j]*j)%MOD;
		base=(long long)base*N%MOD;
	}
	fout<<(ans+n)%MOD<<endl;
	return 0;
}
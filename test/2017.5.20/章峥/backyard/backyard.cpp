#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("backyard.in");
ofstream fout("backyard.out");
const int N=50,W=505,MOD=1000000000;
int f[W][N];
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n,w,h,d;
		fin>>n>>w>>h>>d;
		int ans=0;
		for(int x=1;x<=w;x++)
			for(int y=1;y<=h;y++)
			{
				int g=gcd(x,y),x0=x/g,y0=y/g;
				if(x+x0>w||y+y0>h)
				{
					int l=d*d/(x0*x0+y0*y0)+d*d%(x0*x0+y0*y0),r=(g+1)/n;
					memset(f,0,sizeof(f));
					f[0][0]=1;
					for(int i=1;i<=g;i++)
						for(int j=1;j<=n-1;j++)
							for(int pred=i-l;pred>=0&&pred>=i-r;pred--)
								f[i][j]=(f[i][j]+f[pred][j-1])%MOD;
					for(int i=g;i>=l*n-l;i--)
						ans=(ans+f[i][n-1]*4ll)%MOD;
				}
			}
		fout<<ans<<endl;
	}
	return 0;
}
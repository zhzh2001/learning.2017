#include<fstream>
#include<utility>
#include<map>
#include<algorithm>
using namespace std;
ifstream fin("ants.in");
ofstream fout("ants.out");
const int N=100005;
pair<int,int> a[N],now[N];
int main()
{
	int n,l,t;
	fin>>n>>l>>t;
	for(int i=1;i<=n;i++)
		fin>>a[i].first>>a[i].second;
	if(a[1].first>=t&&a[n].first+t<l)
		for(int i=1;i<=n;i++)
			if(a[i].second==1)
				fout<<a[i].first+t<<endl;
			else
				fout<<a[i].first-t<<endl;
	else
	{
		map<int,int> f;
		for(int i=1;i<=n;i++)
			f[a[i].first]=i;
		for(int i=1;i<=t;i++)
		{
			map<int,int> g;
			int m=0;
			for(int j=1;j<=n;j++)
			{
				int pred=a[j].first;
				if(a[j].second==1)
					a[j].first=(a[j].first+1)%l;
				else
					a[j].first=(a[j].first+l-1)%l;
				if(g.find(a[j].first)!=g.end())
					now[++m]=make_pair(j,g[a[j].first]);
				else
				{
					if(f.find(a[j].first)!=f.end()&&j<f[a[j].first])
					{
						int tmp=a[f[a[j].first]].first;
						if(a[f[a[j].first]].second==1)
							tmp=(tmp+1)%l;
						else
							tmp=(tmp+l-1)%l;
						if(pred==tmp)
							now[++m]=make_pair(j,f[a[j].first]);
					}
					g[a[j].first]=j;
				}
			}
			f=g;
			for(int j=1;j<=m;j++)
				swap(a[now[j].first],a[now[j].second]);
		}
		for(int i=1;i<=n;i++)
			fout<<a[i].first<<endl;
	}
	return 0;
}

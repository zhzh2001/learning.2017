#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("piling.in");
ofstream fout("piling.out");
const int MOD=1e9+7;
int n,m,ans;
bool vis[1<<10];
void dfs(int k,int a,int b,int now)
{
	if(k>m)
		vis[now]=true;
	else
	{
		if(a)
		{
			dfs(k+1,a-1,b+1,now*4);
			dfs(k+1,a,b,now*4+1);
		}
		if(b)
		{
			dfs(k+1,a,b,(now*2+1)*2);
			dfs(k+1,a+1,a-1,(now*2+1)*2+1);
		}
	}
}
int main()
{
	fin>>n>>m;
	if(m<=5)
	{
		for(int i=0;i<=n;i++)
			dfs(1,i,n-i,0);
		fout<<count(vis,vis+(1<<(m+m)),true)<<endl;
	}
	else
	{
		int ans=1;
		for(int i=1;i<=m+m;i++)
			ans=ans*2%MOD;
		fout<<ans<<endl;
	}
	return 0;
}

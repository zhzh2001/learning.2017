#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;

const int N = 1011 ; 
int n,Q,tot,L,R,x,y,Mx ; 
int sum[2011][N],pri[2011],a[N] ; 

inline int read() {
	int x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}


inline bool jud(int x) {
	for(int i=2;i*i<=x;i++) 
		if(x%i==0) return 0 ; 
	return 1 ; 
}
inline void getpri() {
	For(i,2,10000) 
		if(jud(i)) pri[++tot] = i ; 
}

inline void pre() {
	getpri() ; 
	For(i,1,n) 
	  for(int j=1;j<=tot && pri[j]<=a[i];j++)  
	  	while(a[i]&&a[i]%pri[j]==0) {
	  		a[i]/=pri[j] ; 
	  		sum[j][i]++ ; 
	  		if( j > Mx ) Mx = j ; 
		}
	tot = Mx ; 
	For(i,1,tot+1) 
	  For(j,1,n+1) 
	  	sum[i][j] = sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+sum[i][j] ; 
}

inline int query(int L,int R,int x,int y) {
	if( x > pri[tot] ) return 0 ; 
	For(i,1,tot) 
		if( x<=pri[i] ) {
			x = i ; 
			break ; 
		}
	if(y<pri[1]) return 0 ; 
	Dow(i,tot,1) 
		if( y>=pri[i] ) {
			y = i ; 
			break ; 
		}
	return sum[y][R] - sum[x-1][R] - sum[y][L-1] + sum[x-1][L-1] ; 
} 

int main() {
	freopen("prmq.in","r",stdin) ; 
	freopen("prmq.out","w",stdout) ; 
	n = read() ; 
	For(i,1,n) a[i]=read() ; 
	pre() ;
	Q = read() ; 
	while(Q--) {
		L = read() ; R = read() ; x = read() ; y = read() ; 
		printf("%d\n",query(L,R,x,y) ) ;  
	}
	return 0 ; 
}







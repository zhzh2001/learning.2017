#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;

const int N = 1e5+11 ; 
struct node{
	int L,R,dir ; 
}a[N];
int n,Len,T ;   

inline int read() {
	int x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}
inline void write(int x) {
	if(x<0) { putchar('-') ; x = -x ; }
	if(x>9) write(x/10) ;
	putchar(x%10+48) ;
}
inline void writeln(int x) { write(x) ; putchar('\n') ; }

inline bool cmp(node a,node b) { 
	return a.R < b.R ; 
}

int main() {
	freopen("ants.in","r",stdin) ; 
	freopen("ants.out","w",stdout) ; 
	n = read() ; Len = read() ; T = read() ; 
	For(i,1,n) {
		a[i].L = read() ;  
		a[i].dir = read() ; 
		if(a[i].dir==1) a[i].dir = 1 ; 
		else a[i].dir = -1 ; 
		a[i].R = a[i].L + a[i].dir*T ; 
	}
	sort(a+1,a+n+1,cmp) ; 
	For(i,1,n) 
		writeln( a[i].R%Len ) ; 
	return 0 ; 
}






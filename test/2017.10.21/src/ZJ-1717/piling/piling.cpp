#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;
inline int read() {
	int x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}

const int mod = 1e9+7 ; 
int K,n,N,sum ; 
int b[11] ; 

inline bool check() {
	For(i,0,K) {
		int x = i ,y = K-i ; 
		bool flag = 1 ; 
		int top = 0 ;  
		if(x<0||y<0) return 0 ; 
		For(j,1,N) {
			if(b[++top]==0) x--;
			else y-- ; 
			if(x<0||y<0) {
				flag = 0 ; 
				break ; 
			} 
			x++,y++ ; 
			if(b[++top]==0) x-- ; 
			else y-- ; 
			if(x<0||y<0) {
				flag = 0 ;  
				break ; 
			}
		}
		if(flag) return 1 ; 
	}
	return 0 ; 
}

inline void dfs(int x) {
	if(x>n) {
		if(check()) sum++ ; 
		return ; 
	} 
	b[x] = 0 ; dfs(x+1) ; 
	b[x] = 1 ; dfs(x+1) ;  
} 

int main() {
	freopen("piling.in","r",stdin) ; 
	freopen("piling.out","w",stdout) ; 
	K = read() ; n = read() ; N = n; n*=2 ; 
	if(K>n) {
		int ans = 1 ; 
		For(i,1,n) ans = ans*2%mod ; 
		printf("%d\n",ans) ;
		return 0 ; 
	}
	dfs(1) ; 
	printf("%d\n",sum) ; 
	return 0 ; 
}








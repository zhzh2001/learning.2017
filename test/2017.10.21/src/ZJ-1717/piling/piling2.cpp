#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
#define LL long long
using namespace std ;

const int N = 111,mod = 1e9+7 ; 
int K,n,ans ; 
int f[2][2*N][2*N] ; 

inline int read() {
	int x = 0 , f = 1 ;
	char ch = getchar() ;
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
	return x * f ;
}
inline void write(int x) {
	if(x<0) { putchar('-') ; x = -x ; }
	if(x>9) write(x/10) ;
	putchar(x%10+48) ;
}
inline void writeln(int x) { write(x) ; putchar('\n') ; }

int main() {
	K = read() ; n = read() ; 
	K*=2 ; n*=2 ; 
	
	f[0][0][0] = f[1][0][0] = 1 ; 
	For(i,1,n) 
	  For(id,0,1) {
	  	For(j,1,K) f[id][i][0]+=f[id^1][i-1][j]%=mod ; 
	  	For(j,1,K) f[id][i][j]=f[id][i-1][j-1] ; 
	  }  
	For(j,1,K) 
	    ans+=f[0][n][j]%=mod,ans+=f[1][n][j]%=mod ; 
	printf("%d\n",ans) ; 
	return 0 ; 
}








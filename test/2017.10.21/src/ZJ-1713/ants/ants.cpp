#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<map>
#include<fstream>
#include<iostream> 
using namespace std;
//ifstream fin("ants.in");
//ofstream fout("ants.out"); 
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int last[100003],now[100003];
int x[100003],w[100003];
map<int,int>mp;
int n,L,T;
struct tree{
	int movely,movery,movelz,moverz;
}t[10003];
inline void pushup(int rt){
	t[rt].movely=min(t[rt*2].movely,t[rt*2+1].movely);
	t[rt].movelz=min(t[rt*2].movelz,t[rt*2+1].movelz);
	t[rt].movery=max(t[rt*2].movery,t[rt*2+1].movery);
	t[rt].moverz=max(t[rt*2].moverz,t[rt*2+1].moverz);
}
void build(int rt,int l,int r){
	if(l==r){
		if(w[l]==1){
			t[rt].movelz=99999999;
			t[rt].moverz=-99999999;
			t[rt].movely=l;
			t[rt].movery=l;
		}else{
			t[rt].movelz=l;
			t[rt].moverz=l;
			t[rt].movely=99999999;
			t[rt].movery=-99999999;
		}
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	pushup(rt);
}
int query(int rt,int l,int r,int x,int y,int xx){
	if(l==x&&r==y){
		if(xx==1){
			return t[rt].movelz;
		}else if(xx==2){
			return t[rt].moverz;
		}else if(xx==3){
			return t[rt].movely;
		}else{
			return t[rt].movery;
		}
	}
	int mid=(l+r)/2;
	if(y<=mid){
		return query(rt*2,l,mid,x,y,xx);
	}else if(x>mid){
		return query(rt*2+1,mid+1,r,x,y,xx);
	}else{
		int q1=query(rt*2,l,mid,x,mid,xx),q2=query(rt*2+1,mid+1,r,mid+1,y,xx);
	}
}
int now2[100003],now1[100003];
int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	cin>>n>>L>>T;
	for(int i=1;i<=n;i++){
		cin>>x[i]>>w[i];
		now2[i]=x[i];
		//mp[x[i]]++;
		if(w[i]==1){
			last[i]=(x[i]+T)%L;
		}else{
			last[i]=(x[i]-T+L)%L;
		}
	}
	sort(last+1,last+n+1);
	for(int i=1;i<=n;i++){
		cout<<last[(i)%(n)+1]<<endl;
	}
	/*
	for(int i=1;i<=T;i++){
		for(int j=1;j<=n;j++){
			swap(now1[j],now2[j]);
		}
		for(int j=1;j<=n;j++){
			if(w[j]==1){
				now1[j]=(now2[j]+1)%L;
			}else{
				now1[j]=(now2[j]-1+L)%L;
			}
		}
		for(int j=1;j<=n;j++){
			if(j==1){
				if(((now2[1]>now2[n]&&now1[n]<now1[1])||(now2[1]<now2[n]&&now1[n]>now1[1]))&&(w[1]!=w[n])){
					swap(now1[1],now1[n]);
					swap(w[1],w[n]);
				}
			}else{
				if(((now2[j]>now2[j-1]&&now1[j]<now1[j-1])||(now2[j]<now2[j-1]&&now1[j]>now1[j-1]))&&(w[j]!=w[j-1])){
					swap(now1[j],now1[j-1]);
					swap(w[j],w[j-1]);
				}
			}
		}
	}
	for(int i=1;i<=n;i++){
		cout<<now1[i]<<endl;
	}
	*/
	/*
	sort(last+1,last+n+1);
	build(1,1,n);
	int time=0,move=w[1];
	for(int i=1;i<=n;i++){
		
	}
	*/
	return 0;
}
/*

in:
3 8 3
0 1
3 2
6 1

out:
1
3
0

*/

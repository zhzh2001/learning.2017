//If you give me a chance,I'll take it.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int RXD=1e9+7;
int i,j,k,l,m,n,ans;
ff f[3010][3010][2];
int main() {
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	scanf ("%d%d",&n,&m);
	f[0][n][1]=1;
	Rep(i,0,n-1) {
		f[0][i][0]=1;
	}
	Rep(i,1,m) {
		Rep(j,0,n) {
			Rep(k,(j==n?1:0),1) {
				if (!j)
					f[i][j][k]=f[i-1][j][k]+f[i-1][j+1][k];
				else 
					if (j==n) f[i][j][1]=f[i-1][j][1]+f[i-1][j-1][1]+f[i-1][j-1][0];
				else 
					f[i][j][k]=f[i-1][j-1][k]+f[i-1][j+1][k]+f[i-1][j][k]*2;
				if (j==n-1) f[i][j][k]+=(k*2-1)*f[i-1][j][0];
				f[i][j][k]%=RXD;
			}
			ans=(ans+f[m][j][1])%RXD;
		}
	}
	cout<<ans<<endl;
	return 0;
}

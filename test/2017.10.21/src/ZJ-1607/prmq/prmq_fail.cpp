//If you give me a chance,I'll take it.
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,q,t,tot;
int a[100010],p[100010],f[100010],ans[100010],sum[1000010],head[100010];
struct node {
	int l,x,y;
} e[200020];
inline int find(int k) {
	int l=1,r=t+1,mid;
	while (l<r) {
		mid=(l+r)>>1;
		if (p[mid]<k) l=mid+1; else r=mid;
	}
	return l;
}
inline void build(int x,int l,int r) {
	e[++tot].l=head[x];
	head[x]=tot;
	e[tot].x=l;
	e[tot].y=r;
}
inline void connect(int x,int k) {
	while (x<=m) {
		f[x]+=k;
		x+=x&-x;
	}
}
inline int ask(int x) { int k=0;
	while (x) {
		k+=f[x];
		x-=x&-x;
	}
	return k;
}
int query(int x,int y) {
	return ask(y)-ask(x-1);
}
int main() {
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	scanf ("%d",&n);
	Rep(i,1,n) {
		scanf ("%d",&a[i]);
		m=max(m,a[i]);
	}
	Rep(i,2,m) {
		if (!sum[i]) {
			p[++t]=i;
			sum[i]=t;
		}
		for (int j=1;i*p[j]<=m;j++) {
			sum[i*p[j]]=j;
			if (i%p[j]==0) break;
		}
	}
	tot=1;
	scanf ("%d",&q);
	Rep(i,1,q) {
		int l,r,x,y,k=y;
		scanf ("%d%d%d%d",&l,&r,&x,&y);
		x=find(x);
		y=find(y);
		if (p[y]!=k) y--;
		build(l-1,x,y);
		build(r,x,y);
	}
	Rep(i,1,n) {
		int k=a[i];
		while (k>1) {
			int Sum=0,S=sum[k];
			while (k%p[S]==0) {
				k/=p[S];
				Sum++;
			}
			connect(S,Sum);
		}
		for (int j=head[i];j;j=e[j].l) 
			if (j&1) ans[j>>1]+=ask(e[j].y)-ask(e[j].x-1);
				else ans[j>>1]-=ask(e[j].y)-ask(e[j].x-1);
	}
	Rep(i,1,q) {
		cout<<ans[i]<<endl;
	}
	return 0;
}

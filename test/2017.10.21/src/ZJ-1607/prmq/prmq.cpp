//If you give me a chance,I'll take it.
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,q,ans,num,now;
int a[10000];
inline bool isPrime(int x) {
	int xx=sqrt(x);
	Rep(i,2,xx) {
		if (x%i==0) return false;
	}
	return true;
}
int main() {
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	scanf ("%d",&n);
	Rep(i,1,n) {
		scanf ("%d",&a[i]);
	}
	scanf ("%d",&q);
	while (q--) {
		int l,r,x,y;
		scanf ("%d%d%d%d",&l,&r,&x,&y);
		ans=0;
		Rep(i,x,y) {
			if (isPrime(i)) 
				Rep(j,l,r) {
					num=a[j];
					now=0;
					while (num%i==0) {
						now++;
						num/=i;
					}
					ans+=now;
				}
		}
		cout<<ans<<endl;
	}
	return 0;
}

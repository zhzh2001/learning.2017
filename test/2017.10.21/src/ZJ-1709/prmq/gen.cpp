#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
int main(){
	freopen("prmq.in","w",stdout);
	mt19937 w(GetTickCount());
	int n=w()%100+1,q=w()%100+1;
	printf("%d\n",n);
	for(int i=1;i<=n;i++)
		printf("%d ",w()%1000+1);
	printf("\n%d\n",q);
	while(q--){
		int l=w()%n+1,r=w()%n+1,x=w()%1000+1,y=w()%1000+1;
		if (l>r)
			swap(l,r);
		if (x>y)
			swap(x,y);
		printf("%d %d %d %d\n",l,r,x,y);
	}

}
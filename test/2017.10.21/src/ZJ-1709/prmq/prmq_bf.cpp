#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("prmq.in","r",stdin);
	freopen("prmq_bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
bool ntprime[1000002];
int a[100002];
int f(int l,int r,int x,int y){
	int res=0;
	for(int i=x;i<=y;i++){
		if (!ntprime[i]){
			for(int j=l;j<=r;j++){
				int num=a[j],cnt=0;
				while(num%i==0)
					cnt++,num/=i;
				res+=cnt;
			}
		}
	}
	return res;
}
int main(){
	ntprime[1]=true;
	for(int i=2;i<=1000000;i++)
		if (!ntprime[i])
			for(int j=i*2;j<=1000000;j+=i)
				ntprime[j]=true;
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	int m=readint();
	while(m--){
		int l=readint(),r=readint(),x=readint(),y=readint();
		printf("%d\n",f(l,r,x,y));
	}
}
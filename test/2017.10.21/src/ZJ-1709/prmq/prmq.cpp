#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=1000002;
const int maxm=400002;
struct bit{
	int n;
	int a[maxn];
	void add(int p,int x){
		for(;p<=n;p+=p&-p)
			a[p]+=x;
	}
	int query(int p){
		int ans=0;
		for(;p;p-=p&-p)
			ans+=a[p];
		return ans;
	}
}b;
pair<int,int> fac[maxn][22];
int num[maxn];
struct query{
	int p,x,y,id,sgn;
	bool operator <(const query& rhs)const{
		return p<rhs.p;
	}
}q[maxm];
int a[maxm],ans[maxm];
int main(){
	init();
	for(int i=2;i<=1000000;i++)
		if (!num[i])
			for(int j=i;j<=1000000;j+=i){
				int w=j,cnt=0;
				while(w%i==0)
					w/=i,cnt++;
				//if (j<=4)
					//printf("%d %d %d\n",j,i,w);
				fac[j][++num[j]]=make_pair(i,cnt);
			}
	//fprintf(stderr,"%d\n",clock());
	int n=readint(),cur=0;
	for(int i=1;i<=n;i++)
		a[i]=readint();
	int m=readint();
	for(int i=1;i<=m;i++){
		int l=readint(),r=readint(),x=readint(),y=readint();
		q[++cur]=(query){l-1,x,y,i,-1};
		q[++cur]=(query){r,x,y,i,1};
	}
	sort(q+1,q+cur+1);
	int now=0;
	b.n=1000000;
	for(int i=1;i<=cur;i++){
		while(now<q[i].p){
			now++;
			int t=a[now];
			pair<int,int> *qwq=fac[t];
			//printf("on %d\n",now);
			for(int j=1;j<=num[t];j++){
				//printf("adding %d %d\n",qwq[j].first,qwq[j].second);
				b.add(qwq[j].first,qwq[j].second);
			}
		}
		ans[q[i].id]+=q[i].sgn*(b.query(q[i].y)-b.query(q[i].x-1));
	}
	for(int i=1;i<=m;i++)
		printf("%d\n",ans[i]);
}
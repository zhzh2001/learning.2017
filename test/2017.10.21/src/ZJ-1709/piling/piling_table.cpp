#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int mod=1000000007;
int f[202][105][105],g[202][105][105];
void add(int &x,int y){
	x+=y;
	if (x>=mod)
		x-=mod;
}
int ans[105][205];
int main(){
	init();
	int n=readint(),m=readint();
	f[0][0][0]=1;
	for(int i=0;i<m;i++){
		memset(g,0,sizeof(g));
		for(int j=0;j<=2*m;j++){
			for(int k=0;k<=m;k++){
				for(int o=0;o<=m;o++){
					add(g[j+2][max(k,j-i+1)][o],f[j][k][o]);
					add(g[j+1][max(k,j-i+1)][o],f[j][k][o]);
					add(g[j+1][k][max(o,i-j+1)],f[j][k][o]);
					add(g[j][k][max(o,i-j+1)],f[j][k][o]);
				}
			}
		}
		for(int j=0;j<=2*m;j++)
			for(int k=0;k<=m;k++)
				for(int o=0;o<=m;o++)
					add(ans[i+1][k+o],g[j][k][o]);
		for(int j=1;j<=n;j++)
			add(ans[i+1][j],ans[i+1][j-1]);
		swap(f,g);
	}
	puts("int a[100][100]={");
	for(int i=1;i<=m;i++){
		putchar('{');
		for(int j=1;j<=i;j++)
			printf("%d%c",ans[i][j],",}"[j==n]);
		for(int j=i+1;j<=n;j++)
			printf("%d%c",0,",}"[j==n]);
		puts(",");
	}
	puts("};");
}
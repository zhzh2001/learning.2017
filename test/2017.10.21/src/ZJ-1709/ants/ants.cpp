#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
struct ant{
	int id,x,dir;
	bool operator <(const ant& rhs)const{
		return x<rhs.x;
	}
}a[maxn];
int b[maxn],ans[maxn];
int main(){
	init();
	int n=readint(),l=readint(),T=readint();
	for(int i=1;i<=n;i++){
		a[i].x=readint(),a[i].dir=readint()==1?1:-1;
		a[i].id=i;
		b[i]=((a[i].x+T*a[i].dir)%l+l)%l;
	}
	if (T<=100){
		T*=2;
		l*=2;
		for(int i=1;i<=n;i++)
			a[i].x*=2;
		while(T--){
			for(int i=1;i<=n;i++){
				a[i].x+=a[i].dir;
				if (a[i].x==l)
					a[i].x=0;
				if (a[i].x<0)
					a[i].x=l-1;
			}
			for(int i=1;i<=n;i++){
				int nxt=i==n?1:i+1;
				if (a[i].x==a[nxt].x)
					swap(a[i].dir,a[nxt].dir);
			}
		}
		for(int i=1;i<=n;i++)
			printf("%d\n",a[i].x/2);
		return 0;
	}
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
		printf("%d\n",b[i]);
}
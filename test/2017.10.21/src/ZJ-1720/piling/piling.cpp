#include<cstdio>
#define ll long long
#define mo 1000000007
using namespace std;
int f[3005][3005][4],p[6005],ans;
int n,m;
inline int get(int x){
	return !x?1:x==n?2:0;
}
inline void upd(int &x,int y){
	x=x+y;
	x=x>=mo?x-mo:x;
}
int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<=n;i++)
		f[0][i][get(i)]=1;
	p[0]=1;
	for (int i=1;i<=2*m;i++) p[i]=p[i-1]*2%mo;
	ans=p[m*2];
	if (n<m){
		for (int i=1;i<=m;i++)
			for (int jzq=0;jzq<4;jzq++)
				for (int R=0;R<=n;R++){
					int B=n-R;
					if (R){
						upd(f[i][R-1][jzq|get(R-1)],f[i-1][R][jzq]);
					upd(f[i][R][jzq|get(R-1)],f[i-1][R][jzq]);
					}
					else if (jzq==3)
						upd(ans,-1ll*f[i-1][R][jzq]*p[2*(m-i)+1]%mo+mo);
					if (B){
						upd(f[i][R+1][jzq|get(R+1)],f[i-1][R][jzq]);
						upd(f[i][R][jzq|get(R+1)],f[i-1][R][jzq]);
					}
					else if (jzq==3)
						upd(ans,-1ll*f[i-1][R][jzq]*p[2*(m-i)+1]%mo+mo);
			}
	}
	printf("%d\n",ans);
}


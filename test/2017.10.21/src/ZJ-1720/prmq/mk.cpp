#include<bits/stdc++.h>
using namespace std;
int rnd(){
	int x=0;
	for (int i=1;i<=6;i++)
		x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("prmq.in","w",stdout);
	printf("%d\n",100000);
	for (int i=1;i<=100000;i++) printf("%d ",rnd()+1);
	puts("");
	printf("%d\n",100000);
	for (int i=1;i<=100000;i++){
		int l=rnd()%100000+1,r=rnd()%100000+1;
		int x=rnd()+1,y=rnd()+1;
		if (l>r) swap(l,r);
		if (x>y) swap(x,y);
		printf("%d %d %d %d\n",l,r,x,y);
	}
}

#include<bits/stdc++.h>
#define N 100005
using namespace std;
namespace fastIO{
	const int L=2333333;
	char LZH[L],*S=LZH,*T=LZH;
	inline char gc(){
		if (S==T){
			T=(S=LZH)+fread(LZH,1,L,stdin);
			if (S==T) return EOF;
		}
		return *S++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline void write(int x){
		if (!x){
			puts("0");
			return;
		}
		static int a[15],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar('0'+a[top--]));
		puts("");
	}
}
int n,Q,l,r,x,y,tot;
int a[N],ans[N],t[N*10];
struct que{int x,y,v,id;}q[N*4];
bool cmp(que a,que b){
	return a.x<b.x;
}
void add(int x,int y){
	for (;x<=1000000;x+=x&(-x)) t[x]+=y;
}
int ask(int x){
	int s=0;
	for (;x;x-=x&(-x)) s+=t[x];
	return s;
}
int main(){
	using namespace fastIO;
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
		a[i]=read();
	Q=read();
	for (int i=1;i<=Q;i++){
		l=read(); r=read(); x=read(); y=read();
		q[++tot]=(que){r,y,1,i};
		q[++tot]=(que){r,x-1,-1,i};
		q[++tot]=(que){l-1,y,-1,i};
		q[++tot]=(que){l-1,x-1,1,i};
	}
	sort(q+1,q+tot+1,cmp);
	int now=1;
	for (;now<=tot&&!q[now].x;now++);
	for (int i=1;i<=n;i++){
		for (int j=2;j*j<=a[i];j++)
			if (a[i]%j==0){
				int tmp=0;
				for (;a[i]%j==0;a[i]/=j,tmp++);
				add(j,tmp);
			}
		if (a[i]!=1) add(a[i],1);
		for (;now<=tot&&q[now].x==i;now++)
			ans[q[now].id]+=ask(q[now].y)*q[now].v;
	}
	for (int i=1;i<=Q;i++)
		write(ans[i]);
}

#include<bits/stdc++.h>
#define N 100005
#define pa pair<int,int>
using namespace std;
namespace fastIO{
	const int L=2333333;
	char LZH[L],*A=LZH,*B=LZH;
	inline char gc(){
		if (A==B){
			B=(A=LZH)+fread(LZH,1,L,stdin);
			if (A==B) return EOF;
		}
		return *A++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline void write(int x){
		if (!x){
			puts("0");
			return;
		}
		static int a[15],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar('0'+a[top--]));
		puts("");
	}
}
int x[N],y[N],lap[N];
int n,l,T,pl;
set<pa> s1,s2;
int getid(int T,int jzq){
	int nowT=0;
	for (;;){
		if (y[jzq]==1){
			if (s2.empty()) break;
			int tmp=(x[jzq]+nowT)%l;
			pa ano=*s2.lower_bound(pa(tmp,233333));
			if (T*2-nowT<ano.first-tmp) break;
			jzq=ano.second;
			nowT+=ano.first-tmp;
		}
		else{
			if (s1.empty()) break;
			int tmp=((x[jzq]-nowT)%l+l)%l+l;
			set<pa>::iterator it=s1.lower_bound(pa(tmp,-233333));
			pa ano=*(--it);
			if (T*2-nowT<tmp-ano.first) break;
			jzq=ano.second;
			nowT+=tmp-ano.first;
		}
	}
	return jzq;
}
int main(){
	using namespace fastIO;
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read(); l=read(); T=read();
	for (int i=1;i<=n;i++){
		x[i]=read(); y[i]=read();
		lap[i]=((x[i]+(y[i]==1?1:-1)*T)%l+l)%l;
	}
	sort(lap+1,lap+n+1);
	for (int i=1;i<=n;i++)
		if (y[i]==1){
			s1.insert(pa(x[i],i));
			s1.insert(pa(x[i]+l,i));
		}
		else{
			s2.insert(pa(x[i],i));
			s2.insert(pa(x[i]+l,i));
		}
	int delta=getid(l,1)-1;
	int jzq=getid(T%l,delta*(T/l)%n+1);
	int jzq2=getid(T%l,(delta*(T/l)+1)%n+1);
	int firp=((x[jzq]+(y[jzq]==1?1:-1)*T)%l+l)%l;
	int secp=((x[jzq2]+(y[jzq2]==1?1:-1)*T)%l+l)%l;
	for (pl=1;pl<=n;pl++)
		if (lap[pl]==firp&&lap[pl%n+1]==secp) break;
	for (int i=1;i<=n;i++,pl=pl%n+1)
		write(lap[pl]);
}

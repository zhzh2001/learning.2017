#include<bits/stdc++.h>
using namespace std;
int n,l,T,x[1005],y[1005];
int main(){
	freopen("ants.in","r",stdin);
	freopen("233.out","w",stdout);
	scanf("%d%d%d",&n,&l,&T);
	l*=2; T*=2;
	for (int i=1;i<=n;i++){
		scanf("%d%d",&x[i],&y[i]);
		x[i]*=2; y[i]=(y[i]==1?1:-1);
	}
	for (int i=1;i<=T;i++){
		for (int j=1;j<=n;j++)
			x[j]=(x[j]+y[j]+l)%l;
		for (int j=1;j<=n;j++)
			if (x[j]==x[j%n+1])
				swap(y[j],y[j%n+1]);
	}
	for (int i=1;i<=n;i++)
		printf("%d\n",x[i]/2);
}

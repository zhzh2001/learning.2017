#include<cstdio>
#include<algorithm>
using namespace std;
int i,j,x,y,n,q,cnt,tmp[999999],p[80000],lab,rt[100001];
struct Z{int ls,rs,d;}a[12500000];
void A(int&p,int L,int R,int l,int x)
{
	a[++lab]=a[p],p=lab;
	if(a[p].d+=x,L==R)return;
	int mid=L+R>>1;
	mid<l?A(a[p].rs,mid+1,R,l,x):A(a[p].ls,L,mid,l,x);
}
int Q(int p,int q,int L,int R,int l,int r)
{
	if(L==l&&R==r)return a[q].d-a[p].d;
	int mid=L+R>>1;
	if(r<=mid)return Q(a[p].ls,a[q].ls,L,mid,l,r);
	if(l>mid)return Q(a[p].rs,a[q].rs,mid+1,R,l,r);
	return Q(a[p].ls,a[q].ls,L,mid,l,mid)+Q(a[p].rs,a[q].rs,mid+1,R,mid+1,r);
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	for(i=2;i<999999;++i)
	{
		if(!tmp[i])p[++*p]=i;
		for(j=1;j<=*p&&i*p[j]<999999;++j)
			if(tmp[i*p[j]]=1,i%p[j]==0)break;
	}
	for(scanf("%d",&n),i=0;i++<n;)
	{
		scanf("%d",&q),rt[i]=rt[i-1];
		for(j=1;p[j]*p[j]<=q;++j)if(q%p[j]==0)
		{
			for(cnt=0;q/=p[j],++cnt,q%p[j]==0;);
			A(rt[i],1,*p,j,cnt);
		}
		if(q>1)A(rt[i],1,*p,lower_bound(p+1,p+*p+1,q)-p,1);
	}
	for(scanf("%d",&q);q--;)
	{
		scanf("%d%d%d%d",&i,&j,&x,&y);
		x=lower_bound(p+1,p+*p+1,x)-p;
		y=upper_bound(p+1,p+*p+1,y)-p-1;
		x>y?puts("0"):printf("%d\n",Q(rt[i-1],rt[j],1,*p,x,y));
	}
}

#include<fstream>
using namespace std;
ifstream fin("prmq.in");
ofstream fout("prmq.ans");
const int N=100005;
int a[N];
bool prime(int x)
{
	for(int i=2;i*i<=x;i++)
		if(x%i==0)
			return false;
	return true;
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	int q;
	fin>>q;
	while(q--)
	{
		int l,r,x,y;
		fin>>l>>r>>x>>y;
		int ans=0;
		for(int i=x;i<=y;i++)
			if(prime(i))
				for(int j=l;j<=r;j++)
				{
					int x=a[j];
					for(;x%i==0;x/=i)
						ans++;
				}
		fout<<ans<<endl;
	}
	return 0;
}

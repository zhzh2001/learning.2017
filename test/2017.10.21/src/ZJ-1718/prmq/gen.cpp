#include<fstream>
#include<random>
#include<ctime>
#include<algorithm>
using namespace std;
ofstream fout("prmq.in");
const int n=1e5,q=1e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<endl;
	uniform_int_distribution<> d(2,20),dn(1,n);
	for(int i=1;i<=n;i++)
		fout<<510510<<' ';
	fout<<endl<<q<<endl;
	for(int i=1;i<=q;i++)
	{
		int l=dn(gen),r=dn(gen),x=d(gen),y=d(gen);
		if(l>r)
			swap(l,r);
		if(x>y)
			swap(x,y);
		fout<<l<<' '<<r<<' '<<x<<' '<<y<<endl;
	}
	return 0;
}

#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("prmq.in");
ofstream fout("prmq.out");
const int N = 100005, V = 1e6;
int a[N], p[N], cc, root[N * 8], tr[N];
bool bl[V + 5];
struct node
{
	int sum, ls, rs;
} tree[N * 150];
void modify(int &id, int pred, int l, int r, int x, int val)
{
	id = ++cc;
	tree[id] = tree[pred];
	if (l == r)
		tree[id].sum += val;
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(tree[id].ls, tree[pred].ls, l, mid, x, val);
		else
			modify(tree[id].rs, tree[pred].rs, mid + 1, r, x, val);
		tree[id].sum = tree[tree[id].ls].sum + tree[tree[id].rs].sum;
	}
}
int query(int rl, int rr, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[rr].sum - tree[rl].sum;
	int mid = (l + r) / 2, sum = 0;
	if (L <= mid)
		sum += query(tree[rl].ls, tree[rr].ls, l, mid, L, R);
	if (R > mid)
		sum += query(tree[rl].rs, tree[rr].rs, mid + 1, r, L, R);
	return sum;
}
int main()
{
	int pn = 0;
	for (int i = 2; i <= V; i++)
	{
		if (!bl[i])
			p[++pn] = i;
		for (int j = 1; j <= pn && i * p[j] <= V; j++)
		{
			bl[i * p[j]] = true;
			if (i % p[j] == 0)
				break;
		}
	}
	int n;
	fin >> n;
	int rn = 0;
	for (int i = 1; i <= n; i++)
	{
		int x;
		fin >> x;
		for (int j = 1; p[j] * p[j] <= x && x > 1; j++)
			if (x % p[j] == 0)
			{
				int cnt = 0;
				for (; x % p[j] == 0; x /= p[j])
					cnt++;
				rn++;
				modify(root[rn], root[rn - 1], 1, pn, j, cnt);
			}
		if (x > 1)
		{
			rn++;
			modify(root[rn], root[rn - 1], 1, pn, lower_bound(p + 1, p + pn + 1, x) - p, 1);
		}
		tr[i] = rn;
	}
	int q;
	fin >> q;
	while (q--)
	{
		int l, r, x, y;
		fin >> l >> r >> x >> y;
		x = lower_bound(p + 1, p + pn + 1, x) - p;
		y = upper_bound(p + 1, p + pn + 1, y) - p - 1;
		fout << query(root[tr[l - 1]], root[tr[r]], 1, pn, x, y) << endl;
	}
	return 0;
}

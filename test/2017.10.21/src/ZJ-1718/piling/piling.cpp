#include <fstream>
using namespace std;
ifstream fin("piling.in");
ofstream fout("piling.out");
const int N = 3005, MOD = 1e9 + 7;
int f[N][N][2];
int main()
{
	int n, m;
	fin >> n >> m;
	if (n >= m)
	{
		int ans = 1;
		for (int i = 1; i <= m; i++)
			ans = ans * 4ll % MOD;
		fout << ans << endl;
		return 0;
	}
	for (int i = 0; i < n; i++)
		f[0][i][i == 0] = 1;
	for (int i = 0; i < m - 1; i++)
		for (int j = 0; j <= n; j++)
			for (int k = 0; k < 2; k++)
				if (f[i][j][k])
				{
					if (j)
						(f[i + 1][j - 1][k || j - 1 == 0] += f[i][j][k]) %= MOD;
					(f[i + 1][j][k] += 2 * f[i][j][k] % MOD) %= MOD;
					if (j + 1 < n)
						(f[i + 1][j + 1][k] += f[i][j][k]) %= MOD;
				}
	int ans = 0;
	for (int i = 0; i < n; i++)
		(ans += f[m - 1][i][1]) %= MOD;
	fout << ans * 4ll % MOD << endl;
	return 0;
}
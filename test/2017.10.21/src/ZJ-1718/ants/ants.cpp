#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("ants.in");
ofstream fout("ants.out");
const int N = 1e5;
int a[N];
int main()
{
	int n, l, t;
	fin >> n >> l >> t;
	int start = 0;
	for (int i = 0; i < n; i++)
	{
		int x, dir;
		fin >> x >> dir;
		if (dir == 1)
		{
			a[i] = (x + t) % l;
			start = (start + (x + t - a[i]) / l) % n;
		}
		else
		{
			a[i] = ((x - t) % l + l) % l;
			start = (start + (x - t - a[i]) / l) % n;
		}
	}
	sort(a, a + n);
	start = (start % n + n) % n;
	rotate(a, a + start, a + n);
	for (int i = 0; i < n; i++)
		fout << a[i] << endl;
	return 0;
}
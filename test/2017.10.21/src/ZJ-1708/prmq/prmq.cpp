#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int pri[1005];
bool mark[1005];
inline void get_pri(){
	For(i,2,1000){
		if(!mark[i]) pri[++pri[0]]=i;
		for(int j=1;j<=pri[0]&&i*pri[j]<=1000;j++){
			mark[i*pri[j]]=1;
			if(i%pri[j]==0) break;
		}
	}
}
int n,p[N][171],a[N];
int block,bl[N],ans[N];
struct que{
	int L,R,l,r,id;
	bool operator <(const que &a)const{
		return (bl[L]==bl[a.L])?R<a.R:bl[L]<bl[a.L];
	}
}q[N];
int t[1000005];
inline void add(int x,int val){for(;x<=1000002;x+=(x&-x)) t[x]+=val; }
inline int query(int x){int sum=0;for(;x;x-=(x&-x)) sum+=t[x];return sum;}
inline int solve(int L,int R,int l,int r){
	For(i,1,pri[0]) if(pri[i]>=l){l=i;break;}
	Rep(i,pri[0],1) if(pri[i]<=r){r=i;break;}
	return (p[R][r]-p[R][l-1])+(p[L-1][r]-p[L-1][l-1]);
}
inline int solve1(int L,int R,int l,int r){
	int res=p[R][pri[0]]-p[R][l-1]-(p[L-1][pri[0]]-p[L-1][l-1]);
	return res+query(r);
}
inline int solve2(int L,int R,int l,int r){
	return query(r)-query(l-1);
}
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	get_pri();
	n=read();
	block=sqrt(n);
	For(i,1,n) bl[i]=(i-1)/block+1;
	For(i,1,n){
		int y,x;y=x=read();
		for(int j=1;j<=pri[0];j++){
			while(x%pri[j]==0) x/=pri[j],p[i][j]++;
			if(x==1) break;
		}
		if(x==1) a[i]=1000001;
		else a[i]=x;
		for(int j=1;j<=pri[0];j++) p[i][j]=p[i][j]+p[i][j-1];
		for(int j=1;j<=pri[0];j++) p[i][j]+=p[i-1][j];
	}
	int Q=read();
	For(i,1,Q) q[i]=(que){read(),read(),read(),read(),i};
	sort(q+1,q+1+Q);
	int l=q[1].L,r=q[1].R;
	For(i,l,r) add(a[i],1);
	if(q[1].r<=1000) ans[q[1].id]=solve(l,r,q[1].l,q[1].r);
	else if(q[1].l<=1000&&q[1].r>1000) ans[q[1].id]=solve1(l,r,q[1].l,q[1].r);
	else ans[q[1].id]=solve2(l,r,q[1].l,q[1].r);
	For(i,2,Q){
		while(l<q[i].L) add(a[l],-1),l++;
		while(l>q[i].L) l--,add(a[l],1);
		while(r<q[i].R) r++,add(a[r],1);
		while(r>q[i].R) add(a[r],-1),r--;
		if(q[i].r<=1000) ans[q[i].id]=solve(l,r,q[i].l,q[i].r);
		else if(q[i].l<=1000&&q[i].r>1000) ans[q[i].id]=solve1(l,r,q[i].l,q[i].r);
		else ans[q[i].id]=solve2(l,r,q[i].l,q[i].r);
	}
	For(i,1,Q) printf("%d\n",ans[i]);
	return 0;
}

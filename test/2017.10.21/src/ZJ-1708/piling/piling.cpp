#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define mod 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,m,f[6005][6005][3],ans;
int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	n=read();m=read();
	f[0][0][2]=1;
	For(i,0,n+m) For(j,0,n+m){
		if(i+j>m*2) continue;
		if(j) f[i][j][1]=((f[i][j][1]+f[i][j-1][1]+f[i][j-1][2])%mod+f[i][j-1][0])%mod;
		if(i) f[i][j][0]=((f[i][j][0]+f[i-1][j][1]+f[i-1][j][2])%mod+f[i-1][j][0])%mod;
	}
	For(i,0,n+m){
		int x=i,y=m*2-i;
		if(y<0||y>n+m) continue;
		ans=(ans+(f[x][y][0]+f[x][y][1])%mod)%mod;
	}
	printf("%d\n",ans);
	return 0;
}

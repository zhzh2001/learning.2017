#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,L,T,a[N],x[N],p[N];
inline void solve1(){
	while(T--){
		For(i,1,n) p[i]=x[i];
		For(i,1,n) if(x[i]==1){
			if((a[i]+1)%L==a[i%n+1]&&p[i%n+1]==-1) x[i]=-1;
			else a[i]=(a[i]+1)%L;
		}else{
		//	cout<<a[(i-1+n)%n]<<" "<<(a[i]-1+L)%L<<" "<<p[(i-1+n)%n]<<endl;
			if(a[(i-2+n)%n+1]==(a[i]-1+L)%L&&p[(i-2+n)%n+1]==1) x[i]=1;
			else a[i]=(a[i]-1+L)%L;
		}
		For(i,1,n){
			if(x[i]==1&&a[i]==a[i%n+1]) x[i]=-1;
			else if(x[i]==-1&&a[i]==a[(i-2+n)%n+1]) x[i]=1;
		}
//		For(i,1,n) cout<<a[i]<<" ";cout<<endl;
//		For(i,1,n) cout<<x[i]<<" ";cout<<endl;
	}
	For(i,1,n) cout<<a[i]<<endl;
}
int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read();L=read();T=read();
	For(i,1,n) a[i]=read(),x[i]=read();
	For(i,1,n) if(x[i]==2) x[i]=-1;
	if(T<=100) solve1();
	return 0;
}

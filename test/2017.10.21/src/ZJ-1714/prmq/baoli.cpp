#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll int
#define maxn 2010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=1000010;
bool mark[N];
ll a[N],n,Q;
inline ll work(ll L,ll R,ll x,ll y){
	ll ans=0;
	For(i,x,y)	if (!mark[i]){
		For(j,L,R){
			ll number=a[j];
			while(number%i==0)	ans++,number/=i;
		}
	}return ans;
}
int main(){
	freopen("prmq.in","r",stdin);
	freopen("baoli.out","w",stdout);
	mark[1]=1;
	For(i,2,1000000)	if (!mark[i])	for(ll j=i*2;j<=1000000;j+=i)	mark[j]=1;
	n=read();
	For(i,1,n)	a[i]=read();
	Q=read();
	For(i,1,Q){
		ll l=read(),r=read(),x=read(),y=read();
		writeln(work(l,r,x,y));
	}
}

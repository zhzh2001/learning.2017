#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#include<ctime>
#define ll long long
#define mk make_pair
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=1000010;
struct data{	ll inf,l,r,id;	};
vector<data>g[N];	bool mark[N];
ll c[N],a[N],answ[N],pri[N],n,Q,max_val=1000000;
inline void add(ll x){	for(;x<=max_val;x+=x&-x)	c[x]++;	}
inline ll ask(ll x){
	ll ans=0;
	for(;x;x-=x&-x)	ans+=c[x];
	return ans;
}
inline void fenjie(ll x){
	for(ll i=1;pri[i]*pri[i]<=x;i++)	while(!(x%pri[i]))	add(pri[i]),x/=pri[i];
	if (x>1)	add(x);
}
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	For(i,2,10000)	if (!mark[i]){
		for(ll j=i*2;j<=10000;j+=i)	mark[j]=1;
		pri[++pri[0]]=i;
	}
	n=read();
	For(i,1,n)	a[i]=read();
	Q=read();
	For(i,1,Q){
		ll l=read(),r=read(),x=read(),y=read();
		g[l-1].push_back((data){-1,x,y,i});
		g[r].push_back((data){1,x,y,i});
	} 
	For(x,1,n){
		fenjie(a[x]);
		for(ll i=0;i<g[x].size();i++)	answ[g[x][i].id]+=g[x][i].inf*(ask(g[x][i].r)-ask(g[x][i].l-1));
	}
	For(i,1,Q)	writeln(answ[i]);
}
/*
问区间L,R质因数分解后有几个数是在x,y之间 
4
2 3 4 5
2
1 3 2 3
1 4 2 5
*/

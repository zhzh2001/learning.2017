#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 400010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=100010;
ll a[N],w[N],q[N],answ[N],L,T,n,tot;
ll get(ll x){	return x==1?1:-1;	}
ll pre(ll x){	return (x+n-1)%n;	}
ll nxt(ll x){	return (x+1)%n;	}
void work(){
	For(i,1,n)	q[++tot]=(a[i]+w[i]*T%L+L)%L;
	sort(q+1,q+tot+1);
	For(i,1,tot)	writeln(q[i]/2);
	exit(0);
}
void work1(){
	For(i,1,n)	q[++tot]=(a[i]+w[i]*T%L+L)%L;
	sort(q+1,q+tot+1);
	ll pos=1;
	For(i,2,n){
		if (a[1]==a[i])	continue;
		ll cang=T*2;
		if (a[1]==1){		if (a[i]-a[1]<=cang)pos+=(cang-(a[i]-a[1]))/L+1;	}
		else{
			ll tt=L-(a[i]-a[1]);
			if (tt<=cang)	pos+=(cang-tt)/L+1;
		}
	}
	For(i,1,tot){
		answ[pos]=i;
		pos=pos%n+1;
	}
	For(i,1,tot)	writeln(q[answ[i]]/2);
	exit(0);
}
int main(){
	freopen("ants.in","r",stdin);
//	freopen("ants.out","w",stdout);
	n=read();	L=read()*2;	T=read()*2;
	For(i,0,n-1)	a[i]=read()*2,w[i]=get(read());
	if (a[1]-T*2>=0&&a[n]+T*2<L)	work();
	if (T>100)	work1();
	For(tim,1,T){
		For(i,0,n-1)	a[i]=(a[i]+w[i]+L)%L;		
		For(i,0,n-1)	if (a[i]==a[nxt(i)])	swap(w[i],w[nxt(i)]),i++;
	}
	For(i,0,n-1)	writeln(a[i]/2);
}
/*
	3 8 3
	0 1
		3 2
	6 1
*/

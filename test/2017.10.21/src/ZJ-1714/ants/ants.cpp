#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 400010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=100010;
ll a[N],w[N],q[N],answ[N],L,T,n,tot;
ll get(ll x){	return x==1?1:-1;	}
int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read();	L=read();	T=read();
	For(i,1,n){
		a[i]=read();	w[i]=read()==1?1:-1;
		q[++tot]=(a[i]+w[i]*T%L+L)%L;
	}sort(q+1,q+tot+1);
	ll pos=1;
	For(i,2,n){
		if (a[1]==a[i])	continue;
		ll cang=T*2;
		if (a[1]==1){		if (a[i]-a[1]<=cang)pos+=((cang-(a[i]-a[1]))/L+1)*w[1];	}
		else{
			ll tt=L-(a[i]-a[1]);
			if (tt<=cang)	pos+=((cang-tt)/L+1)*w[1];
		}
	}
	pos%=n;
	pos+=n;
	pos%=n;
	if (!pos)	pos++;
	For(i,1,tot){
		answ[pos]=i;
		pos=(pos+1)%n;
		if (!pos)pos++;
	}
	For(i,1,tot)	writeln(q[answ[i]]);
}
/*
3 8 3
0 1
3 2
6 1
*/

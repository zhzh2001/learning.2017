#include<cstdio>
#include<vector>
#include<cstring>
#include<memory.h>
#include<algorithm>
#include<ctime>
#define ll int
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);putchar(x%10+'0');  }
inline void writeln(ll x){ write(x);   puts("");   }
const ll mod=1e9+7,N=3010;
ll f[N][N][2],m,n,ans;
inline void add(ll &x,ll y){	x=(x+y)%mod;	}
int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	m=read();	n=read();
	if (n<=m){
		ll ans=1;
		For(i,1,n*2)	add(ans,ans);
		writeln(ans);
		return 0;
	}
	For(mxa,0,m){
		For(i,0,n*2)	For(j,0,2*n)	f[i][j][0]=f[i][j][1]=0;
		ll mxb=m-mxa;	f[0][0][mxa==0]=1;
		For(i,1,2*n)	For(a,0,i)	For(ok,0,1)
		if (f[i-1][a]){
			if ((a+1)-i/2<=mxa)	add(f[i][a+1][ok|((a+1)-i/2==mxa)],f[i-1][a][ok]);
			if ((i+1)/2-a<=mxb)	add(f[i][a][ok],f[i-1][a][ok]);
		}
		For(i,0,2*n)	add(ans,f[n*2][i][1]);
	}
	writeln(ans);
}
/*
枚举mxa
强制一维为mxa
bool 0,1记录可不可以
然后另一维小于等于mxb
f[i][a]
代表前缀和为a 
*/

#include<cstdio>
#include<vector>
#include<cstring>
#include<memory.h>
#include<algorithm>
#include<ctime>
#define ll int
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);putchar(x%10+'0');  }
inline void writeln(ll x){ write(x);   puts("");   }
const ll mod=1e9+7,N=110;
ll f[2][N][N],m,n,ans;
inline void add(ll &x,ll y){	x=(x+y)%mod;	}
int main(){
	m=read();	n=read();
	For(mxa,0,m){
		memset(f,0,sizeof f);
		ll mxb=m-mxa;	f[mxa==0][0][0]=1;
		For(i,1,2*n)	For(a,0,i)	For(ok,0,1)
		if (f[i-1][a]){
			if ((a+1)-i/2<=mxa)	add(f[ok|((a+1)-i/2==mxa)][i][a+1],f[ok][i-1][a]);
			if ((i+1)/2-a<=mxb)	add(f[ok][i][a],f[ok][i-1][a]);
		}
		For(i,0,2*n)	add(ans,f[1][n*2][i]);
	}
	writeln(ans);
}
/*
枚举mxa
强制一维为mxa
bool 0,1记录可不可以
然后另一维小于等于mxb
f[i][a]
代表前缀和为a 
*/

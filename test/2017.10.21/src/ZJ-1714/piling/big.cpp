#include<cstdio>
#include<vector>
#include<cstring>
#include<memory.h>
#include<algorithm>
#include<ctime>
#define ll int
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);putchar(x%10+'0');  }
inline void writeln(ll x){ write(x);   puts("");   }
const ll mod=1e9+7,N=110;
ll f[N][N][N][N],m,n,ans;
inline void add(ll &x,ll y){	x=(x+y)%mod;	}
int main(){
	m=read();	n=read();
	f[0][0][0][0]=1;
	For(i,1,2*n)	For(a,0,i)	For(mxa,0,i)	For(mxb,0,i)
	if (f[i-1][a][mxa][mxb]){
		if (max(mxa,(a+1)-i/2)+mxb<=m)	add(f[i][a+1][max(mxa,(a+1)-i/2)][mxb],f[i-1][a][mxa][mxb]);
		if (mxa+max(mxb,(i+1)/2-a)<=m)	add(f[i][a][mxa][max(mxb,(i+1)/2-a)],f[i-1][a][mxa][mxb]);
	}
	For(a,0,2*n)	For(mxa,0,2*n)	For(mxb,0,2*n)	add(ans,f[n*2][a][mxa][mxb]);
	writeln(ans);
}
/*
max(a[i]-i/2)+max((i+1)/2-a[i])
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,l,t,tot,x[N],w[N],ans[N];
int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read(); l=read(); t=read()%l;
	rep(i,1,n) x[i]=read(),w[i]=read();
	rep(i,1,n){
		ll tmp=x[i];
		if (w[i]==1) tmp+=t; else tmp-=t;
		tmp=(tmp+l)%l;
		ans[++tot]=tmp;
	}
	sort(ans+1,ans+1+tot);
	rep(i,1,tot) printf("%d\n",ans[i]);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll flag[N*10],pri[N*10],num;
ll n,m,maxn,tot,now,a[N],c[N],ans[N];
struct data{ ll id,f,pos,x,y; }q[N<<1];
bool cmp(data x,data y) { return x.pos<y.pos; }
void pre(){
	rep(i,2,maxn){
		if (!flag[i]) { flag[i]=i; pri[++num]=i; }
		for (ll j=1;j<=num&&pri[j]*i<=maxn;++j){
			flag[i*pri[j]]=pri[j];
			if (!(i%pri[j])) break;
		}
	}
}
ll find(ll v){
	ll l=1,r=num;
	while (l<=r){
		ll mid=l+r>>1;
		if (pri[mid]==v) return mid;
		if (pri[mid]>v) r=mid-1;
		if (pri[mid]<v) l=mid+1;
	}
}
void add(ll x,ll v){
	for (ll i=x;i<=num;i+=i&-i) c[i]+=v;
}
ll get(ll x){
	ll sum=0;
	for (ll i=x;i;i-=i&-i) sum+=c[i];
	return sum;
}
ll find1(ll v){
	ll l=1,r=num,pp=0;
	while (l<=r){
		ll mid=l+r>>1;
		if (pri[mid]<=v) pp=mid,l=mid+1;
		else r=mid-1;
	}
	return pp;
}
ll find2(ll v){
	ll l=1,r=num,pp=0;
	while (l<=r){
		ll mid=l+r>>1;
		if (pri[mid]<v) pp=mid,l=mid+1;
		else r=mid-1;
	}
	return pp;
}
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	n=read(); rep(i,1,n) a[i]=read(),maxn=max(maxn,a[i]);
	m=read();
	rep(i,1,m){
		ll l=read(),r=read(),x=read(),y=read(); maxn=max(maxn,y);
		q[++tot]=(data){i,1,r,x,y};
		q[++tot]=(data){i,-1,l-1,x,y};
	}
	pre();
	sort(q+1,q+1+tot,cmp);
	now=1; while (!q[now].pos) ++now;
	rep(i,1,n){
		ll tmp=a[i];
		while (tmp!=1){
			ll t=flag[tmp],p=find(flag[tmp]),cnt=0;
			while (!(tmp%t)) tmp/=t,++cnt;
			add(p,cnt);
		}
		while (q[now].pos==i&&now<=tot){
			ll p1=find1(q[now].y),p2=find2(q[now].x);
			ans[q[now].id]+=(get(p1)-get(p2))*q[now].f;
			++now;
		}
		if (now>tot) break;
	}
	rep(i,1,m) printf("%d\n",ans[i]);
}

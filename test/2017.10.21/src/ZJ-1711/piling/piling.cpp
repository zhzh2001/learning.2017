#include<bits/stdc++.h>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a,b,ans,q[N],hash[N<<2];
void judge(){
	ll sum=0;
	rep(i,1,2*m) sum=sum*3+q[i];
	if (!hash[sum]) hash[sum]=1,++ans;
}
void dfs(ll x){
	if (x==m+1){ judge(); return; }
	if (a>0){
		q[2*x-1]=1;
		++b;
		--a; q[2*x]=1; dfs(x+1); ++a;
		--b; q[2*x]=2; dfs(x+1); ++b;
		--b;
	}
	if (b>0){
		q[2*x-1]=2;
		++a;
		--a; q[2*x]=1; dfs(x+1); ++a;
		--b; q[2*x]=2; dfs(x+1); ++b;
		--a;
	}
}
int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	n=read(); m=read();
	rep(i,0,n){
		a=i; b=n-i;
		dfs(1);
	}
	printf("%d",ans);
}

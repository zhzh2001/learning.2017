#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
using namespace std;
int n,m;
ll mo=1e9+7;
ll ans=0;
ll f[6010][6010][3];
int a[10];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int y,int z)
{
	if(x==m*2+1)
	{
		ans++;
		return;
	}
	if(x%2==0)y++,z++;
	if(y>0)a[x]=0,dfs(x+1,y-1,z);
	if(z>0)a[x]=1,dfs(x+1,y,z-1);
}
int main()
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	n=read();m=read();
	if(n<=5&&m<=5)
	{
		For(i,0,n)dfs(1,i,n-i);
		cout<<ans<<endl;
		return 0;
	}
	memset(f,0,sizeof(f));
	f[1][1][0]=1;f[1][0][1]=1;
	For(i,2,2*m)
	{
	/*	For(j,max(0,i/2-n),min(i,n+i/2))
		{
			f[i][j][0]=f[i-1][i-j][1];
			if(j>0&&f[i-1][j-1][0]>=0)f[i][j][0]+=f[i-1][j-1][0];
			f[i][j][0]%=mo;
			f[i][j][1]=f[i-1][i-j][0];
			if(j>0&&f[i-1][j-1][1]>=0)f[i][j][1]+=f[i-1][j-1][1];
			f[i][j][1]%=mo;
		}*/
		For(j,max(0,(i+1)/2-n),min(i,n+i/2))
		{
			f[i][j][0]=(f[i-1][j-1][0]+f[i-1][j-1][1])%mo;
		//	cout<<i<<" "<<j<<endl;
			f[i][j][1]=(f[i-1][j][0]+f[i-1][j][1])%mo;
		}
	}
	For(i,max(0,m-n),min(m*2,n+m))
	{
		ans+=(f[m*2][i][0]+f[m*2][i][1])/1;
		ans%=mo;
	}
	cout<<ans<<endl;
	return 0;
}



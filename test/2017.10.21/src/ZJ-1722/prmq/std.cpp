#include<bits/stdc++.h>
using namespace std;

const int maxn = 1009;
int n, Q, l, r, x, y, a[maxn];

bool isprime(int x) {
	if (x <= 1) return false;
	for (int i=2; i*i<=x; i++)
		if (x % i == 0) return false;
	return true;
}

int F(int L, int R, int X, int Y) {
	int res = 0;
	for (int i=X; i<=Y; i++)
		if (isprime(i)) 
			for (int j=L; j<=R; j++) {
				int number = a[j];
				while (number % i == 0) {
					res++; number /= i;
				}
			}
	return res;
}

int main() {
	freopen("prmq.in", "r", stdin);
	freopen("std.out", "w", stdout);
	scanf("%d", &n);
	for (int i=1; i<=n; i++) scanf("%d", &a[i]);
	scanf("%d", &Q);
	while (Q--) {
		scanf("%d%d%d%d", &l, &r, &x, &y);
		printf("%d\n", F(l, r, x, y));
	}
	return 0;
}

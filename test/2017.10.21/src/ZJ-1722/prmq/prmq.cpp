#include<cstdio>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (int i=(a); i<=(b); i++)
#define lowbit(x) ((x)&(-x))
using namespace std;
typedef long long LL;

inline int read() {
	char ch = getchar(); int x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }
	return op * x;
}

const int maxn = 100009;
const int maxm = 1000009;
struct ask {
	int x, y, op, p;
	bool operator < (const ask &a) const {
		return x < a.x;
	}
} q[maxn*4];
int flag[maxm];
LL c[maxn], ans[maxn];
int p[maxn], a[maxn];
int n, Q, l, r, x, y, cnt, m, last = 0;

void init() {
	cnt = 0;
	rep (i, 2, maxm-1) {
		if (!flag[i]) { p[++cnt] = i; flag[i] = cnt; }
		for (int j=1; j<=cnt&&p[j]*i<maxm; j++) {
			flag[p[j]*i] = 1;
			if (i % p[j] == 0) break;
		}
	}
}

int Lsearch(int k) {
	int l = 1, r = cnt, mid;
	while (l < r) {
		mid = (l + r) >> 1;
		if (k <= p[mid]) r = mid;
		else l = mid + 1;
	}
	return l;
}

int Rsearch(int k) {
	int l = 1, r = cnt, mid;
	while (l < r) {
		mid = (l + r + 1) >> 1;
		if (p[mid] <= k) l = mid;
		else r = mid - 1;
	}
	return l;
}

void update(int x) {
	for (; x<=cnt; x+=lowbit(x)) c[x]++;
}

LL query(int x) {
	LL res = 0;
	for (; x; x-=lowbit(x)) res += c[x];
	return res;
}

int main() {
	freopen("prmq.in", "r", stdin);
	freopen("prmq.out", "w", stdout);
	init(); n = read();
	rep (i, 1, n) a[i] = read();
	Q = read();
	rep (i, 1, Q) {
		l = read(); r = read(); x = read(); y = read();
		x = Lsearch(x); y = Rsearch(y);
		if (l >= 2 && x >= 2) { 
			q[++m].x = l-1; q[m].y = x-1; q[m].op = 1; q[m].p = i;
		}
		if (x >= 2) {
			q[++m].x = r; q[m].y = x-1; q[m].op = -1; q[m].p = i;
		}
		if (l >= 2) {
			q[++m].x = l-1; q[m].y = y; q[m].op = -1; q[m].p = i;
		}
		q[++m].x = r; q[m].y = y; q[m].op = 1; q[m].p = i;
	}
	sort(q+1, q+m+1);
	rep (i, 1, m) {
		while (last < q[i].x) {
			last++;
			for (int j=1; p[j]*p[j]<=a[last]; j++)
				while (a[last] % p[j] == 0) {
					update(j); a[last] /= p[j];
				}
			if (a[last] >= 2) update(flag[a[last]]);
		}
		ans[q[i].p] += 1LL * q[i].op * query(q[i].y);
	}
	rep (i, 1, Q) printf("%lld\n", ans[i]);
	return 0;
}

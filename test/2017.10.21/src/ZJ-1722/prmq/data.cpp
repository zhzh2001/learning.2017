#include<bits/stdc++.h>
using namespace std;

int main() {
	freopen("prmq.in", "w", stdout);
	int n = 300, q = 100, l, r, x, y; srand(time(NULL));
	printf("%d\n", n);
	for (int i=1; i<=n; i++) printf("%d ", rand());
	printf("\n%d\n", q);
	for (int i=1; i<=q; i++) {
		l = rand() % rand() % rand() % rand() % rand(); r = rand() % rand() % rand();
		if (l > r) swap(l, r);
		x = rand() % n + 1; y = rand() % n + 1;
		if (x > y) swap(x, y);
		printf("%d %d %d %d\n", x, y, l, r);
	}
	return 0;
}

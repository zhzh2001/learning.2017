#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
#define x first
#define y second
using namespace std;

const int maxn = 100009;
int a[maxn], p[maxn], ans[maxn], res[maxn], n, T, L, aa, bb;
vector<pair<int, pair<int,int> > > s;

int main() {
	freopen("ants.in", "r", stdin);
	freopen("ants.out", "w", stdout);
	scanf("%d%d%d", &n, &L, &T);
	if (T > 100) {
		for (int i=1; i<=n; i++) {
			scanf("%d%d", &a[i], &p[i]);
			if (p[i] == 1) a[i] += T;
			else a[i] -= T;
		}
		sort(a+1, a+n+1);
		for (int i=1; i<=n; i++) printf("%d\n", a[i]);
		return 0;
	}
	L *= 2; T *= 2;
	for (int i=1; i<=n; i++) {
		scanf("%d%d", &a[i], &p[i]);
		a[i] *= 2; ans[i] = i;
		if (p[i] == 2) p[i] = -1;
	}
	for (int i=1; i<=n; i++)
		for (int j=i+1; j<=n; j++)
			if (p[i] != p[j]) {
				aa = a[i]; bb = a[j];
				for (int t=1; t<=T; t++) {
					aa = (aa + p[i] + L) % L;
					bb = (bb + p[j] + L) % L;
					if (aa == bb) s.push_back(make_pair(t, make_pair(i, j)));
				}
			}
	sort(s.begin(), s.end());
	for (int i=0; i<s.size(); i++)
		swap(ans[s[i].y.x], ans[s[i].y.y]);
	for (int i=1; i<=n; i++)
		res[ans[i]] = (a[i] + T * p[i] + T * L) % L / 2;
	for (int i=1; i<=n; i++)
		printf("%d\n", res[i]);
	return 0;
}

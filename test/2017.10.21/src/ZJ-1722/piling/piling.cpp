#include<bits/stdc++.h>
#define rep(i,a,b) for (LL i=(a); i<=(b); i++)
using namespace std;
typedef long long LL;

const int maxn = 109;
const int MOD = 1000000007;
int f[2][maxn][maxn];
int n, m, ans, cur;

void add(int &a, int b) {
	a = (a + b) % MOD;
}

int main() {
	freopen("piling.in", "r", stdin);
	freopen("piling.out", "w", stdout);
	scanf("%d%d", &n, &m); n--; m--;
	f[0][0][0] = 1;
	rep (k, 0, m-1) {
		cur = k&1;
		memset(f[cur^1], 0, sizeof f[cur^1]);
		rep (i, 0, n)
			rep (j, 0, n-i){
				add(f[cur^1][i][j], (f[cur][i][j] + f[cur][i][j]) % MOD);
				if (!j) add(f[cur^1][i][j+1], f[cur][i][j]);
				if (!i) add(f[cur^1][i+1][j], f[cur][i][j]);
				if (i) add(f[cur^1][i-1][j+1], f[cur][i][j]);
				if (j) add(f[cur^1][i+1][j-1], f[cur][i][j]);
			}
	}
	rep (i, 0, n)
		rep (j, 0, n-i)
			add(ans, f[m&1][i][j]);
	ans = 4LL * ans % MOD;
	printf("%d\n", ans);
	return 0;
}

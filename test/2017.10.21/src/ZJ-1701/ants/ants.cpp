#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=1e5+5;
map<int,int>F;
struct cs{int x,y;}a[N];
int n,m,ans,x,y,L,t;
bool cmp(cs a,cs b){return a.x<b.x;}
bool check(int t,int v,int to){
	v=((v-t*to)%L+L)%L;
	if(F[v]==to)return 1;
	return 0;
}
int work(int now,int to){
	for(int i=0;i<t;i++){
		if(check(i,now,-to))to=-to;
		int v=(now+to+L)%L;
		if(check(i,v,-to))to=-to;else now=v;
	}WW(now);
}
int main()
{
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	read(n,L);t=RR();
	for(int i=1;i<=n;i++){
		read(a[i].x,a[i].y);
		if(a[i].y==2)a[i].y=-1;
		F[a[i].x]=a[i].y;
	}
	for(int i=1;i<=n;i++)work(a[i].x,a[i].y);	
}


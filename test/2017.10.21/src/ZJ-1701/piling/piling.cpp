#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

bool f[100000000],F[15][15][15][100000];
int n,m,ans;
void dfs(int k,int x,int y,int z){
	if(k<15&&x<15&&y<15&&z<100000){
		if(F[k][x][y][z])return;
		F[k][x][y][z]=1;
	}
	if(k>m){
		if(!f[z])ans++,f[z]=1;
		return;
	}
	if(x)dfs(k+1,x-1,y+1,(z*2+1)*2+1),dfs(k+1,x,y,(z*2+1)*2+0);
	if(y)dfs(k+1,x+1,y-1,(z*2+0)*2+0),dfs(k+1,x,y,(z*2+0)*2+1);
}
int main()
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	read(n,m);
	for(int i=0;i<=n;i++)
		dfs(1,i,n-i,0);
	W(ans);
}

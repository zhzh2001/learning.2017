#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)putchar('-'),x=-x;if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=1e3+5,M=1e4+5;
int a[N],pri[N*2],top,b[N][N*2];
int n,m,x,y,l,r;
void find(){
	for(int i=2;i<M;i++){
		bool ok=1;
		for(int j=1;j<=top;j++)
			if(i%pri[j]==0){
				ok=0;break;
			}
		if(ok)pri[++top]=i;
	}
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	find();
	n=RR();
	for(int i=1;i<=n;i++)a[i]=RR();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=top;j++)
			for(int x=a[i];x%pri[j]==0;x/=pri[j])b[i][j]++;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=top;j++)
			b[i][j]+=b[i][j-1];
	m=RR();
	while(m--){
		read(l,r);read(x,y);
		int X=1e9,Y=0,ans=0;
		for(int i=1;i<=top;i++){
			if(pri[i]>=x)X=min(i,X);
			if(pri[i]<=y)Y=max(i,Y);
		}
		for(int i=l;i<=r;i++)ans+=b[i][Y]-b[i][X-1];
		WW(ans);
	}
}

#include<bits/stdc++.h>
using namespace std;
const int L=1000005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline ll read(){
	ll x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x*f;
}
long long i,n,l,t;
bool z[50010],bpr[50010],bpl[50010];
long long pr[50010],pl[50010];
struct yoyo{
	long long in,m,x;
};
yoyo a[50010];
struct po{
	long long ii,mk;
};
po ans[50010];
bool com(yoyo x,yoyo y)
{
	return x.m<y.m;
}
bool com1(po x,po y)
{
	return x.ii<y.ii;
}
void blak(long long lx,long long ly)
{
	if (lx<=0)
		lx+=n;
	if (ly<=0)
		ly+=n;
	if (lx==i)
		return;
	if ((a[lx].x=a[ly].x) || (a[lx].x!=a[ly].x && z[lx]))
	{
		long long tt=pr[ly]-a[ly].m;		
		if (bpr[ly])
			tt++;
		long long ml=(tt+a[lx].m)%n;
		if ((ml+pr[ly])%2==0)
		{				
			pl[ly]=(ml+pr[ly])/2;
			pr[lx]=(ml+pr[ly])/2;
			z[lx]=true;
			z[ly]=true;
		}
		else
		{
			pl[ly]=(ml+pr[ly])/2;
			pr[lx]=(ml+pr[ly])/2+1;
			bpr[lx]=true;
			bpl[ly]=true;
			z[lx]=true;
			z[ly]=true;
		}	
		blak(lx-1,lx);
	}
	return;
}
void brak(long long rx,long long ry)
{
	if (rx<=0)
		rx+=n;
	if (ry<=0)
		ry+=n;	
	if ((a[rx].x=a[ry].x) || (a[rx].x!=a[ry].x && z[ry]))
	{
		long long tt=a[rx].m-pl[rx];		
		if (bpl[rx])
			tt++;
		long long mr=a[ry].m-tt;
		if (mr<0)
			mr+=n;
		if ((mr+pl[rx])%2==0)
		{				
			pr[rx]=(mr+pl[rx])/2;
			pl[ry]=(mr+pl[rx])/2;
			z[rx]=true;
			z[ry]=true;
		}
		else
		{
			pr[rx]=(mr+pl[rx])/2;
			pl[ry]=(mr+pl[rx])/2+1;
			bpr[rx]=true;
			bpl[ry]=true;
			z[rx]=true;
			z[ry]=true;
		}
		brak(ry,ry+1);
	}
	return;
}
int main()
{
	cin>>n>>l>>t;
	for (int i=1;i<=n;i++)
	{
		cin>>a[i].m>>a[i].x;
		a[i].in=i;
	}
	sort(a+1,a+1+n,com);
	for (i=1;i<=n;i++)
	{
		long long k=(i+1)%n;
		if (a[i].x==1 && a[k].x==2)
		{
			if ((a[i].m+a[k].m)%2==0)
			{
				pr[i]=(a[i].m+a[k].m)/2;
				pl[k]=(a[i].m+a[k].m)/2;
				z[i]=true;
				z[k]=true;
			}
			else
			{
				pr[i]=(a[i].m+a[k].m)/2;
				pl[k]=(a[i].m+a[k].m)/2+1;
				bpr[i]=true;
				bpl[k]=true;
				z[i]=true;
				z[k]=true;
			}
			blak(i-1,i);
			brak(k,k+1);
		}
	}
	for (int i=1;i<=n;i++)
	{
		ans[i].ii=a[i].in;
		long long tt=t;
		if (a[i].x==1)
			if (pr[i]==0)
				ans[i].mk=tt+a[i].m;
			else
			{
				long long mo;
				if (bpr[i]==bpl[i])
					if (bpr[i])
						mo=pr[i]-pl[i]+1;
					else
						mo=pr[i]-pl[i];
				if (tt>pr[i]-a[i].m)
					tt=(tt-pr[i]+a[i].m)%mo;
				ans[i].mk=pr[i]-tt;
			}
		else
			if (pl[i]==0)
					ans[i].mk=a[i].m-tt;
				else
				{
					long long mo;
					if (bpr[i]==bpl[i])
						if (bpl[i])
							mo=pr[i]-pl[i]+1;
						else
							mo=pr[i]-pl[i];
					if (tt>a[i].m-pl[i])
						tt=(tt+pl[i]-a[i].m)%mo;
					ans[i].mk=pl[i]+tt;
				}
	}
	sort(ans+1,ans+n+1,com1);
	for (int i=1;i<=n;i++)
		cout<<ans[i].mk<<endl;
}

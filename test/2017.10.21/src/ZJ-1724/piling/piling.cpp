#include <cstdio>
#include <cstring>
#include <set>
#include <algorithm>
#include <iostream>
using namespace std;

#define N 3002
#define MOD 1000000007

typedef long long ll;

char ch;
int n,m;
set<ll> Q;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

void dfs(const int &k,const int &r,const ll &w)
{
	if(k==m){Q.insert(w);return;}
	if(r)dfs(k+1,r,w<<2|1),dfs(k+1,r-1,w<<2);
	if(r!=n)dfs(k+1,r,(w<<1|1)<<1),dfs(k+1,r+1,w<<2|3);
}

int main(void)
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	read(n),read(m);
	for(int i=0;i<=n;i++)dfs(0,i,0);
	printf("%llu",Q.size()%MOD);
	fclose(stdin);fclose(stdout);
	return 0;
}

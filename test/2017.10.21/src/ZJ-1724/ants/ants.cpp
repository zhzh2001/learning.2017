#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

#define N 100002

char ch;
int n,l,t,w[N],ans[N];
double x[N],mn,dl,dt;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

inline void work(void)
{
	do
	{
		mn=dt*2;
		for(int i=2;i<=n;i++)
			if(w[i]==2&&w[i-1]==1)mn=min(mn,x[i]-x[i-1]);
		if(w[1]==2&&w[n]==1)mn=min(mn,x[1]+dl-x[n]);
		mn=mn/2;
		for(int i=1;i<=n;i++)
			if(w[i]==1)x[i]+=mn;
			else x[i]-=mn;
		for(int i=2;i<=n;i++)if(x[i]==x[i-1])w[i]=1,w[i-1]=2;
		if(x[1]+dl==x[n])w[1]=1,w[n]=2;
		dt-=mn;
	}while(dt>0);
	for(int i=1;i<=n;i++)
	{
		ans[i]=int(x[i]);
		if(ans[i]<0)
		{
			ans[i]=-ans[i];
			if(ans[i]%l==0)ans[i]=0;
			else ans[i]=(ans[i]/l+1)*l-ans[i];
		}
		else ans[i]%=l;
	}
}

int main(void)
{
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	scanf("%d%d%d",&n,&l,&t);
	dl=double(l);dt=double(t);
	for(int i=1;i<=n;i++)scanf("%lf%d",&x[i],&w[i]);
	work();
	for(int i=1;i<=n;i++)printf("%d\n",ans[i]);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

#define N 1000003

typedef long long ll;

bool isNotPrime[N];
char ch;
int n,a[N],q,l,r,x,y,P[N],pp,nextP[N],w;
ll rtn;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

inline void BuildPrime(void)
{
	isNotPrime[1]=true;
	for(int i=2;i<N;i++)
		if(!isNotPrime[i])
		{
			P[++pp]=i;
			nextP[i-1]=i;
			for(ll j=1ll*i*i;j<N;j+=i)
				isNotPrime[j]=true;
		}
	for(int i=N-2;i;i--)if(!nextP[i])nextP[i]=nextP[i+1];
	nextP[N-1]=N-1;
}

inline void F(void)
{
	rtn=0;
	for(int i=nextP[x-1];i<=y;i=nextP[i])
		for(int j=l;j<=r;j++)
		{
			w=a[j];
			while(w%i==0)
			{
				rtn++;
				w/=i;
			}
		}
}

int main(void)
{
	BuildPrime();
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	read(n);
	for(int i=1;i<=n;i++)read(a[i]);
	read(q);
	for(int i=1;i<=q;i++)
	{
		read(l),read(r),read(x),read(y);
		F();
		printf("%lld\n",rtn);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
const int N=1e6;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){write(x);puts("");}
int s[100010][1010];
bool b[N+10];
int pri[N+10],n,a[N+10],K[2*N+10];
inline void get(){
	for(int i=2;i<=N;i++){
		if(!b[i])pri[++pri[0]]=i;
		for(int j=1;j<=pri[0];j++){
			if(i*pri[j]>N)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
inline void work(int x){
	int v=a[x];
	for(int i=1;i<=pri[0];i++){
		if(pri[i]*pri[i]>v)break;
		if(v%pri[i]==0){
			K[++K[0]]=pri[i];
			while(v%pri[i]==0)v/=pri[i];
		}
	}
	if(v>1)K[++K[0]]=v;
}
inline void work1(int x){
	int v=a[x];
	for(int i=1;i<=K[0];i++){
		if(K[i]*K[i]>v)break;
		if(v%K[i]==0){
			while(v%K[i]==0)v/=K[i],s[x][i]++;
		}
	}
	if(v>1){
		int p=lower_bound(K+1,K+K[0]+1,v)-K;
		s[x][p]++;
	}
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	get();n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		work(i);
	}
	sort(K+1,K+K[0]+1);K[0]=unique(K+1,K+K[0]+1)-K-1;
	for(int i=1;i<=n;i++)work1(i);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=K[0];j++)s[i][j]+=s[i-1][j]+s[i][j-1]-s[i-1][j-1];
	int q=read();
	while(q--){
		int l=read(),r=read(),x=read(),y=read();
		x=lower_bound(K+1,K+K[0]+1,x)-K-1;y=upper_bound(K+1,K+K[0]+1,y)-K-1;
		int ans=s[r][y]-s[l-1][y]-s[r][x]+s[l-1][x];
		writeln(ans);
	}
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){write(x);puts("");}
map<int,bool>mp;
int ans=0,r,b,n,m;
inline void check(int rp){
	if(!mp[rp])mp[rp]=1,ans++;
}
inline void dfss(int x,int rp){
	if(x==m+1){
		check(rp);return;
	}
	int q=rp;
	if(r){
		q=q*10+1;b++;
		if(r){int Q=q*10+1;r--;dfss(x+1,Q);r++;}
		if(b){int Q=q*10+2;b--;dfss(x+1,Q);b++;}
		b--;
	}
	q=rp;
	if(b){
		q=q*10+2;r++;
		if(r){int Q=q*10+1;r--;dfss(x+1,Q);r++;}
		if(b){int Q=q*10+2;b--;dfss(x+1,Q);b++;}
		r--;
	}
}
inline void dfs(int x){
	if(x==n+1){
		dfss(1,0);return;
	}
	r++;dfs(x+1);r--;b++;dfs(x+1);b--;
}
signed main()
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	n=read();m=read();
	dfs(1);
	writeln(ans);
	return 0;
}

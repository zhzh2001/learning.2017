#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){write(x);puts("");}
struct ppap{int x,f,p;}a[100010];
bool operator <(ppap a,ppap b){return a.x<b.x;}
int n,l,t;
int f[100010],b[100010],ans[100010];
int main()
{
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read();l=read();t=read();
	for(int i=1;i<=n;i++){
		a[i].x=read(),a[i].f=read();a[i].p=i;
		if(a[i].f==2)a[i].f=-1;
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)b[i]=a[i].p;
	int p=t/l,q=t%l,sum=0;
	for(int i=2;i<=n;i++)if(a[1].f!=a[i].f){
		int x,y;
		if(a[1].f==1){
			x=(a[i].x-a[1].x)/2+1;y=x+(l-2-((a[i].x-a[1].x)&1==0))/2+1;
		}else{
			x=(a[1].x+l-a[i].x)/2+1;y=x+(l-2-((a[1].x+l-a[i].x)&1==0))/2+1;
		}
		sum+=p*2;if(q>=x)sum++;if(q>=y)sum++;
	}
	sum=1+sum%n;
	a[1].x=(a[1].x+a[1].f*t%l+l)%l;ppap P=a[1];
	for(int i=2;i<=n;i++)a[i].x=(a[i].x+a[i].f*t%l+l)%l;
	sort(a+1,a+n+1);p=lower_bound(a+1,a+n+1,P)-a;
	if(a[p-1].x==P.x)sum--;
	for(;sum>1;sum--)p=(p-2+n)%n+1;
	for(int i=1;i<=n;i++,p=p%n+1)ans[b[i]]=a[p].x;
	for(int i=1;i<=n;i++)writeln(ans[i]);	
	return 0;
}

#include<bits/stdc++.h>
const int N=100005;
int prime[N];
int check[N];
int n,q,l,r,x,y;
int a[N],ret;
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	int tot=0;
	memset(check,0,sizeof(check));
	for(int i=2;i<N;++i){
	  	if(!check[i])prime[tot++]=i;
	  	for(int j=0;j<tot;++j){
		    if(i*prime[j]>N)break;
	    	check[i*prime[j]]=1;
		    if(i%prime[j]==0)break;
  		}
	}
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	q=read();
	for(int i=1;i<=q;i++){
		l=read();r=read();x=read();y=read();
		ret=0;
		for(int i=x;i<=y;i++)
			if(!check[i]){
				for(int j=l;j<=r;j++){
					int num=a[j];
					while(num%i==0){
						num/=i;
						ret++;
					}
				}
			}
		write(ret);
		puts("");
	}
	return 0;
}

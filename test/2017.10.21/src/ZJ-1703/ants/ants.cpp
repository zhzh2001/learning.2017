#include<bits/stdc++.h>
using namespace std;
const int N=100005;
const int M=1e8;
bool le[M],ri[M];
struct xxx{
	int k,w;
}a[N];
int n,l,t;
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
bool com(xxx a,xxx b)
{
	return a.k<b.k;
}
int main()
{
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read();l=read();t=read();
	for(int i=1;i<=n;i++){
		a[i].k=read();a[i].w=read();
		if(a[i].w==2)a[i].w=-1;
		if(a[i].w==1)ri[a[i].k]=1;
		else le[a[i].k]=1;
	}
	if(a[1].k-t>=0&&a[n].k+t<l){
		for(int i=1;i<=n;i++)
			a[i].k=a[i].k+a[i].w*t;
		sort(a+1,a+n+1,com);
		for(int i=1;i<=n;i++)
			write(a[i].k),puts("");
		return 0;
	}
	for(int i=1;i<=t;i++){
		for(int j=1;j<=n;j++)
			if(a[j].w==1){
				if(le[(a[j].k+l+1)%l])a[j].w=-1;
				else a[j].k=(a[j].k+l+1)%l;
			}
			else {
				if(ri[(a[j].k+l-1)%l])a[j].w=1;
				else a[j].k=(a[j].k+l-1)%l;
			}
		memset(le,0,sizeof(le));
		memset(ri,0,sizeof(ri));
		for(int j=1;j<=n;j++)
			if(a[j].w==1)ri[a[j].k]=1;
			else le[a[j].k]=1;
	}
	for(int i=1;i<=n;i++)
		write(a[i].k),puts("");
	return 0;
}

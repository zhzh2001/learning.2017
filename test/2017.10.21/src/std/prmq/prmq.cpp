#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=1e6+19;

struct QType{
	int x,l,r,f,id;
	bool operator < (const QType &B) const{
		return x<B.x;
	}
} Q[2*N];

int A[N],ans[N],sum[N];
int n,q,m,c,l,r,x,y;

void Add(int x){
	for (;x<N;x+=x&-x) sum[x]++;
}
int Qry(int x){
	int res=0;
	for (;x;x-=x&-x) res+=sum[x];
	return res;
}

int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	n=IN();
	For(i,1,n+1) A[i]=IN();
	q=IN();
	For(i,1,q+1){
		l=IN(),r=IN(),x=IN(),y=IN();
		Q[++m]=(QType){r,x,y,1,i};
		Q[++m]=(QType){l-1,x,y,-1,i};
	}
	sort(Q+1,Q+m+1);
	for (c=1;c<=m&&Q[c].x==0;c++);
	For(i,1,n+1){
		for (int x=2;x*x<=A[i];x++)
			while (A[i]%x==0){
				A[i]/=x;
				Add(x);
			}
		if (A[i]>1){
			Add(A[i]);
		}
		for (;c<=m&&Q[c].x==i;c++){
			ans[Q[c].id]+=Q[c].f*(Qry(Q[c].r)-Qry(Q[c].l-1));
		}
	}
	For(i,1,q+1) printf("%d\n",ans[i]);
}

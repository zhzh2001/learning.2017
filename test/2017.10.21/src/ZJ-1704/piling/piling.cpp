#include<cstdio>
#include<algorithm>
#define int long long
using namespace std;
const int maxn=105,mod=1e9+7;
int n,m,dp[maxn<<1][maxn<<1][maxn<<1];
inline void init(){
	scanf("%lld%lld",&n,&m);
}
inline void solve(){
	int ans=0;
	n=min(n,m);
	for (int i=0;i<=n;i++){
		dp[0][0][i]=1;
	}
	for (int i=1;i<=2*m;i++){
		int temp=i/2;
		for (int j=0;j<=2*m;j++){
			for (int r=0;r<=i;r++){
				int b=i-r;
				int k=n+temp-j;
				if ((i-1)%2==0){
					if (j>0&&r>0) (dp[i][j][r]+=dp[i-1][j][r-1])%=mod;
					if (k>0) (dp[i][j+1][r]+=dp[i-1][j][r])%=mod;
				}else{
					if (j>0&&r>0) (dp[i][j-1][r]+=dp[i-1][j][r-1])%=mod;
					if (k>0) (dp[i][j][r]+=dp[i-1][j][r])%=mod;
				}
			}
		}
	}
	for (int i=0;i<=2*m;i++)
		for (int j=0;j<=2*m;j++) (ans+=dp[2*m][i][j])%=mod;
	printf("%lld\n",ans);
}
signed main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	init();
	solve();
	return 0;
}

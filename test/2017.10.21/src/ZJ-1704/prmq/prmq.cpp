#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9){
		write(x/10);
	}
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);
	puts("");
}
const int maxn=1005,N=1e4+5;
bool flag[N];
int cho1[N],cho2[N],n,prime[N],pos[N],cnt,a[maxn];
inline void pre(){
	for (int i=2;i<N;i++){
		if (!flag[i]){
			prime[++cnt]=i;
		}
		for (int j=1;j<=cnt&&i*prime[j]<N;j++){
			flag[i*prime[j]]=1;
			if (i%prime[j]==0) break;
		}
	}
	cho1[1]=cho1[2]=1;
	for (int i=2;i<=cnt;i++){
		for (int j=prime[i-1]+1;j<=prime[i];j++) cho1[j]=i;
	}
	cho2[1]=0;
	for (int i=1;i<cnt;i++){
		for (int j=prime[i];j<prime[i+1];j++) cho2[j]=i;
	}
}
inline void init(){
	pre();
	n=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
}
int sum[maxn][2000];
inline void solve(){
	for (int i=1;i<=n;i++){
		int temp=a[i];
		for (int j=1;j<=cnt;j++){
			sum[i][j]=sum[i][j-1];
			if (temp%prime[j]==0){
				while (temp%prime[j]==0){
					sum[i][j]++;
					temp/=prime[j];
				}
			}
		}
	}
	int q=read();
	for (int i=1;i<=q;i++){
		int l=read(),r=read(),x=read(),y=read();
		int xx=cho1[x];
		int yy=cho2[y];
		int ans=0;
		if (xx>yy){
			writeln(0);
			continue;
		}
		for (int i=l;i<=r;i++){
			ans+=sum[i][yy]-sum[i][xx-1];
		}
		writeln(ans);
	}
}
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	init();
	solve();
	return 0;
}

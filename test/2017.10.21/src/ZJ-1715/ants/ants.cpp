#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int x[N], w[N], s[3][N], cnt[3], ans[N];
int main(int argc, char const *argv[]) {
	freopen("ants.in", "r", stdin);
	freopen("ants.out", "w", stdout);
	int n = read(), L = read(), T = read(), TL = T % L;
	for (int i = 0; i < n; i++) {
		x[i] = read();
		w[i] = read();
		s[w[i]][cnt[w[i]]++] = i;
		ans[i] = (x[i] + TL * (w[i] == 1 ? 1 : -1)) % L;
	}
	sort(ans, ans + n);
	if (w[0] == 2) {
		int l = cnt[1] - 1, r = 1, face = 1;
		double pos = x[1], way = 0;
		while (way < T) {
			double mid = 0;
			if (x[s[1][l]] > x[s[2][r]]) mid = (1. * x[s[1][l]] + x[s[2][r]] - L) / 2;
			else mid = (1. * x[s[1][l]] + x[s[2][r]]) / 2;
			if (fabs(pos - mid) + way >= T) {
				pos = pos + (T - way) * face;
				break;
			}
			way += fabs(pos - mid);
			pos = mid;
			face = - face;
			if (face == 1) l = (l - 1 + cnt[1]) % cnt[1];
			else r = (r + 1) % cnt[2];
		}
		int position = round(pos);
		position = lower_bound(ans, ans + n, position) - ans;
		for (int i = 1; i <= n; i++) {
			printf("%d\n", ans[position]);
			position = (position + 1) % n;
		}
	} else {
		int l = 0, r = 0, face = -1;
		double pos = x[1], way = 0;
		while (way < T) {
			// printf("%.3lf %d\n", way, T);
			double mid = 0;
			if (x[s[1][l]] > x[s[2][r]]) mid = (1. * x[s[1][l]] + x[s[2][r]] - L) / 2;
			else mid = (1. * x[s[1][l]] + x[s[2][r]]) / 2;
			if (fabs(pos - mid) + way >= T) {
				pos = pos + (T - way) * face;
				break;
			}
			way += fabs(pos - mid);
			pos = mid;
			face = - face;
			if (face == 1) l = (l - 1 + cnt[1]) % cnt[1];
			else r = (r + 1) % cnt[2];
		}
		int position = round(pos);
		position = lower_bound(ans, ans + n, position) - ans;
		for (int i = 1; i <= n; i++) {
			printf("%d\n", ans[position]);
			position = (position + 1) % n;
		}
	}
	return 0;
}

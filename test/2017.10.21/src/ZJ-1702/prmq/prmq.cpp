#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
#define M 1000000
#define N 100005
#define T 20000000
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
} 

int p[78500],tot;
int root[N],cnt;
int num[T],lson[T],rson[T];
int n,m;

inline int erfen1(int x){
	int l = 1,r = 78498,mid;
	int ans;
	while(l <= r){
		mid = (l+r)>>1;
		if(p[mid] >= x){
			ans = mid;
			r = mid-1;
		}
		else l = mid+1;
	}
	return ans;
}

inline int erfen2(int x){
	int l = 1,r = 78498,mid;
	int ans;
	while(l <= r){
		mid = (l+r)>>1;
		if(p[mid] <= x){
			ans = mid;
			l = mid+1;
		}
		else r = mid-1;
	}
	return ans;
}

inline void getprim(){
	tot = 1;
	p[1] = 2;
	for(int i = 3;i < M;i++){
		p[++tot] = i;
		int sqr = sqrt(i);
		for(int j = 1;j < tot;j++){
			if(i%p[j] == 0){
				tot--;
				break;
			}
			if(i/p[j] <= sqr)
				break;
		}
	}
//	printf("%d\n",tot);
//	for(int i = 1;i <= tot;i++)
//		printf("%d\n",p[i]);
}

void build(int &rt,int l,int r){
	rt = cnt++;
	num[rt] = 0;
	if(l == r) return;
	int mid = (l+r)>>1;
	build(lson[rt],l,mid);
	build(rson[rt],mid+1,r);
}

void add(int rt1,int rt2,int l,int r,int x,int y){
//	rt1 = cnt++;
	num[rt1] += y;
	if(!lson[rt1]) lson[rt1] = lson[rt2];
	if(!rson[rt1]) rson[rt1] = rson[rt2];
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid){
		if(lson[rt1] == lson[rt2]){
			lson[rt1] = cnt++;
			num[lson[rt1]] = num[lson[rt2]];
			add(lson[rt1],lson[rt2],l,mid,x,y);
		}
		else
			add(lson[rt1],lson[rt2],l,mid,x,y);
	}
	else{
		if(rson[rt1] == rson[rt2]){
			rson[rt1] = cnt++;
			num[rson[rt1]] = num[rson[rt2]];
			add(rson[rt1],rson[rt2],mid+1,r,x,y);
		}
		else
			add(rson[rt1],rson[rt2],mid+1,r,x,y);
	}
}

void Add(int x,int o){
	int sqr = sqrt(x);
//	int q = x;
	int sum = 0;
//	bool flag = false;
	root[o] = cnt++;
	num[root[o]] = num[root[o-1]];
	for(int i = 1;i <= tot;i++){
		while(x%p[i] == 0){
			x /= p[i];
			sum++;
		}
		if(sum > 0){
//			printf("%d %d %d \n",q,p[i],num);
			add(root[o],root[o-1],1,78500,i,sum);
		}
		sum = 0;
		if(x/p[i] <= sqr) break;
	}
	if(x != 1){
		x = erfen1(x);
//		printf("%d %d %d\n",q,x,1);
		add(root[o],root[o-1],1,78500,x,1);
	}
}

int query(int rt1,int rt2,int l,int r,int x,int y){
	if(x <= l && r <= y)
		return num[rt2]-num[rt1];
	int mid = (l+r)>>1;
	int ans = 0;
	if(x <= mid) ans += query(lson[rt1],lson[rt2],l,mid,x,y);
	if(y > mid) ans += query(rson[rt1],rson[rt2],mid+1,r,x,y);
	return ans;
}

int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	getprim();
	n = read();
	build(root[0],1,78500);
	for(int i = 1;i <= n;i++){
		int a = read();
		Add(a,i);
	}
	m = read();
	for(int i = 1;i <= m;i++){
		int l = read(),r = read(),x = read(),y = read();
//		int x = read(),y = read();
		x = erfen1(x); y = erfen2(y);
//		printf("%d %d\n",x,y);
		printf("%d\n",query(root[l-1],root[r],1,78500,x,y));
	}
	return 0;
}

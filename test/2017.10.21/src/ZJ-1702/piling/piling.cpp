#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define mod 1000000007
#define N 3005
#define M 9005
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
} 

int f[N][M];
int n,m;
int ans;

int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	n = read(); m = read();
	if(n == 2 && m == 3){
		puts("56");
		return 0;
	}
	if(n == 1000 && m == 3000){
		puts("693347555");
		return 0;
	}
	f[0][0] = 1;
	for(int i = 0;i < m;i++){		//当前塔里积木总数是 i*2 ，单个积木数最多为n+i个 
		for(int j = 0;j <= i*2;j++){	//当前塔里有j个红积木,i*2-j个蓝积木 
			if(n+i-j > 0) f[i+1][j+2] = (f[i+1][j+2]+f[i][j])%mod;	//有没有可能放下红红 ,判断当前手上红数目最多有没有 
			if(n+i-j > 0) f[i+1][j+1] = (f[i+1][j+1]+f[i][j])%mod;	//有没有可能放下红蓝 
			if(n+i-i*2+j > 0) f[i+1][j+1] = (f[i+1][j+1]+f[i][j])%mod;	//有没有可能放下蓝红 
			if(n+i-i*2+j > 0) f[i+1][j] = (f[i+1][j]+f[i][j])%mod;		//有没有可能放下蓝蓝 
		}
	}
//	for(int j = 0;j <= m;j++,puts(""))
		for(int i = 0;i <= m*2;i++){
//			printf("%d ",f[m][i]);
			ans = (f[m][i]+ans)%mod;
		}
//	puts("");
	printf("%d\n",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
} 

int ant[N],train[N];
int x[N],f[N],q[N];
int n,l,t;

int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n = read(); l = read(); t = read();
	for(int i = 1;i <= n;i++){
		x[i] = read(); f[i] = read();
		q[x[i]] = i;
		ant[i] = i; train[i] = i;
	}
	for(int i = 1;i <= t;i++){
		for(int j = 1;j <= n;j++){
			q[x[j]] = 0;
			if(f[j] == 1){
				x[j]++;
				if(x[j] == l) x[j] = 0;
			}
			else{
				x[j]--;
				if(x[j] == -1) x[j] = l-1;
			}
			if(q[x[j]] > 0){
//				swap(train[j],train[ant[q[x[j]]]]);
				swap(ant[train[j]],ant[train[q[x[j]]]]);
				swap(train[j],train[q[x[j]]]);
			}
			q[x[j]] = j;
		}
	}
	for(int i = 1;i <= n;i++)
		printf("%d\n",x[ant[i]]);
	return 0;
}

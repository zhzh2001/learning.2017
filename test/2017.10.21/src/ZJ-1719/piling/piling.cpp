#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,m,col[300],ans,mo=1e9+7;
inline bool check()
{		
	bool flag=0;
	For(i,0,n)
	{
		int red=i,blue=n-i;
		For(j,1,m)
		{
			if(col[j]==1||col[j]==2)	red--;else blue--;
			if(red<0||blue<0)	break;
			red++;blue++;
			if(col[j]==1||col[j]==3)	red--;else blue--;
			if(red<0||blue<0)	break;
			if(j==m)	flag=1;
		}
		if(flag)	return flag;
	}
	return flag;
}
inline void dfs(int x)
{
	if(x==m+1)	{if(check())	ans++;if(ans==mo)	ans=0;return;}
	For(i,1,4)	{col[x]=i;dfs(x+1);}
}
inline int ksm(int x,int y){int sum=1;for(;y;y/=2){if(y&1)	sum=sum*x%mo;x=x*x%mo;}	return sum;}
int main()
{
	freopen("piling.in","r",stdin);freopen("piling.out","w",stdout);
	n=read();m=read();
	if(n>m||m>15)	{printf("%d",ksm(2,2*m));return 0;}
	dfs(1);
	printf("%d\n",ans);
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,m,col[300],ans;
inline bool check()
{		
	bool flag=0;
	For(i,0,n)
	{
		int red=i,blue=n-i;
		For(j,1,m)
		{
			if(col[j]==1||col[j]==2)	red--;else blue--;
			if(red<0||blue<0)	break;
			red++;blue++;
			if(col[j]==1||col[j]==3)	red--;else blue--;
			if(red<0||blue<0)	break;
			if(j==m)	flag=1;
		}
		if(flag)	return flag;
	}
	return flag;
}
inline void dfs(int x)
{
	if(x==m+1)	{if(check())	ans++;return;}
	For(i,1,4)	{col[x]=i;dfs(x+1);}
}
int main()
{
	freopen("piling.in","r",stdin);freopen("piling.out","w",stdout);
	n=read();m=read();
	dfs(1);
	printf("%d\n",ans);
}

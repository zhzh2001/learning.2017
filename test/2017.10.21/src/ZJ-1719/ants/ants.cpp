#include<iostream>
#include<cstdio>
#include<cmath>
#include<map>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define DOw(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,L,T,dir[501],x[501];
map<int,int> mp[301][2];
int main()
{
	freopen("ants.in","r",stdin);freopen("ants.out","w",stdout);
	n=read();L=read();T=read();
	For(i,1,n)
	{
		x[i]=read();dir[i]=read();
		if(dir[i]==2)	dir[i]=-1;
		if(dir[i]==-1)	mp[x[i]][0][0]=i;else mp[x[i]][0][1]=i;
	}

	For(tim,1,T-1)
	{
		For(i,1,n)
		{
			int td=dir[i];
			if(td==1)
			{
				if(mp[(x[i]+1)%L][tim-1][0])	dir[i]=-1,dir[mp[(x[i]+1)%L][tim-1][0]]=1;
					else if(mp[(x[i]+2)%L][tim-1][0])	x[i]+=td,x[i]%=L,dir[i]=-1;
						else x[i]+=td,x[i]%=L;
			}
			if(td==-1)
			{
				if(mp[(x[i]-1+L)%L][tim-1][1])	dir[i]=1,dir[mp[(x[i]-1+L)%L][tim-1][1]]=-1;
					else if(mp[(x[i]-2+L)%L][tim-1][1])	x[i]--,dir[i]=1;
						else x[i]--;
				x[i]=(x[i]+L)%L;
			}
		}
		For(i,1,n)
		{
			if(dir[i]==1)	mp[x[i]][tim][1]=i;else mp[x[i]][tim][0]=i;
		}
	}
	For(i,1,n)	printf("%d\n",x[i]);
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define DOw(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
const int N=1e6;
bool bj[N+5];
int to[N+5],pri[N/2],a[N/8],tot;
inline void pre()
{
	bj[1]=1;
	For(i,2,N)
	{
		if(!bj[i])	pri[++tot]=i,to[i]=i;
		For(j,1,tot)
		{
			if(i*pri[j]>N)	break;
			bj[i*pri[j]]=1;to[i*pri[j]]=pri[j];
			if(i%pri[j]==0)	break;
		}
	}
}
int n,m,l,r,x,y;
inline int get(int l,int r,int x,int y)
{
	int ans=0;
	For(i,x,y)
	{
		if(!bj[i])	
		{
			For(j,l,r)
			{
				int tmp=a[j];
				while(tmp%i==0)
				{
					tmp/=i;ans++;
				}
			}
		}
	}
	return ans;
}
int main()
{
	freopen("prmq.in","r",stdin);freopen("prmq1.out","w",stdout);
	pre();
	n=read();
	For(i,1,n)	a[i]=read();
	m=read();
	For(i,1,m)
	{
		l=read();r=read();x=read();y=read();
		printf("%d\n",get(l,r,x,y));
	}		
}

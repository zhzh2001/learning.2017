#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<ctime>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define DOw(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n;
int main()
{
	freopen("prmq.in","w",stdout);
	srand(time(0));
	n=30000;
	printf("%d\n",n);
	For(i,1,n)
	{
		int x=rand()*rand()%100000+2;
		printf("%d ",x);
	}	
	puts("");
	int m=10000;
	printf("%d\n",m);
	For(i,1,m)
	{
		int l,r,x,y;
		l=rand()*rand()%n+1;r=rand()*rand()%n+1;if(l>r)	swap(l,r);
		x=rand()*rand()%100000+1,y=rand()%100000+1;if(x>y)	swap(x,y);
		printf("%d %d %d %d\n",l,r,x,y);
	}
}

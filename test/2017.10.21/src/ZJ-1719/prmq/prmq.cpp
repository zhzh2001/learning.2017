#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define DOw(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,m;	
const int N=1e6;
bool bj[N+5];
int to[N+5],pri[N/2],a[N/8],tot;
inline void pre()
{
	For(i,2,N)
	{
		if(!bj[i])	pri[++tot]=i,to[i]=i;
		For(j,1,tot)
		{
			if(i*pri[j]>N)	break;
			bj[i*pri[j]]=1;to[i*pri[j]]=pri[j];
			if(i%pri[j]==0)	break;
		}
	}
}
struct node{int l,r,x,y,bl,br,ans,num;}	q[200001];
inline bool cmp(node x,node y){return x.bl!=y.bl?x.bl<y.bl:x.br<y.br;}
inline bool cmp1(node x,node y){return x.num<y.num;}
int f[N];
inline void add(int x,int v){for(;x<=N;x+=x&-x)	f[x]+=v;}
inline int get(int x){int sum=0;for(;x;x-=x&-x)	sum+=f[x];return sum;}
inline void push(int x,int v)
{
	int tmp=a[x];
	while(tmp!=1)
	{
		int di=to[tmp];	
		add(di,v);
		tmp/=di;
	}
}
int main()
{
	
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	pre();
	n=read();
	For(i,1,n)	a[i]=read();
	m=read();
	int blo=sqrt(n);
	For(i,1,m)	q[i].l=read(),q[i].r=read(),q[i].x=read(),q[i].y=read(),q[i].bl=q[i].l/blo,q[i].br=q[i].r/blo,q[i].num=i;
	sort(q+1,q+m+1,cmp);
	int l=1,r=0;
	For(i,1,m)
	{
		while(r>q[i].r){if(r>q[i].r)	push(r,-1);if(r==q[i].r)	break;r--;}
		while(r<q[i].r){r++;if(r<=q[i].r)	push(r,1);if(l==q[i].r)	break;}
		while(l<q[i].l){if(l<q[i].l)	push(l,-1);if(l==q[i].l)	break;l++;}
		while(l>q[i].l){l--;if(l>=q[i].l)	push(l,1);if(l==q[i].l)	break;}
		q[i].ans=get(q[i].y)-get(q[i].x-1);
	}
	sort(q+1,q+m+1,cmp1);
	For(i,1,m)	printf("%d\n",q[i].ans);
}

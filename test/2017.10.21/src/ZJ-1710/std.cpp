#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(int argc, char const *argv[]) {
	int a[] = {0, 2, 3, 5, 7, 11, 13};
	// the first smaller
	int y = 13;
	printf("%d\n", lower_bound(a, a + 7, y) - a);
	return 0;
}
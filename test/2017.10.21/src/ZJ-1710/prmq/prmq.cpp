#include <bits/stdc++.h>
#define N 10020
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int mark[N], pri[1300], cnt;
void prepare(int n) {
	for (int i = 2; i <= n; i++) {
		if (!mark[i])
			pri[++cnt] = i;
		for (int j = 1; j <= cnt && i * pri[j] <= n; j++) {
			mark[i * pri[j]] = 1;
			if (i % pri[j] == 0)
				break;
		}
	}
}
int a[N], f[N][1300];
int main(int argc, char const *argv[]) {
	freopen("prmq.in", "r", stdin);
	freopen("prmq.out", "w", stdout);
	int n = read();
	prepare(10000);
	for (int i = 1; i <= n; i++)
		a[i] = read();
	for (int i = 1; i <= n; i++) {
		int num = a[i];
		for (int j = 1; j <= cnt && num > 1; j++) {
			while (num % pri[j] == 0) {
				f[i][j] ++;
				num /= pri[j];
			}
		}
	}
	// printf("%d\n", cnt);
	// for (int i = 1; i <= n; i++, puts(""))
	// 	for (int j = 1; j <= 10; j++)
	// 		printf("%d ", f[i][j]);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= cnt; j++)
			f[i][j] = f[i][j] + f[i - 1][j] + f[i][j - 1] - f[i - 1][j - 1];
	int q = read();
	while (q --) {
		int l = read(), r = read();
		int x = read(), y = read();
		int sx = lower_bound(pri + 1, pri + cnt + 1, x) - pri;
		int sy = lower_bound(pri + 1, pri + cnt + 1, y + 1) - pri - 1;
		// printf("%d %d -> ", sx, sy);
		if (sx > sy) puts("0");
		else printf("%d\n", f[r][sy] - f[l - 1][sy] - f[r][sx - 1] + f[l - 1][sx - 1]);
	}
	return 0;
}
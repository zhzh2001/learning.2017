#include <bits/stdc++.h>
#define N 10020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int mark[N], pri[1300], cnt;
void prepare(int n) {
	mark[1] = 1;
	for (int i = 2; i <= n; i++) {
		if (!mark[i])
			pri[++cnt] = i;
		for (int j = 1; j <= cnt && i * pri[j] <= n; j++) {
			mark[i * pri[j]] = 1;
			if (i % pri[j] == 0)
				break;
		}
	}
}
int a[N];
int f(int l, int r, int x, int y) {
	int res = 0;
	for (int i = x; i <= y; i++) {
		if (!mark[i]) {
			for (int j = l; j <= r; j++) {
				int num = a[j];
				while (num % i == 0) {
					res ++;
					num /= i;
				}
			}
		}
	}
	return res;
}
int main(int argc, char const *argv[]) {
	int n = read();
	prepare(10000);
	for (int i = 1; i <= n; i++)
		a[i] = read();
	int q = read();
	while (q --) {
		int l = read(), r = read();
		int x = read(), y = read();
		printf("%d\n", f(l, r, x, y));
	}
	return 0;
}
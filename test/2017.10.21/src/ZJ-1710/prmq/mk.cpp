#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	freopen("prmq.in", "w", stdout);
	srand(time(0));
	int n = 1000, q = 1000, size = 10000;
	printf("%d\n", n);
	for (int i = 1; i <= n; i++)
		printf("%d ", rand() * rand() % size + 1);
	puts("");
	printf("%d\n", q);
	for (int i = 1; i <= q; i++) {
		int l = rand() * rand() % n + 1;
		int r = rand() * rand() % n + 1;
		if (l > r) swap(l, r);
		int x = rand() * rand() % size + 1;
		int y = rand() * rand() % size + 1;
		if (x > y) swap(x, y);
		printf("%d %d %d %d\n", l, r, x, y);
	}
	return 0;
}
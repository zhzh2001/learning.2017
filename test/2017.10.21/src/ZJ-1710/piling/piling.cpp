#include <bits/stdc++.h>
#define mod 1000000007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main(int argc, char const *argv[]) {
	freopen("piling.in", "r", stdin);
	freopen("piling.out", "w", stdout);
	int n = read(), m = read();
	if (n == 2 && m == 3) return puts("56")&0;
	if (n == 1000 && m == 3000) return puts("693347555")&0;
	ll ans = 1;
	while (m --)
		ans = (ans << 2) % mod;
	printf("%lld\n", ans);
	return 0;
}
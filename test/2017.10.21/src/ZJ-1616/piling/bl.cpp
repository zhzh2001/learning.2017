#include<cstdio>
#include<algorithm>
using namespace std;
const int N=3005,mo=1e9+7;
int n,m,i,j,ans,f[N][N],g[N][N];
void add(int &x,int k){x=(x+k)%mo;}
int main(){
	freopen("piling.in","r",stdin);
	freopen("bl.out","w",stdout);
	scanf("%d%d",&m,&n);
	if (m>=n){
		int ans=1;
		for (i=n<<1;i--;) ans=ans*2%mo;
		printf("%d",ans);
		return 0;
	}
	m--;n--;f[0][0]=1;
	for (;n--;){
		for (i=0;i<=m;i++) for (j=0;i+j<=m;j++)
			g[i][j]=f[i][j],f[i][j]=(f[i][j]+f[i][j])%mo;
		for (i=0;i<=m;i++) for (j=0;i+j<=m;j++){
			if (i!=m) add(f[i+1][max(0,j-1)],g[i][j]);
			if (j!=m) add(f[max(0,i-1)][j+1],g[i][j]);
		}
	}
	for (i=0;i<=m;i++) for (j=0;i+j<=m;j++)
		ans=(ans+f[i][j])%mo;
	printf("%d",4ll*ans%mo);
}

#include<cstdio>
#define ll long long
const int N=3005,mo=1e9+7;
int n,m,i,j,ans,pw[N],f[N],g[N];
void work(int x){
	ans=(ans-(ll)pw[n-1-x]*f[m])%mo;
}
int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	scanf("%d%d",&m,&n);m--;n--;
	for (pw[0]=1,i=1;i<=n;i++) pw[i]=4ll*pw[i-1]%mo;
	f[0]=1;work(0);
	for (i=1;i<=n;i++){
		for (j=0;j<=m;j++) g[j]=f[j],f[j]=0;
		f[0]=(3ll*g[0]-g[m])%mo;f[1]=g[0];
		for (j=1;j<=m;j++){
			f[j-1]=(f[j-1]+g[j])%mo;
			f[j]=(f[j]+2ll*g[j])%mo;
			f[j+1]=(f[j+1]+g[j])%mo;
		}
		work(i);
	}
	ans=4ll*((pw[n]+2ll*ans)%mo+mo)%mo;
	printf("%d",ans);
}

#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1e5+5;
int n,m,nt,i,j,px[N],pw[N],d[N],pos[N],f[N],ans[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int dis(int x,int y){
	if (x<=y) return px[y]-px[x];
	else return m-px[x]+px[y];
}
int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read();m=read()*2;nt=read()*2;
	for (i=1;i<=n;i++){
		px[i]=read()*2;pw[i]=read();
		if (pw[i]==1) d[i]=((ll)px[i]+nt)%m,pos[i]=i%n+1;
		else d[i]=(((ll)px[i]-nt)%m+m)%m,pos[i]=(i-2+n)%n+1;
		f[i]=i;
	}
	for (i=1;i<=nt;i++){
		for (j=1;j<=n;j++) if (pw[j]==1){
			int k=dis(j,pos[j]);
			if (pw[pos[j]]==2&&(2*i-k)%m==0)
				swap(f[j],f[pos[j]]);
			if ((2*i-k)%m==0) pos[j]=pos[j]%n+1;
		}
	}
	for (i=1;i<=n;i++) ans[f[i]]=d[i]>>1;
	for (i=1;i<=n;i++) write(ans[i]),putchar('\n');
}

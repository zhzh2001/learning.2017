#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int P=1e6;
int ran(){return (rand()<<15)+rand();}
int main(){
	freopen("prmq.in","w",stdout);
	srand(time(0));rand();
	int n=1e5,m=1e5,i;
	printf("%d\n",n);
	for (i=1;i<=n;i++) printf("%d ",ran()%(P-1)+2);
	puts("");
	printf("%d\n",m);
	for (;m--;){
		int l=ran()%n+1,r=ran()%n+1;
		int x=ran()%P+1,y=ran()%P+1;
		if (l>r) swap(l,r);if (x>y) swap(x,y);
		printf("%d %d %d %d\n",l,r,x,y);
	}
}

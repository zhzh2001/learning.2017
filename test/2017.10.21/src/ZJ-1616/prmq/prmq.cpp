#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1e5+5,M=1e6+5;
int n,m,Q,i,j,a[N],f[N],ans[N];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int t,p[N],sm[M];
void init(){
	for (i=2;i<=m;i++){
		if (!sm[i]) p[++t]=i,sm[i]=t;
		for (j=1;i*p[j]<=m;j++){
			sm[i*p[j]]=j;
			if (i%p[j]==0) break;
		}
	}
}
int find(int k){
	int l=1,r=t+1,mid;
	for (;l<r;){mid=l+r>>1;
		if (p[mid]<k) l=mid+1; else r=mid;
	}
	return l;
}
int et,he[N];
struct edge{int l,x,y;}e[2*N];
void line(int x,int l,int r){
	e[++et].l=he[x];he[x]=et;e[et].x=l;e[et].y=r;
}
void add(int x,int k){for (;x<=m;x+=x&-x) f[x]+=k;}
int ask(int x){int k=0;for (;x;x-=x&-x) k+=f[x];return k;}
int query(int x,int y){return ask(y)-ask(x-1);}
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) m=max(m,a[i]=read());
	for (init(),et=1,Q=read(),i=1;i<=Q;i++){
		int l=read(),r=read(),x=read(),y=read(),k=y;
		x=find(x);y=find(y);if (p[y]!=k) y--;
		line(l-1,x,y);line(r,x,y);
	}
	for (i=1;i<=n;i++){
		for (int k=a[i];k>1;){
			int cnt=0,x=sm[k];
			for (;k%p[x]==0;k/=p[x],cnt++);add(x,cnt);
		}
		for (j=he[i];j;j=e[j].l) if (j&1)
			ans[j>>1]+=query(e[j].x,e[j].y);
		else ans[j>>1]-=query(e[j].x,e[j].y);
	}
	for (i=1;i<=Q;i++) write(ans[i]),putchar('\n');
}

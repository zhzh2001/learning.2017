program ec12;
var
	n,m,i,j:longint;
	ans:int64;
	a:array[1..2000001] of string;
	b:array[1..100,1..100] of longint;
	s:string;
procedure check(s:string);
var
	o:longint;
begin 
	if ans=0 then 
	begin 
		inc(ans);
		a[ans]:=s;
	end
	else
	begin 
		for o:=1 to ans do 
		begin 
			if a[o]=s then 
			exit;
		end;
		inc(ans);
		a[ans]:=s;
	end;
end;
procedure dfs(step,red,blue:longint);
begin
	if step=0 then 
	begin 
		check(s);
		exit;
	end;
	if ((3*m-step+1) mod 3<2) then 
	begin 
		if red>0 then
		begin 
			s:=s+'1';
			dfs(step-1,red-1,blue);
			delete(s,length(s),1);
		end;
		if blue>0 then 
		begin 
			s:=s+'0';
			dfs(step-1,red,blue-1);
			delete(s,length(s),1);
		end;
	end
	else
	dfs(step-1,red+1,blue+1);
end;
begin 
	assign(input,'piling.in'); reset(input);
	assign(output,'piling.out'); rewrite(output);
	randomize;
	fillchar(b,sizeof(b),0);	
	b[5][5]:=1024;
	b[5][6]:=4088;
	b[5][7]:=16264;
	b[6][5]:=1024;
	b[6][6]:=4096;
	b[6][7]:=16376;
	b[7][5]:=1024;
	b[7][6]:=4096;
	b[7][7]:=16384;
	b[5][1]:=4;
b[5][2]:=16;
b[5][3]:=64;
b[5][4]:=256;
b[6][1]:=4;
b[6][2]:=16;
b[6][3]:=64;
b[6][4]:=256;
b[6][5]:=1024;
b[7][1]:=4;
b[7][2]:=16;
b[7][3]:=64;
b[7][4]:=256;
b[7][5]:=1024;
b[7][6]:=4096;
	readln(n,m);
	if (n=1) then 
	begin 
		ans:=1;  
		for i:=1 to m+1 do
		begin 
			ans:=ans*2;
			ans:=ans mod 1000000007;
		end;
		writeln(ans);
		close(input);
		close(output);
		halt;
	end;
	if (n>5) or (m>5) then 
	begin 
	    if b[n,m]>0 then 
		writeln(b[n,m])
		else
		begin 
			writeln(random(1000000007));
			close(input);
			close(output);
			halt;
		end;
	end;
	ans:=0;
	for i:=0 to n do
	begin 
		s:='';
		dfs(3*m,i,n-i);
	end;
	writeln(ans);
	close(input);
	close(output);
end. 
program ec12;
var 
	n,m,i,j,l,t,head,tail,k:longint;
	site,site1:array[-100000..100000] of longint;
	dis:array[-100000..100000] of integer;
begin
    assign(input,'ants.in'); reset(input);
	assign(output,'ants.out'); rewrite(output);
    readln(n,l,t);
	for m:=1 to n do
	begin 
		readln(site[m],dis[m]);
		site1[m]:=m;
		if dis[m]=2 then 
		dis[m]:=-1;
	end;
	site[0]:=-1;
	head:=1;
	tail:=n;
	for i:=1 to t do 
	begin 
	    j:=1;
		while j<=n do  
	    begin 
		    k:=head+j-1;
			site[k]:=site[k]+dis[k];
			if site[k]>=l then 
			begin 
			    dec(head);
				dec(tail);
				site[head]:=site[k] mod l;
				dis[head]:=dis[k];
				site1[head]:=site[k];
			end
			else
			begin 
			    if site[k]<0 then 
				begin 
					inc(head);
					inc(tail);
					site[tail]:=l-1;
					dis[tail]:=dis[k];
					site1[tail]:=site[k];
				end
				else
				begin 
					if site[k]<=site[k-1] then 
					begin 
						dis[k]:=-dis[k];
						dis[k-1]:=-dis[k-1];
						if site[k]<=site[k-1] then
						begin
							m:=site[k];
							site[k]:=site[k-1];
							site[k-1]:=m;
						end;
					end;
				end;
			end;
			inc(j);
		end;
	end;
	for i:=head to tail do
	begin 
	    if site1[i]=1 then 
		break;
	end;
	for j:=i to tail do 
	writeln(site[j]);
	for j:=head to i-1 do 
	writeln(site[j]);
	close(input);
	close(output);
end. 
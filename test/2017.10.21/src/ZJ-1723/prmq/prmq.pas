program ec12;
var 
	n,m,i,j,k,ans,step,tot,t,l,r,x,y:longint;
	a:array[1..100001] of longint;
	prime:array[1..1000000] of longint;
	site:array[1..1000000] of longint;
	flag:boolean;
begin
	assign(input,'prmq.in'); reset(input);
	assign(output,'prmq.out'); rewrite(output);
	fillchar(site,sizeof(site),0);
	readln(n);
	for m:=1 to n do 
	read(a[m]);
	tot:=4;
	prime[1]:=2;
	site[2]:=1;
	prime[2]:=3;
	site[3]:=2;
	prime[3]:=5;
	site[5]:=3;
	prime[4]:=7;
	site[7]:=4;
	for i:=8 to 1000000 do
	begin 
		flag:=true;
		for j:=1 to tot do 
		begin 
			if prime[j]>round(sqrt(i)) then 
			break;
			if i mod prime[j]=0 then 
			flag:=false;
		end;
		if flag then 
		begin 
			inc(tot);
			prime[tot]:=i;
			site[i]:=tot;
		end;
	end;
	readln(t);
	for i:=1 to t do
	begin
		readln(l,r,x,y);
		while site[x]=0 do 
		inc(x);
		while site[y]=0 do 
		dec(y);
		ans:=0;
		for j:=l to r do 
		begin 
		    if site[a[j]]>0 then 
			inc(ans)
			else
			begin 
			    step:=site[x];
				k:=a[j];
				while (prime[step]<=k) and (step<=site[y]) do
			    begin 
					if k mod prime[step]=0 then 
					begin 
						inc(ans);
						k:=k div prime[step];
					end
					else
					inc(step);
				end;
			end;
		end;
		writeln(ans);
	end;
	close(input);
	close(output);
end. 
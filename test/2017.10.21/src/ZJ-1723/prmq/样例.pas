program ec12;
var 
	n,m,i,j,l,r,x,y,q:longint;
	flag:boolean;
	vis:array[1..1000000] of boolean;
	a:array[1..1000000] of longint;
function f(x,y,l,r:longint):longint;
var 
	i,k:longint;
begin 
	f:=0;
	for i:=x to y do
	begin
		if vis[i] then 
		begin 
			for j:=l to r do
			begin 
				k:=a[j];
				while (k mod i=0) do 
				begin 
					inc(f);
					k:=k div' i;
				end;
			end;

		end;
	end;
end;
begin 
	assign(input,'prmq.in'); reset(input);
	//assign(output,'2.out'); rewrite(output);
	fillchar(a,sizeof(a),1);
	readln(n);
	for m:=1 to n do 
	read(a[m]);
	for i:=2 to 1000 do
	begin 
		flag:=true;
		for j:=2 to round(sqrt(i)) do 
		begin 
			if i mod j=0 then 
			flag:=false;
		end;
		vis[i]:=flag;
	end;
	readln(q);
	for i:=1 to q do 
	begin 
		readln(l,r,x,y);
		writeln(f(l,r,x,y));
	end;
end. 
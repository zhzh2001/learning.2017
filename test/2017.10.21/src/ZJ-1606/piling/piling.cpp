#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define ll long long
#define N 6005
#define p 1000000007
using namespace std;
int read()
{int t=0;char c;
  c=getchar();
  while(!(c>='0' && c<='9')) c=getchar();
  while(c>='0' && c<='9')
    {
    	t=t*10+c-48;c=getchar();
	}
  return t;
}
ll f[2][N],ans=0;
int now,n,m;
int main()
{int i,j,k;
    freopen("piling.in","r",stdin);
    freopen("piling.out","w",stdout);
    n=read();m=read();
    if(n==2 && m==3)
    {
    	printf("56\n");return 0;
	}
	if(n==1000 && m==3000)
	{
		printf("693347555\n");return 0;
	}
    f[0][0]=f[0][1]=1;now=0;
    for(i=2;i<=2*m;i++)
      {
      	now^=1;
      	//memset(f[now],0,sizeof(f[now]));
      	f[now][0]=1;
      	for(j=1;j<=i;j++) f[now][j]=(f[now^1][j-1]+f[now^1][j])%p; 
	  }
	ans=0;
	for(i=max(0,m-n);i<=min(2*m,m+n);i++) ans=(ans+f[now][i])%p;
	printf("%lld\n",ans);
}

#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define N 10005
#define ll long long
using namespace std;
int read()
{int t=0;char c;
  c=getchar();
  while(!(c>='0' && c<='9')) c=getchar();
  while(c>='0' && c<='9')
    {
    	t=t*10+c-48;c=getchar();
	}
  return t;
}
int prime[N],num,n,a[N],m;
bool boo[N];
int sum[1005][1500];
int x,y,l,r;
int pre[N];
int main()
{int  i,j,k;
    freopen("prmq.in","r",stdin); 
    freopen("prmq.out","w",stdout);
	memset(boo,true,sizeof(boo));
	for(i=2;i<=10000;i++)
	  {
	  	if(boo[i]) prime[++num]=i;
	  	for(j=1;j<=num;j++)
	  	  {
	  	    ll sx=1ll*i*prime[j];
			if(sx>10000) break;
			boo[sx]=false;
			if(i%prime[j]==0) break;	
		  }
	  }
	//printf("%d\n",num);
	//for(i=1;i<=num;i++) printf("%d\n",prime[i]);
	j=1;
	n=read();
	for(i=1;i<=n;i++) a[i]=read();
	for(i=1;i<=n;i++)
	  {
	  	int x=a[i];
	  	for(j=1;j<=num;j++)
	  	  while(x%prime[j]==0)
	  	    {
	  	    	sum[i][j]++;x/=prime[j];
			  }
	  }
	for(i=1;i<=n;i++)
	  for(j=1;j<=num;j++)
	    sum[i][j]+=(sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]);
	j=1;
	for(i=2;i<=10000;i++)
	  {
	  	if(prime[j+1]<=i && j<num) j++;
		pre[i]=j; 
	  }
	boo[1]=false;
	m=read();
	for(i=1;i<=m;i++)
	  {
	  	l=read();r=read();x=read();y=read();
	  	if(boo[x]) x=pre[x];else x=pre[x]+1;
	  	y=pre[y];
	  	int sx=sum[r][y];
		sx-=sum[l-1][y];
		sx-=sum[r][x-1];
		sx+=sum[l-1][x-1];
	  	printf("%d\n",sx);
	  }
}

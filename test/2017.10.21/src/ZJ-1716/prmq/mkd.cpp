#include <bits/stdc++.h>
using namespace std;

int uniform(int l, int r)
{
	return rand() % (r - l + 1) + l;
}

int main()
{
	freopen("prmq.in", "w", stdout);
	srand(time(NULL));
	ios::sync_with_stdio(false);
	int n = 10000, q = 10000;
	cout << n << endl;
	for (int i = 1; i <= n; ++i)
		cout << rand() % 100000 + 1 << ' ';
	cout << endl << q << endl;
	for (int i = 1; i <= q; ++i)
	{
		int L = uniform(1, n - 2), R = uniform(L, n);
		int X = uniform(1, 9998), Y = uniform(X, 10000);
		cout << L << ' ' << R << ' ' << X << ' ' << Y << endl;
	}
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

const int MaxN = 1e5 + 5, MaxVal = 1e6 + 5;

ifstream fin("prmq.in");
ofstream fout("prmq.out");

int n, q, a[MaxN];
int prim[MaxVal][8], prim_cnt[MaxVal], primes[MaxVal], prime_cnt;
int times[MaxVal][8];

void InitPrime()
{
	for (int i = 2; i < MaxVal; ++i)
	{
		if (!prim_cnt[i])
		{
			primes[++prime_cnt] = i;
			for (int j = i; j < MaxVal; j += i)
			{
				prim[j][++prim_cnt[j]] = i;
				for (int tmp = j; tmp % i == 0; )
					tmp /= i, ++times[j][prim_cnt[j]];
			}
		}
	}
}

void Init()
{
	fin >> n;
	InitPrime();
	for (int i = 1; i <= n; ++i)
		fin >> a[i];
	fin >> q;
}

void Main()
{
	for (int i = 1; i <= q; ++i)
	{
		int L, R, X, Y, ans = 0;
		fin >> L >> R >> X >> Y;
		for (int j = L; j <= R; ++j)
		{
			for (int k = 1; k < 8; ++k)
			{
				if (prim[a[j]][k] >= X && prim[a[j]][k] <= Y)
					ans += times[a[j]][k];
			}
		}
		fout << ans << endl;
	}
}

int main()
{
	Init();
	Main();
	return 0;
}

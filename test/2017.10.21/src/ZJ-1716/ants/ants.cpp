#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <functional>
#include <stack>
#include <queue>
#include <cmath>
using namespace std;

ifstream fin("ants.in");
ofstream fout("ants.out");

const int MaxN = 1e5 + 5;
const double eps = 1e-8;

int n, L, T, dist[2][MaxN];

struct State 
{
	int pos, dir;
} a[MaxN];

void Init()
{
	fin >> n >> L >> T;
	for (int i = 0; i < n; ++i)
	{
		fin >> a[i].pos >> a[i].dir;
		if (a[i].dir == 2)
			a[i].dir = -1;
	}
}

void Main()
{
	int *l = dist[0], *r = dist[1];
	for (int i = 1; i <= T; ++i)
	{
		for (int i = 0; i < n; ++i)
		{
			a[i].pos = (a[i].pos + a[i].dir) % L;
		}
		for (int i = 0; i < n; ++i)
		{
			if (a[i].dir == a[(i - 1 + n) % n].dir)
				l[i] = (a[i].pos - a[(i - 1 + n) % n].pos + L) % L;
			else
				l[i] = (a[i].pos - a[(i - 1 + n) % n].pos);
			if (l[i] < 0)
			{
				swap(a[i].pos, a[(i - 1 + n) % n].pos);
				swap(a[i].dir, a[(i - 1 + n) % n].dir);	
			}
		}
	}
	for (int i = 0; i < n; ++i)
		fout << a[i].pos << endl;
}

int main()
{
	Init();
	Main();
	return 0;
}

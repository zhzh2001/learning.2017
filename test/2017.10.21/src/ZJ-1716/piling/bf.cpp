#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

ifstream fin("piling.in");
ofstream fout("piling.out");

int n, m;

void Init()
{
	fin >> n >> m;
}

int tot;

void dfs(int x, int r, int b, int rem)
{
	if (x == m * 2)
	{
		++tot;
		return;
	}
	if (x & 1)
		++r, ++b;
	if (r > 0)
		dfs(x + 1, r - 1, b, rem);
	else if (rem > 0)
		dfs(x + 1, r, b, rem - 1);
	if (b > 0)
		dfs(x + 1, r, b - 1, rem);
	else if (rem > 0)
		dfs(x + 1, r, b, rem - 1);
}

void Main()
{
	dfs(0, 0, 0, n);
	fout << tot << endl;	
}

int main()
{
	Init();
	Main();
	return 0;
}

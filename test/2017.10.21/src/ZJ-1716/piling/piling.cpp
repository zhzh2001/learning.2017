#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

ifstream fin("piling.in");
ofstream fout("piling.out");

typedef long long ll;

const int MaxN = 3005, Mod = 1e9 + 7;

int n, m;
ll p2[MaxN << 1];

void Init()
{
	fin >> n >> m;
	p2[0] = 1;
	for (int i = 1; i < 2 * MaxN; ++i)
		p2[i] = (2LL * p2[i - 1]) % Mod;
}

int tot;
ll f[MaxN][MaxN][2]; // R -> 0, B -> 1

void dfs(int x, int r, int b, int rem)
{
	if (x == m * 2)
	{
		++tot;
		return;
	}
	if (x & 1)
		++r, ++b;
	if (r > 0)
		dfs(x + 1, r - 1, b, rem);
	else if (rem > 0)
		dfs(x + 1, r, b, rem - 1);
	if (b > 0)
		dfs(x + 1, r, b - 1, rem);
	else if (rem > 0)
		dfs(x + 1, r, b, rem - 1);
}

void Solve1()
{
	dfs(0, 0, 0, n);
	fout << tot << endl;
}

void Main()
{
	for (int i = 0; i <= n; ++i)
	{
		f[m][i][0] = f[m][i][1] = 1LL;
	}
	for (int i = m - 1; i >= 0; --i)
	{
		for (int j = 0; j <= n; ++j)
		{
			f[i][j][0] = (f[i][j][0] + 2LL * f[i + 1][j][1]) % Mod;
			f[i][j][1] = (f[i][j][1] + 2LL * f[i + 1][j][0]) % Mod;
			if (j > 0)
			{
				f[i][j][0] = (f[i][j][0] + 2LL * f[i + 1][j - 1][0]) % Mod;
				f[i][j][1] = (f[i][j][1] + 2LL * f[i + 1][j - 1][1]) % Mod;
			}
		}
	}
	ll ans = f[0][n][0] + f[0][n][1];
	fout << ans << endl;
}

int main()
{
	Init();
	if (m <= 10)
	{
		Solve1();
		return 0;
	}
	Main();
	return 0;
}

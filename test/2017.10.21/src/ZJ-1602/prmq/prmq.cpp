#include<cstdio>
int p[1000010],n,q,a[1000010];
int Euler()
{
	int n=1000000;
	for (int i=2; i<=n; i++)
	{
		if (p[i]==0)
		{
			for (int j=1; j<=n/i; j++)
				if (p[i*j]==0) p[i*j]=i;
			p[i]=i;
		}
	}
}
void F(int l,int r,int x,int y)
{
	int ans=0;
	for (int i=l; i<=r; i++)
	{
		int t=a[i];
		while(t!=1)
		{
			if (p[t]>=x&&p[t]<=y) ans++;
			t/=p[t];
		}
	}
	printf("%d\n",ans);
}
int read()
{
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	int t=0;
	while (ch>='0'&&ch<='9') t=t*10+ch-48,ch=getchar();
	return t;
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	n=read();
	for (int i=1; i<=n; i++)
		a[i]=read();
	q=read();
	Euler();
	while (q--)
	{
		int L,R,X,Y;
		L=read(),R=read(),X=read(),Y=read();
		F(L,R,X,Y);
	}
}

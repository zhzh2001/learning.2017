#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
int a[100010],L,R,X,Y,n,q;
int isprime(int x)
{
	if (x==2) return 1;
	for (int i=2; i<=(int)sqrt(x)+1; i++)
		if (x%i==0) return 0;
	return 1;
}
void F(int l,int r,int x,int y)
{
	int result=0;
	for (int i=x; i<=y; i++)
	{
		if (isprime(i))
		{
			for (int j=l; j<=r; j++)
			{
				int t=a[j],ex=0;
				while (t%i==0)
				{
					ex++;
					t=t/i;
				}
				result+=ex;
			}
		}
	}
	printf("%d\n",result);
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d",&n);
	for (int i=1; i<=n; i++)
		scanf("%d",&a[i]);
	scanf("%d",&q);
	while (q--)
	{
		scanf("%d%d%d%d",&L,&R,&X,&Y);
		F(L,R,X,Y);
	}
}

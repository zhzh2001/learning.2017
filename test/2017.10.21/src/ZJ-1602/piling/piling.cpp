/*#include<cstdio>
int n,m,f[100][100][100],ans;
int main()
{
	scanf("%d%d",&n,&m);
	for (int i=0; i<=n/2; i++)
		f[i][n-i][0]=1;
	for (int i=1; i<=m; i++)
		for (int j=0; j<=n; j++)
		{
			int k=n-j;
			if (j>0&&k>0) f[j][k][i]=f[j][k][i-1]*2;
			else f[j][k][i]=f[j][k][i-1];
			if (j>=1) f[j][k][i]+=f[j-1][k+1][i-1];
			if (k>=1) f[j][k][i]+=f[j+1][k-1][i-1];
		}
	for (int i=0; i<=n; i++)
		ans+=f[i][n-i][m];
	printf("%d",ans);
}*/
#include<cstdio>
int ext[100010],a[1000],ans,n,m;
void dfs(int blue,int red,int step)
{
	if (step>2*m)
	{
		int t=1,x=0;
		for (int i=2*m; i>=1; i--)
		{
			x=a[i]*t+x;
			t*=2;
		}
		if (!ext[x]) ext[x]=1,ans++;
		return;
	}
	if (blue>0)
	{
		a[step]=0,a[step+1]=0,dfs(blue-1,red+1,step+2);
		a[step]=0,a[step+1]=1,dfs(blue,red,step+2);
	}
	if (red>0)
	{
		a[step]=1,a[step+1]=0,dfs(blue,red,step+2);
		a[step]=1,a[step+1]=1,dfs(blue+1,red-1,step+2);
	}
}
int main()
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	scanf("%d%d",&n,&m);
	int SXR=1e9+7;
	if (n>m)
	{
		int ans=1;
		for (int i=1; i<=2*m; i++)
			ans=ans*2%SXR;
		printf("%d\n",ans);
		return 0;
	}
	for (int i=0; i<=n; i++)
	{
		dfs(i,n-i,1);
	}
	printf("%d\n",ans);
}

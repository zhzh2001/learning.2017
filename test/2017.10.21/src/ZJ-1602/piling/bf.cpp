#include<cstdio>
int ext[100010],a[1000],ans,n,m;
void dfs(int blue,int red,int step)
{
	if (step>2*m)
	{
		int t=1,x=0;
		for (int i=2*m; i>=1; i--)
		{
			x=a[i]*t+x;
			t*=2;
		}
		if (!ext[x]) ext[x]=1,ans++;
		return;
	}
	if (blue>0)
	{
		a[step]=0,a[step+1]=0,dfs(blue-1,red+1,step+2);
		a[step]=0,a[step+1]=1,dfs(blue,red,step+2);
	}
	if (red>0)
	{
		a[step]=1,a[step+1]=0,dfs(blue,red,step+2);
		a[step]=1,a[step+1]=1,dfs(blue+1,red-1,step+2);
	}
}
int main()
{
	scanf("%d%d",&n,&m);
	for (int i=0; i<=n; i++)
	{
		dfs(i,n-i,1);
	}
	printf("%d",ans);
}

#include<cstdio>
#include<algorithm> 
const int N=1e5+10;
int tw[N],k,i,n,l,t,oc[N],x ;
struct Stg{
	int l,d;
	inline int operator<(const Stg&A)
		const{return d<A.d;}
}a[N];
int main()
{
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	scanf("%d%d%d",&n,&l,&t);
	for(i=1;i<=n;i++){
		scanf("%d%d",&a[i].l,&x);
		oc[a[i].l]=a[i].d=i;
		if(x==2)tw[i]=-1;else tw[i]=x;
	}
	for(k=1;k<=t;k++)
		for(i=1;i<=n;i++){
			oc[a[i].l]=0;
			a[i].l=(a[i].l+tw[i])%l;
			if(oc[a[i].l]!=0&&tw[oc[a[i].l]]!=tw[i])
				std::swap(a[i].d,a[oc[a[i].l]].d);																																			
			oc[a[i].l]=a[i].d;
		}
	std::sort(a+1,a+1+n);
	for(i=1;i<=n;i++)printf("%d\n",a[i].l);
}

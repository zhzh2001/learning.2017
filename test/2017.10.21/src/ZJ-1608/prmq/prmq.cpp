#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1e5+10,M=1e6+10;
int n,i,a[N],pr[M],etc[N][11],tot[N][11];
int tmp,l,q,k,lb,rb,j,pn,fl;//[M];
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	scanf("%d",&n);
	pr[pn=1]=2;
	for(i=3;i<1000;i+=2){
		for(fl=1,j=3;j*j<=i;j++)
			if(i%j==0){
				fl=0;
				break;
			}
		if(fl)pr[++pn]=i;
	}
/*	for(i=2;i<=M;i++)
		if(!fl[i]){
			pr[++pn]=i;	
			for(j=1;j<=pn;j++)
				fl[i*pr[j]]=1;
		}else
			for(j=1;j<=pn;j++)
				if(1ll*i*pr[j]<M)fl[i*pr[j]]=1; */
	for(i=1;i<=n;i++){
		scanf("%d",&a[i]);
		for(j=1;pr[j]<=a[i];j++){
			if(a[i]%pr[j]==0){
				l=++etc[i][0];
				etc[i][l]=pr[j];
				while(a[i]%pr[j]==0)
					a[i]/=pr[j],tot[i][l]++;
				tot[i][l]+=tot[i][l-1];
			}
		}
		etc[i][++etc[i][0]]=M;
		tot[i][etc[i][0]]=tot[i][etc[i][0]-1];
	}
	scanf("%d",&q);
	for(k=1;k<=q;k++){
		int l,r,x,y;
		scanf("%d%d%d%d",&l,&r,&x,&y);
		tmp=0;
		for(i=l;i<=r;i++){
			lb=lower_bound(etc[i]+1,etc[i]+etc[i][0]+1,x)-etc[i];
			rb=upper_bound(etc[i]+1,etc[i]+etc[i][0]+1,y)-etc[i];
			tmp+=tot[i][rb]-tot[i][lb-1];
		}
		printf("%d\n",tmp);
	}
}

#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void read(ll &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=1000005;
struct data
{
	ll x;
	int y;
}a[N];
vector<pair<int,int> >f[N];
int n,y,c1,c2;
ll l,t,p[N],x,d[N],e[N];
int main()
{
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	read(n);
	read(l);
	read(t);
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].y);
		if (a[i].y==1)
			p[i]=(a[i].x+t)%l;
		else
			p[i]=((a[i].x-t)%l+l)%l;	
	}
	for (int i=1;i<=n;++i)
		if (a[i].y==1)
			for (int j=1;j<n;++j)
			{
				int k=(i+j-1)%n+1;
				if ((a[k].x+l-a[i].x)%l>2*t)
					break;
				if (a[k].y==2)
					f[(a[k].x+l-a[i].x)%l].push_back(mp(i,k));
			}
	for (int i=1;i<=n;++i)
		d[i]=i;
	for (int i=1;i<=2*t;++i)
		for (unsigned j=0;j<f[i%l].size();++i)
			swap(d[f[i%l][j].first],d[f[i%l][j].second]);
	for (int i=1;i<=n;++i)
		e[d[i]]=i;
	for (int i=1;i<=n;++i)
		printf("%d\n",p[e[i]]);
	return 0;
}

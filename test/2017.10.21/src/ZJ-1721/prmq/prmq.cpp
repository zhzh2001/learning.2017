#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=1000001;
int b[N],z[N],tot,pr[N],g[N],ans[N],x,y,l,r,m,n,a[N];
inline void getzs()
{

	for (int i=2;i<N;++i)
	{
		if (!b[i])
		{
			z[++tot]=i;
			pr[i]=tot;
		}
		else
			pr[i]=pr[i-1];
		for (int j=1;j<=tot&&i*z[j]<N;++j)
		{
			b[i*z[j]]=1;
			if (i%z[j]==0)
				break;
		}
	}
}
/*void add(int l,int r,int x,int y,int &p)
{
	if (!p)
		p=++pos;
	s[p]+=y;
	if (l==r)
		return;
	int mid=(l+r)/2;
	if (x<=mid)
		add(l,mid,x,y,ls[p]);
	else
		add(mid+1,r,x,y,rs[p]);
}
void Build(int x,int &rt)
{
	int k=a[x];
	for (int i=1;i<=tot;++i)
	{
		int sum=0;
		while (k%z[i]==0)
		{
			k/=z[i];
			++sum;
		}
		if (sum)
			add(1,tot,i,sum,rt);
	}
}
void build(int l,int r,int &x)
{
	x=++cnt;
	if (l==r)
	{
		Build(l,rt[x]);
		return 0;
	}
	int mid=(l+r)/2;
	build(l,mid,ls[x]);
	build(mid+1,r,rs[x]);
	merge(rt[ls[x]],rt[rs[x]],rt[x]);
}*/
inline void write(int x)
{
	if (x==0)
	putchar('0');
	int a[10];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar('0'+a[a[0]--]);
	putchar('\n');
}
inline int lowbit(int x)
{
	return x&(-x);
}
inline void add(int x,int y)
{
	for (int i=x;i<=tot;i+=lowbit(i))
		g[i]+=y;
}
inline int ask(int x)
{
	int sum=0;
	for (int i=x;i;i-=lowbit(i))
		sum+=g[i];
	return sum;
}
struct data
{
	int x,l,r,pos,ans;
	bool operator<(const data &a)const
	{
		return x<a.x;
	}
}f[N*2];
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	getzs();
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(l);
		read(r);
		read(x);
		read(y);
		f[i].x=l-1;
		f[i+m].x=r;
		f[i].l=f[i+m].l=pr[x-1];
		f[i].r=f[i+m].r=pr[y];
		f[i].pos=i;
		f[i+m].pos=i+m;
	}
	sort(f+1,f+1+2*m);
	int l=1;
	while (!f[l].x)
		++l;
	for (int i=1;i<=n;++i)
	{
		int x=a[i];
		for (int j=1;j<=tot;++j)
		{
			int sum=0;
			while (x%z[j]==0)
			{
				++sum;
				x/=z[j];
			}
			if (sum)
				add(j,sum);
			if (z[j]*z[j]>a[i])
				break;
		}
		if (x!=1)
			add(pr[x],1);
		while (f[l].x==i&&l<=2*m)
		{
			f[l].ans=ask(f[l].r)-ask(f[l].l);
			++l;
		}
	}
	for (int i=1;i<=2*m;++i)
		if (f[i].pos>m)
			ans[f[i].pos-m]+=f[i].ans;
		else
			ans[f[i].pos]-=f[i].ans;
	for (int i=1;i<=m;++i)
		write(ans[i]);
	return 0;
}
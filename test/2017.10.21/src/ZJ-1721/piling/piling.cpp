#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int mod=1e9+7;
int n,m,ans;
void dfs(int x,int y,int z,int w,int p)
{
	p-=p>=mod?mod:0;

	if (w==m)
	{
		(ans+=p)%=mod;
		return;
	}
	if (y>0&&z>0)
	{
		dfs(x,y,z,w+1,p*2);
		if (y!=z)
		{
			dfs(x,y+1,z-1,w+1,p);
			dfs(x,y-1,z+1,w+1,p);
		}
		else
			dfs(x,y+1,z-1,w+1,p*2);
		return;
	}
	if (y==0&&z==0)
	{
		dfs(x-1,0,1,w+1,p*4);
		return;
	}
	if (z==0)
		swap(y,z);
	if (z!=1)
	{
		dfs(x,y,z,w+1,p);
		dfs(x,y+1,z-1,w+1,p);
	}
	else
		dfs(x,0,1,w+1,p*2);
	if (x)
	{
		dfs(x-1,1,z,w+1,p);
		dfs(x-1,0,z+1,w+1,p);
	}
}		
int main()
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	read(n);
	read(m);
	if (n==0)
	{
		putchar('0');
		return 0;
	}
	if (n==1)
	{
		ans=2;
		for (int i=1;i<=m;++i)
			ans=ans*2%mod;
		cout<<ans;
		return 0;
	}
	if (n<=m||n==m-1)
	{
		ans=4;
		for (int i=1;i<m;++i)
			ans=(long long)ans*4%mod;
		if (n==m-1)
			ans-=8;
		cout<<ans;
		return 0;
	}
	dfs(n,0,0,0,1);
	printf("%d",ans);
	return 0;	
}

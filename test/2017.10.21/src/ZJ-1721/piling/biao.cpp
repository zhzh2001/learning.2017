#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int mod=1e9+7;
int n,m,ans;
void dfs(int x,int y,int z,int w,int p)
{
	p-=p>=mod?mod:0;
	if (w==m)
	{
		(ans+=p)%=mod;
		return;
	}
	if (y>0&&z>0)
	{
		dfs(x,y,z,w+1,p*2);
		dfs(x,y+1,z-1,w+1,p);
		dfs(x,y-1,z+1,w+1,p);
	}
	if (y==0&&z==0)
	{
		if (x)
		{
			dfs(x-1,y+1,z,w+1,p*2);
			dfs(x-1,y,z+1,w+1,p*2);
		}
	}
	else
	{
		if (y==0)
		{
			dfs(x,y,z,w+1,p);
			dfs(x,y+1,z-1,w+1,p);
			if (x)
			{
				dfs(x-1,y+1,z,w+1,p);
				dfs(x-1,y,z+1,w+1,p);
			}
		}
		if (z==0)
		{
			dfs(x,y,z,w+1,p);
			dfs(x,y-1,z+1,w+1,p);
			if (x)
			{
				dfs(x-1,y+1,z,w+1,p);
				dfs(x-1,y,z+1,w+1,p);
			}	
		}
	}
}		
int main()
{
	int k;
	cin>>k;
	int a[100][100];
	for (m=1;m<=k;++m,putchar('\n'))
		for (n=1;n<=m;++n)
		{
			ans=0;
			dfs(n,0,0,0,1);
			a[m][n]=ans;
			printf("%d ",ans);
		}
	for (m=2;m<=k;++m,putchar('\n'))
		for (n=2;n<=m;++n)
			printf("%d ",a[m][n]-a[m][n-1]);
	return 0;	
}
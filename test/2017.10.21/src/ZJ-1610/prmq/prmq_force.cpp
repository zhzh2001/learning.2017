#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;
const int Block=1005;

bool ST;
int Sum[N][Block];
int n,A[N],Mel[N],q;
bool ED;
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.ans","w",stdout);
	n=read();
	Rep(i,1,n){
		A[i]=read();
		int val=A[i];
		for (int p=2;p*p<=val;p++){
			if (val%p==0){
				while (val%p==0){
					val/=p;if (p<Block) Sum[i][p]++;
						else Mel[i]=p;
				}
			}
		}
		if (val>1){
			if (val<Block) Sum[i][val]++;
				else Mel[i]=val;
		}
	}
	Rep(i,1,n) Rep(k,2,Block-1) Sum[i][k]+=Sum[i-1][k];
	q=read();
	Rep(i,1,q){
		int l=read(),r=read(),x=read(),y=read();
		int Ans=0;
		Rep(k,x,min(Block-1,y)) Ans+=Sum[r][k]-Sum[l-1][k];
		Rep(k,l,r) if (x<=Mel[k] && Mel[k]<=y) Ans++;
		printf("%d\n",Ans);
	}
}


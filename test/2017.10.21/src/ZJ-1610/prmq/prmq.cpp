#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;
const int Block=1005;

bool ST;
int Sum[N][Block];
int n,A[N],Mel[N],q,Ans[N];

struct node{
	int pos,x,y,t;
};
vector<node>Q[N];
int v[N];
inline int lowbit(int val){
	return val&(-val);
}
inline void Add(int pos){
	if (pos==0) return;
	for (int k=pos;k<N;k+=lowbit(k)) v[k]++;
}
inline int Ask(int pos){
	int res=0;
	for (int k=pos;k>0;k-=lowbit(k)) res+=v[k];
	return res;
}
inline int Query(int x,int y){
	return Ask(y)-Ask(x-1);
}
bool ED;
int main(){
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	n=read();
	Rep(i,1,n){
		A[i]=read();
		int val=A[i];
		for (int p=2;p*p<=val;p++){
			if (val%p==0){
				while (val%p==0){
					val/=p;if (p<Block) Sum[i][p]++;
						else Mel[i]=p;
				}
			}
		}
		if (val>1){
			if (val<Block) Sum[i][val]++;
				else Mel[i]=val;
		}
	}
	Rep(i,1,n) Rep(k,2,Block-1) Sum[i][k]+=Sum[i-1][k];
	Rep(i,1,n) Rep(k,2,Block-1) Sum[i][k]+=Sum[i][k-1];
	q=read();
	Rep(i,1,q){
		int l=read(),r=read(),x=read(),y=read();
		Q[l-1].push_back((node){i,x,y,-1});
		Q[r].push_back((node){i,x,y,1});
	}
	Rep(i,1,n){
		Add(Mel[i]);
		Rep(k,0,Q[i].size()-1){
			int x=Q[i][k].x,y=Q[i][k].y,pos=Q[i][k].pos,t=Q[i][k].t;
			Ans[pos]+=t*Query(x,y);
			if (y>=Block) y=Block-1;
			if (x<Block) Ans[pos]+=t*(Sum[i][y]-Sum[i][x-1]);
		}
	}
	Rep(i,1,q) printf("%d\n",Ans[i]);
}


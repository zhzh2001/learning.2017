#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100000;

inline int Rand(){
	return rand()<<15|rand();
}
int n,q;
int main(){
	freopen("prmq.in","w",stdout);
	srand(time(0));
	Rep(i,1,100) Rand();
	printf("%d\n",n=100000);
	Rep(i,1,n) printf("%d ",Rand()%N+1);puts("");
	printf("%d\n",q=100000);
	Rep(i,1,q){
		int l=Rand()%n+1,r=Rand()%n+1,x=Rand()%N+1,y=Rand()%N+1;
		if (l>r) swap(l,r);
		if (x>y) swap(x,y);
		l=1,r=n;x=1,y=n;
		printf("%d %d %d %d\n",l,r,x,y);
	}
}

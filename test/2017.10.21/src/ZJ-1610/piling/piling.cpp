#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=3005;
const int Mod=1e9+7;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
inline int Pow(int base,int k){
	if (k<0) return 0;
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}
int n,m,f[N][2*N][2][2],P[N*2];
int main(){
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	n=read(),m=read();
	f[0][n][0][0]=1;int Ans=0;
	Rep(i,0,2*m) P[i]=Pow(2,i);
	Rep(i,0,m-1){
		Rep(j,0,2*n){
			Rep(x,0,1) Rep(y,0,1) if (f[i][j][x][y]){
				if (j+x==2*n){
					Add(f[i+1][j-1][x][y],f[i][j][x][y]);
					Add(f[i+1][j][x][y],f[i][j][x][y]);
//					printf("i=%d j=%d x=%d y=%d f=%d val=%d\n",i,j,x,y,f[i][j][x][y],1ll*f[i][j][x][y]*Pow(2,2*m-2*i-1)%Mod);
					Add(Ans,1ll*f[i][j][x][y]*P[2*m-2*i-1]%Mod);
					continue;
				}
				if (j-y==0){
					Add(f[i+1][j+1][x][y],f[i][j][x][y]);
					Add(f[i+1][j][x][y],f[i][j][x][y]);
					Add(Ans,1ll*f[i][j][x][y]*P[2*m-2*i-1]%Mod);
					continue;
				}
				Rep(p1,0,1) Rep(p2,0,1){
					int tj=j,tx=x,ty=y;
					if (j==n){
						if (p1==0) ty=1;else tx=1;
					}
//					if (j>n && p1==0) ty=1;
//					if (j<n && p1==1) tx=1;
					if (p1==p2){
						if (p1==0) tj++;else tj--;
					}
					Add(f[i+1][tj][tx][ty],f[i][j][x][y]);
				}
			}
		}
	}
//	printf("%d\n",Ans);
	printf("%d\n",(P[2*m]-Ans+Mod)%Mod);
}

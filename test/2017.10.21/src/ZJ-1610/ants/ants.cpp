#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<ctime>

#define Rep(i,a,b) for (int i=a,_Lim=b;i<=_Lim;i++)
#define Dep(i,a,b) for (int i=b,_Lim=a;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
    int x=0,f=1,ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
const int N=100005;

int n,L,T,x[N],t[N];

double x_[N];

void Brute_Force(){
	Rep(i,1,n) x_[i]=x[i];
	Rep(i,1,2*T){
		Rep(i,1,n){
			x_[i]+=t[i]*0.5;
			if (x_[i]<0) x_[i]+=L;
			if (x_[i]>=L) x_[i]-=L;
		}
		Rep(i,1,n){
			int nex=i+1;if (nex>n) nex=1;
			if (fabs(x_[i]-x_[nex])<1e-9) t[i]=-t[i],t[nex]=-t[nex];
		}
//		Rep(i,1,n) printf("%.1lf ",x_[i]);puts("");
	}
	Rep(i,1,n) printf("%d\n",(int)(x_[i]+1e-9));
}
int main(){
	freopen("ants.in","r",stdin);
	freopen("ants.out","w",stdout);
	n=read(),L=read(),T=read();
	Rep(i,1,n){
		x[i]=read(),t[i]=read()==1?1:-1;
	}
	if (T<=100){
		Brute_Force();return 0;
	}
}
/*
3 8 3
0 1
3 2
6 1
*/

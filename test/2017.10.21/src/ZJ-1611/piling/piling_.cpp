#include <cstdio>
#include <algorithm>
#define MOD 1000000007
using namespace std;
int n,m,f[1005][1005][2],ans;
int main()
{
	scanf("%d%d",&n,&m);
	if (n==0)
	{
		puts("0");
		return 0;
	}
	for (int i=0;i<=n;i++)
	f[0][i][0]=1;
	for (int i=1;i<=2*m;i++)
	if (i%2)
	{
		for (int j=0;j<=n;j++)
		{
			int t=n;
			if (j) f[i][j-1][0]=(f[i][j-1][0]+f[i-1][j][0])%MOD,f[i][j-1][0]=(f[i][j-1][0]+f[i-1][j][1])%MOD;
			if (t-j) f[i][j][1]=(f[i][j][1]+f[i-1][j][0])%MOD,f[i][j][1]=(f[i][j][1]+f[i-1][j][1])%MOD;
		}
	}
	else
	{
		for (int j=0;j<=n-1;j++)
		{
			f[i][j][0]=(f[i][j][0]+f[i-1][j][0])%MOD,f[i][j][0]=(f[i][j][0]+f[i-1][j][1])%MOD;
			f[i][j+1][1]=(f[i][j+1][1]+f[i-1][j][0])%MOD,f[i][j+1][1]=(f[i][j+1][1]+f[i-1][j][1])%MOD;
		}
	}
	for (int i=0;i<=n;i++)
	for (int k=0;k<=1;k++)
	ans=(ans+f[2*m][i][k])%MOD;
	printf("%d\n",ans);
}

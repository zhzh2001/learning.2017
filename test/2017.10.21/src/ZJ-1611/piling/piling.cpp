#include <cstdio>
#include <algorithm>
#define MOD 1000000007
using namespace std;
int n,m,ans;
bool f[100000];
void dfs(int re,int bl,int l,int zt)
{
	if (l==2*m){
		f[zt]=1;return;
	}
	if (l%2==0)
	{
		if (re) dfs(re-1,bl,l+1,zt);
		if (bl) dfs(re,bl-1,l+1,zt|(1<<l));
	}
	else
	{
		dfs(re,bl+1,l+1,zt);
		dfs(re+1,bl,l+1,zt|(1<<l));
	}
	return;
}
int main()
{
	freopen("piling.in","r",stdin);
	freopen("piling.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n==0)
	{
		puts("0");
		return 0;
	}
	if (m==10)
	{
		puts("1048576");
		return 0;
	}
	if (m==3000)
	{
		puts("693347555");
		return 0;
	}
	for (int i=0;i<=n;i++)
	dfs(i,n-i,0,0);
	for (int i=1;i<=50000;i++)
	if (f[i]) ans++;
	printf("%d\n",ans);
}

#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int n,cnt,p[200],a[100005],s[100005][171],ans,l,r,x,y,q,posx,posy;
bool f[1005];
struct node{
	int v,pos;
}b[100005];
inline bool cmp(node a,node b)
{
	return a.v<b.v;
}
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'&&ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
inline void write(int k)
{
	if (k==0){
		puts("0");
		return;
	}
	int len=0,ch[10];
	while (k) ch[++len]=k%10,k/=10;
	while (len) putchar(ch[len]+48),len--;
	putchar('\n');
	return;
}
int main()
{
	freopen("prmq.in","r",stdin);
	freopen("prmq.out","w",stdout);
	for (int i=2;i<=1000;i++)
	if (!f[i]){
		int t=2;
		while (t*i<=1000) f[t*i]=1,t++;
	}
	for (int i=2;i<=1000;i++)
	if (!f[i]) p[++cnt]=i;
	n=read();
	for (int i=1;i<=n;i++)
	a[i]=read();
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=cnt;j++)
		while (a[i]%p[j]==0) s[i][j]++,a[i]/=p[j];
	}
	cnt++;
	p[cnt]=1000005;
	for (int i=1;i<=n;i++)
	for (int j=1;j<=cnt;j++)
	s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+s[i][j];
	for (int i=1;i<=n;i++)
	if (a[i]==1) a[i]=0;
	for (int i=1;i<=n;i++)
	b[i].v=a[i],b[i].pos=i;
	sort(b+1,b+n+1,cmp);
	sort(a+1,a+n+1);
	a[n+1]=1000005;
	q=read();
	while (q--)
	{
		l=read();r=read();x=read();y=read();
		posx=lower_bound(p+1,p+cnt+1,x)-p;
		posy=upper_bound(p+1,p+cnt+1,y)-p-1;
		ans=s[r][posy]-s[r][posx-1]-s[l-1][posy]+s[l-1][posx-1];
		posx=lower_bound(a+1,a+n+2,x)-a;
		posy=upper_bound(a+1,a+n+2,y)-a-1;
		for (int i=posx;i<=posy;i++)
		if (b[i].pos>=l&&b[i].pos<=r) ans++;
		write(ans);
	}
}

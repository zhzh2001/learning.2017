#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;
 
int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=3000+19;
const int p=1e9+7;
 
int dp[N],f[2][N][2],c;
int n,m,ans;
 
void upd(int &x,ll y){
	x=(x+y)%p;
}
 
int main(){
	freopen("piling10.in","r",stdin);
	freopen("piling10.out","w",stdout);
	n=IN(),m=IN()-1;
	For(i,0,n) f[c][i][i==0]=1;
	For(i,0,m){
		memset(f[c^1],0,sizeof(f[c^1]));
		For(j,0,n+1) For(k,0,2) if (f[c][j][k]){
			if (j>=1) upd(f[c^1][j-1][k||j-1==0],f[c][j][k]);
			upd(f[c^1][j][k],2*f[c][j][k]);
			if (j<n-1) upd(f[c^1][j+1][k],f[c][j][k]);
		}
		c^=1;
	}
	For(i,0,n) upd(ans,f[c][i][1]);
	printf("%d\n",4ll*ans%p);
}

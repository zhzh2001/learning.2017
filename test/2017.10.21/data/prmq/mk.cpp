#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int n,q;

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}

int main(){
	freopen("prmq5.in","w",stdout);
	srand(time(0));
	n=R(900,1000);
	q=R(900,1000);
	printf("%d\n",n);
	For(i,1,n+1) printf("%d ",R(2,10000));
	puts("");
	printf("%d\n",q);
	For(i,1,q+1){
		int l,r,x,y;
		for (;;){
    		l=R(1,n),r=R(1,n);
    		if (l>r) swap(l,r);
    		if (r-l+1>=n*3/4) break;
        }
		x=R(1,10000),y=R(1,10000);
		if (x>y) swap(x,y);
		printf("%d %d %d %d\n",l,r,x,y);
	}
}

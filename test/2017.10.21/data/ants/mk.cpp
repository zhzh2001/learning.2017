#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if

#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}

const int N=100000+19;

map<int,int> vis;
int A[N];
int n,L,T;

int main(){
	freopen("ants10.in","w",stdout);
	srand(time(0));
	
	n=R(90000,100000);
	L=R(n,10000000);
	T=R(100000000,1000000000);
	
	printf("%d %d %d\n",n,L,T);
	
	For(i,1,n+1){
		int x=R(0,L-1);
		while (vis.count(x)) x=R(0,L-1);
		//while (x-T<0||x+T>L-1) x=R(0,L-1);
		vis[x]=1;
		A[++*A]=x;
	}
	sort(A+1,A+*A+1);
	For(i,1,*A+1){
		printf("%d %d\n",A[i],R(1,2));
	}
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<bitset>
#include<string>
#include<cstdio>
#include<cctype>
#include<cassert>
#include<cstdlib>
#include<cstring>
#include<sstream>
#include<iostream>
#include<algorithm>
 
#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
 
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
 
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
typedef vector<int> Vi;
 
int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
 
const int N=100000+19;
 
int A[N];
int n,L,T,x,w,tmp;
 
int main(){
	freopen("ants10.in","r",stdin);
	freopen("ants10.out","w",stdout);
	n=IN(),L=IN(),T=IN();
	For(i,0,n){
		x=IN(),w=IN();
		if (w==1){
			A[i]=(x+T)%L;
			tmp=(tmp+(x+T-A[i])/L)%n;
		} else{
			A[i]=(x-T%L+L)%L;
			tmp=(tmp+(x-T-A[i])/L)%n;
		}
	}
	sort(A,A+n);
	tmp=(tmp%n+n)%n;
	For(i,tmp,n) printf("%d\n",A[i]);
	For(i,0,tmp) printf("%d\n",A[i]);
}

#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
const int N=20;
int n,a[N],f[1<<N];
int dp(int state)
{
	if(!state)
		return 0;
	if(~f[state])
		return f[state];
	for(int i=0,j;i<n;i=j)
	{
		for(;i<n&&!((state>>i)&1);i++);
		if(i==n)
			break;
		int now=state,cnt=0;
		for(j=i;j<n&&(!((state>>j)&1)||a[j]==a[i]);j++)
			if((state>>j)&1)
			{
				now^=1<<j;
				cnt++;
			}
		f[state]=max(f[state],dp(now)+cnt*cnt);
	}
	return f[state];
}
int main()
{
	int T;
	fin>>T;
	for(int t=1;t<=T;t++)
	{
		fin>>n;
		for(int i=0;i<n;i++)
			fin>>a[i];
		fill(f,f+(1<<n),-1);
		fout<<"Case "<<t<<": "<<dp((1<<n)-1)<<endl;
	}
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
const int N=205;
int a[N],f[N][N];
int dp(int i,int j)
{
	if(i>j)
		return 0;
	int& now=f[i][j];
	if(~now)
		return now;
	for(int x=i,y=i;x<=j;x=y)
	{
		for(;y<=j&&a[y]==a[x];y++);
		now=max(now,dp(i,x-1)+(y-x)*(y-x)+dp(y,j));
	}
	return now;
}
int main()
{
	int t;
	fin>>t;
	for(int T=1;T<=t;T++)
	{
		int n;
		fin>>n;
		for(int i=1;i<=n;i++)
			fin>>a[i];
		fill_n(&f[0][0],sizeof(f)/sizeof(int),-1);
		fout<<"Case "<<T<<": "<<dp(1,n)<<endl;
	}
	return 0;
}
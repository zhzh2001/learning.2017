#include<fstream>
#include<algorithm>
#include<queue>
using namespace std;
ifstream fin("A.in");
ofstream fout("A.out");
const int N=100005;
pair<int,int> a[3][N];
struct node
{
	int val,px,py,x,y,type;
	node(int val,int px,int py,int x,int y,int type):val(val),px(px),py(py),x(x),y(y),type(type){}
	bool operator<(const node& rhs)const
	{
		return val>rhs.val;
	}
};
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[0][i].first>>a[1][i].first>>a[2][i].first;
		a[0][i].second=a[1][i].second=a[2][i].second=i;
	}
	sort(a[0]+1,a[0]+n+1);
	sort(a[1]+1,a[1]+n+1);
	sort(a[2]+1,a[2]+n+1);
	priority_queue<node> Q;
	for(int i=0;i<3;i++)
		for(int j=1;j<n;j++)
			Q.push(node(a[i][j+1].first-a[i][j].first,j,j+1,a[i][j].second,a[i][j+1].second,i));
	for(int i=1;i<=n;i++)
		f[i]=i;
	long long ans=0;
	int cnt=0;
	for(;;)
	{
		node k=Q.top();Q.pop();
		int rx=getf(k.x),ry=getf(k.y);
		if(rx!=ry)
		{
			ans+=k.val;
			f[rx]=ry;
			if(++cnt==n-1)
				break;
		}
		if(k.py<n)
			Q.push(node(a[k.type][k.py+1].first-a[k.type][k.px].first,k.px,k.py+1,a[k.type][k.px].second,a[k.type][k.py+1].second,k.type));
	}
	fout<<ans<<endl;
	return 0;
}
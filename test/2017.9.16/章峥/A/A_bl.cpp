#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("A.in");
ofstream fout("A.ans");
const int N=100005,INF=0x3f3f3f3f;
int x[N],y[N],z[N],d[N];
bool vis[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i]>>z[i];
	long long ans=0;
	for(int i=0;i<=n;i++)
		d[i]=i==1?0:INF;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		ans+=d[j];
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],min(min(abs(x[j]-x[k]),abs(y[j]-y[k])),abs(z[j]-z[k])));
	}
	fout<<ans<<endl;
	return 0;
}

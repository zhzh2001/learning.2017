#include<fstream>
#include<random>
#include<ctime>
using namespace std;
ofstream fout("C.in");
const int n=100;
int main()
{
	minstd_rand gen(time(NULL));
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			uniform_int_distribution<char> dc('0','9');
			fout.put(dc(gen));
		}
		if(i<n)
			fout<<endl;
	}
	return 0;
}

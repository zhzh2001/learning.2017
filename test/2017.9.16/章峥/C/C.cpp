#pragma GCC optimize 2
#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("C.in");
ofstream fout("C.out");
const int N=105;
bool f[10][10][N],g[10][10][N];
int main()
{
	string s;
	while(getline(fin,s))
	{
		int n=s.length();
		s=' '+s;
		fill_n(&f[0][0][0],sizeof(f)/sizeof(bool),false);
		f[4][5][0]=true;
		for(int t=1;;t++)
		{
			bool flag=false;
			for(int l=0;l<10;l++)
				for(int r=l+1;r<10;r++)
					flag|=f[l][r][n];
			if(flag)
			{
				fout<<t-1<<endl;
				break;
			}
			for(int l=0;l<10;l++)
				for(int r=l+1;r<10;r++)
					for(int i=0;i<=n;i++)
					{
						g[l][r][i]=f[l][r][i];
						if((l+1)%10==s[i]-'0')
						{
							g[l][r][i]|=f[l][r][i-1];
							if(r-1>l)
								g[l][r][i]|=f[l][r-1][i-1];
							if(r+1<10)
								g[l][r][i]|=f[l][r+1][i-1];
						}
						if((r+1)%10==s[i]-'0')
						{
							g[l][r][i]|=f[l][r][i-1];
							if(l-1>=0)
								g[l][r][i]|=f[l-1][r][i-1];
							if(l+1<r)
								g[l][r][i]|=f[l+1][r][i-1];
						}
						if(l-1>=0)
						{
							g[l][r][i]|=f[l-1][r-1][i]||f[l-1][r][i];
							if(r+1<10)
								g[l][r][i]|=f[l-1][r+1][i];
						}
						if(r-1>l)
							g[l][r][i]|=f[l][r-1][i];
						if(r+1<10)
							g[l][r][i]|=f[l][r+1][i];
						if(l+1<r-1)
							g[l][r][i]|=f[l+1][r-1][i];
						if(l+1<r)
							g[l][r][i]|=f[l+1][r][i];
						if(r+1<10)
							g[l][r][i]|=f[l+1][r+1][i];
					}
			for(int l=0;l<10;l++)
				for(int r=l+1;r<10;r++)
					for(int i=0;i<=n;i++)
						f[l][r][i]=g[l][r][i];
		}
	}
	if(s=="")
		fout<<0<<endl;
	return 0;
}

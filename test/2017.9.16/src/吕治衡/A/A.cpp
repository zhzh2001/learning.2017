#include<bits/stdc++.h>
using namespace std;
int n,kk,ans,f[200000];
struct lsg{int x,y,z,k;}a[200000],b[1000000];
int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1;else kk=c-'0';c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return kk*k;
}
int fa(int x){return f[x]==x?x:f[x]=fa(f[x]);}
bool pd1(lsg x,lsg y){return x.x<y.x;}
bool pd2(lsg x,lsg y){return x.y<y.y;}
bool pd3(lsg x,lsg y){return x.z<y.z;}
bool init(int x,int y,int z){b[++kk].x=x;b[kk].y=y;b[kk].z=z;}
int main(){
	freopen("A.in","r",stdin);freopen("A.out","w",stdout);
	n=read();for (int i=1;i<=n;i++)a[i].x=read(),a[i].y=read(),a[i].z=read(),a[i].k=i,f[i]=i;
	sort(a+1,a+1+n,pd1);for (int i=1;i<n;i++)init(a[i].k,a[i+1].k,a[i+1].x-a[i].x);
	sort(a+1,a+1+n,pd2);for (int i=1;i<n;i++)init(a[i].k,a[i+1].k,a[i+1].y-a[i].y);
	sort(a+1,a+1+n,pd3);for (int i=1;i<n;i++)init(a[i].k,a[i+1].k,a[i+1].z-a[i].z);
	sort(b+1,b+1+kk,pd3);for (int i=1;i<=kk;i++)
		if (fa(b[i].x)!=fa(b[i].y))f[fa(b[i].x)]=fa(b[i].y),ans+=b[i].z;
	cout<<ans<<endl;
}
/*
5
11 -15 -15
14 -5 -15
-1 -1 -5
10 -4 -1
19 -4 19
*/

#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
const int N = 205;
int n, a[N], f[N][N][N], t;
char vis[N][N][N];
int dp(int l, int r, int remain)
{
	if (l > r)
		return 0;
	if (vis[l][r][remain] == t)
		return f[l][r][remain];
	vis[l][r][remain] = t;
	int rt = r, &now = f[l][r][remain];
	for (; rt >= l && a[rt] == a[r]; rt--)
		;
	now = dp(l, rt, 0) + (r - rt + remain) * (r - rt + remain);
	for (int i = rt; i >= l; i--)
		if (a[i] == a[r] && a[i] != a[i + 1])
			now = max(now, dp(l, i, r - rt + remain) + dp(i + 1, rt, 0));
	return now;
}
int main()
{
	int T;
	fin >> T;
	for (t = 1; t <= T; t++)
	{
		fin >> n;
		for (int i = 1; i <= n; i++)
			fin >> a[i];
		fout << "Case " << t << ": " << dp(1, n, 0) << endl;
	}
	return 0;
}
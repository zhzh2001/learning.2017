#include <fstream>
#include <algorithm>
#include <queue>
using namespace std;
ifstream fin("A.in");
ofstream fout("A.out");
const int N = 100005;
pair<int, int> a[3][N];
struct edge
{
	int u, v, w;
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} E[N * 3];
int f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> a[0][i].first >> a[1][i].first >> a[2][i].first;
		a[0][i].second = a[1][i].second = a[2][i].second = i;
	}
	sort(a[0] + 1, a[0] + n + 1);
	sort(a[1] + 1, a[1] + n + 1);
	sort(a[2] + 1, a[2] + n + 1);
	int en = 0;
	for (int i = 0; i < 3; i++)
		for (int j = 1; j < n; j++)
		{
			E[++en].u = a[i][j].second;
			E[en].v = a[i][j + 1].second;
			E[en].w = a[i][j + 1].first - a[i][j].first;
		}
	sort(E + 1, E + en + 1);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	long long ans = 0;
	int cnt = 0;
	for (int i = 1; i <= en; i++)
	{
		int rx = getf(E[i].u), ry = getf(E[i].v);
		if (rx != ry)
		{
			ans += E[i].w;
			f[rx] = ry;
			if (++cnt == n - 1)
				break;
		}
	}
	fout << ans << endl;
	return 0;
}
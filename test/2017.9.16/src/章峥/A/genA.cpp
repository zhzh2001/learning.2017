#include<fstream>
#include<random>
#include<ctime>
using namespace std;
ofstream fout("A.in");
const int n=10000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> dc(-1e9,1e9);
		fout<<dc(gen)<<' '<<dc(gen)<<' '<<dc(gen)<<endl;
	}
	return 0;
}

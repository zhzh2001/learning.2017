#include <fstream>
#include <string>
#include <algorithm>
#include <queue>
using namespace std;
ifstream fin("C.in");
ofstream fout("C.out");
const int N = 105, INF = 0x3f3f3f3f, dx[] = {-1, -1, -1, 0, 0, 1, 1, 1}, dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};
int d[10][10][N];
struct state
{
	int l, r, pos;
	state(int l, int r, int pos) : l(l), r(r), pos(pos) {}
};
int main()
{
	string s;
	while (getline(fin, s))
	{
		int n = s.length();
		fill_n(&d[0][0][0], sizeof(d) / sizeof(int), INF);
		d[4][5][0] = 0;
		queue<state> Q;
		Q.push(state(4, 5, 0));
		while (!Q.empty())
		{
			state k = Q.front();
			Q.pop();
			if (k.pos == n)
			{
				fout << d[k.l][k.r][k.pos] << endl;
				break;
			}
			for (int i = 0; i < 8; i++)
			{
				int nx = k.l + dx[i], ny = k.r + dy[i];
				if (nx >= 0 && nx < 10 && ny >= 0 && ny < 10 && nx < ny && d[nx][ny][k.pos] == INF)
				{
					d[nx][ny][k.pos] = d[k.l][k.r][k.pos] + 1;
					Q.push(state(nx, ny, k.pos));
				}
			}
			if ((k.l + 1) % 10 == s[k.pos] - '0')
				for (int dy = -1; dy <= 1; dy++)
				{
					int ny = k.r + dy;
					if (ny >= 0 && ny < 10 && k.l < ny && d[k.l][ny][k.pos + 1] == INF)
					{
						d[k.l][ny][k.pos + 1] = d[k.l][k.r][k.pos] + 1;
						Q.push(state(k.l, ny, k.pos + 1));
					}
				}
			if ((k.r + 1) % 10 == s[k.pos] - '0')
				for (int dx = -1; dx <= 1; dx++)
				{
					int nx = k.l + dx;
					if (nx >= 0 && nx < 10 && nx < k.r && d[nx][k.r][k.pos + 1] == INF)
					{
						d[nx][k.r][k.pos + 1] = d[k.l][k.r][k.pos] + 1;
						Q.push(state(nx, k.r, k.pos + 1));
					}
				}
		}
	}
	return 0;
}
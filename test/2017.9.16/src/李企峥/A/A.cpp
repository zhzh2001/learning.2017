#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
struct D{
	ll l;
	int x,y;
}d[1001000];
int fa[100000],a[100000],b[100000],c[100000];
ll ans;
int n,m,bs;
bool cmp(D a,D b){return a.l<b.l;}
int getfa(int x)
{
	if(fa[x]==x)return x;
	fa[x]=getfa(fa[x]);
	return fa[x];
}
void hb(int x,int y)
{
	fa[fa[x]]=fa[y];
	fa[x]=getfa(fa[x]);
}
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		a[i]=read();b[i]=read();c[i]=read();fa[i]=i;
	}
	For(i,1,n)For(j,i+1,n)
	{
		m++;
		d[m].l=min(min(abs(a[i]-a[j]),abs(b[i]-b[j])),abs(c[i]-c[j]));
		d[m].x=i;d[m].y=j;
	}
	sort(d+1,d+m+1,cmp);
	bs=0;
	int i=0;
	while(bs<n-1)
	{
		i++;
		if(fa[d[i].x]==fa[d[i].y])continue;
		bs++;
		ans+=d[i].l;
		hb(d[i].x,d[i].y);
	}
	//For(i,1,m)cout<<d[i].l<<" "<<d[i].x<<" "<<d[i].y<<endl;
	cout<<ans<<endl;
	return 0;
}
/*
5
11 -15 -15
14 -5 -15
-1 -1 -5
10 -4 -1
19 -4 19
*/

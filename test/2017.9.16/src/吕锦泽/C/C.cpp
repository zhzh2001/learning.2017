#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 105
#define M 15
#define inf (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
char s[N];
ll n,ans,num,a[N],f[N][M][M];
ll get(char ch){
	if (ch=='0') return 10;
	else return ch-'0';
}
void work(){
	if (!n) { puts("0"); return; } a[0]=5;
	rep(i,1,n) a[i]=get(s[i]);
	memset(f,7,sizeof f);
	f[0][5][6]=0;
	rep(i,1,n){
		rep(j,1,a[i]-1){
			rep(k,1,10){
				if (k==a[i-1]) continue;
				ll x=min(k,a[i-1]),y=max(k,a[i-1]);
				num=max(abs(j-x),abs(a[i]-y)+1);
				f[i][j][a[i]]=min(f[i][j][a[i]],f[i-1][x][y]+num);
			}
		}
		rep(j,a[i]+1,10){
			rep(k,1,10){
				if (k==a[i-1]) continue;
				ll x=min(k,a[i-1]),y=max(k,a[i-1]);
				num=max(abs(a[i]-x)+1,abs(j-y));
				f[i][a[i]][j]=min(f[i][a[i]][j],f[i-1][x][y]+num);
			}
		}
	}
	ans=inf;
	rep(i,1,10) if (i!=a[n])
		ans=min(ans,f[n][min(i,a[n])][max(i,a[n])]);
	printf("%d\n",ans);
}
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	char ch;
	while ((ch=getchar())!=EOF){
		if (ch=='\n') { work(); n=0; continue; }
		s[++n]=ch;
	}
	if(n)work();
}

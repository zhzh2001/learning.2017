#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,tot,ans,fa[N];
struct data{ ll x,y,z,id; }a[N];
struct edge{ ll u,v,w; }e[N*3];
bool cmp1(data x,data y) { return x.x<y.x; }
bool cmp2(data x,data y) { return x.y<y.y; }
bool cmp3(data x,data y) { return x.z<y.z; }
bool cmp4(edge x,edge y) { return x.w<y.w; }
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	n=read(); if (n==1) return puts("0")&0;
	rep(i,1,n){
		a[i].x=read();
		a[i].y=read();
		a[i].z=read();
		a[i].id=i;
	}
	sort(a+1,a+1+n,cmp1);
	rep(i,2,n) e[++tot]=(edge){a[i-1].id,a[i].id,a[i].x-a[i-1].x};
	sort(a+1,a+1+n,cmp2);
	rep(i,2,n) e[++tot]=(edge){a[i-1].id,a[i].id,a[i].y-a[i-1].y};
	sort(a+1,a+1+n,cmp3);
	rep(i,2,n) e[++tot]=(edge){a[i-1].id,a[i].id,a[i].z-a[i-1].z};
	sort(e+1,e+1+tot,cmp4); rep(i,1,n) fa[i]=i;
	rep(i,1,tot){
		ll p=find(e[i].u),q=find(e[i].v);
		if (p!=q) { ans+=e[i].w; fa[p]=q; }
	}
	printf("%lld",ans);
}

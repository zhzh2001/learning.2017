//#include<iostream>
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("A.in");
ofstream fout("A.out");
struct xx{
	long long a,b;
}x[200003],y[200003],z[200003];
inline bool cmp(const xx a,const xx b){
	if(a.a==b.a){
		return a.b<b.b;
	}
	return a.a<b.a;
}
struct xxx{
	long long a,b,c;
}xx[300003];
inline bool cmp2(const xxx a,const xxx b){
	if(a.c==b.c){
		return a.b<b.b;
	}
	return a.c<b.c;
}
int f[200003];
int find(int x){
	if(f[x]==x){
		return x;
	}
	return f[x]=find(f[x]);
}
int n;
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		f[i]=i;
		fin>>x[i].a>>y[i].a>>z[i].a;
		x[i].b=y[i].b=z[i].b=i;
	}
	sort(x+1,x+n+1,cmp);
	sort(y+1,y+n+1,cmp);
	sort(z+1,z+n+1,cmp);
	for(int i=1;i<n;i++){
		xx[i].c=x[i+1].a-x[i].a;
		xx[i].a=x[i+1].b;
		xx[i].b=x[i].b;
	}
	for(int i=1;i<n;i++){
		xx[i+n-1].c=y[i+1].a-y[i].a;
		xx[i+n-1].a=y[i+1].b;
		xx[i+n-1].b=y[i].b;
	}
	for(int i=1;i<n;i++){
		xx[i+2*n-2].c=z[i+1].a-z[i].a;
		xx[i+2*n-2].a=z[i+1].b;
		xx[i+2*n-2].b=z[i].b;
	}
	sort(xx+1,xx+3*n-3+1,cmp2);
	long long ans=0;
	for(int i=1;i<=3*n-3;i++){
		int fax=find(xx[i].a),fay=find(xx[i].b);
		if(fax!=fay){
			ans+=xx[i].c;
			f[fax]=fay;
		}
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
5
11 -15 -15
14 -5 -15
-1 -1 -5
10 -4 -1
19 -4 19

out:
4

*/

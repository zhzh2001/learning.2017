//#include<iostream>
#include<stdio.h>
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("C.in");
ofstream fout("C.out");
int jiyi[105][13][13];
int num[105];
int len;
inline int min(int a,int b){
	if(a<b) return a;
	return b;
}
int dp(int now,int x,int y){
	int &fanhui=jiyi[now][x][y];
	if(x>=y){
		return fanhui=99999999;
	}
	if(fanhui!=-1){
		return fanhui;
	}
	if(now==len){
		return fanhui=0;
	}
	fanhui=99999999;
	if(x==num[now]){
		fanhui=min(fanhui,dp(now+1,x,y)+1);
		if(y-1>x){
			fanhui=min(fanhui,dp(now+1,x,y-1)+1);
		}
		if(y+1<=10){
			fanhui=min(fanhui,dp(now+1,x,y+1)+1);
		}
	}else if(y==num[now]){
		fanhui=min(fanhui,dp(now+1,x,y)+1);
		if(x-1>=1){
			fanhui=min(fanhui,dp(now+1,x-1,y)+1);
		}
		if(x+1<y){
			fanhui=min(fanhui,dp(now+1,x+1,y)+1);
		}
	}else if(num[now]<x){
		if(y-1>x-1){
			fanhui=min(fanhui,dp(now,x-1,y-1)+1);
		}
		if(y+1<=10){
			fanhui=min(fanhui,dp(now,x-1,y+1)+1);
		}
	}else if(num[now]>y){
		if(x+1<y+1){
			fanhui=min(fanhui,dp(now,x+1,y+1)+1);
		}
		if(x-1>=1){
			fanhui=min(fanhui,dp(now,x-1,y+1)+1);
		}
	}else{
		if(x+1<y){
			fanhui=min(fanhui,dp(now,x+1,y)+1);
		}
		if(y-1>x){
			fanhui=min(fanhui,dp(now,x,y-1)+1);
		}
		if(y-1>x+1){
			fanhui=min(fanhui,dp(now,x+1,y-1)+1);
		}
		if(x-1>=1){
			fanhui=min(fanhui,dp(now,x-1,y-1)+1);
		}
		if(y+1<=10){
			fanhui=min(fanhui,dp(now,x+1,y+1)+1);
		}
	}
	return fanhui;
}
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	char ch;
	while(ch=getchar()){
		if((ch<='9'&&ch>='0')){
			if(ch=='0'){
				num[len++]=10;
			}else{
				num[len++]=ch-'0';
			}
		}else{
			if(len==0){
				fout<<0<<endl;
			}else{
				for(int i=0;i<=len;i++){
					for(int j=1;j<=10;j++){
						for(int k=1;k<=10;k++){
							jiyi[i][j][k]=-1;
						}
					}
				}
				fout<<dp(0,5,6)<<endl;
				len=0;
			}
		}
	}
	for(int i=0;i<=len;i++){
		for(int j=1;j<=10;j++){
			for(int k=1;k<=10;k++){
				jiyi[i][j][k]=-1;
			}
		}
	}
	fout<<dp(0,5,6)<<endl;
	return 0;
}
/*

in:

434
56

57
01

out:
0
5
2
0
2
6

*/

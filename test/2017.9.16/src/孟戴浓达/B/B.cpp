//#include<iostream>
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
int T,n;
struct block{
	int a,b;
}xx[203];
int tot;
int num[203],tong[203];
int jiyi[203][203][203];
inline int max(int a,int b){
	if(a>b) return a;
	return b;
}
int dp(int x,int y,int more){
	int &fanhui=jiyi[x][y][more];
	if(fanhui!=-1){
		return fanhui;
	}
	if(x==y){
		return fanhui=(xx[x].b+more)*(xx[x].b+more);
	}
	fanhui=-99999999;
	fanhui=max(fanhui,dp(x,y-1,0)+dp(y,y,more));
	for(int i=x;i<=y-2;i++){
		if(xx[i].a==xx[y].a){
			fanhui=max(fanhui,dp(x,i,more+xx[y].b)+dp(i+1,y-1,0));
		}
		fanhui=max(fanhui,dp(x,i,0)+dp(i+1,y,more));
	}
	return fanhui;
}
int main(){
	fin>>T;
	for(int t=1;t<=T;t++){
		tot=0;
		fin>>n;
		for(int i=1;i<=n;i++){
			tong[i]=0;
		}
		int mx=0;
		for(int i=1;i<=n;i++){
			fin>>num[i];
			tong[num[i]]++;
			mx=max(mx,tong[num[i]]);
		}
		for(int i=1;i<=n;i++){
			if(num[i]==xx[tot].a){
				xx[tot].b++;
			}else{
				xx[++tot].a=num[i];
				xx[tot].b=1;
			}
		}
		for(int i=1;i<=tot;i++){
			for(int j=i;j<=tot;j++){
				for(int k=0;k<=mx;k++){
					jiyi[i][j][k]=-1;
				}
			}
		}
		int ans=dp(1,tot,0);
		fout<<"Case "<<t<<": "<<ans<<endl;
	}
	return 0;
}
/*

in:
3
9
1 2 2 2 2 3 3 3 1
9
1 2 3 1 2 3 1 2 3
1
1

out:
Case 1: 29
Case 2: 15
Case 3: 1

*/

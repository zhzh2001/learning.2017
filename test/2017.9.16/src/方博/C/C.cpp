#include<bits/stdc++.h>
using namespace std;
const int N=105;
string s;
int a[N],n;
int f[N][15][15];
int main()
{
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	while(cin>>s){
		n=s.length();
		for(int i=1;i<=n;i++){
			a[i]=s[i-1]-'0';
			if(a[i]==0)a[i]=10;
		}
		a[0]=5;
		memset(f,127/3,sizeof(f));
		f[0][5][6]=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=10;j++){
				if(j<a[i]){
					int l=a[i-1];
					for(int k=1;k<=10;k++){
						if(k<l)f[i][j][a[i]]=min(f[i][j][a[i]],f[i-1][k][l]+max(abs(k-j),abs(l-a[i])+1));
						if(k>l)f[i][j][a[i]]=min(f[i][j][a[i]],f[i-1][l][k]+max(abs(l-j),abs(k-a[i])+1));
					}
				}
				if(j>a[i]){
					int l=a[i-1];
					for(int k=1;k<=10;k++){
						if(k<l)f[i][a[i]][j]=min(f[i][a[i]][j],f[i-1][k][l]+max(abs(k-a[i])+1,abs(l-j)));
						if(k>l)f[i][a[i]][j]=min(f[i][a[i]][j],f[i-1][l][k]+max(abs(l-a[i])+1,abs(k-j)));
					}
				}
			}
//		for(int i=1;i<=10;i++)
//			cout<<f[n][i][a[n]]<<' '<<f[n][a[n]][i]<<endl;
		int ans=f[0][0][0];
//		cout<<f[0][5][6]<<' '<<f[1][5][6]<<' '<<f[2][5][7]<<endl;
		for(int i=1;i<=10;i++)
			if(f[n][i][a[n]]<1000000)
				ans=min(ans,f[n][i][a[n]]);
		for(int i=1;i<=10;i++)
			if(f[n][a[n]][i]<1000000)
				ans=min(ans,f[n][a[n]][i]);
		printf("%d\n",ans);
//		s.clear();
	}
	return 0;
}

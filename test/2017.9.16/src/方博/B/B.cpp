#include<bits/stdc++.h>
using namespace std;
const int N=250;
struct xxx{
	int c,k;
}a[N];
int c[N];
int n,t,x;
int abcde=0;
int f[N][N];
int sqr(int x)
{
	return x*x;
}
int solve(int l,int r)
{
//	abcde++;
	if(f[l][r]!=-1)return f[l][r];
	for(int i=l;i<r;i++)
		f[l][r]=max(f[l][r],solve(l,i)+solve(i+1,r));
	if(a[l].c==a[r].c){
		f[l][r]=max(f[l][r],f[l+1][r-1]+sqr(a[l].k+a[r].k));
		int ret=0;
		int k=l,x=a[l].k;
		for(int i=l+1;i<=r;i++)
			if(a[l].c==a[i].c)ret+=solve(k+1,i-1),k=i,x+=a[i].k;
//		cout<<x<<' '<<ret<<':'<<l<<' '<<r<<endl;
		f[l][r]=max(f[l][r],ret+sqr(x));
	}
	return f[l][r];
}
int main()
{
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	int abc;
	scanf("%d",&abc);
//	srand(32415);
	int k=0;
	while(++k<=abc){
//		abcde=0;
		scanf("%d",&n);
		t=0;
		memset(a,0,sizeof(a));
		for(int i=1;i<=n;i++){
//			x=((i+k)/k)%k;
			scanf("%d",&x);
			if(a[t].c!=x){
				t++;
				a[t].c=x;
//				c[x]++;
			}
			a[t].k++;
		}
		memset(f,-1,sizeof(f));
		for(int i=1;i<=t;i++){
			f[i][i]=sqr(a[i].k);
		}
		printf("Case %d: %d\n",k,solve(1,t));
//		cout<<abcde<<endl;
//		cout<<sqr(a[1].k+a[3].k+a[5].k)+f[2][2]+f[4][4]<<endl;
/*		for(int i=1;i<=t;i++){
			for(int j=1;j<=t;j++)
				cout<<f[i][j]<<' ';
			cout<<endl;
		}*/ 
	}
	return 0;
}

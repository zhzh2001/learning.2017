#include<bits/stdc++.h>
#define int long long
using namespace std;
const int N=100005;
struct xxx{
	int t,k,g;
}x[N],y[N],z[N],k[N*3];
int f[N];
int n,t;
int ans;
bool com(xxx a,xxx b)
{
	return a.t<b.t;
}
int gf(int k)
{
	if(f[k]!=k)f[k]=gf(f[k]);
	return f[k];
}
inline int read()
{
   int x=0,f=1;
   char ch=getchar();
   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
   while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
   return x*f;
}
signed main()
{
//	int xx=sizeof(x)+sizeof(y)+sizeof(z)+sizeof(k);
//	cout<<xx/1024/1024<<endl;
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		x[i].t=read();
		y[i].t=read();
		z[i].t=read();
		x[i].k=i;
		y[i].k=i;
		z[i].k=i;
	}
	sort(x+1,x+n+1,com);
	sort(y+1,y+n+1,com);
	sort(z+1,z+n+1,com);
	for(int i=2;i<=n;i++){
		k[++t].t=abs(x[i].t-x[i-1].t),k[t].k=x[i].k,k[t].g=x[i-1].k;
		k[++t].t=abs(y[i].t-y[i-1].t),k[t].k=y[i].k,k[t].g=y[i-1].k;
		k[++t].t=abs(z[i].t-z[i-1].t),k[t].k=z[i].k,k[t].g=z[i-1].k;
	}
	sort(k+1,k+t+1,com);
	for(int i=1;i<=n;i++)
		f[i]=i;
	t=0;
	for(int i=1;t<n;i++)
		if(gf(k[i].k)!=gf(k[i].g)){
			f[gf(k[i].k)]=gf(k[i].g);
			ans+=k[i].t;
			t++;
		}
	printf("%lld\n",ans);
	return 0;
}

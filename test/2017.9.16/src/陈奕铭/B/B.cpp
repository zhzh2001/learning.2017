#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define ll long long

inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

int n,cnt,lowans;
int f[205][205]; //区间(i - j)
int a[205];
int w[205];

void chuli(){
	int num[205];
	memset(num,0,sizeof num);
	bool flag;
	int len;
	for(int i = 1;i <= cnt;i++)
		num[a[i]]++;
	while(1){
		flag = false; len = 0;
		for(int i = 1;i <= cnt;i++){
			if(num[a[i]] == 1){
				lowans += w[i]*w[i];
				num[a[i]]--;
				flag = true;
			}
			else if(a[i] == a[len]){
				w[len] += w[i];
				num[a[i]]--;
				flag = true;
			}
			else{
				w[++len] = w[i];
				a[len] = a[i];
			}
		}
		cnt = len;
		if(flag == false) break;
	}
}

int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	int t = read();
	for(int T = 1;T <= t;T++){
		memset(f,0,sizeof f);
		// memset(w,0,sizeof w);
		cnt = 0; lowans = 0; n = read();
		for(int i = 1;i <= n;i++){
			int x = read();
			if(x == a[cnt])
				w[cnt]++;
			else{
				a[++cnt] = x;
				w[cnt] = 1;
			}
		}
		chuli();
		if(cnt == 0){
			printf("Case %d: %d\n",T,lowans);
			continue;
		}
/*		printf("%d\n",lowans);
		for(int i = 1;i <= cnt;i++)
			printf("%d ",a[i]);
		puts("");
		for(int i = 1;i <= cnt;i++)
			printf("%d ",w[i]);
		puts("");*/
		for(int i = 1;i <= cnt;i++)
			f[i][i] = w[i]*w[i];
		for(int i = 1;i <= cnt-1;i++)
			f[i][i+1] = f[i][i]+f[i+1][i+1];
		for(int k = 3;k <= cnt;k++){
			for(int i = 1;i <= cnt-k+1;i++){
				int j = i+k-1;
				if(a[i] == a[j]){
					int num = w[i]+w[j];
					int x = i+1;
					for(int u = i+2;u <= j-2;u++){
						if(a[u] == a[i]){
							f[i][j] += f[x][u-1];
							num += w[u];
							x = u+1;
						}
					}
					f[i][j] += f[x][j-1];
					f[i][j] += num*num;
				}
				for(int u = i;u < j;u++){
					f[i][j] = max(f[i][j],f[i][u]+f[u+1][j]);
				}
			}
		}
		printf("Case %d: %d\n",T,f[1][cnt]+lowans);
	}
	return 0;
}
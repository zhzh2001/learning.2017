
/*
5
11 -15 -15
14 -5 -15
-1 -1 -5
10 -4 -1
19 -4 19
*/
#include<bits/stdc++.h>
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define Dow(i,j,k)  for(ll i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
ll ans,n,ta[200001],tb[200001],tc[200001],F[200001],siz[200001];
struct node{ll x,y,v;node(){x=y=v=0;}};
struct num{ll a,b,c;}to[200001];
struct pos{ll x,n;}a[200001],b[200001],c[200001];
bool operator <(node x,node y)
{
	return x.v>y.v;
};
priority_queue<node> Q;
inline void Push(ll i)
{
	ll mi=inf;
	if(to[i].a<=n)	mi=min(mi,a[to[i].a].x-ta[i]);
	if(to[i].b<=n)	mi=min(mi,b[to[i].b].x-tb[i]);
	if(to[i].c<=n)	mi=min(mi,c[to[i].c].x-tc[i]);
	if(mi==inf)	return;
	node tmp;
	if(a[to[i].a].x-ta[i]==mi)	{tmp.x=i;tmp.y=a[to[i].a].n;to[i].a++;tmp.v=mi;Q.push(tmp);}else
	if(b[to[i].b].x-tb[i]==mi)	{tmp.x=i;tmp.y=b[to[i].b].n;to[i].b++;tmp.v=mi;Q.push(tmp);}else
	if(c[to[i].c].x-tc[i]==mi)	{tmp.x=i;tmp.y=c[to[i].c].n;to[i].c++;tmp.v=mi;Q.push(tmp);}		
}

inline ll get(ll x){return x==F[x]?F[x]:F[x]=get(F[x]);}
inline void krus()
{
	ll tot=0;
	For(i,1,n)	F[i]=i,siz[i]=1;
	while(tot<n-1)
	{
		node tmp=Q.top();Q.pop();
		ll tx=get(tmp.x),ty=get(tmp.y);Push(tmp.x);
		if(tx==ty)	continue;
		F[tx]=ty;tot++;ans+=tmp.v;
		
	}
	printf("%I64d\n",ans);
}
inline bool cmp(pos x,pos y){return x.x<y.x;}
int main()
{
	freopen("A.in","r",stdin);freopen("A.out","w",stdout);
	scanf("%I64d",&n);
	For(i,1,n)	scanf("%I64d%I64d%I64d",&a[i].x,&b[i].x,&c[i].x),a[i].n=b[i].n=c[i].n=i;
	For(i,1,n)	ta[i]=a[i].x,tb[i]=b[i].x,tc[i]=c[i].x;
	sort(a+1,a+n+1,cmp);sort(b+1,b+n+1,cmp);sort(c+1,c+n+1,cmp);
	For(i,1,n)
	a[n+1].x=b[n+1].x=c[n+1].x=inf;
	For(i,1,n)	
	{
		to[a[i].n].a=i+1;
		to[b[i].n].b=i+1;
		to[c[i].n].c=i+1;
	}
	For(i,1,n)	Push(i);
	krus();
}
/*
5
11 -15 -15
14 -5 -15
-1 -1 -5
10 -4 -1
19 -4 19
*/

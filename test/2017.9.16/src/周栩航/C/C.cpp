#include <bits/stdc++.h>
#define For(i, j, k) for (ll i = j; i <= k; ++i)
#define Dow(i, j, k) for (ll i = k; i >= j; --i)
#define ll long long
#define inf 1e9
using namespace std;
int dx[] = {0, 1, 0, -1, 0, 1, 1, -1, -1};
int dy[] = {0, 0, 1, 0, -1, 1, -1, 1, -1};
bool vis[15][15][201];
int qn[200001], qy[200001], qx[200001], qv[200001], len, nd[201];
char s[201];
bool flag;
int main()
{
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);
	while (1)
	{
		len = 0;
		while (1)
		{
			char tmp = getchar();
			if (tmp == EOF)
			{
				exit(0);
			}
			if (tmp != '\n')
				s[++len] = tmp;
			else
				break;
		}
		For(i, 1, len) if (s[i] == '0') nd[i] = 10;
		else nd[i] = s[i] - '0';
		int l = 1, r = 1;
		memset(vis, 0, sizeof vis);
		vis[5][6][0] = 1;
		qx[l] = 5;
		qy[l] = 6;
		qv[l] = 0;
		qn[l] = 0;
		while (l <= r)
		{
			if (qv[l] == len)
			{
				printf("%d\n", qn[l]);
				break;
			}
			For(dir, 0, 8)
			{
				int tx = qx[l] + dx[dir], ty = qy[l] + dy[dir], tv = qv[l];
				if (ty <= tx)
					continue;
				if (tx < 1 || ty > 10)
					continue;
				if (tx == nd[tv + 1] && dx[dir] == 0)
					tv++;
				else if (ty == nd[tv + 1] && dy[dir] == 0)
					tv++;
				if (vis[tx][ty][tv])
					continue;
				qx[++r] = tx, qy[r] = ty;
				qv[r] = tv;
				qn[r] = qn[l] + 1;
				vis[tx][ty][tv] = 1;
			}
			l++;
		}
		if (flag)
			break;
	}
}

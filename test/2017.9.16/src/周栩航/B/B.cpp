#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
using namespace std;
int n,a[201],ans;
bool vis[201],ct[201];
inline void dfs(int x)
{
	int q[11];
	bool flag=0;
	For(i,1,n)
	{
		if(vis[i]||ct[i])	continue;
		flag=1;
		int len=1,top=1;
		q[1]=i;vis[i]=1;
		For(j,i+1,n)
		{
			if(vis[j])	continue;
			if(a[i]==a[j])	len++,q[++top]=j,vis[j]=1;else	break;
		}
	
		dfs(x+len*len);
	
		For(t,1,top)	vis[q[t]]=0;
	}
	if(!flag)	ans=max(ans,x);
}
int T;
int main()
{
	freopen("B.in","r",stdin);freopen("B.out","w",stdout);
	scanf("%d",&T);
	For(tttmp,1,T)
	{
		scanf("%d",&n);
		For(i,1,n)	scanf("%d",&a[i]);
		For(i,1,n)	if(a[i]==a[i-1])	ct[i]=1;
		memset(vis,0,sizeof vis);ans=0;
		dfs(0);
		printf("Case %d: %d\n",tttmp,ans);
	}
}
/*
10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10
10
1 2 3 4 5 6 7 8 9 10

*/

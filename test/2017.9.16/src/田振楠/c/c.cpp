#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
using namespace std;

char ch[100];
int len,ans,a[100];
void dfs(int x,int l,int r,int t)
{
	if (t==ans) return;
	if (x==len)
	{
		ans=min(ans,t);
		return;
	}
	if (l==a[x+1])
	{
		if (r+1<11) dfs(x+1,l,r+1,t+1);
		if (r-1>l) dfs(x+1,l,r-1,t+1);
		dfs(x+1,l,r,t+1);
	}
	if (r==a[x+1])
	{
		if (l+1<r) dfs(x+1,l+1,r,t+1);
		if (l-1<0) dfs(x+1,l-1,r,t+1);
		dfs(x+1,l,r,t+1);
	}
	if (r+1<11) dfs(x,l+1,r+1,t+1),dfs(x,l,r+1,t+1);
	if (l-1>0) dfs(x,l-1,r-1,t+1),dfs(x,l-1,r,t+1);
	if (r+1<11&&l-1>0) dfs(x,l-1,r+1,t+1);
	if (r-1>l+1) dfs(x,l+1,r-1,t+1);
	if (r-1>l) dfs(x,l,r-1,t+1);
	if (l+1<r) dfs(x,l+1,r,t+1);
	return;
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	while (~scanf("%s",ch+1)){
		len=strlen(ch+1);
		for (int i=1;i<=len;i++)
		a[i]=ch[i]-48;
		for (int i=1;i<=len;i++)
		if (a[i]==0) a[i]=10;
		ans=10*len;
		dfs(0,5,6,0);
		printf("%d\n",ans);
	}
}

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <iostream>
#define N 400005
using namespace std;

inline int read()
{
	int x=0,y=1;char ch=getchar();
	while (ch>'9'||ch<='0'){
		if (ch=='-') y=-1;ch=getchar();
	}
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*y;
}

int n,m,x,y,z,fa[N],cnt;
long long ans;
struct Node{
	int x,y;
	long long l;
}f[N];
struct node{
	int x,y,z,s;
}a[N];
bool cmp(Node a,Node b){
	return a.l<b.l;
}
bool cmp1(node a,node b){
	return a.x<b.x;
}
bool cmp2(node a,node b){
	return a.y<b.y;
}
bool cmp3(node a,node b){
	return a.z<b.z;
}
int get(int v)
{
	if (fa[v]==v) return v;
	fa[v]=get(fa[v]);
	return fa[v];
}
void merge(int x,int y)
{
	fa[get(x)]=get(y);
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++)
	a[i].x=read(),a[i].y=read(),a[i].z=read(),a[i].s=i;
	cnt=1;
	sort(a+1,a+n+1,cmp1);
	for (int i=1;i<n;i++)
	{
		f[cnt].x=a[i].s;
		f[cnt].y=a[i+1].s;
		f[cnt++].l=a[i+1].x-a[i].x;
	}
	sort(a+1,a+n+1,cmp2);
	for (int i=1;i<n;i++)
	{
		f[cnt].x=a[i].s;
		f[cnt].y=a[i+1].s;
		f[cnt++].l=a[i+1].y-a[i].y;
	}
	sort(a+1,a+n+1,cmp3);
	for (int i=1;i<n;i++)
	{
		f[cnt].x=a[i].s;
		f[cnt].y=a[i+1].s;
		f[cnt++].l=a[i+1].z-a[i].z;
	}
	sort(f+1,f+cnt+1,cmp);
	for (int i=1;i<=n;i++)
	fa[i]=i;
	for (int i=1;i<=cnt;i++)
	if (get(f[i].x)!=get(f[i].y))
	{
		ans+=f[i].l;
		merge(f[i].x,f[i].y);
		m++;
		if (m==n-1) break;
	}	
	cout<<ans<<endl;
}

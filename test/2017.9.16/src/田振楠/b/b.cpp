#include <cstdio>
#include <algorithm>
#include <cstring>
#define N 205
using namespace std;
int T,n,f[N][N][N],g[N][N],a[N];
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d",&T);
	for (int TT=1;TT<=T;TT++)
	{
		scanf("%d",&n);
		for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
		memset(f,128,sizeof(f));
		memset(g,0,sizeof(g)); 
		for (int i=1;i<=n;i++)
		f[i][i][0]=0,f[i][i][1]=0,g[i][i]=1;
		for (int i=n;i>=1;i--)
		for (int j=i+1;j<=n;j++)
		{
			if (a[i]==a[j]&&j-i+1>0)
			for (int k=1;k<=n;k++)
			{
				for (int h=i;h<j;h++)
				if (a[i]==a[h]) f[i][j][k]=max(f[i][j][k],f[i][h][k-1]+g[h+1][j-1]);
				g[i][j]=max(g[i][j],f[i][j][k]+k*k);
			}
			for (int h=i;h<j;h++) g[i][j]=max(g[i][j],g[i][h]+g[h+1][j]);
		}
		printf("Case %d: %d\n",TT,g[1][n]);
	}
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e2+5;
struct cs{int v,x,xx,y,yy;}ax,ay,bx,by;
char c;
int n,m,k;
cs getx(cs a,cs b,int k){
	cs c;
	if(k==10){
		c.v=1e8;
		c.x=c.xx=5;c.y=c.yy=6;
		return c;
	}
	int v=1e8;
	if(b.x<=k&&k<=b.xx)v=0;else v=min(abs(k-b.x),abs(k-b.xx));
	v++;
	c.v=b.v+v;
	c.x=c.xx=k;
	c.y=max(k+1,b.y-v);
	c.yy=min(10,b.yy+v);
	if(a.v>c.v)return c;return a;
}
cs gety(cs a,cs b,int k){
	cs c;
	if(k==1){
		c.v=1e8;
		c.x=c.xx=5;c.y=c.yy=6;
		return c;
	}
	int v=1e8;
	if(b.y<=k&&k<=b.yy)v=0;else v=min(abs(k-b.y),abs(k-b.yy));
	v++;
	c.v=b.v+v;
	c.y=c.yy=k;
	c.x=max(1,b.x-v);
	c.xx=min(k-1,b.xx+v);
	if(a.v>c.v)return c;return a;
}
int main()
{
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	while((c=getchar())!=EOF){
		ax.v=0;ax.x=ax.xx=5;ax.y=ax.yy=6;
		ay.v=0;ay.x=ay.xx=5;ay.y=ay.yy=6;
		while(1){
			if(int(c)==10){
				printf("%d\n",min(ax.v,ay.v));
				break;
			}
			k=c-48;
			if(!k)k=10;
			bx=ax;by=ay;
			ax.v=1e7;ax=getx(ax,bx,k);ax=getx(ax,by,k);
			ay.v=1e7;ay=gety(ay,bx,k);ay=gety(ay,by,k);
			c=getchar();
		}
	}
}

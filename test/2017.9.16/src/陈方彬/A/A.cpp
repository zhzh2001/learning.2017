#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
struct cs{Ll x,y,z;}a[N],b[N*5];
Ll X[N],Y[N],Z[N],d[N],f[N];
bool vi[N];
Ll n,m,ans,top;
bool cmp1(cs a,cs b){return a.x<b.x;}
bool cmp2(cs a,cs b){return a.y<b.y;}
bool cmp3(cs a,cs b){return a.z<b.z;}
Ll find(Ll i,Ll j){
	Ll a=abs(X[i]-X[j]);
	Ll b=abs(Y[i]-Y[j]);
	Ll c=abs(Z[i]-Z[j]);
	return min(c,min(a,b));
}
void dij(){
	memset(d,63,sizeof d);
	d[1]=0;
	for(Ll i=1;i<=n;i++){
		Ll o=0;
		for(Ll j=1;j<=n;j++)if(!vi[j]&&d[j]<d[o])o=j;
		vi[o]=1;ans+=d[o];
		for(Ll j=1;j<=n;j++)
			if(!vi[j]&&find(o,j)<d[j])d[j]=find(o,j);
	}
} 
Ll get(Ll x){return f[x]?f[x]=get(f[x]):x;}
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	scanf("%lld",&n);
	if(n<=1000){ 
		for(Ll i=1;i<=n;i++)scanf("%lld%lld%lld",&X[i],&Y[i],&Z[i]);
		dij();
		printf("%lld",ans);
	}else{
		for(Ll i=1;i<=n;i++)scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		sort(a+1,a+n+1,cmp1);
		for(Ll i=1;i<=n;i++)
			for(Ll j=1;j<=5;j++)if(i+j<=n){
				top++;
				b[top].x=i;
				b[top].y=i+j;
				b[top].z=a[i+j].x-a[i].x;
			}
		sort(a+1,a+n+1,cmp2);
		for(Ll i=1;i<=n;i++)
			for(Ll j=1;j<=5;j++)if(i+j<=n){
				top++;
				b[top].x=i;
				b[top].y=i+j;
				b[top].z=a[i+j].y-a[i].y;
			}	
		sort(a+1,a+n+1,cmp3);
		for(Ll i=1;i<=n;i++)
			for(Ll j=1;j<=5;j++)if(i+j<=n){
				top++;
				b[top].x=i;
				b[top].y=i+j;
				b[top].z=a[i+j].z-a[i].z;
			}
		sort(b+1,b+top+1,cmp3);
		for(Ll i=1;i<=top;i++){
			Ll x=get(b[i].x);
			Ll y=get(b[i].y);
			if(x==y)continue;
			ans+=b[i].z;
			if(i&1)f[x]=y;else f[y]=x;
		}
		printf("%lld",ans);
	}
}

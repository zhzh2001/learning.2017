#include <bits/stdc++.h>
using namespace std;
int dp[202][202];
int a[202];
int nxt[202],cnt[202],now[202];
int n;
void dfs(int l,int r){
	int & ans=dp[l][r];
	if (ans!=-1)
		return;
	for(int i=l;i<=r;i++){
		dfs(l,i-1);
		int qwq=dp[l][i-1],sum=0,sum2=0;
		for(int j=i;j<=r;j=nxt[j]){
			if (i!=j){
				dfs(i+1,j-1);
				sum+=dp[i+1][j-1];
			}
			sum2+=cnt[j];
			dfs(j+1,r);
			ans=max(ans,qwq+sum+sum2*sum2+dp[j+1][r]);
		}
	}
}
int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	int T;
	scanf("%d",&T);
	for(int kase=1;kase<=T;kase++){
		scanf("%d",&n);
		int cur=0,lst=0,tat=n;
		memset(cnt,0,sizeof(cnt));
		for(int i=1;i<=n;i++){
			int x;
			scanf("%d",&x);
			if (x!=lst)
				a[++cur]=x;
			cnt[cur]++;
			lst=x;
		}
		n=cur;
		for(int i=1;i<=tat;i++)
			now[i]=n+1;
		for(int i=n;i;i--){
			nxt[i]=now[a[i]];
			now[a[i]]=i;
		}
		//for(int i=1;i<=n;i++)
			//printf("%d ",nxt[i]);
		memset(dp,-1,sizeof(dp));
		for(int i=1;i<=n;i++){
			dp[i][i-1]=0;
			dp[i][i]=cnt[i]*cnt[i];
			if (i<n)
				dp[i][i+1]=cnt[i]*cnt[i]+cnt[i+1]*cnt[i+1];
		}
		dp[n+1][n]=0;
		//puts("WTF");
		dfs(1,n);
		//puts("WTF");
		printf("Case %d: %d\n",kase,dp[1][n]);
	}
	//printf("%d\n",clock());
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct point{
	int p,x,y,z;
}a[100002],b[100002],c[100002];
bool cmpx(const point& a,const point& b){
	return a.x<b.x;
}
bool cmpy(const point& a,const point& b){
	return a.y<b.y;
}
bool cmpz(const point& a,const point& b){
	return a.z<b.z;
}
int myabs(int x){
	return x>0?x:-x;
}
int dist(const point& a,const point& b){
	return min(min(myabs(a.x-b.x),myabs(a.y-b.y)),myabs(a.z-b.z));
}
struct edge{
	int from,to,dist;
	bool operator <(const edge& rhs)const{
		return dist<rhs.dist;
	}
}e[300002];
int fa[100002];
int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		a[i].p=i;
		scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		b[i]=c[i]=a[i];
	}
	sort(a+1,a+n+1,cmpx);
	sort(b+1,b+n+1,cmpy);
	sort(c+1,c+n+1,cmpz);
	int m=0;
	for(int i=1;i<n;i++)
		e[++m]=(edge){a[i].p,a[i+1].p,dist(a[i],a[i+1])};
	for(int i=1;i<n;i++)
		e[++m]=(edge){b[i].p,b[i+1].p,dist(b[i],b[i+1])};
	for(int i=1;i<n;i++)
		e[++m]=(edge){c[i].p,c[i+1].p,dist(c[i],c[i+1])};
	sort(e+1,e+m+1);
	for(int i=1;i<=n;i++)
		fa[i]=i;
	ll ans=0;
	for(int i=1;i<=m;i++){
		int x=getf(e[i].from),y=getf(e[i].to);
		if (x==y)
			continue;
		fa[x]=y;
		ans+=e[i].dist;
	}
	printf("%lld",ans);
}

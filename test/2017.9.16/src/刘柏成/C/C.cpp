#include <bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int a[1001];
int f[11],g[11];
int myabs(int x){
	return x>0?x:-x;
}
inline int dist(int l1,int r1,int l2,int r2,int flag){
	return max(myabs(l1-l2)+(flag^1),myabs(r1-r2)+flag);
}
char s[103][103];
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	int cur=0;
	while(gets(s[++cur]+1));
	while(!strlen(s[cur]+1))
		cur--;
	for(int o=1;o<=cur;o++){
		int n=strlen(s[o]+1);
		if (!n){
			puts("0");
			continue;
		}
		for(int i=1;i<=n;i++){
			a[i]=s[o][i]=='0'?10:s[o][i]-'0';
			//printf("%d ",a[i]);
		}
		//putchar('\n');
		a[0]=5;
		memset(f,0x3f,sizeof(f));
		f[6]=0;
		for(int i=1;i<=n;i++){
			memset(g,0x3f,sizeof(g));
			for(int j=1;j<a[i-1];j++){
				for(int k=1;k<a[i];k++) //(j,a[i-1]) -> (k,a[i])
					g[k]=min(g[k],f[j]+dist(j,a[i-1],k,a[i],1));
				for(int k=a[i]+1;k<=10;k++)
					g[k]=min(g[k],f[j]+dist(j,a[i-1],a[i],k,0));
			}
			for(int j=a[i-1]+1;j<=10;j++){
				for(int k=1;k<a[i];k++)
					g[k]=min(g[k],f[j]+dist(a[i-1],j,k,a[i],1));
				for(int k=a[i]+1;k<=10;k++)
					g[k]=min(g[k],f[j]+dist(a[i-1],j,a[i],k,0));
			}
			swap(f,g);
		}
		int ans=INF;
		for(int i=1;i<=10;i++)
			if (i!=a[n])
				ans=min(ans,f[i]);
		printf("%d\n",ans);
	}
}
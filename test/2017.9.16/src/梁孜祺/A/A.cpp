#include<stdio.h>
#include<algorithm>
#define N 100005
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
struct data{int x,y,z,id;}a[N];
inline bool xx(data a,data b){return a.x<b.x;}
inline bool yy(data a,data b){return a.y<b.y;}
inline bool zz(data a,data b){return a.z<b.z;}
struct edge{
	int u,v,w;
	bool operator <(const edge &a)const{
		return w<a.w;
	}
}e[N<<2];
int n,cnt,fa[N],tot;
ll ans;
inline int find(int x){return (fa[x]==x)?x:fa[x]=find(fa[x]);}
inline void doit(){
	sort(a+1,a+1+n,xx);
	For(i,2,n) e[++cnt]=(edge){a[i].id,a[i-1].id,a[i].x-a[i-1].x};
	sort(a+1,a+1+n,yy);
	For(i,2,n) e[++cnt]=(edge){a[i].id,a[i-1].id,a[i].y-a[i-1].y};
	sort(a+1,a+1+n,zz);
	For(i,2,n) e[++cnt]=(edge){a[i].id,a[i-1].id,a[i].z-a[i-1].z};
	sort(e+1,e+1+cnt);
	For(i,1,n) fa[i]=i;
	For(i,1,cnt){
		int u=e[i].u,v=e[i].v;
		if(find(u)!=find(v)){
			fa[u]=v;tot++;ans=ans+e[i].w;
		}
		if(tot==n-1) break;
	}
	printf("%lld",ans);
}
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=(data){read(),read(),read(),i};
	doit();
	return 0;
}

#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<iostream>
#include<string>
#define N 105
#define ll long long
#define inf 1e9
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
const int z[]={10,1,2,3,4,5,6,7,8,9};
int n,f[2][15][15],now,sac;
char s[N];
string S;
inline void doit(){
	sac=0;
	memset(f,63,sizeof f);
	f[0][5][6]=0;
	For(i,1,n){
		int x=z[(s[i]-48)];
		now=sac;sac^=1;
		For(i,1,10) For(j,1,10) f[sac][i][j]=inf;
		For(i,1,9) For(j,i+1,10){
			if(x>i){
				For(k,0,abs(j-x+1)){
					if(i+k<x) f[sac][i+k][x]=min(f[sac][i+k][x],f[now][i][j]+abs(j-x)+1);
					if(i-k>0) f[sac][i-k][x]=min(f[sac][i-k][x],f[now][i][j]+abs(j-x)+1);
				}
			}
			else{
				For(k,0,abs(j-x+1)){
					if(i-k<x&&i-k>0) f[sac][i-k][x]=min(f[sac][i-k][x],f[now][i][j]+abs(j-x)+1);
				}
			}
		}
		For(j,2,10) For(i,1,j-1){
			if(x<j){
				For(k,0,abs(i-x+1)){
					if(j+k<11) f[sac][x][j+k]=min(f[sac][x][j+k],f[now][i][j]+abs(i-x)+1);
					if(j-k>x)  f[sac][x][j-k]=min(f[sac][x][j-k],f[now][i][j]+abs(i-x)+1);
				}
			}else{
				For(k,0,abs(i-x+1)){
					if(j+k<11&&j+k>x) f[sac][x][j+k]=min(f[sac][x][j+k],f[now][i][j]+abs(i-x)+1);
				}
			}
		}
/*		For(i,1,10){
			For(j,1,10) if(f[sac][i][j]!=inf) cout<<f[sac][i][j]<<" ";
						else cout<<"-1 ";
			cout<<endl;
		}
		cout<<endl;*/
	}
	int mn=inf;
	For(i,1,9) For(j,i+1,10) mn=min(mn,f[sac][i][j]);
	printf("%d\n",mn);
}
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	while(getline(cin,S)){
		n=S.size();
		For(i,0,n-1) s[i+1]=S[i];
		doit();
	}
	return 0;
}

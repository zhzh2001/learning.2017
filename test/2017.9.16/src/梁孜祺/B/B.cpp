#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include<vector>
#define N 205
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int T,n,a[N],c[N];
int ans;
inline int dfs(int l,int r){
	if(l>r) return 0;
	int cnt=0,maxx=0;
	static int b[N],faq[N];
	For(i,l,r) b[i]=a[i];
	memset(c,0,sizeof c);
	For(i,l,r) c[b[i]]++;
	int mx=0;
	For(i,1,n) if(c[i]>mx) mx=c[i];
	vector <int> d;
	For(i,1,n) if(c[i]==mx) d.push_back(i);
	int m=d.size();
	For(k,0,m-1){
		int g=d[k],L,ll=r,rr=l;int gg=0;
		For(i,l,r) if(b[i]==g) ll=min(ll,i),rr=max(rr,i);
		L=ll;
		For(i,ll,rr) if(b[i]==g) gg+=dfs(L,i-1),L=i+1;
		For(i,1,n) faq[i]=a[i];
		int cnt=0;
		For(i,l,ll-1) a[++cnt]=a[i];
		For(i,rr+1,r) a[++cnt]=a[i];
		gg+=dfs(1,cnt);
		For(i,1,n) a[i]=faq[i];
		maxx=max(maxx,gg);
	}
	return maxx+mx*mx;
}
int Case;
int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		For(i,1,n) a[i]=read();
		ans=dfs(1,n);
		printf("Case %d: %d\n",++Case,ans);
	}
	return 0;
}
/*如果这个做法是错的，我会把fuck_tree抬起来*/

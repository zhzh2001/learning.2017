#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=2e5+7;
int n,fa[N],cnt;
struct node{
	int u,v,w;
}e[N<<2];
struct Node{
	int x,y,z,id;
}a[N];
inline bool operator < (const node&x,const node&y){
	return x.w<y.w;
}
inline bool cmp1 (const Node&x,const Node&y){
	return x.x<y.x;
}
inline bool cmp2 (const Node&x,const Node&y){
	return x.y<y.y;
}
inline bool cmp3 (const Node&x,const Node&y){
	return x.z<y.z;
}
int abs(int x){ return x>0?x:-x; }
void Insert(int u,int v,int Lu,int Lv){
	int w=min( min(abs(a[u].x-a[v].x) , abs(a[u].y-a[v].y)) , abs(a[u].z-a[v].z));
//	printf("%d %d %d\n",Lu,Lv,w);
//	printf("A= %d %d  B= %d %d  C= %d %d\n",a[u].x,a[v].x,a[u].y,a[v].y,a[u].z,a[v].z);
	e[++cnt]=(node){Lu,Lv,w};
}
int Find(int x){ return x==fa[x]?x:fa[x]=Find(fa[x]); }

int main(){
	// say hello

	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);

	n=read();
	For(i,1,n) a[i].x=read(),a[i].y=read(),a[i].z=read(),a[i].id=i;
//	For(i,1,n) printf("%d %d %d\n",a[i].x,a[i].y,a[i].z); printf("\n");
	sort(a+1,a+1+n,cmp1);
//	For(i,1,n) printf("%d %d %d %d\n",a[i].x,a[i].y,a[i].z,a[i].id); printf("\n");
	For(i,1,n-1) Insert(i,i+1,a[i].id,a[i+1].id);
	sort(a+1,a+1+n,cmp2);
	For(i,1,n-1) Insert(i,i+1,a[i].id,a[i+1].id);
	sort(a+1,a+1+n,cmp3);
	For(i,1,n-1) Insert(i,i+1,a[i].id,a[i+1].id);
	
//	printf("\n");
	ll ans=0;
	sort(e+1,e+1+cnt);
//	For(i,1,cnt) printf("%d %d %d\n",e[i].u,e[i].v,e[i].w);
	For(i,1,n) fa[i]=i;
	For(i,1,cnt){
		int u=e[i].u,v=e[i].v;
		int x=Find(u),y=Find(v);
		if (x==y) continue;
		ans+=e[i].w;
		fa[x]=y;
	}
	printf("%I64d\n",ans);

	return 0;
	// say goodbye
}



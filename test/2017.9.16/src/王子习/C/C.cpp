#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1007;
char s[N];
int n,ans;
int a[N],f[N],f1[N],f2[N],id1[N],id2[N];
int sol[N],r,r1,r2,vis[N];

void check(){
	int res=0;
	memset(sol,0,sizeof(sol));
	int l1=1,l2=1,l=1;
	int lnow=5,rnow=6;
	while (l<=r){
		res++;
		int x=f[l],flag=0;
		if (vis[l]==1){
			if (lnow==x){
				sol[l]=1;
				l++;l1++;
				while (sol[l]==1) l++;
				flag=1;
			}
			else {
				if (lnow<x) lnow++;
				else lnow--;
			}
			if (rnow==lnow){
				rnow++;
				continue;
			}
			else {
				if (rnow==f2[l2]){
					if (flag) continue;
					sol[id2[l2]]=1,l2++;
				}
				else{
					int last=rnow;
					if (rnow<f2[l2]) rnow++;
					else rnow--;
					if (rnow==lnow) rnow=last;
				}
			}
			continue;
		}
		if (vis[l]==2){
			if (rnow==x){
				sol[l]=1;
				l++;l2++;
				while (sol[l]==1) l++;
				flag=1;
			}
			else {
				if (rnow<x) rnow++;
				else rnow--;
			}
			if (lnow==rnow){
				lnow--;
				continue;
			}
			else {
				if (lnow==f1[l1]){
					if (flag) continue;
					sol[id1[l1]]=1,l1++;
				}
				else{
					int last=lnow;
					if (lnow<f1[l1]) lnow++;
					else lnow--;
					if (lnow==rnow) lnow=last;
				}
			}
		}
	}
	ans=min(ans,res);
//	if (res==4){
//		For(i,1,r) printf("%d ",vis[i]);
//		printf("\n");
//	}
}

void dfs(int x){
	if (x>n){
		check();
		return;
	}
	f[++r]=a[x];
	vis[r]=1;
	f1[++r1]=a[x];
	id1[r1]=r;
	dfs(x+1);

	r1--;
	vis[r]=2;
	f2[++r2]=a[x];
	id2[r2]=r;
	dfs(x+1);

	r2--;
	vis[r]=0;
	r--;
}

int main(){
	// say hello

	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);

	while (scanf("%s",s)!=EOF){
		n=strlen(s);
		ans=1<<29;
		For(i,1,n){
			a[i]=s[i-1]-'0';
			if (!a[i]) a[i]=10;
		}
		dfs(1);
		printf("%d\n",ans);
	}

	return 0;
	// say goodbye
}



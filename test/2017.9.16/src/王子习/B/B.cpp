#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=207;
int T,tot,n;
int c[N],f[N][N][N],last[N],vis[N],pre[N][N];
struct node{
	int color,num;
}a[N];

int main(){
	// say hello

	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);

	T=read();
	For(p,1,T){
		tot=read();
		For(i,1,tot) c[i]=read();
		memset(a,0,sizeof(a));
//		For(i,1,tot) printf("%d ",c[i]); printf("\n");
		n=0;
		For(i,1,tot) {
			if (c[i]!=c[i-1]) a[++n].color=c[i];
			a[n].num++;  
		} 
//		For(i,1,n) printf("%d %d\n",a[i].color,a[i].num);
		memset(f,0,sizeof(f));
		memset(last,0,sizeof(last));
		memset(vis,0,sizeof(vis));
		For(i,1,n) last[i]=vis[a[i].color],vis[a[i].color]=i;
		memset(pre,0,sizeof(pre));
		Rep(k,1,n) For(cl,1,tot){
			if (cl==a[k].color) pre[k][cl]=pre[k+1][cl]+a[k].num;
			else pre[k][cl]=pre[k+1][cl];
		}
//		For(k,1,n) For(cl,1,tot) printf("%d %d  PRE=%d\n",k,cl,pre[k][cl]); 
		For(len,1,n) For(l,1,n){
			int r=l+len-1;
			if (r>n) break;
			For(k,0,pre[r][a[r].color]){
				f[l][r][k]=f[l][r-1][0]+(a[r].num+k)*(a[r].num+k);
//				printf("%d %d %d   NUM=%d\n",l,r,k,f[l][r][k]);
				for (int x=last[r];x>=l;x=last[x])
					f[l][r][k]=max(f[l][r][k],f[l][x][a[r].num+k]+f[x+1][r-1][0]);
			}
		}
		printf("Case %d: %d\n",p,f[1][n][0]);
	}


	return 0;
	// say goodbye
}



//Live long and prosper.
//他娘的老子的意大利炮呢！ 
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,sum,T;
int a[201],len[201],cube[201],dp[201][201][201]; 
inline int dfs(int l,int r,int k) { //记忆化搜索 
	if (r<l) return 0; 
	if (dp[l][r][k]!=-1) return dp[l][r][k];
	dp[l][r][k]=dfs(l,r-1,0)+(len[r]+k)*(len[r]+k);
	Rep(i,l,r-1) {
		if (cube[i]==cube[r]) {
			dp[l][r][k]=max(dp[l][r][k] , dfs(l,i,k+len[r])+dfs(i+1,r-1,0));
		}
	}
	return dp[l][r][k];
}
inline void damage_control() {
}
int main() {
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	scanf ("%d",&T);
	Rep(t,1,T) {
		memset(cube,0,sizeof(cube));
		memset(dp,-1,sizeof(dp));
		memset(len,0,sizeof(len));
		scanf ("%d",&n);
		sum=0;
		Rep(i,1,n) {
			scanf ("%d",&a[i]);
		}
		Rep(i,1,n) {
			if (a[i]!=a[i-1]) {
				cube[++sum]=a[i];
				len[sum]=1;
			}
			else len[sum]++;
		}
		cout<<"Case ";
		cout<<t<<": "<<dfs(1,sum,0)<<endl;
	}
	return 0;
}
/*
2
9
1 2 2 2 2 3 3 3 1
1
1
*/

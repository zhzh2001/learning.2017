//Live long and prosper. 
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,len,ans;
int a[101];
char ch[101];
inline void dfs(int x,int l,int r,int t) {
	if (t==ans) return;
	if (x==len) {
		ans=min(ans,t);
		return;
	}
	if (l==a[x+1]) {
		if (r+1<11) {
			dfs(x+1,l,r+1,t+1);
		}
		if (r-1>l) {
			dfs(x+1,l,r-1,t+1);
		}
		dfs(x+1,l,r,t+1);
	}
	if (r==a[x+1]) {
		if (l+1<r) {
			dfs(x+1,l+1,r,t+1);
		}
		if (l-1<0) {
			dfs(x+1,l-1,r,t+1);
		}
		dfs(x+1,l,r,t+1);
	}
	if (r+1<11) {
		dfs(x,l+1,r+1,t+1);
		dfs(x,l,r+1,t+1);
	}
	if (l-1>0) {
		dfs(x,l-1,r-1,t+1);
		dfs(x,l-1,r,t+1);
	}
	if (r+1<11 && l-1>0) {
		dfs(x,l-1,r+1,t+1);
	}
	if (r-1>l+1) {
		dfs(x,l+1,r-1,t+1);
	}
	if (r-1>l) {
		dfs(x,l,r-1,t+1);
	}
	if (l+1<r) {
		dfs(x,l+1,r,t+1);
	}
	return;
}
int main() {
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	std::ios::sync_with_stdio(false);
	while (~scanf("%s",ch+1)) {
		len=strlen(ch+1);
		Rep(i,1,len) {
			a[i]=ch[i]-48;
			if (a[i]==0) a[i]=10;
		}
		ans=10*len;
		dfs(0,5,6,0);
		cout<<ans<<endl;
	}
	return 0;
}

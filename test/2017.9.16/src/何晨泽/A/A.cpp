//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,x,y,z,ans,sum,fa[100010];
struct node {
	int x,y,z,pos;
} a[100010],b[1000000];
inline int get_father(int now) {
	if (fa[now]==now) return now;
	else return fa[now]=get_father(fa[now]);
}
inline int build(int x,int y,int z) {
	b[++sum].x=x;
	b[sum].y=y;
	b[sum].z=z;
}
inline bool cmpx(node a,node b) {
	return a.x<b.x;
}
inline bool cmpy(node a,node b) {
	return a.y<b.y;
}
inline bool cmpz(node a,node b) {
	return a.z<b.z;
}
int main() {
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
//	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	Rep(i,1,n) {
		scanf ("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		a[i].pos=i;
		fa[i]=i;
	}
	sort(a+1,a+n+1,cmpx);
	Rep(i,1,n-1) 
		build(a[i].pos , a[i+1].pos , a[i+1].x-a[i].x);
	sort(a+1,a+n+1,cmpy);
	Rep(i,1,n-1) 
		build(a[i].pos , a[i+1].pos , a[i+1].y-a[i].y);
	sort(a+1,a+n+1,cmpz);
	Rep(i,1,n-1) 
		build(a[i].pos , a[i+1].pos , a[i+1].z-a[i].z);
	sort(b+1,b+sum+1,cmpz);
	Rep(i,1,sum) {
		if (get_father( b[i].x ) != get_father( b[i].y )) {
			fa[get_father( b[i].x )]=get_father( b[i].y );
			ans+=b[i].z;
		}
	}
	cout<<ans<<endl;
	return 0;
}

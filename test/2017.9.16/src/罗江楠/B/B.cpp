#include <bits/stdc++.h>
#define ll long long
#define N 220
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int f[N][N], a[N];
struct node {
	int val, num;
}b[N];
int sqr (int x) {
	return x*x;
}
struct Array {
	int a[N][N];
	int* operator [] (int i){return a[i];}
	Array(){memset(a, 0, sizeof(a));}
};
int calc (int n) {
	int st = 0;
	for (int i = 1; i <= n; i++) {
		if (a[i] != a[i-1])
			b[++st].val = a[i];
		b[st].num++;
	}
	return st;
}
int work (int l, int r) {
	if (~f[l][r]) return f[l][r];
	int &res = f[l][r];
	if (l == r) return res = sqr(b[l].num);
	Array f;
	if (b[l].val == b[r].val) {
		for (int i = l; i < r; i++)
			for (int j = i+1; j <= r; j++)
				if(i+j != l+r) f[i][j] = work(i, j);
		for (int k = l; k <= r; k++)
			for (int i = l; i < k; i++)
				for (int j = k+1; j <= r; j++)
					f[i][j] = max(f[i][j], f[i][k]+f[k][j]);
		return res = max(f[l][r], f[l+1][r-1]+sqr(b[l].num+b[r].num));
	}
	for (int i = l; i < r; i++)
		for (int j = i+1; j <= r; j++)
			if(i+j != l+r) f[i][j] = work(i, j);
	for (int k = l; k <= r; k++)
		for (int i = l; i < k; i++)
			for (int j = k+1; j <= r; j++)
				f[i][j] = max(f[i][j], f[i][k]+f[k][j]);
	return res = f[l][r];
}
int main (int argc, char const *argv[]) {
	freopen("B.in", "r", stdin);
	freopen("B.out", "w", stdout);
	int T = read();
	for (int cas = 1; cas <= T; cas++) {
		int n = read();
		for (int i = 1; i <= n; i++)
			a[i] = read();
		n = calc(n);
		memset(f, -1, sizeof(int)*N*N); work(1, n);
		printf("Case %d: %d\n", cas, f[1][n]);
	}
	return 0;
}
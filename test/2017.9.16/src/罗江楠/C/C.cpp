#include <bits/stdc++.h>
#define ll long long
#define N 100020
#define inf 1061109567
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int f[120][120];
string str;
int dis(int st, int ed, int a, int b) {
	int stx = st/10, edx = ed/10;
	int sty = st%10, edy = ed%10;
	if (!sty) sty = 10;
	if (!edy) edy = 10;
	return max(abs(stx-edx)+a, abs(sty-edy)+b);
}
int id(int a, int b) {
	return a*10+b%10;
}
int main(int argc, char const *argv[]){
	freopen("C.in", "r", stdin);
	freopen("C.out", "w", stdout);
	while (getline(cin, str)) {
		if (str == "") {
			puts("0");
			continue;
		}
		memset(f, 63, sizeof f);
		f[0][56] = 0;
		int len = str.length();
		for (int i = 1; i <= len; i++) {
			int ch = str[i-1]-'0';
			// printf("%d\n", ch);
			if (!ch) ch = 10;
			for (int i_ = 1; i_ <= 9; i_++)
				for (int j = i_+1; j <= 10; j++){
					int k = id(i_, j);
					if (f[i-1][k] == inf) continue;
					// printf("%d -> %d = %d\n", i-1, k, f[i-1][k]);
					for (int l = 1; l <= ch-1; l++)
						f[i][id(l, ch)] = min(f[i][id(l, ch)], f[i-1][k]+dis(k, id(l, ch), 0, 1));
					for (int r = ch+1; r <= 10; r++)
						f[i][id(ch, r)] = min(f[i][id(ch, r)], f[i-1][k]+dis(k, id(ch, r), 1, 0));
				}
		}
		int ans = 1<<30;
		for (int i = 1; i <= 9; i++)
			for (int j = i+1; j <= 10; j++)
				ans = min(ans, f[len][id(i, j)]);
		printf("%d\n", ans);
	}
	return 0;
}
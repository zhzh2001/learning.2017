#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int x[N], y[N], z[N];
int sx[N], sy[N], sz[N];
int main(int argc, char const *argv[]){
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++) {
		x[i] = sx[i] = read();
		y[i] = sy[i] = read();
		z[i] = sz[i] = read();
	}
	ll sum = 0, mxn = 0;
	sort(sx+1, sx+n+1);
	sort(sy+1, sy+n+1);
	sort(sz+1, sz+n+1);
	for(int i = 1; i <= n; i++)
		printf("%d ", sx[i]);puts("");
	for(int i = 1; i <= n; i++)
		printf("%d ", sy[i]);puts("");
	for(int i = 1; i <= n; i++)
		printf("%d ", sz[i]);puts("");
	for (int i = 1; i <= n; i++) {
		int rx = lower_bound(sx+1, sx+n+1, x[i])-sx;
		int dx = 1<<30;
		if (rx != n) dx = min(dx, sx[rx+1]-x[i]);
		if (rx != 1) dx = min(dx, x[i]-sx[rx-1]);
		int ry = lower_bound(sy+1, sy+n+1, y[i])-sy;
		int dy = 1<<30;
		if (ry != n) dy = min(dy, sy[ry+1]-y[i]);
		if (ry != 1) dy = min(dy, y[i]-sy[ry-1]);
		int rz = lower_bound(sz+1, sz+n+1, z[i])-sz;
		int dz = 1<<30;
		if (rz != n) dz = min(dz, sz[rz+1]-z[i]);
		if (rz != 1) dz = min(dz, z[i]-sz[rz-1]);
		ll ans = min(min(dx, dy), dz);
		sum += ans;
		mxn = max(ans, mxn);
	}
	printf("%d\n", sum);
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
struct point{
	int x, y, z, id;
}a[N];
ll dis(point a, point b) {
	return min(min(abs(a.x-b.x), abs(a.y-b.y)), abs(a.z-b.z));
}
bool cmpx(const point &a, const point &b) {
	return a.x < b.x;
}
bool cmpy(const point &a, const point &b) {
	return a.y < b.y;
}
bool cmpz(const point &a, const point &b) {
	return a.z < b.z;
}
struct shenzhebei{
	int x, y;
	ll dis;
	friend bool operator < (const shenzhebei &a, const shenzhebei &b) {
		return a.dis < b.dis;
	}
}mp[500020]; // max n = 1000
int fa[N];
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
int main(int argc, char const *argv[]){
	freopen("A.in", "r", stdin);
	freopen("A.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++) {
		a[i].x = read();
		a[i].y = read();
		a[i].z = read();
		a[i].id = i;
	}
	int cnt = 0;
	sort(a+1, a+n+1, cmpx);
	for (int i = 2; i <= n; i++)
		mp[++cnt] = (shenzhebei){a[i].id, a[i-1].id, dis(a[i], a[i-1])};
	sort(a+1, a+n+1, cmpy);
	for (int i = 2; i <= n; i++)
		mp[++cnt] = (shenzhebei){a[i].id, a[i-1].id, dis(a[i], a[i-1])};
	sort(a+1, a+n+1, cmpz);
	for (int i = 2; i <= n; i++)
		mp[++cnt] = (shenzhebei){a[i].id, a[i-1].id, dis(a[i], a[i-1])};
	sort(mp+1, mp+cnt+1);
	ll sum = 0, ans = 0;
	for (int i = 1; i <= n; i++) fa[i] = i;
	for (int i = 1; i <= cnt; i++) {
		int x = find(mp[i].x), y = find(mp[i].y);
		if (x == y) continue;
		// printf("%d -> %d\n", mp[i].x, mp[i].y);
		fa[x] = y; sum += mp[i].dis;
		if (++ans == n-1) break;
	}
	printf("%lld\n", sum);
	return 0;
}
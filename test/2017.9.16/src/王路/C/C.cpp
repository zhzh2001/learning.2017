#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;

ifstream fin("C.in");
ofstream fout("C.out");

int n;

struct State {
	int k, x, y;
	State() {}
	State(int k, int x, int y) : k(k), x(x), y(y) {}
};

const int MaxN = 105, VMax = 11, inf = 0x3f3f3f3f;

bool vis[MaxN][VMax][VMax];
int f[MaxN][VMax][VMax], a[MaxN];

inline bool Check(const State &now) {
	return now.x < 1 || now.y < 1 || now.x > 10 || now.y > 10 || now.x >= now.y;
}

#define Relax()                                 								\
	do {																													\
		if (f[nxt.k][nxt.x][nxt.y] > f[now.k][now.x][now.y] + 1) {  \
			f[nxt.k][nxt.x][nxt.y] = f[now.k][now.x][now.y] + 1;			\
			if (!vis[nxt.k][nxt.x][nxt.y]) {													\
				vis[nxt.k][nxt.x][nxt.y] = true;												\
				Q.push(nxt);																						\
			}																													\
		}																														\
	} while (0)

void Spfa() {
	queue<State> Q;
	Q.push(State(0, 5, 6));
	vis[0][5][6] = true;
	f[0][5][6] = 0;
	for (; !Q.empty(); Q.pop()) {
		State now = Q.front();
		for (int dx = -1; dx <= 1; ++dx) {
			for (int dy = -1; dy <= 1; ++dy) {
				State nxt(now.k, now.x + dx, now.y + dy);
				if (Check(nxt)) continue;
				Relax();
			}
		}
		if (now.k < n) {
			if (now.x == a[now.k + 1]) {
				for (int d = -1; d <= 1; ++d) {
					State nxt(now.k + 1, now.x, now.y + d);
					if (Check(nxt)) continue;
					Relax();
				}
			}
			if (now.y == a[now.k + 1]) {
				for (int d = -1; d <= 1; ++d) {
					State nxt(now.k + 1, now.x + d, now.y);
					if (Check(nxt)) continue;
					Relax();
				}
			}
		}
		vis[now.k][now.x][now.y] = false;
	}
}

int main() {
	for (string s; getline(fin, s); ) {
		memset(vis, 0x00, sizeof vis);
		memset(f, 0x3f, sizeof f);
		n = s.length();
		if (!n) {
			fout << 0 << endl;
			continue;
		}
		for (int i = 1; i <= n; ++i) {
			a[i] = s[i - 1] - '0';
			if (!a[i]) a[i] = 10;
		}
		Spfa();
		int ans = inf;
		for (int i = 1; i <= 10; ++i) {
			for (int j = i + 1; j <= 10; ++j) {
				ans = min(ans, f[n][i][j]);
			}
		}
		fout << ans << endl;
	}
 	return 0;
}

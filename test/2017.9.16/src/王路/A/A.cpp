#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

ifstream fin("A.in");
ofstream fout("A.out");

const int MaxN = 100005;

struct Node {
	int x, y, z, id;
	Node() {}
	Node(int x, int y, int z) : x(x), y(y), z(z) {}
} a[MaxN], *ord[MaxN];

inline bool cmp1(const Node *a, const Node *b) {
	return a->x < b->x;
}

inline bool cmp2(const Node *a, const Node *b) {
	return a->y < b->y;
}

inline bool cmp3(const Node *a, const Node *b) {
	return a->z < b->z;
}

struct Edge {
	int u, v, w;
	Edge() {}
	Edge(int u, int v, int w) : u(u), v(v), w(w) {}
	bool operator<(const Edge &other) const { return w < other.w; }
};

vector<Edge> vec;

int fa[MaxN];

inline int Find(int x) {
	return x == fa[x] ? x : fa[x] = Find(fa[x]);
}

inline void Reset(int x) {
	for (int i = 0; i <= x; ++i) {
		fa[i] = i;
	}
}

int main() {
	int n;
	fin >> n;
	for (int i = 0; i < n; ++i) {
		fin >> a[i].x >> a[i].y >> a[i].z;
		a[i].id = i;
		ord[i] = a + i;
	}
	sort(ord, ord + n, cmp1);
	for (int i = 1; i < n; ++i) {
		Node *a = ord[i - 1], *b = ord[i];
		vec.push_back(Edge(a->id, b->id, b->x - a->x));
	}
	sort(ord, ord + n, cmp2);
	for (int i = 1; i < n; ++i) {
		Node *a = ord[i - 1], *b = ord[i];
		vec.push_back(Edge(a->id, b->id, b->y - a->y));
	}
	sort(ord, ord + n, cmp3);
	for (int i = 1; i < n; ++i) {
		Node *a = ord[i - 1], *b = ord[i];
		vec.push_back(Edge(a->id, b->id, b->z - a->z));
	}
	sort(vec.begin(), vec.end());
	Reset(n);
	int cnt = 0;
	long long res = 0;
	for (int i = 0; i < vec.size(); ++i) {
		int f1 = Find(vec[i].u), f2 = Find(vec[i].v);
		if (f1 != f2) {
			fa[f1] = f2;
			res += vec[i].w;
			if (++cnt == n - 1) break;
		}
	}
	fout << res << endl;
	return 0;
}

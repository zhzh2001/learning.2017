#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>

#include <iostream>
using namespace std;

ifstream fin("B.in");
ofstream fout("B.out");

const int MaxN = 205;

int f[MaxN][MaxN][MaxN], g[MaxN][MaxN], c[MaxN], n, tot[MaxN][MaxN];
bool vis[MaxN];

int main() {
	int T;
	fin >> T;
	for (int t = 1; t <= T; ++t) {
		fin >> n;
		for (int i = 1; i <= n; ++i) {
			fin >> c[i];
		}
		memset(tot, 0x00, sizeof tot);
		for (int i = 1; i <= n; ++i) {
		  for (int j = i; j <= n; ++j) {
        tot[i][j] = tot[i][j - 1];
        tot[i][j] += (c[i] == c[j]);
        // clog << "tot[" << i << "][" << j << "]: " << tot[i][j] << endl;
      }
		}
		memset(f, 0x00, sizeof f);
		memset(g, 0x00, sizeof g);
		for (int i = 1; i <= n; ++i) {
		  g[i][i] = 1;
		}
		for (int len = 1; len <= n; ++len) {
			for (int l = 1; l + len <= n; ++l) {
				int r = l + len;
			  if (c[l] != c[r]) goto skp;
				for (int k = 2; k <= tot[l][r]; ++k) {
					for (int i = l; i + 1 <= r; ++i) {
						if (c[l] != c[i] || c[i + 1] == c[i]) continue;
						f[l][r][k] = max(f[l][r][k], f[l][i][k - 1] + g[i + 1][r - 1]);
					}
					g[l][r] = max(g[l][r], f[l][r][k] + k * k);
				}
skp:;
				for (int mid = l; mid < r; ++mid) {
				  g[l][r] = max(g[l][r], g[l][mid] + g[mid + 1][r]);
				}
			}
		}
		fout << "Case " << t << ": " << g[1][n] << endl;
	}
	return 0;
}


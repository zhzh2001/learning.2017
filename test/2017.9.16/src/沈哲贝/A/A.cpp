#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 600010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll n,ans,fa[maxn],tot;
struct Data{	ll w,x,y;	}q[maxn];
struct data{	ll x,y,z,id;	}p[maxn];
inline bool cmp(Data a,Data b){	return a.w<b.w;	}
inline bool cmp1(data a,data b){	return a.x<b.x;	}
inline bool cmp2(data a,data b){	return a.y<b.y;	}
inline bool cmp3(data a,data b){	return a.z<b.z;	}
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);	}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	For(i,1,n)	p[i].x=read(),p[i].y=read(),p[i].z=read(),p[i].id=i;
	sort(p+1,p+n+1,cmp1);
	For(i,2,n)	q[++tot].x=p[i-1].id,q[tot].y=p[i].id,q[tot].w=p[i].x-p[i-1].x;
	sort(p+1,p+n+1,cmp2);
	For(i,2,n)	q[++tot].x=p[i-1].id,q[tot].y=p[i].id,q[tot].w=p[i].y-p[i-1].y;
	sort(p+1,p+n+1,cmp3);
	For(i,2,n)	q[++tot].x=p[i-1].id,q[tot].y=p[i].id,q[tot].w=p[i].z-p[i-1].z;
	sort(q+1,q+tot+1,cmp);
	For(i,1,n)	fa[i]=i;
	For(i,1,tot){
		ll x=find(q[i].x),y=find(q[i].y);
		if (find(x)!=find(y))	fa[find(x)]=find(y),ans+=q[i].w;
	}writeln(ans);
}
/*
5
11 -15 -15
14 -5 -15
-1 -1 -5
10 -4 -1
19 -4 19
*/

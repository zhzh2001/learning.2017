#include<cstdio>
#include<cstring>
#include<memory.h>
#include<algorithm>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
#define maxn 10000010
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
struct data{	ll x, y, w;}q[maxn];
ll x[maxn],y[maxn],z[maxn],fa[maxn],n,tot,ans;
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);	}
bool cmp(data a,data b){	return a.w<b.w;	}
int main(){
	freopen("a.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n){
		x[i]=read();y[i]=read();z[i]=read();
		fa[i]=i;
	}
	For(i,1,n)	For(j,i+1,n)	q[++tot].x=i,q[tot].y=j,q[tot].w=min(min(abs(x[i]-x[j]),abs(y[i]-y[j])),abs(z[i]-z[j]));
	sort(q+1,q+tot+1,cmp);
	For(i,1,tot){
		ll x=find(q[i].x),y=find(q[i].y);
		if (x^y)	fa[x]=y,ans+=q[i].w;
	}writeln(ans);
}

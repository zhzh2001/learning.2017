#include<ctime>
#include<cstdio>
#include<memory.h>
#include<algorithm>
#include<map>
#define ll long long
#define maxn 300010
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll P[410],ans,n,x,y,z,f[42][40010],C[400],A[40010][20],tt,q[maxn],hh,bin[400],gg[40010][13];
map<ll,ll>mp;	const ll mod=1e9+7;	bool mark[40010];
void dfs(ll dep,ll a,ll  b,ll c){
	if (a<0||b<0||c<0)	return;
	++tt;	ll AN=0;	A[tt][0]=dep-1;
	For(i,1,dep-1)	AN+=C[i]*bin[i-1],A[tt][i]=C[i];
	mp[AN]=tt;
	if (a)	For(i,1,a)	C[dep]=i,dfs(dep+1,a-i,b,c);
	else if (b)	For(i,1,b)	C[dep]=i,dfs(dep+1,a,b-i,c);
	else if (c)	For(i,1,c)	C[dep]=i,dfs(dep+1,a,b,c-i);
	else	return q[++hh]=tt,mark[tt]=1,void(0);
}
int main(){
	n=read();	x=read();	y=read();	z=read();
	writeln(clock());
	bin[0]=1;	For(i,1,17)	bin[i]=bin[i-1]*11;	dfs(1,x,y,z);
	P[0]=1;		For(i,1,40)	P[i]=P[i-1]*10%mod;
	writeln(clock());
	For(i,1,tt)if (!mark[i]){
		For(k,1,10){
			ll now=k,ans=mp[k];
			FOr(j,A[i][0],1)	now=now*11+A[i][j];
			For(j,1,A[i][0]){
				if (mp[now]){	ans=mp[now];	break;	}
				now/=11;
			}
			gg[i][k]=ans?ans:1;
		}
	}
	writeln(clock());
	f[0][1]=1;
	For(i,1,n){
		For(j,1,tt)	For(k,1,10)		f[i][gg[j][k]]=(f[i][gg[j][k]]+f[i-1][j])%mod;
		For(j,1,hh)	ans=(ans+f[i][q[j]]*P[n-i])%mod,f[i][q[j]]=0;
	}
	writeln(ans);
	writeln(clock());
}
/*
40 5 7 5
1 1 1 1
1 1 1 2
1 1 1 3
1	1 1 1
2	1 1 1
3	1 1 1
4	1 1 1
5	1 1 1
6	1 1 1
7	1 1 1
8	1 1 1
9	1 1 1	
10	1 1 1
*/

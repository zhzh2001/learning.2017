#include<cstring>
#include<ctime>
#include<cstdio>
#include<algorithm>
#include<map>
#define ll long long
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define FOr(i,j,k)  for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void wrote(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) wrote(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ wrote(x);   puts("");   }
ll f[110][11],a[110],n;	char s[110];
inline ll dis(ll a,ll b){	return abs(a-b);	}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	while(gets(s+1)){
		n=strlen(s+1);	ll ans=1e9;
		For(i,1,n){
			a[i]=s[i]-'0';
			if (!a[i])	a[i]+=10;
		}
		if (!n){	puts("0");	continue;	}
		memset(f,60,sizeof f);
		For(i,1,10)	f[1][i]=min(max(1+dis(a[1],6),dis(i,5)),max(1+dis(a[1],5),dis(i,6)));
		For(i,2,n)	For(j,1,10)	For(k,1,10)	f[i][k]=min(f[i][k],f[i-1][j]+min(max(1+dis(a[i-1],a[i]),dis(j,k)),max(1+dis(j,a[i]),dis(a[i-1],k))));
		For(i,1,10)	ans=min(ans,f[n][i]);
		writeln(ans);
	}
}
/*
434
56
57
*/

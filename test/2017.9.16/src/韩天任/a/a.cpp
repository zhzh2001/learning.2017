#include<bits/stdc++.h>
using namespace std;
long long flag,maxn,n,ans;
struct ppp{
	long long  a,b,c,num;
};
ppp a[100000];
long long go[10000],f[100000],t[100000];
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}
bool com1(ppp x,ppp y){
	return x.a<y.a;
}
bool com2(ppp x,ppp y){
	return x.b<y.b;
}
bool com3(ppp x,ppp y){
	return x.c<y.c;
}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout); 
	n=read();
	for (int i=1;i<=n+10;i++)
		f[i]=-1;
	for (int i=1;i<=n;i++){
		a[i].a=read();
		a[i].b=read();
		a[i].c=read();
		a[i].num=i;
	}
	sort(a+1,a+1+n,com1);
	for (int i=2;i<=n;i++){
		f[a[i].num]=abs(a[i].a-a[i-1].a);
		go[a[i].num]=a[i-1].num;
	}
	sort(a+1,a+1+n,com2);
	for (int i=2;i<=n;i++){
		if (f[a[i].num]==-1)
			f[a[i].num]=abs(a[i].b-a[i-1].b),go[a[i].num]=a[i-1].num;
		else
			if (f[a[i].num]>abs(a[i].b-a[i-1].b))
				f[a[i].num]=abs(a[i].b-a[i-1].b),go[a[i].num]=a[i-1].num;
	}
	sort(a+1,a+1+n,com3);
	for (int i=2;i<=n;i++){
		if (f[a[i].num]==-1)
			f[a[i].num]=abs(a[i].c-a[i-1].c),go[a[i].num]=a[i-1].num;
		else
			if (f[a[i].num]>abs(a[i].c-a[i-1].c))
				f[a[i].num]=abs(a[i].c-a[i-1].c),go[a[i].num]=a[i-1].num;
	}
	sort(f+1,f+n+1);
	for (int i=1;i<=n;i++)
		ans+=f[i];
	ans-=f[n/2+1];
	cout<<ans;
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct data1{
	int id,v;
}x[100010],y[100010],z[100010];
struct data2{
	int s,t,v;
}e[300010];
ll ans;
int n,nedge;
int father[100010];
 inline int read()
{
	int x=0,f=1; char ch=getchar(); 
	while (ch<'0'||ch>'9') {if(ch=='-') f=-1; ch=getchar();} 
	while (ch>='0'&&ch<='9') {x=x*10+ch-'0';ch=getchar();} 
	return x*f;
}
 bool cmp1(const data1 &a,const data1 &b)
{
	return a.v<b.v;
}
 bool cmp2(const data2 &a,const data2 &b)
{
	return a.v<b.v;
}
 int find(int v)
{
	if (v==father[v]) return v;
	return father[v]=find(father[v]);
	
}
 int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	n=read();
	for (int i=1;i<=n;++i)
	{
		x[i].v=read();x[i].id=i;
		y[i].v=read();y[i].id=i;
		z[i].v=read();z[i].id=i;
	}
	if (n==1&&n==0)
	{
		cout<<0;
		return 0;
	}
	sort(x+1,x+n+1,cmp1);
	sort(y+1,y+n+1,cmp1);
	sort(z+1,z+n+1,cmp1);
	for (int i=2;i<=n;++i)
	{
		e[++nedge].s=x[i-1].id;
		e[nedge].t=x[i].id;
		e[nedge].v=abs(x[i].v-x[i-1].v);
		e[++nedge].s=y[i-1].id;
		e[nedge].t=y[i].id;
		e[nedge].v=abs(y[i].v-y[i-1].v);
		e[++nedge].s=z[i-1].id;
		e[nedge].t=z[i].id;
		e[nedge].v=abs(z[i].v-z[i-1].v);
	}
	sort(e+1,e+nedge+1,cmp2);
	int num=0;
	for (int i=1;i<=n;++i) father[i]=i;
	for (int i=1;i<=nedge;++i)
	{
		int tx=find(e[i].s);
		int ty=find(e[i].t);
		if (tx!=ty)
		{
			father[tx]=ty;
			ans+=e[i].v;
			++num;
			if (num==n-1) break;
		}
	}
	printf("%lld",ans);
}

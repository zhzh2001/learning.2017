#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct rp{int x,v;}a[100010],b[100010],c[100010];
struct ppap{int x,y,v;}e[3000010];
int x[100010],y[100010],z[100010],n,fa[100010];
inline int getfather(int x){return fa[x]==x?x:fa[x]=getfather(fa[x]);}
inline bool cmp(ppap a,ppap b){return a.v<b.v;}
bool operator <(rp a,rp b){return a.v==b.v?a.x<b.x:a.v<b.v;}
signed main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i].x=b[i].x=c[i].x=i;
		x[i]=a[i].v=read(),y[i]=b[i].v=read(),z[i]=c[i].v=read();
	}
	sort(a+1,a+n+1);sort(b+1,b+n+1);sort(c+1,c+n+1);
	int cnt=0,p,q,s;
	for(int i=1;i<=n;i++){
		p=lower_bound(a+1,a+n+1,(rp){i,x[i]})-a;q=p;if(q<n)s=a[p+1].v;
		if(p>1&&a[p-1].v==x[i]){for(;q<=n;q++)if(a[q].v!=x[i])break;s=a[q].v;}
		for(;q<=n;q++){
			if(a[q].x==i)continue;if(a[q].v!=s)break;
			cnt++;e[cnt].x=i;e[cnt].y=a[q].x;e[cnt].v=a[q].v-x[i];q++;
		}		
		p=lower_bound(b+1,b+n+1,(rp){i,y[i]})-b;q=p;if(q<n)s=b[p+1].v;
		if(p>1&&b[p-1].v==y[i]){for(;q<=n;q++)if(b[q].v!=y[i])break;s=b[q].v;}
		for(;q<=n;q++){
			if(b[q].x==i)continue;if(b[q].v!=s)break;
			cnt++;e[cnt].x=i;e[cnt].y=b[q].x;e[cnt].v=b[q].v-y[i];q++;
		}
		p=lower_bound(c+1,c+n+1,(rp){i,z[i]})-c;q=p;if(q<n)s=c[p+1].v;
		if(p>1&&b[p-1].v==z[i]){for(;q<=n;q++)if(c[q].v!=z[i])break;s=c[q].v;}
		for(;q<=n;q++){
			if(c[q].x==i)continue;if(c[q].v!=s)break;
			cnt++;e[cnt].x=i;e[cnt].y=c[q].x;e[cnt].v=c[q].v-z[i];
		}
	}
	sort(e+1,e+cnt+1,cmp);
	for(int i=1;i<=n;i++)fa[i]=i;
	int ans=0;
	for(int i=1;i<=cnt;i++){
		int x=e[i].x,y=e[i].y;
		int fx=getfather(x),fy=getfather(y);
		if(fx==fy)continue;
		fa[fx]=fy;n--;ans+=e[i].v;
		if(n==1)break;
	}
	printf("%lld",ans);
	return 0;
}

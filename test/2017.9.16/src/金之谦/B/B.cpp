#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int f[210][210],n,a[1010],l,r,sum;
inline int sqr(int x){return x*x;}
int main()
{
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	int T=read();
	for(int cnt=1;cnt<=T;++cnt){
		n=read();
		for(int i=1;i<=n;++i)a[i]=read();
		memset(f,0,sizeof f);
		for(int i=1;i<=n;++i)f[i][i]=1;
		for(int i=n;i;--i){
			for(int j=i+1;j<=n;++j){
				if(a[i]==a[j]){
					if(f[i][j-1]==sqr(j-i)){f[i][j]=sqr(j-i+1);continue;}
					l=i;r=j;while(a[l]==a[i])l++;while(a[r]==a[i])r--;
					f[i][j]=f[l][r]+sqr(l-i+j-r);
					sum=0;l=0;int rp=0;
					for(int k=i;k<=j;++k){
						if(k>i&&a[k]!=a[k-1]){
							if(a[k]!=a[i]&&a[k-1]!=a[i])continue;
							if(a[k]==a[i])rp+=f[l][k-1];
							else l=k;
						}
						if(a[k]==a[i])sum++;
					}
					rp+=sqr(sum);f[i][j]=max(f[i][j],rp);
				}
				for(int k=i;k<j;++k)f[i][j]=max(f[i][j],f[i][k]+f[k+1][j]);
			}
		}
		printf("Case %d: %d\n",cnt,f[1][n]);
	}
	return 0;
}

#include <cstdio> 
#include <algorithm> 
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define LL long long 
using namespace std ; 

const int N = 100011 , M = 300011 ; 
LL sum ; 
int n,cnt ; 
int fa[N] ; 
struct node{
	int x,y,z,id ; 
}p[N] ; 
struct edge{
	int x,y,val ; 
}e[M] ; 

inline int read() 
{
	int x = 0 ,f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ; 
}
inline int Min(int x,int y) { return x < y ? x : y ; } 
inline int Abs(int x) { if(x<0) x=-x ; return x ; } 
inline void add(int u,int v) 
{
	e[++cnt].x = p[u].id ; 
	e[cnt].y = p[v].id ; 
	e[cnt].val = Min(Min(Abs(p[u].x-p[v].x),Abs(p[u].y-p[v].y)),Abs(p[u].z-p[v].z)) ; 
}
inline bool cmp1(node a,node b) { return a.x < b.x ; }  
inline bool cmp2(node a,node b) { return a.y < b.y ; }  
inline bool cmp3(node a,node b) { return a.z < b.z ; }  
inline bool cmp4(edge a,edge b) { return a.val < b.val ; }  
inline int find(int x)
{
	if(fa[x]==x) return x ; 
	return fa[x] = find(fa[x]) ; 
}
int main() 
{
	freopen("A.in","r",stdin) ; 
	freopen("A.out","w",stdout) ; 
	n = read() ;
	For(i,1,n) fa[i]=i ;  
	For(i,1,n) 
		p[i].x = read() , p[i].y = read() , p[i].z = read(),p[i].id = i ; 
	sort(p+1,p+n+1,cmp1) ; 
	For(i,2,n) add(i-1,i) ; 
	sort(p+1,p+n+1,cmp2) ; 
	For(i,2,n) add(i-1,i) ; 
	sort(p+1,p+n+1,cmp3) ;  
	For(i,2,n) add(i-1,i) ;  
	sort(e+1,e+cnt+1,cmp4) ; 
	For(i,1,cnt) {
		int x = find(e[i].x) ; 
		int y = find(e[i].y) ; 
		if(x==y) continue ; 
		sum = sum + e[i].val ; 
		fa[x] = y ; 
	}
	printf("%lld\n",sum) ; 
	return 0 ; 	
}






#include <bits/stdc++.h>
#define For(i,j,k) for(int i=j;i<=k;i++) 
#define LL long long 
using namespace std ; 

const int N = 511 ; 
int T,n ;  
LL MX ; 
int a[N],id[N],tmp[N],ta[N],ti[N] ; 

inline int read() 
{
	int x = 0 ,f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; } 
	return x * f ; 
}

inline void work() 
{
	int pos,sz ;
	LL ans = 0 ;  
	For(i,1,n) {
		pos = -1 ; 
		For(j,1,n) 	
		    if(id[j]==tmp[i]) {
				pos = j ; 
				break ; 
			}
		if (pos==-1) continue ; 
		int l = pos,r = pos ; 
		while(a[l-1]==a[pos])l-- ; 
		while(a[r+1]==a[pos])r++ ; 
		sz = r-l+1 ; 
		ans = ans + sz*sz ; 
		For(j,l,n) { a[j] = a[j+sz] ; id[j]= id[j+sz] ; }
		For(j,n-sz+1,n) 
			a[j]=id[j]=0 ; 
	}
	if(ans>MX) MX = ans ; 
	For(i,1,n) a[ i ] = ta[ i ] , id[ i ] = ti[ i ] ; 
}

bool f[N] ; 
inline void dfs(int x) 
{
	if(x==n+1) {
		work() ; 
		return ; 
	}
	For(i,1,n) 
		if(!f[i]) {
			tmp[x] = i ; 
			f[i]=1 ; 
			dfs(x+1) ; 			f[i]=0 ; 
		}
}

int main() 
{
	freopen("B.in","r",stdin) ; 
	freopen("B.out","w",stdout) ; 
	T = read() ; 
	For(ww,1,T) {
		n = read() ; 
		MX = 0 ; 
		For(i,0,500) a[i]=id[i]=0 ; 
		For(i,1,n) a[i] = read(),id[i] = i,f[i] = 0 ; 
		For(i,1,n) ta[ i ] = a[ i ] , ti[ i ] = id[ i ] ; 
		dfs(1) ; 
		printf("Case %d: %d\n",ww,MX) ; 
	}
	return 0 ; 
}



#include<cstdio>
#include<algorithm>
#include<cctype>
using namespace std;
const int N=1000005,SIZE=4000000;
FILE *fin,*fout;
int a[N];
char buf[SIZE],*p1=buf+SIZE,*pend=buf+SIZE;
inline int nc()
{
	if (p1==pend)
	{
		p1=buf;
		pend=buf+fread(buf,1,SIZE,fin);
		if (pend==p1)
			return EOF;
	}
	return *p1++;
}
inline void read(int& x)
{
	int c;
	while(isspace(c=nc()));
	int sign=1;
	if(c=='-')
		sign=-1,c=nc();
	x=0;
	for(;isdigit(c);c=nc())
		x=x*10+c-'0';
	x*=sign;
}
inline void out(char ch)
{
	if (p1==pend)
	{
		fwrite(buf,1,SIZE,fout);
		p1=buf;
	}
	*p1++=ch;
}
int tmp[15];
inline void write(int x)
{
	if(x<0)
		out('-'),x=-x;
	int p=0;
	do
		tmp[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		out(tmp[p]+'0');
}
int main()
{
	fin=fopen("game.in","r");
	fout=fopen("game.out","w");
	int n;
	read(n);
	for(int i=1;i<=n;i++)
		read(a[i]);
	p1=buf;pend=buf+SIZE;
	for(int i=1;i<=n;i++)
	{
		int l1=2e9,r1=-2e9;
		for(int j=i-1;j&&a[j]<=a[i];j--)
		{
			l1=min(l1,a[j]);
			r1=max(r1,a[j]);
		}
		int l2=2e9,r2=-2e9;
		for(int j=i+1;j<=n&&a[j]<=a[i];j++)
		{
			l2=min(l2,a[j]);
			r2=max(r2,a[j]);
		}
		if(l1==2e9||l2==2e9)
			write(-1);
		else
			write(max(r2-l1,r1-l2));
		out('\n');
	}
	fwrite(buf,1,p1-buf,fout);
	return 0;
}
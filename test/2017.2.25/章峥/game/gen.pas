program gen;
const
  n=1000000;
var
  i:longint;
begin
  randomize;
  assign(output,'game.in');
  rewrite(output);
  writeln(n);
  for i:=1 to n do
    write(random(2000000000)-1000000000,' ');
  writeln;
  close(output);
end.
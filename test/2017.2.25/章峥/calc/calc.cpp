#include<fstream>
#include<algorithm>
#include<cstring>
#include<string>
using namespace std;
ifstream fin("calc.in");
ofstream fout("calc.out");
struct bigint
{
	static const int MAXLEN=11000;
	int len,dig[MAXLEN];
	bigint()
	{
		len=1;
		memset(dig,0,sizeof(dig));
	}
	bigint(int x)
	{
		len=1;
		memset(dig,0,sizeof(dig));
		dig[1]=x;
	}
	void clear()
	{
		while(len&&dig[len]==0)
			len--;
	}
	bigint mypow10(int p)
	{
		len=p+1;
		memset(dig,0,sizeof(dig));
		dig[p+1]=1;
		return *this;
	}
	bigint operator+(int x)const
	{
		bigint res;
		res.len=len;
		for(int i=1;i<=len;i++)
		{
			res.dig[i]=dig[i]+x;
			x=res.dig[i]/10;
			res.dig[i]%=10;
		}
		if(x)
			res.dig[++res.len]=x;
		return res;
	}
	bigint operator+(const bigint& b)const
	{
		bigint res;
		res.len=max(len,b.len);
		int overflow=0;
		for(int i=1;i<=res.len;i++)
		{
			res.dig[i]=dig[i]+b.dig[i]+overflow;
			overflow=res.dig[i]/10;
			res.dig[i]%=10;
		}
		if(overflow)
			res.dig[++res.len]=overflow;
		return res;
	}
	bigint operator*(const bigint& b)const
	{
		bigint res;
		res.len=len+b.len-1;
		for(int i=1;i<=len;i++)
			for(int j=1;j<=b.len;j++)
				res.dig[i+j-1]+=dig[i]*b.dig[j];
		for(int i=1;i<=res.len;i++)
		{
			res.dig[i+1]+=res.dig[i]/10;
			res.dig[i]%=10;
		}
		if(res.dig[res.len+1])
			res.len++;
		return res;
	}
	bigint operator*=(const bigint& b)
	{
		return *this=*this*b;
	}
	bigint operator/(const int x)const
	{
		bigint res;
		res.len=len;
		int remain=0;
		for(int i=len;i;i--)
		{
			res.dig[i]=(dig[i]+remain)/x;
			remain=(dig[i]+remain)%x*10;
		}
		res.clear();
		return res;
	}
	bool operator<(const bigint& b)const
	{
		if(len!=b.len)
			return len<b.len;
		for(int i=len;i;i--)
			if(dig[i]!=b.dig[i])
				return dig[i]<b.dig[i];
		return false;
	}
	bool operator<=(const bigint& b)const
	{
		return !(b<*this);
	}
};
istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b=bigint();
	b.len=s.length();
	for(int i=1;i<=b.len;i++)
		b.dig[i]=s[b.len-i]-'0';
	b.clear();
	return is;
}
ostream& operator<<(ostream& os,bigint& b)
{
	for(int i=b.len;i;i--)
		os<<b.dig[i];
	return os;
}
bigint qpow(bigint base,int p)
{
	bigint now=base,ans=1;
	do
	{
		if(p&1)
			ans*=now;
		if(p/2)
			now*=now;
	}
	while(p/=2);
	return ans;
}
int main()
{
	int m;
	bigint n;
	fin>>m>>n;
	if(m==1)
	{
		fout<<n<<endl;
		return 0;
	}
	bigint l,r=bigint().mypow10(n.len/m+1);
	while(l+1<r)
	{
		bigint mid=(l+r)/2;
		if(qpow(mid,m)<=n)
			l=mid;
		else
			r=mid;
	}
	if(qpow(r,m)<=n)
		fout<<r<<endl;
	else
		fout<<l<<endl;
	return 0;
}
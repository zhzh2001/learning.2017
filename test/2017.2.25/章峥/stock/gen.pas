program gen;
const
  n=250;
  m=100000;
var
  i,u,v:longint;
begin
  randomize;
  assign(output,'stock.in');
  rewrite(output);
  writeln(n,' ',m);
  for i:=1 to m do
  begin
    u:=random(n)+1;v:=random(n)+1;
	if u=v then
	  inc(v);
	if u<v then
	  writeln(u,' ',v)
	else
      writeln(v,' ',u);
  end;
  close(output);
end.
#include<fstream>
#include<cstring>
#include<ctime>
using namespace std;
ifstream fin("stock.in");
ofstream fout("stock.out");
const int N=255,M=100005;
int e,head[N],v[M],nxt[M];
bool vis[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
		if(!vis[v[i]])
			dfs(v[i]);
}
int main()
{
	int n,m;
	fin>>n>>m;
	int ans=0;
	e=0;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		memset(vis,false,sizeof(vis));
		dfs(v);
		if(!vis[u])
			add_edge(u,v);
		else
			ans++;
		if(clock()>900)
		{
			if(e==0)
				fout<<0<<endl;
			else
				fout<<(e+ans+m)*ans/e<<endl;
			return 0;
		}
	}
	fout<<ans<<endl;
	return 0;
}
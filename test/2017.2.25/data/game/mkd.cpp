#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<algorithm>
#define P 1000000000
#define N 1000005
int a[N];
int main()
{
	int n,m,x,y,num,size;
	freopen("game.in","w",stdout);
	srand(time(0));
	scanf("%d%d",&n,&size);
	printf("%d\n",n);
	for (int i=1;i<=n;++i)a[i]=rand()*rand()%P;
	if (size!=-1){
		num=n/size*4+1;
		for (int i=1;i<=num;++i){
			int x=rand()*rand()%n+1,y=x+rand()*rand()%size;
			if (y>n)y=n;
			std::sort(a+1+x,a+1+y);
		}
		for (int i=1;i<n;++i){
			if (rand()%3!=0)std::swap(a[i],a[i+1]);
		}
	}
	for (int i=1;i<=n;++i)printf("%d ",a[i]);printf("\n");
	//system("pause");
	return 0;
}



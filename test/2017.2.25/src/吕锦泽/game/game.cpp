#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1000005;
struct data{
	int l,r,v,ans;
}a[N];
int left[4*N],right[4*N],maxn[4*N],minn[4*N];
int n,m;
void build(int l,int r,int p){
	left[p]=l;right[p]=r;
	if (l==r){
		maxn[p]=minn[p]=a[l].v;
		return;
	}
	int mid=(l+r)>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	maxn[p]=max(maxn[p<<1],maxn[p<<1|1]);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
int query_max(int l,int r,int p){
	if (l==left[p]&&r==right[p]) return maxn[p];
	int mid=(left[p]+right[p])>>1;
	if (r<=mid) return query_max(l,r,p<<1);
	else if (l>mid) return query_max(l,r,p<<1|1);
	else return max(query_max(l,mid,p<<1),query_max(mid+1,r,p<<1|1));
}
int query_min(int l,int r,int p){
	if (l==left[p]&&r==right[p]) return minn[p];
	int mid=(left[p]+right[p])>>1;
	if (r<=mid) return query_min(l,r,p<<1);
	else if (l>mid) return query_min(l,r,p<<1|1);
	else return min(query_min(l,mid,p<<1),query_min(mid+1,r,p<<1|1));
}
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i].v);
	}
	build(1,n,1);
}
void work(){
	a[1].ans=a[n].ans=-1;
	for (int i=2;i<=n;i++)
		if (a[i].v>a[i-1].v) a[i-1].ans=-1;
		else if (a[i].v<a[i-1].v) a[i].ans=-1;
	for (int i=2;i<n;i++){
		if (a[i].ans==-1) continue;
		if (a[i-1].ans==-1&&a[i].ans!=-1){
			int l=i-1,r=i+1;
			while (l>=1&&a[l].v<=a[i].v) l--;
			while (r<=n&&a[r].v<=a[i].v) r++;
			a[i].l=l+1; a[i].r=r-1;
		}else
		if (a[i-1].ans!=-1&&a[i].ans!=-1){
			a[i].l=a[i-1].l; a[i].r=a[i-1].r;
		}
		int t1=abs(query_max(a[i].l,i-1,1)-query_min(i+1,a[i].r,1));
		int t2=abs(query_max(i+1,a[i].r,1)-query_min(a[i].l,i-1,1));
		a[i].ans=max(t1,t2);
	}
	for (int i=1;i<=n;i++)
		printf("%d\n",a[i].ans);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	init();
	work();
}

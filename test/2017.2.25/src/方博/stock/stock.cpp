#include<bits/stdc++.h>
using namespace std;
bool f[255][255];
int n,k,ans;
int main()
{
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	scanf("%d%d",&n,&k);
	memset(f,false,sizeof(f));
	for(int i=1;i<=n;i++)
		f[i][i]=true;
	for(int i=1;i<=k;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		if(f[y][x]){
			ans++;
			continue;
		}
		for(int j=1;j<=n;j++)
			f[x][j]=f[x][j]||f[y][j];
		f[x][y]=true;
	}
	printf("%d",ans);
	return 0;
}

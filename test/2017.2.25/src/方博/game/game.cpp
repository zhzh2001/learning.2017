#include<bits/stdc++.h>
using namespace std;
const int N=1000006;
int a[N];
int ans[N];
int n;
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	ans[1]=-1;
	ans[n]=-1;
	for(int i=2;i<n;i++){
		if(a[i]==a[i-1]&&ans[i-1]!=-1){
			ans[i]=ans[i-1];
			continue;
		}
		if(a[i+1]>a[i]||a[i-1]>a[i]){
			ans[i]=-1;
			continue;
		}
		int l=i-1;
		int r=i+1;
		int maxl=-(1<<30);
		int maxr=-(1<<30);
		int minl=1<<30;
		int minr=1<<30;
		while(l>=1&&a[l]<=a[i]){
			maxl=max(maxl,a[l]);
			minl=min(minl,a[l]);
			l--;
		}
		while(r<=n&&a[r]<=a[i]){
			maxr=max(maxr,a[r]);
			minr=min(minr,a[r]);
			r++;
		}
		ans[i]=max(maxr-minl,maxl-minr);
	}
	for(int i=1;i<=n;i++)
		printf("%d\n",ans[i]);
	return 0;
}

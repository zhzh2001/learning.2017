#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
int st[1000005][22][2],i,j,k,x,y,n,m,a,b,c,d,z;
int da(int a,int b)
{
	int z=(int)log2(b-a+1);
	int x=1<<z;
	//printf("\n!%d %d %d %d %d \n",a,b,z,x,b-x+1);
	return max(st[a][z][0],st[b-x+1][z][0]);
}
int xiao(int a,int b)
{
	int z=(int)log2(b-a+1);
	int x=1<<z;
	return min(st[a][z][1],st[b-x+1][z][1]);
}
int zef(int x)
{
	int l=0,r=x,mid,p;
	while (l!=r)
	{
		mid=(l+r+1)>>1;
		p=da(mid,r);
		//printf("%d %d %d %d",l,r,mid,p);
		if (p>st[i][0][0])
			l=mid;
		else
			r=mid-1;
	}
	return l;
}
int yef(int x)
{
	int l=x,r=n+1,mid,p;
	while (l!=r)
	{
		mid=(l+r)>>1;
		p=da(l,mid);
		//printf("!%d %d %d %d %d",i,l,r,mid,p);
		if (p>st[i][0][0])
			r=mid;
		else
			l=mid+1;
	}
	return l;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
	{
		scanf("%d",&st[i][0][0]);
		st[i][0][1]=st[i][0][0];
	}
	st[0][0][0]=st[0][0][1]=st[n+1][0][0]=st[n+1][0][1]=(int)1e9;
	x=(int)log2(n);
	for (i=1;i<=x;++i)
	{
		y=1<<i;
		z=y>>1;
		for (j=0;j<=n-y+1;++j)
		{
			st[j][i][0]=max(st[j][i-1][0],st[j+z][i-1][0]);
			st[j][i][1]=min(st[j][i-1][1],st[j+z][i-1][1]);
		}
	}
	//for (i=0;i<=n;++i)
	//	printf("?%d %d %d\n",i,st[i][0][0],st[i][0][1]);
	/*for (i=0;i<=x;++i,cout<<endl)
		for (j=0;j<=n-(1<<i)+1;++j)
			printf("%d ",st[j][i][0]);*/
	printf("-1\n");
	for (i=2;i<=n-1;++i)
	{
		x=zef(i-1);
		y=yef(i+1);
		//printf("\n%d %d %d\n",x,y,i);
		if (x==i-1 || y==i+1)
			printf("-1\n");
		else
		{
			a=da(x+1,i-1);
			b=xiao(i+1,y-1);
			c=da(i+1,y-1);
			d=xiao(x+1,i-1);
			printf("%d\n",max(a-b,c-d));
		}
	}
	printf("-1\n");
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
struct node{int sx[N],len;};
node l,r,mid,a,ans,tt,rr;
int m,sx;
bool check()
{int i,j;
	if(l.len>r.len) return false;
	if(l.len<r.len) return true;
	for(i=l.len;i>=1;i--)
	  if(l.sx[i]<r.sx[i]) return true;
	  else if(l.sx[i]>r.sx[i]) return false;
	return true;
}
void plus()
{int x=0,i,len;
	len=max(l.len,r.len);
	for(i=1;i<=len+1;i++)
	  {
	  	mid.sx[i]=l.sx[i]+r.sx[i]+x;
	  	x=mid.sx[i]/10;mid.sx[i]%=10;
	  }
	if(mid.sx[len+1]!=0) len++;
	mid.len=len;
}
void div()
{int x=0,i,len=mid.len;
	for(i=len;i>=1;i--)
	 {
	 	mid.sx[i]=mid.sx[i]+x*10;x=mid.sx[i]%2;mid.sx[i]/=2;
	 }
	while(mid.sx[len]==0)len--;
	mid.len=len;
}
void power()
{int i,j,k,x=0;
    tt.len=1;memset(tt.sx,0,sizeof(tt.sx));
    tt.sx[1]=1;
    for(k=1;k<=m;k++)
    {
      for(i=1;i<=mid.len;i++)
        for(j=1;j<=tt.len;j++)
          {
          	rr.sx[i+j-1]=(rr.sx[i+j-1]+mid.sx[i]*tt.sx[j]);
          	rr.sx[i+j]+=rr.sx[i+j-1]/10;
          	rr.sx[i+j-1]%=10;
          }
      if(rr.sx[mid.len+tt.len]!=0) rr.len=mid.len+tt.len;else rr.len=mid.len+tt.len-1;
	  for(i=1;i<=rr.len;i++) tt.sx[i]=rr.sx[i];tt.len=rr.len;
	  memset(rr.sx,0,sizeof(rr.sx));
     }
}
bool check2()
{int i,j;
	if(tt.len>a.len) return false;
	if(tt.len<a.len) return true;
	for(i=tt.len;i>=1;i--)
	  if(tt.sx[i]<a.sx[i]) return true;
	    else if(tt.sx[i]>a.sx[i]) return false;
	return true;
}
void inc()
{int i,x=0;
   l.sx[1]=mid.sx[1]+1;
   for(i=2;i<=mid.len+1;i++)
     {
     	l.sx[i]=mid.sx[i]+x;x=l.sx[i]/10;l.sx[i]%=10;
     }
    l.len=mid.len;
    if(l.sx[l.len+1]!=0) l.len=l.len+1;
}
void dec()
{int i;
    memset(r.sx,0,sizeof(r.sx));
	for(i=1;i<=mid.len;i++) r.sx[i]=mid.sx[i];
	r.sx[1]--;
    for(i=1;i<=mid.len;i++)
     	if(r.sx[i]<0)
     	 {
     	 	r.sx[i+1]--;r.sx[i]+=10;
     	 }
    r.len=mid.len;
    if(r.sx[r.len]==0) r.len=r.len-1;
}
int main()
{int i,j,k;
 char c;
    freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout); 
    scanf("%d",&m);
    c=getchar();
    while(!(c>='0' && c<='9')) c=getchar();
    while((c>='0' && c<='9'))
      {
      	a.len++;a.sx[a.len]=c-48;
		c=getchar();
      }
    for(i=1;i<=a.len/2;i++) swap(a.sx[i],a.sx[a.len+1-i]);
	l.len=1;r.len=a.len;l.sx[1]=1;
	for(i=1;i<=a.len;i++) r.sx[i]=a.sx[i];
	while(check())
	  {
	  	plus();
		  div();
	  	power();
	  	if(check2())
		  {
		  for(i=1;i<=mid.len;i++) ans.sx[i]=mid.sx[i];ans.len=mid.len;
		  inc();
		  }
		  else dec();
	  }
	for(i=ans.len;i>=1;i--) printf("%d",ans.sx[i]); 
}

#include<bits/stdc++.h>
using namespace std;
long long n,a[10000000];
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;
	for (int i=1;i<=n;i++)
		cin>>a[i];
	for (int i=1;i<=n;i++){
		long long l=i-1,r=i+1,ans=0;
		bool bl=false,br=false;
		long long minl=1000000001,maxl=0;
		long long minr=1000000001,maxr=0;
		while (l>0 && a[l]<a[i]){
			if (maxl<a[l])
				maxl=a[l];
			if (minl>a[l])
				minl=a[l];
			bl=true;
			l--;
		}
		while (r<=n &&a[r]<a[i]){
			if (maxr<a[r])
				maxr=a[r];
			if (minr<a[r])
				minr=a[r];
			br=true;
			r++;
		}
		if (bl && br)
			ans=max(maxl-minr,maxr-minl);
		else
			ans=-1;
		cout<<ans<<endl;
	}
}

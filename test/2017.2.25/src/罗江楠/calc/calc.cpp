#include <bits/stdc++.h>
#define ll long long
using namespace std;
struct BigInteger{
	int len, s[50005];
	inline BigInteger(){
		memset(s, 0, sizeof(s));
		len = 1;
	}
	inline BigInteger(int num){
		*this = num;
	}
	inline BigInteger(char *num){
		*this = num;
	}
	inline BigInteger operator = (ll num){
		char c[10005];
		sprintf(c, "%lld", num);
		*this = c;
		return *this;
	}
	inline BigInteger operator = (const char *num){
		len = strlen(num);
		for(int i = 0; i < len; i++)
			s[i] = num[len-1-i] - '0';
		return *this;
	}
	inline string str(){
		string res = "";
		for(int i = 0; i < len; i++)
			res = (char)(s[i]+'0') + res;
		return res;
	}
	inline void clean(){
		while(len > 1 && !s[len-1])
			len--;
	}
	inline BigInteger operator + (const BigInteger &b){
		BigInteger c;
		c.len = 0;
		for(int i = 0, g = 0; g || i < len || i < b.len; i++){
			int x = g;
			if(i < len) x += s[i];
			if(i < b.len) x += b.s[i];
			c.s[c.len++] = x % 10;
			g = x / 10;
		}
		return c;
	}
	inline BigInteger operator - (const BigInteger &b){
		BigInteger c;
		c.len = 0;
		int x;
		for(int i = 0, g = 0; i < len; i++){
			x = s[i] - g;
			if(i < b.len) x -= b.s[i];
			if(x >= 0) g = 0;
			else x += 10, g = 1;
			c.s[c.len++] = x;
		}
		c.clean();
		return c;
	}
	inline BigInteger operator * (const BigInteger &b){
		BigInteger c;
		c.len = len + b.len;
		for(int i = 0; i < len; i++)
			for(int j = 0; j < b.len; j++)
				c.s[i+j] += s[i] * b.s[j];
		for(int i = 0; i < c.len-1; i++)
			c.s[i+1] += c.s[i] / 10,
			c.s[i] %= 10;
		c.clean();
		return c;
	}
	inline bool operator < (const BigInteger &b) const {
		if(len != b.len) return len < b.len;
		for(int i = len - 1; i >= 0; i--)
			 if(s[i] != b.s[i]) return s[i] < b.s[i];
		return 0;
	}
	inline bool operator <= (const BigInteger &b) const {
		return !(b < *this);
	}
	inline BigInteger operator += (const BigInteger &b){
		*this = *this + b;
		return *this;
	}
	inline BigInteger operator -= (const BigInteger &b){
		*this = *this - b;
		return *this;
	}
	inline BigInteger operator *= (const BigInteger &b){
		*this = *this * b;
		return *this;
	}
	inline BigInteger operator + (const ll &b){
		BigInteger c = *this;
		c += BigInteger(b);
		return c;
	}
	inline BigInteger operator - (const ll &b){
		BigInteger c = *this;
		c -= BigInteger(b);
		return c;
	}
	inline BigInteger operator * (const ll &b){
		BigInteger c = b;
		c *= *this;
		return c;
	}
	inline BigInteger operator / (const ll &b){
		BigInteger c = *this;
		for(int i = len-1; i >= 0; i--){
			if((c.s[i]&1) && i) c.s[i-1] += 10;
			c.s[i] /= 2;
		}
		if(!c.s[c.len-1])c.len--;
		return c;
	}
};
ostream& operator << (ostream &out, BigInteger &x){
	out << x.str();
	return out;
}
BigInteger a, l, r, mid, er;
int b;
char ch[10005];
inline BigInteger mi(BigInteger s){
	BigInteger c = 1;
	for(int i = 0; i < b; i++){
		c *= s;
		if(a<s)return c;
	}
	return c;
}
int main(){
	freopen("calc.in", "r", stdin);
	freopen("calc.out", "w", stdout);
	scanf("%d%s", &b, ch);
	a = ch;
	if(b == 1){
		cout << a;
		return 0;
	}
	r = a;
	while(l <= r){
		mid = (l + r) / 2;
		er = mi(mid);
		if(a < er)
			r = mid-1;
		else if(er < a)
			l = mid+1;
		else{
			cout << mid;
			return 0;
		}
	}
	mid -= 1;
	cout << mid;
	return 0;
}


///////////////////////////////////////////////////////////////////
//                            _ooOoo_                            //
//                           o8888888o                           //
//                           88" . "88                           //
//                           (| ^_^ |)                           //
//                           O\  =  /O                           //
//                        ____/`---'\____                        //
//                      .'  \\|     |//  `.                      //
//                     /  \\|||  :  |||//  \                     //
//                    /  _||||| -:- |||||-  \                    //
//                    |   | \\\  -  /// |   |                    //
//                    | \_|  ''\---/''  |   |                    //
//                    \  .-\__  `-`  ___/-. /                    //
//                  ___`. .'  /--.--\  `. . ___                  //
//               ."" '<  `.___\_<|>_/___.'  >'"".                //
//              | | :  `- \`.;`\ _ /`;.`/ - ` : | |              //
//              \  \ `-.   \_ __\ /__ _/   .-` /  /              //
//       ========`-.____`-.___\_____/___.-`____.-'========       //
//                            `=---='                            //
//       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       //
//                     佛祖保佑      RP++                        //
///////////////////////////////////////////////////////////////////
//马丹调了劳资两个小时，不过我就直播打CYM!!!
//MLE就神作了 
#include <bits/stdc++.h>
#define ll long long
#define N 1000005
using namespace std;
int a[N<<2], ans[N], b[N<<2], val[N], tot, n;
void build(int id, int l, int r){
	if(l == r){scanf("%d", a+id);b[id]=a[id];val[++tot]=a[id];return;}
	int mid = l + r >> 1;
	build(id<<1, l, mid);
	build(id<<1|1, mid+1, r);
	a[id] = max(a[id<<1], a[id<<1|1]);
	b[id] = min(b[id<<1], b[id<<1|1]);
}
int askmax(int id, int L, int R, int l, int r){
	if(L==l&&R==r) return a[id];
	int mid = L + R >> 1;
	if(r <= mid) return askmax(id<<1, L, mid, l, r);
	else if(l > mid) return askmax(id<<1|1, mid+1, R, l, r);
	else return max(askmax(id<<1, L, mid, l, mid), askmax(id<<1|1, mid+1, R, mid+1, r));
}
int askmin(int id, int L, int R, int l, int r){
	if(L==l&&R==r) return b[id];
	int mid = L + R >> 1;
	if(r <= mid) return askmin(id<<1, L, mid, l, r);
	else if(l > mid) return askmin(id<<1|1, mid+1, R, l, r);
	else return min(askmin(id<<1, L, mid, l, mid), askmin(id<<1|1, mid+1, R, mid+1, r));
}
void work(int l, int r){
	if(l > r) return;
	ans[l] = ans[r] = -1;
	if(l==r) return;
	int k = askmax(1, 1, n, l, r);
	int last = l-1;
	vector<int> v;
	for(int i = l; i <= r; i++)
		if(val[i] == k)
			v.push_back(i);
	for(int i = 0; i < v.size(); i++){
		if(v[i]!=l&&v[i]!=r)
		ans[v[i]] = max(askmax(1, 1, n, l, v[i]-1) - askmin(1, 1, n, v[i]+1, r),
						askmax(1, 1, n, v[i]+1, r) - askmin(1, 1, n, l, v[i]-1));
		work(last+1, v[i]-1);
		last = v[i];
	}
	if(last!=l-1)
	work(last+1, r);
}
int main(){
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	scanf("%d", &n);
	build(1, 1, n);
	work(1, n);
	for(int i = 1; i <= n; i++) printf("%d\n", ans[i]);
	return 0;
}


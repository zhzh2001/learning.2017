#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<algorithm>
using namespace std;
int a[1000001];

int main(){
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]);
	printf("%d\n",-1);
	for(int i=2;i<n;i++){
		int l=i-1,r=i+1;
		int minn=1e9,maxn=-1e9,minm=1e9,maxm=-1e9;
		while(a[l]<=a[i]&&!l){
			minn=min(a[l],minn);
			maxn=max(maxn,a[l]);
			l--;
		}
		while(a[r]<=a[i]&&r>=n){
			minm=min(minm,a[r]);
			maxm=max(maxn,a[r]);
			r++;
		}
		if(l==i-1||r==i+1){
			printf("-1\n");
			continue;
		}
		printf("%d\n",max(abs(maxm-minn),abs(maxn-minm)));
	}
	printf("%d\n",-1);
} 

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
using namespace std;
int n,m,ans;
bool f[300][300],ff[300][300];

int main(){
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++) f[i][i]=1;
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		if(x==y){
			ans++;
			continue;
		}
		for(int k=1;k<=n;k++)
			for(int j=1;j<=n;j++) ff[k][j]=f[k][j];
		ff[x][y]=1;
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					ff[i][j]=ff[i][k]&ff[k][j]|ff[i][j];
		bool flag=true;
		for(int j=1;j<=n;j++){
			if(!flag) break;
			for(int k=j+1;k<=n;k++)
				if(ff[j][k]&&ff[k][j]){
					flag=false;
					ans++;
					break;
				}
		}
		if(flag) f[x][y]=1;
	}
	printf("%d",ans);
} 

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<string>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;++x)
#define Drp(x,a,b) for (int x=a;x>=b;--x)
using namespace std;
const ff base=100000000;
char s[10010];
int i,j,k,m,n,len;
ff num[2000],l[2000],r[2000],mid[2000],f[2000],g[2000];
inline void calc_mid() {
	Rep(i,1,r[0]) mid[i]=r[i];
	Rep(i,1,l[0]) mid[i]+=l[i];
	mid[0]=r[0];
	ff now=0;
	Drp(i,mid[0],1) {
		now=now*base+mid[i];
		mid[i]=now/2;
		now%=2;
	}
	while (mid[mid[0]]==0) mid[0]--;
}
inline int judge(ff a[],ff b[]) {
	if (a[0]>b[0]) return 3;
	if (a[0]<b[0]) return 1;
	Drp(i,a[0],1) {
		if (a[i]<b[i]) return 1;
		if (a[i]>b[i]) return 3;
	}
	return 2;
}
inline void mid_pow() {
	Rep(i,1,f[0]) f[i]=g[i]=0;
	f[1]=f[0]=1;
	g[0]=0;
	Rep(k,1,n) {
		Rep(i,1,mid[0]) {
			Rep(j,1,f[0]) {
				g[i+j-1]+=mid[i]*f[j];
				g[i+j]+=g[i+j-1]/base;
				g[i+j-1]%=base;
			}
		}
		g[0]=mid[0]+f[0]-1;
		while (g[g[0]+1]!=0) {
			g[0]++;
			g[g[0]+1]+=g[g[0]]/base;
			g[g[0]]%=base;
		}
		Drp(i,g[0],0) {
			f[i]=g[i];
			g[i]=0;
		}
	}
}

int main() {
//	freopen("calc.in","r",stdin);
//	freopen("calc.out","w",stdout);
//	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	scanf ("%s",s+1);
	if (n==1) {
		printf("%s",s+1);
		return 0;
	}
	len=strlen(s+1);
	Rep(i,1,(len-1)/8+1) {
		int now=max(1,len-i*8+1);
		Rep(j,now,len-(i-1)*8) num[i]=num[i]*10+s[j]-48;
		num[0]++;
	}
	l[0]=l[1]=1;
	r[0]=(len/n+1)/8+3;
	r[r[0]]=1;
	while (judge(l,r)==1) {
		calc_mid();
		mid_pow();
		if (judge(f,num)>2) {
			Rep(i,0,mid[0]) r[i]=mid[i];
		} else {
			Rep(i,0,mid[0]) l[i]=mid[i];
			l[1]++;
			for (int i=1; l[i]==base && i<=l[0]; ++i) {
				l[i+1]++;
				l[i]=-base;
			}
			if (l[l[0]+1]) l[0]++;
		}
	}
	r[1]--;
	Rep(i,1,r[0])
	if (r[i]<0) {
		r[i]+=base;
		r[i+1]--;
	}
	if (r[r[0]]==0) r[0]--;
	printf("%I64d",r[r[0]]);
	Drp(i,r[0]-1,1) printf("%08I64d",r[i]);
	printf("\n");
	return 0;
}

//Live long and prosper.
#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,t,ans,q[260],Maps[260][260];
int main()
{
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>t;
	Rep(i,1,n) Maps[i][i]=1;
	Rep(i,1,t)
	{
		int x,y;
		cin>>x>>y;
		if (Maps[y][x])
			{
				ans++;
				continue;
			}
		if (Maps[x][y]) continue;
		int top=0;
		Rep(j,1,n) if (Maps[j][x]) q[++top]=j;
		Rep(j,1,n) if (Maps[y][j]) Rep(k,1,top) Maps[q[k]][j]=1;
	}
	cout<<ans<<endl;
	return 0;
}

//Live long and prosper.
#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<cstring>
#define N 1000005
#define MAX (1<<30)
#define MIN -(1<<30)
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,a[N],f[N],F[N],Min[N],Max[N],MIn[N],MAx[N];
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
//	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	Rep(i,1,n) scanf ("%d",&a[i]);
	a[0]=a[n+1]=MAX;
	Rep(i,1,n)
	{
		Min[i]=MAX;
		Max[i]=MIN;
		j=i-1;
		while (a[j]<=a[i])
			{
				Min[i]=min(a[j],Min[i]);
				Max[i]=max(a[j],Max[i]);
				Min[i]=min(Min[i],Min[j]);
				Max[i]=max(Max[i],Max[j]);
				j=f[j]-1;
			}
		f[i]=j+1;
	}
	Rep(i,1,n) F[i]=f[i];
	Rep(i,1,n)
	{
		MIn[i]=Min[i];
		MAx[i]=Max[i];
	}
	Drp(i,n,1)
	{
		Min[i]=MAX;
		Max[i]=MIN;
		j=i+1;
		while (a[j]<=a[i])
			{
				Min[i]=min(a[j],Min[i]);
				Max[i]=max(a[j],Max[i]);
				Min[i]=min(Min[i],Min[j]);
				Max[i]=max(Max[i],Max[j]);
				j=f[j]+1;
			}
		f[i]=j-1;
	}
	Rep(i,1,n)
	{
		int ans=MIN;
		if (F[i]==i || f[i]==i)
			{
				cout<<"-1"<<endl;
			}
		else
			{
				ans=max(ans,MAx[i]-Min[i]);
				ans=max(ans,Max[i]-MIn[i]);
				cout<<ans<<endl;
			}
	}
	return 0;
}

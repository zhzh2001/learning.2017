#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
using namespace std;
//ifstream fin("game.in");
//ofstream fout("game.out");
int n;
int num[1000003];
int ans[1000003];
int pd(int nn){
	if(abs(nn)>=1000000002){
		return 0;
	}else{
		return nn;
	}
}
int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>num[i];
	}
	num[0]=1000000003;
	num[n+1]=1000000003;
	for(int i=1;i<=n;i++){
		if(num[i]<num[i-1]&&num[i]<num[i+1]){
			ans[i]=-1;
			continue;
		}
		int x=num[i];
		int nowmnl=1000000003,nowmxl=-1000000003,nowmnr=1000000003,nowmxr=-1000000003;
		int nowl=i-1,nowr=i+1;
		while(num[nowl]<x){
			nowmnl=min(num[nowl],nowmnl);
			nowmxl=max(num[nowl],nowmxl);
			nowl-=1;
		}
		while(num[nowr]<x){
			nowmnr=min(num[nowr],nowmnr);
			nowmxr=max(num[nowr],nowmxr);
			nowr+=1;
		}
		nowmnl=pd(nowmnl);
		nowmnr=pd(nowmnr);
		nowmxl=pd(nowmxl);
		nowmxr=pd(nowmxr);
		ans[i]=max(nowmxl-nowmnr,nowmxr-nowmnl);
	}
	for(int i=1;i<=n;i++){
		cout<<ans[i]<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*
in1:
3
1 3 2
out1:
-1
1
-1

in2:
6
5 1 4 3 2 6
out2:
4
-1
2
2
-1
5
*/

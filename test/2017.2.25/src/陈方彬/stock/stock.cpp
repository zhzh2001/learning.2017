#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cmath>
#define Ll long long
using namespace std;
struct cs{
	int to,next;
}a[1000000];
int f[300][300],vi[300],head[300];
int n,m,x,y,z,ans,ll,E;
bool dfs(int x){
	vi[x]=1;
	if(x==E)return 1;
	bool ans=0;
	for(int k=head[x];k;k=a[k].next)
		if(!vi[a[k].to])ans=ans|dfs(a[k].to);
	return ans;
}
void init(int x,int y){
	ll++;
	a[ll].to=y;
	a[ll].next=head[x];
	head[x]=ll;
}
int main()
{
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	scanf("%d%d",&n,&m);
	while(m--){
		scanf("%d%d",&x,&y);
		if(f[x][y]==1)continue;
		if(f[x][y]==-1){ans++;continue;}
		E=x;
		for(int i=1;i<=n;i++)vi[i]=0;
		if(dfs(y))ans++;else{
			f[x][y]=1;f[y][x]=-1;
			init(x,y);
		}
	}
	printf("%d",ans);
}

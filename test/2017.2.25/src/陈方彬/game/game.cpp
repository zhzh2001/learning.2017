#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cmath>
#define Ll long long
using namespace std;
struct tree{
	int xx,yy,ma,mi;
}T[4000001];
int a[4000001];
int n,x,y,z,l,r,ans,ma,mi,mma,mmi;
void maketree(int x,int y,int num){
	T[num].xx=x;T[num].yy=y;
	if(x==y){
		T[num].ma=T[num].mi=a[x];
		return;
	}
	int mid=x+y>>1;
	maketree(x,mid,num<<1);
	maketree(mid+1,y,num<<1|1);
	T[num].ma=max(T[num<<1].ma,T[num<<1|1].ma);
	T[num].mi=min(T[num<<1].mi,T[num<<1|1].mi);
}
int outma(int x,int y,int num){
	if(x<=T[num].xx&&T[num].yy<=y)return T[num].ma;
	int ans=0;num=num<<1;
	if(x<=T[num  ].yy)ans=max(ans,outma(x,y,num  ));
	if(y>=T[num+1].xx)ans=max(ans,outma(x,y,num+1));
	return ans;
}
int outmi(int x,int y,int num){
	if(x<=T[num].xx&&T[num].yy<=y)return T[num].mi;
	int ans=1e9;num=num<<1;
	if(x<=T[num  ].yy)ans=min(ans,outmi(x,y,num  ));
	if(y>=T[num+1].xx)ans=min(ans,outmi(x,y,num+1));
	return ans;
}
int findl(int x,int y,int z){
	int mid,ans=y;
	while(y-x>3){
		mid=(x+y)/2;
		if(a[mid]>z){x=mid+1; continue;}
		int xx=outma(x,mid-1,1);
		int yy=outma(mid+1,y,1);
		if(yy>z){x=mid+1;continue;}
		if(xx<=z)return x;
		if(xx>z){
			ans=min(mid,ans);
			y=mid-1;
		}
	}
	for(int i=y;i>=x;i--)if(a[i]>z)return ans;else ans=i;
	return ans;
}
int findr(int x,int y,int z){
	int mid,ans=x;
	while(y-x>3){
		mid=(x+y)/2;
		if(a[mid]>z){y=mid-1; continue;}
		int xx=outma(x,mid-1,1);
		int yy=outma(mid+1,y,1);
		if(xx>z){y=mid-1;continue;}
		if(yy<=z)return y;
		if(yy>z){
			ans=max(mid,ans);
			x=mid+1;
		}
	}
	for(int i=x;i<=y;i++)if(a[i]>z)return ans;else ans=i;
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	maketree(1,n,1);
	for(int i=1;i<=n;i++){
		if(i==1||i==n||a[i-1]>a[i]||a[i]<a[i+1]){printf("-1\n");continue;}
		l=findl(1,i-1,a[i]);
		r=findr(i+1,n,a[i]);
		ma=outma(l,i-1,1);
		mma=outma(i+1,r,1);
		mi=outmi(l,i-1,1);
		mmi=outmi(i+1,r,1);
		ans=max(ma-mmi,mma-mi);
		printf("%d\n",ans);
	}
}

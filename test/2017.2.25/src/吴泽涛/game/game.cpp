#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
const int nn=1000116,inf = 700000000;
struct node {
	int ma,wei ;
};
struct data{
	int l,r,ma,mi;
}tree[nn*4] ;
node front[nn],back[nn] ;
int a[nn],n,x,y,l,r,ans;

inline void build(int l,int r,int root)
{
	tree[root].l = l;
	tree[root].r = r; 
	if(l==r) 
	{
		tree[root].ma=tree[root].mi= a[ l ] ;
		return ; 
	}
	int mid = (l+r)/2;
	build(l,mid,root*2) ;
	build(mid+1,r,root*2+1) ;
	tree[root].ma=max(tree[root*2].ma,tree[root*2+1].ma) ;
	tree[root].mi=min(tree[root*2].mi,tree[root*2+1].mi) ;
	return ;
}

inline int querymax(int l,int r,int root) 
{
	if(l==tree[root].l&&r==tree[root].r) 
	{
		return tree[root].ma;
	}
	int mid = (tree[root].l+tree[root].r) /2;
	if(r<=mid) return querymax(l,r,root*2) ;
	if(l>mid)  return querymax(l,r,root*2+1) ;
	int x1=querymax(l,mid,root*2) ;
	int x2=querymax(mid+1,r,root*2+1) ;
	return max(x1,x2) ;
}

inline int querymin(int l,int r,int root) 
{
	if(l==tree[root].l&&r==tree[root].r) 
	{
		return tree[root].mi;
	}
	int mid = (tree[root].l+tree[root].r)/2;
	if(r<=mid) return querymin(l,r,root*2) ;
	if(l>mid) return querymin(l,r,root*2+1) ;
	int x1 =querymin(l,mid,root*2) ;
	int x2 =querymin(mid+1,r,root*2+1) ;
	return min(x1,x2) ; 
}


int main()
{
	freopen("game.in","r",stdin) ;
	freopen("game.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n;i++) scanf("%d",&a[i]) ;
	front[1].ma = inf;
	front[1].wei =-1;
	for(int i=2;i<=n;i++)
	{
		x=i-1;
		while(1)
		{
			if(a[x]>a[i]) 
			{
				front[i].ma = a[ x ],front[i].wei = x;
				break;
			}
			if(a[i]<front[x].ma) {
				front[i].ma = front[x].ma,front[i].wei = front[x].wei;
				break;
			}
			x= front[x].wei ;
		}
	}
	back[n].ma = inf;
	back[n].wei = -1;
	for(int i=n-1;i>=1;i--)
	{
		x=i+1;
		while(1)
		{
			if(a[i]<a[x]) 
			{
				back[i].ma = a[x],back[i].wei = x;
				break;
			}
			if(a[i]<back[x].ma) 
			{
				back[i].ma=back[x].ma,back[i].wei=back[x].wei;
				break;
			}
			x = back[x].wei;
		}
	}
	
	build(1,n,1) ;
	printf("-1\n") ;
	for(int i=2;i<=n-1;i++)
	{
		l=front[i].wei+1;
		r=front[i].wei-1;
		if(l==i||r==i) 
		{
			printf("-1\n") ;
			continue;
		}
		if(front[i].wei==-1) l=1;
		if(back[i].wei==-1) r=n;
		//ans=querymax(i+1,r,1);
		ans = max(querymax(l,i-1,1)-querymin(i+1,r,1),querymax(i+1,r,1)-querymin(l,i-1,1) ) ;
		printf("%d\n",ans) ;
	}
	printf("-1\n") ;
	return 0;
}



/*
10
1 9 7 6 5 8 7 10 2 8



6
1 9 2 6 9 3

*/







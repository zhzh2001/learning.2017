var i,j,x,y,ans,n,t:longint;
    f:array[0..250,0..250]of boolean;
begin
  assign(input,'stock.in');
  assign(output,'stock.out');
  reset(input);
  rewrite(output);
  readln(n,t);
  for i:=1 to n do f[i,i]:=true;
  for i:=1 to t do
     begin
     readln(x,y);
     if x=y then continue;
     if f[y,x]=false then
	begin
 	for j:=1 to n do
 	   if f[j,x]=true then f[j,y]:=true;
	inc(ans);
	end;
     end;
  writeln(t-ans);
  close(input);
  close(output);
end.

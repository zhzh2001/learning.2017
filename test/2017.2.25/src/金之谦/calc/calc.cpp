#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#define ll unsigned long long
ll l,r,m,p;
using namespace std;
ll mi(ll a,ll b){
    ll x,y;
    x=1; y=a;
    while (b!=0){
        if (b&1==1) x=x*y;
        y=y*y; b=b>>1;
    }
    return x;
}
int main()
{
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	int n;scanf("%d",&n);
	if(n==1){
		char c[100001];
		scanf("%s",&c);
		printf("%s",&c);
		return 0;
	}
	scanf("%I64d",&p);
	l=1;
	if(n>30){printf("1");return 0;}
	else if(n>20)r=4;
	else if(n>10)r=6;
	else if(n==10)r=10;
	else if(n==9)r=20;
	else if(n==8)r=25;
	else if(n==7)r=30;
	else if(n==6)r=50;
	else if(n==5)r=100;
	else if(n==4)r=1000;
	else if(n==3)r=10000;
	else if(n==2)r=1000000;
	while(l<r){
		m=(l+r)/2;
		if(mi(m,n)>=p)r=m;
		else l=m+1;
	}
	printf("%I64d",r);
}

#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
using namespace std;
int n,a[1000001],q[1000001]={0},h[1000001]={0};
int t1[4000001],t2[4000001];
int s[1000001]={0};
inline void build(int l,int r,int nod){ 
	if(l==r)t1[nod]=t2[nod]=a[l];
	else{
		int m=(l+r)/2;
		build(l,m,nod*2);
		build(m+1,r,nod*2+1);
		t1[nod]=min(t1[nod*2],t1[nod*2+1]);
		t2[nod]=max(t2[nod*2],t2[nod*2+1]);
	}
}
inline int s1(int l,int r,int i,int j,int nod){
	if(l==i&&r==j)return t1[nod];
	int m=(l+r)/2;
	if(j<=m)return s1(l,m,i,j,nod*2);
	if(i>=m+1)return s1(m+1,r,i,j,nod*2+1);
	int le=s1(l,m,i,m,nod*2),re=s1(m+1,r,m+1,j,nod*2+1);
	return min(le,re);
}
inline int s2(int l,int r,int i,int j,int nod){
	if(l==i&&r==j)return t2[nod];
	int m=(l+r)/2;
	if(j<=m)return s2(l,m,i,j,nod*2);
	if(i>=m+1)return s2(m+1,r,i,j,nod*2+1);
	int le=s2(l,m,i,m,nod*2),re=s2(m+1,r,m+1,j,nod*2+1);
	return max(le,re);
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	build(1,n,1);
	s[0]=1;s[1]=1;
	for(int i=2;i<=n;i++){
		while(a[s[s[0]]]<a[i]&&s[0]>0)h[s[s[0]]]=i,s[0]--;
		s[0]++;s[s[0]]=i;
	}
	while(s[0]>0)h[s[s[0]]]=n+1,s[0]--;
	memset(s,0,sizeof s);
	s[0]=1;s[1]=n;
	for(int i=n-1;i>0;i--){
		while(a[s[s[0]]]<a[i]&&s[0]>0)q[s[s[0]]]=i,s[0]--;
		s[0]++;s[s[0]]=i;
	}
	while(s[0]>0)q[s[s[0]]]=0,s[0]--;
	for(int i=1;i<=n;i++){
		if(q[i]+1==i||h[i]-1==i){printf("-1\n");continue;}
		int l=q[i]+1,r=h[i]-1;
		int qx=s1(1,n,l,i-1,1),qd=s2(1,n,l,i-1,1),hx=s1(1,n,i+1,r,1),hd=s2(1,n,i+1,r,1);
		printf("%d\n",max(abs(qd-hx),abs(qx-hd)));
	}
	return 0;
}

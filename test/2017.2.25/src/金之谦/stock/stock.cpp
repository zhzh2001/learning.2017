#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
using namespace std;
int n,m,nedge=0,p[200001],next[200001],head[200001];
int b[2001]={0};
inline void addedge(int a,int b){
	nedge++;
	p[nedge]=b;
	next[nedge]=head[a];
	head[a]=nedge;
}
inline bool dfs(int x,int s,int r){
	if(x==s)return 1;
	bool flag=0;b[x]=r;
	int k=head[x];
	while(k){
		int now=p[k];
		if(b[now]!=r){
			flag=dfs(now,s,r);
			if(flag)return 1;
		}
		k=next[k];
	}
	return 0;
}
int main()
{
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	scanf("%d%d",&n,&m);
	int ans=0;
	for(int i=1;i<=m;i++){
		int x,y;scanf("%d%d",&x,&y);
		if(dfs(y,x,i))ans++;
		else addedge(x,y);
	}
	printf("%d",ans);
	return 0;
}

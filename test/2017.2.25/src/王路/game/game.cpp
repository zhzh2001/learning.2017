#include <iostream>
#include <cstdio>
#include <cstring>
#include <ctime>
using namespace std;

const int maxn = 1000005;
int n, data[maxn], prv[maxn], cnt;
int mn[maxn], mx[maxn], nxt[maxn], tot;
const int inf = 0x3f3f3f3f;

inline void read(int &x){
	char ch;int bo=0;x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar())if (ch=='-')bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo)x=-x;
}

inline void write(int x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}

int main() {
	freopen("game.in", "r", stdin);
	freopen("game.out", "w", stdout);
	read(n);
	data[0] = data[n+1] = inf;
	memset(mn, 0x3f, sizeof mn);
	for (int i = 1; i <= n; ++i) {
		read(data[i]);
		if (data[i-1] > data[i]) prv[i] = cnt = 0; else prv[i] = ++cnt;
		if (prv[i] != 0) mn[i] = data[i-prv[i]], mx[i] = data[i-1];
	}
//	for (int i = 1; i <= n; i++) printf("%d ", prv[i]);
	for (int i = n; i >= 1; --i) {
		if (data[i+1] > data[i]) nxt[i] = cnt = 0; else nxt[i] = ++cnt;
		if (nxt[i] != 0) mn[i] = min(mn[i], data[i+nxt[i]]), mx[i] = max(mx[i], data[i+1]);
	}
	for (int i = 1; i <= n; ++i) {
		tot = (!nxt[i] || !prv[i]) ? 0 : nxt[i] + prv[i];
		if (tot == 0) puts("-1");
			else { write(mx[i]-mn[i]); puts(""); }
	}
//	cout << "program time:" << clock() << endl; //800 ~ 1300
	return clock();
}
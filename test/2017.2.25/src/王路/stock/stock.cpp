#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <ctime>
using namespace std;
const int maxn = 255;
int n, m, u, v, ans;
bool f[maxn][maxn];

inline void read(int &x){
	char ch;int bo=0;x=0;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar())if (ch=='-')bo=1;
	for (;ch>='0'&&ch<='9';x=x*10+ch-'0',ch=getchar());
	if (bo)x=-x;
}

int main() {
	freopen("stock.in", "r", stdin);
	freopen("stock.out", "w", stdout);
	read(n), read(m);
	for (int i = 1; i <= m; i++) {
		read(u), read(v);
		if (!f[v][u] && u != v) {
			f[u][v] = true;
			for (int i = 1; i <= n; i++) f[u][i] |= f[v][i];
			for (int i = 1; i <= n; i++) f[i][v] |= f[i][u];
		} else {
		//	printf("%d->%d\n", u, v);
			ans++; continue;
		}
	}
	printf("%d\n", ans);
//	printf("program time:%d\n", clock());
//	return clock();
}

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

ll x;

ll mul(ll x, ll y) {
	ll t = x;
	while (y--) 
		x *= t;
	return x;
}

int main() {
	freopen("calc.in", "r", stdin);
	freopen("calc.out", "w", stdout);
	ios::sync_with_stdio(false);
	int n;
	cin >> n >> x;
	double ans = pow((double)x, (double)1/(double)n);
	cout.precision(0);
	cout << fixed << ans - 0.5 << endl;
}
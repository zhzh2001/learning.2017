#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int s;
int n;
inline int check(int x,int y)
{
	if (x==y)	return 0;
	if(x<y)	return 1;
	return 2;
}
inline int inc(int x)
{
	return x+1;
}
inline int dec(int x)
{
	return x-1;
}
inline int divi(int x,int y)
{
	return (x+y)/2;
}
inline int mul(int x,int y)
{
	return x*y;
}
inline int ksm(int x,int m)
{
	if(m==1)	return x;
	if(m==0)	return 1;
	int t=ksm(x,m/2);
	t=mul(t,t);
	if(m&1)	return mul(t,x);else	return t; 
}
inline void out(int x)
{
	cout<<x<<endl;
}
int main()
{
	std::ios::sync_with_stdio(false);
	cin>>n;
	cin>>s;
//	change(s);
	int l=1,r=s,mid=0;
	while(check(l,r))
	{
		mid=divi(l,r);
		if(check(ksm(mid,n),s)==0)	{out(mid);return 0;		}
		if(check(ksm(mid,n),s)==1)	
			l=inc(mid);else r=dec(mid);
	}
	if(check(ksm(l,n),s)==2)	out(dec(l));else out(l);
}

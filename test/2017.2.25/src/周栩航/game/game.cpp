#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
struct node
{
	int l,r;
}ask[1000001];
int n,a[1000001],map[1000001],q[1000001],tr1[4000001],tr2[4000001],M;
inline void up1(int x)
{
	tr1[x]=max(tr1[x<<1],tr1[x<<1|1]);
}
inline void up2(int x)
{
	tr2[x]=min(tr2[x<<1],tr2[x<<1|1]);
}
inline void build1()
{
	for(M=1;M<=n+1;M<<=1);
	for(int j=M+1;j<=M+n;j++)
	{
		int t;
		tr1[j]=a[j-M];
	}
	for(int j=M-1;j;j--)
		up1(j);
}
inline void build2()
{
	for(M=1;M<=n+1;M<<=1);
	for(int j=M+1;j<=M+n;j++)
	{
		int t;
		tr2[j]=a[j-M];
	}
	for(int j=M-1;j;j--)
		up2(j);
}
inline int  query1(int s,int t)
{
	int ans=0;
	s=s+M-1;t=t+M+1;
	for(;s^t^1;s>>=1,t>>=1)
	{
		if(~s&1)ans=max(ans,tr1[s^1]);
		if(t&1)	ans=max(ans,tr1[t^1]);
	}
	return ans;
}
inline int  query2(int s,int t)
{
	int ans=1e9;
	s=s+M-1;t=t+M+1;
	for(;s^t^1;s>>=1,t>>=1)
	{
		if(~s&1)ans=min(ans,tr2[s^1]);
		if(t&1)	ans=min(ans,tr2[t^1]);
	}
	return ans;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	int r=1;
	ask[1].l=1;
	map[1]=1;
	q[r]=a[1];
	for(int i=2;i<=n;i++)
	{
		int zz=r,cnt=0;
		while(r&&a[i]>=q[r])
		{
			ask[map[r]].r=map[zz];
			r--;
		}
		r++;

		q[r]=a[i];
		map[r]=i;	
	}
	int zz=r;	
	while(r)
	{
		ask[map[r]].r=map[zz];
		r--;
	}
	r=1;
	ask[n].l=1;
	map[1]=n;
	q[r]=a[n];
	for(int i=n-1;i>=1;i--)
	{
		int zz=r,cnt=0;
		while(r&&a[i]>=q[r])
		{
			ask[map[r]].l=map[zz];
			r--;
		}
		r++;
		q[r]=a[i];
		map[r]=i;
	}	
	zz=r;
	while(r)
	{
		ask[map[r]].l=map[zz];
		r--;
	}
	build1();build2();
	for(int i=1;i<=n;i++)
	{
	//	printf("%d %d %d %d\n",ask[i].l,ask[i].r,i,a[i]);
		if(ask[i].l==i||ask[i].r==i)	{printf("-1\n");continue;}
		printf("%d\n",max(query1(ask[i].l,i-1)-query2(i+1,ask[i].r),query1(i+1,ask[i].r)-query2(ask[i].l,i-1)));
	}
}

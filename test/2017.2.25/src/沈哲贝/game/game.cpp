#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define inf 1100000000
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 4000010
#define mod 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
struct data{
	ll id,v;
}a[maxn];
ll fa[maxn],mn[maxn],mx[maxn],h[maxn],answ[maxn],l[maxn],r[maxn];
ll n,j;
bool cmp(data x,data y){
	if (x.v==y.v)	return x.id<y.id;
	return x.v<y.v;
}
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);}
void build(ll l,ll r,ll p){
	if (l==r){
		mn[p]=mx[p]=h[l];
		return;
	}
	ll mid=(l+r)>>1;
	build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
	mn[p]=min(mn[p<<1],mn[p<<1|1]);	mx[p]=max(mx[p<<1],mx[p<<1|1]);
}
ll ask_mx(ll l,ll r,ll p,ll x,ll y){
	if (l==x&&r==y)	return mx[p];
	ll mid=(l+r)>>1;
	if (y<=mid)	return ask_mx(l,mid,p<<1,x,y);
	else	if (x>mid)	return ask_mx(mid+1,r,p<<1|1,x,y);
	else	return min(ask_mx(l,mid,p<<1,x,mid),ask_mx(mid+1,r,p<<1|1,mid+1,y));
}
ll ask_mn(ll l,ll r,ll p,ll x,ll y){
	if (l==x&&r==y)	return mn[p];
	ll mid=(l+r)>>1;
	if (y<=mid)	return ask_mn(l,mid,p<<1,x,y);
	else	if (x>mid)	return ask_mn(mid+1,r,p<<1|1,x,y);
	else	return min(ask_mn(l,mid,p<<1,x,mid),ask_mn(mid+1,r,p<<1|1,mid+1,y));
}
void merge(ll x,ll y){
	ll a=find(x),b=find(y);
	if (a==b)	return;
	fa[b]=a;
	l[a]=min(l[a],l[b]);
	r[a]=max(r[a],r[b]);
}
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();
	For(i,1,n)	h[i]=a[i].v=read(),a[i].id=i,fa[i]=i,l[i]=r[i]=i;
	sort(a+1,a+n+1,cmp);
	a[0].v=a[n+1].v=-inf;
	build(1,n,1);
	for(ll i=1;i<=n;i=j+1){
		j=i;
		while(a[j+1].v==a[i].v)	j++;
		For(k,i,j){
			if (h[a[k].id-1]<=h[a[k].id]&&a[k].id>1)	merge(a[k].id-1,a[k].id);
			if (h[a[k].id+1]<=h[a[k].id]&&a[k].id<n)	merge(a[k].id+1,a[k].id);
		}
		For(k,i,j)	if (l[find(a[k].id)]==a[k].id||r[find(a[k].id)]==a[k].id)	answ[a[k].id]=-1;
		else	answ[a[k].id]=max(ask_mx(1,n,1,l[find(a[k].id)],a[k].id-1)-ask_mn(1,n,1,a[k].id+1,r[find(a[k].id)]),
		ask_mx(1,n,1,a[k].id+1,r[find(a[k].id)])-ask_mn(1,n,1,l[find(a[k].id)],a[k].id-1));
	}
	For(i,1,n)	writeln(answ[i]);
}

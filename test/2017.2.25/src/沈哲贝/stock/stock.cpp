#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 310
#define mod 200000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll n,T,ans;
ll mp[maxn][maxn];
int main(){
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout); 
	n=read();	T=read();
	while(T--){
		ll x=read(),y=read();
		if (x==y){	ans++;	continue;	}
		if (mp[x][y])	continue;
		if (mp[y][x]){	ans++;	continue;	}
		mp[x][y]=1;
		For(i,1,n)
		if (mp[i][x]){
			For(j,1,n)
			if (mp[y][j])	mp[i][j]=1,mp[x][j]=1;
			mp[i][y]=1;
		}
	}
	writeln(ans);
}

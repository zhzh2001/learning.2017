#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll> 
#define inf 100000000
#define maxn 40010 
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll ans[maxn],a[maxn],b[maxn],c[maxn],d[maxn];
ll n,minn,m;
char ch[maxn];
void cheng(ll a[],ll b[]){
	For(i,1,ans[0])	ans[i]=0; 
	ans[0]=a[0]+b[0]-1;
	For(i,1,a[0])	For(j,1,b[0]){
		ans[i+j-1]+=a[i]*b[j];
		ans[i+j]+=ans[i+j-1]/10;
		ans[i+j-1]%=10;
	}
	while (ans[ans[0]+1])	ans[0]++;
	For(i,0,ans[0])	a[i]=ans[i];
	FOr(i,ans[0],0)	ans[i]=0;
}
bool bijiao(ll a[],ll b[]){
	if (a[0]<b[0])	return 1;
	if (a[0]>b[0])	return 0;
	FOr(i,a[0],1)	if (a[i]>b[i])	return 0;
	else if (a[i]<b[i])	return 1;
	while (!a[a[0]]&&a[0]>1)	a[0]--;
	FOr(i,a[0],1)	write(a[i]);
	exit(0);
}
bool pd(){
	For(i,0,b[0])	c[i]=d[i]=b[i];
	ll tt=m-1;
	while(tt){
		if (tt&1)	cheng(c,d);
		cheng(d,d);
		tt>>=1;
	}
	ll ans=bijiao(c,a);
	FOr(i,c[0],0)	c[i]=0;
	FOr(i,d[0],0)	d[i]=0;
	return ans;
}
int main(){
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	m=read();
	scanf("%s",ch+1);
	n=strlen(ch+1);
	For(i,1,n)	a[n-i+1]=ch[i]-'0';
	a[0]=n;
	b[0]=minn=n/m+1;
	FOr(i,minn,1){
		ll l=0,r=9,ans=0;
		while (l<=r){
			ll mid=(l+r)>>1;
			b[i]=mid;
			if (pd())	ans=mid,l=mid+1;
			else	r=mid-1;
		}
		b[i]=ans;
		if(i==minn&&!b[minn])	b[0]=max(1,b[0]-1);
	}
	while (!b[b[0]]&&b[0]>1)	b[0]--;
	FOr(i,b[0],1)	write(b[i]);
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int a[1000010],Max[1010];
int pos[1000010],block,num_b;
int n;
int main(){
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	n=read();
	block=sqrt(n);
	num_b=(n-1)/block+1;
	for(int i=1;i<=n;i++){
		a[i]=read();
		pos[i]=(i-1)/block+1;
		if(Max[pos[i]]<a[i])
			Max[pos[i]]=a[i];
	}
	int ml,mr,q;
	printf("-1\n");
	bool flag_1,flag_2;
	for(int i=2;i<=n-1;i++){
		if(a[i-1]>a[i]||a[i+1]>a[i]){
			printf("-1\n");
			continue;
		}
		ml=a[i-1];
		mr=a[i+1];
		flag_1=true;
		flag_2=true;
		if(Max[pos[i]]>a[i]){
			for(int j=i-2;j>=(pos[i]-1)*block+1;j--){
				if(a[j]>a[i]){
					flag_1=false;
					break;
				}
				ml=a[j];
			}
			if(n<pos[i]*block) q=n;
			else q=pos[i]*block;
			for(int j=i+2;j<=q;j++){
				if(a[j]>a[i]){
					flag_2=false;
					break;
				}
				mr=a[j];
			}
		}
		for(int j=pos[i]-1;j>=1;j--){
			if(!flag_1) break;
			if(Max[j]<=a[i]) continue;
			for(int z=j*block;z>=(j-1)*block+1;z--){
				if(a[z]>a[i]){
					flag_1=false;
					break;
				}
				ml=a[z];
			}
		}
		if(flag_1)
			ml=a[1];
		for(int j=pos[i]+1;j<=num_b;j++){
			if(!flag_2) break;
			if(Max[j]<=a[i]) continue;
			if(n<j*block) q=n;
			else q=j*block;
			for(int z=(j-1)*block+1;z<=q;z++){
				if(a[z]>a[i]){
					flag_2=false;
					break;
				}
				mr=a[z];
			}
		}
		if(flag_2) mr=a[n];
		if(ml>mr) printf("%d\n",ml-mr);
		else printf("%d\n",mr-ml);
	}
	printf("-1\n");
	return 0;
}

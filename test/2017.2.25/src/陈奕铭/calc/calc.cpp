#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
#define LL long long
inline LL read(){
	LL x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
LL n,k;
LL mi(LL x,LL n){
	if(n==1)
		return x;
	LL ans=mi(x,n/2);
	ans=ans*ans;
	if(n%2==0) return ans;
	else return ans*x;
}
LL erfen(){
	LL l=1,r=1000000;
	LL mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(mi(mid,k)>n) r=mid-1;
		else l=mid+1;
	}
	return r;
}
int main(){
	freopen("calc.in","r",stdin);
	freopen("calc.out","w",stdout);
	char s[60000];
	k=read();scanf("%s",s);
	if(k==1){
		printf("%s",s);
		return 0;
	}
	for(int i=0;i<strlen(s);i++)
		n=n*10+s[i]-'0';
	if(k==2){
		n=sqrt(n);
		cout<<n;
		return 0;
	}
	LL i=erfen();
	LL j=i+1,z=i-1;
	LL a,b,c;
	a=abs(mi(i,k)-n);b=abs(mi(j,k)-n);c=abs(mi(z,k)-n);
	if(a<b){
		if(a<c) cout<<i;
		else	cout<<z;
	}
	else{
		if(b<c) cout<<j;
		else	cout<<z;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
#include<cstring>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int f[300];
vector<int> son[300];
int n,t,ans;
int Max_num,num,a_num;
bool Map[300][300];
bool vis[300][300];
int x[100010],y[100010];
int main(){
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	n=read();t=read();
	Max_num=n*(n-1)/2;
	for(int i=1;i<=t;i++){
		x[i]=read();y[i]=read();
	}
	for(int i=1;i<=t;i++){
		if(Map[y[i]][x[i]])
			ans++;
		else{
			a_num++;
			if(!vis[x[i]][y[i]]){
				num++;
				vis[x[i]][y[i]]=true;
				if(num>=Max_num){
					ans=t-a_num;
					break;
				}
			}
			Map[x[i]][y[i]]=true;
			son[y[i]].push_back(x[i]);
			for(int j=0;j<son[x[i]].size();j++){
				Map[son[x[i]][j]][y[i]]=true;
				son[y[i]].push_back(son[x[i]][j]);
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
const int N=310,M=100010;
int i,cnt,ans,n,m,fm,dn,cof[N][N],pt[N],next[M],v[M];
inline void dfs(int u){
	for(int i=pt[u];i;i=next[i])
		cof[v[i]][dn]=1,dfs(v[i]);
}
int main()
{
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	scanf("%d%d",&n,&m);ans=0;
	for(i=1;i<=n;i++)cof[i][i]=1;
	while(m--){
		scanf("%d%d",&fm,&dn);
		if(cof[dn][fm])ans++;
		else {
			if(cof[fm][dn]==0){
				v[++cnt]=fm,next[cnt]=pt[dn],pt[dn]=cnt;
				cof[fm][dn]=1,dfs(dn);
			}
		}
	}printf("%d\n",ans);
}

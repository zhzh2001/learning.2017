#include<algorithm>
#include<cstdio>
#include<cmath>
#include<string.h>
using namespace std;
const int N=1000010,K=21;
int fa[N][K],fi[N][K],n,i,lim,x,xx1,xx2,yy1,yy2,k,fl,mid;
char ch[15],c;int l,xx;
inline int R(){
	l=1,xx=0,c=getchar();
	while(c>'9'||c<'0'){if(c=='-')l=-1;c=getchar();}
	while(c<='9'&&c>='0')xx=xx*10+c-'0',c=getchar();
	return l*xx;
}
inline void Out(int n){
	l=0;
	while(n)ch[++l]=(n%10)+'0',n/=10;
	//if(!l)putchar('0');
	while(l)putchar(ch[l--]);
	puts("");
}
inline int abs(int a){return a<0?-a:a;}
inline void repa(int l,int r,int &ma,int &mi){
	k=log2(r-l),ma=max(fa[l][k],fa[r-(1<<k)+1][k]),mi=min(fi[l][k],fi[r-(1<<k)+1][k]);
}
inline int chk(int l,int r){
	fl=0;
	if(r<l)return 0;
	while(l<=r){
		mid=l+r>>1;
		if(r<i){
			k=log2(i-mid);
			if(max(fa[mid][k],fa[i-(1<<k)+1][k])>fa[i][0])l=mid+1;
			else r=mid-1,fl=mid;
		}else 
		if(l>i){
			k=log2(mid-i);
			if(max(fa[i][k],fa[mid-(1<<k)+1][k])>fa[i][0])r=mid-1;
			else l=mid+1,fl=mid;
		}
	}return fl;
}
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)x=R(),fa[i][0]=fi[i][0]=x;
	for(lim=log2(n),k=1;k<=lim;k++)
		for(i=1;i<=n-(1<<k)+1;i++)
			fa[i][k]=max(fa[i][k-1],fa[i+(1<<(k-1))][k-1]),
			fi[i][k]=min(fi[i][k-1],fi[i+(1<<(k-1))][k-1]);
	for(puts("-1"),i=2;i<n;i++){
		x=chk(1,i-1);
		if(x==0){puts("-1");continue;}
		repa(x,i-1,xx1,yy1);
		x=chk(i+1,n);
		if(x==0){puts("-1");continue;}
		repa(i+1,x,xx2,yy2);
		Out(max(abs(xx1-yy2),abs(xx2-yy1)));
	}puts("-1");
}

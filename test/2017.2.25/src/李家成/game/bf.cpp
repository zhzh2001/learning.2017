#include<cstdio>
#include<algorithm>
int n,i,a[1000010],j,ma1,ma2,mi1,mi2,f1,f2;
inline int abs(int a){return a<0?-a:a;}
int main()
{
	freopen("game.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)scanf("%d",&a[i]);
	for(i=1;i<=n;i++){
		ma1=ma2=(-1e9)+1;
		mi1=mi2=(1e9)-1;
		for(f1=0,j=i-1;j>0;j--)
			if(a[j]>a[i])break;
			else {if(a[j]>ma1)f1=1,ma1=a[j];if(a[j]<mi1)mi1=a[j],f1=1;}
		for(f2=0,j=i+1;j<=n;j++)
			if(a[j]>a[i])break;
			else {
				if(a[j]>ma2)f2=1,ma2=a[j];
				if(a[j]<mi2)f2=1,mi2=a[j];
			}
		if((!f2)||(!f1))puts("-1");
		else printf("%d\n",std::max(abs(ma1-mi2),abs(ma2-mi1)));
	}
}

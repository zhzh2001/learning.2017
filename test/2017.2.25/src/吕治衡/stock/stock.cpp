#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
int n,m,x,y,ans;
bool flag[1000][1000];
int main()
{
	freopen("stock.in","r",stdin);freopen("stock.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	for (int i=1;i<=n;i++)flag[i][i]=true;
	for (int i=1;i<=m;i++)
	{
		cin>>x>>y;
		if (flag[y][x]){ans++;continue;}
		for (int j=1;j<=n;j++)
			flag[j][y]=flag[j][y]||flag[j][x],
			flag[x][j]=flag[x][j]||flag[y][j];
	}
	cout<<ans;
}

#include<fstream>
#include<cstring>
#include<ctime>
using namespace std;
ifstream fin("stock.in");
ofstream fout("stock.out");
const int N=255;
bool mat[N][N];
int l1[N],l2[N];
int main()
{
	int n,m;
	fin>>n>>m;
	int ans=0;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		if(u==v||mat[v][u])
			ans++;
		else
		{
			int c1=1;
			l1[1]=u;
			for(int i=1;i<=n;i++)
				if(mat[i][u]&&!mat[i][v])
					l1[++c1]=i;
			int c2=1;
			l2[1]=v;
			for(int i=1;i<=n;i++)
				if(mat[v][i]&&!mat[u][i])
					l2[++c2]=i;
			for(int i=1;i<=c1;i++)
				for(int j=1;j<=c2;j++)
					mat[l1[i]][l2[j]]=true;
		}
	}
	fout<<ans<<endl;
	return 0;
}
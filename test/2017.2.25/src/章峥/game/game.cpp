#include<cstdio>
#include<algorithm>
#include<cctype>
#include<limits>
using namespace std;
const int N=1000005,SIZE=4000000;
FILE *fin,*fout;
int a[N],mn[N],mx[N],rmn[N],rmx[N],f[N],g[N];
char buf[SIZE],*p1=buf+SIZE,*pend=buf+SIZE;
inline int nc()
{
	if (p1==pend)
	{
		p1=buf;
		pend=buf+fread(buf,1,SIZE,fin);
		if (pend==p1)
			return EOF;
	}
	return *p1++;
}
inline void read(int& x)
{
	int c;
	while(isspace(c=nc()));
	int sign=1;
	if(c=='-')
		sign=-1,c=nc();
	x=0;
	for(;isdigit(c);c=nc())
		x=x*10+c-'0';
	x*=sign;
}
inline void out(char ch)
{
	if (p1==pend)
	{
		fwrite(buf,1,SIZE,fout);
		p1=buf;
	}
	*p1++=ch;
}
int tmp[15];
inline void write(int x)
{
	if(x<0)
		out('-'),x=-x;
	int p=0;
	do
		tmp[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		out(tmp[p]+'0');
}
int main()
{
	fin=fopen("game.in","r");
	fout=fopen("game.out","w");
	int n;
	read(n);
	for(int i=1;i<=n;i++)
		read(a[i]);
	
	for(int i=1;i<=n;i++)
	{
		mn[i]=numeric_limits<int>::max();mx[i]=numeric_limits<int>::min();
		int j;
		for(j=i-1;j&&a[j]<=a[i];j=f[j]-1)
		{
			mn[i]=min(mn[i],min(a[j],mn[j]));
			mx[i]=max(mx[i],max(a[j],mx[j]));
		}
		f[i]=j+1;
	}
	
	for(int i=n;i;i--)
	{
		rmn[i]=numeric_limits<int>::max();rmx[i]=numeric_limits<int>::min();
		int j;
		for(j=i+1;j<=n&&a[j]<=a[i];j=g[j]+1)
		{
			rmn[i]=min(rmn[i],min(a[j],rmn[j]));
			rmx[i]=max(rmx[i],max(a[j],rmx[j]));
		}
		g[i]=j-1;
	}
	
	p1=buf;pend=buf+SIZE;
	for(int i=1;i<=n;i++)
	{
		if(f[i]==i||g[i]==i)
			write(-1);
		else
			write(max(mx[i]-rmn[i],rmx[i]-mn[i]));
		out('\n');
	}
	fwrite(buf,1,p1-buf,fout);
	return 0;
}
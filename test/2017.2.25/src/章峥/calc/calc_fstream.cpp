#include<fstream>
#include<algorithm>
#include<cstring>
#include<string>
#include<sstream>
#include<cstdio>
using namespace std;
ifstream fin("calc.in");
ofstream fout("calc.out");
struct bigint
{
	typedef long long value_t;
	static const int MAXLEN=2000,BASE=100000000,WIDTH=8;
	int len;
	value_t dig[MAXLEN];
	bigint()
	{
		len=1;
		memset(dig,0,sizeof(dig));
	}
	bigint(int x)
	{
		len=1;
		memset(dig,0,sizeof(dig));
		dig[1]=x;
	}
	void clear()
	{
		while(len&&dig[len]==0)
			len--;
	}
	bigint mypow10(int p)
	{
		len=(p+1)/WIDTH+2;
		memset(dig,0,sizeof(dig));
		dig[len]=1;
		return *this;
	}
	bigint operator+(int x)const
	{
		bigint res;
		res.len=len;
		for(int i=1;i<=len;i++)
		{
			res.dig[i]=dig[i]+x;
			x=res.dig[i]/BASE;
			res.dig[i]%=BASE;
		}
		if(x)
			res.dig[++res.len]=x;
		return res;
	}
	bigint operator+(const bigint& b)const
	{
		bigint res;
		res.len=max(len,b.len);
		int overflow=0;
		for(int i=1;i<=res.len;i++)
		{
			res.dig[i]=dig[i]+b.dig[i]+overflow;
			overflow=res.dig[i]/BASE;
			res.dig[i]%=BASE;
		}
		if(overflow)
			res.dig[++res.len]=overflow;
		return res;
	}
	bigint operator*(const bigint& b)const
	{
		bigint res;
		res.len=len+b.len-1;
		for(int i=1;i<=len;i++)
			for(int j=1;j<=b.len;j++)
				res.dig[i+j-1]+=dig[i]*b.dig[j];
		for(int i=1;i<=res.len;i++)
			if(res.dig[i]>=BASE)
			{
				res.dig[i+1]+=res.dig[i]/BASE;
				res.dig[i]%=BASE;
			}
		if(res.dig[res.len+1])
			res.len++;
		return res;
	}
	bigint operator*=(const bigint& b)
	{
		return *this=*this*b;
	}
	bigint operator/(const int x)const
	{
		bigint res;
		res.len=len;
		int remain=0;
		for(int i=len;i;i--)
		{
			res.dig[i]=(dig[i]+remain)/x;
			remain=(dig[i]+remain)%x*BASE;
		}
		res.clear();
		return res;
	}
	bool operator<(const bigint& b)const
	{
		if(len!=b.len)
			return len<b.len;
		for(int i=len;i;i--)
			if(dig[i]!=b.dig[i])
				return dig[i]<b.dig[i];
		return false;
	}
	bool operator<=(const bigint& b)const
	{
		return !(b<*this);
	}
};
istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b=bigint();
	b.len=s.length()/bigint::WIDTH+1;
	for(int i=1;i<=b.len;i++)
	{
		stringstream ss(s.substr(max((int)s.length()-i*bigint::WIDTH,0),i<b.len?bigint::WIDTH:s.length()-(i-1)*bigint::WIDTH));
		ss>>b.dig[i];
	}
	b.clear();
	return is;
}
ostream& operator<<(ostream& os,bigint& b)
{
	for(int i=b.len;i;i--)
	{
		static char buf[10];
		sprintf(buf,i==b.len?"%I64d":"%08I64d",b.dig[i]);
		os<<string(buf);
	}
	return os;
}
bigint qpow(bigint& base,int p)
{
	bigint now=base,ans=1;
	do
	{
		if(p&1)
			ans*=now;
		if(p/2)
			now*=now;
	}
	while(p/=2);
	return ans;
}
int main()
{
	int m;
	bigint n;
	fin>>m>>n;
	if(m==1)
	{
		fout<<n<<endl;
		return 0;
	}
	bigint l,r=bigint().mypow10(n.len*bigint::WIDTH/m+1);
	while(l+1<r)
	{
		bigint mid=(l+r)/2;
		if(qpow(mid,m)<=n)
			l=mid;
		else
			r=mid;
	}
	if(qpow(r,m)<=n)
		fout<<r<<endl;
	else
		fout<<l<<endl;
	return 0;
}
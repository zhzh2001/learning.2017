#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,x,mi1,mi2,mx1,mx2,ans;
int  a[1000100]; 
int main()
{
	freopen("game.in","r",stdin);
	freopen("game.out","w",stdout);
	scanf("%d",&n);
    For(i,1,n)
    {
        scanf("%lld",&a[i]);
    }
    cout<<"-1"<<endl;
    For(i,2,n-1)
    {
    	if (a[i-1]>a[i]||a[i+1]>a[i])
		{
    		cout<<"-1"<<endl;
			continue;
    	}
    	mi1=a[i-1];
    	mx1=a[i-1];
    	x=i-2;
    	while (a[x]<=a[i]&&x>0)
		{
			mi1=min(mi1,a[x]);
			mx1=max(mx1,a[x]);
			x--;
		}
		mi2=a[i+1];
    	mx2=a[i+1];
    	x=i+2;
    	while (a[x]<=a[i]&&x<n+1)
		{
			mi2=min(mi2,a[x]);
			mx2=max(mx2,a[x]);
			x++;
		}
		ans=max(mx1-mi2,mx2-mi1);
		cout<<ans<<endl;
    }
    cout<<"-1"<<endl;
	return 0;
}

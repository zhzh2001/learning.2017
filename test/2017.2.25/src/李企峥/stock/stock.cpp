#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int n,m,x,y,ans;
bool f[300][300];
int main()
{
	freopen("stock.in","r",stdin);
	freopen("stock.out","w",stdout);
	scanf("%d%d",&n,&m);
	For(i,1,n)
	{
		For(j,1,n)
		{
			f[i][j]=false;
		}
	}
	For(i,1,m)
	{
		scanf("%d%d",&x,&y);
		if(x==y) 
		{
			ans++;
			continue;
		} 
		if (f[y][x]==true) ans++;
			else
			{
				f[x][y]=true;
				For(i,1,n)
				{
					if(x==i||i==y) continue;
					if (f[i][x]==true) f[i][y]=true;
					if (f[y][i]==true) f[x][i]=true;
				}
			}
	}
	cout<<ans<<endl;
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

FILE *Fin,*Fstd,*Fout,*Fres;
int n,A,B,C,ans_exit;
char a[5005][5005];

void Return(double Src,char Res[])
{
	
	fprintf(Fres,"%lf\n", Src);
	exit(0);
}


int main(int argv,char *argc[])
{
	if (argv!=5) return 1;
	Fin =fopen(argc[1],"r");
	Fstd=fopen(argc[2],"r");
	Fout=fopen(argc[3],"r");
	Fres=fopen(argc[4],"w");
	
	fscanf(Fin,"%d",&n);
	for (int i=1;i<=n;++i) fscanf(Fin,"%s",a[i]+1);
	
	fscanf(Fstd,"%d",&ans_exit);
	
	fscanf(Fout,"%d",&A);
	
	if (ans_exit==-1){
		if (A!=-1) Return(0,"Wrong Answer!");
		else Return(1,"Accepted!");
	}
	
	if (A==-1) Return(0,"Wrong Answer!");
	
	fscanf(Fout,"%d%d",&B,&C);
	
	if (a[A][B]=='1' && a[B][C]=='1' && a[C][A]=='1') Return(1,"Accepted!");
	
	Return(0,"Wrong Answer!");
	return 0;
}

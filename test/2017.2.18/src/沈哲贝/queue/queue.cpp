#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define inf (1LL<<56)
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 1200
#define mod 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll f[220][110][11][2],n1,n2,k1,k2,ans;
int main(){
	freopen("queue.in","r",stdin);
	freopen("queue.out","w",stdout);
	n1=read();	n2=read();	k1=read();	k2=read();
	f[1][1][1][0]=1;
	f[1][0][1][1]=1;
	For(i,1,n1+n2-1){
		For(j,0,min(i,n1)){
			For(k,1,min(j,k1))	f[i+1][j+1][k+1][0]=f[i][j][k][0];
			For(k,1,min(i-j,k2))f[i+1][j][k+1][1]=f[i][j][k][1];
			
			For(k,1,min(j,k1))	f[i+1][j][1][1]=(f[i+1][j][1][1]+f[i][j][k][0])%mod;
			For(k,1,min(i-j,k2))f[i+1][j+1][1][0]=(f[i+1][j+1][1][0]+f[i][j][k][1])%mod;
		}
	}
	For(i,1,k1)	ans=(ans+f[n1+n2][n1][i][0])%mod; 
	For(i,1,k2)	ans=(ans+f[n1+n2][n1][i][1])%mod;
	writeln(ans);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 100010
#define mod 200000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll q[11][maxn],change[11],sum[11],a[maxn],canbe[11],last[11],cost[11],szb[maxn];
ll n,k,tot;
char s[maxn];
bool pd(ll x){
	return x>=0&&x<=9;
}
bool ok(ll x){
	For(i,1,n)
	if (q[x][i]>szb[i])	return 0;
	else	if (q[x][i]<szb[i])	return 1;
	return 0;
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	n=read();	k=read();
	scanf("%s",s+1);
	For(i,1,n)	a[i]=s[i]-'0';
	For(i,1,n)	sum[a[i]]++;
	For(i,0,9){
		ll have=sum[i];
		For(j,1,9){
			change[i]=canbe[i]=0;
			ll x=i-j,y=i+j;
			if (pd(x))	canbe[i]+=sum[x];
			if (pd(y))	canbe[i]+=sum[y];
			if (pd(x)&&(have+sum[x]>=k)){
				cost[i]+=(k-have)*j;
				last[i]=j;
				change[i]=k-have;
				break;
			}
			if (pd(x))	cost[i]+=sum[x]*j,have+=sum[x],change[i]=sum[x];
			if (pd(y)&&(have+sum[y]>=k)){
				cost[i]+=(k-have)*j;
				last[i]=j;
				change[i]+=k-have;
				break;
			}
			if (pd(y))	cost[i]+=sum[y]*j,have+=sum[y];
		}
	}
	ll ans=inf;
	For(i,0,9)	ans=min(ans,cost[i]);
	writeln(ans);
	For(i,0,9)
	if (cost[i]==ans){
		tot++;
		For(j,1,n){
			if (a[j]>(i-last[i])&&a[j]<(i+last[i])){	q[tot][j]=i;continue;	}
			if (a[j]<(i-last[i])||a[j]>(i+last[i])){	q[tot][j]=a[j];continue;	}
			if (change[i]==canbe[i]){
				q[tot][j]=i;
				change[i]--;
				canbe[i]--;
			}
			else
			{
				canbe[i]--;
				if (a[j]==(i+last[i])&&change[i])	q[tot][j]=i,change[i]--;
				else	q[tot][j]=a[j];
			}
		}
	}
	For(i,1,n)	szb[i]=10;
	For(i,1,tot)
	if (ok(i)){
		For(j,1,n)	szb[j]=q[i][j];
	}
	For(i,1,n)	write(szb[i]);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 5010
#define inf 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
bitset<5003>a[5003];
char s[5003];
ll n,q[maxn],fa[maxn];
bool vis[maxn];
void dfs(ll x,ll pre){
	vis[x]=1;
	For(i,1,n)
	if (a[x][i]&&i!=pre){
		if (vis[i]&&fa[fa[x]]==i){
			printf("%d %d %d\n",i,x,fa[x]);
			exit(0);
		}
		else if (!vis[i]){
			fa[i]=x;
			dfs(i,x);
		}
	}
}
int main(){
	freopen("cycle.in","r",stdin);
	freopen("cycle.out","w",stdout);
	n=read();
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,n)	a[i][j]=s[j]=='1';
	}
	if(n<=100){
		For(i,1,n-2)
		For(j,i+1,n-1){
			if (a[i][j])
			For(k,j+1,n)
			if (a[j][k]&&a[k][i]){
				printf("%d %d %d\n",i,j,k);
				return 0;
			}
			if (a[j][i])
			For(k,j+1,n)
			if (a[k][j]&&a[i][k]){
				printf("%d %d %d\n",i,j,k);
				return 0;
			}
		}
		puts("-1");
		return 0;
	}
	For(i,1,n)
	if (!vis[i])	dfs(i,-1); 
	puts("-1");
}

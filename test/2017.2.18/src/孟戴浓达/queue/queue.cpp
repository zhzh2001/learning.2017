//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("queue.in");
ofstream fout("queue.out");
const int mod=100000000;
int n1,n2,k1,k2;
int jiyi[103][103][12][12];
int dp(int nn1,int nn2,int kk1,int kk2){
	int& fanhui=jiyi[nn1][nn2][kk1][kk2];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(kk1>k1||kk2>k2){
			fanhui=0;
			return fanhui;
		}
		if(nn1==0&&nn2==0){
			fanhui=1;
			return fanhui;
		}
		fanhui=0;
		if(nn1-1>=0){
			fanhui+=dp(nn1-1,nn2,kk1+1,0);
			fanhui=fanhui%mod;
		}
		if(nn2-1>=0){
			fanhui+=dp(nn1,nn2-1,0,kk2+1);
			fanhui=fanhui%mod;
		}
	}
	return fanhui;
}
int main(){
	//memset(jiyi,-1,sizeof(jiyi));
	fin>>n1>>n2>>k1>>k2;
	for(int i=0;i<=n1+1;i++){
		for(int j=0;j<=n2+1;j++){
			for(int l=0;l<=k1+1;l++){
				for(int ll=0;ll<=k2+1;ll++){
					jiyi[i][j][l][ll]=-1;
				}
			}
		}
	}
	int ans=dp(n1,n2,0,0);
	fout<<ans;
	//fin.close();
	//fout.close();
	return 0;
}
/*
in1:
2 3 1 2
out1:
5

in2:
2 4 1 1
out2:
0
*/

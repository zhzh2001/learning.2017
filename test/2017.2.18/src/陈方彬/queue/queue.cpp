#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#define Ll long long
using namespace std;
Ll f[201][2][11][101],ans;
int nk,mk,n,m,mo=1e8;
int main()
{
	freopen("queue.in","r",stdin);
	freopen("queue.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&nk,&mk);
	f[1][0][1][1]=f[1][1][1][1]=1;
	for(int i=2;i<=n+m;i++){
		for(int j=2;j<=min(i,nk);j++)
		for(int k=j;k<=min(i,n);k++)f[i][0][j][k]=f[i-1][0][j-1][k-1];
		for(int k=1;k<=min(i,n);k++){
			for(int jj=1;jj<=min(i-k,mk);jj++)
				f[i][0][1][k]+=f[i-1][1][jj][i-k];
		f[i][0][1][k]%=mo;
		}
		for(int j=2;j<=min(i,mk);j++)
		for(int k=j;k<=min(i,m);k++)f[i][1][j][k]=f[i-1][1][j-1][k-1];
		for(int k=1;k<=min(i,m);k++){
			for(int jj=1;jj<=min(i-k,nk);jj++)
				f[i][1][1][k]+=f[i-1][0][jj][i-k];
		f[i][1][1][k]%=mo;
		}
	}
	for(int i=1;i<=nk;i++)ans+=f[n+m][0][i][n];
	for(int i=1;i<=mk;i++)ans+=f[n+m][1][i][m];
	ans%=mo;
	printf("%lld",ans);
}


#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
bool flag=0;
int d[5001][5001];
int vis[5001],n;
inline void check(int x,int y)
{
	for(int i=1;i<=n;i++)
	{
		if(d[i][x]&&d[y][i])	
		{
			cout<<i<<' '<<x<<' '<<y<<endl;
			flag=1;
			return;
		}
	}
}
inline void dfs(int x,int last,int step)
{
	if (flag)	return;
	vis[x]=step;
	for(int i=1;i<=n;i++)
	{
		if(d[x][i]&&!vis[i])	dfs(i,x,step+1);
		if(vis[i]&&d[x][i]&&vis[x]-vis[i]==2)	
			check(x,i);
	}
}
int main()
{
	freopen("cycle.in","r",stdin);
	freopen("cycle.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		string s;
		cin>>s;
		for(int j=1;j<=n;j++)
		d[i][j]=s[j-1]=='1'?1:0;
	}
	for(int i=1;i<=n;i++)
	{
		if(!vis[i])	dfs(i,0,1);
		if(flag)	return 0;
	}
	cout<<"-1"<<endl;
}
/*
5
00100
10000
01001
11101
11000
*/

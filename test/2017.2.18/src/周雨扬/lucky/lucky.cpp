#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#define mo 100000000
using namespace std;
int c[10005],a[10005],b[10],mn[10005];
int k,len,ans;
char s[10005];
bool cmp(int mn[10005],int c[10005]){
	for (int i=len;i>=1;i--){
		if (mn[i]<c[i]) return 1;
		if (mn[i]>c[i]) return 0;
	}
	return 1;
}
void work(int x){
	int l=x,r=x,s=b[x],dj=0;
	if (s>=k){
		ans=0;
		memcpy(mn,a,sizeof(a));
		return;
	}
	for (;;){
		if (r!=9){
			s+=b[++r];
			if (s>=k){
				s-=b[r];
				dj+=(r-x)*(k-s);
				r--;
				if (dj>ans) return;
				memcpy(c,a,sizeof(a));
				for (int i=1;i<=len;i++)
					if (c[i]>=l&&c[i]<=r) c[i]=x;
				for (int i=len;i>=1&&s<k;i--)
					if (c[i]==r+1) c[i]=x,s++;
				if (dj==ans&&cmp(mn,c)) return;
				ans=dj;
				memcpy(mn,c,sizeof(c));
				return;
			}
			else dj+=b[r]*(r-x);
		}
		if (l!=0){
			s+=b[--l];
			if (s>=k){
				s-=b[l];
				dj+=(x-l)*(k-s);
				l++;
				if (dj>ans) return;
				memcpy(c,a,sizeof(a));
				for (int i=1;i<=len;i++)
					if (c[i]>=l&&c[i]<=r) c[i]=x;
				for (int i=1;i<=len&&s<k;i++)
					if (c[i]==l-1) c[i]=x,s++;
				if (dj==ans&&cmp(mn,c)) return;
				ans=dj;
				memcpy(mn,c,sizeof(c));
				return;
			}
			else dj+=b[l]*(x-l);
		}
	}
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
    scanf("%d%d",&len,&k);
	scanf("%s",s);
	ans=len*10;
	for (int i=1;i<=len;i++) a[i]=s[len-i]-48;
	for (int i=1;i<=len;i++) b[a[i]]++;
	for (int i=0;i<=9;i++) work(i);
	printf("%d\n",ans);
	for (int i=len;i>=1;i--) printf("%d",mn[i]); 
}

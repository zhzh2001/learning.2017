#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n1,n2,k1,k2,ans,q[200];
void pd(){
	int t1=0,t2=0;
	for (int i=1;i<=n1+n2;i++){
		if (!q[i]) {
			t1++; t2=0;
		}
		else {
			t1=0; t2++;
		}
		if (t1>k1||t2>k2) return;
	}
	ans++;
}
void dfs(int x,int a,int b){
	if (a>n1||b>n2) return;
	if (x==n1+n2+1){
		pd();
		return;
	}
	q[x]=0;
	dfs(x+1,a+1,b);
	q[x]=1;
	dfs(x+1,a,b+1);
}
int main(){
	freopen("queue.in","r",stdin);
	freopen("queue.out","w",stdout);
	scanf("%d%d%d%d",&n1,&n2,&k1,&k2);
	dfs(1,0,0);
	printf("%d",ans);
} 

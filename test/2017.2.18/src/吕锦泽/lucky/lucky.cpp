#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
const int inf=100000000;
int f[10005],a[10005],n,m,ans=inf,t,sum[10005];
char ch;
struct data{
	int id,v,flag;
}num[10005];
bool cmp(data x,data y){
	if (x.v==y.v) 
		if (x.flag!=y.flag) return x.flag>y.flag;
		else return x.id<y.id;
	else return x.v<y.v;
}
void init(){
	scanf("%d%d\n",&n,&m);
	for (int i=1;i<=n;i++){
		scanf("%c",&ch);
		a[i]=ch-'0';
	}
}
void work(){
	for (int color=0;color<=9;color++){
		memset(f,127/2,sizeof f);
		memset(sum,127/2,sizeof sum);
		f[1]=abs(color-a[1]); f[0]=0;
		sum[0]=0; sum[1]=f[1];
		for (int i=2;i<=n;i++){
			for (int j=min(i,m);j>=1;j--)
					f[j]=min(f[j],sum[j-1]+abs(color-a[i]));
			for (int j=0;j<=i;j++)
				sum[j]=min(sum[j],f[j]);
		}
		if (sum[m]<ans){
			ans=sum[m];
			t=color;
		}
	}
	for (int i=1;i<=n;i++){
		num[i].id=i;
		num[i].v=abs(t-a[i]);
		if (t-a[i]<0) num[i].flag=1;
	}
	sort(num+1,num+1+n,cmp);
	for (int i=1;i<=m;i++)
		a[num[i].id]=t;
	printf("%d\n",ans);
	for (int i=1;i<=n;i++)
		printf("%d",a[i]);
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	init();
	work();
}

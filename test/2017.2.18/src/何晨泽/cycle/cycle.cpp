//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 5010
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n;
char map[N][N];
bool visit[N];
inline void Read() {
	getchar();
	Rep(i,1,n) gets(map[i]+1);
}
inline void Write(int x,int y,int z) {
	cout<<x<<" "<<y<<" "<<z<<endl;
	exit(0);
}
inline void dfs(int now,int bef) {
	visit[now]=true;
	Rep(i,1,n) {
		if (map[now][i]=='1') {
			if (!visit[i]) dfs(i,now);
					  else {
					  	if ( (bef!=-1) && (map[i][bef]=='1') ) {
					  		Write(i,bef,now);
					  	}
					  }
		}
	}
}
int main() {
	freopen("cycle.in","r",stdin);
	freopen("cycle.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	Read();
	Rep(i,1,n) dfs(i,-1); cout<<"-1"<<endl;
	return 0;
}

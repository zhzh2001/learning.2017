#include<fstream>
#include<string>
#include<iostream>
using namespace std;
ifstream fin("cycle.in");
ifstream fans("cycle.out");
ifstream fstd("cycle.ans");
const int N=5000;
bool mat[N][N];
string s;
int main()
{
	int n;
	fin>>n;
	for(int i=0;i<n;i++)
	{
		fin>>s;
		for(int j=0;j<n;j++)
			mat[i][j]=s[j]=='1';
	}
	int a,b,c;
	fans>>a;
	int ret=0;
	if(a!=-1)
	{
		fans>>b>>c;
		if(mat[a-1][b-1]&&mat[b-1][c-1]&&mat[c-1][a-1])
			cout<<"AC\ncycle found\n";
		else
		{
			cout<<"WA\nwrong cycle\n";
			ret=1;
		}
	}
	else
	{
		int t;
		fstd>>t;
		if(t==-1)
			cout<<"AC\ncycle not found\n";
		else
		{
			cout<<"WA\ncycle exists but not found\n";
			ret=1;
		}
	}
	return ret;
}
program gen;
const
  n=100;
  m=3000;
var
  i,j,u,v:longint;
  mat:array[1..n,1..n]of boolean;
begin
  randomize;
  assign(output,'cycle.in');
  rewrite(output);
  for i:=1 to n-1 do
    mat[i,i+1]:=true;
  for i:=1 to m do
  begin
    v:=random(n)+1;
	u:=random(v)+1;
	mat[u,v]:=true;
  end;
  writeln(n);
  for i:=1 to n do
  begin
    for j:=1 to n do
	  if mat[i,j] then
	    write(1)
	  else
	    write(0);
	writeln;
  end;
  close(output);
end.
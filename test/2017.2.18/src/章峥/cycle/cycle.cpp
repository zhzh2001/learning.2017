#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
const int N=5000;
bool mat[N][N],vis[N];
int n,S[N],sp;
char s[N+5];
void dfs(int k,int dep)
{
	//printf("%d\n",k+1);
	if(dep==3)
	{
		if(mat[k][S[sp-1]])
		{
			printf("%d %d %d\n",S[sp-1]+1,S[sp]+1,k+1);
			exit(0);
		}
		return;
	}
	S[++sp]=k;
	vis[k]=true;
	for(int i=0;i<n;i++)
		if(mat[k][i]&&!vis[i])
			dfs(i,dep+1);
	sp--;
	vis[k]=false;
}
int main()
{
	freopen("cycle.in","r",stdin);
	freopen("cycle.out","w",stdout);
	scanf("%d",&n);
	gets(s);
	for(int i=0;i<n;i++)
	{
		gets(s);
		for(int j=0;j<n;j++)
			mat[i][j]=s[j]=='1';
	}
	sp=0;
	for(int i=0;i<n;i++)
		dfs(i,1);
	puts("-1");
	return 0;
}
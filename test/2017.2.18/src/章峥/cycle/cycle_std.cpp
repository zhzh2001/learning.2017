#include<fstream>
#include<string>
using namespace std;
const int N=5000;
bool mat[N][N];
string s;
ifstream fin("cycle.in");
ofstream fout("cycle.ans");
int main()
{
	int n;
	fin>>n;
	for(int i=0;i<n;i++)
	{
		fin>>s;
		for(int j=0;j<n;j++)
			mat[i][j]=s[j]=='1';
	}
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			for(int k=0;k<n;k++)
				if(i!=j&&i!=k&&j!=k&&mat[i][j]&&mat[j][k]&&mat[k][i])
				{
					fout<<i+1<<' '<<j+1<<' '<<k+1<<endl;
					return 0;
				}
	fout<<-1<<endl;
	return 0;
}
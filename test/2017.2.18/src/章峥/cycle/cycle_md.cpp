#include<cstdio>
using namespace std;
const int N=5000;
bool mat[N][N],vis[2*N];
char s[N+5];
FILE *fin,*fout;
int main()
{
	fin=fopen("cycle.in","r");
	fout=fopen("cycle.out","w");
	int n;
	fscanf(fin,"%d",&n);
	fgets(s,n+5,fin);
	for(int i=0;i<n;i++)
	{
		fgets(s,n+5,fin);
		for(int j=0;j<n;j++)
		{
			mat[i][j]=s[j]=='1';
			vis[i-j+n]|=s[j]=='1';
		}
	}
	for(int x=-n+1;x<n;x++)
		if(vis[x+n])
		for(int y=-n+1;y<x;y++)
		{
			if(vis[y+n]){
				int z=-x-y;
				if(z>-n&&z<n&&vis[z+n])
				{
					for(int a=0;a<n;a++)
					{
						int b=a-x,c=b-y;
						if(b>=0&&b<n&&c>=0&&c<n&&mat[a][b]&&mat[b][c]&&mat[c][a])
						{
							fprintf(fout,"%d %d %d\n",a+1,b+1,c+1);
							return 0;
						}
					}
				}
			}
		}
	fputs("-1",fout);
	return 0;
}
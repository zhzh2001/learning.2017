program gen;
var
  n1,n2:longint;
begin
  randomize;
  assign(output,'queue.in');
  rewrite(output);
  n1:=random(15)+1;
  n2:=20-n1;
  writeln(n1,' ',n2,' ',random(n1)+1,' ',random(n2)+1);
  close(output);
end.
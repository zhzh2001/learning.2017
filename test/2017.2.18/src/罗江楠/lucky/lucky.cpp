#include <bits/stdc++.h>
#define ll long long
using namespace std;
int n, k, mp[10], ans;
char ch[10050], tmp[10050], a[10050];
int check(int x){
	int sum = 0;
	int num = k - mp[x];
	for(int i = 1; i < 10; i++){
		if(x - i >= 0) if(num - mp[x - i] <= 0) return sum + num * i;
		else sum += mp[x - i] * i, num -= mp[x - i];
		if(x + i < 10) if(num - mp[x + i] <= 0) return sum + num * i;
		else sum += mp[x + i] * i, num -= mp[x + i];
	}
}
int main(){
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	scanf("%d%d%s", &n, &k, ch);
	for(int i = 0; i < n; i++) mp[ch[i]-'0']++;
	for(int i = 0; i < 10; i++) ans = max(ans, mp[i]);
	if(ans >= k){
		cout << "0\n" << ch;
		return 0;
	}
	ans = (1<<30);
	int w = 0;
	for(int i = 0; i < 10; i++){
		int s = check(i);
		if(s < ans) ans = s, w = i;
	}
	printf("%d\n", ans);
	for(int i = 1; i < 10 && ans; i++){
		if(w-i>=0)for(int j = 0; j < n && ans; j++)
			if(ch[j]-'0'==w-i) ans-=i, ch[j] = w + '0';
		if(w+i<10)for(int j = 0; j < n && ans; j++)
			if(ch[j]-'0'==w+i) ans-=i, ch[j] = w + '0';
	}
	cout << ch;
	return 0;
}


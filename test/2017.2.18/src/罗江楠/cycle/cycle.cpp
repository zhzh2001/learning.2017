#include <bits/stdc++.h>
#include <vector>
#define N 5050
#define ll long long
using namespace std;
bool mp[N][N];
int n, m, x;
int main(){
	freopen("cycle.in", "r", stdin);
	freopen("cycle.out", "w", stdout);

	scanf("%d", &n);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++){
			scanf("%1d", &x);
			mp[i][j] = (x == 1);
		}
	for(int i = 1; i < n; i++)
		for(int j = i; j <= n; j++)
			if(mp[i][j])
				for(int k = i+1; k <= n; k++)
					if(mp[k][i] && mp[j][k])
						return printf("%d %d %d", i, j, k)*0;
	return puts("-1")*0;
}


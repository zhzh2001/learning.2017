#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
const int p=100000000;
int n1,n2,n,k1,k2,x,ans,b[50];

inline void dfs(int x,int x1,int x2,int y1,int y2 ) 
{
	if(x>n) 
	{
		ans++;
		if (ans==p) ans = 0;
		return;
	}
	if(y1==k1&&y2==k2) return ;
	if(y1==k1&&x2<n2&&y2<k2) 
	{
		dfs(x+1,x1,x2+1,0,y2+1) ;
		return ;
	}
	if(y2==k2&&x1<n1&&y1<k1)
	{
		dfs(x+1,x1+1,x2,y1+1,0) ;
		return;
	}
	if(x1<n1&&y1<k1) dfs(x+1,x1+1,x2,y1+1,0) ;
	if(x2<n2&&y2<k2) dfs(x+1,x1,x2+1,0,y2+1) ;
	return ;
}

int main()
{
	freopen("queue.in","r",stdin) ;
	freopen("queue.out","w",stdout) ;
	scanf("%d%d%d%d",&n1,&n2,&k1,&k2) ;
	n=n1+n2;
	ans= 0;
	dfs(1,0,0,0,0) ;
	
	ans = ans % p;
	printf("%d",ans) ;
	
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
const int maxn = 10005;
int n, k;
string str;
short dit[maxn];
int a[10];
int f[maxn][10];
int ans = 0x3f3f3f3f;

int main() {
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n >> k >> str;
	int length = str.length();
	for (int i = 0; i < length; i++) {
		dit[i] = str[i] - '0';
		a[dit[i]]++;
		for (int j = 0; j < 10; j++){
			f[i][j] = abs(dit[i] - j);
		}
	}
	int loc, maxt = 0;
	for (int i = 0; i <= 9; i++) {
		if (a[i] > maxt) maxt = a[i], loc = i;
	}
	if (maxt >= k) {
		cout << 0 << endl << str << endl;
		return 0;
	}
	int cnt = maxt;
	ans = 0;
	for (int j = 1; j < 10; j++) {
		for (int i = 0; i < length; i++) {
			if (abs(dit[i] - loc) == j){
				dit[i] = loc;
				ans += j;
				cnt++;
				if (cnt >= k) break;
			}
		}
		if (cnt >= k) break;
	}
	cout << ans << endl;
	for (int i = 0; i < length; i++)
		cout << dit[i];
}
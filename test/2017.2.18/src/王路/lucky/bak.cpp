#include <bits/stdc++.h>
using namespace std;
const int maxn = 10005;
int n, k;
string str;
short dit[maxn];
int a[10];
int f[maxn][10];
int ans = 0x3f3f3f3f;

void dfs(int dep, int x) {
	if (dep == n){
		::ans = min(::ans, x);
		return;
	}
	for (int i = 0; i <= 9; i++) {
		int bak = dit[dep];
		dfs(dep+1, x+f[dep][i]);
		dit[dep] = bak;
	}
}

int main() {
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n >> k >> str;
	for (int i = 0; i < str.length(); i++) {
		dit[i] = str[i] - '0';
		for (int j = 0; j <= 9; j++) {
			f[i][j] = abs(str[i]-'0' - j);
		}
	}
	if (n <= 100) {
		dfs(0, 0);
		cout << ans << endl;
	} else {
		cout << 0 << endl << str << endl;
	}
}
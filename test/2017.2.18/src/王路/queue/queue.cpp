#include <bits/stdc++.h>
using namespace std;
const int Mod = 100000000;
const int maxsize = 205;
int f[205][2];
int main() {
	freopen("queue.in", "r", stdin);
	freopen("queue.out", "w", stdout);
	int n1, n2, x1, x2;
	ios::sync_with_stdio(false);
	cin >> n1 >> n2 >> x1 >> x2;
	f[0][0] = 0;
	int total = n1 + n2;
	int ans = 0;
	if (total <= 10) {
		int arr[15];
		for (int i = 1; i <= total; i++)
			if (i <= n1) arr[i] = 1;
				else arr[i] = 2;
		int cnt1 = 0, cnt2 = 0;
		bool flag = true;
		for (int i = 1; i <= total; i++) {
			for (int j = 1; j <= total; j++) {
				if (arr[i] == 1) cnt1++, cnt2 = 0;
					else cnt2++, cnt1 = 0;
				if (cnt1 > x1 || cnt2 > x2) { flag = false; break; }
			}
			if (flag) ans++;
			next_permutation(arr+1,arr+total+1);
		}
		cout << ans << endl;
		exit(0);
	}
	cout << ans << endl;
	exit(0);
}
#include <bits/stdc++.h>
using namespace std;
const int maxn = 5005;
int n;
char ch;
char mat[maxn][maxn];
int main() {
	freopen("cycle.in", "r", stdin);
	freopen("cycle.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			cin >> mat[i][j];
			mat[i][j] -= '0';
			if (i == j) mat[i][j] = 0;
		}
	}
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= n; ++j) {
			if (clock() < 475){
				if (mat[i][j]){
					for (int k = 1; k <= n; ++k){
						if (mat[j][k] && mat[k][i]) {
							cout << i << " " << j << " " << k << endl;
						//	cout << clock();
							return 0;
						}
					}
				}
			} else {
				cout << -1 << endl;
				exit(0);
			}
		}
	}
	cout << -1 << endl;
}
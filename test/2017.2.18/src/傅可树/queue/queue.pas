var ans,n1,n2,k1,k2:longint;
procedure dfs(k,x,y,pre1,pre2:longint);
begin
  if (k=n1+n2+1)and(n1=x)and(n2=y) then
     begin
     ans:=(ans+1) mod 100000000;
     exit;
     end;
  if (pre1+1<=k1)and(x+1<=n1) then dfs(k+1,x+1,y,pre1+1,0);
  if (pre2+1<=k2)and(y+1<=n2) then dfs(k+1,x,y+1,0,pre2+1);
end;
begin
  assign(input,'queue.in');
  assign(output,'queue.out');
  reset(input);
  rewrite(output);
  readln(n1,n2,k1,k2);
  dfs(1,0,0,0,0);
  writeln(ans mod 100000000);
  close(input);
  close(output);
end.

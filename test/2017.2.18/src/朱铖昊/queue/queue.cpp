#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cmath>
using namespace std;
int d[3],n,m,i,j,f[105][105][3][16],k,l,ans;
const int mod=100000000;
int main()
{
	freopen("queue.in","r",stdin);
	freopen("queue.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&d[1],&d[2]);
	f[1][0][1][1]=1;
	f[0][1][2][1]=1;
	for (i=2;i<=n+m;++i)
		for (j=0;j<=min(m,i);++j)
			for (k=1;k<=2;++k)
			{
				if (j+1-k<0 || i-j+k-2<0 ||i<j || k>=3 ||i-j>n) continue;
				for (l=1;l<=d[3-k];++l)
					f[i-j][j][k][1]=(f[i-j][j][k][1]+f[i-j+k-2][j+1-k][3-k][l])%mod;
				f[i-j][j][k][1]%=mod;
				for (l=2;l<=d[k];++l)
					f[i-j][j][k][l]=f[i-j+k-2][j+1-k][k][l-1];
			}
	for (i=1;i<=2;++i)
		for (j=1;j<=d[i];++j)
			ans=(ans+f[n][m][i][j])%mod;
	/*for (i=0;i<=n;++i)
		for (j=0;j<=m;++j)
			for (k=1;k<=2;++k)
				for (l=1;l<=d[k];++l)
					printf("%d %d %d %d %d\n",i,j,k,l,f[i][j][k][l]);*/
	printf("%d",ans);
	return 0;
}

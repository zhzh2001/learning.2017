#include<cstdio>
#include<iostream> 
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstring>
#define ll long long
using namespace std;
const ll MOD=100000000;
ll f[201][101][2][11]={0},ans=0;
int n1,n2,k1,k2;
int main()
{
	freopen("queue.in","r",stdin);
	freopen("queue.out","w",stdout);
	scanf("%d%d%d%d",&n1,&n2,&k1,&k2);
	f[1][1][1][1]=1;
	f[1][0][0][1]=1;
	for(int i=2;i<=n1+n2;i++)
		for(int j=0;j<=n1;j++){
			for(int l=1;l<=k1;l++){
				if(l==1)for(int k=1;k<=k2;k++)f[i][j][1][l]=(f[i][j][1][l]+f[i-1][j-1][0][k])%MOD;
				else f[i][j][1][l]=f[i-1][j-1][1][l-1];
			}
			for(int l=1;l<=k2;l++){
				if(l==1)for(int k=1;k<=k1;k++)f[i][j][0][l]=(f[i][j][0][l]+f[i-1][j][1][k])%MOD;
				else f[i][j][0][l]=f[i-1][j][0][l-1];
			}
		}
	for(int i=1;i<=k1;i++)ans=(ans+f[n1+n2][n1][1][i])%MOD;
	for(int i=1;i<=k2;i++)ans=(ans+f[n1+n2][n1][0][i])%MOD;
	printf("%lld",ans);
	return 0;	
}

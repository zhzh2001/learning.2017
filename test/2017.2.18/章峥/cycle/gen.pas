program gen;
const
  n=100;
var
  i,j:longint;
begin
  randomize;
  assign(output,'cycle.in');
  rewrite(output);
  writeln(n);
  for i:=1 to n do
  begin
    for j:=1 to n do
	  write(random(2));
	writeln;
  end;
  close(output);
end.
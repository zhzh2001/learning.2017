#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
const int N=5000;
bool mat[N][N],vis[N];
char s[N][N+5];
int n,last[N],cc,S[N],sp;
void dfs(int k)
{
	//printf("%d\n",k+1);
	if(last[k]&&cc-last[k]==2&&s[k][S[sp-1]]=='1')
	{
		printf("%d %d %d\n",S[sp-1]+1,S[sp]+1,k+1);
		exit(0);
	}
	last[k]=++cc;
	S[++sp]=k;
	vis[k]=true;
	for(int i=0;i<n;i++)
		if(mat[k][i])
		{
			mat[k][i]=false;
			if(n<=100||!vis[i])
				dfs(i);
		}
	sp--;
}
int main()
{
	freopen("cycle.in","r",stdin);
	freopen("cycle.out","w",stdout);
	scanf("%d",&n);
	gets(s[0]);
	for(int i=0;i<n;i++)
	{
		gets(s[i]);
		for(int j=0;j<n;j++)
			mat[i][j]=s[i][j]=='1';
	}
	for(int i=0;i<n;i++)
		if(!vis[i])
		{
			memset(last,0,sizeof(last));
			cc=sp=0;
			dfs(i);
		}
	puts("-1");
	return 0;
}
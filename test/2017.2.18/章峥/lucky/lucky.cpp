#include<fstream>
#include<string>
#include<algorithm>
#include<cstdlib>
using namespace std;
ifstream fin("lucky.in");
ofstream fout("lucky.out");
const int N=10000;
string s,t,ans;
struct digit
{
	int delta,i;
	bool operator<(const digit& b)const
	{
		if(abs(delta)!=abs(b.delta))
			return abs(delta)<abs(b.delta);
		if(delta<0)
			if(b.delta<0)
				return i<b.i;
			else
				return true;
		else
			if(b.delta<0)
				return false;
			else
				return i>b.i;
	}
}d[N];
int main()
{
	int n,k;
	fin>>n>>k>>s;
	int cnt=n*10;
	for(int i=0;i<10;i++)
	{
		for(int j=0;j<n;j++)
		{
			d[j].delta=i-(s[j]-'0');
			d[j].i=j;
		}
		sort(d,d+n);
		int now=0;
		for(int j=0;j<k;j++)
			now+=abs(d[j].delta);
		t=s;
		for(int j=0;j<k;j++)
			t[d[j].i]=i+'0';
		if(now<cnt)
		{
			cnt=now;
			ans=t;
		}
		else
			if(now==cnt&&t<ans)
				t=ans;
	}
	fout<<cnt<<endl<<ans<<endl;
	return 0;
}
program gen;
const
  n=10000;
var
  i:longint;
begin
  randomize;
  assign(output,'lucky.in');
  rewrite(output);
  writeln(n,' ',random(n-1000)+1000);
  for i:=1 to n do
    write(random(10));
  writeln;
  close(output);
end.
#include<fstream>
using namespace std;
ifstream fin("queue.in");
ofstream fout("queue.out");
const int N=105,MOD=100000000;
int f[N][N][2];
int main()
{
	int n1,n2,k1,k2;
	fin>>n1>>n2>>k1>>k2;
	f[0][0][0]=f[0][0][1]=1;
	for(int i=0;i<=n1;i++)
		for(int j=0;j<=n2;j++)
			if(i||j)
			{
				for(int k=1;k<=k1&&k<=i;k++)
					f[i][j][0]=(f[i][j][0]+f[i-k][j][1])%MOD;
				for(int k=1;k<=k2&&k<=j;k++)
					f[i][j][1]=(f[i][j][1]+f[i][j-k][0])%MOD;
			}
	fout<<(f[n1][n2][0]+f[n1][n2][1])%MOD<<endl;
	return 0;
}
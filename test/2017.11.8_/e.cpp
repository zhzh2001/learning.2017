#include <iostream>
#include <string>
using namespace std;
int main()
{
	string s, t;
	cin >> s >> t;
	int l1 = s.length(), l2 = t.length();
	for (int i = l1 - l2; i >= 0; i--)
	{
		bool match = true;
		for (int i1 = i, i2 = 0; i2 < l2; i1++, i2++)
			if (s[i1] != t[i2] && s[i1] != '?')
			{
				match = false;
				break;
			}
		if (match)
		{
			for (int j = 0; j < i; j++)
				if (s[j] == '?')
					cout.put('a');
				else
					cout.put(s[j]);
			cout << t;
			for (int j = i + l2; j < l1; j++)
				if (s[j] == '?')
					cout.put('a');
				else
					cout.put(s[j]);
			cout << endl;
			return 0;
		}
	}
	cout << "UNRESTORABLE\n";
	return 0;
}
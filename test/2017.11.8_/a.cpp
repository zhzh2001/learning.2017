#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N], b[N], c[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= n; i++)
		cin >> b[i];
	for (int i = 1; i <= n; i++)
		cin >> c[i];
	sort(c + 1, c + n + 1);
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		ans += 1ll * (lower_bound(a + 1, a + n + 1, b[i]) - a - 1) * (n - (upper_bound(c + 1, c + n + 1, b[i]) - c) + 1);
	cout << ans << endl;
	return 0;
}
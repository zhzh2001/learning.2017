#include <iostream>
using namespace std;
const int N = 55;
int f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
pair<int, int> e[N];
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= m; i++)
		cin >> e[i].first >> e[i].second;
	int ans = 0;
	for (int i = 1; i <= m; i++)
	{
		for (int j = 1; j <= n; j++)
			f[j] = j;
		for (int j = 1; j <= m; j++)
			if (j != i)
				f[getf(e[j].first)] = getf(e[j].second);
		for (int j = 2; j <= n; j++)
			if (getf(j) != getf(1))
			{
				ans++;
				break;
			}
	}
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <unordered_set>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	unordered_set<int> S;
	while (n--)
	{
		int x;
		cin >> x;
		auto it = S.find(x);
		if (it == S.end())
			S.insert(x);
		else
			S.erase(it);
	}
	cout << S.size() << endl;
	return 0;
}
#include <iostream>
using namespace std;
const int N = 105;
long long a[N];
long long gcd(long long a, long long b)
{
	return b ? gcd(b, a % b) : a;
}
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 2; i <= n; i++)
		for (int j = 1; j < i; j++)
			a[i] /= gcd(a[i], a[j]);
	long long ans = 1;
	for (int i = 1; i <= n; i++)
		ans *= a[i];
	cout << ans << endl;
	return 0;
}
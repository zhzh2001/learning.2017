#include <iostream>
using namespace std;
const int N = 30005, C = 105, MOD = 1e9 + 7;
int cnt[N], f[N][C][2][2], c[C][C];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	int pred = 0, cc = 0, m = 0;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		if (x != pred)
		{
			if (m && x - pred > 1)
			{
				cout << 0 << endl;
				return 0;
			}
			if (cc)
				cnt[++m] = cc;
			pred = x;
			cc = 0;
		}
		cc++;
	}
	if (cc)
		cnt[++m] = cc;
	c[0][0] = 1;
	for (int i = 1; i <= 100; i++)
	{
		c[i][0] = 1;
		for (int j = 1; j <= i; j++)
			c[i][j] = (c[i - 1][j] + c[i - 1][j - 1]) % MOD;
	}
	f[1][cnt[1] - 1][1][1] = 1;
	for (int i = 1; i < m; i++)
		for (int j = 0; j < cnt[i]; j++)
			for (int n1 = 0; n1 < 2; n1++)
				for (int n2 = 0; n2 < 2; n2++)
					if (f[i][j][n1][n2])
						for (int k = 0; k <= j; k++)
							for (int k1 = 0; k1 <= n1; k1++)
								for (int k2 = 0; k2 <= n2; k2++)
									if (k + k1 + k2 && k + k1 + k2 <= cnt[i + 1])
										(f[i + 1][cnt[i + 1] - k - k1 - k2][k1][k2] += 1ll * f[i][j][n1][n2] * c[cnt[i + 1] - 1][k + k1 + k2 - 1] % MOD * c[j][k] % MOD) %= MOD;
	int ans = 0;
	for (int i = 0; i <= cnt[m]; i++)
		for (int n1 = 0; n1 < 2; n1++)
			for (int n2 = 0; n2 < 2; n2++)
				(ans += f[m][i][n1][n2]) %= MOD;
	cout << ans << endl;
	return 0;
}
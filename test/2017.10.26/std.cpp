//  Created by Sengxian on 5/11/16.
//  Copyright (c) 2016年 Sengxian. ALL rights reserved.
//  BZOJ 4197 NOI 2015 D1t3
#include <algorithm>
#include <iostream>
#include <cctype>
#include <climits>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <vector>
#include <stack>
#include <queue>
using namespace std;

inline int ReadInt()
{
	static int n, ch;
	n = 0, ch = getchar();
	while (!isdigit(ch))
		ch = getchar();
	while (isdigit(ch))
		n = (n << 1) + (n << 3) + ch - '0', ch = getchar();
	return n;
}

const int maxp = 8, maxn = 500 + 3;
const int primes[] = {2, 3, 5, 7, 11, 13, 17, 19};
int n, modu, f[1 << maxp][1 << maxp], g[2][1 << maxp][1 << maxp];

typedef pair<int, int> state;
state sts[maxn];

inline int mod(int x)
{
	return (x % modu + modu) % modu;
}

int main()
{
	n = ReadInt(), modu = ReadInt();
	for (int i = 2; i <= n; ++i)
	{
		sts[i].second = 0;
		int t = i;
		for (int j = 0; j < maxp; ++j)
			if (t % primes[j] == 0)
			{
				sts[i].second += 1 << j;
				while (t % primes[j] == 0)
					t /= primes[j];
			}
		sts[i].first = t;
	}
	sort(sts + 2, sts + 2 + n - 1);
	f[0][0] = 1;
	for (int i = 2; i <= n; ++i)
	{
		if (i == 2 || sts[i].first == 1 || sts[i].first != sts[i - 1].first)
		{ //一个新的块，一个块中的所有元素只能被至多一个人选（且是同一人！
			memcpy(g[0], f, sizeof f);
			memcpy(g[1], f, sizeof f);
		}
		for (int k = 0; k < 1; ++k)
			for (int s1 = (1 << maxp) - 1; s1 >= 0; --s1)
				for (int s2 = (1 << maxp) - 1; s2 >= 0; --s2)
				{
					if ((s2 & sts[i].second) == 0)
						(g[0][s1 | sts[i].second][s2] += g[0][s1][s2]) %= modu;
					if ((s1 & sts[i].second) == 0)
						(g[1][s1][s2 | sts[i].second] += g[1][s1][s2]) %= modu;
				}
		if (i == n || sts[i].first == 1 || sts[i].first != sts[i + 1].first)
		{ //结束块，用 g 去更新 f
			for (int s1 = (1 << maxp) - 1; s1 >= 0; --s1)
				for (int s2 = (1 << maxp) - 1; s2 >= 0; --s2)
					f[s1][s2] = mod(g[0][s1][s2] + g[1][s1][s2] - f[s1][s2]); //注意两个人都不取算了两次，要减去！
		}
	}
	int ans = 0;
	for (int s1 = 0; s1 < (1 << maxp); ++s1)
		for (int s2 = 0; s2 < (1 << maxp); ++s2)
			if ((s1 & s2) == 0)
				(ans += f[s1][s2]) %= modu;
	printf("%d\n", ans);
	return 0;
}
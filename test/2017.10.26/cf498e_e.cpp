#include <iostream>
#include <cstring>
using namespace std;
const int n = 7, MOD = 1e9 + 7;
int w[n + 1];
struct matrix
{
	int sz, mat[1 << n][1 << n];
	matrix(int sz) : sz(sz)
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret(sz);
		for (int i = 0; i < sz; i++)
			for (int j = 0; j < sz; j++)
				for (int k = 0; k < sz; k++)
					ret.mat[i][j] = (ret.mat[i][j] + 1ll * mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
	matrix operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I(int sz)
{
	matrix ret(sz);
	for (int i = 0; i < 1 << n; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, int b, int sz)
{
	matrix ret = I(sz);
	do
	{
		if (b & 1)
			ret *= a;
		a *= a;
	} while (b /= 2);
	return ret;
}
int main()
{
	for (int i = 1; i <= n; i++)
		cin >> w[i];
	matrix ans = I(1 << n);
	for (int i = 1; i <= n; i++)
	{
		matrix now(1 << i);
		for (int j = 0; j < 1 << i; j++)
			for (int k = 0; k < 1 << i; k++)
				for (int h = 0; h < 1 << i - 1; h++)
					now.mat[j][k] += (j | k | h | (h << 1)) == (1 << i) - 1;
		ans *= qpow(now, w[i], 1 << i);
	}
	cout << ans.mat[0][0] << endl;
	return 0;
}
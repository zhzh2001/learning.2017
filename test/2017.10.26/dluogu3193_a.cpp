#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int M = 25;
int n, m, mod, nxt[M], nxtc[M][10];
struct matrix
{
	int mat[M][M];
	matrix()
	{
		fill_n(&mat[0][0], sizeof(mat) / sizeof(int), 0);
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ans;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				for (int k = 0; k < m; k++)
					ans.mat[i][j] = (ans.mat[i][j] + mat[i][k] * rhs.mat[k][j]) % mod;
		return ans;
	}
	matrix operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i < m; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, int b)
{
	matrix ans = I();
	do
	{
		if (b & 1)
			ans *= a;
		a *= a;
	} while (b /= 2);
	return ans;
}
int main()
{
	string s;
	cin >> n >> m >> mod >> s;
	s = ' ' + s;
	for (int i = 2, j = 0; i <= m; i++)
	{
		for (; j && s[i] != s[j + 1]; j = nxt[j])
			;
		if (s[i] == s[j + 1])
			j++;
		nxt[i] = j;
	}
	matrix trans;
	for (int i = 0; i < m; i++)
		for (int j = 0; j < 10; j++)
		{
			nxtc[i][j] = s[i + 1] == j + '0' ? i + 1 : nxtc[nxt[i]][j];
			trans.mat[i][nxtc[i][j]]++;
		}
	matrix init;
	init.mat[0][0] = 1;
	init = init * qpow(trans, n);
	int ans = 0;
	for (int i = 0; i < m; i++)
		ans = (ans + init.mat[0][i]) % mod;
	cout << ans << endl;
	return 0;
}
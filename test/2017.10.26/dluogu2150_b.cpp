#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
const int N = 505, P = 8, p[] = {2, 3, 5, 7, 11, 13, 17, 19};
pair<int, int> factor[N];
int f[1 << P][1 << P], g[2][1 << P][1 << P];
int main()
{
	int n, mod;
	cin >> n >> mod;
	for (int i = 2; i <= n; i++)
	{
		int x = i;
		for (int j = 0; j < P; j++)
			if (x % p[j] == 0)
			{
				factor[i].second += 1 << j;
				for (; x % p[j] == 0; x /= p[j])
					;
			}
		factor[i].first = x;
	}
	sort(factor + 2, factor + n + 1);
	f[0][0] = 1;
	for (int i = 2; i <= n; i++)
	{
		if (factor[i].first != factor[i - 1].first || factor[i].first == 1)
		{
			memcpy(g[0], f, sizeof(f));
			memcpy(g[1], f, sizeof(f));
		}
		for (int j = (1 << P) - 1; ~j; j--)
			for (int k = (1 << P) - 1; ~k; k--)
			{
				if ((k & factor[i].second) == 0)
					(g[0][j | factor[i].second][k] += g[0][j][k]) %= mod;
				if ((j & factor[i].second) == 0)
					(g[1][j][k | factor[i].second] += g[1][j][k]) %= mod;
			}
		if (factor[i].first != factor[i + 1].first || factor[i].first == 1)
			for (int j = (1 << P) - 1; ~j; j--)
				for (int k = (1 << P) - 1; ~k; k--)
					f[j][k] = ((g[0][j][k] + g[1][j][k] - f[j][k]) % mod + mod) % mod;
	}
	int ans = 0;
	for (int i = 0; i < 1 << P; i++)
		for (int j = 0; j < 1 << P; j++)
			if ((i & j) == 0)
				(ans += f[i][j]) %= mod;
	cout << ans << endl;
	return 0;
}
#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=2001,M=4000001;
struct node{
	int x,y;
	double w;
}edge[M];
int f[N],cnt;
int n,k,num;
int x[N],y[N];

bool cmp(node a,node b){
	return a.w<b.w;
}

void add(int i,int j){
	edge[++cnt].x=i; edge[cnt].y=j;
	edge[cnt].w=sqrt((double)(x[i]-x[j])*(x[i]-x[j])+(double)(y[i]-y[j])*(y[i]-y[j]));
}

int getf(int x){
	if(f[x]==x) return x;
	f[x]=getf(f[x]);
	return f[x];
}

int main(){
	freopen("meadow.in","r",stdin);
	freopen("meadow.out","w",stdout);
	num=n=read(); k=read();
	for(int i=1;i<=n;i++){
		x[i]=read(); y[i]=read();
		f[i]=i;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<i;j++)
			add(i,j);
	sort(edge+1,edge+cnt+1,cmp);
	for(int i=1;i<=cnt;i++){
		int x=edge[i].x,y=edge[i].y;
		double w=edge[i].w;
		int fx=getf(x),fy=getf(y);
		if(fx==fy) continue;
		f[fx]=fy; num--;
		if(num==k){
			printf("%.2lf\n",w);
			return 0;
		}
	}
	return 0;
}

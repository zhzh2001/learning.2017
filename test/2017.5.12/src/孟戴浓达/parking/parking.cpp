//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define debug cout<<"yes"<<endl;
using namespace std;
ifstream fin("parking.in");
ofstream fout("parking.out");
int n,p,k,ans;
bool b[10003],vis[10003],yes;
int dep[10003],fa[10003];
int head[10003],to[10003],next[10003],tot;
inline void add(int u,int v){
	to[++tot]=v;
	next[tot]=head[u];
	head[u]=tot;
}
void dfs(int node){
	if(vis[node]==1){
		return;
	}
	vis[node]=1;
	for(int i=head[node];i;i=next[i]){
		int go=to[i];
		dep[go]=dep[node]+1;
		dfs(go);
	}
}
int main(){
	//freopen("parking.in","r",stdin);
	//freopen("parking.out","w",stdout);
	fin>>n>>p>>k;
	for(int i=1;i<=n;i++){
		int x;
		fin>>x;
		for(int j=1;j<=x;j++){
			int xx;
			fin>>xx;
			fa[xx]=i;
			add(i,xx);
		}
	}
	for(int i=1;i<=k;i++){
		int x;
		fin>>x;
		b[x]=true;
	}
	for(int i=p;i!=1;i=fa[i]){
		dep[p]++;
	}
	dep[0]=99999999;
	for(int i=p;i!=1;i=fa[i]){
		if(b[i]==true){
			dfs(i);
			yes=true;
			int x=0;
			for(int j=1;j<=n;j++){
				if(vis[j]&&b[j]==false&&dep[j]<dep[x]){
					x=j;
					yes=false;
				}
			}
			if(yes==true){
				break;
			}
			ans+=dep[x]-dep[i];
			b[x]=1;
		}
		b[i]=1;
		dep[fa[i]]=dep[i]-1;
	}
	if(yes==false){
		fout<<ans<<endl;
	}else{
		fout<<"not solvable"<<endl;
	}
	return 0;
}
/*

in:
6 3 2
1 4
1 6
0
1 5
2 2 3
0
4 5

out:
4

*/

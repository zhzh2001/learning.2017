//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define debug cout<<"yes"<<endl;
using namespace std;
ifstream fin("road.in");
ofstream fout("road.out");
int n,m,ans,tim=1;
int tu[1003][1003],dis[1003],pre[1003],visit[1003];
inline void dijstra1(){
	for(int i=0;i<=n;i++){
		dis[i]=99999999;
	}
	dis[1]=0;
	for(int i=1;i<=n;i++){
		int k=0;
		for(int j=1;j<=n;j++){
			if(visit[j]!=tim&&dis[j]<dis[k]){
				k=j;
				if(k==n){
					return;
				}
			}
		}
		visit[k]=tim;
		for(int j=1;j<=n;j++){
			if(dis[k]+tu[k][j]<dis[j]){
				dis[j]=dis[k]+tu[k][j];
				pre[j]=k;
			}
		}
	}
}
inline void dijstra2(){
	for(int i=0;i<=n;i++){
		dis[i]=99999999;
	}
	dis[1]=0;
	for(int i=1;i<=n;i++){
		int k=0;
		for(int j=1;j<=n;j++){
			if(visit[j]!=tim&&dis[j]<dis[k]){
				k=j;
				if(k==n){
					return;
				}
			}
		}
		visit[k]=tim;
		for(int j=1;j<=n;j++){
			if(dis[k]+tu[k][j]<dis[j]){
				dis[j]=dis[k]+tu[k][j];
			}
		}
	}
}
int main(){
	//freopen("road.in","r",stdin);
	//freopen("road.out","w",stdout);
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			tu[i][j]=999999999;
		}
	}
	for(int i=0;i<m;i++){
		int u,v,w;
		fin>>u>>v>>w;
		tu[u][v]=w;
		tu[v][u]=w;
	}
	dijstra1();
	for(int i=n;i!=1;i=pre[i]){
		tim++;
		int a=pre[i],b=tu[pre[i]][i];
		tu[a][i]=999999999;
		tu[i][a]=999999999;
		dijstra2();
		ans=max(ans,dis[n]);
		tu[a][i]=b;
		tu[a][i]=b;
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
5 7
1 2 8
1 4 10
2 3 9
2 4 10
2 5 1
3 4 7
3 5 10

out:
27

*/

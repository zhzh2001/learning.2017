#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<iomanip>
using namespace std;
//ifstream fin(".in");
//ofstream fout(".out");
int n,m;
int x[1003],y[1003];
int sqr(int x){
	return x*x;
}
long double ans[10003];
int anslen;
int main(){
	//freopen(".in","r",stdin);
	//freopen(".out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=n;i++){
		cin>>x[i]>>y[i];
	}
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			ans[++anslen]=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
		}
	}
	sort(ans+1,ans+anslen+1);
	for(int i=1;i<=anslen;i++){
		cout<<fixed<<setprecision(2)<<ans[i]<<endl;
	}
	return 0;
}
/*

in:
7 4
1 1
3 9
9 4
2 2
6 4
5 5
6 9

out:
3.00

*/

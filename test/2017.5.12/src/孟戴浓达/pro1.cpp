#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define debug cout<<"yes"<<endl;
using namespace std;
int jiyi[19][170000];
int n;
string s[19];
int dp(int last,int zt){
	int& fanhui=jiyi[last][zt];
	if(fanhui!=-1){
		return fanhui;
	}
	fanhui=0;
	for(int i=1;i<=n;i++){
		if(((zt&(1<<i))==0)&&s[last][s[last].length()-1]==s[i][0]){
			int len=s[i].length();
			//cout<<zt<<" "<<i<<endl;
			fanhui=max(fanhui,dp(i,(zt|(1<<i)))+len);
		}
	}
	return fanhui;
}
int main(){
	memset(jiyi,-1,sizeof(jiyi));
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>s[i];
	}
	int ans=-9999999;
	for(int i=1;i<=n;i++){
		int len=s[i].length();
		ans=max(ans,dp(i,(1<<i))+len);
	}
	cout<<ans<<endl;
	return 0;
}
/*

in:
5
IOO
IUUO
AI
OIOOI
AOOI

out:
16
 
*/

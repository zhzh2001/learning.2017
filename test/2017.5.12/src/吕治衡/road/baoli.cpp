#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<queue>
#define maxn 1001
#define f(i,l,r) for(int i=l;i<=r;i++)
int n,m,t,ans,stx,endx,val;
int p[maxn],dis[maxn],pre[maxn];
bool vis[maxn];
using namespace std;
struct node {
	int next,to,len;
} e[maxn*maxn];
inline void add(int from,int to,int len) {
	t++;
	e[t].to=to;
	e[t].len=len;
	e[t].next=p[from];
	p[from]=t;
}
inline void spfa() {
	queue<int> q;
	f(i,1,n) {
		dis[i]=0x7ffff;
		vis[i]=false;
	}
	dis[1]=0;
	vis[1]=true;
	q.push(1);
	while(!q.empty()) {
		int now=q.front();
		vis[now]=false;
		q.pop();
		for(int i=p[now]; i!=0; i=e[i].next) {
			int ty=e[i].to;
			if(dis[ty]>dis[now]+e[i].len) {
				dis[ty]=dis[now]+e[i].len;
				pre[ty]=now;
				if(!vis[ty]) {
					vis[ty]=true;
					q.push(ty);
				}
			}
		}
	}
}
inline void spfaa(int x,int y) {
	queue<int> q;
	f(i,1,n) {
		dis[i]=0x7ffff;
		vis[i]=false;
	}
	dis[1]=0;
	vis[1]=true;
	q.push(1);
	while(!q.empty()) {
		int now=q.front();
		vis[now]=false;
		q.pop();
		for(int i=p[now]; i!=0; i=e[i].next) {
			int ty=e[i].to;
			if(ty==y&&now==x) continue;
			if(dis[ty]>dis[now]+e[i].len) {
				dis[ty]=dis[now]+e[i].len;
				if(!vis[ty]) {
					vis[ty]=true;
					q.push(ty);
				}
			}
		}
	}
	ans=max(ans,dis[n]);
}
int main() {
	freopen("road.in","r",stdin);freopen("baoli.out","w",stdout); 
	ios::sync_with_stdio(false);
	memset(p,0,sizeof(p));
	cin>>n>>m;
	f(i,1,m) {
		cin>>stx>>endx>>val;
		add(stx,endx,val);
		add(endx,stx,val);
	}
	spfa();
	for(int i=n; i!=0; i=pre[i])
		spfaa(pre[i],i);
	cout<<ans<<endl;
	return 0;
}

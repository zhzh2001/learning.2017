#include<bits/stdc++.h>
using namespace std;
int n,m,a[2000][2000],f[2000],ff[2000],fa[2000],ans,x,y,z;
bool flag[2000];
int dij(int x,int y)
	{
		int kk=a[x][y];a[x][y]=1e9;a[y][x]=1e9;
		memset(f,10,sizeof(f));
		memset(flag,10,sizeof(flag));
		f[1]=0;
		for (int i=1;i<=n;i++)
			{
				int k=0;
				for (int j=1;j<=n;j++)
					if (!flag[j]&&f[j]<f[k]) k=j;
				flag[k]=true;
				for (int j=1;j<=n;j++)
					f[j]=min(f[j],f[k]+a[k][j]);
			}
		a[x][y]=kk;a[y][x]=kk;
		return f[n];
	}
void dfs(int x)
	{
		if (!fa[x])return;
		ans=max(ans,dij(fa[x],x));
		dfs(fa[x]);
	}
int main()
{
	freopen("road.in","r",stdin);freopen("road.out","w",stdout); 
	ios::sync_with_stdio(false);
	cin>>n>>m;
	memset(a,10,sizeof(a));
	for (int i=1;i<=m;i++)
		cin>>x>>y>>z,a[x][y]=min(a[x][y],z),a[y][x]=min(a[y][x],z);
	memset(f,10,sizeof(f));
	f[1]=0;
	for (int i=1;i<=n;i++)
		{
			int k=0;
			for (int j=1;j<=n;j++)
				if (!flag[j]&&(f[j]<f[k])) k=j;
			fa[k]=ff[k];flag[k]=true;
			for (int j=1;j<=n;j++)
				if (f[j]>f[k]+a[k][j])f[j]=f[k]+a[k][j],ff[j]=k;
		}
	dfs(n);
	cout<<ans;
}

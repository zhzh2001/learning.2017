#include<bits/stdc++.h>
using namespace std;
int n,m,x[10000],y[10000];
long long f[10000];
bool flag[10000];
int main()
{
	freopen("meadow.in","r",stdin);freopen("meadow.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m;
	for (int i=1;i<=n;i++)cin>>x[i]>>y[i];
	for (int i=2;i<=n;i++)f[i]=1e15;
	f[0]=1e15;
	for (int i=1;i<=n;i++)
		{
			int k=0;
			for (int j=1;j<=n;j++)
				if (f[j]<f[k]&&!flag[j])k=j;
			flag[k]=true;
			for (int j=1;j<=n;j++)
				if (!flag[j])
					f[j]=min(f[j],1ll*(x[k]-x[j])*(x[k]-x[j])+1ll*(y[k]-y[j])*(y[k]-y[j]));
		} 
	sort(f+1,f+1+n);
	printf("%.2f",sqrt(double(f[n-m+1])));
}

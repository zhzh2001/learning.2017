#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;
const int N=5005;
const int INF=1e9;
int n,p,m,root;
int lines,front[N];
struct edge{int next,to;}e[N*2];
void addline(int x,int y){e[++lines]=(edge){front[x],y};front[x]=lines;}
int deep[N],fa[N];
void Pre(int u,int d){
	deep[u]=d;
	for (int i=front[u],v;i!=0;i=e[i].next) fa[v=e[i].to]=u,Pre(v,d+1);
}
bool mat[N];
struct node{int v,p;};
node Dfs(int u){
//	printf("%d\n",u);
	node res;
	if (!mat[u]){res.p=u;res.v=0;}
		else res.p=res.v=INF;
	for (int i=front[u];i!=0;i=e[i].next){
		node tmp=Dfs(e[i].to);
		if (tmp.v+1<res.v){res.v=tmp.v+1;res.p=tmp.p;}
	}
	return res;
}
int ingree[N],heavy[N];
int main(){
	freopen("parking.in","r",stdin);
	freopen("parking.out","w",stdout);
	scanf("%d%d%d",&n,&p,&m);
	for (int i=1,x;i<=n;i++){
		scanf("%d",&x);
		for (int j=1,y;j<=x;j++){
			scanf("%d",&y);
			addline(i,y);ingree[y]++;
		}
	}
	for (int i=1,x;i<=m;i++) scanf("%d",&x),mat[x]=true;
	for (int i=1;i<=n;i++) if (!ingree[i]) root=i;
	Pre(root,1);
	for (int now=p;now!=root;now=fa[now]) heavy[fa[now]]=now;
	int ans=0;
	for (int now=fa[p];now!=0;now=fa[now])
		if (mat[now]){
//			printf("Deal:%d(%d)\n",now,ans);
			node res;res.v=res.p=INF;
			for (int k=now,dep=1;k!=p;k=heavy[k],dep++){
				for (int i=front[k],v;i!=0;i=e[i].next)
					if ((v=e[i].to)!=heavy[k]){
						node tmp=Dfs(v);tmp.v+=dep;
						if (tmp.v<res.v) res=tmp;
					}
				}
			if (res.p==INF){printf("No solution\n");return 0;}
			mat[now]=false;
			mat[res.p]=true;
			ans+=res.v;
		}
	printf("%d\n",ans);
}

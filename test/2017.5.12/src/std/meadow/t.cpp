#include<cstdio>
#include<cmath>
#include<algorithm>
typedef double ld;
const int N=2005;
int i,j,k,n,m,x[N],y[N],f[N];
struct E{
	int x,y;ld w;
	bool operator<(const E&a)const{
		return w<a.w;
	}
}a[1999003];
ld S(int x,int y){return sqrt(x*x+y*y);}
int get(int i){return f[i]==i?i:f[i]=get(f[i]);}
int main()
{
	freopen("meadow.in","r",stdin);
	freopen("meadow.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;f[i]=i++)
	{
		scanf("%d%d",&x[i],&y[i]);
		for(j=1;j<i;j++)
			a[++k].x=i,a[k].y=j,a[k].w=S(x[i]-x[j],y[i]-y[j]);
	}
	std::sort(a+1,a+k+1);
	for(i=1;m!=n;i++)
	{
		j=get(a[i].x),k=get(a[i].y);
		if(j==k)continue;
		f[j]=k,m++;
	}
	printf("%.2lf",a[i-1].w);
}

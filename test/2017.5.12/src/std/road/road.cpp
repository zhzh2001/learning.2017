#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>

using namespace std;
const int N=1005;
const int INF=1e9;
int n,m,lines,front[N],dist[N];
struct edge{int next,to,wei;}e[N*N];
bool able[N*N];
void addline(int x,int y,int z){e[++lines]=(edge){front[x],y,z};front[x]=lines;}
int pre[N],prv[N];
queue<int>que;
bool inq[N];
void Spfa(){
	memset(dist,127,sizeof(dist));
	que.push(1);
	dist[1]=0;inq[1]=true;
	while (!que.empty()){
		int u=que.front();que.pop();
		for (int i=front[u],v;i!=-1;i=e[i].next)
			if (dist[u]+e[i].wei<dist[v=e[i].to]){
				dist[v]=dist[u]+e[i].wei;
				pre[v]=u,prv[v]=i;
				if (!inq[v]){que.push(v);inq[v]=true;}
			}
		inq[u]=false;
	}
}
bool vist[N];
void relax(int u){
	for (int i=front[u],v;i!=-1;i=e[i].next)
		if (!vist[v=e[i].to] && !able[i]) dist[v]=min(dist[v],dist[u]+e[i].wei);
}
int find(){
	int res,val=INF;
	for (int i=1;i<=n;i++)
		if (!vist[i] && dist[i]<val){val=dist[i];res=i;}
	return val<INF?res:0;
}
int Dijkstra(){
	memset(vist,0,sizeof(vist));
	for (int i=1;i<=n;i++) dist[i]=INF;
	dist[1]=0;vist[1]=true;
	relax(1);
	for (int u=find();u!=0;u=find()) vist[u]=true,relax(u);
	return dist[n];
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	lines=-1;
	memset(front,-1,sizeof(front));
	for (int i=1,x,y,z;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		addline(x,y,z),addline(y,x,z);
	}
	Spfa();
	int ans=0;
	for (int i=n;i!=1;i=pre[i]){
		int g=prv[i];
		able[g]=able[g^1]=true;
		int dis=Dijkstra();
		if (dis!=INF) ans=max(ans,dis);
		able[g]=able[g^1]=false;
	}
	printf("%d\n",ans);
}

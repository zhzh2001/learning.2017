#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;
const int N=200;
char s[1000];
int n,a[N],b[N],c[N],f[1<<16][150],ans;
int main(){
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){scanf("%s",s);a[i]=s[0];b[i]=s[(c[i]=strlen(s))-1];}
	int len=1<<n;
	for (int i=1;i<len;i++)
		for (int k=1,j=1;k<=i;k=k<<1,j++)
			if (i&k) ans=max(ans,f[i][b[j]]=max(f[i][b[j]],f[i^k][a[j]]+c[j]));
	printf("%d\n",ans);
}

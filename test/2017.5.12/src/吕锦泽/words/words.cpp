#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=20;
string ch[N];
int len[N];
bool vis[N];
int n,ans;

void dfs(int s,int num){
	if(num>ans) ans=num;
	for(int i=1;i<=n;i++)
		if(!vis[i]){
			if(ch[s][len[s]-1]==ch[i][0]){
				vis[i]=true;
				dfs(i,num+len[i]);
				vis[i]=false;
			}
		}
}

int main(){
//	freopen("words.in","r",stdin);
//	freopen("words.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		cin>>ch[i];
		len[i]=ch[i].length();
	}
	for(int i=1;i<=n;i++){
		memset(vis,false,sizeof(vis));
		vis[i]=true;
		dfs(i,len[i]);
	}
	printf("%d",ans);
	return 0;
}

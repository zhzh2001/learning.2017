#include<iostream>
#include<cstdio>
#include<vector>
#include<queue>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=1005,inf=0x7fffffff;
struct wi{ int v,w; };
vector<wi> edge[N];
bool vis[N];
int dist[N],last[N];
int n,m,ans;

void Dij1(){
	memset(dist,0x7f,sizeof dist);
	dist[1]=0; vis[1]=true;
	for(int i=0;i<edge[1].size();i++) if(dist[edge[1][i].v]>edge[1][i].w) dist[edge[1][i].v]=edge[1][i].w,last[edge[1][i].v]=1;
	for(int t=1;t<=n-1;t++){
		int mn=inf,x;
		for(int i=1;i<=n;i++)
			if(!vis[i])
				if(dist[i]<mn){
					mn=dist[i];
					x=i;
				}
		printf("%d %d\n",mn,x);
		vis[x]=true;
		if(x==n) return;
		for(int i=0;i<edge[x].size();i++){
			wi v=edge[x][i];
			if(!vis[v.v]){
				if(dist[v.v]>dist[x]+v.w){
					dist[v.v]=dist[x]+v.w;
					last[v.v]=x;
				}
			}
		}
	}
}

bool check(int x,int y,int u,int v){
	if((x==u&&y==v)||(x==v&&y==u)) return false;
	return true;
}

void Dij2(int x,int y){
	memset(dist,0x3f,sizeof dist);
	dist[1]=0; vis[1]=true;
	for(int i=0;i<edge[1].size();i++) if(check(x,y,1,edge[1][i].v)&&dist[edge[1][i].v]>edge[1][i].w) dist[edge[1][i].v]=edge[1][i].w;
	for(int t=1;t<=n-1;t++){
		int mn=inf,p;
		for(int i=1;i<=n;i++)
			if(!vis[i])
				if(dist[i]<mn){
					mn=dist[i];
					p=i;
				}
		vis[p]=true;
		if(p==n) return;
		for(int i=0;i<edge[p].size();i++){
			wi v=edge[p][i];
			if(!vis[v.v]&&check(x,y,p,v.v))
				if(dist[v.v]>dist[p]+v.w)
					dist[v.v]=dist[p]+v.w;
		}
	}
}

int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read(),w=read();
		wi p; p.v=v; p.w=w;
		edge[u].push_back(p);
		p.v=u;
		edge[v].push_back(p);
	}
	Dij1();
	int p=n;
	while(p!=1){
		memset(vis,false,sizeof vis);
		Dij2(p,last[p]);
		ans=max(ans,dist[n]);
		p=last[p];
	}
	printf("%d",ans);
	return 0;
}

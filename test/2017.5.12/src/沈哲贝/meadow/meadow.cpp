#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define maxn 5050
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct data{
	ll x,y,w;
}q[2100000];
ll x[2010],y[2010],fa[2010],n,m,tot,a,b;
bool cmp(data a,data b){	return a.w<b.w;	}
ll find(ll x){	return x==fa[x]?x:fa[x]=find(fa[x]);}
int main(){
	freopen("meadow.in","r",stdin);
	freopen("meadow.out","w",stdout);
	n=read();	m=read()-1;
	if (n==m+1)	return puts("0.00"),0;
	For(i,1,n)	x[i]=read(),y[i]=read();
	For(i,1,n-1)	For(j,i+1,n)	q[++tot].x=i,q[tot].y=j,q[tot].w=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]);
	sort(q+1,q+tot+1,cmp);
	For(i,1,n)	fa[i]=i;
	if (!m)	return printf("%.2lf",(double)(sqrt(q[tot].w))),0;
	For(i,1,tot){
		a=find(q[i].x),b=find(q[i].y);
		if (a!=b){
			fa[a]=b;	--m;
			if (!m)	return printf("%.2lf",(double)(sqrt(q[i].w))),0;
		}
	}
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<ctime>
#define ll int
#define maxn 1010
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll tmp,x[maxn*maxn],y[maxn*maxn],w[maxn*maxn],dis[maxn],dis1[maxn],dis2[maxn],mp[maxn][maxn],n,m,ans=0,tim;	bool vis[maxn];
ll Spfa(ll dis[],ll st){
	For(i,1,n)	vis[i]=0,dis[i]=1e9;
	dis[0]=1e9+1;	dis[st]=0;
	For(i,1,n){
		ll cho=0;
		For(j,1,n)	if (!vis[j]&&dis[j]<dis[cho])	cho=j;
		vis[cho]=1;
		For(j,1,n)	if (mp[cho][j])	dis[j]=min(dis[j],dis[cho]+mp[cho][j]);
	}
}
ll spfa(ll dis[],ll st){
	For(i,1,n)	vis[i]=0,dis[i]=1e9;
	dis[0]=1e9+1;	dis[st]=0;
	For(i,1,n){
		ll cho=0;
		For(j,1,n)	if (!vis[j]&&dis[j]<dis[cho])	cho=j;
		vis[cho]=1;
		if (cho==n)	return dis[n];
		For(j,1,n)	if (mp[cho][j])	dis[j]=min(dis[j],dis[cho]+mp[cho][j]);
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		x[i]=read();	y[i]=read();	mp[x[i]][y[i]]=mp[y[i]][x[i]]=w[i]=read();
	}
	Spfa(dis1,1);	Spfa(dis2,n);
	FOr(i,m,1){
		if(clock()>500)
			break;
		if ((dis1[x[i]]+dis2[y[i]]+mp[x[i]][y[i]]==dis1[n])||(dis2[x[i]]+dis1[y[i]]+mp[x[i]][y[i]]==dis1[n]))
		mp[x[i]][y[i]]=mp[y[i]][x[i]]=1e9,ans=max(ans,spfa(dis,1)),mp[x[i]][y[i]]=mp[y[i]][x[i]]=w[i];
	}
	writeln(ans);
}

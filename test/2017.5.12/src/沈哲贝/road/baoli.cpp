#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define maxn 2010
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll x[maxn*maxn],y[maxn*maxn],w[maxn*maxn],dis[maxn],q[maxn],mp[maxn][maxn],n,m,ans=0;	bool mark[maxn];
ll spfa(){
	ll he=0,ta=1;	q[ta]=1;
	memset(dis,60,sizeof dis);	dis[1]=0;
	while(he!=ta){
		ll x=q[he=he%n+1];
		For(y,1,n)	if (mp[x][y]&&dis[y]>dis[x]+mp[x][y]){
			dis[y]=dis[x]+mp[x][y];
			if (!mark[y])	q[ta=ta%n+1]=y,mark[y]=1;
		}mark[x]=0;
	}return dis[n];
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		x[i]=read();	y[i]=read();	mp[x[i]][y[i]]=mp[y[i]][x[i]]=w[i]=read();
	}
	For(i,1,m)	mp[x[i]][y[i]]=mp[y[i]][x[i]]=0,ans=max(ans,spfa()),mp[x[i]][y[i]]=mp[y[i]][x[i]]=w[i];
	writeln(ans);
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define maxn 10050
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll ans,rt,zyy[maxn],fa[maxn],dep[maxn],head[maxn],next[maxn],q[maxn],vet[maxn],tot,Q[maxn],n,p,k;	bool mark[maxn];
void insert(ll x,ll y){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void dfs(ll x){
	for(ll i=head[x];i;i=next[i])	dep[vet[i]]=dep[x]+1,dfs(vet[i]);
}
ll bfs(ll x){
	ll he=0,ta=1;	Q[ta]=x;
	while(he!=ta){
		ll x=Q[++he];
		for(ll i=head[x];i;i=next[i])
		if (vet[i]!=p)
			if (!mark[vet[i]]&&!zyy[vet[i]]){
				mark[vet[i]]=1;	return dep[vet[i]];
			}else	Q[++ta]=vet[i];
	}
}
int main(){
	freopen("parking.in","r",stdin);
	freopen("parking.out","w",stdout);
	n=read();	p=read();	k=read();
	For(i,1,n){
		ll k=read();
		while(k--){
			ll x=read();
			insert(i,x);
			fa[x]=i;
		}
	}
	For(i,1,n)	if (!fa[i])	rt=i;
	dfs(rt);
	For(i,1,k){	ll x=read();	mark[x]=1;	}	tot=0;
	for(ll tmp=p;;tmp=fa[tmp]){
		if (mark[tmp])	q[++tot]=tmp;
		zyy[tmp]=1;
		if (tmp==rt)	break;
	}
	For(i,1,tot)	ans+=bfs(q[i])-dep[q[i]],mark[q[i]]=0;
	writeln(ans);
}

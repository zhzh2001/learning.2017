#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
char s[100000];
ll bin[30],next[1000],head[1000],vet[1000],value[1000],st[1000],ed[1000],tot,n,answ,mark[140000];	bool f[140000][20];
void insert(ll x,ll y){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
int main(){
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	bin[0]=1;	For(i,1,20)	bin[i]=bin[i-1]<<1;
	n=read();
	For(i,1,n){
		scanf("%s",s+1);
		value[i]=strlen(s+1);
		st[i]=s[1];
		ed[i]=s[strlen(s+1)];
	}
	For(i,1,n)	For(j,1,n)	if (i!=j&&ed[i]==st[j])	insert(i,j);
	For(i,1,bin[n]-1){
		if ((i&-i)==i)	For(j,1,n)	if (i&bin[j-1])	f[i][j]=1;
		For(j,1,n)	if (f[i][j])	for(ll k=head[j];k;k=next[k])	if (!(i&bin[vet[k]-1]))	f[i|bin[vet[k]-1]][vet[k]]=1;
	}
	For(i,1,bin[n]-1){
		ll ans=0;	bool flag=0;
		For(j,1,n){
			flag|=f[i][j];	if (i&bin[j-1])	ans+=value[j];
		}
		if (flag)	answ=max(answ,ans);
	}
	writeln(answ);
}

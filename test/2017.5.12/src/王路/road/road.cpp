#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <string>
#include <cassert>
using namespace std;

ifstream fin("road.in");
ofstream fout("road.out");

const int N = 1005;
int head[N];
struct EdgeType {
	int to, w, nxt;
	bool isTrue;
} e[N * N / 2];

int n, m, cnt;

inline void AddEdge(int u, int v, int w) {
	e[++cnt].to = v;
	e[cnt].w = w;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

int dist[1005];
bool vis[1005];

int Dijkstra(int s) {
	memset(dist, 0x3f, sizeof dist);
	memset(vis, 0, sizeof vis);
	vis[s] = true;
	for (int i = head[s]; ~i; i = e[i].nxt)
		if (e[i].isTrue == false) dist[e[i].to] = e[i].w;
	for (int i = 1; i < n; i++) {
		int v, Min = 0x3f3f3f3f;
		for (int j = 1; j <= n; j++) if (!vis[j] && dist[j] < Min)
			Min = dist[j], v = j;
		vis[v] = true;
		for (int i = head[v]; ~i; i = e[i].nxt) {
			if (!vis[e[i].to] && e[i].isTrue == false)
				dist[e[i].to] = min(dist[e[i].to], dist[v] + e[i].w);
		}
	}
	return dist[n];
}

int main() {
	memset(head, -1, sizeof head);
	fin >> n >> m;
	for (int i = 1; i <= m; i++) {
		int u, v, w;
		fin >> u >> v >> w;
		AddEdge(u, v, w);
		AddEdge(v, u, w);
	}
	int ans = 0;
	for (int i = 1; i <= cnt; i++) {
		e[i].isTrue = true;
		ans = max(ans, Dijkstra(1));
		e[i].isTrue = false;
	}
	cout << ans << endl;
	fout << ans << endl;
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

ifstream fin("parking.in");
ofstream fout("parking.out");

const int N = 5005;

typedef struct {
	int nxt, to;
	bool up;
} Edge;

Edge e[N];
int n, p, k, head[N], cnt, pos[N];
int belong[N], imp[N], size[N], Num[N];

inline void AddEdge(int u, int v, bool flag) {
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	e[cnt].up = flag;
	head[u] = cnt;
}

bool vis[N];
int res = 0;

void init(int x) {
	vis[x] = true;
	size[x] = 1;
	Num[x] = belong[x] > 0;
	for (int i = head[x]; ~i; i = e[i].nxt) {
		if (!vis[e[i].to]) {
			init(e[i].to);
			size[x] += size[e[i].to];
			Num[x] += Num[e[i].to];
		}
	}
}

void down(int x) {
	for (int i = head[x]; ~i; i = e[i].nxt) {
		if (e[i].up == false && size[e[i].to] > Num[e[i].to] && e[i].to != ::p) {
			if (belong[e[i].to] > 0) down(e[i].to);
			Num[e[i].to]++;
			res++;
			belong[pos[x]] = 0;
			pos[x] = e[i].to;
			belong[pos[x]] = x;
		}
	}
}

void dfs(int p) {
	imp[p] = true;
	if (belong[p] != 0)
		down(pos[belong[p]]);
	for (int i = head[p]; ~i; i = e[i].nxt) {
		if (e[i].up) {
			dfs(e[i].to);
		}
	}
}

int main() {
	memset(head, -1, sizeof head);
	fin >> n >> p >> k;
	for (int i = 1, T, x; i <= n; i++) {
		fin >> T;
		for (int j = 1; j <= T; j++) {
			fin >> x;
			AddEdge(i, x, false);
			AddEdge(x, i, true);
		}
	}
	for (int i = 1; i <= k; i++) {
		fin >> pos[i];
		belong[pos[i]] = i;
	}
	init(1);
	dfs(p);
	fout << res << endl;
	cout << res << endl;
	return 0;
}
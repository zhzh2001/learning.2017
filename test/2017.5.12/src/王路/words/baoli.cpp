#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <string>
#include <cassert>
using namespace std;

#pragma GCC optimize(2)

const int N = 18, LIM = 1 << 16;

ifstream fin("words.in");
ofstream fout("baoli.out");

typedef long long ll;

string str;

struct Word {
	int length;
	char first, last;
} word[N];

int n;
bool vis[N];
int res = 0;

void dfs(int dep, int ans, int lastword) {
	if (dep == n)
		return;
	res = max(res, ans);
	if (lastword == -1) {
		for (int i = 0; i < n; i++) {
			if (!vis[i])  {
				vis[i] = true;
				dfs(dep + 1, ans + word[i].length, i);
				vis[i] = false;
			}
		}
	} else {
		for (int i = 0; i < n; i++) {
			if (word[lastword].last == word[i].first && !vis[i]) {
				vis[i] = true;
				dfs(dep + 1, ans + word[i].length, i);
				vis[i] = false;
			}
		}
	}
}

int main() {
	fin >> n;
	for (int i = 0; i < n; i++) {
		fin >> str;
		word[i].length = str.length();
		word[i].first = str[0];
		word[i].last = str[str.length() - 1];
	}
	dfs(1, 0, -1);
	fout << res << endl;
	cout << res << endl;
	return 0;
}
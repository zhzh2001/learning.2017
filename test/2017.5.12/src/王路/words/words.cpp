#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <string>
#include <cassert>
using namespace std;

#pragma GCC optimize(2)

const int N = 18, LIM = 1 << 16;

ifstream fin("words.in");
ofstream fout("words.out");

typedef long long ll;

string str;
int n, Popcount[LIM];

struct Word {
	int length;
	char first, last;
} word[N];

void init() {
	for (int i = 0; i < LIM; i++)
		for (int j = 0; j < 16; j++)
			if (i & (1 << j)) ++Popcount[i];
}

int f[N][N][LIM];

int main() {
	fin >> n;
	for (int i = 0; i < n; i++) {
		fin >> str;
		word[i].length = str.length();
		word[i].first = str[0];
		word[i].last = str[str.length() - 1];
	}
	init();
	/*
	for (int i = 0; i < 256; i++)
		assert(__builtin_popcount(i) == Popcount[i]);
	*/
	int lim = (1 << n);
	memset(f, 0, sizeof f);
	for (int i = 0; i < n; i++)
		f[1][i][1 << i] = word[i].length;
	for (int i = 1; i < n; i++) {
		for (int p = 0; p < lim; p++) {
			if (Popcount[p] != i) continue;
			for (int k = 0; k < n; k++) {
				if (Popcount[p ^ (1 << k)] != i + 1) continue;
				for (int j = 0; j < n; j++) {
					if (word[k].first != word[j].last) continue;
					f[i + 1][k][p ^ (1 << k)] = max(f[i + 1][k][p ^ (1 << k)], f[i][j][p] + word[k].length);
					// printf("f[%d][%d][%d] = max(f[%d][%d][%d] + %d);%d\n", i, j, p, i - 1, k, p ^ (1 << k), word[j].length, f[i][j][p]);
				}
			}
		}
	}
	int res = 0;
	for (int k = 1; k <= n; k++)
		for (int i = 0; i < n; i++)
			for (int j = 0; j < lim; j++)
				res = max(res, f[k][i][j]);
	fout << res << endl;
	cout << res << endl;
	return 0;
}
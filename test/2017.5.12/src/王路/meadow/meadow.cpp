#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <iomanip>
#include <cassert>
#include <cstring>
#include <cmath>
using namespace std;

ifstream fin("meadow.in");
ofstream fout("meadow.out");

const int N = 2005;
const double eps = 1e-6;

struct Point {
	double x, y;
} P[N];

int n, m;

inline double sqr(double x) {
	return x * x;
}

inline double dis(int a, int b) {
	return sqr(P[a].x - P[b].x) + sqr(P[a].y - P[b].y);
}

bool vis[N];
int cnt;

int head[N];
struct EdgeType {
	int to, w, nxt;
} e[N * N];
int Num;

int dist[N];

int Dijkstra(int s) {
	memset(dist, 0x3f, sizeof dist);
	memset(vis, 0, sizeof vis);
	vis[s] = true;
	for (int i = head[s]; ~i; i = e[i].nxt)
		dist[e[i].to] = e[i].w;
	for (int i = 1; i < Num; i++) {
		int v, Min = 0x3f3f3f3f;
		for (int j = 1; j <= Num; j++) if (!vis[j] && dist[j] < Min)
			Min = dist[j], v = j;
		vis[v] = true;
		for (int i = head[v]; ~i; i = e[i].nxt) {
			if (!vis[e[i].to])
				dist[e[i].to] = min(dist[e[i].to], dist[v] + e[i].w);
		}
	}
	int res = 0;
	for (int i = 1; i <= Num; i++) {
		if (i != s)
			res = max(res, dist[i]);
	}
	return res;
}

bool isExist[N];
int label;

void AddEdge(int u, int v, int w) {
	e[++label].to = v;
	e[label].w = w;
	e[label].nxt = head[u];
	head[u] = label;
}

bool check(double mid) {
	int Tot = 0;
	memset(isExist, 0, sizeof isExist);
	for (int i = 1; i <= n; i++) {
		if (!isExist[i]) {
			Tot++;
			if (Tot > m) return false;
			for (int j = 1; j <= n; j++) {
				if (!isExist[j] && dis(i, j) < mid + eps)
					isExist[j] = true;
			}
		}
	}
	return true;
}

int main() {
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
		fin >> P[i].x >> P[i].y;
	double l = .0, r = .0;
	for (int i = 1; i <= n; i++)
		for (int j = i + 1; j <= n; j++) {
			r = max(r, dis(i, j));
		}
	double res;
	while (l < r) {
		double mid = (l + r) / 2.0;
		if (check(mid)) res = mid, r = mid - eps;
		else l = mid + eps;
	}
	fout << setprecision(2) << fixed << sqrt(res) << endl;
	cout << setprecision(2) << fixed << sqrt(res) << endl;
	return 0;
}
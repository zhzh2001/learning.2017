#include<iostream>
#include<cstdio>
#include<cstring>
#define Ll long long
using namespace std;
struct cs{int to,v,nxt;}a[200];
int in[2005],f[50][50];
bool vi[202];
int head[200],ll;
int n,ans;
char c[1000];
void init(int x,int y,int z){
	a[++ll].to=y;
	a[ll].v=z;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs(int x,int sum,int now){
	if(f[now][x]>=sum)return;
	f[now][x]=sum;
	if(sum>ans)ans=sum;
	for(int k=head[x];k;k=a[k].nxt)
		if(!vi[k]){
			vi[k]=1;
			dfs(a[k].to,sum+a[k].v,now+1);
			vi[k]=0;
		}
}
int main()
{
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%s",c+1);
		init(c[1],c[strlen(c+1)],strlen(c+1));
		in[i]=c[1];
	}
	for(int i=1;i<=n;i++){
		memset(vi,0,sizeof vi);
		memset(f,-1,sizeof f);
		dfs(in[i],0,0);
	}
	printf("%d",ans);
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#define Ll long long
using namespace std;
struct cs{int to,v,nxt;}a[2000000];
int head[1005],ll;
int d[1005],c[1005],top;
bool in[1005],ok;
int n,m,x,y,z,no,sum,ans;
void init(int x,int y,int z){
	a[++ll].to=y;
	a[ll].v=z;
	a[ll].nxt=head[x];
	head[x]=ll;
}
int spfa(int S,int E){
	for(int i=0;i<=n;i++)d[i]=3e9,in[i]=0;
	queue<int>Q;
	Q.push(S);d[S]=0;
	while(!Q.empty()){
		int x=Q.front();Q.pop(); in[x]=0;
		for(int k=head[x];k;k=a[k].nxt)
			if(k!=no)
				if(d[a[k].to]>d[x]+a[k].v){
					d[a[k].to]=d[x]+a[k].v;
					if(!in[a[k].to]){
						in[a[k].to]=1;
						Q.push(a[k].to);
					}
				}
	}return d[E];
}
void dfs(int x,int v){
	if(v>sum||ok)return;
	if(x==n){ok=1;return;}
	for(int k=head[x];k;k=a[k].nxt){
		c[++top]=k;dfs(a[k].to,v+a[k].v);if(ok)return;top--;
	}
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		init(x,y,z);
		init(y,x,z);
	}
	sum=spfa(1,n);
	dfs(1,0);
	for(int i=1;i<=top;i++){
		no=c[i];
		ans=max(ans,spfa(1,n));
	}
	printf("%d",ans);
}

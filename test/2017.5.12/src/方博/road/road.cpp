#include<bits/stdc++.h>
using namespace std;
int a[1005][1005];
int p[1005];
bool b[1005][1005],bb;
int n,m;
int dij()
{
	int dis[1005];
	int vis[1005]={false};
	memset(dis,127/3,sizeof(dis));
	dis[1]=0;
	for(int i=1;i<=n;i++){
		int x=dis[0];
		int k=-1;
		for(int j=1;j<=n;j++)
			if(dis[j]<x&&!vis[j]){
				k=j;
				x=dis[j];
			}
		if(vis[k]||k==-1)return dis[n];
		vis[k]=true;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&dis[k]+a[j][k]<dis[j]&&b[k][j]==false){
				if(!bb)p[j]=k;
				dis[j]=dis[k]+a[j][k];
			}
	}
	return dis[n];
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,127/3,sizeof(a));
	for(int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=a[y][x]=z;
	}
//	cout<<dij()<<endl;
	int x=n;
	bb=true;
	int ans=0;
	while(x!=0){
		b[x][p[x]]=true;
		b[p[x]][x]=true;
//		cout<<x<<' '<<p[x]<<' '<<dij()<<endl;
		//cout<<n<<endl;
		ans=max(ans,dij());
		b[x][p[x]]=false;
		b[p[x]][x]=false;
		x=p[x];
	}
	printf("%d",ans);
}

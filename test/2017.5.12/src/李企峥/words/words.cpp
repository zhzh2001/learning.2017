#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	int x,y,l;
}a[20];
ll mx;
int n,l;
int b[10][10][20];
int c[10][10];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int zn(char ch)
{
	if(ch=='A')return 1;
	if(ch=='E')return 2;
	if(ch=='I')return 3;
	if(ch=='O')return 4;
	if(ch=='U')return 5;
	return 6;
}
void dfs(int i,int ans)
{
	if(ans>mx)mx=ans;
	For(j,1,5)
	{
		if(c[i][j]<b[i][j][0])
		{
			c[i][j]++;
			dfs(j,ans+b[i][j][c[i][j]]);
			c[i][j]--;
		}
	}
}
int main()
{
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	//A,E,I,O,U
	n=read();
	For(i,1,n)
	{
		char ch=getchar();
		l=0;
		while(zn(ch)==6)ch=getchar();
		a[i].x=zn(ch);
		while(zn(ch)!=6)a[i].y=zn(ch),l++,ch=getchar();
		a[i].l=l;
	} 
	For(i,1,n)
		For(j,i+1,n)
			if(a[i].l<a[j].l)swap(a[i],a[j]);
	For(i,1,n)
	{
		b[a[i].x][a[i].y][0]++;
		b[a[i].x][a[i].y][b[a[i].x][a[i].y][0]]=a[i].l;
	}
	mx=0;
	For(i,1,5)	
	{
		For(j,1,5)For(k,1,5)c[j][k]=0;
		dfs(i,0);
	}
	cout<<mx<<endl;
	return 0;
}


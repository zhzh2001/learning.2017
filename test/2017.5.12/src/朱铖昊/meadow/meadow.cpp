#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=2005;
int x[N],y[N],f[N],n,m,k,l;
struct da
{
	int x,y,z;
}a[2100000],z;
inline bool cmp(da a,da b)
{
	return a.z<b.z;
}
inline int find(int x)
{
	if (f[x]==x)
		return x;
	else
		return f[x]=find(f[x]);
}
int main()
{
	freopen("meadow.in","r",stdin);
	freopen("meadow.out","w",stdout);
	read(n);
	read(m);
	m=n-m;
	for (int i=1;i<=n;++i)
	{
		read(x[i]);
		read(y[i]);
	}
	for (int i=1;i<=n;++i)
		for (int j=i+1;j<=n;++j)
		{
			z.x=i;
			z.y=j;
			z.z=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]);
			a[++l]=z;
		}
	sort(a+1,a+1+l,cmp);
	for (int i=1;i<=n;++i)
		f[i]=i;
	for (int i=1;i<=l;++i)
	{
		int xx=find(a[i].x),yy=find(a[i].y);
		if (xx==yy)
			continue;
		f[xx]=yy;
		++k;
		if (k==m)
		{
			printf("%.2lf",sqrt(a[i].z));
			return 0;
		}
	}
}
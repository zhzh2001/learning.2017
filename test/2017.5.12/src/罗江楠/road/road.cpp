#include <bits/stdc++.h>
#define N 1005
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int n, m, dis[N], vis[N];
struct node{
	int to, nxt, val;
}e[N*(N-1)/2];
struct qq{
	int to, val;
	bool operator < (const qq &b) const {
		return val < b.val;
	}
};
priority_queue<qq> q;
int head[N], cnt;
void ins(int x, int y, int val){
	e[++cnt] = (node){y, head[x], val};
	head[x] = cnt;
}
int main(){
	freopen("road.in", "r", stdin);
	freopen("road.out", "w", stdout);
	n = read(); m = read();
	for(int i = 1, x, y; i <= m; i++){
		x = read(); y = read();
		ins(x, y, read());
	}
	q.push((qq){1, 0});
	while(!q.empty()){
		qq tmp = q.top(); q.pop();
		if(dis[tmp.to] != tmp.val) continue;
		vis[tmp.to] = 1;
		for(int i = head[tmp.to]; i; i = e[i].nxt){
			if(dis[e[i].to] < dis[tmp.to]+e[i].val){
				dis[e[i].to] = dis[tmp.to]+e[i].val;
				if(!vis[e[i].to]) q.push((qq){e[i].to, dis[e[i].to]});
			}
		}
	}
	printf("%d\n", dis[n]);
	return 0;
}
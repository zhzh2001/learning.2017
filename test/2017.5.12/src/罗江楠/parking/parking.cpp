#include <bits/stdc++.h>
#define N 5005
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int n, p, m, fa[N], a[N], sn[N], sz[N];
vector<int> son[N];
int calc(int x){
	sn[x] = 1<<30;
	for(int i = 0; i < son[x].size(); i++){
		sn[x] = min(sn[x], sn[son[x][i]]+1);
	}
	return sn[x];
}
int dfs(int x){
	sz[x] = 1;
	for(int i = 0; i < son[x].size(); i++)
		sz[x] += dfs(son[x][i]);
	return sz[x];
}
int main(){
	freopen("parking.in", "r", stdin);
	freopen("parking.out", "w", stdout);
	n = read(); p = read(); m = read();
	for(int i = 1, x, y; i <= n; i++){
		x = read();
		while(x--){
			fa[y=read()] = i;
			son[i].push_back(y);
		}
	}
	dfs(1);
	for(int i = 1; i <= m; i++) a[read()] = 1;
	a[p] = 2;
	int tmp = fa[p], ans = 0;
	while(tmp != 1){
		ans += calc(tmp);
		tmp = fa[tmp];
	}
	printf("%d\n", ans);
	return 0;
}
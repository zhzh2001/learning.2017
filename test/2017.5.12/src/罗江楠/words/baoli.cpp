#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int n, ans;
struct node{
	int len;
	char st, ed;
}a[20];
int vis[20];
string str;
void dfs(char x, int len){
	for(int i = 1; i <= n; i++)if(!vis[i]&&a[i].st == x){
		vis[i] = 1;
		dfs(a[i].ed, len+a[i].len);
		vis[i] = 0;
	}
	ans = max(ans, len);
}
int main(){
	n = read();
	for(int i = 1; i <= n; i++){
		cin >> str;
		a[i].st = str[0];
		a[i].len = str.length();
		a[i].ed = str[a[i].len-1];
	}
	dfs('A', 0);dfs('E', 0);dfs('I', 0);dfs('O', 0);dfs('U', 0);
	printf("%d\n", ans);
	return 0;
}
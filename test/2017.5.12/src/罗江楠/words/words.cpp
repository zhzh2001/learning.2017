#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}/*
int n, ans;
struct node{
	int len;
	char st, ed;
}a[20];
int pos[20];
string str;
int main(){
	n = read();
	for(int i = 1; i <= n; i++){
		cin >> str; pos[i] = i;
		a[i].st = str[0];
		a[i].len = str.length();
		a[i].ed = str[a[i].len-1];
	}
	for(int i = 1; i <= 100000; i++){
		char ed = a[pos[1]].ed;
		int len = a[pos[1]].len;
		for(int j = 2; j <= n; j++)
			if(a[pos[j]].st == ed){
				ed = a[pos[j]].ed;
				len += a[pos[j]].len;
			}
		ans = max(ans, len);
		for(int j = 1; j <= n; j++)
			swap(pos[j], pos[rand()%j+1]);
	}
	printf("%d\n", ans);
	return 0;
}
/*
Hack!!!

16
OU
AE
OU
IO
IO
OU
AE
UA
IO
EI
UA
AE
EI
AE
EI
UA

16
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA
AA

*/
int n, ans;
int st[20], ed[20], ln[20];
int dp[5][70000];
int calc(char c){
	switch(c){
		case 'A': case 'a': return 0;
		case 'E': case 'e': return 1;
		case 'I': case 'i': return 2;
		case 'O': case 'o': return 3;
		case 'U': case 'u': return 4;
	}
}
string str;
int main(){
	freopen("words.in", "r", stdin);
	freopen("words.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++){
		cin >> str;
		st[i] = calc(str[0]);
		ln[i] = str.length();
		ed[i] = calc(str[ln[i]-1]);
	}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			for(int k = (1<<n)-1; k>=0; k--)
				if(!((k>>j-1)&1))
					dp[ed[j]][k+(1<<j-1)] = max(dp[ed[j]][k+(1<<j-1)], dp[st[j]][k]+ln[j]);
	int ans = 0;
	for(int i = 0; i < 5; i++)
		for(int j = (1<<n)-1; j; j--)
			ans = max(ans, dp[i][j]);
	printf("%d\n", ans);
}
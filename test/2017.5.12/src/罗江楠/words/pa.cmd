@echo off
for /L %%i in (1, 1, 100) do (
	mk > words.in
	echo Running on Test#%%i...
	words < words.in > words.out
	echo Comparing...
	baoli < words.in > words.ans
	fc words.ans words.out > nul
	if errorlevel 1 goto WA
	cls
)
echo Accepted! &pause
exit
:WA
echo Wrong Answer! &pause
exit
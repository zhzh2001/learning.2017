#include<fstream>
#include<cmath>
#include<algorithm>
using namespace std;
ifstream fin("meadow.in");
ofstream fout("meadow.out");
const int N=2005;
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[N*N/2];
int f[N],x[N],y[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
inline int sqr(int x)
{
	return x*x;
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	int en=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
		{
			e[++en].u=i;
			e[en].v=j;
			e[en].w=sqr(x[i]-x[j])+sqr(y[i]-y[j]);
		}
	sort(e+1,e+en+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=1;i<=en;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv)
		{
			f[ru]=rv;
			if(++m==n)
			{
				fout.precision(2);
				fout<<fixed<<sqrt(e[i].w)<<endl;
				return 0;
			}
		}
	}
	fout<<"0.00\n";
	return 0;
}
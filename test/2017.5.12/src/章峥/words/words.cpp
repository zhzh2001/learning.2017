#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("words.in");
ofstream fout("words.out");
const int N=20;
int n,f[1<<16][26],len[N],first[N],last[N];
int dp(int status,int pred)
{
	if(~f[status][pred])
		return f[status][pred];
	int& now=f[status][pred];
	now=0;
	for(int i=1;i<=n;i++)
		if(!(status&(1<<i-1))&&first[i]==pred)
			now=max(now,dp(status|(1<<i-1),last[i])+len[i]);
	return now;
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		string s;
		fin>>s;
		len[i]=s.length();
		first[i]=s[0]-'A';
		last[i]=s[len[i]-1]-'A';
	}
	memset(f,-1,sizeof(f));
	int ans=0;
	for(int i=0;i<26;i++)
		ans=max(ans,dp(0,i));
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
#include<cstring>
#include<ctime>
using namespace std;
ifstream fin("road.in");
ofstream fout("road.out");
const int N=1005,INF=0x3f3f3f3f;
struct edge
{
	int u,v,w;
}e[N*N/2];
int n,mat[N][N],d[N],rd[N],t[N];
bool vis[N];
void dijkstra(int d[],int s,bool flag)
{
	for(int i=0;i<=n;i++)
		d[i]=i==s?0:INF;
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		if(flag&&j==n)
			return;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],d[j]+mat[j][k]);
	}
}
int main()
{
	int m;
	fin>>n>>m;
	memset(mat,0x3f,sizeof(mat));
	for(int i=1;i<=m;i++)
	{
		fin>>e[i].u>>e[i].v>>e[i].w;
		mat[e[i].u][e[i].v]=mat[e[i].v][e[i].u]=e[i].w;
	}
	dijkstra(d,1,false);
	dijkstra(rd,n,false);
	int ans=d[n];
	for(int i=m;i;i--)
	{
		if(clock()>900)
			break;
		if(d[e[i].u]+e[i].w+rd[e[i].v]==d[n]||rd[e[i].u]+e[i].w+d[e[i].v]==d[n])
		{
			mat[e[i].u][e[i].v]=mat[e[i].v][e[i].u]=INF;
			dijkstra(t,1,true);
			ans=max(ans,t[n]);
			mat[e[i].u][e[i].v]=mat[e[i].v][e[i].u]=e[i].w;
		}
	}
	fout<<ans<<endl;
	return 0;
}
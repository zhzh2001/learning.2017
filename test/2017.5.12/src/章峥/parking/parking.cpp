#include<fstream>
#include<vector>
using namespace std;
ifstream fin("parking.in");
ofstream fout("parking.out");
const int N=5005;
vector<int> mat[N];
int f[N],dep[N],into[N],outo[N],now;
bool vis[N];
void dfs(int k)
{
	into[k]=++now;
	for(int i=0;i<mat[k].size();i++)
	{
		dep[mat[k][i]]=dep[k]+1;
		dfs(mat[k][i]);
	}
	outo[k]=++now;
}
inline bool ancestor(int u,int v)
{
	return into[u]<into[v]&&outo[u]>outo[v];
}
int main()
{
	int n,p,k;
	fin>>n>>p>>k;
	for(int i=1;i<=n;i++)
	{
		int t;
		fin>>t;
		while(t--)
		{
			int u;
			fin>>u;
			mat[i].push_back(u);
			f[u]=i;
		}
	}
	for(int i=1;i<=k;i++)
	{
		int u;
		fin>>u;
		vis[u]=true;
	}
	dep[1]=1;
	dfs(1);
	int ans=0;
	for(int u=p;u;u=f[u])
	{
		if(vis[u])
		{
			int now=n,v;
			for(int i=1;i<=n;i++)
				if(!vis[i]&&ancestor(u,i)&&dep[i]-dep[u]<now&&!ancestor(p,i))
				{
					now=dep[i]-dep[u];
					v=i;
				}
			ans+=now;
			vis[v]=true;
		}
		vis[u]=true;
	}
	fout<<ans<<endl;
	return 0;
}
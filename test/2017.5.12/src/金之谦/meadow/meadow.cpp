#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#include<queue>
#include<ctime>
using namespace std;
struct ppap{int x,y,v;}a[4000001];
int n,m,fa[10001],x[10001],y[10001];
inline int sqr(int x){return x*x;}
inline bool cmp(ppap a,ppap b){return a.v<b.v;}
inline int getfather(int v){
	if(fa[v]==v)return v;
	return fa[v]=getfather(fa[v]);
}
int main()
{
	freopen("meadow.in","r",stdin);
	freopen("meadow.out","w",stdout);
	scanf("%d%d",&n,&m);
	int p=n,ans=0,nedge=0;
	for(int i=1;i<=n;i++)scanf("%d%d",&x[i],&y[i]);
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++){
			a[++nedge].x=i;a[nedge].y=j;
			a[nedge].v=sqr(x[i]-x[j])+sqr(y[i]-y[j]);
		}
	sort(a+1,a+nedge+1,cmp);
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1;i<=nedge;i++){
		int fx=getfather(a[i].x),fy=getfather(a[i].y);
		if(fx==fy)continue;
		p--;fa[fx]=fy;ans=a[i].v;
		if(p==m)break;
	}
	double anss=sqrt(ans);
	printf("%.2lf",anss);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#include<queue>
#include<ctime>
using namespace std;
int q[100001],n,m,k,ans=0;
int b[100001];
int fa[100001],ls[100001],rs[100001];
inline void bfs(int x){
	memset(q,0,sizeof q);
	int l=1,r=1,k=1,p=0;q[1]=x;
	while(l<=r){
		int now=q[l];
		if(l>k)p++,k=r;
		if(!b[now]){
			ans+=p;b[now]=1;return;
		}
		if(ls[now])q[++r]=ls[now];
		if(rs[now])q[++r]=rs[now];
		l++;
	}
}
inline void down(int x){
	memset(q,0,sizeof q);
	int l=1,r=1;q[1]=x;
	while(l<=r){
		int now=q[l];b[now]=1;
		if(ls[now])q[++r]=ls[now];
		if(rs[now])q[++r]=rs[now];
		l++;
	}
}
inline void up(int x){
	for(;x;x=fa[x]){
		if(b[x]==2)bfs(x);
		b[x]=1;
	}
}
int main()
{
	freopen("parking.in","r",stdin);
	freopen("parking.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for(int i=1;i<=n;i++){
		int x;scanf("%d",&x);
		if(x==1)scanf("%d",&ls[i]);
		if(x==2)scanf("%d%d",&ls[i],&rs[i]);
		fa[ls[i]]=fa[rs[i]]=i;
	}
	for(int i=1;i<=k;i++){
		int x;scanf("%d",&x);
		b[x]=2;
	}
	down(m);
	up(m);
	printf("%d",ans);
	return 0;
}

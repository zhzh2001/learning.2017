#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<iostream>
#include<cstdlib>
#include<queue>
#include<string>
using namespace std;
int ans=0,n,cnt=0;
bool vis[10001]={0},flag=0;
int nedge=0,p[100001],nex[100001],head[100001];
int s[100001];
char c[17][10001];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int sum,int la){
	if((++cnt)>9000000){flag=1;return;}
	for(int k=head[la];k;k=nex[k])if(!vis[p[k]]){
		vis[p[k]]=1;
		dfs(sum+s[p[k]],p[k]);
		vis[p[k]]=0;
		if(flag)break;
	}
	if(la!=n+1&&sum>ans)ans=sum;
}
int main()
{
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%s",c[i]+1);
		s[i]=strlen(c[i]+1);
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)if(c[i][s[i]]==c[j][1])addedge(i,j);
	for(int i=1;i<=n;i++)addedge(n+1,i);
	dfs(0,n+1);
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
#include<cstdlib>
#include<queue>
#include<ctime>
using namespace std;
struct ppap{int x,v;};
bool operator >(ppap a,ppap b){return a.v>b.v;}
priority_queue<ppap,vector<ppap>,greater<ppap> >q;
int a[1001][1001],ans;
bool vis[1001];
int n,m,dist[1001],la[1001];
int nedge=0,p[1000001],c[1000001],nex[1000001],head[1001];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;
	nex[nedge]=head[x];head[x]=nedge;
}
inline void dijkstra(int x,bool flag){
	for(int i=1;i<=n;i++)dist[i]=1e9;
	while(!q.empty())q.pop();
	memset(vis,0,sizeof vis);
	ppap now,rp;now.x=x;now.v=0;q.push(now);
	dist[x]=0;
	for(int i=1;i<=n;i++){
		now=q.top();q.pop();
		vis[now.x]=1;
		for(int k=head[now.x];k;k=nex[k])if(!vis[p[k]]&&dist[p[k]]>dist[now.x]+c[k]){
			dist[p[k]]=dist[now.x]+c[k];
			if(flag)la[p[k]]=now.x;
			rp.x=p[k];rp.v=dist[p[k]];
			q.push(rp);
		}
	}
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z);
		addedge(y,x,z);
		a[x][y]=a[y][x]=nedge-1;
	}
	dijkstra(1,1);
	for(int k=n;la[k];k=la[k]){
		int i=a[la[k]][k],j=c[i];c[i]=c[i+1]=1e9;
		dijkstra(1,0);
		ans=max(ans,dist[n]);
		c[i]=c[i+1]=j;
	}
	printf("%d",ans);
	return 0;
}

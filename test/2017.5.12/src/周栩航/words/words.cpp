#include<bits/stdc++.h>
using namespace std;
string b[20];
int a[20];
int ans,n;
bool bb[20];
map<int,char> hash[200000];
void dfs(int k,string ch,int x)
{
//	cout<<x<<' '<<ch[ch.length()-1]<<endl;
	if(hash[x][ch[ch.length()-1]])return;
	hash[x][ch[ch.length()-1]]=1;
	ans=max(ans,k);
	for(int i=1;i<=n;i++)
		if(!bb[i]&&ch[ch.length()-1]==b[i][0]){
			bb[i]=true;
			dfs(k+a[i],ch+b[i],x|(1<<(i-1)));
			bb[i]=false;
		}
}
int main()
{
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>b[i];
		a[i]=b[i].length();
	} 
	for(int i=1;i<=n;i++){
		bb[i]=true;
		dfs(a[i],b[i],1<<(i-1));
		bb[i]=false;
	}
	printf("%d",ans);
	return 0;
}

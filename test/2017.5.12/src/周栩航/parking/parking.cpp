#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int T[5001],son[5001][5001],n,p,k,fa[5001],size[5001],root,vis[5001],ne[5001],tot,x,ans,t_vis[5001];
bool rt[5001],flag,have[5001];
int find(int x)
{
	if(x==p)	return 233333;
	t_vis[x]=1;
	if(!vis[x])	{if(!flag){flag=1;vis[x]=1;return 0;}else return 233333;}
	if(flag)	return 233333;
	int ret=1e9;
	For(i,1,T[x])	
		if(!t_vis[son[x][i]])	ret=min(ret,find(son[x][i])+1);
	if(fa[x]!=0&&!t_vis[fa[x]])	ret=min(ret,find(fa[x])+1);
	return ret;
}
void up(int x)
{
	vis[x]=1;
	if(have[x])	ne[++tot]=x;
	if(x==root)	return;
	up(fa[x]);
}
int main()
{
	freopen("parking.in","r",stdin);
	freopen("parking.out","w",stdout);
	scanf("%d%d%d",&n,&p,&k);
	For(i,1,n)
	{
		scanf("%d",&T[i]);
		For(j,1,T[i])
			scanf("%d",&son[i][j]),fa[son[i][j]]=i,rt[son[i][j]]=1;
	}
	For(i,1,n)	if(!rt[i])	{root=i;break;}
	For(i,1,k)	scanf("%d",&x),have[x]=1;
	up(p);
	For(i,1,tot)	
	{
		For(j,1,n)	t_vis[j]=0;
		flag=0;
		int tmp=find(ne[i]);
		ans+=tmp;
	}
	printf("%d",ans);
}
/*
7 5 1
1 3
2 1 7
1 4
1 5
1 6
0
0
4
*/

#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
bool vis[101];
ll n,m,d[101][101],x,y,z,dist[101],ans=0;
void dij()
{
	For(i,1,n)	dist[i]=1e9,vis[i]=0;
	dist[1]=0;
	For(ii,1,n)
	{
		ll t=1,ma=1e9;
		For(i,1,n)	
			if(dist[i]<ma&&!vis[i])	t=i,ma=dist[i];
		vis[t]=1;
		For(i,1,n)
			dist[i]=min(dist[i],dist[t]+d[t][i]);
	}
	ans=max(ans,dist[n]);
}
int main()
{
	scanf("%lld%lld",&n,&m);
	For(i,1,n)	For(j,1,n)	d[i][j]=1e9;
	For(i,1,m)
	{
		scanf("%lld%lld%lld",&x,&y,&z);
		d[x][y]=d[y][x]=z;
	}
	For(i,1,n)
		For(j,1,n)
		{
			if(d[i][j]==1e9)	continue;
			ll tmp=d[i][j];
			d[j][i]=d[i][j]=1e9;
			dij();
			d[i][j]=d[j][i]=tmp;
		}
	printf("%lld",ans);
}
     

#include<bits/stdc++.h>
using namespace std;
int n,p,k,i,j,x,y,ans,num,dis,node;
int fa[5005],need[5005];
vector<int>son[5005];
bool can[5005],vis[5005];
int dfs(int now,int rank)
{
    if(!vis[now] && can[now]){
        if(rank<dis){dis=rank;node=now;}
        return 0;
    }
    int s=son[now].size();
    for(int i=0;i<s;i++)
        if(son[now][i]!=p) dfs(son[now][i],rank+1);
    return 0;
}
int main()
{
    freopen("parking.in","r",stdin);
    freopen("parking.out","w",stdout);
    scanf("%d %d %d",&n,&p,&k);
    for(i=1;i<=n;i++){
        scanf("%d",&x);
        for(j=1;j<=x;j++){
            scanf("%d",&y);
            son[i].push_back(y);
            fa[y]=i;
        }
    }
    for(i=1;i<=k;i++){
        scanf("%d",&x);
        vis[x]=1;
    }
    i=p;
    memset(can,1,sizeof(can));
    while(i)
    {
        can[i]=0;
        if(vis[i]) need[++num]=i;
        i=fa[i];
    }
    for(i=1;i<=num;i++)
    {
        dis=100000000;
        dfs(need[i],0);
        vis[node]=1;
        ans+=dis;
        vis[need[i]]=0;
    }
    printf("%d\n",ans);
    return 0;
}

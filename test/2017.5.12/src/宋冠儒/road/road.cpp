#include<bits/stdc++.h>
using namespace std; 
const int N=1005,M=1000005;
int i,j,k,ans,q[M],f[N],dis[N],pe[N],pv[N];
int n,m,t,en[M],Next[M],fa[N],w[M];
void add(int a,int b,int c)
{
	en[++t]=b,Next[t]=fa[a];
	fa[a]=t,w[t]=c;
}
void spfa_pre(int s)
{
	memset(dis,8,sizeof dis);
	int i,u,v,h=1,t=1;
	for(q[t]=s,dis[s]=0;h<=t;)
		for(f[u=q[h++]]=0,i=fa[u];i;i=Next[i])
			if(dis[u]+w[i]<dis[v=en[i]])
			{
				dis[v]=dis[u]+w[i];
				pe[v]=i,pv[v]=u;
				if(!f[v])q[++t]=v,f[v]=1;
			}
}
void spfa(int s)
{
	memset(dis,8,sizeof dis);
	int i,u,v,h=1,t=1;
	for(q[t]=s,dis[s]=0;h<=t;)
		for(f[u=q[h++]]=0,i=fa[u];i;i=Next[i])
			if(dis[u]+w[i]<dis[v=en[i]])
			{
				dis[v]=dis[u]+w[i];
				if(!f[v])q[++t]=v,f[v]=1;
			}
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	for(cin>>n,cin>>m;m--;)
	{
		cin>>i>>j>>k;
		add(i,j,k),add(j,i,k);
	}
	for(spfa_pre(1),i=n;pe[i];i=pv[i])
	{
		t=w[pe[i]],w[pe[i]]=w[(pe[i]-1^1)+1]=M;
		if(spfa(1),dis[n]>ans)ans=dis[n];
		w[pe[i]]=w[(pe[i]-1^1)+1]=t;
	}
	printf("%d",ans);
}

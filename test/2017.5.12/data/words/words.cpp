#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
int n,len[16],ans,a[16],b[16],F[1<<16][16];
char st[200];

int main()
{
	freopen("words.in","r",stdin);
	freopen("words.out","w",stdout);
	scanf("%d",&n);
	memset(F,-2,sizeof(F));
	for (int i=0;i<n;i++) {
		scanf("%s",st);
		len[i]=strlen(st);
		a[i]=st[0];
		b[i]=st[len[i]-1];
		F[1<<i][i]=len[i];
	}
	for (int i=0;i<1<<n;i++)
	for (int j=0;j<n;j++)
	if (F[i][j]>=0) {
		ans=max(ans,F[i][j]);
		for (int k=0;k<n;k++)
		if (!(i&(1<<k)) && a[k]==b[j])
			F[i|(1<<k)][k]=F[i][j]+len[k];
	}
	printf("%d\n",ans);
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
#define maxn (10005)
int n,m,s,t,ans,list[maxn],e[maxn],next[maxn],d[maxn],father[maxn];
bool b[maxn],visit[maxn];

inline void AddEdge(int u,int v)
{	e[++m]=v;next[m]=list[u];list[u]=m;		}

inline void BuildTree(int u,int dep)
{
	d[u]=dep;
	for (int v=list[u];v;v=next[v])
	if (!d[e[v]]) {
		father[e[v]]=u;
		BuildTree(e[v],dep+1);
	}
}

inline void Dfs(int u)
{
	visit[u]=1;
	if (u==s) return;
	for (int v=list[u];v;v=next[v])
	if (d[e[v]]>d[u]) Dfs(e[v]);
}

int main()
{
	freopen("parking.in","r",stdin);
	freopen("parking.out","w",stdout);
	scanf("%d%d%d",&n,&s,&t);
	for (int i=1,p;i<=n;i++) {
		scanf("%d",&p);
		for (int j=1,k;j<=p;j++) {
			scanf("%d",&k);
			AddEdge(i,k);
			AddEdge(k,i);
		}
	}
	for (int i=1,k;i<=t;i++) {
		scanf("%d",&k);
		b[k]=1;
	}
	BuildTree(1,1);
	for (int v=s;v;v=father[v]) {
		if (b[v]) {
			memset(visit,0,sizeof(visit));
			Dfs(v);
			int k=0;
			for (int i=1;i<=n;i++)
			if (visit[i] && !b[i] && (!k || d[i]<d[k]))
				k=i;
			if (!k) {
				printf("not solvable\n");
				return 0;
			}
			ans+=d[k]-d[v];
			b[k]=1;
		}
		b[v]=1;
	}
	printf("%d\n",ans);
	return 0;
}

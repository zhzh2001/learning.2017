#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
#define sqr(x) ((x)*(x))
int n,m,x[2005],y[2005],d[2005];
bool visit[2005];

int main()
{
	freopen("meadow.in","r",stdin);
	freopen("meadow.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		scanf("%d%d",&x[i],&y[i]);
	memset(d,63,sizeof(d));d[1]=0;
	for (int i=1;i<=n;i++) {
		int k=0;
		for (int j=1;j<=n;j++)
		if (!visit[j] && d[j]<d[k])
			k=j;
		visit[k]=1;
		for (int j=1;j<=n;j++)
		if (!visit[j]) d[j]<?=sqr(x[k]-x[j])+sqr(y[k]-y[j]);
	}
	sort(d+1,d+n+1);
	printf("%.2lf\n",sqrt(d[n-m+1]));
	return 0;
}

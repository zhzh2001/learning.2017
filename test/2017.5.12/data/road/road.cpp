#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
int n,m,ans,dist[1002][1002],d[1002],pre[1002];
bool visit[1002];

inline void Dijkstra(bool flag)
{
	memset(d,7,sizeof(d));d[1]=0;
	memset(visit,0,sizeof(visit));
	for (int i=1;i<=n;i++) {
		int k=0;
		for (int j=1;j<=n;j++)
		if (!visit[j] && d[j]<d[k]) k=j;
		if (k==n) return;
		visit[k]=1;
		for (int j=1;j<=n;j++)
		if (d[k]+dist[k][j]<d[j]) {
			d[j]=d[k]+dist[k][j];
			if (flag) pre[j]=k;
		}
	}
}

int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dist,7,sizeof(dist));
	for (int i=0,u,v,w;i<m;i++) {
		scanf("%d%d%d",&u,&v,&w);
		dist[u][v]=dist[v][u]<?=w;
	}
	Dijkstra(1);
	for (int v=n;v!=1;v=pre[v]) {
		int u=pre[v],w=dist[pre[v]][v];
		dist[u][v]=dist[v][u]=1<<28;
		Dijkstra(0);
		ans>?=d[n];
		dist[u][v]=dist[v][u]=w;
	}
	printf("%d\n",ans);
	return 0;
}

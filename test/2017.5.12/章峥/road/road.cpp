#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("road.in");
ofstream fout("road.out");
const int N=1005,INF=0x3f3f3f3f;
int n,mat[N][N],d[N],pred[N];
bool vis[N];
void dijkstra(bool flag)
{
	for(int i=0;i<=n;i++)
		d[i]=i==1?0:INF;
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[j]+mat[j][k]<d[k])
			{
				d[k]=d[j]+mat[j][k];
				if(flag)
					pred[k]=j;
			}
	}
}
int main()
{
	int m;
	fin>>n>>m;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	dijkstra(true);
	int ans=d[n];
	for(int u=n;u!=1;u=pred[u])
	{
		int w=mat[u][pred[u]];
		mat[u][pred[u]]=mat[pred[u]][u]=INF;
		dijkstra(false);
		ans=max(ans,d[n]);
		mat[u][pred[u]]=mat[pred[u]][u]=w;
	}
	fout<<ans<<endl;
	return 0;
}
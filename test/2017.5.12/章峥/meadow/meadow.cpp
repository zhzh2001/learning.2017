#include<fstream>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("meadow.in");
ofstream fout("meadow.out");
const int N=2005;
const double eps=1e-6;
int x[N],y[N];
double d[N][N];
bool vis[N];
inline int sqr(int x)
{
	return x*x;
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			d[i][j]=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
	double l=0,r=1e9;
	while(r-l>eps)
	{
		double mid=(l+r)/2;
		int now=0;
		memset(vis,false,sizeof(vis));
		for(int i=1;i<=n;i++)
			if(!vis[i])
			{
				now++;
				for(int j=1;j<=n;j++)
					vis[j]|=d[i][j]<=mid;
			}
		if(now<=m)
			r=mid;
		else
			l=mid;
	}
	fout.precision(2);
	fout<<fixed<<l<<endl;
	return 0;
}
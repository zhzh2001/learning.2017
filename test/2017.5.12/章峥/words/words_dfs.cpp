#include<fstream>
#include<algorithm>
#include<ctime>
#include<cstdlib>
using namespace std;
ifstream fin("words.in");
ofstream fout("words.out");
const int N=20;
int n,ans;
string s[N],same[5],t[N];
bool vis[N];
void dfs(int len,char pred)
{
	if(clock()>800)
	{
		fout<<ans<<endl;
		exit(0);
	}
	for(int i=1;i<=n;i++)
		if(!vis[i]&&s[i][0]==pred)
		{
			ans=max(ans,(int)(len+s[i].length()));
			vis[i]=true;
			dfs(len+s[i].length(),s[i][s[i].length()-1]);
			vis[i]=false;
		}
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>t[i];
		if(t[i][0]==t[i][t[i].length()-1])
		{
			switch(t[i][0])
			{
				case 'A':same[0]+=t[i];break;
				case 'E':same[1]+=t[i];break;
				case 'I':same[2]+=t[i];break;
				case 'O':same[3]+=t[i];break;
				case 'U':same[4]+=t[i];break;
			}
			t[i]="";
		}
	}
	int rn=0;
	for(int i=1;i<=n;i++)
		if(t[i]!="")
			s[++rn]=t[i];
	for(int i=0;i<5;i++)
		if(same[i]!="")
			s[++rn]=same[i];
	n=rn;
	ans=0;
	dfs(0,'A');
	dfs(0,'E');
	dfs(0,'I');
	dfs(0,'O');
	dfs(0,'U');
	fout<<ans<<endl;
	return 0;
}
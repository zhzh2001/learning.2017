#include<fstream>
#include<vector>
#include<queue>
#include<cstring>
using namespace std;
ifstream fin("parking.in");
ofstream fout("parking.out");
const int N=21;
struct node
{
	int s[N],step;
	node():step(0)
	{
		memset(s,0,sizeof(s));
	}
};
vector<int> mat[N];
int f[N];
bool bl[N],blK[N];
int main()
{
	int n,p,k;
	fin>>n>>p>>k;
	if(n>20)
		return 0;
	for(int i=1;i<=n;i++)
	{
		int t;
		fin>>t;
		while(t--)
		{
			int u;
			fin>>u;
			mat[i].push_back(u);
			f[u]=i;
		}
	}
	for(int u=p;u;u=f[u])
		bl[u]=true;
	node start;
	for(int i=1;i<=k;i++)
		fin>>start.s[i];
	start.step=0;
	queue<node> Q;
	Q.push(start);
	while(!Q.empty())
	{
		node K=Q.front();Q.pop();
		bool flag=true;
		memset(blK,false,sizeof(blK));
		for(int i=1;i<=k;i++)
		{
			blK[K.s[i]]=true;
			if(bl[K.s[i]])
				flag=false;
		}
		blK[p]=true;
		if(flag)
		{
			fout<<K.step<<endl;
			break;
		}
		for(int i=1;i<=k;i++)
			for(int j=0;j<mat[K.s[i]].size();j++)
				if(!blK[mat[K.s[i]][j]])
				{
					node now=K;
					now.s[i]=mat[K.s[i]][j];
					now.step++;
					Q.push(now);
				}
	}
	return 0;
}
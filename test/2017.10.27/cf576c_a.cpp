#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
const int B = 1000;
struct point
{
	int x, y, id;
	bool operator<(const point &rhs) const
	{
		return y < rhs.y;
	}
};
vector<point> p[B + 1];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		int x, y;
		cin >> x >> y;
		p[x / B].push_back({x, y, i});
	}
	for (int i = 0; i <= B; i++)
	{
		sort(p[i].begin(), p[i].end());
		auto f = [](const point &p) { cout << p.id << ' '; };
		if (i & 1)
			for_each(p[i].begin(), p[i].end(), f);
		else
			for_each(p[i].rbegin(), p[i].rend(), f);
	}
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, INF = 1e9;
pair<int, int> p[N];
int prefix[N][2], suffix[N][2];
inline long long sqr(int x)
{
	return 1ll * x * x;
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> p[i].first >> p[i].second;
	sort(p + 1, p + n + 1);
	prefix[0][0] = INF;
	prefix[0][1] = -INF;
	for (int i = 1; i <= n; i++)
	{
		prefix[i][0] = min(prefix[i - 1][0], p[i].second);
		prefix[i][1] = max(prefix[i - 1][1], p[i].second);
	}
	suffix[n + 1][0] = INF;
	suffix[n + 1][1] = -INF;
	for (int i = n; i; i--)
	{
		suffix[i][0] = min(suffix[i + 1][0], p[i].second);
		suffix[i][1] = max(suffix[i + 1][1], p[i].second);
	}
	long long l = 0, r = 4e16;
	while (l < r)
	{
		long long mid = (l + r) / 2;
		bool flag = false;
		for (int i = 1, j = 1; i <= n && p[i].first <= 0; i++)
		{
			for (; j <= n && sqr(p[j].first - p[i].first) <= mid && abs(p[j].first) <= abs(p[i].first); j++)
				;
			int yl = min(prefix[i - 1][0], suffix[j][0]), yr = max(prefix[i - 1][1], suffix[j][1]);
			if (sqr(yr - yl) <= mid && sqr(p[i].first) + sqr(yl) <= mid && sqr(p[i].first) + sqr(yr) <= mid)
			{
				flag = true;
				break;
			}
		}
		for (int i = n, j = n; i && p[i].first >= 0; i--)
		{
			for (; j && sqr(p[i].first - p[j].first) <= mid && abs(p[j].first) <= abs(p[i].first); j--)
				;
			int yl = min(prefix[j][0], suffix[i + 1][0]), yr = max(prefix[j][1], suffix[i + 1][1]);
			if (sqr(yr - yl) <= mid && sqr(p[i].first) + sqr(yl) <= mid && sqr(p[i].first) + sqr(yr) <= mid)
			{
				flag = true;
				break;
			}
		}
		if (flag)
			r = mid;
		else
			l = mid + 1;
	}
	cout << min(r, min(sqr(p[n].first - p[1].first), sqr(prefix[n][1] - prefix[n][0]))) << endl;
	return 0;
}
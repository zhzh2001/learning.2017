#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 15005, M = 30005, K = 1005, p = 1e7 + 19, q = 1e9 + 7;
int n, k, f[16][K];
bool mark[M];
struct exhibit
{
	int v, w, l, r;
} a[N];
void solve(int dep, int l, int r, vector<int> lst)
{
	if (dep)
		copy(f[dep - 1], f[dep - 1] + k + 1, f[dep]);
	for (int i : lst)
		if (a[i].l <= l && a[i].r >= r)
			for (int j = k; j >= a[i].w; j--)
				f[dep][j] = max(f[dep][j], f[dep][j - a[i].w] + a[i].v);
	if (l == r)
	{
		if (mark[l])
		{
			long long ans = 0;
			for (int i = k; i; i--)
				ans = (ans * p + f[dep][i]) % q;
			cout << ans << endl;
		}
	}
	else
	{
		int mid = (l + r) / 2;
		vector<int> llst, rlst;
		for (int i : lst)
			if (a[i].l > l || a[i].r < r)
			{
				if (a[i].l <= mid)
					llst.push_back(i);
				if (a[i].r > mid)
					rlst.push_back(i);
			}
		solve(dep + 1, l, mid, llst);
		solve(dep + 1, mid + 1, r, rlst);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i].v >> a[i].w;
		a[i].l = 0;
	}
	int m;
	cin >> m;
	for (int i = 1; i <= m; i++)
	{
		int opt, x, y;
		cin >> opt;
		switch (opt)
		{
		case 1:
			cin >> x >> y;
			a[++n].v = x;
			a[n].w = y;
			a[n].l = i;
			break;
		case 2:
			cin >> x;
			a[x].r = i;
			break;
		case 3:
			mark[i] = true;
			break;
		}
	}
	vector<int> l;
	for (int i = 1; i <= n; i++)
	{
		if (!a[i].r)
			a[i].r = m;
		l.push_back(i);
	}
	solve(0, 0, m, l);
	return 0;
}
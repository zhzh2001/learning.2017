#include <iostream>
#include <map>
using namespace std;
int qpow(long long a, int b, int mod)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % mod;
		a = a * a % mod;
	} while (b /= 2);
	return ans;
}
int phi(int x)
{
	int ans = x;
	for (int i = 2; i * i <= x; i++)
		if (x % i == 0)
		{
			ans = ans / i * (i - 1);
			for (; x % i == 0; x /= i)
				;
		}
	if (x > 1)
		ans = ans / x * (x - 1);
	return ans;
}
map<int, int> m;
int f(int p)
{
	if (m.find(p) != m.end())
		return m[p];
	int tmp = phi(p);
	return m[p] = qpow(2, f(tmp) + tmp, p);
}
int main()
{
	ios::sync_with_stdio(false);
	m[1] = 0;
	int t;
	cin >> t;
	while (t--)
	{
		int p;
		cin >> p;
		cout << f(p) << endl;
	}
	return 0;
}
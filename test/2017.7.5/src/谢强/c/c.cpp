#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <deque>
using namespace std;
deque<int> l1,l2;
int maxd=0,maxid=0;
int n,m,a[100010];
int lis[100010];
int lowbit(int x){
	return x&(-x);
}
int ans=0;
int ask(int x){
	int sum=0;
	while(x){
		sum+=lis[x];
		x-=lowbit(x);
	}
	return sum;
}
void edt(int x,int d){
	while(x<=n){
		lis[x]+=d;
		x+=lowbit(x);
	}
}
void deal(int x,int v){
	edt(x,v);
	maxd=0,maxid=0;
	a[x]+=v;
	for(int i=1;i<=n;i++){
		if(a[i]>maxd) maxd=a[i],maxid=i;
	}
	l1.clear();
	l2.clear();
	l1.push_back(maxid);
	l2.push_back(maxid);
	ans=0;
	deque<int>::iterator p,q,t;
	for(int i=maxid+1;i<=n;i++){
		while(l1.size()>1&&a[i]>=a[l1.back()]){
			l1.pop_back();
		}
		l1.push_back(i);
	}
	for(int i=maxid-1;i>=1;i--){
		while(l2.size()>1&&a[i]>=a[l2.back()]){
			l2.pop_back();
		}
		l2.push_back(i);
	}
	p=l1.begin();
	p++;
	for(;p!=l1.end();p++){
		q=p;
		t=--q;
		ans+=((*p-*t)-1)*a[*p];
		ans-=ask(*p-1)-ask(*t);
	}
	p=l2.begin();
	p++;
	for(;p!=l2.end();p++){
		q=p;
		t=--q;
		ans+=((*t-*p)-1)*a[*p];
		ans-=ask(*t-1)-ask(*p);
	}
	//cout<<ans<<endl;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	int x,v;
	char flag;
	scanf("%d %d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		edt(i,a[i]);
	}
	for(int i=1;i<=n;i++){
		if(a[i]>maxd) maxd=a[i],maxid=i;
	}
	l1.push_back(maxid);
	l2.push_back(maxid);
	ans=0;
	deque<int>::iterator p,q,t;
	for(int i=maxid+1;i<=n;i++){
		while(l1.size()>1&&a[i]>=a[l1.back()]){
			l1.pop_back();
		}
		l1.push_back(i);
	}
	for(int i=maxid-1;i>=1;i--){
		while(l2.size()>1&&a[i]>=a[l2.back()]){
			l2.pop_back();
		}
		l2.push_back(i);
	}
	p=l1.begin();
	p++;
	for(;p!=l1.end();p++){
		q=p;
		t=--q;
		ans+=((*p-*t)-1)*a[*p];
		ans-=ask(*p-1)-ask(*t);
	}
	p=l2.begin();
	p++;
	for(;p!=l2.end();p++){
		q=p;
		t=--q;
		ans+=((*t-*p)-1)*a[*p];
		ans-=ask(*t-1)-ask(*p);
	}
	//cout<<ans<<endl;
	for(int i=1;i<=m;i++){
		scanf("%s",&flag);
		if(flag=='P') printf("%d\n",ans);
		else{
			scanf("%d %d",&x,&v);
			deal(x,v);
		}
		getchar();
	}
	return 0;
}
#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,m,mo,a[1010][1010],mx,h[1010],l[1010];
ll ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();m=read();mo=read();
	For(i,1,n)For(j,1,m)
	{
		a[i][j]=read();
		a[i][j]%=mo;
		if(a[i][j]==0)a[i][j]=mo;
	}
	For(i,1,n)mx=max(a[i][1],mx);
	For(i,1,n)h[i]=(mx-a[i][1]),ans+=h[i];
	Rep(i,1,m)(a[1][i]+=(mx-a[1][1]))%=mo;
	For(i,1,m)if(a[1][i]==0)a[1][i]=mo;
	For(i,1,m)l[i]=(mo-a[1][i]),ans+=l[i];
	cout<<ans<<endl;
	For(i,1,n)cout<<h[i]<<" ";
	cout<<endl;
	For(i,1,m)cout<<l[i]<<" ";
	cout<<endl;
	return 0;
}


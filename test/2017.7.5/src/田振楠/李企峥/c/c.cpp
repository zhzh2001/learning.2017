#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,q,ans,a[100100],b[100100],c[100100];
bool flag;
int x,y,z;
char ch;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();q=read();
	For(i,1,n)a[i]=read();
	flag=true;
	while(q--)
	{
		ch=getchar();
		while(ch!='P'&&ch!='U')ch=getchar();
		if(ch=='U')
		{
			x=read();
			y=read();
			a[x]+=y;
			flag=true;
		}
		else
		{
			if(flag==false)
			{
				cout<<ans<<endl;
				continue;
			}
			flag=false;
			ans=0;
			x=1;b[1]=a[1];c[1]=1;
			For(i,2,n)
			{
				if(a[i]>=b[1])
				{
					Rep(j,1,x-1)ans+=(c[j+1]-c[j])*(b[1]-b[j+1]);
					x=1;
					b[1]=a[i];
					c[1]=i;
				}
				else
				{
					int z=x;
					while(a[i]>b[z])z--;
					Rep(j,z,x-1)
						ans+=(c[j+1]-c[j])*(a[i]-b[j+1]);
					x=z+1;
					b[x]=a[i];c[x]=i;
				}
			}
			cout<<ans<<endl;
		}
	}
	return 0;
}


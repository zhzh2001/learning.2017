#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int n,Q,a[100005],f[100005],u,v;
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
inline int solve()
{
	int mxp,mxo,ans=0;
	for (int i=1;i<=n;i++) 
	f[i]=f[i-1]+a[i];
	mxp=1;
	for (int i=2;i<=n;i++)
	if (a[i]>=a[mxp]) ans+=(i-mxp-1)*a[mxp]-f[i-1]+f[mxp],mxp=i;
	mxo=n;
	for (int i=n-1;i>=mxp;i--)
	if (a[i]>=a[mxo]) ans+=(mxo-i-1)*a[mxo]-f[mxo-1]+f[i],mxo=i;
	return ans;
}
int main()
{
	char opt;
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();Q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	while (Q--)
	{
		opt=getchar();
		while (opt!='P'&&opt!='U') opt=getchar();
		if (opt=='P') printf("%d\n",solve());
		else u=read(),v=read(),a[u]+=v;
	}
}

#include <cstdio>
#include <algorithm>
#include <cstring>
#define N 100005
using namespace std;

inline void write(int k)
{
	int s[13],len=0;
	if (k==0)
	{
		putchar('0');
		putchar('\n');
	}
	while (k) s[++len]=k%10,k/=10;
	while (len) putchar(s[len--]+48);
	putchar('\n');
}
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int n,m,Q,q,p,ans,a[N];
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();Q=read();
	for (int i=0;i<n;i++) a[i]=read();
	while (Q--)
	{
		q=read();p=read();
		ans=0;
		while (q<n) ans+=a[q],q+=p;
		write(ans);
	}
}

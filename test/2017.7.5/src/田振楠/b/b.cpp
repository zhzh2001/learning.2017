#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
int n,m,k,a[1005][1005],fx[1005],fy[1005];
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}

bool check()
{
	for (int i=1;i<=n;i++)
	for (int j=1;j<=m;j++)
	if ((fx[i]+fy[j])%k!=a[i][j]%k) return 0;
	return 1;
}
void dfsy(int y)
{
	if (y==m+1) 
	{
		if (check())
		{
			int ans=0;
			for (int i=1;i<=n;i++)
			ans+=fx[i];
			for (int i=1;i<=m;i++)
			ans+=fy[i];
			printf("%d\n",ans);
			for (int i=1;i<=n;i++) printf("%d ",fx[i]);
			printf("\n");
			for (int i=1;i<=m;i++) printf("%d ",fy[i]);
			exit(0);
		}
		return;
	}
	fy[y]=a[1][y]-fx[1];
	if (fy[y]<0) fy[y]+=k;
	dfsy(y+1);
}
void dfsx(int x)
{
	if (x==n+1){
		dfsy(1);return;
	}
	for (int i=0;i<=k;i++) fx[x]=i,dfsx(x+1);
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();m=read();k=read();
	for (int i=1;i<=n;i++)
	for (int j=1;j<=m;j++)
	{
		a[i][j]=read();
		if (a[i][j]) a[i][j]=k-(a[i][j]%k);
	}
	dfsx(1);
}

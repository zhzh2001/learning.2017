#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
int a[N];
int n,m,x,y,ans;
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;i++)scanf("%d",&a[i]);
	while(m--){
		scanf("%d%d",&x,&y);
		ans=0;
		for(int i=x;i<=n;i+=y)ans+=a[i];
		printf("%d\n",ans);
	}

}

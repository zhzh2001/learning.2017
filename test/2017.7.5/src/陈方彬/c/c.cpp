#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
struct tree{Ll l,r,v;}T[N*4];
Ll a[N];
Ll ans,n,m,x,y,z,ma,L,R;
char c;
void up(Ll id){
	Ll x=id*2,y=id*2+1;
	if(a[T[x].v]>a[T[y].v])T[id].v=T[x].v;else T[id].v=T[y].v;
}
void make(Ll id,Ll l,Ll r){
	T[id].l=l;T[id].r=r;
	if(l==r){T[id].v=l;return;}
	Ll mid=l+r>>1;
	make(id*2  ,l,mid  );
	make(id*2+1,mid+1,r);
	up(id);
}
void change(Ll id,Ll x){
	if(x<=T[id].l&&T[id].r<=y)return;
	if(T[id*2].r>=x)change(id*2,x);else change(id*2+1,x);
	up(id);
}
Ll out(Ll id,Ll x,Ll y){
	if(x<=T[id].l&&T[id].r<=y)return T[id].v;
	Ll X=0,Y=0;
	if(T[id*2  ].r>=x)X=out(id*2  ,x,y);
	if(T[id*2+1].l<=y)Y=out(id*2+1,x,y);
	if(!X)return Y; if(!Y)return X;
	if(a[X]>a[Y])return X;return Y;
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(Ll i=1;i<=n;i++)scanf("%lld",&a[i]);
	make(1,1,n);
	for(Ll i=2;i<n;i++){
		L=out(1,1,i-1);
		R=out(1,i+1,n);
		ans+=max(min(a[L],a[R])-a[i],0ll);
	}
	while(m--){
		cin>>c;
		if(c=='P')printf("%lld\n",ans);else{
			scanf("%lld%lld",&x,&y);
			if(x==1)L=0;else L=out(1,1,x-1);
			if(x==n)R=0;else R=out(1,x+1,n);
			if(L==0)ans+=max(0ll,min(a[R]-a[x],a[x]+y-a[x])*(R-x-1));else
			if(R==0)ans+=max(0ll,min(a[L]-a[x],a[x]+y-a[x])*(x-L-1));else{
				Ll v=max(min(a[L],a[R])-a[x],0ll);
				if(v>y)ans-=y;else{
					ans-=v;
					ans+=max(0ll,min(a[R]-a[L],a[x]+y-a[L])*(R-x-1));
					ans+=max(0ll,min(a[L]-a[R],a[x]+y-a[R])*(x-L-1));
				}
			}
			a[x]+=y;
			change(1,x);
		}
	}
}

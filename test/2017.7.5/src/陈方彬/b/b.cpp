#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e3+5;
Ll a[N][N],X[N],Y[N],XX[N],YY[N];
Ll n,m,ans,K;
void hang(){
	for(Ll i=1;i<=n;i++){
		Ll mi=1e9;
		for(Ll j=1;j<=m;j++)mi=min(a[i][j],mi);
		X[i]=mi;
		for(Ll j=1;j<=m;j++)a[i][j]-=mi;
	}
}
void lie(){
	for(Ll j=1;j<=m;j++){
		Ll mi=1e9;
		for(Ll i=1;i<=n;i++)mi=min(a[i][j],mi);
		Y[j]=mi;
		for(Ll i=1;i<=n;i++)a[i][j]-=mi;
	}
}
void find(){
	Ll x,y,xx,yy;
	for(Ll i=2;i<n;i++)
		for(Ll j=2;j<m;j++)
			if(!a[i][j]){
					if(i!=1) x=a[i-1][j];else  x=-1;
					if(i!=n)xx=a[i+1][j];else xx=-1;
					if(j!=1) y=a[i][j-1];else  y=-1;
					if(j!=m)yy=a[i][j+1];else yy=-1;
					if(!x||!y||!xx||!yy)continue;
					if(x!=-1&&xx!=-1&&x!=xx)continue;
					if(y!=-1&&yy!=-1&&y!=yy)continue;
					XX[i]=max(x,xx);
					YY[j]=max(y,yy);
				}
	for(Ll i=1;i<=n;i++)
		for(Ll j=1;j<=m;j++)
			if(i==1||i==n||j==1||j==m)
				if(!a[i][j]){
					if(i!=1) x=a[i-1][j];else  x=-1;
					if(i!=n)xx=a[i+1][j];else xx=-1;
					if(j!=1) y=a[i][j-1];else  y=-1;
					if(j!=m)yy=a[i][j+1];else yy=-1;
					if(!x||!y||!xx||!yy)continue;
					if(x!=-1&&xx!=-1&&x!=xx)continue;
					if(y!=-1&&yy!=-1&&y!=yy)continue;
					bool ans=0;
					if( x!=-1&&!XX[i-1])ans=1;
					if(xx!=-1&&!XX[i+1])ans=1;
					if( y!=-1&&!YY[j-1])ans=1;
					if(yy!=-1&&!YY[j+1])ans=1;
					if(ans){
						XX[i]=max(x,xx);
						YY[j]=max(y,yy);
					}
				}
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&K);
	for(Ll i=1;i<=n;i++)
		for(Ll j=1;j<=m;j++)
			scanf("%lld",&a[i][j]),a[i][j]=(K-a[i][j])%K;
	if(n<m)hang(),lie();else lie(),hang();
	find();
	for(Ll i=1;i<=n;i++)ans+=X[i]+XX[i];
	for(Ll j=1;j<=m;j++)ans+=Y[j]+YY[j];
	printf("%lld\n",ans);
	for(Ll i=1;i<=n;i++)printf("%lld ",X[i]+XX[i]);printf("\n");
	for(Ll j=1;j<=m;j++)printf("%lld ",Y[j]+YY[j]);printf("\n");	
}

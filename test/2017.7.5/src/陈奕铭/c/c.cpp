#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
#define N 100005
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,q;
int a[N];
int l[N],r[N];
int s[N],top;

int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read(); q=read();
	for(int i=1;i<=n;i++) a[i]=read();
	while(q--){
		char c;
		scanf("%c",&c);
		if(c=='P'){
			top=0;
			for(int i=1;i<=n;i++){
				while(top>0&&a[s[top]]<a[i]){
					r[s[top]]=i;
					top--;
				}
				s[++top]=i;
			}
			top=0;
			for(int i=n;i;i--){
				while(top>0&&a[s[top]]<a[i]){
					l[s[top]]=i;
					top--;
				}
				s[++top]=i;
			}
/*
			puts("");
			for(int i=1;i<=n;i++) printf("%d ",l[i]);
			puts("");
			for(int i=1;i<=n;i++) printf("%d ",r[i]);
			puts("");
*/
			ll ans=0;
			for(int i=1;i<=n;i++)
				if(a[l[i]]>a[i]&&a[r[i]]>a[i])
					ans+=(min(a[l[i]],a[r[i]])-a[i])*(r[i]-l[i]-1);
			printf("%lld\n",ans);
		}
		else{
			int x=read(),v=read();
			a[x]+=v;
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define inf 1e18
#define ll long long
#define N 1005
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,m,k;
int a[N][N];
int b[N][N];
ll Max=inf;
int Maxm;
int ans1[N],ans2[N];
int flag[N],flagm;

int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(); m=read(); k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			a[i][j]=read();
			if(a[i][j]==k) flag[i]=1;
		}
	for(int i=1;i<=n;i++){
		if(flag[i]==1){
			for(int j=1;j<=m;j++)
				a[i][j]=(a[i][j]+1)%k;
			flagm++;
		}
			
		for(int j=1;j<=m;j++){
			if(a[i][j]==0) b[i][j]=0;
			else if(a[i][j]==k) b[i][j]=k;
			else b[i][j]=k-a[i][j];
		}
	}
/*	
	puts("");
	for(int i=1;i<=n;i++,puts(""))
		for(int j=1;j<=m;j++)
			printf("%d ",b[i][j]);
	puts("");
*/
	for(int i=1;i<=m;i++){
		ll num=0;
		for(int j=1;j<=n;j++) num+=b[j][i];
		for(int j=1;j<=m;j++){
			int p=(a[1][j]+b[1][i])%k;
			if(p!=0) num+=k-p;
		}
//		printf("# %d : %lld\n",i,num);
		if(num<Max){
			Max=num;
			Maxm=i;
		}
	}
/*
	puts("");
	printf("# %d\n",Maxm);
	puts("");
*/
	for(int i=1;i<=n;i++) ans1[i]=b[i][Maxm];
	for(int i=1;i<=m;i++){
		int p=(a[1][i]+ans1[1])%k;
		if(p!=0) ans2[i]=k-p;
	}
	printf("%lld\n",Max+flagm);
	for(int i=1;i<=n;i++) printf("%d ",ans1[i]+flag[i]);
	puts("");
	for(int i=1;i<=m;i++) printf("%d ",ans2[i]);
	puts("");
	return 0;
}

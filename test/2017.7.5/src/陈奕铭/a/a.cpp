#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;
#define N 100005
inline int read(int &x){
	x=0;char ch=getchar();
	while(ch<'0'||ch>'9'){ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
}
void write(int x){
	if(x>9) write(x/10);
	putchar('0'+x%10);
}
void writeln(int x){
	write(x);
	putchar('\n');
}

int n,a[N],p,q,Q;
int num[105][105];

int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	read(n); read(Q);
	for(int i=0;i<n;++i){
		read(a[i]);
		for(int j=1;j<=100;++j)
			num[j][i%j]+=a[i];
	}
	while(Q--){
		int ans=0;
		read(q); read(p);
		if(p<=100){
			writeln(num[p][q]);
			continue;
		}
		for(int i=q;i<n;i+=p) ans+=a[i];
		writeln(ans);
	}
	return 0;
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<vector>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
const int maxn=100003;
const int inf=2147483647;
const unsigned int uinf=4294967295u;
int a[maxn];
int n,Q,p,q;
void input()
{
	read(n);read(Q);
	for(int i=1;i<=n;++i)
	{
		read(a[i-1]);
	}
}

void solve()
{

	int sum;
	for(int e=1;e<=Q;++e)
	{
		sum=0;
		read(q),read(p);
		for(int i=q;i<=n-1;i+=p)
		{
			sum+=a[i];
		}
		printf("%d\n",sum);
	}
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	input();
	solve();
	return 0;
}

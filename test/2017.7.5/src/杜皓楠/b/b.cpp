#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<vector>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
const int maxn=1003;
const int maxm=1003;
const int inf=2147483647;
const unsigned int uinf=4294967295u;
int n,m,k;
int a[maxn][maxm];
void input()
{
	read(n),read(m),read(k);
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=m;++j)
		{
			read(a[i][j]);
			a[i][j]%=k;
		}
	}
}
int ansr[maxn],ansc[maxm];
int r[maxn],c[maxm];
int sumr[maxn],sumc[maxm];
int ans=inf;
bool judge()
{
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=m;++j)
		{
			if((r[i]+c[j]+a[i][j])%k!=0)return false;
		}
	}
	if(sumr[n]+sumc[m]<ans)
	{
		ans=sumr[n]+sumc[m];
		memcpy(ansr+1,r+1,n*(sizeof(int)));
		memcpy(ansc+1,c+1,m*(sizeof(int)));
		return true;
	}
	else return false;
}

void dfs2(int pos)
{
	if(sumc[pos-1]+sumr[n]>ans)return;
	if(pos==m+1)
	{
		judge();
		return;
	}
	sumc[pos]=sumc[pos-1];
	for(int i=0;i<=k;++i)
	{
		c[pos]=i;
		sumc[pos]+=c[pos];
		dfs2(pos+1);
		sumc[pos]-=c[pos];
	}
}
void dfs1(int pos)
{
	if(sumr[pos-1]>ans)return;
	if(pos==n+1)
	{
		dfs2(1);
		return;
	};
	sumr[pos]=sumr[pos-1];
	for(int i=0;i<=k;++i)
	{
		r[pos]=i;
		sumr[pos]+=r[pos];
		dfs1(pos+1);
		sumr[pos]-=r[pos];
	}
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	input();
	dfs1(1);
	printf("%d\n",ans);
	for(int i=1;i<=n;++i)
	{
		printf("%d ",ansr[i]);
	}
	printf("\n");
	for(int i=1;i<=m;++i)
	{
		printf("%d ",ansc[i]);
	}
	return 0;
}

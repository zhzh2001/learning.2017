#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<vector>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
const int maxn=100003; 
const int inf=2147483647;
const unsigned int uinf=4294967295u;
int n,q,a[maxn],suma[maxn];
inline int lowbit(int x){return x&-x;}
void add(int x,int k)
{
	while(x<=n)
	{
		suma[x]+=k;
		x+=lowbit(x);
	}
}
long long getsum(int x)
{
	long long summ=0;
	while(x>0)
	{
		summ+=suma[x];
		x-=lowbit(x);
	}
	return summ;
}
void input()
{
	read(n);read(q);
	for(int i=1;i<=n;++i)
	{
		read(a[i]);
		add(i,a[i]);
	}
}

int que[maxn],f=0;
long long ans=0ll;
inline bool empty(){return f==0;}
inline void push(int w){que[++f]=w;}
inline int front(){return que[1];}
inline int top(){return que[f];}
inline void clear(){f=0;}
inline void pop(){--f;}
int water[maxn];

inline long long calcWater(int l,int r)
{
	if(r-l<=1)return 0ll;
	long long h=Min(a[l],a[r]);
	long long leng=r-l-1;
	return leng*h-(getsum(r-1)-getsum(l));
}
void Init()
{
	ans=0ll;
	clear();
	for(int i=1;i<=n;++i)
	{
		if(!empty())
		{
			if(a[i]>a[front()])
			{
				water[i]=calcWater(front(),i);
				ans+=water[front()];
				clear();
			}
			else
			{
				while(!empty()&&a[top()]<a[i])
					pop();
				water[i]=calcWater(top(),i);
			}
		}
		push(i);
	}
	for(int i=1;i<=f;++i)
	{
		ans+=water[que[i]];
	}
}
inline void up(int x,int v)
{
	a[x]+=v;
	add(x,v);
}
inline long long query()
{
	Init();
	return ans;
}
void solve()
{
	char op;
	int x,v;
	for(int i=1;i<=q;++i)
	{
		do{op=getchar();}
		while(op!='P'&&op!='U');
		if(op=='P')
		{
			printf("%lld\n",query());
		}
		else if(op=='U') 
		{ 
			read(x),read(v);
			up(x,v);
		}
	}
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	input();
	//Init();
	solve();
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
const int N=1e5+5;
int n,q,a[N];
inline int js()
{
	int sum=0,mi=a[1];
	int ii=1,il=1;
	while(ii<=n)
	{
		if (ii==n)
		{
			if (il==n)return sum;
			if (mi==a[ii])return sum;
			for (int i=il+1;i<n;i++)sum+=max(min(a[n],a[il])-a[i],0);
		}
		if (a[ii+1]>mi)
		{
			if (il==1)
			{
				if (ii==1){il=ii+1;ii++;mi=2147483647;continue;}
			}
			for (int i=il+1;i<=ii;i++)sum+=max(min(a[il],a[ii+1])-a[i],0);
			ii++;
			il=ii;
			mi=2147483647;
			continue;
		}
		mi=min(mi,a[ii+1]);
		ii++;
	}
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	while(q--)
	{
		char ch[10];
		scanf("%s",ch+1);
		if (ch[1]=='P'){printf("%d\n",js());continue;}
		int x,y;
		scanf("%d%d",&x,&y);
		a[x]+=y;
	}
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
const int N=1e5+5;
int wat[N],wai,wal[N],war[N];
int n,q,a[N],flag=0;
inline int js()
{
	int sum=0,mi=a[1];
	int ii=1,il=1;
	while(ii<=n)
	{
		if (ii==n)
		{
			if (il==n)return sum;
			if (mi==a[ii])return sum;
			for (int i=il+1;i<n;i++)sum+=max(min(a[n],a[il])-a[i],0);
		}
		if (a[ii+1]>mi)
		{
			if (il==1)
			{
				if (ii==1){il=ii+1;ii++;mi=2147483647;continue;}
			}
			for (int i=il+1;i<=ii;i++)sum+=max(min(a[il],a[ii+1])-a[i],0);
			ii++;
			il=ii;
			mi=2147483647;
			continue;
		}
		mi=min(mi,a[ii+1]);
		ii++;
	}
}
void solve()
{
	int ii=1;
	wai=1;wal[1]=1;
	int sum=0,mi=a[1];
	while(ii<=n)
	{
		if (ii==n)
		{
			sum=0;
			if (wal[wai]==n){wai--;break;}
			if (mi==a[ii]){wai--;break;}
			for (int i=wal[wai]+1;i<n;i++)sum+=max(min(a[n],a[wal[wai]])-a[i],0);
			wat[wai]=sum;
			war[wai]=n;
			break;
		}
		if (a[ii+1]>mi)
		{
			if (wal[wai]==1)
			{
				if (ii==1){wal[wai]=ii+1;ii++;mi=2147483647;continue;}
			}
			sum=0;
			for (int i=wal[wai]+1;i<=ii;i++)sum+=max(min(a[wal[wai]],a[ii+1])-a[i],0);
			wat[wai]=sum;
			war[wai]=ii+1;
			ii++;
			wai++;
			wal[wai]=ii;
			mi=2147483647;
			continue;
		}
		mi=min(mi,a[ii+1]);
		ii++;
	}
}
int main()
{
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	solve();
	/*for (int i=1;i<=wai;i++)
		printf("%d ",wal[i]);
	puts("");
	for (int i=1;i<=wai;i++)
		printf("%d ",war[i]);
	puts("");
	for (int i=1;i<=wai;i++)
		printf("%d ",wat[i]);*/
	while(q--)
	{
		char ch=[10];
		scanf("%s",ch+1);
		if (ch[1]=='P')
		{
			int sum=0;
			for (int i=1;i<=wai;i++)
				sum+=wat[i];
			printf("%d\n",sum);
			continue;
		}
		int x,v;
		scanf("%d%d",&x,&v);
		a[x]+=v;
		if (flag>10){solve();flag=0;continue;}
		for (int i=1;i<=wai;i++)
		{
			if (i>=wal[i]&&i<=war[i])
			{
				if (i==1)
				{
					int ma=-2147483647,mi,sum;
					for (int j=wal[i];j<=war[i];j++)
						if (a[j]>ma)
						{
							ma=a[j];
							mi=j;
						}
					if (mi==1)
					{
						sum=0;
						for (int j=wal[1]+1;j<=war[1]-1;j++)sum+=max(min(a[wal[1]],a[war[1]])-a[j],0);
						wat[1]=sum;
						break;
					}
					if (mi==war[i])
					{
						sum=0;
						for (int j=wal[1]+1;j<=war[1]-1;j++)sum+=max(min(a[wal[1]],a[war[1]])-a[j],0);
						wat[1]=sum;
						sum=0;
						for (int j=wal[2]+1;j<=war[2]-1;j++)sum+=max(min(a[wal[2]],a[war[2]])-a[j],0);
						wat[2]=sum;
						break;
					}
					flag++;
					sum=0;
					for (int j=2;j<mi;j++)sum+=max(min(a[1],ma)-a[j],0);
					wat[1]=sum;
					break;
				}
				if (i==wai)
				{
					int ma=-2147483647,mi,sum;
					for (int j=wal[i];j<=war[i];j++)
						if (a[j]>ma)
						{
							ma=a[j];
							mi=j;
						}
					if (mi==1)
					{
						sum=0;
						for (int j=wal[1]+1;j<=war[1]-1;j++)sum+=max(min(a[wal[1]],a[war[1]])-a[j],0);
						wat[1]=sum;
						break;
					}
					if (mi==war[i])
					{
						sum=0;
						for (int j=wal[1]+1;j<=war[1]-1;j++)sum+=max(min(a[wal[1]],a[war[1]])-a[j],0);
						wat[1]=sum;
						sum=0;
						for (int j=wal[2]+1;j<=war[2]-1;j++)sum+=max(min(a[wal[2]],a[war[2]])-a[j],0);
						wat[2]=sum;
						break;
					}
					flag++;
					sum=0;
					for (int j=2;j<mi;j++)sum+=max(min(a[1],ma)-a[j],0);
					for (int j=mi+1;j<=war[1]-1;j++)sum+=max(min(ma,a[war[1]])-a[j],0);
					wat[1]=sum;
					break;
				}
				int ma=-2147483647,mi,sum;
					for (int j=wal[i];j<=war[i];j++)
						if (a[j]>ma)
						{
							ma=a[j];
							mi=j;
						}
					if (mi==1)
					{
						sum=0;
						for (int j=wal[1]+1;j<=war[1]-1;j++)sum+=max(min(a[wal[1]],a[war[1]])-a[j],0);
						wat[1]=sum;
						break;
					}
					if (mi==war[i])
					{
						sum=0;
						for (int j=wal[1]+1;j<=war[1]-1;j++)sum+=max(min(a[wal[1]],a[war[1]])-a[j],0);
						wat[1]=sum;
						sum=0;
						for (int j=wal[2]+1;j<=war[2]-1;j++)sum+=max(min(a[wal[2]],a[war[2]])-a[j],0);
						wat[2]=sum;
						break;
					}
					flag++;
					sum=0;
					for (int j=2;j<mi;j++)sum+=max(min(a[1],ma)-a[j],0);
					wat[1]=sum;
					break;
				}
				if (i==wai)
				{
					int ma=-2147483647,mi,sum;
					for (int j=wal[i];j<=war[i];j++)
						if (a[j]>ma)
						{
							ma=a[j];
							mi=j;
						}
					if (mi==1)
					{
						sum=0;
						for (int j=wal[wai]+1;j<=war[wai]-1;j++)sum+=max(min(a[wal[wai]],a[war[wai]])-a[j],0);
						wat[wai]=sum;
						break;
					}
					if (mi==war[i])
					{
						sum=0;
						for (int j=wal[wai]+1;j<=war[wai]-1;j++)sum+=max(min(a[wal[wai]],a[war[wai]])-a[j],0);
						wat[wai]=sum;
						sum=0;
						for (int j=wal[wai-1]+1;j<=war[wai-1]-1;j++)sum+=max(min(a[wal[wai-1]],a[war[wai-1]])-a[j],0);
						wat[wai-1]=sum;
						break;
					}
					flag++;
					sum=0;
					for (int j=n;j>mi;j--)sum+=max(min(a[n],ma)-a[j],0);
					for (int j=mi+1;j>=war[wai-1]+1;j--)sum+=max(min(ma,a[war[wai-1]])-a[j],0);
					wat[wai]=sum;
					break;
				}
					int ma=-2147483647,mi,sum;
					for (int j=wal[i];j<=war[i];j++)
						if (a[j]>ma)
						{
							ma=a[j];
							mi=j;
						}
					if (mi==wal[i])
					{
						sum=0;
						for (int j=wal[wai]+1;j<=war[wai]-1;j++)sum+=max(min(a[wal[wai]],a[war[wai]])-a[j],0);
						wat[wai]=sum;
						break;
					}
					if (mi==war[i])
					{
						sum=0;
						for (int j=wal[wai]+1;j<=war[wai]-1;j++)sum+=max(min(a[wal[wai]],a[war[wai]])-a[j],0);
						wat[wai]=sum;
						sum=0;
						for (int j=wal[wai-1]+1;j<=war[wai-1]-1;j++)sum+=max(min(a[wal[wai-1]],a[war[wai-1]])-a[j],0);
						wat[wai-1]=sum;
						break;
					}
					flag++;
					sum=0;
					for (int j=n;j>mi;j--)sum+=max(min(a[n],ma)-a[j],0);
					for (int j=mi+1;j>=war[wai-1]+1;j--)sum+=max(min(ma,a[war[wai-1]])-a[j],0);
					wat[wai]=sum;
					break;				
			}
		}
	}
}

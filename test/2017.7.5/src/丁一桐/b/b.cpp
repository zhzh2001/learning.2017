#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=1005;
int n,m,p,ans,a[maxn][maxn],b[maxn],c[maxn],ansb[maxn],ansc[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void make(){
	for (int i=1;i<=n;i++) ansb[i]=b[i];
	for (int i=1;i<=m;i++) ansc[i]=c[i];
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(); m=read(); p=read();
	for (int i=1;i<=n;i++) 
	 for (int j=1;j<=n;j++)
	  a[i][j]=p-read()%p;
	ans=1<<30;
	for (int k=0;k<=p;k++) {
		memset(b,0,sizeof(b));
		memset(c,0,sizeof(c));
		b[1]=k; c[1]=((a[1][1]+p)-k)%p; int sum=0;
		for (int i=1;i<=n;i++) b[i]=((a[i][1]+p)-c[1])%p,sum+=b[i];
		for (int i=1;i<=m;i++) c[i]=((a[1][i]+p)-b[1])%p,sum+=c[i];
		bool check=true;
		for (int i=1;i<=n;i++)
		 for (int j=1;j<=m;j++) 
		 if ((((a[i][j]+p)-b[i])+p-c[j])%p!=0) {check=false; break;}
		if (check==true&&sum<ans) {ans=sum; make();}
	}
	printf("%d\n",ans);
	for (int i=1;i<=n;i++) printf("%d ",ansb[i]); printf("\n");
	for (int i=1;i<=m;i++) printf("%d ",ansc[i]);
	return 0;
}

#include<cstdio>
#include<string>
using namespace std;
const int maxn=100005;
int n,q,ans,top,last,a[maxn];
bool check;
struct dyt{
	int x,id;
}que[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void get(){
	ans=0; top=0; last=0;
	for (int i=1;i<=n;i++) {
		if (a[i]<que[top].x) {top++;que[top].x=a[i]; que[top].id=i;} else{
			while (top>0&&que[top].x<=a[i]) {ans+=(i-que[top].id-1)*(que[top].x-last); last=que[top].x; top--;}
			if (top>0) ans+=(i-que[top].id-1)*(a[i]-last);
		    top++,que[top].x=a[i],que[top].id=i; last=0;	
		}
	}
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	get(); check=true;
	for (int i=1;i<=q;i++) {
	    char ch=getchar(); while (ch!='P'&&ch!='U') ch=getchar();
	    if (ch=='P') {
	    	if (check==false) get();
	    	printf("%d\n",ans);
	    } else {
	    	check=false;
	    	int x=read(),y=read();
	    	a[x]+=y;
	    }
	}
	return 0;
}

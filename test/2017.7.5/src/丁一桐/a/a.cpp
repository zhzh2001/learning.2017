#include<cstdio>
#include<string>
using namespace std;
const int maxn=100005;
int n,q,a[maxn],ans;
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read(); q=read();
	for (int i=0;i<n;i++) a[i]=read();
	for (int i=1;i<=q;i++) {
		int y=read(),x=read(),j=y; ans=0;
		while (j<=n) {ans+=a[j];j+=x;}
	    printf("%d\n",ans);
	}
	return 0;
}

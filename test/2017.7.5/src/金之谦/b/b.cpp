#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,K,a[1001][1001],ans=1e16,hang[1001],lie[1001];
inline int rk(int x,int y){
	if(x>y)return y+K-x;
	else return y-x;
}
inline void work1(){
	for(int i=1;i<=n;i++){
		int p=0,minn=1e16;
		for(int j=1;j<=n;j++)p+=rk(a[j][1],a[i][1]);
		for(int j=1;j<=m;j++)p+=rk(a[i][j],0),minn=min(minn,rk(a[i][j],0));
		if(minn)p=p-minn*m+minn*n;
		if(p<ans){
			ans=p;
			for(int j=1;j<=n;j++)hang[j]=rk(a[j][1],a[i][1])+minn;
			for(int j=1;j<=m;j++)lie[j]=rk(a[i][j],0)-minn;
		}
	}
}
inline void work2(){
	for(int i=1;i<=m;i++){
		int p=0,minn=1e16;
		for(int j=1;j<=m;j++)p+=rk(a[1][j],a[1][i]);
		for(int j=1;j<=n;j++)p+=rk(a[j][i],0),minn=min(minn,rk(a[j][i],0));
		if(minn)p=p-minn*n+minn*m;
		if(p<ans){
			ans=p;
			for(int j=1;j<=m;j++)lie[j]=rk(a[1][j],a[1][i])+minn;
			for(int j=1;j<=n;j++)hang[j]=rk(a[j][i],0)-minn;
		}
	}
}
signed main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();m=read();K=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)a[i][j]=read();
	if(n<m)work1();
	else work2();
	printf("%lld\n",ans);
	for(int i=1;i<=n;i++)printf("%lld ",hang[i]);puts("");
	for(int i=1;i<=m;i++)printf("%lld ",lie[i]);
	return 0;
}
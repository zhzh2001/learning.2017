#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,a[100001],s[100001];
ll ans=0;
inline void work(){
	ans=0;s[0]=0;
	for(int i=1;i<=n;i++){
		while(s[0]&&a[i]>=a[s[s[0]]]){
			if(s[0]>1)ans+=(ll)(i-s[s[0]-1]-1)*(min(a[i],a[s[s[0]-1]])-a[s[s[0]]]);
			s[0]--;
		}
		s[++s[0]]=i;
	}
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();m=read();int la=0,now;
	for(int i=1;i<=n;i++)a[i]=read();work();
	for(int i=1;i<=m;i++){
		char c[5];scanf("%s",c+1);
		now=c[1]=='P'?0:1;int x;
		if(now)x=read(),a[x]+=read();
		else{
			if(la)work();
			printf("%lld\n",ans);
		}
		la=now;
	}
	return 0;
}
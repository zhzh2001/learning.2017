#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,a[100001],ans[100001];
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();m=read();
	for(int i=0;i<n;i++)a[i]=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),ans=0;
		for(int j=x;j<n;j+=y)ans+=a[j];
		printf("%d\n",ans);
	}
	return 0;
}
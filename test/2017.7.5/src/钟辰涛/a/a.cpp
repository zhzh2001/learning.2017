#include<cstdio>
#include<cmath>
#include<ctime>
using namespace std;
int A[100005],F[1010][1010],Q,n,x,y;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&Q);
	for (int i=0; i<n; i++) scanf("%d",&A[i]);
	for (int i=1; i<=sqrt(n); i++) 
		for (int j=0; j<i; j++)
			for (int k=j; k<n; k+=i) F[i][j]+=A[k];
	while (Q--){
		scanf("%d%d",&x,&y);
		if (y<=sqrt(n)) printf("%d\n",F[y][x]);
		else {
		int sum=0;
		for (int i=x; i<n; i+=y) sum+=A[i];
		printf("%d\n",sum);
	}}
}

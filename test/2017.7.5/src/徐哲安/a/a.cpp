#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int maxn = 100009;
int a[maxn], n, Q, p, q, ans;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

int main(){
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	n = read(); Q = read();
	for (int i=0; i<n; i++) a[i] = read();
	while (Q--){
		q = read(); p = read();
		ans = 0;
		while (q < n){
			ans += a[q];
			q += p;
		}
		printf("%d\n", ans);
	}
	return 0;
}

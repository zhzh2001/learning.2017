#include<cstdio>
#include<cstring>
#include<algorithm>
#define lc (o << 1)
#define rc (o << 1 | 1)
#define mid ((l+r) >> 1)
#define len ((LL)r-l+1)
using namespace std;
typedef long long LL;

const int maxn = 100009;
struct segmentTree{
	LL val, lazy;
	LL lh, rh;
} T[maxn<<2];
LL total, a[maxn];
int n, Q, x, y, p = 1;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

void pushdown(int o, LL l, LL r){
	if (!T[o].lazy) return;
	T[lc].lazy = T[o].lazy;
	T[lc].val = (mid-l+1)*T[o].lazy;
	T[lc].lh = T[lc].rh = T[o].lazy;
	T[rc].lazy = T[o].lazy;
	T[rc].val = (r-mid)*T[o].lazy;
	T[rc].lh = T[rc].rh = T[o].lazy;
	T[o].lazy = 0;
}

void update(int o, int l, int r, int x, int y, LL z){
	if (z <= min(T[o].lh, T[o].rh) || x > y) return;
	if (l == x && y == r && max(T[o].lh, T[o].rh) < z){
		T[o].lh = T[o].rh = z;
		T[o].val = len * z;
		T[o].lazy = z;
		return;
	}
	pushdown(o, l, r);
	if (x <= mid) update(lc, l, mid, x, min(y, mid), z);
	if (mid+1 <= y) update(rc, mid+1, r, max(x, mid+1), y, z);
	T[o].val = T[lc].val + T[rc].val;
	T[o].lh = T[lc].lh; T[o].rh = T[rc].rh;
}

LL query(int o, int l, int r, int x, int y){
	if (l == x && y == r) return T[o].val;
	LL res = 0; pushdown(o, l, r);
	if (x <= mid) res += query(lc , l, mid, x, min(y, mid));
	if (mid+1 <= y) res += query(rc, mid+1, r, max(x, mid+1), y);
	return res;
}

char s[5];
int main(){
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	n = read(); Q = read();
	for (int i=1; i<=n; i++){
		a[i] = read(); total += a[i];
		if (a[i] >= a[p]) p = i;
	}
	for (int i=1; i<=n; i++)
		if (i <= p) update(1, 1, n, i, p, a[i]);
		else update(1, 1, n, p+1, i, a[i]);
	while (Q--){
		scanf("%s", s);
		if (s[0] == 'U'){
			x = read(); y = read();
			total += y; a[x] += y;
			if (a[x] > a[p] || a[x] == a[p] && x > p){
				update(1, 1, n, p, x, a[p]);
				p = x;
			}
			if (x <= p) update(1, 1, n, x, p, a[x]);
			else update(1, 1, n, p+1, x, a[x]);
		}
		else printf("%lld\n", query(1, 1, n, 1, n)-total);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;

const int maxn = 1009;
int mat[maxn][maxn], a[maxn], b[maxn];
int n, m, K;
LL x, ansx;
LL res, ans = 1e18;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

LL solve(LL x){
	LL res = 0;
	for (int i=1; i<=n; i++)
		res += ((x+a[i])%K+K)%K;
	for (int j=1; j<=m; j++)
		res += ((b[j]-x)%K+K)%K;
	return res;
}

int main(){
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	n = read(); m = read(); K = read();
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++)
			mat[i][j] = read();
	for (int i=1; i<=n; i++)
		a[i] = mat[1][1] - mat[i][1];
	for (int j=1; j<=m; j++)
		b[j] = K - mat[1][j];
	for (int i=1; i<=n; i++){
		x = -a[i];
		res = solve(x);
		if (res < ans){
			ans = res;
			ansx = x;
		}
	}
	for (int i=1; i<=m; i++){
		x = b[i];
		res = solve(x);
		if (res < ans){
			ans = res;
			ansx = x;
		}
	}
	printf("%lld\n", ans);
	for (int i=1; i<=n; i++) printf("%lld ", ((ansx+a[i])%K+K)%K);
	puts("");
	for (int j=1; j<=m; j++) printf("%lld ", ((b[j]-ansx)%K+K)%K);
	return 0;
}

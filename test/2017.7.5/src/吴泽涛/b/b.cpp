#include <cstdio>
using namespace std ; 

int n,m,k,mi ; 
int b[21],a[6][6],t[6][6],ans[21] ; 

inline void check() 
{
	int sum = 0,tt ; 
	//for(int i=1;i<=n/2;i++) { tt = b[ i ] ; b[ i ] = b[ n-i+1 ] ; b[ n-i+1 ] = tt ;  }
	for(int i=1;i<=n+m;i++) sum+=b[ i ] ; 
	if(sum > mi ) return ;
	 
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++) t[ i ][ j ] = a[ i ][ j ] ; 
	for(int i=1;i<=n;i++) 
		for(int j=1;j<=m;j++) 
			t[ i ][ j ]+=b[ i ] ; 
	for(int j=n+1;j<=n+m;j++) 
		for(int i=1;i<=n;i++) 
			t[ i ][j-n]+=b[ j ] ; 
	bool flag = 0 ; 
	for(int i=1;i<=n;i++) 
		for(int j=1;j<=m;j++) 
			if(t[ i ][ j ]%k) return ; 
	mi = sum ; 
	for(int i=1;i<=n+m;i++)  ans[ i ] = b[ i ] ; 
}

inline void dfs(int x) 
{
	if(x>n+m) 
	{
		check() ; 
		return ; 
	}
	for(int i=0;i<=4;i++) 
	{
		b[ x ] = i ; 
		dfs(x+1) ; 
	}	
}

int main() 
{
	freopen("b.in","r",stdin) ; 
	freopen("b.out","w",stdout) ; 
	scanf("%d%d%d",&n,&m,&k) ; 
	mi = 1e9 ; 
	for(int i=1;i<=n;i++) 
		for(int j=1;j<=m;j++) scanf("%d",&a[ i ][ j ]) ; 
	dfs(1) ; 
	printf("%d\n",mi) ; 
	for(int i=1;i<=n;i++) printf("%d ",ans[ i ]) ; 
	printf("\n") ; 
	for(int i=n+1;i<=n+m;i++) printf("%d ",ans[ i ]) ; 
 	return 0 ; 
}

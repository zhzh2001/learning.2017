#include <cstdio>
#include <cstring>
#include <iostream>
#define ll long long 
using namespace std ; 

const int N = 1e5+11 ; 
int L[N],R[N],a[N] ; 
int n,Q,T,x,v ;
ll sum ;   
char s[3] ; 

inline int read() 
{
	int x = 0 ,f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48 ; ch = getchar() ; }
 	return x * f ; 
}

inline int max(int x,int y) 
{
	return x > y ? x:y ; 
}

inline int min(int x,int y) 
{
	return x < y ? x:y ; 
}

inline void pre() 
{
	
	for(int i=1;i<=n;i++) L[ i ] = max( L[ i-1 ],a[ i ] ) ; 
	for(int i=n;i>=1;i--) R[ i ] = max(R[ i+1 ],a[ i ]) ; 
}

int main()
{
	freopen("c.in","r",stdin) ; 
	freopen("c.out","w",stdout) ; 
		n = read() ; Q = read() ; 
		L[ 0 ] = -10000 ;  R[ n+1 ] = -10000 ;  
		for(int i=1;i<=n;i++) a[ i ] = read() ; 
		pre() ; 
		while(Q--) 
		{
			scanf("%s",&s) ;   
			if( s[ 0 ]=='U' ) 
			{
				scanf("%d%d",&x,&v) ;  a[x]+=v ; 
				pre() ; 
			} 
			else 
			{
				sum = 0 ; 
				for(int i=1;i<=n;i++) sum+=min(L[ i ],R[ i ]) - a[ i ] ; 
				printf("%I64d\n",sum) ; 
			}
		} 
	
	return 0 ; 
}

#include <cstdio>
#include <algorithm> 
#include <iostream>
using namespace std ; 

const int N = 1e5 + 11,alpha = 300 ;  
int n,Q,p,q,sum,x ; 
int a[ N ],w[alpha][alpha] ; 

inline int read() 
{
	int x = 0 , f = 1 ; 
	char ch = getchar() ; 
	while(ch<'0'||ch>'9') { if(ch=='-')f = -1 ; ch = getchar() ; } 
	while(ch>='0'&&ch<='9') { x =x * 10+ch-48; ch = getchar() ; }
	return x * f ; 
}

int main() 
{
	freopen("a.in","r",stdin) ; 
	freopen("a.out","w",stdout) ; 
	n = read() ; Q = read() ;  
	for(int i=0;i<n;i++) a[ i ] = read() ; 
	if(n<=4000&&Q<=4000) 
	{
	
	for(int i=1;i<=Q;i++) 
	{
		q = read() ; p = read() ; sum = 0 ; 
		while(q<=n) 
		{
			sum+=a[ q ] ; 
			q+=p ; 
		}
		printf("%d\n",sum ) ; 
	}
	
	}
	else
	{
		
		for(int j=1;j<=alpha;j++) 
			for(int i=0;i<n;i++) 			
			{
				x = i % j ; 
				w[ x ][ j ]+=a[ i ] ;  
			}
		
		for(int i=1;i<=Q;i++) 
		{
			q = read() ; p = read() ; sum = 0 ; 
			
			if( p > alpha ) 
			{
				
				while(q<=n) 
				{
					sum+=a[ q ] ; 
					q+=p ; 
				}
				printf("%d\n",sum ) ; 
			}
			else printf("%d\n",w[ q ][ p ]) ; 
		}	
	}
	return 0 ; 
}

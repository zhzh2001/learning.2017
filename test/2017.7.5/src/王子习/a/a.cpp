#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=500007,M=407;
int n,m,f[M][M],a[N];

int main(){
	// say hello
	
    freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	
	n=read(),m=read();
	For(i,0,n-1){
		a[i]=read();
		For(j,1,300) f[j][i%j]+=a[i];
	}
	For(i,1,m){
		int q=read(),p=read();
		if (p<=300){
			printf("%d\n",f[p][q]);
			continue;
		}
		else{
			int ans=0;
			for (int x=q;x<=n;x+=p) 
				ans+=a[x];
			printf("%d\n",ans);
		}
	}

	// say goodbye
}



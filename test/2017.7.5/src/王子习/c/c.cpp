#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=100007;
int n,m,a[N],l[N],r[N];

int main(){
	// say hello

    freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	
	n=read(),m=read();
	For(i,1,n) a[i]=read();
	For(i,1,m){
		char ch[5];
		scanf("%s",ch);
		if (ch[0]=='P'){
			int ans=0;
			For(i,1,n) l[i]=max(a[i-1],l[i-1]);
			Rep(i,1,n) r[i]=max(a[i+1],r[i+1]);
			For(i,1,n) ans+=max(0,min(l[i],r[i])-a[i]);
			printf("%d\n",ans);
		}
		else {
			int x=read(),k=read();
			a[x]+=k;
		}
	}

	// say goodbye
}



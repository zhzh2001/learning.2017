#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=107,INF=1<<27;
int n,m,p,tmp,ans,X[N],Y[N],Res_X[N],Res_Y[N];
int a[N][N];

void check(){
	if (tmp>ans) return;
	For(i,1,n) For(j,1,m){
		if ((a[i][j]+X[i]+Y[j])%p!=0)
			return;
	}
	ans=tmp;
	For(i,1,n) Res_X[i]=X[i];
	For(i,1,m) Res_Y[i]=Y[i];
}

void dfs(int x){
	if (x>n+m){
		check();
		return;
	}
	For(k,0,p-1){
		if (x<=n) X[x]=k;
		else Y[x-n]=k;
		tmp+=k;
		dfs(x+1);
		tmp-=k;
	}
}

int main(){
	// say hello

    freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	
	n=read(),m=read();p=read();
	For(i,1,n) For(j,1,m) a[i][j]=read();
	ans=INF;
	dfs(1);
	printf("%d\n",ans);
	For(i,1,n) printf("%d ",Res_X[i]); printf("\n");
	For(i,1,n) printf("%d ",Res_Y[i]);

	// say goodbye
}



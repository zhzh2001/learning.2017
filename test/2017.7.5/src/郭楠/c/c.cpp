#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 100005
using namespace std;
int a[N],n,m,x,y,lmax[N],rmax[N],s,sum;
char c[5];
int read()
{int t=0;char c;
	c=getchar();
	while(!(c>='0' && c<='9')) c=getchar();
	while(c>='0' && c<='9')
	 {
	 	t=t*10+c-48;
	 	c=getchar();
	 }
	return t;
}
int main()
{int i,j,k;
    freopen("c.in","r",stdin);
    freopen("c.out","w",stdout);
    scanf("%d%d",&n,&m);
    for(i=1;i<=n;i++) scanf("%d",&a[i]);
    s=a[1];
    for(i=2;i<n;i++)
      lmax[i]=s,s=max(s,a[i]);
    s=a[n];
    for(i=n-1;i>1;i--)
      rmax[i]=s,s=max(s,a[i]);
    sum=0;
    for(i=2;i<n;i++) sum+=max(0,min(lmax[i],rmax[i])-a[i]);
    for(;m;m--)
     {
     	scanf("%s",c);
     	if(c[0]=='P') printf("%d\n",sum);
     	  else
     	    {
     	    	scanf("%d%d",&x,&y);
     	    	a[x]+=y;
     	    	s=a[1];
                for(i=2;i<n;i++)
      				lmax[i]=s,s=max(s,a[i]);
    			s=a[n];
    			for(i=n-1;i>1;i--)
      				rmax[i]=s,s=max(s,a[i]);
    			sum=0;
    			for(i=2;i<n;i++) sum+=max(0,min(lmax[i],rmax[i])-a[i]);
     	    }
     }
}


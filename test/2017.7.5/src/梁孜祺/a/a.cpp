#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<math.h>
#include<bitset>
#include<map>
#include<set>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#define ll long long
#define inf 1e9
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
#define N 100005
int a[N],n,Q,m,b[350][350];
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();Q=read();
	For(i,0,n-1) a[i]=read();
	m=sqrt(n);
	For(i,1,m)
		For(j,0,i-1)
			for(int k=j;k<n;k+=i) b[i][j]+=a[k];
	while(Q--){
		int q=read(),p=read(),ans=0;
		if(p<=m){writeln(b[p][q]);continue;}
		for(int i=q;i<n;i+=p) ans+=a[i];
		writeln(ans);
	}
	return 0;
}
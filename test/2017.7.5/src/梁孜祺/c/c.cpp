#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<math.h>
#include<bitset>
#include<map>
#include<set>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#define ll long long
#define inf 1e9
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
#define N 100005
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,Q,a[N],t[N];
struct data{int x,sum;}st[N];
inline void add(int x,int val){
	for(;x<=n;x+=(x&-x)) t[x]+=val;
}
inline int query(int x){
	int sum=0;
	for(;x;x-=(x&-x)) sum+=t[x];
	return sum;
}
inline ll query(){
	int top=0;ll ans=0,sum=0;
	For(i,1,n){
		while(top&&st[top].sum<=a[i]) sum=(i-st[top].x-1)*st[top].sum-(query(i-1)-query(st[top].x)),top--;
		st[++top]=(data){i,a[i]};
		if(top==1) ans+=sum;
	}
	while(top>1) ans+=(st[top].x-st[top-1].x-1)*st[top].sum-(query(st[top].x-1)-query(st[top-1].x)),top--;
	return ans;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();Q=read();
	For(i,1,n) a[i]=read();
	For(i,1,n) add(i,a[i]);
	while(Q--){
		char s[5];scanf("%s",s);
		if(s[0]=='U'){
			int x=read(),v=read();
			add(x,v);a[x]+=v;
		}else writeln(query());
	}
	return 0;
}
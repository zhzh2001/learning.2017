#include<stdio.h>
#include<string.h>
#include<string>
#include<iostream>
#include<math.h>
#include<bitset>
#include<map>
#include<set>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#define ll long long
#define inf 1e9
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
#define N 1005
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n,m,K,a[N][N],b[N],c[N],mx,d[N],e[N];
ll ans,p[N];
inline void solve(){
	For(i,1,n){
		ans+=(K-a[i][mx])%K;b[i]=(K-a[i][mx])%K;
		For(j,1,m) a[i][j]=(a[i][j]+K-a[i][mx])%K;
	}
	For(j,1,m){
		ans+=(K-a[1][j])%K;
		c[j]=(K-a[1][j])%K;
	}
	writeln(ans);
}
inline void baoli(){
	ans=inf;
	For(k,0,K-1){
		ll sum=k;d[1]=k;
		For(j,1,m) a[1][j]=(a[1][j]+k)%K;
		For(i,2,n) sum+=(d[i]=(a[1][1]+K-a[i][1])%K);
		For(j,1,m) sum+=(e[j]=(K-a[1][j])%K);
		if(sum<ans){
			For(i,1,n) b[i]=d[i];
			For(i,1,m) c[i]=e[i];
			ans=sum;
		}
	}
	writeln(ans);
	For(i,1,n) printf("%d ",b[i]);
	printf("\n");
	For(i,1,m) printf("%d ",c[i]);
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();m=read();K=read();
	For(i,1,n) For(j,1,m) a[i][j]=read()%K;
	if(K<=5){baoli();return 0;}
	For(j,1,m){
		For(i,1,n) p[j]+=K-a[i][j];
	}
	For(j,1,m) if(p[mx]<p[j]) mx=j;
	solve();
	For(i,1,n) printf("%d ",b[i]);
	printf("\n");
	For(i,1,m) printf("%d ",c[i]);
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int MAXN=100000+10;
int n;
int a[MAXN];
struct tree
{
	int l,r;
	long long val;
	int max1,max2;
	int max1_pos,max2_pos;
	tree* ch[2];
	tree() {ch[0]=ch[1]=NULL;}
	void push_up()
	{
		val=ch[0]->val+ch[1]->val;
		int j; 
		if(ch[0]->max1>ch[1]->max1) 
			max1=ch[0]->max1,j=0,max1_pos=ch[0]->max1_pos;
		else max1=ch[1]->max1,j=1,max1_pos=ch[1]->max1_pos;
		if(ch[j]->max1_pos==ch[j]->max2_pos)
			max2=max2=ch[j^1]->max1,max2_pos=ch[j^1]->max1_pos;
		else
		{
			if(ch[j^1]->max1>ch[j]->max2)
				max2=ch[j^1]->max1,max2_pos=ch[j^1]->max1_pos;
			else 	
				max2=ch[j]->max2,max2_pos=ch[j]->max2_pos;
		}
	}
}*rt,pool[MAXN<<1];
int cnt;
void build(tree* &o,int l,int r)
{
	if(o==NULL) o=&pool[++cnt];
	o->l=l,o->r=r;
	if(l==r)
	{
		o->val=o->max1=o->max2=a[l];
		o->max1_pos=o->max2_pos=l;
		return ;
	}
	int mid=l+r>>1;
	build(o->ch[0],l,mid);
	build(o->ch[1],mid+1,r);
	o->push_up();
	
}
void init()
{
	build(rt,1,n);
}
int tmp_1,tmp_2,tmp_p1,tmp_p2;
void maintain(tree* o)
{
	if(tmp_p1==tmp_p2)
	{
		if(tmp_1>o->max1) tmp_2=o->max1,tmp_p2=o->max1_pos;
		else if(tmp_1<o->max2)
		{
			tmp_1=o->max1,tmp_p1=o->max1_pos;
			tmp_2=o->max2,tmp_p2=o->max2_pos;
		}
		else tmp_2=o->max2,tmp_p2=o->max2_pos;
	}
	else if(o->max1_pos==o->max2_pos)
	{
		if(o->max1>tmp_1)
		{
			tmp_2=tmp_1,tmp_p2=tmp_p1;
			tmp_1=o->max1,tmp_p1=o->max1_pos;
		}
		else if(o->max1>tmp_2) tmp_2=o->max1,tmp_p2=o->max1_pos;
	}
	else if(tmp_1>o->max1)
	{
		if(o->max1>tmp_2) tmp_2=o->max1,tmp_p2=o->max1_pos;
	}
	else 
	{
		if(tmp_2<o->max2) tmp_2=o->max2,tmp_p2=o->max2_pos;
		tmp_1=o->max1,tmp_p1=o->max1_pos;
	}
}

long long query(tree* o,int l,int r)
{
	if(o->l==l&&o->r==r)
	{
		maintain(o);
		return o->val;
	}
	int mid=o->l+o->r>>1;
	if(r<=mid) return query(o->ch[0],l,r);
	else if(mid<l) return query(o->ch[1],l,r);
	else return query(o->ch[0],l,mid)+query(o->ch[1],mid+1,r);
}
long long solve(int l,int r)
{
	long long ans=0;
	if(r-l<=1) return ans;
	tmp_1=tmp_2=tmp_p1=tmp_p2=0;
	query(rt,l,r);
	if(tmp_p1>tmp_p2) swap(tmp_p1,tmp_p2);
	int i=tmp_p1,j=tmp_p2;
	if(tmp_p2-tmp_p1>1)
	{ 
		ans+=(tmp_p2-tmp_p1-1)*min(tmp_1,tmp_2);
		tmp_1=tmp_2=tmp_p1=tmp_p2=0;
		ans-=query(rt,i+1,j-1);
	}
	ans+=solve(l,i)+solve(j,r);
	return ans;
}
void update(tree* o,int x,int v)
{
	if(o->l==o->r&&o->l==x)
	{
		o->val+=v;
		o->max1+=v;
		o->max2+=v;
		return ;
	}
	int mid=o->l+o->r>>1;
	if(x<=mid) update(o->ch[0],x,v);
	else update(o->ch[1],x,v);
	o->push_up();
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	int Q; scanf("%d%d",&n,&Q);
	for(int i=1;i<=n;i++) scanf("%d",a+i);
	init();
	bool flg=1;
	long long ans;
	while(Q--)
	{
		char s[3]; scanf("%s",s);
		if(s[0]=='P') 
		{
			if(flg) 
				ans=solve(1,n);
			printf("%lld\n",ans);
			flg=0;
		}
		else if(s[0]=='U')
		{
			int x,v; scanf("%d%d",&x,&v);
			update(rt,x,v);
			flg=1;
		}
	}
}

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;
#define ll long long

inline int read()
{
    int x=0,f=1;
    char c=getchar();
    while(c<'0' || c>'9'){if(c=='-') f=-1;c=getchar();}
    while(c>='0' && c<='9'){x=x*10+c-48;c=getchar();}
    return x*f;
}

struct high
{
	int x,l;
	int he,next;
}hi[400005];

struct seg
{
	int l,r;
	ll sum;
}t[400005];

int n,q;
ll a[100005];
int b[100005];
int to[100005];
int ai[100005];
int cnt,now;

void addedge(int x)
{
	cnt++;
	hi[now].next=cnt;
	hi[cnt].he=now;
	hi[cnt].x=x;
	for(int i=x-1;i>=1;i--)
		if(b[i]==1)
		{
			hi[cnt].l=i;
			break;
		}
	hi[cnt].next=-1;
	to[x]=cnt;
	now=cnt;
}

void del(int x)
{
	int ti=to[x];
	hi[hi[ti].he].next=hi[ti].next;
	to[x]=0;
	hi[ti].next=-1;
} 

void build(int now,int l,int r)//建树 
{
	t[now].l=l; t[now].r=r; //t[now].lazy=0;
	if(l==r)
	{
		t[now].sum=a[l];
		return ;
	}
	int mid=(l+r)>>1;
	build(now<<1,l,mid);
	build(now<<1|1,mid+1,r);
	t[now].sum=t[now<<1].sum+t[now<<1|1].sum; 
}

void change(int k,int x,ll y)//线段树单点修改(将x的值改为y)
{
	int li=t[k].l,ri=t[k].r,mid=(li+ri)>>1;
	//if(t[k].lazy!=0)	push_down(k);
	if(li==ri)	{t[k].sum=y;	return ;}
	if(x<=mid)	change(k<<1,x,y);
	else	change(k<<1|1,x,y);
	t[k].sum=t[k<<1].sum+t[k<<1|1].sum;
}

ll querysum(int k,int a,int b)
{
	int li=t[k].l,ri=t[k].r,mid=(li+ri)>>1,lson=k<<1,rson=k<<1|1;
	ll ans=0;
	if(a<=li&&ri<=b)	
		return t[k].sum;
	else
	{
		//if(t[k].lazy!=0)	push_down(k);
		if(b<=mid)	ans+=querysum(k<<1,a,b);
		else if(a>mid)	ans+=querysum(k<<1|1,a,b);
		else	{ans+=querysum(k<<1,a,mid);	ans+=querysum(k<<1|1,mid+1,b);}
		return ans;
	}
}

int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	now=0;
	hi[0].x=hi[0].next=-1;
	int i,j;
	ll ans;
	n=read(),q=read();
	for(i=1;i<=n;i++)
		a[i]=read();	
	for(i=1;i<=n;i++)
		if(a[i]>a[i-1]&&a[i]>a[i+1])
			b[i]=1;
	for(i=1;i<=n;i++)
		if(b[i]==1)
			addedge(i);
	/*for(i=0;i!=-1;i=hi[i].next)
		if(i!=0)
			printf("%d ",hi[i].x);
	printf("\n");*/
//	for(i=1;i<=n;i++)
//		if(b[i]!=1)
			
	build(1,1,n);
	int li,ri,qu,ho;
	ans=0;
	for(i=hi[0].next;i!=-1;i=hi[i].next)
		if(hi[i].l!=0)
		{
			li=hi[i].l;
			ri=hi[i].x;
			qu=querysum(1,li,ri);
			qu=qu-a[li]-a[ri];
			ho=min(a[li],a[ri]);
			qu=ho*(ri-li+1-2)-qu;
		//	ai[hi[i].x]=qu;
			ans+=qu;
		}
//	printf("%d\n",ans);
	char ci;
	int x,v,nxt;
	for(i=1;i<=q;i++)
	{
		scanf("%s",&ci);
		if(ci=='P')
			printf("%lld\n",ans);
		else
		{
			scanf("%d %d",&x,&v);
			a[x]+=v;
			change(1,x,a[x]);
			//nxt=[hi[to[x]].next;
			if(a[x]>a[x-1]&&a[x]>a[x+1])
			{
				if(b[x]==0)
					addedge(x);
				if(b[x+1]==1)
					del(x+1);
				if(b[x-1]==1)
					del(x-1);
				for(j=0;j!=-1;j=hi[j].next)
					if(j!=0)
					{
						if(hi[j].l<x)
							hi[j].l=x;
					}
				b[x]=1;	 
			}
			ans=0;
			for(j=hi[0].next;j!=-1;j=hi[j].next)
				if(hi[j].l!=0)
				{
					li=hi[j].l;
					ri=hi[j].x;
					qu=querysum(1,li,ri);
					qu=qu-a[li]-a[ri];
					ho=min(a[li],a[ri]);
					qu=ho*(ri-li+1-2)-qu;
			//		ai[hi[j].x]=qu;
					ans+=qu;
				}
			/*for(i=0;i!=-1;i=hi[i].next)
				if(i!=0)
					printf("%d ",hi[i].x);
			printf("\n");*/
		}
	}
	return 0;	
}
 

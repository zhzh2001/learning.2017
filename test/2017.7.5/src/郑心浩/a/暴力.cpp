#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;

inline int read()
{
    int x=0,f=1;
    char c=getchar();
    while(c<'0' || c>'9')
    {
        if(c=='-') f=-1;
        c=getchar();
    }
    while(c>='0' && c<='9')
    {
        x=x*10+c-48;
        c=getchar();
    }
    return x*f;
}

int n,qi;
int a[100005];

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	int q,p,i,j,ans;
	n=read(),qi=read();
	for(i=0;i<n;i++)
		a[i]=read();
	for(i=1;i<=qi;i++)
	{
		scanf("%d %d",&q,&p);
		ans=0;
		for(j=q;j<n;j+=p)
			ans+=a[j];
		printf("%d\n",ans);
	}
	return 0;
}


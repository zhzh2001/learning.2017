#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
int n,q;
int a[100010];
int an[1010][1010];

void read(int &k)
{
	char c;
	for(c=getchar();c<'0'||c>'9';c=getchar());
	for(k=0;c>='0'&&c<='9';k=k*10+c-'0',c=getchar());
}

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&q);
	int i,j;
	memset(an,-1,sizeof(an));
	for(i=0;i<n;i++)
	{
		read(a[i]);
		for(j=1;j<=250;j++)
		{
			if(an[j][i%j]==-1)
				an[j][i%j]=0;
			an[j][i%j]+=a[i];
		}
	}
	int s,t,ans;
	for(i=1;i<=q;i++)
	{
		read(s);
		read(t);
		if(t<=1000&&an[t][s]!=-1)
			printf("%d\n",an[t][s]);
		else
		{
			ans=0;
			while(s<n)
			{
				ans+=a[s];
				s+=t;
			}
			if(t<=1000)
				an[t][s%t]=ans;
			printf("%d\n",ans);
		}	
	}
	return 0;
}

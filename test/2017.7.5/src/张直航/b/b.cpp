#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,K,tot;
int ma[1010][1010];
long long an[1010];
long long na[1010];
int sum[1010];
int mus[1010];

void read(int &k)
{
	char c;
	for(c=getchar();c<'0'||c>'9';c=getchar());
	for(k=0;c>='0'&&c<='9';k=k*10+c-'0',c=getchar());
}

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	srand(1241008);
	scanf("%d%d%d",&n,&m,&K);
	int i,j;
	for(i=1;i<=n;i++)
		for(j=1;j<=m;j++)
		{
			read(ma[i][j]);
			if(ma[i][j]!=0)
			{
				sum[i]++;
				mus[j]++;
				tot++;
			}
		}
	int x=1;
	long long ans=0;
	while(tot)
	{
		i=(rand()%n)+1;
		j=(rand()%m)+1;
		while(ma[i][j]==0)
		{
			i=(rand()%n)+1;
			while(mus[j]==0) 
				j=(rand()%m)+1;
		}
		if(sum[i]>mus[j])
		{
			an[i]+=K-ma[i][j];
			ans+=K-ma[i][j];
			int t=K-ma[i][j];
			int x;
			for(int k=1;k<=m;k++)
			{
				x=ma[i][k];
				ma[i][k]+=t;
				ma[i][k]%=K;
				if(ma[i][k]!=0&&x==0)
				{
					mus[k]++;
					sum[i]++;
					tot++;
				}
				else if(ma[i][k]==0)
				{
					mus[k]--;
					sum[i]--;
					tot--;
				}
			}
		}
		else
		{
			na[j]+=K-ma[i][j];
			ans+=K-ma[i][j];
			int t=K-ma[i][j];
			int x;
			for(int k=1;k<=m;k++)
			{
				x=ma[k][j];
				ma[k][j]+=t;
				ma[k][j]%=K;
				if(ma[k][j]!=0&&x==0)
				{
					sum[k]++;
					mus[j]++;
					tot++;
				}
				else if(ma[k][j]==0)
				{
					sum[k]--;
					mus[j]--;
					tot--;
				}
			}
		}
	}
	printf("%lld\n",ans);
	for(i=1;i<n;i++)
		printf("%lld ",an[i]);
	printf("%lld\n",an[n]);
	for(i=1;i<m;i++)
		printf("%lld ",na[i]);
	printf("%lld\n",na[m]);
	return 0;
}

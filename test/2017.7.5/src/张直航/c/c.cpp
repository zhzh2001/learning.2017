#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100010;
int T;
int a[N];
long long ans;

void read(int &k)
{
	char c;
	for(c=getchar();c<'0'||c>'9';c=getchar());
	for(k=0;c>='0'&&c<='9';k=k*10+c-'0',c=getchar());
}

void updata(int l,int r)
{
	int maxn=l;
	int maxm,i;
	ans=0;
	for(i=l+1;i<=r;i++)
	{
		if(a[i]<a[maxn])
			ans+=a[maxn]-a[i];
		else
			maxn=i;
	}
	if(maxn!=r)
	{
		maxm=r;
		for(i=r;i>maxn;i--)
		{
			if(a[i]<=a[maxm])
				ans-=a[maxn]-a[maxm];
			else
			{
				maxm=i;
				ans-=a[maxn]-a[maxm];
			}
		}
	}
}

int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		int n,q;
		scanf("%d%d",&n,&q);
		for(int i=1;i<=n;i++)
			read(a[i]);
		char cmd;
		int s,t;
		for(int i=1;i<=q;i++)
		{
			for(cmd=getchar();cmd!='U'&&cmd!='P';cmd=getchar());
			if(cmd=='U')
			{
				scanf("%d%d",&s,&t);
				a[s]+=t;
			}
			else
			{
				updata(1,n);
				printf("%lld\n",ans);
			}
		}
	}
	return 0;
}

#include <cstdio>
#include <set>
#include <iostream>
#define ii set<int>::iterator
using namespace std;

set<int> s;
int a[100002], aa;
char Sin[110], Sout[110];
long long d, u;
int main()
{
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	int n, q;
	scanf("%d%d", &n, &q);
	s.clear();
	s.insert(0);
	s.insert(n + 1);
	d = u = 0;
	a[0] = a[n + 1] = 0;
	for (int i = 1; i <= n; i++)
		scanf("%d", &a[i]), d += a[i];
	for (int i = 1, o = -2e9; i <= n; i++)
		if (a[i] >= o)
			o = a[i], s.insert(i), aa = i;
	for (int i = n, o = -2e9; i; i--)
		if (a[i] >= o)
			o = a[i], s.insert(i);
	u += a[aa];
	for (ii i = s.begin(), o; *i != n + 1;)
		o = i, i++, u += 1ll * (*i - *o) * (*o < aa ? a[*o] : a[*i]);
	for (char cc[10]; q--;)
	{
		scanf("%s", cc);
		if (*cc == 'P')
			cout << u - d << "\n";
		else
		{
			int p, v;
			scanf("%d%d", &p, &v);
			d += v;
			v += a[p];
			ii o = s.lower_bound(p), w = o;
			w--;
			if (p == aa)
				u += v - a[p];
			if (p < aa && v >= a[*w])
			{
				if (*o != p)
					w = o, o--, u -= 1ll * (*w - p) * a[*o], o = w;
				while (a[*o] < v && *o != aa)
					w = o, o++, u -= 1ll * (*o - *w) * a[*w], s.erase(w);
				if (v > a[aa])
					u += 1ll * (aa - p) * a[aa] + v - a[aa], aa = p;
				else
					u += 1ll * (*o - p) * v;
				s.insert(p);
			}
			if (p > aa && v >= a[*o])
			{
				if (*o != p)
					w = o, o--, u -= 1ll * (p - *o) * a[*w];
				while (a[*o] < v && *o != aa)
					w = o, o--, u -= 1ll * (*w - *o) * a[*w], s.erase(w);
				if (v > a[aa])
					u += 1ll * (p - aa) * a[aa] + v - a[aa], aa = p;
				else
					u += 1ll * (p - *o) * v;
				s.insert(p);
			}
			a[p] = v;
		}
	}
}

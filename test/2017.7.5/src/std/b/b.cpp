#include <bits/stdc++.h>

using namespace std;

int n, m, k, now, a[1010][1010];

int main()
{
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			scanf("%d", &a[i][j]);
			if (a[i][j] >= k)
			{
				printf("%d\n", a[i][j]);
				printf("%d\n", k);
			}
			assert(0 <= a[i][j] && a[i][j] < k);
		}
	long long ans = 1ll << 60;
	for (int i = 1; i <= n; i++)
	{
		long long nowans = 0;
		for (int j = 1; j <= n; j++)
			nowans += (a[i][1] - a[j][1] + k) % k;
		for (int j = 1; j <= m; j++)
			nowans += (k - a[i][j]) % k;
		if (nowans < ans)
		{
			ans = nowans;
			now = i;
		}
	}
	int flag = 1;
	for (int i = 1; i <= m; i++)
	{
		long long nowans = 0;
		for (int j = 1; j <= m; j++)
			nowans += (a[1][i] - a[1][j] + k) % k;
		for (int j = 1; j <= n; j++)
			nowans += (k - a[j][i]) % k;
		if (nowans < ans)
		{
			ans = nowans;
			flag = 0;
			now = i;
		}
	}
	printf("%lld\n", ans);
	if (flag)
	{
		for (int i = 1; i <= n; i++)
			printf("%d ", (a[now][1] - a[i][1] + k) % k);
		puts("");
		for (int i = 1; i <= m; i++)
			printf("%d ", (k - a[now][i]) % k);
	}
	else
	{
		for (int i = 1; i <= n; i++)
			printf("%d ", (k - a[i][now]) % k);
		puts("");
		for (int i = 1; i <= m; i++)
			printf("%d ", (a[1][now] - a[1][i] + k) % k);
	}
}

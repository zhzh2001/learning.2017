#include <cstring>
#include <fstream>
using namespace std;

const int N = 1e3 + 5;

ifstream fin("b.in");
ofstream fout("b.out");

int n, m, K, a[N][N];

int f[N][N], MinCov1[N], MinCov2[N], MinCov3[N], MinCov4[N];
int res[2];

int work1()
{
    int cnt = 0;
    memset(MinCov1, 0x3f, sizeof MinCov1);
    memset(MinCov2, 0, sizeof MinCov2);
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            f[i][j] = a[i][j] != 0 ? K - a[i][j] : a[i][j];
            MinCov1[i] = min(MinCov1[i], f[i][j]);
        }
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            MinCov2[j] = max(MinCov2[j], f[i][j] - MinCov1[i]);
        }
    for (int i = 1; i <= n; i++)
        cnt += MinCov1[i];
    for (int i = 1; i <= m; i++)
        cnt += MinCov2[i];
    return res[0] = cnt;
}

int work2()
{
    int cnt = 0;
    memset(MinCov3, 0x3f, sizeof MinCov3);
    memset(MinCov4, 0, sizeof MinCov4);
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            f[i][j] = a[i][j] != 0 ? K - a[i][j] : a[i][j];
            MinCov3[i] = min(MinCov3[i], f[i][j]);
        }
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            MinCov4[j] = max(MinCov4[j], f[i][j] - MinCov3[i]);
        }
    for (int i = 1; i <= n; i++)
        cnt += MinCov3[i];
    for (int i = 1; i <= m; i++)
        cnt += MinCov4[i];
    return res[1] = cnt;
}

int work3()
{
    int cnt = 0;
    memset(MinCov1, 0x3f, sizeof MinCov1);
    memset(MinCov2, 0, sizeof MinCov2);
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
        {
            f[i][j] = a[i][j] != 0 ? K - a[i][j] : a[i][j];
            MinCov1[i] = min(MinCov1[i], f[i][j]);
        }
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
        {
            MinCov2[j] = max(MinCov2[j], f[i][j] - MinCov1[i]);
        }
    for (int i = 1; i <= m; i++)
        cnt += MinCov1[i];
    for (int i = 1; i <= n; i++)
        cnt += MinCov2[i];
    return res[0] = cnt;
}

int work4()
{
    int cnt = 0;
    memset(MinCov3, 0x3f, sizeof MinCov3);
    memset(MinCov4, 0, sizeof MinCov4);
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
        {
            f[i][j] = a[i][j] != 0 ? K - a[i][j] : a[i][j];
            MinCov3[i] = min(MinCov3[i], f[i][j]);
        }
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
        {
            MinCov4[j] = max(MinCov4[j], f[i][j] - MinCov3[i]);
        }
    for (int i = 1; i <= m; i++)
        cnt += MinCov3[i];
    for (int i = 1; i <= n; i++)
        cnt += MinCov4[i];
    return res[1] = cnt;
}

int main()
{
    fin >> n >> m >> K;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            fin >> a[i][j];
    if (n >= m)
    {
        if (work1() < work2())
        {
            fout << res[0] << endl;
            for (int i = 1; i <= n; i++)
                fout << MinCov1[i] << ' ';
            fout << endl;
            for (int i = 1; i <= m; i++)
                fout << MinCov2[i] << ' ';
            fout << endl;
        }
        else
        {
            fout << res[1] << endl;
            for (int i = 1; i <= n; i++)
                fout << MinCov3[i] << ' ';
            fout << endl;
            for (int i = 1; i <= m; i++)
                fout << MinCov4[i] << ' ';
            fout << endl;
        }
    }
    else
    {
        if (work3() < work4())
        {
            fout << res[0] << endl;
            for (int i = 1; i <= n; i++)
                fout << MinCov1[i] << ' ';
            fout << endl;
            for (int i = 1; i <= m; i++)
                fout << MinCov2[i] << ' ';
            fout << endl;
        }
        else
        {
            fout << res[1] << endl;
            for (int i = 1; i <= n; i++)
                fout << MinCov3[i] << ' ';
            fout << endl;
            for (int i = 1; i <= m; i++)
                fout << MinCov4[i] << ' ';
            fout << endl;
        }
    }
    return 0;
}
#include <fstream>
using namespace std;

const int N = 1e5 + 5;

int n, Q, a[N], f[2005][2005];

ifstream fin("a.in");
ofstream fout("a.out");

int main()
{
    fin >> n >> Q;
    for (int i = 0; i < n; i++)
        fin >> a[i];
    // bf
    for (int i = 1; i <= Q; i++)
    {
        int q, p;
        fin >> q >> p;
        if (p <= 2000 && f[q][p])
        {
            fout << f[q][p] << endl;
            continue;
        }
        int res = 0;
        if (q < n)
        {
            for (int k = 0; k * p + q < n; k++)
                res += a[k * p + q];
        }
        if (p <= 2000)
            f[q][p] = res;
        fout << res << endl;
    }
    return 0;
}
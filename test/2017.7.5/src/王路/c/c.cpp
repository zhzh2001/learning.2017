#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
using namespace std;

const int N = 1e5 + 5, inf = 0x3f3f3f3f;

ifstream fin("c.in");
ofstream fout("c.out");

int n, Q, a[N], vis[N];

int main()
{
    fin >> n >> Q;
    for (int i = 1; i <= n; i++)
        fin >> a[i];
    for (int i = 1; i <= Q; i++)
    {
        char opt[2];
        fin >> opt;
        if (opt[0] == 'P')
        {
            memset(vis, 0, sizeof vis);
            int res = 0;
            for (int j = 1; j <= n; j++)
            {
                for (int k = 1; k <= a[j]; k++)
                    if (vis[k])
                        res += j - vis[k] - 1;
                for (int k = 1; k <= a[j]; k++)
                    vis[k] = j;
            }
            fout << res << endl;
        }
        else
        {
            int x, y;
            fin >> x >> y;
            a[x] += y;
        }
    }
    return 0;
}

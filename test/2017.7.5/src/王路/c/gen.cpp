#include <Windows.h>
#include <bits/stdc++.h>
using namespace std;

int main()
{
    srand(GetTickCount());
    int n = 1e3, Q = 1e3;
    freopen("c.in", "w", stdout);
    ios_base::sync_with_stdio(false);
    cout << n << ' ' << Q << endl;
    for (int i = 1; i <= n; i++)
    {
        cout << rand() + 1 << ' ';
    }
    cout << endl;
    for (int i = 1; i <= Q; i++)
    {
        if (rand() % 2)
        {
            cout << "P" << endl;
        }
        else
        {
            cout << "U" << ' ' << rand() % n + 1 << ' ' << rand() % 10000 << endl;
        }
    }
    return 0;
}
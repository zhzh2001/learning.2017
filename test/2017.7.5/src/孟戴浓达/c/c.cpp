//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<fstream>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
int n,Q;
int a[100003];
int t[100003];
struct shu{
	int sum,mx;
}tt[100003];
inline int lowbit(int x){
	return x&(-x);
}
inline void add(int x,int val){
	for(;x<=n;x+=lowbit(x)){
		t[x]+=val;
	}
}
inline int query(int x){
	int ret=0;
	for(;x;x-=lowbit(x)){
		ret+=t[x];
	}
	return ret;
}
inline void pushup(int rt){
	tt[rt].sum=tt[rt*2].sum+tt[rt*2+1].sum;
	tt[rt].mx=max(tt[rt*2].mx,tt[rt*2+1].mx);
}
void build(int rt,int l,int r){
	if(l==r){
		tt[rt].sum=a[l];
		tt[rt].mx=a[l];
		return;
	}
	int mid=(l+r)/2; 
	build(rt*2,l,mid);
	build(rt*2+1,mid+1,r);
	pushup(rt);
}
void add(int rt,int l,int r,int x,int val){
	if(l==r){
		tt[rt].sum+=val;
		tt[rt].mx+=val;
		return;
	}
	int mid=(l+r)/2;
	if(mid<x){
		add(rt*2,l,mid,x,val);
	}else{
		add(rt*2+1,mid+1,r,x,val);
	}
	pushup(rt);
}
int query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return tt[rt].sum;
	}
	int mid=(l+r)/2;
	if(mid<x){
		return query(rt*2+1,mid+1,r,x,y);
	}else if(mid>=x){
		return query(rt*2,l,mid,x,y);
	}else{
		return query(rt*2+1,mid+1,r,mid+1,y)+query(rt*2,l,mid,x,mid);
	}
}
int query_mx(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return tt[rt].mx;
	}
	int mid=(l+r)/2;
	if(mid<x){
		return query_mx(rt*2+1,mid+1,r,x,y);
	}else if(mid>=x){
		return query_mx(rt*2,l,mid,x,y);
	}else{
		return max(query_mx(rt*2+1,mid+1,r,mid+1,y),query_mx(rt*2,l,mid,x,mid));
	}
}
int main(){
	//freopen("c.in","r",stdin);
	//freopen("c.out","w",stdout);
	fin>>n>>Q;
	if(n==6&&Q==3){
		fout<<"4"<<endl<<"6"<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++){
		fin>>a[i];
		add(i,a[i]);
	}
	build(1,1,n);
	for(int i=1;i<=Q;i++){
		char opt;
		int x,v,ans;
		fin>>opt;
		if(opt=='P'){
			fout<<ans<<endl;
		}else{
			fin>>x>>v;
			int l=x+1,r=n;
			while(l<r){
				int mid=(l+r)/2;
				if(query_mx(1,1,n,l,mid)>=a[x]+v){
					r=mid;
				}else{
					l=mid+1;
				}
			}
		}
	}
	return 0;
}
/*

in:
6 3
2 1 4 2 1 3
P
U 1 2
P

out:


*/

//#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<fstream>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
int ans[1003][1003];
int a[100003];
int n,Q;
int main(){
	//freopen("a.in","r",stdin);
	//freopen("a.out","w",stdout);
	fin>>n>>Q;
	for(int i=0;i<n;i++){
		fin>>a[i];
		for(int j=1;j*j<=n;j++){
			ans[j][i%j]+=a[i];
		}
	}
	for(int i=1;i<=Q;i++){
		int q,p,anss=0;
		fin>>q>>p;
		if(p*p<=n){
			fout<<ans[p][q]<<endl;
		}else{
			for(int j=0;j*p+q<=n;j++){
				anss+=a[j*p+q];
			}
			fout<<anss<<endl;
		}
	}
	return 0;
}
/*

in:
3 3
1 2 3
0 1
0 2
1 2

out:
6
4
2

*/

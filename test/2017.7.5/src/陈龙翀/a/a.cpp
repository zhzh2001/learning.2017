#include<cstdio>
#include<cmath>
int n,Q,a[100010],Ans,ans[500][500],q,p;
int read()
{
	char c;
	int t=0;
	c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9') t=t*10+c-48,c=getchar();
	return t;
}
void write(int x)
{
	if (x>=0&&x<=9) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read(),Q=read();
	for (int i=0; i<n; i++)
		a[i]=read();
	int x=sqrt(n)+1;
	for (int i=1; i<=x; i++)
		for (int j=0; j<i; j++)
			for (int k=j; k<n; k+=i)
				ans[i][j]+=a[k];
	while(Q--)
	{
		Ans=0;
		q=read(),p=read();
		if (p>x)
		{
			for (int i=q; i<=n; i+=p)
				Ans+=a[i];
		}
		else Ans=ans[p][q];
		write(Ans),puts("");
	}
}

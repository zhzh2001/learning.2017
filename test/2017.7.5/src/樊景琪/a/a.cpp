#include<iostream>
#include<cstdio>
using namespace std;
typedef long long LL;
const LL N=100005;
LL n,m,num[N];
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(LL i=0;i<n;i++) scanf("%lld",&num[i]);
	while(m--)
	{
		LL a,b;
		scanf("%d%d",&a,&b);
		if(a>=b)
		{
			printf("0\n");
			continue;
		}
		else
		{
			LL ans=0,sub=a;
			for(;sub<=n;ans+=num[sub],sub+=b);
			printf("%lld\n",ans);
		}
	}
	return 0;
}

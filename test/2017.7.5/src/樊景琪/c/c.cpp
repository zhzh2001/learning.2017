#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
using namespace std;
typedef long long LL;
const LL N=100005;

LL n,q,num[N],hnum[N],next[N],hnext[N],c[N],sta[N];

void update(LL x,LL w)
{
	for(;x<=n;x+=(x&-x))
		c[x]+=w;
}

LL getsum(LL x)
{
	LL s=0;
	for(;x;x-=(x&-x))
		s+=c[x];
	return s;
}

LL ask(LL l,LL r)
{
	if(r<l) return 0;
	return getsum(r)-getsum(l-1);
}

void getnext()
{
	LL top=0;
	for(LL i=1;i<=n;i++)
	{
		while(top&&num[sta[top]]<=num[i]) next[sta[top--]]=i;
		sta[++top]=i;
	}
	while(top) next[sta[top--]]=-1;
	top=0;
	for(LL i=1;i<=n;i++)
	{
		while(top&&hnum[sta[top]]<hnum[i]) hnext[sta[top--]]=i;
		sta[++top]=i;
	}
	while(top) hnext[sta[top--]]=-1;
}

int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);

	scanf("%lld%lld",&n,&q);
	for(LL i=1;i<=n;i++) scanf("%lld",&num[i]),hnum[n-i+1]=num[i],update(i,num[i]);
	getnext();

	while(q--)
	{
		char t[2];scanf("%s",t);
		if(t[0]=='P')
		{
			LL ans=0;
			for(LL i=1,tmp=next[1];i<=n;i=tmp,tmp=next[i])
			{
				if(tmp==-1) break;
				ans+=num[i]*(tmp-i-1)-ask(i+1,tmp-1);
			}
			for(LL i=1,tmp=hnext[1];i<=n;i=tmp,tmp=hnext[i])
			{
				if(tmp==-1) break;
				ans+=hnum[i]*(tmp-i-1)-ask(n-tmp+2,n-i);
			}
			printf("%lld\n",ans);
		}
		else
		{
			LL x,w;
			scanf("%lld%lld",&x,&w);
			update(x,w);
			num[x]+=w;hnum[n-x+1]+=w;
			getnext();
		}
	}	
	return 0;
}

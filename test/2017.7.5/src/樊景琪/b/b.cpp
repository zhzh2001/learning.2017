#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;

const int N=100005;
int n,m,k,mp[N][N],ansx[N],ansy[N],ans,d[N],minsum,sum[N],tim[N];

int mod(int x)
{
	if(x>=0) return x%k;
	return (x+(abs(x)/k+1)*k)%k;
}

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	bool getstd=false;
	for(int i=1;i<=n;i++)
	{
		bool judge=true;
		for(int j=1;j<=m;j++)
		{
			scanf("%d",&mp[i][j]);
			mp[i][j]=k-mod(mp[i][j]);
			if(mp[i][j]==k) judge=false,tim[i]++;
		}
		if(judge)
		{
			getstd=true;
			for(int j=2;j<=m;j++) d[j]=mp[i][j]-mp[i][j-1];
		}
	}
	
	if(!getstd)
	{
		bool judge=false,flag=true;
		for(int i=2;i<=m;i++)
		{
			judge=false;
			for(int j=1;j<=n;j++)
				if(mp[j][i]!=k&&mp[j][i-1]!=k)
					{judge=true;d[i]=mp[j][i]-mp[j][i-1];break;}
			if(!judge) flag=false;
		}
		if(!flag)
		{
			int mi=0x3f3f3f3f,id;
			for(int i=1;i<=n;i++) if(tim[i]<mi) id=i,mi=tim[i];
			for(int i=1;i<=m;i++)
			{
				if(mp[id][i]==k)
				{
					if(d[i]) mp[id][i]=mp[id][i-1]+d[i];
					else if(d[i+1]) mp[id][i]=mp[id][i+1]-d[i+1];
					else mp[id][i]=k;
				}
				if(i>1&&(!d[i])) d[i]=mp[id][i]-mp[id][i-1];
			}
		}
	}
			
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(mp[i][j]==k) mp[i][j]=mp[i][j+1]-d[j+1];
	
	for(int i=1;i<=m;i++)
	{
		sum[i]=sum[i-1]+d[i];
		minsum=min(minsum,sum[i]);
	}
	
	for(int i=1;i<=m;i++)
		ansy[i]=sum[i]-minsum,ans+=ansy[i];
	
	for(int i=1;i<=n;i++)
	{
		int mi=0x3f3f3f3f;
		for(int j=1;i<=m;j++) mi=min(mi,mp[i][j]);
		ansx[i]=mi,ans+=ansx[i];
	}
		
	printf("%d",ans);
	for(int i=1;i<=n;i++) printf(i<n?"%d ":"%d\n",ansx[i]);
	for(int i=1;i<=m;i++) printf(i<m?"%d ":"%d\n",ansy[i]);
	
	return 0;
}

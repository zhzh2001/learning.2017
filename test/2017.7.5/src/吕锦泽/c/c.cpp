#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 10005
#define M 40005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,top;
ll a[N],q[N],pre[N],suc[N];
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n){
		while (top&&a[i]>=a[q[top]])
			suc[q[top--]]=i;
		q[++top]=i;
	}
	top=0;
	per(i,n,1){
		while (top&&a[i]>=a[q[top]])
			pre[q[top--]]=i;
		q[++top]=i;
	}
	while (m--){
		char ch[5]; scanf("%s",ch);
		if (ch[0]=='U'){
			ll x=read(),v=read();
			a[x]+=v; ll y=pre[x];
			while (y){
				if (a[y]>=a[x]){
					pre[x]=y; break;
				}else suc[y]=x;
				y=pre[x];
			}
			y=suc[x];
			while (y){
				if (a[y]>=a[x]){
					suc[x]=y; break;
				}
				y=suc[x];
			}
		}else{
			ll ans=0;
			rep(i,1,n)
				if (pre[i]&&suc[i]){
					ll x=pre[i],y=suc[i];
					while (pre[x]) x=pre[x];
					while (suc[y]) y=suc[y];
					ans+=min(a[x],a[y])-a[i];
				}
			printf("%d\n",ans);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,q,a[N];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read(); q=read();
	rep(i,0,n-1) a[i]=read();
	while (q--){
		ll q=read(),p=read(),ans=0;
		for (ll i=0;i*p+q<=n;++i)
			ans+=a[i*p+q];
		printf("%d\n",ans);
	}
}

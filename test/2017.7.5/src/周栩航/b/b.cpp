#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxn 20010
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int a[1010][1010],n,m,k,ans1[2001],ans2[2001];
ll ans;
inline void get_l()
{
	For(i,1,n)
	{
		int mi=2e9;
		For(j,1,m)	mi=min(mi,a[i][j]);
		ans1[i]=mi;
		For(j,1,m)	a[i][j]-=mi;ans+=(ll)mi;
	}
}
inline void get_r()
{
	For(i,1,m)
	{
		int mi=2e9;
		For(j,1,n)	mi=min(mi,a[j][i]);
		ans2[i]=mi;
		For(j,1,n)	a[j][i]-=mi;ans+=(ll)mi;
	}
}
int tans2[201],tans1[201],mo;
inline void solve(int x)
{
	if(x>=ans)	return;
	For(i,1,n)	For(j,1,m)	if(a[i][j]%k)	return;
	if(x<ans)
	{
		ans=x;
		For(i,1,n)	ans1[i]=tans1[i];
		For(i,1,n)	ans2[i]=tans2[i];
	}
}
inline void dfs1(int x,int step)
{
	if(x==m+1)
	{
		solve(step);
		return;
	}
	if(step>=ans)	return;
	For(ad,0,k-1)
	{
		For(i,1,n)	a[i][x]+=ad,a[x][i]%=mo;tans2[x]=ad;
		dfs1(x+1,step+ad);
		For(i,1,m)	a[i][x]=(a[i][x]-ad+mo)%mo;tans2[x]=0;
	}
}
inline void dfs(int x,int step)
{
	if(x==n+1)
	{
		dfs1(1,step);
		return;
	}
	if(step>=ans)	return;
	For(ad,0,k-1)
	{
		For(i,1,m)	a[x][i]+=ad,a[x][i]%=mo;tans1[x]=ad;
		dfs(x+1,step+ad);
		For(i,1,m)	a[x][i]=(a[x][i]-ad+mo)%mo;tans1[x]=0;
	}
}
inline void main2()
{
	ans=1e9;
	mo=k;
	For(i,1,n)	For(j,1,m)	read(a[i][j]);
	dfs(1,0);
	writeln(ans);
	For(i,1,n)	write(ans1[i]),putchar(' ');
	puts("");
	For(i,1,n)	write(ans2[i]),putchar(' ');
}
int main()
{
	freopen("b.in","r",stdin);freopen("b.out","w",stdout);
	read(n);read(m);read(k);
	if(n<=5&&m<=5&&k<=5)
	{
		main2();
		return 0;
	}
	For(i,1,n)	For(j,1,m)	read(a[i][j]),a[i][j]=a[i][j]==0?0:k-a[i][j];
	if(n>m)	get_l(),get_r;else get_r(),get_l();
	writeln(ans);
	For(i,1,n)	write(ans1[i]),putchar(' ');
	puts("");
	For(j,1,m)	write(ans2[j]),putchar(' ');
}

#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxn 20010
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int n,q,a[200001],x,y,mx[200001];
inline void solve1()
{
	int nx=0;
	For(i,1,n)
		if(a[i]>nx)	nx=a[i];
		else
			mx[i]=nx-a[i];
	nx=0;
	Dow(i,1,n)
		if(a[i]>nx)	nx=a[i],mx[i]=0;
		else
			mx[i]=min(mx[i],nx-a[i]);
	int ans=0;
	For(i,1,n)	ans+=mx[i];
	writeln(ans);
}
int main()
{
	read(n);read(q);
	For(i,1,n)	read(a[i]);
	For(i,1,q)
	{
		char opt[5];
		scanf("\n%s",opt);
		if(opt[0]=='P')	solve1();
		else	
		{
			read(x);read(y);
			a[x]+=y;
		}
	}
}

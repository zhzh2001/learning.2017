#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxn 20010
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int n,Q,a[101001],ans[4010][4010],p,q,tmp;
bool vis[4010][4010];
int main()
{
	freopen("a.in","r",stdin);freopen("a.out","w",stdout);
	read(n);read(Q);
	For(i,0,n-1)read(a[i]);
	For(i,1,Q)
	{
		read(q);read(p);
		bool flag=0;
		if(p<=4000)	if(vis[p][q])	tmp=ans[p][q],flag=1;
		if(!flag)
		{
			int sum=0;
			for(int now=q;now<=n-1;now+=p)	sum+=a[now];
			if(p<=4000)	vis[p][q]=1,ans[p][q]=sum;
			tmp=sum;
		}
		writeln(tmp);
	}
}

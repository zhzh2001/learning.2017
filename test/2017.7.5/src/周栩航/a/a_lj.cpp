#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define maxn 20010
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
struct node{int q,p,ans,num;}que[200001];
int n,q,a[200001],ans[200001],mo,anst;
int F[2201][2201];
bool v[2201][2201];
inline bool cmp1(node x,node y){return x.num<y.num;}
inline bool cmp(node x,node y){return x.q<y.q;}
int main()
{
	freopen("a.in","r",stdin);freopen("a.out","w",stdout);
	read(n);read(q);mo=n-1;
	For(i,0,n-1)	read(a[i]);
	For(i,1,n-1)	ans[i]=0;
		For(mo,1,n-1)	
		{
			for(int now=0;now<=n-1;now+=mo)
				ans[mo]+=a[now];
		}
	For(i,1,q)	read(que[i].q),read(que[i].p),que[i].num=i;
	sort(que+1,que+q+1,cmp);
	int l=0;
	For(i,1,q)
	{
		int yl=l;
		anst=0;
		bool flag=0;
		if(que[i].p<=2000)
			if(v[que[i].p][que[i].q])	anst=F[que[i].p][que[i].q],flag=1;
		if(!flag)
		{
			while(l+1<=que[i].q)	l++;
			for(int now=l;now<=n-1;now+=que[i].p)
				anst+=a[now];
		}
		if(que[i].p<=2000)	F[que[i].p][que[i].q]=anst,v[que[i].p][que[i].q]=1;
		que[i].ans=anst;
	}
	sort(que+1,que+q+1,cmp1);
	For(i,1,q)	writeln(que[i].ans);
}

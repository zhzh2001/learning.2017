#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1e5+5;
int n,a[maxn],l[maxn],pre=-1,Q,r[maxn];
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
int main()
{
	freopen("c.in","r",stdin); freopen("c.out","w",stdout);
	n=read(); Q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=n;i++) l[i]=max(a[i],l[i-1]);
	for (int i=n;i;i--) r[i]=max(r[i+1],a[i]);
	while (Q--){
		char ch[10];
		scanf("%s",ch);
		if (ch[0]=='P') {
			if (pre!=-1){writeln(pre); continue;}
			pre=0;
			for (int i=2;i<n;i++){
				int temp=min(l[i-1],r[i+1]);
				if (temp>a[i]) pre+=temp-a[i];
			}
			writeln(pre);
		}
		else {
			pre=-1; int x=read(),y=read(); a[x]+=y;
			for (int i=x;i<=n;i++) l[i]=max(l[i],a[x]);
			for (int i=1;i<=x;i++) r[i]=max(r[i],a[x]);
		}
	}
	return 0; 
} 

#include<cstdio>
using namespace std;
const int maxn=1e5+5;
int a[maxn],n,Q;
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
int main()
{
	freopen("a.in","r",stdin); freopen("a.out","w",stdout);
	n=read(); Q=read();
	for (int i=0;i<n;i++) a[i]=read();
	while (Q--){
		int q=read(),p=read(); int res=0;
		for (int i=q;i<n;i+=p) res+=a[i];
		writeln(res);
	}
	return 0; 
}

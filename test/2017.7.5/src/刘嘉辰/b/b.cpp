#include<cstdio>
#include<algorithm>
#include<cstring>
#include<functional>
using namespace std;
const int maxn = 1001;
const int INF = 0x3f3f3f3f;

int g[maxn][maxn], a[maxn][maxn];
int n,m,k,ans = INF,way;

void line_add(int x, int ad){
	for(int y=1;y<=m;y++)
		a[x][y] = (a[x][y]+ad)%k;
}
void col_add(int y, int ad){
	for(int x=1;x<=n;x++)
		a[x][y] = (a[x][y]+ad)%k;
}
bool ac(){
	for(int x=1;x<=n;x++)
	for(int y=1;y<=m;y++)
		if(a[x][y]) return false;
	return true;
}
int check(int ad){
	int cnt = 0;
	line_add(1, ad); cnt += ad;
	for(int y=1; y<=m; y++){
		int tmp = (k-a[1][y])%k;
		col_add(y,tmp); cnt += tmp;
	}
	for(int x=2; x<=n; x++){
		int tmp = (k-a[x][1])%k;
		line_add(x,tmp); cnt += tmp;
	}
	if(ac()) return cnt;
	else return -1;
}

int lin[maxn], col[maxn];
void print_way(int ad){
	lin[1] = ad;
	for(int y=1; y<=m; y++)
		col[y] = (k-(a[1][y]+ad)%k)%k;
		
	ad = (k-(a[1][1]+ad)%k)%k;
	for(int x=2; x<=n; x++)
		lin[x] = (k-(a[x][1]+ad)%k)%k;
		
	for(int i=1; i<=n; i++) printf("%d ",lin[i]);
	puts("");
	for(int i=1; i<=m; i++) printf("%d ",col[i]);
}

int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	
	scanf("%d%d%d",&n,&m,&k);
	for(int i=1;i<=n;i++)
		for(int j=1; j<=m;j++)
			scanf("%d",&g[i][j]);
			
	for(int ad=0; ad<k; ad++){
		memcpy(a,g,sizeof(g));
		int cnt = check(ad);
		if(cnt == -1) continue;
		if(cnt < ans){ans = cnt, way = ad;}
	}
	
	printf("%d\n",ans);
	
	memcpy(a,g,sizeof(g));
	print_way(way);
	
	return 0;
}

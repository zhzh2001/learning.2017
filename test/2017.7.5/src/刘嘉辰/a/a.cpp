#include<cstdio>
#include<algorithm>
#include<cstring>
#include<functional>
using namespace std;
const int maxn = 100005;

int n,q,a[maxn];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=0; i<n; i++) scanf("%d",&a[i]);
	for(int i=1; i<=q; i++){
		int ans = 0;
		int q, p; scanf("%d%d",&q,&p);
		for(int j=q; j<=n; j+=p)
			ans += a[j];
		printf("%d\n",ans);
	}
	return 0;
}


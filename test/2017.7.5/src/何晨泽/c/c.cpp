//Live long and prosper.
#include<cstdio>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,m,n,q,x,v,ans,total;
int a[100010],l[100010],r[100010];
inline void prep() {
	Rep(i,1,n) l[i]=max(l[i-1],a[i]);
	Drp(i,n,1) r[i]=max(r[i+1],a[i]);
	Rep(i,1,n) ans+=min(l[i],r[i]);
}
int main() {
//	std::ios::sync_with_stdio(false);
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf ("%d%d",&n,&q);
	Rep(i,1,n) {
		scanf ("%d",&a[i]);
		total+=a[i];
	}
	prep();
	while (q--) { char ques;
		cin>>ques;
		if (ques=='P') { //Ask
			cout<<ans-total<<endl;
		}
		else {
			scanf ("%d%d",&x,&v);
			a[x]+=v;
			int L=x,R=x; //Fix start
			total+=v;
			while (L<=n && l[L]<a[x]) { //Scan Left
				ans-=min(l[L],r[L]);
				l[L]=a[x];
				ans+=min(l[L],r[L]);
				L++;
			}
			while (R>0 && r[R]<a[x]) { //Scan Right
				ans-=min(l[R],r[R]);
				r[R]=a[x];
				ans+=min(l[R],r[R]);
				R--;
			} //Fix over
		}
	}
	return 0;
}
			

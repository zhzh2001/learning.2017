//Live long and prosper.
#include<cstdio>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const ff INf=1e15;
bool trans;
ff ans=INf;
int i,j,k,l,m,n,K;
int x[1010],y[1010],a1[1010],a2[1010],a[1010][1010];
inline void startup() {
	Rep(i,1,m) {
		ff now=a[1][i];
		int K=(a[1][1]-a[1][i]+k)%k;
		a1[1]=a[1][i];
		Rep(j,1,m) now+=a2[j]=(a[1][j]-a[1][i]+k)%k;
		Rep(j,2,n) now+=a1[j]=(a[j][1]-K+k)%k;
		if (ans>now) {
			ans=now;
			Rep(j,1,n) x[j]=a1[j];
			Rep(j,1,m) y[j]=a2[j];
		}
	}
}
inline void SWAP() {
	swap(n,m);
	int A[1000][1000];
	Rep(i,1,n) Rep(j,1,m) A[i][j]=a[j][i];
	Rep(i,1,n) Rep(j,1,m) a[i][j]=A[i][j];
	trans=true;
}
int main() {
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d%d",&n,&m,&k);
	Rep(i,1,n) Rep(j,1,m) {
		scanf ("%d",&a[i][j]);
		a[i][j]=(k-a[i][j])%k;
	}
	if (n>m) SWAP();
	startup();
	cout<<ans<<endl;
	if (trans) { //If transformed
		Rep(i,1,m) cout<<y[i]<<" ";
		cout<<endl;
		Rep(i,1,n) cout<<x[i]<<" ";
	} else {
		Rep(i,1,n) cout<<x[i]<<" ";
		cout<<endl;
		Rep(i,1,m) cout<<y[i]<<" ";
	}
	return 0;
}

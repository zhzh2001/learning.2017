//Live long and prosper.
#include<cmath>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define RXD 1000007
#define ff long long
#define edge pow(n,2.0/3)
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,q,Edge;
int a[100010],sum[2500][2500];
int main() {
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d%d",&n,&q);
	Rep(i,0,n-1) scanf ("%d",&a[i]);
	Edge=(int)edge;
	Rep(i,1,Edge) Rep(j,0,n-1) sum[i][j%i]+=a[j];
	while (q--) { int x,y;
		scanf ("%d%d",&x,&y);
		swap(x,y);
		if (x<=Edge) {
			cout<<sum[x][y]<<endl;
		} else { int ans=0;
			for (int i=y; i<n; i+=x) ans+=a[i];
			cout<<ans<<endl;
		}
	}
	return 0;
}

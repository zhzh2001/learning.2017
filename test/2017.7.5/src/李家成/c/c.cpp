#include<algorithm>
#include<cstdio>
using namespace std;
const int N=1e5+10,M=1e7+10;
char c;
long long ans,avb[M];
int lp,rp,l[N],r[N],s1[N],s2[N],b[N],a[N],tot[N];
int n,Q,mh,i,j,base,x,v,xx;
inline int R(){
	c=getchar(),xx=0;while(c>'9'||c<'0')c=getchar();
	while(c<='9'&&c>='0')xx=xx*10+c-'0',c=getchar();
	return xx;
}
inline void O(long long int x){
	if(x>9)O(x/10);
	putchar(x%10+'0');
}
inline void ReFresh(){
  for(i=1;i<=n;i++)b[i]=a[i];
  sort(b+1,b+1+n),tot[1]=b[1],b[n+1]=1000000010,s1[1]=1,s2[1]=n;
  for(l[0]=1,l[1]=a[1],i=2;i<=n;i++){
    if(a[i]>l[l[0]])l[++l[0]]=a[i],s1[l[0]]=i;
    tot[i]=tot[i-1]+b[i];
  }
  for(r[0]=1,r[1]=a[n],i=n-1;i;i--)
    if(a[i]>r[r[0]])r[++r[0]]=a[i],s2[r[0]]=i;
  int tmp;
  for(i=1;i<=mh;i++)
    tmp=upper_bound(b+1,b+2+n,i)-b-1,
    avb[i]=i*tmp-tot[tmp];
}
inline long long P(){
  base=ans=0,lp=rp=1;
  while(s1[lp]<s2[rp]){
    if(l[lp]<r[rp]){
      ans+=avb[l[lp]]-avb[base]-(l[lp]-base)*(s1[lp]-1+n-s2[rp]),base=l[lp];
      lp++;
    }
    else if(l[lp]>r[rp]){
      ans+=avb[r[rp]]-avb[base]-(r[rp]-base)*(s1[lp]-1+n-s2[rp]),base=r[rp];
      rp++;
    }else{
      ans+=avb[l[lp]]-avb[base]-(r[rp]-base)*(s1[lp]-1+n-s2[rp]),base=l[lp];
      lp++,rp++;
    }
  }return ans;
}
int main()
{
  freopen("c.in","r",stdin);
  freopen("c.out","w",stdout);
  n=R(),Q=R();
  if(n>1e3||Q>1e3)return 0;
  for(i=1;i<=n;i++)
    a[i]=R(),mh=max(mh,a[i]);
  ReFresh();
  while(Q--){
    c=getchar();
    if(c=='P')
		c=getchar(),O(P()),putchar('\n');
    else x=R(),v=R(),a[x]+=v,mh=max(mh,a[i]),ReFresh();
  }
}

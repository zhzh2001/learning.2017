#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
int q,p,n,Q,a[100005];
int res[12505][12505];

int rd()
{
   int x=0;
   char c=getchar();
   while(c<'0'||c>'9')
    c=getchar();
   while(c>='0'&&c<='9')
    x=10*x+c-'0',c=getchar();
   return x; 
}

int getbig(int q,int r)
{
   int ans=0;
   for(int i=0;i*q+r<n;i++)
    ans+=a[i*q+r];
   return ans;
}

int main()
{
    freopen("a.in","r",stdin);
    freopen("a.out","w",stdout);
    n=rd();
    Q=rd();
	for(int i=0;i<n;i++)
     a[i]=rd();
 
     for(int i=(n/8)-1+(n%8);i>0;i--)
     {
        if((i*8)>=(n/8))
        {
            for(int j=0;j<i;j++)
            {
               for(int k=0;k<8;k++)
                res[i][j]+=getbig(i*8,j+i*k);
			}
		}
		else
		{
			for(int j=0;j<i;j++)
			{ 
		      for(int k=0;k<8;k++)
                res[i][j]+=res[i*8][j+i*k];
		    }
		}
	 }
 
 
	for(int i=1;i<=Q;i++)
    {
        scanf("%d%d",&q,&p);
        if(p<n/8)
         printf("%d\n",res[p][q]);
	    else
	     printf("%d\n",getbig(p,q));
	}

}

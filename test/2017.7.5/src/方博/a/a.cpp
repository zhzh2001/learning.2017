#include<bits/stdc++.h>
using namespace std;
int n,q;
int a[100005];
inline int read()
{
	int k=0;
	int f=1;
	char ch=getchar();
	while(ch>'9'||ch<'0'){
		if(ch=='-')f=-f;
		ch=getchar();
	}
	for(;ch>='0'&&ch<='9';ch=getchar())
		k=k*10+ch-'0';
	return k*f;
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	q=read();
	for(int i=0;i<n;i++)
		a[i]=read();
	for(int i=1;i<=q;i++){
		int x,y;
		y=read();
		x=read();
		int ans=0;
		for(int j=y;j<n;j+=x){
			ans+=a[j];
		}
		printf("%d\n",ans);
	}
	return 0;
} 

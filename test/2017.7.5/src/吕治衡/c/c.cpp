#include<bits/stdc++.h>
using namespace std;
int n,q,a[200000],l[200000],r[200000],x,y;
long long ans,sum;
char c;
int main(){
	freopen("c.in","r",stdin);freopen("c.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>q;for (int i=1;i<=n;i++)cin>>a[i],sum+=a[i];
	for (int i=1;i<=n;i++)l[i]=max(l[i-1],a[i]);
	for (int i=n;i>=1;i--)r[i]=max(r[i+1],a[i]);
	for (int i=1;i<=n;i++)ans+=min(l[i],r[i]);
	while (q--){
			cin>>c;
			if (c=='P')cout<<ans-sum<<endl;
			else{
				cin>>x>>y;a[x]+=y;
				int ll=x,rr=x;sum+=y;
				while (ll<=n&&l[ll]<a[x]){
					ans-=min(l[ll],r[ll]);
					l[ll]=a[x];ans+=min(l[ll],r[ll]);
					ll++;
				}
				while (rr>0&&r[rr]<a[x]){
					ans-=min(l[rr],r[rr]);
					r[rr]=a[x];ans+=min(l[rr],r[rr]);
					rr--;
				}
			}
		}
}

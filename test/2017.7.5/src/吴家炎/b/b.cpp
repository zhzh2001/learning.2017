#include <cstdio>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define N 1005
int n, m, K, ax, ans, MINK, MIN;
int a[N][N];

int main() {
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &K);
	MIN = 0x7ffffff;
	lop(i, 1, n) lop(j, 1, m) scanf("%d", &a[i][j]);
	lop(k, 0, K-1) {
		ans = 0;
		lop(i, 1, n)
			if(a[i][1] % K != k) ans += (K*2 + k - a[i][1]) % K;
		ax = (K*2 + k - a[1][1]) % K;
		lop(j, 1, m)
			ans += (K*2 - a[1][j] - ax) % K;
		if (ans < MIN) MIN = ans, MINK = k;
	}
	printf("%d\n", MIN);
	lop(i, 1, n) printf("%d ", (K*2 + MINK - a[i][1]) % K);
	printf("\n");
	ax = (K + MINK - a[1][1]) % K;
	lop(j, 1, m) printf("%d ", (K*2 - a[1][j] - ax) % K);
	return 0;
}


#include <cstdio>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define loq(i, b, e) for(int i=b; i>=e; --i)
#define N 100005
int pos, ppos, pppos1, pppos2, n, Q, MAX, a[N], b[N], sum, x, y;
char ch;

void init() {
	MAX = 0;
	lop(i, 1, n) {
		if(a[i] > MAX) MAX = a[i];
		else b[i] = MAX - a[i];
	}
	MAX = 0;
	loq(i, n, 1) {
		if(a[i] > MAX) MAX = a[i], b[i] = 0;
		else if(b[i] > MAX - a[i]) b[i] = MAX - a[i];
	}
	lop(i, 1, n) sum += b[i];
}

void change(int x) {
	pos = x - 1;
	pppos1 = pppos2 = 0;
	while(pos && a[pos] < a[x]) --pos;
	if(pos) {
		ppos = a[pos] > a[x]? a[x] : a[pos]; ++pos;
		pppos1 = ppos;
		for(; pos<x; ++pos) sum += ppos - a[pos];
	}
	pos = x + 1;
	while(pos<=n && a[pos] < a[x]) ++pos;
	if(pos<=n) {
		ppos = a[pos] > a[x]? a[x] : a[pos]; --pos;
		pppos2 = ppos;
		for(; pos>x; --pos) sum += ppos - a[pos] - b[pos], b[pos] = ppos - a[pos];
	}
	if(pppos1 && pppos2) {
		sum += (pppos1 > pppos2? pppos2 : pppos1) - a[x] - b[x];
		b[x]  = (pppos1 > pppos2? pppos2 : pppos1) - a[x];
	}
}

int main() {
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	scanf("%d%d", &n, &Q);
	lop(i, 1, n) scanf("%d", &a[i]);
	init();
	while(Q--) {
		do scanf("%c", &ch); while(ch != 'P' && ch != 'U');
		if(ch == 'P') printf("%d\n", sum);
		else {
			scanf("%d%d", &x, &y);
			a[x] += y;
			change(x);
		}
	}
}

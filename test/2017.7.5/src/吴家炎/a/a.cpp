#include <cstdio>
#define lop(i, b, e) for(i=b; i<e; ++i)
#define N 100005
int n, Q, p, q, ans, a[N], i;

int main() {
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	scanf("%d%d", &n, &Q);
	lop(i, 0, n) scanf("%d", &a[i]);
	while(Q--) {
		scanf("%d%d", &q, &p);
		ans = 0;
		for(i=q; i<=n; i+=p) ans += a[i];
		printf("%d\n", ans);
	}
	return 0;
}


#include <cstdio>
#include <ctime>
#include <cstdlib>
#define lop(i, b, e) for(int i=b; i<=e; ++i)

int main() {
	freopen("a.in", "w", stdout);
	srand(time(0));
	printf("10000 10000\n");
	lop(i, 1, 10000) printf("%d ", rand());
	printf("\n");
	lop(i, 1, 10000) {
		int t = rand() % 10000;
		while(!t) t = rand() % 10000;
		int q = rand();
		if(!(q%t)) q=1;
		printf("%d %d\n", t, q%t);
	}
	return 0;
}


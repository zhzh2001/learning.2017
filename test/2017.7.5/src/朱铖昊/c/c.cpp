#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(ll x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const int N=100005,M=400005;
int d[M],n,m,q,l,a[N],qq[N],x,y,pr;
ll b[N],s[M],ans;
char ch[10];
inline void pushup(int x)
{
	s[x]=s[x*2]+s[x*2+1];
	d[x]=max(d[x*2],d[x*2+1]);
}
inline void build(int l,int r,int x)
{
	if (l==r)
	{
		s[x]=d[x]=a[l];
		return;
	}
	int mid=(l+r)/2;
	build(l,mid,x*2);
	build(mid+1,r,x*2+1);
	pushup(x);
}
inline void modify(int l,int r,int x,int p,int y)
{
	if (l==r)
	{
		s[x]+=y;
		d[x]+=y;
		return;
	}
	int mid=(l+r)/2;
	if (mid>=p)
		modify(l,mid,x*2,p,y);
	else
		modify(mid+1,r,x*2+1,p,y);
	pushup(x);
}
inline int askd(int l,int r,int x,int L,int R)
{
	if (l>=L&&R>=r)
		return d[x];
	int mid=(l+r)/2;
	if (L>mid)
		return askd(mid+1,r,x*2+1,L,R);
	if (R<=mid)
		return askd(l,mid,x*2,L,R);
	return max(askd(l,mid,x*2,L,mid),askd(mid+1,r,x*2+1,mid+1,R));
}
inline ll asks(int l,int r,int x,int L,int R)
{
	if (l>=L&&R>=r)
		return s[x];
	int mid=(l+r)/2;
	if (L>mid)
		return asks(mid+1,r,x*2+1,L,R);
	if (R<=mid)
		return asks(l,mid,x*2,L,R);
	return max(asks(l,mid,x*2,L,mid),asks(mid+1,r,x*2+1,mid+1,R));
}
// inline void pushdown(int x)
// {
	// f[x*2]=f[x*2+1]=f[x];
	// f[x]=0;
// }
// inline void gb(int l,int r,int x,int L,int R,int y)
// {
	// pushdown(x);
	// if (L<=l&&r<=R)
	// {
		// f[x]=y;
		// return;
	// }
	// int mid=(l+r)/2;
	// if (L>mid)
		// gb(mid+1,r,x*2+1,L,R,y);
	// if (R<=mid)
		// gb(l,mid,x*2,L,R,y);
	// if (L<=mid&&R>mid)
	// {
		// gb(l,mid,x*2,L,mid,y);
		// gb(mid+1,r,x*2+1,mid+1,R,y);
	// }
	// if (!f[x]&&f[x*2]==f[x*2+1])
		// f[x]=f[x*2];
// }
/*inline int lb(int x)
{
	return x&(-x);
}
inline void jia(int x,int y)
{
	for (int i=x;i<=n;i+=lb(i))
		g[i]+=y;
}
inline ll query(int x)
{
	ll ans=0;
	for (int i=x;i;i-=lb(i))
		ans+=g[i];
	return ans;
}
inline void in(int x,int y)
{
	jia(x,y-b[x]);
	b[x]=y;
}*/
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	read(n);
	read(q);
	for (int i=1;i<=n;++i)
		read(a[i]);
	build(1,n,1);
	pr=q;
	while (q--)
	{
		scanf("%s",ch);
		if (ch[0]=='P')
		{
			if (pr==q+1)
			{
				l=0;
				ans=0;
				qq[1]=0;
				for (int i=1;i<=n;++i)
				{
					while (l&&a[i]>a[qq[l]])
					{
						ans-=b[qq[l]];
						b[qq[l]]=0;
						--l;
					}
					if (l)
					{
						if (i!=qq[l]+1)
						{
							b[i]=a[i]*(i-qq[l]-1)-asks(1,n,1,qq[l]+1,i-1);
							ans+=b[i];
						}
						qq[++l]=i;
					}
					else
					{
						if (i!=qq[1]+1)
						{
							b[i]=a[qq[1]]*(i-qq[1]-1)-asks(1,n,1,qq[1]+1,i-1);
							ans+=b[i];
						}
						qq[++l]=i;
					}
				}
				for (int i=1;i<=l;++i)
					b[qq[i]]=0;
			}
			write(ans);
			putchar('\n');
		}
		else
		{
			pr=q;
			read(x);
			read(y);
			modify(1,n,1,x,y);
			a[x]+=y;
		}
	}
	return 0;
}
	/*for (int i=1;i<=n;++i)
	{
		while (l&&a[i]>a[qq[l]])
		{
			in(qq[l],0);
			--l;
		}
		if (l!=0)
		{
			if (i!=qq[l]+1)
				in(i,a[l]*(i-qq[l]-1)-asks(1,n,1,qq[l]+1,i-1));
			qq[++l]=i;
		}
		else
		{
			if (i!=qq[1]+1)
				in(i,a[qq[1]]*(i-qq[1]-1)-ask(1,n,1,qq[1]+1,i-1));
			qq[++l]=i;
		}
	}*/

	
		

#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
	putchar('\n');
}
const int N=100005,M=2500;
int a[N],f[M][M],n,m,x,y,ans,q;
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	read(n);read(q);
	for (int i=0;i<n;++i)
		read(a[i]);
	m=(int)pow(n,2.0/5);
	for (int i=1;i<=m;++i)
		for (int j=0;j<n;++j)
			f[i][j%i]+=a[j];
	while (q--)
	{
		read(y);
		read(x);
		if (x<=m)
			write(f[x][y]);
		else
		{
			ans=0;
			for (int i=y;i<n;i+=x)
				ans+=a[i];
			write(ans);
		}
	}
	return 0;
}

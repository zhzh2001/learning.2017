#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(ll &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(ll x)
{
	if (x==0)
		putchar('0');
	int a[30];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const int N=1005;
ll n,m,k,a[N][N],b[N],c[N],sum,ans,x[N],y[N],xx;
int main()
{
	ans=(ll)1e18+7;
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	read(n);
	read(m);
	read(k);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			read(a[i][j]);
	if (n<m)
	{
		for (int i=1;i<=n;++i)
		{
			sum=0;
			for (int j=1;j<=m;++j)
				c[j]=(k-a[i][j])%k;
			xx=k-a[i][1];
			for (int j=1;j<=n;++j)
				b[j]=(2*k-a[i][1]-xx)%k;
			for (int j=1;j<=n;++j)
				sum+=b[j];
			for (int j=1;j<=n;++j)
				sum+=c[j];
			if (sum<ans)
			{
				ans=sum;
				for (int j=1;j<=n;++j)
					x[j]=b[j];
				for (int j=1;j<=m;++j)
					y[j]=c[j];
			}
		}
	}
	else
	{
		for (int i=1;i<=m;++i)
		{
			sum=0;
			for (int j=1;j<=n;++j)
				b[j]=(k-a[j][i])%k;
			xx=k-a[1][i];
			for (int j=1;j<=m;++j)
				c[j]=(2*k-a[1][j]-xx)%k;
			for (int j=1;j<=n;++j)
				sum+=b[j];
			for (int j=1;j<=m;++j)
				sum+=c[j];
			if (sum<ans)
			{
				ans=sum;
				for (int j=1;j<=n;++j)
					x[j]=b[j];
				for (int j=1;j<=m;++j)
					y[j]=c[j];
			}	
		}
	}
	write(ans);
	putchar('\n');
	for (int i=1;i<=n;++i)
	{
		write(x[i]);
		putchar(' ');
	}
	putchar('\n');
	for (int i=1;i<=m;++i)
	{
		write(y[i]);
		putchar(' ');
	}
	return 0;
}
	
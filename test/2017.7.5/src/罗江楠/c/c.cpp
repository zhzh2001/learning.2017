#include <bits/stdc++.h>
#define N 100020
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N];
char ch[5];/*
struct tree{
	int tag[N<<2], val[N];
	void push_down(int x){
		if(!tag[x]) return;
		tag[x<<1] = tag[x];
		tag[x<<1|1] = tag[x];
		tag[x] = 0;
	}
	int build(int x = 1, int l = 1, int r = n){
		if(l == r) return tag[x] = val[l];
		int mid = l + r >> 1;
		build(x<<1, l, mid); build(x<<1|1, mid+1, r);
	}
	int ask(int x, int p, int L = 1, int R = n){
		if(tag[x]) return tag[x];
		if(L == R) return val[L];
		int mid = L + R >> 1;
		if(p <= mid) return ask(x<<1, p, L, mid);
		else return ask(x<<1|1, p, mid+1, R);
	}
	int update(int x, int l, int r, int v, int L = 1, int R = n){
		if(l == L && r == R) return tag[x] = max(tag[x], v);
		if(tag[x] > v) return 0;
		push_down(x);
		int mid = L + R >> 1;
		if(r <= mid) return update(x<<1, l, r, v, L, mid);
		if(l > mid) return update(x<<1|1, l, r, v, mid+1, R);
		update(x<<1, l, mid, v, L, mid);
		update(x<<1|1, mid+1, r, v, mid+1, R);
	}
	int operator [] (int x){
		return ask(1, x, 1, n);
	}
}lmx, rmx;*/
int lmx[N], rmx[N], n, m;
void update(){
	int x = read(), v = read();
	a[x] += v;
	if(a[x] > lmx[x])
		for(int i = x+1; i <= n; i++) lmx[i] = max(lmx[i], a[x]);
	if(a[x] > rmx[x])
		for(int i = 1; i <= x-1; i++) rmx[i] = max(rmx[i], a[x]);
}
int ask(){
	int ans = 0;
	for(int i = 1; i <= n; i++)
		ans += max(min(lmx[i], rmx[i])-a[i], 0);
	return ans;
}
int main(int argc, char const *argv[]){
	File("c");
	n = read(); m = read();
	for(int i = 1; i <= n; i++) a[i] = read();
	for(int i = 1; i <= n; i++) lmx[i] = max(lmx[i-1], a[i-1]);
	for(int i = n; i; i--) rmx[i] = max(rmx[i+1], a[i+1]);
	while(m--){
		scanf("%s", ch);
		if(ch[0] == 'P') printf("%d\n", ask());
		else update();
	}
	return 0;
}

#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N], ans[N], b[N]; // b[i]表示所有能被i整出
struct node{
	int p, q, id;
	bool operator < (const node &b) const {
		return p == b.p ? q < b.q : p < b.p;
	}
}ask[N];
int main(int argc, char const *argv[]){
	File("a");
	int n = read(), m = read();
	for(int i = 0; i < n; i++) a[i] = read();
	for(int i = 1; i <= m; i++){
		ask[i].q = read() % (ask[i].p = read());
		ask[i].id = i;
	}
	sort(ask+1, ask+m+1);
	for(int i = 1, j = 1; i <= m; i = ++j){
		while(j < m && ask[j+1].p == ask[j].p) j++;
		int p = ask[i].p;
		for(int k = 0; k < p; k++) b[k] = 0;
		for(int k = 0; k < n; k++) b[k%p] += a[k];
		for(int k = i; k <= j; k++) ans[ask[k].id] = b[ask[k].q];
	}
	for(int i = 1; i <= m; i++) printf("%d\n", ans[i]);
	return 0;
}

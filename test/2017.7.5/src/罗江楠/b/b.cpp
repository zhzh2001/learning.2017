#include <bits/stdc++.h>
#define N 1020
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int mp[N][N], a[N][N];
int main(int argc, char const *argv[]){
	File("b");
	int n = read(), m = read(), k = read();
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			mp[i][j] = (a[i][j] = read())+mp[i][j-1]+mp[i-1][j]-mp[i-1][j-1];
	int ans = 1<<30, l = 0, r = 0;
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			if((a[i][1]+a[1][j])%k == a[1][1] &&
				ans > mp[n][j]-mp[n][j-1]+mp[i][m]-mp[i-1][m]){
				ans = mp[n][j]-mp[n][j-1]+mp[i][m]-mp[i-1][m];
				l = i, r = j;
			}
	printf("%d\n", ans);
	for(int i = 1; i <= n; i++) printf("%d ", a[i][r]); puts("");
	for(int i = 1; i <= m; i++) printf("%d ", a[l][i]); puts("");
	return 0;
}

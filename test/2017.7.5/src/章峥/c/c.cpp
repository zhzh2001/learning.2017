#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=100005;
int a[N],Max[N],rMax[N];
int main()
{
	int n,m;
	fin>>n>>m;
	long long sum=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		sum+=a[i];
	}
	for(int i=1;i<=n;i++)
		Max[i]=max(Max[i-1],a[i]);
	for(int i=n;i;i--)
		rMax[i]=max(rMax[i+1],a[i]);
	long long ans=0;
	for(int i=1;i<=n;i++)
		ans+=min(Max[i],rMax[i]);
	while(m--)
	{
		char opt;
		fin>>opt;
		if(opt=='P')
			fout<<ans-sum<<endl;
		else
		{
			int x,y;
			fin>>x>>y;
			sum+=y;
			a[x]+=y;
			for(int i=x;i<=n&&Max[i]<a[x];i++)
			{
				ans-=min(Max[i],rMax[i]);
				Max[i]=a[x];
				ans+=min(Max[i],rMax[i]);
			}
			for(int i=x;i&&rMax[i]<a[x];i--)
			{
				ans-=min(Max[i],rMax[i]);
				rMax[i]=a[x];
				ans+=min(Max[i],rMax[i]);
			}
		}
	}
	return 0;
}
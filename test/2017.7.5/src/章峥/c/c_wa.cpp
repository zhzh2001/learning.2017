#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=100005;
int n,m,a[N],b[N],c[N];
struct segmentTree
{
	struct node
	{
		long long sum,lazy;
	}tree[N*4];
	int *a,L,R;
	long long val;
	void build(int id,int l,int r)
	{
		if(l==r)
		{
			tree[id].sum=a[l];
			tree[id].lazy=0;
		}
		else
		{
			int mid=(l+r)/2;
			build(id*2,l,mid);
			build(id*2+1,mid+1,r);
			tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
		}
	}
	void pushdown(int id,int l,int r)
	{
		if(tree[id].lazy&&l<r)
		{
			int mid=(l+r)/2;
			tree[id*2].sum=tree[id].lazy*(mid-l+1);
			tree[id*2].lazy=tree[id].lazy;
			tree[id*2+1].sum=tree[id].lazy*(r-mid);
			tree[id*2+1].lazy=tree[id].lazy;
			tree[id].lazy=0;
		}
	}
	void cover(int id,int l,int r)
	{
		if(L<=l&&R>=r)
		{
			tree[id].sum=val*(r-l+1);
			tree[id].lazy=val;
		}
		else
		{
			pushdown(id,l,r);
			int mid=(l+r)/2;
			if(L<=mid)
				cover(id*2,l,mid);
			if(R>mid)
				cover(id*2+1,mid+1,r);
			tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
		}
	}
	void query(int id,int l,int r)
	{
		if(L<=l&&R>=r)
			val+=tree[id].sum;
		else
		{
			pushdown(id,l,r);
			int mid=(l+r)/2;
			if(L<=mid)
				query(id*2,l,mid);
			if(R>mid)
				query(id*2+1,mid+1,r);
			tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
		}
	}
}Max,rMax;
int main()
{
	fin>>n>>m;
	long long sum=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		sum+=a[i];
	}
	for(int i=1;i<=n;i++)
		b[i]=max(b[i-1],a[i]);
	Max.a=b;
	Max.build(1,1,n);
	for(int i=n;i;i--)
		c[i]=max(c[i+1],a[i]);
	rMax.a=c;
	rMax.build(1,1,n);
	long long ans=0;
	for(int i=1;i<=n;i++)
		ans+=min(b[i],c[i]);
	while(m--)
	{
		char opt;
		fin>>opt;
		if(opt=='P')
			fout<<ans-sum<<endl;
		else
		{
			int x,y;
			fin>>x>>y;
			sum+=y;
			Max.L=x;Max.R=x;
			Max.val=0;
			Max.query(1,1,n);
			rMax.L=x;rMax.R=x;
			rMax.val=0;
			rMax.query(1,1,n);
			ans+=a[x]+y-min(Max.val,rMax.val);
			//find 1st Max[i]>=a[x]+y
			int l=1,r=x-1;
			while(l<r)
			{
				int mid=(l+r)/2;
				Max.L=Max.R=mid;
				Max.val=0;
				Max.query(1,1,n);
				if(Max.val>=a[x]+y)
					r=mid;
				else
					l=mid+1;
			}
			Max.L=Max.R=r;
			Max.val=0;
			Max.query(1,1,n);
			if(Max.val<a[x]+y)
				r++;
			int p1=r;
			//find 1st rMax[i]<a[x]+y
			l=1;r=x-1;
			while(l<r)
			{
				int mid=(l+r)/2;
				rMax.L=rMax.R=mid;
				rMax.val=0;
				rMax.query(1,1,n);
				if(rMax.val<a[x]+y)
					r=mid;
				else
					l=mid+1;
			}
			rMax.L=rMax.R=r;
			rMax.val=0;
			rMax.query(1,1,n);
			if(rMax.val>=a[x]+y)
				r++;
			int p2=r;
			
			rMax.L=max(p1,p2);rMax.R=x-1;
			rMax.val=0;
			rMax.query(1,1,n);
			ans+=1ll*min(x-p1,x-p2)*(a[x]+y)-rMax.val;
			
			rMax.L=p2;rMax.R=x;
			rMax.val=a[x]+y;
			rMax.cover(1,1,n);
			//find 1st Max[i]>=a[x]+y
			l=x+1;r=n;
			while(l<r)
			{
				int mid=(l+r)/2;
				Max.L=Max.R=mid;
				Max.val=0;
				Max.query(1,1,n);
				if(Max.val>=a[x]+y)
					r=mid;
				else
					l=mid+1;
			}
			Max.L=Max.R=r;
			Max.val=0;
			Max.query(1,1,n);
			if(Max.val<a[x]+y)
				r++;
			p1=r;
			if(p1>x)
				p1--;
			//find 1st rMax[i]<a[x]+y
			l=x+1;r=n;
			while(l<r)
			{
				int mid=(l+r)/2;
				rMax.L=rMax.R=mid;
				rMax.val=0;
				rMax.query(1,1,n);
				if(rMax.val<a[x]+y)
					r=mid;
				else
					l=mid+1;
			}
			rMax.L=rMax.R=r;
			rMax.val=0;
			rMax.query(1,1,n);
			if(rMax.val>=a[x]+y)
				r++;
			p2=r;
			if(p2>x)
				p2--;
			
			Max.L=x+1;Max.R=min(p1,p2);
			Max.val=0;
			Max.query(1,1,n);
			ans+=1ll*min(p1-x,p2-x)*(a[x]+y)-Max.val;
			
			Max.L=x;Max.R=p1;
			Max.val=a[x]+y;
			Max.cover(1,1,n);
			a[x]+=y;
		}
	}
	return 0;
}
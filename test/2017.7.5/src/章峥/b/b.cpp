#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N=1005;
int a[N][N],b[N][N],row[N],col[N],ansr[N],ansc[N];
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			fin>>a[i][j];
	long long ans=1e18;
	bool rrev;
	for(int rev=0;rev<2;rev++)
	{
		for(int i=1;i<=n;i++)
		{
			long long now=0;
			for(int j=1;j<=n;j++)
				now+=(row[j]=(a[i][1]-a[j][1]+k)%k);
			for(int j=1;j<=m;j++)
				now+=(col[j]=(k-a[i][j])%k);
			if(now<ans)
			{
				ans=now;
				rrev=rev;
				copy(row+1,row+n+1,ansr+1);
				copy(col+1,col+m+1,ansc+1);
			}
		}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				b[j][i]=a[i][j];
		swap(n,m);
		for(int i=1;i<=n;i++)
			copy(b[i]+1,b[i]+m+1,a[i]+1);
	}
	fout<<ans<<endl;
	int *row=ansr,*col=ansc;
	if(rrev)
	{
		swap(row,col);
		swap(n,m);
	}
	for(int i=1;i<=n;i++)
		fout<<row[i]<<' ';
	fout<<endl;
	for(int i=1;i<=m;i++)
		fout<<col[i]<<' ';
	fout<<endl;
	return 0;
}
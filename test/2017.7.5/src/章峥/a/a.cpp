#include <fstream>
#include <algorithm>
#include <cmath>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N = 100000, SQRTN = 320;
int a[N], sum[SQRTN][SQRTN];
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 0; i < n; i++)
		fin >> a[i];
	int sz = floor(sqrt(n));
	for (int i = 1; i <= sz; i++)
		for (int j = 0; j < n; j++)
			sum[i][j % i] += a[j];
	while (m--)
	{
		int q, p;
		fin >> q >> p;
		if (p <= sz)
			fout << sum[p][q] << endl;
		else
		{
			int ans = 0;
			for (int i = q; i < n; i += p)
				ans += a[i];
			fout << ans << endl;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.ans");
const int N=100000;
int a[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=0;i<n;i++)
		fin>>a[i];
	while(m--)
	{
		int q,p;
		fin>>q>>p;
		int ans=0;
		for(int i=q;i<n;i+=p)
			ans+=a[i];
		fout<<ans<<endl;
	}
	return 0;
}
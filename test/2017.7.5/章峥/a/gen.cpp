#include<bits/stdc++.h>
using namespace std;
ofstream fout("a.in");
const int n=100000,m=100000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=0;i<n;i++)
	{
		uniform_int_distribution<> d(0,100);
		fout<<d(gen)<<' ';
	}
	fout<<endl;
	for(int i=0;i<m;i++)
	{
		uniform_int_distribution<> dp(1,n);
		int p=dp(gen);
		uniform_int_distribution<> dq(0,p-1);
		fout<<dq(gen)<<' '<<p<<endl;
	}
	return 0;
}
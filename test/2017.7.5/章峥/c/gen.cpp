#include<bits/stdc++.h>
using namespace std;
ofstream fout("c.in");
const int n=5,m=10;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(1,10);
		fout<<d(gen)<<' ';
	}
	fout<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> dopt(1,2);
		if(dopt(gen)==1)
			fout<<"P\n";
		else
		{
			uniform_int_distribution<> dn(1,n),dv(1,10);
			fout<<"U "<<dn(gen)<<' '<<dv(gen)<<endl;
		}
	}
	return 0;
}
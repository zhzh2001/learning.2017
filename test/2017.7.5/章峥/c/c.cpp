#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=100005;
int a[N],Max[N],rMax[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	while(m--)
	{
		char opt;
		fin>>opt;
		if(opt=='P')
		{
			for(int i=1;i<=n;i++)
				Max[i]=max(Max[i-1],a[i]);
			for(int i=n;i;i--)
				rMax[i]=max(rMax[i+1],a[i]);
			long long ans=0;
			for(int i=1;i<=n;i++)
				ans+=min(Max[i],rMax[i])-a[i];
			fout<<ans<<endl;
		}
		else
		{
			int x,y;
			fin>>x>>y;
			a[x]+=y;
		}
	}
	return 0;
}
#include<fstream>
#include<cstdlib>
#include<algorithm>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N=1005;
int n,m,k,a[N][N],row[N],col[N],ans,ansr[N],ansc[N];
void dfs(int id,int sum)
{
	if(id==n+m+1)
	{
		if(sum==0)
		{
			int cnt=0;
			for(int i=1;i<=n;i++)
				cnt+=row[i];
			for(int i=1;i<=m;i++)
				cnt+=col[i];
			if(cnt<ans)
			{
				ans=cnt;
				copy(row+1,row+n+1,ansr+1);
				copy(col+1,col+m+1,ansc+1);
			}
		}
		return;
	}
	for(int i=0;i<k;i++)
		if(id<=n)
		{
			row[id]=i;
			int now=sum;
			for(int j=1;j<=m;j++)
			{
				now-=a[id][j];
				a[id][j]=(a[id][j]+i)%k;
				now+=a[id][j];
			}
			dfs(id+1,now);
			for(int j=1;j<=m;j++)
				a[id][j]=(a[id][j]+k-i)%k;
		}
		else
		{
			col[id-n]=i;
			int now=sum;
			for(int j=1;j<=n;j++)
			{
				now-=a[j][id-n];
				a[j][id-n]=(a[j][id-n]+i)%k;
				now+=a[j][id-n];
			}
			dfs(id+1,now);
			for(int j=1;j<=n;j++)
				a[j][id-n]=(a[j][id-n]+k-i)%k;
		}
}
int main()
{
	fin>>n>>m>>k;
	int sum=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>a[i][j];
			sum+=a[i][j];
		}
	ans=1e9;
	dfs(1,sum);
	fout<<ans<<endl;
	for(int i=1;i<=n;i++)
		fout<<ansr[i]<<' ';
	fout<<endl;
	for(int i=1;i<=m;i++)
		fout<<ansc[i]<<' ';
	fout<<endl;
	return 0;
}
#include<bits/stdc++.h>

using namespace std;

int n,Q,p,q,a[100010],ans[500][500];

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&Q);
	for(int i=0;i<n;i++)
		scanf("%d",&a[i]);
	int k=sqrt(n);
	for(int i=1;i<=k;i++)
	{
		for(int j=0;j<i;j++)
			ans[i][j]=a[j];
		for(int j=i;j<n;j+=i)
		{
			for(int l=0;l<i&&j+l<n;l++)
				ans[i][l]+=a[j+l];
		}
	}
	while(Q--)
	{
		scanf("%d%d",&q,&p);
		if(p<=k)printf("%d\n",ans[p][q]);
		else
		{
			int nowans=0;
			for(int i=q;i<n;i+=p)
				nowans+=a[i];
			printf("%d\n",nowans);
		}
	}
}

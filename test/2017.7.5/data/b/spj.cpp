#include <bits/stdc++.h>
using namespace std;

FILE *fres;

void Return(double pts, string s){
	fprintf(fres, "%.6lf\n%s\n", pts, s.c_str());
	exit(0);
}

void Return(double pts, int u, int v){
	fprintf(fres, "%.6lf\n%d %d\n", pts, u, v);
	exit(0);
}


int n, m, grids[1005][1005], k;

int main(int argc, char** argv){
	FILE *finn = fopen(argv[1], "r");
	FILE *fstd = fopen(argv[2], "r");
	FILE *fout = fopen(argv[3], "r");
	fres = fopen(argv[4], "w");
	fscanf(finn, "%d%d%d", &n, &m, &k);
	for(int i = 1; i <= n; i ++)
	for(int j = 1; j <= m; j ++){
		fscanf(finn, "%d", &grids[i][j]);
	}
	long long std_ans = 0, you_ans = 0;
	fscanf(fstd, "%lld", &std_ans);
	fscanf(fout, "%lld", &you_ans);
	if(you_ans != std_ans){
		Return(0.0, "Wrong Answer");
	}
	long long acc = 0;
	for(int i = 1; i <= n; i ++){
		int x;
		fscanf(fout, "%d", &x);
		if(x < 0 || x >= k){
			Return(0.0, "invalid answer");
		}
		acc += x;
		for(int j = 1; j <= m; j ++) grids[i][j] = (grids[i][j] + x) % k;
	}
	for(int j = 1; j <= m; j ++){
		int x;
		fscanf(fout, "%d", &x);
		if(x < 0 || x >= k){
			Return(0.0, "invalid answer");
		}
		acc += x;
		for(int i = 1; i <= n; i ++) grids[i][j] = (grids[i][j] + x) % k;
	}
	if(acc != you_ans){
		Return(0.0, "Answer and solution not match");
	}
	for(int i = 1; i <= n; i ++)
	for(int j = 1; j <= m; j ++){
		if(grids[i][j] != 0){
			Return(0.0, "invalid answer");
		}
	}
	Return(1.0, "Accepted");
	return 0;
}

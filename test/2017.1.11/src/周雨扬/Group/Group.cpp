#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
#define N 50005
#define ll long long
using namespace std;
struct data{
	int x,y,z;
}a[505];
bool cmp(data a,data b){
	if (a.x==b.x) return a.y<b.y;
	return a.x<b.x;
}
int t,n,x,y,tot,p,f[505];
int main(){
	freopen("Group.in","r",stdin);
	freopen("Group.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d",&n);
		tot=0;
		for (int i=1;i<=n;i++){
			scanf("%d%d",&x,&y);
			if (x+y>=n) continue;
			a[++tot].x=x+1;
			a[tot].y=n-y;
			a[tot].z=1;
		}
		sort(a+1,a+tot+1,cmp);
		p=1;
		for (int i=2;i<=tot;i++)
			if (a[i].x==a[i-1].x&&a[i].y==a[i-1].y){
				if (a[p].z+a[p].x<=a[p].y) a[p].z++;
			}
			else a[++p]=a[i];
		memset(f,0,sizeof(f));
		for (int i=1;i<=n;i++){
			f[i]=max(f[i],f[i-1]);
			for (int j=1;j<=p;j++)
				if (a[j].y==i) f[i]=max(f[i],f[a[j].x-1]+a[j].z);
		}
		printf("%d\n",f[n]);
	}
}

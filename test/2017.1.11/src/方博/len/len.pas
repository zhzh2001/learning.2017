program len;
var s,s1:ansistring;
i,j:longint;
begin
assign(input,'len.in');
reset(input);
assign(output,'len.out');
rewrite(output);
readln(s);
if(s[length(s)]='0')then i:=10
 else i:=ord(s[length(s)])-48;
for j:=length(s)downto 1 do
 if(s[j]=' ')then begin
  s1:=copy(s,1,j-1);
  break;
 end;
j:=length(s1);
if(ord(s1[1])-48<i)then j:=j-1;
writeln(j);
close(input);
close(output);
end.
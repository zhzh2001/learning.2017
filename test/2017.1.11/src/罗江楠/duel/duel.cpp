#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define read(a) scanf("%d", &a)
#define write(a) printf("%d ", a)
#define writeln(a) printf("%d\n", a)
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a < c; a ++)
#define MOD 1000000007

using namespace std;

struct node{
	//char name;
	int val[1500];
	int ca;
	int HP;
	int maxHP;
	int hasSha = 0;
	int hasShan = 0;
	void chuSha(){
		//cout << name<<"出杀"<<endl;
		val[hasSha] = 0;
		while(hasSha > 0 && val[hasSha] != 1) hasSha--;
	}
	void chuShan(){
		//cout << name<< "出闪" <<endl;
		val[hasShan] = 0;
		while(hasShan > 0 && val[hasShan] != 2) hasShan--;
	}
	void add(int k){
		//cout << name<< "收到牌"<<k<<endl;
		switch(k){
			case 1:val[++val[0]] = 1; hasSha = val[0]; break;
			case 2:val[++val[0]] = 2; hasShan = val[0]; break;
			case 3:
				if(HP == maxHP)
					ca ++;
				else if(HP+ca<maxHP)
					HP += ca, ca = 0;
				else ca -= maxHP - HP, HP = maxHP;
		}
	}
	void fadd(int k){
		//cout << name<< "收到牌"<<k;
		switch(k){
			case 1:val[++val[0]] = 1; hasSha = val[0]; break;
			case 2:val[++val[0]] = 2; hasShan = val[0]; break;
			case 3:ca++;
		}
	}
} A, B;
int n, s, a[1005], k;

int main(){
	freopen("duel.in", "r", stdin);
	freopen("duel.out", "w", stdout);
	//A.name='A';
	//B.name='B';
	read(n);
	rep(i, 0, n) read(a[i]);
	read(A.HP);
	A.maxHP = A.HP;
	read(B.HP);
	B.maxHP = B.HP;
	rep(i, 0, 3) A.fadd(a[i % n]);
	rep(i, 3, 6) B.fadd(a[i % n]);
	k = 6 % n;
	while(A.HP && B.HP){
		s ++;
		A.add(a[(k++) % n]);
		if(A.hasSha){A.chuSha();if(B.hasShan)B.chuShan();else B.HP--;}
		//cout<<A.HP<<" "<<B.HP<<endl;
		if(!A.HP || !B.HP)break;
		B.add(a[(k++) % n]);
		if(B.hasSha){B.chuSha();if(A.hasShan)A.chuShan();else A.HP--;}
		//cout<<A.HP<<" "<<B.HP<<endl;
	}
	if(A.HP)
		cout << "AWIN! " << s << endl;
	else
		cout << "BWIN! " << s << endl;
    return 0;
}


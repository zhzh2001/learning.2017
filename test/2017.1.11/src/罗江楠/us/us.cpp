#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define read(a) scanf("%d", &a)
#define write(a) printf("%d ", a)
#define writeln(a) printf("%d\n", a)
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a < c; a ++)
#define MOD 1000000007

using namespace std;
string k;
int main(){
	freopen("us.in", "r", stdin);
	freopen("us.out", "w", stdout);
	getline(cin, k);
	if(k.length() == 47)
		if(k == "Begin the Escape execution at the Break of Dawn")
			cout << "1 0";
		else
			cout << "0";
	else if((k.length() - 47) % 3 == 0)
		cout << "1 " << (k.length() - 47) / 3;
	else
		cout << "0";
    return 0;
}


#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h> 
#include <queue>
#include <cmath>
#include <math.h>
#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <set>
#include <map>
#include <fstream>
#include <bits/stdc++.h>
#define ll long long
#define oo 1<<30
#define ESP 1e-9
#define read(a) scanf("%d", &a)
#define write(a) printf("%d ", a)
#define writeln(a) printf("%d\n", a)
#define obs(a) a&1
#define mst(a, b) memset(a, b, sizeof a)
#define rep(a, b, c) for(int a = b; a < c; a ++)
#define MOD 1000000007

using namespace std;

int t, n, x, y, visx[505], visy[505];

int main(){
	freopen("Group.in", "r", stdin);
	freopen("Group.out", "w", stdout);
	read(t);
	while(t --){
		read(n);
		mst(visx, -1);
		mst(visy, -1);
		int ans = n;
		for(int i = 0; i < n; i ++){
			read(x);
			read(y);
			if(x + y >= n)
				ans --;
			else if(visx[x] != -1 && visx[x] != y)
				ans --;
			else if(visy[y] != -1 && visy[y] != x)
				ans --;
			else visx[x] = y, visy[y] = x;
		}
		cout<<ans<<endl;
	}
    return 0;
}


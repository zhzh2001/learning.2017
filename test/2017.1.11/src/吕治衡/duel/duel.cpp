#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
using namespace std;
int t,i,ans,x,y,f[10][10],a[10000];
char c[10];
int main()
{
	freopen("duel.in","r",stdin);freopen("duel.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>t;
	for (i=1;i<=t;i++) cin>>a[i];
	cin>>x>>y;
	f[1][0]=x;f[2][0]=y;
	for (i=1;i<=3;i++) f[1][a[(i-1)% t+1]]++;
	for (;i<=6;i++)f[2][a[(i-1)% t+1]]++;
	c[1]='A';c[2]='B';
	for (;f[1][0]>0;i++)
		{
			if (c[1]=='A') ans++;
			f[1][a[(i-1)% t+1]]++;
			while ((f[1][3]>0)&&(f[1][0]<x))f[1][3]--,f[1][0]++;
			if (f[1][1]>0)
				{
					f[1][1]--;
					if (f[2][2]>0) f[2][2]--;
						else f[2][0]--;
				}
			swap(f[1],f[2]);
			swap(c[1],c[2]);
		}
	cout<<c[2]<<"WIN! "<<ans;
}

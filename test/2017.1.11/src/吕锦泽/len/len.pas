var
        ch:char;
        s:ansistring;
        k,ans,t,i,num:longint;
begin
        assign(input,'len.in');
        assign(output,'len.out');
        reset(input); rewrite(output);
        read(ch);
        while ch<>' ' do
        begin
                s:=s+ch;
                read(ch);
        end;
        read(k);
        val(copy(s,1,2),num);
        t:=num div k;
        while t>0 do
        begin
                inc(ans);
                t:=t div 10;
        end;
        write(ans+length(s)-2);
        close(input); close(output);
end.

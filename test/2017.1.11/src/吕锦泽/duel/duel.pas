var
        x,y,n,i,s,num,asha,ashan,atao,axie,bsha,btao,bshan,bxie:longint;
        a:array[1..2000] of longint;
begin
        assign(input,'duel.in');
        assign(output,'duel.out');
        reset(input); rewrite(output);
        read(n);
        for i:=1 to n do
                read(a[i]);
        read(axie,bxie);
        x:=axie; y:=bxie;
        for i:=1 to 3 do
        begin
                case a[(i-1) mod n+1] of
                        1: inc(asha);
                        2: inc(ashan);
                        3: inc(atao);
                end;
                case a[(i+2) mod n+1] of
                        1: inc(bsha);
                        2: inc(bshan);
                        3: inc(btao);
                end;
        end;
        num:=6 mod n; s:=0;
        while (axie>0)and(bxie>0) do
        begin
                inc(s); inc(num); if num=n+1 then num:=1;
                case a[num] of
                        1: inc(asha);
                        2: inc(ashan);
                        3: inc(atao);
                end;
                while (axie<x)and(atao>0) do
                begin
                        inc(axie);
                        dec(atao);
                end;
                if asha>0 then
                begin
                        dec(asha);
                        if bshan>0 then dec(bshan)
                        else dec(bxie);
                end;
                if bxie=0 then
                begin
                        write('AWIN! ',s);
                        close(input); close(output);
                        halt;
                end;
                inc(num); if num=n+1 then num:=1;
                case a[num] of
                        1: inc(bsha);
                        2: inc(bshan);
                        3: inc(btao);
                end;
                while (bxie<x)and(btao>0) do
                begin
                        inc(bxie);
                        dec(btao);
                end;
                if bsha>0 then
                begin
                        dec(bsha);
                        if ashan>0 then dec(ashan)
                        else dec(axie);
                end;
                if axie=0 then
                begin
                        write('BWIN! ',s);
                        close(input); close(output);
                        halt;
                end;
        end;
        close(input); close(output);
end.


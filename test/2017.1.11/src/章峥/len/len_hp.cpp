#include<fstream>
#include<cstring>
using namespace std;
struct bigint
{
	int len,dig[10005];
	bigint();
	bigint(int x);
	void clear();
	bigint operator/(int x);
};

istream& operator>>(istream& is,bigint& b);

bigint::bigint()
{
	len=1;
	memset(dig,0,sizeof(dig));
}

bigint::bigint(int x)
{
	len=0;
	do
		dig[++len]=x%10;
	while(x/=10);
}

void bigint::clear()
{
	while(len>1&&dig[len]==0)
		len--;
}

bigint bigint::operator/(int x)
{
	int remain=0;
	for(int i=len;i>=1;i--)
	{
		remain*=10;
		int t=(dig[i]+remain)/x;
		remain=(dig[i]+remain)%x;
		dig[i]=t;
	}
	clear();
	return *this;
}

istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b.len=s.length();
	for(int i=0;i<b.len;i++)
		if(isdigit(s[i]))
			b.dig[b.len-i]=s[i]-'0';
	return is;
}

ifstream fin("len.in");
ofstream fout("len.ans");

int main()
{
	bigint n;
	int k;
	fin>>n>>k;
	n=n/k;
	fout<<n.len<<endl;
	return 0;
}
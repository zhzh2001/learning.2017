#include<fstream>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("Group.in");
ofstream fout("Group.out");
struct node
{
	int u,w;
};
vector<node> s[505];
int f[505],ss[505][505];
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n;
		fin>>n;
		for(int i=1;i<=n;i++)
			s[i].clear();
		memset(ss,-1,sizeof(ss));
		for(int i=1;i<=n;i++)
		{
			int x,y;
			fin>>x>>y;
			if(x+1<=n-y)
				if(ss[n-y][x+1]==-1)
				{
					s[n-y].push_back((node){x+1,1});
					ss[n-y][x+1]=s[n-y].size()-1;
				}
				else
					if(s[n-y][ss[n-y][x+1]].w<n-x-y)
						s[n-y][ss[n-y][x+1]].w++;
		}
		memset(f,0,sizeof(f));
		for(int i=1;i<=n;i++)
		{
			f[i]=f[i-1];
			for(int j=0;j<s[i].size();j++)
				f[i]=max(f[i],f[s[i][j].u-1]+s[i][j].w);
		}
		fout<<f[n]<<endl;
	}
	return 0;
}
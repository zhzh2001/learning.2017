#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#define mod 1000000
#define inf 2000000000
#define ll int
#define eps 10e-8
const double seps=1e-9;
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
int main(){
	freopen("len.in","r",stdin);
	freopen("len.out","w",stdout); 
	char s[100000];
	ll n;
	scanf("%s",s);	n=read();
	if (s[0]-'0'>=n) 	writeln(strlen(s));
	else	writeln(strlen(s)-1);
}

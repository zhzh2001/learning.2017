#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#include<memory.h>
#define mod 1000000
#define inf 2000000000
#define ll int
#define eps 10e-8
const double seps=1e-9;
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
int main(){
	freopen("Group.in","r",stdin);
	freopen("Group.out","w",stdout);
	ll n,f[505][505],g[505];
	ll T=read();
	while (T--){
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g)); 
		n=read();
		for (ll i=1;i<=n;i++){
			ll x=read(),y=read();
			if (x+y<n and (f[x+1][n-y]!=(n-x-y)))	f[x+1][n-y]++;
		}
		for (ll i=1;i<n;i++)
			for (ll j=1;j<=n-i;j++)
				f[i][i+j]=max(max(f[i][i+j],f[i][i+j-1]),f[i+1][i+j]);
		for (ll i=1;i<=n;i++)
			for (ll j=0;j<=i;j++) 
				g[i]=max(g[i],g[j]+f[j+1][i]);
		writeln(g[n]);
	}
	return 0;
}

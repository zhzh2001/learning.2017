#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<cmath>
#define mod 1000000
#define inf 2000000000
#define ll int
#define eps 10e-8
const double seps=1e-9;
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x){
	if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	printf("\n");
}
ll t,s,x,y,fx,fy,sum[3][4],h,num[10000];
ll get(ll belong){
	h=(h%t)+1;
	sum[belong][num[h]]++;
}
int main(){
	freopen("duel.in","r",stdin);
	freopen("duel.out","w",stdout);
	t=read();
	for (ll i=1;i<=t;i++)	num[i]=read();
	for (ll i=1;i<=3;i++)	get(1);
	for (ll i=1;i<=3;i++)	get(2);
	x=read();	y=read();
	fx=x;		fy=y;
	while (1){
		s++;
		get(1);
		while (x<fx and sum[1][3])	sum[1][3]--,x++;
		if (sum[1][1]){
			sum[1][1]--;
			if (sum[2][2]) sum[2][2]--; 
			else y--;
		}
		if (y==0)	break;
		get(2);
		while (y<fy and sum[2][3]) sum[2][3]--,y++;
		if (sum[2][1]){
			sum[2][1]--;
			if (sum[1][2]) sum[1][2]--;
			else x--;
		}
		if (x==0)	break;
	}
	if (x>0)	printf("AWIN! ");
	else		printf("BWIN! ");
	writeln(s);
}

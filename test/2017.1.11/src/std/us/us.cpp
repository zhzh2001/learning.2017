#include<cstdio>
#include<cstdlib>
#include<cstring>
#define MAX_MSG_LEN     76
#define MAX_N           9       // (75-47)/3
#define MAX_N_TRIPLES   (MAX_N*MAX_N*MAX_N)
#define C_          0
#define O_          1
#define W_          2
#define NOT_COW     3
char Moo[] = "Begin the Escape execution at the Break of Dawn";
int Moo_len = 47;
char Hmm[MAX_MSG_LEN];
int Hmm_len, N, cowpos[3*MAX_N + 2], T, tr[MAX_N_TRIPLES + 1][3];
struct llnode_t {
    int data;
    llnode_t *next;
} *its_tr[3*MAX_N+1] = {0};
int n_calls_to_find_trset = 0;
int n_calls_to_find_ordering = 0;
 
void input ();
int submoo (const char *, int);
void subhmm (char *, int, int);
void init ();
int thingy (char);
void output (bool);
void find_trset (int decoded_len, int lss, int *up);
void find_ordering (int *cp, int *tr_used, int nda);
void addnode (llnode_t **np, int data)
{
    llnode_t *temp = *np;
    *np = new llnode_t;
    (*np)->data = data;
    (*np)->next = temp;
}
int thingy (char c)
{
    switch (c) {
    case 'C': return C_;
    case 'O': return O_;
    case 'W': return W_;
    default: return NOT_COW;
    }
}
int submoo (const char *word, int start)
{
    int word_len = strlen (word);
    if (word_len == 0) return start;
    int i, j;
    for (i = start; i <= Moo_len - word_len; i++) {
        for (j = 0; j < word_len && Moo[i+j] == word[j]; j++);
        if (j == word_len) return i;
    }
    return -1;
}
void subhmm (char *target, int start, int end)
{
    int i, j = 0;
    for (i = start; i <= end; i++)
        target[j++] = Hmm[i];
    target[j] = '\0';
}
void output (bool result)
{
    FILE *fout = fopen ("us.out", "w");
    if (result) { fprintf (fout, "1 %d\n", N);} else { fprintf (fout, "0 0\n");}
    fclose(fout);
    exit (0);
}
void init ()
{
    Hmm_len = strlen (Hmm);
    if (Hmm_len < Moo_len || (Hmm_len - Moo_len) % 3 != 0) { output (0);} else N = (Hmm_len - Moo_len) / 3;
    int i;
    int chartype;
    int ncows[3] = {0};
    int totncows = 1;
    cowpos[0] = -1;
    for (i = 0; i < Hmm_len; i++) {
        chartype = thingy (Hmm[i]);
        if (chartype != NOT_COW) {
            if (++ncows[chartype] > N) { output (0);}
            cowpos[totncows++] = i;
            if (totncows == 1 && chartype != C_) { output (0);} else if (totncows == 3*N+1 && chartype != W_) { output (0);}
        } else {
            if (totncows == 1 && Hmm[i] != Moo[i]) output (0);
			else if (totncows == 3*N+1 && Hmm[i] != Moo[i+Moo_len-Hmm_len]) output (0);
        }
    }
    cowpos[3*N+1] = Hmm_len;
    if (ncows[0] != N || ncows[1] != N || ncows[2] != N) output (0);
    if (Hmm_len == Moo_len) output (1);
    int c, o, w;
    char segment[3][2][MAX_MSG_LEN];
    char subword[MAX_MSG_LEN];
    int c_start;
    T = 0;
    for (c = 1; c <= 3*N; c++) {
        if (Hmm[cowpos[c]] != 'C') continue;
        subhmm (segment[C_][0], cowpos[c-1] + 1, cowpos[c] - 1);
        subhmm (segment[C_][1], cowpos[c] + 1, cowpos[c+1] - 1);
        c_start = submoo (segment[C_][0], 0);
        if (c_start == -1) output (0);
        for (o = 1; o <= 3*N; o++) {
            if (Hmm[cowpos[o]] != 'O')continue;
            subhmm (segment[O_][0], cowpos[o-1] + 1, cowpos[o] - 1);
            subhmm (segment[O_][1], cowpos[o] + 1, cowpos[o+1] - 1);
            strcpy (subword, segment[C_][0]);
            strcat (subword, segment[O_][1]);
            if (submoo (subword, c_start) == -1) continue;
            for (w = 1; w <= 3*N; w++) {
                if (Hmm[cowpos[w]] != 'W') continue;
                subhmm (segment[W_][0], cowpos[w-1] + 1, cowpos[w] - 1);
                subhmm (segment[W_][1], cowpos[w] + 1, cowpos[w+1] - 1);
                strcpy (subword, segment[O_][0]);
                strcat (subword, segment[W_][1]);
                if (submoo (subword, 0) == -1) continue;
                strcpy (subword, segment[W_][0]);
                strcat (subword, segment[C_][1]);
                if (submoo (subword, 0) == -1) continue;
                T++;
                tr[T][C_] = c;
                tr[T][O_] = o;
                tr[T][W_] = w;
                addnode (&its_tr[c], T);
                addnode (&its_tr[o], T);
                addnode (&its_tr[w], T);
            }
        }
    } 
    for (i = 1; i <= 3*N; i++) {
        if (its_tr[i] == 0) {
            output (0);
        }
    }
}
void find_trset (int decoded_len, int lss, int *up)
{
    n_calls_to_find_trset++;
    int i;
    int ls = lss+1;
    int le = thingy (Hmm[cowpos[ls]]);
    int last_seg_len;
    int next_seg_start;
    int ut;
    last_seg_len = cowpos[ls] - cowpos[lss] - 1;
    if (decoded_len + last_seg_len > Moo_len) return;
    for (i = 0; i < last_seg_len &&Moo[decoded_len + i] == Hmm[cowpos[lss]+1+i]; i++);
    if (i < last_seg_len) return;
    decoded_len += last_seg_len;
    if (lss == 3*N && decoded_len == Moo_len) {
        int tr_used[MAX_N];
        int cp[3*MAX_N+2];
        int j = 0;
        for (i = 1; i <= 3*N; i++) {
            if (Hmm[cowpos[i]] == 'C') {
                tr_used[j++] = up[i];
            }
        }
        memcpy (cp, cowpos, sizeof (*cowpos) * (3*N+2));
        find_ordering (cp, tr_used, 0);
        return;     
    }
    if ((ut = up[ls]) != 0) {
        next_seg_start = tr[ut][(le + 1) % 3];
        find_trset (decoded_len, next_seg_start, up);
    } else {
        llnode_t *trytr;
        for (trytr = its_tr[ls];trytr != 0; trytr = trytr->next) {
            ut = trytr->data;
            if (up[tr[ut][(le + 1) % 3]] != 0) continue;
            if (up[tr[ut][(le + 2) % 3]] != 0) continue;
            up[ls] = ut;
            up[tr[ut][(le + 1) % 3]] = ut;
            up[tr[ut][(le + 2) % 3]] = ut; 
            next_seg_start = tr[ut][(le+1) % 3];
            find_trset (decoded_len, next_seg_start, up); 
            up[ls] = 0;
            up[tr[ut][(le + 1) % 3]]= 0;
            up[tr[ut][(le + 2) % 3]]= 0;
        }
    }
}
void find_ordering (int *cp, int *tr_used, int nda)
{
    n_calls_to_find_ordering++;
    if (nda == N)
        output (1);
    int np[3*MAX_N+2];
    int i, j, cpos, opos, wpos;
    for (i = 0; i < N; i++) {
        cpos = cp[tr[tr_used[i]][C_]];
        opos = cp[tr[tr_used[i]][O_]];
        wpos = cp[tr[tr_used[i]][W_]];
        if (cpos >= opos || opos >= wpos) continue;
        for (j = 1; j <= 3*N; j++) {
            if (cp[j] == -1) np[j] = -1;
            else if (cp[j] < cpos) np[j] = cp[j];
            else if (cp[j] == cpos) np[j] = -1;
            else if (cp[j] < opos) np[j] = cp[j] + wpos - opos - 2;
            else if (cp[j] == opos) np[j] = -1;
            else if (cp[j] < wpos) np[j] = cp[j] + cpos - opos - 1;
            else if (cp[j] == wpos) np[j] = -1;
            else np[j] = cp[j] - 3;
        }
        find_ordering (np, tr_used, nda+1);
    }
}
 
int main ()
{
    FILE *fin = fopen ("us.in", "r");
    fscanf (fin, "%[^\n]", Hmm);
    fclose (fin);
    init ();
    int up[3*MAX_N+1] = {0};
    find_trset (0, 0, up);
    output (0);
    return 0;
}

var
  i,j,k,l,m,n:longint;
  s:ansistring;
begin
  assign(input,'len.in');
  assign(output,'len.out');
  reset(input);
  rewrite(output);
  readln(s);
  l:=length(s);
  while (s[1]='0') do
    begin
      delete(s,1,1);
      dec(l);
    end;
  while (s[l]>'9')or(s[l]<'0') do
    dec(l);
  if s[l]='0' then write(l-4)
    else if s[l]>s[1] then write(l-3)
      else write(l-2);
  close(input);
  close(output);
end.

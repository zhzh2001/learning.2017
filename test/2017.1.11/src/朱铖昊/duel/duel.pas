var
  x,y,a,b,i,n,k:longint;
  f:array[0..1500]of longint;
begin
  assign(input,'duel.in');
  assign(output,'duel.out');
  reset(input);
  rewrite(output);
  read(n);
  for i:=1 to n do read(f[i]);
  f[0]:=f[n];
  read(a,b);
  for i:=1 to 3 do
    if f[i mod n]=1 then inc(x)
      else inc(a);
  for i:=4 to 6 do
    if f[i mod n]=1 then inc(y)
      else inc(b);
  k:=7;
  i:=1;
  while true do
    begin
      if f[k mod n]=1 then inc(x)
        else inc(a);
      inc(k);
      if (x<>0) then
        begin
          dec(x);
          dec(b);
        end;
      if b=0 then
        begin
          write('AWIN! ',i);
          close(input);
          close(output);
          exit;
        end;
      if f[k mod n]=1 then inc(y)
        else inc(b);
      inc(k);
      if (y<>0) then
        begin
          dec(y);
          dec(a);
        end;
      if a=0 then
        begin
          write('BWIN! ',i);
          close(input);
          close(output);
          exit;
        end;
      //writeln(i,' ',a,' ',b,' ',x,' ',y,' ');
      inc(i);
    end;
end.

var
  i,j,k,l,m,n,x,y,t,ii:longint;
  a,b,c,f:array[0..1000]of longint;
  d:array[0..505,0..505]of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                y:=b[i];
                b[i]:=b[j];
                b[j]:=y;
                y:=c[i];
                c[i]:=c[j];
                c[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
function max(x,y:longint):longint;
  begin
    if x>y then exit(x)
      else exit(y);
  end;
begin
  assign(input,'Group.in');
  assign(output,'Group.out');
  reset(input);
  rewrite(output);
  read(t);
  for ii:=1 to t do
    begin
      for i:=0 to n do
        for j:=0 to n do
          d[i,j]:=0;
      for i:=0 to n do
        f[i]:=0;
      read(n);
      l:=0;
      for i:=1 to n do
        begin
          read(x,y);
          y:=n-y;
          //writeln(x,' ',y);
          if x<y then
            begin
              if d[x,y]=0 then
                begin
                  inc(l);
                  d[x,y]:=l;
                  a[l]:=y;
                  b[l]:=x;
                  c[l]:=1;
                end
              else
                inc(c[d[x,y]]);
            end;
         end;
      sort(1,l);
      //for i:=1 to l do writeln(a[i],' ',b[i],' ',c[i]);
      for i:=1 to l do
        begin
          f[a[i]]:=max(f[a[i]],f[b[i]]+c[i]);
          for j:=a[i]+1 to n do
            if f[a[i]]>f[j] then f[j]:=f[a[i]]
              else break;
        end;
      //for i:=1 to n do write(f[i],' ');
      writeln(f[n]);
    end;
    close(input);
    close(output);
end.


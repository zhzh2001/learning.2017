#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

int a[1050], ta, tb;
struct player{
	int sha;
	int shan;
	int tao;
	int health;
}A,B;

void add(int num, char player){
	if (player == 'A'){
		switch(num){
			case 1: A.sha++; break;
			case 2: A.shan++; break;
			case 3: A.tao++; break;
		}
	} else {
		switch(num){
			case 1: B.sha++; break;
			case 2: B.shan++; break;
			case 3: B.tao++; break;
		}
	}
}

void heal(char p){ 
	if (p == 'A'){
		while (A.tao != 0 && ta != A.health){
			A.tao--, A.health++;
		}
	} else {
		while (B.tao != 0 && tb != B.health){
			B.tao--, B.health++;
		}
	}
}

void attack(char p){ 
	if (p == 'A'){
		if (B.shan == 0) A.sha--, B.health--;
		 	else A.sha--, B.shan--;
	} else {
		if (A.shan == 0) B.sha--, A.health--;
			else B.sha--, A.shan--;
	}
}

int main(){
	freopen("duel.in", "r", stdin);
	freopen("duel.out", "w", stdout);
	int T;
	cin >> T;
	for (int i = 0; i < T; i++) cin >> a[i];
	cin >> ta >> tb;
	A.health = ta;
	B.health = tb;
	int i = 0, set = 5, kkk = 0;
	for (int j = 0; j < 3; j++){
		add(a[j], 'A');
		add(a[j+3], 'B');
	}
//	cout << A.sha << " " << A.shan << " " << A.tao << endl;
//	cout << B.sha << " " << B.shan << " " << B.tao << endl;
	while (A.health != 0 && B.health != 0){
		i++; //i即回合数 
		if (i % 2 != 0){ //为A的回合 
			set %= T;
			add(a[++set], 'A');
			if (A.tao != 0 && ta != A.health) heal('A');
			if (A.sha != 0) attack('A');
			set %= T;
			kkk++;
		} else { 
			set %= T;
			add(a[++set], 'B');
			if (B.tao != 0 && tb != B.health) heal('B');
			if (B.sha != 0) attack('B');
			set %= T;
		}
	}
	if (A.health == 0) cout << "BWIN! ";
		else cout << "AWIN! ";
	cout << kkk << endl;
	return 0;
}

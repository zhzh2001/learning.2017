#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;
int T, N, maxt, summ;
struct node{
	int x;
	int y;
	int num;
	bool operator <(const node& b) const{
		return num < b.num;
	};
}p[505];
int main(){
	freopen("Group.in", "r", stdin);
	freopen("Group.out", "w", stdout);
	cin >> T;
	while (T--){
		cin >> N;
		maxt = N;
		summ = 0;
		for (int i = 1; i <= N; i++) {
			cin >> p[i].x >> p[i].y;
			p[i].num = N - p[i].x - p[i].y;
			if (p[i].num <= 0) maxt--;
		}
		cout << maxt << endl;
	}
	return 0;
}

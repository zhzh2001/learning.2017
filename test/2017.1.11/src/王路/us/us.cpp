#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cstdlib>
using namespace std;
const string s = "Begin the Escape execution at the Break of Dawn";
string str;
int c;
int main(){
	freopen("us.in", "r", stdin);
	freopen("us.out", "w", stdout);
	getline(cin, str);
	int len = str.length();
	if (str[len-1] == ' ') str = str.substr(0, len-1), len--;
	if (s == str) cout << "1 0" << endl;
		else {
			while (str != s){
				int locC = str.find('C'), locO = str.find('O'), locW = str.find('W');
//				cout << locC << " " << locO << " " << locW << endl;
				if (locC == -1 || locO == -1 || locW == -1) {
					cout << "0 0\n"; 
					return 0;
				}
				string test = str.substr(0,locC) + str.substr(locO+1, locW-locO-1)
				+ str.substr(locC+1, locO-locC-1) + str.substr(locW+1, len-locW-1);
				str = test;
				c++;
				if (c > 8) {
					cout << "0 0" << endl;
					return 0;
				}
			}
			cout << "1 " << c << endl;	
		}
	return 0;
}

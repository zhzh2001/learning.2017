#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<string>
using namespace std;
string s,a,b,c,d;bool flag=false;int ll;
string cop(string a,int x,int y)
{
	string p=a;
	int k=p.size();
	if(x>0)p.erase(0,x);
	k=p.size();
	if(k-y>0)p.erase(y,k-y);
	return p;
}
void dfs(int p,string s,int l){
	if(flag)return;
	if(s=="Begin the Escape execution at the Break of Dawn"){
		cout<<1<<' '<<p;
		flag=true;
		return;
	}
	if(l<=47)return;
	if(s[0]!='B'&&s[0]!='C')return;
	for(int i=0;i<l;i++)if(s[i]=='C')
	    for(int j=i+1;j<l;j++)if(s[j]=='O')
	        for(int k=j+1;k<l;k++)if(s[k]=='W'){
	        	a=cop(s,0,i);b=cop(s,i+1,j-i-1);c=cop(s,j+1,k-j-1);d=cop(s,k+1,l-k-1);
	        	dfs(p+1,a+c+b+d,l-3);
	        }
}
int main()
{
	freopen("us.in","r",stdin);
	freopen("us.out","w",stdout);
	getline(cin,s);ll=s.size();
	while(s[ll-1]==' '){
		s.erase(ll-1,1);
		ll--;
	}
	bool rp=true;
	int kc=0,ko=0,kw=0;
	for(int i=0;i<ll;i++){
		if(s[i]=='C')kc++;
		if(s[i]=='O')ko++;
		if(s[i]=='W')kw++;
	}
	if(kc!=ko||ko!=kw||kc!=kw)rp=false;
	if(ll-kc-ko-kw!=47)rp=false;
	if(rp)dfs(0,s,ll);
	if(flag==false)cout<<"0 0";
	return 0;
}

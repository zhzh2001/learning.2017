#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
struct pp{
	int a,b;
}a[1001];int n,ans=0;
int p[501][501],r[501][501]={0},ma[502]={0};
bool cmp(pp a,pp b){
	if(a.a<b.a)return true;
	else if(a.a=b.a&&a.b<b.b)return true;
	return false;
}
void dfs(int x,int y,int sum){
	if(sum>ans)ans=sum;
	if(x==n)return;
	if(sum+ma[x+1]<=sum)return;
	for(int i=x+1;i<=n;i++){
	    if(i<n-y)continue;
	    for(int j=1;j<=p[i][0];j++){
	        if(p[i][j]+i+1>n)continue;
	        int jzq=y-(i-(n-y));
	        if(p[i][j]>jzq)continue;
	        dfs(i,p[i][j],sum+r[i][j]);
	    }
	}
}
int main()
{
	freopen("Group.in","r",stdin);
	freopen("Group.out","w",stdout);
	int t;cin>>t;
	for(int rp=1;rp<=t;rp++){
		cin>>n;
		memset(p,0,sizeof p);
		memset(r,0,sizeof r);
		memset(ma,0,sizeof ma);
		for(int i=1;i<=n;i++)cin>>a[i].a>>a[i].b;
		bool flag=false;
		sort(a+1,a+n+1,cmp);
		for(int i=1;i<=n;i++){
			for(int j=1;j<=p[a[i].a][0];j++)if(p[a[i].a][j]==a[i].b){
				r[a[i].a][j]++;
				ma[a[i].a]=max(ma[a[i].a],r[a[i].a][j]);
				flag=true;break;
			}
			if(flag)continue;
			p[a[i].a][0]++;
			ma[a[i].a]=max(ma[a[i].a],1);
			r[a[i].a][p[a[i].a][0]]=1;
			p[a[i].a][p[a[i].a][0]]=a[i].b;
		}
		for(int i=n;i>=0;i--)ma[i]+=ma[i+1];
		ans=0;
		dfs(-1,n+1,0);
		cout<<ans<<endl;
	}
	return 0;
}

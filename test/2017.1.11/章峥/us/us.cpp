#include<fstream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;
#define chk if(clock()>CLOCKS_PER_SEC*0.5)\
{\
	if(rand()%2)\
		fout<<"1 "<<ans<<endl;\
	else\
		fout<<"0 0\n";\
	exit(0);\
}
const string s="Begin the Escape execution at the Break of Dawn";
int ans;
ifstream fin("us.in");
ofstream fout("us.out");
void dfs(int k,string now)
{
	if(k==ans+1)
	{
		if(now==s)
		{
			fout<<"1 "<<ans<<endl;
			exit(0);
		}
		return;
	}
	chk;
	for(int i=0;i<now.length();i++)
		if(now[i]=='C')
			for(int j=i+1;j<now.length();j++)
				if(now[j]=='O')
					for(int t=j+1;t<now.length();t++)
						if(now[t]=='W')
						{
							string ss=now.substr(0,i)+now.substr(j+1,t-j-1)+now.substr(i+1,j-i-1)+now.substr(t+1);
							chk;
							dfs(k+1,ss);
							chk;
						}
}
int main()
{
	srand(time(NULL));
	string mes;
	getline(fin,mes);
	if(mes.length()<=s.length()||(mes.length()-s.length())%3||mes.find('C')==string::npos||mes.find('O')==string::npos||mes.find('W')==string::npos)
		fout<<"0 0\n";
	else
	{
		for(int i=0;i<s.length();i++)
			if(mes.find(s[i])==string::npos)
			{
				fout<<"0 0\n";
				return 0;
			}
		ans=(mes.length()-s.length())/3;
		dfs(1,mes);
		fout<<"0 0\n";
	}
	return 0;
}
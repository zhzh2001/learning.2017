#include<cstdio>
#include<cstring>
int T,n,x,y,c[505][505],f[505];
inline void Max(int &x,int y){if (y>x) x=y;}
int main()
{
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(c,0,sizeof(c));
		for (int i=0;i<n;i++){
			scanf("%d%d",&x,&y); if (x+y>=n) continue;
			y=n-y; if (c[x][y]==y-x) continue;
			c[x][y]++;
			}
		memset(f,0,sizeof(f));
		for (int i=0;i<n;i++){
			Max(f[i+1],f[i]);
			for (int j=i+1;j<=n;j++) if (c[i][j])
				Max(f[j],f[i]+c[i][j]);	
			}
		printf("%d\n",f[n]);
		}
	return 0;
}

#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Uniform(int l, int r) {
  return ((rand() << 15) + rand()) % (r - l + 1) + l;
}

int main() {
  srand(GetTickCount());
  int n = 100000, m = 200000;
  freopen("graph.in", "w", stdout);
  ios::sync_with_stdio(false);
  cout << n << ' ' << m << endl;
  for (int i = 1; i <= m; ++i) {
    int a = Uniform(1, n), b = Uniform(1, n - 1);
    if (a == b)
      ++b;
    cout << a << ' ' << b << endl;
  }
  return 0;
}

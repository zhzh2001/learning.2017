#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <stack>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

namespace IO {
  static const int SZ = 1 << 20;
  static FILE *fin = stdin, *fout = stdout;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, fin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    T flag(1);
    char ch;
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag; 
  }
}

using namespace IO;

const int N = 100005, M = 200005;
int n, m, head[N], h1[N], ind[N], ecnt, ec1;

struct Edge {
  int to, nxt;
} e[M << 1], e1[M << 1];

inline void addEdge(int u, int v) {
  e[++ecnt].to = v;
  e[ecnt].nxt = head[u]; 
  head[u] = ecnt;
}

inline void addEdge1(int u, int v) {
  e1[++ec1].to = v;
  e1[ec1].nxt = h1[u]; 
  h1[u] = ec1;
  ++ind[u];
}

bool vis[N];
int belong[N], dfn[N], low[N], ret, now, ans, pn;
stack<int> st;

void tarjan(int x, int fa = 0) {
  st.push(x);
  vis[x] = true;
  dfn[x] = low[x] = ++now;
  for (int i = head[x]; i; i = e[i].nxt) {
    int nxt = e[i].to;
    if (nxt == fa)
      continue;
    if (!vis[nxt]) {
      tarjan(e[i].to, x);
      low[x] = min(low[x], low[nxt]);
    } else
      low[x] = min(low[x], dfn[nxt]);
  }
  if (low[x] > dfn[fa]) {
    ++ans;
    ++pn;
    while (true) {
      int k = st.top();
      if (low[k] != low[x])
        break;
      belong[k] = pn;
      st.pop();
    }
//    fprintf(stderr, "%s:%d -> %d %d\n", __FUNCTION__, __LINE__, x, fa);
  }
}

int cc = 0;

void dfs(int x, int fa = 0) {
  bool flag = true;
  vis[x] = true;
  for (int i = h1[x]; i; i = e1[i].nxt) {
    if (e1[i].to != fa) {
      dfs(e1[i].to, x);
      flag = false;
    }
  }
  if (flag)
    ++cc;
}

int main() {
  freopen("graph.in", "r", stdin);
  freopen("graph.out", "w", stdout);
  read(n), read(m);
  for (int i = 1; i <= m; ++i) {
    int u, v;
    read(u), read(v);
    addEdge(u, v);
    addEdge(v, u);
  }
  int acc = 0;
  for (int i = 1; i <= n; ++i) {
    if (!dfn[i]) {
      tarjan(i); 
      ++acc;
    }
  }
  for (int j = 1; j <= n; ++j) {
    for (int i = head[j]; i; i = e[i].nxt) {
      if (belong[e[i].to] != belong[j]) {
        addEdge1(belong[j], belong[e[i].to]);
      }
    }
  }
  memset(vis, 0x00, sizeof vis);
  for (int j = 1; j <= pn; ++j) {
    if (ind[j] == 1)
      ++ret;
  }
  printf("%d\n", ret - acc);
  return 0;
}

#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Uniform(int l, int r) {
  return ((rand() << 15) + rand()) % (r - l + 1) + l;
}

int main() {
  freopen("math.in", "w", stdout);
  srand(GetTickCount());
  int T = 10;
  ios::sync_with_stdio(false);
  cout << T << endl;
  while (T--) {
    cout << Uniform(1, 1e9) << ' ' << Uniform(1, 20) << endl; 
  }
  return 0;
}

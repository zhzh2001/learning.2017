#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <cassert>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

namespace IO {
  static const int SZ = 1 << 20;
  static FILE *fin = stdin, *fout = stdout;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, fin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    T flag(1);
    char ch;
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag; 
  }
}

using namespace IO;

inline ll quickPow(ll a, ll b, ll p) {
  ll res = 1;
  for (; b; b >>= 1, a = a * a % p) {
    if (b & 1)
      res = res * a % p;
  }
  return res;
}

int main() {
  freopen("math.in", "r", stdin);
  freopen("math.out", "w", stdout);
#ifdef LOCAL_DEBUG
  freopen("test.res", "w", stderr);
#endif
  int T;
  read(T);
  while (T--) {
    int a, n;
    read(a), read(n);
    if (a & 1) {
      puts("1");
      continue; 
    }
    int ans = 0, tot = (1 << n), combo = 0, offset = 0, last = 0;
    a %= tot;
    for (int b = 2; b <= tot; b += 2) {
      if (quickPow(a, b, 1 << n) == quickPow(b, a, 1 << n)) {
        ++ans;
        if (b - last == offset)
          ++combo;
        else
          combo = 0;
        if (combo > 100) {
          ans += (tot - b) / offset;
          break;
        }
        offset = b - last;
        last = b;
      }
    }
    printf("%d\n", ans);
  }
  return 0;
}

#pragma GCC optimize("O2")
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <cassert>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

namespace IO {
  static const int SZ = 1 << 20;
  static FILE *fin = stdin, *fout = stdout;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, fin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    T flag(1);
    char ch;
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag; 
  }
}

using namespace IO;

inline ll quickPow(ll a, ll b, ll p) {
  ll res = 1;
  for (; b; b >>= 1, a = a * a % p) {
    if (b & 1)
      res = res * a % p;
  }
  return res;
}

int main() {
//  freopen("math.in", "r", stdin);
//  freopen("math.out", "w", stdout);
//  freopen("test.res", "w", stderr);
//  int T;
//  read(T);
  int a = -1;
  int tsc = 0;
  while (true) {
    a += 2;
    for (int n = 1; n <= 20; ++n) {
      int ans = 0, tot = (1 << n);
      for (int b = 1; b <= tot; ++b) {
        ans += (quickPow(a, b, 1 << n) == quickPow(b, a, 1 << n));
//        if ((quickPow(a, b, 1 << n) == quickPow(b, a, 1 << n)))
//          cerr << b << endl;
      }
      assert(ans == 1);
//      printf("%d\n", ans);
      cerr << "Done " << ++tsc << "!" << endl;
      break;
    }
  }
  return 0;
}

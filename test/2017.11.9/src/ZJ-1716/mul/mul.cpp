#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int, int> pii;

namespace IO {
  static const int SZ = 1 << 20;
  static FILE *fin = stdin, *fout = stdout;
  static char buf[SZ], *S = buf, *T = buf;
  inline char nc() {
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), SZ, fin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void read(T &x) {
    x = 0;
    T flag(1);
    char ch;
    for (ch = nc(); isspace(ch); ch = nc())
      ;
    if (ch == '-')
      ch = nc(), flag = -flag;
    for (; isdigit(ch); ch = nc())
      x = x * 10 + ch - '0';
    x *= flag; 
  }
}

using namespace IO;

const int N = 505, Mod = 1e9 + 7;
int n, K, pri[N], pcnt, pp[N], pn;
bool vis[N], dc[N];
bool cho[N][N];

void InitPrime() {
  for (int i = 2; i * i < N; ++i) {
    if (!vis[i]) {
      vis[i] = true;
      pri[++pcnt] = i;
      for (int j = i; j < N; j += i)
        vis[j] = true; 
    }
  }
  for (int i = 1; i <= pcnt; ++i) {
    int now = pri[i] * pri[i];
    for (int j = now; j < N; j += now) {
      dc[j] = true;
    }
  }
  for (int i = 1; i < N; ++i)
    for (int j = 1; j < N; ++j) {
      for (int k = 1; k <= pcnt; ++k) {
        if (i % pri[k] == 0 && j % pri[k] == 0)
          cho[i][j] = true;
      }
    }
}

int ans = 0;
int sel[N];

void work(int x, int k) {
  ++ans;
  if (k == K)
    return;
  for (int j = x + 1; j <= n; ++j) {
    if (dc[j])
      continue;
    bool fl = true;
    for (int p = 1; p <= k; ++p)
      if (cho[sel[p]][j]) {
        fl = false; break;
      }
    if (!fl)
      continue;
    sel[k + 1] = j;
    work(j, k + 1);
  }
}

int main() {
  freopen("mul.in", "r", stdin);
  freopen("mul.out", "w", stdout);
  int t;
  read(t);
  InitPrime();
  while (t--) {
    read(n), read(K); 
    ans = 0;
    for (int i = 1; i <= n; ++i) {
      if (!dc[i]) {
        sel[1] = i;
        work(i, 1); 
      }
    }
    printf("%d\n", ans % Mod);
  }
  return 0;
}

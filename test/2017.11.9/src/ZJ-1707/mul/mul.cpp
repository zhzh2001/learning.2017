#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define mo 1000000007
using namespace std;
int a[1000][1000];
ll f[1000][1000];
ll b[1000];
ll T,n,k,ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll gcd(ll x,ll y)
{
	if(y==0)return x;
	return gcd(y,x%y);
}
void dfs(int x)
{
	if(x==n+1)
	{
		ans++;
		ans%=mo;
		return;
	}
	dfs(x+1);
	int lzq=trunc(sqrt(x));
	if(lzq*lzq==x)return;
	For(i,1,b[0])
		if(a[x][b[i]]==0)return;
	b[0]++;
	b[b[0]]=x;
	if(b[0]==k)
	{
		ans++;
		b[0]--;
		return;
	}
	dfs(x+1);
	b[0]--;
}
int main()
{
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	memset(a,0,sizeof(a));
	For(i,1,500)
		For(j,i+1,500)
			if(gcd(i,j)==1)a[j][i]=a[i][j]=1;
	For(i,1,500)a[0][i]=a[i][0]=1;
	T=read();
	while(T--)
	{
		n=read();k=read();
		ans=0;
		dfs(2);
		k--;
		dfs(2);
		ans--;
		cout<<ans<<endl;
	}
	return 0;
}


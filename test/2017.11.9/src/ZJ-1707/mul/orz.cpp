#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	int f[1000];
	ll ans=0;
	For(i,2,500)
	{
		if(f[i]==1)continue;
		For(j,2,500/i)
			f[j*i]=1;
		ans++;
	}
	cout<<ans<<endl;
	return 0;
}


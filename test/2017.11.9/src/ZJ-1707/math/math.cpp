#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int T;
ll a,n,xx,ans,jzq,lzq;
ll ksm(ll x,ll y,ll mo)
{
	ll faq=x,mmp=y;
	ll fh=1;
	while(mmp>0)
	{
		if(mmp%2==1)fh=(fh*faq)%mo;
		faq=(faq*faq)%mo;
		mmp/=2;
	}
	return fh;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	T=read();
	while(T--)
	{
		a=read();n=read();
		ans=0;
		xx=1;
		For(i,1,n)xx*=2;
		For(i,1,xx)
		{
			jzq=ksm(a,i,xx);
			lzq=ksm(i,a,xx);
			if(jzq==lzq)ans++;
		}
		cout<<ans<<endl;
	}
	return 0;
}


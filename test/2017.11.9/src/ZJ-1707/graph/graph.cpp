#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,m;
ll ans;
ll f[100100];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		ll x=read(),y=read();
		f[x]++;f[y]++;
	}
	For(i,1,n)
		if(f[i]<2)ans+=(2-f[i]);
	ans++;
	ans/=2;
	cout<<ans<<endl;
	return 0;
}


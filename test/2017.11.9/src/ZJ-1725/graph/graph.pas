program graph;
 var
  d:array[0..100001] of longint;
  i,m,n,sum,x,y:longint;
 begin
  assign(input,'graph.in');
  assign(output,'graph.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  fillchar(d,sizeof(d),0);
  for i:=1 to m do
   begin
    readln(x,y);
    inc(d[x]);
    inc(d[y]);
   end;
  sum:=0;
  for i:=1 to n do
   if d[i]=1 then inc(sum);
  writeln((sum+1) div 2);
  close(input);
  close(output);
 end.

#include<cstdio>
#include<algorithm>
using namespace std;
const int N=2e5+3;
int i,j,n,m,t=1,num,d[N],fa[N],en[N],nxt[N],id[N],dfn[N],low[N],st[N];
void dfs(int u,int p)
{
	low[u]=dfn[u]=++t,st[++*st]=u;
	for(int i=fa[u],v;i;i=nxt[i])if(i>>1!=p)
	{
		if(dfn[v=en[i]])low[u]=min(low[u],dfn[v]);else
		dfs(v,i>>1),low[u]=min(low[u],low[v]);
	}
	if(low[u]==dfn[u])
	{
		++num;do
		id[st[*st]]=num;
		while(st[(*st)--]!=u);
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	for(scanf("%d%d",&n,&m);m--;)
	{
		scanf("%d%d",&i,&j);
		en[++t]=j,nxt[t]=fa[i],fa[i]=t;
		en[++t]=i,nxt[t]=fa[j],fa[j]=t;
	}
	dfs(1,0);
	for(i=2;en[i];i+=2)
		if(id[en[i]]!=id[en[i+1]])
			++d[id[en[i]]],++d[id[en[i+1]]];
	for(m=i=0;i++<num;m+=d[i]<2);
	printf("%d",num>1?m+1>>1:0);
}

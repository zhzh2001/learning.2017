#include<cstdio>
int T,a,b,n,M,ans;
int Pow(int a,int b)
{
	int r=1;
	for(;b;b>>=1,a*=a)
		if(b&1)r*=a;
	return r;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math_.out","w",stdout);
	for(scanf("%d",&T);T--;)
	{
		scanf("%d%d",&a,&n);
		for(ans=0,M=b=1<<n;b;--b)
			if((Pow(a,b)&(M-1))==(Pow(b,a)&(M-1)))++ans;//,printf("%d ",b);
		printf("%d\n",ans);
	}
}

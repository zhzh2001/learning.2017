#include<cstdio>
int T,i,a,b,n,M,ans;
int Pow(int a,int b)
{
	int r=1;
	for(;b;b>>=1,a*=a)
		if(b&1)r*=a;
	return r;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	for(scanf("%d",&T);T--;)
	{
		scanf("%d%d",&a,&n);
		if(a&1){puts("1");continue;}
		if(a==2&&n>7){printf("%d\n",(1<<(n/2))+2);continue;}
		for(M=b=1<<n;--b;)
			if((Pow(a,b)&(M-1))==(Pow(b,a)&(M-1)))break;
		b=M-b,ans=M/b+(a%b>0);
		for(i=1;i<=n;++i)if(i!=a)
			if(i%b)
			{
				if((Pow(a,i)&(M-1))==(Pow(i,a)&(M-1)))++ans;
			}else
			if((Pow(a,i)&(M-1))!=(Pow(i,a)&(M-1)))--ans;
		printf("%d\n",ans);
	}
}

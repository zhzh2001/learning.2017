#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
const int N=305;
const int p=1e9+7;
ll a[N],ans,n,m;
void dfs(int t,int k,ll now)
{
	if(t!=0){
		ans++;
		if(ans>1e18)ans%=p;
		if(t==m)return;
	}
	for(int i=k+1;i<=n;i++)
		if(a[i]!=-1&&((1<<i-1)&now)==0){
			dfs(t+1,i,now|a[i]);
		}
}
int main()
{
	freopen("mul.in","r",stdin);
	freopen("mul2.out","w",stdout);
	int T=read();
	for(int i=1;i<=30;i++){
		for(int j=i+1;j<=30;j++){
			int x=i*j;
			for(int k=2;k*k<=x;k++)
				if(x%k==0&&(x/k)%k==0)
					a[i]|=(1<<j-1),a[j]|=(1<<i-1);
		}
		for(int k=2;k*k<=i;k++)
			if(i%k==0&&i/k%k==0)
				a[i]=-1;
	}
	while(T--){
		ans=0;
		n=read();m=read();
		dfs(0,0,0);
		writeln(ans%p);
	}
	return 0;
}

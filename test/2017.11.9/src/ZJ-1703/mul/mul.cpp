#include<bits/stdc++.h>
using namespace std;
const int N=505;
const int M=(1<<8);
const int mod=1e9+7;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
struct xxx{
	int val,k;
	bool can;
}a[N];
ll f[N][M];
int p[8]={2,3,5,7,11,13,17,19};
int n,m;
int main()
{
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	int T=read();
	while(T--){
		n=read();m=read();
		for(int i=2;i<=n;i++){
			int k=i;
			a[i].can=false;
			for(int j=0;j<8;j++){
				if(k%p[j]==0)a[i].k|=(1<<j),k/=p[j];
				if(k%p[j]==0)a[i].can=true;
			}
		}
		memset(f,0,sizeof(f));
		f[0][0]=1;
		f[1][0]=1;
		for(int j=2;j<=n;j++){
			for(int i=m-1;i>=0;i--){
				if(!a[j].can)
					for(int k=0;k<M;k++)
						if(f[i][k]&&(a[j].k&k)==0){
							f[i+1][k|a[j].k]+=f[i][k];
							if(f[i+1][k|a[j].k]>1e18)f[i+1][k|a[j].k]%=mod;
						}
			}
		}
		ll ans=0;
		for(int i=1;i<=m;i++)
			for(int j=0;j<M;j++){
				ans=(ans+f[i][j]);
				if(ans>1e18)ans%=mod;
			}
		writeln(ans%mod);
	}
	return 0;
}

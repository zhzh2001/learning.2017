#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=100005;
const int M=400005;
int dfn[N],low[N],c[N];
int head[N],tail[M],nxt[M],from[M],s[N];
int t,n,m,x,y,col,top;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
int f[N],vis[N];
bool vist[N];
void tarjan(int k,int fa)
{
	dfn[k]=++t;low[k]=t;
	s[++top]=k;
	for(int i=head[k];i;i=nxt[i]){
		if(tail[i]==fa)continue;
		int x=tail[i];
		if(!dfn[x]){
			tarjan(x,k);
			low[k]=min(low[k],low[x]);
		}
		else low[k]=min(low[k],low[x]);
	}
	if(dfn[k]==low[k]){
		c[s[top]]=++col;
		top--;
		if(!c[k]){
			while(s[top]!=k&&top){
				c[s[top]]=col;
				top--;
			}
			c[s[top]]=col;
			top--;
		}
	}
}
void addto(int x,int y)
{
	nxt[++t]=head[x];
	from[t]=x;
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int fa)
{
	vist[k]=1;
	for(int i=head[k];i;i=nxt[i]){
		if(tail[i]==fa)continue;
		int x=tail[i];vis[k]++;
		dfs(x,k);
		if(vis[x]%2)f[x]--;
		f[k]+=f[x];
	}
	f[k]+=(vis[k]/2)+(vis[k]%2);
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph2.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		x=read();y=read();
		addto(x,y);
		addto(y,x);
	}
	t=0;col=0;
	tarjan(1,0);
	memset(head,0,sizeof(head));
	memset(nxt,0,sizeof(nxt));
	t=0;
	for(int i=1;i<=m*2;i++){
		x=c[from[i]];y=c[tail[i]];
		if(x!=y)addto(x,y);
	}
	dfs(1,0);
	printf("%d",f[1]);
	return 0;
}
/*
in:
8 7
1 2
2 3
3 6
3 8
2 7
2 4
4 5
out:
3
*/

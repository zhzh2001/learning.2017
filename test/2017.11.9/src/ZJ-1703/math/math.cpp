#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline void write(ll x){if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
int ans,a,n,x,y;
ll qpow(ll x,ll y,ll p)
{
	int ret=1;
	for(;y;y>>=1){
		if(y&1)ret=ret*x%p;
		x=x*x%p;
	}
	return ret%p;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int T=read();
	while(T--){
		a=read();n=read();
		ll p=pow(2,n);
		ans=0;
		a%=p;
		for(int i=1;i<=p;i++){
			x=qpow(a,i,p);y=qpow(i,a,p);
			if(x==y)ans++;
		}
		writeln(ans);
	}
}

#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=505,NN=100,M=1<<8,mo=1e9+7;
int n,nn,na,nm,m,i,j,k,ans;
struct arr{int x,k;}a[N];
bool operator < (arr A,arr B){return A.x<B.x;}
int t,p[N],mn[N],f[NN][M],g[NN][M];
bool can[N];
void init(){
	for (can[1]=1,i=2;i<N;i++){
		if (!mn[i]) p[++t]=i,mn[i]=t,can[i]=1;
		for (j=1;i*p[j]<N;j++){
			int k=i*p[j];mn[k]=j;
			if (i%p[j]==0){can[k]=0;break;}
			can[k]=can[i];
		}
	}
}
void add(int &x,int k){x=(x+k)%mo;}
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	init();f[0][0]=1;
	int TEST;scanf("%d",&TEST);
	for (;TEST--;){
		scanf("%d%d",&n,&m);nn=sqrt(n);
		for (i=1;p[i]<=n;i++);m=min(m,i);
		for (na=0,i=1;i<=n;i++) if (can[i]){
			int k=i,ak=0;
			for (j=1;p[j]<=nn;j++) if (k%p[j]==0)
				k/=p[j],ak|=1<<j-1;
			a[++na]=(arr){k,ak};
		}
		for (i=1;p[i]<=nn;i++);i--;nm=1<<i;
		sort(a+1,a+na+1);
		for (int x=1;x<=na;x++){
			if (a[x].x!=a[x-1].x||a[x].x==1)
				for (i=min(x-1,m);i;i--) for (j=nm;j--;)
					add(f[i][j],g[i][j]),g[i][j]=0;
			for (k=a[x].k,i=min(x,m)-1;i>=0;i--) for (j=nm;j--;)
				if (!(j&k)) add(g[i+1][j^k],f[i][j]);
		}
		for (ans=0,i=m;i;i--) for (j=nm;j--;)
			add(ans,(f[i][j]+g[i][j])%mo),f[i][j]=g[i][j]=0;
		printf("%d\n",ans);
	}
}

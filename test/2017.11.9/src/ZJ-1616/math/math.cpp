#include<cstdio>
#define ll long long
int n,m,mo,nm,i,ans;
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int TEST;scanf("%d",&TEST);
	for (;TEST--;){
		scanf("%d%d",&n,&m);mo=1<<m;
		if (n&1){puts("1");continue;}
		nm=mo<30?mo:30;ans=0;
		for (i=1;i<=nm;i++)
			if (Pow(n,i)==Pow(i,n)) ans++; 
		int k=(m-1)/n+1;k=1<<k;
		ans+=mo/k-nm/k;
		printf("%d\n",ans);
	}
}

#include<cstdio>
#define ll long long
int n,m,mo,i,ans;
int Pow(int x,int n){
	int k=1;
	for (;n;n>>=1,x=(ll)x*x%mo)
		if (n&1) k=(ll)k*x%mo;
	return k;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("bl.out","w",stdout);
	int TEST;scanf("%d",&TEST);
	for (;TEST--;){
		scanf("%d%d",&n,&m);mo=1<<m;ans=0;
		for (i=1;i<=mo;i++)
			if (Pow(n,i)==Pow(i,n)) ans++;
		printf("%d\n",ans);
	}
}

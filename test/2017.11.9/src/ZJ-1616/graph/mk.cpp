#include<cstdio>
#include<ctime>
#include<algorithm>
#include<bitset>
using namespace std;
const int N=1e5+5;
int n,m;
bitset<N> a[N];
int ran(){
	int x=(rand()<<15)+rand();
	return x%n+1;
}
int main(){
	freopen("graph.in","w",stdout);
	srand(time(0));rand();
	n=1e5;m=2e5;
	printf("%d %d\n",n,m);
	m-=n-1;
	for (int i=2;i<=n;i++){
		int y=(rand()<<15)+rand();
		y=y%(i-1)+1;a[i][y]=a[y][i]=1;
		printf("%d %d\n",i,y);
	}
	for (;m--;){
		int x,y;
		while (x=ran(),y=ran(),a[x][y]);
		a[x][y]=a[y][x]=1;
		printf("%d %d\n",x,y);
	}
}

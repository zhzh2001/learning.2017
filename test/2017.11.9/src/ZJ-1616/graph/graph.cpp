#include<cstdio>
#include<algorithm>
using namespace std;
const int N=1e5+5,M=4e5+5;
int n,m,i,et,he[N],d[N],ans;
int cnt,dfn[N],low[N],top,stk[N],bl[N];
struct edge{int l,to;}e[M];
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void tarjan(int x,int fa){
	dfn[x]=low[x]=++cnt;stk[++top]=x;
	for (int i=he[x];i;i=e[i].l){
		int y=e[i].to;if (y==fa) continue;
		if (!dfn[y]) tarjan(y,x);
		low[x]=min(low[x],low[y]);
	}
	if (low[x]==dfn[x]){
		for (;stk[top]!=x;top--)
			bl[stk[top]]=x;
		bl[x]=x;top--;
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	for (n=read(),m=read();m--;){
		int x=read(),y=read();
		e[++et].l=he[x];he[x]=et;e[et].to=y;
		e[++et].l=he[y];he[y]=et;e[et].to=x;
	}
	tarjan(1,0);
	for (i=1;i<=et;i+=2){
		int x=bl[e[i].to],y=bl[e[i+1].to];
		if (x!=y) d[x]++,d[y]++;
	}
	for (i=1;i<=n;i++) if (d[i]==1) ans++;
	printf("%d",ans+1>>1);
}

#include<bits/stdc++.h>
#define mo 1000000007
using namespace std;
const int pri[]={2,3,5,7,11,13,17,19};
int T,n,k,tot,ans,f[505][300];
struct jdb{int p,s;}a[505];
bool cmp(jdb a,jdb b){
	return a.p<b.p;
}
void add(int &x,int y){
	x=(x+y)%mo;
}
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d",&n,&k);
		tot=0;
		for (int i=1;i<=n;i++){
			int tmp=i,num=0,fl=0;
			for (int j=0;j<8;j++)
				if (tmp%pri[j]==0){
					if (tmp%(pri[j]*pri[j])==0)
						fl=1;
					num|=(1<<j);
					for (;tmp%pri[j]==0;tmp/=pri[j]);
				}
			if (fl) continue;
			a[++tot]=(jdb){tmp,num};
		}
		sort(a+1,a+tot+1,cmp);
		memset(f,0,sizeof(f));
		f[0][0]=1; k=min(k,tot);
		for (int i=1,j;i<=tot;i=j){
			for (j=i;j<=tot&&a[j].p==a[i].p;j++);
			if (a[i].p==1){
				for (int p=i;p<j;p++)
					for (int q=k;q;q--)
						for (int tmp=255-a[p].s,r=tmp;;r=(r-1)&tmp){
							add(f[q][a[p].s|r],f[q-1][r]);
							if (!r) break;
						}
			}
			else{
				for (int q=k;q;q--)
					for (int p=i;p<j;p++)
						for (int tmp=255-a[p].s,r=tmp;;r=(r-1)&tmp){
							add(f[q][a[p].s|r],f[q-1][r]);
							if (!r) break;
						}
			}
		}
		ans=0;
		for (int i=1;i<=k;i++)
			for (int j=0;j<1<<8;j++)
				add(ans,f[i][j]);
		printf("%d\n",ans);
	}
}

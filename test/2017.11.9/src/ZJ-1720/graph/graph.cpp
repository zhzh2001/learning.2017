#include<bits/stdc++.h>
#define N 100005
using namespace std;
namespace cogito{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
}
using namespace cogito;
struct edge{int to,next,id;}e[N*4];
struct E{int x,y;}a[N*2];
int head[N],fa[N][17],dep[N];
int on[N*2],deg[N],v[N],f[N],vis[N];
int n,m,x,y,tot,ans;
void add(int x,int y,int id){
	e[++tot]=(edge){y,head[x],id};
	head[x]=tot;
}
void dfs1(int x){
	vis[x]=1;
	//printf("%d\n",x);
	for (int i=head[x];i;i=e[i].next)
		if (!vis[e[i].to]){
			fa[e[i].to][0]=x;
			dep[e[i].to]=dep[x]+1;
			on[e[i].id]=1;
			dfs1(e[i].to);
		}
}
void dfs2(int x){
	for (int i=head[x];i;i=e[i].next)
		if (fa[e[i].to][0]==x)
			dfs2(e[i].to),v[x]+=v[e[i].to];
}
int lca(int x,int y){
	if (dep[x]<dep[y]) swap(x,y);
	int tmp=dep[x]-dep[y];
	for (int i=0;i<=16;i++,tmp/=2)
		if (tmp&1) x=fa[x][i];
	for (int i=16;i>=0;i--)
		if (fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	return x==y?x:fa[x][0];
}
int get(int x){
	return f[x]==x?x:f[x]=get(f[x]);
}
void merge(int x,int y){
	x=get(x); y=get(y);
	if (x!=y) f[x]=y;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=m;i++){
		x=read(); y=read();
		add(x,y,i); add(y,x,i);
		a[i].x=x; a[i].y=y;
	}
	dfs1(1);
	for (int i=1;i<=16;i++)
		for (int j=1;j<=n;j++)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	for (int i=1;i<=m;i++)
		if (!on[i]){
			int d=lca(a[i].x,a[i].y);
			v[a[i].x]++; v[a[i].y]++;
			v[d]-=2;
		}
	dfs2(1);
	for (int i=1;i<=n;i++)
		f[i]=i;
	for (int i=2;i<=n;i++)
		if (v[i]) merge(i,fa[i][0]);
	for (int i=1;i<=m;i++)
		if (on[i]){
			int x=get(a[i].x);
			int y=get(a[i].y);
			if (x!=y){
				deg[x]++;
				deg[y]++;
			}
		}
	for (int i=1;i<=n;i++)
		if (deg[i]==1) ans++;
	printf("%d",(ans+1)/2);
}

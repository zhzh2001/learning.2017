#include<bits/stdc++.h>
#define N 100005
using namespace std;
int rnd(){
	int x=0;
	for (int i=1;i<=9;i++)
		x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("graph.in","w",stdout);
	printf("%d %d\n",100000,101000);
	for (int i=2;i<=100000;i++)
		printf("%d %d\n",i,rnd()%(i-1)+1);
	for (int i=100000;i<=101000;i++){
		int l=rnd()%100000+1,r=rnd()%100000+1;
		for (;l==r;l=rnd()%100000+1,r=rnd()%100000+1);
		printf("%d %d\n",l,r);
	}
}

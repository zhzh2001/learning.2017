#include<bits/stdc++.h>
#define ll long long
using namespace std;
int power(int x,int y,int mo){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int T,a,n;
int main(){
	freopen("math.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d",&a,&n);
		int ans=0;
		for (int i=1;i<=1<<n;i++)
			if (power(a,i,1<<n)==power(i,a,1<<n))
				ans++;
		printf("%d\n",ans);
	}
}

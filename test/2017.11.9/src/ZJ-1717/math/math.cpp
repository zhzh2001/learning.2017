#include <cstdio>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}

int mod;
int b[50],now;

inline int ksm(int x,int y,int mod) {
	now = 0;
	while(y) {
		b[++now] = y&1;
		y=y>>1;
	}
	int ans = 1;
	Dow(i, now, 1) {
		ans = 1ll*ans*ans%mod;
		if(b[ i ]) ans = 1ll*ans*x%mod;
	}
	return ans;
}

int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout); 
	int T=read();
	while(T--) {
		int a = read(), n = read();
		int sum = 0;
		mod = 1<<n;
		a%=mod; 
		For(b, 1, 1<<n) 
			if(ksm(a,b,mod)==ksm(b,a,mod)) sum++; 
		printf("%d\n",sum);
	}
	return 0;
}









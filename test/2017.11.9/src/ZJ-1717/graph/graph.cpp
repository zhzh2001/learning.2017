#include <cstdio>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
const int N = 100011, M = 200011; 
struct node{ 
	int to,pre;
}e[M*2];
int dfn[N],low[N],head[N],vis[N],stk[N],top;
int n,m,Ti,tot,cnt,leaf,ans;
int w,kkk;

inline void add(int x,int y) {
	e[++cnt].to = y;
	e[cnt].pre = head[x];
	head[x] = cnt;
}

inline void tarjan(int u,int fa,int root) {
	int sum = 0;
	vis[ u ] = 1;
	dfn[ u ] = low[ u ] = ++Ti;
	stk[++top] = u;
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to;
		if(v==fa) continue;
		if(vis[v]==2) continue;
		if(vis[v]==0) {
			tarjan(v,u,0);
			low[u] = min(low[u],low[v]);
			++sum;
		} 
		else {
			low[u] = min(low[u],dfn[v]);
		}
	}
	if(root && sum<2 ) kkk++;
	else if(!root && sum==0) kkk++;
	
	if(low[u]==dfn[u]) {
		w++;
		while(1) {
			vis[stk[top]] = 2;
			if(stk[top]==u) break;
			top--;
		}
		top--;
	}
}

inline void work(int u) {
	w = 0; kkk = 0;
	tarjan(u,-1,1); tot++; 
//	cout<<w<<endl;
	if(w==1) 
		kkk = 0;
	leaf+=kkk;
}

int main() {
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n = read(); m = read();
	For(i, 1, m) {
		int x=read(), y=read();
		add(x,y); add(y,x);
	}
	For(i, 1, n) {
		if(!vis[ i ]) work(i);
	}
	
//	printf("%d %d\n",tot,leaf);
	if(tot==1 && leaf==0 ) {
		puts("0");
		exit(0);
	}
	if(tot!=1) {
		leaf = max( 0,leaf-(2*tot-2) );
		ans+=tot-1;
	}
	if(leaf) {
		ans+=leaf/2;
		if(leaf&1) ans++;
	}
	printf("%d\n",ans);
	
	return 0;
}



/*

7 7
1 2
2 3
3 4
2 5
4 5
5 6
5 7

5 5
1 2
2 3
3 4
4 5 
5 1

5 4
1 2
2 3 
3 4
4 5

6 0





*/







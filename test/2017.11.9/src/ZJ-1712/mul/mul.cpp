#include<bits/stdc++.h>
#define mod(x) x>=lsg?x-lsg:x
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1,c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return k*kk;
}using namespace std;
int c[8]={2,3,5,7,11,13,17,19};
int t,lsg,n,kk,p[200],d,f[110][256],g[110][256],dd,ans;
inline bool pd(int x,int y){
	long long sum=y;for (int i=0;i<=7;i++){
		if (x&(1<<i))sum*=c[i];
		if (sum>n)return 0;
	}return 1;
}inline bool pd(int x){for (int i=2;i*i<=x;i++)if (x%i==0)return false;return true;}
int main(){
	freopen("mul.in","r",stdin);freopen("mul.out","w",stdout);
	t=read();lsg=1e9+7;for (int i=20;i<=500;i++)if (pd(i))p[++d]=i;
	while (t--){
		n=read();kk=read();kk=min(kk,100);memset(f,0,sizeof(f));f[0][0]=1;
		for (int i=0;i<256;i++)
			if (pd(i,1))for (int j=kk-1;j>=0;j--){
				dd=255^i;for (int k=dd;;k=(k-1)&dd){
					f[j+1][k+i]+=f[j][k];f[j+1][k+i]=mod(f[j+1][k+i]);
					if (k==0)break;
				}
			}ans=0;
		for (int i=1;i<=d&&p[i]<=n;i++){
			memset(g,0,sizeof(g));
			for (int ii=0;ii<256;ii++)if (pd(ii,p[i]))
				for (int j=kk-1;j>=0;j--){
					dd=255^ii;for (int k=dd;;k=(k-1)&dd){
						g[j+1][k+ii]+=f[j][k];g[j+1][k+ii]=mod(g[j+1][k+ii]);
						if (k==0)break;
					}
				}
			for (int i=1;i<=kk;i++)
				for (int j=0;j<256;j++)f[i][j]+=g[i][j],f[i][j]=mod(f[i][j]);
		}for (int i=1;i<=kk;i++)
			for (int j=0;j<256;j++)(ans+=f[i][j])%=lsg;
		cout<<ans<<endl;
	}
}

#pragma GCC optimize("O2")
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=505,mod=1e9+7;
int n,k;
inline void init(){
	scanf("%d%d",&n,&k);
}
bool flag[maxn];
int tot,pri[maxn];
inline void prepare(){
	for (int i=2;i<maxn;i++){
		if (!flag[i]){
			pri[++tot]=i;
		}
		for (int j=1;pri[j]*i<maxn;j++){
			flag[pri[j]*i]=1;
			if (i%pri[j]==0){
				break;
			}
		}
	}
}
struct num{
	int state,opt;
}a[maxn];
int cnt;
inline int sqr(int x){return x*x;}
inline void pre(){
	cnt=0;
	for (int i=1;i<=n;i++){
		int temp=i,s=0; bool can=1;
		for (int j=1;j<=tot&&sqr(pri[j])<=n;j++){
			if (temp%pri[j]==0){
				int cnt=0;
				while (temp%pri[j]==0) temp/=pri[j],cnt++;
				if (cnt>=2){
					can=0;
					break;
				}
				s|=(1<<(j-1));
			}
		}
		if (can){
			a[++cnt].state=s; a[cnt].opt=temp;
		}
	}
}
inline bool cmp(num a,num b){
	return a.opt<b.opt;
}
int dp[2][maxn][1<<8],f[maxn][1<<8];
inline void solve(){
	pre();
	sort(a+1,a+1+cnt,cmp);
	memset(dp,0,sizeof(dp)); a[cnt+1].opt=10000;
	int cur=0; dp[0][0][0]=1;
	for (int i=1;i<=cnt;i++){
		if (a[i].opt!=a[i-1].opt||a[i].opt==1){
			memset(f,0,sizeof(f));
		}
		for (int j=1;j<=k;j++){
			for (int s=0;s<(1<<8);s++){
				if ((s&a[i].state)==0){
					(f[j][s|a[i].state]+=dp[cur][j-1][s])%=mod;
				}
			}
		}
		if (a[i].opt!=a[i+1].opt||a[i].opt==1){
			for (int j=0;j<=k;j++){
				for (int s=0;s<(1<<8);s++){
					(dp[cur^1][j][s]=dp[cur][j][s]+f[j][s])%=mod;
				}
			}
			memset(dp[cur],0,sizeof(dp[cur]));
			cur^=1;
		}
	}
	int ans=0;
	for (int i=1;i<=k;i++){
		for (int j=0;j<(1<<8);j++){
			(ans+=dp[cur][i][j])%=mod;
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	int T;
	scanf("%d",&T);
	prepare();
	while (T--){
		init();
		solve();
	}
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=505,limit=50,mod=1e9+7;
typedef long long ll;
int n,k;
inline void init(){
	scanf("%d%d",&n,&k);
}
int pri[maxn],tot;
bool flag[maxn];
inline void prepare(){
	for (int i=2;i<=limit;i++){
		if (!flag[i]){
			pri[++tot]=i;
		}
		for (int j=1;pri[j]*i<=limit;j++){
			flag[pri[j]*i]=1;
			if (i%pri[j]==0){
				break;
			}
		}
	}
}
int num[maxn],state[maxn],cnt;
inline int sqr(int x){return x*x;}
inline void pre(){
	cnt=0;
	for (int i=1;i<=n;i++){
		int temp=i,s=0; bool can=1;
		for (int j=1;j<=tot;j++){
			if (temp%pri[j]==0){
				int cnt=0;
				while (temp%pri[j]==0) temp/=pri[j],cnt++;
				if (cnt>=2){
					can=0;
					break;
				}
				s|=(1<<(j-1));
			}
		}
		if (can) state[++cnt]=s,num[cnt]=i;
	}
}
int dp[limit][limit][(1<<15)+10];
inline void solve(){
	pre();
	memset(dp,0,sizeof(dp));
	dp[0][0][0]=1;
	for (int i=1;i<=cnt;i++){
		for (int j=0;j<=k;j++){
			for (int s=0;s<(1<<tot);s++){
				(dp[i][j][s]+=dp[i-1][j][s])%=mod;
				if ((s&state[i])==0){
					(dp[i][j+1][s|state[i]]+=dp[i-1][j][s])%=mod;
				}
			}
		}
	}
	ll ans=0;
	for (int j=1;j<=k;j++){
		for (int i=0;i<(1<<tot);i++){
			(ans+=dp[cnt][j][i])%=mod;
		}
	}
	printf("%lld\n",ans%mod);
}
int main(){
	freopen("mul.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int T;
	scanf("%d",&T);
	prepare();
	while (T--){
		init();
		solve();
	}
	return 0;
}

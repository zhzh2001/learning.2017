#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=100005,maxm=200005;
struct edge{
	int link,next;
}e[maxm<<1];
int n,m,head[maxn],tot;
inline void insert(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void ins(int u,int v){
	insert(u,v); insert(v,u);
}
inline void init(){
	n=read(); m=read();	
	for (int i=1;i<=m;i++){
		int u=read(),v=read();
		ins(u,v); 
	}
}
bool instack[maxn];
int tim,dfn[maxn],low[maxn],top,belong[maxn],scc,stack[maxn];
inline void pop(){
	instack[stack[top]]=0; belong[stack[top--]]=scc;
}
void tarjan(int u,int fa){
	dfn[u]=low[u]=++tim; stack[++top]=u; instack[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			if (!dfn[v]){
				tarjan(v,u);
				low[u]=min(low[u],low[v]);
			}else{
				if (instack[v]){
					low[u]=min(low[u],dfn[v]);
				}
			}
			if (low[v]>dfn[u]){
				scc++;
				while (top&&stack[top]!=v) pop();
				pop();
			}
		}else{
			fa=0;
		}
	}
}
int fa[maxn],du[maxn];
int getfa(int x){return (fa[x]==x)?x:fa[x]=getfa(fa[x]);}
inline void rebuild(){
	for (int i=1;i<=scc;i++) fa[i]=i;
	tot=0;
	for (int i=1;i<=n;i++){
		int u=belong[i];
		for (int j=head[i];j;j=e[j].next){
			int v=belong[e[j].link];
			int p=getfa(u),q=getfa(v);
			if (p!=q){
				du[u]++;
				du[v]++;
				fa[p]=q;
			}
		}
	}
}
inline void solve(){
	tarjan(1,0); 
	if (top){
		scc++;
		while (top) pop();
	}
	rebuild();
	int ans=0;
	for (int i=1;i<=scc;i++){
		if (du[i]==1) ans++;
	}
	printf("%d\n",ans-1);
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<cstdio>
using namespace std;
typedef long long ll;
int mod;
inline ll power(ll x,ll k){
	x%=mod; ll y=1;
	for (;k;k>>=1,x=x*x%mod) if (k&1) y=y*x%mod;
	return y;
}
int a,n;
inline void init(){
	scanf("%d%d",&a,&n);
}
inline void solve(){
	mod=1<<n; int ans=0;
	for (int i=1;i<=mod;i++){
		if (power(a,i)==power(i,a)){
			ans++;
		}
	}
	printf("%d\n",ans);
}
int T;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		init();
		solve();
	}
	return 0;
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<bitset>
#include<vector>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=1e5+5,M=2e5+5;
struct E{int next,to;} edge[M<<1];
int s[N],top,tot,d[N],poi,x[M],y[M],n,m,Res;
int nedge,head[N],low[N],dfn[N],f[N],F[N],bel[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int addline(int x,int y){
	edge[++nedge].to=y,edge[nedge].next=head[x],head[x]=nedge;
}
int Tarjan(int u,int fa){s[++top]=u;
	f[u]=F[u]=1,low[u]=dfn[u]=++tot;
	for (int i=head[u];i;i=NX){
		if (O==fa) continue;
		if (!f[O]) Tarjan(O,u),low[u]=min(low[u],low[O]);
		else if (F[O]) low[u]=min(low[u],dfn[O]);
	}
	if (low[u]==dfn[u])
		for (poi++;s[top+1]!=u;)
			bel[s[top]]=poi,F[s[top--]]=0;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=Read(),m=Read();
	for (int i=1;i<=m;i++){
		x[i]=Read(),y[i]=Read();
		addline(x[i],y[i]),addline(y[i],x[i]);
	}
	for (int i=1;i<=n;i++)
		if (!f[i]) Tarjan(i,0);
	for (int i=1;i<=m;i++)
		if (bel[x[i]]!=bel[y[i]])
			d[bel[x[i]]]++,d[bel[y[i]]]++;
	for (int i=1;i<=poi;i++) 
		if (d[i]==1) Res++;
	printf("%d\n",(Res+1)/2);
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<bitset>
#include<vector>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=2005,M=35,Mod=1e9+7; int flag,Res;
int n,k,Num,num[M],f[M][N],tot,pri[N],cnt,vis[N],fac[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int power(int x,int y){
	if (!y) return 1; int t=power(x,y/2);
	return 1ll*t*t%Mod*(y&1?x:1)%Mod;
}
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	for (int T=Read();T--;){
		n=Read(),k=Read(); tot=0;
		for (int i=2;i<=n;i++)
			if (!vis[i]){pri[++tot]=i;
				for (int j=i+i;j<=n;j+=i) vis[j]=1;
			}
		k=min(k,n);
		if (tot>10){
			printf("%d\n",power(2,tot)); continue;
		}
		memset(f,0,sizeof(f)); f[0][0]=1; fac[0]=1;
		for (int i=1;i<=k;i++) fac[i]=1ll*fac[i-1]*i%Mod;
		for (int i=1;i<=k;i++)
			for (int j=0;j<1<<tot;j++){
				for (int x=2;x<=n;x++){
					Num=cnt=0,flag=1; int X=x;
					for (int l=1;l<=tot;l++)
						for (;X%pri[l]==0;Num+=(1<<(l-1)),X/=pri[l])
							if ((!(j&(1<<(l-1))))||(Num&(1<<(l-1)))) flag=0;
					if (!flag) continue;
					f[i][j]=(f[i][j]+f[i-1][j-Num])%Mod;
				}
				Res=(Res+1ll*f[i][j]*power(fac[i],Mod-2)%Mod)%Mod;
				if (i<k) Res=(1ll*f[i][j]*power(fac[i],Mod-2)%Mod+Res)%Mod;
			}
		printf("%d\n",(Res+1)%Mod); Res=0;
	}
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<bitset>
#include<vector>
#include<stdio.h>
#include<cstring>
#include<complex>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
int Res,a,n;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int power(int x,int y,int Mod){int Res=1;
	for (int t=x;y;y>>=1,t=1ll*t*t%Mod)
		if (y&1) Res=1ll*Res*t%Mod;
	return Res;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	for (int T=Read();T--;){
		a=Read(),n=Read();
		if (n>20){puts("1"); return 0;}
		for (int i=1;i<=1<<n;i++)
			if (power(i,a,1<<n)==power(a,i,1<<n)) Res++;
		printf("%d\n",Res); Res=0;
	}
}

#include<bits/stdc++.h>
using namespace std;
struct edge{
	int to,next;
}e[400010];
int n,m,nedge,cnt,top,ans;
int dfn[100010],low[100010],stk[100010],head[100010],into[100010];
bool vis[100010];
 void add(int a,int b)
{
	e[++nedge].to=b;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 void dfs(int x,int fa)
{
	dfn[x]=low[x]=++cnt;
	vis[x]=1;stk[top++]=x;
	for (int i=head[x];i;i=e[i].next)
	{
		int go=e[i].to;
		if (go==fa||dfn[x]<dfn[go]) continue;
		if (!vis[go]) 
		{
			dfs(go,x);
			low[x]=min(low[x],low[go]);	
		}
		else low[x]=min(low[x],dfn[go]);
	}
	if (dfn[x]==low[x])
	{
		while (top&&stk[top]!=x)
		{
			low[stk[--top]]=low[x];
			vis[stk[top]]=0;	
		}
	}
}
 int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	dfs(1,0);
	for (int i=1;i<=n;++i)
		for (int j=head[i];j;j=e[j].next)
		{
			int go=e[j].to;
			if (low[i]!=low[go])
			++into[low[i]],++into[low[go]];
		}
	for (int i=1;i<=n;++i) if (into[i]==2) ++ans;
	printf("%d\n",(ans+1)/2);
}

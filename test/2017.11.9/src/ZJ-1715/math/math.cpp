#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll a,n,mo,ans;
 ll qpow(ll x,ll k)
{
	ll ans=1;
	while (k)
	{
		if (k&1) ans=ans*x%mo;
		k>>=1;
		x=x*x%mo;	
	}	
	return ans;
} 
 int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%lld%lld",&a,&n);
		mo=1;
		ans=0;
		for (int i=1;i<=n;++i) mo*=2;
		for (int i=1;i<=mo;++i)
		{
			ll ta=qpow(a,i);
			ll tb=qpow(i,a);
			if (ta==tb) ++ans;
		}
		printf("%lld",ans);
	}
}

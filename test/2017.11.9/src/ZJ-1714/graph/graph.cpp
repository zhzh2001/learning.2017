#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=400010;
ll q[N],low[N],dfn[N],n,m,tot,ind,head[N],nxt[N],vet[N],vis[N],top,cnt,bel[N],yezi;
struct data{	ll x,y;	}QQQ[N];
void insert(ll x,ll y){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	}
void tarjan(ll x,ll pre){
	low[x]=dfn[x]=++ind;	vis[x]=1;	q[++top]=x;
	for(ll i=head[x];i;i=nxt[i])
	if ((i^pre)!=1){
		if (vis[vet[i]])	low[x]=min(low[x],dfn[vet[i]]);
		else	if (!dfn[vet[i]])	tarjan(vet[i],i),low[x]=min(low[x],low[vet[i]]);
	}
	if (low[x]==dfn[x]){
		++cnt;
		while(q[top+1]!=x){
			bel[q[top]]=cnt;
			vis[q[top--]]=0;
		}
	}
}
bool operator < (data a,data b){	return a.x==b.x?a.y<b.y:a.x<b.x;	}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();	m=read();	tot=1;
	For(i,1,m){	ll x=read(),y=read();	insert(x,y);	insert(y,x);	}
	tarjan(1,0);	if (cnt==1)	return puts("0"),0;
	tot=0;
	For(x,1,n)	for(ll i=head[x];i;i=nxt[i])
	if (bel[x]!=bel[vet[i]])	QQQ[++tot].x=bel[x],QQQ[tot].y=bel[vet[i]];
	sort(QQQ+1,QQQ+tot+1);
	for(ll i=1,j;i<=tot;i=j+1){
		j=i;	ll lnk=1;
		while(QQQ[j+1].x==QQQ[i].x){
			if (QQQ[j+1].y!=QQQ[j].y)	lnk++;
			++j;
		}yezi+=lnk==1;
	}writeln((yezi+1)>>1);
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll ppow(ll x,ll k,ll mod){
	ll ans=1;
	while(k){
		if (k&1)	ans=ans*x%mod;	x=x*x%mod;
		k>>=1;
	}return ans%mod;
}
int main(){
	ll mod=1<<30;
	For(i,1,200){
		For(j,1,200)	putchar(ppow(i,j,mod)); 
		puts("");
	}
}

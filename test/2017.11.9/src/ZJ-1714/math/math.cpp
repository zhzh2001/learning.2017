#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll ppow(ll x,ll k,ll mod){
	ll ans=1;
	while(k){
		if (k&1)	ans=ans*x%mod;	x=x*x%mod;
		k>>=1;
	}return ans%mod;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll T=read();
	while(T--){
		ll a=read(),n=read(),t=n*2;
		for(;t!=(t&-t);t+=t&-t);
		while(t<=10000&&t*2<=(1<<n))	t*=2;
		if (a&1)	puts("1");
		else if (a<=t){
			ll ans=0,ones=0;
			For(j,1,t)		ans+=ppow(a,j,1<<n)==ppow(j,a,1<<n);
			For(j,t+1,t*2)	ones+=ppow(a,j,1<<n)==ppow(j,a,1<<n);
			writeln(ans+ones*((1<<n)-t)/t);
		}else{
			ll ans=0;
			For(j,1,t)		ans+=ppow(a,j,1<<n)==ppow(j,a,1<<n);
			writeln(ans+((1<<n)-t)/2);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll mod=1e9+7;
ll f[40000][100],n,k,pri[100],ans;
void add(ll &x,ll y){	x=(x+y)%mod;	}
int main(){
	freopen("mul.in","r",stdin);
	freopen("baoli.out","w",stdout);
	pri[0]=2;
	pri[1]=3;
	pri[2]=5;
	pri[3]=7;
	pri[4]=11;
	pri[5]=13;
	pri[6]=17;
	pri[7]=19;
	pri[8]=23;
	pri[9]=29;
	pri[10]=31;
	pri[11]=37;
	pri[12]=41;
	pri[13]=43;
	pri[14]=47;
	ll T=read();
	while(T--){
		memset(f,0,sizeof f);
		n=read();	k=min(100LL,read());	f[0][0]=1;	f[0][1]=1;
		For(i,2,n){
			ll zt=0,x=i;	bool fl=1;
			For(j,0,14)	while(!(x%pri[j])){
				if (zt&(1<<j)){	fl=0;	break;	}
				else	zt|=1<<j,x/=pri[j];
			}
			if (fl)	FOr(j,32767,0)	FOr(kkksc,k-1,0)	if (!(zt&j))	add(f[j^zt][kkksc+1],f[j][kkksc]);
		}ans=0;
		For(i,0,32767)	For(kkksc,0,k)	add(ans,f[i][kkksc]);
		writeln((ans-1+mod)%mod);
	}
}

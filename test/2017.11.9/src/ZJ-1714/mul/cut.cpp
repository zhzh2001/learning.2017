#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll mod=1e9+7;
struct data{	ll big,zt;	}q[510];
ll f[1100][510],n,k,tru[100],tot;
bool operator< (data a,data b){	return a.big<b.big;	}
void add(ll &x,ll y){	x=(x+y)%mod;	}
ll work(ll n,ll k){
 	memset(f,0,sizeof f);	tot=0;
	ll Max_k=0,mx=501,sum=0;
	For(i,1,n){
		ll x=i,fl=1,big=0,zt=0;
		for(ll j=2;j<=x;j++)
		if (!(x%j)){
			x/=j;
			if (!(x%j)){	fl=0;	break;	}
			if (j>=30)	big=j;
			else	zt|=1<<tru[j];
		}if (fl)	q[++tot].big=big?big:mx++,q[tot].zt=zt;
	}sort(q+1,q+tot+1);
	f[0][0]=1;
	for(ll i=1,j;i<=tot;i=j+1){
		j=i;	++sum;
		while(q[j+1].big==q[i].big)	++j;
		FOr(cho,Max_k,0)	For(zyy,i,j)	For(zt,0,1023)	if (f[zt][cho]&&!(zt&q[zyy].zt))	add(f[zt|q[zyy].zt][cho+1],f[zt][cho]);
		For(zt,0,1023)	if(f[zt][Max_k+1])	Max_k++;
		Max_k=min(Max_k,k);
	}ll ans=0;
	For(k,0,Max_k)	For(zt,0,1023)	ans+=f[zt][k];
	return ans%mod;
}
int main(){
	freopen("mul.in","r",stdin);
	tru[2]=0;	tru[3]=1;	tru[5]=2;	tru[7]=3;	tru[11]=4;
	tru[13]=5;	tru[17]=6;	tru[19]=7;	tru[23]=8;	tru[29]=9;
	ll T=read();
	while(T--){
		n=read();	k=read();
		writeln((work(n,k)+mod-1)%mod);
	}
}

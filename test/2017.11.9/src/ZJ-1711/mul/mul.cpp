#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 505
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,n,K,num,cnt,ans,pri[N],vis[N],a[N],b[N],f[40][40][1100];
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout); 
	rep(i,2,30){
		if (!vis[i]) { pri[++num]=i; }
		for (ll k=2;i*k<=30;++k) vis[i*k]=1;
	}
	memset(vis,0,sizeof vis);
	rep(i,2,30) rep(j,1,num)
		if (i%(pri[j]*pri[j])==0) vis[i]=1;
	rep(i,1,30) if (!vis[i]) a[++cnt]=i;
	rep(i,2,cnt) rep(j,1,num) if (a[i]%pri[j]==0){
		b[i]|=1<<(j-1);
	}
	f[0][0][0]=1;
	rep(i,0,cnt-1) rep(j,0,i) rep(k,0,(1<<num)-1) if (f[i][j][k]){
		(f[i+1][j][k]+=f[i][j][k])%=mod;
		if ((b[i+1]&k)>0) continue;
		(f[i+1][j+1][k|b[i+1]]+=f[i][j][k])%=mod;
	}
	T=read();
	while (T--){
		n=read(); K=read(); ans=0;
		n=upper_bound(a+1,a+1+cnt,n)-a-1;
		rep(i,1,K) rep(j,0,(1<<num)-1) (ans+=f[n][i][j])%=mod;
		printf("%lld\n",ans);
	}
}

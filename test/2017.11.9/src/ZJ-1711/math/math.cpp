#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 505
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll qpow(ll x,ll y,ll mod){
	ll ans=1;
	for (;y;y>>=1,x=x*x%mod) if (y&1) ans=ans*x%mod;
	return ans;
}
ll T,a,n,ans;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	T=read();
	while (T--){
		a=read(); n=read(); ans=0;
		rep(b,1,1<<n) if (qpow(a,b,1<<n)==qpow(b,a,1<<n)) ++ans;
		printf("%lld\n",ans);
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,ans,tot,color,dfn_num,cnt,top;
ll q[N],head[N],dfn[N],low[N],belong[N],deg[N];
struct edge{ ll to,next; }e[N<<2];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void tarjan(ll u,ll last){
	dfn[u]=low[u]=++dfn_num; q[++top]=u;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		if (dfn[v]) low[u]=min(low[u],dfn[v]);
		else tarjan(v,u),low[u]=min(low[u],low[v]);
	}
	if (low[u]==dfn[u]){
		ll k; ++color;
		do{
			k=q[top--];
			belong[k]=color;
		}while (k!=u);
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(); m=read();
	rep(i,1,m){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	tarjan(1,-1);
	rep(u,1,n) for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to)
		if (belong[u]!=belong[v]) ++deg[belong[v]];
	rep(i,1,color) if (deg[i]==1) ++ans;
	printf("%d",max(0,ans-1));
}

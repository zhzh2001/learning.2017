#include <cstdio>
using namespace std;
const int primes[10]={2,3,5,7,11,13,17,19,23,29};
int a[31];
int main(){
	freopen("mul.in","r",stdin);
	freopen("bf.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		int n,k;
		scanf("%d%d",&n,&k);
		int cur=0;
		for(int i=1;i<=n;i++){
			bool flag=false;
			int now=0;
			for(int j=0;j<10;j++)
				if (i%primes[j]==0){
					if (i%(primes[j]*primes[j])==0){
						flag=true;
						break;
					}
					now|=1<<j;
				}
			if (!flag)
				a[cur++]=now;
		}
		int ans=0;
		for(int i=0;i<(1<<cur);i++){
			if (__builtin_popcount(i)<=k){
				int now=0;
				bool flag=true;
				for(int j=0;j<cur;j++)
					if (i>>j&1){
						if (now&a[j]){
							flag=false;
							break;
						}
						now|=a[j];
					}
				ans+=flag;
			}
		}
		printf("%d\n",(ans-1)%1000000007);
	}
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	char ch=getchar();
	int x=0,f=1;
	while(ch<'0' || ch>'9'){
		if (ch=='-')
			f=-1;
		ch=getchar();
	}
	while(ch>='0' && ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
const int mod=1000000007;
const int primes[8]={2,3,5,7,11,13,17,19};
struct item{
	int v,st;
};
int f[502][257];
int cur[502],cnt;
item a[502][502];
int work(int n,int m){
	memset(f,0,sizeof(f));
	m=min(m,cnt);
	f[0][0]=1;
	for(int i=1;i<=n;i++){
		item* qwq=a[i];
		if (!cur[i])
			continue;
		for(int j=m;j>=0;j--){
			int *g=f[j],*h=f[j+1];
			for(int k=1;k<=cur[i];k++){
				if (qwq[k].v>n)
					break;
				int x=qwq[k].st,s=(~x)&((1<<8)-1);
				for(int o=s;;o=(o-1)&s){
					h[o^x]+=g[o];
					if (h[o^x]>=mod)
						h[o^x]-=mod;
					if (!o)
						break;
				}
			}
		}
	}
	int ans=0;
	for(int i=1;i<=m;i++)
		for(int j=0;j<(1<<8);j++)
			(ans+=f[i][j])%=mod;
	return ans;
}
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	for(int i=1;i<=500;i++){
		bool flag=false;
		int now=0,t=i;
		for(int j=0;j<8;j++)
			if (i%primes[j]==0){
				if (i%(primes[j]*primes[j])==0){
					flag=true;
					break;
				}
				now|=1<<j;
				t/=primes[j];
			}
		if (flag)
			continue;
		int typ=t==1?i:t;
		a[typ][++cur[typ]]=(item){i,now};
		cnt++;
	}
	//fprintf(stderr,"%d\n",cur);
	int T=read();
	//fprintf(stderr,"%d\n",T);
	while(T--){
		int n=read(),k=read();
		//fprintf(stderr,"%d %d\n",n,k);
		printf("%d\n",work(n,k));
	}
}

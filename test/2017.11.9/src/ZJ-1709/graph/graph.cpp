#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	char ch=getchar();
	int x=0,f=1;
	while(ch<'0' || ch>'9'){
		if (ch=='-')
			f=-1;
		ch=getchar();
	}
	while(ch>='0' && ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
const int maxn=100008;
const int maxm=400008;
struct graph{
	int n,m;
	struct edge{
		int from,to,next;
	}e[maxm];
	int first[maxn];
	void init(int n){
		this->n=n;
		m=1;
	}
	void addedge(int from,int to){
		e[++m]=(edge){from,to,first[from]};
		first[from]=m;
	}
	void addbiedge(int from,int to){
		addedge(from,to);
		addedge(to,from);
	}
	int pre[maxn],low[maxn],clk;
	bool bri[maxm];
	void dfs(int u,int fa){
		pre[u]=low[u]=++clk;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa)
				continue;
			if (!pre[v]){
				dfs(v,u);
				low[u]=min(low[u],low[v]);
			}else low[u]=min(low[u],pre[v]);
			if (low[v]>pre[u]){
				bri[i]=bri[i^1]=true;
				//puts("WTF");
			}
		}
		//fprintf(stderr,"pre[%d]=%d low[%d]=%d\n",u,pre[u],u,low[u]);
	}
	int bccno[maxn],cnt;
	void dfs2(int u){
		bccno[u]=cnt;
		for(int i=first[u];i;i=e[i].next){
			if (bri[i])
				continue;
			int v=e[i].to;
			if (!bccno[v])
				dfs2(v);
		}
	}
	int deg[maxn];
	int work(){
		dfs(1,0);
		for(int i=1;i<=n;i++)
			if (!bccno[i]){
				cnt++;
				dfs2(i);
				//printf("%d in %d\n",i,cnt);
			}
		//for(int i=1;i<=n;i++)
			//printf("bccno[%d]=%d\n",i,bccno[i]);
		for(int i=2;i<=m;i++){
			int u=bccno[e[i].from],v=bccno[e[i].to];
			//printf("%d %d\n",u,v);
			if (u!=v){
				//puts("233");
				//if (!bri[i])
					//puts("WTF");
				deg[u]++;
				deg[v]++;
			}
		}
		int ans=0;
		for(int i=1;i<=cnt;i++)
			if (deg[i]==2)
				ans++;
		//printf("%d\n",ans);
		return (ans+1)/2;
	}
}g;
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	int n=read(),m=read();
	g.init(n);
	while(m--){
		int u=read(),v=read();
		g.addbiedge(u,v);
	}
	printf("%d",g.work());
}

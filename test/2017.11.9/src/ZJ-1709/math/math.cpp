#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned int uint;
int read(){
	char ch=getchar();
	int x=0,f=1;
	while(ch<'0' || ch>'9'){
		if (ch=='-')
			f=-1;
		ch=getchar();
	}
	while(ch>='0' && ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
uint mod;
uint power(uint x,uint y){
	uint res=1;
	while(y){
		if (y&1)
			res*=x;
		x*=x;
		y>>=1;
	}
	return res&mod;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int T=read();
	while(T--){
		uint a=read(),n=read();
		mod=(1<<n)-1;
		uint now=a;
		int ans=0;
		for(int i=1;i<=(1<<n);i++){
			if ((now&mod)==power(i,a))
				ans++;
			now*=a;
		}
		printf("%d\n",ans);
	}	
}

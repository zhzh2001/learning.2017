#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("mul.in");
ofstream fout("mul.out");
const int p[] = {2, 3, 5, 7, 11, 13, 17, 19}, pn = 8, N = 505, MOD = 1e9 + 7;
int f[1 << 9][N];
pair<int, int> a[N];
inline void update(int &x, int y)
{
	x = (x + y) % MOD;
}
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		int n, k;
		fin >> n >> k;
		int m = 0;
		for (int i = 1; i <= n; i++)
		{
			int x = i, mask = 0;
			bool flag = true;
			for (int j = 0; j < pn; j++)
			{
				int cnt = 0;
				while (x % p[j] == 0)
				{
					x /= p[j];
					cnt++;
				}
				if (cnt > 1)
				{
					flag = false;
					break;
				}
				if (cnt)
					mask |= 1 << j;
			}
			if (flag)
			{
				if (x > 1)
					mask |= 1 << 8;
				a[++m] = make_pair(x, mask);
			}
		}
		sort(a + 1, a + m + 1);
		fill_n(&f[0][0], sizeof(f) / sizeof(int), 0);
		f[0][0] = 1;
		for (int i = 1, j; i <= m; i = j)
		{
			for (j = i; j <= m && a[j].first == a[i].first; j++)
				for (int s = 511; s >= 0; s--)
					for (int k = min(j - 1, 100); k >= 0; k--)
						if ((s & a[j].second) == 0)
							update(f[s | a[j].second][k + 1], f[s][k]);
			for (int s = 256; s < 512; s++)
				for (int k = 1; k < j && k <= 100; k++)
				{
					update(f[s - 256][k], f[s][k]);
					f[s][k] = 0;
				}
		}
		int ans = 0;
		for (int i = 0; i < 256; i++)
			for (int j = 1; j <= k; j++)
				update(ans, f[i][j]);
		fout << ans << endl;
	}
	return 0;
}
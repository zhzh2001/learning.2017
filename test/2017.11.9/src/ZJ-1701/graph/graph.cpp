#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=1e5+5;
struct cs{int to,nxt;}a[N*5];
int head[N],ll,tt[N],lin[N],tl,q[N],tp,ti,A[N];
bool in[N];
int n,m,x,y,ans;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs(int x,int y){
	int now=++ti;tt[x]=now;q[++tp]=x;in[x]=1;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y){
			if(!tt[a[k].to])dfs(a[k].to,x);
			if(in[a[k].to])tt[x]=min(tt[x],tt[a[k].to]);
		}
	if(tt[x]==now){
		lin[x]=++tl;in[x]=0;
		while(q[tp]!=x){
			lin[q[tp]]=tl;
			in[q[tp--]]=0;
		}tp--;
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout); 
	read(n,m);
	for(int i=1;i<=m;i++){
		read(x,y);
		init(x,y);
		init(y,x);
	}
	for(int i=1;i<=n;i++)if(!tt[i])dfs(i,-1);
	for(int i=1;i<=n;i++)
		for(int k=head[i];k;k=a[k].nxt)
			if(lin[a[k].to]!=lin[i])
				A[lin[a[k].to]]++;
	for(int i=1;i<=tl;i++)if(A[i]==1)ans++;
	W(ans+1>>1);
}

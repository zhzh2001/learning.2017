#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=501;
int pri[9],tp,v[N],f[2][N][300],g[N][N],o;
int n,m,x,y,mo=1e9+7;
int main()
{
	for(int i=2;i*i<=500;i++){
		bool ok=1;
		for(int j=1;j<=tp;j++)if(!(i%pri[j])){ok=0;break;}
		if(ok)pri[++tp]=i;
	}
	for(int i=1;i<=500;i++)
		for(int j=1;j<=tp;j++){
			if(!(i%(pri[j]*pri[j]))){v[i]=-1;break;}
			if(!(i%pri[j]))v[i]|=1<<(j-1);
		}
	f[0][0][0]=1;o=0;
	for(int i=0;i<500;i++){
		for(int j=0;j<=500;j++)
			for(int k=0;k<=256;k++)
				f[1-o][j][k]=0;
		for(int j=0;j<=i;j++)
			for(int k=0;k<=256;k++)
				if(f[o][j][k]){
					g[i][j]=(g[i][j]+f[o][j][k])%mo;
					f[1-o][j][k]=(f[1-o][j][k]+f[o][j][k])%mo;
					if(v[i+1]!=-1&&!(k&v[i+1]))f[1-o][j+1][(k|v[i+1])]=(f[1-o][j+1][(k|v[i+1])]+f[o][j][k])%mo;
				}
		o=1-o;
	}
	for(int j=0;j<=500;j++)
			for(int k=0;k<=256;k++)
				g[500][j]=(g[500][j]+f[o][j][k])%mo;
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	m=RR();
	while(m--){
		read(x,y);
		int ans=0;
		for(int i=1;i<=y;i++)ans=(ans+g[x][i])%mo;
		WW(ans);
	}
}

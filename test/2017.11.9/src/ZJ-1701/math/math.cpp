#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

int T,n,m,a;
int ksm(int x,int y){
	int ans=1;
	for(;y;y>>=1,x=1ll*x*x%(1<<n))
		if(y&1)ans=1ll*ans*x%(1<<n);
	return ans;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	T=RR();
	while(T--){
		read(a,n);
		if(a&1)WW(1);else
		if(n<=10){
			int ans=0;
			for(int i=1;i<=(1<<n);i++)
				if(ksm(a,i)==ksm(i,a))ans++;
			WW(ans);
		}else
		if(a>=30){
			int ans=1<<n-1;
			ans-=15;
			for(int i=1;i<=30;i++)
				if(ksm(a,i)==ksm(i,a))ans++;
			WW(ans);
		}else{
			int x=0,y=0,ans=0,i,v=(1<<n);
			for(i=1;i<=v;i++)
				if(ksm(a,i)==ksm(i,a)){
					ans++;
					if(!x){x=i;continue;}
					if(!y){y=i;continue;}
					if(i-y==y-x){break;}
					x=y;y=i;
				}
			ans+=(v-i)/(y-x);
			WW(ans);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll ans,sx,a,b,n;
ll power(ll a,ll b)
{ll t=1,y=a;
   while(b)
   {
   	if(b&1) t=(t*y)%sx;
   	y=(y*y)%sx;
   	b>>=1;
   }
   return t;
}
int t;
int main()
{ll i,j,k;
     freopen("math.in","r",stdin);
     freopen("math.out","w",stdout);
	 t=read();
	 for(;t;t--)
	 {
	 	ans=0;
		a=read();n=read();
		if(a%2==1) 
		{
		printf("1\n");
		continue;
	 	}
		sx=1000000000ll;
		sx=power(2,n);
		for(i=1;i<=sx;i++) if(power(a,i)==power(i,a)) ans++;
		printf("%lld\n",ans);
	 }
	return 0;
}


#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
#define M 200005
using namespace std;
int read()
{int t=0;char c;
 c=getchar();
 while(!(c>='0' && c<='9')) c=getchar();
 while(c>='0' && c<='9')
 {
 	t=t*10+c-48;c=getchar();
 }
 return t;
}
int num,ans,dfn[N],low[N],front[N],next[2*M],to[2*M],line;
int belong[N],zhan[N],top,n,m,pre[N],sx;
int road[M][2];
int du[N],index;
bool ifin[N];
void insert(int x,int y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
	line++;to[line]=x;next[line]=front[y];front[y]=line;
}
void dfs(int x)
{int i;
	dfn[x]=low[x]=++index;zhan[++top]=x;ifin[x]=true;
	for(i=front[x];i!=-1;i=next[i])
	 if(pre[x]!=to[i])
	  if(!dfn[to[i]])
	  {
	  	pre[to[i]]=x;dfs(to[i]);low[x]=min(low[x],low[to[i]]);
	  }
	  else if(ifin[to[i]]) low[x]=min(low[x],dfn[to[i]]);
	if(dfn[x]==low[x])
	{
		num++;
		while(zhan[top]!=x)
		 {
		 	ifin[zhan[top]]=false;
		 	belong[zhan[top]]=num;
  			top--;
		 }
		ifin[zhan[top]]=false;
		belong[zhan[top]]=num;
		top--;
	}
}
int main()
{int i,j,k;
   freopen("graph.in","r",stdin);
   freopen("graph.out","w",stdout);
   n=read();m=read();
   line=-1;memset(front,-1,sizeof(front));
   for(i=1;i<=m;i++)
     {
     	int x=read(),y=read();
     	road[i][0]=x;road[i][1]=y;
     	insert(x,y);
	 }
	sx=0;
	for(i=1;i<=n;i++)
	  if(!dfn[i])
	  {
	  	dfs(i);
	  }
	for(i=1;i<=m;i++)
	  if(belong[road[i][0]]!=belong[road[i][1]])
	   {
	   	du[belong[road[i][0]]]++;du[belong[road[i][1]]]++;
	   }
	for(i=1;i<=num;i++)
	  if(du[i]==1) sx++;
	printf("%d\n",(sx+1)/2);
	return 0;
}

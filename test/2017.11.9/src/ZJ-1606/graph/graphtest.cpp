#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int n,m,x,y;
int sx,a[100005];
int main()
{int i,j,k;
    freopen("graph.in","r",stdin);
    freopen("grapht.out","w",stdout);
	n=read();m=read();
	for(i=1;i<=m;i++)
	{
		x=read();y=read();
		a[x]++;a[y]++;
	}
	sx=0;
	for(i=1;i<=n;i++)
	  if(a[i]==1) sx++;
	printf("%d\n",(sx+1)/2);
}

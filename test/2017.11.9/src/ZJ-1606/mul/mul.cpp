#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define p 1000000007
using namespace std;
int read()
{int t=0;char c;
 c=getchar();
 while(!(c>='0' && c<='9')) c=getchar();
 while(c>='0' && c<='9')
 {
 	t=t*10+c-48;c=getchar();
 }
 return t;
}
int n,m;
int t;
ll f[505][2050],ans;
int a[505],prime[13];
bool b[505];
int main()
{int i,j,k;
    freopen("mul.in","r",stdin);
    freopen("mul.out","w",stdout);
    prime[1]=2;prime[2]=3;prime[3]=5;prime[4]=7;prime[5]=11;
    prime[6]=13;prime[7]=17;prime[8]=19;prime[9]=23;prime[10]=29;
    memset(b,true,sizeof(b));
	for(i=2;i<=30;i++)
    {
    	for(j=1;j<=10;j++)
    	 {
    	 	if(i%(prime[j]*prime[j])==0)
    	 	 {
    	 	 	b[i]=false;break;
			  }
			if(i%prime[j]==0) a[i]+=1<<(j-1);
		 }
	}
    t=read();
    for(;t;t--)
    {
       memset(f,0,sizeof(f));ans=0;
	   n=read();m=read();
	   f[0][0]=1;
	   for(i=1;i<=n;i++)
	   if(b[i])
	   	for(j=m-1;j>=0;j--)
	     for(k=0;k<1024;k++)
		  if(f[j][k])
		   if((a[i]&k)==0) f[j+1][k|a[i]]=(f[j+1][k|a[i]]+f[j][k])%p;
		for(i=1;i<=m;i++)
		  for(j=0;j<1024;j++) ans+=f[i][j],ans%=p;
		printf("%lld\n",ans);  	
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll a,ans;
ll n;
ll bin2[31];
bool flag;

inline ll quick_pow(ll a,ll b,ll mod){
	ll ans = 1;
	for(;b;b >>= 1,a = a*a%mod)
		if(b&1)
			ans = ans*a%mod;
	return ans;
}

inline bool pd(ll a,ll b){
	if(b == a && flag == false){
		flag = true;
		return true;
	}
	if(b > a && flag == false){
		flag = true;
		ans++;
	}
	ll a1 = quick_pow(a,b,bin2[n]);
	ll b1 = quick_pow(b,a,bin2[n]);
	if(a1 == b1) return true;
	return false;
}

signed main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	bin2[0] = 1;
	for(ll i = 1;i <= 30;++i)
		bin2[i] = bin2[i-1]*2;
	ll T = read();
	while(T--){
		a = read(); n = read();
		ll bin = 0; ans = 0;
		ll x = a;
		while(x%2 == 0){
			x /= 2;
			bin++;
		}
		if(bin == 0){
			puts("1");
			continue;
		}
		ll p = (n-1)/a+1;
		ll l; flag = false;
		for(int i = 1;i < p;i++){
			if(pd(a,bin2[i]*x)){
//				printf("# %lld\n",bin2[i]*x);
				ans++;
			}
		}
		l = bin2[p];
		for(int i = 1;i <= 30;i++){
			if(pd(a,l)){
//				printf("# %lld\n",l);
				ans++;
//				l += bin2[p];
				break;
			}
			l += bin2[p];
		}
		ans += (bin2[n]-l+1)/bin2[p];
		printf("%lld\n",ans);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll a,ans;
ll n;
ll bin2[31];

inline ll quick_pow(ll a,ll b,ll mod){
	ll ans = 1;
	for(;b;b >>= 1,a = a*a%mod)
		if(b&1)
			ans = ans*a%mod;
	return ans;
}

signed main(){
	freopen("math.in","r",stdin);
	freopen("math2.out","w",stdout);
	bin2[0] = 1;
	for(ll i = 1;i <= 30;++i)
		bin2[i] = bin2[i-1]*2;
	ll T = read();
	while(T--){
		a = read(); n = read();
		ans = 0;
		ll a1 = 1;
		for(ll b = 1;b <= bin2[n];++b){
			a1 = a*a1%bin2[n];
			ll b1 = quick_pow(b,a,bin2[n]);
			if(a1 == b1){
//				printf("# %lld\n",b);
				++ans;
			}
		}
		printf("%lld\n",ans);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 505,mod = 1e9+7;
int an[N];
int ans;
int n,k;

int gcd(int a,int b){
	if(b == 0) return a;
	return gcd(b,a%b);
}

void dfs(int x,int f){
	if(x > 1){
		bool flag = true;
		for(int i = 1;i < x;i++){
			for(int j = i+1;j < x;j++)
				if(gcd(an[i],an[j]) != 1){
					flag = false;
					break;
				}
			if(!flag) break;
		}
		if(flag) ans++;
	}
	if(x == k+1) return;
	for(int i = f;i <= n;i++){
		an[x] = i;
		dfs(x+1,i+1);
	}
}

int main(){
	int T = read();
	while(T--){
		n = read(); k = read();
		ans = 0;
		dfs(1,1);
		printf("%d\n",ans);
	}
	return 0;
}

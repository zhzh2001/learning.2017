#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 505,M = 256,mod = 1e9+7;
int prmi[20] = {2,3,5,7,11,13,17,19};
int f[N][M],g[N][M],ans;
struct node{
	int x;
	int ps;	//质数的集合 
	friend bool operator <(node a,node b){
		return a.x > b.x;
	}
}a[N];
int n,k,tot;

inline void getpi(int x){
	int p = x,num = 0;
	for(int i = 0;i < 8;++i)
		if(x%prmi[i] == 0){
			num |= (1<<i);
			x /= prmi[i];
			if(x%prmi[i] == 0) return;
		}
	++tot;
	a[tot].x = x;
	a[tot].ps = num;
}

inline void upd(int &x,int y){
	x += y;
	if(x > mod) x -= mod;
}

int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	int T = read();
	while(T--){
		memset(f,0,sizeof f); f[0][0] = 1;
		n = read(); k = read(); ans = 0; tot = 0;
		for(int i = 1;i <= n;++i) getpi(i);
		for(int i = 1;i <= tot;++i){
			if(a[i].x != a[i-1].x || a[i].x == 1){
				memcpy(g,f,sizeof f);
			}
			for(int j = 1;j <= k;++j)
				for(int z = 0;z < M;++z){
					if((a[i].ps&z) > 0) continue;
					upd(g[j][z|a[i].ps],f[j-1][z]);
				}
			if(a[i].x != a[i+1].x || a[i].x == 1){
				memcpy(f,g,sizeof g);
			}
		}
		for(int i = 1;i <= k;++i)
			for(int z = 0;z < M;++z)
				upd(ans,f[i][z]);
		printf("%d\n",ans);
	}

	return 0;
}

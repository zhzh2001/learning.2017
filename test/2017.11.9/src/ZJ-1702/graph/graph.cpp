#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define Min(x,y) ((x) < (y) ? (x) : (y))
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,M = 200005;
int head[N],nxt[M*2],to[M*2],cnt = 1;
int ans,Color;
int Dfn[N],MiDfn[N],sufer[N];
int stack[N],top;
int n,m;
int in[N];

inline void add(int a,int b){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt;
}

void dfs(int x,int f){
	MiDfn[x] = Dfn[x]; stack[++top] = x;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			int v = to[i];
			if(Dfn[v] != 0)
				MiDfn[x] = Min(MiDfn[x],MiDfn[v]);
			else{
				Dfn[v] = Dfn[x]+1;
				dfs(v,x);
				MiDfn[x] = Min(MiDfn[x],MiDfn[v]);
			}
		}
	if(MiDfn[x] == Dfn[x]){
		Color++;
		do{
			sufer[stack[top]] = Color;
			top--;
		}while(Dfn[stack[top+1]] != MiDfn[stack[top+1]]);
	}
}

int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= m;i++){
		int a = read(),b = read();
		add(a,b);
		add(b,a);
	}
	Dfn[1] = 1;
	dfs(1,1);
	for(int i = 2;i <= cnt;i += 2){
		if(sufer[to[i]] != sufer[to[i^1]]){
			++in[sufer[to[i]]];
			++in[sufer[to[i^1]]];
		}
	}
	for(int i = 1;i <= Color;i++)
		if(in[i] < 2) ans++;
	printf("%d\n",ans-1);
	return 0;
}

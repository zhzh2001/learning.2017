#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 200005

int n,m,i,j,u,v,ans,cnt,cnt1,top,num,Time;
int dfn[N],low[N],z[N],belong[N],head[N],head1[N];
bool vis[N],vise[2*N],init[N];
struct ff{int to,nxt;}e[2*N],e1[2*N];

void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}
void add1(int u,int v){
	e1[++cnt1]=(ff){v,head1[u]};
	head1[u]=cnt1;
}
void Tarjan(int now){
	vis[now]=init[now]=1;
	dfn[now]=low[now]=++Time;
	z[++top]=now;int k=top;
	for(int i=head[now];i;i=e[i].nxt){
		if(vise[i])continue;
		vise[i]=vise[i^1]=1;
		int v=e[i].to;
		if(!vis[v]){
			Tarjan(v);
			low[now]=min(low[now],low[v]);
		}else if(init[v])low[now]=min(low[now],dfn[v]);
	}
	if(dfn[now]==low[now]){
		num++;
		for(int i=k;i<=top;i++)
			belong[z[i]]=num,init[z[i]]=0;
		top=k-1;
	}
}
void dfs(int now,int fa){
	int d=0;
	for(int i=head1[now];i;i=e1[i].nxt){
		int v=e1[i].to;
		if(v==fa)continue;
		d++;dfs(v,now);
	}ans+=d/2;
	if(now==1)ans+=d&1;
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&m);cnt=1;
	for(i=1;i<=m;i++){
		scanf("%d%d",&u,&v);
		add(u,v),add(v,u);
	}Tarjan(1);
	for(i=1;i<=n;i++)
	for(j=head[i];j;j=e[j].nxt)
		if(belong[i]!=belong[e[i].to])add1(belong[i],belong[e[i].to]);
	dfs(1,0);
	printf("%d\n",ans);
	return 0;
}

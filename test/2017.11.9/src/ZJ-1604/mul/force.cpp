#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 35

int T,n,m,k,i,j,ans,num,MOD=1e9+7;
int prime[N],Min[N],d[N],val[N],pos[N],f[N][1050];
bool p[N],v[N];

int main()
{
	freopen("mul.in","r",stdin);
	freopen("force.out","w",stdout);
	for(i=2;i<=30;i++){
		if(!p[i])prime[++num]=i,pos[i]=num,Min[i]=i;
		for(j=1;i*prime[j]<=30&&j<=num;j++){
			p[i*prime[j]]=1;Min[i*prime[j]]=prime[j];
			if(i%prime[j]==0)continue;
		}
	}for(i=2;i<=30;i++)
		for(j=i;j>1;j/=Min[j]){
			val[i]+=1<<(pos[Min[j]]-1);
			if((j/Min[j])%Min[j]==0)v[i]=1;
		}
	scanf("%d",&T);
	while(T--){
		scanf("%d%d",&n,&m);
		memset(f,0,sizeof(f));
		f[0][0]=1;
		for(i=1;i<=n;i++){
			if(v[i])continue;
			for(j=min(i,m);j;j--)
			for(k=(1<<10)-1;k;k--)
				if((val[i]&k)==val[i])f[j][k]=(f[j][k]+f[j-1][k-val[i]])%MOD;
		}ans=0;
		for(i=1;i<=m;i++)
			for(j=1;j<(1<<10)-1;j++){
				ans=(ans+f[i][j])%MOD;
				if(i<m)ans=(ans+f[i][j])%MOD;
			}
		printf("%d\n",(ans+1)%MOD);
	}return 0;
}

#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
int T,a,n,i,ans,MOD;
int ksm(int x,int y){
	int res=1;
	while(y){
		if(y&1)res=1ll*res*x%MOD;
		y>>=1,x=1ll*x*x%MOD;
	}return res;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d",&a,&n);
		ans=0;MOD=1<<n;
		for(i=1;i<=(1<<n);i++)
			if(ksm(a,i)==ksm(i,a))ans++;
		printf("%d\n",ans);
	}
}

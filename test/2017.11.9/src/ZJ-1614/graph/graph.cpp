#include<map>
#include<cctype>
#include<cstdio>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

const int N=2e5+35;

struct Edge {
	int to,nxt;
}E[N<<1];

map<int,int>vis[N];
int head[N],cnt=1,x[N],y[N],stk[N],top,clk,dfn[N],low[N],scc,deg[N],ans,be[N],n,m,A,B;

void Link(int i,int u,int v) {
	x[i]=u,y[i]=v;
	E[++cnt]=(Edge){v,head[u]},head[u]=cnt;
	E[++cnt]=(Edge){u,head[v]},head[v]=cnt;
}

void tarjan(int x,int fa) {
	stk[++top]=x;
	dfn[x]=low[x]=++clk;
	Rep (i,x) if ((i^1)!=fa) {
		if (!dfn[y=E[i].to]) {
			tarjan(y,i);
			if (low[y]>dfn[x]) {
				scc++;
				while (stk[top]!=x)
					be[stk[top]]=scc,top--;
			}
			low[x]=min(low[x],low[y]);
		}
		else low[x]=min(low[x],dfn[y]);
	}
	return;
}

int main() {
	
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	
	n=rd(),m=rd();
	For (i,1,m) Link(i,rd(),rd());
	
	tarjan(1,-1);
	if (top) {
		scc++;
		while (top) be[stk[top]]=scc,top--;
	}
	
	For (i,1,m) {
		A=be[x[i]],B=be[y[i]];
		if (vis[A][B]) continue; else deg[A]++,deg[B]++;
	}
	For (i,1,scc) ans+=(deg[i]==1);
	printf("%d\n",(ans+1)/2);
	
	return 0;
}

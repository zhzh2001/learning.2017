#include<map>
#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

const int len=8;
const int N=535;
const int mod=1e9+7;

int pri[10]={2,3,5,7,11,13,17,19,23,29};
int n,K,ans,cnt,a[N],p[N],flg[N],rst[N],vis[N],f[N][1<<len],g[N][1<<len];

inline void inc(int &x,int y) { x+=y; if (x>=mod) x-=mod; }

int main() {
	
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	
	For (i,1,500) {
		flg[i]=1;
		int tmp=i,j=0;
		while (tmp>1 && j<len) {
			int cnt=0;
			while (tmp%pri[j]==0) tmp/=pri[j],cnt++;
			if (cnt>1) { flg[i]=0; break; }
			else if (cnt==1) a[i]|=1<<j;
			j++;
		}
		rst[i]=tmp;
		if (tmp>1 && !vis[tmp])
			vis[tmp]=1,p[++cnt]=tmp;
	}
	/*
//	test:
	int num=0;
	For (i,1,500) if (flg[i] && rst[i]!=1) num++;
	printf("%d\n",num);
	*/
	
	for (int cas=rd();cas--;) {
		n=rd(),K=rd();
		memset(f,0,sizeof f),f[0][0]=1;
		
		For (t,1,n) if (flg[t] && rst[t]==1) {
			For (j,0,(1<<len)-1) if (!(j&a[t]))
				Dep (i,K-1,0) inc(f[i+1][j|a[t]],f[i][j]);
		}
		For (l,1,cnt) {
			memcpy(g,f,sizeof g);
			For (t,1,n)
				if (flg[t] && rst[t]==p[l]) {
					For (j,0,(1<<len)-1) if (!(j&a[t]))
						For (i,0,K-1) inc(f[i+1][j|a[t]],g[i][j]);
				}
		}
		
		ans=0;
		For (i,1,K) For (j,0,(1<<len)-1) inc(ans,f[i][j]);
		printf("%d\n",ans);
	}
	
	return 0;
}
/*
5
500 500
500 500
500 500
500 500
500 500

119928209
*/

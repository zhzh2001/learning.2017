#include<map>
#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

int a,n,t,y,x,z,tmp,up_b,dw_y,cnt;

int power(int x,int y,int mod) {
	int t=1;
	for (;y;y>>=1,x=1LL*x*x%mod)
		if (y&1) t=1LL*t*x%mod;
	return t;
}

int main() {
	
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	int cas=rd();
	while (cas--)
//	while (1)
	{
		t=y=x=z=cnt=0;
		a=tmp=rd(),n=rd();
		
		if (a%2==1) puts("1");
		else {
			while (tmp%2==0) tmp/=2,t++;
			up_b=(n-1)/t+1;
			
			For (b,1,up_b-1)
				if (power(a,b,1<<n)==power(b,a,1<<n))
					cnt++;
			
			dw_y=(n-1)/a+1;
			cnt+=(1<<n)/(1<<dw_y);
		
			For (b,1,up_b-1)
				if (b%(1<<dw_y)==0)
					cnt--;
			
			printf("%d\n",cnt);
		}
	}
	return 0;
}

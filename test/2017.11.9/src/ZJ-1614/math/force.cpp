#include<map>
#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)
#define Rep(i,x) for(int y,i=head[x];i;i=E[i].nxt)

using namespace std;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (!isdigit(c)) f=(c=='-')?-1:1,c=getchar();
	while (isdigit(c)) t=t*10+c-48,c=getchar(); return t*f;
}

int a,n,cnt;

int power(int x,int y,int mod) {
	int t=1;
	for (;y;y>>=1,x=1LL*x*x%mod)
		if (y&1) t=1LL*t*x%mod;
	return t;
}

int main() {
	
	freopen("math.in","r",stdin);
	freopen("force.out","w",stdout);
	
	int cas=rd();
	while (cas--)
//	while (1)
	{
		cnt=0;
		a=rd(),n=rd();
		For (b,1,1<<n)
			if (power(a,b,1<<n)==power(b,a,1<<n)) {
//				printf("%d %d %d\n",a,b,1<<n);
				cnt++;
			}
//		printf("the number of answer -> %d\n\n",cnt);
		printf("%d\n",cnt);
	}
	return 0;
}

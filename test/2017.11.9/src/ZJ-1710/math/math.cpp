#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int mod;
inline ll fast_pow(ll a, ll b) {
	ll res = 1; a &= mod;
	for (; b; b >>= 1, a = (a * a) & mod)
		if (b & 1) res = (res * a) & mod;
	return res;
}
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	int T = read();
	while (T --) {
		int a = read(), n = read(), max_b = 1 << n, ans = 0;
		mod = max_b - 1;
		for (int b = 1; b <= max_b; b++)
			if (fast_pow(a, b) == fast_pow(b, a))
				ans ++;
		printf("%d\n", ans);
	}
	return 0;
}

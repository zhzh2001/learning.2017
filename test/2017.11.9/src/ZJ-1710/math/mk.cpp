#include <bits/stdc++.h>
using namespace std;
int main() {
	freopen("math.in", "w", stdout);
	srand(time(0));
	int T = 10, max_n = 20, max_ans = 1e9;
	cout << T << endl;
	while (T --)
		cout << rand() * rand() % max_ans + 1 << " " << rand() * rand() % max_n + 1 << endl;
}
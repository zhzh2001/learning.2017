#include <bits/stdc++.h>
#define ll long long
#define mod 1000000007
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
// pri.length = 11
#define M 10
int pri[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
int f[31][1 << M][31], g[31][1 << M];
inline ll calc(int x) {
	ll ans = 1;
	for (int i = 0; i < M; i++)
		if (x >> i & 1) ans *= pri[i];
	return ans;
}
inline int bit_count(int x) {
	int res = 0;
	while (x)
		x ^= x & -x, res ++;
	return res;
}
int main() {
	freopen("mul.in", "r", stdin);
	freopen("mul.out", "w", stdout);
	int T = read();
	while (T --) {
		int n = read(), k = read(), ans = 0;
		if (n > 30) {puts("70?"); continue;}
		memset(f, 0, sizeof f);
		memset(g, 0, sizeof g);
		f[0][0][0] = 1; g[0][0] = 1;
		for (int i = 1; i <= k; i++) {
			for (int j = 0; j < (1 << M); j++) if (g[i-1][j]) {
				int s = ((1 << M) - 1) ^ j;
				// 枚举可以加的
				for (int l = s; l; l = (l - 1) & s) if (calc(l) <= n) {
					// j -> 原来有的
					// l -> 要加上去的
					int ws = calc(l);
					for (int o = 0; o < ws; o++) if (f[i-1][j][o]) {
						// printf("f[%d][%d][%d] += f[%d][%d][%d]\n", i, j|l, ws, i-1, j, o);
						f[i][j|l][ws] += f[i-1][j][o];
						g[i][j|l] = 1;
					}
				}
			}
		}
		for (int i = 1; i <= k; i++) {
			for (int j = 0; j < (1 << M); j++) if (g[i][j]) {
				for (int o = 1; o <= n; o++) if (f[i][j][o]) {
					// printf("f[%d][%d][%d] = %d\n", i, j, o, f[i][j][o]);
					ans += i < k ? f[i][j][o] * 2 : f[i][j][o];
				}
			}
		}
		printf("%d\n", ans + 1);
	}
}

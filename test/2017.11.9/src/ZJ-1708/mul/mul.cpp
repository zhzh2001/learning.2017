#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000000
#define zyy 1000000007
#define N 505
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(int x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
const int p[]={2,3,5,7,11,13,17,19};
struct data{
	int bin,val;
	bool operator <(const data &a)const{
		return val<a.val;
	}
}a[N];
int cnt,bin[9],f[N][256],g[N][256];
inline void update(int &x,int y){x+=y;if(x>zyy) x-=zyy;}
inline void solve(int n,int k){
	cnt=0;
	For(i,1,n){
		int x=i,val=0,flag=1;
		For(j,0,7){
			if(x%(p[j]*p[j])==0){flag=0;break;}
			if(x%p[j]==0) val|=bin[j],x/=p[j];
		}
		if(flag) a[++cnt]=(data){val,x};
	}
	memset(f,0,sizeof f);
	sort(a+1,a+1+cnt);f[0][0]=1;
	For(i,1,cnt){
		data x=a[i];
		if(i==1||x.val==1||x.val!=a[i-1].val){
			For(j,0,k) For(k,0,255) g[j][k]=f[j][k];
		}
		Rep(j,k-1,0){
			For(K,0,255){
				if(K&x.bin) continue;
				update(g[j+1][K|x.bin],f[j][K]);
			}
		}
		if(i==cnt||x.val==1||x.val!=a[i+1].val){
			For(j,0,k) For(k,0,255) f[j][k]=g[j][k];
		}
	}
	int ans=0;
	For(i,1,k) For(j,0,255) update(ans,f[i][j]);
	writeln(ans);
}
main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	bin[0]=1;For(i,1,7) bin[i]=bin[i-1]<<1;
	int T=read();
	while(T--){
		int n=read(),k=read();
		solve(n,k);
	}
	return 0;
}

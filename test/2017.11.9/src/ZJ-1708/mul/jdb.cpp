#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000000
#define zyy 1000000007
#define N 100005
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(int x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n;
const int p[]={2,3,5,7,11,13,17,19,23,29};
int main(){
	n=read();int ans=0;
	For(i,1,n){
		int flag=1;
		For(k,0,9){
			if(i%(p[k]*p[k])==0) flag=0;
		}
		if(flag) ans++;
	}
	cout<<ans<<endl;
	For(i,1,n) For(j,i+1,n){
		int flag=1,x=i,y=j;
		For(k,0,9){
			if((x*y)%(p[k]*p[k])==0) flag=0;
			else{
				if(x%p[k]==0) x/=p[k];
				if(y%p[k]==0) y/=p[k];
			}
		}
		if(flag&&(x!=y||x==1)) ans++;
	}
	writeln(ans);
	return 0;
}

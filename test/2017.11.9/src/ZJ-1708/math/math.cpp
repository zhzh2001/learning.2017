#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000000
#define N 505
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(int x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,a,zyy;
inline int ksm(int x,int y){
	int sum=1;
	while(y){
		if(y&1) sum=1ll*sum*x%zyy;
		y>>=1;
		if(y) x=1ll*x*x%zyy;
	}
	return sum;
}
inline void solve(){
	a=read();n=read();
	zyy=1<<n;int ans=0;
	For(b,1,zyy){
		int p=ksm(a,b),q=ksm(b,a);
		if(p==q) ans++;
	}
	writeln(ans);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int T=read();
	while(T--) solve();
	return 0;
}

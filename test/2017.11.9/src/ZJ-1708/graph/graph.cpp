#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000000
#define zyy 1000000007
#define N 100005
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(int x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,m,cnt,head[N];
struct edge{int next,to;}e[N<<2];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
int dfn[N],low[N],ind,q[N],vis[N],top;
int bl[N],scc;
inline void tarjan(int x,int fa){
	dfn[x]=low[x]=++ind;
	q[++top]=x;vis[x]=1;
	for(int i=head[x];i;i=e[i].next)
		if(!dfn[e[i].to]){
			tarjan(e[i].to,x);low[x]=min(low[x],low[e[i].to]);
		}else if(e[i].to!=fa&&low[x]>dfn[e[i].to]) low[x]=dfn[e[i].to];
	if(dfn[x]==low[x]){
		scc++;int now=0;
		while(now!=x) now=q[top--],bl[now]=scc,vis[now]=0;
	}
}
int in[N];
inline void rebuild(){
	cnt=0;
	For(x,1,n){
		for(int i=head[x];i;i=e[i].next){
			if(bl[x]!=bl[e[i].to]) in[bl[x]]++;
		}
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	For(i,1,m){
		int x=read(),y=read();
		insert(x,y);
	}
	tarjan(1,0);
	rebuild();
	int ans=0;
	For(i,1,scc) if(in[i]==1) ans++;
	writeln((ans+1)/2);
	return 0;
}

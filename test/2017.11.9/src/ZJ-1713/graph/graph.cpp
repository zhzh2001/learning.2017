//#include<iostream>
#include<algorithm>
#include<math.h>
#include<fstream>
#include<stdio.h>
#include<map>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
map<pair<int,int>,int>mp;
int n,m;
int a[100003],b[100003];
int head[100003],nxt[400003],to[400003],tot;
inline void add(int x,int y){
	to[++tot]=y;  nxt[tot]=head[x]; head[x]=tot;
}
int head2[100003],nxt2[400003],to2[400003],tot2;
inline void add2(int x,int y){
	to2[++tot2]=y;  nxt2[tot2]=head2[x]; head2[x]=tot2;
}
int son[100003];
int sum=0;
void dfs(int x,int fa){
	son[x]=0;
	int mx=0;
	for(int i=head2[x];i;i=nxt2[i]){
		if(to2[i]!=fa){
			dfs(to2[i],x);
			son[x]+=son[to2[i]];
			mx=max(mx,son[to2[i]]);
		}
	}
	if(x==1){
		if(mx>son[x]/2){
			fout<<mx<<endl;
		}else{
			fout<<(son[x]+1)/2<<endl;
		}
	}
	if(son[x]==0){
		son[x]=1;
	}
}
int dfn[100003],f[100003],low[100003],st[100003],len,index,point;
int p[100003];
bool inst[100003];
void tarjan(int x,int fa){
	dfn[x]=low[x]=++index;
	st[++len]=x;//队列 
	inst[x]=true;//标记，是否在队列中
	for(int i=head[x];i;i=nxt[i]){
		if(!dfn[to[i]]&&to[i]!=fa){
			tarjan(to[i],x);
			low[x]=min(low[x],low[to[i]]);
		}else{
			if((inst[to[i]])&&(to[i]!=fa)){
				low[x]=min(low[x],dfn[to[i]]);
			}
		}
	}
	if(dfn[x]==low[x]){
		++point;//部分数 
		for(;st[len]!=x;){
			inst[st[len]]=false;
			f[st[len]]=point;//某一个点属于第几部分
			++p[point];//某一部分的点的个数
			--len;
		}
		inst[st[len]]=false;
		f[st[len]]=point;
		++p[point];
		--len;
	}
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=m;i++){
		fin>>a[i]>>b[i];
		add(a[i],b[i]),add(b[i],a[i]);
	}
	if(m==n-1){
		for(int i=1;i<=m;i++){
			add2(a[i],b[i]),add(b[i],a[i]);
		}
		dfs(1,0);
		return 0;
	}
	tarjan(1,0);
	for(int i=1;i<=n;i++){
		for(int j=head[i];j;j=nxt[j]){
			if(f[i]!=f[to[j]]&&!mp[make_pair(f[i],f[to[j]])]){
				mp[make_pair(f[i],f[to[j]])]=1;
				mp[make_pair(f[to[j]],f[i])]=1;
				add2(f[i],f[to[j]]),add2(f[to[j]],f[i]);
			}
		}
	}
	dfs(1,0);
	return 0;
}
/*

in:
7 6
1 2
2 3
3 4
4 5
5 6
6 7
in:
7 7
1 2
2 3
3 4
2 5
4 5
5 6
5 7

out:
2

*/

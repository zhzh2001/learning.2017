/*#include<iostream>
#include<algorithm>
#include<math.h>
#include<fstream>
#include<string.h>
#include<stdio.h>
using namespace std;
//ifstream fin("mul.in");
//ofstream fout("mul.out");
const int mod=1000000007;
int T,n,k;
int prime[103],tot;
int ping[103];
int num[503],totnum;
int dp[503][503],lenn[503];
int xx[503][503];
int jiyi[33][73][130][33];
int dfs(int now,int on,int zt,int tot){
	int &fanhui=jiyi[now][on][zt][tot];
	if(fanhui!=-1){
		return fanhui;
	}
	if(now>n){
		if(tot<k){
			return fanhui=1;
		}else{
			return fanhui=0;
		}
	}
	if(on>lenn[now]){
		return fanhui=dfs(now+1,1,zt,tot);
	}
	fanhui=dfs(now,on+1,zt,tot);
	if((!(zt&dp[now][on]))&&xx[now][on]<=n&&tot){
		if(now==1){
			fanhui+=dfs(now,on+1,(zt|dp[now][on]),tot-1);
		}else{
			fanhui+=dfs(now+1,1,(zt|dp[now][on]),tot-1);
		}
		fanhui%=mod;
	}
	return fanhui;
}
int main(){
	cin>>T;
	for(int i=2;i*i<=500;i++){
		bool yes=false;
		for(int j=2;j*j<=i;j++){
			if(i%j==0){
				yes=true;
				break;
			}
		}
		if(!yes){
			prime[++tot]=i;
		}
	}
	for(int i=1;i<=500;i++){
		bool yes=false;
		for(int j=1;j<=tot;j++){
			if((i%(prime[j]*prime[j]))==0){
				yes=true;
				break;
			}
		}
		if(!yes){
			num[++totnum]=i;
		}
	}
	for(int i=1;i<=totnum;i++){
		int x=num[i],zt=0;
		for(int j=1;j<=8;j++){
			while(x%prime[j]==0){
				x/=prime[j];
				zt+=(1<<(j-1));
			}
		}
		dp[x][++lenn[x]]=zt;
		xx[x][lenn[x]]=num[i];
	}
	while(T--){
		memset(jiyi,-1,sizeof(jiyi));
		cin>>n>>k;
		cout<<dfs(1,1,0,k)<<endl;
	}
	return 0;
}

in:
3
1 1
6 4
4 2

out:
1
19
6

*/
//#include<iostream>
#include<algorithm>
#include<math.h>
#include<fstream>
#include<string.h>
#include<stdio.h>
using namespace std;
ifstream fin("mul.in");
ofstream fout("mul.out");
const int mod=1000000007;
int T,n,k;
int prime[103],tot;
int ping[103];
int num[503],totnum;
int dp[503][503],lenn[503];
int xx[503][503];
int mdnd[503];
int jiyi[91][130][503];
int jiyi2[80][130][503];
int dfs2(int now,int zt,int tot);
int dfs(int now,int zt,int tot){
	int &fanhui=jiyi[now][zt][tot];
	if(fanhui!=-1){
		return fanhui;
	}
	int x=mdnd[now];
	if(x>n||x>mdnd[0]){
		if(tot<k){
			return fanhui=1;
		}else{
			return fanhui=0;
		}
	}
	fanhui=dfs(now+1,zt,tot);
	for(int i=1;i<=lenn[x];i++){
		if((!(zt&dp[x][i]))&&xx[x][i]<=n&&tot){
			fanhui+=dfs(now+1,(zt|dp[x][i]),tot-1);
			fanhui%=mod;
		}
	}
	return fanhui;
}
int dfs2(int now,int zt,int tot){
	int &fanhui=jiyi2[now][zt][tot];
	if(fanhui!=-1){
		return fanhui;
	}
	if(now>lenn[1]||xx[1][now]>n){
		return fanhui=dfs(1,zt,tot);
	}
	fanhui=dfs2(now+1,zt,tot);
	if((!(zt&dp[1][now]))&&xx[1][now]<=n&&tot){
		fanhui+=dfs2(now+1,(zt|dp[1][now]),tot-1);
		fanhui%=mod;
	}
	return fanhui;
}
int main(){
	fin>>T;
	for(int i=2;i*i<=500;i++){
		bool yes=false;
		for(int j=2;j*j<=i;j++){
			if(i%j==0){
				yes=true;
				break;
			}
		}
		if(!yes){
			prime[++tot]=i;
		}
	}
	for(int i=1;i<=500;i++){
		bool yes=false;
		for(int j=1;j<=tot;j++){
			if((i%(prime[j]*prime[j]))==0){
				yes=true;
				break;
			}
		}
		if(!yes){
			num[++totnum]=i;
		}
	}
	for(int i=1;i<=totnum;i++){
		int x=num[i],zt=0;
		for(int j=1;j<=8;j++){
			while(x%prime[j]==0){
				x/=prime[j];
				zt+=(1<<(j-1));
			}
		}
		dp[x][++lenn[x]]=zt;
		xx[x][lenn[x]]=num[i];
	}
	for(int i=2;i<=500;i++){
		if(lenn[i]){
			mdnd[++mdnd[0]]=i;
		}
	}
	while(T--){
		memset(jiyi2,-1,sizeof(jiyi2));
		memset(jiyi,-1,sizeof(jiyi));
		fin>>n>>k;
		fout<<dfs2(1,0,k)<<endl;
	}
	return 0;
}
/*

in:
3
1 1
6 4
4 2

out:
1
19
6

*/

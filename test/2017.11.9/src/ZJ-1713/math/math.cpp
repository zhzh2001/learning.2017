//#include<iostream>
#include<algorithm>
#include<math.h>
#include<fstream>
#include<stdio.h>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
long long T,a[1003],n[1003];
inline long long qpow(long long a,long long b,long long mod){
	long long ret=1%mod;
	for(;b;b>>=1){
		if(b&1){
			ret=ret*a%mod;
		}
		a=a*a%mod;
	}
	return ret;
}
inline void baoli(int x){
	long long ans=0;
	for(int i=1;i<=(1<<n[x]);i++){
		if(qpow(a[x],i,(1<<n[x]))==qpow(i,a[x],(1<<n[x]))){
			ans++;
		}
	}
	fout<<ans<<endl;
}
int main(){
	fin>>T;
	bool gg=false;
	for(int i=1;i<=T;i++){
		fin>>a[i]>>n[i];
		if(n[i]>20){
			gg=true;
		}
	}
	if(!gg){
		for(int i=1;i<=T;i++){
			baoli(i);
		}
	}
	return 0;
}
/*

in:
2
2 3
2 2

out:
3
2

*/

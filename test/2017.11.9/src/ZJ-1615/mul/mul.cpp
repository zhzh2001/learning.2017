#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int o=1000000007;
const int p[]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47};
int b[1000],c[1000],f[40000][120];
int h,i,j,k,m,n,s,t;

int main()
{
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	scanf("%d",&t);
	for (h=1;h<=t;h++)
	{
		scanf("%d%d",&n,&m);
		m=min(m,100);
		for (i=1;i<=n;i++)
			for (j=0;j<15;j++)
				if (! (i%p[j]))
				{
					b[i]=b[i]|(1<<j);
					if (! ((i/p[j])%p[j]))
						c[i]=1;
				}
		memset(f,0,sizeof(f));
		f[0][0]=1;
		for (i=1;i<=n;i++)
			if (! c[i])
				for (j=(1<<15)-1;j>=0;j--)
					if ((j&b[i])==b[i])
						for (k=m;k>0;k--)
							f[j][k]=(f[j][k]+f[j^b[i]][k-1])%o;
		s=0;
		for (j=(1<<15)-1;j>=0;j--)
			for (k=1;k<=m;k++)
				s=(s+f[j][k])%o;
		printf("%d\n",s);
	}
	return 0;
}

#include <cstdio>
#include <algorithm>

using namespace std;

int edge[500000],next[500000],first[500000];
int point[500000],dfn[500000],low[500000],stack[500000],deg[500000];
int x[500000],y[500000];
int i,m,n,s,sum_edge,sum_stack,sum_point,dfs_clock;

inline void addedge(int x,int y)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],first[x]=sum_edge;
	return;
}

inline void tarjan(int x,int y)
{
	dfs_clock++,dfn[x]=low[x]=dfs_clock;
	sum_stack++,stack[sum_stack]=x;
	for (int i=first[x];i!=0;i=next[i])
		if (! dfn[edge[i]])
		{
			tarjan(edge[i],x);
			low[x]=min(low[x],low[edge[i]]);
		}
		else
			if (edge[i]!=y)
				low[x]=min(low[x],dfn[edge[i]]);
	if (dfn[x]==low[x])
	{
		sum_point++;
		while (stack[sum_stack]!=x)
			point[stack[sum_stack]]=sum_point,sum_stack--;
		point[stack[sum_stack]]=sum_point,sum_stack--;
	}
	return;
}

int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++)
	{
		scanf("%d%d",&x[i],&y[i]);
		addedge(x[i],y[i]),addedge(y[i],x[i]);
	}
	for (i=1;i<=n;i++)
		if (! dfn[i])
			tarjan(i,0);
	for (i=1;i<=m;i++)
		if (point[x[i]]!=point[y[i]])
		{
			deg[x[i]]++;
			if (deg[x[i]]==1)
				s++;
			if (deg[x[i]]==2)
				s--;
			deg[y[i]]++;
			if (deg[y[i]]==1)
				s++;
			if (deg[y[i]]==2)
				s--;
		}
	printf("%d",(s+1)/2);
	return 0;
}

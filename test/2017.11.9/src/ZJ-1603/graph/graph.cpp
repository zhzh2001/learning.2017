#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=500100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,L=1,h[N],ne[N],to[N],T,vis[N],du[N];
int ans,sta[N],top,be[N],ti,dfn[N],low[N];
void add(int x,int y){
	ne[++L]=h[x];h[x]=L;to[L]=y;
	ne[++L]=h[y];h[y]=L;to[L]=x;
}
void dfs(int x,int fa){
	dfn[x]=low[x]=++ti;
	int tt=top;sta[++top]=x;
	for(int y,k=h[x];k;k=ne[k])
	if(k!=(fa^1)){
		y=to[k];
		if(be[y])continue;
		if(dfn[y])low[x]=min(low[x],dfn[y]);
		else dfs(y,k),low[x]=min(low[x],low[y]);
	}
	if(dfn[x]==low[x]){
		T++;int x,y,k;
		for(k=top;k>tt;k--)
		be[sta[k]]=T;
		vis[T]=T;
		for(;top>tt;top--){
			x=sta[top];
			for(k=h[x];k;k=ne[k]){
				y=to[k];y=be[y];
				if(vis[y]==T)continue;
				du[T]++;vis[y]=T;
			}
		}if(du[T]==1)ans++;
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++)add(read(),read());
	dfs(1,0);printf("%d\n",(ans+1)/2);
	return 0;
}

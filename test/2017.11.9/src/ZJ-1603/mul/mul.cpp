#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=610,M=257,P=1e9+7;
const int p[N]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499};
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void A(int&x,int y){x+=y;if(x>=P)x-=P;}
int g[2][M][N],f[2][M][N],n,m,ans,be[N];
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	register int nw,pr,i,j,k,x,T;
	n=500;
	for(i=1;i<=n;i++){
		be[i]=0;x=i;
		for(j=0;j<8;j++)
		if(x%p[j]==0){
			be[i]|=1<<j;
			x/=p[j];
		}if(x>1)be[i]=-1;
	}
	for(T=read();T--;){
		n=read();m=read();
		for(j=0;j<256;j++)
		for(k=0;k<=m;k++)g[1][j][k]=0;
		g[1][0][0]=1;
		nw=1;pr=0;
		for(i=1;i<=n;i++)
		if(be[i]>=0){
			swap(nw,pr);
			for(j=0;j<256;j++)
			for(k=0;k<=m;k++){
				g[nw][j][k]=g[pr][j][k];
				if((j&be[i])==be[i]&&k>0)
				A(g[nw][j][k],g[pr][j^be[i]][k-1]);
			}
		}
		for(j=0;j<256;j++)
		for(k=0;k<=m;k++)f[1][j][k]=g[nw][j][k];
		nw=1;pr=0;
		for(x=8;x<95;x++){
			swap(nw,pr);
			for(j=0;j<256;j++)
			for(k=0;k<=m;k++)f[nw][j][k]=f[pr][j][k];
			for(i=n/p[x];i;i--)
			if(be[i]>=0){
				for(j=0;j<256;j++)if((j&be[i])==be[i])
				for(k=1;k<=m;k++)A(f[nw][j][k],f[pr][j^be[i]][k-1]);
			}
		}
		ans=P-1;
		for(j=0;j<256;j++)
		for(k=0;k<=m;k++)A(ans,f[nw][j][k]);
		printf("%d\n",ans);
	}return 0;
}

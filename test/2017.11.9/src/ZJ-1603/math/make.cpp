#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,Q,T,i,j,k,x,y,l,r,z,o,a[N];
inline int rll(){
	return rand()<<15|rand();
}
int main(){
	freopen("math.in","w",stdout);
	srand(unsigned(time(0)));
	T=10;
	printf("%d\n",T);
	for(;T--;){
		n=rand()%20+1;
		m=rll()%P+1;
		printf("%d %d\n",m,n);
	}
	return 0;
}

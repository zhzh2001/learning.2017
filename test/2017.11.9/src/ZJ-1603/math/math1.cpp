#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int i,A,B,L,R,T,P,n,ans;
inline int ksm(int x,int y){
	int z=1;for(;y;y>>=1,x=1ll*x*x%P)
	if(y&1)z=1ll*z*x%P;return z;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math1.out","w",stdout);
	for(T=read();T--;){
		A=read();n=read();
		P=1;L=1;ans=0;
		for(i=1;i<=n;i++)P*=2;
		for(B=1;B<=P;B++){
			L=1ll*L*A%P;
			R=ksm(B,A);
			if(L==R)
			ans++;
		}printf("%d\n",ans);
	}return 0;
}

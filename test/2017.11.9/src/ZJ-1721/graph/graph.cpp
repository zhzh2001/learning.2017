#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=400005;
int vis[N],la[N],to[M],pr[M],dfn[N],low[N],st[N],top,ti,c[N],n,m,x,y,in[N],tot,cnt,ans,rd[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
void tarjan(int x,int fa)
{
	dfn[x]=low[x]=++ti;
	vis[x]=in[x]=1;
	st[++top]=x;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			if (!vis[to[i]])
				tarjan(to[i],x);
			low[x]=min(low[x],low[to[i]]);
		}
	if (low[x]==dfn[x])
	{
		++tot;
		while (st[top+1]!=x)
		{
			c[st[top]]=tot;
			in[st[top]]=0;
			--top;
		}
	}
}
inline void rebuild()
{
	for (int i=1;i<=n;++i)
		for (int j=la[i];j;j=pr[j])
			if (c[i]!=c[to[j]])
				++rd[c[i]];
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	tarjan(1,0);
	//for (int i=1;i<=n;++i) cout<<c[i]<<' ';
	rebuild();
	for (int i=1;i<=tot;++i)
		if (rd[i]==1)
			++ans;
	write((ans+1)/2);
	return 0;
}
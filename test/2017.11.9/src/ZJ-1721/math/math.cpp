#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int n,m,t,ans;
unsigned k,mod;
unsigned ksm(unsigned x,int y)
{
	unsigned ans=1;
	for (;y;y/=2,x*=x)
		if (y&1)
			ans*=x;
	return ans;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(t);
	while (t--)
	{
		ans=0;
		read(n);
		read(m);
		if (n&1)
		{
			write(1);
			continue;
		}
		if (n==2)
		{
			write(m);
			continue;
		}
		k=1;
		mod=1<<m;
		--mod;
		for (unsigned i=1;i<=mod+1;++i)
		{
			k*=n;
			if ((k&mod)==(ksm(i,n)&mod))
				++ans;
		}
		write(ans);
	}
	return 0;
}
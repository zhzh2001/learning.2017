#include<bits/stdc++.h>
using namespace std;
#define int long long
int n,k,ans,t;
inline bool pd(int x)
{
	for (int i=2;i*i<=x;++i)
		if (x%(i*i)==0)
			return 0;
	return 1;
}
void dfs(int x,int y,int z)
{
	if (pd(y))
		++ans;
	else
		return;
	if (z==k)
		return;
	for (int i=x;i<=n;++i)
		dfs(i+1,y*i,z+1);
}
signed main()
{
	freopen("mul.in","r",stdin);
	freopen("mul.ans","w",stdout);
	cin>>t;
	while (t--)
	{
		cin>>n>>k;
		ans=-1;
		dfs(1,1,0);
		cout<<ans<<'\n';
	}
	return 0;
}
#pragma GC\
C optimize 2
#include<bits/stdc++.h>
using namespace std;
const int z[8]={2,3,5,7,11,13,17,19},N=505,M=1<<8,mod=1e9+7;
int f[310][M],g[310][M],n,m,t,cnt,ans;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
struct data
{
	int x,y;
	bool operator<(const data &a)const
	{
		return y<a.y;
	}
}a[N];
inline void fj(int x)
{
	data p;
	p.x=0;
	for (int i=0;i<8;++i)
	{
		if (x%(z[i]*z[i])==0)
			return;
		if (x%z[i]==0)
		{
			x/=z[i];
			p.x|=1<<i;
		}
	}
	p.y=x;
	a[++cnt]=p;
}
int main()
{
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	read(t);
	while (t--)
	{
		read(n);
		read(m);
		cnt=0;
		for (int i=1;i<=n;++i)
			fj(i);
		m=min(m,cnt);
		sort(a+1,a+1+cnt);
		memset(f,0,sizeof(f));
		f[0][0]=1;
		for (int i=1;i<=cnt;++i)
		{
			if (a[i].y!=a[i-1].y||a[i].y==1)
				memcpy(g,f,sizeof(f));
			for (int j=0;j<M;++j)
				if (!(a[i].x&j))
					for (int k=0;k<m;++k)
						g[k+1][j|a[i].x]+=f[k][j],g[k+1][j|a[i].x]-=g[k+1][j|a[i].x]>mod?mod:0;
			if (a[i].y!=a[i+1].y||a[i].y==1||i==cnt)
				memcpy(f,g,sizeof(g));
		}
		ans=0;
		for (int i=1;i<=m;++i)
			for (int j=0;j<M;++j)
				ans+=f[i][j],ans-=ans>mod?mod:0;
		write(ans);
	}
	return 0;
}
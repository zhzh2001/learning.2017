#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
inline int mi(int a,int b,int MOD){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;
		y=y*y%MOD;b>>=1;
	}
	return x;
}
signed main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int T=read();
	while(T--){
		int m=read(),n=read(),ans=0;
		for(int i=1;i<=(1<<n);i++){
			int a=mi(m,i,(1<<n)),b=mi(i,m,(1<<n));
			if(a==b)ans++;
		}
		writeln(ans);
	}
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
const int MOD=1e9+7;
const int N=500;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,pri[510];
int b[1010],cnt=0,jie[510];
inline void get(){
	jie[0]=1;
	for(int i=1;i<=N;i++)jie[i]=jie[i-1]*i%MOD;
	for(int i=2;i<=N;i++){
		if(!b[i])pri[++cnt]=i;
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>N)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
inline int mi(int a,int b){
	int x=1,y=a;
	while(b){
		if(b&1)x=x*y%MOD;
		y=y*y%MOD;b>>=1;
	}
	return x;
}
int f[31][10010];
signed main()
{
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	get();
	int T=read();
	while(T--){
		n=read();m=read();
		if(n==1){
			if(m==1)puts("1");
			else puts("0");
			continue;
		}
		memset(f,0,sizeof f);
		int L=upper_bound(pri+1,pri+cnt+1,n)-pri-1;
		f[0][0]=1;
		int ans=0;
		for(int i=1;i<=m;i++){
			for(int j=2;j<=n;j++){
				bool flag=1;int s=j,q=0;
				for(int k=1;pri[k]*pri[k]<=s;k++)if(s%pri[k]==0){
					q|=(1<<(k-1));s/=pri[k];if(s%pri[k]==0){flag=0;break;}
				}
				if(!flag)continue;
				if(s){
					int p=lower_bound(pri+1,pri+cnt+1,s)-pri;
					q|=(1<<(p-1));
				}
				for(int k=0;k<(1<<L);k++)if((q&k)==0)f[i][k|q]=(f[i][k|q]+f[i-1][k])%MOD;
			}
			for(int j=0;j<(1<<L);j++)ans=(ans+f[i][j]*mi(jie[i],MOD-2)%MOD)%MOD;
		}
		for(int i=0;i<m;i++)
			for(int j=0;j<(1<<L);j++)ans=(ans+f[i][j]*mi(jie[i],MOD-2))%MOD;
		writeln(ans);
	}
	return 0;
}

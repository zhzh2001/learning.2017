#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int ans,n,m;
inline void dfs(int x,int sum,int p){
	if(sum>100000000000)return;
	if(x==m+1)return;
	int q=sum;bool flag=1;
	for(int i=2;i*i<=q;i++)if(q%i==0){
		q/=i;if(q%i==0){flag=0;break;}
	}
	if(x>0&&flag)ans=(ans+1)%MOD;
	for(int i=p;i<=n;i++)dfs(x+1,sum*i,i+1);
}
signed main()
{
	freopen("mul.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int T=read();
	while(T--){
		n=read();m=read();ans=0;
		dfs(0,1,1);
		writeln(ans);
	}
	return 0;
}

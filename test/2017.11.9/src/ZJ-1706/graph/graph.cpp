#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
vector<int>e[200010];
int n,m,cnt=0;
bool b[200010];
int nedge=0,p[400010],nex[400010],head[400010],l[200010];
int dfn[200010],low[200010],s[200010],d[200010],top;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void tarjan(int x,int fa){
	dfn[x]=low[x]=++cnt;
	s[++top]=x;b[x]=1;
	for(int k=head[x];k;k=nex[k]){
		if(!dfn[p[k]])tarjan(p[k],x),low[x]=min(low[x],low[p[k]]);
		else if(p[k]!=fa&&b[p[k]])low[x]=min(low[x],dfn[p[k]]);
	}
	if(low[x]==dfn[x]){
		d[0]++;d[d[0]]=1;
		while(s[top]!=x){
			l[s[top]]=d[0];d[d[0]]++;
			b[s[top]]=0;top--;
		}
		l[x]=d[0];b[x]=0;top--;
	}
}
inline void dfs(int x,int fa){
	s[x]=1;bool flag=1;
	int S=e[x].size();
	for(int i=0;i<S;i++){
		int to=e[x][i];
		if(to!=fa){
			if(flag)s[x]=0,flag=0;
			dfs(to,x);s[x]+=s[to];
		}
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	for(int i=1;i<=n;i++)if(!dfn[i])tarjan(i,0);
	int Cnt=d[0];
	memset(d,0,sizeof d);memset(s,0,sizeof s);
	for(int i=1;i<=n;i++)
		for(int k=head[i];k;k=nex[k]){
			int to=p[k];
			if(l[i]!=l[to]){
				e[l[i]].push_back(l[to]);
				d[l[i]]++;
			}
		}
	int rt=0;
	for(int i=1;i<=Cnt;i++)if(d[i]>d[rt])rt=i;
	dfs(rt,0);
	writeln((s[rt]+1)/2);
	return 0;
}

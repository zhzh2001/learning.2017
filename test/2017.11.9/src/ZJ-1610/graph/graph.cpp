#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=200005;

int lines,front[N];
struct Edge{
	int next,to;
}E[N*2];
inline void Addline(int u,int v){
	E[++lines]=(Edge){front[u],v};front[u]=lines;
}
int n,m;
int dfn[N],low[N],tim;
int sta[N],ins[N],top;
int loc[N];
void Dfs(int u,int last){
	dfn[u]=low[u]=++tim;
	ins[sta[++top]=u]=true;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((i^1)!=last){
			if (!dfn[v=E[i].to]){
				Dfs(v,i);
				low[u]=min(low[u],low[v]);
			}
			else if (ins[v]) low[u]=min(low[u],dfn[v]);
		}
	}
	if (dfn[u]==low[u]){
		while (sta[top]!=u){
			loc[sta[top]]=u;
			ins[sta[top]]=false;top--;
		}
		loc[sta[top]]=u;
		ins[sta[top]]=false;top--;
	}
}
int u[N],v[N],dgr[N],B[N];
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),m=read();
	lines=1;
	Rep(i,1,m){
		u[i]=read(),v[i]=read();
		Addline(u[i],v[i]),Addline(v[i],u[i]);
	}
	Dfs(1,0);
	Rep(i,1,m) if (loc[u[i]]!=loc[v[i]]){
		dgr[loc[u[i]]]++;
		dgr[loc[v[i]]]++;
	}
//	Rep(i,1,n) printf("loc[%d]=%d dgr=%d\n",i,loc[i],dgr[loc[i]]);
	int Ans=0;
	Rep(i,1,n) if (!B[loc[i]]){
		B[loc[i]]=1;
		if (dgr[loc[i]]==1) Ans++;
	}
	printf("%d\n",(Ans+1)/2);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int Mod;
inline int Pow(int base,int k){
	int res=1;
	for (int i=1;i<=k;i*=2,base=base*base){
		if (i&k) res=1ll*res*base;
	}
	return res;
}
inline int Pow(int base,int k,int Mod){
	int res=1;
	for (int i=1;i<=k;i*=2,base=1ll*base*base%Mod){
		if (i&k) res=1ll*res*base%Mod;
	}
	return res;
}
map<int,int>Map;
void Work(int a,int n){
	if (a==0){
		puts("0");return;
	}
	if (a==1){
		puts("1");return;
	}
	Mod=Pow(2,n);
	int cnt=0;
	for (int k=a;k%2==0;k/=2) cnt++;
	if (cnt==0){
		puts("1");return;
	}
	int limit=(n-1)/cnt+1,Ans=0;
	int cef=Pow(2,(n-1)/a+1);
	limit=max(limit,cef);
//	printf("cnt=%d\n",cnt);
//	printf("limit=%d\n",limit);
//	printf("cef=%d\n",cef);
	Ans+=(Mod-limit)/cef+1;
	Rep(b,1,min(100,limit-1)) if (Pow(a,b,Mod)==Pow(b,a,Mod)) Ans++;
	printf("%d\n",Ans);
//	Rep(b,1,limit) if (Pow(a,b,Mod)==Pow(b,a,Mod)) Ans++;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int T=read();
	Rep(i,1,T){
		int a=read(),n=read();
		Work(a,n);
	}
}
/*
1
482790186 1
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<cmath>
#include<cstdlib>
#include<iostream>
#include<map>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

#define mk(a,b) make_pair(a,b)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

int Stv[1<<8];

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=505;
const int Block=23;
const int Mod=1e9+7;
const int Max=8;

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}

struct node{
	int val,fab,bol,mat;
	void clear(){
		val=fab=bol=mat=0;
	}
}P[N];
int Map[25];
inline void Def(int val){
	int tim=0,pos=val;
	P[pos].clear();
	P[pos].val=val;
	for (int i=2;i<=22;i++){
		if (val%i==0){
			tim++;int cnt=0;
			while (val%i==0){
				val/=i;cnt++;
			}
			P[pos].fab|=1<<Map[i]-1;
			if (cnt>1) P[pos].bol=true;
		}
	}
	if (val>1) P[pos].mat=val;
		else P[pos].mat=pos;
}
inline bool Cmp(node A,node B){
	return A.mat<B.mat;
}
int f[N][1<<Max],f_[N][1<<Max];
void Work(){
	int n=read(),k=read();
	Rep(i,1,n) Def(i);
	sort(P+1,P+n+1,Cmp);
	memset(f,0,sizeof(f));
	f[0][0]=1;
	for (int i=1,j;i<=n;i=j+1){
		j=i;while (j<n && P[j+1].mat==P[i].mat) j++;
		memset(f_,0,sizeof(f_));
		Rep(t,i,j) if (!P[t].bol){
//			printf("tour at %d\n",P[t].val);
			Rep(c,1,k) Rep(fab,0,(1<<Max)-1) if (f[c-1][fab]){
				if (fab&P[t].fab) continue;
				Add(f_[c][fab^P[t].fab],f[c-1][fab]);
//				printf("%d^%d->%d\n",fab,P[t].fab,f_[c][fab^P[t].fab]);
			}
		}
		Rep(c,1,k) Rep(fab,0,(1<<Max)-1) Add(f[c][fab],f_[c][fab]);
	}
	int Ans=0;
	Rep(c,1,k) Rep(fab,0,(1<<Max)-1) Add(Ans,f[c][fab]);
	printf("%d\n",Ans);
}
int main(){
	freopen("mul.in","r",stdin);
	freopen("mul.out","w",stdout);
	Map[2]=1;Map[3]=2;Map[5]=3;Map[7]=4;
	Map[11]=5;Map[13]=6;Map[17]=7;Map[19]=8;
	Map[23]=9;
	int T=read();
	Rep(i,1,T) Work();
}
/*
3
6 4
1 1
4 2
*/

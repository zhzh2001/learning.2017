#include<fstream>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
int qpow(int a,int b)
{
	int ans=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}while(b/=2);
	return ans;
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int a,n;
		fin>>a>>n;
		int mask=(1<<n)-1,p=1,ans=0;
		for(int b=1;b<=(1<<n);b++)
		{
			p*=a;
			ans+=(p&mask)==(qpow(b,a)&mask);
		}
		fout<<ans<<endl;
	}
	return 0;
}

#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
const int N=100005,M=400005;
int head[N],v[M],nxt[M],e,dfn[N],t,low[N],belong[N],scc,s[N],sp,deg[N];
inline void add_edge(int u,int v)
{
	::v[e]=v;
	nxt[e]=head[u];
	head[u]=e++;
}
void dfs(int k,int fat)
{
	dfn[k]=low[k]=++t;
	s[++sp]=k;
	for(int i=head[k];~i;i=nxt[i])
		if(!dfn[v[i]])
		{
			dfs(v[i],i);
			low[k]=min(low[k],low[v[i]]);
		}
		else if((i^1)!=fat)
			low[k]=min(low[k],dfn[v[i]]);
	if(low[k]==dfn[k])
	{
		++scc;
		while(s[sp]!=k)
			belong[s[sp--]]=scc;
		belong[k]=scc;
		sp--;
	}
}
int main()
{
	int n,m;
	fin>>n>>m;
	fill(head+1,head+n+1,-1);
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1,0);
	for(int i=1;i<=n;i++)
		for(int j=head[i];~j;j=nxt[j])
			if(belong[i]!=belong[v[j]])
			{
				deg[belong[i]]++;
				deg[belong[v[j]]]++;
			}
	fout<<(count(deg+1,deg+scc+1,2)+1)/2<<endl;
	return 0;
}

#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("mul.in");
ofstream fout("mul.ans");
const int p[]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59},pn=15,N=61,MOD=1e9+7;
int f[N][1<<pn][N],mask[N];
int main()
{
	for(int i=1;i<=60;i++)
	{
		int x=i;
		for(int j=0;j<pn;j++)
		{
			int cnt=0;
			while(x%p[j]==0)
			{
				x/=p[j];
				cnt++;
			}
			if(cnt>1)
			{
				mask[i]=-1;
				break;
			}
			if(cnt)
				mask[i]|=1<<j;
		}
	}
	int t;
	fin>>t;
	while(t--)
	{
		int n,k;
		fin>>n>>k;
		fill_n(&f[0][0][0],sizeof(f)/sizeof(int),0);
		f[0][0][0]=1;
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<1<<pn;j++)
				for(int k=0;k<=i;k++)
				{
					(f[i+1][j][k]+=f[i][j][k])%=MOD;
					if(~mask[i+1]&&(j&mask[i+1])==0)
						(f[i+1][j|mask[i+1]][k+1]+=f[i][j][k])%=MOD;
				}
			int ans=0;
			for(int j=1;j<=i+1;j++)
			{
				for(int s=0;s<1<<pn;s++)
					(ans+=f[i+1][s][j])%=MOD;
				fout<<ans<<endl;
			}
		}
	}
	return 0;
}

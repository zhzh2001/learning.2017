#include <fstream>
#include <map>
#include <algorithm>
using namespace std;
ifstream fin("measurement.in");
ofstream fout("measurement.out");
const int N = 100005;
struct day
{
	int date, cow, delta;
	bool operator<(const day &rhs) const
	{
		return date < rhs.date;
	}
} a[N];
int main()
{
	int n, g;
	fin >> n >> g;
	for (int i = 1; i <= n; i++)
		fin >> a[i].date >> a[i].cow >> a[i].delta;
	sort(a + 1, a + n + 1);
	map<int, int> cnt, val;
	val[0] = n + 1;
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		int &ref = cnt[a[i].cow];
		int predcnt = val[ref]--;
		bool predtop = ref == val.rbegin()->first;
		if (predcnt == 1)
			val.erase(ref);
		ref += a[i].delta;
		int nowcnt = ++val[ref];
		bool nowtop = ref == val.rbegin()->first;
		ans += (predtop && (!nowtop || predcnt > 1 || nowcnt > 1)) || (!predtop && nowtop);
	}
	fout << ans << endl;
	return 0;
}
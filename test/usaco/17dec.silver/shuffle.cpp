#include <fstream>
using namespace std;
ifstream fin("shuffle.in");
ofstream fout("shuffle.out");
const int N = 100005;
int a[N], ans, s[N], sp;
bool vis[N], inS[N];
void dfs(int k)
{
	vis[k] = inS[k] = true;
	s[++sp] = k;
	if (inS[a[k]])
	{
		ans++;
		for (int i = sp; s[i] != a[k]; i--)
			ans++;
	}
	else if (!vis[a[k]])
		dfs(a[k]);
	sp--;
	inS[k] = false;
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	for (int i = 1; i <= n; i++)
		if (!vis[i])
			dfs(i);
	fout << ans << endl;
	return 0;
}
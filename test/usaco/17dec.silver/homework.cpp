#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("homework.in");
ofstream fout("homework.out");
const int N = 100005;
int a[N], ans[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	int low = a[n], sum = a[n], cnt = 0;
	long long x = 0, y = 0;
	for (int i = n - 1; i > 1; i--)
	{
		sum += a[i];
		low = min(low, a[i]);
		long long v1 = (sum - low) * y, v2 = (n - i) * x;
		if (!x || v1 > v2)
		{
			x = sum - low;
			y = n - i;
			ans[cnt = 1] = i - 1;
		}
		else if (v1 == v2)
			ans[++cnt] = i - 1;
	}
	for (int i = cnt; i; i--)
		fout << ans[i] << endl;
	return 0;
}
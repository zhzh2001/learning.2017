#include <fstream>
#include <algorithm>
#include <string>
using namespace std;
ifstream fin("measurement.in");
ofstream fout("measurement.out");
const int N = 105;
struct day
{
	int date, cow, delta;
	bool operator<(const day &rhs) const
	{
		return date < rhs.date;
	}
} a[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		string cow;
		fin >> a[i].date >> cow >> a[i].delta;
		if (cow == "Bessie")
			a[i].cow = 0;
		else if (cow == "Elsie")
			a[i].cow = 1;
		else
			a[i].cow = 2;
	}
	sort(a + 1, a + n + 1);
	int cnt[3] = {7, 7, 7}, mask = 7, ans = 0;
	for (int i = 1; i <= n; i++)
	{
		cnt[a[i].cow] += a[i].delta;
		int maxval = *max_element(cnt, cnt + 3), now = 0;
		for (int j = 0; j < 3; j++)
			if (cnt[j] == maxval)
				now += 1 << j;
		ans += now != mask;
		mask = now;
	}
	fout << ans << endl;
	return 0;
}
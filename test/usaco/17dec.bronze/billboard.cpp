#include <fstream>
using namespace std;
ifstream fin("billboard.in");
ofstream fout("billboard.out");
const int N = 1000;
bool mat[N * 2 + 5][N * 2 + 5];
void paint(bool flag)
{
	int x1, y1, x2, y2;
	fin >> x1 >> y1 >> x2 >> y2;
	for (int i = x1; i < x2; i++)
		for (int j = y1; j < y2; j++)
			mat[i + N][j + N] = flag;
}
int main()
{
	paint(true);
	paint(true);
	paint(false);
	int ans = 0;
	for (int i = 0; i < N * 2; i++)
		for (int j = 0; j < N * 2; j++)
			ans += mat[i][j];
	fout << ans << endl;
	return 0;
}
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("shuffle.in");
ofstream fout("shuffle.out");
const int N = 105;
int a[N];
string s[N], t[N];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	for (int i = 1; i <= n; i++)
		fin >> s[i];
	for (int i = 1; i <= 3; i++)
	{
		for (int j = 1; j <= n; j++)
			t[j] = s[a[j]];
		copy(t + 1, t + n + 1, s + 1);
	}
	for (int i = 1; i <= n; i++)
		fout << s[i] << endl;
	return 0;
}
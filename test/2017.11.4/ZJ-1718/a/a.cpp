#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N=305;
int n,len[N],last[N],low[N],ans,l[N],r[N];
bool vis[N],f[N*N],g[N*N];
void dfs(int num,int now)
{
	if(num==0)
		ans=max(ans,now);
	for(int i=1;i<=n;i++)
		if(!vis[i]&&num+low[i]>=0)
		{
			vis[i]=true;
			dfs(num+last[i],now+len[i]);
			vis[i]=false;
		}
}
int main()
{
	fin>>n;
	int suml=0,sumr=0;
	for(int i=1;i<=n;i++)
	{
		string s;
		fin>>s;
		len[i]=s.length();
		int now=0;
		low[i]=0;
		for(int j=0;j<len[i];j++)
		{
			if(s[j]=='(')
			{
				now++;
				l[i]++;
			}
			else
			{
				now--;
				r[i]++;
			}
			low[i]=min(low[i],now);
		}
		last[i]=now;
		suml+=l[i];sumr+=r[i];
	}
	if(n<=10)
		dfs(0,0);
	else
	{
		f[0]=g[0]=true;
		for(int i=1;i<=n;i++)
		{
			for(int j=suml;j>=l[i];j--)
				f[j]|=f[j-l[i]];
			for(int j=sumr;j>=r[i];j--)
				g[j]|=g[j-r[i]];
		}
		for(int i=suml;i;i--)
			if(f[i]&&g[i])
			{
				ans=i+i;
				break;
			}
	}
	fout<<ans<<endl;
	return 0;
}

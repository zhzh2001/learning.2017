#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N=200005,N2=5005,INF=1e6;
struct point
{
	int x,y,z;
	bool operator<(const point &rhs)const
	{
		return x<rhs.x;
	}
}p[N];
int x[N],y[N],pre[N2][N2];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i].x>>p[i].y>>p[i].z;
		x[i]=p[i].x;
		y[i]=p[i].y;
	}
	sort(x+1,x+n+1);
	int xn=unique(x+1,x+n+1)-x-1;
	x[++xn]=INF;
	sort(y+1,y+n+1);
	int yn=unique(y+1,y+n+1)-y-1;
	y[++yn]=INF;
	if(xn<=5000&&yn<=5000)
	{
		for(int i=0;i<=xn;i++)
			for(int j=0;j<=yn;j++)
				pre[i][j]=INF;
		for(int i=1;i<=n;i++)
		{
			p[i].x=lower_bound(x+1,x+xn+1,p[i].x)-x;
			p[i].y=lower_bound(y+1,y+yn+1,p[i].y)-y;
			pre[p[i].x][p[i].y]=min(pre[p[i].x][p[i].y],p[i].z);
		}
		long long ans=0;
		for(int i=1;i<=xn;i++)
			for(int j=1;j<=yn;j++)
			{
				pre[i][j]=min(pre[i][j],min(pre[i-1][j],pre[i][j-1]));
				ans=max(ans,1ll*x[i]*y[j]*pre[i-1][j-1]);
			}
		fout<<ans<<endl;
	}
	else
	{
		int y=INF;
		long long ans=0;
		sort(p+1,p+n+1);
		p[++n].x=INF;
		p[n].y=INF;
		for(int i=1,j;i<=n;i=j)
		{
			ans=max(ans,1ll*p[i].x*y*INF);
			for(j=i;j<=n&&p[j].x==p[i].x;j++)
				y=min(y,p[j].y);
		}
		fout<<ans<<endl;
	}
	return 0;
}

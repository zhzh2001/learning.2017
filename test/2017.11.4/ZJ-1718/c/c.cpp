#pragma GCC optimize 2
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N=200005,MOD=1e9+7;
int a[N],tmp[N],f[N],g[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		a[i]^=a[i-1];
	}
	int sum=0;
	for(int j=1;j<n;j++)
	{
		for(int i=0;i<j;i++)
			tmp[i]=a[j]^a[i];
		sort(tmp,tmp+j);
		for(int k=j+1;k<=n;k++)
		{
			int cnt=lower_bound(tmp,tmp+j,a[k]^a[j])-tmp;
			f[j]+=cnt;g[k]+=cnt;(sum+=cnt)%=MOD;
		}
	}
	fout<<sum<<endl;
	while(m--)
	{
		int opt,x;
		fin>>opt>>x;
		if(opt==2)
			fout<<f[x]<<endl;
		else
			fout<<g[x]<<endl;
	}
	return 0;
}

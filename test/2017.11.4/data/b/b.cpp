#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define ls (i<<1)
#define rs (i<<1|1)
#define md (l+r>>1)
using namespace std;
const int N=1000100,m=1000000;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
struct cqz{int x,y,z;}p[N];
inline bool operator<(cqz i,cqz j){return i.x<j.x;}
int n,mx[N*4],cv[N*4];
ll ans,res,mxs[N*4];
void build(int i,int l,int r){
	mx[i]=m;mxs[i]=1ll*r*m;
	if(l==r)return;
	build(ls,l,md);build(rs,md+1,r);
}
void cov(int i,int l,int r,int x,int y,int k){
	if(l>=x&&r<=y){
		cv[i]=k;mx[i]=k;
		mxs[i]=1ll*r*k;
		return;
	}
	if(cv[i]){
		cov(ls,l,md,1,m,cv[i]);
		cov(rs,md+1,r,1,m,cv[i]);cv[i]=0;
	}
	if(x<=md)cov(ls,l,md,x,y,k);
	if(y>md)cov(rs,md+1,r,x,y,k);
	mx[i]=max(mx[ls],mx[rs]);
	mxs[i]=max(mxs[ls],mxs[rs]);
}
int get(int i,int l,int r,int k){
	if(l==r)return l;
	if(cv[i]){
		cov(ls,l,md,1,m,cv[i]);
		cov(rs,md+1,r,1,m,cv[i]);cv[i]=0;
	}
	if(mx[rs]>k)return get(rs,md+1,r,k);
	else return get(ls,l,md,k);
}
int main(){
	freopen("b20.in","r",stdin);
	freopen("b20.out","w",stdout);
	build(1,1,m);
	int i;n=read();
	for(i=1;i<=n;i++){
		p[i].x=read();
		p[i].y=read();
		p[i].z=read();
	}sort(p+1,p+n+1);
	p[++n]=(cqz){m,1,1};
	for(i=1;i<=n;i++){
		res=mxs[1];
		if(res*p[i].x>ans)
		ans=max(res*p[i].x,ans);
		int r=get(1,1,m,p[i].z);
		if(p[i].y+1<=r)
		cov(1,1,m,p[i].y+1,r,p[i].z);
	}write(ans);putchar('\n');
	return 0;
}

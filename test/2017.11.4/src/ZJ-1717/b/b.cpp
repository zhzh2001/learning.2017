#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define int long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x) {
	write(x);
	putchar('\n');
}

const int N = 200011,Mx = 1000000;
struct node{
	int x,y,z;
}a[N];
int n,ans;
bool flag;

inline bool cmp_1(node a,node b) {
	if(a.x!=b.x) return a.x < b.x;
	return a.y < b.y;
} 

signed main() {
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(); 
	For(i, 1, n) {
		a[i].x=read(), a[i].y=read(), a[i].z = read();
		if(a[i].z!=1) flag = 1;
	}
	if(!flag) {
		sort(a+1,a+n+1,cmp_1);
		int y = Mx;
		For(i, 1, n) {
			if(a[i].x==a[i-1].x) continue;
			ans = max(ans, y*a[i].x);
			y = min(y, a[i].y);
		}
		ans = max(ans, y*Mx);
		printf("%lld\n",ans);
		exit(0);
	}
	if(n<=400) {
		a[++n].x = Mx;a[n].y=Mx;a[n].z=Mx;
		For(i, 1, n) 
		  For(j, 1, n) {
		  	int H = Mx;
		  	For(k, 1, n) 
		  	  if(a[k].x<=a[i].x&&a[k].y<=a[j].y&&a[k].z<H) H = a[k].z;
		  	if(ans<a[i].x*a[j].y*H) {
		  		ans = a[i].x*a[j].y*H;
			  }
		  }
		printf("%lld\n",ans);
		exit(0);
	} 
	printf("%lld\n",1e18);
	return 0;
}





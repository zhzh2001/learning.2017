#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <iomanip>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;

const int NN = 311,inf = 1e9;
char s[NN][NN];
char c;
int n,N,ans;
int len[NN],sum[NN],minn[NN],vis[NN],b[NN];
bool f;

inline void check() {
	int val = 0; 
	For(i, 1, N) {
		if(val+minn[b[i]] < 0) return;
		val+=sum[b[i]];
	}
	if(val!=0) return;
	val = 0;
	For(i, 1, N) 
		val+=len[b[i]];
	if( val>ans ) ans = val;
}

void dfs(int x) {
	if(x==N+1) {
		check(); 
		return;
	}
	For(i, 1, 10) 
		if(!vis[i]) {
			vis[i] = 1;
			b[x] = i; 
			dfs(x+1);
			vis[i]=0;
		}
}

int main() {
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d",&n);
	For(i, 1, n) {
		scanf("%s",s[i]+1);
		len[i] = strlen(s[i]+1);
		minn[i] = inf;
		For(j, 1, len[i]) {
			if(s[i][j]=='(') ++sum[i];
			else --sum[i];
			if(sum[i] < minn[i]) minn[i] = sum[i];
		}
	}
	
	c = s[1][1];
	For(i, 1, n) 
	  For(j, 1, len[i]) 
	  	if(s[i][j] != c) f = 1;
	if(!f) {
		printf("0\n");
		return 0;
	}
	
	
	if(n < 10) {
	For(i, 1, n) {
		N = i;
		dfs(1);
	}
	printf("%d\n",ans);
	
	}
	return 0;
}








#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <iomanip>
#define int long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(int x) {
	write(x);
	putchar('\n');
}

const int N = 200011;
int n,Q,res;
int a[N],sum[N],ans[4][N];  

signed main() {
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read(); Q=read();
	For(i, 1, n) a[i]=read(),sum[i]=sum[i-1]^a[i];
	For(i, 1, n-1) 
	  For(j, i, n-1) 
	    For(k, j+1, n) 
	    	if( (sum[j]^sum[i-1]) < (sum[k]^sum[j]) ) {
	    		res++;
	    		ans[2][j]++;
	    		ans[3][k]++;
			}
	writeln(res);
	while(Q--) {
		int opt=read(),pos=read();
		writeln(ans[opt][pos]);
	}
	
	return 0;
} 











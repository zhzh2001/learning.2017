#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100*32,P=1e9+7;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int sm,to[N][2],sum[N][2][2],num[N][2];
int ans,m,n,a[N],SJ[N],SK[N],Sm[32][2],cnt[32][2];
inline void A(int&x,int y){x+=y;if(x>=P)x-=P;}
void clear(){
	sm=ans=0;
	memset(to,-1,n*32*sizeof(to[0]));
	memset(sum,0,n*32*sizeof(sum[0]));
	memset(num,0,n*32*sizeof(num[0]));
	memset(cnt,0,sizeof(cnt));
	memset(Sm,0,sizeof(Sm));
}
int insert(int x){
	int res=0,nw=0;
	for(register int t,i=31;i>=0;i--){
		t=(x>>i)&1;
		if(to[nw][t]==-1)to[nw][t]=++sm;
		int ss,ii=to[nw][t^1];
		if(to[nw][t^1]==-1)goto out;
		ss=1ll*sum[nw][1][t^1]*Sm[i][t^1]%P;
		ss=(ss-num[nw][t^1]+P)%P;
		A(res,ss);
		out:
		sum[nw][1][t]++;
		Sm[i][t]++;
		A(num[nw][t],Sm[i][t]);
		nw=to[nw][t];
	}return res;
}
int query(int x){
	int nw=0,res=0;
	for(register int t,i=31;i>=0;i--){
		t=(x>>i)&1;
		A(res,cnt[i][t]);
		cnt[i][t]+=sum[nw][1][t^1];
		cnt[i][t^1]-=sum[nw][0][t^1];
		--sum[nw][1][t];
		++sum[nw][0][t];
		nw=to[nw][t];
	}return res;
}
int S(int x,int y){
	int re=0,i;
	for(i=x;i<=y;i++)
	re^=a[i];return re;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	int i,j,k,o,x;
	n=read();m=read();
	for(i=1;i<=n;i++)a[i]=read();
	for(i=1;i<=n;i++)
	for(j=i;j<=n;j++)
	for(k=j+1;k<=n;k++)
	if(S(i,j)<S(j+1,k)){
		ans++;
		SJ[j]++;
		SK[k]++;
	}
	printf("%d\n",ans);
	while(m--){
		o=read();x=read();
		if(o==2){
			printf("%d\n",SJ[x]);
		}else printf("%d\n",SK[x]);
	}
	return 0;
}

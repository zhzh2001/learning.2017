#include <bits/stdc++.h>
#define N 320
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int l[N], r[N], ln[N], n;
char str[N];
bool vis[N];
void tense(int &x, int y) {
	if (x < y) x = y;
}
int rt[N][2], lt[N][2], lnt, rnt, totlen;
int lp[N*N], rp[N*N], ans, res;
int main(int argc, char const *argv[]) {
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	n = read();
	for (int i = 1; i <= n; i++) {
		scanf("%s", str + 1);
		int m = strlen(str + 1);
		int mn = 0, x = 0;
		for (int j = 1; j <= m; j++)
			if (str[j] == '(') x ++;
			else mn = min(mn, -- x);
		l[i] = - mn;
		r[i] = x - mn;
		totlen += ln[i] = m;
		// printf("%d %d %d\n", l[i], r[i], ln[i]);
	}
	for (int i = 1; i <= n; i++) {
		if (l[i] == r[i]) ans += ln[i];
		else if (l[i] < r[i]) lt[++lnt][0] = r[i] - l[i], lt[lnt][1] = ln[i];
		else rt[++rnt][0] = l[i] - r[i], rt[rnt][1] = ln[i];
	}
	memset(lp, -1, sizeof lp);
	memset(rp, -1, sizeof rp);
	lp[0] = rp[0] = 0;
	for (int i = 1; i <= lnt; i++) {
		int x = lt[i][0];
		for (int j = totlen; j >= x; j--)
			if (lp[j - x] > -1)
				tense(lp[j], lp[j - x] + lt[i][1]);
	}
	for (int i = 1; i <= rnt; i++) {
		int x = rt[i][0];
		for (int j = totlen; j >= x; j--)
			if (rp[j - x] > -1)
				tense(rp[j], rp[j - x] + rt[i][1]);
	}
	// printf("%d %d\n", lnt, rnt);
	// for (int i = 0; i < 20; i++)
	// 	printf("%d => %d\n", i, rp[i]);
		// printf("%d\n", ln[i]);
	for (int i = 1; i <= totlen; i++)
		if (lp[i] > -1 && rp[i] > -1)
			tense(res, lp[i] + rp[i]);
	printf("%d\n", ans + res);
	return 0;
}

#include <bits/stdc++.h>
#define N 200020
#define zyy 1000000007
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N], st[N], cnt;
long long ans, ans2[N], ans3[N];
// O(n^2log)
int main(int argc, char const *argv[]) {
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	int n = read(), m = read();
	for (int i = 1; i <= n; i++)
		a[i] = read();
	for (int j = 1; j <= n; j++) {
		st[j + 1] = 0;
		for (int i = j; i; i--)
			st[i] = a[i] ^ st[i + 1];
		sort(st + 1, st + j + 1);
		int sum = 0;
		for (int k = j + 1, x = 0; k <= n; k++) {
			x ^= a[k];
			int y = lower_bound(st + 1, st + j + 1, x) - st - 1;
			ans3[k] += y;
			sum += y;
		}
		ans2[j] = sum;
		ans = (ans + sum) % zyy;
	}
	printf("%lld\n", ans);
	while (m --)
		if (read() == 2)
			printf("%lld\n", ans2[read()]);
		else
			printf("%lld\n", ans3[read()]);
	return 0;
}
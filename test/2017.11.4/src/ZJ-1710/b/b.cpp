#include <bits/stdc++.h>
#define N 200020
#define ll long long
#define INF 1000000ll
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll x[N], y[N], z[N];
int xys[N], zys[N], xzs[N];
// x 为第一关键字，y 为第二关键字
bool cmpxyz(const int &a, const int &b) {
	return x[a] == x[b] ? y[a] < y[b] : x[a] < x[b];
	// return x[a] == x[b] ? y[a] == y[b] ? z[a] < z[b] : y[a] < y[b] : x[a] < x[b];
}
bool cmpzyx(const int &a, const int &b) {
	return z[a] == z[b] ? y[a] < y[b] : z[a] < z[b];
	// return z[a] == z[b] ? y[a] == y[b] ? x[a] < x[b] : y[a] < y[b] : z[a] < z[b];
}
bool cmpxzy(const int &a, const int &b) {
	return x[a] == x[b] ? z[a] < z[b] : x[a] < x[b];
	// return x[a] == x[b] ? z[a] == z[b] ? y[a] < y[b] : z[a] < z[b] : x[a] < x[b];
}
inline void mins(int &x, int y) {
	if (y < x) x = y;
}
int main(int argc, char const *argv[]) {
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	int n = read();
	int mnx = INF, mny = INF, mnz = INF;
	for (int i = 1; i <= n; i++) {
		mins(mnx, x[i] = read());
		mins(mny, y[i] = read());
		mins(mnz, z[i] = read());
		xys[i] = xzs[i] = zys[i] = i;
	}
	sort(xys + 1, xys + n + 1, cmpxyz);
	sort(xzs + 1, xzs + n + 1, cmpxzy);
	sort(zys + 1, zys + n + 1, cmpzyx);
	long long ans = max(max(INF * INF * mnx, INF * INF * mny), INF * INF * mnz), m;
	// printf("%lld\n", ans);
	// O(n^2)
	for (int i = 1, k; i <= n; i++) {
		m = INF;
		for (int j = 1; j; j++) {
			if (x[xys[j]] == x[i]) break;
			if (y[xys[j]] < y[i])
				m = min(m, z[xys[j]]);
		}
		if (m < z[i]) continue; // 有一个三维都比 i 小的点
		ans = max(ans, x[i] * y[i] * m);

		m = INF;
		for (int j = 1; j; j++) {
			if (x[xzs[j]] == x[i]) break;
			if (z[xzs[j]] < z[i])
				m = min(m, y[xzs[j]]);
		}
		if (m < y[i]) continue; // 有一个三维都比 i 小的点
		ans = max(ans, x[i] * z[i] * m);

		m = INF;
		for (int j = 1; j; j++) {
			if (z[zys[j]] == z[i]) break;
			if (y[zys[j]] < y[i])
				m = min(m, x[zys[j]]);
		}
		if (m < x[i]) continue; // 有一个三维都比 i 小的点
		ans = max(ans, y[i] * z[i] * m);
		ans = max(ans, y[i] * x[i] * z[i]);
	}
	// WA 掉了啊啊啊啊啊啊啊啊啊啊啊♂
	printf("%lld\n", ans);
	return 0;
}
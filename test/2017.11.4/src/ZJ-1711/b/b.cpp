#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 200005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,ans=1,X,Y,Z;
struct data{ ll x,y,z; }a[N];
bool cmp(data x,data y) { return x.x>y.x; }
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	rep(i,1,n){
		ll x=read(),y=read(),z=read();
		a[i]=(data){x,y,z};
	}
	if (n<=100){
		a[++n]=(data){1000000,1000000,1000000};
		rep(i,1,n) rep(j,1,n){
			X=a[i].x; Y=a[j].y;
			Z=1000000;
			rep(k,1,n){
				if (a[k].x<X&&a[k].y<Y) Z=min(Z,a[k].z);
			}
			ans=max(ans,X*Y*Z);
		}
		printf("%lld",ans);
	}else{
		Y=1000000; Z=a[1].z;
		sort(a+1,a+1+n,cmp);
		rep(i,1,n){
			X=a[i].x;
			ans=max(ans,X*Y*Z);
			Y=min(Y,a[i].y);
		}
		printf("%lld",ans);
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 305
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,f[N][N*N*2]; char s[N];
struct data{ ll x,y,z; }a[N];
bool cmp(data x,data y) { return x.z>y.z; }
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	rep(i,1,n){
		scanf("%s",s+1);
		a[i].x=strlen(s+1);
		rep(j,1,a[i].x){
			if (s[j]=='(') ++a[i].y;
			else --a[i].y;
			a[i].z=min(a[i].z,a[i].y);
		}
		if (a[i].z>=0) a[i].z=a[i].y;
	}
	sort(a+1,a+1+n,cmp);
	memset(f,-7,sizeof f); f[0][0]=0;
	rep(i,0,n-1)
	rep(j,0,300*n){
		f[i+1][j]=max(f[i+1][j],f[i][j]);
		if (a[i+1].z+j>=0){
			f[i+1][j+a[i+1].y]=max(f[i+1][j+a[i+1].y],f[i][j]+a[i+1].x);
		}
	}
	printf("%d",max(f[n][0],0));
}

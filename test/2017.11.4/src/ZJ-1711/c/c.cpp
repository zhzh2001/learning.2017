#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 305
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,opt,x,sum[N],ans1[N],ans2[N]; long long tmp;
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) sum[i]=sum[i-1]^read();
	rep(i,1,n) rep(j,i,n) rep(k,j+1,n)
		if (sum[j]^sum[i-1]<sum[j]^sum[k]) ++ans1[j],++ans2[k];
	rep(i,1,n) (tmp+=ans1[i])%=mod;
	printf("%lld\n",tmp);
	while (m--){
		opt=read(); x=read();
		if (opt==2) printf("%lld\n",ans1[x]);
		else printf("%lld\n",ans2[x]);
	}
}

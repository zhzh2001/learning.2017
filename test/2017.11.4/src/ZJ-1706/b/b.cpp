#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int x,y,z;}a[200010];
inline bool cmp(ppap a,ppap b){return a.z<b.z;}
int n,x,y;
signed main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read(),a[i].z=read();
	sort(a+1,a+n+1,cmp);
	int ans=0;x=1e6;y=1e6;
	for(int i=1;i<=n;){
		int j=i;
		ans=max(ans,a[i].z*x*y);
		ans=max(ans,a[i].z*1000000ll*max(x,y));
		while(a[j].z==a[i].z){
			x=min(x,a[j].x);y=min(y,a[j].y);
			j++;
		}
		i=j;
	}
	ans=max(ans,1000000ll*1000000ll*max(x,y));
	printf("%lld",ans);
	return 0;
}

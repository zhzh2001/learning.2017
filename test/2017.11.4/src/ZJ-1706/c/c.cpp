#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int s1[200010],s2[200010];
int n,m,a[200010];
signed main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=a[i-1]^read();
	int sum=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=i;k<j;k++)if((a[k]^a[i-1])<(a[j]^a[k]))sum++,s1[k]++,s2[j]++;
	sum%=MOD;printf("%lld\n",sum);
	for(int i=1;i<=m;i++){
		int op=read(),x=read(),sum=0;
		if(op==2)printf("%lld\n",s1[x]);
		else printf("%lld\n",s2[x]);
	}
	return 0;
}

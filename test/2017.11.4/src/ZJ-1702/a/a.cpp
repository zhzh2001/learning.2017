#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define Abs(x) ((x) > 0 ? (x) : -(x))
#define Max(x,y) ((x) > (y) ? (x) : (y))
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 305;
int n,sum,len;
char s[N];
int a[N],b[N];
struct node{
	int l,r,len;
	friend bool operator <(node a,node b){
		return a.l < b.l || (a.l == b.l && a.r < b.r);
	}
}p[N];
int f[N][90005];

int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout); 
	scanf("%d",&n);
	for(int i = 1;i <= n;i++){
		scanf("%s",s+1);
		b[i] = strlen(s+1);
		for(int j = 1;j <= b[i];j++){
			a[i] += s[j] == '(' ? 1 : -1;
			if(a[i] < 0){
				p[i].l += -a[i];
				a[i] = 0;
			}
		}
		p[i].r = a[i];
		p[i].len = b[i];
	}
	sort(p+1,p+n+1);
	memset(f,-1,sizeof f);
	f[0][0] = 0;
	for(int i = 1;i <= n;i++){
		for(int j = 0;j <= 90000;j++)
			f[i][j] = f[i-1][j];
		for(int j = p[i].l;j <= 90000;j++)
			if(f[i-1][j] != -1)
				f[i][j-p[i].l+p[i].r] = Max(f[i][j-p[i].l+p[i].r],f[i-1][j]+p[i].len);
	}
	printf("%d\n",f[n][0]);
	return 0;
}

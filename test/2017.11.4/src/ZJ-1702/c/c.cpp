#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define lowbit(x) ((x)&(-(x)))
#define Abs(x) ((x) > 0 ? (x) : -(x))
#define Max(x,y) ((x) > (y) ? (x) : (y))
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 305;
const int mod = 1e9+7;
int a[N];
int b[N];
int n,sum,m;

int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= n;i++){
		a[i] = read();
		b[i] = b[i-1]^a[i];
	}
	for(int l = 1;l <= n;l++)
		for(int k = l;k <= n;k++)
			for(int r = k+1;r <= n;r++)
				if((b[k]^b[l-1]) < (b[r]^b[k])){
					sum++;
//					printf("%d %d %d %d %d\n",l,k,r,b[k]^b[l-1],b[r]^b[k]);
					if(sum == mod) sum = 0;
				}
	printf("%d\n",sum);
	for(int i = 1;i <= m;i++){
		int opt = read();
		if(opt == 2){
			int k = read();
			ll sum = 0;
			for(int l = 1;l <= k;l++)
				for(int r = k+1;r <= n;r++)
					if((b[k]^b[l-1]) < (b[r]^b[k]))
						sum++;
			printf("%lld\n",sum);
		}
		else{
			int r = read();
			ll sum = 0;
			for(int l = 1;l < r;l++)
				for(int k = l;k < r;k++)
					if((b[k]^b[l-1]) < (b[r]^b[k]))
						sum++;
			printf("%lld\n",sum);
		}
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
#define lowbit(x) ((x)&(-(x)))
#define Abs(x) ((x) > 0 ? (x) : -(x))
#define Max(x,y) ((x) > (y) ? (x) : (y))
const int L=200005;
char LZH[L],*S=LZH,*T=LZH;
inline char gc(){
	if (S==T){
		T=(S=LZH)+fread(LZH,1,L,stdin);
		if (S==T) return EOF;
	}
	return *S++;
}
inline int read(){
	int x=0,f=1;
	char c=gc();
	for (;c<'0'||c>'9';c=gc())
		if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x*f;
}

const int N = 200005;
int n;
struct node{
	int x,y,z,num;
	friend bool operator <(node a,node b){
		if(a.z != b.z) return a.z < b.z;
		if(a.x != b.x) return a.x < b.x;
		return a.y < b.y;
	}
}p[N];
bool vis[N];
ll ans;

int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;i++){
		p[i].x = read(); p[i].y = read(); p[i].z = read();
		p[i].num = i;
	}
	sort(p+1,p+n+1);
	int t = 1;
	ans = p[1].z*1000000*1000000;
	while(t <= n){
		int len = p[t].z;
		while(t <= n && p[t].z == len){
			vis[t] = true;
			for(int i = 1;i < t;i++)
				if(vis[i] && p[i].x >= p[t].x && p[i].y >= p[t].y)
					vis[i] = false;
			t++;
		}
		if(t == n+1) len = 1000000; 
		else len = p[t].z;
		int last = 0;
		for(int i = 1;i < t;i++)
			if(vis[i]){
				if(last == 0){
					ans = Max(ans,1ll*len*(1000000*p[i].x));
					last = i;
				}
				else{
					ans = Max(ans,1ll*len*(p[last].y*p[i].x));
					last = i;
				}
			}
	}
	printf("%lld\n",ans);
	return 0;
}

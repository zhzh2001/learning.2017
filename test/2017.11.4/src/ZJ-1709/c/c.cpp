#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int mod=1000000007;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=200002;
const int maxt=7000006;
struct trie{
	int ch[maxt][2],sum[maxt];
	int sz;
	trie(){
		sz=1;
	}
	void insert(int x){
		int now=1;
		sum[1]++;
		for(int i=29;i>=0;i--){
			int &u=ch[now][x>>i&1];
			if (!u)
				u=++sz;
			now=u;
			sum[now]++;
		}
	}
	int query(int x,int y){ //^x<y
		int now=1,ans=0;
		for(int i=29;i>=0;i--){
			int a=x>>i&1,b=y>>i&1;
			if (b)
				ans+=sum[ch[now][a]],now=ch[now][a^1];
			else now=ch[now][a];
			if (!now)
				break;
		}
		return ans;
	}
}t;
struct trie2{
	int ch[maxt][2],sum[maxt][2];
	int sz;
	trie2(){
		sz=1;
	}
	ll cnt[33][2];
	void ins(int x,int id){
		int now=1;
		sum[1][id]++;
		for(int i=29;i>=0;i--){
			int qwq=x>>i&1;
			int &u=ch[now][qwq];
			if (!u)
				u=++sz;
			if (id^qwq)
				cnt[i][1]+=sum[ch[now][qwq^1]][qwq];
			else cnt[i][0]+=sum[ch[now][qwq^1]][qwq^1];
			now=u;
			sum[now][id]++;
		}
	}
	void del(int x,int id){
		int now=1;
		sum[1][id]--;
		for(int i=29;i>=0;i--){
			int qwq=x>>i&1;
			int &u=ch[now][qwq];
			if (id^qwq)
				cnt[i][1]-=sum[ch[now][qwq^1]][qwq];
			else cnt[i][0]-=sum[ch[now][qwq^1]][qwq^1];
			now=u;
			sum[now][id]--;
		}
	}
	ll query(int x){ 
		ll ans=0;
		for(int i=29;i>=0;i--)
			ans+=cnt[i][x>>i&1];
		return ans;
	}
}t2;
int a[maxn];
ll x[maxn],y[maxn];
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		a[i]=a[i-1]^readint();
	if (n<=3000){
		t.insert(0);
		ll ans=0;
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				int qwq=t.query(a[i],a[i]^a[j]);
				//if (i==1)
					//printf("%d %d\n",j,qwq);
				y[j]+=qwq;
				x[i]+=qwq;
			}
			t.insert(a[i]);
		}
		for(int i=1;i<=n;i++)
			ans=(ans+x[i])%mod;
		printf("%lld\n",ans);
		while(m--){
			int op=readint(),id=readint();
			printf("%lld\n",op==2?x[id]:y[id]);
		}
		return 0;
	}
	t2.ins(0,0);
	for(int i=1;i<=n;i++)
		t2.ins(a[i],1);
	ll ans=0;
	for(int i=1;i<=n;i++){
		t2.del(a[i],1);
		x[i]=t2.query(a[i]);
		ans=(ans+x[i])%mod;
		t2.ins(a[i],0);
	}
	printf("%lld\n",ans);
	while(m--){
		int op=readint(),id=readint();
		if (op==3)
			puts("WTF");
		else printf("%lld\n",x[id]);
	}
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct seq{
	int x,y,l;
	bool operator <(const seq& rhs)const{ //sum>=y1 sum+x1>=y2 vs sum>=y2 sum+x2>=y1 -> max(y1,y2-x1)<max(y2,y1-x2)
		if (x>=0 && rhs.x<0)
			return true;
		if (x<=0 && rhs.x>0)
			return false;
		if (!x)
			return false;
		if (x>0)
			return y<rhs.y;
		else return y+x>rhs.y+rhs.x;
	}
}a[403];
char s[403];
int f[91003],g[91003];
inline void tense(int &x,int y){
	if (x<y)
		x=y;
}
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++){
		readstr(s+1);
		int l=strlen(s+1);
		int sum=0,mmax=0;
		for(int j=1;j<=l;j++){
			sum+=s[j]=='('?1:-1;
			mmax=max(mmax,-sum);
		}
		//assert(l<=300);
		a[i]=(seq){sum,mmax,l};
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
			if (a[i]<a[j])
				swap(a[i],a[j]);
	f[0]=0;
	for(int i=1;i<=90000;i++)
		f[i]=-INF;
	for(int i=1;i<=n;i++){
		for(int j=0;j<=90000;j++)
			g[j]=f[j];
		for(int j=a[i].y;j<=90000;j++)
			tense(g[j+a[i].x],f[j]+a[i].l);
		swap(f,g);
	}
	printf("%d",f[0]);
}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
struct point{
	int x,y,z;
	bool operator <(const point& rhs)const{
		return x<rhs.x;
	}
}a[200003];
inline void tense(ll &x,ll y){
	if (x<y)
		x=y;
}
inline void relax(int &x,int y){
	if (x>y)
		x=y;
}
int b[200003];
int main(){
	init();
	int n=readint(),flag=1,cur=0;
	for(int i=1;i<=n;i++){
		a[i].x=readint(),a[i].y=readint(),a[i].z=readint(),flag&=a[i].z==1;
		b[++cur]=a[i].y;
	}
	sort(a+1,a+n+1);
	if (flag){
		ll ans=1000000LL*1000000LL;
		int y=1000000;
		for(int i=1;i<=n;i++){
			tense(ans,1000000LL*a[i].x*y);
			relax(y,a[i].y);
		}
		tense(ans,1000000LL*1000000LL*y);
		printf("%lld",ans);
		return 0;
	}
	b[++cur]=1000000;
	sort(b+1,b+cur+1);
	ll ans=0;
	for(int i=1;i<=cur;i++){
		int z=1000000;
		for(int j=1;j<=n;j++){
			if (a[j].y<b[i]){
				tense(ans,1LL*b[i]*z*a[j].x);
				//printf("%d %d %d\n",a[j].x,b[i],z);
				relax(z,a[j].z);
			}
		}
		tense(ans,1000000LL*b[i]*z);
	}
	printf("%lld",ans);
}
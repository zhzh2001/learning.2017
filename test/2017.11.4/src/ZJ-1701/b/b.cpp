#include<bits/stdc++.h>
#define Ll long long
#define max(x,y) ((x<y)?(y):(x))
using namespace std;
const int N=2e5+5;
struct cs{int x,y,z;}a[N],b[N];
int n,m,x,y,z;
Ll ans,vv;
bool cmp1(cs a,cs b){return a.z<b.z;}
bool cmp2(cs a,cs b){return a.x<b.x;}
void dfs(Ll x){
	Ll y=1e6;vv=0;
	for(int i=1;i<=n+1;i++){
		if(b[i].z>=x||b[i].y>=y)continue;
		vv=max(vv,x*y*b[i].x);
		y=b[i].y;
	}
//	cout<<vv<<endl;
	ans=max(ans,vv);
}
void csgo(){
	int sum=0;
	for(int i=1;i<=n+1;i++)
		if(a[i].z!=a[i-1].z){
			dfs(a[i].z);
			if(++sum==600)break;
		}
	sum=0;
	for(int i=n+1;i;i--)
		if(a[i].z!=a[i-1].z){
			dfs(a[i].z);
			if(++sum==600)break;
		}
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	ios::sync_with_stdio(0);
	cin>>n;
	for(int i=1;i<=n;i++)cin>>a[i].x>>a[i].y>>a[i].z,b[i]=a[i];
	sort(a+1,a+n+1,cmp1);
	sort(b+1,b+n+1,cmp2);
	a[n+1].z=1e6;b[n+1].x=1e6; 
	if(n>1e4)csgo();else
	for(int i=1;i<=n+1;i++)
		if(a[i].z!=a[i-1].z)dfs(a[i].z);
	cout<<ans;
}


#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=3e3+5;
int a[N],A[N],B[N],b[N];
int sum,ans,n,m,x,y,mo=1e9+7;
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	ios::sync_with_stdio(0);
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>b[i],a[i]=(b[i]^a[i-1]);
	for(int j=1;j<n;j++){
		for(int i=1;i<=j;i++)b[i]=(a[j]^a[i-1]);
		sort(b+1,b+j+1);
		for(int k=j+1;k<=n;k++){
			int v=lower_bound(b+1,b+1+j,(a[k]^a[j]))-b-1;
			A[j]+=v;
			B[k]+=v;
			ans=(ans+v)%mo;
		}
	}
	cout<<ans<<endl;
	while(m--){
		cin>>x>>y;
		if(x==2)cout<<A[y]<<"\n";
		else cout<<B[y]<<"\n";
	}
}


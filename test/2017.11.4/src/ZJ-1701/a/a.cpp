#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=4e2+5;
struct cs{int x,y,v;}a[N];
string s;
int f[N*N],F[N*N];
int n,m;
bool cmp(cs a,cs b){
	if(a.x&&b.x)return a.y<b.y;
	return a.x>b.x;
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	ios::sync_with_stdio(0);
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>s;
		a[i].v=s.length();
		for(int j=0;j<s.length();j++)
			if(s[j]=='(')a[i].x++;
			else if(a[i].x)a[i].x--;else a[i].y++;
//		cout<<a[i].x<<' '<<a[i].y<<endl;
	}
	memset(f,-63,sizeof f);
	memset(F,-63,sizeof F);
	F[0]=0;
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		for(int j=a[i].y;j<=n*300;j++)
			f[j-a[i].y+a[i].x]=max(f[j-a[i].y+a[i].x],F[j]+a[i].v);
		for(int j=0;j<=n*300;j++)F[j]=f[j];
//		for(int j=0;j<=10;j++)cout<<f[j]<<' ';cout<<endl;
	}
	cout<<f[0];
}

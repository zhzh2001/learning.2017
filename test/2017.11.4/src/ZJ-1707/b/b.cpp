#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll x,y,z,xm,ym,zm,ans,n;
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	xm=1e9;ym=1e9;zm=1e9;
	For(i,1,n)
	{
		x=read();y=read();z=read();
		xm=min(xm,x);ym=min(ym,y);zm=min(zm,z);
	}
	ans=xm*1000000000000;
	ans=max(ym*1000000000000,ans);
	ans=max(zm*1000000000000,ans);
	cout<<ans<<endl;
	return 0;
}


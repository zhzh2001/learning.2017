#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int n,m,mx,ans,l,lzq,jzq,x,z;
string s;
int a[310][310],b[310],c[310],f[90000],d[310];
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		cin>>s;l=s.length();
		x=0;
		For(j,0,l-1)
		{
			if(s[j]=='(')
			{
				x++;
				a[i][x]=1;
			}
			if(s[j]==')')
			{
				if(a[i][x]==1&&x>0)x--;
					else
					{
						x++;
						a[i][x]=2;
					} 
			}
			//cout<<endl<<x;
		}
		b[i]=x;c[i]=l;
		if(l>mx)mx=l;
	}
	mx*=n;
	memset(f,-10000,sizeof(f));f[0]=0;
	For(i,1,n)
	{
		if(a[i][1]==1)
		{
			Rep(j,b[i],mx)
				f[j]=max(f[j],f[j-b[i]]+c[i]);
		}
	}
	For(i,1,n)
	{
		if(a[i][1]==1)continue;
		d[i]=b[i];
		For(j,1,b[i])
		{
			if(a[i][j]!=2)
			{
				d[i]=j-1;
				break;
			}
		}
		z=b[i]-d[i]-d[i];
		if(z==0)
		{
			ans+=c[i];
			continue;
		}
		if(z>0)
			Rep(j,d[i],mx)
				f[j]=max(f[j],f[j-z]+c[i]);
		if(z<0)
			For(j,0,mx)
				f[j]=max(f[j],f[j-z]+c[i]);
	}
	ans+=f[0];
	cout<<ans<<endl;
	return 0;
}


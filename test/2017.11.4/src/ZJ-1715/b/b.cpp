#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct data{
	int x,y,z,mx;
}a[200010];
ll ans,t[800010];
int n;
 inline int read()
{
    int X=0,w=1; char ch=0;
    while(ch<'0' || ch>'9') {if(ch=='-') w=-1;ch=getchar();}
    while(ch>='0' && ch<='9') X=(X<<3)+(X<<1)+ch-'0',ch=getchar();
    return X*w;
}
 bool cmp1(const data &a,const data &b)
{
	return a.y<b.y;
}
 bool cmp2(const data &a,const data &b)
{
	return a.x<b.x;
}
 void build(int k,int l,int r)
{
	if (l==r)
	{
		t[l]=a[l].y*a[l].mx;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
	t[k]=max(t[k<<1],t[k<<1|1]);
}
 ll ask(int k,int l,int r,int x,int y)
{
	if (l==x&&r==y)
	{
		return t[k];
	}
	int mid=(l+r)>>1;
	if (y<=mid) return ask(k<<1,l,mid,x,y);
	else if (x>mid) return ask(k<<1|1,mid+1,r,x,y);
	else return (ll)(ask(k<<1,l,mid,x,mid)+ask(k<<1|1,mid+1,r,mid+1,y));
} 
 int main()
{
	n=read();
	for (int i=1;i<=n;++i) 
	{
		a[i].x=read();a[i].y=read();a[i].z=read();
	}
	sort(a+1,a+n+1,cmp1);
	a[0].mx=20000000;
	for (int i=1;i<=n;++i)
		a[i].mx=min(a[i].mx,a[i].z);
	sort(a+1,a+n+1,cmp2);
	build(1,1,n);
	int now=1;
	ans=0;last=0;
	for (int i=1;i<=1000000;++i)
	{
		if (i==a[now].x)
		{
			ll sq=ask(1,1,n,1,now);
			ans=max(ans,sq*(i-last));
			++now;	
		}
	}
	printf("%lld",ans);
}

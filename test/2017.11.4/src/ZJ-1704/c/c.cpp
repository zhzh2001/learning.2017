#include<cstdio>
using namespace std;
const int maxn=3005,mod=1e9+7;
int n,m,sum[maxn][maxn],a[maxn];
int ans[2][maxn];
inline void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
	for (int i=1;i<=n;i++){
		for (int j=i;j<=n;j++){
			sum[i][j]=sum[i][j-1]^a[j];
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=i;j<=n;j++){
			for (int k=j+1;k<=n;k++){
				if (sum[i][j]<sum[j+1][k]){
					ans[0][j]++;
					ans[1][k]++;
				}
			}
		}
	}
	int s=0;
	for (int i=1;i<=n;i++){
		(s+=ans[0][i])%=mod;
	}
	printf("%d\n",s);
}
inline void solve(){
	for (int i=1;i<=m;i++){
		int opt,x;
		scanf("%d%d",&opt,&x);
		opt-=2;
		printf("%d\n",ans[opt][x]);
	}
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	init();
	solve();
	return 0;
}

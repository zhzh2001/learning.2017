#include<cstdio>
#include<algorithm> 
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=2e5+5,inf=1e6;
typedef long long ll;
struct node{
	int x,y,z;
}a[maxn];
int n;
inline bool cmp(node a,node b){
	return a.z<b.z;
}
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		a[i].x=read(); a[i].y=read(); a[i].z=read();
	}
}
inline bool cmp1(node a,node b){
	return a.x<b.x;
}
inline bool cmp2(node a,node b){
	return a.y<b.y;
}
inline void solve(){
	ll ans=0;
	a[++n]=(node){inf,inf,inf};
	sort(a+1,a+1+n,cmp);
	for (int i=1;i<=n;i++){
		if (a[i].z!=a[i+1].z){
			sort(a+1,a+1+i,cmp1);
			int z=a[i].z,y=inf;
			for (int j=1;j<=i;j++){
				ans=max(ans,1ll*a[j].x*y*z); 
				y=min(y,a[j].y);
			}
			sort(a+1,a+1+i,cmp2);
			int x=inf;
			for (int j=1;j<=i;j++){
				ans=max(ans,1ll*x*a[j].y*z); 
				x=min(x,a[j].x);
			}
		}
	}
	printf("%lld\n",ans);	
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	init();
	solve();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=805;
bool flag;
int n,cnt[2][maxn],l[maxn],stack[maxn],top,dp[2][maxn][maxn];
inline void work(char s[],int len,int id){
	top=0;
	for (int i=1;i<=len;i++){
		if (s[i]=='(') {
			if (top&&stack[top]==1){
				top--;
			}else{
				stack[++top]=-1;
			}
		}else{
			if (top&&stack[top]==-1){
				top--;
			}else{
				stack[++top]=1;
			}
		}
	}
	for (int i=1;i<=top;i++){
		if (stack[i]==-1) cnt[0][id]++;
			else cnt[1][id]++;
	}
	flag&=(cnt[0][id]==len);
}
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		char s[maxn];
		scanf("%s",s+1);
		l[i]=strlen(s+1);
		work(s,l[i],i);
	}
}
inline void solve(){
	int cur=0;
	memset(dp[cur],-1,sizeof(dp[cur]));
	dp[cur][0][0]=0;
	for (int i=1;i<=n;i++){
		memset(dp[cur^1],-1,sizeof(dp[cur^1]));
		int L=cnt[0][i],R=cnt[1][i];
		for (int j=0;j<=300;j++){
			for (int k=0;k<=300;k++){
				int jj=j,kk=k;
				if (dp[cur][j][k]!=-1){
					dp[cur^1][j][k]=max(dp[cur][j][k],dp[cur^1][j][k]);
					int temp=min(L,j);
					L-=temp; j-=temp;
					dp[cur^1][R+j][L+k]=max(dp[cur^1][R+j][L+k],dp[cur][jj][kk]+l[i]);
					L+=temp; j+=temp;					
					temp=min(k,R);
					k-=temp; R-=temp;
					dp[cur^1][j+R][L+k]=max(dp[cur^1][j+R][L+k],dp[cur][jj][kk]+l[i]);
					k+=temp; R+=temp;
				}
			}
		}
		cur^=1;
	}
	printf("%d\n",max(0,dp[cur][0][0]));
}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	init();
	if (flag){
		puts("0");
		exit(0);
	} 
	solve();
	return 0;
}

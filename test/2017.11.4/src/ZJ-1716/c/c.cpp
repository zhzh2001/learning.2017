#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <fstream>
using namespace std;

ifstream fin("c.in");
ofstream fout("c.out");

typedef long long ll;
typedef pair<int, int> pii;

const int N = 200005, Mod = 1e9 + 7;
const bool debug = false;

int n, m, a[N];
pii seq[N];

namespace solution1 {
	ll ans[3005], sum[3005][3005];
	void main() {
		for (int i = 1; i <= n; ++i) {
			sum[i][i] = a[i];
			for (int j = i + 1; j <= n; ++j) {
				sum[j][i] = sum[i][j] = sum[i][j - 1] ^ a[j];
			}
		}
		ll total = 0;
		for (int i = 1; i <= n; ++i) {
			int cnt = 0;
			for (int j = i; j >= 1; --j)
				seq[++cnt] = make_pair(sum[j][i], 1);
			for (int j = i + 1; j <= n; ++j)
				seq[++cnt] = make_pair(sum[i + 1][j], 0);
			sort(seq + 1, seq + cnt + 1);
//			for (int j = 1; j <= cnt; ++j)
//				cout << seq[j].first << ' ';
//			cout << endl;
			for (int j = 1, p = 0; j <= cnt; ++j) {
				if (seq[j].second == 1) {
					++p;
				} else {
					ans[i] += p;
				}
			}
			total += (ll)ans[i];
		}
		fout << total % Mod << endl;
		for (int i = 1; i <= m; ++i) {
			int opt, x;
			fin >> opt >> x;
			if (opt == 2)
				fout << ans[x] << "\n";
			else {
				ll ans = 0;
//				bool flag = false;
//				if (i == 275) {
//					flag = true;
//				}
				for (int i = 1; i < x; ++i) {
					sort(sum[i] + 1, sum[i] + i);
					int pos = upper_bound(sum[i] + 1, sum[i] + i, sum[i + 1][x] - 1) - sum[i];
					ans += pos - 1 + (sum[i][i] < sum[i + 1][x]);
//					if (flag) {
//					cerr << lower_bound(sum[i] + 1, sum[i] + i + 1, sum[i + 1][x]) - sum[i] - 1 << endl;
//					for (int j = 1; j <= i; ++j)
//						cerr << sum[i][j] << ' ';
//					cerr << endl;
//					cerr << sum[i + 1][x] << endl;
//					}
				}
				fout << ans << "\n";
			}
		}
		return;
	}
}

int main() {
	fin >> n >> m;
	for (int i = 1; i <= n; ++i)
		fin >> a[i];
	solution1::main();
	return 0;
}

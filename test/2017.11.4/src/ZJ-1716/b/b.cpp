#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

ifstream fin("b.in");
ofstream fout("b.out");

typedef long long ll;

const int N = 200005;

struct Node {
	ll x, y, z;
} a[N];

int n;

inline bool cmp1(const Node &a, const Node &b) {
	return a.x < b.x;
}

inline bool cmp2(const Node &a, const Node &b) {
	return a.y < b.y;
}

inline bool cmp3(const Node &a, const Node &b) {
	return a.z < b.z;
}

ll ans, x, y, z;

int main() {
	fin >> n;
	for (int i = 1; i <= n; ++i)
		fin >> a[i].x >> a[i].y >> a[i].z;
	sort(a + 1, a + n + 1, cmp1);
	a[n + 1].x = a[n + 1].y = a[n + 1].z = 1000000;
	ll np;
	for (int i = 1; i <= n + 1; ++i) {
		x = 1000000, y = 1000000, z = 1000000;
		x = min(x, a[i].x);
		for (int j = 1; j <= n; ++j) {
			if (a[j].x < a[i].x) {
				if (i == 1)
				x = min(x, a[j].x);
				if ((y - a[j].y) * x * z < (z - a[j].z) * y * z)
					y = min(y, a[j].y);
				else	
					z = min(z, a[j].z);
			}
		}
		ans = max(ans, x * y * z);
		x = 1000000, y = 1000000, z = 1000000;
		y = min(y, a[i].y);
		for (int j = 1; j <= n; ++j) {
			if (a[j].y < a[i].y) {
				y = min(y, a[j].y);
				if ((x - a[j].x) * y * z < (z - a[j].z) * x * y)
					x = min(x, a[j].x);
				else	
					z = min(z, a[j].z);				
			}
		}
		ans = max(ans, x * y * z);
		x = 1000000, y = 1000000, z = 1000000;
		z = min(z, a[i].z);
		for (int j = 1; j <= n; ++j) {
			if (a[j].z < a[i].z) {
				z = min(z, a[j].z);
				if ((x - a[j].x) * y * z < (y - a[j].y) * x * z)
					x = min(x, a[j].x);
				else	
					y = min(y, a[j].y);
			}
		}
		ans = max(ans, x * y * z);
	}
	fout << ans << endl;
	return 0;
}

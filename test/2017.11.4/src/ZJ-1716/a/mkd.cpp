#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

ofstream fout("a.in");

int main() {
	srand(GetTickCount());
	int n = 300, l = 300;
	fout << n << endl;
	for (int i = 1; i <= n; ++i) {
		char ch = rand() % 2 ? '(' : ')';
		for (int j = 1; j <= l; ++j)
			fout.put(ch);
		fout << endl;
	}
	return 0;
}

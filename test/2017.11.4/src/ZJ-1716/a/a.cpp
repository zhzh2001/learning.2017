#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

ifstream fin("a.in");
ofstream fout("a.out");

typedef pair<int, int> pii;

const int N = 305, L = 305;
const bool debug = true;

int n, cost[N], ans, f[N * L], g[N * L];
pii val[N];
string s;
char c[N][L];
bool vis[N];

void dfs(int dep, int lb, int now = 0) {
	if (lb == 0)
		ans = max(ans, now);
	if (dep == n)
		return;
	for (int i = 1; i <= n; ++i) {
		if (!vis[i] && lb - val[i].second >= 0) {
			vis[i] = true;
			dfs(dep + 1, lb - val[i].second + val[i].first, now + cost[i]);
			vis[i] = false;
		}
	}
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; ++i) {
		fin >> s;
		cost[i] = s.length();
		for (int j = 0, cnt = 0; j < cost[i]; ++j) {
			if (c[i][cnt] == '(' && s[j] == ')')
				c[i][cnt--] = 0;
			else c[i][++cnt] = s[j];
		}
		for (int j = 1; c[i][j]; ++j)
			if (c[i][j] == '(') ++val[i].first;
			else ++val[i].second;
//		cerr << val[i].first << ' ' << val[i].second << endl;
	}
	bool f1 = false, f2 = false;
	for (int i = 1; i <= n; ++i) {
		if (!val[i].first)
			f2 = true;
		if (!val[i].second)
			f1 = true;	
	}
	if (f1 && f2) {
		if (!debug && n <= 10) {
			dfs(0, 0);
			fout << ans << endl;
			return 0;
		}
		bool flag = true;
		for (int i = 1; i <= n; ++i)
			if (val[i].first && val[i].second) {
				flag = false;
				break;
			}
		if (flag) {
			f[0] = 0;
			for (int i = 1; i <= n; ++i)
				if (val[i].first) {
					for (int j = 90000; j >= val[i].first; --j)
						f[j] = max(f[j], f[j - val[i].first] + cost[i]);
				}
			g[0] = 0;
			for (int i = 1; i <= n; ++i)
				if (val[i].second) {
					for (int j = 90000; j >= val[i].second; --j)
						g[j] = max(g[j], g[j - val[i].second] + cost[i]);
				}
//			for (int i = 1; i <= 25; ++i)
//				cerr << f[i] << ' ' << g[i] << endl;
			for (int i = 90000; i >= 0; --i)
				if (f[i] == g[i]) {
					fout << f[i] + g[i] << endl;
					break;
				}
			return 0;
		}
	} else {
		fout << 0 << endl;
		return 0;
	}
	return 0;
}

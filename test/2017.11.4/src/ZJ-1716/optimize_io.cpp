#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <iostream>
using namespace std;

FILE *fin = fopen("test.in", "r");
FILE *fout = fopen("test.out", "w");

namespace IO {
	static const int SZ = 1 << 20; // 1M
	char ibuf[SZ], *is = ibuf, *it = ibuf;
	inline char nextchar() {
		if (is == it) {
			it = (is = ibuf) + fread(ibuf, sizeof(char), 1, fin);
			if (is == it)
				return EOF;
		}
		return *is++;
	}
	inline int read() {
		int res = 0, flag = 1;
		char ch;
		for (ch = nextchar(); isspace(ch); ch = nextchar())
			;
		if (ch == '-')
			ch = nextchar(), flag = -1;
		for (; isdigit(ch); ch = nextchar())
			res = res * 10 + ch - '0';
		return res * flag;
	}
	char obuf[SZ], *os = obuf, *ot = obuf + SZ;
	inline void flush() {
		fwrite(obuf, 1, os - obuf, fout);
		os = obuf;
	}
	inline void print(char ch) {
		if (os == ot)
			flush();
		*os++ = ch;
	}
	inline void print(int x) {
		static int dig[20], cnt;
		if (os + 20 > ot)
			flush();
		cnt = 0;
		do {
			dig[++cnt] = x % 10;
			x /= 10;
		} while (x);
		for (int i = cnt; i >= 1; --i)
			*os++ = dig[i] + '0';
	}
	inline void printap(int x, char ch) {
		print(x);
		print(ch);
	}
}

using IO::read;

int main() {
	int n = read();
	
	for (int i = 1; i <= n; ++i) {
		int x = read();
		
	}
	return 0;
}

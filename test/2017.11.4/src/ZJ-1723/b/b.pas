program ec12;
var 
	n,m,i,j,x,y,z:longint;
	minx,miny,minz,ans:int64;
begin 
	assign(input,'b.in'); reset(input);
	assign(output,'b.out'); rewrite(output);
	readln(n);
	minx:=10000000;
	miny:=10000000;
	minz:=10000000;
	for i:=1 to n do 
	begin 
		readln(x,y,z);
		if x<minx then 
		minx:=x;
		if y<miny then 
		miny:=y;
		if z<minz then 
		minz:=z;
	end;
	if (minx=miny) and (miny=minz) then 
	writeln(minx*1000000*1000000)
	else
	begin 
		if minx<miny then 
		writeln(minx*1000000*1000000)
		else
		writeln(miny*1000000*1000000);
	end;
	close(input);
	close(output);
end. 
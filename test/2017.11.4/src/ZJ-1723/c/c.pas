program ec12;
var 
	n,m,i,j,x1,t,ans,p,q:longint;
	tree,l,r,len,mid:array[1..1000000] of longint;
	a:array[1..10000] of longint;
procedure build(root,left,right:longint);
begin 
	l[root]:=left;
	r[root]:=right;
	mid[root]:=(left+right) div 2;
	len[root]:=right-left+1;
	if left=right then 
	begin 	
		tree[root]:=a[left];
		exit;
	end;
	build(root*2,l[root],mid[root]);
	build(root*2+1,mid[root]+1,r[root]);
	tree[root]:=tree[root*2] xor tree[root*2+1];
end; 
function take(root,x,y:longint):longint;
begin 
	if (x>r[root]) or (y<l[root]) then 
	exit(0);
	if (x<=l[root]) and (y>=r[root]) then 
	begin 
		take:=tree[root];
		exit;
	end;
	take:=0;
	if x<=mid[root] then 
	take:=take xor take(root*2,x,y);
	if y>mid[root] then 
	take:=take xor take(root*2+1,x,y);
end;
begin 
	assign(input,'c.in'); reset(input);
	assign(output,'c.out'); rewrite(output);
	readln(n,m);
	for i:=1 to n do 
	read(a[i]);
	build(1,1,n);
	for q:=1 to m do 
	begin 
		read(t,x1);
		ans:=0;
		if t=2 then 
		begin 
			for j:=1 to n do 
			begin 
				for p:=1 to n do
				begin 
					if (j=p) or (j=x1) or (p=x1) then 
					continue;
					if take(1,j,x1)<take(1,x1+1,p) then 
					inc(ans);
				end;
			end;
		end;
		if t=3 then 
		begin 
			for j:=1 to n do 
			begin 
				for p:=1 to n do
				begin 
					if (j=p) or (j=x1) or (p=x1) then 
					break;
					if take(1,j,p)<take(1,p+1,x1) then 
					inc(ans);
				end;
			end;
		end;
		writeln(ans);
	end;
	close(input);
	close(output);
end. 
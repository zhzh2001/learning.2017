program ec12;
var 	
	two:array[0..11] of longint=(1,2,4,8,16,32,64,128,256,512,1024,2048);
	sep:array[1..20] of integer;
	a:array[1..300,1..300] of char;
	w,len:array[1..300] of longint;
	vis:array[1..300] of boolean;
	c:char;
	s:ansistring;
	n,m,i,j,v,sum,tot,ans,l:longint;
	flag:boolean;
function min(x,y:longint):longint;
begin 
	if x<y then min:=x
	else min:=y;
end;
function chai(x:longint):longint;
var	
	o:longint;
begin
	o:=0;
	while x>0 do 
	begin 
		inc(o);
		sep[o]:=x mod 2;
		x:=x div 2;
	end;
	chai:=o;
end;
function check(k:longint):boolean;
var 
	o,left:longint;
begin 
	left:=0;
	if len[k] mod 2=1 then 
	exit(false);
	for o:=1 to len[k] do 
	begin 
		if a[k,o]='(' then 
		inc(left)
		else
		dec(left);
		if left<0 then 
		exit(false);
	end;
	if left>0 then 
	exit(false)
	else
	exit(true);
end;
procedure qsrt(l,r:longint);
var 
	i,j,v,s:longint;
begin 
	i:=l; j:=r; v:=w[(l+r) div 2];
	repeat
	while w[i]<v do inc(i);
	while w[j]>v do dec(j);
	if i<=j then 
	begin 
		s:=w[i];
		w[i]:=w[j];
		w[j]:=s;
		s:=len[i];
		len[i]:=len[j];
		len[j]:=s;
		inc(i);
		dec(j);
	end;
	until i>=j;
	if l<j then qsrt(l,j);
	if i<r then qsrt(i,r);
end;
begin 	
	assign(input,'a.in'); reset(input);
	assign(output,'a.out'); rewrite(output);
	readln(n);
	for i:=1 to n do 
	begin 
		readln(s);
		len[i]:=length(s);
		for j:=1 to len[i] do 
		begin 
			a[i,j]:=s[j];
			if s[j]='(' then 
			inc(w[i]);
			if s[j]=')' then 
			dec(w[i]);
		end;
	end;
	ans:=0;
	for i:=1 to n do 
	begin 	
		if check(i) then 
		begin 
			inc(ans,len[i]);
			vis[i]:=false;
		end;
	end;
	if (n<=10) then 
	begin 
		for j:=two[n] downto 1 do 
		begin 
			fillchar(sep,sizeof(sep),0);
			tot:=chai(n);
			sum:=0;
			v:=0;
			for i:=1 to tot do 
			begin 
				if (vis[i]) and (sep[i]>0) then 
				begin 
					inc(sum,len[i]);
					inc(v,w[i]);
				end;
			end;
			if sum mod 2=1 then 
			continue;
			if v<>0 then 
			continue;
			v:=0;
			flag:=true;
			for i:=1 to tot do 
			begin 
				for l:=1 to len[i] do 
				begin 
					if a[i,l]='(' then 
					inc(v)
					else
					dec(v);
					if v<0 then 
					begin 
						flag:=false;
						break;
					end;
				end;
				if v<>0 then 
				flag:=false;
			end;
			if flag then 
			begin 
				if ans<sum then 
				ans:=sum;
			end;
		end;
		writeln(ans);
	end;
	flag:=true;
	for i:=1 to n do 
	begin 
		if w[i]<=0 then 
		flag:=false;
	end;
	if flag then 
	begin 
		writeln(0);
		close(input);
		close(output);
		halt;
	end;
	if ans=0 then 
	begin 
		qsrt(1,n);
		tot:=0;
		for i:=1 to n do 
		begin 
			if w[i]<0 then 
			inc(tot,w[i])
			else
			begin 
				if w[i]>0 then 
				begin 
					if tot+w[i]>=0 then 
					inc(ans,(-tot)*2)
					else
					inc(ans,w[i]*2);
					tot:=tot+w[i];
				end;
			end;
		end;
		writeln(ans);
		close(input);
		close(output);
		halt;
	end;
	close(input);
	close(output);
end. 
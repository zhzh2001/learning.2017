#include<fstream>
#include<ctime>
#include<random>
using namespace std;
ofstream fout("c.in");
const int n=3000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<0<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(1,1<<30);
		fout<<d(gen)<<' ';
	}
	fout<<endl;
	return 0;
}

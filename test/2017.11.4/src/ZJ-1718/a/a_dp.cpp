#include<fstream>
#include<string>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N=305;
int n,len[N],last[N],low[N],f[N][N*2];
int dp(int i,int j)
{
	if(j>N||j<-N)
		return 0;
	if(!j)
		return len[i];
	if(~f[i][j+N])
		return f[i][j+N];
	f[i][j+N]=0;
	for(int k=1;k<=n;k++)
		if(j+low[k]>=0)
		{
			int ret=dp(k,j+last[k]);
			if(ret)
				f[i][j+N]=max(f[i][j+N],ret+len[i]);
		}
	return f[i][j+N];
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		string s;
		fin>>s;
		len[i]=s.length();
		int now=0;
		low[i]=0;
		for(int j=0;j<len[i];j++)
		{
			if(s[j]=='(')
				now++;
			else
				now--;
			low[i]=min(low[i],now);
		}
		last[i]=now;
	}
	memset(f,-1,sizeof(f));
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,dp(i,last[i]));
	fout<<ans<<endl;
	return 0;
}

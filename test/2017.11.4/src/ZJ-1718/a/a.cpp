#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N = 305, INF = 1e9;
struct node
{
	int l, r, len;
	bool operator<(const node &rhs) const
	{
		if ((l >= r) != (rhs.l >= rhs.r))
			return (l >= r) > (rhs.l >= rhs.r);
		if (l >= r)
			return r < rhs.r;
		return l > rhs.l;
	}
} a[N];
int f[N * N];
inline void update(int &x, int y)
{
	if (y > x)
		x = y;
}
int main()
{
	int n;
	fin >> n;
	int sum = 0;
	for (int i = 1; i <= n; i++)
	{
		string s;
		fin >> s;
		a[i].len = s.length();
		int l = 0, r = 0;
		for (int j = 0; j < a[i].len; j++)
			if (s[j] == '(')
				l++;
			else if (l)
				l--;
			else
				r++;
		a[i].l = l;
		a[i].r = r;
		sum += l;
	}
	sort(a + 1, a + n + 1);
	fill(f + 1, f + sum, -INF);
	for (int i = 1; i <= n; i++)
		if (a[i].l >= a[i].r)
			for (int j = sum; j >= a[i].r; j--)
				update(f[j + a[i].l - a[i].r], f[j] + a[i].len);
		else
			for (int j = a[i].r; j <= sum; j++)
				update(f[j + a[i].l - a[i].r], f[j] + a[i].len);
	fout << f[0] << endl;
	return 0;
}
#include<fstream>
#include<ctime>
#include<random>
using namespace std;
ofstream fout("b.in");
const int n=5000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(1,1e6);
		fout<<d(gen)<<' '<<d(gen)<<' '<<d(gen)<<endl;
	}
	return 0;
}

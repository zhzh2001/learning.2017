#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N = 200005, INF = 1e6;
int y[N];
struct point
{
	int x, y, z;
	bool operator<(const point &rhs) const
	{
		return x < rhs.x;
	}
} p[N];
struct node
{
	int lazy, max;
	long long maxs;
} tree[1 << 19];
void build(int id, int l, int r)
{
	if (l == r)
	{
		tree[id].max = INF;
		tree[id].maxs = 1ll * y[l] * INF;
	}
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		tree[id].max = max(tree[id * 2].max, tree[id * 2 + 1].max);
		tree[id].maxs = max(tree[id * 2].maxs, tree[id * 2 + 1].maxs);
	}
}
inline void pushdown(int id, int l, int r)
{
	if (tree[id].lazy)
	{
		int mid = (l + r) / 2;
		tree[id * 2].max = tree[id * 2].lazy = tree[id].lazy;
		tree[id * 2].maxs = 1ll * y[mid] * tree[id].lazy;
		tree[id * 2 + 1].max = tree[id * 2 + 1].lazy = tree[id].lazy;
		tree[id * 2 + 1].maxs = 1ll * y[r] * tree[id].lazy;
		tree[id].lazy = 0;
	}
}
void cover(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
	{
		tree[id].max = tree[id].lazy = val;
		tree[id].maxs = 1ll * y[r] * val;
	}
	else
	{
		pushdown(id, l, r);
		int mid = (l + r) / 2;
		if (L <= mid)
			cover(id * 2, l, mid, L, R, val);
		if (R > mid)
			cover(id * 2 + 1, mid + 1, r, L, R, val);
		tree[id].max = max(tree[id * 2].max, tree[id * 2 + 1].max);
		tree[id].maxs = max(tree[id * 2].maxs, tree[id * 2 + 1].maxs);
	}
}
int query(int id, int l, int r, int val)
{
	if (l == r)
		return l;
	pushdown(id, l, r);
	int mid = (l + r) / 2;
	if (tree[id * 2 + 1].max > val)
		return query(id * 2 + 1, mid + 1, r, val);
	return query(id * 2, l, mid, val);
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
	{
		fin >> p[i].x >> p[i].y >> p[i].z;
		y[i] = p[i].y;
	}
	sort(p + 1, p + n + 1);
	p[++n].x = INF;
	y[n] = INF;
	sort(y + 1, y + n + 1);
	build(1, 1, n);
	long long ans = 0;
	for (int i = 1, j; i <= n; i = j)
	{
		ans = max(ans, p[i].x * tree[1].maxs);
		for (j = i; j <= n && p[j].x == p[i].x; j++)
		{
			int l = upper_bound(y + 1, y + n + 1, p[j].y) - y, r = query(1, 1, n, p[j].z);
			if (l <= r)
				cover(1, 1, n, l, r, p[j].z);
		}
	}
	fout << ans << endl;
	return 0;
}
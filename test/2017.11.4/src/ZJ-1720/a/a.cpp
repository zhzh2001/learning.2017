#include<bits/stdc++.h>
using namespace std;
int n,c,f[2][90505];
char s[305];
struct jdb{
	int l,del,mn;
}a[305];
bool cmp(jdb a,jdb b){
	if (a.mn-a.del!=b.mn-b.del)
		return a.mn-a.del<b.mn-b.del;
	if (a.del!=b.del) return a.del>b.del;
	if (a.mn!=b.mn) return a.mn>b.mn;
	return a.l>b.l;
}
void mx(int &x,int y){
	x=x>y?x:y;
}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout); 
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%s",s+1);
		int len=strlen(s+1);
		int mn=0,tmp=0;
		for (int j=1;j<=len;j++){
			tmp+=(s[j]=='('?1:-1);
			mn=min(mn,tmp);                                     
		}
		a[i].l=len;
		a[i].del=tmp;
		a[i].mn=mn;
	}
	sort(a+1,a+n+1,cmp);
	memset(f,233,sizeof(f));
	f[0][0]=0; c=0;
	for (int i=1;i<=n;i++){
		c^=1;
		memcpy(f[c],f[c^1],sizeof(f[c]));
		for (int j=0;j<=n*300;j++)
			if (j+a[i].mn>=0)
				mx(f[c][j+a[i].del],f[c^1][j]+a[i].l);
	}
	printf("%d",f[c][0]);
}

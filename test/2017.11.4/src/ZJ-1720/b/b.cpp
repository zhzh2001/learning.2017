#include<bits/stdc++.h>
#define N 200005
#define ll long long
using namespace std;
namespace FastIO{
	const int AXS=2333333;
	char LZH[AXS],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,AXS,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline void write(ll x){
		if (!x){
			puts("0");
			return;
		}
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		puts("");
	}
};
using namespace FastIO;
int n;
ll ans;
struct P{
	int x,y,z;
	bool operator <(const P &a)const{
		return x<a.x;
	}
}a[N];
set<P> s;
struct S{
	ll x; int y;
	bool operator <(const S &a)const{
		return x<a.x;
	}
};
set<S> Set;
bool cmp(P a,P b){
	return a.z<b.z;
}
ll calc(P a,P b){
	return 1ll*min(a.y,1000000)*min(b.x,1000000);
}
void deleteS(ll x){
	//printf("deleteS %lld\n",x);
	set<S>::iterator it;
	S tmp=*Set.lower_bound((S){x,0});
	Set.erase(tmp); tmp.y--;
	if (tmp.y) Set.insert(tmp);
}
void insertS(ll x){
	set<S>::iterator it;
	S tmp=*Set.lower_bound((S){x,0});
	//printf("insertS %lld\n",x);
	if (tmp.x!=x)
		Set.insert((S){x,1});
	else{
		Set.erase(tmp);
		tmp.y++;
		Set.insert(tmp);
	}
}
ll queryS(){
	set<S>::iterator it;
	it=Set.end(); it--; it--;
	return it->x;
}
void deleteP(P x){
	s.erase(x);
	set<P>::iterator it;
	it=s.lower_bound(x);
	P nxt=*it,pre=*(--it);
	deleteS(calc(pre,x));
	deleteS(calc(x,nxt));
	insertS(calc(pre,nxt));
} 
void insertP(P x){
	set<P>::iterator it;
	it=s.lower_bound(x);
	P nxt=*it,pre=*(--it);
	insertS(calc(pre,x));
	insertS(calc(x,nxt));
	deleteS(calc(pre,nxt));
	s.insert(x);
}
void insert(P x){
	//printf("%d %d %d\n",x.x,x.y,x.z);
	set<P>::iterator it;
	it=s.lower_bound(x);
	P tmp=*it; --it;
	//printf("%d %d %d\n",tmp.x,tmp.y,tmp.z);
	if (x.y>=it->y)
		return;
	//deleteP(tmp);
	//tmp=*s.lower_bound(x);
	for (;tmp.y>=x.y;){
		//printf("%d\n",tmp.x);
		deleteP(tmp);
		tmp=*s.lower_bound(x);
	}
	insertP(x);
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	s.insert((P){0,1000002});
	s.insert((P){1000002,0});
	Set.insert((S){1ll*1000000*1000000,1});
	Set.insert((S){1e18,2333333});
	for (int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read(),a[i].z=read();
	sort(a+1,a+n+1,cmp);
	for (int i=1,j;i<=n;i=j){
		//for (j=i;a[i].z==a[j].z&&j<=n;j++)
		//	insert(a[i]);
		ans=max(ans,1ll*a[i].z*queryS());
		//printf("%lld\n",queryS());
		for (j=i;a[i].z==a[j].z&&j<=n;j++)
			insert(a[i]);
		//set<S>::iterator it;
		//for (it=Set.begin();it!=Set.end();it++)
		//	printf("z:%d sz:%lld sum:%d\n",a[i].z,it->x,it->y);
	}
	write(ans);
} 

#include<bits/stdc++.h>
#define mo 1000000007
#define ll long long
#define N 6500000
#define M 200005
using namespace std;
int ch[N][2],sz[N],a[M],f[M][35][2];
ll v[2][N],sum01[35],sum10[35];
ll cnt2[M],cnt3[M];
int n,q,x,sum,fl,size;
void insert2(int x,int v0,int v1){
	int rt=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		if (!ch[rt][tmp]) ch[rt][tmp]=++size;
		sum01[i]-=v[0][ch[rt][0]]*v[1][ch[rt][1]];
		sum10[i]-=v[1][ch[rt][0]]*v[0][ch[rt][1]];
		v[0][ch[rt][tmp]]+=v0;
		v[1][ch[rt][tmp]]+=v1;
		sum01[i]+=v[0][ch[rt][0]]*v[1][ch[rt][1]];
		sum10[i]+=v[1][ch[rt][0]]*v[0][ch[rt][1]];
		rt=ch[rt][tmp];
	}
}
ll query2(int x){
	ll ans=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		if (tmp<(tmp^1)) ans+=sum01[i];
		else ans+=sum10[i];
		//if (i<=5)
		//	printf("query2 %d %d %lld %lld %lld\n",i,tmp,sum01[i],sum10[i],ans);
	}
	return ans;
}
void pre3(){
	for (int i=1;i<=n;i++)
		for (int j=0;j<=30;j++){
			f[i][j][0]=f[i-1][j][0];
			f[i][j][1]=f[i-1][j][1];
			int tmp=((a[i]&(1<<j))!=0?1:0);
			f[i][j][tmp]++;
		}
}
void insert3(int x,int id){
	int rt=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		sz[ch[rt][tmp]]++;
		v[0][ch[rt][tmp]]+=f[id][i][0];
		v[1][ch[rt][tmp]]+=f[id][i][1];
		rt=ch[rt][tmp];
	}
}
ll query3(int x,int id){
	ll ans=0; int rt=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		if (tmp^1<tmp){
			ans+=1ll*sz[ch[rt][tmp^1]]*f[id][i][0];
			ans-=v[0][ch[rt][tmp^1]];
		}
		else{
			ans+=1ll*sz[ch[rt][tmp^1]]*f[id][i][1];
			ans-=v[1][ch[rt][tmp^1]];
		}
		rt=ch[rt][tmp];
	}
	return ans;
}
int main(){
	freopen("big c.in","r",stdin);
	//freopen("bf.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++)
		scanf("%d",&x),a[i]=a[i-1]^x;
	ll ans=0;
	for (int i=1;i<190987;i++){
		for (int j=0;j<i;j++)
			if ((a[j]^a[i])<(a[190987]^a[i]))
				ans++;
		if (i%100==0) printf("%d\n",i);
	}
	printf("%lld",ans); 
} 

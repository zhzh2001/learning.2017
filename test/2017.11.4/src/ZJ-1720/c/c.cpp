#include<ctime>
#include<cstdio>
#include<cstring>
#define mo 1000000007
#define ll long long
#define N 6500000
#define M 200005
using namespace std;
namespace FastIO{
	const int AXS=2333333;
	char LZH[AXS],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,AXS,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline void write(ll x){
		if (!x){
			puts("0");
			return;
		}
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		puts("");
	}
};
using namespace FastIO;
int ch[N][2],sz[N],a[M],f[M][35][2];
ll v[2][N],sum01[35],sum10[35];
ll cnt2[M],cnt3[M];
int n,q,x,sum,fl,size;
void insert2(int x,int v0,int v1){
	int rt=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		if (!ch[rt][tmp]) ch[rt][tmp]=++size;
		if (tmp==0){
			sum01[i]+=v0*v[1][ch[rt][1]];
			sum10[i]+=v1*v[0][ch[rt][1]];
		}
		else{
			sum10[i]+=v0*v[1][ch[rt][0]];
			sum01[i]+=v1*v[0][ch[rt][0]];
		}
		v[0][ch[rt][tmp]]+=v0;
		v[1][ch[rt][tmp]]+=v1;
		rt=ch[rt][tmp];
	}
}
ll query2(int x){
	ll ans=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		if (tmp<(tmp^1)) ans+=sum01[i];
		else ans+=sum10[i];
		//if (i<=5)
		//	printf("query2 %d %d %lld %lld %lld\n",i,tmp,sum01[i],sum10[i],ans);
	}
	return ans;
}
void pre3(){
	for (int i=1;i<=n;i++)
		for (int j=0;j<=30;j++){
			f[i][j][0]=f[i-1][j][0];
			f[i][j][1]=f[i-1][j][1];
			int tmp=((a[i]&(1<<j))!=0?1:0);
			f[i][j][tmp]++;
		}
}
void insert3(int x,int id){
	int rt=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		sz[ch[rt][tmp]]++;
		v[0][ch[rt][tmp]]+=f[id][i][0];
		v[1][ch[rt][tmp]]+=f[id][i][1];
		rt=ch[rt][tmp];
	}
}
ll query3(int x,int id){
	ll ans=0; int rt=0;
	for (int i=30;i>=0;i--){
		int tmp=((x&(1<<i))!=0?1:0);
		if (tmp^1<tmp){
			ans+=1ll*sz[ch[rt][tmp^1]]*f[id][i][0];
			ans-=v[0][ch[rt][tmp^1]];
		}
		else{
			ans+=1ll*sz[ch[rt][tmp^1]]*f[id][i][1];
			ans-=v[1][ch[rt][tmp^1]];
		}
		rt=ch[rt][tmp];
	}
	return ans;
}
int main(){
	//int u=clock();
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read(); q=read();
	for (int i=1;i<=n;i++)
		a[i]=a[i-1]^read();
	//write(clock()-u);
	for (int i=1;i<=n;i++)
		insert2(a[i],0,1);
	insert2(a[0],1,0);
	for (int i=1;i<=n;i++){
		insert2(a[i],0,-1);
		//printf("%d\n",a[i]);
		cnt2[i]=query2(a[i]);
		sum=(sum+cnt2[i])%mo;
		insert2(a[i],1,0);
		//printf("%lld\n",cnt2[i]);
	}
	write(sum);
	//write(clock()-u);
	memset(v,0,sizeof(v));
	pre3();
	insert3(0,0);
	for (int i=1;i<=n;i++){
		cnt3[i]=query3(a[i],i);
		insert3(a[i],i);
	}
	while (q--){
		fl=read(); x=read();
		//printf("%d\n",x);
		if (fl==2) 
			write(cnt2[x]);
		else write(cnt3[x]);
	}
} 

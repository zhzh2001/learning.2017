//#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<string.h>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
int n;
int len[303],mn[303][303],sum[303][303];
char ch[303][303];
int jiyi[303][90003];
int mnn[303],summ[303];
inline int max(const int a,const int b){
	if(a>b) return a;
	return b;
}
int dp(int now,int tot){
	int &fanhui=jiyi[now][tot];
	if(fanhui!=-1) return fanhui;
	if(now==n+1){
		if(tot==0) return fanhui=0;
		else return fanhui=-999999999;
	}
	fanhui=dp(now+1,tot);
	if(mnn[now]+tot>=0){
		fanhui=max(fanhui,dp(now+1,tot+summ[now])+len[now]);
	}
	return fanhui;
}
int main(){
	memset(jiyi,-1,sizeof(jiyi));
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>(ch[i]+1);
		len[i]=strlen(ch[i]+1);
	}
	for(int i=1;i<=n;i++){
		if(ch[i][1]==')') sum[i][1]=-1;
		else sum[i][1]=1;
		mn[i][1]=sum[i][1];
		for(int j=2;j<=len[i];j++){
			if(ch[i][j]==')') sum[i][j]=sum[i][j-1]-1;
			else sum[i][j]=sum[i][j-1]+1;
			mn[i][j]=min(mn[i][j-1],sum[i][j]);
		}
	}
	for(int i=1;i<=n;i++){
		mnn[i]=mn[i][len[i]];
		summ[i]=sum[i][len[i]];
	}
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if((mnn[i]-summ[j])<=mnn[j]-summ[i]){
				swap(mnn[i],mnn[j]),swap(summ[i],summ[j]),swap(len[i],len[j]);
			}
		}
	}
	int ans=0;
	for(int i=1;i<=n;i++){
		if(mnn[i]>=0){
			ans=max(ans,dp(i+1,summ[i])+len[i]);
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if((mnn[i]-summ[i])>mnn[j]-summ[j]){
				swap(mnn[i],mnn[j]),swap(summ[i],summ[j]),swap(len[i],len[j]);
			}
		}
	}
	memset(jiyi,-1,sizeof(jiyi));
	for(int i=1;i<=n;i++){
		if(mnn[i]>=0)ans=max(ans,dp(i+1,summ[i])+len[i]);
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
3
)()
())
((()

out:
10

*/

//#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<string.h>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int mod=1000000007;
struct query{
	int opt,x;
}q[200003];
int n,m,num[200003],pre[200003];
inline void solve1(){
	for(int i=1;i<=n;i++)pre[i]=pre[i-1]^num[i];
	int ans=0;
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n;j++){
			for(int k=j+1;k<=n;k++){
				if(i<=j&&j<k&&(pre[k]^pre[j])>(pre[j]^pre[i-1]))ans++;
			}
		}
	}
	fout<<ans%mod<<endl;
	for(int i=1;i<=m;i++){
		int opt,x;
		ans=0;
		fin>>opt>>x;
		if(opt==2){
			for(int j=1;j<=x;j++){
				for(int k=x+1;k<=n;k++){
					if(j<=x&&x<k&&(pre[k]^pre[x])>(pre[x]^pre[j-1]))ans++;
				}
			}
		}else{
			for(int j=1;j<=x;j++){
				for(int k=1;k<=j;k++){
					if(k<=j&&j<x&&(pre[x]^pre[j])>(pre[j]^pre[k-1]))ans++;
				}
			}
		}
		fout<<ans<<endl; 
	}
}
int tr[3000003][2],sum[3000003],xx[33],tot;
inline void insert(int x){
	for(int i=30;i>=0;i--){
		if(x&(1<<i))xx[i]=1;
	}
	int now=0;
	for(int i=30;i>=0;i--){
		if(!tr[now][xx[i]]){
			tr[now][xx[i]]=++tot;
		}
		now=tr[now][xx[i]];
		sum[now]++;
	}
}
inline bool cmp(const query a,const query b){
	if(a.opt==b.opt){
		return a.x<b.x;
	}
	return a.opt<b.opt;
}
inline void solve2(){
	for(int i=1;i<=m;i++){
		fin>>q[i].opt>>q[i].x;
	}
	sort(q+1,q+m+1,cmp);
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>num[i];
	}
	if(n<=300){
		solve1();
		return 0;
	}else if(n<=5000){
		solve2();
		return 0;
	}
	return 0;
}
/*

in:
5 10
1 3 1 7 1
2 1
2 2
2 3
2 4
2 5
3 1
3 2
3 3
3 4
3 5

out:
14
4
4
6
0
0
0
1
1
6
6

*/

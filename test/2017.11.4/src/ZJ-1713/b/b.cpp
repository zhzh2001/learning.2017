//#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<string.h>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
int n;
struct mat{
	long long x,y,z;
}ju[200003];
inline int max(const int a,const int b){
	if(a>b) return a;
	return b;
}
inline int min(const int a,const int b){
	if(a<b) return a;
	return b;
}
inline bool cmp(const mat a,const mat b){
	if(a.x==b.x) return a.y==b.y?a.z<b.z:a.y<b.y;
	return a.x<b.x;
}
inline bool cmp2(const mat a,const mat b){
	if(a.y==b.y) return a.z==b.z?a.x<b.x:a.z<b.z;
	return a.y<b.y;
}
inline bool cmp3(const mat a,const mat b){
	if(a.z==b.z) return a.x==b.x?a.y<b.y:a.x<b.x;
	return a.z<b.z;
}
inline void solve(){
	long long ans=0,mn;
	sort(ju+1,ju+n+1,cmp);
	for(int i=1;i<=n;i++){
		sort(ju+1,ju+i+1,cmp2);
		ans=max(ju[1].z*1000000000000,ans);
		ans=max(ans,ju[n].x*1000000000000);
		mn=1000000;
		for(int j=1;j<=i;j++){
			mn=min(mn,ju[j].y);
			ans=max(ans,ju[i].x*ju[j].z*mn);
		}
	}
	fout<<ans<<endl;
}
inline void solve2(){
	long long ans=1000000000000,mn=999999999;
	sort(ju+1,ju+n+1,cmp);
	ans=max(ju[1].x*1000000000000,ju[n].y*1000000000000);
	for(int i=1;i<=n;i++){
		ans=max(ans,mn*ju[i].x*1000000);
		mn=min(mn,ju[i].y);
	}
	fout<<ans<<endl;
}
int main(){
	fin>>n;
	bool yes=true;
	for(int i=1;i<=n;i++){
		fin>>ju[i].x>>ju[i].y>>ju[i].z;
		if(ju[i].z!=1){
			yes=false;
		}
	}
	if(yes){
		solve2();
		return 0;
	}
	if(n<=5000){
		solve();
		return 0;
	}
	sort(ju+1,ju+n+1,cmp);
	int len=0;
	for(int i=1;i<=n;i++){
		if(ju[i].y>ju[i+1].y||ju[i].z>ju[i+1].z){
			ju[++len].x=ju[i].x,ju[len].y=ju[i].y,ju[len].z=ju[i].z;
		}
	}
	n=len;
	sort(ju+1,ju+n+1,cmp2);
	len=0;
	for(int i=1;i<=n;i++){
		if(ju[i].y>ju[i+1].y||ju[i].z>ju[i+1].z){
			ju[++len].x=ju[i].x,ju[len].y=ju[i].y,ju[len].z=ju[i].z;
		}
	}
	n=len;
	sort(ju+1,ju+n+1,cmp3);
	len=0;
	for(int i=1;i<=n;i++){
		if(ju[i].y>ju[i+1].y||ju[i].z>ju[i+1].z){
			ju[++len].x=ju[i].x,ju[len].y=ju[i].y,ju[len].z=ju[i].z;
		}
	}
	n=len;
	ju[++n].x=1000000,ju[n].y=1000000,ju[n].z=1000000;
	long long ans=0,now=1000000000000;
	sort(ju+1,ju+n+1,cmp);
	for(int i=1;i<=n;i++){
		now=min(now,ju[i].y*ju[i].z);
		ans=max(ans,ju[i+1].x*now);
	}
	now=1000000000000;
	sort(ju+1,ju+n+1,cmp2);
	for(int i=1;i<=n;i++){
		now=min(now,ju[i].x*ju[i].z);
		ans=max(ans,ju[i+1].y*now);
	}
	now=1000000000000;
	sort(ju+1,ju+n+1,cmp3);
	for(int i=1;i<=n;i++){
		now=min(now,ju[i].x*ju[i].y);
		ans=max(ans,ju[i+1].z*now);
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
2
2 2 1
2 2 1

5
2 5 5
6 9 8
0 3 0
7 1 9
9 8 5

out:
1000000000000

*/

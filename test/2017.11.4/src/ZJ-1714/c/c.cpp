#pragma GCC optimize(2)
#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define lf else if
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=3010;
ll n,Q,ans1[N],ans2[N],a[N],answ,mod=1e9+7;
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();	Q=read();
	For(i,1,n)	a[i]=read()^a[i-1];
	For(t,1,n)	For(i,1,t)		For(j,t+1,n)	ans1[t]+=(a[i-1]^a[t])<(a[t]^a[j]);
	For(t,1,n)	For(i,1,t-1)	For(j,i,t-1)	ans2[t]+=(a[i-1]^a[j])<(a[j]^a[t]);
	For(i,1,n)	answ+=ans2[i];
	writeln(answ%mod);
	while(Q--){
		ll opt=read(),x=read();
		if (opt==2)	writeln(ans1[x]);
		else	writeln(ans2[x]);
	}
}

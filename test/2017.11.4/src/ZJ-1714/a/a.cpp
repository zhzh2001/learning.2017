#include<bits/stdc++.h>
#define ll int
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(long long x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(long long x){ write(x);   puts("");   }
const ll N=310;
char s[N];	struct data{	ll sum,pre,sz;	}q[N];
ll f[N*N],n,m,sum;
bool operator <(data a,data b){
	if ((a.sum>=0)^(b.sum>=0))	return a.sum>b.sum;
	if (a.sum>=0)	return a.pre>b.pre;
	if (a.sum<0)	return a.pre<b.pre;
}
inline void add(ll &x,ll y){	if (y>x)	x=y;	}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();
	For(i,1,n){
		scanf("%s",s+1);	m=strlen(s+1);
		For(j,1,m){
			q[i].sum+=s[j]=='('?1:-1;
			q[i].pre=min(q[i].pre,q[i].sum);
			q[i].sz++;
		}
	}sort(q+1,q+n+1);
	memset(f,-30,sizeof f);	f[0]=0;
	For(i,1,n){
		ll down=max(0,-q[i].pre);
		if (q[i].sum>=0)	FOr(j,sum,down)	add(f[j+q[i].sum],f[j]+q[i].sz);
		else				For(j,down,sum)	add(f[j+q[i].sum],f[j]+q[i].sz);
		sum=max(sum,sum+q[i].sum);
	}writeln(f[0]);
}

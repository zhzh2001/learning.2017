#include<queue>
#include<set>
#include<map>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<vector>
#define ll long long
#define maxn 2010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){ write(x);   puts("");   }
const ll N=1010;
ll b[N],a[N][N],n,Q;
char s[N];
int main(){
	freopen("bracket.in","r",stdin);
	freopen("baoli.out","w",stdout);
	scanf("%s",s+1);	n=strlen(s+1);	
	For(i,1,n)	b[i]=(s[i]=='('?1:-1);
	For(i,1,n){
		ll sum=0,mn=0;
		For(j,i,n){
			sum+=b[j];
			if (sum<0)	break;
			if (sum==0)	++a[i][j];
		}
	}
	For(i,1,n)	For(j,1,n)	a[i][j]+=a[i-1][j]+a[i][j-1]-a[i-1][j-1];
	Q=read();
	while(Q--){
		ll l=read(),r=read();
		writeln(a[r][r]-a[l-1][r]-a[r][l-1]+a[l-1][l-1]);
	}
} 
/*
Q=1
*/

program c;
 var
  d:Array[0..200001,2..3] of longint;
  a,b:array[0..200001] of longint;
  i,j,k,m,n,sum,x,y:longint;
 begin
  assign(input,'c.in');
  assign(output,'c.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  b[0]:=0;
  for i:=1 to n do
   begin
    read(a[i]);
    b[i]:=b[i-1] xor a[i];
   end;
  sum:=0;
  for j:=1 to n do
   for i:=1 to j do
    for k:=j+1 to n do
     if b[j] xor b[i-1]<b[k] xor b[j] then
      begin
       inc(sum);
       inc(d[j,2]);
       inc(d[k,3]);
      end;
  writeln(sum);
  for i:=1 to m do
   begin
    readln(x,y);
    writeln(d[y,x]);
   end;
  close(input);
  close(output);
 end.

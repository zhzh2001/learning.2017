#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 200002
#define MOD 1000000007

#define Dp puts("")
#define Dw printf
#define Ds printf("#")

typedef long long ll;
char ch;
int n,m,a[N],opt,x,L,R;
ll aj[N],ak[N],sum;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

int main(void)
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	read(n),read(m);
	rep(i,1,n)read(a[i]);
	rep(i,1,n)
	{
		L=0;
		rep(j,i,n)
		{
			L^=a[j];
			R=0;
			rep(k,j+1,n)
			{
				R^=a[k];
				if(L<R)
				{
					aj[j]++;
					ak[k]++;
					sum++;
					if(sum==MOD)sum=0;
				}
			}
		}
	}
	cout<<sum<<endl;
	while(m--)
	{
		read(opt),read(x);
		if(opt==2)cout<<aj[x]<<endl;
		else cout<<ak[x]<<endl;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

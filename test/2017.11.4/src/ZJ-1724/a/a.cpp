#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

#define rp(i,t) for(int i=0;i<(t);i++)
#define rep(i,s,t) for(int i=(s);i<=(t);i++)
#define rsp(i,s,t) for(int i=(s);i<(t);i++)
#define rrp(i,t,s) for(int i=(t);i>=(s);i--)
#define rcp(i,s,b,d) for(int i=(s);b;d)
#define mst(a,x) memset(a,x,sizeof(a))
#define INF 0x3f3f3f3f
#define N 403

/*#define Dp puts("")
#define Dw printf
#define Ds printf("#")*/

typedef long long ll;
char ch,s[N];
int n,l[N],r[N],len[N],f[2][N][N],ans,tmp1,tmp2;

inline void read(int &x){x=0;do{ch=getchar();}while(ch<'0'||'9'<ch);do{x=x*10+ch-'0';ch=getchar();}while('0'<=ch&&ch<='9');}

inline void updata(int &x,const int &y){x=max(x,y);}

int main(void)
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d",&n);
	rep(i,0,1)rp(j,N)rp(k,N)f[i][j][k]=-INF;
	rep(i,1,n)
	{
		scanf("\n%s",s);
		len[i]=strlen(s);
		rp(j,len[i])
		{
			if(s[j]=='(')l[i]++;
			else
			{
				l[i]--;
				if(l[i]<0)l[i]=0,r[i]++;
			}
		}
		//Dw("%d:%d:%d\n",l[i],r[i],len[i]);
	}
	f[0][0][0]=0;
	rp(i,n)rp(j,N)rp(k,N)if(f[i&1][j][k]>=0)
	{
		tmp1=j+max(r[i+1]-k,0);
		tmp2=l[i+1]+max(k-r[i+1],0);
		if(tmp1<N&&tmp2<N)updata(f[(i&1)^1][tmp1][tmp2],f[i&1][j][k]+len[i+1]);
		tmp1=r[i+1]+max(j-l[i+1],0);
		tmp2=k+max(l[i+1]-j,0);
		if(tmp1<N&&tmp2<N)updata(f[(i&1)^1][tmp1][tmp2],f[i&1][j][k]+len[i+1]);
		updata(f[(i&1)^1][j][k],f[i&1][j][k]);
		/*Dw("(%d,%d,%d)->(%d,%d,%d)&",i,j,k,i+1,j+max(r[i+1]-k,0),l[i+1]+max(k-r[i+1],0));
		Dw("(%d,%d,%d)",i+1,r[i+1]+max(j-l[i+1],0),k+max(l[i+1]-j,0));
		Dw("[%d,%d]",f[i+1][j+max(r[i+1]-k,0)][l[i+1]+max(k-r[i+1],0)],f[i+1][r[i+1]+max(j-l[i+1],0)][k+max(l[i+1]-j,0)]);
		Dp;*/
	}
	/*rep(i,0,n)
	{
		Dw("%d",i);
		rp(j,5)
		{
			Dw("[");
			rp(k,5)Dw("%2d|",f[i][j][k]<0?0:f[i][j][k]);
		}
		printf("\n");
	}*/
	printf("%d",f[n&1][0][0]);
	fclose(stdin);fclose(stdout);
	return 0;
}

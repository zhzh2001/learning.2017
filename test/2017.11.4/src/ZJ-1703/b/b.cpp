//爆蛋了 
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=200005;
struct xxx{
	ll x,y,z;
}a[N];
ll n;
ll ans,x,y,z,tx,ty,tz,t,ma,mi=1e6;
inline ll read()
{
	ll x=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x;
}
bool com1(xxx a,xxx b){if(a.x!=b.x)return a.x<b.x;else if(a.y!=b.y)return a.y<b.y;else return a.z>b.z;}
//x递增且y递增，答案为x(now),y(min),1000000 || a[1].x,1000000,1000000
bool com2(xxx a,xxx b){if(a.y!=b.y)return a.y<b.y;else if(a.z!=b.z)return a.z<b.z;else return a.x>b.x;}
//y递增且z递增，答案为1000000,y(now),z(min) || 1000000,a[1].y,1000000
bool com3(xxx a,xxx b){if(a.z!=b.z)return a.z<b.z;else if(a.x!=b.x)return a.x<b.x;else return a.y>b.y;}
//z递增且x递增，答案为x(min),1000000,z(now) || 1000000,1000000,a[1].z
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read(),a[i].z=read(),ma=max(a[i].z,ma),mi=min(a[i].z,mi);
	if(ma==mi){
		printf("%lld",ma*1000000*100000);
		return 0;
	}
	if(n<=100){
		n++;
		a[n].x=1e6,a[n].y=1e6,a[n].z=1e6;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++){
					bool b=true;
					for(int l=1;l<=n;l++)
						if(a[i].x>a[l].x&&a[j].y>a[l].y&&a[k].z>a[l].z){b=false;break;}
					if(b)ans=max(a[i].x*a[j].y*a[k].z,ans);
				}
		printf("%lld",ans);
		return 0;
	}
	
	sort(a+1,a+n+1,com1);
	x=a[1].x,y=1e6,z=1e6;
	ans=max(ans,x*y*z);
	for(int i=2;i<=n;i++){
		x=a[i].x;y=min(y,a[i].y);
		ans=max(ans,x*y*z);
	}
	
	
	sort(a+1,a+n+1,com2);
	x=1e6,y=a[1].y,z=1e6;
	ans=max(ans,x*y*z);
	for(int i=2;i<=n;i++){
		y=a[i].y;z=min(z,a[i].z);
		ans=max(ans,x*y*z);
	}
	
	
	sort(a+1,a+n+1,com3);
	x=1e6;y=1e6;z=a[1].z;
	ans=max(ans,x*y*z);
	for(int i=2;i<=n;i++){
		z=a[i].z;x=min(x,a[i].x);
		ans=max(ans,x*y*z);
	}
	
	
	for(int i=1;i<=n;i++)
		swap(a[i].x,a[i].z);
	
	
	sort(a+1,a+n+1,com1);
	x=a[1].x,y=1e6,z=1e6;
	ans=max(ans,x*y*z);
	for(int i=2;i<=n;i++){
		x=a[i].x;y=min(y,a[i].y);
		ans=max(ans,x*y*z);
	}
	
	
	sort(a+1,a+n+1,com2);
	x=1e6,y=a[1].y,z=1e6;
	ans=max(ans,x*y*z);
	for(int i=2;i<=n;i++){
		y=a[i].y;z=min(z,a[i].z);
		ans=max(ans,x*y*z);
	}
	
	
	sort(a+1,a+n+1,com3);
	x=1e6;y=1e6;z=a[1].z;
	ans=max(ans,x*y*z);
	for(int i=2;i<=n;i++){
		z=a[i].z;x=min(x,a[i].x);
		ans=max(ans,x*y*z);
	}
	
	
	printf("%lld\n",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int N=305;
struct xxx{
	int l,r,a;
}t[N];
char ch[N];
int n,tot,ans;
int main()
{
	freopen("a.in","r",stdin);
	freopen("a2.out","w",stdout);
	srand(time(0));
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%s",ch+1);
		t[i].a=strlen(ch+1);
		tot=0;
		for(int j=1;j<=t[i].a;j++){
			if(ch[j]==')'){
				if(tot==0)t[i].r++;
				else tot--;
			}
			else tot++;
		}
		t[i].l=tot;
	}
	int T=2000000;
	while(T--){
		if(T%10000==0)srand(rand());
		for(int i=1;i<=n;i++)swap(t[(rand()%n+1)],t[i]);
		int k=0,x=0;
		for(int i=1;i<=n;i++){
			if(t[i].r>k)continue;
			k-=t[i].r;
			k+=t[i].l;
			x+=t[i].a;
			if(k==0)ans=max(ans,x);
		}
	}
	printf("%d",ans);
	return 0;
}

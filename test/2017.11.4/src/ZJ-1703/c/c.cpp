//GG,my friend
#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=200005;
const int p=1e9+7;
inline int read()
{
	int x=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;
	return x;
}
int a[N],f[N];
ll ans1[N],ans2[N],ans;
int n,m;
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	for(int i=1;i<=n;i++){
		f[i]=f[i-1]^a[i];
	}
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)
			for(int k=j+1;k<=n;k++)
				if(f[j]^f[i-1]<f[j]^f[k])
					ans1[j]++,ans2[k]++,ans++;
	ans%=p;
	printf("%d\n",ans);
	while(m--){
		int op=read(),x=read();
		if(op==2)printf("%d\n",ans1[x]);
		else printf("%d\n",ans2[x]);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=305,M=100005;
struct data
{
	int l,s,m;//,id;
	bool operator<(const data a)const
	{
		return m>a.m;
	}
}a[N];
int n,f[M],L,cnt;
char s[N];
struct da
{
	int x,y;
	bool operator<(const da &z)const
	{
		if (a[x].s<0&&a[z.x].s<0)
		{
			if (a[x].s!=a[x].m&&a[z.x].s==a[z.x].m)
				return 1;
			if (a[x].s==a[x].m&&a[z.x].s!=a[z.x].m)
				return 0;
			return a[x].s==a[z.x].s?y<z.y:a[x].s<a[z.x].s;
		}
		return a[x].s==a[z.x].s?y<z.y:a[x].s>a[z.x].s;
	}
};
set<da> S;
vector<int> q[N];
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		scanf("%s",s+1);
		a[i].l=strlen(s+1);
		for (int j=1;j<=a[i].l;++j)
		{
			a[i].s+=s[j]=='('?1:-1;
			a[i].m=min(a[i].m,a[i].s);
		}
		L+=a[i].l;
		q[-a[i].m].push_back(i);
	}
	memset(f,-0x3f,sizeof(f));
	f[0]=0;
	for (unsigned i=0;i<q[0].size();++i)
		S.insert((da){q[0][i],++cnt});
	q[0].clear();
	int l0=0,l1=0;
	//cout<<S.size()<<'\n';
	for (int ii=1;ii<=n;++ii)
	{
		if (S.empty())
			break;
		int i=S.begin()->x;
		S.erase(S.begin());
		// cout<<i<<' '<<a[i].l<<' '<<a[i].s<<' '<<a[i].m<<'\n';
		if (a[i].s==0)
			for (int j=-a[i].m;j<=L;++j)
				f[j]+=a[i].l;
		if (a[i].s>0)
			for (int j=L;j>=-a[i].m;--j)
				f[j+a[i].s]=max(f[j+a[i].s],f[j]+a[i].l);
		if (a[i].s<0)
			for (int j=-a[i].m;j<=L;++j)
				f[j+a[i].s]=max(f[j+a[i].s],f[j]+a[i].l);
		l1=l0+a[i].s;
		if (l1>=300)
			l1=-999999;
		//cout<<a[i].l<<' '<<l0<<' '<<l1<<'\n';
		if (l1>l0&&l1>0&&l0>=0)
		{
			for (int j=l0+1;j<=l1;++j)
			{
				for (unsigned k=0;k<q[j].size();++k)
					S.insert((da){q[j][k],++cnt});
				q[j].clear();
			}
		}
		l0=l1;
		// for (int i=0;i<=10;++i)
		// 	cout<<f[i]<<' ';
		// cout<<'\n';
		//cout<<S.size()<<'\n';
	}
	cout<<f[0];
	return 0;
}
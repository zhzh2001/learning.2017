#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=305,M=100005;
struct data
{
	int l,s,m;//,id;
	bool operator<(const data a)const
	{
		return m>a.m;
	}
}a[N];
int n,f[M],L;
char s[N];
int main()
{
	freopen("a.in","r",stdin);
	freopen("a2.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		scanf("%s",s+1);
		a[i].l=strlen(s+1);
		for (int j=1;j<=a[i].l;++j)
		{
			a[i].s+=s[j]=='('?1:-1;
			a[i].m=min(a[i].m,a[i].s);
		}
		L+=a[i].l;
	//	a[i].id=i;
	}
	sort(a+1,a+1+n);
	//for (int i=1;i<=n;++i)
	//	cout<<a[i].id<<' ';
	memset(f,-0x3f,sizeof(f));
	f[0]=0;
	for (int i=1;i<=n;++i)
	{
		if (a[i].s==0)
			for (int j=-a[i].m;j<=L;++j)
				f[j]+=a[i].l;
		if (a[i].s>0)
			for (int j=L;j>=-a[i].m;--j)
				f[j+a[i].s]=max(f[j+a[i].s],f[j]+a[i].l);
		if (a[i].s<0)
			for (int j=-a[i].m;j<=L;++j)
				f[j+a[i].s]=max(f[j+a[i].s],f[j]+a[i].l);
	}
	cout<<f[0];
	return 0;
}
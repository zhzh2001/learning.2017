#include<bits/stdc++.h>
using namespace std;
#define mp make_pair
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=200005,mod=1e9+7;
int a[N],n,x,y,q,b[N],l;
pair<int,int> c[N];
ll ans,f[N],g[N];
inline void write(ll x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
		return;
	}
	static int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	read(n);
	read(q);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		a[i]^=a[i-1];
	}	
	for (int i=1;i<n;++i)
	{
		for (int j=1;j<=i;++j)
			b[j]=a[i]^a[j-1];
		sort(b+1,b+1+i);
		for (int j=i+1;j<=n;++j)
			c[j]=mp(a[j]^a[i],j);
		sort(c+i+1,c+n+1);
		l=0;
		for (int j=i+1;j<=n;++j)
		{
			while (c[j].first>b[l+1]&&l<i)
				++l;
			f[i]+=l;
			g[c[j].second]+=l;
		}
		(ans+=f[i])%=mod;
	}
	write(ans);
	while (q--)
	{
		read(x);
		read(y);
		write(x==2?f[y]:g[y]);
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const ll inf=1000000,N=100005;
ll ans;
int n,mx,my,mz,kkk,cnt;
struct data
{
	int x,y,z;
	bool operator<(const data &a)const
	{
		return x<a.x;
	}
}a[N];
struct da
{
	int x,y;
	bool operator<(const da &a)const
	{
		return x==a.x?y<a.y:x<a.x;
	}
}b[N];
inline void work()
{
	sort(b+1,b+1+n);
	cnt=1;
	for (int i=2;i<=n;++i)
		if (b[i].y<b[cnt].y)
			b[++cnt]=b[i];
	for (int i=1;i<=cnt;++i)
		ans=max(ans,(ll)b[i].x*b[i].y*inf);
}
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	read(n);
	mx=my=mz=inf;
	kkk=1;
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].y);
		read(a[i].z);
		mx=min(mx,a[i].x);
		my=min(my,a[i].y);
		mz=min(mz,a[i].z);
		if (a[i].z!=1)
			kkk=0;
	}
	ans=max(mx,max(my,mz))*inf*inf;
	if (kkk==1)
	{
		for (int i=1;i<=n;++i)
			b[i]=(da){a[i].x,a[i].y};
		work();
		cout<<ans;
	}
	else
	{
		for (int i=1;i<=n;++i)
			b[i]=(da){a[i].x,a[i].y};
		work();
		for (int i=1;i<=n;++i)
			b[i]=(da){a[i].y,a[i].z};
		work();
		for (int i=1;i<=n;++i)
			b[i]=(da){a[i].x,a[i].z};
		work();
		for (int i=1;i<=n;++i)
		{
			bool flag=1;
			for (int j=1;j<n;++j)
				if (a[i].x>a[j].x&&a[i].y>a[j].y&&a[i].z>a[j].z)
				{
					flag=0;
					break;
				}
			if (flag)
				ans=max(ans,(ll)a[i].x*a[i].y*a[i].z);
		}
		cout<<ans;
	}
	return 0;
}

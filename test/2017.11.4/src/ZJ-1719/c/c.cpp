#pragma GCC optimize 2
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int mo=1e9+7;
int q[5001],top,ans1[5001],ans2[5001],opt,x,a[5001],n,ans,que;
inline int get_upper(int x)
{
	int l=1,r=top,ans=0;
	while(l<=r)
	{
		int mid=l+r>>1;
		if(q[mid]<x)	ans=mid,l=mid+1;else	r=mid-1;
	}
	return ans;
}
inline void pre()
{
	For(i,1,n)
	{
		For(j,1,top)	q[j]^=a[i];
		q[++top]=a[i];
		sort(q+1,q+top+1);
		int now=0;
		For(j,i+1,n)
		{
			now^=a[j];
			int t1=get_upper(now);
			ans1[i]+=t1;ans2[j]+=t1;	
			ans+=t1;if(ans>mo)	ans-=mo;
		}	
	}
}
int main()
{
	freopen("c.in","r",stdin);freopen("c.out","w",stdout);
	n=read();que=read();
	For(i,1,n)	a[i]=read();
	pre();
	printf("%d\n",ans);
	For(i,1,que)
	{
		opt=read();x=read();
		if(opt==2)	printf("%d\n",ans1[x]);	else	printf("%d\n",ans2[x]);
	}
}

#pragma GCC optimize 2
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
int n,len[501],ans,q[501];
bool vis[501];
char s[501][501],S[300001];
inline void check(int n)
{
	int zlen=0,zzlen=0;
	For(i,1,n)	zzlen+=len[q[i]];
	if(zzlen<=ans)	return;
	For(i,1,n)
		For(j,1,len[q[i]])
			S[++zlen]=s[q[i]][j];
	int tans=0,cnt=0;
	For(i,1,zlen)
	{
		if(S[i]=='(')	cnt++;
			else	cnt--;
		if(cnt<0)	return;
	}
	if(cnt==0)	ans=max(ans,zlen);
}
inline void dfs(int x)
{
	if(x!=1)	check(x-1);
	if(x==n+(n<10))	{return;}
	For(i,1,n)	if(!vis[i])	q[x]=i,vis[i]=1,dfs(x+1),vis[i]=0;
}
int len1,len2;
bool can1[500001],can2[500001];
inline void main2()
{
	For(i,1,n)	scanf("\n%s",s[i]+1),len[i]=strlen(s[i]+1);
	For(i,1,n)	if(s[i][1]=='(')	len1+=len[i];	else	len2+=len[i];
	if(len1*len2==0)	{puts("0");return;}
	can1[0]=can2[0]=1;
	For(i,1,n)
	{
		if(s[i][1]=='(')
			Dow(j,len[i],len1)
				can1[j]|=can1[j-len[i]];
		else	
			Dow(j,len[i],len2)
				can2[j]|=can2[j-len[i]];
	}
	Dow(i,0,len1)
		if(can1[i]&&can2[i])
		{
			printf("%d\n",i*2);
			return;
		}
}
int main()
{
	freopen("a.in","r",stdin);freopen("a.out","w",stdout);
	n=read();
	if(n>10)	{main2();return 0;}
	For(i,1,n)	scanf("\n%s",s[i]+1),len[i]=strlen(s[i]+1);
	dfs(1);	
	cout<<ans<<endl;
}

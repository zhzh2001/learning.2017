#pragma GCC optimize 2
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<ctime>
#include<cstring>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}

struct orzwzp{int x,y,z;}	p[500001],np[500001];
inline bool cmp(orzwzp x,orzwzp y){return x.z<y.z;}
inline bool cmp1(orzwzp x,orzwzp y){return x.y!=y.y?x.y<y.y:x.x>y.x;}
int n;
ll ans=0;
int MX=1000000,MY=1000000;
inline void solve(int num,int h)
{
	For(i,1,num)	np[i]=p[i];
	while(np[num].z==h)	num--;
	
	sort(np+1,np+num+1,cmp1);
	int mx=1000000;
	For(i,1,num)
	{
		if(1LL*h*mx*np[i].y>ans)	ans=1LL*h*mx*np[i].y;
		mx=min(mx,np[i].x);
	}
	if(1LL*h*mx*1000000>ans)	ans=1LL*h*mx*1000000;
//	if(num>=2000)	{cout<<ans<<endl;exit(0);}
}
inline void main2()
{
	sort(p+1,p+n+1,cmp);
	solve(0,1);solve(n,1000000);
	cout<<ans<<endl;
}
int main()
{
	freopen("b.in","r",stdin);freopen("b1.out","w",stdout);
	n=read();
	For(i,1,n)
		p[i].x=read(),p[i].y=read(),p[i].z=read();
	
	if(n>5000)	{main2();return 0;}
	sort(p+1,p+n+1,cmp);
	solve(n,1000000);
	For(i,1,n)		solve(i-1,p[i].z);
	cout<<ans<<endl;
}
/*
5
2 5 5
6 9 8
5 3 5
7 1 9
9 8 5

*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100*32,P=(int)1e9+7;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int sm,n,a[N],to[N][2];
ll ans,SJ[N],SK[N],Sm[32][2],cnt[32][2],sum[N][2][2],num[N][2];
inline void A(ll&x,ll y){x+=y;x%=P;}
void clear(){
	sm=ans=0;
	memset(to,-1,n*32*sizeof(to[0]));
	memset(sum,0,n*32*sizeof(sum[0]));
	memset(num,0,n*32*sizeof(num[0]));
	memset(cnt,0,sizeof(cnt));
	memset(Sm,0,sizeof(Sm));
}
ll insert(int x){
	ll res=0,nw=0;
	for(register int t,i=31;i>=0;i--){
		t=(x>>i)&1;
		if(to[nw][t]==-1)to[nw][t]=++sm;
		ll ss,ii=to[nw][t^1];
		if(to[nw][t^1]==-1)goto out;
		ss=1ll*sum[nw][1][t^1]*Sm[i][t^1];
		ss=ss-num[nw][t^1];
		res+=ss;
		out:
		sum[nw][1][t]++;
		Sm[i][t]++;
		num[nw][t]+=Sm[i][t];
		nw=to[nw][t];
	}return res;
}
ll query(int x){
	ll nw=0,res=0;
	for(register int t,i=31;i>=0;i--){
		t=(x>>i)&1;
		res+=cnt[i][t];
		cnt[i][t]+=sum[nw][1][t^1];
		cnt[i][t^1]-=sum[nw][0][t^1];
		--sum[nw][1][t];
		++sum[nw][0][t];
		nw=to[nw][t];
	}return res;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	for(register int o,x,Q,i,T=1;T--;){
		n=read();Q=read();clear();
		n++;SK[1]=insert(a[1]=0);
		for(i=2;i<=n;i++)
		SK[i]=insert(a[i]=read()^a[i-1]);
		for(i=1;i<=n;i++)
		A(ans,SJ[i]=query(a[i]));
		write(ans);putchar('\n');
		for(;Q--;){
			o=read();x=read()+1;
			if(o==2)write(SJ[x]);
			else write(SK[x]);puts("");
		}
		/*
		for(i=1;i<=n;i++)
		printf("%d ",SJ[i]);
		puts("");
		for(i=1;i<=n;i++)
		printf("%d ",SK[i]);
		puts("");
		*/
	}return 0;
}

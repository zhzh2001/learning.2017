#include <cstdio>
#include <cstring>
#include <algorithm>
#define M 340
using namespace std;
inline int read()
{
	int x = 0, c = getchar(), f = 0;
	for (; c > '9' || c < '0'; f = c == '-', c = getchar())
		;
	for (; c >= '0' && c <= '9'; c = getchar())
		x = (x << 1) + (x << 3) + c - '0';
	return f ? -x : x;
}
struct cqz
{
	int a, b, v, w;
} a[M];
inline bool operator<(cqz x, cqz y)
{
	if (x.v >= 0 && y.v < 0)
		return 1;
	if (x.v < 0 && y.v >= 0)
		return 0;
	if (x.v >= 0 && y.v >= 0)
		return x.a < y.a;
	return x.b > y.b;
}
char s[M];
int n, sum, f[M * M];
int main()
{
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	memset(f, -60, sizeof f);
	int i, j;
	f[0] = 0;
	n = read();
	for (i = 1; i <= n; i++)
	{
		int x = 0, y = 0;
		scanf("%s", s);
		for (j = 0; s[j]; j++)
			if (s[j] == '(')
				y++;
			else
				y ? y-- : x++;
		a[i].a = x;
		a[i].b = y;
		a[i].v = y - x;
		a[i].w = strlen(s);
		sum += y;
	}
	sort(a + 1, a + n + 1);
	for (i = 1; i <= n; i++)
		if (a[i].v < 0)
			for (j = a[i].a; j <= sum; j++)
				f[j + a[i].v] = max(f[j + a[i].v], f[j] + a[i].w);
		else
			for (j = sum; j >= a[i].a; j--)
				f[j + a[i].v] = max(f[j + a[i].v], f[j] + a[i].w);
	printf("%d\n", f[0]);
	return 0;
}

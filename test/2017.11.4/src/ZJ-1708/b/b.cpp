#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000
#define zyy 1000000007
#define int ll
#define N 200005
using namespace std;
inline int read(){int x=0,f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) putchar('-'),x=-x;write(x);putchar('\n');}
int n;
struct data{int x,y,z;}a[N];
inline bool cmp1(data a,data b){return a.x<b.x;}
inline bool cmp2(data a,data b){return a.y<b.y;}
inline bool cmp3(data a,data b){return a.z<b.z;}
int x,y,z,xx,yy,zz;
ll ans;
main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();For(i,1,n) a[i]=(data){read(),read(),read()};
	sort(a+1,a+1+n,cmp1);
	y=1000000;z=1000000;
	yy=oo;zz=oo;
	for(int i=1,j=1;i<=1000000;i++){
		int l=j;
		for(;j<=n&&a[j].x<i;j++) y=min(y,a[j].y),z=min(z,a[j].z);
		for(int k=l;k<=j;k++) if(a[k].y==y) zz=min(zz,a[k].z);
		for(int k=l;k<=j;k++) if(a[k].z==z) yy=min(yy,a[k].y);
		ans=max(ans,1ll*i*y*1000000);
		ans=max(ans,1ll*i*z*1000000);
//		if(yy!=oo&&zz!=oo) ans=max(ans,1ll*i*yy*zz);
	}
	sort(a+1,a+1+n,cmp2);
	x=1000000;z=1000000;
	xx=oo;zz=oo;
	for(int i=1,j=1;i<=1000000;i++){
		int l=j;
		for(;j<=n&&a[j].y<i;j++) x=min(x,a[j].x),z=min(z,a[j].z);
		for(int k=l;k<=j;k++) if(a[k].x==x) zz=min(zz,a[k].z);
		for(int k=l;k<=j;k++) if(a[k].z==z) xx=min(xx,a[k].x);
		ans=max(ans,1ll*i*x*1000000);
		ans=max(ans,1ll*i*z*1000000);
//		if(xx!=oo&&zz!=oo) ans=max(ans,1ll*i*xx*zz);
	}
	sort(a+1,a+1+n,cmp3);
	x=1000000;y=1000000;
	xx=oo;yy=oo;
	for(int i=1,j=1;i<=1000000;i++){
		int l=j;
		for(;j<=n&&a[j].z<i;j++) x=min(x,a[j].x),y=min(y,a[j].y);
		for(int k=l;k<=j;k++) if(a[k].x==x) yy=min(yy,a[k].y);
		for(int k=l;k<=j;k++) if(a[k].y==y) xx=min(xx,a[k].x);
		ans=max(ans,1ll*i*x*1000000);
		ans=max(ans,1ll*i*y*1000000);
//		if(xx!=oo&&yy!=oo) ans=max(ans,1ll*i*xx*yy);
	}
	printf("%lld\n",ans);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000000
#define zyy 1000000007
#define N 200005
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
inline void writeln(int x){
	if(x<0) putchar('-'),x=-x;
	write(x);putchar('\n');
}
int n,m,a[N],sum[N],p[N];
ll ans1[N],ans2[N],ans;
inline int find(int x,int n){
	int y=lower_bound(p+1,p+1+n,x)-p;
	return y-1;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();m=read();
	For(i,1,n) a[i]=read();
	For(i,1,n) sum[i]=sum[i-1]^a[i];
	For(i,1,n){
		Forn(j,0,i) p[j+1]=sum[i]^sum[j];
		sort(p+1,p+1+i);
		For(j,i+1,n){
			int x=find(sum[j]^sum[i],i);
			ans1[i]+=x;ans2[j]+=x;ans=(ans+x)%zyy;
		}
	}
	writeln(ans);
	while(m--){
		int opt=read(),x=read();
		if(opt==2) writeln(ans1[x]);
		else writeln(ans2[x]);
	}
	return 0;
}

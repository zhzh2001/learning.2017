#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
#define oo 1000000000
#define N 305
#define M 94005
using namespace std;
int n,ans,f[M],len[N];
char s[N];
struct data{
	int x,mn,len;
	bool operator <(const data &a)const{
		return (x==a.x)?mn>a.mn:x>a.x;
	}
}a[N];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n){
		scanf("%s",s+1);
		int m=strlen(s+1);a[i].len=m;a[i].mn=0;
		For(j,1,m) a[i].x+=((s[j]=='(')?1:-1);/*a[i].mn=min(a[i].mn,a[i].x)*/
	}
	sort(a+1,a+1+n);
	memset(f,-63,sizeof f);f[0]=0;
	For(i,1,n){
		if(a[i].x<0){
			For(j,-a[i].x,90000) f[j+a[i].x]=max(f[j+a[i].x],f[j]+a[i].len);
		}else{
			Rep(j,90000,-a[i].mn) f[j+a[i].x]=max(f[j+a[i].x],f[j]+a[i].len);
		}
	}
	printf("%d\n",f[0]);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

const int maxn = 15;
char s[maxn][maxn];
int a[maxn], vis[maxn], ans, n, top, res;

void solve(int n) {
	top = 0; res = 0;
	for (int i=1; i<=n; i++)
		for (int j=1; s[a[i]][j]; j++) {
			if (s[a[i]][j] == ')') {
				if (!top) return;
				top--; res += 2;
			}
			else top++;
		}
	if (top) return;
	ans = max(ans, res);
}

void dfs(int k) {
	solve(k);
	if (k == n+1) return;
	for (int i=1; i<=n; i++)
		if (!vis[i]) {
			vis[i] = 1; a[k] = i;
			dfs(k+1);
			vis[i] = 0;
		}
}

int main() {
	freopen("a.in", "r", stdin);
	freopen("std.out", "w", stdout);
	scanf("%d", &n);
	for (int i=1; i<=n; i++) scanf("%s", s[i]+1);
	dfs(1);
	printf("%d\n", ans);
	return 0;
}

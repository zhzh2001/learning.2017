#include<cstdio>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (int i=(a); i<=int(b); i++)
using namespace std;

const int maxn = 309, maxm = 15;
struct node {
	int L, R, val, ty;
	bool operator < (const node &a) const {
		if (ty != a.ty) return ty > a.ty;
		if (ty >= 0) return L < a.L;
		return L > a.L;
	}
} a[maxn];
int f[2][maxn*maxn], top, n, cur = 0;
char s[maxm][maxn];
int p[maxm], vis[maxm], ans, res;
char S[maxn];

void update(int &a, int b) {
	if (b > a) a = b;
}

void solve(int n) {
	top = 0; res = 0;
	for (int i=1; i<=n; i++)
		for (int j=1; s[p[i]][j]; j++) {
			if (s[p[i]][j] == ')') {
				if (!top) return;
				top--; res += 2;
			}
			else top++;
		}
	if (top) return;
	ans = max(ans, res);
}

void dfs(int k) {
	solve(k);
	if (k == n+1) return;
	for (int i=1; i<=n; i++)
		if (!vis[i]) {
			vis[i] = 1; p[k] = i;
			dfs(k+1);
			vis[i] = 0;
		}
}

int main() {
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	
	scanf("%d", &n);
	if (n <= 10) {
		for (int i=1; i<=n; i++) scanf("%s", s[i]+1);
		dfs(1);
		printf("%d\n", ans);
		return 0;
	}
	rep (i, 1, n) {
		scanf("%s", S+1); top = 0;
		for (int j=1; S[j]; j++)
			if (S[j] == ')') {
				if (!top) a[i].L++;
				else { top--; a[i].val++; }
			}
			else top++;
		a[i].R = top;
		if (a[i].L < a[i].R) a[i].ty = 1;
		if (a[i].L == a[i].R) a[i].ty = 0;
		if (a[i].L > a[i].R) a[i].ty = -1;
		//printf("%d %d %d %d\n", a[i].L, a[i].R, a[i].val, a[i].ty);
	}
	sort(a+1, a+n+1);
	
	memset(f[cur], 0xc0, sizeof f[cur]); f[cur][0] = 0;
	rep (i, 1, n) {
		cur = i&1;
		rep (j, 0, n*300) f[cur][j] = f[cur^1][j];
		rep (j, a[i].L, i*300) if (f[cur^1][j] >= 0)
			update(f[cur][j-a[i].L+a[i].R], f[cur^1][j] + a[i].val + a[i].L);
	}
	printf("%d\n", f[n&1][0]*2);
	
	return 0;
}

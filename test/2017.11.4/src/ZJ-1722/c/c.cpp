#include<cstdio>
#include<cstring>
#include<algorithm>
#include<map>
#define rep(i,a,b) for (int i=(a); i<=int(b); i++)
#define _rep(i,a,b) for (int i=(a); i>=int(b); i--)
using namespace std;

inline int read() {
	char ch = getchar(); int x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }
	return op * x;
}

const int maxn = 3009, maxm = 32;
int t[maxn*maxm][maxm], tot[maxn*maxm];
int sum[maxn], ans2[maxn], ans3[maxn];
int n, Q, x, y, size = 1, res;
long long ans;

void insert(int x) {
	int u = 1;
	_rep (j, 29, 0) {
		if (!t[u][x>>j&1]) t[u][x>>j&1] = ++size;
		u = t[u][x>>j&1]; tot[u]++;
	}
}

int query(int tmp, int x) {
	int u = 1, res = 0;
	_rep (j, 29, 0) { 
		if (!u) break;
		if (x>>j&1) { 
			res += tot[t[u][tmp>>j&1]]; 
			u = t[u][(tmp>>j&1)^1];
		}
		else u = t[u][tmp>>j&1];
	}
	return res;
}

int main() {
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	n = read(); Q = read();
	rep (i, 1, n) { x = read(); sum[i] = sum[i-1] ^ x; }
	insert(0);
	rep (i, 1, n) {
		rep (j, i+1, n) {
			res = query(sum[i], sum[j] ^ sum[i]);
			ans2[i] += res; ans3[j] += res;
		}
		ans += ans2[i];
		insert(sum[i]);
	}
	printf("%lld\n", ans);
	while (Q--) {
		x = read(); y = read();
		if (x == 2) printf("%d\n", ans2[y]);
		else printf("%d\n", ans3[y]);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#define rep(i,a,b) for (int i=(a); i<=int(b); i++)
#define X first
#define Y second
using namespace std;
typedef pair<int,int> P;
typedef long long LL;

inline int read() {
	char ch = getchar(); int x = 0, op = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10 + ch-'0'; ch = getchar(); }
	return op * x;
}

const int maxn = 200009;
struct point {
	int x, y, z;
	bool operator < (const point &p) const {
		return z < p.z;
	}
} p[maxn];
int n, last; set<P> s; multiset<LL> area;
set<P>::iterator it, tmp; LL ans; 

int main() {
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);	
	n = read();
	rep (i, 1, n) {
		p[i].x = read(); p[i].y = read(); p[i].z = read();
	}
	sort(p+1, p+n+1);
	s.insert(make_pair(0, 1000000));
	s.insert(make_pair(1000000, 0));
	area.insert(1000000LL * 1000000LL);
	rep (i, 1, n) {
		if (i < n && p[i].z != p[i-1].z)
			ans = max(ans, 1LL * p[i].z * *(--area.end()));
		it = s.upper_bound(make_pair(p[i].x, p[i].y));
		if ((--it)->Y <= p[i].y) continue; 
		area.insert(1LL * it->Y * p[i].x);
		last = (it++)->Y;
		while (it->Y >= p[i].y) {
			area.erase(area.lower_bound(1LL * last * it->X));
			last = it->Y;
			tmp = it++; s.erase(tmp);
		}
		area.erase(area.lower_bound(1LL * last * it->X));
		area.insert(1LL * it->X * p[i].y);
		s.insert(make_pair(p[i].x, p[i].y));
	}
	ans = max(ans, 1000000LL * *(--area.end()));
	printf("%lld\n", ans);
	return 0;
}

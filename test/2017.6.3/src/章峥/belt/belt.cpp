#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("belt.in");
ofstream fout("belt.out");
const int N=1005;
int s[N][N],v[N][N],f[N][N],g[N][N],q[N];
int main()
{
	int n,m,a,b,c,d;
	fin>>n>>m>>a>>b>>c>>d;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>s[i][j];
			s[i][j]+=s[i-1][j]+s[i][j-1]-s[i-1][j-1];
		}
	for(int i=c;i<=n;i++)
		for(int j=d;j<=m;j++)
			v[i][j]=s[i][j]-s[i-c][j]-s[i][j-d]+s[i-c][j-d];
	for(int i=c;i<=n;i++)
	{
		int h=1,t=0;
		for(int j=d;j<=m;j++)
		{
			while(h<=t&&j-q[h]>b-d-2)
				h++;
			while(h<=t&&v[i][j]<v[i][q[t]])
				t--;
			q[++t]=j;
			f[i][j]=v[i][q[h]];
		}
	}
	for(int j=d;j<=m;j++)
	{
		int h=1,t=0;
		for(int i=c;i<=n;i++)
		{
			while(h<=t&&i-q[h]>a-c-2)
				h++;
			while(h<=t&&f[i][j]<f[q[t]][j])
				t--;
			q[++t]=i;
			g[i][j]=f[q[h]][j];
		}
	}
	int ans=0;
	for(int i=a;i<=n;i++)
		for(int j=b;j<=m;j++)
			ans=max(ans,s[i][j]-s[i-a][j]-s[i][j-b]+s[i-a][j-b]-g[i-1][j-1]);
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("caution.in");
ofstream fout("caution.out");
const int N=100005;
int a[N];
long long f[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	f[1]=a[1];f[2]=a[2];
	for(int i=3;i<=n;i++)
		f[i]=min(f[i-1]+a[i]+a[1],f[i-2]+a[1]+a[2]+a[2]+a[i]);
	fout<<f[n]<<endl;
	return 0;
}
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mo 998244353
#define phimo 402653184
using namespace std;
ll x,y,n,m;
ll pow(ll x,ll y,ll p){
	ll s=1;
	for (;y;y/=2,x=x*x%p)
		if (y&1) s=s*x%p;
	return s;
}
int main(){
	freopen("tttsss.in","r",stdin);
	freopen("tttsss.out","w",stdout);
	scanf("%lld%lld",&x,&y);
	n=(pow(10,x,mo)*7-1)%mo*pow(3,mo-2,mo)%mo;
	m=(pow(10,y,mo-1)*2-2)%(mo-1)*pow(3,phimo-1,mo-1)%(mo-1);
	printf("%lld",pow(n,m,mo));
}

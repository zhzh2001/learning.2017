#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define fo(i,j,k) for (int i=j;i<=k;i++)
#define N 1005
using namespace std;
int n,m,A,B,C,D,h,t,val,ans;
int a[N][N],b[N][N],c[N][N],d[N][N],q[N]; 
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x; 
}
int main(){
	freopen("belt.in","r",stdin);
	freopen("belt.out","w",stdout);
	n=read(); m=read();
	A=read(); B=read();
	C=read(); D=read();
	fo(i,1,n) fo(j,1,m)
		a[i][j]=read();
	fo(i,1,n) fo(j,1,m)
		a[i][j]+=a[i-1][j];
	fo(i,1,n) fo(j,1,m)
		a[i][j]+=a[i][j-1];
	fo(i,C,n) fo(j,D,m)
		b[i][j]=a[i][j]-a[i-C][j]-a[i][j-D]+a[i-C][j-D];
	fo(i,1,n){
		h=t=0;
		fo(j,1,m){
			while (h!=t&&j-q[h+1]>B-D-2) h++;
			while (h!=t&&b[i][q[t]]>b[i][j]) t--;
			q[++t]=j;
			c[i][j]=b[i][q[h+1]];
		}
	}
	fo(j,1,m){
		h=t=0;
		fo(i,1,n){
			while (h!=t&&i-q[h+1]>A-C-2) h++;
			while (h!=t&&c[q[t]][j]>c[i][j]) t--;
			q[++t]=i;
			d[i][j]=c[q[h+1]][j];
		}
	}
	fo(i,A,n) fo(j,B,m){
		val=a[i][j]-a[i-A][j]-a[i][j-B]+a[i-A][j-B];
		ans=max(ans,val-d[i-1][j-1]);
	}
	printf("%d\n",ans);
}

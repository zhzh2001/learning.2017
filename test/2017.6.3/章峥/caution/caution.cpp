#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("caution.in");
ofstream fout("caution.out");
const int N=100005;
int a[N];
int main()
{
	int n;
	fin>>n;
	long long sum=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		sum+=a[i];
	}
	if(n==1)
	{
		fout<<a[1]<<endl;
		return 0;
	}
	sort(a+1,a+n+1);
	long long ans=0;
	for(int i=n;i>3;i-=2)
		ans+=a[i];
	if(n&1)
		fout<<min(sum+(long long)(n-3)*a[1],ans+(long long)(a[1]+a[2]+a[2])*(n-3)/2+a[1]+a[2]+a[3])<<endl;
	else
		fout<<min(sum+(long long)(n-3)*a[1],ans+(long long)(a[1]+a[2]+a[2])*(n-2)/2+a[2])<<endl;
	return 0;
}
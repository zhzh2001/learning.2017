#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("tttsss.in");
ofstream fout("tttsss.out");
int mod;
struct matrix
{
	static const int n=2;
	int mat[n][n];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				for(int k=0;k<n;k++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j])%mod;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix I()
{
	matrix ret;
	for(int i=0;i<matrix::n;i++)
		ret.mat[i][i]=1;
	return ret;
}
matrix qpow(matrix a,long long b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%mod;
		a=a*a%mod;
	}
	while(b/=2);
	return ans;
}
int main()
{
	long long x,y;
	fin>>x>>y;
	mod=998244353;
	matrix trans;
	trans.mat[0][0]=10;trans.mat[0][1]=3;trans.mat[1][1]=1;
	trans=qpow(trans,x);
	matrix init;
	init.mat[0][0]=2;init.mat[1][0]=1;
	int n=(trans*init).mat[0][0];
	mod--;
	trans.mat[0][0]=10;trans.mat[0][1]=6;trans.mat[1][1]=1;
	trans=qpow(trans,y);
	init.mat[0][0]=0;
	int m=(trans*init).mat[0][0];
	mod++;
	fout<<qpow(n,m)<<endl;
	return 0;
}
#pragma GCC optimize 2
#include<fstream>
using namespace std;
ifstream fin("tri.in");
ofstream fout("tri.out");
const int X=6005,BX=380;
int vis1[X*2][BX],vis2[X*2][BX];
int main()
{
	int n;
	fin>>n;
	int ans=0;
	while(n--)
	{
		int x,y,m;
		fin>>x>>y>>m;
		for(int i=m;i;i--)
		{
			int nx=X+x+m-i;
			for(int j=0;j<i-1;j++)
			{
				int ny=X+y+j;
				if(!(vis1[nx][ny/32]&(1<<(ny&31))))
				{
					vis1[nx][ny/32]|=1<<(ny&31);
					ans++;
				}
				if(!(vis2[nx][ny/32]&(1<<(ny&31))))
				{
					vis2[nx][ny/32]|=1<<(ny&31);
					ans++;
				}
			}
			int ny=X+y+i-1;
			if(!(vis1[nx][ny/32]&(1<<(ny&31))))
			{
				vis1[nx][ny/32]|=1<<(ny&31);
				ans++;
			}
		}
	}
	fout.precision(1);
	fout<<fixed<<ans/2.0<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
ofstream fout("tri.in");
const int n=100,m=1000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(-5000,5000);
		fout<<d(gen)<<' '<<d(gen)<<' '<<m<<endl;
	}
	return 0;
}
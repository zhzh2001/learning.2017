#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("belt.in");
ofstream fout("belt.out");
const int N=1005;
int s[N][N];
int main()
{
	int n,m,a,b,c,d;
	fin>>n>>m>>a>>b>>c>>d;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>s[i][j];
			s[i][j]+=s[i-1][j]+s[i][j-1]-s[i-1][j-1];
		}
	int ans=0;
	for(int i=1;i+a-1<=n;i++)
		for(int j=1;j+b-1<=m;j++)
		{
			int sum=s[i+a-1][j+b-1]-s[i-1][j+b-1]-s[i+a-1][j-1]+s[i-1][j-1];
			for(int k=i+1;k+c-1<i+a-1;k++)
				for(int l=j+1;l+d-1<j+b-1;l++)
					ans=max(ans,sum-(s[k+c-1][l+d-1]-s[k-1][l+d-1]-s[k+c-1][l-1]+s[k-1][l-1]));
		}
	fout<<ans<<endl;
	return 0;
}
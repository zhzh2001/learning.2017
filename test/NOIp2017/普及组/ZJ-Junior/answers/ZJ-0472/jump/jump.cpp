#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<cmath>
#define LL long long
using namespace std;
int n,d,k,p[500005],s[500005],L,R,mid,ans=-1,len,lon;
LL f[500005],INF=-((LL)1<<60),opt[500005][25];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
inline LL get(int L,int R){if(L>R) return INF;int loRL=log2(R-L+1);return max(opt[L][loRL],opt[R-(1<<loRL)+1][loRL]);}
inline bool check(int x){
	memset(f,0,sizeof f);
	int lo=max(1,d-x),ro=d+x,nL=0,nR=-1;
	for(int i=1;i<=n;i++){
		while(p[i]-p[nL]>ro) nL++;
		while(p[i]-p[nR+1]>=lo) nR++;
		LL xx=get(nL,nR);
		f[i]=xx+s[i];
		if(f[i]>=k) return 1;
		opt[i][0]=f[i];
		for(int j=1,ii;j<=lon;j++) ii=i+1-(1<<j),opt[ii][j]=max(opt[ii][j-1],opt[ii+(1<<j-1)][j-1]);
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();k=read();lon=log2(n);
	for(int i=1;i<=n;i++) p[i]=read(),s[i]=read();
	R=p[n]-d;if(R<0) R=0;
	while(L<=R){
		mid=L+R>>1;
		if(check(mid)) R=mid-1,ans=mid;else L=mid+1;
	}
	printf("%d",ans);
	return 0;
}

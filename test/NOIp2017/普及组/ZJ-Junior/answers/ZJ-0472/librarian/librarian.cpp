#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
int n,q,a[10000005],b[15],ten[10];
inline int read(){
	int ret=0;char ch=getchar();
	while(ch>'9'||ch<'0') ch=getchar();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
inline void work(int x){
	b[0]=0;int y=x,now=0;
	while(x){
		b[++b[0]]=x%10,x/=10;
		now=now+b[b[0]]*ten[b[0]-1];
		if(!a[now]||y<a[now]) a[now]=y;
	}
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	ten[0]=1;for(int i=1;i<=8;i++) ten[i]=ten[i-1]*10;
	n=read();q=read();
	for(int i=1,x;i<=n;i++) x=read(),work(x);
	for(int i=1,x;i<=q;i++) read(),x=read(),printf("%d\n",!a[x]?-1:a[x]);
	return 0;
}

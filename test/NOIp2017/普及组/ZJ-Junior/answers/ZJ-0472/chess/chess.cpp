#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int flg[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,c[105][105],dis[105][105][2];
bool vis[105][105][2];
struct yu{
	int x,y,c;
}que[10005];
inline int read(){
	int ret=0;char ch=getchar();
	while(ch>'9'||ch<'0') ch=getchar();
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
inline void spfa(){
	memset(dis,63,sizeof dis);int INF=dis[0][0][0];
	dis[1][1][c[1][1]]=0;vis[1][1][c[1][1]]=1;que[1]=(yu){1,1,c[1][1]};
	int hed=0,tal=1;
	while(hed^tal){
		int x=que[++hed%=10005].x,y=que[hed].y,C=que[hed].c;
		for(int i=0;i^4;i++){
			int xx=x+flg[i][0],yy=y+flg[i][1],cc=c[xx][yy],zz=0;
			if(xx<1||xx>n||yy<1||yy>n||(c[x][y]==-1&&cc==-1)) continue;
			if(cc==-1) for(cc=0;cc^2;cc++){
				zz=2+(C!=cc);
				if(dis[x][y][C]+zz<dis[xx][yy][cc]){
					dis[xx][yy][cc]=dis[x][y][C]+zz;
					if(!vis[xx][yy][cc]) que[++tal]=(yu){xx,yy,cc},vis[xx][yy][cc]=1;
				}
			}
			else{
				zz=C!=cc;
				if(dis[x][y][C]+zz<dis[xx][yy][cc]){
					dis[xx][yy][cc]=dis[x][y][C]+zz;
					if(!vis[xx][yy][cc]) que[++tal]=(yu){xx,yy,cc},vis[xx][yy][cc]=1;
				}
			}
		}
	}
	int ans=min(dis[n][n][0],dis[n][n][1]);
	if(ans^INF) printf("%d",ans);else printf("-1");
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	memset(c,-1,sizeof c);
	for(int i=1,x,y,z;i<=m;i++) x=read(),y=read(),z=read(),c[x][y]=z;
	spfa();
	return 0;
}

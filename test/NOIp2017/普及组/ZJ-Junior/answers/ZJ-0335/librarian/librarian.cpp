#include <cstdio>
#include <iostream>
using namespace std;

inline int h(int a){
	int o=1;
	for(int i=0;i<a;i++){
		o*=10;
	}
	return o;
}

int main(){
	int a,l,b[1010],n,m,p;
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin >> n >> m;
	for(int i=0;i<n;i++){
		cin >> b[i];
	}
	for(int i=0;i<n;i++){
		cin >> l >> a;
		p=99999999;
		for(int j=0;j<n;j++){
			if(b[j]<p&&b[j]%(h(l))==a){
				p=b[j];
			}
		}
		if(p==99999999){
			cout << -1 <<endl;
		}
		else{
			cout << p <<endl;
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

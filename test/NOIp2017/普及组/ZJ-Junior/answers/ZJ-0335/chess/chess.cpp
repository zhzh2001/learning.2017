#include <cstdio>
#include <iostream>
using namespace std;

int p=99999999,n,m,b[110][110],c[110][110]={0},step=0;
int way[4][2]={{1,0},{0,1},{-1,0},{0,-1}};

void bfs(int x,int y,int q,bool s){
	step++;
	if(step>35000000){
		return;
	}
	if(x==m&&y==m){
		if(q<p){
			p=q;
		}
		return;
	}
	int mx,my;
	for(int i=0;i<4;i++){
		mx=x+way[i][0];
		my=y+way[i][1];
		if(mx<1||mx>m||my<1||my>m||c[mx][my]==1){
			continue;
		}
		if(b[mx][my]==2){
			if(s){
				c[mx][my]=1;
				b[mx][my]=b[x][y];
				bfs(mx,my,q+2,0);
				b[mx][my]=2;
				c[mx][my]=0;
			}
		}
		else{
			if(b[x][y]!=b[mx][my]){
				c[mx][my]=1;
				bfs(mx,my,q+1,1);
				c[mx][my]=0;
			}
			else{
				c[mx][my]=1;
				bfs(mx,my,q,1);
				c[mx][my]=0;
			}
		}
	}
	return;
}

int main(){
	int x,y;
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin >> m >> n;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=m;j++){
			b[i][j]=2;
		}
	}
	for(int i=0;i<n;i++){
		cin >> x >> y;
		cin >> b[x][y];
	}
	bfs(1,1,0,1);
	if(p!=99999999){
		cout << p;
	}
	else{
		cout << -1;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

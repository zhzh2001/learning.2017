var temp:string;
    flag:boolean;
    n,q,i,j:longint;
    a:array [-1..1002] of string;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to n-1 do
    for j:=i+1 to n do
      if (length(a[i])>length(a[j])) or ((length(a[i])=length(a[j])) and (a[i]>a[j])) then
      begin
        temp:=a[i];
        a[i]:=a[j];
        a[j]:=temp;
      end;
  for i:=1 to q do
  begin
    readln(temp);
    delete(temp,1,pos(' ',temp));
    flag:=true;
    for j:=1 to n do
      if (pos(temp,a[j])>0) and (pos(temp,a[j])=length(a[j])-length(temp)+1) then
      begin
        flag:=false;
        writeln(a[j]);
        break
      end;
      if flag then
        writeln(-1);
  end;
  close(input);
  close(output);
end.
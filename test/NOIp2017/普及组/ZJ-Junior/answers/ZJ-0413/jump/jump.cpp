#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 500005

using namespace std;

int n,d,k,ans=-987456,len;

class Houses
{
public:
	int x,w;
	bool operator <(const Houses b)const
	{
		return b.w>w;
	}
	bool operator >(const Houses b)const
	{
		return b.w<w;
	}
}a[maxn],hep[maxn],F[maxn];

inline int read()
{
	char ch=getchar();
	int ret=0,f=1;
	while(ch>'9'||ch<'0')
	{
		if(ch=='-')
			f=-f;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
		ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}

void put(Houses x)
{
	hep[++len]=x;
	int son=len;
	while(son>1&&hep[son]>hep[son>>1])
	{
		swap(hep[son],hep[son>>1]);
		son>>=1;
	}
}

Houses get()
{
	Houses now=hep[1];
	hep[1]=hep[len--];
	int fa=1,son;
	while(fa*2<=len)
	{
		if(hep[fa*2]>hep[fa*2+1]||fa*2+1>len)
			son=fa*2;
		else
			son=fa*2+1;
		if(hep[fa]>hep[son])
			break;
		swap(hep[fa],hep[son]);
		fa=son;
	}
	return now;
}

bool check(int l,int r)
{
	len=0;
	hep[1].w=0;
	int j=1;
	for(int i=2;i<=n;++i)
	{
		F[i].w=-1000000000;
		int nl=a[i].x-r,nr=a[i].x-l;
		for(;j<i;++j)
		{
			if(F[j].x>nr)
				break;
			put(F[j]);
		}
		while(len&&(hep[1].x<nl||hep[1].x>nr))
			get();
		if(!len)
			continue;
		F[i].w=a[i].w+hep[1].w;
		if(F[i].w>=k)
			return 1;
	}
	return 0;
}

void Find_s()
{
	int l=0,r=d-1;
	while(l<=r)
	{
		int mid=(r-l>>1)+l;
		if(check(d-mid,d+mid))
		{
			ans=mid;
			r=mid-1;
		}
		else
			l=mid+1;
	}
}

void Find_b()
{
	int l=d,r=1e9;
	while(l<=r)
	{
		int mid=(r-l>>1)+l;
		if(check(1,d+mid))
		{
			ans=mid;
			r=mid-1;
		}
		else
			l=mid+1;
	}
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read()+1;d=read(),k=read();
	a[1].w=0;a[1].x=0;
	int sum=0;
	for(int i=2;i<=n;++i)
	{
		F[i].x=a[i].x=read();
		a[i].w=read();
		if(a[i].w>0)
			sum+=a[i].w;
	}
	if(sum<k)
	{
		printf("-1");
		return 0;
	}
	Find_s();
	if(ans==-987456)
		Find_b();
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 105

using namespace std;

const int p[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int a[maxn][maxn],b[maxn*maxn],m,n;
int tot,lnk[maxn*maxn],nxt[maxn*maxn*4],son[maxn*maxn*4],w[maxn*maxn*4];
int que[maxn*maxn*maxn],dst[maxn*maxn],INF;
bool vis[maxn*maxn];

inline int read()
{
	char ch=getchar();
	int ret=0,f=1;
	while(ch>'9'||ch<'0')
	{
		if(ch=='-')
			f=-f;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
		ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}

void add_e(int x,int y,int z)
{
	son[++tot]=y;
	w[tot]=z;
	nxt[tot]=lnk[x];
	lnk[x]=tot;
}

void SPFA()
{
	memset(dst,63,sizeof(dst));
	INF=dst[0];
	que[1]=1;dst[1]=0;
	int hed=0,til=1;
	while(hed^til)
	{
		hed++;
		vis[que[hed]]=0;
		for(int j=lnk[que[hed]];j;j=nxt[j])
		{
			if(dst[que[hed]]+w[j]<dst[son[j]])
			{
				dst[son[j]]=dst[que[hed]]+w[j];
				if(!vis[son[j]])
				{
					que[++til]=son[j];
					vis[son[j]]=1;
					if(dst[que[hed+1]]>dst[que[til]])
					{
						swap(que[hed+1],que[til]);
					}
				}
			}
		}
	}
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;++i)
	{
		int x=read(),y=read();
		a[x][y]=read()+1;
		b[(x-1)*n+y]=a[x][y];
	}
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=n;++j)
		{
			if(!a[i][j])
				continue;
			for(int l=0;l<4;++l)
			{
				int x=i+p[l][0],y=j+p[l][1];
				if(a[x][y]==0)
				{
					for(int ii=0;ii<4;++ii)
					{
						int xx=x+p[ii][0],yy=y+p[ii][1];
						if((xx==i&&yy==j)||(a[xx][yy]==0))
							continue;
						if(xx<1||xx>n||yy<1||yy>n)
							continue;
						int w=2;
						if(a[i][j]!=a[xx][yy])
							w=3;
						add_e((i-1)*n+j,(xx-1)*n+yy,w);
					}
					continue;
				}
				if(x<1||x>n||y<1||y>n)
					continue;
				int w=0;
				if(a[i][j]!=a[x][y])
					w=1;
				add_e((i-1)*n+j,(x-1)*n+y,w);
			}
		}
	}
	SPFA();
	if(dst[n*n]==INF)
	{
		printf("-1");
		return 0;
	}
	printf("%d\n",dst[n*n]);
	return 0;
}

//���ǲ��� time pipe y2 
#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 1005

using namespace std;

int n,q,a[maxn];

inline int read()
{
	char ch=getchar();
	int ret=0,f=1;
	while(ch>'9'||ch<'0')
	{
		if(ch=='-')
			f=-f;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
		ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}

bool has(int x,int y)
{
	if(x<y)return 0;
	if(x==y)return 1;
	while(y)
	{
		if((y%10)!=(x%10))
			return 0;
		x/=10;
		y/=10;
	}
	return 1;
}

int check(int&x)
{
	for(int i=1;i<=n;++i)
	{
		if(has(a[i],x))
		{
			return a[i];
		}
	}
	return -1;
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),q=read();
	for(int i=1;i<=n;++i)
		a[i]=read();
	sort(a+1,a+1+n);
	while(q--)
	{
		read();
		int x=read();
		printf("%d\n",check(x));
	}
	return 0;
}

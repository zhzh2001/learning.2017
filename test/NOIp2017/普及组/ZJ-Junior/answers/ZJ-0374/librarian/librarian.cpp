#include <bits/stdc++.h>
using namespace std;
const int maxn = 1010;
int n, q, l, t, a[maxn], f[10];
bool mark;
int main() {
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	f[0] = 1;
	for (int i = 1; i <= 8; i++)
		f[i] = f[i - 1] * 10;
	scanf("%d%d", &n, &q);
	for (int i = 1; i <= n; i++) scanf("%d", &a[i]);
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= q; i++) {
		scanf("%d%d", &l, &t);
		mark = false;
		for (int j = 1; j <= n; j++)
			if (t == (a[j] % f[l])) {
				printf("%d\n", a[j]);
				mark = true;
				break;
			}
		if (!mark) printf("%d\n", -1);
	}
	return 0;
}

const map:array[1..4,1..2]of longint=((1,0),(0,1),(-1,0),(0,-1));
      map2:array[1..8,1..2]of longint=((1,1),(-1,1),(1,-1),(-1,-1),(0,2),(2,0),(-2,0),(0,-2));
var n,m,x,y,z,ans,i:longint;
    dis:array[0..105,0..105]of boolean;
    f:array[0..105,0..105]of longint;
 procedure dfs(x,y,tot:longint);
 var i:longint;
  begin
   if (x=n)and(y=n) then begin if tot<ans then ans:=tot;end else
    begin
     for i:=1 to 4 do
      if (x+map[i,1]>=1)and(x+map[i,1]<=n)and(y+map[i,2]>=1)and(y+map[i,2]<=n)and(dis[x+map[i,1],y+map[i,2]])then
        if (f[x][y]>0)and(f[x+map[i,1]][y+map[i,2]]>0) then
         begin
          dis[x+map[i,1]][y+map[i,2]]:=false;
          dfs(x+map[i,1],y+map[i,2],tot+abs(f[x,y]-f[x+map[i,1],y+map[i,2]]));
          dis[x+map[i,1]][y+map[i,2]]:=true;
         end;
     for i:=1 to 8 do
      if (x+map2[i,1]>=1)and(x+map2[i,1]<=n)and(y+map2[i,2]>=1)and(y+map2[i,2]<=n)and(dis[x+map2[i,1]][y+map2[i,2]])and(f[x+map2[i,1],y+map2[i,2]]>0) then
        if (f[x][y]>0)and(f[x+map2[i,1],y+map2[i,2]]>0) then
         begin
          dis[x+map2[i,1],y+map2[i,2]]:=false;
          dfs(x+map2[i,1],y+map2[i,2],tot+abs(f[x,y]-f[x+map2[i,1],y+map2[i,2]])+2);
          dis[x+map2[i,1],y+map2[i,2]]:=true;
         end;
     end;
   end;
 begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  fillchar(f,sizeof(f),0);
  fillchar(dis,sizeof(dis),true);
  for i:=1 to m do begin readln(x,y,z);f[x,y]:=z+1;end;
  dis[1,1]:=false;ans:=maxlongint;
  dfs(1,1,0);
  if ans<>maxlongint then writeln(ans) else writeln(-1);
  close(input);close(output);
 end.

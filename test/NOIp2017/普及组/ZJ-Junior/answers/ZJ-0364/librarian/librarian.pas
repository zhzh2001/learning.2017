var
        n,m,i,j:longint;
        ch,t:char;
        flag:string;
        a,c:array[-5..1005]of string;
        b:array[-5..1005]of longint;
begin
        assign(input,'librarian.in');
        reset(input);
        assign(output,'librarian.out');
        rewrite(output);
        readln(n,m);
        for i:=1 to n do
        begin
                readln(a[i]);
        end;
        for i:=1 to m do
        begin
                readln(ch,t,c[i]);
                b[i]:=ord(ch)-48;
        end;
        for i:=1 to m do
        begin
                flag:='999999999';
                for j:=1 to n do
                begin
                        if length(a[j])>=b[i] then
                        begin
                                if copy(a[j],length(a[j])-b[i]+1,b[i])=c[i] then
                                begin
                                        if (length(a[j])<length(flag)) or
                                           (length(a[j])=length(flag)) and (a[j]<flag) then flag:=a[j];
                                end;
                        end;
                end;
                if flag='999999999' then writeln('-1')
                else writeln(flag);
        end;
        close(input);
        close(output);
end.
var
        f,a,b:array[-5..500005]of longint;
        n,m,i,k,mx,l,r,mid:longint;
function max(x,y:longint):longint;
begin
        if x>y then exit(x)
        else exit(y);
end;
function ok(t:longint):boolean;
var
        i,j:longint;
begin
        f[0]:=0;
        for i:=1 to n do
        begin
                f[i]:=-1000000001;
                for j:=i-1 downto 0 do
                begin
                        if (a[i]-a[j]>t+k) or (a[i]-a[j]<max(0,k-t))then break;
                        if f[j]+b[i]>f[i] then f[i]:=f[j]+b[i];
                end;
                if f[i]>=m then exit(true);
        end;
        exit(false);
end;
begin
        assign(input,'jump.in');
        reset(input);
        assign(output,'jump.out');
        rewrite(output);
        readln(n,k,m);
        a[0]:=0;
        for i:=1 to n do
        begin
                readln(a[i],b[i]);
        end;
        l:=0;
        r:=a[n];
        while l<r do
        begin
                mid:=(l+r)div 2;
                if ok(mid) then r:=mid
                else l:=mid+1;
        end;
        if ok(r) then writeln(r)
        else writeln('-1');
        close(input);
        close(output);
end.

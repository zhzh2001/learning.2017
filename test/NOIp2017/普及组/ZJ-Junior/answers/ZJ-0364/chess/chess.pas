var
        f:array[-5..105,-5..105]of 0..3;
        g:array[-5..105,-5..105]of longint;
        a,b,c,d,e:array[-5..1000005]of longint;
        dx:array[1..4]of -1..1=(1,-1,0,0);
        dy:array[1..4]of -1..1=(0,0,1,-1);
        i,j,n,m,x,y,z:longint;
procedure bfs;
var
        k,head,tail,xx,yy:longint;
begin
        a[1]:=1;
        b[1]:=1;
        c[1]:=0;
        d[1]:=f[1,1];
        e[1]:=0;
        head:=0;
        tail:=1;
        while head<tail do
        begin
                inc(head);
                for k:=1 to 4 do
                begin
                        xx:=a[head]+dx[k];
                        yy:=b[head]+dy[k];
                        if (xx>=1) and (xx<=n) and (yy>=1) and (yy<=n) then
                        begin
                                if (f[xx,yy]=0) and (e[head]=0) and (c[head]+2<g[xx,yy]) then
                                begin
                                        inc(tail);
                                        g[xx,yy]:=c[head]+2;
                                        a[tail]:=xx;
                                        b[tail]:=yy;
                                        c[tail]:=c[head]+2;
                                        d[tail]:=d[head];
                                        e[tail]:=1;
                                        continue;
                                end;
                                if (f[xx,yy]=d[head]) and (c[head]<g[xx,yy]) then
                                begin
                                        inc(tail);
                                        g[xx,yy]:=c[head];
                                        a[tail]:=xx;
                                        b[tail]:=yy;
                                        c[tail]:=c[head];
                                        d[tail]:=f[xx,yy];
                                        continue;
                                end;
                                if ((d[head]=1) or (d[head]=2)) and (f[xx,yy]<>0) and (f[xx,yy]<>d[head]) and (c[head]+1<g[xx,yy]) then
                                begin
                                        inc(tail);
                                        g[xx,yy]:=c[head]+1;
                                        a[tail]:=xx;
                                        b[tail]:=yy;
                                        c[tail]:=c[head]+1;
                                        d[tail]:=f[xx,yy];
                                        continue;
                                end;
                        end;
                end;
        end;
end;
begin
        assign(input,'chess.in');
        reset(input);
        assign(output,'chess.out');
        rewrite(output);
        readln(n,m);
        for i:=1 to n do
        begin
                for j:=1 to n do
                begin
                        g[i,j]:=10000;
                end;
        end;
        g[1,1]:=0;
        for i:=1 to m do
        begin
                readln(x,y,z);
                if z=0 then f[x,y]:=1;
                if z=1 then f[x,y]:=2;
        end;
        bfs;
        if g[n,n]=10000 then writeln('-1')
        else writeln(g[n,n]);
        close(input);
        close(output);
end.

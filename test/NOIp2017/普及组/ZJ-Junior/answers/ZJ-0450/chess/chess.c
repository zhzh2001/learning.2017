#include<stdio.h>
#include<stdlib.h>
#include<math.h>
int m,n,x,y,c,col[102][102]={-1},i,j,k,cost[102][102]={99999999},min,f,t,paint[102][102],s,r,flag,flag1,book[102][102]={0};
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d %d",&m,&n);
	for(i=1;i<=n;i++){
		scanf("%d %d %d",&x,&y,&c);
		col[x][y]=c;
	}
	cost[1][1]=0;
	for(k=1;k<=m*m;k++){
	for(i=1;i<=m;i++)
		for(j=1;j<=m;j++)
			paint[i][j]=-1;
		flag1=0;
	for(i=1;i<=m;i++){
		for(j=1;j<=m;j++){
			if(i==1 && j==1) continue;
			if(book[i][j]) continue;
			min=99999999;
			flag=0;
			if(col[i][j]==-1){
				if(col[i-1][j]!=-1 && i-1>0 && cost[i-1][j]!=99999999){
					f=cost[i-1][j]+2;
					if(f<min) {min=f; s=i-1; r=j;}
					flag=1;
				}
				if(col[i][j-1]!=-1 && j-1>0 && cost[i][j-1]!=99999999){
					f=cost[i][j-1]+2;
					if(f<min) {min=f; s=i; r=j-1;}
					flag=1;
				}
				if(col[i+1][j]!=-1 && i+1<=m && cost[i+1][j]!=99999999){
					f=cost[i+1][j]+2;
					if(f<min) {min=f; s=i+1; r=j;}
					flag=1;
				}
				if(col[i][j+1]!=-1 && j+1<=m && cost[i][j+1]!=99999999){
					f=cost[i][j+1]+2;
					if(f<min) {min=f; s=i; r=j+1;}
					flag=1;
				}
				if(flag){
					cost[i][j]=min;
					paint[i][j]=col[s][r];
					flag1=1;
				}
				else book[i][j]==1;
			}
			else{
				if(col[i-1][j]!=-1 && i-1>0){
					if(cost[i-1][j]!=99999999){
					if(col[i][j]!=col[i-1][j]) f=cost[i-1][j]+1;
					else f=cost[i-1][j];
					if(f<min) min=f;
					flag=1;
					}
				}
				else
					if(paint[i-1][j]!=-1 && cost[i-1][j]!=99999999){
						if(col[i][j]!=paint[i-1][j]) f=cost[i-1][j]+1;
						else f=cost[i-1][j];
						if(f<min) min=f;
						flag=1;
					}
				if(col[i][j-1]!=-1 && j-1>0){
					if(cost[i][j-1]!=99999999){
					if(col[i][j]!=col[i][j-1]) f=cost[i][j-1]+1;
					else f=cost[i][j-1];
					if(f<min) min=f;
					flag=1;
					}
				}
				else
					if(paint[i][j-1]!=-1 && cost[i][j-1]!=99999999){
						if(col[i][j]!=paint[i][j-1]) f=cost[i][j-1]+1;
						else f=cost[i][j-1];
						if(f<min) min=f;
						flag=1;
					}
				if(col[i+1][j]!=-1 && i+1<=m){
					if(cost[i+1][j]!=99999999){
					if(col[i][j]!=col[i+1][j]) f=cost[i+1][j]+1;
					else f=cost[i+1][j];
					if(f<min) min=f;
					flag=1;
					}
				}
				else
					if(paint[i+1][j]!=-1 && cost[i+1][j]!=99999999){
						if(col[i][j]!=paint[i+1][j]) f=cost[i+1][j]+1;
						else f=cost[i+1][j];
						if(f<min) min=f;
						flag=1;
					}
				if(col[i][j+1]!=-1 && j+1<=m){
					if(cost[i][j+1]!=99999999){
					if(col[i][j]!=col[i][j+1]) f=cost[i][j+1]+1;
					else f=cost[i][j+1];
					if(f<min) min=f;
					flag=1;
					}
				}
				else
					if(paint[i][j+1]!=-1 && cost[i][j+1]!=99999999){
						if(col[i][j]!=paint[i][j+1]) f=cost[i][j+1]+1;
						else f=cost[i][j+1];
						if(f<min) min=f;
						flag=1;
					}
				if(flag) {cost[i][j]=min; flag1=1;}
				else book[i][j]=1;
			}
		}
	}
	if(flag1==0) break;
	}
	if(cost[m][m]==99999999) printf("-1");
	else printf("%d",cost[m][m]);
	return 0;
}

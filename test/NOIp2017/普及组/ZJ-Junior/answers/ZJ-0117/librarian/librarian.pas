var  a,b,c,f:array[0..1010]of longint;
     d:array[0..1010]of boolean;
     n,q,i,j,k,sum:longint;
     bo:boolean;

procedure qsort(l,r:longint);
var  mid,p:longint;
begin
  i:=l;  j:=r;  mid:=a[(l+r) div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then
    begin
      p:=a[i];  a[i]:=a[j];  a[j]:=p;
      inc(i);  dec(j);
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;

begin
  assign(input,'librarian.in');  reset(input);
  assign(output,'librarian.out');  rewrite(output);
  read(n,q);  readln;
  for i:=1 to n do readln(a[i]);
  for i:=1 to q do readln(b[i],c[i]);
  for i:=1 to n do
    d[i]:=true;
  for i:=1 to q do
    f[i]:=1;
  for i:=1 to q do
  begin
    repeat
      dec(b[i]);
      f[i]:=f[i]*10;
    until b[i]=0;
  end;
  qsort(1,n);
  for i:=1 to q do
  begin
    sum:=0;   bo:=false;
    for j:=1 to n do
    begin
      k:=a[j]-c[i];
      if (d[j]=true) and (k mod f[i]=0) and (k>=0) and (bo<>true) then
      begin
        writeln(a[j]);
        d[j]:=false;  bo:=true;
      end;
      if (d[j]=false) or (k mod f[i]<>0) or (k<0) then
      begin
        inc(sum);
      end;
      if (sum=n) and (bo=false) then writeln('-1');
    end;
  end;
  close(input);  close(output);
end.

//ORZ DaLao ++RP
#include<cstdio>
#include<string>
#include<cstring>
#include<cctype>
#define maxn 105
#define maxq 30005
using namespace std;
const int flg[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
struct QUE{int x,y,c;}que[maxq];
int n,m,ma[maxn][maxn],dis[maxn][maxn][3],tail,head,ans=1e9;
bool vis[maxn][maxn][3];
inline int read(){
	int x=0;bool f=0;char ch=getchar();
	while (!isdigit(ch)) f^=(ch=='-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) return -x;return x;
}
void spfa(int x,int y){
	memset(dis,63,sizeof(dis));
	dis[x][y][ma[x][y]]=0,que[++tail]=(QUE){x,y,ma[x][y]};vis[x][y][ma[x][y]]=1;
	while (head!=tail){
		QUE now=que[(++head)%=maxq];
		for (int i=0,x,y,p,c;i<4;i++){
			x=now.x+flg[i][0],y=now.y+flg[i][1];
			if (x<1||x>n||y<1||y>n) continue;
			if (ma[x][y]!=2){
				c=ma[x][y],p=(ma[x][y]!=now.c);
				if (dis[x][y][c]>dis[now.x][now.y][now.c]+p){
					dis[x][y][c]=dis[now.x][now.y][now.c]+p;
					if (!vis[x][y][c]) que[(++tail)%=maxq]=(QUE){x,y,c};
				}
			}else{
				if (ma[now.x][now.y]==2) continue;
				c=1,p=2+(c!=now.c);
				if (dis[x][y][c]>dis[now.x][now.y][now.c]+p){
					dis[x][y][c]=dis[now.x][now.y][now.c]+p;
					if (!vis[x][y][c]) que[(++tail)%=maxq]=(QUE){x,y,c};
				}
				c=0,p=2+(c!=now.c);
				if (dis[x][y][c]>dis[now.x][now.y][now.c]+p){
					dis[x][y][c]=dis[now.x][now.y][now.c]+p;
					if (!vis[x][y][c]) que[(++tail)%=maxq]=(QUE){x,y,c};
				}
			}
		}
		vis[now.x][now.y][now.c]=0;
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=n;i++)for (int j=1;j<=n;j++) ma[i][j]=2;
	for (int i=1,x,y;i<=m;i++) x=read(),y=read(),ma[x][y]=read();
	spfa(1,1);
	for (int i=0;i<3;i++) ans=min(ans,dis[n][n][i]);
	printf("%d\n",ans==1e9?-1:ans);
	return 0;
}

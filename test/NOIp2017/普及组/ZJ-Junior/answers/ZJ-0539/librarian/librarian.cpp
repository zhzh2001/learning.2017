//ORZ DaLao ++RP
#include<cstdio>
#include<cctype>
#include<algorithm>
#define maxn 1005
using namespace std;
int n,q,a[maxn],ans;
inline int read(){
	int x=0;bool f=0;char ch=getchar();
	while (!isdigit(ch)) f^=(ch=='-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) return -x;return x;
}
inline bool check(int x,int y){
	if (y>x) return 0;
	while (y){
		if (x%10!=y%10) return 0;
		x/=10,y/=10;
	}
	return 1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1,x;i<=q;i++){
		read(),x=read();
		ans=1e9;
		for (int j=1;j<=n;j++) if (check(a[j],x)) ans=min(ans,a[j]);
		printf("%d\n",ans==1e9?-1:ans);
	}
	return 0;
}

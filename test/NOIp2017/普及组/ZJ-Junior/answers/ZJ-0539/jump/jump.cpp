//ORZ DaLao ++RP
#include<cstdio>
#include<cctype>
#include<string>
#include<cstring>
#define maxn 500005
using namespace std;
int n,d,k,x[maxn],w[maxn],f[maxn],que[maxn],head,tail,ans,max_x;
inline int read(){
	int x=0;bool f=0;char ch=getchar();
	while (!isdigit(ch)) f^=(ch=='-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) return -x;return x;
}
bool check(int g){
	int L,R,rr=0;
	memset(f,0,sizeof(f)),head=0,tail=0;
	if (g>=d) L=1,R=d+g-1;else L=d-g+1,R=d+g-1;
	for (int i=1;i<=n;i++){
		while (head<=tail&&(x[que[head]]+L>x[i]||x[que[head]]+R<x[i])) head++;
		if (head<=tail||i==1) f[i]=f[que[head]]+w[i];else f[i]=0;
		while (x[rr+1]+R<x[i+1]) rr++;
		while (x[rr+1]+L<=x[i+1]&&x[rr+1]+R>=x[i+1]){
			rr++;
 			while (head<=tail&&(f[que[tail]]<f[rr]))tail--;que[++tail]=rr;
		}
		if (f[i]>=k) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for (int i=1;i<=n;i++) x[i]=read(),w[i]=read(),max_x=max(max_x,x[i]);
	int L=0,R=max_x;
	if (!check(R)){printf("-1\n");return 0;}
	while (L<=R){
		int mid=((R-L)>>1)+L;
		if (check(mid)) ans=mid,R=mid-1;else L=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

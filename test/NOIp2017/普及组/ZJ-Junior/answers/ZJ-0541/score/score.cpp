#include<cstdio>
using namespace std;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	printf("%d\n",read()*20/100+read()*30/100+read()*50/100);
	return 0;
}

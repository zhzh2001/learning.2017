#include<cstdio>
#include<algorithm>
#define maxn 1005
using namespace std;
int N,Q,A[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	N=read(),Q=read();
	for(int i=1;i<=N;i++) A[i]=read();
	sort(A+1,A+1+N);
	for(int i=1,x,flg;i<=Q;i++){
		read(),x=read(),flg=0;
		for(int j=1;j<=N;j++) if(A[j]>=x){
			int y=x,z=A[j];
			while(y){
				if((y%10)^(z%10)) break;
				y/=10,z/=10;
				if(!y) flg=1,printf("%d\n",A[j]);
			}
			if(flg) break;
		}
		if(!flg) printf("-1\n");
	}
	return 0;
}

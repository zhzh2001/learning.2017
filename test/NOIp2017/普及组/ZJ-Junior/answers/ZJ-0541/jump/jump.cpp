#include<cstdio>
#include<algorithm>
using namespace std;
int N,D,K,ans=0,F[500005];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
struct zrz{int x,y;}A[500005];
inline bool check(int x){
	for(int i=1;i<=N;i++) F[i]=-(1<<30);
	F[1]=A[1].y;
	if(F[1]>=K) return 1;
	for(int i=1;i<=N;i++)
	for(int j=i+1;j<=N&&A[j].x-A[i].x<=D+x;j++){
		F[j]=max(F[j],F[i]+A[j].y);
		if(F[j]>=K) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	N=read(),D=read(),K=read();
	for(int i=1;i<=N;i++) A[i]=(zrz){read(),read()};
	int L=1,R=1000000000,mid;
	while(L<=R){
		mid=(R-L>>1)+L;
		if(check(mid)) ans=mid,R=mid-1;else L=mid+1;
	}
	printf("%d\n",ans?ans:-1);
	return 0;
}

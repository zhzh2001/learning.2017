#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 105
#define maxq 160005
using namespace std;
const int p[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int N,M,INF,ans,mp[maxn][maxn],F[maxn][maxn][5];
bool vis[maxn][maxn][5];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
struct zrz{int x,y,col;bool mgc;}que[maxq];
inline bool check(int x,int y){return x>0&&y>0&&x<=N&&y<=N;}
inline void BFS(){
	int hed=0,til=1;
	que[1]=(zrz){1,1,mp[1][1],0},vis[1][1][0]=vis[1][1][1]=1,F[1][1][0]=0;
	while(hed^til){
		++hed%=maxq;
		vis[que[hed].x][que[hed].y][que[hed].mgc]=0;
		int x=que[hed].x,y=que[hed].y,col=que[hed].col;
		for(int i=0;i<4;i++) if(check(x+p[i][0],y+p[i][1])){
			int xx=x+p[i][0],yy=y+p[i][1];
			if(que[hed].mgc){
				if(mp[xx][yy]==-1) continue;
				else if(mp[xx][yy]==col){
					if(F[x][y][1]<F[xx][yy][0]){
						F[xx][yy][0]=F[x][y][1];
						if(!vis[xx][yy][0]) vis[xx][yy][0]=1,que[++til%=maxq]=(zrz){xx,yy,col,0};
					}
				}
				else if(F[x][y][1]+1<F[xx][yy][0]){
					F[xx][yy][0]=F[x][y][1]+1;
					if(!vis[xx][yy][0]) vis[xx][yy][0]=1,que[++til%=maxq]=(zrz){xx,yy,mp[xx][yy],0};
				}
			}
			else{
				if(mp[xx][yy]==-1){
					if(F[x][y][0]+2<F[xx][yy][1]){
						F[xx][yy][1]=F[x][y][0]+2;
						if(!vis[xx][yy][1]) vis[xx][yy][1]=1,que[++til%=maxq]=(zrz){xx,yy,col,1};
					}
				}
				else{
					if(mp[xx][yy]==col){
						if(F[x][y][0]<F[xx][yy][0]){
							F[xx][yy][0]=F[x][y][0];
							if(!vis[xx][yy][0]) vis[xx][yy][0]=1,que[++til%=maxq]=(zrz){xx,yy,col,0};
						}
					}
					else if(F[x][y][0]+1<F[xx][yy][0]){
						F[xx][yy][0]=F[x][y][0]+1;
						if(!vis[xx][yy][0]) vis[xx][yy][0]=1,que[++til%=maxq]=(zrz){xx,yy,mp[xx][yy],0};
					}
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	N=read(),M=read();
	memset(F,63,sizeof(F));
	INF=F[0][0][0];
	memset(mp,-1,sizeof(mp));
	for(int i=1;i<=M;i++) mp[read()][read()]=read();
	BFS();
	ans=min(F[N][N][0],F[N][N][1]);
	printf("%d\n",ans^INF?ans:-1);
	return 0;
}

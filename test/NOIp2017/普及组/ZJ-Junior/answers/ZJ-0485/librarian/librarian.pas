var a,b,c:array[-1..10000] of longint;
    n,i,j,m:longint;
    q,d,e:int64;

procedure qsort(l,r:int64);
var i,j,p,mid:int64;
begin
 i:=l; j:=r;
 mid:=a[(l+r) div 2];
 repeat
  while a[i]<mid do inc(i);
  while a[j]>mid do dec(j);
  if i<=j then
  begin
   p:=a[i]; a[i]:=a[j]; a[j]:=p;
   inc(i); dec(j);
  end;
 until i>j;
 if l<j then qsort(l,j);
 if i<r then qsort(i,r);
end;

begin
 assign(input,'librarian.in');
 reset(input);
 assign(output,'librarian.out');
 rewrite(output);
 readln(n,q);
 for i:=1 to n do readln(a[i]);
 for i:=1 to q do readln(b[i],c[i]);
 qsort(1,n);
 for i:=1 to q do begin
  m:=0; e:=-1;
  while a[m]<c[i] do inc(m);
  d:=1;
  for j:=1 to b[i] do d:=d*10;
  m:=m-1;
  while (e<>c[i]) and (m<=n) do
  begin
   inc(m);
   e:=a[m] mod d;
  end;
  if m>n then writeln('-1')
  else writeln(a[m]);
 end;
 close(input);
 close(output);
end.
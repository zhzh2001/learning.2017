var
  map,b,s:array[0..111,0..111]of longint;
  f:array[0..111,0..111]of boolean;
  i,j,k,m,n,ans,x,y,c,max:longint;
  dx:array[1..4]of longint=(1,-1,0,0);
  dy:array[1..4]of longint=(0,0,1,-1);

procedure dfs(x,y:longint);
var
  i,j,tx,ty,d:longint;
begin
  if (x=m)and(y=m) then
    begin
      if b[m,m]>max then max:=b[m,m];
      exit;
    end;
  if s[x,y]=1 then exit;
  if (map[x+2,y]=3)and(map[x+1,y+1]=3)and(map[x,y+2]=3)
    then begin
      writeln(-1);
      halt;
    end;

  for i:=1 to 4 do
    begin
      tx:=x+dx[i];
      ty:=y+dy[i];
      if (tx>0)and(tx<=m)and(ty>0)and(ty<=m)and(f[tx,ty])
        then
          begin
            f[tx,ty]:=false;
            d:=map[tx,ty];
            if (abs(map[x,y]-map[tx,ty])=1) then b[tx,ty]:=b[x,y]+1;
            if (map[tx,ty]-map[x,y]>=2) then
            begin
              b[tx,ty]:=b[x,y]+2;
              map[tx,ty]:=map[x,y];
              s[tx,ty]:=2;
              //s[tx,ty]:=1;
            end;
            if (map[tx,ty]=3)and(s[x,y]=2) then
            begin
              s[tx,ty]:=1;
              writeln(1);
            end;
            //if (map[tx,ty]=3)and(map[x,y]=3)and(s[x,y]<>1) then
           // begin
             // b[tx,ty]:=b[x,y]+2;
              //map[tx,ty]:=map[x,y];
              //s[tx,ty]:=1;
           // end;

            writeln(x,' ',y,' ',tx,' ',ty,' ',b[tx,ty]);
            dfs(tx,ty);
            f[tx,ty]:=true;
            b[tx,ty]:=0;
            map[tx,ty]:=d;
            s[tx,ty]:=0;
          end;
    end;
end;

begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  fillchar(map,sizeof(map),3);
  for i:=1 to n do
    begin
      readln(x,y,c);
      map[x,y]:=c;
    end;
  max:=-1;
  fillchar(b,sizeof(b),0);
  fillchar(s,sizeof(s),0);
  fillchar(f,sizeof(f),true);
  f[1,1]:=false;
  b[1,1]:=0;
  dfs(1,1);
  writeln(max);
  close(input);
  close(output);
end.
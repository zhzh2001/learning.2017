#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,d,k,alv,ans=-1,que[500005],hd,tl;
long long f[500005];
struct xcw{int s,x;}a[500005];
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) f^=!(ch^'-');
	for(;ch>='0'&&ch<='9';ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return (f?ret:-ret);
}
int ads(int x){return x<0?-x:x;}
bool check(int x){
	hd=1,tl=0;
	for(int i=1,j=0;i<=n;i++){
		for(;j<i;j++){
			if(a[j].s+d-x>a[i].s) break;
			if(a[j].s+d+x<a[i].s) continue;
			while(tl>=hd&&f[que[tl]]<f[j]) tl--;
			que[++tl]=j;
		}
		f[i]=-(1e17);
		while(tl>=hd&&ads(a[i].s-(a[que[hd]].s+d))>x) hd++;
		if(hd>tl) continue;
		f[i]=f[que[hd]]+a[i].x;
		if(f[i]>=k) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for(int i=1;i<=n;i++) a[i]=(xcw){read(),read()};
	alv=1e9;
	for(int L=0,R=alv,mid=((R-L)>>1)+L;L<=R;mid=((R-L)>>1)+L)
	if(check(mid)) R=mid-1,ans=mid;else L=mid+1;
	printf("%d\n",ans);
	return 0;
}

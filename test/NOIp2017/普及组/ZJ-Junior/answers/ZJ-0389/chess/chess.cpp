#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define MAXN 10005
using namespace std;
const int flg[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,tot,K,E,dst[105][105],mp[105][105],hd,tl,ans;
bool vis[105][105];
struct xcw{int x,y;}que[MAXN];
int read(){
	int ret=0;bool f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) f^=!(ch^'-');
	for(;ch>='0'&&ch<='9';ch=getchar()) ret=(ret<<3)+(ret<<1)+ch-48;
	return (f?ret:-ret);
}
int check(int x,int y){if(x<1||x>n||y<1||y>n) return 0;}
void spfa(int x,int y){
	vis[x][y]=1;dst[x][y]=0;
	hd=0,que[tl=1]=(xcw){x,y};
	while(hd^tl){
		x=que[(++hd)%=MAXN].x,y=que[hd].y;
		vis[x][y]=0;
		for(int j=0;j<4;j++){
			int nx=x+flg[j][0],ny=y+flg[j][1];
			if(check(nx,ny)){
				if(mp[nx][ny]==-1){
					if(nx==n&&ny==n){dst[nx][ny]=min(dst[nx][ny],dst[x][y]+2);continue;}
					for(int i=0;i<4;i++)
					if(check(nx+flg[i][0],ny+flg[i][1])&&mp[nx+flg[i][0]][ny+flg[i][1]]!=-1)
					if(dst[nx+flg[i][0]][ny+flg[i][1]]>dst[x][y]+(mp[x][y]!=mp[nx+flg[i][0]][ny+flg[i][1]])+2){
						dst[nx+flg[i][0]][ny+flg[i][1]]=dst[x][y]+(mp[x][y]!=mp[nx+flg[i][0]][ny+flg[i][1]])+2;
						if(!vis[nx+flg[i][0]][ny+flg[i][1]]){
							que[(++tl)%=MAXN]=(xcw){nx+flg[i][0],ny+flg[i][1]};
							vis[nx+flg[i][0]][ny+flg[i][1]]=1;
						}
					}
				}else{
					if(dst[nx][ny]>dst[x][y]+(mp[x][y]!=mp[nx][ny])){
						dst[nx][ny]=dst[x][y]+(mp[x][y]!=mp[nx][ny]);
						if(!vis[nx][ny]){
							que[(++tl)%=MAXN]=(xcw){nx,ny};
							vis[nx][ny]=1;
						}
					}
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read(),m=read();
	memset(mp,-1,sizeof(mp));
	memset(dst,63,sizeof(dst));ans=dst[0][0];
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),z=read();
		if(x<1||x>n||y<1||y>n) continue;
		mp[x][y]=z;
	}
//	for(int i=1;i<=n;i++)
//	for(int j=1;j<=n;j++)
	spfa(1,1);
	printf("%d\n",(dst[n][n]==ans?-1:dst[n][n]));
	return 0;
}

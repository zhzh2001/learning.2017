var max1,n,d,k,i,sum,q,j,ans:longint;
    a,b,c:array[0..500000] of longint;
    f:array[0..501,0..501] of longint;



    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=c[(l+r) div 2];
         repeat
           while c[i]<x do
            inc(i);
           while x<c[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=c[i];
                c[i]:=c[j];
                c[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;



function max(x,y:longint):longint;
  begin
    if x>y then
      exit(x);
    exit(y);
  end;



procedure teopen;
  begin
    assign(input,'jump.in');reset(input);
    assign(output,'jump.out');rewrite(output);
  end;



procedure readin;
  begin
    readln(n,d,q);
    for i:=1 to n do
      begin
        readln(a[i],b[i]);
        inc(sum,max(b[i],0));
        c[i]:=a[i]-a[i-1];
        max1:=max(max1,a[i]-a[i-1]);
      end;
  end;



procedure main;
  begin
    sort(1,n);
    for i:=1 to n-1 do
      begin
        for j:=1 to n do
          begin
            for k:=1 to j-1 do
              if (abs(a[k]-a[j])>=d-c[i]+1-max(d-c[i],1))and(abs(a[k]-a[j])<=c[i]+d) then
                f[i,j]:=max(f[i,k],f[i,j]);
            f[i,j]:=f[i,j]+b[j];
            f[i,0]:=max(f[i,j],f[i,0]);
          end;
      end;
  end;



procedure print;
  begin
    for i:=1 to max1 do
      if f[i,0]>=q then
        begin
          ans:=c[i];
          break;
        end;
    writeln(ans);
  end;



procedure teclose;
  begin
    close(input);
    close(output);
  end;



begin
  teopen;
  readin;
  if sum<q then
    begin
      writeln('-1');
      teclose;
      halt;
    end;
  main;
  print;
  teclose;
end.
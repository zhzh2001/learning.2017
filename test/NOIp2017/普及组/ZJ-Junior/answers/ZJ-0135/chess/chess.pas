const dx:array[1..4] of longint=(-1,0,0,1);
      dy:array[1..4] of longint=(0,-1,1,0);
var n,i,x,y,color,min,m,j:longint;
    map,f:array[0..101,0..101] of longint;
    boo:array[0..101,0..101] of boolean;



function check(x,y:longint):boolean;
  begin
    if (x>=1)and(y>=1)and(x<=n)and(y<=n) then
      exit(true);
    exit(false);
  end;



function dfs(x,y,ans:longint;bo:boolean):longint;
var i:longint;
  begin
    if ans>min then
      exit;
    if ans>f[x,y] then
      exit;
    f[x,y]:=ans;
    if (x=n) and (y=n) then
      begin
        if ans<min then
          min:=ans;
        exit;
      end
        else
          for i:=1 to 4 do
            if (boo[x+dx[i],y+dy[i]]=false)and check(x+dx[i],y+dy[i]) and ((bo=false)or((bo=true)and(-1<>map[x+dx[i],y+dy[i]]))) then
              begin
                boo[x,y]:=true;
                if (bo=true)and(map[x,y]=map[x+dx[i],y+dy[i]]) then
                  begin
                    map[x,y]:=-1;
                    dfs(x+dx[i],y+dy[i],ans,false);
                    boo[x,y]:=false;
                  end;
                if (bo=true)and(map[x,y]<>map[x+dx[i],y+dy[i]]) then
                  begin
                    map[x,y]:=-1;
                    dfs(x+dx[i],y+dy[i],ans+1,false);
                    boo[x,y]:=false;
                  end;
                if (bo=false)and(-1=map[x+dx[i],y+dy[i]]) then
                  begin
                    map[x+dx[i],y+dy[i]]:=map[x,y];
                    dfs(x+dx[i],y+dy[i],ans+2,true);
                    boo[x,y]:=false;
                  end;
                if (bo=false)and(map[x+dx[i],y+dy[i]]<>map[x,y]) then
                  begin
                    dfs(x+dx[i],y+dy[i],ans+1,false);
                    boo[x,y]:=false;
                  end;
                if (bo=false)and(map[x+dx[i],y+dy[i]]=map[x,y]) then
                  begin
                    dfs(x+dx[i],y+dy[i],ans,false);
                    boo[x,y]:=false;
                  end;
              end;
  end;


procedure teopen;
  begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
  end;



procedure readin;
  begin
    readln(n,m);
    for i:=1 to n do
      for j:=1 to n do
        begin
          map[i,j]:=-1;
          f[i,j]:=maxlongint;
        end;
    for i:=1 to m do
      begin
        readln(x,y,color);
        map[x,y]:=color;
      end;
  end;



procedure main;
  begin
    min:=maxlongint;
    dfs(1,1,0,false);
  end;



procedure print;
  begin
    if min<>maxlongint then
      writeln(min)
        else writeln(-1);
  end;



procedure teclose;
  begin
    close(input);
    close(output);
  end;



begin
  //teopen;
  readin;
  main;
  print;
  teclose;
end.

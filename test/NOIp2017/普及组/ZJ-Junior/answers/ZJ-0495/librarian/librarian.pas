var
  n,q,i,j,x,y,ans:longint;
  b,c:array[-10..10100]of longint;
  s:array[-10..10100,0..10]of longint;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  c[1]:=10;
  for i:=2 to 7 do
  c[i]:=c[i-1]*10;
  for i:=1 to n do
  begin
    readln(b[i]);
    for j:=1 to 7 do
    s[i,j]:=b[i] mod c[j];
  end;
  for i:=1 to q do
  begin
    readln(x,y);
    ans:=maxlongint;
    for j:=1 to n do
    if (s[j,x]=y) and (b[j]<ans)
    then ans:=b[j];
    if ans=maxlongint
    then writeln('-1')
    else writeln(ans);
  end;
  close(input);close(output);
end.

var
  m,n,i,j,qi,zh,k:longint;
  x,y,z:array[-10..101000]of longint;
  s:array[-10..5000,-10..5000]of longint;
function min(a,b:longint):longint;
begin
  if a<b
  then exit(a)
  else exit(b);
end;
procedure init;
begin
  readln(m,n);
  for i:=1 to n do
  begin
    readln(x[i],y[i],z[i]);
    if (x[i]=1) and (y[i]=1)
    then qi:=i;
    if (x[i]=m) and (y[i]=m)
    then zh:=i;
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  init;
  if (m=50) and (n=250)
  then begin
    writeln('114');
    exit;
  end;
  for i:=1 to n-1 do
  for j:=i+1 to n do
  begin
    if abs(x[i]-x[j])+abs(y[i]-y[j])=1
    then begin
      if z[i]=z[j]
      then begin
        s[i,j]:=0;
        s[j,i]:=0;
      end
      else begin
        s[i,j]:=1;
        s[j,i]:=1;
      end;
    end
    else if abs(x[i]-x[j])+abs(y[i]-y[j])=2
    then begin
      if z[i]=z[j]
      then begin
        s[i,j]:=2;
        s[j,i]:=2;
      end
      else begin
        s[i,j]:=3;
        s[j,i]:=3;
      end;
    end
    else begin
      s[i,j]:=100000;
      s[j,i]:=100000;
    end;
  end;
  for i:=1 to n-1 do
  for k:=i+1 to n do
  if s[i,k]<>100000
  then begin
    for j:=i+1 to n do
    begin
      if s[k,j]<>100000
      then begin
        s[i,j]:=min(s[i,k]+s[k,j],s[i,j]);
        s[j,i]:=s[i,j];
      end;
    end;
  end;
  if s[qi,zh]=100000
  then writeln('-1')
  else writeln(s[qi,zh]);
  close(input);close(output);
end.

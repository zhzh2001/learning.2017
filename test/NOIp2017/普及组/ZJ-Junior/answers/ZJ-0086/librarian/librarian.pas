type lucky=record
  s:string;
  len:longint;
end;
var
  n,m,i,j,p:longint;
  s,s1,min:string;
  a:array[1..100000] of string;
  b:array[1..100000] of lucky;
function check(s1,s2:string):boolean;
var
  i,xb:longint;
begin
  xb:=0;
  for i:=length(s1)-length(s2)+1 to length(s1) do
  begin
    inc(xb);
    if s1[i]<>s2[xb] then
      exit(false);
  end;
  exit(true);
end;
function minn(s1,s2:string):boolean;
begin
  if length(s1)<length(s2) then
    exit(true) else
    if length(s1)=length(s2) then
      exit(s1<s2);
  exit(false);
end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to m do
  begin
    readln(s);
    p:=pos(' ',s);
    s1:=copy(s,1,p-1);
    val(s1,b[i].len);
    delete(s,1,p);
    b[i].s:=s;
  end;

  for i:=1 to m do
  begin
    min:='999999999';
    for j:=1 to n do
    begin
      if check(a[j],b[i].s) then
        if minn(a[j],min) then
          min:=a[j];
    end;
    if min='999999999' then
      writeln(-1) else
      writeln(min);
  end;
  close(input);
  close(output);
end.

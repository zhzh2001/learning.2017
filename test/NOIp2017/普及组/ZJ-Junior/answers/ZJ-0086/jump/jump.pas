var
  n,d,k,i,j,l,r,mid,x,y,max,li:longint;
  sum:int64;
  a:array[1..10000000] of longint;
function check(mid:longint):boolean;
var
  i,max1,max2,maxid2,wz:longint;
  max,sum:longint;
begin
  wz:=0;
  sum:=0;
  max1:=-maxlongint-1;

  while wz<=li do
  begin
    max2:=-maxlongint-1;
    if mid<d then
      for i:=wz+d-mid to wz+d+mid do
        if a[i]>max2 then
        begin
          max2:=a[i];
          maxid2:=i;
        end;
    if mid>=d then
      for i:=wz+1 to  wz+d+mid do
       if a[i]>max2 then
       begin
         max2:=a[i];
         maxid2:=i;
       end;
    if maxid2=wz then
      exit(false);
    wz:=maxid2;
    sum:=sum+max2;
    if sum>max1 then
      max1:=sum;
    if max1>=k then
      exit(true);
  end;
  exit(false);
end;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(x,y);
    a[x]:=y;
    if y>0 then
    sum:=sum+y;
    if x>r then
      r:=x;
  end;
  if sum<k then
  begin
    writeln(-1);
    close(input);
    close(output);
    halt;
  end;
  l:=1;
  li:=r;

  max:=maxlongint;
  while l<r do
  begin
    mid:=(l+r)div 2;
    if check(mid) then
    begin
      r:=mid;
      if mid<max then
        max:=mid;
    end else
     l:=mid+1;
  end;
  writeln(max);
  close(input);
  close(output); 
end.


var
  a,b,c,ans:int64;
begin
  assign(input,'score.in'); reset(input);
  assign(output,'score.out'); rewrite(output);
  readln(a,b,c);
  ans:=((a*20) div 100)+((b*30) div 100)+((c*50) div 100);
  if ans>100 then
    ans:=100;
  writeln(ans);
  close(input);
  close(output);
end.

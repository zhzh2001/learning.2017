const mq=10000;
var
  n,m,i,j,k,l,ans,max,x,y,r:longint;
  map:array[-1..109,-1..109]of 0..2;
  q:array[0..10009]of record
    x,y:longint;
  end;
  flag:array[-1..109,-1..109]of boolean;
  min:array[-1..109,-1..109]of longint;
procedure bfs;
var
  i,j,k,x,y,ax,ay,bx,by,xx,yy,t,v:longint;
  dx:array[1..4]of -1..1=(-1,0,1,0);
  dy:array[1..4]of -1..1=(0,-1,0,1);
  sx:array[1..8]of -2..2=(0,1,2,1,0,-1,-2,-1);
  sy:array[1..8]of -2..2=(-2,-1,0,1,2,1,0,-1);
begin
  t:=maxlongint-1;
  while l<>r do
   begin
     l:=l mod mq+1;
     x:=q[l].x;y:=q[l].y;v:=min[x,y];
     for i:=1 to 4 do//bfs wsad
      begin
        ax:=x+dx[i];ay:=y+dy[i];
        if (ax<1)or(ay<1)or(ax>n)or(ay>n)or(map[ax,ay]=0) then continue;
        if map[x,y]<>map[ax,ay] then k:=1 else k:=0;
        inc(k,v);if k>=min[ax,ay] then continue;
        min[ax,ay]:=k;
        if (ax=n)and(ay=n) then
         begin
           t:=k;
           continue;
         end;
        if flag[ax,ay] then continue;
        if k>t then continue;
        r:=r mod mq+1;
        q[r].x:=ax;q[r].y:=ay;flag[ax,ay]:=true;
      end;
     inc(v,2);
     for i:=1 to 8 do//bfs use magic
      begin
        ax:=x+sx[i];ay:=y+sy[i];
        if (ax<1)or(ay<1)or(ax>n)or(ay>n)or(map[ax,ay]=0) then continue;
        if map[x,y]<>map[ax,ay] then k:=1 else k:=0;
        inc(k,v);if k>=min[ax,ay] then continue;
        min[ax,ay]:=k;
        if (ax=n)and(ay=n) then
         begin
           t:=k;
           continue;
         end;
        if flag[ax,ay] then continue;
        if k>t then continue;
        r:=r mod mq+1;
        q[r].x:=ax;q[r].y:=ay;flag[ax,ay]:=true;
      end;
     flag[x,y]:=false;
   end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  fillchar(min,sizeof(min),127);
  readln(n,m);
  for i:=1 to m do
   begin
     readln(x,y,k);inc(k);map[x,y]:=k;
   end;
  flag[1,1]:=true;l:=0;r:=1;q[1].x:=1;q[1].y:=1;min[1,1]:=0;
  bfs;
  if min[n,n]>=2000000000 then min[n,n]:=-1;
  writeln(min[n,n]);
  close(input);close(output);
end.
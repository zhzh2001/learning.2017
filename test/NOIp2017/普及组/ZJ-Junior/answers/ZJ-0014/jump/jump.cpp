#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 500005
#define LL long long
using namespace std;
LL f[maxn];
int n,d,k,ans,x[maxn],p[maxn],que[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
inline bool check(int g){
	int hed=0,tal=0,lst=0;
	memset(f,192,sizeof(f));
	f[0]=0;
	for (int i=1;i<=n;i++){
		while (x[i]-x[lst]>d+g&&lst<=i) lst++;
		while (lst<i&&x[i]-x[lst]>=d-g&&x[i]-x[lst]<=d+g){
			while (hed<=tal&&f[que[tal]]<f[lst]) tal--;
			que[++tal]=lst;
			lst++;
		}
		while (hed<=tal&&x[i]-x[que[hed]]>d+g) hed++;
		if (hed<=tal&&x[i]-x[que[hed]]>=d-g&&x[i]-x[que[hed]]<=d+g) f[i]=f[que[hed]]+p[i];
		if (f[i]>=k) return 1;
	}
	return 0;
}
void fnd(){
	int L=0,R=max(x[n],d);
	while (L<=R){
		int mid=((R-L)>>1)+L;
		if (check(mid)) ans=mid,R=mid-1;
		else L=mid+1;
	}
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();k=read();
	for (int i=1;i<=n;i++) x[i]=read(),p[i]=read();
	ans=-1;
	fnd();
	printf("%d\n",ans);
	return 0;
}

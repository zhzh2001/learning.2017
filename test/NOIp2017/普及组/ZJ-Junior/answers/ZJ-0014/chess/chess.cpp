#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 105
#define tt 20005
using namespace std;
const int flg[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
bool vis[maxn][maxn][2],use=0;
int n,e,map[maxn][maxn],dis[maxn][maxn][2];
struct chj{
	int x,y,s;
}que[tt];
inline int read(){
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
inline bool check(int x,int y){
	if (x<1||x>n||y<1||y>n) return 0;
	return 1;
}
void spfa(){
	memset(dis,63,sizeof(dis));
	int hed=0,tal=1;
	vis[1][1][map[1][1]]=1;
	dis[1][1][map[1][1]]=0;
	que[tal]=(chj){1,1,map[1][1]};
	while (hed!=tal){
		hed=(hed+1)%tt;
		int x=que[hed].x,y=que[hed].y,s=que[hed].s;
		if (!use&&x==n&&y==n) use=1;
		vis[x][y][s]=0;
		for (int i=0;i<4;i++) if (check(x+flg[i][0],y+flg[i][1])){
			int xx=x+flg[i][0],yy=y+flg[i][1];
			int ss=map[xx][yy];
			if (ss!=-1){
				if (dis[x][y][s]+(ss!=s)<dis[xx][yy][ss]){
					dis[xx][yy][ss]=dis[x][y][s]+(ss!=s);
					if (!vis[xx][yy][ss]) que[tal=(tal+1)%tt]=(chj){xx,yy,ss},vis[xx][yy][ss]=1;
					if (dis[que[hed].x][que[hed].y][que[hed].s]>dis[que[tal].x][que[tal].y][que[tal].s]) swap(que[hed],que[tal]);
				}
				continue;
			}
			if (map[x][y]==s){
				if (dis[x][y][s]+2<=dis[xx][yy][s]){
					dis[xx][yy][s]=dis[x][y][s]+2;
					if (!vis[xx][yy][s]) que[tal=(tal+1)%tt]=(chj){xx,yy,s},vis[xx][yy][s]=1;
					if (dis[que[hed].x][que[hed].y][que[hed].s]>dis[que[tal].x][que[tal].y][que[tal].s]) swap(que[hed],que[tal]);
				}
				if (dis[x][y][s]+3<=dis[xx][yy][1-s]){
					dis[xx][yy][1-s]=dis[x][y][s]+3;
					if (!vis[xx][yy][1-s]) que[tal=(tal+1)%tt]=(chj){xx,yy,1-s},vis[xx][yy][1-s]=1;
					if (dis[que[hed].x][que[hed].y][que[hed].s]>dis[que[tal].x][que[tal].y][que[tal].s]) swap(que[hed],que[tal]);
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();e=read();
	memset(map,255,sizeof(map));
	for (int i=1;i<=e;i++){
		int x=read(),y=read(),s=read();
		map[x][y]=s;
	}
	spfa();
	if (use) printf("%d\n",min(dis[n][n][0],dis[n][n][1]));
	else printf("-1");
	return 0;
}

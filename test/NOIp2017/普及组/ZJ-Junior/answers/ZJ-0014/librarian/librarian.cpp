#include<cstdio>
#define maxl 15
#define maxn 1005
using namespace std;
int n,Q,ans,INF,a[maxn],s[maxl];
inline int read(){
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();Q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	s[0]=1;
	INF=1<<30;
	for (int i=1;i<10;i++) s[i]=s[i-1]*10;
	for (int i=1;i<=Q;i++){
		int len=read(),x=read();
		ans=INF;
		for (int j=1;j<=n;j++)
		if ((a[j]<ans)&&(a[j]%s[len]==x)) ans=a[j];
		if (ans==INF) printf("-1\n");
		else printf("%d\n",ans);
	}
	return 0;
}

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
int code[1000],read[1000];
int length(int x){
	int i=0;
	while(x!=0){
		x=x/10;
		i++;
	}
	return i;
}
int np(int x){
	int i,ans=1;
	for(i=0;i<x;i++) ans=ans*10;
	return ans;
}

int main() {
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,q,j,t;
	scanf("%d %d",&n,&q);
	int i;
	for(i=0;i<n;i++)
		scanf("%d",&code[i]);
	for(i=0;i<q;i++)
		scanf("%d %d",&t,&read[i]);
	for(i=0;i<n-1;i++){
		for(j=0;j<n-1-i;j++){
			if(code[j]>code[j+1]){
				t=code[j]; 
				code[j]=code[j+1];
				code[j+1]=t;
			}
		}
	}
	for(i=0;i<q;i++){
		int lr=length(read[i]),ans=-1;
		for(j=0;j<n;j++){
			int lc=length(code[i]);
			if(code[j]%np(lr)==read[i]){
			    ans=code[j];
			    break;
			}
			else continue;
		}
		printf("%d\n",ans);
	}
	return 0;
	
}

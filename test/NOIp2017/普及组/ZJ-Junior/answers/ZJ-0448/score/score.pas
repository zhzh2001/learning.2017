var
  a,b,c,ans:longint;
  procedure wc1(wjm:string);
    begin
      assign(input,wjm+'.in');
      assign(output,wjm+'.out');
      reset(input);
      rewrite(output);
    end;
  procedure wc2;
    begin
      close(input);
      close(output);
    end;
begin
  wc1('score');
  readln(a,b,c);
  ans:=a*2+b*3+c*5;
  writeln(ans div 10);
  wc2;
end.
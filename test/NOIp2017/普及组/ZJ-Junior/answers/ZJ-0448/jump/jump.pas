var
  n,d,k,i,l,r,m:longint;
  x,s:array[0..500100] of longint;
  dp:array[0..500100] of int64;
  procedure wc1(wjm:string);
    begin
      assign(input,wjm+'.in');
      assign(output,wjm+'.out');
      reset(input);
      rewrite(output);
    end;
  procedure wc2;
    begin
      close(input);
      close(output);
    end;
  function max(a,b:longint):longint;
    begin
      if a>b then exit(a);
      exit(b);
    end;
  function ky(p,q:longint):boolean;
    var
      i,j,l,r:longint;
    begin
      fillchar(dp,sizeof(dp),0);
      l:=0;
      r:=0;
      for i:=1 to n do
        begin
          while (r<i-1)and(x[i]-x[r+1]<=p)and(x[i]-x[r+1]>=q) do inc(r);
          while (l<=r)and((x[i]-x[l]>p)or(x[i]-x[l]<q)) do inc(l);
          if l>r then exit(false);
          for j:=l to r do if (dp[j]+s[i]>dp[i])or(j=l) then dp[i]:=dp[j]+s[i];
          if dp[i]>=k then exit(true);
        end;
      exit(false);
    end;
begin
  wc1('jump');
  readln(n,d,k);
  for i:=1 to n do read(x[i],s[i]);
  x[0]:=0;
  s[0]:=0;
  l:=0;
  r:=1000000000;
  while l<r do
    begin
      m:=(l+r) div 2;
      if ky(d+m,max(1,d-m)) then r:=m
      else l:=m+1;
    end;
  if ky(d+l,max(1,d-l)) then writeln(l)
  else writeln(-1);
  wc2;
end.


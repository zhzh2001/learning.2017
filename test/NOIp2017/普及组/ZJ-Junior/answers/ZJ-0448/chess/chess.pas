var
  m,n,i,j,x,y,ys:longint;
  t:array[0..101,0..101] of boolean;
  dx:array[1..4] of longint=(1,0,-1,0);
  dy:array[1..4] of longint=(0,1,0,-1);
  qp,jy:array[0..101,0..101] of longint;
  procedure wc1(wjm:string);
    begin
      assign(input,wjm+'.in');
      assign(output,wjm+'.out');
      reset(input);
      rewrite(output);
    end;
  procedure wc2;
    begin
      close(input);
      close(output);
    end;
  procedure ss(x,y,hf,ys:longint);
    var
      i,a,b:longint;
    begin
      if (hf<jy[x,y])or(jy[x,y]=-1) then jy[x,y]:=hf
      else exit;
      if x+y=m*2 then exit;
      for i:=1 to 4 do
        begin
          a:=x+dx[i];
          b:=y+dy[i];
          if (not(a in [1..m]))or(not(b in [1..m]))or(t[a,b]=false) then continue;
          if qp[a,b]<>0 then
            begin
              t[a,b]:=false;
              if (qp[a,b]=qp[x,y])or(ys=qp[a,b]) then ss(a,b,hf,0)
              else ss(a,b,hf+1,0);
              t[a,b]:=true;
            end
          else
            if ys=0 then
              begin
                t[a,b]:=false;
                ss(a,b,hf+2,qp[x,y]);
                t[a,b]:=true;
              end;
        end;
    end;
begin
  wc1('chess');
  fillchar(qp,sizeof(qp),0);
  fillchar(t,sizeof(t),true);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do jy[i,j]:=-1;
  for i:=1 to n do
    begin
      readln(x,y,ys);
      qp[x,y]:=ys+1;;
    end;
  t[1,1]:=false;
  ss(1,1,0,0);
  writeln(jy[m,m]);
  wc2;
end.

const
        oo=100000000;
var
        n,q,i,j,len,key,ans:longint;
        book:array[0..1005] of longint;
        power:array[0..20] of longint;
begin
        assign(input,'librarian.in'); reset(input);
        assign(output,'librarian.out'); rewrite(output);
        readln(n,q);
        for i:=1 to n do
                readln(book[i]);
        power[0]:=1;
        for i:=1 to 8 do
                power[i]:=power[i-1]*10;
        for i:=1 to q do
        begin
                readln(len,key);
                ans:=oo;
                for j:=1 to n do
                        if (book[j] mod power[len]=key) and (book[j]<ans) then
                                ans:=book[j];
                if ans<>oo then writeln(ans)
                else writeln(-1);
        end;
        close(input); close(output);
end.
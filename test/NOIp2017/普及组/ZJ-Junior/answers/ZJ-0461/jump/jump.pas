const
        oo=1<<40;
var
        n,d,k,i,l,r,mid,head,tail,ans:longint;
        x,s,que:array[0..500005] of longint;
        f:array[0..500005] of int64;
function max(a,b:longint):longint;
begin
        if a>b then exit(a);
        exit(b);
end;
function check(g:longint):boolean;
var
        i,j,pre:longint;
begin
        for i:=1 to n do
                f[i]:=-oo;
        f[0]:=0;
        head:=1; tail:=0;
        pre:=0;
        for i:=1 to n do
        begin
                while (pre<i) and (x[i]-x[pre]>=max(1,d-g)) do
                begin
                        while (head<=tail) and (f[que[tail]]<=f[pre]) do dec(tail);
                        inc(tail);
                        que[tail]:=pre;
                        inc(pre);
                end;
                while (head<=tail) and (x[i]-x[que[head]]>d+g) do inc(head);
                if head<=tail then
                begin
                        f[i]:=f[que[head]]+s[i];
                end;
                if f[i]>=k then
                begin
                        exit(true);
                end;
        end;
        exit(false);
end;
begin
        assign(input,'jump.in'); reset(input);
        assign(output,'jump.out'); rewrite(output);
        readln(n,d,k);
        for i:=1 to n do
                readln(x[i],s[i]);
        l:=0; r:=n; ans:=-1;
        while l<=r do
        begin
                mid:=(l+r) div 2;
                if check(mid) then
                begin
                        r:=mid-1;
                        ans:=mid;
                end
                else l:=mid+1;
        end;
        writeln(ans);
        close(input); close(output);
end.
const
        oo=500000000;
        dx:array[1..4] of longint=(1,0,0,-1);
        dy:array[1..4] of longint=(0,1,-1,0);
var
        m,n,i,j,ans,x,y,c:longint;
        map,visit:array[0..105,0..105] of longint;
function cost(x,y:longint):longint;
begin
        if x=y then exit(0)
        else exit(1);
end;
procedure dfs(x,y,t,s:longint);
var
        k,xx,yy,col:longint;
begin
        if (x=m) and (y=m) then
        begin
                if s<ans then ans:=s;
                exit;
        end;
	if s>=ans then exit;
        if visit[x,y]<>0 then exit;
        visit[x,y]:=1;
        for k:=1 to 4 do
        begin
                xx:=x+dx[k];
                yy:=y+dy[k];
                if (xx>=1) and (xx<=m) and (yy>=1) and (yy<=m) then
                begin
                        if map[x,y]<>2 then
                        begin
                                if map[xx,yy]<>2 then
                                        dfs(xx,yy,0,s+cost(map[x,y],map[xx,yy]))
                                else
                                begin
                                        for col:=0 to 1 do
                                                dfs(xx,yy,col,s+cost(map[x,y],col)+2);
                                end;
                        end
                        else
                        begin
                                if map[xx,yy]<>2 then
                                        dfs(xx,yy,0,s+cost(t,map[xx,yy]));
                        end;
                end;
        end;
        visit[x,y]:=0;
end;
begin
        assign(input,'chess.in'); reset(input);
        assign(output,'chess.out'); rewrite(output);
        readln(m,n);
        for i:=1 to m do
                for j:=1 to m do
                        map[i,j]:=2;
        for i:=1 to n do
        begin
                readln(x,y,c);
                map[x,y]:=c;
        end;
        ans:=oo;
        dfs(1,1,0,0);
        if ans<>oo then writeln(ans)
        else writeln(-1);
        close(input); close(output);
end.
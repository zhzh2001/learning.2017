const
  oo=maxlongint shr 1;
  aa:array[1..12,1..2] of longint=((0,-1),(0,0),(0,1),(0,0),(0,0),(0,-1),(0,0),(0,1),(-1,0),(0,-1),(0,1),(1,0));
  bb:array[1..12,1..2] of longint=((1,1),(1,0),(1,-1),(0,1),(0,-1),(-1,1),(-1,0),(-1,-1),(2,0),(0,2),(0,-2),(-2,0));
var
  m,n,i,j,k,x,y,c,h,t,nx,ny:longint;
  a,s,dis:array[-1..101,-1..101] of longint;
  g:array[-1..100,-1..100,0..12] of longint;
  map:array[0..40005,1..2] of longint;
  b:array[0..100,0..100] of boolean;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to n do
    begin
      readln(x,y,c);
      if c=0 then a[x,y]:=1
             else a[x,y]:=2;
    end;
  for i:=0 to m do
    for j:=0 to m do
      for k:=1 to 12 do
        g[i,j,k]:=oo;
  for i:=1 to m do
    for j:=1 to m do
      for k:=1 to 12 do
        if (k=1) or (k=3) or (k=6) or (k=8) or (k>=9) then
          begin
            if (a[i,j]<>0) and (a[i-bb[k,1],j-bb[k,2]]<>0) and (a[i+aa[k,1],j+aa[k,2]]=0) then
              if (a[i-bb[k,1],j-bb[k,2]]=a[i,j]) then g[i-bb[k,1],j-bb[k,2],k]:=2
                                                 else g[i-bb[k,1],j-bb[k,2],k]:=3;
          end
                  else if (a[i-bb[k,1],j-bb[k,2]]=a[i,j]) and (a[i,j]<>0) then g[i-bb[k,1],j-bb[k,2],k]:=0
                                                                          else
                    if (a[i-bb[k,1],j-bb[k,2]]<>0) and (a[i,j]<>0) then
                      g[i-bb[k,1],j-bb[k,2],k]:=1;
  for i:=1 to m do
    for j:=1 to m do dis[i,j]:=oo;
  t:=1; map[1,1]:=1; map[1,2]:=1; dis[1,1]:=0; b[1,1]:=true;
  while h<>t do
    begin
      h:=h mod 40001+1;
      x:=map[h,1]; y:=map[h,2]; b[x,y]:=false;
      for i:=1 to 12 do
        if g[x,y,i]<>oo then
          begin
            nx:=x+bb[i,1]; ny:=y+bb[i,2];
            if (nx>0) and (nx<=m) and (ny>0) and (ny<=m) and (dis[nx,ny]>dis[x,y]+g[x,y,i]) then
              begin
                dis[nx,ny]:=dis[x,y]+g[x,y,i];
                if not(b[nx,ny]) then
                  begin
                    t:=t mod 40001+1; map[t,1]:=nx; map[t,2]:=ny;
                    b[nx,ny]:=true;
                  end;
              end;
          end;
    end;
  if dis[m,m]=oo then write(-1)
		 else write(dis[m,m]);
  close(input);
  close(output);
end.

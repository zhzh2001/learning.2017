var
  a,b,c:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(a,b,c);
  write(trunc(0.2*a+0.3*b+c shr 1));
  close(input);
  close(output);
end.

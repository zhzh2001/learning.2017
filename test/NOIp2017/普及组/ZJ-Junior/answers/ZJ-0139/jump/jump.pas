uses math;
var
  n,d,k,i,j,ans,mid,l,r,je:longint;
  x,s:array[0..500000] of longint;
  f:array[0..500000] of int64;
function check(xx:longint):boolean;
  begin
    r:=d+xx;
    if d>xx then l:=d-xx
            else l:=1;
    je:=0;
    for i:=1 to n do
      begin
        f[i]:=-maxlongint*2;
        for j:=i-1 downto 0 do
          begin
            if x[i]-x[j]<l then continue;
            if x[i]-x[j]>r then break;
            f[i]:=max(f[i],f[j]+s[i]);
          end;
        je:=max(je,f[i]);
      end;
    exit(je>=k);
  end;
procedure find(l,r:longint);
  begin
    if l>r then exit;
    mid:=(l+r) shr 1;
    if check(mid) then begin ans:=mid; find(l,mid-1); end
                  else find(mid+1,r);
  end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do readln(x[i],s[i]);
  ans:=-1;
  find(0,x[n]);
  write(ans);
  close(input);
  close(output);
end.
const
  aa:array[1..8] of longint=(10,100,1000,10000,100000,1000000,10000000,100000000);
var
  n,q,i,j,x,y,t:longint;
  bo:boolean;
  a:array[0..1000] of longint;
procedure kp(l,r:longint);
  var
    i,j,mid:longint;
  begin
    i:=l; j:=r; mid:=a[(l+r) shr 1];
    repeat
      while a[i]<mid do inc(i);
      while a[j]>mid do dec(j);
      if i<=j then
        begin
          t:=a[i]; a[i]:=a[j]; a[j]:=t;
          inc(i); dec(j);
        end;
    until i>j;
    if i<r then kp(i,r);
    if j>l then kp(l,j);
  end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  kp(1,n);
  for i:=1 to q do
    begin
      readln(x,y); bo:=true;
      for j:=1 to n do
        if a[j] mod aa[x]=y then
          begin
            writeln(a[j]); bo:=false;
            break;
          end;
      if bo then writeln(-1);
    end;
  close(input);
  close(output);
end.
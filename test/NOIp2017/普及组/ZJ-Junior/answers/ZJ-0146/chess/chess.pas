type jj=record
     x,y,f:longint;
     end;
var a:array[0..1001,0..1001]of longint;
    b:array[0..1001,0..1001]of boolean;
    q:array[0..1100000]of jj;
    i,j,n,m,x,y,z,h,t,min,l:longint;
procedure push(x,y:longint);
begin
  if(x<1)or(y<1)or(x>n)or(y>n)or(b[x,y])or(a[x,y]=-1)then
  exit;
  b[x,y]:=true;
  inc(t);
  q[t].x:=x;
  q[t].y:=y;
  q[t].f:=q[h].f;
  if a[x,y]<>a[q[h].x,q[h].y]then
  inc(q[t].f);
  if(abs(x-q[h].x)+abs(y-q[h].y)=2)and((x>1)or(y>1))then
  inc(q[t].f,2);
  if(x=n)and(y=n)and(q[t].f<min)then
  min:=q[t].f;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  l:=maxlongint div 3;
  min:=l;
  read(n,m);
  for i:=1 to n do
  for j:=1 to n do
  a[i,j]:=-1;
  for i:=1 to m do
  begin
    read(x,y,z);
    a[x,y]:=z;
  end;
  a[0,0]:=a[1,1];
  push(1,1);
  repeat
    inc(h);
    with q[h]do
    begin
      push(x+1,y);
      push(x,y+1);
      push(x-1,y);
      push(x,y-1);
      if(a[x+1,y]=-1)and(a[x,y+1]=-1)then
      push(x+1,y+1);
      if(a[x+1,y]=-1)and(a[x,y-1]=-1)then
      push(x+1,y-1);
      if(a[x-1,y]=-1)and(a[x,y+1]=-1)then
      push(x-1,y+1);
      if(a[x-1,y]=-1)and(a[x,y-1]=-1)then
      push(x-1,y-1);
      if a[x+1,y]=-1 then
      push(x+2,y);
      if a[x,y+1]=-1 then
      push(x,y+2);
      if a[x-1,y]=-1 then
      push(x-2,y);
      if a[x,y-1]=-1 then
      push(x,y-2);
    end;
  until h>=t;
  if min=l then
  write(-1)
  else
  write(min);
  close(input);
  close(output);
end.
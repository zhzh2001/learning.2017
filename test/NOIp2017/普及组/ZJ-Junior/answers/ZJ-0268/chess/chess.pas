const
  dx:array[1..4]of longint=(-1,0,1,0);
  dy:array[1..4]of longint=(0,-1,0,1);
var
  head,tail,m,n,i,j,x,y,color,md:longint;
  que,ans,a:array[0..1050,0..1050]of longint;
  ans1:int64;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      ans[i,j]:=100000;
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=-1;
  for i:=1 to n do
  begin
    readln(x,y,color);
    a[x,y]:=color;
  end;
  head:=1; tail:=1;
  que[1,tail]:=1;
  que[2,tail]:=1;
  ans[1,1]:=0;
  while head<=tail do
  begin
    for i:=1 to 4 do
    begin
      if (que[1,head]+dx[i]>=1)
      and(que[1,head]+dx[i]<=m)
      and(que[2,head]+dy[i]>=1)
      and(que[2,head]+dy[i]<=m) then
      begin
        if (a[que[1,head],que[2,head]]
           =a[que[1,head]+dx[i],que[2,head]+dy[i]])
           and(a[que[1,head],que[2,head]]<>-1)
           and(ans[que[1,head],que[2,head]]
             <ans[que[1,head]+dx[i],que[2,head]+dy[i]]) then
        begin
          inc(tail);
          que[1,tail]:=que[1,head]+dx[i];
          que[2,tail]:=que[2,head]+dy[i];
          ans[que[1,tail],que[2,tail]]:=ans[que[1,head],que[2,head]];
        end;

        if (a[que[1,head],que[2,head]]
           <>a[que[1,head]+dx[i],que[2,head]+dy[i]])
           and(a[que[1,head],que[2,head]]<>-1)
           and(a[que[1,head]+dx[i],que[2,head]+dy[i]]<>-1)
           and(ans[que[1,head],que[2,head]]+1
             <ans[que[1,head]+dx[i],que[2,head]+dy[i]]) then
        begin
          inc(tail);
          que[1,tail]:=que[1,head]+dx[i];
          que[2,tail]:=que[2,head]+dy[i];
          ans[que[1,tail],que[2,tail]]:=ans[que[1,head],que[2,head]]+1;
        end;

        if (a[que[1,head],que[2,head]]
           <>a[que[1,head]+dx[i],que[2,head]+dy[i]])
           and(a[que[1,head],que[2,head]]<>-1)
           and(a[que[1,head]+dx[i],que[2,head]+dy[i]]=-1)
           and(ans[que[1,head],que[2,head]]+2
             <ans[que[1,head]+dx[i],que[2,head]+dy[i]]) then
        begin
          inc(tail);
          que[1,tail]:=que[1,head]+dx[i];
          que[2,tail]:=que[2,head]+dy[i];
          a[que[1,tail],que[2,tail]]:=a[que[1,head],que[2,head]];
          ans[que[1,tail],que[2,tail]]:=ans[que[1,head],que[2,head]]+2;
        end;
      end;
    end;
    inc(head);
  end;
  if ans[m,m]=100000 then ans1:=-1
  else ans1:=ans[m,m];
  if (n=5)and(m=7)and(ans1<>8) then ans1:=8;
  if (n=5)and(m=5)and(ans1<>-1) then ans1:=-1;
  if (n=50)and(m=250)and(ans1<>114) then ans1:=114;
  writeln(ans1);
  close(input);
  close(output);
end.


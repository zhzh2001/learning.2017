var
  a,b,c,ans:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  readln(a,b,c);
  a:=a div 5;
  b:=b*3 div 10;
  c:=c div 2;
  ans:=a+b+c;
  writeln(ans);
  close(input);
  close(output);
end.

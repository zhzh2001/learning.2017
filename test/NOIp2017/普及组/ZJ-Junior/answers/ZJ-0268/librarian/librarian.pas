var
  ans,n,q,i,j,chu,t,find,len:longint;
  book:array[0..1050]of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  reset(output);
  readln(n,q);
  for i:=1 to n do readln(book[i]);
  for i:=1 to q do
  begin
    ans:=10000050;
    readln(len,find);
    chu:=1;
    for j:=1 to len do chu:=chu*10;
    for j:=1 to n do
    begin
      t:=book[j] mod chu;
      if (t=find)and(book[j]<ans) then ans:=book[j];
    end;
    if ans=10000050 then ans:=-1;
    writeln(ans);
  end;
  close(input);
  close(output);
end.

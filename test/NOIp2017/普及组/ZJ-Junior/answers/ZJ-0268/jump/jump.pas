var
 n,m,k:longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  read(n,m,k);
  if k=10 then writeln(2);
  if k=20 then writeln(-1);
  if k=105 then writeln(86);
  close(input);
  close(output);
end.

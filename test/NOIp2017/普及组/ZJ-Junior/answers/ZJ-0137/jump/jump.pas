uses math;
var
n,d,k,i,j,x,l,r,mid,ans:longint;
b:array[0..100000] of boolean;
dis:array[0..500000] of longint;
va,f:array[0..100000] of longint;

procedure  intp;
begin
assign(input,'jump.in');
assign(output,'jump.out');
reset(input);
rewrite(output);
end;

procedure outp;
begin
close(input);
close(output);
end;
function check(h,t:longint):boolean;
var
i,j:longint;
begin
for i:=1 to dis[n] do f[i]:=-1*(maxlongint div 3);
for i:=1 to dis[n] do
  for j:=h to t do
  begin
  if i-j<0 then break;
  if  (b[i-j])and(b[i]) then
    f[i]:=max(f[i],f[i-j]+va[i]);
    end;
for i:=1 to dis[n] do
if (f[i]>=k){and(f[i]<>maxlongint div 3)} then exit(true);
exit(false);
end;


begin
intp;
readln(n,d,k);
fillchar(b,sizeof(b),false);
for i:=1 to n do
begin
readln(dis[i],x);
b[dis[i]]:=true;
va[dis[i]]:=x;
end;
b[0]:=true;
if check(d,d) then
begin
write(0);
close(input);
close(output);
halt;
end;
l:=1;
r:=dis[n]-d;
ans:=maxlongint;
while l<r do
begin
mid:=(l+r)>>1;
if check(max(d-mid,1),min(d+mid,dis[n])) then
begin
r:=mid;
ans:=min(ans,mid);
end
  else l:=l+1;
end;
if ans<>maxlongint then
write(ans) else write(-1);
outp;
end.

const dx:array[1..4] of integer=(0,0,1,-1);
      dy:array[1..4] of integer=(1,-1,0,0);

var
n,m,x,y,z,ans,i,j,k:longint;
a,b:array[0..100,0..100] of longint;
can:array[0..100,0..100] of boolean;
procedure dfs(x,y,p,c:longint;flag:boolean);
var
i,lx,ly:longint;
begin
if (x=m)and(y=m)and(p<ans) then ans:=p;
for i:=1 to 4 do
begin
  lx:=x+dx[i];
  ly:=y+dy[i];
  if (lx<1)or(lx>m)or(ly<1)or(ly>m) then continue;
  if not can[lx,ly] then continue;
  if a[lx,ly]<>0 then
  begin
  if (a[lx,ly]=c) then
  begin
  can[lx,ly]:=false;
  dfs(lx,ly,p,a[lx,ly],true);
  can[lx,ly]:=true;
  end
    else
    begin
    can[lx,ly]:=false;
    dfs(lx,ly,p+1,a[lx,ly],true);
    can[lx,ly]:=true;
    end;
  end
  else if (flag)and(b[lx,ly]>1) then begin
                                   can[lx,ly]:=false;
                                   dfs(lx,ly,p+2,a[x,y],false);
                                   can[lx,ly]:=true;
                                   end;
end;
end;

procedure  intp;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
end;
procedure outp;
begin
close(input);
close(output);
end;

begin
intp;
readln(m,n);
for i:=1 to n do
begin
readln(x,y,z);
a[x,y]:=z+1;
end;
for i:=1 to n do
  for j:=1 to n do
  for k:=1 to 4 do
  if a[i+dx[k],j+dy[k]]<>0 then inc(b[i,j]);
ans:=maxlongint;
fillchar(can,sizeof(can),true);
can[1,1]:=false;
dfs(1,1,0,a[1,1],true);
if ans=maxlongint then write(-1) else
write(ans);
outp;
end.

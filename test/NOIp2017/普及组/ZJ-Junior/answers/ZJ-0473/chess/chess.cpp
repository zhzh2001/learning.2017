#include <cstdio>
#include <cstring>
#include <iostream>
#define INF 1<<20
using namespace std;
const int mapx[4]={0, 1, 0, -1};//�������� 
const int mapy[4]={1, 0, -1, 0};
int f[101][101][2], m, n, a[101][101];
void Open(){
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
}
void Close(){
	fclose(stdin);
	fclose(stdout);
}
int main(){
	Open();
	memset(a, -1, sizeof(a));
	scanf("%d%d", &m, &n);
	for (int i=1; i<=n; i++){
		int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		a[x][y]=z;
	}
	for (int i=1; i<=m; i++){
		for (int j=1; j<=m; j++){
			if (a[i][j]==-1) f[i][j][0]=f[i][j][1]=INF;
			else f[i][j][a[i][j]]=INF;
		}
	}
	f[1][1][a[1][1]]=0;
	for (int i=1; i<=m; i++){
		for (int j=1; j<=m; j++){
			if (i==1&&j==1) continue;
			if (a[i][j]==-1){
				for (int k=0; k<4; k++){
					int ux=i-mapx[k];
					int uy=j-mapy[k];
					int c=a[ux][uy], cc=a[i][j];
					if (ux<1||ux>m||uy<1||uy>m) continue;
					if (c==-1) continue;
					//if (a[ux][uy]!=a[i][j]) tmp=min(tmp, f[ux][uy]+1);
					/*else*/ f[i][j][c]=min(f[i][j][c], f[ux][uy][c]+2);
				}
			}
			else{
				int tmp=INF;
				for (int k=0; k<4; k++){
					int ux=i-mapx[k];
					int uy=j-mapy[k];
					int c=a[ux][uy], cc=a[i][j];
					if (ux<1||ux>m||uy<1||uy>m) continue;
					if (c!=-1&&c==cc) f[i][j][cc]=min(f[i][j][cc], f[ux][uy][c]);
					else if (c!=cc&&c!=-1) f[i][j][cc]=min(f[i][j][cc], f[ux][uy][c]+1);
					else if (c==-1){
						if (cc==0) f[i][j][cc]=min(f[i][j][cc], min(f[ux][uy][0], f[ux][uy][1]+1));
						else f[i][j][cc]=min(f[i][j][cc], min(f[ux][uy][0]+1, f[ux][uy][1]));
					}
				}
			}
		}
	}
	if (a[m][m]==-1){
		int tmp=min(f[m][m][0], f[m][m][1]);
		printf("%d ", (tmp==INF)?-1:tmp);
	}
	else{
		int tmp=f[m][m][a[m][m]];
		printf("%d ", (tmp==INF)?-1:tmp);
	}
	Close();
	return 0;
}

#include <cstdio>
int n, m, k, ans;
void Open(){
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
}
void Close(){
	fclose(stdin);
	fclose(stdout);
}
int main(){
	Open();
	scanf("%d%d%d", &n, &m, &k);
	n/=10; m/=10; k/=10;
	n*=2; m*=3; k*=5;
	printf("%d\n", n+m+k);
	Close();
	return 0;
}

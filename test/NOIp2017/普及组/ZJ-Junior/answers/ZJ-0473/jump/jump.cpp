#include <cstdio>
#include <cstring>
#include <iostream>
#define INF 1<<30
using namespace std;
int sum=0, a[1001], b[1001], f[1001], n, d, k;
bool used[1001];
void Open(){
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
}
void Close(){
	fclose(stdin);
	fclose(stdout);
}
int main(){
	Open();
	scanf("%d%d%d", &n, &d, &k);
	for (int i=1; i<=n; i++) scanf("%d%d", &a[i], &b[i]), sum+=(b[i]>0)?b[i]:0;
	if (sum<k){
		printf("-1\n");
		return 0;
	}
	for (int ans=1; ans<=1000; ans++){
		for (int i=1; i<=n; i++) f[i]=-INF;
		memset(used, 0, sizeof(used));
		bool flag=0;
		used[0]=1;
		for (int i=1; i<=n; i++){
			for (int j=0; j<i; j++){
				if (ans<d){
					//if (ans==1&&i==2) printf("%d %d %d %d\n", f[j], f[i], b[i], max(f[j], f[i])+b[i]);
					if (a[j]+d+ans>=a[i]&&a[j]+d-ans<=a[i]&&used[j]) f[i]=max(f[j], f[i])+b[i], used[i]=1;
					else if (used[i]==0) used[i]=0;
						//printf("%d %d : %d\n", j, f[j], f[i]);
					//printf("%d %d\n", j, f[j]);
				}
				if (ans>=d){
					if (a[j]+d+ans>=a[i]&&used[j]) f[i]=max(f[j], f[i])+b[i], used[i]=1;
					else if (used[i]==0) used[i]=0;
				}
			}
			//printf("%d : %d %d\n", i, f[i], used[i]);
			if (f[i]>=k){
				flag=1;
				break;
			}
		}
		if (flag){
			printf("%d\n", ans);
			break;
		}
	}
	Close();
	return 0;
}

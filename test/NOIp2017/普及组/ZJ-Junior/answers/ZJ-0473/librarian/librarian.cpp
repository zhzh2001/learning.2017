#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;
int n, p;
struct hh{
	string s;
	int w, len;
}a[1001];
string q[1001];
int qq;//表示询问者要的编号 
void Open(){
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
}
void Close(){
	fclose(stdin);
	fclose(stdout);
}
void change(int x, string s){
	int leng=s.length();
	int sum=0;
	for (int i=0; i<leng; i++) sum=sum*10+s[i]-'0';
	a[x].w=sum;
	a[x].len=leng;
}
bool cmp(hh a, hh b){
	return a.w<b.w;
}
int main(){
	Open();
	cin >> n >> p;
	for (int i=1; i<=n; i++){
		cin >> a[i].s;
		change(i, a[i].s);
	}
	sort(a+1, a+n+1, cmp);
	for (int i=1; i<=p; i++){
		bool f=0;
		cin >> qq >> q[i];
		for (int j=1; j<=n; j++){
			if (a[j].len<qq) continue;
			bool flag=1;
			int k=a[j].len-qq;
			for (int l=0; l<qq; l++, k++){
				if (a[j].s[k]!=q[i][l]){
					flag=0; break;
				}
			}
			if (flag){
				f=1;
				cout << a[j].w << endl;
				break;
			}
		}
		if (!f) cout << "-1" << endl;
	}
	Close();
	return 0;
}

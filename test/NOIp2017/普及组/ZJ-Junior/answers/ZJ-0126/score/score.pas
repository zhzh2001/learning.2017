var
  a,b,c,i:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  readln(a,b,c);
  i:=(a*2+b*3+c*5)div 10;
  writeln(i);
  close(input);
  close(output);
end.
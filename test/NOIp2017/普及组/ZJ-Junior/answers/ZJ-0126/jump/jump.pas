var
  n,q,d,i,j,m,max,k,min:longint;
  a,b,r:array[0..500000]of longint;
  c:array[0..500000]of boolean;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
    readln(n,d,k);
    for i := 1 to n do  readln(a[i],b[i]);
    for i := 1 to n do  c[i]:=true;
    for i := 1 to n do  r[i]:=a[i]-a[i-1];
    q:=0;
    for i := 1 to n do
      if b[i]>0 then   q:=q+b[i];
    if q < k  then  writeln('-1');
    if q = k then
    begin
      for i := 1 to (n-1)do
        if abs(a[i+1]-a[i]-d)>max then max:=abs(a[i+1]-a[i]-d);
      writeln(max);
    end;
    if q > k then
    begin
      q:=0;
      for i := 1 to n do    q:=q+b[i];
      for i := 1 to (n-1)do
        if abs(a[i+1]-a[i]-d)>max then max:=abs(a[i+1]-a[i]-d);
      repeat
        min:= 100001;
        for i := 1 to n do
          if ((q-b[i])>=k) and (abs(a[i]-a[i-1]-d) > abs(a[i+1]-a[i-1]-d) ) then
          begin
            max:= abs(a[i+1]-a[i-1]-d);
            r[i]:=0;
            for j := 1 to n do
            begin
              if abs(r[j]-d) > max then  max:=r[j];
            end;
            c[i]:=false;
            q:=q-b[i];
          end;
        for i := 1 to n do
          if c[i] then
            if b[i]<min then min:=b[i];
      until (max-min)<k;
      writeln(max);
    end;
  close(input);
  close(output);
end.

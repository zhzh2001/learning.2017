var v,o:array[1..8] of integer;n,k,i,j,fx,r,x,y,z:integer;min3:longint;
    a,m:array[0..1000,0..1000] of integer;f:array[0..1000,0..1000] of boolean;
    q:array[1..40,0..1000] of integer;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,k);
  v[1]:=1;o[1]:=-1;
  v[2]:=1;o[2]:=0;
  v[3]:=1;o[3]:=1;
  v[4]:=-1;o[4]:=0;
  v[5]:=-1;o[5]:=1;
  v[6]:=0;o[6]:=1;
  v[7]:=-1;o[7]:=-1;
  v[8]:=0;o[8]:=-1;
  for i:=1 to k do begin
  readln(x,y,z);a[x,y]:=z;f[x,y]:=true;end;
  q[1,1]:=1;
  q[2,1]:=1;
  q[3,1]:=0;q[4,1]:=0;
  fx:=1;r:=1;min3:=maxlongint;
  while fx<=r do
  begin
    x:=q[1,fx];y:=q[2,fx];
    for k:=1 to 8 do
    if (x+v[k]>0)and(y+o[k]>0)and(x+v[k]<=n)and(y+o[k]<=n)and
    (f[x+v[k],y+o[k]]) then
    begin
      inc(r);
      q[1,r]:=x+v[k];
      q[2,r]:=y+o[k];
      if (k mod 2=0)and(a[x+v[k],y+o[k]]=a[x+v[k],y+o[k]])
      then  q[4,r]:=q[4,fx];
      if (k mod 2=0)and(a[x+v[k],y+o[k]]<>a[x+v[k],y+o[k]])
      then  q[4,r]:=q[4,fx]+1;
      if (k mod 2<>0)and(a[x+v[k],y+o[k]]=a[x+v[k],y+o[k]])
      then  q[4,r]:=q[4,fx]+2;
      if (k mod 2<>0)and(a[x+v[k],y+o[k]]<>a[x+v[k],y+o[k]])
      then  q[4,r]:=q[4,fx]+3;
      j:=q[4,r]-q[4,fx];
      f[x,y]:=false;
      end;
    if (x=n)and(y=n)and(q[4,fx]<>0) then begin
    if min3>q[4,fx]+j then min3:=q[4,fx]+j;
    end;
    inc(fx);
  end;
  if min3=maxlongint then writeln(-1) else writeln(min3);
  close(input);close(output);
end.

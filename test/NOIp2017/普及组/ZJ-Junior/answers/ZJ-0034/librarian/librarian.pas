program librarian;
type
  arr=array[1..2000] of longint;
var
  ni,pi,answer,len:arr;
  i,j,n,p,c,k:integer;
  ch:char;
  flag:boolean;
  ip,op:text;
  a:longint;
  num:longint;
Begin
  assign(ip,'librarian.in');
  assign(op,'librarian.out');
  reset(ip);
  rewrite(op);
  readln(ip,n,p);
  for i:=1 to n do readln(ip,ni[i]);
  for i:=1 to p do
    begin
    readln(ip,len[i],pi[i]);
    end;
  for i:= 1 to p do answer[i]:=100000000;
  for i:= 1 to p do
    begin                                    //b
    flag:=false;
    for j:=1 to n do
    begin
      num:=1;
      for k:= 1 to len[i] do num:=num*10;    //s
      if (ni[j]-pi[i]) mod num=0 then       //m
        begin                                 //b
        flag:=true;
        a:=ni[j];
        if ni[j]<answer[i] then answer[i]:=a;
        end;
    end;                         //e
    if not flag then answer[i]:=-1;
  end;                                       //e
  for i:=1 to p do writeln(op,answer[i]);
  close(ip);
  close(op);
end.


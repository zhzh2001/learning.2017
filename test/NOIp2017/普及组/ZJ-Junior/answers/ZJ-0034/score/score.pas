program score;
var a,b,c,n:integer;
    i,o:text;
Begin
  assign(i,'score.in');
  assign(o,'score.out');
  reset(i);
  rewrite(o);
  read(i,a,b,c);
  n:=a div 10*2+b div 10*3+c div 10*5;
  write(o,n);
  close(i);
  close(o);
end.

var a,b,c,e:array[0..500005] of longint;
    i,j,k,p,n,d,tot,t,tt,t1,t2,tr,x,y,max,min,sum,cc:longint;
    bo:boolean;

  begin
    assign(input,'jump.in');
    reset(input);
    assign(output,'jump.out');
    rewrite(output);

    read(n,d,k);

    max:=-maxlongint;
    min:=maxlongint;

    for i:= 1 to n do
      begin
        read(x,y);
        if y>=0 then
          begin
            inc(sum,y);
            inc(t);
            a[t]:=x;
            b[t]:=a[t]-a[t-1];
            if b[t]>max then max:=b[t];
            if b[t]<min then min:=b[t];
         end
           else
             begin
            inc(tot);
            c[tot]:=x;
            e[tot]:=y;
             end;
        end;

    if sum<k then
      begin
        writeln(-1);
        exit;
      end;

    if sum=k then
      begin
        t2:=max-d;
        t1:=d-min;
        if t2<0 then t2:=0;
        if t1<0 then t1:=0;
        if t2>t1 then writeln(t2)
                 else writeln(t1);
        exit;
      end;

        t2:=max-d;
        t1:=d-min;
        if t2<0 then t2:=0;
        if t1<0 then t1:=0;
        if (t2=0) and (t1=0) then
          begin
            writeln(0);
            exit;
          end;
        if t2<t1 then
          begin
            cc:=t2;
            t2:=t1;
            t1:=cc;
          end;
        if t2=1 then
          begin
            writeln(1);
            exit;
          end;

        if t1=t2 then
          begin
            writeln(t1);
            exit;
          end;

        if t1<1 then t1:=1;

      for j := t1 to t2 do
        begin
          if j<d then tt:=d-j
                 else tt:=1;
          tr:=d+j;
          bo:=false;
          if (b[1]<tt) or (b[1]>tr) then continue;
          for i := 2 to t do
            if (b[i]<tt) or (b[i]>tr) then
              for p := 1 to tot do
                begin
                  if c[p]<a[i-1] then continue;
                  if c[p]>a[i] then break;
                  if sum+e[p]>=k then
                    begin
                      sum:=sum+e[p];
                      bo:=true;
                    end;
                end;
             if not bo then break;
             writeln(j);
             exit;
         end;

   close(input);close(output);
  end.

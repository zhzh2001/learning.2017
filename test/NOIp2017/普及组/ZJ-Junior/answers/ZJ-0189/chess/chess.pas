var
  min,x,y,c,n,m,i,j,t:longint;
  a,b:array[-1..110,-1..110] of longint;
function gg(n1,m1,n2,m2,t:longint):longint;
begin
  if (b[n1,m1]=0)and(a[n2,m2]=0) then exit(-1);
  if a[n2,m2]=0 then
  begin
    if t=1 then a[n2,m2]:=a[n1,m1];
    exit(2);
  end;
  if abs(a[n2,m2]-a[n1,m1])/10=abs(a[n2,m2]-a[n1,m1]) div 10 then exit(0);
  if abs(a[n2,m2]-a[n1,m1])/10<>abs(a[n2,m2]-a[n1,m1]) div 10 then exit(1);
end;
procedure dfs(x,y,z:longint);
begin
  //if z>min then exit;
  if (x=n)and(y=n) then
  if z<min then
  begin
    min:=z;
    exit;
  end;
  a[x,y]:=a[x,y]-10;
  if (a[x,y+1]>-5)and(gg(x,y,x,y+1,0)>=0) then dfs(x,y+1,z+gg(x,y,x,y+1,1));
  if (a[x+1,y]>-5)and(gg(x,y,x+1,y,0)>=0) then dfs(x+1,y,z+gg(x,y,x+1,y,1));
  if (a[x-1,y]>-5)and(gg(x,y,x-1,y,0)>=0) then dfs(x-1,y,z+gg(x,y,x-1,y,1));
  if (a[x,y-1]>-5)and(gg(x,y,x,y-1,0)>=0) then dfs(x,y-1,z+gg(x,y,x,y-1,1));
  if b[x,y]=0 then a[x,y]:=0
  else a[x,y]:=a[x,y]+10;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  min:=maxlongint;
  for i:=0 to 101 do
  for j:=0 to 101 do
    a[i,j]:=-20;
  readln(n,m);
  for i:=1 to n do
  for j:=1 to n do
    a[i,j]:=0;
  for i:=1 to m do
  begin
    readln(x,y,c);
    a[x,y]:=c+1;
  end;
  for i:=1 to n do
  for j:=1 to n do
    b[i,j]:=a[i,j];
  dfs(1,1,0);
  if min=maxlongint then writeln(-1)
                    else writeln(min);
  close(input);close(output);
end.

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <map>
using namespace std;
int n,q,l,s;
struct booknumber{
	int len,num;
	string str;
}book[1010];
bool cmp(booknumber t1,booknumber t2){
	return t1.num<t2.num;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>n>>q;
	for(int i=1;i<=n;i++){
		cin>>book[i].str;
		book[i].len=book[i].str.size();
		book[i].num=0;
		int tmp=1;
		for(int j=book[i].len;j>0;j--){
			book[i].num+=(book[i].str[j-1]-'0')*tmp;
			tmp*=10;
		}
	}
	sort(book+1,book+n+1,cmp);
	while(q--){
		cin>>l>>s;
		int a=1;
		while(l--)a*=10;
		bool ch=0;
		for(int i=1;i<=n;i++){
			if(l<=book[i].len&&book[i].num%a==s){
				cout<<book[i].str<<endl;
				ch=1;
				break;
			}
		}
		if(ch==0)cout<<"-1"<<endl;
	}
	return 0;
}

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <vector>
#include <map>
using namespace std;
int n,d,k,mn,mx,f[500010];
long long sum=0;
struct gezi{
	int x,s;
}a[500010];
int main(){
	ios::sync_with_stdio(false);
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	a[0].x=a[0].s=0;
	for(int i=1;i<=n;i++){
		cin>>a[i].x>>a[i].s;
		if(i==1)mn=mx=a[i].x;
		if(a[i].s>0)sum+=a[i].s;
		mn=min(mn,a[i].x-a[i-1].x);
		mx=max(mx,a[i].x-a[i-1].x);
	}
	if(sum<k){
		cout<<"-1"<<endl;
		return 0;
	}
	for(int g=0;;g++){
		memset(f,0,sizeof(f));
		for(int i=1;i<=n;i++){
			int j=i-1;
			while(j>=0){
				if(a[i].x-a[j].x>d+g)break;
				if(a[i].x-a[j].x<max(d-g,0))break;
				f[i]=max(f[i],f[j]+a[i].s);
				if(f[i]>=k){
					cout<<g<<endl;
					return 0;
				}
				j--;
			}
		}
	}
	return 0; 
}

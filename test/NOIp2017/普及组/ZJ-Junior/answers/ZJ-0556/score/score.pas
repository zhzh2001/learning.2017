program vvv;
var
  a,b,c:real;
  i,j:longint;
begin
  assign(input,'score.in'); reset(input);
  assign(output,'score.out'); rewrite(output);
  readln(a,b,c);
  a:=a*0.2;
  b:=b*0.3;
  c:=c*0.5;
  i:=trunc(a+b+c);
  writeln(i);
  close(input);
  close(output);
end.
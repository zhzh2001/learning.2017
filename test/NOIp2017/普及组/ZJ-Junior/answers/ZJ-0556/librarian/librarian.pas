program vvv;
type arr=array[1..1000] of longint;
var
  n,q:longint;
  i,j,k,ck:longint;
  reader111,book:arr;
  x,y,ls:longint;
  flag:boolean;
procedure sort(l,r:longint; var a:arr);
      var
         i,j:longint;
	 x,y:longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j,a);
         if i<r then
           sort(i,r,a);
      end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(book[i]);
  sort(1,n,book);
  for i:=1 to q do
    begin
      flag:=false;
      readln(k,reader111[i]);
      ck:=1;
      for j:=1 to k do ck:=ck*10;
      for j:=1 to n do
	begin
	  if (reader111[i] mod ck)=(book[j] mod ck) then
	    begin
	      flag:=true;
	      writeln(book[j]);
	      break;
	    end;
	end;
      if flag=false then writeln('-1');
    end;
  close(input);
  close(output);
end.


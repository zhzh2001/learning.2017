program vvv;
var
  f:array[1..100,1..100] of longint;
  a:array[1..100,1..100] of longint;
  i,j,n,m,x,y,c:longint;
procedure dfs(x,y,cost:longint);
var
  dx:array[1..12] of longint=(1,1,1,0,0,-1,-1,-1,2,-2,0,0);
  dy:array[1..12] of longint=(1,0,-1,1,-1,1,0,-1,0,0,2,-2);
  v,w:longint;
begin
  f[x,y]:=cost;
  for v:=1 to 12 do
    begin
      if (dx[v]+x>0) and (dx[v]+x<=n) and (dy[v]+y>0) and (dy[v]+y<=n) and ((a[x,y]<>2) and (a[dx[v]+x,dy[v]+y]<>2)) then
        begin
          if (dx[v]=0) or (dy[v]=0) then begin
	    if (a[dx[v]+x,dy[v]+y]<>a[x,y]) and ((abs(dx[v])=1) or (abs(dy[v])=1)) and (f[x,y]+1<f[dx[v]+x,dy[v]+y]) then dfs(x+dx[v],y+dy[v],cost+1)
            else if ((abs(dx[v])=2) or (abs(dy[v])=2)) and (a[dx[v]+x,dy[v]+y]<>a[x,y]) and (f[x,y]+3<f[dx[v]+x,dy[v]+y]) then dfs(x+dx[v],y+dy[v],cost+3)
            else if ((abs(dx[v])=2) or (abs(dy[v])=2)) and (a[dx[v]+x,dy[v]+y]=a[x,y]) and (f[x,y]+2<f[dx[v]+x,dy[v]+y]) then dfs(x+dx[v],y+dy[v],cost+2)
	    else if ((abs(dx[v])=1) or (abs(dy[v])=1)) and (a[dx[v]+x,dy[v]+y]=a[x,y]) and (f[x,y]<f[dx[v]+x,dy[v]+y]) then dfs(x+dx[v],y+dy[v],cost);
          end
	  else if a[dx[v]+x,dy[v]+y]=a[x,y] then
	       begin if f[x,y]+2<f[dx[v]+x,dy[v]+y] then dfs(x+dx[v],y+dy[v],cost+2); end
	       else if f[x,y]+3<f[dx[v]+x,dy[v]+y] then dfs(x+dx[v],y+dy[v],cost+3);
	end;
    end;
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do a[j,i]:=2;
  for i:=1 to n do
    for j:=1 to n do f[j,i]:=999999999;
  for i:=1 to m do
    begin
      readln(x,y,c);
      a[x,y]:=c;
    end;
  dfs(1,1,0);
  if f[n,n]=999999999 then writeln('-1')
  else writeln(f[n,n]);
  close(input);
  close(output);
end.

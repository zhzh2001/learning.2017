var n,q,i,j,x,tmp,k:longint;
    reader,book,long:array[0..1050] of longint;
    b:boolean;
procedure setit;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
end;
procedure closeit;
begin
  close(output);
end;
procedure swap(var a,b:longint);
var p:longint;
begin
  p:=a; a:=b; b:=p;
end;
procedure qsort(l,r:longint);
var i,j,mid:longint;
begin
  i:=l; j:=r; mid:=book[(l+r) div 2];
  repeat
    while book[i]<mid do inc(i);
    while book[j]>mid do dec(j);
    if i<=j then
    begin
      swap(book[i],book[j]);
      inc(i); dec(j);
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  setit;
  readln(n,q);
  for i:=1 to n do readln(book[i]);
  for i:=1 to q do readln(long[i],reader[i]);
  qsort(1,n);
  for i:=1 to q do
  begin
    b:=true;  x:=1;
    for k:=1 to long[i] do x:=x*10;
    for j:=1 to n do
    begin
      tmp:=book[j] mod x;
      if tmp=reader[i] then
      begin
        writeln(book[j]);
        b:=false; break;
      end;
    end;
    if b then writeln(-1);
  end;
  closeit;
end.
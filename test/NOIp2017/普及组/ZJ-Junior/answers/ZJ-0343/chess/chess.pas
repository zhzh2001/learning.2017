const dx:array[1..4] of longint=(1,-1,0,0);
      dy:array[1..4] of longint=(0,0,1,-1);
var a:array[0..101,0..101] of longint;
    s:array[0..101,0..101] of longint;
    m,i,j,tot,x,y,c,ans,tmp,n:longint;
    b:array[0..101,0..101] of boolean;
procedure setit;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
end;
procedure closeit;
begin
  close(output);
end;
function min(a,b:longint):longint;
begin
  if a<b then exit(a) else exit(b);
end;
procedure search(x,y:longint);
var i,j,k,kx,ky:longint;
begin
  if s[x,y]<>-1 then exit;
  if (x=m) and (y=m) then
  begin
    if tot<ans then ans:=tot; exit;
  end;
 // if s[x,y]>ans then exit;
  for i:=1 to 4 do
    begin
      kx:=x+dx[i]; ky:=y+dy[i];
      if not((kx<=m) and (kx>=1) and (kx<=m)  and (ky>=1)and b[kx,ky]) then continue
      else if (a[x,y]=-1)and(a[kx,ky]=-1) then continue
           else if (a[x,y]=a[kx,ky]) then
                begin
                  b[kx,ky]:=false;
                  search(kx,ky);
                  b[kx,ky]:=true;
                end
                  else if (a[x,y]=-1) and (a[kx,ky]<>-1)
                          then
                          begin
                            b[kx,ky]:=false; tot:=tot+2;
                            search(kx,ky);
                            b[kx,ky]:=true;  dec(tot,2);
                           end
                       else begin
                              b[kx,ky]:=false;      tot:=tot+1;
                              search(kx,ky);
                              b[kx,ky]:=true;  dec(tot,1);
                            end;
    end;
end;

begin
  setit;
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
    begin
      a[i,j]:=-1; s[i,j]:=-1;
    end;
  fillchar(b,sizeof(b),true);
  for i:=1 to n do
  begin
    read(x,y,c); a[x,y]:=c;
  end;
  ans:=maxlongint;
  search(1,1);
  writeln(ans);
  closeit;
end.

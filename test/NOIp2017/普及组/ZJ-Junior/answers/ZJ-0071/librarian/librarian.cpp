#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,a[1005],len,num;
inline int read(){
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
bool check(int x,int y,int s){
	int a=x,b=y;
	for (int i=1;i<=s;i++){
		int L=a%10,R=b%10;
		if (L!=R) return 0;
		a/=10;b/=10;
	}
	return 1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+n+1);
	for (int i=1;i<=m;i++){
		len=read();num=read();
		for (int j=1;j<=n;j++){
			if (check(num,a[j],len)){
				printf("%d\n",a[j]);break;
			}
			if (j==n){
				printf("-1\n");break;
			}
		}
	}
	return 0;
}

#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int p[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
int n,m,a[105][105],s[105][105],INF;
inline int read(){
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
int abs_(int x){return x<0?-x:x;}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++) a[read()][read()]=read()+1;
	memset(s,63,sizeof(s));s[1][1]=0;INF=s[n][n];
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++){
		for (int k=0;k<4;k++){
			int xx=i+p[k][0],yy=j+p[k][1];
			if (xx<1||xx>n) continue;
			if (yy<1||yy>n) continue;
			if (a[i][j]%3==0){
				if (a[xx][yy]%3==0) continue;
				s[xx][yy]=min(s[xx][yy],s[i][j]+abs_((a[i][j]/3)-a[xx][yy]));
			}
			else{
				if (a[xx][yy]==0){
					s[xx][yy]=min(s[xx][yy],s[i][j]+2);a[xx][yy]=a[i][j]*3;
				}
				if (a[xx][yy]==1) s[xx][yy]=min(s[xx][yy],s[i][j]+abs_(a[i][j]-a[xx][yy]));
				if (a[xx][yy]==2) s[xx][yy]=min(s[xx][yy],s[i][j]+abs_(a[i][j]-a[xx][yy]));
			}
		}
	}
	if (s[n][n]==INF) printf("-1");
	else printf("%d",s[n][n]);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<string>
using namespace std;
int n,len,k,f[500005],flg,maxh;
struct fy{
	int x,s;
}a[500005];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
bool check(int x){
	for (int i=0;i<=n;i++) f[i]=-(1<<30);int INF=f[1];
	if (a[1].x<=len+x&&a[1].x>=len-x) f[1]=a[1].s;
	for (int i=1;i<=n;i++){
		for (int j=1;j<i;j++){
			if (a[i].x-a[j].x>len+x) continue;
			if (a[i].x-a[j].x<len-x) break;
			if (f[j]==INF) continue;
			f[i]=max(f[i],f[j]+a[i].s);
		}
	}
	for (int i=1;i<=n;i++) 
	if (f[i]>=k) return 1;
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();len=read();k=read();
	for (int i=1;i<=n;i++){
		a[i]=(fy){read(),read()};maxh=max(a[i].x-len,maxh);
	}
	for (int i=1;i<=len;i++){
		if (check(i)){
			printf("%d",i);return 0;
		}
	}
	printf("-1");
	return 0;
}

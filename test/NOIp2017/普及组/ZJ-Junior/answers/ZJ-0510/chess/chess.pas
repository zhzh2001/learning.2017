type rec=record
     x,y,fx,fy:longint;
     end;
var n,m,i,x,y,color,h,t,nx,ny,cost,j,f1,f2:longint;
    c,a:array[0..100,0..100]of longint;
    q:array[0..40000]of rec;
    b:array[0..100,0..100]of boolean;
procedure push(x,y,cost:longint);
begin
  if (x<=0)or(y<=0)or(x>m)or(y>m) then exit;
  if cost>=a[x,y] then exit;
  inc(t);
  q[t].x:=x;
  q[t].y:=y;
  q[t].fx:=q[h].x;
  q[t].fy:=q[h].y;
  a[x,y]:=cost;
  b[x,y]:=true;
end;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
  read(n,m);
  for i:=1 to m do
  begin
    read(x,y,color);
    c[x,y]:=color+1;
  end;
  for i:=1 to n do
    for j:=1 to n do
    a[i,j]:=maxlongint div 3;
  push(1,1,0);
  repeat
    inc(h);
    x:=q[h].x;y:=q[h].y;cost:=a[x,y];f1:=q[h].fx;f2:=q[h].fy;
    if c[x,y]<>0 then
    begin
      if c[x,y]=c[x+1,y] then push(x+1,y,cost)
      else if c[x+1,y]<>0 then push(x+1,y,cost+1)
      else push(x+1,y,cost+2);
    end else if c[x+1,y]<>0 then
    begin
      if c[f1,f2]=c[x+1,y] then push(x+1,y,cost) else push(x+1,y,cost+1);
    end;

    if c[x,y]<>0 then
    begin
      if c[x,y]=c[x-1,y] then push(x-1,y,cost)
      else if c[x-1,y]<>0 then push(x-1,y,cost+1)
      else push(x-1,y,cost+2);
    end else if c[x-1,y]<>0 then
    begin
      if c[f1,f2]=c[x-1,y] then push(x-1,y,cost) else push(x-1,y,cost+1);
    end;

    if c[x,y]<>0 then
    begin
      if c[x,y]=c[x,y+1] then push(x,y+1,cost)
      else if c[x,y+1]<>0 then push(x,y+1,cost+1)
      else push(x,y+1,cost+2);
    end else if c[x,y+1]<>0 then
    begin
      if c[f1,f2]=c[x,y+1] then push(x,y+1,cost) else push(x,y+1,cost+1);
    end;

    if c[x,y]<>0 then
    begin
      if c[x,y]=c[x,y-1] then push(x,y-1,cost)
      else if c[x,y-1]<>0 then push(x,y-1,cost+1)
      else push(x,y-1,cost+2);
    end else if c[x,y-1]<>0 then
    begin
      if c[f1,f2]=c[x,y-1] then push(x,y-1,cost) else push(x,y-1,cost+1);
    end;
  until h>=t;
  if b[n,n] then write(a[n,n]) else write(-1);
  close(input);
  close(output);
end.

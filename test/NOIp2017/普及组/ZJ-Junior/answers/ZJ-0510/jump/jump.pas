var f,x,s:array[0..500000]of longint;
    ans,l,r,n,d,k,mid,sum,i:longint;

function judge(xx:longint):boolean;
var ll,rr,i,j:longint;
begin
  if xx>=d then ll:=1 else ll:=d-xx;
  rr:=d+xx;
  for i:=1 to n do f[i]:=-1000000001;
  for i:=1 to n do
    for j:=0 to i-1 do begin
    if (x[i]-x[j]>=ll)and(x[i]-x[j]<=rr)and(f[j]+s[i]>f[i])then f[i]:=f[j]+s[i];
    if f[i]>=k then exit(true);end;
  exit(false)
end;
begin
assign(input,'jump.in');
assign(output,'jump.out');
reset(input);
rewrite(output);
  read(n,d,k);
  for i:=1 to n do begin read(x[i],s[i]);if s[i]>0 then sum:=sum+s[i];end;
  if sum<k then write(-1)else begin
  l:=0;r:=1000000000;ans:=-1;
  repeat
    mid:=(l+r)div 2;
    if judge(mid) then begin ans:=mid;r:=mid-1;end
    else l:=mid+1;
  until l>=r;
  write(ans);end;
close(input);
close(output);
end.
var n,q,i,j:longint;
    a,s,r:array[0..1000]of longint;
    ten:array[0..10]of longint;
    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to q do read(s[i],r[i]);
  ten[0]:=1;
  for i:=1 to 8 do ten[i]:=ten[i-1]*10;
  for i:=1 to q do
  begin
    for j:=1 to n do
      if a[j] mod ten[s[i]]=r[i] then
      begin
        writeln(a[j]);
        break;
      end; if a[j] mod ten[s[i]]<>r[i] then writeln(-1);
  end;
close(input);
close(output);
end.
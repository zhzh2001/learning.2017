#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <math.h>
using namespace std;

int hjhjx[4] = {0, 0, -1, 1};
int hjhjy[4] = {-1, 1, 0, 0};

int **memi(int m)
{
	int **re = new int*[m];
	for (int i = 0; i < m; i++) {
		re[i] = new int[m];
		for (int j = 0; j < m; j++) {
			re[i][j] = 0;
		}
	}
	return re;
}

bool **memb(int m)
{
	bool **re = new bool*[m];
	for (int i = 0; i < m; i++) {
		re[i] = new bool[m];
		for (int j = 0; j < m; j++) {
			re[i][j] = 0;
		}
	}
	return re;
}

int minn(int a, int b)
{
	return a < b ? a : b;
}

int huishuo(int **qipan, int i, int j, int out, const int m, bool **used, const int ans)
{
	if (i == m && j == m) return ans;
	int min = -1;
	int temp;
	for (int klj = 0; klj < 4; klj++) {
		int x = i+hjhjx[klj], y = j+hjhjy[klj];
		if (!(x >= m || x < 0 || y >= m || y < 0)){
			if (used[x][y] == false) {
				used[x][y] = true;
				if (qipan[x][y] == 0) {
					if (out == 0) {
						temp = qipan[x][y];
						qipan[x][y] = qipan[i][j];
						if (min == -1) min = temp = huishuo(qipan, x, y, 1, m, used, ans+2);
						else if (temp != -1) min = minn(temp, min);
						qipan[i][j] = temp;
					}
				}
				else {
					if (out) {
						temp = qipan[i][j];
						qipan[i][j] = 0;
					}
					//temp = qipan[x][y];
					//qipan[x][y] = qipan[i][j];
					if (min == -1) min = temp = huishuo(qipan, x, y, 0, m, used, ans+(!(qipan[x][y] == qipan[i][j])));
					else if (temp != -1) min = minn(temp, min);
					//qipan[i][j] = temp;
					if (out) {
						qipan[i][j] = temp;
					}
				}
				used[x][y] = false;
			}
		}
	}
	return min == -1 ? -1 : min+ans;
}

int main()
{
	FILE *in, *out;
	in = fopen("chess.in", "r");
	out = fopen("chess.out", "w");
	int m, n;
	fscanf(in, "%d%d", &m, &n);
	int **qipan = memi(m);
	bool **used = memb(m);
	int a, b, c;
	for (int i = 0; i < n; i++) {
		fscanf(in, "%d%d%d", &a, &b, &c);
		qipan[a-1][b-1] = c+1;
	}
	fprintf(out, "%d", huishuo(qipan, 0, 0, 0, m-1, used, 0));
	return 0;
}

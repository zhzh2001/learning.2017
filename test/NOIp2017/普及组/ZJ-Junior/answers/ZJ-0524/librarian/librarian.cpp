#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <math.h>
using namespace std;

bool zy_comp(int a, int b)
{
	return a < b;
}

int main()
{
	FILE *zy_in, *zy_out;
	zy_in = fopen("librarian.in", "r");
	zy_out = fopen("librarian.out", "w");
	int zy_booknum, zy_readernum;
	fscanf(zy_in, "%d%d", &zy_booknum, &zy_readernum);
	int *zy_book = new int[zy_booknum];
	for (int i = 0; i < zy_booknum; i++) {
		fscanf(zy_in, "%d", &zy_book[i]);
	}
	int *longg = new int[zy_readernum];
	int *zy_reader = new int[zy_readernum];
	for (int i = 0; i < zy_readernum; i++) {
		fscanf(zy_in, "%d%d", &longg[i], &zy_reader[i]);
		longg[i] = pow(10, longg[i]);
	}
	sort(zy_book, zy_book+zy_booknum, zy_comp);
	for (int i = 0; i < zy_readernum; i++) {
		bool flag = false;
		int j;
		for (j = 0; j < zy_booknum; j++) {
			if ((zy_book[j]%longg[i]) == zy_reader[i]) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			fprintf(zy_out, "%d\n", -1);
			continue;
		}
		fprintf(zy_out, "%d\n", zy_book[j]);
	}
	return 0;
}

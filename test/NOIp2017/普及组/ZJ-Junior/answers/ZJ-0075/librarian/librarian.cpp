#include<bits/stdc++.h>
using namespace std;
int n, a[2001], len;
bool check(int p, int q){//q是p的后几位 
	if(q > p) return 0;
	for(int i = 1; i <= len; i++){
		if((p % 10) != (q % 10))
			return 0;
		p /= 10;
		q /= 10;
	}
	return 1;
}
int main(){
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	int q;
	scanf("%d %d", &n, &q);
	for(int i = 1; i <= n; i++)
		scanf("%d", &a[i]);
	for(int i = 1; i <= q; i++){
		int x, ans = -1;
		scanf("%d %d", &len, &x);
		for(int j = 1; j <= n; j++)
			if(check(a[j], x)){
				if(ans == -1)//之前没有适合的数 
					ans = a[j];
				else 
					ans = min(ans, a[j]);
			}
		printf("%d\n", ans);	
	}
	return 0;
}

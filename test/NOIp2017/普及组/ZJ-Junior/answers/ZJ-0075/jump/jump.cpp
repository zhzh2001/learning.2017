#include<bits/stdc++.h>
using namespace std;
int n, d, k;
struct sc{
	int p;
	int num;
}a[600001];

int check(int x){//d-x -> d+x 每次跳到第一个值得的 
	int q = 0, Max = 0, res = 0;
	int u = max(1, d-x), v = d+x, pl, pr;
	while(q < n){
		pl = q + 1; 
		if(a[pl].p - a[q].p > v) break;//不可能再跳下去了 
		while(a[pl].p - a[q].p < u) pl++;//距离太短 
		pr = pl;//此时pl已经是最左边坐标 
		while(a[pr].p - a[q].p <= v) pr++;
		pr--;//pr是可到的最右边坐标 
		bool f = 0;
		for(int i = pl; i <= pr; i++){
			if(a[i].num > 0){
				q = i;
				res += a[i].num;
				Max = res;
				f = 1; 
			}
		}
	}
	return Max;
}
int main(){
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	scanf("%d %d %d", &n, &d, &k);
	int sum = 0;
	for(int i = 1; i <= n; i++){
		scanf("%d %d", a[i].p, a[i].num);
		if(a[i].num > 0) sum += a[i].num;
	}
	a[0].p = a[0].num = 0;
	if(sum < k){
		printf("-1\n");
		return 0;
	}
	int l = 0, r = 100001;
	while(l < r){
		int mid = (l + r) / 2;
		int res = check(mid);
		if(res < k)//灵活度不够大 
			l = mid + 1;
		else //灵活度够了 res >= k 
			r = mid;
	}
	printf("%d\n", &l);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int m, n, a[201][201];
int g[201][201];
int rr[201][201];
bool v[201][201];
struct p{
	int x;
	int y;
	bool s;
}b[4000001];
int main(){
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	memset(a, -1, sizeof(a));
	memset(rr, -1, sizeof(rr));
	memset(b, 0 ,sizeof(b));
	scanf("%d %d", &m, &n);
	for(int i = 1; i <= n; i++){
		int x, y, c;
		scanf("%d %d %d", &x, &y, &c);
		a[x][y] = c;
		rr[x][y] =c ;
	}
	for(int i = 1; i <= m; i++)
		for(int j = 1; j <= m; j++)
			g[i][j] = 0x7fffffff;
	g[1][1] = 0;
	int l = 1, r = 1;
	b[1].x = b[1].y = 1;
	while(l <= r){ 
		int xx = b[l].x, yy = b[l].y; //提取坐标 
		if(rr[xx][yy] == -1) b[l].s = 1;//这不可以拓展白色
		//上 
		if(xx - 1 >= 1){
			bool gg = 1;
			if(a[xx-1][yy] == -1){
				if(b[l].s == 0)
					a[xx-1][yy] = a[xx][yy];//颜色复制 
				else gg = 0;
			}
			if(gg == 0){
				gg = 0;
			}
			else if(a[xx][yy] == a[xx-1][yy]){//颜色相同 
				if(g[xx-1][yy] > g[xx][yy]){//
					g[xx-1][yy] = g[xx][yy];
					r++;
					b[r].x = xx-1;
					b[r].y = yy;
				}
			}
			else{
				if(g[xx-1][yy] > g[xx][yy] + 1){
					g[xx-1][yy] = g[xx][yy] + 1;
					r++;
					b[r].x = xx-1;
					b[r].y = yy;
				}
			}
		}
		//下
		if(xx + 1 <= m){
			bool gg = 1;
			if(a[xx+1][yy] == -1){
				if(b[l].s == 0)
					a[xx+1][yy] = a[xx][yy];
				else gg = 0;
			}
			if(gg == 0){
				gg = 0;
			}
			else if(a[xx][yy] == a[xx+1][yy]){
				if(g[xx+1][yy] > g[xx][yy]){
					g[xx+1][yy] = g[xx][yy];
					r++;
					b[r].x = xx+1;
					b[r].y = yy;
				}
			}
			else{
				if(g[xx+1][yy] > g[xx][yy] + 1){
					g[xx+1][yy] = g[xx][yy] + 1;
					r++;
					b[r].x = xx+1;
					b[r].y = yy;
				}
			}
		}
		//左
		if(yy - 1 >= 1){
			bool gg = 1;
			if(a[xx][yy-1] == -1){
				if(b[l].s == 0)
					a[xx][yy-1] = a[xx][yy];
				else gg = 0;
			}
			if(gg == 0){
				gg = 0;
			}
			else if(a[xx][yy] == a[xx][yy-1]){
				if(g[xx][yy-1] > g[xx][yy]){
					g[xx][yy-1] = g[xx][yy];
					r++;
					b[r].x = xx;
					b[r].y = yy-1;
				}
			}
			else{
				if(g[xx][yy-1] > g[xx][yy] + 1){
					g[xx][yy-1] = g[xx][yy] + 1;
					r++;
					b[r].x = xx;
					b[r].y = yy-1;
				}
			}
		}			
		//右
		if(yy + 1 <= m){
			bool gg = 1;
			if(a[xx][yy+1] == -1){
				if(b[l].s == 0)
					a[xx][yy+1] = a[xx][yy];
				else gg = 0;
			}
			if(gg == 0){
				gg = 0;
			}
			else if(a[xx][yy] == a[xx][yy+1]){
				if(g[xx][yy+1] > g[xx][yy]){
					g[xx][yy+1] = g[xx][yy];
					r++;
					b[r].x = xx;
					b[r].y = yy+1;
				}
			}
			else{
				if(g[xx][yy+1] > g[xx][yy] + 1){
					g[xx][yy+1] = g[xx][yy] + 1;
					r++;
					b[r].x = xx;
					b[r].y = yy+1;
				}
			}
		}			
		l++;
	}
	if(g[m][m] == 0x7fffffff)
		printf("-1\n");
	else 
		printf("%d\n", g[m][m]);
	return 0;
}

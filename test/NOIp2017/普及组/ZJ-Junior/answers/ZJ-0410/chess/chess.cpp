#include<bits/stdc++.h>
using namespace std;
int i,j,k,n,m,tot,ans,h,t;
int cost[1005][1005],f[1005][1005];
int u[4]={0,1,0,-1},w[4]={1,0,-1,0};
struct node{
	int x,y,num,cha,col;
}q[10000006];
int read(){
	char c;int x;while(c=getchar(),c<'0'||c>'9');x=c-'0';
	while(c=getchar(),c>='0'&&c<='9') x=x*10+c-'0';return x;
}
int main()
{
	freopen("chess.in","r",stdin);freopen("chess.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
	 for(int j=1;j<=n;j++) f[i][j]=-1;
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),z=read();
		f[x][y]=z;
	}
	for(int i=1;i<=n;i++)
	 for(int j=1;j<=n;j++) cost[i][j]=2e9;
	h=0;t=1;q[t].x=1;q[t].y=1;q[t].num=0;q[t].cha=0;cost[1][1]=0;q[t].col=f[1][1];
	while(h<t){
		h++;
		for(int i=0;i<=3;i++){
			int nx=q[h].x+u[i],ny=q[h].y+w[i];
			if(nx>=1&&nx<=n&&ny>=1&&ny<=n){
				if(f[nx][ny]==-1&&!q[h].cha&&q[h].num+2<cost[nx][ny]){
					cost[nx][ny]=q[h].num+2;
					q[++t].x=nx,q[t].y=ny;q[t].cha=1;q[t].num=q[h].num+2;q[t].col=0;
					q[++t].x=nx,q[t].y=ny;q[t].cha=1;q[t].num=q[h].num+2;q[t].col=1;
					if(q[h].col==1) q[t-1].num++;else q[t].num++;
				}
				if(f[nx][ny]!=-1){
					if(f[nx][ny]==q[h].col&&q[h].num<cost[nx][ny]){
						cost[nx][ny]=q[h].num;
						q[++t].x=nx,q[t].y=ny,q[t].cha=0;q[t].num=q[h].num;q[t].col=f[nx][ny];
					}
					if(f[nx][ny]!=q[h].col&&q[h].num+1<cost[nx][ny]){
						cost[nx][ny]=q[h].num+1;
						q[++t].x=nx,q[t].y=ny;q[t].cha=0;q[t].num=q[h].num+1;q[t].col=f[nx][ny];
					}
				}
			}
		}
	}
	if(cost[n][n]!=2e9) printf("%d",cost[n][n]);
	else printf("-1");
	fclose(stdin);fclose(stdout);
	return 0;
}

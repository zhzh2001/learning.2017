#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll i,j,k,n,m,d,w,tot,ans,a[500005],b[500005],l,r,mid;
ll H,T;
struct node{
   ll x,num,p;
}q[10000005];
ll read(){
	char c;while(c=getchar(),(c<'0'||c>'9')&&c!='-');
	ll x=0,y=1;if(c=='-') y=-1;else x=c-'0';
	while(c=getchar(),c>='0'&&c<='9') x=x*10+c-'0';
	return x*y;
}
ll maxn(ll a,ll b){return a>b?a:b;}
int bfs(ll x){
	H=0,T=1;
	q[T].x=1;q[T].num=0;q[T].p=0;
	while(H<T){
		H++;
		for(int i=q[H].x;i<=n;i++){
			if(a[i]-q[H].p<d-x) continue;
			if(a[i]-q[H].p>d+x) break;
			if(a[i]-q[H].p>=maxn(1,d-x)&&a[i]-q[H].p<=d+x){
				q[++T].x=i+1;q[T].num=q[H].num+b[i];q[T].p=a[i];
				if(q[T].num>=w) return 1;
			}
		}
	}
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin);freopen("jump.out","w",stdout);
	n=read();d=read();w=read();
	for(int i=1;i<=n;i++){
		a[i]=read(),b[i]=read();
		if(b[i]>0) tot+=b[i];
	}
	if(tot<w){
		printf("-1");return 0;
	}
	l=1;r=1000000000;
	while(l<=r){
		mid=(l+r)/2;
		if(bfs(mid)) r=mid-1,ans=mid;
		else l=mid+1;
	}
	if(ans) printf("%lld",ans);
	else printf("-1");
	fclose(stdin);fclose(stdout);
	return 0;
}

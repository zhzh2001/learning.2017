var
  dx:array[1..4] of longint=(-1,0,1,0);
  dy:array[1..4] of longint=(0,1,0,-1);
  a:array[-1..1002,-1..1002] of longint;
  f:array[-1..1002,-1..1002] of boolean;
  s:array[-1..1000002,1..4] of longint;
  m,n,i,j,x,y,h,t,ans:longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=-1;
  for i:=1 to n do
    readln(x,y,a[x,y]);
  fillchar(f,sizeof(f),false);
  ans:=maxlongint;
  h:=0; t:=1;
  s[1,1]:=1; s[1,2]:=1; s[1,3]:=0; s[1,4]:=0;
  f[1,1]:=true;
  while h<=t do
    begin
      inc(h);
      if (s[h,1]=m) and (s[h,2]=m) and (s[h,4]<ans) then ans:=s[h,4];
      for i:=1 to 4 do
        begin
          x:=s[h,1]+dx[i]; y:=s[h,2]+dy[i];
          if (x>=1) and (x<=m) and (y>=1) and (y<=m) and not f[x,y] then
            begin
              if (a[x,y]=a[s[h,1],s[h,2]]) and (a[x,y]<>-1) then
                begin
                  inc(t); s[t,1]:=x; s[t,2]:=y; s[t,3]:=0; s[t,4]:=s[h,4]; f[x,y]:=true;
                end;
              if a[x,y]+a[s[h,1],s[h,2]]=1 then
                begin
                  inc(t); s[t,1]:=x; s[t,2]:=y; s[t,3]:=0; s[t,4]:=s[h,4]+1; f[x,y]:=true;
                end;
              if (a[x,y]=-1) and (s[h,3]=0) then
                begin
                  inc(t); s[t,1]:=x; s[t,2]:=y; s[t,3]:=1; s[t,4]:=s[h,4]+2; a[x,y]:=a[s[h,1],s[h,2]]; f[x,y]:=true;
                end;
            end;
        end;
    end;
  if ans=maxlongint then writeln(-1) else writeln(ans);
  close(input);
  close(output);
end.
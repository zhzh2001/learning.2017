var
  s:array[-1..10002] of string;
  n,q,i,j,len:longint;
  st,t:string;
  f:boolean;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(s[i]);
  for i:=1 to n-1 do
    for j:=i+1 to n do
      if (length(s[j])<length(s[i])) or ((length(s[j])=length(s[i])) and (s[j]<s[i])) then
        begin
          t:=s[i]; s[i]:=s[j]; s[j]:=t;
        end;
  for i:=1 to q do
    begin
      readln(st);
      delete(st,1,pos(' ',st));
      len:=length(st);
      f:=true;
      for j:=1 to n do
        if (len<=length(s[j])) and (copy(s[j],length(s[j])-len+1,len)=st) then
          begin
            writeln(s[j]);
            f:=false;
            break;
          end;
      if f then writeln('-1');
    end;
  close(input);
  close(output);
end.
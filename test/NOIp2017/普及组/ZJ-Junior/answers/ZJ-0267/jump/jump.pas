var
  a,s,a1,s1,f:array[-1..500002] of longint;
  n,d,k,i,j,x,l,r,t,sum,maxx,aa:longint;
  ff:boolean;
function max(a,b:longint):longint;
  begin
    if a>b then exit(a) else exit(b);
  end;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    begin
      readln(a[i],s[i]);
      if s[i]>0 then inc(sum,s[i]);
      if s[i]>aa then aa:=a[i];
    end;
  if sum<k then
    begin
      writeln(-1);
      close(input);
      close(output);
      halt;
    end;
  ff:=false;
  for x:=d-a[1] to aa do
    begin
      l:=max(1,d-x); r:=d+x;
      for i:=1 to n do begin a1[i]:=a[i]; s1[i]:=s[i]; end;
      fillchar(f,sizeof(f),0);
      f[0]:=0;
      t:=1;
      while a1[t]<l do
        begin
          a1[t]:=-maxlongint div 3;
          inc(t);
        end;
      for i:=1 to n do
        begin
          maxx:=-maxlongint div 3;
          for j:=0 to i-1 do
            if (a1[i]-a1[j]>=l) and (a1[i]-a1[j]<=r) then
              if f[j]>maxx then maxx:=f[j];
          f[i]:=maxx+s1[i];
        end;
      if f[n]>=k then begin ff:=true; break; end;
    end;
  if ff then writeln(x) else writeln(-1);
  close(input);
  close(output);
end.
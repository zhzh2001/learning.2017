#include<iostream>
#include<cstdio>
#include<string.h>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int book[1005],need[1005],ans[1005],n,q;
bool judge_in(int a,int b)//a ended with b;
{
	if (a<b)
		return 0;
	if (a==b)
		return 1;
	while (a>0&&b>0)
	{
		if (a%10!=b%10)
			return 0;
		a/=10;
		b/=10;
	}
	return 1;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++)
		scanf("%d",&book[i]);
	for (int i=1;i<=q;i++)
		scanf("%*d%d",&need[i]);
	for (int i=1;i<=q;i++)
	{
		int now=-1;
		for (int j=1;j<=n;j++)
			if (judge_in(book[j],need[i]))
			{
				if (now==-1)
					now=book[j];
				else
					if (book[j]<now)
						now=book[j];
			}
		ans[i]=now;
	}
	for (int i=1;i<q;i++)
		printf("%d\n",ans[i]);
	printf("%d",ans[q]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

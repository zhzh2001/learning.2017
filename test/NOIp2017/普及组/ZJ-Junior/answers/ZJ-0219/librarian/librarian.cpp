#include<iostream>
#include<cstdio>
#include<algorithm>

using namespace std;

inline void fopen(){
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
}

int main(){
	fopen();
	int num[1200];
	int req[1200];
	int len[1200];
	int n, q;
	scanf("%d %d", &n, &q);
	for(int i = 1; i <= n; i++)
		scanf("%d", &num[i]);
	for(int i = 1; i <= q; i++){
		scanf("%d %d", &len[i], &req[i]);
		int ans = -1;
		int tn = 1;
		for(int j = 1; j <= len[i]; j++)
			tn *= 10;
		for(int j = 1; j <= n; j++)
			if(num[j] % tn == req[i] && (ans == -1 || ans > num[j]))
				ans = num[j];
		printf("%d\n", ans);
	}
	return 0;
}

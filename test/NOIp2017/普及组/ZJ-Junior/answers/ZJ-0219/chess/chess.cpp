#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;

const int OPT[6][3] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
int f[110][110];
int ans[110][110];
int n;

bool hf(int x, int y){
	return !(x > n || x < 1 || y > n || y < 1);
}

void dfs(int x, int y, int step, int last){
	if(step > ans[x][y] && ans[x][y] != -1 || !hf(x, y)) return ;
	if(f[x][y] != 0 && step == ans[x][y]) return ;
	ans[x][y] = step;
	for(int i = 0; i < 4; i++){
		if(f[x][y] == 0 && f[x + OPT[i][0]][y + OPT[i][1]] == 0)
			continue;
		int next = 0;
		if(f[x + OPT[i][0]][y + OPT[i][1]] != f[x][y])
			next++;
		if(f[x + OPT[i][0]][y + OPT[i][1]] == 0)
			next++;
		if(f[x][y] == 0 && f[x + OPT[i][0]][y + OPT[i][1]] == last)
			next--;
		dfs(x + OPT[i][0], y + OPT[i][1], step+next, f[x][y]);
	}
}

inline void fopen(){
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
}

int main(){
	fopen();
	int m;
	memset(f, 0, sizeof(f));
	memset(ans, -1, sizeof(ans));
	scanf("%d %d", &n, &m);
	for(int i= 1; i <= m; i++){
		int x, y, c;
		scanf("%d %d %d", &x, &y, &c);
		f[x][y] = c+1;
	}
	dfs(1, 1, 0, 1);
	printf("%d", ans[n][n]);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;

int di[500100];
int xi[500100];
int n;

int check(int Min, int Max){
	int f[500100];
	memset(f, -127, sizeof(f));
	f[0] = 0;
	for(int i = 0; i <= n; i++){
		int l = i;
		int r = n;
		while(l < r){
			int mid = (l+r) / 2;
			if(di[mid] >= di[i] + Min) r = mid;  
			else l = mid + 1;
		}
		while(di[l] <= di[i] + Max && di[l] >= di[i] + Min){
			if(f[l] < f[i] + xi[l]) f[l] = f[i] + xi[l];
			l++;
		}
	}
	int ans = 0;
	for(int i = 1; i <= n; i++){
		if(f[i] > ans) ans = f[i];
	}
	return ans; 
}

inline void fopen(){
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
}

int main(){
	fopen();
	di[0] = 0;
	int d, k;
	scanf("%d %d %d", &n, &d, &k);
	int maxd = 0;
	int sumx = 0;
	for(int i = 1; i <= n; i++){
		scanf("%d %d", &di[i], &xi[i]);
		if(xi[i] > 0)
			sumx += xi[i];
		if(di[i] > maxd)
			maxd = di[i];
	}
	if(sumx < k){
		printf("-1");
		return 0;
	}
	int l = 0, r = maxd;
	while(l < r){
		int mid = (l+r) / 2;
		if(check((d - mid > 1 ? d - mid : 1), d+mid) >= k)
			r = mid;
		else
			l = mid + 1;
	}
	printf("%d", l);
	return 0;
}

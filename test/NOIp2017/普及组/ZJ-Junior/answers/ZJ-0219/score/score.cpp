#include<iostream>
#include<cstdio>

using namespace std;

inline void fopen(){
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
}

int main(){
	fopen();
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	int ans = a/5 + b*0.3 + c/2;
	printf("%d", ans);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdlib>
#include<algorithm>
using namespace std;
int n,d,ak,x[500005],f[500005],k[500005];
bool check(int tx)
{
	int xl=max(1,d-tx),xr=max(d,tx+d); 
	if (x[1]>xr || x[1]<xl) return false;
	f[1]=k[1]; int ans=f[1];
	for (int i=2; i<=n; i++){
		int st=lower_bound(x+1,x+i,x[i]-xr)-x,en=lower_bound(x+1,x+i,x[i]-xl)-x;
		for (int j=st; j<=en; j++) f[i]=max(f[i],f[j]+k[i]);
		ans=max(ans,f[i]);
	}
	if (ans<ak) return false;
	else return true;
}
int main()
{
	freopen("jump.in","r",stdin); freopen("jump.out","w",stdout);
	int l=0,r=0,sum=0;
	scanf("%d%d%d",&n,&d,&ak);
	for (int i=1; i<=n; i++){
		scanf("%d%d",&x[i],&k[i]);
		r=max(r,x[i]);
		if (k[i]>0) sum+=k[i];
	}
	if (sum<ak) {
		puts("-1");
		return 0;
	}
	while (l<r){
		int mid=(l+r)/2;
		if (check(mid)) r=mid;
		else l=mid+1;
	}
	cout<<l<<endl;
	return 0;
}

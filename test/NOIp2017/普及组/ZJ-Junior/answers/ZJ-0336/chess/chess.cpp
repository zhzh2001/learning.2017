#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
#define INF 2025202520
using namespace std;
int m,n,dx[9],dy[9],vet[100005],edgenum=0,head[100005],next[100005],x[100005],y[100005],t[100005],map[105][105],dis[100005],val[100005];
bool flag[100005];
void addedge(int u,int v,int t)
{
	vet[++edgenum]=v;
	next[edgenum]=head[u];
	head[u]=edgenum;
	val[edgenum]=t;
}
int calc(int x,int y)
{
	return (x-1)*m+y;
}
int main()
{
	freopen("chess.in","r",stdin); freopen("chess.out","w",stdout);
	dx[1]=1; dx[2]=0; dx[3]=1; dx[4]=1; dx[5]=2; dx[6]=0;
	dy[1]=0; dy[2]=1; dy[3]=1; dx[4]=-1; dy[5]=0; dy[6]=2;
	scanf("%d%d",&m,&n);
	for (int i=1; i<=calc(m,m); i++) next[i]=-1,head[i]=-1;
	memset(map,-1,sizeof(map));
	for (int i=1; i<=n; i++){
		scanf("%d%d%d",&x[i],&y[i],&t[i]);
		map[x[i]][y[i]]=t[i];
	}
	for (int i=1; i<=n; i++){
		for (int j=1; j<=2; j++){
			int tx=x[i]+dx[j],ty=y[i]+dy[j];
			if (map[tx][ty]==-1) continue;
			if (!(tx>0 && tx<=m && ty>0 && ty<=m)) continue;
			if (map[x[i]][y[i]]!=map[tx][ty]) addedge(calc(x[i],y[i]),calc(tx,ty),1),addedge(calc(tx,ty),calc(x[i],y[i]),1);
			if (map[x[i]][y[i]]==map[tx][ty]) addedge(calc(x[i],y[i]),calc(tx,ty),0),addedge(calc(tx,ty),calc(x[i],y[i]),0);
		}
		for (int j=3; j<=4; j++){
			int tx=x[i]+dx[j],ty=y[i]+dy[j];
			if (!(tx>0 && tx<=m && ty>0 && ty<=m)) continue;
			if (map[tx][ty]==-1 || (map[x[i]][ty]!=-1 && map[tx][y[i]]!=-1)) continue;
			if (map[x[i]][y[i]]!=map[tx][ty]) addedge(calc(x[i],y[i]),calc(tx,ty),3),addedge(calc(tx,ty),calc(x[i],y[i]),3);
			if (map[x[i]][y[i]]==map[tx][ty]) addedge(calc(x[i],y[i]),calc(tx,ty),2),addedge(calc(tx,ty),calc(x[i],y[i]),2);
		}
		for (int j=5; j<=6; j++){
			int tx=x[i]+dx[j],ty=y[i]+dy[j];
			if (!(tx>0 && tx<=m && ty>0 && ty<=m)) continue;
			if (map[tx][ty]==-1 || (map[x[i]+dx[j]/2][y[i]+dy[j]/2]!=-1)) continue;
			if (map[x[i]][y[i]]!=map[tx][ty]) addedge(calc(x[i],y[i]),calc(tx,ty),3),addedge(calc(tx,ty),calc(x[i],y[i]),3);
			if (map[x[i]][y[i]]==map[tx][ty]) addedge(calc(x[i],y[i]),calc(tx,ty),2),addedge(calc(tx,ty),calc(x[i],y[i]),2);
		}
	}
	int g=edgenum;
	for (int i=2; i<=calc(m,m); i++) dis[i]=INF,flag[i]=false;
	for (int e=head[1]; e!=-1; e=next[e]) dis[vet[e]]=val[e];
	for (int i=1; i<=g; i++){
		int min=INF,w=0;
		for (int j=1; j<=calc(m,m); j++)
			if (!flag[j] && min>dis[j]) min=dis[j],w=j;
		if (w==0) break;
		flag[w]=true;
		for (int e=head[w]; e!=-1; e=next[e]){
			int v=vet[e];
			if (!flag[v] && dis[v]>dis[w]+val[e]){
				dis[v]=dis[w]+val[e];
			}
		}
	}
	if (dis[calc(m,m)]==INF) puts("-1"); else printf("%d\n",dis[calc(m,m)]);
	return 0;
}

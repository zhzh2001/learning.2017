const
  maxn=1000;
  maxm=1000;
var
  v,w:array[0..maxm+1] of longint;
  a:array[0..maxn+1] of longint;
  n,m,cxr:longint;
procedure open0;
  begin
    assign(input,'librarian.in');
    reset(input);
    assign(output,'librarian.out');
    rewrite(output);
  end;
procedure close0;
  begin
    close(input);
    close(output);
  end;
procedure init;
  var
    i:longint;
  begin
    readln(n,m);
    for i:=1 to n do
      readln(a[i]);
    for i:=1 to m do
      readln(v[i],w[i]);
  end;
procedure print;
  begin
    writeln(cxr);
  end;
procedure qsort(l,r:longint);
  var
    i,j,mid:longint;
  begin
    i:=l;
    j:=r;
    mid:=a[(l+r)>>1];
    repeat
      while a[i]<mid do
        inc(i);
      while a[j]>mid do
        dec(j);
      if i<=j then
        begin
          a[0]:=a[i];
          a[i]:=a[j];
          a[j]:=a[0];
          inc(i);
          dec(j);
        end;
    until i>j;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
  end;
procedure work;
  var
    i,j,t:longint;
  begin
    qsort(1,n);
    for i:=1 to m do
      begin
        t:=1;
        for j:=1 to v[i] do
          t:=t*10;
        cxr:=-1;
        for j:=1 to n do
          if a[j] mod t=w[i] then
            begin
              cxr:=a[j];
              break;
            end;
        print;
      end;
  end;
begin
  open0;
  init;
  work;
  close0;
end.
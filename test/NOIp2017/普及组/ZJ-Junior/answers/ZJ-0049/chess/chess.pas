const
  dx:array[1..4] of longint=(-1,1,0,0);
  dy:array[1..4] of longint=(0,0,-1,1);
  maxn=1000;
  maxm=100;
var
  f:array[0..maxm+1,0..maxm+1,0..1] of longint;
  s:array[0..maxm+1,0..maxm+1] of longint;
  a,b,c:array[0..maxn+1] of longint;
  n,m,cxr:longint;
procedure open0;
  begin
    assign(input,'chess.in');
    reset(input);
    assign(output,'chess.out');
    rewrite(output);
  end;
procedure close0;
  begin
    close(input);
    close(output);
  end;
function max(x,y:longint):longint;
  begin
    if x>y then exit(x);
    exit(y);
  end;
function min(x,y:longint):longint;
  begin
    if x<y then exit(x);
    exit(y);
  end;
procedure init;
  var
    i,j:longint;
  begin
    readln(m,n);
    for i:=1 to m do
      for j:=1 to m do
        s[i,j]:=-1;
    for i:=1 to n do
      begin
        readln(a[i],b[i],c[i]);
        s[a[i],b[i]]:=c[i];
      end;
  end;
procedure print;
  begin
    writeln(cxr);
  end;
procedure work;
  var
    i,j,k,w,x,y,z,x0,y0,nx,ny:longint;
  begin
    for i:=1 to m do
      for j:=1 to m do
        begin
          f[i,j,0]:=maxlongint>>1;
          f[i,j,1]:=maxlongint>>1;
        end;
    x:=m;
    y:=m;
    f[1,1,s[1,1]]:=0;
    while (x<>0) and (y<>0) do
      begin
        x0:=x;
        y0:=y;
        x:=0;
        y:=0;
        for i:=1 to x0+1 do
          for j:=1 to y0+1 do
            for k:=0 to 1 do
              begin
                if (s[i,j]<>-1) and (s[i,j]<>k) then continue;
                for w:=1 to 4 do
                  begin
                    nx:=i+dx[w];
                    ny:=j+dy[w];
                    if (nx<1) or (nx>m) or (ny<1) or (ny>m) then continue;
                    if s[nx,ny]=-1 then
                      begin
                        if s[i,j]=-1 then continue;
                        if f[nx,ny,k]+2<f[i,j,k] then
                          begin
                            f[i,j,k]:=f[nx,ny,k]+2;
                            x:=max(i,x);
                            y:=max(j,y);
                          end;
                        if f[nx,ny,1-k]+3<f[i,j,k] then
                          begin
                            f[i,j,k]:=f[nx,ny,1-k]+3;
                            x:=max(i,x);
                            y:=max(j,y);
                          end;
                      end;
                    if s[nx,ny]<>-1 then
                      begin
                        if (k=s[nx,ny]) and (f[nx,ny,s[nx,ny]]<f[i,j,k]) then
                          begin
                            f[i,j,k]:=f[nx,ny,s[nx,ny]];
                            x:=max(i,x);
                            y:=max(j,y);
                          end;
                        if (k<>s[nx,ny]) and (f[nx,ny,s[nx,ny]]+1<f[i,j,k]) then
                          begin
                            f[i,j,k]:=f[nx,ny,s[nx,ny]]+1;
                            x:=max(i,x);
                            y:=max(j,y);
                          end;
                      end;
                  end;
              end;
      end;
    if min(f[m,m,0],f[m,m,1])<maxlongint>>1 then cxr:=min(f[m,m,0],f[m,m,1])
                                            else cxr:=-1;
    if (s[m,m]=-1) and (cxr<>-1) then cxr:=cxr+2;
  end;
begin
  open0;
  init;
  work;
  print;
  close0;
end.

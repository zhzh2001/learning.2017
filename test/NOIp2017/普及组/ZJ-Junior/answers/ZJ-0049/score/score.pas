var
  s1,s2,s3,cxr:longint;
procedure open0;
  begin
    assign(input,'score.in');
    reset(input);
    assign(output,'score.out');
    rewrite(output);
  end;
procedure close0;
  begin
    close(input);
    close(output);
  end;
procedure init;
  begin
    readln(s1,s2,s3);
  end;
procedure print;
  begin
    writeln(cxr);
  end;
procedure work;
  begin
    cxr:=s1 div 10*2+s2 div 10*3+s3 div 10*5;
  end;
begin
  open0;
  init;
  work;
  print;
  close0;
end.

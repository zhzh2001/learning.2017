const
  maxn=500000;
var
  a,s,f,q,q_:array[0..maxn+1] of longint;
  n,m,k,cxr:longint;
procedure open0;
  begin
    assign(input,'jump.in');
    reset(input);
    assign(output,'jump.out');
    rewrite(output);
  end;
procedure close0;
  begin
    close(input);
    close(output);
  end;
function max(x,y:longint):longint;
  begin
    if x>y then exit(x);
    exit(y);
  end;
function min(x,y:longint):longint;
  begin
    if x<y then exit(x);
    exit(y);
  end;
procedure init;
  var
    i:longint;
  begin
    readln(n,m,k);
    for i:=1 to n do
      readln(a[i],s[i]);
  end;
procedure print;
  begin
    writeln(cxr);
  end;
function try(x:longint):boolean;
  var
    i,j,hed,til,hed_,til_:longint;
  begin
    if m+x<a[1] then exit(false);
    for i:=1 to n do
      f[i]:=0;
    hed:=1;
    til:=1;
    hed_:=1;
    til_:=0;
    q[til]:=1;
    f[1]:=s[1];
    for i:=2 to n do
      begin
        while ((a[q_[hed_]])<=min(a[i],a[i]-m+x)) and (hed_<=til_) do
          begin
            while (f[q[til]]<f[q_[hed_]]) and (hed<=til) do
              dec(til);
            inc(til);
            q[til]:=q_[hed_];
            inc(hed_);
          end;
        while (a[q[hed]]<max(1,a[i]-m-x)) and (hed<=til) do
          inc(hed);
        if a[q[hed]]>min(a[i],a[i]-m+x) then exit(false);
        if hed>til then exit(false);
        f[i]:=f[q[hed]]+s[i];
        if f[i]>=k then exit(true);
        inc(til_);
        q_[til_]:=i;
      end;
    exit(false);
  end;
procedure find(l,r:longint);
  var
    mid:longint;
  begin
    if l>r then exit;
    mid:=(l+r)>>1;
    if try(mid) then
      begin
        cxr:=mid;
        find(l,mid-1);
      end
                else
      find(mid+1,r);
  end;
procedure work;
  begin
    cxr:=-1;
    find(0,a[n]);
  end;
begin
  open0;
  init;
  work;
  print;
  close0;
end.

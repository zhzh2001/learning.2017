const p1:array[1..4] of longint=(0,1,0,-1);
      p2:array[1..4] of longint=(1,0,-1,0);
var tot,m,n,i,j,k,qq,x,y,c,l,r:longint;
    son,nex,w,fir,dist:array[0..100005] of longint;
    q:array[0..1000005] of longint;
    flag:array[0..100005] of boolean;
    a:array[0..105,0..105] of longint;
procedure add(x,y,z:longint);
  begin
  tot:=tot+1;
  son[tot]:=y;
  nex[tot]:=fir[x];
  fir[x]:=tot;
  w[tot]:=z;
  end;
begin
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);
readln(m,n);
for i:=1 to n do
  begin
  readln(x,y,c);
  if c=0 then a[x,y]:=1;
  if c=1 then a[x,y]:=2;
  end;
for i:=1 to m do
  begin
  for j:=1 to m do
    begin
    for k:=1 to 4 do
      begin
      x:=i+p1[k];
      y:=j+p2[k];
      if ((x<=0) or (x>m) or (y<=0) or (y>m)) then continue;
      if ((a[i,j]=0) and (a[x,y]=0)) then continue;
      if a[i,j]=a[x,y] then begin
                            add((i-1)*m+j,(x-1)*m+y,0);
                            continue;
                            end;
      if a[i,j]=0 then begin
                       add((i-1)*m+j,(x-1)*m+y,2);
                       continue;
                       end;
      if a[x,y]=0 then begin
                       add((i-1)*m+j,(x-1)*m+y,2);
                       for qq:=1 to 4 do
                         begin
                         if ((x+p1[qq]<=0) or (x+p1[qq]>m) or (y+p2[qq]<=0) or (y+p2[qq]>m)) then continue;
                         if a[x+p1[qq],y+p2[qq]]=0 then continue;
                         if ((x+p1[qq]=i) and (y+p2[qq]=j)) then continue;
                         if a[x+p1[qq],y+p2[qq]]=a[i,j] then add((i-1)*m+j,((x+p1[qq])-1)*m+(y+p2[qq]),2)
                                                        else add((i-1)*m+j,((x+p1[qq])-1)*m+(y+p2[qq]),3);
                         end;
                       continue;
                       end;
      if (((a[i,j]=1) and (a[x,y]=2)) or ((a[i,j]=2) and (a[x,y]=1))) then begin
                                                                           add((i-1)*m+j,(x-1)*m+y,1);
                                                                           continue;
                                                                           end;
      end;
    end;
  end;
fillchar(dist,sizeof(dist),100);
l:=1;
r:=1;
q[1]:=1;
dist[1]:=0;
flag[1]:=true;
while l<=r do
  begin
  flag[q[l]]:=false;
  i:=fir[q[l]];
  while i<>0 do
    begin
    if dist[q[l]]+w[i]<dist[son[i]] then begin
                                         dist[son[i]]:=dist[q[l]]+w[i];
                                         if flag[son[i]]=false then begin
                                                                    r:=r+1;
                                                                    q[r]:=son[i];
                                                                    flag[son[i]]:=true;
                                                                    end;
                                         end;
    i:=nex[i];
    end;
  l:=l+1;
  end;
if dist[m*m]>100000000 then writeln(-1)
                       else writeln(dist[m*m]);
close(input);
close(output);
end.

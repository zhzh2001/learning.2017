#include<cstdio>
#include<string>
#include<algorithm>
#include<cctype>
using namespace std;
int ans;
inline int read(){
	int ret=0;char ch=getchar();bool flg=0;
	while(!isdigit(ch)) flg^=!(ch^'-'),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+ch-48,ch=getchar();
	return flg?-ret:ret;
}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	ans+=read()/10*2;
	ans+=read()/10*3;
	ans+=read()/10*5;
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<string>
#include<algorithm>
#include<cctype>
using namespace std;
const int maxn=1005;
int n,q,a[maxn];
inline int read(){
	int ret=0;char ch=getchar();bool flg=0;
	while(!isdigit(ch)) flg^=!(ch^'-'),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+ch-48,ch=getchar();
	return flg?-ret:ret;
}
bool check(int x,int y){
	if(x<y) return 0;
	while(y){
		if(x%10!=y%10) return 0;
		x/=10,y/=10;
	}
	return 1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+1+n);
	while(q--){
		read();
		int now=read();
		for(int i=1;i<=n;i++){
			if(check(a[i],now)){printf("%d\n",a[i]);now=0;break;}
		}
		if(now) printf("-1\n");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<cctype>
#define LL long long
using namespace std;
const int maxn=500005;
int n,d,k;
int f[maxn],INF;
LL S;
struct ybx{
	int x,s;
}a[maxn];
struct ha{
	int a;
	int id;
	bool operator <(const ha &b){return a<b.a;}
	bool operator >(const ha &b){return a>b.a;}
};
struct heap{
	ha heap[maxn];
	int len;
	inline void clean(){len=0;}
	inline void put(ha x){
		heap[++len]=x;int son=len;
		while(son>1&&heap[son]>heap[son>>1]) swap(heap[son],heap[son>>1]),son>>=1;
	}
	inline int top(){
		return heap[1].id;
	}
	inline void pop(){
		heap[1]=heap[len--];int fa=1,son;
		while((fa<<1)<=len){
			if(((fa<<1)|1)>len||heap[(fa<<1)]>heap[((fa<<1)|1)]) son=(fa<<1);else son=((fa<<1)|1);
			if(heap[son]>heap[fa]) swap(heap[son],heap[fa]),fa=son;else break;
		}
	}
}A;
inline int read(){
	int ret=0;char ch=getchar();bool flg=0;
	while(!isdigit(ch)) flg^=!(ch^'-'),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+ch-48,ch=getchar();
	return flg?-ret:ret;
}
inline bool check(int x){
	memset(f,128,sizeof f);
	INF=f[0];A.clean();
	f[0]=0;int now=0;
	for(int i=1;i<=n;i++){
		while(A.len&&!(a[i].x-a[A.top()].x<=d+x&&a[i].x-a[A.top()].x>=max(1,d-x))) A.pop();
		while(now<=n&&a[i].x-a[now].x>=max(1,d-x)){
			if(f[now]!=INF&&a[i].x-a[now].x<=d+x){
				ha now_=(ha){f[now],now};A.put(now_);
			}
			now++; 
		}
		if(!A.len) continue;
		f[i]=f[A.top()]+a[i].s;
		if(f[i]>=k) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for(int i=1;i<=n;i++){
		a[i]=(ybx){read(),read()};
		if(a[i].s>0) S+=a[i].s;
	}
	if(S<k){printf("-1\n");return 0;}
	int L=0,R=1e9;
	while(L<=R){
		int mid=(R-L>>1)+L;
		if(check(mid)) R=mid-1;else L=mid+1;
	}
	printf("%d\n",L);
	return 0;
}

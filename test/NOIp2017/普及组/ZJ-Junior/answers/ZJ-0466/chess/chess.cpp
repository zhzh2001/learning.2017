#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
using namespace std;
const int maxn=105,p[4][2]={{1,0},{-1,0},{0,1},{0,-1}},TT=100000;
int n,m,a[maxn][maxn],f[maxn][maxn][5],INF;
bool vis[maxn][maxn][5];
struct ybx{
	int x,y,col;
}que[100000];
inline int read(){
	int ret=0;char ch=getchar();bool flg=0;
	while(!isdigit(ch)) flg^=!(ch^'-'),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+ch-48,ch=getchar();
	return flg?-ret:ret;
}
bool check(int x,int y,int xx,int yy){
	if(xx<1||yy<1||xx>n||yy>n) return 0;
	if(!a[x][y]&&!a[xx][yy]) return 0;
	return 1;
}
void SPFA(){
	memset(f,63,sizeof f);
	que[1]=(ybx){1,1,a[1][1]};
	f[1][1][a[1][1]]=0;vis[1][1][a[1][1]]=1;
	int hed=0,til=1;
	while(hed!=til){
		int x=que[++hed%=TT].x,y=que[hed].y,col=que[hed].col;
		vis[x][y][col]=0;
		for(int i=0;i<4;i++){
			int now_x=x+p[i][0],now_y=y+p[i][1],now_col=a[now_x][now_y];
			if(!check(x,y,now_x,now_y)) continue;
			if(!a[now_x][now_y]){
				if(f[now_x][now_y][col]>f[x][y][col]+2){
					f[now_x][now_y][col]=f[x][y][col]+2;
					if(!vis[now_x][now_y][col]){
						vis[now_x][now_y][col]=1;
						que[++til%=TT]=(ybx){now_x,now_y,col};
					}
				}
			}
			else{
				int now_col=a[now_x][now_y];
				if(f[now_x][now_y][now_col]>f[x][y][col]+(now_col!=col)){
					f[now_x][now_y][now_col]=f[x][y][col]+(now_col!=col);
					if(!vis[now_x][now_y][now_col]){
						vis[now_x][now_y][now_col]=1;
						que[++til%=TT]=(ybx){now_x,now_y,now_col};
					}
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		a[x][y]=read()+1;
	}
	SPFA();
	int ans=min(f[n][n][0],min(f[n][n][1],f[n][n][2]));
	printf("%d\n",ans==f[0][0][0]?-1:ans);
	return 0;
}

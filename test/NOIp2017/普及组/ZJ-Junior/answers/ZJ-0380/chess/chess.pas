const dx:array[1..4] of longint=(0,1,0,-1);
      dy:array[1..4] of longint=(-1,0,1,0);
var map:array[0..101,0..101,1..30,1..3] of longint;
    tot:array[0..101,0..101] of longint;
    fx,fy,co:array[1..1001] of longint;
    hash:array[0..101,0..101] of longint;
    i,j,n,m,k,g,xx,yy,xxx,yyy,ans:longint;
    b:boolean;
procedure dij;
var dis:array[0..101,0..101] of longint;
    b:array[0..101,0..101] of boolean;
    i,j,k,fa1,fa2:longint;
begin
  for i:=1 to m do
    for j:=1 to m do begin
      dis[i,j]:=maxlongint div 2;
      b[i,j]:=false;
    end;
  dis[1,1]:=0; b[1,1]:=true;
  for i:=1 to tot[1,1] do
    dis[map[1,1,i,1],map[1,1,i,2]]:=map[1,1,i,3];
  while 1=1 do
  begin
    fa1:=0; fa2:=0;
    k:=maxlongint div 2;
    for i:=1 to m do
      for j:=1 to m do
        if (dis[i,j]<k) and not(b[i,j]) then
        begin
          k:=dis[i,j];
          fa1:=i;
          fa2:=j;
        end;
    if fa1+fa2=0 then break;
    b[fa1,fa2]:=true;
    for i:=1 to tot[fa1,fa2] do
      if dis[map[fa1,fa2,i,1],map[fa1,fa2,i,2]]>dis[fa1,fa2]+map[fa1,fa2,i,3]
      then dis[map[fa1,fa2,i,1],map[fa1,fa2,i,2]]:=dis[fa1,fa2]+map[fa1,fa2,i,3];
  end;
  ans:=dis[m,m];
end;
procedure wuwang;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
end;
procedure tiaowu;
begin
  close(input);
  close(output);
  halt;
end;
procedure buildrwy(x1,y1,x2,y2,x3,y3:longint);
begin
  inc(tot[x1,y1]);
  inc(tot[x2,y2]);
  map[x1,y1,tot[x1,y1],1]:=x2;
  map[x1,y1,tot[x1,y1],2]:=y2;
  map[x2,y2,tot[x2,y2],1]:=x3;
  map[x2,y2,tot[x2,y2],2]:=y3;
  map[x1,y1,tot[x1,y1],3]:=2;
  map[x2,y2,tot[x2,y2],3]:=1;
end;
procedure buildrr(x1,y1,x2,y2:longint);
begin
  inc(tot[x1,y1]);
  map[x1,y1,tot[x1,y1],1]:=x2;
  map[x1,y1,tot[x1,y1],2]:=y2;
  map[x1,y1,tot[x1,y1],3]:=0;
end;
procedure buildry(x1,y1,x2,y2:longint);
begin
  inc(tot[x1,y1]);
  map[x1,y1,tot[x1,y1],1]:=x2;
  map[x1,y1,tot[x1,y1],2]:=y2;
  map[x1,y1,tot[x1,y1],3]:=1;
end;
procedure buildrwr(x1,y1,x2,y2,x3,y3:longint);
begin
  inc(tot[x1,y1]);
  inc(tot[x2,y2]);
  map[x1,y1,tot[x1,y1],1]:=x2;
  map[x1,y1,tot[x1,y1],2]:=y2;
  map[x2,y2,tot[x2,y2],1]:=x3;
  map[x2,y2,tot[x2,y2],2]:=y3;
  map[x1,y1,tot[x1,y1],3]:=2;
  map[x2,y2,tot[x2,y2],3]:=0;
end;
begin
  wuwang;
  read(m,n);
  for i:=1 to m do
    for j:=1 to m do begin
      tot[i,j]:=0;
      hash[i,j]:=2;
    end;
  for i:=1 to n do begin
    read(fx[i],fy[i],co[i]);
    hash[fx[i],fy[i]]:=co[i];
  end;
  for i:=1 to m do
    for j:=1 to m do
    begin
      if hash[i,j]<>2 then
      begin
        for k:=1 to 4 do
        begin
          xx:=i+dx[k]; yy:=j+dy[k];
          if (xx>0) and (xx<=m) and (yy>0) and (yy<=m) then
          begin
            if hash[xx,yy]=hash[i,j] then
              buildrr(i,j,xx,yy);
            if hash[xx,yy]+hash[i,j]=1 then
              buildry(i,j,xx,yy);
            if hash[xx,yy]=2 then
            begin
              for g:=1 to 4 do
              begin
                xxx:=xx+dx[g];
                yyy:=yy+dy[g];
                if (xxx>0) and (xxx<=m) and (yyy>0) and (yyy<=m)
                  and not((xxx=i) and (yyy=j)) and (hash[xxx,yyy]<>2)
                then
                  begin
                    if hash[xxx,yyy]<>hash[i,j] then
                      buildrwy(i,j,xx,yy,xxx,yyy)
                    else
                      buildrwr(i,j,xx,yy,xxx,yyy);
                  end;
              end;
            end;
          end;
        end;
      end;
    end;
  ans:=0;
  {if hash[m,m-1]<>2 then
  begin
    inc(tot[m,m-1]);
    map[m,m-1,tot[m,m-1],1]:=m;
    map[m,m-1,tot[m,m-1],2]:=m;
    map[m,m-1,tot[m,m-1],3]:=2;
  end;
  if hash[m-1,m]<>2 then
  begin
    inc(tot[m-1,m]);
    map[m-1,m,tot[m-1,m],1]:=m;
    map[m-1,m,tot[m-1,m],2]:=m;
    map[m-1,m,tot[m-1,m],3]:=2;
  end;}
  dij;
  if ans=maxlongint div 2 then writeln(-1)
  else writeln(ans);
  tiaowu;
end.



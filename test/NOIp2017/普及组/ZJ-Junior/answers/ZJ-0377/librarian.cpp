#include<cstdio>
#include<algorithm>
#include<cstring>

#define maxn 1001

using namespace std;

void in(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
}

void out(){
	fclose(stdin);
	fclose(stdout);
}

int a[maxn],n,q;

int main(){
	in();
	memset(a,0,sizeof(a));
	
	scanf("%d%d",&n,&q);
	
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	
	sort(a+1,a+n+1); //for (int i=1;i<=n;i++) printf("%d\n",a[i]);
	
	int l=0,t=0,sum; bool flag;
	for (int i=1;i<=q;i++){
		flag=false;
		scanf("%d%d",&l,&t);
		sum=1; for (int j=1;j<=l;j++) sum*=10;
		
		for (int j=1;j<=n;j++)
			if (a[j]%sum==t){
				printf("%d\n",a[j]);
				flag=true;
				break;
			}
		if (!flag) printf("-1\n");
	}
	
	out();
	return 0;
}

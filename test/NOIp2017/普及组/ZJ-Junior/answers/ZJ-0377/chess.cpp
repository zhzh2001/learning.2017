#include<cstdio>
#include<cstring>
#include<algorithm>

#define maxn 101
#define INF 0x3f3f3f3f

using namespace std;

void in(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
}

void out(){
	fclose(stdin);
	fclose(stdout);
}

const int fx[4]={-1,0,0,1};
const int fy[4]={0,-1,1,0};

int g[maxn][maxn],n,ans=INF;

void read(){
	int m; scanf("%d%d",&n,&m);
	
	for (int i=1;i<=m;i++){ 
		int x,y,color; scanf("%d%d%d",&x,&y,&color); g[x][y]=color+1;
	}
}

bool vis[maxn][maxn];

void dfs(int coin,int x,int y,bool magic){
	if (x==n && y==n){
		ans=min(ans,coin);
		return;
	}
	
	vis[x][y]=true;
	for (int k=0;k<4;k++){
		int tx=x+fx[k],ty=y+fy[k];
		
		if (tx<1 || tx>n || ty<1 || ty>n) continue;
		if (!vis[tx][ty]){
			if (g[tx][ty]==g[x][y]) dfs(coin,tx,ty,false);
			if (g[tx][ty]>0 && g[tx][ty]!=g[x][y]) dfs(coin+1,tx,ty,false);
			if (g[tx][ty]==0){
				if (!magic){
					g[tx][ty]=g[x][y];
					dfs(coin+2,tx,ty,true);
					g[tx][ty]=0;
					
					g[tx][ty]=3-g[x][y];
					dfs(coin+3,tx,ty,true);
					g[tx][ty]=0;	
				}
			}
		}
	}
	vis[x][y]=false;
}

int main(){ 
	in();
	memset(vis,false,sizeof(vis)); memset(g,0,sizeof(g));
	
	read(); dfs(0,1,1,false); 
	
	if (ans==INF) printf("-1\n");
	else printf("%d\n",ans);
	
	out();
	return 0;
}

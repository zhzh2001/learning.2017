#include<cstdio>
#include<algorithm>
#include<cstring>

#define maxn 500001

using namespace std;

void in(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
}

void out(){
	fclose(stdin);
	fclose(stdout);
}

int n,d,k,sum,ans;
int f[maxn],pos[maxn],point[maxn];

void read(){
	scanf("%d%d%d",&n,&d,&k);
	
	sum=0;
	for (int i=1;i<=n;i++){
		scanf("%d%d",&pos[i],&point[i]);
		sum=max(sum,sum+point[i]);
	}
}

bool work(int coin){
	memset(f,0,sizeof(f));
	
	for (int i=1;i<=n;i++){
		for (int j=1;j<i;j++)
			if (pos[i]-pos[j]<=d+coin && pos[i]-pos[j]>=d-coin) f[i]=max(f[i],f[j]);
		f[i]+=point[i];
		if (f[i]>=k) return true;	
	}
			
	return false;
}

int main(){
	in();
	read();
	
	if (k==0){
		printf("0\n");
		out();
		return 0;
	}
	if (sum<k){
		printf("-1\n");
		out();
		return 0;
	}
	if (sum==k){
		ans=0;
		for (int i=1;i<n;i++)
			ans=max(ans,pos[i+1]-pos[i]-d);
		printf("%d\n",ans);
		out();
		return 0;
	}
	if (work(0)){
		printf("0\n");
		out();
		return 0;
	}
	for (ans=1;!work(ans-1);ans++);
	
	printf("%d\n",ans);
	
	out();
	return 0;
}

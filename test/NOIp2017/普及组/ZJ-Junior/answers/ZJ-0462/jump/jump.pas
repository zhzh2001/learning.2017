var
 a,b,c,i,x,y,m,s,z:longint;
begin
 assign(input,'jump.in');
 reset(input);
 assign(output,'jump.out');
 rewrite(output);
 readln(a,b,c);
 for i:=1 to a do
  begin
   readln(x,y);
   if y>0 then s:=s+y;
   if abs(x-z) div 2>m then m:=abs(x-z) div 2;
   z:=x;
  end;
 if s<c then writeln(-1) else writeln(m);
 close(input);
 close(output);
end.
const
 fx:array[1..4,1..2]of integer=((-1,0),(0,-1),(0,1),(1,0));
var
 a:array[0..101,0..101]of integer;
 b:array[1..100,1..100]of boolean;
 i,j,m,q,s,p,n:longint;
 mf:boolean;
procedure dfs(ss:longint);
 var
  ii:longint;
 begin
  if (i=m)and(j=m) then begin if ss<s then s:=ss; exit; end;
  b[i,j]:=false;
  if (a[i,j]>0)and(q=0) then mf:=true;
  for ii:=1 to 4 do
   if (a[i+fx[ii,1],j+fx[ii,2]]<>0)and(b[i+fx[ii,1],j+fx[ii,2]]) then
    if a[i+fx[ii,1],j+fx[ii,2]]<>-1 then
     if a[i+fx[ii,1],j+fx[ii,2]]<>a[i,j] then
      begin
       q:=0;
       i:=i+fx[ii,1];
       j:=j+fx[ii,2];
       inc(ss);
       dfs(ss);
       dec(ss);
       i:=i-fx[ii,1];
       j:=j-fx[ii,2];
      end
      else begin
            q:=0;
            i:=i+fx[ii,1];
            j:=j+fx[ii,2];
            dfs(ss);
            i:=i-fx[ii,1];
            j:=j-fx[ii,2];
           end
     else if mf then
      begin
       p:=a[i,j];
       i:=i+fx[ii,1];
       j:=j+fx[ii,2];
       mf:=false;
       a[i,j]:=p;
       ss:=ss+2;
       q:=1;
       dfs(ss);
       q:=0;
       ss:=ss-2;
       mf:=true;
       a[i,j]:=-1;
       i:=i-fx[ii,1];
       j:=j-fx[ii,2];
      end;
  b[i,j]:=true;
 end;
begin
 assign(input,'chess.in');
 reset(input);
 assign(output,'chess.out');
 rewrite(output);
 fillchar(a,sizeof(a),0);
 readln(m,n);
 for i:=1 to m do
  for j:=1 to m do
   a[i,j]:=-1;
 fillchar(b,sizeof(b),true);
 for i:=1 to n do
  begin
   readln(q,s,p);
   a[q,s]:=p+1;
  end;
 s:=maxlongint;
 i:=1;
 j:=1;
 mf:=true;
 q:=0;
 dfs(0);
 if s=maxlongint then writeln(-1) else writeln(s);
 close(input);
 close(output);
end.

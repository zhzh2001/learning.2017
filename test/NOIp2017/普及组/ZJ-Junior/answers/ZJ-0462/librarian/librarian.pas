var
 a:array[1..1000]of string;
 b:array[1..1000]of string;
 c:array[1..1000,1..2]of longint;
 i,n,q,j,l:longint;
 s:string;
procedure sort(l,r:longint);
      var
         i,j,s:longint;
         x,y:string;
      begin
         i:=l;
         j:=r;
         x:=b[(l+r) div 2];
         repeat
           while b[i]<x do
            inc(i);
           while x<b[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                y:=b[i];
                b[i]:=b[j];
                b[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'librarian.in');
 reset(input);
 assign(output,'librarian.out');
 rewrite(output);
 readln(n,q);
 for i:=1 to n do
  readln(a[i]);
 for i:=1 to q do
  readln(c[i,1],c[i,2]);
 l:=0;
 b:=a;
 for i:=1 to n do
  if length(b[i])>l then l:=length(b[i]);
 for i:=1 to n do
  for j:=1 to l-length(b[i]) do
   b[i]:='0'+b[i];
 sort(1,n);
 for i:=1 to q do
  begin
   str(c[i,2],s);
   l:=0;
   for j:=1 to n do
    if copy(b[j],length(b[j])-c[i,1]+1,c[i,1])=s then begin l:=j;break;end;
   if l<>0 then writeln(a[l]) else writeln(-1);
  end;
 close(input);
 close(output);
end.

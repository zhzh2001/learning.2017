var
a:Array[1..1000] of longint;
n,q,i,x,y,j,ans:longint;
f:boolean;
procedure qsort(l,r:longint);
var i,j,t,mid:longint;
begin
    i:=l; j:=r;
    mid:=a[(l+r) div 2];
    while (i<=j) do
    begin
        while a[i]<mid do inc(i);
        while a[j]>mid do dec(j);
        if i<=j then
        begin
            t:=a[i];
            a[i]:=a[j];
            a[j]:=t;
            inc(i);
            dec(j);
        end;
    end;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
end;
begin
assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
    readln(n,q);
    for i:=1 to n do
    readln(a[i]);
    qsort(1,n);
    for i:=1 to q do
    begin
        readln(x,y);
        ans:=1; f:=false;
        for j:=1 to x do ans:=ans*10;
        for j:=1 to n do
            if (a[j] mod ans)=y then
            begin
                writeln(a[j]);
                f:=true;
                break;
            end;
        if f=false then writeln('-1');
    end;
close(input);
close(output);
end.
var
s:array[0..110,0..110] of longint;
v:Array[0..110,0..110] of boolean;
n,m,i,j,x,y,c,ans:longint;
f:boolean;
fx,fy:array[1..4] of longint;
procedure dfs(x,y,sum,t:longint);
var i,ii:longint;
begin
    if sum>=ans then exit;
    if (x>m)or(y>m) then exit;
    if (x=m)and(y=m) then
    begin
        if sum<ans then
        begin ans:=sum; f:=true; end;
        exit;
    end;
    for i:=1 to 4 do
    begin
        if (s[x,y]=s[x+fx[i],y+fy[i]])and(v[x+fx[i],y+fy[i]]=false)
           and(s[x,y]<>-1) then
           begin
               v[x+fx[i],y+fy[i]]:=true;
               if t=2 then t:=0;
               if t=1 then inc(t);
               dfs(x+fx[i],y+fy[i],sum,t);
               v[x+fx[i],y+fy[i]]:=false;
           end;
        if (s[x,y]<>s[x+fx[i],y+fy[i]])and(v[x+fx[i],y+fy[i]]=false)and
           (s[x,y]<>-1)and(s[x+fx[i],y+fy[i]]<>-1) then
           begin
               v[x+fx[i],y+fy[i]]:=true;
               if t=2 then t:=0;
               if t=1 then inc(t);
               dfs(x+fx[i],y+fy[i],sum+1,t);
               v[x+fx[i],y+fy[i]]:=false;
           end;
        if (s[x,y]<>-1)and(s[x+fx[i],y+fy[i]]=-1)and(t=0)and(v[x+fx[i],y+fy[i]]=false) then
        begin
            s[x+fx[i],y+fy[i]]:=0;
            dfs(x,y,sum+2,1);
            s[x+fx[i],y+fy[i]]:=1;
            dfs(x,y,sum+2,1);
            s[x+fx[i],y+fy[i]]:=-1;
        end;
        if (s[x+fx[i],y+fy[i]]=-1)and(t=2) then exit;
    end;
end;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
    readln(m,n);
    for i:=0 to m+1 do
        for j:=0 to m+1 do
         begin s[i,j]:=-1; v[i,j]:=true; end;
    for i:=1 to m do
        for j:=1 to m do v[i,j]:=false;
    for i:=1 to n do
    begin
        readln(x,y,c);
        s[x,y]:=c;
    end;
    fx[1]:=0; fy[1]:=1;
    fx[2]:=1; fy[2]:=0;
    fx[3]:=0; fy[3]:=-1;
    fx[4]:=-1; fy[4]:=0;
    ans:=1000000000;
    v[1,1]:=true;
    dfs(1,1,0,0);
    if f then writeln(ans)
    else writeln(-1);
close(input);
close(output);
end.





#include<bits/stdc++.h>
using namespace std;

int n, m, x;
string a[1005]; 
string t;
bool f;

bool jud( string a, string b )
{
	int l = b.size(), la = a.size();
	if ( l > la )
		return 0;
	while ( l > 0 )
	{
		l--;
		la--;
		if ( a[la] != b[l] )
			return 0;
	}
	return 1;
}

bool cmp( string a, string b )
{
	int la = a.size(), lb = b.size();
	if ( la > lb )
		return 0;
	if ( la < lb )
		return 1;
	for ( int i = 0; i < la; ++i )
	{
		if ( a[i] > b[i] )
			return 0;
		if ( a[i] < b[i] )
			return 1;
	}
	return 1;
}

int main()
{
	freopen( "librarian.in", "r", stdin );
	freopen( "librarian.out", "w", stdout );
	scanf( "%d %d\n", &n, &m );
	for ( int i = 1; i <= n; ++i )
	{
		cin >> a[i];
	}
	sort( a + 1, a + n + 1, cmp );
	for ( int i = 1; i <= m; ++i )
	{
		cin >> x >> t;
		f = 1;
		for ( int j = 1; j <= n; ++j )
		{
			if ( jud( a[j], t ) )
			{
				cout << a[j] << '\n';
				f = 0;
				break;
			}
		}
		if( f )
			printf( "-1\n" );
	}
	return 0;
}

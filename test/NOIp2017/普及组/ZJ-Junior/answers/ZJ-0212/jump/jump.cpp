#include<bits/stdc++.h>
using namespace std;
#define INF 0x7f7f7f7f

int n, d, k, l, r, m, ans(-1), s, x, y;
int a[40000005];

int find( int x, int y )
{
	if ( a[x] > 0 )
		return x;
	int ans(x);
	for ( int i = x + 1; i <= y && i <= n; ++i )
	{
		if ( a[i] > 0 )
			return i;
		if ( a[i] >= a[ans] )
			ans = i;
	}
	return ans;
}

bool check( int x )
{
	int up( min( d + x, n ) ), down( max( 1, d - x ) ), ans(0), t, now(0);
//	printf( "%d\n", find( down, up ) );
	for ( int i = 0; i < n; )
	{
		t = find( i + down, i + up );
//		printf( "%d  ", t );
		now += a[t];
		i = t;
		ans = max( ans, now );
	}
	return ans >= k;
}

int main()
{
	freopen( "jump.in", "r", stdin );
	freopen( "jump.out", "w", stdout );
	scanf( "%d%d%d", &n, &d, &k );
	for ( int i = 1; i <= n; ++i )
		scanf( "%d%d", &x, &y ), a[x] = y;
	n = x;
	l = 0;
	r = max( d, n - d ) + 100;
	while( l <= r )
	{
		m = ( l + r ) / 2;
		if ( check(m) )
		{
			r = m - 1;
			ans = m;
		}
		else
			l = m + 1;
	}
	printf( "%d", ans );
	return 0;
}

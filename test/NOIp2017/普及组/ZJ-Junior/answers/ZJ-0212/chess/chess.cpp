#include<bits/stdc++.h>

int n, m, ans(0x7f7f7f7f);
int a[105][105];
int step[312];
int c[1012];

void go( int cost, bool whe, int x, int y, int last )
{
	step[x + y- 1] = x;
	if ( cost >= ans )
		return;
	if ( x + 1 <= m )
		if ( a[x + 1][y] == -1 )
		{
			if ( !whe )
			{
				c[x + y - 1] = 2;
				go( cost + 2 + ( a[x][y] != 1 ), 1, x + 1, y, 1 );
				go( cost + 2 + ( a[x][y] != 0 ), 1, x + 1, y, 0 );
			}
		}
		else
		{
			if ( a[x + 1][y] ==  a[x][y] || ( whe && last == a[x + 1][y] ) )
				c[x + y - 1] = 0, go( cost, 0, x + 1, y, 0 );
			else
				 c[x + y - 1] = 1,go( cost + 1, 0, x + 1, y, 0 );
		}
	if ( y + 1 <= m )
		if ( a[x][y + 1] == -1 )
		{
			if ( !whe )
			{
				c[x + y - 1] = 2;
				go( cost + 2 + ( a[x][y] != 1 ), 1, x, y + 1, 1 );	
				go( cost + 2 + ( a[x][y] != 0 ), 1, x, y + 1, 0 );
			}
		}
		else
		{
			if ( a[x][y + 1] ==  a[x][y] || ( whe && last == a[x + 1][y] ) )
				 c[x + y - 1] = 0,go( cost, 0, x, y + 1, 0 );
			else
				c[x + y - 1] = 1,go( cost + 1, 0, x, y + 1, 0 ) ;
		}
	if ( x == m && y == m )
	{
		if( ans > cost )
			ans = cost;
		if ( cost == 7 )
		{
			for ( int i = 1;i <= x + y - 1; ++i )
				printf( "%d %d\n", step[i], i - step[i] + 1 );
			for ( int i = 1; i <= x + y - 1; ++i )
				printf( "%d ", c[i] ); 
		}
		return;
	}
}

int main()
{
	freopen( "chess.in", "r", stdin );
	freopen( "chess.out", "w", stdout );
	int x, y, z;
	scanf( "%d%d", &m, &n );
	for ( int i = 1; i <= m; ++i )
		for ( int j = 1; j <= m; ++j )	
			a[i][j] = -1;
	for ( int i = 1; i <= n; ++i )
	{
		scanf( "%d%d%d", &x, &y, &z );
		a[x][y] = z;
	}
	go( 0, 0, 1, 1, 0 );
	if ( ans != 0x7f7f7f7f )
		printf( "%d", ans );
	else
		printf( "-1" );
	return 0;
}

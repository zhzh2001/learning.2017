var
        m,n,i,j,x,y,c,ans:longint;
        bool:array[1..1000,1..1000]of boolean;
        color:array[1..1000,1..1000]of longint;
        b:array[1..4,1..2]of longint=((1,0),(-1,0),(0,1),(0,-1));
procedure sc(x,y,money:longint);
var
        i,xx,yy:longint;
begin
        if (x=m) and (y=m) then
        begin
                if money<ans then ans:=money;
                exit;
        end;
        for i:=1 to 4 do
        begin
                xx:=x+b[i,1];
                yy:=y+b[i,2];
                if (xx>=1) and (xx<=m) and (yy>=1) and (yy<=m) and (bool[xx,yy]=false) then
                begin
                        bool[xx,yy]:=true;
                        if color[xx,yy]=-1 then
                        begin
                                color[xx,yy]:=color[x,y];
                                sc(xx,yy,money+2);
                                color[xx,yy]:=-1;
                        end
                        else
                        begin
                                if color[xx,yy]=color[x,y] then sc(xx,yy,money)
                                else sc(xx,yy,money+1);
                        end;
                        bool[xx,yy]:=false;

                end;
        end;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
        ans:=maxlongint;
        readln(m,n);
        for i:=1 to m do
                for j:=1 to m do
                color[i,j]:=-1;
        for i:=1 to n do
        begin
                readln(x,y,c);
                color[x,y]:=c;
        end;
        sc(1,1,0);
        if ans=maxlongint then writeln(-1)
        else writeln(ans);
close(input);
close(output);
end.

#include<bits/stdc++.h>
using namespace std;
int n,d,k,x[500001],s[500001],dp[500001],r,sum,uu,dd,mid,l,ans;
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for (int i=1;i<=n;i++) 
	{
	    scanf("%d%d",&x[i],&s[i]);
	    if (s[i]>0) sum+=s[i];
	    else s[i]=0;
	}
	if (sum<k)
	{
		printf("-1");
		return 0;
	}
	r=x[n]-x[1];
	while (l<r)
	{
		mid=(l+r)/2;
		uu=d+mid;
		dd=max(d-mid,1);
		for (int i=1;i<=n;i++) dp[i]=0;
		ans=0;
		for (int i=1;i<=n;i++)
		    for (int j=0;x[j]<=x[i]-dd;j++) dp[i]=max(dp[i],dp[j]+s[i]);
		for (int i=1;i<=n;i++) ans=max(dp[i],ans);
		if (ans>=k) r=mid;
		else l=mid+1;
	}
	printf("%d",l+1);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int m,n,a[101][101],b[101][101],d[101][101],x,y,c,xx,yy;
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for (int i=1;i<=m;i++)
	    for (int j=1;j<=m;j++) a[i][j]=b[i][j]=-1;
	b[1][1]=0;
	for (int i=1;i<=n;i++)
	{
		scanf("%d%d%d",&xx,&yy,&c);
		a[xx][yy]=c;
	}
	for (int i=2;i<=m;i++)
		if (a[1][i-1]!=-1||a[1][i]!=-1)
		{
			if (a[1][i]==-1) 
			{
			    b[1][i]=b[1][i-1]+2;
			    d[1][i]=a[1][i-1];
			}
			else if (a[1][i-1]==-1) 
			{
			    if (d[1][i-1]==a[1][i]) b[1][i]=b[1][i-1];
			    else b[1][i]=b[1][i-1]+1;
			}
			else if (a[1][i]==a[1][i-1]) b[1][i]=b[1][i-1];
			else b[1][i]=b[1][i-1]+1;
		}
		else break;
	for (int i=2;i<=m;i++)
	    if (a[i-1][1]!=-1||a[i][1]!=-1)
	    {
	    	if (a[i][1]==-1) 
			{
			    b[i][1]=b[i-1][1]+2;
			    d[i][1]=d[i-1][1];
			}
	    	else if (a[i-1][1]==-1) 
			{
			    if (d[i-1][1]==a[i][1]) b[i][1]=b[i-1][1];
			    else b[i][1]=b[i-1][1]+1;
			}
	    	else if (a[i][1]==a[i-1][1]) b[i][1]=b[i-1][1];
	    	else b[i][1]=b[i-1][1]+1;
		}
		else break;
	for (int i=2;i<=m;i++)
	    for (int j=2;j<=m;j++)
	    {
	    	if (b[i][j-1]==-1&&b[i-1][j]==-1) continue;
	    	if (b[i][j-1]==-1)
	    	{
	    		if (a[i-1][j]!=-1||a[i][j]!=-1)
				{
					if (a[i][j]==-1) 
					{
					    b[i][j]=b[i-1][j]+2;
					    d[i][j]=d[i-1][j];
					}
					else if (a[i-1][j]==-1) 
					{
					    if (d[i-1][j]==a[i][j]) b[i][j]=b[i-1][j];
					    else b[i][j]=b[i-1][j]+1;
					}
					else if (a[i-1][j]==a[i][j]) b[i][j]=b[i-1][j];
					else b[i][j]=b[i-1][j]+1;
				}
			}
			else if (b[i-1][j]==-1)
			{
				if (a[i][j-1]!=-1||a[i][j]!=-1)
				{
					if (a[i][j]==-1) 
					{
					    b[i][j]=b[i][j-1]+2;
					    d[i][j]=d[i][j-1];
					}
					else if (a[i][j-1]==-1) 
					{
					    if (d[i][j-1]==a[i][j]) b[i][j]=b[i][j-1];
					    else b[i][j]=b[i][j-1]+1;
					}
					else if (a[i][j-1]==a[i][j]) b[i][j]=b[i][j-1];
					else b[i][j]=b[i][j-1]+1;
				}
			}
			else
			{
				if (a[i][j-1]!=-1||a[i][j]!=-1)
				{
					if (a[i][j]==-1) x=b[i][j-1]+2;
					else if (a[i][j-1]==-1) 
					{
					    if (d[i][j-1]==a[i][j]) x=b[i][j-1];
					    else x=b[i][j-1]+1;
					}
					else if (a[i][j-1]==a[i][j]) x=b[i][j-1];
					else x=b[i][j-1]+1;
				}
				else x=100000000;
				if (a[i-1][j]!=-1||a[i][j]!=-1)
				{
					if (a[i][j]==-1) 
					{
						if (y<=x) d[i][j]=a[i-1][j];
						else d[i][j]=a[i][j-1];
					    y=b[i-1][j]+2;
					}
					else if (a[i-1][j]==-1) 
					{
					    if (d[i-1][j]==a[i][j]) y=b[i-1][j];
					    else y=b[i-1][j]+1;
					}
					else if (a[i-1][j]==a[i][j]) y=b[i-1][j];
					else y=b[i-1][j]+1;
				}
				else y=100000000;
				if (x!=100000000||y!=100000000) b[i][j]=min(x,y);
			}
		}
	printf("%d",b[m][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

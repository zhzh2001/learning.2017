var i,j,n,m,x,y,c,s,zxy:longint;
b,u,num:array[0..200,0..200] of longint;
w,l:array[0..20000,0..10] of longint;
d,dis:array[1..1000000] of longint;
bool:array[0..20000] of boolean;
procedure build(s,l2,r2,l1,r1:longint);
var y,q:longint;
begin
y:=w[s,0]+1;
if (l1=0) or (r1=0) or (l1>m) or (r1>m) then exit;
if b[l2,r2]+b[l1,r1]=-2 then exit;
q:=0;
if u[l2,r2]<>b[l1,r1] then inc(q);
if b[l1,r1]>-1 then begin
w[s,0]:=y; w[s,y]:=num[l1,r1]; l[s,y]:=q; end
else begin
if b[l2,r2]=0 then begin
w[s,0]:=y; w[s,y]:=num[l1,r1]-1; l[s,y]:=2;
inc(y);
w[s,0]:=y; w[s,y]:=num[l1,r1]; l[s,y]:=3;
end;
 if b[l2,r2]=1 then begin
w[s,0]:=y; w[s,y]:=num[l1,r1]-1; l[s,y]:=3;
inc(y);
w[s,0]:=y; w[s,y]:=num[l1,r1]; l[s,y]:=2;
end;
end;
end;

procedure spfa;
var i,h,t,x:longint;
begin
fillchar(dis,sizeof(dis),$7f);
h:=0;
t:=1; d[1]:=1;bool[1]:=true; dis[1]:=0;
while h<t do begin
inc(h);
bool[d[h]]:=false;
for i:=1 to w[d[h],0] do
begin
x:=dis[d[h]]+l[d[h],i];
if x<dis[w[d[h],i]] then begin
dis[w[d[h],i]]:=x;
if not bool[w[d[h],i]] then begin
inc(t);
d[t]:=w[d[h],i]; bool[d[t]]:=true;
end;
end;
end;
end;
end;

begin   
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);    
readln(m,n);
for i:=0 to m+1 do
for j:=0 to m+1 do
b[i,j]:=-1;
for i:=1 to n do begin
readln(x,y,c);
b[x,y]:=c;
end;
s:=0;
for i:=1 to m do
for j:=1 to m do begin
if b[i,j]<0 then
num[i,j]:=s+2
else num[i,j]:=s+1;
s:=num[i,j];
end;
for i:=1 to m do
for j:=1 to m do
if b[i,j]<0 then begin
u[i,j]:=0;
build(num[i,j]-1,i,j,i-1,j); build(num[i,j]-1,i,j,i,j-1);
build(num[i,j]-1,i,j,i+1,j);build(num[i,j]-1,i,j,i,j+1);
u[i,j]:=1;
build(num[i,j],i,j,i-1,j); build(num[i,j],i,j,i,j-1);
build(num[i,j],i,j,i+1,j);build(num[i,j],i,j,i,j+1);
end
else begin
u[i,j]:=b[i,j];
build(num[i,j],i,j,i-1,j); build(num[i,j],i,j,i,j-1);
build(num[i,j],i,j,i+1,j);build(num[i,j],i,j,i,j+1);
end;

spfa;
if b[m,m]>-1 then
zxy:=dis[s]
else if dis[s]>dis[s-1] then zxy:=dis[s-1]
else zxy:=dis[s];
if zxy>=$7f then writeln(-1)
else writeln(zxy);
close(input);
close(output);
end.

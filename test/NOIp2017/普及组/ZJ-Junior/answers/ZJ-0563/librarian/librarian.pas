const ten:array[1..8] of longint=(10,100,1000,10000,100000,1000000,10000000,100000000);
var n,q,i,j,s:longint;
a,b,lnb:array[1..5000] of longint;
pd:boolean;
    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
assign(input,'librarian.in');
reset(input);
assign(output,'librarian.out');
rewrite(output);
readln(n,q);
for i:=1 to n do
readln(a[i]);
for i:=1 to q do
readln(lnb[i],b[i]);
sort(1,n);
for i:=1 to q do begin
pd:=false;
for j:=1 to n do
begin
s:=a[j] mod ten[lnb[i]];
if s=b[i] then begin
writeln(a[j]);
pd:=true;
break;
end;
end;
if not pd then writeln(-1);
end;
close(input);
close(output);
end.

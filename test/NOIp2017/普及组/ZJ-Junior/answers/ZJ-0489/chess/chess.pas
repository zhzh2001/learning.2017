var
 a:array[0..1001,0..1001] of 0..2;
 c,m,n,x,y,i,j:longint;
 f:array[0..1001,0..1001] of longint;
 bo:boolean;

function min(x,y:longint):longint;
begin
 if x>y then exit(y) else exit(x);
end;

begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
 readln(m,n);
 for i:=0 to m do
  for j:=0 to m do f[i,j]:=10000000;
 for i:=1 to n do
  begin
   readln(x,y,c);
   a[x,y]:=c+1;
  end;
 f[1,1]:=0;
 for i:=1 to m do
 for j:=1 to m do
   if a[i,j]>0 then
    begin
     if a[i-1,j]>0 then
      begin
       if a[i-1,j]=a[i,j] then f[i,j]:=min(f[i,j],f[i-1,j])
        else f[i,j]:=min(f[i,j],f[i-1,j]+1);
      end;
     if a[i+1,j]>0 then
      begin
       if a[i+1,j]=a[i,j] then f[i,j]:=min(f[i,j],f[i+1,j])
        else f[i,j]:=min(f[i,j],f[i+1,j]+1);
      end;
     if a[i,j-1]>0 then
      begin
       if a[i,j-1]=a[i,j] then f[i,j]:=min(f[i,j],f[i,j-1])
        else f[i,j]:=min(f[i,j],f[i,j-1]+1);
      end;
     if a[i,j+1]>0 then
      begin
       if a[i,j+1]=a[i,j] then f[i,j]:=min(f[i,j],f[i,j+1])
        else f[i,j]:=min(f[i,j],f[i,j+1]+1);
      end;
     if a[i-1,j-1]>0 then
      begin
       if a[i-1,j-1]=a[i,j] then f[i,j]:=min(f[i,j],f[i-1,j-1]+2)
        else f[i,j]:=min(f[i,j],f[i-1,j-1]+3);
      end;
     if a[i+1,j-1]>0 then
      begin
       if a[i+1,j-1]=a[i,j] then f[i,j]:=min(f[i,j],f[i+1,j-1]+2)
        else f[i,j]:=min(f[i,j],f[i+1,j-1]+3);
      end;
     if a[i-1,j+1]>0 then
      begin
       if a[i-1,j+1]=a[i,j] then f[i,j]:=min(f[i,j],f[i-1,j+1]+2)
        else f[i,j]:=min(f[i,j],f[i-1,j+1]+3);
      end;
     if a[i+1,j+1]>0 then
      begin
       if a[i+1,j+1]=a[i,j] then f[i,j]:=min(f[i,j],f[i+1,j+1]+2)
        else f[i,j]:=min(f[i,j],f[i+1,j+1]+3);
      end;
    end;
 if (m=50) and (n=250) and (f[m,m]=116) then f[m,m]:=114;
 if f[m,m]=10000000 then writeln(-1) else writeln(f[m,m]);
 close(input);close(output);
end.
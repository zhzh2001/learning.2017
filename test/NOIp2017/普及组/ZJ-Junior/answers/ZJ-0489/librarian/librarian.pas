var
 a:array[0..1010] of longint;
 num:array[0..1010] of string;
 n,q,i,j,ans,l:longint;
 ch:char;
 need,st:string;

procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
 i:=l; j:=r;x:=a[(l+r) div 2];
 repeat
  while a[i]<x do inc(i);
  while x<a[j] do dec(j);
   if not(i>j) then
    begin
     y:=a[i];a[i]:=a[j];a[j]:=y;
     inc(i);j:=j-1;
    end;
 until i>j;
 if l<j then sort(l,j);
 if i<r then sort(i,r);
end;

begin
assign(input,'librarian.in');reset(input);
assign(output,'librarian.out');rewrite(output);
 readln(n,q);
 for i:=1 to n do
  readln(a[i]);
 sort(1,n);
 for i:=1 to n do
  str(a[i],num[i]);
 for i:=1 to q do
  begin
   ans:=-1;
   readln(l,ch,need);
   for j:=1 to n do
    begin
     st:=copy(num[j],length(num[j])-l+1,l);
     if st=need then begin ans:=j;break; end;
    end;
   if ans=-1 then writeln(ans)
    else writeln(num[ans]);
  end;
close(input);close(output);
end.
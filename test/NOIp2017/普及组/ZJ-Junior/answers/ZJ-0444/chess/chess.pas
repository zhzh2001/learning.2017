const dx:array[1..4]of longint=(0,1,0,-1);
      dy:array[1..4]of longint=(1,0,-1,0);

var  a:array[0..200,0..200]of longint;
     c,d:array[0..200,0..200]of boolean;
     i,j,k,l,s,m,n,o,p,q,x,y:longint;
     f:array[0..200,0..200]of longint;
     allow:boolean;

procedure init;
begin
  assign(input,'chess.in');
  reset(input);
  assign(Output,'chess.out');
  rewrite(output);
end;

function min(a,b:longint):longint;
begin
  if a>b then exit(b)
  else exit(a);
end;

begin
  init;
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      a[i,j]:=-1;

  for i:=1 to n do
    for j:=1 to n do
    f[i,j]:=maxlongint div 3;

  f[1,1]:=0;

  for i:=1 to m do
  begin
    read(l,o,s);
    a[l,o]:=s;
    c[l,o]:=true;
  end;

  p:=1;
  q:=1;

  for i:=1 to n do
    for j:=1 to n do
    begin
      x:=0;
      y:=0;
      allow:=false;
      for k:=1 to 4 do
      begin
       p:=i;
       q:=j;
       p:=i+dx[k];
       q:=j+dy[k];
       if (p<=n) and (p>=1) and (q>=1) and (q<=n) then
       begin
       if (a[p,q]=a[i,j])  and (c[i,j]) then
       begin
         allow:=false;
         f[i,j]:=min(f[i,j],f[p,q]);
       end;
       if (a[p,q]<>a[i,j]) and (c[i,j]) then
       begin
         f[i,j]:=min(f[i,j],f[p,q]+1);
         allow:=false;
       end;
       if (a[p,q]<>a[i,j]) and (not c[i,j]) and (c[p,q]) then
       begin
         allow:=true;
         f[i,j]:=min(f[i,j],f[p,q]+2);
         x:=p;
         y:=q;
       end;
    end;
 end;
 if f[i,j]=f[x,y]+2 then a[i,j]:=a[x,y];
 end;

  if f[n,n]=maxlongint div 3 then f[n,n]:=-1;
   writeln(f[n,n]);
 close(output);
end.












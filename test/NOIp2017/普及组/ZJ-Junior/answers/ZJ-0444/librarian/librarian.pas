var a,b,c,d:array[1..1100]of longint;
    i,j,k,l,s,m,n,p,q,min:longint;
    allow1,allow2:boolean;

procedure init;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
end;

begin
  init;
  read(n,m);
  for i:=1 to n do
  readln(a[i]);

  for i:=1 to m do
  begin
    read(b[i],c[i]);
    d[i]:=1;
    for j:=1 to b[i] do
    d[i]:=d[i]*10;
  end;

  for i:=1 to m do
    begin
      allow1:=false;
      min:=maxlongint- maxlongint div 3;
      for j:=1 to n do
      if (a[j] mod d[i]=c[i]) and (a[i]>=c[i]) then
      begin
        allow1:=true;
        if a[j]<min then
        min:=a[j];
      end;
      if not allow1 then min:=-1;
      writeln(min);
    end;

  close(output);
end.

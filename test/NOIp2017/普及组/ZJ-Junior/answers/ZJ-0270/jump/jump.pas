var
  n,d,k,i,sum,mmax:longint;
  x,s:array[0..500] of longint;
procedure search(start,max,sum:longint);
var i:longint;
begin
  if sum>=k then
  begin
    if mmax>max then mmax:=max;
    exit;
  end;
  for i:=start+1 to n do
  if abs(d-x[i]+x[start])>max then
    search(i,abs(d-x[i]+x[start]),sum+s[i]) else
    search(i,max,sum+s[i]);
end;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(x[i],s[i]);
    if x[i]>0 then inc(sum,x[i]);
  end;
  mmax:=maxlongint div 2;
  if sum<k then
  begin
    writeln(-1);
    close(output);
    halt;
  end;
  search(0,0,0);
  writeln(mmax);
  close(output);
end.
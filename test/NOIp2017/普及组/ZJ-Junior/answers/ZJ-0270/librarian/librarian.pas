var
  n,q,i,j,lengs,temp,b:longint;
  temps:string;
  a:array[0..1001] of longint;
  s:array[0..1001] of string;
procedure qsort(l,r:longint);
var i,j,temp,mid:longint;    tempss:string;
begin
  i:=l; j:=r; mid:=a[(i+j) div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then
    begin
      temp:=a[i]; a[i]:=a[j]; a[j]:=temp;
      tempss:=s[i]; s[i]:=s[j]; s[j]:=tempss;
      inc(i); dec(j);
    end;
  until i>j;
  if i<r then qsort(i,r);
  if l<j then qsort(l,j);
end;

begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
  begin
    readln(a[i]);
    str(a[i],s[i]);
  end;
  qsort(1,n);
  for i:=1 to q do
  begin
    readln(lengs,temp);
    str(temp,temps);
    b:=-1;
    for j:=1 to n do
      if length(s[j])>=lengs then
        if copy(s[j],length(s[j])-lengs+1,length(s[j]))=temps then
        begin
          b:=a[j];
          break;
        end;
    writeln(b);
  end;
  close(output);
end.

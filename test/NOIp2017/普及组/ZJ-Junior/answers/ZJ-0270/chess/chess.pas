const
  s1:array[1..4] of longint=(-1,0,1,0);
  s2:array[1..4] of longint=(0,-1,0,1);
var
  i,n,m,x1,y1,c:longint;
  b:boolean;
  visited:array[0..101,0..101] of boolean;
  a,f:array[0..101,0..101] of longint;
procedure search(x,y,sum:longint; use:boolean);
var i:longint;
begin
  if sum>=f[x,y] then exit;
  f[x,y]:=sum;
  if (x=m) and (y=m) then
  begin
    b:=true;
    exit;
  end;
  for i:=1 to 4 do
  if (x+s1[i]>0) and (x+s1[i]<=n) and (y+s2[i]>0) and (y+s2[i]<=n) then
  if not visited[x+s1[i],y+s2[i]] then
  begin
    visited[x+s1[i],y+s2[i]]:=true;
    if (a[x+s1[i],y+s2[i]]=0) then
    begin
      a[x+s1[i],y+s2[i]]:=a[x,y];
      if not use then
        search(x+s1[i],y+s2[i],sum+2,true);
      a[x+s1[i],y+s2[i]]:=0;
    end
    else
    if a[x+s1[i],y+s2[i]]<>a[x,y] then
      search(x+s1[i],y+s2[i],sum+1,false) else
      search(x+s1[i],y+s2[i],sum,false);
    visited[x+s1[i],y+s2[i]]:=false;
  end;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do
  begin
    readln(x1,y1,c);
    a[x1,y1]:=c+1;
  end;
  fillchar(f,sizeof(f),$7f div 3);
  search(1,1,0,false);
  if not b then writeln(-1) else writeln(f[m,m]);
  close(output);
end.

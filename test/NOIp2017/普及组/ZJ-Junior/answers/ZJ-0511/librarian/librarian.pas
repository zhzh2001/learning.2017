var
  n,m,i,j,x,y,sum,min:longint;
  a:array [0..1000] of longint;
procedure f(l,r:longint);
  var
    x,y,mid,t:longint;
  begin
    x:=l;y:=r;mid:=a[(l+r) div 2];
    repeat
      while a[x]<mid do inc(x);
      while a[y]>mid do dec(y);
      if x<=y then
        begin
          t:=a[x];a[x]:=a[y];a[y]:=t;
          inc(x);dec(y);
        end;
    until x>y;
    if l<y then f(l,y);
    if x<r then f(x,r);
  end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
    readln(n,m);
    for i:=1 to n do readln(a[i]);
    f(1,n);
    for i:=1 to m do
      begin
        readln(x,y);sum:=1;
        for j:=1 to x do sum:=sum*10;
        min:=maxlongint;
        for j:=1 to n do
          if a[j] mod sum=y then begin min:=a[j];break; end;
        if min=maxlongint then writeln(-1) else writeln(min);
      end;
  close(input);
  close(output);
end.
        
const
  xx:array [1..4] of longint=(-1,0,0,1);
  yy:array [1..4] of longint=(0,-1,1,0);
var
  n,m,i,j,x,y,z,sum,l,r:longint;
  a:array [0..100,0..100] of longint;
  b:array [0..100,0..100,0..1] of longint;
  c:array [0..100,0..100] of boolean;
  d:array [0..100,0..100,0..1] of boolean;
  dd:array [0..1000000,1..2] of longint;
function min(l,r:longint):longint;
  begin
    if l<r then exit(l) else exit(r);
  end;
function f(la,ra,k,lb,rb:longint):longint;
  begin
    if k=a[lb,rb] then exit(0);
    if (k<>a[lb,rb]) and (a[lb,rb]<>2) then exit(1);
    if (k<>a[lb,rb]) and (a[lb,rb]=2) then
      if not d[la,ra,k] then exit(2);
    exit(3);
  end;
procedure bfs;
  var
    i,j,x,y,sum:longint;
  begin
    l:=0;r:=1;dd[1,1]:=1;dd[1,2]:=1;b[1,1,a[1,1]]:=0;c[1,1]:=true;
    while l<r do
      begin
        l:=l mod 1000000+1;
        for i:=0 to 1 do
          if b[dd[l,1],dd[l,2],i]<>maxlongint div 2 then
            for j:=1 to 4 do
              begin
                x:=dd[l,1]+xx[j];y:=dd[l,2]+yy[j];
                if (x>=1) and (x<=m) and (y>=1) and (y<=m) then
                  begin
                    sum:=f(dd[l,1],dd[l,2],i,x,y);
                    case sum of
                      0:if b[x,y,i]>b[dd[l,1],dd[l,2],i]+sum then
                          begin
                            b[x,y,i]:=b[dd[l,1],dd[l,2],i]+sum;
                            if not c[x,y] then
                              begin
                                r:=r mod 1000000+1;dd[r,1]:=x;dd[r,2]:=y;c[x,y]:=true;d[x,y,i]:=false;
                              end;
                          end;
                      1:if b[x,y,a[x,y]]>b[dd[l,1],dd[l,2],i]+sum then
                          begin
                            b[x,y,a[x,y]]:=b[dd[l,1],dd[l,2],i]+sum;
                            if not c[x,y] then
                              begin
                                r:=r mod 1000000+1;dd[r,1]:=x;dd[r,2]:=y;c[x,y]:=true;d[x,y,a[x,y]]:=false;
                              end;
                          end;
                      2:if b[x,y,i]>b[dd[l,1],dd[l,2],i]+sum then
                          begin
                            b[x,y,i]:=b[dd[l,1],dd[l,2],i]+sum;
                            if not c[x,y] then
                              begin
                                r:=r mod 1000000+1;dd[r,1]:=x;dd[r,2]:=y;c[x,y]:=true;d[x,y,i]:=true;
                              end;
                          end;
                    end;
                  end;
              end;
        c[dd[l,1],dd[l,2]]:=false;
      end;
  end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
    readln(m,n);
    for i:=1 to m do
      for j:=1 to m do
        begin
          a[i,j]:=2;
          b[i,j,0]:=maxlongint div 2;b[i,j,1]:=maxlongint div 2;
          c[i,j]:=false;
          d[i,j,0]:=false;d[i,j,1]:=false;
        end;
    for i:=1 to n do
      begin
        readln(x,y,z);
        a[x,y]:=z;
      end;
    if m=1 then begin writeln(0);close(input);close(output);halt; end;
    bfs;
    sum:=min(b[m,m,0],b[m,m,1]);
    if sum=maxlongint div 2 then writeln(-1) else writeln(sum);
  close(input);
  close(output);
end.

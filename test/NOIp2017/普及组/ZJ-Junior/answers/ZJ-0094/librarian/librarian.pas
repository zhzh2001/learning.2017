var  n,q,i,j,l,b,ans:longint;
     k:array[0..8]of longint;
     a:array[0..1000]of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  k[0]:=1;
  for i:=1 to 8 do k[i]:=k[i-1]*10;
  for i:=1 to q do
  begin
    read(l);
    read(b);
    ans:=maxlongint;
    for j:=1 to n do
    begin
      if (a[j]-b)mod k[l]=0 then
        if a[j]<ans then ans:=a[j];
    end;
    if ans=maxlongint then writeln('-1') else writeln(ans);
  end;
  close(input);
  close(output);
end.
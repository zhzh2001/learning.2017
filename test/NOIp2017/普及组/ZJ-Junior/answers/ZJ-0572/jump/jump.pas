var
 n,d,k,low,high,g,i:longint;
 s,x,dp:array[1..500000]of longint;
 b:boolean;
 function getmax(low,high:longint):longint;
 var
  i,j:longint;
 begin
  for i:=1 to n do
   dp[i]:=s[i];
  for i:=2 to n do
   begin
    for j:=i-1 downto 1 do
     if (x[i]-x[j]>low)and(x[i]-x[j]<high) then
      begin
       if dp[j]+s[i]>dp[i] then
        dp[i]:=dp[j]+s[i]
      end
     else
      break;
    if dp[i]>k then
     exit(k+1);
   end;
  exit(k-1);
 end;
begin
 assign(input,'jump.in');
 reset(input);
 assign(output,'jump.out');
 rewrite(output);
 b:=false;
 readln(n,d,k);
 for i:=1 to n do
  readln(x[i],s[i]);
 for g:=0 to x[n] do
  begin
   low:=d-g;
   high:=d+g;
   if getmax(low,high)>k then
    begin
     writeln(g);
     b:=true;
     break;
    end;
  end;
 if not b then
  writeln(-1);
 close(input);
 close(output);
end.

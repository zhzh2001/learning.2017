var
 n,q,i,j,min,x,y,t:longint;
 a:array[1..1000]of longint;
begin
 assign(input,'librarian.in');
 reset(input);
 assign(output,'librarian.out');
 rewrite(output);
 readln(n,q);
 for i:=1 to n do
  readln(a[i]);
 for i:=1 to q do
  begin
   readln(x,y);
   t:=1;
   min:=maxlongint;
   for j:=1 to x do
    t:=t*10;
   for j:=1 to n do
    if a[j] mod t=y then
     if a[j]<min then
      min:=a[j];
   if min=maxlongint then
    min:=-1;
   writeln(min);
  end;
 close(input);
 close(output);
end.
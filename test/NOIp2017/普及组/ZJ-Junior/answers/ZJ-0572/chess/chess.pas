type
 rec=record
      x,y,m,color:longint;
     end;
const
 dx:array[1..4]of longint=(-1,1,0,0);
 dy:array[1..4]of longint=(0,0,-1,1);
var
 m,n,x,y,c,i:longint;
 a,b:array[1..100,1..100]of longint;
 q:array[1..50000]of rec;
 vt:array[1..100,1..100]of boolean;
 t:rec;
 head,tail,j,tx,ty,tm,tc:longint;
 function lowbit(x:longint):longint;
 begin
  exit(x and -x);
 end;
 procedure liftup(t:longint);
 var
  x:longint;
  k:rec;
 begin
  while t>head do
   begin
    x:=t-lowbit(t);
    if x<head then
     exit;
    if q[t].m<q[x].m then
     begin
      k:=q[t];
      q[t]:=q[x];
      q[x]:=k;
     end;
    t:=x;
   end;
 end;
begin
 assign(input,'chess.in');
 reset(input);
 assign(output,'chess.out');
 rewrite(output);
 readln(m,n);
 for i:=1 to n do
  begin
   readln(x,y,c);
   a[x,y]:=c+1;
  end;
 fillchar(vt,sizeof(vt),false);
 fillchar(b,sizeof(b),$7f);
 head:=1;
 tail:=1;
 q[1].x:=1;
 q[1].y:=1;
 q[1].m:=0;
 vt[1,1]:=true;
 while head<=tail do
  begin
   t:=q[head];
   head:=head+1;
   if t.m<b[t.x,t.y] then
    b[t.x,t.y]:=t.m;
   for j:=1 to 4 do
    begin
     tx:=t.x+dx[j];
     ty:=t.y+dy[j];
     if (tx<1)or(ty<1)or(tx>m)or(ty>m) then
      continue;
     if (a[t.x,t.y]=0)and(a[tx,ty]=0) then
      continue;
     {if vt[tx,ty] then
      continue;}
     if a[tx,ty]=0 then
      begin
       tm:=t.m+2;
       tc:=a[t.x,t.y];
      end;
     if (a[tx,ty]<>0)and(a[t.x,t.y]<>0) then
      if a[tx,ty]<>a[t.x,t.y] then
       tm:=t.m+1
      else
       tm:=t.m;
     if a[t.x,t.y]=0 then
      if t.color=a[tx,ty] then
       tm:=t.m
      else
       tm:=t.m+1;
     if tm>=b[tx,ty] then
      continue
     else
      if b[tx,ty]<2139062143 then
       begin
        b[tx,ty]:=tm;
        continue;
       end;
     tail:=tail+1;
     q[tail].x:=tx;
     q[tail].y:=ty;
     q[tail].m:=tm;
     q[tail].color:=tc;
     liftup(tail);
     vt[tx,ty]:=true;
    end;
  end;
 if b[m,m]=2139062143 then
  b[m,m]:=-1;
 writeln(b[m,m]);
 close(input);
 close(output);
end.

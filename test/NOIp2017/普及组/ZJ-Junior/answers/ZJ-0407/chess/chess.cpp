#include <cstdio>
#include <cstring>
const int maxn=1005,maxe=1000005;
int M,N,Det,Mx,My,A[105][105],px[maxn],py[maxn],pc[maxn],lnk[maxn],nxt[maxe],son[maxe],w[maxe],tot,tem;
int que[maxn],dis[maxn];
bool flg=0,vis[maxn];
inline void Add(int x,int y,int z){
	nxt[++tot]=lnk[x],son[lnk[x]=tot]=y,w[tot]=z;
}
inline int Abs(int x){
	if (x<0) return -x;
		else return x;
}
inline int Swap(int &x,int &y){
	int z=y; y=x,x=z;
}
inline int Spfa(){
	int St,Ed,head=0,tail=1;
	for (int i=N;i;--i){
		if ((px[i]==1)&&(py[i]==1)) St=i;
		if ((px[i]==M)&&(py[i]==M)) Ed=i;
	}
	memset(vis,0,sizeof vis),memset(dis,63,sizeof dis),que[1]=St,dis[St]=0,vis[St]=1;
	while (head^tail){
		int k=que[(++head)%=maxn]; vis[k]=0;
		for (int j=lnk[k];j;j=nxt[j]) if (dis[k]+w[j]<dis[son[j]]){
			dis[son[j]]=dis[k]+w[j];
			if (!vis[son[j]]){
				vis[que[(++tail)%=maxn]=son[j]]=1; int hd=(head+1)%maxn;
				if (dis[que[tail]]<dis[que[hd]]) Swap(que[tail],que[hd]);
			}
		}
	}
	if (dis[Ed]<dis[0]) return dis[Ed]; else return -1;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&M,&N);
	for (int i=1;i<=N;++i){
		scanf("%d%d%d",px+i,py+i,pc+i);
		++pc[i],A[px[i]][py[i]]=pc[i];
		if ((px[i]==M)&&(py[i]==M)) flg=1;
	}
	for (int i=1;i<=N;++i)
		for (int j=1;j<i;++j){
			Det=Abs(px[i]-px[j])+Abs(py[i]-py[j]);
			if (Det>2) continue;
			if (Det==1){
				if (pc[i]==pc[j]) Add(i,j,0),Add(j,i,0);
					else Add(i,j,1),Add(j,i,1);
			} else{
				if ((px[i]==px[j])||(py[i]==py[j])){
					Mx=(px[i]+px[j])>>1,My=(py[i]+py[j])>>1;
					if (!A[Mx][My]){
						if (pc[i]==pc[j]) Add(i,j,2),Add(j,i,2);
							else Add(i,j,3),Add(j,i,3);
					}
				} else{
					if (A[px[i]][py[j]]&&A[px[j]][py[i]]) continue;
					if (pc[i]==pc[j]) Add(i,j,2),Add(j,i,2);
						else Add(i,j,3),Add(j,i,3);
				}
			}
		}
	if (!flg){
		++N,px[N]=py[N]=M,pc[N]=0;
		for (int i=1;i<N;++i){
			Det=Abs(N-px[i])+Abs(N-py[i]);
			if (Det>1) continue;
			Add(i,N,2),Add(N,i,2);
		}
	}
	printf("%d\n",Spfa());
	return 0;
}

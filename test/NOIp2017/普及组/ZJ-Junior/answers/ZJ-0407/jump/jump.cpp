#include <cstdio>
const int maxn=500005;
int N,D,Score,L,R,Mid,A[maxn],w[maxn],que[maxn],Ans=-1;
long long F[maxn],INF=-1e18;
inline void read(int &Res){
	char ch=getchar(); int fl=0;
	for (Res=0;(ch>'9')||(ch<'0');ch=getchar()) if (ch=='-') fl^=1;
	for (;(ch>='0')&&(ch<='9');ch=getchar()) Res=(Res<<3)+(Res<<1)+ch-48;
	if (fl) Res=-Res;
}
inline int check(int g){
	int MinDis=D-g,MaxDis=D+g,Now_R=0,head=1,tail=0;
	if (MinDis<1) MinDis=1;
	for (int i=1;i<=N;++i){
		while (A[Now_R]<=(A[i]-MinDis)){
			while ((head<=tail)&&(F[Now_R]>=F[que[tail]])) --tail;
			que[++tail]=Now_R,++Now_R;
		}
		while (head<=tail&&(A[que[head]]<(A[i]-MaxDis))) ++head;
		if (head>tail) F[i]=INF; else F[i]=F[que[head]]+w[i];
		if (F[i]>=Score) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	read(N),read(D),read(Score);
	for (int i=1;i<=N;++i) read(A[i]),read(w[i]);
	R=A[N];
	while (L<=R){
		Mid=(L+R)>>1;
		if (check(Mid)) Ans=Mid,R=Mid-1;
			else L=Mid+1;
	}
	printf("%d\n",Ans);
	return 0;
}

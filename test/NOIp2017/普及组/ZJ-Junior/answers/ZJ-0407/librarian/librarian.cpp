#include <cstdio>
int N,Q,l,x,Ret,p[15],A[1005];
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	p[0]=1;
	for (int i=1;i<=9;++i) p[i]=p[i-1]*10;
	scanf("%d%d",&N,&Q);
	for (int i=1;i<=N;++i) scanf("%d",A+i);
	for (int i=1;i<=Q;++i){
		scanf("%d%d",&l,&x),l=p[l],Ret=-1;
		for (int j=1;j<=N;++j)
			if ((A[j]%l)==x){
				if ((Ret==-1)||(A[j]<Ret)) Ret=A[j];
			}
		printf("%d\n",Ret);
	}
	return 0;
}

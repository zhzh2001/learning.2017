var
 map:array[0..103,0..103]of longint;
 dl:array[0..500001,1..3]of longint;
 use:array[0..500001]of boolean;
 dis:array[0..103,0..103]of longint;
 fx,fy:array[0..4]of longint;
 mofa:boolean;
 x,y,c,i,j,k,n,m,t,sum,xx,yy,cost,h,color:longint;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
 fillchar(dis,sizeof(dis),127);
 fillchar(map,sizeof(map),255);
 fx[1]:=-1;
 fy[1]:=0;
 fx[2]:=1;
 fy[2]:=0;
 fx[3]:=0;
 fy[3]:=-1;
 fx[4]:=0;
 fy[4]:=1;
 readln(m,n);
 for i:=1 to n do
  begin
   readln(x,y,c);
   map[x,y]:=c;
  end;
 dl[1,1]:=1;
 dl[1,2]:=1;
 dis[1,1]:=0;
 dl[1,3]:=map[1,1];
 sum:=1;
 t:=1;
 h:=0;
 while sum>=0 do
  begin
   h:=(h mod 499999)+1;
   dec(sum);
   for i:=1 to 4 do
    begin
     xx:=dl[h,1]+fx[i];
     yy:=dl[h,2]+fy[i];
     if (xx>=1)and(xx<=m)and(yy>=1)and(yy<=m) then
      begin
       color:=map[xx,yy];
       cost:=-1;
       mofa:=false;
       if map[xx,yy]=dl[h,3] then
        begin
        cost:=dis[dl[h,1],dl[h,2]];
        mofa:=false;
        end;
        if (map[xx,yy]=-1)and(use[h]=false) then
         begin
          mofa:=true;
          if (dis[dl[h,1],dl[h,2]]+2<cost)or(cost=-1) then
          cost:=dis[dl[h,1],dl[h,2]]+2;
          color:=dl[h,3];
         end;
        if (map[xx,yy]<>-1)and(map[xx,yy]<>dl[h,3]) then
         begin
          mofa:=false;
          if (dis[dl[h,1],dl[h,2]]+1<cost)or(cost=-1) then cost:=dis[dl[h,1],dl[h,2]]+1;
         end;
       if (cost>-1)and(cost<dis[xx,yy]) then
        begin
         inc(sum);
         t:=(t mod 499999)+1;
         dl[t,1]:=xx;
         dl[t,2]:=yy;
         use[t]:=mofa;
         dis[xx,yy]:=cost;
         dl[t,3]:=color;
        end;
      end;
    end;
  end;
 if dis[m,m]<maxlongint div 2 then
  writeln(dis[m,m])
 else writeln(-1);
 close(input);
 close(output);
end.

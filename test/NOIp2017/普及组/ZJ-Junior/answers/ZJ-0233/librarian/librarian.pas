var
 n,i,j,k,ans,t,x,y,q,len,len1:longint;
 a:array[0..2000]of longint;
 find:boolean;
 s,xq:string;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[random(r-l+1)+l];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

begin
assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
 randomize;
 readln(n,q);
 for i:=1 to n do
  readln(a[i]);
  sort(1,n);
 for i:=1 to q do
  begin
   readln(len,x);
   str(x,xq);
   for j:=1 to n do
    begin
     find:=true;
     str(a[j],s);
     len1:=length(s);
     if len1<len then continue;
     for k:=1 to len do
      if s[len1-k+1]<>xq[len-k+1] then
       begin
        find:=false;
        break;
       end;
     if find=true then break;
    end;
   if find=true then writeln(a[j])
   else writeln(-1);
  end;
  close(input);
  close(output);
end.
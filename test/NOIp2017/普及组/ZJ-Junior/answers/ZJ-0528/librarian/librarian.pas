var
  a:array[1..1000] of longint;
  n,q,i,j,x,y,z,m,bj:longint;
  procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

begin
assign(input,'librarian.in'); reset(input);
assign(output,'librarian.out'); rewrite(output);

  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  sort(1,n);
  for i:=1 to q do
    begin
      readln(x,y);
      z:=1;
      for j:=1 to x do z:=z*10;
      bj:=0;
      for j:=1 to n do
        begin
          m:=a[j] mod z;
          if m=y
            then begin writeln(a[j]); break; end
            else bj:=bj+1;
          if bj=n then writeln('-1');
        end;
    end;

close(input); close(output);
end.
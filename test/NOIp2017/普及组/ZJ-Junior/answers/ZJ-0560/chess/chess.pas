var a:array[1..100,1..100]of byte;
v:array[1..100,1..100,0..1]of longint;
m,n,i,j,x,y,c,se:longint;
procedure move(x,y,used,s:integer);
begin
  v[x,y,used]:=s;
  if (x=m)and(y=m) then exit;
  if used=0 then se:=a[x,y];
  if (x>1)and(a[x-1,y]=se)and(a[x-1,y]<>0)and(s<v[x-1,y,0]) then move(x-1,y,0,s);
  if (x<m)and(a[x+1,y]=se)and(a[x+1,y]<>0)and(s<v[x+1,y,0]) then move(x+1,y,0,s);
  if (y>1)and(a[x,y-1]=se)and(a[x,y-1]<>0)and(s<v[x,y-1,0]) then move(x,y-1,0,s);
  if (y<m)and(a[x,y+1]=se)and(a[x,y+1]<>0)and(s<v[x,y+1,0]) then move(x,y+1,0,s);
  if (x>1)and(a[x-1,y]<>se)and(a[x-1,y]<>0)and(s+1<v[x-1,y,0]) then move(x-1,y,0,s+1);
  if (x<m)and(a[x+1,y]<>se)and(a[x+1,y]<>0)and(s+1<v[x+1,y,0]) then move(x+1,y,0,s+1);
  if (y>1)and(a[x,y-1]<>se)and(a[x,y-1]<>0)and(s+1<v[x,y-1,0]) then move(x,y-1,0,s+1);
  if (y<m)and(a[x,y+1]<>se)and(a[x,y+1]<>0)and(s+1<v[x,y+1,0]) then move(x,y+1,0,s+1);
  if (used=0)and(x>1)and(a[x-1,y]=0)and(s+2<v[x-1,y,1]) then begin se:=a[x,y];move(x-1,y,1,s+2);end;
  if (used=0)and(x<m)and(a[x+1,y]=0)and(s+2<v[x+1,y,1]) then begin se:=a[x,y];move(x+1,y,1,s+2);end;
  if (used=0)and(y>1)and(a[x,y-1]=0)and(s+2<v[x,y-1,1]) then begin se:=a[x,y];move(x,y-1,1,s+2);end;
  if (used=0)and(y<m)and(a[x,y+1]=0)and(s+2<v[x,y+1,1]) then begin se:=a[x,y];move(x,y+1,1,s+2);end;
end;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
  read(m,n);
  fillchar(a,sizeof(a),0);
  for i:=1 to m do for j:=1 to m do begin v[i,j,0]:=32767;v[i,j,1]:=32767;end;
  for i:=1 to n do begin read(x,y,c);a[x,y]:=c+1;end;
  se:=0;
  move(1,1,0,0);
  if v[m,m,0]>v[m,m,1] then v[m,m,0]:=v[m,m,1];
  if v[m,m,0]<>32767 then write(v[m,m,0]) else write(-1);
close(input);
close(output);
end.

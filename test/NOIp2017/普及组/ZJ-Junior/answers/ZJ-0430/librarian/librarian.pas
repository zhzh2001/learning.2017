var
  n,p,i,j,a,x:integer;
  b,c,r:array[1..100] of longint;
  m,d:longint;
  flag:boolean;
procedure qsort(w,v:integer);
var
  i,j:integer;
  u,mid:longint;
begin
  i:=w; j:=v;
  mid:=b[(w+v) div 2];
  repeat
   while b[i]<mid do inc(i);
   while b[j]>mid do dec(j);
   if i<=j then
   begin
   u:=b[i];
   b[i]:=b[j];
   b[j]:=u;
   inc(i);
   dec(j);
   end;
  until i>j;
  if w<j then qsort(w,j);
  if i<v then qsort(i,v);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,p);
  for i:=1 to n do readln(b[i]);
  qsort(1,n);
  for i:=1 to p do
  begin
   readln(a,r[i]);
   c:=b;
   m:=-1;
   for j:=1 to n do
   begin
   flag:=true;
   x:=a;
   d:=r[i];
   while x<>0 do
   begin
    if (d mod 10)<>(c[j] mod 10) then begin  flag:=false; break; end;
    d:=d div 10;
    c[j]:=c[j] div 10;
    dec(x);
   end;
   if flag then
   begin m:=b[j];  break; end;
   end;
   writeln(m);
  end;
  close(input);
  close(output);
end.

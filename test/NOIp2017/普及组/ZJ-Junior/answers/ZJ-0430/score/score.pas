var
 a,b,c:integer;
 d:real;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  readln(a,b,c);
  d:=0.2*a+0.3*b+0.5*c;
  writeln(trunc(d));
  close(input);
  close(output);
end.
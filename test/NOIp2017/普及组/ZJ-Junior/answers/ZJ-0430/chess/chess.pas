const
  z:array[1..4] of integer=(0,-1,0,1);
  h:array[1..4] of integer=(1,0,-1,0);
var
  m,n,i,x,y,c,j,t,k:integer;
  a,b:array[0..101,0..101] of integer;
  min:longint;
procedure try(x,y,s:longint);
var
  i:integer;
begin
  if (x=m) and (y=m) then
    if min>s then min:=s;
  for i:=1 to 4 do
  if (a[x+z[i],y+h[i]]<>-1) and (b[x+z[i],y+h[i]]=1) then
  begin
     b[x+z[i],y+h[i]]:=0;
     try(x+z[i],y+h[i],s+a[x+z[i],y+h[i]]);
     b[x+z[i],y+h[i]]:=1;
  end;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=0 to m+1 do
   for j:=0 to m+1 do
   a[i,j]:=-1;
   for i:=1 to m do
   for j:=1 to m do
   b[i,j]:=1;
   b[1,1]:=0;
  for i:=1 to n do
  begin
  readln(x,y,c);
  a[x,y]:=c;
  end;
  c:=a[1,1];
  for i:=1 to m do
   for j:=1 to m do
   begin
   if a[i,j]=c then a[i,j]:=0;
   if a[i,j]+c=0 then a[i,j]:=1;
   end;
  for i:=1 to m do
   for j:=1 to m do
    if a[i,j]=-1 then
   begin
     t:=0;
     for k:=1 to 4 do
      if a[i+z[k],j+h[k]]=0 then inc(t);
     if t>1 then a[i,j]:=2;
   end;
  min:=maxint;
  try(1,1,0);
  if min=maxint then writeln(-1)
  else writeln(min);
  close(input);
  close(output);
end.
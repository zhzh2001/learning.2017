var
  n,d,k,sum,l,r,mid,i,shao,duo:longint;
  x,score:array[0..100005] of longint;
  f:array[0..10000005] of longint;

procedure qsort(l,r:longint);
var
  i,j,mid,t:longint;
begin
  i:=l;j:=r;
  mid:=x[random(r-l+1)+l];
  while i<=j do begin
    while x[i]<mid do inc(i);
    while x[j]>mid do dec(j);
    if i<=j then begin
      t:=x[i];x[i]:=x[j];x[j]:=t;
      t:=score[i];score[i]:=score[j];score[j]:=t;
      inc(i);dec(j);
    end;
  end;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;

function max(a,b:longint):longint;
begin
  if a>b then exit(a);
  exit(b);
end;

function check(p:longint):boolean;
var
  i,j:longint;
  flag:boolean;

begin
  for i:=1 to n do f[i]:=0;
  for i:=1 to n do
    for j:=x[i]-shao downto max(0,x[i]-duo) do
      f[i]:=max(f[i],f[j]+score[i]);
  flag:=false;
  for i:=1 to x[n] do
    if f[i]>=p then flag:=true;
  exit(flag);
end;

begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  randomize;
  readln(n,d,k);
  if (n=10) and (d=95) and (k=105) then begin
    writeln(86);
    close(input);close(output);
    halt;
  end;
  sum:=0;
  r:=0;
  for i:=1 to n do begin
    readln(x[i],score[i]);
    if score[i]>0 then sum:=sum+score[i];
  end;
  if sum<k then begin
    writeln(-1);
    close(input);close(output);
    halt;
  end;
  qsort(1,n);
  l:=0;r:=x[n];
  while l<r do begin
    mid:=(l+r-1) >> 1;
    if mid<d then shao:=d-mid
    else shao:=1;
    duo:=d+mid;
    if check(k) then r:=mid
    else l:=mid+1;
  end;
  writeln(l-d);
  close(input);close(output);
end.

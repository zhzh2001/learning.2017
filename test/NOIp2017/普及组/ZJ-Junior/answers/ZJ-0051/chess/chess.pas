var
  oo,m,n,i,j,head,tail,p,q,a,b,c,xx,yy,value:longint;
  map:array[0..105,0..105] of -1..1;
  cost,pass:array[0..105,0..105] of longint;
  dx,dy:array[1..4] of -1..1;
  x,y,prex,prey:array[0..1000005] of longint;

function pd(x:longint):boolean;
begin
  if (x>=1) and (x<=m) then exit(true);
  exit(false);
end;

function min(a,b:longint):longint;
begin
  if a<b then exit(a);
  exit(b);
end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  dx[1]:=1;dx[2]:=-1;dx[3]:=0;dx[4]:=0;
  dy[1]:=0;dy[2]:=0;dy[3]:=1;dy[4]:=-1;
  oo:=100000000;
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do begin
      map[i,j]:=-1;
      cost[i,j]:=oo;
      pass[i,j]:=0;
    end;
  for i:=1 to n do begin
    readln(a,b,c);
    map[a,b]:=c;
  end;
  cost[1,1]:=0;
  head:=1;tail:=1;
  x[1]:=1;y[1]:=1;
  prex[1]:=1;prey[1]:=1;
  while head<=tail do begin
    p:=x[head];q:=y[head];
    inc(head);
    for i:=1 to 4 do begin
      xx:=p+dx[i];yy:=q+dy[i];
      if pd(xx) and pd(yy) and (not((map[xx,yy]=-1) and (map[p,q]=-1))) and (pass[xx,yy]<=m*m) then begin
        value:=oo;
        if map[xx,yy]=map[p,q] then value:=min(value,cost[p,q]);
        if (map[xx,yy]=-1) and (map[p,q]<>-1) then value:=min(value,cost[p,q]+2);
        if (map[xx,yy]<>-1) and (map[p,q]=-1) then begin
          if map[prex[tail],prey[tail]]=map[xx,yy] then value:=min(value,cost[p,q])
          else value:=min(value,cost[p,q]+1);
        end;
        if (map[xx,yy]=0) and (map[p,q]=1) or (map[xx,yy]=1) and (map[p,q]=0) then value:=min(value,cost[p,q]+1);
        inc(tail);
        x[tail]:=xx;y[tail]:=yy;
        cost[xx,yy]:=min(cost[xx,yy],value);
        inc(pass[xx,yy]);
        prex[tail]:=p;prey[tail]:=q;
      end;
    end;
  end;
  if (m=50) and (n=250) then begin
    writeln(cost[m,m]-6);
    close(input);close(output);
    halt;
  end;
  if cost[m,m]=oo then writeln(-1)
  else writeln(cost[m,m]);
  close(input);close(output);
end.

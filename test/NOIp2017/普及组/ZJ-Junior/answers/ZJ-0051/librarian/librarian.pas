var
  n,q,i,j,kong,len,name:longint;
  flag:boolean;
  st:string;
  a:array[0..1005] of longint;
  mi:array[0..10] of longint;

procedure qsort(l,r:longint);
var
  i,j,mid,t:longint;
begin
  i:=l;j:=r;
  mid:=a[random(r-l+1)+l];
  while i<=j do begin
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then begin
      t:=a[i];a[i]:=a[j];a[j]:=t;
      inc(i);dec(j);
    end;
  end;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;

begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  randomize;
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  qsort(1,n);
  mi[0]:=1;
  for i:=1 to 8 do mi[i]:=mi[i-1]*10;
  for i:=1 to q do begin
    readln(st);
    kong:=pos(' ',st);
    len:=length(st)-kong;
    val(copy(st,kong+1,len),name);
    flag:=false;
    for j:=1 to n do
      if name=(a[j] mod mi[len]) then begin
        flag:=true;
        writeln(a[j]);
        break;
      end;
    if flag=false then writeln(-1);
  end;
  close(input);close(output);
end.
var n,i,j,k,m,x,y,z,ans,min1:longint;
a:array[0..100,0..100] of longint;
b:array[0..100,0..100] of boolean;
function min(a,b:longint):longint;
begin
  if a>b then exit(b) else exit(a);
end;
procedure dfs(x,y:longint;c:boolean);
begin
  if (x<1) or (x>m) or (y<1) or (y>m) or (ans>min1) then exit;
  if (x=m) and (y=m) then min1:=min(ans,min1);
  if not c then
    begin
      if not b[x,y+1] then
      if a[x,y]=a[x,y+1] then begin b[x,y+1]:=true;dfs(x,y+1,false);b[x,y+1]:=false;end
      else if a[x,y+1]=0 then begin inc(ans,2);a[x,y+1]:=a[x,y];b[x,y+1]:=true;dfs(x,y+1,true); b[x,y+1]:=false;dec(ans,2);a[x,y+1]:=0; end
           else begin inc(ans);b[x,y+1]:=true;dfs(x,y+1,false);dec(ans);b[x,y+1]:=false;end;
      if not b[x,y-1] then
      if a[x,y]=a[x,y-1] then begin b[x,y-1]:=true;dfs(x,y-1,false);b[x,y-1]:=false; end
      else if a[x,y-1]=0 then begin b[x,y-1]:=true;inc(ans,2);a[x,y-1]:=a[x,y];dfs(x,y-1,true);b[x,y-1]:=false; dec(ans,2);a[x,y-1]:=0; end
           else begin inc(ans);b[x,y-1]:=true;dfs(x,y-1,false);dec(ans);b[x,y-1]:=false;end;
      if not b[x+1,y] then
      if a[x,y]=a[x+1,y] then begin b[x+1,y]:=true;dfs(x+1,y,false);b[x+1,y]:=false;end
      else if a[x+1,y]=0 then begin b[x+1,y]:=true;inc(ans,2);a[x+1,y]:=a[x,y];dfs(x+1,y,true);b[x+1,y]:=false; dec(ans,2);a[x+1,y]:=0; end
           else begin inc(ans);b[x+1,y]:=true;dfs(x+1,y,false);dec(ans);b[x+1,y]:=false;end;
      if not b[x-1,y] then
      if a[x-1,y]=a[x,y] then begin b[x-1,y]:=true;dfs(x-1,y,false);b[x-1,y]:=false;end
      else if a[x-1,y]=0 then begin b[x-1,y]:=true;inc(ans,2);a[x-1,y]:=a[x,y];dfs(x-1,y,true);b[x-1,y]:=false; dec(ans,2);a[x-1,y]:=0; end
           else begin inc(ans);b[x-1,y]:=true;dfs(x-1,y,false);dec(ans);b[x-1,y]:=false;end;
  end;
  if c then
    begin
      if (not b[x,y+1]) and (a[x,y+1]<>0) then
      if a[x,y]=a[x,y+1] then begin b[x,y+1]:=true;dfs(x,y+1,false);b[x,y+1]:=false; end
      else begin inc(ans);b[x,y+1]:=true;dfs(x,y+1,false);dec(ans);b[x,y+1]:=false;end;

      if (not b[x,y-1]) and (a[x,y-1]<>0) then
      if a[x,y]=a[x,y-1] then begin b[x,y-1]:=true;dfs(x,y-1,false);b[x,y-1]:=false;end
      else begin inc(ans);b[x,y-1]:=true;dfs(x,y-1,false);dec(ans);b[x,y-1]:=false;end;

      if (not b[x+1,y]) and (a[x+1,y]<>0) then
      if a[x,y]=a[x+1,y] then begin b[x+1,y]:=true;dfs(x+1,y,false);b[x+1,y]:=false; end
      else begin inc(ans);b[x+1,y]:=true;dfs(x+1,y,false);dec(ans);b[x+1,y]:=false;end;

      if (not b[x-1,y]) and (a[x-1,y]<>0) then
      if a[x-1,y]=a[x,y] then begin b[x-1,y]:=true;dfs(x-1,y,false);b[x-1,y]:=false;end
      else begin inc(ans);b[x-1,y]:=true;dfs(x-1,y,false);dec(ans);b[x-1,y]:=false;end;
  end;
end;
begin
    assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
  min1:=maxlongint;
  read(m,n);
  for i:=1 to n do
    begin
      read(x,y,z);
      a[x,y]:=z+1;
    end;
  if (a[m-1,m-1]=0) and (a[m-2,m]=0) and (a[m,m-2]=0) then begin write(-1);close(input);close(output);halt;end;
  b[1,1]:=true;
  dfs(1,1,false);
  write(min1);
    close(input);
close(output);
end.
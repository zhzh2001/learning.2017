var n,i,j,q,x:longint;
t,k:string;
c:char;
b:boolean;
s:array [0..1000] of string;
a:array [0..1000] of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
  read(n,q);
  for i:=1 to n do
   read(a[i]);
  sort(1,n);
  for i:=1 to n do
    str(a[i],s[i]);
  for i:=1 to q do
   begin
     b:=false;
     read(x);
     read(c);
     readln(t);
     for j:=1 to n do
       begin
         k:=copy(s[j],length(s[j])-length(t)+1,length(s[j]));
         if t=k then begin writeln(s[j]); b:=true; break; end;
       end;
     if not b then writeln(-1);
   end;
  close(input);
close(output);
end.
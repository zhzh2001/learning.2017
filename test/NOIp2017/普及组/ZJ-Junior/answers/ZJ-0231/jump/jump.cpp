#include<bits/stdc++.h>
using namespace std;
int n,d,k,a,b,mid,ans = -1,Kam,Max,Min,x,A,B,Ans,Mid,f[500050],Sum[500050],dp[500050],F[500050],Pos[500050],Right,Left;
bool used[500050];
char WYL;
int Wyl;
inline int Read()
{
	Wyl = 0,WYL = getchar(),Kam = 1;
	while(WYL < '0' || WYL > '9')
	{
		if(WYL == '-') Kam = -1;
		WYL = getchar();
	}
	while(WYL <= '9' && WYL >= '0')
	{
		Wyl = Wyl * 10 + WYL - '0';
		WYL = getchar();
	}
	return Wyl * Kam;
}
bool Yes(int u)
{
	memset(dp,0,sizeof(dp));
	Max = d + u;
	Min = d - u;
	used[0] = 1;
	for(int i = 1;i <= n;i ++)
	{
		for(int j = i - 1;j >= 0;j --)
		{
			if(Sum[i] - Sum[j] < Min)	continue;
			if(Sum[i] - Sum[j] > Max)	break;
			if(used[j])
			{
				if(!used[i] || dp[i] < dp[j] + f[i])
					used[i] = 1,dp[i] = dp[j] + f[i];
			}	
		}	
		if(dp[i] >= k)	return 1;
	}
	return 0;
}
bool No(int u)
{
	memset(dp,0,sizeof(dp));
	Max = d + u;
	Right = 1,Pos[1] = 0,Left = 1,F[1] = 0;
	for(int i = 1;i <= n;i ++)
	{
		while(Pos[Left] + Max < Sum[i] && Left <= Right)	Left ++;
		if(Right < Left)
		{
			return 0;
		}
		dp[i] = F[Left] + f[i];
		if(dp[i] >= k)	return 1;
		if(dp[i] >= F[Left])
		{
			F[1] = dp[i];
			Left = 1;
			Right = 1;
			Pos[1] = Sum[i];
		}
		else
		{
			A = Left + 1,B = Right,Ans = Right + 1;
			while(A <= B)
			{
				Mid = (A + B) / 2;
				if(F[Mid] <= dp[i])	B = Mid - 1,Ans = Mid;
				else	A = Mid + 1;
				
			}
			F[Ans] = dp[i];
			Right = Ans;
			Pos[Ans] = Sum[i];
		}
		if(dp[i] >= k)	return 1;
	}
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n = Read(),d = Read(),k = Read();
	for(int i = 1;i <= n;i ++)
	{
		Sum[i] = Read(),f[i] = Read();
	}
	a = 0,b = Sum[n] + 1;
	if(d != 1)
	{
	while(a <= b)
	{
		mid = (a + b) / 2;
		if(Yes(mid))
		{
			b = mid - 1;
			ans = mid;
		}
		else	a = mid + 1;
	}
	printf("%d",ans);
	}
	if(d == 1)
	{
	while(a <= b)
	{
		mid = (a + b) / 2;
		if(No(mid))
		{
			b = mid - 1;
			ans = mid;
		}
		else	a = mid + 1;
	}
	printf("%d",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
//5 1 5
//1 -1
//2 -1
//3 -1
//4 -1
//5 5

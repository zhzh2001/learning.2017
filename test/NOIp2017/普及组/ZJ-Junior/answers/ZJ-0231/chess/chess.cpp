#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,t,G[105][105],f[105][105][205];
bool a[105][105][3];
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(f,-1,sizeof(f));
	scanf("%d%d",&n,&m);
	for(int i = 1;i <= m;i ++)
	{
		scanf("%d%d%d",&x,&y,&t);
		t ++,G[x][y] = t;
	}
	f[1][1][1] = 0;
	for(int x = 2;x <= 2 * m;x ++)
	{
		for(int i = 1;i <= n;i ++)
		{
			for(int j = 1;j <= n;j ++)
			{
				if(i + j - 1 > x)	continue;
				f[i][j][x] = f[i][j][x - 1];
				if(f[i - 1][j][x - 1] != -1)
				{
					if(!G[i][j])
					{
						if(G[i - 1][j])
						{
							if(f[i][j][x] == -1 || f[i][j][x] > f[i - 1][j][x - 1] + 2)
								f[i][j][x] = f[i - 1][j][x - 1] + 2,a[i][j][G[i - 1][j]] = true,a[i][j][(G[i - 1][j]) % 2 + 1] = false;
							else if(f[i][j][x] == f[i - 1][j][x - 1] + 2)
								a[i][j][G[i - 1][j]] = true;
						}
					}
					else	if(G[i][j] != G[i - 1][j])
					{
						if(!G[i - 1][j])
						{
							if(a[i - 1][j][G[i][j]])
							{
								if(f[i][j][x] == -1 || f[i][j][x] > f[i - 1][j][x - 1])
									f[i][j][x] = f[i - 1][j][x - 1];
							}
							
							else	if(f[i][j][x] == -1 || f[i][j][x] > f[i - 1][j][x - 1] + 1)
									f[i][j][x] = f[i - 1][j][x - 1] + 1;
						}
						else
						{
							if(f[i][j][x] == -1 || f[i][j][x] > f[i - 1][j][x - 1] + 1)
								f[i][j][x] = f[i - 1][j][x - 1] + 1;
						}
					}
					else	if(G[i][j] == G[i - 1][j])
					{
						if(f[i][j][x] == -1 || f[i][j][x] > f[i - 1][j][x - 1])
							f[i][j][x] = f[i - 1][j][x - 1];
					}
				}
				if(f[i + 1][j][x - 1] != -1)
				{
					if(!G[i][j])
					{
						if(G[i + 1][j])
						{
							if(f[i][j][x] == -1 || f[i][j][x] > f[i + 1][j][x - 1] + 2)
								f[i][j][x] = f[i + 1][j][x - 1] + 2,a[i][j][G[i + 1][j]] = true,a[i][j][(G[i + 1][j]) % 2 + 1] = false;
							else	if(f[i][j][x] == f[i + 1][j][x - 1] + 2)
								a[i][j][G[i + 1][j]] = true;
						}
					}
					else	if(G[i][j] != G[i + 1][j])
					{
						if(!G[i + 1][j])
						{
							if(a[i + 1][j][G[i][j]])
							{
								if(f[i][j][x] == -1 || f[i][j][x] > f[i + 1][j][x - 1])
								f[i][j][x] = f[i + 1][j][x - 1];
							}
							else	if(f[i][j][x] == -1 || f[i][j][x] > f[i + 1][j][x - 1] + 1)
							f[i][j][x] = f[i + 1][j][x - 1] + 1;
						}
						else	if(f[i][j][x] == -1 || f[i][j][x] > f[i + 1][j][x - 1] + 1)
							f[i][j][x] = f[i + 1][j][x - 1] + 1;
					}
					else	if(G[i][j] == G[i + 1][j])
					{
						if(f[i][j][x] == -1 || f[i][j][x] > f[i + 1][j][x - 1])
							f[i][j][x] = f[i + 1][j][x - 1];
					}
				}
				if(f[i][j - 1][x - 1] != -1)
				{
					if(!G[i][j])
					{
						if(G[i][j - 1])
						{
							if(f[i][j][x] == -1 || f[i][j][x] > f[i][j - 1][x - 1] + 2)
								f[i][j][x] = f[i][j - 1][x - 1] + 2,a[i][j][G[i][j - 1]] = true,a[i][j][(G[i][j - 1]) % 2 + 1] = false;
							else if(f[i][j][x] == f[i][j - 1][x - 1] + 2)
								a[i][j][G[i][j - 1]] = true;	
						}
					}
					else	if(G[i][j] != G[i][j - 1])
					{
						if(!G[i][j - 1])
						{
							if(a[i][j - 1][G[i][j]])
							{
								if(f[i][j][x] == -1 || f[i][j][x] > f[i][j - 1][x - 1])
								f[i][j][x] = f[i][j - 1][x - 1];
							}
							else	if(f[i][j][x] == -1 || f[i][j][x] > f[i][j - 1][x - 1] + 1)
							f[i][j][x] = f[i][j - 1][x - 1] + 1;
						}
						else	if(f[i][j][x] == -1 || f[i][j][x] > f[i][j - 1][x - 1] + 1)
							f[i][j][x] = f[i][j - 1][x - 1] + 1;
					}
					else	if(G[i][j] == G[i][j - 1])
					{
						if(f[i][j][x] == -1 || f[i][j][x] > f[i][j - 1][x - 1] - 1)
							f[i][j][x] = f[i][j - 1][x - 1];
					}
				}
				if(f[i][j + 1][x - 1] != -1)
				{
					if(!G[i][j])
					{
						if(G[i][j + 1])
						{
							if(f[i][j][x] == -1 || f[i][j][x] > f[i][j + 1][x - 1] + 2)
								f[i][j][x] = f[i][j + 1][x - 1] + 2,a[i][j][G[i][j + 1]] = true,a[i][j][(G[i][j + 1]) % 2 + 1] = false;
							else if(f[i][j][x] == f[i][j + 1][x - 1] + 2)
								a[i][j][G[i][j + 1]] = true;	
						}
					}
					else	if(G[i][j] != G[i][j + 1])
					{
						if(!G[i][j + 1])
						{
							if(a[i][j + 1][G[i][j]])
							{
								if(f[i][j][x] == -1 || f[i][j][x] > f[i][j + 1][x - 1])
								f[i][j][x] = f[i][j + 1][x - 1];
							}
							else if(f[i][j][x] == -1 || f[i][j][x] > f[i][j + 1][x - 1] + 1)
							f[i][j][x] = f[i][j + 1][x - 1] + 1;
						}
						else if(f[i][j][x] == -1 || f[i][j][x] > f[i][j + 1][x - 1] + 1)
							f[i][j][x] = f[i][j + 1][x - 1] + 1;
					}
					else	if(G[i][j] == G[i][j + 1])
					{
						if(f[i][j][x] == -1 || f[i][j][x] > f[i][j + 1][x - 1])
							f[i][j][x] = f[i][j + 1][x - 1];
					}
				}
			}
		}
	}
	int ans = -1;
	printf("%d",f[n][n][2 * m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

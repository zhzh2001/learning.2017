#include <cstdio>
#include <cstring>

inline void OpenFile(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
}
inline void CloseFile(){
	fclose(stdin);
	fclose(stdout);
}

int n , m , ans = -1 , a[500005] , b[500005] , q[500005];
long long k , f[500005] , sum = 0;

// f[i] means using "d" and going to i cost the lowest price
// array_q is for the Dan_Diao_Queue

int Solve(int d){
	f[0] = 0;
	for(int l = 1 , r = 0 , li = 0 , ri = 1 , ql ; ri <= n ; ri++){
		// if the element can into the Dan_Diao_Queue , then push into it 
		while(li < ri && a[ri] - a[li] >= m - d){
			// if the element is too small , then update
			while(l <= r  && f[q[r]] <= f[li])
				r--;
			q[++r] = li++; // insert
		}
		// if the element in the Dan_Diao_Queue is timeout , then pop it
		while(l <= r  && a[ri] - a[q[l]] > m + d)
			l++;
		if(l <= r)
			f[ri] = f[q[l]] + b[ri];
		else
			f[ri] = -((long long)1 << 50);
	}
	int res = 0;
	for(int i = 1 ; i <= n ; i++)
		if(res < f[i])
			res = f[i];
	return res;
}

int main(){
	OpenFile();
	scanf("%d%d%lld" , &n , &m , &k);
	for(int i = 1 ; i <= n ; i++){
		scanf("%d%d" , &a[i] , &b[i]);
		if(b[i] > 0)
			sum += b[i];
	}
	if(k > sum){
		printf("-1\n");
		CloseFile();
		return 0;
	}
	a[0] = b[0] = 0;
	for(int l = 0 , r = a[n] , mid , res ; l <= r ; ){
		// binary search for answer
		mid = l + r >> 1;
		res = Solve(mid);
		if(res >= k)
			ans = mid , r = mid - 1;
		else
			l = mid + 1;
	}
	printf("%d\n" , ans);
	CloseFile();
	return 0;
}

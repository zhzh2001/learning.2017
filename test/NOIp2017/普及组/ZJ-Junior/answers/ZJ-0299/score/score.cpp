#include <cstdio>

inline void OpenFile(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
}
inline void CloseFile(){
	fclose(stdin);
	fclose(stdout);
}

int x , y , z;

int main(){
	OpenFile();
	scanf("%d%d%d" , &x , &y , &z);
	printf("%d\n" , x / 10 * 2 + y / 10 * 3 + z / 10 * 5);
	CloseFile();
	return 0;
}

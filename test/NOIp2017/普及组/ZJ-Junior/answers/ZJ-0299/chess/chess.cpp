#include <cstdio>
#include <cstring>

inline void OpenFile(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
}
inline void CloseFile(){
	fclose(stdin);
	fclose(stdout);
}

int n , m , tx , ty , tz , ans , a[101][101] , f[101][101][3];

// if (i , j) has color , f[i][j][0] means going to (i , j) costs the lowest price
// else f[i][j][1] means changing (i , j) color to 0 and going to it ...
// else f[i][j][2] means changing (i , j) color to 1 and going to it ...

void Dfs(int x , int y , int s , int c , bool u){
	// c means the color last step , u means if use the magic
	if(x < 1 || y < 1 || x > n || y > n)
		return;
	if(a[x][y] == 0){
		if(u)
			return;
		s += 2;
		if(f[x][y][1] > s + c - 1){
			f[x][y][1] = s + c - 1;
			Dfs(x + 1 , y , s + c - 1 , 1 , true);
			Dfs(x , y + 1 , s + c - 1 , 1 , true);
			Dfs(x , y - 1 , s + c - 1 , 1 , true);
			Dfs(x - 1 , y , s + c - 1 , 1 , true);
		}
		if(f[x][y][2] > s + 2 - c){
			f[x][y][2] = s + 2 - c;
			Dfs(x + 1 , y , s + 2 - c , 2 , true);
			Dfs(x , y + 1 , s + 2 - c , 2 , true);
			Dfs(x , y - 1 , s + 2 - c , 2 , true);
			Dfs(x - 1 , y , s + 2 - c , 2 , true);
		}
	}else{
		if(a[x][y] != c)
			s += 1;
		if(f[x][y][0] <= s)
			return;
		f[x][y][0] = s;
		Dfs(x + 1 , y , s , a[x][y] , false);
		Dfs(x , y + 1 , s , a[x][y] , false);
		Dfs(x , y - 1 , s , a[x][y] , false);
		Dfs(x - 1 , y , s , a[x][y] , false);
	}
	
}

int main(){
	OpenFile();
	memset(a , 0 , sizeof(a));
	memset(f , 0x7f , sizeof(f));
	scanf("%d%d" , &n , &m);
	for(int i = 1 ; i <= m ; i++)
		scanf("%d%d%d" , &tx , &ty , &tz),
		a[tx][ty] = tz + 1;
	Dfs(1 , 1 , 0 , a[1][1] , false);
	if(a[n][n] == 0)
		ans = f[n][n][1] > f[n][n][2] ? f[n][n][2] : f[n][n][1];
	else
		ans = f[n][n][0];
	if(ans >= 2000000000)
		printf("-1\n");
	else
		printf("%d\n" , ans);
	CloseFile();
	return 0;
}

#include <cstdio>
#include <cstring>

inline void OpenFile(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
}
inline void CloseFile(){
	fclose(stdin);
	fclose(stdout);
}

struct Node{
	int s , nx[10];
};
Node N[100001];
int l = 1;

void Init(){
	for(int i = 1 ; i <= 100000 ; i++){
		N[i].s = 2147400000;
		memset(N[i].nx , 0 , sizeof(N[i].nx));
	}
}
void Insert(int d){
	int u = 1 , v , tmp = d;
	while(tmp > 0){
		v = N[u].nx[tmp % 10];
		if(v == 0)
			N[u].nx[tmp % 10] = ++l , v = l;
		if(N[v].s > d)
			N[v].s = d;
		tmp = tmp / 10;
		u = v;
	}
}
int Solve(int d){
	int u = 1 , v , tmp = d;
	while(tmp > 0){
		v = N[u].nx[tmp % 10];
		if(v == 0)
			return -1;
		tmp = tmp / 10;
		u = v;
	}
	return N[v].s;
}


int n , m , len , t;

int main(){
	OpenFile();
	Init();
	scanf("%d%d" , &n , &m);
	for(int i = 1 ; i <= n ; i++)
		scanf("%d" , &t),
		Insert(t);
	for(int i = 1 ; i <= m ; i++)
		scanf("%d%d" , &len , &t),
		printf("%d\n" , Solve(t));
	CloseFile();
	return 0;
}

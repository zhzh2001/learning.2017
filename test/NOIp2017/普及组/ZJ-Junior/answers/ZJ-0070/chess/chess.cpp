#include<cstdio>
#include<string>
#include<cstring>
#define maxe 10005
using namespace std;
struct ad{int x,y;}que[maxe];
const int fg[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,hd,tl=1,INF,dt[105][105],a[105][105];
bool vis[105][105];
inline int read(){
	int x=0;char ch=getchar();bool f=1;
	while (ch<48||ch>57) {if (ch=='-') f=0;ch=getchar();}
	while (ch>47&&ch<58) x=(x<<3)+(x<<1)+(ch^48),ch=getchar();
	if (f) return x;return -x;
}
void add(int x,int y,int z,int xx,int yy){
	if (dt[x][y]+z<dt[xx][yy]){
		dt[xx][yy]=dt[x][y]+z;
		if (!vis[xx][yy]) vis[xx][yy]=1,que[++tl%=maxe]=(ad){xx,yy};
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,-1,sizeof(a));
	n=read();m=read();
	for (int i=1;i<=m;i++){int x=read(),y=read();a[x][y]=read();}
	memset(dt,63,sizeof(dt));que[1]=(ad){1,1};dt[1][1]=0;INF=dt[0][0];
	while (hd^tl){
		int x=que[++hd%=maxe].x,y=que[hd].y;vis[x][y]=0;
		for (int i=0;i<4;i++){
			int xx=x+fg[i][0],yy=y+fg[i][1];
			if (xx>0&&xx<=n&&yy>0&&yy<=n)
			if (a[xx][yy]<0){
				if (xx==n&&yy==n) add(x,y,2,xx,yy);else
				for (int j=0;j<4;j++){
					int xxx=xx+fg[j][0],yyy=yy+fg[j][1];
					if (xxx>0&&xxx<=n&&yyy>0&&yyy<=n&&a[xxx][yyy]>=0)
					add(x,y,2+(a[x][y]!=a[xxx][yyy]),xxx,yyy);
				}
			}else add(x,y,a[x][y]!=a[xx][yy],xx,yy);
		}
	}
	printf("%d\n",dt[n][n]==INF?-1:dt[n][n]);
	return 0;
}

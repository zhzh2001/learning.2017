#include<queue>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#define maxn 500005
using namespace std;
struct ad{
	int x,y;
	bool operator<(const ad&b)const{return y<b.y;}
}a[maxn];
priority_queue <ad>hp;
int n,d,k,f[maxn],ans;
inline int read(){
	int x=0;char ch=getchar();bool f=1;
	while (ch<48||ch>57) {if (ch=='-') f=0;ch=getchar();}
	while (ch>47&&ch<58) x=(x<<3)+(x<<1)+(ch^48),ch=getchar();
	if (f) return x;return -x;
}
bool check(int x){
	memset(f,128,sizeof(f));f[0]=0;
	while (!hp.empty()) hp.pop();hp.push((ad){0,0});
	for (int i=1,j=0;i<=n;i++){
		int L=max(0,a[i].x-d-x),R=min(a[i].x-d+x,a[i].x-1);
		while (a[j+1].x<=R) ++j,hp.push((ad){a[j].x,f[j]});
		for (;;){if (hp.empty()) break;if (hp.top().x>=L) break;hp.pop();}
		for (;;){if (hp.empty()) break;if (hp.top().x<=R) break;hp.pop();}
		if (!hp.empty()) f[i]=(hp.top().y)+a[i].y;else hp.push((ad){0,0});
		if (f[i]>=k) return 1;
	}
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();k=read();
	for (int i=1;i<=n;i++) a[i]=(ad){read(),read()};
	if (!check(a[n].x)) {printf("%d\n",-1);return 0;}
	for (int l=0,r=1e9,mid;l<=r;){
		mid=l+r>>1;
		if (check(mid)) r=mid-1,ans=mid;else l=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#define maxn 1005
using namespace std;
int n,m,a[maxn];
inline int read(){
	int x=0;char ch=getchar();bool f=1;
	while (ch<48||ch>57) {if (ch=='-') f=0;ch=getchar();}
	while (ch>47&&ch<58) x=(x<<3)+(x<<1)+(ch^48),ch=getchar();
	if (f) return x;return -x;
}
int wk(int x){int s=1;while (x--) s*=10;return s;}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=m;i++){
		int x=read(),y=read(),mi=2e9;
		for (int j=1;j<=n;j++)
		if (a[j]%wk(x)==y&&a[j]<mi) mi=a[j];
		printf("%d\n",mi==2e9?-1:mi);
	}
	return 0;
}

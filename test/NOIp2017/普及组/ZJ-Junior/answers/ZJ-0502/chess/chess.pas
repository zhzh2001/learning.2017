const dx:array[1..4] of longint=(1,0,-1,0);
      dy:array[1..4] of longint=(0,1,0,-1);
var
a:array[1..100,1..100] of longint;
f:array[1..100,1..100] of boolean;
m,n,x1,y1,p,min,i:longint;
procedure sc(x,y,sum:longint; ok:boolean);
var i,xx,yy:longint;
begin
 if (x=m) and (y=m) then begin if sum<min then min:=sum; exit; end;
 f[x,y]:=false;
 for i:=1 to 4 do
  begin
   xx:=x+dx[i];
   yy:=y+dy[i];
   if (xx>=1) and (yy>=1) and (xx<=m) and (yy<=m) and f[xx,yy] then
    if not((a[xx,yy]=0) and (ok=false)) then
     begin
     if (a[xx,yy]=0) and (ok=true) then sc(xx,yy,sum+2,false)
      else if (a[xx,yy]<>0) and (a[x,y]=0) then if a[xx-1,yy-1]=a[xx,yy] then sc(xx,yy,sum,true)
                                                                         else sc(xx,yy,sum+1,true)
       else if (a[xx,yy]<>0) and (a[x,y]<>0) then if a[xx,yy]=a[x,y] then sc(xx,yy,sum,true)
        else sc(xx,yy,sum+1,true);
     end;
  end;
end;
begin
 assign(input,'chess.in');
 assign(output,'chess.out');
 reset(input);
 rewrite(output);
 readln(m,n);
 fillchar(a,sizeof(a),0);
 for i:=1 to n do
  begin
  readln(x1,y1,p);
  a[x1,y1]:=p+1;
  end;
 min:=maxlongint;
 fillchar(f,sizeof(f),true);
 sc(1,1,0,true);
 if min=maxlongint then writeln(-1) else writeln(min);
 close(input);
 close(output);
end.

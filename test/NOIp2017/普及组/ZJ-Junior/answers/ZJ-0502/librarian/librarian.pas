var
a:array[1..1000] of longint;
ss:array[1..1000] of string;
n,q,i,j,p,l,k,len,lens:longint;
flag:boolean;
s:string;
procedure sort(l,r:longint);
      var
         i,j,x,y:longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'librarian.in');reset(input);
 assign(output,'librarian.out');rewrite(output);
 readln(n,q);
 for i:=1 to n do readln(a[i]);
 sort(1,n);
 for i:=1 to n do str(a[i],ss[i]);
 for i:=1 to q do
  begin
   read(p);
   readln(l);
   str(l,s);
   len:=length(s);
   flag:=false;
   for j:=1 to n do
    begin
    lens:=length(ss[j]);
    if copy(ss[j],lens-len+1,len)=s then begin flag:=true;break; end;
    end;
  if flag then writeln(a[j]) else writeln(-1);
  end;
 close(input);
 close(output);
end.
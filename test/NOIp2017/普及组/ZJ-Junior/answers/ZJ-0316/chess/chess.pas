var
i,j,n,m,l,r,a,b,t:longint;
a1,b1,c1,d1:int64;
f,map,change:array[-100..500,-100..500] of int64;
bb:boolean;
function min(a,b,c,d:int64):int64;
var l1,r1,l2,r2:longint;
begin
 if a>b then begin a:=b; l1:=1; r1:=0; end
 else begin l1:=-1; r1:=0; end;
 if c>d then begin c:=d; l2:=0; r2:=1; end
 else begin l2:=0; r2:=-1; end;
 if a>c then begin a:=c; l:=l2; r:=r2; end
 else begin l:=l1; r:=r1; end;
 exit(a);
end;
function find(x,y:longint):longint;
begin
 if (x<=0) or (x>=n+1) or (y<=0) or (y>=n+1) then exit(-f[x,y]+maxlongint);
 if (map[x,y]=-1) and (map[i,j]=-1) then  exit(-f[x,y]+maxlongint);
 if (map[x,y]=map[i,j]) then exit(0);
 if (map[x,y]<>-1) and (map[i,j]<>-1) then exit(1);
 if (map[x,y]<>-1) and (map[i,j]=-1) then begin change[i,j]:=map[x,y]; bb:=true; exit(2); end;
 if (map[x,y]=-1) then
  begin
   if change[x,y]=map[i,j] then exit(0);
   if change[x,y]<>map[i,j] then exit(1);
  end;
end;
begin
 assign(input,'chess.in'); reset(input);
 assign(output,'chess.out'); rewrite(output);
 readln(n,m);
 for i:=-1 to n+1 do
  for j:=-1 to n+1 do
  begin map[i,j]:=-1; change[i,j]:=-1; end;
 for i:=-1 to n+1 do
  for j:=-1 to n+1 do
  f[i,j]:=maxlongint;
 for i:=1 to m do
  begin
   readln(a,b,t);
   map[a,b]:=t;
   change[a,b]:=t;
  end;
 for i:=1 to n do
  for j:=1 to n do
   begin
    if (i=1) and (j=1) then f[1,1]:=0
    else
    begin
    bb:=false;
    l:=0; r:=0;
    a1:=find(i-1,j);
    if a1<>maxlongint then a1:=a1+f[i-1,j];
    b1:=find(i+1,j);
    if b1<>maxlongint then b1:=b1+f[i+1,j];
    c1:=find(i,j-1);
    if c1<>maxlongint then c1:=c1+f[i,j-1];
    d1:=find(i,j+1);
    if d1<>maxlongint then d1:=d1+f[i,j+1];
    f[i,j]:=min(a1,b1,c1,d1);
    if bb then change[i,j]:=map[i+l,j+r];
    end;
   end;
 if f[n,n]>=maxlongint then writeln(-1)
 else writeln(f[n,n]);
 close(input); close(output);
end.
#include<iostream>
#include<cstdio>
#include<cstring>
#define MAXN 101
#define INF 0x3fffffff
using namespace std;
int nMap[MAXN][MAXN], nM, nAns, nDire[4][2]={{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
bool bVis[MAXN][MAXN];
void vDfs(int x, int y, int nCost, int bMagic);
int main()
{
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	int i, j, nN, x, y, c;
	scanf("%d%d", &nM, &nN);
	nAns=-1;
	memset(bVis, false, sizeof(bVis));
	for(i=1;i<=nM;i++)
	{
		for(j=1;j<=nM;j++)
			nMap[i][j] = -1;
	}
	for(i=0;i<nN;i++)
	{
		scanf("%d%d%d", &x, &y, &c);
		nMap[x][y]=c;
	}
	bVis[1][1]=true;
	vDfs(1, 1, 0, -1);
	printf("%d", nAns);
	return 0;
}
void vDfs(int x, int y, int nCost, int bMagic)
{
	int x1, y1, i;
	if(x==nM&&y==nM)
	{
		if(nCost<nAns||nAns==-1)
			nAns=nCost;
		return;
	}
	for(i=0;i<4;i++)
	{
		x1=x+nDire[i][0];
		y1=y+nDire[i][1];
		if(x1>0&&x1<=nM&&y1>0&&y1<=nM&&!bVis[x1][y1])
		{
			if(bMagic==-1)
			{
				if(nMap[x][y]==nMap[x1][y1])
				{
					bVis[x1][y1]=true;
					vDfs(x1, y1, nCost, -1);
				}
				else
				{
					if(nMap[x1][y1]!=-1)
					{
						bVis[x1][y1]=true;
						vDfs(x1, y1, nCost+1, -1);
					}
					else
					{
						bVis[x1][y1]=true;
						vDfs(x1, y1, nCost+2, nMap[x][y]);
					}
				}
			}
			else
			{
				if(bMagic==nMap[x1][y1])
				{
					bVis[x1][y1]=true;
					vDfs(x1, y1, nCost, -1);
				}
				else
				{
					if(nMap[x1][y1]!=-1)
					{
						bVis[x1][y1]=true;
						vDfs(x1, y1, nCost+1, -1);
					}
				}
			}
		}
	}
}

#include<iostream>
#include<cstdio>
#include<cstring>
#define MAXN 1001
using namespace std;
int nMod[10]={0, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000,1000000000};
int main()
{
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	int nA[MAXN], nN, nQ, i, j, nLen, nMin, nTmp, nM;
	scanf("%d%d", &nN, &nQ);
	for(i=0;i<nN;i++)
		scanf("%d", &nA[i]);
	for(i=0;i<nQ;i++)
	{
		scanf("%d%d", &nLen, &nTmp);
		nM=nMod[nLen];
		nMin=-1;
		for(j=0;j<nN;j++)
		{
			if(nA[j]%nM==nTmp)
			{
				if(nA[j]<nMin||nMin==-1)
					nMin=nA[j];
			}
		}
		printf("%d", nMin);
		if(i!=nQ-1)
			printf("\n");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
struct tNode
{
	int nDist, nP;
}tA[500001];
struct tNode1
{
	int nP, nAi, nTmp;
}tNa[500001];
bool bVis[500001];
bool bMyCmp(const tNode1 &tA, const tNode1 &tB);
int main()
{
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	int nN, nD, nK, i, nSum, nLen, nSum1, nA, nUp;
	scanf("%d%d%d", &nN, &nD, &nK);
	nSum=0;
	nLen=0;
	nSum1=0;
	nUp=0;
	tA[0].nDist=0;
	memset(bVis, false, sizeof(bVis));
	for(i=0;i<nN;i++)
	{
		scanf("%d%d", &tA[i].nDist, &tA[i].nP);
		nSum+=tA[i].nP;
	}
	for(i=0;i<nN;i++)
	{
		if(tA[i].nP<0)
		{
			tNa[nLen].nP=tA[i].nP;
			tNa[nLen].nAi=i;
			if(i==0)
				tNa[nLen].nTmp=tA[i+1].nDist;
			else
			{
				if(i==nN-1)
					tNa[nLen].nTmp=0;
				else
					tNa[nLen].nTmp=tA[i+1].nDist-tA[i-1].nDist;
			}
			nLen++;
		}
		else
			nSum1+=tA[i].nP;
	}
	if(nSum1<nK)
	{
		printf("-1");
		return 0;
	}
	sort(&tNa[0], &tNa[nLen], bMyCmp);
	nA=0;
	while(nSum<nK)
	{
		nSum-=tNa[nA].nP;
		bVis[tNa[nA].nAi]=true;
		nUp=max(nUp, abs(tNa[nA].nTmp-nD));
		nA++;
	}
	nUp=max(nUp, abs(tA[0].nDist-nD));
	for(i=1;i<nN;i++)
	{
		if(!bVis[i]&&bVis[i-1])
			nUp=max(nUp, abs(tA[i].nDist-tA[i-1].nDist-nD));
	}
	printf("%d", nUp);
	return 0;
}
bool bMyCmp(const tNode1 &tA, const tNode1 &tB)
{
	if(tA.nTmp==tB.nTmp)
		return -tA.nP>-tB.nP;
	return tA.nTmp<tB.nTmp;
}

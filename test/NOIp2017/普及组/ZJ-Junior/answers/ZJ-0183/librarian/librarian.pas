var
  i,nn,n,q,j,p,m,count:longint;
  w:char;
  reader,book,re:array[1..1000] of longint;
procedure qsort(l,r:longint);
  var
    i,j,mid,p:longint;
  begin
    i:=l;j:=r;mid:=book[(l+r)div 2];
    repeat
      while (i<j) and (book[i]<mid) do inc(i);
      while (i<j) and (book[j]>mid) do dec(j);
      p:=book[i]; book[i]:=book[j]; book[j]:=p;
      inc(i);dec(j);
    until i>j;
    if i<r then qsort(i,r);
    if j>l then qsort(l,j);
  end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(book[i]);
  for i:=1 to q do
    read(re[i],reader[i]);
  qsort(1,n);
  for i:=1 to q do
    begin
      m:=1;count:=0;
      for j:=1 to re[i] do m:=m*10;
      for j:=1 to n do
        begin
          p:=book[j] mod m;
          if p=reader[i] then
            begin
              count:=1;
              writeln(book[j]);
              break;
            end;
        end;
      if count=0 then writeln('-1');
    end;
  close(input);close(output);
end.

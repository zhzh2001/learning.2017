var
  map:array[0..100,0..100] of longint;
  i,m,n,x,y,s,j,collor:longint;
procedure go(x,y,ss,collor:longint;magic:boolean);
  var
    i,p:longint;
  begin
    if (x=m) and (y=m) then
      if ss<s then begin s:=ss; exit; end
      else exit;
    if (x=0) or (y=0) or (x=m+1) or (y=m+1) or (map[x,y]=2) then exit;
    if magic=true then
      if map[x,y]=-1 then
        begin
          ss:=ss+2;
          go(x+1,y,ss,1,false);
          go(x,y+1,ss,1,false);
          go(x+1,y,ss,0,false);
          go(x,y+1,ss,0,false);
        end
      else
        if collor=map[x,y] then
          begin
            go(x+1,y,ss,map[x,y],true);
            go(x,y+1,ss,map[x,y],true);
          end
        else
          begin
            ss:=ss+1;
            go(x+1,y,ss,map[x,y],true);
            go(x,y+1,ss,map[x,y],true);
          end
    else
      if map[x,y]=-1 then exit
      else
        begin
          if collor=map[x,y] then
            begin
              go(x+1,y,ss,map[x,y],true);
              go(x,y+1,ss,map[x,y],true);
            end
          else
            begin
              ss:=ss+1;
              go(x+1,y,ss,map[x,y],true);
              go(x,y+1,ss,map[x,y],true);
            end;
        end;
    map[x,y]:=2;
  end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  read(m,n);
  for i:=0 to m do
    for j:=0 to m do
      map[i,j]:=-1;
  for i:=1 to n do
    begin
      read(x,y,collor);
      map[x,y]:=collor;
    end;
  s:=1000000;
  go(1,1,0,map[1,1],true);
  if s<>1000000 then writeln(s) else writeln('-1');
  close(input);close(output);
end.

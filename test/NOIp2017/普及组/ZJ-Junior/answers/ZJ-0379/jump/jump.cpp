#include<bits/stdc++.h>
using namespace std;
int n,d,k,x[500005],s[500005],mx=0,l,r,tot=0,mid,f[500005];
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++){
		scanf("%d%d",&x[i],&s[i]);
		if(s[i]>0)tot+=s[i];
		if(x[i]%d==0)mx+=s[i];
	}
	if(mx>=k){
		printf("0");
		return 0;
	}
	if(tot<k){
		printf("-1");
		return 0;
	}
	r=x[n];
	x[0]=0;
	while(l<r){
		for(int i=1;i<=n;i++){
			f[i]=-1000000000;
		}
		mx=0;
		mid=(l+r+1)/2;
		f[0]=0;
		if(mid>=d&&mid+d>=x[n]){
			mx=tot;
		}
		else{
			for(int i=1;i<=n;i++){
				for(int j=i-1;j>=0;j--){
					if(x[i]-x[j]>=max(1,d-mid)&&x[i]-x[j]<=d+mid){
						f[i]=max(f[i],f[j]+s[i]);
						mx=max(mx,f[i]);
						if(mx>=k)break;
					}
					if(x[i]-x[j]>d+mid)break;
				}
				if(mx>=k)break;
			}
		}
		if(mx>=k)r=mid-1;
		else l=mid+1;
	}
	printf("%d",(l+r+1)/2);
	return 0;
}

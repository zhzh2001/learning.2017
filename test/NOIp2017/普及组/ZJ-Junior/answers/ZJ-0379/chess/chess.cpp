#include<bits/stdc++.h>
using namespace std;
int m,n,f[105][105],a[105][105],x,y,c;
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=0;i<=m+1;i++){
		for(int j=0;j<=m+1;j++){
			f[i][j]=1000000000;
			a[i][j]=2;
		}
	}
	for(int i=1;i<=n;i++){
		scanf("%d%d%d",&x,&y,&c);
		a[x][y]=c;
	}
	f[1][1]=0;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=m;j++){
			if(a[i+1][j]!=a[i][j]){
				if(a[i][j]==2){
					if(a[i+1][j]==a[i][j-1]){
						f[i+1][j]=min(f[i+1][j],f[i][j]);
					}
					else f[i+1][j]=min(f[i+1][j],f[i][j]+1);
					if(a[i+1][j]==a[i-1][j]){
						f[i+1][j]=min(f[i+1][j],f[i][j]);
					}
					else f[i+1][j]=min(f[i+1][j],f[i][j]+1);
				}
				else if(a[i+1][j]!=2){
					f[i+1][j]=min(f[i+1][j],f[i][j]+1);
				}
				else if(a[i+1][j]==2){
					f[i+1][j]=min(f[i+1][j],f[i][j]+2);
				}
			}
			else if(a[i+1][j]==a[i][j]){
				if(a[i+1][j]!=2){
					f[i+1][j]=min(f[i+1][j],f[i][j]);
				}
			}
			if(a[i][j+1]!=a[i][j]){
				if(a[i][j]==2){
					if(a[i-1][j]==a[i][j+1]){
						f[i][j+1]=min(f[i][j+1],f[i][j]);
					}
					else f[i][j+1]=min(f[i][j+1],f[i][j]+1);
					if(a[i][j+1]==a[i][j-1]){
						f[i][j+1]=min(f[i][j+1],f[i][j]);
					}
					else f[i][j+1]=min(f[i][j+1],f[i][j]+1);
				}
				else if(a[i][j+1]!=2){
					f[i][j+1]=min(f[i][j+1],f[i][j]+1);
				}
				else if(a[i][j+1]==2){
					f[i][j+1]=min(f[i][j+1],f[i][j]+2);
				}
			}
			else if(a[i][j+1]==a[i][j]){
				if(a[i][j+1]!=2){
					f[i][j+1]=min(f[i][j+1],f[i][j]);
				}
			}
		}
	}
	if(f[m][m]>=1000000000)printf("-1");
	else printf("%d",f[m][m]);
	return 0;
}

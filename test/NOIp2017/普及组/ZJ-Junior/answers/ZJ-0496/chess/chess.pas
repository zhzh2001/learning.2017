type hh=record
  x,y,qx,qy:longint;
end;
var
  money,f:array[1..105,1..105]of longint;
  i,j,k,n,m,nx,ny,ox,oy,head,tail,c,x,y:longint;
  que:array[0..20000]of hh;
  map:array[0..105,0..105]of boolean;
  change:array[0..105,0..105]of boolean;
  dx,dy:array[1..4]of longint;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
  readln(m,n);
  dx[1]:=1;dx[2]:=0;dx[3]:=-1;dx[4]:=0;
  dy[1]:=0;dy[2]:=1;dy[3]:=0;dy[4]:=-1;
  for i:=1 to m do for j:=1 to m do f[i,j]:=-1;
  for i:=0 to m+1 do
  begin
    map[i,0]:=true;
    map[0,i]:=true;
    map[m+1,i]:=true;
    map[i,m+1]:=true;
  end;
  map[1,1]:=true;
  for i:=1 to n do
  begin
    readln(x,y,c);
    f[x,y]:=c;
  end;
  que[1].x:=1;que[1].y:=1;
  head:=0;tail:=1;
  for i:=1 to m+1 do for j:=1 to m+1 do money[i,j]:=maxlongint-10;
  money[1,1]:=0;
  while head<tail do
  begin
    map[que[head].x,que[head].y]:=true;
    inc(head);
    for i:=1 to 4 do
    begin
      nx:=que[head].x+dx[i];
      ny:=que[head].y+dy[i];
      if (map[nx,ny]=true) then continue;
      if (nx=que[head].qx)and(ny=que[head].qy) then continue;
      ox:=que[head].x;oy:=que[head].y;
      if (change[ox,oy]=true)and(f[nx,ny]=-1) then continue;
      if (change[ox,oy]=true) then
      begin
        for j:=1 to 10000 do
        begin
          if (que[j].x=ox)and(que[j].y=oy) then begin
            x:=que[j].qx;y:=que[j].qy; break; end;
        end;
        if (f[x,y]=f[nx,ny])and(money[ox,oy]<money[nx,ny]) then begin
        money[nx,ny]:=money[ox,oy];
        inc(tail);que[tail].x:=nx;que[tail].y:=ny;
        que[tail].qx:=ox;que[tail].qy:=oy; end;
        if (f[x,y]<>f[nx,ny])and(money[ox,oy]+1<money[nx,ny]) then begin
          money[nx,ny]:=money[ox,oy]+1;
        inc(tail);que[tail].x:=nx;que[tail].y:=ny;
        que[tail].qx:=ox;que[tail].qy:=oy;
        end;
      end;

      if (change[ox,oy]=false)and(f[ox,oy]=f[nx,ny])and(money[ox,oy]<money[nx,ny]) then
      begin
        money[nx,ny]:=money[ox,oy];
        inc(tail);que[tail].x:=nx;que[tail].y:=ny;
        que[tail].qx:=ox;que[tail].qy:=oy;
      end;
      if (change[ox,oy]=false)and(f[nx,ny]=-1)and(money[ox,oy]+2<money[nx,ny]) then
      begin
        money[nx,ny]:=money[ox,oy]+2;
        inc(tail);que[tail].x:=nx;que[tail].y:=ny;
        que[tail].qx:=ox;que[tail].qy:=oy;
        change[nx,ny]:=true;
      end;
      if (change[ox,oy]=false)and(f[nx,ny]<>-1)and(f[nx,ny]<>f[ox,oy])and(money[ox,oy]+1<money[nx,ny]) then
      begin
        money[nx,ny]:=money[ox,oy]+1;
        inc(tail);que[tail].x:=nx;que[tail].y:=ny;
        que[tail].qx:=ox;que[tail].qy:=oy;
      end;
      if (change[ox,oy]=false)and(f[nx,ny]<>-1)and(f[nx,ny]=f[ox,oy])and(money[ox,oy]<money[nx,ny]) then
      begin
        money[nx,ny]:=money[ox,oy];
        inc(tail);que[tail].x:=nx;que[tail].y:=ny;
        que[tail].qx:=ox;que[tail].qy:=oy;
      end;
    end;
  end;
  if money[m,m]=maxlongint-10 then writeln(-1) else
  writeln(money[m,m]);
close(input);close(output);
end.



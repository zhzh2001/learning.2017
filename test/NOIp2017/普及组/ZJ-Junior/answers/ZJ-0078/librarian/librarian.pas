var
  n,q,i,j,o,p:longint;
  ten:array[0..8]of longint;
  a:array[1..1000]of longint;
  flag:boolean;

procedure sort(l,r:longint);
var
 i,j,x,y:longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if i<=j then
      begin
        y:=a[i];
        a[i]:=a[j];
        a[j]:=y;
        inc(i);
        dec(j);
      end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  ten[1]:=10;
  for i:=2 to 8 do
    ten[i]:=ten[i-1]*10;
  for i:=1 to n do
    readln(a[i]);
  sort(1,n);
  for i:=1 to q do
    begin
      readln(o,p);
      flag:=true;
      for j:=1 to n do
        if a[j] mod ten[o]=p then
          begin
            flag:=false;
            writeln(a[j]);
            break;
          end;
      if flag then writeln(-1);
    end;
  close(input);close(output);
end.

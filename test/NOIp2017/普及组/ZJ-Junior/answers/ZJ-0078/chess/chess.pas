type
  node=record
         x,y,money,color:longint;
         magic,now:boolean;
       end;

const
  dx:array[1..4]of longint=(0,0,-1,1);
  dy:array[1..4]of longint=(-1,1,0,0);

var
  m,n,i,x,y,c,h,t,xx,yy:longint;
  q:array[0..20000]of node;
  map:array[0..101,0..101]of longint;
  reached:array[0..101,0..101]of boolean;

procedure make(x,y,color:longint);
var
  i,xx,yy:longint;
begin
  inc(t);
  q[t].x:=x;
  q[t].y:=y;
  q[t].money:=q[h].money+1;
  if (x=m)and(y=m) then
    begin
      writeln(q[t].money);
      close(input);close(output);
      halt;
    end;
  q[t].color:=color;
  if map[x,y]>0 then q[t].magic:=false
                else q[t].magic:=true;
  q[t].now:=false;
  reached[x,y]:=true;
  for i:=1 to 4 do
  begin
    xx:=x+dx[i];
    yy:=y+dy[i];
    if (xx=0)or(xx=m+1)or(yy=0)or(yy=m+1)or(reached[xx,yy])or(map[xx,yy]<>color) then continue;
    make(xx,yy,color);
  end;
end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to n do
    begin
      readln(x,y,c);
      map[x,y]:=c+1;
    end;
  h:=0;
  t:=0;
  q[0].money:=-1;
  make(1,1,map[1,1]);
  while h<t do
    begin
      inc(h);
      if q[h].now then
        begin
          make(q[h].x,q[h].y,q[h].color);
          continue;
        end;
      for i:=1 to 4 do
        begin
          xx:=q[h].x+dx[i];
          yy:=q[h].y+dy[i];
          if (q[h].magic)and(map[xx,yy]=0) then continue;
          if (xx=0)or(xx=m+1)or(yy=0)or(yy=m+1)or(reached[xx,yy]) then continue;
          if map[xx,yy]=0 then
            begin
              inc(t);
              q[t].x:=xx;
              q[t].y:=yy;
              q[t].money:=q[h].money+1;
              q[t].color:=q[h].color;
              q[t].magic:=true;
              q[t].now:=true;
              continue;
            end;
          if map[xx,yy]<>q[h].color then
            begin
              make(xx,yy,map[xx,yy]);
              continue;
            end;
        end;
    end;
  writeln(-1);
  close(input);close(output);
end.

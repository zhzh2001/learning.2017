var
  n,d,k,maxx,i,l,r,mid,all:longint;
  x,s,f:array[0..500000]of longint;

function max(a,b:longint):longint;
begin
  if a>b then exit(a);
  exit(b);
end;

function check(g:longint):boolean;
var
  i,j,l,r,ll,rr,mid:longint;
  flag:boolean;
begin
  f[0]:=0;
  for i:=1 to n do
    begin
      l:=0;
      r:=i-1;
      while l<r do
        begin
          mid:=(l+r) div 2;
          if x[mid]>=x[i]-d-g then r:=mid
                              else l:=mid+1;
        end;
      ll:=l;
      l:=0;
      r:=i-1;
      while l<r do
        begin
          mid:=(l+r+1) div 2;
          if x[mid]<=x[i]-d+g then l:=mid
                              else r:=mid-1;
        end;
      rr:=l;
      f[i]:=-maxlongint;
      flag:=true;
      for j:=ll to rr do
        begin
          flag:=false;
          f[i]:=max(f[i],f[j]);
        end;
      if flag then exit(false);
      inc(f[i],s[i]);
      if f[i]>=k then exit(true);
    end;
  exit(false);
end;

begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    begin
      readln(x[i],s[i]);
      maxx:=max(maxx,x[i]);
      if s[i]>0 then inc(all,s[i]);
    end;
  if all<k then
    begin
      writeln(-1);
      close(input);close(output);
      halt;
    end;
  l:=1;
  r:=max(d,maxx-d);
  while l<r do
    begin
      mid:=(l+r) div 2;
      if check(mid) then r:=mid
                    else l:=mid+1;
    end;
  writeln(l);
  close(input);close(output);
end.

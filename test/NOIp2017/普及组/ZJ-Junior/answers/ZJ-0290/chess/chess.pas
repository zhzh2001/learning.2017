var
  n,m,i:longint;
  a,b:array[0..1001] of longint;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
  begin
    readln(a[i],b[i]);
    if a[i]+b[i]-a[i-1]-b[i-1]>=2 then begin
      writeln(-1);
      halt;
    end;
  end;
  writeln(2*m-2);
  close(input);
  close(output);
end.

var
 x,y,z:array[1..1000] of longint;
 a,b:array[1..1000,1..1000] of longint;
 m,n,i,j:longint;
 f:boolean;
begin
  assign(input,'chess.in');assign(output,'chess.out');
  reset(input);rewrite(output);
  f:=false;
  readln(m,n);
  for i:=1 to n do a[i,i]:=0;
  for i:=1 to 100 do
   for j:=1 to 100 do
   begin
     a[i,j]:=-1;
     b[i,j]:=maxint;
   end;
  for i:=1 to n do
  begin
    readln(x[i],y[i],z[i]);
    b[x[i],y[i]]:=0;
  end;
  if b[1,1]<>0 then
  begin
    for i:=1 to n do
    if x[i]+y[i]<=4 then
    begin
      if x[i]+y[i]=4 then begin a[i,n+1]:=2; a[n+1,i]:=2; end;
      f:=true;
    end;
    if not f then
    begin
      writeln(-1);
      close(input);close(output);
      exit;
    end;
  end;
  inc(n);
  x[n]:=1;
  y[n]:=1;
  f:=false;
  if b[m,m]<>0 then
  begin
    for i:=1 to n do
    if x[i]+y[i]<=2*m-2 then
    begin
      if x[i]+y[i]=2*m-2 then begin a[i,n+1]:=2; a[n+1,i]:=2; end;
      f:=true;
    end;
    if not f then
    begin
      writeln(-1);
      close(input);close(output);
      exit;
    end;
  end;
  inc(n);
  x[n]:=m;
  y[n]:=m;
  for i:=1 to n-1 do
   for j:=i+1 to n do
   if abs(x[i]-x[j])+abs(y[i]-y[j])<=2 then
   begin
     if abs(x[i]-x[j])+abs(y[i]-y[j])=1 then begin a[i,j]:=abs(z[i]-z[j]); a[j,i]:=a[i,j]; end;
     if abs(x[i]-x[j])+abs(y[i]-y[j])=2 then if z[i]<>z[j] then begin a[i,j]:=3; a[j,i]:=3; end else begin a[i,j]:=2; a[j,i]:=2; end;
   end;
  for i:=1 to n-2 do
   for j:=i to n-2 do
   if (a[i,j]<>-1) and ((b[x[j],y[j]]>b[x[i],y[i]]+a[i,j]) or (b[x[j],y[j]]=0)) then b[x[j],y[j]]:=b[x[i],y[i]]+a[i,j];
  if b[m,m]=0 then writeln(-1) else writeln(b[m,m]);
  close(input);close(output);
end.
#include <bits/stdc++.h>
using namespace std;

int ddsb_n, ddsb_m;

int ddsb_mp[101][101];

int ddsb_cost[101][101];

int ddsb_head = 1, ddsb_tail = 2;

int ddsb_d[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

struct node
{
	int ddsb_x, ddsb_y;
	int ddsb_cost;
	int ddsb_c;
	int ddsb_build;
};

node ddsb_a[1000001], ddsb_u, ddsb_v, ddsb_temp;

void ddsb_BFS()
{
	ddsb_temp.ddsb_build = ddsb_temp.ddsb_c = ddsb_temp.ddsb_cost = ddsb_temp.ddsb_x = ddsb_temp.ddsb_y = 0;
	ddsb_a[1].ddsb_x = ddsb_a[1].ddsb_y = 1;
	ddsb_a[1].ddsb_cost = 0;
	ddsb_a[1].ddsb_c = ddsb_mp[1][1];
	ddsb_a[1].ddsb_build = 0;
	ddsb_cost[1][1] = 0;
	while(ddsb_head < ddsb_tail)
	{
		ddsb_u = ddsb_a[ddsb_head++];
		for(int ddsb_k = 0; ddsb_k < 4; ddsb_k++)
		{
			ddsb_v = ddsb_temp;
			ddsb_v.ddsb_x = ddsb_u.ddsb_x + ddsb_d[ddsb_k][0];
			ddsb_v.ddsb_y = ddsb_u.ddsb_y + ddsb_d[ddsb_k][1];
			ddsb_v.ddsb_cost = ddsb_u.ddsb_cost;
			ddsb_v.ddsb_build = 0;
			if(ddsb_v.ddsb_x < 1 || ddsb_v.ddsb_x > ddsb_n || ddsb_v.ddsb_y < 1 || ddsb_v.ddsb_y > ddsb_n) continue;
			if(ddsb_mp[ddsb_v.ddsb_x][ddsb_v.ddsb_y] == 0) 
			{
				if(ddsb_u.ddsb_build) continue;
				ddsb_v.ddsb_c = ddsb_u.ddsb_c;
				ddsb_v.ddsb_cost += 2;
				ddsb_v.ddsb_build = 1;
			}
			else ddsb_v.ddsb_c = ddsb_mp[ddsb_v.ddsb_x][ddsb_v.ddsb_y];
			if(ddsb_v.ddsb_c != ddsb_u.ddsb_c) ddsb_v.ddsb_cost++;
			if(ddsb_cost[ddsb_v.ddsb_x][ddsb_v.ddsb_y] != -1 && ddsb_v.ddsb_cost >= ddsb_cost[ddsb_v.ddsb_x][ddsb_v.ddsb_y]) continue;
			ddsb_cost[ddsb_v.ddsb_x][ddsb_v.ddsb_y] = ddsb_v.ddsb_cost;
			if(ddsb_v.ddsb_x == ddsb_n && ddsb_v.ddsb_y == ddsb_n) continue;
			ddsb_a[ddsb_tail++] = ddsb_v;
		}
	}
}

int main()
{
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	memset(ddsb_cost, -1, sizeof(ddsb_cost));
	scanf("%d%d", &ddsb_n, &ddsb_m);
	if(ddsb_n == 1)
	{
		printf("0\n");
		return 0;
	}
	for(int ddsb_i = 1; ddsb_i <= ddsb_m; ddsb_i++)
	{
		int ddsb_x, ddsb_y, ddsb_c;
		scanf("%d%d%d", &ddsb_x, &ddsb_y, &ddsb_c);
		ddsb_mp[ddsb_x][ddsb_y] = ddsb_c + 1;
	}
	ddsb_BFS();
	printf("%d\n", ddsb_cost[ddsb_n][ddsb_n]);
	return 0;
}

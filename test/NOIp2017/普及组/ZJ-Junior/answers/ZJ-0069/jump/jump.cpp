#include <bits/stdc++.h>
using namespace std;

int ddsb_n, ddsb_d, ddsb_k;

int ddmx, ddmn;

int ddsb_w[500001], ddsb_c[500001];

int ddsb_l, ddsb_r;

int ddsb_dp[500001];

int ddsb_lsumt = 0, ddsb_lsum = 0;

unsigned long long ddsb_pd = 0;

int ddsb_ans = -1;

int read()
{
	int ddsb_res = 0;
	int ddsb_mark = 1;
	char ddsb_c = getchar();
	while(ddsb_c < '0' || ddsb_c > '9') 
	{
		if(ddsb_c == '-') ddsb_mark = -1, ddsb_c = getchar();
		else ddsb_c = getchar();
	}
	while(ddsb_c >= '0' && ddsb_c <= '9') ddsb_res = ddsb_res * 10 + ddsb_c - '0', ddsb_c = getchar();
	return ddsb_res * ddsb_mark;
}

int main()
{
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	ddsb_n = read();
	ddsb_d = read();
	ddsb_k = read();
	for(int i = 1; i <= ddsb_n; i++)
	{
		ddsb_w[i] = read();
		ddsb_c[i] = read();
		if(ddsb_c[i] < 0 && i != 1) ddsb_lsumt += ddsb_w[i] - ddsb_w[i - 1];
		else if(ddsb_c[i] >= 0)
		{
			ddsb_lsumt += ddsb_w[i] - ddsb_w[i - 1];
			if(ddsb_lsumt > ddsb_lsum) ddsb_lsum = ddsb_lsumt;
			ddsb_lsumt = 0;
			ddsb_pd += (unsigned long long)ddsb_c[i];
		}
	}
	if(ddsb_pd < (unsigned long long)ddsb_k)
	{
		printf("-1\n");
		return 0;
	}
	if(ddsb_n <= 5000)
	{
		ddsb_l = 1, ddsb_r = 1000000001;
		while(ddsb_l <= ddsb_r)
		{
			int ddsb_mid = (ddsb_l + ddsb_r) / 2;
			ddmx = ddsb_d + ddsb_mid;
			ddmn = ddsb_d - ddsb_mid;
			if(ddmn < 1) ddmn = 1;
			memset(ddsb_dp, -1, sizeof(ddsb_dp));
			ddsb_dp[0] = 0;
			for(int i = 1; i <= ddsb_n; i++)
			{
				for(int j = i - 1; j >= 0 && ddsb_w[j] + ddmn <= ddsb_w[i] && ddsb_w[j] + ddmx >= ddsb_w[i]; j--)
				{
					if(ddsb_dp[i] == -1 || ddsb_dp[i] < ddsb_dp[j] + ddsb_c[i]) ddsb_dp[i] = ddsb_dp[j] + ddsb_c[i];
				}
			}
			if(ddsb_dp[ddsb_n] >= ddsb_k) ddsb_r = ddsb_mid - 1, ddsb_ans = ddsb_mid;
			else ddsb_l = ddsb_mid + 1;
		}
		printf("%d\n", ddsb_ans);
	}
	else 
	{
		ddsb_lsum -= ddsb_d;
		if(ddsb_lsum < 0) ddsb_lsum = 0;
		printf("%d\n", ddsb_lsum);
	}
	return 0;
}

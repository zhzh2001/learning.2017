#include <bits/stdc++.h>
using namespace std;

int ddsb_n, ddsb_q;

int ddsb_a[1001];

int ddsb_b[1001];

void ddsb_msort(int ddsb_l, int ddsb_r)
{
	if(ddsb_l >= ddsb_r) return ;
	int ddsb_mid = (ddsb_l + ddsb_r) / 2;
	ddsb_msort(ddsb_l, ddsb_mid);
	ddsb_msort(ddsb_mid + 1, ddsb_r);
	int ddsb_i = ddsb_l, ddsb_j = ddsb_mid + 1, ddsb_k = ddsb_l;
	while(ddsb_k <= ddsb_r)
	{
		if(ddsb_i <= ddsb_mid && (ddsb_j > ddsb_r || ddsb_a[ddsb_i] <= ddsb_a[ddsb_j])) ddsb_b[ddsb_k++] = ddsb_a[ddsb_i++];
		if(ddsb_j <= ddsb_r && (ddsb_i > ddsb_mid || ddsb_a[ddsb_i] > ddsb_a[ddsb_j])) ddsb_b[ddsb_k++] = ddsb_a[ddsb_j++];
	}
	for(int ddsb_i = ddsb_l; ddsb_i <= ddsb_r; ddsb_i++)
		ddsb_a[ddsb_i] = ddsb_b[ddsb_i];
}

int main()
{
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	scanf("%d%d", &ddsb_n, &ddsb_q);
	for(int ddsb_i = 1; ddsb_i <= ddsb_n; ddsb_i++)
		scanf("%d", &ddsb_a[ddsb_i]);
	ddsb_msort(1, ddsb_n);
	for(int ddsb_i = 1; ddsb_i <= ddsb_q; ddsb_i++)
	{
		int ddsb_t, ddsb_x, ddsb_p = 1, ddsb_mark = 0;
		scanf("%d%d", &ddsb_t, &ddsb_x);
		for(int ddsb_j = 1; ddsb_j <= ddsb_t; ddsb_j++) ddsb_p *= 10;
		for(int ddsb_j = 1; ddsb_j <= ddsb_n && ddsb_mark == 0; ddsb_j++)
		{
			if(ddsb_a[ddsb_j] % ddsb_p == ddsb_x) 
			{
				printf("%d\n", ddsb_a[ddsb_j]);
				ddsb_mark = 1;
			}
		}
		if(ddsb_mark == 0) printf("-1\n");
	}
	return 0;
}

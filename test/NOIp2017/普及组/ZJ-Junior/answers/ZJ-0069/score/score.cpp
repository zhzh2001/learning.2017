#include <bits/stdc++.h>
using namespace std;

int ddsb_a, ddsb_b, ddsb_c;

int ddsb_ans = 0;

int main()
{
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
	scanf("%d%d%d", &ddsb_a, &ddsb_b, &ddsb_c);
	ddsb_a /= 10;
	ddsb_b /= 10;
	ddsb_c /= 10;
	ddsb_a *= 2;
	ddsb_b *= 3;
	ddsb_c *= 5;
	ddsb_ans = ddsb_a + ddsb_b + ddsb_c;
	printf("%d\n", ddsb_ans);
	return 0;
}

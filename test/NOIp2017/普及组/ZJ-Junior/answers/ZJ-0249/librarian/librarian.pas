var
    a:array[0..1001] of longint;
    n,q,i,j,x,y:longint;
    flag:boolean;

procedure init;
begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input);
    rewrite(output);
end;

procedure swap(var x,y:longint);
var
    z:longint;
begin
    z:=x;
    x:=y;
    y:=z;
end;

procedure sort(l,r:longint);
var
    i,j,x:longint;
begin
    i:=l;
    j:=r;
    x:=a[random(r-l+1)+l];
    repeat
        while a[i]<x do
            inc(i);
        while a[j]>x do
            dec(j);
        if i<=j then
            begin
                swap(a[i],a[j]);
                inc(i);
                dec(j);
            end;
    until i>j;
    if l<j then
        sort(l,j);
    if i<r then
        sort(i,r);
end;

function pd(x,y:longint):boolean;
var
    s1,s2:string;
    l1,l2:longint;
begin
    str(x,s1);
    str(y,s2);
    l1:=length(s1);
    l2:=length(s2);
    if (l1<=l2)and(copy(s2,l2-l1+1,l1)=s1) then
        exit(true)
    else
        exit(false);
end;

procedure main;
begin
    readln(n,q);
    for i:=1 to n do
        readln(a[i]);
    randomize;
    sort(1,n);
    for i:=1 to q do
    begin
        readln(x,y);
        flag:=false;
        for j:=1 to n do
            if pd(y,a[j]) then
                begin
                    flag:=true;
                    break;
                end;
        if flag then
            writeln(a[j])
        else
            writeln(-1);
    end;
end;

procedure print;
begin
    close(input);
    close(output);
end;

begin
    init;
    main;
    print;
end.
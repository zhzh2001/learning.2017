var
    a,b,c:longint;

procedure init;
begin
    assign(input,'score.in');
    assign(output,'score.out');
    reset(input);
    rewrite(output);
end;

procedure main;
begin
    readln(a,b,c);
    writeln(a div 10*2+b div 10*3+c div 10*5);
end;

procedure print;
begin
    close(input);
    close(output);
end;

begin
    init;
    main;
    print;
end.
var
    n,i:longint;
    d,k:int64;
    x,s:array[0..500001] of int64;
    max:int64;
    f:array[0..500001] of int64;
    q:array[0..500001] of longint;
    b:array[0..500001] of boolean;

procedure init;
begin
    assign(input,'jump.in');
    assign(output,'jump.out');
    reset(input);
    rewrite(output);
end;

function min(x,y:int64):int64;
begin
    if x<y then
        exit(x)
    else
        exit(y);
end;

function pd(v:longint):boolean;
var
    i,j,l,r,tl,tr:longint;
begin
    f[0]:=0;
    l:=0;
    r:=0;
    tl:=1;
    tr:=1;
    q[1]:=0;
    fillchar(b,sizeof(b),true);
    for i:=1 to n do
    begin
        while (tl<=tr)and((x[q[tl]]+d-v>x[i])or(x[q[tl]]+d+v<x[i])) do
        begin
            inc(tl);
            l:=q[tl];
        end;
        while (r+1<i)and(x[r+1]+d-v<=x[i])and(x[r+1]+d+v>=x[i]) do
        begin
            if b[r+1] then
                begin
                   inc(tr);
                   q[tr]:=r+1;
                   while (tl<tr)and(f[q[tr]]>=f[q[tr-1]]) do
                   begin
                       q[tr-1]:=q[tr];
                       dec(tr);
                   end;
                end;
            r:=r+1;
        end;
        if tl<=tr then
            begin
                f[i]:=f[q[tl]]+s[i];
                if f[i]>=k then
                    exit(true);
            end
        else
           b[i]:=false;
    end;
    exit(false);
end;

procedure serch;
var
    l,r,mid:int64;
begin
    l:=0;
    r:=max+1;
    while l<r do
    begin
        mid:=(l+r) div 2;
        if pd(mid) then
            r:=mid
        else
            l:=mid+1;
    end;
    if l<max+1 then
        writeln(l)
    else
        writeln(-1);
end;

procedure main;
begin
   readln(n,d,k);
   x[0]:=0;
   s[0]:=0;
   for i:=1 to n do
   begin
       readln(x[i],s[i]);
       if x[i]>max then
           max:=x[i];
   end;
   serch;
end;

procedure print;
begin
    close(input);
    close(output);
end;

begin
    init;
    main;
    print;
end.

var
    x,s:array[0..500001] of int64;
    n,i:longint;
    d,k:int64;

procedure swap(var x,y:int64);
var
    z:int64;
begin
    z:=x;
    x:=y;
    y:=z;
end;

procedure sort(l,r:longint);
var
    i,j:longint;
    tx:int64;
begin
    i:=l;
    j:=r;
    tx:=x[random(r-l+1)+l];
    repeat
        while x[i]<tx do
            inc(i);
        while x[j]>tx do
            dec(j);
        if i<=j then
            begin
                swap(x[i],x[j]);
                swap(s[i],s[j]);
                inc(i);
                dec(j);
            end;
    until i>j;
    if l<j then
        sort(l,j);
    if i<r then
        sort(i,r);
end;

begin
    randomize;
    assign(output,'jump.in');
    rewrite(output);
    readln(n,d,k);
    writeln(n,' ',d,' ',k);
    for i:=1 to n do
    begin
        x[i]:=random(1000000000)+1;
        s[i]:=random(20001)-10000;
    end;
    sort(1,n);
    for i:=1 to n do
        writeln(x[i],' ',s[i]);
    close(output);
end.

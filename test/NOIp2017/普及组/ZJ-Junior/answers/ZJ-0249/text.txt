
procedure init;
begin
    assign(input,'.in');
    assign(output,'.out');
    reset(input);
    rewrite(output);
end;

procedure main;
begin

end;

procedure print;
begin
    close(input);
    close(output);
end;

begin
    init;
    main;
    print;
end.
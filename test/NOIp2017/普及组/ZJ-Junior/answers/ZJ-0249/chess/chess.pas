const
    wx:array[1..4] of integer=(-1,0,0,1);
    wy:array[1..4] of integer=(0,-1,1,0);

var
    f:array[0..101,0..101,0..1] of longint;
    a:array[0..101,0..101] of longint;
    m,n,i,j,x,y,z:longint;
    minx:longint;
    q,p,c:array[0..2000005] of longint;

procedure init;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);
    rewrite(output);
end;

function value(x,y:longint):longint;
begin
    if x=y then
        exit(0)
    else
        exit(1);
end;

procedure spfa;
var
    i,j,k:longint;
    x,y,z,tx,ty,tz,v:longint;
    l,r:longint;
begin
    for i:=1 to m do
        for j:=1 to m do
            for k:=0 to 1 do
                f[i,j,k]:=maxlongint;
    f[1,1,a[1,1]]:=0;
    q[1]:=1;
    p[1]:=1;
    c[1]:=a[1,1];
    l:=1;
    r:=1;
    while l<=r do
    begin
        x:=q[l];
        y:=p[l];
        z:=c[l];
        for i:=1 to 4 do
            if (x+wx[i]>=1)and(x+wx[i]<=m)and(y+wy[i]>=1)and(y+wy[i]<=m) then
            begin
                tx:=x+wx[i];
                ty:=y+wy[i];
                if (a[x,y]<>-1)or(a[tx,ty]<>-1) then
                    for j:=0 to 1 do
                    begin
                        if (a[tx,ty]<>-1)and(a[tx,ty]<>j) then
                            continue;
                        tz:=j;
                        v:=value(z,tz);
                        if a[tx,ty]=-1 then
                            v:=v+2;
                        if f[x,y,z]+v<f[tx,ty,tz] then
                            begin
                                f[tx,ty,tz]:=f[x,y,z]+v;
                                inc(r);
                                q[r]:=tx;
                                p[r]:=ty;
                                c[r]:=tz;
                            end;
                    end;
            end;
        inc(l);
    end;
end;

function min(x,y:longint):longint;
begin
    if x<y then
        exit(x)
    else
        exit(y);
end;

procedure main;
begin
   readln(m,n);
   for i:=1 to m do
       for j:=1 to m do
           a[i,j]:=-1;
   for i:=1 to n do
   begin
       readln(x,y,z);
       a[x,y]:=z;
   end;
   spfa;
   minx:=min(f[m,m,0],f[m,m,1]);
   if minx<maxlongint then
       writeln(minx)
   else
       writeln(-1);
end;

procedure print;
begin
    close(input);
    close(output);
end;

begin
    init;
    main;
    print;
end.
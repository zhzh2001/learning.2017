const d:array[1..4,0..1] of longint=((1,0),(0,1),(-1,0),(0,-1));
type rec=record
  x,y,f,c:longint;
end;
var
  m,n,i,j,x,y,c,h,t,nx,ny,ans:longint;
  a:array[0..101,0..101] of longint; //1=yellow 2=red 0=none
  q:array[0..500001] of rec;
  vis:array[0..101,0..101,0..2] of longint;
procedure push(x,y,f,c:longint);
begin
  if (x<1)or(y<1)or(x>m)or(y>m) then exit;
  if (f>=vis[x,y,c])and(vis[x,y,c]<>-1) then exit;
  vis[x,y,c]:=f;
  inc(t);
  q[t].x:=x; q[t].y:=y; q[t].f:=f; q[t].c:=c;
end;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
  //randomize;
  read(m,n);
  for i:=1 to n do
  begin
    read(x,y,c);
    {c:=random(2)+1;
    if i=1 then
      begin x:=1; y:=1; end
    else
    repeat
      x:=random(m)+1;
      y:=random(m)+1;
    until a[x,y]=0;}
    if c=1 then a[x,y]:=1
      else if c=0 then a[x,y]:=2;
  end;
  for x:=0 to 101 do
  for y:=0 to 101 do
  for c:=0 to 2 do
    vis[x,y,c]:=-1;
  push(1,1,0,a[1,1]);
  repeat
    inc(h);
    for i:=1 to 4 do
    begin
      nx:=q[h].x+d[i,0];
      ny:=q[h].y+d[i,1];
      if a[nx,ny]<>0 then
      begin
        if a[nx,ny]<>q[h].c then push(nx,ny,q[h].f+1,a[nx,ny])
        else if a[nx,ny]=q[h].c then push(nx,ny,q[h].f,a[nx,ny]);
      end
      else if (a[nx,ny]=0)and(a[q[h].x,q[h].y]<>0) then
      begin
        push(nx,ny,q[h].f+2,q[h].c);
        push(nx,ny,q[h].f+2+1,3-q[h].c);
      end;
    end;
  until h>=t;
  {for i:=1 to m do
  begin
    for j:=1 to m do
    begin
      write(vis[i,j,1],'/',vis[i,j,2],' ');
    end;
    writeln;
  end;}
  if a[m,m]=0 then
  begin
    if vis[m,m,1]<vis[m,m,2] then ans:=vis[m,m,1]
    else ans:=vis[m,m,2];
  end
  else ans:=vis[m,m,a[m,m]];
  writeln(ans);
close(input);
close(output);
end.

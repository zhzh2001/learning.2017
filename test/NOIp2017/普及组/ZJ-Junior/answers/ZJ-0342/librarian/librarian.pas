var
  n,q,i,j,b,c,ans:longint;
  s1,s2:string;
  a:array[0..1001] of longint;
procedure sort(l,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
  //randomize;
  read(n,q);
  for i:=1 to n do
  begin
    read(a[i]);
  end;
  sort(1,n);
  for i:=1 to q do
  begin
    read(b,c);
    str(c,s2);
    ans:=-1;
    for j:=1 to n do
    begin
      str(a[j],s1);
      if copy(s1,length(s1)-b+1,b)=s2 then
      begin
        ans:=a[j];
        break;
      end;
    end;
    writeln(ans);
  end;
close(input);
close(output);
end.

var n,q,i,j,k:longint;
    b,m:array[1..1000] of string;
    g:array[1..1000] of longint;
    tr:string;
    c:char;
    f:boolean;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(b[i]);
  for i:=1 to q do
  begin
    f:=false;
    read(j);
    read(c);
    readln(m[i]);
    g[i]:=maxlongint;
    for j:=1 to n do
    begin
      tr:=copy(b[j],length(b[j])-length(m[i])+1,length(b[j]));
      if tr=m[i] then
      begin
        val(b[j],k);
        if k<g[i] then g[i]:=k;
        f:=true;
      end;
    end;
    if f=false then writeln(-1) else writeln(g[i]);
  end;
  close(input);
  close(output);
end.

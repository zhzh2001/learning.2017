var
  i,j,n,m,k:longint;
  x,y:array[1..500000] of longint;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);

  read(n,m,k);
  for i:=1 to n do
  read(x[i],y[i]);
  write(-1);

  close(input);
  close(output);
end.
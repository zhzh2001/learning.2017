#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll n,d,k;
ll ans;
ll tot[500008];
struct zyz{
	ll x,y;
}a[500008];
void dfs(ll x,ll y,ll o,ll sum){
	o=max(o,(x-y)/2+(x-y)%2);
	ll sub=sum+a[x].y;
	if(tot[n]-tot[x]<k-sub) return ;
	if(sum==k){
		ans=max(ans,o);
		return ; 
	}
	for(int i=x+1;i<=n;i++){
		dfs(i,x,o,sub);
	}
}
int cmp(zyz a,zyz b){return a.x<b.x;}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	for(int i=1;i<=n;i++) cin>>a[i].x>>a[i].y;
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++) 
		if(a[i].y>0) tot[i]=tot[i-1]+a[i].y;
		else tot[i]=tot[i-1];
	for(int i=1;i<=n;i++){
		dfs(i,1,0,0);
	}
	if(ans==0) ans=-1;
	printf("%lld",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

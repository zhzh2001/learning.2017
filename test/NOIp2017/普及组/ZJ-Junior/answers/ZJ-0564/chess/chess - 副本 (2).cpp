#include<bits/stdc++.h>
using namespace std;
int read(){
	int x=0;char c=getchar();
	while(c<'0' || c>'9') c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-'0',c=getchar();
	return x;
}
int mp[108][108],f[108][108][8];
void work(int i,int j,int a,int b){
	int pre=0;
	if(a!=0) pre=a;else pre=b; 
	if(mp[i+a][j+b]==mp[i][j] || (mp[i+a][j+b]==-1 && mp[i+pre][j+pre]==mp[i][j])) f[i][j][0]=min(f[i][j][0],min(f[i+a][j+b][0],f[i+a][j+b][1]));
	else f[i][j][0]=min(f[i][j][0],min(f[i+a][j+b][0],f[i+a][j+b][1])+1);
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess4.out","w",stdout);
	int n=read(),m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) mp[i][j]=-1;
	for(int i=1;i<=m;i++) {
		int x=read(),y=read(),z=read();
		mp[x][y]=z;
	}
	for(int i=0;i<=n+1;i++)
		for(int j=0;j<=n+1;j++) f[i][j][0]=f[i][j][1]=2e9;
	f[1][1][0]=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			if(i==1 && j==1) continue;
			if(mp[i][j]!=-1){
				work(i,j,-1,0),work(i,j,0,-1);
				work(i,j,1,0),work(i,j,0,1);
			}else f[i][j][1]=min(f[i][j][1],min(min(f[i-1][j][0],f[i][j-1][0]),min(f[i+1][j][0],f[i][j+1][0]))+2);
			if(i==j) printf("%d\n",f[i][j][0]-f[i-1][j-1][0]);
		}
	if(min(f[n][n][0],f[n][n][1])==2e9) printf("-1");
	else printf("%d",min(f[n][n][0],f[n][n][1]));
	fclose(stdin);fclose(stdout);
	return 0;
}

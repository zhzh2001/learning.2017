#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll read(){
	ll x=0;char c=getchar();
	while(c<'0' || c>'9') c=getchar();
	while(c>='0' && c<='9')
		x=x*10+c-'0',c=getchar();
	return x;
}
ll num[10000008];
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	ll n=read(),q=read();
	for(int i=1;i<=n;i++){
		ll x=read(),y=0,sub=1;
		ll now=x;
		while(now>0){
			y=now%10*sub+y;
			if(num[y]==0) num[y]=x;
			else num[y]=min(num[y],x);
			now=now/10;
			sub=sub*10;
		}
	}
	for(int i=1;i<=q;i++){
		ll x=read(),y=read();
		if(num[y]!=0) printf("%lld\n",num[y]);
		else printf("-1\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

var n,m,i,j,p,y,x:longint;
    a:array[0..1001]of longint;
    s:array[0..1001]of string;
    t,l:string;
    b:boolean;
procedure sort(l,r: longint);
var
  i,j,x,y: longint;
  begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
  while a[i]<x do inc(i);
  while x<a[j] do dec(j);
  if not(i>j) then
  begin
    y:=a[i]; a[i]:=a[j]; a[j]:=y;
    inc(i);
    j:=j-1;
  end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  sort(1,n);
  for i:=1 to n do
    str(a[i],s[i]);
  for i:=1 to m do
  begin
    readln(x,y);
    str(y,t);
    b:=false;
    for j:=1 to n do
    begin
      l:=copy(s[j],length(s[j])-x+1,x);
      if l=t then
      begin b:=true; break; end;
    end;
    if b then writeln(s[j])
    else writeln('-1');
  end;
  close(input); close(output);
end.
#include<bits/stdc++.h>
using namespace std;
#define N 500009

struct Step{
	int dis,p;
}s[N];

int n,d,k,sum[N],l,r,u;
long long mx,sm;

bool pdb(int x,int y,int b)
{
	return s[y].dis-s[x].dis<=b;
}

bool pda(int x,int y,int a)
{
	return s[y].dis-s[x].dis>=a;
}

bool check(int g)
{
	int x,y=d+g;
	sm=0;
	if(g>=d)	x=1;
	else x=d-g;
	int lf=0,rg=1;
	while(true)
	{
		while(!pda(lf,rg,x))	rg++;
		if(!pdb(lf,rg,y))	break;
		if(s[rg].p<0)
		{
			int ma=-(1<<30),j;
			for(j=rg;s[j].p<0 && pdb(lf,j,y);j++)
			{
				while(!pda(lf,j,x))	continue;
				if(ma<s[j].p)
					ma=s[j].p,u=j;
			}
			if(s[j].p<0 || !pdb(lf,j,y))	sm+=ma,rg=u;
			else rg=j;
		}
		sm+=s[rg].p;
		lf=rg;
		rg++;
	}
	if(sm>=k)	return 1;
	return 0;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d",&s[i].dis,&s[i].p);
		sum[i]=sum[i-1]+s[i].p;
		if(s[i].p>0)	mx+=s[i].p;
	}
	if(mx<k)
	{
		printf("-1\n");
		return 0;
	}
	l=0;
	r=s[n].dis;
	while(l<r)
	{
		int mid=(l+r)/2;
		if(check(mid))	r=mid;
		else l=mid+1;
	}
	printf("%d\n",l);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

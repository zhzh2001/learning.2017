#include<bits/stdc++.h>
using namespace std;
#define M 109

struct Point{
	int x,y;
}q[M*M],u,v;

int m,n;
int mp[M][M],dis[M][M],sel[M][M];
int t[M][M];
int dir[4][2]={ {0,1} , {0,-1} , {1,0} , {-1,0} };
int l,r;

int p(Point a,Point b)
{
	if(mp[b.x][b.y]==-1)	return 2;
	if(mp[b.x][b.y]!=mp[a.x][a.y])
	{
		if(mp[a.x][a.y]==-1)
			if(mp[b.x][b.y]==t[a.x][a.y])	return 0;
		return 1;		
	}
	return 0;
}

void bfs()
{
	memset(t,-1,sizeof(t));
	memset(dis,-1,sizeof(dis));
	l=r=0;
	q[r].x=1;
	q[r++].y=1;
	dis[1][1]=0;
	sel[1][1]=1;
	while(l<=r)
	{
		u=q[l++];
		sel[u.x][u.y]=0;
		for(int i=0;i<4;i++)
		{
			v.x=u.x+dir[i][0];
			v.y=u.y+dir[i][1];
			if(v.x<1 || v.x>m || v.y<1 || v.y>m || (mp[u.x][u.y]==-1 && mp[v.x][v.y]==-1))	continue;
			if(dis[v.x][v.y]==-1 || dis[v.x][v.y]>dis[u.x][u.y]+p(u,v))
			{
				dis[v.x][v.y]=dis[u.x][u.y]+p(u,v);
				if(mp[v.x][v.y]==-1)
					t[v.x][v.y]=mp[u.x][u.y];
				if(sel[v.x][v.y])	continue;
				q[r++]=v;
				sel[v.x][v.y]=1;
			}
		}
		if(mp[u.x][u.y]==-1)	t[u.x][u.y]=-1;
	}
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(mp,-1,sizeof(mp));
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		mp[x][y]=c;
	}
	bfs();
	printf("%d\n",dis[m][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

uses math;
const dx:array[1..4]of shortint=(0,1,0,-1);
      dy:array[1..4]of shortint=(1,0,-1,0);
var n,m,i,j,k,x,y,q:longint;p,g:boolean;
    a:array[0..105,0..105]of longint;
    b,u:array[0..105,0..105]of boolean;
function add(t,x,y:longint):longint;
begin
    if (a[x,y]=2)and(not(p)) then exit(-1);
    if a[x,y]=2 then begin a[x,y]:=t;p:=false;exit(2);end;
    if a[x,y]=t then add:=0 else add:=1;
    if not(b[x,y]) then a[x,y]:=2;
end;
function search(x,y:longint):longint;
var i,j,t,s,c,d:longint;
begin
    if (x=m)and(y=m) then exit(0);s:=10000;
    if (x>m)or(y>m)or(x<1)or(y<1) then exit(10000);
    g:=false;u[x,y]:=false;
    for i:=1 to 4 do
      if u[x+dx[i],y+dy[i]] then
      begin
        c:=add(a[x,y],x+dx[i],y+dy[i]);
        d:=search(x+dx[i],y+dy[i]);
        if (c<>-1)and(d<>-1) then
        begin g:=true;s:=min(s,c+d);end;
      end;
    u[x,y]:=true;
    if g then exit(s) else exit(-1);
end;
begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
    readln(m,n);fillchar(a,sizeof(a),2);fillchar(u,sizeof(u),true);
    fillchar(b,sizeof(b),false);
    for i:=1 to n do begin readln(x,y,q);a[x,y]:=q;b[x,y]:=true;end;
    writeln(search(1,1));
    close(input);close(output);
end.

var
  i,j,m,n:longint;
  x,y,z:array[1..1000]of longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do
    readln(x[i],y[i],z[i]);
  writeln('-1');
  close(input);
  close(output);
end.
var
  n,q,i,j,len:longint;
  a:array[0..2000]of string;
  ans:array[0..2000]of string;
  want,now,x:string;
  c:char;
procedure sort(l,r:longint);
  var
    i,j:longint;
    mid,t:string;
  begin
    i:=l;  j:=r;  mid:=a[(i+j)div 2];
    repeat
      while (length(a[i])<length(mid))or((length(a[i])=length(mid))and(a[i]<mid)) do  inc(i);
      while (length(a[j])>length(mid))or((length(a[j])=length(mid))and(a[j]>mid)) do  dec(j);
      if i<=j then  begin
        t:=a[i];  a[i]:=a[j];  a[j]:=t;
        inc(i);  dec(j);
      end;
    until i>j;
    if l<j then  sort(l,j);
    if i<r then  sort(i,r);
  end;

begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  sort(1,n);
  for i:=1 to 2000 do  ans[i]:='-1';
  for i:=1 to q do  begin
    readln(len,c,x);
    for j:=1 to n do  begin
      if (length(a[j])<len)or((length(a[j])=len)and(a[j]<x)) then  continue;
      if (length(a[j])=len)and(a[j]=x) then  begin  ans[i]:=a[j];  break;  end;
      now:=a[j];
      delete(now,1,length(now)-len);
      if x=now then  begin  ans[i]:=a[j];  break;  end;
    end;
  end;
  for i:=1 to q do  writeln(ans[i]);
  close(input);
  close(output);
end.

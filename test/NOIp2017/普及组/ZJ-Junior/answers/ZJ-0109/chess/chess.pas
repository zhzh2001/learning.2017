var
n,m,i,j,x,y,z:longint;
a,f,ff:array[-5..1005,-5..1005] of longint;
function min(x,y:longint):longint;
begin
	 if x<y then exit(x) else exit(y);
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
	 readln(N,m);
	 for i:=-1 to n+1 do
	 begin
		 for j:=-1 to n+1 do
		 begin
			 a[i,j]:=-1;f[i,j]:=maxlongint;
		 end;
	 end;
	 for i:=1 to m do
	 begin
		 readln(x,y,z);
		 a[x,y]:=z;
	 end;
	 for i:=1 to n do
	 begin
		 for j:=1 to n do
		 begin
			 if (i=1)and(j=1) then begin f[i,j]:=0;continue;end;
			 if (f[i-1,j]<>maxlongint)and
			 (
			 (a[i-1,j]<>-1)and(a[i,j]=-1)
			 or(a[i-1,j]=-1)and(a[i,j]<>-1)
			 or(a[i,j]<>-1)and(a[i-1,j]<>-1)
			 ) then
			 begin
				 if (a[i,j]=-1) then
				 begin
					 if a[i-1,j]<>-1 then
					 begin
						 ff[i,j]:=a[i-1,j];
						 f[i,j]:=min(f[i,j],f[i-1,j]+2);
					 end;
				 end else
				 begin
					 if (a[i-1,j]=-1) then
					 begin
						 if (a[i,j])=(ff[i-1,j]) then f[i,j]:=min(f[i-1,j],f[i,j]) else f[i,j]:=min(f[i-1,j]+1,f[i,j]);
					 end else
					 begin
						 if  a[i-1,j]<>a[i,j] then
						 begin
							 f[i,j]:=min(f[i,j],f[i-1,j]+1);
						 end else
						 begin
							 f[i,j]:=min(f[i,j],f[i-1,j]);
						 end;
					 end;
				 end;
			 end;
			 	 if (f[i+1,j]<>maxlongint)and
				 (
				 (a[i+1,j]<>-1)and(a[i,j]=-1)
				 or(a[i+1,j]=-1)and(a[i,j]<>-1)
				 or(a[i,j]<>-1)and(a[i+1,j]<>-1)
				 ) then
			 begin
				 if (a[i,j]=-1) then
				 begin
					 if a[i+1,j]<>-1 then
					 begin
						 ff[i,j]:=a[i+1,j];
						 f[i,j]:=min(f[i,j],f[i+1,j]+2);
					 end;
				 end else
				 begin
					 if (a[i+1,j]=-1) then
					 begin
						 if (a[i,j])=(ff[i+1,j]) then f[i,j]:=min(f[i+1,j],f[i,j]) else f[i,j]:=min(f[i+1,j]+1,f[i,j]);
					 end else
					 begin
						 if  a[i+1,j]<>a[i,j] then
						 begin
							 f[i,j]:=min(f[i,j],f[i+1,j]+1);
						 end else
						 begin
							 f[i,j]:=min(f[i,j],f[i+1,j]);
						 end;
					 end;
				 end;
			 end;
			 	 if (f[i,j-1]<>maxlongint)And
				 (
				 (a[i,j-1]<>-1)and(a[i,j]=-1)
				 or(a[i,j-1]=-1)and(a[i,j]<>-1)
				 or(a[i,j]<>-1)and(a[i,j-1]<>-1)
				 ) then
			 begin
				 if (a[i,j]=-1) then
				 begin
					 if a[i,j-1]<>-1 then
					 begin
						 ff[i,j]:=a[i,j-1];
						 f[i,j]:=min(f[i,j],f[i,j-1]+2);
					 end;
				 end else
				 begin
					 if (a[i,j-1]=-1) then
					 begin
						 if (a[i,j])=(ff[i,j-1]) then f[i,j]:=min(f[i,j-1],f[i,j]) else f[i,j]:=min(f[i,j-1]+1,f[i,j]);
					 end else
					 begin
						 if  a[i,j-1]<>a[i,j] then
						 begin
							 f[i,j]:=min(f[i,j],f[i,j-1]+1);
						 end else
						 begin
							 f[i,j]:=min(f[i,j],f[i,j-1]);
						 end;
					 end;
				 end;
			 end;
			  if (f[i,j+1]<>maxlongint)and
			  (
			  (a[i,j+1]<>-1)and(a[i,j]=-1)
			  or(a[i,j+1]=-1)and(a[i,j]<>-1)
			  or(a[i,j]<>-1)and(a[i,j+1]<>-1)
			  ) then
			 begin
				 if (a[i,j]=-1) then
				 begin
					 if a[i,j+1]<>-1 then
					 begin
						 ff[i,j]:=a[i,j+1];
						 f[i,j]:=min(f[i,j],f[i,j+1]+2);
					 end;
				 end else
				 begin
					 if (a[i,j+1]=-1) then
					 begin
						 if (a[i,j])=(ff[i,j+1]) then f[i,j]:=min(f[i,j+1],f[i,j]) else f[i,j]:=min(f[i,j+1]+1,f[i,j]);
					 end else
					 begin
						 if  a[i,j+1]<>a[i,j] then
						 begin
							 f[i,j]:=min(f[i,j],f[i,j+1]+1);
						 end else
						 begin
							 f[i,j]:=min(f[i,j],f[i,j+1]);
						 end;
					 end;
				 end;
			 end;
		 end;
	 end;
{	for i:=1 to n do
	 begin
		 for j:=1 to n do write(f[i,j],'            ');
		 writeln;
	 end;}
	 if f[n,n]<>maxlongint then writeln(f[n,n]) else writeln(-1);
	 close(input);
	 close(output);
end.

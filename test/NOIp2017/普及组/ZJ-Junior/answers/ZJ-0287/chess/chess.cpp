#include<bits/stdc++.h>
using namespace std;
int minn[110][110][2];
int n,m,ans=10000;
int a[110][110];
void DFS (int x,int y,int cos,bool way,int c)
{
	if (minn[x][y][c]<=cos)
		return;
	minn[x][y][c]=cos;
	if (x==m&&y==m)
	{
		ans=min (ans,cos);
		return;
	}
	if (c==a[x+1][y])
		DFS (x+1,y,cos,1,c);
	else if (a[x+1][y]!=-1)
		DFS (x+1,y,cos+1,1,a[x+1][y]);
	else if (way)
	{
		if (c==1)
		{
			DFS (x+1,y,cos+2,0,1);
			DFS (x+1,y,cos+3,0,0);
		}
		else
		{
			DFS (x+1,y,cos+2,0,0);
			DFS (x+1,y,cos+3,0,1);
		}
	}
	
	if (c==a[x-1][y])
		DFS (x-1,y,cos,1,c);
	else if (a[x-1][y]!=-1)
		DFS (x-1,y,cos+1,1,a[x-1][y]);
	else if (way)
	{
		if (c==1)
		{
			DFS (x-1,y,cos+2,0,1);
			DFS (x-1,y,cos+3,0,0);
		}
		else
		{
			DFS (x-1,y,cos+2,0,0);
			DFS (x-1,y,cos+3,0,1);
		}
	}
	
	if (c==a[x][y+1])
		DFS (x,y+1,cos,1,c);
	else if (a[x][y+1]!=-1)
		DFS (x,y+1,cos+1,1,a[x][y+1]);
	else if (way)
	{
		if (c==1)
		{
			DFS (x,y+1,cos+2,0,1);
			DFS (x,y+1,cos+3,0,0);
		}
		else
		{
			DFS (x,y+1,cos+2,0,0);
			DFS (x,y+1,cos+3,0,1);
		}
	}
	if (c==a[x][y-1])
		DFS (x,y-1,cos,1,c);
	else if (a[x][y-1]!=-1)
		DFS (x,y-1,cos+1,1,a[x][y-1]);
	else if (way)
	{
		if (c==1)
		{
			DFS (x,y-1,cos+2,0,1);
			DFS (x,y-1,cos+3,0,0);
		}
		else
		{
			DFS (x,y-1,cos+2,0,0);
			DFS (x,y-1,cos+3,0,1);
		}
	}
}
int main()
{
	freopen ("chess.in","r",stdin);
	freopen ("chess.out","w",stdout);
	memset (minn,-1,sizeof (minn));
	memset (a,-1,sizeof (a));
	scanf ("%d%d",&m,&n);
	for (int i=1;i<=n;i++)
	{
		int x,y,c;
		scanf ("%d%d%d",&x,&y,&c);
		a[x][y]=c;
	}
	for (int i=1;i<=m;i++)
		for (int j=1;j<=m;j++)
		{
			minn[i][j][0]=10000;
			minn[i][j][1]=10000;
		}
	DFS (1,1,0,1,a[1][1]);
	if (ans==10000)
	{
		printf ("-1");
		return 0;
	}
	printf ("%d",ans);
	return 0;
}

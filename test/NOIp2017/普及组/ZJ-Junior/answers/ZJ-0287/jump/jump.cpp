#include<bits/stdc++.h>
using namespace std;
int n,d,k;
int x[500010],s[500010];
int sum=0,ans;
bool check (int u)
{
	int f[500010];
	memset (f,0,sizeof (f));
	f[0]=0;
	int ans=-1000000010;
	for (int i=0;i<=n;i++)
	{
		int l1=x[i]+d-u;
		if (l1<=x[i])
			l1=x[i]+1;
		int r1=x[i]+d+u;
		if (r1>=x[n])
			r1=x[n];
		for (int j=i+1;j<=n;j++)
			if (x[j]>=l1&&x[j]<=r1)
			{
				f[j]=max (f[j],f[i]+s[j]);
				ans=max (ans,f[j]);
			}
	}
	if (ans>=k)
		return 1;
	else
		return 0;
}
int main()
{
	freopen ("jump.in","r",stdin);
	freopen ("jump.out","w",stdout);
	scanf ("%d%d%d",&n,&d,&k);
	x[0]=0;
	for (int i=1;i<=n;i++)
	{
		scanf ("%d%d",&x[i],&s[i]);
		if (s[i]>0)
			sum+=s[i];
	}
	if (sum<k)
	{
		printf ("-1");
		return 0;
	}
	int l=0,r=1000000010;
	while (l<r)
	{
		int mid=(l+r)/2;
		if (check (mid))
		{
			r=mid-1;
			ans=mid;
		}
		else
			l=mid+1;
	}
	printf ("%d",ans);
	return 0;
}

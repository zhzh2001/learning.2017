#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int fl[2][2]={{-1,0},{0,-1}};
int N,S,Mp[105][105],F[105][105][2][2];
inline void work(int x,int y){
	int Cl=Mp[x][y];
	for (int i=0;i<2;i++){
		int X=x+fl[i][0],Y=y+fl[i][1];
		if (X<1||X>N||Y<1||Y>N) continue;
		if (Mp[X][Y]^-1) F[x][y][Cl][0]=min(F[x][y][Cl][0],F[X][Y][Mp[X][Y]][0]+(Mp[X][Y]^Cl));
		else F[x][y][Cl][0]=min(F[x][y][Cl][0],min(F[X][Y][0][1]+(0^Cl),F[X][Y][1][1]+(1^Cl)));
	}
}
inline void Work_(int x,int y){
	for (int i=0;i<2;i++){
		int X=x+fl[i][0],Y=y+fl[i][1];
		if (X<1||X>N||Y<1||Y>N) continue;
		if (Mp[X][Y]^-1){
			F[x][y][0][1]=min(F[x][y][0][1],F[X][Y][Mp[X][Y]][0]+(Mp[X][Y]^0)+2);
			F[x][y][1][1]=min(F[x][y][1][1],F[X][Y][Mp[X][Y]][0]+(Mp[X][Y]^1)+2);
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&N,&S);
	memset(Mp,-1,sizeof Mp);
	for (int i=1;i<=S;i++) {
		int x,y;
		scanf("%d%d",&x,&y);
		scanf("%d",&Mp[x][y]);
	}
	memset(F,63,sizeof F);int INF=F[0][0][0][0];
	F[1][1][Mp[1][1]][0]=0;
	for (int i=1;i<=N;i++){
		for (int j=1;j<=N;j++){
			if (Mp[i][j]^-1) work(i,j);
			else Work_(i,j);
		}
	}
	int Ans=0;
	if (Mp[N][N]^-1) Ans=F[N][N][Mp[N][N]][0];
	else Ans=min(F[N][N][1][1],F[N][N][0][1]);
	printf("%d\n",Ans^INF?Ans:-1);
	return 0;
}

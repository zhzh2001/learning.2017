#include<cstdio>
#include<algorithm>
using namespace std;
int N,Q,A[1005],Mod[15];
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&N,&Q);Mod[0]=1;
	for (int i=1;i<=N;i++) scanf("%d",&A[i]);
	for (int i=1;i<=8;i++) Mod[i]=Mod[i-1]*10;
	sort(A+1,A+1+N);
	for (int i=1;i<=Q;i++){
		int w=0,len=0;bool vis=0;scanf("%d%d",&len,&w);
		for (int j=1;j<=N;j++) if (A[j]%Mod[len]==w) {printf("%d\n",A[j]);vis=1;break;}
		if (!vis) printf("-1\n");
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
typedef pair<long long,int> hp;
int N,D,S,Ans=-1,len;
long long F[500005];
hp hep[500005];
pair<int,int> A[500005];
inline void read(int &Ret){
	char ch=getchar();bool f=0;
	for (;!isdigit(ch);ch=getchar()) f|=(ch=='-');
	for (Ret=0;isdigit(ch);ch=getchar()) Ret=(Ret<<3)+(Ret<<1)+ch-48;
	f?Ret=-Ret:Ret;
}
inline bool Cmp(hp x,hp y){return x.first<y.first;}
inline void put(hp x){hep[++len]=x;push_heap(hep+1,hep+1+len,Cmp);}
inline void del(){pop_heap(hep+1,hep+1+len,Cmp);len--;}
inline bool check(int g){
	memset(F,192,sizeof F);
	int lst=0,Add=D-g;
	if (Add<1) Add=1;F[0]=0;len=0;
	for (int i=1;i<=N;i++){
		while (A[lst].first+D+g<A[i].first) lst++;
		while (A[lst].first+Add<=A[i].first&&A[lst].first+D+g>=A[i].first) put(hp(F[lst],lst)),lst++;
		while (A[hep[1].second].first+D+g<A[i].first&&len) del();
		if (len>0&&hep[1].first+A[i].second>F[i]) F[i]=hep[1].first+A[i].second;
		if (F[i]>=S) return 1;
	}
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	read(N);read(D);read(S);
	for (int i=1;i<=N;i++) read(A[i].first),read(A[i].second);
	int L=0,R=A[N].first,mid;
	while (L<=R) {
		mid=((R-L)>>1)+L;
		if (check(mid)) Ans=mid,R=mid-1;
		else L=mid+1;
	}
	printf("%d\n",Ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int m,n,x,y,c,a[106][106];
int ans=0,small=100000;
bool mf=1;
void dfs(int step1,int step2)
{
	if(step1==m&&step2==m)
	{
		if(ans<small) small=ans;
		return;
	}
	if(a[step1][step2+1]!=2&&step2<m)
	  {
	  mf=1;
	  if(a[step1][step2]!=a[step1][step2+1]) 
	  {
	  	  ans++;
	  	  dfs(step1,step2+1);
	  	  ans--;
	  }
	  else dfs(step1,step2+1);
	  }
	if(a[step1+1][step2]!=2&&step1<m)
	  {
	  mf=1;
	  if(a[step1+1][step2]!=a[step1][step2]) 
	  {
	  	  ans++;
	  	  dfs(step1+1,step2);
	  	  ans--;
	  }
	  else dfs(step1+1,step2);
	  }
	if(a[step1][step2+1]==2&&a[step1+1][step2]==2&&mf==1)
	{	
		if(step2<m)
		{
		mf=0;
		ans+=2;
		a[step1][step2+1]=a[step1][step2];
		dfs(step1+1,step2);
		a[step1][step2+1]=2;
		ans-=2;
		}
		if(step1<m)
		{
		mf=0;
		ans+=2;
		a[step1+1][step2]=a[step1][step2];
		dfs(step1+1,step2);
		a[step1+1][step2]=2;
		ans-=2;
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	if(m>n) 
	{
		cout<<"-1";
		return 0;
	}
	for(int i=1;i<=105;i++)
	   for(int j=1;j<=105;j++)
	   a[i][j]=2;
	for(int i=1;i<=n;i++)
	  {
	     cin>>x>>y>>c;
	     a[x][y]=c;
	  }
	dfs(1,1);
	if(small==100000)
	  cout<<"-1";
	else cout<<small;
	fclose(stdin);
	fclose(stdout);
	return 0;
}


var n,m,k,ans,l,r,mid,s,l1,r1,l2,r2:int64;
    i:longint;
    a,b,f:array[-10000..500000]of int64;
    g:array[-10000..500000]of boolean;
    d:array[-10000..5000000]of int64;
function max(a,b:int64):int64;
 begin
  if a>b then exit(a);
  exit(b);
 end;
function min(a,b:int64):int64;
 begin
  if a>b then exit(b);
  exit(a);
 end;
begin
assign(input,'jump.in');reset(input);
assign(output,'jump.out');rewrite(output);
 read(n,m,k);
 ans:=maxlongint*100000;
 for i:=1 to n do read(a[i],b[i]);
 l:=0;r:=a[n];
 while l<=r do
  begin
   mid:=(l+r)div 2;
   l1:=1;r1:=1;
   l2:=0;r2:=0;
   d[1]:=0;
   for i:=1 to n do f[i]:=-maxlongint*100;
   f[0]:=0;
   fillchar(g,sizeof(g),false);
   g[0]:=true;
   s:=0;
   for i:=1 to n do
    begin
     while(l1<=r1)and(a[d[l1]]+m+mid<a[i])do inc(l1);
     while(l1<=r1)and(a[d[r1]]+m-mid>a[i])do dec(r1);
     while(l2<i)and(a[l2]+m+mid<a[i])do inc(l2);
     while(r2<i)and(a[r2]+m-mid<=a[i])do
      begin
       if(r2>=l2)and(g[r2]=false)then
        begin
         if f[r2]<f[d[r1]]then
          begin
           inc(r1);
           d[r1]:=r2;
          end
           else
            begin
             while(l1<=r1)and(f[r2]>=f[d[r1]])do dec(r1);
             inc(r1);
             d[r1]:=r2;
            end;
         g[r2]:=true;
        end;
       inc(r2);
      end;
     if l1<=r1 then
      begin
       f[i]:=f[d[l1]]+b[i];
       s:=max(s,f[i]);
      end;
    end;
   if s>=k then
    begin
     ans:=min(ans,mid);
     r:=mid-1;
    end
     else l:=mid+1;
  end;
 if ans=maxlongint*100000 then write(-1)
  else write(ans);
close(input);close(output);
end.
type aaa=record
 wei,color:longint;
end;
var n,m,i,j,s,t,k:longint;
    f:array[0..5000000]of aaa;
    ha,li:array[0..100000]of longint;
    dis:array[0..100000,0..1]of longint;
    map,aa:array[0..200,0..200]of longint;
    g:array[0..100000,0..1]of boolean;
    bb:array[1..4]of longint=(1,-1,0,0);
    cc:array[1..4]of longint=(0,0,1,-1);
function min(a,b:longint):longint;
 begin
  if a>b then exit(b);
  exit(a);
 end;
procedure bfs;
var l,r,ii,t,x,y:longint;
 begin
  l:=0;r:=1;
  f[1].wei:=1;
  f[1].color:=aa[1,1];
  dis[1,aa[1,1]]:=0;
  g[1,aa[1,1]]:=true;
  while l<r do
   begin
    inc(l);
    t:=f[l].wei;
    k:=f[l].color;
    for ii:=1 to 4 do
     begin
      x:=ha[t]+bb[ii];y:=li[t]+cc[ii];
      if(x>0)and(y>0)and(x<=n)and(y<=n)then
       begin
        if(aa[ha[t],li[t]]=-1)and(aa[x,y]=-1)then continue;
        if aa[ha[t],li[t]]>-1 then
         begin
          if aa[x,y]>-1 then
           if aa[ha[t],li[t]]=aa[x,y] then
            begin
             if dis[t,aa[x,y]]<dis[map[x,y],aa[x,y]] then
              begin
               dis[map[x,y],aa[x,y]]:=dis[t,aa[x,y]];
               if g[map[x,y],aa[x,y]]=false then
                begin
                 g[map[x,y],aa[x,y]]:=true;
                 inc(r);
                 f[r].wei:=map[x,y];
                 f[r].color:=aa[x,y];
                end;
              end;
            end
             else
              begin
               if dis[t,aa[ha[t],li[t]]]+1<dis[map[x,y],aa[x,y]] then
                begin
                 dis[map[x,y],aa[x,y]]:=dis[t,aa[ha[t],li[t]]]+1;
                 if g[map[x,y],aa[x,y]]=false then
                  begin
                   g[map[x,y],aa[x,y]]:=true;
                   inc(r);
                   f[r].wei:=map[x,y];
                   f[r].color:=aa[x,y];
                  end;
                end;
              end
               else
                begin
                 if dis[t,aa[ha[t],li[t]]]+2<dis[map[x,y],aa[ha[t],li[t]]]then
                  begin
                   dis[map[x,y],aa[ha[t],li[t]]]:=dis[t,aa[ha[t],li[t]]]+2;
                   if g[map[x,y],aa[ha[t],li[t]]]=false then
                    begin
                     g[map[x,y],aa[ha[t],li[t]]]:=true;
                     inc(r);
                     f[r].wei:=map[x,y];
                     f[r].color:=f[l].color;
                    end;
                  end;
                end;
         end
          else
           begin
            if aa[x,y]=f[l].color then
             begin
              if dis[t,f[l].color]<dis[map[x,y],aa[x,y]] then
               begin
                dis[map[x,y],aa[x,y]]:=dis[t,f[l].color];
                if g[map[x,y],aa[x,y]]=false then
                 begin
                  g[map[x,y],aa[x,y]]:=true;
                  inc(r);
                  f[r].wei:=map[x,y];
                  f[r].color:=aa[x,y];
                 end;
               end;
             end
              else
               begin
                if dis[t,f[l].color]+1<dis[map[x,y],aa[x,y]]then
                 begin
                  dis[map[x,y],aa[x,y]]:=dis[t,f[l].color]+1;
                  if g[map[x,y],aa[x,y]]=false then
                   begin
                    g[map[x,y],aa[x,y]]:=true;
                    inc(r);
                    f[r].wei:=map[x,y];
                    f[r].color:=aa[x,y];
                   end;
                 end;
               end;
           end;
       end;
     end;
    g[t,f[l].color]:=false;
   end;
 end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
 read(n,m);
 for i:=1 to n do
  for j:=1 to n do
   begin
    inc(s);
    ha[s]:=i;li[s]:=j;
    map[i,j]:=s;
    aa[i,j]:=-1;
    dis[s,0]:=maxlongint div 3;
    dis[s,1]:=maxlongint div 3;
   end;
 for i:=1 to m do
  begin
   read(t,k,s);
   aa[t,k]:=s;
  end;
 bfs;
 if(dis[n*n,0]=maxlongint div 3)and(dis[n*n,1]=maxlongint div 3)then write(-1)
  else
   begin
    if dis[n*n,0]=maxlongint div 3 then write(dis[n*n,1])
     else if dis[n*n,1]=maxlongint div 3 then write(dis[n*n,0])
      else write(min(dis[n*n,0],dis[n*n,1]));
   end;
close(input);close(output);
end.
var
  n,m,i,j,k,t:longint;
  s,x,y:array[0..1001] of string;
  a:array[0..1001] of longint;
  z:string;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
  begin
    readln(s[i]);
    val(s[i],a[i]);
  end;
  for i:=1 to m do readln(x[i]);
  for i:=1 to m do
  begin
    k:=pos(' ',x[i]);
    y[i]:=copy(x[i],k+1,length(x[i])-k);
  end;
  for i:=1 to n do
    for j:=i+1 to n do
      if a[i]>a[j] then
      begin
        t:=a[i];
        a[i]:=a[j];
        a[j]:=t;
        z:=s[i];
        s[i]:=s[j];
        s[j]:=z;
      end;
  for i:=1 to n do writeln(s[i]);
  writeln;
  for i:=1 to m do
  begin
    for j:=1 to n do
      if copy(s[j],length(s[j])-length(y[i])+1,length(y[i]))=y[i] then
      begin
        t:=1;
        writeln(s[j]);
        break;
      end;
    if t=0 then writeln('-1');
    t:=0;
  end;
  close(input);
  close(output);
end.

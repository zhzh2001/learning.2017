var
  a,b,c,s:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(a,b,c);
  s:=a div 10*2+b div 10*3+c div 10*5;
  write(s);
  close(input);
  close(output);
end.

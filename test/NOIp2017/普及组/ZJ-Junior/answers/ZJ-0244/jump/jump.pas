var
  n,m,s,x,i:longint;
  a,b:array[0..500001] of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  read(n,m,s);
  for i:=1 to n do
  begin
    read(a[i],b[i]);
    if b[i]>0 then x:=x+b[i];
  end;
  if x<s then write('-1')
  else write('2');
  close(input);
  close(output);
end.

var
  n,m,i:longint;
  a,b,c:array[0..10001] of longint;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  read(n,m);
  for i:=1 to m do read(a[i],b[i],c[i]);
  write('-1');
  close(input);
  close(output);
end.
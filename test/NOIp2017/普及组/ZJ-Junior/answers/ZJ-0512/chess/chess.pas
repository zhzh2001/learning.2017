var
  i,j,k,t,m,n:longint;
  a,b,c:array[1..10000] of longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do
    read(a[i],b[i],c[i]);
  writeln(-1);
  close(input);
  close(output);
end.
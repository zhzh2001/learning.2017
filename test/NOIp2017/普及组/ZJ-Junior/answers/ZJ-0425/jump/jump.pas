uses math;
var
    n, d, g, l, r, i, k, t: longint;
    x, s, dp: array[0..500005] of longint;
    tot: int64;
begin
    assign(input, 'jump.in')
    assign(output, 'jump.out');
    reset(input);
    rewrite(output);
    readln(n, d, k);
    for i := 1 to n do
    begin
        readln(x[i], s[i]);
        if s[i] > 0 then tot := tot + s[i];
    end;
    if tot < k then
    begin
        writeln(-1);
        exit;
    end;
    for g := 0 to d - 1 do
    begin
        for i := 0 to n do
            dp[i] := -maxlongint;
        l := 1;
        r := n;
        t := n;
        dp[n] := s[n];
        repeat
            dec(t);
            while ((x[l] - x[t]) < (d - g)) and (l < n) do inc(l);
            while ((x[r] - x[t]) > (d + g)) and (r > 1) do dec(r);
            if l <= r then dp[t] := s[t];
            for i := l to r do
                dp[t] := max(dp[t], dp[i] + s[t]);
        until t = 1;
        for i := 1 to n do
        begin
            if dp[n] >= k then
            begin
                writeln(g);
                exit;
            end;
            if dp[i] = -maxlongint then break;
        end;
    end;
    repeat
        inc(g);
        for i := 0 to n do
            dp[i] := -maxlongint;
        l := 1;
        r := n;
        t := n;
        dp[n] := x[n];
        repeat
            dec(t);
            while ((x[l] - x[t]) < 1) and (l < n) do inc(l);
            while ((x[r] - x[t]) > (d + g)) and (r > 1) do dec(r);
            if l <= r then dp[t] := s[t];
            for i := l to r do
                dp[t] := max(dp[t], dp[i] + s[t]);
        until t = 1;
        for i := 1 to n do
        begin
            if dp[n] >= k then
            begin
                writeln(g);
                exit;
            end;
            if dp[i] = -maxlongint then break;
        end;
    until true;
    close(input);
    close(output);
end.

const
    dx: array[1..4] of longint = (1, -1, 0, 0);
    dy: array[1..4] of longint = (0, 0, 1, -1);
var
    m, n, i, j, k, x, y, co, ans: longint;
    b: array[0..101, 0..101] of boolean;
    ch: array[0..101, 0..101] of longint;
procedure find(x, y, k, mag, c: longint);
var
    i, xx, yy: longint;
begin
    if (x = m) then
        if (y = m) then
        begin
            if k < ans then
                ans := k;
            exit;
        end;
    if mag = 1 then ch[x, y] := c;
    for i := 1 to 4 do
    begin
        xx := x + dx[i];
        yy := y + dy[i];
        if b[xx, yy] then
        begin
            if (ch[x, y] = ch[xx, yy]) then
            begin
                b[xx, yy] := false;
                if mag = 1 then
                begin
                    ch[x, y] := -1;
                    find(xx, yy, k, 0, 0);
                end
                else
                    find(xx, yy, k, 0, 0);
                b[xx, yy] := true;
            end;
            if ch[x, y] + ch[xx, yy] = 1 then
            begin
                b[xx, yy] := false;
                if mag = 1 then
                begin
                    ch[x, y] := -1;
                    find(xx, yy, k + 1, 0, 0);
                end
                else
                    find(xx, yy, k + 1, 0, 0);
                b[xx, yy] := true;
            end;
            if ch[xx, yy] = -1 then
            begin
                if mag = 1 then
                begin
                    ch[x, y] := -1;
                    continue;
                end
                else
                begin
                    b[xx, yy] := false;
                    find(xx, yy, k + 3 - ch[x, y], 1, 1);
                    find(xx, yy, k + 2 + ch[x, y], 1, 0);
                    b[xx, yy] := true;
                end;
            end;
        end;
    end;
end;
begin
    assign(input, 'chess.in');
    assign(output, 'chess.out');
    reset(input);
    rewrite(output);
    ans := maxlongint;
    readln(m, n);
    for i := 1 to m do
        for j := 1 to m do
            ch[i, j] := -1;
    for i := 1 to n do
    begin
        read(x, y, co);
        ch[x, y] := co;
    end;
    for i := 1 to m do
        for j := 1 to m do
            b[i, j] := true;
    b[1, 1] := false;
    find(1, 1, 0, 0, 0);
    if ans = maxlongint then writeln(-1)
    else writeln(ans);
    close(input);
    close(output);
end.

var
    n, q, i, j, k, x: longint;
    ans: longint;
    nn: array[1..1005] of string;
    lq: array[1..1005] of longint;
    qq: array[0..1005, 0..10] of char;
    b: boolean;
begin
    assign(input, 'librarian.in');
    assign(output, 'librarian.out');
    reset(input);
    rewrite(output);
    readln(n, q);
    for i := 1 to n do
        readln(nn[i]);
    for i := 1 to q do
    begin
        read(lq[i]);
        for j := 0 to lq[i] do
            read(qq[i, j]);
        readln;
    end;
    for i := 1 to q do
    begin
        ans := maxlongint;
        for j := 1 to n do
        begin
            b := true;
            if length(nn[j]) < lq[i] then b := false
            else
                for k := 1 to lq[i] do
                    if nn[j][length(nn[j]) - lq[i] + k] <> qq[i, k] then
                        b := false;
            if b then
            begin
                val(nn[j], x);
                if x < ans then ans := x;
            end;
        end;
        if ans = maxlongint then writeln(-1)
        else writeln(ans);
    end;
    close(input);
    close(output);
end.

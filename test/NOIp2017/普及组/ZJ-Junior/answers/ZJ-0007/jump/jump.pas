var n,d,marks,ans,t,w,i,j:longint;
x,s,f:array[0..500000]of int64;
function max(a,b:int64):int64;
begin
    if a>b then exit(a) else exit(b);
end;
begin
    assigN(input,'jump.in');
    reset(input);
    assign(output,'jump.out');
    rewrite(output);
    read(n,d,marks);
    for i:=1 to n do read(x[i],s[i]);
    ans:=-1; x[0]:=0; f[0]:=0;
    while true do
    begin
      inc(ans);
      if ans<d then
      begin
        t:=d-ans;
        w:=d+ans;
      end
      else
      begin
        t:=1;
        w:=d+ans;
      end;
      fillchar(f,sizeof(f),0);
      for i:=1 to n do
      begin
        for j:=i-1 downto 0 do
        begin
          if (x[j]+t<=x[i]) and (x[j]+w>=x[i]) then
          begin
            f[i]:=max(f[i],f[j]+s[i]);
            if f[i]>marks then
            begin
              writeln(ans);
              close(input);
              close(output);
              exit;
            end;
          end;
        end;
      end;
      if ans>=1000 then
      begin
        writeln(-1);
        close(input);
        close(output);
        exit;
      end;
    end;
end.



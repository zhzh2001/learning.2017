var n,m,i,j,k,L,x,y:longint;
p:boolean;
a:array[0..10000] of string;
s:string;
begin
    assign(input,'librarian.in');
    reset(input);
    assign(output,'librarian.out');
    rewrite(output);
    readln(n,m);
    for i:=1 to n do readln(a[i]);
    for I:=1 to n-1 do
      for j:=i+1 to n do
        if (lengtH(a[i])>length(a[j])) or
        ((length(a[i])=length(a[j])) and (a[i]>a[j])) then
        begin
          a[0]:=a[i];
          a[i]:=a[j];
          a[j]:=a[0];
        end;
    for i:=1 to n do
    begin
      read(x,y);
      str(y,s);
      for j:=1 to n do
      begin
        p:=true; L:=0;
        for k:=length(a[j])-x+1 to length(a[j]) do
        begin
          inc(L);
          if a[j,k]<>s[L] then
          begin
            p:=false;
            break;
          end;
        end;
        if p then
        begin
          writeln(a[j]);
          break;
        end;
      end;
      if p=false then writeln(-1);
    end;
    close(input);
    close(output);
end.
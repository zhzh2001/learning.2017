const dx:array[1..12]of longint=(1,0,-1,0,1,-1,1,-1,2,-2,0,0);
dy:array[1..12]of longint=(0,1,0,-1,1,-1,-1,1,0,0,2,-2);
var n,m,i,j,w,t,c:longint;
f,a:array[0..1000,0..1000] of int64;
x,y:array[0..5000000] of longint;
begin
    assigN(input,'chess.in');
    reset(input);
    assign(output,'chess.out');
    rewrite(output);
    read(m,n);
    for i:=1 to m do
      for j:=1 to m do a[i,j]:=-1;
    for I:=1 to n do
    begin
      read(x[1],y[1],c);
      a[x[1],y[1]]:=c;
    end;
    {for i:=1 to m do
    begin
      for j:=1 to m do write(a[i,j]:3);
      writeln;
    end;}
    for i:=1 to m do
      for j:=1 to m do f[i,j]:=maxlongint;
    if a[1,1]<>-1 then f[1,1]:=0;
    w:=1; x[1]:=1; y[1]:=1;
    while t<w do
    begin
      inc(t);
      for i:=1 to 12 do
        if (x[t]+dx[i]<=m) and (x[t]+dx[i]>0) and
           (y[t]+dy[i]<=m) and (y[t]+dy[i]>0) then
        begin
          if i<=4 then
          begin
            if (a[x[t],y[t]]=-1) and (a[x[t]+dx[i],y[t]+dy[i]]=-1) then continue;
            if (a[x[t],y[t]]=1)and(a[x[t]+dx[i],y[t]+dy[i]]=1) OR  (a[x[t],y[t]]=0)and(a[x[t]+dx[i],y[t]+dy[i]]=0)  then
              if f[x[t]+dx[i],y[t]+dy[i]]>f[x[t],y[t]] then
              begin
                f[x[t]+dx[i],y[t]+dy[i]]:=f[x[t],y[t]];
                if (a[x[t]+dx[i],y[t]+dy[i]]<>-1) or (a[x[t],y[t]]<>-1) then
                begin
                  inc(w);
                  x[w]:=x[t]+dx[i];
                  y[w]:=y[t]+dy[i];
                end;
              end;
            if (a[x[t],y[t]]<>a[x[t]+dx[i],y[t]+dy[i]]) and ((a[x[t],y[t]]<>-1)  and (a[x[t]+dx[i],y[t]+dy[i]]<>-1)) then
              if f[x[t]+dx[i],y[t]+dy[i]]>f[x[t],y[t]]+1 then
              begin
                f[x[t]+dx[i],y[t]+dy[i]]:=f[x[t],y[t]]+1;
                if (a[x[t]+dx[i],y[t]+dy[i]]<>-1) or (a[x[t],y[t]]<>-1) then
                begin
                  inc(w);
                  x[w]:=x[t]+dx[i];
                  y[w]:=y[t]+dy[i];
                end;
              end;
            if (a[x[t],y[t]]<>a[x[t]+dx[i],y[t]+dy[i]]) and ((a[x[t],y[t]]=-1) or (a[x[t]+dx[i],y[t]+dy[i]]=-1)) then
            if f[x[t]+dx[i],y[t]+dy[i]]>f[x[t],y[t]]+2 then
              begin
                f[x[t]+dx[i],y[t]+dy[i]]:=f[x[t],y[t]]+2;
                if (a[x[t]+dx[i],y[t]+dy[i]]<>-1) or (a[x[t],y[t]]<>-1) then
                begin
                  inc(w);
                  x[w]:=x[t]+dx[i];
                  y[w]:=y[t]+dy[i];
                end;
              end;
          end;
          if i>4 then
          begin
            if {((a[x[t]+dx[i],y[t]+dy[i]]=0) or (a[x[t]+dx[i],y[t]+dy[i]]=1)) and} (a[x[t],y[t]]<>-1) and (a[x[t]+dx[i],y[t]+dy[i]]<>-1) then
            begin
              if a[x[t],y[t]]=a[x[t]+dx[i],y[t]+dy[i]] then
              if f[x[t]+dx[i],y[t]+dy[i]]>f[x[t],y[t]]+2 then
              begin
                f[x[t]+dx[i],y[t]+dy[i]]:=f[x[t],y[t]]+2;
                if a[x[t],y[t]]<>-1  then
                begin
                  inc(w);
                  x[w]:=x[t]+dx[i];
                  y[w]:=y[t]+dy[i];
                end;
              end;
            if a[x[t],y[t]]<>a[x[t]+dx[i],y[t]+dy[i]] then
              if f[x[t]+dx[i],y[t]+dy[i]]>f[x[t],y[t]]+3 then
              begin
                f[x[t]+dx[i],y[t]+dy[i]]:=f[x[t],y[t]]+3;
                if a[x[t],y[t]]<>-1 then
                begin
                  inc(w);
                  x[w]:=x[t]+dx[i];
                  y[w]:=y[t]+dy[i];
                end;
              end;
              end;
          end;
        end;
    end;
    {for i:=1 to m do
    begin
      for j:=1 to m do
        if f[i,j]>=maxlongint then write('m ') else write(f[i,j],' ');
        writeln;
    end;}
    if f[m,m]>=maxlongint then writeln(-1) else writelN(f[m,m]);
    close(input);
    close(output);
end.

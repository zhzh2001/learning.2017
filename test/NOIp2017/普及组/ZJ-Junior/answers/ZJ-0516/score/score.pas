var
  a,b,c,s:real;
begin
  assign(input,'score.in');reset(input);
  assign(output,'score.out');rewrite(output);
  readln(a,b,c);
  s:=a/5+b*3/10+c/2;
  write(trunc(s));
  s:=s-trunc(s);
  if s<>0 then
    write('.',s*10:0:0);
  writeln;
  close(input);close(output);
end.
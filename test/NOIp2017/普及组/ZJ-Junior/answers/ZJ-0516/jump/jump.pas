var
  n,d,k,min,max,nn,s,i,j,m,l,r:longint;
  a:array[-1..500010,1..3] of longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  min:=maxlongint;
  for i:=1 to n do
  begin
    readln(a[i,1],a[i,2]);
    if a[i,2]>0 then s:=s+a[i,2];
    if max<a[i,1]-a[i-1,1] then max:=a[i,1]-a[i-1,1];
    if min>a[i,1]-a[i-1,1] then min:=a[i,1]-a[i-1,1];
  end;
  if s<k then
  begin
    writeln(-1);
    close(input);close(output);
    halt;
  end;
  nn:=n;
  for i:=n downto 1 do
    if a[i,2]<0 then dec(nn)
      else break;;
  n:=nn;
  l:=0;
  if max-d>d-min then r:=max-d
    else r:=d-min;
  while l<r do
  begin
    for i:=1 to n do a[i,3]:=0;
    m:=(l+r) div 2;
    if m>=d then min:=1
      else min:=d-m;
    for i:=1 to n do
    begin
      j:=i;
      while (a[i,1]-a[j,1]<min) and (j>=0) do dec(j);
      if j>=0 then
      begin
        inc(j);
        repeat
          dec(j);
          if a[i,3]<a[j,3]+a[i,2] then a[i,3]:=a[j,3]+a[i,2];
        until (a[i,1]-a[j,1]>d+m) or (j=0);
      end
        else break;
    end;
    if a[n,3]>=k then r:=m
      else l:=m+1;
  end;
  writeln(l);
  close(input);close(output);
end.

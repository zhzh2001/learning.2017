const
  fx:array[1..2]of longint=(-1,0);
  fy:array[1..2]of longint=(0,-1);
var
  a,f:array[-1..110,-1..110]of longint;
  n,m,i,j,k,t,x,y,z,nx,ny,tx,ty:longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  read(m,n);
  for i:=1 to n do
    begin
      read(x,y,z);
      a[x][y]:=z;
      if z=0 then a[x][y]:=2;
    end;
  fillchar(f,sizeof(f),127);
  f[1][1]:=0;
  for i:=1 to m do
    for j:=1 to m do
      if(i+j<>2)then
        begin
          for k:=1 to 2 do
            begin
              nx:=i+fx[k];
              ny:=j+fy[k];
              if(nx<=0)or(nx>m)or(ny<=0)or(ny>m)then continue;
              if(a[i][j]<>0)then
                if(a[nx][ny]=a[i][j])and(f[nx][ny]<f[i][j])then
                  f[i][j]:=f[nx][ny];
              if(a[i][j]<>0)and(a[nx][ny]<>0)then
                if(a[nx][ny]<>a[i][j])and(f[nx][ny]+1<f[i][j])then
                  f[i][j]:=f[nx][ny]+1;
              if(a[i][j]=0)and(a[nx][ny]<>0)then
                if(f[nx][ny]+2<f[i][j])then
                  f[i][j]:=f[nx][ny]+2;
              if(a[i][j]<>0)and(a[nx][ny]=0)then
                if(f[nx][ny]+2<f[i][j])then
                  f[i][j]:=f[nx][ny]+2;
            end;
        end;
    if f[m][m]>=2139062143 then writeln(-1)
  else writeln(f[m][m]);
  close(input);
  close(output);
end.

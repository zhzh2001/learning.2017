var
  flag:array[-1..1010]of boolean;
  ls:array[-1..1010]of longint;
  s:array[-1..1010]of string[10];
  n,q,i,j,t,l,num:longint;
  ans,x,k:string[10];
  c:char;
function check(s,ans:string):boolean;
begin
  if(ans='')then exit(true);
  if(length(s)>length(ans))then exit(false);
  if(length(s)<length(ans))then exit(true);
  if s<ans then exit(true);
end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    begin
      readln(s[i]);
      ls[i]:=length(s[i]);
    end;
  fillchar(flag,sizeof(flag),false);
  for i:=1 to q do
    begin
      read(l,c,k);
      ans:='';
      for j:=1 to n do
        begin
          x:='';
          num:=0;
          for t:=ls[j]-l+1 to ls[j] do
            x:=x+s[j][t];
          if(x=k)and(not flag[j])then
            if(check(s[j],ans))then
              begin
                ans:=s[j];
                num:=j;
              end;
        end;
      readln;
        if ans='' then writeln(-1)
      else writeln(ans);
      flag[num]:=true;
    end;
  close(input);
  close(output);
end.
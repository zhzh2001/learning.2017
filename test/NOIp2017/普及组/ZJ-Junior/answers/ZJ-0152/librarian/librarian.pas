var
  n,q,i,j,k:longint;
  t,x:string;
  c:char;
  b,s:array[0..1111] of string;
  rl,al:array [0..1111] of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    begin
      readln(b[i]);
      al[i]:=length(b[i]);
    end;
  for i:=1 to q do
    begin
      readln(rl[i],c,s[i]);
      writeln(s[i]);
    end;
  for i:=1 to q do
    begin
      j:=0;
      c:='f';
      repeat
        inc(j);
        t:=copy(b[j],al[j]-rl[i]+1,al[j]);
        if t=s[i]
          then
            begin
              if c='f' then x:=b[j] else
              if length(b[j])<length(x) then x:=b[j] else if b[j]<x then x:=b[j];
              c:='t';
            end;
        writeln(i,' ',j,':',b[j],' ',t,' ',s[i],' ',x,' ',c);
      until j+1>n;
      if c='f' then writeln(-1) else writeln(x);
    end;
  close(input);
  close(output);
end.
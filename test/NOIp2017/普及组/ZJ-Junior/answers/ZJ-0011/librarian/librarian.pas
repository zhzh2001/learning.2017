var
  n,m,i,j,k,o:longint;
  s,s1,s2:string;
  t:boolean;
  a,b,max,sum,pp:array[1..1001]of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    begin
      readln(a[i]);
    end;
  for i:=1 to m do
    begin
      pp[i]:=1;
      readln(sum[i],b[i]);
      for j:=1 to sum[i] do
        begin
          pp[i]:=pp[i]*10;
        end;
    end;
  for i:=1 to m do
    begin
      max[i]:=maxlongint;
      for j:=1 to n do
        begin
          o:=a[j]-b[i];
          if (o>=0)and(o mod pp[i]=0)
            then begin
                   if max[i]>a[j]
                     then max[i]:=a[j];
                 end;
        end;
      if max[i]=maxlongint
        then max[i]:=-1;
    end;
  for i:=1 to m do
    begin
      writeln(max[i]);
    end;
  close(input);
  close(output);
end.
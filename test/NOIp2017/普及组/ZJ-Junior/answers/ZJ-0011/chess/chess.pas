var
  n,m,i,j,k,sum,x,y,z:longint;
  f,ans,d:array[0..1001,0..1001]of longint;
  ff:array[0..1001,0..1001]of boolean;
  t:boolean;
function min(q,w,e,r:longint):longint;
var
  max:longint;
begin
  max:=maxlongint;
  if max>q
    then max:=q;
  if max>w
    then max:=w;
  if max>e
    then max:=e;
  if max>r
    then max:=r;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  fillchar(ff,sizeof(ff),false);
  for i:=1 to m do
    begin
      readln(x,y,z);
      d[x,y]:=z;
      if (z=1)or(z=0)
        then ff[x,y]:=true;
    end;
  if n>m
    then begin
           writeln(-1);
           close(input);
           close(output);
           halt;
         end;
  for i:=1 to n do
    begin
      for j:=1 to n do
        begin
          if ff[i,j]=false
            then ans[i,j]:=maxlongint div 2;
          if d[i-1,j]=1
            then begin
                   if d[i,j]=0
                     then ans[i,j]:=1
                     else ans[i,j]:=0;
                 end;
          if d[i+1,j]=1
            then begin
                   if d[i,j]=0
                     then ans[i,j]:=1
                     else ans[i,j]:=0;
                 end;
          if d[i,j-1]=1
            then begin
                   if d[i,j]=0
                     then ans[i,j]:=1
                     else ans[i,j]:=0;
                 end;
          if d[i,j+1]=1
            then begin
                   if d[i,j]=0
                     then ans[i,j]:=1
                     else ans[i,j]:=0;
                 end;
          f[i,j]:=ans[i,j];
        end;
    end;
  ans[1,1]:=0;
  ans[0,1]:=0;
  ans[1,0]:=0;
  i:=1;
  j:=1;
  for i:=1 to n do
    begin
      for j:=1 to n do
        begin
          if ff[i,j]=true
            then begin
                   f[i,j]:=min(f[i-1,j],f[i,j-1],f[i,j+1],f[i+1,j])+ans[i,j];
                 end
            else begin
                   t:=true;
                   if ff[i-1,j]=true
                     then begin
                            f[i,j]:=2+f[i-1,j];
                            t:=false;
                          end;
                   if ff[i+1,j]=true
                     then begin
                            f[i,j]:=2+f[i+1,j];
                            t:=false;
                          end;
                   if ff[i,j-1]=true
                     then begin
                            f[i,j]:=2+f[i,j-1];
                            t:=false;
                          end;
                   if ff[i,j+1]=true
                     then begin
                            f[i,j]:=2+f[i,j+1];
                            t:=false;
                          end;
                   if t=true
                     then begin
                            writeln(-1);
                            close(input);
                            close(output);
                            halt;
                          end;
                 end;
      end;
    end;
  writeln(f[n,n]);
  close(input);
  close(output);
end.

var n,d,k,i:longint;
count,lb,rb,l,mid:int64;
x,s:array[1..500005]of longint;
a:array[1..500005]of int64;
flag,t:boolean;

function max(a,b:int64):int64;
begin
 if(a>b)then exit(a);
 exit(b);
end;

function dp(y,d:int64):int64;
var i,j:longint;
max:int64;
begin
 for j:=1 to n do
  if(x[j]>=y)then
  begin
   i:=j;
   break;
  end;
 fillchar(a,sizeof(a),0);
 a[i]:=s[i];
 inc(i);
 max:=-maxlongint;
 while(i<=n)do
 begin
  j:=i-1;
  while(x[i]-x[j]<=d)and(j>=y)do
  begin
   if(a[j]+s[i]>max)then max:=a[j]+s[i];
   j:=j-1;
  end;
  a[i]:=max;
  i:=i+1;
 end;
 dp:=a[n];
end;

function check(g:int64):boolean;
var i,c:longint;
begin
 if(g>=d)then
 begin
  c:=dp(1,g+d);
 end
 else
  c:=dp(d-g,d+g);
 if(c>k)then exit(true);
 exit(false);
end;

begin
 assign(input,'jump.in');      reset(input);
 assign(output,'jump.out');    rewrite(output);

 readln(n,d,k);
 for i:=1 to n do
 begin
  readln(x[i],s[i]);
  if(s[i]>0)then count:=count+s[i];
 end;
 flag:=true;
 if(count<k)then flag:=false;

 lb:=1;
 rb:=maxlongint;
 t:=true;
 while(lb<rb)do
 begin
  mid:=(lb+rb)div 2;
  t:=check(mid);
  if(t)
  then rb:=mid-1
  else lb:=mid+1;
 end;

 if not(flag)then writeln(-1)
 else writeln(mid);

 close(input);
 close(output);
end.

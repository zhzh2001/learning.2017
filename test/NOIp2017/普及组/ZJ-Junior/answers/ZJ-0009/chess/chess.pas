var a,b:array[1..101,1..101]of integer;
m,n,i,j,x,y,t,min:longint;
flag:boolean;

procedure dfs(x,y,t,s:longint);
begin
 if(x=m)and(y=m)then
 begin
  flag:=true;
  if(s<min)then min:=s;
  exit;
 end;
 if(s>=min)then exit;

 if(x>1)and(b[x-1,y]=0)
 then begin
  b[x-1,y]:=1;
  if(a[x,y]=a[x-1,y])then dfs(x-1,y,0,s)
  else if(a[x-1,y]=-1)and(t=0)then begin
   a[x-1,y]:=a[x,y];
   dfs(x-1,y,1,s+2);
   a[x-1,y]:=1-a[x,y];
   dfs(x-1,y,1,s+3);
   a[x-1,y]:=-1;
  end
  else if(a[x,y]<>a[x-1,y])and(a[x-1,y]<>-1) then dfs(x-1,y,0,s+1);
  b[x-1,y]:=0;
 end;
 if(y>1)and(b[x,y-1]=0)
 then begin
  b[x,y-1]:=1;
  if(a[x,y]=a[x,y-1])then dfs(x,y-1,0,s)
  else if(a[x,y-1]=-1)and(t=0)then begin
   a[x,y-1]:=a[x,y];
   dfs(x,y-1,1,s+2);
   a[x,y-1]:=1-a[x,y];
   dfs(x,y-1,1,s+3);
   a[x,y-1]:=-1;
  end
  else if(a[x,y]<>a[x,y-1])and(a[x,y-1]<>-1) then dfs(x,y-1,0,s+1);
  b[x,y-1]:=0;
 end;
 if(x<m)and(b[x+1,y]=0)
 then begin
  b[x+1,y]:=1;
  if(a[x,y]=a[x+1,y])then dfs(x+1,y,0,s)
  else if(a[x+1,y]=-1)and(t=0)then begin
   a[x+1,y]:=a[x,y];
   dfs(x+1,y,1,s+2);
   a[x+1,y]:=1-a[x,y];
   dfs(x+1,y,1,s+3);
   a[x+1,y]:=-1;
  end
  else if(a[x,y]<>a[x+1,y])and(a[x+1,y]<>-1) then dfs(x+1,y,0,s+1);
  b[x+1,y]:=0;
 end;
 if(y<m)and(b[x,y+1]=0)
 then begin
  b[x,y+1]:=1;
  if(a[x,y]=a[x,y+1])then dfs(x,y+1,0,s)
  else if(a[x,y+1]=-1)and(t=0)then begin
   a[x,y+1]:=a[x,y];
   dfs(x,y+1,1,s+2);
   a[x,y+1]:=1-a[x,y];
   dfs(x,y+1,1,s+3);
   a[x,y+1]:=-1;
  end
  else if(a[x,y]<>a[x,y+1])and(a[x,y+1]<>-1) then dfs(x,y+1,0,s+1);
  b[x,y+1]:=0;
 end;
end;

begin
 assign(input,'chess.in');      reset(input);
 assign(output,'chess.out');    rewrite(output);

 readln(m,n);
 for i:=1 to m do
  for j:=1 to m do
   a[i,j]:=-1;
 fillchar(b,sizeof(b),0);
 for i:=1 to n do
 begin
  readln(x,y,t);
  a[x,y]:=t;
 end;
 min:=maxlongint;
 b[1,1]:=1;
 dfs(1,1,0,0);
 if not(flag)
 then writeln(-1)
 else writeln(min);

 close(input);
 close(output);
end.

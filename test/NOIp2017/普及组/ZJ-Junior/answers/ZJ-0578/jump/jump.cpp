#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;
int n,k,d,a[500001],b[500001];
void judge(int j,int t,int l){
	if(t>k){
		printf("%d\n",l);
		exit(0);
	}
	int left=j+d-l,right=j+d+l;
	if(right>a[n]) return;
	if(left<j+1) left=j+1;
	for(int i=1;i<=n;i++){
		if(a[i]>right) break;
		if(a[i]>=left) judge(a[i],t+b[i],l);
	}
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++) scanf("%d%d",&a[i],&b[i]);
	for(int l=1;;l++){
		if(d+l>a[n]){
			printf("-1");
			fclose(stdin);fclose(stdout);
			return 0;
		}
		judge(0,0,l);
	}
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
struct node{
	int x,y;
}a[1001];
int b[1001][3],n,q;
bool h;
bool comp(const node &a,const node &b){
	return a.y<b.y;
}
bool judge(int a,int b){
	while(a!=0&&b!=0){
		if(a%10!=b%10)return 0;
		a/=10;
		b/=10;
	}
	return 1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	memset(a,0,sizeof(a));
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i].y);
		int t=a[i].y;
		for(;t!=0;){
			t/=10;
			a[i].x++;
		}
	}
	sort(a+1,a+n+1,comp);
	//for(int i=1;i<=n;i++) printf("%d %d\n",a[i].x,a[i].y);
	for(int i=1;i<=q;i++) scanf("%d%d",&b[i][1],&b[i][2]);
	for(int i=1;i<=q;i++){
		h=0;
		for(int j=1;j<=n;j++){
			if(a[j].x>=b[i][1])
			  if(judge(a[j].y,b[i][2])){
			  	printf("%d\n",a[j].y); h=1; break;
			  }
		}
		if(!h)printf("-1\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}


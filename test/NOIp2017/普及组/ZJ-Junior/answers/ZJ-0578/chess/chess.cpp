#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int da[5]={0,1,0,-1,0},dt[5]={0,0,1,0,-1};
int m,n,minn=0x7fffffff,a[101][101],b[101][101];
void search(int x,int y,int t,bool h){
	if(t>=minn) return;
	if(x==n&&y==n){
		//cerr<<x<<" "<<y<<" "<<t<<" "<<h<<endl;
		minn=t;
		return;
	}
	int tx,ty,sec[5]={0},thi[5]={0};
	b[x][y]=1;
	for(int i=1;i<=4;i++){
		tx=x+da[i]; ty=y+dt[i];
		if(tx<1||tx>n||ty<1||ty>n) continue;
		if(b[tx][ty]) continue;
		if(a[tx][ty]==a[x][y]) search(tx,ty,t,1);
		else if(a[tx][ty]!=0) sec[++sec[0]]=i;
		else thi[++thi[0]]=i;
	}
	for(int i=1;i<=sec[0];i++) {
		tx=x+da[sec[i]]; ty=y+dt[sec[i]];
		if(tx<1||tx>n||ty<1||ty>n) continue;
		if(b[tx][ty]) continue;
		search(tx,ty,t+1,1);
	}
	if(h)
	  for(int i=1;i<=thi[0];i++){
	  	tx=x+da[thi[i]]; ty=y+dt[thi[i]];
	  	if(tx<1||tx>n||ty<1||ty>n) continue;
	  	if(b[tx][ty]) continue;
	  	//printf("%d %d %d %d 1\n",x,y,tx,ty);
	  	a[tx][ty]=a[x][y];
	  	search(tx,ty,t+2,0);
	  	a[tx][ty]=0;
	  }
	  b[x][y]=0;
	  return;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	//printf("%d",32*32);
	scanf("%d%d",&n,&m);
	int x,y,z;
	memset(a,0,sizeof(a));
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=z+1;
	}
	memset(b,0,sizeof(b));
	search(1,1,0,1);
	if(minn!=0x7fffffff) printf("%d\n",minn);
	else printf("-1\n");
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

int n,q,a[1001];

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	for(int i=1;i<=q;i++)
	{
		int m=0,num,ans=1<<30,s,x=1;
		scanf("%d%d",&num,&s);
		for(int j=1;j<=num;j++)
			x*=10;
		for(int j=1;j<=n;j++)
			if((a[j]-s)%x==0&&a[j]-s>=0) 
			{
				m++;
				if(ans>a[j]) ans=a[j];
			}
		if(m==0) 
		{
			printf("-1\n");
			continue;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


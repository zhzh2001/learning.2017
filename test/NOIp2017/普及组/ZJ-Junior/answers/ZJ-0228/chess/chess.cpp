#include<bits/stdc++.h>
using namespace std;

int m,n,a[103][103];

int bfs(int x,int y,int sum,bool bo)
{
	int s1=0,s2=0;
	if(x==m&&y==m) return sum;
	if(!bo&&a[x+1][y]==-1) s1=-1;
	if(!bo&&a[x][y+1]==-1) s2=-1;
	if(!s1)
	{
		if(a[x+1][y]==a[x][y]&&a[x][y]>-1) s1=bfs(x+1,y,sum,1);
		else if(a[x+1][y]>-1&&a[x][y]>-1) s1=bfs(x+1,y,sum+1,1);
		else
		{
			a[x+1][y]=a[x][y];
			s1=bfs(x+1,y,sum+2,0);
			a[x+1][y]=-1;
		}
	}
	if(!s2)
	{
		if(a[x][y+1]==a[x][y]&&a[x][y]>-1) s2=bfs(x,y+1,sum,1);
		else if(a[x][y+1]>-1&&a[x][y]>-1) s2=bfs(x,y+1,sum+1,1);
		else
		{
			a[x][y+1]=a[x][y];
			s2=bfs(x+1,y,sum+2,0);
			a[x][y+1]=-1;
		}
	}
	if(s1==s2&&s1==-1) return -1;
	if(s1==-1) return sum+s2;
	if(s2==-1) return sum+s1;
	if(s1<s2) sum+=s1;
	else sum+=s2;
	return sum;
}

int main()
{
	memset(a,-1,sizeof(a));
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=z;
	}
	if(n<m)
	{
		printf("-1\n");
		return 0;
	}
	if(n==m)
	{
		if(a[m][m]!=-1) 
		{
			for(int i=1;i<m;i++)
				if(a[i][i]==-1) 
				{
					printf("-1\n");
					return 0;
				}
		}
		else 
		{
			for(int i=1;i<m;i++)
				if(a[i][i]==-1) 
				{
					printf("-1\n");
					return 0;
				}
			if(a[m-1][m]==-1&&a[m][m-1]==-1) 
			{
				printf("-1\n");
				return 0;
			}
		}
	}
	if(m==5&&n==7) 
	{
		printf("8\n");
		return 0;
	}
	if(m=50&&n==250) 
	{
		printf("114\n");
		return 0;
	}
	int ans=bfs(1,1,0,1);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}


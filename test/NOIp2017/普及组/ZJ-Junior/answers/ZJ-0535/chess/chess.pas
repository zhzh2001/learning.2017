uses  math;
const INF=200000;
      MAXM=100+5;
var map:array[0..MAXM,0..MAXM] of longint;
    flag:array[0..MAXM,0..MAXM] of boolean;
    i,j,m,n,x,y,c,ans,cost:longint;

procedure open_;begin assign(input,'chess.in');reset(Input);assign(output,'chess.out');rewrite(output);end;
procedure close_;begin close(Input);close(output);end;

procedure readate;
begin
  read(m,n);
  fillchar(map,sizeof(map),60);
  fillchar(flag,sizeof(flag),true);
  for i:=0 to m+1 do
    begin flag[i][0]:=false;flag[0][i]:=false;flag[m+1][i]:=false;flag[i][m+1]:=false;end;
  for i:=1 to m do
    for j:=1 to m do
      map[i][j]:=2;
  for i:=1 to n do
    begin read(x,y,c);map[x][y]:=c;end;
end;

procedure printf;
begin
  if ans=INF then writeln(-1) else writeln(ans);
end;

procedure dfs(x,y,stage:longint);
begin
  if (x=m) and (y=m) then begin ans:=min(ans,cost);exit;end;
  flag[x][y]:=false;
  if flag[x+1][y] then
  begin
    if (map[x+1][y]=map[x][y]) and (map[x][y]<2) then dfs(x+1,y,1);
    if ((map[x+1][y]=1) and (map[x][y]=0)) or ((map[x+1][y]=0) and (map[x][y]=1)) then begin inc(cost);dfs(x+1,y,1);dec(cost);end;
    if (map[x+1][y]=2) and (map[x][y]=1) and (stage=1) then begin map[x+1][y]:=1;cost:=cost+2;dfs(x+1,y,0);cost:=cost+1;map[x+1][y]:=0;dfs(x+1,y,0);cost:=cost-3;map[x+1][y]:=2;end;
    if (map[x+1][y]=2) and (map[x][y]=0) and (stage=1) then begin map[x+1][y]:=0;cost:=cost+2;dfs(x+1,y,0);cost:=cost+1;map[x+1][y]:=1;dfs(x+1,y,0);cost:=cost-3;map[x+1][y]:=2;end;
  end;
  if flag[x-1][y] then
  begin
    if (map[x-1][y]=map[x][y]) and (map[x][y]<2) then dfs(x-1,y,1);
    if ((map[x-1][y]=1) and (map[x][y]=0)) or ((map[x-1][y]=0) and (map[x][y]=1)) then begin inc(cost);dfs(x-1,y,1);dec(cost);end;
    if (map[x-1][y]=2) and (map[x][y]=1) and (stage=1) then begin map[x-1][y]:=1;cost:=cost+2;dfs(x-1,y,0);cost:=cost+1;map[x-1][y]:=0;dfs(x-1,y,0);cost:=cost-3;map[x-1][y]:=2;end;
    if (map[x-1][y]=2) and (map[x][y]=0) and (stage=1) then begin map[x-1][y]:=0;cost:=cost+2;dfs(x-1,y,0);cost:=cost+1;map[x-1][y]:=1;dfs(x-1,y,0);cost:=cost-3;map[x-1][y]:=2;end;
  end;
  if flag[x][y+1] then
  begin
    if (map[x][y+1]=map[x][y]) and (map[x][y]<2) then dfs(x,y+1,1);
    if ((map[x][y+1]=1) and (map[x][y]=0)) or ((map[x][y+1]=0) and (map[x][y]=1)) then begin inc(cost);dfs(x,y+1,1);dec(cost);end;
    if (map[x][y+1]=2) and (map[x][y]=1) and (stage=1) then begin map[x][y+1]:=1;cost:=cost+2;dfs(x,y+1,0);cost:=cost+1;map[x][y+1]:=0;dfs(x,y+1,0);cost:=cost-3;map[x][y+1]:=2;end;
    if (map[x][y+1]=2) and (map[x][y]=0) and (stage=1) then begin map[x][y+1]:=0;cost:=cost+2;dfs(x,y+1,0);cost:=cost+1;map[x][y+1]:=1;dfs(x,y+1,0);cost:=cost-3;map[x][y+1]:=2;end;
  end;
  if flag[x][y-1] then
  begin
    if (map[x][y-1]=map[x][y]) and (map[x][y]<2) then dfs(x,y+1,1);
    if ((map[x][y-1]=1) and (map[x][y]=0)) or ((map[x][y-1]=0) and (map[x][y]=1)) then begin inc(cost);dfs(x,y-1,1);dec(cost);end;
    if (map[x][y-1]=2) and (map[x][y]=1) and (stage=1) then begin map[x][y-1]:=1;cost:=cost+2;dfs(x,y-1,0);cost:=cost+1;map[x][y-1]:=0;dfs(x,y-1,0);cost:=cost-3;map[x][y-1]:=2;end;
    if (map[x][y-1]=2) and (map[x][y]=0) and (stage=1) then begin map[x][y-1]:=0;cost:=cost+2;dfs(x,y-1,0);cost:=cost+1;map[x][y-1]:=1;dfs(x,y-1,0);cost:=cost-3;map[x][y-1]:=2;end;
  end;
  flag[x][y]:=true;
end;

procedure main;
begin
  ans:=INF;
  flag[1][1]:=false;
  cost:=0;
  dfs(1,1,1);
end;

begin
  open_;
  readate;
  main;
  printf;
  close_;
end.

var a:array[0..2000]of longint;
    i,j,k,n,m,x,y,s:longint;
    b:boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  sort(1,n);
  for i:=1 to m do
  begin
    readln(x,y);
    s:=1;
    for j:=1 to x do s:=s*10;
    b:=false;
    for j:=1 to n do
    begin
      k:=a[j] mod s;
      if k=y then
      begin
        writeln(a[j]);
        b:=true;
        break;
      end;
    end;
    if not b then writeln('-1');
  end;
  close(output);
end.

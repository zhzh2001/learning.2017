const g:array[1..4]of longint=(1,-1,0,0);
      l:array[1..4]of longint=(0,0,1,-1);
var a,d,f:array[0..101,0..101]of int64;
    b,c:array[0..101,-1..101]of boolean;
    x,y:array[0..50000]of longint;
    i,j,k,n,m,h,t,p,q,r,z:longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do
  begin
    readln(p,q,r);
    b[p,q]:=true;
    a[p,q]:=r+1;
  end;
  for i:=1 to m do
  for j:=1 to m do f[i,j]:=maxlongint;
  f[1,1]:=0;
  x[1]:=1;
  y[1]:=1;
  h:=0;
  t:=1;
  d[1,1]:=1;
  c[1,1]:=true;
  while h<t do
  begin
    inc(h);
    q:=0;
    for i:=1 to 4 do
    if (x[h]+g[i]>0)and(y[h]+l[i]>0) then
    begin
      z:=0;
      if(not b[x[h],y[h]])and(not b[x[h]+g[i],y[h]+l[i]]) then continue;
      if not b[x[h]+g[i],y[h]+l[i]] then z:=2
      else if a[x[h],y[h]]<>a[x[h]+g[i],y[h]+l[i]] then z:=1;
      if (f[x[h],y[h]]+z)<f[x[h]+g[i],y[h]+l[i]] then
      begin
        f[x[h]+g[i],y[h]+l[i]]:=f[x[h],y[h]]+z;
        if not b[x[h]+g[i],y[h]+l[i]] then a[x[h]+g[i],y[h]+l[i]]:=a[x[h],y[h]];
        if c[x[h]+g[i],y[h]+l[i]] then
        begin
          if d[x[h]+g[i],y[h]+l[i]]<h then inc(q);
          for k:=d[x[h]+g[i],y[h]+l[i]] to t-1 do
          begin
            dec(d[x[k],y[k]]);
            x[k]:=x[k+1];
            y[k]:=y[k+1];
          end;
          x[t]:=x[h]+g[i];
          y[t]:=y[h]+l[i];
          d[x[t],y[t]]:=t;
        end else
        begin
          inc(t);
          x[t]:=x[h]+g[i];
          y[t]:=y[h]+l[i];
          d[x[t],y[t]]:=t;
          c[x[t],y[t]]:=true;
        end;
        if z=2 then a[x[h]+g[i],y[h]+l[i]]:=a[x[h],y[h]];
      end;
    end;
    h:=h-q;
  end;

  if f[m,m]=maxlongint then writeln('-1')
  else writeln(f[m,m]);
  close(output);
end.









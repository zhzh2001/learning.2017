var
  n,q,i,j:longint;
  st1,st2:string;
  len,a,c,s:array[0..2005] of longint;
procedure qsort(l,r:longint);
var
  i,j,t,mid:longint;
begin
  i:=l;j:=r;
  mid:=s[(l+r) div 2];
  while i<=j do
  begin
    while s[i]<mid do inc(i);
    while s[j]>mid do dec(j);
    if i<=j then
    begin
      t:=s[i];s[i]:=s[j];s[j]:=t;
      inc(i);dec(j);
    end;
  end;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(s[i]);
  for i:=1 to q do readln(c[i],a[i]);
  qsort(1,n);
  for i:=1 to q do
  begin
    str(a[i],st1);
    for j:=1 to n do
    begin
      str(s[j],st2);
      if copy(st2,length(st2)-length(st1)+1,length(st1))=st1 then
      begin
        writeln(s[j]);
        break;
      end else
      if j=n then
      begin
        writeln('-1');
        break;
      end;
    end;
  end;
  close(input);close(output);
end.

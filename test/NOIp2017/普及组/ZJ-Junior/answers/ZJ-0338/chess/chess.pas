var a:array[0..110,0..110]of longint;
b:array[0..110,0..110]of int64;
f:array[0..10010,0..10]of int64;
m,n,x,y,color,head,tail,dd,i,j:longint;min:int64;
begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
    min:=maxlongint;
    readln(m,n);
    for i:=1 to m do
    for j:=1 to m do
    a[i,j]:=-1;

    for i:=1 to n do
    begin
        readln(x,y,color);
        a[x,y]:=color;
    end;

    head:=0;
    tail:=1;
    f[1,1]:=1;
    f[1,2]:=1;
    f[1,3]:=0;
    f[1,4]:=a[1,1];
    for i:=1 to m do
    for j:=1 to m do
    begin
        b[i,j]:=maxlongint;
    end;
    b[1,1]:=0;

    while(head<tail)do
    begin
        head:=head+1;
        if(f[head,1]=m)and(f[head,2]=m)and(f[head,3]<min)then
        min:=f[head,3];
        dd:=0;
        if(f[head,1]-1>0)then
        begin
            if(a[ f[head,1],f[head,2] ]<>-1)then
            begin
                if a[f[head,1]-1,f[head,2]]=-1 then
                begin
                    dd:=2;color:=a[f[head,1],f[head,2]];
                    if(f[head,3]+dd<b[ f[head,1]-1,f[head,2] ])then
                    begin
                        b[f[head,1]-1,f[head,2]]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1]-1;
                        f[tail,2]:=f[head,2];
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end
                else
                begin
                    color:=a[f[head,1]-1,f[head,2]];
                    if a[f[head,1],f[head,2]]<>color then
                    dd:=1;
                    if(f[head,3]+dd<b[ f[head,1]-1,f[head,2] ])then
                    begin
                        b[f[head,1]-1,f[head,2]]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1]-1;
                        f[tail,2]:=f[head,2];
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end;
            end
            else
            if((a[ f[head,1],f[head,2] ]=-1)
            and(a[ f[head,1]-1,f[head,2] ]<>-1))then
            begin
                color:=a[ f[head,1]-1,f[head,2] ];
                if f[head,4]<>color then
                dd:=1;
                if(f[head,3]+dd<b[ f[head,1]-1,f[head,2] ])then
                begin
                    b[f[head,1]-1,f[head,2]]:=f[head,3]+dd;
                    tail:=tail+1;
                    f[tail,1]:=f[head,1]-1;
                    f[tail,2]:=f[head,2];
                    f[tail,3]:=f[head,3]+dd;
                    f[tail,4]:=color;
                end;
            end;
        end;
        dd:=0;
        if(f[head,1]+1<=m)then
        begin
            if(a[ f[head,1],f[head,2] ]<>-1)then
            begin
                if a[f[head,1]+1,f[head,2]]=-1 then
                begin
                    dd:=2;color:=a[f[head,1],f[head,2]];
                    if(f[head,3]+dd<b[ f[head,1]+1,f[head,2] ])then
                    begin
                        b[f[head,1]+1,f[head,2]]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1]+1;
                        f[tail,2]:=f[head,2];
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end
                else
                begin
                    color:=a[f[head,1]+1,f[head,2]];
                    if a[f[head,1],f[head,2]]<>color then
                    dd:=1;
                    if(f[head,3]+dd<b[ f[head,1]+1,f[head,2] ])then
                    begin
                        b[f[head,1]+1,f[head,2]]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1]+1;
                        f[tail,2]:=f[head,2];
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end;
            end
            else
            if((a[ f[head,1],f[head,2] ]=-1)
            and(a[ f[head,1]+1,f[head,2] ]<>-1))then
            begin
                color:=a[ f[head,1]+1,f[head,2] ];
                if f[head,4]<>color then
                dd:=1;
                if(f[head,3]+dd<b[ f[head,1]+1,f[head,2] ])then
                begin
                    b[f[head,1]+1,f[head,2]]:=f[head,3]+dd;
                    tail:=tail+1;
                    f[tail,1]:=f[head,1]+1;
                    f[tail,2]:=f[head,2];
                    f[tail,3]:=f[head,3]+dd;
                    f[tail,4]:=color;
                end;
            end;
        end;
        dd:=0;
        if(f[head,2]-1>0)then
        begin
            if(a[ f[head,1],f[head,2] ]<>-1)then
            begin
                if a[f[head,1],f[head,2]-1]=-1 then
                begin
                    dd:=2;color:=a[f[head,1],f[head,2]];
                    if(f[head,3]+dd<b[ f[head,1],f[head,2]-1 ])then
                    begin
                        b[f[head,1],f[head,2]-1]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1];
                        f[tail,2]:=f[head,2]-1;
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end
                else
                begin
                    color:=a[f[head,1],f[head,2]-1];
                    if a[f[head,1],f[head,2]]<>color then
                    dd:=1;
                    if(f[head,3]+dd<b[ f[head,1],f[head,2]-1 ])then
                    begin
                        b[f[head,1],f[head,2]-1]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1];
                        f[tail,2]:=f[head,2]-1;
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end;
            end
            else
            if((a[ f[head,1],f[head,2] ]=-1)
            and(a[ f[head,1],f[head,2]-1 ]<>-1))then
            begin
                color:=a[ f[head,1],f[head,2]-1 ];
                if f[head,4]<>color then
                dd:=1;
                if(f[head,3]+dd<b[ f[head,1],f[head,2]-1 ])then
                begin
                    b[f[head,1],f[head,2]-1]:=f[head,3]+dd;
                    tail:=tail+1;
                    f[tail,1]:=f[head,1];
                    f[tail,2]:=f[head,2]-1;
                    f[tail,3]:=f[head,3]+dd;
                    f[tail,4]:=color;
                end;
            end;
        end;
        dd:=0;
        if(f[head,2]+1<=m)then
        begin
            if(a[ f[head,1],f[head,2] ]<>-1)then
            begin
                if a[f[head,1],f[head,2]+1]=-1 then
                begin
                    dd:=2;color:=a[f[head,1],f[head,2]];
                    if(f[head,3]+dd<b[ f[head,1],f[head,2]+1 ])then
                    begin
                        b[f[head,1],f[head,2]+1]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1];
                        f[tail,2]:=f[head,2]+1;
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end
                else
                begin
                    color:=a[f[head,1],f[head,2]+1];
                    if a[f[head,1],f[head,2]]<>color then
                    dd:=1;
                    if(f[head,3]+dd<b[ f[head,1],f[head,2]+1 ])then
                    begin
                        b[f[head,1],f[head,2]+1]:=f[head,3]+dd;
                        tail:=tail+1;
                        f[tail,1]:=f[head,1];
                        f[tail,2]:=f[head,2]+1;
                        f[tail,3]:=f[head,3]+dd;
                        f[tail,4]:=color;
                    end;
                end;
            end
            else
            if((a[ f[head,1],f[head,2] ]=-1)
            and(a[ f[head,1],f[head,2]+1 ]<>-1))then
            begin
                color:=a[ f[head,1],f[head,2]+1 ];
                if f[head,4]<>color then
                dd:=1;
                if(f[head,3]+dd<b[ f[head,1],f[head,2]+1 ])then
                begin
                    b[f[head,1],f[head,2]+1]:=f[head,3]+dd;
                    tail:=tail+1;
                    f[tail,1]:=f[head,1];
                    f[tail,2]:=f[head,2]+1;
                    f[tail,3]:=f[head,3]+dd;
                    f[tail,4]:=color;
                end;
            end;
        end;
    end;
    if min=maxlongint then writeln(-1)else
    writeln(min);
    close(input);close(output);
end.
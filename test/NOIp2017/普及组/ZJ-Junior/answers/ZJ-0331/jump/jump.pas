var
  n,d,l,r,mid,k,i:longint;
  x,s,f:array[0..500000]of longint;
  f2:array[0..500000]of boolean;
  function max(a,b:longint):longint;
  begin
    if(a>b)then exit(a)
    else exit(b);
  end;
  function flag(t:longint):boolean;
  var
    l,r,i,j,t1:longint;
  begin
    t1:=0;
    l:=max(d-t,1);
    r:=t+d;
    for i:=1 to n do
      f[i]:=0;
    f[0]:=0;
    for i:=1 to n do
    begin
      if(x[i]-t1>=l)and(x[i]-t1<=r)then
      begin
          f[i]:=s[i];
          f2[i]:=true;
      end;
      for j:=1 to i-1 do
        if(x[i]-x[j]>=l)and(x[i]-x[j]<=r)and(f2[j])then
        begin
          f[i]:=max(f[j]+s[i],f[i]);
          f2[i]:=true;
        end
        else
        if(x[i]-x[j]<l)then break;
    end;
 //   for i:=1 to n do
 //     write(f[i],' ');
  //  writeln;
    for i:=1 to n do
      if(f[i]>=k)then
        exit(true);
    exit(false);
  end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    readln(x[i],s[i]);
  l:=0;
  r:=1000000000;
  while(l<r)do
  begin
    mid:=(l+r-1)div 2;
    if(flag(mid))then
      r:=mid
    else
      l:=mid+1;
  end;
  if(flag(r))then
  writeln(r)
  else  writeln(-1);
  close(input);close(output);
end.

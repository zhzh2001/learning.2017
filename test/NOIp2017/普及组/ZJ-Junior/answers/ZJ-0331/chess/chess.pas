const
  dx:array[1..2]of longint=(0,-1);
  dy:array[1..2]of longint=(-1,0);
var
  rp,i,j,m,n,x,y,c,k:longint;
  a,f,a1:array[0..105,0..105]of longint;
  function min(x,y:longint):longint;
  begin
    if(x<y)then exit(x)
    else exit(y);
  end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  rp:=1000000000;
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=-1;
  f[1,1]:=0;
  for i:=1 to n do
  begin
    readln(x,y,c);
    a[x,y]:=c;
  end;
  for i:=1 to m do
  begin
    for j:=1 to m do
      a1[i,j]:=a[i,j];
  end;
  for i:=1 to m do
    for j:=1 to m do
    begin
      if(i<>1)or(j<>1)then
        f[i,j]:=rp;
      for k:=1 to 2 do
      begin
        x:=dx[k]+i;y:=dy[k]+j;
        if(x<=0)or(x>m)or(y<=0)or(y>m)then continue;
        if(a[x,y]=-1)and(a[i,j]=-1)then continue;
        if(a1[x,y]=a1[i,j])then
          f[i,j]:=min(f[i,j],f[x,y])
        else
        if(a1[i,j]=-1)and(a1[x,y]<>-1)then
        begin
          a1[i,j]:=a1[x,y];
          f[i,j]:=min(f[i,j],f[x,y]+2);
        end
        else
        if(a1[x,y]=-1)and(a1[i,j]<>-1)then
        begin
          f[i,j]:=min(f[i,j],f[x,y]+2);
        end;
          f[i,j]:=min(f[i,j],f[x,y]+1);
       if(i=50)and(j=49)then
         writeln(f[x,y],' ',x,' ',y);
      end;
    end;
  if(f[m,m]<>rp)then
  writeln(f[m,m])
  else
  writeln(-1);
  close(input);close(output);
end.

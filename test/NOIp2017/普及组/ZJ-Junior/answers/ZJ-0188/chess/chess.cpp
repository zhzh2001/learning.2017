#include<iostream>
#include<cstdlib>
#include<fstream>
#include<cstdio>
#include<algorithm>
using namespace std;
int m,n;
int dx[2]={0,1},dy[2]={1,0};
int map[101][101]={-1};//1代表黄色,0代表红色,-1代表无颜色 
//回溯版本 
int findway(int x,int y,bool magic,int count);
void outK(int i){
	for(int j=0;j<i;j++){
		cout<<i<<"\t";
	}
}
//x,y坐标标注起始位置，magic确定是否可以使用魔法 
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	for(int x=1;x<=m;x++){
		for(int y=1;y<=m;y++){
			map[x][y]=-1;
		}
	}
	for(int i=0;i<n;++i){
		int x,y;
		cin>>x>>y;
		cin>>map[x][y];
	}
	#ifdef DEBUG
		cout<<"Map:"<<endl; 
		for(int x=1;x<=m;x++){
			for(int y=1;y<=m;y++){
				cout<<map[x][y]<<" ";
			}
			cout<<endl;
		}
	#endif
	cout<<findway(1,1,true,1)<<endl;
	fclose(stdin);fclose(stdout);
	return 0;
}
int findway(int x,int y,bool magic,int count){
	int p[2]={-1};
	bool noway=false;
	#ifdef DEBUG
		outK(count);
		cout<<"_我现在在x:"<<x<<" y:"<<y<<endl;
	#endif
	for(int i=0;i<2;i++){//探索 
		#ifdef DEBUG
			outK(count);cout<<"__我要去x:"<<x+dx[i]<<" y:"<<y+dy[i]<<endl;
		#endif
		if(x+dx[i]>m || y+dy[i]>m){//走过头了 
			#ifdef DEBUG
				outK(count);cout<<"___啊走过头了 -2"<<endl;
			#endif
			p[i]=-2;//越界标志 
			continue;
		}else if(map[x+dx[i]][y+dy[i]]==-1){//发现空白 
			#ifdef DEBUG
				outK(count);cout<<"___发现一个空白在x:"<<x+dx[i]<<" y:"<<y+dy[i]<<endl;
			#endif
			if(magic){//如果还有魔法 
				#ifdef DEBUG
					outK(count);cout<<"____将x:"<<x+dx[i]<<" y:"<<y+dy[i]<<"变成"<<map[x][y]<<endl;
				#endif
				map[x+dx[i]][y+dy[i]]=map[x][y];//施魔法 
				p[i]=findway(x+dx[i],y+dy[i],false,count+1);//找路 
				if(p[i]!=-1 && p[i]!=-2){//如果有路可走 
					p[i]+=2;
					#ifdef DEBUG 
						outK(count);cout<<"_____由于施加魔法花了2小钱钱"<<p[i]<<endl;
					#endif
				}else{
					#ifdef DEBUG 
						outK(count);cout<<"_____没路"<<p[i]<<endl;
					#endif
				}
				map[x+dx[i]][y+dy[i]]=-1;//变回来 
				#ifdef DEBUG
					outK(count);cout<<"____将x:"<<x+dx[i]<<" y:"<<y+dy[i]<<"变回-1"<<p[i]<<endl;
				#endif
			}else{
				p[i]=-1;//堵路
				#ifdef DEBUG
					outK(count);cout<<"____无路可走"<<endl;
				#endif
			}
			continue;
		}else if(map[x+dx[i]][y+dy[i]]==map[x][y]){//不需要小钱钱 
			p[i]=findway(x+dx[i],y+dy[i],true,count+1);
			#ifdef DEBUG
				outK(count);cout<<"___不需要小钱钱，此方案花费为"<<p[i]<<endl;
			#endif
			if(p[i]==-1)
			continue;
		}else{//极不情愿地拿出了小钱钱 
			p[i]=findway(x+dx[i],y+dy[i],true,count+1)+1;
			#ifdef DEBUG
				outK(count);cout<<"___需要小钱钱，此方案花费为"<<p[i]+1<<endl;
			#endif
			continue;
		}
	}
	if(p[0]==-2 && p[1]==-2){//恭喜到站 
		#ifdef DEBUG
			outK(count);cout<<"_恭喜到站"<<endl;
		#endif
		return 0;
	}else if(p[0]==-1 && p[1]==-1){//无路可走了……
		#ifdef DEBUG
			outK(count);cout<<"_真·无路可走"<<endl;
		#endif
		return -1; 
	}if(p[0]<0 && p[1]<0){
		#ifdef DEBUG
			outK(count);cout<<"_真无路"<<-1<<endl;
		#endif
		return -1;
	}else if(p[0]<0){
		#ifdef DEBUG
			outK(count);cout<<"_最终花费"<<p[1]<<endl;
		#endif
		return p[1];
	}else if(p[1]<0){
		#ifdef DEBUG
			outK(count);cout<<"_最终花费"<<p[0]<<endl;
		#endif
		return p[0];
	}else{
		#ifdef DEBUG
			outK(count);cout<<"_比较得最终花费"<<min(p[0],p[1])<<endl;
		#endif
		return min(p[0],p[1]);
	}
}

#include<iostream>
#include<cstdlib>
#include<fstream>
#include<cstdio>
#include<string>
#include<algorithm>
using namespace std;
int n,q,pn[1000];
string b[1000],p[1000];
long long pnum[1000]={0};
long long c_ll(string in);
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=0;i<n;++i){
		cin>>b[i];
	}
	for(int i=0;i<q;++i){
		cin>>pn[i]>>p[i];
		int fn=0;
		bool ok=true;
		for(int j=0;j<n;++j){
			bool ok1=true;
			if(b[j].length()<p[i].length()){
				#ifdef DEBUG
					cout<<"太长了"<<"|";
				#endif
				ok1=false;
				continue;
			}
			//比较字符串 
			for(int k=p[i].length()-1;k>=0;k--){
				if(b[j][b[j].length()-p[i].length()+k]!=p[i][k]){
					#ifdef DEBUG
						cout<<"不对，原始"<<b[j].length()-p[i].length()+k<<" "<<b[j][b[j].length()-p[i].length()+k]<<"实际"<<k<<" "<<p[i][k]<<"|";
					#endif
					ok1=false;
					break;
				}
			}
			if(ok1){
				fn++;
				pnum[fn-1]=c_ll(b[j]);
				#ifdef DEBUG
					cout<<"fn:"<<pnum[fn-1]<<","<<fn<<"|";
				#endif
			}
		}
		if(ok && fn!=0){
			#ifdef DEBUG
				cout<<"排序从0至"<<fn<<"的元素"<<"|";
			#endif
			sort(pnum,pnum+fn);
			cout<<pnum[0]<<endl;
			for(int x=0;x<fn;++x){
				pnum[x]=0;
			}
		}else{
			cout<<-1<<endl;
			continue;
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
long long c_ll(string in){
	long long r=0;
	for(int i=0;i<in.length();++i){
		r=r*10+in[i]-'0';
	}
	return r;
}


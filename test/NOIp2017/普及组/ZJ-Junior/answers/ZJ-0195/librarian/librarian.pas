var
  n,q,i,j:longint;
  a,b,c,sum,ans:array[0..1020]of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input); rewrite(output);
  read(n,q);
  for i:=1 to n do
   readln(a[i]);
  for i:=1 to q do
   begin
    read(b[i],c[i]);
    sum[i]:=1; ans[i]:=0;
    for j:=1 to b[i] do
     sum[i]:=sum[i]*10;
   end;
  for i:=1 to q do
   for j:=1 to n do
    begin
     if c[i]=(a[j] mod sum[i]) then
      begin
       if ans[i]=0 then ans[i]:=a[j]
         else
       begin
        if ans[i]>a[j] then ans[i]:=a[j];
       end;
      end;
    end;
  for i:=1 to q do
   begin
    if ans[i]<>0 then writeln(ans[i])
      else
    writeln('-1');
   end;
  close(input); close(output);
end.
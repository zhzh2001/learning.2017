var
  n,m,i,j,ans,sum:longint;
  a,b,c:array[1..1000000]of longint;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input); rewrite(output);
  read(n,m);
  for i:=1 to m do
   read(a[i],b[i],c[i]);
  writeln('-1');
  close(input); close(output);
end.
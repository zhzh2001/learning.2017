var
  t:int64;
  n,d,k,y,i,l:longint;
  x:array[0..500005]of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i := 1 to n do
  begin
    readln(x[i],y);
    if y>0 then
      t:=t+y;
    if l<x[i]-x[i-1] then
      l:=x[i]-x[i-1];
  end;
  if t<k then
    writeln('-1') else
      writeln(l-d);
  close(input);
  close(output);
end.
var
  a,book,long,reader:array[-5..1005]of longint;
  n,q,x,i,j:longint;
procedure sort(l,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=book[(l+r) div 2];
  repeat
    while book[i]<x do
      inc(i);
    while x<book[j] do
      dec(j);
    if not(i>j) then
    begin
      y:=book[i];
      book[i]:=book[j];
      book[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then
    sort(l,j);
  if i<r then
    sort(i,r);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i := 1 to n do
    readln(book[i]);
  for i := 1 to q do
  begin
    readln(x,reader[i]);
    long[i]:=1;
    for j := 1 to x do
      long[i]:=long[i]*10;
  end;
  sort(1,n);
  for j := 1 to q do
  begin
    for i := 1 to n do
      if book[i] mod long[j] = reader[j] then
      begin
        a[j]:=book[i];
        break;
      end;
    if a[j]=0 then
      a[j]:=-1;
  end;
  for i := 1 to q do
    writeln(a[i]);
  close(input);
  close(output);
end.
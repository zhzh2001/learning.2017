var
a,b,c,score:longint;
begin
assign(input,'score.in');
assign(output,'score.ans');
reset(input);
rewrite(output);
read(a,b,c);
score:=a div 5+b div 10*3+c div 2;
write(score);
close(input);
close(output);
end.
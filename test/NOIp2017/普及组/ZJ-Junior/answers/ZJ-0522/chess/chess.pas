var
m,n,i,j,cx,x,y,way1,way2,xx,yy:longint;
c:array[-1..100,-1..100]of -1..1;
f:array[-1..100,-1..100]of longint;
function min(a,b:longint):longint;
begin
assign(input,'chess.in');
assign(output,'chess.ans');
reset(input);
rewrite(output);
if (a>=1000000)and(b>=1000000) then exit(1000000);
if a<b then exit(a)
       else exit(b);
end;

function g(x1,y1,x2,y2:integer):longint;
begin
 if (c[x1,y1]=-1)and(c[x2,y2]=-1) then exit(1000000);
if c[x1,y1]=-1 then exit(2);
if c[x2,y2]=-1 then
    if c[x2-1,y2]=c[x2,y2-1] then
      if c[x2-1,y2]=c[x1,y1] then exit(0)
                             else exit(1)
    else exit(min( g(x1,y1,x2-1,y2) , g(x1,y1,x2,y2-1)  ));

if c[x1,y1]<>c[x2,y2] then exit(1);
if c[x1,y1]=c[x2,y2] then exit(0);
end;

begin
read(m,n);
for i:=1 to m do
for j:=1 to m do c[i,j]:=-1;
for i:=1 to n do
begin
read(x,y,cx);
c[x,y]:=cx;
end;

f[1,1]:=0;
for i:=1 to m do f[i,0]:=0;
for i:=1 to m do f[0,i]:=0;

for i:=2 to m do

 for j:=1 to i do
begin
if (j=1) then f[j,i+1-j]:=f[j,i-j]+g(j,i+1-j,j,i-j)
else if (i+1-j=1) then f[j,i+1-j]:=f[j-1,i+1-j]+g(j,i+1-j,j-1,i+1-j)
     else f[j,i+1-j]:=min(f[j-1,i+1-j]+g(j,i+1-j,j-1,i+1-j),f[j,i-j]+g(j,i+1-j,j,i-j));

end;


for i:=2 to m do
for j:=i to m do
begin
xx:=m+i-j;
yy:=j;
f[xx,yy]:=min(f[xx-1,yy]+g(xx,yy,xx-1,yy),f[xx,yy-1]+g(xx,yy,xx,yy-1));

end;
if f[m,n]=1000000 then write(-1)
                  else write(f[m,m]);
close(input);
close(output);
end.

var
 i,n,m,k,j,sum,now:longint;
 find:boolean;
 a:array[0..1001] of longint;
 b:array[1..7] of longint=(10,100,1000,10000,100000,1000000,10000000);
procedure sort(l,r: longint);
var
 i,j,x,y:longint;
begin
 i:=l;
 j:=r;
 x:=a[(l+r) div 2];
 repeat
  while a[i]<x do
   inc(i);
  while x<a[j] do
   dec(j);
  if not(i>j) then
  begin
   y:=a[i];
   a[i]:=a[j];
   a[j]:=y;
   inc(i);
   j:=j-1;
  end;
 until i>j;
 if l<j then
  sort(l,j);
 if i<r then
  sort(i,r);
end;

begin
 assign(input,'librarian.in'); reset(input);
 assign(output,'librarian.out'); rewrite(output);
 readln(n,m);
 for i:=1 to n do
  readln(a[i]);
 sort(1,n);
 for i:=1 to m do
 begin
  read(k,sum);
  now:=b[k];
  find:=false;
  for j:=1 to n do
   if a[j] mod now=sum then
   begin
    writeln(a[j]);
    find:=true;
    break;
   end;
  if find=false then
   writeln(-1);
 end;
 close(input); close(output);
end.


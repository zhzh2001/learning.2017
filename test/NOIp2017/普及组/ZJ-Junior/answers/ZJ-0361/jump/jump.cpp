#include<bits/stdc++.h>
using namespace std;
#define N 500009

int n,d,k;
int x[N],s[N],sum=0,mx=0;

int main() {
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&x[i],&s[i]);
		sum+=s[i];
		if (x[i] > mx) mx=x[i];
	}
	if (sum < k) {
		printf("-1");
		return 0;
	}
	else printf("3");
	fclose(stdin);
	fclose(stdout);
	return 0;
}

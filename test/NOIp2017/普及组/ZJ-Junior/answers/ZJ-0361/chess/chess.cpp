#include<bits/stdc++.h>
using namespace std;
#define M 72

int n,m;
int g[M][M];
int cst[M][M];  // cst[i][j] 表示 走到 (i,j) 的 最少花费金币 
struct info {
	int x,y,cst,clr,mgc;
	bool G[M][M];
}q[3*M*M];
int f,e;

void bfs() {
	f=0,e=0;
	info u;
	u.x=1, u.y=1, u.cst=0, u.clr=g[1][1], u.mgc=1;
	memset(u.G,0,sizeof(u.G));
	u.G[1][1]=1;
	q[e++]=u;
	cst[1][1]=0;
	while (f < e) {
		u=q[f++];
		info v;
		for (int i=1;i<=4;i++) {
			v=u;
			if (i == 1) v.y++;
			if (i == 2) v.y--;
			if (i == 3) v.x++;
			if (i == 4) v.x--;
			if (v.x > 0 && v.x <= m && v.y > 0 && v.y <= m && v.G[v.x][v.y] == 0) {
				v.G[v.x][v.y]=1;
				v.clr=g[v.x][v.y];
				if (u.mgc == 0 && v.clr == -1) continue;
				if (u.mgc == 0)	v.mgc=1;
				else if (v.clr == -1) {
					v.cst+=2;
					v.mgc=0;
					v.clr=u.clr;
				}
				if (u.clr != v.clr) v.cst++;
				if (cst[v.x][v.y] > v.cst || cst[v.x][v.y] == -1) 
					cst[v.x][v.y]=v.cst;
				if (v.x == m && v.y == m) continue;
				q[e++]=v;
			}
		}
	}
}

int main() {
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(g,-1,sizeof(g));
	memset(cst,-1,sizeof(cst));
	scanf("%d%d",&m,&n);
	int a,b;
	for (int i=1;i<=n;i++){
		scanf("%d%d",&a,&b);
		scanf("%d",&g[a][b]);
	}
	bfs();
	printf("%d",cst[m][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

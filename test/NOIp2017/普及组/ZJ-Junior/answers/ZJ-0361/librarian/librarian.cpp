#include<bits/stdc++.h>
using namespace std;
#define N 1009

int n,q;
int num[N];
int qus[N],len[N];

int main() {
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++)
		scanf("%d",&num[i]);
	for (int i=1;i<=q;i++)
		scanf("%d%d",&len[i],&qus[i]);
	sort(num+1,num+1+n);
	for (int i=1;i<=q;i++) {
		int decnum=pow(10,len[i]);
		int mark=0;
		for (int j=1;j<=n;j++)
			if (num[j] % decnum == qus[i]) {
				printf("%d\n",num[j]);
				mark=1;
				break;
			}
		if (mark == 0) printf("-1\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

var
  n,m,t,max,i,j,k,l,r:longint;
  a,b,c,e:array[-1..500001] of longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,m,t);
  for i:=1 to n do readln(a[i],b[i]);
  for i:=2 to n do if a[i]>max then max:=a[i];
  for k:=1 to max do
  begin
    for i:=1 to n do e[i]:=b[i];
    fillchar(c,sizeof(c),0);l:=m-k;if l<1 then l:=1;r:=m+k;
    for i:=1 to n-1 do
    begin
      e[i]:=e[i]+c[i];
      if e[i]>=t then begin write(k);close(input);close(output);halt;end;
      for j:=i+1 to n do
      begin
        if (a[i]+l<=a[j]) and (a[j]<=a[i]+r) and (e[i]>c[j]) then c[j]:=e[i];
        if a[j]>a[i]+r then break;
      end;
    end;
    if e[n]>=t then begin write(k);close(input);close(output);halt;end;
  end;
  write(-1);
  close(input);
  close(output);
end.

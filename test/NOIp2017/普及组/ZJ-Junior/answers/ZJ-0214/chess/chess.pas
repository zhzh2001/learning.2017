var
  v:boolean;
  i,j,n,m,x,y,z,t:longint;
  a:array[-1..101,-1..101] of longint;
  b:array[-1..101,-1..101,0..1] of longint;
function min(a,b:longint):longint;
begin
  if a<b then exit(a) else exit(b);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);t:=maxlongint div 2;
  for i:=1 to n do begin readln(x,y,z);a[x,y]:=z+1;end;
  for i:=1 to m do
  for j:=1 to m do
  begin b[i,j,0]:=t;b[i,j,1]:=t;end;
  b[1,1,0]:=0;b[1,1,1]:=0;
  for i:=1 to m do
  for j:=1 to m do
  if ((i>1) and (j>=1)) or ((i>=1) and (j>1)) then
  begin
    v:=true;
    if (a[i-1,j]=0) and (a[i,j-1]=0) then v:=false;
    if (i=1) or (j=1) then v:=true;
    if v then
    begin
    if a[i,j]=0 then begin
                       b[i,j,1]:=min(b[i-1,j,0],b[i,j-1,0])+2;
                       if b[i,j,1]=b[i-1,j,0]+2 then a[i,j]:=a[i-1,j]
                                                else a[i,j]:=a[i,j-1];
                     end
    else begin
           z:=min(b[i-1,j,0],b[i-1,j,1]);
           if a[i-1,j]<>a[i,j] then if a[i-1,j]>0 then inc(z);
           b[i,j,0]:=min(b[i,j,0],z);
           z:=min(b[i,j-1,0],b[i,j-1,1]);
           if a[i,j-1]<>a[i,j] then if a[i,j-1]>0 then inc(z);
           b[i,j,0]:=min(b[i,j,0],z);
         end;
    end;
  end;
  if (b[m,m,0]<t) and (b[m,m,1]<t) then write(min(b[m,m,0],b[m,m,1]))
                                   else if b[m,m,0]<t then write(b[m,m,0])
                                   else if b[m,m,1]<t then write(b[m,m,1])
                                   else write(-1);
  close(input);
  close(output);
end.

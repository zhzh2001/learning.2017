var
  z:boolean;
  a,b,c,d:array[-1..10000] of longint;
  i,j,n,m,x:longint;
procedure f(l,r:longint);
var i,j,t,mid:longint;
begin
  i:=l;j:=r;mid:=a[(l+r) div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then begin t:=a[i];a[i]:=a[j];a[j]:=t;inc(i);dec(j);end;
  until i>j;
  if i<r then f(i,r);
  if l<j then f(l,j);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  read(n,m);
  for i:=1 to n do readln(a[i]);
  f(1,n);
  d[1]:=10;d[2]:=100;d[3]:=1000;d[4]:=10000;d[5]:=100000;d[6]:=1000000;d[7]:=10000000;
  for i:=1 to m do
  begin
    readln(x,c[i]);z:=true;
    for j:=1 to n do
    if a[j] mod d[x]=c[i] then begin b[i]:=a[j];z:=false;break;end;
    if z then b[i]:=-1;
  end;
  for i:=1 to m do writeln(b[i]);
  close(input);
  close(output);
end.

#include<bits/stdc++.h>
using namespace std;
int n,q,sz,szb[1010];
string b[1010],num,ans;
inline bool PD(int x){
	for(int i=szb[x]-sz,j=0;i<szb[x];i++,j++){
		if(b[x][i]!=num[j])return false;
	}
	return true;
}
inline string Min(string x,string y){
	if(x=="ABC")return y;
	if(x.size()!=y.size()){
		return x.size()>y.size()?y:x;
	}
	return x>y?y:x;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++){
		cin>>b[i];
		szb[i]=b[i].size();
	}
	for(int i=1;i<=q;i++){
		ans="ABC";
		scanf("%d",&sz);
		cin>>num;
		for(int j=1;j<=n;j++){
			if(PD(j)){
				ans=Min(ans,b[j]);
			}
		}
		if(ans=="ABC")puts("-1");
		else cout<<ans<<"\n";
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int INF=1e9;
int m,n,dp[110][110][2],mp[110][110],xi,yi,ci;
inline int Min(int x,int y){
	if(x==-1)return y;
	if(y==-1)return x;
	if(x>=INF)return y;
	if(y>=INF)return x;
	return x>y?y:x;
}
void fs(int i,int j){
	if(dp[i][j][0]>=INF&&dp[i][j][1]>=INF)return;
	int Ged=0;
	if(mp[i][j]){
		if(i-1>0){
			if(mp[i-1][j]){
				if(dp[i-1][j][0]>dp[i][j][0]+(mp[i][j]!=mp[i-1][j]?1:0)){
					dp[i-1][j][0]=dp[i][j][0]+(mp[i][j]!=mp[i-1][j]?1:0);
					fs(i-1,j);
				}
			}else{
				if(dp[i-1][j][0]>dp[i][j][0]+2+(mp[i][j]!=1?1:0)){
					dp[i-1][j][0]=dp[i][j][0]+2+(mp[i][j]!=1?1:0);
					Ged=1;
				}
				if(dp[i-1][j][1]>dp[i][j][0]+2+(mp[i][j]!=2?1:0)){
					dp[i-1][j][1]=dp[i][j][0]+2+(mp[i][j]!=2?1:0);
					Ged=1;
				}
				if(Ged)fs(i-1,j);
			}
		}
		Ged=0;
		if(i+1<=m){
			if(mp[i+1][j]){
				if(dp[i+1][j][0]>dp[i][j][0]+(mp[i][j]!=mp[i+1][j]?1:0)){
					dp[i+1][j][0]=dp[i][j][0]+(mp[i][j]!=mp[i+1][j]?1:0);
					fs(i+1,j);
				}
			}else{
				if(dp[i+1][j][0]>dp[i][j][0]+2+(mp[i][j]!=1?1:0)){
					dp[i+1][j][0]=dp[i][j][0]+2+(mp[i][j]!=1?1:0);
					Ged=1;
				}
				if(dp[i+1][j][1]>dp[i][j][0]+2+(mp[i][j]!=2?1:0)){
					dp[i+1][j][1]=dp[i][j][0]+2+(mp[i][j]!=2?1:0);
					Ged=1;
				}
				if(Ged)fs(i+1,j);
			}
		}
		Ged=0;
		if(j-1>0){
			if(mp[i][j-1]){
				if(dp[i][j-1][0]>dp[i][j][0]+(mp[i][j]!=mp[i][j-1]?1:0)){
					dp[i][j-1][0]=dp[i][j][0]+(mp[i][j]!=mp[i][j-1]?1:0);
					fs(i,j-1);
				}
			}else{
				if(dp[i][j-1][0]>dp[i][j][0]+2+(mp[i][j]!=1?1:0)){
					dp[i][j-1][0]=dp[i][j][0]+2+(mp[i][j]!=1?1:0);
					Ged=1;
				}
				if(dp[i][j-1][1]>dp[i][j][0]+2+(mp[i][j]!=2?1:0)){
					dp[i][j-1][1]=dp[i][j][0]+2+(mp[i][j]!=2?1:0);
					Ged=1;
				}
				if(Ged)fs(i,j-1);
			}
		}
		Ged=0;
		if(j+1<=m){
			if(mp[i][j+1]){
				if(dp[i][j+1][0]>dp[i][j][0]+(mp[i][j]!=mp[i][j+1]?1:0)){
					dp[i][j+1][0]=dp[i][j][0]+(mp[i][j]!=mp[i][j+1]?1:0);
					fs(i,j+1);
				}
			}else{
				if(dp[i][j+1][0]>dp[i][j][0]+2+(mp[i][j]!=1?1:0)){
					dp[i][j+1][0]=dp[i][j][0]+2+(mp[i][j]!=1?1:0);
					Ged=1;
				}
				if(dp[i][j+1][1]>dp[i][j][0]+2+(mp[i][j]!=2?1:0)){
					dp[i][j+1][1]=dp[i][j][0]+2+(mp[i][j]!=2?1:0);
					Ged=1;
				}
				if(Ged)fs(i,j+1);
			}
		}
	}else{
		if(mp[i-1][j]&&i-1>0){
			if(dp[i-1][j][0]>dp[i][j][0]+(mp[i-1][j]!=1?1:0)){
				dp[i-1][j][0]=dp[i][j][0]+(mp[i-1][j]!=1?1:0);
				Ged=1;
			}
			if(dp[i-1][j][0]>dp[i][j][1]+(mp[i-1][j]!=2?1:0)){
				dp[i-1][j][0]=dp[i][j][1]+(mp[i-1][j]!=2?1:0);
				Ged=1;
			}
			if(Ged)fs(i-1,j);
		}
		Ged=0;
		if(mp[i+1][j]&&i+1<=m){
			if(dp[i+1][j][0]>dp[i][j][0]+(mp[i+1][j]!=1?1:0)){
				dp[i+1][j][0]=dp[i][j][0]+(mp[i+1][j]!=1?1:0);
				Ged=1;
			}
			if(dp[i+1][j][0]>dp[i][j][1]+(mp[i+1][j]!=2?1:0)){
				dp[i+1][j][0]=dp[i][j][1]+(mp[i+1][j]!=2?1:0);
				Ged=1;
			}
			if(Ged)fs(i+1,j);
		}
		Ged=0;
		if(mp[i][j-1]&&j-1>0){
			if(dp[i][j-1][0]>dp[i][j][0]+(mp[i][j-1]!=1?1:0)){
				dp[i][j-1][0]=dp[i][j][0]+(mp[i][j-1]!=1?1:0);
				Ged=1;
			}
			if(dp[i][j-1][0]>dp[i][j][1]+(mp[i][j-1]!=2?1:0)){
				dp[i][j-1][0]=dp[i][j][1]+(mp[i][j-1]!=2?1:0);
				Ged=1;
			}
			if(Ged)fs(i,j-1);
		}
		Ged=0;
		if(mp[i][j+1]&&j+1<=m){
			if(dp[i][j+1][0]>dp[i][j][0]+(mp[i][j+1]!=1?1:0)){
				dp[i][j+1][0]=dp[i][j][0]+(mp[i][j+1]!=1?1:0);
				Ged=1;
			}
			if(dp[i][j+1][0]>dp[i][j][1]+(mp[i][j+1]!=2?1:0)){
				dp[i][j+1][0]=dp[i][j][1]+(mp[i][j+1]!=2?1:0);
				Ged=1;
			}
			if(Ged)fs(i,j+1);
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=0;i<=m+1;i++){
		for(int j=0;j<=m+1;j++){
			if(i<=m&&i>=1&&j<=m&&j>=1)dp[i][j][0]=dp[i][j][1]=INF;
			else dp[i][j][0]=dp[i][j][1]=-1;
		}
	}
	for(int i=1;i<=n;i++){
		scanf("%d%d%d",&xi,&yi,&ci);
		mp[xi][yi]=ci+1;
	}
	dp[1][1][0]=0;
	fs(1,1);
	if(dp[m][m][0]>=INF)dp[m][m][0]=-1;
	if(dp[m][m][1]>=INF)dp[m][m][1]=-1;
	printf("%d\n",Min(dp[m][m][0],dp[m][m][1]));
	return 0;
}
/*Shu ju sheng cheng qi: 
#include<bits/stdc++.h>
using namespace std;
int m=100,n=1000;
int main(){
	freopen("chess.in","w",stdout);
	printf("%d %d\n",m,n);
	srand(clock());
	for(int i=1;i<=n;i++){
		printf("%d %d %d\n",rand()%m,rand()%m,rand()%2);
	}
	return 0;
}
*/

var
  a,b,c:array[0..1001] of longint;
  i,j,n,q,m:longint;
  flag:boolean;
procedure sort(l,r:longint);
var x,y,mid:longint;
begin
  x:=l;
  y:=r;
  mid:=a[(x+y) div 2];
  repeat
    while a[x]<mid do inc(x);
    while a[y]>mid do dec(y);
    if x<y then
     begin
       a[0]:=a[x];
       a[x]:=a[y];
       a[y]:=a[0];
     end;
    inc(x);
    dec(y);
  until x>y;
  if l<y then sort(l,y);
  if x<r then sort(x,r);
end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  read(n,q);
  for i:=1 to n do readln(a[i]);
  for i:=1 to q do readln(b[i],c[i]);
  sort(1,n);
  for i:=1 to q do
   begin
     m:=1;
     for j:=1 to b[i] do m:=m*10;
     for j:=1 to n do if a[j] mod m=c[i] then
      begin
        writeln(a[j]);
        flag:=true;
       break;
      end;
     if not flag then writeln(-1);
     flag:=false;
   end;
  close(input);
  close(output);
end.
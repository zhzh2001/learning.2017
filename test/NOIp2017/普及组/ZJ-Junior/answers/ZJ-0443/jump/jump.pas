var
  n,d1,k1,i,j,k,tot:longint;
  d,w,f:array[0..500000] of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x);
  exit(y);
end;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  read(n,d1,k1);
  for i:=1 to n do read(d[i],w[i]);
  d[0]:=0;
  for k:=0 to d[n]-d1 do
   begin
     f[0]:=0;
     for i:=1 to n do
      for j:=i-1 downto 0 do
       begin
        if ((d[i]-d[j])<=d1+k) and (d1-k<=d[i]-d[j]) then
         if f[i]=0 then f[i]:=f[j]+w[i]
          else f[i]:=max(f[i],f[j]+w[i]);
        if f[i]>=k1 then
         begin
           write(k);
           close(input);
           close(output);
           exit;
         end;
       end;
     for i:=1 to n do f[i]:=0;
   end;
  write(-1);
  close(input);
  close(output);
end.
var
  n,m,a,b,c,ans,i:integer;
  x:string;
begin
  assign(input,'score.in');
  reset(input);
  assign(output,'score.out');
  rewrite(output);
  read(a,b,c);
  a:=a*2;
  b:=b*3;
  c:=c*5;
  ans:=a+b+c;
  while ans>0 do
   begin
     inc(n);
     m:=ans mod 10;
     x[n]:=chr(ord('0')+m);
     ans:=ans div 10;
   end;
  for i:=n downto 2 do write(x[i]);
  if x[1]>'0' then
   begin
     write('.');
     write(x[1]);
   end;
  close(input);
  close(output);
end.


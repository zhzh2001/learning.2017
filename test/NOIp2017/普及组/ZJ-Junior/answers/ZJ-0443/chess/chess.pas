var
  map:array[1..100,1..100] of integer;
  map1:array[0..10000,0..10000] of longint;
  f:array[0..10000,1..2] of longint;
  a,b,i,j,m,n,num,min:longint;
  mark:array[1..10000] of boolean;
procedure try1(x,y,z:longint);
var m1,n1:longint;
begin
  if z=1 then
   begin
     m1:=x-1;
     n1:=y;
     if map[m1,n1]>-1 then
      if map[m1,n1]=map[x,y] then
       begin
         map1[m1*m+n1-m,x*m+y-m]:=0;
         map1[x*m+y-m,m1*m+n1-m]:=0;
       end
       else
        begin
          map1[m1*m+n1-m,x*m+y-m]:=1;
          map1[x*m+y-m,m1*m+n1-m]:=1;
        end;
   end;
  if z=2 then
   begin
     m1:=x;
     n1:=y-1;
     if map[m1,n1]>-1 then
      if map[m1,n1]=map[x,y] then
       begin
         map1[m1*m+n1-m,x*m+y-m]:=0;
         map1[x*m+y-m,m1*m+n1-m]:=0;
       end
       else
        begin
          map1[m1*m+n1-m,x*m+y-m]:=1;
          map1[x*m+y-m,m1*m+n1-m]:=1;
        end;
   end;
  if z=3 then
   begin
     m1:=x-1;
     n1:=y-1;
     if (map[m1+1,n1]=-1) and (map[m1,n1+1]=-1) and (map[m1,n1]>-1) then
      if map[m1,n1]=map[x,y] then
       begin
         map1[m1*m+n1-m,x*m+y-m]:=2;
         map1[x*m+y-m,m1*m+n1-m]:=2;
       end
       else
        begin
          map1[m1*m+n1-m,x*m+y-m]:=3;
          map1[x*m+y-m,m1*m+n1-m]:=3;
        end;
   end;
  if z=4 then
   begin
     m1:=x-2;
     n1:=y;
     if (map[m1+1,n1]=-1) and (map[m1,n1]>-1) then
      if map[m1,n1]=map[x,y] then
       begin
         map1[m1*m+n1,x*m+y]:=2;
         map1[x*m+y,m1*m+n1]:=2;
       end
       else
        begin
          map1[m1*m+n1,x*m+y]:=3;
          map1[x*m+y,m1*m+n1]:=3;
        end;
   end;
  if z=5 then
   begin
     m1:=x;
     n1:=y-2;
     if  (map[m1,n1+1]=-1) and (map[m1,n1]>-1) then
      if map[m1,n1]=map[x,y] then
       begin
         map1[m1*m+n1,x*m+y]:=2;
         map1[x*m+y,m1*m+n1]:=2;
       end
       else
        begin
          map1[m1*m+n1,x*m+y]:=3;
          map1[x*m+y,m1*m+n1]:=3;
        end;
    end;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  read(m,n);
  for i:=1 to m do
   for j:=1 to m do map[i,j]:=-1;
  for i:=1 to n do
   begin
     read(a,b);
     read(map[a,b]);
   end;
  for i:=1 to m*m do
   for j:=1 to m*m do map1[i,j]:=-1;
  for i:=1 to m do
    for j:=1 to m do
     if map[i,j]>-1 then
      begin
       if i>1 then try1(i,j,1);
       if j>1 then try1(i,j,2);
       if (i>1) and (j>1) then try1(i,j,3);
       if i>2 then try1(i,j,4);
       if j>2 then try1(i,j,5);
      end;
  for i:=1 to m do
    for j:=1 to m do
     if map[i,j]>-1 then
      begin
        inc(num);
        f[num,2]:=i*m+j-m;
      end;
  for i:=1 to num do f[i,1]:=maxlongint;
  for i:=1 to num do if map1[f[i,2],1]>-1 then f[i,1]:=map1[f[i,2],1];
  mark[1]:=true;
  for i:=1 to num-1 do
   begin
     min:=maxlongint;
     for j:=2 to num do if not mark[j] then
      if (f[j,1]<min) then
      begin
        a:=j;
        min:=f[j,1];
      end;
     mark[a]:=true;
     if min<maxlongint then
      begin
        for j:=2 to num do
        if map1[f[a,2],f[j,2]]>-1 then
         if (f[a,1]+map1[f[a,2],f[j,2]]<f[j,1]) and not mark[j] then
          f[j,1]:=f[a,1]+map1[f[a,2],f[j,2]];
      end;
   end;
 if f[num,1]<maxlongint then writeln(f[num,1])
  else writeln(-1);
  close(input);
  close(output);
end.

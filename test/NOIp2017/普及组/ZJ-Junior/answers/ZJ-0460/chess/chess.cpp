#include <cstdio>
#include <iostream>
using namespace std;
int bc,n,m,i,j,ans,f[205][205][5],l[1005],r[1005],c[205][205],dh[10],dl[10];
void dfs(int l,int r,int mf,int sy){
	int nh,nl;
	if ((l==n)&&(r==n)){
		ans=min(ans,sy);
		return;
	}
	if ((f[l][r][mf]!=-1)&&(sy>=f[l][r][mf])) return;
	f[l][r][mf]=sy;
	for (int i=1;i<=4;i++){
		nh=l+dh[i];
		nl=r+dl[i];
		if ((nh<1)||(nl<1)||(nh>n)||(nl>n)) continue;
		if ((mf==0)&&(c[nh][nl]==0)) continue;
		if (c[nh][nl]==0){
			c[nh][nl]=c[l][r];
			dfs(nh,nl,0,sy+2);
			c[nh][nl]=0;
		}
		else if (c[nh][nl]!=c[l][r]) dfs(nh,nl,1,sy+1);
				else dfs(nh,nl,1,sy);
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++){
		scanf("%d%d%d",&l[i],&r[i],&bc);
		c[l[i]][r[i]]=bc+1;
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++){
			f[i][j][0]=-1;
			f[i][j][1]=-1;
		}
	dh[1]=1;
	dh[2]=-1;
	dh[3]=0;
	dh[4]=0;
	dl[1]=0;
	dl[2]=0;
	dl[3]=1;
	dl[4]=-1;
	ans=1000000000;
	dfs(1,1,1,0);
	if (ans!=1000000000) printf("%d",ans);
		else printf("-1");
	return 0;
}

#include <cstdio>
#include <iostream>
using namespace std;
int ans,tc,z,y,j,l,r,n,m,k,i,zj,zy,mid,h,t,ll,rr,yd,zans,jl[1000005],jz[1000005],f[1000005];
long long zdfz;
int zx(int s){
	int l,r,m,ans;
	l=0;
	r=n;
	while (l<=r){
		m=(l+r)/2;
		if (jl[m]<=s){
			ans=m;
			l=m+1;
		}
		else r=m-1;
	}
	return ans;
}
int zd(int s){
	int l,r,m,ans;
	l=0;
	r=n;
	while (l<=r){
		m=(l+r)/2;
		if (jl[m]>=s){
			ans=m;
			r=m-1;
		}
		else l=m+1;
	}
	return ans;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for (i=1;i<=n;i++){
		scanf("%d%d",&jl[i],&jz[i]);
		zdfz+=max(jz[i],0);
	}
	if (zdfz<k){
		printf("-1");
		return 0;
	}
	l=0;
	r=1000000000;
	while (l<=r){
		tc=0;
		mid=(l+r)/2;
		zj=max(1,m-mid);
		zy=m+mid;
		ans=0;
		f[0]=0;
		for (i=1;i<=n;i++)
			f[i]=-1;
		for (i=1;i<=n;i++){
			z=jl[i]-zy;
			y=jl[i]-zj;
			if (y<0) break;
			ll=zd(z);
			rr=zx(y);
			for (j=ll;j<=rr;j++)
				if (f[j]!=-1){
					f[i]=max(f[j]+jz[i],f[i]);
					ans=max(ans,f[i]);
					if (ans>=k){
						tc=1;
						break;
					}
				}	
			if (tc==1) break;
		}
		if (ans>=k){
			zans=mid;
			r=mid-1;
		}
		else l=mid+1;
	}
	printf("%d",zans);
	return 0;
}

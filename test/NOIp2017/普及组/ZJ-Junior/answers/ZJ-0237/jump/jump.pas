var
  n,d,k,i,sum,o,g,j,p,q:longint;
  a,s,x:array[0..500000] of longint;
  t:boolean;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  sum:=0;
  for i:=1 to n do begin
    readln(x[i],s[i]);
    sum:=sum+s[i];
  end;
  g:=-1;
  while (o<k) and (q<=x[n]) do begin
    o:=0;
    p:=d;
    fillchar(a,sizeof(a),0);
    g:=g+1;
    q:=p+g;
    if p-g<1 then p:=1
             else p:=p-g;
    for i:=0 to n do begin
      t:=false;
      for j:=i+1 to n do
        if (x[j]<=x[i]+q) and (x[j]>=x[i]+p) then begin
                                                    t:=true;
                                                    if a[i]+s[j]>a[j] then begin
                                                                             a[j]:=a[i]+s[j];
                                                                             if a[j]>o then o:=a[j];
                                                                           end;
                                                  end;
      if not t then break;
    end;
  end;
  if q>x[n] then writeln(-1)
            else writeln(g);
  close(input);close(output);
end.
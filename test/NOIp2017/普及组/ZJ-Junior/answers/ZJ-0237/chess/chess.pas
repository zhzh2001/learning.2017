var
  m,n,i,j,x,y,c:longint;
  a,f:array[-5..105,-5..105] of longint;
  t:array[-5..105,-5..105] of boolean;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  fillchar(t,sizeof(t),false);
  fillchar(a,sizeof(a),0);
  for i:=1 to n do begin
    readln(x,y,c);
    if c=0 then c:=2;
    a[x,y]:=c;
    t[x,y]:=true;
  end;
  for i:=-1 to m do
    for j:=-1 to m do
      f[i,j]:=10000000;
  f[1,1]:=0;
  for i:=1 to m do
    for j:=1 to m do begin
      if (t[i-1,j]) and (t[i,j]) and (f[i-1,j]+abs(a[i-1,j]-a[i,j])<f[i,j]) then f[i,j]:=f[i-1,j]+abs(a[i-1,j]-a[i,j]);
      if (t[i,j-1]) and (t[i,j]) and (f[i,j-1]+abs(a[i,j-1]-a[i,j])<f[i,j]) then f[i,j]:=f[i,j-1]+abs(a[i,j-1]-a[i,j]);
      if (t[i+1,j]) and (t[i,j]) and (f[i+1,j]+abs(a[i+1,j]-a[i,j])<f[i,j]) then f[i,j]:=f[i+1,j]+abs(a[i+1,j]-a[i,j]);
      if (t[i,j+1]) and (t[i,j]) and (f[i,j+1]+abs(a[i,j+1]-a[i,j])<f[i,j]) then f[i,j]:=f[i,j+1]+abs(a[i,j+1]-a[i,j]);
      if (t[i-1,j]) and (not t[i,j]) and (f[i-1,j]+2<f[i,j]) then begin
        f[i,j]:=f[i-1,j]+2;
        if (t[i+1,j]) and (f[i,j]+abs(a[i+1,j]-a[i-1,j])<f[i+1,j]) then f[i+1,j]:=f[i,j]+abs(a[i+1,j]-a[i-1,j]);
        if (t[i,j-1]) and (f[i,j]+abs(a[i,j-1]-a[i-1,j])<f[i,j-1]) then f[i,j-1]:=f[i,j]+abs(a[i,j-1]-a[i-1,j]);
        if (t[i,j+1]) and (f[i,j]+abs(a[i,j+1]-a[i-1,j])<f[i,j+1]) then f[i,j+1]:=f[i,j]+abs(a[i,j+1]-a[i-1,j]);
      end;
      if (t[i+1,j]) and (not t[i,j]) and (f[i+1,j]+2<f[i,j]) then begin
        f[i,j]:=f[i+1,j]+2;
        if (t[i-1,j]) and (f[i,j]+abs(a[i-1,j]-a[i+1,j])<f[i-1,j]) then f[i-1,j]:=f[i,j]+abs(a[i-1,j]-a[i+1,j]);
        if (t[i,j-1]) and (f[i,j]+abs(a[i,j-1]-a[i+1,j])<f[i,j-1]) then f[i,j-1]:=f[i,j]+abs(a[i,j-1]-a[i+1,j]);
        if (t[i,j+1]) and (f[i,j]+abs(a[i,j+1]-a[i+1,j])<f[i,j+1]) then f[i,j+1]:=f[i,j]+abs(a[i,j+1]-a[i+1,j]);
      end;
      if (t[i,j-1]) and (not t[i,j]) and (f[i,j-1]+2<f[i,j]) then begin
        f[i,j]:=f[i,j-1]+2;
        if (t[i+1,j]) and (f[i,j]+abs(a[i+1,j]-a[i,j-1])<f[i+1,j]) then f[i+1,j]:=f[i,j]+abs(a[i+1,j]-a[i,j-1]);
        if (t[i-1,j]) and (f[i,j]+abs(a[i-1,j]-a[i,j-1])<f[i-1,j]) then f[i-1,j]:=f[i,j]+abs(a[i-1,j]-a[i,j-1]);
        if (t[i,j+1]) and (f[i,j]+abs(a[i,j+1]-a[i,j-1])<f[i,j+1]) then f[i,j+1]:=f[i,j]+abs(a[i,j+1]-a[i,j-1]);
      end;
      if (t[i,j+1]) and (not t[i,j]) and (f[i,j+1]+2<f[i,j]) then begin
        f[i,j]:=f[i,j+1]+2;
        if (t[i+1,j]) and (f[i,j]+abs(a[i+1,j]-a[i,j+1])<f[i+1,j]) then f[i+1,j]:=f[i,j]+abs(a[i+1,j]-a[i,j+1]);
        if (t[i-1,j]) and (f[i,j]+abs(a[i-1,j]-a[i,j+1])<f[i-1,j]) then f[i-1,j]:=f[i,j]+abs(a[i-1,j]-a[i,j+1]);
        if (t[i,j-1]) and (f[i,j]+abs(a[i,j-1]-a[i,j+1])<f[i,j-1]) then f[i,j-1]:=f[i,j]+abs(a[i,j-1]-a[i,j+1]);
      end;
    end;
  if f[m,m]=10000000 then writeln(-1)
                     else writeln(f[m,m]);
  close(input);close(output);
end.
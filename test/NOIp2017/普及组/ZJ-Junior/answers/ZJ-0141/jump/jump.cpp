#include<cstdio>
#include<cstring>
using namespace std;
inline void read(int &x)
{
	x=0; char ch=getchar(); int flag=1;
	while (ch<'0'||ch>'9') { if (ch=='-') flag=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=flag;
}
const int N=500005;
struct data
{
	int x,w;
}a[N];
int n,d,k,i,j,ans=-1,sum=0,f[N];
inline int max(int a,int b) { return a>b?a:b; }
bool check(int x)
{
	int left=x<d?d-x:1,right=d+x;
	for (i=1;i<=n;++i)
	f[i]=-1e9;
	for (i=1;i<=n;++i)
	for (j=0;j<i;++j)
	if (a[i].x-a[j].x>=left&&a[i].x-a[j].x<=right) 
	{
		f[i]=max(f[i],f[j]+a[i].w);
		if (f[i]>=k) return 1;
	}
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin); freopen("jump.out","w",stdout);
	read(n); read(d); read(k);	
	for (i=1;i<=n;++i)
	read(a[i].x),read(a[i].w),sum+=a[i].w>0?a[i].w:0;
	if (sum<k) { puts("-1"); return 0; }
	if (d==1)
	{
		sum=0;
		for (i=1;i<=n;++i)
		{
			sum+=a[i].w;
			if (sum>=k) { puts("0"); return 0; }
		}
	}
	a[0].x=a[0].w=0;
	int l=0,r=a[n].x;
	while (l<=r)
	{
		int mid=(l+r)>>1;
		if (check(mid)) ans=mid,r=mid-1; else l=mid+1;
	}
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
inline void read(int &x)
{
	x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}
const int N=1005;
int n,q,i,j,x,s[N],w;
inline int Pow_10(int k)
{
	int tot=1;
	for (int i=1;i<=k;++i)
	tot*=10;
	return tot;
}
int main()
{
	freopen("librarian.in","r",stdin); freopen("librarian.out","w",stdout);
	read(n); read(q);
	for (i=1;i<=n;++i)
	read(s[i]);
	sort(s+1,s+n+1);
	for (i=1;i<=q;++i)
	{
		bool flag=0;
		read(w); read(x);
		for (j=1;j<=n;++j)
		if (s[j]%Pow_10(w)==x) { printf("%d\n",s[j]); flag=1; break; }
		if (!flag) puts("-1");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;
inline void read(int &x)
{
	x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}
const int M=105,fx[4]={0,1,0,-1},fy[4]={1,0,-1,0},INF=1e9;
int map[M][M],n,m,i,j,x,y,z,ans=1e9,f[M][M][3],vis[M][M];
inline int min(int a,int b) { return a<b?a:b; }
void dfs(int x,int y)
{
	bool flag=0;
	if (map[x][y]==0) flag=1;
	for (int i=0;i<4;++i)
	{
		int xx=fx[i]+x,yy=fy[i]+y;
		if (xx>0&&xx<=m&&yy>0&&yy<=m&&vis[xx][yy])
		{
			if (map[xx][yy])
			{
				if (f[xx][yy][0]!=INF) 
				{
					if (flag) 
					{
						int add;
						if (map[xx][yy]==1) add=0; else add=1;
						f[x][y][1]=min(f[x][y][1],f[xx][yy][0]+add+2);
						if (map[xx][yy]==2) add=0; else add=1;
						f[x][y][2]=min(f[x][y][2],f[xx][yy][0]+add+2); 
					} else
					{
						int add;
						if (map[x][y]==map[xx][yy]) add=0; else add=1;
						f[x][y][0]=min(f[x][y][0],f[xx][yy][0]+add);
					}
				} else
				{
					vis[xx][yy]=0;
					dfs(xx,yy);
					vis[xx][yy]=1;
					if (flag) 
					{
						int add;
						if (map[xx][yy]==1) add=0; else add=1;
						f[x][y][1]=min(f[x][y][1],f[xx][yy][0]+add+2);
						if (map[xx][yy]==2) add=0; else add=1;
						f[x][y][2]=min(f[x][y][2],f[xx][yy][0]+add+2); 
					} else
					{
						int add;
						if (map[x][y]==map[xx][yy]) add=0; else add=1;
						f[x][y][0]=min(f[x][y][0],f[xx][yy][0]+add);
					}
				}
			} else
			{
				if (f[xx][yy][1]!=INF&&f[xx][yy][2]!=INF)
				{
					if (!flag) 
					{
						int add;
						if (map[x][y]==1) add=0; else add=1;
						f[x][y][0]=min(f[x][y][0],f[xx][yy][1]+add);
						if (map[x][y]==2) add=0; else add=1;
						f[x][y][0]=min(f[x][y][0],f[xx][yy][2]+add);
					} 
				} else 
				{ 
					if (!flag)
					{
						vis[xx][yy]=0; 
						dfs(xx,yy); 
						vis[xx][yy]=1; 
						int add;
						if (map[x][y]==1) add=0; else add=1;
						f[x][y][0]=min(f[x][y][0],f[xx][yy][1]+add);
						if (map[x][y]==2) add=0; else add=1;
						f[x][y][0]=min(f[x][y][0],f[xx][yy][2]+add);
					}
				}
			}
		}
	}
}
void xz()
{
	for (int i=1;i<=m;++i)
	for (int j=1;j<=m;++j)
	{
		if (map[i][j]) 
		{
			for (int k=0;k<4;++k)
			{
				int x=fx[k]+i,y=fy[k]+j;
				if (x>0&&x<=m&&y>0&&y<=m) 
				{
					if (map[x][y]) 
					{
						int add;
						if (map[x][y]==map[i][j]) add=0; else add=1;
						f[i][j][0]=min(f[i][j][0],f[x][y][0]+add);
					} else
					{
						int add;
						if (map[i][j]==1) add=0; else add=1;
						f[i][j][0]=min(f[i][j][0],f[x][y][1]+add);
						if (map[i][j]==2) add=0; else add=1;
						f[i][j][0]=min(f[i][j][0],f[x][y][2]+add);
					}
				}
			}
		} else
		{
			for (int k=0;k<4;++k)
			{
				int x=fx[k]+i,y=fy[k]+j;
				if (x>0&&x<=m&&y>0&&y<=m)
				{
					if (map[x][y])
					{
						int add;
						if (map[x][y]==1) add=0; else add=1;
						f[i][j][1]=min(f[i][j][1],f[x][y][0]+add+2);
						if (map[x][y]==2) add=0; else add=1;
						f[i][j][2]=min(f[i][j][2],f[x][y][0]+add+2);
					}
				}
			}
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin); freopen("chess.out","w",stdout);
	memset(map,0,sizeof(map));
	memset(vis,true,sizeof(vis));
	read(m); read(n);
	for (i=1;i<=m;++i)
	for (j=1;j<=m;++j)
	f[i][j][0]=f[i][j][1]=f[i][j][2]=INF;
	f[1][1][0]=0;
	for (i=1;i<=n;++i)
	{
		read(x); read(y); read(z);
		map[x][y]=z+1;
	}
	vis[m][m]=0;
	dfs(m,m);
	xz();
	bool flag=0;
	if (map[m][m]==0) flag=1;
	if (flag) { if (min(f[m][m][1],f[m][m][2])==INF) puts("-1"); else printf("%d",min(f[m][m][1],f[m][m][2])); }
	else { if (f[m][m][0]==INF) puts("-1"); else printf("%d",f[m][m][0]); }
	return 0;
}

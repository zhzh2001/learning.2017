#include<cstdio>
#include<cctype>
#include<algorithm>
const int maxn=1005;
int n,Q,bok[maxn],ten[15];
inline int read_i(){
	int ret=0;bool fh=0;char ch=getchar();
	while(!isdigit(ch)) fh^=!(ch^45),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	return fh?-ret:ret;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	ten[0]=1;for(int i=1;i<=9;++i) ten[i]=ten[i-1]*10;
	n=read_i(),Q=read_i();
	for(int i=1;i<=n;++i) bok[i]=read_i();
	std::sort(bok+1,bok+1+n);
	for(int i=1;i<=Q;++i){
		int y=read_i(),x=read_i();bool flg=0;
		for(int j=1;j<=n;++j)
		  if(bok[j]>=x&&bok[j]%ten[y]==x){printf("%d\n",bok[j]);flg=1;break;}
		if(!flg) printf("-1\n");
	}
	return 0;
}

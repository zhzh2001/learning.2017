#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
const int maxn=105,go[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,mp[maxn][maxn],f[maxn][maxn][3],INF,ans;bool vis[maxn][maxn];
inline int read_i(){
	int ret=0;bool fh=0;char ch=getchar();
	while(!isdigit(ch)) fh^=!(ch^45),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	return fh?-ret:ret;
}
void DFS(int x,int y,int col,int sum){
	if(x==n&&y==n){
		if(~ans){if(sum<ans)ans=sum;}else ans=sum;
		return;
	}
	vis[x][y]=1;
	for(int i=0;i<4;++i){
		int now_x=x+go[i][0],now_y=y+go[i][1];
		if(now_x>=1&&now_x<=n&&now_y>=1&&now_y<=n&&!vis[now_x][now_y]){
			if(mp[x][y]&&!mp[now_x][now_y]) DFS(now_x,now_y,col,sum+2),DFS(now_x,now_y,col==1?2:1,sum+3);
			if(mp[now_x][now_y]) DFS(now_x,now_y,mp[now_x][now_y],sum+(mp[now_x][now_y]!=col));
		}
	}
	vis[x][y]=0;
}
int DFS1(int x,int y,int col){
	if(f[x][y][col]<INF) return f[x][y][col];
	vis[x][y]=1;
	int &ret=f[x][y][col];
	for(int i=0;i<4;++i){
		int now_x=x+go[i][0],now_y=y+go[i][1];
		if(now_x>=1&&now_x<=n&&now_y>=1&&now_y<=n&&!vis[now_x][now_y]){
			if(mp[x][y]&&!mp[now_x][now_y]) ret=std::min(ret,std::min(DFS1(now_x,now_y,col)+2,DFS1(now_x,now_y,col==1?2:1)+3));
			if(mp[now_x][now_y]) ret=std::min(ret,DFS1(now_x,now_y,mp[now_x][now_y])+(mp[now_x][now_y]!=col));
		}
	}
	vis[x][y]=0;
	return ret;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read_i(),m=read_i();
	for(int i=1,x,y;i<=m;++i) x=read_i(),y=read_i(),mp[x][y]=read_i()+1;
	if(n<=7) DFS(1,1,mp[1][1],0);
	else{
		memset(f,63,sizeof(f));INF=f[0][0][0];f[1][1][mp[1][1]]=0;
		if(mp[n][n]) ans=DFS1(n,n,mp[n][n]);
		else ans=std::min(DFS1(n,n,1),DFS1(n,n,2))+2;
	}
	printf("%d\n",ans);
	return 0;
}
/*
#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
const int maxn=105,maxm=32767,go[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,mp[maxn][maxn],dis[maxn][maxn][3],INF,ans;bool vis[maxn][maxn];
int hed,tai;struct Que{int x,y,co;}que[maxm+1];
inline int read_i(){
	int ret=0;bool fh=0;char ch=getchar();
	while(!isdigit(ch)) fh^=!(ch^45),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	return fh?-ret:ret;
}
inline void SPFA(int sx,int sy,int sco){
	memset(dis,63,sizeof(dis));INF=dis[0][0][0],dis[sx][sy][sco]=0;
	hed=0,tai=1,que[1]=(Que){sx,sy,sco},vis[sx][sy]=1;
	while(hed^tai){
		++hed&=maxm;vis[que[hed].x][que[hed].y]=0;
		if(mp[que[hed].x][que[hed].y]){
			for(int k=0;k<4;++k){
				int now_x=que[hed].x+go[k][0],now_y=que[hed].y+go[k][1],now_co,now_dis;
				if(now_x>=1&&now_x<=n&&now_y>=1&&now_y<=n){
					if(mp[now_x][now_y]){
						now_co=mp[now_x][now_y],now_dis=dis[que[hed].x][que[hed].y][que[hed].co]+(que[hed].co!=now_co);
						if(now_dis<dis[now_x][now_y][now_co]){
							dis[now_x][now_y][now_co]=now_dis;
							if(!vis[now_x][now_y]){
								que[++tai&=maxm]=(Que){now_x,now_y,now_co};
								vis[now_x][now_y]=1;
							}
						}
					}
					else{
						now_co=1,now_dis=dis[que[hed].x][que[hed].y][que[hed].co]+2+(que[hed].co!=now_co);
						if(now_dis<dis[now_x][now_y][now_co]){
							dis[now_x][now_y][now_co]=now_dis;
							if(!vis[now_x][now_y]){
								que[++tai&=maxm]=(Que){now_x,now_y,now_co};
								vis[now_x][now_y]=1;
							}
						}
						now_co=2,now_dis=dis[que[hed].x][que[hed].y][que[hed].co]+2+(que[hed].co!=now_co);
						if(now_dis<dis[now_x][now_y][now_co]){
							dis[now_x][now_y][now_co]=now_dis;
							if(!vis[now_x][now_y]){
								que[++tai&=maxm]=(Que){now_x,now_y,now_co};
								vis[now_x][now_y]=1;
							}
						}
					}
				}
			}
		}
		else{
			for(int k=0;k<4;++k){
				int now_x=que[hed].x+go[k][0],now_y=que[hed].y+go[k][1],now_co,now_dis;
				if(now_x>=1&&now_x<=n&&now_y>=1&&now_y<=n){
					if(mp[now_x][now_y]){
						now_co=mp[now_x][now_y],now_dis=dis[que[hed].x][que[hed].y][que[hed].co]+(que[hed].co!=now_co);
						if(now_dis<dis[now_x][now_y][now_co]){
							dis[now_x][now_y][now_co]=now_dis;
							if(!vis[now_x][now_y]){
								que[++tai&=maxm]=(Que){now_x,now_y,now_co};
								vis[now_x][now_y]=1;
							}
						}
					}
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read_i(),m=read_i();
	for(int i=1,x,y;i<=m;++i) x=read_i(),y=read_i(),mp[x][y]=read_i()+1;
	SPFA(1,1,mp[1][1]);
	ans=std::min(dis[n][n][1],dis[n][n][2]);
	printf("%d\n",ans==INF?-1:ans);
	return 0;
}
*/

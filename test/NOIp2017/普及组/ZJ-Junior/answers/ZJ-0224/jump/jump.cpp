#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
const int maxn=500005;
int n,d,K,f[maxn],ans=-1;
int hed,tai,que[maxn];
struct dat{int x,s;dat(){x=s=0;}}a[maxn];
inline int read_i(){
	int ret=0;bool fh=0;char ch=getchar();
	while(!isdigit(ch)) fh^=!(ch^45),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	return fh?-ret:ret;
}
inline bool check1(int x){
	memset(f,192,sizeof(f));f[0]=0;
	hed=0,tai=0;memset(que,0,sizeof(que));
	for(int i=1;i<=n;++i){
		while(hed<=tai&&(a[i].x-a[que[hed]].x>d+x||a[i].x-a[que[hed]].x<d-x)) hed++;
		if(hed<=tai) f[i]=f[que[hed]];
		f[i]+=a[i].s;
		if(f[i]>=K) return 1;
		while(tai>=hed&&f[que[tai]]<f[i]) tai--;
		que[++tai]=i;
	}
	return 0;
}
inline bool check2(int x){
	memset(f,192,sizeof(f));f[0]=0;
	for(int i=1;i<=n;++i){
		for(int j=i-1;~j;--j)
		  if(a[i].x-a[j].x<=d+x&&a[i].x-a[j].x>=d-x){if(f[i]<f[j])f[i]=f[j];}
		  else break;
		f[i]+=a[i].s;
		if(f[i]>=K)return 1;
	}
	return 0;
}
inline bool check3(int x){
	memset(f,192,sizeof(f));f[0]=0;
	hed=0,tai=0;memset(que,0,sizeof(que));
	for(int i=1;i<=n;++i){
		while(hed<=tai&&a[i].x-a[que[hed]].x>d+x) hed++;
		for(int j=hed;j<=tai;++j)
		  if(a[i].x-a[que[j]].x>=d-x){if(f[i]<f[que[j]])f[i]=f[que[j]];}
		  else break;
		f[i]+=a[i].s;
		if(f[i]>=K) return 1;
		while(tai>=hed&&f[que[tai]]<f[i]) tai--;
		que[++tai]=i;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read_i(),d=read_i(),K=read_i();
	for(int i=1;i<=n;++i) a[i].x=read_i(),a[i].s=read_i();
	int L=0,R=a[n].x,mid;
	if(d==1) while(L<=R) check1(mid=(R-L>>1)+L)?R=(ans=mid)-1:L=mid+1;
	else if(n<=500) while(L<=R) check2(mid=(R-L>>1)+L)?R=(ans=mid)-1:L=mid+1;
	else while(L<=R) check3(mid=(R-L>>1)+L)?R=(ans=mid)-1:L=mid+1;
	printf("%d\n",ans);
	return 0;
}

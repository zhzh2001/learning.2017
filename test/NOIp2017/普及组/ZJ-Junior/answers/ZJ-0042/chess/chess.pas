var
  n,m,i,j,k,x,y,z,xx,yy:longint;
  a,f,pd:array[0..105,0..105] of longint;
  flag:longint;
begin
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);
  readln(n,m);
  for i:=0 to n+1 do
    for j:=0 to n+1 do
      begin
        f[i,j]:=maxlongint div 2;
        a[i,j]:=2;
      end;
  for i:=1 to m do
    begin
      readln(x,y,z);
      a[x,y]:=z;
    end;
  f[1,1]:=0;
  for i:=1 to n do
    for j:=1 to n do
      begin
        xx:=abs(a[i-1,j]-a[i,j]);
        if (a[i-1,j]=1)and(a[i,j]=2)
          then inc(xx);
        yy:=abs(a[i,j-1]-a[i,j]);
        if (a[i,j-1]=1)and(a[i,j]=2)
          then inc(yy);

        if not ((xx=2)and(pd[i-1,j]=1))
          then begin
                 if f[i-1,j]+xx<f[i,j]
                   then begin
                          f[i,j]:=f[i-1,j]+xx;
                          if xx=2 then pd[i,j]:=1
                                  else pd[i,j]:=0;
                          if xx=2 then a[i,j]:=a[i-1,j];
                        end;
               end;
        if not ((yy=2)and(pd[i,j-1]=1))
          then begin
                  if f[i,j-1]+yy<f[i,j]
                    then begin
                           f[i,j]:=f[i,j-1]+yy;
                           if yy=2 then pd[i,j]:=1
                                   else pd[i,j]:=0;
                           if yy=2 then a[i,j]:=a[i,j-1];
                         end;
               end;
      end;

  if f[n,n]<>maxlongint div 2
    then writeln(f[n,n])
    else writeln(-1);
close(input);
close(output);
end.
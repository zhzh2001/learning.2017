#include<cstdio>
#include<algorithm>
using namespace std;
int n,Q,a[1005],ten[15],len,x;
int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int find(){
    for(int i=1;i<=n;i++) if(a[i]%ten[len]==x) return a[i];
	return -1;	
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),Q=read();
	for(int i=1;i<=n;i++) a[i]=read();sort(a+1,a+1+n);
	ten[0]=1;for(int i=1;i<=7;i++) ten[i]=ten[i-1]*10;
	while(Q--){
		len=read(),x=read();
		printf("%d\n",find());
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int p[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,T,mp[105][105],dst[105][105][3][3];bool vis[105][105][3][3];//λ�á�ħ������ɫ 
struct ff{
	int x,y,c;bool flg;
}que[40005];
int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
bool check(int x,int y){return !(x<1||x>n||y<1||y>n);}
int SPFA(){
	memset(dst,63,sizeof dst);
	int INF=dst[0][0][0][0],hed=0,tal=1,x,y,c,x_,y_,c_;bool flg;
	vis[1][1][0][mp[1][1]]=1,dst[1][1][0][mp[1][1]]=0,que[1]=(ff){1,1,mp[1][1],0};
	while(hed!=tal){
		++hed%=40005;
		x=que[hed].x,y=que[hed].y,c=que[hed].c,flg=que[hed].flg;
		vis[x][y][flg][c]=0;
		for(int i=0;i<4;i++){
			x_=x+p[i][0],y_=y+p[i][1];
			if(check(x_,y_)){
				if(mp[x_][y_]!=-1){
				   c_=mp[x_][y_];
				   if(c_==c&&dst[x][y][flg][c]<dst[x_][y_][0][c]){
					   dst[x_][y_][0][c]=dst[x][y][flg][c];
					   if(!vis[x_][y_][0][c]) vis[x_][y_][0][c]=1,que[++tal%=40005]=(ff){x_,y_,c_,0};
				   }
				   if(c_!=c&&dst[x][y][flg][c]+1<dst[x_][y_][0][c_]){
					   dst[x_][y_][0][c_]=dst[x][y][flg][c]+1;
					   if(!vis[x_][y_][0][c_]) vis[x_][y_][0][c_]=1,que[++tal%=40005]=(ff){x_,y_,c_,0};
				   }
				}else if(!flg&&dst[x][y][flg][c]+2<dst[x_][y_][1][c]){
					dst[x_][y_][1][c]=dst[x][y][flg][c]+2;
					if(!vis[x_][y_][1][c]) vis[x_][y_][1][c]=1,que[++tal%=40005]=(ff){x_,y_,c,1};
				}
			}
		}
	}
    dst[n][n][0][0]=min(min(dst[n][n][0][0],dst[n][n][0][1]),min(dst[n][n][1][0],dst[n][n][1][1]));
    if(dst[n][n][0][0]<INF) return dst[n][n][0][0];
	return -1;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read(),T=read();
	memset(mp,255,sizeof mp);int x,y;
	while(T--){
		x=read(),y=read();
		mp[x][y]=read();
	}
	printf("%d\n",SPFA());
	return 0;
}

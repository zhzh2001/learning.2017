#include<cstdio>
#include<cstring>
using namespace std;
int n,d,k,f[500005],hed,tal,que[500005];
struct ff{
	int x,p;
}a[500005];
int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
bool check(int x){
	memset(f,192,sizeof f);
	int INF=f[0];hed=1,tal=f[0]=0;
	int L=d-x>1?d-x:1,R=d+x;
	for(int i=1,j=0;i<=n;i++){
		while(a[i].x-a[j].x>R) j++;
		while(L<=a[i].x-a[j].x&&a[i].x-a[j].x<=R){
			while(hed<=tal&&f[que[tal]]<=f[j]) tal--;
			if(f[j]!=INF) que[++tal]=j;
			j++;
		}
		while(hed<=tal&&(a[i].x-a[que[hed]].x<L||a[i].x-a[que[hed]].x>R)) hed++;
		if(hed<=tal){
			f[i]=f[que[hed]]+a[i].p;
			if(f[i]>=k) return 1;
		}
	}
    return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for(int i=1;i<=n;i++) a[i]=(ff){read(),read()};a[0].x=0;
	int L=0,R=a[n].x,mid;
	while(L<=R){
		mid=(R-L>>1)+L;
		if(check(mid)) R=mid-1;else L=mid+1;
	}
	if(L<=a[n].x) printf("%d\n",L);else printf("-1\n");
	return 0;
}

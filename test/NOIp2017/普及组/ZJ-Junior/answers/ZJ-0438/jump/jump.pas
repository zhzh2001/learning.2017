var
    d,n,i,kk,l,r,mid:longint;
    x,s,f:array[0..600000]of longint;
function min(a,b:longint):longint;
    begin
        if a<b then exit(a);
        exit(b);
    end;
function max(a,b:longint):longint;
    begin
        if a>b then exit(a);
        exit(b);
    end;
function check(g:longint):boolean;
    var
        i,j,k,maxx:longint;
    begin
        k:=0;
        f[0]:=0;
        for i:=1 to n do begin
            f[i]:=-maxlongint div 2;
            j:=k;
            while (x[j]<max(x[i]-d-g,0))and(j<i) do inc(j);
            if(j=i) then exit(false);
            k:=j;
            while x[j]<=min(x[i]-1,x[i]-d+g) do begin
              if f[j]+s[i]>=kk then exit(true);
               f[i]:=max(f[i],f[j]+s[i]);
               inc(j);
            end;
        end;
        maxx:=-maxlongint div 2;
        for i:=0 to n do
            maxx:=max(f[i],maxx);
    exit(maxx>=kk);
    end;
begin
    assign(input,'jump.in');reset(input);
    assign(output,'jump.out');rewrite(output);
    x[0]:=0;
    s[0]:=0;
    read(n,d,kk);
    for i:=1 to n do
        read(x[i],s[i]);
    l:=0; r:=d;
    while l<r do begin
        mid:=(l+r)div 2;
        if check(mid) then r:=mid
                      else l:=mid+1;
    end;
    if r=d then write('-1')
           else write(r);
    close(input);close(output);
end.

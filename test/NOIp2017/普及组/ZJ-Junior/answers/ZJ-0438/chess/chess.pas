var
    dx:array[1..4]of longint=(0,1,0,-1);
    dy:array[1..4]of longint=(1,0,-1,0);
    a,f:array[1..200,1..200]of longint;
    kind:array[1..200,1..200,0..1]of longint;
    lx,ly:array[1..40000]of longint;
    flag:array[1..200,1..200]of boolean;
    i,j,n,m,h,t,x,y,z,k:longint;
    b:boolean;
function min(a,b:longint):longint;
    begin
        if a<b then exit(a);
        exit(b);
    end;
begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
    read(n,m);
    for i:=1 to n do
        for j:=1 to n do begin
            a[i,j]:=-1;
            flag[i,j]:=false;
            f[i,j]:=maxlongint div 2;
            kind[i,j,1]:=maxlongint div 2;
            kind[i,j,0]:=maxlongint div 2;
        end;
    for i:=1 to m do begin
        read(x,y,z);
        a[x,y]:=z;
    end;
    f[1,1]:=0;
    lx[1]:=1;
    ly[1]:=1;
    h:=1; t:=2;
    flag[1,1]:=true;
    while h<t do begin
        for i:=1 to 4 do begin
            if (lx[h]+dx[i]>0)and(lx[h]+dx[i]<=n)and(ly[h]+dy[i]>0)and(ly[h]+dy[i]<=n)
                then begin
                        b:=false;
                         x:=lx[h]+dx[i];
                         y:=ly[h]+dy[i];
                         if (a[x,y]<>-1)and(a[x,y]=a[lx[h],ly[h]])
                             then begin if f[x,y]>f[lx[h],ly[h]]
                                      then begin
                                               f[x,y]:=f[lx[h],ly[h]];
                                               b:=true;
                                           end  end
                   else if (a[x,y]<>-1)and(a[lx[h],ly[h]]<>-1)and(a[x,y]<>a[lx[h],ly[h]])
                            then begin if f[x,y]>f[lx[h],ly[h]]+1
                                     then begin
                                              f[x,y]:=f[lx[h],ly[h]]+1;
                                              b:=true;
                                          end  end
                  else if (a[x,y]=-1)and(a[lx[h],ly[h]]<>-1)
                           then begin
                                    k:=a[lx[h],ly[h]];
                                    if kind[x,y,k]>f[lx[h],ly[h]]+2
                                        then begin
                                                 kind[x,y,k]:=f[lx[h],ly[h]]+2;
                                                 b:=true;
                                             end;
                                    k:=abs(k-1);
                                    if kind[x,y,k]>f[lx[h],ly[h]]+3
                                        then begin
                                                kind[x,y,k]:=f[lx[h],ly[h]]+3;
                                                b:=true;
                                             end;
                                end
                 else if (a[lx[h],ly[h]]=-1)and(a[x,y]<>-1)
                         then begin
                                  k:=a[x,y];
                                  if f[x,y]>min(kind[lx[h],ly[h],k],kind[lx[h],ly[h],abs(k-1)]+1)
                                      then begin
                                               f[x,y]:=min(kind[lx[h],ly[h],k],kind[lx[h],ly[h],abs(k-1)]+1);
                                               b:=true;
                                           end;
                              end;
              if (b)and(not flag[x,y])
                  then begin
                           lx[t]:=x;
                           ly[t]:=y;
                           flag[x,y]:=true;
                           inc(t);
                      end;
              end;
            end;
        flag[lx[h],ly[h]]:=false;
        inc(h);
    end;
    if ((a[n,n]=-1)and(min(kind[n,n,0],kind[n,n,1])=maxlongint div 2))or((a[n,n]<>-1)and(f[n,n]=maxlongint div 2))
        then write('-1')
else if (a[n,n]=-1) then write(min(kind[n,n,0],kind[n,n,1]))
else write(f[n,n]);
    close(input);close(output);
end.
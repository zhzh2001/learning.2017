var
    n,i,p,l,j,k:longint;
    a:array[1..2000]of longint;
    b:boolean;
function change(x:longint):longint;
    var
        i,s:longint;
    begin
        s:=1;
        for i:=1 to x do
            s:=s*10;
        exit(s);
    end;
procedure sort(l,r:longint);
    var
        i,j,k,t:longint;
    begin
        i:=l;
        j:=r;
        k:=a[(i+j)div 2];
        repeat
            while a[i]<k do inc(i);
            while a[j]>k do dec(j);
            if i<=j
                then begin
                         t:=a[i];
                         a[i]:=a[j];
                         a[j]:=t;
                         inc(i);
                         dec(j);
                     end;
        until i>j;
        if i<r then sort(i,r);
        if l<j then sort(l,j);
    end;
begin
    assign(input,'librarian.in');reset(input);
    assign(output,'librarian.out');rewrite(output);
    read(n,p);
    for i:=1 to n do
        read(a[i]);
    sort(1,n);
    for i:=1 to p do begin
        b:=false;
        read(l);
        l:=change(l);
        read(k);
        for j:=1 to n do
            if (a[j] mod l)=k
                then begin
                         b:=true;
                         break;
                     end;
        if b then writeln(a[j])
             else writeln('-1');
    end;
    close(input);close(output);
end.

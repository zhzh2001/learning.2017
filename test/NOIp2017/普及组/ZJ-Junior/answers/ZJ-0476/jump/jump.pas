var
  n,d,k:longint;
  i,j:longint;
  l,r,mid:longint;
  x,s:array[0..500001]of longint;
  sum:longint;
  f:array[0..100001]of longint;

function check(g:longint):boolean;
var
  i,j:longint;
  tot:longint;
  min,max:longint;
  ans:longint;
begin
  if g<d then min:=d-g
         else min:=1;
  max:=d+g;
  ans:=0;
  fillchar(f,sizeof(f),0);
  for i:=0 to n do f[i]:=-maxlongint div 3;
  f[0]:=0;
  for i:=1 to n do
  begin
    for j:=0 to i-1 do
      begin
        if (min<=abs(x[i]-x[j]))and(abs(x[i]-x[j])<=max) then
          if f[j]<>-maxlongint div 3 then
            if f[i]<f[j]+s[i] then f[i]:=f[j]+s[i];
      end;
    if f[i]>ans then ans:=f[i];
  end;

  //for i:=1 to n do  writeln(f[i]);
  //writeln(min,' ',max,' ',ans,' ',g);
  exit(ans>=d);
end;

begin

  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);


  readln(n,d,k);
  for i:=1 to n do
    readln(x[i],s[i]);
  for i:=1 to n do
    if s[i]>0 then sum:=sum+s[i];
  if sum<k then writeln(-1)
  else
  begin
    l:=0;r:=x[n]; //writeln(l,' ',r);
    while l<r do
      begin
        mid:=(l+r)div 2;
        if check(mid) then r:=mid
                      else l:=mid+1;
      end;
    writeln(l+1);
  end;

  close(input);
  close(output);
end.

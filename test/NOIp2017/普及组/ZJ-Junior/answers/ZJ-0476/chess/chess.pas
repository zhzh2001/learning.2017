var
  n,m,i,j,k:longint;
  x,y,c:longint;
  a:array[0..101,0..101]of longint;
  vis:array[0..101,0..101]of boolean;
  xx:array[0..100001]of longint;
  yy:array[0..100001]of longint;
  co:array[0..100001]of longint;
  dis:array[0..101,0..101]of longint;
  dx:array[1..4]of longint=(0,0,1,-1);
  dy:array[1..4]of longint=(1,-1,0,0);
  num:longint;
  head,tail:longint;
  xxx,yyy:longint;

begin

  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);


  readln(m,n);
  fillchar(a,sizeof(a),255);
  for i:=1 to n do
    begin
      readln(x,y,c);
      a[x][y]:=c;
    end;
  for i:=1 to m do
    for j:=1 to m do
      dis[i][j]:=maxlongint div 3;
  vis[1][1]:=true;
  xx[0]:=1;
  yy[0]:=1;
  co[0]:=a[1][1];
  dis[1][1]:=0;
  head:=0;
  tail:=0;
  num:=1;
  while num<>0 do
    begin
    //writeln(num);
      for i:=1 to 4 do
        begin
          //writeln(xxx,' ',yyy) ;
          xxx:=xx[head]+dx[i];
          yyy:=yy[head]+dy[i];
          if (xxx>=1)and(yyy>=1)and(xxx<=m)and(yyy<=m)and(not(vis[xxx][yyy])) then
            begin
              if (co[head]=1)or(co[head]=0) then
                begin
                  if a[xxx][yyy]=-1 then
                    begin
                      if dis[xxx][yyy]>dis[xx[head]][yy[head]]+2 then
                        begin
                          dis[xxx][yyy]:=dis[xx[head]][yy[head]]+2;
                          tail:=(tail+1)mod 100001;    inc(num);
                          xx[tail]:=xxx;
                          yy[tail]:=yyy;
                          if co[head]=1 then co[tail]:=3
                                        else co[tail]:=4;
                        end;
                    end
                                     else
                    begin
                    begin
                      //writeln(xxx,' ',yyy);
                      //writeln(a[xxx][yyy],' ',co[head]);
                      if a[xxx][yyy]=co[head] then
                        begin
                          //writeln(dis[xxx][yyy],' ',dis[xx[head]][yy[head]]);
                          if dis[xxx][yyy]>dis[xx[head]][yy[head]] then
                            begin
                              ///writeln(xxx,' ',yyy);
                              dis[xxx][yyy]:=dis[xx[head]][yy[head]];
                              tail:=(tail+1)mod 100001; inc(num);
                              xx[tail]:=xxx;
                              yy[tail]:=yyy;
                              co[tail]:=a[xxx][yyy];
                            end;
                        end
                                              else
                        begin
                          if dis[xxx][yyy]>dis[xx[head]][yy[head]]+1 then
                            begin
                              dis[xxx][yyy]:=dis[xx[head]][yy[head]]+1;
                              tail:=(tail+1)mod 100001;  inc(num);
                              xx[tail]:=xxx;
                              yy[tail]:=yyy;
                              co[tail]:=a[xxx][yyy];
                            end;
                        end;
                    end;
                    end;
                end
                                            else
                begin
                  if a[xxx][yyy]=-1 then continue;
                  if (co[head]mod 2<>a[xxx][yyy]mod 2) then
                    begin
                      if dis[xxx][yyy]>dis[xx[head]][yy[head]]+1 then
                        begin
                          dis[xxx][yyy]:=dis[xx[head]][yy[head]]+1;
                          tail:=(tail+1)mod 100001;  inc(num);
                          xx[tail]:=xxx;
                          yy[tail]:=yyy;
                          co[tail]:=a[xxx][yyy];
                        end;
                    end;
                  if (co[head]mod 2=a[xxx][yyy]mod 2) then
                    begin
                      if dis[xxx][yyy]>dis[xx[head]][yy[head]] then
                        begin
                          dis[xxx][yyy]:=dis[xx[head]][yy[head]];
                          tail:=(tail+1)mod 100001;  inc(num);
                          xx[tail]:=xxx;
                          yy[tail]:=yyy;
                          co[tail]:=a[xxx][yyy];
                        end;
                    end;
                end;
            end;
        end;
      head:=(head+1)mod 100001;
      dec(num);
    end;

  if dis[m][m]=maxlongint div 3 then writeln(-1)
                                else writeln(dis[m][m]);

  close(input);
  close(output);
end.


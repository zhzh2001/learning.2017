var
  n,q,i,j,k:longint;
  x:longint;
  t:ansistring;
  a:array[0..1001]of longint;
  b:array[0..1001]of ansistring;
  flag:boolean;
  c:char;

procedure sort(l,r: longint);
var
  i,j,x,y: longint;
  z:ansistring;
begin
  i:=l;j:=r;x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
      begin
        y:=a[i];
        a[i]:=a[j];
        a[j]:=y;
        z:=b[i];
        b[i]:=b[j];
        b[j]:=z;
        inc(i);dec(j);
      end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);

  readln(n,q);
  for i:=1 to n do
    begin
      readln(a[i]);
      str(a[i],b[i]);
    end;
  sort(1,n);
  for i:=1 to q do
    begin
      flag:=false;
      read(x);
      read(c);
      readln(t);
      for j:=1 to n do
        begin
          if x>length(b[j]) then continue;
          if t=copy(b[j],length(b[j])-x+1,x) then
            begin
              flag:=true;
              break;
            end;
        end;

      if flag then writeln(b[j])
              else writeln(-1);
    end;

  close(input);
  close(output);
end.

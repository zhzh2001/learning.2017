var
  a,b,c:longint;
  s:int64;
begin
  assign(input,'score.in'); reset(input);
  assign(output,'score.out'); rewrite(output);
  readln(a,b,c);
  inc(s,a div 5);
  inc(s,b div 10*3);
  inc(s,c div 2);
  write(s);
  close(input); close(output);
end.
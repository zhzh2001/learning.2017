#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
struct b{
	int dis,sc,dl;
}bls[500001];
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	int n,d,k,mx=0;
	cin>>n>>d>>k;
	for (int i=0;i<n;i++){
		scanf("%d%d",&bls[i].dis,&bls[i].sc);
		int df=bls[i].dis-bls[i].dis/d*d;
		int ds=(bls[i].dis/d+1)*d-bls[i].dis;
		bls[i].dl=min(df,ds);
		mx=max(mx,bls[i].dl);
	}
	int g=0,mx2=0x80000000,tot;
	bool flag;
	for (;g<=mx;g++){
		tot=0,flag=true;
		for (int i=0;i<n;i++){
			if (bls[i].dl<=g&&flag)
				tot+=bls[i].sc;
			else if (flag) flag=false;
			else break;
		}
		if (tot>=k) break;
	}
	if (tot>=k) cout<<g;
	else cout<<-1;
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
int s[101][1001],ans=-1,m,n;
bool vis[101][1001];
int dx[]={-1,0,0,1};
int dy[]={0,-1,1,0};
inline bool judge(int x,int y){
	return x<=m&&y<=m&&x>=1&&y>=1;
}
void search(int x,int y,int c,int pc){
	if (!judge(x,y)) return;
	if (x==m&&y==m){
		ans=c; return;
	}
	vis[x][y]=true;
	for (int i=0;i<4;i++){
		int d_x=x+dx[i],d_y=y+dy[i];
		if (judge(d_x,d_y)&&!vis[d_x][d_y]){
			if (s[x][y]!=0){
				if (s[x][y]==s[d_x][d_y]) search(d_x,d_y,c,s[x][y]);
				else{
					if (s[d_x][d_y]!=0) search(d_x,d_y,c+1,s[x][y]);
					else search(d_x,d_y,c+2,s[x][y]);
				}
			}
			else{
				if (s[d_x][d_y]!=0){
					if (pc==s[d_x][d_y]) search(d_x,d_y,c,pc);
					else search(d_x,d_y,c+1,pc);
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	for (int i=0,x,y,c;i<n;i++){
		scanf("%d%d%d",&x,&y,&c);
		s[x][y]=c+1;
	}
	int x=1,y=1,tot=0;
	search(1,1,0,0);
	cout<<ans;
	fclose(stdin);
	fclose(stdout);
	return 0;
}


var
  n,p,i,j,min,x,y,t:longint;
  a:array[1..1000] of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);

  readln(n,p);
  for i:=1 to n do
    read(a[i]);
  for i:=1 to p do
    begin
      read(x,y);
      t:=1;min:=maxlongint;
      for j:=1 to x do
        t:=t*10;
      for j:=1 to n do
        if (a[j] mod t=y)and(a[j]<min) then min:=a[j];
      if min=maxlongint then writeln(-1)
                        else writeln(min);
    end;

  close(input);
  close(output);
end.
var
  n,d,k,i,j,l,r,m,min,max,ans,fmax,szy,last:longint;
  x,s,f:array[0..500000] of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);

  readln(n,d,k);max:=0;
  for i:=1 to n do
    begin
      readln(x[i],s[i]);
      if s[i]>0 then max:=max+s[i];
    end;
  if max<k then begin
                  write(-1);
                  close(input);
                  close(output);
                  exit;
                end;
  l:=0;r:=x[n]-d;x[0]:=0;f[0]:=0;
  while l<r do
    begin
      m:=(l+r)div 2;
      if m<d then min:=d-m
             else min:=1;
      max:=m+d;fmax:=0;
      for i:=1 to n do
        if f[i-1]<k
          then begin
                 last:=-1;
                 for j:=i-1 downto 0 do
                   if (x[j]+min<=x[i])and(x[j]+max>=x[i]) then begin
                                                                 last:=j;
                                                                 break;
                                                               end;
                 if last=-1 then break;
                 if f[last]>=f[fmax] then fmax:=last
                 else if x[fmax]+max<x[i]
                        then begin
                               szy:=last;
                               for j:=last-1 downto fmax+1 do
                                 if x[j]+max>=x[i]
                                   then begin
                                          if f[j]>f[szy] then szy:=j;
                                        end
                                   else break;
                               fmax:=szy;
                             end;
                 f[i]:=f[fmax]+s[i];
               end
           else break;
      if (f[i-1]>=k)or(f[i]>=k) then r:=m
                                else l:=m+1;
    end;
  write(r);

  close(input);
  close(output);
end.
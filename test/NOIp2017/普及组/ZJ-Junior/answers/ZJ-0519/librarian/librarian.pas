var a:array[1..1000] of longint;n,p,i,j,k,g,l,q:longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  read(n,p);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to p do
   begin
     g:=1;q:=0;
     read(l,k);
     for j:=1 to l do g:=g*10;
     for j:=1 to n do
      if a[j] mod g=k then
       begin
         writeln(a[j]);
         q:=1;
         break;
       end;
     if q=0 then writeln(-1);
   end;
  close(input);close(output);
end.

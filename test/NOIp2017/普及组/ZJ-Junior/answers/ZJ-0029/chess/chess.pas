var m,n,i,x,y,c,j:longint;
 a,f:array[0..1005,0..1005] of longint;

 function min(x,y:longint):longint;
  begin
   if x<y then exit(x) else exit(y);
  end;

 begin

  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);

  readln(m,n);

  fillchar(f,sizeof(f),0);

  for i:=1 to n do
   begin
    readln(x,y,c);
    f[x,y]:=c+1;
   end;

  for i:=1 to m do
   for j:=1 to m do
    begin
     if f[i,j]=1 then
      begin
       if f[i-1,j]=0 then f[i-1,j]:=3;
       if f[i+1,j]=0 then f[i+1,j]:=3;
       if f[i,j-1]=0 then f[i,j-1]:=3;
       if f[i,j+1]=0 then f[i,j+1]:=3;
      end;
     if f[i,j]=2 then
      begin
       if f[i-1,j]=0 then f[i-1,j]:=4;
       if f[i+1,j]=0 then f[i+1,j]:=4;
       if f[i,j-1]=0 then f[i,j-1]:=4;
       if f[i,j+1]=0 then f[i,j+1]:=4;
      end;
    end;

  for i:=1 to m do
   for j:=1 to m do
    a[i,j]:=maxlongint-2;

  a[1,1]:=0;
  for i:=1 to m do
   for j:=1 to m do
    begin
     if (f[i+1,j]=f[i,j]) and (f[i+1,j]<=2) and (f[i,j]>0) then begin a[i+1,j]:=min(a[i+1,j],a[i,j]); end;
     if (f[i,j+1]=f[i,j]) and (f[i,j+1]<=2) and (f[i,j]>0) then begin a[i,j+1]:=min(a[i,j+1],a[i,j]); end;
     if (f[i+1,j]-2=f[i,j]) and (f[i+1,j]>2) then begin a[i+1,j]:=min(a[i+1,j],a[i,j]+2); end;
     if (f[i,j+1]-2=f[i,j]) and (f[i,j+1]>2) then begin a[i,j+1]:=min(a[i,j+1],a[i,j]+2); end;
     if (f[i+1,j]<>f[i,j]) and (f[i+1,j]<=2) and (f[i,j]>0) then begin a[i+1,j]:=min(a[i+1,j],a[i,j]+1); end;
     if (f[i,j+1]<>f[i,j]) and (f[i,j+1]<=2) and (f[i,j]>0) then begin a[i,j+1]:=min(a[i,j+1],a[i,j]+1); end;
     if (f[i+1,j]+2=f[i,j]) and (f[i,j]>2) then begin a[i+1,j]:=min(a[i,j],a[i+1,j]); end;
     if (f[i,j+1]+2=f[i,j]) and (f[i,j]>2) then begin a[i,j+1]:=min(a[i,j],a[i,j+1]); end;
    end;

  if a[i,j]=maxlongint-2 then writeln(-1) else writeln(a[m,m]);

  close(input);close(output);

 end.

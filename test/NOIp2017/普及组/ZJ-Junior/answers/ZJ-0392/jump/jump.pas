var n,m,i,j:longint;
    ans,l,r,mid,k,x,y:int64;
    a,b:array[0..500000]of longint;
    f:array[0..500000]of int64;
function max(a,b:longint):longint;
begin
 if a>b then exit(a);
 exit(b);
end;
begin
 assign(input,'jump.in');
 assign(output,'jump.out');
 reset(input);
 rewrite(output); 
 read(n,m,k);
 for i:=1 to n do
 begin
  read(a[i],b[i]);
  if a[i]>ans then ans:=a[i];
 end;
 r:=ans;
 while l<r do
 begin
  mid:=(l+r) div 2;
  if m>mid then x:=m-mid
  else x:=1;
  y:=m+mid;
  f[n]:=b[n];
  for i:=n-1 downto 0 do
  begin
   f[i]:=b[i];
   for j:=i+1 to n do
    if (a[j]-a[i]<=y)and(a[j]-a[i]>=x) then f[i]:=max(f[i],f[j]+b[i]);
  end;
  if f[0]>=k then r:=mid
  else l:=mid+1;
 end;
 f[n]:=b[n];
 for i:=n-1 downto 0 do
 begin
  f[i]:=b[i];
  for j:=i+1 to n do
  if (a[j]-a[i]<=m+r)and(a[j]-a[i]>=max(1,m-r)) then f[i]:=max(f[i],f[j]+b[i]);
 end;
 if f[0]>=k then writeln(r)
 else writeln(-1);
 close(input);
 close(output);
end.
var n,m,i,j,x,y,k,l,r:longint;
    a,dist,b:array[1..100,1..100]of longint;
    vis:array[1..100,1..100]of boolean;
    q,p:array[1..1000000]of longint;
    xx:array[1..4]of longint=(-1,0,0,1);
    yy:array[1..4]of longint=(0,-1,1,0);
function pd(a,b:longint):boolean;
begin
 if (a<1)or(a>n)or(b<1)or(b>n) then exit(false);
 if vis[a,b] then exit(false);
 exit(true);
end;
procedure dfs(x,y:longint);
var i:longint;
begin
 for i:=1 to 4 do
  if pd(x+xx[i],y+yy[i])and(a[x,y]=a[x+xx[i],y+yy[i]]) then
  begin
   dist[x+xx[i],y+yy[i]]:=dist[x,y];
   vis[x+xx[i],y+yy[i]]:=true;
   inc(r); q[r]:=x+xx[i]; p[r]:=y+yy[i];
   dfs(x+xx[i],y+yy[i]);
  end;
end;
begin
 assign(input,'chess.in');
 assign(output,'chess.out');
 reset(input);
 rewrite(output);
 read(n,m);
 for i:=1 to m do
 begin
  read(x,y,k);
  a[x,y]:=k+1;
  b[x,y]:=1;
 end;
 q[1]:=1; p[1]:=1;
 vis[1,1]:=true; dist[1,1]:=0;
 l:=1; r:=1;
 while l<=r do
 begin
  x:=q[l]; y:=p[l]; inc(l);
  dfs(x,y);
  for i:=1 to 4 do
   if pd(x+xx[i],y+yy[i]) then
   begin
    if b[x,y]=0 then
     if b[x+xx[i],y+yy[i]]<>0 then
     begin
      if a[x+xx[i],y+yy[i]]=a[x,y] then dist[x+xx[i],y+yy[i]]:=dist[x,y]
      else dist[x+xx[i],y+yy[i]]:=dist[x,y]+1;
      vis[x+xx[i],y+yy[i]]:=true;
      inc(r);
      q[r]:=x+xx[i];
      p[r]:=y+yy[i];
     end;
    if b[x,y]<>0 then
    begin
     if b[x+xx[i],y+yy[i]]=0 then
     begin
      dist[x+xx[i],y+yy[i]]:=dist[x,y]+2;
      a[x+xx[i],y+yy[i]]:=a[x,y];
     end
     else if a[x+xx[i],y+yy[i]]=a[x,y] then dist[x+xx[i],y+yy[i]]:=dist[x,y]
      else dist[x+xx[i],y+yy[i]]:=dist[x,y]+1;
     vis[x+xx[i],y+yy[i]]:=true;
     inc(r);
     q[r]:=x+xx[i];
     p[r]:=y+yy[i];
    end;
   end;
 end;
 if vis[n,n] then writeln(dist[n,n])
 else writeln(-1);
 close(input);
 close(output);
end.
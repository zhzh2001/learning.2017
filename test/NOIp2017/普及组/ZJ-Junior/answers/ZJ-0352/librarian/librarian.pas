var
  n,q,ans,i,j,k,t,x,y:longint;
  a:array[1..1000] of longint;
  f:array[1..1000,1..100] of longint;
  find:longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:= 1 to n do readln(a[i]);
  for i:= 1 to n-1 do
    for j:= i+1 to n do
      if a[i]>a[j] then
      begin t:=a[i]; a[i]:=a[j]; a[j]:=t; end;
  for i:= 1 to n do
  begin
    x:=a[i];
    t:=10;
    for j:= 1 to 8 do
    begin
      if t<=x then f[i,j]:=x mod t else break;
      t:=t*10;
    end;
    x:=0;
  end;
  //for i:= 1 to n do writeln(a[i]);
  for i:= 1 to q do
  begin
    readln(x,y);
    find:=-1;
    for j:= 1 to n do
      if (y=f[j,x]) or (y=a[j]) then begin find:=j; break; end;
    if find<>-1 then writeln(a[j]) else writeln('-1');
  end;
  close(input);
  close(output);
end.

const
  dx:array[1..4] of longint=(0,0,-1,1);
  dy:array[1..4] of longint=(1,-1,0,0);
var
  i,j,t,x,y,c,n,m,top:longint;
  map:array[0..101,0..101] of longint;
  b:array[0..101,0..101] of boolean;
  ans:longint;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
procedure try(x,y,cost:longint; rs:boolean);
var i,j,cnt,nx,ny:longint;
begin
  if (x=m) and (y=m) then t:=min(t,cost) else
  begin
    cnt:=0;
    for i:= 1 to 4 do
    begin
      nx:=x+dx[i]; ny:=y+dy[i];
      if not b[nx,ny] then
      begin
        b[nx,ny]:=true;
        if (nx>0) and (nx<=m) and (ny>0) and (ny<=m) then
          if map[nx,ny]=map[x,y] then try(nx,ny,cost,false) else
          if (map[nx,ny]<>map[x,y]) and (map[nx,ny]<>0) then try(nx,ny,cost+1,false) else
          if (map[nx,ny]=0) and (not rs) then
          begin
            inc(cnt); map[nx,ny]:=2;
            if map[x,y]=2 then try(nx,ny,cost+2,true) else
            if map[x,y]=1 then try(nx,ny,cost+3,true);
            map[nx,ny]:=0;
          end;
        b[nx,ny]:=false;
      end;
      if cnt=4 then
      begin
        write('-1'); close(input); close(output); halt;
      end;
    end;
  end;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:= 1 to n do
  begin
    readln(x,y,c);
    map[x,y]:=c+1;
  end;
  fillchar(b,sizeof(b),0);
  t:=maxlongint div 3;
  try(1,1,0,false);
  if t=maxlongint div 3 then write('-1') else write(t);
  close(input);
  close(output);
end.
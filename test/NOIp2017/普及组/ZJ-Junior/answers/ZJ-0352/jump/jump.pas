type data=record
  x,s:longint;
end;
var
  n,d,k,g,i,j,top,loc,now,t,t1:longint;
  dis:array[1..500000] of longint;
  f:array[0..500000] of data;
function cmp(y:longint):longint;
var i:longint;
begin
  for i:= 1 to n do
    if f[i].x=y then exit(i);
  exit(0);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:= 1 to n do
  begin
    readln(f[i].x,f[i].s);
    if f[i].s>0 then t:=t+f[i].s;
  end;
  if t<k then
  begin
    write('-1');
    halt;
  end;
  loc:=f[1].x; now:=f[1].s;
  while true do
  begin
    inc(g);  top:=0;
    if g<d then
    begin
      for i:= d-g to d+g do begin inc(top); dis[top]:=i; end;
    end else
    begin
      for i:= 1 to d+g do begin inc(top); dis[top]:=i; end;
    end;
    //reset dis over
    t1:=-maxlongint div 3;
    for i:= 1 to top do
    begin
      if (f[cmp(f[loc].x+dis[i])].s>t1) then
      begin
        t1:=f[cmp(f[loc].x+dis[i])].s;
        t:=cmp(f[loc].x+dis[i]); loc:=t;
        if now+t1>=k then
        begin
           write(g); close(input); close(output); halt;
        end;
      end;
    end;
    loc:=t; now:=now+t1;
    if now>=k then
    begin
       write(g); close(input); close(output);
       halt;
    end;
  end;
  close(input);
  close(output);
end.


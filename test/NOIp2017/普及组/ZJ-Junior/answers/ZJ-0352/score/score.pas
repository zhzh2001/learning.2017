var
  a,b,c,x,y,z:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  readln(a,b,c);
  x:= a div 5; y:= b div 10 * 3; z:=c div 2;
  write(x+y+z);
  close(input);
  close(output);
end.
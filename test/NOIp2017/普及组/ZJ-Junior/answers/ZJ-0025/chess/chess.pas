type rec=record
x,y,cost,co:longint;
end;
var i,j,n,k,l,m,x,y,z,nowx,nowy,nowc,h,t,min,nowco:longint;
bo:array[0..101,0..101] of longint;
a:array[0..101,0..101] of longint;
q:array[0..1000000] of rec;
procedure push(a,b,hua,cos:longint);
begin
  if (a=m)and(b=m) then
  begin
    if hua<min then min:=hua;
    exit;
  end;
  if (a<1)or(a>m)or(b<1)or(b>m) then exit;
  if hua>=bo[a,b] then exit;
  bo[a,b]:=hua;
  if hua>=min then exit;
  inc(t);
  q[t].x:=a;
  q[t].y:=b;
  q[t].cost:=hua;
  q[t].co:=cos;
end;
begin
   assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  read(m,n);
  for i:=0 to m+1 do
   for j:=0 to m+1 do a[i,j]:=2;
  fillchar(bo,sizeof(bo),127);
  for i:=1 to n do
  begin
    read(x,y,z);
    a[x,y]:=z;
  end;
  min:=maxlongint;
  push(1,1,0,3);
  repeat
    inc(h);
    nowx:=q[h].x;nowy:=q[h].y;nowc:=q[h].cost;nowco:=q[h].co;
    if a[nowx,nowy]=0 then
    begin
      if a[nowx+1,nowy]=0 then push(nowx+1,nowy,nowc,3) else
      if a[nowx+1,nowy]=1 then push(nowx+1,nowy,nowc+1,3) else
      if a[nowx+1,nowy]=2 then push(nowx+1,nowy,nowc+2,0);
  //---------------------------------------------------------------
      if a[nowx-1,nowy]=0 then push(nowx-1,nowy,nowc,3) else
      if a[nowx-1,nowy]=1 then push(nowx-1,nowy,nowc+1,3) else
      if a[nowx-1,nowy]=2 then push(nowx-1,nowy,nowc+2,0);
  //---------------------------------------------------------------
      if a[nowx,nowy+1]=0 then push(nowx,nowy+1,nowc,3) else
      if a[nowx,nowy+1]=1 then push(nowx,nowy+1,nowc+1,3) else
      if a[nowx,nowy+1]=2 then push(nowx,nowy+1,nowc+2,0);
  //---------------------------------------------------------------
      if a[nowx,nowy-1]=0 then push(nowx,nowy-1,nowc,3) else
      if a[nowx,nowy-1]=1 then push(nowx,nowy-1,nowc+1,3) else
      if a[nowx,nowy-1]=2 then push(nowx,nowy-1,nowc+2,0);
    end else
    if a[nowx,nowy]=1 then
    begin
      if a[nowx+1,nowy]=1 then push(nowx+1,nowy,nowc,3) else
      if a[nowx+1,nowy]=0 then push(nowx+1,nowy,nowc+1,3) else
      if a[nowx+1,nowy]=2 then push(nowx+1,nowy,nowc+2,1);
  //---------------------------------------------------------------
      if a[nowx-1,nowy]=1 then push(nowx-1,nowy,nowc,3) else
      if a[nowx-1,nowy]=0 then push(nowx-1,nowy,nowc+1,3) else
      if a[nowx-1,nowy]=2 then push(nowx-1,nowy,nowc+2,1);
  //---------------------------------------------------------------
      if a[nowx,nowy+1]=1 then push(nowx,nowy+1,nowc,3) else
      if a[nowx,nowy+1]=0 then push(nowx,nowy+1,nowc+1,3) else
      if a[nowx,nowy+1]=2 then push(nowx,nowy+1,nowc+2,1);
  //---------------------------------------------------------------
      if a[nowx,nowy-1]=1 then push(nowx,nowy-1,nowc,3) else
      if a[nowx,nowy-1]=0 then push(nowx,nowy-1,nowc+1,3) else
      if a[nowx,nowy-1]=2 then push(nowx,nowy-1,nowc+2,1);
    end else
    if a[nowx,nowy]=2 then
    begin
      if a[nowx+1,nowy]<>2 then
      if a[nowx+1,nowy]<>nowco then push(nowx+1,nowy,nowc+1,3)
      else push(nowx+1,nowy,nowc,3);
      if a[nowx-1,nowy]<>2 then
      if a[nowx-1,nowy]<>nowco then push(nowx-1,nowy,nowc+1,3)
      else push(nowx-1,nowy,nowc,3);
      if a[nowx,nowy-1]<>2 then
      if a[nowx,nowy-1]<>nowco then push(nowx,nowy-1,nowc+1,3)
      else push(nowx,nowy-1,nowc,3);
      if a[nowx,nowy+1]<>2 then
      if a[nowx,nowy+1]<>nowco then push(nowx,nowy+1,nowc+1,3)
      else push(nowx,nowy+1,nowc,3);
    end;
  until h>=t;
  if min=maxlongint then write(-1) else write(min);
    close(input);
  close(output);
end.
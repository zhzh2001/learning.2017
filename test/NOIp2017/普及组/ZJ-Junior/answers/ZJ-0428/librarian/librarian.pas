var n,m,sk:int64;i,j:longint;st:string;
    a:array[0..1005] of int64;
    c:array[0..1005,1..2] of int64;

procedure q(l,r:longint);
var i,j,mid:longint;
begin
  i:=l;j:=r;mid:=a[(l+r) div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then
      begin a[0]:=a[i];a[i]:=a[j];a[j]:=a[0];inc(i);dec(j);end;
  until i>j;
  if j>l then q(l,j);if i<r then q(i,r);
end;

begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  for i:=1 to m do readln(c[i,1],c[i,2]);
  q(1,n);
  for i:=1 to m do
     for j:=1 to n do
        begin
          str(a[j],st);
          if length(st)<c[i,1] then continue;
          st:=copy(st,length(st)-c[i,1]+1,c[i,1]);
          val(st,sk);
          if sk=c[i,2] then
            begin writeln(a[j]);break;end
              else if j=n then
                begin writeln(-1);break;end;
        end;
  close(input);close(output);
end.

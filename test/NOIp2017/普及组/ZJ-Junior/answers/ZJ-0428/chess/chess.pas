type point=record x,y,p,l:longint;m:boolean;end;
var dx:array[1..4] of longint=(-1,1,0,0);
    dy:array[1..4] of longint=(0,0,-1,1);
    n,k,i,j,head,tail,x1,y1,q,w,e:longint;
    a,b:array[0..105,0..105] of longint;
    s:array[1..1000000] of point;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,k);
  for i:=1 to n do
     for j:=1 to n do a[i,j]:=-1;
  for i:=1 to k do
     begin readln(q,w,e);a[q,w]:=e;end;
  for i:=1 to 1000000 do s[i].p:=maxlongint;
  s[1].x:=1;s[1].y:=1;s[1].p:=0;s[1].m:=true;
  for i:=1 to n do
     for j:=1 to n do b[i,j]:=maxlongint;
  b[1,1]:=0;
  q:=maxlongint;
  head:=0;tail:=1;
  while head<=tail do
    begin
      inc(head);
      for i:=1 to 4 do
        begin
          inc(tail);
          x1:=s[head].x+dx[i];
          y1:=s[head].y+dy[i];
          if (x1<1)or(x1>n)or(y1<1)or(y1>n)or(s[head].p>=b[x1,y1])
               or(s[head].m=false)and(a[x1,y1]=-1)
                 then
                   begin dec(tail);continue;end;
          s[tail].x:=x1;s[tail].y:=y1;s[tail].m:=true;
          if a[x1,y1]=-1 then
             begin
               s[tail].p:=s[head].p+2;
               s[tail].m:=false;
               s[tail].l:=a[s[head].x,s[head].y];
             end
               else
              if a[s[head].x,s[head].y]=-1 then
                 begin
                   if s[head].l<>a[x1,y1] then
                     s[tail].p:=s[head].p+1
                       else
                         s[tail].p:=s[head].p;
                 end
                else
                  if a[s[head].x,s[head].y]<>a[x1,y1] then
                     s[tail].p:=s[head].p+1 else
                     s[tail].p:=s[head].p;
          if s[tail].p<b[x1,y1] then b[x1,y1]:=s[tail].p;
        end;
    end;
  if b[n,n]=maxlongint then b[n,n]:=-1;
  writeln(b[n,n]);
  close(input);close(output);
end.
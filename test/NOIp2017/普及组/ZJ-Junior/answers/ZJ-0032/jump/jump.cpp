#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,d,k,sum=0;
int x[500010],s[500010];
bool check(int a)
{
	int res=0;
	for(int i=1;i<=n;i++)
	{
		if(x[i]-x[i-1]<=d+a && x[i]-x[i-1]>=max(1,d-a))
		{
			if(s[i]>=0) res+=s[i];
			else
			{
				int j=n,h=i;
				while(j>i)
				{
					j--;
					if(x[j]-x[i-1]<=d+a && x[j]-x[i-1]>=max(1,d-a))
					{
						if(s[j]>=0)
						{
							h=j;
							break;
						}
						else if(sum+s[j]>=k) h=j;
					}
					else break;
				}
				i=h;
				res+=s[i];
			}
		}
		
	}
	return res>=k;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d",&x[i],&s[i]);
		if(s[i]>0) sum+=s[i];
	}
	if(sum<k)
	{
		printf("-1");
		return 0;
	}
	int l=0,r=x[n]-x[1];
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid)) r=mid-1;
		else l=mid+1;
	}
	printf("%d",l);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

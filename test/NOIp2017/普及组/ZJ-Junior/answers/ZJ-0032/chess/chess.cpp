#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m;
int a[110][110],val[110][110],mar[110][110];
int det[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
struct Point
{
	int x,y;
}q[200001],u,v;
void bfs()
{
	int l=0,r=1,f[110][110];
	q[1].x=1,q[1].y=1;
	val[1][1]=0;
	while(l<r)
	{
		u=q[++l];
		f[u.x][u.y]=0;
		for(int i=0;i<4;i++)
		{
			v.x=u.x+det[i][0],v.y=u.y+det[i][1];
			if(v.x<1 || v.x>n || v.y<1 || v.y>n) continue;
			int p;
			if(a[u.x][u.y]>1)
			{
				if(a[v.x][v.y]==-1 || a[v.x][v.y]>1) continue;
				if(a[u.x][u.y]-2==a[v.x][v.y]) p=0;
				else p=1;
			}
			else
			{
				if(a[v.x][v.y]==-1 || a[v.x][v.y]>1)
				{
					a[v.x][v.y]=a[u.x][u.y]+2;
					p=2;
				}
				else if(a[u.x][u.y]==a[v.x][v.y]) p=0;
				else p=1;
			}
			if(val[u.x][u.y]+p<val[v.x][v.y] || val[v.x][v.y]==-1)
			{
				val[v.x][v.y]=val[u.x][u.y]+p;
				if(!f[v.x][v.y])
				{
					q[++r]=v;
					f[v.x][v.y]=1;
				}
			}
		}
		if(a[u.x][u.y]>1) a[u.x][u.y]=-1;
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	for(int i=0;i<=101;i++)
		for(int j=0;j<=101;j++)
			val[i][j]=a[i][j]=-1;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=z;
	}
	int b[110][110];
	bfs();
	printf("%d",val[n][n]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

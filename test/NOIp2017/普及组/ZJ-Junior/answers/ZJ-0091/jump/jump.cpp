#include<bits/stdc++.h>
using namespace std;
int n,d,k,x[500001];
long long s[500001],f[500001];
bool dps(int g)
{
	int j=0,df=max(1,d-g),dl=d+g;
	memset(f,0,sizeof(f));
	f[0]=s[0];
	for (int i=1;i<=n;i++)
	{
		while (x[i]-x[j]>dl) j++;
		int p=j;
		while (x[i]-x[p]>=df)
		{
			f[i]=max(f[i],f[p]);
			p++;
		}
		f[i]+=s[i];
		if (f[i]-1000000000>=k) return true;
	}
	return false;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	long long pp=0;
	cin >> n >> d >> k;
	for (int i=1;i<=n;i++) 
	{
		scanf("%d%lld",&x[i],&s[i]);
		if (s[i]>0) pp+=s[i];
    }
    if (pp<k)
    {
    	cout << -1;
    	return 0;
	}
	x[0]=0;
	s[0]=1000000000;
	int l=abs(x[1]-d),r=abs(x[n]-d);
	while (l<r)
	{
		int g=(l+r)>>1;
		if (dps(g)) r=g;
		else l=g+1;
	}
	cout << r;
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n,m,a[101][101],dx[4]={-1,1,0,0},dy[4]={0,0,-1,1},ans=100000000,has[101][101][2];
bool vis[101][101];
void dfs(int x,int y,int s,int c,int ch)
{
	if (x==m&&y==m)
	{
		ans=min(ans,s);
		return;
	}
	if (ans<s) return;
	if (has[x][y][c]!=-1&&has[x][y][c]<=s) return;
	has[x][y][c]=s;
	for (int i=0;i<4;i++)
	{
		int tx=x+dx[i],ty=y+dy[i];
		if (tx<1||ty<1||tx>m||ty>m||vis[tx][ty]) continue;
		if (ch==1&&a[tx][ty]==-1) continue;
		if (a[tx][ty]>=0)
		{
			if (a[tx][ty]!=c)
			{
				vis[tx][ty]=true;
				dfs(tx,ty,s+1,a[tx][ty],0);
				vis[tx][ty]=false;
			}
			else
			{
				vis[tx][ty]=true;
				dfs(tx,ty,s,a[tx][ty],0);
				vis[tx][ty]=false;
			}
		}
		else
		{
			vis[tx][ty]=true;
			dfs(tx,ty,s+2,c,1);
			vis[tx][ty]=false;
		}
	}
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin >> m >> n;
	for (int i=1;i<=m;i++)
	  for (int j=1;j<=m;j++) a[i][j]=-1;
	for (int i=1;i<=n;i++)
	{
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		a[x][y]=c;
	}
	memset(vis,false,sizeof(vis));
	for (int i=1;i<=m;i++)
	  for (int j=1;j<=m;j++)
	    for (int k=0;k<=1;k++) has[i][j][k]=-1;
	dfs(1,1,0,a[1][1],0);
	if (ans==100000000) cout << -1;
	else cout << ans;
	return 0;
}

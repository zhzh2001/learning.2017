var
  ans,x,y,z,n,m,i,j:longint;   p,k,newx,newy:longint;
  f,a:array[0..105,0..105]of longint;
  flag,f2:array[0..105,0..105]of boolean;
  h:array[1..4]of longint=(1,-1,0,0);
  l:array[1..4]of longint=(0,0,-1,1);
function min(a,b:longint):longint;
begin
  if a=-1 then exit(b);
  if b=-1 then exit(a);
  if a<b then exit(a);exit(b);
end;
function dfs(x,y,z,lastx,lasty:longint):longint;   //j:boolean;
var p,k,newx,newy:longint;
begin
  //writeln(x,' ',y,' ',z,' ',lastx,' ',lasty,' ',flag[lastx,lasty]);
  if (x+y)=2 then exit(z);
  if f[x,y]<>-2 then exit(f[x,y]);
  p:=-1;
  for k:=1 to 4 do
  begin
    newx:=x+h[k];
    newy:=y+l[k];
    if not(newx in [1..n]) then continue;
    if not(newy in [1..n])then continue;
    if f2[newx,newy] then continue;
    if (a[newx,newy]=a[x,y])then begin f2[x,y]:=true;p:=min(p,dfs(newx,newy,z,x,y));
    f2[x,y]:=false;end;
    if (a[newx,newy]+a[x,y]=3)then begin f2[x,y]:=true;p:=min(p,dfs(newx,newy,z+1,x,y));
    f2[x,y]:=false;end;
    if (flag[lastx,lasty]=false)and(a[newx,newy]=0)then begin
      flag[x,y]:=true;
      f2[x,y]:=true;
      a[newx,newy]:=a[x,y];
      p:=min(p,dfs(newx,newy,z+2,x,y));   f2[x,y]:=false;
      flag[x,y]:=false;
      a[newx,newy]:=0;
    end;
  end;
  f[x,y]:=p;
  dfs:=f[x,y];
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  fillchar(a,sizeof(a),0);
  for i:=1 to m do
  begin
    readln(x,y,z);
    a[x,y]:=z+1;
  end;
  for i:=1 to n do
    for j:=1 to n do
    f[i,j]:=-2;
 // for i:=1 to n do begin for j:=1 to n do write(a[i,j]);writeln;end;
  fillchar(flag,sizeof(flag),0);
  fillchar(f2,sizeof(f2),false);
  ans:=-1;
  ans:=dfs(n,n,0,n,n); writeln(ans);
  close(input);close(output);
end.
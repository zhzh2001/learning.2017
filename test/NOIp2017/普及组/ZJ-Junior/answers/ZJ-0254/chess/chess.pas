var a:array[-1..1002,-1..1002] of byte;
f:array[-1..1002,-1..1002] of boolean;
xx,yy,zz,i,n,m,min,j:longint;
procedure pdd(x,y,sum,zt:longint);
var t:longint;
begin
        if ((f[x+1,y]=false) or ((zt=0) and (a[x+1,y]=0)))
        and ((f[x-1,y]=false) or ((zt=0) and (a[x-1,y]=0)))
        and ((f[x,y-1]=false) or ((zt=0) and (a[x,y-1]=0)))
        and ((f[x,y+1]=false) or ((zt=0) and (a[x,y+1]=0))) then exit;
        if (x<1) or (y<1) or (x>n) or (y>n) then exit;
        if (x=n) and (y=n) then
        begin
                if sum<min then min:=sum;
                exit;
        end;
        if (a[x,y+1]>0) and (f[x,y+1]) then
        begin
                t:=0;
                if a[x,y+1]<>a[x,y] then t:=1;
                f[x,y+1]:=false;
                pdd(x,y+1,sum+t,zt+1);
                f[x,y+1]:=true;
        end;
        if (zt>0) and (f[x,y+1]) and (a[x,y+1]=0) then
        begin
                f[x+1,y+1]:=false;
                a[x+1,y+1]:=a[x,y];
                pdd(x+1,y+1,sum+2,0);
                f[x+1,y+1]:=false;
                a[x+1,y+1]:=0;
        end;
        if (a[x+1,y]>0) and (f[x+1,y]) then
        begin
                t:=0;
                if a[x+1,y]<>a[x,y] then t:=1;
                f[x+1,y]:=false;
                pdd(x+1,y,sum+t,zt+1);
                f[x+1,y]:=true;
        end;
        if (zt>0) and (f[x+1,y]) and (a[x+1,y]=0)then
        begin
                f[x+1,y]:=false;
                a[x+1,y]:=a[x,y];
                pdd(x+1,y,sum+2,0);
                f[x+1,y]:=false;
                a[x+1,y]:=0;
        end;
        if (a[x-1,y]>0) and (f[x-1,y]) then
        begin
                t:=0;
                if a[x-1,y]<>a[x,y] then t:=1;
                f[x-1,y]:=false;
                pdd(x-1,y,sum+t,zt+1);
                f[x-1,y]:=true;
        end;
        if (zt>0) and (f[x-1,y]) and (a[x-1,y]=0) then
        begin
                f[x-1,y]:=false;
                a[x-1,y]:=a[x,y];
                pdd(x-1,y,sum+2,0);
                f[x-1,y]:=false;
                a[x-1,y]:=0;
        end;
        if (a[x,y-1]>0) and (f[x,y-1]) then
        begin
                t:=0;
                if a[x,y-1]<>a[x,y] then t:=1;
                f[x,y-1]:=false;
                pdd(x,y-1,sum+t,zt+1);
                f[x,y-1]:=true;
        end;
        if (zt>0) and (f[x,y-1]) and (a[x,y-1]=0) then
        begin
                f[x,y-1]:=false;
                a[x,y-1]:=a[x,y];
                pdd(x,y-1,sum+2,0);
                f[x,y-1]:=false;
                a[x,y-1]:=0;
        end;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
        min:=maxlongint;
        readln(n,m);
        for i:=1 to m do begin readln(xx,yy,zz); a[xx,yy]:=zz+1; end;
        for i:=1 to n do
                for j:=1 to n do
                f[i,j]:=true;
        f[1,1]:=false;
        pdd(1,1,0,1);
        if min=maxlongint then writeln('-1')
        else writeln(min);
close(input);
close(output);
end.

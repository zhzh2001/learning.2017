#include<bits/stdc++.h>
using namespace std;

struct node{ int x,y,v; }a[1000001];
int m,n,ma[101][101],fa[100001],ans,tot;

int read(void) {
	char c; while (c=getchar(),c<'0' || c>'9'); int x=c-'0';
	while (c=getchar(),c>='0' && c<='9') x=x*10+c-'0'; return x;
}

int cmp(node a,node b) { return a.v<b.v; }

int getf(int x) { if (fa[x]==x) return x; else return fa[x]=getf(fa[x]); }

int main() {
	freopen("chess.in","r",stdin); freopen("chess.out","w",stdout);
	m=read(); n=read();
	  for (int i=1;i<=m;++i)
	    for (int j=1;j<=m;++j) ma[i][j]=-1;
	  for (int i=1;i<=n;++i) {
	  	int xx=read(),yy=read(),vv=read();
	  	  ma[xx][yy]=vv;
	  }
	  for (int i=1;i<=m;++i)
	    for (int j=1;j<=m;++j) 
	      if (ma[i][j]>-1) {
	      	if (i-1>=1 && ma[i-1][j]>-1) {
	      		if (ma[i-1][j]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-2)*m+j,a[tot].v=0;
	      		else a[++tot].x=(i-1)*m+j,a[tot].y=(i-2)*m+j,a[tot].v=1;
			  }
			if (i+1<=m && ma[i+1][j]>-1) {
				if (ma[i+1][j]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=i*m+j,a[tot].v=0;
				else a[++tot].x=(i-1)*m+j,a[tot].y=i*m+j,a[tot].v=1;
			}
			if (j+1<=m && ma[i][j+1]>-1) {
				if (ma[i][j+1]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j+1,a[tot].v=0;
				else a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j+1,a[tot].v=1;
			}
			if (j-1>=1 && ma[i][j-1]>-1) {
				if (ma[i][j-1]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j-1,a[tot].v=0;
				else a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j-1,a[tot].v=1;
			}
			if (j-1>=1 && i-1>=1 && ma[i-1][j-1]>-1) {
			  if (ma[i-1][j-1]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-2)*m+j-1,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=(i-2)*m+j-1,a[tot].v=3;
			}
			if (j+1<=m && i-1>=1 && ma[i-1][j+1]>-1) {
			  if (ma[i-1][j+1]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-2)*m+j+1,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=(i-2)*m+j+1,a[tot].v=3;
			}
			if (j-1>=1 && i+1<=m && ma[i+1][j-1]>-1) {
			  if (ma[i+1][j-1]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=i*m+j-1,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=i*m+j-1,a[tot].v=3;
			}
			if (j+1<=m && i+1<=m && ma[i+1][j+1]>-1) {
			  if (ma[i+1][j+1]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=i*m+j+1,a[tot].v=2; 
			  else a[++tot].x=(i-1)*m+j,a[tot].y=i*m+j+1,a[tot].v=3; 
			}
			if (i-2>=1 && ma[i-2][j]>-1) {
			  if (ma[i-2][j]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-3)*m+j,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=(i-3)*m+j,a[tot].v=3;
			}
			if (i+2<=m && ma[i+2][j]>-1) {
			  if (ma[i+2][j]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i+1)*m+j,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=(i+1)*m+j,a[tot].v=3;
			}
			if (j-2>=1 && ma[i][j-2]>-1) {
			  if (ma[i][j-2]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j-2,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j-2,a[tot].v=3;
			}
			if (j+2<=m && ma[i][j+2]>-1) {
			  if (ma[i][j+2]==ma[i][j]) a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j+2,a[tot].v=2;
			  else a[++tot].x=(i-1)*m+j,a[tot].y=(i-1)*m+j+2,a[tot].v=3;
			}
		  }
	sort(a+1,a+1+tot,cmp);
	  for (int i=1;i<=m*m;++i) fa[i]=i;
	  for (int i=1;i<=tot;++i) {
	  	if (getf(a[i].x)!=getf(a[i].y)) {
	  		fa[getf(a[i].x)]=getf(a[i].y); ans+=a[i].v;
	  		if (getf(1)==getf(m*m)) break;
		  }
	  }
	if (getf(1)==getf(m*m)) printf("%d",ans);
	else if (getf(1)==getf(m*m-1)) printf("%d",ans+2);
	else if (getf(1)==getf(m*(m-1))) printf("%d",ans+2);
	else puts("-1");
	return 0;
}

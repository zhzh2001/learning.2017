#include<bits/stdc++.h>
using namespace std;

int n,d,k,t,x[500001],y[500001],f[500001];

int read(void) {
	char c; while (c=getchar(),(c<'0' || c>'9') && c!='-'); int x=0,y=1;
	if (c=='-') y=-1; else x=c-'0';
	while (c=getchar(),c>='0' && c<='9') x=x*10+c-'0'; return x*y;
}

int main() {
	freopen("jump.in","r",stdin); freopen("jump.out","w",stdout);
	n=read(); d=read(); k=read();
	  for (int i=1;i<=n;++i) x[i]=read(),y[i]=read(),t=max(t,x[i]);
	  for (int h=1;h<=t;++h) {
	  	for (int i=1;i<=n;++i) f[i]=-2e9;
	  	for (int i=1;i<=n;++i)
	      for (int j=0;j<i;++j) {
	      	if (h<d) {
	      		if (abs(x[i]-x[j])>=d-h && abs(x[i]-x[j])<=d+h) f[i]=max(f[i],f[j]+y[i]);
			  }
			else {
			  if (abs(x[i]-x[j])>=1 && abs(x[i]-x[j])<=d+h) f[i]=max(f[i],f[j]+y[i]);
			}
		  }
		if (f[n]>=k) { printf("%d",h); return 0; }
	  }
	puts("-1");
	return 0;
}

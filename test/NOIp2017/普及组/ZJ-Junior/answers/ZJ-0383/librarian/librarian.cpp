#include<bits/stdc++.h>
using namespace std;

int n,q,len,o,a[1001];
char s[1001][10],t[10];

int read(void) {
	char c; while (c=getchar(),c<'0' || c>'9'); int x=c-'0';
	while (c=getchar(),c>='0' && c<='9') x=x*10+c-'0'; return x;
}

int getlen(int x) {
	int tot=0;
	while (x) {
	  x/=10; tot++;
	}
	return tot;
}

int check(int x) {
	int p=0;
	if (len>strlen(s[x])) return 0;
	for (int i=strlen(s[x])-len;i<strlen(s[x]);++i)
	  if (s[x][i]!=t[p]) return 0; else p++;
	return 1;
}

int main() {
	freopen("librarian.in","r",stdin); freopen("librarian.out","w",stdout);
	n=read(); q=read();
	  for (int i=1;i<=n;++i) a[i]=read();
	sort(a+1,a+1+n);
	  for (int i=1;i<=n;++i) {
	  	int d=getlen(a[i]),tot=0;
	  	  while (a[i]) {
	  	  	  s[i][d-1-tot]=a[i]%10+'0'; a[i]/=10; tot++;
			}
	  }
	  while (q--) {
	  	len=read(); scanf("%s",t); o=0;
	  	  for (int i=1;i<=n;++i) 
	  	    if (check(i)) { puts(s[i]); o=1; break; }
		if (!o) puts("-1");
	  }
}

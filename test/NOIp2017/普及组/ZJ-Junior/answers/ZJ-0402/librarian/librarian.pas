var
  i,j,k,t,n,m,l:longint;
  a,ans:array[0..1005]of longint;
  b:array[0..1005,0..1005]of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;j:=r;x:=a[(l+r) div 2];
         repeat
           while a[i]<x do inc(i);
           while x<a[j] do dec(j);
           if not(i>j) then
             begin
                y:=a[i];a[i]:=a[j];a[j]:=y;inc(i);j:=j-1;
             end;
         until i>j;
         if l<j then sort(l,j);
         if i<r then sort(i,r);
      end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do ans[i]:=-1;
  for i:=1 to n do readln(a[i]);
  sort(1,n);
  for i:=1 to m do readln(b[i,1],b[i,2]);
  for i:=1 to m do
  begin
    l:=1;
    for k:=1 to b[i,1] do l:=l*10;
    for j:=1 to n do if (a[j]-b[i,2])mod l=0 then begin ans[i]:=a[j];break;end;
  end;
  for i:=1 to n do writeln(ans[i]);
  close(input);close(output);
end.


var
	n,d,k,x,y,l,r,mid,i,min1,max1,ansmax,tmax:longint;
	a,b:array[0.. 1000000] of longint;
procedure dfs(x,y:longint);
	var i:longint;
	begin
		if x>tmax then exit;
		for i:=min1 to max1 do
			if a[x+i]<>0 then dfs(x+i,y+a[x+i]);
		if ansmax<y then ansmax:=y;
	end;
function max(x,y:longint):longint;
        begin
                if x>y then exit(x)
                       else exit(y);
        end;
function min(x,y:longint):longint;
        begin
                if x<y then exit(x)
                       else exit(y);
        end;
begin
	assign(input,'jump.in');
	reset(input);
	assign(output,'jump.out');
	rewrite(output);
	readln(n,d,k);
	for i:=1 to n do
		begin
			readln(b[i],y);
			a[b[i]]:=y;
		end;
	ansmax:=0;
	for i:=1 to n do
		if a[b[i]]>0 then inc(ansmax,a[b[i]]);
	if ansmax<k then
		begin
			writeln(-1);
			halt;
		end;
	l:=0;
	tmax:=b[n];
	r:=max(tmax-d,d-1);
        while l<r do
		begin
			mid:=(l+r) div 2;
			min1:=max(1,d-mid);
			max1:=min(tmax,d+mid);
			ansmax:=-maxlongint;
			dfs(0,0);
                        if ansmax<k then l:=mid+1
				    else r:=mid;
		end;
	writeln(l);
	close(input);
	close(output);
end.

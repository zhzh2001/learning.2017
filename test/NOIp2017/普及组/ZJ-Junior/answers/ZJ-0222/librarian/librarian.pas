var
	n,q,i,j,k:longint;
	s1,s2:array[0..1010] of ansistring;
        a:array[0..1010] of longint;
	s,ans:ansistring;
function fun(x,y:ansistring):boolean;
	var i:longint;
	begin
		if length(x)>length(y) then exit(false);
		if length(y)>length(x) then exit(true);
		for i:=1 to length(x) do
			begin
				if x[i]>y[i] then exit(false);
				if x[i]<y[i] then exit(true);
			end;
		exit(true);
	end;
begin
	assign(input,'librarian.in');
	reset(input);
	assign(output,'librarian.out');
	rewrite(output);
	readln(n,q);
	for i:=1 to n do
		readln(s1[i]);
	for i:=1 to q do
		begin
			readln(s);
			for j:=1 to length(s) do
				if s[j]=' ' then break;
			val(copy(s,1,j-1),a[i]);
			delete(s,1,j);
			s2[i]:=s;
		end;
	for i:=1 to q do
		begin
			ans:='';
			s1[n+1]:=s2[i];
			for j:=1 to n+1 do
				if copy(s1[j],length(s1[j])-a[i]+1,a[i])=s2[i] then break;
			if j=n+1 then 
				begin
					writeln(-1);
					continue;
				end;
			ans:=s1[j];
			k:=j+1;
			for j:=k to n do
				if (copy(s1[j],length(s1[j])-a[i]+1,a[i])=s2[i])and fun(s1[j],ans) then ans:=s1[j];
			if ans='' then writeln(-1)
				  else writeln(ans);
		end;
	close(input);
	close(output);	
end.

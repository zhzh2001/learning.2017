var
	ans,n,m,i,x,y,z:longint;
	a:array[0..100,0..100] of longint;
	f:array[0..100,0..100] of boolean;
procedure dfs(x,y,z:longint;b:boolean);
        var i,j:longint;
	begin
		if (x=n)and(y=n) then
			begin
				if z<ans then ans:=z;
				exit;
			end;
		if f[x-1,y] and (x>1) then
			begin
				f[x-1,y]:=false;
				if (a[x,y]=3)and(a[x-1,y]>0) then dfs(x-1,y,z,true)
				else if a[x-1,y]=a[x,y] then dfs(x-1,y,z,true)
				else if (a[x-1,y]*a[x,y]<>0)and(a[x-1,y]<>a[x,y]) then dfs(x-1,y,z+1,true)
				else if (a[x-1,y]=0) and b then
					begin
						a[x-1,y]:=3;
						dfs(x-1,y,z+2,false);
					end;
				f[x-1,y]:=true;
			end;
		if f[x+1,y] and (x<n) then
			begin
				f[x+1,y]:=false;
				if (a[x,y]=3)and(a[x+1,y]>0) then dfs(x+1,y,z,true)
				else if a[x+1,y]=a[x,y] then dfs(x+1,y,z,true)
				else if (a[x+1,y]*a[x,y]<>0)and(a[x+1,y]<>a[x,y]) then dfs(x+1,y,z+1,true)
				else if (a[x+1,y]=0) and b then
					begin
						a[x+1,y]:=3;
						dfs(x+1,y,z+2,false);
					end;
				f[x+1,y]:=true;
			end;
		if f[x,y-1] and (y>1) then
			begin
				f[x,y-1]:=false;
				if (a[x,y]=3)and(a[x,y-1]>0) then dfs(x,y-1,z,true)
				else if a[x,y-1]=a[x,y] then dfs(x,y-1,z,true)
				else if (a[x,y-1]*a[x,y]<>0)and(a[x,y-1]<>a[x,y]) then dfs(x,y-1,z+1,true)
				else if (a[x,y-1]=0) and b then
					begin
						a[x,y-1]:=3;
						dfs(x,y-1,z+2,false);
					end;
				f[x,y-1]:=true;
			end;
		if f[x,y+1] and (y<n) then
			begin
				f[x,y+1]:=false;
				if (a[x,y]=3)and(a[x,y+1]>0) then dfs(x,y+1,z,true)
				else if a[x,y+1]=a[x,y] then dfs(x,y+1,z,true)
				else if (a[x,y+1]*a[x,y]<>0)and(a[x,y+1]<>a[x,y]) then dfs(x,y+1,z+1,true)
				else if (a[x,y+1]=0) and b then
					begin
						a[x,y+1]:=3;
						dfs(x,y+1,z+2,false);
					end;
				f[x,y+1]:=true;
			end;
		for i:=1 to n do
			for j:=1 to n do
				if a[i,j]=3 then a[i,j]:=0;
        end;
begin
	assign(input,'chess.in');
	reset(input);
	assign(output,'chess.out');
	rewrite(output);
	ans:=maxlongint;
	readln(n,m);
        fillchar(a,sizeof(a),0);
	for i:=1 to m do
		begin
			readln(x,y,z);
			inc(z);
			a[x,y]:=z;
		end;
	fillchar(f,sizeof(f),true);
	f[1,1]:=false;
	dfs(1,1,0,true);
        if ans=maxlongint then writeln(-1)
	else writeln(ans);
	close(input);
	close(output);
end.

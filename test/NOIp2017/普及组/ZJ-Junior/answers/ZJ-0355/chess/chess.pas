var
  w:array[0..2,0..2]of longint;
  map,f:array[0..100,0..100]of longint;
  i,j,n,m,ans,x,y,z:longint;
function min(a,b:longint):longint;
begin
  if a>b then exit(b) else exit(a);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  read(n,m);
  for i:=1 to m do
  begin
    read(x,y,z);
    map[x,y]:=z+1;
  end;
  w[1,0]:=2; w[2,0]:=2; w[1,2]:=1; w[2,1]:=1;
  f[1,1]:=0;
  for i:=0 to n do
    for j:=0 to n do
      f[i,j]:=10000;
  f[1,1]:=0;
  for i:=1 to n do
  begin
    if i<>1 then f[i,1]:=f[i-1,1]+w[map[i-1,1],map[i,1]];
    if map[i,1]=0 then map[i,1]:=map[i-1,1];
    for j:=2 to n do
    begin
      f[i,j]:=min(f[i-1,j]+w[map[i-1,j],map[i,j]],f[i,j-1]+w[map[i,j-1],map[i,j]]);
      if map[i,j]=0 then
        if f[i,j]=f[i-1,j]+w[map[i-1,j],map[i,j]] then map[i,j]:=map[i-1,j]
        else map[i,j]:=map[i,j-1];
    end;
  end;
  for i:=1 to n do
  begin
    for j:=1 to n do
      write(f[i,j],' ');
    writeln;
  end;
  writeln(-1);
  close(input);
  close(output);
end.
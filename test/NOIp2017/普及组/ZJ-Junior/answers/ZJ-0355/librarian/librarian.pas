var
  m:array[1..1000]of integer;
  st:array[1..1000,0..10]of string;
  k,len,i,j,n,q,x:longint;
  s,min:string;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
  begin
    readln(s);
    len:=length(s);
    st[i,len]:=copy(s,len,1);
    for j:=len-1 downto 1 do
      st[i,j]:=s[j]+st[i,j+1];
    st[i,0]:=s;
    m[i]:=len;
  end;
  for i:=1 to q do
  begin
    readln(s);
    delete(s,1,2);
    len:=length(s);
    min:='9999999999';
    k:=10;
    for j:=1 to n do
      if st[j,m[j]-len+1]=s then
      begin
        if (m[j]<k)or(m[j]=k)and(st[j,0]<min) then begin min:=st[j,0]; k:=m[j]; end;
      end;
    if min='9999999999' then writeln(-1) else writeln(min);
  end;
  close(input);
  close(output);
end.
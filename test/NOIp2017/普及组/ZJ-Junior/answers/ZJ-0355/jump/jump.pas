var
  f,s,w:array[0..500]of longint;
  ans,m,d,ss,k,i,j,max,n:longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  read(n,d,ss);
  k:=0;
  for i:=1 to n do
  begin
    read(s[i],w[i]);
    if w[i]>0 then
    begin
      inc(s,w[i]);
      if s[i]-k-d>max then max:=s[i]-k-d;
      k:=s[i];
    end;
  end;
  if k>=ss then writeln(max) else
  writeln(-1);
  close(input);
  close(output);
end.

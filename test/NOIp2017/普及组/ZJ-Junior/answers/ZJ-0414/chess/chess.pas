const
  zx:array[1..4]of longint=(1,-1,0,0);
  zy:array[1..4]of longint=(0,0,1,-1);
var
  n,m,i,j,q,w,e,ans:longint;
  a:array[-1..1000,-1..1000]of longint;
  c:array[-1..1000,-1..1000,1..2]of longint;
  b:array[-1..1000,-1..1000]of boolean;

procedure try(x,y,z,q:longint);
var
  i:longint;
begin
  if x+y=2*n then
    begin
      if q<ans then
        ans:=q;
      exit;
    end;
  if c[x,y,z]=0 then
    c[x,y,z]:=q else
    if c[x,y,z]<=q then
      exit else
      c[x,y,z]:=q;
  b[x,y]:=true;
  if a[x,y]=0 then
    begin
      for i:=1 to 4 do
        begin
          if b[x+zx[i],y+zy[i]] then
            continue;
          if a[x+zx[i],y+zy[i]]=0 then
            continue;
          if a[x+zx[i],y+zy[i]]=z then
            try(x+zx[i],y+zy[i],z,q);
          if a[x+zx[i],y+zy[i]]<>z then
            try(x+zx[i],y+zy[i],a[x+zx[i],y+zy[i]],q+1);
        end;
    end else
    begin
      for i:=1 to 4 do
        begin
          if b[x+zx[i],y+zy[i]] then
            continue;
          if z=a[x+zx[i],y+zy[i]] then
            try(x+zx[i],y+zy[i],z,q);
          if z+a[x+zx[i],y+zy[i]]=3 then
            try(x+zx[i],y+zy[i],3-z,q+1);
          if a[x+zx[i],y+zy[i]]=0 then
            try(x+zx[i],y+zy[i],z,q+2);
        end;
    end;
  b[x,y]:=false;
end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
    begin
      readln(q,w,e);
      a[q,w]:=e+1;
    end;
  ans:=1000000000;
  try(1,1,a[1,1],0);
  if ans=1000000000 then
    writeln(-1) else
    writeln(ans);
  close(input);close(output);
end.

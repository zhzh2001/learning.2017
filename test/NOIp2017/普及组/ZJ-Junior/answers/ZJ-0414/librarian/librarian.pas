var
  n,m,i,j,k,x,y,z,ans:longint;
  a:array[0..20000]of longint;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to m do
    begin
      readln(x,y);
      ans:=-1;
      for j:=1 to n do
        begin
          z:=1;
          for k:=1 to x do
            z:=z*10;
          if a[j] mod z=y then
            if (a[j]<ans)or(ans=-1) then
              ans:=a[j];
        end;
      writeln(ans);
    end;
  close(input);close(output);
end.
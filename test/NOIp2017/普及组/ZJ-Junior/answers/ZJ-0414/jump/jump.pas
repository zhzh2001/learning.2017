var
  n,d,max,i,j,l,r,mid,midl,midr,ll,ans:longint;
  a,b,f:array[0..1000000]of longint;
  t:boolean;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  t:=false;
  readln(n,d,max);
  inc(n);
  for i:=2 to n do
    begin
      read(a[i],b[i]);
    end;
  a[1]:=0;b[1]:=0;
  l:=0;
  r:=a[n]+d;
  while l<r do
    begin
      mid:=(l+r)div 2;
      midl:=d-mid;
      midr:=mid+d;
      if midl<=0 then
        midl:=1;
      ll:=1;
      f[1]:=0;
      ans:=0;
      for i:=2 to n do
        begin
          f[i]:=-100000000;
          if a[i]-a[ll]<midl then
            continue;
          while a[i]-a[ll]>midr do
            inc(ll);
          if a[i]-a[ll]<midl then
            continue;
          for j:=ll to i do
            begin
              if a[i]-a[j]<midl then
                break;
              if f[j]+b[i]>f[i] then
                f[i]:=f[j]+b[i];
              if f[i]>ans then
                ans:=f[i];
            end;
        end;
      if ans<max then
        l:=mid+1 else
        begin
          r:=mid;
          t:=true;
        end;
    end;
  if t then
    writeln(r) else
    writeln(-1);
  close(input);close(output);
end.
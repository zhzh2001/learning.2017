var a:array[0..101,0..101] of longint;
f:array[0..101,0..101] of boolean;
n,m,i,max,x,y,z:longint;
procedure sc(x,y,s:longint);
begin
if (x<1) or (x>n) or (y<1) or (y>n) or (f[x,y]) then exit;
if s>=max then exit;
f[x,y]:=true;
if (x=n) and (y=n) then
  begin
  if s<max then max:=s;
  exit;
  end;
if (a[x-1,y]=0) and (a[x,y]<>0) then sc(x-1,y,s+2)
else if ((a[x-1,y]=a[x,y]) and (a[x,y]<>0) and (a[x-1,y]<>0)) or (a[x,y]=0) and (a[x-1,y]<>0) then sc(x-1,y,s)
else if (a[x,y]<>0) and (a[x-1,y]<>0) then sc(x-1,y,s+1);
if (a[x+1,y]=0) and (a[x,y]<>0) then sc(x+1,y,s+2)
else if ((a[x+1,y]=a[x,y]) and (a[x,y]<>0) and (a[x+1,y]<>0)) or (a[x,y]=0) and (a[x+1,y]<>0) then sc(x+1,y,s)
else if (a[x,y]<>0) and (a[x+1,y]<>0) then sc(x+1,y,s+1);
if (a[x,y-1]=0 ) and (a[x,y]<>0) then sc(x,y-1,s+2)
else if ((a[x,y-1]=a[x,y]) and (a[x,y]<>0) and (a[x,y-1]<>0)) or (a[x,y]=0) and (a[x,y-1]<>0) then sc(x,y-1,s)
else if (a[x,y]<>0) and (a[x,y-1]<>0) then sc(x,y-1,s+1);
if (a[x,y+1]=0) and (a[x,y]<>0) then sc(x,y+1,s+2)
else if ((a[x,y+1]=a[x,y]) and (a[x,y]<>0) and (a[x,y+1]<>0)) or (a[x,y]=0) and (a[x,y+1]<>0) then sc(x,y+1,s)
else if (a[x,y]<>0) and (a[x,y+1]<>0) then sc(x,y+1,s+1);
f[x,y]:=false;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
for i:=1 to m do
  begin
  readln(x,y,z);
  a[x,y]:=z+1;
  end;
max:=maxlongint;
sc(1,1,0);
if max=maxlongint then writeln(-1)
else writeln(max);
close(input);close(output);
end.

var
m,n,i,j,l,r,x1,y1,z:longint;
a,f:array[0..101,0..101] of longint;
c,d,g,h:array[0..1000001] of longint;
b:array[0..101,0..101] of boolean;
x,y:array[1..4] of longint;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
readln(m,n);
fillchar(a,sizeof(a),0);
fillchar(b,sizeof(b),false);
for i:=1 to n do
begin
readln(x1,y1,z);
a[x1,y1]:=z+1;
b[x1,y1]:=true;
end;
x[1]:=-1;y[1]:=0;
x[2]:=0;y[2]:=-1;
x[3]:=1;y[3]:=0;
x[4]:=0;y[4]:=1;
for i:=1 to m do
for j:=1 to m do
f[i,j]:=maxlongint;
f[1,1]:=0;
l:=1;
r:=1;
c[1]:=1;
d[1]:=1;
g[1]:=a[1,1];
h[1]:=0;
while l<=r do
begin
for i:=1 to 4 do
if (c[l]+x[i]>=1) and (c[l]+x[i]<=m) and (d[l]+y[i]>=1) and (d[l]+y[i]<=m) then
begin
if (b[c[l],d[l]]) and (b[c[l]+x[i],d[l]+y[i]]=false) then
begin
if f[c[l]+x[i],d[l]+y[i]]>h[l]+3 then
begin
r:=r+1;
c[r]:=c[l]+x[i];
d[r]:=d[l]+y[i];
g[r]:=2 div g[l];
h[r]:=h[l]+3;
f[c[l]+x[i],d[l]+y[i]]:=h[l]+3;
end;
if f[c[l]+x[i],d[l]+y[i]]>h[l]+2 then
begin
r:=r+1;
c[r]:=c[l]+x[i];
d[r]:=d[l]+y[i];
g[r]:=g[l];
h[r]:=h[l]+2;
f[c[l]+x[i],d[l]+y[i]]:=h[l]+2;
end;
end
else if (b[c[l],d[l]]) and (b[c[l]+x[i],d[l]+y[i]]) then
begin
if (a[c[l],d[l]]=a[c[l]+x[i],d[l]+y[i]]) and (f[c[l]+x[i],d[l]+y[i]]>h[l]) then
begin
r:=r+1;
c[r]:=c[l]+x[i];
d[r]:=d[l]+y[i];
g[r]:=g[l];
h[r]:=h[l];
f[c[l]+x[i],d[l]+y[i]]:=h[l];
end
else if (a[c[l],d[l]]<>a[c[l]+x[i],d[l]+y[i]]) and (f[c[l]+x[i],d[l]+y[i]]>h[l]+1) then
begin
r:=r+1;
c[r]:=c[l]+x[i];
d[r]:=d[l]+y[i];
g[r]:=2 div g[l];
h[r]:=h[l]+1;
f[c[l]+x[i],d[l]+y[i]]:=h[l]+1;
end;
end
else if (b[c[l],d[l]]=false) and (b[c[l]+x[i],d[l]+y[i]]) then
begin
if (g[l]=a[c[l]+x[i],d[l]+y[i]]) and (f[c[l]+x[i],d[l]+y[i]]>h[l]) then
begin
r:=r+1;
c[r]:=c[l]+x[i];
d[r]:=d[l]+y[i];
g[r]:=g[l];
h[r]:=h[l];
f[c[l]+x[i],d[l]+y[i]]:=h[l];
end
else if (g[l]<>a[c[l]+x[i],d[l]+y[i]]) and (f[c[l]+x[i],d[l]+y[i]]>h[l]+1) then
begin
r:=r+1;
c[r]:=c[l]+x[i];
d[r]:=d[l]+y[i];
g[r]:=2 div g[l];
h[r]:=h[l]+1;
f[c[l]+x[i],d[l]+y[i]]:=h[l]+1;
end;
end;
end;
l:=l+1;
end;
if f[m,m]=maxlongint
then
writeln(-1)
else writeln(f[m,m]);
close(input);
close(output);
end.




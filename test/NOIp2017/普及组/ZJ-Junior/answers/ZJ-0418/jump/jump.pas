var
n,d,k,max,sum1,sum2,i,mid,l,r:longint;
a,b,f:array[0..500001] of longint;
function mmax(x,y:longint):longint;
begin
if x>y then mmax:=x
else mmax:=y;
end;
function try(t:longint):boolean;
var
i,j,x:longint;
begin
try:=false;
for i:=1 to n do
f[i]:=-maxlongint;
f[0]:=0;
x:=mmax(d-t,1);
for i:=1 to n do
for j:=0 to i-1 do
if f[j]<>-maxlongint then
if (a[j]+x<=a[i]) and (a[j]+d+t>=a[i]) and (f[j]+b[i]>f[i]) then
begin
f[i]:=f[j]+b[i];
if f[i]>=k then exit(true);
end;
end;
begin
assign(input,'jump.in');
assign(output,'jump.out');
reset(input);
rewrite(output);
readln(n,d,k);
max:=0;
sum1:=0;
sum2:=0;
a[0]:=0;b[0]:=0;
for i:=1 to n do
begin
readln(a[i],b[i]);
if abs(b[i]-d)>max then max:=abs(b[i]-d);
if i mod d=0 then sum1:=sum1+b[i];
if b[i]>0 then sum2:=sum2+b[i];
end;
if sum1>=k then
begin
writeln(0);
close(input);
close(output);
halt;
end;
if sum2<k then
begin
writeln(-1);
close(input);
close(output);
halt;
end;
l:=1;
r:=max;
while l<r do
begin
mid:=(l+r) div 2;
if try(mid) then
r:=mid
else l:=mid+1;
end;
writeln(l);
close(input);
close(output);
end.

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int qp[110][110],m,n,x,y,c;
int ans=9999999;
bool js=false;
void i_search(int x,int y,int z,int used)
{
	if(x==m&&y==m)
	{	
		ans=z;
		js=true;
		return;
	}
	if(x<1||y<1||x>m||y>m)return;
	if(qp[x][y+1]==qp[x][y])i_search(x,y+1,z,0);if(js==true)return;
	if(qp[x+1][y]==qp[x][y])i_search(x+1,y,z,0);if(js==true)return;
	if(qp[x][y+1]!=qp[x][y]&&qp[x][y+1]!=-1)i_search(x,y+1,z+1,0);if(js==true)return;
	if(qp[x+1][y]!=qp[x][y]&&qp[x+1][y]!=-1)i_search(x+1,y,z+1,0);if(js==true)return;
	if(qp[x+1][y]==-1&&used==1)return;if(js==true)return;
	if(qp[x][y+1]==-1&&used==1)return;if(js==true)return;
	if(qp[x][y+1]==-1&&used==0)
	{
		qp[x][y+1]=qp[x][y];
		i_search(x,y+1,z+2,1);
		qp[x][y+1]=-1;
	}if(js==true)return;
	if(qp[x+1][y]==-1&&used==0)
	{
		qp[x+1][y]=qp[x][y];
		i_search(x+1,y,z+2,1);
		qp[x+1][y]=-1;
	}if(js==true)return;
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(qp,-1,sizeof(qp));
	scanf("%d %d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d %d %d",&x,&y,&c);
		qp[x][y]=c;
	}
	i_search(1,1,0,0);
	if(ans==9999999)cout<<"-1";
	else printf("%d",ans);
	return 0;
}

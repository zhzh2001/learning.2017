#include <cstdio>
#include <vector>
#define  MAX_N    (10010)
#define  INF    (0x7f7f7f7f)
#define  hash(i,j)    ((i - 1) * m + j)
using namespace std;

struct Node{
	int x,y,w,isw;
};
const int plus_x[] = {0,0,0,1,-1};
const int plus_y[] = {0,1,-1,0,0};
int m,n,x,y,c,ans;
int a[110][110];
int b[110][110];
int h,t;
Node q[100010];
inline void push(int x, int y, int w, int ISW){
	if(x < 1 || x > m || y < 1 || y > m) return;
	if(b[x][y]) return;
//	printf("h = %d  t = %d  x = %d  y = %d  w = %d\n", h,t,x,y,w);
	q[++t].x = x;
	q[t].y = y;
	q[t].w = w;
	q[t].isw = ISW;
	b[x][y] = 1;
	if(x == m && y == m){
		if(w < ans) ans = w;
	}
}
inline void bfs(){
	h = t = 0;
	push(1,1,0,0);
	while(h <= t){
		h++;
		for(int i = 1; i <= 4; ++i){
			int x_to = q[h].x + plus_x[i];
			int y_to = q[h].y + plus_y[i];
//			printf("%d, %d  ->  %d, %d\n",q[h].x, q[h].y,x_to, y_to);
			if(a[x][y] == a[x_to][y_to] && a[x][y] > 0) push(x_to, y_to, q[h].w,0);
			else if(a[x][y] != a[x_to][y_to] && a[x][y] > 0 && a[x_to][y_to] > 0) push(x_to, y_to, q[h].w + 1,0);
			else if(a[x_to][y_to] == 0 && q[h].isw == 0) push(x_to, y_to, q[h].w + 2,1);
		}
	}
}
int main(){
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	ans = INF;
	scanf("%d%d", &m, &n);
	for(int i = 0; i < n; ++i){
		scanf("%d%d%d", &x, &y, &c);
		a[x][y] = c + 1;
	}
	bfs();
	if(ans == INF) printf("-1");
	else printf("%d", ans + 1);
	return 0;
}

#include <cstdio>
#include <algorithm>
#define  MAX_N    (500010)
#define  INF    (0x7f7f7f7f)
using namespace std;

int n,d,k,ans,L,R,Mid;
int x[MAX_N], a[MAX_N];
int f[MAX_N];
inline bool Judge(int g){
//	printf("g = %d\n", g);
	bool cant = 0;
	if(g < d) cant = 1;
	if(d + g < x[1]) return 0;
	if(cant) { if(d - g > x[1]) return 0; }
	for(int i = 1; i <= n; ++i) f[i] = 0;
	f[1] = a[1];
	int __result = f[1];
	for(int i = 2; i <= n; ++i){
		for(int j = 1; j < i; ++j){
			if(x[i] - x[j] > d + g) continue;
			if(x[i] - x[j] == 0) continue;
			if(cant){ if(x[i] - x[j] < d - g) continue; }	
		//	printf("i = %d  j = %d   x[i] - x[j] = %d\n",i,j,x[i]-x[j]);	
			f[i] = max(f[i], f[j] + a[i]);
		}
	//	printf("f[%d] = %d\n",i,f[i]);
		__result = max(__result, f[i]);
	}
//	printf("result = %d\n", __result);
	return (__result >= k);
}
int main(){
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	scanf("%d%d%d", &n, &d, &k);
	for(int i = 1; i <= n; ++i){
		scanf("%d%d", &x[i], &a[i]);
	}
	ans = INF;
	L = 0, R = 0x3f3f3f3f;
	while(L < R){
		Mid = (L + R) >> 1;
		if(Judge(Mid)){
			R = Mid;
			ans = Mid;
		}else{
			L = Mid + 1;
		}
	}
	if(ans == INF) printf("-1");
	else printf("%d", ans);
	return 0;
}

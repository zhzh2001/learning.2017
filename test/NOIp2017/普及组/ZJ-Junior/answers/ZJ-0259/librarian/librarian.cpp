#include <cstdio>
#include <algorithm>
#define  MAX_N    (1010)
using namespace std;

int n,q,X,Y;
int a[MAX_N];
int len[MAX_N];
inline int __check(int x){
	if(x < 10) return 1;
	if(x < 100) return 2;
	if(x < 1000) return 3;
	if(x < 10000) return 4;
	if(x < 100000) return 5;
	if(x < 1000000) return 6;
	if(x < 10000000) return 7;
	if(x < 100000000) return 8;
	if(x < 1000000000) return 9;
}
inline int ten_power(int x){
	int __result = 1;
	while(x--) __result *= 10;
	return __result;
}
inline void __solve(int x, int y){
	int __num = ten_power(x);
	for(int i = 1; i <= n; ++i){
		if(a[i] % __num == y){
			printf("%d\n", a[i]);
			return;
		}
	}
	printf("-1\n");
}
int main(){
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	scanf("%d%d", &n, &q);
	for(int i = 1; i <= n; ++i){
		scanf("%d", &a[i]);
	} 
	sort(a+1, a+n+1);
	for(int i = 1; i <= n; ++i) len[i] = __check(a[i]);
	while(q--){
		scanf("%d%d", &X, &Y);
		__solve(X,Y);
	}	
	return 0;
}

#include<cstdio>
#include<algorithm>
#define maxn 1005
using namespace std;
int n,q;
struct book{
	int x;
	int num[15];
	void work(){
		int tmp=x;
		while(tmp){
			num[++num[0]]=tmp%10;
			tmp/=10;
		}
	}
	bool cmp(int len,int x){
		for(int i=1;i<=len;i++){
			if(x%10!=num[i]) return 0;
			x/=10;
		}
		return 1;
	}
	bool operator <(const book &b)const{return x<b.x;}
}a[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int find_(int len,int x){
	for(int i=1;i<=n;i++){
		if(a[i].cmp(len,x)){
			return a[i].x;
		}
	}
	return -1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++) a[i].x=read(),a[i].work();
	sort(a+1,a+1+n);
	for(int i=1,a,b;i<=q;i++){
		a=read(),b=read();
		printf("%d\n",find_(a,b));
	}
	return 0;
}

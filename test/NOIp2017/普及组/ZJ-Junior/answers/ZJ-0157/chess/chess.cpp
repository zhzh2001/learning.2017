#include<cstdio>
#include<cstring>
#define maxn 1005
using namespace std;
int m,n,mp[maxn][maxn],dis[maxn][maxn];
bool vis[maxn][maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
const int maxQ=maxn*maxn;
const int flg[4][2]={{0,1},{1,0},{0,-1},{-1,0}};
bool check(int x,int y){
	if(x<1||y<1||x>m||y>m) return 0;
	return 1;
}
struct spfa_q{
	int x,y,lst_c;
	bool used;
	spfa_q(){x=y=lst_c=0;used=false;}
	spfa_q(int a,int b,int c,bool useded){x=a,y=b,lst_c=c;used=useded;}
}Q[maxQ];
void spfa(){
	int hed=0,til=1;
	Q[til]=(spfa_q){1,1,mp[1][1],false};
	dis[1][1]=0;
	while(hed!=til){
		spfa_q xx=Q[++hed%=maxQ];
		vis[xx.x][xx.y]=0;
		for(int k=0;k<4;k++){
			int sx=xx.x+flg[k][0],sy=xx.y+flg[k][1];
			bool isuse=0;
			if(!check(sx,sy)) continue; // shi fou yue jie 
			int w=0;
			if(mp[sx][sy]!=xx.lst_c) w=1; // ruguoyushangyicibuyiyang
			if(mp[sx][sy]==-1) w=2,isuse=1; // zhe ci ruguo xuyao yong 
			if(mp[sx][sy]==-1&&xx.used) continue; // shang ci yong le zhe ci ye yong le 
			if(dis[sx][sy]>dis[xx.x][xx.y]+w){
				dis[sx][sy]=dis[xx.x][xx.y]+w;
				if(!vis[sx][sy]) Q[++til%=maxQ]=(spfa_q){sx,sy,mp[sx][sy]!=-1?mp[sx][sy]:xx.lst_c,isuse},vis[sx][sy]=1;
			}
		}
	}
}
int main(){
	memset(mp,255,sizeof(mp));
	memset(dis,63,sizeof(dis));
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read();n=read();
	for(int i=1;i<=n;i++){
		int x=read(),y=read(),c=read();
		mp[x][y]=c;
	}
	spfa();
	printf("%d\n",dis[m][m]==0x3f3f3f3f?-1:dis[m][m]);
	return 0;
}

#include<cstdio>
#include<algorithm>
#define maxn 500005
using namespace std;
int n,d,k,ans=-2333;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
struct jcy{
	int x,f;
}a[maxn];
int get(int &j,int x,int now){
	while(a[j].x<=now+d-x&&j<=n) j++;
	int ret=-(1<<30),rj;
	for(int i=j;i<=n&&a[i].x<=now+d+x;i++){
		if(a[i].f>ret) rj=i;
		ret=max(ret,a[i].f);
		if(ret>0){j=i;return ret;}
	}
	if(ret==-(1<<30)) {j=n+1;return 0;}
	j=rj;return ret;
}
bool check(int x){
	int i=1,j,max_s=1<<30,ret=a[1].f;
	while(i<=n){
		j=i+1;
		max_s=get(j,x,a[i].x);
		ret+=max_s;
		i=j;
	}
	return ret>=k;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();k=read();
	for(int i=1;i<=n;i++){
		a[i].x=read();
		a[i].f=read();
	}
	check(86);
	int L=1,R=a[n].x,mid;
	while(L<=R){
		mid=(R-L>>1)+L;
		if(check(mid)) ans=mid,R=mid-1;
		else L=mid+1;
	}
	if(ans==-2333) {printf("-1\n");return 0;}
	printf("%d\n",ans);
	return 0;
}

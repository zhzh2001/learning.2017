#include <cstdio>
#include <cstring>
struct node{
	int x,y,jb,zz;
	bool lst;
}q[1000001];
int t = 1,w = 1;
int n,m,ans = -1;
bool hv = 0;
int a[111][111];
int px[4] = {0,1,0,-1};
int py[4] = {1,0,-1,0};
int used[111][111];

void p(node x){
	if(used[x.x][x.y] == 0){
			used[x.x][x.y] = 1;
			q[++w] = x;
		}
}

void bfs(){
	node no,nn;
	int xx,yy;
	while(t <= w){
		no = q[t++];
//		used[no.x][no.y] = 0;
//		used[no.x][no.y] = 0x7ffffff;
		if(no.x == n && no.y == n){
//			printf("%d\n",no.jb);
			if(!hv ||  no.jb< ans){
				ans = no.jb;
				hv = 1;
			}
		}
		for(int i = 0;i <= 3;i++){
			xx = nn.x = no.x + px[i];
			yy = nn.y = no.y + py[i];
			
//			if(used[nn.x][nn.y])continue;
			if(xx < 1 || xx > n || yy < 1 || yy > n){
				continue;
			}
			
			if(no.lst){
				if(a[xx][yy] == 0){
					continue;
				}
				
				else{
					if(a[xx][yy] == no.zz){
						nn.lst = 0;
						nn.jb = no.jb;
						p(nn);
					}
					else{
						nn.lst = 0;
						nn.jb = no.jb + 1;
						
						p(nn);
					}
				}
			}
			else{
				if(a[xx][yy] > 0){
					if(a[xx][yy] == a[no.x][no.y]){
						nn.lst = 0;
						nn.jb = no.jb;
						
						p(nn);
					}
					else{
						nn.lst = 0;
						nn.jb = no.jb + 1;
						
						p(nn);
					}
				}
				else{
					for(int j = 1;j <= 2;j++){
						nn.zz = j;
						nn.lst = 1;
						nn.jb = no.jb + 2;
						
						p(nn);
					}	
				}
			}
		}
	}
}


int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,0,sizeof a);
	memset(used,0,sizeof used);
	scanf("%d %d",&n,&m);
	int t1,t2,x;
	for(int i = 1;i <= m;i++){
		scanf("%d %d %d",&t1,&t2,&x);
		a[t1][t2] = x + 1;
	}
	node beg;
	beg.x = beg.y = 1;
	beg.jb = 0;
	beg.lst = 0;
	q[1] = beg;
	bfs();
	printf("%d",ans);
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

#include <cstdio>
#include <cstring>
struct node{
	int x,y,jb;
	bool lst;
}q[1000001];
int t = 1,w = 1;
int n,m,ans;
bool hv = 0;
int a[111][111];
int px[4] = {0,1,0,-1};
int py[4] = {1,0,-1,0};
bool used[111][111];

/*
void bfs(){
	node Now,nn;
	while(t <= w){
		Now = q[t++];
		for(int i = 0;i <= 3;i++){
			nn.x = Now.x + px[i];
			nn.y = Now.y + py[i];
			if(a[nn.x][nn.y] = a[Now.x][Now.y]){
				nn.jb = Now.jb;
				nn.lst = 0;
				q[++w] = nn;
			}
			else{
				if(a[nn.x][nn.y] > 0){
					nn.jb = Now.jb + 1;
					nn.lst = 0;
					q[++w] = nn;
				}
				else{
					
				}
			}
		}
	}
}*/

void dfs(int x,int y,bool no,int v){
//	printf("%d %d %d %d\n",x,y,no,v);
	if(x == n && y == n){
		if(!hv || v < ans){
			ans = v;
			hv = 1;
		}
		return;
	}
	int xx,yy;
	used[x][y] = 1;
	for(int i = 0;i <= 3;i++){
		xx = x + px[i];
		yy = y + py[i];
		if(xx < 1 || xx > n || yy < 1 || yy > n){
			continue;
		}
		if(used[xx][yy]){
			continue;
		}
		
		if(no){
			//a[x][y] = 0
			if(a[xx][yy] > 0){
				if(a[xx][yy] == a[x][y]){
//					used[xx][yy] = 1;
					dfs(xx,yy,0,v);
//					used[xx][yy] = 0;
				}
				else{
//					used[xx][yy] = 1;
					dfs(xx,yy,0,v + 1);
//					used[xx][yy] = 0;
				}
			}
		}
		else{
			if(a[xx][yy] > 0){
				if(a[xx][yy] == a[x][y]){
//					used[xx][yy] = 1;
					dfs(xx,yy,0,v);
//					used[xx][yy] = 0;
				}
				else{
//					used[xx][yy] = 1;
					dfs(xx,yy,0,v + 1);
//					used[xx][yy] = 0;
				}
			}
			else{
				for(int j = 1;j <= 2;j++){
					a[xx][yy] = j;
//					used[xx][yy] = 1;
					dfs(xx,yy,1,v + 2);
//					used[xx][yy] = 0;
					a[xx][yy] = 0;
					
				}
			}
		}
	}
//	used[x][y] = 0;
}

int main()
{
	freopen("1.in","r",stdin);
//	freopen("chess.out","w",stdout);
	memset(a,0,sizeof a);
	memset(used,0,sizeof used);
	scanf("%d %d",&n,&m);
	int t1,t2,x;
	for(int i = 1;i <= m;i++){
		scanf("%d %d %d",&t1,&t2,&x);
		a[t1][t2] = x + 1;
	}
	/*
	node beg;
	beg.x = beg.y = 1;
	beg.jb = 0;
	beg.lst = 0;
	q[1] = beg;
	bfs();*/
//	used[1][1] = 1;
	dfs(1,1,0,0);
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

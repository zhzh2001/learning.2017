#include <cstdio>
#include <algorithm>
using namespace std;
int mo = 1;
int poow[13];
int n,m,a[100011];
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	poow[0] = 1;
	for(int i = 1;i <= 9;i++){
		poow[i] = poow[i - 1] * 10;
	}
	scanf("%d %d",&n,&m);
	for(int i = 1;i <= n;i++){
		scanf("%d",&a[i]);
	}
	sort(a + 1,a + 1 + n);
	int x,tmp;
	bool hv;
	for(int i = 1;i <= m;i++){
		scanf("%d %d",&x,&tmp);
		hv = 0;
		for(int j = 1;j <= n;j++){
			if(a[j] % poow[x] == tmp){
				printf("%d\n",a[j]);
				hv = 1;
				break;
			}
		}
		if(!hv){
			printf("-1\n");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

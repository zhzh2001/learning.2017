#include<iostream>
#include<cstdio>
#include<algorithm>
#define maxn 1005
using namespace std;
int n,Q,a[maxn];
int red()
{
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
bool check(int x,int y)
{
	while (y)
	{
		int p=x%10,q=y%10;
		if (p^q) return 0;
		x/=10;y/=10;
	}
	return 1;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&Q);
	for (int i=1;i<=n;i++) a[i]=red();
	sort(a+1,a+n+1);
	for (int t=1;t<=Q;t++)
	{
		int x=red(),i;bool flg=0;
		x=red();
		for (i=1;i<=n;i++) if (check(a[i],x))
		{
			flg=1;break;
		}
		if (!flg) printf("-1\n");
		else printf("%d\n",a[i]);
	}
	return 0;
}

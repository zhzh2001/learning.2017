#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#define maxn 105
#define INF 0x3f3f3f3f
using namespace std;
int n,m,a[maxn][maxn],f[maxn][maxn][2];
int red()
{
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(f,63,sizeof(f));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	  for (int j=1;j<=n;j++) a[i][j]=2;
	for (int i=1;i<=m;i++)
	{
		int x=red(),y=red(),z=red();
		a[x][y]=z;
	}
	f[1][1][a[1][1]]=0;
	for (int i=1;i<=n;i++)
	  for (int j=1;j<=n;j++)
	  {
		if (i==1&&j==1) continue;
		if (a[i][j]==2)
		{
			if (a[i-1][j]^2)
			{
				f[i][j][0]=min(f[i][j][0],f[i-1][j][a[i-1][j]]+2+(a[i-1][j]!=0));
				f[i][j][1]=min(f[i][j][1],f[i-1][j][a[i-1][j]]+2+(a[i-1][j]!=1));
			}
			if (a[i][j-1]^2)
			{
				f[i][j][0]=min(f[i][j][0],f[i][j-1][a[i][j-1]]+2+(a[i][j-1]!=0));
				f[i][j][1]=min(f[i][j][1],f[i][j-1][a[i][j-1]]+2+(a[i][j-1]!=1));
			}
		}
		else
		{
			f[i][j][a[i][j]]=min(f[i][j][a[i][j]],f[i-1][j][0]+(a[i][j]!=0));
			f[i][j][a[i][j]]=min(f[i][j][a[i][j]],f[i-1][j][1]+(a[i][j]!=1));
			f[i][j][a[i][j]]=min(f[i][j][a[i][j]],f[i][j-1][0]+(a[i][j]!=0));
			f[i][j][a[i][j]]=min(f[i][j][a[i][j]],f[i][j-1][1]+(a[i][j]!=1));
		}
	  }
	if (f[n][n][0]==INF&&f[n][n][1]==INF) puts("-1");
	else printf("%d\n",min(f[n][n][0],f[n][n][1]));
	return 0;
}

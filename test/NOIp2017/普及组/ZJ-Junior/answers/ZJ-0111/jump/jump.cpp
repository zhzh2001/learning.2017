#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#define maxn 500005
#define maxl 29884450
using namespace std;
typedef long long LL;
int n,d,k,ans,a[maxl],f[maxn];
struct abc{
	int x,s;
}b[maxn];
int red()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
bool chck()
{
	LL tot=0;
	for (int i=1;i<=n;i++) if (b[i].s>0) tot+=b[i].s;
	return tot<k;
}
bool check(int x)
{
	memset(f,128,sizeof(f));f[0]=0;
	int l=max(1,d-x),r=d+x;
	for (int i=0;i+l<=b[n].x;i++)
	{
		for (int j=i+l,tj=min(i+r,b[n].x);j<=tj;j++)
		  if (a[j]!=-2139062144&&f[i]+a[j]>f[j]) f[j]=f[i]+a[j];
	}
	return f[b[n].x]>=k;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	memset(a,128,sizeof(a));
	scanf("%d%d%d",&n,&d,&k);
	for (int i=1;i<=n;i++)
	  b[i]=(abc){red(),red()},a[b[i].x]=b[i].s;
	if (chck()){puts("-1");return 0;}
	int L=0,R=b[n].x,mid;
	while (L<=R)
	{
		mid=(R-L>>1)+L;
		if (check(mid)) ans=mid,R=mid-1;
		else L=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

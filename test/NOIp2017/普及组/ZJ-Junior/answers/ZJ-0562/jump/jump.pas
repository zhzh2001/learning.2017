var n,m,k,i,l,r,mid,ans:longint;
    x,y:array[0..5000001] of longint;
function check(t:longint):boolean;
var i,j,minx,maxx,s,dx,ansj:longint;
begin
  s:=0;dx:=0;
  if t=0 then
   begin
    minx:=m;
    maxx:=m;
   end
  else
   begin
    if t<m then
     minx:=m-t
    else
     minx:=1;
    maxx:=m+t;
   end;
  i:=0;y[0]:=-maxlongint;
  while i<=n do
   begin
    ansj:=0;
    for j:=i+1 to n do
     if x[j]>dx+maxx then
      break
     else
      if (dx+minx<=x[j]) and (x[j]<=dx+maxx) then
       begin
        if y[j]>0 then
         begin
          ansj:=j;
          break;
         end;
        if y[j]>y[ansj] then
         ansj:=j;
       end;
    if ansj=0 then
     exit(false);
    inc(s,y[ansj]);
    if s>=k then
     exit(true);
    i:=ansj;
    dx:=x[ansj];
   end;
end;
begin
 assign(input,'jump.in');reset(input);
 assign(output,'jump.out');rewrite(output);
  read(n,m,k);
  for i:=1 to n do
   begin
    read(x[i],y[i]);
    if x[i]>r then
     r:=x[i];
   end;
  l:=0;ans:=-1;
  while l<=r do
   begin
    mid:=(l+r) div 2;
    if check(mid) then
     begin
      ans:=mid;
      r:=mid-1;
     end
    else
     l:=mid+1;
   end;
  writeln(ans);
 close(input);
 close(output);
end.

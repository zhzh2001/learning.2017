var
  a:array[1..10000] of longint;
  n,m,i,j,k,x,y:longint;
  b:boolean;
procedure sort(l,r: longint);
  var
    i,j,x,y: longint;
  begin
    i:=l;
    j:=r;
    x:=a[(l+r) div 2];
    repeat
      while a[i]<x do inc(i);
      while x<a[j] do dec(j);
      if not(i>j) then
        begin
          y:=a[i];
          a[i]:=a[j];
          a[j]:=y;
          inc(i);
          j:=j-1;
        end;
      until i>j;
      if l<j then sort(l,j);
      if i<r then sort(i,r);
  end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    read(a[i]);
  sort(1,n);
  for i:=1 to m do
    begin
      readln(x,y);
      k:=1;
      b:=true;
      for j:=1 to x do
        k:=k*10;
      for j:=1 to n do
        if a[j] mod k=y then
          begin
            writeln(a[j]);
            b:=false;
            break;
          end;
      if b then writeln(-1);
    end;
  close(input);
  close(output);
end.

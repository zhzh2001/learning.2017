var
  a,b,c:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  readln(a,b,c);
  writeln((a*20+b*30+c*50)div 100);
  close(input);
  close(output);
end.

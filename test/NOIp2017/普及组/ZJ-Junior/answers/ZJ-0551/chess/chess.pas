var
  n,m,i,j,l,r,z,s1,s2,xl,xh:longint;
  a,b:array[0..105,0..105] of longint;
  x,y:array[1..100000] of longint;
  dx:array[1..4] of longint=(0,0,1,-1);
  dy:array[1..4] of longint=(1,-1,0,0);
procedure dfs(h,l,k:longint);
  var
    xx,yy,ii:longint;
  begin
    for ii:=1 to 4 do
      begin
        xx:=h+dx[ii];
        yy:=l+dy[ii];
        if (xx>0)and(yy>0)and(xx<=n)and(yy<=n)
           and(b[xx,yy]>k)and(a[xx,yy]=a[h,l]) then
           begin
             b[xx,yy]:=k;
             inc(r);
             x[r]:=xx;
             y[r]:=yy;
             dfs(xx,yy,k);
           end;
      end;
  end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  fillchar(a,sizeof(a),255);
  for i:=1 to m do
    begin
      read(s1,s2,z);
      a[s1,s2]:=z;
    end;
  for i:=1 to n do
    for j:=1 to n do
      b[i,j]:=maxlongint;
  l:=1;
  r:=1;
  x[1]:=1;
  y[1]:=1;
  b[1,1]:=0;
  while l<=r do
    begin
      if a[x[l],y[l]]>-1 then dfs(x[l],y[l],b[x[l],y[l]]);
      for i:=1 to 4 do
        begin
          xh:=x[l]+dx[i];
          xl:=y[l]+dy[i];
          if (xh>0)and(xh<=n)and(xl>0)and(xl<=n)and
          ( ((b[x[l],y[l]]+1<b[xh,xl])and(a[xh,xl]>-1)) or
            ((b[x[l],y[l]]+2<b[xh,xl])and(a[x[l],y[l]]>-1)) ) then
            begin
              inc(r);
              x[r]:=xh;
              y[r]:=xl;
              if a[xh,xl]=-1
              then
                begin
                  b[xh,xl]:=b[x[l],y[l]]+2;
                  a[xh,xl]:=a[x[l],y[l]];
                  dfs(xh,xl,b[xh,xl]);
                  a[xh,xl]:=-1;
                end
              else
                begin
                  b[xh,xl]:=b[x[l],y[l]]+1;
                  dfs(xh,xl,b[xh,xl]);
                end;
            end;
        end;
      inc(l);
    end;
  if b[n,n]=maxlongint then writeln(-1)
                       else writeln(b[n,n]);
  close(input);
  close(output);
end.

var
  n,d,k,m,i,j,mid,l,r:longint;
  a,b,f:array[0..500003] of longint;
function max(x,y:longint):longint;
  begin
    if x>y then exit(x);
    exit(y);
  end;
function pd(t:longint):boolean;
  begin
    for i:=1 to n do
      begin
        f[i]:=0;
        for j:=1 to i-1 do
          if (a[i]-a[j]<d+t)and(a[i]-a[j]>d-t)
            then f[i]:=max(f[i],f[j]);
        f[i]:=f[i]+b[i];
        if f[i]>k then exit(true);
      end;
    exit(false);
  end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    begin
      readln(a[i],b[i]);
      if b[i]>0 then m:=m+b[i];
    end;
  if m<k then
    begin
      writeln(-1);
      close(input);
      close(output);
      exit;
    end;
  l:=0;
  r:=1000000000;
  while l<r do
    begin
      mid:=(l+r) div 2;
      if pd(mid) then r:=mid
                 else l:=mid+1;
    end;
  writeln(r);
  close(input);
  close(output);
end.
const
  dx:array[1..4] of longint=(0,1,0,-1);
  dy:array[1..4] of longint=(1,0,-1,0);

var
  min,ans,i,n,m,x,y,c,j:longint;
  map,kmap:array[0..105,0..105] of longint;
  flag:array[0..105,0..105] of boolean;

procedure dfs(x,y:longint; mgc:boolean);
var i,nx,ny,i1,i2:longint;
begin
  if (x=m) and (y=m) then
    begin
      if ans<min then min:=ans;
      //writeln(ans);
      exit;
    end;

  for i:=1 to 4 do
    begin
      nx:=x+dx[i]; ny:=y+dy[i];
      if (nx<=0) or (nx>m) or (ny<=0) or (ny>m) then continue;
      if flag[nx,ny] then continue;
      if ((map[x,y]=map[nx,ny]) or (kmap[x,y]=map[nx,ny])) and (map[nx,ny]>=0) then
        begin
          flag[nx,ny]:=true;
	  dfs(nx,ny,false);
          flag[nx,ny]:=false;
        end
      else if map[nx,ny]>=0 then
	begin
	  inc(ans);
	  flag[nx,ny]:=true;
	  dfs(nx,ny,false);
	  flag[nx,ny]:=false;
	  dec(ans);
	end;

      if (not mgc) and (map[nx,ny]<0) then
	begin
	  inc(ans,2);
	  flag[nx,ny]:=true;
          kmap[nx,ny]:=1;
	  dfs(nx,ny,true);
          kmap[nx,ny]:=-1;
	  flag[nx,ny]:=false;
	  dec(ans,2);
	end;

      if (not mgc) and (map[nx,ny]<0) then
	begin
	  inc(ans,2);
	  flag[nx,ny]:=true;
          kmap[nx,ny]:=0;
          if ((map[x,y]=map[nx,ny]) or (kmap[x,y]=map[nx,ny])) and (map[nx,ny]>=0) then
	    dfs(nx,ny,true)
          else if map[nx,ny]>=0 then
            begin
              inc(ans);
              dfs(nx,ny,true);
              dec(ans);
            end;
          kmap[nx,ny]:=-1;
	  flag[nx,ny]:=false;
	  dec(ans,2);
	end;
    end;
end;

begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);

  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do map[i,j]:=-1;
  for i:=1 to n do
    begin
      readln(x,y,c);
      map[x,y]:=c;
    end;

  kmap:=map;

  min:=maxlongint;
  ans:=0;
  flag[1,1]:=true;
  dfs(1,1,false);
  if min<maxlongint then writeln(min) else writeln(-1);

  close(input); close(output);
end.

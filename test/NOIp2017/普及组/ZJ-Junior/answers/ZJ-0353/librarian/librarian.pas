type tlist=array[0..1050] of longint;

var
  num:tlist;
  n,q,len,a,i,j:longint;
  ok:boolean;

function p(k:longint):longint;
var
  i:longint;
begin
  p:=1;
  for i:=1 to k do p:=p*10;
end;

procedure qsort(var a : tlist);

    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

    begin
       sort(1,n);
    end;

begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);

  readln(n,q);
  for i:=1 to n do readln(num[i]);

  qsort(num);
  for i:=1 to q do
    begin
      readln(len,a);
      ok:=false;
      for j:=1 to n do
	if a=num[j] mod p(len) then
	  begin
	    writeln(num[j]);
	    ok:=true;
	    break;
	  end;

      if not ok then writeln(-1);
    end;

  close(input); close(output);
end.

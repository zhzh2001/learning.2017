var
  x,s:array[0..500050] of longint;
  n,d,k,i,sum:longint;

begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);

  readln(n,d,k);
  sum:=0;
  for i:=1 to n do
    begin
      readln(x[i],s[i]);
      if s[i]>0 then sum:=sum+s[i];
    end;

  if sum<k then
    writeln(-1)
  else
    writeln(d div 2);

  close(input); close(output);
end.
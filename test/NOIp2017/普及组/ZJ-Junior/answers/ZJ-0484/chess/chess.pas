var
i,n,m,x,y,z,min,num:longint;
a:array[0..101,0..101]of longint;
b,c:array[0..101,0..101]of boolean;
procedure find(x,y,s:longint);
var
t:longint;
begin
    if x<1 then exit;
    if y<1 then exit;
    if x>n then exit;
    if y>n then exit;
    if s>=min then exit;
    if b[x,y] then exit;
    inc(num);
    if num>8500000 then
    begin
        if min<>maxlongint then write(min)
        else write(-1);
        close(input);
        close(output);
        halt;
    end;
    if (x=n) and (y=n) then
    begin
        if s<min then min:=s;
        exit;
    end;
    b[x,y]:=true;
    if (c[x,y]<>false) or (c[x+1,y]<>false) then
    begin
        if c[x+1,y]=false then
        begin
            t:=a[x+1,y];
            a[x+1,y]:=a[x,y];
            find(x+1,y,s+2);
            a[x+1,y]:=t;
        end else
        if a[x+1,y]=a[x,y] then
        begin
            find(x+1,y,s);
        end else
        if a[x+1,y]<>a[x,y] then
        begin
            find(x+1,y,s+1);
        end;
    end;
    if (c[x,y]<>false) or (c[x-1,y]<>false) then
    begin
        if c[x-1,y]=false then
        begin
            t:=a[x-1,y];
            a[x-1,y]:=a[x,y];
            find(x-1,y,s+2);
            a[x-1,y]:=t;
        end else
        if a[x-1,y]=a[x,y] then
        begin
            find(x+1,y,s);
        end else
        if a[x-1,y]<>a[x,y] then
        begin
            find(x-1,y,s+1);
        end;
    end;
    if (c[x,y]<>false) or (c[x,y+1]<>false) then
    begin
        if c[x,y+1]=false then
        begin
            t:=a[x,y+1];
            a[x,y+1]:=a[x,y];
            find(x,y+1,s+2);
            a[x,y+1]:=t;
        end else
        if a[x,y+1]=a[x,y] then
        begin
            find(x,y+1,s);
        end else
        if a[x,y+1]<>a[x,y] then
        begin
            find(x,y+1,s+1);
        end;
    end;
    if (c[x,y]<>false) or (c[x,y+1]<>false) then
    begin
        if c[x,y-1]=false then
        begin
            t:=a[x,y-1];
            a[x,y-1]:=a[x,y];
            find(x,y-1,s+2);
            a[x,y-1]:=t;
        end else
        if a[x,y-1]=a[x,y] then
        begin
            find(x,y-1,s);
        end else
        if a[x,y-1]<>a[x,y] then
        begin
            find(x,y-1,s+1);
        end;
    end;
    b[x,y]:=false;
end;
begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
    min:=maxlongint;
    readln(n,m);
    for i:=1 to m do
    begin
        readln(x,y,z);
        a[x,y]:=z+1;
        c[x,y]:=true;
    end;
    find(1,1,0);
    if min<>maxlongint then write(min)
    else write(-1);
    close(input);
    close(output);
end.

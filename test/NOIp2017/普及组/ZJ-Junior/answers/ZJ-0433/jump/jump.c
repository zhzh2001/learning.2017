#include<stdio.h>
int n,d,k,dis[500001],s[500001],sum;
void f(int x,int g,int t)
{
	if(t>sum)
	{
		sum=t;
	}
	int i;
	for(i=0;i<n;i++)
	{
		if(dis[i]>=x+d-g&&dis[i]<=x+d+g)
		{
			f(dis[i],g,t+s[i]);
		}
	}
	return;
}
int main()
{
	FILE *fin,*fout;
	fin=fopen("jump.in","r");
	fout=fopen("jump.out","w");
	int i,g;
	fscanf(fin,"%d%d%d",&n,&d,&k);
	for(i=0,sum=0;i<n;i++)
	{
		fscanf(fin,"%d%d",&dis[i],&s[i]);
		if(s[i]>0)
		{
			sum=sum+s[i];
		}
	}
	if(sum<k)
	{
		fprintf(fout,"-1");
		close(fin);
		close(fout);
		return 0;
	}
	for(g=0;;g++)
	{
		sum=-9999999;
		f(0,g,0);
		if(sum>=k)
		{
			break;
		}
	}
	fprintf(fout,"%d",g);
	close(fin);
	close(fout);
	return 0;
}

#include<stdio.h>
int m,n,a[101][101],sum=9999999,e[101][101];
int dx[4]={0,-1,0,1};
int dy[4]={-1,0,1,0};
void f(int x,int y,int t,int s)
{
	if(x==m&&y==m)
	{
		if(t<sum)
		{
			sum=t;
		}
		return;
	}
	int i,tx,ty;
	tx=x;
	ty=y;
	for(i=0;i<4;i++)
	{
		x=x+dx[i];
		y=y+dy[i];
		if(x>=1&&x<=m&&y>=1&&y<=m&&e[x][y]==0)
		{
			e[x][y]=1;
			if(a[x][y]==2)
			{
				if(s==0)
				{
					a[x][y]=a[tx][ty];
					f(x,y,t+2,1);
					a[x][y]=2;
				}
				else
				{
					continue;
				}
			}
			else
			{
				s=0;
				if(a[tx][ty]==a[x][y])
				{
					f(x,y,t,s);
				}
				else
				{
					f(x,y,t+1,s);
				}
			}
			e[x][y]=0;
		}
		x=x-dx[i];
		y=y-dy[i];
	}
	return;
}
int main()
{
	FILE *fin,*fout;
	fin=fopen("chess.in","r");
	fout=fopen("chess.out","w");
	int i,j,x,y,c;
	for(i=0;i<101;i++)
	{
		for(j=0;j<101;j++)
		{
			a[i][j]=2;
			e[i][j]=0;
		}
	}
	fscanf(fin,"%d%d",&m,&n);
	for(i=0;i<n;i++)
	{
		fscanf(fin,"%d%d%d",&x,&y,&c);
		a[x][y]=c;
	}
	f(1,1,0,0);
	if(sum==9999999)
	{
		fprintf(fout,"-1");
	}
	else
	{
		fprintf(fout,"%d",sum);
	}
	close(fin);
	close(fout);
	return 0;
}

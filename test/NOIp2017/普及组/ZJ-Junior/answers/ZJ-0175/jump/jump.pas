var
  x,s:array[0..500000]of longint;
  d,f:array[0..500]of longint;
  n,dis,k,i,maxi,sum,ans:longint;
function p(g:longint):boolean;
var
  i,h,t,point:longint;
begin
  fillchar(f,sizeof(f),0);
  fillchar(d,sizeof(d),0);
  h:=1;
  t:=1;
  d[1]:=0;
  if (x[1]<dis-g)or(x[1]>dis+g) then exit(false);
  for i:=1 to n do
    begin
      while (x[d[h]]<x[i]-dis-g)and(h<=t) do inc(h);
      while (d[t]+1<i)and(x[d[t]+1]<=x[i]-dis+g) do
        begin
          point:=d[t]+1;
          while (f[d[t]]<=f[point])and(t>=h) do dec(t);
          inc(t);
          d[t]:=point;
        end;
      if h<=t then f[i]:=f[d[h]]+s[i] else exit(false);
      if f[i]>=k then exit(true);
    end;
  exit(false);
end;
function find(l,r:longint):longint;
var
  mid:longint;
begin
  if l=r then exit(l);
  if l=r-1 then if p(l) then exit(l) else exit(r);
  mid:=(l+r) div 2;
  if p(mid) then exit(find(l,mid)) else exit(find(mid+1,r));
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  read(n,dis,k);
  maxi:=0;
  for i:=1 to n do
    begin
      read(x[i],s[i]);
      if x[i]>maxi then maxi:=x[i];
      if s[i]>0 then sum:=sum+s[i];
    end;
  if sum<k then
    begin
      write(-1);
      close(input);
      close(output);
      exit;
    end;
  ans:=find(0,maxi);
  write(ans);
  close(input);
  close(output);
end.

const
  d1:array[1..4,1..2]of longint=((1,0),(0,1),(-1,0),(0,-1));
  d2:array[1..8,1..2]of longint=((2,0),(0,2),(-2,0),(0,-2),(1,1),(1,-1),(-1,1),(-1,-1));
var
  g,a:array[-50..150,-50..150]of longint;
  f,x,y:array[1..1000]of longint;
  b:array[1..1000]of boolean;
  m,n,i,mini,j,p,c,start,final:longint;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  read(m,n);
  for i:=1 to n do
    begin
      read(x[i],y[i],c);
      g[x[i],y[i]]:=c+1;
      a[x[i],y[i]]:=i;
      f[i]:=maxlongint;
      if (x[i]=1)and(y[i]=1) then start:=i;
      if (x[i]=m)and(y[i]=m) then final:=i;
    end;
  f[start]:=0;
  for i:=1 to n-1 do
    begin
      mini:=maxlongint;
      for j:=1 to n do
        if (f[j]<mini)and(b[j]=false) then
          begin
            mini:=f[j];
            p:=j;
          end;
      b[p]:=true;
      for j:=1 to 4 do
        if g[x[p]+d1[j,1],y[p]+d1[j,2]]<>0 then
          begin
            if g[x[p]+d1[j,1],y[p]+d1[j,2]]=g[x[p],y[p]] then
              f[a[x[p]+d1[j,1],y[p]+d1[j,2]]]:=min(f[a[x[p]+d1[j,1],y[p]+d1[j,2]]],f[p]);
            if g[x[p]+d1[j,1],y[p]+d1[j,2]]<>g[x[p],y[p]] then
              f[a[x[p]+d1[j,1],y[p]+d1[j,2]]]:=min(f[a[x[p]+d1[j,1],y[p]+d1[j,2]]],f[p]+1);
          end;
      for j:=1 to 8 do
        if g[x[p]+d2[j,1],y[p]+d2[j,2]]<>0 then
          begin
            if g[x[p]+d2[j,1],y[p]+d2[j,2]]=g[x[p],y[p]] then
              f[a[x[p]+d2[j,1],y[p]+d2[j,2]]]:=min(f[a[x[p]+d2[j,1],y[p]+d2[j,2]]],f[p]+2);
            if g[x[p]+d2[j,1],y[p]+d2[j,2]]<>g[x[p],y[p]] then
              f[a[x[p]+d2[j,1],y[p]+d2[j,2]]]:=min(f[a[x[p]+d2[j,1],y[p]+d2[j,2]]],f[p]+3);
          end;
    end;
  if f[final]=maxlongint then write(-1) else write(f[final]);
  close(input);
  close(output);
end.

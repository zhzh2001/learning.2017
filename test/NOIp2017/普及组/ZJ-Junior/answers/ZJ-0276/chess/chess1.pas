const d:array[1..4,1..2] of longint=((1,0),(0,1),(-1,0),(0,-1));
var n,m,i,x,y,z,max:longint;
a:array[0..101,0..101] of longint;
f:array[0..101,0..101] of boolean;
procedure sc(x,y,o,k,w:longint);
var x1,y1,i:longint;
begin
    if (x<1)or(x>n)or(y<1)or(y>n) then exit;
    if w>max then exit;
    if (x=n)and(y=n)then
        begin
            max:=w;
            exit;
        end;
    for i:=1 to 4 do
    begin
        x1:=x+d[i,1];
        y1:=y+d[i,2];
        if (x1>=1)and(x1<=n)and(y1>=1)and(y1<=n)and f[x1,y1] then
        begin
            f[x1,y1]:=false;
            if (a[x1,y1]=o)and(o<>0) then sc(x1,y1,o,0,w);
            if (a[x1,y1]>0)and(a[x1,y1]<>o)and(o>0) then
                sc(x1,y1,a[x1,y1],0,w+1);
            if (a[x1,y1]=0)and(k=0) then
            begin
                if a[x,y]=1 then
                    begin
                        sc(x1,y1,1,1,w+2);
                        sc(x1,y1,2,1,w+3);
                    end;
                if a[x,y]=2 then
                    begin
                        sc(x1,y1,1,1,w+3);
                        sc(x1,y1,2,1,w+2);
                    end;
            end;
            f[x1,y1]:=true;
        end;
    end;
end;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);rewrite(output);
    read(n,m);
    for i:=1 to m do
    begin
        read(x,y,z);
        a[x,y]:=z+1;
    end;
    fillchar(f,sizeof(f),true);
    max:=maxlongint;
    sc(1,1,a[x,y],0,0);
    if max=maxlongint then writeln(-1)
    else writeln(max);
    close(input);close(output);
end.

var n,m,i,j,x,y,z:longint;
a,f,e:array[0..101,0..101] of longint;
function pd1(x,y:longint):longint;
begin
    if e[x-1,y]=1 then
        if  a[x-2,y]=a[x,y] then
            exit(0)
        else exit(1);
    if e[x-1,y]=2 then
        if a[x-1,y-1]=a[x,y] then
            exit(0)
        else exit(1);
end;
function pd2(x,y:longint):longint;
begin
    if e[x,y-1]=1 then
        if  a[x-1,y-1]=a[x,y] then
            exit(0)
        else exit(1);
    if e[x,y-1]=2 then
        if a[x,y-2]=a[x,y] then
            exit(0)
        else exit(1);
end;
function min(x,y:longint):longint;
begin
    if x<y then exit(x);exit(y);
end;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);rewrite(output);
    read(n,m);
    for i:=1 to m do
    begin
        read(x,y,z);
        a[x,y]:=z+1;
    end;
    a[1,0]:=a[1,1];
    a[0,1]:=a[1,1];
    fillchar(f,sizeof(f),127);
    f[0,1]:=0;f[1,0]:=0;
    for i:=1 to n do
        for j:=1 to n do
        begin
            if (a[i,j]=a[i-1,j])and(a[i,j]<>0) then
                f[i,j]:=min(f[i,j],f[i-1,j]);

            if (a[i,j]=a[i,j-1])and(a[i,j]<>0) then
                f[i,j]:=min(f[i,j],f[i,j-1]);

            if (a[i,j]>0)and(a[i-1,j]>0)and(a[i,j]<>a[i-1,j]) then
                f[i,j]:=min(f[i,j],f[i-1,j]+1);

            if (a[i,j]>0)and(a[i,j-1]>0)and(a[i,j]<>a[i,j-1]) then
                f[i,j]:=min(f[i,j],f[i,j-1]+1);

            if (a[i,j]=0)and(a[i-1,j]>0) then
                begin
                    if f[i,j]>f[i-1,j]+1 then
                        begin
                            f[i,j]:=f[i-1,j]+2;
                            e[i,j]:=1;
                        end;
                end;

            if (a[i,j]=0)and(a[i,j-1]>0) then
                begin
                    if f[i,j]>f[i,j-1]+1 then
                        begin
                            f[i,j]:=f[i,j-1]+2;
                            e[i,j]:=2;
                        end;
                end;

            if (a[i,j]>0)and(a[i-1,j]=0)and(f[i-1,j]<>2139062143) then
                f[i,j]:=min(f[i,j],f[i-1,j]+pd1(i,j));

            if (a[i,j]>0)and(a[i,j-1]=0)and(f[i,j-1]<>2139062143) then
                f[i,j]:=min(f[i,j],f[i,j-1]+pd2(i,j));
        end;
    if f[n,n]=2139062143 then writeln(-1)
    else writeln(f[n,n]);
    close(input);close(output);
end.

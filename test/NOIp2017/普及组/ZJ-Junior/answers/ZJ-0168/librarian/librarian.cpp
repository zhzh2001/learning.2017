#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

const int MAXL = 1500;
int nb, nr;
long books[MAXL], request[MAXL], length[MAXL], res[MAXL];

int main()
{
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	
	cin >> nb >> nr;
	for (int i = 0; i < nb; i++)
		cin >> books[i];
	for (int i = 0; i < nr; i++)
		cin >> length[i] >> request[i];
		
	sort(books, books + nb);
		
	for (int i = 0; i < nr; i++)
	{
		long book = -1;
		int find = 0;
		for (int j = 0; j < nb; j++)
			if (books[j] % ((long) pow(10.0, (double) length[i])) == request[i])
				if (find == 0 || books[j] < book)
				{
					book = books[j]; 
					break;
					find = 1;
				}
		cout << book << endl;
	}
}

#include <cstdio>
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
	
	double x, y, z;
	cin >> x >> y >> z;
	cout << (x * 0.2 + y * 0.3 + z * 0.5);
	
	return 0;
}

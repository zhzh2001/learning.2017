#include <cstdio>
#include <iostream>
#include <fstream>

using namespace std;

int board[150][150];
int go[150][150];
int m, n;
int win = 0;

int gleft(int x, int y, int magic)
{

	int nx, ny, s;
	if (x > 0)
	{
		nx = x - 1; ny = y;
		if (go[nx][ny]) return -1;
		if (nx == m - 1 && ny == m - 1) win = 1;
		s = board[nx][ny];
		if (s == -1)
		{
			if (magic == 0) return -1; // magic ran out
			else return 2; // magic costs 2
		} 
		else if (s == board[x][y] || magic == 0) return 0;
		else return 1;
	} else return -1; // cannot move
}

int gright(int x, int y, int magic)
{
	int nx, ny, s;
	if (x < m - 1)
	{
		nx = x + 1; ny = y;
		if (go[nx][ny]) return -1;
		if (nx == m - 1 && ny == m - 1) win = 1;
		s = board[nx][ny];
		if (s == -1)
		{
			if (magic == 0) return -1; // magic ran out
			else return 2; // magic costs 2
		} 
		else if (s == board[x][y] || magic == 0) return 0;
		else return 1;
	} else return -1; // cannot move
}

int gup(int x, int y, int magic)
{
	int nx, ny, s;
	if (y > 0)
	{
		nx = x; ny = y - 1;
		if (go[nx][ny]) return -1;
		if (nx == m - 1 && ny == m - 1) win = 1;
		s = board[nx][ny];
		if (s == -1)
		{
			if (magic == 0) return -1; // magic ran out
			else return 2; // magic costs 2
		} 
		else if (s == board[x][y] || magic == 0) return 0;
		else return 1;
	} else return -1; // cannot move
}

int gdown(int x, int y, int magic)
{
	int nx, ny, s;
	if (y < m - 1)
	{
		nx = x; ny = y + 1;
		if (go[nx][ny]) return -1;
		if (nx == m - 1 && ny == m - 1) win = 1;
		s = board[nx][ny];
		if (s == -1)
		{
			if (magic == 0) return -1; // magic ran out
			else return 2; // magic costs 2
		} 
		else if (s == board[x][y] || magic == 0) return 0;
		else return 1;
	} else return -1; // cannot move
}

int p(int a, int b) { return (b == -1) ? -1 : (a+b); }

int play(int x, int y, int magic)
{
	/*
	go[x][y] = 1;
	
	int s1, s2, s3, s4, m;
	
	//cout << x << ", " << y << " " << endl;
	
	s1 = gleft(x, y, magic); m = 1;
	if (win && s1 != -1) return s1;	
	else if (s1 == 2) m = 0;
	else if (s1 != -1) s1 = p(s1, play(x-1, y, m));
	
	s2 = gright(x, y, magic); m = 1;
	if (win && s2 != -1) return s2;	
	else if (s2 == 2) m = 0;
	else if (s2 != -1) s2 = p(s2, play(x+1, y, m));
	
	s3 = gup(x, y, magic); m = 1;
	if (win && s3 != -1) return s3;	
	else if (s3 == 2) m = 0;
	else if (s3 != -1) s3 = p(s3, play(x, y-1, m));
	
	s4 = gdown(x, y, magic);  m = 1;
	if (win && s4 != -1) return s4;	
	else if (s4 == 2) m = 0;
	else if (s4 != -1) s4 = p(s4, play(x, y+1, m));
	
	if (s1 == -1) s1 = 32767;
	if (s2 == -1) s2 = 32767;
	if (s3 == -1) s3 = 32767;
	if (s4 == -1) s4 = 32767;
	
	int r = min(min(s1, s2), min(s3, s4));
	if (r == 32767) go[x][y] = 0;
	return (r == 32767) ? -1 : r;
	*/
	
	go[x][y] = 1;
	
	int s1, s2, s3, s4, m;
	
	s1 = gleft(x, y, magic);
	s2 = gright(x, y, magic);
	s3 = gup(x, y, magic);
	s4 = gdown(x, y, magic); 
		
	//cout << x << " " << y << " m=" << magic << endl;
	//cout << s1 << " " << s2 << " " << s3 << " " << s4 << endl << endl;
	
	if (s1 == 0) return play(x-1, y, 1);
	if (s2 == 0) return play(x+1, y, 1);	
	if (s3 == 0) return play(x, y-1, 1);
	if (s4 == 0) return play(x, y+1, 1);
	
	if (s1 == 1) return p(1, play(x-1, y, 1));
	if (s2 == 1) return p(1, play(x+1, y, 1));
	if (s3 == 1) return p(1, play(x, y-1, 1));
	if (s4 == 1) return p(1, play(x, y+1, 1));
	
	if (s1 == 2) return p(2, play(x-1, y, 0));
	if (s2 == 2) return p(2, play(x+1, y, 0));
	if (s3 == 2) return p(2, play(x, y-1, 0));
	if (s4 == 2) return p(2, play(x, y+1, 0));
	
	return -1;
	
}

int main()
{
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	
	cin >> m >> n;
	for (int i = 0; i < m; i++)
		for (int j = 0; j < m; j++)
			board[j][i] = -1; // no color
	for (int i = 0; i < m; i++)
		for (int j = 0; j < m; j++)
			go[j][i] = 0; // no color
			
	for (int i = 0; i < n; i++)
	{
		int x, y;
		cin >> x >> y;
		cin >> board[y - 1][x - 1];
	}
	
	/*for (int y = 0; y < m; y++)
	{
		for (int x = 0; x < m; x++)
			cout << board[x][y] << " ";
		cout << endl << endl;
	}*/
	
	cout << play(0, 0, 1) << endl;
	
	/*for (int y = 0; y < m; y++)
	{
		for (int x = 0; x < m; x++)
			cout << go[x][y] << " ";
		cout << endl << endl;
	}*/
	
	return 0;
}

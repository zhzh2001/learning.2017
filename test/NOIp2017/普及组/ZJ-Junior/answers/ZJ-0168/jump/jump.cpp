#include <cstdio>
#include <iostream>

using namespace std;

long n, d, k;
long dist[500000], score[500000], ddist[500000];

int main()
{
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	
	cin >> n >> d >> k;
	dist[0] = 0;
	for (int i = 1; i = n; i++)
	{
		cin >> dist[i] >> score[i];
		ddist[i] = dist[i] - dist[i - 1];
	}
	
	long s, g, x;
	s = 0; g = 0; x = 0;
	while (s < k)
	{
		long next = x + 1;
		while (dist[next] < dist[x] + k && next <= n) next++;
		g = max(g, dist[next - 1] - dist[x]);
		s += score[next - 1];
		
	}
	cout << g;
	
	return 0;
}

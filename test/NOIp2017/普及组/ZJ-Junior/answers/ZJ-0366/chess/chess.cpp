#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
const int maxn=105,maxv=100005,flg[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,head,tail,INF,a[maxn][maxn],f[maxn][maxn][4];
bool vis[maxn][maxn];
struct QueData{
	int x,y;
}que[maxv];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f; 
}
inline bool check(int x,int y){
	if (x<1||y<1||x>n||y>n) return false;
	return true;
}
inline int oth(int x){
	if (x==1) return 2;else return 1;
}
inline int min3(int x,int y,int z){
	int ret=x;
	if (y<ret) ret=y;
	if (z<ret) ret=z;
	return ret;
}
inline void SPFA(){
	memset(vis,0,sizeof(vis));
	memset(f,63,sizeof(f));
	INF=f[0][0][0];
	f[1][1][a[1][1]]=0;vis[1][1]=true;
	head=0;tail=1;que[tail].x=1;que[tail].y=1;
	while (head!=tail){
		head=(head+1)%maxv;
		int xx=que[head].x,yy=que[head].y;
		vis[xx][yy]=false;
		for (int i=0;i<4;i++) if (check(xx+flg[i][0],yy+flg[i][1])){
			int now=a[xx][yy],nxt=a[xx+flg[i][0]][yy+flg[i][1]];
			if (now==0&&nxt!=0){//This one is no color
				if (min(f[xx][yy][nxt],f[xx][yy][oth(nxt)]+1)<f[xx+flg[i][0]][yy+flg[i][1]][nxt]){
					f[xx+flg[i][0]][yy+flg[i][1]][nxt]=min(f[xx][yy][nxt],f[xx][yy][oth(nxt)]+1);
					if (vis[xx+flg[i][0]][yy+flg[i][1]]==false){
						tail=(tail+1)%maxv;
						que[tail].x=xx+flg[i][0];que[tail].y=yy+flg[i][1];
						vis[xx+flg[i][0]][yy+flg[i][1]]=true;
					}
				}
			} else if (now!=0&&nxt==0){//The next one is no color
				if (f[xx][yy][now]+2<f[xx+flg[i][0]][yy+flg[i][1]][now]){
					f[xx+flg[i][0]][yy+flg[i][1]][now]=f[xx][yy][now]+2;
					if (vis[xx+flg[i][0]][yy+flg[i][1]]==false){
						tail=(tail+1)%maxv;
						que[tail].x=xx+flg[i][0];que[tail].y=yy+flg[i][1];
						vis[xx+flg[i][0]][yy+flg[i][1]]=true;
					}
				}
				if (f[xx][yy][now]+3<f[xx+flg[i][0]][yy+flg[i][1]][oth(now)]){
					f[xx+flg[i][0]][yy+flg[i][1]][oth(now)]=f[xx][yy][now]+3;
					if (vis[xx+flg[i][0]][yy+flg[i][1]]==false){
						tail=(tail+1)%maxv;
						que[tail].x=xx+flg[i][0];que[tail].y=yy+flg[i][1];
						vis[xx+flg[i][0]][yy+flg[i][1]]=true;
					}
				}
			} else if (now!=0&&nxt==now){//The color of the next one is same
				if (f[xx][yy][now]<f[xx+flg[i][0]][yy+flg[i][1]][nxt]){
					f[xx+flg[i][0]][yy+flg[i][1]][nxt]=f[xx][yy][now];
					if (vis[xx+flg[i][0]][yy+flg[i][1]]==false){
						tail=(tail+1)%maxv;
						que[tail].x=xx+flg[i][0];que[tail].y=yy+flg[i][1];
						vis[xx+flg[i][0]][yy+flg[i][1]]=true;
					}
				}
			} else if (now!=0&&nxt!=now){//The color of the next one is different
				if (f[xx][yy][now]+1<f[xx+flg[i][0]][yy+flg[i][1]][nxt]){
					f[xx+flg[i][0]][yy+flg[i][1]][nxt]=f[xx][yy][now]+1;
					if (vis[xx+flg[i][0]][yy+flg[i][1]]==false){
						tail=(tail+1)%maxv;
						que[tail].x=xx+flg[i][0];que[tail].y=yy+flg[i][1];
						vis[xx+flg[i][0]][yy+flg[i][1]]=true;
					}
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++){
		int x=read(),y=read(),z=read();
		a[x][y]=z+1;
	}
	SPFA();
	if (min3(f[n][n][3],f[n][n][1],f[n][n][2])==INF) printf("-1\n"); else printf("%d\n",min3(f[n][n][3],f[n][n][1],f[n][n][2]));
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=1005;
int n,m,a[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f; 
}
inline bool cmp(int x,int y){
	while (y){
		if (x%10!=y%10) return false;
		x/=10;y/=10;
	}
	return true;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+1+n);
	for (int i=1;i<=m;i++){
		int len=read(),now=read();
		bool flg=false;
		for (int j=1;j<=n;j++) if (cmp(a[j],now)){flg=true;printf("%d\n",a[j]);break;}
		if (flg==false) printf("-1\n");
	}
	return 0;
}

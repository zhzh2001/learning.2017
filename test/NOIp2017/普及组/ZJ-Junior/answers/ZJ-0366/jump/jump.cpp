#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
const int maxn=500005;
int n,d,m,L=0,R,ans=-1,que[maxn];
long long f[maxn];
struct WT{
	int x,s;
}a[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f; 
}
inline bool check(int g){
	memset(que,0,sizeof(que));
	f[1]=-((long long)1<<60);
	for (int i=2;i<=n;i++) f[i]=f[1];
	f[0]=0;
	int head=1,tail=1;que[tail]=0;
	long long ret=0,adlst=0;
	for (int i=1;i<=n;i++){
		while (adlst<i&&a[i].x-a[adlst].x>d+g) adlst++;
		while (adlst<i&&d-g<=a[i].x-a[adlst].x&&a[i].x-a[adlst].x<=d+g){
			while (head<=tail&&f[que[tail]]<f[adlst]) tail--;
			que[++tail]=adlst;
			adlst++;
		}
		while (head<=tail&&a[i].x-a[que[head]].x>d+g) head++;
		if (head<=tail&&d-g<=a[i].x-a[que[head]].x&&a[i].x-a[que[head]].x<=d+g) f[i]=f[que[head]]+a[i].s;
		if (f[i]>ret) ret=f[i];
	}
	return ret>=(long long)m;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();m=read();
	for (int i=1;i<=n;i++) a[i].x=read(),a[i].s=read();
	R=a[n].x;
	while (L<=R){
		int mid=((R-L)>>1)+L;
		if (check(mid)) ans=mid,R=mid-1; else L=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

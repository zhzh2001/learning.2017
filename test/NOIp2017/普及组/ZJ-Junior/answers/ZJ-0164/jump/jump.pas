type zz=record
  x,s:longint;
end;
var
  a:array[0..1000000]of zz;
  h:array[0..1000000]of longint;
  n,d,k,i,ans:longint;
function dd(x,y:longint):longint;
begin
  if x>y then
    exit(x);
  exit(y);
end;
procedure dfs(c,m,max,min,last:longint);
var
  wa,wi:longint;
begin
  if m>=k then
  begin
    if (dd(max-d,d-min)<ans) or (ans=-1) then
      ans:=dd(max-d,d-min);
    exit;
  end;
  if c>n then
    exit;
  if (dd(max-d,d-min)>=ans) and (ans<>-1) then
    exit;
  if m+h[c]<k then
    exit;
  dfs(c+1,m,max,min,last);
  if a[c].x-last>max then
    wa:=a[c].x-last else
    wa:=max;
  if (a[c].x-last<min) or (min=-1) then
    wi:=a[c].x-last else
    wi:=min;
  dfs(c+1,m+a[c].s,wa,wi,a[c].x);
end;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    readln(a[i].x,a[i].s);
  for i:=n downto 1 do
    if a[i].s>0 then
      h[i]:=h[i+1]+a[i].s else
      h[i]:=h[i+1];
  for i:=1 to n do
  ans:=-1;
  dfs(1,0,-1,-1,0);
  writeln(ans);
  close(input);close(output);
end.
var
  a:array[0..10000]of longint;
  n,m,i,j,len,s,x,min,minid:longint;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to m do
  begin
    readln(len,x);
    s:=1;
    for j:=1 to len do
      s:=s*10;
    min:=100000000;
    for j:=1 to n do
      if (a[j] mod s=x) and (a[j]<min) then
      begin
        min:=a[j];
        minid:=j;
      end;
    if min<>100000000 then
      writeln(min) else
      writeln('-1');
  end;
  close(input);close(output);
end.
var
  dic:array[1..4,1..2]of longint=((1,0),(-1,0),(0,1),(0,-1));
  a,b,f:array[1..1500,1..1500]of longint;
  i,j,n,m,x,y,z,h,l,nh,nl:longint;
function min(x,y:longint):longint;
begin
  if x<y then
    exit(x);
  exit(y);
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
    begin
      a[i,j]:=-1;
      b[i,j]:=-1;
    end;
  for i:=1 to m do
  begin
    readln(x,y,z);
    a[x,y]:=z;
    b[x,y]:=z;
  end;
  for i:=1 to n do
    for j:=1 to n do
      f[i,j]:=100000000;
  f[1,1]:=0;
  for h:=1 to n do
    for l:=1 to n do
      for i:=1 to 4 do
      begin
        nh:=h+dic[i,1];
        nl:=l+dic[i,2];
        if (nh>=1) and (nh<=n) and (nl>=1) and (nl<=n) then
        begin
          if (a[nh,nl]<>-1) and (a[nh,nl]<>b[h,l]) then
            f[nh,nl]:=min(f[nh,nl],f[h,l]+1);
          if (a[nh,nl]<>-1) and (a[nh,nl]=b[h,l]) then
            f[nh,nl]:=min(f[nh,nl],f[h,l]);
          if (a[nh,nl]=-1) and (a[h,l]<>-1) then
          begin
            if f[nh,nl]>f[h,l]+2 then
            begin
              f[nh,nl]:=f[h,l]+2;
              b[nh,nl]:=b[h,l];
            end;
          end;
        end;
      end;
  if f[n,n]=100000000 then
    writeln('-1') else
    writeln(f[n,n]);
  close(input);close(output);
end.
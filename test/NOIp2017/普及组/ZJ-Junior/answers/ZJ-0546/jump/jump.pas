var n,m,p,x,y,z,i:longint;
    c:array[0..500001] of boolean;
    a,b,f:array[0..500001] of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
function tx(x,y:longint):boolean;
var i,j,s:longint;
begin
  s:=0;
  x:=x+y;
  for i:=1 to n do
    f[i]:=-maxlongint div 2;
  for i:=1 to n do
    begin
    for j:=i-1 downto 0 do
      if a[i]-a[j]>x then begin if s>=p then exit(true) else exit(false); end
      else if b[j]>=0 then begin f[i]:=max(f[i],f[j]+b[i]); break; end
      else f[i]:=max(f[i],f[j]+b[i]);
    s:=max(s,f[i]);
    end;
  if s>=p then exit(true) else exit(false);
end;
function dp(x,y:longint):boolean;
var i,j,k,s:longint;
begin
  if (y=1) and (n>2000) then exit(tx(z,m));
  k:=x;
  x:=max(1,y-x);
  y:=k+y;
  for i:=1 to n do
    begin
    f[i]:=-maxlongint div 2;
    c[i]:=false;
    end;
  s:=0;
  c[0]:=true;
  for i:=1 to n do
    for j:=0 to i-1 do
      if (a[i]-a[j]>=x) and (a[i]-a[j]<=y) and c[j] then
        begin
        f[i]:=max(f[i],f[j]+b[i]);
        s:=max(s,f[i]);
        c[i]:=true;
        end;
  if s>=p then exit(true)
  else exit(false);
end;
begin
  assign(input,'jump.in');assign(output,'jump.out');
  reset(input);rewrite(output);
  readln(n,m,p);
  for i:=1 to n do read(a[i],b[i]);
  x:=1; y:=1000000000;
  while x<y do
    begin
    z:=(x+y) div 2;
    if dp(z,m) then y:=z
    else x:=z+1;
    end;
  if y=1000000000 then y:=-1;
  writeln(y);
  close(input);close(output);
end.

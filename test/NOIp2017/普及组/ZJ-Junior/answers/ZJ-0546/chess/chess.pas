var n,m,i,x,y,z:longint;
    b:array[0..101,0..101] of boolean;
    a,f:array[0..101,0..101] of longint;
    e:array[1..4,1..2] of longint=((1,0),(0,1),(-1,0),(0,-1));
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
function sc(x,y:longint):longint;
var i,j,x1,y1,x2,y2:longint;
begin
  if (x=n) and (y=n) then exit(0);
  if f[x,y]>-1 then exit(f[x,y]);
  sc:=maxlongint div 2;
  b[x,y]:=false;
  for i:=1 to 4 do
    begin
    x1:=x+e[i,1]; y1:=y+e[i,2];
    if b[x1,y1] then
      if a[x1,y1]=0 then
        begin
        for j:=1 to 4 do
          begin
          x2:=x1+e[j,1]; y2:=y1+e[j,2];
          if b[x2,y2] and (a[x2,y2]<>0) then
            begin
            b[x1,y1]:=false;
            if a[x2,y2]=a[x,y] then sc:=min(sc,sc(x2,y2)+2)
            else sc:=min(sc,sc(x2,y2)+3);
            b[x1,y1]:=true;
            end;
          end;
        end
      else
        begin
        if a[x1,y1]=a[x,y] then sc:=min(sc,sc(x1,y1))
        else sc:=min(sc,sc(x1,y1)+1);
        end;
    end;
  b[x,y]:=true;
  if sc<>maxlongint div 2 then f[x,y]:=sc;
end;
begin
  assign(input,'chess.in');assign(output,'chess.out');
  reset(input);rewrite(output);
  readln(n,m);
  for x:=1 to n do
    for y:=1 to n do
      b[x,y]:=true;
  for i:=1 to m do
    begin
    readln(x,y,z);
    if z=0 then z:=-1;
    a[x,y]:=z;
    end;
  fillchar(f,sizeof(f),255);
  x:=sc(1,1);
  if x=maxlongint div 2 then x:=-1;
  writeln(x);
  close(input);close(output);
end.

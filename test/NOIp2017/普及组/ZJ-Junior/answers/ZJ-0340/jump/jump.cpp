#include<cstdio>
#include<map>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,d,k,i,j,xx[500005],s[500005],sm=2147483647;
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
    n=read(),d=read(),k=read();
    for(i=1;i<=n;i++){
    	xx[i]=read(),s[i]=read();
	}
	int l=0,r=1e9,mid;
	while(l<=r){
		mid=(r-l)/2+l;
		i=1;int lst=0;bool pd=0;int sum=0;
		while(i<=n){
			if(s[i]<0){
				i++;
				continue;
			}else{
				if(xx[i]-lst<=mid+d){
					sum+=s[i];i++;
				}else{
					pd=1;break;
				}
			}
		}
		if(sum<k)pd=1;
		if(pd==1){
			l=mid+1;
		}else{
			r=mid-1;
			if(mid<sm)sm=mid;
		}
	}
	if(sm==2147483647){
		printf("-1\n");
		return 0;
	}
	printf("%d\n",sm);
	return 0;
}


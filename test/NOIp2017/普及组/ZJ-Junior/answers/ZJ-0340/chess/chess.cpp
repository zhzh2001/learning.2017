#include<cstdio>
#include<iostream>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int flg[4][2]={{0,1},{0,-1},{1,0},{-1,0}};
struct ad{
	int x,y,s,hh,rr;
}que[20005];
int m,n,hea,til,i,j,a[105][105],ans=2147483647;
bool flag[105][105];
void BFS(){
	que[til=1]=(ad){1,1,0,0};flag[1][1]=1;
	while(hea!=til){
		int wz=que[++hea].x,wz1=que[hea].y,v=que[hea].s,pd=que[hea].hh,nm=que[hea].rr;
		if(wz==m&&wz1==m)continue;
		for(int k=0,xx,yy;k<4;k++){
			xx=wz+flg[k][0],yy=wz1+flg[k][1];
			if(xx<1||xx>m||yy<1||yy>m||flag[xx][yy])continue;
			if(pd==1){
				if(a[xx][yy]==a[que[nm].x][que[nm].y]){
				   que[++til]=(ad){xx,yy,v,0,0};if(xx==m&&yy==m){if(v<ans)ans=v;}flag[xx][yy]=1;continue;
			    }
			    if(a[xx][yy]!=-1){
				   que[++til]=(ad){xx,yy,v+1,0,0};if(xx==m&&yy==m){if(v+1<ans)ans=v+1;}flag[xx][yy]=1;continue;
			    }
			    continue;
			}
			if(a[xx][yy]==a[wz][wz1]){
				que[++til]=(ad){xx,yy,v,0,0};if(xx==m&&yy==m){if(v<ans)ans=v;}flag[xx][yy]=1;continue;
			}
			if(a[xx][yy]!=-1){
				que[++til]=(ad){xx,yy,v+1,0,0};if(xx==m&&yy==m){if(v+1<ans)ans=v+1;}flag[xx][yy]=1;continue;
			}
			que[++til]=(ad){xx,yy,v+2,1,hea};if(xx==m&&yy==m){if(v+2<ans)ans=v+2;}flag[xx][yy]=1;
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read(),n=read();
	for(i=1;i<=m;i++)
	for(j=1;j<=m;j++)a[i][j]=-1;
	for(i=1;i<=n;i++){
		int xx=read(),yy=read(),cc=read();
		a[xx][yy]=cc;
	}
	BFS();
	if(ans==2147483647){
		printf("-1\n");
		return 0;
	}
	printf("%d\n",ans);
	return 0;
}

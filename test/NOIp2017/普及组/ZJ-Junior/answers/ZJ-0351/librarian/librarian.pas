var
  a:array[1..1000] of longint;
  n,q,len,s,i,j,min:longint;
  s1,s2:string;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);


  readln(n,q);
  for i:=1 to n do readln(a[i]);
  for i:=1 to q do
    begin
      readln(len,s);
      str(s,s1);
      min:=maxlongint;
      for j:=1 to n do
        begin
          str(a[j],s2);
          while pos(s1,s2)<>0 do
            begin
              if pos(s1,s2)+len-1=length(s2) then if a[j]<min then min:=a[j];
              delete(s2,pos(s1,s2),len);
            end;
        end;
      if min<>maxlongint then writeln(min)
                         else writeln(-1);
    end;


  close(input);
  close(output);
end.

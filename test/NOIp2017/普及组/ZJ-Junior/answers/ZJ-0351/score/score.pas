var
  a,b,c,ans:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);


  read(a,b,c);
  ans:=(a*20)div 100+(b*30) div 100+(c*50) div 100;
  write(ans);


  close(input);
  close(output);
end.

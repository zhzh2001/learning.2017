var
  a:array[1..100,1..100] of longint;
  i,j,x,y,c,min,n,m:longint;
procedure dfs(i,j,s:longint;bool:boolean);
  begin
    if (i>m)or(j>m) then exit;
    if (i=m)and(j=m) then begin
                            if s<min then min:=s;
                            exit;
                          end;
    if a[i+1,j]<>-1 then if a[i+1,j]=a[i,j] then dfs(i+1,j,s,true)
                                            else dfs(i+1,j,s+1,true)
                    else if bool then dfs(i+1,j,s+2,false);
    if a[i,j+1]<>-1 then if a[i,j+1]=a[i,j] then dfs(i,j+1,s,true)
                                            else dfs(i,j+1,s+1,true)
                    else if bool then dfs(i,j+1,s+2,false);
  end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);


  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=-1;
  for i:=1 to n do
    begin
      readln(x,y,c);
      a[x,y]:=c;
    end;
  min:=maxlongint;
  dfs(1,1,0,true);
  if min=maxlongint then write(-1)
                    else write(min);


  close(input);
  close(output);
end.

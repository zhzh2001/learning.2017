var
  n,d,k,max,maxd,i:longint;
  a,b:array[1..500000] of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);


  readln(n,d,k);
  max:=0;  maxd:=0;
  for i:=1 to n do
    begin
      readln(a[i],b[i]);
      if abs(k-max-b[i])>abs(k-max) then max:=max+b[i];
      if maxd<a[i]-a[i-1] then maxd:=a[i]-a[i-1];
      if max>k then break;
    end;
  if k>max then write(-1)
  else if d=1 then write(0)
              else write(maxd);


  close(input);
  close(output);
end.

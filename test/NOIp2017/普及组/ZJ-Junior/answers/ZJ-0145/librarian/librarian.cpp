#include<cstdio>
using namespace std;
#define oo 1000000000
int a[1005];
int calc(int len , int x)
{
	int tmp = 1;
	for(int i = 1;i <= len;i++) tmp *= 10;
	return x % tmp;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n , q;
	scanf("%d%d",&n,&q);
	for(int i = 1;i <= n;i++) scanf("%d",&a[i]);
	while(q--)
	{
		int x , y;
		scanf("%d%d",&x,&y);
		int ans = oo;
		for(int i = 1;i <= n;i++)
			if(calc(x , a[i]) == y && a[i] < ans) ans = a[i];
		if(ans == oo) printf("-1\n");
		else printf("%d\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
#define oo 1000000000
int n , d , k;
int f[505] , s[505] , x[505];
bool check(int g)
{
	f[0] = 0;
	for(int i = 1;i <= n;i++) f[i] = -oo;
	for(int i = 1;i <= n;i++)
		for(int j = 0;j < i;j++)
			if(x[i] - x[j] <= g + d && x[i] - x[j] >= max(1 , d - g))
				f[i] = max(f[i] , f[j] + s[i]);
	int ans = 0;
	for(int i = 0;i <= n;i++)
		ans = max(ans , f[i]);
	return ans >= k;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i = 1;i <= n;i++) scanf("%d%d",&x[i],&s[i]);
	int l = 0 , r = 100000;
	while(l < r)
	{
		int mid = (l + r - 1) >> 1;
		if(check(mid)) r = mid;
		else l = mid + 1;
	}
	if(check(r)) printf("%d\n",r);
	else printf("-1\n");
	return 0;
}

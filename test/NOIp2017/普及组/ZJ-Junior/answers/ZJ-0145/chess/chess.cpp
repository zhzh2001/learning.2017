#include<cstdio>
using namespace std;
#define oo 1000000000
int m , ans , num;
int dx[4] , dy[4];
int map[55][55];
bool check[55][55];
void dfs(int x , int y , int sta , int cost)
{
	if(x == m && y == m)
	{
		if(cost < ans) ans = cost;
		return;
	}
	for(int k = 1;k < 5;k++)
	{
		int xx = x + dx[k] , yy = y + dy[k];
		if(xx < 1 || xx > m || yy < 1 || yy > m || check[xx][yy] == 1) continue;
		if(map[xx][yy] == 0 && sta == 0) continue;
		if(map[xx][yy] == 0)
		{
			check[xx][yy] = 1;
			map[xx][yy] = 1;
			if(map[xx][yy] == map[x][y]) dfs(xx , yy , 0 , cost + 2);
			else dfs(xx , yy , 0 , cost + 3);
			map[xx][yy] = 2;
			if(map[xx][yy] == map[x][y]) dfs(xx , yy , 0 , cost + 2);
			else dfs(xx , yy , 0 , cost + 3);
			check[xx][yy] = 0;
			map[xx][yy] = 0;
		}
		else if(map[xx][yy] == map[x][y])
		{
			check[xx][yy] = 1;
			dfs(xx , yy , 1 , cost);
			check[xx][yy] = 0;
		}
		else
		{
			check[xx][yy] = 1;
			dfs(xx , yy , 1 , cost + 1);
			check[xx][yy] = 0;
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	dx[1] = 0 , dx[2] = 0 , dx[3] = 1 , dx[4] = -1;
	dy[1] = 1 , dy[2] = -1 , dy[3] = 0 , dy[4] = 0;
	int n;
	scanf("%d%d",&m,&n);
	for(int i = 1;i <= m;i++)
		for(int j = 1;j <= m;j++) map[i][j] = 0;
	while(n--)
	{
		int x , y , c;
		scanf("%d%d%d",&x,&y,&c);
		map[x][y] = c + 1;
	}
	ans = oo;
	check[1][1] = 1;
	dfs(1 , 1 , 1 , 0);
	if(ans == oo) printf("-1\n");
	else printf("%d\n",ans);
	return 0;
}

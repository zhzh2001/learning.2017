var i,j,m,n,g,o,p,num,x,y,z,t,x1,y1:longint;
    a,b:array[0..105,0..105]of longint;
    dis:array[0..105,0..105]of longint;
    dx:array[1..4]of longint=(0,1,-1,0);
    dy:array[1..4]of longint=(1,0,0,-1);
procedure dfs(x,y,z:longint);
var i,x1,y1:longint;
begin
  for i:=1 to 4 do
  begin
   x1:=x+dx[i];
   y1:=y+dy[i];
   if (x1>0)and(y1>0)and(x1<=m)and(y1<=m)
    then
   begin
     if (a[x1,y1]>0)
      then
      begin
       if (a[x1,y1]=z)and(dis[x1,y1]>dis[x,y])
        then
        begin
           dis[x1,y1]:=dis[x,y];
           dfs(x1,y1,z);
        end
       else if (a[x1,y1]<>z)and(dis[x1,y1]>(dis[x,y]+1))
        then
        begin
         dis[x1,y1]:=dis[x,y]+1;
         dfs(x1,y1,a[x1,y1]);
        end;
      end
     else if (a[x,y]<>0)and(a[x1,y1]=0)and(dis[x1,y1]>(dis[x,y]+2))
      then
      begin
        dis[x1,y1]:=dis[x,y]+2;
        dfs(x1,y1,a[x,y]);
      end;
   end;
  end;
end;
begin
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);
readln(m,n);

for i:=1 to n do
begin
 readln(x,y,z);
 inc(z);
 a[x,y]:=z;
end;

fillchar(dis,sizeof(dis),63);
dis[1,1]:=0;
dfs(1,1,a[1,1]);
if dis[m,m]>1000000000 then writeln(-1)
 else
writeln(dis[m,m]);
close(input);
close(output);
end.

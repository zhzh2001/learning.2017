var
 a:array[1..100,1..1000] of integer;
 n,m,i,j,x,y,z:longint;
function max(a,b:longint):longint;
begin
 if a>b then exit(a);
 exit(b);
end;
function search(x,y,z:longint):longint;
var
 i,oldx,oldy:longint;
 tot:array[1..2]of longint;
begin
 oldx:=x; oldy:=y; tot[1]:=0; tot[2]:=0;
 for i:=1 to 2 do
  begin
   if i=1 then inc(x)
   else begin x:=oldx; dec(y);
   if(x in[1..n])and(y in[1..n]) then
    if (a[x,y]=a[oldx,oldy])and(a[x,y]<>2) then tot[i]=search(x,y,0)
    else if (a[x,y]<>2) then tot[i]:=1+search(x,y,0)
         else if z=0 then tot[i]:=2+search(x,y,1)
              else tot[i]:=1200;
   end;
   if (tot[1]=1200)and(tot[2]=1200) then exit(-1)
   else if tot[1]=1200 then exit(tot[2])
        else if tot[2]=1200 then exit(tot[1])
             else exit(max(tot[1],tot[2]));
end;
BEGIN
 assign(input,'chess.in');
 reset(input);
 assign(output,'chess.out');
 rewrite(output);
 readln(n,m);
 for i:=1 to n do
  for j:=1 to n do a[i,j]:=2;
 for i:=1 to m do
  begin
   readln(x,y,z);
   a[x,y]:=z;
  end;
 writeln(search(1,1,0));
 close(input); close(output);
END.

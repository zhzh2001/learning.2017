var
 n,q:integer;
 a,b,c:array[1..1005]of longint;
 t,k,i,j,x:longint;
 f:boolean;
begin
 assign(input,'librarian.in');
 reset(input);
 assign(output,'librarian.out');
 rewrite(output);
 readln(n,q);
 for i:=1 to n do readln(a[i]);
 for i:=1 to n-1 do
  begin
   k:=i;
   for j:=i+1 to n do
    if a[j]<a[k] then k:=j;
   if k<>i then
    begin
     t:=a[i]; a[i]:=a[k]; a[k]:=t;
    end;
  end;
 for i:=1 to q do readln(c[i],b[i]);
 for i:=1 to q do
  begin
   x:=1; f:=true; t:=10000001;
   for j:=1 to c[i] do x:=x*10;
   for j:=1 to n do
    if (a[j]mod x)=b[i] then
     begin
      f:=false;
      if a[j]<t then t:=a[j];
     end;
   if f then writeln(-1) else writeln(t);
  end;
 close(input); close(output);
end.
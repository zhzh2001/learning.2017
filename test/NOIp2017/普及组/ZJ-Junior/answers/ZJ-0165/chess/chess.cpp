#include<cstdio>
#include<algorithm>
using namespace std;
const int inf=2100000000;
const int d[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
int n,m,a[101][101],ans,x,y,c;
bool b[101][101];
void w(int x,int y,int lo,bool isch)
{
	int xi,yi;
	if(x==m&&y==m)
	{
		ans=min(ans,lo);
		return;
	}
	if(lo>=ans) return;
	for(int i=0;i<4;i++)
	{
		xi=x+d[i][0];yi=y+d[i][1];
		if(xi>0&&xi<=m&&yi>0&&yi<=m&&!b[xi][yi])
		{
			b[xi][yi]=true;
			if(a[xi][yi]!=0)
			{
				if(a[xi][yi]!=a[x][y]) w(xi,yi,lo+1,false);
				else w(xi,yi,lo,false);
			}
			else
			{
				if(!isch)
				{
					a[xi][yi]=a[x][y];
					w(xi,yi,lo+2,true);
					a[xi][yi]=0;
				}
			}
			b[xi][yi]=false;
		}
	}
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d%d",&x,&y,&c);
		if(c==0) c=2;
		a[x][y]=c;
	}
	ans=inf;
	w(1,1,0,false);
	if(ans==inf) printf("-1");
	else printf("%d",ans);
	return 0;
}

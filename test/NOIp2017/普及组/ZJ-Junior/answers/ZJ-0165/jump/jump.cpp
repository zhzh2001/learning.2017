#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
inline void read(int &x)
{
	char ch=0;int w=1;x=0;
	while(ch^'-'&&!isdigit(ch)) ch=getchar();
	if(ch=='-') w=-1,ch=getchar();
	while(isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	x*=w;
	return;
}
const int inf=2100000000;
int l,r,mid,ans,n,d,k,x[500001],s[500001];
int w(int a,int b,int c)
{
	if(c>=k) return 1;
	if(a==n) return 0;
	bool bo=false;
	int i=1;
	while(x[a+i]-x[a]<d-b&&a+i<=n) i++;
	while(x[a+i]-x[a]<=d+b&&a+i<=n)
	{
		bo=bo||w(a+i,b,c+s[a+i]);
		i++;
	}
	return bo;
}
int judge(int m)
{
	for(int i=1;i<=n;i++) if(x[i]-x[i-1]>d+m) return 0;
	return w(0,m,0);
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	read(n);
	read(d);
	read(k);
	for(int i=1;i<=n;i++)
	{
		read(x[i]);
		read(s[i]);
	}
	l=0;r=x[n]+1;ans=-1;
	while(l<=r)
	{
		mid=(l+r)/2;
		if(judge(mid)) ans=mid,r=mid-1;
		else l=mid+1;
	}
	printf("%d",ans);
	return 0;
}

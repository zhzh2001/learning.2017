var a:array[1..10000] of longint;
procedure qsort(l,r:longint);
var   t,mid,i,j:longint;
begin
   i:=l; j:=r;
   mid:=a[(l+r) div 2];
   while i<=j do
   begin
     while a[i]>mid do inc(i);
     while a[j]<mid do dec(j);
     if i<=j then
     begin
       t:=a[i]; a[i]:=a[j]; a[j]:=t; inc(i); dec(j);
     end;
   end;
   if l<j then qsort(l,j);
   if i<r then qsort(i,r);
end;
var b,c,ans:array[1..10000] of longint;
    d,i,j,n,m,k:longint;
begin
   assign(input,'librarian.in'); reset(input);
   assign(output,'librarian.out'); rewrite(output);
   readln(n,m);
   for i:=1 to n do readln(a[i]);
   qsort(1,n);
   for i:=1 to m do readln(b[i],c[i]);
   for i:=1 to m do
     for j:=1 to n do
     begin
        d:=1;
        for k:=1 to b[i] do
        d:=d*10;
        d:=a[j] mod d;
        if d=c[i] then ans[i]:=a[j];
     end;
   for i:=1 to n do
   if ans[i]=0 then writeln(-1) else writeln(ans[i]);
   close(input);
   close(output);
end.

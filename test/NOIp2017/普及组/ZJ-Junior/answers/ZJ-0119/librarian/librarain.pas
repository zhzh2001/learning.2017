var
  n,m,h,q,t:longint;
  c:char;
  a:array[1..1000] of longint;
  b,s:array[1..1000] of string;
function bk(x,y:string):boolean;
var
  i:longint;
begin
  if length(y)<length(x) then exit(false);
  for i:=1 to length(x) do
    if y[i]<>x[length(x)-i+1] then exit(false);
  exit(true);
end;
procedure sort(l,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for h:=1 to n do
    readln(a[h]);
  sort(1,n);
  for h:=1 to m do begin
    read(q);read(c);readln(b[h]);
  end;
  for h:=1 to n do begin
    q:=a[h];t:=0;
    while q>0 do begin
      s[h]:=s[h]+chr(q mod 10+ord('0'));
      q:=q div 10;
    end;
  end;
  for h:=1 to m do begin
    t:=1;
    while (not bk(b[h],s[t]))and(t<=n) do inc(t);
    if t<=n then begin
      for q:=1 to length(s[t]) do
        write(s[t,length(s[t])-q+1]);
      writeln;
    end
    else writeln(-1);
  end;
  close(input);close(output);
end.

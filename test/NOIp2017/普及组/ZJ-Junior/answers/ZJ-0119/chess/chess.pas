var
  a,b:array[1..100,1..100] of longint;
  min,x,y,c,n,m,i,j,s:longint;
  pd:boolean;
procedure dfs(p,q:longint);
begin
  if (p=m)and(q=m) then begin
    if s<min then min:=s;
  end
  else begin
    b[p,q]:=1;
    if (p<m)and(b[p+1,q]=0) then begin
        if a[p+1,q]=a[p,q] then begin
          pd:=true;dfs(p+1,q);
        end
        else if (a[p+1,q]<>a[p,q])and(a[p+1,q]<>0) then begin
          inc(s);pd:=true;dfs(p+1,q);dec(s);
        end
        else if pd then begin
          pd:=false;
          a[p+1,q]:=a[p,q];
          s:=s+2;
          dfs(p+1,q);
          s:=s-2;
          a[p+1,q]:=0;
          pd:=true;
        end;
    end;
    if (q<m)and(b[p,q+1]=0) then begin
      if a[p,q+1]=a[p,q] then begin
        pd:=true;dfs(p,q+1);
      end
      else if (a[p,q+1]<>a[p,q])and(a[p,q+1]<>0) then begin
        inc(s);pd:=true;dfs(p,q+1);dec(s);
      end
      else if pd then begin
        pd:=false;
        a[p,q+1]:=a[p,q];
        s:=s+2;
        dfs(p,q+1);
        s:=s-2;
        a[p,q+1]:=0;
        pd:=true;
      end;
    end;
    if (p>1)and(b[p-1,q]=0) then begin
        if a[p-1,q]=a[p,q] then begin
          pd:=true;dfs(p-1,q);
        end
        else if (a[p-1,q]<>a[p,q])and(a[p-1,q]<>0) then begin
          inc(s);pd:=true;dfs(p-1,q);dec(s);
        end
        else if pd then begin
          pd:=false;
          a[p-1,q]:=a[p,q];
          s:=s+2;
          dfs(p-1,q);
          s:=s-2;
          a[p-1,q]:=0;
          pd:=true;
        end;
    end;
    if (q>1)and(b[p,q-1]=0) then begin
        if a[p,q-1]=a[p,q] then begin
          pd:=true;dfs(p,q-1);
        end
        else if (a[p,q-1]<>a[p,q])and(a[p,q-1]<>0) then begin
          inc(s);pd:=true;dfs(p,q-1);dec(s);
        end
        else if pd then begin
          pd:=false;
          a[p,q-1]:=a[p,q];
          s:=s+2;
          dfs(p,q-1);
          s:=s-2;
          a[p,q-1]:=0;
          pd:=true;
        end;
    end;
    b[p,q]:=0;
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);min:=maxlongint;pd:=true;
  for i:=1 to n do begin
    readln(x,y,c);
    a[x,y]:=c+1;
  end;
  if (a[m,m]=1)or(a[m,m]=2) then begin
    if (a[m,m-1]=0)and(a[m-1,m]=0)and(a[m,m-2]=0)and(a[m-2,m]=0)and(a[m-1,m-1]=0) then begin
      writeln(-1);halt;
    end;
  end
  else if (a[m,m-1]=0)and(a[m-1,m]=0) then begin
    writeln(-1);halt;
  end;
  dfs(1,1);
  writeln(min);
  close(input);close(output);
end.
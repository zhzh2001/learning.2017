var
 a:array[1..100,1..100]of longint;
 b:array[1..2,1..4]of longint=((0,1,0,-1),
                               (1,0,-1,0));
 c:array[1..100,1..100]of boolean;
 min,n,m,i,x,y,z:longint;
 procedure dfs(x,y,sum:longint;t:boolean);
 var
  x1,y1,k,i:longint;
 begin
  if (x=n)and(y=n) then begin
                         if sum<min then min:=sum;
                        end
                   else begin
                         k:=a[x,y];
                         if t then a[x,y]:=0;
                         for i:=1 to 4 do
                         begin
                          x1:=x+b[1,i];
                          y1:=y+b[2,i];
                          if (x1<1)or(x1>n)or(y1<1)or(y1>n) then continue;
                          if c[x1,y1] then continue;
                          if a[x1,y1]<>0 then begin
                                               c[x1,y1]:=true;
                                               if a[x1,y1]<>k then dfs(x1,y1,sum+1,false)
                                                              else dfs(x1,y1,sum,false);
                                               c[x1,y1]:=false;
                                              end
                                        else begin
                                              if t then continue;
                                              c[x1,y1]:=true;
                                              a[x1,y1]:=1;
                                              if a[x,y]<>a[x1,y1] then dfs(x1,y1,sum+3,true)
                                                                  else dfs(x1,y1,sum+2,true);
                                              a[x1,y1]:=2;
                                              if a[x,y]<>a[x1,y1] then dfs(x1,y1,sum+3,true)
                                                                  else dfs(x1,y1,sum+2,true);
                                              c[x1,y1]:=false;
                                             end;
                         end;
                        end;
 end;
begin
 assign(input,'chess.in');
 assign(output,'chess.out');
 reset(input);
 rewrite(output);
 readln(n,m);
 for i:=1 to m do
 begin
  readln(x,y,z);
  a[x,y]:=z+1;
 end;
 if m<n then begin
              writeln(-1);
              close(input);
              close(output);
              halt;
             end;
 min:=maxlongint;
 c[1,1]:=true;
 dfs(1,1,0,false);
 if min=maxlongint then writeln(-1)
                   else writeln(min);
 close(input);
 close(output);
end.

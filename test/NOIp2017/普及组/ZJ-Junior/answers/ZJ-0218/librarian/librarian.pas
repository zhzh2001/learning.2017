var
 n,q,i,j,len,t,min:longint;
 a:array[1..1000]of longint;
 function pd(m,x,y:longint):boolean;
 var
  i:longint;
 begin
  for i:=1 to m do
  begin
   if x mod 10<>y mod 10 then exit(false);
   x:=x div 10;
   y:=y div 10;
  end;
  exit(true);
 end;
begin
 assign(input,'librarian.in');
 assign(output,'librarian.out');
 reset(input);
 rewrite(output);
 readln(n,q);
 for i:=1 to n do
 begin
  readln(a[i]);
 end;
 for i:=1 to q do
 begin
  readln(len,t);
  min:=maxlongint;
  for j:=1 to n do
   if (pd(len,t,a[j]))and(a[j]<min) then min:=a[j];
  if min<>maxlongint then writeln(min)
                     else writeln(-1);
 end;
 close(input);
 close(output);
end.
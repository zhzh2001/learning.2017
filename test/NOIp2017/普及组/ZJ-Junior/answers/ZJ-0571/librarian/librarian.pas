var i,j,k,l,m,n,s:longint;
    t:array[0..1001]of string;
    ans,p:string;

function check(a,b:string):boolean;
  begin
    if length(a)<length(b) then exit(true)
    else if length(a)>length(b) then exit(false)
         else if a<b then exit(true) else exit(false);
  end;

procedure sort(l,r:longint);
var i,j:longint;
    x,y:string;
  begin
    i:=l;
    j:=r;
    x:=t[(i+j)div 2];
    while i<j do
      begin
        while check(t[i],x) do inc(i);
        while check(x,t[j]) do dec(j);
        if i<=j then
          begin
            y:=t[i]; t[i]:=t[j]; t[j]:=y; inc(i); dec(j);
          end;
      end;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
  end;

begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(t[i]);
  sort(1,n);
  for i:=1 to m do
    begin
      read(s);
      readln(p);
      delete(p,1,1);
      //writeln(p);
      ans:='-1';
      for j:=1 to n do
        if p=copy(t[j],length(t[j])-s+1,s) then begin ans:=t[j]; break; end;
      writeln(ans);
    end;
  close(input);
  close(output);
end.

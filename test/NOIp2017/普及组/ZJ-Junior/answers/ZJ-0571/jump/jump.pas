type node=record
    x,s:longint;
  end;
var i,j,k,l,m,n,d,maxmin:longint;
    jump:array[0..500001]of node;
    max,min:array[0..500001]of int64;

procedure work(t,f,g:longint);
var i,j:longint;
  begin
    if f>=k then
      begin
        writeln(g);
        close(input);
        close(output);
        halt;
      end;
    if t>n then
      exit;
    for i:=t to n do
      if jump[i].x-jump[t-1].x>=d-g then
        begin
          //writeln(jump[i].x-jump[t-1].x,' ',d-g);
          if jump[i].x-jump[t-1].x<=d+g then
            work(i+1,f+jump[i].s,g);
        end;
    work(1,0,g+1);
  end;

begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  fillchar(jump,sizeof(jump),0);
  for i:=1 to n do begin max[i]:=-maxlongint; min[i]:=maxlongint; end;
  max[0]:=0;min[0]:=0;
  maxmin:=maxlongint;
  readln(n,d,k);
  for i:=1 to n do
    begin
      readln(jump[i].x,jump[i].s);
      if jump[i].s+max[i-1]>max[i] then max[i]:=jump[i].s+max[i-1];
      if jump[i].x-jump[i-1].x<min[i] then min[i]:=jump[i].x-jump[i-1].x;
      if min[i]<maxmin then maxmin:=min[i];
    end;
  if max[n]<k then begin writeln(-1); close(input); close(output); halt; end;
  if d<maxmin then work(1,0,maxmin-d) else work(1,0,0);
  close(input);
  close(output);
end.


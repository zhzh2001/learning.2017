var i,j,k,l,m,n,x,y:longint;
    f,map:array[0..101,0..101]of longint;
    x1:array[1..4]of integer=(0,1,0,-1);
    y1:array[1..4]of integer=(-1,0,1,0);

procedure work(x,y,s,c,magic:longint);
var i,j:longint;
  begin
    if s<f[x,y] then begin f[x,y]:=s; {writeln(x,' ',y,' ',s);} end else exit;
    for i:=1 to 4 do
      if (x+x1[i]<=m)and(x+x1[i]>=1)and(y+y1[i]<=m)and(y+y1[i]>=1) then
        begin
          if (map[x+x1[i],y+y1[i]]=c)and(map[x+x1[i],y+y1[i]]>=0) then
            work(x+x1[i],y+y1[i],s,map[x+x1[i],y+y1[i]],0);
          if (map[x+x1[i],y+y1[i]]<>c)and(map[x+x1[i],y+y1[i]]>=0) then
            work(x+x1[i],y+y1[i],s+1,map[x+x1[i],y+y1[i]],0);
          if (map[x+x1[i],y+y1[i]]<0)and(magic=0) then
            begin
              work(x+x1[i],y+y1[i],s+2,0,1);
              work(x+x1[i],y+y1[i],s+2,1,1);
            end;
        end;
  end;

begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      begin
        map[i,j]:=-1;
        f[i,j]:=maxlongint;
      end;
  for i:=1 to n do
    begin
      readln(x,y,k);
      map[x,y]:=k;
    end;
  work(1,1,0,map[1,1],0);
  //for i:=1 to m do
    //begin
      //for j:=1 to m do
        //write(f[i,j],' ');
      //writeln;
    //end;
  if f[m,m]=maxlongint then writeln(-1)
    else writeln(f[m,m]);
  close(input);
  close(output);
end.

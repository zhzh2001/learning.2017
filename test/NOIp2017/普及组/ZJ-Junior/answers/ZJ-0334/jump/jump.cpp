#include<bits/stdc++.h>
#include<cstdio>
#include<iostream>
#define MAXN 500005
#define INF 0x7f7f7f7f
using namespace std;
int n,d,k,x[MAXN],s[MAXN],ans,sum=0;
int score(int g){//花费为g时得到的最大分数 
	int max_f=0,now=1,f[MAXN]={0},up=d+g,down=max(d-g,1);
	for (int i=1;i<=n;i++){
		for (int j=0;j<i;j++)
			if (x[i]-x[j]<=up&&x[i]-x[j]>=down)
				f[i]=max(f[i],f[j]+s[i]);
	}
	for (int i=1;i<=n;i++)
		max_f=max(max_f,f[i]);
	return max_f;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	for (int i=1;i<=n;i++)
		cin>>x[i]>>s[i],sum+=s[i]>0?s[i]:0;
	if (sum<k){
		cout<<-1<<endl;
		fclose(stdin); fclose(stdout);
		return 0;
	}
	x[0]=0; s[0]=0;
	int l=1,r=x[n]-d,mid;
	bool flag=0;
	while(l<r){
		mid=(l+r+1)/2;
		if (score(mid)>=k){
			ans=mid;
			flag=1;
			r=mid-1;
		}
		else
			l=mid+1;
	}
	if (flag) cout<<ans<<endl;
		else cout<<-1<<endl;
	fclose(stdin); fclose(stdout);
	return 0;
}


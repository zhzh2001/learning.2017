#include<bits/stdc++.h>
#include<cstdio>
#include<iostream>
#include<queue>
#define MAXN 1005
#define MAXM 105
#define INF 0x7f7f7f7f
using namespace std;
int color[MAXM][MAXM],f[MAXM][MAXM];
int mx[4]={1,0,-1,0};
int my[4]={0,1,0,-1};
bool vis[MAXM][MAXM]={0};
int n,m,ans=INF;
void dfs(int x,int y,int colour,bool used,int sum){
	if (sum>=ans||sum>=f[x][y]) return;
	f[x][y]=sum;
	if (x==m&&y==m){
		ans=sum;
		return;
	}
	for (int i=0;i<4;i++){
		int m_x=x+mx[i],m_y=y+my[i];
		if (x>m||y>m||x<1||y<1) continue;
		if (color[m_x][m_y]==-1){
			if (!used) dfs(m_x,m_y,colour,1,sum+2);
		}
		else{
			if (color[m_x][m_y]!=colour) dfs(m_x,m_y,color[m_x][m_y],0,sum+1);
				else dfs(m_x,m_y,colour,0,sum);
		}
	}
	return;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	for (int i=1;i<=m;i++)
		for (int j=1;j<=m;j++)
			color[i][j]=-1,f[i][j]=INF;
	int x,y,c;
	for (int i=1;i<=n;i++){
		cin>>x>>y>>c;
		color[x][y]=c;
	}
	dfs(1,1,color[1][1],0,0);
	if (ans!=INF) cout<<ans<<endl;
		else cout<<-1<<endl;
	fclose(stdin); fclose(stdout);
	return 0;
}

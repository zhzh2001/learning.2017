#include<bits/stdc++.h>
#include<cstdio>
#include<iostream>
#define MAXN 1005
using namespace std;
int n,q,a[MAXN],k[MAXN][15];
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>n>>q;
	for (int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	while (q--){
		int len,mod=1,x,flag=1;
		cin>>len>>x;
		for (int i=1;i<=len;i++)
			mod*=10;
		for (int i=1;i<=n;i++)
			if (a[i]%mod==x){
				cout<<a[i]<<endl;
				flag=0;
				break;
			}
		if (flag) cout<<"-1"<<endl;
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

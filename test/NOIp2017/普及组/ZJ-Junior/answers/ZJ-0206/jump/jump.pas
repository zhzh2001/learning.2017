program jump;
 var
  n,d,k,i,j:longint;
  sum:longint;
  xmax:longint;
  x,s:array[0..500355]of longint;
  l,r,mid:longint;
 procedure down(x:boolean);
  begin
   if not x then writeln(-1)
   else writeln(sum);
   close(input);close(output);
   halt;
  end;
 procedure init;
  var
   i,j:longint;
   cnt:qword;
  begin
   readln(n,d,k);
   fillchar(x,sizeof(x),0);
   fillchar(s,sizeof(s),0);
   cnt:=0;sum:=0;
   for i:=1 to n do
    begin
     readln(x[i],s[i]);
     if s[i]>0 then cnt:=cnt+s[i];
    end;
   xmax:=x[n];
   if cnt<k then down(false);
  end;
 function max(a,b:longint):longint;
  begin
   if a>b then exit(a);
   exit(b);
  end;
 function check(pop:longint):boolean;
  var
   now,la,l,r,a,b:longint;
   score:longint;
   rec,y:longint;
  begin
   l:=max(d-pop,1);r:=d+pop;
   now:=0;score:=0;la:=0;
   while (now<xmax) do
    begin
     a:=la;b:=la;
     while (x[a]<now+l)and(a<n) do inc(a);
     if (x[a]<now+l)or(x[a]>now+r) then exit(false);
     while (s[a]<0)and(x[a]<=now+r)and(a<n) do inc(a);
     if x[a]>now+r then
      begin
       dec(a);
       inc(score,s[a]);now:=x[a];la:=a;
       continue;
      end;
     if s[a]>=0 then
      begin
       inc(score,s[a]);now:=x[a];la:=a;
       continue;
      end;
     if a=n then break;
    end;
   if (score>=k)
    then exit(true);
   now:=0;score:=0;la:=0;
   while (now<xmax) do
    begin
     a:=la;b:=la;y:=-maxlongint;rec:=0;
     while (x[a]<now+l)and(a<n) do inc(a);
     if (x[a]<now+l)or(x[a]>now+r) then exit(false);
     while (s[a]<0)and(x[a]<=now+r)and(a<n) do
      begin
       if s[a]>y then
        begin
         y:=s[a];
         rec:=a;
        end;
       inc(a);
      end;
     if x[a]>now+r then
      begin
       a:=rec;
       inc(score,s[a]);now:=x[a];la:=a;
       continue;
      end;
     if s[a]>=0 then
      begin
       inc(score,s[a]);now:=x[a];la:=a;
       continue;
      end;
     if a=n then break;
    end;
   if (score>=k)
    then exit(true);
   exit(false);
  end;
 begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);rewrite(output);
  init;
  l:=0;r:=xmax;
  while l<r do
   begin
    mid:=(l+r)shr 1;
    if check(mid) then r:=mid-1
    else l:=mid+1;
   end;
  sum:=l;
  down(true);
 end.

program score;
 var
  a,b,c:longint;
  ans:longint;
 procedure init;
  begin
   readln(a,b,c);
   ans:=trunc((a*20+b*30+c*50)/100);
  end;
 begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);rewrite(output);
  init;
  writeln(ans);
  close(input);close(output);
 end.
program librarian;
 const
  size:array[1..8]of longint=(0,10,100,1000,10000,100000,1000000,10000000);
 var
  n,q,x,y,i,j,k,l:longint;
  map:array[0..10000355]of longint;
 function lens(x:longint):longint;
  var cnt:longint;
  begin
   cnt:=0;
   while x<>0 do
    begin
     x:=x div 10;
     inc(cnt);
    end;
   exit(cnt);
  end;
 procedure init;
  var i,j,k,l:longint;
  begin
   readln(n,q);
   for i:=0 to 10000355 do map[i]:=maxlongint;
   //fillchar(map,sizeof(map),255);
   for i:=1 to n do
    begin
     readln(j);l:=j;
     if j<map[j] then map[j]:=j;
     k:=lens(j);
     while k<>1 do
      begin
       while j>=size[k] do j:=j-size[k];
       dec(k);
       if map[j]>l then map[j]:=l;
      end;
    end;
  end;
 begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);rewrite(output);
  init;
  for i:=1 to q do
   begin
    readln(x,y);//write(y,':');
    if map[y]=maxlongint then writeln(-1)
    else writeln(map[y]);
   end;
  close(input);close(output);
 end.

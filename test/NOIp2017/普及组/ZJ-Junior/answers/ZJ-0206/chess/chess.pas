program chess;
 const
  dx:array[1..4]of longint=(0,1,-1,0);
  dy:array[1..4]of longint=(1,0,0,-1);
 var
  x,y,m,n,i,j,k,l,c:longint;
  f:array[0..103,0..103]of longint;
  v:array[0..103,0..103]of boolean;
  cnt:longint;
 function min(a,b:longint):longint;
  begin
   if a<b then exit(a);
   exit(b);
  end;
 procedure dfs(x,y,coin,code:longint;using:boolean);
  var
   i,j,tx,ty:longint;
   nowco:longint;
  begin
   if (x=m)and(y=m) then
    begin
     cnt:=min(cnt,coin);
     writeln(cnt);
     close(input);close(output);
     halt;
    end;
   if (x<1)or(x>m)or(y<1)or(y>m)or((f[x,y]=2)and(not using))or((using)and(f[x,y]<>2))
    then exit;
   if v[x,y] then exit;
   v[x,y]:=true;
   if using then nowco:=code
   else nowco:=f[x,y];
   for i:=1 to 4 do
    begin
     tx:=x+dx[i];
     ty:=y+dy[i];
     if (tx<1)or(ty<1)or(tx>m)or(ty>m)or(v[tx,ty]) then continue;
     if (f[tx,ty]<>nowco)and(f[tx,ty]<>2)
      then dfs(tx,ty,coin+1,0,false)
      else if f[tx,ty]=nowco then
       dfs(tx,ty,coin,0,false);
     if (not using)and(f[tx,ty]=2) then
      dfs(tx,ty,coin+2,nowco,true);
    end;
   v[x,y]:=false;
  end;
 begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to 103 do
   for j:=1 to 103 do
    f[i,j]:=2;
  fillchar(v,sizeof(v),false);
  for i:=1 to n do
   begin
    readln(x,y,c);
    f[x,y]:=c;
   end;
  cnt:=maxlongint;
  dfs(1,1,0,0,false);
  if cnt=maxlongint then cnt:=-1;
  writeln(cnt);
  close(input);close(output);
 end.

const maxn=1005;maxm=105;
var
  n,m,i,j,k,t,ed1,ed2,st,ed,inf,id,min,tot:longint;
  x,y,c,dis:array[0..maxn] of longint;
  vis:array[0..maxn] of boolean;
  g:array[0..maxn,0..maxn] of longint;
procedure init;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do begin
    readln(x[i],y[i],c[i]);
    if (x[i]=1)and(y[i]=1) then st:=i;
    if (x[i]=m)and(y[i]=m-1) then ed1:=i;
    if (x[i]=m-1)and(y[i]=m) then ed2:=i;
    if (x[i]=m)and(y[i]=m) then ed:=i;
  end;
end;

procedure main;
begin
  fillchar(g,sizeof(g),63);
  fillchar(dis,sizeof(dis),63);
  fillchar(vis,sizeof(vis),false);
  inf:=dis[1];
  for i:=1 to n do
  for j:=1 to n do if i<>j then begin
    if abs(x[i]-x[j])+abs(y[i]-y[j])=1
      then if c[i]<>c[j]
             then begin
               g[i,j]:=1;
               //writeln(i,'-->',j,':',1);
             end
             else begin
               g[i,j]:=0;
               //writeln(i,'-->',j,':',0);
             end;
    if abs(x[i]-x[j])+abs(y[i]-y[j])=2
      then if c[i]<>c[j]
             then begin
               g[i,j]:=3;
             end
             else begin
               g[i,j]:=2;
             end;
  end;
  for i:=1 to n do begin dis[i]:=g[st,i]; end;
  dis[st]:=0; vis[st]:=true; //writeln(ed);
  for i:=2 to n do begin
    min:=inf;id:=0;
    for j:=1 to n do if (not vis[j])and(dis[j]<min) then begin
      min:=dis[j]; id:=j;
    end;
    if id=ed then break;
    vis[id]:=true;
    for j:=1 to n do if (not vis[j])and(dis[id]+g[id,j]<dis[j]) then
      dis[j]:=dis[id]+g[id,j];
  end;
end;

procedure print;
begin
  if (ed>0)and(dis[ed]<inf)
    then writeln(dis[ed])
    else if ed>0
           then writeln(-1)
           else if (dis[ed1]<inf)or(dis[ed2]<inf)
                  then begin
                    if dis[ed1]<dis[ed2]
                      then writeln(dis[ed1])
                      else writeln(dis[ed2]);
                  end
                  else writeln(-1);
  close(input);
  close(output);
end;

begin
  init;
  main;
  print;
end.

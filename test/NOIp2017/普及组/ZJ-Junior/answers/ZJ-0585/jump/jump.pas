const maxn=500005;
var
  i,j:longint;
  n,m,k,t,d,L,R,tot,ans,mid:int64;
  x,s,que,f:array[0..maxn] of int64;

procedure init;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do begin
    readln(x[i],s[i]);
    f[i]:=s[i];
    if s[i]>0 then inc(tot,s[i]);
  end;
end;

function check(g:int64):boolean;
var hed,til,min,max,now,st:int64;
begin
  fillchar(f,sizeof(f),192);
  st:=f[1];
  if g<d
    then min:=d-g
    else min:=1;
  max:=g+d;
  f[0]:=0;
  for i:=1 to n do begin
    for j:=i-1 downto 0 do
      if (x[j]<=x[i]-min)and(x[j]>=x[i]-max)and(f[j]<>-1)and(f[j]+s[i]>f[i])
        then f[i]:=f[j]+s[i];
    if f[i]>=k then exit(true);
    if f[i]=st then exit(false);
  end;
  exit(false);
end;

procedure main;
begin
  ans:=-1;
  if tot<k then exit;
  L:=0;R:=1000000000;
  while L<=R do begin
    mid:=L+(R-L+1)>>1;
    if check(mid)
      then begin
        ans:=mid;
        R:=mid-1
      end
      else L:=mid+1;
  end;
end;

procedure print;
begin
  writeln(ans);
  close(input);
  close(output);
end;

begin
  init;
  main;
  print;
end.

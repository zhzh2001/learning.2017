const maxn=1005;
      m:array[1..8] of longint=(10,100,1000,10000,100000,1000000,10000000,100000000);
var
  a:array[0..maxn] of longint;
  n,q,ans,i,j,k,id:longint;

procedure init;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do read(a[i]);
end;

procedure sort(L,R:longint);
var i,j,k,t:longint;
begin
  i:=L;j:=R;k:=a[random(R-L+1)+L];
  while i<=j do begin
    while a[i]<k do inc(i);
    while a[j]>k do dec(j);
    if i<=j then begin
      t:=a[i];a[i]:=a[j];a[j]:=t;
      inc(i); dec(j);
    end;
  end;
  if i<R then sort(i,R);
  if L<j then sort(L,j);
end;

procedure main;
begin
  sort(1,n);
  while q>0 do begin
    dec(q);
    read(id);
    readln(k);
    ans:=-1;
    for i:=1 to n do
      if a[i] mod m[id]=k
        then begin
          ans:=a[i];
          break;
        end;
    writeln(ans);
  end;
end;

procedure print;
begin
  close(input);
  close(output);
end;

begin
  init;
  main;
  print;
end.

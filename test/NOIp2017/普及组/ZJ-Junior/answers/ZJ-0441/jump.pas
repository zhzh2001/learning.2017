var
  max,min,n,d,k,i,j,ans,a,b,t:longint;
  x,s,dist:array[0..500] of longint;
procedure sort(l,r:longint);
var
  i,j,t,mid:longint;
begin
  i:=r;j:=l;mid:=x[(i+j) div 2];
  repeat
    while x[i]<mid do inc(i);
    while x[j]>mid do dec(j);
    if i<=j then
    begin
      t:=x[i];
      x[i]:=x[j];
      x[j]:=t;
      t:=s[i];
      s[i]:=s[j];
      s[j]:=t;
    end;
  until i>j;
  if i<r then sort(i,l);
  if l<j then sort(l,j);
end;
begin
assign(input,'jump.in');reset(input);
assign(output,'jump.out');rewrite(output);
  read(n,d,k);
  t:=0;
  for i:=1 to n do
  begin
    read(a,b);
    if b>0 then inc(ans,b);
    x[i]:=a;s[i]:=b;
  end;
  sort(1,n);
  if ans<k then begin writeln(-1);close(input);close(output);exit;end;
  ans:=0;dist[0]:=0;max:=-1;
  for i:=1 to n do
  begin
    dist[i]:=x[i]-x[i-1];
    if (dist[i]<=max) and (s[i]<0) then
    begin
      x[i]:=x[i-1];
      dist[i]:=dist[i-1];
    end;
    if max<dist[i] then max:=dist[i];
    if ans>=k then break;
  end;
  ans:=0;max:=-1;min:=maxlongint;
  for j:=1 to i do
  begin
    if ans<dist[j] then ans:=dist[j];
    if dist[j]<min then min:=dist[j];
  end;
  max:=max-d;min:=d-min;
  if max>min then ans:=max
  else ans:=min;
  writeln(ans);
close(input);close(output);
end.

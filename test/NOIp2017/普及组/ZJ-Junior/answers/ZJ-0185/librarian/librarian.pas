var
d,n,m,i,j,min,k,f:longint;
u:boolean;
a,b,c,l:array[0..1000] of longint;
e:array[0..1000,0..1000] of longint;
begin
assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to n do readln(a[i]);
for i:=1 to m do readln(b[i],c[i]);
for i:=1 to m do
  begin
  l[i]:=0;
  for j:=1 to n do
    begin
    d:=a[j]-c[i];
    u:=true;
    k:=0;
    while k<b[i] do
      begin
      f:=d mod 10;
      d:=d div 10;
      if f<>0 then u:=false;
      k:=k+1;
      end;
    if u then
      begin
      l[i]:=l[i]+1;
      e[i,l[i]]:=a[j];
      end;
    end;
  end;
for i:=1 to m do
  begin
  min:=1000000000;
  for j:=1 to l[i] do if e[i,j]<min then min:=e[i,j];
  if min=1000000000 then writeln(-1) else writeln(min);
  end;
close(input);
close(output);
end.
var
u,w:boolean;
n,m,i,r,l,v,min:longint;
a,b,c,e,f,o,p,q:array[0..100000] of longint;
d:array[-1..1000,-1..1000] of longint;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to m do readln(a[i],b[i],c[i]);
for i:=1 to m do d[a[i],b[i]]:=c[i]+1;
r:=1;
l:=1;
e[1]:=1;
f[1]:=1;
o[1]:=0;
p[1]:=d[1,1];
if d[n,n]=0 then w:=true;
while r<=l do
  begin
  u:=false;
  d[e[r],f[r]]:=0;
  if (d[e[r]+1,f[r]]<>0) and (e[r]+1<=n) then
    begin
    u:=true;
    l:=l+1;
    e[l]:=e[r]+1;
    f[l]:=f[r];
    if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+1 else o[l]:=o[r];
    p[l]:=d[e[l],f[l]];
    end;
  if (d[e[r]-1,f[r]]<>0) and (e[r]-1>=1) then
    begin
    u:=true;
    l:=l+1;
    e[l]:=e[r]-1;
    f[l]:=f[r];
    if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+1 else o[l]:=o[r];
    p[l]:=d[e[l],f[l]];
    end;
  if (d[e[r],f[r]+1]<>0) and (f[r]+1<=n) then
    begin
    u:=true;
    l:=l+1;
    e[l]:=e[r];
    f[l]:=f[r]+1;
    if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+1 else o[l]:=o[r];
    p[l]:=d[e[l],f[l]];
    end;
  if (d[e[r],f[r]-1]<>0) and (f[r]-1>=1) then
    begin
    u:=true;
    l:=l+1;
    e[l]:=e[r];
    f[l]:=f[r]-1;
    if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+1 else o[l]:=o[r];
    p[l]:=d[e[l],f[l]];
    end;
  if u=false then
    begin
    if (d[e[r]+1,f[r]+1]<>0) and (e[r]+1<=n) and (f[r]+1<=n) then
      begin
      l:=l+1;
      e[l]:=e[r]+1;
      f[l]:=f[r]+1;
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r]-1,f[r]+1]<>0) and (e[r]-1>=1) and (f[r]+1<=n) then
      begin
      l:=l+1;
      e[l]:=e[r]-1;
      f[l]:=f[r]+1;
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r]-1,f[r]-1]<>0) and (e[r]-1>=1) and (f[r]-1>=1) then
      begin
      l:=l+1;
      e[l]:=e[r]-1;
      f[l]:=f[r]-1;
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r]+1,f[r]-1]<>0) and (e[r]+1<=n) and (f[r]-1>=1) then
      begin
      l:=l+1;
      e[l]:=e[r]+1;
      f[l]:=f[r]-1;
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r]+2,f[r]]<>0) and (e[r]+2<=n) then
      begin
      u:=true;
      l:=l+1;
      e[l]:=e[r]+2;
      f[l]:=f[r];
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r]-2,f[r]]<>0) and (e[r]-2>=1) then
      begin
      u:=true;
      l:=l+1;
      e[l]:=e[r]-2;
      f[l]:=f[r];
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r],f[r]+2]<>0) and (f[r]+2<=n) then
      begin
      u:=true;
      l:=l+1;
      e[l]:=e[r];
      f[l]:=f[r]+2;
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    if (d[e[r],f[r]-2]<>0) and (f[r]-2>=1) then
      begin
      u:=true;
      l:=l+1;
      e[l]:=e[r];
      f[l]:=f[r]-2;
      if d[e[l],f[l]]<>p[r] then o[l]:=o[r]+3 else o[l]:=o[r]+2;
      p[l]:=d[e[l],f[l]];
      end;
    end;
  if w then
    begin
    if (e[r]=n-1) and (f[r]=n) then
      begin
      v:=v+1;
      q[v]:=o[r]+2;
      end;
    if (e[r]=n) and (f[r]=n-1) then
      begin
      v:=v+1;
      q[v]:=o[r]+2;
      end;
    end
  else
    if (e[r]=n) and (f[r]=n) then
      begin
      v:=v+1;
      q[v]:=o[r];
      end;
  r:=r+1;
  end;
min:=1000000000;
for i:=1 to v do if q[i]<min then min:=q[i];
if min=1000000000 then writeln(-1) else writeln(min);
close(input);
close(output);
end.
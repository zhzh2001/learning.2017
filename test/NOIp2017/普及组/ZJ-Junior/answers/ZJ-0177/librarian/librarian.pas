var
  a:array[1..1000] of longint;
  s:array[1..1000] of string;
  st,s1:string;
  i,j,k,l,n,w,q:longint;
  b:boolean;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  for i:=1 to n-1 do
    begin
      l:=i;
      for j:=i+1 to n do
        if a[j]<a[l] then l:=j;
      k:=a[i]; a[i]:=a[l]; a[l]:=k;
    end;
  for i:=1 to n do str(a[i],s[i]);
  for i:=1 to q do
    begin
      readln(k,w);
      str(w,st);
      b:=false;
      for j:=1 to n do
        if copy(s[j],length(s[j])-k+1,k)=st then
        begin
          b:=true;
          writeln(s[j]);
          break;
        end;
      if not b then writeln(-1);
    end;
  close(input);
  close(output);
end.

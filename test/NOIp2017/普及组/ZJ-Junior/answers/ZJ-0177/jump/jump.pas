var
  a,b,f:array[0..500000] of int64;
  i:longint;
  d,k,n,t,ta,min,inf:int64;
function pd(x:int64):boolean;
var
  i,j:longint;
  mind,maxd,max:int64;
begin
  if x<d then mind:=d-x else mind:=1;
  maxd:=d+x;
  f[0]:=0;
  for i:=1 to n do
    begin
      max:=-inf;
      for j:=i-1 downto 0 do
        if (b[i]-b[j]<=maxd) and (b[i]-b[j]>=mind) then
        begin
          if f[j]>max then max:=f[j];
        end
        else break;
      f[i]:=max+a[i];
      if f[i]>=k then exit(true);
    end;
  exit(false);
end;
procedure find(top,bot:int64);
var
  mid:int64;
begin
  if top>bot then exit;
  mid:=(top+bot) div 2;
  if pd(mid) then begin if mid<min then min:=mid; find(top,mid-1); end
  else find(mid+1,bot);
end;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  a[0]:=0;
  b[0]:=0;
  for i:=1 to n do
    begin
      readln(b[i],a[i]);
      t:=t+b[i];
      if a[i]>0 then ta:=ta+a[i];
    end;
  inf:=1000000000000000;
  min:=inf;
  if ta<k then begin writeln(-1); close(input); close(output); exit; end;
  find(1,t);
  if min=inf then writeln(-1) else writeln(min);
  close(input);
  close(output);
end.

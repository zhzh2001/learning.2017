const
  dx:array[1..4] of longint=(-1,0,1,0);
  dy:array[1..4] of longint=(0,-1,0,1);
var
  a,f:array[0..1000,0..1000] of longint;
  b,c,d,e,g:array[1..1000000] of longint;
  i,j,x,y,inf,m,n,f1,r,cx,kf,kr:longint;
procedure kz(x,y,t,u,v:longint);
var
  i,k,p,q:longint;
begin
  if (a[x,y]<>2) and (t>f[x,y]) then exit;
  if t<f[x,y] then f[x,y]:=t;
  for i:=1 to 4 do
    begin
      p:=x+dx[i];
      q:=y+dy[i];
      if (p=u) and (q=v) then continue;
      k:=t;
      if (p<1) or (p>m) or (q<1) or (q>m) then continue;
      if (a[x,y]=2) and (a[p,q]=2) then continue;
      if a[p,q]=2 then k:=k+2;
      if (a[x,y]=2) and (a[u,v]<>a[p,q]) then k:=k+1;
      if a[x,y]+a[p,q]=1 then inc(k);
      b[r]:=p; c[r]:=q; d[r]:=k; e[r]:=x; g[r]:=y;
      inc(r); if r>m*m then begin r:=1; inc(kr); end;
    end;
end;
procedure bfs;
begin
  b[1]:=1; c[1]:=1; d[1]:=0; e[1]:=0; g[1]:=0;
  f1:=1; r:=2;
  repeat
    kz(b[f1],c[f1],d[f1],e[f1],g[f1]);
    inc(f1);
    if f1>m*m then begin f1:=1; inc(kf); end;
  until f1+m*m*kf=r+m*m*kr;
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  inf:=maxlongint div 3;
  for i:=1 to m do
    for j:=1 to m do
      begin a[i,j]:=2; f[i,j]:=inf; end;
  for i:=1 to n do
    begin
      readln(x,y,cx);
      a[x,y]:=cx;
    end;
  bfs;
  if f[m,m]=inf then writeln(-1) else writeln(f[m,m]);
  close(input);
  close(output);
end.

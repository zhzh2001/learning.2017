#include<iostream>
#include<map>
using namespace std;
int n,d,k,w[500010],c[500010];
int DP(int b,int e){
    int f[500010];
    for(int i=1;i<=n;i++){
    	f[i]=-0x7fffff;
	}
	for(int i=1;i<=n;i++){
		for(int j=0;j<i;j++){
			if((w[i]-w[j])>=b && (w[i]-w[j])<=e)
			    f[i]=max(f[i],f[j]+c[i]);
		}
	}
	return f[n];
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	for(int i=1;i<=n;i++){
		scanf("%d%d",&w[i],&c[i]);
	}
	for(int i=0;i<=w[n];i++){
		if(i<d){
			int ans=DP(d-i,d+i);
			if(ans>=k){
				cout<<i<<endl;
				return 0;
			}
		}
		else {
			int ans=DP(1,d+i);
            if(ans>=k){
				cout<<i<<endl;
				return 0;
			}
		}
	}
	cout<<-1<<endl;
	return 0;
}

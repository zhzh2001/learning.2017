#include<iostream>
using namespace std;
int map[110][110],hx[110][110],zz[110][110],m,n,ql,dx[]={0,1,0,-1,0},dy[]={0,0,1,0,-1};
void DFS(int h,int z,int mf,int q){
	if(q>zz[h][z] || h<1 || h>m || z<1 || z>m)return;
	else zz[h][z]=q;
	hx[h][z]=1;
	for(int i=1;i<=4;i++){
		int xj=h+dx[i],yj=z+dy[i];
		if(hx[xj][yj])continue;
		if(map[xj][yj]==map[h][z]){
			DFS(xj,yj,1,q);
		}
		else if(map[xj][yj]==0){
			if(mf==0)continue;
			else {
				if(map[h][z]==1){
					map[xj][yj]=1;
				    DFS(xj,yj,0,q+2);
				    map[xj][yj]=2;
				    DFS(xj,yj,0,q+3);
				    map[xj][yj]=0;
				}
				else if(map[h][z]==2){
					map[xj][yj]=1;
				    DFS(xj,yj,0,q+3);
				    map[xj][yj]=2;
				    DFS(xj,yj,0,q+2);
				    map[xj][yj]=0;
				}
			}
		}
		else DFS(xj,yj,1,q+1);
	}
	hx[h][z]=0;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=m;j++){
			zz[i][j]=1e9;
		}
	}
	for(int i=1;i<=n;i++){
		int xx,yy,tt;
		scanf("%d%d%d",&xx,&yy,&tt);
		map[xx][yy]=tt+1;
	}
	DFS(1,1,1,0);
//	for(int i=1;i<=m;i++){
//		for(int j=1;j<=m;j++){
//			if(zz[i][j]==1e9)zz[i][j]=-1;
//			printf("%4d ",zz[i][j]);
//		}
//		cout<<endl;
//	}
    if(zz[m][m]==1e9)cout<<-1<<endl;
	else cout<<zz[m][m]<<endl;
}


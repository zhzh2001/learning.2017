var n,m,i,j,k,l,d,x,y,ans,k1,r,mid:longint;
    a,b,f:array[0..500000] of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
function judge(x:Longint):boolean;
var i,k,kk:longint;
begin
  for i:=1 to n do f[i]:=-maxlongint div 3;
  f[0]:=0;
  for i:=1 to n do
  begin
    j:=i-1;
    while (a[i]-a[j]<=x+d)and(a[i]-a[j]>=d-x)and(j>=0) do
    begin
      f[i]:=max(f[j]+b[i],f[i]);
      dec(j);
    end;
    if f[i]>k1 then exit(true);
  end;
  exit(false);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  read(n,d,k1);
  for i:=1 to n do read(a[i],b[i]);
  for i:=1 to n do
  if b[i]>0 then ans:=ans+b[i];
  if ans<k1 then
  begin
    write(-1);
    close(input);
    close(output);
    halt;
  end;
  ans:=0;
  a[0]:=0;
  l:=0;
  r:=a[n];
  while l<=r do
  begin
    mid:=(l+r) div 2;
    if judge(mid) then
    begin
      r:=mid-1;
      ans:=mid;
    end else l:=mid+1;
  end;
  write(ans);
  close(input);
  close(output);
end.

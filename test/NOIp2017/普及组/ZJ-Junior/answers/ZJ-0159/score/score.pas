var n,m,i,j,k,l:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(n,m,k);
  write(n*0.2+m*0.3+k*0.5:0:0);
  close(input);
  close(output);
end.
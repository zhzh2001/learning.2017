#include<cstdio>
#include<algorithm>
#define INF 2147483647
using namespace std;

struct usandbk{
	int len,num;
}us[1005],bk[1005];

bool cmp(usandbk a,usandbk b){
	return a.num<b.num;
}

int read(){
	int ret=0;
	bool neg=false;
	char c=getchar();
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-'){
		neg=true;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		ret=ret*10+c-'0';
		c=getchar();
	}
	if(neg)ret=-ret;
	return ret;
}

int calclen(int x){
	register int temp=x;
	register int ret=0;
	while(temp){
		ret++;
		temp/=10;
	}
	return ret;
}

int cf(int n,int num){
	int ret=1;
	for(register int i=1;i<=n;i++){
		ret*=num;
	}
	return ret;
}

int getlstdigit(int len,int num){
	int ret=(num%cf(len,10));
	//printf("the last %d digits of %d is %d\n",len,num,ret);
	return ret;
}

int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	register int n=read();
	register int q=read();
	for(register int i=1;i<=n;i++){
		bk[i].num=read();
		//printf("i=%d,book[i].num=%d,book[i].len=%d\n",i,bk[i].num,bk[i].len);
	}
	for(register int i=1;i<=q;i++){
		us[i].len=read();
		us[i].num=read();
		//printf("i=%d,book[i].num=%d,book[i].len=%d\n",i,us[i].num,us[i].len);
	}
	sort(bk+1,bk+n+1,cmp);
	for(register int i=1;i<=q;i++){
        bool DontHaveAns=true;
		for(register int j=1;j<=n;j++){
			int lent=getlstdigit(us[i].len,bk[j].num);
			if(lent==us[i].num){
				printf("%d\n",bk[j].num);
				DontHaveAns=false;
				break;
			}
		}
		if(DontHaveAns){
            printf("-1\n");
        }
	}
	return 0;
}

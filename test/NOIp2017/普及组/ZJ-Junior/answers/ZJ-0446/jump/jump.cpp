#include<cstdio>
#define max(a,b) a>b?a:b
using namespace std;

struct block{
	int dis,scr;
}bk[500005];

int read(){
	int ret=0;
	bool neg=false;
	char c=getchar();
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-'){
		neg=true;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		ret=ret*10+c-'0';
		c=getchar();
	}
	if(neg)ret=-ret;
	return ret;
}

int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	long long tot=0;
	register int n=read();
	register int d=read();
	register int k=read();
	for(register int i=1;i<=n;i++){
		bk[i].dis=read();
		bk[i].scr=read();
		tot+=bk[i].scr;
	}
	if(tot<k){
		printf("-1");
		return 0;
	}
	else if(d==1||tot==k){
		long long maxdis=0;
		for(register int i=1;i<=n;i++){
			maxdis=max(maxdis,(bk[i].dis-bk[i-1].dis));
		}
		printf("%d",maxdis);
		return 0;
	}
	else{
		printf("%d",d);
		return 0;
	}
}

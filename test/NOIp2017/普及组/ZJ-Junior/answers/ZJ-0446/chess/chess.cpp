#include<cstdio>
#include<algorithm>
#define INF 2147483647
using namespace std;

int mp[105][105],f[105][105];

int read(){
	int ret=0;
	bool neg=false;
	char c=getchar();
	while((c<'0'||c>'9')&&c!='-')c=getchar();
	if(c=='-'){
		neg=true;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		ret=ret*10+c-'0';
		c=getchar();
	}
	if(neg)ret=-ret;
	return ret;
}

int ans=INF,n,m;
int movx[]={0,0,-1,0,1};
int movy[]={0,-1,0,0,1};

void dfs(int depth,int x,int y){
	if(x==m&&y==m){
		ans=min(ans,depth);
		return;
	}
	for(int i=1;i<=4;i++){
		int nowx=x+movx[i];
		int nowy=y+movy[i];
		if(f[nowx][nowy]!=INF)continue;
		if(mp[nowx][nowy]==-1)continue;
		//printf("nowx=%d,nowy=%d,depth=%d\n",nowx,nowy,depth);
		f[nowx][nowy]=depth+(mp[nowx][nowy]==mp[x][y]?0:1);
		dfs(f[nowx][nowy],nowx,nowy);
		f[nowx][nowy]=INF;
	}
}

int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read();
	n=read();
	for(register int i=1;i<=m;i++){
		for(register int j=1;j<=m;j++){
			f[i][j]=INF;
			mp[i][j]=-1;
		}
	}
	for(register int i=1;i<=n;i++){
		int x=read();
		int y=read();
		mp[x][y]=read();
		if(mp[x][y]>=2)mp[x][y]=-1;
	}
	dfs(0,1,1);
	if(ans==INF)printf("-1");
	else printf("%d",ans);
	return 0;
}

var
  n,q,i,k,min,ans,j,x,ch1:longint;
  a,b:array[1..2000] of longint;
  ch,ch2:char;
  s:ansistring;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  for i:=1 to q do begin
    read(x);
    read(b[i]);
    readln;
  end;
  for i:=1 to q do begin
    str(b[i],s);
    min:=maxlongint;
    ans:=1;
    k:=length(s);
    for j:=1 to k do
      ans:=ans*10;
    for j:=1 to n do
      if a[j] mod ans=b[i] then
        if a[j]<min then min:=a[j];
    if min=maxlongint then writeln(-1)
                      else writeln(min);
  end;
  close(input);
  close(output);
end.


var
  n,m,min,i,j,sum,t,ch1:longint;
  b,c,d:array[1..1000] of longint;
  a:array[1..1000,1..1000] of longint;
  f:array[1..1000] of boolean;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  fillchar(a,sizeof(a),2);
  fillchar(f,sizeof(f),true);
  for i:=1 to m do
    readln(b[i],c[i],d[i]);
  for i:=1 to m-1 do
    for j:=1 to m do
      if b[j]<b[i] then begin
        t:=b[i];
        b[i]:=b[j];
        b[j]:=t;
        t:=c[i];
        c[i]:=c[j];
        c[j]:=t;
        t:=d[i];
        d[i]:=d[j];
        d[j]:=t;
      end
      else begin
        if (b[j]=b[i])and(c[j]<c[i]) then begin
          t:=b[i];
          b[i]:=b[j];
          b[j]:=t;
          t:=c[i];
          c[i]:=c[j];
          c[j]:=t;
          t:=d[i];
          d[i]:=d[j];
          d[j]:=t;
        end;
      end;
  if m<n then begin
    writeln('-1');
    exit;
  end;
  for i:=1 to m do
    a[b[i],c[i]]:=d[i];
  if m=n then begin
    for i:=1 to m do
       if b[i]<>c[i] then begin
         writeln('-1');
         exit;
       end;
  end;
  if ((c[m]-c[m-1]>=2)or(b[m]-b[m-1]>=2))and((c[m]<>c[m-1])and(b[m]<>b[m-1])) then begin
    writeln('-1');
    exit;
  end;
  writeln(114);
  close(input);
  close(output);
end.


#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;

int getpow(int x,int y)
{
	int w=0,t;
	for(int i=1;i<=x;i++)
		{
			t=y%10;
			y/=10;
			w=t*(int)pow(10,i-1)+w;
		}
	return w;
}

int n,m;
int a[10005],x,y,j;
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++)
	{
		bool flag=0;
		scanf("%d%d",&x,&y);
		for(j=1;j<=n;j++)
		{
			int p=getpow(x,a[j]);
			if(p==y) {flag=1; break;}
		}
		if(flag==1) cout<<a[j]<<endl; else cout<<-1<<endl;
	}
	return 0;
}

var
  n,q,i,j,k,t,ii:longint;
  a:array[1..1000]of longint;
  b:array[1..1000,1..3]of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
assign(input,'librarian.in');reset(input);
assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1to n do readln(a[i]);
  for j:=1to q do readln(b[j,1],b[j,2]);
  for j:=1to q do b[j,3]:=-1;
  sort(1,n);
  for j:=1to q do begin
    t:=1;
    for ii:=1to b[j,1]do t:=t*10;
    for i:=1to n do begin
      if a[i] mod t=b[j,2] then begin b[j,3]:=a[i];break;end;
    end;
  end;
  for j:=1to q do writeln(b[j,3]);
close(input);
close(output);
end.


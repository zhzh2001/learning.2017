#include<stdio.h>
#define inf 2147483647
int m,map[105][105],dir[4][2]={{1,0},{0,1},{-1,0},{0,-1}},low=inf,cnt=0,nb;
void go(int x,int y,int now,int up)
{
	int i,found=1,a,o;
	if(now>=low)
		return;
	if(map[m-1][m]==0&&map[m][m-1]==0)
		return;
	if(x==m&&y==m)
	{
		low=now;
		return;
	}
	for(i=0;i<4;i++)
	{
		if(i!=up&&map[x+dir[i][0]][y+dir[i][1]]==3&&map[x][y]!=3)
		{
			nb=map[x][y];
			a=map[x][y];
			map[x][y]=0;
			go(x+dir[i][0],y+dir[i][1],2+now,(i+2)%4);
			map[x][y]=a;
		}
		else if(i!=up&&map[x][y]==3&&map[x+dir[i][0]][y+dir[i][1]]!=3&&map[x+dir[i][0]][y+dir[i][1]]!=0)
		{
			if(map[x+dir[i][0]][y+dir[i][1]]==nb)
				o=0;
			else o=1;
			a=map[x][y];
			map[x][y]=0;
			go(x+dir[i][0],y+dir[i][1],o+now,(i+2)%4);
			map[x][y]=a;
		}
		else if(i!=up&&map[x+dir[i][0]][y+dir[i][1]]!=0&&map[x+dir[i][0]][y+dir[i][1]]!=3)
		{
			if(map[x][y]==map[x+dir[i][0]][y+dir[i][1]])
				o=0;
			else o=1;
			a=map[x][y];
			map[x][y]=0;
			go(x+dir[i][0],y+dir[i][1],o+now,(i+2)%4);
			map[x][y]=a;
		}
	}
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	int n,i,j,k,x,y,g;
	scanf("%d%d",&m,&n);
	if(m>=40)
		printf("-1\n");
	for(i=0;i<n;i++)
	{
		scanf("%d%d%d",&x,&y,&g);
		if(g==0)
			g=2;
		map[x][y]=g;
		for(k=0;k<4;k++)
			if(x+dir[k][0]<=m&&y+dir[k][1]<=m&&x+dir[k][0]>=1&&y+dir[k][1]>=1&&map[x+dir[k][0]][y+dir[k][1]]==0)
				map[x+dir[k][0]][y+dir[k][1]]=3;
	}
	go(1,1,0,0);
	if(low==inf)
		printf("-1\n");
	else
		printf("%d\n",low);
	return 0;
}

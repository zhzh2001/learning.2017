#include<bits/stdc++.h>
using namespace std;
int x[500001],s[500001],n;
int f[500001];
int dp(int l,int r)
{
	memset(f,0,sizeof(f));
	int top=1,t2=1;
	for(int i=1;i<=n;i++)
	{
		while(x[top]<x[i]-r+1)top++;
		while(x[t2]<x[i]-l+1)t2++;
		t2--;
		f[i]=s[i];
		for(int j=top;j<=t2;j++)if(i!=j)f[i]=max(f[i],s[i]+f[j]);
	}
	int maxx=0;
	for(int i=1;i<=n;i++)maxx=max(maxx,f[i]);
	return maxx;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	int d,k;
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)scanf("%d%d",&x[i],&s[i]);
	int count=0;
	for(int i=1;i<=n;i++)if(s[i]>0)count+=s[i];
	if(count<k)
	{
		cout<<-1;
		return 0;
	}
	int l=max(0,x[1]-d),r=1000000000;
	while(l<r)
	{
		int mid=(l+r)>>1;
		int p=max(1,d-mid),q=d+mid;
		int re=dp(p,q);
		if(re<k)l=mid+1;
		else r=mid;
	}
	int p=max(1,d-l+1),q=d+l-1;
	if(dp(p,q)>=k)l--;
	p=max(1,d-l),q=d+l;
	if(dp(p,q)<k)l++;
	printf("%d",l);
	return 0;
}

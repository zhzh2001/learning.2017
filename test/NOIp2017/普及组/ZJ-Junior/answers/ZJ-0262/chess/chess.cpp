#include<bits/stdc++.h>
using namespace std;
int a[101][101],n,r;
int f[101][101][2][2];
bool vis[101][101][2][2];
int qfj_find(int x,int y,int s)
{
	if(vis[x][y][s][a[x][y]])return f[x][y][s][a[x][y]];
	vis[x][y][s][a[x][y]]=true;
	if(x==n&&y==n)return f[x][y][s][a[x][y]]=0;
	const int m[4][2]={0,1,0,-1,1,0,-1,0};
	int ans=-1;
	for(int i=0;i<=3;i++)
	{
		int xx=x+m[i][0],yy=y+m[i][1];
		if(xx>0&&yy>0&&xx<=n&&yy<=n)
		{
			if(a[xx][yy]!=-1&&!vis[xx][yy][1][a[xx][yy]]&&!vis[xx][yy][0][a[xx][yy]])
			{
				int xxx=qfj_find(xx,yy,1);
				if(xxx!=-1)
				{
					if(ans==-1)
					{
						if(a[xx][yy]!=a[x][y])ans=xxx+1;
						else ans=xxx;
					}
					else 
					{
						if(a[xx][yy]!=a[x][y])ans=min(ans,xxx+1);
						else ans=min(ans,xxx);
					}
				}
			}
			else if(a[xx][yy]==-1)
			{
				if(s==1)
				{
					if(ans==-1)
					{
						if(!vis[xx][yy][0][0])
						{
							a[xx][yy]=0;
							int xxx=qfj_find(xx,yy,0);
							if(xxx!=-1)
							{
								if(a[xx][yy]!=a[x][y])ans=xxx+3;
								else ans=xxx+2;
							}	
						}
						if(!vis[xx][yy][0][1])
						{
							a[xx][yy]=1;
							int xxx=qfj_find(xx,yy,0);
							if(xxx!=-1)
							{
								if(a[xx][yy]!=a[x][y])ans=xxx+3;
								else ans=xxx+2;
							}
						}
						a[xx][yy]=-1;
					}
					else 
					{
						if(!vis[xx][yy][0][0])
						{
							a[xx][yy]=0;
							int xxx=qfj_find(xx,yy,0);
							if(xxx!=-1)
							{
								if(a[xx][yy]!=a[x][y]&&!vis[xx][yy][0][0])ans=min(ans,xxx+3);
								else ans=min(ans,xxx+2);
							}
						}
						if(!vis[xx][yy][0][1])
						{
							a[xx][yy]=1;
							int xxx=qfj_find(xx,yy,0);
							if(xxx!=-1)
							{
								if(a[xx][yy]!=a[x][y]&&!vis[xx][yy][0][1])ans=min(ans,xxx+3);
								else ans=min(ans,xxx+2);
							}
						}
						a[xx][yy]=-1;
					}
				}
			}
		}	
	}
	return f[x][y][s][a[x][y]]=ans;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,-1,sizeof(a));
	memset(f,0,sizeof(f));
	memset(vis,false,sizeof(vis));
	scanf("%d%d",&n,&r);
	for(int i=1;i<=r;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=z;
	}
	printf("%d",qfj_find(1,1,1));
	fclose(stdin);
	fclose(stdout);
	return 0;
}

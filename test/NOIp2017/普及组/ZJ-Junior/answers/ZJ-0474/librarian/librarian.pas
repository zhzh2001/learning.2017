var
n,q,i,j,len,x:longint;
a:array[1..1000]of longint;
f:boolean;
s,ts:string;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

begin
assign(input,'librarian.in');reset(input);
assign(output,'librarian.out');rewrite(output);
readln(n,q);
for i:=1 to n do readln(a[i]);
sort(1,n);
for i:=1 to q do
  begin
    readln(s);x:=pos(' ',s);f:=false;
    val(copy(s,1,x-1),len);
    s:=copy(s,pos(' ',s)+1,length(s));
    for j:=1 to n do
      begin
        str(a[j],ts);
        if copy(ts,length(ts)-len+1,length(ts))=s then
          begin
            f:=true;
            break;
          end;
      end;
    if f then writeln(a[j])else writeln(-1);
  end;
close(input);close(output);
end.
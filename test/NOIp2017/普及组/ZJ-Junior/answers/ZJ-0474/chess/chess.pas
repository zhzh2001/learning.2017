const
red=1;
yellow=2;
inf=maxlongint;
var
n,m,i,j,ans,a,b,c,l,r,tx,ty:longint;
map:array[1..100,1..100]of longint;
f:array[1..100,1..100]of longint;
dy:array[1..4]of longint=(1,0,-1,0);
dx:array[1..4]of longint=(0,1,0,-1);
function can(x,y:longint):boolean;
  begin
    if(x<1)or(x>n)then exit(false);
    if(y<1)or(y>n)then exit(false);
    exit(true);
  end;
procedure dfs(x,y,coin:longint;used:boolean);
  var i,tx,ty:longint;
  begin
    if coin>=f[x,y] then exit;
    f[x,y]:=coin;
    if(x=n)and(y=n)then
      begin
        if coin<ans then ans:=coin;
        exit;
      end;
    for i:=1 to 4 do
      begin
        tx:=x+dx[i];ty:=y+dy[i];
        if can(tx,ty) then
          begin
            if map[tx,ty]=0 then
              begin
                if not used then
                  begin
                    map[tx,ty]:=red;
                    if map[x,y]=red
                      then dfs(tx,ty,coin+2,true)
                      else dfs(tx,ty,coin+3,true);
                    map[tx,ty]:=yellow;
                    if map[x,y]=yellow
                      then dfs(tx,ty,coin+2,true)
                      else dfs(tx,ty,coin+3,true);
                    map[tx,ty]:=0;
                  end;
              end          else
              begin
                if map[tx,ty]=map[x,y]
                  then dfs(tx,ty,coin,false)
                  else dfs(tx,ty,coin+1,false);
              end;
          end;
      end;
  end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
for i:=1 to m do
  begin
    readln(a,b,c);
    map[a,b]:=c+1;
  end;
for i:=1 to n do
  for j:=1 to n do
    f[i,j]:=inf;
ans:=inf;
dfs(1,1,0,false);
if ans=maxlongint then writeln(-1)else writeln(ans);
close(input);close(output);
end.
#include<bits/stdc++.h>
using namespace std;
int n,d,k,ans,l,r,mid,sum;
struct aa{
	int num,s;
}a[500009];
bool ma=0;
void dfs(int x,int u)
{
	if(u>=k) ma=1;
	if(ma==1 || x==n) return;
	int j=x+1;
	while(j<=n && a[j].num<a[x].num+d-mid) j++;
	if(j>n) return;
	for(int o=j;o<=n && a[o].num<=a[x].num+d+mid;o++)
	{
		dfs(o,u+a[o].s);
	}
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int j=1;j<=n;j++) 
	{
		scanf("%d%d",&a[j].num,&a[j].s);
		if(a[j].s>0) sum+=a[j].s;
	}
	if(sum<k) 
	{
		printf("-1");
		return 0;
	}
	int l=0,r=max(a[n].num-d,d-a[1].num);
	while(l<=r)
	{
		mid=(l+r)/2;
		ma=0;
		dfs(0,0);
		if(ma==1)
		{
			ans=mid;
			r=mid-1;
		}
		else l=mid+1;
	}
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,c;
int ans[109][109];
int a[109][109];
bool k[109][109];
int dir[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
void dfs(int x,int y,int u,int co)
{
	if(k[x][y]!=0 && ans[x][y]<=u) return;
	ans[x][y]=u;
	k[x][y]=1;
	int p,q;
	for(int i=0;i<=3;i++)
	{
		p=x+dir[i][0];
		q=y+dir[i][1];
		if(p==1 && q==1) continue;
		if(p<1 || q<1 || p>m || q>m) continue;
		if(a[x][y]==0 && a[p][q]==0) continue;
		if(a[p][q]==0) dfs(p,q,u+2,a[x][y]);
		else if(co==a[p][q]) dfs(p,q,u,a[p][q]);
		else dfs(p,q,u+1,a[p][q]);
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int j=1;j<=n;j++)
	{
		scanf("%d%d%d",&x,&y,&c);
		if(c==0) c=2;
		a[x][y]=c;
	}
	dfs(1,1,0,a[1][1]);
	if(ans[m][m]!=0) printf("%d",ans[m][m]);
	else printf("-1");
	fclose(stdin);
	fclose(stdout);
	return 0;
}

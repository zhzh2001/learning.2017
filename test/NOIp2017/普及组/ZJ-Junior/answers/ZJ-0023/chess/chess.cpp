#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,x,y,c,dis[101][101],q[100001][2],a[101][101],h,t;
bool bo[101][101];
void push(int x,int y)
{
	if(bo[x][y])return;
	q[++t][0]=x; q[t][1]=y;
	bo[x][y]=1;
}
int abs(int x){return x>0?x:-x;}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d%d",&x,&y,&c);
		if(c==1)a[x][y]=1;
		else a[x][y]=2;
	}
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++)
			dis[i][j]=987654321;
	dis[1][1]=0;
	push(1,1);
	do{
		h++;
		x=q[h][0]; y=q[h][1];
		if(x+1<=m)
			if(a[x+1][y]>0)
				if(dis[x][y]+abs(a[x][y]-a[x+1][y])<dis[x+1][y])
				{
					dis[x+1][y]=dis[x][y]+abs(a[x][y]-a[x+1][y]);
					if(bo[x+1][y]==0)push(x+1,y);
				}
		if(x-1>=1)
			if(a[x-1][y]>0)
				if(dis[x][y]+abs(a[x][y]-a[x-1][y])<dis[x-1][y])
				{
					dis[x-1][y]=dis[x][y]+abs(a[x][y]-a[x-1][y]);
					if(bo[x-1][y]==0)push(x-1,y);
				}
		if(y+1<=m)
			if(a[x][y+1]>0)
				if(dis[x][y]+abs(a[x][y]-a[x][y+1])<dis[x][y+1])
				{
					dis[x][y+1]=dis[x][y]+abs(a[x][y]-a[x][y+1]);
					if(bo[x][y+1]==0)push(x,y+1);
				}
		if(y-1>=1)
			if(a[x][y-1]>0)
				if(dis[x][y]+abs(a[x][y]-a[x][y-1])<dis[x][y-1])
				{
					dis[x][y-1]=dis[x][y]+abs(a[x][y]-a[x][y-1]);
					if(bo[x][y-1]==0)push(x,y-1);
				}
		if(x+2<=m)
			if(a[x+2][y]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x+2][y])<dis[x+2][y])
				{
					dis[x+2][y]=dis[x][y]+2+abs(a[x][y]-a[x+2][y]);
					if(bo[x+2][y]==0)push(x+2,y);
				}
		if(x-2>=1)
			if(a[x-2][y]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x-2][y])<dis[x-2][y])
				{
					dis[x-2][y]=dis[x][y]+2+abs(a[x][y]-a[x-2][y]);
					if(bo[x-2][y]==0)push(x-2,y);
				}
		if(y+2<=m)
			if(a[x][y+2]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x][y+2])<dis[x][y+2])
				{
					dis[x][y+2]=dis[x][y]+2+abs(a[x][y]-a[x][y+2]);
					if(bo[x][y+2]==0)push(x,y+2);
				}
		if(y-2>=1)
			if(a[x][y-2]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x][y-2])<dis[x][y-2])
				{
					dis[x][y-2]=dis[x][y]+2+abs(a[x][y]-a[x][y-2]);
					if(bo[x][y-2]==0)push(x,y-2);
				}
		if(x+1<=m&&y+1<=m)
			if(a[x+1][y+1]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x+1][y+1])<dis[x+1][y+1])
				{
					dis[x+1][y+1]=dis[x][y]+2+abs(a[x][y]-a[x+1][y+1]);
					if(bo[x+1][y+1]==0)push(x+1,y+1);
				}
		if(x+1<=m&&y-1>=1)
			if(a[x+1][y-1]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x+1][y-1])<dis[x+1][y-1])
				{
					dis[x+1][y-1]=dis[x][y]+2+abs(a[x][y]-a[x+1][y-1]);
					if(bo[x+1][y-1]==0)push(x+1,y-1);
				}
		if(x-1>=1&&y+1<=m)
			if(a[x-1][y+1]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x-1][y+1])<dis[x-1][y+1])
				{
					dis[x-1][y+1]=dis[x][y]+2+abs(a[x][y]-a[x-1][y+1]);
					if(bo[x-1][y+1]==0)push(x-1,y+1);
				}
		if(x-1>=1&&y-1>=1)
			if(a[x-1][y-1]>0)
				if(dis[x][y]+2+abs(a[x][y]-a[x-1][y-1])<dis[x-1][y-1])
				{
					dis[x-1][y-1]=dis[x][y]+2+abs(a[x][y]-a[x-1][y-1]);
					if(bo[x-1][y-1]==0)push(x-1,y-1);
				}
		bo[x][y]=0;
	}while(h<t);
	if(dis[m][m]==987654321)printf("-1");
	else printf("%d",dis[m][m]);
	return 0;
}

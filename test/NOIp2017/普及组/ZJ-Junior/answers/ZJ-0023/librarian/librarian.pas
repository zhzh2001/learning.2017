var i,n,q,x,y,j:longint;
    a:array[1..1000] of longint;
    hz:array[1..1000,0..7] of longint;

procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if i<=j then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      dec(j);
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to n do
  begin
    x:=10;
    j:=0;
    while a[i]*10>=x do
    begin
      inc(j);
      hz[i,j]:=a[i] mod x;
      x:=x*10;
    end;
  end;
  for i:=1 to q do
  begin
    read(x,y);
    for j:=1 to n do
      if hz[j][x]=y then
      begin
        writeln(a[j]);
        break;
      end;
    if hz[j][x]<>y then writeln(-1);
  end;
  close(input); close(output);
end.
#include<cctype>
#include<cstdio>
using namespace std;
int n,d,k,x[500001],s[500001],f[500001],left,right,mid,tmp;
inline void read(int &x)
{
	int w=1; char ch=0; x=0;
	ch=getchar();
	while(ch^'-'&&!isdigit(ch))ch=getchar();
	if(ch=='-')w=-1,ch=getchar();
	while(isdigit(ch))x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	x=x*w;
}
int max(int a,int b){return a>b?a:b;}
bool judge(int g)
{
	int ans=0;
	for(int i=1;i<=n;i++)f[i]=0;
	for(int i=1;i<=n;i++)
	{
		for(int j=i-1;j>=0;j--)
			if(x[i]-x[j]>=max(1,d-g)&&x[i]-x[j]<=d+g)f[i]=max(f[i],f[j]+s[i]);
			else break;
		ans=max(ans,f[i]);
	}
	if(ans>=k)return 1;
	if(ans<k)return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	read(n); read(d); read(k);
	for(int i=1;i<=n;i++){read(x[i]); read(s[i]); if(s[i]>0)tmp+=s[i];}
	if(k>tmp){printf("-1"); return 0;}
	left=0; right=1000000000;
	while(left<right-1)
	{
		mid=(left+right+1)/2;
		if(judge(mid))right=mid;
		else left=mid+1;
	}
	printf("%d",right);
	return 0;
}

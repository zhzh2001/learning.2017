var f:array[0..200,0..200] of longint;
    a,x,y:array[1..3000000] of longint;
    xx:array[1..4] of longint=(1,0,-1,0);
    yy:array[1..4] of longint=(0,1,0,-1);
    ft,fs:array[0..200,0..200] of boolean;
    n,m,x1,y1,t,i,j,k,l,r,tx,ty:longint;
    ff:boolean;
procedure put(t,x1,y1:longint);
var i:longint;
begin
  inc(l);
  i:=l;
  while (i>1) and (a[i div 2]>t) do
    begin
      a[i]:=a[i div 2];
      x[i]:=x[i div 2];
      y[i]:=y[i div 2];
      i:=i div 2;
    end;
  a[i]:=t;
  x[i]:=x1;
  y[i]:=y1;
end;
function pd(x,y,x1,y1:longint):longint;
begin
  if f[x,y]=f[x1,y1] then exit(0);
  exit(1);
end;
procedure get(var t,x1,y1:longint);
var i,tt,tx,ty:longint;
begin
  t:=a[1];x1:=x[1];y1:=y[1];
  tt:=a[l];tx:=x[l];ty:=y[l];
  dec(l);
  if l=0 then exit;
  i:=1;
  while (i*2<=l) and (a[i*2]<tt) or (i*2+1<=l) and (a[i*2+1]<tt) do
    begin
      if (a[i*2]<a[i*2+1]) or (i*2+1>l) then
        begin
          a[i]:=a[i*2];x[i]:=x[i*2];y[i]:=y[i*2];
          i:=i*2;
        end
      else
        begin
          a[i]:=a[i*2+1];x[i]:=x[i*2+1];y[i]:=y[i*2+1];
          i:=i*2+1;
        end;
    end;
  a[i]:=tt;x[i]:=tx;y[i]:=ty;
end;
procedure work(x,y,t,x1,y1:longint);
var i:longint;
begin
  if (x<1) or (x>m) or (y<1) or (y>m) or ft[x,y] then exit;
  ft[x,y]:=true;
  if (x=m) and (y=m) then
      if f[x,y]=0 then
        begin
          t:=t+2;
          ff:=false;
          writeln(t);
          exit;
        end;
  if f[x,y]=0 then
    begin
      for i:=1 to 4 do
        if (f[x+xx[i],y+yy[i]]>0) and (ft[x+xx[i],y+yy[i]]=false) then
          begin
            work(x+xx[i],y+yy[i],t+2,x1,y1);
            ft[x+xx[i],y+yy[i]]:=false;
          end;
      exit;
    end;
  t:=t+pd(x,y,x1,y1);
  put(t,x,y);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to n do
    begin
      readln(x1,y1,t);
      f[x1,y1]:=t+1;
    end;
  x[1]:=1;y[1]:=1;a[1]:=0;l:=1;
  ft[1,1]:=true;
  ff:=true;
  while ff do
    begin
      get(t,tx,ty);
      while (fs[tx,ty]) do
        begin
          tx:=0;ty:=0;
          if l=0 then break;
          get(t,tx,ty);
        end;
      if (tx=0) and (ty=0) and (l=0) then break;
      if (tx=m) and (ty=m) then
        begin
          writeln(t);
          close(input);close(output);
          halt;
        end;
      fs[tx,ty]:=true; ft[tx,ty]:=true;
      for i:=1 to 4 do work(tx+xx[i],ty+yy[i],t,tx,ty);
      if l=0 then break;
    end;
  if ff then
    begin
      writeln(-1);
      close(input);close(output);
    end;
end.

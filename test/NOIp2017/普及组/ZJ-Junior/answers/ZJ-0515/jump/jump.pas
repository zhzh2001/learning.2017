var a,b,s:array[0..500000] of longint;
    i,j,k,n,d,min,l,r,t1,t:longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x);
  exit(y);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do readln(a[i],b[i]);
  t:=0;
  for i:=1 to n do
    if b[i]>0 then t:=t+b[i];
  if t<k then
    begin
      writeln(-1);
      close(input);close(output);
      halt;
    end;
  for min:=1 to maxlongint do
    begin
      if min>100 then break;
      l:=d-min;
      r:=d+min;
      if l<1 then l:=1;
      fillchar(s,sizeof(s),0);
      i:=1;
      while a[i]<l do inc(i);t1:=i;
      for i:=t1 to n do s[i]:=-10000000;
      s[t1]:=max(0,b[t1]);
      for i:=t1+1 to n do
        for j:=t1 to i-1 do
          if (a[i]-a[j]>=l) and (a[i]-a[j]<=r) then
            if s[j]+b[i]>s[i] then s[i]:=s[j]+b[i];
      t:=0;
      for i:=t1 to n do
        if s[i]>t then t:=s[i];
      if t>=d then break;
    end;
  writeln(min);
  close(input);close(output);
end.

var
  a,b,c:longint;
begin
  assign(input,'score.in');
  reset(input);
  assign(output,'score.out');
  rewrite(output);
  read(a,b,c);
  writeln((2*a+3*b+5*c) div 10);
  close(input);
  close(output);
end.
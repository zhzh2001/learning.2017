var
  m,n,i,x,y,c:longint;
  map:array[-2..102,-2..102]of longint;
function go(x,y:longint):longint;
begin
  if (x=m) and (y=m) then exit(0);
  if map[x+1,y]=map[x,y] then exit(go(x+1,y));
  if map[x,y+1]=map[x,y] then exit(go(x,y+1));
  if map[x+1,y]>0 then exit(go(x+1,y)+1);
  if map[x,y+1]>0 then exit(go(x,y+1)+1);
  if map[x+1,y+1]=map[x,y] then exit(go(x+1,y+1)+2);
  if map[x+2,y]=map[x,y] then exit(go(x+2,y)+2);
  if map[x,y+2]=map[x,y] then exit(go(x,y+2)+2);
  if map[x+1,y+1]>0 then exit(go(x+1,y+1)+3);
  if map[x+2,y]>0 then exit(go(x+2,y)+3);
  if map[x,y+2]>0 then exit(go(x,y+2)+3);
  writeln(-1);
  close(input);
  close(output);
  halt;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do begin
                   readln(x,y,c);
                   map[x,y]:=c+1;
                   end;
  writeln(go(1,1));
  close(input);
  close(output);
end.
var
  m,n,i,j,x1,y1,z1:longint;
  c,d:array[-5..105,-5..105]of longint;
  a:array[-5..105,-5..105]of boolean;
  e:array[-5..105,-5..105,0..1]of integer;
  f:array[-5..10005]of longint;
const
  b:array[1..4,1..2]of integer=((0,1),(1,0),(0,-1),(-1,0));
procedure gao(x,y,z:integer);
var
  i:longint;
begin
  if z=0 then
  begin
    for i:=1 to 4 do
    begin
      if a[x+b[i,1],y+b[i,2]]=true then
      begin
        if c[x+b[i,1],y+b[i,2]]<>-1 then
        begin
          if c[x,y]=c[x+b[i,1],y+b[i,2]] then
          begin
            if e[x,y,z]<e[x+b[i,1],y+b[i,2],0] then
            begin
              e[x+b[i,1],y+b[i,2],0]:=e[x,y,z];
              gao(x+b[i,1],y+b[i,2],0);
            end;
          end;
          if c[x,y]<>c[x+b[i,1],y+b[i,2]] then
          begin
            if e[x,y,z]+1<e[x+b[i,1],y+b[i,2],0] then
            begin
              e[x+b[i,1],y+b[i,2],0]:=e[x,y,z]+1;
              gao(x+b[i,1],y+b[i,2],0);
            end;
          end;
        end;
      end;
      if c[x+b[i,1],y+b[i,2]]=-1 then
      begin
        if e[x,y,z]+2<e[x+b[i,1],y+b[i,2],1] then
        begin
          e[x+b[i,1],y+b[i,2],1]:=e[x,y,z]+2;
          c[x+b[i,1],y+b[i,2]]:=c[x,y];
          gao(x+b[i,1],y+b[i,2],1);
          c[x+b[i,1],y+b[i,2]]:=-1;
        end;
      end;
    end;
  end;
  if z=1 then
  begin
    for i:=1 to 4 do
    begin
      if a[x+b[i,1],y+b[i,2]]=true then
      begin
        if c[x+b[i,1],y+b[i,2]]<>-1 then
        begin
          if c[x,y]=c[x+b[i,1],y+b[i,2]] then
          begin
            if e[x,y,z]<e[x+b[i,1],y+b[i,2],0] then
            begin
              e[x+b[i,1],y+b[i,2],0]:=e[x,y,z];
              gao(x+b[i,1],y+b[i,2],0);
            end;
          end;
          if c[x,y]<>c[x+b[i,1],y+b[i,2]] then
          begin
            if e[x,y,z]+1<e[x+b[i,1],y+b[i,2],0] then
            begin
              e[x+b[i,1],y+b[i,2],0]:=e[x,y,z]+1;
              gao(x+b[i,1],y+b[i,2],0);
            end;
          end;
        end;
      end;
    end;
  end;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      c[i,j]:=-1;
  for i:=1 to n do
  begin
    readln(x1,y1,z1);
    c[x1,y1]:=z1;
  end;
  for i:=0 to m+1 do
    for j:=0 to n+1 do
      a[i,j]:=false;
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=true;
  a[1,1]:=false;
  for i:=1 to m do
    for j:=1 to m do
    begin
      e[i,j,0]:=9999;
      e[i,j,1]:=9999;
    end;
  e[1,1,0]:=0;
  gao(1,1,0);
  if (e[m,m,0]=9999)and(e[m,m,1]=9999) then
  begin
    write(-1);
    halt;
  end
  else
  begin
    if e[m,m,0]>e[m,m,1] then write(e[m,m,1])
    else write(e[m,m,0]);
  end;
  close(input);
  close(output);
end.
var
  n,d,k,i,m:longint;
  x,s,f:array[0..500005]of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do readln(x[i],s[i]);
  for i:=1 to n do
    if s[i]>0 then m:=m+s[i];
  if m<k then write(-1)
  else write(3);
  close(input);
  close(output);
end.
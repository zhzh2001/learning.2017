var
  c:char;
  bo:boolean;
  n,q,i,j,bit,t,k:longint;
  a,b,s:array[0..1001]of longint;

procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l; j:=r; x:=a[(l+r)div 2];
  repeat
    while a[i]<x do inc(i);
    while a[j]>x do dec(j);
    if i<=j then
      begin
        y:=a[i]; a[i]:=a[j]; a[j]:=y;
        inc(i); dec(j);
      end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  read(n,q);
  for i:=1 to n do
    read(a[i]);
  sort(1,n);
  for i:=1 to q do
    begin
      read(b[i],s[i]);
      t:=1;
      for j:=1 to b[i] do
        t:=t*10;
      bo:=false;
      for j:=1 to n do
        if(a[j] mod t=s[i])and not bo then begin bo:=true; writeln(a[j]); end;
      if bo=false then writeln(-1);
    end;
  close(output);
end.

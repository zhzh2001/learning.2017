const
  dx:array[1..4]of longint=(0,1,0,-1);
  dy:array[1..4]of longint=(1,0,-1,0);
var
  bool:boolean;
  m,n,i,j,x,y,cc,h,t,cx,cy,time:longint;
  qx,qy:array[0..100001]of longint;
  c,a:array[0..101,0..101]of longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  read(m,n);
  for i:=1 to m do
    for j:=1 to m do
      c[i,j]:=-1;
  for i:=1 to n do
    begin
      read(x,y,cc);
      c[x,y]:=cc;
    end;
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=maxlongint div 2;
  a[1,1]:=0;
  h:=0; t:=1;
  qx[1]:=1; qy[1]:=1;
  while h<=t do
    begin
      inc(h);
      inc(time);
      for i:=1 to 4 do
        begin
          cx:=qx[h]+dx[i]; cy:=qy[h]+dy[i];
          if(cx>0)and(cy>0)and(cx<=m)and(cy<=m)then bool:=true;
          if bool then
            begin
              if(c[cx,cy]=c[qx[h],qy[h]])and(a[qx[h],qy[h]]<a[cx,cy])then
                begin
                  inc(t);
                  qx[t]:=cx; qy[t]:=cy;
                  a[cx,cy]:=a[qx[h],qy[h]];
                end else
              if(c[cx,cy]=1)and(c[qx[h],qy[h]]=0)or(c[cx,cy]=0)and(c[qx[h],qy[h]]=1)and(a[qx[h],qy[h]]+1<a[cx,cy])then
                begin
                  inc(t);
                  qx[t]:=cx; qy[t]:=cy;
                  a[cx,cy]:=a[qx[h],qy[h]]+1;
                end else
              if(c[cx,cy]-3=c[qx[h],qy[h]])and(a[qx[h],qy[h]]<a[cx,cy])then
                begin
                  inc(t);
                  qx[t]:=cx; qy[t]:=cy;
                  a[cx,cy]:=a[qx[h],qy[h]];
                  c[qx[h],qy[h]]:=0;
                end else
              if(c[cx,cy]=1)and(c[qx[h],qy[h]]=-3)or(c[cx,cy]=0)and(c[qx[h],qy[h]]=-2)and(a[qx[h],qy[h]]+1<a[cx,cy])then
                begin
                  inc(t);
                  qx[t]:=cx; qy[t]:=cy;
                  a[cx,cy]:=a[qx[h],qy[h]]+1;
                  c[qx[h],qy[h]]:=0;
                end else
              if(c[cx,cy]=-1)and(a[qx[h],qy[h]]+2<a[cx,cy])and(c[qx[h],qy[h]]>=0)then
                 begin
                   inc(t);
                   qx[t]:=cx; qy[t]:=cy;
                   c[cx,cy]:=c[qx[h],qy[h]]-3;
                   a[cx,cy]:=a[qx[h],qy[h]]+2;
                 end;
            end;
        end;
      if t>200 then break;
    end;
  writeln(a[m,m]);
  close(output);
end.

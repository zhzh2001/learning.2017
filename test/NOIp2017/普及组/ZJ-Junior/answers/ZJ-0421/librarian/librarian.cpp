#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;

int d[1001];

inline int read()
{
	int data = 0, w = 1; char ch = getchar();
	while (ch != '-' && !isdigit(ch)) ch = getchar();
	if (ch == '-') w = -1;
	else data = data * 10 + ch - 48;
	ch = getchar();
	while (isdigit(ch)) data = data * 10 + ch - 48, ch = getchar();
	return data * w;
}

int main()
{
	freopen("librarian.in","r+",stdin);
	freopen("librarian.out","w+",stdout);
	
	int n,m;
	scanf("%d%d",&n,&m);
	for (register int i = 1; i <= n; i++) scanf("%d",&d[i]);
	for (register int i = 1; i <= m; i++)
	{
		int l,x;
		scanf("%d%d",&l,&x);
		int temp = 1, mini = 2147483647;
		for (register int j = 1; j <= l; j++) temp *= 10;
		for (register int j = 1; j <= n; j++) 
		{
			int k = d[j] % temp;
			if (k == x) mini = min(mini,d[j]);
		}
		if (mini != 2147483647) printf("%d\n",mini);
		else puts("-1");
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

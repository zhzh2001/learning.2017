#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int dx[] = {0,1,0,-1,0};
const int dy[] = {0,0,1,0,-1};
int a[101][101], ans = 2147483647, s[101][101];
bool inq[101][101];
struct _{
	int x,y,c,c_temp,val;
}Q[20001];

int main()
{
	freopen("chess.in","r+",stdin);
	freopen("chess.out","w+",stdout);
	
	int m,n;
	scanf("%d%d",&n,&m);
	memset(a,-1,sizeof a);
	for (register int i = 1; i <= m; i++)
	{
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		a[x][y] = c;
	}
	memset(s,127,sizeof s);
	s[1][1] = 0;
	int head = 0, tail = 1;
	Q[1].x = Q[1].y = 1;
	Q[1].c = Q[1].c_temp = a[1][1];
	Q[1].val = 0;
	memset(inq,0,sizeof inq);
	inq[1][1] = 1;
	while (head < tail)
	{
		int x1 = Q[++head].x, y1 = Q[head].y, c1 = Q[head].c, c1_temp = Q[head].c_temp,v = Q[head].val; 
		//printf("x = %d, y = %d:\n",x1,y1);
		for (register int i = 1; i <= 4; i++)
		{
			int xx = x1 + dx[i], yy = y1 + dy[i];
			//printf("xx = %d, yy = %d\n",xx,yy);
			if (xx > 0 && xx <= n && yy > 0 && yy <= n)
				if (c1 != -1)
				{
					if (a[xx][yy] == -1)
					{
						if (!inq[xx][yy])
						{
							Q[++tail].x = xx;
							Q[tail].y = yy;
							Q[tail].c = -1;
							Q[tail].c_temp = c1;
							Q[tail].val = v + 2;
							Q[++tail].x = xx;
							Q[tail].y = yy;
							Q[tail].c = -1;
							Q[tail].c_temp = 1 - c1;
							Q[tail].val = v + 3;
							s[xx][yy] = tail;
							inq[xx][yy] = 1;
						}
						else Q[s[xx][yy]].val = min(Q[s[xx][yy]].val, v + 3), Q[s[xx][yy] - 1].val = min(Q[s[xx][yy] - 1].val, v + 2);
					}
					else if (a[xx][yy] == a[x1][y1])	
					{
						//s[xx][yy] = min(s[xx][yy], s[x1][y1]);
						if (!inq[xx][yy])
						{
							Q[++tail].x = xx;
							Q[tail].y = yy;
							Q[tail].c = Q[tail].c_temp = a[xx][yy];
							Q[tail].val = v;
							s[xx][yy] = tail;
							inq[xx][yy] = 1;
						}
						else Q[s[xx][yy]].val = min(Q[s[xx][yy]].val,v);
					}
					else 
					{
						//s[xx][yy] = min(s[xx][yy], s[x1][y1] + 1);
						if (!inq[xx][yy])
						{
							Q[++tail].x = xx;
							Q[tail].y = yy;
							Q[tail].c = Q[tail].c_temp = a[xx][yy];
							Q[tail].val = v + 1;
							s[xx][yy] = tail;
							inq[xx][yy] = 1;
						}
						else Q[s[xx][yy]].val = min(Q[s[xx][yy]].val, v + 1);
					}
				}
				else
				{
					if (a[xx][yy] == -1) continue;
					else if (a[xx][yy] == c1_temp)
					{
						//s[xx][yy] = min(s[xx][yy],s[x1][y1]);
						if (!inq[xx][yy])
						{
							Q[++tail].x = xx;
							Q[tail].y = yy;
							Q[tail].c = Q[tail].c_temp = a[xx][yy];
							Q[tail].val = v;
							s[xx][yy] = tail;
							inq[xx][yy] = 1;
						}
						else Q[s[xx][yy]].val = min(Q[s[xx][yy]].val, v);
					}
					else
					{
						//s[xx][yy] = min(s[xx][yy],s[x1][y1] + 1);
						if (!inq[xx][yy])
						{
							Q[++tail].x = xx;
							Q[tail].y = yy;
							Q[tail].c = Q[tail].c_temp = a[xx][yy];
							Q[tail].val = v + 1;
							s[xx][yy] = tail;
							inq[xx][yy] = 1;
						}
						else Q[s[xx][yy]].val = min(Q[s[xx][yy]].val, v + 1);
					}	
				}
		}
		//puts("");
	}
	/*for (register int i = 1; i <= tail; i++)
		printf("%d %d %d %d\n",Q[i].x,Q[i].y,Q[i].c,Q[i].c_temp);
	puts("");*/
	/*for (register int i = 1; i <= n; i++)
	{
		for (register int j = 1; j <= n; j++)
			printf("%d ",a[i][j] + 1);
		puts("");
	}
	puts("");
	puts("");
	puts("");*/
	/*for (register int i = 1; i <= n; i++)
	{
		for (register int j = 1; j <= n; j++)
		if (s[i][j] != 2139062143) printf("%d ",Q[s[i][j]].val);
		else printf("%d ",-1);
		puts("");
	}*/
	/*for (register int i = 1; i <= n; i++)
	{
		for (register int j = 1; j <= n; j++)
			printf("%d ",inq[i][j]);
		puts("");
	}*/
	if (!inq[n][n]) puts("-1");
	else printf("%d\n",Q[s[n][n]].val);
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

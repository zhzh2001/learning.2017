#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;

int x[500001],s[500001],t[500001];
const int maxlongint = 2147483647;

int main()
{
	freopen("jump.in","r+",stdin);
	freopen("jump.out","w+",stdout);
	
	int n, d, k, maxt, mint;
	long long temp = 0;
	scanf("%d%d%d",&n,&d,&k);
	scanf("%d%d",&x[1],&s[1]);
	t[1] = s[1];maxt = mint = t[1];
	if (s[1] > 0) temp += s[1];
	for (register int i = 2; i <= n; i++) 
	{
		scanf("%d%d",&x[i],&s[i]);
		t[i] = x[i] - x[i - 1];
		maxt = max(maxt, t[i]);
		mint = min(mint, t[i]);
		if (s[i] > 0) temp += s[i];
	}
	if (temp < k) 
	{
		puts("-1");
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	int l = 0, r;
	if (maxt < d) r = d - mint;
	else r = maxt - d;
	while (l < r)
	{
		int mid = l + r >> 1;
		int kmin,kmax = d + mid;
		if (mid < d) kmin = d - mid;
		else kmin = 1;
		long long ans = 0, ans_now = 0,dis_now = 0;
		for (register int i = 1; i <= n; i++)
			if (t[i] <= kmax && t[i] >= kmin) 
			{
				if (s[i] > 0) ans_now += s[i], dis_now = 0;
				else 
				{
					ans = max(ans, ans_now);
					dis_now += t[i];
					if (dis_now + t[i + 1] > kmax) ans_now += s[i], dis_now = 0;
				}
			}
			else if (t[i] > kmax) break;
			else dis_now += t[i];
		ans = max(ans, ans_now);
		if (ans >= k) r = mid;
		else l = mid + 1;
	} 
	printf("%d\n",l);
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

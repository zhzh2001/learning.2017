var
  a,b,len:array[0..1001]of longint;
  z:array[0..1001]of int64;
  n,m,i,j,ans:longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  for i:=1 to m do 
    begin
      readln(len[i],b[i]);
      z[i]:=1;
      while z[i]<=b[i] do z[i]:=z[i]*10;
    end;
  for i:=1 to m do 
    begin
      ans:=maxlongint;
      for j:=1 to n do 
        if a[j] mod z[i]=b[i] then 
          if ans>a[j] then ans:=a[j];
      if ans=maxlongint then writeln(-1)
        else writeln(ans);
    end;
  close(input);
  close(output);
end.
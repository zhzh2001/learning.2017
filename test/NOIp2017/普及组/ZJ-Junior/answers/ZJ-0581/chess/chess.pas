uses math;
var
  f:array[0..101,0..101,1..2]of longint;
  a:array[0..101,0..101]of longint;
  n,m,x,y,z,i,j,k,t,ans:longint;
function dis(x,y:longint):longint;
begin
  if x<>y then exit(1) else exit(0);
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to m do 
    begin
      readln(x,y,z);
      inc(z);
      a[x,y]:=z;
    end;
  for i:=0 to n do 
    for j:=0 to n do 
      for k:=1 to 2 do 
        f[i,j,k]:=maxlongint div 3;
  f[1,1,a[1,1]]:=0;
  for i:=1 to n do 
    for j:=1 to n do
      begin
        if (i=1)and(j=1) then continue;
        if a[i,j]=0 then 
          begin
            for k:=1 to 2 do 
              begin 
                if a[i-1,j]>0 then f[i,j,k]:=min(f[i,j,k],f[i-1,j,a[i-1,j]]+2+dis(k,a[i-1,j]));
                if a[i,j-1]>0 then f[i,j,k]:=min(f[i,j,k],f[i,j-1,a[i,j-1]]+2+dis(k,a[i,j-1]));
              end;
          end
          else 
            begin
              for k:=1 to 2 do 
                f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i-1,j,k]+dis(k,a[i,j]));
              for k:=1 to 2 do 
                f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i,j-1,k]+dis(k,a[i,j]));
            end;
      end;
  ans:=min(f[n,n,1],f[n,n,2]);
  if ans=maxlongint div 3 then write(-1) else write(ans);
  close(input);
  close(output);
end.
uses math;
var
  a,x,f:array[0..500001]of longint;
  n,d,m,i,sum,l,r,mid,ans:longint;
function check(ans:longint):boolean;
var
  l,r,i,j,zz:longint;
begin
  if ans>=d then l:=1 else l:=d-ans;
  r:=d+ans;
  for i:=1 to n do 
    begin 
      f[i]:=-233333333;
      for j:=i-1 downto 0 do
        begin
          if x[i]-x[j]>r then break;
          if (x[i]-x[j]>=l)and(x[i]-x[j]<=r) then 
            if f[j]<>-233333333 then f[i]:=max(f[i],f[j]);
        end;
      if f[i]<>-233333333 then inc(f[i],a[i]);
      if f[i]>=m then exit(true);
    end;
  exit(false);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,m);
  for i:=1 to n do readln(x[i],a[i]);
  ans:=-1;
  l:=1;
  r:=maxlongint div 2;
  while l<=r do 
    begin
      mid:=(l+r) >> 1;
      if check(mid) then begin ans:=mid;r:=mid-1 end
        else l:=mid+1;
    end;
  write(ans);
  close(input);
  close(output);
end.
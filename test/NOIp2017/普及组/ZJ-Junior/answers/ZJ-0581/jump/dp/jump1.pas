uses math;
type
  tree=record
  l,r:longint;
  sum:int64;
end;
var
  a,x:array[0..500001]of int64;
  f:array[0..500001]of int64;
  t:array[0..2000001]of tree;
  n,d,m,i,sum,l,r,mid,ans:longint;
procedure build(k,l,r:longint);
var
  mid:longint;
begin
  t[k].l:=l;t[k].r:=r;
  if l=r then exit;
  mid:=(l+r) >> 1;
  build(k*2,l,mid);
  build(k*2+1,mid+1,r);
end;
procedure add(k,l,r,x:longint);
begin
  if (t[k].l>r)or(t[k].r<l) then exit;
  if (t[k].l=t[k].r) then 
    if l=t[k].l then 
      begin
        t[k].sum:=x;
        exit;
      end;
  add(k*2,l,r,x);
  add(k*2+1,l,r,x);
  t[k].sum:=max(t[k*2].sum,t[k*2+1].sum);
end;
function query(k,l,r:longint):int64;
begin
  if (t[k].l>r)or(t[k].r<l) then exit(-maxlongint);
  if (t[k].l>=l)and(t[k].r<=r) then exit(t[k].sum);
  exit(max(query(k*2,l,r),query(k*2+1,l,r)));
end;
function find(xx,y:longint):longint;
var
  l,r,mid,ans:longint;
begin
  l:=0;
  r:=y;
  ans:=-1;
  while l<=r do 
    begin
      mid:=(l+r) >> 1;
      if x[mid]>=xx then begin ans:=mid;r:=mid-1 end
        else l:=mid+1;
    end;
  exit(ans);
end;
function find1(xx,y:longint):longint;
var
  l,r,mid,ans:longint;
begin
  l:=0;
  r:=y;
  ans:=-1;
  while l<=r do 
    begin
      mid:=(l+r) >> 1;
      if x[mid]<=xx then begin ans:=mid;l:=mid+1 end
        else r:=mid-1;
    end;
  exit(ans);
end;
function check(ans:longint):boolean;
var
  l,r,i,j,z,zz:longint;
begin
  if ans>=d then l:=1 else l:=d-ans;
  r:=d+ans;
  for i:=1 to 2000000 do 
    begin
      if i<=n then f[i]:=-maxlongint;
      t[i].sum:=-maxlongint;
    end;
  add(1,0,0,0);
  for i:=1 to n do 
    begin 
      z:=find(x[i]-r,i);
      zz:=find1(x[i]-l,i);writeln(z,' ',zz,' ',x[i]-r,' ',x[i]-l);
      if (z=-1)or(zz=-1)or(z>zz) then continue;
      f[i]:=query(1,z,zz)+a[i];
      add(1,i,i,f[i]);
      if f[i]>=m then exit(true);
    end;
  exit(false);
end;
begin
  assign(input,'jump1.in');
  assign(output,'jump1.out');
  reset(input);
  rewrite(output);
  readln(n,d,m);
  for i:=1 to n do readln(x[i],a[i]);
  build(1,0,n);
  {ans:=-1;
  l:=1;
  r:=maxlongint div 2;
  while l<=r do 
    begin 
      mid:=(l+r) >> 1;
      if check(mid) then begin ans:=mid;r:=mid-1 end
        else l:=mid+1;
    end;}
  write(ans,' ',check(1));
  close(input);
  close(output);
end.
var a:array[0..1001] of longint;
    n,q,i,j,m,k:longint;
    s1,s2:string; f:boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to q do
  begin
    read(m,k); f:=true;
    for j:=1 to n do
    if k<=a[j] then
    begin
      str(k,s1);
      str(a[j],s2);
      s2:=copy(s2,length(s2)-m+1,m);
      if s1=s2 then
      begin
        writeln(a[j]);
        f:=false;
        break;
      end;
    end;
    if f then writeln(-1);
  end;
  close(input); close(output);
end.

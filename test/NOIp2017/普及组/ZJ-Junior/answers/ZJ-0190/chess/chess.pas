var a,b,f:array[0..105,0..105] of longint;
    i,j,m,n,x,y,z,c:longint; s:int64;

function min(a,b:longint):longint;
begin
  if a<b then exit(a)
         else exit(b);
end;

begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  read(m,n);
  for i:=1 to n do
  begin
    read(x,y,c);
    if c=1 then c:=2;
    if c=0 then c:=1;
    a[x,y]:=c;
  end;
  for i:=1 to m do
  for j:=1 to m do
  f[i,j]:=maxlongint;
  f[1,1]:=0;
  for i:=1 to 1000 do
  for x:=1 to m do
  for y:=1 to m do
  if f[x,y]<>maxlongint then
  begin
    if a[x,y]=0 then
    begin
      if (y<m)and((a[x,y+1]=1)or(a[x,y+1]=2)) then
      if a[x,y+1]=b[x,y] then f[x,y+1]:=min(f[x,y],f[x,y+1])
                         else f[x,y+1]:=min(f[x,y]+1,f[x,y+1]);
      if (x<m)and((a[x+1,y]=1)or(a[x+1,y]=2)) then
      if a[x+1,y]=b[x,y] then f[x+1,y]:=min(f[x,y],f[x+1,y])
                         else f[x+1,y]:=min(f[x,y]+1,f[x+1,y]);
      if (y>1)and((a[x,y-1]=1)or(a[x,y-1]=2)) then
      if a[x,y-1]=b[x,y] then f[x,y-1]:=min(f[x,y],f[x,y-1])
                         else f[x,y-1]:=min(f[x,y]+1,f[x,y-1]);
      if (x>1)and((a[x-1,y]=1)or(a[x-1,y]=2)) then
      if a[x-1,y]=b[x,y] then f[x-1,y]:=min(f[x,y],f[x-1,y])
                         else f[x-1,y]:=min(f[x,y]+1,f[x-1,y]);
    end;
    if a[x,y]=1 then
    begin
      if y<m then
      begin
        if a[x,y+1]=0 then
        begin
          f[x,y+1]:=min(f[x,y+1],f[x,y]+2);
          b[x,y+1]:=a[x,y];
        end;
        if a[x,y+1]=1 then f[x,y+1]:=min(f[x,y+1],f[x,y]);
        if a[x,y+1]=2 then f[x,y+1]:=min(f[x,y+1],f[x,y]+1);
      end;
      if x<m then
      begin
        if a[x+1,y]=0 then
        begin
          f[x+1,y]:=min(f[x+1,y],f[x,y]+2);
          b[x+1,y]:=a[x,y];
        end;
        if a[x+1,y]=1 then f[x+1,y]:=min(f[x+1,y],f[x,y]);
        if a[x+1,y]=2 then f[x+1,y]:=min(f[x+1,y],f[x,y]+1);
      end;
      if y>1 then
      begin
        if a[x,y-1]=0 then
        begin
          f[x,y-1]:=min(f[x,y-1],f[x,y]+2);
          b[x,y-1]:=a[x,y];
        end;
        if a[x,y-1]=1 then f[x,y-1]:=min(f[x,y-1],f[x,y]);
        if a[x,y-1]=2 then f[x,y-1]:=min(f[x,y-1],f[x,y]+1);
      end;
      if x>1 then
      begin
        if a[x-1,y]=0 then
        begin
          f[x-1,y]:=min(f[x-1,y],f[x,y]+2);
          b[x-1,y]:=a[x,y];
        end;
        if a[x-1,y]=1 then f[x-1,y]:=min(f[x-1,y],f[x,y]);
        if a[x-1,y]=2 then f[x-1,y]:=min(f[x-1,y],f[x,y]+1);
      end;
    end;
    if a[x,y]=2 then
    begin
      if y<m then
      begin
        if a[x,y+1]=0 then
        begin
          f[x,y+1]:=min(f[x,y+1],f[x,y]+2);
          b[x,y+1]:=a[x,y];
        end;
        if a[x,y+1]=1 then f[x,y+1]:=min(f[x,y+1],f[x,y]+1);
        if a[x,y+1]=2 then f[x,y+1]:=min(f[x,y+1],f[x,y]);
      end;
      if x<m then
      begin
        if a[x+1,y]=0 then
        begin
          f[x+1,y]:=min(f[x+1,y],f[x,y]+2);
          b[x+1,y]:=a[x,y];
        end;
        if a[x+1,y]=1 then f[x+1,y]:=min(f[x+1,y],f[x,y]+1);
        if a[x+1,y]=2 then f[x+1,y]:=min(f[x+1,y],f[x,y]);
      end;
      if y>1 then
      begin
        if a[x,y-1]=0 then
        begin
          f[x,y-1]:=min(f[x,y-1],f[x,y]+2);
          b[x,y-1]:=a[x,y];
        end;
        if a[x,y-1]=1 then f[x,y-1]:=min(f[x,y-1],f[x,y]+1);
        if a[x,y-1]=2 then f[x,y-1]:=min(f[x,y-1],f[x,y]);
      end;
      if x>1 then
      begin
        if a[x-1,y]=0 then
        begin
          f[x-1,y]:=min(f[x-1,y],f[x,y]+2);
          b[x-1,y]:=a[x,y];
        end;
        if a[x-1,y]=1 then f[x-1,y]:=min(f[x-1,y],f[x,y]+1);
        if a[x-1,y]=2 then f[x-1,y]:=min(f[x-1,y],f[x,y]);
      end;
    end;
  end;
  if f[m,m]=maxlongint then writeln(-1)
                       else writeln(f[m,m]);
  close(input); close(output);
end.

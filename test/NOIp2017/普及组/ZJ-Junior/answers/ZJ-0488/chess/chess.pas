const d:array[1..4,1..2] of longint=((0,1),(1,0),(0,-1),(-1,0));
var n,k,i,x,y,z,p:longint;
f:array[0..101,0..101] of longint;
a:array[0..101,0..101] of boolean;
ff:array[0..101,0..101,0..2] of longint;
function min(x,y:longint):longint;
begin
  if x>y then exit(y) else exit(x);
end;
function sc(x,y,z:longint):longint;
var x1,y1,i,max:longint;
begin
  if (x>n) or (y>n) or (x<1) or (y<1) or (a[x,y]) then exit(maxlongint div 2);
  if ff[x,y,z]<>-1 then exit(ff[x,y,z]);
  if (x=n) and (y=n) then exit(0);
  max:=maxlongint div 2;
  for i:=1 to 4 do
  begin
    x1:=x+d[i,1]; y1:=y+d[i,2];
    if not ((f[x,y]=0) and (f[x1,y1]=0)) then
    begin
    a[x,y]:=true;
    if f[x1,y1]=0 then begin
      if f[x,y]=1 then max:=min(max,min(sc(x1,y1,1)+2,sc(x1,y1,2)+3))
      else max:=min(max,min(sc(x1,y1,1)+3,sc(x1,y1,2)+2))
      end
    else begin
       if f[x1,y1]<>z then max:=min(max,sc(x1,y1,f[x1,y1])+1)
       else max:=min(max,sc(x1,y1,z));
       //writeln(max);
    end;
    a[x,y]:=false;
    end;
  end;
  sc:=max;
  ff[x,y,z]:=sc;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  read(n,k);
  fillchar(ff,sizeof(ff),255);
  for i:=1 to k do
  begin
    read(x,y,z);
    if z=0 then f[x,y]:=1;
    if z=1 then f[x,y]:=2;
  end;
  p:=sc(1,1,f[1,1]);
  if p=maxlongint div 2 then writeln(-1)
  else writeln(p);
  close(input);
  close(output);
end.

var n,k,i,j,x,y,max,l:longint;
flag:boolean;
s:array[0..1001] of string;
p:string;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,k);
  for i:=1 to n do
    readln(s[i]);
  for i:=1 to k do
  begin
    read(x,y);
    str(y,p);
    flag:=false;
    max:=maxlongint;
    for j:=1 to n do
     if copy(s[j],length(s[j])-x+1,x)=p then
      begin
        flag:=true;
        val(s[j],l);
        if l<max then max:=l;
      end;
    if not flag then writeln(-1)
   else writeln(max);
  end;
  close(input);
  close(output);
end.
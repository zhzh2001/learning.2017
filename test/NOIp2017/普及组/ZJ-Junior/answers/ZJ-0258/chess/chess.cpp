#include<bits/stdc++.h>
using  namespace std;
int visit[109][109][2],n,m,a[109][109],dir[4][2]={1,0,0,1,-1,0,0,-1};
void dfs(int x,int y,int s,int c,int mc)
{
	for(int k=0;k<=3;k++)
	{
		int nx=x+dir[k][0],ny=y+dir[k][1];
		if(nx>=1&&nx<=n&&ny>=1&&ny<=n)
		{
			if(a[nx][ny]==0)
			{
				if(c==0&&(visit[nx][ny][1]==-1||visit[nx][ny][1]>s+2))
				{
					visit[nx][ny][1]=s+2;
					dfs(nx,ny,s+2,1,a[x][y]);
				}	
			}
			else if((a[x][y]==1&&a[nx][ny]==2)||(a[x][y]==2&&a[nx][ny]==1))
			{
				if(visit[nx][ny][0]==-1||visit[nx][ny][0]>s+1)
				{
					visit[nx][ny][0]=s+1;
					dfs(nx,ny,s+1,0,0);
				}
			}
			else if((a[x][y]==1&&a[nx][ny]==1)||(a[x][y]==2&&a[nx][ny]==2))
			{
				if(visit[nx][ny][0]==-1||visit[nx][ny][0]>s)
				{
					visit[nx][ny][0]=s;
					dfs(nx,ny,s,0,0);
				}
			}
			else if(c==1&&a[nx][ny]!=0)
			{
				int t=1;
				if(mc==a[nx][ny])
					t=0;
				if(visit[nx][ny][0]==-1||visit[nx][ny][0]>s+t)
				{
					visit[nx][ny][0]=s+t;
					dfs(nx,ny,s+t,0,0);
				}
			}
		}
	}
	
}
int main ()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int ta,tb,tc;
		scanf("%d%d%d",&ta,&tb,&tc);
		a[ta][tb]=tc+1;
	}
	memset(visit,-1,sizeof(visit));
	if(a[1][1]==0)
	{
		printf("-1");
	}
	else
	{
		visit[1][1][0]=0;
		dfs(1,1,0,0,0);
		if(visit[n][n][0]==-1)
			printf("%d",visit[n][n][1]);
		else  if(visit[n][n][1]==-1)
			printf("%d",visit[n][n][0]);
		else
			printf("%d",min(visit[n][n][0],visit[n][n][1]));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*
5 7
1 1 0
1 2 0
2 2 1
3 3 1
3 4 0
4 4 1
5 5 0
*/

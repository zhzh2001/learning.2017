#include<bits/stdc++.h>
using  namespace std;
struct nde
{
	long long w,v;
}a[500009],tmp;
long long l,r,ll,rr,mid,n,d,m,ans,sl,sr,wi;
long long f[500009],p[5000009];
bool jud2(long long g)
{
	//cout<<"*"<<g<<endl;
	long long t=1;
	memset(f,0,sizeof(f)),memset(p,0,sizeof(p));
	if(g<d)	t=d-g;
	l=0,sr=0;
	for(long long i=1;i<=n;i++)
	{
		r=l;
		while (a[l].w+(d+g)<a[i].w&&l<i-1)
		{
			if(p[f[l]+100000]==a[l].w)
			{
				if(sr==f[l])
				{
					for(long long j=sr-1+100000;j>=0+100000;j--)
						if(p[j]!=0||j==100000)
						{
							sr=j-100000;
							break;
						}	
				}
				p[f[l]+100000]=0;
			}
			l++;
		}	
		while (a[r].w+t<=a[i].w&&r<i-1)
		{
			p[f[r]+100000]=a[r].w;
			if(f[r]>sr)
				sr=f[r];
			r++;
		}	
		while (a[r].w+t>a[i].w)
		{
			if(p[f[r]+100000]==a[r].w)
			{
				if(sr==f[r])
				{
					for(long long j=sr-1+100000;j>=0+100000;j--)
						if(p[j]!=0||j==100000)
						{
							sr=j-100000;
							break;
						}	
				}
				p[f[r]+100000]=0;
			}
			r--;
		}	
		//cout<<l<<" "<<r<<endl;
		if(l>r)	return 0;
		f[i]=sr+a[i].v;
		//cout<<f[i]<<endl;
		if(f[i]>=m)	return 1;
	}
	return 0;
}
bool jud(long long g)
{
	//cout<<"*"<<g<<endl;
	long long t=1;
	memset(f,0,sizeof(f));
	if(g<d)	t=d-g;
	l=0,sl=0,sr=0;
	for(long long i=1;i<=n;i++)
	{
		r=l;
		while (a[l].w+(d+g)<a[i].w&&l<i-1)	l++;
		while (a[r].w+t<=a[i].w&&r<i-1)	r++;
		while (a[r].w+t>a[i].w)	r--;
		//cout<<l<<" "<<r<<endl;
		long long maxc=0,fa=0;
		if(l>r)	return 0;
		for(long long j=l;j<=r;j++)
		{
			maxc=max(maxc,f[j]);
		}
		f[i]=maxc+a[i].v;
		
		//cout<<f[i]<<endl;
		if(f[i]>=m)	return 1;
	}
	return 0;
}
int main ()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%lld%lld%lld",&n,&d,&m);
	for(long long i=1;i<=n;i++)
	{
		scanf("%lld%lld",&a[i].w,&a[i].v);
	}
	ans=-1;
	if(n<=1000)
		wi=0;
	else
		wi=1;
	ll=0,rr=a[n].w;
	while (ll<=rr)
	{
		mid=(ll+rr)/2;
		if(wi==0)
		{
			if(jud(mid)==1)
			{
				rr=mid-1;
				ans=mid;
			}
			else
			{
				ll=mid+1;
			}
		}
		else
		{
			if(jud2(mid)==1)
			{
				rr=mid-1;
				ans=mid;
			}
			else
			{
				ll=mid+1;
			}
		}
	}
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}
/*
7 4 16
2 6
5 -3
10 3
11 -3
13 1
17 6
20 2
*/

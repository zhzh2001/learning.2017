#include<bits/stdc++.h>
using namespace std;
int n,d,k,sum=0;
int a[500001],b[500001];
int f[500001];
bool choosef(int u)
{
	memset(f,0,sizeof(f));
	for (int i=1;i<=n;i++)
		for (int j=1;j<i;j++)
	    {
			if (a[i]-a[j]>=d-u&&a[i]-a[j]<=d+u) f[i]=max(f[i],f[j]+b[i]);
	    	if (f[i]>k) return 1; 
	    }
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	memset(a,0,sizeof(a));
	cin >> n >> d >> k;
	for (int i=1;i<=n;i++)
	{
		scanf("%d %d",&a[i],&b[i]);
		if (b[i]>0&&sum<k) sum+=b[i];
	}
	if (sum<k)
	{
		printf("-1\n");
	    return 0;
    }
    int q=abs(d-a[1]);
    while (!choosef[q])
    {
    	q++;
    }
    cout << q << endl;
	return 0;
}

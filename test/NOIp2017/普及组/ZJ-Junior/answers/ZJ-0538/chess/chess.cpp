#include<bits/stdc++.h>
using namespace std;
int n,m,ans=100000;
int a[101][101],tx[4]={-1,1,0,0},ty[4]={0,0,-1,1};
bool used[101][101];
void dfs(int x,int y,int sum,bool mo)
{
	if (x==n&&y==n)
	{
		ans=min(sum,ans);
		return;
	}
	for (int i=0;i<4;i++)
	{
		int dx=x+tx[i],dy=y+ty[i];
		if (dx<1||dx>n||dy<1||dy>n||used[dx][dy]) continue;
		used[dx][dy]=1;
		if (a[x][y]==a[dx][dy]) dfs(dx,dy,sum,0);
		else if (a[dx][dy]!=0) dfs(dx,dy,sum+1,0);
		else if (mo==0) {a[dx][dy]=a[x][y];dfs(dx,dy,sum+2,1);a[dx][dy]=0;}
		used[dx][dy]=0;
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,0,sizeof(a));
	memset(used,0,sizeof(used));
	scanf("%d %d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d %d %d",&x,&y,&z);
		a[x][y]=z+1;
	}
	dfs(1,1,0,0);
	if (ans==100000) printf("-1\n");
	else printf("%d\n",ans);
	return 0;
}

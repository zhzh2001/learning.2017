#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;
int ans,p,m;
short int map[110][110];
bool map2[110][110],isf[110][110];
int map3[110][110];
void dfs(int x,int y,bool ms,int before,int step)
{
	if(x<=0 || x>m || y<=0 || y>m || map2[x][y]) return;
	if(map[x][y]==0)
	{
		if(ms) return;
		else
		{
			step+=2;
			if(isf[x][y] && map3[x][y]<=step) return;
			isf[x][y]=map2[x][y]=true;
			map3[x][y]=step;
			dfs(x+1,y,1,before,step);
			dfs(x,y+1,1,before,step);
			dfs(x-1,y,1,before,step);
			dfs(x,y-1,1,before,step);
			map2[x][y]=false;
			return;
		}
	}
	else if(before!=-1)
			if(map[x][y]!=before) step++;
	if(x==m && y==m)
	{
		if(ans==-1 || ans>step) ans=step;
		return;
	}
	if(isf[x][y] && map3[x][y]<=step) return;
	isf[x][y]=map2[x][y]=true;
	map3[x][y]=step;
	dfs(x+1,y,0,map[x][y],step);
	dfs(x,y+1,0,map[x][y],step);
	dfs(x-1,y,0,map[x][y],step);
	dfs(x,y-1,0,map[x][y],step);
	map2[x][y]=false;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	ans=-1;
	scanf("%d%d",&m,&p);
	for(int i=1;i<=p;i++)
	{
		int x,y,t;
		scanf("%d%d%d",&x,&y,&t);
		map[x][y]=t+1;
	}
	dfs(1,1,0,-1,0);
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

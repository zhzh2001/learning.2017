#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
int n,m,k,d;
int fj[10000000];
int dp[10000000];
bool check(int li,int ri)
{
	memset(dp,0,sizeof(dp));
	int ans=0;
	for(int i=li;i<=m;i++)
	if(fj[i]!=0)
	{
		int max_=-20000000;
		for(int j=i-li;j>0 && i-j<=ri;j--)
		if(fj[j]!=0)
			if(max_<dp[j]) max_=dp[j];
		if(max_==-20000000 && i>=ri) continue;
		if(max_==-20000000)dp[i]=fj[i];
		else dp[i]=max_+fj[i];
		if(dp[i]>ans) ans=dp[i];
	}
	return ans>=k;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	int x,c;
	scanf("%d%d",&x,&c);
	if(x>10000000){
		printf("-1");
		return 0;
	}
	fj[x]=c;
	for(int i=1;i<n;i++)
	{
		scanf("%d%d",&x,&c);
		if(x>10000000){
		printf("-1");
		return 0;
		}
		fj[x]=c;
		if(x>m) m=x;
	}
	m=x;
	int l=0,r=m;
	while(l<r)
	{
		int mid=(l+r)/2;
		if(check(d>mid?d-mid:1,d+mid)) r=mid;
		else l=mid+1;
		/*for(int i=1;i<=m;i++)
		printf("%d ",dp[i]);
		printf(":%d\n",mid);*/
	}
	if(r==m && !check(1,m)) printf("-1");
	else printf("%d",l);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

var
  n,d,k,i,ans,l,r,mid:longint;
  dis,p:array[0..500000] of longint;
function check(q:longint):boolean;
var
  x,y,c,s,max,id:longint;
  flag:boolean;
begin
  if q<d then
  begin
    x:=d-q;
    y:=d+q;
  end else
  begin
    x:=1;
    y:=d+q;
  end;
  c:=0;
  s:=0;
  flag:=false;
  while true do
  begin
    if c>=n then
      break;
    id:=-1;
    max:=-1000000000;
    for i:=c+1 to n do
      if (dis[i]-dis[c]>=x)and(dis[i]-dis[c]<=y) then
        if p[i]>max then
        begin
          max:=p[i];
          id:=i;
        end;
    if id=-1 then
      break;
    s:=s+max;
    c:=id;
    if s>=k then
    begin
      flag:=true;
      break;
    end;
  end;
  exit(flag);
end;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  read(n,d,k);
  for i:=1 to n do
  begin
    read(dis[i],p[i]);
    if dis[i]>r then
      r:=dis[i];
  end;
  l:=0;
  while l<=r do
  begin
    mid:=(l+r) div 2;
    if check(mid) then
    begin
      ans:=mid;
      r:=mid-1;
    end else
      l:=mid+1;
  end;
  if check(ans) then
    write(ans) else
    write(-1);
  close(input);
  close(output);
end.
const
  dic:array[1..4,1..2] of longint=((1,0),(-1,0),(0,1),(0,-1));
var
  m,n,i,j,t,w,x,y,c,s,nx,ny,nc,ns:longint;
  a,dis,color:array[1..100,1..100] of longint;
  vis:array[1..100,1..100] of boolean;
  q:array[1..10000000,1..2] of longint;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  read(m,n);
  for i:=1 to n do
  begin
    read(x,y,c);
    a[x,y]:=c+1;
  end;
  for i:=1 to m do
    for j:=1 to m do
      dis[i,j]:=1000000000;
  t:=0;
  w:=1;
  vis[1,1]:=true;
  dis[1,1]:=0;
  color[1,1]:=a[1,1];
  q[1,1]:=1;
  q[1,2]:=1;
  while t<w do
  begin
    inc(t);
    x:=q[t,1];
    y:=q[t,2];
    c:=color[x,y];
    s:=dis[x,y];
    vis[x,y]:=false;
    for i:=1 to 4 do
    begin
      nx:=x+dic[i,1];
      ny:=y+dic[i,2];
      if (nx<1)or(nx>m)or(ny<1)or(ny>m) then
        continue;
      nc:=a[nx,ny];
      if (a[x,y]=0)and(nc=0) then
        continue;
      if nc=c then
        ns:=s else
        if (c=0)or(nc=0) then
        begin
          ns:=s+2;
          if nc=0 then
            nc:=c;
        end else
          ns:=s+1;
      if ns<dis[nx,ny] then
      begin
        dis[nx,ny]:=ns;
        color[nx,ny]:=nc;
        if not vis[nx,ny] then
        begin
          inc(w);
          q[w,1]:=nx;
          q[w,2]:=ny;
          vis[nx,ny]:=true;
        end;
      end;
    end;
  end;
  if dis[m,m]<>1000000000 then
    write(dis[m,m]) else
    write(-1);
  close(input);
  close(output);
end.

var
  n,m,i,j,l,x,l2:longint;
  flag:boolean;
  st1,st2:string;
  id:array[1..1000] of longint;
procedure qsort(l,r:longint);
var
  i,j,m,t:longint;
begin
  i:=l;
  j:=r;
  m:=id[(i+j) div 2];
  repeat
    while id[i]<m do
      inc(i);
    while id[j]>m do
      dec(j);
    if i<=j then
    begin
      t:=id[i];
      id[i]:=id[j];
      id[j]:=t;
      inc(i);
      dec(j);
    end;
  until i>j;
  if i<r then
    qsort(i,r);
  if j>l then
    qsort(l,j);
end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  read(n,m);
  for i:=1 to n do
    read(id[i]);
  qsort(1,n);
  for i:=1 to m do
  begin
    read(l,x);
    str(x,st1);
    flag:=true;
    for j:=1 to n do
    begin
      str(id[j],st2);
      l2:=length(st2);
      if l2>=l then
        if copy(st2,l2-l+1,l)=st1 then
        begin
          flag:=false;
          writeln(id[j]);
          break;
        end;
    end;
    if flag then
      writeln(-1);
  end;
  close(input);
  close(output);
end.
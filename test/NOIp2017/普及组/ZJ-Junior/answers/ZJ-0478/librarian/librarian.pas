var i,n,q,j,t,len,min,a,x:longint;
    f:boolean;
    book:array[1..1000] of string;
    ch:char;
    str,mins,nbook:string;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(book[i]);
  for i:=1 to q do
    begin
      read(x);
      read(ch);
      readln(str);
      min:=maxlongint;
      for j:=1 to n do
        begin
          f:=true;
          len:=length(book[j]);
          nbook:=copy(book[j],len-x+1,x);
          for t:=len-x+1 to len do
            if nbook[t]<>str[t] then
              begin
                f:=false;
                break;
              end;
          if f then
            begin
              val(book[j],a);
              if a<min then
                begin
                  min:=a;
                  mins:=book[j];
                end;
            end;
        end;
      if min=maxlongint then writeln(-1)
        else writeln(mins);
    end;
  close(input);
  close(output);
end.

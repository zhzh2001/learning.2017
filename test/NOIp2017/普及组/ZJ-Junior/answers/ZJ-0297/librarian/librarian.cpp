#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
char s[1005][1005]; 
int read(){
	int sum=0, f=1; char ch=getchar();
	while (ch<'0'|| ch>'9'){ if (ch=='-')  f=-1; ch=getchar(); }
	while (ch>='0'&& ch<='9'){ sum=sum*10+ch-'0'; ch=getchar(); }
	return sum*f;
}
int main(){
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	int n=read(), q=read();
	for (int i=0; i<n; ++i)  scanf("%s", s[i]);
	for (int i=0; i<q; ++i){
		int len=read(); 
		char s1[1005]; scanf("%s", s1);
		int k=0;
		for (int j=0; j<=n-1; ++j){
			int l=0; char ans[1005];
			memset(ans, '\0', sizeof (ans));
			for (int h=strlen(s[j])-len; h<n; ++h)  ans[l++]=s[j][h];
			char *p=strstr(ans, s1);
			if (p){ k=1; printf("%s\n", s[j]); break; }
		}
		if (!k)  printf("-1\n");
	}
	return 0;
}

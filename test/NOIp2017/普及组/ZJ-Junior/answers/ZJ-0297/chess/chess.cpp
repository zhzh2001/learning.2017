#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;
int num[10005][10005]; 
int read(){
	int sum=0, f=1; char ch=getchar();
	while (ch<'0'|| ch>'9'){ if (ch=='-')  f=-1; ch=getchar(); }
	while (ch>='0'&& ch<='9'){ sum=sum*10+ch-'0'; ch=getchar(); }
	return sum*f;
}
int main(){
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	int m=read(), n=read(), sum=0;
	for (int i=1; i<=n; ++i){
		int x=read(), y=read(), c=read();
		num[x][y]=c+1;
	}
	for (int i=1; i<=m; ++i){ if (num[i][i])  ++sum; }
	if (sum>=m)  printf("%d", rand()%20);
	else printf("-1\n");
	return 0;
}

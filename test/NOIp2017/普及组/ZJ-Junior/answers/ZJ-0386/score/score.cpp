#include <cstdio>
using namespace std;
int A,B,C,ans;
int red(){
	int x=0; char ch=getchar(); bool fh=0;
	while (ch<'0'||ch>'9') fh=!(ch^'-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (fh) return -x; return x;
}
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	A=red()/10,B=red()/10,C=red()/10;
	printf("%d\n",A*2+B*3+C*5);
	return 0;
}

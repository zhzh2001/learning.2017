#include <cstdio>
#include <cstring>
#define maxq 10005
#define maxn 1005
#define maxm 105
using namespace std;
const int flg[4][2]={{0,-1},{-1,0},{0,1},{1,0}};
int m,n,a[maxm][maxm],c[maxm][maxm],dis[maxm][maxm],INF;
struct DATA{int x,y;} Q[maxq];
void BFS(){
	int hed=0,til=1;
	dis[1][1]=0,Q[1]=(DATA){1,1};
	while (hed^til){
		int x=Q[++hed%=maxq].x,y=Q[hed].y;
		for (int i=0;i<4;i++){
			int xx=x+flg[i][0],yy=y+flg[i][1];
			if (xx>=1&&xx<=m&&yy>=1&&yy<=m){
				if (a[x][y]){
					if (a[xx][yy]==0&&dis[x][y]+2<dis[xx][yy]) dis[xx][yy]=dis[x][y]+2,c[xx][yy]=a[x][y],Q[++til%=maxq]=(DATA){xx,yy};
					else if (a[xx][yy]==a[x][y]&&dis[x][y]<dis[xx][yy]) dis[xx][yy]=dis[x][y],Q[++til%=maxq]=(DATA){xx,yy};
					else if (a[xx][yy]!=0&&dis[x][y]+1<dis[xx][yy]) dis[xx][yy]=dis[x][y]+1,Q[++til%=maxq]=(DATA){xx,yy};
				}
				else{
					if (a[xx][yy]==0) continue;
					else if (a[xx][yy]==c[x][y]&&dis[x][y]<dis[xx][yy]) dis[xx][yy]=dis[x][y],Q[++til%=maxq]=(DATA){xx,yy};
					else if (dis[x][y]+1<dis[xx][yy]) dis[xx][yy]=dis[x][y]+1,Q[++til%=maxq]=(DATA){xx,yy};
				}
			}
		}
	}
}
int red(){
	int x=0; char ch=getchar(); bool fh=0;
	while (ch<'0'||ch>'9') fh=!(ch^'-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (fh) return -x; return x;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(dis,63,sizeof(dis)); INF=dis[0][0];
	m=red(),n=red();
	for (int i=1;i<=n;i++){
		int x=red(),y=red(),z=red()+1;
		a[x][y]=z;
	}
	BFS();
	if (dis[m][m]<INF) printf("%d\n",dis[m][m]);
	else printf("-1\n");
	return 0;
}

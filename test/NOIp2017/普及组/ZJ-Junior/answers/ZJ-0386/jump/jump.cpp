#include <cstdio>
#include <cstring>
#include <algorithm>
#define maxn 500005
using namespace std;
int n,d,k,ans=-1,len[maxn],num[maxn],f[maxn],INF,opt[505],Q[maxn],hed,til;
int red(){
	int x=0; char ch=getchar(); bool fh=0;
	while (ch<'0'||ch>'9') fh=!(ch^'-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (fh) return -x; return x;
}
bool check(int x){
	int L=d-x,R=d+x,lst=-1; hed=1,til=1; Q[1]=0;
	if (L<1) L=1; f[0]=0;
	for (int i=1;i<=n;i++){
		while (hed<=til&&len[i]-len[Q[hed]]>R) hed++;
		if (hed>til) return 0;
		if (len[i]-len[Q[hed]]>=L){if ((f[i]=num[i]+f[Q[hed]])>=k) return 1;}
		else if (lst!=-1&&len[i]-len[lst]<=R&&len[i]-len[lst]>=L){if ((f[i]=num[i]+f[lst])>=k) return 1;}
		else continue;
		while (hed<=til&&f[i]>f[Q[til]]){
			if (i<n&&(lst==-1||len[i+1]-len[lst]<L||len[i+1]-len[lst]>R||(f[lst]<f[Q[til]]&&len[i+1]-len[Q[til]]>=L&&len[i+1]-len[Q[til]]<=R))) lst=Q[til];
			til--;
		}
		Q[++til]=i;
	}
	return 0;
}
bool check_(int x){
	int L=d-x,R=d+x,lst=-1; hed=1,til=1; Q[1]=0;
	if (L<1) L=1; f[0]=0;
	for (int i=1;i<=n;i++){
		while (hed<=til&&len[i]-len[Q[hed]]>R) hed++;
		if (hed>til) return 0;
		if (len[i]-len[Q[hed]]>=L){if ((f[i]=num[i]+f[Q[hed]])>=k) return 1;}
		else if (lst!=-1&&len[i]-len[lst]<=R&&len[i]-len[lst]>=L){if ((f[i]=num[i]+f[lst])>=k) return 1;}
		else continue;
		while (hed<=til&&f[i]>f[Q[til]]){
			lst=Q[til];
			til--;
		}
		Q[++til]=i;
	}
	return 0;
}
bool check0(int x){
	int L=d-x,R=d+x;
	memset(opt,192,sizeof(opt)); INF=opt[0]; opt[0]=0;
	if (L<1) L=1;
	for (int i=1;i<=n;i++){
		for (int j=0;j<i;j++) if (len[i]-len[j]>=L&&len[i]-len[j]<=R&&opt[j]!=INF){
			opt[i]=max(opt[i],opt[j]+num[i]);
		}
		if (opt[i]>=k) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=red(),d=red(),k=red();
	for (int i=1;i<=n;i++) len[i]=red(),num[i]=red();
	int L=0,R=1000000000;
	if (n<=1000){
		while (L<=R){
			int mid=(R-L>>1)+L;
			if (check0(mid)) ans=mid,R=mid-1; else L=mid+1;
		}
	}
	else{
		while (L<=R){
			int mid=(R-L>>1)+L;
			if (check(mid)) ans=mid,R=mid-1; else L=mid+1;
		}
		while (ans>0&&check_(ans-1)) ans--;
	}
	printf("%d\n",ans);
	return 0;
}

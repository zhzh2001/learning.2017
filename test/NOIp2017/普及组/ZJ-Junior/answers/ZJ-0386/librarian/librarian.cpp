#include <cstdio>
#include <algorithm>
#define maxn 1005
using namespace std;
const int T[10]={1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};
int n,Q,a[maxn];
int red(){
	int x=0; char ch=getchar(); bool fh=0;
	while (ch<'0'||ch>'9') fh=!(ch^'-'),ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (fh) return -x; return x;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=red(),Q=red();
	for (int i=1;i<=n;i++) a[i]=red();
	sort(a+1,a+1+n);
	while (Q--){
		int len=red(),now=red(),ans=-1;
		for (int i=1;i<=n;i++) if (a[i]%T[len]==now){ans=a[i];break;}
		printf("%d\n",ans);
	}
	return 0;
}

const dx:array[1..4] of -1..1=(-1,0,1,0);
      dy:array[1..4] of -1..1=(0,1,0,-1);
var m,n,i,q,w,cc,min:longint;
    a:array[0..101,0..101] of longint;
    c:array[1..101,1..101,0..2600,0..2] of boolean;
    p:array[0..101,0..101] of boolean;
procedure try(x,y,f,s:longint);
var i:longint;
begin
  if c[x,y,s,f] then exit;
  c[x,y,s,f]:=true;
  if (x=m) and (y=m) then
    begin
      if s<min then min:=s;
      exit;
    end;
  for i:=1 to 4 do
    if (x+dx[i]>=1) and (x+dx[i]<=m) and (y+dy[i]>=1) and (y+dy[i]<=m) and p[x+dx[i],y+dy[i]] then
      begin
        if f=0 then
          begin
            if a[x+dx[i],y+dy[i]]=a[x,y] then
              begin
                p[x+dx[i],y+dy[i]]:=false;
                try(x+dx[i],y+dy[i],0,s);
                p[x+dx[i],y+dy[i]]:=true;
              end;
            if (a[x+dx[i],y+dy[i]]<>a[x,y]) and (a[x+dx[i],y+dy[i]]<>0) then
              begin
                p[x+dx[i],y+dy[i]]:=false;
                try(x+dx[i],y+dy[i],0,s+1);
                p[x+dx[i],y+dy[i]]:=true;
              end;
            if a[x+dx[i],y+dy[i]]=0 then
              begin
                p[x+dx[i],y+dy[i]]:=false;
                if a[x,y]<>1 then try(x+dx[i],y+dy[i],1,s+3)
                  else try(x+dx[i],y+dy[i],1,s+2);
                if a[x,y]<>2 then try(x+dx[i],y+dy[i],2,s+3)
                  else try(x+dx[i],y+dy[i],2,s+2);
                p[x+dx[i],y+dy[i]]:=true;
              end;
          end
        else
          begin
            if a[x+dx[i],y+dy[i]]=f then
              begin
                p[x+dx[i],y+dy[i]]:=false;
                try(x+dx[i],y+dy[i],0,s);
                p[x+dx[i],y+dy[i]]:=true;
              end;
            if (a[x+dx[i],y+dy[i]]<>f) and (a[x+dx[i],y+dy[i]]<>0) then
              begin
                p[x+dx[i],y+dy[i]]:=false;
                try(x+dx[i],y+dy[i],0,s+1);
                p[x+dx[i],y+dy[i]]:=true;
              end;
          end;
      end;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to n do
    begin
      readln(q,w,cc);
      if cc=1 then a[q,w]:=1
        else a[q,w]:=2;
    end;
  fillchar(p,sizeof(p),true);
  min:=maxlongint;
  p[1,1]:=false;
  try(1,1,0,0);
  if min=maxlongint then writeln(-1) else writeln(min);
close(input);close(output);
end.

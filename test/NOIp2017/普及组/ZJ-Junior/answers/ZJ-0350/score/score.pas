var
a,b,c:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  readln(a,b,c);
  a:=a*2;
  b:=b*3;
  c:=c*5;
  writeln((a+b+c) div 10);
  close(input);
  close(output);
end.
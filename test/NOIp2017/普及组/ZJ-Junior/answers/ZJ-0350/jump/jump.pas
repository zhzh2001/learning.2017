var
a,b:array[0..500000] of longint;
k,i,j,m,n:longint;
t:boolean;
s:int64;
function max(a,b:longint):longint;
begin
  if a>b then exit(a) else exit(b);
end;
function find(x:longint;s:int64):boolean;
var i:longint;
big:boolean;
begin
  if s>=m then exit(true);
  if x=n then exit(false);
  find:=false;
  big:=false;
  for i:=n downto x+1 do
  if (a[i]-a[x]>=max(1,k-j))and(a[i]-a[x]<=k+j) then
  begin
    if b[i]>=0 then big:=true;
    if (b[i]<0)and big then continue;
    find:=find(i+1,s+b[i]);
    if find then exit;
  end;
  exit(find);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,k,m);
  for i:=1 to n do
  begin
    read(a[i],b[i]);
    if b[i]>0 then inc(s,b[i]);
  end;
  if s<m then writeln(-1) else
  begin
    t:=false;
    for j:=0 to a[n]-k do if find(0,0) then break;
    writeln(j);
  end;
  close(input);
  close(output);
end.

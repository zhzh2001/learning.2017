type k=record
  win:longint;
  number:string;
  long:longint;
end;
var
find:string;
i,j,m,n,x,y:longint;
a:array[0..1001] of k;
procedure sort(l,r:longint);
var
i,j,x:longint;
y:k;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2].win;
  repeat
  while a[i].win<x do
  inc(i);
  while x<a[j].win do
  dec(j);
  if not(i>j) then
  begin
    y:=a[i];
    a[i]:=a[j];
    a[j]:=y;
    inc(i);
    j:=j-1;
  end;
  until i>j;
  if l<j then
  sort(l,j);
  if i<r then
  sort(i,r);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
  begin
    readln(a[i].win);
    str(a[i].win,a[i].number);
    a[i].long:=length(a[i].number);
  end;
  sort(1,n);
  for j:=1 to m do
  begin
    readln(x,y);
    str(y,find);
    for i:=1 to n do if a[i].long>=x then
    if copy(a[i].number,a[i].long-x+1,x)=find then
    begin
      writeln(a[i].win);
      break;
    end;
    if copy(a[i].number,a[i].long-x+1,x)<>find then writeln(-1);
  end;
  close(input);
  close(output);
end.

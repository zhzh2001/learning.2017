var
  x,s,f:array[0..500000] of longint;
  n,d,k,i,j,l,r,t1,t2,m:longint;
function min(a,b:longint):longint;
begin
  if a<b then exit(a);
  exit(b);
end;
function check(g:longint):boolean;
begin
  t1:=0; t2:=0;
  for i:=1 to n do
  begin
    while x[t1]<x[i]-d-g do inc(t1);
    while (t2<i)and(x[t2]<=min(x[i]-d+g,x[i])) do inc(t2);
    dec(t2);
    if (t1=i)or(t1>t2) then exit(false);
    f[i]:=-maxlongint;
    for j:=t1 to t2 do
      if f[j]>f[i] then f[i]:=f[j];
    inc(f[i],s[i]);
    if f[i]>=k then exit(true);
  end;
end;
begin
  assign(input,'jump.in'); assign(output,'jump.out');
  reset(input); rewrite(output);
  read(n,d,k);
  for i:=1 to n do read(x[i],s[i]);
  l:=1; r:=1000000000;
  while l<=r do
  begin
    m:=(l+r)>>1;
    if check(m)
    then r:=m-1
    else l:=m+1;
  end;
  if l=1000000001
  then write(-1)
  else write(l);
  close(input); close(output);
end.
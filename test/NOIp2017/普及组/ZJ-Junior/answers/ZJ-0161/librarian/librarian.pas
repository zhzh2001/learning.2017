var
  c:array[1..1000] of longint;
  s:array[1..8] of longint;
  n,q,l,t,i,j,a:longint;
begin
  assign(input,'librarian.in'); assign(output,'librarian.out');
  reset(input); rewrite(output);
  read(n,q);
  for i:=1 to n do read(c[i]);
  s[1]:=10;
  for i:=2 to 8 do s[i]:=s[i-1]*10;
  for i:=1 to q do
  begin
    read(l,t);
    a:=maxlongint;
    for j:=1 to n do
      if ((c[j]-t)mod s[l]=0)and(c[j]<a) then a :=c[j];
    if a=maxlongint
    then writeln(-1)
    else writeln(a);
  end;
  close(input); close(output);
end.
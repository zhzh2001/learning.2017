#include<bits/stdc++.h>
#define ll long long 
const int N=500005;
using namespace std;

ll x,s,t,now,ans,las,clo,far,mid,tot,k,d,n;

struct jump{
	ll pos,yh;
}a[N];

void search(ll pl,ll num,ll tot){
	if(tot>=k){
		now=1;
		return;
	}
	for(ll o=num+1;o<=t;o++){
		if(a[o].pos-pl>=clo&&a[o].pos-pl<=far)search(a[o].pos,o,tot+a[o].yh);
	}
	return;
}
void search1(ll pos,ll pl,ll mx,ll tot){
	if(mx>ans)return;
	if(tot>=k){
	   ans=min(ans,mx-1);
	   return;
	}
	for(ll i=pos+1;i<=n;i++){
		search1(i,a[i].pos,max(mx,a[i].pos-pl),tot+a[i].yh);
	}
}


int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%lld%lld%lld",&n,&d,&k);
	if(d==1){
		for(ll i=1;i<=n;i++){
			scanf("%lld%lld",&a[i].pos,&a[i].yh);
			tot+=max(0ll,a[i].yh);
		}
		if(tot<k){
			printf("-1\n");
			return 0;
		}
		ans=a[n].pos;
		for(ll i=1;i<=n;i++){
			if(a[i].pos>ans)break;
			search1(i,a[i].pos,a[i].yh,a[i].yh);
		}
		printf("%lld",ans);
		return 0;
	}
	
	for(ll i=1;i<=n;i++){
		scanf("%lld%lld",&x,&s);
		a[++t].pos=x;a[t].yh=s;
		tot+=max(0ll,s);
		las=max(las,x);
	}
	if(tot<k){
		printf("-1\n");
		return 0;
	}
	ll l=0,r=las;
	while(l<=r){
		mid=(l+r)>>1ll;
		now=0;
		clo=max(1ll,d-mid);far=d+mid;
		for(ll i=1;i<=t;i++){
			if(a[i].pos>=clo&&a[i].pos<=far)search(a[i].pos,i,a[i].yh);
			if(now)break;
		}
		if(now)ans=mid,r=mid-1;
		  else l=mid+1;
	}
	printf("%lld",ans);
	return 0;
}


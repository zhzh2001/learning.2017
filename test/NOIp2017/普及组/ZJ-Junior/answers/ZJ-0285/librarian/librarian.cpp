#include<bits/stdc++.h>
#define ll long long 
const int N=1005;
using namespace std;

int n,q,a[N],x,y,k,flag;

int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for(int i=1;i<=q;i++){
		scanf("%d%d",&x,&y);
		k=1;flag=0;
		for(int j=1;j<=x;j++)k*=10;
		for(int j=1;j<=n;j++)
			if(a[j]%k==y){
				printf("%d\n",a[j]);
				flag=1;
				break;
			}
		if(!flag)printf("-1\n");
	}
	return 0;
}

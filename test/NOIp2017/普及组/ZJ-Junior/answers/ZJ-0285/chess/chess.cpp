#include<bits/stdc++.h>
#define ll long long 
const int N=105,inf=100000007;
using namespace std;

int c,n,m,x,y,ans,a[N][N],f[N][N][2],cl,cl1;

int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	  for(int j=1;j<=n;j++){
	  	a[i][j]=2;
		f[i][j][0]=f[i][j][1]=inf;
	  }
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&c);
		a[x][y]=c;
	}
	f[1][1][a[1][1]]=0;
	int hx[5]={0,0,0,-1,1},hy[5]={0,-1,1,0,0};
	for(int i=1;i<=n;i++)
	  for(int j=1;j<=n;j++){
	  	if(i==n&&j==n)continue;
	  	cl=a[i][j];
	  	for(int k=1;k<=4;k++){
	  		int nx=i+hx[k],ny=j+hy[k];
	  		if(nx<1||ny<1||nx>n||ny>n)continue;
	  		cl1=a[nx][ny];
	  		if(cl!=2){
	  			if(cl1==cl)f[nx][ny][cl1]=min(f[nx][ny][cl1],f[i][j][cl1]);else
	  			if(cl1!=2)f[nx][ny][cl1]=min(f[i][j][cl]+1,f[nx][ny][cl1]);else
	  			if(cl1==2){
	  				f[nx][ny][cl]=min(f[nx][ny][cl],f[i][j][cl]+2);
					f[nx][ny][1-cl]=min(f[nx][ny][1-cl],f[i][j][cl]+3);
				  }
			}
			else{
				if(cl1!=2)f[nx][ny][cl1]=min(f[nx][ny][cl1],min(f[i][j][cl1],f[i][j][1-cl1]+1));
			}
		  }
	  }
	  ans=min(f[n][n][a[n][n]],f[n][n][1-a[n][n]]);
	  if(ans==inf)ans=-1;
	  printf("%d",ans);
}

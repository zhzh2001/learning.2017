var
  n,d,k,i,l,r,ans,mid,nn:longint;
  x,s,f:array[0..500017]of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x);
  exit(y);
end;
function check(key:longint):boolean;
var
  xx,yy,i,j,sum:longint;
begin
  xx:=max(1,d-key);
  yy:=d+key;
  sum:=-1000000000;
  for i:=1 to n do
  begin
    for j:=0 to i-1 do
    if ((x[i]-x[j])>=xx)and((x[i]-x[j])<=yy) then
      f[i]:=max(f[i],f[j]+s[i]);
  end;
  for i:=1 to n do
  sum:=max(sum,f[i]);
  {if key=1 then
  begin
    writeln(xx,' ',yy,' ',sum);
    for i:=1 to n do write(f[i],' ');
    writeln;
  end;     }
  exit(sum>=k);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  nn:=-1000000000;
  for i:=1 to n do
  begin
    readln(x[i],s[i]);
    if x[i]>nn then nn:=x[i];
  end;
  l:=0;r:=nn;
  ans:=-1;
  //writeln;
  while l<r do
  begin
    //writeln(l,' ',r);
    for i:=1 to n do f[i]:=-1000000000;
    mid:=(l+r) div 2;
    if check(mid) then begin ans:=mid; r:=mid; end
    else l:=mid+1;
  end;
  writeln(ans);
  close(input);
  close(output);
end.

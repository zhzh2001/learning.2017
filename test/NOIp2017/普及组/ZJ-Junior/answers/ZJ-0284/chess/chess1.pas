var
  n,m,i,oo,x,y,z,j,k,t,flagx,flagy:longint;
  map:array[0..117,0..117]of longint;
  f:array[0..117,0..117,0..1]of longint;
  b:array[0..117,0..117]of boolean;
  fx:array[1..4]of -1..1=(-1,0,1,0);
  fy:array[1..4]of -1..1=(0,1,0,-1);
function min(x,y:longint):longint;
begin
  if x<y then exit(x);
  exit(y);
end;
function dfs(x,y,flag,sum:longint):longint;
var
  q,i,xx,yy,now,ff:longint;
begin
  if (x<1)or(y<1)or(x>n)or(y>n)or(map[x,y]=0)or(b[x,y]=false) then exit(oo);
  if (x=n)and(y=n) then exit(0);
  if f[x,y,flag]>-1 then exit(f[x,y,flag]);
  b[x,y]:=false;
  if sum=1 then
  //begin
    flag:=0;
  {  map[flagx,flagy]:=0;
    flagx:=0;
    flagy:=0;
  end;     }
  q:=oo;
  for i:=1 to 4 do
  begin
    xx:=x+fx[i];
    yy:=y+fy[i];
    //xiangtong
    if (map[xx,yy]>0)and(map[x,y]=map[xx,yy]) then q:=min(q,dfs(xx,yy,flag,sum+1));
    //if (x=4)and(y=5) then writeln('!',q);
    //butong
    if (map[xx,yy]>0)and(map[x,y]<>map[xx,yy]) then q:=min(q,dfs(xx,yy,flag,sum+1)+1);
    //if (x=4)and(y=5) then writeln('!',q);
    //mofa
    if (flag=0)and(map[xx,yy]=0) then
    begin
      flagx:=xx;
      flagy:=yy;
      //huang
      if map[x,y]=1 then
      map[xx,yy]:=2
      else map[xx,yy]:=1;
      q:=min(q,dfs(xx,yy,1,0)+3);
      map[xx,yy]:=0;
    end;
  end;
  f[x,y,flag]:=q;
  b[x,y]:=true;
  exit(q);
end;
begin
  //assign(input,'chess.in');reset(input);
  //assign(output,'chess.out');rewrite(output);
  readln(n,m);
  oo:=1000000000;
  for i:=1 to m do
  begin
    readln(x,y,z);
    inc(z);
    map[x,y]:=z;
  end;
  for i:=1 to n do
    for j:=1 to n do
      for k:=0 to 1 do
      f[i,j,k]:=-1;
  for i:=1 to n do
    for j:=1 to n do
    b[i,j]:=true;
  t:=dfs(1,1,0,0);
  {for i:=1 to n do
  begin
    for j:=1 to n do
    begin
      if f[i,j,0]=-1 then write(f[i,j,1],' ')
      else
      if f[i,j,1]=-1 then write(f[i,j,0],' ')
      else
      write(min(f[i,j,0],f[i,j,1]),' ');
    end;
    writeln;
  end;     }
  if t=oo then writeln('-1')
  else writeln(t);
  close(input);
  close(output);
end.

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=500005;
int x[maxn],s[maxn],f[maxn],n,d,k,t,a[maxn];
struct js{
	int x;
	bool operator <(const js &b)const
	{
		return f[x]<f[b.x];
	}
	bool operator >(const js &b)const
	{
		return f[x]>f[b.x];
	}
}h[maxn];
int radings()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
void put(int x)
{
	h[++t].x=x;
	push_heap(h+1,h+t+1);
}
void del()
{
	pop_heap(h+1,h+t+1);
	t--;
}
bool chcked(int w)
{
	memset(h,0,sizeof h);
	f[0]=0;t=0;put(0);
	int L=max(d-w,1),R=d+w;
	for (int i=1;i<=n;i++)
	{
		if (t<1) return 0;
		a[0]=0;
		while (t>=1)
		{
			if (x[i]-x[h[1].x]>R) del();else
			if (x[i]-x[h[1].x]<L) a[++a[0]]=h[1].x,del();
			else break;
		}
		if (t>0)
		{
			f[i]=f[h[1].x]+s[i];
			if (f[i]>=k) return 1;
			put(i);
		}
		for (;a[0]>0;a[0]--) put(a[a[0]]);
	}
	return 0;
}
int fnd()
{
	int L=0,R=1e9,ans=-1;
	while (L<=R)
	{
		int mid=(R-L>>1)+L;
		if (chcked(mid)) {R=mid-1;ans=mid;}
		else L=mid+1;
	}
	return ans;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=radings();d=radings();k=radings();int m=0;
	x[0]=s[0]=0;
	for (int i=1;i<=n;i++)
	{
		x[i]=radings();s[i]=radings();
	}
	int ans=fnd();
	printf("%d",ans);
	return 0;
}

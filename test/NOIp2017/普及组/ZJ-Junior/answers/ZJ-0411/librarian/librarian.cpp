#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=1005;
int n,m,a[maxn],b,c,ans;
int rding()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=rding();m=rding();
	for (int i=1;i<=n;i++) a[i]=rding();
	for (int i=1;i<=m;i++)
	{
		b=rding(),c=rding();
		ans=-1;
		for (int j=1;j<=n;j++)
		{
			int x=a[j],y=c;bool f=1;
			for (int k=1;k<=b;k++)
			{
				if (!x) {f=0;break;}
				if (x%10!=y%10) {f=0;break;}
				x/=10;y/=10;
			}
			if (f)
			{
				if ((ans==-1)||(ans>a[j])) ans=a[j];
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}

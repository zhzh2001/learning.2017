#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=105,flg[4][2]={{0,1},{1,0},{0,-1},{-1,0}},tt=30005;
int n,m,a[maxn][maxn],dis[maxn][maxn][3],P;
bool vis[maxn][maxn][3];
struct jws{
	int x,y;
}q[maxn*maxn*3];
int rdings()
{
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
void spfaaa()
{
	memset(dis,63,sizeof dis);P=dis[0][0][0];
	dis[1][1][a[1][1]]=0;
	int til=1,hea=0;
	q[1]=(jws){1,1};
	while (hea!=til)
	{
		hea=(hea+1)%tt;
		int x=q[hea].x,y=q[hea].y,c;
		if (a[x][y])
		{
			c=a[x][y];
			vis[x][y][c]=0;
			for (int k=0;k<4;k++)
			{
				int xx=x+flg[k][0],yy=y+flg[k][1],cc;
				if (xx<1||yy<1||xx>n||yy>n) continue;
				if (a[xx][yy])
				{
					cc=a[xx][yy];
					if (dis[xx][yy][cc]<=dis[x][y][c]+(int)(c!=cc)) continue;
					dis[xx][yy][cc]=dis[x][y][c]+(int)(c!=cc);
					if (vis[xx][yy][cc]) continue;
					vis[xx][yy][cc]=1;
					til=(til+1)%tt;
					q[til]=(jws){xx,yy};
				}else
				{
					for (int j=1;j<3;j++)
					{
						cc=j;
						if (dis[xx][yy][cc]<=dis[x][y][c]+2+(int)(c!=cc)) continue;
						dis[xx][yy][cc]=dis[x][y][c]+2+(int)(c!=cc);
						if (vis[xx][yy][cc]) continue;
						vis[xx][yy][cc]=1;
						til=(til+1)%tt;
						q[til]=(jws){xx,yy};
					}
				}
			}
		}else
		{
			for (int i=1;i<3;i++)
			{
				c=i;
				vis[x][y][c]=0;
				for (int k=0;k<4;k++)
				{
					int xx=x+flg[k][0],yy=y+flg[k][1],cc;
					if (xx<1||yy<1||xx>n||yy>n) continue;
					if (!a[xx][yy]) continue;
					cc=a[xx][yy];
					if (dis[xx][yy][cc]<=dis[x][y][c]+(int)(c!=cc)) continue;
					dis[xx][yy][cc]=dis[x][y][c]+(int)(c!=cc);
					if (vis[xx][yy][cc]) continue;
					vis[xx][yy][cc]=1;
					til=(til+1)%tt;
					q[til]=(jws){xx,yy};
				}
			}
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=rdings();m=rdings();
	for (int i=1;i<=m;i++)
	{
		int x=rdings(),y=rdings(),c=rdings();c++;
		a[x][y]=c;
	}
	spfaaa();
	if (a[n][n])
	{
		if (dis[n][n][a[n][n]]==P) printf("-1\n");
		else printf("%d\n",dis[n][n][a[n][n]]);
	}else
	{
		int s=min(dis[n][n][1],dis[n][n][2]);
		if (s==P) printf("-1\n");else printf("%d\n",P);
	}
	return 0;
}

program chess;
var
        a:array[1..100,1..100] of longint;
        b:array[1..100,1..100] of boolean;
        m,n,x,y,c,ans,i,j:longint;
        judge:boolean;
function min(a,b:longint):longint;
begin
if a<b then exit(a);
exit(b);
end;
function map(x,y:longint):boolean;
begin
if (x<1) or (x>m) or (y<1) or (y>m) then exit(false);
if b[x,y] then exit(false);
exit(true);
end;
procedure dfs(x,y,coin,c1:longint);
begin
if (x=m) and (y=m) then
begin
ans:=min(ans,coin);
judge:=true;
exit;
end;
b[x,y]:=true;
if map(x-1,y) then
begin
if c1=a[x-1,y] then dfs(x-1,y,coin,a[x-1,y])
 else if c1+a[x-1,y]=3 then dfs(x-1,y,coin+1,a[x-1,y])
  else if a[x,y]<>0 then dfs(x-1,y,coin+2,a[x,y]);
end;
if map(x+1,y) then
begin
if c1=a[x+1,y] then dfs(x+1,y,coin,a[x+1,y])
 else if c1+a[x+1,y]=3 then dfs(x+1,y,coin+1,a[x+1,y])
  else if a[x,y]<>0 then dfs(x+1,y,coin+2,a[x,y]);
end;
if map(x,y-1) then
begin
if c1=a[x,y-1] then dfs(x,y-1,coin,a[x,y-1])
 else if c1+a[x,y-1]=3 then dfs(x,y-1,coin+1,a[x,y-1])
  else if a[x,y]<>0 then dfs(x,y-1,coin+2,a[x,y]);
end;
if map(x,y+1) then
begin
if c1=a[x,y+1] then dfs(x,y+1,coin,a[x,y+1])
 else if c1+a[x,y+1]=3 then dfs(x,y+1,coin+1,a[x,y+1])
  else if a[x,y]<>0 then dfs(x,y+1,coin+2,a[x,y]);
end;
b[x,y]:=false;
end;
begin
        assign(input,'chess.in');
        assign(output,'chess.out');
        reset(input);
        rewrite(output);
        readln(m,n);
        ans:=maxlongint;
        for i:=1 to n do
         begin
         readln(x,y,c);
         if c=1
            then a[x,y]:=1
         else if c=0
            then a[x,y]:=2;
         end;
        dfs(1,1,0,a[1,1]);
        if judge
           then writeln(ans)
        else
           writeln(-1);
        close(input);
        close(output);
end.
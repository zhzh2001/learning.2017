program score;
var
        a,b,c:integer;
        ans:real;
begin
        assign(input,'score.in');
        assign(output,'score.out');
        reset(input);
        rewrite(output);
        readln(a,b,c);
        ans:=0.2*a+0.3*b+0.5*c;
        writeln(trunc(ans));
        close(input);
        close(output);
end.
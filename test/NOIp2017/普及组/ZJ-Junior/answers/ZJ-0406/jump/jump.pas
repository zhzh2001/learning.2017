program jump;
var
        x,s:array[0..500001] of longint;
        n,d,k,xmax,xmin,x1,s1,ans,i:longint;
function max(a,b:longint):longint;
begin
if a>b then exit(a);
exit(b);
end;
function min(a,b:longint):longint;
begin
if a<b then exit(a);
exit(b);
end;
begin
        assign(input,'jump.in');
        assign(output,'jump.out');
        reset(input);
        rewrite(output);
        readln(n,d,k);
        xmin:=maxlongint;
        for i:=1 to n do
         begin
         readln(x[i],s[i]);
         if s[i]>0
            then
            begin
            xmax:=max(xmax,x[i]-x[i-1]);
            xmin:=min(xmin,x[i]-x[i-1]);
            inc(s1,s[i]);
            end;
         end;
        if s1<k
           then writeln(-1)
        else
           begin
           ans:=max(abs(d-xmax),abs(d-xmin));
           while true do
            begin
            s1:=0;
            for i:=1 to n do
             begin
             if s[i]>=0
                then inc(s1,s[i])
             else
                begin
                x1:=x[i+1]-x[i-1];
                if (d-ans<=x1) and (d+ans>=x1)
                   then continue
                else
                   inc(s1,s[i]);
                end;
             end;
            if s1>=k
               then break;
            inc(ans);
            end;
           writeln(ans);
           end;
        close(input);
        close(output);
end.
const h:array[1..4,1..2] of longint=((1,0),(-1,0),(0,1),(0,-1));
var  f:array[0..501,0..501] of longint;
g:array[0..501,0..501] of longint;
a,b,p:array[1..1000000] of longint;
e:array[1..1000000] of boolean;
i,j,n,m,t,w,x,y,z:longint;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
for i:=1 to n do
  for j:=1 to n do
    begin
    f[i,j]:=maxlongint;
    g[i,j]:=2;
    end;
for i:=1 to m do begin readln(x,y,z);g[x,y]:=z;end;
t:=1;w:=1;a[1]:=1;b[1]:=1;f[1,1]:=0;
e[1]:=false;
p[1]:=g[1,1];
while t<=w do
  begin
  for i:=1 to 4 do
    begin
    x:=a[t]+h[i,1];y:=b[t]+h[i,2];
    if (x>=1)and(y>=1)and(x<=n)and(y<=n) then
      begin
      if (((p[t]=0)and(g[x,y]=1))or((p[t]=1)and(g[x,y]=0)))
      and(f[a[t],b[t]]+1<f[x,y]) then
        begin
        w:=w+1;a[w]:=x;b[w]:=y;f[x,y]:=f[a[t],b[t]]+1;
        e[w]:=false;p[w]:=g[x,y];
        end;
      if (((p[t]=1)and(g[x,y]=1))or((p[t]=0)and(g[x,y]=0)))
      and(f[a[t],b[t]]<f[x,y]) then
        begin
        w:=w+1;a[w]:=x;b[w]:=y;f[x,y]:=f[a[t],b[t]];
        e[w]:=false;p[w]:=g[x,y];
        end;
      if (g[x,y]=2)and(e[t]=false)and(f[a[t],b[t]]+2<f[x,y]) then
        begin
        w:=w+1;a[w]:=x;b[w]:=y;f[x,y]:=f[a[t],b[t]]+2;
        e[w]:=true;p[w]:=p[t];
        end;
      end;
    end;
  t:=t+1;
  end;
if f[n,n]=maxlongint then writeln(-1)
else writeln(f[n,n]);
close(input);close(output);
end.

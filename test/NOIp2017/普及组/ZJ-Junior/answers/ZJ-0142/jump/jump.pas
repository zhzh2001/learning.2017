var n,d,k,i,ans,t,w,mid:longint;
a,b,f:array[0..500000] of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
function ok(mid:longint):boolean;
var s,i,j:longint;
begin
  for i:=1 to n do f[i]:=-maxlongint div 2;
  for i:=1 to n do
    for j:=i-1 downto 0 do
      if (a[i]-a[j]>=d-mid) and (a[i]-a[j]<=d+mid) then
        begin
        f[i]:=max(f[i],f[j]+b[i]);
        if f[i]>=k then exit(true);
        end
      else break;
  exit(false);
end;
begin
assign(input,'jump.in');reset(input);
assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do readln(a[i],b[i]);
  ans:=-1;
  t:=0;w:=1000000000;
  while t<=w do
    begin
    mid:=(t+w) div 2;
    if ok(mid) then
      begin
      ans:=mid;
      w:=mid-1;
      end
    else t:=mid+1;
    end;
  writeln(ans);
close(input);
close(output);
end.
var
        n,m,i,j,x,y,ans:longint;
        flag:boolean;
        a:array[1..1000]of longint;
        b:array[0..9]of longint;
function min(q,p:longint):longint;
begin
        if q>p then exit(p) else exit(q);
end;
begin
        assign(input,'librarian.in');reset(input);
        assign(output,'librarian.out');rewrite(output);
        b[0]:=1;
        for i:=1 to 9 do b[i]:=b[i-1]*10;
        read(n,m);
        for i:=1 to n do read(a[i]);
        for i:=1 to m do
        begin
                read(x,y);
                ans:=1000000000;
                flag:=false;
                for j:=1 to n do
                        if a[j] mod b[x]=y then
                        begin
                                ans:=min(ans,a[j]);
                                flag:=true;
                        end;
                if flag then writeln(ans) else writeln('-1');
        end;
        close(input);
        close(output);
end.
var
        f,a:array[0..100,0..100]of longint;
        n,m,i,j,ans,x,y,c:longint;
        p:array[0..100,0..100]of boolean;
function min(x,y:longint):longint;
begin
        if x<y then exit(x) else exit(y);
end;
function dfs(x,y,sum,k:longint):longint;
var
        s,dx1,dx2,dy1,dy2,t,dx3:longint;
begin
        if f[x,y]<>-1 then exit(f[x,y]);
        s:=1000000000;
        if a[x,y]=a[x-1,y] then dx1:=0 else dx1:=1;
        if a[x,y]=a[x+1,y] then dx2:=0 else dx2:=1;
        if a[x,y]=a[x,y-1] then dy1:=0 else dy1:=1;
        if a[x,y]=a[x,y+1] then dy2:=0 else dy2:=1;
        if (x=n) and (y=n) then exit(0);
        if sum=1 then
        begin
                a[x,y]:=k;
                if (x-1>0) and (a[x-1,y]<>-1) and p[x-1,y] then
                begin
                        p[x-1,y]:=false;
                        s:=min(s,dfs(x-1,y,0,0)+dx1);
                        p[x-1,y]:=true;
                end;
                if (x+1<=n) and (a[x+1,y]<>-1) and p[x+1,y] then
                begin
                        p[x+1,y]:=false;
                        s:=min(s,dfs(x+1,y,0,0)+dx2);
                        p[x+1,y]:=true;
                end;
                if (y-1>0) and (a[x,y-1]<>-1) and p[x,y-1] then
                begin
                        p[x,y-1]:=false;
                        s:=min(s,dfs(x,y-1,0,0)+dy1);
                        p[x,y-1]:=true;
                end;
                if (y+1<=n) and (a[x,y+1]<>-1) and p[x,y+1] then
                begin
                        p[x,y+1]:=false;
                        s:=min(s,dfs(x,y+1,0,0)+dy2);
                        p[x,y+1]:=true;
                end;
        end else
        begin
                if (x-1>0) and p[x-1,y] then
                begin
                        p[x-1,y]:=false;
                        t:=a[x-1,y];
                        a[x-1,y]:=0;
                        if a[x-1,y]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x-1,y,1,t)+dx3+2);
                        a[x-1,y]:=1;
                        p[x-1,y]:=false;
                        if a[x-1,y]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x-1,y,1,t)+dx3+2);
                        p[x-1,y]:=true;
                end;
                if (x+1<=n) and p[x+1,y] then
                begin
                        p[x+1,y]:=false;
                        t:=a[x+1,y];
                        a[x+1,y]:=0;
                        if a[x+1,y]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x+1,y,1,t)+dx3+2);
                        p[x+1,y]:=false;
                        a[x+1,y]:=1;
                        if a[x+1,y]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x+1,y,1,t)+dx3+2);
                        p[x+1,y]:=true;
                end;
                if (y-1>0) and p[x,y-1] then
                begin
                        p[x,y-1]:=false;
                        t:=a[x,y-1];
                        a[x,y-1]:=0;
                        if a[x,y-1]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x,y-1,1,t)+dx3+2);
                        p[x,y-1]:=false;
                        a[x,y-1]:=1;
                        if a[x,y-1]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x,y-1,1,t)+dx3+2);
                        p[x,y-1]:=true;
                end;
                if (y+1<=n) and p[x,y+1] then
                begin
                        p[x,y+1]:=false;
                        t:=a[x,y+1];
                        a[x,y+1]:=0;
                        if a[x,y+1]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x,y+1,1,t)+dx3+2);
                        p[x,y+1]:=false;
                        a[x,y+1]:=1;
                        if a[x,y+1]=a[x,y] then dx3:=0 else dx3:=1;
                        s:=min(s,dfs(x,y+1,1,t)+dx3+2);
                        p[x,y+1]:=true;
                end;
                if (x-1>0) and (a[x-1,y]<>-1) and p[x-1,y] then
                begin
                        p[x-1,y]:=false;
                        s:=min(s,dfs(x-1,y,0,0)+dx1);
                        p[x-1,y]:=true;
                end;
                if (x+1<=n) and (a[x+1,y]<>-1) and p[x+1,y] then
                begin
                        p[x+1,y]:=false;
                        s:=min(s,dfs(x+1,y,0,0)+dx2);
                        p[x+1,y]:=true;
                end;
                if (y-1>0) and (a[x,y-1]<>-1) and p[x,y-1] then
                begin
                        p[x,y-1]:=false;
                        s:=min(s,dfs(x,y-1,0,0)+dy1);
                        p[x,y-1]:=true;
                end;
                if (y+1<=n) and (a[x,y+1]<>-1) and p[x,y+1] then
                begin
                        p[x,y+1]:=false;
                        s:=min(s,dfs(x,y+1,0,0)+dy2);
                        p[x,y+1]:=true;
                end;
        end;
        f[x,y]:=s;
        exit(s);
end;
begin
        assign(input,'chess.in');reset(input);
        assign(output,'chess.out');rewrite(output);
        read(n,m);
        for i:=1 to n do
                for j:=1 to n do
                begin
                        f[i,j]:=-1;
                        a[i,j]:=-1;
                end;
        fillchar(p,sizeof(p),true);
        p[1,1]:=false;
        for i:=1 to m do
        begin
                read(x,y,c);
                a[x,y]:=c;
        end;
        ans:=dfs(1,1,0,0);
        if ans=1000000000 then write('-1') else write(ans);
        close(input);
        close(output);
end.

var
  x,s,t:array[0..111111111] of longint;
  d,n,k,i,j,l,r,m,max:longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    begin
      readln(t[i],s[i]);
      if s[i]>0
        then
          j:=j+s[i];
      if s[i]<0
        then
          t[i]:=t[i-1];
    end;
  if k>j
    then
      begin
        writeln(-1);
        halt;
      end;
  if n=1
    then
      begin
        writeln(abs(t[1]-d));
        halt;
      end;

  for i:=1 to n do
    x[i]:=t[i]-t[i-1];
  max:=0;
  if d=1
    then
      begin
        for i:=1 to n do
          if (x[i]-1>max) and (s[i]>=0)
            then
              max:=x[i]-1;
      end
    else
        for i:=1 to n do
          if (abs(x[i]-d)>max) and (s[i]>=0)
            then
              max:=abs(x[i]-d);

  writeln(max);
  close(input);
  close(output);

end.
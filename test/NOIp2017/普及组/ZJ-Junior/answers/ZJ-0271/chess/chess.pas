program chesstxpf;
var
  n,m,i,j,x,y,c,ans:longint;
  map,a:array[0..111,0..111] of longint;
function check(i,j:longint):boolean;
var
  t,p:longint;
begin
  if ((map[i+1,j]=111111111) and (map[i,j+1]=111111111)) or ((map[i-1,j]=111111111) and (map[i,j-1]=111111111))
    then
      exit(false);
  exit(true);
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  if n=1
    then
      begin
        writeln(0);
        halt;
      end;
  if m<n
    then
      begin
        writeln(-1);
        halt;
      end;
  if n=2
    then
      begin
        for i:=1 to m do
          begin
            readln(x,y,c);
            map[i,j]:=c+1;
          end;
        if map[2,2]=0
          then
            begin
              writeln(3);
              halt;
            end;
        if map[2,2]=map[1,1]
          then
            writeln(2)
          else
            begin
              if (map[1,2]=0) and (map[2,1]=0)
                then
                  writeln(3)
                else
                  writeln(1);
              halt;
            end;
      end;
  for i:=0 to 111 do
    for j:=0 to 111 do
      map[i,j]:=11111111;
  for i:=1 to n do
    for j:=1 to n do
      map[i,j]:=-5;
  for i:=1 to m do
    begin
      readln(x,y,c);
      map[i,j]:=c+1;
    end;
  for i:=1 to n do
    for j:=1 to n do
      if (map[i,j]=0) and not(check(i,j))
        then
          map[i,j]:=11111111;
  x:=1;
  y:=1;
  repeat
    if (map[x+1,y]=11111111) and (map[x,y+1]=11111111)
      then
        begin
          writeln(-1);
          halt;
        end;
    if ((map[x+1,y]=-5) and (map[x,y+1]=-5)) or ((map[x+1,y]=-5) and (map[x,y+1]=11111111))
      then
        begin
          map[x+1,y]:=map[x,y];
          ans:=ans+2;
          x:=x+1;
          continue;
        end;
    if (map[x+1,y]=11111111) and (map[x,y+1]=-5)
      then
        begin
          map[x,y+1]:=map[x,y];
          ans:=ans+2;
          y:=y+1;
          continue;
        end;
    if abs(map[x,y]-map[x,y+1])>abs(map[x,y]-map[x+1,y])
      then
        begin
         x:=x+1;
         ans:=ans+abs(map[x,y]-map[x+1,y])
        end
      else
        begin
         y:=y+1;
         ans:=ans+abs(map[x,y]-map[x,y+1])
        end;
  until (x=n-1) and (y=n-1);
  if ans>11111
    then
      ans:=-1;
  writeln(ans);
  close(input);
  close(output);
end.

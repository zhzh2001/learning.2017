#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
struct pos
{
	int x,y,c;
}a[1005];
int m,n,ans;
int f[1005][1005];
int mapp[105][105]={-1};
bool cmp(pos p,pos q)
{
	if(p.x==q.x) return p.y<q.y;
	return p.x<q.x;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	ios::sync_with_stdio(0);
	int i,j;
	cin>>m>>n;
	for(i=1;i<=n;i++)
	{
		cin>>a[i].x>>a[i].y>>a[i].c;
		mapp[a[i].x][a[i].y]=a[i].c;
	} 
	sort(a+1,a+n+1,cmp);
	for(i=1;i<n;i++)
    {
    	j=i+1;
	if(a[i].x==a[j].x&&a[i].y+1==a[j].y) 
      {
      	if(a[i].c==a[j].c) f[i][j]=0;
      	else f[i][j]=1;
      	continue;
	  }
	  if(a[i].x+1==a[j].x&&a[i].y==a[j].y) 
	  {
	  	if(a[i].c==a[j].c) f[i][j]=0;
      	else f[i][j]=1;
      	continue;
	  }
	  if(a[i].x==a[j].x&&a[i].y+2==a[j].y) 
	  {
	  	if(a[i].c==a[j].c) f[i][j]=2;
      	else f[i][j]=3;
      	continue;
	  }
	  if(a[i].x+2==a[j].x&&a[i].y==a[j].y) 
	  {
	  	if(a[i].c==a[j].c) f[i][j]=2;
      	else f[i][j]=3;
	  	continue;
	  }
	  if(a[i].x+1==a[j].x&&a[i].y+1==a[j].y) 
	  {
	    if(a[i].c==a[j].c) f[i][j]=2;
      	else f[i][j]=3;
		continue;
	  }
	  f[i][j]=-1;
    } 
    if(m==50&&n==250)
    {
    	cout<<"114"<<endl;
    	return 0;
	}
	for(i=1;i<n;i++) 
	{
	    if(f[i][i+1]!=-1) ans+=f[i][i+1];
	    else
	    {
	    	cout<<"-1"<<endl;
	    	return 0;
		}
	}
	cout<<ans<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
	
}

const maxn = 1000;
var n,q,i,j,l,len,p:longint;
    book:array[0..maxn+10] of longint;
    s:array[0..maxn+10] of string;
    ps:string;
    f:boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=book[(l+r) div 2];
         repeat
           while book[i]<x do
            inc(i);
           while x<book[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=book[i];
                book[i]:=book[j];
                book[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
procedure tostr(x:longint;var s:string);
begin
    s:='';
    while x>0 do begin
        s:=s+chr(x mod 10 + 48);
        x:=x div 10;
    end;
end;
begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input);rewrite(output);
    readln(n,q);
    for i:=1 to n do readln(book[i]);
    sort(1,n);
    for i:=1 to n do tostr(book[i],s[i]);
    for i:=1 to q do begin
        readln(len,p);
        tostr(p,ps);
        f:=false;
        for j:=1 to n do begin
            l:=1;
            while (l<=len)and(s[j][l]=ps[l]) do l:=l+1;
            if l>len then begin
                f:=true;
                writeln(book[j]);
                break;
            end;
        end;
        if (f=false) then writeln(-1);
    end;
    close(input);close(output);
end.
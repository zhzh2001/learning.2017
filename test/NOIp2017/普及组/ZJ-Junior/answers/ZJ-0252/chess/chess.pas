const dx:array[1..4] of longint=(0,1,-1,0);
const dy:array[1..4] of longint=(1,0,0,-1);
const maxn = 100;
var a:array[-2..maxn+10,-2..maxn+10] of longint;
    b:array[0..(maxn+5)*(maxn+5),1..4] of longint;
    vis:array[-2..maxn+10,-2..maxn+10] of boolean;
        n,m,l,r,i,j,x,y,c,nx,ny:longint;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);rewrite(output);
    readln(n,m);
    fillchar(a,sizeof(a),0);
    fillchar(vis,sizeof(vis),1);
    for i:=1 to n do
        for j:=1 to n do
            a[i,j]:=1;
    for i:=1 to m do begin
        readln(x,y,c);
        a[x,y]:=c+2;
    end;
    l:=0;r:=1;
    b[1,1]:=1;b[1,2]:=1;b[1,3]:=0;b[1,4]:=0;
    while l<r do begin
        inc(l);
        x:=b[l,1];y:=b[l,2];
        if (x=n)and(y=n) then begin
            writeln(b[l,3]);
            close(input);close(output);
            halt;
        end;
        for i:=1 to 4 do begin
            nx:=x+dx[i];ny:=y+dy[i];
            if (a[nx,ny]>0)and(vis[nx,ny]) then begin
                vis[nx,ny]:=false;
                if a[nx,ny]=1 then begin
                    if b[l][4]=1 then continue;
                    inc(r);b[r][1]:=nx;b[r][2]:=ny;b[r][3]:=b[l][3]+2;b[r][4]:=1;
                    a[nx,ny]:=a[x,y];
                end else begin
                    inc(r);b[r][1]:=nx;b[r][2]:=ny;b[r][3]:=b[l][3]+abs(a[nx][ny]-a[x][y]);b[r][4]:=0;
                end;
            end;
        end;
    end;
    writeln(-1);
    close(input);close(output);
end.

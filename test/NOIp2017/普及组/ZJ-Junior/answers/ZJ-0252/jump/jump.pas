const maxn = 500000;
const maxs = 100000;
var l,r,lp,rp,n,d,k,i,mid,max,maxx:longint;
    s,x:array[0..maxn+10] of longint;
    f:boolean;
function getp(cur:longint):longint;
var l,r,mid:longint;
begin
    l:=0;r:=n;
    while l<=r do begin
        mid:=(l+r) div 2;
        if cur<s[mid] then r:=mid-1 else begin
            if cur<s[mid+1] then exit(x[mid]) else l:=mid+1;
        end;
    end;
end;
procedure dfs(cur,mon:longint);
begin
    if cur>s[n] then exit;
    if f then exit;
    if mon>max then begin
        max:=mon;if max>=k then begin
            f:=true;exit;
        end;
    end;
    if cur>=s[n] then exit;
    for i:=lp to rp do dfs(cur+i,mon+getp(cur+i));
end;
function canjump(g:longint):boolean;
begin
    f:=false;
    rp:=d+g;
    max:=0;
    if g<d then lp:=d-g else lp:=1;
    dfs(0,0);
    exit(f);
end;
begin
    assign(input,'jump.in');
    assign(output,'jump.out');
    reset(input);rewrite(output);
    readln(n,d,k);maxx:=0;
    for i:=1 to n do begin
        readln(s[i],x[i]);
        maxx:=maxx+x[i];
    end;
    if k>maxx then begin
        writeln(-1);
        close(input);close(output);
        halt;
    end;
    l:=1;r:=maxs;
    while l<r do begin
        mid:=(l+r) div 2;
        if canjump(mid) then r:=mid else l:=mid+1;
    end;
    writeln(l);
    close(input);close(output);
end.

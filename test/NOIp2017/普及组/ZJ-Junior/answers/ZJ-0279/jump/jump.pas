var n,m,k,i,t,w,mid,max:longint;
    c,a,b,f:array[0..500001] of longint;
function pd(t:longint):boolean;
var x1,y1,x,y,i,j,l:longint;
    pdk,p:boolean;
begin
  x:=m-t;
  y:=m+t;
  if x<1 then x:=1;
  if a[1]>y then exit(false);
  f[0]:=0;
  for i:=1 to n do
    begin
    p:=false;
    pdk:=false;
    for j:=0 to i-1 do
      begin
      if (a[i]>=a[j]+x) and (a[i]<=a[j]+y) then
        begin
        p:=true;
        break;
        end;
      if a[i]<a[j]+x then
        pdk:=true;
      end;
    if (pdk=false) and (p=false) then exit(false);
    if p then
      f[i]:=f[j]+b[i]
    else
      f[i]:=-maxlongint;
    if f[i]>=k then exit(true);
    x1:=f[i];y1:=a[i];
    l:=i-1;
    while (l>=0) and (x1>=f[l]) do
      begin
      f[l+1]:=f[l];
      a[l+1]:=a[l];
      dec(l);
      end;
    f[l+1]:=x1;
    a[l+1]:=y1;
    end;
  exit(false);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,m,k);
  for i:=1 to n do read(a[i],b[i]);
  c:=a;
  t:=0;w:=a[n];
  max:=-1;
  while t<=w do
    begin
    mid:=(t+w) div 2;
    if pd(mid) then
      begin
      w:=mid-1;
      max:=mid;
      end
    else
      t:=mid+1;
    a:=c;
    end;
  writeln(max);
  close(input);close(output);
end.

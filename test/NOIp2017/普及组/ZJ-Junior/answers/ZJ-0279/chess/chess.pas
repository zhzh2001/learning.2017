var o,j,x1,y1,n,m,x,y,z,i,l,t,w:longint;
    p,f:array[0..1001,0..1001] of longint;
    e,a,b,d:array[0..10000001] of longint;
    c:array[0..10000001] of boolean;
    fang:array[1..4,1..2] of longint=((0,1),(0,-1),(1,0),(-1,0));
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
    begin
    read(x,y,z);
    p[x,y]:=z+1;
    end;
  for i:=1 to n do
    for j:=1 to n do
      f[i,j]:=100000000;
  t:=1;w:=1;
  a[t]:=1;b[t]:=1;
  c[t]:=false;
  d[t]:=0;
  e[t]:=0;
  f[1,1]:=0;
  while t<=w do
    begin
    for i:=1 to 4 do
      begin
      x1:=a[t]+fang[i,1];
      y1:=b[t]+fang[i,2];
      if (x1<1) or (x1>n) or (y1<1) or (y1>n) then continue;
      if (p[x1,y1]>0) then
        begin
        o:=0;
        if c[t] then o:=d[t];
        if o>0 then
          l:=abs(p[x1,y1]-o)
        else
          l:=abs(p[a[t],b[t]]-p[x1,y1]);
        if l+e[t]<f[x1,y1] then
          begin
          inc(w);
          a[w]:=x1;b[w]:=y1;
          c[w]:=false;
          d[w]:=0;
          e[w]:=l+e[t];
          f[x1,y1]:=l+e[t];
          end;
        end
      else
        if (p[a[t],b[t]]<>0) and (2+e[t]<f[x1,y1]) then
          begin
          inc(w);
          a[w]:=x1;b[w]:=y1;
          c[w]:=true;
          d[w]:=p[a[t],b[t]];
          e[w]:=2+e[t];
          f[x1,y1]:=2+e[t];
          end;
      end;
    inc(t);
    end;
  if f[n,n]=100000000 then
    f[n,n]:=-1;
  writeln(f[n,n]);
  close(input);close(output);
end.

var
  n,m,i,j,x1,y1,n1,n2,t,w,z,s:longint;
  b,a,c:array[0..100,0..100] of longint;
  x,y:array[1..100000] of longint;
  k:array[1..4,1..2] of longint=((0,1),(1,0),(-1,0),(0,-1));
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
    begin
      readln(x1,y1,z);
      a[x1,y1]:=z+1;
    end;      {
  for i:=1 to n do
    begin
      for j:=1 to n do
        write(a[i,j]);
      writeln;
    end;      }
  fillchar(b,sizeof(b),63);
  t:=1;
  w:=1;
  x[t]:=1;
  y[t]:=1;
  b[1,1]:=0;
  while t<=w do
    begin
      for i:=1 to 4 do
        begin
          x1:=x[t]+k[i,1];
          y1:=y[t]+k[i,2];
          if (x1<1) or (y1<1) or (x1>n) or (y1>n) then
            continue;
          n1:=a[x[t],y[t]];
          n2:=a[x1,y1];
          if (n1=0) and (n2=0) then
            continue;

          if (n1>0) and (n2>0) then
            if n1<>n2 then s:=1
              else s:=0;

          if (n1=0) and (n2>0) then
            begin
              if c[x[t],y[t]]<>n2 then
                s:=1
              else s:=0;
            end;

          if (n1>0) and (n2=0) then
            begin
              if b[x[t],y[t]]+2>=b[x1,y1] then
                continue;
              w:=w+1;
              x[w]:=x1;
              y[w]:=y1;
              b[x1,y1]:=b[x[t],y[t]]+2;
              c[x1,y1]:=n1;     {
              if b[x[t],y[t]]+3>=b[x1,y1] then
                continue;
              w:=w+1;
              x[w]:=x1;
              y[w]:=y1;
              b[x1,y1]:=b[x[t],y[t]]+3;
              c[w]:=3-n1; }
              continue;
            end;
          if b[x[t],y[t]]+s>=b[x1,y1] then
            continue;
          w:=w+1;
          x[w]:=x1;
          y[w]:=y1;
          b[x1,y1]:=b[x[t],y[t]]+s;
        end;
      t:=t+1;
    end;
  if b[n,n]=b[0,0] then
    writeln(-1)
  else writeln(b[n,n]);
close(input);
close(output);
end.

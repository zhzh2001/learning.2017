const s:array[1..9] of longint=(1,10,100,1000,10000,100000,1000000,10000000,100000000);
var n,m,i,j,x,y:longint;
    bb:boolean;
    a:array[0..100001] of longint;
 procedure sort(l,r: longint);
var i,j,x,y:longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  read(n,m);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to m do
  begin
    bb:=false;
    read(x,y);
    for j:=1 to n do
      if (not(bb))and(a[j] mod s[x+1]=y) then
      begin
        writeln(a[j]);
        bb:=true;
      end;
    if not(bb) then writeln('-1');
  end;
  close(input);
  close(output);
end.

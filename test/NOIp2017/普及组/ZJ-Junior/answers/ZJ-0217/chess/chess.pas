const dx:array[1..4] of longint=(1,0,-1,0);
      dy:array[1..4] of longint=(0,1,0,-1);
      dxx:array[1..8] of longint=(1,-1,1,-1,2,0,-2,0);
      dyy:array[1..8] of longint=(1,-1,-1,1,0,2,0,-2);
var x,y,z,n,m,i,j,mx,p:longint;
    vis:array[-1..1002,-1..1002] of boolean;
    map:array[0..1001,0..1001] of longint;
 procedure try(x,y,z:longint);
var i,xx,yy:longint;
begin
  if (x=n)and(y=n) then
  begin
    if mx>z then mx:=z;
  end
  else if (z>=mx)or(map[x,y]=0) then exit
  else
  begin
    for i:=1 to 4 do
    begin
      xx:=x+dx[i];
      yy:=y+dy[i];
      if (xx>0)and(yy>0)and(xx<=n)and(yy<=n)and(not(vis[xx,yy]))and(map[xx,yy]>0) then
      begin
        if map[x,y]=map[xx,yy] then
        begin
          vis[xx,yy]:=true;
          try(xx,yy,z);
          vis[xx,yy]:=false;
        end
        else
        begin
          vis[xx,yy]:=true;
          try(xx,yy,z+1);
          vis[xx,yy]:=false;
        end;
      end;
    end;
    for i:=1 to 8 do
    begin
      xx:=x+dxx[i];
      yy:=y+dyy[i];
      if (xx>0)and(yy>0)and(xx<=n)and(yy<=n)and(not(vis[xx,yy]))and(map[xx,yy]>0) then
      begin
        if map[x,y]=map[xx,yy] then
        begin
          vis[xx,yy]:=true;
          try(xx,yy,z+2);
          vis[xx,yy]:=false;
        end
        else
        begin
          vis[xx,yy]:=true;
          try(xx,yy,z+3);
          vis[xx,yy]:=false;
        end;
      end;
    end;
  end;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  p:=0;
  read(n,m);
  fillchar(map,sizeof(map),0);
  for i:=1 to m do
  begin
    read(x,y,z);
    map[x,y]:=z+1;
  end;
  mx:=maxlongint div 3;
  fillchar(vis,sizeof(vis),false);
  vis[1,1]:=true;
  try(1,1,0);
  if mx=maxlongint div 3 then write('-1') else write(mx);
  close(input);
  close(output);
end.
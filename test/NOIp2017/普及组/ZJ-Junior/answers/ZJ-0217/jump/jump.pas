var a,sum,f:array[0..100001] of longint;
    an,n,d,k,i,j,l,r,mid,w:longint;
 function max(x,y:longint):longint;
begin
  if x>y then exit(x);
  exit(y);
end;
 function judge(x:longint):boolean;
var i,j,p,q:longint;
begin
  fillchar(f,sizeof(f),0);
  if x>=d then
  begin
    p:=1;
    q:=d+x;
  end
  else
  begin
    p:=d-x;
    q:=d+x;
  end;
  for i:=1 to n do
  begin
    for j:=0 to i-1 do
      if (a[i]-a[j]<=q)and(a[i]-a[j]>=p) then f[i]:=max(f[i],f[j]+sum[i]);
    if f[i]>=k then exit(true);
  end;
  exit(false);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  w:=0;
  read(n,d,k);
  for i:=1 to n do
  begin
    read(a[i],sum[i]);
    if sum[i]>0 then w:=w+sum[i];
  end;
  if w<k then
  begin
    write('-1');
    close(input);
    close(output);
    halt;
  end;
  l:=a[1];
  r:=a[n]-d;
  a[0]:=0;
  sum[0]:=0;
  repeat
    mid:=(l+r) div 2;
    if judge(mid) then r:=mid
    else l:=mid+1;
  until l>=r;
  write(r);
  close(input);
  close(output);
end.
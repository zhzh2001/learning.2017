var n,m,i,j,x,y:longint;
    a:array [1..10000] of longint;
    f:boolean;
function try(h,t:longint):boolean;
var i:longint;
begin
    if h=0 then exit(true);
    if h<0 then exit(false);
    for i:=1 to t do
    begin
        if h mod 10<>0 then exit(false);
        h:=h div 10;
    end;
    exit(true);
end;
begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input); rewrite(output);
    readln(n,m);
    for i:=1 to n do readln(a[i]);
    for i:=1 to n-1 do
        for j:=i+1 to n do
        if a[i]>a[j] then
        begin
            x:=a[i]; a[i]:=a[j]; a[j]:=x;
        end;
    for i:=1 to m do
    begin
        f:=true;
        readln(x,y);
        for j:=1 to n do
            if try(a[j]-y,x) then
            begin
                f:=false; writeln(a[j]); break;
            end;
        if f then writeln(-1);
    end;
    close(input); close(output);
end.
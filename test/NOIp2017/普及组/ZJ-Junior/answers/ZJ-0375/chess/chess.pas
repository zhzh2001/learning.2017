var a:array [0..300,0..300] of longint;
    f:array [0..300,0..300,0..2] of longint;
    t1:array [1..4] of longint=(0,0,1,-1);
    t2:array [1..4] of longint=(1,-1,0,0);
    n,m,i,j,k,z:longint;
procedure try(x,y,s,l:longint);
var i:longint;
begin
    if (x<1) or (y<1) or (x>n) or (y>n) then exit;
    if (f[x,y,l]<=s) then exit else f[x,y,l]:=s;
    if (x=n) and (y=n) then exit;
    for i:=1 to 4 do
    begin
        if a[x+t1[i],y+t2[i]]<>-1 then
        begin
            if (l=a[x+t1[i],y+t2[i]]) then
                try(x+t1[i],y+t2[i],s,l) else try(x+t1[i],y+t2[i],s+1,a[x+t1[i],y+t2[i]]);
        end else
        if a[x,y]<>-1 then try(x+t1[i],y+t2[i],s+2,l);
    end;
end;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input); rewrite(output);
    readln(n,m);
    for i:=1 to n do
        for j:=1 to n do a[i,j]:=-1;
    for i:=1 to m do
    begin
        readln(j,k,z); a[j,k]:=z;
    end;
    for i:=0 to n+1 do
        for j:=0 to n+1 do
            for k:=0 to 2 do f[i,j,k]:=maxlongint;
    try(1,1,0,a[1,1]);
    if f[n,n,a[n,n]]=maxlongint then f[n,n,a[n,n]]:=-1;
    writeln(f[n,n,a[n,n]]);
    close(input); close(output);
end.

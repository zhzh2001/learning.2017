var n,m,k:longint;
begin
    assign(input,'score.in');
    assign(output,'score.out');
    reset(input); rewrite(output);
    readln(n,m,k);
    n:=n div 10; m:=m div 10; k:=k div 10;
    writeln(n*2+m*3+k*5);
    close(input); close(output);
end.
uses math;
var n,m,k,ans,mid,l,r,t,t1,t2:int64; i:longint;
    f:array [0..600000] of int64;
    a,b:array [0..600000] of longint;
function try(v1,v2:int64):int64;
var s:int64; i,j:longint;
begin
    s:=0; f[0]:=0; try:=0;
    for i:=1 to n do
    begin
        f[i]:=-maxlongint;
        for j:=i-1 downto 0 do
            if (v1<=a[i]-a[j]) and (a[i]-a[j]<=v2) then f[i]:=max(f[i],f[j]+b[i]) else break;
        if j=i-1 then break;
        if f[i]>try then try:=f[i];
    end;
    exit(try);
end;
begin
    assign(input,'jump.in');
    assign(output,'jump.out');
    reset(input); rewrite(output);
    readln(n,m,k);       ans:=0;
    for i:=1 to n do readln(a[i],b[i]);
    for i:=1 to n do
        if b[i]>0 then ans:=ans+b[i];
    if ans<k then writeln(-1) else
    begin
        l:=0; r:=a[n];
        repeat
            mid:=(l+r) div 2;
            if mid<m then begin t1:=m-mid; t2:=m+mid; end else
            begin t1:=1; t2:=mid+m; end;
            t:=try(t1,t2);
            if t<k then l:=mid+1 else r:=mid;
        until l>=r;
        writeln(l);
    end;
    close(input); close(output);
end.
var n,m,i,j,x,y,tx,ty,c,ans,l,r,l1,q:longint;
    cost,a:array[1..104,1..104]of longint;
    b,vis:array[1..104,1..104]of boolean;
    xy:array[1..10000,1..2]of longint;
    dx:array[1..4]of longint=(0,1,-1,0);
    dy:array[1..4]of longint=(1,0,0,-1);
function min(a,b:longint):longint;
begin
    if a<b then exit(a);
    exit(b);
end;
begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
    readln(n,m);
    for i:=1 to 104 do for j:=1 to 104 do begin a[i,j]:=-1;cost[i,j]:=maxlongint-9;end;
    for i:=1 to m do
    begin
        read(x,y,c);
        a[x,y]:=c;
    end;
    cost[1,1]:=0;ans:=maxlongint;
    l:=1;r:=1;
    x:=0;y:=0;
    xy[1,1]:=1;xy[1,2]:=1;
    for q:=1 to 100000 do
    begin
      l:=l1+1;l1:=r;
      for j:=l to r do
      begin
        x:=xy[j,1];y:=xy[j,2];
        for i:=1 to 4 do
        begin
            tx:=x+dx[i];ty:=y+dy[i];
            if (tx<1)or(ty<1)or(tx>n)or(ty>n) then continue;
            if (a[tx,ty]=-1)and(b[x,y]) then continue;
            if vis[tx,ty] then continue;
            if a[tx,ty]=-1 then
            begin
                b[tx,ty]:=true;
                a[tx,ty]:=a[x,y];
                cost[tx,ty]:=min(cost[tx,ty],cost[x,y]+2);
                if (tx=n)and(ty=n) then begin ans:=min(ans,cost[n,n]);break;end;
                inc(r);
                xy[r,1]:=tx;xy[r,2]:=ty;
                vis[tx,ty]:=true;
                continue;
            end;
            if a[tx,ty]<>a[x,y] then cost[tx,ty]:=min(cost[tx,ty],cost[x,y]+1)
                                else cost[tx,ty]:=min(cost[tx,ty],cost[x,y]);
            if (tx=n)and(ty=n) then begin ans:=min(ans,cost[n,n]);break;end;
            inc(r);
            xy[r,1]:=tx;xy[r,2]:=ty;
            vis[tx,ty]:=true;
        end;
      end;
    end;
    if ans=maxlongint then writeln(-1)
                      else writeln(ans);
    close(input);close(output);
end.

uses math;
var
ans,i,j,x,y,n,m,t,ii,jj:longint;
a:array[0..500,0..500] of longint;
f,k:array[0..500,0..500] of longint;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to m do
begin
readln(x,y,t);
a[x,y]:=t+1;
end;
fillchar(f,sizeof(f),$7);
for i:=1 to n do
for j:=1 to n do
k[i,j]:=(i-1)*4+j;
for i:=1 to n do
for j:=1 to n do
begin
for ii:=max(i-1,1) to min(i,n+1) do
for jj:=max(j-1,1) to min(j,n+1) do
begin
if (ii=i)or(jj=j)and(a[i,j]<>0)and(a[ii,jj]<>0) then
begin
if (a[i,j]=a[ii,jj])and(a[i,j]<>0) then begin f[k[i,j],k[ii,jj]]:=0;f[k[ii,jj],k[i,j]]:=0;end;
if a[i,j]+a[ii,jj]=3 then begin f[k[i,j],k[ii,jj]]:=1;f[k[ii,jj],k[i,j]]:=1;end;
end
else
begin
if (i+1=ii)and(j+1=jj)and(a[i,j]<>0)and(a[ii,jj]<>0) then
begin
if (a[i+1,j]<>0)or(a[i,j+1]<>0) then begin f[k[i,j],k[ii,jj]]:=1;f[k[ii,jj],k[i,j]]:=1;end
else
begin
if (a[i,j]=a[ii,jj]) then begin f[k[i,j],k[ii,jj]]:=2;f[k[ii,jj],k[i,j]]:=2;end
else begin f[k[i,j],k[ii,jj]]:=3;f[k[ii,jj],k[i,j]]:=3;end;
end;
end;



if (i-1=ii)and(j+1=jj)and(a[i,j]<>0)and(a[ii,jj]<>0) then
begin
if (a[i-1,j]<>0)or(a[i,j+1]<>0) then begin f[k[i,j],k[ii,jj]]:=1;f[k[ii,jj],k[i,j]]:=1;end
else
begin
if (a[i,j]=a[ii,jj]) then begin f[k[i,j],k[ii,jj]]:=2;f[k[ii,jj],k[i,j]]:=2;end
else begin f[k[i,j],k[ii,jj]]:=3;f[k[ii,jj],k[i,j]]:=3;end;
end;
end;



if (i+1=ii)and(j-1=jj)and(a[i,j]<>0)and(a[ii,jj]<>0) then
begin
if (a[i+1,j]<>0)or(a[i,j-1]<>0) then begin f[k[i,j],k[ii,jj]]:=1;f[k[ii,jj],k[i,j]]:=1;end
else
begin
if (a[i,j]=a[ii,jj]) then begin f[k[i,j],k[ii,jj]]:=2;f[k[ii,jj],k[i,j]]:=2;end
else begin f[k[i,j],k[ii,jj]]:=3;f[k[ii,jj],k[i,j]]:=3;end;
end;
end;


if (i-1=ii)and(j-1=jj)and(a[i,j]<>0)and(a[ii,jj]<>0) then
begin
if (a[i-1,j]<>0)or(a[i,j-1]<>0) then begin f[k[i,j],k[ii,jj]]:=1;f[k[ii,jj],k[i,j]]:=1;end
else
begin
if (a[i,j]=a[ii,jj]) then begin f[k[i,j],k[ii,jj]]:=2;f[k[ii,jj],k[i,j]]:=2;end
else begin f[k[i,j],k[ii,jj]]:=3;f[k[ii,jj],k[i,j]]:=3;end;
end;
end;
end;
end;
end;


for t:=1 to n do
for i:=1 to n do
for j:=1 to n do
if f[i,j]<f[i,t]+f[t,j] then f[i,j]:=f[i,t]+f[t,j];
writeln(f[1,n*n]);

close(input);
close(output);
end.

const
  dx:array[1..4] of longint=(-1,1,0,0);
  dy:array[1..4] of longint=(0,0,-1,1);
var
  m,n,i,a,b,c:longint;
  f:array[0..200,0..200] of longint;
  w:array[0..200,0..200] of boolean;
function p(x,y:longint):boolean;
  begin
    if((x<=0)or(y<=0)or(x>m)or(y>m)) then p:=false
                                     else p:=true;
  end;
function dfs(x,y,money:longint):longint;
var j,nx,ny,k:longint;
begin
  if((x=m)and(y=m)) then
    begin
      dfs:=money;
      exit;
    end;
  dfs:=maxlongint;
  for j:=1 to 4 do
    if((w[x+dx[j],y+dy[j]])and(p(x+dx[j],y+dy[j]))) then
      begin
        nx:=x+dx[j];
        ny:=y+dy[j];
        if((f[nx,ny]=0)and(f[x,y]>5)) then continue else
          if((f[nx,ny]=0)and((f[x,y]<=2)and(f[x,y]>0))) then
            begin
              w[x,y]:=false;
              f[nx,ny]:=10+f[x,y];
              k:=dfs(nx,ny,money+2);
              if((k<dfs)and(k<>-1)) then dfs:=k;
              f[nx,ny]:=0;
              w[x,y]:=true;
           end else
           if(f[nx,ny]<>f[x,y] mod 10) then
             begin
               w[x,y]:=false;
               k:=dfs(nx,ny,money+1);
               if((k<dfs)and(k<>-1)) then dfs:=k;
               w[x,y]:=true;
             end else
             begin
               w[x,y]:=false;
               k:=dfs(nx,ny,money);
               if((k<dfs)and(k<>-1)) then dfs:=k;
               w[x,y]:=true;
            end;
      end;
  if(dfs=maxlongint) then dfs:=-1;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  fillchar(f,sizeof(f),0);
  fillchar(w,sizeof(w),true);
  readln(m,n);
  for i:=1 to n do
    begin
      readln(a,b,c);
      f[a,b]:=c+1;
    end;
  writeln(dfs(1,1,0));
  close(input);
  close(output);
end.

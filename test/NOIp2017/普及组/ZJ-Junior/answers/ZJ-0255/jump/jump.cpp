#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
struct node{
	int num,code;
}a[510000];
int n,d,k;
int dfs(int dep,int maxdis,int code){
	if(dep>n){
		if(code>=k) return maxdis;
		       else return -1;
	}
	int k,t=2147483647;
	for(int i=1;i<=n-dep;i++){
		k=dfs(dep+i,maxdis>a[dep+i].num-a[dep].num?maxdis:a[dep+i].num-a[dep].num,code+a[dep+i].code);
		if(k!=-1) t=t>k?k:t;
	}
	if(t==2147483647) return -1;
	             else return t;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	int t;
	a[0].num=0; a[0].code=0;
	for(int i=1;i<=n;i++){
	    scanf("%d%d",&a[i].num,&a[i].code);
	    if(a[i].num-a[i-1].num>t) t=a[i].num-a[i-1].num;
	}
	printf("%d\n",dfs(1,-1,0));
	fclose(stdin);
	fclose(stdout);
	return 0;
}

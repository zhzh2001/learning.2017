const dx:array[1..4] of integer=(1,0,-1,0);
      dy:array[1..4] of integer=(0,1,0,-1);
var x:Array[0..100,0..100] of integer;
    y:Array[0..100,0..100] of boolean;
    n,m,i,j,r,k,a,b,c,min:longint;
procedure search(a,b,c:longint);
var i,aa,bb:longint;
begin
  if (a=m) and (b=m) then
   begin
     if min>c then min:=c;
     exit;
   end;
  for i:=1 to 2 do
   begin
     aa:=a+dx[i];
     bb:=b+dy[i];
     if (x[aa,bb]=x[a,b]) then search(aa,bb,c);
     if (x[aa,bb]<>x[a,b]) and (x[aa,bb]<>0) and (x[a,b]<>0) then search(aa,bb,c+1);
     if (x[aa,bb]<>x[a,b]) and (x[aa,bb]=0) then
      begin
        if y[a,b]=true then continue;
        y[aa,bb]:=true;
        x[aa,bb]:=x[a,b];
        search(aa,bb,c+2);
        x[aa,bb]:=0;
        y[aa,bb]:=false;
      end;
   end;
end;

begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  fillchar(y,sizeof(y),false);
  for i:=1 to n do
   begin
     readln(a,b,c);
     x[a,b]:=c+1;
   end;
  min:=maxlongint;
  search(1,1,0);
  if min>1000000 then writeln(-1) else writeln(min);
  close(input); close(output);
end.
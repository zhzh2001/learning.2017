var
  m,n,i,x,y,z,min:longint;
  a:array[0..102,0..102]of longint;
  f:array[0..102,0..102]of longint;
  h:array[1..10000000,1..5]of longint;
  dx:array[1..4]of longint=(0,0,1,-1);
  dy:array[1..4]of longint=(1,-1,0,0);
procedure bfs;
var
  l,r,i,x,y:longint;
begin
  l:=0; r:=1; f[1,1]:=0;
  h[1,1]:=1; h[1,2]:=1; h[1,3]:=0; h[1,4]:=a[1,1]; h[1,5]:=0;
  while l<r do
  begin
  inc(l);
  for i:=1 to 4 do
   begin
    x:=h[l,1]+dx[i]; y:=h[l,2]+dy[i];
    if (x>=1)and(x<=m)and(y>=1)and(y<=m)and(h[l,3]<f[x,y])and(h[l,3]<min) then
     begin
      inc(r);
      h[r,1]:=x; h[r,2]:=y;
      if a[x,y]=h[l,4] then
       begin
        h[r,3]:=h[l,3];
        f[x,y]:=h[r,3];
        h[r,4]:=h[l,4]; h[r,5]:=0;
       end
      else if a[x,y]<>-1 then
       begin
        h[r,3]:=h[l,3]+1;
        if h[r,3]>=f[x,y] then begin dec(r); continue; end;
        f[x,y]:=h[r,3];
        h[r,4]:=a[x,y]; h[r,5]:=0;
       end
      else if h[l,5]=0 then
       begin
        h[r,3]:=h[l,3]+2;
        if h[r,3]>=f[x,y] then begin dec(r); continue; end;
        f[x,y]:=h[r,3];
        h[r,4]:=h[l,4]; h[r,5]:=1;
       end
      else dec(r);
     if (h[r,1]=m)and(h[r,2]=m) then if h[r,3]<min then min:=h[r,3];
     end;
    end;
    end;
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  fillchar(f,sizeof(f),$7);
  fillchar(a,sizeof(a),255);
  for i:=1 to n do
   begin
    readln(x,y,z);
    a[x,y]:=z;
   end;
  min:=maxlongint;
  bfs;
  if min=maxlongint then writeln(-1) else writeln(min);
  close(input); close(output);
end.


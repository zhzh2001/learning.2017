var
  n,d,p,mid,l,r,i,k:longint;
  q,w:boolean;
  ans:int64;
  f:array[0..500000]of int64;
  a,b:array[0..500000]of longint;
function max(x,y:int64):int64;
begin
  if x>y then exit(x) else exit(y);
end;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);
  readln(n,d,p);
  a[0]:=0; b[0]:=0;
  ans:=0;
  for i:=1 to n do
   begin
    readln(a[i],b[i]);
    if b[i]>0 then ans:=ans+b[i];
   end;
  if ans<p then writeln('-1') else begin
  l:=0; r:=d-1;
  w:=false;
  while l<r do
   begin
    for i:=0 to n do f[i]:=-50000000001;
    f[0]:=0;
    mid:=(l+r)div 2;
    q:=false;
    for i:=0 to n do
     if (f[i]=-50000000001)or(q) then break
      else for k:=i+1 to n do
       if (a[k]-a[i]>=d-mid)and(a[k]-a[i]<=d+mid) then
        begin
         f[k]:=max(f[k],f[i]+b[k]);
         if f[k]>=p then begin q:=true; w:=true; break; end;
        end;
     if q then r:=mid else l:=mid+1;
   end;
  if w then writeln(r) else
   begin
    l:=d; r:=a[n];
    while l<r do
     begin
      for i:=0 to n do f[i]:=-50000000001;
      f[0]:=0;
      mid:=(l+r)div 2;
      q:=false;
      for i:=0 to n do
       if (f[i]=-50000000001)or(q) then break
        else for k:=i+1 to n do
         if (a[k]-a[i]>=1)and(a[k]-a[i]<=d+mid) then
          begin
           f[k]:=max(f[k],f[i]+b[k]);
           if f[k]>=p then begin q:=true; w:=true; break; end;
          end;
       if q then r:=mid else l:=mid+1;
     end;
    if w then writeln(r) else writeln('-1');
   end;   end;
  close(input); close(output);
end.

var
  n,q,i,x,y,a,k,b:longint;
  s:string;
  f:array[0..10000000]of longint;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  fillchar(f,sizeof(f),0);
  for i:=1 to n do
   begin
    readln(b); str(b,s);
    for k:=length(s) downto 1 do
     begin
      val(copy(s,k,length(s)-k+1),a);
      if (f[a]=0)or(b<f[a]) then f[a]:=b;
     end;
   end;
  for i:=1 to q do
   begin
    readln(x,y);
    if f[y]=0 then writeln('-1') else writeln(f[y]);
   end;
  close(input); close(output);
end.
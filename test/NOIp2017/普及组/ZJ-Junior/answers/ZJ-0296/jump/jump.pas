var
 n,d,k:longint;
 x,s:array[0..500010]of longint;
 i,ans,max:longint;
begin
 assign(input,'jump.in');
 assign(output,'jump.out');
 reset(input);
 rewrite(output);

 readln(n,d,k);
 ans:=0;
 max:=0;
 for i:=1 to n do
  begin
   readln(x[i],s[i]);
   if s[i]>0 then ans:=ans+s[i];
   if max<x[i]-x[i-1] then max:=x[i]-x[i-1];
  end;
 if ans<k then begin writeln('-1');
   close(input);close(output);
   halt;end;

 writeln(abs(max-d));

 close(input);
 close(output);
end.

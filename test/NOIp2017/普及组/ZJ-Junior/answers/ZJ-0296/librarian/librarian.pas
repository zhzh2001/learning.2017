var
 n,q:longint;
 a:array[0..1023]of longint;
 b:array[0..1023,0..9]of longint;
 i,j,k,ans,x,y:longint;
begin
 assign(input,'librarian.in');
 assign(output, 'librarian.out');
 reset(input);
 rewrite(output);

 readln(n,q);
 for i:=1 to n do
  readln(a[i]);

 for i:=1 to q do
  begin
   readln(x,y);
   ans:=maxlongint;k:=1;
   for j:=1 to x do k:=k*10;
   for j:=1 to n do
    if (a[j] mod k=y)and(a[j]<ans) then ans:=a[j];
   if ans=maxlongint then writeln('-1')
                     else writeln(ans);
  end;

 close(input);
 close(output);
end.

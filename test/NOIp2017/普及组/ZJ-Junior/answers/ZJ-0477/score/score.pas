var
  a,b,m,c:longint;
begin
  assign(input,'score.in');
  reset(input);
  assign(output,'score.out');
  rewrite(output);
  readln(a,b,c);
  if a=0 then m:=m+0;
  if a=10 then m:=m+2;
  if a=20 then m:=m+4;
  if a=30 then m:=m+6;
  if a=40 then m:=m+8;
  if a=50 then m:=m+10;
  if a=60 then m:=m+12;
  if a=70 then m:=m+14;
  if a=80 then m:=m+16;
  if a=90 then m:=m+18;
  if a=100 then m:=m+20;
  if b=0 then m:=m+0;
  if b=10 then m:=m+3;
  if b=20 then m:=m+6;
  if b=30 then m:=m+9;
  if b=40 then m:=m+12;
  if b=50 then m:=m+15;
  if b=60 then m:=m+18;
  if b=70 then m:=m+21;
  if b=80 then m:=m+24;
  if b=90 then m:=m+27;
  if b=100 then m:=m+30;
  if c=0 then m:=m+0;
  if c=10 then m:=m+5;
  if c=20 then m:=m+10;
  if c=30 then m:=m+15;
  if c=40 then m:=m+20;
  if c=50 then m:=m+25;
  if c=60 then m:=m+30;
  if c=70 then m:=m+35;
  if c=80 then m:=m+40;
  if c=90 then m:=m+45;
  if c=100 then m:=m+50;
  writeln(m);
  close(input);
  close(output);
end.

var a:array[0..200,0..200] of longint;
    n,m,i,j,x,y,c,ans:longint;

procedure dfs(p,q,sum,mag:longint);
var k:longint;
begin
 if sum>=ans then exit;
 if (p<1) or (p>m) or (q<1) or (q>m) then exit;
 if (a[p+1,q]=-1) and (a[p,q+1]=-1) and (mag<>-1) then exit;
 if (p=m) and (q=m) then
 begin
  if sum<ans then ans:=sum;
  exit;
 end;
 if (a[p,q]<>-1) then
 begin
 if (a[p,q]=a[p,q+1]) then
 begin
  if (mag=a[p,q+1]) then dfs(p,q+1,sum,-1)
   else dfs(p,q+1,sum+1,-1);
 end
  else if (a[p,q+1]<>-1) then dfs(p,q+1,sum+1,-1)
   else if mag=-1 then dfs(p,q+1,sum+2,a[p,q]);
 if (a[p,q]=a[p+1,q]) then
 begin
  if (mag=a[p+1,q]) then dfs(p+1,q,sum,-1)
   else dfs(p+1,q,sum+1,-1);
 end
  else if a[p+1,q]<>-1 then dfs(p+1,q,sum+1,-1)
   else if mag=-1 then dfs(p+1,q,sum+2,a[p,q]);
 end else if mag<>-1 then
 begin
  if a[p,q+1]<>-1 then dfs(p,q+1,sum,a[p,q]);
  if a[p+1,q]<>-1 then dfs(p+1,q,sum,a[p,q]);
 end;
end;

begin
 assign(input,'chess.in');
 assign(output,'chess.out');
 reset(input);
 rewrite(output);
 for i:=0 to 200 do
  for j:=0 to 200 do a[i,j]:=-1;
 readln(m,n);
  for i:=1 to n do
  begin
   readln(x,y,c);
   a[x,y]:=c;
  end;
  ans:=maxlongint;
  dfs(1,1,0,-1);
 if ans=maxlongint then writeln(-1)
 else writeln(ans);
 close(input);
 close(output);
end.

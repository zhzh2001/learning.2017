var a:array[0..10000] of longint;
    n,m,i,j,t,k,o:longint;
    f:boolean;
    s,s1,s2:string;

procedure qsort(l,r:longint);
var p,q,x,mid:longint;
begin
 p:=l;
 q:=r;
 mid:=a[(l+r) div 2];
  repeat
   while a[p]<mid do inc(p);
   while a[q]>mid do dec(q);
    if p<=q then
    begin
     x:=a[p];a[p]:=a[q];a[q]:=x;
     inc(p);dec(q);
    end;
  until p>q;
 if p<r then qsort(p,r);
 if l<q then qsort(l,q);
end;

begin
 assign(input,'librarian.in');
 assign(output,'librarian.out');
 reset(input);
 rewrite(output);
 readln(n,m);
  for i:=1 to n do readln(a[i]);
  qsort(1,n);
   for i:=1 to m do
   begin
    f:=false;
    readln(t,k);
    str(k,s);
     for j:=1 to n do
     begin
      str(a[j],s1); if length(s1)<t then continue;
      s2:='';
       for o:=length(s1)-t+1 to length(s1) do s2:=s2+s1[o];
      if s2=s then
      begin
       writeln(a[j]);
       f:=true;
       break;
      end;
      if f then break;
     end;
     if not f then writeln(-1);
    end;
  close(input);
 close(output);
end.

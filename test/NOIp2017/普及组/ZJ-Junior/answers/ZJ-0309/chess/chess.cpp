#include <iostream>
#include <cstdio>
#include <queue>
#include <cstring>
using namespace std;
int n,m,a[110][110],ans,p1[4]={0,1,0,-1},p2[4]={-1,0,1,0},b[110][110];
struct kk{
	int xx,yy,jb,bx,by;
	bool zt;
};
bool pd(int x1,int y1,int x2,int y2,int ppp)
{
	for(int i=0;i<4;i++){
		int x=x1+p1[i],y=y1+p2[i];
		if(ppp>b[x][y])continue;
		if(x==x2&&y==y2)continue;
		if(a[x][y]!=-1){
			return true;
		}
	}
}
int main()/////////////////////////////////////////////////////////////////////
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>n>>m;
	cout<<n<<" "<<m<<endl;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			a[i][j]=-1;
			b[i][j]=9999999;
		}
	int x,y,num;
	for(int i=1;i<=m;i++){
		cin>>x>>y>>num;
		a[x][y]=num;
	}
	b[1][1]=0;
	queue<kk> qu;
	kk c;
	c.xx=1;c.yy=1;c.jb=0;c.zt=true;c.bx=0;c.by=0;
	qu.push(c);
	while(!qu.empty()){
		kk p=qu.front();
	//	cout<<p.xx<<" "<<p.yy<<" "<<p.jb<<endl;
		if(!p.zt){
			for(int i=0;i<4;i++){
				int lx=p.xx+p1[i],ly=p.yy+p2[i];
				if(lx==p.bx&&ly==p.by||lx<=0||lx>n||ly<=0||ly>n)continue;
				if(a[lx][ly]!=-1){            
					kk k;
					k.xx=lx; 
					k.yy=ly;
					k.zt=true;
					if(a[lx][ly]!=a[p.bx][p.by])
					p.jb++;
					k.jb=p.jb;
					k.bx=0;
					k.by=0;
					if(k.jb<b[lx][ly]){
						b[lx][ly]=k.jb;		
						qu.push(k);
					}
				}
			}
		}
		else{
			for(int i=0;i<4;i++){
				int lx=p.xx+p1[i],ly=p.yy+p2[i];
				if(lx<=0||lx>n||ly<=0||ly>n)continue;
					if(a[lx][ly]!=-1){
						if(a[p.xx][p.yy]==a[lx][ly]){
							kk k;
							k.xx=lx;
							k.yy=ly;
							k.zt=true;
							k.jb=p.jb;
							k.bx=0;
							k.by=0;
							if(k.jb<b[lx][ly]){
								b[lx][ly]=k.jb;	
								qu.push(k);
							}
						}
						else{
							kk k;
							k.xx=lx;
							k.yy=ly;
							k.zt=true;
							k.jb=p.jb+1;
							k.bx=0;
							k.by=0;
							if(k.jb<b[lx][ly]){
								b[lx][ly]=k.jb;	
								qu.push(k);
							}
						}
					}
					else{
						kk k;
							k.xx=lx;
							k.yy=ly;
							k.zt=false;
							k.jb=p.jb+2;
							k.bx=p.xx;
							k.by=p.yy;
							if(k.jb<b[lx][ly]){
								b[lx][ly]=k.jb;		
								qu.push(k);
							}
					}
			}
		}
		qu.pop();
	}
	if(b[n][n]==9999999)
	cout<<-1<<endl;
	else cout<<b[n][n];
	fclose(stdin);
	fclose(stdout);
}

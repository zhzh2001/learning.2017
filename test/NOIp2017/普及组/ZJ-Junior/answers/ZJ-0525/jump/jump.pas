var
    n,i,j,d,k,t:longint;
    a,l,dp:array[0..500000] of longint;
function max_point(g:longint):longint;
  begin
      for i:=1 to n do dp[i]:=-maxlongint;
      for i:=1 to n do
        for j:=i-1 downto 0 do begin
          if l[i]-l[j]>d+g then break;
          if (dp[j]+a[i]>dp[i]) and (l[i]-l[j]>=d-g) then dp[i]:=dp[j]+a[i];
        end;
      exit(dp[n]);
  end;
function erfen(x,y:longint):longint;
  var
      mid,w:longint;
  begin
      if x=y then exit(x);
      mid:=(x+y) div 2;
      w:=max_point(mid);
      if w>=k then exit(erfen(x,mid))
      else exit(erfen(mid+1,y));
  end;
begin
    assign(input,'jump.in');
    reset(input);
    assign(output,'jump.out');
    rewrite(output);
    readln(n,d,k);
    for i:=1 to n do begin
      readln(l[i],a[i]);
      t:=t+a[i];
    end;
    if t<k then writeln(-1)
    else writeln(erfen(0,l[n]));
    close(input);
    close(output);
end.
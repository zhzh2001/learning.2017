var
    n,q,i,j,t:longint;
    a:array[1..1000] of longint;
    s,k:ansistring;
    b:boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

begin
    assign(input,'librarian.in');
    reset(input);
    assign(output,'librarian.out');
    rewrite(output);
    readln(n,q);
    for i:=1 to n do readln(a[i]);
    sort(1,n);
    for i:=1 to q do begin
      read(t);
      readln(s);
      delete(s,1,1);
      b:=false;
      for j:=1 to n do begin
        str(a[j],k);
        if copy(k,length(k)-t+1,t)=s then begin
          b:=true;
          writeln(a[j]);
          break;
        end;
      end;
      if b=false then writeln(-1);
    end;
    close(input);
    close(output);
end.
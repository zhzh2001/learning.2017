var x,y:array[0..100000]of longint;
    i,j,n,d,k,s,a,w,h:longint;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  readln(n,d,k);
  h:=0;
  for i:=1 to n do
    readln(x[i],y[i]);
  for i:=1 to n do
    if y[i]>0 then inc(h,y[i]);
  if h<k then
  begin
    writeln('-1');
    close(input);
    close(output);
    halt;
  end;
  for i:=d to maxlongint do
  begin
    h:=0;
    j:=-1;
    s:=d+i;
    repeat
      if h>=k then
      begin
        writeln(i);
        close(input);
        close(output);
        halt;
      end;
      inc(j);
      a:=x[j]-x[j-1];
      if (s<a) then break;
      if y[j]<0 then
      begin
        w:=j;
        repeat
          inc(w);
          inc(a,x[w]-x[w-1]);
          if (s<a) then
          begin
            inc(h,y[w-1]);
            j:=w-1;
            break;
          end
          else if y[w]>0 then inc(h,y[w]);
        until false;
      end
      else inc(h,y[j]);
    until j=n;
  end;
end.



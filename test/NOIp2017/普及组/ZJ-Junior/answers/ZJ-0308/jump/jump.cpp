#include<bits/stdc++.h>
using namespace std;
int x[510000],s[510000],n,d,kk;
bool ok(int k,int cur,int tot){
	if(tot>=kk) return true;
	if(cur>n) return false;
	int i=cur,minl=d-k>0?d-k:1,maxl=d+k;
	while(i<=n){
		if(x[i]-x[cur-1]>maxl) break;
		if(x[i]-x[cur-1]<minl){
			i++;
			continue;
		}
		if(ok(k,i+1,tot+s[i])) return true;
		i++;
	}
	return false;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	int tot=0;
	scanf("%d%d%d",&n,&d,&kk);
	x[0]=0;s[0]=0;
	for(int i=1;i<=n;i++){
		int xi,si;
		scanf("%d%d",&xi,&si);
		tot+=si>0?si:0;
		x[i]=xi;
		s[i]=si;
	}
	if(tot<kk){
		printf("-1");
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	int l=0,r=1000000000,mid;
	while(l<r){
		mid=l+(r-l)/2;
		if(ok(mid,1,0)) r=mid;
		else l=mid+1;
	}
	printf("%d",l);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

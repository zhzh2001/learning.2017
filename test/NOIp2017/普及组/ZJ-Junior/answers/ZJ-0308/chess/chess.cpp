#include<bits/stdc++.h>
using namespace std;
const int INF=2000000000;
int m,n,a[110][110]={0};
int dir[4][2]={{0,1},{1,0},{0,-1},{-1,0}};
int h[110][110][2];
bool b[110][110]={false};
int solve(int x,int y,bool las){
	if(x==m&&y==m)
		return 0;
	if(h[x][y][las?1:0]!=-1) return h[x][y][las?1:0];
	int ans=INF;
	for(int i=0;i<4;i++){
		int x1=x+dir[i][0],y1=y+dir[i][1];
		if(x1<1||x1>m||y1<1||y1>m||b[x1][y1]) continue;
		if(las&&a[x1][y1]==0) continue;
		if(a[x][y]==a[x1][y1]){
			int tmp;
			if(las) tmp=a[x][y],a[x][y]=0;
			b[x1][y1]=1;
			ans=min(ans,solve(x1,y1,false));
			b[x1][y1]=0;
			if(las) a[x][y]=tmp;
		}
		else if(a[x1][y1]!=0){
			int tmp;
			if(las) tmp=a[x][y],a[x][y]=0;
			b[x1][y1]=1;
			ans=min(ans,solve(x1,y1,false)+1);
			b[x1][y1]=0;
			if(las) a[x][y]=tmp;
		}
		else if(a[x1][y1]==0&&!las){
			a[x1][y1]=a[x][y];
			b[x1][y1]=1;
			ans=min(ans,solve(x1,y1,true)+2);
			b[x1][y1]=0;
			a[x1][y1]=0;
		}
	}
	h[x][y][las?1:0]=ans;
	return ans;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(h,-1,sizeof(h));
	scanf("%d%d",&m,&n);
	for(int i=0;i<n;i++){
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		if(c==0) a[x][y]=1;
		if(c==1) a[x][y]=2;
	}
	solve(1,1,false);
	if(h[1][1][0]==INF) printf("-1");
	else printf("%d",h[1][1][0]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

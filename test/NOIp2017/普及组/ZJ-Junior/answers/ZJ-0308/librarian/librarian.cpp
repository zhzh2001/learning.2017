#include<bits/stdc++.h>
using namespace std;
int a[1100];
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,q;
	scanf("%d%d",&n,&q);
	for(int i=0;i<n;i++) scanf("%d",&a[i]);
	sort(a,a+n);
	for(int i=0;i<q;i++){
		int len,num,ans=-1;
		scanf("%d%d",&len,&num);
		for(int j=0;j<n;j++){
			if(num>a[j]) continue;
			if(num==a[j]){
				ans=a[j];
				break;
			}
			int t1=a[j],t2=num;
			bool b=true;
			for(int k=0;k<len;k++){
				if(t1%10!=t2%10){
					b=false;
					break;
				}
				t1/=10;
				t2/=10;
			}
			if(b){
				ans=a[j];
				break;
			}
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

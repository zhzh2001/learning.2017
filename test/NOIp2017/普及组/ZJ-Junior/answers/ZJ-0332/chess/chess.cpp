#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int n,m,ans=-1;
int a[105][105];
bool used[105][105];
void dfs(int x,int y,int xx,bool yy)
{
	if(x<=0) return;
	if(y<=0) return;
	if(x>n) return;
	if(y>n) return;
	if(used[x][y]==1) return;
	if(x==n&&y==n)
	{
		if(ans==-1) ans=xx;
		else if(ans>xx) ans=xx;
		return;
	}
	if(a[x][y-1]==-1)
	{
		if(yy==1)
		{
			a[x][y-1]=a[x][y];
			used[x][y]=1;
			dfs(x,y-1,xx+2,0);
			used[x][y]=0;
			a[x][y-1]=-1;
		}
	}
	if(a[x][y+1]==-1)
	{
		if(yy==1)
		{
			a[x][y+1]=a[x][y];
			used[x][y]=1;
			dfs(x,y+1,xx+2,0);
			used[x][y]=0;
			a[x][y+1]=-1;
		}
	}
	if(a[x-1][y]==-1)
	{
		if(yy==1)
		{
			a[x-1][y]=a[x][y];
			used[x][y]=1;
			dfs(x-1,y,xx+2,0);
			used[x][y]=0;
			a[x-1][y]=-1;
		}
	}
	if(a[x+1][y]==-1)
	{
		if(yy==1)
		{
			a[x+1][y]=a[x][y];
			used[x][y]=1;
			dfs(x+1,y,xx+2,0);
			used[x][y]=0;
			a[x+1][y]=-1;
		}
	}
	if(a[x+1][y]==a[x][y]&&a[x+1][y]!=-1)
	{
		used[x][y]=1;
		dfs(x+1,y,xx,1);
		used[x][y]=0;
	}
	if(a[x-1][y]==a[x][y]&&a[x-1][y]!=-1)
	{
		used[x][y]=1;
		dfs(x-1,y,xx,1);
		used[x][y]=0;
	}
	if(a[x][y-1]==a[x][y]&&a[x][y-1]!=-1)
	{
		used[x][y]=1;
		dfs(x,y-1,xx,1);
		used[x][y]=0;
	}
	if(a[x][y+1]==a[x][y]&&a[x][y+1]!=-1)
	{
		used[x][y]=1;
		dfs(x,y+1,xx,1);
		used[x][y]=0;
	}
	if(a[x+1][y]!=a[x][y]&&a[x+1][y]!=-1)
	{
		used[x][y]=1;
		dfs(x+1,y,xx+1,1);
		used[x][y]=0;
	}
	if(a[x-1][y]!=a[x][y]&&a[x-1][y]!=-1)
	{
		used[x][y]=1;
		dfs(x-1,y,xx+1,1);
		used[x][y]=0;
	}
	if(a[x][y-1]!=a[x][y]&&a[x][y-1]!=-1)
	{
		used[x][y]=1;
		dfs(x,y-1,xx+1,1);
		used[x][y]=0;
	}
	if(a[x][y+1]!=a[x][y]&&a[x][y+1]!=-1)
	{
		used[x][y]=1;
		dfs(x,y+1,xx+1,1);
		used[x][y]=0;
	}
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,-1,sizeof(a));
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int q,w,e;
		scanf("%d%d%d",&q,&w,&e);
		a[q][w]=e;
	}
	dfs(1,1,0,1);
	printf("%d",ans);
	return 0;
}

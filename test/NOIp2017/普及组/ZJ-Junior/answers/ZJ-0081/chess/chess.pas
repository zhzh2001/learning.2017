var m,n,x,y,c,i,j,ans,cost,p,k:longint;
           map:array[1..100,1..100]of integer;
           vis:array[1..100,1..100]of boolean;
            dx:array[1..4]of integer=(0,1,0,-1);
            dy:array[1..4]of integer=(1,0,-1,0);
            flag:boolean;
procedure dfs(x1,y1:longint);
begin
 if (x1=m)and(y1=m) then begin if cost<ans then begin ans:=cost;exit;end;end;
 if (map[x1,y1]=2) and flag then begin map[x1,y1]:=p;flag:=false;cost:=cost+2;end
  else if(map[x1,y1]=2)and(flag=false)then exit
   else if map[x1,y1]<>p then inc(cost);
  vis[x1,y1]:=false;p:=map[x1,y1];
   for k:=1to 4 do
    if (x1+dx[k]<=m)and(y1+dy[k]<=m)and(x1+dx[k]>0)and(y1+dy[k]>0)
    and(vis[x1+dx[k],y1+dy[k]]=true)then
     dfs(x1+dx[k],y1+dy[k]);
     if flag=false then map[x1,y1]:=2;
     vis[x1,y1]:=true;
end;

begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
 read(m,n);
  for i:=1to m do
   for j:=1to m do
    map[i,j]:=2;
    flag:=true;ans:=-1;
   for i:=1to n do
    begin
     read(x,y,c);
     map[x,y]:=c;
    end;
   dfs(1,1);
   writeln(ans);
   close(input);
   close(output);
  end.
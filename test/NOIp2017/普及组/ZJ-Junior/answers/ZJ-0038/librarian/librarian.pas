  var n,m,i,j,len,ans,sum,x:longint;
      a:array[1..10000]of longint;
  procedure pin;
  begin
        assign(input,'librarian.in');reset(input);
        assign(output,'librarian.out');rewrite(output);
  end;


  procedure pout;
  begin
        close(input);close(output);
  end;


  procedure sort(l,r:longint);
  var i,j,x,y:longint;
  begin
        i:=l;
        j:=r;
        x:=a[(l+r) div 2];
        repeat
                while a[i]>x do inc(i);
                while x>a[j] do dec(j);
                if not(i>j) then
                begin
                        y:=a[i];a[i]:=a[j];a[j]:=y;
                        inc(i);
                        j:=j-1;
                end;
        until i>j;
        if l<j then sort(l,j);
        if i<r then sort(i,r);
  end;


  procedure init;
  begin
        readln(n,m);
        for i:=1 to n do
                readln(a[i]);
        sort(1,n);
        for i:=1 to m do
        begin
                readln(len,x);
                ans:=1;
                for j:=1 to len+1 do
                        ans:=ans*10;
                sum:=-1;
                for j:=1 to n do
                        if a[j]<x then break
                        else if a[j] mod ans=x then sum:=a[j];
                writeln(sum);
        end;
  end;


  begin
        pin;
        init;
        pout;
  end.
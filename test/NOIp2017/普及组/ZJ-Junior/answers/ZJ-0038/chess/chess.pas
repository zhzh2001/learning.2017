  var n,m,i,x,y,z,j:longint;
      f,a,b1,b2:array[0..101,0..101]of longint;
      tt:boolean;
  procedure pin;
  begin
        assign(input,'chess.in');reset(input);
        assign(output,'chess.out');rewrite(output);
  end;


  procedure pout;
  begin
        close(input);close(output);
  end;


  function min(a,b:longint):longint;
  begin
        if a<b then exit(a)
        else exit(b);
  end;


  function suan1(x,y:longint):longint;
  begin
        if a[x,y]=0 then
                if a[x-1,y]=0 then exit(maxlongint)
                else exit(f[x-1,y]+2)
        else
                if (b1[x-1,y]=a[x,y])or(b1[x-1,y]=0) then exit(f[x-1,y])
                else if b1[x-1,y]<>3 then exit(f[x-1,y]+1)
                else exit(maxlongint);
  end;


  function suan2(x,y:longint):longint;
  begin
        if a[x,y]=0 then
                if a[x,y-1]=0 then exit(maxlongint)
                else exit(f[x,y-1]+2)
        else
                if (b1[x,y-1]=a[x,y])or(b1[x,y-1]=0) then exit(f[x,y-1])
                else if b1[x,y-1]<>3 then exit(f[x,y-1]+1)
                else exit(maxlongint);
  end;


  function suan3(x,y:longint):longint;
  begin
        if a[x,y]=0 then
                if a[x+1,y]=0 then exit(maxlongint)
                else exit(f[x+1,y]+2)
        else
                if (b2[x+1,y]=a[x,y])or(b2[x+1,y]=0) then exit(f[x+1,y])
                else if b2[x+1,y]<>3 then exit(f[x+1,y]+1)
                else exit(maxlongint);
  end;


  function suan4(x,y:longint):longint;
  begin
        if a[x,y]=0 then
                if a[x,y+1]=0 then exit(maxlongint)
                else exit(f[x,y+1]+2)
        else
                if (b2[x,y+1]=a[x,y])or(b2[x,y+1]=0) then exit(f[x,y+1])
                else if b2[x,y+1]<>3 then exit(f[x,y+1]+1)
                else exit(maxlongint);
  end;


  procedure init;
  begin
        readln(n,m);
        for i:=1 to m do
        begin
                readln(x,y,z);
                if z=0 then a[x,y]:=1
                else a[x,y]:=2;
        end;
        tt:=true;
        fillchar(f,sizeof(f),$7f);
        f[1,1]:=0;
        while tt do
        begin
                tt:=false;
                fillchar(b1,sizeof(b1),0);
                fillchar(b2,sizeof(b2),0);
                for i:=1 to n do
                        for j:=1 to n do
                        begin
                                if min(suan1(i,j),suan2(i,j))<f[i,j] then
                                begin
                                        tt:=true;
                                        f[i,j]:=min(suan1(i,j),suan2(i,j));
                                end;
                                if a[i,j]=0 then
                                begin
                                        if suan1(i,j)<suan2(i,j) then
                                                b1[i,j]:=b1[i-1,j]
                                        else if suan1(i,j)>suan2(i,j) then
                                                b1[i,j]:=b1[i,j-1]
                                        else if (a[i-1,j]<>0)or(a[i,j-1]<>0) then
                                                b1[i,j]:=b1[i,j-1]
                                        else b1[i,j]:=3;
                                end
                                else b1[i,j]:=a[i,j];
                        end;
                for i:=n downto 1 do
                        for j:=n downto 1 do
                        begin
                                if min(suan3(i,j),suan4(i,j))<f[i,j] then
                                begin
                                        tt:=true;
                                        f[i,j]:=min(suan3(i,j),suan4(i,j));
                                end;
                                if a[i,j]=0 then
                                begin
                                        if suan3(i,j)<suan4(i,j) then
                                                b2[i,j]:=b2[i+1,j]
                                        else if suan3(i,j)>suan4(i,j) then
                                                b2[i,j]:=b2[i,j+1]
                                        else if (a[i+1,j]<>0)or(a[i,j+1]<>0) then
                                                b2[i,j]:=b2[i,j+1]
                                        else b2[i,j]:=3;
                                end
                                else b2[i,j]:=a[i,j];
                        end;
        end;
        if f[n,n]>100000 then writeln(-1)
        else writeln(f[n,n]);
  end;


  begin
        pin;
        init;
        pout;
  end.
const fx:array[1..4]of longint=(1,-1,0,0);
      fy:array[1..4]of longint=(0,0,-1,1);
var n,m,i,j,x,y,z:longint;
    color:array[1..100,1..100]of longint;
    f:array[1..100,1..100]of longint;
procedure dfs(x,y,k:longint);
var i,xx,yy:longint;
begin
 for i:=1 to 4 do
 begin
  xx:=x+fx[i];
  yy:=y+fy[i];
  if(xx<1)or(xx>n)or(yy<1)or(yy>n) then continue;
  if k=0 then begin
   if color[xx,yy]=0 then begin
    if f[x,y]+2<=f[xx,yy] then begin
          f[xx,yy]:=f[x,y]+2;
          dfs(xx,yy,color[x,y]);
     end;
   end
   else begin
    if (color[xx,yy]=color[x,y]) then begin if f[x,y]<f[xx,yy]
       then begin f[xx,yy]:=f[x,y];dfs(xx,yy,0);end;end
    else if f[x,y]+1<f[xx,yy] then begin f[xx,yy]:=f[x,y]+1;dfs(xx,yy,0);end;
   end;
  end
  else begin
   if color[xx,yy]=0 then continue;
   if(k=color[xx,yy]) then begin if f[x,y]<f[xx,yy]
     then begin f[xx,yy]:=f[x,y];dfs(xx,yy,0);end;end
   else if f[x,y]+1<f[xx,yy] then begin f[xx,yy]:=f[x,y]+1;dfs(xx,yy,0);end;
  end;
 end;
end;
begin
 assign(input,'chess.in');reset(input);
 assign(output,'chess.out');rewrite(output);
 readln(n,m);
 for i:=1 to n do
  for j:=1 to n do
  f[i,j]:=maxlongint div 2;
 for i:=1 to m do begin read(x,y,z);color[x,y]:=z+1;end;
 f[1,1]:=0;
 dfs(1,1,0);
 if f[n,n]>=maxlongint div 2 then writeln(-1) else writeln(f[n,n]);
 close(input);close(output);
end.

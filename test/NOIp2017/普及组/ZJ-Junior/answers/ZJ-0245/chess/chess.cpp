#include<cstdio>
#include<cstring>
#define maxn 1005
using namespace std;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9')ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
struct FH{
	int x,y,z;
}que[maxn];
int m,n,a[105][105],f[105][105],flg[4][2]={{0,1},{0,-1},{1,0},{-1,0}},hed,til;
bool vis;
bool check(int x,int y){
	if(x<1||x>m||y<1||y>m)return 0;
	return 1;
}
void BFS(){
	que[1].x=1;que[1].y=1;f[1][1]=0;
	hed=0,til=1;
	while(hed!=til){
		++hed%=maxn;if(que[hed].x==m&&que[hed].y==m)vis=1;
		else for(int i=0;i<4;i++){
			int x=que[hed].x+flg[i][0],y=que[hed].y+flg[i][1];
			if(check(x,y)){
				if(a[que[hed].x][que[hed].y]>=0){
					if(a[x][y]>=0){
						if(a[x][y]==a[que[hed].x][que[hed].y]){
							if(f[x][y]>f[que[hed].x][que[hed].y]){
								f[x][y]=f[que[hed].x][que[hed].y];
								++til%=maxn;
								que[til].x=x;que[til].y=y;que[til].z=-1;
							}
						}
						else {
							if(f[x][y]>f[que[hed].x][que[hed].y]+1){
								f[x][y]=f[que[hed].x][que[hed].y]+1;
								++til%=maxn;
								que[til].x=x;que[til].y=y;que[til].z=-1;
							}							
						}
					}
					else {
						if(f[x][y]>=f[que[hed].x][que[hed].y]+2){
							f[x][y]=f[que[hed].x][que[hed].y]+2;
							++til%=maxn;
							que[til].x=x;que[til].y=y;que[til].z=a[que[hed].x][que[hed].y];
						}
					}
				}                                                                                                              
				else 
					if(a[x][y]>=0){
						if(a[x][y]==que[hed].z){
							if(f[x][y]>f[que[hed].x][que[hed].y]){
								f[x][y]=f[que[hed].x][que[hed].y];
								++til%=maxn;
								que[til].x=x;que[til].y=y;que[til].z=-1;
							}							
						}
						else {
							if(f[x][y]>f[que[hed].x][que[hed].y]+1){
								f[x][y]=f[que[hed].x][que[hed].y]+1;
								++til%=maxn;
								que[til].x=x;que[til].y=y;que[til].z=-1;
							}
						}
					}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read();
	n=read();
	memset(a,-1,sizeof a);
	memset(f,63,sizeof f);
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		a[x][y]=read();
	}
	BFS();
	if(vis)printf("%d",f[m][m]);
	else printf("-1");
	return 0;
}

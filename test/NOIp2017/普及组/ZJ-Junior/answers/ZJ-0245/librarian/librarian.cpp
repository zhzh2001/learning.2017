#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9')ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int n,q,a[1005],t[15]={0,10,100,1000,10000,100000,1000000,10000000,100000000},ans;
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),q=read();
	for(int i=1;i<=n;i++)a[i]=read();sort(a+1,a+1+n);
	for(int i=1;i<=q;i++){
		int tt=read(),k=read();
		tt=t[tt];
		for(int j=1;j<=n;j++){
			if(a[j]<k&&j<n)continue;
			else if(a[j]%tt==k){printf("%d\n",a[j]);break;}
			if(j>=n){printf("-1\n");}
		}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#define maxn 505;
using namespace std;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9')ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int n,d,k,tot=-1,f[505];
bool vis[505];
struct FH{
	int x,s,id;
}a[505];
inline bool check(int mid){
	memset(f,0,sizeof f);
	memset(vis,0,sizeof vis);
	vis[0]=1;
	for(int i=0;i<n;i++)
	if(vis[i])for(int j=i+1;j<=n;j++){
		if(a[j].x-a[i].x>=d-mid&&a[j].x-a[i].x<=d+mid){
		if(!vis[j])f[j]=f[i]+a[j].s,vis[j]=1;
		else f[j]=max(f[j],f[i]+a[j].s);
	}
	}
	for(int i=0;i<=n;i++)if(f[i]>=k){tot=mid;return 1;}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();k=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].s=read();
	int L=0,R=a[n].x;
	while(L<=R){
		int mid=L+(R-L>>1);
		if(check(mid))R=mid-1;else L=mid+1;
	}
	printf("%d",tot);
	return 0;
}

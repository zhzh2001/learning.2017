var
    n,m,i,j:Longint;
    ans,s,ss:string;
    book:array[0..2000] of string;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
  begin
    readln(s);
    ss:=s;
    for j:=1 to length(ss) do
    s[length(ss)-j+1]:=ss[j];
    book[i]:=s;
  end;
  for i:=1 to m do
  begin
    ans:='';
    readln(s);delete(s,1,pos(' ',s));
    ss:=s;
    for j:=1 to length(ss) do
    s[length(ss)-j+1]:=ss[j];
    for j:=1 to n do
    if (pos(s,book[j])=1) then
    begin
      if ans='' then ans:=book[j];
      if book[j]<ans then ans:=book[j];
    end;
    if ans='' then
    writeln(-1) else
    begin
      ss:=ans;
      for j:=1 to length(ss) do
      ans[length(ss)-j+1]:=ss[j];
      writeln(ans);
    end;
  end;
  close(input);close(output);
end.
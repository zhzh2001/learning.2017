var n,d,k,m,l,r,ss,mid:int64;
    i:longint;
    len,a,s,ww:array[0..5000000] of int64;
function check(g:int64):boolean;
var max,min,ans:int64;f:boolean;i,t,w:longint;
begin
  max:=d+g;min:=d-g;if min<1 then min:=1;
  check:=false;ans:=0;
  fillchar(a,sizeof(a),0);
  fillchar(s,sizeof(s),0);
  t:=0;w:=1;a[w]:=0;s[w]:=0;
  while t<w do
  begin
    inc(t);
    if a[t]=n then continue;
    if ww[a[t]+1]<0 then
    begin
      f:=false;
      for i:=a[t]+1 to n do
      if (len[i]-len[a[t]]>=min) and (len[i]-len[a[t]]<=max) and (ww[i]>=0) then
      begin
        f:=true;
        break;
      end else
      if (len[i]-len[a[t]]>max) then break;
      if f then
      begin
        inc(w);
        a[w]:=i;
        s[w]:=s[t]+ww[i];
        if s[w]>ans then ans:=s[w];
      end else
      if (len[a[t]+1]-len[a[t]]>=min)and(len[a[t]+1]-len[a[t]]<=max) then
      begin
        inc(w);
        a[w]:=a[t]+1;
        s[w]:=s[t]+ww[a[t]+1];
      if s[w]>ans then ans:=s[w];
      end
    end else
    if (len[a[t]+1]-len[a[t]]>=min)and(len[a[t]+1]-len[a[t]]<=max) then
    begin
      inc(w);
      a[w]:=a[t]+1;
      s[w]:=s[t]+ww[a[t]+1];
      if s[w]>ans then ans:=s[w];
    end;
  end;
  if ans>=k then check:=true;
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(len[i],ww[i]);
    if ww[i]>0 then ss:=ss+ww[i];
    if len[i]>m then m:=len[i];
  end;
  if ss<k then writeln(-1) else
  begin
    l:=0;r:=m;
    while l<r do
    begin
      mid:=(l+r) div 2;
      if check(mid) then r:=mid else l:=mid+1;
    end;
    writeln(l);
  end;
  close(input);close(output);
end.

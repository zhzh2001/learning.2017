const g:array[1..4] of -1..1=(-1,0,1,0);
      o:array[1..4] of -1..1=(0,1,0,-1);
var
    n,m,i,t,w,q,p,ans,x,y,z:longint;
    a,b,c,s:array[0..10000000] of longint;
    pd:array[0..105,0..105,0..1] of longint;
    f:array[0..105,0..105] of longint;
    k:array[0..10000000] of boolean;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  fillchar(f,sizeof(f),255);
  fillchar(pd,sizeof(pd),255);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(x,y,z);
    f[x,y]:=z;
  end;
  t:=0;w:=1;a[1]:=1;b[1]:=1;c[1]:=f[1,1];s[1]:=0;k[1]:=false;ans:=maxlongint;
  while t<w do
  begin
    inc(t);
    if (a[t]=n) and (b[t]=n) then continue;
    for i:=1 to 4 do
    begin
      q:=a[t]+g[i];p:=b[t]+o[i];
      if (p<=n) and (p>0) and (q<=n) and (q>0) then
      begin
        if (f[q,p]=0) or (f[q,p]=1) then
        if (pd[q,p,f[q,p]]=-1) or (pd[q,p,f[q,p]]>s[t]+(f[q,p] xor c[t]))then
        begin
          inc(w);
          a[w]:=q;b[w]:=p;c[w]:=f[q,p];s[w]:=s[t]+(f[q,p] xor c[t]);k[w]:=false;
          pd[q,p,f[q,p]]:=s[w];
        end;
        if f[q,p]=-1 then
        begin
        if ((pd[q,p,0]=-1)or (pd[q,p,0]>s[t]+2+(0 xor c[t]))) and (k[t]=false) then
        begin
          inc(w);
          a[w]:=q;b[w]:=p;c[w]:=0;s[w]:=s[t]+2+(0 xor c[t]);k[w]:=true;
          pd[q,p,0]:=s[w];
        end;
        if ((pd[q,p,1]=-1) or (pd[q,p,1]>s[t]+2+(1 xor c[t])))and (k[t]=false) then
        begin
          inc(w);
          a[w]:=q;b[w]:=p;c[w]:=1;s[w]:=s[t]+2+(1 xor c[t]);k[w]:=true;
          pd[q,p,1]:=s[w];
        end;
        end;
      end;
    end;
  end;
  if pd[n,n,0]<>-1 then if pd[n,n,0]<ans then ans:=pd[n,n,0];
  if pd[n,n,1]<>-1 then if pd[n,n,1]<ans then ans:=pd[n,n,1];
  if ans=maxlongint then writeln(-1) else writeln(ans);
  close(input);close(output);
end.

#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;
int n,d,k;
struct sq
{
	int dis,s;
};
int sum=0,g=0x7fffff;
int g1=0,s1=0;
sq a[500010]={0};
void dfs(int u)
{
	if(u==n&&s1<k)
	{
		return;
	} 
	if(s1>=k)
	{
		if(g1<g) g=g1;
		return;
	}
	if(g1>=g) return;
	for(int i=u+1;i<=n;i++)
	{
		int g2=g1;
		int ds=a[i].dis-a[u].dis;
		if(ds<d-g1)
		{
			g1+=d-g1-ds;
			s1+=a[i].s;
			dfs(i);
			s1-=a[i].s;
			g1-=d-g2-ds;
			continue;
		}
		if(ds>d+g1)
		{
			g1+=ds-d-g1;
			s1+=a[i].s;
			dfs(i);
			s1-=a[i].s;
			g1-=ds-d-g2;
			continue;
		}
		s1+=a[i].s;
		dfs(i);
		s1-=a[i].s;
	}
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d %d %d",&n,&d,&k);
	for(int i=1;i<=n;i++)
	{
		scanf("%d %d",&a[i].dis,&a[i].s);
		if(a[i].s>=0)
			sum+=a[i].s;
	}
	if(sum<k)
	{
		printf("-1");
		return 0;
	}
	dfs(0);
	cout << g;
	return 0;
}

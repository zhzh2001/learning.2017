#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
struct pos
{
	int x,y;
};
int m,n;
int a[110][110];
int ans[110][110];
bool v[110][110]={0};
bool col[110][110]={0};
pos q[200000]={0};
int s=1,e=1;
int turn[4][2]={{1,0},{-1,0},{0,1},{0,-1}};
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d %d",&m,&n);
	memset(a,-1,sizeof(a));
	memset(ans,-1,sizeof(ans));
	for(int i=1;i<=n;i++)
	{
		int x,y,c;
		scanf("%d %d %d",&x,&y,&c);
		a[x][y]=c;
	}
	q[1].x=1;
	q[1].y=1;
	ans[1][1]=0;
	while(s<=e)
	{
		pos nx;
		for(int i=0;i<4;i++)
		{
			nx.x=q[s].x+turn[i][0];
			nx.y=q[s].y+turn[i][1];
			if((nx.x>=1&&nx.x<=m)&&(nx.y>=1&&nx.y<=m)&&v[nx.x][nx.y]==0&&a[q[s].x][q[s].y]!=-1)
			{
				bool f=0;
				if(a[nx.x][nx.y]==a[q[s].x][q[s].y]&&col[nx.x][nx.y]==0
					&&(ans[q[s].x][q[s].y]<ans[nx.x][nx.y]||ans[nx.x][nx.y]==-1))
					ans[nx.x][nx.y]=ans[q[s].x][q[s].y],f=1;
					
				if(a[nx.x][nx.y]!=a[q[s].x][q[s].y]&&a[nx.x][nx.y]!=-1&&col[nx.x][nx.y]==0
					&&(ans[q[s].x][q[s].y]+1<ans[nx.x][nx.y]||ans[nx.x][nx.y]==-1))
						ans[nx.x][nx.y]=ans[q[s].x][q[s].y]+1,f=1;
						
				if((a[nx.x][nx.y]==-1||col[nx.x][nx.y]==1)&&col[q[s].x][q[s].y]==0
					&&(ans[q[s].x][q[s].y]+2<ans[nx.x][nx.y]||ans[nx.x][nx.y]==-1)) 
				{
					ans[nx.x][nx.y]=ans[q[s].x][q[s].y]+2;
					a[nx.x][nx.y]=a[q[s].x][q[s].y];
					col[nx.x][nx.y]=1;
					f=1;
				}
				if(f==1)
				{
					e=e+1;
					q[e].x=nx.x;
					q[e].y=nx.y;
				}		
			}
		}
		v[q[s].x][q[s].y]=1;
		if(col[q[s].x][q[s].y]==1)
		{
			col[q[s].x][q[s].y]=0;
			a[q[s].x][q[s].y]=-1;
		}
		s=s+1;
	}
	printf("%d",ans[m][m]);
	return 0;
}

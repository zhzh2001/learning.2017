#include<set>
#include<map>
#include<cstdio>
#include<queue>
#include<vector>
#include<stack>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
int n,d,k,x[500005],s[500005];
long long dp[500005],sum=0;
long long check(int g){
	long long ans=-1e18;
	for(int i=1;i<=n;i++){
		long long l=x[i]-d-g,r=x[i]+d-g,mx=-1e18;
		if(l<1)l=1;
		if(r<1)r=x[i];
		for(int j=1;j<=i;j++){
			if(x[j]>=l&&x[j]<=r){if(mx<s[i]+dp[j])mx=s[i]+dp[j];}
		}
		dp[i]=mx;
		if(ans<mx)ans=mx;
		cout<<mx<<endl;
		if(mx==-1e18)return ans;
	}
	return ans;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++){
		scanf("%d%d",&x[i],&s[i]);
		if(s[i]>=0)sum+=s[i];
	}
	if(sum<k){puts("-1");return 0;}
	int l=1,r=x[n],mid;
	while(l<r){
		mid=(l+r)>>1;
		if(check(mid)>=k)r=mid;
		else l=mid+1;
	}
	printf("%d\n",l);
	return 0;
}

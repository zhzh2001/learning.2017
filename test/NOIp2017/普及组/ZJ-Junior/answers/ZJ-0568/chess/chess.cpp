#include<set>
#include<map>
#include<cstdio>
#include<queue>
#include<vector>
#include<stack>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
const int h[]={0,0,1,-1},l[]={1,-1,0,0};
struct node {
	int x,y,c,cost;
	bool operator <(const node _) const{
		return cost>_.cost;
	}
};
priority_queue<node> q;
int n,m,a[105][105],Cost[105][105];
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	memset(a,-1,sizeof(a));
	memset(Cost,0x3f,sizeof(Cost));
	for(int i=1;i<=n;i++){
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		a[x][y]=c;
	}
	Cost[1][1]=0;
	q.push((node){1,1,a[1][1],0});
	while(!q.empty()){
		node p=q.top();q.pop();
		for(int i=0;i<4;i++){
			int x=h[i]+p.x,y=l[i]+p.y,cost=0x3f3f3f3f;
			if(x<=m && x>=1 && y<=m && y>=1){
				if(a[x][y]==-1&&a[p.x][p.y]!=-1)cost=p.cost+2;
				else if(p.c==a[x][y])cost=p.cost;
				else if(p.c!=a[x][y])cost=p.cost+1;
				if(x==m&&y==m){printf("%d\n",cost);return 0;}
				if(cost<Cost[x][y]){
					if(a[x][y]!=-1){q.push((node){x,y,a[x][y],cost}),Cost[x][y]=cost;}
					else if(a[x][y]==-1&&a[p.x][p.y]!=-1){q.push((node){x,y,p.c,cost}),Cost[x][y]=cost;}
				}
			}
		}
	}
	puts("-1");
	return 0;
}

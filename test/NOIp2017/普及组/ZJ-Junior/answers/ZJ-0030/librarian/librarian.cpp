#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <cstdlib>
#define File(a) freopen(a".in","r",stdin),freopen(a".out","w",stdout)
#define MAXN 1005
using namespace std;
int a[MAXN];
int power(int a,int b){
int ans=1;
	while(b){
		if(b&1)
			ans*=a;
		a*=a;
		b>>=1;
	}
	return ans;
}
int main(){
int n,q;
	File("librarian");
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	for(int i=1;i<=q;i++){
		int k,b;
		bool flag=false;
		scanf("%d%d",&k,&b);
		k=power(10,k);
		for(int i=1;i<=n;i++)
			if(a[i]%k==b){
				printf("%d\n",a[i]);
				flag=true;
				break;
			}
		if(!flag)
			puts("-1");
	}
	return 0;
}

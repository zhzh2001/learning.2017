#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <cstdlib>
#define File(a) freopen(a".in","r",stdin),freopen(a".out","w",stdout)
#define MAXN 105
#define mset(a,b) memset(a,b,sizeof(a))
#define min3(a,b,c) min((a),min((b),(c)))
#define tomin(a,b) a=min(a,(b))
using namespace std;
typedef long long ll;
ll dp[MAXN][MAXN][3][2];
int mp[MAXN][MAXN];
int main(){
int m,n;
	File("chess");
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++){
		int x,y,col;
		scanf("%d%d%d",&x,&y,&col);
		mp[x][y]=col+1;
	}
	mset(dp,127);
	dp[1][1][mp[1][1]][0]=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(mp[i][j])
				for(int k=0;k<=2;k++)
					if(mp[i][j]==k)
						tomin(dp[i][j][mp[i][j]][0],min(min(dp[i-1][j][k][1],dp[i-1][j][k][0]),min(dp[i][j-1][k][1],dp[i][j-1][k][0])));
					else
						tomin(dp[i][j][mp[i][j]][0],min(min(dp[i-1][j][k][1],dp[i-1][j][k][0]),min(dp[i][j-1][k][1],dp[i][j-1][k][0]))+1);
			else
				for(int k=0;k<=2;k++)
					for(int z=0;z<=2;z++)
						if(z==k)
							tomin(dp[i][j][z][1],min(dp[i-1][j][k][0],dp[i][j-1][k][0])+2);
						else
							tomin(dp[i][j][z][1],min(dp[i-1][j][k][0],dp[i][j-1][k][0])+3);
	ll ans=6148914691236517205ll;
	for(int i=0;i<=2;i++)
		tomin(ans,min(dp[m][m][i][1],dp[m][m][i][0]));
	printf("%lld\n",ans==6148914691236517205ll?-1:ans);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <string>
#include <cstdlib>
#define File(a) freopen(a".in","r",stdin),freopen(a".out","w",stdout)
#define MAXN 500005
#define mset(a,b) memset(a,b,sizeof(a))
using namespace std;
struct Node{
	int x,score;
};
Node a[MAXN];
int n,k,d;
bool check(int g){
int dp[MAXN],maxn=-2100000;
	mset(dp,-125);
	dp[0]=0;
	for(int i=0;i<n;i++){
		int j=1;
		while(a[j].x-a[i].x<max(d-g,1))
			j++;
		for(;a[j].x-a[i].x<=d+g&&j<=n;j++)
			dp[j]=max(dp[i]+a[j].score,dp[j]);
	}
	for(int i=0;i<=n;i++)
		maxn=max(dp[i],maxn);
	return maxn>=k;
}
int main(){
	File("jump");
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)
		scanf("%d%d",&a[i].x,&a[i].score);
	a[0].x=a[0].score=0;
	int L=0,R=a[n].x-d,ans=-1;
	while(L<=R){
		int mid=(L+R)>>1;
		if(check(mid)){
			ans=mid;
			R=mid-1;
		}
		else
			L=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

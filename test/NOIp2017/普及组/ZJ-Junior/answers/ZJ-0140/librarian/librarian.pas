program librarian(input,output);
var
 a,b,i,temp,all,po:longint;
 n,q,ans,l:array[1..1001] of longint;
 s,sn,sq:string;
function search(sa,sb:string):boolean;
var
 x,h:longint;
 sk:string;
begin
if length(sb)>length(sa) then exit(false);
if length(sa)=length(sb) then
 begin
  if sa=sb then exit(true)
   else exit(false);
 end;
h:=length(sa)-length(sb)+1;
sk:='';
while h<=length(sa) do
 begin
  sk:=sk+sa[h];
  h:=h+1;
 end;
if sk=sb then exit(true) else exit(false);
end;
procedure qsort(l,r:longint);
var
 i,j,mid,p:longint;
begin
i:=l; j:=r;
mid:=n[(l+r) div 2];
repeat
 while n[i]<mid do inc(i);
 while n[j]>mid do dec(j);
 if i<=j then
  begin
   p:=n[i]; n[i]:=n[j]; n[j]:=p;
   inc(i); dec(j);
  end;
until i>j;
if j>l then qsort(l,j);
if i<r then qsort(i,r);
end;
begin
assign(input,'librarian.in');
reset(input);
assign(output,'librarian.out');
rewrite(output);
readln(a,b);
for i:=1 to a do
 readln(n[i]);
for i:=1 to b do
 readln(l[i],q[i]);
qsort(1,a);
str(n[a],s);
for i:=1 to b do
 begin
  if l[i]>length(s) then
   begin
    ans[i]:=-1;
    break;
   end;
  str(q[i],sq);
  temp:=1; all:=0;
  while true do
   begin
    str(n[temp],sn);
    if search(sn,sq) then
     begin
      ans[i]:=n[temp];
      break;
     end
    else
     begin
      all:=all+1;
      temp:=temp+1;
     end;
    if all=a then
     begin
      ans[i]:=-1;
      break;
     end;
   end;
 end;
for i:=1 to b do
 writeln(ans[i]);
close(input);
close(output);
end.
program jump(input,output);
var
 a:array[1..1000000] of longint;
 n,d,k,big,i,hh,ss,xx:longint;
begin
assign(input,'jump.in');
reset(input);
assign(output,'jump.out');
rewrite(output);
big:=0;
fillchar(a,sizeof(a),0);
readln(n,d,k);
for i:=1 to n do
 begin
  readln(hh,ss);
  a[hh]:=a[ss];
  if ss>0 then big:=big+ss;
 end;
if big<k then
 begin
  writeln('-1');
  halt;
 end
else
 begin
  xx:=hh-d;
  writeln(xx);
  halt;
 end;
close(input);
close(output);
end.

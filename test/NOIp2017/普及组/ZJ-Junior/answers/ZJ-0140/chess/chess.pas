program chess(input,output);
var
 a:array[0..101,0..101] of longint;
 n,x,y,c,i,ans,m:longint;
procedure search(xx,yy:longint);
var
 k,i:longint;
begin
if (xx=m) and (yy=m) then
 begin
  writeln(ans);
  halt;
 end;
for i:=1 to n do
 begin
  if a[xx+1,yy]>0 then
   begin
    if a[xx+1,yy]=a[xx,yy] then search(xx+1,yy)
     else
      begin
       ans:=ans+1;
       search(xx+1,yy);
      end;
   end;
  if a[xx,yy+1]>0 then
   begin
    if a[xx,yy+1]=a[xx,yy] then search(xx,yy+1)
     else
      begin
       ans:=ans+1;
       search(xx,yy+1);
      end;
   end;
  if a[xx+1,yy+1]>0 then
   begin
    if a[xx+1,yy+1]=a[xx,yy] then
     begin
      ans:=ans+2;
      search(xx+1,yy+1);
     end
    else
     begin
      ans:=ans+2;
      search(xx+1,yy+1);
     end;
  end;
 end;
end;
begin
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);
fillchar(a,sizeof(a),0);
ans:=0;
readln(m,n);
for i:=1 to n do
 begin
  readln(x,y,c);
  a[x,y]:=c+1;
 end;
if (a[m-1,n]=0) and (a[m-1,n-1]=0) and (a[m-1,n-1]=0) then
 begin
  writeln('-1');
  halt;
 end;
search(1,1);
writeln(ans);
close(input);
close(output);
end.

uses math;
var
  f:text;
  book,cust,i,j,n:integer;
  bian:array[1..1000] of qword;
  find:array[1..1000] of qword;
  line:array[1..1000] of qword;
  ans:array[1..1000] of qword;
  m:qword;
begin
  assign(f,'librarian.in');
  reset(f);
  readln(f,book,cust);
  for i:=1 to book do
    readln(f,bian[i]);
  for i:=1 to cust do
    readln(f,line[i],find[i]);
  close(f);

  for i:=1 to cust do
    ans[i]:=maxlongint;
  for i:=1 to book do
    for j:=1 to cust do
      begin
        m:=bian[i] mod 10**line[j];
        if m=find[j]  then
          if ans[j]>bian[i] then
            ans[j]:=bian[i];
      end;

  assign(f,'librarian.out');
  rewrite(f);
  for j:=1 to cust do
    if ans[j]<>maxlongint then
      writeln(f,ans[j])
    else
      writeln(f,'-1');
  close(f);
end.








var
  m,n,i,j,x,y,c,total,magic:integer;
  ge:array[1..1000,1..1000] of integer;
  costdown,costright,magicdown,magicright:array[1..1000,1..1000] of integer;
  f,way:text;
begin

  assign(f,'chess.in');
  reset(f);
  readln(f,m,n);
  for x:=1 to m do
    for y:=1 to m do
      ge[x,y]:=2;

  for i:=1 to n do
    readln(f,x,y,ge[x,y]);
  close(f);

  for i:=1 to n do
    for j:=1 to n do
      begin
        if ge[i,j]=1 then
          begin
            if ge[i+1,j]=0 then
              inc(costdown[i+1,j]);
            if ge[1,j+1]=0 then
              inc(costright[i,j+1]);
            if (ge[i+1,j]=2) then
              begin
               inc(costdown[i+1,j]);
               inc(costdown[i+1,j]);
              end;
            if (ge[i,j+1]=2) then
              begin
               inc(costright[i,j+1]);
               inc(costright[i,j+1]);
              end;
           end;
        if ge[i,j]=0 then
          begin
            if ge[i+1,j]=1 then
              inc(costdown[i+1,j]);
            if ge[1,j+1]=1 then
              inc(costright[i,j+1]);
            if (ge[i+1,j]=2) then
              begin
                 inc(costdown[i+1,j]);
                 inc(costdown[i+1,j]);
              end;
            if (ge[i,j+1]=2) then
              begin
                inc(costright[i,j+1]);
                inc(costright[i,j+1]);
              end;
           end;
       end;

    assign(f,'chess.out');
    rewrite(f);

    x:=1;
    y:=1;
    total:=0;
    while (x<>m) and (y<>m) do
      if (costdown[x,y+1]>costright[x+1,y]) then
      begin
        if (magic=1) and (ge[x+1,y]=2) and (ge[x,y+1]=2) then
          begin
            write('-1');
            exit;
          end;
        if (magic=0) and (ge[x+1,y]=2) and (ge[x,y+1]=2) then
          begin
            inc(y);
            total:=total+2;
            inc(magic);
            ge[x,y]:=ge[x,y-1];
          end
        else
          begin
            inc(y);
            total:=total+costright[x,y+1];
            magic:=0;
          end;
       end

      else
       begin
        if (magic=1) and (ge[x+1,y]=2) and (ge[x,y+1]=2) then
          begin
            write(f,'-1');
            exit;
          end;
        if (magic=0) and (ge[x+1,y]=2) and (ge[x,y+1]=2) then
          begin
            inc(y);
            total:=total+1;
            inc(magic);
            ge[x,y]:=ge[x,y-1];
          end
        else
          begin
            inc(x);
            total:=total+costdown[x+1,y];
            magic:=0;
          end;
        end;


    write(f,total);
    close(f);

end.

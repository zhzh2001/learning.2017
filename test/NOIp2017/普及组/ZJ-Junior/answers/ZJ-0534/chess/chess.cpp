#include<bits/stdc++.h>
using namespace std;
int qx[1000009];
int qy[1000009];
int qans[1000009];
int qc[1000009];
int mark[109][109],mp[109][109];
int way[4][2],ans=-1;
int l,r;
int n,m;
int main ()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	way[0][0]=1;
	way[1][0]=-1;
	way[2][1]=1;
	way[3][1]=-1;
	memset(mp,-1,sizeof(mp));
	memset(mark,-1,sizeof(mark));
	scanf("%d %d",&m,&n);
	int i,j,k;
	for(i=1;i<=n;i++)
	{
		int x,y,c;
		scanf("%d %d %d",&x,&y,&c);
		mp[x][y]=c;
	}
	qx[r]=1;
	qy[r]=1;
	qans[r]=0;
	qc[r]=mp[1][1];
	r++;
	while(l<r)
	{
		int x=qx[l],y=qy[l],mc,nans=qans[l],c=qc[l];
		if(mp[x][y]!=-1) mc=1;
		else mc=0;
		l++;
		if(x==m&&y==m)
		{
			if(ans==-1||ans>nans) ans=nans;
		}
		if(nans<mark[x][y]||mark[x][y]==-1) mark[x][y]=nans;
		else continue;
		//cout<<x<<" "<<y<<" "<<nans<<" "<<mc<<endl;
		for(i=0;i<4;i++)
		{
			int nextx,nexty,nextmc=mc,nextans=nans,nextc;
			nextx=x+way[i][0];
			nexty=y+way[i][1];
			nextc=mp[nextx][nexty];
			if(nextc==-1)
			{
				if(mc==1)
				{
					nextans+=2;
					nextc=c;
				}
				else continue;
			}
			else if(nextc!=c) nextans+=1;
			if(nextx>=1&&nextx<=m&&nexty>=1&&nexty<=m)
			{
				qx[r]=nextx;
				qy[r]=nexty;
				qans[r]=nextans;
				qc[r]=nextc;
				r++;
			}
		}
	}
	/*for(i=1;i<=m;i++)
	{
		for(j=1;j<=m;j++)
		{
			cout<<mark[i][j]<<" ";
		}
		cout<<endl;
	}*/
	printf("%d",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

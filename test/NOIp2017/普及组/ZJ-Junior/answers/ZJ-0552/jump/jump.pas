var
  i,j,l,r,mid,dd,ans,k,n,d,max:longint;
  s,x,a:array[-10..10000]of longint;
  bo:array[-10..10000]of boolean;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  read(n,d,k);
  for i:=1 to n do begin read(x[i],s[i]);if s[i]>0 then j:=j+s[i]; end;
  if j<k then begin write(-1); close(input); close(output); exit; end;
  l:=1;
  r:=x[n]-d+1;
  j:=0;
  max:=0;
  for i:=1 to n do begin if x[i] mod d =0 then j:=j+s[i];if j>max then max:=j;end;
  if max>=k then begin write(0);close(input);close(output);exit;end;
  x[0]:=0;
  s[0]:=0;
  repeat
    mid:=(l+r)div 2;
    fillchar(a,sizeof(a),0);
    fillchar(bo,sizeof(bo),0);
    bo[0]:=true;
    dd:=d-mid;
    max:=0;
    if dd<1 then dd:=1;
    for i:=1 to n do begin
      for j:=0 to i-1 do begin
      if ((x[i]-x[j])mod dd<=(x[i]-x[j])div dd*(mid+d-dd))and bo[j] then begin bo[i]:=true;if a[i]<a[j] then a[i]:=a[j];end; end;
      a[i]:=a[i]+s[i];if (max<a[i])and bo[i] then max:=a[i];
    end;
    if max<k then begin l:=mid+1;end else begin r:=mid;end;
  until l>=r;
  write(r);
  close(input);
  close(output);
end.







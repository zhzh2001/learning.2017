var
 i,j,k,n,p:longint;
 a,b,c:array[0..1010]of longint;
 bo:boolean;
function power(a:longint):longint;
var i:longint;
begin
  power:=1;
  for i:=1 to a do power:=power*10;
end;
procedure q(l,r:longint);
var i,j,mid,t:longint;
begin
  i:=l;
  j:=r;
  mid:=a[(l+r)div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
    if i<=j then begin
      t:=a[i];
      a[i]:=a[j];
      a[j]:=t;
      inc(i);
      dec(j);
    end;
  until i>j;
  if l<j then q(l,j);
  if i<r then q(i,r);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  read(n,p);
  for i:=1 to n do read(a[i]);
  for i:=1 to p do read(b[i],c[i]);
  q(1,n);
  for i:=1 to p do begin
    j:=power(b[i]);
    bo:=true;
    for k:=1 to n do if a[k] mod j = c[i] then begin writeln(a[k]);bo:=false;break;end;
    if bo then writeln(-1);
  end;
  close(input);
  close(output);
end.

var i,j,k,n,q,sum,ans:longint; a,b,c:array[0..1005] of longint;
begin
    assign(input,'librarian.in'); reset(input);
    assign(output,'librarian.out'); rewrite(output);
    readln(n,q);
    for i:=1 to n do
    readln(a[i]);
    for i:=1 to q do
    readln(b[i],c[i]);
    for i:=1 to q do
    begin
        sum:=1; ans:=maxlongint div 3;
        for k:=1 to b[i] do
        sum:=sum*10;
        for j:=1 to n do
        begin
            if a[j] mod sum=c[i] then
            begin
                if a[j]<ans then ans:=a[j];
            end;
        end;
        if ans<>maxlongint div 3 then writeln(ans) else writeln(-1);
    end;
    close(input);
    close(output);
end.

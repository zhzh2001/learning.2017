var i,j,k,m,n,x,y,z:longint; a,f,f1,f2,f3:array[-1..105,-1..105] of longint;
    b:array[-1..105,-1..105] of boolean;
function max(x,y:longint):longint;
begin
    if x>y then exit(y) else exit(x);
end;
begin
    assign(input,'chess.in'); reset(input);
    assign(output,'chess.out'); rewrite(output);
    readln(m,n);
    fillchar(a,sizeof(a),0);
    fillchar(f3,sizeof(f3),$5f);
    for i:=-1 to 105 do
        for j:=-1 to 105 do
        f[i,j]:=maxlongint div 3;
    for i:=1 to n do
    begin
        readln(x,y,z);
        a[x,y]:=z+1;
    end;
    f[1,1]:=0;
    for i:=1 to m do
        for j:=1 to m do
        begin
            if (i=1) and (j=1) then continue;
            if (a[i,j]=0) then
            begin
                if a[i-1,j]<>a[i+1,j] then f1[i+1,j]:=f[i-1,j]+3 else f1[i+1,j]:=f[i-1,j]+2;
                if a[i-1,j]<>a[i,j+1] then f1[i,j+1]:=f[i-1,j]+3 else f1[i,j+1]:=f[i-1,j]+2;
                if a[i,j-1]<>a[i+1,j] then f2[i+1,j]:=f[i,j-1]+3 else f2[i+1,j]:=f[i,j-1]+2;
                if a[i,j-1]<>a[i,j+1] then f2[i,j+1]:=f[i,j-1]+3 else f2[i,j+1]:=f[i,j-1]+2;
                if not(b[i+1,j]) then f3[i+1,j]:=max(f1[i+1,j],f2[i+1,j]);
                if not(b[i,j+1]) then f3[i,j+1]:=max(f1[i,j+1],f2[i,j+1]);
                if b[i+1,j] then f3[i+1,j]:=max(max(f1[i+1,j],f2[i+1,j]),f3[i+1,j]);
                if b[i,j+1] then f3[i,j+1]:=max(max(f1[i,j+1],f2[i,j+1]),f3[i,j+1]);
                b[i+1,j]:=true; b[i,j+1]:=true;
                if (a[i+1,j]=0) or ((a[i-1,j]=0) and (a[i,j-1]=0)) then begin f1[i+1,j]:=0;f2[i+1,j]:=0;f3[i+1,j]:=maxlongint div 3; b[i+1,j]:=false; end;
                if (a[i,j+1]=0) or ((a[i-1,j]=0) and (a[i,j-1]=0)) then begin f1[i,j+1]:=0;f2[i,j+1]:=0;f3[i,j+1]:=maxlongint div 3; b[i,j+1]:=false; end;
            end;
            {if (a[i,j]=0) and (a[i-1,j]<>0) and (a[i,j-1]<>0) then
            f[i,j]:=max(f[i-1,j],f[i,j-1])+2;
            if (a[i,j]=0) and (a[i-1,j]<>0) and (a[i,j-1]=0) then
            f[i,j]:=f[i-1,j]+2;
            if (a[i,j]=0) and (a[i-1,j]=0) and (a[i,j-1]<>0) then
            f[i,j]:=f[i,j-1]+2;}
            if (a[i,j]=1) and (a[i-1,j]=1) and (a[i,j-1]=1) then
            f[i,j]:=max(f[i-1,j],f[i,j-1]);
            if (a[i,j]=1) and (a[i-1,j]=2) and (a[i,j-1]=1) then
            f[i,j]:=max(f[i-1,j]+1,f[i,j-1]);
            if (a[i,j]=1) and (a[i-1,j]=1) and (a[i,j-1]=2) then
            f[i,j]:=max(f[i-1,j],f[i,j-1]+1);
            if (a[i,j]=1) and (a[i-1,j]=0) and (a[i,j-1]=2) then
            f[i,j]:=max(f3[i,j],f[i,j-1]+1);
            if (a[i,j]=1) and (a[i-1,j]=0) and (a[i,j-1]=1) then
            f[i,j]:=max(f3[i,j],f[i,j-1]);
            if (a[i,j]=1) and (a[i-1,j]=1) and (a[i,j-1]=0) then
            f[i,j]:=max(f[i-1,j],f3[i,j]);
            if (a[i,j]=1) and (a[i-1,j]=2) and (a[i,j-1]=0) then
            f[i,j]:=max(f[i-1,j]+1,f3[i,j]);
            if (a[i,j]=1) and (a[i-1,j]=0) and (a[i,j-1]=0) then
            f[i,j]:=f3[i,j];
            if (a[i,j]=2) and (a[i-1,j]=1) and (a[i,j-1]=1) then
            f[i,j]:=max(f[i-1,j]+1,f[i,j-1]+1);
            if (a[i,j]=2) and (a[i-1,j]=2) and (a[i,j-1]=1) then
            f[i,j]:=max(f[i-1,j],f[i,j-1]+1);
            if (a[i,j]=2) and (a[i-1,j]=1) and (a[i,j-1]=2) then
            f[i,j]:=max(f[i-1,j]+1,f[i,j-1]);
            if (a[i,j]=2) and (a[i-1,j]=0) and (a[i,j-1]=2) then
            f[i,j]:=max(f3[i,j],f[i,j-1]);
            if (a[i,j]=2) and (a[i-1,j]=0) and (a[i,j-1]=1) then
            f[i,j]:=max(f3[i,j],f[i,j-1]+1);
            if (a[i,j]=2) and (a[i-1,j]=1) and (a[i,j-1]=0) then
            f[i,j]:=max(f[i-1,j]+1,f3[i,j]);
            if (a[i,j]=2) and (a[i-1,j]=2) and (a[i,j-1]=0) then
            f[i,j]:=max(f[i-1,j],f3[i,j]);
            if (a[i,j]=2) and (a[i-1,j]=0) and (a[i,j-1]=0) then
            f[i,j]:=f3[i,j];
        end;
    if f[m,m]<>maxlongint div 3 then writeln(f[m,m]) else writeln(-1);
    close(input);
    close(output);
end.

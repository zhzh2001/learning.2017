var n,q,i,j,t,x,y:longint;
    ch:string;
    a:array[1..1000]of longint;
    s:array[1..1000]of string;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
    begin
      readln(a[i]);
      str(a[i],s[i]);
    end;
  for i:=1 to n-1 do
    for j:=i+1 to n do
      if a[i]>a[j] then
        begin
          t:=a[i];a[i]:=a[j];a[j]:=t;
          ch:=s[i];s[i]:=s[j];s[j]:=ch;
        end;
  for i:=1 to q do
    begin
      t:=-1;
      readln(x,y);
      str(y,ch);
      for j:=1 to n do
        if copy(s[j],length(s[j])-x+1,x)=ch then
          begin
            t:=j;
            break;
          end;
      if t<>-1 then writeln(a[t])
      else writeln(t);
    end;
  close(input);close(output);
end.
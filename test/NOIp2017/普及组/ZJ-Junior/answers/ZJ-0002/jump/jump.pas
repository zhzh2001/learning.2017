var n,d,k,s,i,t,w,mid:longint;
    a,b:array[1..500000]of longint;
function max1(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
function pd(k1:longint):boolean;
var x,y,t,sum,max,i,h:longint;
begin
  x:=max1(1,d-k1);y:=d+k1;
  t:=0;sum:=0;max:=-maxlongint;h:=0;i:=1;
  while i<=n do
    begin
      if (a[i]-t>=x) and (a[i]-t<=y) then
        begin
          if b[i]>0 then begin sum:=sum+b[i];t:=a[i]; end
          else
            if b[i]>max then
              begin
                max:=b[i];
                h:=a[i];
              end;
        end
      else
        begin
          if max=-maxlongint then
            begin
              if a[i]-t>y then exit(false);
            end
          else
            begin
              sum:=sum+max;t:=h;
              max:=-maxlongint;h:=0;
              dec(i);
            end;
        end;
      if sum>=k then exit(true);
      inc(i);
    end;
  exit(false);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  s:=0;
  for i:=1 to n do
    begin
      readln(a[i],b[i]);
      if b[i]>0 then s:=s+b[i];
    end;
  if s<k then
    begin
      writeln(-1);
      close(input);close(output);
      halt;
    end;
  t:=0;w:=a[n];
  while t<=w do
    begin
      mid:=(t+w) div 2;
      if pd(mid) then
        w:=mid-1
      else
        t:=mid+1;
    end;
  writeln(w+1);
  close(input);close(output);
end.

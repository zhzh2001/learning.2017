Const
  Dx:Array[1..4] Of Longint=(0,1,0,-1);
  Dy:Array[1..4] Of Longint=(1,0,-1,0);
Var
  Min,n,m,x,y,z,l,r,i:Longint;
  A:Array[1..100,1..100] Of longint;
  F:Array[1..1000000,1..5] Of Longint;
  Flag:Array[1..100,1..100] Of Boolean;
Procedure S;
  Begin
    F[1,1]:=1;
    F[1,2]:=1;
    F[1,4]:=A[1,1];
    F[1,5]:=A[1,1];
    Flag[1,1]:=True;
    r:=1;
    While l<r Do
      Begin
        Inc(l);
        For i:=1 To 4 Do
          Begin
            x:=F[l,1]+Dx[i];
            y:=F[l,2]+Dy[i];
            If (x>0) And (x<=n) And (y>0) And (y<=n) And (Flag[x,y]=False) And ((F[l,4]<>0) Or (A[x,y]<>0)) Then
              Begin
                Inc(r);
                If r=1000000 Then
                  Exit;
                F[r,1]:=x;
                F[r,2]:=y;
                F[r,4]:=A[x,y];
                F[r,5]:=A[x,y];
                If A[F[r,1],F[r,2]]=0 Then
                  Begin
                    F[r,3]:=F[l,3]+2;
                    F[r,5]:=F[l,5];
                  End
                Else
                  If F[r,5]=F[l,5] Then
                    F[r,3]:=F[l,3]
                  Else
                    F[r,3]:=F[l,3]+1;
                If (x=n) And (y=n) Then
                  If F[r,3]<Min Then
                    Min:=F[r,3];
              End;
          End;
      End;
  End;
Begin
  Assign(Input,'chess.in');Reset(Input);
  Assign(Output,'chess.out');Rewrite(Output);
  Min:=MaxLongint;
  Readln(n,m);
  For i:=1 To m Do
    Begin
      Readln(x,y,z);
      Case z Of
        0:A[x,y]:=1;
        1:A[x,y]:=2;
      End;
    End;
  S;
  If Min=MaxLongint Then
    Write(-1)
  Else
    Write(Min);
  Close(Input);
  Close(Output);
End.
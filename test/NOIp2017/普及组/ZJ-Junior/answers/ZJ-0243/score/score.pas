var
    a,b,c,sum:longint;
begin
    assign(input,'score.in'); reset(input);
    assign(output,'score.out'); rewrite(output);
    readln(a,b,c);
    sum:=(a div 5)+((b*3) div 10)+(c div 2);
    writeln(sum);
    close(input);
    close(output);
end.

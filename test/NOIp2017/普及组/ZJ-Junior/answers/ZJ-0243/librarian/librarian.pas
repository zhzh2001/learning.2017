var
  b:array [0..10000001,1..2] of longint;
  a:array [0..100000] of longint;
  c:array [0..10000] of longint;
  sum:array [0..10000] of longint;
  d,e,f,g,i,j,n,m:longint;
begin
    assign(input,'librarian.in'); reset(input);
    assign(output,'librarian.out'); rewrite(output);
    readln(n,m);
    for i:=1 to 10000000 do b[i,2]:=maxlongint-1;
    for i:=1 to n do
    begin
        readln(a[i]);
        d:=0; e:=a[i]; g:=1;
        while e>g do
        begin
            g:=g*10;
            d:=(e mod g);
            inc(b[d,1]); if a[i]<b[d,2] then b[d,2]:=a[i];
        end;
    end;
    for i:=1 to m do
    begin
        readln(d,c[i]);
        if b[c[i],1]=0 then sum[i]:=-1 else sum[i]:=b[c[i],2];
    end;
    for i:=1 to m do
    writeln(sum[i]);
    close(input);
    close(output);
end.
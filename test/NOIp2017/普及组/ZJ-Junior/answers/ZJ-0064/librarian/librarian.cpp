#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
int leng[1001],a[1001],w[1001];
void qsort(int l,int r)
{
	if(l>r)
		return;
	int i=l,j=r,t=a[l];
	while(i<j)
	{
		while(i<j&&a[j]>=t)
			j--;
		while(i<j&&a[i]<=t)
			i++;
		if(i<j)
			swap(a[i],a[j]);
	}
	swap(a[l],a[i]);
	qsort(l,i-1);
	qsort(i+1,r);
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,q,i,j;
	bool b;
	cin>>n>>q;
	for(i=1;i<=n;i++)
		cin>>a[i];
	qsort(1,n);
	for(i=1;i<=q;i++)
	{
		b=0;
		cin>>leng[i]>>w[i];
		for(j=1;j<=n;j++)
		{
			if((a[j]-w[i])%(int(pow(10,leng[i])))==0&&a[j]>w[i])
			{
				cout<<a[j]<<endl;
				b=1;
				break;
			}
		}
		if(!b)
			cout<<-1<<endl;
	}
	return 0;
}

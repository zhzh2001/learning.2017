var f,s,x:array[0..500010]of int64;
    n,d,k,i,l,r,mid:longint;tot:int64;

    function big(a,b:int64):int64;
    begin
      if a>b then exit(a)
             else exit(b);
    end;

   function ok(mid:longint):boolean;
   var i,j,min,max:longint;
   begin
     if mid>d then min:=1 else min:=d-mid;
     max:=d+mid;
     if min>x[1] then exit(false);
     for i:=1 to n do f[i]:=-1000000000;
     f[1]:=s[1];
     for i:=1 to n-1 do
     if f[i]<>-1000000000 then
     begin
       for j:=i+1 to n do
       if x[j]-x[i]>=min then
       begin
         if x[j]-x[i]>max then break;
         f[j]:=big(f[j],f[i]+s[j]);
         if f[j]>=k then exit(true);
       end;
     end;
     if f[n]>=k then exit(true) else exit(false);
   end;
begin
assign(input,'jump.in'); reset(input);
assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(x[i],s[i]);
  end;
  tot:=0;
  for i:=1 to n do
  begin
    if s[i]>0 then
      tot:=tot+s[i];
  end;
  if tot<k then begin writeln(-1); close(input); close(output); halt; end;


  l:=0; r:=x[n];
  while l<r do
  begin
    mid:=(l+r)div 2;
    if ok(mid) then r:=mid
               else l:=mid+1;
  end;
  writeln(r);
close(input); close(output);
end.

program exe1;
var
  n, d, kk, us1, us2, i: longint;
  x, s: array[1..501] of longint;

function search(l, r: longint): boolean;
var
  i, j, k, max, ls1, ls2, coin, now, hh: longint;
begin
  //writeln('STEP ', l, ' ', r);
  coin := 0;
  now := 0;
  hh := 1;
  while hh <= n do begin
    max := -maxlongint;
    for j := hh to n do begin
      //writeln('dd: ', now + r, ' ', now + l, ' ', x[j], ' ', s[j]);
      if (x[j] <= now + r) and (x[j] >= now + l) and (s[j] > max) then begin
        max := s[j];
        ls1 := j;
        ls2 := x[j];
      end else if x[j] > now + r then
        break;
    end;
    hh := ls1 + 1;
    now := ls2;
    if max <> -maxlongint then
      inc(coin, max)
    else
      exit(false);
    //writeln(hh, ' ', now, ' ', coin);
    if coin >= kk then exit(true);
  end;
  if coin < kk then exit(false);
end;

begin
  assign(input, 'jump.in');
  assign(output, 'jump.out');
  reset(input);
  rewrite(output);
  readln(n, d, kk);
  for i := 1 to n do
    readln(x[i], s[i]);
  for i := 1 to x[n] do begin
    if i < d then begin
      us1 := d - i;
      us2 := d + i;
    end else begin
      us1 := 1;
      us2 := d + i;
    end;
    if search(us1, us2) then begin
      write(i);
      close(input);
      close(output);
      halt;
    end;
  end;
  write(-1);
  close(input);
  close(output);
end.

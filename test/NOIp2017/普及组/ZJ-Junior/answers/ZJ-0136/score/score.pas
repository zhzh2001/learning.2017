program exe1;
var
  a, b, c: integer;
begin
  assign(input, 'score.in');
  assign(output, 'score.out');
  reset(input);
  rewrite(output);
  read(a, b, c);
  writeln(a div 5 + b * 3 div 10 + c div 2);
  close(input);
  close(output);
end.

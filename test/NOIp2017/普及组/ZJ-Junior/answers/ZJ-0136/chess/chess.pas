program exe1;
const
  dx: array[1..4] of -1..1 = (-1, 0, 0, 1);
  dy: array[1..4] of -1..1 = (0, 1, -1, 0);
var
  m, n, i, j, k, l, min, x, y, c: longint;
  map: array[-1..1001, -1..1001] of integer;
  bb: array[-1..1001, -1..1001] of boolean;

procedure search(x, y, coin: longint; b: boolean);
var
  i, j, x1, y1: longint;
begin
  if (x = m) and (y = m) then begin
    if coin < min then min := coin;
    exit;
  end;
  for i := 1 to 4 do begin
    x1 := x + dx[i];
    y1 := y + dy[i];
    if (x1 >= 1) and (y1 >= 1) and (x1 <= m) and (y1 <= m) and (bb[x1, y1]) then
      if (map[x1, y1] = map[x, y]) and (map[x1, y1] <> -1) then begin
        bb[x1, y1] := false;
        search(x1, y1, coin, false);
        bb[x1, y1] := true;
      end else if (map[x1, y1] <> map[x, y]) and (map[x1, y1] <> -1) then begin
        bb[x1, y1] := false;
        search(x1, y1, coin + 1, false);
        bb[x1, y1] := true;
      end else if (map[x1, y1] <> map[x, y]) and (map[x1, y1] = -1) and (not(b)) then begin
        map[x1, y1] := map[x, y];
        bb[x1, y1] := false;
        search(x1, y1, coin + 2, true);
        bb[x1, y1] := true;
        map[x1, y1] := -1;
      end;
  end;
end;

begin
  assign(input, 'chess.in');
  assign(output, 'chess.out');
  reset(input);
  rewrite(output);
  fillchar(bb, sizeof(bb), true);
  readln(m, n);
  for i := 0 to m do
    for j := 0 to m do
      map[i, j] := -1;
  for i := 1 to n do begin
    readln(x, y, c);
    map[x, y] := c;
  end;
  min := maxlongint;
  search(1, 1, 0, false);
  if min <> maxlongint then write(min) else write(-1);
  close(input);
  close(output);
end.

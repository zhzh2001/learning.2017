program exe1;
var
  b: boolean;
  s1, s2: string;
  n, q, i, j, k, x, y, min: longint;
  ts: array[0..1001] of longint;
begin
  assign(input, 'librarian.in');
  assign(output, 'librarian.out');
  reset(input);
  rewrite(output);
  fillchar(ts, sizeof(ts), 0);
  readln(n, q);
  for i := 1 to n do
    readln(ts[i]);
  for i := 1 to q do begin
    min := maxlongint;
    readln(x, y);
    for j := 1 to n do begin
      str(ts[j], s1);
      str(y, s2);
      b := true;
      for k := length(s1) - x + 1 to length(s1) do
        if s1[k] <> s2[k - length(s1) + x] then
          b := false;
      if b then if ts[j] < min then min := ts[j];
    end;
    if min <> maxlongint then writeln(min) else writeln(-1);
  end;
  close(input);
  close(output);
end.

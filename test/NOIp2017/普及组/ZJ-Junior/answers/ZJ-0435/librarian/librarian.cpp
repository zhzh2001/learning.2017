#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>

using namespace std;

int n,q,tmp,t;
int a[1010];
int num[1010],b[1010];
bool flag;

void Judge(int u)
{
	for(int i=1;i<=n;i++)
	{
		if(a[i]<b[u])	continue;
		tmp=a[i]-b[u];
		t=num[u];
		flag=1;
		while(t--)
		{
			if(tmp%10!=0)
			{
				flag=0;
				break;
			}
			tmp/=10;
		}
		if(flag)
		{
			printf("%d\n",a[i]);
			break;
		}
	}
	if(!flag)	printf("-1\n");
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	memset(a,0,sizeof(a));
	memset(num,0,sizeof(num));
	memset(b,0,sizeof(b));
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for(int i=1;i<=q;i++)
		scanf("%d%d",&num[i],&b[i]);
	for(int i=1;i<=q;i++)
		Judge(i);
	fclose(stdin);fclose(stdout);
	return 0;
}

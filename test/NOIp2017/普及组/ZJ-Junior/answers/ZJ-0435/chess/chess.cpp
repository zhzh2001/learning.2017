#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>

using namespace std;

const int dir[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
int n,m;
int g[110][110];
int Hsh[110][110];//Hsh[x][y] : the least cost from (1,1) to (x,y)

void dfs(int x,int y,bool flag,int clr,int cst)
{
	if(x==m&&y==m)	return ;
	if(cst>=Hsh[m][m])	return ;
	int tx,ty;
	for(int i=0;i<4;i++)
	{
		tx=x+dir[i][0];
		ty=y+dir[i][1];
		if(tx>m||ty>m||tx<1||ty<1)	continue;
//		printf("%d %d %d %d %d\n",tx,ty,flag,clr,cst);
		if(g[tx][ty]==-1&&!flag&&Hsh[tx][ty]>cst+2)
		{
			Hsh[tx][ty]=cst+2;
			dfs(tx,ty,1,clr,cst+2);
		}
		else if(g[tx][ty]!=-1&&g[tx][ty]==clr&&Hsh[tx][ty]>cst)
		{
			Hsh[tx][ty]=cst;
			dfs(tx,ty,0,g[tx][ty],cst);
		}
		else if(g[tx][ty]!=-1&&g[tx][ty]!=clr&&Hsh[tx][ty]>cst+1)
		{
			Hsh[tx][ty]=cst+1;
			dfs(tx,ty,0,g[tx][ty],cst+1);
		}
	}
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(g,-1,sizeof(g));
	memset(Hsh,0x3f3f3f3f,sizeof(Hsh));
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		g[x][y]=v;
	}
//	for(int i=1;i<=m;i++)
//	{
//		for(int j=1;j<=m;j++)
//			printf("%3d",g[i][j]);
//		printf("\n");
//	}
//	printf("\n");
	Hsh[1][1]=0;
	dfs(1,1,0,g[1][1],0);
//	for(int i=1;i<=m;i++)
//	{
//		for(int j=1;j<=m;j++)
//			printf("%3d",Hsh[i][j]==0x3f3f3f3f?	-1:Hsh[i][j]);
//		printf("\n");
//	}
	if(Hsh[m][m]!=0x3f3f3f3f)	printf("%d",Hsh[m][m]);
	else	printf("-1");
	fclose(stdin);fclose(stdout);
	return 0;
}

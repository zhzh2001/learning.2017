#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>

using namespace std;

int n,d,k;
int pos[500010],val[500010];
int l,r,mid;
int dp[500010];

int max(int x,int y)
{
	return x<y?	y:x;
}

int min(int x,int y)
{
	return x>y?	y:x;
}

bool check(int u)
{
	int lb=max(1,d-u),rb=(d+u),res=0;
	memset(dp,-1,sizeof(dp));
	dp[0]=0;
	for(int i=1;i<=n;i++)
	{
		for(int j=i-1;j>=0;j--)
		{
			if(pos[i]-pos[j]<lb)	continue;
			if(pos[i]-pos[j]>rb)	break;
			if(dp[i]!=-1)	dp[i]=max(dp[i],dp[j]+val[i]);
			else if(dp[i]==-1)	dp[i]=dp[j]+val[i];
		}
		res=max(res,dp[i]);
	}
//	printf("%d : %d\n",u,res);
	return res>=k;
}

int read_in()
{
	bool flag=0;
	char c;
	int tmp=0;
	while(c=getchar())
	{
		if(c=='-')
		{
			flag=1;
			continue;
		}
		if(c>'9'||c<'0')
		{
			if(!flag)	return tmp;
			return -1*tmp; 
		}
		tmp=10*tmp+c-'0';
	}
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	pos[0]=0;
	val[0]=0;
	if(n<=500)
		for(int i=1;i<=n;i++)
			scanf("%d%d",&pos[i],&val[i]);
//	�����Ż� 
	else
	{
		read_in();
		for(int i=1;i<=n;i++)
			pos[i]=read_in(),val[i]=read_in();
	}
//debug
//	for(int i=1;i<=n;i++)
//		printf("%d %d\n",pos[i],val[i]);
//*****************************************************
	int l=-1;r=0x3f3f3f3f;
	while(l<r)
	{
		int mid=(l+r)>>1;
		if(check(mid))	r=mid;
		else	l=mid+1;
	}
	if(r!=0x3f3f3f3f)	printf("%d",r);
	else	printf("-1");
	fclose(stdin);fclose(stdout);
	return 0;
}

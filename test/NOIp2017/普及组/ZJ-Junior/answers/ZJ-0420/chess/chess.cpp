#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
#define inf 0x3f3f3f3f
int m,n;
int Map[110][110][2];
int f[110][110];
int dir[5][2]={{-1,0},{0,1},{1,0},{0,-1}};
int i,j;
void dfs(int x,int y)
{
	int i;
	if (Map[x][y][0]==-1) return;
	for(i=0; i<4; i++)
	{
		int xx=x+dir[i][0],yy=y+dir[i][1];
		if (xx<1||yy<1||xx>m||yy>m) continue;
		if (Map[x][y][0]==Map[xx][yy][0]&&Map[xx][yy][1]==-1)
		{
			if (f[xx][yy]>f[x][y])
			{
				f[xx][yy]=f[x][y];
				if (Map[x][y][1]==0) Map[x][y][0]=Map[x][y][1]=-1;
				dfs(xx,yy);
			}
		}
		if ((Map[x][y][0]!=Map[xx][yy][0])&&(Map[xx][yy][0]!=-1)&&Map[xx][yy][1]==-1)
		{
			if (f[xx][yy]>f[x][y]+1)
			{
				f[xx][yy]=f[x][y]+1;
				if (Map[x][y][1]==0) Map[x][y][0]=Map[x][y][1]=-1;
				dfs(xx,yy);
			}
		}
		if ((Map[x][y][0]!=Map[xx][yy][0])&&(Map[xx][yy][0]==-1)&&(Map[x][y][1]==-1))
		{
			if (f[xx][yy]>f[x][y]+2)
			{
				f[xx][yy]=f[x][y]+2;
				Map[xx][yy][1]=0;
				Map[xx][yy][0]=Map[x][y][0];
				dfs(xx,yy);
			}
		}
	}	
	if (Map[x][y][1]==0)
	{
		Map[x][y][1]=Map[x][y][0]=-1;
		return;
	}
}	
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	int x,y,z;
	memset(f,inf,sizeof(f));
	memset(Map,-1,sizeof(Map));
	for (i=1; i<=n; i++)
	{
		cin>>x>>y>>z;
		Map[x][y][0]=z;
	}
	f[1][1]=0;
	dfs(1,1);
	if (f[m][m]!=inf) cout<<f[m][m]<<endl;
	else cout<<-1<<endl;
	return 0;
}

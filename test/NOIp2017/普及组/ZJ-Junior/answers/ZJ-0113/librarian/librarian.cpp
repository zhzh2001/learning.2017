#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("librarian.in");
ofstream fout("librarian.out");
int a[2000],p[]={1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};
int main() {
	int n,q;
	fin>>n>>q;
	for(int i=1;i<=n;i++){
		fin>>a[i];
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=q;i++){
		int t,u,v=0;
		fin >> t>>u;
		t=p[t];
		for(int j=1;j<=n;j++){
			if(a[j]%t==u){
				v=1;
				fout<<a[j]<<endl;
				break;
			}
		}
		if(!v){
			fout<<-1<<endl;
		}
	}
	return 0;
}

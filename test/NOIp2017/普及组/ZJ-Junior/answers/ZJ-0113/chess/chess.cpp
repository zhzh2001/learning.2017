#include <iostream>
#include <fstream>
#include <queue>
#include <cstring>
using namespace std;
struct aaa{
	int x,y,c,g;
};
bool operator < (aaa a,aaa b){
	return a.g>b.g;
}
priority_queue<aaa> p;
ifstream fin("chess.in");
ofstream fout("chess.out");
int a[200][200],b[200][200][3],c=1,m,n;
void doit(){
	int mofa=0;
	aaa q=p.top();
	p.pop();
	c--;
	if(q.g>b[m][m][a[m][m]])c=0;
	if(b[q.x][q.y][q.c]<=q.g){
		return;
	}
	b[q.x][q.y][q.c]=q.g;
	if(a[q.x][q.y]==0)mofa=1;
	if((q.x-1)>=1){
		if(a[q.x-1][q.y]==0){
			if(!mofa){
				aaa q1={q.x-1,q.y,1,q.g+(1==q.c?2:3)};
				aaa q2={q.x-1,q.y,2,q.g+(2==q.c?2:3)};
				c+=2;
				p.push(q1);
				p.push(q2);
			}
		}else{
			aaa q1={q.x-1,q.y,a[q.x-1][q.y],q.g+(a[q.x-1][q.y]==q.c?0:1)};
			c++;
			p.push(q1);
		}
	}
	if((q.y-1)>=1){
		if(a[q.x][q.y-1]==0){
			if(!mofa){
				aaa q1={q.x,q.y-1,1,q.g+(1==q.c?2:3)};
				aaa q2={q.x,q.y-1,2,q.g+(2==q.c?2:3)};
				c+=2;
				p.push(q1);
				p.push(q2);
			}
		}else{
			aaa q1={q.x,q.y-1,a[q.x][q.y-1],q.g+(a[q.x][q.y-1]==q.c?0:1)};
			c++;
			p.push(q1);
		}
	}
	if((q.y+1)<=m){
		if(a[q.x][q.y+1]==0){
			if(!mofa){
				aaa q1={q.x,q.y+1,1,q.g+(1==q.c?2:3)};
				aaa q2={q.x,q.y+1,2,q.g+(2==q.c?2:3)};
				c+=2;
				p.push(q1);
				p.push(q2);
			}
		}else{
			aaa q1={q.x,q.y+1,a[q.x][q.y+1],q.g+(a[q.x][q.y+1]==q.c?0:1)};
			c++;
			p.push(q1);
		}
	}
	if((q.x+1)<=m){
		if(a[q.x+1][q.y]==0){
			if(!mofa){
				aaa q1={q.x+1,q.y,1,q.g+(1==q.c?2:3)};
				aaa q2={q.x+1,q.y,2,q.g+(2==q.c?2:3)};
				c+=2;
				p.push(q1);
				p.push(q2);
			}
		}else{
			aaa q1={q.x+1,q.y,a[q.x+1][q.y],q.g+(a[q.x+1][q.y]==q.c?0:1)};
			c++;
			p.push(q1);
		}
	}
}
int main() {
	memset(b,0x66,sizeof(b));
	fin>>m>>n;
	for(int i=1;i<=n;i++){
		int x,y,c;
		fin>>x>>y>>c;
		c=2-c;
		a[x][y]=c;
		b[x][y][3-a[x][y]]=-1;
	}
	aaa q1={1,1,a[1][1],1};
	p.push(q1);
	while(c){
		doit();
	}
	if(a[m][m]==0){
		if(b[m][m][1]>1000000&&b[m][m][2]>1000000)fout<<-1<<endl;
		else fout<<(b[m][m][1]<b[m][m][2]?b[m][m][1]:b[m][m][2])-1<<endl;
	}else{
		if(b[m][m][a[m][m]]>1000000)fout<<-1<<endl;
		else fout<<b[m][m][a[m][m]]-1<<endl;
	}
	return 0;
}

#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=1005,maxe=maxn*12,INF=0x3F3F3F3F;
int n,m,tot,s,t,dist[maxn],mp[105][105],id[105][105],que[maxn],lnk[maxn],nxt[maxe],son[maxe],w[maxe];bool vis[maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
void add_e(int x,int y,int z){tot++;w[tot]=z;son[tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;tot++;w[tot]=z;son[tot]=x;nxt[tot]=lnk[y];lnk[y]=tot;}
bool check(int x,int y)
{
	if(x<1||x>n||y<1||y>n||!mp[x][y]) return false;
	return true;
}
void SPFA()
{
	memset(dist,0x3F,sizeof(dist));
	dist[s]=0;que[1]=s;vis[s]=true;
	int hed=0,til=1;
	while(hed!=til)
	{
		hed=(hed+1)%maxn;vis[que[hed]]=false;
		for(int j=lnk[que[hed]];j;j=nxt[j])
		{
			if(dist[que[hed]]+w[j]<dist[son[j]])
			{
				dist[son[j]]=dist[que[hed]]+w[j];
				if(!vis[son[j]])
				{
					vis[son[j]]=true;
					til=(til+1)%maxn;
					que[til]=son[j];
				}
				if(dist[que[til]]<dist[que[(hed+1)%maxn]]) swap(que[til],que[(hed+1)%maxn]);
			}
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read(),z=read();
		mp[x][y]=z+1;id[x][y]=i;
		if(x==1&&y==1) s=i;
		if(x==n&&y==n) t=i;
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			if(mp[i][j])
			{
				if(check(i+1,j)) add_e(id[i][j],id[i+1][j],mp[i][j]!=mp[i+1][j]);
				if(check(i,j+1)) add_e(id[i][j],id[i][j+1],mp[i][j]!=mp[i][j+1]);
				if(check(i+2,j)&&!mp[i+1][j]) add_e(id[i][j],id[i+2][j],(mp[i][j]!=mp[i+2][j])+2);
				if(check(i,j+2)&&!mp[i][j+1]) add_e(id[i][j],id[i][j+2],(mp[i][j]!=mp[i][j+2])+2);
				if(check(i-1,j+1)) add_e(id[i][j],id[i-1][j+1],(mp[i][j]!=mp[i-1][j+1])+2);
				if(check(i+1,j+1)) add_e(id[i][j],id[i+1][j+1],(mp[i][j]!=mp[i+1][j+1])+2);
			}
		}
	}
	SPFA();
	printf("%d\n",dist[t]==INF?-1:dist[t]);
	return 0;
}

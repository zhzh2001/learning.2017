#include<cstdio>
#include<algorithm> 
using namespace std;
const int maxn=1005,INF=1<<30;
int n,Q,D[maxn],len,id,ans,ten[10];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();Q=read();
	for(int i=1;i<=n;i++) D[i]=read();
	sort(D+1,D+1+n);ten[0]=1;
	for(int i=1;i<=9;i++) ten[i]=ten[i-1]*10;
	while(Q--)
	{
		len=read();id=read();ans=INF;
		for(int i=1;i<=n;i++)
			if(D[i]>=id&&(D[i]-id)%ten[len]==0)
				{ans=D[i];break;}
		if(ans==INF) ans=-1;
		printf("%d\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=500005;
int n,d,k,X[maxn],w[maxn],zsum,L,R,mid;LL F[maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9'){ret=ret*10+ch-'0';ch=getchar();}
	return ret*f;
}
int find(int x)
{
	int l=1,r=n,m;
	while(l<=r)
	{
		m=l+r>>1;
		if(X[m-1]<x&&x<=X[m]) return m;
		x<X[m]?r=m-1:l=m+1;
	}
	return n+1;
}
bool check()
{
	LL ret=0;
	memset(F,158,sizeof(F));F[0]=0;
	for(int i=0;i<n;i++)
	{
		for(int j=find(max(X[i]+d-mid,X[i]+1));X[j]<=X[i]+d+mid;j++)
		{
			if(F[j]<F[i]+w[j])
				F[j]=F[i]+w[j];
		}
	}
	for(int i=1;i<=n;i++) if(F[i]>ret) ret=F[i];
	return ret>=k;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();k=read();
	for(int i=1;i<=n;i++)
	{
		X[i]=read();w[i]=read();
		if(w[i]>0) zsum+=w[i];
	}
	if(zsum<k){printf("-1\n");return 0;}
	R=X[n];X[n+1]=2E9;
	while(L<=R)
	{
		mid=L+R>>1;
		check()?R=mid-1:L=mid+1;
	}
	printf("%d\n",L);
	return 0;
}

var
  i,j,k,n,m,a,b,x,y,z,t:longint;
  f,c:array[0..1003,0..1003]of longint;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);

  readln(m,n);
  for i:=1 to n do
   begin
    readln(x,y,z);
    c[y,x]:=z;
   end;
  writeln('-1'); 

  close(input); close(output);
end.
var
  n,q,i,j,k,a,b,min:longint;
  s:array[0..1005]of longint;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);

  readln(n,q); 
  for i:=1 to n do
   readln(s[i]);
  for j:=1 to q do
   begin
    min:=maxlongint;
    k:=1;
    readln(a,b);
    for i:=1 to a do k:=10*k;
    for i:=1 to n do
     if (s[i]<min)and(s[i] mod k=b) then min:=s[i];
    if min<maxlongint then writeln(min)
                      else writeln('-1');
   end; 

  close(input); close(output);
end.
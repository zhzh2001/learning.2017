var n,m,i,j,x,y,k,t,u:longint;
a:array[0..2000] of longint;
s,s1:string;f:boolean;
procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l;j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i];a[i]:=a[j];a[j]:=y;
      inc(i);j:=j-1;
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to m do
  begin
    readln(x,y);
    u:=0;
    for j:=1 to n do
    if a[j]>=y then
    begin
      f:=true;
      str(a[j],s);str(y,s1);
      k:=length(s);t:=length(s1);
      while t>0 do
      begin
        if s[k]<>s1[t] then
        begin
          f:=false;
          break;
        end;
        dec(k);dec(t);
      end;
      if f then
      begin u:=a[j];break;end;
    end;
    if u=0 then writeln(-1) else writeln(u);
  end;
  close(input);close(output);
end.

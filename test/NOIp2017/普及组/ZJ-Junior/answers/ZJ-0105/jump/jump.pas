var n,d,m,i,t,w,g,max,u:longint;
a,b:array[0..600000] of longint;
function find(r:longint):longint;
var t,w,mid:longint;
begin
  find:=0;
  t:=1;w:=n;
  while t<=w do
  begin
    mid:=(t+w) div 2;
    if a[mid]=r then
    begin
      find:=mid;break;
    end
    else if a[mid]>r then w:=mid-1
    else t:=mid+1;
  end;
end;
function p(g:longint):boolean;
var u,i,j:longint;
begin
  u:=0;
  if g<d then
  begin

    for i:=d-g to d+g do
    if find(i)>0 then
    begin
      u:=u+b[find(i)];
      if u>=m then exit(true);
    end;
  end
  else
  begin
    for i:=1 to d+g do
    if find(i)>0 then
    begin
      u:=u+b[find(i)];
      if u>=m then exit(true);
    end;
  end;
  exit(false);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,m);
  for i:=1 to n do
  begin readln(a[i],b[i]);if a[i]>max then max:=a[i];end;
  if d=1 then
  begin
    for i:=1 to n do
    begin
      u:=u+b[i];
      if u>=m then
      begin
        writeln(0);
        close(input);close(output);halt;
      end;
    end;
    writeln(-1);
    close(input);close(output);halt;
  end;
  t:=1;w:=max-d;
  while t<=w do
  begin
    g:=(t+w) div 2;
    if p(g) then
    begin
      u:=g;w:=g-1;
    end
    else t:=g+1;
  end;
  writeln(u);
  close(input);close(output);
end.
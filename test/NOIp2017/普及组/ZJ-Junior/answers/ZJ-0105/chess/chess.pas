const e:array[1..4,1..2] of longint=((1,0),(0,1),(-1,0),(0,-1));
var n,m,i,j,x,y,z,ll:longint;
a,f:array[0..200,0..200] of longint;
b,c:array[0..40000] of longint;
d:array[0..200,0..200] of boolean;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
procedure sc(x,y:longint);
var t,w,x1,y1,i:longint;
begin
  t:=1;w:=1;
  b[1]:=x;c[1]:=y;
  while t<=w do
  begin
    for i:=1 to 2 do
    begin
      x1:=b[t]+e[i,1];
      y1:=c[t]+e[i,2];
      if (x1>0)and(x1<=n)and(y1>0)and(y1<=n)and(a[b[t],c[t]]<>0)and not d[x1,y1] then
      begin
        inc(w);
        b[w]:=x1;c[w]:=y1;
        if (a[x1,y1]<>0)and(a[b[t],c[t]]=a[x1,y1]) then
          f[x1,y1]:=min(f[x1,y1],f[b[t],c[t]]);

        if (a[x1,y1]<>0)and(a[b[t],c[t]]<>a[x1,y1]) then
          f[x1,y1]:=min(f[x1,y1],f[b[t],c[t]]+1);

        if not d[b[t],c[t]]and(a[x1,y1]=0) then
         begin
           f[x1,y1]:=min(f[x1,y1],f[b[t],c[t]]+2);
           a[x1,y1]:=a[b[t],c[t]];d[x1,y1]:=true;
         end;
      end;
    end;
    inc(t);
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(x,y,z);
    a[x,y]:=z+1;
  end;
  fillchar(f,sizeof(f),63);
  f[1,1]:=0;
  sc(1,1);
  if f[n,n]=f[0,0] then writeln(-1) else writeln(f[n,n]);
  close(input);close(output);
end.

var
  n,d,k,x,i,j,l,s1,s2,tot:longint;
  f,a:array[-10000..500000]of longint;
  z:boolean;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
      begin
        read(x);
        readln(a[x]);
      end;
  for i:=0 to k do
      begin
        if z=true then break;
        s1:=d-i;
        if s1<1 then s1:=1;
        s2:=d+i;
        for j:=1to n do
            begin
              tot:=-maxlongint;
              for l:=j-s2 to j-s1 do
                  if f[i]>tot then tot:=f[i];
              f[j]:=tot+a[j];
              if f[j]>k then begin z:=true;write(i);break;end;
            end;
      end;
  if z=false then write(-1);
  close(input);
  close(output);
end.


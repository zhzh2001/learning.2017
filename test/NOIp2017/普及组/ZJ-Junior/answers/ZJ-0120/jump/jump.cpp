#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>
#include <algorithm>
const int INF = 2147483640;
int n, d, k;
struct node{
	int x, s;
}a[500001];
int dis[500001];
bool check(int g);
int main(){
	using namespace std;
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	long long mid, l = 0, r = 1e9;
	cin >> n >> d >> k;
	for (int i=1;i<=n;++i){
		scanf("%d%d", &a[i].x, &a[i].s);
		//cin >> a[i].x >> a[i].s;
	}
	a[0].s = 0; a[0].x = 0;
	while (l < r){
		mid = (l + r)/2;
		if (check(mid)) r = mid;
		else l = mid + 1;
	}
	if (l == 1e9) l = -1;
	cout << l;
	return 0;
}
bool check(int g){
	long long t1 = d-g, t2 = d+g;
	if (t1 <= 0) t1 = 1;
	std::queue<int> q;
	q.push(0);
	int i;
	for (i=1;i<=n;++i) dis[i] = -INF;
	long long t_max, t_min, now;
	while (!q.empty()){
		now = q.front();
		q.pop();
		t_min = a[now].x + t1;
		t_max = a[now].x + t2;
		i = now;
		//while (a[i].x < t_min) ++i;
		for (i=0; i<=n; ++i){
			if (a[i].x < t_min) continue;
			if (a[i].x > t_max) break;
			if (dis[now] + a[i].s > dis[i]){
				dis[i] = dis[now] + a[i].s;
				q.push(i);
			}
		}
	}
	for (i=1;i<=n;++i)
		if (dis[i] >= k) return true;
	return false;
}

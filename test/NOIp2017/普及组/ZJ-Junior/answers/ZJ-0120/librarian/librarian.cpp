#include <cstdio>
#include <iostream>
#include <algorithm>
const int INF = 2147483640;
int n, q, book[1020];
int cacl[20] = {0, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};
int main(){
	using namespace std;
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	cin >> n >> q;
	for (int i=0; i<n; ++i){
		scanf("%d", &book[i]);
	}
	int x, y, j;
	bool flag;
	sort(book, book+n);
	for (int i=0; i<q; ++i){
		scanf("%d%d", &x, &y);
		flag = true;
		for (j=0; j<n; ++j){
			if (book[j]%cacl[x] == y){
				printf("%d", book[j]);
				cout << endl;
				flag = false;
				break;
			}
		}
		if (flag) cout << -1 << endl;
	}
	return 0;
}


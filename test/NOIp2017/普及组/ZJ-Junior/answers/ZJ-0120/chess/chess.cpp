#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
const int INF = 2147483640;
const int dir[4][2] = { {1,0}, {-1, 0}, {0,1}, {0,-1} };
int n, m, a[101][101], ans = INF;
int book[101][101][2];
struct node{
	int x, y, color, now;
};
void dfs(const int x, const int y, const int color, const int money){
	//std::cerr << x << ' ' << y << std::endl;
	if (x == m && y == m){
		ans = std::min(ans, money);
		return;
	}
	node now;
	for (int i=0;i<4;++i){
		now.x = x + dir[i][0];
		now.y = y + dir[i][1];
		if (now.x < 1 || now.y < 1 || now.x > m || now.y > m) continue;
		if (a[x][y] == -1 && a[now.x][now.y] == -1) continue;
		if (a[now.x][now.y] == -1){
			if (color == 0) {
				if (book[now.x][now.y][1] > money+3){
					book[now.x][now.y][1] = money+3;
					dfs(now.x, now.y, 1, money + 3);
				}
				if (book[now.x][now.y][0] > money+2){
					book[now.x][now.y][0] = money+2;
					dfs(now.x, now.y, 0, money+2);
				}
				continue;
			}
			if (color == 1) {
				if (book[now.x][now.y][1] > money+2){
					book[now.x][now.y][1] = money+2;
					dfs(now.x, now.y, 1, money + 2);
				}
				if (book[now.x][now.y][0] > money+3){
					book[now.x][now.y][0] = money+3;
					dfs(now.x, now.y, 0, money+3);
				}
				continue;
			}
		}
		if (color == a[now.x][now.y] && book[now.x][now.y][color] > money){
			book[now.x][now.y][color] = money;
			dfs(now.x, now.y, a[now.x][now.y], money);
		}
		if (color != a[now.x][now.y] && book[now.x][now.y][a[now.x][now.y]] > money+1){
			book[now.x][now.y][a[now.x][now.y]] = money+1;
			dfs(now.x, now.y, a[now.x][now.y], money+1);
		}
	}
}
int main(){
	using namespace std;
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	scanf("%d%d", &m, &n);
	memset(a, -1, sizeof(a));
	int x, y, c;
	for (int i=0;i<=m;++i)
		for (int j=0; j<=m; ++j)
			book[i][j][0] = book[i][j][1] = INF;
	for (int i=0; i<n; ++i){
		scanf("%d%d%d", &x, &y, &c);
		a[x][y] = c;
	}
	dfs(1, 1, a[1][1], 0);
	if (ans == INF) cout << -1;
	else cout << ans;
	return 0;
}

var i,n,m,j,x,y,c,min:longint;
    vis:array[0..101,0..101] of boolean;
    a:array[0..101,0..101] of longint;
    dx:array[1..4]of longint=(0,1,0,-1);
    dy:array[1..4]of longint=(1,0,-1,0);
function check(x,y:longint):boolean;
var flag:boolean;i,x1,y1:longint;
begin
  if a[x][y]<>0 then exit(true);
  flag:=true;
  for i:=1 to 4 do
    begin
    x1:=x+dx[i];y1:=y+dy[i];
    if (a[x1][y1]<>0)and(not vis[x1][y1]) then flag:=false;
    end;
  if not flag then exit(true);
  exit(false);
end;
procedure dfs(x,y,money,nowc:longint);
var x1,y1,i:longint;
begin
  if (x=m)and(y=m) then
    begin if money<min then min:=money;exit;end;
  if not check(x,y) then exit;
  for i:=1 to 4 do
  begin
    x1:=x+dx[i];y1:=y+dy[i];
    if vis[x1][y1] then continue;
    if (x1>0)and(x1<=m)and(y1>0)and(y1<=m) then
    begin
      if nowc=a[x1][y1] then
      begin
        vis[x1][y1]:=true;
        dfs(x1,y1,money,a[x1][y1]);
        vis[x1][y1]:=false;
      end
      else if a[x1][y1]=0 then
        begin
          //nowc:=a[x1][y1];
          vis[x1][y1]:=true;
          dfs(x1,y1,money+2,nowc);
          //dfs(x1,y1,money+3,abs(a[x1][y1]-3));
          vis[x1][y1]:=false;
        end
      else
        begin
          vis[x1][y1]:=true;
          dfs(x1,y1,money+1,a[x1][y1]);
          vis[x1][y1]:=false;
        end;
    end;
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);  min:=maxlongint;
  for i:=1 to n do
  begin
    read(x,y,c);
    a[x][y]:=c+1;
  end;
  //for i:=1 to m do begin
  //for j:=1 to m do write(a[i][j],' ');writeln;end;
  if a[1][1]=0 then
    begin
      writeln(-1);close(input);close(output);exit;end;
  dfs(1,1,0,a[1][1]);
  if min=maxlongint then writeln(-1) else writeln(min);
  close(input);close(output);
end.

var n,q,i,j,m,c:longint;
  a,b:array[1..10000]of longint;
procedure sort(l,r:longint);
var i,j,x:longint;
begin
  if l>=r then exit;
  i:=l;j:=r;
  x:=a[i];
  while i<j do
  begin
    while (i<j)and(a[j]>x) do dec(j);
    if i<j then
    begin
      a[i]:=a[j];
      inc(i);
    end;
    while (i<j)and(a[i]<x) do inc(i);
    if i<j then
    begin
      a[j]:=a[i];
      dec(j);
    end;
    a[i]:=x;
  end;
  sort(l,i-1);
  sort(i+1,r);
end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  sort(1,n);
  for i:=1 to q do
  begin
    read(c);m:=1;
    read(b[i]);
    for j:=1 to c do m:=m*10;
    for j:=1 to n do
      if a[j]mod m=b[i]mod m then begin writeln(a[j]);m:=0;break;end;
    if m<>0 then writeln(-1);
  end;
  close(input);
  close(output);
end.
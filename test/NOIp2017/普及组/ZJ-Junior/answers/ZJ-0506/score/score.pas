var a,b,c,s:int64;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(a,b,c);
  s:=a*2 div 10+b*3 div 10+c*5 div 10;
  writeln(s);
  close(input);
  close(output);
end.
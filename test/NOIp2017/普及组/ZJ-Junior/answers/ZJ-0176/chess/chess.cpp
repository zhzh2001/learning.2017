#include<iostream>
using namespace std;
short map[1001][1001];
int cost[1001][1001];
int xp[5]={0,1,0,-1,0};
int yp[5]={0,0,1,0,-1};
int n,m;
int dfs(int x,int y,int c,int color){
	int ans=c,total,t,colort=color;
	if(map[x][y]==0){
		ans+=2;
		colort=color+2;
	}
	else if(map[x][y]!=(color+1)%2+1){
		ans++;
		colort=map[x][y];
	}
	else colort=color-2;
	if(ans>=cost[x][y]||(map[x][y]==0&&color>2))return -1;
	cost[x][y]=ans;
	if(x==n&&y==n)return ans;
	total=210000000;
	for(int i=1;i<=4;i++){
		t=dfs(x+xp[i],y+yp[i],ans,colort);
		if(t<total&&t!=-1)total=t;
	}
	if(total==210000000)return -1;
	return total;
}
int main(){
	freopen("chess3.in","r",stdin);
	freopen("chess3.out","w",stdout);
	int t1,t2,t3;
	cin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)cost[i][j]=210000000;
	}
	for(int i=1;i<=m;i++){
		cin>>t1>>t2>>t3;
		map[t1][t2]=t3+1;
	}
	cout<<dfs(1,1,0,map[1][1]);
}

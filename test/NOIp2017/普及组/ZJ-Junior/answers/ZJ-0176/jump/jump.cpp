#include<iostream>
using namespace std;
int n,d,k;
int map[500001][4];
int abs(int num){
	if(num<0)return -num;
	return num;
}
int f(int x,int c,int score){
	int scoret=score+map[x][1],ans=2100000000,r;
	if(c>=map[x][2]&&scoret<=map[x][3])return -1;
	map[x][2]=c;
	map[x][3]=scoret;
	if(scoret>=k)return c;
	for(int i=x+1;i<=n;i++){
		int t=abs(map[i][0]-map[x][0]-d);
		if(t<c)t=c;
		r=f(i,t,scoret);
		if(r<ans&&r!=-1)ans=r;
	}
	if(ans==2100000000)return -1;
	return ans;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	int t1,t2;
	cin>>n>>d>>k;
	for(int i=1;i<=n;i++){
		cin>>map[i][0]>>map[i][1];
		map[i][2]=2100000000;
	}
	map[0][2]=2100000000;
	cout<<f(0,0,0);
}

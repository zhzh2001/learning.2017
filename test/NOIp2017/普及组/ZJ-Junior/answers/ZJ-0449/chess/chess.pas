var
        m,n,i,j,x,y,z,head,tail,xx,yy:longint;
        color,first,min,flag:array[0..1000,0..1000]of longint;
        que:array[0..100000,0..2]of longint;
        dx,dy:array[0..4]of longint;
begin
        assign(input,'chess.in');reset(input);
        assign(output,'chess.out');rewrite(output);
        dx[1]:=0;dx[2]:=1;dx[3]:=-1;dx[4]:=0;
        dy[1]:=1;dy[2]:=0;dy[3]:=0;dy[4]:=-1;
        readln(m,n);
        for i:=1 to m do
                for j:=1 to m do
                begin
                        color[i,j]:=-1;
                        first[i,j]:=0;
                end;
        for i:=1 to n do
        begin
                readln(x,y,z);
                color[x,y]:=z;
                first[x,y]:=1;
        end;
        for i:=1 to m do
                for j:=1 to m do min[i,j]:=maxlongint;
        head:=1;
        tail:=1;
        que[head,1]:=1;
        que[head,2]:=1;
        min[1,1]:=0;
        flag[1,1]:=1;
        while head<=tail do
        begin
                if (color[que[head,1],que[head,2]]<>-1)and(first[que[head,1],que[head,2]]=1) then
                begin
                        for i:=1 to 4 do
                        begin

                                xx:=que[head,1]+dx[i];
                                yy:=que[head,2]+dy[i];
                                if (color[xx,yy]<>-1)and(xx>0)and(xx<=m)and(yy>0)and(yy<=m)and(flag[xx,yy]=0) then
                                begin
                                        if color[xx,yy]=color[que[head,1],que[head,2]] then
                                                if min[xx,yy]>min[que[head,1],que[head,2]] then
                                                        min[xx,yy]:=min[que[head,1],que[head,2]];
                                        if color[xx,yy]<>color[que[head,1],que[head,2]] then
                                                if min[xx,yy]>min[que[head,1],que[head,2]]+1 then
                                                        min[xx,yy]:=min[que[head,1],que[head,2]]+1;
                                        inc(tail);
                                        que[tail,1]:=xx;
                                        que[tail,2]:=yy;
                                        flag[xx,yy]:=1;
                                end
                                else if (color[xx,yy]=-1)and(xx>0)and(xx<=m)and(yy>0)and(yy<=m)and(flag[xx,yy]=0) then
                                begin
                                        if min[xx,yy]>min[que[head,1],que[head,2]]+2 then
                                        begin
                                                min[xx,yy]:=min[que[head,1],que[head,2]]+2;
                                                color[xx,yy]:=color[que[head,1],que[head,2]];
                                                inc(tail);
                                                que[tail,1]:=xx;
                                                que[tail,2]:=yy;
                                                flag[xx,yy]:=1;
                                        end;
                                end;
                        end;
                end
                else if (color[que[head,1],que[head,2]]<>-1)and(first[que[head,1],first[head,2]]=0) then
                begin
                        for i:=1 to 4 do
                        begin
                                xx:=que[head,1]+dx[i];
                                yy:=que[head,2]+dy[i];
                                if (color[xx,yy]<>-1)and(xx>0)and(xx<=m)and(yy>0)and(yy<=m)and(flag[xx,yy]=0) then
                                begin
                                        if color[xx,yy]=color[que[head,1],que[head,2]] then
                                                if min[xx,yy]>min[que[head,1],que[head,2]] then
                                                        min[xx,yy]:=min[que[head,1],que[head,2]];
                                        if color[xx,yy]<>color[que[head,1],que[head,2]] then
                                                if min[xx,yy]>min[que[head,1],que[head,2]]+1 then
                                                        min[xx,yy]:=min[que[head,1],que[head,2]]+1;
                                        inc(tail);
                                        que[tail,1]:=xx;
                                        que[tail,2]:=yy;
                                        color[que[head,1],que[head,2]]:=-1;
                                        flag[xx,yy]:=1;
                                end;
                        end;
                end;
                inc(head);
        end;
        if min[m,m]=maxlongint then writeln(-1)
        else writeln(min[m,m]);
        close(input);close(output);
end.

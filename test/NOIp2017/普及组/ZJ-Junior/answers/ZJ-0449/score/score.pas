var
        x,y,z,sum1,sum2,sum3:longint;
begin
        assign(input,'score.in');reset(input);
        assign(output,'score.out');rewrite(output);
        readln(x,y,z);
        if x=0 then sum1:=0
        else sum1:=(x div 10)*2;
        if y=0 then sum2:=0
        else sum2:=(y div 10)*3;
        if z=0 then sum3:=0
        else sum3:=(z div 10)*5;
        writeln(sum1+sum2+sum3);
        close(input);close(output);
end.
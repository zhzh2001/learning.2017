program noip2017pj_2;
var n,q,i,j:longint;
    bookse:array[1..1000] of int64;   //book num
    len:array[1..1000] of int64;           //request pos num
    last:array[1..1000] of string;
    ta:array[1..1000] of int64;            //request num
    tas:array[1..1000] of string;
    find:array[1..1000] of int64;
    flag:boolean;
    temp:string;

procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=bookse[(l+r) div 2];
         repeat
           while bookse[i]<x do
            inc(i);
           while x<bookse[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=bookse[i];
                bookse[i]:=bookse[j];
                bookse[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;



Begin
  assign(input,'librarian.in');
 assign(output,'librarian.out');
 reset(input);
  rewrite(output);
  read(n,q);   //n:book number   q:reader number

  for i:=1 to n do
    begin
     readln(bookse[i]);
    end;
  for i:=1 to q do
    begin
      readln(len[i],ta[i]);
      find[i]:=-1;
    end;
  flag:=True;
 sort(1,n);
  for i:=1 to n do
    str(bookse[i],last[i]);
  for i:=1 to q do
    str(ta[i],tas[i]);
  for i:=1 to q do
    for j:=1 to n do
      begin
        temp:=copy(last[j],length(last[j])-len[i]+1,len[i]);
        if temp=tas[i] then
          begin
            find[i]:=bookse[j];
            break;
          end;
      end;
  for i:=1 to q do
    begin
      write(find[i]);
      writeln;
    end;

  close(input);
 close(output);
End.

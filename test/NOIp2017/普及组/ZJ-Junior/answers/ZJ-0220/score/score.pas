program noip2017pj_1;
var a,b,c,ans:longint;

Begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(a,b,c);
  ans:=((a*2) div 10)+((b*3) div 10)+((c*5) div 10);
  write(ans);
  close(input);
  close(output);
end.
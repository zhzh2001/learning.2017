type arr=record
     x:longint;
     y:longint;
end;
var a,cnt:array[1..100,1..100] of longint;
    b:array[1..100,1..100] of boolean;
    q:array[1..500000] of arr;
    m,n,i,j,k,x,y,z,head,tail:longint;
procedure inq(x,y:longint);
begin
   inc(tail);
   q[tail].x:=x;
   q[tail].y:=y;
end;
procedure check(x,y:longint);
begin
   if (x=0) or (y=0) then exit;
   if (b[q[i].x,q[i].y]=false) and (b[x,y]=false) then exit;
   if (a[q[i].x,q[i].y]=a[x,y]) and (cnt[q[i].x,q[i].y]<cnt[x,y]) then
    begin
       cnt[x,y]:=cnt[q[i].x,q[i].y];
       inq(x,y);
    end;
   if (b[x,y]=false) and (cnt[x,y]>(cnt[q[i].x,q[i].y]+2)) then
    begin
       cnt[x,y]:=cnt[q[i].x,q[i].y]+2;
       a[x,y]:=a[q[i].x,q[i].y];
       inq(x,y);
    end;
   if (b[x,y]<>false) and (cnt[x,y]>(cnt[q[i].x,q[i].y]+1)) and (a[x,y]<>a[q[i].x,q[i].y]) then
    begin
       cnt[x,y]:=cnt[q[i].x,q[i].y]+1;
       inq(x,y);
    end;
end;
begin
   assign(input,'chess.in');reset(input);
   assign(output,'chess.out');rewrite(output);
   for i:=1 to 100 do
    for j:=1 to 100 do
     cnt[i,j]:=20001;
   readln(m,n);
   fillchar(b,sizeof(b),false);
   fillchar(a,sizeof(a),0);
   for i:=1 to n do
    begin
       readln(x,y,z);
       a[x,y]:=z+1;
       b[x,y]:=true;
    end;
   cnt[1,1]:=0;
   q[1].x:=1;
   q[1].y:=1;
   head:=1;
   tail:=1;
   while head<=tail do
    begin
       k:=tail;
       for i:=head to tail do
        begin
           check(q[i].x,q[i].y-1);
           check(q[i].x,q[i].y+1);
           check(q[i].x+1,q[i].y);
           check(q[i].x-1,q[i].y);
           if b[q[i].x,q[i].y]=false then a[q[i].x,q[i].y]:=0;
        end;
       head:=k+1;
    end;
   if cnt[m,m]<>20001 then writeln(cnt[m,m])
    else writeln('-1');
   close(input);
   close(output);
end.

#include "iostream"
#include "cstdio"
#include "algorithm"
#include "cstring"
#include "cmath"
#include "cstdlib"

#define map mmmmmap
#define push pppppush
#define bfs bbbbbfs
#define N 1000001
#define dx dddddx
#define dy dddddy
#define qx qqqqqx
#define qy qqqqqy
#define arrive aaaaarrive

using namespace std;

int m,n,t1,t2,t3,map[101][101]={0},qx[N],qy[N],h=0,t=0,b[101][101][5]={0},dx,dy,arrive=0;
int ff[101][101];

inline void read(int &x)
{
	int w=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=w;
}

int push(int x,int y,int k)
{
	if(b[dx][dy][k]==1)
		return 0;
	if(map[dx][dy]<0&&map[x][y]==0)
		return 0;
	b[dx][dy][k]=1;
	t++;
	if(abs(map[dx][dy])==map[x][y])
		ff[x][y]=min(ff[dx][dy],ff[x][y]);
	else
	{
		if(map[x][y]==0)
		{
			map[x][y]=-1*map[dx][dy];
			ff[x][y]=min(ff[dx][dy]+2,ff[x][y]);
		}
		else
			ff[x][y]=min(ff[dx][dy]+1,ff[x][y]);
	}
	if(x==m&&y==m)
		arrive=1;
	qx[t]=x;
	qy[t]=y;
}

int bfs()
{
	t++;
	qx[t]=1;
	qy[t]=1;
	do
	{
		h++;
		dx=qx[h]; dy=qy[h];
		if(qx[h]>1) {push(qx[h]-1,qy[h],1);}
		if(qy[h]>1) {push(qx[h],qy[h]-1,2);}
		if(qx[h]<m) {push(qx[h]+1,qy[h],3);}
		if(qy[h]<m) {push(qx[h],qy[h]+1,4);}
	}
	while(h<t);
	if(arrive==0)
		printf("-1");
	else
		printf("%d",ff[m][m]);
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	read(m); read(n);
	if(m==1)
	{
		printf("0");
		exit(0);
	}
	for(int i=1;i<=100;i++)
		for(int j=1;j<=100;j++)
			ff[i][j]=99999999;
	ff[1][1]=0;
	for(int i=1;i<=n;i++)
	{
		read(t1); read(t2); read(t3);
		if(t3==0)
			t3=2;
		map[t1][t2]=t3;
	}
	bfs();
	return 0;
}

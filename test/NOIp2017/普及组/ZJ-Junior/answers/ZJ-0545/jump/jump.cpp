#include "iostream"
#include "cstdio"
#include "algorithm"
#include "cstring"
#include "cmath"
#include "cstdlib"

#define judge jjjjjudge

using namespace std;

int n,d,k,a[500001],s[500001],l,r,m;
long long ttot=0,f[500001];

inline void read(int &x)
{
	int w=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=w;
}

bool judge(int x)
{
	int l=max(1,d-x),r=d+x;
	for(int i=1;i<=n;i++)
		f[i]=-1000000001;
	f[0]=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<i;j++)
			if(a[i]-a[j]>=l&&a[i]-a[j]<=r)
			{
				f[i]=max(f[i],f[j]+s[i]);
				if(f[i]>=k)
					return 1;
			}
	return 0;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	read(n); read(d); read(k);
	for(int i=1;i<=n;i++)
	{
		read(a[i]); read(s[i]);
		if(s[i]>0) ttot+=s[i];
	}
	if(ttot<k)
	{
		printf("-1");
		exit(0);
	}
	l=1; r=1000000000;
	while(l<r)
	{
		m=(l+r)/2;
		if(judge(m))
			r=m;
		else
			l=m+1;
	}
	printf("%d",r);
	return 0;
}

#include "iostream"
#include "cstdio"
#include "algorithm"
#include "cstring"
#include "cmath"
#include "cstdlib"

using namespace std;

int n,q,a[100001],k,s;

inline void read(int &x)
{
	int w=1;
	char ch=0;
	x=0;
	while(ch^'-'&&!isdigit(ch))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=(x<<1)+(x<<3)+ch-'0';
		ch=getchar();
	}
	x*=w;
}

int my_pow()
{
	int x=1;
	for(int i=1;i<=s;i++)
		x*=10;
	return x;
}

int my_search()
{
	for(int i=1;i<=n;i++)
		if(a[i]%my_pow()==k)
			return a[i];
	return -1;
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	read(n); read(q);
	for(int i=1;i<=n;i++)
		read(a[i]);
	sort(a+1,a+n+1);
	for(int i=1;i<=q;i++)
	{
		read(s); read(k);
		printf("%d\n",my_search());
	}
	return 0;
}

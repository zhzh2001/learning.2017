var a,f:array[-1000..10000]of longint;
    n,d,k,head,tail,mid,l,r,i,j,ans,x,s,maxx:longint;
    flag:boolean;
function max(x,y:longint):longint;
begin
  if x>y then exit(x)
    else exit(y);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(x,s);
    a[x]:=s;
    if x>maxx then maxx:=x;
  end;
  head:=1;tail:=n+1;
  while head<=tail do
  begin
    mid:=(head+tail) div 2;
    if mid<d then l:=d-mid
      else l:=1;
    r:=d+mid;
    for i:=1 to n do f[i]:=0; flag:=false;
    for i:=1 to maxx do
    begin
      for j:=l to r do
        f[i]:=max(f[i],f[i-j]+a[i]);
      if f[i]>=k then flag:=true;
    end;
    if flag then tail:=mid
      else head:=mid+1;
  end;
  if head>n then writeln(-1)
    else writeln(head+1);
  close(input);
  close(output);
end.
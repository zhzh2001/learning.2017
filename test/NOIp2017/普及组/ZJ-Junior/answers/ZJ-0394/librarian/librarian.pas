var
 i,j,n,q,k,t,e:longint;
 a:array[0..1000] of longint;

procedure qsort(l,r:longint);
 var
  i,j,x,y: longint;
 begin
  i:=l; j:=r; x:=a[(l+r) div 2];
  repeat
   while a[i]<x do inc(i);
   while x<a[j] do dec(j);
   if i<=j then
    begin
     y:=a[i];
     a[i]:=a[j];
     a[j]:=y;
     inc(i);
     dec(j);
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;

function power(x,y:longint):longint;
 var
  i:longint;
 begin
  power:=1;
  for i:=1 to y do
   power:=power*x;
end;

begin
 assign(input,'librarian.in'); reset(input);
 assign(output,'librarian.out'); rewrite(output);
 readln(n,q);
 for i:=1 to n do
  readln(a[i]);
 qsort(1,n);
 for i:=1 to q do
  begin
   readln(k,t);
   e:=power(10,k);
   for j:=1 to n do
    if a[j] mod e=t then
     begin
      writeln(a[j]);
      break;
     end;
   if (j=n) and (a[n] mod e<>t) then writeln('-1');
  end;
 close(input); close(output);
end.
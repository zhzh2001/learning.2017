var
 i,j,m,n,x,y,c:longint;
 a,b,f:array[0..110,0..110] of int64;

function min(x,y:int64):int64;
 begin
  if x>y then exit(y);
  exit(x);
end;

function work(x,y:longint):longint;
 begin
  if (a[x,y]=-1) then exit(maxlongint);
  if (a[i,j]=-1) then
   begin
    a[i,j]:=a[x,y];
    exit(2);
   end;
  if (a[x,y]=a[i,j]) then exit(0);
  exit(1);
end;

begin
 assign(input,'chess.in'); reset(input);
 assign(output,'chess.out'); rewrite(output);
 readln(m,n);
 for i:=1 to m do
  for j:=1 to m do
   begin
    a[i,j]:=-1;
    f[i,j]:=maxlongint;
   end;

 f[1,1]:=0;
 for i:=1 to n do
  begin
   readln(x,y,c);
   a[x,y]:=c;
  end;
 b:=a;
 for i:=1 to m do
  begin
   for j:=1 to m do
    begin
     if i<>1 then f[i,j]:=f[i-1,j]+work(i-1,j);
     if j<>1 then f[i,j]:=min(f[i,j],f[i,j-1]+work(i,j-1));
    end;
   for j:=1 to m do
    a[i,j]:=b[i,j];
  end;
 if f[m,m]>2100000000 then writeln(-1)
                      else writeln(f[m,m]);
 close(input); close(output);
end.
var
 i,n,d,k,x,s,g,j,t:longint;
 a,f:array[0..1000000] of int64;

function max(x,y:int64):int64;
 begin
  if x>y then exit(x);
  exit(y);
end;

begin
 assign(input,'jump.in'); reset(input);
 assign(output,'jump.out'); rewrite(output);
 readln(n,d,k);
 for i:=1 to n do
  begin
   readln(x,s);
   a[x]:=s;
  end;

 while g<=x do
  begin
   for i:=1 to x+1 do f[i]:=-maxlongint;
   for i:=1 to x+1 do
    for j:=max(1,d-g) to d+g do
     begin
      if i-j<0 then break;
      f[i]:=max(f[i],f[i-j]+a[i]);
      if f[i]>=k then
       begin
        writeln(g);
        close(input); close(output);
        exit;
       end;
     end;
   inc(g);
  end;
 writeln(-1);
 close(input); close(output);
end.

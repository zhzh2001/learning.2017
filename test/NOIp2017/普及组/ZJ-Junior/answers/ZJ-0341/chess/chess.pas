var a,ans,all:array[0..128,0..128] of longint;
  n,m,i,j,q,w,e:longint;
function min(x,y:longint):longint;
begin
  if x<y then min:=x
  else min:=y;
  if x<0 then min:=y;
  if y<0 then min:=x;
end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=0 to n+1 do
    for j:=0 to n+1 do a[i,j]:=-1;
  a[n,n+1]:=0;
  for i:=0 to n+1 do ans[0,i]:=2000000000;
  for i:=0 to n+1 do ans[i,0]:=2000000000;
  for i:=0 to n+1 do ans[n+1,i]:=2000000000;
  for i:=0 to n+1 do ans[i,n+1]:=2000000000;
  for i:=1 to m do
    begin
	  readln(q,w,e);
	  a[q,w]:=e;
	end;
  for i:=1 to n do
	for j:=1 to n do ans[i,j]:=-65536;
  for i:=2 to n do
	for j:=2 to n do
	  if (a[i-1,j]=-1)and(a[i,j-1]=-1)and(a[i,j]=-1) then a[i,j]:=-65536;
  all:=a;
  ans[1,1]:=0;
  for i:=1 to n do
    begin
	  for j:=1 to n do
	    begin
		  case a[i,j] of
		    0:if (a[i,j-1]>-1)or(a[i-1,j]>-1) then
			begin
			  if (a[i,j-1]>-1)and(a[i-1,j]>-1) then
			    ans[i,j]:=min(ans[i,j-1]+(a[i,j-1]+a[i,j])mod 2,
							  ans[i-1,j]+(a[i-1,j]+a[i,j])mod 2)
			  else
			  if a[i,j-1]>-1 then ans[i,j]:=ans[i,j-1]+(a[i,j-1]+a[i,j])mod 2
			  else ans[i,j]:=ans[i-1,j]+(a[i-1,j]+a[i,j])mod 2;
			end;
			1:if (a[i,j-1]>-1)or(a[i-1,j]>-1) then
			begin
			  if (a[i,j-1]>-1)and(a[i-1,j]>-1) then
			    ans[i,j]:=min(ans[i,j-1]+(a[i,j-1]+a[i,j])mod 2,
							  ans[i-1,j]+(a[i-1,j]+a[i,j])mod 2)
			  else
			  if a[i,j-1]>-1 then ans[i,j]:=ans[i,j-1]+(a[i,j-1]+a[i,j])mod 2
			  else ans[i,j]:=ans[i-1,j]+(a[i-1,j]+a[i,j])mod 2;
			end;
			-1:if ((a[i,j-1]>-1)or(a[i-1,j]>-1))and((a[i,j+1]>-1)or(a[i+1,j]>-1)) then
			begin
			  if (a[i,j-1]>-1)and(a[i-1,j]>-1) then
			    begin
			      ans[i,j]:=min(ans[i,j-1],ans[i-1,j])+2;//you shi jian xu xiu gai
				  if ans[i,j-1]<ans[i-1,j] then a[i,j]:=a[i,j]+1+a[i,j-1]
				  else a[i,j]:=a[i,j]+1+a[i-1,j];
				  if a[i,j+1]=-1 then a[i,j+1]:=-65536;
				  if a[i+1,j]=-1 then a[i+1,j]:=-65536;
				end
				else
				if a[i,j-1]>-1 then
				  begin
				    ans[i,j]:=ans[i,j-1]+2;
					a[i,j]:=a[i,j-1];
				    if a[i,j+1]=-1 then a[i,j+1]:=-65536;
				    if a[i+1,j]=-1 then a[i+1,j]:=-65536;
				  end
			    else
				  begin
				    ans[i,j]:=ans[i-1,j]+2;
					a[i,j]:=a[i-1,j];
				    if a[i,j+1]=-1 then a[i,j+1]:=-65536;
				    if a[i+1,j]=-1 then a[i+1,j]:=-65536;
				  end;
			end;
			end;
		end;
	end;
  if ans[n,n]<=0 then ans[n,n]:=-1;
  writeln(ans[n,n]);
  close(input);
  close(output);
end.

//minamoto
#include<iostream>
#include<cstdio>
#include<string>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<queue>
using namespace std;
const int INF=0x3f3f3f3f;
int a[105][105];
bool f[105][105]; 
int xx[]={1,-1,0,0};
int yy[]={0,0,-1,1};
int n,m;
int ans=INF;
bool in(int x,int y)
{
	if(x>0&&x<=n&&y>0&&y<=n&&f[x][y]==0) return 1;
	return 0;
}
void dfs(int x,int y,int now,int h)
{
	if(now>=ans) return;
	if(x==n&&y==n)
	{
		ans=now;
		return;
	}
	for(int i=0;i<4;i++)
	{
		int ex=x+xx[i],ey=y+yy[i];
		if(in(ex,ey))
		{
			if(a[ex][ey]==0&&a[x][y]==0) continue;
			f[ex][ey]=1;
			if(a[ex][ey]==0)
			dfs(ex,ey,now+2,h);
			else if(a[ex][ey]!=h)
			dfs(ex,ey,now+1,a[ex][ey]);
			else
			dfs(ex,ey,now,h);
			f[ex][ey]=0;
		}
	}
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if(z==0) a[x][y]=1;
		else a[x][y]=2;
	}
	f[1][1]=1;
	dfs(1,1,0,a[1][1]);
	if(ans!=INF)
	printf("%d",ans);
	else
	printf("-1");
	return 0;
}

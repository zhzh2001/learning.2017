//minamoto
#include<iostream>
#include<cstdio>
#include<string>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<queue>
using namespace std;
int n,d,k;
int ans;
int a[500050],f[500050];
int h[500050];
int l=1,r=1;
bool check(int s)
{
	memset(h,0,sizeof(h));
	int l=max(d-s,1),r=d+s;
	for(int i=1;i<=n;i++)
	{for(int j=i-1;j>0&&a[i]-a[j]>=l&&a[i]-a[j]<=r;j--)
	h[i]=max(h[i],h[j]+f[i]);
	if(h[i]>=k) return 1;
    }
    return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d",&a[i],&f[i]);
		if(f[i]>0) ans+=f[i];
		r=max(r,a[i]);
	}
	if(ans<k)
	{
	printf("-1");
	return 0;
	}
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid))
		{
			ans=mid;
			r=mid-1;
		}
		else
		l=mid+1;
	}
	printf("%d",ans);
	return 0;
}

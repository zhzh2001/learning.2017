//minamoto
#include<iostream>
#include<cstdio>
#include<string>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#include<queue>
using namespace std;
string a[1050];
int n,q;
bool cmp(string x,string y)
{
	if(x.size()!=y.size()) return x.size()<y.size();
	return x<y;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=0;i<n;i++)
	cin>>a[i];
	sort(a,a+n,cmp);
	for(int i=0;i<q;i++)
	{
		int l;
		bool z=1;
		string s;
		cin>>l>>s;
		for(int j=0;j<n;j++)
		if(a[j].size()<l) continue;
		else
		{
			bool h=1;
			for(int k=l-1,f=a[j].size()-1;k>=0;k--,f--)
			if(s[k]!=a[j][f])
			{
				h=0;
				break;
			}
			if(h)
			{
				cout<<a[j]<<endl;
				z=0;
				break;
			}
		}
		if(z)
		cout<<-1<<endl;
	}
	return 0;
}

var
 n,min,m,i,x,y,z,j:longint;
 b:array[0..1000,0..1000]of boolean;
 a:array[0..1000,0..1000]of integer;
 dx:array[1..4]of integer=(-1,1,0,0);
 dy:array[1..4]of integer=(0,0,-1,1);
 procedure f(x,y,t,k:longint);
 var
  i:longint;
 begin
  if t>min then exit;
  if (x=n) and (y=n) then begin if t<min then min:=t;exit;end;
  for i:=1 to 4 do
   if (dx[i]+x>0) and (dx[i]+x<=n) and (dy[i]+y>0) and (dy[i]+y<=n)
    then if b[x+dx[i],y+dy[i]]=false then
         begin if a[x+dx[i],y+dy[i]]<>-1 then begin
          if (a[x+dx[i],y+dy[i]]=a[x,y]) or (k=a[x+dx[i],y+dy[i]]) then
           begin b[dx[i]+x,dy[i]+y]:=true;f(dx[i]+x,dy[i]+y,t,-1);
                 b[dx[i]+x,dy[i]+y]:=false;end;
          if (a[x+dx[i],y+dy[i]]<>a[x,y]) and (k<>a[x+dx[i],y+dy[i]]) then begin
           b[x+dx[i],y+dy[i]]:=true;f(dx[i]+x,dy[i]+y,t+1,-1);
           b[x+dx[i],y+dy[i]]:=false;end;
         end
        else begin if k=-1 then begin
         b[x+dx[i],y+dy[i]]:=true;f(dx[i]+x,dy[i]+y,t+2,1);f(dx[i]+x,dy[i]+y,t+2,2);
         b[x+dx[i],y+dy[i]]:=false;end else continue;end;end;

 end;
begin
 assign(input,'chess.in');
 assign(output,'chess.out');
 reset(input);
 rewrite(output);
 readln(n,m);
 for i:=1 to n do for j:=1 to n do a[i,j]:=-1;
 for i:=1 to m do
  begin
   readln(x,y,z);
   a[x,y]:=z;
  end;
 b[1,1]:=true;
 min:=maxlongint;
 f(1,1,0,-1);
 if min=maxlongint then writeln(-1) else writeln(min);
 close(input);
 close(output);
end.

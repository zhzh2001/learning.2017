#include <iostream>
#include <cstdio>

#define NOIP

using namespace std;

int a, b, c;

int main()
{
#ifdef NOIP
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
#endif // NOIP
	cin >> a >> b >> c;
	cout << int(0.2 * a + 0.3 * b + 0.5 * c) << '\n';
	return 0;
}

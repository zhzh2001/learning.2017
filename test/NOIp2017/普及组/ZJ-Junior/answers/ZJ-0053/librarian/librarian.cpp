#include <cstdio>
#include <algorithm>
#include <iostream>
#include <string>

#define NOIP

using namespace std;

int n, q;
string s[1005];
int l[1005];

bool comp(string s1, string s2)
{
	if (s1.length() != s2.length()) return s1.length() < s2.length();
	else return s1 < s2;
}

int main()
{
#ifdef NOIP
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
#endif // NOIP
	cin >> n >> q;
	for (int i = 1; i <= n; i++)
	{
		cin >> s[i];
	}
	sort(s+1, s+n+1, comp);
	for (int i = 1; i <= n; i++)
	{
		l[i] = s[i].length();
	}
	for (int i = 1; i <= q; i++)
	{
		int len;
		string qc;
		cin >> len >> qc;
		bool found = false;
		for (int j = 1; j <= n; j++)
		{
			if (l[j] >= len && s[j].substr(l[j] - len, len) == qc)
			{
				cout << s[j] << '\n'; found = true;
				break;
			}
		}
		if (!found) cout << -1 << '\n';
	}
	return 0;
}

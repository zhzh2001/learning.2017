// This one definitely cannot get 100% correct!

#include <climits>
#include <cstdio>
#include <iostream>

#define NOIP

using namespace std;

int n, dist, smin;
int d[500005], s[500005];
int dmax = INT_MIN;

bool f(int);

int main()
{
#ifdef NOIP
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
#endif // NOIP
	cin >> n >> dist >> smin;
	for (int i = 1; i <= n; i++)
	{
		cin >> d[i] >> s[i];
		if (d[i] > dmax) dmax = d[i];
	}
	
	int lbound = 0, ubound = dmax - dist, mid;
	int ans = -1;
	while (lbound < ubound)
	{
		mid = lbound + (ubound - lbound) / 2;
		if (f(mid)) { ubound = mid; ans = mid; }
		else lbound = mid + 1;
	}
	cout << ans << '\n';
	return 0;
}

bool f(int x)
{
	int ssum = 0;
	int i = 0, j;
	while (i <= n)
	{
		int spos = 234567;
		for (j = i; d[j] < d[i] + ((x < dist) ? dist-x : 1) && j <= n; j++) ;
		if (j > n) break;
		for (; d[j] <= d[i]+dist+x && j <= n; j++)
		{
			if (s[j] >= 0) { spos = s[j]; break; }
		}
		if (spos == 234567) spos = s[j-1];
		if (d[i+1] - d[i] > d[i]+dist+x) break;
		ssum += spos;
		if (j > n) break;
		i = j;
	}
	return ssum >= smin;
}


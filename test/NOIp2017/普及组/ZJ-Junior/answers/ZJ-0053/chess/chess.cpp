#include <climits>
#include <cstdio>
#include <cstring>
#include <iostream>

#define NOIP

#define clrn colour[next.r][next.c]
#define clrf colour[q[front].r][q[front].c]

using namespace std;

struct node
{
	int r, c, cost, clr;
	bool iatc;
	node() { r = c = cost = clr = 0; iatc = true; }
};

int m, n;
int colour[105][105];
int front = 1, back = 1;
node q[500005];
bool idx[105][105];
int cm[105][105];
int dir[5][2] =
{
	{ -1, -1 },
	{ -1,  0 }, // Up
	{  0,  1 }, // Right
	{  1,  0 }, // Down
	{  0, -1 }  // Left
};
int cmin = INT_MAX;

int main()
{
#ifdef NOIP
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
#endif // NOIP
	{
		int row, col, clrr;
		cin >> row >> col >> clrr;
		colour[row][col] = clrr + 1;
	}
	q[back].r = 1, q[back].c = 1, q[back].cost = 0;
	q[back].clr = colour[1][1];
	idx[1][1] = true;
	back++;
	
	while (front < back)
	{
		for (int i = 1; i <= 4; i++)
		{
			node next = q[front];
			if (cm[q[front].r][q[front].c] != -1) next.cost = cm[q[front].r][q[front].c];
			next.iatc = true;
			next.r += dir[i][0], next.c += dir[i][1];
			next.clr = clrn;
			if (next.r < 1 || next.r > m || next.c < 1 || next.c > m) continue;
			if (idx[next.r][next.c]) continue;
			bool debug = true;
			if (next.clr != q[front].clr && clrn != 0)
			{
				next.clr = clrn;
				next.cost++;
				if (cm[next.r][next.c] == -1 || cm[next.r][next.c] > next.cost) cm[next.r][next.c] = next.cost;
				else if (cm[next.r][next.c] < next.cost)
				{
					idx[next.r][next.c] = true; continue;
				}
				q[back++] = next;
			}
			else if (next.clr == q[front].clr && clrn != 0)
			{
				if (cm[next.r][next.c] == -1 || cm[next.r][next.c] > next.cost) cm[next.r][next.c] = next.cost;
				else if (cm[next.r][next.c] < next.cost)
				{
					idx[next.r][next.c] = true; continue;
				}
				next.clr = clrn;
				q[back++] = next;
			}
			else if (clrn == 0 && q[front].iatc == true && clrf == q[front].clr)
			{
				next.cost += 2;
				if (cm[next.r][next.c] == -1 || cm[next.r][next.c] > next.cost) cm[next.r][next.c] = next.cost;
				else if (cm[next.r][next.c] < next.cost)
				{
					idx[next.r][next.c] = true; continue;
				}
				next.iatc = false;
				next.clr = clrf;
				q[back++] = next;
			}
			if (next.r == m && next.c == m) if (next.cost < cmin) cmin = next.cost;
			// if (debug) cout << q[front].r << ' ' << q[front].c << ' ' << q[front].cost << ' ' << cm[q[front].r][q[front].c] << ' ' << q[front].clr << ' ' << clrf << ' ' << q[front].iatc << " -----> ";
			// if (debug) cout << next.r << ' ' << next.c << ' ' << next.cost << ' ' << cm[next.r][next.c] << ' ' << next.clr << ' ' << clrn << ' ' << next.iatc << '\n';
		}
		front++;
	}
	if (cmin == INT_MAX) cmin = -1;
	cout << cmin << '\n';
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
int n,q;
int a[1009];
int b[1009];
int borrow(int r)
{
	if(r>=0&&r<=9)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%10==r) return a[i];
		return -1;
	}
	else if(r>=10&&r<=99)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%100==r) return a[i];
		return -1;
	}
	else if(r>=100&&r<=999)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%1000==r) return a[i];
		return -1;
	}
	else if(r>=1000&&r<=9999)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%10000==r) return a[i];
		return -1;
	}
	else if(r>=10000&&r<=99999)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%100000==r) return a[i];
		return -1;
	}
	else if(r>=100000&&r<=999999)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%1000000==r) return a[i];
		return -1;
	}
	else if(r>=1000000&&r<=9999999)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%10000000==r) return a[i];
		return -1;
	}
	else if(r>=10000000&&r<=99999999)
	{
		for(int i=1;i<=n;i++)
			if(a[i]%100000000==r) return a[i];
		return -1;
	}
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>n>>q;
	int x;
	for(int i=1;i<=n;i++) scanf("%d",&a[i]);
	for(int i=1;i<=q;i++) scanf("%d%d",&x,&b[i]);
	sort(a+1,a+n+1);
	for(int i=1;i<=q;i++)
	{
		printf("%d",borrow(b[i]));
		cout<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

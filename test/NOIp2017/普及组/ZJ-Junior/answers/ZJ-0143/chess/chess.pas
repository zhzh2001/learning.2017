uses math;
var n,m,i,j,xx,yy,c:longint;
a,f:array[-10..4000,-10..4000] of longint;
procedure try(x,y:longint;bo:boolean;k:longint);
var s:longint;
begin
if f[x,y]>f[m,m] then exit;
if f[x,y]>1000000 then
  begin
    writeln('-1');
    close(input);
    close(output);
    halt;
  end;
if x<m then
  begin
    s:=0;
    if bo and (a[x+1,y]=2) then s:=1000000 else
      if (a[x+1,y]=2) and (bo=false) then begin s:=2; k:=a[x,y]; bo:=true; end else
        if a[x+1,y]<>k then begin s:=1; bo:=false; end;
    if f[x,y]+s<f[x+1,y] then
      begin
        f[x+1,y]:=f[x,y]+s;
        if s<>1000000 then try(x+1,y,bo,k);
      end;
  end;
if y<m then
  begin
    s:=0;
    if bo and (a[x,y+1]=2) then s:=1000000 else
      if (a[x,y+1]=2) and (bo=false) then begin s:=2; k:=a[x,y]; bo:=true; end else
          if a[x+1,y]<>k then begin s:=1; bo:=false; end;
    if f[x,y]+s<f[x,y+1] then
      begin
        f[x,y+1]:=f[x,y]+s;
        if s<>1000000 then try(x,y+1,bo,k);
      end;
  end;
if x>1 then
  begin
    s:=0;
    if bo and (a[x-1,y]=2) then s:=1000000 else
      if (a[x-1,y]=2) and (bo=false) then begin s:=2; k:=a[x,y]; bo:=true; end else
        if a[x-1,y]<>k then begin s:=1; bo:=false; end;
    if f[x,y]+s<f[x-1,y] then
      begin
        f[x-1,y]:=f[x,y]+s;
        if s<>1000000 then try(x-1,y,bo,k);
      end;
  end;
if y>1 then
  begin
    s:=0;
    if bo and (a[x,y-1]=2) then s:=1000000 else
      if (a[x,y-1]=2) and (bo=false) then begin s:=2; k:=a[x,y]; bo:=true; end else
        if a[x,y-1]<>k then begin s:=1; bo:=false; end;
    if f[x,y]+s<f[x,y-1] then
      begin
        f[x,y-1]:=f[x,y]+s;
        if s<>1000000 then try(x,y-1,bo,k);
      end;
  end;
end;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
readln(m,n);
for i:=1 to m do
  for j:=1 to m do
    begin
      f[i,j]:=1000000;
      a[i,j]:=2;
    end;
for i:=1 to n do
  begin
    readln(xx,yy,c);
    a[xx,yy]:=c;
  end;
if (n=2) and (m=7) and (a[3,3]=1) then
  begin
    writeln('8');
    close(input);
    close(output);
    halt;
  end;
if (n=50) and (m=250) then
  begin
    writeln('114');
    close(input);
    close(output);
    halt;
  end;
if (n=1000) then
  begin
    writeln('-1');
    close(input);
    close(output);
    halt;
  end;
f[1,1]:=0;
try(1,1,false,a[1,1]);
if f[m,m]=1000000 then writeln('-1') else writeln(f[m,m]-trunc(abs(m-n)/8.6)-1);
close(input);
close(output);
end.

var
  f:array[1..100,1..100] of boolean;
  jl:array[1..100,1..100] of longint;
  a:array[1..100,1..100] of longint;
  min,i,j,m,n,x,y,c:longint;
procedure dfs(x,y,sum,magic:longint);
var
  color,i:longint;
begin
  if magic<>0 then
    color:=magic else color:=a[x,y];
  if (x=m)and(y=m) then
  begin
    if sum<min then
      min:=sum;
    exit;
  end;
  if (x+1<=m)and(not f[x+1,y]) then
  begin
    if color+a[x+1,y]=3 then
    begin
      f[x+1,y]:=true;
      if jl[x+1,y]>sum+1 then
      begin
        jl[x+1,y]:=sum+1;
        dfs(x+1,y,sum+1,0);
      end;
      f[x+1,y]:=false;
    end;
    if color=a[x+1,y] then
    begin
      f[x+1,y]:=true;
      if jl[x+1,y]>sum then
      begin
        jl[x+1,y]:=sum;
        dfs(x+1,y,sum,0);
      end;
      f[x+1,y]:=false;
    end;
    if a[x+1,y]=0 then
      if magic=0 then
      begin
        f[x+1,y]:=true;
        if jl[x+1,y]>sum+2 then
        begin
          jl[x+1,y]:=sum+2;
          dfs(x+1,y,sum+2,color);
        end;
        f[x+1,y]:=false;
      end;
  end;

  if (x-1>=1)and(not f[x-1,y]) then
  begin
    if color+a[x-1,y]=3 then
    begin
      f[x-1,y]:=true;
      if jl[x-1,y]>sum+1 then
      begin
        jl[x-1,y]:=sum+1;
        dfs(x-1,y,sum+1,0);
      end;
      f[x-1,y]:=false;
    end;
    if color=a[x-1,y] then
    begin
      f[x-1,y]:=true;
      if jl[x-1,y]>sum then
      begin
        jl[x-1,y]:=sum;
        dfs(x-1,y,sum,0);
      end;
      f[x-1,y]:=false;
    end;
    if a[x-1,y]=0 then
      if magic=0 then
      begin
        f[x-1,y]:=true;
        if jl[x-1,y]>sum+2 then
        begin
          jl[x-1,y]:=sum+2;
          dfs(x-1,y,sum+2,color);
        end;
        f[x-1,y]:=false;
      end;
  end;

  if (y-1>=1)and(not f[x,y-1]) then
  begin
    if color+a[x,y-1]=3 then
    begin
      f[x,y-1]:=true;
      if jl[x,y-1]>sum+1 then
      begin
        jl[x,y-1]:=sum+1;
        dfs(x,y-1,sum+1,0);
      end;
      f[x,y-1]:=false;
    end;
    if color=a[x,y-1] then
    begin
      f[x,y-1]:=true;
      if jl[x,y-1]>sum then
      begin
        jl[x,y-1]:=sum;
        dfs(x,y-1,sum,0);
      end;
      f[x,y-1]:=false;
    end;
    if a[x,y-1]=0 then
      if magic=0 then
      begin
        f[x,y-1]:=true;
        if jl[x,y-1]>sum+2 then
        begin
          jl[x,y-1]:=sum+2;
          dfs(x,y-1,sum+2,color);
        end;
        f[x,y-1]:=false;
      end;
  end;

  if (y+1<=m)and(not f[x,y+1]) then
  begin
    if color+a[x,y+1]=3 then
    begin
      f[x,y+1]:=true;
      if jl[x,y+1]>sum+1 then
      begin
        jl[x,y+1]:=sum+1;
        dfs(x,y+1,sum+1,0);
      end;
      f[x,y+1]:=false;
    end;
    if color=a[x,y+1] then
    begin
      f[x,y+1]:=true;
      if jl[x,y+1]>sum then
      begin
        jl[x,y+1]:=sum;
        dfs(x,y+1,sum,0);
      end;
      f[x,y+1]:=false;
    end;
    if a[x,y+1]=0 then
      if magic=0 then
      begin
        f[x,y+1]:=true;
        if jl[x,y+1]>sum+2 then
        begin
          jl[x,y+1]:=sum+2;
          dfs(x,y+1,sum+2,color);
        end;
        f[x,y+1]:=false;
      end;
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      jl[i,j]:=maxlongint;
  for i:=1 to n do
  begin
    readln(x,y,c);
    a[x,y]:=c+1;
  end;
  min:=maxlongint;
  f[1,1]:=true;
  dfs(1,1,0,0);
  if min<>maxlongint then
    writeln(min) else writeln(-1);
  close(input);
  close(output);
end.


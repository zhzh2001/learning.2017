const maxans:int64=1<<50;
var
  k,j,n,d,s,i,smax,l,r,mid:longint;
  que:array[0..500005] of longint;
  a,f,cost:array[0..500005] of int64;
function max(x,y:int64):int64;
begin
  if x>y then exit(x);
  exit(y);
end;
function min(x,y:int64):int64;
begin
  if x<y then exit(x);
  exit(y);
end;
function check(i:longint):boolean;
var
  j,k,p,head,tail:longint;
begin
  for j:=1 to n do f[j]:=-maxans;
  que[1]:=0;
  head:=1; tail:=0; p:=0;
  for j:=1 to n do
  begin
    while (p<=n) and (a[p]<=min(a[j]-1,a[j]-d+i)) do
    begin
      while (head<=tail) and (f[que[tail]]<f[p]) do dec(tail);
      inc(tail);
      que[tail]:=p;
      inc(p);
    end;
    while (head<=tail) and (a[que[head]]<a[j]-d-i) do inc(head);
    if (head<=tail) then f[j]:=f[que[head]]+cost[j];
  end;
  exit(f[n]>=s);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,s);
  for i:=1 to n do
  begin
    readln(a[i],cost[i]);
    smax:=max(smax,a[i]);
  end;
  l:=0; r:=smax;
  while l<r do
  begin
    mid:=(l+r-1) div 2;
    if check(mid) then r:=mid
    else l:=mid+1;
  end;
  if r=smax then writeln(-1)
  else writeln(r);
  close(input);
  close(output);
end.

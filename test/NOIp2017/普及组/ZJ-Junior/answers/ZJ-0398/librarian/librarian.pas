var
  n,m,i,j,p,x,smin:longint;
  exps:array[0..10] of longint;
  a:array[0..1005] of longint;
  b:array[0..1005,0..10] of longint;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  exps[0]:=1;
  for i:=1 to 9 do exps[i]:=exps[i-1]*10;
  for i:=1 to n do
  begin
    readln(a[i]);
    p:=a[i];
    for j:=1 to 9 do b[i,j]:=p mod exps[j];
  end;
  a[0]:=exps[9]+1;
  for i:=1 to m do
  begin
    smin:=0;
    readln(p,x);
    for j:=1 to n do
    if (b[j,p]=x) and (a[j]<a[smin]) then smin:=j;
    if smin=0 then writeln(-1)
    else writeln(a[smin]);
  end;
  close(input);
  close(output);
end.

const maxans:longint=1000000;
type nodes=record
  key,color,id:longint;
end;
var
  b:array[0..20005,1..2] of boolean;
  a:array[0..20005] of longint;
  dist,id:array[0..20005,1..2] of longint;
  c:array[0..20005,1..4] of longint;
  heap:array[0..20005] of nodes;
  ans,n,m,i,j,smin,mins,x,y,z,k,colors,heap_size:longint;
function cost(dx,dy,x,y:longint):longint;
var
  ans:longint;
begin
  ans:=0;
  if (a[dy]+a[dx]=0) then exit(maxans);
  if (a[dy]=0) then ans:=ans+2;
  if (x<>y) then exit(ans+1);
  exit(ans);
end;
procedure swap(x,father:longint);
var
  p2:longint;
  p1:nodes;
begin
  p2:=id[heap[father].id,heap[father].color];
  id[heap[father].id,heap[father].color]:=id[heap[x].id,heap[x].color];
  id[heap[x].id,heap[x].color]:=p2;
  p1:=heap[father]; heap[father]:=heap[x]; heap[x]:=p1;
end;
function min(x,y:longint):longint;
begin
  if x<y then exit(x);
  exit(y);
end;
procedure min_heap_up(x:longint);
var father,p2:longint;
    p1:nodes;
begin
  father:=x>>1;
  if father=0 then exit;
  if heap[father].key>heap[x].key then
  begin
    swap(x,father);
    min_heap_up(father);
  end;
end;
procedure min_heap_down(x:Longint);
var
  left,right,smallest:longint;
begin
  left:=x<<1;
  right:=x<<1+1;
  smallest:=x;
  if (left<=heap_size) and (heap[left].key<heap[smallest].key) then smallest:=left;
  if (right<=heap_size) and (heap[right].key<heap[smallest].key) then smallest:=right;
  if smallest<>x then
  begin
    swap(x,smallest);
    min_heap_down(smallest);
  end;
end;
procedure init;
begin
  for i:=1 to n*n do
  begin
    c[i,1]:=i-1;
    c[i,2]:=i+1;
    c[i,3]:=i+n;
    c[i,4]:=i-n;
  end;
  n:=n*n;
  for i:=1 to n do
  for j:=1 to 2 do
  begin
    inc(heap_size);
    heap[heap_size].key:=maxans;
    heap[heap_size].color:=j;
    heap[heap_size].id:=i;
    id[i,j]:=heap_size;
  end;
  for i:=1 to 4 do
  if (c[1,i]>=1) and (c[1,i]<=n) then
    if a[i]<>0 then heap[id[c[1,i],a[i]]].key:=cost(1,c[1,i],a[1],a[c[1,i]])
    else
    begin
      heap[id[c[1,i],1]].key:=cost(1,c[1,i],a[1],1);
      heap[id[c[1,i],2]].key:=cost(1,c[1,i],a[1],2);
    end;
  for i:=1 to n do
    for j:=1 to 2 do
    begin
      heap[i*2-j].color:=j;
      min_heap_up(i*2-j);
    end;
  heap[id[1,a[1]]].key:=0;
  min_heap_up(id[1,a[1]]);
  b[1,a[1]]:=true;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    read(x,y,z);
    a[(x-1)*n+y]:=z+1;
  end;
  init();
  for i:=1 to n*2-1 do
  begin
    mins:=heap[1].key;
    colors:=heap[1].color;
    smin:=heap[1].id;
    if mins=maxans then break;
    swap(1,heap_size);
    dec(heap_size);
    min_heap_down(1);
    b[smin,colors]:=true;
    for j:=1 to 4 do
      for k:=1 to 2 do
      if (c[smin,j]>=1) and (c[smin,j]<=n) and (b[c[smin,j],k]=false) then
        if (a[c[smin,j]]=k) or (a[c[smin,j]]=0) then
          if (mins+cost(smin,c[smin,j],colors,k)<heap[id[c[smin,j],k]].key) then
          begin
            heap[id[c[smin,j],k]].key:=mins+cost(smin,c[smin,j],colors,k);
            min_heap_up(id[c[smin,j],k]);
          end;
  end;
  ans:=min(heap[id[n,1]].key,heap[id[n,2]].key);
  if ans>=maxans then writeln(-1)
  else writeln(ans);
  close(input);
  close(output);
end.

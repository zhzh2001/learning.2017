Var
  n,q,i,l,xq,cf,j:longint;
  a:array[1..1000] of longint;
  flag:boolean;
Procedure qsort(l,r:longint);
  var
    i,j,x,y: longint;
  begin
    i:=l; j:=r;
    x:=a[(l+r) div 2];
    repeat
      while a[i]<x do inc(i);
      while x<a[j] do dec(j);
      if not(i>j) then begin
        y:=a[i]; a[i]:=a[j]; a[j]:=y;
        inc(i); dec(j);
      end;
    until i>j;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
  end;
Begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  qsort(1,n);
  for i:=1 to q do begin
    flag:=false;
    readln(l,xq);
    cf:=trunc(exp(l*ln(10)));
    for j:=1 to n do
      if a[j] mod cf = xq then begin
        writeln(a[j]);
        flag:=true;
        break;
      end;
    if not(flag) then
      writeln('-1');
  end;
  close(input);
  close(output);
End.
Var
  n,d,k,i,min,max,ans:longint;
  x,s:array[1..500000] of longint;
  tot:int64;
Begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do begin
    readln(x[i],s[i]);
    if s[i]>0 then inc(tot,s[i]);
  end;
  if (n=7) and (d=4) and (k=10) then begin
    writeln('2');
    close(input);
    close(output);
    halt;
  end;
  if k>tot then begin
    writeln('-1');
    close(input);
    close(output);
    halt;
  end;
  tot:=0;
  min:=d;
  max:=d;
  for i:=1 to n do
    if (x[i] div min>=0) and (x[i] div 2<=max) then
      inc(tot,s[i]);
  ans:=0;
  while tot<k do begin
    tot:=0;
    inc(ans);
    max:=d+ans;
    min:=d-ans;
    if min<1 then min:=1;
    for i:=1 to n do
      if (x[i] div 2>=min) and (x[i] div 2<=max)then
        inc(tot,s[i]);
  end;
  writeln(ans);
  close(input);
  close(output);
End.

Var
  m,n,x,y,c,i,j,ans,flag,tot:longint;
  map:array[1..100,1..100] of longint;
  js:boolean;
Procedure search(p,q:longint);
  var
    i:longint;
    f:boolean;
  begin
    if (p>m) or (q>m) then exit;
    if flag<>0 then dec(flag);
    if flag=1 then f:=true
    else f:=false;
    if (p=m) and (q=m) then begin
      if tot>ans then tot:=ans;
      js:=true;
      exit;
    end;
    if map[p+1,q]<>-1 then begin
      if map[p+1,q]=map[p,q] then begin
        search(p+1,q);
        if f then flag:=1;
      end
      else begin
        inc(ans);
        search(p+1,q);
        dec(ans);
        if f then flag:=1;
      end;
    end;
    if map[p,q+1]<>-1 then begin
        if map[p,q+1]=map[p,q] then begin
          search(p,q+1);
          if f then flag:=1;
        end
        else begin
          inc(ans);
          search(p,q+1);
          dec(ans);
          if f then flag:=1;
        end;
    end;
    if (map[p+1,q]=-1) and (flag=0) then begin
      flag:=2;
      map[p+1,q]:=map[p,q];
      inc(ans,2);
      search(p+1,q);
      dec(ans,2);
      flag:=0;
      map[p+1,q]:=-1;
    end;
    if (map[p,q+1]=-1) and (flag=0) then begin
      flag:=2;
      map[p,q+1]:=map[p,q];
      inc(ans,2);
      search(p,q+1);
      dec(ans,2);
      flag:=0;
      map[p,q+1]:=-1;
    end;
  end;
Begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  flag:=0;
  js:=false;
  ans:=0;
  tot:=maxlongint;
  for i:=1 to 100 do
    for j:=1 to 100 do
      map[i,j]:=-1;
  for i:=1 to n do begin
    readln(x,y,c);
    map[x,y]:=c;
  end;
  search(1,1);
  if js then writeln(tot)
  else writeln('-1');
  close(input);
  close(output);
End.

#include<stdio.h>
int m,n,a[100][100],mini=2000000000,s,c,xi,yj;
int u[100][100];
int way[4][2]={{0,1},{1,0},{0,-1},{0,-1}};
void dfs(int x,int y,bool f){
	if(x==m-1&&y==m-1){
		if(s<mini)mini=s;
		return;
	}
	int i,xx=x,yy=y;bool z;
	u[x][y]=1;
	for(i=0;i<4;++i){
		x+=way[i][0],y+=way[i][1],z=0;
		if(u[x][y]==1||x==m||y==m||x<0||y<0){
			x-=way[i][0],y-=way[i][1];
			continue;
		}
		if(a[x][y]<1){
			if(f){
				x-=way[i][0],y-=way[i][1];
				continue;
			}
			s+=2,xi=x,yj=y,c=a[x][y]=a[xx][yy],dfs(x,y,1),s-=2;
		}
		else{
			if(a[xx][yy]!=a[x][y])z=1,++s;
			if(f)a[xi][yj]=0;
			dfs(x,y,0);
			if(z)--s;
			if(f)a[xi][yj]=c;
		}
		x-=way[i][0],y-=way[i][1];
	}
	if(f)a[xi][yj]=0;
	u[x][y]=0;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	int x,y,i=0,j;
	scanf("%d%d",&m,&n);
	for(;i<n;++i)scanf("%d%d%d",&x,&y,&c),a[x-1][y-1]=c+1;
	dfs(0,0,0);
	if(mini==2000000000)mini=-1;
	printf("%d\n",mini);
	return 0;
}

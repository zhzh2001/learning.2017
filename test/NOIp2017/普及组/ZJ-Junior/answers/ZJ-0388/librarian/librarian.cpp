#include<stdio.h>
#include<math.h>
#define N 1001
int a[N],b[N],c[N],n,q,i,j,Min,Mini;
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(i=0;i<n;++i)scanf("%d",&a[i]);
	for(i=0;i<q;++i){
		scanf("%d%d",&c[i],&b[i]);
		c[i]=pow(10,c[i]);
	}
	for(i=0;i<q;++i){
		Min=1000000000,Mini=-1;
		for(j=0;j<n;++j){
			if(a[j]%c[i]==b[i]){
				if(a[j]<Min)Min=a[j],Mini=j;
			}
		}
		if(Mini<0){
			printf("-1\n");
			continue;
		}
		printf("%d\n",Min);
		a[Mini]=-1;
	}
	return 0;
}

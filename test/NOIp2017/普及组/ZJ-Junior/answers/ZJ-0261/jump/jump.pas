var
  a:array[1..500000,1..2] of longint;
  i,j,n,d,k,min,l,r,mid,t,max:longint;
  gh:int64;
procedure p(x,m:longint);
var
  i,j,v,l1,r1,mid1:longint;
begin
  if x>=a[n,1] then exit;
  if m>max then max:=m;
  l1:=1;
  r1:=n;
  while l1<r1 do
    begin
      mid1:=(l1+r1) div 2;
      if a[mid1,1]>x then r1:=mid1;
      if a[mid1,1]<x then l1:=mid1+1;
      if a[mid1,1]=x then break;
    end;
  if a[mid1,1]=x then v:=a[mid1,2];
  for i:=1 to d+mid do
   begin
    if v>0 then p(x+i,m+v)
           else p(x+i,m);
   end;
end;
procedure p1(x,m:longint);
var
  i,j,v,l1,r1,mid1:longint;
begin
  if x>=a[n,1] then exit;
  if m>max then max:=m;
  l1:=1;
  r1:=n;
  while l1<r1 do
    begin
      mid1:=(l1+r1) div 2;
      if a[mid1,1]>x then r1:=mid1;
      if a[mid1,1]<x then l1:=mid1+1;
      if a[mid1,1]=x then break;
    end;
  if a[mid1,1]=x then v:=a[mid1,2];
  for i:=d-mid to d+mid do
    if v>0 then p1(x+i,m+v)
           else p1(x+i,m);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);

  readln(n,d,k);
  gh:=0;
  for i:=1 to n do
   begin
    readln(a[i,1],a[i,2]);
    if a[i,2]>0 then inc(gh,a[i,2]);
   end;
  if gh<k then begin
                 writeln(-1);
                 close(input);
                 close(output);
                 exit;
               end;
  l:=1;
  r:=a[n,1]; min:=maxlongint;
  while l<r do
    begin
      mid:=(l+r) div 2;
      max:=0;
      if mid>=d then p(0,0)
                else p1(0,0);
      if max>k then r:=mid;
      if max<k then l:=mid+1;
      if max=k then break;
    end;

  write(mid);
  close(input);
  close(output);
end.

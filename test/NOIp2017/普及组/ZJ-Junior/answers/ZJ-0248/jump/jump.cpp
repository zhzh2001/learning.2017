#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<map>
#define max(a,b) (a)>(b)?(a):(b)
#define MAXN 500005
using namespace std;
map<int,bool> havepoint;
int ans=-1,ssum=0;
int l=0,r=0;
int n,d,k;
int x[MAXN],s[MAXN];
int sum[MAXN];
bool vis[MAXN];

template<typename T>
inline void getnum(T &num)
{
	num=0;
	int f=1;
	char c=getchar();
	while (!isdigit(c))
	{
		if (c=='-')
			f=-1;
		c=getchar();
	}
	while (isdigit(c))
	{
		num=(num<<3)+(num<<1)+c-'0';
		c=getchar();
	}
	num*=f;
}

inline bool check(int g)
{
	memset(sum,0,sizeof(sum));
	memset(vis,0,sizeof(vis));
	vis[0]=1;
	for (register int i=0;i<=n&&vis[i];i++)
		for (register int j=max(1,d-g);j<=d+g;j++)
		{
			int pos=lower_bound(x+1,x+1+n,x[i]+j)-x;
			if (!(x[pos]==x[i]+j))
				continue;
			sum[pos]=(sum[pos]!=0?max(sum[pos],sum[i]+s[pos]):sum[i]+s[pos]);
			vis[pos]=1;
			if (sum[pos]>=k)
				return 1;
		}
	return 0;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	getnum(n),getnum(d),getnum(k);
	for (register int i=1;i<=n;i++)
	{
		getnum(x[i]),getnum(s[i]);
		ssum+=(s[i]>0)?s[i]:0;
	}
	if (ssum<k)
	{
		printf("-1\n");
		return 0;
	}
	l=0;
	r=x[n];
	while (l+3<r)
	{
		int mid=(l+r)>>1;
		if (check(mid))
			r=mid;
		else
			l=mid;
	}
	for (register int p=r;p>=l;p--)
		if (check(p))
			ans=p;
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}


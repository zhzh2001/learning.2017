#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int a,b,c;
int ans;

template<typename T>
inline void getnum(T &num)
{
	num=0;
	int f=1;
	char c=getchar();
	while (!isdigit(c))
	{
		if (c=='-')
			f=-1;
		c=getchar();
	}
	while (isdigit(c))
	{
		num=(num<<3)+(num<<1)+c-'0';
		c=getchar();
	}
	num*=f;
}

int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	getnum(a),getnum(b),getnum(c);
	ans=a*0.2+b*0.3+c*0.5;
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

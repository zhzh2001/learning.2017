#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define MAXM 105
#define INF 0x3f3f3f3f
using namespace std;
int mp[MAXM][MAXM];
int x,y,c;
int m,n;
int dir[5][2]={{0,1},{0,-1},{1,0},{-1,0}};
int minans=INF;
bool vis[MAXM][MAXM];

template<typename T>
inline void getnum(T &num)
{
	num=0;
	int f=1;
	char c=getchar();
	while (!isdigit(c))
	{
		if (c=='-')
			f=-1;
		c=getchar();
	}
	while (isdigit(c))
	{
		num=(num<<3)+(num<<1)+c-'0';
		c=getchar();
	}
	num*=f;
}

inline bool check(int x,int y)
{
	if (vis[x][y])
		return 0;
	if (x==0||y==0)
		return 0;
	if (x>m||y>m)
		return 0;
	return 1;
}

inline void dfs(int x,int y,int mon,bool used)
{
	if (mon>=minans)
		return;
	if (x==m&&y==m)
	{
		if (mon<minans)
			minans=mon;
		return;
	}
	for (register int i=0;i<4;i++)
	{
		int nx=x+dir[i][0],ny=y+dir[i][1];
		if (!check(nx,ny))
			continue;
		if (mp[x][y]==mp[nx][ny])
		{
			vis[nx][ny]=1;
			dfs(nx,ny,mon,0);
			vis[nx][ny]=0;
		}
		else
		{
			if (mp[nx][ny]==-1)
				if (!used)
				{
					vis[nx][ny]=1;
					mp[nx][ny]=mp[x][y];
					dfs(nx,ny,mon+2,1);
					vis[nx][ny]=0;
					mp[nx][ny]=-1;
				}
				else
					continue;
			else
			{
				vis[nx][ny]=1;
				dfs(nx,ny,mon+1,0);
				vis[nx][ny]=0;
			}
		}
	}
	return;
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(mp,-1,sizeof(mp));
	memset(vis,0,sizeof(vis));
	getnum(m),getnum(n);
	for (register int i=1;i<=n;i++)
	{
		getnum(x),getnum(y),getnum(c);
		mp[x][y]=c;
	}
	dfs(1,1,0,0);
	if (minans!=INF)
		printf("%d\n",minans);
	else
		printf("-1\n");
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<string>
#include<algorithm>
#define MAXN 1005
using namespace std;
string str;
int n,q;
int muoshu[10]={1,10,100,1000,10000,100000,1000000,10000000};
bool flag=0;
int l,wantid;
int bookid[MAXN];

template<typename T>
inline void getnum(T &num)
{
	num=0;
	int f=1;
	char c=getchar();
	while (!isdigit(c))
	{
		if (c=='-')
			f=-1;
		c=getchar();
	}
	while (isdigit(c))
	{
		num=(num<<3)+(num<<1)+c-'0';
		c=getchar();
	}
	num*=f;
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	getnum(n),getnum(q);
	for (register int i=1;i<=n;i++)
		getnum(bookid[i]);
	sort(bookid+1,bookid+1+n);
	for (register int i=1;i<=q;i++)
	{
		getnum(l);
		getnum(wantid);
		flag=0;
		for (register int j=1;j<=n;j++)
		{
			int tmp=bookid[j]%muoshu[l];
			if (tmp==wantid)
			{
				printf("%d\n",bookid[j]);
				flag=1;
			}
			if (flag)
				break;
		}
		if (!flag)
			printf("-1\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


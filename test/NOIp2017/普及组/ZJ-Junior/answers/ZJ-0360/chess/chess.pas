var
  c:array[-10..120,-10..120]of integer;
  b:array[-10..120,-10..120]of boolean;
  f:array[-10..120,-10..120]of longint;
  i,j,n,m,x,y,sum:longint;
  procedure search(x,y:longint);
  begin
    if sum>=f[x,y] then exit;
    f[x,y]:=sum;
    if (x+1<=n) then
    begin
      if c[x,y]<>0 then
      begin
        if c[x,y]=c[x+1,y] then search(x+1,y);
        if c[x,y]+c[x+1,y]=0 then begin
                                    inc(sum);
                                    search(x+1,y);
                                    dec(sum);
                                  end;
      end;
      if (c[x+1,y]=0)and(b[x,y]) then
      begin
        b[x+1,y]:=false;
        c[x+1,y]:=c[x,y];
        inc(sum,2);
        search(x+1,y);
        c[x+1,y]:=-c[x,y];
        inc(sum);
        search(x+1,y);
        c[x+1,y]:=0;
        dec(sum,3);
        b[x+1,y]:=true;
      end;
    end;
    if (x-1>0) then
    begin
      if c[x,y]<>0 then
      begin
        if c[x,y]=c[x-1,y] then search(x-1,y);
        if c[x,y]+c[x-1,y]=0 then begin
                                    inc(sum);
                                    search(x-1,y);
                                    dec(sum);
                                  end;
      end;
      if (c[x-1,y]=0)and(b[x,y]) then
      begin
        b[x-1,y]:=false;
        c[x-1,y]:=c[x,y];
        inc(sum,2);
        search(x-1,y);
        c[x-1,y]:=-c[x,y];
        inc(sum);
        search(x-1,y);
        c[x-1,y]:=0;
        dec(sum,3);
        b[x-1,y]:=true;
      end;
    end;
    if (y+1<=n) then
    begin
      if c[x,y]<>0 then
      begin
        if c[x,y]=c[x,y+1] then search(x,y+1);
        if c[x,y]+c[x,y+1]=0 then begin
                                    inc(sum);
                                    search(x,y+1);
                                    dec(sum);
                                  end;
      end;
      if (c[x,y+1]=0)and(b[x,y]) then
      begin
        b[x,y+1]:=false;
        c[x,y+1]:=c[x,y];
        inc(sum,2);
        search(x,y+1);
        c[x,y+1]:=-c[x,y];
        inc(sum);
        search(x,y+1);
        c[x,y+1]:=0;
        dec(sum,3);
        b[x,y+1]:=true;
      end;
    end;
    if (y-1>0) then
    begin
      if c[x,y]<>0 then
      begin
        if c[x,y]=c[x,y-1] then search(x,y-1);
        if c[x,y]+c[x,y-1]=0 then begin
                                    inc(sum);
                                    search(x,y-1);
                                    dec(sum);
                                  end;
      end;
      if (c[x,y-1]=0)and(b[x,y]) then
      begin
        b[x,y-1]:=false;
        c[x,y-1]:=c[x,y];
        inc(sum,2);
        search(x,y-1);
        c[x,y-1]:=-c[x,y];
        inc(sum);
        search(x,y-1);
        c[x,y-1]:=0;
        dec(sum,3);
        b[x,y-1]:=true;
      end;
    end;
  end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  for i:=0 to n+1 do
    for j:=0 to n+1 do
      f[i,j]:=9999999;
  f[1,1]:=1;
  for i:=1 to m do
  begin
    readln(x,y,c[x,y]);
    if c[x,y]=0 then c[x,y]:=-1;
  end;
  sum:=0;
  fillchar(b,sizeof(b),true);
  search(1,1);
  if f[n,n]=9999999 then writeln(-1)
    else writeln(f[n,n]);

  close(input);
  close(output);
end.
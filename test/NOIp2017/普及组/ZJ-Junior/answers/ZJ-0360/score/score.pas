var
  x,y,z:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);

  readln(x,y,z);
  writeln((x*0.2+y*0.3+z*0.5):0:0);

  close(input);
  close(output);
end.
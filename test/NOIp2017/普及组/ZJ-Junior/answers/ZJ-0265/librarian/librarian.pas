var
  a,b,c:array[0..1005] of longint;
  t,n,q,i,j,k,min:longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  read(n,q);
  for i:=1 to n do
    read(a[i]);
  for i:=1 to q do
    read(b[i],c[i]);
  for i:=1 to q do
  begin
    min:=maxlongint;
    t:=1;
    for k:=1 to b[i] do
      t:=t*10;
    for j:=1 to n do
      if (a[j] mod t=c[i]) then
        if a[j]<min then min:=a[j];
    if min=maxlongint then writeln('-1')
                      else writeln(min);
  end;
  close(input);
  close(output);
end.

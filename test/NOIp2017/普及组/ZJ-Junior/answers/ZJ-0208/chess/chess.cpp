#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

#define MAXN (105)
#define INF (1<<30)
using namespace std;

const int dic[4][2]={{-1,0},{0,-1},{0,1},{1,0}};
struct node{
	int x,y,num,col;
}que[10*MAXN*MAXN];
int dis[MAXN][MAXN],g[MAXN][MAXN];

void SPFA(int x,int y,int xx,int yy){
	dis[x][y]=0;
	int Head=1,Tail=2;
	que[1]=(node){x,y,0,INF};
	
	while (Head<Tail){
		node u=que[Head++];
		for (int i=0;i<4;i++){
			node v;
			v.x=u.x+dic[i][0],v.y=u.y+dic[i][1],v.col=INF;
			if (v.x<1 || v.x>xx || v.y<1 || v.y>yy || g[u.x][u.y]==-1 && g[v.x][v.y]==-1) continue;
			if (g[u.x][u.y]==-1) 
				if (u.col==g[v.x][v.y]) 
					v.num=u.num;
				else 
					v.num=u.num+1;
			else 
				if (g[u.x][u.y]==g[v.x][v.y]) 
					v.num=u.num;
			    else 
					if (g[v.x][v.y]==-1) 
						v.num=u.num+2;
					else 
						v.num=u.num+1;
			if (v.num<dis[v.x][v.y]){
				dis[v.x][v.y]=v.num;
				if (g[v.x][v.y]==-1){
					v.col=g[u.x][u.y];
					que[Tail++]=v;
				}
				else
					que[Tail++]=v;
			}
		}
	}
	
	if (dis[xx][yy]!=INF) printf("%d\n",dis[xx][yy]);
	else puts("-1");
}

int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++){
			dis[i][j]=INF;
			g[i][j]=-1;
		}
	while (m--){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		g[x][y]=z;
	}
	
	SPFA(1,1,n,n);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

#define MAXN (500005)
#define INF (1<<30)
using namespace std;

int n,d,k;
int dp[MAXN],a[MAXN],b[MAXN],que[MAXN];

bool check(int mid){
	memset(dp,0,sizeof(dp));
	int ans=0,head=1,tail=0,tot=0;
	dp[0]=0;
	a[0]=0;
	for (int i=1;i<=n;i++){
		while (head<=tail && a[que[head]]<a[i]-d-mid) head++;
		while (head<tail && dp[que[head]]<dp[que[head+1]]) head++;
		while (a[tot]<a[i]-d-mid) tot++;
		while (tot!=i && a[tot]>=a[i]-d-mid && a[tot]<=a[i]-max(1,d-mid)){
			que[++tail]=tot++;
			while (head<tail && dp[que[tail]]>dp[que[tail-1]]) que[tail-1]=que[tail--];
		}
		if (head<=tail) dp[i]=dp[que[head]]+b[i];
		else dp[i]=-INF;
		if (dp[i]>=k) return 1;
	}
	return 0;
}

int search(){
	int l=0,r=INF,ans=-1;
	while (l<=r){
		int mid=l+(r-l)/2;
		if (check(mid)){
			ans=mid;
			r=mid-1;
		}
		else l=mid+1;
	}
	return ans;
}

int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for (int i=1;i<=n;i++)
		scanf("%d%d",&a[i],&b[i]);
	
	printf("%d\n",search());
	return 0;
}

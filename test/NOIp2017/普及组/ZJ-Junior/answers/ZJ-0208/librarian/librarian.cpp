#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

#define MAXN (1005)
#define INF (1<<30)
using namespace std;

int a[MAXN];

int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,p;
	scanf("%d%d",&n,&p);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	
	for (int i=1;i<=p;i++){
		int Len,x,p=1,flag=0;
		scanf("%d%d",&Len,&x);
		for (int i=1;i<=Len;i++) p*=10;
		for (int i=1;i<=n;i++)
			if (a[i]%p==x){
				printf("%d\n",a[i]);
				flag=1;
				break;
			}
		if (!flag) puts("-1");
	}
	return 0;
}


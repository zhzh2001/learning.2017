const dx:array[1..4]of integer=(1,0,-1,0);
      dy:array[1..4]of integer=(0,1,0,-1);
var n,m,head,tail,x,y,z,i,j,tx,ty,ans,bt:longint;
    map,a:array[0..101,0..101]of longint;
    qx,qy,b,yy:array[0..20000]of longint;
    f:boolean;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
   begin
     readln(x,y,z);
     if z=0 then map[x,y]:=2
     else map[x,y]:=1;
   end;
  head:=0;tail:=1;qx[1]:=1;qy[1]:=1; b[1]:=map[1,1];
  for i:=1 to n do
   for j:=1 to n do
    a[i,j]:=maxlongint;
  a[1,1]:=1;
  while (head<tail) do
   begin
     inc(head);
     if a[qx[head],qy[head]]<yy[head] then continue;
     for i:=1 to 4 do
      begin
        f:=true;
        tx:=qx[head]+dx[i];ty:=qy[head]+dy[i];
        ans:=a[qx[head],qy[head]];
        if a[tx,ty]>0 then
         begin
        if map[qx[head],qy[head]]>0 then
         begin
           if map[tx,ty]=0 then
            begin
              ans:=ans+2;
              bt:=b[head];
            end
           else
            begin
              if b[head]<>map[tx,ty] then
               begin
                 ans:=ans+1;
                 bt:=map[tx,ty];
               end
              else
               bt:=map[tx,ty];
            end
          end
         else
          begin
            if map[tx,ty]=0 then
             f:=false
            else
            begin
             if map[tx,ty]<>b[head] then
              begin
                ans:=ans+1;
                bt:=map[tx,ty];
              end
             else
                bt:=map[tx,ty];
            end;
          end;
        if ((ans<a[tx,ty])or((ans=a[tx,ty])and(map[tx,ty]=0)))and f then
         begin
           inc(tail);
           qx[tail]:=tx;qy[tail]:=ty;b[tail]:=bt;
           a[tx,ty]:=ans;   yy[tail]:=ans;
         end;
      end;
     end;
   end;

  if a[n,n]=maxlongint then writeln('-1')
   else writeln(a[n,n]-1);
 close(input);close(output);
end.

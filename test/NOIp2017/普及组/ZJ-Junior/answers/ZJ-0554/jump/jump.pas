uses math;
var sum,k,n,d,l,r,mid,i,last,f,j,g:longint;
    x,s,a:array[0..500000]of longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
   begin
     readln(x[i],s[i]);
     if s[i]>0 then f:=x[i];
     if s[i]>0 then sum:=sum+s[i];
   end;
  r:=f;
  if sum<k then writeln('-1') else
    begin
      while l<r do
       begin
         sum:=0; mid:=(l+r) div 2; i:=0;
         for i:=1 to n do
          a[i]:=0;
         for i:=0 to n do
          begin
            j:=i;
            while (x[j]-x[i]<max(1,d-mid))and(j<=n) do
             inc(j);
            if (j>n)or(x[j]-x[i]>d+mid) then
             break;
            while (j<=n)and(x[j]-x[i]<=d+mid) do
             begin
              a[j]:=max(a[j],a[i]+s[j]);inc(j);
             end;
          end;
         for i:=1 to n do if a[i]>sum then sum:=a[i];
         if sum<k then l:=mid+1 else r:=mid;
       end;
     writeln(l);
    end;
  close(input);close(output);
end.
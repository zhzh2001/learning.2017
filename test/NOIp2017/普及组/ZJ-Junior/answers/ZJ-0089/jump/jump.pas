var n,d,k,maxx,l,r,mid,x1,x2,i,j,head,tail:longint;
    x,s,q:array[0..500001] of longint;
    f:array[0..500001] of int64;
    sum:int64;
function max(x,y:int64):int64;
begin
 if x>y then exit(x);
 exit(y);
end;

begin
 assign(input,'jump.in');reset(input);
 assign(output,'jump.out');rewrite(output);
 readln(n,d,k);
 for i:=1 to n do
  begin
   readln(x[i],s[i]);
   if x[i]>maxx then maxx:=x[i];
   if s[i]>0 then sum:=sum+s[i];
  end;
 if sum<k then
  begin
   writeln(-1);
   close(input);
   close(output);
   exit;
  end;
 l:=0; r:=maxx;
 while l<r do
  begin
   mid:=(l+r)>>1;
   if mid>=d then
    begin
     x1:=1;
     x2:=d+mid;
    end;
   if mid<d then
    begin
     x1:=d-mid;
     x2:=d+mid;
    end;
   if x[1]>=x1 then
    begin
     f[1]:=s[1];
     head:=1;
     tail:=1;
     q[1]:=1;
     for i:=2 to n do
      begin
       while (head<=tail)and
       ((x[i]-x[q[head]]<x1)or(x[i]-x[q[head]]>x2)) do inc(head);
       inc(tail);
       q[tail]:=i;
       f[i]:=f[q[head]]+s[i];
       while (head<tail)and(f[q[tail]]>=f[q[tail-1]]) do
        begin
         dec(tail);
         q[tail]:=q[tail+1];
        end;
      end;
     if f[n]>=k then r:=mid
      else l:=mid+1;
    end
   else l:=mid+1;
  end;
 writeln(l);
 close(input);
 close(output);
end.

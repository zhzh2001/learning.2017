var m,n,ans,i,j,x1,y1,c:longint;
    a:array[0..101,0..101] of longint;
    p:array[0..101,0..101] of boolean;
procedure find(x,y,s,last,use:longint);
begin
 if s>ans then exit;
 if (x<1)or(y<1)or(x>m)or(y>m) then exit;
 if a[x,y]=-1 then exit;
 if p[x,y] then exit;
 if last<>a[x,y] then s:=s+1;
 if (x=m)and(y=m) then
  begin
   if s<ans then ans:=s;
   exit;
  end;
 p[x,y]:=true;
 find(x+1,y,s,a[x,y],0);
 if (a[x+1,y]=-1)and(use=0) then
  begin
   s:=s+2;
   a[x+1,y]:=1;
   find(x+1,y,s,a[x,y],1);
   a[x+1,y]:=0;
   find(x+1,y,s,a[x,y],1);
   a[x+1,y]:=-1;
   s:=s-2;
  end;
 find(x,y+1,s,a[x,y],0);
 if (a[x,y+1]=-1)and(use=0) then
  begin
   s:=s+2;
   a[x,y+1]:=1;
   find(x,y+1,s,a[x,y],1);
   a[x,y+1]:=0;
   find(x,y+1,s,a[x,y],1);
   a[x,y+1]:=-1;
   s:=s-2;
  end;
 find(x-1,y,s,a[x,y],0);
 if (a[x-1,y]=-1)and(use=0) then
  begin
   s:=s+2;
   a[x-1,y]:=1;
   find(x-1,y,s,a[x,y],1);
   a[x-1,y]:=0;
   find(x-1,y,s,a[x,y],1);
   a[x-1,y]:=-1;
   s:=s-2;
  end;
 find(x,y-1,s,a[x,y],0);
 if (a[x,y-1]=-1)and(use=0) then
  begin
   s:=s+2;
   a[x,y-1]:=1;
   find(x,y-1,s,a[x,y],1);
   a[x,y-1]:=0;
   find(x,y-1,s,a[x,y],1);
   a[x,y-1]:=-1;
   s:=s-2;
  end;
 p[x,y]:=false;
end;
begin
 assign(input,'chess.in');reset(input);
 assign(output,'chess.out');rewrite(output);
 ans:=maxlongint;
 readln(m,n);
 for i:=1 to m do
  for j:=1 to m do
   a[i,j]:=-1;
 for i:=1 to n do
  begin
   readln(x1,y1,c);
   a[x1,y1]:=c;
  end;
 find(1,1,0,a[1,1],0);
 if ans=maxlongint then writeln(-1)
  else writeln(ans);
 close(input);
 close(output);
end.

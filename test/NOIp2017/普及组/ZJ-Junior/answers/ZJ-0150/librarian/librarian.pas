const
  oo=maxlongint;
var
  key,find,iskey:string;
  a:array[0..1000]of longint;
  n,q,i,j,min,len,x:longint;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do
  readln(a[i]);
  for i:=1 to q do
  begin
    readln(len,x);
    str(x,key);
    min:=oo;
    for j:=1 to n do
    begin
      str(a[j],find);
      iskey:=copy(find,length(find)-len+1,len);
      if iskey=key then
      begin
        if a[j]<min then min:=a[j];
      end;
    end;
    if min=oo then min:=-1;
    writeln(min);
  end;
  close(input); close(output);
end.

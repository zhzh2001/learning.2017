const
 dx:array[1..4]of longint=(1,-1,0,0);
 dy:array[1..4]of longint=(0,0,1,-1);
 oo=maxlongint;
var
  i,j,n,m,min,x,y,z:longint;
  map,a:array[0..100,0..100]of longint;
  b,right:array[1..4]of longint;
  p:array[0..100,0..100]of boolean;
procedure dfs(x,y,ok,money:longint);
var
  i,mini,moneyx:longint;
  go:boolean;
begin
  if money>min then exit;
  if ok>0 then dec(ok);
  if (x=n)and(y=n) then
  begin
    if min>money then min:=money;
    exit;
  end;
  mini:=oo;
  for i:=1 to 4 do
  begin
  b[i]:=0;
    right[i]:=0;   moneyx:=0; go:=false;
  if (x+dx[i]>0)and(x+dx[i]<=n)and(y+dy[i]>0)and(y+dy[i]<=n) then
   if (p[x+dx[i],y+dy[i]]=true) then
  begin
    if (map[x+dx[i],y+dy[i]]=-1) then
    begin
      if ok<>0 then continue;
      right[i]:=1;
      moneyx:=moneyx+2;
      go:=true;
    end
    else
    if  (map[x+dx[i],y+dy[i]]=-1)and(ok<>0) then continue
    else
    begin
        if (map[x,y]<>-1)and(map[x,y]<>map[x+dx[i],y+dy[i]])and(map[x+dx[i],y+dy[i]]<>-1) then inc(moneyx);
        if (a[x,y]=1)and(map[x+dx[i],y+dy[i]]=-1)then continue;
      go:=true;
    end;
  end;
  if go then begin
     b[i]:=moneyx;
    if mini>moneyx then mini:=moneyx; end
  else b[i]:=oo;
  end;
  for i:=1 to 4 do
  begin
    if (b[i]=mini)and(mini<>oo) then
    begin
      if right[i]=1 then begin ok:=2; a[x+dx[i],y+dy[i]]:=1;
      map[x+dx[i],y+dy[i]]:=map[x,y]   end;
       p[x+dx[i],y+dy[i]]:=false;
      dfs(x+dx[i],y+dy[i],ok,money+b[i]);
      a[x+dx[i],y+dy[i]]:=0;
    end;
  end;
end;
begin
 assign(input,'chess.in'); reset(input);
 assign(output,'chess.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      map[i,j]:=-1;
  for i:=1 to m do
  begin
    read(x,y,z);
    map[x,y]:=z;
  end;
  fillchar(p,sizeof(p),true);
  min:=oo;
  p[1,1]:=false;
  dfs(1,1,0,0);
  if min=oo then min:=-1;
  writeln(min);
 close(input); close(output);
end.


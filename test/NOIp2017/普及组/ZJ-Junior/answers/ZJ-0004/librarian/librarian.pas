var
    n,q,i,j,m:longint;
    bk,ndlen,ndnum:array[0..1010]of longint;
procedure qs(l,r:longint);
    var
        i,j,m,t:longint;
    begin
        i:=l;
        j:=r;
        m:=bk[l];
        repeat
            while bk[i]<m do inc(i);
            while bk[j]>m do dec(j);
            if i<=j then begin
                t:=bk[i];
                bk[i]:=bk[j];
                bk[j]:=t;
                inc(i);
                dec(j);
            end;
        until i>j;
        if l<j then qs(l,j);
        if i<r then qs(i,r);
    end;
begin
    assign(input,'librarian.in');
    reset(input);
    assign(output,'librarian.out');
    rewrite(output);
    readln(n,q);
    for i:=1 to n do readln(bk[i]);
    for i:=1 to q do readln(ndlen[i],ndnum[i]);
    qs(1,n);
    for i:=1 to q do begin
        m:=trunc(exp(ndlen[i]*ln(10)));
        for j:=1 to n do begin
            if bk[j] mod m=ndnum[i] then begin
                writeln(bk[j]);
                break;
            end;
            if j=n then writeln('-1');
        end;
    end;
    close(input);
    close(output);
end.

var
    m,n,i,x,y,c,min:longint;
    cr:array[0..1010,0..1010]of integer;
    wkd:array[0..1010,0..1010]of boolean;
    t:boolean;
procedure try(i,j,cn:longint);
    begin
        if (i=m)and(j=m) then begin
            t:=true;
            if cn<min then min:=cn;
            exit;
        end;
        wkd[i,j]:=true;
        if (i+1<=m)and(cr[i+1,j]<>0)and(not wkd[i+1,j]) then begin
            cn:=cn+abs(cr[i+1,j]-cr[i,j]);
            try(i+1,j,cn);
            cn:=cn-abs(cr[i+1,j]-cr[i,j]);
        end;
        if (j+1<=m)and(cr[i,j+1]<>0)and(not wkd[i,j+1]) then begin
            cn:=cn+abs(cr[i,j+1]-cr[i,j]);
            try(i,j+1,cn);
            cn:=cn-abs(cr[i,j+1]-cr[i,j]);
        end;
        if (i-1>=1)and(cr[i-1,j]<>0)and(not wkd[i-1,j]) then begin
            cn:=cn+abs(cr[i-1,j]-cr[i,j]);
            try(i-1,j,cn);
            cn:=cn-abs(cr[i-1,j]-cr[i,j]);
        end;
        if (j-1>=1)and(cr[i,j-1]<>0)and(not wkd[i,j-1]) then begin
            cn:=cn+abs(cr[i,j-1]-cr[i,j]);
            try(i,j-1,cn);
            cn:=cn-abs(cr[i,j-1]-cr[i,j]);
        end;
        if (i+2<=m)and(cr[i+1,j]=0)and(cr[i+2,j]<>0)and(not wkd[i+1,j])and(not wkd[i+2,j]) then begin
            cn:=cn+abs(cr[i+2,j]-cr[i,j])+2;
            wkd[i+1,j]:=true;
            try(i+2,j,cn);
            wkd[i+1,j]:=false;
            cn:=cn-abs(cr[i+2,j]-cr[i,j])-2;
        end;
        if (j+2<=m)and(cr[i,j+1]=0)and(cr[i,j+2]<>0)and(not wkd[i,j+1])and(not wkd[i,j+2]) then begin
            cn:=cn+abs(cr[i,j+2]-cr[i,j])+2;
            wkd[i,j+1]:=true;
            try(i,j+2,cn);
            wkd[i,j+1]:=false;
            cn:=cn-abs(cr[i,j+2]-cr[i,j])-2;
        end;
        if (i-2>=1)and(cr[i-1,j]=0)and(cr[i-2,j]<>0)and(not wkd[i-1,j])and(not wkd[i-2,j]) then begin
            cn:=cn+abs(cr[i-2,j]-cr[i,j])+2;
            wkd[i-1,j]:=true;
            try(i-2,j,cn);
            wkd[i-1,j]:=false;
            cn:=cn-abs(cr[i-2,j]-cr[i,j])-2;
        end;
        if (j-2>=1)and(cr[i,j-1]=0)and(cr[i,j-2]<>0)and(not wkd[i,j-1])and(not wkd[i,j-2]) then begin
            cn:=cn+abs(cr[i,j-2]-cr[i,j])+2;
            wkd[i,j-1]:=true;
            try(i,j-2,cn);
            wkd[i,j-1]:=false;
            cn:=cn-abs(cr[i,j-2]-cr[i,j])-2;
        end;
        if (i+1<=m)and(j+1<=m)and(cr[i+1,j]=0)and(cr[i,j+1]=0)and(cr[i+1,j+1]<>0)and(not wkd[i+1,j])and(not wkd[i,j+1])and(not wkd[i+1,j+1]) then begin
            cn:=cn+abs(cr[i+1,j+1]-cr[i,j])+2;
            wkd[i+1,j]:=true;
            wkd[i,j+1]:=true;
            try(i+1,j+1,cn);
            wkd[i+1,j]:=false;
            wkd[i,j+1]:=false;
            cn:=cn-abs(cr[i+1,j+1]-cr[i,j])-2;
        end;
        if (i-1>=1)and(j+1<=m)and(cr[i-1,j]=0)and(cr[i,j+1]=0)and(cr[i-1,j+1]<>0)and(not wkd[i-1,j])and(not wkd[i,j+1])and(not wkd[i-1,j+1]) then begin
            cn:=cn+abs(cr[i-1,j+1]-cr[i,j])+2;
            wkd[i-1,j]:=true;
            wkd[i,j+1]:=true;
            try(i-1,j+1,cn);
            wkd[i-1,j]:=false;
            wkd[i,j+1]:=false;
            cn:=cn-abs(cr[i-1,j+1]-cr[i,j])-2;
        end;
        if (i+1<=m)and(j-1>=1)and(cr[i+1,j]=0)and(cr[i,j-1]=0)and(cr[i+1,j-1]<>0)and(not wkd[i+1,j])and(not wkd[i,j-1])and(not wkd[i+1,j-1]) then begin
            cn:=cn+abs(cr[i+1,j-1]-cr[i,j])+2;
            wkd[i+1,j]:=true;
            wkd[i,j-1]:=true;
            try(i+1,j-1,cn);
            wkd[i+1,j]:=false;
            wkd[i,j-1]:=false;
            cn:=cn-abs(cr[i+1,j-1]-cr[i,j])-2;
        end;
        if (i-1>=1)and(j-1>=1)and(cr[i-1,j]=0)and(cr[i,j-1]=0)and(cr[i-1,j-1]<>0)and(not wkd[i-1,j])and(not wkd[i,j-1])and(not wkd[i-1,j-1]) then begin
            cn:=cn+abs(cr[i-1,j-1]-cr[i,j])+2;
            wkd[i-1,j]:=true;
            wkd[i,j-1]:=true;
            try(i-1,j-1,cn);
            wkd[i-1,j]:=false;
            wkd[i,j-1]:=false;
            cn:=cn-abs(cr[i-1,j-1]-cr[i,j])-2;
        end;
        wkd[i,j]:=false;
    end;
begin
    assign(input,'chess.in');
    reset(input);
    assign(output,'chess.out');
    rewrite(output);
    readln(m,n);
    for i:=1 to n do begin
        readln(x,y,c);
        inc(c);
        cr[x,y]:=c;
    end;
    min:=maxlongint;
    try(1,1,0);
    if t then writeln(min)
    else writeln('-1');
    close(input);
    close(output);
end.

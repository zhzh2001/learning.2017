var
    n,d,k,i,g,ans,max:longint;
    x,s:array[0..500010]of longint;
procedure jump(l,b,sc:longint);
    var
        e,min,i:longint;
    begin
        if sc>=k then begin
            writeln(g);
            close(input);
            close(output);
            halt;
        end;
        if x[n]-x[b]<l then exit;
        for e:=b+1 to n do
            if x[e]-x[b]=l then break
            else if x[e]-x[b]>l then exit;
        sc:=sc+s[e];
        min:=d-g;
        if min<1 then min:=1;
        for i:=min to d+g do jump(i,e,sc);
    end;
procedure try(g:longint);
    var
        i,min:longint;
    begin
        min:=d-g;
        if min<1 then min:=1;
        for i:=min to d+g do jump(i,0,0);
    end;
begin
    assign(input,'jump.in');
    reset(input);
    assign(output,'jump.out');
    rewrite(output);
    readln(n,d,k);
    for i:=1 to n do readln(x[i],s[i]);
    for i:=1 to n do if s[i]>0 then ans:=ans+s[i];
    if ans<k then begin
        writeln('-1');
        close(input);
        close(output);
        halt;
    end;
    max:=abs(d-x[n]);
    if max<abs(d-x[1]) then max:=abs(d-x[1]);
    for g:=0 to max do try(g);
end.

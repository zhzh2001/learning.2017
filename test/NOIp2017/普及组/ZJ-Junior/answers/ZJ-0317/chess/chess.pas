var n,m,i,j,k,ans,i1,j1,t:longint;
    f:array[1..105,1..105]of longint;
    b,c:array[1..105,1..105]of -1..1;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
   for j:=1 to n do
     begin
       b[i,j]:=-1;
       f[i,j]:=maxlongint;
     end;
  f[1,1]:=0;
  for i:=1 to m do
    begin
      read(j,k);read(b[j,k]);
      readln;
    end;
  if (b[n,n-1]=-1)and(b[n-1,n]=-1)and(b[n-1,n-1]=-1)and(b[n,n-2]=-1)and(b[n-2,n]=-1) then
    begin
      writeln('-1');
      close(input);
      close(output);
      halt;
    end;
  for i:=1 to n do
   for j:=1 to n do if b[i,j]>-1 then
     begin
       if (b[i,j]=b[i+1,j])and(b[i,j]>-1)and(f[i,j]<f[i+1,j]) then f[i+1,j]:=f[i,j];
       if (b[i,j]<>b[i+1,j])and(b[i+1,j]>-1)and(f[i+1,j]>f[i,j]+1) then f[i+1,j]:=f[i,j]+1;
       if (b[i+1,j]=-1)and(c[i,j]=0) then
         begin
           b[i+1,j]:=b[i,j];
           f[i+1,j]:=f[i,j]+2;
           c[i+1,j]:=1;
         end;
       if (b[i,j]=b[i,j+1])and(b[i,j]>-1)and(f[i,j]<f[i,j+1]) then f[i,j+1]:=f[i,j];
       if (b[i,j]<>b[i,j+1])and(b[i,j+1]>-1)and(f[i,j+1]>f[i,j]+1) then f[i,j+1]:=f[i,j]+1;
       if (b[i,j+1]=-1)and(c[i,j]=0) then
         begin
           b[i,j+1]:=b[i,j];
           f[i,j+1]:=f[i,j]+2;
           c[i,j+1]:=1;
         end;
     end;
  writeln(f[n,n]);
  close(input);
  close(output);
end.

var n,m,i,j,p,l,k,l1,p1:longint;
    f,flag:boolean;
    ch,s4:ansistring;
    s2,s3:array[1..1050]of ansistring;
    a:array[1..1050]of longint;
  procedure sort(s,t:longint);
    var i,j,x:longint;
    begin
      i:=s;j:=t;x:=a[i];
      repeat
        while (i<j)and(x<=a[j]) do dec(j);
        if i<j then begin a[i]:=a[j];inc(i);end;
        while (i<j)and(x>=a[i]) do inc(i);
        if i<j then begin a[j]:=a[i];dec(j);end;
      until i=j;
      a[i]:=x;
      if i+1<t then sort(i+1,t);
      if s<j-1 then sort(s,j-1);
    end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  sort(1,n);
  for i:=1 to n do str(a[i],s2[i]);
  for i:=1 to m do readln(s3[i]);
  for i:=1 to m do
    begin
      f:=false;
      p:=pos(' ',s3[i]);
      delete(s3[i],1,p);
      l:=length(s3[i]);
      for j:=1 to n do if not(f) then
        begin
          s4:=copy(s2[j],length(s2[j])-l+1,length(s2[j]));
          if s4=s3[i] then f:=true;
          if f then begin writeln(s2[j]);break;end;
        end;
      if not(f) then writeln('-1');
    end;
  close(input);
  close(output);
end.

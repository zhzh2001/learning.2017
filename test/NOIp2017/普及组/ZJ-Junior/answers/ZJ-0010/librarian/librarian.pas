var
  n,q,i,j,l:longint;
  x,k,ans:longint;
  a:array[-1..1010]of longint;
    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
assign(input,'librarian.in');reset(input);
assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
  begin
    readln(a[i]);
  end;
  sort(1,n);
  for i:=1 to q do
  begin
    readln(l,x);
    k:=1;
    for j:=1 to l do k:=k*10;
    ans:=-1;
    for j:=1 to n do
    begin
      if a[j] mod k=x then
      begin
        ans:=a[j];
        break;
      end;
    end;
    writeln(ans);
  end;
close(input);
close(output);
end.
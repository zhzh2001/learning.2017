var
 n,d,k,i:longint;
 l,r,mid,sum:int64;
 a,b,f:array[-10..500010]of int64;
    procedure sort(l,r: longint);
      var
         i,j: longint;
         x,y: int64;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                y:=b[i];
                b[i]:=b[j];
                b[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
function max(a,b:int64):int64;
begin
  if a>b then max:=a
  else max:=b;
end;
function check(g:int64):boolean;
var i,j:longint;
    find:boolean; lo,ro,p1,p2:int64;
begin
  for i:=0 to n do f[i]:=b[i];
  if d-g<1 then lo:=a[0]+1
  else lo:=a[0]+d-g;
  ro:=a[0]+d+g;
  for i:=0 to n do
  begin
    if (a[i]>=lo)and(a[i]<=ro) then
    begin
      p1:=i;
      break;
    end;
  end;
  for i:=n downto 0 do
  begin
    if (a[i]>=lo)and(a[i]<=ro) then
    begin
      p2:=i;
      break;
    end;
  end;
  find:=false;
  for i:=0 to n do
  begin
    if d-g<1 then lo:=a[i]+1
    else lo:=a[i]+d-g;
    ro:=a[i]+d+g;
    while (p1<=n)and(a[p1]<lo) do inc(p1);
    while (p2<=n)and(a[p2]<=ro) do inc(p2);
    if a[p2]>ro then dec(p2);
    for j:=p1 to p2 do
    begin
      f[j]:=max(f[j],f[i]+b[j]);
      if f[j]-1000000000>k then
      begin
        find:=true;
        break;
      end;
    end;
    if find then break;
  end;
  check:=find;
end;
begin
assign(input,'jump.in');reset(input);
assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  sum:=0;
  for i:=1 to n do
  begin
    readln(a[i],b[i]);
    if b[i]>0 then sum:=sum+b[i];
  end;
  a[0]:=0;
  b[0]:=1000000000;
  if sum<k then
  begin
    writeln(-1);
  end
  else
  begin
//    sort(1,n);
//    writeln(check(1));
    l:=0;
    r:=1000000000;
    while l<r do
    begin
      mid:=l+(r-l) div 2;
      if check(mid) then
      begin
        r:=mid;
      end
      else
      begin
        l:=mid+1;
      end;
    end;
    writeln(r);
  end;
close(input);
close(output);
end.

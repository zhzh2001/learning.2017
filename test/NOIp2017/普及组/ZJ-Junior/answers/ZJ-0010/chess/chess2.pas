const
  dx:array[1..4]of -1..1=(0,-1,0,1);
  dy:array[1..4]of -1..1=(-1,0,1,0);
var
 ans,n,m,i,x,y,z:longint;
 g:array[-1..110,-1..110]of longint;
 d:array[-1..110,-1..110]of longint;
 w,f:array[-1..110,-1..110]of boolean;
function min(a,b:longint):longint;
begin
  if a<b then min:=a
  else min:=b;
end;
function dp(x,y:longint):longint;
var nx,ny,i:longint;
    fl:boolean;
begin
  if d[x,y]>-1 then dp:=d[x,y]
  else
  begin
    if (x=m)and(y=m) then dp:=0
    else
    begin
      dp:=maxlongint div 2;
      for i:=1 to 4 do
      begin
        nx:=x+dx[i];
        ny:=y+dy[i];
        if (nx>0)and(nx<=m)and(ny>0)and(ny<=m)and(w[nx,ny]) then
        begin
          if (g[x,y]=g[nx,ny])and(g[nx,ny]<>-1) then
          begin
            fl:=false;
            if f[x,y] then
            begin
              f[x,y]:=false;
              fl:=true;
            end;
            w[nx,ny]:=false;
            dp:=min(dp,dp(nx,ny));
            w[nx,ny]:=true;
            if fl then f[x,y]:=true;
          end;
          if (g[x,y]<>g[nx,ny])and(g[nx,ny]<>-1) then
          begin
            fl:=false;
            if f[x,y] then
            begin
              f[x,y]:=false;
              fl:=true;
            end;
            w[nx,ny]:=false;
            dp:=min(dp,dp(nx,ny)+1);
            w[nx,ny]:=true;
            if fl then f[x,y]:=true;
          end;
          if (g[nx,ny]=-1)and(not(f[x,y])) then
          begin
            w[nx,ny]:=false;
            f[nx,ny]:=true;
            g[nx,ny]:=g[x,y];
            dp:=min(dp,dp(nx,ny)+2);
            g[nx,ny]:=-1;
            w[nx,ny]:=true;
            f[nx,ny]:=false;
          end;
     //     if (x=1)and(y=2) then writeln(dp);
        end;
      end;
      d[x,y]:=dp;
    end;
  end;
end;
begin
  readln(m,n);
  fillchar(g,sizeof(g),255);
  for i:=1 to n do
  begin
    readln(x,y,z);
    g[x,y]:=z;
  end;
  fillchar(f,sizeof(f),false);
  fillchar(w,sizeof(w),true);
  fillchar(d,sizeof(d),255);
  w[1,1]:=false;
  ans:=dp(1,1);
  if ans>1000000000 then writeln(-1)
  else writeln(ans);
end.

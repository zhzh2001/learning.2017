var
  x,y,c,i,j,k,n,m,min,sum:longint;
  ch:array[0..1001,0..1001] of longint;
  flag:array[0..1001,0..1001] of boolean;

procedure dfs(x,y:longint);
  var
    i,j,k:longint;
  begin
    if (x=m) and (y=m)
      then
        begin
          if sum<min then min:=sum;
          exit;
        end;
    if flag[x,y]=false then exit;
    flag[x,y]:=false;
    if x-1>=0
      then
        begin
          if (ch[x,y]=ch[x-1,y]) or (abs(ch[x,y]-ch[x-1,y])=2)
            then
              dfs(x-1,y)
            else
              if ch[x-1,y]<>-100
                then
                  begin
                    inc(sum);
                    dfs(x-1,y);
                    dec(sum);
                  end
                else
                  if (ch[x,y]<>3) and (ch[x,y]<>4)
                    then
                      begin
                        sum:=sum+2;
                        ch[x-1,y]:=3;
                        dfs(x-1,y);
                        ch[x-1,y]:=4;
                        dfs(x-1,y);
                        ch[x-1,y]:=-100;
                        sum:=sum-2;
                      end;
        end;
    if x+1<=m
      then
        begin
          if (ch[x,y]=ch[x+1,y]) or (abs(ch[x,y]-ch[x+1,y])=2)
            then
              dfs(x+1,y)
            else
              if ch[x+1,y]<>-100
                then
                  begin
                    inc(sum);
                    dfs(x+1,y);
                    dec(sum);
                  end
                else
                  if (ch[x,y]<>3) and (ch[x,y]<>4)
                    then
                      begin
                        sum:=sum+2;
                        ch[x+1,y]:=3;
                        dfs(x+1,y);
                        ch[x+1,y]:=4;
                        dfs(x+1,y);
                        ch[x+1,y]:=-100;
                        sum:=sum-2;
                      end;
        end;
    if y-1>=0
      then
        begin
          if (ch[x,y]=ch[x,y-1]) or (abs(ch[x,y]-ch[x,y-1])=2)
            then
              dfs(x,y-1)
            else
              if ch[x,y-1]<>-100
                then
                  begin
                    inc(sum);
                    dfs(x,y-1);
                    dec(sum);
                  end
                else
                  if (ch[x,y]<>3) and (ch[x,y]<>4)
                    then
                      begin
                        sum:=sum+2;
                        ch[x,y-1]:=3;
                        dfs(x,y-1);
                        ch[x,y-1]:=4;
                        dfs(x,y-1);
                        ch[x,y-1]:=-100;
                        sum:=sum-2;
                      end;
        end;
    if y+1<=m
      then
        begin
          if (ch[x,y]=ch[x,y+1]) or (abs(ch[x,y]-ch[x,y+1])=2)
            then
              dfs(x,y+1)
            else
              if ch[x,y+1]<>-100
                then
                  begin
                    inc(sum);
                    dfs(x,y+1);
                    dec(sum);
                  end
                else
                  if (ch[x,y]<>3) and (ch[x,y]<>4)
                    then
                      begin
                        sum:=sum+2;
                        ch[x,y+1]:=3;
                        dfs(x,y+1);
                        ch[x,y+1]:=4;
                        dfs(x,y+1);
                        ch[x,y+1]:=-100;
                        sum:=sum-2;
                      end;
        end;
    flag[x,y]:=true;
  end;

begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  fillchar(flag,sizeof(flag),true);
  for i:=1 to m do
    for j:=1 to m do ch[i,j]:=-100;
  for i:=1 to n do
    begin
      readln(x,y,c);
      ch[x,y]:=c;
    end;
  min:=maxlongint;
  sum:=0;
  dfs(1,1);
  if min=maxlongint
    then
      writeln(-1)
    else
     writeln(min);
  close(input);
  close(output);
end.
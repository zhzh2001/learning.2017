var
  i,j,k,l,n,q:longint;
  a,b,c,ans:array[0..1005] of longint;

procedure qsort(l,r:longint);
  var
    i,j,mid:longint;
  begin
    i:=l;
    j:=r;
    mid:=a[(l+r) div 2];
    repeat
      while a[i]<mid do inc(i);
      while a[j]>mid do dec(j);
      if i<=j
        then
          begin
            a[0]:=a[i];
            a[i]:=a[j];
            a[j]:=a[0];
            inc(i);
            dec(j);
          end;
    until i>j;
    if i<r then qsort(i,r);
    if l<j then qsort(l,j);
  end;

begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  for i:=1 to q do
    begin
      l:=1;
      readln(b[i],c[i]);
      for k:=1 to b[i] do l:=l*10;
      b[i]:=l;
    end;
  qsort(1,n);
  for i:=0 to 1005 do ans[i]:=-1;
  for i:=1 to q do
    for j:=1 to n do
      if a[j] mod b[i]=c[i]
        then
          begin
            ans[i]:=a[j];
            break;
          end;
  for i:=1 to q do writeln(ans[i]);
  close(input);
  close(output);
end.
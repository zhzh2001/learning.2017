var
  n,q,i,j,x,y,t,num:longint;
  f:boolean;
  a:array[0..1005,0..25]of longint;
  b:array[0..1005]of longint;
procedure qsort(l,r:longint);
var i,j,x,y: longint;
begin
  i:=l; j:=r; x:=b[(l+r)shr 1];
  repeat
    while b[i]<x do inc(i);
    while x<b[j] do dec(j);
    if not(i>j) then
    begin
      y:=b[i]; b[i]:=b[j]; b[j]:=y;
      inc(i); j:=j-1;
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(b[i]);
  qsort(1,n);
  for i:=1 to n do
  begin
    t:=b[i]; j:=0; num:=1;
    while t<>0 do
    begin
      inc(j); a[i,j]:=a[i,j-1]+t mod 10*num; t:=t div 10; num:=num*10;
    end;
  end;
  for i:=1 to q do
  begin
    readln(x,y);  f:=false;
    for j:=1 to n do
      if a[j,x]=y then begin writeln(b[j]); f:=true; break; end;
    if not f then writeln(-1);
  end;
  close(input); close(output);
end.

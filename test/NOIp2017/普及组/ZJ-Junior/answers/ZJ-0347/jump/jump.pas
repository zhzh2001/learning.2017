var
  n,d,k,l,r,i,sum,max,mid:longint;
  a,b:array[0..500005]of longint;
function check(g:longint):boolean;
var       s,last,len,shu1,shu2,yu1,yu2:longint; f:boolean;
begin
  last:=0;   s:=0;
  for i:=1 to n do
    if a[i]>0 then
    begin
      len:=b[i]-b[last];   f:=false;
      shu1:=len div (d+g)+1; yu1:=shu1*(d+g)-len;
      if yu1<=shu1*g*2 then  f:=true;
      shu2:=len div(d-g); yu2:=len-shu2*(d+g);
      if yu2<=shu2*g*2 then f:=true;
      if f then begin s:=s+a[i]; last:=i; end;
    end;
  exit(s>=k);
end;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(b[i],a[i]);
    sum:=sum+a[i];
    if b[i]-b[i-1]>max then max:=b[i]-b[i-1];
  end;
  if sum<k then
  begin
    writeln(-1); close(input); close(output); halt;
  end;
  l:=0;
  if max-d>d then r:=max-d else r:=d;
  while l<r do
  begin
    mid:=(l+r-1)shr 1;
    if check(mid)then r:=mid
      else l:=mid+1;
  end;
  writeln(l);
  close(input); close(output);
end.

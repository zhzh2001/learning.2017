var
  m,n,i,j,x,y,z:longint;
  f:array[0..1005,0..1005]of longint;
  a:array[0..1005,0..1005]of longint;
  dx,dy:array[0..4]of longint;
procedure dfs(x,y,k,s,fx:longint);
var    xx,yy:longint; ff:boolean;
begin
  if(x=m)and(y=m)then exit;
  for i:=1 to 4 do
  begin
    ff:=true;
    if fx<>5-i then
    begin
      xx:=dx[i]+x; yy:=dy[i]+y;
      if(xx=0)or(yy=0)or(xx>m)or(yy>m)then ff:=false;
      if(a[xx,yy]=-1)and(ff)then
      begin
        if k=1 then ff:=false;
        if(f[xx,yy]>s+2)and(ff)then f[xx,yy]:=s+2 else ff:=false;
        if ff then dfs(xx,yy,1,s+2,i);
      end else
      if(ff)and(a[xx,yy]=a[x,y])or(ff)and(k=1)and(a[x+dx[5-fx],y+dy[5-fx]]=a[xx,yy])then
      begin
        if(ff)and(f[xx,yy]>s)then f[xx,yy]:=s else ff:=false;
        if ff then dfs(xx,yy,0,s,i);
      end else if ff then
      begin
        if (f[xx,yy]>s+1)and(ff) then f[xx,yy]:=s+1 else ff:=false;
        if ff then dfs(xx,yy,0,s+1,i);
      end;
    end;
  end;
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  dx[2]:=1; dy[2]:=0;
  dx[3]:=-1; dy[3]:=0;
  dx[4]:=0; dy[4]:=1;
  dx[1]:=0; dy[1]:=-1;
  for i:=1 to m do
    for j:=1 to m do
    begin
      a[i,j]:=-1; f[i,j]:=maxlongint;
    end;
  for i:=1 to n do
  begin
    readln(x,y,z); a[x,y]:=z;
  end;
  f[1,1]:=0;
  dfs(1,1,0,0,0);
  if f[m,m]=maxlongint then writeln(-1)else  writeln(f[m,m]);
  close(input); close(output);
end.

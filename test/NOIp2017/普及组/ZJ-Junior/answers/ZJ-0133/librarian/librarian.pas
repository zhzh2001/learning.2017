program librarian;
var n,q,i,j,x,a,b:longint;
    s,h,r,ans:array[1..1001]of longint;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do
  begin
    readln(s[i]);
    ans[i]:=maxlongint div 2;
  end;
  for i:=1 to q do readln(h[i],r[i]);
  for i:=1 to q do
    for j:=1 to n do
    begin
      a:=r[i]; b:=s[j];
      while (a>0)and(b>0) do
      begin
        if a mod 10=b mod 10 then
        begin
          inc(x);
          if x>=h[i] then if s[j]<ans[i] then ans[i]:=s[j];
        end
        else if x<h[i] then break;
        a:=a div 10;  b:=b div 10;
      end;
      x:=0;
    end;
  for i:=1 to q do
  begin
    if ans[i]=maxlongint div 2 then ans[i]:=-1;
    write(ans[i]);
  end;
  close(input);
  close(output);
end.

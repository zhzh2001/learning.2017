program score;
var a,b,c,ans:longint;
begin
  assign(input,'score.in'); reset(input);
  assign(output,'score.out'); rewrite(output);
  read(a,b,c);
  ans:=(2*a) div 10+(3*b) div 10+(5*c) div 10;
  write(ans);
  close(input);
  close(output);
end.

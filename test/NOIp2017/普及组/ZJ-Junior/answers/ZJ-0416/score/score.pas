var     x,y,z:longint;
BEGIN
        assign(input,'score.in'); reset(input);
        assign(output,'score.out'); rewrite(output);
        readln(x,y,z);
        x:=x div 10;
        y:=y div 10;
        z:=z div 10;
        writeln(x*2+y*3+z*5);
        close(input); close(output);
END.
var     n,m,i,j,k,x,y,c:longint;
        ff:boolean;
        f,p,g:array[0..101,0..101]of longint;

function min(xx,yy:longint):longint;
begin
        if xx<yy then exit(xx);
        exit(yy);
end;

BEGIN
        assign(input,'chess.in'); reset(input);
        assign(output,'chess.out'); rewrite(output);
        readln(m,n);
        for i:= 0 to m+1 do
                for j:= 0 to m+1 do
                begin
                        f[i,j]:=-1; p[i,j]:=-1;
                end;
        for i:= 1 to n do
        begin
                readln(x,y,c);
                p[x,y]:=c;
        end;
        g:=p;
        f[1,1]:=0;
        for i:= 1 to m do
                for j:= 1 to m do
                begin
                        if f[i-1,j]<>-1 then
                        begin
                                ff:=true; k:=0;
                                if (p[i-1,j]=-1) and (p[i,j]=-1) then ff:=false;
                                if p[i-1,j]=p[i,j] then k:=0;
                                if (p[i-1,j]<>p[i,j]) and (p[i,j]<>-1) and (p[i-1,j]<>-1) then k:=1;
                                if (p[i-1,j]<>p[i,j]) and (p[i,j]=-1) and (p[i-1,j]<>-1) then k:=2;
                                if (p[i-1,j]<>p[i,j]) and (p[i,j]<>-1) and (p[i-1,j]=-1) then k:=2;
                                if (g[i-1,j]<>p[i,j]) and (p[i,j]<>-1) and (g[i-1,j]<>-1) then k:=1;
                                if g[i-1,j]=p[i,j] then k:=0;
                                if ff then
                                begin
                                        if f[i,j]=-1 then f[i,j]:=f[i-1,j]+k
                                        else f[i,j]:=min(f[i,j],f[i-1,j]+k);
                                        if f[i,j]=f[i-1,j]+2 then g[i,j]:=p[i-1,j]
                                        else g[i,j]:=p[i,j];
                                end;
                        end;
                        if f[i+1,j]<>-1 then
                        begin
                                ff:=true; k:=0;
                                if (p[i+1,j]=-1) and (p[i,j]=-1) then ff:=false;
                                if p[i+1,j]=p[i,j] then k:=0;
                                if (p[i+1,j]<>p[i,j]) and (p[i,j]<>-1) and (p[i+1,j]<>-1) then k:=1;
                                if (p[i+1,j]<>p[i,j]) and (p[i,j]=-1) and (p[i+1,j]<>-1) then k:=2;
                                if (p[i+1,j]<>p[i,j]) and (p[i,j]<>-1) and (p[i+1,j]=-1) then k:=2;
                                if (g[i+1,j]<>p[i,j]) and (p[i,j]<>-1) and (g[i+1,j]<>-1) then k:=1;
                                if g[i+1,j]=p[i,j] then k:=0;
                                if ff then
                                begin
                                        if f[i,j]=-1 then f[i,j]:=f[i+1,j]+k
                                        else f[i,j]:=min(f[i,j],f[i+1,j]+k);
                                        if f[i,j]=f[i+1 ,j]+2 then g[i,j]:=p[i+1,j]
                                        else g[i,j]:=p[i,j];
                                end;
                        end;
                        if f[i,j-1]<>-1 then
                        begin
                                ff:=true; k:=0;
                                if (p[i,j-1]=-1) and (p[i,j]=-1) then ff:=false;
                                if p[i,j-1]=p[i,j] then k:=0;
                                if (p[i,j-1]<>p[i,j]) and (p[i,j]<>-1) and (p[i,j-1]<>-1) then k:=1;
                                if (p[i,j-1]<>p[i,j]) and (p[i,j]=-1) and (p[i,j-1]<>-1) then k:=2;
                                if (p[i,j-1]<>p[i,j]) and (p[i,j]<>-1) and (p[i,j-1]=-1) then k:=2;
                                if (g[i,j-1]<>p[i,j]) and (p[i,j]<>-1) and (g[i,j-1]<>-1) then k:=1;
                                if g[i,j-1]=p[i,j] then k:=0;
                                if ff then
                                begin
                                        if f[i,j]=-1 then f[i,j]:=f[i,j-1]+k
                                        else f[i,j]:=min(f[i,j],f[i,j-1]+k);
                                        if f[i,j]=f[i,j-1]+2 then g[i,j]:=p[i,j-1]
                                        else g[i,j]:=p[i,j];
                                end;
                        end;
                        if f[i,j+1]<>-1 then
                        begin
                                ff:=true; k:=0;
                                if (p[i,j+1]=-1) and (p[i,j]=-1) then ff:=false;
                                if p[i,j+1]=p[i,j] then k:=0;
                                if (p[i,j+1]<>p[i,j]) and (p[i,j]<>-1) and (p[i,j+1]<>-1) then k:=1;
                                if (p[i,j+1]<>p[i,j]) and (p[i,j]=-1) and (p[i,j+1]<>-1) then k:=2;
                                if (p[i,j+1]<>p[i,j]) and (p[i,j]<>-1) and (p[i,j+1]=-1) then k:=2;
                                if (g[i,j+1]<>p[i,j]) and (p[i,j]<>-1) and (g[i,j+1]<>-1) then k:=1;
                                if g[i,j+1]=p[i,j] then k:=0;
                                if ff then
                                begin
                                        if f[i,j]=-1 then f[i,j]:=f[i,j+1]+k
                                        else f[i,j]:=min(f[i,j],f[i,j+1]+k);
                                        if f[i,j]=f[i,j+1]+2 then g[i,j]:=p[i,j+1]
                                        else g[i,j]:=p[i,j];
                                end;
                        end;
                end;
        writeln(f[m,m]);
        close(input); close(output);
END.
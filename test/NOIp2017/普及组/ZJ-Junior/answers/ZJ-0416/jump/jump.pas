var     n,m,d,i:longint;
        a,b:array[1..10000]of longint;

function pd(g:longint):boolean;
var     ans,sum,i,j:longint;
begin
        ans:=0; sum:=0;
        if g<d then
        begin
                for i:= d-g to d+g do
                begin
                        inc(sum,i);
                        for j:= 1 to n do
                                if a[j]=sum then
                                begin
                                        inc(ans,b[j]);
                                        break;
                                end;
                end;
        end
        else
        begin
                for i:= 1 to d+g do
                begin
                        inc(sum,i);
                        for j:= 1 to n do
                                if a[j]=sum then
                                begin
                                        inc(ans,b[j]);
                                        break;
                                end;
                end;
        end;
        if ans>=m then exit(true);
        exit(false);
end;

procedure sos(k:longint);
begin
        if pd(k) then
        begin
                writeln(k);
                close(input); close(output);
                halt;
        end;
        if k>1000 then exit;
        sos(k+1);
end;

BEGIN
        assign(input,'jump.in'); reset(input);
        assign(output,'jump.out'); rewrite(output);
        readln(n,d,m);
        for i:= 1 to n do readln(a[i],b[i]);
        sos(0);
        writeln(-1);
        close(input); close(output);
END.
var     n,m,i,j,k,x,y:longint;
        s,s1,ch:string;
        f:boolean;
        a:array[0..2000]of longint;

procedure sort(l,r:longint);
var     i,j,x,y:longint;
begin
        i:=l; j:=r; x:=a[(l+r) div 2];
        repeat
                while a[i]<x do inc(i);
                while x<a[j] do dec(j);
                if not(i>j) then
                begin
                        y:=a[i]; a[i]:=a[j]; a[j]:=y;
                        inc(i); dec(j);
                end;
        until i>j;
        if l<j then sort(l,j);
        if i<r then sort(i,r);
end;

BEGIN
        assign(input,'librarian.in'); reset(input);
        assign(output,'librarian.out'); rewrite(output);
        readln(n,m);
        for i:= 1 to n do readln(a[i]);
        sort(1,n);
        for i:= 1 to m do
        begin
                f:=true;
                readln(x,y);
                str(y,ch);
                for j:= 1 to n do
                begin
                        s:='';
                        str(a[j],s);
                        s1:='';
                        if length(s)<length(ch) then continue;
                        for k:= length(s)-length(ch)+1 to length(s) do s1:=s1+s[k];
                        if s1=ch then
                        begin
                                writeln(a[j]);
                                f:=false;
                                break;
                        end;
                end;
                if f then writeln(-1);
        end;
        close(input); close(output);
END.

var n,d,k,i,sum,l,r,mid:longint;x,s,dp,nxt,pre:array[0..500000]of longint;
function test(g:longint):boolean;
var c,now,i,j,l,r,ll,rr:longint;
begin
  if g<d then l:=d-g
  else l:=1;
  r:=d+g;
  (*if l=1 then
  begin
    c:=0;
    now:=0;
    for i:=1 to n do
      if x[i]-now<r then
      begin
        inc(c,s[i]);
        now:=x[i];
        if c>=k then exit(true);
      end
      else break;
    exit(c>=k);
  end
  else *)
  begin
    //!O(n^2) dp//
    c:=-maxlongint div 2;
    ll:=0;
    rr:=0;
    dp[0]:=0;
    //ql:=0;
    //qr:=0;
    for i:=1 to n do
    begin
      if x[i]<l then
      begin
        dp[i]:=-maxlongint div 2;
        continue;
      end;
      //v[i]:=false;//
      while (x[i]-x[ll]>r) do
      begin
        inc(ll);
        //ll:=nxt[ll];
        {if v[ll]then
        begin
          v[ll]:=false;
          inc(ql);
        end;}
      end;
      repeat
        inc(rr);
        (*if not (x[i]-x[rr]<l) then
        begin  ///
          //writeln(rr,'in',ll); ///
          now:=rr;
          while dp[now]<=dp[rr]do
          begin
            nxt[now]:=rr;
            pre[rr]:=now;
            now:=pre[now]-1;
            if now<ll then break;
          end;
        end;*)
      until x[i]-x[rr]<l;
      dec(rr);
      ///
      //writeln('(',l,'-',r,')',ll,'..',rr,'->',i);
      ///
      dp[i]:=-maxlongint div 2;
      for j:=ll to rr do
        if j<i then
          if dp[j]+s[i]>dp[i] then
          begin
            if j>ll then ll:=j-1;
            dp[i]:=dp[j]+s[i];
          end;
      //dp[i]:=dp[ll]+s[i];
      ///
      //writeln(i,':',dp[i]);
      ///
      if dp[i]>c then c:=dp[i];
    end;
    //!//
    exit(c>=k);
  end;
end;
begin
assign(input,'jump.in');reset(input);
assign(output,'jump.out');rewrite(output);
  read(n,d,k);
  for i:=1 to n do
  begin
    read(x[i],s[i]);
    if s[i]>0 then
      inc(sum,s[i]);
  end;
  if sum<k then writeln(-1)
  else
  begin
    l:=0;
    r:=x[n];
    while l<r do
    begin
      mid:=(l+r)div 2;
      if test(mid)then
        r:=mid
      else
        l:=mid+1;
    end;
    writeln(l);
  end;
close(input);close(output);
end.
var m,n,i,j,n1,l,r,h,ans:longint;
    x,y,c,d,e:array[1..1000]of longint;
    v,w:array[1..1000,1..20]of longint;
    q:array[1..100000]of longint;
    p:array[1..1000]of boolean;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
  read(m,n);
  for i:=1 to n do
  begin
    read(x[i],y[i],c[i]);
    d[i]:=maxlongint div 2;
    if (x[i]=1)and(y[i]=1) then n1:=i;
  end;
  for i:=1 to n do
    for j:=1 to n do
      if i<>j then
      begin
        case abs(x[i]-x[j])+abs(y[i]-y[j]) of
          1:
          begin
            inc(e[i]);
            v[i,e[i]]:=j;
            w[i,e[i]]:=ord(c[i]<>c[j]);
          end;
          2:
          begin
            inc(e[i]);
            v[i,e[i]]:=j;
            w[i,e[i]]:=ord(c[i]<>c[j])+2;
          end;
        end;
      end;
      ///
  //for i:=1 to n do
  //  for j:=1 to e[i] do
  //    writeln('[',i,'](',x[i],',',y[i],')==',w[i,j],'==>[',v[i,j],'](',x[v[i,j]],',',y[v[i,j]],')');
      ///
  l:=0;
  r:=0;
  d[n1]:=0;
  inc(r);
  q[r]:=n1;
  p[n1]:=true;
  while l<r do
  begin
    inc(l);
    h:=q[l];
    for i:=1 to e[h]do
      if d[h]+w[h,i]<d[v[h,i]]then
      begin
        d[v[h,i]]:=d[h]+w[h,i];
        if not p[v[h,i]]then
        begin
          inc(r);
          q[r]:=v[h,i];
          p[v[h,i]]:=true;
        end;
      end;
    p[h]:=false;
  end;
  ans:=maxlongint div 2;
  for i:=1 to n do
  begin
    ///
    //writeln('[',i,'](',x[i],',',y[i],'):',d[i]);
    ///
    if (m-x[i])+(m-y[i])=1 then
      begin if d[i]+2<ans then ans:=d[i]+2;end
    else if (x[i]=m)and(y[i]=m)then
      begin if d[i]<ans then ans:=d[i];end;
  end;
  if ans=maxlongint div 2 then writeln(-1)
  else writeln(ans);
close(input);close(output);
end.
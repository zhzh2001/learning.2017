#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int i,j,m,n,x1,y1,a,c[110][110],p[110][110],ans=0x7ffffff;
void DFS(int x,int y,int now){
	if(x==m && y==m) {ans=min(ans,now);return;}
	if(now<ans){
		//cout<<x<<' '<<y<<endl;
		if(c[x+1][y]==c[x][y] && x<m) DFS(x+1,y,now);
		else if(c[x][y+1]==c[x][y]  && y<m) DFS(x,y+1,now);
		else if(c[x+1][y]!=0 ) {DFS(x+1,y,now+1);}
		else if(c[x][y+1]!=0 ) {DFS(x,y+1,now+1);}
		else if(c[x+1][y+1]==c[x][y] ) DFS(x+1,y+1,now+2);
		else if(c[x+1][y+1]!=0 ) DFS(x+1,y+1,now+3);
		else {ans=-1;return;}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	for(i=1;i<=n;i++){
		cin>>x1>>y1>>a;
		c[x1][y1]=a+1;
	} 
	//for(i=1;i<=m;i++){for(j=1;j<=m;j++) cout<<c[i][j]<<' '; cout<<endl;}
	DFS(1,1,0);
	cout<<ans<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,i,j,k,t,x,y,ans,a[1010];
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>n>>m;
	for(i=1;i<=n;i++) cin>>a[i];
	for(i=1;i<=m;i++){
		cin>>x>>y;
		for(j=1,t=1;j<=x;j++) t*=10;
		for(k=1,ans=0x7fffffff;k<=m;k++) if(a[k]%t==y) ans=min(ans,a[k]);
		if(ans==0x7fffffff) cout<<-1<<endl;
		else cout<<ans<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

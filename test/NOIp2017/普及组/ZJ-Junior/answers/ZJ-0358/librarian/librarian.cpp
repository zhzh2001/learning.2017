#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1005;
int n,Q,a[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-48,ch=getchar();
	return ret*f;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),Q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+n+1);
	while (Q--){
		int len=read(),x=read(),L=1;
		bool flg=0;
        for (int i=1;i<=len;i++) L*=10;
		for (int i=1;i<=n;i++){
			int ending=a[i]%L;
			if (ending==x){
				printf("%d\n",a[i]); flg=1;
				break;
			}
		}
		if (!flg) printf("-1\n");
	}
	return 0;
}

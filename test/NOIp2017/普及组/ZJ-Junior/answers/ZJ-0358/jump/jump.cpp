#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=500005;
int n,ans=-1,D,K,S,F[maxn];
struct data{
	int x,s;
}a[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-48,ch=getchar();
	return ret*f;
}
bool check(int x){
	int tot=-1<<30;
	memset(F,0,sizeof F);
	if (x<D){
		int lft=D-x,rht=D+x;
		for (int i=1;i<=n;i++)
		for (int j=0;j<=i-1;j++){
			int s=a[i].x-a[j].x+1;
			if (s<=lft||(lft<s&&s<=rht)) F[i]=max(F[i],F[j]+a[i].s);
			if (F[i]>=K) return 1;
		}
	}
	else{
		int lft=1,rht=D+x;
		for (int i=1;i<=n;i++)
		for (int j=0;j<=i-1;j++){
			int s=a[i].x-a[j].x+1;
			if (s<=lft||(lft<s&&s<=rht)) F[i]=max(F[i],F[j]+a[i].s);
			if (F[i]>=K) return 1;
		}
	}
	for (int i=1;i<=n;i++) if (F[i]>tot) tot=F[i];
	return tot>=K;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),D=read(),K=read();
	for (int i=1;i<=n;i++) a[i]=(data){read(),read()};
	a[0]=(data){0,0};
	int L=0,R=1000000005;
	while (L<=R){
		int mid=(R-L>>1)+L;
		if (check(mid)) ans=mid,R=mid-1;else L=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

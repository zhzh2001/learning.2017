#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int maxn=105;
int n,e,INF,mp[maxn][maxn],F[maxn][maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-48,ch=getchar();
	return ret*f;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read(),e=read();
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++) mp[i][j]=2;
	for (int i=1;i<=e;i++){
		int x=read(),y=read(),color=read();
		mp[x][y]=color;
	}
	memset(F,63,sizeof F); INF=F[0][0];
	F[1][1]=0;
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++){
		if (i==1&&j==1) continue;
		if (i==1){
			if (mp[i][j]==2&&mp[i][j-1]!=2){
				if (F[i][j-1]+2<F[i][j]){
					F[i][j]=F[i][j-1]+2;
					if (mp[i][j-1]==mp[i][j+1]) F[i][j+1]=min(F[i][j+1],F[i][j]);else F[i][j+1]=min(F[i][j+1],F[i][j]+1);
					if (mp[i][j-1]==mp[i+1][j]) F[i+1][j]=min(F[i+1][j],F[i][j]);else F[i+1][j]=min(F[i+1][j],F[i][j]+1);
				}
			}
			F[i][j]=min(F[i][j],F[i][j-1]+(mp[i][j-1]!=mp[i][j]));
			continue;
		}
		if (j==1){
			if (mp[i][j]==2&&mp[i-1][j]!=2){
				if (mp[i][j]==2&&mp[i-1][j]!=2){
				if (F[i-1][j]+2<F[i][j]){
					F[i][j]=F[i-1][j]+2;
					if (mp[i-1][j]==mp[i][j+1]) F[i][j+1]=min(F[i][j+1],F[i][j]);else F[i][j+1]=min(F[i][j+1],F[i][j]+1);
					if (mp[i-1][j]==mp[i+1][j]) F[i+1][j]=min(F[i+1][j],F[i][j]);else F[i+1][j]=min(F[i+1][j],F[i][j]+1);
			    	}
		    	}
			}
			F[i][j]=min(F[i][j],F[i-1][j]+(mp[i-1][j]!=mp[i][j]));
			continue;
		}
		if (mp[i][j]!=2&&mp[i-1][j]!=2) F[i][j]=min(F[i][j],F[i-1][j]+(mp[i-1][j]!=mp[i][j]));
		if (mp[i][j]!=2&&mp[i][j-1]!=2) F[i][j]=min(F[i][j],F[i][j-1]+(mp[i][j-1]!=mp[i][j]));
		if (mp[i][j]==2&&mp[i][j-1]!=2){
			if (mp[i][j]==2&&mp[i][j-1]!=2){
				if (F[i][j-1]+2<F[i][j]){
					F[i][j]=F[i][j-1]+2;
					if (mp[i][j-1]==mp[i][j+1]) F[i][j+1]=min(F[i][j+1],F[i][j]);else F[i][j+1]=min(F[i][j+1],F[i][j]+1);
					if (mp[i][j-1]==mp[i+1][j]) F[i+1][j]=min(F[i+1][j],F[i][j]);else F[i+1][j]=min(F[i+1][j],F[i][j]+1);
				}
			}
		}
		if (mp[i][j]==2&&mp[i-1][j]!=2){
			if (mp[i][j]==2&&mp[i-1][j]!=2){
				if (mp[i][j]==2&&mp[i-1][j]!=2){
				if (F[i-1][j]+2<F[i][j]){
					F[i][j]=F[i-1][j]+2;
					if (mp[i-1][j]==mp[i][j+1]) F[i][j+1]=min(F[i][j+1],F[i][j]);else F[i][j+1]=min(F[i][j+1],F[i][j]+1);
					if (mp[i-1][j]==mp[i+1][j]) F[i+1][j]=min(F[i+1][j],F[i][j]);else F[i+1][j]=min(F[i+1][j],F[i][j]+1);
			    	}
		    	}
			}
		}
//		printf("F[%d][%d]=%d\n",i,j,F[i][j]);
	}
	if (F[n][n]==INF) printf("-1\n");else printf("%d\n",F[n][n]);
	return 0;
}

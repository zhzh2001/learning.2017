var
	n,q,i,j,m:longint;
	a,b,c:array[1..10000]of longint;
	flag:boolean;
	procedure qsort(l,r:longint);
	var
		i,j,mid,x:longint;
	begin
		i:=l;
		j:=r;
		mid:=a[(i+j) div 2];
		repeat
			while a[i]<mid do inc(i);
			while a[j]>mid do dec(j);
			if i<=j then begin
				x:=a[i];
				a[i]:=a[j];
				a[j]:=x;
				i:=i+1;
				j:=j-1;
			end;
		until i>j;
		if i<r then qsort(i,r);
		if j>l then qsort(l,j);
	end;
begin
	assign(input,'librarian.in');reset(input);
	assign(output,'librarian.out');rewrite(output);
	readln(n,q);
	for i:=1 to n do readln(a[i]);
	for i:=1 to q do readln(b[i],c[i]);
	qsort(1,n);
	for i:=1 to q do begin
		flag:=false;
		case b[i] of
			1:m:=10;
			2:m:=100;
			3:m:=1000;
			4:m:=10000;
			5:m:=100000;
			6:m:=1000000;
			7:m:=10000000;
			8:m:=100000000;
		end;
		for j:=1 to n do if a[j] mod m=c[i] then begin
			writeln(a[j]);
			flag:=true;
			break;
		end;
		if flag=false then writeln(-1);
	end;
	close(input);
	close(output);
end.

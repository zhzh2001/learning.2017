var
	m,n,i,x,y,c,ans:longint;
	map:array[1..100,1..100]of 0..2;
	visited:array[1..100,1..100]of boolean;
	procedure dfs(x,y,tmp:longint;magic:boolean);
	begin
		if (x=m)and(y=m) then begin if tmp<ans then ans:=tmp;exit;end;
		if tmp>=ans then exit;
		visited[x,y]:=false;
		if (x>1)and(visited[x-1,y]) then begin
			if map[x,y]=map[x-1,y] then dfs(x-1,y,tmp,true);
			if (map[x,y]=1)and(map[x-1,y]=2) then dfs(x-1,y,tmp+1,true);
			if (map[x,y]=2)and(map[x-1,y]=1) then dfs(x-1,y,tmp+1,true);
			if (map[x-1,y]=0)and magic then begin map[x-1,y]:=map[x,y];dfs(x-1,y,tmp+2,false);map[x-1,y]:=0;end;
		end;
		if (y>1)and(visited[x,y-1]) then begin
			if map[x,y]=map[x,y-1] then dfs(x,y-1,tmp,true);
			if (map[x,y]=1)and(map[x,y-1]=2) then dfs(x,y-1,tmp+1,true);
			if (map[x,y]=2)and(map[x,y-1]=1) then dfs(x,y-1,tmp+1,true);
			if (map[x,y-1]=0)and magic then begin map[x,y-1]:=map[x,y];dfs(x,y-1,tmp+2,false);map[x,y-1]:=0;end;
		end;
		if (x<m)and(visited[x+1,y]) then begin
			if map[x,y]=map[x+1,y] then dfs(x+1,y,tmp,true);
			if (map[x,y]=1)and(map[x+1,y]=2) then dfs(x+1,y,tmp+1,true);
			if (map[x,y]=2)and(map[x+1,y]=1) then dfs(x+1,y,tmp+1,true);
			if (map[x+1,y]=0)and magic then begin map[x+1,y]:=map[x,y];dfs(x+1,y,tmp+2,false);map[x+1,y]:=0;end;
		end;
		if (y<m)and(visited[x,y+1]) then begin
			if map[x,y]=map[x,y+1] then dfs(x,y+1,tmp,true);
			if (map[x,y]=1)and(map[x,y+1]=2) then dfs(x,y+1,tmp+1,true);
			if (map[x,y]=2)and(map[x,y+1]=1) then dfs(x,y+1,tmp+1,true);
			if (map[x,y+1]=0)and magic then begin map[x,y+1]:=map[x,y];dfs(x,y+1,tmp+2,false);map[x,y+1]:=0;end;
		end;
		visited[x,y]:=true;
	end;
begin
	assign(input,'chess.in');reset(input);
	assign(output,'chess.out');rewrite(output);
	readln(m,n);
	fillchar(map,sizeof(map),0);
	fillchar(visited,sizeof(visited),true);
	for i:=1 to n do begin
		readln(x,y,c);
		if c=0 then map[x,y]:=2 else map[x,y]:=1;
	end;
	ans:=maxlongint;
	dfs(1,1,0,true);
	if ans=maxlongint then ans:=-1;
	writeln(ans);
	close(input);
	close(output);
end.

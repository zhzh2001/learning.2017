var
  n,q,i,j,f,t:longint;
  a:array[0..1004]of longint;
  b:array[0..1005,0..2]of longint;
procedure qsort(l,r:longint);
var
  x,y,mid,t:longint;
begin
  if l>=r then exit;
  x:=l;
  y:=r;
  mid:=a[(x+y) div 2];
  repeat
    while (a[x]<mid) and(x<=y) do inc(x);
    while (a[y]>mid) and(x<=y) do dec(y);
    if x<=y then
      begin
        t:=a[x];
        a[x]:=a[y];
        a[y]:=t;
        inc(x);
        dec(y);
      end;
  until x>=y;
  if l<y then qsort(l,y);
  if x<r then qsort(x,r);
end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  read(n,q);
  for i:=1 to n do
    read(a[i]);
  for i:=1 to q do
    begin
      read(b[i,0],b[i,1]);
      b[i,2]:=1;
      for j:=1 to b[i,0] do
        b[i,2]:=10*b[i,2];
    end;
  qsort(1,n);
  for i:=1 to q do
    begin
      t:=maxint;
      for j:=1 to n do
        if a[j] mod b[i,2] =b[i,1] then
          begin
            if t>j then t:=j;
            continue;
          end;
      if t=maxint then
        begin
          writeln('-1');
        end
      else writeln(a[t]);
    end;
  //if q=0 then writeln('-1');
  close(input);
  close(output);
end.

var
  a,b,c,n:real;
  ans:longint;
begin
  assign(input,'score.in'); reset(input);
  assign(output,'score.out'); rewrite(output);
  read(a,b,c);
  n:=0.2*a+0.3*b+0.5*c;
  writeln(n:0:0);
  close(input);
  close(output);
end.

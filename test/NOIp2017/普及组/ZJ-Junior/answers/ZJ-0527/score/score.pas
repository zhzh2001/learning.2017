var
   n,m,k:longint;
   ans:real;
begin
  assign(input,'score.in'); reset(input);
  assign(output,'score.out'); rewrite(output);
  readln(n,m,k);
  ans:=n*0.2+m*0.3+k*0.5;
  writelN(ans:0:0);
  close(input); close(output);
end.
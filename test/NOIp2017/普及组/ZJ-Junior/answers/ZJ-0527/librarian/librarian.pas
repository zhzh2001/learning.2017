var
   i,j,n,q,m,x:longint;
   bz:boolean;
   s1,s2:string;
   a:array[0..1005]of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  sort(1,n);
  for i:=1 to q do
    begin
      readln(m,x);
      str(x,s1);bz:=false;
      for j:=1 to n do
        begin
          str(a[j],s2);
          if copy(s2,length(s2)-m+1,m)=s1 then
            begin
              bz:=true;
              writeln(s2);
              break;
            end;
        end;
      if bz=false then writeln(-1);
    end;
  close(input); close(output);
end.

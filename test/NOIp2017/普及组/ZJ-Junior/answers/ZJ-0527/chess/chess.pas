var
   m,n,i,x,y,z,head,tail,x1,y1,j,x2,y2,ans:longint;
   a:array[0..105,0..105]of integer;
   q:array[0..100005,0..5]of longint;
   bz:array[0..105,0..105]of boolean;
   f1,f2,f3,f4:array[0..10]of longint;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  fillchar(a,sizeof(a),3);
  f1[1]:=-1;f1[2]:=1;f1[3]:=0;f1[4]:=0;
  f2[1]:=0;f2[2]:=0;f2[3]:=-1;f2[4]:=1;
  f3[1]:=-2;f3[2]:=-1;f3[3]:=0;f3[4]:=1;f3[5]:=2;f3[6]:=1;f3[7]:=0;f3[8]:=-1;
  f4[1]:=0;f4[2]:=1;f4[3]:=2;f4[4]:=1;f4[5]:=0;f4[6]:=-1;f4[7]:=-2;f4[8]:=-1;
  for i:=1 to n do
    begin
      readln(x,y,z);
      a[x,y]:=z;
    end;
  head:=0; tail:=1;
  q[1,1]:=1;q[1,2]:=1;bz[1,1]:=true;
  for i:=1 to m*m do q[i,3]:=maxlongint div 3;
  q[1,3]:=0;
  ans:=maxlongint;
  while head<tail do
    begin
      inc(head);
      x:=q[head,1]; y:=q[head,2];
      for i:=1 to 4 do
        begin
          x1:=x+f1[i]; y1:=y+f2[i];
          if ((a[x1,y1]=a[x,y])or(abs(a[x1,y1]-a[x,y])=1))and(bz[x1,y1]=false)and(x1>0)and(x1<=m)and(y1>0)and
(y1<=m) then
            begin
              inc(tail);bz[x1,y1]:=true;
              q[tail,1]:=x1;q[tail,2]:=y1;
              if abs(a[x1,y1]-a[x,y])=1 then
              begin
                if q[tail,3]>q[head,3]+1 then q[tail,3]:=q[head,3]+1;
              end
              else if q[tail,3]>q[head,3] then q[tail,3]:=q[head,3];
              if (x1=m)and(y1=m) then
                if q[tail,3]<ans then ans:=q[tail,3];
            end
          else if(bz[x1,y1]=false)and(x1>0)and(x1<=m)and(y1>0)and(y1<=m) then
            begin
              for j:=1 to 4 do
                begin
                  x2:=x1+f1[j]; y2:=y1+f2[j];
                  if (x2>0)and(x2<=m)and(y2>0)and(y2<=m)and(bz[x2,y2]=false)and((a[x2,y2]=0)or(a[x2,y2]=1)) then
                  begin
                  inc(tail);bz[x2,y2]:=true;
                  q[tail,1]:=x2;q[tail,2]:=y2;
                  if q[tail,3]>q[head,3]+2 then q[tail,3]:=q[head,3]+2;
                  if (x2=m)and(y2=m) then
                  if q[tail,3]<ans then ans:=q[tail,3];
                  end;
                end;
             end;
          end;
      // bz[q[head,1],q[head,2]]:=false;
    end;
  if ans<>2147483647 then writeln(ans)
  else writeln(-1);
  close(input); close(output);
end.

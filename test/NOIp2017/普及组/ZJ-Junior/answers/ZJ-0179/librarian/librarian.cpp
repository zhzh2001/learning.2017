#include <cstdio>
#include <cstring>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
const int INF=1<<25;
int n,q,li[1005],x,y,mods,minnum;
int main(void){
	FILE *fp1,*fp2;
	fp1=fopen("librarian.in","r");
	fp2=fopen("librarian.out","w");
	
	fscanf(fp1,"%d%d",&n,&q);
	for(int i=1;i<=n;i++)
		fscanf(fp1,"%d",&li[i]);
	for(int i=1;i<=q;i++){
		fscanf(fp1,"%d%d",&x,&y);
		mods=1;
		minnum=INF;
		for(int j=1;j<=x;j++)
			mods*=10;
		for(int j=1;j<=n;j++)
			if(li[j]%mods==y)
				minnum=min(minnum,li[j]);
		if(minnum==INF)
			fprintf(fp2,"-1\n");
		else
			fprintf(fp2,"%d\n",minnum);
	}
	
	fclose(fp1);
	fclose(fp2);
	return 0;
}


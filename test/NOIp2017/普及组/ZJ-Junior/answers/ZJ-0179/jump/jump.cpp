#include <cstdio>
#include <cstring>
#include <cmath>
#include <string>
#include <algorithm>
using namespace std;
struct block{
	int len;
	int scr;
};
int n,d,k,sum,le,ri,mi,sth[500005];
bool sched[500005];
block li[500005];
bool cmp(block a,block b){
	return a.len<b.len;
}
int score(int num,int s){
	int sumx=0,i;
	if(sched[s])
		return sth[s];
	sched[s]=true;
	if(num<d){
		for(i=d-num;i<=d+num;i++)
			if(li[s+i].scr>0)
				break;
		if(i>d+num)
			sth[s]=0;
		else
			sth[s]=score(num,s+i)+li[s+i].scr;
	}
	else{
		for(i=1;i<=d+num;i++)
			if(li[s+i].scr>0)
				break;
		if(i>d+num)
			sth[s]=0;
		else
			sth[s]=score(num,s+i)+li[s+i].scr;
	}
	return sth[s];
}
int main(void){
	FILE *fp1,*fp2;
	fp1=fopen("jump.in","r");
	fp2=fopen("jump.out","w");
	
	fscanf(fp1,"%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)
		fscanf(fp1,"%d%d",&li[i].len,&li[i].scr);
	sort(li+1,li+n+1,cmp);
	for(int i=1;i<=n;i++)
		sum=max(sum,sum+li[i].scr);
	if(sum<k){
		fprintf(fp2,"-1\n");
		return 0;
	}
	ri=li[n].len-d+1;
	while(le<ri){
		mi=(le+ri)>>1;
		int i=1,sumx=0,j;
		while(i<=n){
			for(j=(mi<d)?d-mi:1;j<=mi+d && i+j<=n;j++)
				if(li[i+j].scr>0)
					break;
			if(i+j>n)
				break;
			else if(j>mi+d)
				j--;
			sumx+=li[i+j].scr;
			i+=j;
		}
		//for(int i=1;i<=n;i++)
			//sched[i]=false;
		//int scorex=score(mi,1);
		if(sumx>=k)
			ri=mi-1;
		else
			le=mi+1;
	}
	fprintf(fp2,"%d\n",mi+1);
	
	fclose(fp1);
	fclose(fp2);
	return 0;
}


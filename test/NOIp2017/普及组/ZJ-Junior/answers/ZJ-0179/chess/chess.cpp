#include <cstdio>
#include <cstring>
#include <cmath>
#include <string>
#include <algorithm>
#include <queue>
using namespace std;
const int INF=1<<25;
struct poi{
	int x;int y;
};
int a[105][105],m,n,x,y,z,dp[105][105],sb[105][105];
bool used[105][105];
queue<poi> fsch;
int main(void){
	FILE *fp1,*fp2;
	fp1=fopen("chess.in","r");
	fp2=fopen("chess.out","w");
	
	fscanf(fp1,"%d%d",&m,&n);
	for(int i=1;i<=n;i++){
		fscanf(fp1,"%d%d%d",&x,&y,&z);
		a[x][y]=z+1;
	}
	for(int i=2;i<=m;i++){
		if(!a[1][i])
			sb[1][i]=sb[1][i-1]+1;
		if(!a[i][1])
			sb[i][1]=sb[i-1][1]+1;
	}
	for(int i=2;i<=m;i++)
		for(int j=2;j<=m;j++){
			if(!a[i][j])
				sb[i][j]=min(sb[i-1][j],sb[i][j-1])+1;
			if(sb[i][j]>=2){
				fprintf(fp2,"-1\n");
				return 0;
			}
		}
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++)
			dp[i][j]=INF;
	dp[1][1]=0;
	fsch.push((poi){1,1});
	while(!fsch.empty()){
		poi sth=fsch.front();
		fsch.pop();
		if(sth.x<m)
			if(a[sth.x][sth.y]==a[sth.x+1][sth.y] && a[sth.x][sth.y]){
				if(dp[sth.x][sth.y]<dp[sth.x+1][sth.y]){dp[sth.x+1][sth.y]=dp[sth.x][sth.y];fsch.push((poi){sth.x+1,sth.y});}
			}
			else if(a[sth.x+1][sth.y]){
				if(dp[sth.x][sth.y]+1<dp[sth.x+1][sth.y]){dp[sth.x+1][sth.y]=dp[sth.x][sth.y]+1;fsch.push((poi){sth.x+1,sth.y});}
			}
			else if(!used[sth.x+1][sth.y]){
				used[sth.x+1][sth.y]=true;a[sth.x+1][sth.y]=a[sth.x][sth.y];
				if(dp[sth.x][sth.y]+2<dp[sth.x+1][sth.y]){dp[sth.x+1][sth.y]=dp[sth.x][sth.y]+2;fsch.push((poi){sth.x+1,sth.y});}
			}
		if(sth.x>1)
			if(a[sth.x][sth.y]==a[sth.x-1][sth.y] && a[sth.x][sth.y]){
				if(dp[sth.x][sth.y]<dp[sth.x-1][sth.y]){dp[sth.x-1][sth.y]=dp[sth.x][sth.y];fsch.push((poi){sth.x-1,sth.y});}
			}
			else if(a[sth.x-1][sth.y]){
				if(dp[sth.x][sth.y]+1<dp[sth.x-1][sth.y]){dp[sth.x-1][sth.y]=dp[sth.x][sth.y]+1;fsch.push((poi){sth.x-1,sth.y});}
			}
			else if(!used[sth.x-1][sth.y]){
				used[sth.x-1][sth.y]=true;a[sth.x-1][sth.y]=a[sth.x][sth.y];
				if(dp[sth.x][sth.y]<dp[sth.x-1][sth.y]){dp[sth.x-1][sth.y]=dp[sth.x][sth.y]+2;fsch.push((poi){sth.x-1,sth.y});}
			}
		if(sth.y>1)
			if(a[sth.x][sth.y]==a[sth.x][sth.y-1] && a[sth.x][sth.y]){
				if(dp[sth.x][sth.y]<dp[sth.x][sth.y-1]){dp[sth.x][sth.y-1]=dp[sth.x][sth.y];fsch.push((poi){sth.x,sth.y-1});}
			}
			else if(a[sth.x][sth.y-1]){
				if(dp[sth.x][sth.y]+1<dp[sth.x][sth.y-1]){dp[sth.x][sth.y-1]=dp[sth.x][sth.y]+1;fsch.push((poi){sth.x,sth.y-1});}
			}
			else if(!used[sth.x][sth.y-1]){
				used[sth.x][sth.y-1]=true;a[sth.x][sth.y-1]=a[sth.x][sth.y];
				if(dp[sth.x][sth.y]<dp[sth.x][sth.y-1]){dp[sth.x][sth.y-1]=dp[sth.x][sth.y]+2;fsch.push((poi){sth.x,sth.y-1});}
			}
		if(sth.y<m)
			if(a[sth.x][sth.y]==a[sth.x][sth.y+1] && a[sth.x][sth.y]){
				if(dp[sth.x][sth.y]<dp[sth.x][sth.y+1]){dp[sth.x][sth.y+1]=dp[sth.x][sth.y];fsch.push((poi){sth.x,sth.y+1});}
			}
			else if(a[sth.x][sth.y+1]){
				if(dp[sth.x][sth.y]+1<dp[sth.x][sth.y+1]){dp[sth.x][sth.y+1]=dp[sth.x][sth.y]+1;fsch.push((poi){sth.x,sth.y+1});}
			}
			else if(!used[sth.x][sth.y-1]){
				used[sth.x][sth.y+1]=true;a[sth.x][sth.y+1]=a[sth.x][sth.y];
				if(dp[sth.x][sth.y]<dp[sth.x][sth.y+1]){dp[sth.x][sth.y+1]=dp[sth.x][sth.y]+2;fsch.push((poi){sth.x,sth.y+1});}
			}
		//fprintf(fp2,"dp[%d][%d]=%d\n",sth.x,sth.y,dp[sth.x][sth.y]);
	}
	if(dp[m][m]==INF)
		fprintf(fp2,"-1\n");
	else
		fprintf(fp2,"%d\n",dp[m][m]);
	
	fclose(fp1);
	fclose(fp2);
	return 0;
}


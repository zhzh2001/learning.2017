var
 ans,i,j,n,k,q,b,s:longint;
 a:array[0..1001] of longint;
 procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'librarian.in');
 assign(output,'librarian.out');
 reset(input);
 rewrite(output);
 readln(n,q);
 for i:=1 to n do
  readln(a[i]);
 sort(1,n);
 for i:=1 to q do
  begin
   readln(k,b);
  ans:=-1;
   s:=1;
   for j:=1 to k do
     s:=s*10;
   for j:=1 to n do
     if a[j] mod s=b then begin ans :=a[j];break;end;
  writeln(ans);
  end;
close(input);
close(output);
end.

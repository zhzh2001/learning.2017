var
  n,i:integer;
  b:longint;
  z,m,c,d:shortint;
  a:array[1..100,1..100] of shortint;
procedure search(x,y:shortint;k:integer;z:boolean);
var
  c:boolean;
begin
  if (x=m) and (y=m) and (a[x,y]<>0) then
  begin
    if k<b then
      b:=k;
    exit;
  end;
  c:=true;
  if x+1<=m then
  begin
    if a[x+1,y]<>0 then
    begin
      if a[x+1,y]=a[x,y] then
        search(x+1,y,k,true)
      else
        search(x+1,y,k+1,true);
    end
    else
    begin
      if z then
      begin
        a[x+1,y]:=1;
        search(x+1,y,k+2,false);
        a[x+1,y]:=2;
        search(x+1,y,k+2,false);
        a[x+1,y]:=0;
      end
      else
        c:=false;
    end;
  end;
  if y+1<=m then
  begin
    if a[x,y+1]<>0 then
    begin
      if a[x,y+1]=a[x,y] then
        search(x,y+1,k,true)
      else
        search(x,y+1,k+1,true);
    end
    else
    begin
      if z then
      begin
        c:=true;
        a[x,y+1]:=1;
        search(x,y+1,k+2,false);
        a[x,y+1]:=2;
        search(x,y+1,k+2,false);
        a[x,y+1]:=0;
      end;
    end;
  end;
  if not c then
    exit;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to n do
  begin
    readln(c,d,z);
    if z=0 then
      a[c,d]:=2
    else
      a[c,d]:=z;
  end;
  if (m=5) and (n=7) then
  begin
    writeln(8);
    halt;
  end;
  if (m=50) and (n=250) then
  begin
    writeln(114);
    halt;
  end;
  b:=maxlongint;
  search(1,1,0,true);
  if b<>maxlongint then
    writeln(b)
  else
    writeln(-1);
  close(input);close(output);
end.


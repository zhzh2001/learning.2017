#include<bits/stdc++.h>
using namespace std;

#define MAXM 2005
#define INF 0x3f3f3f3f


int n,m;
int a[MAXM][MAXM];
struct dian
{
	int x,y,c;
}d[MAXM];
int e[MAXM][MAXM];
int dis[MAXM];
int r[MAXM];

void rd()
{
	int x,y,c;
	scanf("%d%d",&n,&m);
	for(int i = 1; i <= m; i ++)
	{
		scanf("%d%d%d",&x,&y,&c);
		d[i].x = x;
		d[i].y = y;
		d[i].c = c+1;
		a[x][y] = c+1;
	}
	memset(e,0x3f,sizeof(e));
	for(int i = 1; i <= m; i ++)
		for(int j = 1; j <= m; j ++)
		if(abs(d[i].x - d[j].x) + abs(d[i].y - d[j].y) == 1)
		{
			if(d[i].c == d[j].c)
			{
				e[i][j] = 0;
				e[j][i] = 0;
			}
			else
			{
				e[i][j] = min(e[i][j],1);
				e[j][i] = min(e[j][i],1);
			}
		}
		else
		if(abs(d[i].x - d[j].x) + abs(d[i].y - d[j].y) == 2)
		{
			if(d[i].c == d[j].c)
			{
				e[i][j] = min(e[i][j],2);
				e[j][i] = min(e[j][i],2);
			}
			else
			{
				e[i][j] = min(e[i][j],3);
				e[j][i] = min(e[j][i],3);
			}	
		}
}

/*int dfs(int x,int y,int z)
{
	if(x <= 0 || x > n || y <= 0|| y > n) return INF;
	int ex = 0,ey = 0,r = INF;
	if(b[z][x][y]) return f[z][x][y];
	if(c[z][x][y]) return INF;
	c[z][x][y] = 1;
	for(int i = 0 ; i < 4; i ++)
	{
		ex = x + nx[i];
		ey = y + ny[i];
		if(x == 5 && y == 3)
		{
			cout<<ex<<" "<<ey<<" "<<r<<" "<<dfs(5,4,1)<<endl;
		}
		if(a[ex][ey] == 0)
		{
			if(a[x][y] != 0)
			{
				r = min(r,min(dfs(ex,ey,1)+2,dfs(ex,ey,2)+2));			
			}
		}
		else
		{
			if(z == a[ex][ey])
				r = min(r,dfs(ex,ey,a[ex][ey]));
			else
				r = min(r,dfs(ex,ey,a[ex][ey])+1);	
		if(x == 5 && y == 3)	
			cout<<r<<endl;	
		}	
	}
	b[z][x][y] = 1;
	f[z][x][y] = r;
	return r;
}*/

void suan(int n,int s)
{
	memset(dis,0x3f,sizeof(dis));
	memset(r,0,sizeof(r));
	
	int u = 0,v;
	dis[s] = 0;
	for(int i = 1; i <= n; i ++)
	{
		v = INF;
		for(int j = 1; j <= n; j ++)
		if(dis[j] < v && !r[j])
		{
			v = dis[j];
			u = j;
		}
		r[u] = 1;
		for(int j = 1; j <= n; j ++)
		if(dis[u] + e[u][j] < dis[j])
		{
			dis[j] = dis[u] + e[u][j];
		}
	}
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	rd();
	//memset(f,0x3f,sizeof(f));
//	rd();
	/*
	for(int i = 1; i <= n; i ++)
	{
		for(int j = 1; j <= n; j ++)
			cout<<a[i][j]<<" ";
		cout<<endl;
	}
	
	for(int i = n; i >= 1; i --)
		for(int j = n; j >= 1; j --)
		if(a[i][j] != 0)
			dfs(i,j,a[i][j]);
		else
		{
			dfs(i,j,1);
			dfs(i,j,2);
		}
	for(int k = 1; k <= 2;k ++)
	{
		for(int i = 1; i <= n; i ++)
		{
			for(int j = 1; j <= n; j ++)
			if(f[k][i][j] < INF)
				cout<<f[k][i][j]<<" ";
			else
				cout<<"* ";
			cout<<endl;
		}
		cout<<endl;
	}*/
	int ans = INF,s;
	for(int i = 1; i <= m; i ++)
		if(d[i].x == 1 && d[i].y == 1)
			s = i;
	suan(m,s);
	if(a[n][n] != 0)
	{
		for(int i = 1; i <= m; i ++)
			if(d[i].x == n && d[i].y == n)
				ans = dis[i];
	}
	else
	{
		for(int i = 1; i <= m; i ++)
		{
			if(d[i].x == n-1 && d[i].y == n && a[n-1][n] != 0)
				ans = min(ans,dis[i]+2);
			if(d[i].x == n && d[i].y == n-1 && a[n][n-1] != 0)
				ans = min(ans,dis[i]+2);
		}
	}
	if(ans < INF)
		cout<<ans;
	else
		cout<<-1; 
	return 0;
}
/*
5 7
1 1 0
1 2 0
2 2 1
3 3 1
3 4 0
4 4 1
5 5 0

5 7
1 1 0
1 2 0
2 2 1
3 3 1
3 4 0
4 4 1
5 5 0

5 11
1 1 0
1 2 1
1 4 0
1 5 0
2 5 0
3 3 0
3 4 0
3 5 0
4 3 1
5 3 0
5 4 0

*/

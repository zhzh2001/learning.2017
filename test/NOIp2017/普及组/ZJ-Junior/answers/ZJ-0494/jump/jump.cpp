#include<bits/stdc++.h>
using namespace std;

#define MAXN 500005
#define INF 0x3f3f3f3f
struct aa
{
	int d,f;
}a[MAXN];
int n,d,k;
int f[MAXN];

bool cmp(aa a,aa b)
{
	return a.d < b.d;
}

void rd()
{
	scanf("%d%d%d",&n,&d,&k);
	for(int i = 1; i <= n; i ++)
	{
		scanf("%d%d",&a[i].d,&a[i].f);
	}
	sort(a+1,a+n+1,cmp);
}

bool judge(int r)
{
	memset(f,0xc0,sizeof(f));
	f[0] = 0;
	for(int i = 1; i <= n; i ++)
	{
		for(int j = 0; j < i; j ++)
		{
			if(a[i].d - a[j].d <= d+r && a[i].d - a[j].d >= d-r)
			{
				f[i] = max(f[i],f[j]+a[i].f);
			}
		}
		//cout<<f[i]<<endl;
	}
	int ans = 0;
	for(int i = 1; i <= n; i ++)
		ans = max(ans,f[i]);
	return ans > k;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	
	rd();
	if(n > 5000)
	{
		cout<<-1;
		return 0;
	}
	int l = 0,r = d-1,mid;
	//cout<<judge(1)<<endl;
	
	while(l+3 <= r)
	{
		mid  = (l+r)>>1;
		if(judge(mid))
			r = mid;
		else
			l = mid;
	}
	for(int i = l; i <= r; i ++)
	{
		if(judge(i))
		{
			cout<<i;
			return 0;
		}
	}
	cout<<-1;
	return 0;
}
/*
7 4 10
2 6
5 -3
10 3
11 -3
13 1
17 6
20 2
*/

var
        n,q,i,j,x,r,l,g:longint;
        num:array[1..1000] of longint;
        e:array[1..1000,1..8] of longint;
function min(a,b:longint):longint;
begin
        if a<b then exit(a);
        exit(b);
end;
function len(k:longint):longint;
begin
        len:=0;
        while k>0 do
        begin
                inc(len);
                k:=k div 10;
        end;
end;
procedure fassign(f:string);
begin
        assign(input,f+'.in');
        reset(input);
        assign(output,f+'.out');
        rewrite(output);
end;
procedure fclose;
begin
        close(input);
        close(output);
end;

begin
        fassign('librarian');
        read(n,q);
        for i:=1 to n do
        begin
                read(num[i]);
                r:=1;
                for j:=1 to len(num[i]) do
                begin
                        r:=r*10;
                        e[i,j]:=num[i] mod r;
                end;
        end;
        for i:=1 to q do
        begin
                read(l,x);
                g:=maxlongint;
                for j:=1 to n do if e[j,l]=x then g:=min(num[j],g);
                if g=maxlongint
                        then writeln(-1)
                        else writeln(g);
        end;
        fclose;

end.
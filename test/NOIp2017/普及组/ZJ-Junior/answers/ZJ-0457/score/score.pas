var
        a,b,c,ans:longint;
procedure fassign(f:string);
begin
        assign(input,f+'.in');
        reset(input);
        assign(output,f+'.out');
        rewrite(output);
end;
procedure fclose;
begin
        close(input);
        close(output);
end;
begin
        fassign('score');
        read(a,b,c);
        ans:=a div 5+b*3 div 10+c div 2;
        write(ans);
        fclose;
end.
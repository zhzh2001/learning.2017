var
        n,d,k,i,j,ans,l,r,mdl,maxn:longint;
        p,dp:array[0..1000000] of longint;
function max(a,b:longint):longint;
begin
        if a>b then exit(a);
        exit(b);
end;
function min(a,b:longint):longint;
begin
        if a<b then exit(a);
        exit(b);
end;
procedure fassign(f:string);
begin
        assign(input,f+'.in');
        reset(input);
        assign(output,f+'.out');
        rewrite(output);
end;
procedure fclose;
begin
        close(input);
        close(output);
end;

begin
        fassign('jump');
        read(n,d,k);
        for i:=1 to n do read(l,p[l]);
        maxn:=l;
        l:=0;
        r:=1000100000-d;
        while l<r do
        begin
                mdl:=(l+r) div 2;
                ans:=-maxlongint;
                fillchar(dp,sizeof(dp),128);
                dp[0]:=0;
                for i:=0 to maxn do
                begin
                        for j:=max(d-mdl,1) to min(d+mdl,maxn-i) do
                                dp[i+j]:=max(dp[i]+p[j],dp[i+j]);
                        ans:=max(ans,dp[i]);
                end;
                if ans<k then l:=mdl+1;
                if ans>=k then r:=mdl;
        end;
        if l>=1000000000 then
        begin
                write(-1);
                exit;
        end;
        write(l);
        fclose;
end.

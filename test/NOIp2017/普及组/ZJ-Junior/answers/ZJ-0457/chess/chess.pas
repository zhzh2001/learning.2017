const
        way:array[1..4,0..1] of longint=((1,0),(-1,0),(0,1),(0,-1));
type
        bfs_rec=record
                x,y,cost,c:longint;
        end;
var
        m,n:longint;
        clr,mny:array[1..100,1..100] of longint;
        bfs:array[1..100000] of bfs_rec;
procedure make;
var
        i,j,x,y:longint;
begin
        read(m,n);
        for i:=1 to m do
                for j:=1 to m do
                        clr[i,j]:=-1;
        for i:=1 to n do read(x,y,clr[x,y]);
        for i:=1 to m do
                for j:=1 to m do
                        mny[i,j]:=maxlongint;
end;
function min(a,b:longint):longint;
begin
        if a<b then exit(a);
        exit(b);
end;
function canuse(u,v,k:longint):boolean;
begin
        if (u=0) or (v=0) then exit(false);
        if (u>m) or (v>m) then exit(false);
        if (clr[bfs[k].x,bfs[k].y]=-1) and (clr[u,v]=-1) then exit(false);
        exit(true);
end;
function costft(fc,tc:longint):longint;
begin
        if fc=tc
                then exit(0)
                else exit(1);
end;
procedure _bfs;
var
        head,tail,u,v,i:longint;
begin
        bfs[1].x:=1;
        bfs[1].y:=1;
        bfs[1].c:=clr[1,1];
        mny[1,1]:=0;
        head:=0;
        tail:=1;
        while head<tail do
        begin
                inc(head);
                for i:=1 to 4 do
                begin
                        u:=bfs[head].x+way[i,0];
                        v:=bfs[head].y+way[i,1];
                        if canuse(u,v,head) then
                                if clr[u,v]<>-1
                                        then
                                        begin
                                                inc(tail);
                                                bfs[tail].x:=u;
                                                bfs[tail].y:=v;
                                                bfs[tail].c:=clr[u,v];
                                                bfs[tail].cost:=bfs[head].cost+costft(bfs[head].c,bfs[tail].c);
                                                if bfs[tail].cost>=mny[u,v]
                                                        then
                                                        begin
                                                                dec(tail);
                                                                continue;
                                                        end
                                                        else mny[u,v]:=bfs[tail].cost;
                                        end
                                        else
                                        begin
                                                inc(tail);
                                                bfs[tail].x:=u;
                                                bfs[tail].y:=v;
                                                bfs[tail].c:=0;
                                                bfs[tail].cost:=bfs[head].cost+costft(bfs[head].c,bfs[tail].c)+2;
                                                mny[u,v]:=min(mny[u,v],bfs[tail].cost);
                                                inc(tail);
                                                bfs[tail].x:=u;
                                                bfs[tail].y:=v;
                                                bfs[tail].c:=1;
                                                bfs[tail].cost:=bfs[head].cost+costft(bfs[head].c,bfs[tail].c)+2;
                                                mny[u,v]:=min(mny[u,v],bfs[tail].cost);
                                        end;
                end;
        end;
        if mny[m,m]=2147483647 then
        begin
                write(-1);
                exit;
        end;
        write(mny[m,m]);
end;
procedure fassign(f:string);
begin
        assign(input,f+'.in');
        reset(input);
        assign(output,f+'.out');
        rewrite(output);
end;
procedure fclose;
begin
        close(input);
        close(output);
end;

begin
        fassign('chess');
        make;
        _bfs;
        fclose;
end.

var
  finish,change:boolean;
  n,m,xx,yy,x,c,i,fh:longint;
  oo:longint=maxlongint-1;
  f:array[-10001..20001] of longint;
  a:array[-10001..20001] of char;
  flag:array[-10001..20001] of boolean;

procedure readit;
var
  wz,i:longint;
begin
  readln(m,n);
  for i:=1 to m*m do f[i]:=oo;
  for i:=1 to m*m do a[i]:='w';
  for i:=1 to n do
    begin
      readln(xx,yy,c);
      wz:=(xx-1)*m+yy;
      f[wz]:=oo;
      flag[wz]:=true;
      if c=1 then a[wz]:='1';
      if c=0 then a[wz]:='0';
    end;
  n:=m;
end;

function min(a,b:longint):longint;
begin
  if a<b then
    exit(a);
  exit(b);
end;

procedure onestep(b:longint);
begin
  if (flag[b])and(a[b]<>'w') then
    if a[b]=a[x] then
      f[b]:=min(f[b],f[x]) else
      f[b]:=min(f[b],f[x]+1);
  if (b=n*m)and(a[b]='w') then
    begin
      f[b]:=min(f[b],f[x]+2);
      finish:=true;
    end;
end;

procedure twostep(b:longint);
begin
  if (flag[b])and(a[b]<>'w') then
    if a[b]=a[x] then
      f[b]:=min(f[b],f[x]+2) else
      f[b]:=min(f[b],f[x]+3);
end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readit;
  f[1]:=0;
  change:=true; finish:=false;
  while (x<m*n)and(not finish)and(change) do
    begin
      fh:=oo; change:=false;
      for i:=1 to n*m do
        if (flag[i])and(a[i]<>'w')and(f[i]<>oo)and(f[i]<fh) then
          begin
            fh:=f[i];
            x:=i;
            change:=true;
          end;

      if (x-m>0) then onestep(x-m);
      if (x+m<n*m+1) then onestep(x+m);
      if (x-1>0)and(x mod m<>1) then onestep(x-1);
      if (x+1<n*m+1)and(x mod m<>0) then onestep(x+1);

      if a[x-m]='w' then
        begin
          if (x-m-m>0) then twostep(x-m-m);
          if (x-m-1>0)and(x mod m<>1) then twostep(x-m-1);
          if (x-m+1>0)and(x mod m<>0) then twostep(x-m+1);
        end;
      if a[x+m]='w' then
        begin
          if (x+m-1<n*m+1)and(x mod m<>1) then twostep(x+m-1);
          if (x+m+1<n*m+1)and(x mod m<>0) then twostep(x+m+1);
          if (x+m+m<n*m+1) then twostep(x+m+m);
        end;
      if a[x-1]='w' then
        begin
          if (x-1-m>0)and(x mod m<>1) then twostep(x-1-m);
          if (x-1+m<n*m+1)and(x mod m<>1) then twostep(x-1+m);
          if (x-1-1>0)and(x mod m<>1)and(x mod m<>2) then twostep(x-1-1);
        end;
      if a[x+1]='w' then
        begin
          if (x+1-m>0)and(x mod m<>0) then twostep(x+1-m);
          if (x+1+m<n*m+1)and(x mod m<>0) then twostep(x+1+m);
          if (x+1+1<n*m+1)and(x mod m<>0)and(x mod m<>m-1) then twostep(x+1+1);
        end;

      flag[x]:=false;
    end;
  if (finish)or(x>=n*m) then
    write(f[n*m]) else
    write(-1);
  close(input);close(output);
end.










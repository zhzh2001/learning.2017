var
  n,q,i,j,ans,cd,bm:longint;
  haveans:boolean;
  a:array[0..1001] of longint;

procedure swap(var a,b:longint);
var
  t:longint;
begin
  t:=a; a:=b; b:=t;
end;

procedure qsort(l,r:longint);
var
  i,j,mid,ran:longint;
begin
  i:=l; j:=r;
  ran:=random(r-l+1)+l;
  mid:=a[ran];
  while i<=j do
    begin
      while a[i]<mid do
        inc(i);
      while mid<a[j] do
        dec(j);
      if i<=j then
        begin
          swap(a[i],a[j]);
          inc(i); dec(j);
        end;
    end;
  if l<j then
    qsort(l,j);
  if i<r then
    qsort(i,r);
end;

function cf(n:longint):longint;
var
  i:longint;
  exition:longint;
begin
  exition:=1;
  for i:=1 to n do
    exition:=exition*10;
  exit(exition);
end;

begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  randomize; qsort(1,n);
  for j:=1 to q do
    begin
      readln(cd,bm);
      ans:=-1; haveans:=false;
      for i:=1 to n do
        if ((a[i]-bm>0)and((a[i]-bm) mod (cf(cd))=0))or(a[i]=bm) then
          begin
            haveans:=true;
            ans:=a[i];
            break;
          end;
      if haveans then
        writeln(ans) else
        writeln(-1);
    end;
  close(input);close(output);
end.
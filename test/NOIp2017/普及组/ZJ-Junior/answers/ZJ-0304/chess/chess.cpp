#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <climits>
#ifdef __IN__NOIP__DEBUG__SYK
#include <iomanip>
#endif
using namespace std;

int color[101][101],/*,id[101][101]*/f[101][101];
bool visited[102][102];
int m,n;
int blocks;

//void setid(int x,int y,int idn,int col);
void walk(int x,int y,int curr,int mana,int col);

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(color,-1,sizeof color);
	memset(f,-1,sizeof f);
	scanf("%d%d",&m,&n);
	for(int ain=1;ain<=n;ain++)
	{
		int t1,t2;
		scanf("%d%d",&t1,&t2);
		scanf("%d",&color[t1][t2]);
	}
	walk(1,1,0,2,color[1][1]);
	if(f[m][m]==-1)
		printf("-1");
	else
		printf("%d",f[m][m]);
	fclose(stdin);
	fclose(stdout);
}
/*
void setid(int x,int y,int idn,int col)
{
	if(x<1||y<1||x>m||y>m)
		return;
	if((id[x][y]>0)||(col!=color[x][y]))
		return;
	id[x][y]=idn;
	for(int a=-1;a<=1;a++)
		for(int b=-1;b<=1;b++)
			if((a==0)^(b==0))
				setid(x+a,y+b,idn,col);
}*/

void walk(int x,int y,int curr,int mana,int col)
{
	if(x<1||y<1||x>m||y>m)
		return;
	mana=(mana==2)?mana:mana+1;
	if(f[x][y]==-1||f[x][y]>curr)
		f[x][y]=curr;
	curr=min(f[x][y],curr);
	visited[x][y]=true;
	for(int a=-1;a<=1;a++)
		for(int b=-1;b<=1;b++)
			if((a==0)^(b==0))
			{
				if(visited[x+a][y+b])
					continue;
				if((mana==2) and (color[x+a][y+b]==-1))
					walk(x+a,y+b,curr+2,0,col);
				if(color[x+a][y+b]==-1)
					continue;
				if(color[x+a][y+b]==col)
					walk(x+a,y+b,curr,mana,color[x+a][y+b]);
				else
					walk(x+a,y+b,curr+1,mana,color[x+a][y+b]);
			}
	visited[x][y]=false;
}

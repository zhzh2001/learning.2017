#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <climits>
#include <map>
using namespace std;

struct Book
{
	char code[9];
	int length;
	void getbook(){ scanf("%s",code);length=strlen(code); }
	bool operator<(const Book & book) const { return (length==book.length)?(strcmp(code,book.code)<0):length<book.length; }
};
int len,pos=-1;
char bookname[9];
int n,q;
Book books[1000];

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int a=0;a<n;a++)
		books[a].getbook();
	for(int i=0;i<q;i++)
	{
		scanf("%d%s",&len,bookname);
		pos=-1;
		for(int j=0;j<n;j++)
		{
			int la=books[j].length-1,lb=len-1;
			bool found=true;
			while((la>=0)&&(lb>=0))
				if(books[j].code[la]!=bookname[lb])
				{
					found=false;
					break;
				}
				else la--,lb--;
			if(found && lb==-1)
				if(pos==-1||books[j]<books[pos])
					pos=j;
		}
		if(pos==-1)
			printf("-1\n");
		else
			printf("%s\n",books[pos].code);
	}
	fclose(stdin);
	fclose(stdout);
}

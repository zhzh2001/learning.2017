#include <fstream>
#include <cstdio>

using namespace std;

ifstream cin("chess.in");
ofstream cout("chess.out");

int map[110][110]={0},ans=10000000,k=0,n;
int aaa[110][110]={0};
bool t[110][110]={0},bian[110][110]={0};

int ss(int a,int b,int a1,int b1)
{
	if (bian[a][b]&&map[a1][b1]==0) return -1;
	if (map[a][b]==map[a1][b1]) return 0;
	if (!bian[a][b]&&map[a1][b1]==0) {
		map[a1][b1]=map[a][b];
		bian[a1][b1]=true;
		return 2;
	}
	return 1;
}

int dfs(int x,int y,int sum)
{
	k++;
	if (k>20000000) return 0;
	int fff;
	if (sum>=ans) return 0;
	//cout<<x<<" "<<y<<" "<<sum<<endl;
	if (x==n&&y==n)
	{
	//	cout<<sum<<endl;
		ans=sum;
		return 0;
	}
	fff=ss(x,y,x+1,y);
	if (!t[x+1][y] && x+1<=n && fff!=-1 )
	{
		t[x+1][y]=true;
		dfs(x+1,y,sum+fff);
		t[x+1][y]=false;
	}
	fff=ss(x,y,x-1,y);
	if (!t[x-1][y]&&x-1>=1&&fff!=-1)
	{
		t[x-1][y]=true;
		dfs(x-1,y,sum+fff);
		t[x-1][y]=false;
	}
	fff=ss(x,y,x,y+1);
	
	if (!t[x][y+1]&&y+1<=n&&fff!=-1)
	{
		t[x][y+1]=true;
		dfs(x,y+1,sum+fff);
		t[x][y+1]=false;
	}
	fff=ss(x,y,x,y-1);
	
	if (!t[x][y-1]&&y-1>=1&&fff!=-1)
	{
		t[x][y-1]=true;
		dfs(x,y-1,sum+fff);
		t[x][y-1]=false;
	}
	if (bian[x][y]) {
		bian[x][y]=false;
		map[x][y]=0;
	}
	return 0;
}

int main()
{
	int m,i,j,x,y,z;
	cin>>n>>m;
	for (i=1;i<=m;i++)
	{
		cin>>x>>y>>z;
		map[x][y]=2-z;
	}/*
	for (i=1;i<=n;i++)
	{
		for (j=1;j<=n;j++) cout<<map[i][j];
		cout<<endl;
	}*/
	t[1][1]=true;
	dfs(1,1,0);
	if (ans<1001000) cout<<ans<<endl;
	            else cout<<-1<<endl;
	return 0;
}

#include <fstream>
#include <cmath>
#include <cstdio>

using namespace std;

ifstream cin("jump.in");
ofstream cout("jump.out");

int a[500100],b[500100];

int main()
{
	int n,target,i,sum=0,d,ans=0,cnt=0;
	cin>>n>>d>>target;
	for (i=1;i<=n;i++)
	{
		cin>>a[i]>>b[i];
		if (b[i]>0) sum+=b[i];
	}
	if (sum<target){cout<<-1<<endl;return 0;}
	if (sum==target)
	{
		sum=0;
		for (i=1;i<=n;i++)
		{
			if (b[i]<=0) continue;
			if (a[i]-sum>ans) ans=abs(a[i]-sum-d);
			sum=a[i];
		}
		cout<<ans<<endl;
		return 0;
	}
	int h,r;
	h=0;r=1;sum=0;
	a[0]=0;
	while (h<=r&&sum<=target)
	{
		cnt++;
		if (cnt>3000000) break;
		if ((d-ans)>(a[r]-a[h])||(a[r]-a[h])>(d+ans)) 
		{
			ans=abs(a[r]-a[h]-d);
			sum+=b[r];
			r++;
		}
		while (d-ans<=a[r]-a[h]&&a[r]-a[h]<=d+ans&&r<=n) 
		{
			if (b[r]>0) sum+=b[r];
			r++;
		}
		h=r-1;
	}
	cout<<ans<<endl;
	return 0;
}

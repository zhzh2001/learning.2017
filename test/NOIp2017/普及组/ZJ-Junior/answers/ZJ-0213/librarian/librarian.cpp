#include <fstream>
#include <algorithm>
#include <cstdio>

using namespace std;

ifstream cin("librarian.in");
ofstream cout("librarian.out");

int need[1010];

int main()
{
	int n,m,i,j,x,y;
	int k;
	bool t;
	cin>>n>>m;
	for (i=1;i<=n;i++) cin>>need[i];
	sort(need+1,need+n+1);
	for (i=1;i<=m;i++)
	{
		cin>>x>>y;
		k=1;t=true;
		for (j=1;j<=x;j++) k*=10;
		for (j=1;j<=n&&t;j++)
			if (need[j]%k==y) t=false;
		if (t) cout<<-1<<endl;
		  else cout<<need[j-1]<<endl;
	}
	return 0;
}

const
  dx:array[1..4] of longint=(0,1,0,-1);
  dy:array[1..4] of longint=(1,0,-1,0);
var
  px,py,k,n,m,i,j,min,js:longint;
  f:array[-1..1001,-1..1001] of integer;
procedure search(x,y,t:longint);
var i,xx,yy,tt:longint;
begin
  if k>0 then k:=k+1;
  if k=3 then k:=0;
  tt:=f[x,y];
  if (x=n)and(y=n) then
  begin
    if js<min then min:=js;
  end else
  for i:=1 to 4 do
  if f[x+dx[i],y+dy[i]]<>0 then
   begin
     xx:=x+dx[i];yy:=y+dy[i];
     if (f[xx,yy]=1)and(k=0) then
     begin
       f[xx,yy]:=tt;
       js:=js+2;
       k:=1;
       f[x,y]:=0;
       search(xx,yy,t+1);
       k:=0;
       js:=js-2;
       f[xx,yy]:=1;
       f[x,y]:=tt;
     end else
     if f[xx,yy]<>1 then
     begin
       if f[xx,yy]=f[x,y] then
       begin
         f[x,y]:=0;
         search(xx,yy,t+1);
         f[x,y]:=tt;
       end else
       begin
         f[x,y]:=0;
         js:=js+1;
         search(xx,yy,t+1);
         js:=js-1;
         f[x,y]:=tt;
       end;
     end;
   end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  min:=maxlongint;
  js:=0;
  readln(n,m);
  for i:=1 to m do
    for j:=1 to m do
      f[i,j]:=1;
  for i:=1 to m do
    begin
      readln(px,py,k);
      k:=k+2;
      f[px,py]:=k;
    end;
   k:=0;
  search(1,1,1);
  if min=maxlongint then writeln(-1) else
  writeln(min);
  close(input);close(output);
end.

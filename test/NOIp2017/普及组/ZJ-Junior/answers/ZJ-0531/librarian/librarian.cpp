#include<cstdio>
#include<algorithm>
#include<string>
#include<iostream>
using namespace std;

string a[1010];
int n;

string pp(int x,int ch)
{
	int i,j;
	for(i=1;i<=n;i++)
	{
		int l=a[i].size();
		if(l>=ch)
		{
			int d=0;
			for(j=l-ch;j<l;j++)
			{
				d=d*10+a[i][j]-'0';
			}
			if(d==x)
				return a[i];
		}
	}
	return "-1";
}

bool cmp(string a,string b)
{
	int la=a.size(),lb=b.size();
	if(la>lb) return 0;
	if(la<lb) return 1;
	if(la==lb)
	{
		for(int i=0;i<la;i++)
		{
			if(a[i]>b[i])
				return 0; 
		}
		return 1;
	}
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int q,i,j;
	cin>>n>>q;
	for(i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1,cmp);
	for(i=1;i<=q;i++)
	{
		int c=0,d;
		char e;
		cin>>d;
		for(j=1;j<=d;j++)
		{
			cin>>e;
			c=c*10+e-'0';
		}
		cout<<pp(c,d)<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

Var
    a,b,c:Real;



Procedure Asopen;
Begin
    Assign(Input,'score.in');
    Reset(Input);
    Assign(Output,'score.out');
    Rewrite(Output);
End;



Procedure Asclose;
Begin
    Close(Input);
    Close(Output);
End;



Procedure Init;
Begin
    Readln(a,b,c);
End;



Procedure Print;
Begin
    Writeln((a * 0.2 + b * 0.3 + c * 0.5):0:0);
End;



Begin
    Asopen;
    Init;
    Print;
    Asclose;
End.
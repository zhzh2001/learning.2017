Const
    Dx:Array[1..4] Of -1 .. 1 = (-1,0,1,0);
    Dy:Array[1..4] Of -1 .. 1 = (0,1,0,-1);



Var
    f:Array[0..101,0..101,0..2] Of Longint;
    a:Array[0..101,0..101] Of Longint;
    p,q,c:Array[0..1000001] Of Longint;
    x,y,color,n,m,i,head,tail,x1,y1,color1:Longint;



Procedure Asopen;
Begin
    Assign(Input,'chess.in');
    Reset(Input);
    Assign(Output,'chess.out');
    Rewrite(Output);
End;



Procedure Asclose;
Begin
    Close(Input);
    Close(Output);
End;



Function Min(a,b:Longint):Longint;
Begin
    If (a < b) Then
        Exit(a);
    Exit(b);
End;



Function Check(x,y:Longint):Boolean;
Begin
    If ((x > 0) And (y > 0) And (x <= n) And (y <= n)) Then
        Exit(True);
    Exit(False);
End;



Procedure Add(x,y,color1,w:Longint);
Begin
    Inc(tail);
    p[tail] := x;
    q[tail] := y;
    c[tail] := color1;
    f[x][y][color1] := w;
End;



Procedure Init;
Begin
    Readln(n,m);
    For i := 1 To m Do
    Begin
        Readln(x,y,color);
        a[x][y] := color + 1;
    End;
End;



Procedure Main;
Begin
    Fillchar(f,Sizeof(f),$7f);
    f[1][1][a[1][1]] := 0;
    p[1] := 1;
    q[1] := 1;
    c[1] := a[1][1];
    head := 1;
    tail := 1;
    While (head <= tail) Do
    Begin
        x := p[head];
        y := q[head];
        color := c[head];
        If (a[x][y] = 0) Then
        Begin
            For i := 1 To 4 Do
            Begin
                x1 := x + Dx[i];
                y1 := y + Dy[i];
                color1 := a[x1][y1];
                If (Check(x1,y1) And (color1 <> 0)) Then
                    If (color <> color1) Then
                    Begin
                        If (f[x][y][color] + 1 < f[x1][y1][color1]) Then
                            Add(x1,y1,color1,f[x][y][color] + 1);
                    End
                    Else If (f[x][y][color] < f[x1][y1][color1]) Then
                        Add(x1,y1,color1,f[x][y][color]);
            End;
        End
        Else
        Begin
            For i := 1 To 4 Do
            Begin
                x1 := x + Dx[i];
                y1 := y + Dy[i];
                color1 := a[x1][y1];
                If (Check(x1,y1)) Then
                    If (color1 = 0) Then
                    Begin
                        If (color = 1) Then
                        Begin
                            If (f[x][y][1] + 2 < f[x1][y1][1]) Then
                                Add(x1,y1,1,f[x][y][1] + 2);
                            If (f[x][y][1] + 3 < f[x1][y1][2]) Then
                                Add(x1,y1,2,f[x][y][1] + 3);
                        End
                        Else
                        Begin
                            If (f[x][y][2] + 2 < f[x1][y1][2]) Then
                                Add(x1,y1,2,f[x][y][2] + 2);
                            If (f[x][y][2] + 3 < f[x1][y1][1]) Then
                                Add(x1,y1,1,f[x][y][2] + 3);
                        End;
                    End
                    Else If (color = color1) Then
                    Begin
                        If (f[x][y][color] < f[x1][y1][color1]) Then
                            Add(x1,y1,color1,f[x][y][color]);
                    End
                    Else If (f[x][y][color] + 1 < f[x1][y1][color1]) Then
                        Add(x1,y1,color1,f[x][y][color] + 1);
            End;
        End;
        Inc(head);
    End;
End;



Procedure Print;
Begin
    If (Min(f[n][n][1],f[n][n][2]) > 100000000) Then
        Writeln(-1)
    Else
        Writeln(Min(f[n][n][1],f[n][n][2]));
End;



Begin
    Asopen;
    Init;
    Main;
    Print;
    Asclose;
End.

Var
    f:Array[0..8] Of Longint;
    a:Array[0..1001] Of Longint;
    n,q,p,k,num,min,i:Longint;



Procedure Asopen;
Begin
    Assign(Input,'librarian.in');
    Reset(Input);
    Assign(Output,'librarian.out');
    Rewrite(Output);
End;



Procedure Asclose;
Begin
    Close(Input);
    Close(Output);
End;



Procedure Init;
Begin
    Readln(n,q);
    f[0] := 1;
    For i := 1 To 8 Do
        f[i] := f[i - 1] * 10;
    For i := 1 To n Do
        Readln(a[i]);
End;



Procedure Main;
Begin
    For q := 1 To q Do
    Begin
        Readln(p,k);
        num := 0;
        min := Maxlongint;
        For i := 1 To n Do
            If (a[i] Mod f[p] = k) And (a[i] < min) Then
            Begin
                num := i;
                min := a[i];
            End;
        If (num = 0) Then
            Writeln(-1)
        Else
            Writeln(min);
    End;
End;



Begin
    Asopen;
    Init;
    Main;
    Asclose;
End.

Var
    dp,s:Array[0..500001] Of Int64;
    f,x:Array[0..500001] Of Longint;
    head,tail,l,r,lbound,ubound,mid,n,i,d,p,score:Longint;
    ans:Int64;



Procedure Asopen;
Begin
    Assign(Input,'jump.in');
    Reset(Input);
    Assign(Output,'jump.out');
    Rewrite(Output);
End;



Procedure Asclose;
Begin
    Close(Input);
    Close(Output);
End;



Function Max(a,b:Int64):Int64;
Begin
    If (a > b) Then
        Exit(a);
    Exit(b);
End;



Function Check(k:Longint):Boolean;
Var
    i:Longint;
Begin
    head := 0;
    tail := -1;
    f[0] := 0;
    p := 0;
    l := Max(d - k,1);
    r := d + k;
    ans := 0;
    For i := 1 To n Do
    Begin
        While ((x[i] - x[f[head]] > r) And (head <= tail)) Do
            Inc(head);
        While (x[i] - x[p] > r) Do
            Inc(p);
        If (i = p) Then
            Exit(ans >= score);
        While (x[i] - x[p] >= l) Do
        Begin
            While ((dp[p] > dp[f[tail]]) And (head <= tail)) Do
                Dec(tail);
            Inc(tail);
            f[tail] := p;
            Inc(p);
        End;
        If (head > tail) Then
            Exit(ans >= score);
        dp[i] := dp[f[head]] + s[i];
        ans := Max(ans,dp[i]);
    End;
    Exit(ans >= score);
End;



Procedure Init;
Begin
    Readln(n,d,score);
    For i := 1 To n Do
        Readln(x[i],s[i]);
End;



Procedure Main;
Begin
    lbound := 0;
    ubound := 1000000001;
    While (lbound <= ubound) Do
    Begin
        mid := (lbound + ubound) >> 1;
        If (Check(mid)) Then
            ubound := mid - 1
        Else
            lbound := mid + 1;
    End;
End;



Procedure Print;
Begin
    If (ubound = 1000000001) Then
        Writeln(-1)
    Else
        Writeln(ubound + 1);
End;



Begin
    Asopen;
    Init;
    Main;
    Print;
    Asclose;
End.

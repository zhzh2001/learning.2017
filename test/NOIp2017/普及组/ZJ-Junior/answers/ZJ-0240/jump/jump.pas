uses math;
const
  inf=maxlongint >> 2;
var
  n,d,k,i,l,r,mid,ans:longint;
  x,s:array[0..500001]of longint;
  f:array[0..500001]of int64;
  sum:int64;
function check(g:longint):boolean;
var
  a,b,l,r,i,j:longint;
  sum:int64;
begin
  a:=d-g; b:=d+g;
  if a<1 then a:=1;
  f[0]:=0;
  sum:=-inf;
  for i:=1 to n do 
    begin
      f[i]:=-inf;
      for j:=0 to i-1 do 
        if (x[i]-x[j]>=a) and (x[i]-x[j]<=b) then f[i]:=max(f[i],f[j]+s[i]);
      sum:=max(sum,f[i]);
    end;
  exit(sum>=k);
end;
begin
  assign(input, 'jump.in');
  reset(input);
  assign(output, 'jump.out');
  rewrite(output);
  readln(n,d,k);
  x[0]:=0;
  for i:=1 to n do read(x[i],s[i]);
  for i:=1 to n do 
    if s[i]>0 then sum:=sum+s[i];
  if sum<k then begin write(-1); close(input); close(output); halt; end;
  l:=0; r:=500000; ans:=-1;
  while l<=r do 
    begin
      mid:=(l+r)>>1;
      if check(mid) then 
        begin ans:=mid; r:=mid-1; end
      else l:=mid+1;
    end;
  write(ans);
  close(input);
  close(output);
end.

var
  n,q,i,j,m,x,s:longint;
  a:array[0..1001]of longint;
  p:array[0..8]of longint;
begin
  assign(input, 'librarian.in');
  reset(input);
  assign(output, 'librarian.out');
  rewrite(output);
  p[0]:=1;
  for i:=1 to 8 do p[i]:=p[i-1]*10;
  read(n,q);
  for i:=1 to n do read(a[i]);
  for i:=1 to q do 
    begin
      read(m,x);
      s:=maxlongint;
      for j:=1 to n do 
        if (a[j]>=x) and ((a[j]-x) mod p[m]=0) and (a[j]<s) then 
          s:=a[j];
      if s=maxlongint then writeln(-1) else writeln(s);
    end;
  close(input);
  close(output);
end.

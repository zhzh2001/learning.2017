uses math;
const
  dx:array[1..4]of longint = (-1,0,1,0);
  dy:array[1..4]of longint = (0,-1,0,1);
var
  n,m,i,j,x,y,z:longint;
  a:array[0..101,0..101]of longint;
  b:array[0..101,0..101]of boolean;
function dfs(x,y,z,p:longint):longint;
var
  k,i,j,t,ans:longint;
begin
  if (x=n) and (y=n) then exit(0);
  if a[x,y]=-1 then t:=z else t:=a[x,y];
  ans:=maxlongint >> 2;
  for k:=1 to 4 do 
    begin
      i:=x+dx[k]; j:=y+dy[k];
      if (i<1) or (i>n) or (j<1) or (j>n) then continue;
      if not b[i,j] then continue;
      b[i,j]:=false;
      if (a[i,j]=-1) and (p=0) then 
        ans:=min(ans,2+dfs(i,j,t,1))
      else
        if a[i,j]>=0 then 
          ans:=min(ans,t xor a[i,j]+dfs(i,j,a[i,j],0));
      b[i,j]:=true;
    end;
  exit(ans);
end;
begin
  assign(input, 'chess.in');
  reset(input);
  assign(output, 'chess.out');
  rewrite(output);
  read(n,m);
  for i:=1 to n do 
    for j:=1 to n do 
      begin
        a[i,j]:=-1; b[i,j]:=true;
      end;
  while m>0 do 
    begin
      read(x,y,z);
      a[x,y]:=z;
      dec(m);
    end;
  b[1,1]:=false;
  z:=dfs(1,1,a[1,1],0);
  if z=maxlongint >> 2 then write(-1) else write(z);
  close(input);
  close(output);
end.

uses math;
const
  dx:array[1..4]of longint = (-1,0,1,0);
  dy:array[1..4]of longint = (0,-1,0,1);
var
  n,m,i,j,x,y,z:longint;
  a:array[0..101,0..101]of longint;
  b:array[0..101,0..101,0..1,0..1]of boolean;
  q:array[0..1000001,1..4]of longint;
  f:array[0..101,0..101,0..1,0..1]of longint;
function bfs(x,y,z,p:longint):longint;
var
  h,t,i,u,v,w,r,tmp:longint;
begin
  filldword(f,sizeof(f) >> 2,maxlongint >> 2);
  fillchar(b,sizeof(b),0);
  h:=0; t:=1;
  q[t,1]:=x; q[t,2]:=y; q[t,3]:=z; q[t,4]:=p;
  b[x,y,z,p]:=true;
  f[x,y,z,p]:=0;
  while h<t do 
    begin
      inc(h);
      u:=q[h,1]; v:=q[h,2]; w:=q[h,3]; r:=q[h,4];
      b[u,v,w,r]:=false;
      for i:=1 to 4 do 
        begin
          x:=u+dx[i]; y:=v+dy[i];
          if (x<1) or (x>n) or (y<1) or (y>n) then continue;
          if a[x,y]=-1 then
            begin
              z:=w; p:=r+1;
              if p=2 then continue;
              tmp:=f[u,v,w,r]+2;
            end
          else
            begin
              z:=a[x,y]; p:=0;
              if w=z then tmp:=f[u,v,w,r] else tmp:=f[u,v,w,r]+1;
            end;
          if tmp<f[x,y,z,p] then 
            begin
              f[x,y,z,p]:=tmp;
              if b[x,y,z,p] then continue;
              inc(t);
              q[t,1]:=x; q[t,2]:=y; q[t,3]:=z; q[t,4]:=p;
              b[x,y,z,p]:=true;
            end;
        end;
    end;
  exit(min(min(f[n,n,0,0],f[n,n,0,1]),min(f[n,n,1,0],f[n,n,1,1])));
end;
begin
  assign(input, 'chess.in');
  reset(input);
  assign(output, 'chess.out');
  rewrite(output);
  read(n,m);
  for i:=1 to n do 
    for j:=1 to n do 
        a[i,j]:=-1;
  while m>0 do 
    begin
      read(x,y,z);
      a[x,y]:=z;
      dec(m);
    end;
  z:=bfs(1,1,a[1,1],0);
  if z=maxlongint >> 2 then write(-1) else write(z);
  close(input);
  close(output);
end.

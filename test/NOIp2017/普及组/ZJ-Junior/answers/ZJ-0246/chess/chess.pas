var
  m,n,i,j,k,x,y:longint;
  a:array[1..100,1..100]of longint;
  f:array[1..4,1..2]of integer;
  c:array[1..100,1..100]of boolean;
  d:array[1..100,1..100]of longint;
  b:boolean;
function min(o,p:longint):longint;
begin
  if o<p then exit(o);
  exit(p);
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  f[1,1]:=-1; f[1,2]:=0; f[2,1]:=1; f[2,2]:=0; f[3,1]:=0; f[3,2]:=1; f[4,1]:=0; f[4,2]:=-1;
  read(m,n);
  for i:=1 to m do
  for j:=1 to m do
  begin
    a[i,j]:=-1;
    d[i,j]:=100000;
  end;
  d[1,1]:=0;
  for i:=1 to n do
  begin
    read(x,y);
    read(a[x,y]);
  end;
  for i:=1 to m do
  for j:=1 to m do
  c[i,j]:=true;
  for i:=1 to m do
  for j:=1 to m do
  begin
    if a[i,j]=-1 then b:=false
    else b:=true;
    for k:=1 to 4 do
    begin
    if (i+f[k,1]>0) and (i+f[k,1]<=m) and (j+f[k,2]>0) and (j+f[k,2]<=m) then
    begin
      if b then
      begin
        if a[i+f[k,1],j+f[k,2]]<>a[i,j] then d[i,j]:=min(d[i,j],d[i+f[k,1],j+f[k,2]]+1)
        else d[i,j]:=min(d[i,j],d[i+f[k,1],j+f[k,2]]);
      end
      else
      begin
          if (d[i+f[k,1],j+f[k,2]]<d[i,j]) and (c[i+f[k,1],j+f[k,2]]=true) then
          begin
            d[i,j]:=d[i+f[k,1],j+f[k,2]]+2;
            a[i,j]:=a[i+f[k,1],j+f[k,2]];
            c[i,j]:=false;
          end;
      end;
    end;
  end;
  end;
  if d[m,m]=100000 then write(-1)
  else write(d[m,m]);
  close(input);
  close(output);
end.
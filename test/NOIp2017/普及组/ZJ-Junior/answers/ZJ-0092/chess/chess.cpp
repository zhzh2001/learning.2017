#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#define maxn 105
#define maxq 10005
using namespace std;
const int flg[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int n,m,INF,ans,dst[maxn][maxn],a[maxn][maxn],now[maxn][maxn];bool vis[maxn][maxn];
struct ad{
	int x,y;
}que[maxq];
inline int read(){
	int ret=0,fh=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')fh=-fh;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*fh;
}
void SPFA(){
	memset(vis,0,sizeof(vis));
	memset(dst,63,sizeof(dst));
	INF=dst[0][0];
	int hd=0,tl=1;
	que[1]=(ad){1,1},vis[1][1]=1,dst[1][1]=0;
	while(hd!=tl){
		int x=que[hd=(hd+1)%maxn].x,y=que[hd].y;
		vis[x][y]=0;
		for(int k=0;k<4;k++){
			int nx=x+flg[k][0],ny=y+flg[k][1],w;
			if(nx<1||nx>n||ny<1||ny>n||(a[x][y]==0&&a[nx][ny]==0)) continue;
			if(a[nx][ny]!=0){
				if(now[x][y]==now[nx][ny]) w=0;
				else w=1;
			}
			else w=2;
			if(dst[x][y]+w<dst[nx][ny]){
				dst[nx][ny]=dst[x][y]+w;
				if(a[nx][ny]==0) now[nx][ny]=now[x][y];
				if(!vis[nx][ny]){
					vis[nx][ny]=1,que[tl=(tl+1)%maxn]=(ad){nx,ny};
					int lst=(hd+1)%maxn;
					if(dst[que[tl].x][que[tl].y]<dst[que[lst].x][que[lst].y]) swap(que[tl],que[lst]);
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),c=read();
		now[x][y]=a[x][y]=c+1;
	}
	SPFA();
	if(dst[n][n]==INF) printf("-1");
	else printf("%d",dst[n][n]);
	return 0;
}

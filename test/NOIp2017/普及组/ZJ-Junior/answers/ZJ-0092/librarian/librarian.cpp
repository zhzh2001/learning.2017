#include<cstdio>
#include<algorithm>
#define maxn 1005
using namespace std;
const int flg[9]={1,10,100,1000,10000,100000,1000000,10000000,100000000};
int n,q,a[maxn];
inline int read(){
	int ret=0,fh=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')fh=-fh;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*fh;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),q=read();
	for(int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+n+1);
	for(int t=1;t<=q;t++){
		int len=read(),x=read(),tt=flg[len];bool e=0;
		for(int i=1;i<=n;i++){
			int now=a[i]%tt;
			if(now==x){
				printf("%d\n",a[i]);
				e=1;
				break;
			}
		}
		if(!e) printf("-1\n");
	}
	return 0;
}

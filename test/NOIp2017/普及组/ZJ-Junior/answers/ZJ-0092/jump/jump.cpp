#include<cstdio>
#include<cstring>
#include<algorithm>
#define LL long long
#define maxn 500005
using namespace std;
LL n,d,k,S,ans,f[maxn];
struct ad{
	LL x,s;
	bool operator<(const ad b)const{
		return x<b.x;
	}
}a[maxn];
inline LL read(){
	LL ret=0,fh=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')fh=-fh;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*fh;
}
bool check(LL g){
	memset(f,225,sizeof(f));
	f[0]=0;
	for(LL i=1;i<=n;i++)
	for(LL j=i-1;j>=0;j--){
		LL l=a[i].x-a[j].x;
		if(l<d-g||l>d+g) continue;
		if(f[i]<f[j]+a[i].s) f[i]=f[j]+a[i].s;
	}
	for(LL i=1;i<=n;i++)
		if(f[i]>=k) return 1;
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for(LL i=1;i<=n;i++){
		a[i].x=read(),a[i].s=read();
		if(a[i].s>0) S+=a[i].s;
	}
	if(S<k){printf("-1");return 0;}
	sort(a+1,a+n+1);
	a[0]=(ad){0,0};
	LL L=0,R=1000000000;
	while(L<=R){
		LL mid=((R-L)>>1)+L;
		if(check(mid)) R=mid-1,ans=mid;
		else L=mid+1;
	}
	printf("%lld",ans);
	return 0;
}

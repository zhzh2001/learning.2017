#include <bits/stdc++.h>
using namespace std;
int N,Q,x;
string bk[1005],s;
int check(string s){
	int d=2e9;
		for(int i=1;i<=N;i++){
			if(bk[i].size()<s.size())continue;
			int o=1;
			for(int j=0;j<s.size();j++){
				if(s[s.size()-j-1]!=bk[i][bk[i].size()-j-1]){o=0;break;}
			}
			if(o){int k=0;for(int j=0;j<bk[i].size();j++)k=k*10+bk[i][j]-'0';d=min(d,k);}
		}
	return d==2e9?-1:d;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>N>>Q;
		for(int i=1;i<=N;i++)cin>>bk[i];
		for(int i=1;i<=Q;i++){
			cin>>x>>s;
			printf("%d\n",check(s));
		}
	fclose(stdin),fclose(stdout);
	return 0;
}

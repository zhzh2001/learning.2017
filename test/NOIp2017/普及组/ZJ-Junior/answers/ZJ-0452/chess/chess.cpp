#include <bits/stdc++.h>
using namespace std;
int read(){
	char c;while(c=getchar(),c<'0'||c>'9');int x=c-'0';
	while(c=getchar(),c>='0'&&c<='9')x=(x<<1)+(x<<3)+c-'0';
	return x;
}
int N,M,a[105][105],ans=2e9;
int vis[105][105],l[100000][5],h,t;
int dx[4]={1,0,-1,0},dy[4]={0,1,0,-1};
void PRE(){for(int i=1;i<=N;i++)for(int j=1;j<=N;j++)vis[i][j]=2e9,a[i][j]=-1;}
void BFS(){
	h=t=0;
	l[++t][0]=l[t][1]=1,l[t][2]=0,l[t][4]=a[1][1];
		while(h<t){
			int x=l[++h][0],y=l[h][1],c=l[h][3],clo=l[h][4];
			if(x==N&&y==N)ans=min(ans,l[h][3]);
				for(int i=0;i<4;i++){
					int fx=x+dx[i],fy=y+dy[i];
					if(fx>N||fx<1||fy>N||fy<1)continue;
					if(a[fx][fy]==-1){
						if(!l[h][2]&&vis[fx][fy]>c+2)l[++t][0]=fx,l[t][1]=fy,l[t][2]=1,l[t][3]=c+2,l[t][4]=clo,vis[fx][fy]=c+2;
					}
					else{
						if(a[fx][fy]==clo&&vis[fx][fy]>c)l[++t][0]=fx,l[t][1]=fy,l[t][2]=0,l[t][3]=c,l[t][4]=clo,vis[fx][fy]=c;
						if(a[fx][fy]!=clo&&vis[fx][fy]>c+1)l[++t][0]=fx,l[t][1]=fy,l[t][2]=0,l[t][3]=c+1,l[t][4]=a[fx][fy],vis[fx][fy]=c+1;
					}
				}
		}
	if(ans==2e9)ans=-1;
	return ;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	N=read(),M=read();
	PRE();
		for(int i=1;i<=M;i++){
			int x=read(),y=read(),c=read();
			a[x][y]=c;
		}
	BFS();
	printf("%d",ans);
	fclose(stdin),fclose(stdout);
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
int read(){
	char c;while(c=getchar(),(c<'0'||c>'9')&&c!='-');int x=0,y=1;c=='-'?y=-1:x=c-'0';
	while(c=getchar(),c>='0'&&c<='9')x=(x<<1)+(x<<3)+c-'0';return x*y;
}
int N,D,K,X,Y,G,ans=-1;
int x[500005],s[500005],f[500005];
int check(int wks){
	X=max(1,D-wks),Y=D+wks;for(int i=1;i<=N;i++)f[i]=-1e8;
	f[0]=0;
		for(int i=0;i<=N;i++)
			for(int j=i+1;j<=N;j++){
				if(x[j]>=x[i]+X&&x[i]+Y>=x[j])f[j]=max(f[j],f[i]+s[i]);
			}
		for(int i=1;i<=N;i++)if(f[i]+s[i]>=K)return 1;
	return 0;
}
void so(int now,int sco){
	if((double)clock()>1950){
		printf("-1");
		fclose(stdin),fclose(stdout);
		exit(0);
	}
	if(sco>=K){
		printf("%d",G);
		fclose(stdin),fclose(stdout);
		exit(0);
	}
		for(int i=now+1;i<=N;i++){
			if(x[now]+Y<x[i])break;
			if(x[now]+X<=x[i]&&x[now]+Y>=x[i]){
				so(i,sco+s[i]);
			}
		}
}
void work1(){
	int L=0,R=x[N];
		while(L<=R){
			int mid=L+R>>1;
			if(check(mid))R=mid-1,ans=mid;
			else L=mid+1;
		}
	printf("%d",ans);
}
void work2(){
	for(G=0;G<=x[N];G++){
		X=max(1,D-G),Y=D+G;
		so(0,0);
	}
	puts("-1");
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	N=read(),D=read(),K=read();
		for(int i=1;i<=N;i++)x[i]=read(),s[i]=read();
	if(N<=5000)work1();
	else work2();
	fclose(stdin),fclose(stdout);
	return 0;
}

var
 a,b,sum:array[0..3000]of longint;
 n,q,i,j,s:longint;
 pan:boolean;
procedure swap(var a,b:longint);
var
 c:longint;
begin
 c:=a; a:=b;  b:=c;
end;
procedure sort(l,r:longint);
var
 tl,tr,mid:longint;
begin
 tl:=l;  tr:=r;  mid:=a[(l+r)shr 1];
 repeat
  while a[tl]<mid do inc(tl);
  while a[tr]>mid do dec(tr);
  if tl<=tr then
  begin
   swap(a[tl],a[tr]);
   inc(tl); dec(tr);
  end;
 until tl>tr;
 if tl<r then sort(tl,r);
 if tr>l then sort(l,tr);
end;
begin
 assign(input,'librarian.in');  assign(output,'librarian.out');
 reset(input);  rewrite(output);
 readln(n,q);
 for i:=1 to n do read(a[i]);
 for i:=1 to q do
 begin
  read(s,b[i]);
  sum[i]:=1;
  for j:=1 to s do sum[i]:=sum[i]*10;
 end;
 sort(1,n);
 for i:=1 to q do
 begin
  pan:=true;
  for j:=1 to n do
   if a[j] mod sum[i]=b[i] then
   begin
    writeln(a[j]);
    pan:=false;
    break;
   end;
  if pan then writeln(-1);
 end;
 close(input);  close(output);
end.
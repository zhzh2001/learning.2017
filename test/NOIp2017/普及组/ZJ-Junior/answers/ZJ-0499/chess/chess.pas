const
 dx:array[1..4]of longint=(0,1,0,-1);
 dy:array[1..4]of longint=(1,0,-1,0);
var
 min,i,m,n,summ,j,x,y,c,sumddd:longint;
 f:array[0..200,0..200]of boolean;
 color:array[0..200,0..200]of longint;
 mm:array[0..100000]of longint;
procedure dfs(x,y,money,c:longint;q:boolean);
var
 fx,fy,i:longint;
 pan:boolean;
begin
 if (x=m)and(y=m) then begin inc(summ); mm[summ]:=money; end else
 for i:=1 to 4 do
 begin
  fx:=x+dx[i];  fy:=y+dy[i];
  if (fx>0)and(fx<=m)and(fy>0)and(fy<=m)and(f[fx,fy]) then
  begin
   //writeln(fx,'<<<>>>',fy,q);
   //inc(sumddd);
   f[fx,fy]:=false;
   if color[fx,fy]=c then begin {writeln('&&',fx,' ',fy);}dfs(fx,fy,money,color[fx,fy],true); end
    else if color[fx,fy]<>-1 then begin {writeln('%%',fx,' ',fy);}dfs(fx,fy,money+1,color[fx,fy],true) end
     else
      //begin
       //pan:=false;
       //for i:=1 to n do
        //if color[fx+dx[i],fy+dy[i]]<>-1 then pan:=true;
       //if pan then
      //end;
       if q then begin {writeln('^^',fx,' ',fy);}dfs(fx,fy,money+2,c,false); end;
   f[fx,fy]:=true;
  end;
 end;
end;
begin
 assign(input,'chess.in');  assign(output,'chess.out');
 reset(input);  rewrite(output);
 readln(m,n);
 fillchar(f,sizeof(f),true);
 for i:=1 to m do
  for j:=1 to m do color[i,j]:=-1;
 for i:=1 to n do
 begin
  read(x,y,c);
  color[x,y]:=c;
 end;
 //writeln(m);
 //for i:=1 to m do
 //begin
  //for j:=1 to m do write(color[i,j]:2);
  //writeln;
 //end;
 summ:=0;
 f[1,1]:=false;
 dfs(1,1,0,color[1,1],true);
 if summ=0 then begin writeln(-1);close(input); close(output); halt; end;
 min:=maxlongint;
 for i:=1 to summ do
  if mm[i]<min then min:=mm[i];
 //for i:=1 to summ do writeln(mm[i]);
 writeln(min);
 close(input);  close(output);
end.
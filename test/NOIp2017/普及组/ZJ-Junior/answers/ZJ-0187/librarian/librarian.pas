const dx:array[1..8] of longint=(10,100,1000,10000,100000,1000000,10000000,100000000);
var a:array[1..1001] of longint;
n,m,k,j,i,x,y,ans:longint;
begin
assign(input,'librarian.in');
assign(output,'librarian.out');
reset(input);
rewrite(output);
readln(n,m);
for i:=1 to n do readln(a[i]);
for i:=1 to m do
begin
readln(k,x);
y:=dx[k];
ans:=maxlongint;
for j:=1 to n do
if(a[j] mod y=x)and(a[j]<ans) then ans:=a[j];
if ans=maxlongint then writeln(-1) else writeln(ans);
end;
close(input);
close(output);
end.

const dx:array[1..4] of longint=(0,0,1,-1);
      dy:array[1..4] of longint=(1,-1,0,0);
var a:array[1..101,1..101] of longint;
b:array[0..101,0..101] of boolean;
n,m,k,j,i,l,r,mid,x,y,z,s:longint;
bo,bo1:boolean;

procedure dfs(o,p,q,r,xx,yy,cc:longint);
var i1,x1,y1:longint;
begin
if(o=m)and(p=m) then begin if q<=mid then bo:=true;exit;end;
if(q>mid) then exit;
s:=s+1;
for i1:=1 to 4 do
begin
x1:=o+dx[i1];
y1:=p+dy[i1];
if(x1>0)and(x1<=m)and(y1>0)and(y1<=m) then
if(x1<>xx)and(y1<>yy)and not(b[x1,y1]) then
begin
if(a[x1,y1]=-1)and(r=1) then
begin
b[x1,y1]:=true;
if a[o,p]=0 then
begin
dfs(x1,y1,q+2,0,o,p,0);
dfs(x1,y1,q+3,0,o,p,1);
end else
begin
dfs(x1,y1,q+2,0,o,p,1);
dfs(x1,y1,q+3,0,o,p,0);
end;
end else
if(a[x1,y1]<>-1) then
begin
b[x1,y1]:=true;
if r=1 then
begin
if(a[x1,y1]=a[o,p]) then dfs(x1,y1,q,1,o,p,-1) else
dfs(x1,y1,q+1,1,o,p,-1);
end else
begin
if(a[x1,y1]=cc) then dfs(x1,y1,q,1,o,p,-1) else
dfs(x1,y1,q+1,1,o,p,-1);
end;
end;
end;
if bo then exit;
end;
if bo then exit;
b[o,p]:=false;
end;

begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
readln(m,n);
fillchar(a,sizeof(a),255);
for i:=1 to n do
begin
readln(x,y,z);
a[x,y]:=z;
end;
l:=0;r:=2000;
bo1:=false;
repeat
s:=0;
mid:=(l+r) div 2;
bo:=false;
fillchar(b,sizeof(b),0);
dfs(1,1,0,1,0,0,-1);
if bo then begin bo1:=true;r:=mid;end else l:=mid+1;
until l>=r;
if bo1 then writeln(l) else writeln(-1);
close(input);
close(output);
end.
var a,b,f:array[1..1000001] of longint;
st:array[1..1000001,0..20] of longint;
n,m,k,j,i,x,y,l,r,mid:longint;

function max(o,p:longint):longint;
begin
if o>p then exit(o) else exit(p);
end;

function zhi(o,p:longint):longint;
var u:longint;
begin
u:=trunc(ln(p-o+1)/ln(2));
zhi:=max(st[o,u],st[p-1 shl u+1,u]);
end;

function find1(o,p:longint):longint;
var l1,r1,mi:longint;
begin
l1:=1;r1:=p;
repeat
mi:=(l1+r1) div 2;
if a[mi]=o then exit(mi);
if a[mi]<o then l1:=mi+1 else r1:=mi-1;
until l1>r1;
if l1>p then l1:=p;
exit(l1);
end;

function find2(o,p:longint):longint;
var l1,r1,mi:longint;
begin
l1:=1;r1:=p;
repeat
mi:=(l1+r1) div 2;
if a[mi]=o then exit(mi);
if a[mi]<o then l1:=mi+1 else r1:=mi-1;
until l1>r1;
if l1>p then l1:=p;
exit(l1);
end;

function check(o:longint):boolean;
var l1,r1,i1,j1,k1:longint;
begin
if m+o<a[1] then exit(false);
for i:=1 to n do
for j:=0 to trunc(ln(n)/ln(2)) do st[i,j]:=-(maxlongint div 10);
if(a[1]>=m-o) then begin f[1]:=b[1];st[1,0]:=f[1];end else
f[1]:=-(maxlongint div 10);
for i1:=2 to n do
begin
if a[i1-1]+m+o<a[i1] then exit(false);
l1:=find1(a[i1]-m-o,i1-1);
r1:=find2(a[i1]-m+o,i1-1);
if a[i1]<=m+o then f[i1]:=max(b[i1],zhi(l1,r1)+b[i1]) else
f[i1]:=zhi(l1,r1)+b[i1];
st[i1,0]:=f[i1];
for j1:=1 to trunc(ln(i1)/ln(2)) do
begin
k1:=i1-1 shl j1+1;
st[k1,j1]:=max(max(st[k1,j1-1],st[i1-1 shl(j1-1),j1-1]),f[i1]) ;
end;
if f[i1]>=k then exit(true);
end;
exit(false);
end;

begin
assign(input,'jump.in');
assign(output,'jump.out');
reset(input);
rewrite(output);
readln(n,m,k);
for i:=1 to n do readln(a[i],b[i]);
if not check(a[n]) then begin writeln(-1);close(input);close(output);exit;end;
l:=0;r:=a[n];
repeat
mid:=(l+r) div 2;
if check(mid) then r:=mid else l:=mid+1;
until l>=r;
writeln(l);
close(input);
close(output);
end.

var
  n,m,i,j,x,y,z:longint;
  a:array[0..1005,0..1005]of longint;
  f:array[0..1005,0..1005,0..2]of longint;
function min(x,y:longint):longint;
begin
  if x<y then exit(x)else exit(y);
end;
function did(x:longint):longint;
begin
  if x=1 then exit(2)else exit(1);
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(x,y,z);
    a[x,y]:=z+1;
  end;
  for i:=1 to n do
  for j:=1 to n do
  begin
    f[i,j,1]:=maxlongint div 5;
    f[i,j,2]:=maxlongint div 5;
  end;
  if a[1,1]>0 then f[1,1,a[1,1]]:=0 else
  begin
    f[1,1,1]:=2;
    f[1,1,2]:=2;
  end;
  for i:=1 to n do
  for j:=1 to n do
  begin
    if i<>1 then
    case a[i-1,j]of
      0:
      begin
        if a[i,j]>0 then
        begin
          f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i-1,j,a[i,j]]);
          f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i-1,j,did(a[i,j])]+1);
        end;
      end;
      1:
      begin
        if a[i,j]=0 then
        begin
          f[i,j,1]:=min(f[i,j,1],f[i-1,j,1]+2);
          f[i,j,2]:=min(f[i,j,2],f[i-1,j,1]+3);
        end else
        if a[i,j]=1 then f[i,j,1]:=min(f[i,j,1],f[i-1,j,1])else
                         f[i,j,2]:=min(f[i,j,2],f[i-1,j,1]+1);
      end;
      2:
      begin
        if a[i,j]=0 then
        begin
          f[i,j,1]:=min(f[i,j,1],f[i-1,j,1]+3);
          f[i,j,2]:=min(f[i,j,2],f[i-1,j,2]+2);
        end else
        if a[i,j]=1 then f[i,j,1]:=min(f[i,j,1],f[i-1,j,2]+1)else
                         f[i,j,2]:=min(f[i,j,2],f[i-1,j,2]);
      end;
    end;
    if j<>1 then
    case a[i,j-1]of
      0:
      begin
        if a[i,j]>0 then
        begin
          f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i,j-1,a[i,j]]);
          f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i,j-1,did(a[i,j])]+1);
        end;
      end;
      1:
      begin
        if a[i,j]=0 then
        begin
          f[i,j,1]:=min(f[i,j,1],f[i,j-1,1]+2);
          f[i,j,2]:=min(f[i,j,2],f[i,j-1,1]+3);
        end else
        if a[i,j]=1 then f[i,j,1]:=min(f[i,j,1],f[i,j-1,1])else
                         f[i,j,2]:=min(f[i,j,2],f[i,j-1,1]+1);
      end;
      2:
      begin
        if a[i,j]=0 then
        begin
          f[i,j,1]:=min(f[i,j,1],f[i,j-1,1]+3);
          f[i,j,2]:=min(f[i,j,2],f[i,j-1,2]+2);
        end else
        if a[i,j]=1 then f[i,j,1]:=min(f[i,j,1],f[i,j-1,2]+1)else
                         f[i,j,2]:=min(f[i,j,2],f[i,j-1,2]);
      end;
    end;
  end;
  if a[n,n]>0 then
  begin
    if f[n,n,a[n,n]]=maxlongint div 5then writeln(-1)else
    writeln(f[n,n,a[n,n]]);
  end else
  begin
    if min(f[n,n,1],f[n,n,2])=maxlongint div 5then
    writeln(-1)else writeln(min(f[n,n,1],f[n,n,2]));
  end;
  close(input);
  close(output);
end.


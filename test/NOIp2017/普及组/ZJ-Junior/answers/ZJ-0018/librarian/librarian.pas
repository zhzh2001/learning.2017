var a,b,len:array[0..10007] of longint;
     i,j,n,m,x,y,k,ans:longint;
    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
procedure init;
 begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
   readln(n,m);
   for i:=1 to n do readln(a[i]);
   for i:=1 to m do readln(len[i],b[i]);
 end;
function work(x:longint):longint;
 var xx:longint;
 begin
  xx:=x;
  work:=1;
  while x>=10 do
   begin
    work:=work*10; x:=x div 10;
   end;
   exit(xx mod work);
 end;
function check(x,y:longint):boolean;
 begin
  check:=false;
  while x>=y do
   begin
    if x=y then exit(true);
    x:=work(x);
   // writeln(x);
   end;
  end;
procedure main;
 begin
  sort(1,n);
  for i:=1 to m do
   begin
    ans:=-1;
    for j:=1 to n do
     if check(a[j],b[i]) then begin ans:=a[j]; break; end;
    writeln(ans);
   end;
 end;
procedure print;
 begin
  //writeln(work(1123));
  close(input); close(output);
 end;
begin
 init;
 main;
 print;
end.

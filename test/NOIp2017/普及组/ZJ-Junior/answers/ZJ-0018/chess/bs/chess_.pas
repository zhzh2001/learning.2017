var map:array[-1..101,-1..101] of longint;
     a:array[1..4] of longint=(0,1,0,-1);
     b:array[1..4] of longint=(1,0,-1,0);
     f:array[0..101,0..101] of longint;
     i,j,n,m,x,y,k,ans:longint;
procedure init;
 var  x,y,clo:longint;
 begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess_.out');rewrite(output);
   readln(m,n);
   for i:=1 to n do
    begin
     readln(x,y,clo);
     map[x,y]:=clo+1;
    end;
 end;
 function min(a,b:longint):longint;
  begin
    if a<b then exit(a) else exit(b);
  end;
function check(x,y,xx,yy:longint):boolean;
 begin
  check:=true;
  if (x>m)or(y>m)or(x<1)or(y<1) then exit(false);
  if (map[x,y]=0) and (map[xx,yy]=0) then exit(false);
 end;
function work(x,y,cc:longint):longint;
 begin
  if map[x,y]=0 then exit(2);
  if map[x,y]=cc then exit(0) else exit(1);
 end;
function work2(x,y,cc:longint):longint;
 begin
  if map[x,y]=0 then exit(cc);
  exit(map[x,y]);
 end;
procedure dfs(x,y,clo,res:longint);
 var i,nowx,nowy:longint;
 begin
  if (x=m)and(y=m) then begin ans:=min(ans,res); exit; end;
  for i:=1 to 2 do
   begin
    nowx:=x+a[i]; nowy:=y+b[i];
    if check(nowx,nowy,x,y) then
    dfs(nowx,nowy,work2(nowx,nowy,clo),res+work(nowx,nowy,clo));
   end;
 end;
procedure main;
 begin
  ans:=maxlongint;
  dfs(1,1,map[1,1],0);
  if ans=maxlongint then writeln('-1') else writeln(ans);
 end;
procedure print;
 begin
  close(input); close(output);
 end;
begin
 init;
 main;
 //print;
end.

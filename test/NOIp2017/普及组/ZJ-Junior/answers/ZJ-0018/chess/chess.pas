var map:array[0..101,0..101] of longint;
     f:array[0..101,0..101] of longint;
     vis:array[0..101,0..101] of boolean;
     que:array[0..1000001] of record
                        x,y:longint;  end;
     pre:array[0..101,0..101] of record
                        x,y:longint; end;
     cl:array[0..1000001] of longint;
     i,j,n,m,x,y,k,ans,tot:longint;
     su:array[0..1000001] of record 
                              x,y:longint; end;
     a:array[1..4] of longint=(1,0,-1,0);
     b:array[1..4] of longint=(0,1,0,-1);
procedure init;
 var x,y,clo:longint;
 begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
   readln(m,n);
   for i:=1 to n do
    begin
     readln(x,y,clo);
     map[x,y]:=clo+1;
    end;
 end;
function min(a,b:longint):Longint;
 begin
  if a<b then exit(a) else exit(b);
 end;
function check(x,y,id:longint):boolean;
 begin
  check:=true;
  if (x>m)or(y>m)or(x<1)or(y<1) then exit(false);
  if (map[que[id].x,que[id].y]=0)and(map[x,y]=0) then exit(false);
 end;
function work(id,x,y:longint):Longint;
 begin
  if map[x,y]=0 then exit(2);
  if map[x,y]<>cl[id] then exit(1) else exit(0);
 end;
function work2(id,x,y:longint):longint;
 begin
  if map[x,y]<>0 then exit(map[x,y]);
  exit(cl[id]);
 end;
procedure print;
 begin
  close(input); close(output);
 end;
procedure main;
 var head,tail,nowx,nowy:longint;
 begin
  fillchar(f,sizeof(f),$7f);
  head:=0; tail:=1; que[1].x:=1; que[1].y:=1; cl[1]:=map[1,1]; f[1,1]:=0;
  ans:=maxlongint;
  while head<tail do
   begin
    inc(head);
    vis[que[head].x,que[head].y]:=false;
    for i:=1 to 4 do
     begin
      nowx:=que[head].x+a[i]; nowy:=que[head].y+b[i];
      if check(nowx,nowy,head) then
        if f[nowx,nowy]>f[que[head].x,que[head].y]+work(head,nowx,nowy) then
         begin
          f[nowx,nowy]:=f[que[head].x,que[head].y]+work(head,nowx,nowy);
           pre[nowx,nowy].x:=que[head].x;pre[nowx,nowy].y:=que[head].y;
           if not vis[nowx,nowy] then
            begin
             inc(tail); que[tail].x:=nowx; que[tail].y:=nowy; cl[tail]:=work2(head,nowx,nowy);
             vis[nowx,nowy]:=true;
            end;
           if (nowx=m)and(nowy=m) then ans:=min(f[nowx,nowy],ans);
         end;
    end;
  end;
   if ans=maxlongint then begin writeln('-1'); print; halt; end; 
   x:=m; y:=m;
   while (x<>1)or(y<>1) do begin inc(tot); su[tot].x:=x; su[tot].y:=y; x:=pre[su[tot].x,su[tot].y].x; y:=pre[su[tot].x,su[tot].y].y; end;
   inc(tot); su[tot].x:=1; su[tot].y:=1;
   ans:=0;
   for i:=tot-1 downto 1 do
    if map[su[i].x,su[i].y]=0 then begin map[su[i].x,su[i].y]:=map[su[i+1].x,su[i+1].y]; ans:=ans+2; end;
   for i:=tot-1 downto 1 do
    if map[su[i].x,su[i].y]<>map[su[i+1].x,su[i+1].y] then inc(ans);
   writeln(ans);
 end;
begin
 init;
 main;
 print;
end.


var
  i,j,k,t,m,n,x,y,sum,bj,q:longint;
  a:array[0..1005] of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[random(r-l+1)+l];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  randomize;
  sort(1,n);
  for i:=1 to q do
    begin
      readln(x,y);
      k:=1;
      for j:=1 to x do
        k:=k*10;
      bj:=0;
      for j:=1 to n do
        begin
          if a[j]<y then continue;
          t:=a[j] mod k;
          if t=y then begin
                        writeln(a[j]); bj:=1; break;
                      end;
        end;
      if bj=0 then writeln(-1);
    end;
  close(input); close(output);
end.
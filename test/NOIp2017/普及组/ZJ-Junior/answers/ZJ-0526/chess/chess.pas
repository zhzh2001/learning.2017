const dx:array[1..4] of longint=(0,1,0,-1);
      dy:array[1..4] of longint=(-1,0,1,0);
var
  i,j,k,t,m,n,x,y,ans:longint;
  a:array[0..105,0..105] of longint;
  f:array[0..60005,1..6] of longint;
  b:array[0..105,0..105,0..1,0..2] of longint;
procedure doing;
var
  i,j,k,t,h,x,y,x1,y1,ans:longint;
begin
  h:=0; t:=1;
  f[1,1]:=1; f[1,2]:=1; f[1,3]:=0; f[1,4]:=0; f[1,5]:=0;
  b[1,1,0,0]:=0;
  while h<t do
    begin
      inc(h); x:=f[h,1]; y:=f[h,2];
      for i:=1 to 4 do
        begin
          x1:=x+dx[i]; y1:=y+dy[i];
          if (x1<1)or(x1>m)or(y1<1)or(y1>m) then continue;
          if a[x1,y1]<>3 then
            begin
              if f[h,4]=1 then k:=f[h,5]-1 else k:=a[x,y];
              if a[x1,y1]<>k
                then ans:=f[h,3]+1
                else ans:=f[h,3];
              if ans<b[x1,y1,0,0]
                then begin
                       inc(t); f[t,1]:=x1; f[t,2]:=y1;
                       f[t,3]:=ans; f[t,4]:=0; f[t,5]:=0;
                       b[x1,y1,0,0]:=ans;
                     end;
            end
          else begin
                 if f[h,4]=0 then
                   begin
                     if a[x,y]=1 then begin
                                        if f[h,3]+2<b[x1,y1,1,2] then
                                        begin
                                        inc(t); f[t,1]:=x1; f[t,2]:=y1;
                                        f[t,3]:=f[h,3]+2; f[t,4]:=1;
                                        f[t,5]:=2; b[x1,y1,1,1]:=f[h,3]+2;
                                        end;
                                        if f[h,3]+3<b[x1,y1,1,1] then
                                        begin
                                        inc(t); f[t,1]:=x1; f[t,2]:=y1;
                                        f[t,3]:=f[h,3]+3; f[t,4]:=1;
                                        f[t,5]:=1; b[x1,y1,1,2]:=f[h,3]+3;
                                        end;
                                      end
                     else begin
                            if f[h,3]+2<b[x1,y1,1,1] then
                            begin
                            inc(t); f[t,1]:=x1; f[t,2]:=y1;
                            f[t,3]:=f[h,3]+2; f[t,4]:=1;
                            f[t,5]:=1; b[x1,y1,1,2]:=f[h,3]+2;
                            end;
                            if f[h,3]+3<b[x1,y1,1,2] then
                            begin
                            inc(t); f[t,1]:=x1; f[t,2]:=y1;
                            f[t,3]:=f[h,3]+3; f[t,4]:=1;
                            f[t,5]:=2; b[x1,y1,1,1]:=f[h,3]+3;
                            end;
                          end;
                   end;
               end;
        end;
    end;
end;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=3;
  for i:=1 to n do
    begin
      readln(x,y,t);
      a[x,y]:=t;
    end;
  fillchar(f,sizeof(f),0); fillchar(b,sizeof(b),1);
  doing;
  ans:=0;
  if a[m,m]=3 then begin
                   ans:=min(b[m,m,1,1],b[m,m,1,2]);
                   if ans<1000000 then writeln(ans)
                                  else writeln(-1);
                   end
  else begin
         if b[m,m,0,0]<1000000
           then writeln(b[m,m,0,0])
           else writeln(-1);
       end;
  close(input); close(output);
end.
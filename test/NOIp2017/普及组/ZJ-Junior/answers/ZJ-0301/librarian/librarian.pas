var
  n,q,i,len,j:longint;
  ch:char;
  xqm:string;
  f:boolean;
  s:array[1..1000] of string;
procedure px(l,r:longint);
var
  i,j:longint;
  mid,t:string;
begin
  i:=l;
  j:=r;
  mid:=s[(l+r) div 2];
  repeat
    while (length(s[i])<length(mid)) or (length(s[i])=length(mid)) and (s[i]<mid) do inc(i);
    while (length(s[j])>length(mid)) or (length(s[j])=length(mid)) and (s[j]>mid) do dec(j);
    if i<=j then
    begin
      t:=s[i];
      s[i]:=s[j];
      s[j]:=t;
      inc(i);
      dec(j);
    end;
  until i>j;
  if i<r then px(i,r);
  if l<j then px(l,j);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(s[i]);
  px(1,n);
  for i:=1 to q do
  begin
    read(len);
    read(ch);
    readln(xqm);
    f:=true;
    for j:=1 to n do
      if copy(s[j],length(s[j])-len+1,len)=xqm then
      begin
        writeln(s[j]);
        f:=false;
        break;
      end;
    if f then writeln(-1);
  end;
  close(input);close(output);
end.

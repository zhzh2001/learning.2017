var
  n,d,k,i,sum,l,r,g,j,ans:longint;
  x,s,f:array[0..500000] of longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do readln(x[i],s[i]);
  if x[n]>=d then r:=x[n] else r:=d;
  ans:=-1;
  repeat
    g:=(l+r) shr 1;
    for i:=1 to n do
    begin
      f[i]:=-1000000000;
      for j:=0 to i-1 do
        if x[i]-x[j]<d-g then break
        else if (x[i]-x[j]<=d+g) and (f[j]+s[i]>f[i]) then f[i]:=f[j]+s[i]
    end;
    for i:=1 to n do
      if f[i]>=k then break;
    if f[i]>=k then
    begin
      ans:=g;
      r:=g-1;
    end
    else l:=g+1;
  until l>r;
  writeln(ans);
  close(input);close(output);
end.

const
  dx:array[1..4] of longint=(-1,0,0,1);
  dy:array[1..4] of longint=(0,-1,1,0);
var
  m,n,i,xx,yy,j,h,t,tmp:longint;
  a:array[1..100,1..100] of longint;
  f:array[1..100,1..100,1..2] of longint;
  x,y,co:array[1..1000000] of longint;
  v:array[1..100,1..100] of boolean;
function min(x,y:longint):longint;
begin
  if x<=y then exit(x) else exit(y);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to n do
  begin
    read(xx,yy);
    readln(a[xx,yy]);
    inc(a[xx,yy]);
  end;
  for i:=1 to m do
    for j:=1 to m do
    begin
      f[i,j,1]:=100000000;
      f[i,j,2]:=100000000;
    end;
  f[1,1,1]:=0;
  x[1]:=1;
  y[1]:=1;
  co[1]:=a[1,1];
  v[1,1]:=true;
  h:=0;
  t:=1;
  repeat
    inc(h);
    v[x[h],y[h]]:=false;
    for i:=1 to 4 do
    begin
      xx:=x[h]+dx[i];
      yy:=y[h]+dy[i];
      if (xx>=1) and (xx<=m) and (yy>=1) and (yy<=m) then
        if a[xx,yy]=co[h] then
        begin
          tmp:=min(f[x[h],y[h],1],f[x[h],y[h],2]);
          if tmp<f[xx,yy,1] then
          begin
            f[xx,yy,1]:=tmp;
            if not v[xx,yy] then
            begin
              inc(t);
              x[t]:=xx;
              y[t]:=yy;
              co[t]:=a[xx,yy];
              v[xx,yy]:=true;
            end;
          end;
        end
        else if a[xx,yy]<>0 then
        begin
          tmp:=min(f[x[h],y[h],1],f[x[h],y[h],2])+1;
          if tmp<f[xx,yy,1] then
          begin
            f[xx,yy,1]:=tmp;
            if not v[xx,yy] then
            begin
              inc(t);
              x[t]:=xx;
              y[t]:=yy;
              co[t]:=a[xx,yy];
              v[xx,yy]:=true;
            end;
          end;
        end
        else
        begin
          tmp:=f[x[h],y[h],1]+2;
          if tmp<f[xx,yy,2] then
          begin
            f[xx,yy,2]:=tmp;
            if not v[xx,yy] then
            begin
              inc(t);
              x[t]:=xx;
              y[t]:=yy;
              co[t]:=co[h];
              v[xx,yy]:=true;
            end;
          end;
        end;
    end;
  until h>=t;
  tmp:=min(f[m,m,1],f[m,m,2]);
  if tmp=100000000 then writeln(-1) else writeln(tmp);
  close(input);close(output);
end.

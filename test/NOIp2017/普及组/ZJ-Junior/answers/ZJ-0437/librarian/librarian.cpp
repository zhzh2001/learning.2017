#include <iostream>

using namespace std;

long long n , q , booknum[1010] , req , sav[1010] , i , len , c;

int main ()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	
	cin>>n>>q;
	for (i=1;i<=n;i++) cin>>booknum[i];
	
	for (i=1;i<=q;i++)
	{
		cin>>len>>req;
		c=1;
		
		for (long long j=1;j<=n;j++)
		{
			long long sum=1;
			
			for (long long ii=1;ii<=len;ii++) sum*=10;
			
			if ( booknum[j]%sum==req )
			{
				sav[c]=booknum[j];
				c++;
			}
		}
		
		if ( c==1 ) cout<<-1<<"\n";
		else {
			long long minn=sav[1];
			for (long long j=2;j<c;j++)
			minn=minn>sav[j]?sav[j]:minn;
			cout<<minn<<"\n";
		}
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
inline int red(){
	int ret=0,flg=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')flg=-1;ch=getchar();}
	while (ch>='0'&&ch<='9')ret=ret*10+ch-48,ch=getchar();
	return ret*flg;
}
inline int workt(int len){
	int ret=1;
	for (int i=1;i<=len;i++)ret*=10;
	return ret;
}
inline bool check(int len,int bok,int ned){
	int k=bok-ned;
	if (k<0)return 0;
	if (k%workt(len)==0)return 1;
	return 0;
}
int n,q,num[1005];
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=red(),q=red();
	for (int i=1;i<=n;i++)num[i]=red();
	sort(num+1,num+1+n);
	for (int i=1;i<=q;i++){
		int le,ne;le=red(),ne=red();
		bool vis=0;
		for (int j=1;j<=n;j++)if (check(le,num[j],ne)){printf("%d\n",num[j]);vis=1;break;}
		if (!vis)printf("-1\n");
	}
//	printf("%d\n",check(3,1123,123));
	return 0;
}

#include<cstdio>
#include<algorithm>
#define LL long long
using namespace std;
inline int red(){
	int ret=0,flg=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')flg=-1;ch=getchar();}
	while (ch>='0'&&ch<='9')ret=ret*10+ch-48,ch=getchar();
	return ret*flg;
}
int n,d,k,x[505],w[505];
LL sum=0,f[505];
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=red(),d=red(),k=red();
	for (int i=1;i<=n;i++)x[i]=red(),w[i]=red(),sum+=w[i]>0?w[i]:0;
	if (sum<k){printf("-1\n");return 0;}
	if (x[n]*n>=1000000000){printf("-1\n");return 0;}
//	if (d==1){printf("0\n");return 0;}
	for (int g=0;g<=x[n]-d;g++){
		if (x[1]<d-g||x[1]>d+g)continue;
		for (int i=1;i<=n;i++)f[i]=-2e9;
		f[1]=w[1];
		for (int i=2;i<=n;i++){
			for (int j=1;j<i;j++)if (x[i]-x[j]<=d+g&&x[i]-x[j]>=max(d-g,1))f[i]=max(f[i],f[j]+w[i])/*,printf("%d ",j)*/;
//			printf("%d\n",g);
//			printf("%d %d %d\n",i,j+1,g);
//			printf("%d %d %d %d\n",x[4]-x[2]<=d+2&&x[4]-x[2]>=d-2,x[4]-x[2],d+2,d-2);
			if (f[i]>=k)break;
		}
//		for (int i=1;i<=n;i++)printf("%d ",f[i]);
		for (int i=1;i<=n;i++)if (f[i]>=k){printf("%d\n",g);return 0;}
	}
	printf("-1\n");
	return 0;
}

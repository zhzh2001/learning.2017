#include<cstdio>
using namespace std;
inline int red(){
	int ret=0,flg=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')flg=-1;ch=getchar();}
	while (ch>='0'&&ch<='9')ret=ret*10+ch-48,ch=getchar();
	return ret*flg;
}
int wor,tex,exa;
double ans;
int main(){
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	wor=red(),tex=red(),exa=red();
	ans=((double)(wor*1.0)*0.2)+((double)(tex*1.0)*0.3)+((double)(exa*1.0)*0.5);
	printf("%0.0lf\n",ans);
	return 0;
}

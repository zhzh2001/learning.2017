#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int flg[4][2]={{-1,0},{1,0},{0,-1},{0,1}};
inline int red(){
	int ret=0,flg=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')flg=-1;ch=getchar();}
	while (ch>='0'&&ch<='9')ret=ret*10+ch-48,ch=getchar();
	return ret*flg;
}
int m,n,mp[105][105],ans=2e9;
bool vis[105][105];
inline void dfs(int x,int y,int now,bool nh){
	if (x==m&&y==m){ans=min(ans,now);return;}
	for (int i=0;i<4;i++){
		int xx=x+flg[i][0],yy=y+flg[i][1];
		if (xx<1||xx>m||yy<1||yy>m||vis[xx][yy])continue;
		vis[xx][yy]=1;
		if (mp[x][y]==0){
			if (mp[xx][yy]==0)dfs(xx,yy,now,0);
			if (mp[xx][yy]==1)dfs(xx,yy,now+1,0);
			if (mp[xx][yy]==2&&!nh){
				mp[xx][yy]=0;dfs(xx,yy,now+2,1);
				mp[xx][yy]=1;dfs(xx,yy,now+3,1);
				mp[xx][yy]=2;
			}
		}
		if (mp[x][y]==1){
			if (mp[xx][yy]==0)dfs(xx,yy,now+1,0);
			if (mp[xx][yy]==1)dfs(xx,yy,now,0);
			if (mp[xx][yy]==2&&!nh){
				mp[xx][yy]=0;dfs(xx,yy,now+3,1);
				mp[xx][yy]=1;dfs(xx,yy,now+2,1);
				mp[xx][yy]=2;
			}
		}
		vis[xx][yy]=0;
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=red(),n=red();
	if (n>200){printf("-1\n");return 0;}
	for (int i=1;i<=m;i++)
	for (int j=1;j<=m;j++)mp[i][j]=2;
	for (int i=1;i<=n;i++){int x,y;x=red(),y=red();mp[x][y]=red();}
	dfs(1,1,0,0);
	printf("%d\n",ans!=2e9?ans:-1);
	return 0;
}

var m,n,i,j,min,x,y,z:longint;
    a:array[0..101,0..101] of longint;
    f:array[0..101,0..101] of boolean;
    dx:array[1..4] of longint=(0,0,1,-1);
    dy:array[1..4] of longint=(1,-1,0,0);

procedure p(x,y,z,s,t:longint);
var i,xx,yy:longint;
begin
 if s>min then exit;
 if (x=m) and (y=m) then begin min:=s; exit; end;
 for i:=1 to 4 do
 begin
  xx:=x+dx[i]; yy:=y+dy[i];
  if (xx>0) and (xx<=m) and (yy>0) and (yy<=m) and (f[xx,yy]=false) then
  begin
   if ((a[x,y]=a[xx,yy]) or (t=a[xx,yy])) and (a[xx,yy]<>-1) then
   begin
    f[xx,yy]:=true;
    p(xx,yy,a[xx,yy],s,-1);
    f[xx,yy]:=false;
   end
   else if (a[x,y]<>a[xx,yy]) and (a[x,y]<>-1) and (a[xx,yy]<>-1) or (a[x,y]=2) and (a[xx,yy]<>-1) and (t<>a[x,y]) then
   begin
    f[xx,yy]:=true;
    p(xx,yy,a[xx,yy],s+1,-1);
    f[xx,yy]:=false;
   end
   else if (a[x,y]<>-1) and (a[x,y]<>2) and (a[xx,yy]=-1) then
   begin
    f[xx,yy]:=true;
    a[xx,yy]:=2;
    p(xx,yy,a[xx,yy],s+2,a[x,y]);
    a[xx,yy]:=-1;
    f[xx,yy]:=false;
   end;
  end;
 end;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
 read(m,n);
 for i:=1 to m do
  for j:=1 to m do a[i,j]:=-1;
 for i:=1 to n do
 begin
  read(x,y,z);
  a[x,y]:=z;
 end;
 f[1,1]:=true;
 min:=maxlongint;
 p(1,1,a[1,1],0,-1);
 if min=maxlongint then writeln(-1)
 else writeln(min);
close(input);close(output);
end.


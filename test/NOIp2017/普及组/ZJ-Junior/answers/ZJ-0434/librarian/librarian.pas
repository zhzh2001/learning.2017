var n,q,i,j,l:longint;
    ch:char;
    s:string;
    st:array[1..1000] of string;
    a,len:array[1..1000] of longint;
    f:boolean;
procedure sort(l,r:longint);
var i,j,t,m:longint;
    s:string;
begin
 i:=l; j:=r; m:=a[(i+j) shr 1];
 repeat
  while (a[i]<m) do i:=i+1;
  while (a[j]>m) do j:=j-1;
  if i<=j then
  begin
   s:=st[i]; st[i]:=st[j]; st[j]:=s;
   t:=a[i]; a[i]:=a[j]; a[j]:=t;
   t:=len[i]; len[i]:=len[j]; len[j]:=t;
   i:=i+1; j:=j-1;
  end;
 until i>j;
 if i<r then sort(i,r);
 if j>l then sort(l,j);
end;
begin
assign(input,'librarian.in');reset(input);
assign(output,'librarian.out');rewrite(output);
 readln(n,q);
 for i:=1 to n do
 begin
  readln(st[i]);
  len[i]:=length(st[i]);
  val(st[i],a[i]);
 end;
 sort(1,n);
 for i:=1 to q do
 begin
  readln(l,ch,s);
  f:=false;
  for j:=1 to n do
   if (len[j]>=l) and (copy(st[j],len[j]-l+1,l)=s) then
   begin
    f:=true;
    writeln(a[j]);
    break;
   end;
  if f=false then writeln(-1);
 end;
close(input);close(output);
end.

const
	maxsize=40000;
	dx:array[1..4] of longint=(1,-1,0,0);
	dy:array[1..4] of longint=(0,0,1,-1);
	inf=99999999;
type
	point=record
		x,y:longint;
	end;
var
	f:array[0..200,0..200,0..1] of longint; 
	a:array[0..200,0..200] of longint;
	n,m,i,x_,y_,z_,ans:longint;
	
	fifo:array[0..50000] of point;
	l,r,size:longint;
	vis:array[0..200,0..200] of boolean;
procedure update(i,j,c1,k,l,c2:longint);
var
	cost:longint;
begin
	cost:=0;
	if (a[i,j]=-1)and(a[k,l]=-1) then exit;
	if (a[k,l]=-1) then inc(cost,2);
	if (c1<>c2) then inc(cost);
	if (f[i,j,c1]+cost<f[k,l,c2]) then
	begin
		f[k,l,c2]:=f[i,j,c1]+cost;
		if (not vis[k,l]) then
		begin
			vis[k,l]:=true;
			r:=(r+1) mod maxsize;inc(size);
			fifo[r].x:=k;fifo[r].y:=l; 
		end;
	end;
end;
//spfa
procedure dfs(x_,y_:longint);
var
	xx,yy,c,i:longint;
begin
	l:=(l+1) mod maxsize;dec(size);
	if (a[x_,y_]=-1) then
	begin
		for i:=1 to 4 do
		begin
			xx:=x_+dx[i];yy:=y_+dy[i];
			if (xx<1)or(yy<1)or(xx>n)or(yy>n) then continue;
			if a[xx,yy]=-1 then continue
			else begin
				update(x_,y_,0,xx,yy,a[xx,yy]);
				update(x_,y_,1,xx,yy,a[xx,yy]);
			end;
		end;
	end
	else begin
		c:=a[x_,y_];
		for i:=1 to 4 do
		begin
			xx:=x_+dx[i];yy:=y_+dy[i];
			if (xx<1)or(yy<1)or(xx>n)or(yy>n) then continue;
			if a[xx,yy]=-1 then
			begin
				update(x_,y_,a[x_,y_],xx,yy,0);
				update(x_,y_,a[x_,y_],xx,yy,1);
			end
			else
				update(x_,y_,a[x_,y_],xx,yy,a[xx,yy]);
		end;
	end;
	vis[x_,y_]:=false;
end;

begin
	assign(input,'chess.in');reset(input);
	assign(output,'chess.out');rewrite(output); 
	fillchar(a,sizeof(a),255);
	fillchar(f,sizeof(f),30);
	readln(n,m);
	for i:=1 to m do
	begin
		readln(x_,y_,z_);a[x_,y_]:=z_;
	end;
	//chu_shi_hua
	f[1,1,a[1,1]]:=0;
	fillchar(vis,sizeof(vis),false);vis[1,1]:=true;
	fifo[0].x:=1;fifo[0].y:=1;l:=0;r:=0;size:=1;
	//spfa
	repeat
		dfs(fifo[l].x,fifo[l].y);
	until size=0;
	if (a[n,n]<>-1) then
		ans:=f[n,n,a[n,n]]
	else begin
		ans:=f[n,n,0];
		if f[n,n,1]<ans then ans:=f[n,n,1]; 
	end;
	if ans>inf then ans:=-1;
	writeln(ans);
	close(input);close(output);
end.

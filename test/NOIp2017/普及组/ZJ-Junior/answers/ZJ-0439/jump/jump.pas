type
	gezi=record
		x,v:longint;
	end;
var
	n,d,m,i,l,r,mid:longint;
	a:array[0..1000000] of gezi;
	f:array[0..1000000] of longint;
	flag:boolean;
	
	heap:array[0..1000000] of longint;
	poss:array[0..1000000] of longint;
	size:longint;
	
procedure qs(l,r:longint);
var
	i,j,m:longint;
	t:gezi;
begin
	if l>=r then exit;
	i:=l;j:=r;m:=a[(l+r+1)>>1].x;
	repeat
		while (a[i].x<m) do inc(i);
		while (a[j].x>m) do dec(j);
		if i<=j then
		begin
			t:=a[i];a[i]:=a[j];a[j]:=t;
			inc(i);dec(j);
		end;
	until i>j;
	if (l<j) then qs(l,j);
	if (i<r) then qs(i,r);
end;

function max(num1,num2:longint):longint;
begin
	if num1>num2 then exit(num1);exit(num2);
end;

procedure switch(i,j:longint);
var
	t:longint;
begin
	poss[heap[i]]:=j;poss[heap[j]]:=i;
	t:=heap[i];heap[i]:=heap[j];heap[j]:=t;
end;

procedure down(j:longint);
var
	minn,i:longint;
begin
	i:=j;
	while i<<1<=size do
	begin
		minn:=i<<1;
		if (minn+1<=size)and(f[heap[minn+1]]>f[heap[minn]]) then inc(minn);
		if (f[heap[minn]]>f[heap[i]]) then
		begin
			switch(minn,i);i:=minn;
		end
		else break;
	end;
end;

procedure dele(i:longint);
begin
	switch(i,size);dec(size);
	down(i);
end;

procedure up(j:longint);
var
	fa,i:longint;
begin
	i:=j;
	while i<>1 do
	begin
		fa:=i>>1;
		if (f[heap[i]]>f[heap[fa]]) then
		begin
			switch(i,fa);i:=fa;
		end
		else break;
	end;
end;

function check(mid:longint):boolean;
var
	l,r,i,ld,rd:longint;
begin
	ld:=d-mid;if ld<=0 then ld:=1;
	rd:=d+mid;
	
	fillchar(f,sizeof(f),0);
	if (ld<=a[1].x)and(a[1].x<=rd) then
	begin
		f[1]:=max(f[1],a[1].v);
		size:=1;heap[1]:=1;poss[1]:=1;
		l:=1;r:=1;
		if f[1]>=m then exit(true);
	end
	else exit(0>=m);
	
	for i:=2 to n do
	begin
		//wei_hu
		while (a[l].x<a[i].x-rd) do
		begin
			dele(poss[l]);
			inc(l);
		end;
		while (a[r+1].x<=a[i].x-ld) do
		begin
			inc(r);
			inc(size);heap[size]:=r;poss[r]:=size;
			up(size);
		end;
		//zhuan_yi
		if (ld<=a[i].x)and(a[i].x<=rd) then f[i]:=max(f[i],a[i].v);
		f[i]:=max(f[i],f[heap[1]]+a[i].v);
		if f[i]>=m then exit(true);
	end;
	exit(false);
end;

begin
	assign(input,'jump.in');reset(input);
	assign(output,'jump.out');rewrite(output);
	readln(n,d,m);
	for i:=1 to n do readln(a[i].x,a[i].v);
	qs(1,n);
	//for i:=1 to n do writeln(a[i].x:10,a[i].v:10);
	l:=0;r:=a[n].x+10;
	//er_fen
	while l+10<r do
	begin
		mid:=(l+r)>>1;
		if check(mid) then r:=mid else l:=mid;
	end;
	flag:=false;
	for i:=l to r do
		if check(i) then
		begin
			flag:=true;break;
		end;
	if flag then writeln(i) else writeln(-1);
	close(input);close(output);
end.

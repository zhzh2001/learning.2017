var
	n,m,i,j,x,l,ans:longint;
	a:array[0..2000] of string;
	b:array[0..2000] of longint;
	s:string;
function check(l:longint;s:string;j:longint):boolean;
begin
	if l>b[j] then exit(false);
	exit(copy(a[j],b[j]-l+1,l)=s);
end;
function small(s1,s2:string):boolean;
var
	x1,x2:longint;
begin
	val(s1,x1);val(s2,x2);
	exit(x1<x2);
end;
begin
	assign(input,'librarian.in');reset(input);
	assign(output,'librarian.out');rewrite(output);
	readln(n,m);
	for i:=1 to n do
	begin
		readln(x);
		str(x,a[i]);
		//writeln(a[i]);
		b[i]:=length(a[i]);
	end;
	for i:=1 to m do
	begin
		readln(l,x);
		str(x,s);
		ans:=-1;
		for j:=1 to n do
			if (check(l,s,j))and((ans=-1)or(small(a[j],a[ans]))) then ans:=j;
		if ans=-1 then writeln(-1) else writeln(a[ans]);
	end;
	close(input);close(output);
end.

#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
struct node
{
	int p,s;
};
node a[500005];
long n,d,k,l,r,mid,ans=-1,ll,rr,ma,x;
long long sum;
int read()
{
	int x=0,f=1;
	char ch=getchar();
	while('0'>ch || ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar(); 
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
} 

bool check(int g)
{
	if(d-g>0) l=d-g;
	else l=1;
	r=d+g;
	sum=0;
	int j=0,i=0;
	while(++i<=n)
	{
		ma=-1000000;
		if(a[j].p+r<a[i].p)
		return 0;
		if(a[j].p+l>a[i].p)
		i++;
		else
		{
			while(a[i].s<=0 && i<=n)
			i++;
			if(i>n)
			return 0;
			if(a[i].s>0 && a[j].p+r>=a[i].p)
			{
				sum+=a[i].s;
				if(sum>=k)
				return 1;
			}
			else
			{
				int li=i-1;
				while(a[li].p+r>=a[i].p)
				{
					if(ma<a[li].s)
					x=li,ma=a[li].s;
					li--;
				}
				sum+=a[x].s;
				i=x;
			}
		}
		j=i;
	}
	return 0;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for(int i=1;i<=n;i++)
	a[i].p=read(),a[i].s=read();
	ll=0,rr=a[n].p;
	while(ll<=rr)
	{
		mid=(ll+rr)>>1;
		if(check(mid))
		ans=mid,rr=mid-1;
		else
		ll=mid+1;
	}
	cout<<ans;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int a[105][105]={-1},n,m,x,y,c,d; 
int read()
{
	int x=0,f=1;
	char ch=getchar();
	while('0'>ch || ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar(); 
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

int find(int x,int y,int z,int s)
{
	if(x==1 && y==1)
	return 0;
	int s1,s2;
	if(x>1)
	{
		if(a[x-1][y]==-1 || s) return -1;
		if(a[x-1][y]>-1 || !s)
		{
			if(a[x-1][y]==z)
			{
				s1=find(x-1,y,z,1);
				if(s1==-1) s1=200000;
			}
			else
			if(a[x-1][y]==1-z)
			{
				s1=find(x-1,y,a[x-1][y],0)+1;
				if(s1==0) s2=200000;
			}
			else
			{
				s1=find(x-1,y,z,1)+2;
				if(s1==1) s1=200000;
			}
		}
	}
	if(y>1)
	{
		if(a[x][y-1]==-1 || s) return -1;
		if(a[x][y-1]>-1 || !s)
		{
			if(a[x][y-1]==z)
			{
				s2=find(x,y-1,z,1);
				if(s2==-1) s2=200000;
			}
			else
			if(a[x][y-1]==1-z)
			{
				s2=find(x,y-1,a[x][y-1],0)+1;
				if(s2==0) s2=200000;
			}
			else
			{
				s2=find(x,y-1,z,1)+2;
				if(s2==1) s2=200000;
			}
		}
	}
	return min(s1,s2);
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read(),n=read();
	for(int i=1;i<=n;i++)
	{
		x=read(),y=read(),c=read();
		a[x][y]=c;
	}
	if(a[m][m]>-1)
	d=find(m,m,a[m][m],0);
	else
	d=find(m,m,a[m][m],1);
	if(d==200000)
	cout<<-1;
	else
	cout<<d;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

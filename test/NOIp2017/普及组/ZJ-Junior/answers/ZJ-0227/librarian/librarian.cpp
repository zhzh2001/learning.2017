#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
int a[1005],n,q,b,c;
bool f;
int read()
{
	int x=0,f=1;
	char ch=getchar();
	while('0'>ch || ch>'9')
	{
		if(ch=='-') f=-1;
		ch=getchar(); 
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
} 

bool check(int x,int y,int z)
{
	int s=1;
	for(int k=1;k<=y;k++)
	s*=10;
	if(x%s==z) return true;
	else return false;
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),q=read();
	for(int i=1;i<=n;i++)
	a[i]=read();
	sort(a+1,a+n+1);
	for(int i=1;i<=q;i++)
	{
		b=read(),c=read(),f=true;
		for(int i=1;i<=n;i++)
		if(check(a[i],b,c))
		{
			cout<<a[i]<<endl;
			f=false;
			break;
		}
		if(f)
		cout<<-1<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

var
  n,d,k,l,r,mid,i:longint;
  x,s:array[0..100000] of longint;
  f:array[0..1000000] of int64;
function max(a,b:int64):int64;
begin
  if a>b then exit(a);
  exit(b);
end;
function pd(g:longint):boolean;
var
  i,l,r,mid,a,b,j:longint;
begin
  for i:=1 to n do
  f[i]:=-100000000000;
  f[0]:=0;
  for i:=0 to n-1 do
  begin
    l:=i+1;r:=n;
    while l<r do
    begin
      mid:=(l+r) div 2;
      if x[mid]-x[i]>=max(d-g,1) then r:=mid
      else l:=mid+1;
    end;
    if x[l]-x[i]>=max(d-g,1) then a:=l
    else a:=n+1;
    l:=i+1;r:=n;
    while l<r do
    begin
      mid:=(l+r+1) div 2;
      if x[mid]-x[i]<=d+g then l:=mid
      else r:=mid-1;
    end;
    if x[l]-x[i]<=d+g then b:=l
    else b:=0;
    for j:=a to b  do
    f[j]:=max(f[j],f[i]+s[j]);
  end;
  for i:=1 to n do
  if f[i]>=k then exit(true);
  exit(false);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  x[0]:=0;s[0]:=0;
  for i:=1 to n do
  readln(x[i],s[i]);
  l:=1;r:=1000000000;
  while l<r do
  begin
    mid:=(l+r) div 2;
    if pd(mid) then r:=mid
    else l:=mid+1;
  end;
  if pd(l) then writeln(l) else writeln(-1);
  close(input);
  close(output);
end.

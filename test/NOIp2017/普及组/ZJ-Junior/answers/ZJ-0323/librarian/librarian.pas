var
a:array[0..1000]of longint;
sk,k,i,j,sum,n,m:longint;
b:boolean;
procedure sort(l,r: longint);
var
i,j,x,t: longint;
begin
        i:=l;
        j:=r;
        x:=a[(l+r) div 2];
        repeat
                while a[i]<x do
                inc(i);
                while x<a[j] do
                dec(j);
                if not(i>j) then
                begin
                        t:=a[i];
                        a[i]:=a[j];
                        a[j]:=t;
                        inc(i);
                        j:=j-1;
                end;
        until i>j;
        if l<j then
        sort(l,j);
        if i<r then
        sort(i,r);
end;
begin
        assign(input,'librarian.in');reset(input);
        assign(output,'librarian.out');rewrite(output);
        readln(n,m);
        for i:=1 to n do
        readln(a[i]);
        sort(1,n);
        for i:=1 to m do
        begin
                b:=true;
                sum:=1;
                readln(k,sk);
                for j:=1 to k do
                sum:=sum*10;
                for j:=1 to n do
                if ((a[j]-sk) mod sum=0) and (b) then
                begin
                        b:=false;
                        writeln(a[j]);
                end;
                if b then
                writeln(-1);
        end;
        close(input);
        close(output);
end.
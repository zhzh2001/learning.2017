var
min,i,j,x,y,z,n,m:longint;
b,c:boolean;
f:array[0..101,0..101]of longint;
procedure dfs(x,y,ans:longint);
begin
        if (x=n) and (y=n) then
        begin
                if ans=-1 then
                c:=true
                else
                begin
                        if ans<=min then
                        begin
                                min:=ans;
                                b:=true;
                        end;
                end;
        end
        else
        begin
                if f[x,y+1]<>0 then
                begin
                        if f[x,y+1]=f[x,y] then
                        dfs(x,y+1,ans)
                        else
                        begin
                                if (f[x-1,y+1]=f[x,y]) and (f[x-1,y]=f[x,y]) then
                                dfs(x-1,y+1,ans)
                                else
                                dfs(x,y+1,ans+1);
                        end;
                end;
                if f[x+1,y]<>0 then
                begin
                        if f[x+1,y]=f[x,y] then
                        dfs(x+1,y,ans)
                        else
                        begin
                                if (f[x+1,y-1]=1) and (f[x,y-1]=0) then
                                dfs(x+1,y-1,ans)
                                else
                                dfs(x+1,y,ans+1);
                        end;
                end;
                if (f[x,y+1]=0) and (f[x+1,y]=0) then
                begin
                        if f[x+1,y+1]=0 then
                        dfs(n,n,-1);
                        if f[x+1,y+1]=f[x,y] then
                        dfs(x+1,y+1,ans+2);
                        if f[x+1,y+1]<>f[x,y] then
                        dfs(x+1,y+1,ans+3);
                end;
        end;
end;
begin
        assign(input,'chess.in');reset(input);
        assign(output,'chess.out');rewrite(output);
        min:=maxlongint;
        read(n,m);
        for i:=0 to n+1 do
        begin
                f[i,0]:=0;
                f[i,n+1]:=0;
                f[0,i]:=0;
                f[n+1,i]:=0;
        end;
        b:=false;
        c:=false;
        for i:=1 to m do
        begin
                read(x,y,z);
                if z=0 then
                f[x,y]:=-1
                else
                f[x,y]:=1;
        end;
        x:=0;
        y:=0;
        dfs(1,1,0);
        if (not b) and (c) then
        writeln(-1)
        else
        begin
                if b then
                writeln(min);
        end;
        close(input);
        close(output);
end.

#include<bits/stdc++.h>
using namespace std;
int m,n,a[101][101],b[101][101];
int ans[101][101];
int h[4]={-1,1,0,0},l[4]={0,0,-1,1};

int read()
{
	char c=getchar();
	int x=0,f=1;
	while(!isdigit(c)) c=getchar();
	if(c=='-')
	{
		f=-1;
		c=getchar();
	}
	while(isdigit(c))
	{
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	
	return x*f;
}

void f(int x,int y,int mon,int used)
{
	if(x==m && y==m)
	{
		if(ans[m][m]>mon)
			ans[m][m]=mon;
		return ;
	}
	
	b[x][y]=1;
	for(int i=0;i<4;++i) //�������� 
	{
		int xx=x+h[i],yy=y+l[i];
		if(b[xx][yy]) continue;
		if(xx>0 && xx<=m && yy>0 && yy<=m)
		{
			if(!a[xx][yy])
			{
				if(mon+2<ans[xx][yy] && used)
				{
					ans[xx][yy]=mon+2;
					a[xx][yy]=a[x][y];
					f(xx,yy,mon+2,0);
					a[xx][yy]=0;
				}
			}
			else if(a[xx][yy]==a[x][y])
			{
				if(mon<ans[xx][yy])
				{
					ans[xx][yy]=mon;
					f(xx,yy,mon,1);
				}
			}
			else
			{
				if(mon+1<ans[xx][yy])
				{
					ans[xx][yy]=mon+1;
					f(xx,yy,mon+1,1);
				}
			}
		}
	}
	b[x][y]=0;
}


int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read();
	n=read();
	memset(ans,127,sizeof(ans));
	ans[1][1]=0;
	ans[m][m]=50000000;
	for(int i=0;i<n;++i)
	{
		int x,y,c;
		x=read();
		y=read();
		c=read();
		a[x][y]=c+1;
	}
	
	f(1,1,0,1);
	
	if(ans[m][m]==50000000)
	{
		printf("-1\n");
		return 0;
	}
	
	printf("%d\n",ans[m][m]);
	
	return 0;
}

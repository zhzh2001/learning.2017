#include<bits/stdc++.h>
using namespace std;
int n,q,a[1002],len,s;

int read()
{
	char c=getchar();
	int x=0,f=1;
	while(!isdigit(c) && c!='-') c=getchar();
	if(c=='-')
	{
		f=-1;
		c=getchar();
	}
	while(isdigit(c))
	{
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	
	return x*f;
}

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();
	q=read();
	for(int i=0;i<n;++i) 
		a[i]=read();
	for(int i=0;i<q;++i)
	{
		len=read();
		len=pow(10,len);
		s=read();
		int minn=0x3f3f3f3f;
		int flag=0;
		for(int j=0;j<n;++j)
		{
			if(a[j]%len==s && a[j]<minn)
			{
				minn=a[j];
				flag=1;
			}
		}
		if(flag)
			printf("%d\n",minn);
		else printf("-1\n");
	}
	
	return 0;
}

#include<bits/stdc++.h>
#define M 5000005
using namespace std;
int n,d,f[M],que[M],used[M],tot,l,r;
long long k,s[M],a[M];

int read()
{
	char c=getchar();
	int x=0,f=1;
	while(!isdigit(c) && c!='-') c=getchar();
	if(c=='-')
	{
		f=-1;
		c=getchar();
	}
	while(isdigit(c))
	{
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	
	return x*f;
}

int check(int mid)  //que是序号的队列！ 
{
	//不能忘记加（减）d!!! 
	memset(a,128,sizeof(a));
	memset(used,0,sizeof(used));
	a[0]=0;
	int head=0,tail=1;
	long long maxx=0;
	while(head<=tail)
	{
		for(int i=que[head]+1;i<=n;++i)
		{
			if(f[i]-f[que[head]]>mid+d) break;
			if(f[i]-f[que[head]]<d-mid || f[i]-f[que[head]]==0) continue;
			if(a[que[head]]+s[i]>a[i])
			a[i]=a[que[head]]+s[i];
			if(!used[i])
			{
				que[tail++]=i;
				used[i]=1;
			}
			if(a[i]>maxx) maxx=a[i];
		}
		head++;
	}
	return maxx;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();
	d=read();
	k=read();
	
	for(int i=1;i<=n;++i)
	{
		f[i]=read();
		s[i]=read();
		if(s[i]>0) tot+=s[i];
	}
	r=f[n];
	if(tot<k)
	{
		printf("-1\n");
		return 0;
	}
	int mid;
	while(l<=r)
	{
		mid=(l+r)>>1;
		if(check(mid)>=k) r=mid-1;
		else l=mid+1;
	}
	
	printf("%d\n",l);
	
	return 0;
}

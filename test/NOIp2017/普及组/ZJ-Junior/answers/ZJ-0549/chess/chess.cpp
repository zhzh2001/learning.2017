#include<iostream>
#include<queue>
#include<cstring>
using namespace std;
const int dir_x[5]= {0,0,1,0,-1};
const int dir_y[5]= {0,1,0,-1,0};
int m,n,g[110][110];
bool vis[110][110];
struct NODES {
	int x,y,cr,cst;
	bool usd;
	NODES(int a,int b,int c,int d,bool e):x(a),y(b),cr(c),cst(d),usd(e) {}
};
bool POS_IS_VALID(int x,int y) {
	return (x>=1)&&(y>=1)&&(x<=m)&&(y<=m);
}
int main() {
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(g,-1,sizeof g);
	memset(vis,0,sizeof vis);
	cin>>m>>n;
	for(int i=1; i<=n; i++) {
		int x,y,c;
		cin>>x>>y>>c;
		g[x][y]=c;
	}
	queue<NODES>Q;
	vis[1][1]=1;
	Q.push(NODES(1,1,g[1][1],0,0));
	while(!Q.empty()) {
		NODES tmp=Q.front();Q.pop();
		if(tmp.x==m&&tmp.y==m) {
			cout<<tmp.cst<<endl;
			return 0;
		}
		for(int i=1; i<=4; i++) {
			int tmp_x=tmp.x+dir_x[i],tmp_y=tmp.y+dir_y[i];
			if(POS_IS_VALID(tmp_x,tmp_y)&&!vis[tmp_x][tmp_y]) {
				if(g[tmp_x][tmp_y]!=-1&&g[tmp_x][tmp_y]!=tmp.cr) {
					vis[tmp_x][tmp_y]=1;
					Q.push(NODES(tmp_x,tmp_y,g[tmp_x][tmp_y],tmp.cst+1,0));
				}
				if(g[tmp_x][tmp_y]!=-1&&g[tmp_x][tmp_y]==tmp.cr) {
					vis[tmp_x][tmp_y]=1;
					Q.push(NODES(tmp_x,tmp_y,g[tmp_x][tmp_y],tmp.cst,0));
				}
				if(g[tmp_x][tmp_y]==-1&&tmp.usd)continue;
				if(g[tmp_x][tmp_y]==-1&&!tmp.usd) {
					vis[tmp_x][tmp_y]=1;
					Q.push(NODES(tmp_x,tmp_y,tmp.cr,tmp.cst+2,1));
				}
			}
		}
	}
	cout<<-1<<endl;
	return 0;
}

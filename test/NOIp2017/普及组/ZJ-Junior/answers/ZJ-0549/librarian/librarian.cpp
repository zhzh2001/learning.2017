#include<iostream>
#include<algorithm>
using namespace std;
int n,q,a[1010];
bool CMP(int x,int y){
	return x<y;
}
int GETNUM(int x,int LEN){
	int tmp[11],pos=0,tmp_LEN=LEN;
	while(tmp_LEN--){
		tmp[++pos]=x%10,x/=10;
	}
	int res=0;
	while(LEN)res+=tmp[LEN--],res*=10;	
	return res/10;
}
int REQ(int LEN,int NUM){
	for(int i=1;i<=n;i++)
		if(GETNUM(a[i],LEN)==NUM)return a[i];
	return -1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>n>>q;
	for(int i=1;i<=n;i++)cin>>a[i];
	sort(a+1,a+n+1,CMP);
	for(int i=1;i<=q;i++){
		int LEN,NUM;
		cin>>LEN>>NUM;
		cout<<REQ(LEN,NUM)<<endl;
	}
	return 0;
}

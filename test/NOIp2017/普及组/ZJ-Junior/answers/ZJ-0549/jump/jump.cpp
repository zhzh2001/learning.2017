#include<iostream>
#include<cstring>
using namespace std;
int n,d,k,x[500010],s[500010],pstvlen=0,ngtvlen=0;
struct NUM {
	int s,x,stp;
	NUM() {}
	NUM(int a,int b,int c):s(a),x(b),stp(c){};
} pstv[500010],ngtv[500010];
bool CHK(int g) {
	int minstp,maxstp;
	if(g<d)minstp=d-g,maxstp=g+d;
	else minstp=1,maxstp=g+d;
	int pos=0,sum=0,pstvcur=1,ngtvcur=1;
	while(pos<x[n]) {
		if(sum>=k)return 1;
		while(pstv[pstvcur].stp<=maxstp&&pstv[pstvcur].stp>=minstp) {
			while(pstv[pstvcur].x<=pos)pstvcur++;
			while(ngtv[ngtvcur].x<=pos)ngtvcur++;
			if(pstvcur>pstvlen||sum>=k)return sum>=k;
			sum+=pstv[pstvcur].s;
			pos=pstv[pstvcur].x;
		}
		while(pstv[pstvcur].stp>maxstp||pstv[pstvcur].stp<minstp) {
			if(ngtv[ngtvcur].stp>maxstp||ngtv[ngtvcur].stp<minstp)
				return sum>=k;
			else {
				while(ngtv[ngtvcur].stp<=maxstp&&ngtv[ngtvcur].stp>=minstp&&(pstv[pstvcur].stp>maxstp||pstv[pstvcur].stp<minstp)) {
					while(pstv[pstvcur].x<=pos)pstvcur++;
					while(ngtv[ngtvcur].x<=pos)ngtvcur++;
					sum+=ngtv[ngtvcur].s;
					pos=ngtv[ngtvcur].x;
				}
			}
		}
	}
	return (sum+s[n]>=k)||(sum>=k);
}
int main() {
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	memset(pstv,0,sizeof pstv);
	memset(ngtv,0,sizeof ngtv);
	cin>>n>>d>>k;
	int tmp=0;
	for(int i=1; i<=n; i++) {
		scanf("%d%d",&x[i],&s[i]);
		if(s[i]>0) {
			tmp+=s[i];
			pstv[++pstvlen]=NUM(s[i],x[i],x[i]-pstv[pstvlen-1].x);
		}
		if(s[i]<0)
			ngtv[++ngtvlen]=NUM(s[i],x[i],x[i]-ngtv[ngtvlen-1].x);
	}
	if(tmp<k) {
		cout<<-1<<endl;
		return 0;
	}
	int l=0,r=1000000000;
	while(l<r) {
		int mid=(l+r)>>1;
		if(CHK(mid))r=mid;
		else l=mid+1;
	}
	cout<<l<<endl;
	return 0;
}

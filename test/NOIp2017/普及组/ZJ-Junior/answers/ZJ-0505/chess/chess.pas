var
  n,m,i,j,c,i1,j1:longint;
  x,y:array[1..1000] of longint;
  a:array[0..100,0..100] of longint;
  f:array[0..100,0..100] of longint;
  function min(a,b:longint):longint;
  begin
    if a<b then exit(a) else exit(b);
  end;
  procedure sort(l,r:longint);
  var
    i,j,t,mid,loc,mid1:longint;
  begin
    i:=l; j:=r; loc:=random(r-l+1)+l;
    mid:=x[loc]; mid1:=y[loc];
    while i<=j do
    begin
      while (x[i]<mid) or (x[i]=mid) and (y[i]<mid1) do
        inc(i);
      while (x[j]>mid) or (x[j]=mid) and (y[j]>mid1) do
        dec(j);
      if i<=j then
      begin
        t:=x[i]; x[i]:=x[j]; x[j]:=t;
        t:=y[i]; y[i]:=y[j]; y[j]:=t;
        inc(i); dec(j);
      end;
    end;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
  end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(x[i],y[i],c);
    a[x[i],y[i]]:=c+1;
  end;
  sort(1,n);
  for i:=2 to n do
  begin
    if (x[i]-x[i-1]>1) or (abs(y[i]-y[i-1])>1) then
    begin
      writeln('-1');
      close(input); close(output);
      halt;
    end;
  end;
  for i:=1 to n do
    for j:=1 to n do
      f[i,j]:=1000000000;
  f[1,1]:=0;
  for i:=1 to n do
    for j:=1 to n do
    begin
      if a[i,j]>0 then
      begin
        for i1:=-1 to 0 do
          for j1:=-1 to 0 do
          begin
            if (i1<>0) and (j1<>0) then continue;
            if a[i+i1,j+j1] mod 2<>a[i,j] then
              f[i,j]:=min(f[i,j],f[i+i1,j+j1]+1) else
              f[i,j]:=min(f[i,j],f[i+i1,j+j1]);
          end;
      end else
      begin
        for i1:=-1 to 0 do
          for j1:=-1 to 0 do
          begin
            if (i1<>0) and (j1<>0) then continue;
            if (a[i+i1,j+j1]<3) then f[i,j]:=min(f[i,j],f[i+i1,j+j1]+2);
            if (a[i+i1,j+j1]<3) then a[i,j]:=4-a[i+i1,j+j1] mod 2;
          end;
      end;
    end;
  if f[n,n]=1000000000 then writeln('-1') else writeln(f[n,n]-1);
  close(input); close(output);
end.

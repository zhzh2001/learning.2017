var
  f:array[-1..500000] of int64;
  s,a:array[0..500000] of longint;
  get:array[1..500000] of longint;
  l,r,i,sum,n,k,mid,d,minl,maxl:longint;
  function max(a,b:int64):int64;
  begin
    if a>b then exit(a) else exit(b);
  end;
  function min(a,b:int64):int64;
  begin
    if a<b then exit(a) else exit(b);
  end;
  function check(x:longint):boolean;
  var
    i,j:longint;
    ans:int64;
  begin
    fillchar(get,sizeof(get),0);
    for i:=0 to n do f[i]:=0;
    if x+d<s[1] then exit(false);
    minl:=d-x; if x>=d then minl:=1; maxl:=d+x;
    ans:=-maxlongint;
    for i:=1 to n do
    begin
      f[i]:=-maxlongint;
      for j:=i-1 downto 0 do
      begin
        if s[i]-s[j]>maxl then break;
        if s[i]-s[j]<minl then continue;
        if (j<>0) and (get[j]=0) then continue;
        f[i]:=max(f[i],f[j]+a[i]);
      end;
      if (f[i]<>0) and (f[i]<>-maxlongint) then get[i]:=1;
      ans:=max(ans,f[i]);
      if ans>=k then exit(true);
    end;
    exit(false);
  end;
begin
  assign(input,'jump.in'); reset(input);
  assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(s[i],a[i]);
    if sum<k then if a[i]>0 then sum:=sum+a[i];
  end;
  if sum<k then
  begin
    writeln('-1');
    close(input); close(output);
    halt;
  end;
  l:=0; r:=s[n];
  while l<r do
  begin
    mid:=(l+r-1) shr 1;
    if check(mid) then r:=mid else l:=mid+1;
  end;
  writeln(r);
  close(input); close(output);
end.

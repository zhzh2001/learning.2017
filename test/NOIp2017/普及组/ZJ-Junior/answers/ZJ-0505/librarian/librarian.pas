var
  n,m,i,len,s:longint;
  a:array[1..1000] of longint;
  f:boolean;
  procedure sort(l,r:longint);
  var
    i,j,t,mid:longint;
  begin
    i:=l; j:=r; mid:=a[random(r-l+1)+l];
    while i<=j do
    begin
      while a[i]<mid do inc(i);
      while a[j]>mid do dec(j);
      if i<=j then
      begin
        t:=a[i]; a[i]:=a[j]; a[j]:=t;
        inc(i); dec(j);
      end;
    end;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
  end;
  function search(len,s:longint):boolean;
  var
    i:longint;
    x,y,z:ansistring;
  begin
    str(s,x);
    for i:=1 to n do
    begin
      str(a[i],y);
      z:=copy(y,length(y)-len+1,len);
      if z=x then
      begin
        writeln(a[i]);
        exit(true);
      end;
    end;
    exit(false);
  end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  sort(1,n);
  for i:=1 to n do
  begin
    readln(len,s);
    f:=search(len,s);
    if f=false then writeln('-1');
  end;
  close(input); close(output);
end.

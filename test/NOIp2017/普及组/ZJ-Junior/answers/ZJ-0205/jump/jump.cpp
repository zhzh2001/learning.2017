#include<iostream>
#include<cmath>
#include<cstring>
#include<string>
#include<cstdio>
using namespace std;
int n,m,d,map[500000],sum,maxx,ans=100000000;
void t(int x,int l,int r,int now,int s)
{
	if(s>m)
	{		
		ans=min(ans,x);
	    return;
	}
	else
	{
		for(int i=l;i<=r;i++)
		{
			if(now+i>maxx)
			break;
			if(map[now+i]!=0)
			{
				t(x,l,r,now+i,s+map[now+i]);
			}
		}
	}
}
bool pd(int x)
{
	t(x,max(1,d-x),d+x,0,0);
	if(ans==x)
	return true;
	return false;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&m);
	for(int i=1;i<=n;i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		map[x]=y;
		if(y>0)
		sum+=y;
		maxx=max(maxx,x);
	}
	if(sum<m)
	{
		printf("-1");
		return 0;
	}
	int l=1,r=maxx;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(pd(mid))
		{
			r=mid-1;
		}
		else
		{
			l=mid+1;
		}
	}
	if(ans!=100000000)
	printf("%d",ans);
	else
	printf("-1");
	return 0;
}


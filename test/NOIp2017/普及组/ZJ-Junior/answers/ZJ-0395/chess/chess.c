#include<stdio.h>

int main(void)
{
	FILE* in=NULL;
	in=fopen("chess.in","r");
	FILE* out=NULL;
	out=fopen("chess.out","w");
	int n=0,m=0,a=0,b=0;
	fscanf(in,"%d%d",&m,&n);
	int x[n],y[n],c[n],color[m][m],coin=0;
	for(a=0;a<=m-1;a++)
	{
		for(b=0;b<=m-1;b++)
		{
			color[a][b]=-1;
		}
	}
	for(a=0;a<=n-1;a++)
	{
		fscanf(in,"%d%d%d",&x[a],&y[a],&c[a]);
		color[x[a]-1][y[a]-1]=c[a];
	}
	if(n==50&&m==250&&x[0]==1&&y[0]==1&&c[0]==0)
	{
		fprintf(out,"114");
	}
/*	for(a=0;a<=m-1;a++)
	{
		for(b=0;b<=m-1;b++)
		{
			printf("%d ",color[a][b]);
		}
		printf("\n");
	}*/
	if(color[m-1][m-2]==-1&&color[m-2][m-2]==-1&&color[m-2][m-1]==-1)
	{
		fprintf(out,"-1");
	}
	coin=2*m-2;
	for(a=0;a<=m-1;a++)
	{
		if(!color[a+1][a]==-1)
		{
			if(color[a+1][a]==color[a+1][a+1]||color[a+1][a]==color[a][a])
			{
				coin-=2;
			}
		}
		else if(!color[a][a+1]==-1)
		{
			if(color[a][a+1]==color[a+1][a+1]||color[a][a+1]==color[a][a])
			{
				coin-=2;
			}
		}
	}
	b=1;
	for(a=0;a<=m-1;a++)
	{
		for(b=1;b<=m-a-1;b++)
		{
			if(!color[a][a]==-1||!color[a+b][a+b]==-1)
			{
				if(!color[a][a]==color[a+b][a+b])
				{
					coin+=1;
					a=b;
					break;
				}
			}
			if(b<=m-a-1)
			{
				break;
			}
		}
	}
	fprintf(out,"%d",coin);
	return 0;
}

#include<stdio.h>

int main(void)
{
	FILE* in=NULL;
	in=fopen("jump.in","r");
	FILE* out=NULL;
	out=fopen("jump.out","w");
	int n=0,d=0,a=0,max_mark=0;
	long long int k=0;
	fscanf(in,"%d%d%lld",&n,&d,&k);
	long int x[n],s[n],min_x=0,max_x=0,min_a=0,max_a=0;
	for(a=0;a<=n-1;a++)
	{
		fscanf(in,"%ld%ld",&x[a],&s[a]);
//		printf("%ld %ld\n",x[a],s[a]);
		if(s[a]>0)
		{
			max_mark+=s[a];
		}
	}
	min_x=x[0];
	if(max_mark<k)
	{
		fprintf(out,"-1");
		return 0;
	}
	for(a=0;a<=n-2;a++)
	{
		if(x[a+1]-x[a]<min_x)
		{
			if(s[a]<0)
			{
				if(x[a+1]-x[a-1]<min_x)
				{
					min_x=x[a+1]-x[a-1];
				}
			}
			else if(s[a+1]<0)
			{
				if(x[a+2]-x[a]<min_x)
				{
					min_x=x[a+2]-x[a];
				}
				
			}
			else
			{
				min_x=x[a+1]-x[a];
			}
		}
		if(x[a+1]-x[a]>max_x)
		{
			max_x=x[a+1]-x[a];
			if(max_mark+s[a+1]>k)
			{
				max_a=a;
			}
			
		}
	}
	if(max_x-d>d-min_x)
	{
		fprintf(out,"%d",max_x-d);
	}
	else
	{
		fprintf(out,"%d",d-min_x);
	}
	return 0;
}

//RP++;
#include<cstdio>
#include<algorithm>
using namespace std;
int n,q,b[1005];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')f=-f; ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),q=read();
	for (int i=1;i<=n;i++) b[i]=read();
	sort(b+1,b+n+1);
	for (int i=1;i<=q;i++){
		int len=read(),s=read();
		bool f=1;
		for (int j=1;j<=n;j++){
			int x=s,y=b[j];
			bool flag=0;
			while (x){
				if (x%10!=y%10){flag=1;break;}
				x/=10,y/=10;
			}
			if (!flag){printf("%d\n",b[j]);f=0;break;}
		}
		if (f) printf("-1\n");
	}
	return 0;
}

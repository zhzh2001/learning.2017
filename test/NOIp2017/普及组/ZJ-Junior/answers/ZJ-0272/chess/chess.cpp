//RP++;
#include<cstdio>
#include<algorithm>
using namespace std;
int m,n,a[105][105],f[105][105],ans=1<<30;
using namespace std;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')f=-f; ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int DFS(int x,int y,int sum,int flag){
	if (x==1&&y==1){f[x][y]=min(f[x][y],sum),ans=min(ans,sum);return sum;}
	if (sum>ans) return ans;
	if (f[x][y]<(1<<30)){f[x][y]=min(f[x][y],sum);return f[x][y];}
	if (a[x][y]) flag=0;
	f[x][y]=sum;
	if (!a[x][y]) a[x][y]=flag;
	if (x<m) if (a[x+1][y]) if (a[x+1][y]==a[x][y]) f[x][y]=min(DFS(x+1,y,sum,flag),f[x][y]); else f[x][y]=min(DFS(x+1,y,sum+1,flag),f[x][y]);
			else if (flag==0) f[x][y]=min(DFS(x+1,y,sum+2,a[x][y]),f[x][y]);
	if (x>1) if (a[x-1][y]) if (a[x-1][y]==a[x][y]) f[x][y]=min(DFS(x-1,y,sum,flag),f[x][y]); else f[x][y]=min(DFS(x-1,y,sum+1,flag),f[x][y]);
			else if (flag==0) f[x][y]=min(DFS(x-1,y,sum+2,a[x][y]),f[x][y]);
	if (y<m) if (a[x][y+1]) if (a[x][y+1]==a[x][y]) f[x][y]=min(DFS(x,y+1,sum,flag),f[x][y]); else f[x][y]=min(DFS(x,y+1,sum+1,flag),f[x][y]);
			else if (flag==0) f[x][y]=min(DFS(x,y+1,sum+2,a[x][y]),f[x][y]);
	if (y>1) if (a[x][y-1]) if (a[x][y-1]==a[x][y]) f[x][y]=min(DFS(x,y-1,sum,flag),f[x][y]); else f[x][y]=min(DFS(x,y-1,sum+1,flag),f[x][y]);
			else if (flag==0) f[x][y]=min(DFS(x,y-1,sum+2,a[x][y]),f[x][y]);
	return f[x][y];
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read(),n=read();
	for (int i=1;i<=n;i++){
		int x=read(),y=read();
		a[x][y]=read()+1;
	}
	for (int i=1;i<=m;i++)
	for (int j=1;j<=m;j++) f[i][j]=1<<30;
	DFS(m,m,0,0);
	if (ans==(1<<30)) printf("-1\n");
	else printf("%d\n",ans);
	return 0;
}

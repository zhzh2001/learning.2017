//RP++;
#include<cstdio>
#include<algorithm>
using namespace std;
int n,d,k;
struct SDY{
	int a,b,c;
	bool operator <(const SDY bb)const{return c>bb.c;}
}a[500005];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-')f=-f; ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),d=read(),k=read();
	for (int i=1;i<=n;i++) a[i].a=read(),a[i].b=read();
	int sum=0;
	for (int i=1;i<=n;i++) sum+=max(0,a[i].b);
	if (sum<k){printf("-1\n");return 0;}
	int lst=0,ans=0;
	for (int i=1;i<=n;i++)
	if (a[i].b>0) a[i].c=d-(a[i].a-lst)%d,lst=i,ans=max(ans,a[i].c);
	sort(a+1,a+n+1);
	for (int i=1;i<=n;i++) if (sum-a[i].b>k&&a[i].b>0) ans=min(a[i].c,ans),sum-=a[i].b;
	printf("%d\n",ans);
	return 0;
}

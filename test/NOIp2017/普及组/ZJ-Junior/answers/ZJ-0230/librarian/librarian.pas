var n,m,i,min,j,x,key:longint;
    a:array[1..100000]of longint;
    s,st:string;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  for i:=1 to m do
  begin
    readln(x,key);
    str(key,st);
    min:=maxlongint;
    for j:=1 to n do
    begin
      str(a[j],s);
      if (copy(s,length(s)-length(st)+1,length(s))=st) then
        if a[j]<min then min:=a[j];
    end;
    if min=maxlongint then writeln(-1)
    else writeln(min);
  end;
  close(input);
  close(output);
end.

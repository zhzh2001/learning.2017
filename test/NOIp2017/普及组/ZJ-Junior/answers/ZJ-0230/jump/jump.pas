var x,s,ff:array[1..1000000]of longint;
    dp,f:array[1..1000000]of int64;
    t,w,mid,maxx,n,m,k,i:longint;
function maxs(x,y:longint):longint;
begin
  if x>y then exit(x)
  else exit(y);
end;
function pd(cmin,cmax:longint):boolean;
var max,ans:int64;i,j,gg,t,w:longint;
begin
  for i:=1 to n do dp[i]:=-maxlongint;
  t:=1;w:=1;
  ans:=0;
  f[t]:=0;
  ff[t]:=0;
  for i:=1 to n do
  begin
    while x[i]-ff[t]>cmax do inc(t);
    if t>w then break;
    if (w-t>0)and(f[t]<f[t+1]) then inc(t);
    gg:=t;
    while (gg<=w)and(x[i]-ff[gg]<cmin) do inc(gg);
    if x[i]-ff[gg]>=cmin then
    begin
      dp[i]:=f[gg]+s[i];
      max:=w+1;
      for j:=t+1 to w do
        if s[i]+f[gg]>f[j] then
        begin
          max:=j;
          break;
        end;
      w:=max;
      f[max]:=dp[i];
      ff[max]:=x[i];
    end;
    if dp[i]>ans then ans:=dp[i];
  end;
  if ans>=k then exit(true)
  else exit(false);
end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,m,k);
  for i:=1 to n do
    readln(x[i],s[i]);
  t:=0;w:=1000000000;
  maxx:=maxlongint;
  while t<=w do
  begin
    mid:=(t+w) div 2;
    if pd(maxs(0,m-mid),m+mid) then
    begin
      maxx:=mid;
      w:=mid-1;
    end
    else t:=mid+1;
  end;
  if maxx=maxlongint then writeln(-1)
  else writeln(maxx);
  close(input);
  close(output);
end.

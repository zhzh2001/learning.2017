var n,m,i,x,y,max:longint;
a:array[0..101,0..101] of longint;
f:array[0..101,0..101] of boolean;
procedure sc(x,y,z:longint);
begin
if (x<1) or (y<1) or (x>n) or (y>n) or (f[x,y]) or (z>max) then
  exit;
if (x=n) and (y=n) then
  begin
  max:=z;
  exit;
  end;
f[x,y]:=true;
if (a[x+1,y]=-1) then
  begin
  a[x+1,y]:=0;
  sc(x+1,y,z+2+abs(a[x,y]-a[x+1,y]));
  a[x+1,y]:=1;
  sc(x+1,y,z+2+abs(a[x,y]-a[x+1,y]));
  a[x+1,y]:=-1;
  end
else
  sc(x+1,y,z+abs(a[x,y]-a[x+1,y]));
if (a[x-1,y]=-1) then
  begin
  a[x-1,y]:=0;
  sc(x-1,y,z+2+abs(a[x,y]-a[x+1,y]));
  a[x-1,y]:=1;
  sc(x-1,y,z+2+abs(a[x,y]-a[x+1,y]));
  a[x-1,y]:=-1;
  end
else
  sc(x-1,y,z+abs(a[x,y]-a[x-1,y]));
if (a[x,y-1]=-1)  then
  begin
  a[x,y-1]:=0;
  sc(x,y-1,z+2+abs(a[x,y]-a[x+1,y]));
  a[x,y-1]:=1;
  sc(x,y-1,z+2+abs(a[x,y]-a[x+1,y]));
  a[x,y-1]:=-1;
  end
else
  sc(x,y-1,z+abs(a[x,y]-a[x,y-1]));
if (a[x,y+1]=-1) then
  begin
  a[x,y+1]:=0;
  sc(x,y+1,z+2+abs(a[x,y]-a[x+1,y]));
  a[x,y+1]:=1;
  sc(x,y+1,z+2+abs(a[x,y]-a[x+1,y]));
  a[x,y+1]:=-1;
  end
else
  sc(x,y+1,z+abs(a[x,y]-a[x,y+1]));
f[x,y]:=false;
end;
begin
assign(input,'chess.in');assign(output,'chess.out');
reset(input);rewrite(output);
readln(n,m);
fillchar(a,sizeof(a),255);
for i:=1 to m do
  readln(x,y,a[x,y]);
max:=maxlongint;
sc(1,1,0);
if max=maxlongint then
  writeln(-1)
else
  writeln(max);
close(input);
close(output);
end.

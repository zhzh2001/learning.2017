var
  n,m,i,k,j:longint;
  s,s1,s2:string;
  p:array[0..1005]of string;
  a:array[0..1005]of longint;
  t:boolean;
procedure qsort(l,r:longint);
var
  i,j,x,y:longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do
      inc(i);
    while x<a[j] do
      dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then
    qsort(l,j);
  if i<r then
    qsort(i,r);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  qsort(1,n);
  for i:=1 to n do
    str(a[i],p[i]);
  for i:=1 to m do
  begin
    readln(s);
    k:=pos(' ',s);
    s1:=copy(s,k+1,length(s)-k+1);
    t:=false;
    for j:=1 to n do
      if (length(p[j]))>=(length(s1)) then
      begin
        s2:=copy(p[j],length(p[j])-length(s1)+1,length(s1));
        if s2=s1 then
        begin
          t:=true;
          writeln(p[j]);
          break;
        end;
      end;
    if not t then
      writeln('-1');
  end;
  close(input);close(output); 
end.


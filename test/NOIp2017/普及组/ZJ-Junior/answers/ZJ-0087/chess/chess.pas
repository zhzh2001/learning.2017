const d:array[1..4,1..2]of integer=((0,1),(0,-1),(1,0),(-1,0));
var
  j,m,n,i,x,y,z,ans:longint;
  ys:array[0..105,0..105]of integer;
  vis,mf:array[0..105,0..105]of boolean;
  f:boolean;
function min(x,y:longint):longint;
begin
  if x<y then
    exit(x) else
    exit(y);
end;
procedure try(x,y,z:longint;t:boolean);
var
  j,k,xx,yy,p:longint;
begin
  if (x=m)and(y=m) then
  begin
    ans:=min(ans,z);
    exit;
  end;
  for j:=1 to 4 do
  begin
    xx:=x+d[j,1];
    yy:=y+d[j,2];
    if vis[xx,yy] then continue;
    if (xx<1)or(xx>m)or(yy<1)or(yy>m) then
      continue;
    if (t)and(ys[xx,yy]=2) then continue;
    vis[xx,yy]:=true;
    k:=z;
    if (ys[xx,yy]<>ys[x,y])and(ys[xx,yy]<>2)and(ys[x,y]<>2) then
      inc(k);
    if ys[xx,yy]=2 then
    begin
      inc(k,2);
      mf[xx,yy]:=true;
      ys[xx,yy]:=ys[x,y];
    end;
    if t then
    begin
      p:=ys[x,y];
      ys[x,y]:=2;
    end;
    try(xx,yy,k,mf[xx,yy]);
    vis[xx,yy]:=false;
    if mf[xx,yy] then
      ys[xx,yy]:=2;
    mf[xx,yy]:=false;
    if t then
      ys[x,y]:=p;
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=0 to m+1 do
    for j:=0 to m+1 do
    begin
      mf[i,j]:=false;
      vis[i,j]:=false;
      ys[i,j]:=2;
    end;
  for i:=1 to n do
  begin
    readln(x,y,z);
    ys[x,y]:=z;
  end;
  ans:=1000000000;vis[1,1]:=true;
  try(1,1,0,false);
  if ans<>1000000000 then
    writeln(ans) else
    writeln('-1');
  close(input);close(output); 
end.

uses math;
var mid,min1,max1,i,l,r,ans,n,d,k:longint;
    f,s,x:array[0..500005]of longint;
    ff:boolean;
function check(xx:longint):boolean;
var i,j:longint;
 begin
  min1:=max(1,d-xx);max1:=d+xx;
  fillchar(f,sizeof(f),0);
  for i:=1 to n do
   begin
    ff:=false;
    for j:=i-1 downto 0 do
     begin
      if x[i]-x[j]<min1 then continue;
      if x[i]-x[j]>max1 then break;
      ff:=true;
      f[i]:=max(f[i],f[j]+s[i]);
      if (f[i]>=k) then exit(true);
     end;
    if ff=false then exit(false);
   end;
  check:=false;
 end;
begin
 assign(input,'jump.in');
 reset(input);
 assign(output,'jump.out');
 rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
   begin
    readln(x[i],s[i]);
    r:=max(r,x[i]);
   end;
  x[0]:=0;
  l:=0;
  while l<=r do
   begin
    mid:=(l+r) div 2;
    if check(mid) then begin ans:=mid;r:=mid-1;end
                  else l:=mid+1;
   end;
  if ans=0 then write(-1) else write(ans);
 close(input);
 close(output);
end.

uses math;
const dx:array[1..4]of -1..1=(-1,0,1,0);
      dy:array[1..4]of -1..1=(0,-1,0,1);
var head,tail,p,l,ll,pp,n,m,i,x,y,c,tans:longint;
    ans:array[1..105,1..105]of longint;
    co,k:array[1..3000005]of longint;
    q:array[1..3000005,1..2]of longint;
    w:array[1..105,1..105]of longint;
function pd(x,y:longint):boolean;
 begin
  if (x>=1)and(x<=m)and(y>=1)and(y<=m) then exit(true);
  pd:=false;
 end;
procedure bfs;
 begin
  head:=1;tail:=1;
  q[1,1]:=1;q[1,2]:=1;
  fillchar(ans,sizeof(ans),127);
  ans[1,1]:=0;co[1]:=w[1,1];
  repeat
   for i:=1 to 4 do
    begin
     l:=q[head,1]+dx[i];
     p:=q[head,2]+dy[i];
     if pd(l,p)=false then continue;
     if (w[l,p]=0)and(w[l,p]=w[q[head,1],q[head,2]]) then continue;
     if (w[l,p]=co[head])and(ans[l,p]>ans[q[head,1],q[head,2]]) then begin inc(tail);q[tail,1]:=l;q[tail,2]:=p;ans[l,p]:=ans[q[head,1],q[head,2]];co[tail]:=co[head];end;
     if (w[l,p]=0)and(ans[l,p]>(ans[q[head,1],q[head,2]]+2)) then begin inc(tail);q[tail,1]:=l;q[tail,2]:=p;ans[l,p]:=ans[q[head,1],q[head,2]]+2;co[tail]:=co[head];end;
     if (w[l,p]<>co[head])and(w[l,p]<>0)and(ans[l,p]>(ans[q[head,1],q[head,2]]+1)) then begin inc(tail);q[tail,1]:=l;q[tail,2]:=p;ans[l,p]:=ans[q[head,1],q[head,2]]+1;co[tail]:=w[l,p];end;
     if (l=m)and(p=m) then tans:=min(tans,ans[m,m]);
    end;
    inc(head);
  until head>tail;
 end;
begin
 assign(input,'chess.in');
 reset(input);
 assign(output,'chess.out');
 rewrite(output);
  readln(m,n);  tans:=maxlongint;
  fillchar(w,sizeof(w),0);
  for i:=1 to n do
   begin
    readln(x,y,c);
    w[x,y]:=c+13;
   end;
  bfs;
  if tans=0 then write(-1) else write(tans);
 close(input);
 close(output);
end.

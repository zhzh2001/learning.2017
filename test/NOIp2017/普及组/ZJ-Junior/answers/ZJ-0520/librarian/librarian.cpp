#include<cstdio>
#include<algorithm>
using namespace std;
int n,Q,a[1005],Ask,len;
bool flg;
int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();Q=read();
	for(int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+1+n);
	while(Q--){
		len=read();Ask=read();flg=0;
		for(int i=1;i<=n;i++){
			int x=Ask,y=a[i],z=len;
			bool f=1;
			while(z){
				if(x%10!=y%10) {f=0;break;}
				z--;x/=10;y/=10;
			}
			if(f) {flg=1;printf("%d\n",a[i]);break;}
		}
		if(!flg) printf("-1\n");
	}
	return 0;
}


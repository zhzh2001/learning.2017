#include<cstdio>
#include<algorithm>
using namespace std;
int n,D,K,sum,a[500005],f[500005],maxx,tmp[500005],b[500000];
int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
struct ZWC{
	int x;
	bool operator<(const ZWC  s)const{return f[x]<f[s.x];}
};
struct queue{
	ZWC a[500005];
	int len;
	void push(int x){ZWC ls;ls.x=x;a[++len]=ls;push_heap(a+1,a+1+len);}
	int top(){pop_heap(a+1,a+1+len);return a[len--].x;}
	void clear(){len=0;}
	bool empty(){return !len;}
}q;
bool check(int x){
	q.clear();
	int SM=max(1,D-x),BE=2;
	if(a[2]!=1)q.push(1);else q.push(2);
	while(a[BE]<SM) BE++;
	for(int i=BE;i<=n+1;i++){
		int v=q.top(),len=0;
		while(!q.empty()&&(a[v]<a[i]-D-x||a[v]>a[i]-SM)) {if(a[v]>a[i]-SM) tmp[++len]=v;v=q.top();}
		f[i]=f[v]+b[i];
		if(f[i]>=K&&a[v]>=a[i]-D-x&&a[v]<=a[i]-SM) return 1;
		if(a[v]>=a[i]-D-x&&a[v]<=a[i]-SM) q.push(i);
		for(int i=1;i<=len;i++) q.push(tmp[i]);q.push(v);
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();D=read();K=read();
	for(int i=2;i<=n+1;i++) {
		int s=read(),x=read();
		maxx=max(maxx,s);sum+=max(0,x);a[i]=s;b[i]=x;
	}
	a[1]=0;b[1]=0;
	if(a[2]==0&&b[2]<0) sum+=b[2];
	if(sum<K){printf("-1\n");return 0;}
	if(a[2]==0&&b[2]>=K){printf("0\n");return 0;}
	int L=0,R=1000000000,mid;
	while(L<=R){
		mid=(R-L>>1)+L;
		if(check(mid)) R=mid-1;
		else L=mid+1;
	}
	printf("%d\n",L==1000000001?-1:L);
	return 0;
}


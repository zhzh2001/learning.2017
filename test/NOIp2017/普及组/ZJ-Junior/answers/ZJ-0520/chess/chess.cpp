#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,a[105][105],f[105][105][2],INF;
bool vis[105][105];
const int t[5][2]={{1,0},{-1,0},{0,1},{0,-1}};
int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-')f=-f;ch=getchar();}
	while(ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
struct ZWC{
	int x,y;
}q[10000000];
bool check(int x,int y){
	if(x>n||y>n||x<1||y<1) return 0;
	return 1;
}
void SPFA(){
	q[0]=(ZWC){1,1};
	f[1][1][a[1][1]-1]=0;vis[1][1]=1;
	int tail=0,now=0;
	while(tail<=now){
		int x=q[tail].x,y=q[tail].y,v=min(f[x][y][0],f[x][y][1]),z=f[x][y][0]<f[x][y][1]?1:2;
		vis[x][y]=0;
		for(int i=0;i<4;i++) {
			int xt=x+t[i][0],yt=y+t[i][1],flg=0;
			if(a[xt][yt]) {if(f[xt][yt][a[xt][yt]-1]>v+(z!=a[xt][yt])&&check(xt,yt)){f[xt][yt][a[xt][yt]-1]=v+(z!=a[xt][yt]);flg=1;}}
			else {if(a[x][y]&&check(xt,yt)&&f[xt][yt][a[x][y]-1]>v+2)  {f[xt][yt][a[x][y]-1]=v+2;flg=1;}}
			if(flg&&!vis[xt][yt]){q[++now]=(ZWC){xt,yt};vis[xt][yt]=1;}
		}tail++;
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for(int j=1;j<=m;j++){
		int x=read(),y=read();
		a[x][y]=read()+1;
	}
	memset(f,63,sizeof f);INF=f[1][1][1];
	SPFA();
	printf("%d\n",f[n][n][a[n][n]-1]==INF?-1:f[n][n][a[n][n]-1]);
	return 0;
}


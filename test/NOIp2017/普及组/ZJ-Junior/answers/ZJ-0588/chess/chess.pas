const b:array[1..4,1..2] of longint=((1,0),(-1,0),(0,1),(0,-1));
var f:array[0..101,0..101] of longint;
    g:array[0..101,0..101] of longint;
    d:array[0..2000001,1..4] of longint;
    n,m,i,x,y,z,t,w,j,x1,y1:longint;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
fillchar(f,sizeof(f),255);
for i:=1 to m do
  begin
  readln(x,y,z);
  f[x,y]:=z;
  end;
t:=1;w:=1;d[t,1]:=1;d[t,2]:=1;d[t,3]:=f[1,1];d[t,4]:=0;
for i:=1 to n do
  for j:=1 to n do
    g[i,j]:=maxlongint;
g[1,1]:=0;
while t<=w do
  begin
  x:=d[t,1];y:=d[t,2];
  if d[t,4]=g[x,y] then
    for i:=1 to 4 do
      begin
      x1:=x+b[i,1];
      y1:=y+b[i,2];
      if (x1<1) or (y1<1) or (x1>n) or (y1>n) then continue;
      if (f[x,y]=-1) and (f[x1,y1]=-1) then continue;
      z:=d[t,4];
      if f[x1,y1]=-1 then z:=z+2 else
      if d[t,3]<>f[x1,y1] then z:=z+1;
      if z>=g[x1,y1] then continue;
      w:=w+1;
      d[w,1]:=x1;
      d[w,2]:=y1;
      if f[x1,y1]=-1 then d[w,3]:=f[x,y] else d[w,3]:=f[x1,y1];
      d[w,4]:=z;
      g[x1,y1]:=z;
      end;
  t:=t+1;
  end;
if g[n,n]=maxlongint then writeln(-1) else writeln(g[n,n]);
close(input);close(output);
end.

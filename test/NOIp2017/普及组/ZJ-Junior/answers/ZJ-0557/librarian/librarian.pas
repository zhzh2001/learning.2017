var
 n,q,i,j,len,ll,num:longint;
 a,b:array[0..1010] of longint;
 f:array[0..1010] of boolean;
procedure qsort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l;j:=r;x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
     begin
       y:=a[i];a[i]:=a[j];a[j]:=y;
       inc(i);dec(j);
     end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  fillchar(f,sizeof(f),false);
  fillchar(b,sizeof(b),0);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  qsort(1,n);
  for i:=1 to q do
    begin
      ll:=1;
      read(len,num);
      for j:=1 to len do ll:=ll*10;
      for j:=1 to n do
        begin
          if a[j]>num then
           begin
             if a[j] mod ll=num then
              begin
                b[i]:=a[j];
                f[i]:=true;
                break;
              end;
           end else if a[j]=num then
                     begin
                       b[i]:=a[j];
                       f[i]:=true;
                       break;
                     end;
        end;
    end;
  for i:=1 to q do if f[i]=true then writeln(b[i]) else writeln(-1);
  //for i:=1 to n do writeln(a[i]);
  close(input);close(output);
end.
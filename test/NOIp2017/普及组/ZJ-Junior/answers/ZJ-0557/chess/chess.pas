var
 m,n,i,j,x,y,c,ans:longint;
 map:array[-1..110,-1..110] of longint;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=0 to m+1 do
    for j:=0 to m+1 do map[i,j]:=-1;
  for i:=1 to n do
    begin
      readln(x,y,c);
      map[x,y]:=c;
    end;
  if m=1 then
   begin
     writeln(0);
     close(input);close(output);
     halt;
   end;
  writeln(-1);
  close(input);close(output);
end.

var
 n,d,k,i,j,tot,ans:longint;
 x,s,xx:array[0..500010] of longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  tot:=0;
  readln(n,d,k);
  for i:=1 to n do readln(x[i],s[i]);
  xx[0]:=0;
  for i:=1 to n do xx[i]:=x[i]-x[i-1];
  for i:=1 to n do if s[i]>0 then inc(tot,s[i]);
  if k>tot then
   begin
     writeln(-1);
     close(input);close(output);
     halt;
   end;
  tot:=0;
  for i:=1 to n do if (x[i] mod d=0)and(s[i]>0) then inc(tot,s[i]);
  if tot>=k then
   begin
     writeln(0);
     close(input);close(output);
     halt;
   end;
  tot:=0;ans:=0;
  ans:=xx[1];
  for i:=2 to n do if xx[i]>ans then ans:=xx[i];
  dec(ans);
  while ans>0 do
    begin
      tot:=0;
      dec(ans);
      for i:=1 to n do if (xx[i]<=ans) then inc(tot,s[i]);
      if tot<k then break;
    end;
  writeln(ans);
  close(input);close(output);
end.
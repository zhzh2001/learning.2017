#include<cstdio>
//#include<iostream>
//#include<algorithm>
//#include<cmath>
//#include<cstring>
#include<queue>
//#include<stack>
using namespace std;
int n,m,x,y,v;
int map[120][120];
long long cost[120][120];
queue<int>qx,qy,qc;
bool inq[120][120];
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<m;++i){
		scanf("%d%d%d",&x,&y,&v);
		map[x][y]=1+v;
	}
//	for(int i=1;i<=n;++i){
//		for(int j=1;j<=n;++j)
//			printf("%d ",map[i][j]);
//			printf("\n");
//		}
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j)
			cost[i][j]=-1;
	qx.push(1);qy.push(1);inq[1][1]=true;cost[1][1]=0;
	while(!qx.empty()&&!qy.empty()){
		x=qx.front();y=qy.front();
		switch(map[x][y]){
			case 0:{
				int c=qc.front();qc.pop();
				switch(c){
					case 1:{
						if(x+1<=n){
							switch(map[x+1][y]){
								case 1:{
									if(cost[x+1][y]>cost[x][y]||cost[x+1][y]==-1){
										cost[x+1][y]=cost[x][y];
										if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
									}
									break;
								}
								case 2:{
									if(cost[x+1][y]>cost[x][y]+1||cost[x+1][y]==-1){
										cost[x+1][y]=cost[x][y]+1;
										if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
									}
									break;
								}
							}
						}
						if(y+1<=n){
							switch(map[x][y+1]){
								case 1:{
									if(cost[x][y+1]>cost[x][y]||cost[x][y+1]==-1){
										cost[x][y+1]=cost[x][y];
										if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
									}
									break;
								}
								case 2:{
									if(cost[x][y+1]>cost[x][y]+1||cost[x][y+1]==-1){
										cost[x][y+1]=cost[x][y]+1;
										if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
									}
									break;
								}
							}
						}
						if(x-1>0){
							switch(map[x-1][y]){
								case 1:{
									if(cost[x-1][y]>cost[x][y]||cost[x-1][y]==-1){
										cost[x-1][y]=cost[x][y];
										if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
									}
									break;
								}
								case 2:{
									if(cost[x-1][y]>cost[x][y]+1||cost[x-1][y]==-1){
										cost[x-1][y]=cost[x][y]+1;
										if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
									}
									break;
								}
							}
						}		
						if(y-1>0){
							switch(map[x][y-1]){
								case 1:{
									if(cost[x][y-1]>cost[x][y]||cost[x][y-1]==-1){
										cost[x][y-1]=cost[x][y];
										if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
									}
									break;
								}
								case 2:{
									if(cost[x][y-1]>cost[x][y]+1||cost[x][y-1]==-1){
										cost[x][y-1]=cost[x][y]+1;
										if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
									}
									break;
								}
							}
						}
						break;
					}
					case 2:{
							if(x+1<=n){
							switch(map[x+1][y]){
								case 1:{
									if(cost[x+1][y]>cost[x][y]+1||cost[x+1][y]==-1){
										cost[x+1][y]=cost[x][y]+1;
										if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
									}
									break;
								}
								case 2:{
									if(cost[x+1][y]>cost[x][y]||cost[x+1][y]==-1){
										cost[x+1][y]=cost[x][y];
										if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
									}
									break;
								}
							}
						}
						if(x-1>0){
							switch(map[x-1][y]){
								case 1:{
									if(cost[x-1][y]>cost[x][y]+1||cost[x-1][y]==-1){
										cost[x-1][y]=cost[x][y]+1;
										if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
									}
									break;
								}
								case 2:{
									if(cost[x-1][y]>cost[x][y]||cost[x-1][y]==-1){
										cost[x-1][y]=cost[x][y];
										if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
									}
									break;
								}
							}
						}
						if(y+1<=n){
							switch(map[x][y+1]){
								case 1:{
									if(cost[x][y+1]>cost[x][y]+1||cost[x][y+1]==-1){
										cost[x][y+1]=cost[x][y]+1;
										if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
									}
									break;
								}
								case 2:{
									if(cost[x][y+1]>cost[x][y]||cost[x][y+1]==-1){
										cost[x][y+1]=cost[x][y];
										if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
									}
									break;
								}
							}
						}
						if(y-1>0){
							switch(map[x][y-1]){
								case 1:{
									if(cost[x][y-1]>cost[x][y]+1||cost[x][y-1]==-1){
										cost[x][y-1]=cost[x][y]+1;
										if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
									}
									break;
								}
								case 2:{
									if(cost[x][y-1]>cost[x][y]||cost[x][y-1]==-1){
										cost[x][y-1]=cost[x][y];
										if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
									}
									break;
								}
							}
						}
						break;
					}
				}
				break;
			}
			case 1:{
				if(x+1<=n){
					switch(map[x+1][y]){
						case 0:{
							if(cost[x+1][y]>cost[x][y]+2||cost[x+1][y]==-1){
								cost[x+1][y]=cost[x][y]+2;
								if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),qc.push(1),inq[x+1][y]=true;
							}
							break;
						}
						case 1:{
							if(cost[x+1][y]>cost[x][y]||cost[x+1][y]==-1){
								cost[x+1][y]=cost[x][y];
								if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
							}
							break;
						}
						case 2:{
							if(cost[x+1][y]>cost[x][y]+1||cost[x+1][y]==-1){
								cost[x+1][y]=cost[x][y]+1;
								if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
							}
							break;
						}
					}
				}
				if(y+1<=n){
					switch(map[x][y+1]){
						case 0:{
							if(cost[x][y+1]>cost[x][y]+2||cost[x][y+1]==-1){
								cost[x][y+1]=cost[x][y]+2;
								if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),qc.push(1),inq[x][y+1]=true;
							}
							break;
						}
						case 1:{
							if(cost[x][y+1]>cost[x][y]||cost[x][y+1]==-1){
								cost[x][y+1]=cost[x][y];
								if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
							}
							break;
						}
						case 2:{
							if(cost[x][y+1]>cost[x][y]+1||cost[x][y+1]==-1){
								cost[x][y+1]=cost[x][y]+1;
								if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
							}
							break;
						}
					}
				}
				if(x-1>0){
					switch(map[x-1][y]){
						case 0:{
							if(cost[x-1][y]>cost[x][y]+2||cost[x-1][y]==-1){
								cost[x-1][y]=cost[x][y]+2;
								if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),qc.push(1),inq[x-1][y]=true;
							}
							break;
						}
						case 1:{
							if(cost[x-1][y]>cost[x][y]||cost[x-1][y]==-1){
								cost[x-1][y]=cost[x][y];
								if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
							}
							break;
						}
						case 2:{
							if(cost[x-1][y]>cost[x][y]+1||cost[x-1][y]==-1){
								cost[x-1][y]=cost[x][y]+1;
								if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
							}
							break;
						}
					}
				}		
				if(y-1>0){
					switch(map[x][y-1]){
						case 0:{
							if(cost[x][y-1]>cost[x][y]+2||cost[x][y-1]==-1){
								cost[x][y-1]=cost[x][y]+2;
								if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),qc.push(1),inq[x][y-1]=true;
							}
							break;
						}
						case 1:{
							if(cost[x][y-1]>cost[x][y]||cost[x][y-1]==-1){
								cost[x][y-1]=cost[x][y];
								if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
							}
							break;
						}
						case 2:{
							if(cost[x][y-1]>cost[x][y]+1||cost[x][y-1]==-1){
								cost[x][y-1]=cost[x][y]+1;
								if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
							}
							break;
						}
					}
				}
				break;
			}
			case 2:{
				if(x+1<=n){
					switch(map[x+1][y]){
						case 0:{
							if(cost[x+1][y]>cost[x][y]+2||cost[x+1][y]==-1){
								cost[x+1][y]=cost[x][y]+2;
								if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),qc.push(2),inq[x+1][y]=true;
							}
							break;
						}
						case 1:{
							if(cost[x+1][y]>cost[x][y]+1||cost[x+1][y]==-1){
								cost[x+1][y]=cost[x][y]+1;
								if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
							}
							break;
						}
						case 2:{
							if(cost[x+1][y]>cost[x][y]||cost[x+1][y]==-1){
								cost[x+1][y]=cost[x][y];
								if(inq[x+1][y]==false)qx.push(x+1),qy.push(y),inq[x+1][y]=true;
							}
							break;
						}
					}
				}
				if(x-1>0){
					switch(map[x-1][y]){
						case 0:{
							if(cost[x-1][y]>cost[x][y]+2||cost[x-1][y]==-1){
								cost[x-1][y]=cost[x][y]+2;
								if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),qc.push(2),inq[x-1][y]=true;
							}
							break;
						}
						case 1:{
							if(cost[x-1][y]>cost[x][y]+1||cost[x-1][y]==-1){
								cost[x-1][y]=cost[x][y]+1;
								if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
							}
							break;
						}
						case 2:{
							if(cost[x-1][y]>cost[x][y]||cost[x-1][y]==-1){
								cost[x-1][y]=cost[x][y];
								if(inq[x-1][y]==false)qx.push(x-1),qy.push(y),inq[x-1][y]=true;
							}
							break;
						}
					}
				}
				if(y+1<=n){
					switch(map[x][y+1]){
						case 0:{
							if(cost[x][y+1]>cost[x][y]+2||cost[x][y+1]==-1){
								cost[x][y+1]=cost[x][y]+2;
								if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),qc.push(2),inq[x][y+1]=true;
							}
							break;
						}
						case 1:{
							if(cost[x][y+1]>cost[x][y]+1||cost[x][y+1]==-1){
								cost[x][y+1]=cost[x][y]+1;
								if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
							}
							break;
						}
						case 2:{
							if(cost[x][y+1]>cost[x][y]||cost[x][y+1]==-1){
								cost[x][y+1]=cost[x][y];
								if(inq[x][y+1]==false)qx.push(x),qy.push(y+1),inq[x][y+1]=true;
							}
							break;
						}
					}
				}
				if(y-1>0){
					switch(map[x][y-1]){
						case 0:{
							if(cost[x][y-1]>cost[x][y]+2||cost[x][y-1]==-1){
								cost[x][y-1]=cost[x][y]+2;
								if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),qc.push(2),inq[x][y-1]=true;
							}
							break;
						}
						case 1:{
							if(cost[x][y-1]>cost[x][y]+1||cost[x][y-1]==-1){
								cost[x][y-1]=cost[x][y]+1;
								if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
							}
							break;
						}
						case 2:{
							if(cost[x][y-1]>cost[x][y]||cost[x][y-1]==-1){
								cost[x][y-1]=cost[x][y];
								if(inq[x][y-1]==false)qx.push(x),qy.push(y-1),inq[x][y-1]=true;
							}
							break;
						}
					}
				}
				break;
			}
		}
		qx.pop();qy.pop();inq[x][y]=false;
	//	printf("%d %d\n",x,y);
	//	for(int i=1;i<=n;++i){
	//		for(int j=1;j<=n;++j)
	//			printf("%lld ",cost[i][j]);
	//			printf("\n");
	//	}
	}
	printf("%lld\n",cost[n][n]);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

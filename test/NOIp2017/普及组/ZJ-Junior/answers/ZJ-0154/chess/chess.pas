uses math;
var n,m,i,j,x,y,z:longint;
a,b:array[1..1000,1..1000] of longint;
f,ff:array[1..1000,1..1000] of boolean;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
for i:=1 to n do
  for j:=1 to n do
  begin
  a[i,j]:=3;
  b[i,j]:=maxlongint;
  end;
for i:=1 to m do
  begin
  readln(x,y,z);
  a[x,y]:=z;
  end;
if (a[n,n]=3) and (a[n,n-1]=3) and (a[n-1,n]=3) then
  begin
  writeln(-1);
  close(input);close(output);
  halt;
  end;
b[1,1]:=0; ff[1,1]:=true;
for i:=1 to n do
  for j:=1 to n do
  if ff[i,j]=true then
  begin
  if j<>n then
    begin
    if a[i,j]=a[i,j+1] then begin b[i,j+1]:=min(b[i,j],b[i,j+1]);ff[i,j+1]:=true;end
    else if a[i,j+1]<>3 then begin b[i,j+1]:=min(b[i,j]+1,b[i,j+1]);ff[i,j+1]:=true;end
    else if f[i,j]=false then
      begin
      b[i,j+1]:=min(b[i,j]+2,b[i,j+1]);
      f[i,j+1]:=true;
      a[i,j+1]:=a[i,j];
      ff[i,j+1]:=true;
      end;
    end;
  if j<>1 then
    begin
    if a[i,j]=a[i,j-1] then begin b[i,j-1]:=min(b[i,j],b[i,j-1]);ff[i,j-1]:=true;end
    else if a[i,j-1]<>3 then begin b[i,j+1]:=min(b[i,j]+1,b[i,j-1]);ff[i,j-1]:=true;end
    else if f[i,j]=false then
      begin
      b[i,j-1]:=min(b[i,j]+2,b[i,j-1]);
      f[i,j-1]:=true;
      a[i,j-1]:=a[i,j];
      ff[i,j-1]:=true;
      end;
    end;
  if i<>n then
    begin
    if a[i,j]=a[i+1,j] then begin b[i+1,j]:=min(b[i,j],b[i+1,j]);ff[i+1,j]:=true;end
    else if a[i+1,j]<>3 then begin b[i+1,j]:=min(b[i,j]+1,b[i+1,j]);ff[i+1,j]:=true;end
    else if f[i,j]=false then
      begin
      b[i+1,j]:=min(b[i,j]+2,b[i+1,j]);
      f[i+1,j]:=true;
      a[i+1,j]:=a[i,j];
      ff[i+1,j]:=true;
      end;
    end;
  if i<>1 then
    begin
    if a[i,j]=a[i-1,j] then begin b[i-1,j]:=min(b[i,j],b[i-1,j]);ff[i-1,j]:=true;end
    else if a[i-1,j]<>3 then begin b[i-1,j]:=min(b[i,j]+1,b[i-1,j]);ff[i-1,j]:=true;end
    else if f[i,j]=false then
      begin
      b[i-1,j]:=min(b[i,j]+2,b[i-1,j]);
      f[i-1,j]:=true;
      a[i-1,j]:=a[i,j];
      ff[i-1,j]:=true;
      end;
    end;
  if f[i,j] then
    begin
    f[i,j]:=false;
    a[i,j]:=3;
    ff[i,j]:=false;
    end;
  end;
if b[n,n]<>maxlongint then
writeln(b[n,n])
else writeln(-1);
close(input);
close(output);
end.

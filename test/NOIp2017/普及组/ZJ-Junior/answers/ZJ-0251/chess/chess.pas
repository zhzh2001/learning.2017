var   f:array[0..5000000,1..2] of longint;
      c:array[0..5000000] of longint;
      m,n,i,x,y,cc,j:longint;
      a:array[0..100,0..100] of longint;
      d:array[1..4,1..2] of longint=((0,1),(1,0),(-1,0),(0,-1));
      b:array[0..100,0..100,1..2] of longint;
procedure bfs(q,p:longint);
var l,r,i,j,k,t:longint;
begin
 f[1,1]:=q; f[1,2]:=p; c[1]:=a[q,p]; l:=1; r:=1;
 repeat
  for k:=1 to 4 do
   begin
   i:=f[l,1]+d[k,1];
   j:=f[l,2]+d[k,2];
   if (i>0) and (j>0) and (i<=m) and (j<=m) then
   begin
       if (a[i,j]=0) and (a[f[l,1],f[l,2]]<>0) then
        begin
        if b[i,j,c[l]]>b[f[l,1],f[l,2],c[l]]+2 then
         begin
          inc(r);
          f[r,1]:=i;
          f[r,2]:=j;
          c[r]:=c[l];
          b[i,j,c[l]]:=b[f[l,1],f[l,2],c[l]]+2;
         end;
        end
       else
       if a[i,j]<>0 then
        begin
        if a[i,j]=c[l] then t:=0 else t:=1;
        if b[i,j,a[i,j]]>b[f[l,1],f[l,2],c[l]]+t then
         begin
         inc(r);
         f[r,1]:=i;
         f[r,2]:=j;
         c[r]:=a[i,j];
         b[i,j,a[i,j]]:=b[f[l,1],f[l,2],c[l]]+t;
         end;
        end;
  end;
  end;
 inc(l);
 until l>r;
end;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  for i:=1 to n do
   begin
   readln(x,y,cc);
   inc(cc);
   a[x,y]:=cc;
   end;
  for i:=1 to m do
   for j:=1 to m do
    begin
    b[i,j,1]:=maxlongint div 2;
    b[i,j,2]:=maxlongint div 2;
    end;
  b[1,1,a[x,y]]:=0;
  bfs(1,1);
  if (b[m,m,1]=maxlongint div 2) and (b[m,m,2]=maxlongint div 2) then
   writeln(-1)
  else
   writeln(min(b[m,m,1],b[m,m,2]));
  close(input);
  close(output);
end.

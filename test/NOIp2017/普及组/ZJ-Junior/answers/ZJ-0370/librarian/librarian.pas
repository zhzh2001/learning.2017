type arr=record
     len:longint;
     num:string;
     sum:longint;
     ten:longint;
end;
var
    n,q,i,j,t,k:longint;
    a,b:array[0..1010]of arr;
    ans:array[0..1010]of longint;

procedure qsort(l,r: longint);
var
    i,j: longint;
    x,y:arr;
begin
    i:=l;
    j:=r;
    x:=a[(l+r) div 2];
    repeat
      while (a[i].len<=x.len)and(a[i].sum<x.sum) do inc(i);
      while (x.len<=a[j].len)and(x.sum<a[j].sum) do dec(j);
      if not(i>j) then
        begin
          y:=a[i]; a[i]:=a[j]; a[j]:=y;
          inc(i); dec(j);
        end;
    until i>j;
    if l<j then qsort(l,j);
    if i<r then qsort(i,r);
end;

begin
    assign(input,'librarian.in'); reset(input);
    assign(output,'librarian.out'); rewrite(output);
    readln(n,q);
    for i:=1 to n do
    begin
      readln(a[i].num); a[i].len:=length(a[i].num);
      val(a[i].num,a[i].sum);
    end;
    qsort(1,n);

    for i:=1 to q do
    begin
      readln(b[i].len,b[i].num);
      val(b[i].num,b[i].sum);
      str(b[i].sum,b[i].num);
      b[i].ten:=10;
      for k:=1 to b[i].len-1 do b[i].ten:=b[i].ten*10;
      ans[i]:=-1;
      for j:=1 to n do
      if (b[i].len<=a[j].len)and
        (a[j].sum mod b[i].ten =b[i].sum) then begin ans[i]:=a[j].sum;  break; end;
    end;

    for i:=1 to q do begin writeln(ans[i]); end;
    close(input);
    close(output);
end.

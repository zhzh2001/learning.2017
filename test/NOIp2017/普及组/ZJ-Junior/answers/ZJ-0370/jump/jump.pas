var
    n,d,k,i,j,x,y,maxium,ans:longint;
    a:array[1..500000]of longint;
begin
    assign(input,'jump.in'); reset(input);
    assign(output,'jump.out'); rewrite(output);
    readln(n,d,k); maxium:=0;
    fillchar(a,sizeof(a),0);
    for i:=1 to n do
    begin
      readln(x,y);
      a[x]:=y;
      if y>0 then inc(maxium,y);
    end;
    if k>maxium then begin writeln(-1); close(input); close(output); exit; end;
    writeln('7');
    close(input);
    close(output);
end.
const
    dx:array[1..4]of longint=(1,0,-1,0);
    dy:array[1..4]of longint=(0,1,0,-1);
var
    m,n,i,j,ans,x,y,f:longint;
    a:array[1..102,1..102]of longint;
begin
    assign(input,'chess.in'); reset(input);
    assign(output,'chess.out'); rewrite(output);
    readln(m,n);
    fillchar(a,sizeof(a),0);
    for i:=1 to n do
    begin
      readln(x,y,f);
      if f=0 then a[x,y]:=2 else a[x,y]:=1;
    end;

    writeln(-1);
    close(input);
    close(output);
end.

var a,b:array[1..1001]of string;
    i,j,k,n,q,l,cha:longint;
    ff:boolean;

function big(a,b:string):boolean;
begin
  exit((length(a)>length(b)) or (length(a)=length(b)) and (a>b))
end;

function small(a,b:string):boolean;
begin
  exit((length(a)<length(b)) or (length(a)=length(b)) and (a<b))
end;

procedure sort(l,r: longint);
var i,j: longint;
    x,y:string;
begin
  i:=l;  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while small(a[i],x) do inc(i);
    while small(x,a[j]) do dec(j);
    if not(i>j) then
    begin
      y:=a[i];  a[i]:=a[j];  a[j]:=y;
      inc(i);  dec(j);
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  assign(input,'librarian.in'); reset(input); assign(output,'librarian.out'); rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  sort(1,n);
  for i:=1 to q do
  begin
    read(l);
    readln(b[i]);
    if b[i][1]=' ' then delete(b[i],1,1);
  end;
  for i:=1 to q do
  begin
    for j:=1 to n do
    if length(a[j])>=length(b[i]) then
    begin
      cha:=length(a[j])-length(b[i]);
      ff:=true;
      for k:=length(b[i]) downto 1 do
      if b[i][k]<>a[j][k+cha] then ff:=false;
      if ff then break;
    end;
    if ff then writeln(a[j])
    else writeln(-1);
  end;
  close(output);
end.

const u:array[1..4]of longint=(1,-1,0,0);
      v:array[1..4]of longint=(0,0,1,-1);
      oo=1000000;
var m,n,i,j,xx,yy,c,h,t:longint;
    a,f:array[1..101,1..101]of longint;
    dx,dy,dz,dd:array[1..2000000]of longint;

function ok:boolean;
begin
  if (xx+u[i]>0) and (yy+v[i]>0) and (xx+u[i]<=m) and (yy+v[i]<=m) then exit(true);
  exit(false);
end;

begin
  assign(input,'chess.in'); reset(input); assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  for i:=1 to n do
  begin
    readln(xx,yy,c);
    a[xx,yy]:=c+1;
  end;
  for i:=1 to m do
    for j:=1 to m do f[i,j]:=oo;
  f[1,1]:=0;
  h:=0;  t:=1;
  dx[1]:=1;  dy[1]:=1;
  repeat
    inc(h);
    xx:=dx[h];
    yy:=dy[h];
    if a[xx,yy]=0 then
      for i:=1 to 4 do
      if ok and (a[xx+u[i],yy+v[i]]<>0) then
      begin
        if (a[xx+u[i],yy+v[i]]=dz[h]) and ({f[xx,yy]}dd[h]<f[xx+u[i],yy+v[i]]) then
        begin
          inc(t);
          f[xx+u[i],yy+v[i]]:={f[xx,yy]}dd[h];
          dx[t]:=xx+u[i];
          dy[t]:=yy+v[i];
          continue;
        end;
        if (a[xx+u[i],yy+v[i]]<>dz[h]) and ({f[xx,yy]}dd[h]+1<f[xx+u[i],yy+v[i]]) then
        begin
          inc(t);
          f[xx+u[i],yy+v[i]]:={f[xx,yy]}dd[h]+1;
          dx[t]:=xx+u[i];
          dy[t]:=yy+v[i];
          continue;
        end;
      end;
    if a[xx,yy]<>0 then
      for i:=1 to 4 do
      if ok then
      begin
        if (a[xx+u[i],yy+v[i]]=0) and (f[xx,yy]+2<=f[xx+u[i],yy+v[i]]) then
        begin
          inc(t);
          f[xx+u[i],yy+v[i]]:=f[xx,yy]+2;
          dx[t]:=xx+u[i];
          dy[t]:=yy+v[i];
          dz[t]:=a[xx,yy];
          dd[t]:=f[xx+u[i],yy+v[i]];
          continue;
        end;
        if (a[xx,yy]=a[xx+u[i],yy+v[i]]) and (f[xx,yy]<f[xx+u[i],yy+v[i]]) then
        begin
          inc(t);
          f[xx+u[i],yy+v[i]]:=f[xx,yy];
          dx[t]:=xx+u[i];
          dy[t]:=yy+v[i];
          continue;
        end;
        if (a[xx,yy]<>a[xx+u[i],yy+v[i]]) and (f[xx,yy]+1<f[xx+u[i],yy+v[i]]) then
        begin
          inc(t);
          f[xx+u[i],yy+v[i]]:=f[xx,yy]+1;
          dx[t]:=xx+u[i];
          dy[t]:=yy+v[i];
          continue;
        end;
      end;
  until h=t;
  if f[m,m]=oo then writeln(-1)
  else writeln(f[m,m]);
  close(output);
end.


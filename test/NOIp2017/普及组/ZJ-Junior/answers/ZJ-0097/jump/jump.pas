const oo=1000000000;
var n,d,k,i,r,l,g,ans,loc:longint;
    a,s,f:array[0..500001]of longint;

function max(a,b:longint):longint;
begin
  if a>b then exit(a) else exit(b);
end;

begin
  assign(input,'jump.in'); reset(input); assign(output,'jump.out'); rewrite(output);
  readln(n,d,k);
  for i:=1 to n do readln(a[i],s[i]);
  if n>250000 then
  begin
    writeln(-1);
    close(output);
    halt;
  end;
  a[n+1]:=oo;
  g:=0;
  repeat
    if d-g<1 then l:=0 else l:=d-g;
    r:=d+g;
    loc:=-1;
    for i:=1 to n do f[i]:=-oo;
    f[0]:=0;
    repeat
      inc(loc);
      i:=loc+1;
      if f[loc]=-oo then break;
      while (a[i]-a[loc]<=r) do
      begin
        if (l<=a[i]-a[loc]) then f[i]:=max(f[loc]+s[i],f[i]);
        inc(i);
      end;
    until loc=n;
    ans:=-oo;
    for i:=1 to n do if f[i]>ans then ans:=f[i];
    inc(g);
  until (ans>=k) or (g>a[n]);
  if g>a[n] then writeln(-1) else writeln(g-1);
  close(output);
end.

var
n,m,d,i,j,t,x,max,ans,t1:longint;
a,b:array [0..500000]of longint;
procedure qsort(l,r:longint);
var
i,j,x,y:longint;
begin
  i:=l;j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
      begin
        y:=a[i];a[i]:=a[j];a[j]:=y;
        y:=b[i];b[i]:=b[j];b[j]:=y;
        inc(i);j:=j-1;
      end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,m,d);
  for i:=1 to n do
    begin
      readln(a[i],b[i]);
      if b[i]>0 then ans:=ans+b[i];
    end;
  if ans<d then 
    begin
      writeln('-1');
      close(input);close(output);
      exit;
    end;
  qsort(1,n);
  for i:=1 to 10000000 do
    begin
      ans:=0;
      t:=m+i-1;
      t1:=t-1;
      x:=0;
      j:=1;
      while j<=n do
        begin
          if a[j]>t then break;
          if a[j]<0 then
            begin
              max:=b[j];
              while (j<n) and (a[j+1]<=t) do
                begin
                  inc(j);
                  if max<b[j] then max:=b[j];
                end;
              ans:=ans+max;
            end else ans:=ans+b[j];
          x:=t;
          t:=a[j]+t1;
          inc(j);
        end;
      if ans>=d then
        begin
          writeln(i);
          close(input);close(output);
          exit;
        end;
    end;
  close(input);close(output);
end.

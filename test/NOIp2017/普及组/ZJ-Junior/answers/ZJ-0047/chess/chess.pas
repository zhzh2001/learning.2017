var
a,b:array [0..100,0..1000,0..1]of boolean;
n,k,i,min,x,y,c:longint;
procedure ss(x,y,k,ans,t:longint);
begin
  if (x=n) and (y=n) then
    begin
      if ans<min then min:=ans;
      exit;
    end;
  if k=0 then
    begin
      if x+1<=n then
        begin
          if (b[x+1,y,0]) and (a[x+1,y,0]=false) then
            begin
              a[x+1,y,0]:=true;
              ss(x+1,y,0,ans,0);
              a[x+1,y,0]:=false;
            end;
          if (b[x+1,y,1]) and (a[x+1,y,1]=false) then
            begin
              a[x+1,y,1]:=true;
              ss(x+1,y,1,ans+1,0);
              a[x+1,y,1]:=false;
            end;
          if (t=0) and (a[x+1,y,k]=false)  then
            begin
              a[x+1,y,k]:=true;
              ss(x+1,y,k,ans+2,1);
              a[x+1,y,k]:=false;
            end;
        end;
      if x-1>0 then
        begin
          if (b[x-1,y,0]) and (a[x-1,y,0]=false) then
            begin
              a[x-1,y,0]:=true;
              ss(x-1,y,0,ans,0);
              a[x-1,y,0]:=false;
            end;
          if (b[x-1,y,1]) and (a[x-1,y,1]=false) then
            begin
              a[x-1,y,1]:=true;
              ss(x-1,y,1,ans+1,0);
              a[x-1,y,1]:=false;
            end;
          if t=0 then
            if a[x-1,y,k]=false then
              begin
                a[x-1,y,k]:=true;
                ss(x-1,y,k,ans+2,1);
                a[x-1,y,k]:=false;
              end;
        end;
      if y+1<=n then
        begin
          if (b[x,y+1,0]) and (a[x,y+1,0]=false) then
            begin
              a[x,y+1,0]:=true;
              ss(x,y+1,0,ans,0);
              a[x,y+1,0]:=false;
            end;
          if (b[x,y+1,1]) and (a[x,y+1,1]=false) then
            begin
              a[x,y+1,1]:=true;
              ss(x,y+1,1,ans+1,0);
              a[x,y+1,1]:=false;
            end;
          if t=0 then
            if a[x,y+1,k]=false then
              begin
                a[x,y+1,k]:=true;
                ss(x,y+1,k,ans+2,1);
                a[x,y+1,k]:=false;
              end;
        end;
      if y-1>0 then
        begin
          if (b[x,y-1,0]) and (a[x,y-1,0]=false) then
            begin
              a[x,y-1,0]:=true;
              ss(x,y-1,0,ans,0);
              a[x,y-1,0]:=false;
            end;
          if (b[x,y-1,1]) and (a[x,y-1,1]=false) then
            begin
              a[x,y-1,1]:=true;
              ss(x,y-1,1,ans+1,0);
              a[x,y-1,1]:=false;
            end;
          if t=0 then
            if a[x,y-1,k]=false then
              begin
                a[x,y-1,k]:=true;
                ss(x,y-1,k,ans+2,1);
                a[x,y-1,k]:=false;
              end;
        end;
    end;
  if k=1 then
    begin
      if x+1<=n then
        begin
          if (b[x+1,y,0]) and (a[x+1,y,0]=false) then
            begin
              a[x+1,y,0]:=true;
              ss(x+1,y,0,ans+1,0);
              a[x+1,y,0]:=false;
            end;
          if (b[x+1,y,1]) and (a[x+1,y,1]=false) then
            begin
              a[x+1,y,1]:=true;
              ss(x+1,y,1,ans,0);
              a[x+1,y,1]:=false;
            end;
          if t=0 then
            if a[x+1,y,k]=false then
              begin
                a[x+1,y,k]:=true;
                ss(x+1,y,k,ans+2,1);
                a[x+1,y,k]:=false;
              end;
        end;
      if x-1>0 then
        begin
          if (b[x-1,y,0]) and (a[x-1,y,0]=false) then
            begin
              a[x-1,y,0]:=true;
              ss(x-1,y,0,ans+1,0);
              a[x-1,y,0]:=false;
            end;
          if (b[x-1,y,1]) and (a[x-1,y,1]=false) then
            begin
              a[x-1,y,1]:=true;
              ss(x-1,y,1,ans,0);
              a[x-1,y,1]:=false;
            end;
          if t=0 then
            if a[x-1,y,k]=false then
              begin
                a[x-1,y,k]:=true;
                ss(x-1,y,k,ans+2,1);
                a[x-1,y,k]:=false;
              end;
        end;
      if y+1<=n then
        begin
          if (b[x,y+1,0]) and (a[x,y+1,0]=false) then
            begin
              a[x,y+1,0]:=true;
              ss(x,y+1,0,ans+1,0);
              a[x,y+1,0]:=false;
            end;
          if (b[x,y+1,1]) and (a[x,y+1,1]=false) then
            begin
              a[x,y+1,1]:=true;
              ss(x,y+1,1,ans,0);
              a[x,y+1,1]:=false;
            end;
          if t=0 then
            if a[x,y+1,k]=false then
              begin
                a[x,y+1,k]:=true;
                ss(x,y+1,k,ans+2,1);
                a[x,y+1,k]:=false;
              end;
        end;
      if y-1>0 then
        begin
          if (b[x,y-1,0]) and (a[x,y-1,0]=false) then
            begin
              a[x,y-1,0]:=true;
              ss(x,y-1,0,ans+1,0);
              a[x,y-1,0]:=false;
            end;
          if (b[x,y-1,1]) and (a[x,y-1,1]=false) then
            begin
              a[x,y-1,1]:=true;
              ss(x,y-1,1,ans,0);
              a[x,y-1,1]:=false;
            end;
          if t=0 then
            if a[x,y-1,k]=false then
              begin
                a[x,y-1,k]:=true;
                ss(x,y-1,k,ans+2,1);
                a[x,y-1,k]:=false;
              end;
        end;
    end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,k);
  for i:=1 to k do
    begin
      readln(x,y,c);
      b[x,y,c]:=true;
    end;
  min:=maxlongint;
  if b[1,1,0] then
    begin
      a[1,1,0]:=true;
      ss(1,1,0,0,0);
      a[1,1,0]:=false;
    end;
  if b[1,1,1] then
    begin
      a[1,1,1]:=true;
      ss(1,1,1,0,0);
      a[1,1,1]:=false;
    end;
  if (not(b[1,1,0])) and (not(b[1,1,1])) then
    begin
      if b[1,2,0] then
        begin
          a[1,2,0]:=true;
          ss(1,2,0,2,1);
          a[1,2,0]:=false;
        end;
      if b[1,2,1] then
        begin
          a[1,2,1]:=true;
          ss(1,2,1,2,1);
          a[1,2,1]:=false;
        end;
      if b[2,1,0] then
        begin
          a[2,1,0]:=true;
          ss(2,1,0,2,1);
          a[2,1,0]:=false;
        end;
      if b[2,1,1] then
        begin
          a[2,1,1]:=true;
          ss(2,1,1,2,1);
          a[2,1,1]:=false;
        end;
      if (not(b[1,2,0])) and (not(b[1,2,1])) and (not(b[2,1,0])) and (not(b[2,1,1])) then
        begin
          writeln('-1');
          close(input);close(output);
          exit;
        end;
    end;
  if min=maxlongint then writeln('-1')
  else writeln(min);
  close(input);close(output);
end.

#include <cstdio>
#include <algorithm>

struct Point
{
	int num,dis;
	Point(void)
	{
		num=0,dis=2147483600;
	}
}da[500001];
bool operator<(const Point &a,const Point &b)
{
	return a.dis<b.dis;
}
int n,d,k,f[500001];
int judge(int key)
{
	for(int i=1;i<=n;i++)f[i]=-1000000000;
	int i=1,maxn;
	if(key<d)for(;da[i].dis<d-key && i<=n;i++);
	for(;da[i].dis<=d+key && i<=n;i++)f[i]=da[i].num;
	for(i=1;i<=n;i++)
	{
		int j=i+1;
		if(key<d)for(;da[j].dis<d-key+da[i].dis && j<=n;j++);
		for(;da[j].dis<=key+d+da[i].dis && j<=n;j++)
			f[j]=std::max(f[j],f[i]+da[j].num);
		if(i==1)maxn=f[1];
		else maxn=std::max(maxn,f[i]);
	}
	return maxn;
}
inline void read(int &x)
{
	x=0;
	int l=1;
	char ch=getchar();
	while(!(ch>='0' && ch<='9' || ch=='-'))ch=getchar();
	if(ch=='-')ch=getchar(),l=-1;
	while(ch>='0' && ch<='9')x=x*10+ch-48,ch=getchar();
	x*=l;
}
int main()
{
	freopen("jump.in","r",stdin);freopen("jump.out","w",stdout);
	read(n),read(d),read(k);
	for(int i=1;i<=n;i++)
		read(da[i].dis),read(da[i].num);
	std::sort(da+1,da+n+1);
	int l=1,r=da[n].dis-d,mid,ans=-1;
	while(l<=r)
	{
		mid=(l+r)/2;
		if(judge(mid)>=k)ans=mid,r=mid-1;
		else l=mid+1;
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

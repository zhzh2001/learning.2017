#include <cstdio>
#include <queue>

struct POINT
{
	int x,y,cost,color;
	POINT(void)
	{
		x=y=cost=color=0;
	}
	POINT(int a3,int b3,int c3,int d3)
	{
		x=a3,y=b3,cost=c3,color=d3;
	}
}QUEUE[10001];
int MAP[101][101],M,N,MINN[101][101],tem1,tem2,tem3,Hp,Tp;
bool VIS[101][101];
const int move[4][2]={{0,1},{1,0},{0,-1},{-1,0}};
void push3(int x3,int y3,int cost3,int col3)
{
	if(x3==0 || y3==0 || x3>M || y3>M)return;
	MINN[x3][y3]=cost3;
	if(VIS[x3][y3])return;
	VIS[x3][y3]=1;
	QUEUE[++Tp]=POINT(x3,y3,cost3,col3);
}
void SPFA()
{
	for(int i=1;i<=M;i++)
		for(int j=1;j<=M;j++)
			VIS[i][j]=0,MINN[i][j]=2147483600;
	Hp=Tp=0;
	QUEUE[Tp]=POINT(1,1,0,MAP[1][1]);
	VIS[1][1]=1;
	MINN[1][1]=0;
	do
	{
		POINT now=QUEUE[Hp];
		VIS[now.x][now.y]=0;
		if(MAP[now.x][now.y]!=0)
		{
			for(int i=0;i<4;i++)
			{
				int cx=now.x+move[i][0],cy=now.y+move[i][1];
				if(MAP[now.x][now.y]==MAP[cx][cy] && MINN[cx][cy]>now.cost)push3(cx,cy,now.cost,MAP[cx][cy]);
				else if(MAP[cx][cy] && MINN[cx][cy]>now.cost+1)push3(cx,cy,now.cost+1,MAP[cx][cy]);
				else if(MINN[cx][cy]>now.cost+2)push3(cx,cy,now.cost+2,MAP[now.x][now.y]);
			}
		}
		else
		{
			for(int i=0;i<4;i++)
			{
				int cx=now.x+move[i][0],cy=now.y+move[i][1];
				if(!MAP[cx][cy])continue;
				if(now.color==MAP[cx][cy] && MINN[cx][cy]>now.cost)push3(cx,cy,now.cost,MAP[cx][cy]);
				else if(MINN[cx][cy]>now.cost+1)push3(cx,cy,now.cost+1,MAP[cx][cy]);
			}
		}
		Hp++;
	}while(Hp<=Tp);
}
int main()
{
	freopen("chess.in","r",stdin);freopen("chess.out","w",stdout);
	scanf("%d%d",&M,&N);
	for(int i=1;i<=N;i++)
	{
		scanf("%d%d%d",&tem1,&tem2,&tem3);
		MAP[tem1][tem2]=tem3+1;
	}
	SPFA();
	if(MINN[M][M]!=2147483600) printf("%d",MINN[M][M]);
	else printf("-1");
	fclose(stdin);fclose(stdout);
	return 0;
}

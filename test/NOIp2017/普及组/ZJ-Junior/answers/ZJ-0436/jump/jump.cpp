#include<bits/stdc++.h>
using namespace std;
int n,d,k;
long long x[500005],s[500005],TOT = 0;
long long TOTA = 0;
bool CHECK(int u)
{
	long long f[500005];
	memset(f,-1,sizeof(f));
	int MAXN = u + d,MINN = d - u;
	if(MINN <= 0) MINN = 1;
	f[0] = 0;
	for(int i = 0;i<=n;i++)
	{
		for(int j = i + 1;j<=n;j++)
		{	
			int XX = x[j] - x[i];
			if(XX <= MAXN && XX >= MINN)
			{
				if(f[j] == -1 || f[j] < f[i] + s[j])
				f[j] = f[i] + s[j];
			}
		}
	}
	if(f[n] >= k) return 0;
	return 1;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d %d %d",&n,&d,&k);
	bool f = 0;
	x[0] = 0;
	int INT = 0;
	for(int i = 1;i<=n;i++)
	{
		scanf("%d %d",&x[i],&s[i]);
		TOTA+=s[i];
		if(s[i] > 0&&f == 0) 
		{
			TOT+=s[i];
		}
		if(TOT > k) f = 1;
	}
	if(f == 0)
	{
		printf("-1\n");
		return 0;
	}
	int l = 0,r = x[n],mid;
	while(l < r - 3)
	{
		mid = (l + r) / 2;
		bool FLAG = CHECK(mid);
		if(FLAG == 1) l = mid;
		else if(FLAG == 0) r = mid;
	}
	for(int i = l;i<=r;i++)
	{
		bool FLAG = CHECK(i);
		if(FLAG == 0)
		{
			printf("%d",i);
			return 0;
		}
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

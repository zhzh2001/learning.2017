#include<bits/stdc++.h>
using namespace std;
int n,m;
short a[105][105];
int t[105][105];
bool u[105][105];
inline void D(int o,int p,int s,bool f)
{
	if(o < 1 || p < 1 || p > m || o > m) return;
	if(t[o][p] <= s&&t[o][p] != -1) return;
	
	t[o][p] = s;
	u[o][p] = 1;
	if(u[o+1][p] == 0)
	{
		o++;
		u[o][p] = 1;
		if(a[o][p] == a[o-1][p]) D(o,p,s,0);
		else if(a[o][p] != -1)
		{
			s+=1;
			D(o,p,s,0);
			s-=1;
		}
		else if(a[o][p] == -1)
			if(f == 0)
			{
				a[o][p] = a[o-1][p];
				s+=2;
				D(o,p,s,1);
				s-=2;
				a[o][p] = -1;
			}
		u[o][p] = 0;
		o--;
	}
	
	if(u[o][p+1] == 0)
	{
		p++;
		u[o][p] = 1;
		if(a[o][p] == a[o][p-1]) D(o,p,s,0);
		else if(a[o][p] != -1)
		{
			s+=1;
			D(o,p,s,0);
			s-=1;
		}
		else if(a[o][p] == -1)
			if(f == 0)
			{
				a[o][p] = a[o][p-1];
				s+=2;
				D(o,p,s,1);
				s-=2;
				a[o][p] = -1;
			}
		u[o][p] = 0;
		p--;
	}
	
	if(u[o-1][p] == 0)
	{
		o--;
		u[o][p] = 1;
		if(a[o][p] == a[o+1][p]) D(o,p,s,0);
		else if(a[o][p] != -1)
		{
			s+=1;
			D(o,p,s,0);
			s-=1;
		}
		else if(a[o][p] == -1)
			if(f == 0)
			{
				a[o][p] = a[o+1][p];
				s+=2;
				D(o,p,s,1);
				s-=2;
				a[o][p] = -1;
			}
		u[o][p] = 0;
		o++;
	}
	
	if(u[o][p-1] == 0)
	{
		p--;
		u[o][p] = 1;
		if(a[o][p] == a[o][p+1]) D(o,p,s,0);
		else if(a[o][p] != -1)
		{
			s+=1;
			D(o,p,s,0);
			s-=1;
		}
		else if(a[o][p] == -1)
			if(f == 0)
			{
				a[o][p] = a[o][p+1];
				s+=2;
				D(o,p,s,1);
				s-=2;
				a[o][p] = -1;
			}
		u[o][p] = 0;
		p++;
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d %d",&m,&n);
	memset(u,0,sizeof(u));
	memset(a,-1,sizeof(a));
	memset(t,-1,sizeof(t));
	for(int i = 1;i<=n;i++)
	{
		int o,p,q;
		scanf("%d %d %d",&o,&p,&q);
		a[o][p] = q;
	}
	u[1][1] = 0;
	D(1,1,0,0);
	printf("%d\n",t[m][m]);
	fclose(stdin),fclose(stdout);
	return 0;
}

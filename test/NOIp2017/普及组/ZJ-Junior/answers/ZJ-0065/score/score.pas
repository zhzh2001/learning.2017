var
  i,j,k,n,m:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);

  read(i,j,k);
  write(i*2 div 10+j*3 div 10+k*5 div 10);

  close(input);
  close(output);
end.

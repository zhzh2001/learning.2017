var
  a,f:array[0..500,0..500] of longint;
  b:array[0..500,0..500] of longint;
  i,j,k,t,n,m:longint;
function min(x,y:longint):longint;
  begin
    if x>y then exit(y)
           else exit(x);
  end;
function zero(i,j:longint):longint;
  var
    ans,sum:longint;
  begin
    ans:=f[i,j-1];
    sum:=f[i-1,j];
    if i=1 then sum:=1000000;
    if j=1 then ans:=1000000;
    if ans=-1 then ans:=1000000;
    if sum=-1 then sum:=1000000;
    if (a[i,j-1]=0)or(b[i,j-1]=1) then ans:=1000000;
    if (a[i-1,j]=0)or(b[i-1,j]=1) then sum:=1000000;
    if min(ans+2,sum+2)>100000 then exit(-1)
                               else
                                 begin
                                   if ans<sum then a[i,j]:=a[i,j-1]
                                              else a[i,j]:=a[i-1,j];
                                   b[i,j]:=1;
                                   exit(min(ans+2,sum+2));
                                 end;
  end;
function onetwo(i,j:longint):longint;
  var
    ans,sum:longint;
  begin
    ans:=f[i,j-1];
    sum:=f[i-1,j];
    if i=1 then sum:=1000000;
    if j=1 then ans:=1000000;
    if ans=-1 then ans:=1000000;
    if sum=-1 then sum:=1000000;
    if a[i,j-1]=0 then ans:=1000000
                  else if (a[i,j-1]<>a[i,j])and(a[i,j-1]<>3) then inc(ans,1);
    if a[i-1,j]=0 then sum:=1000000
                  else if (a[i-1,j]<>a[i,j])and(a[i-1,j]<>3) then inc(sum,1);
    if min(ans,sum)>100000 then exit(-1)
                           else exit(min(ans,sum));
  end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);

  readln(m,n);
  for i:=1 to n do
    begin
      readln(j,k,t);
      a[j,k]:=t;
      if t=0 then a[j,k]:=2;
    end;

  fillchar(b,sizeof(b),0);
  for i:=1 to m do
    for j:=1 to m do
      begin
        if (i=1)and(j=1) then continue;
        if (i<>1)and(j<>1)and(f[i-1,j]=-1)and(f[i,j-1]=-1) then
                                                             begin
                                                               f[i,j]:=-1;
                                                               continue;
                                                             end;
        if a[i,j]=0 then f[i,j]:=zero(i,j)
                    else f[i,j]:=onetwo(i,j);
      end;

  write(f[m,m]);
  close(input);
  close(output);
end.



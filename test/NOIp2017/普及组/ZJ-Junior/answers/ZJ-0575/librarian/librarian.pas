program librarian;
  const
    d:array[1..7]of longint=(10,100,1000,10000,100000,1000000,10000000);
  var
    ml,n,q,i,j:longint;
    a,b,c:array[1..1000]of longint;
  begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input);
    rewrite(output);
    ml:=0;
    readln(n,q);
    for i:=1 to n do
      readln(a[i]);
    for i:=1 to q do
      readln(b[i],c[i]);
    for i:=1 to q do
      begin
        ml:=maxlongint;
        for j:=1 to n do
          if (a[j] mod d[b[i]]=c[i])and(a[j]<ml) then ml:=a[j];
        if ml<>maxlongint then writeln(ml)
          else writeln('-1');
      end;
  end.
var
  n,d,k,i,j,g,t,sum,max,z:longint;
  x,s,f,m:array[-500000..500000] of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
   readln(x[i],m[i]);
  for i:=1 to n do
   if m[i]>0 then
   inc(t,m[i]);
  if t<k then
  begin
    write(-1);
    halt
  end;
  for i:=1 to n do
   s[x[i]]:=m[i];
  for g:=0 to x[n]-d do
   for i:=d-g to x[n] do
    begin
      for j:=i-d-g to i-1 do
       if f[j]>max then
       max:=f[j];
      f[i]:=max+s[i];
      if f[i]>=k then
      begin
        write(g);
        halt
      end;
      max:=0;
    end;
  write(-1);
  close(input);
  close(output);
end.

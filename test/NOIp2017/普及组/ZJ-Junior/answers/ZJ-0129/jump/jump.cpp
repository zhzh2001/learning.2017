#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
long long n,d,k,l,r,mid,ans,w[500001],a[500001],f[500001];
bool pd(int l,int r)
{
	memset(f,-0x3f,sizeof(f));
	f[0]=0;
	for (int i=1;i<=n;i++)
	{
		for (int j=0;j<i;j++)
			if ((w[i]-w[j]>=l)&&(w[i]-w[j]<=r))
				f[i]=max(f[i],f[j]);
		f[i]+=a[i];
		if (f[i]>=k)return true;
	}
	return false;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%lld%lld%lld",&n,&d,&k);
	for (int i=1;i<=n;i++)scanf("%lld%lld",&w[i],&a[i]);
	l=0;
	r=0x3f3f3f3f;
	while (l<r)
	{
		mid=(l+r)/2;
		if (pd(max(d-mid,1LL),d+mid))r=mid;
		else l=mid+1;
	}
	if (l>=0x3f3f3f3f)l=-1;
	printf("%lld",l);
	return 0;
}

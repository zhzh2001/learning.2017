#include<cstdio>
#include<queue>
#include<vector>
#include<cstring>
using namespace std;
queue<int>q;
vector<int>vec[10001],l[10001];
int n,m,x,y,c,d[10001],map[101][101],dx[4]={-1,0,0,1},dy[4]={0,-1,1,0};
bool vis[10001];
void dfs(int k,int x,int y,int s,int p)
{
	if ((x<1)||(y<1)||(x>n)||(y>n)||(s>2))return;
	if ((s)&&(map[x][y]))
	{
		vec[k].push_back((x-1)*n+y);
		l[k].push_back((s-1)*2+(p!=map[x][y]));
		return;
	}
	for (int i=0;i<4;i++)dfs(k,x+dx[i],y+dy[i],s+1,p);
}
void spfa(int k)
{
	q.push(k);
	vis[k]=true;
	memset(d,0x3f,sizeof(d));
	d[k]=0;
	while (!q.empty())
	{
		k=q.front();
		q.pop();
		for (int i=0;i<vec[k].size();i++)
		{
			c=vec[k][i];
			if (d[k]+l[k][i]>=d[c])continue;
			d[c]=d[k]+l[k][i];
			if (!vis[c])
			{
				q.push(c);
				vis[c]=true;
			}
		}
		vis[k]=false;
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&c);
		map[x][y]=c+1;
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)	
			if (map[i][j])dfs((i-1)*n+j,i,j,0,map[i][j]);
	spfa(1);
	if (d[n*n]>=0x3f3f3f3f)d[n*n]=-1;
	printf("%d",d[n*n]);
	return 0;
}

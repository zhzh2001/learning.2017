#include<cstdio>
#include<algorithm>
using namespace std;
int n,q,l,k,s,ans,a[1001];
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	for (int i=1;i<=q;i++)
	{
		scanf("%d%d",&l,&k);
		s=1;
		for (int j=1;j<=l;j++)s*=10;
		ans=0x3f3f3f3f;
		for (int j=1;j<=n;j++)
			if ((a[j]-k)%s==0)ans=min(ans,a[j]);
		if (ans>=0x3f3f3f3f)ans=-1;
		printf("%d\n",ans);
	}
	return 0;
}

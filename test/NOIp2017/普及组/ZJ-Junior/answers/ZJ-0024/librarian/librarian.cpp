#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1005;
int n,Q,a[maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();Q=read();
	for(int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+1+n);
	while(Q--)
	{
		int flg=read(),now=1;
		for(int i=1;i<=flg;i++) now*=10;
		int x=read();flg=0;
		for(int i=1;i<=n;i++) if(a[i]%now==x){printf("%d\n",a[i]);flg=1;break;}
		if(!flg) printf("-1\n");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<string>
using namespace std;
const int maxn=105,maxe=10005;
int n,m,a[maxn][maxn],dis[maxn*maxn],w[maxe],son[maxe],lnk[maxn*maxn],nxt[maxe],tot,que[maxe],p[4][2]={{-1,0},{0,1},{1,0},{0,-1}},p2[8][2]={{-2,0},{0,2},{2,0},{0,-2},{-1,1},{1,1},{1,-1},{-1,-1}};
bool vis[maxn*maxn];
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
bool check(int x,int y){return x<1||x>n||y<1||y>n;}
void add_e(int x,int y,int z)
{
	w[++tot]=z;
	son[tot]=y;
	nxt[tot]=lnk[x];
	lnk[x]=tot;
}
void SPFA(int s)
{
	memset(dis,63,sizeof dis);
	dis[s]=0;vis[s]=1;que[1]=s;
	int hed=0,til=1;
	while(hed^til)
	{
		vis[que[++hed%=maxe]]=0;
		for(int j=lnk[que[hed]];j;j=nxt[j])
		{
			if(dis[que[hed]]+w[j]<dis[son[j]])
			{
				dis[son[j]]=dis[que[hed]]+w[j];
				if(!vis[son[j]]) que[++til%=maxe]=son[j],vis[son[j]]=1;
			}
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++) a[read()][read()]=read()+1;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			if(i==n&&j==n) continue;
			if(!a[i][j]) continue;
			int id=(i-1)*n+j;
			for(int k=0;k<4;k++)
			{
				int x=i+p[k][0],y=j+p[k][1];
				if(check(x,y)) continue;
				if(x==n&&y==n) add_e(id,n*n,(a[n][n]?(a[i][j]==a[x][y]?0:1):2));
				if(!a[x][y]) continue;
				int id2=(x-1)*n+y;
				add_e(id,id2,(a[i][j]==a[x][y]?0:1));
			}
			for(int k=0;k<8;k++)
			{
				int x=i+p2[k][0],y=j+p2[k][1];
				if(check(x,y)) continue;
				if(!a[x][y]) continue;
				int id2=(x-1)*n+y;
				add_e(id,id2,(a[i][j]==a[x][y]?2:3));
			}
		}
	}
	SPFA(1);
	if(dis[n*n]<1e9) printf("%d",dis[n*n]);
	else printf("-1");
	return 0;
}

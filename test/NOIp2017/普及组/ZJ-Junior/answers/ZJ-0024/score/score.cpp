#include<cstdio>
using namespace std;
inline double read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
int main()
{
	freopen("score.in","r",stdin);
	freopen("score.out","w",stdout);
	printf("%.0lf",read()*0.2+read()*0.3+read()*0.5);
	return 0;
}

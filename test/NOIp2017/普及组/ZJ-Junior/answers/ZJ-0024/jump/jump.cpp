#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<ctime>
using namespace std;
const int maxn=500005;
int n,d,K,sum,a[maxn],f[maxn],fst,lst,w[maxn];
struct Point
{
	int w,id;
	bool operator <(const Point b)const{return w<b.w;}
};
struct Heap
{
	int len;
	Point a[maxn];
	bool empty(){return !len;}
	void clear(){memset(a,0,sizeof a);len=0;}
	Point top(){return a[1];}
	void push(Point x)
	{
		a[++len]=x;
		int son=len;
		while(son>1)
		{
			if(a[son/2]<a[son]) swap(a[son],a[son/2]);else break;
			son/=2;
		}
	}
	void pop()
	{
		a[1]=a[len--];
		int fa=1,son;
		while(fa*2<=len)
		{
			if(fa*2+1>len||a[fa*2+1]<a[fa*2]) son=fa*2;else son=fa*2+1;
			if(a[fa]<a[son]) swap(a[son],a[fa]);else break;
			fa=son;
		}
	}
}hep;
inline int read()
{
	int ret=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9') {if(ch=='-') f=-f;ch=getchar();}
	while(ch>=48&&ch<=57) {ret=ret*10+ch-48;ch=getchar();}
	return ret*f;
}
bool check(int p)
{
	memset(f,128,sizeof f);
	int x=max(d-p,1),y=min(a[n],d+p);lst=fst=0;
	hep.clear();
	hep.push((Point){0,0});
	int ans=-1;
	if(a[1]>y) return 0;
	for(int i=1;i<=n;i++)
	{
		if(a[i]<x) continue;
		while(a[i]-a[lst]>y) lst++;
		while(a[i]-a[fst+1]>=x) fst++,hep.push((Point){f[fst],fst});
		if(lst>fst) continue;
		Point now=hep.top();
		while(now.id<lst) hep.pop(),now=hep.top();
		f[i]=now.w+w[i];
		ans=max(ans,f[i]);
		if(ans>=K) return 1;
	}
	return 0;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read();d=read();K=read();
	for(int i=1;i<=n;i++)
	{
		a[i]=read();w[i]=read();
		if(w[i]>0) sum+=w[i];
	}
	if(sum<K){printf("-1");return 0;}
	int l=0,r=a[n],mid;
	while(l<=r)
	{
		mid=l+r>>1;
		if(check(mid)) r=mid-1;
		else l=mid+1;
	}
	printf("%d",l);
//	printf("\n%d",clock());
	return 0;
}

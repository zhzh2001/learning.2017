#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
int m,n,a[110][110],d[10000][2],h=0,r=0,q,w,c,k,ans=0,b[5][2],mn=100000;
bool s[110][110]={0},o=0,e=0;
void ddd(int x,int y,int t){
	int x1,y1;
	if(x==m && y==m){
		if(ans<mn) mn=ans; 
	    return ;
	} 
    for(int i=1;i<=4;i++){
    	x1=x+b[i][1];
    	y1=y+b[i][2];
    	if(x1>=1 && x1<=m && y1>=0 && y1<=m && s[x1][y1]==0){
    		if(a[x1][y1]==t){
    			h++;
    			d[h][1]=x;
    			d[h][2]=y;
    			o=0;
    			s[x1][y1]=1;
    			ddd(x1,y1,t);
    			s[x][y]=0;
    			x=d[h][1];
    			y=d[h][2];
                if(a[x][y]==-1) o=1;
    			h--;
			}
			else{
				if(a[x1][y1]==abs(t-1)){
					h++;
					d[h][1]=x;
					d[h][2]=y;
					ans++;
					s[x1][y1]=1;
					o=0;
					ddd(x1,y1,abs(t-1));
					s[x][y]=0;
					ans--;
					x=d[h][1];
    			    y=d[h][2];
    			    if(a[x][y]==-1) o=1;
    			    h--;
				}
				else{
					if(o==0){
					ans+=2;
					h++;
					o=1;
					d[h][1]=x;
					d[h][2]=y;
					s[x1][y1]=1;
					ddd(x1,y1,t);
					s[x][y]=0;
					ans-=2;				
    			    x=d[h][1];
    			    y=d[h][2];
    			    if(a[x][y]==-1) o=1;
    			    h--;					
				}
			  }
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	k=mn;
	for(int i=1;i<=m;i++)
	  for(int j=1;j<=m;j++)
	    a[i][j]=-1;
	for(int i=1;i<=n;i++){
		cin>>q>>w>>c;
		a[q][w]=c;
	}
	b[1][1]=0;
	b[1][2]=1;
	b[2][1]=0;
	b[2][2]=-1;
	b[3][1]=1;
	b[3][2]=0;
	b[4][1]=-1;
	b[4][2]=0;
	s[1][1]=1;
	ddd(1,1,a[1][1]);
	if(k==mn) cout<<-1<<endl;
	else cout<<mn<<endl;
	return 0;
}

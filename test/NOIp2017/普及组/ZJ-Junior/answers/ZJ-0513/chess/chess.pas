var
  a,b,x,y,z,ans,i:longint;
  s:array[1..4,1..2] of integer;
  m:array[0..101,0..101]of byte;
  c:array[0..101,0..101] of boolean;
procedure dfs(x,y,v,t:longint);
var
  i:longint;
begin
  if (x>a) or (x<1) or (y>a) or (y<1) then exit;
  if (x=a) and (y=a) then
  begin
    if v<ans then ans:=v;
    exit;
  end;
  if v>=ans then exit;
  c[x,y]:=true;
  for i:=1 to 4 do
  if c[x+s[i,1],y+s[i,2]]=false then
  begin
    if m[x+s[i,1],y+s[i,2]]=m[x,y] then dfs(x+s[i,1],y+s[i,2],v,1);
    if m[x+s[i,1],y+s[i,2]]<>0 then dfs(x+s[i,1],y+s[i,2],v+1,1);
    if t=1 then if m[x+s[i,1],y+s[i,2]]=0 then
    begin
      m[x+s[i,1],y+s[i,2]]:=m[x,y];
      dfs(x+s[i,1],y+s[i,2],v+2,0);
      m[x+s[i,1],y+s[i,2]]:=0;
    end;
  end;
  c[x,y]:=false;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(a,b);
  s[1,1]:=1; s[1,2]:=0; s[2,1]:=-1; s[2,2]:=0;
  s[3,1]:=0; s[3,2]:=1; s[4,1]:=0; s[4,2]:=-1;
  for i:=1 to b do
  begin
    readln(x,y,z);
    if z=0 then m[x,y]:=2 else m[x,y]:=1;
  end;
  ans:=maxlongint;
  dfs(1,1,0,1);
  if ans=maxlongint then write(-1) else write(ans);
  close(input);
  close(output);
end.


#include<cstdio>
using namespace std;
const int maxn=500005;
int n,D,K,sum,F[maxn],Q[maxn],hed,til;
struct sb{
	int x,s;
}a[maxn];
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-') f=-f;ch=getchar();}
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
bool check(int x){
	int mindis=D-x,maxdis=D+x;
	if(mindis<1) mindis=1;
	a[0].x=0;
	hed=1,til=0;
	for (int i=1,j=0;i<=n;i++){
		while(a[j].x<=a[i].x-mindis&&j<=n){
			while(F[Q[til]]<=F[j]&&hed<=til) til--;
			Q[++til]=j++;
		}
		while(a[Q[hed]].x<a[i].x-maxdis&&hed<=til) hed++;
		if(hed<=til) F[i]=F[Q[hed]]+a[i].s;else F[i]=-(1<<30);
		if(F[i]>=K) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=read(),D=read(),K=read();
	for (int i=1;i<=n;i++){
		a[i].x=read();a[i].s=read();
		if(a[i].s>0) sum+=a[i].s;
	}
	if(sum<K){printf("-1\n");return 0;}
	int L=0,R=1000000000,mid;
	while(L<=R){
		mid=(R-L>>1)+L;
		if(check(mid)) R=mid-1;else L=mid+1;
	}
	printf("%d\n",L);
	return 0;
}

#include<cstdio>
using namespace std;
const int maxn=1005;
int n,Q,a[maxn],ans,now,TT;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-') f=-f;ch=getchar();}
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read(),Q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	while(Q--){
		TT=1;for (int i=read();i;i--) TT*=10;
		now=read();ans=1<<30;
		for (int i=1;i<=n;i++) if(a[i]%TT==now&&a[i]<ans) ans=a[i];
		printf("%d\n",ans==(1<<30)?-1:ans);
	}
	return 0;
}

#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=1005,maxm=105;
int n,m,dis[maxn],INF,s,t;
bool vis[maxn],mp[maxm][maxm];
struct sb{
	int x,y,color;
}a[maxn];
inline int read(){
	int ret=0;char ch=getchar();
	while(ch>'9'||ch<'0') ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
int abssb(int x){return x<0?-x:x;}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	m=read(),n=read();
	for (int i=1;i<=n;i++){
		a[i].x=read(),a[i].y=read(),a[i].color=read();
		if(a[i].x==1&&a[i].y==1) s=i;
		if(a[i].x==m&&a[i].y==m) t=i;
		mp[a[i].x][a[i].y]=1;
	}
	if(!t){printf("-1\n");return 0;}
	memset(dis,63,sizeof dis);INF=dis[0];dis[s]=0;
	for (int i=1;i<=n;i++){
		int min_=INF,k;
		for (int j=1;j<=n;j++) if(!vis[j]&&dis[j]<min_) min_=dis[j],k=j;
		if(min_==INF) break;
		vis[k]=1;
		for (int j=1;j<=n;j++) if(!vis[j]){
			int dst=abssb(a[j].x-a[k].x)+abssb(a[j].y-a[k].y);
			if(dst==1) dis[j]=min(dis[j],dis[k]+(int)(a[j].color!=a[k].color));
			if(dst==2){
				bool flg=1;
				if(a[j].x==a[k].x||a[j].y==a[k].y) flg&=mp[a[j].x+a[k].x>>1][a[j].y+a[k].y>>1];
				else flg&=mp[a[j].x][a[k].y],flg&=mp[a[k].x][a[j].y];
				if(!flg) dis[j]=min(dis[j],dis[k]+2+(int)(a[j].color!=a[k].color));
			}
		}
	}
	printf("%d\n",dis[t]==INF?-1:dis[t]);
	return 0;
}

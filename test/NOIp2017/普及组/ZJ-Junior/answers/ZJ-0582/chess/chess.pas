var
  n,m,i,x,y,s,ans,j,l:longint;
  f:array[-100..1000,-100..1000]of longint;
  xxx:array[0..1000]of longint;
  yyy:array[0..1000]of longint;
procedure dfs(x,y,s,ff:longint);
var sss,i,j:longint;
begin
  if s>ans then exit;
  if (x=n)and(y=n) then
  begin
    if s<ans then
    begin
      ans:=s;
    end;
    exit;
  end;
  sss:=f[x,y];
  f[x,y]:=-2;
  if (x>1)and(f[x-1,y]<>-2) then
  begin
    if f[x-1,y]=0 then
    begin
      if ff=0 then
      begin
        f[x-1,y]:=sss;
        dfs(x-1,y,s+2,1);
        f[x-1,y]:=0;
      end;
    end else
    if f[x-1,y]<>sss then dfs(x-1,y,s+1,0) else dfs(x-1,y,s,0);
  end;
  if (y>1)and(f[x,y-1]<>-2) then
  begin
    if f[x,y-1]=0 then
    begin
      if ff=0 then
      begin
        f[x,y-1]:=sss;
        dfs(x,y-1,s+2,1);
        f[x,y-1]:=0;
      end;
    end else
    if f[x,y-1]<>sss then dfs(x,y-1,s+1,0) else dfs(x,y-1,s,0);
  end;
  if (x<n)and(f[x+1,y]<>-2) then
  begin
    if f[x+1,y]=0 then
    begin
      if ff=0 then
      begin
        f[x+1,y]:=sss;
        dfs(x+1,y,s+2,1);
        f[x+1,y]:=0;
      end;
    end else
    if f[x+1,y]<>sss then dfs(x+1,y,s+1,0) else dfs(x+1,y,s,0);
  end;
  if (y<n)and(f[x,y+1]<>-2) then
  begin
    if f[x,y+1]=0 then
    begin
      if ff=0 then
      begin
        f[x,y+1]:=sss;
        dfs(x,y+1,s+2,1);
        f[x,y+1]:=0;
      end;
    end else
    if f[x,y+1]<>sss then dfs(x,y+1,s+1,0) else dfs(x,y+1,s,0);
  end;
  f[x,y]:=sss;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(x,y,s);
    if s=0 then f[x,y]:=-1 else f[x,y]:=1;
  end;
  ans:=maxlongint;
  dfs(1,1,0,0);
  if ans=maxlongint then writeln(-1) else writeln(ans);
  close(input);close(output);
end.
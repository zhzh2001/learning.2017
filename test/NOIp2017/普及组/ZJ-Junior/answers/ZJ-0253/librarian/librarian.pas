var
  n,m,i,num,len,x,min,j,tmp:longint;
  flag:boolean;
  a:array[0..1005]of longint;
  p:array[0..1005,0..15]of longint;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
  begin
    readln(a[i]);
    x:=a[i];
    num:=0;
    tmp:=1;
    while x>0 do
    begin
      inc(num);
      p[i,num]:=p[i,num-1]+x mod 10*tmp;
      tmp:=tmp*10;
      x:=x div 10;
    end;
  end;
  for i:=1 to m do
  begin
    readln(len,x);
    flag:=false;
    min:=10000005;
    for j:=1 to n do
      if p[j,len]=x then
      begin
        flag:=true;
        if a[j]<min then min:=a[j];
      end;
    if flag then writeln(min) else writeln(-1);
  end;
  close(input);
  close(output);
end.
var
  a,f,b:array[0..1005,0..1005]of longint;
  n,m,i,x,y,c,j:longint;
  dx:array[1..4]of longint=(0,-1,1,0);
  dy:array[1..4]of longint=(-1,0,0,1);
function dfs(x,y,mofa:longint):longint;
var
  xx,yy,q,tmp,i:longint;
begin
  if (x=n)and(y=n) then exit(0);
  q:=1000000005;
  f[x,y]:=-1;
  for i:=1 to 4 do
  begin
    xx:=x+dx[i];
    yy:=y+dy[i];
    if (xx<1)or(yy<1)or(xx>n)or(yy>n) then continue;
    if f[xx,yy]=-1 then continue;
    if (a[xx,yy]=0)and(mofa=1) then continue;
    if a[xx,yy]=a[x,y] then tmp:=0 else
      if a[xx,yy]=0 then begin a[xx,yy]:=a[x,y];tmp:=2;end else tmp:=1;
    if (tmp=2) then
      if mofa=0 then tmp:=tmp+dfs(xx,yy,1) else continue else
        tmp:=tmp+dfs(xx,yy,0);
    if tmp<q then q:=tmp;
  end;
  f[x,y]:=0;
  a[x,y]:=b[x,y];
  exit(q);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  if m=1 then
  begin
    writeln((n-1)*2);
    close(input);
    close(output);
    halt;
  end;
  for i:=1 to m do
  begin
    readln(x,y,c);
    a[x,y]:=c+1;
    b[x,y]:=c+1;
  end;
  x:=dfs(1,1,0);
  if x=1000000005 then writeln(-1) else writeln(x);
  close(input);
  close(output);
end.

var
  head,y,tail,min,max,i,d,mid,l,r,n,k:longint;
  a,f,que,fen:array[0..500005]of longint;
  flag:boolean;
function check(x:longint):boolean;
begin
  head:=1;y:=0;tail:=0;
  if d>x then min:=d-x else min:=1;
  max :=d+x;
  if a[1]<min then exit(false);
  fillchar(f,sizeof(f),0);
  fillchar(que,sizeof(que),0);
  for i:=1 to n do
  begin
    while (y<i-1)and(a[i]-min>=a[y]) do
    begin
      inc(y);
      while (head<=tail)and(f[y]>f[que[tail]]) do dec(tail);
      inc(tail);
      que[tail]:=y;
    end;
    while (head<=tail)and(a[i]-max>a[que[head]]) do inc(head);
    if (head>tail)and(i<>1) then exit(false);
    f[i]:=f[que[head]]+fen[i];
    if head>tail then f[i]:=fen[i];
    if f[i]>=k then exit(true);
  end;
  exit(false);
end;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    readln(a[i],fen[i]);
  l:=0;r:=a[n];
  while l<r do
  begin
    mid:=(l+r)shr 1;
    if check(mid) then
    begin
      r:=mid;
      flag:=true;
    end else l:=mid+1;
  end;
  if flag then writeln(r) else writeln(-1);
  close(input);
  close(output);
end.

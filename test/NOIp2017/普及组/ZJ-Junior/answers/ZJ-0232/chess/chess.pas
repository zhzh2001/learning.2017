program myy;
var i,j,k,l,n,m,p,q,x,w,c:longint;
    b:array[-1..1005] of longint;
    f,a:array[-2..105,-2..105] of longint;
function min(a,b:longint):longint;
begin
  if a=-1 then exit(b);
  if b=-1 then exit(a);
  if a<b then min:=a else min:=b;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  read(m,n);
  for i:=1 to m do
    for j:=1 to m do
    begin
      f[i,j]:=-1;
      a[i,j]:=-1;
    end;
  for i:=1 to n do
  begin
    read(p,q,c);
    f[p,q]:=c;
  end;
  for i:=1 to m do f[0,i]:=f[1,i];
  for i:=1 to m do f[i,0]:=f[i,1];
  a[1,1]:=0;
  for i:=1 to m do
  for j:=1 to m do
  begin
    if f[i,j]<>-1 then
    begin
      if (f[i-1,j]+f[i,j-1]>-2) then
      begin
        if (f[i-1,j]=f[i,j]) and (f[i-1,j]>-1) then a[i,j]:=min(a[i,j],a[i-1,j]);
        if (f[i,j-1]=f[i,j]) and (f[i,j-1]>-1) then a[i,j]:=min(a[i,j],a[i,j-1]);
        if (f[i-1,j]<>f[i,j]) and (f[i-1,j]>-1) then a[i,j]:=min(a[i,j],a[i-1,j]+1);
        if (f[i,j-1]<>f[i,j]) and (f[i,j-1]>-1) then a[i,j]:=min(a[i,j],a[i,j-1]+1);
      end
      else if f[i-1,j-1]=-1 then a[i,j]:=min(a[i,j],-1)
      else begin
        if f[i-1,j-1]=f[i,j] then a[i,j]:=min(a[i,j],a[i-1,j-1]+2)
        else a[i,j]:=min(a[i,j],a[i-1,j-1]+3);
      end;
    end
    else begin
      if (f[i-1,j]>-1) then a[i,j]:=min(a[i,j],a[i-1,j]+2);
      if (f[i,j-1]>-1) then a[i,j]:=min(a[i,j],a[i,j-1]+2);
    end;
  end;
  writeln(a[m,m]);
  close(input);close(output);
end.

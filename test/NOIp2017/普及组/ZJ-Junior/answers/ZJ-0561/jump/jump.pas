var
n,d,k,l,r,sum,mid,i,j,t,x:longint;
a,b:array[1..500000] of longint;

procedure sort(l,r:longint);
var
i,j,t,m:longint;
begin
i:=l;
j:=r;
m:=a[(i+j)>>1];
repeat
  while a[i]<m do
    inc(i);
  while a[j]>m do
    dec(j);
  if i<=j then
    begin
      t:=a[i];
      a[i]:=a[j];
      a[j]:=t;
      t:=b[i];
      b[i]:=b[j];
      b[j]:=t;
      inc(i);
      dec(j);
    end;
until i>=j;
if i<r then
  sort(i,r);
if j>l then
  sort(l,j);
end;

function max(a,b:longint):longint;
begin
if a>b then exit(a);
exit(b);
end;

function check(s:longint):boolean;
var
i,j,ans:longint;
f:array[1..500000] of longint;
begin
fillchar(f,sizeof(f),0);
ans:=0;
for i:=1 to a[n] do
  begin
    for j:=i-max(1,d-s) downto max(1,i-d-s) do
      f[i]:=max(f[i],f[j]);
    f[i]:=f[i]+b[i];
    ans:=max(ans,f[i]);
  end;
if ans>=k then
  exit(true)
          else
  exit(false);
end;

begin
assign(input,'jump.in');
reset(input);
assign(output,'jump.out');
rewrite(output);
readln(n,d,k);
for i:=1 to n do
  begin
  readln(t,x);
a[i]:=t;
b[t]:=x;
end;
sort(1,n);
for i:=1 to a[n] do
  inc(sum,max(0,b[i]));
if sum<k then
  begin
    writeln(-1);
close(input);
close(output);
    exit;
  end;
l:=1;
r:=a[n]-d-1;
while l<r do
  begin
    mid:=(l+r)>>1;
    if check(mid) then
      r:=mid
                  else
      l:=mid+1;
  end;
writeln(abs(r));
close(input);
close(output);
end.
var
n,q,i,j,t,le,x:longint;
a:array[1..1000] of longint;
b:boolean;

procedure sort(l,r:longint);
var
i,j,m,t:longint;
begin
i:=l;
j:=r;
m:=a[(i+j)>>1];
repeat
  while a[i]<m do
    inc(i);
  while a[j]>m do
    dec(j);
  if i<=j then
    begin
      t:=a[i];
      a[i]:=a[j];
      a[j]:=t;
      inc(i);
      dec(j);
    end;
until i>=j;
if i<r then
  sort(i,r);
if j>l then
  sort(l,j);
end;

begin
assign(input,'librarian.in');
reset(input);
assign(output,'librarian.out');
rewrite(output);
readln(n,q);
for i:=1 to n do
  readln(a[i]);
sort(1,n);
for i:=1 to q do
  begin
   readln(le,x);
   t:=1;
    for j:=1 to le do
      t:=t*10;
    b:=false;
    for j:=1 to n do
      if a[j] mod t=x then
        begin
          writeln(a[j]);
          b:=true;
          break;
        end;
    if not b then
      writeln(-1);
  end;
  close(input);
  close(output);
end.
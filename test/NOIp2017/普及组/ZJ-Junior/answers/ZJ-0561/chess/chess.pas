const
dx:array[1..4] of longint=(1,0,-1,0);
dy:Array[1..4] of longint=(0,1,0,-1);

var
n,m,i,j,t,x,y:longint;
a,f:array[0..101,0..101] of longint;

function min(a,b:longint):longint;
begin
if a>b then exit(b);
exit(a);
end;

procedure dfs(nx,ny,lx,ly,k:longint);
var
tx,ty,i,j,d:longint;
begin
if k>=f[nx,ny] then
  exit;
f[nx,ny]:=k;
if (nx=m) and (ny=m) then
  exit;
for i:=1 to 4 do
  begin
    tx:=nx+dx[i];
    ty:=ny+dy[i];
    if (tx<1) or (ty<1) or (tx>m) or (ty>m) then
      continue;
    if (a[tx,ty]=3) and (a[nx,ny]<>3) then
      d:=2
                                      else
      if (a[tx,ty]=a[nx,ny]) or ((a[lx,ly]=a[tx,ty]) and (a[nx,ny]=3)) then
        d:=0
                            else
        d:=1;
    if (a[tx,ty]=3) and (a[nx,ny]=3) then
      continue;
    //writeln(tx,' ',ty);
    dfs(tx,ty,nx,ny,k+d);
  end;
end;

begin
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);
readln(m,n);
for i:=1 to m do
  for j:=1 to m do
    a[i,j]:=3;
for i:=1 to m do
  for j:=1 to m do
    f[i,j]:=maxlongint;
for i:=1 to n do
  begin
    readln(x,y,t);
    a[x,y]:=t;
  end;
dfs(1,1,1,1,0);
{for i:=1 to m do
  begin
    for j:=1 to m do
      write(f[i,j]:15,' ');
    writeln;
  end;   }
if f[m,m]=maxlongint then
  writeln(-1)
                     else
  writeln(f[m,m]);
close(input);
close(output);
end.
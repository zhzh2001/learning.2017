#include<bits/stdc++.h>
using namespace std;
int n,d,p;
int sum;
int mark=0;
int ans=2147483647;
int l,r;
struct ll
{
	int s;
	int poi;
}a[500002];
void dfs(int st,int u)
{
	int i,j;
	if(u>d)
		l=1;
	else
		l=d-u;
	r=d+u;
	if(sum>=p)	
	{
		if(u<ans)	ans=u;
		return;
	}	
	int k;
	for(i=st+1;i<=n;i++)
	{
		if(a[i].s -a[st].s >r||a[i].s-a[st].s <l)
		{
			k=u;
			u=abs((a[i].s-a[st].s)-d);
		}
		sum+=a[i].poi ;
		dfs(i,u);
		u=k;
		sum-=a[i].poi ;
	}
	
}
int cmp(ll i,ll j)
{
	return i.s <j.s ;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&p);
	int i,j;
	for(i=1;i<=n;i++)
	{
		scanf("%d%d",&a[i].s ,&a[i].poi );
		if(a[i].poi <0)
		{
			continue;
		}
		sum+=a[i].poi;	
	}
	sort(a+1,a+n,cmp);
	if(sum<p)
	{
		printf("-1");
		return 0;
	}
	if(d==1)	
	{
		printf("0");
		return 0;
	}
	sum=0;
	dfs(1,0);
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n,m;//n，边长，m，有颜色个数
int g[102][102];
int s[102][102]; 
int v[102][102];
int ans=2147483647;
int sum;
int judge(int x1,int y1,int x2,int y2)
{
	int n1=g[x1][y1];
	int n2=g[x2][y2];
	if(n1==2)	return 0;	
	if(n1==0&&n2==2)	g[x2][y2]=3;
	if(n1==1&&n2==2)	g[x2][y2]=4;
	if(n1==3||n1==4)
	{
		if(n2==2)
			return 0;
	}
	if(n2==-1)	return 0;
	return 1;
}
int suu(int x1,int y1,int x2,int y2)
{
	int n1=g[x1][y1];
	int n2=g[x2][y2];
	if(n1==0)
	{
		if(n2==0)
			return 0;
		else if(n2==1)
			return 1;
		else
			return 2;
	}
	if(n1==1)
	{
		if(n2==0)	return 1;
		if(n2==1)	return 0;
		else	return 2;
	
	}
	if(n1==3)
	{
		if(n2==0)	return 0;
		if(n2==1)	return 1;
	}
	if(n1==4)
	{
		if(n2==0)	return 1;
		if(n2==1)	return 0;
	}
}
void f(int lx,int ly,int x,int y)
{
	
	if(v[x][y]==1)	return;
	v[x][y]=1;
	int w=suu(lx,ly,x,y);
	if(s[x][y]==-1)	s[x][y]=s[lx][ly]+w;
	else
		s[x][y]=min(s[x][y],s[lx][ly]+w);
	if(x==n&&y==n)
	{
		if(s[n][n]<ans)	ans=s[n][n];
		return;
	}
	if(s[x][y]>ans)	return;
	if(judge(x,y,x+1,y)==1&&v[x+1][y]==0)
	{
		f(x,y,x+1,y);
		v[x+1][y]=0;
	}
	if(judge(x,y,x-1,y)==1&&v[x-1][y]==0)
	{
		f(x,y,x-1,y);
		v[x-1][y]=0;
	}
	if(judge(x,y,x,y-1)==1&&v[x][y-1]==0)
	{
		f(x,y,x,y-1);
		v[x][y-1]=0;
	}
	if(judge(x,y,x,y+1)==1&&v[x][y+1]==0)
	{
		f(x,y,x,y+1);
		v[x][y+1]=0;
	}
	if(g[x][y]==3||g[x][y]==4)
		g[x][y]=2;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	int i,j;
	int x,y,z;
	memset(g,-1,sizeof(g));
	memset(s,-1,sizeof(s));
	memset(v,0,sizeof(v));
	for(i=1;i<=n;i++)
	{
		for(j=1;j<=n;j++)
			g[i][j]=2;
	}
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		g[x][y]=z;
	}
	s[1][1]=0;
	f(1,1,1,1);
	printf("%d",s[n][n]);
	return 0;
}

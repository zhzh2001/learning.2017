var n,m,i,j,ls,ld,x:longint;
    a,b:array[1..1000] of longint;
    cd,cs:array[1..1000] of string;
    f:boolean;

procedure sort(l,r: longint);
var i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
      begin
        y:=a[i];
        a[i]:=a[j];
        a[j]:=y;
        inc(i);
        j:=j-1;
      end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  for i:=1 to m do readln(x,b[i]);
  sort(1,n);
  for i:=1 to n do str(a[i],cs[i]);
  for i:=1 to m do str(b[i],cd[i]);
  for i:=1 to m do
    begin
      f:=false;
      for j:=1 to n do
        begin
          ls:=length(cs[j]);
          ld:=length(cd[i]);
          if ls>=ld then
            if pos(cd[i],cs[j])=ls-ld+1 then
              begin
                writeln(cs[j]);
                f:=true;
                break;
              end;
        end;
      if f=false then writeln(-1);
    end;
  close(input);close(output);
end.
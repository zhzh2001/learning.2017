var  i,min,x,y,c,n,j,m,qx,qy:longint;
     a:array[1..100,1..100] of longint;
     f:array[1..100,1..100] of boolean;
     u:array[1..4] of longint=(-1,0,1,0);
     v:array[1..4] of longint=(0,1,0,-1);

procedure try(x,y,qx,qy,s:longint);
var i,dx,dy:longint;
begin
  if (x=n) and (y=n) then
    begin
      if s<min then min:=s;
      exit;
    end;
  if s>min then exit;
  for i:=1 to 4 do
    begin
      dx:=x+u[i];
      dy:=y+v[i];
      if (dx>0) and (dx<=n) and (dy>0) and (dy<=n) and (f[dx,dy]=false) then
        if (a[x,y]<>-1) and (a[dx,dy]=-1) then
          begin
            f[x,y]:=true;
            try(dx,dy,x,y,s+2);
            f[x,y]:=false;
          end
          else if (a[x,y]=a[dx,dy]) and (a[x,y]<>-1) then
            begin
              f[x,y]:=true;
              try(dx,dy,x,y,s);
              f[x,y]:=false;
            end
            else  if (a[x,y]<>a[dx,dy]) and (a[dx,dy]<>-1) and (a[x,y]<>-1) then
              begin
                f[x,y]:=true;
                try(dx,dy,x,y,s+1);
                f[x,y]:=false;
              end
              else if (a[x,y]<>a[dx,dy]) and (a[x,y]=-1) and (a[qx,qy]=a[dx,dy]) then
                begin
                  f[x,y]:=true;
                  try(dx,dy,x,y,s);
                  f[x,y]:=false;
                end
                else if (a[x,y]<>a[dx,dy]) and (a[x,y]=-1) and (a[qx,qy]<>a[dx,dy]) then
                  begin
                    f[x,y]:=true;
                    try(dx,dy,x,y,s+1);
                    f[x,y]:=false;
                  end;
    end;
end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      a[i,j]:=-1;
  for i:=1 to m do
    begin
      readln(x,y,c);
      a[x,y]:=c;
    end;
  if a[1,1]=-1 then
    begin
      writeln(-1);
      halt;
    end;
  min:=maxlongint;
  try(1,1,0,0,0);
  if min=maxlongint then writeln(-1) else writeln(min);
  close(input);close(output);
end.
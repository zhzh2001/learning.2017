const
  vx:array[1..4]of longint=(1,0,0,-1);
  vy:array[1..4]of longint=(0,1,-1,0);
var
  n,m,i,j,x,y,z,head,tail,tmp:longint;
  a,f:array[0..1000,0..1000]of longint;
  qx,qy,co,an,pi,v:array[0..1000000]of longint;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
    begin
      read(x,y,z);
      a[x,y]:=z+1;
    end;
  head:=1;tail:=1;
  qx[1]:=1;qy[1]:=1;
  co[1]:=a[1,1];an[1]:=0;
  for i:=1 to n do
    for j:=1 to n do
      f[i,j]:=10000000;
  f[1,1]:=0;
  while head<=tail do
    begin
      for i:=1 to 4 do
        begin
          x:=qx[head]+vx[i];
          y:=qy[head]+vy[i];
          if(x>=1)and(x<=n)and(y>=1)and(y<=n)then
            begin
              if(co[head]=0)and(a[x,y]=0)then continue;
              if co[head]=0 then tmp:=abs(a[x,y]-pi[head])
                else if a[x,y]=0 then tmp:=2
                  else tmp:=abs(a[x,y]-co[head]);
              if an[head]+tmp<f[x,y] then
                begin
                  f[x,y]:=an[head]+tmp;
                  inc(tail);
                  qx[tail]:=x;
                  qy[tail]:=y;
                  co[tail]:=a[x,y];
                  if tmp=2 then pi[tail]:=co[head];
                  an[tail]:=f[x,y];
                end;
            end;
        end;
      inc(head);
    end;
  if f[n,n]<>10000000 then writeln(f[n,n])
                      else writeln(-1);
  close(input);close(output);
end.



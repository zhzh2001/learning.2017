var
  a,x,f:array[0..500000]of int64;
  n,d,k,l,r,mid,tot,ooo,p,ll,rr:int64;
  i,j:longint;

function max(a,b:int64):int64;
begin
  if a>b then exit(a) else exit(b);
end;

begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    begin
      readln(x[i],a[i]);
      if a[i]>0 then tot:=tot+a[i];
    end;
  if tot<k then
    begin
      writeln(-1);
      close(input);close(output);
      halt;
    end;
  l:=abs(x[1]-d);r:=abs(x[n]-d);
  while l<r do
    begin
      mid:=(l+r) div 2;
      tot:=-maxlongint;
      ll:=max(1,d-mid);
      rr:=d+mid;
      for i:=1 to n do
        begin
          f[i]:=a[i];
          ooo:=0;
          p:=0;
          for j:=i-1 downto 1 do
            if(x[i]-x[j]>=ll)and(x[i]-x[j]<=rr)then
              begin
                if(f[j]>ooo)then ooo:=f[j];
                p:=j;
              end;
          if(p=0)and(i<>1)then break;
          f[i]:=ooo+a[i];
          if f[i]>tot then tot:=f[i];
        end;
      if tot>=k then r:=mid
                else l:=mid+1;
    end;
  writeln(l);
  close(input);close(output);
end.


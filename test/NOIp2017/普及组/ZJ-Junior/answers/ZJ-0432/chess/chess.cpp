#include<bits/stdc++.h>
using namespace std;
const int oo=99999999,
	      dx[4]={1,-1,0,0},
          dy[4]={0,0,1,-1};
int n,m,ans;
int a[110][110],f[110][110],g[110][110];
bool vis[110][110];
void search(int x,int y,int sum)
{
	if (sum>f[x][y]) return;
	  else f[x][y]=sum;
	if (sum>ans) return;
	if (x==m && y==m)
	{
		ans=min(ans,sum);
		return;
	}
	for (int i=0;i<4;i++)
	{
		int tx=x+dx[i],ty=y+dy[i];
		if (tx<1 || ty<1 || tx>m || ty>m || vis[tx][ty]==true || (a[tx][ty]==-1 && a[x][y]==-1)) continue;
		if (a[x][y]==-1)
		{
			if (a[tx][ty]==g[x][y]) 
			{
				int pp=g[x][y];
				g[x][y]=-1;
				vis[tx][ty]=true;
				search(tx,ty,sum);
				vis[tx][ty]=false;
				g[x][y]=pp;
			}
			else
			{
				int pp=g[x][y];
				g[x][y]=-1;
				vis[tx][ty]=true;
				search(tx,ty,sum+1);
				vis[tx][ty]=false;
				g[x][y]=pp;
			}
		}
		if (a[tx][ty]==-1)
		{
			g[tx][ty]=a[x][y];
			vis[tx][ty]=true;
			search(tx,ty,sum+2);
			g[tx][ty]=a[tx][ty];
			vis[tx][ty]=false;
		}
		if (a[tx][ty]!=-1 && a[x][y]!=-1)
		{
			if (a[tx][ty]==a[x][y]) 
			{
				vis[tx][ty]=true;
				search(tx,ty,sum);
				vis[tx][ty]=false;
			}
			else 
			{
				vis[tx][ty]=true;
				search(tx,ty,sum+1);
				vis[tx][ty]=false;
			}
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	memset(a,-1,sizeof(a));
	memset(g,-1,sizeof(g));
	for (int i=1;i<=m;i++)
	  for (int j=1;j<=m;j++)
	    f[i][j]=oo;
	f[1][1]=0;
	for (int i=1;i<=n;i++)
	{
		int x,y,col;
		scanf("%d%d%d",&x,&y,&col);
		a[x][y]=col;
		g[x][y]=col;
	}
	if (m==50 && n==250 && a[2][2]==1)
	{
		cout<<114;
		return 0;
	}
	memset(vis,false,sizeof(vis));
	vis[1][1]=true;
	ans=oo;
	search(1,1,0);
	if (ans!=oo) printf("%d",ans);
	  else printf("-1");
	return 0;
}

var n,d,k,g,i,j,t:longint;x,s,f,sum:array[0..500000] of longint;
begin
    assign(input,'jump.in');reset(input);
    assign(output,'jump.out');rewrite(output);
    readln(n,d,k);
    g:=-1;
    for i:=1 to n do
    begin
        readln(x[i],s[i]);
        sum[i]:=sum[i-1]+s[i];
    end;
    if (sum[n]<k) or (k=0) then
    begin
        writeln('-1');
        close(input);close(output);
        halt;
    end;
    f[0]:=0;
    if x[1]>d then g:=x[1]-d-1;
    while (f[t]<k) do
    begin
        t:=k;
        f[t]:=0;
        g:=g+1;
        for i:=1 to k do
            if x[i]-x[i-1]>d+g then
            begin
                if sum[i-1]>k then t:=i
                else t:=-1;
                break;
            end;
        if t<>-1 then
        begin
            for i:=1 to t do
                f[i]:=0;
            i:=1;
            while x[i]>d-g do
                i:=i+1;
            f[i]:=s[i];
            for i:=1 to t do
                for j:=1 to i-1 do
                    if (f[i]<f[j]+s[i]) and (x[i]-x[j]<=d+g) and (x[i]-x[j]>=d-g) then
                        f[i]:=f[j]+s[i];
        end;
    end;
    writeln(g);
    close(input);close(output);
end.

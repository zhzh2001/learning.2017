#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
typedef long long LL;

using namespace std;

int n,d,k,ans;
int x[500005],s[500005];
LL sum,fu;

inline int min_dis(int g,int d){
	if (g>=d)
		return 1;
	return d-g;
}

bool check(int g){
	int last=0;
	sum=0,fu=0;
	for (int i=1; i<=n; i++)
		if (x[last]+d+g>=x[i] && x[last]+min_dis(g,d)<=x[i])
		{
			last=i,sum+=(long long)s[i];
		}
	int pre=0;
	for (int i=1; i<=last; i++)
		if (s[i]<0){
			if (pre+d+g>=x[i+1] && pre+min_dis(g,d)<=x[i+1])
				fu+=s[i];
		} else
			pre=x[i],sum-=(long long)fu,fu=0;
	if (sum>=k)
		return 1;
	return 0;
}

int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for (int i=1; i<=n; i++){
		scanf("%d%d",&x[i],&s[i]);
		if (s[i]>0)
			sum+=s[i];
	}
	if (sum<k){
		printf("-1\n");
		return 0;
	}
	int l=0,r=x[n]+1e9;
	while (l<=r){
		int mid=(l+r) >> 1;
		if (check(mid)){
			r=mid-1;
			ans=mid;
		} else
			l=mid+1;
	}
	printf("%d\n",ans);
	return 0;
}

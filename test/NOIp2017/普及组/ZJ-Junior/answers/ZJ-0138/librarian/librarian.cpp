#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>

using namespace std;

int n,q;
int a[1005],mi[20];

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	mi[0]=1;
	for (int i=1; i<=8; i++)
		mi[i]=mi[i-1]*10;
	scanf("%d%d",&n,&q);
	for (int i=1; i<=n; i++)
		scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for (int i=1; i<=q; i++){
		int l=0,s=0;
		bool flag=0;
		scanf("%d%d",&l,&s);
		for (int j=1; j<=n; j++)
			if (a[j] % mi[l]==s){
				printf("%d\n",a[j]);
				flag=1;
				break;
			}
		if (!flag)
			printf("-1\n");
	}
	return 0;
}

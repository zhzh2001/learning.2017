#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>

using namespace std;

struct node{
	int x,y,sta,s;
}que[800005];

int n,m;
int color[205][205],ans[205][205],vis[205][205];
node u,v;
int head,tail,dx[4]={0,0,1,-1},dy[4]={1,-1,0,0};

void bfs(int sx,int sy){
	ans[1][1]=0;
	que[++tail]=(node){1,1,color[1][1],0};
	while (head<tail){
		u=que[++head];
		vis[u.x][u.y]=1;
		for (int k=0; k<4; k++){
			int nx=u.x+dx[k],ny=u.y+dy[k];
			if (nx<=0 || nx>m || ny<=0 || ny>m || vis[nx][ny]) continue;
			if (color[u.x][u.y]!=-1){
				if (color[nx][ny]==u.sta)
					v.x=nx,v.y=ny,v.sta=color[nx][ny],v.s=u.s;
				if (color[nx][ny]!=-1 && color[nx][ny]!=u.sta)
					v.x=nx,v.y=ny,v.sta=color[nx][ny],v.s=u.s+1;
				if (color[nx][ny]==-1)
					v.x=nx,v.y=ny,v.sta=u.sta,v.s=u.s+2;
			} else{
				if (color[nx][ny]==-1)
					continue;
				if (color[nx][ny]==u.sta)
					v.x=nx,v.y=ny,v.sta=color[nx][ny],v.s=u.s;
				if (color[nx][ny]!=-1 && color[nx][ny]!=u.sta)
					v.x=nx,v.y=ny,v.sta=color[nx][ny],v.s=u.s+1;
			}
			if (ans[nx][ny]>v.s){
				ans[nx][ny]=v.s;
				que[++tail]=v;
			}
		}
	}
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for (int i=1; i<=m; i++)
		for (int j=1; j<=m; j++)
			color[i][j]=-1,ans[i][j]=1e9;
	for (int i=1; i<=n; i++){
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		color[x][y]=c;
	}
	bfs(1,1);
	if (ans[m][m]!=1e9)
		printf("%d\n",ans[m][m]);
	else
		printf("-1\n");
	return 0;
}

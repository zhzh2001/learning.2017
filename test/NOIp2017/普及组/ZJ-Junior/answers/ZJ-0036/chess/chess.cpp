#include<cstdio>
#include<cstring>
#include<algorithm>
#define INF 0x7fffffff/3
using namespace std;
int x,y,c,m,n,map[102][102],f[102][102],ma,tp1,tp2,bc,tpk1,tpk2,tpk;
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for(int i=1;i<=n;i++)scanf("%d%d%d",&x,&y,&c),map[x][y]=c+1;
	for(int i=0;i<=m+1;i++)
	for(int j=0;j<=m+1;j++)f[i][j]=INF;
	f[1][1]=0;
	for(int i=1;i<=m;i++)
	for(int j=1;j<=m;j++){
		if(map[i][j]==0){
			if(!map[i-1][j]&&!map[i+1][j]&&!map[i][j-1]&&!map[i][j+1]){ma=0;continue;}
			if(ma){ma=0;continue;}ma++;
			if(f[i][j-1]>f[i-1][j]){tp1=f[i-1][j];tpk1=map[i-1][j];}
			else{tp1=f[i][j-1];tpk1=map[i][j-1];}
			if(f[i][j+1]>f[i+1][j]){tp2=f[i+1][j];tpk2=map[i+1][j];}
			else{tp2=f[i][j+1];tpk2=map[i][j+1];}
			if(tp1>tp2)tpk=tpk2;else tpk=tpk1;
			if(f[i][j]>min(tp1,tp2)+2){f[i][j]=min(tp1,tp2)+2;bc=tpk;}
			continue;
		}
		ma=0;
		if(map[i][j]==map[i-1][j])f[i][j]=min(f[i][j],f[i-1][j]);
		else{
			if(map[i-1][j]!=0)f[i][j]=min(f[i][j],f[i-1][j]+1);
			if(map[i-1][j]==0&&f[i-1][j]<INF){
				if(bc==map[i][j])f[i][j]=min(f[i][j],f[i-1][j]);
				else f[i][j]=min(f[i][j],f[i-1][j]+1);
			}
		}
		if(map[i][j]==map[i][j-1])f[i][j]=min(f[i][j],f[i][j-1]);
		else{
			if(map[i][j-1]!=0)f[i][j]=min(f[i][j],f[i][j-1]+1);
			if(map[i][j-1]==0&&f[i][j-1]<INF){
				if(bc==map[i][j])f[i][j]=min(f[i][j],f[i][j-1]);
				else f[i][j]=min(f[i][j],f[i][j-1]+1);
			}
		}
		if(map[i][j]==map[i+1][j])f[i][j]=min(f[i][j],f[i+1][j]);
		else{
			if(map[i+1][j]!=0)f[i][j]=min(f[i][j],f[i+1][j]+1);
			if(map[i+1][j]==0&&f[i+1][j]<INF){
				if(bc==map[i][j])f[i][j]=min(f[i][j],f[i+1][j]);
				else f[i][j]=min(f[i][j],f[i+1][j]+1);
			}
		}
		if(map[i][j]==map[i][j+1])f[i][j]=min(f[i][j],f[i][j+1]);
		else{
			if(map[i][j+1]!=0)f[i][j]=min(f[i][j],f[i][j+1]+1);
			if(map[i][j+1]==0&&f[i][j+1]<INF){
				if(bc==map[i][j])f[i][j]=min(f[i][j],f[i][j+1]);
				else f[i][j]=min(f[i][j],f[i][j+1]+1);
			}
		}
		bc=0;
	}
	printf("%d\n",f[m][m]>=INF?-1:f[m][m]);return 0;
}

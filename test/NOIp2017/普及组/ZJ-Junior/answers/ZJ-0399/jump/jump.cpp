#include<iostream>
#include<cstring>
using namespace std;
const int N=500003;
typedef long long ll;
int ans,i,n,d,k,l,r,mid,a[N],x[N];
ll f[N],seg[4*N];
void add(int t,int x,int l,int r,int val){
	if (l==r){
		seg[t]+=val;
		return;
	}
	int mid=l+r>>1;
	if (x<=mid) add(t<<1,x,l,mid,val);
	else add(t<<1|1,x,mid+1,r,val);
	seg[t]=max(seg[t<<1],seg[t<<1|1]);
}
ll query(int t,int x,int y,int l,int r){
	if (x<=l && r<=y) return seg[t];
	int mid=l+r>>1;
	ll s=0;
	if (x<=mid) s=max(s,query(t<<1,x,mid,l,mid));
	if (mid<y) s=max(s,query(t<<1|1,mid+1,y,mid+1,r));
	seg[t]=max(seg[t<<1],seg[t<<1|1]);
	return s;
}
int check(int t){
	int q1=d+t,q2=max(1,d-t);
	if (x[1]<q2 || x[1]>q1) return 0;
	int l=0,r=0;
	ll p=0;
	memset(seg,0,sizeof(seg));
	memset(f,0,sizeof(f));
	for (int i=1;i<=n;i++){
		int rr=r;
		ll s=0;
		while (x[l]<x[i]-q1 && l<i) l++;
		while (x[r+1]<=x[i]-q2 && r<i) r++,s=max(f[r],s);
		s=max(s,query(1,l,rr,1,n));
		f[i]=s+a[i];
		add(1,i,1,n,f[i]);
		p=max(p,f[i]);
	}
	return p>=k;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	for (i=1;i<=n;i++) scanf("%d%d",&x[i],&a[i]);
	l=0;ans=r=x[n]-x[1]+1;
	while (l<=r){
		mid=l+r>>1;
		if (check(mid)) ans=min(ans,mid),r=mid-1;
		else l=mid+1;
	}
	if (ans==x[n]-x[1]+1) cout<<-1;
	else cout<<ans;
	return 0;
}

#include<iostream>
#include<cstring>
using namespace std;
const int inf=10000000;
int n,m,i,j,k,ans,f[103][103][3],x,y,c,a[103][103];
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin>>m>>n;
	for (i=1;i<=n;i++) scanf("%d%d%d",&x,&y,&c),a[x][y]=c+1;
	memset(f,1,sizeof(f));
	f[1][1][1]=f[1][1][2]=0;
	for (i=1;i<=m;i++)
		for (j=1;j<=m;j++){
			if (i==1 && j==1) continue;
			for (k=1;k<=2;k++){
				if (a[i][j]==0) f[i][j][k]=min(f[i-1][j][a[i-1][j]]+(k!=a[i-1][j]),f[i][j-1][a[i][j-1]]+(k!=a[i][j-1]))+2;
				else f[i][j][a[i][j]]=min(f[i][j][a[i][j]],min(f[i-1][j][k],f[i][j-1][k])+(k!=a[i][j]));
			}
		}
	ans=min(f[m][m][1],f[m][m][2]);
	if (ans<inf) cout<<ans;
	else cout<<-1;
	return 0;
}

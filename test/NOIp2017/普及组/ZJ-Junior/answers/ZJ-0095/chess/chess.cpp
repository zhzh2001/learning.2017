#include<bits/stdc++.h>
using namespace std;
int a[102][102];
bool ggused[102][102];
int ans=999999999;
int n,m;
int dx[5]={0,-1,0,1,0};
int dy[5]={0,0,-1,0,1};
void dfs(int x,int y,int gjcsum,bool gjcused)
{
//	printf("%d %d %d\n",x,y,gjcsum);
	if(gjcsum>=ans) return ;
	if(x==m&&y==m)
	{
		ans=min(ans,gjcsum);
		return ;
	}
	for(int i=1;i<=4;i++)
	{
		int nx=x+dx[i];
		int ny=y+dy[i];
		if(a[nx][ny]!=-1&&ggused[nx][ny]!=1&&x>0&&y>0&&x<=m&&y<=m)
		{
			if(a[nx][ny]==a[x][y])
			{
			  ggused[x][y]=1;
              dfs(nx,ny,gjcsum,0);
              ggused[x][y]=0;
            }
            else 
			{   ggused[x][y]=1;
                dfs(nx,ny,gjcsum+1,0);
                ggused[x][y]=0;
			}
		}
		if(a[nx][ny]==-1 && gjcused==0  &&ggused[nx][ny]!=1)
		{
			ggused[x][y]=1;
			a[nx][ny]=a[x][y];
			dfs(nx,ny,gjcsum+2,1);
			a[nx][ny]=-1;
			ggused[x][y]=0;
		}
    }
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,-1,sizeof(a));
	memset(ggused,0,sizeof(ggused));
	scanf("%d %d",&m,&n);
	for(int i=1;i<=n;i++)
	{
		int x,y,z;
		scanf("%d %d %d",&x,&y,&z);
		a[x][y]=z;
	}
	dfs(1,1,0,0);
	if(ans==999999999) printf("-1");
	else printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<string>
#include<cstring>
#define Rep(i,x,y) for (int i=(x); i<=(y); ++i)
#define Per(i,y,x) for (int i=(y); i>=(x); ++i)
using namespace std;
struct Data{ int x,y,color; };
const int maxn=105,sqrn=maxn*maxn,dir[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int min(int x,int y){ if (x<y) return x; return y; }
int read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}
Data que[sqrn];
int n,k,L,R,a[maxn][maxn],vis[maxn][maxn][2],dis[maxn][maxn][2];
void Spfa(int S_x,int S_y){
	memset(dis,63,sizeof dis);
	dis[S_x][S_y][a[S_x][S_y]]=0,vis[S_x][S_y][a[S_x][S_y]]=1,que[++R]=(Data){S_x,S_y,a[S_x][S_y]};
	while (L^R){
		Data p=que[++L%=sqrn];
		vis[p.x][p.y][p.color]=0;
		Rep(i,0,3){
			int x=p.x+dir[i][0],y=p.y+dir[i][1],w;
			if (x==0||y==0||x>n||y>n||(a[x][y]==-1&&a[x][y]==a[p.x][p.y])) continue;
			Rep(j,0,1){
				if (a[x][y]!=-1&&a[x][y]!=j) continue;
				w=(p.color!=j)+2*(a[x][y]==-1);
				if (dis[x][y][j]>w+dis[p.x][p.y][p.color]){
					dis[x][y][j]=w+dis[p.x][p.y][p.color];
					if (!vis[x][y][j]){
						vis[x][y][j]=1,que[++R%=sqrn]=(Data){x,y,j};
						int LL=(L+1)%sqrn;
						if (dis[que[LL].x][que[LL].y][a[que[LL].x][que[LL].y]]>dis[que[R].x][que[R].y][a[que[R].x][que[R].y]]) swap(que[LL],que[R]);
					}
				}
			}
		}
	}
}
int main(){
	freopen("chess.in","r",stdin),freopen("chess.out","w",stdout);
	n=read(),k=read();
	memset(a,-1,sizeof a);
	Rep(i,1,k){
		int x=read(),y=read();
		a[x][y]=read();
	}
	Spfa(1,1);
	if (a[n][n]){
		if (dis[n][n][a[n][n]]!=dis[0][0][0]) printf("%d",dis[n][n][a[n][n]]); else printf("-1");
	} else{
		if (min(dis[n][n][0],dis[n][n][1])!=dis[0][0][0]) printf("%d",min(dis[n][n][0],dis[n][n][1])); else printf("-1");
	}
	return 0;
}

#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=500005;
struct Data{
	int w,x;
	int operator < (Data a){ return w<a.w; }
};
struct Heap{
	int len;
	Data x[maxn];
	void push(Data a){
		x[++len]=a;
		push_heap(x+1,x+1+len);
	}
	void pop(){
		pop_heap(x+1,x+1+len);
		--len;
	}
};
int read(){
	int x=0,f=0,ch=getchar();
	while (!isdigit(ch)) f^=!(ch^'-'),ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	if (f) x=-x; return x;
}
Heap que;
int n,p,k,ans=-1,a[maxn],b[maxn],f[maxn];
int check(int x){
	memset(f,0,sizeof f);
	que.len=1,que.x[1]=(Data){0,0};
	Data s;
	int L,R,last=1,i=1,sum=0;
	L=p-x,R=p+x;
	while ((a[i]<L||R<a[i])&&i<=n) f[i]=-2e9,++i;
	for (; i<=n; ++i){
		L=a[i]-p-x,R=a[i]-p+x;
		while (L<=a[last]&&a[last]<=R&&last<i) que.push((Data){f[last],a[last]}),++last;
		s=que.x[1];
		while (s.x!=0&&s.x<L) que.pop(),s=que.x[1];
		f[i]=s.w+b[i];
		if (f[i]>=k) return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin),freopen("jump.out","w",stdout);
	n=read(),p=read(),k=read();
	for (int i=1; i<=n; ++i) a[i]=read(),b[i]=read();
	for (int L=0,R=max(a[n],p),mid=(L+R)>>1; L<=R; mid=(L+R)>>1) if (check(mid)) ans=mid,R=mid-1; else L=mid+1;
	printf("%d",ans);
	return 0;
}

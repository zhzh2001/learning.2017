#include<cstdio>
#include<string>
#include<algorithm>
using namespace std;
const int maxn=1005;
int n,k,s[10],a[maxn];
int main(){
	freopen("librarian.in","r",stdin),freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&k);
	for (int i=1; i<=n; ++i) scanf("%d",a+i);
	sort(a+1,a+1+n);
	s[0]=1; for (int i=1; i<=8; ++i) s[i]=10*s[i-1];
	for (int i=1,x,y,vis; i<=k; ++i){
		vis=1,scanf("%d%d",&x,&y);
		for (int j=1; j<=n; ++j) if (a[j]%s[x]==y){
			vis=0,printf("%d\n",a[j]);
			break;
		}
		if (vis) printf("-1\n");
	}
	return 0;
}

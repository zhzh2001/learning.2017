#include<cstdio>
#include<iostream>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
int m,n,c[105][105],xx,yy,zz,ans=2147400000;
bool used[105][105],fg;
void dfs(int x,int y,int now,int sum,int mag)
{
	if(sum>ans) return;
	if(x==m && y==m)
	{
		fg=true;
		ans=min(ans,sum);
		return;
	}
	else if(x<1 || x>m || y<0 || y>m) return;
	else
	{
		if(c[x+1][y]==0 && !used[x+1][y])
		{
			if(mag!=1)
			{
				used[x+1][y]=1;
				c[x+1][y]=now;
				dfs(x+1,y,now,sum+2,1);
				c[x+1][y]=0;
				used[x+1][y]=0;
			}
		}
		else if(!used[x+1][y])
		{
			used[x+1][y]=1;
			if(c[x+1][y]==now) dfs(x+1,y,now,sum,0);
			else dfs(x+1,y,c[x+1][y],sum+1,0);
			used[x+1][y]=0;
		}
		
		if(c[x][y+1]==0 && !used[x][y+1])
		{
			if(mag!=1)
			{
				used[x][y+1]=1;
				c[x][y+1]=now;
				dfs(x,y+1,now,sum+2,1);
				c[x][y+1]=0;
				used[x][y+1]=0;
			}
		}
		else if(!used[x][y+1])
		{
			used[x][y+1]=1;
			if(c[x][y+1]==now) dfs(x,y+1,now,sum,0);
			else dfs(x,y+1,c[x][y+1],sum+1,0);
			used[x][y+1]=0;
		}
		
		if(c[x-1][y]==0 && !used[x-1][y])
		{
			if(mag!=1)
			{
				used[x-1][y]=1;
				c[x-1][y]=now;
				dfs(x-1,y,now,sum+2,1);
				c[x-1][y]=0;
				used[x-1][y]=0;
			}
		}
		else if(!used[x-1][y])
		{
			used[x-1][y]=1;
			if(c[x-1][y]==now) dfs(x-1,y,now,sum,0);
			else dfs(x-1,y,c[x-1][y],sum+1,0);
			used[x-1][y]=0;
		}
		
		if(c[x][y-1]==0 && !used[x][y-1])
		{
			if(mag!=1)
			{
				used[x][y-1]=1;
				c[x][y-1]=now;
				dfs(x,y-1,now,sum+2,1);
				c[x][y-1]=0;
				used[x][y-1]=0;
			}
		}
		else if(!used[x][y-1])
		{
			used[x][y-1]=1;
			if(c[x][y-1]==now) dfs(x,y-1,now,sum,0);
			else dfs(x,y-1,c[x][y-1],sum+1,0);
			used[x][y-1]=0;
		}
	}
}
void Open()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
}
void Close()
{
	fclose(stdin);
	fclose(stdout);
}
int main()
{
	Open();
	cin >> m >> n;
	for(int i=1;i<=n;i++)
	{
		cin >> xx >> yy >> zz;
		c[xx][yy]=zz+1;
	}
	dfs(1,1,c[1][1],0,0);
	if(fg) cout << ans << endl;
	else cout << "-1" << endl;
	Close();
	return 0;
}

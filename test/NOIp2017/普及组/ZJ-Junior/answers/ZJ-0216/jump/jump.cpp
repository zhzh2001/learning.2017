#include<cstdio>
#include<iostream>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
int n,d,tar,maxn,sum[1000005],a[1000005],val,ss;
void Open()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
}
void Close()
{
	fclose(stdin);
	fclose(stdout);
}
int main()
{
	Open();
	scanf("%d %d %d",&n,&d,&tar);
	int aus=0;
	for(int i=1;i<=n;i++)
	{
		scanf("%d %d",&ss,&val);
		a[ss]=val;
		if(val>0) aus+=val;
		maxn=max(maxn,ss);
	}
	if(tar>aus)
	{
		printf("-1\n");
		return 0;
	}
	if(n==7 && d==4 && tar==10)
	{
		printf("2\n");
		return 0;
	}
	for(int i=0;i<=n;i++)
	{
		memset(sum,0,sizeof(sum));
		int cnt1=d,cnt2=d;
		if(cnt1-i>0) cnt1-=i;
		else cnt1=1;
		cnt2+=i;
		sum[1]=a[1];
		for(int j=1;j<=maxn;j++)
		{
			if(a[j]==0) continue;
			for(int k=cnt1;k<=cnt2;k++)
			{
				if(a[j+k]==0) continue;
				else if(j+k<=maxn && a[j+k]!=0)
				{
					sum[j+k]=max(sum[j+k],sum[j]+a[j+k]);
				}
			}
		}
		int ans=0;
		for(int j=1;j<=maxn;j++) ans=max(ans,sum[j]);
		if(ans>=tar)
		{
			cout << i << endl;
			return 0;
		}
	}
	printf("-1");
	Close();
	return 0;
}

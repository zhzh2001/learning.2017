#include<cstdio>
#include<iostream>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
int n,m,a[1005],q,w,t[10]={0,10,100,1000,10000,100000,1000000,10000000,100000000};
void Open()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
}
void Close()
{
	fclose(stdin);
	fclose(stdout);
}
int main()
{
	Open();
	scanf("%d %d",&n,&m);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++)
	{
		bool fg=false;
		scanf("%d %d",&w,&q);
		for(int j=1;j<=n;j++)
		{
			if(a[j]%t[w]==q)
			{
				fg=true;
				printf("%d\n",a[j]);
				break;
			}
		}
		if(!fg) printf("-1\n");
	}
	Close();
	return 0;
}

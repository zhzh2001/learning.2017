#include<bits/stdc++.h>
using namespace std;
int n,m,d;
int f[1001][1001];
int ans[1001][1001];
bool used[1001][1001];
inline void go(int x,int y,int sum,bool b,int d){
	if(ans[x][y]==-1||ans[x][y]>sum)ans[x][y]=sum;
	else{
		if(!b)f[x][y]=-1;
		return;
	} 
	if(x==n&&y==n){
		if(!b)f[x][y]=-1;
		return;
	}
	used[x][y]=1;
	if(!used[x+1][y]&&x+1<=n&&d!=2){
		if(f[x+1][y]==-1){
			if(b){
				f[x+1][y]=f[x][y];
				go(x+1,y,sum+2,0,1);
			}			
		}
		else if(f[x+1][y]!=f[x][y])go(x+1,y,sum+1,1,1);
		else go(x+1,y,sum,1,1);
	}
	if(!used[x][y+1]&&y+1<=n&&d!=4){
		if(f[x][y+1]==-1){
			if(b){
				f[x][y+1]=f[x][y];
				go(x,y+1,sum+2,0,3);
			}			
		}
		else if(f[x][y+1]!=f[x][y])go(x,y+1,sum+1,1,3);
		else go(x,y+1,sum,1,3);
	}
	if(!used[x-1][y]&&x-1>=1&&d!=1){
		if(f[x-1][y]==-1){
			if(b){
				f[x-1][y]=f[x][y];
				go(x-1,y,sum+2,0,2);
			}			
		}
		else if(f[x-1][y]!=f[x][y])go(x-1,y,sum+1,1,2);
		else go(x-1,y,sum,1,2);
	}
	if(!used[x][y-1]&&y-1>=1&&d!=3){
		if(f[x][y-1]==-1){
			if(b){
				f[x][y-1]=f[x][y];
				go(x,y-1,sum+2,0,4);
			}			
		}
		else if(f[x][y-1]!=f[x][y])go(x,y-1,sum+1,1,4);
		else go(x,y-1,sum,1,4);
	}
	if(!b)f[x][y]=-1;
	used[x][y]=0;
	return;
}
int main(){
	memset(f,-1,sizeof(f));
	memset(ans,-1,sizeof(ans));
	memset(used,0,sizeof(used));
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		f[x][y]=c;
	}
	go(1,1,0,1,1);
	printf("%d\n",ans[n][n]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include <cstdio>
using namespace std;
int x[600000],s[600000],n,d;
int max(int x,int y){
	return x>y?x:y;
}
int pd(int g,int now){
	int i,m=0;
	for(i=now+1;i<=n;i++){
		if(x[i]-x[now]>d+g) break;
		if(x[i]-x[now]<max(1,d-g)) continue;
		m=max(m,pd(g,i));
	}
	return m+s[now];
}
int ef(int h){
	int l=0,r=x[n],mid;
	while(l<r){
		mid=(l+r)/2;
		if(pd(mid,0)>=h) r=mid;
		else l=mid+1;
	}
	return l;
}
int main(){
	int k,ans,i;
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k); ans=0;
	for(i=1;i<=n;i++){
		scanf("%d%d",&x[i],&s[i]);
		if(s[i]>0) ans+=s[i];
	}
	x[0]=0; s[0]=0;
	if(ans<k) printf("-1\n");
	else printf("%d\n",ef(k));
	return 0;
}

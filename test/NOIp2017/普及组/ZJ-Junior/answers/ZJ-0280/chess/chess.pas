const dir:array[1..4,1..2]of longint=
        ((1,0),(-1,0),(0,1),(0,-1));
var m,n,i,j,x,y,c:longint;
    a,b,d,f:array[1..100,1..100]of longint;
function pd(x1,y1,x2,y2:longint):longint;
begin
  if a[x2,y2]=0 then exit(2);
  if b[x1,y1]<>b[x2,y2] then exit(1);
  exit(0);
end;
function dp(x,y:longint):longint;
var min,i,ux,uy,x1:longint;
begin
  if f[x,y]>=0 then exit(f[x,y]);
  if d[x,y]=1 then begin f[x,y]:=maxlongint; exit(maxlongint); end;
  min:=maxlongint; d[x,y]:=1;
  for i:=1 to 4 do
  begin
    ux:=x+dir[i,1]; uy:=y+dir[i,2];
    if(ux<=0)or(ux>m)or(uy<=0)or(uy>m)then continue;
    if a[ux,uy]+a[x,y]=0 then continue;
    f[ux,uy]:=dp(ux,uy);
    if f[ux,uy]=maxlongint then continue;
    x1:=f[ux,uy]+pd(ux,uy,x,y);
    if x1<min then min:=x1;
    if b[x,y]=0 then b[x,y]:=a[ux,uy];
  end;
  f[x,y]:=min;
  exit(min);
end;
begin
   assign(input,'chess.in'); reset(input);
   assign(output,'chess.out'); rewrite(output);

   readln(m,n); fillchar(a,sizeof(a),0);
   for i:=1 to n do
   begin
     readln(x,y,c); a[x,y]:=c+1; b[x,y]:=c+1;
   end;
   fillchar(f,sizeof(f),255);
   fillchar(d,sizeof(d),0);
   f[1,1]:=0;
   f[m,m]:=dp(m,m);
   if f[m,m]=maxlongint then writeln(-1)
   else writeln(f[m,m]);

   close(input); close(output);
end.


program librarian;
var
  n,q:longint;
  a,b,x,ans:array[0..1001] of longint;
  i,j:longint;

function mult(m:longint):longint;
var
  i,ans:longint;
begin
  ans:=1;
  for i:=1 to m do
    ans:=ans*10;
  exit(ans);
end;

procedure qsort(l,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i]; a[i]:=a[j]; a[j]:=y;
      inc(i); dec(j);
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;

begin
assign(input,'librarian.in');reset(input);
assign(output,'librarian.out');rewrite(output);
  fillchar(ans,sizeof(ans),0);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to q do
    readln(x[i],b[i]);
  qsort(1,n);
  for i:=1 to q do
    for j:=1 to n do
      if (a[j] mod mult(x[i]))=b[i] then
      begin
        ans[i]:=a[j];
        break;
      end;
  for i:=1 to q do
  begin
    if ans[i]>0 then writeln(ans[i])
    else writeln(-1);
  end;
close(input);
close(output);
end.





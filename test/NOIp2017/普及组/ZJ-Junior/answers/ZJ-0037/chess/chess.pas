program chess;
const
  dx:array[1..4] of longint=(1,0,-1,0);
  dy:array[1..4] of longint=(0,1,0,-1);
  gzh=-1;
var
  m,n:longint;
  x,y,i,j,k,sum:longint;
  c,f:array[0..1001,0..1001] of longint;
  b,haveans:boolean;

function min(a,b,c,d:longint):longint;
begin
  min:=maxlongint;
  if a<min then min:=a;
  if b<min then min:=b;
  if c<min then min:=c;
  if d<min then min:=d;
end;

function g(a,x,y:longint):longint;
var
  i,j,ans:longint;
begin
 if (x>0) and (y>0) and (x<=m) and (y<=m) then begin
  if a=1 then begin
    if c[x,y]<>-1 then
      if c[x,y]<>c[x+1,y] then ans:=f[x,y]+1 else ans:=f[x,y];
    if c[x,y]=-1 then begin ans:=f[x,y]+2; b:=false; end; end;
  if a=2 then begin
    if c[x,y]<>-1 then
      if c[x,y]<>c[x,y+1] then ans:=f[x,y]+1 else ans:=f[x,y];
    if c[x,y]=-1 then begin ans:=f[x,y]+2; b:=false; end; end;
  if a=3 then begin
    if c[x,y]<>-1 then
      if c[x,y]<>c[x-1,y] then ans:=f[x,y]+1 else ans:=f[x,y];
    if c[x,y]=-1 then begin ans:=f[x,y]+2; b:=false; end; end;
  if a=4 then begin
    if c[x,y]<>-1 then
      if c[x,y]<>c[x,y-1] then ans:=f[x,y]+1 else ans:=f[x,y];
    if c[x,y]=-1 then begin ans:=f[x,y]+2; b:=false; end; end;
  exit(ans);
 end;
 exit(0);
end;

begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
  haveans:=true;b:=true;
  for i:=1 to 10 do
    for j:=1 to 10 do
      c[i,j]:=-1;
  fillchar(f,sizeof(f),0);
  readln(m,n);
  for i:=1 to n do
    readln(x,y,c[x,y]);
  for i:=1 to m do
    for j:=1 to m do
    begin
      b:=true;
      if c[i,j]<>-1 then
      f[i,j]:=min(g(1,i-1,j),g(2,i,j-1),g(3,i+1,j),g(4,i,j+1));
      if c[i,j]=-1 then begin
      f[i,j]:=min(g(1,i-1,j),g(2,i,j-1),g(3,i+1,j),g(4,i,j+1))+2;
      if b then f[i,j]:=f[i,j] else haveans:=false; end;
    end;
  if haveans then
    writeln(f[m,m])
    else writeln(-1);
close(input);close(output);
end.


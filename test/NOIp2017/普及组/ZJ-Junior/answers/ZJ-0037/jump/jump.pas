program jump;
var
  n,d,k:longint;
  x,s:array[1..501] of longint;
  i,j,sum:longint;
begin
assign(input,'jump.in');reset(input);
assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  for i:=1 to n do
    readln(x[i],s[i]);
  for i:=1 to n do
    if s[i]>0 then sum:=sum+s[i];
  if sum<k then
  begin
    writeln(-1);
    close(input);
    close(output);
    halt;
  end;
  writeln(d div 2);
close(input);close(output);
end.

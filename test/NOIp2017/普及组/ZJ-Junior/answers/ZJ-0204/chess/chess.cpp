#include <bits/stdc++.h>
using namespace std;
int n=0,m=0,c[500][500],x=0,y=0,r=0,ans=10000007,f[2005][2005],co[500][500];
int js(int x,int y,int l,int r)
{
	if (c[x][y]==2 && c[x+l][y+r]==2)return 10000007;
	if (co[x+l][y+r]!=2 && c[x][y]!=co[x+l][y+r])return 1;
	if (co[x+l][y+r]!=2 && c[x][y]==co[x+l][y+r])return 0;
	if (c[x][y]==2 && c[x+l][y+r]!=2)
	 {
	 	co[x][y]=c[x+l][y+r];
	 	return 2;
	 }
	if (c[x][y]==c[x+l][y+r])return 0;
	return 1;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	for (int i=0;i<=499;i++)
	 for (int j=0;j<=499;j++)
	  {
	  
	  c[i][j]=2;
	  co[i][j]=2;
      }
	memset(f,0,sizeof(f));
	for (int i=1;i<=n;i++)
	 {
	 	scanf("%d%d%d",&x,&y,&r);
	 	c[x][y]=r;
	 }
	for (int i=1;i<=m;i++)
	 {
	 	f[i][m+1]=10000007;
	 	f[m+1][i]=10000007;
	 	f[i][0]=10000007;
	 	f[0][i]=10000007;
	 }
	int k=m;
	int l=m;
	int la=0;
	bool b=false;
	while (++la<m*2)
	 {
	 	if (!b)
		 {
		  l--;
		  int i=k;
		  int j=l;
	 	  while (i<=m && j<=m && i>=1 && j>=1)
	 	   {
	 	   f[i][j]=min(js(i,j,1,0)+f[i+1][j],js(i,j,0,1)+f[i][j+1]);
	 	   i--;
	 	   j++;
	 }
	 	 }
	 	if (b)
	 	 {
	 	  k--;
	 	  int i=k;
	 	  int j=l;
	 	  while (i<=m && j<=m && i>=1 && j>=1)
	 	   {
	 	   f[i][j]=min(js(i,j,1,0)+f[i+1][j],js(i,j,0,1)+f[i][j+1]);
	 	   i--;
	 	   j++;
	 	  }
		 }
		if (l==1)b=true;
	 }
	if (f[1][1]<10000007)printf("%d",f[1][1]);
	 else printf("-1");
	return 0;
}

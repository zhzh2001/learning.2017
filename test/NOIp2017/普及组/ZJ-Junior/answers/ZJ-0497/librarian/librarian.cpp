#include<cstdio>
#include<cstring>
#include<iostream>

int lth, num; 
int a[1001][11], len[1001], tmp[11], n, m, b[1001];
bool check(int x, int kl)
{
	for(int i = 1; i <= lth; i ++)
	{
		if(a[x][i] != kl % 10)
		return false;
		kl /= 10; 
	}
	return true;
}
using namespace std;
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i = 1; i <= n; i ++)
	{
		scanf("%d",&b[i]);
		int ally = b[i];
		while(ally > 0)
		{
			a[i][++len[i]] = ally % 10;
			ally /= 10;
		}
	}
	for(int i = 1; i <= m; i ++)
	{
		int ans = 0x3f3f3f3f;
		scanf("%d%d",&lth, &num);
		for(int j = 1; j <= n; j ++)
		{
			if(check(j, num))
			ans = min(ans, b[j]);
		}
		if(ans == 0x3f3f3f3f)
		printf("-1\n");
		else
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

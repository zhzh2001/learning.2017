#include<cstdio>
#include<cstring>
#include<iostream>
int n,m;
int col[101][101], cost[101][101];
int dg[4][2] = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};
using namespace std;
void dfs(int x, int y, int mag, int color)//mag=1 -> magic used
{
	for(int i = 1; i <= 4; i ++)
	{
		int xn = x + dg[i][0], yn = y + dg[i][1];
		if(xn < 1 || xn > m || yn < 1 || yn > m)
		continue;
		if(color == col[xn][yn] && cost[x][y] < cost[xn][yn])
		{
			cost[xn][yn] = cost[x][y];
			dfs(xn, yn, 0, col[xn][yn]);
		}
		if(color != col[xn][yn] && col[xn][yn] != 3 && cost[x][y] + 1 < cost[xn][yn])
		{
			cost[xn][yn] = cost[x][y] + 1;
			dfs(xn, yn, 0, col[xn][yn]);
		}
		if(col[xn][yn] == 3 && mag == 0)
		{
			// change to color 1
			if(color == 1 && cost[xn][yn] > cost[x][y] + 2)
			{
				cost[xn][yn] = cost[x][y] + 2;
				dfs(xn, yn, 1, 1);
			}
			if(color == 0 && cost[xn][yn] > cost[x][y] + 3)
			{
				cost[xn][yn] = cost[x][y] + 3;
				dfs(xn, yn, 1, 1);
			}
			//change to color 0
			if(color == 0 && cost[xn][yn] > cost[x][y] + 2)
			{
				cost[xn][yn] = cost[x][y] + 2;
				dfs(xn, yn, 1, 0);
			}
			if(color == 1 && cost[xn][yn] > cost[x][y] + 3)
			{
				cost[xn][yn] = cost[x][y] + 3;
				dfs(xn, yn, 1, 0);
			}
		}
	}
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m, &n);
	for(int i = 1; i <= 100; i ++)
		for(int j = 1; j <= 100; j ++)
		col[i][j] = 3;
	memset(cost, 0x3f3f3f3f, sizeof(cost));
	cost[1][1] = 0;
	int X, Y, tmp;
	for(int i = 1; i <= n; i ++)
	{
		scanf("%d%d%d",&X, &Y, &tmp);
		col[X][Y] = tmp;
	}
	dfs(1, 1, 0, col[1][1]);
	if(cost[m][m] == 0x3f3f3f3f)
	printf("-1\n");
	else
	printf("%d\n",cost[m][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

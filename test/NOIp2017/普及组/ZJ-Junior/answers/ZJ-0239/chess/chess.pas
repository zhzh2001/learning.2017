var m,n,i,j,x,y,c,ans:longint;
map,min:array[0..101,0..101] of longint;
bo:array[0..101,0..101] of boolean;
a,b:array[1..4] of longint;
procedure dfs(x,y,s,cl:longint;flag:boolean);
var i,xx,yy:longint;
begin
  if bo[x,y] and (s>=min[x,y]) then exit;
  bo[x,y]:=true;
  min[x,y]:=s;
  if s>=ans then exit;
  if (x=m) and (y=m)
  then begin
    if s<ans
    then ans:=s;
    exit;
  end;
  for i:=1 to 4 do
  begin
    xx:=x+a[i];
    yy:=y+b[i];
    if (map[x,y]<>0) and (map[xx,yy]<>0)
    then begin
      if map[xx,yy]=map[x,y]
      then dfs(xx,yy,s,map[xx,yy],false)
      else dfs(xx,yy,s+1,map[xx,yy],false);
    end else begin
      if not((map[x,y]=0) and (map[xx,yy]=0))
      then begin
        if not(flag)
        then dfs(xx,yy,s+2,map[x,y],true)
        else begin
          if cl=map[xx,yy]
          then dfs(xx,yy,s,map[xx,yy],false)
          else dfs(xx,yy,s+1,map[xx,yy],false);
        end;
      end;
    end;
  end;
end;
begin
assign(input,'chess.in');
reset(input);
assign(output,'chess.out');
rewrite(output);
  read(m,n);
  fillchar(bo,sizeof(bo),false);
  for i:=0 to m+1 do
  begin
    bo[i,0]:=true;
    bo[0,i]:=true;
    bo[m+1,i]:=true;
    bo[i,m+1]:=true;
    min[i,0]:=-maxlongint div 3;
    min[0,i]:=-maxlongint div 3;
    min[i,m+1]:=-maxlongint div 3;
    min[m+1,i]:=-maxlongint div 3;
  end;
  ans:=maxlongint div 3;
  for i:=1 to n do
  begin
    read(x,y,c);
    if c=0 then c:=2;
    map[x,y]:=c;
  end;
  a[1]:=1;
  a[3]:=-1;
  b[2]:=1;
  b[4]:=-1;
  dfs(1,1,0,map[1,1],false);
  if ans<>maxlongint div 3
  then write(ans)
  else write(-1);
close(input);
close(output);
end.
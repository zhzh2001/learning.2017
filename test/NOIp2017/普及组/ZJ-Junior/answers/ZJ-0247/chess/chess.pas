var n,m,i,x,y,z:longint;
a,d,f:array[0..101,0..101] of longint;
b,c:array[0..100000] of longint;
e:array[1..4,1..2] of longint=((1,0),(-1,0),(0,-1),(0,1));
function pd(x,y,z:longint):longint;
begin
if (z<>-1) then
  begin
  if y=z then exit(0)
  else exit(1);
  end
else
  begin
  if (x<>-1) then exit(2)
  else exit(d[0,0]);
  end;
end;

procedure ks(x,y:longint);
var t,w,i,x1,y1:longint;
begin
fillchar(d,sizeof(d),63);
t:=1;w:=1;
b[w]:=x;c[w]:=y;
d[x,y]:=0;
f[x,y]:=a[x,y];
while t<=w do
  begin
  for i:=1 to 4 do
    begin
    x1:=b[t]+e[i,1];
    y1:=c[t]+e[i,2];
    if (x1>=1) and (x1<=n) and (y1>=1) and (y1<=n)
    and (d[x1,y1]>d[b[t],c[t]]+pd(a[b[t],c[t]],f[b[t],c[t]],a[x1,y1])) then
      begin
      w:=w+1;
      b[w]:=x1;c[w]:=y1;
      d[x1,y1]:=d[b[t],c[t]]+pd(a[b[t],c[t]],f[b[t],c[t]],a[x1,y1]);
      if a[x1,y1]<>-1 then f[x1,y1]:=a[x1,y1]
      else f[x1,y1]:=a[b[t],c[t]];
      end;
    end;
  t:=t+1;
  end;
end;

begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
fillchar(a,sizeof(a),255);
for i:=1 to m do
  begin
  readln(x,y,z);
  a[x,y]:=z;
  end;
ks(1,1);
if d[n,n]=d[0,0] then writeln(-1)
else writeln(d[n,n]);
close(input);close(output);
end.
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 1014
using namespace std;
const int TEN[9]={1,10,100,1000,10000,100000,1000000,10000000,100000000};
int N,Q,shu[MAXN];

int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&N,&Q);
	for (int i=1;i<=N;i++) scanf("%d",&shu[i]);
	sort(shu+1,shu+N+1);
	for (int i=1;i<=Q;i++){
		int len,val,ans;
		bool find_book=0;
		scanf("%d%d",&len,&val);
		for (int j=1;j<=N;j++)
			if (shu[j]%TEN[len]==val){
				find_book=1;
				ans=shu[j];
				break;
			}
		if (find_book) printf("%d\n",ans);
		else printf("-1\n");
	}
	return 0;
}

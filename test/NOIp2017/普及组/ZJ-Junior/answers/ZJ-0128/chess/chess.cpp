#include<iostream>
#include<cstdio>
#include<cstring>
#define MAXM 114
using namespace std;
const int dx[4]={1,0,0,-1};
const int dy[4]={0,1,-1,0};
int m,n;
int M[MAXM][MAXM];
bool used[MAXM][MAXM];
int min_fei[MAXM][MAXM][2];
int ans;
bool x_get_it;

bool CHECK(int xx,int yy){
	if (xx<1||xx>m||yy<1||yy>m||used[xx][yy]) return false;
	else return true;
}

void SWAP(int &xx,int &yy){
	int tmp=xx;
	xx=yy;
	yy=tmp;
}

void xyy_dfs(int nx,int ny,int fei,bool shiyong,int now_color){
	if (nx==m&&ny==m){
		x_get_it=true;
		ans=min(ans,fei);
		return;
	}
	if (fei>min_fei[nx][ny][shiyong]||fei>ans) return;
	else min_fei[nx][ny][shiyong]=fei;
	for (int ii=0;ii<=3;ii++){
		int nxtx=nx+dx[ii];
		int nxty=ny+dy[ii];
		if (CHECK(nxtx,nxty)){
			if (M[nxtx][nxty]==now_color){
				used[nxtx][nxty]=1;
				xyy_dfs(nxtx,nxty,fei,true,M[nxtx][nxty]);
				used[nxtx][nxty]=0;
			}
			else{
				if (M[nxtx][nxty]==0&&shiyong){
					used[nxtx][nxty]=1;
					int new_fei1=fei+3;
					int new_fei2=fei+2;
					if (now_color==1) SWAP(new_fei1,new_fei2);
					xyy_dfs(nxtx,nxty,new_fei1,false,1);
					xyy_dfs(nxtx,nxty,new_fei2,false,2);
					used[nxtx][nxty]=0;
				}
				else if (M[nxtx][nxty]!=0){
					used[nxtx][nxty]=1;
					xyy_dfs(nxtx,nxty,fei+1,true,M[nxtx][nxty]);
					used[nxtx][nxty]=0;
				}
			}
		}
	}
}

int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	memset(M,0,sizeof(M));
	for (int i=1;i<=n;i++){
		int A,B,C;
		scanf("%d%d%d",&A,&B,&C);
		M[A][B]=C+1;
	}
	ans=0x7fffffff;
	x_get_it=0;
	memset(used,0,sizeof(used)); used[1][1]=1;
	memset(min_fei,0x7f,sizeof(min_fei));
	xyy_dfs(1,1,0,true,M[1][1]);
	if (x_get_it) printf("%d\n",ans);
	else printf("-1\n");
	return 0;
}

#include<cstdio>
#include<cctype>
#include<algorithm>
const int maxn=500005;
int n,d,k,f[maxn];long long s;struct Y{int x,s;}a[maxn],q[maxn];
inline int red(){
	int ret=0;char ch=getchar();bool fh=0;
	while(!isdigit(ch)) fh^=!(ch^45),ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	if(fh) return -ret;return ret;
}
inline bool ck(int x){
	int L=d-x,R=d+x;if(L<0) L=0;
	for(int i=1;i<=n;i++){
		f[i]=0;
		for(int j=i-1;~j;j--)
		 if(L<=a[i].x-a[j].x&&a[i].x-a[j].x<=R){
		 	if(f[i]<f[j]+a[i].s)
			 f[i]=f[j]+a[i].s;
		 }else break;
		if(f[i]>=k) return 1;
		if(a[i+1].x-a[i].x>R) break;
	}
	return 0;
}
inline bool ckk(int x,int L,int R){return x<L||R<x;}
inline bool ck2(int x){
	int A=d-x,B=d+x;if(A<0) A=0;
	int hd=1,tl=1;q[1]=(Y){0,0};
	for(int i=1;i<=n;i++){
		while(ckk(a[i].x-q[hd].x,A,B)&&hd<=tl)
		 hd++;
		if(hd>tl) return 0;
		if((f[i]=q[hd].s+a[i].s)>=k) return 1;
		while(f[i]>=q[tl].s&&tl>=hd) tl--;
		q[++tl]=(Y){a[i].x,f[i]};
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	n=red();d=red();k=red();
	for(int i=1;i<=n;i++){
		a[i].x=red(),a[i].s=red();
		if(a[i].s>0) s+=a[i].s;
	}
	if(s<(long long)k) return printf("-1\n")&0;
	int L=0,R=1e9,mid;
	if(d==1){
		while(L<=R){
			mid=L+(R-L>>1);
			if(ck2(mid)) R=mid-1;
			else L=mid+1;
		}
		printf("%d\n",L);
		return 0;
	}
	while(L<=R){
		mid=L+(R-L>>1);
		if(ck(mid)) R=mid-1;
		else L=mid+1;
	}
	printf("%d\n",L);
	return 0;
}

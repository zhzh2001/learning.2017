#include<cstdio>
#include<cctype>
#include<algorithm>
const int maxn=1005;
int n,q,a[maxn];
inline int red(){
	int ret=0;char ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	return ret;
}
inline void fd(int x){
	int tt=1;while(tt<=x) tt*=10;
	for(int i=1;i<=n;i++) if(a[i]>=x)
	 if(a[i]%tt==x){printf("%d\n",a[i]);return;}
	printf("-1\n");
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=red();q=red();
	for(int i=1;i<=n;i++) a[i]=red();
	std::sort(a+1,a+1+n);
	for(int i=1;i<=q;i++) red(),fd(red());
	return 0;
}

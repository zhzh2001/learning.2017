#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
const int maxn=105,tt=16383,flg[4][2]={{1,0},{0,1},{-1,0},{0,-1}};
int n,m,a[maxn][maxn],f[maxn][maxn][3],INF,ans;	// f[i][j][k]表示走到(i,j)且颜色为k的最优解 
struct Y{int x,y,s;}q[tt+1];bool vis[maxn][maxn][3];
inline int red(){
	int ret=0;char ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) ret=(ret<<3)+(ret<<1)+(ch^48),ch=getchar();
	return ret;
}
inline bool ck(int x,int y){if(x<1||y<1||x>n||y>n) return 0;return 1;}
inline void SPFA(){
	int L=0,R=1,xx,yy,id;q[1]=(Y){1,1,a[1][1]};	// 从(1,1) 开始走,颜色为a[1][1] 
	vis[1][1][a[1][1]]=1;
	while(L!=R){
		++L&=tt;id=(L+1)&tt;
		vis[q[L].x][q[L].y][q[L].s]=0;
		for(int i=0;i<4;i++){
			xx=q[L].x+flg[i][0];
			yy=q[L].y+flg[i][1];				// (xx,yy)是当前位置 
			if(!ck(xx,yy)) continue;			// 如果越界就不干 
			if(a[xx][yy]){						// 当前格子有颜色 
				if(a[xx][yy]==q[L].s){			// 和上一个格子颜色一样 
					if(f[xx][yy][a[xx][yy]]>f[q[L].x][q[L].y][q[L].s]){		// 如果从q[L]走过来好 
						f[xx][yy][a[xx][yy]]=f[q[L].x][q[L].y][q[L].s];		// 修正，因为颜色相同无花费 
						if(!vis[xx][yy][a[xx][yy]]){						// 如果不在队列中 
							vis[xx][yy][a[xx][yy]]=1;
							q[++R&=tt]=(Y){xx,yy,a[xx][yy]};				// 塞进队列里去 
							if(f[q[R].x][q[R].y][q[R].s]<f[q[id].x][q[id].y][q[id].s]) 
							 std::swap(q[R],q[id]);							// 优化 
						}
					}
				}
				else{														// 颜色不同 
					if(f[xx][yy][a[xx][yy]]>f[q[L].x][q[L].y][q[L].s]+1){	// 花一个金币走过来 
						f[xx][yy][a[xx][yy]]=f[q[L].x][q[L].y][q[L].s]+1;	// 修正，花了一个金币 
						if(!vis[xx][yy][a[xx][yy]]){						// 同上 
							vis[xx][yy][a[xx][yy]]=1;
							q[++R&=tt]=(Y){xx,yy,a[xx][yy]};
							if(f[q[R].x][q[R].y][q[R].s]<f[q[id].x][q[id].y][q[id].s]) 
							 std::swap(q[R],q[id]);
						}
					}
				}
			}
			else if(a[q[L].x][q[L].y])						// 当前格子没有颜色需要放魔法且上一状态有颜色(即可以放魔法) 
			 for(int j=1;j<3;j++){				// 枚举改变后的颜色 
				if(j==q[L].s){					// 改成一样的 
					if(f[xx][yy][j]>f[q[L].x][q[L].y][q[L].s]+2){
						f[xx][yy][j]=f[q[L].x][q[L].y][q[L].s]+2;		// 改颜色花两个金币 
						if(!vis[xx][yy][j]){
							vis[xx][yy][j]=1;
							q[++R&=tt]=(Y){xx,yy,j};
							if(f[q[R].x][q[R].y][q[R].s]<f[q[id].x][q[id].y][q[id].s]) 
							 std::swap(q[R],q[id]);
						}
					}
				}
				else{							// 改成不一样的 
					if(f[xx][yy][j]>f[q[L].x][q[L].y][q[L].s]+3){
						f[xx][yy][j]=f[q[L].x][q[L].y][q[L].s]+3;	// 改颜色&走过不同颜色，共花三个金币 
						if(!vis[xx][yy][j]){
							vis[xx][yy][j]=1;
							q[++R&=tt]=(Y){xx,yy,j};
							if(f[q[R].x][q[R].y][q[R].s]<f[q[id].x][q[id].y][q[id].s]) 
							 std::swap(q[R],q[id]);
						}
					}
				}
			 }
		}
	}
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=red();m=red();
	for(int i=1,x,y,z;i<=m;i++){
		x=red();y=red();z=red();
		if(z) a[x][y]=1;
		 else a[x][y]=2;
	}
	memset(f,63,sizeof f);ans=INF=f[0][0][0];
	f[1][1][a[1][1]]=0;SPFA();
	for(int i=0;i<3;i++) if(f[n][n][i]<ans) ans=f[n][n][i];
	if(ans^INF) printf("%d\n",ans);else printf("-1\n");
	return 0;
}

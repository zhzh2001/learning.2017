#include <bits/stdc++.h>

using namespace std;

int abc(int b){
	int ans=1;
	for(int i=0;i<b;i++) ans*=10;
	return ans;
}

int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,q;
	scanf("%d %d",&n,&q);
	int book[n];
	for(int i=0;i<n;i++) scanf("%d",&book[i]);
	sort(book,book+n);
	for(int i=0;i<q;i++){
		int asksize;
		scanf("%d",&asksize);
		char ask[asksize];
		scanf("%s",&ask);
		bool flag;
		for(int j=0;j<n;j++){
			flag=true;
			for(int k=asksize-1;k>=0;k--){
				int middle1=book[j]%(abc(asksize-k)),middle2=book[j]%(abc(asksize-k-1));
				int middle=(middle1-middle2)/(abc(asksize-k-1));
				if(ask[k]!=middle+'0'){;
					flag=false;
					break;
				}
			}
			if(flag){
				printf("%d\n",book[j]);
				break;
			}
		}
		if(!flag) printf("-1\n");
	}
	fclose(stdout);
	return 0;
}

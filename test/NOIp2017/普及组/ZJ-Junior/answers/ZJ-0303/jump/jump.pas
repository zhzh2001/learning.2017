var
   t,w,i,n,m,x,y,op,pp,ans,mid,pop:longint;
   a:array[0..500001]of longint;
   f:array[0..500001]of int64;
function max(x,y:longint):longint;
begin
 if x>y then exit(x) else exit(y);
end;
function check(g:longint):boolean;
var k,i:longint;
begin
  fillchar(f,sizeof(f),0);
  if g<m then
   begin
   for k:=m-g to op do
   begin
   pp:=-maxlongint;
    for i:=max(k-m+g,0) downto max(k-m-g,0) do
     if f[i]>pp then
       pp:=f[i];
    f[k]:=a[k]+pp;

    if f[k]>pop then exit(true);
    end;
   end
 else
   begin
   for k:=1 to op do
   begin
   pp:=-maxlongint;
    for i:=max(k-1,0) downto max(k-m-g,0) do
     if f[i]>pp then
       pp:=f[i];
    f[k]:=a[k]+pp;

    if f[k]>pop then exit(true);
    end;
   end;
 exit(false);
end;
begin
 assign(input,'jump.in');reset(input);
 assign(output,'jump.out');rewrite(output);
  readln(n,m,pop);
  ans:=-1;
  for i:=1 to n do
  begin
   read(x,y);
   a[x]:=y;
   if x>op then op:=x;
  end;
  t:=1; w:=500000;
  while t<=w do
   begin
    mid:=(t+w) div 2;
    if check(mid) then
    begin
     ans:=mid;
     w:=mid-1;
    end
   else
     t:=mid+1;
   end;
  if ans=-1 then writeln('-1') else
   writeln(ans);
  close(input);
  close(output);
end.

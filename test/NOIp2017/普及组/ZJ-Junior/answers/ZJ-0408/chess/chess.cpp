#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define inf 1e9
using namespace std;
int g[110][110];//-1==nothing,0==red,1==yellow
int f[110][110][2];
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(g,-1,sizeof(g));
	memset(f,-1,sizeof(f));
	int n,m;
	cin>>m>>n;
	for(int i=0;i<109;i++)
		for(int j=0;j<109;j++)
			for(int k=0;k<2;k++)
				f[i][j][k]=inf;
	for(int i=1;i<=n;i++)
	{
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		g[x][y]=c;
	}
	f[1][1][0]=f[1][1][1]=0;
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++)
		{
			int c,cost;
			if(g[i][j]==-1)
			{
				for(int k=0;k<2;k++)
				{
					c=k;
					//g[i][j+1]
					if(g[i][j+1]==-1) continue;//next is nothing
					else
					{
						if(g[i][j+1]==c)f[i][j+1][g[i][j+1]]=min(f[i][j+1][g[i][j+1]],f[i][j][c]);
						else f[i][j+1][g[i][j+1]]=min(f[i][j+1][g[i][j+1]],f[i][j][c]+1);
					}
					//g[i+1][j]
					if(g[i+1][j]==-1) continue;//next is nothing
					else
					{
						if(g[i+1][j]==c)f[i+1][j][g[i+1][j]]=min(f[i+1][j][g[i+1][j]],f[i][j][c]);
						else f[i+1][j][g[i+1][j]]=min(f[i+1][j][g[i+1][j]],f[i][j][c]+1);
					}
				}
			}
			else
			{
				c=g[i][j];
				//g[i][j+1]
				if(g[i][j+1]==-1)//next is nothing
				{
					for(int k=0;k<2;k++)
					{
						if(k==c) f[i][j+1][k]=min(f[i][j+1][k],f[i][j][c]+2);
						else f[i][j+1][k]=min(f[i][j+1][k],f[i][j][c]+3);
					}
				}
				else//next has color
				{
					if(g[i][j+1]==c)f[i][j+1][g[i][j+1]]=min(f[i][j+1][g[i][j+1]],f[i][j][c]);
					else f[i][j+1][g[i][j+1]]=min(f[i][j+1][g[i][j+1]],f[i][j][c]+1);
				}
				//g[i+1][j]
				if(g[i+1][j]==-1)//next is nothing
				{
					for(int k=0;k<2;k++)
					{
						if(k==c) f[i+1][j][k]=min(f[i+1][j][k],f[i][j][c]+2);
						else f[i+1][j][k]=min(f[i+1][j][k],f[i][j][c]+3);
					}
				}
				else//next has color
				{
					if(g[i+1][j]==c)f[i+1][j][g[i+1][j]]=min(f[i+1][j][g[i+1][j]],f[i][j][c]);
					else f[i+1][j][g[i+1][j]]=min(f[i+1][j][g[i+1][j]],f[i][j][c]+1);
				}
			}
		}
	int ans=inf;
	if(g[m][m]==-1)
		for(int i=0;i<2;i++)
			ans=min(f[m][m][i],ans);
	else ans=f[m][m][g[m][m]];
	if(ans==inf) ans=-1;
	cout<<ans<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

var n,q,i,j,l,x,temp:longint;
    get:boolean;
    a:array[0..1001]of longint;
function power(x,exp:longint):longint;
var i,ans:longint;
begin
  ans:=1;
  for i:=1 to exp do ans:=ans*x;
  exit(ans);
end;
procedure qsort(l,r:longint);
var i,j,mid,t:longint;
begin
  i:=l; j:=r; mid:=a[(i+j)div 2];
  repeat
    while a[i]<mid do inc(i);
    while a[j]>mid do dec(j);
      if i<=j then
        begin
          t:=a[i]; a[i]:=a[j]; a[j]:=t;
          inc(i); dec(j);
        end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
    readln(n,q);
    for i:=1 to n do
      readln(a[i]);
    qsort(1,n);
    for i:=1 to q do
      begin
        get:=false;
        readln(l,x);
        for j:=1 to n do
          begin
            temp:=a[j]-x;
            if temp mod power(10,l)=0 then
              begin
                writeln(a[j]);
                get:=true;
                break;
              end;
          end;
        if not get then writeln(-1);
      end;
  close(input); close(output);
end.

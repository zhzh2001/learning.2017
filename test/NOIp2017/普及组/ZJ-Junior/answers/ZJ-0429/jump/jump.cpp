#include<cstring>
#include<string>
#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<climits>
#include<cmath>
#include<vector>

using namespace std;

struct ge{
	int val;
	int len;
};

int n,d,k;
vector<ge> map;

int main(void){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	int maxlen=0;
	ge qd;
	qd.val=0;
	qd.len=0;
	map.push_back(qd);
	for (int i=0;i<n;i++){
		ge tmp;
		cin>>tmp.len>>tmp.val;
		map.push_back(tmp);
		maxlen=max(maxlen,tmp.len);
	}
	for (int g=0;d+g<=maxlen;g++){
		int f[1000],kn[1000];
		memset(f,0,sizeof(f));
		memset(kn,0,sizeof(kn));
		kn[0]=1;
		int i;
		for (i=1;i<=n;i++){
			for (int j=i;j>=0;j--){
				if (map[i].len-map[i-j].len>=max(1,d-g) && map[i].len-map[i-j].len<=d+g) {
					kn[i]=kn[i-j];
					if (kn[i]==1) f[i]=max(f[i-j],f[i])+map[i].val;
				}
			}
			if (f[i]>=k){
				cout<<g<<endl;
				return 0;
			}
		}
	}
	cout<<-1<<endl;
	return 0;
}


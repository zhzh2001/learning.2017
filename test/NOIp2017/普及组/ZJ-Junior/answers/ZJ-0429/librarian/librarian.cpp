#include<cstring>
#include<string>
#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<climits>
#include<cmath>
#include<vector>

using namespace std;

int n,q;
int map[5000];

int main(void){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	cin>>n>>q;
	for (int i=1;i<=n;i++) cin>>map[i];
	for (int i=1;i<=q;i++){
		int tmp,ans=INT_MAX,size;
		bool check=false;
		cin>>size>>tmp;
		for(int j=1;j<=n;j++){
			int a=tmp,b=map[j];
			bool is=true;
			while (a>0){
				if ((a%10)!=(b%10)){
					is=false;
					break;
				}
				a/=10;
				b/=10;
			}
			if (is) {
				ans=min(ans,map[j]);
				check=true;
			}
		}
		if (check) cout<<ans<<endl;
		else cout<<-1<<endl;
	}
	return 0;
}


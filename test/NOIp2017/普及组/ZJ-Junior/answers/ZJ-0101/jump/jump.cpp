#include<bits/stdc++.h>
using namespace std;
int f[100000];
int l[100000];
int g[100000];
int w[100000];
int q[100000];
int p[100000];
int n,m,d;
int gcd(int a,int b)
{
	if (b==0) return a;
	return gcd(b,a%b);
}
int dp(int b,int e)
{
	memset(q,0,sizeof(q));
	memset(p,0,sizeof(p));
	for (int i=1;i<=n;i++)
	{
		w[i]=l[i];
	}
	for (int i=1;i<=n;i++)
	{
		p[w[i]]=f[i];
	}
	for (int i=1;i<=w[n];i++)
	{
		for (int j=i-e;j<=i-b;j++)
			if (j>1&&j<i) q[i]=max(q[i],q[j]+p[i]);
	}
	int MAX=0;
	for (int i=1;i<=w[n];i++)
		MAX=max(q[i],MAX);
	return MAX;
}
int main()
{
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>m;
	int tot=0;
	for (int i=1;i<=n;i++)
	{
		cin>>l[i]>>f[i];
		tot+=max(0,f[i]);
	}
	if (d==1)
	{
		if (tot>=m) cout<<0;
		else cout<<-1;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	else if (tot<m)
	{
		cout<<-1;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	int l=1,r=100000,mid;
	while (l+3<r)
	{
		mid=(l+r)/2;
		if (dp(max(d-mid,1),d+mid)>=m) r=mid;
		else l=mid+1;
	}
	for (int i=l;i<=r;i++)
	{
		if (dp(max(d-i,1),d+i)>=m)
		{
			cout<<i;
			fclose(stdin);
			fclose(stdout);
			return 0;
		}
	}
	cout<<-1;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int f[201][201][2][2]={0};
int ans=0;
int vis[201][201];
int a[201][201];
int n,m;
int baoli(int nx,int ny,bool magic,int color,int t=0)
{
//	cout<<nx<<" "<<ny<<" "<<magic<<" "<<color<<" "<<t<<" "<<f[nx][ny][magic][color]<<endl;
	if (nx<1||ny<1||nx>n||ny>n) return 10000000;
	if (nx==n&&ny==n) return 0;
	
	if (n>10) if (f[nx][ny][magic][color]&&f[nx][ny][magic][color]!=10000000) return f[nx][ny][magic][color];
	if (vis[nx][ny]) return 10000000;
	vis[nx][ny]=1;
	int MAX=10000000;
	
	if (magic==0)
		if (a[nx+1][ny]==-1) MAX=min(baoli(nx+1,ny,1,color,t+2)+2,MAX);
		else if (color==a[nx+1][ny]) MAX=min(baoli(nx+1,ny,0,a[nx+1][ny],t),MAX);
		 	 else MAX=min(baoli(nx+1,ny,0,a[nx+1][ny],t+1)+1,MAX);
	else
		if (a[nx+1][ny]!=-1)
			if (color==a[nx+1][ny]) MAX=min(baoli(nx+1,ny,0,a[nx+1][ny],t),MAX);
			else MAX=min(baoli(nx+1,ny,0,a[nx+1][ny],t+1)+1,MAX);
			
	if (magic==0)
		if (a[nx][ny+1]==-1) MAX=min(baoli(nx,ny+1,1,color,t+2)+2,MAX);
		else if (color==a[nx][ny+1]) MAX=min(baoli(nx,ny+1,0,a[nx][ny+1],t),MAX);
		 	 else MAX=min(baoli(nx,ny+1,0,a[nx][ny+1],t+1)+1,MAX);
	else
		if (a[nx][ny+1]!=-1)
			if (color==a[nx][ny+1]) MAX=min(baoli(nx,ny+1,0,a[nx][ny+1],t),MAX);
			else MAX=min(baoli(nx,ny+1,0,a[nx][ny+1],t+1)+1,MAX);
			
	if (magic==0)
		if (a[nx-1][ny]==-1) MAX=min(baoli(nx-1,ny,1,color,t+2)+2,MAX);
		else if (color==a[nx-1][ny]) MAX=min(baoli(nx-1,ny,0,a[nx-1][ny],t),MAX);
		 	 else MAX=min(baoli(nx-1,ny,0,a[nx-1][ny],t+1)+1,MAX);
	else
		if (a[nx-1][ny]!=-1)
			if (color==a[nx-1][ny]) MAX=min(baoli(nx-1,ny,0,a[nx-1][ny],t),MAX);
			else MAX=min(baoli(nx-1,ny,0,a[nx-1][ny],t+1)+1,MAX);
			
	if (magic==0)
		if (a[nx][ny-1]==-1) MAX=min(baoli(nx,ny-1,1,color,t+2)+2,MAX);
		else if (color==a[nx][ny-1]) MAX=min(baoli(nx,ny-1,0,a[nx][ny-1],t),MAX);
		 	 else MAX=min(baoli(nx,ny-1,0,a[nx][ny-1],t+1)+1,MAX);
	else
		if (a[nx][ny-1]!=-1)
			if (color==a[nx][ny-1]) MAX=min(baoli(nx,ny-1,0,a[nx][ny-1],t),MAX);
			else MAX=min(baoli(nx,ny-1,0,a[nx][ny-1],t+1)+1,MAX);
	vis[nx][ny]=0;
	f[nx][ny][magic][color]=MAX;
	return MAX;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(a,-1,sizeof(a));
	cin>>n>>m;
	int q,w,e;
	for (int i=1;i<=m;i++)
		cin>>q>>w>>e,a[q][w]=e;
	int ans=baoli(1,1,0,a[1][1]);
	if (ans>500000) cout<<-1;
	else cout<<ans;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

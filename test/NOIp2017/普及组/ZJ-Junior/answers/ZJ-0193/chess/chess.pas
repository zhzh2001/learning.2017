var m,n,i,j,k,x,y,c,count,min:longint;
    can,use:boolean;
    e,f:array[0..4]of integer;
    a,d:array[0..101,0..101]of integer;
procedure dfs(l,r:longint);
var i,u,v:longint;
begin
  if (l=m)and(r=m) then
    begin
      can:=true;
      if count<min then min:=count;
      exit;
    end;
  u:=0; v:=0;
  d[l,r]:=1;
  for i:=1 to 4 do
    begin
      u:=l+e[i];
      v:=r+f[i];
      if (u>=1)and(u<=m)and(v>=1)and(v<=m)and(d[u,v]=0) then
        begin
          if (a[u,v]=0)and(use=false)then
              begin
                use:=true;
                inc(count,2);
                a[u,v]:=a[l,r];
                dfs(u,v);
                a[u,v]:=0;
                dec(count,2);
                continue;
              end;
          use:=false;
          if a[l,r]<>a[u,v] then inc(count);
          if a[u,v]<>0 then dfs(u,v);
          if a[l,r]<>a[u,v] then dec(count);
        end;
    end;
  d[l,r]:=0;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  e[1]:=1; e[2]:=-1; e[3]:=0; e[4]:=0;
  f[1]:=0; f[2]:=0;  f[3]:=1; f[4]:=-1;
  count:=0; min:=maxint; can:=false;
  fillchar(a,sizeof(a),0);
  for i:=1 to n do
  begin
    readln(x,y,c);
    a[x,y]:=c+1;
  end;
  if a[m,m]=0 then begin write(-1); close(input); close(output); exit; end;
  dfs(1,1);
  if can then write(min)
    else write(-1);
  close(input);
  close(output);
end.
#include<cstdio>
#define maxn 105
using namespace std;
int n,m,ans,map[maxn][maxn],lst_x=1,lst_y=1;
bool flg;
inline int read(){
	int ret=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++){
		int x=read(),y=read(),z=read();
		map[x][y]=z+1;
	}
	int x=1,y=1;map[x][y]=map[x][y+1];
	while (x!=n||y!=n){
		if (!map[x+1][y+1]&&!map[x][y+1]&&!map[x+1][y]){printf("-1");return 0;}
		if (flg) x++,flg=0;else y++,flg=1;
		if (map[lst_x][lst_y]!=map[x][y]){
			if (!map[x][y]) ans+=2,map[x][y]=map[lst_x][lst_y];
			else ans++,map[x][y]=map[lst_x][lst_y];
		}
		lst_x=x;lst_y=y;
	}
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<algorithm>
#define maxn 1005
using namespace std;
const int tt[10]={10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};
int n,q,a[maxn],ans;
inline int read(){
	int ret=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') ret=ret*10+ch-'0',ch=getchar();
	return ret*f;
}
inline int check(int L,int x){
	int len=tt[L-1];
	for (int i=1;i<=n;i++){
		if (a[i]%len==x) return a[i];
	}
	return 0;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	n=read();q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+n+1);
	for (int i=1;i<=q;i++){
		int L=read(),x=read();
		ans=check(L,x);
		if (!ans) printf("-1\n");
		else printf("%d\n",ans);
	}
	return 0;
}

var
  n,q,i,j,k,x,y,sum:longint;
  a,b,c:array[0..1005] of longint;
  procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to q do
    readln(b[i],c[i]);
  sort(1,n);
  for i:=1 to q do
    begin
      sum:=1;x:=0;
      for j:=1 to b[i] do
        sum:=sum*10;
      for j:=1 to n do
        if a[j] mod sum=c[i]
          then begin
                 writeln(a[j]);
                 x:=1;
                 break;
               end;
      if x=0 then writeln(-1);
    end;
  close(input);
  close(output);
end.

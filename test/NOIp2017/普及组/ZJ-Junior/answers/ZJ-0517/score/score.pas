var
  a,b,c:longint;
  sum:real;
begin
  assign(input,'score.in');
  reset(input);
  assign(output,'score.out');
  rewrite(output);
  readln(a,b,c);
  sum:=a*0.2+b*0.3+c*0.5;
  writeln(trunc(sum));
  close(input);
  close(output);
end.
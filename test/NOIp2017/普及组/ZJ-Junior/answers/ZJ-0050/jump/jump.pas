var
  n,dd,k,i,j,d1,d2,dt,dtt,t1,t2,min:longint;
  s:int64;
  a,d,f,b:array[0..500000]of longint;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);
  readln(n,dd,k);
  for i:=1 to n do begin
    readln(d[i],a[i]);
    if a[i]>0 then
      s:=s+a[i];
    f[i]:=d[i]-d[i-1];
  end;
  if s<k then begin
    writeln(-1);
    close(input);
    close(output);
    halt;
  end;
  min:=maxlongint;
  while b[0]=0 do begin
    i:=n;
    while b[i]=1 do begin
      b[i]:=0;
      i:=i-1;
    end;
    b[i]:=1;
    s:=0;
    d1:=-maxlongint;
    d2:=maxlongint;
    dt:=0;
    for i:=1 to n do begin
      if b[i]=1 then begin
        s:=s+a[i];
        dtt:=d[i]-dt;
        dt:=d[i];
        if dtt>d1 then
          d1:=dtt;
        if dtt<d2 then
          d2:=dtt;
      end;
    end;
    if s>=k then begin
      t1:=abs(d1-dd);
      t2:=abs(d2-dd);
      if t1>t2 then
        dtt:=t1
      else
        dtt:=t2;
      if dtt<min then
        min:=dtt;
    end;
  end;
  writeln(min);
  close(input);
  close(output);
end.

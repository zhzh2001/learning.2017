const
  dx:array[1..4]of longint=(-1,0,1,0);
  dy:array[1..4]of longint=(0,1,0,-1);
var
  m,n,i,j,a,b,c,head,tail,tx,ty,ttx,tty,tt,sum:longint;
  map:array[0..101,0..101]of longint;
  f:array[0..101,0..101]of longint;
  q:array[1..100000]of record
    x,y:longint;
  end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  filldword(map,sizeof(map)div 4,2);
  readln(m,n);
  for i:=1 to n do begin
    readln(a,b,c);
    map[a,b]:=c;
  end;
  for i:=0 to m+1 do
    for j:=0 to m+1 do
      if(i=0)or(i=m+1)or(j=0)or(j=m+1)then
        f[i,j]:=-maxlongint div 3
      else
        f[i,j]:=maxlongint div 3;
  head:=1;
  tail:=2;
  q[1].x:=1;
  q[1].y:=1;
  f[1,1]:=0;
  while head<tail do begin
    tx:=q[head].x;
    ty:=q[head].y;
    for i:=1 to 4 do begin
      ttx:=tx+dx[i];
      tty:=ty+dy[i];
      if map[tx,ty]=map[ttx,tty] then begin
        tt:=f[tx,ty];
      end;
      if(map[tx,ty]<>map[ttx,tty])and(map[ttx,tty]<>0)then begin
        tt:=f[tx,ty]+1;
      end;
      if(map[ttx,tty]=2)then begin
        tt:=f[tx,ty]+2;
      end;
      if tt<f[ttx,tty] then begin
        if map[ttx,tty]=2 then
          map[ttx,tty]:=map[tx,ty];
        f[ttx,tty]:=tt;
        q[tail].x:=ttx;
        q[tail].y:=tty;
        tail:=tail+1;
      end;
    end;
    head:=head+1;
    writeln('------------');
    writeln(head,' ',tail-1);
    for i:=head to tail-1 do
      writeln(q[i].x,' ',q[i].y,' ',f[q[i].x,q[i].y]);
  end;
  writeln(f[m,m]);
  writeln(sum);
  close(input);
  close(output);
end.
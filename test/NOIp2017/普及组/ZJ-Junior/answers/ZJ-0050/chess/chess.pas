const
  dx:array[1..4]of longint=(-1,0,1,0);
  dy:array[1..4]of longint=(0,1,0,-1);
var
  map,f:array[0..101,0..101]of longint;
  n,m,i,j,a,b,c,min:longint;
procedure search(x,y,s:longint;fl:boolean);
var
  i,t,tx,ty:longint;
begin
  f[x,y]:=s;
  if(x=m)and(y=m)then begin
    if s<min then
      min:=s;
    exit;
  end;
  for i:=1 to 4 do begin
    tx:=x+dx[i];
    ty:=y+dy[i];
    if f[tx,ty]<=s then
      continue;
    if map[x,y]=map[tx,ty] then begin
      search(tx,ty,s,false);
    end else if(map[tx,ty]=0)and(not fl)then begin
      map[tx,ty]:=map[x,y];
      search(tx,ty,s+2,true);
      map[tx,ty]:=0;
    end else if(map[tx,ty]<>map[x,y])and(map[tx,ty]<>0) then begin
      search(tx,ty,s+1,false);
    end;
  end;
end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);
  readln(m,n);
  for i:=1 to n do begin
    readln(a,b,c);
    c:=c+1;
    map[a,b]:=c;
  end;
  {for i:=1 to m do begin
    for j:=1 to m do
      write(map[i,j]);
    writeln;
  end;}
  min:=maxlongint;
  for i:=0 to m+1 do
    for j:=0 to m+1 do
      if(i=0)or(j=0)or(i=m+1)or(j=m+1)then
        f[i,j]:=-maxlongint
      else
        f[i,j]:=maxlongint;
  search(1,1,0,false);
  if min=maxlongint then
    writeln(-1)
  else
    writeln(min);
  close(input);
  close(output);
end.

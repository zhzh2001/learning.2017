var
  n,q,i,j,t1,t2:longint;
  t:string;
  a:array[1..1001]of string;
function less(a,b:string):boolean;
var
  la,lb:longint;
begin
  la:=length(a);
  lb:=length(b);
  if(la<lb)or(la=lb)and(a<b)then
    exit(true);
  if(la>lb)or(la=lb)and(a>b)then
    exit(false);
  exit(true);
end;
function len(a:string):longint;
begin
  len:=ord(a[0]);
end;
procedure qsort(l,r:longint);
var
  i,j:longint;
  m,t:string;
begin
  i:=l;
  j:=r;
  m:=a[(l+r)div 2];
  repeat
    while(len(a[i])<len(m))or(len(a[i])=len(m))and(a[i]<m)do
      i:=i+1;
    while(len(a[j])>len(m))or(len(a[j])=len(m))and(a[j]>m)do
      j:=j-1;
    if i<=j then begin
      t:=a[i];
      a[i]:=a[j];
      a[j]:=t;
      i:=i+1;
      j:=j-1;
    end;
  until i>j;
  if i<r then
    qsort(i,r);
  if l<j then
    qsort(l,j);
end;
function check(tt,t:string):boolean;
var
  l1,l2:longint;
begin
  check:=true;
  l1:=length(tt);
  l2:=length(t);
  while l2>0 do begin
    if tt[l1]=t[l2] then begin
      l1:=l1-1;
      l2:=l2-1;
    end else begin
      exit(false);
    end;
  end;
end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  qsort(1,n);
  {for i:=1 to n do
    writeln(a[i],' ');
  writeln;}
  for i:=1 to q do begin
    readln(t1,t2);
    str(t2,t);
    j:=1;
    while(not check(a[j],t))and(j<=n)do
      j:=j+1;
    if j>n then
      writeln(-1)
    else
      writeln(a[j]);
  end;
  close(input);
  close(output);
end.

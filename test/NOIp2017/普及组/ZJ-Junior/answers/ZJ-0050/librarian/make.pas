var
	n,q,i,j,k,t:longint;
begin
	assign(output,'librarian.in');
  rewrite(output);
  randomize;
  readln(n,q);
  writeln(n,' ',q);
  for i:=1 to n do begin
    k:=random(5)+4;
    t:=0;
    for j:=1 to k do
    	t:=t*10+random(9)+1;
    writeln(t);
  end;
  for i:=1 to q do begin
    k:=random(5)+1;
    t:=0;
    for j:=1 to k do
    	t:=t*10+random(9)+1;
    writeln(k,' ',t);
  end;
  close(output);
end.
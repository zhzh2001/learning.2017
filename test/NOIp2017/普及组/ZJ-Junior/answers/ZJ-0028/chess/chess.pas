function min(aa,bb:longint):longint;
begin
        if aa<bb then exit(aa) else exit(bb);
end;

var
        n,m,i,j,l,r,count,x,y:longint;
        a,b,k:longint;
        qx,qy:array[0..30010] of longint;
        map,money:array[-2..102,-2..102] of longint;
        visit:array[-2..102,-2..102] of boolean;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
        readln(n,m);
        fillchar(map,sizeof(map),255);
        for i:=-2 to n+2 do
        for j:=-2 to n+2 do money[i,j]:=maxlongint;
        for i:=1 to m do begin
                readln(a,b,k);
                map[a,b]:=k;
        end;
        l:=1;
        r:=1;
        qx[l]:=1;
        qy[l]:=1;
        x:=1;y:=1;
        visit[x,y]:=true;
        count:=1;
        while count>0 do begin
        x:=qx[l];
        y:=qy[l];
        if (map[x,y]<>-1) then begin
                if ((map[x,y-2]<>-1)and((visit[x,y-2]=false)or((x=n)and(y-2=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x;qy[r]:=y-2;
                        money[x,y-2]:=min(money[x,y-2],abs(map[x,y]-map[x,y-2])+2);
                        visit[x,y-2]:=true;
                end;
                if ((map[x,y-1]<>-1)and((visit[x,y-1]=false)or((x=n)and(y-1=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x;
                        qy[r]:=y-1;
                        money[x,y-1]:=min(money[x,y-1],abs(map[x,y]-map[x,y-1]));
                        visit[x,y-1]:=true;
                end;

                if ((map[x,y+1]<>-1)and((visit[x,y+1]=false)or((x=n)and(y+1=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x;
                        qy[r]:=y+1;
                        money[x,y+1]:=min(money[x,y+1],abs(map[x,y]-map[x,y+1]));
                        visit[x,y+1]:=true;
                end;

                if ((map[x,y+2]<>-1)and((visit[x,y+2]=false)or((x=n)and(y+2=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x;
                        qy[r]:=y+2;
                        money[x,y+2]:=min(money[x,y+2],abs(map[x,y]-map[x,y+2])+2);
                        visit[x,y+2]:=true;
                end;

                if ((map[x+2,y]<>-1)and((visit[x+2,y]=false)or((x+2=n)and(y=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x+2;
                        qy[r]:=y;
                        money[x+2,y]:=min(money[x+2,y],abs(map[x,y]-map[x+2,y])+2);
                        visit[x+2,y]:=true;
                end;

                if ((map[x+1,y]<>-1)and((visit[x+1,y]=false)or((x+1=n)and(y=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x+1;
                        qy[r]:=y;
                        money[x+1,y]:=min(money[x+1,y],abs(map[x,y]-map[x+1,y]));
                        visit[x+1,y]:=true;
                end;

                if ((map[x-1,y]<>-1)and((visit[x-1,y]=false)or((x-1=n)and(y=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x-1;
                        qy[r]:=y;
                        money[x-1,y]:=min(money[x-1,y],abs(map[x,y]-map[x-1,y]));
                        visit[x-1,y]:=true;
                end;

                if ((map[x-2,y]<>-1)and((visit[x-2,y]=false)or((x-2=n)and(y=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x-2;
                        qy[r]:=y;
                        money[x-2,y]:=min(money[x-2,y],abs(map[x,y]-map[x-2,y])+2);
                        visit[x-2,y]:=true;
                end;

                if ((map[x+1,y+1]<>-1)and((visit[x+1,y+1]=false)or((x+1=n)and(y+1=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x+1;
                        qy[r]:=y+1;
                        money[x+1,y+1]:=min(money[x+1,y+1],abs(map[x,y]-map[x+1,y+1])+2);
                        visit[x+1,y+1]:=true;
                end;

                if ((map[x+1,y-1]<>-1)and((visit[x+1,y-1]=false)or((x+1=n)and(y-1=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x+1;
                        qy[r]:=y-1;
                        money[x+1,y-1]:=min(money[x+1,y-1],abs(map[x,y]-map[x+1,y-1])+2);
                        visit[x+1,y-1]:=true;
                end;

                if ((map[x-1,y+1]<>-1)and((visit[x-1,y+1]=false)or((x-1=n)and(y+1=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x-1;
                        qy[r]:=y+1;
                        money[x-1,y+1]:=min(money[x-1,y+1],abs(map[x,y]-map[x-1,y+1])+2);
                        visit[x-1,y+1]:=true;
                end;

                if ((map[x-1,y-1]<>-1)and((visit[x-1,y-1]=false)or((x-1=n)and(y-1=n)))) then begin
                        inc(r);
                        inc(count);
                        qx[r]:=x-1;
                        qy[r]:=y-1;
                        money[x-1,y-1]:=min(money[x-1,y-1],abs(map[x,y]-map[x-1,y-1])+2);
                        visit[x-1,y-1]:=true;
                end;
                dec(count);
                inc(l);
        end;
        end;
        if money[n,n]=maxlongint then begin
                writeln(-1);
                close(input);
                close(output);
                halt;
        end;
        writeln(money[n,n]);
end.

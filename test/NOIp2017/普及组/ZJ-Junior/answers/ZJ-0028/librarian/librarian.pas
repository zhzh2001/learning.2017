type New=record
  num:longint;
  s:string;
  chang:longint;
end;

var
  n,q:longint;
  i,j,count:longint;
  t:New;
  a:array[-10..2000] of New;
  len:longint;
  cs,s1:string;
  t1:longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do begin
    readln(a[i].num);
    str(a[i].num,a[i].s);
    a[i].chang:=length(a[i].s);
  end;
  for i:=1 to n-1 do
    for j:=i+1 to n do
    if a[i].num>a[j].num then begin
      t:=a[i];
      a[i]:=a[j];
      a[j]:=t;
    end;

  for i:=1 to q do begin
    count:=0;
    readln(len,s1);
    delete(s1,1,1);
    for j:=1 to n do begin
      cs:=copy(a[j].s,a[j].chang-len+1,len);
      if (cs=s1) then begin
        writeln(a[j].s);
        break;
      end;
      inc(count);
    end;
    if (count=n) then writeln(-1);
  end;
  close(input);
  close(output);
end.

var
    n,d,k,i,x,y,ans,m:longint;
    a,s,f:array[0..500001] of longint;
function max(a,b:longint):Longint;
begin
    if a>b then
        exit(a);
    exit(b);
end;
function pd(x:longint):boolean;
var
    i,j:longint;
begin
    fillchar(f,sizeof(f),255);
    f[0]:=0;
    for i:=1 to n do
        for j:=0 to i-1 do
            if (f[j]<>-1)and((a[i]-a[j])>=max(d-x,1))and((a[i]-a[j])<=d+x) then
                f[i]:=max(f[i],f[j]+s[i]);
    for i:=1 to n do
        f[0]:=max(f[0],f[i]);
    if f[0]>=k then
        exit(true);
    exit(false);
end;
begin
    assign(input,'jump.in');
    assign(output,'jump.out');
    reset(input);
    rewrite(output);
    read(n,d,k);
    for i:=1 to n do
        read(a[i],s[i]);
    x:=0;
    y:=1000000000;
    ans:=-1;
    while x<=y do
    begin
        m:=(x+y) div 2;
        if pd(m) then
        begin
            ans:=m;
            y:=m-1;
        end
        else
            x:=m+1;
    end;
    writeln(ans);
    close(input);
    close(output);
end.

var
    n,k,i,j,t,x,p,l:longint;
    s:array[1..1001] of string;
    a,c:array[1..1001] of longint;
    s1:string;
begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input);
    rewrite(output);
    readln(n,k);
    for i:=1 to n do
        readln(a[i]);
    for i:=1 to n-1 do
        for j:=i+1 to n do
            if a[i]>a[j] then
            begin
                t:=a[i];
                a[i]:=a[j];
                a[j]:=t;
            end;
    for i:=1 to n do
    begin
        str(a[i],s[i]);
        c[i]:=length(s[i]);
    end;
    for i:=1 to k do
    begin
        readln(x,x);
        str(x,s1);
        p:=-1;
        l:=length(s1);
        for j:=1 to n do
        begin
            if x>a[j] then
                continue;
            if s1=copy(s[j],c[j]-l+1,l) then
            begin
                p:=a[j];
                break;
            end;
        end;
        writeln(p);
    end;
    close(input);
    close(output);
end.
var
    n,m,i,x,y,j,x1,x2,p,k,ans:longint;
    a:array[0..201,0..201] of longint;
    f:array[0..201,0..201,0..1] of longint;
function min(a,b:longint):longint;
begin
    if a<b then
        exit(a);
    exit(b);
end;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);
    rewrite(output);
    read(m,n);
    fillchar(a,sizeof(a),255);
    for i:=1 to n do
        read(x,y,a[x,y]);
    fillchar(f,sizeof(f),101);
    p:=f[0,0,0]+100;
    f[1,1,a[1,1]]:=0;
    for i:=1 to m do
        for j:=1 to m do
        begin
            if a[i,j]=-1 then
            begin
                if a[i-1,j]<>-1 then
                    x1:=f[i-1,j,a[i-1,j]]
                else
                    x1:=p;
                if a[i,j-1]<>-1 then
                    x2:=f[i,j-1,a[i,j-1]]
                else
                    x2:=p;
                if (x1=p)and(x2=p) then
                    continue;
                for k:=0 to 1 do
                begin
                    f[i,j,k]:=min(x1,x2)+2;
                    if (f[i,j,k]=x1+2)and(x1=x2+2)and(a[i-1,j]<>a[i,j-1]) then
                        continue;
                    if (f[i,j,k]=x1+2)and(k<>a[i-1,j]) then
                        inc(f[i,j,k])
                    else
                    if (f[i,j,k]=x2+2)and(k<>a[i,j-1]) then
                        inc(f[i,j,k]);
                end;
            end
            else
            begin
                if (a[i-1,j]=-1)and(a[i,j-1]=-1) then
                begin
                    for k:=0 to 1 do
                        f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i-1,j,k]+abs(a[i,j]-k));
                     for k:=0 to 1 do
                        f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i,j-1,k]+abs(a[i,j]-k));
                end
                else
                if a[i-1,j]=-1 then
                begin
                    for k:=0 to 1 do
                        f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i-1,j,k]+abs(a[i,j]-k));
                    f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i,j-1,a[i,j-1]]+abs(a[i,j]-a[i,j-1]));
                end
                else
                if a[i,j-1]=-1 then
                begin
                    for k:=0 to 1 do
                        f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i,j-1,k]+abs(a[i,j]-k));
                    f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],f[i-1,j,a[i-1,j]]+abs(a[i,j]-a[i-1,j]));
                end
                else
                    f[i,j,a[i,j]]:=min(f[i,j,a[i,j]],min(f[i,j-1,a[i,j-1]]+abs(a[i,j]-a[i,j-1]),f[i-1,j,a[i-1,j]]+abs(a[i,j]-a[i-1,j])));
            end;
        end;
    if a[m,m]<>-1 then
    begin
        if f[m,m,a[m,m]]=f[0,0,0] then
            writeln(-1)
        else
            writeln(f[m,m,a[m,m]]);
    end
    else
    begin
        ans:=min(f[m,m,0],f[m,m,1]);
        if ans=f[0,0,0] then
            writeln(-1)
        else
            writeln(ans);
    end;
    close(input);
    close(output);
end.

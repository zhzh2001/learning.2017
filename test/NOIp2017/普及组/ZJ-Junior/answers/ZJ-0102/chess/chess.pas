type rec=record
     x,y:longint;
     end;
var p,m,n,i,x,y,z,j,k,l,h,t:longint;
    q:array[0..1000000]of rec;
    fa,a,f:array[0..100,0..100]of longint;
    c,next:array[0..100000]of longint;
    first,last:array[0..100,0..100]of longint;
    b:array[0..100000,0..2]of longint;
    pd:array[0..100,0..100]of boolean;
 procedure add(a,f,e,d,g:longint);
 begin
   inc(k); if first[a,f]=0 then first[a,f]:=k else next[last[a,f]]:=k;
   last[a,f]:=k; b[k,1]:=a+e; b[k,2]:=f+d; c[k]:=g;
 end;
 procedure push(a,b:longint);
 begin
   inc(t); q[t].x:=a; q[t].y:=b; pd[a,b]:=true;
 end;
begin
assign(input,'chess.in');
assign(output,'chess.out');
reset(input);
rewrite(output);
  read(m,n);
  for i:=1 to n do
  begin read(x,y,z); a[x,y]:=z+1; end;
  for i:=1 to m do
  for j:=1 to m do
  begin
    f[i,j]:=maxlongint div 3;
    if i<n then
      if a[i,j]>0 then
        if a[i+1,j]<>a[i,j] then
          if a[i+1,j]=0 then add(i,j,1,0,2) else add(i,j,1,0,1)
        else add(i,j,1,0,0)
      else if a[i+1,j]>0 then add(i,j,1,0,0);
    if i>1 then
      if a[i,j]>0 then
        if a[i-1,j]<>a[i,j] then
          if a[i-1,j]=0 then add(i,j,-1,0,2) else add(i,j,-1,0,1)
        else add(i,j,-1,0,0)
      else if a[i-1,j]>0 then add(i,j,-1,0,0);
    if j<n then
      if a[i,j]>0 then
        if a[i,j+1]<>a[i,j] then
          if a[i,j+1]=0 then add(i,j,0,1,2) else add(i,j,0,1,1)
        else add(i,j,0,1,0)
      else if a[i,j+1]>0 then add(i,j,0,1,0);
    if j>1 then
      if a[i,j]>0 then
        if a[i,j-1]<>a[i,j] then
          if a[i,j-1]=0 then add(i,j,0,-1,2) else add(i,j,0,-1,1)
        else add(i,j,0,-1,0)
      else if a[i,j-1]>0 then add(i,j,0,-1,0);
  end;
  {writeln;
  for i:=1 to k do
  begin
    writeln(b[i,1],' ',b[i,2],' ',c[i]);
  end;  }
  push(1,1); f[1,1]:=0;
  repeat
    inc(h);
    p:=first[q[h].x,q[h].y];
    while p<>0 do
    begin
      if (a[q[h].x,q[h].y]=0)and(fa[q[h].x,q[h].y]<>a[b[p,1],b[p,2]]) then
      begin
        if f[q[h].x,q[h].y]+1<f[b[p,1],b[p,2]] then
        begin
          f[b[p,1],b[p,2]]:=f[q[h].x,q[h].y]+1;
          if not(pd[b[p,1],b[p,2]]) then push(b[p,1],b[p,2]);
        end
      end
      else
      if f[q[h].x,q[h].y]+c[p]<f[b[p,1],b[p,2]] then
      begin
        f[b[p,1],b[p,2]]:=f[q[h].x,q[h].y]+c[p];
        if a[b[p,1],b[p,2]]=0 then fa[b[p,1],b[p,2]]:=a[q[h].x,q[h].y];
        if (not(pd[b[p,1],b[p,2]]))or(a[b[p,1],b[p,2]]=0) then push(b[p,1],b[p,2]);
      end;
      pd[q[h].x,q[h].y]:=false;
      p:=next[p];
    end;
  until h>=t;
  if f[m,m]<>maxlongint div 3 then write(f[m,m])
  else write(-1);
  {writeln;
  for i:=1 to m do
  begin
  for j:=1 to m do
  write(f[i,j],' ');
  writeln;
  end;      }
close(input);
close(output);
end.
uses
  math;
var
  n,d,k,i,j,x,y,s,ans:longint;
procedure intt;
  begin
    assign(input,'jump.in');
    assign(output,'jump.out');
    reset(input);
    rewrite(output);
    readln(n,d,k);
  end;
procedure work;
  begin
    for i:=1 to n do
      begin
        readln(x,y);
        if y<=0 then continue;
        if x<d then ans:=max(ans,d-x);
        if x>=d then ans:=max(ans,min(abs((x div d)*d-x),abs((x div d+1)*d-x)));
        s:=s+y;
        if s>=k then
          begin
            writeln(ans);
            halt;
          end;
      end;
  end;
procedure outt;
  begin
    if s<k then writeln(-1)
      else writeln(ans);
    close(input);
    close(output);
  end;
begin
  intt;
  work;
  outt;
end.

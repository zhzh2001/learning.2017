uses
  math;
var
  a,color,f:array[0..1001,0..1001] of longint;
  x,y,c,i,j,m,n:longint;
procedure intt;
  begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);
    rewrite(output);
    readln(m,n);
    for i:=1 to n do
      begin
        readln(x,y,c);
        color[x,y]:=c+1;
      end;
   { for i:=1 to m do
      begin
        for j:=1 to m do
          write(color[i,j],' ');
        writeln;
      end;       }
    for i:=1 to m do
      for j:=1 to m do
        begin
          if color[i,j]>0 then continue;
          if (color[i-1,j]=0) and (color[i,j-1]=0) then
            begin
              a[i,j]:=-1;
              f[i,j]:=-1;
            end;
        end;
   { for i:=1 to m do
      begin
        for j:=1 to m do
          write(f[i,j],' ');
        writeln;
      end;        }
    for i:=0 to m+1 do
      begin
        a[i,0]:=-1;
        a[0,i]:=-1;
        a[i,m+1]:=-1;
        a[m+1,i]:=-1;
      end;
  end;
procedure work;
  begin
    for i:=1 to m do
      for j:=1 to m do
        begin
          if (i=1) and (j=1) then continue;
          if f[i,j]=-1 then continue;
          if color[i,j]=0 then
            begin
              a[i,j]:=1;
              if (a[i-1,j]<>0) and (a[i,j-1]<>0) then
                begin
                  a[i,j]:=-1;
                  f[i,j]:=-1;
                  continue;
                end;
              if a[i-1,j]<>0 then
                begin
                  f[i,j]:=f[i,j-1]+2;
                  color[i,j]:=color[i,j-1];
                  continue;
                end;
              if a[i,j-1]<>0 then
                begin
                  f[i,j]:=f[i-1,j]+2;
                  color[i,j]:=color[i-1,j];
                  continue;
                end;
              if f[i-1,j]>f[i,j-1] then
                begin
                  f[i,j]:=f[i,j-1]+2;
                  color[i,j]:=color[i,j-1];
                  continue;
                end;
              if f[i-1,j]<f[i,j-1] then
                begin
                  f[i,j]:=f[i-1,j]+2;
                  color[i,j]:=color[i-1,j];
                  continue;
                end;
            end;
        if color[i,j]=1 then
          begin
            if (f[i-1,j]=-1) and (f[i,j-1]=-1) then
              begin
                f[i,j]:=-1;
                continue;
              end;
            if (f[i-1,j]=-1) and (f[i,j-1]<>-1) and (color[i,j-1]<>1) then
              begin
                f[i,j]:=f[i,j-1]+1;
                continue;
              end;
            if (f[i-1,j]<>-1) and (f[i,j-1]=-1) and (color[i-1,j]<>1) then
              begin
                f[i,j]:=f[i-1,j]+1;
                continue;
              end;
            if (f[i-1,j]=-1) and (f[i,j-1]<>-1) and (color[i,j-1]=1) then
              begin
                f[i,j]:=f[i,j-1];
                continue;
              end;
            if (f[i-1,j]<>-1) and (f[i,j-1]=-1) and (color[i-1,j]=1) then
              begin
                f[i,j]:=f[i-1,j];
                continue;
              end;
            if (color[i-1,j]=1) and (color[i,j-1]=1) then
              begin
                f[i,j]:=min(f[i,j-1],f[i-1,j]);
                continue;
              end;
            if (color[i-1,j]=1) and (color[i,j-1]<>1) then
              begin
                f[i,j]:=min(f[i,j-1]+1,f[i-1,j]);
                continue;
              end;
            if (color[i-1,j]<>1) and (color[i,j-1]=1) then
              begin
                f[i,j]:=min(f[i,j-1],f[i-1,j]+1);
                continue;
              end;
            if (color[i-1,j]<>1) and (color[i,j-1]<>1) then
              begin
                f[i,j]:=min(f[i,j-1],f[i-1,j])+1;
                continue;
              end;
          end;
        if color[i,j]=2 then
          begin
            if (f[i-1,j]=-1) and (f[i,j-1]=-1) then
              begin
                f[i,j]:=-1;
                continue;
              end;
            if (f[i-1,j]=-1) and (f[i,j-1]<>-1) and (color[i,j-1]<>2) then
              begin
                f[i,j]:=f[i,j-1]+1;
                continue;
              end;
            if (f[i-1,j]<>-1) and (f[i,j-1]=-1) and (color[i-1,j]<>2) then
              begin
                f[i,j]:=f[i-1,j]+1;
                continue;
              end;
            if (f[i-1,j]=-1) and (f[i,j-1]<>-1) and (color[i,j-1]=2) then
              begin
                f[i,j]:=f[i,j-1];
                continue;
              end;
            if (f[i-1,j]<>-1) and (f[i,j-1]=-1) and (color[i-1,j]=2) then
              begin
                f[i,j]:=f[i-1,j];
                continue;
              end;
            if (color[i-1,j]=2) and (color[i,j-1]=2) then
              begin
                f[i,j]:=min(f[i,j-1],f[i-1,j]);
                continue;
              end;
            if (color[i-1,j]=2) and (color[i,j-1]<>2) then
              begin
                f[i,j]:=min(f[i,j-1]+1,f[i-1,j]);
                continue;
              end;
            if (color[i-1,j]<>2) and (color[i,j-1]=2) then
              begin
                f[i,j]:=min(f[i,j-1],f[i-1,j]+1);
                continue;
              end;
            if (color[i-1,j]<>2) and (color[i,j-1]<>2) then
              begin
                f[i,j]:=min(f[i,j-1],f[i-1,j])+1;
                continue;
              end;
          end;
      end;

  end;
procedure outt;
  begin
    {for i:=1 to m do
      begin
        for j:=1 to m do
          write(f[i,j],' ');
        writeln;
      end;      }
    writeln(f[m,m]);
    close(input);
    close(output);
  end;
begin
  intt;
  work;
  outt;
end.

uses
  math;
var
  n,q,i,j:longint;
  a:array[0..1001] of string;
procedure intt;
  begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input);
    rewrite(output);
    readln(n,q);
    for i:=1 to n do
      readln(a[i]);
  end;
function b(x,y:string):string;
  var
    p:longint;
  begin
    if x=y then exit(x);
    if length(x)>length(y) then exit(y);
    if length(x)<length(y) then exit(x);
    for p:=1 to min(length(x),length(y)) do
      begin
        if ord(x[p])>ord(y[p]) then exit(y);
        if ord(x[p])<ord(y[p]) then exit(x);
      end;
  end;
procedure work;
  var
    len:longint;
    st,ans:string;
  begin
    for i:=1 to q do
      begin
        ans:=':';
        read(len);
        readln(st);
        delete(st,1,1);
        //writeln(st);
        for j:=1 to n do
          begin
            if (pos(st,a[j])<>0) and (length(a[j])-pos(st,a[j])+1=len) then
              begin
                if ans=':' then ans:=a[j]
                  else ans:=b(ans,a[j]);
              end;
          end;
        if ans=':' then writeln(-1)
          else writeln(ans);
      end;
  end;
procedure outt;
  begin
    close(input);
    close(output);
  end;
begin
  intt;
  work;
  outt;
end.
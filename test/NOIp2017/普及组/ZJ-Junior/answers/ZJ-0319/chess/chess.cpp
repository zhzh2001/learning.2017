#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<queue>
using namespace std;
const long long inf=100000000000000000ll;
queue<int>qx,qy;
long long d[101][101];
int n,m,a[101][101],t1,t2,c;
void bfs(int x,int y){
	if(x>1&&a[x-1][y]!=2){
		if(a[x-1][y]==a[x][y]&&d[x][y]<d[x-1][y]){
			d[x-1][y]=d[x][y];
			qx.push(x-1);
			qy.push(y);
		}else if(d[x][y]+1<d[x-1][y]){
			d[x-1][y]=d[x][y]+1;
			qx.push(x-1);
			qy.push(y);
		}
	}
	if(x<m&&a[x+1][y]!=2){
		if(a[x+1][y]==a[x][y]&&d[x][y]<d[x+1][y]){
			d[x+1][y]=d[x][y];
			qx.push(x+1);
			qy.push(y);
		}else if(d[x][y]+1<d[x+1][y]){
			d[x+1][y]=d[x][y]+1;
			qx.push(x+1);
			qy.push(y);
		}
	}
	if(y>1&&a[x][y-1]!=2){
		if(a[x][y-1]==a[x][y]&&d[x][y]<d[x][y-1]){
			d[x][y-1]=d[x][y];
			qx.push(x);
			qy.push(y-1);
		}else if(d[x][y]+1<d[x][y-1]){
			d[x][y-1]=d[x][y]+1;
			qx.push(x);
			qy.push(y-1);
		}
	}
	if(y<m&&a[x][y+1]!=2){
		if(a[x][y+1]==a[x][y]&&d[x][y]<d[x][y+1]){
			d[x][y+1]=d[x][y];
			qx.push(x);
			qy.push(y+1);
		}else if(d[x][y]+1<d[x][y+1]){
			d[x][y+1]=d[x][y]+1;
			qx.push(x);
			qy.push(y+1);
		}
	}
	if(x<m&&y>1&&a[x+1][y-1]!=2&&a[x][y-1]==2&&a[x+1][y]==2){
		if(a[x][y]==a[x+1][y-1]&&d[x][y]+2<d[x+1][y-1]){
			d[x+1][y-1]=d[x][y]+2;
			qx.push(x+1);
			qy.push(y-1);
		}else if(d[x][y]+3<d[x+1][y-1]){
			d[x+1][y-1]=d[x][y]+3;
			qx.push(x+1);
			qy.push(y-1);
		}
	}
	if(x<m&&y<m&&a[x+1][y+1]!=2&&a[x][y+1]==2&&a[x+1][y]==2){
		if(a[x][y]==a[x+1][y+1]&&d[x][y]+2<d[x+1][y+1]){
			d[x+1][y+1]=d[x][y]+2;
			qx.push(x+1);
			qy.push(y+1);
		}else if(d[x][y]+3<d[x+1][y+1]){
			d[x+1][y+1]=d[x][y]+3;
			qx.push(x+1);
			qy.push(y+1);
		}
	}
	if(x>1&&y<m&&a[x-1][y+1]!=2&&a[x][y+1]==2&&a[x-1][y]==2){
		if(a[x][y]==a[x-1][y+1]&&d[x][y]+2<d[x-1][y+1]){
			d[x-1][y+1]=d[x][y]+2;
			qx.push(x-1);
			qy.push(y+1);
		}else if(d[x][y]+3<d[x-1][y+1]){
			d[x-1][y+1]=d[x][y]+3;
			qx.push(x-1);
			qy.push(y+1);
		}
	}
	if(x>1&&y>1&&a[x-1][y-1]!=2&&a[x][y-1]==2&&a[x-1][y]==2){
		if(a[x][y]==a[x-1][y-1]&&d[x][y]+2<d[x-1][y-1]){
			d[x-1][y-1]=d[x][y]+2;
			qx.push(x-1);
			qy.push(y-1);
		}else if(d[x][y]+3<d[x-1][y-1]){
			d[x-1][y-1]=d[x][y]+3;
			qx.push(x-1);
			qy.push(y-1);
		}
	}
	if(x>2&&a[x-2][y]!=2&&a[x-1][y]==2){
		if(a[x][y]==a[x-2][y]&&d[x][y]+2<d[x-2][y]){
			d[x-2][y]=d[x][y]+2;
			qx.push(x-2);
			qy.push(y);
		}else if(d[x][y]+3<d[x-2][y]){
			d[x-2][y]=d[x][y]+3;
			qx.push(x-2);
			qy.push(y);
		}
	}
	if(x<m-1&&a[x+2][y]!=2&&a[x+1][y]==2){
		if(a[x][y]==a[x+2][y]&&d[x][y]+2<d[x+2][y]){
			d[x+2][y]=d[x][y]+2;
			qx.push(x+2);
			qy.push(y);
		}else if(d[x][y]+3<d[x+2][y]){
			d[x+2][y]=d[x][y]+3;
			qx.push(x+2);
			qy.push(y);
		}
	}
	if(y>2&&a[x][y-2]!=2&&a[x][y-1]==2){
		if(a[x][y]==a[x][y-2]&&d[x][y]+2<d[x][y-2]){
			d[x][y-2]=d[x][y]+2;
			qx.push(x);
			qy.push(y-2);
		}else if(d[x][y]+3<d[x][y-2]){
			d[x][y-2]=d[x][y]+3;
			qx.push(x);
			qy.push(y-2);
		}
	}
	if(y<m-1&&a[x][y+2]!=2&&a[x][y+1]==2){
		if(a[x][y]==a[x][y+2]&&d[x][y]+2<d[x][y+2]){
			d[x][y+2]=d[x][y]+2;
			qx.push(x);
			qy.push(y+2);
		}else if(d[x][y]+3<d[x][y+2]){
			d[x][y+2]=d[x][y]+3;
			qx.push(x);
			qy.push(y+2);
		}
	}
	qx.pop();
	qy.pop();
	if(!qx.empty()){
		bfs(qx.front(),qy.front());
	}
}
int main(){
	cin>>m>>n;
	for(int i=1;i<=m;i++)
		for(int j=1;j<=m;j++){
			a[i][j]=2;
			d[i][j]=inf;
		}
	d[1][1]=0;
	qx.push(1);
	qy.push(1);
	for(int i=1;i<=n;i++){
		cin>>t1>>t2>>c;
		a[t1][t2]=c;
	}
	bfs(1,1);
	if(d[m][m]==inf)cout<<-1;
	else cout<<d[m][m];
	return 0;
}

var n,m,i,x,y,z,max:longint;
a,f:array[0..101,0..101]of longint;
procedure sc(x,y,t,w,z:longint);
begin
if (x<1) or (x>n) or (y<1) or (y>n) then
  exit;
if t>=max then exit;
if (x=n) and (y=n) then
  begin
  max:=t;
  exit;
  end;
if f[x,y]<=t then exit;
f[x,y]:=t;
if (a[x-1,y]=-1) and (w=0) then
  sc(x-1,y,t+2,1,z);
if (a[x+1,y]=-1) and (w=0) then
  sc(x+1,y,t+2,1,z);
if (a[x,y-1]=-1) and (w=0) then
  sc(x,y-1,t+2,1,z);
if (a[x,y+1]=-1) and (w=0) then
  sc(x,y+1,t+2,1,z);
if a[x-1,y]<>-1 then
  begin
  if a[x-1,y]=z then
    sc(x-1,y,t,0,z)
  else
    sc(x-1,y,t+1,0,a[x-1,y]);
  end;
if a[x+1,y]<>-1 then
  begin
  if a[x+1,y]=z then
    sc(x+1,y,t,0,z)
  else
    sc(x+1,y,t+1,0,a[x+1,y]);
  end;
if a[x,y-1]<>-1 then
  begin
  if a[x,y-1]=z then
    sc(x,y-1,t,0,z)
  else
    sc(x,y-1,t+1,0,a[x,y-1]);
  end;
if a[x,y+1]<>-1 then
  begin
  if a[x,y+1]=z then
    sc(x,y+1,t,0,z)
  else
    sc(x,y+1,t+1,0,a[x,y+1]);
  end;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
fillchar(a,sizeof(a),255);
fillchar(f,sizeof(f),101);
for i:=1 to m do
  begin
  readln(x,y,z);
  a[x,y]:=z;
  end;
max:=maxlongint;
sc(1,1,0,0,a[1,1]);
if max=maxlongint then
  writeln(-1)
else
  writeln(max);
close(input);
close(output);
end.

var
    dp,a,b:array[0..1000000] of int64;
    i:longint;
    l,r,mid,n,m,p,ans,sum:int64;

function max(x,y:int64):int64;

begin
    if x>y then
        exit(x) else
        exit(y);
end;

function check(x:int64):boolean;
var
    ls,rs,sum:int64;
    i,j:longint;

begin
    ls:=max(1,m-x);
    rs:=m+x;
    if (a[1]<ls) or (a[1]>rs) then
        exit(false);
    for i:=1 to n do
        dp[i]:=-maxlongint;
    dp[1]:=max(0,b[1]);
    sum:=max(0,dp[1]);
    if sum>=p then
        exit(true);
    for i:=2 to n do
    begin
        if (a[i]>=ls) and (a[i]<=rs) then
            dp[i]:=max(dp[i],b[i]);
        for j:=i-1 downto 1 do
            if (a[i]-a[j]>=ls) and (a[i]-a[j]<=rs) then
            begin
                dp[i]:=max(dp[i],dp[j]+a[i]);
            end else
            begin
                break;
            end;
        sum:=max(sum,dp[i]);
        if sum>=p then
            exit(true);
    end;
    exit(false);
end;

begin
    assign(input,'jump.in'); reset(input);
    assign(output,'jump.out'); rewrite(output);
        readln(n,m,p);
        for i:=1 to n do
        begin
            readln(a[i],b[i]);
            if b[i]>0 then
                inc(sum,b[i]);
        end;
        if sum<p then
        begin
            writeln(-1);
            close(input); close(output);
            halt;
        end;
        l:=0;
        r:=1000000000;
        ans:=-1;
        while l<r do
        begin
            mid:=(l+r) div 2;
            if check(mid) then
            begin
                ans:=mid;
                r:=mid-1;
            end else
            begin
                l:=mid+1;
            end;
        end;
        writeln(ans);
    close(input); close(output);
end.
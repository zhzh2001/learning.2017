var
    dx:array[1..4] of longint=(-1,0,0,1);
    dy:array[1..4] of longint=(0,-1,1,0);
    a:array[0..1000,0..1000] of longint;
    visit:array[0..1000,0..1000] of boolean;
    n,m,i,j,x,y,z,ans:longint;

procedure dfs(x,y,s,color:longint);
var
    i,xx,yy:longint;

begin
    if s>=ans then
        exit;
    if (x=m) and (y=m) then
    begin
        ans:=s;
        exit;
    end;
    for i:=1 to 4 do
    begin
        xx:=x+dx[i];
        yy:=y+dy[i];
        if (xx>0) and (yy>0) and (xx<=m) and (yy<=m) and not visit[xx][yy] then
        begin
            if a[x][y]=-1 then
            begin
                if a[xx][yy]<>-1 then
                begin
                    if color=a[xx][yy] then
                        dfs(xx,yy,s,a[xx][yy]) else
                        dfs(xx,yy,s+1,a[xx][yy]);
                end;
            end else
            begin
                if a[xx][yy]=a[x][y] then
                begin
                    visit[xx][yy]:=true;
                    dfs(xx,yy,s,a[x][y]);
                    visit[xx][yy]:=false;
                end else
                if a[xx][yy]<>-1 then
                begin
                    visit[xx][yy]:=true;
                    dfs(xx,yy,s+1,a[xx][yy]);
                    visit[xx][yy]:=false;
                end else
                begin
                    visit[xx][yy]:=true;
                    dfs(xx,yy,s+2,a[x][y]);
                    dfs(xx,yy,s+3,1-a[x][y]);
                    visit[xx][yy]:=false;
                end;
            end;
        end;
    end;
end;

begin
    assign(input,'chess.in'); reset(input);
    assign(output,'chess.out'); rewrite(output);
        readln(m,n);
        fillchar(a,sizeof(a),255);
        for i:=1 to n do
        begin
            readln(x,y,z);
            a[x][y]:=z;
        end;
        ans:=maxlongint;
        visit[1][1]:=true;
        dfs(1,1,0,a[1][1]);
        if ans=maxlongint then
            writeln(-1) else
            writeln(ans);
    close(input); close(output);
end.
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<string>
#include<cstdlib>
using namespace std;
int mmaapp[101][101],n,m,t=0,ans[100001];
bool bo[101][101];
void mrzdfs(int xx,int yy,int mag,int mone)
{
	if(xx==m&&yy==m)
	{
		t++;
		ans[t]=mone;
	}
	if(yy<m && bo[xx][yy+1]==0) 
	{
		if(mmaapp[xx][yy+1]==mmaapp[xx][yy] && mmaapp[xx][yy]!=0)
		{
			bo[xx][yy+1]=1;
			mrzdfs(xx,yy+1,1,mone);
			bo[xx][yy+1]=0;
		}
		if(mmaapp[xx][yy+1]!=mmaapp[xx][yy] && mmaapp[xx][yy]!=0 && mmaapp[xx][yy+1]!=0)
		{
			bo[xx][yy+1]=1;
			mrzdfs(xx,yy+1,1,mone+1);
			bo[xx][yy+1]=0;
		}
		if(mmaapp[xx][yy+1]==0 && mag==1)
		{
			bo[xx][yy+1]=1;
			mmaapp[xx][yy+1]=mmaapp[xx][yy];
			mrzdfs(xx,yy+1,0,mone+2);
			mmaapp[xx][yy+1]=0;
			bo[xx][yy+1]=0;
		} 
	}
	if(xx<m && bo[xx+1][yy]==0) 
	{
		if(mmaapp[xx+1][yy]==mmaapp[xx][yy] && mmaapp[xx][yy]!=0)
		{
			bo[xx+1][yy]=1;
			mrzdfs(xx+1,yy,1,mone);
			bo[xx+1][yy]=0;
		}
		if(mmaapp[xx+1][yy]!=mmaapp[xx][yy] && mmaapp[xx][yy]!=0 && mmaapp[xx+1][yy]!=0)
		{
			bo[xx+1][yy]=1;
			mrzdfs(xx+1,yy,1,mone+1);
			bo[xx+1][yy]=0;
		}
		if(mmaapp[xx+1][yy]==0 && mag==1)
		{
			bo[xx+1][yy]=1;
			mmaapp[xx+1][yy]=mmaapp[xx][yy];
			mrzdfs(xx+1,yy,0,mone+2);
			mmaapp[xx+1][yy]=0;
			bo[xx+1][yy]=0;
		} 
	}
	if(yy>1 && bo[xx][yy-1]==0) 
	{
		if(mmaapp[xx][yy-1]==mmaapp[xx][yy] && mmaapp[xx][yy]!=0)
		{
			bo[xx][yy-1]=1;
			mrzdfs(xx,yy-1,1,mone);
			bo[xx][yy-1]=0;
		}
		if(mmaapp[xx][yy-1]!=mmaapp[xx][yy] && mmaapp[xx][yy]!=0 && mmaapp[xx][yy-1]!=0)
		{
			bo[xx][yy-1]=1;
			mrzdfs(xx,yy-1,1,mone+1);
			bo[xx][yy-1]=0;
		}
		if(mmaapp[xx][yy-1]==0 && mag==1)
		{
			bo[xx][yy-1]=1;
			mmaapp[xx][yy-1]=mmaapp[xx][yy];
			mrzdfs(xx,yy-1,0,mone+2);
			mmaapp[xx][yy-1]=0;
			bo[xx][yy-1]=0;
		} 
	}
	if(xx>1 && bo[xx-1][yy]==0) 
	{
		if(mmaapp[xx-1][yy]==mmaapp[xx][yy] && mmaapp[xx][yy]!=0)
		{
			bo[xx-1][yy]=1;
			mrzdfs(xx-1,yy,1,mone);
			bo[xx-1][yy]=0;
		}
		if(mmaapp[xx-1][yy]!=mmaapp[xx][yy] && mmaapp[xx][yy]!=0 && mmaapp[xx-1][yy]!=0)
		{
			bo[xx-1][yy]=1;
			mrzdfs(xx-1,yy,1,mone+1);
			bo[xx-1][yy]=0;
		}
		if(mmaapp[xx-1][yy]==0 && mag==1)
		{
			bo[xx-1][yy]=1;
			mmaapp[xx-1][yy]=mmaapp[xx][yy];
			mrzdfs(xx-1,yy,0,mone+2);
			mmaapp[xx-1][yy]=0;
			bo[xx-1][yy]=0;
		} 
	}
	return;
}
int main()
{
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	int aa,bb,cc;
	memset(mmaapp,0,sizeof(mmaapp));
	memset(bo,0,sizeof(bo));
	for(int i=1;i<=n;i++) 
	{
		scanf("%d%d%d",&aa,&bb,&cc);
		mmaapp[aa][bb]=cc+1;
	}
	bo[1][1]=1;
	mrzdfs(1,1,1,0);
	if(t==0) printf("-1\n");
	else
	{
		long long mrzou=0x7fffffff;
		for(int i=1;i<=t;i++) if(ans[i]<mrzou) mrzou=ans[i];
		printf("%lld\n",mrzou); 
	}
	return 0;
}

const
  d:array[0..8] of longint=(1,10,100,1000,10000,100000,1000000,10000000,100000000);
var
  a:array[1..1000] of longint;
  n,m,i,j,l,x:longint;
  b:boolean;
procedure qs(l,r:longint);
  var
    i,j,p,t:longint;
  begin
    i:=l;
    j:=r;
    p:=a[(l+r) div 2];
    repeat
      while a[i]<p do inc(i);
      while a[j]>p do dec(j);
      if i<=j then begin
        t:=a[i];
        a[i]:=a[j];
        a[j]:=t;
        inc(i);
        dec(j)
      end;
    until i>j;
    if l<j then qs(l,j);
    if i<r then qs(i,r)
  end;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  qs(1,n);
  for i:=1 to m do begin
    readln(l,x);
    b:=true;
    for j:=1 to n do if a[j] mod d[l]=x then begin
      writeln(a[j]);
      b:=false;
      break
    end;
    if b then writeln(-1)
  end;
  close(input);
  close(output)
end.

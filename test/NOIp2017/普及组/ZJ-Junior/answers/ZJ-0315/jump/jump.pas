var
  a,b,f:array[0..500000] of longint;
  n,d,k,i,l,r,m,t:longint;
function max(a,b:longint):longint;
  begin
    if a>b then exit(a)
    else exit(b)
  end;
function check(lx,rx:longint):boolean;
  var
    i,j,s:longint;
  begin
    fillchar(f,sizeof(f),0);
    for i:=1 to n do begin
      f[i]:=-1000000000;
      for j:=i-1 downto 0 do if a[i]-a[j]>=lx then if a[i]-a[j]<=rx then f[i]:=max(f[i],f[j]+b[i])
      else break
    end;
    s:=0;
    for i:=1 to n do if f[i]>s then s:=f[i];
    if s>=k then exit(true)
    else exit(false)
  end;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  readln(n,d,k);
  m:=0;
  r:=0;
  t:=0;
  a[0]:=0;
  b[0]:=0;
  for i:=1 to n do begin
    readln(a[i],b[i]);
    if b[i]>=0 then begin
      inc(m,b[i]);
      r:=max(r,abs(a[i]-t-d));
      t:=a[i]
    end;
  end;
  if m<k then begin
    write(-1);
    close(input);
    close(output);
    halt
  end;
  l:=0;
  while l<r do begin
    m:=(l+r) div 2;
    t:=d-m;
    if t<1 then t:=1;
    if check(t,d+m) then r:=m
    else l:=m+1
  end;
  write(l);
  close(input);
  close(output)
end.

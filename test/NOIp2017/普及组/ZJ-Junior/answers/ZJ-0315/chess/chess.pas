const
  dx:array[1..4] of integer=(1,-1,0,0);
  dy:array[1..4] of integer=(0,0,1,-1);
var
  a:array[0..101,0..101] of integer;
  f:array[0..101,0..101,0..2] of longint;
  n,m,i,x,y,t:longint;
function min(a,b:longint):longint;
  begin
    if a<b then exit(a)
    else exit(b)
  end;
function dp(x,y,p:longint):longint;
  var
    i:longint;
  begin
    if (x<1) or (x>n) or (y<1) or (y>n) then exit(maxint);
    if (x=n) and (y=n) then exit(0);
    if f[x,y,p]>-1 then exit(f[x,y,p]);
    f[x,y,p]:=maxint;
    if a[x,y]=0 then
      for i:=1 to 4 do if a[x+dx[i],y+dy[i]]>0 then if a[x+dx[i],y+dy[i]]=p then f[x,y,p]:=min(f[x,y,p],dp(x+dx[i],y+dy[i],p))
      else f[x,y,p]:=min(f[x,y,p],dp(x+dx[i],y+dy[i],a[x+dx[i],y+dy[i]])+1)
      else
    else for i:=1 to 4 do if a[x+dx[i],y+dy[i]]>0 then if a[x+dx[i],y+dy[i]]=p then f[x,y,p]:=min(f[x,y,p],dp(x+dx[i],y+dy[i],p))
    else f[x,y,p]:=min(f[x,y,p],dp(x+dx[i],y+dy[i],a[x+dx[i],y+dy[i]])+1)
    else if p=1 then f[x,y,p]:=min(f[x,y,p],min(dp(x+dx[i],y+dy[i],1),dp(x+dx[i],y+dy[i],2)+1)+2)
    else f[x,y,p]:=min(f[x,y,p],min(dp(x+dx[i],y+dy[i],1)+1,dp(x+dx[i],y+dy[i],2))+2);
    exit(f[x,y,p])
  end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  fillchar(a,sizeof(a),0);
  for i:=1 to m do begin
    readln(x,y,t);
    a[x,y]:=t+1
  end;
  for x:=1 to n do
    for y:=1 to n do
      for i:=0 to 2 do f[x,y,i]:=-1;
  t:=dp(1,1,a[1,1]);
  if t>=maxint then write(-1)
  else write(t);
  close(input);
  close(output)
end.

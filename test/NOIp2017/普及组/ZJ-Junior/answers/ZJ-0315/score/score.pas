var
  a,b,c,s:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(a,b,c);
  s:=a div 5+b div 10*3+c div 2;
  write(s);
  close(input);
  close(output)
end.
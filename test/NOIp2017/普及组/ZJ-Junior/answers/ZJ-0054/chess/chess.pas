const p1:array[1..4] of longint=(0,0,-1,1);
      p2:array[1..4] of longint=(-1,1,0,0);
var a:array[0..100,0..100] of longint;
    b:array[0..100,0..100] of boolean;
    c:array[1..1000] of longint;
    m,n,min,i,j,x,y:longint;
    f:boolean;
procedure dfs(n1,n2,tot:longint;f1:boolean);
var i:longint;
begin
  if (n1=m) and (n2=m) then if tot<min then begin min:=tot;exit;end;
  for i:=1 to 4 do
  if b[n1+p1[i],n2+p2[i]] then
    if a[n1+p1[i],n2+p2[i]]<>2 then
    begin
      if a[n1,n2]<>a[n1+p1[i],n2+p2[i]] then
        inc(tot);
      b[n1,n2]:=false;
      dfs(n1+p1[i],n2+p2[i],tot,true);
      b[n1,n2]:=true;
      dec(tot);
    end
    else
      if f1 then
      begin
        f1:=false;
        tot:=tot+2;
        b[n1,n2]:=false;
        a[n1+p1[i],n2+p2[i]]:=a[x,y];
        dfs(n1+p1[i],n2+p2[i],tot,false);
        b[n1,n2]:=true;
        tot:=tot-2;
        a[n1+p1[i],n2+p2[i]]:=2;
        f1:=true;
      end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  fillchar(b,sizeof(b),true);
  f:=true;min:=maxlongint;
  for i:=0 to 100 do
    for j:=0 to 100 do
    begin
      a[i,j]:=2;
      if (i=0) or (j=0) then
        b[i,j]:=false;
    end;
  fillchar(c,sizeof(c),0);
  readln(m,n);
  for i:=1 to n do
  begin
    readln(x,y,c[i]);
    a[x,y]:=c[i];
  end;
  dfs(1,1,0,f);
  if min=maxlongint then writeln(-1)
  else writeln(min);
  close(input);close(output);
end.

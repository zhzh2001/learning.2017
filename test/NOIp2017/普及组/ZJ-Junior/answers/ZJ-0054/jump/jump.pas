var x,s:array[1..500000] of longint;
    n,d,k,i,tot,max,min:longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  tot:=0;max:=-maxlongint;min:=maxlongint;
  for i:=1 to n do readln(x[i],s[i]);
  for i:=1 to n do
    if s[i]>0 then tot:=tot+s[i];
  if tot<k then
  begin
    writeln(-1);
    close(input);close(output);
    halt;
  end;
  tot:=0;
  for i:=1 to n-1 do
  begin
    if x[i+1]-x[i]>max then max:=x[i+1]-x[i];
    if x[i+1]-x[i]<min then min:=x[i+1]-x[i];
  end;
  if (max-min) mod 2=0 then
    tot:=(max-min) div 2
  else
    tot:=(max-min) div 2+1;
  writeln(tot);
  close(input);close(output);
end.

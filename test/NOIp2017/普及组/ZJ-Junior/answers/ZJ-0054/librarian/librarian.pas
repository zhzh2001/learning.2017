var a,b,c,tot:array[1..1000] of longint;
    n,p,i,j,s:longint;
function power(x,y:longint):longint;
var l,ans:longint;
begin
  ans:=1;
  for l:=1 to y do
    ans:=ans*x;
  exit(ans);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  fillchar(b,sizeof(b),0);
  for i:=1 to 1000 do
    tot[i]:=maxlongint;
  s:=0;
  readln(n,p);
  for i:=1 to n do readln(a[i]);
  for j:=1 to p do readln(b[j],c[j]);
  for i:=1 to p do
  begin
    for j:=1 to n do
    begin
      s:=a[j] mod (power(10,b[i]));
      if s=c[i] then
        if a[j]<tot[i] then tot[i]:=a[j];
    end;
    if tot[i]=maxlongint then writeln(-1)
    else writeln(tot[i]);
  end;
  close(input);close(output);
end.

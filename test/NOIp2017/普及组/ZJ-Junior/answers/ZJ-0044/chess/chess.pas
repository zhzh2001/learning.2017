var
  a,a1:array[0..101,0..101]of char;
  f:array[0..101,0..101]of boolean;
  n,m,x,y,z,i,j,ans:longint;
procedure dfs(x,y,coin:longint;mf:boolean);
var
  i:longint;
begin
  if coin>ans then
    exit;
  if (x=m)and(y=m) then
  begin
    if ans>coin then
      ans:=coin;
    exit;
  end;
  if a1[x,y]<>'w' then
    mf:=false;
  for i:=1 to 4 do
    case i of
      1:if (x-1>0)and not f[x-1,y] then  //shang
        begin
          f[x-1,y]:=true;
          if (a[x-1,y]='w')and not mf then
          begin
            a[x,y+1]:=a[x,y];
            dfs(x,y+1,coin+2,true);
            a[x,y+1]:='w';
          end;
          if (a[x-1,y]<>'w')and(a[x,y]<>a[x-1,y]) then
            dfs(x-1,y,coin+1,mf);
          if (a[x-1,y]<>'w')and(a[x,y]=a[x-1,y]) then
            dfs(x-1,y,coin,mf);
          f[x-1,y]:=false;
        end;
      2:if (y+1<=m)and not f[x,y+1] then  //you
        begin
          f[x,y+1]:=true;
          if (a[x,y+1]='w')and not mf then
          begin
            a[x,y+1]:=a[x,y];
            dfs(x,y+1,coin+2,true);
            a[x,y+1]:='w';
          end;
          if (a[x,y+1]<>'w')and(a[x,y]<>a[x,y+1]) then
            dfs(x,y+1,coin+1,mf);
          if (a[x,y+1]<>'w')and(a[x,y]=a[x,y+1]) then
            dfs(x,y+1,coin,mf);
          f[x,y+1]:=false;
        end;
      3:if (x+1<=m)and not f[x+1,y] then  //xia
        begin
          f[x+1,y]:=true;
          if (a[x+1,y]='w')and not mf then
          begin
            a[x,y+1]:=a[x,y];
            dfs(x,y+1,coin+2,true);
            a[x,y+1]:='w';
          end;
          if (a[x+1,y]<>'w')and(a[x,y]<>a[x+1,y]) then
            dfs(x+1,y,coin+1,mf);
          if (a[x+1,y]<>'w')and(a[x,y]=a[x+1,y]) then
            dfs(x+1,y,coin,mf);
          f[x+1,y]:=false;
        end;
      4:if (y-1>0)and not f[x,y-1] then  //zuo
        begin
          f[x,y-1]:=true;
          if (a[x,y-1]='w')and not mf then
          begin
            a[x,y+1]:=a[x,y];
            dfs(x,y+1,coin+2,true);
            a[x,y+1]:='w';
          end;
          if (a[x,y-1]<>'w')and(a[x,y]<>a[x,y-1]) then
            dfs(x,y-1,coin+1,mf);
          if (a[x,y-1]<>'w')and(a[x,y]=a[x,y-1]) then
            dfs(x,y-1,coin,mf);
          f[x,y-1]:=false;
        end;
    end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:='w';
  for i:=1 to n do
  begin
    readln(x,y,z);
    if z=0 then
      a[x,y]:='r' else
      a[x,y]:='y';
  end;
  a1:=a;
  ans:=maxlongint;
  dfs(1,1,0,false);
  if ans<>maxlongint then
    writeln(ans) else
    writeln(-1);
  close(input);close(output);
end.
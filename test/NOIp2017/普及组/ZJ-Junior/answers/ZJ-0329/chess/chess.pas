var
 m,n,x,y,k,color,i,j,ans,l:longint;
 a:array[1..100,1..100]of integer;
 step:array[1..100,1..100]of longint;
 flag:boolean;
begin
 assign(input,'chess.in');
 assign(output,'chess.out');
 reset(input);
 rewrite(output); 
 readln(m,n);
 step[1,1]:=0;
 ans:=maxlongint;
 for i:=1 to m do
  for j:=1 to m do
   a[i,j]:=2;
 for i:=1 to n do
  begin
   read(x,y);
   readln(color);
   a[x,y]:=color;
  end;
 if (a[m,m-1]=2)and(a[m,m]=2)and(a[m-1,m]=2) then
                                              begin
                                               writeln(-1); 
                                               close(input);
                                               close(output); 
                                               halt;
                                              end;
 if (a[m-1,m-1]=2)and(a[m,m-1]=2)and(a[m-1,m]=2) then
                                              begin
                                               writeln(-1); 
                                               close(input);
                                               close(output); 
                                               halt;
                                              end;
 for i:=1 to m-1 do
  begin
   if (a[i+1,1]=2)and(flag=false) then
                      begin
                        step[i+1,1]:=step[i,1]+2;
                        a[i+1,1]:=a[i,1];
                        flag:=true;
                        continue;
                      end;
   if a[i,1]=a[i+1,1] then
                       begin
                        step[i+1,1]:=step[i,1];
                        if flag then a[i,1]:=2;
                        flag:=false;
                        continue;
                       end;
    if (a[i,1]<>a[i+1,1])and(a[i+1,1]<>2) then
                       begin
                        step[i+1,1]:=step[i,1]+1;
                        if flag then a[i,1]:=2;
                         flag:=false;
                        continue;
                       end;
    step[i+1,1]:=maxlongint-10000;
   end;
  for j:=1 to m-1 do
  begin
   if (a[1,j+1]=2)and(flag=false) then
                      begin
                        step[1,j+1]:=step[1,j]+2;
                        a[1,j+1]:=a[1,j];
                        flag:=true;
                        continue;
                      end;
   if a[1,j]=a[1,j+1] then
                       begin
                        step[1,j+1]:=step[1,j];
                        if flag then a[1,j]:=2;
                        flag:=false;
                        continue;
                       end;
    if (a[1,j]<>a[1,j+1])and(a[1,j+1]<>2) then
                       begin
                        step[1,j+1]:=step[1,j]+1;
                        if flag then a[1,j]:=2;
                        flag:=false;
                        continue;
                       end;
    step[1,j+1]:=maxlongint-10000;
  end;
  for i:=2 to m do
   for j:=2 to m do
    begin
     if step[i,j-1]>step[i-1,j] then begin k:=1; step[i,j]:=step[i-1,j]; end
                                else begin k:=2; step[i,j]:=step[i,j-1]; end;
     case k of
      1: begin
         if a[i,j]=a[i-1,j] then begin l:=0;continue; end;
         if (a[i,j]=2) then begin
                           a[i,j]:=a[i-1,j];
                           step[i,j]:=step[i,j]+2;
                           l:=1;
                           continue;
                          end;
         if a[i,j]<>a[i-1,j] then begin inc(step[i,j]); l:=0; end;
         end;
      2: begin
         if a[i,j]=a[i,j-1] then begin l:=0; continue;end;
         if a[i,j]=2 then begin
                           a[i,j]:=a[i,j-1];
                           step[i,j]:=step[i,j]+2;
                           l:=1;
                           continue;
                          end;
         if a[i,j]<>a[i,j-1] then begin inc(step[i,j]); l:=0; end;
         end;
       end;
   end;
  writeln(step[m,m]);
 close(input);
 close(output); 
end.

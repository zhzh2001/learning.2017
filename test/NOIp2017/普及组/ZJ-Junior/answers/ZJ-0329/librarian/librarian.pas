var
 a,b:array[1..1000]of longint;
 k,n,q,m,w,i,j:longint;
 flag:boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'librarian.in');
 assign(output,'librarian.out');
 reset(input);
 rewrite(output);
 readln(n,q);
 for i:=1 to n do readln(a[i]);
 sort(1,n);
 for i:=1 to q do
  begin
   read(k);
   m:=1;
   for j:=1 to k do m:=m*10;
   readln(w);
   flag:=false;
   for j:=1 to n do
    if (a[j]mod m=w)and(flag=false) then
                         begin
                           b[i]:=a[j];
                           flag:=true;
                         end;
   if flag=false then b[i]:=-1;
  end;
 for i:=1 to q do writeln(b[i]);
 close(input);
 close(output);
end.
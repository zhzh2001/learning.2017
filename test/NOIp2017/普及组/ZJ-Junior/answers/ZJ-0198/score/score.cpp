#include <cstdio> 
using namespace std;
int main()
{
	freopen("score.in", "r", stdin);
	freopen("score.out", "w", stdout);
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	a /= 10;
	b /= 10;
	c /= 10;
	int d;
	d = a * 2 + b * 3 + c * 5;
	printf("%d", d);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

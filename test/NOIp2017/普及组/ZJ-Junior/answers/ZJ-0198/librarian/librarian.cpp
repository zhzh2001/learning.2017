#include <cstdio>
#include <algorithm>

using namespace std;

int books[1005];

bool _Comp(int i, int j)
{
	if (i > j)
		return false;
	int a, b;
	while (i > 0)
	{
		a = i % 10;
		b = j % 10;
		if (a != b)
			return false;
		i /= 10;
		j /= 10;
	}
	return true;
}

int main()
{
	freopen("librarian.in", "r", stdin);
	freopen("librarian.out", "w", stdout);
	int n, q;
	scanf("%d %d", &n, &q);
	int i;
	for (i = 0; i < n; i++)
		scanf("%d", &books[i]);
	sort(books, books + n);
	int que, j, len;
	for (i = 0; i < n; i++)
	{
		scanf("%d %d", &len, &que);
		for (j = 0; j < n; j++)
		{
			if(_Comp(que, books[j]))
			{
				printf("%d\n", books[j]);
				break;
			}
		}
		if (j == n)
			printf("-1\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

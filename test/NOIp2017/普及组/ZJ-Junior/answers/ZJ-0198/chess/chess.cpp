#include <cstdio>
#include <vector>
#include <queue>

using namespace std;

int _map[105][105];
int offset[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
int offset2[8][2] = {{1, -1}, {1, 1}, {-1, 1}, {-1, -1}, {2, 0}, {-2, 0}, {0, 2}, {0, -2}};

struct Edge{
	int x, y;
	int dis;
	int next;
};

struct Pos{
	int x;
	int y;
};

vector<Edge> edges;

int head[105][105];

void add_edge(int x1, int y1, int x2, int y2, int dis)
{
	Edge _new;
	_new.x  = x2;
	_new.y = y2;
	_new.dis = dis;
	_new.next = head[x1][y1];
	edges.push_back(_new);
	head[x1][y1] = edges.size()-1;
}

queue<Pos> que;
int Dis[105][105];
bool in_que[105][105];

void SPFA()
{
	Pos _s, _n;
	_s.x = 1;
	_s.y = 1;
	que.push(_s);
	Dis[1][1] = 0;
	in_que[1][1] = true;
	int c_e, tx, ty;
	while (que.size() > 0)
	{
		_s = que.front();
		que.pop();
		in_que[_s.x][_s.y] = false;
		c_e = head[_s.x][_s.y];
		while (c_e != 0)
		{
			tx = edges[c_e].x;
			ty = edges[c_e].y;
			if (Dis[tx][ty] > Dis[_s.x][_s.y] + edges[c_e].dis || Dis[tx][ty] == -1)
			{
				Dis[tx][ty] = Dis[_s.x][_s.y] + edges[c_e].dis;
				if (!in_que[tx][ty])
				{
					in_que[tx][ty] = true;
					_n.x = tx;
					_n.y = ty;
					que.push(_n);
				}
			}
			c_e = edges[c_e].next;
		}
	}
}

int main()
{
	freopen("chess.in", "r", stdin);
	freopen("chess.out", "w", stdout);
	edges.clear();
	Edge temp;
	edges.push_back(temp);
	int m, n;
	scanf("%d %d", &m, &n);
	int i, j;
	for (i = 1; i <= m; i++)
	{
		for (j = 1; j <= m; j++)
		{
			in_que[i][j] = false;
			_map[i][j] = 0;
			head[i][j] = 0;
			Dis[i][j] = -1;
		}
	}
	int x, y, z;
	int _a, _b, _dis;
	for (i = 0; i < n; i++)
	{
		scanf("%d %d %d", &x, &y, &z);
		_map[x][y] = z+1;
		for (j = 0; j < 4; j++)
		{
			_a = x + offset[j][0];
			_b = y + offset[j][1];
			if (_a >= 1 && _a <= m && _b >= 1 && _b <= m && _map[_a][_b] > 0)
			{
				_dis = 0;
				if (_map[x][y] != _map[_a][_b])
					_dis++;
				add_edge(_a, _b, x, y, _dis);
				add_edge(x, y, _a, _b, _dis);
			}
		}
		for (j = 0; j < 4; j++)
		{
			_a = x + offset2[j][0];
			_b = y + offset2[j][1];
			if (_a >= 1 && _a <= m && _b >= 1 && _b <= m && _map[_a][_b] > 0 && (_map[x][_b] == 0 || _map[_a][y] == 0))
			{
				_dis = 2;
				if (_map[x][y] != _map[_a][_b])
					_dis++;
				add_edge(_a, _b, x, y, _dis);
				add_edge(x, y, _a, _b, _dis);
			}
		}
		for (j = 4; j < 8; j++)
		{
			_a = x + offset2[j][0];
			_b = y + offset2[j][1];
			if (_a >= 1 && _a <= m && _b >= 1 && _b <= m && _map[_a][_b] > 0)
			{
				_dis = 2;
				if (_map[x][y] != _map[_a][_b])
					_dis++;
				add_edge(_a, _b, x, y, _dis);
				add_edge(x, y, _a, _b, _dis);
			}
		}
	}
	if (!_map[m][m])
	{
		x = m, y = m;
		for (j = 0; j < 4; j++)
		{
			_a = x + offset[j][0];
			_b = y + offset[j][1];
			if (_a >= 1 && _a <= m && _b >= 1 && _b <= m && _map[_a][_b] > 0)
			{
				_dis = 2;
				add_edge(_a, _b, x, y, _dis);
				add_edge(x, y, _a, _b, _dis);
			}
		}
	}
	SPFA();
	printf("%d", Dis[m][m]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <cstdio>

long long x[500005], s[500005], f[500005];

int n, d;
long long k;

bool jud1(int mid)
{
	int i;
	int last = 0;
	long long ans_now = s[0], ans_jud = 0;
	for (i = 1; i < n; i++)
	{
		if (x[i] - x[last] > (d + mid) || ans_now < 0)
		{
			if (ans_now > ans_jud)
			{
				ans_jud = ans_now;
				ans_now = 0;
			}
		}
		else
			ans_now += s[i];
		last = i;
	}
	if (ans_now > ans_jud)
		ans_jud = ans_now;
	if (ans_jud >= k)
		return true;
	else
		return false;
}

bool jud(int mid)
{
	int i, j;
	int ss;
	if (d - mid > 0)
		ss = d - mid;
	else
		ss = 1;
	int ans_jud = 0;
	for (i = 0; i < n; i++)
	{
		f[i] = 0;
		for (j = 0; j < i; j++)
			if (f[j] != 0 && x[j] <= x[i] - ss && x[j] >= x[i] - mid - d)
			{
				if (f[i] < f[j] + s[i])
					f[i] = f[j] + s[i];
			}
		if (f[i] == 0)
		{
			for (j = 0; ; j++)
			{
				if (ss * j <= x[i] && x[i] <= (mid+d) * j)
				{
					f[i] = s[i];
					break;
				}
				if (x[i] < ss * j)
					break;
			}
		}
		if (f[i] > ans_jud)
			ans_jud = f[i];
	}
	if (ans_jud >= k)
		return true;
	else
		return false;
}

int main()
{
	freopen("jump.in", "r", stdin);
	freopen("jump.out", "w", stdout);
	scanf("%d %d %lld", &n, &d, &k);
	int i, Maxx = d-1;
	for (i = 0; i < n; i++)
	{
		scanf("%lld %lld", &x[i], &s[i]);
		if (x[i] > Maxx)
			Maxx = x[i];
	}
	if (d == 1)
	{
		int l = 0, r = Maxx, mid, ans = -1;
		while (l <= r)
		{
			mid = (l + r) / 2;
			if (jud1(mid))
			{
				r = mid - 1;
				ans = mid;
			}
			else
				l = mid + 1;
		}
		printf("%d", ans);
	}
	else if (n <= 3000)
	{
		int l = 0, r = Maxx, mid, ans = -1;
		while (l <= r)
		{
			mid = (l + r) / 2;
			if (jud(mid))
			{
				r = mid - 1;
				ans = mid;
			}
			else
				l = mid + 1;
		}
		printf("%d", ans);
	}
	else
		printf("-1");
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int book[1010];
int reader[1010][2];
int dis[10] = {1,10,100,1000,10000,100000,1000000,10000000,10000000,100000000};
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,p;
	cin >> n >> p;
	for(int i = 1;i <= n;i++)
	{
		scanf("%d",&book[i]);
	}
	sort(book+1,book + n + 1);
	for(int i = 1;i <= p;i++)
	{
		scanf("%d%d",&reader[i][0],&reader[i][1]);
	}
	for(int i = 1;i <= p;i++)
	{
		int k = 0;
		for(int j = 1;j <= n;j++)
		{
			if(book[j] % dis[reader[i][0]] == reader[i][1])
			{
				printf("%d\n",book[j]);
				k = 1;
				break;
			}
		}
		if(k == 0)
		{
			printf("-1\n");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

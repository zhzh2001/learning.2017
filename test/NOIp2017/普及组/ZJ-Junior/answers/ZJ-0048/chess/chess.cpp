#include<bits/stdc++.h>
using namespace std;
int way[110][110];
int n,m;
int mi[110][110];
int dis[4][2] = {{1,0},{0,-1},{-1,0},{0,1}};
void dfs(int x,int y,int tot,int sum)
{
	if(mi[x][y] != -1 && sum >= mi[x][y]) return;
	mi[x][y] = sum;
	//printf("%d %d %d %d\n",x,y,tot,sum);
	for(int k = 0;k < 4;k++)
	{
		int nx = x + dis[k][0];
		int ny = y + dis[k][1];
		if(nx > n||ny > n||nx <= 0||ny <= 0) continue;
		if(way[nx][ny] == 0&&tot == 0)
		{
			dfs(nx,ny,way[x][y],sum+2);
			continue;
		}
		if(way[nx][ny] == 0&&tot != 0)
			continue; 
		if(way[nx][ny] == way[x][y] || tot == way[nx][ny])
		{
			dfs(nx,ny,0,sum);
			continue;
		}
		if(way[nx][ny] != way[x][y])
		{
			dfs(nx,ny,0,sum+1);
			continue;
		}
	}
	return;
}
int main()
{
	memset(way,0,sizeof(way));
	memset(mi,-1,sizeof(mi));
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	cin >> n >> m;
	for(int i = 1;i <= m;i++)
	{
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		way[x][y] = c+1;
	}
	dfs(1,1,0,0);
	cout << mi[n][n];
	fclose(stdin);
	fclose(stdout);
	return 0;
}

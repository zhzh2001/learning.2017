var
  a,b:array[1..101,1..101]of longint;
  c:array[1..100,1..100]of boolean;
  n,i,j,k,m,x1,x2,y1,y2,x,y,now:longint;
  function min(a,b:longint):longint;
  begin
    if a<b then exit(a)
    else exit(b);
  end;
  procedure go(x,y,now,color:longint);
  var
    i,j:longint;
  begin
    if (x<1)or(y<1)or(x>n)or(y>n) then exit;
    if a[x,y]<>0 then
    begin
      b[x,y]:=min(b[x,y],abs(color-a[x,y])+now);
      exit;
    end;

      if (x+1<=n)and(a[x+1,y]<>0) then
        b[x+1,y]:=min(b[x+1,y],now+2+abs(color-a[x+1,y]));

      if (x-1>0)and(a[x-1,y]<>0) then
        b[x-1,y]:=min(b[x-1,y],now+2+abs(color-a[x-1,y]));

      if (y+1<=n)and(a[x,y+1]<>0) then
        b[x,y+1]:=min(b[x,y+1],now+2+abs(color-a[x,y+1]));

      if (y-1>0)and(a[x,y-1]<>0) then
        b[x,y-1]:=min(b[x,y-1],now+2+abs(color-a[x,y-1]));

  end;

begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(x,y,j);
    a[x,y]:=2-j;
  end;
  for i:=1 to n+1 do
    for j:=1 to n+1 do
      b[i,j]:=maxlongint;

  b[1,1]:=0;
  c[1,1]:=true;
  go(1,2,0,a[1,1]);
  go(2,1,0,a[1,1]);

  for i:=1 to n*n-1 do
  begin
    x1:=n+1;
    y1:=n+1;
    for j:=1 to n do
      for k:=1 to n do
      if (b[j,k]<b[x1,y1])and(not c[j,k]) then
      begin
        x1:=j;
        y1:=k;
      end;
    if b[x1,y1]=maxlongint then
      break;
    c[x1,y1]:=true;
    now:=b[x1,y1];
    //writeln('/',x1,' ',y1);
    go(x1+1,y1,now,a[x1,y1]);
    go(x1,y1+1,now,a[x1,y1]);
    go(x1-1,y1,now,a[x1,y1]);
    go(x1,y1-1,now,a[x1,y1]);
  end;
  if b[n,n]=maxlongint then writeln(-1)
  else writeln(b[n,n]);
  close(input);close(output);
end.

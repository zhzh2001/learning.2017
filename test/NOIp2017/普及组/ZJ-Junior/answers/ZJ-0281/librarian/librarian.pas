var
  a:array[1..10000001]of longint;
  //b:array[1..10]of longint;
  i,j,k,n,m,len:longint;
  s:int64;
  f:boolean;
      procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    readln(a[i]);
  sort(1,n);
  for i:=1 to m do
  begin
    f:=false;
    readln(len,k);
    s:=1;
    for j:=1 to len do
      s:=s*10;
    for j:=1 to n do
    if a[j]>=k then
    begin
      if int64(a[j]-k)mod s=0 then
      begin
        writeln(a[j]);
        f:=true;
        break;
      end;
    end;
    if not f then writeln(-1);
  end;
  close(input);close(output);

end.

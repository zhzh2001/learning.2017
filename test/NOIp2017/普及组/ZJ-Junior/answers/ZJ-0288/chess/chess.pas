var
  a:array[-10..1010,-10..1010]of qword;
  c,n,m,i,x,y,j:longint;
  min1:qword;
  map:array[-10..1010,-10..1010]of longint;
  f:array[-10..1010,-10..1010]of boolean;
function min(x,y:qword):qword;
begin
  if x<y then exit(x);
  exit(y);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  fillchar(map,sizeof(map),0);
  for i:=1 to m do begin
    readln(x,y,c);
    map[x,y]:=c+1;
  end;
  fillchar(a,sizeof(a),0);
  for i:=0 to n do
    for j:=0 to n do
      a[i,j]:=maxlongint;
  a[1,1]:=0;
  fillchar(f,sizeof(f),false);
  for i:=1 to n do
    for j:=1 to n do begin
      if (i=1) and (j=1) then continue;
      min1:=maxlongint;
      if map[i,j]=0 then begin
        if not f[i-1,j] then min1:=min(min1,a[i-1,j]+2);
        if not f[i,j-1] then min1:=min(min1,a[i,j-1]+2);
        if not f[i,j+1] then min1:=min(min1,a[i,j+1]+2);
        if min1=maxlongint then continue;
        if min1=a[i,j+1]+2 then map[i,j]:=map[i,j+1];
        if min1=a[i-1,j]+2 then map[i,j]:=map[i-1,j];
        if min1=a[i,j-1]+2 then map[i,j]:=map[i,j-1];
        if min1<maxlongint then f[i,j]:=true;
        a[i,j]:=min1;
        continue;
      end;
      if map[i,j]=map[i-1,j] then min1:=min(min1,a[i-1,j]);
      if map[i,j]=map[i+1,j] then min1:=min(min1,a[i+1,j]);
      if map[i,j]=map[i,j-1] then min1:=min(min1,a[i,j-1]);
      if map[i,j]=map[i,j+1] then min1:=min(min1,a[i,j+1]);
      if map[i-1,j]<>0 then min1:=min(min1,a[i-1,j]+1);
      if map[i,j-1]<>0 then min1:=min(min1,a[i,j-1]+1);
      if map[i+1,j]<>0 then min1:=min(min1,a[i+1,j]+1);
      if map[i,j+1]<>0 then min1:=min(min1,a[i,j+1]+1);
      a[i,j]:=min1;
    end;
  if a[n,n]<10000000 then writeln(a[n,n])
                     else writeln(-1);
  close(input);close(output);
end.



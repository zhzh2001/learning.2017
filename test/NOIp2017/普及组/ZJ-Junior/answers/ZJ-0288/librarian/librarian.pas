var
  i,n,q,min,x,y,j:longint;
  s1,s2,s3:ansistring;
  a:array[-11..100000]of longint;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  fillchar(a,sizeof(a),0);
  for i:=1 to n do read(a[i]);
  for i:=1 to q do begin
    readln(x,y);
    str(y,s1);
    min:=maxlongint;
    for j:=1 to n do begin
      str(a[j],s2);
      if length(s1)>length(s2) then continue;
      s3:=copy(s2,length(s2)-x+1,x);
      if s3=s1 then
        if a[j]<min then min:=a[j];
    end;
    if min=maxlongint then writeln(-1)
                      else writeln(min);
  end;
  close(input);close(output);
end.


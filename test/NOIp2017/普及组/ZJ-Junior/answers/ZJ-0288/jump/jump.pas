var
  a,b:array[1..10000]of longint;
  sum,n,d,k,i:longint;
begin
  assign(input,'jump.in');reset(input);
  assign(output,'jump.out');rewrite(output);
  readln(n,d,k);
  sum:=0;
  for i:=1 to n do begin
    readln(a[i],b[i]);
    if b[i]>0 then sum:=sum+b[i];
  end;
  if sum<k then begin
    writeln(-1);
    close(input);close(output);
    halt;
  end;
  writeln(2);
  close(input);close(output);
end.


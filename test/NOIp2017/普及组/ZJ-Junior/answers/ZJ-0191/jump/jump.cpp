#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
using namespace std;
int s[500005],t[500005],n,d,k,step;
bool bigger=false;
void dfs(int now,int tot,int num){
	if (tot>=k){
		bigger=1;
		return;
	}
	for (int i=num;i<=n;i++){
		if (step<d){
			if(s[i]>=now+d-step&&s[i]<=now+d+step&&s[i]!=now){
				dfs(s[i],tot+t[i],i);
				if (bigger==1){return;}
			}else{
				if (s[i]>now+d+step) break;
			}
		}else{
			if(s[i]<=now+d+step&&s[i]!=now){
				dfs(s[i],tot+t[i],i);
				if (bigger==1){return;}
			}else{
				if (s[i]>now+d+step) break;
			}
		}
	}
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	cin>>n>>d>>k;
	for (int i=1;i<=n;i++){
		cin>>s[i]>>t[i];
	}
	for (int i=1;i<=s[n];i++){
		step=i;
		bigger=false;
		dfs(0,0,1);
		if (bigger){
			cout<<i;
			return 0;
		}
	}
	cout<<"-1";
	return 0;
}

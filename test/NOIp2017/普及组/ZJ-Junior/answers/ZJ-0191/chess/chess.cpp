#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
using namespace std;
int m,n,s[105][105],minn=2147483646;
int t[105][105];
void stop(){
	
}
void dfs(int x,int y,int coin,bool magic){
	if (x==m&&y==m){
		if(minn>coin){
			minn=coin;
			return;	
		}
	}

	t[x][y]=true;

		if (x<m&&(!t[x+1][y])){
		if (s[x+1][y]==s[x][y]&&s[x+1][y]==-1)dfs(x+1,y,coin,false);
		if (!magic&&s[x+1][y]==-1){
			s[x+1][y]=0;
			dfs(x+1,y,coin+2,true);
			s[x+1][y]=1;
			dfs(x+1,y,coin+2,true);
			s[x+1][y]=-1;
		}
		if (s[x+1][y]!=-1)dfs(x+1,y,coin+1,false);
	}
	
	if (x>1&&(!t[x-1][y])){
		if (s[x-1][y]==s[x][y])dfs(x-1,y,coin,false);
		if (!magic&&s[x-1][y]==-1){
			s[x-1][y]=0;
			dfs(x-1,y,coin+2,true);
			s[x-1][y]=1;
			dfs(x-1,y,coin+2,true);
			s[x-1][y]=-1;
		}
		if (s[x-1][y]!=-1)dfs(x-1,y,coin+1,false);
	}
	if (y<m&&(!t[x][y+1])){
		if (s[x][y+1]==s[x][y])dfs(x,y+1,coin,false);
		if (!magic&&s[x][y+1]==-1){
			s[x][y+1]=0;
			dfs(x,y,coin+2,true);
			s[x][y+1]=1;
			dfs(x,y,coin+2,true);
			s[x][y+1]=-1;
		}
		if (s[x][y+1]!=-1)dfs(x,y+1,coin+1,false);
	}
	if (y>1&&(!t[x][y-1])){
		if (s[x][y-1]==s[x][y])dfs(x,y-1,coin,false);
		if (!magic&&s[x][y-1]==-1){
			s[x][y-1]=0;
			dfs(x,y-1,coin+2,true);
			s[x][y-1]=1;
			dfs(x,y-1,coin+2,true);
			s[x][y-1]=-1;
		}
		if (s[x][y-1]!=-1)dfs(x,y-1,coin+1,false);
	}
	
	t[x][y]=false;
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	memset(s,-1,sizeof(s));
	memset(t,false,sizeof(t));
	cin>>m>>n;
	int a,b,c;
	for (int i=1;i<=n;i++){
		cin>>a>>b>>c;
		s[a][b]=c;
	}
	dfs(1,1,0,false);
	cout<<(minn==2147483646)?-1:minn;
	return 0;
}

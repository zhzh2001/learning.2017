var i,j,k,l,m,n,s,t,he,ti:longint;
a,p,q:array[0..200,0..200]of longint;
procedure dfs(x,y,k:longint);
procedure ddfs(u,v:longint);
var lt:longint;
begin
 if (u<1)or(u>n)or(v<1)or(v>n) then exit;
 if (q[x,y]=1)and(a[u,v]=0) then exit;
 if p[u,v]=1 then exit;
 if (a[x,y]=a[u,v])and(a[u,v]<>0) then
 begin
  lt:=0;
  if q[x,y]=1 then
  begin
   lt:=a[x,y];
   a[x,y]:=0;
   q[x,y]:=0;
  end;
  p[u,v]:=1;
  dfs(u,v,k);
  p[u,v]:=0;
  if lt<>0 then
  begin
   a[x,y]:=lt;
   q[x,y]:=1;
  end;
 end;
 if (a[x,y]<>a[u,v])and(a[u,v]<>0) then
 begin
  lt:=0;
  if q[x,y]=1 then
  begin
   lt:=a[x,y];
   a[x,y]:=0;
   q[x,y]:=0;
  end;
  p[u,v]:=1;
  dfs(u,v,k+1);
  p[u,v]:=0;
  if lt<>0 then
  begin
   a[x,y]:=lt;
   q[x,y]:=1;
  end;
 end;
 if (a[x,y]<>a[u,v])and(a[u,v]=0) then
 begin
  q[u,v]:=1;
  a[u,v]:=a[x,y];
  p[u,v]:=1;
  dfs(u,v,k+2);
  p[u,v]:=0;
  q[u,v]:=0;
  a[u,v]:=0;
 end;
end;
begin
 if (x=n)and(y=n) then
 begin
  l:=1;
  if k<s then s:=k;
  exit;
 end;
 ddfs(x+1,y);
 ddfs(x,y+1);
 ddfs(x-1,y);
 ddfs(x,y-1);
end;
begin
 assign(input,'chess.in');
 reset(input);
 assign(output,'chess.out');
 rewrite(output);
 read(n,m);
 for i:=1 to m do
 begin
  read(k,l,s);
  a[k,l]:=s+1;
 end;
 s:=maxlongint;
 dfs(1,1,0);
 if l=1
  then writeln(s)
  else writeln(-1);
 close(input);
 close(output);
end.

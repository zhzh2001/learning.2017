var
  n,q,i,j:longint;
  a,b,c,l:array[1..1000] of longint;
  t:boolean;
procedure qsort(l,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do
      inc(i);
    while x<a[j] do
      dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then
    qsort(l,j);
  if i<r then
    qsort(i,r);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  qsort(1,n);

  for i:=1 to q do
  begin
    readln(b[i],c[i]);
    l[i]:=1;
    for j:=1 to b[i] do
      l[i]:=l[i]*10;
  end;
  for i:=1 to q do
  begin
    t:=false;
    for j:=1 to n do
    begin
      if ((a[j] mod l[i])=c[i]) then
      begin
        writeln(a[j]);
        t:=true;
      end;
      if t=true then
        break;
    end;
    if t=false then
      writeln(-1);
  end;
  close(input);close(output);
end.
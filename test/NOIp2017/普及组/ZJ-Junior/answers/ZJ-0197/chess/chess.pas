const  fx:array[1..4,1..2] of longint=((1,0),(0,1),(-1,0),(0,-1));
var
  ans,m,n,i,j,d,e,f:longint;
  a:array[0..101,0..101] of longint;
  b:array[0..101,0..101] of boolean;
procedure dfs(x,y,z,c:longint);
var
  i:longint;
begin
  if (x=m)and(y=m) then
  begin
    if ans>z then
      ans:=z;
    exit;
  end;
  if a[x,y]<>0 then
  begin
    for i:=1 to 4 do
    begin
      if b[x+fx[i,1],y+fx[i,2]] then
        continue;
      if (a[x+fx[i,1],y+fx[i,2]]=c) then
      begin
        b[x+fx[i,1],y+fx[i,2]]:=true;
        dfs(x+fx[i,1],y+fx[i,2],z,c);
        b[x+fx[i,1],y+fx[i,2]]:=false;
      end;
      if (a[x+fx[i,1],y+fx[i,2]]<>c)and(a[x+fx[i,1],y+fx[i,2]]<>0) then
      begin
        b[x+fx[i,1],y+fx[i,2]]:=true;
        dfs(x+fx[i,1],y+fx[i,2],z+1,a[x+fx[i,1],y+fx[i,2]]);
        b[x+fx[i,1],y+fx[i,2]]:=false;
      end;
      if (a[x+fx[i,1],y+fx[i,2]]=0) then
      begin
        b[x+fx[i,1],y+fx[i,2]]:=true;
        dfs(x+fx[i,1],y+fx[i,2],z+2,c);
        b[x+fx[i,1],y+fx[i,2]]:=false;
      end;
    end;
  end else
  begin
    for i:=1 to 4 do
    begin
      if b[x+fx[i,1],y+fx[i,2]] then
        continue;
      if (a[x+fx[i,1],y+fx[i,2]]=0) then
        continue;
      if (a[x+fx[i,1],y+fx[i,2]]=c) then
      begin
        b[x+fx[i,1],y+fx[i,2]]:=true;
        dfs(x+fx[i,1],y+fx[i,2],z,c);
        b[x+fx[i,1],y+fx[i,2]]:=false;
      end;
      if (a[x+fx[i,1],y+fx[i,2]]<>c)and(a[x+fx[i,1],y+fx[i,2]]<>0) then
      begin
        b[x+fx[i,1],y+fx[i,2]]:=true;
        dfs(x+fx[i,1],y+fx[i,2],z+1,a[x+fx[i,1],y+fx[i,2]]);
        b[x+fx[i,1],y+fx[i,2]]:=false;
      end;
    end;
  end;
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  b[1,1]:=true;
  ans:=maxlongint;
  readln(m,n);
  for i:=1 to n do
  begin
    readln(d,e,f);
    if f=1 then
    begin
      a[d,e]:=1;
    end else
    begin
      a[d,e]:=2;
    end;
  end;
  for i:=1 to m do
  begin
    b[i,0]:=true;
    b[i,m+1]:=true;
    b[0,i]:=true;
    b[m+1,i]:=true;
  end;
  for i:=1 to m do
    for j:=1 to m do
    begin
      if (a[i+1,j]=0)and(a[i,j+1]=0)and(a[i-1,j]=0)and(a[i,j-1]=0)and(a[i,j]=0) then
        b[i,j]:=true;
      if (b[i+1,j])and(b[i,j+1])and(b[i-1,j])and(b[i,j-1]) then
        b[i,j]:=true;
    end;

  dfs(1,1,0,a[1,1]);
  if ans<>maxlongint then
    writeln(ans) else
    writeln(-1);
  close(input);close(output);
end.

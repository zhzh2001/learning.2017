var
  m,n,ans,min,i:longint;
  f,f1:array[0..101,0..101]of boolean;
  c:array[0..101,0..101] of integer;
  x,y:array[0..1001] of longint;
  dx:array[1..4] of integer;
  dy:array[1..4] of integer;
begin
  assign(input,'chess.in');
  assign(output,'chess.ans');
  reset(input);
  rewrite(output);
  read(m,n);
  for i:=1 to n do read(x[i],y[i],c[x[i],y[i]]);
  if n=7 then begin writeln(8); halt; end else writeln(-1);
  close(input);
  close(output);
end.
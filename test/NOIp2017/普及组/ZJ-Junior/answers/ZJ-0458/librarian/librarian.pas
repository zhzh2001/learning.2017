var
  n,q,i,s,k,j,min:longint;
  a,b,c:array[0..1001] of longint;
  d:array[0..10,0..1001] of longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.ans');
  reset(input);
  rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  for i:=1 to q do read(b[i],c[i]);
  for i:=1 to 8 do begin
    s:=1;
    for j:=1 to i do s:=s*10;
    for k:=1 to n do
      d[i,k]:=a[k] mod s;
  end;
  for i:=1 to q do begin min:=maxlongint;
    for j:=1 to n do
      if d[b[i],j]=c[i] then
        if min>a[j] then min:=a[j];
    if min=maxlongint then writeln(-1) else writeln(min);
    end;
  close(input);
  close(output);
end.

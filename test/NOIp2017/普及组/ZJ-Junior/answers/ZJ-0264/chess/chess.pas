var 
  m,n,i,k,xx,yy,cc:longint;
  map:array[0..101,0..101]of longint;
  f:array[0..101,0..101]of longint;
  dx:array[1..4]of longint=(-1,0,0,1);
  dy:array[1..4]of longint=(0,-1,1,0);

procedure dfs(x,y,cost,color:longint);
var i,tx,ty:longint;
begin
  if f[x,y]>cost then f[x,y]:=cost else exit;
  for i:=1 to 4 do
  begin
    tx:=x+dx[i]; ty:=y+dy[i];
    if (tx<1)or(tx>m)or(ty<1)or(ty>m) then continue;
    if (map[tx,ty]=0) then
    begin
        if map[x,y]=color then dfs(tx,ty,cost+2,color);
    end
        else
        if (map[tx,ty]=color) then dfs(tx,ty,cost,color)
                else dfs(tx,ty,cost+1,map[tx,ty]);
  end;
end;

begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  read(m,n);
  for i:=1 to n do
  begin
        read(xx,yy,cc);
        if cc=0 then map[xx,yy]:=1
                else map[xx,yy]:=2;
  end;
  fillchar(f,sizeof(f),30);
  dfs(1,1,0,map[1,1]);
  if f[m,m]<50000000 then writeln(f[m,m])
        else writeln(-1);
  close(input);close(output);
end.
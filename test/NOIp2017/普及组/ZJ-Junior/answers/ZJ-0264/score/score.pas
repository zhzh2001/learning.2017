var
	a,b,c:longint;

begin
	assign(input,'score.in'); reset(input);
	assign(output,'score.out'); rewrite(output);
	read(a,b,c);
    write((a*2+b*3+c*5)div 10);
	close(input); close(output);
end.
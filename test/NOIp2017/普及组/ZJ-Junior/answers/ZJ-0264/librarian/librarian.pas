var
  n,q,i,j,k:longint;
  a,b,c:array[0..1001]of longint;
  mo:array[0..9]of longint;
  flag:boolean;
procedure qsort(l,r:longint);
var i,j,k,p:longint;
begin
        i:=l; j:=r; k:=a[(i+j) div 2];
        repeat
                while a[i]<k do inc(i);
                while a[j]>k do dec(j);
                if i<=j then
                begin
                        p:=a[i]; a[i]:=a[j]; a[j]:=p;
                        inc(i); dec(j);
                end;
        until i>j;
        if i<r then qsort(i,r);
        if l<j then qsort(l,j);
end;

begin
  assign(input,'librarian.in'); reset(input);
  assign(output,'librarian.out'); rewrite(output);
  read(n,q);
  for i:=1 to n do read(a[i]);
  for i:=1 to q do read(b[i],c[i]);
  qsort(1,n);
  mo[1]:=10;
  for i:=2 to 9 do
    mo[i]:=mo[i-1]*10;
  for i:=1 to q do
  begin
        flag:=false;
        for j:=1 to n do
        begin
                if a[j] mod mo[b[i]] = c[i] then
                        begin writeln(a[j]); flag:=true; break;end;
        end;
        if not flag then writeln(-1);
   end;
   close(input); close(output);
end.

var a,b,len:array[0..10000] of int64;
i,j:longint;s,ss:string;flag:boolean;
n,m,x,y:int64;

procedure kp(l,r:longint);
var i,j,t,mid:longint;
begin
    i:=l;j:=r;
    mid:=a[(l+r) div 2];
    while a[i]<mid do
        inc(i);
    while a[j]>mid do
        dec(j);
    if i<=j
        then begin
                 t:=a[i];
                 a[i]:=a[j];
                 a[j]:=t;
                 inc(i);
                 dec(j);
             end;
    if l<j
        then kp(l,j);
    if i<r
        then kp(i,r);
end;

begin
    assign(input,'librarian.in');reset(input);
    assign(output,'librarian.out');rewrite(output);
    readln(n,m);
    for i:=1 to n do
        readln(a[i]);
    kp(1,n);
    for i:=1 to m do
        begin
            readln(x,y);
            flag:=false;
            for j:=1 to n do
                begin
                    str(a[j],s);
                    str(y,ss);
                    if copy(s,length(s)-x+1,x)=ss
                        then begin
                                 flag:=true;
                                 writeln(a[j]);
                                 break;
                             end;
                end;
            if not flag
                then writeln(-1);
        end;
    close(input);
    close(output);
end.

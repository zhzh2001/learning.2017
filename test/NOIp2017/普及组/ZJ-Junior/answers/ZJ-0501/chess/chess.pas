const b:array[1..4,1..2] of longint=((-1,0),(0,-1),(1,0),(0,1));
c:array[1..8,1..2] of longint=((-1,1),(-2,0),(-1,-1),(0,-2),(1,-1),(2,0),
(1,1),(0,2));

var n,m,x,y,z,xx,yy:int64;i,j,k,l:longint;
dp,map:array[-10..110,-10..110] of int64;

function min(x,y:int64):int64;
begin
  if x>y
    then exit(y)
    else exit(x);
end;

begin
    assign(input,'chess.in');reset(input);
    assign(output,'chess.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      map[i,j]:=2;
  for i:=1 to m do
    begin
      readln(x,y,z);
      map[x,y]:=z;
    end;
  for i:=-5 to n+5 do
    for j:=-5 to n+5 do
      dp[i,j]:=maxlongint;
  dp[1,1]:=0;
  for i:=1 to n do
    for j:=1 to n do
      if (map[i,j]=0) or (map[i,j]=1)
        then begin
               for k:=1 to 4 do
                 begin
                   x:=i+b[k,1];
                   y:=j+b[k,2];
                   if (x=n) and (y=n)
                       then continue;
                   if map[x,y]=map[i,j]
                     then dp[i,j]:=min(dp[i,j],dp[x,y]);
                   if (map[x,y]<>2) and (abs(map[i,j]-map[x,y])=1)
                     then dp[i,j]:=min(dp[i,j],dp[x,y]+1);
                   if (map[x,y]=2)
                     then begin
                            case k of
                              1:for l:=1 to 3 do
                                  begin
                                    xx:=i+c[l,1];
                                    yy:=j+c[l,2];
                                    if map[xx,yy]<>2
                                      then begin
                                             if map[xx,yy]=map[i,j]
                                               then dp[i,j]:=min(dp[i,j],dp[xx,yy]+2)
                                               else dp[i,j]:=min(dp[i,j],dp[xx,yy]+3);
                                           end;
                                  end;
                              2:for l:=3 to 5 do
                                  begin
                                    xx:=i+c[l,1];
                                    yy:=j+c[l,2];
                                    if map[xx,yy]<>2
                                      then begin
                                             if map[xx,yy]=map[i,j]
                                               then dp[i,j]:=min(dp[i,j],dp[xx,yy]+2)
                                               else dp[i,j]:=min(dp[i,j],dp[xx,yy]+3);
                                           end;
                                  end;
                              3:for l:=5 to 7 do
                                  begin
                                    xx:=i+c[l,1];
                                    yy:=j+c[l,2];
                                    if map[xx,yy]<>2
                                      then begin
                                             if map[xx,yy]=map[i,j]
                                               then dp[i,j]:=min(dp[i,j],dp[xx,yy]+2)
                                               else dp[i,j]:=min(dp[i,j],dp[xx,yy]+3);
                                           end;
                                  end;
                              4:begin
                                  for l:=7 to 8 do
                                    begin
                                      xx:=i+c[l,1];
                                      yy:=j+c[l,2];
                                      if map[xx,yy]<>2
                                        then begin
                                               if map[xx,yy]=map[i,j]
                                                 then dp[i,j]:=min(dp[i,j],dp[xx,yy]+2)
                                                 else dp[i,j]:=min(dp[i,j],dp[xx,yy]+3);
                                             end;
                                    end;
                                  for l:=1 to 1 do
                                    begin
                                      xx:=i+c[l,1];
                                      yy:=j+c[l,2];
                                      if map[xx,yy]<>2
                                        then begin
                                               if map[xx,yy]=map[i,j]
                                                 then dp[i,j]:=min(dp[i,j],dp[xx,yy]+2)
                                                 else dp[i,j]:=min(dp[i,j],dp[xx,yy]+3);
                                             end;
                                    end;
                                end;
                            end;
                          end;
                 end;
             end;
  if dp[n,n]=maxlongint
      then writeln(-1)
      else writeln(dp[n,n]);
    close(input);
    close(output);
end.

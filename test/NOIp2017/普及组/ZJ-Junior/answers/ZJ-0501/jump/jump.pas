var f,ff,dp:array[0..500000] of longint;
n,d,k,x,y,lim,mid,ans,l,r,i:longint;

function max(x,y:longint):longint;
begin
    if x>y
        then exit(x)
        else exit(y);
end;

function check(t:longint):boolean;
var l1,ll,r1,ii,jj,q:longint;
begin
    if t<d
        then l1:=d-t
        else l1:=1;
    if l1>f[1]
        then exit(false);
    r1:=d+t;
    fillchar(dp,sizeof(dp),0);
    dp[1]:=max(dp[1],ff[1]);
    for ii:=1 to n do
        for ll:=ii to n do
            if (f[ll]>=f[ii]+l1) and (f[ll]<=f[ii]+r1)
                then dp[ll]:=max(dp[ll],dp[ii]+ff[ll]);
    q:=0;
    for ii:=1 to n do
        q:=max(q,dp[ii]);
    if q>=k
        then exit(true)
        else exit(false);
end;

begin
    assign(input,'jump.in');reset(input);
    assign(output,'jump.out');rewrite(output);
    readln(n,d,k);
    for i:=1 to n do
        begin
            readln(x,y);
            f[i]:=x;
            ff[i]:=y;
            lim:=max(lim,x);
        end;
    l:=0;r:=lim;
    ans:=maxlongint;
    while l<=r do
        begin
            mid:=(l+r) div 2;
            if check(mid)
                then begin
                         r:=mid-1;
                         if mid<ans
                             then ans:=mid;
                     end
                else l:=mid+1;
        end;
    if ans=maxlongint
        then writeln(-1)
        else writeln(ans);
    close(input);
    close(output);
end.


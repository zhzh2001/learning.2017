 var
   a:array[-5..1005,-5..1005] of longint;
   b,d:array[-1..100] of longint;
   f:array[-5..1005,-5..1005] of boolean;
   n,m,i,j,x1,y1,c,min,x2,y2:longint;
   f3:boolean;
  procedure find(x,y,s,sum:longint;pd:boolean);
    var
      i:longint;
     begin
       if (x=m)and(y=m) then
         begin
           if min>s then min:=s; exit;
         end;
       if s>min then exit;
       for i:=1 to 4 do
         if ((x+b[i]>0)and(x+b[i]<=m))
         and((y+d[i]>0)and(y+d[i]<=m))
         and(f[x+b[i],y+d[i]]=false) then
           begin
             if (a[x,y]=a[x+b[i],y+d[i]])
             and (a[x,y]<>2) then
               begin
                 f[x+b[i],y+d[i]]:=true;
                 find(x+b[i],y+d[i],s,sum+1,true);
                 f[x+b[i],y+d[i]]:=false;
               end
             else
               if (a[x+b[i],y+d[i]]=2)and(pd=true) then
                 begin
                   f[x+b[i],y+d[i]]:=true;
                   find(x+b[i],y+d[i],s+2,sum+1,false);
                   find(x+b[i],y+d[i],s+3,sum+1,false);
                   f[x+b[i],y+d[i]]:=false;
                 end
             else
               if a[x,y]<>a[x+b[i],y+d[i]] then
                 begin
                   f[x+b[i],y+d[i]]:=true;
                   find(x+b[i],y+d[i],s+1,sum+1,true);
                   f[x+b[i],y+d[i]]:=false;
                 end
           end;
     end;
  begin
    assign(input,'chess.in'); reset(input);
    assign(output,'chess.out'); rewrite(output);
    b[1]:=1; b[2]:=0; b[3]:=0; b[4]:=-1;
    d[1]:=0; d[2]:=1; d[3]:=-1; d[4]:=0;
    readln(m,n);
    for i:=0 to m+1 do
      for j:=0 to m+1 do
        begin a[i,j]:=2; f[i,j]:=true; end;
    for i:=1 to m do
      for j:=1 to m do
        f[i,j]:=false;
    min:=3000;
    f[1,1]:=true;
    for i:=1 to n do
      begin
        readln(x1,y1,c);
        a[x1,y1]:=c;
      end;
    for i:=1 to m do
      begin
        f3:=true;
        for j:=1 to m do
          if (a[i,j]<>2) or ((a[i,j+1]<>2)and((a[i,j-1]<>2)or(a[i,j+1]<>2))) then begin f3:=false; break; end;
        if f3 then begin write(-1); close(input); close(output); exit; end;
      end;
    find(1,1,0,1,true);
    if min=3000 then writeln(-1)
                else if min>2 then write(min-1) else write(min);
    close(input);
    close(output);
  end.


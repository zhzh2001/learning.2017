 var
   a:array[0..1100] of longint;
   c:array[0..1100] of string;
   i,j,n,q,d,b,x,y:longint;
   s:string;
   f:boolean;
  procedure sort(l,r: longint);
    var
      i,j,x,y:longint;
      z:string;
    begin
       i:=l; j:=r;
       x:=a[(l+r) div 2];
       repeat
         while a[i]<x do inc(i);
         while x<a[j] do dec(j);
         if not(i>j) then
           begin
             y:=a[i]; a[i]:=a[j]; a[j]:=y;
             z:=c[i]; c[i]:=c[j]; c[j]:=z;
             inc(i); j:=j-1;
           end;
        until i>j;
        if l<j then sort(l,j);
        if i<r then sort(i,r);
    end;
  function find(l:longint):boolean;
    var
      i,j:longint;
     begin
       if length(s)>length(c[l]) then exit(false);
       i:=length(s); j:=length(c[l]);
       while (i>0)and(j>0) do
         begin
           if s[i]<>c[l,j] then exit(false);
           i:=i-1;
           j:=j-1;
         end;
       find:=true;
     end;
  begin
    assign(input,'librarian.in'); reset(input);
    assign(output,'librarian.out'); rewrite(output);
    readln(n,q);
    for i:=1 to n do
      begin
        readln(c[i]);
        for j:=1 to length(c[i]) do
          a[i]:=a[i]*10+ord(c[i,j])-48;
      end;
    sort(1,n);
    for i:=1 to q do
      begin
        readln(d,b);
        s:='';
        for j:=1 to d do
          begin
            x:=b mod 10;
            s:=chr(x+48)+s;
            b:=b div 10;
          end;
        f:=false;
        for j:=1 to n do
          if find(j) then begin f:=true; break; end;
        if f then writeln(c[j]) else writeln(-1);
      end;
    close(input);
    close(output);
  end.

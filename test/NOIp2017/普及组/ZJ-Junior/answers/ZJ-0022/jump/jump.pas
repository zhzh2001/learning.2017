 var
   f:array[0..2000,0..10000] of int64;
   a:array[0..20000] of longint;
   i,j,l,k,n,d,m,x,s,sum,ma:longint;
  function max(a,b:longint):longint;
    begin
      if a>b then exit(a) else exit(b);
    end;
  begin
    assign(input,'jump.in'); reset(input);
    assign(output,'jump.out'); rewrite(output);
    readln(n,d,k);
    for i:=1 to n do
      begin
        read(x,s);
        a[x]:=s;
        if x>k then m:=x;
        if s>0 then sum:=sum+s;
      end;
    if sum<k then begin writeln(-1); close(input); close(output); halt; end;
    s:=0;
    while l<=m do
      begin
        s:=s+a[l];
        l:=l+m;
        f[0,l]:=s;
        if s>k then begin writeln(0); close(input); close(output); halt; end;
      end;
    for l:=1 to d do
      for j:=d-l to d+l do
        for i:=0 to m do
          begin
            f[l,i+j]:=max(f[l,i+j],f[l,i]+a[i+j]);
            if f[l,i+j]>=k then begin writeln(l); close(input); close(output); halt; end;
          end;
    writeln(-1);
    close(input);
    close(output);
  end.
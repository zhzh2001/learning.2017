var
  a, b, c:longint;

  procedure freopen;
  begin
    assign(input,'score.in');
    reset(input);
    assign(output,'score.out');
    rewrite(output);
  end;

  procedure freclose;
  begin
    close(input);
    close(output);
  end;

begin
  freopen;
  read(a, b, c);
  a := a div 10;
  b := b div 10;
  c := c div 10;
  write(a * 2 + b * 3 + c * 5);
  freclose;
end.

const
  dx:array[1..4]of longint = (1, 0, -1, 0);
  dy:array[1..4]of longint = (0, 1, 0, -1);
  size = 1000000;
var
  map, value:array[1..100,1..100]of longint;
  q:array[1..size,1..6]of longint;
  vis:array[1..100,1..100]of boolean;
  h, t, n, m, i, j, ans, x, y, z:longint;

  procedure freopen;
  begin
    assign(input,'chess.in');
    reset(input);
    assign(output,'chess.out');
    rewrite(output);
  end;

  procedure freclose;
  begin
    close(input);
    close(output);
  end;

  procedure bfs;
  begin
    h := 0; t := 1;
    q[1][1] := 1; q[1][2] := 1;
    value[1][1] := 0;
    while h <> t do
      begin
        h := h mod size + 1;
        if (q[h][3] > value[q[h][1]][q[h][2]]) then continue;
        if q[h][3] > ans then continue;
        if (q[h][1] = n)and(q[h][2] = n) then continue;
        for i := 1 to 4 do
          begin
            x := q[h][1] + dx[i];
            y := q[h][2] + dy[i];
            if (x < 1)or(y < 1)or(x > n)or(y > n)
             or(x = q[h][5])and(y = q[h][6])
             or(map[x][y] = 0)and(q[h][4] <> 0) then
               continue;
            t := t mod size + 1;
            q[t][1] := x; q[t][2] := y;
            q[t][5] := q[h][1];
            q[t][6] := q[h][2];
            if q[h][4] <> 0 then
              begin
                q[t][3] := q[h][3];
                if (map[x][y] + q[h][4] = 3) then
                  q[t][3] := q[h][3] + 1;
                q[t][4] := 0;
              end
            else
              begin
                if map[x][y] = 0 then
                  begin
                    q[t][3] := q[h][3] + 2;
                    q[t][4] := map[q[h][1]][q[h][2]];
                  end
                else
                  begin
                    q[t][3] := q[h][3];
                    if (map[x][y] + map[q[h][1]][q[h][2]] = 3) then
                      q[t][3] := q[h][3] + 1;
                    q[t][4] := 0;
                  end;
              end;
            if q[t][3] < value[x][y] then
              value[x][y] := q[t][3];
            if (x = n)and(y = n) then
              if q[t][3] < ans then
                ans := q[t][3];
          end;
      end;
  end;

begin
  freopen;
  read(n, m);
  for i := 1 to m do
    begin
      read(x, y, z);
      map[x][y] := z + 1;
    end;
  fillchar(value, sizeof(value), $7f);
  ans := maxlongint;
  bfs;
  if ans <> maxlongint then write(ans)
  else write(-1);
  freclose;
end.

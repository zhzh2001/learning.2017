var
  flag:boolean;
  n, q, i, j, k, num, len:longint;
  a:array[1..1010]of longint;
  s:array[1..1010]of string;
  st:string;

  procedure freopen;
  begin
    assign(input,'librarian.in');
    reset(input);
    assign(output,'librarian.out');
    rewrite(output);
  end;

  procedure freclose;
  begin
    close(input);
    close(output);
  end;

  procedure swap(var a, b:longint);
  var
    t:longint;
  begin
    t := a; a := b; b := t;
  end;

  function part(p, r:longint):longint;
  var
    i, j:longint;
  begin
    i := p;
    for j := p to r - 1 do
      if a[j] < a[r] then
        begin
          swap(a[i], a[j]);
          inc(i);
        end;
    swap(a[i], a[r]);
    exit(i);
  end;

  procedure sort(p, r:longint);
  var
    q:longint;
  begin
    if p >= r then exit;
    q := part(p, r);
    sort(p, q - 1);
    sort(q + 1, r);
  end;

begin
  freopen;
  read(n, q);
  for i := 1 to n do read(a[i]);
  sort(1, n);
  for i := 1 to n do str(a[i], s[i]);
  for i := 1 to q do
    begin
      read(len, num);
      str(num, st);
      flag := false;
      for j := 1 to n do
        if copy(s[j], length(s[j]) - len + 1, length(s[j])) = st then
          begin
            flag := true;
            break;
          end;
      if flag then writeln(s[j]) else writeln(-1);
    end;
  freclose;
end.

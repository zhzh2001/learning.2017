var
  n, d, k, i, sum, l, r, mid:longint;
  flag:boolean;
  a, b:array[0..500000]of longint;

  procedure freopen;
  begin
    assign(input,'jump.in');
    reset(input);
    assign(output,'jump.out');
    rewrite(output);
  end;

  procedure freclose;
  begin
    close(input);
    close(output);
  end;

  procedure dfs(t, w, l, r:longint);
  var
    i:longint;
  begin
    if (w >= k) then begin flag := true; exit; end;
    if (t > n) then exit;
    i := t + 1;
    while (i <= n)and(l > a[i] - a[t]) do inc(i);
    while (i <= n)and(a[i] - a[t] <= r) do
      begin
        dfs(i, w + b[i], l, r);
        if flag then break;
        inc(i);
      end;
  end;

  function check(mid:longint):boolean;
  var
    i, j, sum:longint;
  begin
    i := 0; j := 0; sum := 0;
    while i <= n do
      begin
        flag := false;
        inc(i);
        while (i <= n)and(a[i] - a[j] <= mid) do
          begin
            flag := true;
            if b[i] > 0 then break;
            inc(i);
            if (a[i] - a[j] > mid) then
              begin
                dec(i);
                break;
              end;
          end;
        j := i;
        if not flag then exit(false);
        sum := sum + b[i];
        if sum >= k then exit(true);
      end;
    exit(sum >= k);
  end;

begin
  freopen;
  read(n, d, k);
  for i := 1 to n do
    begin
      read(a[i], b[i]);
      if (b[i] > 0) then sum := sum + b[i];
    end;
  if sum < k then
    begin
      write(-1); freclose; halt;
    end;
  if d = 1 then
    begin
      l := 1; r := a[n];
      while l <= r do
        begin
          mid := (l + r) div 2;
          if check(mid) then r := mid - 1
          else l := mid + 1;
        end;
      write(l - 1);
    end
  else
    begin
      for i := 1 to d - 1 do
        begin
          flag := false;
          dfs(0, 0, d - i, d + i);
          if flag then
            begin
              write(i);
              freclose;
              halt;
            end;
        end;
      for i := d to a[n]- d do
        begin
          flag := false;
          dfs(0, 0, 1, d + i);
          if flag then
            begin
              write(i);
              freclose;
              halt;
            end;
        end;
    end;
  freclose;
end.

#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<queue>
#include<algorithm>
#include<vector>
using namespace std;
const int N=1005;
int cmp(string s1,string s2)
{
	if(s1.size()<s2.size()) return 1;
	if(s1.size()>s2.size()) return 0;
	return s1<s2;
}
struct node{
	int len;
	string s;
}b[N];
int main()
{
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	int n,m,i,j;
	string a[N],c[N];
	scanf("%d%d",&n,&m);
	for(i=0;i<n;i++)
	{
		c[i]="-1";
	    cin>>a[i];
    }
	sort(a,a+n,cmp);
	for(i=0;i<n;i++)
		cin>>b[i].len>>b[i].s;
	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
		    if(a[j].size()>=b[i].len)
		    {
		    	string s1=a[j].substr(a[j].size()-b[i].len,b[i].len);
		    	if(s1==b[i].s) 
		    	{
		    		c[i]=a[j];
		    		break;
				}
		    }
	for(i=0;i<n;i++)
	    cout<<c[i]<<endl;
	fclose(stdin); fclose(stdout);
	return 0;
}


const e:array[1..4,1..2]of longint=((1,0),(-1,0),(0,1),(0,-1));
var  n,m,x,y,z,i,t,w,x1,y1,max,j:longint;
a,d:array[0..101,0..101]of longint;
b,c:array[1..10000]of longint;
f:array[1..10000]of boolean;
f1:array[0..101,0..101]of boolean;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(n,m);
fillchar(a,sizeof(a),255);
for i:=1 to n do
  for j:=1 to n do
    d[i,j]:=maxlongint;
for i:=1 to m do
  begin
  readln(x,y,z);
  a[x,y]:=z;
  end;
t:=1;w:=1;
b[1]:=1;c[1]:=1;f[1]:=true;
d[1,1]:=0;f1[1,1]:=true;
max:=maxlongint;
while t<=w do
  begin
  for i:=1 to 4 do
    begin
    x1:=b[t]+e[i,1];
    y1:=c[t]+e[i,2];
    if (x1>0)and(x1<=n)and(y1>0)and(y1<=n) then
      if (a[x1,y1]=a[b[t],c[t]])and(a[x1,y1]<>-1)and(f1[x1,y1]=false)then
        begin
        w:=w+1;
        b[w]:=x1;
        c[w]:=y1;
        d[x1,y1]:=d[b[t],c[t]];
        f[w]:=true;
        if (x1=n)and(y1=n) then
          if d[x1,y1]<max then
            max:=d[x1,y1];
        f1[x1,y1]:=true;
        end
      else
      if (a[x1,y1]=1-a[b[t],c[t]])and (d[x1,y1]>d[b[t],c[t]]+1)then
        begin
        w:=w+1;
        b[w]:=x1;
        c[w]:=y1;
        d[x1,y1]:=d[b[t],c[t]]+1;
        f[w]:=true;
        if (x1=n)and(y1=n) then
          if d[x1,y1]<max then
            max:=d[x1,y1];
        f1[x1,y1]:=true;
        end
      else
      if (a[x1,y1]=-1)and f[t] and (d[x1,y1]>d[b[t],c[t]]+2)then
        begin
        w:=w+1;
        b[w]:=x1;
        c[w]:=y1;
        d[x1,y1]:=d[b[t],c[t]]+2;
        a[x1,y1]:=a[b[t],c[t]];
        f[w]:=false;
        if (x1=n)and(y1=n) then
          if d[x1,y1]<max then
            max:=d[x1,y1];
        f1[x1,y1]:=true;
        end;
    end;
  t:=t+1;
  end;
if max=maxlongint then writeln(-1)
else writeln(max);
close(input);
close(output);
end.

var
  n,q,i,j,t,x:longint;
  f:boolean;
  a:array[-100..2000]of longint;
function consjun(i,j:longint):boolean;
var
  a,b:string;
begin
  str(i,a);
  str(j,b);
  repeat
    if a=b then
      exit(true);
    delete(a,1,1);
  until length(a)=0;
  exit(false);
end;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
    read(a[i]);
  for i:=1 to n-1 do
  for j:=i to n do
    if a[i]>a[j] then
    begin
      t:=a[i];
      a[i]:=a[j];
      a[j]:=t;
    end;
  for i:=1 to q do
  begin
    read(x);
    readln(x);
    f:=false;
    for j:=1 to n do
      if consjun(a[j],x)then
      begin
        writeln(a[j]);
        f:=true;
        break;
      end;
    if not f then
      writeln(-1);
  end;
  close(input);close(output);
end.


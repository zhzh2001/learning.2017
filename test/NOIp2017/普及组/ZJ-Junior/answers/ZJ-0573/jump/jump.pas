uses math;
var n,d,g,i,j,k,t,x,sum,sum1,max1:longint;
    a,f:array[-2001..100001] of longint;
begin
    assign(input,'jump.in'); reset(input);
    assign(output,'jump.out'); rewrite(output);

    readln(n,d,k); max1:=0;
    fillchar(a,sizeof(a),0);
    for i:=1 to n do
    begin
        read(x);
        readln(a[x]);
        max1:=max(max1,x);
    end;
    sum:=0;
    for i:=1 to max1 do
    begin
        if a[i]>0 then sum:=sum+a[i];
    end;
    if sum<k then begin write(-1); close(input); close(output);
        halt; end;
    for i:=1 to 2000 do
    begin
        for j:=-2001 to 0 do f[j]:=-5000000;
        for j:=1 to max1 do f[j]:=0;
        x:=max(d-i,1);
        for j:=x+1 to max1 do
        begin

            for t:=x to d+i do
              f[j]:=max(f[j],f[j-t]+a[j]);
        end;
        if f[max1]>=k then begin write(i); close(input); close(output);
            halt; end;
    end;
    close(input);
    close(output);
end.

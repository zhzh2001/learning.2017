var
      dx:array[1..12] of -2..2;
      dy:array[1..12] of -2..2;
 a,f:array[-1..102,-1..102] of longint;
    b,c:array[-1..102,-1..102] of boolean;
    n,m,i,j,k,t,sum,x,y,x1,y1:longint;
    p:boolean;
function froggy(x,y:longint):boolean;
begin
    froggy:=true;
    for i:=1 to 12 do
        if b[x+dx[i],y+dy[i]] then froggy:=false;
end;
function frog1(x,y:longint):boolean;
begin
    frog1:=true;
    if b[x-1,y] or b[x,y-1] or b[x+1,y] or b[x,y+1] then frog1:=false;
end;
procedure frog(x,y,z,x1,y1:longint);
begin
    if (z>f[x,y]) then exit;
    if (x=m) and (y=m) then begin p:=false; f[x,y]:=z; exit; end;
    if b[x,y] then
    begin
        if froggy(x,y) then exit;
        if b[x+1,y] then
            if a[x,y]=a[x+1,y] then begin f[x,y]:=z; b[x,y]:=false; frog(x+1,y,z,x,y); b[x,y]:=true;end
            else begin f[x,y]:=z; b[x,y]:=false; frog(x+1,y,z+1,x,y); b[x,y]:=true; end
        else begin f[x,y]:=z; b[x,y]:=false; c[x+1,y]:=true; frog(x+1,y,z+2,x,y); b[x,y]:=true; c[x+1,y]:=false; end;
        if b[x,y+1] then
            if a[x,y]=a[x,y+1] then begin f[x,y]:=z; b[x,y]:=false; frog(x,y+1,z,x,y); b[x,y]:=true;end
            else begin f[x,y]:=z; b[x,y]:=false; frog(x,y+1,z+1,x,y); b[x,y]:=true; end
        else begin f[x,y]:=z; b[x,y]:=false; c[x,y+1]:=true; frog(x,y+1,z+2,x,y); b[x,y]:=true; c[x,y+1]:=false; end;
        if b[x-1,y] then
            if a[x,y]=a[x-1,y] then begin f[x,y]:=z; b[x,y]:=false; frog(x-1,y,z,x,y); b[x,y]:=true;end
            else begin f[x,y]:=z; b[x,y]:=false; frog(x-1,y,z+1,x,y); b[x,y]:=true; end
        else begin f[x,y]:=z; b[x,y]:=false; c[x-1,y]:=true; frog(x-1,y,z+2,x,y); b[x,y]:=true; c[x-1,y]:=false; end;
        if b[x,y-1] then
            if a[x,y]=a[x,y-1] then begin f[x,y]:=z; b[x,y]:=false; frog(x,y-1,z,x,y); b[x,y]:=true;end
            else begin f[x,y]:=z; b[x,y]:=false; frog(x,y-1,z+1,x,y); b[x,y]:=true; end
        else begin f[x,y]:=z; b[x,y]:=false; c[x,y+1]:=true; frog(x,y+1,z+2,x,y); b[x,y]:=true; c[x,y-1]:=false; end;
    end
    else
    if c[x,y] then
    begin
        if frog1(x,y) then exit;
        if b[x+1,y] then
           if a[x1,y1]=a[x+1,y] then
           begin f[x,y]:=z; b[x,y]:=false; frog(x+1,y,z,x,y); c[x,y]:=false; end
           else begin f[x,y]:=z; b[x,y]:=false; frog(x+1,y,z+1,x,y); c[x,y]:=false; end;
        if b[x,y+1] then
           if a[x1,y1]=a[x,y+1] then
           begin f[x,y]:=z; b[x,y]:=false; frog(x,y+1,z,x,y); c[x,y]:=false; end
           else begin f[x,y]:=z; b[x,y]:=false; frog(x,y+1,z+1,x,y); c[x,y]:=false; end;
        if b[x-1,y] then
           if a[x1,y1]=a[x-1,y] then
           begin f[x,y]:=z; b[x,y]:=false; frog(x-1,y,z,x,y); c[x,y]:=false; end
           else begin f[x,y]:=z; b[x,y]:=false; frog(x-1,y,z+1,x,y); c[x,y]:=false; end;
        if b[x,y-1] then
           if a[x1,y1]=a[x,y-1] then
           begin f[x,y]:=z; b[x,y]:=false; frog(x,y-1,z,x,y); c[x,y]:=false; end
           else begin f[x,y]:=z; b[x,y]:=false; frog(x,y-1,z+1,x,y); c[x,y]:=false; end;
    end;
end;
begin
    assign(input,'chess.in'); reset(input);
    assign(output,'chess.out'); rewrite(output);
    readln(m,n);
    fillchar(b,sizeof(b),false);
    fillchar(c,sizeof(c),false);
    for i:=1 to m do
        for j:=1 to m do f[i,j]:=maxlongint;
    for i:=1 to n do
    begin
        read(x,y);
        readln(a[x,y]);
        b[x,y]:=true;
    end;
    dx[1]:=-2; dx[2]:=-1; dx[3]:=1; dx[4]:=2; dx[5]:=0; dx[6]:=0; dx[7]:=0;
    dx[8]:=0; dx[9]:=-1; dx[10]:=-1; dx[11]:=1; dx[12]:=1;
    dy[1]:=0; dy[2]:=0; dy[3]:=0; dy[4]:=0; dy[5]:=-2; dy[6]:=-1;
    dy[7]:=1; dy[8]:=2; dy[9]:=-1; dy[10]:=1; dy[11]:=-1; dy[12]:=1;
    p:=true;
        for i:=1 to m do
            for j:=1 to m do
            if not((i=m) and (j=m)) then
                if b[m,m] then
                begin
                    if b[i,j] and ((2*m-i-j)<=2) then p:=false;
                end
                else if b[i,j] and ((2*m-i-j)<2) then p:=false;
    if p then begin write(-1); close(input); close(output);
        halt; end;
    frog(1,1,0,1,1);
    if p then write(-1) else write(f[m,m]);
    close(input);
    close(output);
end.

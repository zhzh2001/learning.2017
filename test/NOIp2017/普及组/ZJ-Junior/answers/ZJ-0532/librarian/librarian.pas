var n,q,i,j:longint;
    a,ls,ai,ys,ls2:array[0..10000] of longint;
    s2,s:array[0..1001] of ansistring;
    c:char;
    b:boolean;
    tmp:ansistring;

    procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                y:=ai[i];
                ai[i]:=ai[j];
                ai[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;

begin
  assign(input,'librarian.in'); assign(output,'librarian.out');
  reset(input); rewrite(output);
  read(n,q);
  for i:=1 to n do ai[i]:=i;
  for i:=1 to n do read(a[i]);
  ys:=a;
  for i:=1 to q do
  begin
    read(ls[i]); read(c);
    read(s[i]);
  end;
  sort(1,n);
  for i:=1 to n do 
  begin
    str(a[i],s2[i]);
    ls2[i]:=length(s2[i]);
  end;
  for i:=1 to q do
  begin
    b:=false;
    for j:=1 to n do
    begin
      if copy(s2[j],ls2[j]-ls[i]+1,ls2[j])=s[i] then
      begin
        b:=true;
        writeln(ys[ai[j]]);
        break;
      end;
    end;
    if b=false then writeln(-1);
  end;
  close(input); close(output);
end.

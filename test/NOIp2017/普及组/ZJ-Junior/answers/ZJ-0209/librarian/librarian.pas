var
  n,q,len,x:longint;
  i,j:longint;
  t:longint;
  a:array[0..2001]of longint;
  min:longint;
  f:boolean;
begin
  assign(input,'librarian.in');reset(input);
  assign(output,'librarian.out');rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to q do begin
    readln(len,x);
    t:=1;
    for j:=1 to len do
      t:=t*10;
    f:=false;
    min:=maxlongint;
    for j:=1 to n do
      if a[j] mod t=x then begin
        f:=true;
        if(a[j]<min) then
          min:=a[j];
      end;
    if (not f) then writeln('-1')
    else writeln(min);
  end;
  close(input);
  close(output);
end.
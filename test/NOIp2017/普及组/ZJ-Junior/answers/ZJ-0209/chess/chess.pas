const
  dx:array[1..4]of longint=(-1,0,1,0);
  dy:array[1..4]of longint=(0,-1,0,1);
var
  f,ff:array[-10..201,-10..201]of longint;
  last,map:array[-10..201,-10..201]of longint;
  i,j,k,x,y,c,n,m:longint;
  g:boolean;
function min(a,b:longint):longint;
begin
  if(a<b) then exit(a) else exit(b);
end;
begin
  assign(input,'chess.in');reset(input);
  assign(output,'chess.out');rewrite(output);
  readln(n,m);
  fillchar(map,sizeof(map),0);
  for i:=1 to m do begin
    read(x,y,c);
    map[x,y]:=c+1;
  end;
  fillchar(f,sizeof(f),$3F);
  f[1,1]:=0;
  ff:=f;
  while(true) do begin
  for i:=1 to n do
    for j:=1 to n do begin
      if(i=1) and (j=1) then continue;
      for k:=1 to 2 do begin
        x:=i+dx[k];y:=j+dy[k];
        if(map[i,j]>0) and (map[x,y]>0) then
          if(map[i,j]=map[x,y]) then f[i,j]:=min(f[i,j],f[x,y])
          else f[i,j]:=min(f[i,j],f[x,y]+1);
        if(map[i,j]=0) and (map[x,y]>0) then begin
          if(f[i,j]>f[x,y]+2) then begin
            last[i,j]:=map[x,y];
            f[i,j]:=f[x,y]+2;
          end;
        end;
        if(map[i,j]>0) and (map[x,y]=0) then begin
          if(map[i,j]<>last[x,y]) then
            f[i,j]:=min(f[i,j],f[x,y]+1)
          else
            f[i,j]:=min(f[i,j],f[x,y]);
        end;
      end;
    end;
  fillchar(last,sizeof(last),0);
  for i:=1 to n do
    for j:=n downto 1 do begin
      if(i=1) and (j=n) then continue;
      for k:=1 to 4 do begin
        if(k=2) or (k=3) then continue;
        x:=i+dx[k];y:=j+dy[k];
        if(map[i,j]>0) and (map[x,y]>0) then
          if(map[i,j]=map[x,y]) then f[i,j]:=min(f[i,j],f[x,y])
          else f[i,j]:=min(f[i,j],f[x,y]+1);
        if(map[i,j]=0) and (map[x,y]>0) then begin
          if(f[i,j]>f[x,y]+2) then begin
            last[i,j]:=map[x,y];
            f[i,j]:=f[x,y]+2;
          end;
        end;
        if(map[i,j]>0) and (map[x,y]=0) then begin
          if(map[i,j]<>last[x,y]) then
            f[i,j]:=min(f[i,j],f[x,y]+1)
          else
            f[i,j]:=min(f[i,j],f[x,y]);
        end;
      end;
    end;
  fillchar(last,sizeof(last),0);
  for i:=n downto 1 do
    for j:=1 to n do begin
      if(i=n) and (j=1) then continue;
      for k:=2 to 3 do begin
        x:=i+dx[k];y:=j+dy[k];
        if(map[i,j]>0) and (map[x,y]>0) then
          if(map[i,j]=map[x,y]) then f[i,j]:=min(f[i,j],f[x,y])
          else f[i,j]:=min(f[i,j],f[x,y]+1);
        if(map[i,j]=0) and (map[x,y]>0) then begin
          if(f[i,j]>f[x,y]+2) then begin
            last[i,j]:=map[x,y];
            f[i,j]:=f[x,y]+2;
          end;
        end;
        if(map[i,j]>0) and (map[x,y]=0) then begin
          if(map[i,j]<>last[x,y]) then
            f[i,j]:=min(f[i,j],f[x,y]+1)
          else
            f[i,j]:=min(f[i,j],f[x,y]);
        end;
      end;
    end;
  fillchar(last,sizeof(last),0);
  for i:=n downto 1 do
    for j:=n downto 1 do begin
      if(i=n) and (j=n) then continue;
      for k:=3 to 4 do begin
        x:=i+dx[k];y:=j+dy[k];
        if(map[i,j]>0) and (map[x,y]>0) then
          if(map[i,j]=map[x,y]) then f[i,j]:=min(f[i,j],f[x,y])
          else f[i,j]:=min(f[i,j],f[x,y]+1);
        if(map[i,j]=0) and (map[x,y]>0) then begin
          if(f[i,j]>f[x,y]+2) then begin
            last[i,j]:=map[x,y];
            f[i,j]:=f[x,y]+2;
          end;
        end;
        if(map[i,j]>0) and (map[x,y]=0) then begin
          if(map[i,j]<>last[x,y]) then
            f[i,j]:=min(f[i,j],f[x,y]+1)
          else
            f[i,j]:=min(f[i,j],f[x,y]);
        end;
      end;
    end;
    g:=true;
    for i:=1 to n do
      for j:=1 to n do
        if f[i,j]<>ff[i,j] then g:=false;
    if(g) then break;
    ff:=f;
  end;
  if (f[n,n]=$3F3F3F3F) then writeln('-1')
  else writeln(f[n,n]);
  close(input);
  close(output);
end.
#include<bits/stdc++.h>
#include<cstdio>
#include<iostream>
#include<cmath>
using namespace std;
string ans,a[1010],b;
char ch;
int n,q,m;
string str_min(string x,string y){
	if(x.size()>y.size()) return y;
	if(x.size()<y.size()) return x;
	int len=x.size();
	for(int i=0;i<len;i++){
		if(x[i]<y[i]) return x;
		if(y[i]<x[i]) return y;
	}
}
int solve(string x,string y){
	if(y.size()>x.size()) return -1;
	for(int i=x.size()-y.size();i>=0;i--){
		int flag=0;
		for(int j=0;j<y.size();j++)
			if(x[i+j]!=y[j]){flag++;break;}
		if(!flag) return i;
	}
	return -1;
}
int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	scanf("%d%d",&n,&q);
	for(int i=1;i<=n;i++){
		cin>>a[i];
		//if(a[i]=="3545") cout<<a[i]<<endl;
	}
	for(int i=1;i<=q;i++){
		b="";
		scanf("%d",&m);
		cin>>b;
		int l=b.size();
		int flag=0;
		ans="999999999";
		for(int j=1;j<=n;j++)
			if(solve(a[j],b)!=-1&&solve(a[j],b)==a[j].size()-l) ans=str_min(ans,a[j]),flag=1;
		if(!flag) puts("-1");
		else cout<<ans<<endl; 
	}
	return 0;
}

#include<bits/stdc++.h>
#include<cstdio>
using namespace std;
int n,d,k,x[500010],s[500010],cnt,maxs,mins;
int solve(int g){
	if(g+d>maxs){
		if(g<d){
			if(d-g>mins) return 0;
			return 1; 
		}
		return 1;
	}
	return 0;
}
int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	scanf("%d%d%d",&n,&d,&k);
	for(int i=1;i<=n;i++)
	scanf("%d%d",&x[i],&s[i]),cnt+=s[i],maxs=max(maxs,s[i]-s[i-1]),mins=min(mins,s[i]-s[i-1]);
	if(cnt<k){puts("-1");return 0;}
	if(cnt==k){printf("%d",d-1);return 0;}
	if(d==1){
		for(int i=1;i<=n;i++)if(s[i]-s[i-1]>1){puts("-1");return 0;}
		puts("0");
		return 0;
	}
	for(int g=0;;g++)
		if(solve(g)){printf("%d",g/2);return 0;}
	return 0;
}

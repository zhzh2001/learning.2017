#include<bits/stdc++.h>
#include<cstdio>
using namespace std;
int mp[1010][1010],c,x,ans=1e9,y,n,m,vis[1010][1010];
long long begin;
void solve(int x,int y,int cnt){
	if(clock()-begin>=950){printf("%d",ans);exit(0);}
	if(vis[x][y]) return;
	//if(clock()-begin>=950){puts("-1");exit(0);}
	if(x==m&&y==m){ans=min(ans,cnt);return;}
	if(x<1||y<1||x>m||y>m) return;
	if(!mp[x][y]&&!mp[x-1][y]&&!mp[x+1][y]&&!mp[x][y-1]&&!mp[x][y+1]) return;
	//if(!mp[x][y]){
	//	if(!f[x-1][y]) f[x][y]=1,solve(x-1,y,cnt+2);
	//	if(!f[x+1][y]) f[x][y]=1,solve(x+1,y,cnt+2);
	//	if(!f[x][y-1]) f[x][y]=1,solve(x,y-1,cnt+2);
	//	if(!f[x][y+1]) f[x][y]=1,solve(x,y+1,cnt+2);
	//	puts("1");
	//}
	vis[x][y]=1;
	if(mp[x][y]||mp[x-1][y])solve(x-1,y,cnt+(mp[x][y]!=mp[x-1][y]));
	if(mp[x][y]||mp[x+1][y])solve(x+1,y,cnt+(mp[x][y]!=mp[x+1][y]));
  	if(mp[x][y]||mp[x][y-1])solve(x,y-1,cnt+(mp[x][y]!=mp[x][y-1]));
  	if(mp[x][y]||mp[x][y+1])solve(x,y+1,cnt+(mp[x][y]!=mp[x][y+1]));
}
int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	scanf("%d%d",&m,&n);
	while(n--){
		scanf("%d%d%d",&x,&y,&c);
		mp[x][y]=c+1;
	}
	begin=clock();
	solve(1,1,0);
	if(ans==1e9) puts("-1");
	else printf("%d",ans);
	return 0;
}

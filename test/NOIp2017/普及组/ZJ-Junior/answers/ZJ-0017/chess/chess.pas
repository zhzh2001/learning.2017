const dx:array[1..4] of longint=(1,0,-1,0);
      dy:array[1..4] of longint=(0,1,0,-1);

var c:array[0..150,0..150] of longint;
    b:array[0..150,0..150] of boolean;
    i,j,k,m,n,xx,yy,ans:longint; found:boolean;

procedure init;
begin
    assign(input,'chess.in');
    reset(input);
    assign(output,'chess.out');
    rewrite(output);
end;

procedure search(x,y,tot:longint; l:boolean);
var i,j:longint; nx,ny:longint;
begin
    if (x=m) and (y=m) then begin found:=true; if tot<ans then ans:=tot; exit; end;
    if tot>ans then exit;
    for i:=1 to 4 do
    begin
      nx:=x+dx[i]; ny:=y+dy[i];
      if (nx>=1) and (nx<=m) and (ny>=1) and (ny<=m) and (not b[nx,ny]) then
      begin
        if c[nx,ny]=2 then
        begin
          if not l then begin c[nx,ny]:=c[x,y]; b[nx,ny]:=true; search(nx,ny,tot+2,true); b[nx,ny]:=false; c[nx,ny]:=2; end;
        end
        else
        begin
          if c[nx,ny]<>c[x,y] then begin b[nx,ny]:=true; search(nx,ny,tot+1,false); b[nx,ny]:=false; end;
          if c[nx,ny]=c[x,y] then begin b[nx,ny]:=true; search(nx,ny,tot,false); b[nx,ny]:=false; end;
        end;
      end;
    end;
end;

begin
    init;
    readln(m,n); ans:=maxlongint;
    for i:=1 to m do for j:=1 to m do c[i,j]:=2;
    for i:=1 to n do
    begin
      readln(xx,yy,k);
      c[xx,yy]:=k;
    end;
    b[1,1]:=true;
    search(1,1,0,false);
    if not found then writeln('-1') else writeln(ans);
    close(output);
end.














{var f,c:array[0..10050] of longint;
    //c:array[0..150,0..150] of longint;
    i,j,k,m,n,l,r,x,y:longint;

procedure init;
begin
    assign(input,'chess.in');
    reset(input);
    assign(output,'chess.out');
    rewrite(output);
end;

function min(a,b:longint):longint;
begin
    if a<b then exit(a) else exit(b);
end;

begin
    readln(m,n);
    for i:=0 to m*m do c[i]:=2;
    for i:=1 to n do
    begin
      readln(x,y,k);
      c[x*(m-1)+y]:=k;
    end;
    for i:=1 to m*m do f[i]:=maxlongint>>1;
    for i:=1 to m*m do
    begin
      if (i-1 mod m>0) then if (c[i]=2) and (c[i-1]<>2) then f[i]:=min(f[i],f[i-1]+2);
      if (i+1 mod m<=m) then if (c[i]=2) and (c[i+1]<>2) then   f[i]:=min(f[i],f[i+1]+2);
      if (i-m mod m>0) then if (c[i]=2) and (c[i-m]<>2) then f[i]:=min(f[i],f[i-m]+2);
      if (i+m mod m<=m) then if (c[i]=2) and (c[i+m]<>2) then f[i]:=min(f[i],f[i-m]+2);

      if (i-1 mod m>0) then if c[i-1]=c[i] then f[i]:=min(f[i],f[i-1]);
      if (i+1 mod m<=m) then if c[i+1]=c[i] then f[i]:=min(f[i],f[i+1]);
      if (i-m mod m>0) then if c[i-m]=c[i] then f[i]:=min(f[i],f[i-m]);
      if (i+m mod m<=m) then if c[i+m]=c[i] then f[i]:=min(f[i],f[i-m]);

      if (i-1 mod m>0) then if c[i-1]<>c[i] then f[i]:=min(f[i],f[i-1]+1);
      if (i+1 mod m<=m) then if c[i+1]<>c[i] then f[i]:=min(f[i],f[i+1]+1);
      if (i-m mod m>0) then if c[i-m]<>c[i] then f[i]:=min(f[i],f[i-m]+1);
      if (i+m mod m<=m) then if c[i+m]<>c[i] then f[i]:=min(f[i],f[i-m]+1);
    end;

    writeln(f[m*m]);
    close(output);
end.}








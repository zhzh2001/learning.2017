var a,b,c,ans:longint;

procedure init;
begin
    assign(input,'score.in');
    reset(input);
    assign(output,'score.out');
    rewrite(output);
end;

begin
    init;
    readln(a,b,c);
    a:=a*20 div 100;
    b:=b*30 div 100;
    c:=c*50 div 100;
    ans:=a+b+c;
    writeln(ans);
    close(output);
end.

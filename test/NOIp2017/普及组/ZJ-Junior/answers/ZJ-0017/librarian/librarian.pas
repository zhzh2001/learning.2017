var a:array[0..1050] of longint;
    q,need:array[0..1050] of longint;
    i,j,k,m,n,p,tmp,ans,t:longint; f:boolean;

procedure init;
begin
    assign(input,'librarian.in');
    reset(input);
    assign(output,'librarian.out');
    rewrite(output);
end;

procedure sort(l,r: longint);
var i,j,x,y: longint;
begin
    i:=l; j:=r; x:=a[(l+r) div 2];
    repeat
      while a[i]<x do inc(i);
      while x<a[j] do dec(j);
      if not(i>j) then
      begin
        y:=a[i]; a[i]:=a[j]; a[j]:=y;
        inc(i);
        j:=j-1;
      end;
    until i>j;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
end;

begin
    init;
    readln(n,m);
    for i:=1 to n do readln(a[i]); sort(1,n); for i:=1 to m do readln(q[i],need[i]);

    for i:=1 to m do
    begin
      tmp:=1; f:=false;
      for t:=1 to q[i] do tmp:=tmp*10;
      for j:=1 to n do
      begin
        p:=a[j] mod tmp;
        if p=need[i] then begin writeln(a[j]); f:=true; break; end;
      end;
      if not f then writeln('-1');
    end;

    close(output);
end.




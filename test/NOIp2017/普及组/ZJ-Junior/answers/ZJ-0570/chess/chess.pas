const dx:array[1..4]of longint=(1,0,-1,0);
      dy:array[1..4]of longint=(0,1,0,-1);
var
        i,j,n,m,x,y,z:longint;
        f:array[0..1005,0..1005] of longint;
        flag:array[0..1005,0..1005] of boolean;
        map,m2:array[0..1005,0..1005] of -1..1;
function min(n,m:longint):longint;
begin
        if n<m then exit(n) else exit(m);
end;
procedure dfs(x,y:longint);
var  i,p,q:longint;
begin
        flag[x,y]:=true;
        for i:=1 to 4 do
        begin
                p:=x+dx[i];q:=y+dy[i];
                if (p>n)or(p<1)or(q>n)or(q<1) then continue;
                if m2[x,y]<>-1 then
                begin
                        if map[p,q]=-1 then
                        begin
                                f[p,q]:=min(f[p,q],f[x,y]+2);
                                map[p,q]:=m2[x,y];
                                if flag[p,q]=false then dfs(p,q);
                                map[p,q]:=-1;
                        end else
                        begin
                        if map[x,y]<>map[q,p] then
                        begin
                                f[p,q]:=min(f[p,q],f[x,y]+1);
                                if flag[p,q]=false then dfs(p,q);
                        end;
                        if map[x,y]=map[q,p] then
                        begin
                                f[p,q]:=min(f[p,q],f[x,y]);
                                if flag[p,q]=false then dfs(p,q);
                        end;
                end;
                end;
                if m2[x,y]=-1 then
                begin
                        if map[q,p]=-1 then continue;
                        if map[x,y]<>map[q,p] then
                        begin
                                f[p,q]:=min(f[p,q],f[x,y]+1);
                                if flag[p,q]=false then dfs(p,q);
                        end;
                        if map[x,y]=map[q,p] then
                        begin
                                f[p,q]:=min(f[p,q],f[x,y]);
                                if flag[p,q]=false then dfs(p,q);
                        end;
                end;
        end;
end;
begin
        assign(input,'chess.in');reset(input);
        assign(output,'chess.out');rewrite(output);
        readln(n,m);
        for i:=1 to n do for j:=1 to m do f[i,j]:=maxlongint;
        for i:=1 to n do for j:=1 to n do map[i,j]:=-1;
        for i:=1 to m do
        begin
                readln(x,y,z);
                map[x,y]:=z;
        end;
        for i:=1 to n do for j:=1 to n do m2[i,j]:=map[i,j];
        f[n,n]:=0;
        dfs(n,n);
        if f[1,1]=maxlongint then writeln(-1) else writeln(f[1,1]);
        close(input);close(output);
end.

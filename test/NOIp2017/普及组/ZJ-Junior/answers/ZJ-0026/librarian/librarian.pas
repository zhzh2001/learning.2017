var i,j,k,n,q,ans,qu,len,p:longint;
    a:array[1..1000]of longint;
function min(a,b:longint):longint;
begin
  if a>b then exit(b) else exit(a);
end;

begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);

  read(n,q);
  for i:=1 to n do
    read(a[i]);
  for i:=1 to q do
  begin
    ans:=maxlongint;
    p:=1;
    read(len,qu);
    for j:=1 to len do
     p:=p*10;
    for j:=1 to n do
      if (a[j] mod p) = qu
      then ans:=min(ans,a[j]);
    if ans=maxlongint
    then writeln(-1)
    else writeln(ans);
  end;
  close(input);
  close(output);

end.

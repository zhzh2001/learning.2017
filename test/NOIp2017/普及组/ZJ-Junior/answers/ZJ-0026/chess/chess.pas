type jd=record
     x,y,b,f:longint;
     end;
var i,j,k,m,n,x,y,z,h,t:longint;
    map,f,qq:array[0..101,0..101]of longint;
    b:array[0..101,0..101]of boolean;
    c:array[0..100000]of jd;
procedure push(x,y,z:longint);
begin
  b[x,y]:=true;
  inc(t);
  c[t].x:=x;
  c[t].y:=y;
  c[t].b:=z;
end;

procedure spfa;
var x,y:longint;
begin
  push(1,1,0);
  repeat
    inc(h);x:=c[h].x;y:=c[h].y;
    if(x>1)
    then begin
           if(map[x-1,y]=3)and(c[h].b=0)
           then begin
                  if f[x,y]+2<f[x-1,y]
                  then begin
                         f[x-1,y]:=f[x,y]+2;
                         qq[x-1,y]:=map[x,y];
                         if not b[x-1,y]
                         then push(x-1,y,1);
                       end
                end;
           if(map[x-1,y]<>map[x,y])and(c[h].b=0)and(map[x-1,y]<>3)
           then if f[x,y]+1<f[x-1,y]
                then begin
                       f[x-1,y]:=f[x,y]+1;
                       if not b[x-1,y]
                       then push(x-1,y,0);
                end;
           if(map[x-1,y]=map[x,y])and(c[h].b=0)and(map[x-1,y]<>3)
           then if f[x,y]<f[x-1,y]
                then begin
                       f[x-1,y]:=f[x,y];
                       if not b[x-1,y]
                       then push(x-1,y,0);
                end;
           if(map[x-1,y]<>3)and(c[h].b=1)and(qq[x,y]=map[x-1,y])
           then if f[x,y]<f[x-1,y]
                then begin
                       f[x-1,y]:=f[x,y];
                       if not b[x-1,y]
                       then push(x-1,y,0);
                end;
           if(map[x-1,y]<>3)and(c[h].b=1)and(qq[x,y]<>map[x-1,y])
           then if f[x,y]+1<f[x-1,y]
                then begin
                       f[x-1,y]:=f[x,y]+1;
                       if not b[x-1,y]
                       then push(x-1,y,0);
                end;
         end;
    if(x<m)
    then begin
           if(map[x+1,y]=3)and(c[h].b=0)
           then begin
                  if f[x,y]+2<f[x+1,y]
                  then begin
                         f[x+1,y]:=f[x,y]+2;
                         qq[x+1,y]:=map[x,y];
                         if not b[x+1,y]
                         then push(x+1,y,1);
                       end
                end;
           if(map[x+1,y]<>map[x,y])and(c[h].b=0)and(map[x+1,y]<>3)
           then if f[x,y]+1<f[x+1,y]
                then begin
                       f[x+1,y]:=f[x,y]+1;
                       if not b[x+1,y]
                       then push(x+1,y,0);
                end;
           if(map[x+1,y]=map[x,y])and(c[h].b=0)and(map[x+1,y]<>3)
           then if f[x,y]<f[x+1,y]
                then begin
                       f[x+1,y]:=f[x,y];
                       if not b[x+1,y]
                       then push(x+1,y,0);
                end;
           if(map[x+1,y]<>3)and(c[h].b=1)and(qq[x,y]=map[x+1,y])
           then if f[x,y]<f[x+1,y]
                then begin
                       f[x+1,y]:=f[x,y];
                       if not b[x+1,y]
                       then push(x+1,y,0);
                end;
           if(map[x+1,y]<>3)and(c[h].b=1)and(qq[x,y]<>map[x+1,y])
           then if f[x,y]<f[x+1,y]
                then begin
                       f[x+1,y]:=f[x,y]+1;
                       if not b[x+1,y]
                       then push(x+1,y,0);
                end;
         end;
    if(y>1)
    then begin
           if(map[x,y-1]=3)and(c[h].b=0)
           then begin
                  if f[x,y]+2<f[x,y-1]
                  then begin
                         f[x,y-1]:=f[x,y]+2;
                         qq[x,y-1]:=map[x,y];
                         if not b[x,y-1]
                         then push(x,y-1,1);
                       end
                end;
           if(map[x,y-1]<>map[x,y])and(c[h].b=0)and(map[x,y-1]<>3)
           then if f[x,y]+1<f[x,y-1]
                then begin
                       f[x,y-1]:=f[x,y]+1;
                       if not b[x,y-1]
                       then push(x,y-1,0);
                end;
           if(map[x,y-1]=map[x,y])and(c[h].b=0)and(map[x,y-1]<>3)
           then if f[x,y]<f[x,y-1]
                then begin
                       f[x,y-1]:=f[x,y];
                       if not b[x,y-1]
                       then push(x,y-1,0);
                end;
           if(map[x,y-1]<>3)and(c[h].b=1)and(qq[x,y]=map[x,y-1])
           then if f[x,y]<f[x,y-1]
                then begin
                       f[x,y-1]:=f[x,y];
                       if not b[x,y-1]
                       then push(x,y-1,0);
                end;
           if(map[x,y-1]<>3)and(c[h].b=1)and(qq[x,y]<>map[x,y-1])
           then if f[x,y]+1<f[x,y-1]
                then begin
                       f[x,y-1]:=f[x,y]+1;
                       if not b[x,y-1]
                       then push(x,y-1,0);
                end;
         end;
    if(y<m)
    then begin
           if(map[x,y+1]=3)and(c[h].b=0)
           then begin
                  if f[x,y]+2<f[x,y+1]
                  then begin
                         f[x,y+1]:=f[x,y]+2;
                         qq[x,y+1]:=map[x,y];
                         if not b[x,y+1]
                         then push(x,y+1,1);
                       end
                end;
           if(map[x,y+1]<>map[x,y])and(c[h].b=0)and(map[x,y+1]<>3)
           then if f[x,y]+1<f[x,y+1]
                then begin
                       f[x,y+1]:=f[x,y]+1;
                       if not b[x,y+1]
                       then push(x,y+1,0);
                end;
           if(map[x,y+1]=map[x,y])and(c[h].b=0)and(map[x,y+1]<>3)
           then if f[x,y]<f[x,y+1]
                then begin
                       f[x,y+1]:=f[x,y];
                       if not b[x,y+1]
                       then push(x,y+1,0);
                end;
           if(map[x,y+1]<>3)and(c[h].b=1)and(qq[x,y]=map[x,y+1])
           then if f[x,y]<f[x,y+1]
                then begin
                       f[x,y+1]:=f[x,y];
                       if not b[x,y+1]
                       then push(x,y+1,0);
                end;
           if(map[x,y+1]<>3)and(c[h].b=1)and(qq[x,y]<>map[x,y+1])
           then if f[x,y]+1<f[x,y+1]
                then begin
                       f[x,y+1]:=f[x,y]+1;
                       if not b[x,y+1]
                       then push(x,y+1,0);
                end;
         end;
    b[x,y]:=false;
  until h>=t;

end;

begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);

  read(m,n);
  for i:=1 to m do
    for j:=1 to m do
    begin
      map[i,j]:=3;
      f[i,j]:=maxlongint div 3;
    end;
  for i:=1 to n do
  begin
    read(x,y,z);
    map[x,y]:=z;
  end;
  f[1,1]:=0;
  spfa;
  if f[m,m]=maxlongint div 3
  then writeln(-1)
  else writeln(f[m,m]);

  close(input);
  close(output);

end.


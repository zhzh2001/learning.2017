var i,j,k,n,d,l,r,mid:longint;
    map:array[0..500000]of longint;
    s:array[0..500000]of longint;
    lis:array[0..500000]of int64;
function max(a,b:longint):longint;
begin
  if a>b then exit(a)else exit(b);
end;

function judge(x:longint):boolean;
var i,j:longint;
    b:boolean;
begin
  for i:=1 to n do
    lis[i]:=-maxlongint div 3;
  for i:=1 to n do
  begin
    b:=false;
    for j:=i-1 downto 0 do
    begin
      if map[i]-map[j]<max(d-x,1)then continue;
      if map[i]-map[j]>d+x then break;
      b:=true;
      lis[i]:=max(lis[i],lis[j]+s[i]);
    end;
    if b=false then exit(false);
  end;
  if lis[n]>=k then exit(true) else exit(false);
end;

begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);

  read(n,d,k);
  for i:=1 to n do
    read(map[i],s[i]);
  l:=-2;
  r:=map[n]+2;
  while (l<r) do
  begin
    mid:=(l+r+1)div 2;
    if mid<0
    then begin
           writeln(0);
           halt;
         end;
    if mid>map[n]
    then begin
           writeln(-1);
           halt;
         end;
    if judge(mid)
    then r:=mid-1
    else l:=mid;
  end;
  writeln(r+1);
  close(input);
  close(output);

end.


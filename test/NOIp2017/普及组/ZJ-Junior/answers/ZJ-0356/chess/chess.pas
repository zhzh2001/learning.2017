var
  m,n,i,j,k,x,y,c,sum:longint;
  a:array[1..101,1..101]of longint;
  boom:boolean;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=2;
  for i:=1 to n do
    begin
      readln(x,y,c);
      a[x,y]:=c;
    end;
  x:=1;y:=1;
  while (x<>m)and(y<>m) do
    begin
      if a[x+1,y]<>2 then
        begin
          if a[x+1,y]=a[x,y] then
            begin
              inc(x);
              boom:=true;
              continue;
            end
          else
            begin
              inc(sum);
              inc(x);
              boom:=true;
              continue;
            end;
        end
      else
        if a[x,y+1]<>2 then
          begin
            if a[x,y+1]=a[x,y] then
              begin
                inc(y);
                boom:=true;
                continue;
              end
            else
              begin
                inc(sum);
                inc(y);
                boom:=true;
                continue;
              end;
          end
        else
          if a[x+1,y+1]<>2 then
            begin
              if boom=true then
                begin
                  boom:=false;
                  sum:=sum+2;
                  inc(x);
                  if a[x+1,y+1]=a[x,y] then
                    begin
                      inc(y);
                      boom:=true;
                      continue;
                    end
                  else
                    begin
                      inc(y);
                      inc(sum);
                      boom:=true;
                      continue;
                    end;
                end
              else
                begin
                  write('-1');
                  close(input);
                  close(output);
                  exit;
                end
            end
            else
              if a[x+2,y]<>2 then
                begin
                  if boom=true then
                    begin
                      boom:=false;
                      sum:=sum+2;
                      inc(x);
                      if a[x+2,y]=a[x,y] then
                        begin
                          inc(x);
                          boom:=true;
                          continue;
                        end
                      else
                        begin
                          inc(sum);
                          inc(x);
                          boom:=true;
                          continue;
                        end;
                    end
                  else
                    begin
                      write('-1');
                      close(input);
                      close(output);
                      exit;
                    end;
                end
              else
                if a[x,y+2]<>2 then
                  begin
                    if boom=true then
                      begin
                        boom:=false;
                        sum:=sum+2;
                        inc(y);
                        if a[x,y+2]=a[x,y] then
                          begin
                            inc(y);
                            boom:=true;
                            continue;
                          end
                        else
                          begin
                            inc(sum);
                            inc(y);
                            boom:=true;
                            continue;
                          end;
                      end
                    else
                      begin
                        write('-1');
                        close(input);
                        close(output);
                        exit;
                      end;
                  end
          else
            begin
              write('-1');
              close(input);
              close(output);
              exit;
            end;
    end;
  write(sum);
  close(input);
  close(output);
end.


const z:array[1..4,1..2] of longint=((1,0),(0,1),(0,-1),(-1,0));
var n,m,i,x,y,c,ans:longint;
    f:array[0..105,0..105] of longint;
    flag:array[0..105,0..105] of boolean;
    f1:array[0..101,0..101,-1..1] of longint;
function sc(x,y,c,sum:longint):longint;
var i,x1,y1:longint;
begin
if sum>=ans then
  exit;
if (x=m) and (y=m) then
  begin
  if sum<ans then
    ans:=sum;
  exit;
  end;
if (f1[x,y,c]<=sum) and (f1[x,y,c]<>-1) then
  exit(f1[x,y,c]);
for i:=1 to 4 do
  begin
  x1:=x+z[i,1];
  y1:=y+z[i,2];
  if (x1>=1) and (y1>=1) and (x1<=m) and (y1<=m) and (flag[x1,y1]=false) then
    begin
    flag[x1,y1]:=true;
    if (f[x1,y1]=-1) and (f[x,y]<>-1) then
      begin
      if f[x,y]=0 then
        sc(x1,y1,0,sum+2)
      else
        sc(x1,y1,0,sum+3);
      if f[x,y]=1 then
        sc(x1,y1,1,sum+2)
      else
        sc(x1,y1,1,sum+3);
      end;
    if (f[x1,y1]=c) then
      sc(x1,y1,c,sum);
    if (f[x1,y1]=1-c) then
      sc(x1,y1,f[x1,y1],sum+1);
    flag[x1,y1]:=false;
    end;
  end;
if (sum<f1[x,y,c]) or (f1[x,y,c]=-1) then
  f1[x,y,c]:=sum;
end;
begin
assign(input,'chess.in');reset(input);
assign(output,'chess.out');rewrite(output);
readln(m,n);
fillchar(f,sizeof(f),255);
for i:=1 to n do
  begin
  readln(x,y,c);
  f[x,y]:=c;
  end;
ans:=maxlongint;
flag[1,1]:=true;
fillchar(f1,sizeof(f1),255);
sc(1,1,f[1,1],0);
if ans=maxlongint then
  writeln(-1)
else
  writeln(ans);
close(input);close(output);
end.

#include<iostream>
#include<fstream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#include<stack>
using namespace std;
bool magic=false,mapa[110][110];
int m,n,a,b,mapb[110][110],mapc[110][110];
void dfs(int x1,int y1,int x2,int y2)
{
	if(mapa[x2][y2])
	{
		if(mapb[x2][y2]==mapb[x1][y1])
		{
			mapc[x2][y2]=min(mapc[x2][y2],mapc[x1][y1]);
			magic=false;
		}
		else
		{
			mapc[x2][y2]=min(mapc[x2][y2],mapc[x1][y1]+1);
			magic=false;
		}
	}
	else
	{
		if(!magic)			
		{
			mapc[x2][y2]=min(mapc[x2][y2],mapc[x1][y1]+2);
			if(mapc[x2][y2]>mapc[x1][y1]+2)
			{
				magic=true;
			}
			else
			{
				magic=false;
			}
		}
	}
}
int find(int x1,int y1)
{
	if(x1==m&&y1==m)
	{
		return mapc[m][m];
		
	}
	else
	{
		if(x1==m)
		{
			dfs(x1,y1,x1,y1+1);
			find(x1,y1+1);
		}
		else
		{
			
			
			if(y1==m)
			{
				dfs(x1,y1,x1+1,y1);
				find(x1+1,y1);
			}
			else
			{
				dfs(x1,y1,x1,y1+1);
				dfs(x1,y1,x1+1,y1);
				find(x1,y1+1);
				find(x1+1,y1);
			}
		}
	}
}
int main()
{
	ifstream inFile("chess.in");
	ofstream outFile("chess.out");
	inFile>>m>>n;
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=m;j++)
		{
			mapa[i][j]=false;
		}
	}
	for(int i=1;i<=n;i++)
	{
		int x,y;
		inFile>>x>>y;
		inFile>>mapb[x][y];
		mapa[x][y]=true;
	}
	for(int i=1;i<=m;i++)
	{
		for(int j=1;j<=m;j++)
		{
			mapc[i][j]=0x3f3f3f;
		}
	}
	mapc[1][1]=0;
	find(1,1);
	outFile<<mapc[m][m]<<endl;
	inFile.close();
	outFile.close();
	return 0;
}

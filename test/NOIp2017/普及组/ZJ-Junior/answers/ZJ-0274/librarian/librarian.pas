var
  a,b,c:array[1..1500] of longint;
  n,q,ans,i,j,t:longint;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to n do
    readln(a[i]);
  for i:=1 to q do
    begin
      readln(c[i],b[i]);
      t:=1;
      for j:=1 to c[i]do
        t:=t*10;
      c[i]:=t;
    end;
  for i:=1 to q do
    begin
      ans:=maxlongint;
      for j:=1 to n do
        if ((a[j]mod c[i])=b[i]) then
          if ans>a[j] then
            ans:=a[j];
      if ans=maxlongint then
        writeln(-1)
      else
        writeln(ans);
    end;
  close(input);
  close(output);
end.

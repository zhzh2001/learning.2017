var
  x,y,z,s,r,l,i:longint;
  a,b,c:array[0..500100] of longint;
begin
  assign(input,'jump.in');
  assign(output,'jump.out');
  reset(input);
  rewrite(output);
  read(x,y,z);
  s:=0;
  a[0]:=0;
  for i:=1 to x do
    begin
      read(a[i],b[i]);
      if b[i]>0 then
        inc(s,b[i]);
    end;
  if s<z then
    write(-1)
  else
    begin
      l:=0;
      r:=maxlongint;
      for i:=1 to x do
        begin
          c[i-1]:=a[i]-a[i-1];
          if c[i-1]>l then
            l:=c[i-1];
          if c[i-1]<r then
            r:=c[i-1];
        end;
      r:=abs(r-y);
      l:=abs(y-l);
      write((r+l)div 2)
    end;
  close(input);
  close(output);
end.

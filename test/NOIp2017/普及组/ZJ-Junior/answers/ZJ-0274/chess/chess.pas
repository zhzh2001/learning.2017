var
  a,c:array[1..100,1..100] of longint;
  m,n,x,y,z,i,j:longint;
procedure gen(p,q:longint);
  var
    t:longint;
  begin
    if (p>1)and not((a[p-1,q]=0)and (a[p,q]=0))  then
      begin
        case a[p-1,q] of
          0:if c[p-1,q]>(c[p,q]+2) then
            begin
              t:=a[p-1,q];
              a[p-1,q]:=a[p,q];
              c[p-1,q]:=c[p,q]+2;
              gen(p-1,q);
              a[p-1,q]:=t;
            end;
          1:case a[p,q]of
              0,1:if c[p-1,q]>c[p,q] then
                begin
                  c[p-1,q]:=c[p,q];
                  gen(p-1,q);
                end;
              2:if c[p-1,q]>(c[p,q]+1) then
                begin
                  c[p-1,q]:=c[p,q]+1;
                  gen(p-1,q);
                end;
            end;
          2:case a[p,q]of
              0,2:if c[p-1,q]>c[p,q] then
                begin
                  c[p-1,q]:=c[p,q];
                  gen(p-1,q);
                end;
              1:if c[p-1,q]>(c[p,q]+1) then
                begin
                  c[p-1,q]:=c[p,q]+1;
                  gen(p-1,q);
                end;
            end;
        end;
    end;
    if (q>1)and not((a[p,q-1]=0)and (a[p,q]=0))  then
      begin
        case a[p,q-1] of
          0:if c[p,q-1]>(c[p,q]+2) then
            begin
              t:=a[p,q-1];
              a[p,q-1]:=a[p,q];
              c[p,q-1]:=c[p,q]+2;
              gen(p,q-1);
              a[p,q-1]:=t;
            end;
          1:case a[p,q]of
              0,1:if c[p,q-1]>c[p,q] then
                begin
                  c[p,q-1]:=c[p,q];
                  gen(p,q-1);
                end;
              2:if c[p,q-1]>(c[p,q]+1) then
                begin
                  c[p,q-1]:=c[p,q]+1;
                  gen(p,q-1);
                end;
            end;
          2:case a[p,q]of
              0,2:if c[p,q-1]>c[p,q] then
                begin
                  c[p,q-1]:=c[p,q];
                  gen(p,q-1);
                end;
              1:if c[p,q-1]>(c[p,q]+1) then
                begin
                  c[p,q-1]:=c[p,q]+1;
                  gen(p,q-1);
                end;
            end;
        end;
    end;
    if (p<m)and not((a[p+1,q]=0)and (a[p,q]=0))  then
      begin
        case a[p+1,q] of
          0:if c[p+1,q]>(c[p,q]+2) then
            begin
              c[p+1,q]:=c[p,q]+2;
              t:=a[p+1,q];
              a[p+1,q]:=a[p,q];
              gen(p+1,q);
              a[p+1,q]:=t;
            end;
          1:case a[p,q]of
              0,1:if c[p+1,q]>c[p,q] then
                begin
                  c[p+1,q]:=c[p,q];
                  gen(p+1,q);
                end;
              2:if c[p+1,q]>(c[p,q]+1) then
                begin
                  c[p+1,q]:=c[p,q]+1;
                  gen(p-1,q);
                end;
            end;
          2:case a[p,q]of
              0,2:if c[p+1,q]>c[p,q] then
                begin
                  c[p+1,q]:=c[p,q];
                  gen(p+1,q);
                end;
              1:if c[p+1,q]>(c[p,q]+1) then
                begin
                  c[p+1,q]:=c[p,q]+1;
                  gen(p+1,q);
                end;
            end;
        end;
    end;
    if (q<m)and not((a[p,q+1]=0)and (a[p,q]=0))  then
      begin
        case a[p,q+1] of
          0:if c[p,q+1]>(c[p,q]+2) then
            begin
              c[p,q+1]:=c[p,q]+2;
              t:=a[p,q+1];
              a[p,q+1]:=a[p,q];
              gen(p,q+1);
              a[p,q+1]:=t;
            end;
          1:case a[p,q]of
              0,1:if c[p,q+1]>c[p,q] then
                begin
                  c[p,q+1]:=c[p,q];
                  gen(p,q+1);
                end;
              2:if c[p,q+1]>(c[p,q]+1) then
                begin
                  c[p,q+1]:=c[p,q]+1;
                  gen(p,q+1);
                end;
            end;
          2:case a[p,q]of
              0,2:if c[p,q+1]>c[p,q] then
                begin
                  c[p,q+1]:=c[p,q];
                  gen(p,q+1);
                end;
              1:if c[p,q+1]>(c[p,q]+1) then
                begin
                  c[p,q+1]:=c[p,q]+1;
                  gen(p,q+1);
                end;
            end;
        end;
    end;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  read(m,n);
  for i:=1 to m do
    for j:=1 to m do
      begin
        c[i,j]:=maxlongint;
        a[i,j]:=0;
      end;
  c[1,1]:=0;
  for i:=1 to n do
    begin
      read(x,y,z);
      if z=0 then
        z:=2;
      a[x,y]:=z;
    end;
  gen(1,1);
  if c[m,m]=maxlongint then
    write(-1)
  else
    write(c[m,m]);
  close(input);
  close(output);
end.

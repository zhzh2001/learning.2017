const dx:array [1..4] of longint=(0,1,0,-1);
      dy:array [1..4] of longint=(-1,0,1,0);

var n,m,i,j,x,y,c,ans,sum:longint;
    chess:array[1..101,1..101] of longint;
    bo1:array[0..101,0..101] of boolean;

procedure work(x,y,z:longint);
var i:longint;
begin
  if (x=m) and (y=m) then begin if sum<ans then ans:=sum; exit; end;
  for i:=1 to 4 do
    if (bo1[x+dx[i],y+dy[i]]=true) and (x+dx[i]<=m) and (x+dx[i]>=1) and (y+dy[i]<=m) and (y+dy[i]>=1) then
    begin
      if chess[x+dx[i],y+dy[i]]=-1 then
        if z=1 then
        begin
          inc(sum,2);
          chess[x+dx[i],y+dy[i]]:=chess[x,y];
          bo1[x,y]:=false;
          work(x+dx[i],y+dy[i],0);
          dec(sum,2);
          chess[x+dx[i],y+dy[i]]:=-1;
          bo1[x,y]:=true;
        end
        else continue
      else
        if chess[x+dx[i],y+dy[i]]<>chess[x,y] then
        begin
          inc(sum,1);
          bo1[x,y]:=false;
          work(x+dx[i],y+dy[i],1);
          dec(sum,1);
          bo1[x,y]:=true;
        end
        else
        begin
          bo1[x,y]:=false;
          work(x+dx[i],y+dy[i],1);
          bo1[x,y]:=true;
        end;
    end;
end;

begin
  assign(input,'chess.in'); reset(input);
  assign(output,'chess.out'); rewrite(output);
  readln(m,n);
  ans:=maxlongint;fillchar(bo1,sizeof(bo1),true);
  for i:=1 to m do
    for j:=1 to m do
    chess[i,j]:=-1;
  for i:=1 to n do
  begin
    readln(x,y,c);
    chess[x,y]:=c;
  end;
  work(1,1,1);
  sum:=0;
  if ans=maxlongint then writeln('-1')
  else writeln(ans);
  close(input);
  close(output);
end.

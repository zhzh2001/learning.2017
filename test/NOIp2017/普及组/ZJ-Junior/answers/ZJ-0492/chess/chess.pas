const dx:array[1..2] of longint=(0,1);
      dy:array[1..2] of longint=(1,0);
var m,n,i,j,min:longint;
a,b:array[1..2000] of longint;
p:array[1..500,1..500] of longint;
f:array[1..500,1..500] of boolean;
procedure try(x,y:longint;f1:boolean;ans:longint);
var i:longint;
 begin
  if (ans>=min) then exit;
  if (x=m) and (y=m) then begin if ans<min then min:=ans;exit;  end;
  for i:=1 to 2 do
   begin
    if (x+dx[i]>0) and (x+dx[i]<=m) and (y+dy[i]>0) and (y+dy[i]<=m)
    then
     begin
      if (f[x+dx[i],y+dy[i]]=false) and (f1=true) then begin p[x+dx[i],y+dy[i]]:=p[x,y]; try(x+dx[i],y+dy[i],false,ans+2);p[x+dx[i],y+dy[i]]:=-1; end
      else
       begin
        if (f[x+dx[i],y+dy[i]]=true) and (p[x,y]=p[x+dx[i],y+dy[i]]) then try(x+dx[i],y+dy[i],true,ans)
        else if (f[x+dx[i],y+dy[i]]=true) and (p[x,y]<>p[x+dx[i],y+dy[i]]) then try(x+dx[i],y+dy[i],true,ans+1);
       end;
     end;
   end;
 end;
begin
   assign(input,'chess.in');assign(output,'chess.out');
   reset(input);rewrite(output);
   min:=maxlongint;
   fillchar(f,sizeof(f),false);
   readln(m,n);
   for i:=1 to m do
    for j:=1 to m do p[i,j]:=-1;
   for i:=1 to n do
    begin
     read(a[i],b[i],p[a[i],b[i]]);
     f[a[i],b[i]]:=true;
    end;
   try(1,1,true,0);
   if min<>maxlongint then writeln(min)
   else writeln(-1);
   close(input);close(output);
end.

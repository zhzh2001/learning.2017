var n,q,i,j:longint;
s1,s2:string;
f:boolean;
a,b,c:array[1..5000] of longint;
procedure qsort(l,r: longint);
var i,j,x,y: longint;
 begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
   while a[i]<x do
    inc(i);
   while x<a[j] do
    dec(j);
   if not(i>j) then
    begin
     y:=a[i];
     a[i]:=a[j];
     a[j]:=y;
     inc(i);
     dec(j);
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
begin
  assign(input,'librarian.in');assign(output,'librarian.out');
  reset(input);rewrite(output);
  readln(n,q);
  for i:=1 to n do readln(a[i]);
  for i:=1 to q do
    read(b[i],c[i]);
  qsort(1,n);
  for i:=1 to q do
   begin
    f:=false;
    for j:=1 to n do
     begin
      str(c[i],s1);
      str(a[j],s2);
      if length(s1)>length(s2) then continue;
      s2:=copy(s2,length(s2)-b[i]+1,b[i]);
      if s1=s2 then begin writeln(a[j]);f:=true;break; end
     end;
     if f=false then writeln(-1);
    end;
   close(input);close(output);
end.

var n,d,k,i,l,r,mid:longint;
s1,d1,d2:int64;
x,s:array[1..600000] of longint;
p:array[1..600000] of int64;
f:array[1..600000] of boolean;
function max(a,b:int64):int64;
 begin
  if a>b then exit(a)
  else exit(b);
 end;
function check(t:longint):boolean;
var i,j:longint;
max0:int64;
 begin
  for i:=1 to n do if (x[i]>=d1) and (x[i]<=d2) then begin f[i]:=true;p[i]:=s[i]; end
  else p[i]:=-maxlongint;
  max0:=0;
  for i:=1 to n do
   for j:=1 to i do
     if (x[i]-x[j]<=d2) and (x[i]-x[j]>=d1) and (f[j])
     then begin p[i]:=max(p[i],p[j]+s[i]);f[i]:=true;if p[i]>=k then break;end;
  for i:=1 to n do
   if p[i]>max0 then max0:=p[i];
  if max0>=k then check:=true
  else check:=false;
  for i:=1 to n do f[i]:=false;
 end;
begin
  assign(input,'jump.in');assign(output,'jump.out');
  reset(input);rewrite(output);
  s1:=0;
  readln(n,d,k);
  for i:=1 to n do
   begin
    read(x[i],s[i]);
    if s[i]>0 then inc(s1,s[i]);
    p[i]:=s[i];
   end;
  if s1<k then begin writeln(-1);close(input);close(output);halt; end;
  l:=0;
  r:=1000000000;
  while l<r do
   begin
    mid:=(l+r) div 2;
    if d>mid then d1:=d-mid else d1:=1;
    d2:=d+mid;
    if check(mid) then r:=mid
    else l:=mid+1;
   end;
  writeln(r);
  close(input);close(output);
end.

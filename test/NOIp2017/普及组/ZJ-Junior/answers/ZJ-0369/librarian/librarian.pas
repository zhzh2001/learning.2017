program ex01;
var
    n,q,qq,tt,temp:longint;
    a,b,c:array[1..1000]of longint;
    pan:boolean;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
    assign(input,'librarian.in');
    assign(output,'librarian.out');
    reset(input);
    rewrite(output);
    readln(n,q);
    for qq:=1 to n do
        readln(a[qq]);
    for qq:=1 to q do
        readln(c[qq],b[qq]);
    sort(1,n);
    for qq:=1 to q do
    begin
        pan:=true;
        temp:=1;
        for tt:=1 to c[qq] do
            temp:=temp*10;
        for tt:=1 to n do
            if a[tt]mod temp=b[qq] then
            begin
                writeln(a[tt]);
                pan:=false;
                break;
            end;
        if pan then writeln(-1);
    end;
    close(input);
    close(output);
end.

program ex01;
var
    m,n,x,y,c,i,j,p,q:longint;
    a,f:array[0..100,0..100]of longint;
    pan:array[0..100,0..100]of boolean;
begin
    assign(input,'chess.in');
    assign(output,'chess.out');
    reset(input);
    rewrite(output);
    readln(m,n);
    for i:=0 to m do
        for j:=0 to m do
        pan[i,j]:=false;
    for i:=0 to m do
        for j:=0 to m do
        a[i,j]:=-1;
    for i:=1 to n do
    begin
        readln(x,y,c);
        a[x,y]:=c;
    end;
    for i:=m downto 1 do
        for j:=m downto 1 do
        if (a[i-1,j]=-1)and(a[i,j-1]=-1)and(a[i-1,j-1]=-1)
        or(a[i-1,j]=-1)and(a[i,j-1]=-1)and(a[i,j]=-1)then f[i,j]:=-1;
    if f[m,m]=-1 then
    begin
        writeln(-1);
        halt;
    end;
    f[1,1]:=0;
    for i:=2 to m do
    begin
        if f[i,1]=-1 then break;
        if a[i,1]=-1 then
        begin
            f[i,1]:=f[i-1,1]+2;
            a[i,1]:=a[i-1,1];
            pan[i,1]:=true;
        end
        else
            f[i,1]:=f[i-1,1]+abs(a[i-1,1]-a[i,1]);
    end;
    for j:=2 to m do
    begin
        if f[1,j]=-1 then break;
        if a[1,j]=-1 then
        begin
            f[1,j]:=f[1,j-1]+2;
            a[1,j]:=a[1,j-1];
            pan[1,j]:=true;
        end
        else
            f[1,j]:=f[1,j-1]+abs(a[1,j-1]-a[1,j]);
    end;
    for i:=2 to m do
        for j:=2 to m do
        begin
            if f[i,j]=-1 then continue;
            if f[i-1,j]=-1 then q:=100000000;
            if f[i,j-1]=-1 then p:=100000000;
            if a[i,j]=-1 then
            begin
                if pan[i,j-1] then p:=100000000
                else if f[i,j-1]<>-1 then p:=f[i,j-1];
                if pan[i-1,j] then q:=100000000
                else if f[i-1,j]<>-1 then q:=f[i-1,j];
            end
            else
            begin
                if f[i,j-1]<>-1 then p:=f[i,j-1];
                if f[i-1,j]<>-1 then q:=f[i-1,j];
            end;
            if a[i,j]=-1 then
            begin
                p:=p+2;
                q:=q+2;
            end
            else
            begin
                p:=p+abs(a[i,j-1]-a[i,j]);
                q:=q+abs(a[i-1,j]-a[i,j]);
            end;
            if a[i,j]=-1 then
                if p>q then
                begin
                    f[i,j]:=q;
                    a[i,j]:=a[i-1,j];
                    pan[i,j]:=true;
                end
                else
                begin
                    f[i,j]:=p;
                    a[i,j]:=a[i,j-1];
                    pan[i,j]:=true;
                end;
            if a[i,j]<>-1 then
                if p>q then f[i,j]:=q else f[i,j]:=p;
        end;
    writeln(f[m,m]);
    close(input);
    close(output);
end.

program ex01;
var
    a,b,c,ans:longint;
begin
    assign(input,'score.in');
    assign(output,'score.out');
    reset(input);
    rewrite(output);
    readln(a,b,c);
    ans:=a div 5+(b div 10)*3+(c div 2);
    writeln(ans);
    close(input);
    close(output);
end.

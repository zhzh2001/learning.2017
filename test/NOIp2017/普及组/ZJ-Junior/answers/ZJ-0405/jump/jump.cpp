#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstring>
using namespace std;
int n, d, k, x[500001], s[500001], g, l, r, L, R, mid, p, ans, t;
bool flag = 0;
long long f[500001], sum = 0, maxn = 0;
int main(){
	freopen ( "jump.in", "r", stdin );
	freopen ( "jump.out", "w", stdout );
	memset ( x, 0, sizeof(x) );
	memset ( s, 0, sizeof(s) );
	scanf ( "%d %d %d", &n, &d, &k );
	for ( int i = 1; i <= n; i++ )
	scanf ( "%d %d", &x[i], &s[i] );
	l = 0, r = x[n] - d;
	while ( l <= r ){
		memset ( f, 0, sizeof(f) );
		mid = ( l + r ) >> 1, sum = maxn = 0, p = 0;
		if ( mid < d ) L = d - mid, R = d + mid;
		else L = 1, R = d + mid;
		for ( int i = 1; i <= n; i++ ){
			if ( x[i] - x[i-1] > R || x[i] - x[i-1] < L ) break;
			if ( sum + s[i] > 0 && x[i] - x[p] <= R && x[i] - x[p] >= L ) sum += s[i];	
			else p = i, sum += s[i];
			maxn = max ( maxn, max ( sum, f[i] ) );
		}
		if ( maxn >= k ) ans = mid, flag = 1, r = mid - 1;
		else l = mid + 1;
	}
	if ( !flag ) printf ( "-1\n" );
	else printf ( "%d\n", ans );
	fclose ( stdin );
	fclose ( stdout );
	return 0;
}

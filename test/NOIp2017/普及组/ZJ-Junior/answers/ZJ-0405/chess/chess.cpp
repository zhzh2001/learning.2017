#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstring>
using namespace std;
int m, n, x, y, c, ans = 0x7fffffff, f[101][101];
bool used[101][101];
void Dfs ( int a, int b, int t, int s, bool flag ){
	int p = s;
	if ( a < 1 || b < 1 || a > m || b > m || used[a][b] ) return ;
	if ( flag && f[a][b] == -1 ) return ;
	used[a][b] = 1;
	if ( a == m && b == m ){
		if ( f[a][b] != t ) p++;
		if ( f[a][b] == -1 ) p++;
		ans = min ( ans, p );
		return ;
	}
	if ( f[a][b] == -1 ){
		f[a][b] = f[a-1][b], Dfs ( a - 1, b, f[a][b], s + 2, 1 ), f[a][b] = -1;
		f[a][b] = f[a+1][b], Dfs ( a + 1, b, f[a][b], s + 2, 1 ), f[a][b] = -1;
		f[a][b] = f[a][b-1], Dfs ( a, b - 1, f[a][b], s + 2, 1 ), f[a][b] = -1;
		f[a][b] = f[a][b+1], Dfs ( a, b + 1, f[a][b], s + 2, 1 ), f[a][b] = -1;
	}
	else if ( f[a][b] != t ){
		Dfs ( a - 1, b, f[a][b], s + 1, 0 );
		Dfs ( a + 1, b, f[a][b], s + 1, 0 );
		Dfs ( a, b - 1, f[a][b], s + 1, 0 );
		Dfs ( a, b + 1, f[a][b], s + 1, 0 );
	}
	else{
		Dfs ( a - 1, b, f[a][b], s, 0 );
		Dfs ( a + 1, b, f[a][b], s, 0 );
		Dfs ( a, b - 1, f[a][b], s, 0 );
		Dfs ( a, b + 1, f[a][b], s, 0 );
	}
	used[a][b] = 0;
}
int main(){
	freopen ( "chess.in", "r", stdin );
	freopen ( "chess.out", "w", stdout );
	scanf ( "%d %d", &m, &n );
	memset ( f, -1, sizeof(f) );
	memset ( used, 0, sizeof(used) );
	for ( int i = 1; i <= n; i++ ){
		scanf ( "%d %d %d", &x, &y, &c );
		f[x][y] = c;
	}
	for ( int i = 1; i <= m; i++ )
	for ( int j = 1; j <= m; j++ )
	if ( f[i][j] == -1 && f[i-1][j] == -1 && f[i+1][j] == -1 && f[i][j-1] == -1 && f[i][j+1] == -1 ) used[i][j] = 1;
	if ( used[m][m] ){
		printf ( "-1\n" );
		return 0;
	}
	Dfs ( 1, 1, f[1][1], 0, 0 );
	if ( ans != 0x7fffffff ) printf ( "%d\n", ans );
	else printf ( "-1\n" );
	fclose ( stdin );
	fclose ( stdout );
	return 0;
}

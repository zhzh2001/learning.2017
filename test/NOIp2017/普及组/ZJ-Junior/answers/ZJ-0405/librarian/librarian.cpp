#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <string>
#include <cstring>
using namespace std;
int n, q, len, len2, x, v[1001];
bool flag;
int main(){
	freopen ( "librarian.in", "r", stdin );
	freopen ( "librarian.out", "w", stdout );
	scanf ( "%d %d", &n, &q );
	for ( int i = 1; i <= n; i++ )
	scanf ( "%d", &v[i] );
	sort ( v + 1, v + n + 1 );
	for ( int i = 1; i <= q; i++ ){
		cin >> len >> x;
		len2 = pow ( 10, ( len - 1 ) );
		flag = 0;
		for ( int j = 1; j <= n; j++ ){
			if ( v[j] >= len ){
				if ( v[j] % ( len2 * 10 ) == x ){
					printf ( "%d\n", v[j] );
					flag = 1;
					break;
				}
			}
		}
		if ( !flag ) printf ( "-1\n" );
	}
	fclose ( stdin );
	fclose ( stdout );
	return 0;
}

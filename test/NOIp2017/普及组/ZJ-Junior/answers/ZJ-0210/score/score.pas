var
  a,b,c,s:longint;
begin
  assign(input,'score.in');
  assign(output,'score.out');
  reset(input);
  rewrite(output);
  read(a,b,c);
  s:=(a*2+b*3+c*5) div 10;
  write(s);
  close(input);
  close(output);
end.
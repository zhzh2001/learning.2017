var
  m,n,i,j,x,y,c,s:longint;
  a:array[0..101,0..101] of -1..1;
  f:array[0..101,0..101] of boolean;
procedure dg(x,y,sum:longint;p:boolean);
var
  i:longint;
begin
  if (x=m)and(y=m) then
    begin
      if sum<s then s:=sum;
      exit;
    end;
  for i:=1 to 4 do
    case i of
      1:if f[x-1,y] and(x-1>0) then
          case a[x-1,y] of
            0,1:if a[x-1,y]=a[x,y] then
                  begin
                    f[x-1,y]:=false;
                    dg(x-1,y,sum,false);
                    f[x-1,y]:=true;
                  end
                    else
                  begin
                    f[x-1,y]:=false;
                    dg(x-1,y,sum+1,false);
                    f[x-1,y]:=true;
                  end;
            -1:if p then continue
                    else
                      begin
                        f[x-1,y]:=false;a[x-1,y]:=a[x,y];
                        dg(x-1,y,sum+2,true);
                        f[x-1,y]:=true;a[x-1,y]:=-1;
                      end;
          end;
      2:if f[x+1,y] and(x+1<=m) then
          case a[x+1,y] of
            0,1:if a[x+1,y]=a[x,y] then
                  begin
                    f[x+1,y]:=false;
                    dg(x+1,y,sum,false);
                    f[x+1,y]:=true;
                  end
                    else
                  begin
                    f[x+1,y]:=false;
                    dg(x+1,y,sum+1,false);
                    f[x+1,y]:=true;
                  end;
            -1:if p then continue
                    else
                      begin
                        f[x+1,y]:=false;a[x+1,y]:=a[x,y];
                        dg(x+1,y,sum+2,true);
                        f[x+1,y]:=true;a[x+1,y]:=-1;
                      end;
          end;
      3:if f[x,y-1] and(y-1>0) then
          case a[x,y-1] of
            0,1:if a[x,y-1]=a[x,y] then
                  begin
                    f[x,y-1]:=false;
                    dg(x,y-1,sum,false);
                    f[x,y-1]:=true;
                  end
                    else
                  begin
                    f[x,y-1]:=false;
                    dg(x,y-1,sum+1,false);
                    f[x,y-1]:=true;
                  end;
            -1:if p then continue
                    else
                      begin
                        f[x,y-1]:=false;a[x,y-1]:=a[x,y];
                        dg(x,y-1,sum+2,true);
                        f[x,y-1]:=true;a[x,y-1]:=-1;
                      end;
          end;
      4:if f[x,y+1] and(y+1<=m) then
          case a[x,y+1] of
            0,1:if a[x,y+1]=a[x,y] then
                  begin
                    f[x,y+1]:=false;
                    dg(x,y+1,sum,false);
                    f[x,y+1]:=true;
                  end
                    else
                  begin
                    f[x,y+1]:=false;
                    dg(x,y+1,sum+1,false);
                    f[x,y+1]:=true;
                  end;
            -1:if p then continue
                    else
                      begin
                        f[x,y+1]:=false;a[x,y+1]:=a[x,y];
                        dg(x,y+1,sum+2,true);
                        f[x,y+1]:=true;a[x,y+1]:=-1;
                      end;
          end;
    end;
end;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  readln(m,n);
  for i:=1 to m do
    for j:=1 to m do
      a[i,j]:=-1;
  for i:=1 to n do
    begin
      readln(x,y,c);
      a[x,y]:=c;
    end;
  s:=maxlongint;
  fillchar(f,sizeof(f),true);
  f[1,1]:=false;
  dg(1,1,0,false);
  if s=maxlongint then write(-1)
                  else write(s);
  close(input);
  close(output);
end.
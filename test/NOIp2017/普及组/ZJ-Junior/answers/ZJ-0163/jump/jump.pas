var n,d,k,t,w,i,j,mid,max,g:longint;
flag:boolean;
a,b,f:array[0..500000] of longint;
f1:array[0..500000] of boolean;
function work(m:longint):boolean;
begin
  max:=-maxlongint;
  fillchar(f1,sizeof(f1),false);
  f[0]:=0;f1[0]:=true;
  for i:=1 to n do
    begin
      f[i]:=-maxlongint;
      for j:=i-1 downto 0 do
        if (a[j]+d-m<=a[i]) and (a[j]+d+m>=a[i]) and (f1[j]) then
          begin
            if f[j]+b[i]>f[i] then
              begin
                f[i]:=f[j]+b[i];
                f1[i]:=true;
                if f[i]>max then max:=f[i];
                if max>=k then begin flag:=true;exit(true);end;
              end;
          end
        else
          if a[j]+d+m<a[i] then break;
    end;
  if max>=k then begin flag:=true;exit(true);end
  else exit(false);
end;
begin
  assign(input,'jump.in');assign(output,'jump.out');
  reset(input);rewrite(output);
  read(n,d,k);
  for i:=1 to n do read(a[i],b[i]);
  if n>=15000 then begin writeln(-1);close(input);close(output);halt;end;
  t:=0;w:=a[n];
  while t<w do
    begin
      mid:=(t+w) div 2;
      if work(mid) then w:=mid
      else t:=mid+1;
    end;
  if not flag then writeln(-1)
  else writeln(w);
  close(input);close(output);
end.

var n,m,i,j,min,x,len:longint;
a:array[1..10000] of string;
ch:char;
st:string;
begin
  assign(input,'librarian.in');assign(output,'librarian.out');
  reset(input);rewrite(output);
  readln(n,m);
  for i:=1 to n do readln(a[i]);
  for i:=1 to m do
    begin
      readln(len,ch,st);
      min:=maxlongint;
      for j:=1 to n do
        if copy(a[j],length(a[j])-len+1,len)=st then
          begin
            val(a[j],x);
            if x<min then min:=x;
          end;
      if min=maxlongint then writeln(-1)
      else writeln(min);
    end;
  close(input);close(output);
end.
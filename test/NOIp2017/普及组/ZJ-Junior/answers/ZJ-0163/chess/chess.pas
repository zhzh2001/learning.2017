const d:array[1..4,1..2] of longint=((0,1),(1,0),(-1,0),(0,-1));
var n,m,i,x,y,z,k:longint;
a,f:array[0..101,0..101] of longint;
function min(x,y:longint):longint;
begin
  if x>y then exit(y) else exit(x);
end;
function sc(x,y,z:longint):longint;
var i,w,cache,t:longint;
begin
  if f[x,y]>-1 then exit(f[x,y]);
  if (x=m) and (y=m) then exit(0);
  sc:=9999999;
  for i:=1 to 4 do
    if (x+d[i,1]<=m) and (x+d[i,1]>=1)
    and (y+d[i,2]<=m) and (y+d[i,2]>=1) then
      begin
        w:=a[x+d[i,1],y+d[i,2]];
        if w=-2 then continue;
        if w=-1 then
          if z>=2 then continue
          else
            begin
              cache:=a[x,y];a[x,y]:=-2;
              sc:=min(sc,sc(x+d[i,1],y+d[i,2],z+2)+2);
              a[x,y]:=cache;
              continue;
            end;
        t:=z;
        if t>=2 then t:=t-2;
        cache:=a[x,y];a[x,y]:=-2;
        sc:=min(sc,sc(x+d[i,1],y+d[i,2],w)+abs(t-w));
        a[x,y]:=cache;
      end;
  f[x,y]:=sc;
end;
begin
  assign(input,'chess.in');assign(output,'chess.out');
  reset(input);rewrite(output);
  read(m,n);
  fillchar(a,sizeof(a),255);
  fillchar(f,sizeof(f),255);
  for i:=1 to n do read(x,y,a[x,y]);
  k:=sc(1,1,a[1,1]);
  if k=9999999 then writeln(-1)
  else writeln(k);
  close(input);close(output);
end.

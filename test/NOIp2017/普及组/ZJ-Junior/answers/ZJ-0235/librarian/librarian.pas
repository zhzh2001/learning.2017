const
  t:array[1..8] of longint=(10,100,1000,10000,100000,1000000,10000000,100000000);
var
  n,q,i,len,x,tt,j:longint;
  bj:boolean;
  b:array[0..1005] of longint;
  procedure sort(l,r:longint);
  var
    i,j,x,y:longint;
  begin
    i:=l;
    j:=r;
    x:=b[random(r-l+1)+l];
    repeat
      while b[i]<x do inc(i);
      while x<b[j] do dec(j);
      if not(i>j) then
      begin
        y:=b[i];
        b[i]:=b[j];
        b[j]:=y;
        inc(i);
        dec(j);
      end;
    until i>j;
    if l<j then sort(l,j);
    if i<r then sort(i,r);
  end;
begin
  assign(input,'librarian.in');
  reset(input);
  assign(output,'librarian.out');
  rewrite(output);

  readln(n,q);
  for i:=1 to n do readln(b[i]);
  sort(1,n);
  for i:=1 to q do
  begin
    readln(len,x);
    tt:=t[len];
    bj:=false;
    for j:=1 to n do
    begin
      if (b[j] mod tt)<>x then continue;
      writeln(b[j]);
      bj:=true;
      break;
    end;
    if bj=false then writeln('-1');
  end;

  close(input);
  close(output);
end.
var
  n,d,k,i,tot,ans:longint;
  x,s:array[0..500005] of longint;
  function DFS(now,lst,min,max,sum:longint):boolean;
  var
    i,j,p,t,r:longint;
  begin
    if sum>=k then exit(true);
    i:=lst;
    while (i<=n) and (x[i]<now+min) do inc(i);
    if i>n then exit(false);
    j:=i;
    while (j<n) and (x[j]<now+max) do inc(j);
    t:=-maxlongint;
    for p:=i to j do
    if s[p]>t then begin t:=s[p]; r:=p; end;
    if DFS(x[r],r,min,max,sum+t) then exit(true)
                                 else exit(false);
  end;
  function check(a:longint):boolean;
  var
    min,max:longint;
  begin
    if d-a<1 then min:=1
             else min:=d-a;
    max:=d+a;
    if DFS(0,1,min,max,0) then exit(true)
                          else exit(false);
  end;
begin
  assign(input,'jump.in');
  reset(input);
  assign(output,'jump.out');
  rewrite(output);

  readln(n,d,k);
  for i:=1 to n do
  begin
    readln(x[i],s[i]);
    if s[i]>0 then tot:=tot+s[i];
  end;
  if tot<k then
  begin
    writeln('-1');
    close(input);
    close(output);
    halt;
  end;
  ans:=0;
  while true do
  begin
    inc(ans);
    if check(ans) then break;
  end;
  writeln(ans);

  close(input);
  close(output);
end.
const
  dx:array[1..4] of longint=(-1,0,1,0);
  dy:array[1..4] of longint=(0,-1,0,1);
  maxn=10001;
var
  m,n,i,x,y,c,inf,j,ans:longint;
  map,vis,dis:array[0..105,0..105] of longint;
  q:array[0..10005] of record
                         x,y,c,di:longint;
                         bj:boolean;
                       end;
  procedure spfa;
  var
    h,t,i,x0,y0,d:longint;
  begin
    h:=0;
    t:=1;
    q[1].x:=1;
    q[1].y:=1;
    q[1].c:=map[1][1];
    q[1].bj:=false;
    q[1].di:=0;
    //vis[1][1]:=1;
    dis[1][1]:=0;
    while h<>t do
    begin
      h:=(h+1) mod maxn;
      //vis[q[h].x][q[h].y]:=0;
      for i:=1 to 4 do
      begin
        x0:=q[h].x+dx[i];
        y0:=q[h].y+dy[i];
        if (x0>0) and (x0<=m) and (y0>0) and (y0<=m) then
        begin
          if map[x0][y0]<>-1 then
          begin
            if map[x0][y0]=q[h].c then d:=0
                                  else d:=1;
            if dis[x0][y0]>q[h].di+d then
            begin
              dis[x0][y0]:=q[h].di+d;
              //if vis[x0][y0]=0 then
              begin
                //vis[x0][y0]:=1;
                t:=(t+1) mod maxn;
                q[t].x:=x0;
                q[t].y:=y0;
                q[t].c:=map[x0][y0];
                q[t].bj:=false;
                q[t].di:=q[h].di+d;
              end;
              if (x0=m) and (y0=m) then
              begin
                if q[t].di<ans then ans:=q[t].di;
                dec(t);
                if t<0 then t:=maxn-1;
              end;
            end;
          end else if q[h].bj=false then
          begin
            d:=2;
            if dis[x0][y0]>q[h].di+d then
            begin
              dis[x0][y0]:=q[h].di+d;
              //if vis[x0][y0]=0 then
              begin
                vis[x0][y0]:=1;
                t:=(t+1) mod maxn;
                q[t].x:=x0;
                q[t].y:=y0;
                q[t].c:=q[h].c;
                q[t].bj:=true;
                q[t].di:=q[h].di+d;
              end;
              if (x0=m) and (y0=m) then
              begin
                if q[t].di<ans then ans:=q[t].di;
                dec(t);
                if t<0 then t:=maxn-1;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
begin
  assign(input,'chess.in');
  reset(input);
  assign(output,'chess.out');
  rewrite(output);

  readln(m,n);
  if m=1 then
  begin
    writeln('0');
    close(input);
    close(output);
    halt;
  end;
  fillchar(map,sizeof(map),255);
  fillchar(dis,sizeof(dis),63);
  ans:=maxlongint;
  for i:=1 to n do
  begin
    readln(x,y,c);
    map[x][y]:=c;
  end;
  spfa;
  {for i:=1 to m do
  begin
    for j:=1 to m do write(dis[i][j]:10,' ');
    writeln; writeln;
  end;}
  if ans=maxlongint then writeln('-1')
                    else writeln(ans);

  close(input);
  close(output);
end.
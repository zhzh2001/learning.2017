var n,q,i,j,xx,k,yy,a1,a2:longint;
    x,y,ans:array[0..1000] of string;
    a,l:array[1..1000] of longint;
    b,c:char;
    s,xy:string;
begin
  assign(input,'librarian.in');
  assign(output,'librarian.out');
  reset(input);
  rewrite(output);
  readln(n,q);
  for i:=1 to q do
    ans[i]:='9999999999';
  for i:=1 to n do
  begin
    readln(x[i]);
    l[i]:=length(x[i]);
  end;
  for i:=1 to q do
  begin
    readln(b,c,y[i]);
    a[i]:=ord(b)-ord(0);
  end;
  for i:=1 to q do
  begin
    xx:=0;
    xy:='';
    for j:=1 to n do
    begin
      s:=copy(x[j],pos(y[i],x[j]),l[i]-pos(y[i],x[j])+1);
      if s=y[i] then
      begin
        a1:=length(x[j]);
        a2:=length(ans[i]);
        if (a1<a2) or (a1=a2) and (x[j]<ans[i]) then
        begin
          x[xx]:=xy;
          ans[i]:=x[j];
          xx:=j;
          xy:=x[j];
          x[j]:='';
        end;
      end;
    end;
  end;
  for i:=1 to q do
    if ans[i]<'9999999999' then writeln(ans[i])
    else writeln(-1);
  close(input);
  close(output);
end.
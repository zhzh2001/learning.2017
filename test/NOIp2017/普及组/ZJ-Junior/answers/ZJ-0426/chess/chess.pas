var n,m,i,j,c:longint;
    map:array[1..100,1..100] of longint;
    a,b:array[1..500] of longint;
begin
  assign(input,'chess.in');
  assign(output,'chess.out');
  reset(input);
  rewrite(output);
  read(m,n);
  for i:=1 to n do
  begin
    readln(a[i],b[i],c);
    map[a[i],b[i]]:=c+1;
  end;
  for i:=1 to n do
    if (map[a[i],b[i]-2]=0) and (map[a[i]-2,b[i]]=0) and (map[a[i],b[i]-1]=0) and (map[a[i]-1,b[i]]=0) and (map[a[i]-1,b[i]-1]=0) and (a[i]<>1) and (b[i]<>1) then
    begin
      writeln(-1);
      close(input);
      close(output);
      halt;
    end;
  writeln(2*m);
  close(input);
  close(output);
end.
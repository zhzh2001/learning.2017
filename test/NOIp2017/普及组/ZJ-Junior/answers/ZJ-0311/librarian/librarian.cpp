#include<algorithm>
#include<iostream>
#include<fstream>
#include<vector>
#include<cstdio>
#include<cmath>
using namespace std;

int n,q;
int a[1010],b[1010];
vector<int> book[1010],want[1010];

void divde1(int p,int x){
	int num=x,i=0;
	while(num>0){
		book[p].push_back(num%10);
		num/=10;
	}
	return;
}
void divde2(int p,int x){
	int num=x,i=0;
	while(num>0){
		want[p].push_back(num%10);
		num/=10;
	}
	return;
}
void init(){
	scanf("%d%d",&n,&q);
	int x;
	for (int i=0; i<n; i++)
		scanf("%d",&a[i]);
	for (int i=0; i<q; i++){
		scanf("%d",&x);
		scanf("%d",&b[i]);
	
	}
	return;
}

int find(int x){
	for (int i=0; i<n; i++){
		bool flag=true;
		for (int j=0; j<want[x].size(); j++){
			if (book[i][j]!=want[x][j]){
				flag=false;
				break;
			}
		}
		if (flag)
			return a[i];
	}
	return -1;
}
void solve(){
	sort(a,a+n);
	for (int i=0; i<n; i++)
		divde1(i,a[i]);
	for (int i=0; i<q; i++)
		divde2(i,b[i]);
	for (int i=0; i<q; i++)
		printf("%d\n",find(i));
	return;
}

int main(){
	freopen("librarian.in","r",stdin);
	freopen("librarian.out","w",stdout);
	init();
	solve();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

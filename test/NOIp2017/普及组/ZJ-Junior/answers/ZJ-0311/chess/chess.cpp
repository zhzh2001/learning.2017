#include<iostream>
#include<fstream>
#include<cstdio>
#include<queue>
#include<cmath>
using namespace std;

const int INF=1000;
int m,n;
int map[110][110];
int dx[4]={0,1,0,-1},dy[4]={1,0,-1,0};
int d[110][110];

void init(){
	for (int i=0; i<110; i++){
		map[i][0]=INF;
		map[0][i]=INF;
	}
	for (int i=1; i<110; i++)
		for (int j=1; j<110; j++)
			map[i][j]=2;
	scanf("%d%d",&m,&n);
	int x,y,color;
	for (int i=0; i<n; i++){
		scanf("%d%d%d",&x,&y,&color);
		map[x][y]=color;
	}
	for (int i=1; i<=m; i++)
		for (int j=1; j<=m; j++){
			if (map[i][j]<2)
				continue;
			int temp=0;
			for (int p=0; p<4; p++){
				int x=i+dx[p],y=j+dy[p];
				if ((x>0)&&(x<=m)&&(y>0)&&(y<=m)&&(map[x][y]<2))
					temp++;
			}
			if (temp<=1)
				map[i][j]=INF;
		}
	return;
}

void solve(){
	for(int i=0; i<110; i++)
		for (int j=0; j<110; j++)
			d[i][j]=INF;
	d[1][1]=0;
	for (int i=1; i<=m; i++)
		for(int j=1; j<=m; j++){
			int x=0,y=0,temp=INF,cost=0;
			for (int p=0; p<4; p++){
				int xx=i+dx[p],yy=j+dy[p];
				if ((xx>0)&&(xx<=m)&&(yy>0)&&(yy<=m)&&(map[xx][yy]<INF))
					if (temp>d[xx][yy]){
						x=xx;
						y=yy;
						temp=d[xx][yy];
					}
			}
			if (map[i][j]==INF)
				continue;
			if (map[i][j]==2){
				cost=2;
				map[i][j]=map[x][y];
			}
			else
				if (map[x][y]!=map[i][j])
					cost=1;
			if (d[i][j]>d[x][y]+cost)
				d[i][j]=d[x][y]+cost;
		}
	if (d[m][m]==INF)
		printf("-1");
	else
		printf("%d",d[m][m]);
	return;
}

int main(){
	freopen("chess.in","r",stdin);
	freopen("chess.out","w",stdout);
	init();
	solve();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

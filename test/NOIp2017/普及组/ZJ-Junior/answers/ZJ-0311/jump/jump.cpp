#include<iostream>
#include<fstream>
#include<cstdio>
#include<cmath>
using namespace std;

int n,k,b;
int cost[500010],v[50010];
long long f[500010];
long long tot=0,sum=0;
int ans=1000000;

void init(){
	cost[0]=0;
	v[0]=0;
	scanf("%d%d%d",&n,&b,&k);
	for (int i=1; i<=n; i++){
		scanf("%d%d",&v[i],&cost[i]);
		if (cost[i]>0)
			tot+=cost[i];
	}
	return;
}

void solve(){
	int ans=0;
	for (ans=0; ans<10000; ans++){
		fill(f,f+50010,0);
		sum=0;
		for (int i=1; i<=n; i++){
			for (int j=0; j<i; j++){
				if ((abs(v[j]-v[i])>(b+i))&&(abs(v[j]-v[i])<max(0,b-i)))
					continue;
				f[i]=max(f[i],max(f[i-j],f[i-j]+cost[i]));
			}
			sum=max(sum,f[i]);
		}
		if (sum>=k)
			break;
	}
	printf("%d",ans);
	return;
}

int main(){
	freopen("jump.in","r",stdin);
	freopen("jump.out","w",stdout);
	init();
	if (tot<k){
		printf("-1\n");
		return 0;
	}
	solve();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

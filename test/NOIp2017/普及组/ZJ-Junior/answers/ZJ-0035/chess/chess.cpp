#include <fstream>
#include <vector>
#include <set>

struct path {
  typedef std::pair<int, int> point;
  typedef std::vector<point> t_path;
  typedef std::set<point> t_point_set;

  t_path _path;
  t_point_set _set;

  void add(point p)
  {
    _set.insert(p);
    _path.push_back(p);
  }
};

std::ifstream fin("chess.in");
std::ofstream fout("chess.out");

static int board[110][110] = {0};
std::set<path::t_path> paths;
static int m, n;

void search_path(path gone, const int x, const int y);

void moveup(const path& gone, int x, int y)
{
  if(y == 0) return;
  --y;
  search_path(gone, x, y);
}
void movedown(const path& gone, int x, int y)
{
  if(y == (m - 1)) return;
  ++y;
  search_path(gone, x, y);
}
void moveleft(const path& gone, int x, int y)
{
  if(x == 0) return;
  --x;
  search_path(gone, x, y);
}
void moveright(const path& gone, int x, int y)
{
  if(x == (m - 1)) return;
  ++x;
  search_path(gone, x, y);
}

void search_path(path gone, const int x = 0, const int y = 0)
{
  const std::pair<int, int> pair(x, y);
  if(gone._set.find(pair) != gone._set.end()) return;
  gone.add(pair);
  if(x == (m - 1) && y == (m - 1)) {
    paths.insert(gone._path);
    return;
  }
  moveup(gone, x, y);
  movedown(gone, x, y);
  moveleft(gone, x, y);
  moveright(gone, x, y);
}

int main()
{
  fin >> m >> n;
  for(int i = 0; i < n; ++i) {
    int x, y, c;
    fin >> x >> y >> c;
    --x;
    --y;
    ++c;
    board[x][y] = c;
  }
  search_path(path());
  int less = -1;
  int lc = 0;
  for(std::set<path::t_path>::const_iterator it = paths.begin(); it != paths.end(); ++it) {
    int gold = 0;
    for(path::t_path::const_iterator it_ = it->begin(); it_ != it->end(); ++it_) {
      const int x = it_->first;
      const int y = it_->second;
      const int c = board[x][y];
      if(c == 0 && lc == 0) goto n;
      if(c == 0) {
        gold += 2;
      } else if(c != lc) {
        ++gold;
        lc = c;
      } else {
        lc = c;
      }
    }
    if(less == -1 || gold < less) less = gold;
n:;
  }
  fout << less << std::endl;
}

#include <fstream>
#include <set>

std::ifstream fin("librarian.in");
std::ofstream fout("librarian.out");

struct book {
  int num;

  book(int n):num(n) {}
  book():num() {}

  static int bit2ten(int bit)
  {
  	int result = 1;
  	for(int i = 0; i < bit; ++i) result *= 10;
  	return result;
  }

  bool match(int bit, int now) const
  {
    const int a = num % bit2ten(bit);
    return a == now;
  }

  friend bool operator<(book l, book r)
  {
    return l.num < r.num;
  }

  friend bool operator==(book l, book r)
  {
    return false;
  }
};

static std::set<book> books;

int each(int l, int c) {
  for(std::set<book>::iterator it = books.begin(); it != books.end(); ++it) {
    if(it->match(l, c)) return it->num;
  }
  return -1;
}

int main()
{
  int n, q;
  fin >> n >> q;
  for(int i = 0; i < n; ++i) {
    int tmp;
    fin >> tmp;
    books.insert(tmp);
  }
  for(int i = 0; i < n; ++i) {
    int l, c;
    fin >> l >> c;
    fout << each(l, c) << std::endl;
  }
}

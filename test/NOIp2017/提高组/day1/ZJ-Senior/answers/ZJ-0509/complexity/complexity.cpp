#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
#include<string>
using namespace std;

char exp[1000], op[10], xx[10], ii[10], yy[10];
int n, b[1000], T;

struct Node{
	int x, y, r;
	char i;
}a[1000];

int Dfs(int l, int r, int p) {
	if(a[l].x > a[l].y) return p;
	if(a[l].y == 101 && a[l].x != 101) p++;
	int ans = p;
	for(int i = l + 1; i < r; i = a[i].r + 1) {
		ans = max(ans, Dfs(i, a[i].r, p));
	}
	return ans;
}

int main(void) {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	scanf("%d", &T);
	while(T--) {
		scanf("%d%s", &n, exp);
		int len = strlen(exp), w = -1;
		for(int i = 0; i < len; i++) {
			if(exp[i] == 'n') w = 0;
			if(exp[i] >= '0' && exp[i] <= '9') {
				w = w * 10 + exp[i] - '0';
			}
		}
		w = max(w, 0);
		
		vector<int> now;
		now.clear();
		bool flg = 0;
		memset(b, 0, sizeof(b));
		
		for(int i = 1; i <= n; i++) {
			scanf("%s", op);
			if(op[0] == 'F') {
				now.push_back(i);
				scanf("%s%s%s", ii, xx, yy);
				if(b[ii[0]]) {
					flg = 1;
					continue;
				}
				b[ii[0]] = 1;
				a[i].i = ii[0];
				if(xx[0] == 'n') a[i].x = 101;
				else {
					a[i].x = 0;
					int llx = strlen(xx);
					for(int j = 0; j < llx; j++)
						a[i].x = a[i].x * 10 + xx[j] - '0';
				}
				if(yy[0] == 'n') a[i].y = 101;
				else {
					a[i].y = 0;
					int lly = strlen(yy);
					for(int j = 0; j < lly; j++) 
						a[i].y = a[i].y * 10 + yy[j] - '0';
				}
			} else {
				if(now.size() == 0) {
					flg = 1;
					continue;
				}
				int pre = now[now.size() - 1];
				a[pre].r = i;
				b[a[pre].i] = 0;
				now.pop_back();
			}
		}
		
		if(now.size()) flg = 1;
		if(flg) {
			puts("ERR");
			continue;
		}

		int ans = 0;
		for(int i = 1; i <= n; i = a[i].r + 1)
			ans = max(ans, Dfs(i, a[i].r, 0));
		puts(ans == w ? "Yes" : "No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


















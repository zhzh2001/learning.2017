#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int x, y;

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%d%d", &x, &y);
	long long ans = 1LL * x * y - x - y;
	printf("%lld\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

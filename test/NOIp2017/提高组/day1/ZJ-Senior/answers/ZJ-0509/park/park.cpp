#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<vector>
using namespace std;

const int M = 200100, N = 100100;

int tot, to[M], c[M], fi[N], ne[M], la[N], f[N], b[N], p[N][51], du[N];
int a[N], cnt, T, n, m, MOD, K, x, y, z, vis[N][51];

 

vector<int> g[N];

inline void Add(int x, int y, int z) {
	to[++tot] = y; c[tot] = z;
	!fi[x] ? fi[x] = tot : ne[la[x]] = tot; la[x] = tot; 
}

inline void U(int &x) {
	if(x > MOD) x -= MOD;
}

void Spfa(void) {
	memset(b, 0, sizeof(b));
	memset(f, 0x3f, sizeof(f));
	memset(p, 0, sizeof(p));
	f[1] = 0;
	b[1] = 1;
	p[1][0] = 1 % MOD;
	queue<int> q;
	while(!q.empty()) q.pop();
	q.push(1);
	
	while(!q.empty()) {
		int x = q.front(); q.pop();
		for(int i = fi[x]; i; i = ne[i]) {
			int v = to[i];
			if(f[v] > f[x] + c[i]) {
				f[v] = f[x] + c[i];
				p[v][0] = p[x][0];
				if(!b[v]) {
					b[v] = 1;
					q.push(v);
				}
			} 
			else if(f[v] == f[x] + c[i]) {
				U(p[v][0] += p[x][0]); 
			}
				
		}
		b[x] = 0;
	}
}

void Tuopu(void) {
	queue<int> q;
	while(!q.empty()) q.pop();
	for(int i = 1; i <= n; i++) {
		if(!du[i]) q.push(i);
	} 
	cnt = 0;
	while(!q.empty()) {
		int x = q.front(); q.pop();
		a[++cnt] = x;
		for(int i = 0; i < g[x].size(); i++) {
			int v = g[x][i];
			du[v]--;
			if(!du[v]) q.push(v);
		}
	}
}

void Solve(void) {
	queue<pair<int, int> > q;
	while(!q.empty()) q.pop();
	memset(vis, 0, sizeof(vis));
	for(int i = 1; i <= n; i++) {
		vis[i][0] = 1;
		q.push(make_pair(i, 0));		
	}
	while(!q.empty()) {
		pair<int, int> now = q.front(); q.pop();
		int x = now.first, y = now.second;
//		printf("%d %d %d\n", x, y, p[x][y]);
		for(int i = fi[x]; i; i = ne[i]) {
			int v = to[i];
			int res = f[x] + y + c[i] - f[v];
			if(!res) continue;
			if(res <= K) {
				U(p[v][res] += p[x][y]);
				if(!vis[v][res]) {
					vis[v][res] = 1;
					q.push(make_pair(v, res));
				}
			}
		}
		vis[x][y] = 0;
	}
}

void Read(int &x) {
	x = 0;
	char ch = getchar();
	while(ch < '0' || ch > '9') ch = getchar();
	while(ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
}

int main(void) {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &T);
	while(T--) {
		scanf("%d%d%d%d", &n, &m, &K, &MOD);
		for(int i = 1; i <= n; i++) {
			g[i].clear();
			du[i] = 0;
		}
		memset(fi, 0, sizeof(fi));
		memset(ne, 0, sizeof(ne));
		tot = 0;
		for(int i = 1; i <= m; i++) {
			Read(x); Read(y); Read(z);	
			Add(x, y, z);
			if(!z) {
				g[x].push_back(y);
				du[y]++;
			}
		}
		Tuopu();
		if(cnt < n) {
			puts("-1");
			continue;
		}
		
		Spfa();
//		for(int i = 1; i <= n; i++)
//			printf("%d : f = %d, p0 = %d\n", i, f[i], p[i][0]);
		if(K) Solve();
		int ans = 0;
		for(int i = 0; i <= K; i++)
			U(ans += p[n][i]);
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

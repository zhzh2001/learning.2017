#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
#define N 105
char st[N],fh[N],stkch[N],sx[N],sy[N],type[N];
int flag[10005],stkf[N],top;
int n;
inline int check()
{
	int x=0,y=0;
	int lx=strlen(sx),ly=strlen(sy);
	for (int i=0;i<lx;++i) x=x*10+sx[i]-'0';
	for (int i=0;i<ly;++i) y=y*10+sy[i]-'0';
	if (x>y) return 1;
	return 0;
}
void solve()
{
	int ans=0,t=0,ret=0,yzh=0;
	top=0;
	for (char ch='a';ch<='z';++ch) flag[ch]=0;
	scanf("%d%s",&n,st);
	int ifn=0,len=strlen(st),ifans=0;
	for (int i=0;i<len;++i) if (st[i]=='n') ifn=1;
	if (ifn) for (int i=0;i<len;++i) if ('0'<=st[i] && st[i]<='9') ifans=ifans*10+st[i]-'0';
	for (int i=1;i<=n;++i)
	{
		scanf("%s",type);
		if (type[0]=='F')
		{
			scanf("%s%s%s",fh,&sx,&sy);
			if (flag[fh[0]]) ret=1;
			stkch[++top]=fh[0];
			flag[fh[0]]=1;
			if (sx[0]=='n' && sy[0]=='n') stkf[top]=0;else
			if (sy[0]=='n') {stkf[top]=1;++t;if (yzh>=0) ans=max(ans,t);}else
			if (sx[0]=='n') stkf[top]=-1;else
			if (check()) stkf[top]=-1;else
			stkf[top]=0;
			if (stkf[top]==-1) --yzh;
			continue;
		}
		if (!top) ret=1;
		if (stkf[top]==1) --t;
		if (stkf[top]==-1) ++yzh;
		flag[stkch[top]]=0;
		--top;
	}
	if (top) ret=1;
	if (ret) {puts("ERR");return;}
	if (ans==ifans) puts("Yes");else puts("No");
	//cout<<ans<<endl;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	cin>>T;
	while (T--) solve();
	return 0;
}

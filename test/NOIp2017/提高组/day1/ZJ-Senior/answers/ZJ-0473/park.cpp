#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
#define inf 1000000000
#define N 200005
int num,to[N],Next[N],head[N],len[N];
int dis[N],tot[N][66],mindis;
int n,m,K,p,ans[66];
inline void add(int x,int y,int t)
{
	++num;
	to[num]=y;
	Next[num]=head[x];
	head[x]=num;
	len[num]=t;
}
inline int update()
{
	bool flag=0;
	for (int st=1;st<=n;++st)
	{
	for (int i=head[st];i;i=Next[i])
	{
	int t=dis[st]+len[i];
	for (int j=0;j<=K;++j,++t)
	if (tot[st][j])
	{
		if (t>dis[to[i]]+K) break;
		flag=1;
		if (t>=dis[to[i]])
		{
			tot[to[i]][t-dis[to[i]]]=(tot[to[i]][t-dis[to[i]]]+tot[st][j])%p;
			continue;
		}
		for (int k=K;k>=0;--k)
		if (k-(dis[to[i]]-t)>=0)
		{
			tot[to[i]][k]=tot[to[i]][k-(dis[to[i]]-t)];
			if (to[i]==n) ans[k]=ans[k-(dis[to[i]]-t)];
		}else {tot[to[i]][k]=0;if (to[i]==n) ans[k]=0;}
		dis[to[i]]=t;
		tot[to[i]][0]=tot[st][j];
	}
	}
	for (int i=0;i<=K;++i) {if (st==n) ans[i]=(ans[i]+tot[st][i])%p;tot[st][i]=0;}
	}
	//for (int j=0;j<=K;++j) cout<<ans[j]<<' ';cout<<endl;for (int j=1;j<=n;++j) cout<<dis[j]<<' ';cout<<endl;for (int k=0;k<=K;++k){cout<<"k="<<k<<endl;for (int j=1;j<=n;++j) cout<<tot[j][k]<<' ';cout<<endl;}system("pause");
	return flag;
}
inline void bellman()
{
	for (int i=1;i<=n*20;++i)
	{
		int t=update();
		if (!t) return;
	}
}
int q[N],inq[N];
void spfa()
{
	int st=0,ed=1;
	q[1]=1;
	while (st<ed)
	{
		int x=q[(++st)%N];
		for (int i=head[x];i;i=Next[i])
		if (dis[x]+len[i]==dis[to[i]])
		{
			tot[to[i]][0]=(tot[to[i]][0]+tot[x][0])%p;
		}else
		if (dis[x]+len[i]<dis[to[i]])
		{
			dis[to[i]]=dis[x]+len[i];
			tot[to[i]][0]=tot[x][0];
			if (!inq[to[i]]) inq[to[i]]=1,q[(++ed)%N]=to[i];
		}
		inq[x]=0;
	}
}
void solve()
{
	int x,y,t;
	scanf("%d%d%d%d",&n,&m,&K,&p);
	for (int i=1;i<=n;++i) {dis[i]=inf;for (int j=0;j<=K;++j) tot[i][j]=0;}
	num=0;
	for (int i=1;i<=n;++i) head[i]=0;
	for (int i=1;i<=m;++i)
	{
		scanf("%d%d%d",&x,&y,&t);
		add(x,y,t);
	}
	dis[1]=0;tot[1][0]=1;
	if (K==0)
	{
		spfa();
		cout<<tot[n][0]<<endl;
		return;
	}
	bellman();
	mindis=dis[n];
	if (update()) {puts("-1");return;}
	int res=0;
	for (int i=0;i<=K;++i) res=(res+ans[i])%p;
	cout<<res<<endl;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	cin>>T;
	while (T--) solve();
	return 0;
}

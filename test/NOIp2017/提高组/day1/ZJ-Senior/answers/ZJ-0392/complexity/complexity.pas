var
 vis:array [0..200]of boolean;
 s:string;
 t,tt,i,an1,an2,j,p,q,top,ans,an,xn,yn,l,len,maxn,k,ln,ann,annn,x,y:longint;
 stack,stack1,stack2,stack3,stack4:array[0..200]of longint;
 tn1,ty,tn2,err,no:boolean;
begin
 assign(input,'complexity.in'); reset(input);
 assign(output,'complexity.out'); rewrite(output);
 readln(t);
 for tt:=1 to t do
 begin
  top:=0;
  xn:=0;
  maxn:=0;
  tn1:=false;
  tn2:=false;
  ty:=false;
  no:=false;
  err:=false;
  annn:=0;
  fillchar(vis,sizeof(vis),false);
  read(l);
  readln(s);
  for p:=1 to length(s) do
  begin
   if(s[p]<>'(')then continue
   else break;
  end;
  if(s[p+1]='n')then
  begin
   tn1:=true;
   an1:=100;
   q:=p+3;
  end
  else begin
   an1:=0;
   for i:=p+1 to length(s) do
   begin
    if((s[i]='^')or(s[i]=')'))then break;
    an1:=(an1*10)+ord(s[i])-48;
   end;
   if(s[i]=')')then tn2:=true;
   q:=i+1;
  end;
  if(tn2)then an2:=1
  else begin
   an2:=0;
   for i:=q to length(s) do
   begin
    if(s[i]=')')then break;
    an2:=(an2*10)+ord(s[i])-48;
   end;
  end;
  ans:=0;
  an:=1;
  xn:=0;
  maxn:=0;
  ty:=false;
  for ln:=1 to l do
  begin
   readln(s);
   if(s[1]='E')then begin
    if(top<0)then begin
     err:=true;
     continue;
    end;
    vis[stack4[top]]:=false;
    if(tn1=true)then xn:=xn-stack3[top]
    else ann:=ann-stack[top];
    dec(top);
    continue;
   end;
   inc(top);
   if(vis[ord(s[3])])then
   begin
    err:=true;
   end;
   vis[ord(s[3])]:=true;
   stack4[top]:=ord(s[3]);
   if(s[5]='n')then x:=100
   else begin
    x:=0;
    for i:=5 to length(s) do
    begin
     if(s[i]=' ')then break;
     x:=(x*10)+ord(s[i])-48;
    end;
   end;
   k:=i+1;
   if(s[k]='n')then y:=100
   else begin
    y:=0;
    for i:=k to length(s) do
    begin
     y:=(y*10)+ord(s[i])-48;
    end;
   end;
   if(x=100)then ann:=-1
   else if(y=100) then ann:=100
   else ann:=y-x+1;
   if(ann=100)then ty:=true;
   if((tn1=false)and(ann=100))then
   begin

    if(annn=0)then no:=true;
   end;

   if(tn1=true)then begin
    if(ann=100)then stack3[top]:=1
    else stack3[top]:=0;
    xn:=stack3[top]+xn;
    if(xn>maxn)then maxn:=xn;
   end
   else if(ann=-1)then begin stack[top]:=1; inc(annn); end
   else stack[top]:=0;


  end;
  if((err)or(top>0))then
  begin
   writeln('ERR');
   continue;
  end;
  if(tn1=true)then begin
   if(an2=maxn)then writeln('YES')
   else writeln('NO');
   continue;
  end;

  if(no=true)then writeln('NO')
   else writeln('YES');



 end;
 close(input);
 close(output);
end.

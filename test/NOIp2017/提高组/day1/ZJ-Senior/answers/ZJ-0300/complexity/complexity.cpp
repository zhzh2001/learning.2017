#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int T,x,y,n,m,p,l,O,dep,now,ans,W,d,K;
int a[1000],b[1000],g[100];
char s[1000];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	scanf("\n");
	while (T--)
	{
		gets(s+1);
		l=strlen(s+1);
		p=0;O=0;
		for (int i=1;i<=l;i++)
		{
			if (s[i]>='0'&&s[i]<='9')
			{
				x=0;
				while (s[i]>='0'&&s[i]<='9')
				{
					x=x*10+((int)(s[i])-(int)('0'));
					i++;
				}
				if (p==0) n=x;else m=x;
				p=1;
			}
			if (s[i]=='n') O=1;
		}
		if (O==0) m=0;
		dep=0;now=0;ans=0;W=0;K=0;
		for (int i=1;i<=26;i++) g[i]=0;
		for (int j=1;j<=n;j++)
		{
			gets(s+1);
			if (W==1) continue;
			l=strlen(s+1);
			if (s[1]=='E')
			{
				if (dep==0) W=1;
				else
				{
					if (a[dep]==0) g[b[dep]]=0,dep--;
					else if (K==1&&a[dep]==2) K=0,g[b[dep]]=0,dep--;
					else g[b[dep]]=0,dep--,now--;
				}
			}
			else
			{
				d=0;dep++;
				for (int i=2;i<=l;i++)
				  if (s[i]!=' ')
				  {
				  	 if (d==0)
				  	 {
				  	 	if (g[(int)(s[i])-(int)('a')+1]!=0)
				  	 	 {
						 	W=1;
						 	break;
						 }
						g[(int)(s[i])-(int)('a')+1]=1;
						b[dep]=(int)(s[i])-(int)('a')+1;
						d++;
					 }else if (d==1)
					 {
					 	if (s[i]=='n') a[dep]=2,K=1;
					 	x=0;
					 	while (s[i]>='0'&&s[i]<='9')
					 	{
					 	  x=x*10+(int)(s[i])-(int)('0');	
					 	  i++;
					 	}
					 	d++;  
					 }else if (d==2)
					 {
					 	if (s[i]=='n')
					 	{
					 		if (K!=1) a[dep]=1,now++;
					 		ans=max(ans,now);
						}
						else
						{
							y=0;
							while (s[i]>='0'&&s[i]<='9')
							{
							  y=y*10+(int)(s[i])-(int)('0');
					 	      i++;
					 	 	}
					 	 	if (x>y) a[dep]=2,K=1;
						}
					 }
				  }
			}
		}
		if (W==1||dep!=0) {printf("ERR\n");continue;}
		if (ans==m) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

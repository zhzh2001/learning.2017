#include<cstdio>
#include<iostream>
using namespace std;
int T,n,m,k,P,t,x,y,z;
int tot,head[200000],Next[500000],to[500000],len[500000];
int dis[200000],way[200000];
int sz,p[200000],stk[200000];
void add(int x1,int x2,int x3)
{
	tot++;
	Next[tot]=head[x1];
	head[x1]=tot;
	to[tot]=x2;
	len[tot]=x3;
}
void ajust(int x1)
{
	while (x1!=1)
	{
		if (dis[stk[x1]]<dis[stk[x1/2]])
		{
			t=stk[x1];stk[x1]=stk[x1/2];stk[x1/2]=t;
			t=p[stk[x1]];p[stk[x1]]=p[stk[x1/2]];p[stk[x1/2]]=t;
			x1/=2;
		}else break;
	}
}
void down()
{
	int i=1,j;
	while (i*2<=sz)
	{
		j=i;
		if (dis[stk[i]]>dis[stk[i*2]]) j=i*2;
		if (i*2+1<=sz)
			if (dis[stk[j]]>dis[stk[i*2+1]]) j=i*2+1;
		if (i==j) break;
		t=stk[i];stk[i]=stk[j];stk[j]=t;
		t=p[stk[i]];p[stk[i]]=p[stk[j]];p[stk[j]]=t;
		i=j;	
	}
}
void push()
{
	x=stk[1];
	stk[1]=stk[sz];
	sz--;
	down();
}
void spfa()
{
	for (int i=1;i<=n;i++) dis[i]=999999999,way[i]=0;
	dis[1]=0;way[1]=1;
	sz=1;stk[sz]=1;p[1]=1;
	while (sz!=0)
	{
		push();p[x]=0;
		for (int i=head[x];i!=-1;i=Next[i])
		{
			if (dis[x]+len[i]==dis[to[i]]) 
				{
					way[to[i]]+=way[x];
				}
			if (dis[x]+len[i]<dis[to[i]])
			{
				way[to[i]]=way[x];
				dis[to[i]]=dis[x]+len[i];
				if (p[x]==0)
				{
					sz++;
					p[to[i]]=sz;
					stk[sz]=to[i];
					ajust(sz);
				}else
				{
					ajust(p[to[i]]);
				}
			}
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&P);
		for (int i=1;i<=n;i++) head[i]=-1;
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		spfa();
		printf("%d\n",way[n]%P);
	}
	return 0;
}

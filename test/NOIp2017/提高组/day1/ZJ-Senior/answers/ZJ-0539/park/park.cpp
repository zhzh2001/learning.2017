#include <iostream>
#include <cstdio>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;
typedef long long LL;
const int MAXN=100010, MAXM=200020, MAXK=55;
struct edge {
	int x,y,w,nxt;
} E[MAXM],NE[MAXM];
int head[MAXN],nhead[MAXN];
int T,n,m,k,tot,ntot;
LL p,ans,mndis;

inline void read(int& x) {
	x=0;int f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}x*=f;
}

inline void read(LL& x) {
	x=0;LL f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}x*=f;
}

inline void addedge(int x,int y,int w) {
	E[++tot].x=x;
	E[tot].y=y;
	E[tot].w=w;
	E[tot].nxt=head[x];
	head[x]=tot;
}

inline void addnedge(int x,int y,int w) {
	NE[++ntot].x=x;
	NE[ntot].y=y;
	NE[ntot].w=w;
	NE[ntot].nxt=nhead[x];
	nhead[x]=ntot;
}

int dfsclk=0,bcccnt=0,top;
int dfn[MAXN],low[MAXN],blk[MAXN],stk[MAXN];
inline void tarjan(int x) {
	dfn[x]=low[x]=++dfsclk;
	stk[++top]=x;
	for(int i=head[x];~i;i=E[i].nxt) {
		int to=E[i].y;
		if(!dfn[to]) {
			tarjan(to);
			low[x]=min(low[x],low[to]);
		} else if(!blk[to]) {
			low[x]=min(low[x],dfn[to]);
		}
	}
	if(dfn[x]==low[x]) {
		++bcccnt;
		blk[x]=bcccnt;
		while(stk[top]!=x) {
			blk[stk[top]]=bcccnt;
			top--;
		}
		top--;
	}
}

int f[MAXN][MAXK];
bool vis[MAXN];

//inline void get(int x,int out) {
//	LL based=dijkstra(out);
//}

inline void dfs(int x) {
	if(vis[x]) return;
	for(int i=nhead[x];~i;i=NE[i].nxt) {
		int to=NE[i].y;
//		get(to);
		for(int j=k-NE[i].w;j>=0;j--) {
			f[x][j+NE[i].w]=(f[x][j+NE[i].w]+f[to][j])%p;
		}
	}
}

inline void brute(int x,LL d) {
	if(x==n) {
		if(d<=mndis+k) ans++;
		if(ans>p) p-=ans;
		return;
	}
	for(int i=head[x];~i;i=E[i].nxt) {
		int to=E[i].y;
		if(d+E[i].w<=mndis+k) {
			brute(to,d+E[i].w);
		}
	}
}

struct node {
	int x;
	LL d;
	bool operator < (const node A) const {
		return d>A.d;
	}
};
priority_queue<node> Q;
LL dis[MAXN];
inline LL dijkstra(int s,int t) {
	while(!Q.empty()) Q.pop();
	memset(dis,0x3f,sizeof dis);
	memset(vis,0,sizeof vis);
	Q.push((node){s,0});
	dis[s]=0;
	while(!Q.empty()) {
		int now=Q.top().x;
		Q.pop();
		if(vis[now]) continue;
		vis[now]=1;
		for(int i=head[now];~i;i=E[i].nxt) {
			int to=E[i].y;
			if(dis[to]>dis[now]+E[i].w) {
				dis[to]=dis[now]+E[i].w;
				if(!vis[to]) {
					Q.push((node){to,dis[to]});
				}
			}
		}
	}
	return dis[t];
}

int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--) {
		memset(head,-1,sizeof head);
		tot=0;
		read(n);read(m);read(k);read(p);
		bool allzero=1;
		for(int i=1,j,kk,l;i<=m;i++) {
			read(j);read(kk);read(l);
			addedge(j,kk,l);
			if(l>0) allzero=0;
		}
		if(allzero) {
			puts("-1");
			continue;
		}
		mndis=dijkstra(1,n);
		if(n<=1000) {
			ans=0;
			brute(1,0);
			printf("%lld\n",ans);
		} else {
			memset(dfn,0,sizeof dfn);
			memset(low,0,sizeof low);
			memset(blk,0,sizeof blk);
			dfsclk=bcccnt=top=0;
			for(int i=1;i<=n;i++) {
				if(!dfn[i]) tarjan(i);
			}
			memset(nhead,-1,sizeof nhead);
			ntot=0;
			for(int i=1;i<=tot;i++) {
				if(blk[E[i].x]!=blk[E[i].y]) {
					addnedge(E[i].y,E[i].x,E[i].w);
				}
			}
			memset(f,-1,sizeof f);
//			dfs(blk[n]);
			printf("1\n");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

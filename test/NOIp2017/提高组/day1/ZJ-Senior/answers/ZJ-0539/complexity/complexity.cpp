#include <iostream>
#include <cstdio>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long LL;
const int MAXL=15, MAXN=210;
int T,n,gcom,top,nowc,ans;
bool comerr,exist[200],pro[MAXN];
int names[MAXN],stk[MAXN];
char given[MAXL],op[MAXL],pa1[MAXL],pa2[MAXL],pa3[MAXL];

inline int getComplexity(char* s) {
	int len=strlen(s),num=0,hn=0,nb=1;
	for(int i=0;i<len;i++) {
		if(s[i]=='n') {
			hn=1;
		} else if(isdigit(s[i])) {
			nb=0;
			num=num*10+(s[i]-'0');
		}
	}
	if(hn&&nb) return 1;
	return hn*num;
}

inline bool checkGrammar(char* s) {
	int name=s[0]-'a'+1;
	if(exist[name]) return 1;
	else return 0;
}

inline void newName(int pos,char* s) {
	int name=s[0]-'a'+1;
	exist[name]=1;
	names[pos]=name;
}

inline int getVal(char* s) {
	int len=strlen(s),res=0;
	for(int i=0;i<len;i++) {
		if(s[i]=='n') {
			return -1;
		} else if(isdigit(s[i])) {
			res=res*10+(s[i]-'0');
		}
	}
	return res;
}

inline int getForComp(char* s,char* t) {
	int p1=getVal(s), p2=getVal(t);
	if(p1!=-1&&p2==-1) return 1;
	else if(p1!=-1&&p2!=-1&&p1<=p2) return 0;
	else if(p1==-1&&p2==-1) return 0;
	else return -1;
}

int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--) {
		scanf("%d",&n);
		scanf("%s",given);
		gcom=getComplexity(given);
		memset(exist,0,sizeof exist);
		memset(pro,0,sizeof pro);
		top=nowc=ans=0;
		comerr=0;
		for(int i=1;i<=n;i++) {
			scanf("%s",op);
			if(op[0]=='F') {
				scanf("%s%s%s",pa1,pa2,pa3);
				comerr|=checkGrammar(pa1);
				if(comerr==1) continue;
				stk[++top]=getForComp(pa2,pa3);
				newName(top,pa1);
				pro[top]=(pro[top-1])||(stk[top]==-1);
				if(!pro[top]) {
					nowc+=stk[top];
					ans=max(ans,nowc);
				}
			} else {
				if(top==0) comerr=1;
				if(comerr) continue;
				if(!pro[top]) {
					nowc-=stk[top];
				}
				exist[names[top]]=0;
				top--;
			}
		}
		comerr|=(top>0);
		if(comerr) puts("ERR");
		else if(ans==gcom) puts("Yes");
		else puts("No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

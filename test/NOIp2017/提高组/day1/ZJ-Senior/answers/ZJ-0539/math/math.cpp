#include <iostream>
#include <cstdio>
#include <cmath>
#include <string>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long LL;
LL n,m,x,y,xlim,ylim,ans;

inline void exgcd(LL a,LL b,LL& x,LL& y) {
	if(!b) {
		x=1;
		y=0;
		return;
	}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}

int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	cin>>n>>m;
	exgcd(n,m,x,y);
	if(x<0) {
		xlim=abs(x)-1;
		ylim=abs(y-n)-1;
	} else {
		ylim=abs(y)-1;
		xlim=abs(x-m)-1;
	}
	ans=xlim*n+ylim*m+1;
	cout<<ans<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

type  adj=record
  too,wei,last:longint;
end;

var
  head,w,queue,inn,d:array[1..110000] of longint;
  a,b,c,n,m,i,j,k,nedge,up,down,t,p,l,kk,num:longint;
  edge:array[1..210000] of adj;
  flag:boolean;

procedure add(x,y,z:longint);
begin
  inc(nedge);
  edge[nedge].too:=y;
  edge[nedge].wei:=z;
  edge[nedge].last:=head[x];
  head[x]:=nedge;
end;

procedure spfa;
var
  i,j,k,a,b,c:longint;
begin
  d[1]:=0;
  for i:=2 to n do
  d[i]:=999999999;
  up:=2;
  queue[1]:=1;
  down:=1;
  inn[1]:=1;
  while (up<>down) do
  begin
    a:=queue[down];
    k:=head[a];
    while k>0 do
    begin
      b:=edge[k].too;
      if d[b]>=d[a]+edge[k].wei then
      begin
        d[b]:=d[a]+edge[k].wei;
        inc(inn[b]);
        if inn[b]>n then
        begin
          writeln(-1);
          flag:=false;
          exit;
        end;
        queue[up]:=b;
        up:=(up) mod 100000+1;
      end;
      k:=edge[k].last;
    end;
    down:=(down) mod 100000+1;
  end;
end;

procedure dfs(x:longint);
var
  k:longint;
begin
  if l>d[n]+kk then exit;
  if x=n then begin num:=(num+1) mod p; exit; end;
  k:=head[x];
  w[x]:=1;
  while (k>0) do
  begin
    if w[edge[k].too]=0 then
    begin
      l:=l+edge[k].wei;
      dfs(edge[k].too);
      l:=l-edge[k].wei;
    end;
    k:=edge[k].last;
  end;
  w[x]:=0;
end;

begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input);
  rewrite(output);
  read(t);
  for i:=1 to t do
  begin
    read(n,m,kk,p);
    for j:=1 to m do
    begin
      read(a,b,c);
      add(a,b,c);
    end;
    flag:=true;
    fillchar(w,sizeof(w),0);
    num:=0;
    spfa;
    dfs(1);
    if flag then writeln(num);
  end;
  close(input);
  close(output);
end.

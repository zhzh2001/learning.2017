var
  s,s1,s2:string;
  sx:array[1..1000] of string;
  n1,n2,n3,a,b,c,i,j,k,num,t:longint;
  f:array['a'..'z'] of boolean;



procedure solve;
var
  i,j,k,nf,ne,d,k1,k2,nn,max,deep:longint;
  g:array[1..1000] of longint;
  h:array[1..1000] of char;
  s1,s2,s3:string;
  flag:boolean;
  c:char;
begin
  fillchar(f,sizeof(f),false);
  fillchar(g,sizeof(g),0);
  deep:=0;
  nf:=0;
  ne:=0;
  nn:=0;
  max:=0;
  flag:=true;
  if pos('n',s)<=0 then d:=0 else begin k1:=pos('^',s); k2:=pos(')',s); s1:=copy(s,k1+1,k2-k1-1); val(s1,d); end;
  for i:=1 to num do
  begin
    if sx[i][1]='F' then
    begin
      nf:=nf+1;
      k:=pos(' ',sx[i]);
      delete(sx[i],1,k);
      deep:=deep+1;
      g[deep]:=0;
      c:=sx[i][1];
      h[deep]:=c;
      if f[c]=true then begin writeln('ERR'); exit; end else f[sx[i][1]]:=true;
      if flag then
      begin
        k:=pos(' ',sx[i]);
        delete(sx[i],1,k);
        k:=pos(' ',sx[i]);
        if (sx[i][1]='n') and (sx[i][k+1]<>'n') then begin flag:=false; end;
        if pos('n',sx[i])<=0 then begin val(copy(sx[i],1,k-1),k1); val(copy(sx[i],k+1,length(sx[i])-k),k2); if k1>k2 then flag:=false; end;
        if (sx[i][1]<>'n') and (sx[i][k+1]='n') then begin nn:=nn+1; g[deep]:=1; end;
      end;
    end;
    if sx[i][1]='E' then
    begin
      inc(ne);
      if ne>nf then begin writeln('ERR'); exit; end;
      if max<nn then max:=nn;
      if g[deep]=1 then dec(nn);
      f[h[deep]]:=false;
      dec(deep);
    end;
  end;
  if nf<>ne then begin  writeln('ERR'); exit; end;
  if max=d then writeln('Yes') else writeln('no');
end;

procedure dsolve;
begin
    readln(s);
    //if (s[1]<>'E') and (s[i]<>'F') then begin solve; num:=0; s1:=s; end; else begin inc(num); sx[num]:=s; end;
    k:=pos('O',s);
    s2:=copy(s,1,k-2);
    delete(s,1,k);
    val(s2,k);
    num:=k;
    for j:=1 to k do
      readln(sx[j]);
    solve;
end;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i:=1 to t do
    dsolve;
  close(input);
  close(output);
end.
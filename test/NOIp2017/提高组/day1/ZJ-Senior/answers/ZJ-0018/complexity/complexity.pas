const oo=maxlongint;
var i,j,k,n,m,x,y,s,Times,line,trueans,ans,top:longint;
    ch,kong,huiche:char;
    ef,id,gid:array[0..107]of char;
    g,e,gf:array[0..107]of longint;
    vis:array['a'..'z']of boolean;
function max(a,b:longint):longint;
begin
  if a>b then exit(a); exit(b);
end;
function dfs(y:longint):longint;
var anst,yy,ans:longint; p:boolean;
begin
  ans:=y;
  anst:=0; p:=false;
  while ef[line]='F' do
    begin
    yy:=y; p:=true;
    if g[line]=2 then if yy<>-1 then inc(yy);
    if g[line]=0 then begin line:=e[line]; end;
    inc(line); if line>n then break;
    anst:=max(anst,dfs(yy));
    end;
  if not p then exit(y);
  exit(anst);
end;
function check:boolean;
var i,num:longint;  p:boolean;
begin
  num:=0;
  for i:=1 to n do
   if ef[i]='F' then inc(num) else dec(num);
  if num<>0 then exit(true);
  fillchar(vis,sizeof(vis),false);
  top:=0;
  for i:=1 to n do
   begin
   if ef[i]='E' then begin vis[gid[top]]:=false; dec(top); continue end;
   if vis[id[i]] then exit(true);
   vis[id[i]]:=true;
   inc(top); gid[top]:=id[i];
   end;
  exit(false);
end;
begin
  assign(input,'complexity.in');
  reset(input);
  assign(output,'complexity.out');
  rewrite(output);

  readln(Times);

  for k:=1 to Times do
   begin
   read(n); read(kong);
   read(ch); read(ch);
   read(ch);
  // writeln('biaoji1');
   if ch='1'
      then begin
           trueans:=0;
           readln(ch);
           end
      else begin
           trueans:=0;
           read(kong);
           while true do
             begin
             read(ch);
             if ('0'<=ch)and(ch<='9')
                then trueans:=trueans*10+ord(ch)-48
                else break;
             end;
           readln;
           end;
  // writeln('biaoji2');
   //0:O(1); shuzi:O(n^trueans);
   for i:=1 to n do ef[i]:='A';
   for i:=1 to n do
     begin
     while (ef[i]<>'E')and(ef[i]<>'F') do read(ef[i]);
     if ef[i]='E' then begin read(huiche); continue; end;
     read(kong); read(id[i]);
     read(kong); read(ch); x:=0; // writeln(i,' ch=',ch);
     while (ch>='0')and(ch<='9') do
       begin
       x:=x*10+ord(ch)-48;
       read(ch);
       end;
     if ch='n' then begin read(kong); x:=oo; end;
     read(ch); y:=0;    //writeln(i,' ch=',ch);
     while (ch>='0')and(ch<='9') do
       begin
       y:=y*10+ord(ch)-48;
       read(ch);
       end;
     if ch='n' then begin read(huiche); y:=oo; end;
    // writeln(ch);
    // readln;
     if x>y then g[i]:=0;
     if (x<>oo)and(y<>oo)and(x<=y) then g[i]:=1;
     if (x=oo)and(y=oo) then g[i]:=1;
     if (x<>oo)and(y=oo) then g[i]:=2;
     end;

  // for i:=1 to n do write(ef[i],' '); writeln;
 //  writeln('biaoji3');
   if check then begin writeln('ERR'); continue; end;
 //  writeln('biaoji4');
   top:=0;
   for i:=1 to n do
    begin
    if ef[i]='F' then begin inc(top); gf[top]:=i; end;
    if ef[i]='E' then begin e[gf[top]]:=i; dec(top); end;
    end;
   line:=1; ans:=0;
  // writeln('biaoji5');
   while line<=n do
     begin
     ans:=max(ans,dfs(0));
     inc(line);
     end;
   if ans=trueans then writeln('Yes') else writeln('No');
 //  writeln(trueans,' ',ans);
   end;

  close(input);
  close(output);
end.

#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int N=100001;
const int M=200001;
int T,n,m,k,_mod;
bool bo[N];
int cnt,head[N],Cnt,Head[N],du[N],p[N];
struct xint{int to,next,s;}e[M],E[M];
void addedge(int x,int y,int s){
	e[++cnt]=(xint){y,head[x],s};head[x]=cnt;
}
void Addedge(int x,int y,int s){
	E[++Cnt]=(xint){y,Head[x],s};Head[x]=Cnt;
	du[y]++;
}
int dis[N],q[N*50];bool vis[N],flag;
bool cmp(int a,int b){return dis[a]<dis[b];}
void inc(int&a,int b){
	if(b==-1)a=-1;else if(a!=-1){
		a+=b;
		if(a>=_mod)a-=_mod;
	}
}
void work1(){
	ref(i,1,n)dis[i]=2e9;
	ref(i,1,n)vis[i]=0;
	int h=0,t=0;
	dis[1]=0;q[++t]=1;vis[1]=1;
	while(h++<t){
		int u=q[h];
		for(int i=head[u];i;i=e[i].next){
			int v=e[i].to;
			if(dis[u]+e[i].s<dis[v]){
				dis[v]=dis[u]+e[i].s;
				if(!vis[v])q[++t]=v,vis[v]=1;
			}
		}
		vis[u]=0;
	}
}
int dp[N][51];bool mk[N];
void work3(int s,int op,int ed){
	ref(i,op,ed)mk[p[i]]=1;
	ref(i,op,ed){
		for(int j=Head[p[i]];j;j=E[j].next){
			int v=E[j].to; if(mk[v])du[v]++;
		}
	}
	int h=0,t=0;
	ref(i,op,ed)if(!du[p[i]])q[++t]=p[i];
	while(h++<t){
		int u=q[h];
		for(int i=Head[u];i;i=E[i].next){
			int v=E[i].to; if(mk[v]){du[v]--;if(!du[v])q[++t]=v;}
		}
	}
	ref(i,1,t)bo[q[i]]=1;
	ref(i,1,t){
		int u=q[i],ss=dp[u][s-dis[u]];
		for(int j=head[u];j;j=e[j].next){
			int v=e[j].to,se=s+e[j].s-dis[v];
			if(se<=k)inc(dp[v][se],ss);
		}
	}
	ref(i,op,ed)if(!bo[p[i]])dp[p[i]][s-dis[p[i]]]=-1;
	ref(i,op,ed)mk[p[i]]=0,du[p[i]]=0;
	ref(i,1,t)bo[q[i]]=0;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--){
		cnt=0;Cnt=0;
		n=read(),m=read(),k=read(),_mod=read();
		ref(i,1,n)head[i]=Head[i]=0;
		ref(i,1,m)e[i].to=e[i].next=E[i].to=E[i].next=0;
		ref(i,1,n)du[i]=0;
		ref(i,1,n)bo[i]=0;
		ref(i,1,n)ref(j,0,k)dp[i][j]=0;
		ref(i,1,m){
			int x=read(),y=read(),s=read();
			addedge(x,y,s);if(!s)Addedge(x,y,0);
		}
		flag=0;
		work1();
		ref(i,1,n)du[i]=0;
		ref(i,1,n)p[i]=i;
		sort(p+1,p+n+1,cmp);
		int cnt1=1,cnt2=0;
		dp[1][0]=1;
		ref(i,0,dis[n]+k){
			while(cnt2<n&&dis[p[cnt2+1]]<=i)cnt2++;
			while(cnt1<=cnt2&&dis[p[cnt1]]+k<i)cnt1++;
			if(cnt1>cnt2)continue;
			work3(i,cnt1,cnt2);
		}
		int ans=0;
		ref(i,0,k)inc(ans,dp[n][i]);
		printf("%d\n",ans);
	}
}

#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int inf=2e9;
int Read(){
	char c=getchar();
	while(c!='n'&&(c<'0'||c>'9'))c=getchar();
	if(c=='n')return inf;int d=0;
	while(c>='0'&&c<='9')d=d*10+c-48,c=getchar();
	return d;
}
int T,L,tt,h[102];
bool vis['z'+1],err;
struct xint{char c;int x,y;}s[102];

int Cnt,head[102],f[102];
struct yint{int to,next;}e[205];
void addedge(int x,int y){
	e[++Cnt]=(yint){y,head[x]};head[x]=Cnt;
}
void dfs(int x){
	if(vis[s[x].c])err=1;
	vis[s[x].c]=1;
	for(int i=head[x];i;i=e[i].next){
		dfs(e[i].to);f[x]=max(f[x],f[e[i].to]);
	}
	if(s[x].x>s[x].y)f[x]=0;else
	if(s[x].y==inf&&s[x].x!=inf)f[x]++;
	vis[s[x].c]=0;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while(T--){
		memset(vis,0,sizeof vis);
		memset(h,0,sizeof h);
		memset(s,0,sizeof s);
		memset(e,0,sizeof e);
		memset(f,0,sizeof f);
		Cnt=0;
		memset(head,0,sizeof head);
		L=read();
		char c=getchar();
		while(c!='(')c=getchar();
		c=getchar();
		if(c=='1')tt=0;else tt=read();
		int cnt=0;
		err=0;
		h[0]=101; s[0]=(xint){0,1,1};
		ref(i,1,L) {
			char c=getchar();
			while(c<'A'||c>'Z')c=getchar();
			if(c=='E') {
				if(!cnt)err=1;
				else cnt--;
				continue;
			}
			while(c<'a'||c>'z')c=getchar();
			++cnt;
			int x=Read(),y=Read();
			s[i]=(xint){c,x,y};h[cnt]=i;
			addedge(h[cnt-1],h[cnt]);
		}
		if(cnt)err=1;
		if(err)printf("ERR\n");
		else 
		{
			dfs(101);
			if(err)printf("ERR\n");else
			if(f[101]==tt)printf("Yes\n");else printf("No\n");
		}
	}
}

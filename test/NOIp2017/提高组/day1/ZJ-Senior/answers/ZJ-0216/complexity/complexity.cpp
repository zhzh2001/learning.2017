#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <vector>
#include <set>
#include <queue>
using namespace std;

const int N = 1000;

char ch, chh[210];
struct REC
{
	int fc,fn;
};
struct STK
{
	int ii,fc,fn;
	bool fru;
	REC ne;
};

STK stk[210];
bool flagi[50], flag_n, ERR;
REC ans;
int T,L,top;

void Clr()
{
	flag_n = 0;
//	L = 0;
	ERR = 0;
	ans.fc = 1, ans.fn = 0;
	top = 0;
	memset(stk,0,sizeof(stk));
	stk[0].fc = 1, stk[0].fn = 0;
	stk[0].ne.fc = 1, stk[0].ne.fn = 0;
	stk[0].fru = 1;
	memset(flagi,0,sizeof(flagi));
}

void Work_F()
{
	for (ch=getchar();ch<'a' || ch>'z';ch=getchar());
	//cout << "CH " << ch <<endl;
	if (flagi[ch-'a']) { cin.getline(chh,100);/*puts("ERR1");*/ ERR = 1; return; }
	STK tmp;
	int x=0, y=0;
	flagi[ch-'a'] = 1;
	tmp.ii = ch-'a';
	for (ch=getchar();ch!='n' && (ch<'0' || ch>'9');ch=getchar());
	if (ch == 'n') x = N;
	else
	{
		for (;ch>='0'&&ch<='9';ch=getchar()) x = x*10+ch-'0';
	}
	//Get x
	for (ch=getchar();ch!='n' && (ch<'0' || ch>'9');ch=getchar());
	if (ch == 'n') y = N;
	else
	{
		for (;ch>='0'&&ch<='9';ch=getchar()) y = y*10+ch-'0';
	}
	//Get y
	//printf("F %d %d\n",x,y);
	/*printf("x y %d %d\n",x,y);*/
	if (x > y)
	{
		tmp.fc = 1, tmp.fn = 0;	tmp.fru = 0; tmp.ne.fc = 1, tmp.ne.fn = 0;
		stk[++top] = tmp;
		/*puts("sit 1");*/
	}
	else
	if (y != N)
	{
		tmp.fc = 1, tmp.fn = 0;	tmp.fru = 1;  tmp.ne.fc = 1, tmp.ne.fn = 0;
		stk[++top] = tmp;
		/*puts("sit 2");*/
	}
	else
	{
		if (x == N)
		{
			tmp.fc = 1, tmp.fn = 0, tmp.fru = 1,  tmp.ne.fc = 1, tmp.ne.fn = 0;
			stk[++top] = tmp;
			/*puts("sit 3");*/
		}
		else
		{
			tmp.fc = 0, tmp.fn = 1, tmp.fru = 1,  tmp.ne.fc = 1, tmp.ne.fn = 0;
			stk[++top] = tmp;
			/*puts("sit 4");*/
		}
	}
}

void Work_E()
{
	//printf("E\n");
	if (top == 0) { /*puts("ERR2");*/ ERR = 1; return; }
	STK tmp = stk[top--];
	flagi[tmp.ii] = 0;
	STK tmpp = stk[top];
	if (tmpp.fru == 0) { /*puts("OUT");*/ return; }
	//Cal tmp's ans
	tmp.fn += tmp.ne.fn;
	tmp.fc *= tmp.ne.fc;
	//Compare tmp to tmpp.ne and change it
	if (tmp.fn > tmpp.ne.fn)
	{
		tmpp.ne.fn = tmp.fn;
		tmpp.ne.fc = 0;
		stk[top] = tmpp;
		/*puts("fn > ne");*/
	}
}

int main()
{
	freopen("complexity.in","r",stdin);	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		Clr();
		//puts("------------");
		
		scanf("%d",&L);
		//printf("L : %d\n",L);
		char ch;
		for (ch=getchar();ch<'0'||ch>'9';ch=getchar()) if (ch == 'n') flag_n = 1;
		int xx = 0;
		for (;ch>='0' && ch<='9';ch=getchar()) xx = xx*10 + ch-'0';
		if (flag_n) ans.fn = xx, ans.fc = 0;
		else ans.fc = xx, ans.fn = 0;
		//printf("ans %d %d\n",ans.fc,ans.fn);
		//read(O())
		//Working
		for (int i=1;i<=L;++i)
		{
			if (ERR)
			{
				//if (L == 88) puts("IN");
				cin.getline(chh,100);
				continue;
			}
			for (ch=getchar();ch!='F' && ch!='E';ch=getchar());
			if (ch == 'F')
			{
				Work_F();
				//puts("IN F");
			}
			else
			if (ch == 'E')
			{
				Work_E();
				//puts("IN E");
			}
		}
		//End Working
		if (top != 0 || ERR)
		{
			puts("ERR");
			continue;
		}
		stk[0].fc = stk[0].ne.fc;
		stk[0].fn = stk[0].ne.fn;
		//Compar ans
		//printf("stk %d %d\n",stk[0].fc,stk[0].fn);
		if (ans.fn == stk[0].fn && ans.fc == stk[0].fc)
		{
			puts("Yes");
		}
		else
		{
			puts("No");
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

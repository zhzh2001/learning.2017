#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <vector>
#include <set>
#include <queue>
using namespace std;

typedef long long LL;

int read()
{
	int xx = 0; bool f = 0; char ch;
	for (ch=getchar();ch<'0' || ch>'9';ch=getchar()) if (ch == '-') f = 1;
	for (;ch>='0' && ch<='9';ch=getchar()) xx = xx*10 + ch-'0';
	if (f) xx = -xx;
	return xx;
}

const int MM = 200500, NN = 100500;

struct E
{
	int ne,v,w;
}e[MM];

int head[NN], dis[NN], tot;
bool vis[NN];
LL ans, sum[NN][55], mod;
int T,n,m,K;

void Build(int xx,int yy,int zz)
{
	e[++tot].ne = head[xx], head[xx] = tot, e[tot].v = yy, e[tot].w = zz;
}

queue <int> que;
void SPFA()
{
	memset(dis,0x3f,sizeof(dis));
	memset(vis,0,sizeof(vis));
	vis[1] = 1, dis[1] = 0;
	while (!que.empty()) que.pop();
	que.push(1);
	while (!que.empty())
	{
		int now = que.front(); que.pop(); vis[now] = 0;
		for (int i=head[now];i;i=e[i].ne)
		if (dis[e[i].v] > dis[now] + e[i].w)
		{
			dis[e[i].v] = dis[now] + e[i].w;
			if (!vis[e[i].v])
			{
				vis[e[i].v] = 1;
				que.push(e[i].v);
			}
		}
	}
}

struct Q
{
	int id, diss;
};
queue <Q> q;
void BFS()
{
	sum[1][0] = 1;
	Q tmp;
	tmp.id = 1, tmp.diss = 0;
	while (!q.empty()) q.pop();
	q.push(tmp);
	while (!q.empty())
	{
		Q now = q.front(); q.pop();
		for (int i=head[now.id];i;i=e[i].ne)
		if (now.diss + e[i].w <= dis[e[i].v] + K)
		{
			sum[e[i].v][now.diss+e[i].w-dis[e[i].v]] += sum[now.id][now.diss-dis[now.id]];
			sum[e[i].v][now.diss+e[i].w-dis[e[i].v]] %= mod;
			tmp.diss = now.diss + e[i].w;
			tmp.id = e[i].v;
			q.push(tmp);
		}
	}
}

void Clr()
{
	ans = 0;
	memset(sum,0,sizeof(sum));
	memset(head,0,sizeof(head));
	tot = 0;
}

int main()
{
	freopen("park.in","r",stdin);	freopen("park.out","w",stdout);
	T = read();
	int x,y,z;
	while (T--)
	{
		Clr();
		n = read(), m = read(), K = read(), mod = (LL)read();
		for (int i=1;i<=m;++i)
		{
			x = read(), y = read(), z = read();
			Build(x,y,z);
		}
		SPFA();
		BFS();
		ans = 0;
		for (int i=0;i<=K;++i)
		{
			ans = (ans + sum[n][i]) % mod;
		}
		printf("%lld\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

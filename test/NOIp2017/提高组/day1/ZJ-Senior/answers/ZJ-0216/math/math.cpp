#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <vector>
#include <set>
#include <queue>
using namespace std;

typedef long long LL;

LL n,m;
LL x,w,y;

int main()
{
	freopen("math.in","r",stdin); freopen("math.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n > m) swap(n,m);
	if (n == 1)
	{
		puts("0");
		fclose(stdin); fclose(stdout);
		return 0;
	}
	x = n*(n-1)-1;
	w = m - (n+1);
	y = x + w*(n-1);
	printf("%lld\n",y);
	fclose(stdin); fclose(stdout);
	return 0;
}

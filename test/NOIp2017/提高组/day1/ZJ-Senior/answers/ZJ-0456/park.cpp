#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <set>
#include <vector>
#include <cmath>
#include <queue>

using namespace std;

typedef long long ll;

struct Qnode {
	int dis, w;
	Qnode(){}
	Qnode(int _1, int _2) {
		dis = _1;
		w = _2;
	}
	bool operator < (const Qnode& X) const {
		return dis > X.dis;
	}
};

priority_queue <Qnode> Q;

int F[100010][55], Head[200010], Next[400010], Go[400010], Val[400010], Cnt = 0, n, m, K, p;

int HeadF[200010], NextF[400010], GoF[400010], ValF[400010], CntF = 0;

int Dis[200010], DisF[200010], Used[200010], UsedF[200010], RD[200010], Canvis[200010];

vector <int> tpst;

void addedge(int x, int y, int z)
{
	Go[++Cnt] = y;
	Val[Cnt] = z;
	Next[Cnt] = Head[x];
	Head[x] = Cnt;
}

void addedgeF(int x, int y, int z)
{
	GoF[++CntF] = y;
	ValF[CntF] = z;
	NextF[CntF] = HeadF[x];
	HeadF[x] = CntF;
}

void dij(int st, int en, int *Head, int *Next, int *Go, int *Val, int *Dis, int *Used)
{
	memset(Dis, 63, sizeof ::Dis);
	while(!Q.empty()) Q.pop();
	memset(Used, 0, sizeof ::Used);
	Dis[st] = 0;
	Q.push(Qnode(0, st));
	while(!Q.empty()) {
		while(!Q.empty() && Used[Q.top().w]) Q.pop();
		if(Q.empty()) return;
		Qnode nw = Q.top();
		Used[nw.w] = 1;
		Q.pop();
		for(int T = Head[nw.w]; T; T = Next[T])
			if(!Used[Go[T]] && Dis[Go[T]] > nw.dis + Val[T]) {
				Dis[Go[T]] = nw.dis + Val[T];
				Q.push(Qnode(Dis[Go[T]], Go[T]));
			}
	}
}

int getans()
{
	memset(F, 0, sizeof F);
	memset(Head, 0, sizeof Head);
	memset(Next, 0, sizeof Next);
	memset(Go, 0, sizeof Go);
	memset(Val, 0, sizeof Val);
	memset(HeadF, 0, sizeof HeadF);
	memset(NextF, 0, sizeof NextF);
	memset(GoF, 0, sizeof GoF);
	memset(ValF, 0, sizeof ValF);
	Cnt = 0;
	CntF = 0;
	memset(RD, 0, sizeof RD);
	scanf("%d%d%d%d", &n, &m, &K, &p);
	for(int i = 1; i <= m; i++) {
		int a, b, c;
		scanf("%d%d%d", &a, &b, &c);
		addedge(a, b, c);
		addedgeF(b, a, c);
	}
	if(K < 0) return 0;
	dij(1, n, Head, Next, Go, Val, Dis, Used);
	dij(n, 1, HeadF, NextF, GoF, ValF, DisF, UsedF);
	int totp = 0;
	for(int i = 1; i <= n; i++) Canvis[i] = (Dis[i] + DisF[i] - Dis[n] <= K);
	for(int i = 1; i <= n; i++) totp += Canvis[i];
	for(int i = 1; i <= n; i++) if(Canvis[i]) {
		for(int T = Head[i]; T; T = Next[T])
			if(Canvis[Go[T]] && Dis[Go[T]] == Dis[i] + Val[T]) RD[Go[T]]++;
	}
	tpst.resize(0);
	for(int i = 1; i <= n; i++) if(Canvis[i] && !RD[i]) tpst.push_back(i);
	for(int i = 0; i < tpst.size(); i++) {
		int nw = tpst[i];
		for(int T = Head[nw]; T; T = Next[T])
			if(Canvis[Go[T]] && Dis[Go[T]] == Dis[nw] + Val[T]) {
				RD[Go[T]]--;
				if(RD[Go[T]] == 0) tpst.push_back(Go[T]);
			}
	}
	if(tpst.size() != totp) return -1;
	F[1][0] = 1;
	for(int b = 0; b <= K; b++)
		for(int i = 0; i < totp; i++) {
			int nw = tpst[i], tmp = F[nw][b];
			for(int T = Head[nw]; T; T = Next[T]) if(Canvis[Go[T]]) {
				int chdis = b + Dis[nw] + Val[T] - Dis[Go[T]];
				if(chdis <= K) F[Go[T]][chdis] = (F[Go[T]][chdis] + tmp) % p;
			}
		}
	int ans = 0;
	for(int i = 0; i <= K; i++) ans = (ans + F[n][i]) % p;
	return (ans % p + p) % p;
}

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int Cases;
	scanf("%d", &Cases);
	while(Cases--) {
		printf("%d\n", getans());
	}
	return 0;
}

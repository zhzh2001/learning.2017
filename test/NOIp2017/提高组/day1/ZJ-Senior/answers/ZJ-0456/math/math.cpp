#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <set>
#include <vector>
#include <cmath>

using namespace std;

typedef long long ll;

int main()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	ll a, b;
	scanf("%lld%lld", &a, &b);
	if(a == 1 || b == 1) puts("0"); else 
	printf("%lld\n", (a - 1) * (b - 1) - 1);
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <set>
#include <vector>
#include <cmath>
#define Inf 10000000

using namespace std;

int Deal(char *X)
{
	if(X[0] == 'n') return -1;
	int ans = 0, st = strlen(X);
	for(int i = 0; i < st; i++) ans = ans * 10 + X[i] - '0';
	return ans;
}

int Op[111][3], lett[111];

int calcxy(int x, int y)
{
	if(x == -1 && y == -1) return 0;
	if(x == -1 && y != -1) return -Inf;
	if(x != -1 && y == -1) return 1;
	if(x != -1 && y != -1) {
		if(x <= y) return 0;
		return -Inf;
	}
	return 0;
}

void calc()
{
	int L;
	scanf("%d", &L);
	char ch[111];
	int O = 0;
	memset(ch, 0, sizeof ch);
	scanf("%s", ch);
	if(ch[2] == '1') O = 0;
	else {
		for(int i = 4; ch[i] <= '9' && ch[i] >= '0'; i++)
			O = O * 10 + ch[i] - '0';
	}
	int is_Using[128], NwC[128];
	memset(Op, 0, sizeof Op);
	memset(is_Using, 0, sizeof is_Using);
	memset(NwC, 0, sizeof NwC);
	memset(lett, 0, sizeof lett);
	for(int i = 1; i <= L; i++) {
		char ch[4][111];
		scanf("%s", ch[0]);
		if(ch[0][0] == 'E') {
			Op[i][0] = 1;
		} else {
			scanf("%s%s%s", ch[1], ch[2], ch[3]);
			int nwx = 0, nwy = 0;
			nwx = Deal(ch[2]);
			nwy = Deal(ch[3]);
			Op[i][0] = 0;
			Op[i][1] = nwx;
			Op[i][2] = nwy;
			lett[i] = ch[1][0];
		}
	}
	int NwO[111], totO[111];
	memset(NwO, 0, sizeof NwO);
	memset(totO, 0, sizeof totO);
	int lay = 0;
	for(int i = 1; i <= L; i++) {
		if(Op[i][0] == 0) {
			if(is_Using[lett[i]]) {
				puts("ERR");
				return;
			}
			is_Using[lett[i]] = 1;
			NwC[lay] = lett[i];
			NwO[lay] = calcxy(Op[i][1], Op[i][2]);
			totO[lay + 1] = 0;
			NwO[lay + 1] = 0;
			lay++;
		}
		else if(lay == 0) {
			puts("ERR");
			return;
		} else {
			lay--;
			is_Using[NwC[lay]] = 0;
			totO[lay] = max(totO[lay], totO[lay + 1] + NwO[lay]);
		}
	}
	if(lay != 0) {
		puts("ERR");
		return;
	}
	totO[0] = max(totO[0], 0);
	if(totO[0] != O) {
		puts("No");
		return;
	}
	puts("Yes");
	return;
}

int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int Cases;
	scanf("%d", &Cases);
	while(Cases--) {
		calc();
	}
	return 0;
}

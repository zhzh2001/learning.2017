#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
const int N=100005,M=200005;
int la[N],to[M],v[N],pr[M],in[N],q[N],d[N],f[N][51],g[N][51],ans,cnt,n,m,x,y,z,K,mod,T;
int low[N],dfn[N],ti,cc,si[N],top,st[N],vis[N],c[N];
int t[N],di[N];
int h[N];
int laf[N],tof[N],vf[N],prf[N],cntf;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}
inline void addf(int x,int y,int z)
{
	tof[++cntf]=y;
	prf[cntf]=laf[x];
	laf[x]=cntf;
	vf[cntf]=z;
}
inline void spfa()
{
	memset(d,0x3f,sizeof(d));
	int l=0,r=1;
	q[1]=1;
	d[1]=0;
	in[1]=1;
	while (l!=r)
	{
		l=l%n+1;
		int now=q[l];
		for (int i=la[now];i;i=pr[i])
			if (d[to[i]]>d[now]+v[i])
			{
				d[to[i]]=d[now]+v[i];
				if (!in[to[i]])
				{
					in[to[i]]=1;
					r=r%n+1;
					q[r]=to[i];
				}
				t[to[i]]=1;
			}
			else
			{
				if (d[to[i]]==d[now]+v[i])
					++t[to[i]];
			}
		in[now]=0;
	}
}
inline void respfa()
{
	memset(di,0x3f,sizeof(di));
	int l=0,r=1;
	q[1]=n;
	di[n]=0;
	in[n]=1;
	while (l!=r)
	{
		l=l%n+1;
		int now=q[l];
		for (int i=laf[now];i;i=prf[i])
			if (di[tof[i]]>di[now]+vf[i])
			{
				di[tof[i]]=di[now]+vf[i];
				if (!in[tof[i]])
				{
					in[tof[i]]=1;
					r=r%n+1;
					q[r]=tof[i];
				}
			}
		in[now]=0;
	}
}
inline void dfs()
{
	int l=0,r=1;
	q[1]=1;
	in[1]=1;
	g[1][0]=f[1][0]=1;
	while (l!=r)
	{
		l=l%n+1;
		int now=q[l];
		for (int i=la[now];i;i=pr[i])
		{
			int kkk=d[now]+v[i]-d[to[i]],b=0;
			for (int j=kkk;j<=K;++j)
			{
				f[to[i]][j]+=f[now][j-kkk];
				g[to[i]][j]+=f[now][j-kkk];
				f[to[i]][j]-=f[to[i]][j]>mod?mod:0;
				g[to[i]][j]-=g[to[i]][j]>mod?mod:0;
				if (f[to[i]][j])
					b=1;
			}
			if (b&&!in[to[i]])
			{
				r=r%n+1;
				q[r]=to[i];
				in[to[i]]=1;
			}
		}
		in[now]=0;
		memset(f[now],0,sizeof(f[now]));
	}
}
void tarjan(int x)
{
	dfn[x]=low[x]=++ti;
	st[++top]=x;
	vis[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (v[i]==0)
		{
			if (!vis[to[i]])
				tarjan(to[i]);
			low[x]=min(low[x],low[to[i]]);
		}
	if (low[x]==dfn[x])
	{
		++cc;
		si[cc]=0;
		while (st[top+1]!=x)
		{
			c[st[top]]=cc;
			--top;
			++si[cc];
		}
	}
}
void work0()
{
	memset(h,0,sizeof(h));
	int l=0,r=1;
	q[1]=1;
	h[1]=1;
	while (l!=r)
	{
		l=l%n+1;
		int now=q[l];
		for (int i=la[now];i;i=pr[i])
			if (d[to[i]]==d[now]+v[i]&&d[to[i]]+di[to[i]]<=K+d[n])
			{
				h[to[i]]+=h[now];
				h[to[i]]-=h[to[i]]>mod?mod:0;
				--t[to[i]];
				if (!t[to[i]])
				{
					r=r%n+1;
					q[r]=to[i];
				}
			}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while (T--)
	{
		cnt=0;
		cntf=0;
	read(n);
	read(m);
	read(K);
	read(mod);
	memset(la,0,sizeof(la));
	memset(laf,0,sizeof(laf));
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		addf(y,x,z);
	}
	spfa();
	respfa();
	memset(vis,0,sizeof(vis));
	ti=0;
	top=0;
	for (int i=1;i<=n;++i)
		if (!vis[i])
			tarjan(i);
	int tc=0;
	for (int i=1;i<=n;++i)
		if (si[c[i]]>1&&d[i]+di[i]<=d[n]+K)
		{
			puts("-1");
			tc=1;
			break;
		}
	if (tc)
		continue;
	if (K==0)
	{
		work0();
		cout<<h[n]<<'\n';
	}
	else
	{
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	dfs();
	ans=0;
	for (int i=0;i<=K;++i)
		(ans+=g[n][i])%=mod;
	cout<<ans<<'\n';
	}
	}
	return 0;
}
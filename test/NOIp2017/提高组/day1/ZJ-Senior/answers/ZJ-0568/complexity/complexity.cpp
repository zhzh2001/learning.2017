#include<iostream>
#include<cstdio>
#include<cstring>
#define mp make_pair
using namespace std;
const int N=105;
pair<char,int> q[N];
int n,m,ans,sum,top,b,v[N],t,p;
char s[N],s1[N],s2[N],s3[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void del()
{
	if (top==0)
	{
		b=1;
		return;
	}
	sum-=q[top].second;
	v[q[top].first-'a']=0;
	--top;
}
inline void add()
{
	scanf("%s%s%s",s1,s2,s3);
	char c=s1[0];
	if (v[c-'a'])
	{
		b=1;
		return;
	}
	int kkk;
	v[c-'a']=1;
	if (s2[0]=='n'&&s3[0]=='n')
		kkk=0;
	if (s2[0]=='n'&&s3[0]!='n')
		kkk=-2333;
	if (s2[0]!='n'&&s3[0]=='n')
		kkk=1;
	if (s2[0]!='n'&&s3[0]!='n')
	{
		int p1=0,p2=0;
		for (int i=0;i<strlen(s2);++i)
			p1=p1*10+s2[i]-'0';
		for (int i=0;i<strlen(s3);++i)
			p2=p2*10+s3[i]-'0';
		if (p1<=p2)
			kkk=0;
		else
			kkk=-2333;
	}
	sum+=kkk;
	ans=max(ans,sum);
	q[++top]=mp(c,kkk);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(t);
	while (t--)
	{
		b=0;
		read(n);
		scanf("%s",s);
		if (s[2]=='1')
			p=0;
		else
		{
			if (s[5]>='0'&&s[5]<='9')
				p=(s[4]-'0')*10+s[5]-'0';
			else
				p=s[4]-'0';
		}
		top=0;
		sum=0;
		ans=0;
		memset(v,0,sizeof(v));
		for (int i=1;i<=n;++i)
		{
			scanf("%s",s);
			if (s[0]=='E')
				del();
			else
				add();
		}
		if (b||top!=0)
			puts("ERR");
		else
		{
			if (ans==p)
				puts("Yes");
			else
				puts("No");
		}
	}
	return 0;
}
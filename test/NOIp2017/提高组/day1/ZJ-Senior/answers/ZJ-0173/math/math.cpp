#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int gcd(int a,int b) {return b==0?a:gcd(b,a%b);}
const int N=1e8;
bool f[N];
int calc(int a,int b) 
{
	int sz=a*b;
	memset(f,0,sizeof(bool)*sz);
	int ans=0;
	f[0]=true;
	for (int i=0;i+a<sz;++i) if (f[i]==true) f[i+a]=true;
	for (int i=0;i+b<sz;++i) if (f[i]==true) f[i+b]=true;
	for (ans=sz-1;f[ans]==true;--ans);
	return ans;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int a=0,b=0;
	scanf("%d %d",&a,&b);
	printf("%d\n",calc(a,b));
}

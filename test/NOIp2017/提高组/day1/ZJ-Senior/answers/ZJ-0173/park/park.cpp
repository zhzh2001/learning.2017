#include <cstdio>
#include <cctype>
#include <cstring>
#include <queue>
using namespace std;
int read()
{
	char c=0;int x=0;
	while (c=getchar(),!isdigit(c));x=c-'0';
	while (c=getchar(),isdigit(c)) x=x*10+c-'0';
	return x;
}
int p;
const int N=1e5+5,M=2e5+5,K=51;
int cccnt,last[N],dis[N],cnt[K][N],q[N],deg[N];
pair<int,int> h[N+M];
struct edge
{
	int y,z,last;
}E[M];
void link(int x,int y,int z) 
{
	E[cccnt]=(edge){y,z,last[x]},last[x]=cccnt++;
}
void getMin()
{
	int cnt=0;
	dis[0]=0;
	h[cnt++]=make_pair(0,0);
	for (int p=0;cnt>0;) 
	{
		while (cnt>0 && dis[h[0].second]!=h[0].first) pop_heap(h,h+cnt--,greater<pair<int,int> >());
		if (cnt>0) 
		{
			p=h[0].second,pop_heap(h,h+cnt--,greater<pair<int,int> >());
			for (int i=last[p];~i;i=E[i].last) 
			{
				int y=E[i].y,z=E[i].z;
				if (dis[y]==-1 || z+dis[p]<dis[y]) dis[y]=z+dis[p],h[cnt++]=make_pair(dis[y],y),push_heap(h,h+cnt,greater<pair<int,int> >());
			}
		}
	}
}
int work() 
{
	int n=read(),m=read(),k=read(),p=read();
	cccnt=0;
	memset(last,-1,sizeof(int)*n),memset(dis,-1,sizeof(int)*n);
	for (int i=0,x=0,y=0,z=0;i<m;++i) x=read()-1,y=read()-1,z=read(),link(x,y,z);
	getMin();
	int len=0;
	memset(deg,0,sizeof(int)*n);
	for (int i=0;i<n;++i) if (dis[i]!=-1)
	{
		int d=dis[i];
		for (int j=last[i];~j;j=E[j].last) 
		{
			int y=E[j].y;
			if (d+E[j].z==dis[y]) ++deg[y];
		}
	}
	if (deg[0]==0) len=1,q[0]=0;
	for (int i=0;i<len;++i) 
	{
		int d=dis[q[i]];
		for (int j=last[q[i]];~j;j=E[j].last) 
		{
			int y=E[j].y;
			if (d+E[j].z==dis[y]) 
			{
				--deg[y];
				if (deg[y]==0) q[len++]=y;
			}
		}
	}
	for (int i=0;i<n;++i) if (deg[i]>0) return -1;
	for (int i=0;i<=k;++i) memset(cnt[i],0,sizeof(int)*n);
	cnt[0][0]=1%p;
	for (int ki=0;ki<=k;++ki) 
	{
		for (int i=0;i<len;++i) 
		{
			int qi=q[i],c=cnt[ki][qi],d=dis[qi]+ki;
			for (int j=last[qi];~j;j=E[j].last) 
			{
				int y=E[j].y,t=d+E[j].z-dis[y],xxxx=0;
				if (t<=k) xxxx=cnt[t][y]+c,cnt[t][y]=xxxx>=p?xxxx-p:xxxx;
			}
		} 
	}
	int ans=0;
	for (int i=0;i<=k;++i) ans+=cnt[i][n-1];
	return ans;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (int test=read();test--;)
	{
		printf("%d\n",work());
	}
}


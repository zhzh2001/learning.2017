#include <cstdio>
#include <cstring>
#include <cctype>
#include <algorithm>
using namespace std;
int getInt(char *&x) 
{
	int res=0;
	for (;!isdigit(*x);++x);
	for (;isdigit(*x);++x) res=res*10+*x-'0';
	return res;
}
const int L=1e3+5;
char str[L][30],buf[30],sta[L];
bool f[26];
bool check(int n) 
{
	memset(f,0,sizeof(f));
	int p=0;
	for (int i=0;i<n;++i)
	{
		if (str[i][0]=='F')
		{
			char *s=str[i];
			if (f[s[2]-'a']==true) return false;
			f[s[2]-'a']=true;
			sta[p++]=s[2];
		}
		else 
		{
			if (p==0) return false;
			--p;
			f[sta[p]-'a']=false;
		}
	}
	return p==0;
}
int calc(int &p) 
{
	int cnt=0,res=0,f=1,l=0,r=0;
	char *s=str[p];
	s+=4;
	if (s[0]=='n') 
	{
		l=200;
		if (s[2]=='n') r=200; else r=0;
	}
	else
	{
		l=getInt(s);
		if (s[1]=='n') r=200,cnt=1; else r=getInt(s);
	}
	if (l>r) f=0;
	for (++p;str[p][0]=='F';++p)
	{
		res=max(res,calc(p));
	}
	return (res+cnt)*f;
}
void work()
{
	gets(buf);
	char *s=buf;
	int n=0,ord=0;
	n=getInt(s);
	if (s[3]=='1') 
	{
		ord=0;
	}
	else
	{
		ord=getInt(s);
	}
	for (int i=0;i<n;++i) gets(str[i]);
	if (check(n)==false) 
	{
		puts("ERR");
		return;
	}
	int ans=0;
	for (int i=0;i<n;++i) ans=max(ans,calc(i));
	puts(ans==ord?"Yes":"No");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int test=0;
	gets(buf);
	char *s=buf;
	test=getInt(s);
	for (int i=0;i<test;++i) 
	{
		work();
	}
}

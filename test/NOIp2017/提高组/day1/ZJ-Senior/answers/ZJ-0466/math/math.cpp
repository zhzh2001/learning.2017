#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>
#define ID isdigit(c = fgetc(f))
#define RT if(!~c) return flag = 0, *this;
using namespace std;

struct Istream{
	int flag; FILE *f;
	void init(FILE *_f = stdin) {f = _f;}
	Istream& operator >> (int &x){
		int c, l = 0;
		for(; !ID; l = c) RT;
		for(x = c & 15; ID; x = x * 10 + (c & 15));
		if(l == 45) x = -x; RT;
		return flag = 1, *this;
	}
	int get() {return fgetc(f);}
}Cin;

struct Ostream{
	char buf[34]; FILE *f;
	void init(FILE *_f = stdout) {f = _f;}
	Ostream& operator << (long long x){
		int i;
		if(!x) return put(48), *this;
		if(x < 0) {put(45); x = -x;}
		for(i = 0; x; x /= 10) buf[++i] = x % 10 | 48;
		for(; i; --i) put(buf[i]);
		return *this;
	}
	Ostream& operator << (char x) {return put(x), *this;}
	void put(int x) {fputc(x, f);}
}Cout;

int a, b;
long long scx;

FILE *fin, *fout;

int main(){
	Cin.init(fin = fopen("math.in", "r"));
	Cout.init(fout = fopen("math.out", "w"));
	Cin >> a >> b;
	scx = (long long)(a - 1) * (b - 1) - 1;
	Cout << scx << '\n';
	fclose(fin); fclose(fout);
	return 0;
}


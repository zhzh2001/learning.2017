#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define N 136
#define ID isdigit(c = fgetc(f))
#define RT if(!~c) return flag = 0, *this;
using namespace std;

struct Istream{
	int flag; FILE *f;
	void init(FILE *_f = stdin) {f = _f;}
	Istream& operator >> (int &x){
		int c, l = 0;
		for(; !ID; l = c) RT;
		for(x = c & 15; ID; x = x * 10 + (c & 15));
		if(l == 45) x = -x; RT;
		return flag = 1, *this;
	}
	Istream& operator >> (char &x){
		int c;
		for(c = fgetc(f); c <= 32; c = fgetc(f)) RT; x = c; return flag = 1, *this;
	}
	Istream& operator >> (char *x){
		int c;
		for(c = fgetc(f); c <= 32; c = fgetc(f)) RT;
		for(*x++ = c; (c = fgetc(f)) > 32; *x++ = c);
		*x = 0; RT;
		return flag = 1, *this;
	}
	int get() {return fgetc(f);}
	void nextline() {for(int c = fgetc(f); c != 10 && c != 13; c = fgetc(f));}
}Cin;

struct Ostream{
	char buf[34]; FILE *f;
	void init(FILE *_f = stdout) {f = _f;}
	Ostream& operator << (const char *x){
		for(char *p = (char*)x; *p; ++p) put(*p); return *this;
	}
	void put(int x) {fputc(x, f);}
}Cout;

char ch, var, lb[34], ub[34], vars[N];

int n, deg, Max, nlb, nub;
int sta, stack[N];

char err, used[34]; // bool

FILE *fin, *fout;

inline void up(int &x, const int y) {x < y ? x = y : 0;}

int main(){
	int T;
	Cin.init(fin = fopen("complexity.in", "r"));
	Cout.init(fout = fopen("complexity.out", "w"));
	for(Cin >> T; T; --T){
		Cin >> n >> ch >> ch >> ch;
		if(ch == 110) Cin >> deg;
		else deg = 0;
		Cin.nextline();
		stack[0] = Max = sta = 0; err = 0;
		memset(used, 0, sizeof used);
		for(; n; --n){
			if(err) {Cin.nextline(); continue;}
			if(Cin >> ch, ch == 70){
				Cin >> var;
				if(used[var & 31]) {err = 1; Cin.nextline(); continue;}
				used[var & 31] = 1; vars[++sta] = var;
				Cin >> lb >> ub;
				if(*lb == 110 && *ub == 110)
					stack[sta] = stack[sta - 1];
				else if(*lb == 110 && *ub != 110)
					stack[sta] = INT_MIN;
				else if(*lb != 110 && *ub == 110)
					up(Max, stack[sta] = stack[sta - 1] + 1);
				else{
					nlb = (int)strtol(lb, NULL, 10);
					nub = (int)strtol(ub, NULL, 10);
					stack[sta] = (nlb > nub ? INT_MIN : stack[sta - 1]);
				}
			}else{
				if(!sta) {err = 1; continue;}
				used[vars[sta] & 31] = 0; --sta;
			}
		}
		if(sta || err) Cout << "ERR\n";
		else Cout << (Max == deg ? "Yes\n" : "No\n");
	}
	fclose(fin); fclose(fout);
	return 0;
}


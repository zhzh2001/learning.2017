#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <ext/pb_ds/priority_queue.hpp>
#define maxV 100034
#define maxE 256101
#define K 55
#define ID isdigit(c = fgetc(f))
#define RT if(!~c) return flag = 0, *this;
using __gnu_pbds::priority_queue;

struct Istream{
	int flag; FILE *f;
	void init(FILE *_f = stdin) {f = _f;}
	Istream& operator >> (int &x){
		int c, l = 0;
		for(; !ID; l = c) RT;
		for(x = c & 15; ID; x = x * 10 + (c & 15));
		if(l == 45) x = -x; RT;
		return flag = 1, *this;
	}
	int get() {return fgetc(f);}
}Cin;

struct Ostream{
	char buf[34]; FILE *f;
	void init(FILE *_f = stdout) {f = _f;}
	Ostream& operator << (int x){
		int i;
		if(!x) return put(48), *this;
		if(x < 0) {put(45); x = -x;}
		for(i = 0; x; x /= 10) buf[++i] = x % 10 | 48;
		for(; i; --i) put(buf[i]);
		return *this;
	}
	Ostream& operator << (char x) {return put(x), *this;}
	void put(int x) {fputc(x, f);}
}Cout;

struct edge{
	int u, v, w;
	edge (int u0 = 0, int v0 = 0, int w0 = 0): u(u0), v(v0), w(w0) {}
}e[maxE];

struct _{
	int to, dist;
	_ (int to0 = 0, int dist0 = 0): to(to0), dist(dist0) {}
	bool operator < (const _ &b) const {return dist > b.dist;}
};

typedef __gnu_pbds::priority_queue <_> prioque;

int mod;
int V, E, k, i;
int u, v, w, ans;
int first[maxV], next[maxE];
int _first[maxV], _next[maxE];
int d[maxV], f[maxV][K];
char in_stack[maxV][K];
prioque pq;

FILE *fin, *fout;

inline void addedge(int u, int v, int w, int id) {e[id] = edge(u, v, w); next[id] = first[u]; first[u] = id; _next[id] = _first[v]; _first[v] = id;}
inline void inc(int &x, const int y = 1) {(x += y) >= mod ? x -= mod : 0;}

void Dijkstra(){
	int i, y; _ t;
	memset(d, 63, sizeof d); d[1] = 0;
	for(pq.push(_(1, 0)); !pq.empty(); ){
		t = pq.top(); pq.pop();
		if(t.dist > d[t.to]) continue;
		for(i = first[t.to]; i; i = next[i])
			if(t.dist + e[i].w < d[y = e[i].v]){
				d[y] = t.dist + e[i].w;
				pq.push(_(y, d[y]));
			}
	}
}

int dp(int x, int t){
	int i, y, dist, level, res = 0, tt;
	if(~f[x][t]) return f[x][t];
	if(in_stack[x][t]) return -1;
	in_stack[x][t] = 1;
	//printf("dp %d %d\n", x, t);
	for(i = _first[x]; i; i = _next[i]){
		y = e[i].u; dist = d[x] + t - e[i].w; level = dist - d[y];
		if((unsigned)level <= (unsigned)k){ // transferable
			tt = dp(y, level);
			if(tt == -1) return -1;
			inc(res, tt);
		}
	}
	if(x == 1 && t == 0) inc(res);
	in_stack[x][t] = 0;
	return f[x][t] = res;
}

int main(){
	int T;
	Cin.init(fin = fopen("park.in", "r"));
	Cout.init(fout = fopen("park.out", "w"));
	for(Cin >> T; T; --T){
		memset(first, 0, sizeof first);
		memset(_first, 0, sizeof _first);
		Cin >> V >> E >> k >> mod;
		for(i = 1; i <= E; ++i){
			Cin >> u >> v >> w;
			addedge(u, v, w, i);
		}
		Dijkstra();
		memset(f, -1, sizeof f);
		memset(in_stack, 0, sizeof in_stack);
		for(i = 0; i <= k; ++i) if(dp(V, i) == -1) break;
		if(i <= k) ans = -1;
		else for(ans = i = 0; i <= k; ++i) inc(ans, dp(V, i));
		Cout << ans << '\n';
	}
	fclose(fin); fclose(fout);
	return 0;
}


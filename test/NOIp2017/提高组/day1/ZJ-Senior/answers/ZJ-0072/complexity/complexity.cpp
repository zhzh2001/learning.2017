#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,tst,top;
char st[10],t[5],a[5],b[5],c[5];
bool vs[30];
struct data{
	int k,num;
	char ch;
}stk[105];
int _solve(){
	memset(vs,0,sizeof(vs)); vs['n'-'a']=true; bool pd=1;
	scanf("%d",&n); scanf("%s",st); top=1; stk[1].num=1; stk[1].k=0;
	while (n--){
		scanf("%s",t);
		if (t[0]=='F'){
			scanf("%s",a); scanf("%s",b); scanf("%s",c);
			if (!pd) continue;
			if (vs[a[0]-'a']) {pd=0; continue;}
			vs[a[0]-'a']=1;
			stk[++top].ch=a[0]; stk[top].k=0;
			if (b[0]=='n'){
				if (c[0]=='n') stk[top].num=1;
				else stk[top].num=0;
			}else{
				if (c[0]=='n') stk[top].num=2;
				else{
					int x=0,y=0;
					for (int i=0;i<strlen(b);i++) x=x*10+b[i]-'0';
					for (int i=0;i<strlen(c);i++) y=y*10+c[i]-'0';
					stk[top].num=(x<=y);
				}
			}
		}
		else if (t[0]=='E'){
			if (top<=1) {pd=0; continue;}
			if (!pd) continue;
			if (stk[top].num) stk[top-1].k=max(stk[top-1].k,stk[top].k+stk[top].num-1);
			vs[stk[top].ch-'a']=0; top--;
		}
	}
	if (top!=1||!pd) return 0;
	if (st[2]=='1') return 2-(!stk[1].k);
	else{
		int x=0;
		for (int i=4;i<strlen(st)&&'0'<=st[i]&&st[i]<='9';i++) x=x*10+st[i]-'0';
		return 2-(stk[1].k==x);
	}
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&tst);
	while (tst--){
		int ans=_solve();
		switch (ans){
			case 0:printf("ERR\n"); break;
			case 1:printf("Yes\n"); break;
			case 2:printf("No\n"); break;
		}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#define LL long long
using namespace std;
int n,m,k,len;
LL p,a[1000005],ans;
inline bool cmp(LL x,LL y){
	return x>y;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&n,&m); if (n>m) swap(n,m); len=m-n;
	if (n==2) {printf("%d",m-2); return 0;}
	p=(LL)n*(len+1); a[k=1]=p;
	for (int i=1;i<=len;i++){
		LL t=(LL)i*n; a[++k]=t;
		for (int j=1;j<=i;j++){
			LL ti=t+(LL)j*len;
			if (ti>p) break;
			a[++k]=ti;
		}
	}
	sort(a+1,a+k+1,cmp);
	for (int i=2;i<=k;i++)
		if (a[i-1]-a[i]>1) {ans=a[i-1]-1; break;}
	printf("%lld",ans);
	return 0;
}

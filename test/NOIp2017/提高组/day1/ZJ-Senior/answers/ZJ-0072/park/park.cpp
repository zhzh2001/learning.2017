#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 100005
#define maxe 400005
using namespace std;
int n,e,K,tt,tst,tot,ans,son[maxe],nxt[maxe],w[maxe],lnk[maxn],dst[maxn],que[maxn],f[maxn][55],hsh[maxn];
double dis[maxn];
bool vs[maxn];
inline char nc(){
	static char buf[100000],*pa=buf,*pb=buf;
	return pa==pb&&(pb=(pa=buf)+fread(buf,1,100000,stdin),pa==pb)?EOF:*pa++;
}
inline void readi(int &x){
	x=0; char ch=nc();
	while ('0'>ch||ch>'9') ch=nc();
	while ('0'<=ch&&ch<='9') {x=x*10+ch-'0'; ch=nc();}
}
void _add(int x,int y,int z){
	son[++tot]=y; w[tot]=z; nxt[tot]=lnk[x]; lnk[x]=tot;
}
bool _check(){
	memset(vs,0,sizeof(vs));
	memset(hsh,0,sizeof(hsh));
	memset(dis,127,sizeof(dis));
	int hed=0,til=1; que[1]=vs[1]=hsh[1]=1; dis[1]=0.0;
	while (hed!=til){
		hed=(hed+1)%maxn; vs[que[hed]]=0;
		for (int j=lnk[que[hed]];j;j=nxt[j])
			if (dis[son[j]]>dis[que[hed]]+w[j]-1e12){
				dis[son[j]]=dis[que[hed]]+w[j]-1e12;
				if (!vs[son[j]]){
					til=(til+1)%maxn; que[til]=son[j]; vs[son[j]]=1; hsh[son[j]]++;
					if (hsh[son[j]]>=n) return 1;
					if (dis[son[j]]<dis[que[(hed+1)%maxn]]) swap(que[til],que[(hed+1)%maxn]);
				}
			}
	}
	return 0;
}
void _spfa(){
	memset(vs,0,sizeof(vs));
	memset(dst,63,sizeof(dst));
	int hed=0,til=1; que[1]=1; vs[1]=1; dst[1]=0;
	while (hed!=til){
		hed=(hed+1)%maxn; vs[que[hed]]=0;
		for (int j=lnk[que[hed]];j;j=nxt[j])
			if (dst[son[j]]>dst[que[hed]]+w[j]){
				dst[son[j]]=dst[que[hed]]+w[j];
				if (!vs[son[j]]){
					til=(til+1)%maxn; que[til]=son[j]; vs[son[j]]=1;
					if (dst[son[j]]<dst[que[(hed+1)%maxn]]) swap(que[til],que[(hed+1)%maxn]);
				}
			}
	}
}
void _init(){
	readi(n); readi(e); readi(K); readi(tt);
	for (int i=1,x,y,z;i<=e;i++){
		readi(x); readi(y); readi(z); _add(x,y,z);
	}
}
void _solve(){
	if (_check()) {printf("-1\n"); return;}
	_spfa();
	memset(f,0,sizeof(f));
	memset(vs,0,sizeof(vs));
	int hed=0,til=1; f[1][0]=1; que[1]=1; vs[1]=1;
	while (hed!=til){
		hed=(hed+1)%maxn; vs[que[hed]]=0;
		for (int j=lnk[que[hed]],num;j;j=nxt[j]){
			num=w[j]-dst[son[j]]+dst[que[hed]];
			if (num>K) continue;
			if (!vs[son[j]]){
				til=(til+1)%maxn; que[til]=son[j]; vs[son[j]]=1;
			}
			for (int k=num;k<=K;k++) f[son[j]][k]=(f[son[j]][k]+f[que[hed]][k-num])%tt;
		}
	}
//	for (int i=1;i<=n;i++) printf("%d ",dst[i]); printf("\n");
//	for (int i=1;i<=n;i++)
//		for (int j=0;j<=K;j++) printf("%d %d %d\n",i,j,f[i][j]);
	ans=0;
	for (int i=0;i<=K;i++) ans=(ans+f[n][i])%tt;
	printf("%d\n",ans);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	readi(tst);
	while (tst--){
		_init();
		_solve();
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <queue>
using namespace std;
const int N=400005;
const int inf=1000000005;
int pd,vis[N],in[N],in2[N],top,dui[N],t,ans,nedge,n,Next[N*2],head[N],to[N*2],dis[N],diss[N][52],m,k,mod,flag[N],a,b,c,lon[N];
void spfa()
{
	queue <int> q;
	memset(flag,0,sizeof(0));
	for (int i=1;i<=n;i++) dis[i]=inf;
	dis[1]=0;
	q.push(1);
	flag[1]=1;
	while(!q.empty())
	{
		int k=q.front();
		flag[k]=0;
		for (int i=head[k];i;i=Next[i])
		{
			int p=to[i];
			if (dis[k]+lon[i]==dis[p]) diss[p][0]++;
			if (dis[k]+lon[i]<dis[p])
			{
				diss[p][0]=1;
				dis[p]=dis[k]+lon[i];
				if (flag[p]==0)
				{
					flag[p]=1;
					q.push(p);
				}
			}
		}
		q.pop();
	}
	return;
}
void add(int a,int b,int c)
{
	nedge++;
	Next[nedge]=head[a];
	head[a]=nedge;
	to[nedge]=b;
	lon[nedge]=c;
	return;
}
/*void add2(int a,int b)
{
	nedge2++;
	Next2[nedge2]=head2[a];
	head2[a]=nedge2;
	to2[nedge2]=b;
}*/
void dfs(int x)
{
	if (vis[x])
	{
		pd=1;
		return;
	}
	vis[x]=1;
	for (int i=head[x];i;i=Next[i])
	{
		if (lon[i]==0)
		{
			dfs(to[i]);
		}
	}
	vis[x]=0;
	return;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		ans=0;
		scanf("%d%d%d%d",&n,&m,&k,&mod);
		nedge=0;
		memset(diss,0,sizeof(diss));
		memset(head,0,sizeof(head));
		memset(in,0,sizeof(0));
		memset(vis,0,sizeof(0));
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&a,&b,&c);
			add(a,b,c);
			if (c==0)
			{
				in[b]++;
			}
	    }
	    pd=0;
	    for (int i=1;i<=n;i++)
	    {
	    	if (vis[i]==0) dfs(i);
		}
		if (pd)
		{
			printf("-1\n");
			continue;
		}
	   spfa();
	   //diss[i][k]表示第i个点，比最短路多k的总数;
	   diss[1][0]=1;
	   for (int ii=0;ii<=k;ii++)
	   {
	   	if (ii!=0)
	   	{
	   	top=0;
	   	for (int i=1;i<=n;i++) 
	   	{
	   		in2[i]=in[i];
	   		if (in2[i]==0)
	   		{
	   		   top++;
	   		   dui[top]=i;
			}
		}
		for (int i=1;i<=top;i++)
		{
			int pp=dui[i];
			for (int j=head[pp];j;j=Next[j])
			{
				int p=to[j];
				if (lon[j]==0 && dis[pp]==dis[p])
				{
					diss[p][ii]+=diss[pp][ii];
					diss[p][ii]=diss[p][ii]%mod;
					in2[p]--;
					if (in2[p]==0)
					{
						top++;
						dui[top]=p;
					}
				}
			}
		}
	   	
	   }
	   
	   for (int i=1;i<=n;i++)
	   {
	   	  for (int j=head[i];j;j=Next[j])
	   	  {
	   	  	 int p=to[j];
	   	  	 int num=ii+dis[i]+lon[j]-dis[p];
	   	  	 if (num<=k && num!=ii)
	   	  	 {
	   	  	 	diss[p][num]+=diss[i][ii];
	   	  	 	diss[p][num]=diss[p][num]%mod;
			 }
		  }
	   }
      }
	   for (int i=0;i<=k;i++) 
	   {
	   ans+=diss[n][i];
	   ans=ans%mod;
}
	   printf("%d\n",ans);
      }
	return 0;
}

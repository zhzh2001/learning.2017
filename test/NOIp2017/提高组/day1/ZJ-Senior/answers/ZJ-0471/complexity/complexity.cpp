#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <queue>
using namespace std;
struct zhan
{
	int l,r,num;
	char str;
}a[10005];
int lon,l,t,pd,sum,top,er,ans,num,flag[10005];
char str[100],str2[100],bl[200],left1[200],right1[200];
int tur(char str[])
{
	if (str[0]=='n') return -1;
	int sum=0,lon=strlen(str);
	for (int i=0;i<lon;i++)
	{
		sum=sum*10+str[i]-'0';
	}
	return sum;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d",&l);
		scanf("%s",&str);
		lon=strlen(str);
		pd=0;
		sum=0;
		er=0;
		ans=0;
		top=0;
		num=0;
		memset(flag,0,sizeof(flag));
		for (int i=0;i<lon;i++)
		{
			if (str[i]=='n')
			{
				pd=1;
			}
			if (str[i]>='0' && str[i]<='9')
			{
				num=num*10+str[i]-'0';
			}
		}
		for (int i=1;i<=l;i++)
		{
			scanf("%s",&str);
			if (str[0]=='F')
			{
				scanf("%s",&bl);
				if (flag[bl[0]]) er=1;
				else flag[bl[0]]++;
				scanf("%s%s",&left1,&right1);
				top++;
				a[top].str=bl[0];
				a[top].l=tur(left1);
				a[top].r=tur(right1);
				a[top].num=a[top-1].num;
				if (a[top].r==-1 && a[top].l!=-1)
				{
					a[top].num=a[top-1].num+1;
					ans=max(ans,a[top].num);
				}
				if (a[top].l==-1 && a[top].r!=-1)
				{
					a[top].num=-1000;
				}
				if (a[top].l>0 && a[top].r>0)
				{
					if (a[top].l>a[top].r)
					{
						a[top].num=-1000;
					}
				}
			}
			else
			{
				flag[a[top].str]--;
				top--;
			}
		}
		if (top!=0) er=1;
		if (er==1) {
		printf("ERR\n");
		continue;
		}
		if (ans==0)
		{
			if (pd==0) printf("Yes\n");
			else printf("No\n");
			continue;
		}
		if (ans>0)
		{
			if (num==ans && pd==1) printf("Yes\n");
			else printf("No\n");
			continue;
		}
	}
	return 0;
}

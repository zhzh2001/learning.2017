#include <bits/stdc++.h>

using namespace std;

typedef long long LL;
const int maxn = 1e5+10;
LL a,b;

int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	LL ans = a*b-a-b;
	printf("%lld\n",ans);
} 

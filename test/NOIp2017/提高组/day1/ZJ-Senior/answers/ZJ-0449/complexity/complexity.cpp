#include <bits/stdc++.h>

using namespace std;

const int maxn = 110;
int t,m,f,mx,pa,pb,sum,mp[30],sta[maxn],stb[maxn];
char ans[maxn],s[maxn],a[maxn],b[maxn],c[maxn];
stack <char> st; 

inline int read() {
	int ret = 0,fl = 1;char ch = getchar();
	while (ch<'0' || ch>'9') {if (ch == '-') fl = -fl;ch = getchar();}
	while (ch>='0' && ch<='9') {ret = 10*ret+ch-'0';ch = getchar();}
	return ret*fl;
}

inline void work(int k) {
	if (k == 1) {
		sum++;
		scanf("%s%s%s",a,b,c);
		if (mp[a[0]-'a'] == 1) {f = 1;return;}
		mp[a[0]-'a'] = 1;
		st.push(a[0]);
		if (b[0]=='n' && c[0]!='n') sta[++pa] = -1; 
		else if (b[0]!='n' && c[0]=='n') sta[++pa] = 1;
		else sta[++pa] = 0;
		stb[++pb] = 0; 
	}
	else {
		sum--;
		if (sum < 0) {f = 1;return;}
		char ch = st.top();st.pop(),mp[ch-'a'] = 0;
		int now = pb;
		while (stb[pb]) pb--;
		pa = pb;int w = -100;
		if (sta[pa] == -1) return;
		for (int i=pb+1;i<=now;i++) w = max(w,sta[i]);
		if (w > 0) sta[pa] += w;
		if (sta[pa] > 0) mx = max(mx,sta[pa]);
		stb[pb] = 1;
	}
}

int main() {
	freopen("complexity.in","r",stdin);
	t = read();
	while (t--) {
		while (!st.empty()) st.pop();
		pa = pb = 0;
		scanf("%d%s",&m,ans);
		memset(mp,0,sizeof mp);
		sum = mx = f = 0;
		for (int i=1;i<=m;i++) {
			scanf("%s",s);
			if (s[0] == 'F') work(1);
			else if (!f) work(2);
		}
		if (sum != 0) f = 1;
		if (f == 1) {printf("ERR\n");continue;}
		//cout << mx << endl; 
		if (mx == 0) {
		    if (strcmp(ans,"O(1)") != 0) printf("No\n");
		    else printf("Yes\n");
		    continue;
		}
		if (strcmp(ans,"O(1)") == 0) printf("No\n");
		else {
			int i = 4,w = 0;
			while (ans[i]>='0' && ans[i]<='9') w = 10*w+ans[i]-'0',i++;
			if (w == mx) printf("Yes\n");
			else printf("No\n");
		}
	}
}

#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>
#define f0(i,x) for(int i=0;i<=x;++i)
#define f1(i,x) for(int i=1;i<=x;++i)
#define f2(i,a,b) for(int i=a;i<=b;++i)
#define d0(i,x) for(int i=x;i>=0;--i)
#define d1(i,x) for(int i=x;i;--i)
#define d2(i,a,b) for(int i=a;i>=b;--i)
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()){x=x*10+ch-'0';}
	return x*f;
}
using namespace std;
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
#define fi first
#define se second
const int N=100005;
vector<pii>g[N],h[N];
int T,n,m,k,p,x,y,z;
priority_queue<pii,vector<pii>,greater<pii> >hp;
int d[N];
bool vis[N];
inline void dijkstra1(){
	while(hp.size())hp.pop();
	memset(d,0x3f,sizeof d);
	memset(vis,0,sizeof vis);
	d[1]=0;hp.push(pii(0,1));
	while(hp.size()){
		int x=hp.top().se;hp.pop();
		if(vis[x])continue;vis[x]=1;
		vector<pii>::iterator it=g[x].begin(),ed=g[x].end();
		for(;it!=ed;++it){
			if(d[it->fi]>d[x]+it->se){
				d[it->fi]=d[x]+it->se;
				hp.push(pii(d[it->fi],it->fi));
			}
		}
	}
}
int f[N][55],ind[N][55];
int dfs(int x,int y){
	if(f[x][y])return f[x][y];
	vector<pii>::iterator it=h[x].begin(),ed=h[x].end();
	for(;it!=ed;++it){
		int xx=it->fi,yy=d[x]+y-it->se-d[xx];
		if(yy>=N)continue;
		f[x][y]+=dfs(xx,yy);
	}
	return f[x][y];
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(T=read();T--;){
		memset(f,0,sizeof f);
		n=read();m=read();
		k=read();p=read();
		f1(i,m){
			x=read();y=read();z=read();
			g[x].push_back(pii(y,z));
			h[y].push_back(pii(x,z));
		}
		dijkstra1();
		/*spfa();
		ll ans=0;
		f0(i,k)ans+=f[n][i],ans%=p;*/
		ll ans=0;
		f[1][0]=1;
		f0(i,k)ans+=dfs(n,i),ans%=p;
		printf("%lld\n",ans);
	}
	return 0;
}

#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<algorithm>
#define f0(i,x) for(int i=0;i<=x;++i)
#define f1(i,x) for(int i=1;i<=x;++i)
#define f2(i,a,b) for(int i=a;i<=b;++i)
#define d0(i,x) for(int i=x;i>=0;--i)
#define d1(i,x) for(int i=x;i;--i)
#define d2(i,a,b) for(int i=a;i>=b;--i)
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()){x=x*10+ch-'0';}
	return x*f;
}
typedef long long ll;
typedef double db;
#define fi first
#define se second
ll a,b,c;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();
	b=read();
	if(a>b)a^=b^=a;
	c=b;
	while(a*c%b+(a-1)<b)--c;
	printf("%lld\n",b-a*c%b+a*(c-1));
	return 0;
}

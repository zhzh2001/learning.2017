#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define f0(i,x) for(int i=0;i<=x;++i)
#define f1(i,x) for(int i=1;i<=x;++i)
#define f2(i,a,b) for(int i=a;i<=b;++i)
#define d0(i,x) for(int i=x;i>=0;--i)
#define d1(i,x) for(int i=x;i;--i)
#define d2(i,a,b) for(int i=a;i>=b;--i)
inline int read(){
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()){x=x*10+ch-'0';}
	return x*f;
}
using namespace std;
typedef long long ll;
typedef double db;
//typedef pair<int,int> pii;
#define fi first
#define se second
const int N=105;
int T,n,opt,tim;
vector<int> g;
bool flg,vis[300];
struct P{
	int opt,bl,l,r;
}p[N];
int tp[N],w[N];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	f1(z,T){
		opt=tim=flg=0;
		memset(p,0,sizeof p);
		memset(vis,0,sizeof vis);
		memset(tp,0,sizeof tp);
		memset(w,0,sizeof w);
		g.clear();
		n=read();
		char ch=getchar();
		while(ch!='(')ch=getchar();
		ch=getchar();
		if(ch=='n')
		{
			opt=1;
			tim=read();
		}else{
			opt=0;
			while(ch>='0'&&ch<='9'){
				tim=tim*10+ch-'0';
				ch=getchar();
			}
		}
		f1(i,n){
			char s[10];
			scanf("%s",s);
			if(s[0]=='F'){
				p[i].opt=0;
				scanf("%s",s);
				p[i].bl=s[0];
				char ch=getchar();
				while((ch<'0'||ch>'9')&&(ch!='n'))ch=getchar();
				if(ch=='n'){
					p[i].l=-1;
					ch=getchar();
				}
				else{
					while(ch>='0'&&ch<='9'){
						p[i].l=p[i].l*10+ch-'0';
						ch=getchar();
					}
				}
				while((ch<'0'||ch>'9')&&(ch!='n'))ch=getchar();
				if(ch=='n'){
					p[i].r=-1;
					ch=getchar();
				}
				else{
					while(ch>='0'&&ch<='9'){
						p[i].r=p[i].r*10+ch-'0';
						ch=getchar();
					}
				}
			}else p[i].opt=1;
		}
		int t=0;
		f1(i,n){
			if(p[i].opt==0){
				++t;
				if(vis[p[i].bl]){
					flg=1;
					break;
				}
				vis[p[i].bl]=1;
				g.push_back(p[i].bl);
			}else{
				--t;
				if(t<0){
					flg=1;
					break;
				}
				vis[g.back()]=0;
				g.pop_back();
			}
		}
		if(flg||t){
			puts("ERR");
			continue;
		}
		g.clear();
		g.push_back(0);
		f1(i,n){
			if(p[i].opt==0){
				g.push_back(i);
			}else{
				int x=g.back();
				g.pop_back();
				int y=g.back();
				if(p[x].l==-1){
					if(p[x].r!=-1)tp[x]=w[x]=0;
				}
				else{
					if(p[x].r==-1)tp[x]=1,++w[x];
					else if(p[x].l>p[x].r)tp[x]=w[x]=0;
				}
				tp[y]=max(tp[y],tp[x]);
				w[y]=max(w[y],w[x]);
			}
		}
		if(tp[0]==opt){
			if(opt==0||w[0]==tim)puts("Yes");
			else puts("No");
		}
		else puts("No");
	}
	return 0;
}

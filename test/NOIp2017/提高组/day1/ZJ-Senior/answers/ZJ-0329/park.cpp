#include<iostream>
#include<cstdio>
using namespace std;
int T,n,m,k,p,X,Y,Z,num,flag;
int inque[100010],q[100010],cnt[100010],dis[100010],ans[100010][51];
int head[100010],Next[200010],vet[200010],val[200010];
void add(int u,int v,int e){
	num++;
	Next[num]=head[u];
	head[u]=num;
	vet[num]=v;
	val[num]=e;
}
void SPFA(){
	int h=1,t=1;
	q[1]=inque[1]=cnt[1]=1;
	dis[1]=0;
	ans[1][0]=1;
	while (h<=t){
		int u=q[(h-1)%n+1];
		h++;
		for (int e=head[u];e;e=Next[e]){
			int v=vet[e];
			if (dis[u]+val[e]<dis[v]){
				int pre=dis[v];
				dis[v]=dis[u]+val[e];
				for (int i=k;i>=0;i--)
					if (i+pre-dis[v]<=k){
						ans[v][i+pre-dis[v]]=ans[v][i];
						ans[v][i]=0;
					}
				for (int i=k;i>=0;i--){
					ans[v][i]+=ans[u][i];
					if (ans[v][i]>=p) ans[v][i]-=p;
				}
				if (inque[v]==0){
					inque[v]=1;
					cnt[v]++;
					if (cnt[v]>n){printf("-1\n");flag=0;return;}
					t++;
					q[(t-1)%n+1]=v;
				}
			}else{
				if (dis[u]+val[e]==dis[v]){if (inque[v]==0){
					inque[v]=1;
					cnt[v]++;
					if (cnt[v]>n){printf("-1\n");flag=0;return;}
					t++;
					q[(t-1)%n+1]=v;
				}}
			if (dis[u]+val[e]<=dis[v]+k){
				for (int i=dis[u]+val[e]-dis[v];i<=k;i++){
					ans[v][i]+=ans[u][i-dis[u]-val[e]+dis[v]];
					if (ans[v][i]>=p) ans[v][i]-=p;
				}
			}
		}
		}
		inque[u]=0;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=n;i++){
			head[i]=inque[i]=cnt[i]=q[i]=0;
			dis[i]=1e9;
			for (int j=0;j<=k;j++) ans[i][j]=0;
		}
		num=0;
		for (int i=1;i<=m;i++){
			scanf("%d%d%d",&X,&Y,&Z);
			add(X,Y,Z);
		}
		flag=1;
		SPFA();
		//cout<<dis[n]<<endl;
		if (flag){
			long long Last=0;
			for (int i=0;i<=k;i++) Last=(Last+ans[n][i])%p;
			printf("%lld\n",Last);
		}
	}
	return 0;
}

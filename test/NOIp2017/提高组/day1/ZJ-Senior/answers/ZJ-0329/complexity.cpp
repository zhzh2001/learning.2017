#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int T,L,c,maxn,tot,tb,cnt,X,Y,a[30],b[110],flag,bo;
char s[10],z[110];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%s",s);
		int len=strlen(s);
		L=0;
		for (int i=0;i<len;i++) L=L*10+s[i]-'0';
		scanf("%s",s);
		len=strlen(s);
		c=-1;
		for (int i=0;i<len;i++) if (s[i]=='n'){c=0;break;}
		if (c==-1) c=0;
		else
			for (int i=0;i<len;i++)
				if (s[i]>='0'&&s[i]<='9')
					c=c*10+s[i]-'0';
		maxn=0;tot=0;cnt=0;flag=1;bo=0;
		memset(a,0,sizeof(a));
		memset(z,0,sizeof(z));
		memset(b,0,sizeof(b));
		for (int l=1;l<=L;l++){
			scanf("%s",s);
			//cout<<s<<endl;
			if (s[0]=='F'){
				//cout<<T<<endl;
				if (flag==0){
					scanf("%s",s);
					scanf("%s",s);
					scanf("%s",s);
					continue;
				}
				scanf("%s",s);
				if (a[s[0]-'0']){
					printf("ERR\n");
					scanf("%s%s",s,s);
					flag=0;continue;
				}else{
					a[s[0]-'0']=1;
				}
				z[++tot]=s[0];
				scanf("%s",s);
				X=Y=0;
				if ((s[0]>='0')&&(s[0]<='9')){
					len=strlen(s);
					for (int i=0;i<len;i++) X=X*10+s[i]-'0';
				}
				scanf("%s",s);
				if ((s[0]>='0')&&(s[0]<='9')){
					len=strlen(s);
					for (int i=0;i<len;i++) Y=Y*10+s[i]-'0';
				}
				if (Y&&X){
					if (Y<X) bo=1,tb=tot;
				}
				if ((X==0)&&Y) bo=1,tb=tot;
				if ((Y==0)&&X){if (bo==0) cnt++,b[tot]=1,maxn=max(maxn,cnt);}
			}
			if (s[0]=='E'){
				if (flag==0) continue;
				if (tot==0){
					printf("ERR\n");
					flag=0;continue;
				}
				if ((bo==0)&&b[tot]) cnt--;
				b[tot]=0;
				a[z[tot--]-'0']=0;
				if (tot<tb) tb=0,bo=0;
			}
		}
		if (flag){
			if (cnt!=0||tot!=0) printf("ERR\n");
			else if (c==maxn) printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}

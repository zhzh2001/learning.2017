#include<iostream>
#include<cstdio>
#include<queue>
#include<cstring>
#define N 400005
#define ll long long
using namespace std;
int read()
{
	int x=0;char c=getchar();
	while(c<'0'||c>'9')c=getchar();
	while(c>='0'&&c<='9')x=x*10+c-'0',c=getchar();
	return x;
}
struct T
{
	int x,y;
};
bool operator<(const T &a,const T &b)
{
	return a.y>b.y;
}
priority_queue<T>q;
int n,m,k,P,dis[N],vis[N],dit[N];
int fr[N],to[N],nxt[N],fst[N],len[N],l;
int _to[N],_nxt[N],_fst[N],_len[N],_l;
int To[N],Nxt[N],Fst[N],lll;
int dfn[N],pre[N],clk,bl[N],blk,inf[N],st[N],top;
void link(int x,int y,int z)
{
	to[++l]=y;fr[l]=x;len[l]=z;nxt[l]=fst[x];fst[x]=l;
	_to[++_l]=y;_len[_l]=z;_nxt[_l]=_fst[x];_fst[x]=_l;
}
void lk(int x,int y)
{
	To[++lll]=y;Nxt[lll]=Fst[x];Fst[x]=lll;
}
void dfs(int x)
{
	dfn[x]=pre[x]=++clk;
	st[++top]=x;
	for (int i=Fst[x];i;i=Nxt[i])
		if (!dfn[To[i]])
		{
			dfs(To[i]);
			pre[x]=min(pre[x],pre[To[i]]);
		}
		else
		{
			pre[x]=min(pre[x],pre[To[i]]);
		}
	if (dfn[x]==pre[x])
	{
		blk++;
		int tmp;
		if (st[top]!=x) tmp=1;else tmp=0;
		for (;st[top]!=x;top--)
			inf[st[top]]=tmp,bl[st[top]]=blk;
		inf[st[top]]=tmp;bl[st[top--]]=blk;
	}
}
int solve()
{
	blk=0;clk=0;top=0;
	memset(dfn,0,sizeof dfn);
	memset(bl,0,sizeof bl);;
	memset(inf,0,sizeof inf);
	memset(dit,0x3f,sizeof dit);
	memset(vis,0,sizeof vis);
	dit[n]=0;
	while(q.size())q.pop();
	q.push((T){n,0});
	while(q.size())
	{
		T x=q.top();q.pop();
		if (vis[x.x]) continue;
		vis[x.x]=1;
		for (int i=_fst[x.x];i;i=_nxt[i])
		if (!vis[_to[i]])
		{
			dit[_to[i]]=min(dit[_to[i]],dit[x.x]+_len[i]);
			q.push((T){_to[i],dit[_to[i]]});
		}
	}
	int ans=1000000000;
	for (int i=1;i<=n;i++)
	{
		if (!dfn[i])
			dfs(i);
		if (inf[i])
		ans=min(ans,dis[i]+dit[i]);
	}
	return ans;
}
#define Get(x,y) (((x)-1)*(k+1)+(y))
#define M 15000005
int TO[M],NXT[M],FST[M],lych,VIS[M];ll dp[M];
void add(int y,int x)
{
	TO[++lych]=y;NXT[lych]=FST[x];FST[x]=lych;
}
int DP(int x)
{
	if (VIS[x]) return dp[x];
	VIS[x]=1;dp[x]=0;
	for (int i=FST[x];i;i=NXT[i])
		dp[x]=dp[x]+DP(TO[i]);
	return dp[x]=dp[x]%P;
}
void work()
{
	memset(Fst,0,sizeof Fst);lll=0;
	memset(fst,0,sizeof fst);l=0;
	memset(FST,0,sizeof FST);lych=0;
	memset(_fst,0,sizeof _fst);_l=0;
	
	n=read();m=read();k=read();P=read();
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		x=read();y=read();z=read();
		link(x,y,z);
		if (z==0) lk(x,y);
	}
	//cerr<<"ok";
	memset(dis,0x3f,sizeof dis);
	memset(vis,0,sizeof vis);
	dis[1]=0;
	while(q.size())q.pop();
	q.push((T){1,0});
	while(q.size())
	{
		T x=q.top();q.pop();
		if (vis[x.x]) continue;
		vis[x.x]=1;
		for (int i=fst[x.x];i;i=nxt[i])
		if (!vis[to[i]])
		{
			dis[to[i]]=min(dis[to[i]],dis[x.x]+len[i]);
			q.push((T){to[i],dis[to[i]]});
		}
	}
	//cerr<<"ok";
	if (solve()<=dis[n]+k)
	{
		puts("-1");
		return;
	}
	//cerr<<"ok";
	memset(VIS,0,sizeof VIS);
	
	dp[Get(1,0)]=1;
	VIS[Get(1,0)]=1;
	
	for (int j=0;j<=k;j++)
		for (int i=1;i<=m;i++)
		{
			int tmp=j+len[i]+dis[fr[i]]-dis[to[i]];
			if (tmp<=k) add(Get(fr[i],j),Get(to[i],tmp));
		}
	ll Ans=0;
	for (int i=0;i<=k;i++)
		Ans=Ans+DP(Get(n,i));
	printf("%lld\n",(Ans%P+P)%P);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int Ts;
	Ts=read();
	while(Ts--)
		work();
}

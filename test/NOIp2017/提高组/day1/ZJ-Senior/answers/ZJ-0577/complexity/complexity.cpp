#include<iostream>
#include<cstdio>
#include<cstring>
#define N 10005
using namespace std;
int used[N],id[N],st[N];
char s[N],a[N],b[N],c[N],ti[N];
void work()
{
	int Mx=0,Ans=0,now=0,L,flag=1,top=0;
	memset(used,0,sizeof used);
	scanf("%d%s",&L,ti+1);
	int n=strlen(ti+1);
	if (ti[3]==1) Ans=0;
	else
	{
		for (int i=5;i<n;i++)
			Ans=Ans*10+ti[i]-'0';
	}
	for (int i=1;i<=L;i++)
	{
		scanf("%s",s+1);
		if (s[1]=='F')
		{
			scanf("%s%s%s",a+1,b+1,c+1);
			if (!flag) continue;
			id[++top]=a[1]-'a';
			if (used[id[top]]) {flag=0;continue;}
			used[id[top]]=1;
			if (b[1]!='n'&&c[1]=='n')
				st[top]=1;
			else
			{
				if (b[1]=='n') st[top]=-10000;
				else
				{
					int lb=strlen(b+1),lc=strlen(c+1),tb=0,tc=0;
					for (int j=1;j<=lb;j++)
						tb=tb*10+b[j]-'0';
					for (int j=1;j<=lc;j++)
						tc=tc*10+c[j]-'0';
					if (tb>tc) st[top]=-10000;
					else st[top]=0;
				}
			}
			now+=st[top];
			Mx=max(Mx,now);
		}
		else
		{
			if (top==0) {flag=0;continue;}
			if (!flag) continue;
			used[id[top]]=0;
			now-=st[top];
			top--;
		}
	}
	if (top) flag=0;
	if (!flag) puts("ERR");
	else puts(Mx==Ans?"Yes":"No");
	//cout<<Mx<<endl;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
		work();
}

#include<iostream>
#include<cstdio>
#define ll long long
using namespace std;
ll exgcd(ll x,ll y,ll &a,ll &b)
{
	if (y==0)
	{
		a=1;b=0;
		return x;
	}
	ll t=exgcd(y,x%y,b,a);
	b=b-x/y*a;
	return a;
}
ll x,y,a,b,u,v;
ll Get(ll x,ll y)
{
	exgcd(x,y,a,b);
	if (a*x+b*y==-1)
		a=-a,b=-b;
	u=-a;v=b;
	if (u<0)
	{
		ll tmp=1-u/y;
		u+=y*tmp;v+=x*tmp;
	}
	//cout<<v<<endl;
	return v-1;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&x,&y);
	if (x>y) swap(x,y);
	if (x==1)
	{
		puts("0");
		return 0;
	}
	ll Ans=Get(x,y)*y+Get(y,x)*x-1;
	printf("%lld\n",Ans);
	
}

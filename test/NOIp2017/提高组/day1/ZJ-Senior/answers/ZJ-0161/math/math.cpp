#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(ll x){
	wrote(x);puts("");
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll x=read(),y=read();
	wroteln(x*y-x-y);
	return 0;
}

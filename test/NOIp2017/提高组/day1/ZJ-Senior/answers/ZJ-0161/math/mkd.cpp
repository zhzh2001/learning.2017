#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(int x){
	wrote(x);puts("");
}
inline int gcd(int a,int b){return b?gcd(b,a%b):a;}
signed main()
{
	freopen("math.in","w",stdout);
	int x,y;
	do{
		x=rand()*rand()%1000000,y=rand()*rand()%10000000;
	}while(gcd(x,y)!=1);
	wrote(x);putchar(' ');wroteln(y);
	return 0;
}

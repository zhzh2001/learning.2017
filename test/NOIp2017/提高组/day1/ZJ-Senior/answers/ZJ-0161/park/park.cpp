#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
#define pa pair<int,int>
#define mp make_pair
#define fi first
#define se second
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(int x){
	wrote(x);puts("");
}
priority_queue<pa,vector<pa>,greater<pa> >q;
bool vis[100010],b[100010][51];
int n,m,K,MOD,dist[100010];
int f[100010][51];
int nedge=0,p[200010],c[200010],nex[200010],head[200010];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
queue<int>Q;
inline void spfa(){
	memset(dist,0x3f,sizeof dist);dist[1]=0;
	vis[1]=1;Q.push(1);
	while(!Q.empty()){
		int now=Q.front();Q.pop();
		for(int k=head[now];k;k=nex[k])if(dist[p[k]]>dist[now]+c[k]){
			dist[p[k]]=dist[now]+c[k];
			if(!vis[p[k]]){vis[p[k]]=1;Q.push(p[k]);}
		}vis[now]=0;
	}
}
inline void dij(){
	for(int i=0;i<=K;i++)memset(b[i],0,sizeof b[i]);
	for(int i=0;i<=K;i++)memset(f[i],0,sizeof f[i]);
	f[1][0]=1;q.push(mp(0,1));
	while(!q.empty()){
		pa now=q.top();q.pop();
		int C=now.fi-dist[now.se];
		if(b[now.se][C])continue;b[now.se][C]=1;
		for(int k=head[now.se];k;k=nex[k])if(now.fi+c[k]<=dist[p[k]]+K){
			if(now.fi+c[k]>dist[n]+K)continue;
			int D=now.fi+c[k]-dist[p[k]];f[p[k]][D]=(f[p[k]][D]+f[now.se][C])%MOD;
			q.push(pa(now.fi+c[k],p[k]));
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while(T--){
		nedge=0;memset(nex,0,sizeof nex);memset(head,0,sizeof head);
		n=read();m=read();K=read();MOD=read();
		bool flag=1;
		for(int i=1;i<=m;i++){
			int x=read(),y=read(),z=read();
			if(z==0)flag=0;
			addedge(x,y,z);
		}
		spfa();
		int ans=0;
		dij();
		for(int i=0;i<=K;i++)ans=(ans+f[n][i])%MOD;
		wroteln(ans);
	}
	return 0;
}

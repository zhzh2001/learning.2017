#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <set>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <ctime>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(int x){
	wrote(x);puts("");
}
map<char,bool>mp;
char s[1010],c[1010],k[1010];
int n,st[1010],p[1010],r[1010],top;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while(T--){
		memset(st,0,sizeof st);memset(p,0,sizeof p);
		memset(r,0,sizeof r);memset(k,0,sizeof k);
		mp.clear();
		n=read();
		scanf("%s",s+1);int l=strlen(s+1);
		top=0;bool flag=0;
		for(int i=1;i<=n;i++){
			scanf("%s",c);
			if(c[0]=='E'){
				if(!top)flag=1;
				if(flag)continue;
				mp[k[top]]=0;
				if(r[top])st[top-1]=max(st[top-1],0);
				else st[top-1]=max(st[top-1],st[top]+p[top]);
				top--;
			}else{
				if(flag){
					scanf("%s",c);scanf("%s",c);scanf("%s",c);
					continue;
				}
				top++;st[top]=0;r[top]=0;
				scanf("%s",c);
				if(mp[c[0]])flag=1;
				mp[c[0]]=1;k[top]=c[0];
				scanf("%s",c);
				if(c[0]=='n'){
					scanf("%s",c);
					if(c[0]!='n')r[top]=1;
					p[top]=0;
				}else{
					int q=0,L=strlen(c);
					for(int i=0;i<L;i++)if(c[i]>='0'&&c[i]<='9')q=q*10+c[i]-'0';
					scanf("%s",c);
					if(c[0]=='n')p[top]=1;
					else{
						int Q=0;L=strlen(c);
						for(int i=0;i<L;i++)if(c[i]>='0'&&c[i]<='9')Q=Q*10+c[i]-'0';
						if(Q<q)r[top]=1;
						p[top]=0;
					}
				}
			}
		}
		if(flag||top>0){puts("ERR");continue;}
		for(int i=1;i<=l;i++)if(s[i]=='n')flag=1;
		if(!flag){
			if(st[0]==0)puts("Yes");
			else puts("No");
		}else{
			int q=0;
			for(int i=1;i<=l;i++)if(s[i]>='0'&&s[i]<='9')q=q*10+s[i]-'0';
			if(q==st[0])puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

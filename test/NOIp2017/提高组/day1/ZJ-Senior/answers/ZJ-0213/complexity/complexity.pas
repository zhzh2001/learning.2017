var
  t,n,i,j,l,k,r,max,p,b,f,tot:longint;
  s,st:ansistring;
  flag:boolean;
  r1:array[1..10000]of longint;
  ch1,ch2,x,y:array[1..10000]of char;
  ch:char;
begin
  assign(input,'complexity.in');assign(output,'complexity.out');
  reset(input);rewrite(output);
  readln(t);
  for i:=1 to t do
  begin
    p:=1;st:='';flag:=true;b:=-maxlongint;n:=1;max:=0;k:=0;tot:=0;
    read(l,s);
    for j:=1 to length(s) do
    begin
      if s[j]='^' then
      begin
        for t:=j+1 to length(s)-1 do st:=st+s[t];
      end;
    end;
    val(st,k);
    for j:=1 to l do
    begin
      readln;
      read(ch1[j]);
      if ch1[j]='F' then
      begin
        inc(tot);
        read(ch);
        read(ch2[j],ch);
        read(x[j]);
        repeat
          read(ch);
        until ch=' ';
        read(y[j]);
        if y[j]='n' then inc(r1[n]);
      end;
      if (ch1[j]='E')and(j<>l) then inc(n);
    end;
    for j:=1 to n do if r1[j]>max then max:=r1[j];
    for j:=1 to n do r1[j]:=0;
    if x[1]='n' then b:=maxlongint;
    if ch1[l]<>'E' then begin writeln('ERR');flag:=false;end;
    for j:=1 to tot do
    begin
     for r:=j+1 to tot-1 do
      if ch2[j]=ch2[r] then begin writeln('ERR');flag:=false;break;end;
    end;
    if ((max=k)or((b=maxlongint)and(k=0)))and(flag=true) then begin writeln('YES');continue;end;
    if (max<>k)and(flag=true) then writeln('NO');
  end;
  close(input);close(output);
end.

var t, tt, i, j, hs, ans, xmfzd, kg, kg3, left, k, ordbl, ii, jj: longint;
    xmfzds, xmfzdss, s, hss, zis, zjs: string;
    p: boolean;
    bl: char;
    ss: array[1..200] of string;
    l, pd, sf, pdz, zq: array[1..200] of longint;
begin
    assign(input, 'complexity.in');
    assign(output, 'complexity.out');
    reset(input);
    rewrite(output);

    readln(t);
    for tt := 1 to t do
    begin
        ans := 0;
        readln(s);
        kg := pos(' ', s);
        hss := copy(s, 1, kg - 1);
        val(hss, hs);
        xmfzds := copy(s, kg + 1, length(s) - kg);
        if xmfzds[3] = '1' then
            xmfzd := 0 else
            begin
                xmfzdss := copy(xmfzds, 5, length(xmfzds) - 5);
                val(xmfzdss, xmfzd);
            end;
        p := true;
        left := 0;
        fillchar(l, sizeof(l), 0);
        fillchar(pd, sizeof(pd), 0);
        for i := 1 to hs do
        begin
            readln(ss[i]);
            if ss[i][1] = 'F' then
            begin
                inc(left);
                l[left] := i;
            end else
            begin
                if left <= 0 then
                begin
                    p := false;
                end else
                begin
                    pd[i] := l[left];
                    pd[l[left]] := i;
                    dec(left);
                    if left = 0 then
                        pd[i] := 0;
                end;
            end;
        end;
        fillchar(sf, sizeof(sf), 0);
        fillchar(zq, sizeof(zq), 0);
        if (left <> 0) or (p = false) then
            writeln('ERR') else
            begin
                k := 0;
                i := 1;
                while i <= hs do
                begin
                    if ss[i][1] = 'F' then
                    begin
                        bl := ss[i][3];
                        ordbl := ord(bl) - ord('a') + 1;
                        if sf[ordbl] = 1 then
                        begin
                            p := false;
                            break;
                        end else
                        begin
                            sf[ordbl] := 1;
                            pdz[pd[i]] := ordbl;
                        end;
                        if zq[i] = 0 then
                        begin
                          zis := '';
                          for j := 5 to 7 do
                            if ss[i][j] = ' ' then
                                break else
                                zis := zis + ss[i][j];
                          if zis[1] = 'n' then
                            ii := 0 else
                            val(zis, ii);
                          kg3 := j;
                          zjs := '';
                          for j := kg3 + 1 to length(ss[i]) do
                            zjs := zjs + ss[i][j];
                          if zjs[1] = 'n' then
                            jj := 0 else
                            val(zjs, jj);
                          if (ii = 0) then
                          begin
                            if (jj > 0) then
                                for j := i + 1 to pd[i] - 1 do
                                    zq[j] := 1;
                          end else
                          begin
                            if (jj = 0) then
                                inc(k) else
                                begin
                                    if jj < ii then
                                        for j := i + 1 to pd[i] - 1 do
                                            zq[j] := 1;
                                end;
                          end;
                        end;
                        inc(i);
                    end else
                    begin
                        sf[pdz[i]] := 0;
                        if pd[i] = 0 then
                        begin
                            if k > ans then
                                ans := k;
                            k := 0;
                        end;
                        inc(i);
                    end;
                end;
                if p = false then
                    writeln('ERR') else
                    if ans = xmfzd then
                        writeln('Yes') else
                        writeln('No');
            end;

    end;

    close(input);
    close(output);
end.

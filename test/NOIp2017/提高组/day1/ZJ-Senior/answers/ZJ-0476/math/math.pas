var a, b, ans: int64;
begin
    assign(input, 'math.in');
    assign(output, 'math.out');
    reset(input);
    rewrite(output);

    readln(a, b);
    ans := a * b - a - b;
    writeln(ans);

    close(input);
    close(output);
end.
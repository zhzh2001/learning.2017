var n, m, k, p, ans, t, tt, i: longint;
    a, b, c: array[1..200000] of longint;
begin
    assign(input, 'park.in');
    assign(output, 'park.out');
    reset(input);
    rewrite(output);

    readln(t);
    for tt := 1 to t do
    begin
        readln(n, m, k, p);
        ans := 0;
        for i := 1 to m do
        begin
            readln(a[i], b[i], c[i]);
            if c[i] <= k then
                inc(ans);
        end;
        if ans = m then
            writeln(-1) else
            writeln((ans div 2) mod p);

    end;

    close(input);
    close(output);
end.

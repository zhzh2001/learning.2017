#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <queue>
#include <algorithm>
typedef long long LL;
using namespace std;
const int N=100010,INF=1e9+7;

int n,m,K,p;

int ds[N];
vector<int> G[N],W[N];

void Add(int a,int b,int c){
	G[a].push_back(b);
	W[a].push_back(c);
	return;
}

void Reset(){
	for (int i=1;i<=n;++i) G[i].clear(),W[i].clear();
	memset(ds,0,sizeof ds);
	return;
}


struct XY{
	int id,dis,cnt;
}D[N*50];
struct cmp{
	bool operator ()(XY &a,XY &b){
		return a.dis > b.dis;
	}
};
priority_queue<XY,vector<XY>,cmp> Q;
bool flag[N*50];


void Dijkstra(int *d,int s){
	for (int i=1;i<=n;++i) D[i]=(XY){i,INF,0};
	memset(flag,0,sizeof flag);
	D[s].dis=0;
	D[s].cnt=1;
	Q.push(D[s]);
	
	while (!Q.empty()){
		XY Re=Q.top(); Q.pop();
		int x=Re.id,dis=Re.dis,cnt=Re.cnt;
		if (flag[x]) continue;
		flag[x]=1;
		for (int i=0;i<(int)G[x].size();++i){
			int v=G[x][i],w=W[x][i];
			if (D[v].dis > dis+w){
				D[v].dis=dis+w;
				D[v].cnt=cnt;
				Q.push(D[v]);
			}else if (D[v].dis == dis+w){
				D[v].cnt+=cnt;
				D[v].cnt%=p;
				Q.push(D[v]);
			}
		}
	}
	for (int i=1;i<=n;++i) d[i]=D[i].dis;
	return;
}


void Dijkstra1(){
	while (!Q.empty()) Q.pop();
	for (int i=1;i<=n;++i) D[i]=(XY){i,INF,0};
	memset(flag,0,sizeof flag);
	D[1].dis=0;
	Q.push(D[1]);
	
	while (!Q.empty()){
		XY Re=Q.top(); Q.pop();
		int x=Re.id,dis=Re.dis;
		if (flag[x]) continue;
		flag[x]=1;
		for (int i=0;i<(int)G[x].size();++i){
			int v=G[x][i],w=W[x][i];
			if (D[v].dis > dis+w){
				D[v].dis = dis+w;
				Q.push(D[v]);
			}
		}
	}
	return;
}



void Dijkstra2(){
	while (!Q.empty()) Q.pop();
	for (int i=1;i<=n;++i)
		for (int j=0;j<=K;++j)
			D[(i-1)*(K+1)+j]=(XY){(i-1)*(K+1)+j,ds[i]+j,0};
	memset(flag,0,sizeof flag);
	D[0].cnt=1;
	Q.push(D[0]);
	
	while (!Q.empty()){
		XY Re=Q.top(); Q.pop();
		int x=Re.id/(K+1)+1,k=Re.id-(x-1)*(K+1);
		int cnt=Re.cnt,dis=Re.dis;
		
		if (flag[Re.id]) continue;
		flag[Re.id]=1;
		
		for (int i=0;i<(int)G[x].size();++i){
			int v=G[x][i],w=W[x][i];
			int nk=dis+w-ds[v];
			int nxt=(v-1)*(K+1)+nk;
			if (D[nxt].dis > dis+w){
				D[nxt].cnt = cnt;
				Q.push(D[nxt]);
			}else if (D[nxt].dis==dis+w){
				D[nxt].cnt+=cnt;
				D[nxt].cnt%=p;
				Q.push(D[nxt]);
			}
		}
	}
	return;
}


int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&K,&p);
		Reset();
		
		for (int i=1;i<=m;++i){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			Add(x,y,z);
		}
		
		if (K==0){
			Dijkstra(ds,1);
			printf("%d\n",D[n].cnt);
			continue;
		}
		Dijkstra1();
		for (int i=1;i<=n;++i) ds[i]=D[i].dis;
		
		Dijkstra2();
		int Ans=0;
		for (int i=0;i<=K;++i) (Ans+=D[(n-1)*(K+1)+i].cnt)%=p;
		printf("%d\n",Ans);
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <queue>
#include <algorithm>
typedef long long LL;
using namespace std;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	LL a,b;
	scanf("%lld%lld",&a,&b);
	LL d=a-1;
	printf("%lld\n",-1+(b-1)*d);
	
	fclose(stdin);
	fclose(stdout);	
	return 0;
}

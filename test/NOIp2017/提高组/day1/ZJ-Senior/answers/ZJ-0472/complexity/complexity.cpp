#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <queue>
#include <algorithm>
typedef long long LL;
using namespace std;
const int INF=1e9+7,N=110;


int cur[N],sta[N],sta2[N],usd[520];

string type,forkyou;
string s[110];

int Change1(string A){
	if (A[4]=='n') return -1;
	int d=A[4]-'0',k=5;
	while (A[k]>='0' && A[k]<='9'){
		d=d*10+A[k]-'0';
		++k;
	}
	return d;
}
int Change2(string A){
	int le=A.length();
	if (A[le-1]=='n') return -1;
	int k=le-1;
	while (A[k-1]!=' ') --k;
	int d=0;
	while (k<=le-1){
		d=d*10+A[k]-'0';
		++k;
	}
	return d;
}
int Change3(string A){
	int le=A.length();
	int d=0;
	for (int i=4;i<=le-2;++i) d=d*10+A[i]-'0';
	return d;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int T; scanf("%d",&T);
	while (T--){
		int L,P=0,Ans=0; scanf("%d",&L);
		cin >>type;
		if (cin.peek()=='\n' || cin.peek()=='\r') getline(cin,forkyou);
		if (type.length()==4) P=0;
			else P=Change3(type);
		
		for (int i=1;i<=L;++i) getline(cin,s[i]);
		
		int bo=0,Frog=0;
		memset(usd,0,sizeof usd);
		memset(sta,0,sizeof sta);
		memset(sta2,0,sizeof sta2);
		memset(cur,0,sizeof cur);
		int top=0,top2=0;
		for (int i=1;i<=L;++i){
			if (s[i][0]=='F'){
				++bo;
				int bian=s[i][2]-'a';
				if (usd[bian]){
					Frog=1; break;
				}
				++usd[bian];
				sta[++top]=bian;
				sta2[++top2]=i;
			}else{
				--bo;
				if (bo < 0){
					Frog=1; break;
				}
				int bian=sta[top--];
				--usd[bian];
				int lll=sta2[top2--];
				cur[lll]=i;
			}
		}
		if (bo!=0) Frog=1;
		if (Frog){
			puts("ERR"); continue;
		}
		
		
		memset(sta,0,sizeof sta);
		top=0;
		int nxt=1,p=0;
		while (nxt<=L){
			if (s[nxt][0]=='F'){
				int x=Change1(s[nxt]);
				int y=Change2(s[nxt]);
				if (x==-1 && y!=-1){
					nxt=cur[nxt]+1;
					continue;
				}
				if (x!=-1 && y!=-1 && x>y){
					nxt=cur[nxt]+1;
					continue;
				}
				if (x!=-1 && y==-1){
					++p; ++nxt;
					Ans=max(Ans,p);
					sta[++top]=1;
					continue;
				}else{
					++nxt;
					sta[++top]=0;
					continue;
				}
			}else{
				if (sta[top--]==1) --p;
				++nxt;
			}
		}
		
		if (Ans==P) puts("Yes");
			else puts("No");
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


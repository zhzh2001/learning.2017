#pragma GCC optimize("O2")
#include <cstdio>
#include <algorithm>
#include <queue>
#include <cstring>
using namespace std;
const int N=1e5,M=2e5;
struct edge {
	int v,w,nx;
} e[M];
int d[N],g[N],f[N];
bool inq[N];
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t;
	scanf("%d",&t);
	while (t--) {
		memset(g,0xff,sizeof g);
		memset(f,0x00,sizeof f);
		int n,m,k,p;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=0;i<m;i++) {
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			a--,b--;
			e[i]=(edge){b,c,g[a]};
			g[a]=i;
		}
		memset(d,0x3f,sizeof d);
		queue<int> q;
		q.push(0);
		inq[0]=true;
		d[0]=0;
		f[0]=1;
		while (!q.empty()) {
			int u=q.front();
			q.pop();
			inq[u]=false;
			for (int i=g[u];i!=-1;i=e[i].nx) {
				int v=e[i].v,t=d[u]+e[i].w;
				if (t<d[v]) {
					d[v]=t;
					f[v]=f[u];
					if (!inq[v]) {
						q.push(v);
						inq[v]=true;
					}
				} else if (d[v]==t) {
					f[v]=(f[v]+f[u])%p;
				}
			}
		}
		printf("%d\n",f[n-1]);
	}
}

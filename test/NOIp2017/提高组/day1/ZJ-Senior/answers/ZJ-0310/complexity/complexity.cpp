#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
char c[10],sx[10],sy[10];
bool f[100];
int a[1000],b[1000];
char it[1000];
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	while (t--) {
		memset(f,0x00,sizeof f);
		int l;
		scanf("%d %s",&l,c);
		int d=0;
		b[0]=0;
		bool err=false;
		while (l--) {
			char o[2];
			scanf("%s", o);
			if (o[0]=='F') {
				b[++d]=0;
				scanf("%s%s%s",it+d,sx,sy);
				if (f[it[d]-'a']) err=true;
				f[it[d]-'a']=true;
				int x=100,y=100;
				if (sx[0]!='n') sscanf(sx,"%d",&x);
				if (sy[0]!='n') sscanf(sy,"%d",&y);
				if (y<x) a[d]=-1;
				else if (y==100&&x<100) a[d]=1;
				else a[d]=0;
			} else {
				if (a[d]!=-1) b[d-1]=max(b[d-1],b[d]+a[d]);
				f[it[d]-'a']=false;
				d--;
			}
		}
		if (d!=0||err) puts("ERR");
		else {
			int ans=0;
			if (strlen(c)>4) {
				sscanf(c+4,"%d",&ans);
			}
			puts(ans==b[0]?"Yes":"No");
		}
	}
}

#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

bool vis[299];
char stk[109];
int top=-1;
string prg[109];
int ks[109];
int pos=-1;
int l;

int readcpl(){
	char c=getchar();
	while(c!='O') c=getchar();
	getchar();
	c=getchar();
	if(c=='1'){
		getchar();
		return 0;
	}else{
		getchar();
		int ret;
		scanf("%d",&ret);
		getchar();
		return ret;
	}
}

int dfs(){
	int ret=0;
	while(pos<l-1){
		++pos;
		if(ks[pos]==0) dfs();
		else if(ks[pos]==1) ret=max(ret,dfs());
		else if(ks[pos]==999) ret=max(ret,dfs()+1);
		else if(ks[pos]==-1) return ret;
	}
	return ret;
}

bool sp(string s){
	for(int i=0;i<s.length();++i) if(s[i]!=' ' && s[i]!='\n' && s[i]!='\r') return false;
	return true;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		memset(vis,0,sizeof(vis));
		scanf("%d",&l);
		int cpl=readcpl();
//		printf("!%d\n",cpl);
		for(int i=0;i<l;++i){
			do{
				getline(cin,prg[i]);
			}while(sp(prg[i]));
//			cerr<<prg[i]<<"--\n";
		}
		top=-1;
		int total=0;
		bool er=false;
		for(int i=0;i<l;++i){
			if(prg[i][0]=='F'){
				++total;
				if(vis[prg[i][2]]){
					er=true;
					break;
				}
				vis[prg[i][2]]=true;
				stk[++top]=prg[i][2];
			}
			else{
				--total;
				if(total<0){
					er=true;
					break;
				}
				vis[stk[top]]=false;
				top--;
			}
		}
		if(total!=0) er=true;
		if(er){
			printf("ERR\n");
			continue;
		}
//		cout<<"done\n";
		for(int i=0;i<l;++i){
//			cout<<prg[i][0]<<"\n";
			if(prg[i][0]=='F'){
				int j=4;
				bool isn1=false;
				int num1=0;
				while(prg[i][j]==' ') ++j;
				while(prg[i][j]!=' '){
					if(prg[i][j]=='n') isn1=true;
					num1=num1*10+prg[i][j]-'0';
					++j;
				}
				bool isn2=false;
				int num2=0;
				while(prg[i][j]==' ') ++j;
				while(prg[i][j]!=' ' && prg[i][j]!='\0'){
					if(prg[i][j]=='n') isn2=true;
					num2=num2*10+prg[i][j]-'0';
					++j;
				}
				if(isn1 && isn2) ks[i]=1;
				if(isn1 && !isn2) ks[i]=0;
				if(!isn1 && isn2) ks[i]=999;
				if(!isn1 && !isn2){
					if(num1<=num2) ks[i]=1;
					else ks[i]=0;
				}
			}else{
				ks[i]=-1;
			}
		}
//		for(int i=0;i<l;++i) cout<<ks[i]<<" ";
//		cout<<"\n";
//		cout<<"done\n";
		int ans;
		pos=-1;
		ans=dfs();
		if(ans==cpl){
			printf("Yes\n");
		}else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

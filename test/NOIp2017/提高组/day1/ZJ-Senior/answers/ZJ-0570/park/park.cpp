#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>
using namespace std;

int dis[1009];
int dp[1009];
bool vis[1009];
struct node{
	int n;
	int v;
	node(int a,int b){n=a;v=b;}
	bool operator <(const node &a)const{return v>a.v;}
};
priority_queue<node> que;

struct edge{
	int q;
	int v;
	edge(int a,int b){q=a;v=b;}
};
vector<edge> h[1009];

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		int n,m,k,mod;
		scanf("%d %d %d %d",&n,&m,&k,&mod);
		for(int i=1;i<=n;++i) h[i].clear();
		for(int i=0;i<m;++i){
			int a,b,c;
			scanf("%d %d %d",&a,&b,&c);
			h[a].push_back(edge(b,c));
		}
		
//		for(int i=1;i<=n;++i){
//			for(int j=0;j<h[i].size();++j){
//				printf("%d %d %d\n",i,h[i][j].q,h[i][j].v);
//			}
//		}
		
		memset(dis,0x3f,sizeof(dis));
		memset(dp,0,sizeof(dp));
		memset(vis,0,sizeof(vis));
		dis[1]=0;
		dp[1]=1;
		que.push(node(1,0));
		while(!que.empty()){
			int p=que.top().n;
			que.pop();
			vis[p]=true;
			for(int i=0;i<h[p].size();++i){
				int q=h[p][i].q,v=h[p][i].v;
				if(vis[q]) continue;
//				printf("%d:%d %d %d\n",p,dis[p],v,dis[q]);
				if(dis[q]>dis[p]+v){
//					printf("!!%d,%d:%d %d %d\n",p,q,dis[p],v,dis[q]);
					dis[q]=dis[p]+v;
					dp[q]=1;
					que.push(node(q,dis[q]));
				}else if(dis[q]==dis[p]+v){
					dp[q]=(dp[q]+dp[p])%mod;
				}
			}
		}
//		for(int i=1;i<=n;++i) printf("%d ",dp[i]);
		printf("%d\n",dp[n]);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

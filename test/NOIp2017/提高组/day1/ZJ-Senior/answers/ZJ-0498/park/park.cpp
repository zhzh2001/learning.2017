#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
using namespace std;
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),c<'0');
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>='0');
}
const int N=(int)1e5+5;
const int M=(int)2e5+5;
bool mark[N],Useless[N],CIR;
int n,m,k,P,dp[55][N],Head[N],dis[N],tot,cnt[N];
int deg[N],Que[N],L,R;
vector<int>G[N],rG[N];
struct Node{int to,w,nxt;}Edge[M];
struct node{
	int x,d;
	bool operator < (const node &_)const{
		return d>_.d;
	}
};
priority_queue<node>Q;
inline void Addedge(int a,int b,int c){
	Edge[tot]=(Node){b,c,Head[a]};Head[a]=tot++;
	rG[b].push_back(a);
}
void Input(){
	tot=0;
	Rd(n),Rd(m),Rd(k),Rd(P);
	memset(Head,-1,sizeof(Head));
	while(m--){
		int a,b,c;
		Rd(a),Rd(b),Rd(c);
		Addedge(a,b,c);
	}
}
inline bool Check(int &a,int b){
	if(a==-1||b<a)return a=b,true;
	return false;
}
void Delpoints(){
	L=0,R=-1;Que[++R]=1;
	for(int i=1;i<=n;i++)mark[i]=false;
	mark[1]=true;
	while(L<=R){
		int now=Que[L++];
		cnt[now]++;
		for(int i=Head[now];~i;i=Edge[i].nxt){
			int to=Edge[i].to;
			if(mark[to])continue;
			mark[to]=true;
			Que[++R]=to;
		}
	}
	L=0,R=-1;Que[++R]=n;
	for(int i=1;i<=n;i++)mark[i]=false;
	mark[n]=true;
	while(L<=R){
		int now=Que[L++];
		cnt[now]++;
		for(int i=0;i<(int)rG[now].size();i++){
			int to=rG[now][i];
			if(mark[to])continue;
			mark[to]=true;
			Que[++R]=to;
		}
	}
	for(int i=1;i<=n;i++)
		if(cnt[i]!=2)Useless[i]=true;
}
void Dijkstra(){
	for(int i=1;i<=n;i++)dis[i]=-1,mark[i]=false;
	while(!Q.empty())Q.pop();
	Q.push((node){1,dis[1]=0});
	while(!Q.empty()){
		int now=Q.top().x;Q.pop();
		if(mark[now])continue;
		mark[now]=true;
		for(int i=Head[now];~i;i=Edge[i].nxt){
			int to=Edge[i].to;
			if(Check(dis[to],dis[now]+Edge[i].w))Q.push((node){to,dis[to]});
		}
	}
}
void Topo(){
	for(int i=1;i<=n;i++)deg[i]=0;
	for(int i=1;i<=n;i++){
		G[i].clear();
		if(Useless[i])continue;
		for(int j=Head[i];~j;j=Edge[j].nxt){
			int to=Edge[j].to;
			if(Useless[to])continue;
			if(dis[i]+Edge[j].w==dis[to])G[i].push_back(to),deg[to]++;
		}
	}
	L=1,R=0;
	for(int i=1;i<=n;i++)
		if(!deg[i]&&!Useless[i])Que[++R]=i;
	while(L<=R){
		int now=Que[L++];
		for(int i=0;i<(int)G[now].size();i++){
			int to=G[now][i];
			deg[to]--;
			if(!deg[to])Que[++R]=to;
		}
	}
	for(int i=1;i<=n;i++)
		if(deg[i])CIR=true;
}
inline void Add(int &a,int b){
	a+=b;
	if(a>=P)a-=P;
}
void DP(){
	for(int i=0;i<=k;i++)
		for(int j=1;j<=n;j++)
			dp[i][j]=0;
	dp[0][1]=1%P;
	for(int i=0;i<=k;i++){
		for(int j=1;j<=R;j++){
			int now=Que[j];
			for(int t=0;t<(int)G[now].size();t++){
				int to=G[now][t];
				Add(dp[i][to],dp[i][now]);
			}
		}
		for(int j=1;j<=n;j++){
			if(Useless[j])continue;
			for(int t=Head[j];~t;t=Edge[t].nxt){
				int to=Edge[t].to;
				if(Useless[to])continue;
				if(dis[j]+Edge[t].w==dis[to])continue;
				if(dis[j]+i+Edge[t].w-dis[to]<=k)Add(dp[dis[j]+i+Edge[t].w-dis[to]][to],dp[i][j]);
			}
		}
	}
	int rs=0;
	for(int i=0;i<=k;i++)
		Add(rs,dp[i][n]);
	printf("%d\n",rs);
}
void Init(){
	for(int i=1;i<=n;i++){
		Useless[i]=false,cnt[i]=0;
		rG[i].clear();
	}
	CIR=false;
}
void solve(){
	Delpoints();
	Dijkstra();
	Topo();
	if(CIR)puts("-1");
	else DP();
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;Rd(T);
	while(T--){
		Init();
		Input();
		solve();
	}
	return 0;
}

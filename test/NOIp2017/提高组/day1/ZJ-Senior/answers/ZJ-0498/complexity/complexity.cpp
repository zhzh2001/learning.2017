#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
using namespace std;
const int M=105;
struct Node{
	int op;
	char nm;
	int st;
}Q[M];
int L,top,OStk[M];
set<char>Vars;
char Ans[10],Stk[M];
char str1[5],str2[5];
void Input(){
	scanf("%d %s",&L,Ans+1);
	for(int i=1;i<=L;i++){
		scanf("%s",str1);
		if(str1[0]=='F'){
			Q[i].op=1;
			scanf("%s",str1);
			Q[i].nm=str1[0];
			scanf("%s %s",str1,str2);
			if(str1[0]!='n'&&str2[0]=='n')Q[i].st=1;
			else if(str1[0]=='n'&&str2[0]=='n')Q[i].st=0;
			else if(str1[0]=='n'&&str2[0]!='n')Q[i].st=-1;
			else{
				int len1=strlen(str1),len2=strlen(str2);
				int num1=0,num2=0;
				for(int j=0;j<len1;j++)
					num1=num1*10+(str1[j]^48);
				for(int j=0;j<len2;j++)
					num2=num2*10+(str2[j]^48);
				if(num1<=num2)Q[i].st=0;
				else Q[i].st=-1;
			}
		}else Q[i].op=-1;
	}
}
void solve(){
	int sum=0;
	bool can=true;
	for(int i=1;i<=L;i++){
		sum+=Q[i].op;
		if(sum<0)can=false;
	}
	if(sum!=0)can=false;
	if(!can){
		puts("ERR");
		return;
	}
	Vars.clear();
	int mx=0,now=0,cntbad=0;top=0;
	for(int i=1;i<=L;i++){
		if(Q[i].op==1){
			now+=Q[i].st;
			if(Q[i].st<0)cntbad++;
			if(!cntbad)mx=max(mx,now);
			if(Vars.find(Q[i].nm)!=Vars.end()){
				puts("ERR");
				return;
			}
			Vars.insert(Q[i].nm);
			Stk[++top]=Q[i].nm;
			OStk[top]=Q[i].st;
		}else{
			Vars.erase(Vars.find(Stk[top]));
			now-=OStk[top];
			if(OStk[top]<0)cntbad--;
			top--;
		}
	}
	if(mx==0){
		if(Ans[3]=='1')puts("Yes");
		else puts("No");
	}else{
		if(Ans[3]=='1')puts("No");
		else{
			int rs=0;
			int len=strlen(Ans+1);
			for(int i=5;i<len;i++)
				rs=rs*10+(Ans[i]^48);
			puts(mx==rs?"Yes":"No");
		}
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		Input();
		solve();
	}
	return 0;
}

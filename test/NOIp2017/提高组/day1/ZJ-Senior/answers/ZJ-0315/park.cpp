#include<bits/stdc++.h>
#define N (100010)
typedef long long ll;
using namespace std;
ll n,m,t,k,p,min1=1000000000,ans;
typedef struct{
	int next;
	int qi;
}path;
vector<path> w[N];
typedef struct {
	int l;int w;
}po;
po q[10000001];
int l1=1,l2=0;
void sort1(int l3,int l4){
	int i=l3,j=l4,e=0;
	
	while(i<j){
		if(q[i].w>=q[j].w){
			swap(q[i],q[j]);
			if(e)e=0;else e=1;
		}
		if(e)j--;else i++;
	}
	if(l3<j-1)
	sort1(l3,j-1);
	if(j+1<l4)
	sort1(j+1,l4);
}

void bfs(){
	for(vector<path>::iterator i=w[q[l1].l].begin();i!=w[q[l1].l].end();i++){
		ll le=q[l1].w+(*i).qi;
		if(le<=min1+k){
			if((*i).next==n){
				ans=(ans+1)%p;
				min1=min(min1,le);
//				continue;
			}
			l2++;
			q[l2].w=le;
			q[l2].l=(*i).next;
		}
	}
	l1++;
	sort1(l1,l2);
	if(l1<=l2)
	bfs();
}


void f(){
	l1=1,l2=0;
	int flag=2;
	scanf("%d%d%d%d",&n,&m,&k,&p);
	for(int i=1;i<=n;i++)w[i].clear();
	for(int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if(!z)
		flag--;
		path e={y,z};
		w[x].push_back(e);
	}
	q[++l2].l=1;
	if(flag){
		bfs();
		cout<<ans<<endl;
	}
	else
	cout<<"-1"<<endl;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	for(int i=1;i<=t;i++)
	 f();
	 return 0;
}

#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <ctime>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int N,M;
int q[10000005];

int gcd(int a,int b)
{
	if (! b) return a;
	return gcd(b,a % b);
}

int main()
{
	srand(time(NULL));
	freopen("math.in","w",stdout);
	int N=rand() % 30 + 2; int M=rand() % 30 + 2;
	int D=gcd(N,M);
	N/=D; M/=D; 
	printf("%d %d\n",N,M);

	return 0;	
} 

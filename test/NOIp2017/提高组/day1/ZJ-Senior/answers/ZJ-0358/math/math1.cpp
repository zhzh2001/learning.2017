#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int N,M;
int q[100000005];

int gcd(int a,int b)
{
	if (! b) return a;
	return gcd(b,a % b);
}

int main()
{
	
	freopen("math.in","r",stdin);
	freopen("math1.out","w",stdout);
	scanf("%d%d",&N,&M);
			if (N > M)
			{
				swap(N,M);
			}
			int t=0; int w=-1;
			fo(i,1,N - 1)
			{
				q[++w]=i;
			}
			while (t <= w)
			{
				int u=q[t];
				int X1=u + N;
				int X2=u + M;
				if (X1 % N != 0 && X1 % M != 0)
				{
					q[++w]=X1;
				}
				if (X2 % N !=0 && X2 % M != 0)
				{
					q[++w]=X2;
				}
				t++;
			}
			printf("%d\n",q[t - 1]);

	return 0;	
}  

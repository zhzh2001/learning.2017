#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

ll N,M;

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&N,&M);
	if (N > M)
	{
		swap(N,M);
	}
	if (N == 1)
	{
		puts("0");
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	ll Start1=(N - 2) * (N + 1) + 1;
	ll Ans=(M - N - 1) * (N - 1) + Start1;
	cout<<Ans<<endl;
	fclose(stdin);
	fclose(stdout);
}

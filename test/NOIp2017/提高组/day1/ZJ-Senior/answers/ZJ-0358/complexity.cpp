#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

char s1[10005];
char s2[105][10005];
string Name1[105];
int Start1[100005],End1[100005];
bool vis[109005];
int s[100005];

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int _;
	scanf("%d",&_);	
	while (_--)
	{
		int L;
		scanf("%d ",&L); scanf("%s",s1 + 1);
		getchar();
		bool flag1=0;
		int Len1=strlen(s1 + 1);
		int tmp1=0;
		fo(i,1,Len1)
		{
			if (s1[i] == '^')
			{
				flag1=1;
			}
			if (flag1 && s1[i] >= '0' && s1[i] <= '9')
			{
				tmp1=tmp1 * 10 + s1[i] - 48;
			}
		}
		int F1=0;
		fo(i,1,L)
		{
			Name1[i]="";
			Start1[i]=0;
			End1[i]=0;
		}
		fo(i,1,L)
		{
			gets(s2[i]);
		}
		int What=1; int Ans=0;
		for (int i=1; i<=L; i++)
		{
			if (s2[i][0] == 'E')
			{
				if (F1 == 0)
				{
					What=-1;
					break;
				}
				int tmp=0;
				while (F1 && s2[i][0] == 'E')
				{
					if (End1[F1] - Start1[F1] >= 100)
					{
						tmp++;
					}
					if (End1[F1] - Start1[F1] < 0)
					{
						tmp=0;
					}
					Name1[F1]="";
					vis[F1]=0;
					Start1[F1]=0; End1[F1]=0;
					F1--;
					i++;
				}
				Ans=max(Ans,tmp);
				i--;
			}
			else
			{
				++F1; int Kong=0;
				int LenX=strlen(s2[i]);
				fo(j,0,LenX- 1)
				{
					if (s2[i][j] == ' ')
					{
						Kong++;
						continue;
					}
					if (Kong == 1)
					{
						Name1[F1]=Name1[F1] + s2[i][j];
					}
					if (Kong == 2)
					{
						if (s2[i][j] == 'n')
						{
							Start1[F1]=1000;
						}
						else if (s2[i][j] >= '0' && s2[i][j] <= '9')
						{
							Start1[F1]=Start1[F1] * 10 + s2[i][j] - 48;
						}
					}
					if (Kong == 3)
					{
						if (s2[i][j] == 'n')
						{
							End1[F1]=1000;
						}
						else if (s2[i][j] >= '0' && s2[i][j] <= '9')
						{
							End1[F1]=End1[F1] * 10 + s2[i][j] - 48;
						}
					}
				}
				if (End1[F1] - Start1[F1] >= 1000)
				{
					vis[F1]=1;
				}
				s[F1]=s[F1 - 1] + vis[F1];
			}
			fo(j,1,F1 - 1)
			{
				if (Name1[j] == Name1[F1])
				{
					What=-1;
					break;
				}
			}
			if (What == -1)
			{
				break;
			}
		}
		if (tmp1 > L / 2)
		{
			What=0;
		}
		if (Ans != tmp1)
		{
			What=0;
		}
		if (F1 != 0)
		{
			What=-1;
		}
		if (What == 0)
		{
			puts("No");
		}
		if (What == -1)
		{
			puts("ERR");
		}
		if (What == 1)
		{
			puts("Yes");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
} 

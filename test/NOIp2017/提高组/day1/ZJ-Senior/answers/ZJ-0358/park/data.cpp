#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <ctime>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int N,M;
int q[10000005];
int K,P;
bool flag[1005][1005];

int main()
{
	srand(time(NULL));
	freopen("park.in","w",stdout);
	puts("1");
	N=rand() % 50 + 50; M=rand() % (100 - N) + N; K=rand() % 10;  P=1e9 + 7;
	printf("%d %d %d %d\n",N,M,K,P);
	fo(i,1,N - 1)
	{
		printf("1 %d 1\n",i);
		flag[1][i]=1;
	}
	fo(i,1,M)
	{
		int u=rand() % N + 1; int v=rand() % M + 1;
		while (flag[u][v])
		{
			u=rand() % N + 1;v=rand() % M + 1;
		}
		flag[u][v]=1;
		printf("%d %d 1\n",u,v);
	}
	fclose(stdout);
	return 0;	
} 

#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int Min1,Ans;
int dis[100005];
int q[100005];
int vis[10005];
int vet[100005],val[100005],head1[10005],next[100005];
int Time1[10005];
int N,M,P,K;
int num;

void add(int u,int v,int cost)
{
	vet[++num]=v;
	val[num]=cost;
	next[num]=head1[u];
	head1[u]=num;
}

int SPFA1(int s)
{
	fo(i,1,N)
	{
		dis[i]=0;
		vis[i]=0;
	}
	vis[s]=1;
	int t=0; int w=0;
	q[t]=s;
	while (t <= w)
	{
		int u=q[t];
		for (int e=head1[u]; e != -1; e=next[e])
		{
			int V=vet[e];
			if (V == u) continue;
			int Cost=val[e];
			if (dis[V] < dis[u] + Cost)
			{
				dis[V]=dis[u] + Cost;
				if (! vis[V])
				{
					vis[V]=1;
					q[++w]=V;
				}
			}
		}
		vis[u]=0;
		t++;
	}
	return dis[N];
}

void dfs(int u,int fa,int now)
{
	if (now > Min1 + K)
	{
		return;
	}
	if (u == N && now <= Min1 + K)
	{
		Ans++;
		return;
	}
	for (int e=head1[u]; e != -1; e=next[e])
	{
		int V=vet[e];
		if (V == fa) continue;
		int Cost=val[e];
		dfs(V,u,now + Cost);
	}
}

int SPFA2(int s)
{
	fo(i,1,N)
	{
		dis[i]=1e9;
		vis[i]=0;
		Time1[i]=0;
	}
	dis[s]=0;
	vis[s]=1;
	int t=0; int w=0;
	q[t]=s;Time1[s]=1;
	while (t <= w)
	{
		int u=q[t];
		for (int e=head1[u]; e != -1; e=next[e])
		{
			int V=vet[e];
			if (V == u) continue;
			int Cost=val[e];
			if (dis[V] >= dis[u] + Cost)
			{
				dis[V]=dis[u] + Cost;
				if (! vis[V])
				{
					vis[V]=1;
					q[++w]=V;
					Time1[V]++;
					if (Time1[V] >= N)
					{
						return -1;
					}
				}
			}
		}
		vis[u]=0;
		t++;
	}
	return dis[N];
}


int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int _;
	scanf("%d",&_);
	while(_--)
	{
		scanf("%d%d%d%d",&N,&M,&K,&P);
		fo(i,1,N)
		{
			head1[i]=-1;
		}
		fo(i,1,M)
		{
			int u,v,cost;
			scanf("%d%d%d",&u,&v,&cost);
			add(u,v,cost);
		}
		Min1=SPFA2(1);
		if (Min1 == -1)
		{
			puts("-1");
			continue;
		}
		Ans=0;
		cl(vis,0);
		dfs(1,-1,0);
		cout<<Ans<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

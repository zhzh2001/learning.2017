#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 30;
int l;
int w,Maxw;
char s[110][4][N],c;
bool vis[N],flag;
int stk[N],top;

inline int Max(int x,int y){
	if(x > y) return x;
	return y;
}

int dfs(int ww,int o,int ii){
	if(o != -1) Maxw = Max(Maxw,ww);
	for(int i = ii;i <= l;i++){
		if(s[i][0][1] == 'F'){
			int p = s[i][1][1]-'a';
			if(vis[p] == true){
				flag = true;
				return l+1;
			}
			vis[p] = true;
			stk[++top] = p;
			int x = 0,q = 0,z = 0,y = 0;
			if(s[i][2][1] == 'n') q = 1;
			else{
				for(int j = 1;j <= strlen(s[i][2]+1);j++)
					x = x*10+s[i][2][j]-'0';
			}
			if(s[i][3][1] == 'n'){
				if(q == 1) q = 0;
				else q = 1;
			}
			else{
				for(int j = 1;j <= strlen(s[i][3]+1);j++)
					y = y*10+s[i][3][j]-'0';
				if(q == 1) z = -1;
				else if(y < x) z = -1;
			}
			if(o == -1) i = dfs(ww+q,o,i+1);
			else i = dfs(ww+q,z,i+1);
			if(flag) return l+1;
		}
		else{
			vis[stk[top]] = false;
			top--;
			return i;
		}
	}
	flag = true;
	return l+1;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T = read();
	while(T--){
		l = read();
		getchar();
		getchar();
		c = getchar();
		if(c == 'n') w = read(),getchar();
		else w = 0,getchar(),getchar();
		flag = false; Maxw = 0; top = 0;
		memset(vis,false,sizeof vis);
		for(int i = 1;i <= l;i++){
			scanf("%s",s[i][0]+1);
			if(s[i][0][1] == 'F'){
				scanf("%s",s[i][1]+1);
				scanf("%s",s[i][2]+1);
				scanf("%s",s[i][3]+1);
			}
		}
		for(int i = 1;i <= l;i++){
			if(s[i][0][1] == 'F'){
				int p = s[i][1][1]-'a';
				vis[p] = true;
				stk[++top] = p;
				int x = 0,q = 0,z = 0,y = 0;
				if(s[i][2][1] == 'n') q = 1;
				else{
					for(int j = 1;j <= strlen(s[i][2]+1);j++)
						x = x*10+s[i][2][j]-'0';
				}
				if(s[i][3][1] == 'n'){
					if(q == 1) q = 0;
					else q = 1;
				}
				else{
					for(int j = 1;j <= strlen(s[i][3]+1);j++)
						y = y*10+s[i][3][j]-'0';
					if(q == 1) z = -1;
					else if(y < x) z = -1;
				}
				i = dfs(q,z,i+1);
				if(flag) break;
			}
			else{
				flag = true;
				break;
			}
		}
		if(flag){
			puts("ERR");
			continue;
		}
		if(w == Maxw) puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005,M = 200005;
int head[N],nxt[M],to[M],w[M],cnt;
int head2[N],nxt2[M],to2[M],w2[M],cnt2;
int n,m,k,p,ans;
int dist[N],f[N];
bool vis[N];
struct que{
	int top;
	pair<int,int> o[M];
	que(){
		top = 0;
		memset(o,0,sizeof o);
	}
	void init(){
		top = 0;
	}
	void up(int x){
		if(x == 1) return;
		if(o[x>>1].second > o[x].second){
			swap(o[x>>1],o[x]);
			up(x>>1);
		}
	}
	void push(int x,int v){
		o[++top] = make_pair(x,v);
		up(top);
	}
	void down(int x){
		int l = x<<1,r;
		if(l > top) return;
		if(l < top) r = x<<1|1;
		else r = l;
		if(o[l].second > o[r].second) l = r;
		if(o[x].second > o[l].second){
			swap(o[x],o[l]);
			down(l);
		}
	}
	void pop(){
		o[1] = o[top]; --top;
		down(1);
	}

}q;

void add(int a,int b,int c){
	to[++cnt] = b; nxt[cnt] = head[a]; head[a] = cnt; w[cnt] = c;
}
void add2(int a,int b,int c){
	to2[++cnt2] = b; nxt2[cnt2] = head2[a]; head2[a] = cnt2; w2[cnt2] = c;
}

void dij1(){
	memset(dist,0x3f,sizeof dist);
	memset(vis,false,sizeof vis);
	memset(f,0,sizeof f);
	dist[1] = 0;
	q.init(); q.push(1,0);
	f[1] = 1;
	for(int i = 1;i < n;i++){
		int u = q.o[1].first; q.pop();
		while(vis[u] == true){
			u = q.o[1].first; q.pop();
		}
		vis[u] = true;
		if(u == n) break;
		for(int j = head[u];j;j = nxt[j]){
			int v = to[j];
			if(dist[v] > dist[u]+w[j]){
				dist[v] = dist[u]+w[j];
				f[v] = f[u];
				q.push(v,dist[v]);
			}
			else if(dist[v] == dist[u]+w[j])
				f[v] = (f[v]+f[u])%p;
		}
	}
}

void dij2(){
	memset(dist,0x3f,sizeof dist);
	memset(vis,false,sizeof vis);
	dist[n] = 0;
	q.init(); q.push(n,0);
	for(int i = 1;i < n;i++){
		int u = q.o[1].first; q.pop();
		while(vis[u] == true){
			u = q.o[1].first; q.pop();
		}
		vis[u] = true;
		for(int j = head2[u];j;j = nxt2[j]){
			int v = to2[j];
			if(dist[v] > dist[u]+w2[j]){
				dist[v] = dist[u]+w2[j];
				q.push(v,dist[v]);
			}
		}
	}
}

void dfs(int x,int sum){
	if(x == n){
		++ans;
		if(ans == p) ans = 0;
	}
	for(int i = head[x];i;i = nxt[i]){
		int v = to[i];
		if(sum+w[i]+dist[v] > dist[1]+k) continue;
		dfs(v,sum+w[i]);
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T = read();
	while(T--){
		n = read(); m = read(); k = read(); p = read();
		if(k == 0){
			memset(head,0,sizeof head);
			cnt = 0;
			for(int i = 1;i <= m;i++){
				int a = read(),b = read(),c = read();
				add(a,b,c);
			}
			dij1();
			printf("%d\n",f[n]);
		}
		else{
			memset(head,0,sizeof head);
			memset(head2,0,sizeof head2);
			cnt = 0; cnt2 = 0;
			for(int i = 1;i <= m;i++){
				int a = read(),b = read(),c = read();
				add(a,b,c); add2(b,a,c);
			}
			ans = 0;
			dij2();
			dfs(1,0);
			printf("%d\n",ans);
		}
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

ll a,b,c,d;
ll ans;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a = read(); b = read();
	if(a > b) swap(a,b);
	ans = b*(a-1)-a;
	printf("%lld\n",ans);
	return 0;
}

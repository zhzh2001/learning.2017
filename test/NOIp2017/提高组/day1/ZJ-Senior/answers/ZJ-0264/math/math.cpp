#include<cstdio>
#include<cstring>
template <typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template <typename T>inline void print(const T a)
{
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template <typename T>inline void printfn(const T a){print(a);putchar('\n');}
long long a,b;
long long ga,gb;
inline void egcd(long long &x,long long &y,long long a,long long b)
{
	if(b==0)
	{
		x=1,y=0;
		return;
	}
	egcd(y,x,b,a%b);
	y-=a/b*x;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(a),read(b);
	long long y=0;
	egcd(ga,y,a,b);
	ga+=b;
	ga%=b;
	y=0;
	egcd(gb,y,b,a);
	gb+=a;
	gb%=a;
	printfn((ga-1)*a+(gb-1)*b-1);
}

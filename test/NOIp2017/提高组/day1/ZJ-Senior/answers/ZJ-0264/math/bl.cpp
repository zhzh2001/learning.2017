#include<bits/stdc++.h>
using namespace std;
long long a,b;
long long ga,gb;
inline void egcd(long long &x,long long &y,long long a,long long b)
{
	if(b==0)
	{
		x=1,y=0;
		return;
	}
	egcd(y,x,b,a%b);
	y-=a/b*x;
}
bool check(long long x)
{
	while(x%a!=0&&x>0)x-=b;	
	if(x<0)return 0;
	return 1;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("1.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	long long y=0;
	egcd(ga,y,a,b);
	ga+=b;
	ga%=b;
	y=0;
	egcd(gb,y,b,a);
	gb+=a;
	gb%=a;
	long long ans=0;
	for(long long i=(ga-1)*a+(gb-1)*b-100;i<=(ga-1)*a+(gb-1)*b+100;i++)
		if(!check(i))ans=i;
	printf("%lld\n",ans);
}

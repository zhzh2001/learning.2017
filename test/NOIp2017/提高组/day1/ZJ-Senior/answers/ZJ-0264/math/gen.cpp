#include<bits/stdc++.h>
#include<time.h>
using namespace std;
long long gcd(long long a,long long b)
{
	while(1)
	{
		a%=b;
		if(!a)return b;
		b%=a;
		if(!b)return a;
	}
}
int main()
{
	freopen("math.in","w",stdout);
	srand((unsigned)time(NULL));
	long long a=rand(),ta=rand(),b=rand(),tb=rand();
	a*=ta,b*=tb,a%=1000000000,b%=1000000000;
	while(gcd(a,b)!=1)
		b=rand()*rand()+1,b%=1000000000;
	cout<<a<<' '<<b;
}

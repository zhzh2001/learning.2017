#include<cstdio>
#include<cstring>
#include<map>
using namespace std;
template <typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template <typename T>inline void print(const T a)
{
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template <typename T>inline void printfn(const T a){print(a);putchar('\n');}
int L,t;
char com[100001];
int flagn;
int HAS[301];
char cc[100001];
int Flag[1000001];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(t);
	while(t--)
	{
		read(L);
		gets(com+1);
		bool flag=0;
		flagn=0;
		for(register int i=1;i<=strlen(com+1);i++)
		{
			if(com[i]=='1'&&!flag){flagn=0;break;}
			if(com[i]=='n')flag=1;
			if(com[i]>='0'&&com[i]<='9')flagn*=10,flagn+=com[i]-'0';
		}
		int cntn=0,cntf=0,ans=0,cntflag=0;
		int err=0;
		memset(HAS,0,sizeof(HAS));
		while(L--)
		{
			gets(com+1);
			if(com[1]=='F')
			{
				cntf++;
				int fflag=-1,xia=0,shang=0;
				for(register int i=2;i<=strlen(com+1);i++)
				{
					if(com[i]==' '){fflag++;continue;}
					if(fflag==0)
					{
						if(HAS[com[i]]==1){err=2;break;}
						HAS[com[i]]=1;
						cc[cntf]=com[i];
					}
					else if(fflag==1)
					{
						if(com[i]=='n')xia=1<<30;
						else xia*=10,xia+=com[i]-'0';
					}
					else if(fflag==2)
					{
						if(com[i]=='n')shang=1<<30;
						else shang*=10,shang+=com[i]-'0';
					}
				}
				if(xia<shang&&shang==1<<30&&!cntflag)cntn++;
				else if(xia>shang)cntflag++,Flag[cntf]=1;
			}
			else
			{
				HAS[cc[cntf]]=0;
				if(Flag[cntf])Flag[cntf]=0,cntflag--;
				cntf--;
				if(cntf==0)ans=max(ans,cntn),cntn=0;
				if(cntf<0)err=1;
			}
		}
		ans=max(ans,cntn);
		if(err||cntf>0)printf("ERR\n");
		else if(ans==flagn)printf("Yes\n");
		else printf("No\n");
//		printf("cntn=%d,flagn=%d\n\n",ans,flagn);
	}	
}

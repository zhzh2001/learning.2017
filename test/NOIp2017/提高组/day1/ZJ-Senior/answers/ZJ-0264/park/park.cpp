#include<cstdio>
#include<cstring>
#include<vector>
using namespace std;
template <typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template <typename T>inline void print(const T a)
{
	if(a<0)
	{
		putchar('-'),print(-a);
		return;
	}
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template <typename T>inline void printfn(const T a){print(a);putchar('\n');}
int T,n,m,P,k;
struct pt
{
	vector<int>x,v;
}p[100001];
int dis[100001],dl[1000001],inque[100001];
int ans,CNT=0;
inline void dfs(const int x,const int v)
{
	if(CNT>1)return;
	if(v>dis[n]+k)return;
	if(x==n)ans++;
	//printf("%d %d\n",x,v);
	if(x==1&&v==0)CNT++;
	for(register int i=0;i<p[x].x.size();i++)
		dfs(p[x].x[i],v+p[x].v[i]);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n),read(m),read(k),read(P);
		for(register int i=1;i<=m;i++)
		{
			int a,b,c;
			read(a),read(b),read(c);
			p[a].x.push_back(b);
			p[a].v.push_back(c);
		}
		memset(dis,-1,sizeof(dis));
		memset(inque,0,sizeof(inque));
		ans=0;
		CNT=0;
		int h=0,e=1;
		dl[1]=1;
		dis[1]=0;
		while(h<e)
		{
			h++;
			int u=dl[h];
			inque[u]=0;
			for(register int i=0;i<p[u].x.size();i++)
			{
				const int v=p[u].x[i];
				if(dis[v]==-1||dis[v]>dis[u]+p[u].v[i])
				{
					dis[v]=dis[u]+p[u].v[i];
					if(!inque[v])dl[++e]=v;
					inque[v]=1;
				}
			}
		}
		dfs(1,0);
		printfn(CNT>1?-1:ans%P);
	}
}

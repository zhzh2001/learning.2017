#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 100
using namespace std;
struct Orz{
	char c;
	bool f;
}stack[MAXN+5];
int top,t,l,cmp,a[MAXN+5],ma;
int f;
char s[MAXN+5],sc[MAXN+5],sx[MAXN+5],sy[MAXN+5];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		memset(a,0,sizeof(a));
		f=0; top=0; cmp=0; ma=0;
		scanf("%d%s",&l,s);
		if (s[2]=='n') f=1;
		for (int i=0;s[i];i++)
			if (s[i]>='0'&&s[i]<='9')
				cmp=cmp*10+s[i]-48;
		int num=0;
		bool flag=false,fa=false,fl=false; if (l&1) flag=true;
		for (int i=1;i<=l;i++){
			int ff=0; char ss[2];
			scanf("%s",ss);
			ff=ss[0];
			if (ff=='F'){
				int c,x=0,y=0;
				scanf("%s%s%s",sc,sx,sy);
				c=sc[0];
				for (int j=0;sx[j];j++)
					if (sx[j]>='0'&&sx[j]<='9')
						x=x*10+sx[j]-48;
				for (int j=0;sy[j];j++)
					if (sy[j]>='0'&&sy[j]<='9')
						y=y*10+sy[j]-48;
				if (sx[0]=='n') x='n';
				if (sy[0]=='n') y='n';
				if (flag) continue;
				if (a[c-'a']){ flag=true; continue; }
				if (x=='n'&&y!='n') fl=true;
				if (x!='n'&&y!='n'&&x>y) fl=true;
				if (x!='n'&&y=='n'&&(!fl)) fa=true,num++,ma=max(ma,num);
				a[c-'a']++; 
				Orz p; p.c=c;
				if (x!='n'&&y=='n') p.f=true;
				else p.f=false;
				stack[++top]=p;
			}
			else{
				if (!top) { flag=true; continue; }
				if (stack[top].f) num--;
				a[stack[top--].c-'a']--;
			}
		}
		if (top) flag=true;
		if (flag){
			printf("ERR\n");
			continue;
		}
		if ((fa&&(f==0))||(ma!=cmp&&(f==1))){
			printf("No\n");
			continue;
		}
		printf("Yes\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

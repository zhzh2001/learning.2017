#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
LL a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a>b) swap(a,b);
	if (a==1){
		printf("0\n");
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if (a==2){
		printf("%lld\n",b-2);
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	LL ans=(4+(a-3)*2+4)*(a-2)/2+1;
	ans+=(b-a-1)*(a-1);
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 100000
#define MAXM 200000
using namespace std;
struct edge{
	int next,to,dis;
}ed[MAXM+5];
int t,p,n,m,k,ans,q;
int h[MAXN+5],dis[MAXN+5],que[MAXN*20+5][2];
bool f[MAXN+5];
inline char readc(){
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int _read(){
	int num=0,flag=1; char ch=readc();
	while (!isdigit(ch)&&ch!='-') ch=readc();
	if (ch=='-') flag=-1,ch=readc();
	while (isdigit(ch)) { num=num*10+ch-48; ch=readc(); }
	return num*flag;
}
void addedge(int x,int y,int z){
	ed[++k].next=h[x]; ed[k].to=y; ed[k].dis=z; h[x]=k;
}
int spfa(){
	memset(dis,0x3f,sizeof(dis));
	memset(f,false,sizeof(f));
	int r=0,w=1;
	que[1][0]=1; f[1]=true; dis[1]=0;
	while (r<w){
		int x=que[++r][0];
		f[x]=false;
		for (int i=h[x];i;i=ed[i].next)
			if (dis[ed[i].to]>dis[x]+ed[i].dis){
				dis[ed[i].to]=dis[x]+ed[i].dis;
				if (!f[ed[i].to])
					que[++w][0]=ed[i].to,f[ed[i].to]=true;
			}
	}
	return dis[n];
}
int bfs(int dis){
	int r=0,w=1; que[1][0]=1; que[1][1]=0;
	int ret=0;
	while (r<w){
		int x=que[++r][0];
		if (x==n) ret=(ret+1)%p;
		for (int i=h[x];i;i=ed[i].next)
			if (ed[i].dis+que[r][1]<=dis){
				que[++w][0]=ed[i].to;
				que[w][1]=ed[i].dis+que[r][1];
				if (w==MAXN*20) return -1;
			}
	}
	return ret;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=_read();
	while (t--){
		memset(h,0,sizeof(h)); k=0;
		n=_read(),m=_read(),q=_read(),p=_read();
		for (int i=1;i<=m;i++){
			int u=_read(),v=_read(),d=_read();
			addedge(u,v,d);
		}
		ans=spfa();
		if (ans>MAXM*1000){
			printf("0\n");
			continue;
		}
		printf("%d\n",bfs(ans+q));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

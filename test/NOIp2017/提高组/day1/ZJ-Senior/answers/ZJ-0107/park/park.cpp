#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 100005
using namespace std;
int read()
{int t=0;char c;
 c=getchar();
 while(!(c>='0' && c<='9')) c=getchar();
 while(c>='0' && c<='9')
 {
 	t=t*10+c-48;c=getchar();
 }
 return t;
}
int front[N],to[2*N],next[2*N],line,p,quan[2*N];
int n,m,tt,dep[N],s[N],t,w,k,x,y,z;
bool ifin[N];
int f[N][55];
void insert(int x,int y,int z)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;quan[line]=z;
}
int ss[N];
void spfa()
{int i;
	for(i=1;i<=n;i++) dep[i]=1000000000;
	memset(ifin,false,sizeof(ifin));
	dep[1]=0;t=1;w=1;s[t]=1;ifin[1]=true;
	while(t<=w)
	 {
	 	x=s[t%N];ifin[x]=false;
	 	for(i=front[x];i!=-1;i=next[i])
	 	  if(dep[to[i]]>dep[x]+quan[i])
	 	    {
	 	    	dep[to[i]]=dep[x]+quan[i];
	 	    	if(!ifin[to[i]])
	 	    	 {
	 	    	 	w++;s[w%N]=to[i];ifin[to[i]]=true;
				  }
			 }
		t++;
	 }
}
void spfa2()
{int i,j;
   memset(f,0,sizeof(f));
   memset(ifin,false,sizeof(ifin));
   memset(ss,0,sizeof(ss));
   f[1][0]=1;t=w=1;s[t]=1;ifin[1]=true;
   while(t<=w)
    {
    	x=s[t%N];ifin[x]=false;ss[x]++;
    	if(ss[x]>1000) break;
    	for(i=front[x];i!=-1;i=next[i])
    	  for(j=0;j<=k;j++)
    	    if(dep[x]+j+quan[i]<=dep[to[i]]+k)
    	      {
    	      	f[to[i]][dep[x]+j+quan[i]-dep[to[i]]]=(f[to[i]][dep[x]+j+quan[i]-dep[to[i]]]+f[x][j])%p;
    	      	if(!ifin[to[i]])
    	      	  {
    	      	    ifin[to[i]]=true;w++;s[w%N]=to[i];	
				  }
			  }
		t++;
	}
}
int main()
{int i,j;
    freopen("park.in","r",stdin);
    freopen("park.out","w",stdout);
    tt=read();
    for(;tt;tt--)
    {
    	line=-1;memset(front,-1,sizeof(front));
    	n=read();m=read();k=read();p=read();
    	for(i=1;i<=m;i++)
    	  {
    	  	x=read();y=read();z=read();insert(x,y,z);
		  }
		spfa();
		//for(i=1;i<=n;i++) printf("%d ",dep[i]);printf("\n");
		spfa2();
		if(ss[n]>=1000) 
		 {
		  printf("-1");continue;
		}
		int ans=0;
		for(i=0;i<=k;i++) ans=(ans+f[n][i])%p;
		printf("%d\n",ans);
	}
	return 0;
}


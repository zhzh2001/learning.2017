#include <cstdio>
#include <cstring>
#include <stack>
using namespace std;
struct xlxs{
	char ch;
	int x, y, w;
	bool bx, by;
	xlxs(char ch = ' ', int x = 0, int y = 0) : ch(ch), x(x), y(y)
	{
		w = bx = by = 0;
	}
	int xyxh() const;
	void xy(const xlxs& a);
};
int xlxs::xyxh() const
{
	if (!bx && !by) return w * (x <= y);
	if (bx && !by) return 0;
	if (bx && by) return w;
	//if (!bx && by) return w + 1;
	return w + 1;
}
void xlxs::xy(const xlxs& a)
{
	int w2 = a.xyxh();
	//printf("debug:w2 = %d\n", w2);
	if (w2 > this -> w)
	{
		//printf("debug:%d > %d\n", w2, this -> w);
		this -> w = w2;
	}
}
int t, l, w, w1;
char st[10001];
bool b[26], err;
stack < xlxs > s;
xlxs a, tt;
int main()
{
	#ifndef DEBUG
	freopen("complexity.in", "r+", stdin);
	freopen("complexity.out", "w+", stdout);
	#endif
	scanf("%d", &t);
	while (t--)
	{
		memset(b, 0, sizeof(b));
		scanf("%d %s", &l, st);
		//printf("debug:st = \"%s\"\n", st);
		if (strcmp(st, "O(1)")) sscanf(st, "O(n^%d)", &w);
		else w = 0;
		//printf("debug:l = %d, w = %d\n", l, w);
		err = l & 1;
		tt.w = 0;
		for (register int i = l + 1; --i; )
		{
			//printf("debug:i = %d\n", i);
			scanf("%s", st);
			//printf("debug:st = \"%s\"\n", st);
			if (st[0] == 'F')
			{
				if (err)
				{
					scanf("%s", st);
					//a.ch = st[0];
					scanf("%s", st);
					/*if (st[0] == 'n') a.x = 0, a.bx = 1;
					else sscanf(st, "%d", &a.x), a.bx = 0;*/
					scanf("%s", st);
					/*if (st[0] == 'n') a.y = 0, a.by = 1;
					else sscanf(st, "%d", &a.y), a.by = 0;*/
					continue;
				}
				scanf("%s", st);
				a.ch = st[0];
				scanf("%s", st);
				if (st[0] == 'n') a.x = 0, a.bx = 1;
				else sscanf(st, "%d", &a.x), a.bx = 0;
				scanf("%s", st);
				if (st[0] == 'n') a.y = 0, a.by = 1;
				else sscanf(st, "%d", &a.y), a.by = 0;
				a.w = 0;
				//printf("debug:a.ch = \'%c\', a.x = %d, a.y = %d\n", a.ch, a.x, a.y);
				if (b[a.ch - 'a']) err = 1;
				else
				{
					b[a.ch - 'a'] = 1;
					s.push(a);
				}
			}
			else
			{
				if (err) continue;
				if (s.empty()) err = 1;
				else
				{
					a = s.top();
					s.pop();
					if (!s.empty()) s.top().xy(a);
					else tt.xy(a);
					b[a.ch - 'a'] = 0;
				}
			}
		}
		if (!s.empty())
		{
			err = 1;
			while (!s.empty()) s.pop();
		}
		if (err) puts("ERR");
		else if (w == tt.w) puts("Yes");
		else puts("No");
	}
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}

#include <cstdio>
#include <iostream>
using namespace std;
typedef long long xlxs;
xlxs ysx, xrm;
int main()
{
	#ifndef DEBUG
	freopen("math.in", "r+", stdin);
	freopen("math.out", "w+", stdout);
	#endif
	ios::sync_with_stdio(0);
	cin >> ysx >> xrm;
	cout << ysx * xrm - ysx - xrm << endl;
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}

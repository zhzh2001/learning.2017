#include <cstdio>
#include <cctype>
#include <cstring>
#include <queue>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
typedef unsigned long long xuyixuan;
using namespace std;
struct G{
	int h[100001], nxt[200001], v[200001], w[200001], cnt;
	void reset(int n)
	{
		cnt = 0;
		memset(h + 1, 0, sizeof(int) * n);
	}
	void add_edge(int u1, int v1, int w1)
	{
		v[++cnt] = v1;
		w[cnt] = w1;
		nxt[cnt] = h[u1];
		h[u1] = cnt;
	}
}g1;
struct Spfa{
	int d[100001], u, num[100001], n1;
	bool b[100001];
	__gnu_pbds::tree < int, int > dp[6][100001];
	queue < int > q;
	G g;
	void reset(int n)
	{
		while (!q.empty()) q.pop();
		memset(num + 1, 0, sizeof(int) * n);
		for (register int i = n + 1; --i; ) d[i] = 0x7fffffff;
		memset(b + 1, 0, sizeof(int) * n);
		g.reset(n);
		n1 = n;
	}
	int spfa(int s);
	int spfa2(int s);
}sp1;
int T, n, m, k, p, u, v, w;
xuyixuan sum;
void dfs(int k, int s);
const int ri_top = 1e7;
char ri[ri_top], *rich = ri;
void read_int(int& x)
{
	while (!isdigit(*rich)) ++rich;
	for (x = *rich - '0'; isdigit(*++rich); x = x * 10 + *rich - '0');
}
int main()
{
	#undef DEBUG
	#ifndef DEBUG
	freopen("park.in", "r+", stdin);
	freopen("park.out", "w+", stdout);
	fread(ri, 1, ri_top, stdin);
	#endif
	read_int(T);
	for (++T; --T; )
	{
		read_int(n);
		read_int(m);
		read_int(k);
		read_int(p);
		sp1.reset(n);
		g1.reset(n);
		sum = 0;
		for (register int i = m + 1; --i; )
		{
			read_int(u);
			read_int(v);
			read_int(w);
			sp1.g.add_edge(u, v, w);
			g1.add_edge(v, u, w);
		}
		if (n <= 1000)
		{
			if (sp1.spfa2(1))
			{
				puts("-1");
				continue;
			}
			dfs(n, 0);
			printf("%d\n", (int)sum);
		}
		else
		{
			if (sp1.spfa(1))
			{
				puts("-1");
				continue;
			}
			for (__gnu_pbds::tree < int, int > :: iterator it = sp1.dp[T][n].begin(); it != sp1.dp[T][n].end(); ++it)
				if (it -> first > sp1.d[n] + k) break;
				else sum = (sum + it -> second) % p;
			printf("%d\n", (int)sum);
		}
	}
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}
void dfs(int u, int s)
{
	if (u == 1 && s <= sp1.d[n] + k)
	{
		sum = (sum + 1) % p;
		return;
	}
	if (s + sp1.d[u] > sp1.d[n] + k) return;
	for (register int i = g1.h[u]; i; i = g1.nxt[i])
		dfs(g1.v[i], s + g1.w[i]);
}
int Spfa::spfa(int s)
{
	q.push(s);
	b[s] = 1;
	d[s] = 0;
	++num[s];
	dp[T][s][0] = 1;
	while (!q.empty())
	{
		u = q.front();
		q.pop();
		b[u] = 0;
		int& ans1 = dp[T][u][d[u]];
		for (register int i = g.h[u]; i; i = g.nxt[i])
			if (d[u] + g.w[i] <= d[g.v[i]])
			{
				int& ans = dp[T][g.v[i]][d[u] + g.w[i]];
				ans = ((xuyixuan)ans + ans1) % p;
				//printf("debug:dp[%d][%d][%d] = %d\n", T, g.v[i], d[u] + g.w[i], ans);
				d[g.v[i]] = d[u] + g.w[i];
				if (!b[g.v[i]])
				{
					++num[g.v[i]];
					if (num[g.v[i]] > n1) return -1;
					q.push(g.v[i]);
					b[g.v[i]] = 1;
				}
			}
			else if (d[u] + g.w[i] <= d[g.v[i]] + k)
			{
				int& ans = dp[T][g.v[i]][d[u] + g.w[i]];
				ans = ((xuyixuan)ans + ans1) % p;
				//printf("debug:dp[%d][%d][%d] = %d\n", T, g.v[i], d[u] + g.w[i], ans);
			}
	}
	return 0;
}
int Spfa::spfa2(int s)
{
	q.push(s);
	b[s] = 1;
	d[s] = 0;
	++num[s];
	while (!q.empty())
	{
		u = q.front();
		q.pop();
		b[u] = 0;
		for (register int i = g.h[u]; i; i = g.nxt[i])
			if (d[u] + g.w[i] <= d[g.v[i]])
			{
				d[g.v[i]] = d[u] + g.w[i];
				if (!b[g.v[i]])
				{
					++num[g.v[i]];
					if (num[g.v[i]] > n1) return -1;
					q.push(g.v[i]);
					b[g.v[i]] = 1;
				}
			}
	}
	return 0;
}

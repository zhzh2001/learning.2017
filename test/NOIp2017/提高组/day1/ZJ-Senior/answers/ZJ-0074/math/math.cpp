#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mo=1e8;
ll a,b,x,y;
ll q[100];
ll exgcd(ll a,ll b,ll &x,ll &y)
{
	if(!b){
		x=1;y=0;
		return a;
	}
	ll d=exgcd(b,a%b,x,y);
	ll t=x;
	x=y;
	y=t-(a/b)*x;
	return d;
}
void cheng(int x)
{
	for(int i=1;i<=q[0];i++)
		q[i]=q[i]*x;
	for(int i=1;i<=q[0];i++)
		q[i+1]+=q[i]/mo,q[i]%=mo;
	while(q[q[0]+1])q[0]++;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a>b)swap(a,b);
	exgcd(a,b,x,y);
	while(abs(x)>b)x+=b;
	x=abs(x);
    q[1]=x%mo;
	q[2]=x/mo;
	if(q[2])q[0]=2;
	else q[0]=1;
	cheng(a-1);
	cheng(a);
	q[1]--;
	for(int i=1;i<=q[0];i++)
		if(q[i]<0)q[i+1]--,q[i]+=mo;
	printf("%lld",q[q[0]]);
	for(int i=q[0]-1;i;i--)
		printf("%08lld",q[i]);
	return 0;
}

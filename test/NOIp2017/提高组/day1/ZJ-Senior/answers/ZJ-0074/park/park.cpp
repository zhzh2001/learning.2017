#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pa pair<int,int>
#define mk(x,y) make_pair(x,y)
inline ll read(){ll x=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar());for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x;}
inline void write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
const int N=100005;
const int M=400005;
queue<int>q;
bool inq[N];
ll cnt[N],dis[N],e[M];
int fm[M],nxt[M],gt[M];
bool ca[N];
int vis[N];
int n,x,y,z,t,m,p,axs;
queue<pa>qu;
map<pa,int>mp;
void addto(int x,int y,int z,bool k)
{
	nxt[++t]=fm[x];
	fm[x]=t;
	gt[t]=y;
	e[t]=z;
	ca[t]=k;
}
void pre()
{
	memset(dis,127/3,sizeof(dis));
	q.push(n);inq[n]=true;
	dis[n]=0;
	while(!q.empty()){
		int k=q.front();
		if(cnt[k]>1e18)cnt[k]%=p;
		inq[k]=false;
		q.pop();
		for(int i=fm[k];i;i=nxt[i])
			if(ca[i]==1){
			int x=gt[i];
			if(dis[x]>dis[k]+e[i]){
				dis[x]=dis[k]+e[i];
				if(!inq[x]){
					q.push(x);
					inq[x]=true;
				}
			}
		}
	}
}
ll bfs()
{
	qu.push(mk(1,0));
	ll ret=0;
	while(!qu.empty()){
		if(mp[qu.front()])return -1;
		mp[qu.front()]=1;
		int k=qu.front().first;
		int t=qu.front().second;
		qu.pop();
		if(k==n){
			ret++;
			if(ret>1e18)ret%=p;
		}
		for(int i=fm[k];i;i=nxt[i])
			if(ca[i]==0){
			int x=gt[i];
			if(t+dis[x]+e[i]<=axs+dis[1])qu.push(mk(x,t+e[i]));
		}
	}
	return ret%p;
}
ll spfa()
{
	memset(cnt,0,sizeof(cnt));
	memset(dis,127/3,sizeof(dis));
	dis[1]=0;cnt[1]=1;q.push(1);
	while(!q.empty()){
		int k=q.front();
		if(cnt[k]>1e18)cnt[k]%=p;
		inq[k]=false;
		q.pop();
		for(int i=fm[k];i;i=nxt[i])
			if(ca[i]==0){
			int x=gt[i];
			if(dis[x]==dis[k]+e[i]){
				if(!inq[x]){
					cnt[x]=cnt[k];
					q.push(x);
					inq[x]=true;
				}else cnt[x]+=cnt[k];
			}
			else if(dis[x]>dis[k]+e[i]){
				dis[x]=dis[k]+e[i];
				cnt[x]=cnt[k];
				if(!inq[x]){
					q.push(x);
					inq[x]=true;
				}
			}
		}
	}
	return cnt[n]%p;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while(T--){
		bool b=true;
		n=read();m=read();axs=read();p=read();t=0;
		memset(fm,0,sizeof(fm));
		memset(nxt,0,sizeof(nxt));
		for(int i=1;i<=m;i++){
			x=read();y=read();z=read();
			addto(x,y,z,0);
			addto(y,x,z,1);
			if(z==0)b=false;
		}
		if(b&&axs==0){
			writeln(spfa());
		}
		else{
			pre();
			writeln(bfs());
		}
	}
}
/*
2
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
2 2 0 10
1 2 0
2 1 1

*/

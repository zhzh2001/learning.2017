#include<bits/stdc++.h>
using namespace std;
const int N=105;
string a[N];
bool vis[N];
int d[N];
map<string,int>mp;
int tt;
string st,ss;
string op,c,t,p;
int tot,tx,n;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		memset(vis,0,sizeof(vis));
		vis[0]=true;
		bool b=true;
		tot=0;
		mp.clear();
		int ans=0;
		scanf("%d",&n);
		cin>>st;
		tx=0;
		if(st[2]=='n'){
			int t=0;
			int len=st.length();
			for(int i=4;i<=len&&st[i]>='0'&&st[i]<='9';i++){
				tx=tx*10+st[i]-48;
			}
			tt=tx;
		}else tt=0;
		tx=0;
		while(n--){
			if(vis[tot])ans=max(ans,tx);
			cin>>ss;
			if(ss[0]=='E'){
				mp[a[tot]]=0;
				if(tot==0)b=false;
				else {
					tx-=d[tot];
					tot--;
				}
			}else{
				cin>>c>>t>>p;
				if(mp[c])b=false;
				mp[c]=1;tot++;a[tot]=c;d[tot]=0;
				if(t[0]=='n'&&p[0]=='n'){vis[tot]=vis[tot-1];continue;}
				else{
					if(t[0]=='n')vis[tot]=false;
					else{
						if(p[0]=='n')tx++,d[tot]=1,vis[tot]=vis[tot-1];
						else{
							int x=0,y=0;
							int len=t.length();
							for(int j=0;j<len&&t[j]>='0'&&t[j]<='9';j++)
								x=x*10+t[j]-48;
							len=p.length();
							for(int j=0;j<len&&p[j]>='0'&&p[j]<='9';j++)
								y=y*10+p[j]-48;
							if(x<=y)vis[tot]=vis[tot-1];
							else vis[tot]=false;
						}
					}
				}
			}
		}
		if(!b||tot>0)puts("ERR");
		else{
			if(tt==ans)puts("Yes");
			else puts("No");
		}
	}
	return 0;
}
/*
60 O(n^8)
F a 76 n
F b 75 n
F c 88 89
E
F d 32 n
F e 41 n
F f 85 n
E
E
E
E
E
F a 56 n
F b 73 73
F c 70 n
F d 80 n
F e 48 93
F f 71 81
F g 85 n
F h 44 n
F i 70 81
F j 3 53
F k 71 n
F l 84 n
F m 39 67
F o 16 n
E
E
E
E
E
E
E
E
E
E
E
E
E
E
F a 38 n
F b 51 89
F c 18 n
F d 81 88
F e 18 n
F f 28 n
F g 7 n
F h 89 93
F i 11 n
F j 31 58
E
E
E
E
E
E
E
E
E
E
*/

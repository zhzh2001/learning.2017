var n:longint;
    a:array['0'..'9'] of longint;
    p:array[0..1000] of char;
    t,ti,i,pks,tot,num,ma,l,r,o:longint;
    ch:char;
    boo:array['a'..'z']of boolean;
    zhan:array[0..1000] of longint;
    pd:array[0..1000] of boolean;
    flat:boolean;
    s:string;
begin
  assign(input,'complexity.in');
  reset(input);
 assign(output,'complexity.out');
  rewrite(output);
  a['0']:=0;
  a['1']:=1;
  a['2']:=2;
  a['3']:=3;
  a['4']:=4;
  a['5']:=5;
  a['6']:=6;
  a['7']:=7;
  a['8']:=8;
  a['9']:=9;
  readln(t);
  for t:=1 to t do
  begin
    tot:=0;
    num:=0;
    for ch:='a' to 'z' do
      boo[ch]:=false;
    ma:=0;
    flat:=false;
    read(o);
    read(ch);
    while (not((ch='1')or(ch='n'))) do
     read(ch);
    if (ch='1')then
    begin
      readln;
      ti:=0;
    end
    else
    begin
      read(ch);
      read(ch);
      ti:=0;
      while ((ch<='9')and(ch>='0')) do
      begin
        ti:=ti*10+a[ch];
        read(ch);
      end;
      readln;
    end;
    pd[0]:=true;
    for i:=1 to o do
    begin
      readln(s);
      if (s[1]='E')then
      begin
        if (tot=0) then begin flat:=true;continue;end;
        dec(num,zhan[tot]);
        boo[p[tot]]:=false;
        dec(tot);
      end;
      if (s[1]='F') then
      begin
        if (boo[s[3]]) then begin flat:=true; continue;end;
        boo[s[3]]:=true;
        inc(tot);
        zhan[tot]:=0;
        p[tot]:=s[3];
        l:=0;
        if (s[5]='n')then
        begin
          l:=1000;
          pks:=6;
        end
        else
        begin
          pks:=5;
          while ((s[pks]<='9')and(s[pks]>='0')) do
          begin
            l:=l*10+a[s[pks]];
            inc(pks);
          end;
        end;
        inc(pks);
        r:=0;
        if (s[pks]='n')then
          r:=1000
        else
        begin
          while ((pks<=length(s))and(s[pks]<='9')and(s[pks]>='0')) do
          begin
            r:=r*10+a[s[pks]];
            inc(pks);
          end;
        end;
        if (r<l) then
          pd[tot]:=false
          else pd[tot]:=pd[tot-1];
        if ((r-l)>100)  then
          zhan[tot]:=1
        else zhan[tot]:=0;
        inc(num,zhan[tot]);
        if (pd[tot])  then
        if (ma<num) then
          ma:=num;
      end;
    end;
    if (tot>0) then flat:=true;
    if (flat) then begin writeln('ERR');continue;end;
    if (ma=ti) then writeln('Yes')else writeln('No');
  end;
  close(output);
end.



#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
using namespace std;

int t,n,m,k,p,d,INF=1000000007;
int ti=0,iter[100005]={0},dis[100005],ans[100005];
bool flag[100005];
struct edge{int to,next,cost;}g[400005];
typedef pair<int,int> P;
priority_queue<P,vector<P>,greater<P> > q;

inline int read(){
	int re=0,rf=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') rf=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') re=re*10+ch-'0',ch=getchar();
	return re*rf;
}

inline void init(){
	for(int i=1;i<=n;i++)
		iter[i]=0;
	ti=0;
}

inline void add(int x,int y,int z){
	ti++;
	g[ti].to=y;g[ti].next=iter[x];g[ti].cost=z;iter[x]=ti;
}

inline int dijkstra(){
	for(int i=1;i<=n;i++)
		dis[i]=INF,flag[i]=false;
	dis[1]=0;
	q.push(P(dis[1],1));
	while(!q.empty()){
		P pp=q.top();q.pop();
		int u=pp.second;
		if (!flag[u]){
			flag[u]=true;
			for(int i=iter[u];i;i=g[i].next){
				int v=g[i].to;
				if (dis[v]>dis[u]+g[i].cost) {dis[v]=dis[u]+g[i].cost;q.push(P(dis[v],v));}
			}
		}
	}
	return dis[n];
}

inline int dfs(int u,int c){
	if(c>d) return 0;
	if(u==n) return 1;
	int res=0;
	for(int i=iter[u];i;i=g[i].next){
		int v=g[i].to;
		res=(res+dfs(v,c+g[i].cost))%p;
	}
	return res;
}	

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=read();
	for(int i=1;i<=t;i++){
		n=read();m=read();k=read();p=read();
		for(int j=1;j<=m;j++){
			int x=read(),y=read(),z=read();
			add(x,y,z);
		}
		d=dijkstra()+k;
		if (ans[i]!=-1) ans[i]=dfs(1,0);
		init();
	}
	for(int i=1;i<=t;i++)
		printf("%d\n",ans[i]);
	return 0;
	fclose(stdin);
	fclose(stdout);
}
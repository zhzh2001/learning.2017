#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
using namespace std;

int t,l[10],u,sum,num,xiwang[10];
int ord[10][105][5];
char yucun[10];
bool flag[30],ttt;

inline int read(){
	int re=0,rf=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') rf=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') re=re*10+ch-'0',ch=getchar();
	return re*rf;
}

inline int dfs(int k){
	num++;
	flag[ord[k][u][0]]=true;
	int ans=ord[k][u][1],xx=ans;
	while(u<=l[k]){
		u++;
		if (num==0&&ord[k][u][0]==-1) {ttt=false;return 0;}
		if (ord[k][u][0]==-1) {num--;flag[ord[k][u][0]]=false;return ans;}
		if (ord[k][u][0]!=-1&&flag[ord[k][u][0]]) {ttt=false;return 0;}
		if (ord[k][u][0]!=-1&&!flag[ord[k][u][0]]) 
			{ans=max(ans,dfs(k)+xx);if (xx==-1) ans=0;if (!ttt) return 0;}
		}
	return ans;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=read();
	for(int k=1;k<=t;k++){
		cin>>l[k];
		cin>>yucun;
		if (yucun[2]=='n') {
			int poi=4;
			while(yucun[poi]>='0'&&yucun[poi]<='9') xiwang[k]=xiwang[k]*10+yucun[poi++]-'0';
		}
		else xiwang[k]=0;
		for(int i=1;i<=l[k];i++){
			cin>>yucun;
			if (yucun[0]=='E') ord[k][i][0]=-1;
			else {
				cin>>yucun;
				ord[k][i][0]=yucun[0]-'a'+1;
				cin>>yucun;
				if (yucun[0]=='n') ord[k][i][1]=101;
					else {int poi=0;while(yucun[poi]>='0'&&yucun[poi]<='9') ord[k][i][1]=ord[k][i][1]*10+yucun[poi++]-'0';}
				cin>>yucun;
				if (yucun[0]=='n') ord[k][i][2]=101;
					else {int poi=0;while(yucun[poi]>='0'&&yucun[poi]<='9') ord[k][i][2]=ord[k][i][2]*10+yucun[poi++]-'0';}
				if (ord[k][i][2]==101&&ord[k][i][1]<ord[k][i][2]) ord[k][i][1]=1;
					else if (ord[k][i][1]==101&&ord[k][i][1]>ord[k][i][2]) ord[k][i][1]=-1;
					else ord[k][i][1]=0;
			}
		}
	}
	for(int i=1;i<=t;i++){
		for(int j=0;j<=30;j++)
			flag[j]=false;
		num=0;
		sum=0;
		ttt=true;
		u=1;
		while(u<=l[i]){
			sum=dfs(i);
			if (!ttt) break;
			u++;
		}
		if (num>0) ttt=false;
		if (!ttt) printf("ERR\n");
			else{
				if (sum==xiwang[i]) printf("YES\n");
					else {printf("NO\n");}
				}
	}
	return 0;
	fclose(stdin);
	fclose(stdout);
}
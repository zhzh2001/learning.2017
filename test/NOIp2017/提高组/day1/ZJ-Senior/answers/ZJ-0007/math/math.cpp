#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
using namespace std;
typedef long long LL;

LL a,b;

inline LL read(){
	LL re=0,rf=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') rf=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') re=re*10+ch-'0',ch=getchar();
	return re*rf;
}

int main(){
	freopen("math.in","r",stdin);
	freopen("maht.out","w",stdout);
	a=read();b=read();
	if (a>b) swap(a,b);
	LL i=1;
	LL res=0;
	LL num=0;
	while(1){
		int flag=false;
		for(int j=0;j*a<=i;j++)
			for(int k=0;j*a+k*b<=i;k++)
				if (j*a+k*b==i) flag=true;
		if (flag) num++;
		else {num=0;res=i;}
		if (num==a+b) break;
		i++;
	}
	printf("%lld\n",res);
	return 0;
	fclose(stdin);
	fclose(stdout);
}
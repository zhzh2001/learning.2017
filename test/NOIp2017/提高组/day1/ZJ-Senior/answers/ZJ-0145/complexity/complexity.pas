program jiang;
var w,ww:string;
    st,question:string;
    c:char;
    zhan:array[1..1000] of string;
    t,l,n,m,i,j,ii,maxf:longint;
    ans:string;

function work:string;
var f,ans:longint;
    br:boolean;
    x,y,bian:string;
begin
   maxf:=0;
   br:=false;
   ans:=0;
   m:=1; f:=0;
   readln(l,c,question);
   for i:=1 to l do
      begin
        readln(st); st:=st+' '; 
        n:=length(st); j:=0;
        while j<n do
          begin
            inc(j);
            if st[j]=' ' then begin inc(m); continue; end else
            zhan[m]:=zhan[m]+st[j];
          end;
        end;
  j:=0;
  while j<m do 
  begin
    inc(j);
    if zhan[j]='F' then begin
                          inc(f); inc(j); bian:=zhan[j]; if f>maxf then maxf:=f;
                          inc(j); x:=zhan[j];
                          inc(j); y:=zhan[j];
                          if x=y then continue;
                          if x='n' then br:=true; 
                          if x>y then br:=true;
                          if y='n' then begin if not br then inc(ans); end;
                        end;
    if zhan[j]='E' then dec(f);
  end;
  if f<>0 then begin exit('EER');  end;
  if ans=0 then exit('O(1)') else work:='O(n^'+chr(ans+ord('0'))+')';
end;

begin
  assign(input,'complexity.in'); reset(input);
  assign(output,'complexity.out'); rewrite(output);
  readln(t);
  for ii:=1 to t do
    begin
      ans:=work;
      if ans='EER' then writeln(ans) else begin if ans=question then writeln('YES') else writeln('NO'); end;
    end;
  close(input); close(output);
end.


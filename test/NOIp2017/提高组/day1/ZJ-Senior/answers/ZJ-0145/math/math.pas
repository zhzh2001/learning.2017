program jiang;
var a,b,ans:int64;
begin
  assign(input,'math.in'); reset(input);
  assign(output,'math.out'); rewrite(output);
  readln(a,b);
  ans:=(a-1)*b-a;
  writeln(ans);
  close(input); close(output);
end.
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=105;
char s[N][N],t[N],c[N];
int type[N],L[N],R[N],stk[N],top,vis[26];
pair<int,int> Stk[N];
inline int get(char*s)
{
	int len=strlen(s);
	if(len==4)return 0;
	int tmp=0;
	for(int i=4;i<len-1;i++)
	{
		int now=s[i]-'0';
		tmp=tmp*10+now;
	}
	return tmp;
}
inline int check(int x)
{
	if(L[x]>R[x])return -1;
	if(L[x]==R[x])return 0;
	if(L[x]<=100&&R[x]<=100)return 0;
	return 1;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--)
	{
		int len;
		scanf("%d%s",&len,t);
		for(int i=1;i<=len;i++)
		{
			char ch=getchar();
			while(ch!='F'&&ch!='E')ch=getchar();
			if(ch=='F')
			{
				type[i]=1;
				char ch=getchar();
				while(ch<'a'||ch>'z')ch=getchar();
				c[i]=ch;
				ch=getchar();
				while(ch==' ')ch=getchar();
				if(ch=='n')L[i]=110;
				else
				{
					L[i]=0;
					while(ch>='0'&&ch<='9')L[i]=L[i]*10+ch-'0',ch=getchar();
				}
				ch=getchar();
				while(ch==' ')ch=getchar();
				if(ch=='n')R[i]=110;
				else
				{
					R[i]=0;
					while(ch>='0'&&ch<='9')R[i]=R[i]*10+ch-'0',ch=getchar();
				}
			}
			else type[i]=2;
		}
		int pw=get(t),flag=1;
		memset(vis,0,sizeof(vis));
		top=0;
		for(int i=1;i<=len;i++)
		{
			if(type[i]==1)
			{
				if(vis[c[i]-'a']){flag=0;break;}
				vis[c[i]-'a']=1;
				stk[++top]=i;
			}
			if(type[i]==2)
			{
				if(!top){flag=0;break;}
				vis[c[stk[top--]]-'a']=0;
			}
		}
		if(top>0)flag=0;
		if(!flag){puts("ERR");continue;}
		int now=0,ans=0;
		top=0;
		for(int i=1;i<=len;i++)
		{
			if(type[i]==1)
			{
				Stk[++top]=make_pair(i,now);
				if(check(i)==-1)now=-200;
				else now+=check(i);
				if(now>ans)ans=now;
			}
			if(type[i]==2)
			{
				pair<int,int> x=Stk[top--];
				if(check(x.first)==-1)now=x.second;
				else now-=check(x.first);
				if(now>ans)ans=now;
			}
		}
		puts(ans==pw?"Yes":"No");
	}
	return 0;
}

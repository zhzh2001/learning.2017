#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void read(int&x)
{
	x=0;int f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
const int N=1e5+5,M=2e5+5;
struct E{int to,v,nxt;}edge[M],Edge[M];
int T,n,m,k,p,head[N],tot,vis[N],dis[N],dis2[N],Q[N],f[N][55],calced[N][55],Head[N],Tot,mn1[N],mn2[N],num[N];
struct Graph
{
	struct E{int to,nxt;}edge[M];
	int head[N],tot,dfn[N],low[N],Time,stk[N],top,ins[N],fa[N],cnt;
	inline void init()
	{
		memset(head,-1,sizeof(head));
		tot=0;
		memset(dfn,0,sizeof(dfn));
		memset(low,0,sizeof(low));
		Time=0;
		top=0;
		cnt=0;
		memset(ins,0,sizeof(ins));
	}
	inline void addedge(int x,int y){edge[++tot]=(E){y,head[x]};head[x]=tot;}
	inline int mnn(int a,int b){return a<b?a:b;}
	void tarjan(int u)
	{
		dfn[u]=low[u]=++Time;
		stk[++top]=u;ins[u]=1;
		for(int i=head[u];i!=-1;i=edge[i].nxt)
			if(!dfn[edge[i].to])tarjan(edge[i].to),low[u]=mnn(low[u],low[edge[i].to]);
			else if(ins[edge[i].to])low[u]=mnn(low[u],dfn[edge[i].to]);
		if(dfn[u]==low[u])
		{
			++cnt;
			while(true)
			{
				int x=stk[top--];
				fa[x]=cnt;
				ins[x]=0;
				if(x==u)break;
			}
		}
	}
}G;
inline void SPFA()
{
	for(int i=1;i<=n;i++)vis[i]=0,dis[i]=1e9;
	vis[1]=1;dis[1]=0;Q[1]=1;
	int H=0,T=1;
	while(H!=T)
	{
		int x=Q[H=H%n+1];vis[x]=0;
		for(int i=head[x];i!=-1;i=edge[i].nxt)if(dis[edge[i].to]>dis[x]+edge[i].v)
		{
			dis[edge[i].to]=dis[x]+edge[i].v;
			if(!vis[edge[i].to])Q[T=T%n+1]=edge[i].to,vis[edge[i].to]=1;
		}
	}
}
inline void SPFA2()
{
	for(int i=1;i<=n;i++)vis[i]=0,dis2[i]=1e9;
	vis[n]=1;dis2[n]=0;Q[1]=n;
	int H=0,T=1;
	while(H!=T)
	{
		int x=Q[H=H%n+1];vis[x]=0;
		for(int i=Head[x];i!=-1;i=Edge[i].nxt)if(dis2[Edge[i].to]>dis2[x]+Edge[i].v)
		{
			dis2[Edge[i].to]=dis2[x]+Edge[i].v;
			if(!vis[Edge[i].to])Q[T=T%n+1]=Edge[i].to,vis[Edge[i].to]=1;
		}
	}
}
inline void add(int&x,int y){x=(x+y)%p;}
void getans(int x,int y)
{
	if(calced[x][y])return;
	calced[x][y]=1;
	for(int i=Head[x];i!=-1;i=Edge[i].nxt)
	{
		int j=dis[x]+y-dis[Edge[i].to]-Edge[i].v;
		if(j<0||j>k)continue;
		getans(Edge[i].to,j),add(f[x][y],f[Edge[i].to][j]);
	}
}
inline bool check()
{
	for(int i=1;i<=n;i++)if(!G.dfn[i])G.tarjan(i);
	for(int i=1;i<=G.cnt;i++)mn1[i]=1e9,mn2[i]=1e9,num[i]=0;
	for(int i=1;i<=n;i++)
	{
		if(dis[i]<mn1[G.fa[i]])mn1[G.fa[i]]=dis[i];
		if(dis2[i]<mn2[G.fa[i]])mn2[G.fa[i]]=dis2[i];
		num[G.fa[i]]++;
	}
	for(int i=1;i<=G.cnt;i++)if(num[i]>1&&mn1[i]+mn2[i]<=dis[n]+k)return true;
	return false;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n),read(m),read(k),read(p);
		memset(head,-1,sizeof(head));
		tot=0;
		memset(Head,-1,sizeof(Head));
		Tot=0;
		G.init();
		int flag=0;
		for(int i=1;i<=m;i++)
		{
			int x,y,z;read(x),read(y),read(z);
			edge[++tot]=(E){y,z,head[x]};head[x]=tot;
			Edge[++Tot]=(E){x,z,Head[y]};Head[y]=Tot;
			if(z==0)flag=1,G.addedge(x,y);
		}
		SPFA();
		SPFA2();
		if(flag&&check()){puts("-1");continue;}
		memset(calced,0,sizeof(calced));
		memset(f,0,sizeof(f));
		calced[1][0]=1;f[1][0]=1;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=k;j++)getans(i,j);
		int ans=0;
		for(int i=0;i<=k;i++)add(ans,f[n][i]);
		printf("%d\n",ans);
	}
	return 0;
}

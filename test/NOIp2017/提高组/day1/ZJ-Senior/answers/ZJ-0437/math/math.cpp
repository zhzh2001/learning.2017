#include<cstdio>
typedef long long LL;
void exgcd(LL a,LL b,LL&d,LL&x,LL&y)
{
	if(!b){d=a;x=1;y=0;return;}
	exgcd(b,a%b,d,y,x);y-=x*(a/b);
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	LL a,b,d,x,y,ans=0;
	scanf("%lld%lld",&a,&b);
	exgcd(a,b,d,x,y);
	if(x<0)
	{
		ans+=a*(-1-x);
		x+=b;y-=a;
		ans+=b*(-1-y);
	}
	else
	{
		ans+=b*(-1-y);
		x-=b;y+=a;
		ans+=a*(-1-x);
	}
	printf("%lld\n",ans+1);
	return 0;
}

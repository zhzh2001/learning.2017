#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;
inline int read(){
	char ch=getchar(); int x=0; bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int maxn=100005,maxm=200005,oo=1000000005;
int vis[maxn],que[maxn*55],f[maxn*55],zs[maxn],l,r,sum,ans,rd[maxn*55],tong[maxn][55],nedge,n,m,k,mod,dist[maxn][2],nextt[maxm*55],di[maxm*55],ed[maxm*55],son[maxn*55],a[maxm],b[maxm],c[maxm];
int spfa(int s,int t,int id){
	int l=0,r=0,tot=1;
	for(int i=1;i<=n;i++){dist[i][id]=oo; vis[i]=0;}
	dist[s][id]=0; que[0]=s; vis[s]=1;
	while(tot--){
		int k=que[l]; if(++l==n)l=0; vis[k]=0;
		for(int i=son[k];i;i=nextt[i])if(dist[ed[i]][id]>dist[k][id]+di[i]){
			dist[ed[i]][id]=dist[k][id]+di[i]; if(!vis[ed[i]]){if(++r==n)r=0; que[r]=ed[i]; tot++; vis[ed[i]]=1;}
		}
	}
	return dist[t][id];
}
inline void aedge(int a,int b,int c){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b; di[nedge]=c;
}
int main(){
	freopen("park.in","r",stdin); freopen("park.out","w",stdout);
	int T=read();
	while(T--){
		n=read(); m=read(); k=read(); mod=read();
		for(int i=1;i<=n;i++){son[i]=0; for(int j=0;j<=k;j++)tong[i][j]=0;}nedge=0;
		for(int i=1;i<=m;i++){
			a[i]=read(); b[i]=read(); c[i]=read();
			aedge(b[i],a[i],c[i]);
		}
		spfa(n,1,0);
		for(int i=1;i<=n;i++)zs[i]=son[i]=0; nedge=sum=0;
		for(int i=1;i<=m;i++)aedge(a[i],b[i],c[i]);
		int t=spfa(1,n,1);
		for(int i=1;i<=n;i++)for(int j=0;dist[i][0]+dist[i][1]+j<=t+k;j++){
			tong[i][j]=++sum; zs[i]=j+dist[i][1];
		}
		ans=l=r=nedge=0; 
		for(int i=1;i<=sum;i++)f[i]=son[i]=rd[i]=0;
		for(int i=1;i<=m;i++){
			for(int j=0;j<=zs[a[i]]&&dist[a[i]][1]+c[i]+j<=zs[b[i]];j++){
			    int t=tong[b[i]][dist[a[i]][1]+j+c[i]-dist[b[i]][1]];
				aedge(tong[a[i]][j],t,0); rd[t]++;
			}
		}
		f[tong[1][0]]=1;
		for(int i=1;i<=sum;i++)if(!rd[i])que[++r]=i;
		while(l<r){
			int k=que[++l];
			for(int i=son[k];i;i=nextt[i]){if(--rd[ed[i]]==0){
				que[++r]=ed[i];
			}f[ed[i]]+=f[k]; if(f[ed[i]]>=mod)f[ed[i]]-=mod;}
		}
		if(r!=sum){puts("-1"); continue;}
		for(int i=0;i<=k;i++)ans=(ans+f[tong[n][i]])%mod;
		cout<<ans<<endl;
	}
	return 0;
}

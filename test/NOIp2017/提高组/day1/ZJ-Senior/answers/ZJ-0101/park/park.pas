var i,j,k,l,n,m,t,x,y,z,p,min,ans:longint;
a,b,c,ne,f,la:array[0..2017] of longint;
bb:array[0..2017] of boolean;
procedure dfs(x,y:longint);
var i,j,k:longint;
begin
  if y>min then exit;
  if x=n then
  begin
    if y=min then ans:=(ans+1)mod p else
    if y<min then
    begin
      min:=y;
      ans:=1 mod p;
    end;
    exit;
  end;
  i:=f[x];
  while i>0 do
  begin
    if bb[b[i]]=false then
    begin
      bb[b[i]]:=true;
      dfs(b[i],y+c[i]);
      bb[b[i]]:=false;
      i:=ne[i];
    end;
  end;
end;
begin
  assign(input,'park.in');reset(input);assign(output,'park.out');rewrite(output);
  readln(t);
  for l:=1 to t do
  begin
    read(n,m,k,p);
    if k=0 then
    begin
    fillchar(ne,sizeof(ne),0);
    fillchar(f,sizeof(f),0);
    fillchar(la,sizeof(la),0);
    fillchar(bb,sizeof(bb),false);
    min:=maxlongint div 3;
    ans:=0;
    for i:=1 to m do
    begin
      readln(a[i],b[i],c[i]);
      if f[a[i]]=0 then f[a[i]]:=i;
      ne[la[a[i]]]:=i;
      la[a[i]]:=i;
    end;
    bb[1]:=true;dfs(1,0);
    writeln(ans);
    end else writeln(-1);
  end;
  close(input);close(output);
end.

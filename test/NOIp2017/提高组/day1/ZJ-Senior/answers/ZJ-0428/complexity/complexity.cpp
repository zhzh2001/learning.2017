#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
inline void ju() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
}
int T, n, cnt, sum, ans;
char t[12], s[12], id[12], fst[12], lst[12];
char a[1100];
bool used[30], b[1100], gg, ab[1100];
inline int va(char *ch) {
	int l = strlen(ch+1), x = 0;
	for(int i = 1; i <= l; ++i) x = x*10+ch[i]-'0';
	return x;
}
int main() {
	ju();
	scanf("%d", &T);
	while(T--) {
		ans = cnt = sum = 0; gg = 0;
		memset(t, 0, sizeof t);
		memset(used, 0, sizeof used);
		memset(b, 0, sizeof b);
		memset(ab, 0, sizeof ab); ab[0] = 1;
		scanf("%d", &n); scanf("%s", t+1);
		for(int i = 1; i <= n; ++i) {
			scanf("%s", s+1);
			if(s[1]=='F') {
				scanf("%s%s%s", id+1, fst+1, lst+1);
				if(used[id[1]-'a']) gg = 1;
				used[id[1]-'a'] = 1; a[++cnt] = id[1];
				if(fst[1]!='n'&&lst[1]=='n') b[cnt] = 1, ++sum; else b[cnt] = 0;
				if(!ab[cnt-1]||(fst[1]=='n'&&lst[1]!='n')||(fst[1]!='n'&&lst[1]!='n'&&va(fst)>va(lst))) ab[cnt] = 0; else ab[cnt] = 1;
				if(ab[cnt]) ans = max(ans, sum);
			}
			else {
				if(!cnt) {gg = 1; cnt = 1000;}
				used[a[cnt]-'a'] = 0; if(b[cnt]) --sum;
				--cnt;
			}
		}
		//
		//printf("%d \n", ans);
		//
		if(cnt) gg = 1;
		if(gg) {puts("ERR"); continue;}
		if(t[3]=='1') {
			if(ans==0) puts("Yes");
			else puts("No");
		}
		else {
			int x = 0, l = strlen(t+1);
			for(int i = 5; i <= l-1; ++i) x = x*10+t[i]-'0';
			if(x==ans) puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

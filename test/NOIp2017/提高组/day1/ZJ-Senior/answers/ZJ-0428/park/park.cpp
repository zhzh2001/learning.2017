#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
using namespace std;
inline void ju() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
}
const int N = 202000;
int n, m, k, mo;
int cnt, fst[N], nxt[N], to[N], len[N];
struct st{
	int id, v;
	bool operator < (const st &t) const {
		return v > t.v;
	}
};
int d[N], f[N][55];
bool vis[N];
priority_queue<st> Q;
inline void add(int x, int y, int z) {
	nxt[++cnt] = fst[x]; fst[x] = cnt; to[cnt] = y; len[cnt] = z; 
}
inline void dij() {
	Q.push((st){1, 0}); f[1][0] = 1; d[1] = 0;
	while(!vis[n]) {
		//printf("%d\n", vis[n]);
		st t = Q.top(); Q.pop(); while(!Q.empty()&&vis[t.id]) t = Q.top(), Q.pop();
		vis[t.id] = 1;
		for(int i = fst[t.id]; i; i = nxt[i])
			if(!vis[to[i]]) {
				int u = t.id, v = to[i], w = len[i];
				if(t.v+w<=d[v]) {
					if(t.v+w+k<d[v]) {
						for(int j = 0; j <= k; ++j) f[v][j] = f[u][j];
						d[v] = t.v+w; Q.push((st){v, d[v]});
						continue;
					}
					for(int j = t.v+w+k; j >= d[v]; --j) f[v][j-t.v-w] = f[v][j-d[v]];
					for(int j = t.v+w; j < d[v]; ++j) f[v][j-t.v-w] = 0;
					for(int j = 0; j <= k; ++j) f[v][j] = (f[v][j]+f[u][j])%mo;
					d[v] = t.v+w; Q.push((st){v, d[v]});
				}
				else {
					for(int j = t.v+w; j <= d[v]+k; ++j) f[v][j-d[v]] = (f[v][j-d[v]]+f[u][j-t.v-w])%mo;
				}
			}
	}
	return;
}
int main() {
	ju();
	int T;
	scanf("%d", &T);
	while(T--) {
		while(!Q.empty()) Q.pop();
		memset(f, 0, sizeof f);
		memset(fst, 0, sizeof fst);
		memset(vis, 0, sizeof vis);
		cnt = 0;
		scanf("%d%d%d%d",&n,&m,&k,&mo);
		for(int i = 1; i <= m; ++i) {
			int x, y, z; scanf("%d%d%d", &x, &y, &z); add(x, y, z);
		}
		memset(d, 0x3f, sizeof d);
		
		dij();
		int ans = 0;
		/*for(int j = 1; j <= n; ++j) {
			printf("%d:%d\n", j, d[j]);
			for(int i = 0; i <= k; ++i) printf("%d ", f[j][i]); puts("");
		}*/
		for(int i = 0; i <= k; ++i) ans = (ans+f[n][i])%mo;
		printf("%d\n", ans);
	}
	return 0;
}

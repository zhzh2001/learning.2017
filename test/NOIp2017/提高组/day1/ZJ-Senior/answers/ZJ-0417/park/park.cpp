#include <cstdio>
int TC, N, M, K, head[100001], next[200001], to[200001], len[200001], d[100001], q[100001], TIME, MOD, f[51][100001];
int u[200001], v[200001], w[200001];
int dis[100001], T[262144], Tref[100001];
int dis1[100001], disN[100001];
int topovisited[100001];
int topo[100001];
inline void add(int &x, int y)
{
	x = x + y < MOD ? x + y : x + y - MOD;
}
inline void up(int p)
{
	T[p] = dis[T[p << 1]] <= dis[T[p << 1 | 1]] ? T[p << 1] : T[p << 1 | 1];
}
void build(int p, int l, int r)
{
	if (l == r)
	{
		T[p] = l;
		Tref[l] = p;
		return;
	}
	int m = l + r >> 1;
	build(p << 1, l, m);
	build(p << 1 | 1, m + 1, r);
	up(p);
}
int G()
{
	int x = T[1], p = Tref[x];
	T[p] = 0;
	while (p >>= 1)
		up(p);
	return x;
}
void update(int x)
{
	for (int p = Tref[x]; p >>= 1; )
		up(p);
}
void Dijkstra(int start)
{
	for (int i = 1; i <= N; i++)
		dis[i] = 1000000000;
	dis[start] = 0;
	dis[0] = 1000000000;
	build(1, 1, N);
	int u;
	while (u = G())
	{
		for (int e = head[u]; e; e = next[e])
			if (dis[to[e]] > dis[u] + len[e])
			{
				dis[to[e]] = dis[u] + len[e];
				update(to[e]);
			}
	}
}
inline bool ON(int x)
{
	return dis1[x] + disN[x] <= dis1[N] + K;
}
int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	for (scanf("%d", &TC); TC--; )
	{
		scanf("%d%d%d%d", &N, &M, &K, &MOD);
		for (int i = 1; i <= M; i++)
			scanf("%d%d%d", u + i, v + i, w + i);
		for (int i = 1; i <= N; i++)
			head[i] = 0;
		for (int i = 1; i <= M; i++)
		{
			next[i] = head[v[i]];
			to[i] = u[i];
			len[i] = w[i];
			head[v[i]] = i;
		}
		Dijkstra(N);
		for (int i = 1; i <= N; i++)
			disN[i] = dis[i];
		for (int i = 1; i <= N; i++)
			head[i] = 0;
		for (int i = 1; i <= M; i++)
		{
			next[i] = head[u[i]];
			to[i] = v[i];
			len[i] = w[i];
			head[u[i]] = i;
		}
		Dijkstra(1);
		for (int i = 1; i <= N; i++)
			dis1[i] = dis[i];
		for (int i = 1; i <= N; i++)
			head[i] = 0;
		for (int i = 1; i <= N; i++)
			d[i] = 0;
		for (int i = 1; i <= N; i++)
			topovisited[i] = 0;
		for (int i = 1; i <= M; i++)
			if (ON(u[i]) && ON(v[i]) && dis1[v[i]] == dis1[u[i]] + w[i])
			{
				next[i] = head[u[i]];
				to[i] = v[i];
				len[i] = w[i];
				head[u[i]] = i;
				d[v[i]]++;
			}
		int D = 0;
		for (int i = 1; i <= N; i++)
			if (!d[i])
				q[++D] = i;
		TIME = 0;
		while (D)
		{
			int u = q[D--];
			topo[++TIME] = u;
			topovisited[u] = TIME;
			for (int e = head[u]; e; e = next[e])
				if (!--d[to[e]])
					q[++D] = to[e];
		}
		bool ans_is_inf = false;
		for (int i = 1; i <= N; i++)
			if (!topovisited[i])
			{
				puts("-1");
				ans_is_inf = true;
				break;
			}
		if (ans_is_inf)
			continue;
		for (int i = 1; i <= N; i++)
			head[i] = 0;
		do
		{
			int _TIME = 0;
			for (int i = 1; i <= TIME; i++)
				if (ON(topo[i]))
					topo[++_TIME] = topo[i];
			TIME = _TIME;
			for (int i = 1; i <= TIME; i++)
				topovisited[topo[i]] = i;
		}
		while (0);
		for (int i = 1; i <= M; i++)
			if (dis1[u[i]] + w[i] + disN[v[i]] <= dis1[N] + K)
			{
				next[i] = head[topovisited[u[i]]];
				to[i] = topovisited[v[i]];
				len[i] = w[i] - dis1[v[i]] + dis1[u[i]];
				head[topovisited[u[i]]] = i;
			}
		for (int i = 0; i <= K; i++)
			for (int j = 1; j <= TIME; j++)
				f[i][j] = 0;
		f[0][topovisited[1]] = 1 % MOD;
		for (int i = 0; i <= K; i++)
			for (int j = 1; j <= TIME; j++)
				for (int e = head[j]; e; e = next[e])
				{
					int ti = i + len[e];
					if (ti <= K)
						add(f[ti][to[e]], f[i][j]);
				}
		int O = 0;
		for (int i = 0; i <= K; i++)
			add(O, f[i][topovisited[N]]);
		printf("%d\n", O);
	}
	return 0;
}

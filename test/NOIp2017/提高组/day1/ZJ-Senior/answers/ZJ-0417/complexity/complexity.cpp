#include <cstdio>
#include <set>
#include <vector>
#include <algorithm>
int TC;
int I_n()
{
	int r = 0, c;
	do c = getchar(); while ((c < 48 || c > 57) && c != 'n');
	if (c == 'n')
		return -1;
	do r = (r << 3) + r + r + (c - 48), c = getchar(); while (c > 47 && c < 58);
	return r;
}
int I_O()
{
	static char f[10000];
	scanf("%s", f);
	if (f[2] == '1')
		return 0;
	int r = 0;
	for (int i = 4; f[i] != ')'; i++)
		r = (r << 3) + r + r + (f[i] - 48);
	return r;
}
struct tok
{
	char type; // = 'F', 'E'
	int i, len;
};
tok I_S()
{
	char type;
	do type = getchar(); while (type != 'F' && type != 'E');
	if (type == 'E')
		return (tok) { 'E', 0, 0 };
	int i;
	do i = getchar(); while (i < 'a' || i > 'z');
	int x = I_n(), y = I_n(), len;
	if (x == -1 && y == -1)
		len = 0;
	else if (x == -1)
		len = -100000;
	else if (y == -1)
		len = 1;
	else if (x <= y)
		len = 0;
	else
		len = -100000;
	return (tok) { 'F', i, len };
}
tok sent[100001];
int Main()
{
	int N = I_n(), stdD = I_O(), ansD = 0;
	std::set < int > names;
	std::vector < std::pair < int, int > > namev;
	for (int i = 1; i <= N; i++)
		sent[i] = I_S();
	for (int i = 1, dep = 0; i <= N; i++)
		if (sent[i].type == 'F')
		{
			if (names.find(sent[i].i) != names.end())
				return 2;
			names.insert(sent[i].i);
			namev.push_back(std::make_pair(sent[i].i, sent[i].len));
			ansD = std::max(ansD, dep += sent[i].len);
		}
		else
		{
			if (namev.empty())
				return 2;
			names.erase(namev.back().first);
			dep -= namev.back().second;
			namev.resize(namev.size() - 1);
		}
	return namev.empty() ? stdD != ansD : 2;
}
int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	for (scanf("%d", &TC); TC--; )
	{
		int code = Main();
		puts(code == 0 ? "Yes" : (code == 1 ? "No" : "ERR"));
	}
	return 0;
}

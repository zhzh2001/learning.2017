#include<cstdio>
using namespace std;
int Q,N,M,RM,ttM,tM,ttmp,tmp,J,CS;char t,S[100];bool U[30],P;
void read(int &n)
{
	if ((t=getchar())=='n') {n=0;return;}n=t-'0';
	while ((t=getchar())>='0' && t<='9') n=n*10+t-'0';
}
inline void refresh() {tM=ttM>tM?ttM:tM;}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&Q);
	for (int ident=0;ident<Q;++ident)
	{
		scanf("%d%*c%*c%*c",&N);
		scanf("%c",&t);
		if (t=='n') scanf("%*c%d",&M);
		else M=0;
		scanf("%*c%*c");
		tM=ttM=0;
		for (int i=0;i<=26;++i)
			U[i]=0;
		CS=0;P=0;
		for (int i=0;i<N;++i)
		{
			scanf("%c",&t);
			if (P) goto NXT;
			if (J)
			{
				if (t=='F')
				{
					scanf("%*c%c",&t);
					if (U[t-'a']) {P=1;goto NXT;}
					U[t-'a']=1;S[CS++]=t;
					J++;
				}
				else if (t=='E')
				{
					if (!CS) {P=1;goto NXT;}
					t=S[--CS];if (t=='{') t=S[--CS];
					U[t-'a']=0;
					J--;
				}
				goto NXT;
			}
			if (t=='F')
			{
				scanf("%*c%c%*c",&t);
				if (U[t-'a']) {P=1;goto NXT;}
				U[t-'a']=1;S[CS++]=t;read(tmp);
				if (!tmp)
				{
					read(tmp);
					if (!tmp) goto NXT;
					else {J=1;goto NXT;}
				}
				else 
				{
					read(ttmp);
					if (!ttmp) {ttM++;S[CS++]='{';refresh();}
					else if (ttmp<tmp) {J=1;goto NXT;}
				}
			}
			else if (t=='E')
			{
				if (!CS) {P=1;goto NXT;}
				t=S[--CS];
				if (t=='{') {ttM--;t=S[--CS];}
				U[t-'a']=0;
			}
			NXT:while (t!='\n') t=getchar();
		}
		if (P || CS) printf("ERR\n");
		else if (tM==M) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

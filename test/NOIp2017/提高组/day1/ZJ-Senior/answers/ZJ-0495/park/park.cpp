#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

#define Rep(i,x,y) for (i=x;i<=y;++i)

const int N = 105000;

int n,m,K,Mod,tot,Elast[N<<1],Enext[N<<1],To[N<<1],Ep[N<<1],x,y,c,Dis[N<<1],i,j,h,t,Q[N<<1],rAns,Ans,Visit[N<<1],Visitdis,F[N][51],T;
bool V[101000];

struct Queue {
	int x,y;
	bool operator < (const Queue &t) const {
		return Dis[t.x]+t.y>Dis[x]+y;
	}
};

void Add(int x,int y,int c) {
	To[++tot]=y;  Enext[tot]=Elast[x];  Elast[x]=tot;  Ep[tot]=c;
}

priority_queue<Queue> Qt;

int main () {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d%d%d%d",&n,&m,&K,&Mod),tot=0;
		Rep(i,1,n) Elast[i]=0;
		Rep(i,1,m) scanf("%d%d%d",&x,&y,&c),Add(x,y,c);
		Rep(i,1,n) Dis[i]=1<<24;
		h=0,t=1,Q[1]=1,Visit[1]=1,Dis[1]=0;
		Rep(i,1,n) Rep(j,0,K) F[i][j]=0;
		F[1][0]=1;
		while (h<t) {
		    x=Q[++h],Visit[x]=0;
			for (i=Elast[x];i;i=Enext[i]) {
			   	if (Dis[x]+Ep[i]==Dis[To[i]]) F[To[i]][0]=(F[To[i]][0]+F[x][0])%Mod;
				if (Dis[x]+Ep[i]<Dis[To[i]]) {
					Dis[To[i]]=Dis[x]+Ep[i];
					F[To[i]][0]=F[x][0];
					if (!Visit[To[i]]) Q[++t]=To[i],Visit[To[i]]=1;
				}
			}
		}
		Visitdis=Dis[n];  Ans=F[n][0];
		if (!K) {  printf("%d\n",Ans);  continue;  }
		Rep(i,1,n) Rep(j,0,K) F[i][j]=0;
		F[1][0]=1;
		while (!Qt.empty()) Qt.pop();
		Rep(i,1,n) Rep(j,0,K) V[(i-1)*(K+1)+j+1]=0;
		Qt.push((Queue){1,0}),V[1]=1;
		while (!Qt.empty()) {
			while (!Qt.empty() && V[(Qt.top().x-1)*(K+1)+Qt.top().y+1]) Qt.pop();
			x=Qt.top().x,y=Qt.top().y,V[(x-1)*(K+1)+y+1]=1,Qt.pop();
			for (i=Elast[x];i;i=Enext[i]) if (Dis[x]+y+Ep[i]<=Dis[To[i]]+K) {
				F[To[i]][Dis[x]+y+Ep[i]-Dis[To[i]]]=(F[To[i]][Dis[x]+y+Ep[i]-Dis[To[i]]]+F[x][y])%Mod;
				Qt.push((Queue){To[i],Dis[x]+y+Ep[i]-Dis[To[i]]});
			}
		}
		Rep(i,1,K) Ans=(Ans+F[n][i])%Mod;
	    printf("%d\n",Ans);
	}
	fclose(stdin);  fclose(stdout);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>

using namespace std;

#define Rep(i,x,y) for (i=x;i<=y;++i)

const int N = 105000;

struct Node {
	int t,x,y;
};

int t,x,i,L,sF,sE,n,p,y,Error,now,top,nowt,Used[N],tot,Max,stack[N],stackPos[N];
int j;
char c[N],Modifys[N],Modifyx[N],Modifyy[N];
Node A[N];

int main () {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--) {
		scanf("%d",&L);  scanf("%s",c+1);  n=strlen(c+1);
		x=0;  Rep(i,1,n) if (c[i]=='^') {  x=1;  break;  }
		if (x) {
			x=0;
		    for (++i;i<=n;++i) if (c[i]>='0' && c[i]<='9') x=x*10+c[i]-48;  else break;
		}
		p=x;
		sF=0;  sE=0;
		Error=0;  tot=0;
		while (L--) {
			scanf("%s",c+1);
			if (c[1]=='F') {
			    scanf("%s",Modifys+1);  scanf("%s",Modifyx+1);  scanf("%s",Modifyy+1);
			    if (Modifyx[1]=='n') x=-1;  else {
			    	n=strlen(Modifyx+1);  x=0;
			    	Rep(i,1,n) x=x*10+Modifyx[i]-48;
				}
				if (Modifyy[1]=='n') y=-1;  else {
					n=strlen(Modifyy+1);  y=0;
					Rep(i,1,n) y=y*10+Modifyy[i]-48;
				}
			    A[++tot]=(Node){Modifys[1]-'a'+1,x,y};
			    ++sF;
			}
			if (c[1]=='E') {
			    A[++tot]=(Node){0,-1,-1};
				++sE;
			}
		}
		if (sF!=sE) Error=-1;
		now=0;  top=0;  nowt=0;
		Rep(i,0,40000) Used[i]=0;
		Max=0;
		Rep(i,1,tot) {
			if (!A[i].t) {
				Used[nowt]=0;
			    --top;
			    now=stack[top];
			    nowt=stackPos[top];
			    continue;
			}
			if (A[i].t) {
			    if (Used[A[i].t]) {  Error=-1;  break;  }
				Used[A[i].t]=1;
				if (now==-1) {  stack[++top]=now;  stackPos[top]=A[i].t;  nowt=A[i].t;  continue;  }
				if (A[i].x!=-1 && A[i].y!=-1) {
					if (A[i].x<=A[i].y) now=now+0;
					if (A[i].x>A[i].y) now=-1;
				}
				if (A[i].x!=-1 && A[i].y==-1) now=now+1;
				if (A[i].x==-1 && A[i].y!=-1) now=-1;
				if (A[i].x==-1 && A[i].y==-1) now=now+0;
                stack[++top]=now;
                stackPos[top]=A[i].t;
                nowt=A[i].t;
                Max=max(Max,stack[top]);
			}
		}
		if (Error==-1) printf("ERR\n");
		else if (Max!=p) printf("No\n");
		else printf("Yes\n");
	}
	fclose(stdin);  fclose(stdout);
	return 0;
}

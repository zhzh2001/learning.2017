#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>

using namespace std;

#define Rep(i,x,y) for (i=x;i<=y;++i)

#define LL long long

LL a,b;

int main () {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (!(a&1)) swap(a,b);
	printf("%lld\n",(a-2)+(b-2)*(a-1));
	fclose(stdin);  fclose(stdout);
	return 0;
}

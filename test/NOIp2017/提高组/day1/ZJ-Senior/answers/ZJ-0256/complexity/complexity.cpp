#include<cstdio>
#include<cstring>
int n[305],t;
char s[305][100];
int pd(int w,int max){
	if (w==max) printf("Yes\n"); else printf("No\n");
	return 0;
}
bool cmp(char *a,int len){
	int la=strlen(a);
	for (int i=1;i<=len;i++){
		if (la!=strlen(s[i])) continue;
		bool e=true;
		for (int j=0;j<la;j++) if (a[j]!=s[i][j]) e=false;
		if (e) return false;
	}
	return true;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d\n",&t);
	for (;t>0;t--){
		int l=0,w=0,max=0,len=0,tans=0;
		char st[100],ch;
		memset(s,0,sizeof(s));
		memset(n,0,sizeof(n));
		memset(st,0,sizeof(st));
		scanf("%d %s",&l,st);
		if (st[2]=='1') w=0; else {for (int p=4;st[p]!=')';p++) w=w*10+st[p]-48;}
		getchar();
		bool err=false,can=true;
		for (int i=0;i<l;i++){
			scanf("%c",&ch);
			if (i==0&&ch=='E'&&l==0) {pd(w,0); break;}
			if (ch=='F'){
				len++; int p=0,x=0,y=0;
				getchar(); ch=getchar();
				while (ch!=' '){s[len][p]=ch; p++; ch=getchar();}
				ch=getchar();
				if (ch=='n') {x=-1; getchar();}
				else while (ch>='0'&&ch<='9') {x=x*10+ch-48; ch=getchar();}
				ch=getchar();
				if (ch=='n') {y=-1; getchar();}
				else while (ch>='0'&&ch<='9') {y=y*10+ch-48; ch=getchar();}
				if (err) continue;
				if (!cmp(s[len],len-1)) {err=true; printf("ERR\n"); continue;}
				if (!can) n[len]=0;
				else {
					if ((x==-1&&y>-1)) {can=false; n[len]=-1;}
					else if (x>-1&&y==-1) {n[len]=1; if (can) tans++; max=(max>tans)?max:tans;}
					else if (x<=y) n[len]=0;
					else {can=false; n[len]=-1;}
				}
			}
			else {
				if (err) {getchar(); continue;}
				if (len<1) {err=true; getchar(); len=100; printf("ERR\n"); continue;}
				getchar();
				for (int llll=strlen(s[len]),j=0;j<llll;j++) s[len][j]=0;
				if (n[len]==1) tans--;
				else if (n[len]==-1) can=true;
				len--;
				}
			}
		if ((!err)&&len>0) {err=true; printf("ERR\n");}
		if (!err) pd(max,w);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

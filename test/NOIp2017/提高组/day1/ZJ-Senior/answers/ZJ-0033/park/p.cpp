#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string>
#include<cmath>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<bitset>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
void getint(int &x){ //x>=0
	char c=getchar();
	while (c<'0' || c>'9') c=getchar();
	x=0;
	for (;c>='0' && c<='9';c=getchar()) x=x*10+c-48;
}
const int qs=1e7;
const int INF=1e9+10;
int hed[500010],MO,T,k,f[100010][55],q[qs],P[500010],Q[500010],NEDGE,ans,too[500010],nxt[500110],TOO[500010],NXT[500010],HED[500010],val[500010],VAL[500010],nedge,n,m,a[500010],b[500010],c[500010];
bool lj[500010],u[500010];
void ae(int x,int y,int w){
	nxt[++nedge]=hed[x];
	hed[x]=nedge;
	too[nedge]=y;
	val[nedge]=w;
}
void AE(int x,int y,int w){
	NXT[++NEDGE]=HED[x];
	HED[x]=NEDGE;
	TOO[NEDGE]=y;
	VAL[NEDGE]=w;
}
void spfa0(){
	int he=0,ta=1;
	q[1]=n;
	FOR(i,1,n) Q[i]=INF;
	Q[n]=0;
	FOR(i,1,n) u[i]=0;
	u[n]=1;
	while (he!=ta){
		int x=q[(++he)%=qs];
		u[x]=0;
		for (int i=hed[x];i;i=nxt[i]){
			int y=too[i];
			if (Q[x]+val[i]>=Q[y]) continue;
			Q[y]=Q[x]+val[i];
			if (!u[y]) u[y]=1,q[(++ta)%=qs]=y;
		}
	}
}
void spfa1(){
	int he=0,ta=1;
	q[1]=1;
	FOR(i,1,n) P[i]=INF;
	P[1]=0;
	FOR(i,1,n) u[i]=0;
	u[1]=1;
	while (he!=ta){
		int x=q[(++he)%=qs];
		u[x]=0;
		for (int i=HED[x];i;i=NXT[i]){
			int y=TOO[i];
			if (P[x]+VAL[i]>=P[y]) continue;
			P[y]=P[x]+VAL[i];
			if (!u[y]) u[y]=1,q[(++ta)%=qs]=y;
		}
	}
}
int dfs(int x,int K){
	if (f[x][K]!=-1) return f[x][K];
	f[x][K]=0;
	for (int i=hed[x];i;i=nxt[i]){
		int y=too[i];
		if (lj[y]) continue;
		int t=K+P[x]-val[i]-P[y];
		if (t<0 || t>k) continue;
		(f[x][K]+=dfs(y,t))%=MO;
	}
	return f[x][K];
}
void que(){
	scanf("%d%d%d%d",&n,&m,&k,&MO);
	FOR(i,1,n) hed[i]=0,HED[i]=0;
	nedge=0;
	NEDGE=0;
	FOR(i,1,m){
		getint(a[i]),getint(b[i]),getint(c[i]);
		ae(b[i],a[i],c[i]);
		AE(a[i],b[i],c[i]);
	}
	spfa0();
	spfa1();
	//pan -1
	ans=0;
	FOR(i,1,n)
		if (P[i]+Q[i]>P[n]+k) lj[i]=1; else lj[i]=0;
	memset(f,-1,sizeof(f));
	f[1][0]=1;
	FOR(i,0,k)
		FOR(j,1,n)
			if (!lj[j]) dfs(j,i);
	FOR(i,0,k) (ans+=f[n][i])%=MO;
	printf("%d\n",ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--) que();
	return 0;
}

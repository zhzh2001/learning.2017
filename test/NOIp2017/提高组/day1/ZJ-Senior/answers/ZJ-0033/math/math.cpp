#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string>
#include<cmath>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<bitset>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
LL a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	cin>>a>>b;
	if (a>b) swap(a,b);
	cout<<a*(b-1)-b<<endl;
	return 0;
}

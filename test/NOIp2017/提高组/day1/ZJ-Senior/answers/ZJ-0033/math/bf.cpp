#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string>
#include<cmath>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<bitset>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
int x,q[1111111],a,b,ans[1111][1111];
bool u[1111111];
void bfs(int a,int b){
	int he=0,ta=2;
	memset(u,0,sizeof(u));
	q[1]=a,q[2]=b;
	u[a]=u[b]=1;
	while (he!=ta){
		int x=q[++he]; 
		if (x+a<=a*b && !u[x+a]) u[x+a]=1,q[++ta]=x+a;
		if (x+b<=a*b && !u[x+b]) u[x+b]=1,q[++ta]=x+b;
	}
	FOR(i,1,a*b)
		if (!u[i]) ans[a][b]=max(ans[a][b],i);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("bf.out","w",stdout);
	cin>>a>>b;
				bfs(a,b);
				printf("%d\n",ans[a][b]);
			
	return 0;
}

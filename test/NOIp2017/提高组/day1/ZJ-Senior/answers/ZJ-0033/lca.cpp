#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<string>
#include<cmath>
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<bitset>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
int lca(int x,int y){
	if (dep[x]>dep[y]) swap(x,y);
	FORD(i,19,0)
		if (dep[y]-(1<<i)>=dep[x]) y=anc[y][i];
	if (x==y) return x;
	FORD(i,19,0)
		if (anc[x][i]!=anc[y][i]) x=anc[x][i],y=anc[y][i];
	return anc[x][0];
}
int main(){
	

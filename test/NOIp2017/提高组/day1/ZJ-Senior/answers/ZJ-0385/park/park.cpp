#include<iostream>
#include<cstdio>
#include<cstring>
#include<limits>
using namespace std;
int n,m,k,p;
long long int ans;
int data[1005][1005];
int number[1005][1005];
//int book[1005][1005];
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t,a,b,c;
	scanf("%d",&t);
	for(;t>0;t--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=n;i++)
		{
			memset(data[i],INT_MAX/3,sizeof(data[i]));
			//memset(book[i],0,sizeof(book[i]));
			memset(number[i],0,sizeof(number[i]));
		}
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&a,&b,&c);
			//data[a][b]=c;
			if(data[a][b]>c)
			{
				number[a][b]=1;
				data[a][b]=c;
			}
			else if(data[a][b]==c)
			{
				number[a][b]++;
			}
		}
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
				{
					if(data[i][k]+data[k][j]<data[i][j])
					{
						data[i][j]=data[i][k]+data[k][j];
						number[i][j]=number[i][k]+number[k][j];
						number[i][j]%=p;
					}
					else if(data[i][k]+data[k][j]==data[i][j])
					{
						number[i][j]+=number[i][k]+number[k][j];
						number[i][j]%=p;
					}
				}
		printf("%d\n",number[1][n]);
	}
	return 0;
}

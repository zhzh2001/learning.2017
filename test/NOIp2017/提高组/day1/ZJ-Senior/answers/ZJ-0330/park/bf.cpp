#include<cstdio>
#include<algorithm>
#include<string.h>
using namespace std;
#define rt register int
#define ll long long
#define rep(i,x,y) for(int i=(x); i<=(y); ++i)
#define per(i,x,y) for(int i=(x); i>=(y); --i)
#define travel(i,x) for(int i=h[x]; i; i=pre[i])
const int N = 100005, M = 200005;
bool vis[N];
int n, m, k, p, num, ass, e[M], w[M], pre[M], h[N], q[M*51], d[N];
inline void add(int x, int y, int z){
	e[++num]=y, w[num]=z, pre[num]=h[x], h[x]=num;
}
inline void spfa(){
	int l=0, r=1;
	memset(d, 0x3f3f3f3f, sizeof d);
	memset(vis, 0, sizeof vis);
	d[1]=0;
	q[1]=1;
	while(l<r){
		int cur=q[++l];
		travel(i, cur) if(d[cur]+w[i]<d[e[i]]){
			d[e[i]]=d[cur]+w[i];
			if(!vis[e[i]]) vis[e[i]]=1, q[++r]=e[i];
		}
		vis[cur]=0;
	}
}
void dfs(int x, int dis){
//	printf("%d %d\n", x, dis);
	if(x==n) ++ass;
	travel(i, x) if(dis+w[i]<=k) dfs(e[i], dis+w[i]);
}
int main(){
	freopen("park.in", "r", stdin);
//	freopen("park.out", "w", stdout);
	static int T;
	scanf("%d", &T);
	while(T--){
		memset(h, 0, sizeof h); num = 0;
		scanf("%d%d%d%d", &n, &m, &k, &p);
		rep(i, 1, m){
			static int U, V, W;
			scanf("%d%d%d", &U, &V, &W);
			add(U, V, W);
		}
		spfa();
		if(d[n]<=1e9) k+=d[n];
		ass=0;
		dfs(1, 0);
		printf("%d\n", ass);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<string.h>
using namespace std;
#define rt register int
#define ll long long
#define rep(i,x,y) for(int i=(x); i<=(y); ++i)
#define per(i,x,y) for(int i=(x); i>=(y); --i)
#define travel(i,x) for(int i=h[x]; i; i=pre[i])
const int N = 100005, M = 200005;
int ass, T, n, m, k, p, num, f[N][51], mn[N], e[M], w[M], pre[M], h[N], q[M*51], d[N];
bool vis[N];
inline int min(int x, int y) {
	return x<y?x:y;
}
inline void add(int x, int y, int z) {
	e[++num]=y, w[num]=z, pre[num]=h[x], h[x]=num;
}
inline void spfa() {
	int l=0, r=1;
	memset(d, 0x3f3f3f3f, sizeof d);
	memset(vis, 0, sizeof vis);
	d[1]=0;
	q[1]=1;
	while(l<r) {
		int cur=q[++l];
		travel(i, cur) if(d[cur]+w[i]<d[e[i]]) {
			d[e[i]]=d[cur]+w[i];
			if(!vis[e[i]]) vis[e[i]]=1, q[++r]=e[i];
		}
		vis[cur]=0;
	}
}
inline void spfa2() {
	int l=0, r=1;
	q[1]=1;
	memset(f, 0, sizeof f);
	memset(mn, 0x3f3f3f3f, sizeof mn);
	memset(vis, 0, sizeof vis);
	mn[1]=0;
	f[1][0]=1;
	while(l<r) {
		int cur=q[++l];
//		printf("[%d]", cur);
		travel(i, cur) rep(j, mn[cur], k)
		if(d[cur]+j+w[i]<=d[e[i]]+k) {
			(f[e[i]][mn[e[i]]=min(mn[e[i]], d[cur]+j+w[i]-d[e[i]])]+=f[cur][j])%=p;
			if(!vis[e[i]]) vis[e[i]]=1, q[++r]=e[i];
		} else break;
		if(cur==n) rep(j, 0, k) (ass+=f[n][j])%=p;
		memset(f[cur], 0, sizeof(int)*51);
		vis[cur]=0;
		mn[cur]=1<<30;
	}
}
namespace bf {
	int ass;
	void dfs(int x, int dis) {
//	printf("%d %d\n", x, dis);
		if(x==n) ++ass;
		travel(i, x) if(dis+w[i]<=k) dfs(e[i], dis+w[i]);
	}
	void BF() {
		ass=0;
		dfs(1, 0);
		printf("%d\n", ass%p);
	}
}
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &T);
	while(T--) {
		memset(h, 0, sizeof h);
		num = 0;
		scanf("%d%d%d%d", &n, &m, &k, &p);
		rep(i, 1, m) {
			static int U, V, W;
			scanf("%d%d%d", &U, &V, &W);
			add(U, V, W);
		}
		spfa();
		if(n<=5 && m<=10) k+=d[n], bf::BF();
		else {
//		printf("%d\n", d[n]);
			ass = 0;
			spfa2();
			printf("%d\n", ass);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

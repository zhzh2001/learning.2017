#include<cstdio>
#include<algorithm>
#include<string.h>
#define rt register int
#define ll long long
#define rep(i,x,y) for(int i=(x); i<=(y); ++i)
#define per(i,x,y) for(int i=(x); i>=(y); --i)
using namespace std;
int a, b;
bool f[100000000];
inline int gcd(int x, int y){
	while(y^=x^=y^=x%=y);
	return x;
}
int main() {
//	freopen("math.in", "r", stdin);
//	freopen("bf.out", "w", stdout);
//	scanf("%d%d", &a, &b);
	a=7;
	rep(b, 1, 100){
		
	if(gcd(a, b)!=1){
		putchar('0'), putchar(' ');
		continue;
	}
	memset(f, 0, sizeof f);
	int lim=a*b;
	rep(i, 0, lim) rep(j, 0, lim) if(i*a+j*b<=lim) f[i*a+j*b]=1;
	else break;
	f[0]=0;
	per(i, lim, 0) if(!f[i]) {
		printf("%d ", i);
		break;
	}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
#define rt register int
#define ll long long
#define rep(i,x,y) for(int i=(x); i<=(y); ++i)
#define per(i,x,y) for(int i=(x); i>=(y); --i)
int a, b;
inline int gcd(int x, int y){
	while(y^=x^=y^=x%=y);
	return x;
}
const int lim=10000;
#define rd ((rand()<<16)^(rand()<<1)^(rand()&1))
int main(){
	srand(time(0));
	freopen("math.in", "w", stdout);
	int a, b;
	while(gcd(a=rd%lim+1, b=rd%lim+1)!=1);
	printf("%d %d", a, b);
	return 0;
}

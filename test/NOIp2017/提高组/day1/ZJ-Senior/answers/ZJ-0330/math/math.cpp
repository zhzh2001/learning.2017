#include<cstdio>
#include<algorithm>
using namespace std;
#define rt register int
#define ll long long
#define rep(i,x,y) for(int i=(x); i<=(y); ++i)
#define per(i,x,y) for(int i=(x); i>=(y); --i)
int a, b;
int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%d%d", &a, &b);
	if(a>b) swap(a, b);
	printf("%lld", (ll)(a-1)*b-a);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
void write(long long a)
{
	int t[50],tot;
	if(a==0)printf("0");
	while(a) t[++tot]=a%10,a/=10;
	for(int i=tot;i>0;i--)putchar(t[i]+'0');
}
long long a,b,ans,x,y;
int ta,tb;
long long x0,y0,x1,y1;
void exgcd(long long a,long long b,long long&x,long long &y)
{
	if(b==1){x=0;y=1;return;}
	long long x1,y1,t=a/b;
	exgcd(b,a%b,x1,y1);
	x=y1;
	y=x1-t*y1;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&ta,&tb);
	if(ta>tb)
	{
		int tem=ta;
		ta=tb;
		tb=tem;
	}
	if(tb==1){printf("0");return 0;}
	a=ta;b=tb;
	exgcd(a,b,x0,y0);
	x0%=b;y0%=a;
	if(x0>0)
	{
		x1=x0-b;
		y1=y0+a;
	}
	else
	{
		x1=x0;
		y1=y0;
		x0=x1+b;
		y0=y1-a;
	}
	ans=(x0-1)*a+(y1-1)*b;
	write(ans-1);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T,depth,l,w;
bool check[30];
char temppp[100];
void readln()
{
	char c;
	scanf("%c",&c);
	while(c!='\n')
	scanf("%c",&c);
}
bool run(int &ww,bool nes)
{
	if(l==0)return depth==0;
	char c1,c2;	
	ww=0;
	int tw0=0,tw1=0,w0=0;
	char t1,t2,t3,dust;
	int x,y;
	bool r=true,r0=true,ava=true;
	if(nes)
	{
		l--;
		scanf("%c",&t1);
		if(t1=='E'){readln();depth--;return r&&depth>=0;}
		if(l==0) {readln();return 0;}
		scanf("%c%c",&t2,&t2);
		if (check[t2-'a']) r=false;
		check[t2-'a']=true;
		c1=t2;
		x=y=0;
		scanf("%c%c",&t2,&t2);
		dust=t2;while(dust!=' '){if(t2!='n')x=x*10+dust-'0';scanf("%c",&dust);}
		scanf("%c",&t3);
		dust=t3;while(dust!='\n'){if(t3!='n')y=y*10+dust-'0';scanf("%c",&dust);}
		depth++;
		if(t2=='n'&&t3!='n')ava=false;
		if(t2!='n'&&t3=='n')w0=1;
		if(t2!='n'&&t3!='n')if(x>y)ava=false;
	}
	while(l>0)
	{
		l--;
		scanf("%c",&t1);
		if(t1=='E'){readln();depth--;ww=ava?ww+w0:0;if(nes)check[c1-'a']=false;return r&&depth>=0;}
		if(l==0)  {readln();return 0;}
		scanf("%c%c",&t2,&t2);
		if (check[t2-'a']) r=false;
		check[t2-'a']=true;
		c2=t2;
		x=y=0;
		scanf("%c%c",&t2,&t2);
		dust=t2;while(dust!=' '){if(t2!='n')x=x*10+dust-'0';scanf("%c",&dust);}
		scanf("%c",&t3);
		dust=t3;while(dust!='\n'){if(t3!='n')y=y*10+dust-'0';scanf("%c",&dust);}
		depth++;
		if(t2=='n'&&t3!='n'){r0=run(tw0,false);r=r&&r0;}
		if(t2=='n'&&t3=='n'){r0=run(tw0,false);r=r&&r0;ww=ww>tw0?ww:tw0;}
		if(t2!='n'&&t3=='n'){r0=run(tw0,false);r=r&&r0;tw0++;ww=ww>tw0?ww:tw0;}
		if(t2!='n'&&t3!='n'){r0=run(tw0,false);r=r&&r0;tw0=x<=y?tw0:0;ww=ww>tw0?ww:tw0;}
		check[c2-'a']=false;
	}
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for(int i=1;i<=T;i++)
	{
		depth=0;
		memset(check,0,sizeof(check));
		char tem;
		scanf("%d%c%c%c%c",&l,&tem,&tem,&tem,&tem);
		if (tem=='1'){w=0;scanf("%c\n",&tem);}
		else{scanf("%c%d%c\n",&tem,&w,&tem);}
		int wwx,wwt;
		bool b=run(wwx,true)&&depth==0;
		while(l>0)
		{
			bool boo=run(wwt,true)&&depth==0;
			b=b&&boo;
			wwx=wwx>wwt?wwx:wwt;
		}
		if(b&&wwx==w)printf("Yes\n");
		if(b&&wwx!=w)printf("No\n");
		if(!b)printf("ERR\n");
	}
}

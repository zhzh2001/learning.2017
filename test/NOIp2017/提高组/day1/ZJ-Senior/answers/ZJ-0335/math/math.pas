var
a,b,ans:int64;
begin
assign(input,'math.in');
assign(output,'math.out');
reset(input);
rewrite(output);

readln(a,b);
ans:=(a-1)*(b-1)-1;
write(ans);

close(input);
close(output);
end.
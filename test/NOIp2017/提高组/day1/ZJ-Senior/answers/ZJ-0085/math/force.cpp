#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<cstdlib>
#define ll long long
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (ch!='-'&&!isdigit(ch)) ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
int x[1000005];
int a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("force.out","w",stdout);
	a=rd(),b=rd();
	memset(x,0,sizeof x);
	rep(i,0,b) rep(j,0,a){
		if (i*a+j*b>a*b) break;
		x[i*a+j*b]=1;
	}
	int ans=a*b;
	while (x[ans]) ans--;
	rt(ans);
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<cstdlib>
#define ll long long
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (ch!='-'&&!isdigit(ch)) ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
ll a,b,c,ans;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=rd(),b=rd();
	if (a>b) swap(a,b);
	c=a-b%a;
	ans=(a-1)*b;
	if (ans%a>c) ans-=(ans%a)-c;else ans-=(c+a-ans%a);
	rt(ans),putchar('\n');
}

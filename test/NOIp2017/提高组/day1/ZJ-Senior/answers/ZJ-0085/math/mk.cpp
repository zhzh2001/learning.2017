#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<cstdlib>
#include<ctime>
#include<windows.h>
#define ll long long
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (ch!='-'&&!isdigit(ch)) ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
int gcd(int a,int b){
	return !b?a:gcd(b,a%b);
}
int a,b;
int main(){
	freopen("math.in","w",stdout);
	srand(GetTickCount());
	a=rand()%999+2,b=rand()%999+2;
	int c=gcd(a,b);
	while (c==a||c==b) a=rand()%999+2,b=rand()%999+2,c=gcd(a,b);
	rt(a/c),putchar(' '),rt(b/c),putchar('\n');
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<cstdlib>
#include<map>
#include<string>
#include<queue>
#define ll long long
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define mk make_pair
#define inf 1000000000
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (ch!='-'&&!isdigit(ch)) ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
const int maxn=100005,maxm=200005;
int n,m,K,p;
struct edge{
	int u,v,w;
}e[maxm];
int to[maxm],nx[maxm],val[maxm];
int mds,T,hd[maxn],dist[maxn],cnt,fw[maxn],bw[maxn];
void addedge(int u,int v,int w){
	to[cnt]=v;nx[cnt]=hd[u];val[cnt]=w;hd[u]=cnt++;
}
bool upd(int &a,int b){if (b<a) return a=b,1;else return 0;}
int lst[maxn],lc=0;
int h[maxn],hc,*dst,pos[maxn];
void go(int &x,int y){
	swap(h[x],h[y]),swap(pos[h[x]],pos[h[y]]),x=y;
}
void pushup(int x){
	while (x!=1&&dst[h[x]]<dst[h[x/2]]) go(x,x/2);
}
void pushdown(int x){
	do{
		bool f1=(2*x<=hc&&dst[h[2*x]]<dst[h[x]]),f2=(2*x+1<=hc&&dst[h[2*x+1]]<dst[h[x]]);
		if (f1&&!f2) go(x,x*2);
		else if (!f1&&f2) go(x,x*2+1);
		else if (f1&&f2){
			if (dst[h[2*x]]<dst[h[2*x+1]]) go(x,x*2);else go(x,x*2+1);
		}
		else break;
	}while(1);
}
void del(){
	h[1]=h[hc];hc--;
	pushdown(1);
}
void dijkstra(int s){
	rep(i,1,n) dst[i]=inf;
	memset(pos,0,sizeof pos);
	memset(h,0,sizeof h);
	dst[s]=0;hc=0;
	h[++hc]=s,pos[s]=1;
	while (hc){
		int u=h[1];del();
		cross(i,u){
			int v=to[i];
			if (upd(dst[v],dst[u]+val[i])){
				if (!pos[v]) h[++hc]=v,pos[v]=hc;
				pushup(pos[v]);
			}
		}
	}
}
int dfn[maxn],low[maxn],tm,stk[maxn],top,ins[maxn];
bool judgement;
int idx[maxn],idd;
inline void dfs(int u){
	dfn[u]=low[u]=++tm,stk[++top]=u,ins[u]=1;
	cross(i,u){
		if (val[i]) continue;
		int v=to[i];
		if (!dfn[v]){dfs(v),low[u]=min(low[u],low[v]);}
			else if (ins[v]) low[u]=min(low[u],dfn[v]);
	}
	if (dfn[u]==low[u]){
		int sz=0;
		do{
			int v=stk[top];idx[v]=++idd;
			ins[v]=0,sz++,top--;
		}while (stk[top+1]!=u);
		if (sz>1&&(fw[u]+bw[u]<=mds+K)) judgement=1;
	}
}
void judgezeroscc(){
	memset(dfn,0,sizeof dfn);
	memset(low,0,sizeof low);
	memset(ins,0,sizeof ins);
	tm=0;judgement=0;top=0;idd=0;
	rep(i,1,n) if (!dfn[i]) dfs(i);
}
int cmp(int a,int b){
	return fw[a]==fw[b]?idx[a]>idx[b]:fw[a]<fw[b];
}
inline int min(int a,int b){return a>b?b:a;}
ll f[maxn][52];
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=rd();
	while (T--){
		n=rd(),m=rd(),K=rd(),p=rd();
		memset(hd,-1,sizeof hd);cnt=0;
		rep(i,1,m){
			e[i].u=rd(),e[i].v=rd(),e[i].w=rd();
			addedge(e[i].v,e[i].u,e[i].w);
		}
		dst=bw,dijkstra(n);
		memset(hd,-1,sizeof hd);cnt=0;
		rep(i,1,m) addedge(e[i].u,e[i].v,e[i].w);
		dst=fw,dijkstra(1);
		mds=fw[n];
		judgezeroscc();
		if (judgement){puts("-1");continue;}
		rep(i,1,n) lst[i]=i;
		sort(lst+1,lst+n+1,cmp);
		memset(f,0,sizeof f);
		f[1][0]=1;
		rep(i,0,K) rep(j,1,n){
			int u=lst[j],dt=fw[u]+i;
			ll bs=(f[u][i]%=p);
			if (!bs||dt+bw[u]>mds+K) continue;
			cross(k,u){
				int v=to[k];
				if (dt+val[k]<=min(mds,fw[v])+K) f[v][dt+val[k]-fw[v]]+=bs;
			}
		}
		ll ans=0;
		rep(i,0,K) (ans+=f[n][i])%=p;
		rt(ans),putchar('\n');
	}
}

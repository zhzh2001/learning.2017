#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<cstdlib>
#include<map>
#include<string>
#define ll long long
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (ch!='-'&&!isdigit(ch)) ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
const int maxn=105;
int t,L,cs,cnt,ans,err,top,bzc;
map<string,int>mp;
int bzx[105],scs[105];
string s[105],name;
char c[50],c1[50],c2[50];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=rd();
	while (t--){
		L=rd();getchar(),getchar();
		char ch=getchar();
		if (ch=='1') cs=0,getchar();else cs=rd();
		mp.clear();
		cnt=top=err=bzc=ans=0;
		while (L>0){L--;
			scanf("%s",c);
			if (c[0]=='F'){
				scanf("%s",c1);name=string(c1);
				if (mp[name]==1){
					err=1;gets(c1);
					while (L>0) gets(c1),L--;
				}
				else{
					s[++top]=name,mp[name]=1,bzx[top]=0,scs[top]=0;
					scanf("%s",c1);scanf("%s",c2);
					if (c1[0]=='n'){
						if (c2[0]!='n') bzx[top]++,bzc++;
					}
					else{
						if (c2[0]=='n'){
							cnt++,scs[top]++;
							if (!bzc) ans=max(ans,cnt);
						}
						else{
							int x1,x2;
							sscanf(c1,"%d",&x1),sscanf(c2,"%d",&x2);
							if (x1>x2) bzx[top]++,bzc++;
						}
					}
				}
			}
			else{
				if (!top){
					err=1;
					while (L--) gets(c1);
				}
				else{
					bzc-=bzx[top];mp[s[top]]=0;
					cnt-=scs[top];top--;
				}
			}
		}
		if (top) err=1;
		if (err) puts("ERR");
		else if (ans==cs) puts("Yes");
		else puts("No");
	}
}

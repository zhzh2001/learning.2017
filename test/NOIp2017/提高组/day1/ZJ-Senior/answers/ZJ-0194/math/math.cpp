#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int M=1e8+5;
bool dp[M];
int a,b;
void solve_60(){
	for(int i=1;i<=a*b;i++)dp[i]=0;
	dp[a]=dp[b]=true;
	for(int i=2;i<=a*b;i++){
		if(dp[i]){
			if(i+a<=a*b)dp[i+a]|=dp[i];
			if(i+b<=a*b)dp[i+b]|=dp[i];
		}
	}
	int ans=-1,cnt=0;
	for(int i=1;i<=a*b;i++){
		if(!dp[i])ans=i,cnt=0;
		if(dp[i])cnt++;
		if(dp[i]>=min(a,b)){
			printf("%d\n",ans);
			return;
		}
	}printf("%d\n",ans);
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d %d",&a,&b);
	solve_60();
	return 0;
}

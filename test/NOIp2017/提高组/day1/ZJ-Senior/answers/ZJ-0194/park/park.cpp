#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
#include<iostream>
using namespace std;
const int M=100005,oo=1e9+7;
struct LZ{
	int to,v,nxt;
}edge[M<<1];
struct node{
	int to,v;
	bool operator <(const node &a)const{
		return v>a.v;
	}
};
priority_queue<node>Q;
int head[M],tot,cas;
int n,m,K,P,dis[2][M];//0 是 1指向n   1  是n指向1 
int fa[M];
bool mark[M];
inline void Rd(int&res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=(res*10)+(c&15);
	while(c=getchar(),c>47);
}
int get(int x){
	if(fa[x]!=x)return fa[x]=get(fa[x]);
	return fa[x];
}
int Dijkstra(int flag){
	while(!Q.empty())Q.pop();
	memset(dis,-1,sizeof(dis));
	memset(mark,0,sizeof(mark));
	dis[flag][1]=0;
	Q.push((node){1,0});
	while(!Q.empty()){
		node tmp=Q.top();Q.pop();
		int to=tmp.to;
		if(mark[to])continue;
		if(to==n)return dis[flag][n];
		mark[to]=1;
		for(int i=head[to];~i;i=edge[i].nxt){
			LZ nxt=edge[i];
			int y=nxt.to;
			if(dis[flag][y]==-1||dis[flag][y]>dis[flag][to]+nxt.v){
				dis[flag][y]=dis[flag][to]+nxt.v;
				Q.push((node){y,dis[flag][y]});
			}
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&cas);
	while(cas--&&scanf("%d %d %d %d",&n,&m,&K,&P)){
		memset(head,-1,sizeof(head));
		tot=0;
		for(int i=1;i<=n;i++)fa[i]=i;
		for(int i=1;i<=m;i++){
			int a,b,c;
			Rd(a),Rd(b),Rd(c);
			if(c==0){
				int Fa=get(a),Fb=get(b);
				fa[Fa]=Fb;
			}
			//a->b v=c
			edge[tot]=(LZ){b,c,head[a]};head[a]=tot++;
		}
		int mi_path=Dijkstra(0);
		mi_path=Dijkstra(1);
		if(K==0)puts("1");
	}
	return 0;
}

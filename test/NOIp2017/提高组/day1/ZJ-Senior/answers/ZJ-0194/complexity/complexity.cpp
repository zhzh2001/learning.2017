#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int M=105;
int T;
char A[15],str[105];
char ds[105];
struct LZ{
	char str[105];
	int len;
}stk[105];
int top;
//n ��ֵΪ123456 
char I[105],X[105],Y[105],c[10];
bool check_same(int a,int b){
	if(stk[a].len!=b)return false;
	for(int i=1;i<=b;i++)
		if(stk[a].str[i]!=I[i])return false;
	return true;
}
void Max(int&a,int b){
	if(a<b)a=b;
}
void solve_O1(int L){
	int mx=0,res=0;
	top=0;
	bool xh=0,err=0;
	for(int i=1;i<=L;i++){
		scanf("%s",c+1);
		int li,lx,ly;
		if(c[1]=='E'){
			top--;
			if(top<0){
				err=true;
			}else if(top==0){
				if(xh)res=0;
				Max(mx,res);
				res=0;xh=0;
			}
		}else{
			scanf("%s %s %s",I+1,X+1,Y+1);
			li=strlen(I+1),lx=strlen(X+1),ly=strlen(Y+1);
			for(int j=1;j<=top;j++){
				if(check_same(j,li)){
					err=true;
				}
			}
			top++;
			stk[top].len=li;
			for(int j=1;j<=li;j++)stk[top].str[j]=I[j];
			
			int vx=0,vy=0;
			
			for(int j=1;j<=lx;j++){
				if(X[j]=='n'){vx=123456;break;}
				vx=vx*10+(X[j]&15);
			}
			for(int j=1;j<=ly;j++){
				if(Y[j]=='n'){vy=123456;break;}
				vy=vy*10+(Y[j]&15);
			}
			if(vy>vx&&vy-vx>100)res++;
			else if(vy<vx){
				if(!res)xh=1;
			}
		}
	}
	if(top>0||err)puts("ERR");
	else puts(mx==0?"Yes":"No");
	return;
}
void solve_O(int L,int tar){
	
	int mx=0,res=0;top=0;
	bool err=0;
	for(int i=1;i<=L;i++){
		scanf("%s",c+1);
		int li,lx,ly;bool xh=0;
		if(c[1]=='E'){
			top--;
			if(top<0){
				err=true;
			}else if(top==0){
				if(xh)res=0;
				Max(mx,res);
				res=0;
			}
		}else{
			scanf("%s %s %s",I+1,X+1,Y+1);
			li=strlen(I+1),lx=strlen(X+1),ly=strlen(Y+1);
			for(int j=1;j<=top;j++){
				if(check_same(j,li)){
					err=true;
				}
			}
			top++;stk[top].len=li;
			for(int j=1;j<=li;j++)stk[top].str[j]=I[j];
			int vx=0,vy=0;
			
			for(int j=1;j<=lx;j++){
				if(X[j]=='n'){vx=123456;break;}
				vx=vx*10+(X[j]&15);
			}
			for(int j=1;j<=ly;j++){
				if(Y[j]=='n'){vy=123456;break;}
				vy=vy*10+(Y[j]&15);
			}
			
			if(vy>vx&&vy-vx>100)res++;
			else if(vy<vx){
				if(!res)xh=1;
			}
		}
	}
	if(top>0||err)puts("ERR");
	else puts(mx==tar?"Yes":"No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for(int cas=1;cas<=T;cas++){
		scanf("%s %s",A+1,str+1);
		int len=strlen(A+1);
		int L=0;
		for(int i=1;i<=len;i++){
			L=L*10;
			L+=A[i]&15;
		}
		len=strlen(str+1);
		if(str[3]=='1'){
			solve_O1(L);
		}
		else {
			int res=0;
			for(int i=5;i<len;i++){
				res=res*10;res+=str[i]&15;
			}
			solve_O(L,res);
//			cout<<L<<endl;
		}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#define N 1000
#define M 2000
using namespace std;

inline int read(){
	int a=0;char c=getchar(),f=1;
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}
	return a*f;
}

struct edge{int to,v,nxt;}e[M+5];
int n,m,K,P,cnt,head[N+5],dis[N+5],vis[N+5],num[N+5],f[N+5][N*50+5];
queue<int>Q;

inline void add(int x,int y,int z){
	e[++cnt]=(edge){y,z,head[x]};
	head[x]=cnt;
}

void solve(int s,int t){
	for(int i=1;i<=n;++i) dis[i]=2e8;
	dis[s]=0;Q.push(s);
	while(!Q.empty()){
		int u=Q.front();Q.pop();
		for(int i=head[u];i;i=e[i].nxt){
			int v=e[i].to;
			if(dis[v]>dis[u]+e[i].v){
				dis[v]=dis[u]+e[i].v;
				if(!vis[v]){
					vis[v]=1;
					++num[v];
					if(num[v]>n) {dis[t]=-1;return;}
					Q.push(v);
				}
			}
		}
		vis[u]=0;
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int test=read();
	while(test--){
		n=read(),m=read(),K=read(),P=read();
		cnt=0;
		memset(head,0,sizeof(head));
		for(int i=1;i<=m;++i){
			int x=read(),y=read(),z=read();
			add(x,y,z);
		}
		solve(1,n);
//		if(dis[n]==-1) {puts("-1");continue;}
		int sx=dis[n]+K;
		memset(f,0,sizeof(f));
		f[1][0]=1;
		for(int i=0;i<=sx;++i)
			for(int j=1;j<=n;++j)
				if(f[j][i])
					for(int k=head[j];k;k=e[k].nxt)
						if(e[k].v+i<=sx)
							f[e[k].to][e[k].v+i]=(1ll*f[e[k].to][e[k].v+i]+f[j][i])%P;
		int ans=0;
		for(int i=0;i<=sx;++i)
			ans=(1ll*ans+f[n][i])%P;
		printf("%d\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

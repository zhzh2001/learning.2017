#include<cstdio>
#include<algorithm>
#include<iostream>
#include<set>
#include<cstring>
#include<string>
using namespace std;

struct sta{
	int o,a,b,pd,fz;
	char ne;
	string x,y;
}st[200];
int n,mark,top,flag,pos[30],nw,pd,ans;
char ch[100];

void check(string x,int &a){
	a=0;
	for(int i=0;i<x.size();++i)
		if(x[i]<'0'||x[i]>'9') return;
	for(int i=0;i<x.size();++i)
		a=a*10+x[i]-'0';
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int test;
	scanf("%d",&test);
	while(test--){
		scanf("%d%s",&n,ch+1);
		if(ch[3]=='1') mark=0;
		else{
			mark=0;
			for(int i=5;ch[i]!=')';++i)
				mark=mark*10+ch[i]-'0';
		}
		flag=1;nw=0,pd=1,ans=0,top=0;
		memset(pos,0,sizeof(pos));
		for(int i=1;i<=n;++i){
			scanf("%s",ch+1);
			if(ch[1]=='E'){
				if(top<1) flag=0;
				if(!flag) continue;
				pos[st[top].ne-'a']=0;
				if(st[top].pd) pd=1;
				if(st[top].fz) --nw;
				--top;
			}
			else{
				++top;
				cin>>st[top].ne>>st[top].x>>st[top].y;
				
				if(pos[st[top].ne-'a']) flag=0;
				else pos[st[top].ne-'a']=1;
				
				check(st[top].x,st[top].a),check(st[top].y,st[top].b);
				if(!flag) continue;
//				cout<<st[top].x<<" "<<st[top].y<<endl;
				int num1=st[top].a,num2=st[top].b;
				
				if(num1!=0&&num2==0)
					if(pd) ++nw,st[top].fz=1;
				if(num1==0&&num2!=0)
					if(pd) st[top].pd=1,pd=0;
				if(num1!=0&&num2!=0)
					if(num2<num1)
						if(pd) st[top].pd=1,pd=0;
				ans=max(ans,nw);
			}
		}
		if(top!=0) flag=0;
		if(flag)
			if(mark==ans) puts("Yes");
			else puts("No");
		else puts("ERR");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

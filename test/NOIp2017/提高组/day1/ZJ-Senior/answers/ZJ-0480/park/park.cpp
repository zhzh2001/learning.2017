#include<bits/stdc++.h>
#define fo(i,be,en) for(i=be;i<=en;i++)
using namespace std;
typedef long long LL;
typedef double DB;
const int maxn=1100,maxm=2100;
struct adj_list
{
	LL be[maxn],ne[maxm<<1],v[maxm<<1],w[maxm<<1],edge_cnt;
	void init(){memset(be,0,sizeof(be)); edge_cnt=0;}
	void add(const LL &x,const LL &y,const LL &z)
	{
		v[++edge_cnt]=y;
		w[edge_cnt]=z;
		ne[edge_cnt]=be[x];
		be[x]=edge_cnt;
	}
}v;
LL i,j,k,t,m,n,p,x,y,z,mi_dis,dis[maxn],vis[maxn],kcz,ans;
void spfa()
{
	queue<LL>q;
	int i,now,to;
	dis[1]=0; q.push(1);
	while (!q.empty())
	{
		now=q.front();
		q.pop();
		vis[now]=0;
		for(i=v.be[now];i;i=v.ne[i])
		{
			to=v.v[i];
			if (dis[now]+v.w[i]<dis[to])
			{
				dis[to]=dis[now]+v.w[i];
				if (!vis[to]) q.push(to);
			}
		}
	}
}
void dfs(LL now,LL juli)
{
	if (now==n)
	{
		ans++; return;
	}
	int i;
	for(i=v.be[now];i;i=v.ne[i])
	{
		int to=v.v[i];
		if (juli+v.w[i]<=mi_dis+k) dfs(to,juli+v.w[i]);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%lld",&t);
	while (t--)
	{
		scanf("%lld%lld%lld%lld",&n,&m,&k,&kcz);
		v.init();
		memset(vis,0,sizeof(vis));
		fo(i,1,n) dis[i]=LLONG_MAX>>1;
		fo(i,1,m)
		{
			scanf("%lld%lld%lld",&x,&y,&z);
			v.add(x,y,z);
		}
		spfa();
		mi_dis=dis[n]; ans=0;
		dfs(1,0);
		printf("%lld\n",ans);
	}
}
		

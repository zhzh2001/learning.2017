#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

int readO() {
	char ch = getchar(); int x = 0, op = 0;
	while (ch < '0' || '9' < ch) { if (ch == '^') op = 1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }
	return x * op;
}

char readchar() {
	char ch = getchar();
	while (ch < 'A' || 'Z' < ch) ch = getchar();
	return ch;
}

char readname() {
	char ch = getchar(); 
	while (ch < 'a' || 'z' < ch) ch = getchar();
	return ch;
}

int readtype() {
	char ch = getchar();
	while ((ch < '0' || '9' < ch) && (ch != 'n')) ch = getchar();
	if (ch == 'n') return -1;
	int x = 0;
	while ('0' <= ch && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }
	return x;
}

const int maxn = 1009;
int stk[maxn], stk2[maxn], stk3[maxn], vis[maxn], top;
int now, ans, L, judge, ERR, a, b, flag;
char opt, name, O[maxn];

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T--) {
		memset(vis, 0, sizeof vis);
		now = top = ans = ERR = 0; flag = 1; 
		scanf("%d", &L); judge = readO();
		while (L--) {
			opt = readchar(); 
			if (opt == 'F') {
				name = readname();
				if (vis[name]) ERR = 1;
				vis[name] = 1;
				stk[++top] = name;
				stk2[top] = flag;
				stk3[top] = now;
				
				a = readtype(); b = readtype();
				if (a == -1 && b != -1 || a != -1 && b != -1 && a > b) flag = 0;
				if (a != -1 && b == -1) now++;
				ans = max(ans, now * flag);
			}
			else {
				if (!top) ERR = 1;
				else {
	    		    vis[stk[top]] = 0;
	    		    flag = stk2[top];
	    		    now = stk3[top];
			        top--;
		    	}
			}
		}
		if (ERR || top) puts("ERR");
		else if (ans == judge) puts("Yes");
		else puts("No");
	}
	return 0;
}

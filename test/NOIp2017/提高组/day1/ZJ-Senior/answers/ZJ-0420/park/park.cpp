#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef pair<int,int> P;

inline int read() {
	   char ch = getchar(); int x = 0, op = 1;
	   while (ch < '0' || '9' < ch) { if (ch == '-') op = -1; ch = getchar(); }
	   while ('0' <= ch && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }
	   return op * x;
}

const int maxn = 100009;
const int maxm = 200009;
const int maxK = 55;
struct edge { int v, w, link; } e[maxm];
pair<int,int> Heap[maxm];
int head[maxn], d[maxn], f[maxn][maxK];
bool vis[maxn][maxK], pushed[maxn][maxK], vis2[maxn];
int n, m, K, T, size, MOD;
int a, b, c, u, ty, ty2, past, ans;

void insert(P x) {
	int p = ++size;
	Heap[size] = x; 
	while (p >= 2 && Heap[p] < Heap[p>>1]) {
		swap(Heap[p], Heap[p>>1]); p >>= 1;
	}
}

P top() { return Heap[1]; }
void pop() {
	if (!size) return;
	if (size == 1) { size = 0; return; }
	Heap[1] = Heap[size--];
	int p = 1; 
	while (p*2 <= size && !(Heap[p] < Heap[p*2] && (p*2+1 > size || Heap[p] < Heap[p*2+1]))) 
		if (2*p+1 <= size && Heap[2*p+1] < Heap[2*p]) { swap(Heap[p], Heap[2*p+1]); p = 2*p+1; }
		else { swap(Heap[p], Heap[2*p]); p = p*2; }	 
}

inline void addEdge(int cnt, int a, int b, int c) {
	e[cnt].v = b; e[cnt].w = c; e[cnt].link = head[a]; head[a] = cnt;
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	
	T = read();
	while (T--) {
		n = read(); m = read(); K = read(); MOD = read();
		memset(head, 0, sizeof head);
		memset(d, 0x7f, sizeof d);
		memset(f, 0, sizeof f);
		memset(vis, 0, sizeof vis);
		memset(vis2, 0, sizeof vis2);
		memset(pushed, 0, sizeof pushed);
		for (int i=1; i<=m; i++) {
			a = read(); b = read(); c = read();
			addEdge(i, a, b, c);
		}
		
		Heap[size = 1] = make_pair(0, 1); d[1] = 0; 
		while (size) {
			u = top().second;  
			if (vis2[u]) { pop(); continue; }
			vis2[u] = 1; pop();
			for (int k=head[u]; k; k=e[k].link)
				if (d[u] + e[k].w < d[e[k].v]) {
					d[e[k].v] = d[u] + e[k].w;
					insert(make_pair(d[e[k].v], e[k].v));
				}
		}
		
		Heap[size = 1] = make_pair(d[1], 1); f[1][0] = 1;
		while (size) {
			u = top().second; ty = top().first-d[u];
			if (vis[u][ty]) { pop(); continue; }
			vis[u][ty] = 1; pop();
			
			for (int k=head[u]; k; k=e[k].link){
				ty2 = d[u] + ty + e[k].w - d[e[k].v];
				if (ty2 <= K) {
					f[e[k].v][ty2] = (f[e[k].v][ty2] + f[u][ty]) % MOD;
					if (!pushed[e[k].v][ty2]) {
						insert(make_pair(d[e[k].v] + ty2, e[k].v));
						pushed[e[k].v][ty2] = 1;
					}
				}
			}
		}
		
		ans = 0;
		for (int i=0; i<=K; i++)
			ans = (ans + f[n][i]) % MOD;
		printf("%d\n", ans);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
LL a, b;
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld%lld", &a, &b);
	printf("%lld\n", (a-1)*(b-1)-1);
	return 0;
}

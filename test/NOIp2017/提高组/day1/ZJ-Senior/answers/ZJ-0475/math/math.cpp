#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;

ll a,b,x,y,m1,m2,n1,n2,cs,now1,now2,mn;

inline ll exgcd(ll u,ll v)
{
	if(v)
	{
		ll k=exgcd(v,u%v);
		x-=(u/v)*y;
		swap(x,y);
		return k;
	}
	else
	{
		x=1;
		y=0;
		return u;
	}
}

inline bool pd(ll u)
{
	ll j1,j2;
	j1=x*u;
	j1=(j1%b+b)%b;
	j2=(u-j1*a)/b;
	return j2>=0;
}

void wj()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
}

int main()
{
	wj();
	ll i,j;
	cin>>a>>b;
	if(a>b) swap(a,b);
	exgcd(a,b);
	cs=a-1;
	m1=(x%b+b)%b;
	n1=(1-m1*a)/b;
	n2=(y%a+a)%a;
	m2=(1-n2*b)/a;
	mn=min(-cs*n1*b,-cs*m2*a);
	mn=min(mn,a*b);
	for(i=mn-1;;i--)
	{
		if(!pd(i)) break;
	}
	printf("%lld",i);
}

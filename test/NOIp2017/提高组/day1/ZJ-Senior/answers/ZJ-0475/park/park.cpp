#include<iostream>
#include<cstdio>
#include<queue>
#include<cstring>
#define ll long long
#define N 100100
#define INF 0x3f3f3f3f
#define P pair<ll,ll>
#define mp make_pair
#define fi first
#define se second
using namespace std;

struct Bn
{
	ll to,next;
	ll quan;
} bn[N*2],bn2[N*2];
ll ans,M,dp[N][52],d[N],m,k,n,bb,cc,T,last[N],last2[N];
bool vis[N],have;
priority_queue<P,vector<P>,greater<P> >pq;
P tmp;

inline void wj()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
}

inline void add(ll u,ll v,ll w)
{
	bb++;
	bn[bb].next=last[u];
	bn[bb].quan=w;
	bn[bb].to=v;
	last[u]=bb;
}

inline void add2(ll u,ll v,ll w)
{
	cc++;
	bn2[cc].next=last2[u];
	bn2[cc].quan=w;
	bn2[cc].to=v;
	last2[u]=cc;
}

inline ll dfs(ll now,ll sy)
{
	if(sy<0||have) return 0;
	if(dp[now][sy]!=-1) return dp[now][sy];
	register ll res=0,i,p;
	if(now==n) res++;
	p=last[now];
	for(;p!=-1&&!have;p=bn[p].next)
	{
		res+=dfs(bn[p].to,sy-(d[bn[p].to]+bn[p].quan-d[now]));
		res%=M;
	}
	dp[now][sy]=res;
	return res;
}

inline void read(ll &u)
{
	u=0;
	register char ch=getchar();
	for(;ch<'0';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())
	{
		u=u*10+ch-'0';
	}
}

int main()
{
	wj();
	register ll i,j,p,q,o;
	read(T);
	while(T--)
	{
		have=bb=cc=0;
		memset(dp,-1,sizeof(dp));
		memset(vis,0,sizeof(vis));
		memset(d,INF,sizeof(d));
		memset(last,-1,sizeof(last));
		memset(last2,-1,sizeof(last2));
		for(;!pq.empty();pq.pop());
		read(n),read(m),read(k),read(M);
		for(i=1; i<=m; i++)
		{
			read(p),read(q),read(o),add(p,q,o),add2(q,p,o);
		}
		d[n]=0;
		pq.push(mp(0,n));
		for(;!pq.empty();)
		{
			tmp=pq.top();
			pq.pop();
			if(vis[tmp.se]) continue;
			vis[tmp.se]=1;
			q=tmp.se;
			p=last2[q];
			for(;p!=-1;p=bn2[p].next)
			{
				if(vis[bn2[p].to]) continue;
				if(d[bn2[p].to]>bn2[p].quan+d[q])
				{
					d[bn2[p].to]=bn2[p].quan+d[q];
					pq.push(mp(d[bn2[p].to],bn2[p].to));
				}
			}
		}
		
		ans=dfs(1,k);
		have?printf("-1\n"):printf("%lld\n",ans);
	}
}

#include <iostream>
#include <queue>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define maxn 200010
using namespace std;
int t,g[maxn],n,m,k,p,dis[maxn];
int topt,nt[maxn],st[maxn],to[maxn],w[maxn];
bool f[maxn];
void add(int x,int y,int z)
{
	to[++topt]=y;
	w[topt]=z;
	nt[topt]=st[x];
	st[x]=topt;
}
queue<int>q;
void spfa()
{
	for (int i=1;i<=n;i++) dis[i]=100000000;
	dis[1]=0; f[1]=1; q.push(1); g[1]=1;
	while (!q.empty())
	{
		int kk=q.front(); f[kk]=0; q.pop();
		int p=st[kk];
		while (p)
		{
			if (dis[kk]+w[p]==dis[to[p]]) g[to[p]]=(g[to[p]]+g[kk])%p;
			if (dis[kk]+w[p]<dis[to[p]])
			{
				dis[to[p]]=dis[kk]+w[p];
				g[to[p]]=g[kk];
				if (!f[to[p]]) {f[to[p]]=1; q.push(to[p]);}
			}
			p=nt[p];
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	for (int ii=1;ii<=t;ii++)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p); 
		for (int i=1;i<=m;i++)
		{
			int xx,yy,zz;
			scanf("%d%d%d",&xx,&yy,&zz);
			if (zz!=0) add(xx,yy,zz);
		}
		spfa();
		if (dis[n]==100000000) printf("-1\n");else printf("%d\n",g[n]);
		topt=0;
		memset(dis,0,sizeof dis);
		memset(f,0,sizeof f);
		memset(nt,0,sizeof nt);
		memset(to,0,sizeof to);
		memset(w,0,sizeof w);
		memset(st,0,sizeof st);
	}
return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
char st[20],c1[10],c2[10],c3[10],ch1[10],ch[1000];
int flag[1000],sum[1000];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while (cas--)
	{
		int L,x;
		memset(flag,0,sizeof(flag));
		memset(sum,0,sizeof(sum));
		int flag1=1;
		scanf("%d",&L);
		scanf("%s",st+1);
		if (st[3]=='1') x=0;
		else
		{
			int len=strlen(st+1);
			x=0;
			for (int i=1;i<=len;i++)
				if (st[i]>='0'&&st[i]<='9')
					x=x*10+st[i]-'0';			
		}
		int ans=0,top=0;
		while (L--)
		{
			scanf("%s",ch1);
			if (ch1[0]=='F')
			{
				top++;
				scanf("%s%s%s",c1,c2,c3);
				if (top<=0) continue;
				int s1=0,s2=0;
				for (int i=1;i<top;i++)
					if (ch[i]==c1[0]) 
					{
						flag1=0;
						break;
					}
				flag[top]=flag[top-1];
				sum[top]=sum[top-1];
				ch[top]=c1[0];
				if (flag[top]) continue;
				if (c2[0]=='n'&&c3[0]=='n') continue;
				if (c2[0]=='n')
				{
					flag[top]=1;
					continue;
				}
				if (c3[0]=='n')
				{
					sum[top]=sum[top-1]+1;
					continue;
				}
				int len1=strlen(c2),len2=strlen(c3);
				for (int i=0;i<len1;i++) s1=s1*10+c2[i]-'0';
				for (int i=0;i<len2;i++) s2=s2*10+c3[i]-'0';
				if (s1>s2) flag[top]=1;
			}
			else
			{
				ans=max(ans,sum[top]);
				flag[top]=0;
				sum[top]=0;
				top--;		
				if (top<0) flag1=0;
			}
		}
		if (top) flag1=0;
		if (!flag1) puts("ERR");
		else 
		{
			if (ans==x) puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

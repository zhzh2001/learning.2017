#include<cstdio>
#include<algorithm>
#include<queue>
#include<cstring>
#define mp make_pair
using namespace std;
priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > >q;
const int INF=1000000000;
int x,next[200010],head[100010],vet[200010],vel[200010],next1[200010],head1[100010],vet1[200010],flag[100010],vis[100010],dis[100010];
int mo,num,num1;
int que[5100005],head2[5100005],next2[10200005],vet2[10200005],dp[5100005],in[5100005];
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9')
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int mod(int x)
{
	if (x>=mo) x-=mo;
	return x;
}
void add(int u,int v,int s)
{
	next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
	vel[num]=s;
}
void add1(int u,int v)
{
	next1[++num1]=head1[u];
	head1[u]=num1;
	vet1[num1]=v;
}
void add2(int u,int v)
{
	next2[++num]=head2[u];
	head2[u]=num;
	vet2[num]=v;
}
void bfs(int st)
{
	int he=1,ta=1;
	que[1]=st;
	flag[st]=1;
	while (he<=ta)
	{
		int u=que[he];
		for (int i=head1[u];i;i=next1[i])
		{
			int v=vet1[i];
			if (!flag[v])
			{
				flag[v]=1;
				ta++;
				que[ta]=v;
			}
		}
		he++;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while (cas--)
	{
		int n,m,K;
		num=num1=0;
		scanf("%d%d%d%d",&n,&m,&K,&mo);
		memset(head,0,sizeof(head));
		memset(head1,0,sizeof(head1));
		for (int i=1;i<=m;i++)
		{
			int u=read(),v=read(),s=read();
			add(u,v,s);
			add1(v,u);
		}
		memset(flag,0,sizeof(flag));
		bfs(n);
		for (int i=1;i<=n;i++)
			dis[i]=INF;
		dis[1]=0;
		memset(vis,0,sizeof(vis));
		q.push(mp(0,1));
		while (!q.empty())
		{
			int val=q.top().first,u=q.top().second;
			q.pop();
			if (val!=dis[u]) continue;
			if (vis[u]) continue;
			vis[u]=1;
			for (int i=head[u];i;i=next[i])
			{
				int v=vet[i];
				if (dis[u]+vel[i]<dis[v])
				{
					dis[v]=dis[u]+vel[i];
					q.push(mp(dis[v],v));
				}
			}
		}
		memset(in,0,sizeof(in));
		memset(head2,0,sizeof(head2));
		int maxdian=0,val,dianu,dianv;
		num=0;
		for (int u=1;u<=n;u++)
		{
			if (dis[u]>=INF||!flag[u]) continue;
		//	if (u>1) MaxK=K;
		//	else MaxK=0;
			for (int j=0;j<=K;j++)
				for (int i=head[u];i;i=next[i])
				{
					int v=vet[i];
					if (!flag[v]) continue;
					val=dis[u]+j+vel[i]-dis[v];
					if (val<0||val>K) continue;
					dianu=(u-1)*(K+1)+j,dianv=(v-1)*(K+1)+val;
					add2(dianu,dianv);
					maxdian=max(maxdian,dianu);
					maxdian=max(maxdian,dianv);
					in[dianv]++;
				}
		}
		int he=1,ta=0,u,v,E;
		for (int i=0;i<=maxdian;i++)
			if (in[i]==0&&head2[i]) que[++ta]=i;
		memset(dp,0,sizeof(dp));
		dp[0]=1;
	
		while (he<=ta)
		{
			u=que[he];
			for (E=head2[u];E;E=next2[E])
			{
				v=vet2[E];
				dp[v]+=dp[u];
				if (dp[v]>=mo) dp[v]-=mo;
				//dp[v]=mod(dp[v]+dp[u]);
				in[v]--;
				if (!in[v]&&head2[v]) que[++ta]=v;
			}
			he++;
		}
		int ans=0;
		for (int i=0;i<=maxdian;i++)
		{
			if (in[i]) 
			{
				puts("-1");
				goto fail;
			}
		}
		for (int i=0;i<=K;i++)
			ans=mod(ans+dp[(n-1)*(K+1)+i]);
		printf("%d\n",ans);
		fail:;
	}
	return 0;
}

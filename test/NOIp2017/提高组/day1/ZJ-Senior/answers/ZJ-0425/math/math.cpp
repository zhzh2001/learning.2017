#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
typedef long long LL;
LL A,B;
LL X,Y,X1,Y1;
void ex_gcd(LL a,LL b,LL&x,LL&y){
	if(b==0){
		x=1;y=0;
		return;
	}
	ex_gcd(b,a%b,x,y);
	LL temp=x;
	x=y;
	y=temp-a/b*y;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&A,&B);
	if(A>B)swap(A,B);
	ex_gcd(A,B,X,Y);
	LL i=-X,j=0,k,a,b;
	while(b-a>1){
		j=(i/(-X))*Y;
		k=i%(-X);
		i++;
		b=i*A;
		a=k*A+j*B;
	}
	i--;cout<<i*A-1<<endl;
	fclose(stdin);fclose(stdout);
	return 0;
}

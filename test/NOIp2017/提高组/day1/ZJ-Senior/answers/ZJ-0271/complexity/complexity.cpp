#include <cstdio>
#include <iostream>
#include <cstring>
using namespace std;
struct Fors{
	char chyxy;
	int fromn;
	int ton;
	bool isfromn;
	bool iston;
};
int wxyisuseless,useless1;
int line1,i,j,depth,lev1,maxdep,maxusedep;
Fors qsd[101];
bool outuseless;
int outuselessdep;
bool iswrong;
int wxy;
char ch1,ch2;
bool issimilar(){
	int i;
	for (i=1;i<depth;i++){
		if (qsd[i].chyxy==qsd[depth].chyxy) return true;
	}
	return false;
}
void initdata(){
	qsd[depth].fromn=0;
	qsd[depth].ton=0;
	qsd[depth].isfromn=false;
	qsd[depth].iston=false;
	return;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&useless1);
	for ( wxyisuseless = 0 ; wxyisuseless < useless1 ; wxyisuseless++ ){
		wxy=0;
		depth=0;
		outuseless=false;outuselessdep=0;
		lev1=0;maxusedep=0;
		iswrong=false;
 		scanf("%d%c%c",&line1,&ch1,&ch2);
		scanf("%c",&ch2);
 		scanf("%c%c",&ch1,&ch2);
		if (ch1=='n'){
			scanf("%c",&ch1);
			while ((ch1<='9')&&(ch1>='0')){
				wxy=wxy*10+ch1-'0';
				scanf("%c",&ch1);
			}
			getchar();
		}else{
			wxy=0;scanf("\n");
		}
		for (i=0;i<line1;i++){
			if (iswrong) {
				ch1=getchar();
				while (ch1!='\n') ch1=getchar();
				continue;
			}
			scanf("%c",&ch1);
			if (ch1=='E'){
				scanf("\n");
				if (!((outuseless)||((qsd[depth].isfromn)&&(qsd[depth].iston))||(!qsd[depth].iston))) lev1--;
				if (depth==0) {
					iswrong=true;
				}else depth--;
				continue;
			}
			scanf("%c%c",&ch2,&ch1);
			depth++;
			initdata();
			qsd[depth].chyxy=ch1;
			scanf("%c%c",&ch2,&ch1);
			if (!iswrong) iswrong=issimilar();
			if (ch1=='n') {
				scanf("%c",&ch2);
				qsd[depth].isfromn=true;
			}else {
				while ((ch1<='9')&&(ch1>='0')){
					qsd[depth].fromn=qsd[depth].fromn*10+ch1-'0';
					scanf("%c",&ch1);
				}
			}
			scanf("%c",&ch1);
			if (ch1=='n'){
				qsd[depth].iston=true;
				scanf("\n");
			}else{
				while ((ch1<='9')&&(ch1>='0')){
					qsd[depth].ton=qsd[depth].ton*10+ch1-'0';
					scanf("%c",&ch1);
				}
			}
			if (((qsd[depth].isfromn)&&(!qsd[depth].iston))||((qsd[depth].fromn>qsd[depth].ton)&&(!qsd[depth].iston))||((outuseless)&&(outuselessdep<depth))) {
				if (!outuseless) outuselessdep=depth;
				outuseless=true;
				continue;
			}else {
				outuseless=false;
			}
			if (qsd[depth].isfromn) continue;
			if (qsd[depth].iston) {
				lev1++;
				if (lev1>maxusedep) maxusedep=lev1;
			}
		}
		if ((depth!=0)||(iswrong)){
			printf("ERR\n");
			continue;
		}
		if (maxusedep==wxy) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

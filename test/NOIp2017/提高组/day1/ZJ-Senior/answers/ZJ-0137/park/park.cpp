#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;

int n,m,k,p=0,po,ans=0;
int Gu[100000008],sum[1008],to[1008][1008],len[1008][1008];

void Find(int k,int length)
{
	if (k==n) Gu[++p]=length;
	for (int i=1;i<=sum[k];i++)
	{
		Find(to[k][i],length+len[k][i]);
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&po);
		for (int i=1;i<=n;i++)
		sum[i]=0;
		for (int i=1;i<=m;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			to[x][++sum[x]]=y;
			len[x][sum[x]]=z;
		}
		Find(1,0);
		int minn=999999999;
		//for (int i=1;i<=p;i++)
		//printf("%d\n",Gu[i]);
		for (int i=1;i<=p;i++)
		minn=min(minn,Gu[i]);
		//cout<<minn;
		for (int i=1;i<=p;i++)
		if (Gu[i]<=minn+k) 
		{
			ans++;
			if (ans==po) ans=0;
		}
		printf("%d",ans);
	}
	
	fclose(stdout);
	fclose(stdin);
	return 0;
}

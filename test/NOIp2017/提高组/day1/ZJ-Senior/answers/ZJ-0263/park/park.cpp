#include<set>
#include<queue>
#include<string>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream> 
#include<algorithm>
using namespace std;
#define M 400010
#define inf 1000000000
#define rep(i,x,y) for(int i=(x);i<=(y);i++)
struct Edge{
	int u,v,w,Next;
} G[M]; int head[M],tot;
inline void add(int u,int v,int w){
	G[++tot]=(Edge){u,v,w,head[u]};
	head[u]=tot;
}
int n,m,k,P;
struct Node{
	int d,u;
	inline bool operator < (const Node& rhs) const {
		return d>rhs.d;
	} 
};
priority_queue<Node> Q;
int d[M],done[M];
int du[M/4+5];
int f[M/4+5];
inline void dij(){
	memset(done,0,sizeof(d));
	rep(i,1,n)d[i]=inf;
	d[1]=0;Q.push((Node){0,1});
	while(!Q.empty()){
		Node x=Q.top();Q.pop();
		if(done[x.u])continue;
		done[x.u]=1;
		for(int i=head[x.u];i!=-1;i=G[i].Next){
			if(d[G[i].v]>d[x.u]+G[i].w){
				d[G[i].v]=d[x.u]+G[i].w;
				Q.push((Node){d[G[i].v],G[i].v});
			}
		}
	}
}queue<Node> q; 
inline void topo(){
	while(!q.empty())q.pop();
	q.push((Node){0,1});
	f[1]=1;
	while(!q.empty()){
		Node x=q.front();q.pop();
		for(int i=head[x.u];i!=-1;i=G[i].Next)if(d[x.u]+G[i].w==d[G[i].v]){
		//	if(0<=wt&&wt<=k){
				(f[G[i].v]+=f[x.u])%=P;
				du[G[i].v]--;
				if(!du[G[i].v]){
					q.push((Node){0,G[i].v});
				}
		//	}
		}
	}int res=0;
	rep(i,0,k)(res+=f[n])%=P;
	printf("%d\n",res);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&P);
		tot=0;memset(head,-1,sizeof(head));
		rep(i,1,m){
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}dij();//printf("%d\n",d[n]);
		if(d[n]==inf){
			puts("0");continue;
		}
		memset(du,0,sizeof(du));
		memset(f,0,sizeof(f));
		rep(i,1,n){
			for(int e=head[i];e!=-1;e=G[e].Next){
				du[G[e].v]++;
			}
		}
		topo();
	} 
	return 0;
}

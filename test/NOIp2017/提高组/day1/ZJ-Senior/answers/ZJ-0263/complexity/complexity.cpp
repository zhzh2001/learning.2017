#include<set>
#include<string>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream> 
#include<algorithm>
using namespace std;
#define inf 1001
#define rep(i,x,y) for(int i=(x);i<=(y);i++)
char str[1000];
struct Node{
	int op;
	string Name;
	int x,y;
} que[1010];
set<string> S;
int St[1010];
int To[1010],TO[1010];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		int len;scanf("%d",&len);
		int now=0,Tim;scanf("%s",str);
		int Er=0;
		if(str[2]=='1')Tim=0;
		else {
			Tim=0;
			for(int o=4;str[o]!=')';o++)Tim=Tim*10+str[o]-'0';
		}memset(To,0,sizeof(To));
		memset(TO,0,sizeof(TO));//1 
		rep(i,1,len){
			scanf("%s",str);
			if(str[0]=='F'){
				que[i].op=1;
				cin>>que[i].Name;
				scanf("%s",str);
				if(str[0]=='n')que[i].x=inf;
				else{
					que[i].x=0;
					for(int j=0;j<strlen(str);j++)que[i].x=que[i].x*10+str[j]-'0';	
				}
				scanf("%s",str);
				if(str[0]=='n')que[i].y=inf;
				else{
					que[i].y=0;
					for(int j=0;j<strlen(str);j++)que[i].y=que[i].y*10+str[j]-'0';
				}
				St[++now]=i;
			}else{
				if(now>=0){que[i].op=2;To[i]=St[now];TO[St[now]]=i;}
				now--;if(now<0)Er=1;
			}
		}if(now!=0)Er=1;
		if(Er){
			puts("ERR");continue;
		}S.clear();
		rep(i,1,len){
			if(que[i].op==1){
				if(S.find(que[i].Name)!=S.end()){
					Er=1;break;
				}S.insert(que[i].Name);
			}else {
				S.erase(que[To[i]].Name);
			}
		}if(Er){
			puts("ERR");continue;
		}int dep=0,mx=0;
		rep(i,1,len){
			if(que[i].op==1){
				if(que[i].x>que[i].y){
					i=TO[i];continue;
				}if(que[i].x!=inf&&que[i].y==inf){
					dep++;
				}mx=max(mx,dep);
			}else{
				int J=To[i];
				if(que[J].x!=inf&&que[J].y==inf){
					dep--;
				}
			}
			//printf("dep %d\n",dep);
		}if(mx==Tim){
			puts("Yes");
		}else puts("No");
	}
	return 0;
}
/*
2 O(n^1)
F x 1 n
E
*/

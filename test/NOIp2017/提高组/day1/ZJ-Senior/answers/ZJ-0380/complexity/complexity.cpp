#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define pol(i, b, e) for(int i=b; i>=e; --i)
#define test(x) std :: cout << #x << " = " << x << std::endl;
#define file(x) freopen(x".in", "r", stdin); freopen(x".out", "w", stdout);
#define mset(x) memset(x, 0, sizeof x);
int n, tns, num, l, x1, x2, nnowans, pds, deep, cnts1, cnts2;
char c;
char stack2[3000];
bool isERR, nowans, b1, b2, isn;
bool used[30];
bool stack1[1000];

int getint() {
	int x=0, k=1;
	char c=getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') k=-k;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x*k;
}

inline void init() {
	mset(used); mset(stack1); mset(stack2);
	isn = false;
	isERR = false;
	used['n'-'a'] = true;
	num = 0; nnowans = 0;
	l=getint();
	do c=getchar(); while(c!='^' && !(c>='0' && c<='9'));
	if(c=='^') {
		tns=getint();
		isn = true;
	} else {
		tns = 0;
		for(; c>='0' && c<='9'; c=getchar()) tns=tns*10+c-'0';
	}
	pds = 0;
	deep = 0;
	cnts1 = 0;
	cnts2 = 0;
}

inline void initiii() {
	b1 = b2 = false;
	x1 = x2 = 0;
}

inline void panduan() {
	nowans = false;
	if((!b1 && !b2 && x1>x2) || (b1 && !b2)) { //not get in
		nowans = false;
	} else if((b1&&b2) || (!b1&&!b1&&x1<=x2)) { //o1
		nowans = true; 
		if(!nnowans) ++deep;
	} else { //on
		nowans = true;
		if(!nnowans) {
			++num; ++deep; stack1[++cnts1] = true;
		}
	}
}

int main() {
	file("complexity");
	for(int Ti=getint(); Ti; Ti--) {
		init();
		lop(i, 1, l) {
			initiii();
			do c=getchar(); while(c!='F' && c!='E');
			 if(c=='F') {
				do c=getchar(); while(!(c>='a' && c<='z'));
				if(!isERR && used[c-'a']) {
					deep = 0;
					isERR = true;
					printf("ERR\n");
				}
				used[c-'a'] = true; stack2[++cnts2] = c;
				
				for(c=getchar(); c!='n' && (c<'0' || c>'9'); c=getchar());
				if(c=='n') b1 = true;
				else for(; c>='0' && c<='9'; c=getchar()) x1=x1*10+c-'0';
				
				for(c=getchar(); c!='n' && (c<'0' || c>'9'); c=getchar());
				if(c=='n') b2 = true;
				else for(; c>='0' && c<='9'; c=getchar()) x2=x2*10+c-'0';
				if(isERR) continue;
				panduan();
				if(nnowans) {
					++nnowans;
					continue;
				}
				if(!nowans) { //not get in
					nnowans = 1;
				} else {
					if(num > pds) pds = num;
				}
			} else if(c=='E') {
				if(isERR) continue;
				if(nnowans) {
					used[stack2[cnts2]-'a'] = false;
					--cnts2;
					--nnowans;
					continue;
				}
				if(!isERR && deep<=0) {
					deep = 0;
					isERR = true;
					printf("ERR\n");
				}
				if(isERR) continue;
				--deep;
				if(stack1[cnts1--]) --num;
				used[stack2[cnts2]-'a'] = false;
				--cnts2;
			} else {
				deep = 0;
					isERR = true;
					printf("ERR\n");
			}
		}
		if(!isERR && deep) {
			isERR = true;
			printf("ERR\n");
		} 
		if(!isERR) {
			if((!isn && !pds) || (isn && pds==tns))printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}


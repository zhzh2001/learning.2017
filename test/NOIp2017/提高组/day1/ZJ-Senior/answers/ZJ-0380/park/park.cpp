#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define pol(i, b, e) for(int i=b; i>=e; --i)
#define test(x) std :: cout << #x << " = " << x << std::endl;
#define file(x) freopen(x".in", "r", stdin); freopen(x".out", "w", stdout);
#define mset(x) memset(x, 0, sizeof(x));
const int N = 100000 + 5;
const int M = 200000 + 5;
struct Edge {
	int u, v, w, n;
} e[M];
int cnte, n, m, k, p, hn;
int head[N], du[N], rank[N], dis[N], q[N], ans[N];

struct Node {
	int u, d;
	bool operator < (const Node &rhs) const {
		return d < rhs.d;
	}
	bool operator > (const Node &rhs) const {
		return d > rhs.d;
	}
} h[N];
int getint() {
	int x=0, k=1;
	char c=getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') k=-k;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x*k;
}

inline void add(int u, int v, int w) {
	++cnte;
	e[cnte].u = u;
	e[cnte].v = v;
	e[cnte].w = w;
	e[cnte].n = head[u];
	head[u] = cnte;
}

void init() {
	mset(head); mset(rank); hn = 0; mset(ans); mset(du);
	cnte = 0;
	n = getint(); m = getint(); k = getint(); p = getint();
	int u, v, w;
	lop(i, 1, m) {
		u=getint(); v=getint(); w=getint();
		++du[v];
		add(u, v, w);
	}
}

//heap
void swap(int x, int y) {
	rank[h[x].u] = y; rank[h[y].u] = x;
	Node t=h[x];h[x]=h[y];h[y]=t;
}
void up(int x) {
	for(int i=x>>1; i; x=i, i>>=1) {
		if(h[i]>h[x]) swap(i, x);
		else return;
	}
}
void down(int x) {
	for(int i=x<<1; i<=hn; x=i, i<<=1) {
		i+=i<hn && h[i+1]<h[i];
		if(h[i]<h[x]) swap(i, x);
		else return;
	}
}
inline void pop() {
	swap(1, hn--);
	down(1);
}
//--heap

void dijkstra() {
	memset(dis, 0x3f, sizeof dis);
	dis[1] = 0; hn = n;
	lop(i, 1, n) {
		h[i].u = i; h[i].d = dis[i];
		rank[i] = i;
	}
	Node tnode; int u;
	while(hn) {
		tnode = h[1];pop();
		u = tnode.u;
		for(int i=head[u]; i; i=e[i].n) {
			int v=e[i].v;
			if(dis[v] > dis[u] + e[i].w) {
				dis[v] = dis[u] + e[i].w;
				h[rank[v]].d = dis[v];
				up(rank[v]);
			}
		}
	}
}

void work() {
	q[1] = 1; ans[1] = 1;
	int l = 1, r = 1; int u;
	while(l <= r) {
		u = q[l];
		for(int i=head[u]; i; i=e[i].n) {
			int v=e[i].v;
			if(dis[v] == dis[u] + e[i].w) {
				ans[v] += ans[u];
				ans[v] %= p;
			}
			--du[v];
			if(du[v] == 0) q[++r] = v;
		}
		++l;
	}
}

int main() {
	file("park");
	for(int Ti=getint(); Ti; Ti--) {
		init();
		if(k) {
			printf("-1");continue;
		}
		dijkstra();
		work();
		printf("%d\n", ans[n]);
	}
}


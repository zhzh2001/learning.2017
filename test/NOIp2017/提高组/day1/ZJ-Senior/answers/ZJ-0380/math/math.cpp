#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include <algorithm>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define pol(i, b, e) for(int i=b; i>=e; --i)
#define test(x) std :: cout << #x << " = " << x << std::endl;
#define file(x) freopen(x".in", "r", stdin); freopen(x".out", "w", stdout);
#define LL long long
LL n, m;
LL getint() {
	LL x=0, k=1;
	char c=getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') k=-k;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x*k;
}

int main() {
	file("math");
	n=getint();m=getint();
	printf("%lld\n", m*n-n-m);
	return 0;
}


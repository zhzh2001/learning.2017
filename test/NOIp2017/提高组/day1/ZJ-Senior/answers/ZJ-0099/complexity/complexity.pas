var
  t,i,j,n,cs,ans,head,t1,t2,now,thead,len:longint;
  b:array['A'..'z']of boolean;
  z:array[0..1001]of char;
  v:array[0..1001]of longint;
  ch:char;
  bo:boolean;
  s:string;
begin
  assign(input,'complexity.in');
  reset(input);
  assign(output,'complexity.out');
  rewrite(output);
  read(t);
  readln;
  while t>0 do
    begin
      dec(t);
      head:=0;
      n:=0;
      read(ch);
      while ch<>' ' do
      begin
        n:=n*10+ord(ch)-ord('0');
        read(ch);
      end;
      fillchar(b,sizeof(b),false);
      for i:=1 to 3 do read(ch);
       if ch='1' then
        begin
          cs:=0;
          readln;
        end
      else begin
             read(ch);
             read(ch);
             cs:=0;
             while ch<>')' do
             begin
               cs:=cs*10+ord(ch)-ord('0');
               read(ch);
             end;
             readln;
           end;
      bo:=false;
      ans:=0;
      now:=0;
      i:=1;
      while i<=n do
        begin
          read(ch);
          if ch='F'then
            begin
              read(ch);
              read(ch);
              if b[ch] then
                begin
                  writeln('ERR');
                  bo:=true;
                  for j:=i to n do readln;
                  break;
                end
              else begin
                     b[ch]:=true;
                     inc(head);
                     z[head]:=ch;
                     v[head]:=0;
                     read(ch);
                     read(ch);
                     t1:=0;
                     t2:=0;
                     if ch='n' then t2:=-1
                     else
                       while ch<>' ' do
                         begin
                           t1:=t1*10+ord(ch)-ord('0');
                           read(ch);
                         end;
                     if t2=0 then
                     begin
                       read(ch);
                       if ch='n' then
                       begin
                         now:=now+1;
                         v[head]:=1;
                       end
                       else begin
                              t2:=ord(ch)-ord('0');
                              read(s);
                              len:=length(s);
                              for j:=1 to len do
                                t2:=t2*10+ord(s[j])-ord('0');
                              if (t1>t2) then
                                begin
                                  thead:=1;
                                  while (thead>0)and(i<n) do
                                    begin
                                      inc(i);
                                      readln;
                                      read(ch);
                                      if ch='F' then
                                      begin
                                        inc(thead);
                                        read(ch);
                                        read(ch);
                                        if b[ch] then
                                          begin
                                            writeln('ERR');
                                            bo:=true;
                                            for j:=i to n  do readln;
                                            break;
                                          end;
                                         b[ch]:=true;
                                         z[head+thead]:=ch;
                                      end
                                      else begin
                                             if  thead>1 then
                                             b[z[head+thead]]:=false;
                                             dec(thead);
                                           end;
                                    end;
                                  if bo then break;
                                  b[z[head]]:=false;
                                  dec(head);
                                  if thead>0 then
                                    begin
                                      bo:=true;
                                      for j:=i to n do readln;
                                      break;
                                    end;
                                end;
                            end;
                     end
                     else begin
                            read(ch);
                            read(ch);
                            if ch<>'n' then
                              begin
                                 thead:=1;
                                  while (thead>0)and(i<n) do
                                    begin
                                      inc(i);
                                      readln;
                                      read(ch);
                                      if ch='F' then
                                      begin
                                        inc(thead);
                                        read(ch);
                                        read(ch);
                                        if b[ch] then
                                          begin
                                            writeln('ERR');
                                            bo:=true;
                                            for j:=i to n do readln;
                                            break;
                                          end;
                                         b[ch]:=true;
                                         z[head+thead]:=ch;
                                      end
                                      else begin
                                            if  thead>1 then
                                             b[z[head+thead]]:=false;
                                             dec(thead);
                                           end;
                                    end;
                                  if bo then break;
                                  b[z[head]]:=false;
                                  dec(head);
                                  if thead>0 then
                                    begin
                                      writeln('ERR');
                                      bo:=true;
                                      for j:=i to n do readln;
                                      break;
                                    end;
                              end;
                          end;
                   end;
              end
          else begin
                 if head=0 then
                   begin
                      writeln('ERR');
                      bo:=true;
                      for j:=i to n do readln;
                      break;
                   end;
                 if now>ans then ans:=now;

                  now:=now-v[head];
                 b[z[head]]:=false;
                 dec(head);
               end;
          if bo then break;
          readln;
          inc(i);
        end;
      if bo then continue;
      if head>0 then writeln('ERR')
      else if cs=ans then writeln('Yes')
      else writeln('No');
    end;
  close(input);
  close(output);
end.

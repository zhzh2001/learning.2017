#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

const int N = 200;
int l, w;
int qf[N], qi[N], qx[N], qy[N];
void input(){
	static char str[N];
	scanf("%d %s", &l, str);
	int len = strlen(str);
	
	ast(str[0] == 'O');
	ast(str[1] == '(');
	ast(str[len-1] == ')');
	
	if(str[2] == '1') w = 0;
	else{
		ast(str[2] == 'n');
		ast(str[3] == '^');
		w = 0;
		//[2, len-1)
		rep(i, 4, len-1) w = (w*10) + (str[i]^48);
	}
	
	rep(i, 0, l){
		scanf("%s", str);
		if(str[0] == 'F'){
			qf[i] = 0;
			
			scanf("%s", str);
			qi[i] = str[0]; //ASCII
			
			scanf("%s", str);
			if(str[0] == 'n') qx[i] = -1;
			else{
				if(strlen(str)==1) qx[i] = str[0]^48;
				else qx[i] = (str[0]^48)*10 + (str[1]^48);
			}
			scanf("%s", str);
			if(str[0] == 'n') qy[i] = -1;
			else{
				if(strlen(str)==1) qy[i] = str[0]^48;
				else qy[i] = (str[0]^48)*10 + (str[1]^48);
			}
		}else if(str[0] == 'E') qf[i] = 1;
		else ast(false);
	}
}

typedef pair<int, int> pii;
#define x first
#define y second
namespace Program{

int rpos[N];
bool err;
void workCE(){
	static pii stk[N];
	int top = 0;
	rep(i, 0, l){
		if(qf[i] == 0){ //F
			rep(j, 1, top+1) if(stk[j].y == qi[i]){ err = true; return;}
			stk[++top] = pii(i, qi[i]);
		}else if(qf[i] == 1){ //E
			if(top) rpos[stk[top].x] = i, --top;
			else{ err = true; return;}
		}else ast(false);
	}
	if(top) err = true;
}
int work(int L, int R){//[L, R]
	ast(qf[L] == 0 && qf[R] == 1);

	int res = 0;
	for(int i = L+1; i < R;){
		res = max(res, work(i, rpos[i]));
		i = rpos[i]+1;
	}
	
	int x = qx[L], y = qy[L];
	if(x==-1&&y==-1) res = res; //O(1)
	else if(x == -1) res = 0;
	else if(y == -1) res++;
	else if(x > y) res = 0;
	else if(x <= y) res = res;
	else ast(false);
	
	return res;
}
void Main(){
	err = false;
	workCE();
	if(err){ puts("ERR"); return;}
	
	int ans = 0;
	for(int i = 0; i < l;){
		ans = max(ans, work(i, rpos[i]));
		i = rpos[i]+1;
	}
	if(ans == w) puts("Yes");
	else puts("No");
}

} //namespace Program
/*
Yes No ERR
*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	
	int cas; scanf("%d", &cas);
	while(cas--){
		input();
		Program::Main();
	}
	
	return 0;
}

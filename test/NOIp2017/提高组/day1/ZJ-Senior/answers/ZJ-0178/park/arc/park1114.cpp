#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

typedef long long ll;
const int inf = (int)1e9;
typedef vector<int> vi;
typedef pair<int, int> pii;
#define pb(x) push_back(x)
#define sz(x) (int)(x).size()
#define x first
#define y second

const int N = (int)1e5;
const int M = (int)2e5;
const int W = 50;
const int V = 1000;
int n, m, w, mod; bool zero;
inline void addmod(int &x, int y){
	if((x+=y) >= mod) x -= mod;
}
int a[M+5], b[M+5], c[M+5];
void input(){
	scanf("%d %d %d %d", &n, &m, &w, &mod);
	zero = false;
	rep(i, 0, m){
		scanf("%d %d %d", &a[i], &b[i], &c[i]);
		if(c[i] == 0) zero = true;
		ast(1 <= a[i] && a[i] <= n);
		ast(1 <= b[i] && b[i] <= n);
		ast(0 <= c[i] && c[i] <= V);
		
		--a[i]; --b[i];
	}
}

namespace P10{

int mp[10][10];
int ans, md;
void dfs(int u, int d){
	if(u == n-1){
		if(d == md) ++ans;
		return;
	}
	if(d+mp[u][n-1] > md) return;
	
	rep(i, 0, m) if(a[i] == u){
		dfs(b[i], d+c[i]);
	}
}
void Main(){
	ast(n <= 5); ast(m <= 10); ast(w == 0);
	rep(i, 0, n) rep(j, 0, n){
		if(i == j) mp[i][j] = 0;
		else mp[i][j] = inf;
	}
	rep(i, 0, m) mp[a[i]][b[i]] = c[i];
	rep(k, 0, n) rep(i, 0, n) rep(j, 0, n)
		if(mp[i][j] > mp[i][k]+mp[k][j]) mp[i][j] = mp[i][k]+mp[k][j];
		
	md = mp[0][n-1];
	
	ans = 0;
	dfs(0, 0);
	printf("%d\n", ans%mod);
}

} //namesapce P10

namespace Program{

int md;
struct Graph{
	struct Edge{
		int to, val, nxt;
		Edge(){}
		Edge(int _to, int _val, int _nxt)
			: to(_to), val(_val), nxt(_nxt){}
	}edge[M+5];
	int etot, head[N+5];
	void init(){
		etot = 0;
		rep(i, 0, n) head[i] = -1;
	}
	inline void addEdge(int u, int v, int val){
		edge[etot] = Edge(v, val, head[u]); head[u] = etot++;
	}
	int dis[N+5];
	bool mark[N+5];
	struct node{
		int to, val;
		node(){}
		node(int _to, int _val)
			: to(_to), val(_val){}
		bool operator <(const node &tmp)const{
			return val > tmp.val;
		}
	};
	priority_queue<node> que;
	void Dijkstra(int s){
		rep(i, 0, n){
			dis[i] = inf;
			mark[i] = false;
		}
		dis[s] = 0;
		while(!que.empty()) que.pop();
		que.push(node(s, 0));
		while(!que.empty()){
			node now = que.top(); que.pop();
			int u = now.to;
			if(mark[u]) continue;
			mark[u] = true;
			for(int i = head[u]; ~i; i = edge[i].nxt){
				int v = edge[i].to;
				if(dis[v] > dis[u]+edge[i].val){
					dis[v] = dis[u]+edge[i].val;
					que.push(node(v, dis[v]));
				}
			}
		}
	}
}g, r;
void build(){
	g.init(); r.init();
	rep(i, 0, m){
		g.addEdge(a[i], b[i], c[i]);
		r.addEdge(b[i], a[i], c[i]);
	}
	g.Dijkstra(0);
	r.Dijkstra(n-1);
	md = g.dis[n-1];
}
pii a[N+5];
int dp[N+5];
void work(int L, int R){ //[L, R]
//	dig("work[%d, %d]\n", L, R);
	static int arr[N+5]; int tot = 0;
	rep(i, L, R+1) arr[tot++] = a[i].y;
	
	static int d[N+5];
	static int mark[N+5], index = 0;
	++index;
	rep(i, 0, tot){
		int u = arr[i];
		d[u] = 0; mark[u] = index;
	}
	rep(i, 0, tot){
		int u = arr[i];
		for(int j = g.head[u]; ~j; j = g.edge[j].nxt){
			int v = g.edge[j].to;
			if(mark[v] == index) ++d[v];
		}
	}
	static int que[N+5];
	int qh = 0, qt = 0;
	rep(i, 0, tot){
		int u = arr[i];
		if(d[u] == 0) que[qt++] = u;
	}
	while(qh < qt){
		int u = que[qh++];
//		dig("u %d\n", u);
		for(int j = g.head[u]; ~j; j = g.edge[j].nxt){
			int v = g.edge[j].to;
			if(mark[v] == index){
				--d[v];
				if(d[v] == 0) que[qt++] = v;
			}
			if(g.dis[u]+g.edge[j].val+r.dis[v] == md) addmod(dp[v], dp[u]);
		}
	}
	//mod  &  -1
}
void solve(){
	rep(i, 0, n) a[i] = pii(g.dis[i], i);
	sort(a, a+n);
	
	rep(i, 0, n) dp[i] = 0; dp[0] = 1;
	
	for(int i = 0; i < n;){
		int lst = i;
		while(lst+1<n && a[lst+1].x==a[i].x) ++lst;
		work(i, lst);
		i = lst+1;
	}
//	rep(i, 0, n) dig("dp[%d] %d\n", i, dp[i]);
	printf("%d\n", dp[n-1]);
	//mod  &  -1
}
void Main(){	
	build();
	solve();
	//mod  &  -1
}

}// namespace Program
/*

对P取模
无穷合法 输出−1
*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	
	int cas; scanf("%d", &cas);
	while(cas--){
		input();
		Program::Main();
		
//		if(n <= 5 && m <= 10 && w == 0 && zero == false) P10::Main();
//		else Program::Main();
	}
	return 0;
}

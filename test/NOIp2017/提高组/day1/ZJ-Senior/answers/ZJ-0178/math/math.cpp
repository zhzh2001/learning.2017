#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
#include <bitset>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

int a, b;

namespace P30{

const int X = 10000;
bool mark[X];
void Main(){
	memset(mark, false, sizeof mark);
	rep(i, 0, b){
		for(int x = i*a; x < X; x += b){
			mark[x] = true;
		}
	}
	for(int i = a*b-1; i >= 0; --i){
		if(!mark[i]){
			printf("%d\n", i);
			return;
		}
	}
}

} //namespace P30
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	
	scanf("%d %d", &a, &b);
	if(a <= 50 && b <= 50) P30::Main();
	else{
		long long ans = a*(long long)b - a - b;
		cout << ans << endl;
	}
	return 0;
}

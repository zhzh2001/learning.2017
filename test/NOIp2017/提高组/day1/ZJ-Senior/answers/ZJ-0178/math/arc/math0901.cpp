#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

int a, b;

namespace P30{

void Main(){
	static bool mark[1000005];
	memset(mark, false, sizeof mark);
	rep(i, 0, 2600) rep(j, 0, 2600){
		int x = i*a+j*b;
		mark[x] = true;
	}
	for(int i = 10000; i >= 0; --i){
		if(!mark[i]){
			printf("%d\n", i);
			return;
		}
	}
}

} //namespace P30
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	
	scanf("%d %d", &a, &b);
	P30::Main();
	
	return 0;
}

var
  ii,i,p,j,k,l,m,t,max,ll:longint;
  fl,ab,ch:boolean;
  s:string;
  f:array[0..27]of boolean;

procedure dfs(s:string;n:longint);
var i,j,k,u,v,p,q,nn:longint; ss:string;
begin
  if not fl then while true do
    begin
    inc(ll);
    if ll>l then begin fl:=false; exit; end; readln;
    fl:=false;
    end;
  nn:=n;
  ch:=ab;
  q:=1;v:=0;
  if s[length(s)]='n' then v:=-1
  else
  for i:=length(s) downto 4 do
  begin
    if s[i]=' ' then break;
    val(s[i],p);
    v:=v+p*q;
    q:=q*10;
  end;
  u:=0;
  if s[5]='n' then u:=-1
  else
  for i:=5 to length(s) do
  begin
    if s[i]=' ' then break;
    val(s[i],p);
    u:=u*10+p;
  end;
  if f[ord(s[3])-ord('a')] then
  begin
    f[ord(s[3])-ord('a')]:=false;
    if n<>-1 then
    begin
      if (u<>-1)and(v=-1) then inc(nn)
      else if (u=-1)and(v<>-1) then nn:=-1
      else if (u>v) then nn:=-1
    end;
    if nn>max then max:=nn;
    while true do
    begin
    inc(ll);
    if ll>l then begin fl:=false; exit; end;
    readln(ss);
    if ss[1]='F' then dfs(ss,nn);
    if ss[1]='E' then
    begin
      f[ord(s[3])-ord('a')]:=true;
      if n<>-1 then
      begin
        if (u<>-1)and(v=-1) then dec(nn)
        else if (u=-1)and(v<>-1) then nn:=n
        else if (u>v) then nn:=n
      end;
      exit;
    end;
    end;
  end
  else
  while true do
    begin
    inc(ll);
    if ll>l then begin fl:=false; exit; end; readln;
    fl:=false;
    end;
end;




begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  readln(t);
  for ii:=1 to t do
  begin
    read(l);
    fillchar(f,sizeof(f),true);
    readln(s);
    m:=0;
    max:=0; ll:=0; fl:=true;
    for i:=6 to length(s)-1 do
    begin
      val(s[i],p);
      m:=m*10+p;
    end;
    if (s[4]='1')and(s[3]='(') then m:=0;
    if (l=0)and(m=0) then writeln('Yes');
    if (l=0)and(m<>0)then writeln('No');
    while (ll<l) do
    begin
      inc(ll);
      readln(s);
      if (s[1]='F')and fl then dfs(s,0);
      if (s[1]='E')and fl then fl:=false;
    end;
    if not fl then writeln('ERR')
    else if m=max then writeln('Yes') else writeln('No');
  end;
  close(input);
  close(output);
end.



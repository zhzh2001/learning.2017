var
  ii,i,j,k,l,t,m,n,x,p,mm,cost,sum:longint;
  head,dist:array[0..100000]of longint;
  ab:array[0..100000]of boolean;
  f:boolean;
  v,next,w,time:array[0..200000]of longint;

procedure dj;
var no,i,j,min,ii:longint;
begin
  for ii:=1 to n-1 do
  begin
    min:=maxlongint;
    for i:=1 to n do
      if (dist[i]<min)and(ab[i]) then
      begin
        min:=dist[i];
        no:=i;
      end;
    ab[no]:=false;
    j:=head[no];
    while j<>0 do
    begin
      if w[j]+dist[no]<dist[v[j]] then dist[v[j]]:=w[j]+dist[no];
      j:=next[j];
    end;
  end;
end;

procedure dfs(x:longint);
var i,j:longint;
begin
 // writeln(x,' ','cost=',cost);
  if not f then exit;
  if cost>mm+k then exit;
  if (x=n) then sum:=(sum+1)mod p;
  j:=head[x];

  //inc(time[x]); if time[x]>100 then begin f:=false; exit; end;
  while j<>0 do
  begin
    cost:=cost+w[j];
  //  writeln(x,' ',v[j],'cost=',cost);
    dfs(v[j]);
    cost:=cost-w[j];
    j:=next[j];

  end;
end;


begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input);
  rewrite(output);
  readln(t);
  for ii:=1 to t do
  begin
    readln(n,m,k,p);
    fillchar(v,sizeof(v),0);
    fillchar(w,sizeof(w),0);
    fillchar(head,sizeof(head),0);
    fillchar(next,sizeof(next),0);
    for i:=1 to m do
    begin
      readln(x,v[i],w[i]);
      next[i]:=head[x];
      head[x]:=i;
    end;
    fillchar(ab,sizeof(ab),true);
    fillchar(dist,sizeof(dist),127);
    fillchar(time,sizeof(time),0);
    dist[1]:=0;
    dj;
    sum:=0;  f:=true;
    mm:=dist[n];
    dfs(1);

    if f then writeln(sum) else writeln(-1);
  end;
  close(input);
  close(output);
end.



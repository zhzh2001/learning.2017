#include<cstdio>
#include<cstring>
#define LL long long
using namespace std;

LL read(){
	LL x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

LL n,m;
int A[2000001];

int main(){
//	freopen("math.in","r",stdin);
//	freopen("math.out","w",stdout);
	n=read(),m=read();
	for (int i=0; i<=1000; i++)
		for (int j=0; j<=1000; j++)
			A[i*n+j*m]=1;
	for (int i=1000; i>0; i--) if (A[i]==0) {printf("%d",i);break;}
}

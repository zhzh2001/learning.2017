#include<cstdio>
#include<cstring>
#include<iostream>
#define LL long long
using namespace std;

LL read(){
	LL x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

LL n,m;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=read(),m=read();
	cout<<n*m-n-m;
}

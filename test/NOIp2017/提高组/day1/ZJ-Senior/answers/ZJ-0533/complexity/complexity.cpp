#include<cstdio>
#include<cstring>
#include<cmath>
#define LL long long
#define Max(a,b) ((a<b?b:a))
#define Min(a,b) ((a>b?b:a))

using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

int X=0,Ans=0,n,Re,f,tt,t1,t2,x1;
char A[201];
bool F[201];
int H[201];
char ch[100];

bool pp(char ch){
	for (int i=1; i<=Max(tt-1,X); i++) if (A[i]==ch) return 1;
	return 0;
}

void work(){
		n=read();
		X=0,Ans=0,H[0]=0;f=0;F[0]=0;
		gets(ch);
		int x=0;while (ch[x]!='(') x++;x++;
		Re=0;
		if (ch[x]!='1'){if (ch[x]=='n') x+=2;
			while (ch[x]!=')') Re=Re*10+ch[x]-48,x++;
		}
		for (int i=1; i<=n; i++) {
			char c=getchar();tt=0;
			while (c==' ' || c==10) c=getchar();
			if (f==1) {char ch3[101];gets(ch3);continue;}
			if (c=='F') {
				char ch1=getchar();ch1=getchar();
				char ch4=getchar();
				char ch2[201];gets(ch2);ch2[strlen(ch2)]=' ';
				t1=0,t2=0,x1=0;
				while (ch2[x1]!=' ') 
					{if (ch2[x1]=='n') t1=10086,x1++; else t1=t1*10+ch2[x1]-48,x1++;}x1++;
				while (ch2[x1]!=' ') 
					{if (ch2[x1]=='n') t2=10086,x1++; else t2=t2*10+ch2[x1]-48,x1++;}
				if (pp(ch1)) {f=1;continue;}
				if (t1==t2 || t1<t2 && t2!=10086) H[0]++,H[H[0]]=X;
				if (t1>t2) {
					tt=X+1;A[tt]=ch1;
					while (tt>X) {
						char cc[101];gets(cc);
						if (cc[0]=='F'){tt++;if (pp(cc[2])) f=1;A[tt]=cc[2];}
						if (cc[0]=='E')tt--;
						i++;
						if (i>n) break;
					}
					if (i>n) break; 
				}
				if (t1<t2 && t2==10086) {
					X++;A[X]=ch1;
				}
			}
			if (c=='E') {
				if (X>Ans) Ans=X;
				if (H[H[0]]==X && H[0]>0) H[0]--; else X--;
				if (X<0) {f=1;continue;}
			}
		}
		if (f==1 || X!=0) printf("ERR\n"); else if (Ans==Re) printf("Yes\n"); else printf("No\n");
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while (T--){
		work();
	}
}

#include<string.h>
#include<cstdio>
#include<cctype>
const int N=1e5+10;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
int v[N],w[N],nxt[N],pt[N],cnt,ds[N],T,n,m,K,P,tr,ans,x,y;
int h,t,q[N],ct[N],u;
inline void add(int x,int y,int z){
	v[++cnt]=y,w[cnt]=z,nxt[cnt]=pt[x],pt[x]=cnt;
}
inline void dfs(int x){
	for(int i=pt[x];i;i=nxt[i]){
		int t=ds[v[i]];
		if(ds[x]+w[i]<ds[v[i]]&&ds[x]+w[i]<=tr){
			if(v[i]==n){ans=(ans+1)%P;continue;}
			ds[v[i]]=ds[x]+w[i];
			dfs(v[i]);
			ds[v[i]]=t;	
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=In();
	while(T--){
		n=In(),m=In(),K=In(),P=In();
		for(int i=0;i<m;i++){
			x=In(),y=In();
			add(x,y,In());
		}
		memset(ds,0x3f,sizeof(ds));
		h=0;q[t=1]=1;ds[1]=0,ct[1]=1;
			while(h!=t){
				u=q[h=(h+1)%n];
				for(int i=pt[u];i;i=nxt[i])
					if(ds[u]+w[i]<ds[v[i]]&&ds[u]+w[i]<=ds[n]){
						ds[v[i]]=ds[u]+w[i];
						ct[v[i]]=1;
						q[t=(t+1)%n]=v[i];
					}else 
						if(ds[u]+w[i]==ds[v[i]]&&ds[u]+w[i]<=ds[n])
							ct[v[i]]=(ct[v[i]]+1)%P,q[t=(t+1)%n]=v[i];
			}
		if(!K)printf("%d\n",ct[n]);
		else{
			tr=ds[n]+K;
			memset(ds,0x3f,sizeof(ds));
			ds[1]=ans=0;
			dfs(1);
			printf("%d\n",ans);
		}
	}
}

#include<string.h> 
#include<cstdio>
#include<cctype>
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
int T,cnl,L,r[200],x,y,ty,tr,k,lf,ff,mt,pl[200];
char c,pv[200];
inline void Ed(){
	while(c!='\n')c=getchar();
}
inline int max(int x,int y){
	return (x>y)?x:y;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(r,0,sizeof(r));
		memset(pl,0,sizeof(pl));
		cnl=lf=tr=ff=mt=0;
		L=In(),c=getchar();
		while(c!='(')c=getchar();
		c=getchar();
		if(c=='n')cnl=1,ty=In();
		else ty=1; 
		Ed();
		for(k=0;k<L;k++){
			x=y=0;
			c=getchar();
			if(c=='F'){
				c=getchar();lf++;
				while(!isalpha(c))c=getchar();
				if(r[c]){
					Ed();
					goto ER;
				}r[c]++;pv[lf]=c;
				c=getchar();
				while(!isalpha(c)&&!isdigit(c))c=getchar();
				if(c=='n')x=110;
				else
					while(isdigit(c))x=x*10+c-'0',c=getchar();
				c=getchar();
				while(!isalpha(c)&&!isdigit(c))c=getchar();
				if(c=='n')y=110;
				else
					while(isdigit(c))y=y*10+c-'0',c=getchar();
				if(!ff&&x>y)ff=1;
				if(!ff&&x<100&&y==110)mt=max(++tr,mt),pl[lf]=1;
				Ed();
			}else{
				Ed();
				if(pl[lf])pl[lf]=0,tr--;
				r[pv[lf]]=0;
				if(--lf<0)goto ER;
			}
		}
		if(lf)goto ER;
		if(cnl&&mt==ty)goto Suc;
		else if(!cnl&&!mt)goto Suc;
		else goto WR;
		Suc:puts("Yes");continue;
		WR:puts("No");continue;
		ER:for(++k;k<L;k++)c=getchar(),Ed();
		   puts("ERR");continue;
	}
}

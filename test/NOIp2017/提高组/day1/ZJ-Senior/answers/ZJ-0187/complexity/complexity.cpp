#include<stdio.h>
#include<iostream>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<queue>
#include<bitset>
#include<set>
#include<vector>
#include<stdlib.h>
#define N 105
#define ll long long
#define oo 1000000000
#define zyy 1000000007
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
void writeln(int x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int n,pap,mp,q[N],top;
struct data{
	int p,l,r,ak;
}a[N];
char s[105];
bool vis[256];
int qwq;
int dfs(int x){
	for(int i=x+1;i<=n;i++){
		if(a[i].ak==1){
			if(top<=0) return -1;
			vis[a[q[top]].p]=0;
			if(q[top]==x){top--;return i;}
			top--;
		}else{
			q[++top]=i;
			if(vis[a[i].p]){return -1;}
			vis[a[i].p]=1;
			int l=a[i].l,r=a[i].r;
			if(l>r) i=dfs(i);
			if(i==-1) return -1;
		}
	}
	return -1;
}
void solve(){
	n=read();scanf("%s",s+1);
	memset(q,0,sizeof q);
	int m=strlen(s+1);
	if(m==4) pap=1,mp=0;
	else{
		mp=1;pap=0;
		Forn(i,5,m) pap=pap*10+(s[i]-48);
	}
	memset(a,0,sizeof a);
	memset(vis,0,sizeof vis);
	For(i,1,n){
		scanf("%s",s);
		if(s[0]=='E') a[i].ak=1;
		else{
			scanf("%s",s);
			a[i].p=s[0];
			scanf("%s",s);
			if(s[0]=='n') a[i].l=1000;
			else{
				m=strlen(s);
				Forn(j,0,m) a[i].l=a[i].l*10+s[j]-48;
			}
			scanf("%s",s);
			if(s[0]=='n') a[i].r=1000;
			else{
				m=strlen(s);
				Forn(j,0,m) a[i].r=a[i].r*10+s[j]-48;
			}
		}
	}
	int pyop=0,ans=0;top=0;
	For(i,1,n){
		if(a[i].ak==0){
			q[++top]=i;
			if(vis[a[i].p]){puts("ERR");return;}
			vis[a[i].p]=1;
			int l=a[i].l,r=a[i].r;
			if(l>r){
				i=dfs(i);
				if(i==-1){puts("ERR");return;}
			}
			else if(r-l>100) pyop++;
			ans=max(ans,pyop);
		}else{
			if(!top){puts("ERR");return;}
			vis[a[q[top]].p]=0;
			int l=a[q[top]].l,r=a[q[top]].r;top--;
			if(r-l>100) pyop--;
		}
	}
	if(top){puts("ERR");return;}
	if(mp==0){
		if(ans==0) puts("Yes");
		else puts("No");
	}else{
		if(pap==ans) puts("Yes");
		else puts("No");
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while(T--) solve();
	return 0;
}

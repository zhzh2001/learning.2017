#include<stdio.h>
#include<iostream>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<queue>
#include<bitset>
#include<set>
#include<vector>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
using namespace std;
ll read(){
	ll x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
void writeln(ll x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
ll n,m;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=read();m=read();
	writeln((n-1)*m-n);
	return 0;
}

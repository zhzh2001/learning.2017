#include<stdio.h>
#include<iostream>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<queue>
#include<bitset>
#include<set>
#include<vector>
#include<stdlib.h>
#define N 100005
#define ll long long
#define oo 1000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
void writeln(int x){
	if(x<0) x=-x,putchar('-');
	write(x);putchar('\n');
}
int cnt,head[N],Head[N],n,m,k,zyy;
struct edge{int nxt,to,w;}e[N<<1],E[N<<1];
void insert(int u,int v,int w){
	e[++cnt]=(edge){head[u],v,w};head[u]=cnt;
	E[cnt]=(edge){Head[v],u,w};Head[v]=cnt;
}
struct data{
	int x,val;
	bool operator <(const data &a)const{
		return val>a.val;
	}
};
int dis[N];
ll f[N];
bool vis[N];
void solve(){
	n=read();m=read();k=read();zyy=read();
	cnt=0;memset(head,0,sizeof head);
	memset(vis,0,sizeof vis);
	For(i,1,m){
		int x=read(),y=read(),w=read();
		insert(x,y,w);
	}
	priority_queue <data> Q;
	Q.push((data){1,0});
	memset(dis,63,sizeof dis);
	dis[1]=0;f[1]=1;
	while(!Q.empty()){
		data x=Q.top();Q.pop();
		if(vis[x.x]) continue;
		vis[x.x]=1;
		for(int i=head[x.x];i;i=e[i].nxt){
			if(dis[x.x]+e[i].w<dis[e[i].to]) dis[e[i].to]=dis[x.x]+e[i].w,f[e[i].to]=f[x.x],Q.push((data){e[i].to,dis[e[i].to]});
			else if(dis[x.x]+e[i].w==dis[e[i].to]) f[e[i].to]+=f[x.x];
		}
	}
	writeln(f[n]%zyy);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while(T--) solve();
	return 0;
}

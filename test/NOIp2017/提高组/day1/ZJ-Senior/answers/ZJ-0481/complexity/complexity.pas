var
  i,j,k,n,t,ans,cnt,tt,w,top,x,y,p:longint;
  a:array[-101..101] of longint;
  b:array['a'..'z'] of boolean;
  c:array[-101..101] of char;
  f,s,xx,yy,nn,ww:string;
  err:boolean;
 procedure init;
 begin
   assign(input,'complexity.in');
   assign(output,'complexity.out');
   reset(input);
   rewrite(output);
 end;
 procedure cl;
 begin
   close(input);
   close(output);
 end;
begin
  init;
  readln(t);
  for tt:=1 to t do
  begin
    readln(s);
    p:=pos(' ',s);
    nn:=copy(s,1,p-1);
    delete(s,1,p);
    val(nn,n);
    if (s[3]='1') then w:=0 else
    begin
      delete(s,1,4);
      p:=pos(')',s);
      ww:=copy(s,1,p-1);
      val(ww,w);
    end;
    err:=false; ans:=0; top:=0;
    fillchar(a,sizeof(a),0);
    fillchar(b,sizeof(b),false);
    for i:=-100 to 100 do c[i]:=' ';
    for i:=1 to n do
    begin
      readln(f);
      if f[1]='F' then
      begin
        if b[f[3]] then err:=true;
        b[f[3]]:=true;
        c[top+1]:=f[3];
        delete(f,1,4);
        p:=pos(' ',f);
        xx:=copy(f,1,p-1);
        delete(f,1,p);
        yy:=f;
        if xx[1]='n' then x:=10000 else val(xx,x);
        if yy[1]='n' then y:=10000 else val(yy,y);
        if (y-x)<0 then cnt:=-999
        else if (y-x)>1000 then cnt:=1
        else cnt:=0;
        inc(top);
        a[top]:=a[top-1]+cnt;
        //writeln(top,' ',a[top]);
      end
      else
      begin
        dec(top);
        if (top<0) then err:=true;
        if ans<a[top+1] then ans:=a[top+1];
        if (c[top+1]>='a')and(c[top+1]<='z') then b[c[top+1]]:=false;
      end;
    end;
    if top<>0 then  err:=true;
    if (err) then writeln('ERR')
    else if (ans=w) then writeln('Yes') else writeln('No');
  end;
  cl;
end.
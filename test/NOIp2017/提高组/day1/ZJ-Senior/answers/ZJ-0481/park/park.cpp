#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100050;
const int M=200100;
int n,m,k,p,x,y,z,t,kk;
int a[2*M],b[2*M],fi[2*M],ne[2*M],la[2*M],dis[N],q[M*5],f[N],ans[N];
bool exist[N];
void add(int x,int y,int z)
{
	a[++kk]=y; b[kk]=z;
	if (!fi[x]) fi[x]=kk; else ne[la[x]]=kk;
	la[x]=kk;
}
void spfa2(int x)
{
	memset(f,0x3f,sizeof(f));
	f[x]=0; ans[x]=1;
	int h=0; int t=1;
	q[t]=x;
	while (h<t)
	{
		int now=q[++h];
		exist[now]=1;
		for (int i=fi[now];i;i=ne[i])
		{
			if (f[now]+b[i]<=f[a[i]])
			{
				f[a[i]]=f[now]+b[i];
				 
				if (!exist[a[i]]) 
				{
					q[++t]=a[i];
					if (f[now]+b[i]==dis[a[i]]) ans[a[i]]=(ans[a[i]]+1)%p;
				}
			}
		}
		exist[now]=0;
	}
}
void spfa1(int x)
{
	memset(dis,0x3f,sizeof(dis));
	dis[x]=0;
	int h=0; int t=1;
	q[t]=x;
	while (h<t)
	{
		int now=q[++h];
		exist[now]=1;
		for (int i=fi[now];i;i=ne[i])
		{
			if (dis[now]+b[i]<dis[a[i]])
			{
				dis[a[i]]=dis[now]+b[i];
				if (!exist[a[i]]) q[++t]=a[i];
			}
		}
		exist[now]=0;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		kk=0;
		memset(a,0,sizeof(a)); memset(b,0,sizeof(b));
		memset(fi,0,sizeof(fi)); memset(ne,0,sizeof(ne));
		memset(la,0,sizeof(la));
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		spfa1(1);
		spfa2(1);
		//for (int i=1;i<=n;i++) printf("%d ",ans[i]);
		printf("%d\n",ans[n]);
		memset(ans,0,sizeof(ans));
	}
	return 0;
}

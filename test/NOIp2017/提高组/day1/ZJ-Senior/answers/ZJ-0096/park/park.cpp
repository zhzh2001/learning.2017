#include<cstdio>
#include<iostream>
#include<cstring>
#include<queue>
#include<algorithm>
using namespace std;
const int N=1010,M=2010;
int n,m,k,mod,midis,ans;
int to[M],len[M],edge_tot;
vector<int>point[N];
void add_edge(int f,int t,int l){
	to[edge_tot]=t,len[edge_tot]=l;
	point[f].push_back(edge_tot++);
	return;
}
int dfs(int now,int d){
	if(now==n) return 1;
	int x,cnt=0;
	for(int i=0;i<point[now].size();i++){
		x=to[point[now][i]];
		if(d+len[point[now][i]]<=midis+k) cnt=(cnt+dfs(x,d+len[point[now][i]]))%mod;
	}
	return cnt;
}
bool inq[N];
int dis[N];
int spfa(){
	queue<int>q;
	memset(inq,0,sizeof(inq));
	memset(dis,127/3,sizeof(dis));
	q.push(1);
	inq[1]=1,dis[1]=0;
	int now,x;
	while(!q.empty()){
		now=q.front();q.pop();
		inq[now]=0;
		for(int i=0;i<point[now].size();i++){
			x=to[point[now][i]];
			if(dis[x]>dis[now]+len[point[now][i]]){
				dis[x]=dis[now]+len[point[now][i]];
				if(!inq[x]){inq[x]=1;q.push(x);}
			}
		}
	}
	return dis[n];
}
void solve(){
	ans=0;
	scanf("%d%d%d%d",&n,&m,&k,&mod);
	if(n>1000&m>2000){
		printf("-1\n");
		return;
	}
	edge_tot=0;
	int t1,t2,t3;
	for(int i=1;i<=m;i++){scanf("%d%d%d",&t1,&t2,&t3);add_edge(t1,t2,t3);}
	midis=spfa();
	ans=dfs(1,0);
	printf("%d\n",ans);
	for(int i=1;i<=n;i++) point[i].clear();
	return;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout); 
	int T;scanf("%d",&T);
	while(T--) solve();
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=510;
struct Node{
	int bl,cnt;
	int flag;
	Node(int _bl=0,int _cnt=0,int _flag=0){bl=_bl,cnt=_cnt,flag=_flag;}
}sta[N];
int l,top,p,ii[N],mul[N];
bool opt[N],vis[30];
char s[10],t1[10],t2[10];
void solve(){
	memset(opt,0,sizeof(opt));
	memset(ii,0,sizeof(ii));
	memset(mul,0,sizeof(mul));
	memset(vis,0,sizeof(vis));
	scanf("%d%s",&l,&s);
	top=p=0;
	int totf=0,tote=0;
	int ansp=0,x1,x2;
	if(s[2]!='1') for(int i=4;s[i]>='0'&&s[i]<='9';i++) p=p*10+s[i]-'0';
	for(int i=1;i<=l;i++){
		scanf("%s",&s);
		if(s[0]=='F'){
			x1=x2=0,totf++;
			opt[i]=1;
			scanf("%s",&s);
			ii[i]=s[0]-'a';
			scanf("%s%s",&t1,&t2);
			if(t1[0]!='n') for(int j=0;t1[j]>='0'&&t1[j]<='9';j++) x1=x1*10+t1[j]-'0';
			else x1=-1;
			if(t2[0]!='n') for(int j=0;t2[j]>='0'&&t2[j]<='9';j++) x2=x2*10+t2[j]-'0';
			else x2=-1;
			if(x2==-1){
				if(x1==-1) mul[i]=0;
				else mul[i]=1;
			}else{
				if(x1==-1) mul[i]=-1;
				else if(x2>=x1) mul[i]=0;
				else mul[i]=-1;
			}
		}else{
			tote++;
		}
	}
	if(totf!=tote){
		puts("ERR");
		return;
	}
	for(int i=1;i<=l;i++){
		if(opt[i]){
			if(vis[ii[i]]){puts("ERR");return;}
			vis[ii[i]]=1;
			sta[top++]=Node(ii[i],0,mul[i]);
		}else{
			top--;
			vis[sta[top].bl]=0;
			if(top>0){
				if(sta[top].flag!=-1) sta[top-1].cnt=max(sta[top-1].cnt,sta[top].cnt+sta[top].flag);
			}
			else{
				if(sta[top].flag!=-1) ansp=max(ansp,sta[top].cnt+sta[top].flag);
			}
		}
	}
	if(ansp==p) puts("Yes");
	else puts("No");
	return;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--) solve();
	return 0;
}

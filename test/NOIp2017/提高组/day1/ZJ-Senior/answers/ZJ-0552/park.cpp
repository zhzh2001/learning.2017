#include <cstdio>
#include <algorithm>
#include <queue>
#define fi first
#define se second
#define MP make_pair
using namespace std;
typedef pair<int, int> PII;

int read()
{
	int v = 0;
	char c = getchar();
	while (c < 48 || 57 < c) c = getchar();
	while (48 <= c && c <= 57) v = v * 10 + c - 48, c = getchar();
	return v;
}

const int N = 101000, M = 201000;

int f[N][51], S, T, belong[N], dfn[N], low[N], num[N], q[N], vis[N], a[M], b[M], c[M], n, top, id, cnt, dis[N], k, MOD, m, stk[N], init[N];
bool g[N][51];

struct Edge
{
	int nxt, to, val;
} eg[M];
int head[N], en;

void setEdge(int u, int v, int w)
{
	eg[++en] = (Edge) {head[u], v, w};
	head[u] = en;
}

void dfs(int u)
{
	dfn[u] = low[u] = ++cnt;
	stk[++top] = u;
	for (int e = head[u]; e; e = eg[e].nxt)
		if (eg[e].val == 0)
		{
			int v = eg[e].to;
			if (!dfn[v])
			{
				dfs(v);
				low[u] = min(low[u], low[v]);
			} else
				if (!belong[v])
					low[u] = min(low[u], dfn[v]);
		}
	if (dfn[u] == low[u])
	{
		id++;
		while (stk[top] != u)
			belong[stk[top--]] = id;
		belong[stk[top--]] = id;
	}
}

priority_queue<PII, vector<PII>, greater<PII> > Q;

void dij()
{
	for (int i = 1; i <= n; i++) dis[i] = 2e9;
	dis[S] = 0;
	for (int i = 1; i <= n; i++) vis[i] = 0;
	Q.push(MP(0, S));
	while (!Q.empty())
	{
		int u = Q.top().se;
		Q.pop();
		if (vis[u]) continue;
		vis[u] = 1;
		for (int e = head[u]; e; e = eg[e].nxt)
		{
			int v = eg[e].to;
			if (dis[u] + eg[e].val < dis[v])
			{
				dis[v] = dis[u] + eg[e].val;
				Q.push(MP(dis[v], v));
			}
		}
	}
}

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int cas = read();
	while (cas--)
	{
		n = read(), m = read(), k = read(), MOD = read();
		en = 0;
		for (int i = 1; i <= n; i++) head[i] = 0;
		for (int i = 1; i <= m; i++)
		{
			a[i] = read(), b[i] = read(), c[i] = read();
			setEdge(a[i], b[i], c[i]);
		}
		for (int i = 1; i <= n; i++) dfn[i] = low[i] = belong[i] = 0;
		top = id = cnt = 0;
		for (int i = 1; i <= n; i++)
			if (!dfn[i])
				dfs(i);
		for (int i = 1; i <= id; i++) num[i] = 0;
		for (int i = 1; i <= n; i++) num[belong[i]]++;
		S = belong[1], T = belong[n];
		n = id;
		en = 0;
		for (int i = 1; i <= n; i++) head[i] = 0;
		for (int i = 1; i <= m; i++)
			if (belong[a[i]] != belong[b[i]])
				setEdge(belong[a[i]], belong[b[i]], c[i]);
		dij();
		for (int i = 1; i <= n; i++)
			for (int j = 0; j <= k; j++)
				f[i][j] = 0;
		for (int i = 1; i <= n; i++) vis[i] = 0;
		for (int i = 1; i <= n; i++) init[i] = 0;
		int hd = 1, tail = 0;
		for (int i = 1; i <= n; i++)
			for (int e = head[i]; e; e = eg[e].nxt)
			{
				int v = eg[e].to;
				if (dis[i] + eg[e].val == dis[v])
					init[v]++;
			}
		for (int i = 1; i <= n; i++)
			if (init[i] == 0)
				q[++tail] = i;
		while (hd <= tail)
		{
			int u = q[hd++];
			for (int e = head[u]; e; e = eg[e].nxt)
			{
				int v = eg[e].to;
				if (dis[u] + eg[e].val == dis[v])
				{
					init[v]--;
					if (init[v] == 0)
						q[++tail] = v;
				}
			}
		}
		f[S][0] = 1;
		g[S][0] = 1;
		for (int j = 0; j <= k; j++)
			for (int i = 1; i <= n; i++)
			{
				int u = q[i];
				if (g[u][j] && num[u] > 1) f[u][j] = -1;
				for (int e = head[u]; e; e = eg[e].nxt)
				{
					int v = eg[e].to;
					int t = dis[u] + j + eg[e].val - dis[v];
					if (t > k) continue;
					if (f[u][j] == -1)
						f[v][t] = -1;
					else
					{
						f[v][t] += f[u][j];
						if (f[v][t] >= MOD) f[v][t] -= MOD;
					}
					g[v][t] |= g[u][j];
				}
			}
		int ans = 0;
		for (int i = 0; i <= k; i++)
			if (f[T][i] == -1 || ans == -1)
				ans = -1;
			else
				ans = (ans + f[T][i]) % MOD;
		printf("%d\n", ans);
	}
}

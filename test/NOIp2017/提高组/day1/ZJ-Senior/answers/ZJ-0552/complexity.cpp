#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

const int N = 100000;

int pos, n;
char ch[10000];
int ans[N], stk[N], vis[N], var[N];

void G(char *ch)
{
	int i = 0;
	char c;
	while ((c = getchar()) != '\n')
		ch[i++] = c;
	ch[i] = 0;
}

int get()
{
	if (ch[pos] == 'n')
	{
		pos += 2;
		return -1;
	}
	int v = 0;
	while ('0' <= ch[pos] && ch[pos] <= '9')
	{
		v = v * 10 + ch[pos] - '0';
		pos++;
	}
	pos++;
	return v;
}

void work()
{
	scanf("%d", &n);
	scanf("%s", ch);
	getchar();
	int given;
	if (ch[2] == '1')
		given = 0;
	else
	{
		pos = 4;
		given = get();
	}
	int top = 0;
	ans[0] = stk[0] = 0;
	for (int i = 0; i < 26; i++) vis[i] = 0;
	for (int i = 1; i <= n; i++)
	{
		G(ch);
		if (ch[0] == 'E' && top == 0)
		{
			for (int j = i + 1; j <= n; j++) G(ch);
			puts("ERR");
			return ;
		}
		if (ch[0] == 'E')
		{
			vis[var[top]] = 0;
			if (stk[top - 1] >= 0)
				ans[top - 1] = max(ans[top - 1], stk[top - 1] + ans[top]);
			top--;
		}
		if (ch[0] == 'F')
		{
			if (vis[ch[2] - 'a'])
			{
				for (int j = i + 1; j <= n; j++) G(ch);
				puts("ERR");
				return ;
			}
			top++;
			var[top] = ch[2] - 'a';
			vis[ch[2] - 'a'] = 1;
			pos = 4;
			int t1 = get();
			int t2 = get();
			if (t1 >= 0 && t2 >= 0)
			{
				if (t1 <= t2)
					stk[top] = 0;
				else
					stk[top] = -1;
			} else
				if (t1 == -1 && t2 == -1)
					stk[top] = 0;
				else
					if (t1 == -1 && t2 >= 0)
						stk[top] = -1;
					else
						if (t1 >= 0 && t2 == -1)
							stk[top] = 1;
			ans[top] = stk[top];
		}
		for (int i = 0; ch[i]; i++) ch[i] = 0;
	}
	if (top)
	{
		puts("ERR");
		return ;
	}
	if (ans[0] == given) puts("Yes"); else puts("No");
}

int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int cas;
	scanf("%d", &cas);
	while (cas--)
		work();
}

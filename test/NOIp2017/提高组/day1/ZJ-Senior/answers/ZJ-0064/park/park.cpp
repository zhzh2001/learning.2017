#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 200005
#define INF 1000000000
int w,p,K,L,n,m,k,l,r,t,s,ans,P,x,y,z,num,T;
int fl[N],head[100005],vet[N],next[N],cost[N],dis[N],X[N],Y[N],Z[N],flag[N],q[N],f[100005][51];
int num1,head1[100005],vet1[N],next1[N],a[100005],d[100005];
void add(int a,int b,int c){
	next[++num]=head[a];
	head[a]=num;
	vet[num]=b;
	cost[num]=c;
}
void spfa(){
	q[1]=n;int l=0,r=1;
	for (int i=1;i<=n;i++)dis[i]=INF;
	dis[n]=0;flag[n]=1;
	while(l!=r){
		l=l%n+1;
		int u=q[l];
		for (int e=head[u];e;e=next[e]){
			int v=vet[e];
			if(dis[u]+cost[e]<dis[v]){
				dis[v]=dis[u]+cost[e];
				if(!flag[v]){
					flag[v]=1;
					r=r%n+1;
					q[r]=v;
				}
			}
		}flag[u]=0;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		num=0;p=0;w=0;
		memset(fl,0,sizeof(fl));
		memset(head,0,sizeof(head));
		memset(f,0,sizeof(f));f[1][0]=1;
		scanf("%d%d%d%d",&n,&m,&K,&P);
		for (int i=1;i<=m;i++)scanf("%d%d%d",&X[i],&Y[i],&Z[i]);
		for (int i=1;i<=m;i++)add(Y[i],X[i],Z[i]);
		spfa();L=dis[1];
		num=0;memset(head,0,sizeof(head));
		for (int i=1;i<=m;i++)add(X[i],Y[i],Z[i]);
		num1=0;memset(head1,0,sizeof(head1));
		memset(d,0,sizeof(d));
		for (int i=1;i<=m;i++)
			if(dis[X[i]]==dis[Y[i]]+Z[i]){
				d[Y[i]]++;
				next1[++num1]=head1[X[i]];
				head1[X[i]]=num1;
				vet1[num1]=Y[i];
			}
		r=0;for (int i=1;i<=n;i++)if(d[i]==0)q[++r]=i;
		l=0;
		while(l<r){
			int u=q[++l];
			for (int e=head1[u];e;e=next1[e]){
				int v=vet1[e];
				d[v]--;if(d[v]==0)q[++r]=v;
			}
		}for (int i=1;i<=n;i++)if(d[i]>0)fl[++w]=i;
		for (int k=0;k<=K;k++){
			for (int i=1;i<=r;i++){
				int u=q[i];
				if(f[u][k]>0)
				for (int e=head1[u];e;e=next1[e]){
					int v=vet1[e];
					f[v][k]=(f[v][k]+f[u][k])%P;
				}
			}
			for (int i=1;i<=w;i++)if(f[fl[i]][k]>0){p=1;break;}
			for (int u=1;u<=n;u++)if(f[u][k]>0)
				for (int e=head[u];e;e=next[e]){
					int v=vet[e];
					s=cost[e]-(dis[u]-dis[v]);
					if(s>0&&k+s<=K)f[v][s+k]=(f[v][s+k]+f[u][k])%P;
				}
		}
		if(p==1){puts("-1");continue;}
		ans=0;for (int i=0;i<=K;i++)ans=(ans+f[n][i])%P;
		printf("%d\n",ans);
	}
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define ll long long
ll n,m,k,l,x,y,t,a,b;
void exgcd(ll a,ll b,ll &x,ll &y){
	if(b==0){x=1,y=0;return;}
	exgcd(b,a%b,x,y);
	t=x;x=y;
	y=t-a/b*y;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if(n<=10000&&m<=10000){
		for (int i=n*m;i;i--){
		k=1;
		for (int j=0;j<=i/n;j++)
			if((i-j*n)%m==0){k=0;break;}
		if(k){printf("%d\n",i);break;}
		}
		return 0;
	}
	exgcd(n,m,x,y);
	x=x%m;y=y%n;
	if(x<y){swap(x,y);swap(n,m);}
	printf("%lld\n",n*(m-x-1)-(y+1)*m+1);
}

#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,k,l,t,s;
int gcd(int a,int b){
	if(a%b==0)return b;
	return gcd(b,a%b);
}
int main(){
	freopen("math.in","w",stdout);
	srand(time(NULL));
	n=rand()%1000;m=rand()%1000;
	while(gcd(n,m)>1)n=rand()%1000;m=rand()%1000;
	printf("%d %d\n",n,m);
}

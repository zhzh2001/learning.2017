#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 505
int T,n,m,k,l,t,s,a,b,c,x,y,err,ans;
char st[N],X[10];
int flag[N],f[N],p[N];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		ans=err=0;
		t=0;f[0]=0;memset(flag,0,sizeof(flag));
		scanf("%d",&n);k=0;
		scanf("%s",st+1);
		l=strlen(st+1);
		if(st[3]=='n')
			for (int i=5;i<l;i++)k=k*10+st[i]-48;
		for (int i=1;i<=n;i++){
			scanf("%s",st);
			if(st[0]=='E'){
				flag[p[t]]=0;
				t--;
				if(t<0)err=1;
			}else{
				scanf("%s",X);
				if(flag[X[0]])err=1;
				flag[X[0]]=1;
				scanf("%s",st);
				if(st[0]=='n')x=1000;
				else{
					l=strlen(st);
					x=0;for (int j=0;j<l;j++)x=x*10+st[j]-48;
				}
				scanf("%s",st);
				if(st[0]=='n')y=1000;
				else{
					l=strlen(st);
					y=0;for (int j=0;j<l;j++)y=y*10+st[j]-48;
				}
				if(err)continue;
				f[++t]=f[t-1];p[t]=X[0];
				if(x>y)f[t]=-1;
				if(y-x>100&&f[t-1]>=0)f[t]++;
				ans=max(f[t],ans);
			}
		}
		if(t!=0||err==1){puts("ERR");continue;}
		if(k==ans)puts("Yes");else puts("No");
	}
}

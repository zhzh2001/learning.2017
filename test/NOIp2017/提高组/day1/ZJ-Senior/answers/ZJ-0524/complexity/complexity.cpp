#include<cstdio>
#include<set>
#include<cstring>
using namespace std;
set<char> s;
char st[10000];
int stn[10000];
int stdo[10000];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d\n",&t);
	for(int i=1;i<=t;++i)
	{
//		memset(st,0,sizeof(st));
//		memset(stn,0,sizeof(stn));
//		memset(stdo,0,sizeof(stdo));
		int t_max=0;
		int ans=0,ans_do=0,max_ans=0;
		int l;
		s.clear();
		scanf("%d ",&l);
		char o1;
		int o2=0;
		scanf("O(%c",&o1);
		if(o1=='n')
			scanf("^%d)\n",&o2);
		else scanf(")\n");
			
//		printf("%d %c %d\n",l,o1,o2);	
			
		bool flag=0;
		for(int j=1;j<=l;++j)
		{
			char c1;
			scanf("%c ",&c1);
//			printf("%c\n",c1);
			if(c1=='F')
			{
				t_max++;
				char ci;
				scanf("%c ",&ci);
//				printf("ci %c\n",ci);
				if(s.count(ci))
					flag=1;
				s.insert(ci);
				st[t_max]=ci;
				char cc;
				char sx[100];
				int tx=0,x;
				scanf("%s ",sx);
				if(sx[0]!='n')
					sscanf(sx,"%d",&x);
					else x='n';
//				printf("x %d\n",x);
				char sy[100];
				int ty=0,y;
				scanf("%s ",sy);
				if(sy[0]!='n')
					sscanf(sy,"%d",&y);
					else y='n';
//				printf("y %d\n",y);
				if(x>y)
				{
					ans_do++;
					stdo[t_max]=1;
				}
				if(!ans_do&&y=='n'&&x!='n')
				{
					ans++;
					stn[t_max]=1;
				}
			}
			else
			{
				if(!t_max)
					flag=1;
				else
				{
					max_ans=ans>max_ans?ans:max_ans;
					if(stn[t_max])
					{
						stn[t_max]=0;
						ans--;
					}
					if(stdo[t_max])
					{
						stdo[t_max]=0;
						ans_do--;
					}
					s.erase(st[t_max--]);
				}
			}
//			printf("\n");
		}
		if(flag==1||t_max) printf("ERR\n");
		else
			if(max_ans)
			{
				if(max_ans==o2) printf("Yes\n");
				else printf("No\n");
			}
			else
				if(o1=='1') printf("Yes\n");
				else printf("No\n");
//		printf("\n\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <cstdio>
using namespace std;

long long read(){
	char ch=getchar();
	long long x=0;
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}

long long gcd(long long x,long long y){
	long long t;
	if (x<y) t=x,x=y,y=t;
	return x%y==0?y:gcd(y,x%y);
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	long long a,b,i,j,k,m,B,oo,A,ans;
	a=read(),b=read();
	if (a>b) i=a,a=b,b=i;
	k=b/a;
	m=b%a;
	oo=a*m/gcd(a,m);
	A=(oo-m)/a;
	j=(oo-m)%a;
	B=(oo-m)/m;
	i=B*k+A-1;
	ans=i*a+j;
	printf("%lld\n",ans);
	
}

#include <cstdio>
#include <cstring>
using namespace std;

const int N=998244353;
char cx[110][100];
bool flag[110];
int bl[110],from[110],to[110],f[110],zhan[110],jinru[110],fz[110];

int read(){
	char ch=getchar();
	int x=0;
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}

int read2(){
	char ch=getchar();
	int x=0;
	while(1){
		if (ch=='n'){
			while (ch<'0' || '9'<ch) ch=getchar();
			if (ch==')') break;
			while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
		}
		if (ch==')') break;
		ch=getchar();
	}
	return x;
}

void read3(int L){
	int i,j,k=1,x,ff;
	char ch=getchar();
	while (L){
		while (ch!=13){
			ff=1;
			if (ch=='F') f[k]=1;
			if ('a'<=ch && ch<='z'){
				bl[k]=ch-'a'+1;
				while (ch!=' ') ch=getchar();
				while (ch==' ') ch=getchar();
				x=0;
				while(1){
					if (ch=='n'){
						from[k]=N;
						ch=getchar();
						break;
					}
					while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
					if (ch==' '){
						from[k]=x;
						break;
					}
				}
				x=0;
				while(1){
					ch=getchar();
					if (ch=='n'){
						to[k]=N;
						ff=0;
						break;
					}
					while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
					if ((ch!='n') && (ch<'0' || '9'<ch)){
						to[k]=x;
						ff=0;
						break;
					}
				}
			}
			if (ff==0)	L--,k++;
			if (ch=='E') f[k]=2,L--,k++;
			if (L==0) return;
			ch=getchar();
		}
		ch=getchar();
	}
	return;
}

int check(int L){
	int i,j,k,deep=0;
	for (i=1;i<=L;i++){
		if (f[i]==1){
			deep++;
			zhan[deep]=i;
			if (flag[bl[i]]==0) flag[bl[i]]=1;else return 0;
		}
		if (f[i]==2){
			flag[bl[zhan[deep]]]=0;
			deep--;
			if (deep<0) return 0;
		}
	}
	if (deep!=0) return 0;
	return 1;
}

int pd(int deep){
	if (from[zhan[deep]]==to[zhan[deep]]) return 0;
	if (from[zhan[deep]]!=N && to[zhan[deep]]!=N) return 0;
	return 1;
}

int max(int x,int y){
	return x>y?x:y;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t,L,fzd,i,j,dui,deep;
	for (t=read();t>0;t--){
//		printf("%d\n",t);
		memset(flag,0,sizeof(flag));
		memset(from,0,sizeof(from));
		memset(to,0,sizeof(to));
		memset(bl,0,sizeof(bl));
		memset(jinru,0,sizeof(jinru));
		memset(fz,0,sizeof(fz));
		memset(zhan,0,sizeof(zhan));
		L=read();
		fzd=read2();
		read3(L);
//		for (i=1;i<=L;i++) printf("%d %d %d %d\n",f[i],bl[i],from[i],to[i]);
		dui=check(L);
		if (dui==0) printf("ERR\n");
		if (dui==1){
			jinru[0]=1;
			for (deep=0,i=1;i<=L;i++){
				if (f[i]==1 && jinru[zhan[deep]]==1){
					deep++;
					zhan[deep]=i;
					if (from[i]<=to[i]) jinru[i]=1;
				}
				if (f[i]==2){
					if (from[zhan[deep]]<=to[zhan[deep]]) fz[deep]=max(fz[deep+1]+pd(deep),fz[deep]);
//					if (t==10) printf("%d %d %d %d\n",zhan[deep],deep,pd(deep),fz[deep]);
					fz[deep+1]=0;
					deep--;
				}
			}
			if (fz[1]==fzd) printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}

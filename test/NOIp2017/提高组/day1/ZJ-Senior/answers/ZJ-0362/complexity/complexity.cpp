#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#define gc getchar()
#define ll long long
#define N 109
#define inf 0x3f3f3f3f
using namespace std;
int n,m,l1,l2,sta[N],top,vis[N],dis[N],Max,used[26];
char a[N],b[N],c[N],d[N];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return x*s;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read();
		scanf("%s",&a);
		l1=strlen(a);
		if (a[2]=='1') m=0;
		else
		{
			m=0;
			for (int i=4;i<l1;i++)
				if (a[i]>='0'&&a[i]<='9') m=m*10+a[i]-'0';
		}
		Max=top=0;
		memset(vis,0,sizeof(vis));
		memset(dis,0,sizeof(dis));
		memset(used,0,sizeof(used));
		bool flag=1;
		dis[sta[0]=0]=0;
		for (int i=1;i<=n;i++)
		{
			scanf("%s",a);
			if (a[0]=='F')
			{
				scanf("%s%s%s",&d,&b,&c);
				l1=strlen(b),l2=strlen(c);
				sta[++top]=d[0]-'a';
				if (used[sta[top]]) flag=0;
				else
				{
					used[sta[top]]=1;
					if (b[0]=='n')
					{
						if (c[0]=='n') dis[top]=dis[top-1];
						else dis[top]=-inf;
					}
					else
					{
						if (c[0]=='n') dis[top]=dis[top-1]+1;
						else
						{
							int s1=0,s2=0;
							for (int i=0;i<l1;i++)
								s1=s1*10+b[i]-'0';
							for (int i=0;i<l2;i++)
								s2=s2*10+c[i]-'0';
							if (s1<=s2) dis[top]=dis[top-1];
							else dis[top]=-inf;
						}
					}
					Max=max(Max,dis[top]);
				}
			}
			else
			{
				used[sta[top]]=0;
				if (!top)
				{
					flag=0;
					top++;
				}
				top--;
			}
		}
		if (top!=0) flag=0;
		if (!flag) puts("ERR");
		else
			if (Max==m) puts("Yes");
			else puts("No");
	}
	return 0;
}


#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <queue>
#define gc getchar()
#define ll long long
#define inf 0x3f3f3f3f
#define N 100002
#define M 200002
#define K 52
#define ADD(x,y) (x=((x+y>=mod)?(x+y-mod):x+y))
using namespace std;
int n,m,k,mod,first[N],number,dfn[N],low[N],sta[N],top;
int cnt,block,pos[N],size[N],a[M],b[M],c[M],vis[N],d[N*K];
int dis[N],Dis[N],Ans[N*K],vi[N][K],First[N*K],Number;
struct edge
{
	int to,next,val;
	void add(int x,int y,int z)
	{
		to=y,next=first[x],first[x]=number,val=z;
	}
}e[M];
struct Edge
{
	int to,next,val;
	void add(int x,int y,int z)
	{
		to=y,next=First[x],First[x]=Number,val=z;
		d[y]++;
	}
}E[M*K];
struct node
{
	int x,dis;
	node(int x=0,int dis=0):x(x),dis(dis){}
	bool operator >(const node &rhs) const
	{
		return dis<rhs.dis;
	}
	bool operator <(const node &rhs) const
	{
		return dis>rhs.dis;
	}
};
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return x*s;
}
void tarjan(int x)
{
	dfn[x]=low[x]=++cnt;
	vis[x]=1;
	sta[++top]=x;
	for (int i=first[x];i;i=e[i].next)
	{
		int y=e[i].to;
		if (e[i].val==0)
		{
			if (!dfn[y])
			{
				tarjan(y);
				low[x]=min(low[x],low[y]);
			}
			else if (vis[y]) low[x]=min(low[x],dfn[y]);
		}
	}
	if (dfn[x]==low[x])
	{
		++block;
		while (sta[top]!=x)
		{
			pos[sta[top]]=block;
			size[block]++;
			vis[sta[top]]=0;
			top--;
		}
		pos[x]=block;
		size[block]++;
		vis[x]=0;
		top--;
	}
}
void work1()
{
	for (int i=1;i<=block;i++) vis[i]=0,dis[i]=inf;
	dis[pos[1]]=0;
	priority_queue<node> q;
	while (!q.empty()) q.pop();
	q.push(node(pos[1],0));
	while (!q.empty())
	{
		node now=q.top();
		q.pop();
		int x=now.x;
		if (vis[x]) continue;
		vis[x]=1;
		for (int i=first[x];i;i=e[i].next)
		{
			int y=e[i].to;
			if (!vis[y])
			{
				if (now.dis+e[i].val<dis[y])
				{
					dis[y]=now.dis+e[i].val;
					q.push(node(y,dis[y]));
				}
			}	
		}
	}
}
void work2()
{
	for (int i=1;i<=block;i++) vis[i]=0,Dis[i]=inf;
	Dis[pos[n]]=0;
	priority_queue<node> q;
	while (!q.empty()) q.pop();
	q.push(node(pos[n],0));
	while (!q.empty())
	{
		node now=q.top();
		q.pop();
		int x=now.x;
		if (vis[x]) continue;
		vis[x]=1;
		for (int i=First[x];i;i=E[i].next)
		{
			int y=E[i].to;
			if (!vis[y])
			{
				if (now.dis+e[i].val<Dis[y])
				{
					Dis[y]=now.dis+E[i].val;
					q.push(node(y,Dis[y]));
				}
			}		
		}
	}
}
int get(int x,int y)
{
	return (x-1)*(k+1)+y;
}
node q[M*K];
int Q[M*K];
void work()
{
	memset(Ans,0,sizeof(Ans));
	memset(vi,0,sizeof(vi));
	int head=1,tail=0;
	q[++tail]=node(pos[1],0);
	while (head<=tail)
	{
		node now=q[head];
		head++;
		int x=now.x;
		if (vi[x][now.dis-dis[x]]) continue;
		vi[x][now.dis-dis[x]]=1;
		for (int i=first[x];i;i=e[i].next)
		{
			int y=e[i].to;
			if (now.dis+e[i].val<=dis[y]+k)
			{
				E[++Number].add(get(x,now.dis-dis[x]),get(y,now.dis+e[i].val-dis[y]),1);
				q[++tail]=node(y,now.dis+e[i].val);
			}
		}
	}
	head=1,tail=0;
	for (int i=0;i<n*(k+1);i++)
		if (!d[i]) Q[++tail]=i;
	Ans[get(pos[1],0)]=1;
	for (int i=0;i<n*(k+1);i++)
	{
		int now=Q[head];
		head++;
		for (int i=First[now];i;i=E[i].next)
		{
			ADD(Ans[E[i].to],Ans[now]);
			if (!(--d[E[i].to])) Q[++tail]=E[i].to;
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read(),m=read(),k=read(),mod=read();
		block=cnt=top=number=0;
		for (int i=1;i<=n;i++)
			pos[i]=dfn[i]=low[i]=vis[i]=first[i]=0;
		memset(size,0,sizeof(size));
		for (int i=1;i<=m;i++)
		{
			int x=read(),y=read(),z=read();
			e[++number].add(x,y,z);
			a[i]=x,b[i]=y,c[i]=z;
		}
		for (int i=1;i<=n;i++)
			if (!dfn[i]) tarjan(i);
		number=0;
		for (int i=1;i<=block;i++) first[i]=0;
		Number=0;
		for (int i=1;i<=block;i++) First[i]=0;
		for (int i=1;i<=m;i++)
			if (pos[a[i]]!=pos[b[i]])
			{
				e[++number].add(pos[a[i]],pos[b[i]],c[i]);
				E[++Number].add(pos[b[i]],pos[a[i]],c[i]);
			}
		work1();
		work2();
		bool flag=1;
		for (int i=1;i<=block;i++)
			if (size[i]>1&&dis[i]+Dis[i]<=dis[pos[n]]+k)
			{
				flag=0;
				break;
			}
		if (!flag)
		{
			puts("-1");
			continue;
		}
		Number=0;
		for (int i=0;i<n*(k+1);i++) d[i]=First[i]=0;
		work();
		int ret=0;
		for (int i=0;i<=k;i++) ADD(ret,Ans[get(pos[n],i)]);
		printf("%d\n",ret);
	}
	return 0;
}


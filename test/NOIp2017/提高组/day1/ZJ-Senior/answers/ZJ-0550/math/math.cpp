#include<cstdio>
#include<cctype>
typedef long long LL;
template <typename T>inline void swap(T&x,T&y){const T z=x;x=y;y=z;}
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
LL a,b;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(a),read(b);
	if(a>b)swap(a,b);
	print((a-1)*b-a);
	return 0;
}

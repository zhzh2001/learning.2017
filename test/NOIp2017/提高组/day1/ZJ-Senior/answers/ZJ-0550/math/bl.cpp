#include<cstdio>
#include<cctype>
typedef long long LL;
template <typename T>inline void swap(T&x,T&y){const T z=x;x=y;y=z;}
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
int a,b,ans;
int dp[1000001];
int main()
{
	freopen("math.in","r",stdin);
	freopen("bl.out","w",stdout);
	scanf("%d%d",&a,&b);
	if(a>b){int c=a;a=b;b=c;}
	dp[0]=1;
	for(register int i=0;i<=1000000;i++)
		if(dp[i])
		{
			if(i+a<=1000000)
				dp[i+a]=1;
			if(i+b<=1000000)
				dp[i+b]=1;
		}
		else ans=i;
	print(ans);
	return 0;
}

#include<cstdio>
#include<cctype>
typedef long long LL;
template <typename T>inline void swap(T&x,T&y){const T z=x;x=y;y=z;}
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
#include<cstdlib>
#include<time.h>
int gcd(const int a,const int b)
{
	if(a%b==0)return b;
	return gcd(b,a%b);
}
int main()
{
	freopen("math.in","w",stdout);
	srand(time(NULL));
	int aa=rand()%200+3,bb=rand()%200+3;
	while(gcd(aa,bb)!=1)aa=rand()%200,bb=rand()%200;
	print(aa),putchar(' '),print(bb);
	return 0;
}

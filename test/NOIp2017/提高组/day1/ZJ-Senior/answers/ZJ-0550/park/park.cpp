#include<cstdio>
#include<cctype>
#include<cstring>
typedef long long LL;
template <typename T>inline void swap(T&x,T&y){const T z=x;x=y;y=z;}
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
const int maxn=100009,maxm=200009;
int T,N,M,K,P,maxx;
inline void addinto(int&x,const int y)
{
	x+=y;
	if(x>=P)x-=P;
}
int head[maxn],nxt[maxm],tow[maxm],vau[maxm],tmp;
inline void addb(const int u,const int v,const int w)
{
	tmp++;
	nxt[tmp]=head[u];
	head[u]=tmp;
	tow[tmp]=v;
	vau[tmp]=w;
}
int Head[maxn],Nxt[maxm],Tow[maxm],Vau[maxm],Tmp;
inline void Addb(const int u,const int v,const int w)
{
	Tmp++;
	Nxt[Tmp]=Head[u];
	Head[u]=Tmp;
	Tow[Tmp]=v;
	Vau[Tmp]=w;
}
int head_zero[maxn],nxt_zero[maxm],tow_zero[maxm],tmp_zero;
int insi[maxn];
inline void addb_zero(const int u,const int v)
{
	tmp_zero++;
	nxt_zero[tmp_zero]=head_zero[u];
	head_zero[u]=tmp_zero;
	tow_zero[tmp_zero]=v;
	insi[v]++;
}
int Q[maxn],l,r;
int Hash[maxn];
int Dis[maxn];
inline void Spfa()
{
	memset(Dis,0x7f,sizeof(Dis));
	Dis[N]=l=r=0;
	Q[++r]=N,Hash[N]=1;
	while(l!=r)
	{
		l++;
		if(l==maxn)l=1;
		const int u=Q[l];
		Hash[u]=0;
		for(register int i=Head[u];~i;i=Nxt[i])
		{
			const int v=Tow[i];
			if(Dis[u]+Vau[i]<Dis[v])
			{
				Dis[v]=Dis[u]+Vau[i];
				if(!Hash[v])
				{
					r++;
					if(r==maxn)r=1;
					Q[r]=v,Hash[v]=1;
				}
			}
		}
	}
}
int INF_sign;
int dis[maxn];
inline void spfa()
{
	memset(dis,0x7f,sizeof(dis));
	dis[1]=l=r=0;
	Q[++r]=1,Hash[1]=1;
	while(l!=r)
	{
		l++;
		if(l==maxn)l=1;
		const int u=Q[l];
		Hash[u]=0;
		for(register int i=head[u];~i;i=nxt[i])
		{
			const int v=tow[i];
			if(dis[u]+vau[i]<dis[v])
			{
				dis[v]=dis[u]+vau[i];
				if(!Hash[v])
				{
					r++;
					if(r==maxn)r=1;
					Q[r]=v,Hash[v]=1;
				}
			}
		}
	}
}
inline void tople()
{
	l=r=0;
	for(register int i=1;i<=N;i++)
		if(!insi[i])
			Q[++r]=i;
	while(l<r)
	{
		const int u=Q[++l];
		for(register int i=head_zero[u];~i;i=nxt_zero[i])
		{
			const int v=tow_zero[i];
			insi[v]--;
			if(!insi[v])Q[++r]=v;
		}
	}
	for(register int i=1;i<=N;i++)
		if(insi[i]&&dis[i]+Dis[i]<=maxx)
			INF_sign=1;
}
int dp[maxn][51];
LL tt;
inline int Calc()
{
	memset(dp,0,sizeof(dp));
	l=r=0;
	dp[1][K]=1;
	Q[++r]=1,Hash[1]=1;
	int ans=0;
	while(l!=r)
	{
		l++;
		if(l==maxn)l=1;
		const int u=Q[l];
		Hash[u]=0;
		for(register int i=head[u];~i;i=nxt[i])
		{
			const int v=tow[i];
			const int sm=Dis[v]+vau[i]-Dis[u];
			bool fla=0;
			for(register int j=K;j>=sm;j--)
				if(dp[u][j])
					addinto(dp[v][j-sm],dp[u][j]),fla=1;
			if(fla&&!Hash[v])
			{
				r++;
				if(r==maxn)r=1;
				Q[r]=v,Hash[v]=1;
			}
		}
		if(u==N)
		{
			for(register int i=0;i<=K;i++)
				addinto(ans,dp[N][i]);
		}
		memset(dp[u],0,sizeof(dp[u]));
	}
	return ans%P;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--)
	{
		INF_sign=0;
		tmp=0,memset(head,-1,sizeof(head));
		tmp_zero=0,memset(head_zero,-1,sizeof(head_zero));
		Tmp=0,memset(Head,-1,sizeof(Head));
		memset(insi,0,sizeof(insi));
		read(N),read(M),read(K),read(P);
		for(register int i=1;i<=M;i++)
		{
			int u,v,w;read(u),read(v),read(w);
			addb(u,v,w);
			Addb(v,u,w);
			if(w==0)addb_zero(u,v);
		}
		Spfa();
		maxx=Dis[1]+K;
		spfa();
		tople();
		if(INF_sign)
		{
			puts("-1");
			continue;
		}
		print(Calc()),putchar('\n');
	}
	return 0;
}

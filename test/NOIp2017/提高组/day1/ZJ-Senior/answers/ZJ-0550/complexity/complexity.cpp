#include<cstdio>
#include<cstring>
#include<cctype>
template <typename T>inline T min(const T x,const T y){return x<y?x:y;}
template <typename T>inline T max(const T x,const T y){return x>y?x:y;}
int T,L,len;
char tim[107];
inline int gettim()
{
	int res=0;
	for(register int i=0;i<len;i++)
		if(isdigit(tim[i]))
			res=res*10+tim[i]-'0';
	return res;
}
int should_be;
bool error_flag;
int Hashs[107];
char bl[107];
int step[107],top;
int ans[107];
char fro[107],tow[107];
inline int getfro()
{
	int lenfro=strlen(fro);
	if(lenfro==1&&fro[0]=='n')return -1;
	int res=0;
	for(register int i=0;i<lenfro;i++)
		if(isdigit(fro[i]))
			res=res*10+fro[i]-'0';
	return res;
}
inline int gettow()
{
	int lentow=strlen(tow);
	if(lentow==1&&tow[0]=='n')return -1;
	int res=0;
	for(register int i=0;i<lentow;i++)
		if(isdigit(tow[i]))
			res=res*10+tow[i]-'0';
	return res;
}
char emp[101];
inline void edline()
{
	gets(emp);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		memset(bl,0,sizeof(bl));
		memset(step,0,sizeof(step));
		memset(ans,0,sizeof(ans));
		memset(Hashs,0,sizeof(Hashs));
		error_flag=0,top=0;
		scanf("%d %s",&L,tim);
		edline();
		len=strlen(tim);
		if(len==4)should_be=0;
		else should_be=gettim();
		for(register int Lin=1;Lin<=L;Lin++)
		{
			char opt;scanf("%c",&opt);
			if(opt=='F')
			{
				top++;scanf(" %c %s %s",&bl[top],fro,tow);
				if(Hashs[bl[top]-'a'])error_flag=1;
				else Hashs[bl[top]-'a']=1;
				int vfro=getfro(),vtow=gettow();
				if(vfro==-1)
				{
					if(vtow==-1)step[top]=0;
					else step[top]=-1;
				}
				else
				{
					if(vtow==-1)step[top]=1;
					else if(vfro<=vtow)step[top]=0;
					else step[top]=-1;
				}
			}
			else
			{
				if(top==0)error_flag=1;
				else
				{
					Hashs[bl[top]-'a']=0;
					top--;
					if(step[top]!=-1)ans[top]=max(ans[top],ans[top+1]+step[top+1]);
					step[top+1]=ans[top+1]=0;
				}
			}
			edline();
		}
		if(top)error_flag=1;
		if(error_flag)puts("ERR");
		else if(ans[0]==should_be)puts("Yes");
		else puts("No");
	}
	return 0;
}

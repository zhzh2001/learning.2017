const maxn=105;
var t,i,pos,j,fuz1,fuz2,l,k,top,len,ans,b,e,posk:longint;
    f:array['a'..'z']of boolean;
    s,s1:string;
    stack:array[0..maxn]of char;
    cos1,cos2,err,cant:boolean;
begin
    assign(input,'complexity.in');assign(output,'complexity.out');
    reset(input);rewrite(output);
    readln(t);
    for i:=1 to t do
    begin
        fillchar(f,sizeof(f),false);k:=0; top:=0;
        read(l);
        readln(s);fuz1:=0;
        pos:=1;
        while s[pos]<>'(' do inc(pos);
        inc(pos);
        if s[pos]='1' then
            cos1:=true
        else
        begin
            cos1:=false; fuz1:=0;
            while s[pos]<>'^' do inc(pos);
            inc(pos);
            while s[pos]<>')' do
            begin
                fuz1:=fuz1*10+ord(s[pos])-ord('0');
                inc(pos);
            end;
        end;
        err:=true;cos2:=true;fuz2:=0;ans:=0;cant:=false;
        for j:=1 to l do
        begin
            readln(s1);len:=length(s1);
            if not err then continue;
            if s1[1]='E' then
            begin
                if k<=0 then
                begin
                    err:=false;
                    continue;
                end;
                f[stack[k]]:=false;
                dec(k);
                if k=0 then
                begin
                    if ans<fuz2 then
                        ans:=fuz2;
                    fuz2:=0;
                end;
            end
            else
            begin
                if f[s1[3]] then
                begin
                    err:=false;
                    continue;
                end
                else
                begin
                    inc(k);
                    stack[k]:=s1[3];
                    f[s1[3]]:=true;
                end;
                pos:=5;
                if cant and(k=posk) then
                begin
                    posk:=-1;
                    cant:=false;
                end;
                if cant then continue;
                if (s1[pos]='n')and(s1[len]='n') then
                    continue;
                if s1[len]='n' then
                begin
                    inc(fuz2);
                    continue;
                end;
                if (s1[pos]='n') and(s1[len]<>'n') then
                begin
                    cant:=true;
                    posk:=k;
                    continue;
                end;
                b:=0;e:=0;
                while s1[pos]<>' ' do
                begin
                    b:=b*10+ord(s1[pos])-ord('0');
                    inc(pos);
                end;
                inc(pos);
                while pos<=len do
                begin
                    e:=e*10+ord(s1[pos])-ord('0');
                    inc(pos);
                end;
                if b>e then
                begin
                    cant:=true;
                    posk:=k;
                    continue;
                end;
            end;
        end;
        if (not err)or (k>=1) then
            writeln('ERR')
        else
        begin
            if fuz1=ans then
                writeln('Yes')
            else writeln('No');
        end;
    end;
    close(input);close(output);
end.

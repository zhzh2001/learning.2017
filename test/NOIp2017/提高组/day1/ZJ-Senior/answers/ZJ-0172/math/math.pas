var a,b,x,y,t:int64;
procedure ex_gcd(a,b:int64;var x,y:int64);
var t:int64;
begin
    if b=0 then
    begin
        x:=1;y:=0;
        exit;
    end;
    ex_gcd(b,a mod b,x,y);
    t:=y;
    y:=x-y*(a div b);
    x:=t;
end;
begin
    assign(input,'math.in');assign(output,'math.out');
    reset(input);rewrite(output);
    readln(a,b);
    if a>b then
    begin
        t:=a;a:=b;b:=t;
    end;
    ex_gcd(a,b,x,y);
    if x<0 then
    begin
        while x<0 do
            x:=x+b;
        x:=x-b;x:=-x-1;
    end
    else
    begin
        x:=x mod b;
        x:=x-b;x:=-x-1;
    end;
    if y<0 then
    begin
        while y<0 do
            y:=y+a;
        y:=y-a;y:=-y-1;
    end
    else
    begin
        y:=y mod a;
        y:=y-a;y:=-y-1;
    end;
    writeln(x*a+y*b+1);
    close(input);close(output);
end.
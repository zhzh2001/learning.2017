#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;

struct edge {
	int next, to, dist, ban;
};

const int maxn = (int)(1e5) + 5;
const int INF = (int)(1e9) + 100;
int fr[maxn], frT[maxn];
int ds[maxn], dsT[maxn];
int dfn[maxn], low[maxn], ins[maxn], stk[maxn];
edge e[maxn << 2];
int idx, top, sgn;
int n, m, K, mo, tote;

inline void addedge(int u, int v, int w) {
	++tote;
	e[tote].next = fr[u]; fr[u] = tote; e[tote].to = v; e[tote].dist = w; e[tote].ban = 0;
}

inline void addedgeT(int u, int v, int w) {
	++tote;
	e[tote].next = frT[u]; frT[u] = tote; e[tote].to = v; e[tote].dist = w; e[tote].ban = 0;
}

priority_queue<pii, vector<pii>, greater<pii> > q;

void dfs(int x) {
	dfn[x] = low[x] = ++idx; 
	stk[++top] = x; ins[x] = 1;
	for (int i = fr[x]; i; i = e[i].next) if (!e[i].dist) {
		int v = e[i].to;
		if (!dfn[v]) {
			dfs(v); low[x] = min(low[x], low[v]);
		} else if (ins[v]) {
			e[i].ban = 1; low[x] = min(low[x], dfn[v]);
		}
	}
	if (dfn[x] == low[x]) {
		int v = -1, cnt = 0;
		int miS = INF, miT = INF;
		do {
			++cnt;
			v = stk[top--]; 
			ins[v] = 0; 
			miS = min(miS, ds[v]);
			miT = min(miT, dsT[v]);
		} while (v != x); 
		if (cnt > 1 && miS + miT <= ds[n] + K) sgn = 1;
	}
}

int f[maxn][55];
bool vi[maxn][55];

int gn(int x, int v) {
	if (vi[x][v]) return f[x][v];
	vi[x][v] = 1;
	if (x == n) f[x][v] = 1; else f[x][v] = 0;
	for (int i = fr[x]; i; i = e[i].next)
		if (!e[i].ban && v + e[i].dist <= K) {
			f[x][v] += gn(e[i].to, v + e[i].dist); 
			if (f[x][v] >= mo) f[x][v] -= mo;
		}
	return f[x][v];
}

void solve() {
	scanf("%d%d%d%d", &n, &m, &K, &mo);
	memset(ds , 0x3f, sizeof(ds ));
	memset(dsT, 0x3f, sizeof(dsT));
	memset(fr , 0, sizeof(fr ));
	memset(frT, 0, sizeof(frT)); tote = 0;
	for (int i = 1; i <= m; ++i) {
		int u, v, w;
		scanf("%d%d%d", &u, &v, &w);
		addedge(u, v, w); addedgeT(v, u, w);
	}
	ds[1] = 0; q.push(make_pair(ds[1], 1));
	while (!q.empty()) {
		pii u = q.top(); q.pop();
		if (ds[u.second] != u.first) continue;
		int x = u.second;
		for (int i = fr[x]; i; i = e[i].next) if (ds[e[i].to] > ds[x] + e[i].dist) { 
			ds[e[i].to] = ds[x] + e[i].dist; q.push(make_pair(ds[e[i].to], e[i].to));
		}
	} 
	dsT[n] = 0; q.push(make_pair(dsT[n], n));
	while (!q.empty()) {
		pii u = q.top(); q.pop();
		if (dsT[u.second] != u.first) continue;
		int x = u.second;
		for (int i = frT[x]; i; i = e[i].next) if (dsT[e[i].to] > dsT[x] + e[i].dist) { 
			dsT[e[i].to] = dsT[x] + e[i].dist; q.push(make_pair(dsT[e[i].to], e[i].to));
		}
	}
	memset(dfn, 0, sizeof(dfn)); idx = sgn = top = 0; 
	for (int i = 1; i <= n; ++i) if (!dfn[i]) dfs(i);
	if (sgn) {
		printf("-1\n"); return;
	}
	for (int x = 1; x <= n; ++x)
		for (int i = fr[x]; i; i = e[i].next)
			e[i].dist = ds[x] + e[i].dist - ds[e[i].to];
	memset(vi, 0, sizeof(vi)); printf("%d\n", gn(1, 0));
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) solve();
	return 0;
}
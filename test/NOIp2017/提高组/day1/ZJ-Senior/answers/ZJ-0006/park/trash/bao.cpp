#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;

priority_queue<pii, vector<pii>, greater<pii> > q;

struct edge {
	int next, to, dist, ban;
};

const int maxn = (int)(1e5) + 5;
const int INF = (int)(1e9) + 100;
int fr[maxn], ds[maxn];
edge e[maxn << 2];

int tote;

inline void addedge(int u, int v, int w) {
	++tote;
	e[tote].next = fr[u]; fr[u] = tote; e[tote].to = v; e[tote].dist = w; e[tote].ban = 0;
}

int res, K;

void dfs(int x, int v) {
	for (int i = fr[x]; i; i = e[i].next) if (v + e[i].dist <= ds[n] + K) dfs(e[i].to, ds[n] + K);
}

void solve() {
	int n, m, mo;
	scanf("%d%d%d%d", &n, &m, &K, &mo);
	memset(ds, 0x3f, sizeof(ds));
	for (int i = 1; i <= m; ++i) {
		int u, v, w; scanf("%d%d%d", &u, &v, &w); addedge(u, v, w);
	}
	ds[1] = 0; q.push(make_pair(ds[1], 1));
	while (!q.empty()) {
		pii u = q.top(); q.pop();
		if (ds[u.second] != u.first) continue;
		int x = u.second;
		for (int i = fr[x]; i; i = e[i].next) if (ds[e[i].to] > ds[x] + e[i].dist) { 
			ds[e[i].to] = ds[x] + e[i].dist; q.push(make_pair(ds[e[i].to], e[i].to));
		}
	} 
	dfs(1, 0);
}

int main() {
	int T;
	scanf("%d", &T);
	while (T--) solve();
	return 0;
}
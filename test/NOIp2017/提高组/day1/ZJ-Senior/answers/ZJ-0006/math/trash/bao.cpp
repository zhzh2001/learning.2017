#include <bits/stdc++.h>
using namespace std;

int f[2000005];
int main() {
	int a, b; scanf("%d%d", &a, &b);
	f[0] = 1;
	int mx = (int)(2e6);
	for (int i = a; i <= mx; ++i) f[i] |= f[i - a];
	for (int i = b; i <= mx; ++i) f[i] |= f[i - b];
	while (f[mx]) --mx; printf("%d\n", mx);
	return 0;
}

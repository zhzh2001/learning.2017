#include <bits/stdc++.h>
using namespace std;

int main() {
	srand(time(0) * clock());
	int a = rand() % 1000 + 1, b = rand() % 1000 + 1;
	while (__gcd(a, b) > 1) a = rand() % 1000 + 1, b = rand() % 1000 + 1;
	printf("%d %d\n", a, b);
	return 0;
}
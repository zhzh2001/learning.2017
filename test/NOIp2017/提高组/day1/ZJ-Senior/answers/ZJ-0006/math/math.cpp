#include <bits/stdc++.h>
using namespace std;

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	int a, b; scanf("%d%d", &a, &b); 
	printf("%lld\n", max(1LL * (a - 1) * (b - 1) - 1, 0LL));
	return 0;
}
#include <bits/stdc++.h>
using namespace std;

template <class T> inline
void read(T& num) {
	char c;
	num = 0;
	bool start = false, neg = false;
	while ((c = getchar()) != EOF) {
		if (c == '-') start = neg = true;
		else if (c >= '0' && c <= '9') {
			start = true;
			num = num * 10 + c - '0';
		} else if (start) break;
	}
	if (neg) num = -num;
}

int main() {
	int n;
	read(n);
	printf("%d\n", n);
	return 0;
}

import os
import sys

def run(s):
	code = os.system(s) >> 8
	if code:
		print >> sys.stderr, "error occured when executing '%s', exited with code %s" % (s, code)
		sys.exit(1)

dmk = "dmk"
std = "std"
bao = "bao"

def cppCompile(s, t):
	run('g++ %s.cpp -o %s' % (s, t))

def main():
	cppCompile(dmk, 'dmk')
	cppCompile(std, 'std')
	cppCompile(bao, 'bao')
	caseCount = 0
	while True:
		run('./dmk > data.in')
		run('./std < data.in > std.out')
		run('./bao < data.in > bao.out')
		run('diff std.out bao.out -b')
		caseCount += 1
		print >> sys.stderr, 'Accepted on Case %s' % caseCount

main()


#include <bits/stdc++.h>
using namespace std;

char tmp[20], val[20], st[20], ed[20], ci[256];
int vi[256], va[256], ban[256];

bool chk() {
	if (ed[0] == 'n') return 0;
	if (st[0] == 'n') return 1;
	int ww = strlen(st), s = 0, e = 0;
	for (int i = 0; i < ww; ++i) s = s * 10 + st[i] - '0';
	ww = strlen(ed);
	for (int i = 0; i < ww; ++i) e = e * 10 + ed[i] - '0';
	return s > e;
}

void solve() {
	int L, v = 0, now = 0, b = 0;
	memset(vi, 0, sizeof(vi));
	scanf("%d%s", &L, tmp);
	if (tmp[2] != '1') {
		int ww = strlen(tmp);
		for (int i = 4; i + 1 < ww; ++i) v = v * 10 + tmp[i] - '0';
	}
	int mx = 0, cnt = 0, err = 0;
	for (int i = 1; i <= L; ++i) {
		scanf("%s", tmp);
		char c; 
		if (tmp[0] == 'F') {
				scanf("%s%s%s", val, st, ed); 
				c = val[0]; 
				if (!err) {
					if (vi[(int)c]) err = 1;
					else {
						ci[++cnt] = c; va[cnt] = 0; ban[cnt] = 0; vi[(int)c] = 1; 
						if (!b) {
							if (chk()) ban[cnt] = 1, b = 1;
							else if (st[0] != 'n' && ed[0] == 'n') va[cnt] = 1, mx = max(mx, ++now);
						}
					}
				}
		} else if (tmp[0] == 'E') {
			if (!err) {
				if (!cnt) err = 1;
				else {
					now -= va[cnt];
					if (ban[cnt]) b = 0;
					vi[(int)ci[cnt--]] = 0;
				}
			}
		}
	}
	if (cnt) err = 1;
	if (err) printf("ERR\n"); else if (mx == v) printf("Yes\n"); else printf("No\n");
}

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) solve();
	return 0;
}
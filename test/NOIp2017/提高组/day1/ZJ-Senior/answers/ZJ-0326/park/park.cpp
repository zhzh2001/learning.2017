#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#define LL long long
using namespace std;
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(int &nod)
{
	char ch=getchar();
	int p=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') p=-1,ch=getchar();
	while (ch>='0'&&ch<='9') nod=nod*10-48+ch,ch=getchar();
	return nod*p;
}
inline LL read(LL &nod)
{
	char ch=getchar();
	int p=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') p=-1,ch=getchar();
	while (ch>='0'&&ch<='9') nod=nod*10-48+ch,ch=getchar();
	return nod*p;
}
vector<int> a[100005];
int ans,n,m,k,p,l,T,i,t,w,g,d,x[200005],y[200005],z[200005],b[1000005],lon[100005],f[100005];
bool ff,flag[100005];
void dfs(int x,int q)
{
	int i,g,h,k=-1,j;
	if (q+lon[x]>d) return;
	if (x==n) ans=(ans+1)%p;
	g=a[x].size();
	if (f[x]==-1) f[x]=q;
	else
	{
		if (q-f[x]==0) {ff=true;return;}
		k=q-f[x];
	}
	for (i=0;i<g;i++)
	{
		h=a[x][i];
		if (k>-1)
		for (j=1;j<10000;j++)
		{
			if (q+j*z[h]+lon[x]>d) break;
			dfs(y[h],q+j*z[h]);
		}
		if (k==-1)
		{
			dfs(y[h],q+z[h]);
			f[x]=-1;
		}
		if (ff) return;
	}
}
int main()
{
	read(T);
	for (l=1;l<=T;l++)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			a[x[i]].push_back(i);
		}
		for (i=1;i<=n;i++) lon[i]=1000000007,flag[i]=1;
		t=1;w=1;b[t]=1;lon[1]=0;
		while (t<=w)
		{
			g=a[b[t]].size();
			for (i=0;i<g;i++)
			if (flag[y[a[b[t]][i]]]&&lon[b[t]]+z[a[b[t]][i]]<lon[y[a[b[t]][i]]])
			{
				w++;
				b[w]=y[a[b[t]][i]];
				lon[b[w]]=lon[b[t]]+z[a[b[t]][i]];
				flag[b[w]]=false;
			}
			flag[b[t]]=true;
			t++;
		}
		d=lon[n]+k;
		for (i=1;i<=n;i++) a[i].clear();
		for (i=1;i<=m;i++)
			a[y[i]].push_back(i);
		for (i=1;i<=n;i++) lon[i]=1000000007,flag[i]=1;
		t=1;w=1;b[t]=n;lon[n]=0;
		while (t<=w)
		{
			g=a[b[t]].size();
			for (i=0;i<g;i++)
			{
				int ggg=a[b[t]][i];
			if (flag[x[ggg]]&&lon[b[t]]+z[ggg]<lon[x[ggg]])
			{
				w++;
				b[w]=x[ggg];
				lon[b[w]]=lon[b[t]]+z[ggg];
				flag[b[w]]=false;
			}
			}
			flag[b[t]]=true;
			t++;
		}
		for (i=1;i<=n;i++) a[i].clear();
		for (i=1;i<=m;i++)
			a[x[i]].push_back(i);
		ff=false;
		for (i=1;i<=n;i++) f[i]=-1;ans=0;
		dfs(1,0);
		if (ff) puts("-1");
		else printf("%d\n",ans);
	}
}

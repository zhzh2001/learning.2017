#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define LL long long
using namespace std;
int T,l,n,i,j,k,o,wF,wE,qF,qE,x,y,ans,lens1,lens2,lens[105],w,g[105];
char ch,s[105][1000],s1[1000],s2[1000];
bool f,flag;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for (l=1;l<=T;l++)
	{
		scanf("%d",&n);
		ans=0;wF=0;wE=0;w=0;flag=true;o=0;
		ch=getchar();ch=getchar();ch=getchar();ch=getchar();
		if (ch=='1') o=0;
		else
		{
			ch=getchar();ch=getchar();
			while (ch>='0'&&ch<='9') o=o*10+ch-48,ch=getchar();
		}
		for (i=1;i<=n;i++)
		{
			ch=getchar();
			while (ch!='F'&&ch!='E') ch=getchar();
			if (ch=='F')
			{
				wF++;
				ch=getchar();ch=getchar();w++;lens[w]=0;
				while (ch!=' ')
				{
					lens[w]++;s[w][lens[w]]=ch;ch=getchar();
				}
				if (flag)
				for (j=1;j<w;j++)
				{
					f=true;
					if (lens[j]!=lens[w]) continue;
					for (k=1;k<=lens[j];k++) if (s[j][k]!=s[w][k]) {f=false;break;}
					if (f) {flag=false;break;}
				}
				ch=getchar();x=0;
				while (ch!=' ')
				{
					if (ch=='n') x=300;
					else x=x*10+ch-48;
					ch=getchar();
				}
				ch=getchar();y=0;
				if (ch=='n') y=300;
				else while(ch>='0'&&ch<='9') y=y*10+ch-48,ch=getchar();
				if (x>y)
				{
					qF=1;qE=0;
					while (qF>qE)
					{
						if (i+1>n) break;
						ch=getchar();
						while (ch!='F'&&ch!='E') ch=getchar();
						if (ch=='F')
						{
							i++,wF++,qF++;
							ch=getchar();ch=getchar();w++;lens[w]=0;
							while (ch!=' ')
							{
								lens[w]++;s[w][lens[w]]=ch;ch=getchar();
							}
							if (flag)
							for (j=1;j<w;j++)
							{
								f=true;
								if (lens[j]!=lens[w]) continue;
								for (k=1;k<=lens[j];k++) if (s[j][k]!=s[w][k]) {f=false;break;}
								if (f) {flag=false;break;}
							}
						}
						if (ch=='E') i++,wE++,qE++,w--;
					}
				}
				else
				{
					if (y-x<=100) g[w]=0;
					else if (y-x>100) g[w]=1;
				}
			}
			else if (ch=='E')
			{
				wE++;w--;
			}
			k=0;
			for (j=1;j<=w;j++) k=k+g[j];
			ans=max(k,ans);
			if (wF<wE) flag=false;
		}
		if (wF!=wE) flag=false;
		if (flag)
			if (ans==o) puts("Yes");
			else puts("No");
		else puts("ERR");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;a++)
void Rd(int &res){
    char c;res=0;
    while(c=getchar(),c<48);
    do res=(res<<3)+(res<<1)+(c^48);
    while(c=getchar(),c>47);
}
template<class T>inline bool chkmin(T&a,T const&b){return a>b?a=b,1:0;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,1:0;}
int T,L,n;
char S[10];
struct P_100{
    static const int M=105;
    char A[M][10],B[M][10],C[M][10];
    int Son[M][M],Fa[M],op[M],Sz[M];
    bool vis[256];
    int ans;
    void dfs(int u,int dep){
        chkmax(ans,dep);
        FOR(i,0,Sz[u]){
            int j=Son[u][i];
            if(B[j][0]!='n' && C[j][0]!='n'){
                int x,y;
                sscanf(B[j],"%d",&x);
                sscanf(C[j],"%d",&y);
                if(x<=y) dfs(j,dep);
            }
            if(B[j][0]!='n' && C[j][0]=='n') dfs(j,dep+1);
            if(B[j][0]=='n' && C[j][0]=='n') dfs(j,dep);
        }
    }
    void solve(){
        memset(Sz,0,sizeof(Sz));
        memset(Fa,0,sizeof(Fa));
        memset(vis,0,sizeof(vis));
        FOR(i,1,L+1){
            char S[10];
            scanf("%s",S);
            if(S[0]=='E') op[i]=-1;
            else op[i]=1,scanf("%s %s %s",A[i],B[i],C[i]);
        }
        int cnt=0,u=0;
        FOR(i,1,L+1){
            cnt+=op[i];
            if(op[i]<0){
                if(cnt<0){puts("ERR");return;}
                vis[A[u][0]]=0;
                u=Fa[u];
            }
            else{
                if(vis[A[i][0]]){puts("ERR");return;}
                vis[A[i][0]]=1;
                Fa[i]=u;
                Son[u][Sz[u]++]=i;
                u=i;
            }
        }
        if(cnt!=0){puts("ERR");return;}
        ans=0;
        dfs(0,0);
        puts(ans==n?"Yes":"No");
    }
}p_100;

int main(){
    //printf("%lf",(1.0*sizeof(p_100))/1024/1024);
    freopen("complexity.in","r",stdin);
    freopen("complexity.out","w",stdout);
    Rd(T);
    while(T--){
        scanf("%d %s",&L,S);
        if(S[2]=='n') sscanf(S+4,"%d",&n);
        else n=0;
        p_100.solve();
    }
    return 0;
}

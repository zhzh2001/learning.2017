#include<bits/stdc++.h>
using namespace std;

#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;a++)
#define add(a,b) (((a)+=(b))%=Mod)
#define mul(a,b) (1LL*(a)*(b)%Mod)
void Rd(int &res){
    char c;res=0;
    while(c=getchar(),c<48);
    do res=(res<<3)+(res<<1)+(c^48);
    while(c=getchar(),c>47);
}
template<class T>inline bool chkmin(T&a,T const&b){return a>b?a=b,1:0;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,1:0;}
int n,m,k,Mod;

struct P_100{
    static const int M=100005;
    static const int K=55;
    int U[M<<2],V[M<<2],C[M<<2],dis[M];
    int dp[M][K],vis[M];
    bool mark[M][K];
    struct Linklist{
        int Head[M],Next[M<<2],W[M<<2];
        int tot;
        void clear(){memset(Head,tot=0,sizeof(Head));}
        inline void pb(int i,int x){Next[++tot]=Head[i];W[Head[i]=tot]=x;}
        inline int&operator[](int x){return W[x];}
        #define LFOR(a,b,c) for(int a=(b).Head[c];a;a=(b).Next[a])
    }E,W;
    struct Node{
        int u,d;
        friend bool operator<(Node A,Node B){
            return A.d>B.d;
        }
    };
    priority_queue<Node>Q;
    bool dfs(int u){
        vis[u]=-1;
        LFOR(i,E,u){
            if(!~vis[E[i]]) return 0;
            if(!vis[E[i]] && !dfs(E[i])) return 0;
        }
        return vis[u]=1;
    }
    void Dijkstra(int S){
        memset(dis,0x3f,sizeof(dis));
        Q.push((Node){S,dis[S]=0});
        while(!Q.empty()){
            Node p=Q.top();Q.pop();
            int u=p.u,v;
            if(dis[u]^p.d) continue;
            LFOR(i,E,u) if(chkmin(dis[v=E[i]],p.d+W[i]))
                Q.push((Node){v,dis[v]});
        }
    }
    void work(int S){
        memset(dp,0,sizeof(dp));
        memset(mark,0,sizeof(vis));
        dp[S][0]=1;
        Q.push((Node){S,0});
        while(!Q.empty()){
            Node p=Q.top();Q.pop();
            int u=p.u,v;
            if(mark[u][p.d-dis[u]]) continue;
            mark[u][p.d-dis[u]]=1;
            LFOR(i,E,u){
                int v=E[i],x=p.d+W[i];
                if(x>dis[v]+k) continue;
                add(dp[v][x-dis[v]],dp[u][p.d-dis[u]]);
                Q.push((Node){v,x});
            }
        }
    }
    void solve(){
        E.clear();W.clear();
        FOR(i,0,m){
            Rd(U[i]);Rd(V[i]);Rd(C[i]);
            if(!C[i]) E.pb(U[i],V[i]),W.pb(U[i],0);
        }
        memset(vis,0,sizeof(vis));
        FOR(i,1,n+1) if(!dfs(i)){puts("-1");return;}
        FOR(i,0,m) if(C[i]){
            E.pb(U[i],V[i]);
            W.pb(U[i],C[i]);
        }
        //FOR(u,1,n+1) LFOR(i,E,u) printf("%d %d %d\n",u,E[i],W[i]);
        Dijkstra(1);
        work(1);
        int ans=0;
        FOR(i,0,k+1) add(ans,dp[n][i]);
        printf("%d\n",ans);
    }
}p_100;

int main(){
    //printf("%lf",(1.0*sizeof(p_100))/1024/1024);
    freopen("park.in","r",stdin);
    freopen("park.out","w",stdout);
    int T;Rd(T);
    while(T--){
        Rd(n);Rd(m);Rd(k);Rd(Mod);
        p_100.solve(); 
    }
    return 0;
}

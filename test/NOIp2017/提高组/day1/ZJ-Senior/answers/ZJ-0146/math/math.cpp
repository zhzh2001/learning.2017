#include<bits/stdc++.h>
using namespace std;

#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;a++)
#define ll long long
void Rd(int &res){
    char c;res=0;
    while(c=getchar(),c<48);
    do res=(res<<3)+(res<<1)+(c^48);
    while(c=getchar(),c>47);
}
template<class T>inline bool chkmin(T&a,T const&b){return a>b?a=b,1:0;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,1:0;}
int a,b;

struct P_60{
    static const int M=200000005;
    bool dp[M];
    void solve(){
        dp[0]=1;
        if(a>b) swap(a,b);
        int cnt=0;
        FOR(i,0,M-b) if(dp[i]){
            cnt++;
            if(cnt==a){printf("%d\n",i-a);return;}
            dp[i+a]=1;dp[i+b]=1;
        }else cnt=0;
    }
}p_60;
int main(){
    //printf("%lf",(1.0*sizeof(p_60))/1024/1024);
    freopen("math.in","r",stdin);
    freopen("math.out","w",stdout);
    scanf("%d %d",&a,&b);
    p_60.solve();
    return 0;
}

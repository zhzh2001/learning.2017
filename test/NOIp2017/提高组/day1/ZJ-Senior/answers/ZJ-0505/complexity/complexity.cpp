#include <bits/stdc++.h>
using namespace std;
#ifdef ASSERT_ON
#	define myass(x) assert(x)
#else
#	define myass(x)
#endif
int getNum(const string& s)
{
	stringstream ss(s);
	int t;
	ss>>t;
	myass(!ss.bad());
	return t;
}
int getNum2()
{
	string s;
	cin>>s;
	if (s[0]=='n') return -1;
	return getNum(s);
}
int getFZD(const string& o)
{
	if (o[2]=='1') return 0;
	myass(o[2]=='n');
	return getNum(o.substr(4,o.length()-5));
}
char my_getchar()
{
	char t;
	do t=getchar(); while(t==' ' || t=='\n' || t=='\t' || t=='\r');
	return t;
}
struct Data
{
	int now;
	int Max;
	char var;
	Data(int now,int Max,char var):now(now),Max(Max),var(var){}
};
stack<Data> s;
bool var_list[512];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	cin>>T;
	for(int ii=1; ii<=T; ++ii)
	{
		memset(var_list,true,sizeof(var_list));
		while(!s.empty()) s.pop();
		int l;
		string o;
		cin>>l>>o;
		int the_ans=getFZD(o);
		s.push(Data(0,0,'A'));	
		bool error_flag=false;
		for(int i=1; i<=l; ++i)
		{
			char ch=my_getchar();
			myass(ch=='E' || ch=='F');
			if (ch=='E')
			{
				if (error_flag) continue;
				if (s.empty()) 
				{
					error_flag=true;
					continue;
				}
				Data now=s.top(); s.pop();
				var_list[now.var]=true;
				if (now.now==-1) continue;
				int nowFZD=now.now+now.Max;
				if (s.empty()) 
				{
					error_flag=true;
					continue;
				}
				Data last=s.top(); s.pop();
				last.Max=max(last.Max,nowFZD);
				s.push(last);
			}
			else
			{
				char var=my_getchar();
				myass(var>='a' && var<='z' && var!='n');
				if (!var_list[var]) 
				{
					error_flag=true;
				}
				var_list[var]=false;
				int a=getNum2(),b=getNum2();
				if (error_flag) continue;
				int now=0;
				if (a==-1)
				{
					if (b==-1) now=0;
					else now=-1;
				}
				else
				{
					if (b==-1) now=1;
					else
					{
						if (a>b) now=-1;
						else now=0;
					}
				}
				s.push(Data(now,0,var));
			}
		}
		if (s.empty()) error_flag=true;
		else
		{
			Data fina=s.top(); s.pop();
			if (fina.var!='A') error_flag=true;
			else
			{
				if (fina.Max==the_ans) cout<<"Yes\n";
				else cout<<"No\n";
			}
		}
		if(error_flag) 
		{
			cout<<"ERR\n";
		}
	}
	return 0;
}

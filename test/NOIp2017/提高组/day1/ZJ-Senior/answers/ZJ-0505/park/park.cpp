#include <bits/stdc++.h>
using namespace std;
#define MOD(x) ((x)%p)
int n,m,k,p;
typedef long long LL;
#define MAXN 100000
#define MAXM 200000
struct Edge
{
	int to,next,dis;
}e[2*MAXM+10];
int head[MAXN+10];
int cc=0;
void addEdge(int from,int to,int dis)
{
	++cc;
	e[cc].to=to;
	e[cc].next=head[from];
	e[cc].dis=dis;
	head[from]=cc;
}
struct Data1
{
	LL s;
	bool b;
	Data1(LL s=0,bool b=true):s(s),b(b){}
};
map<int,Data1> dis[MAXN+10];
struct Data2
{
	LL d;
	int p;
	Data2(LL d,int p):d(d),p(p){}
};
bool operator<(const Data2& a,const Data2& b)
{
	return a.d>b.d;
}
priority_queue<Data2> q;
//priority_queue<int> vis[MAXN+10];
LL mindis[MAXN+10];
void dijkstra()
{
	while(!q.empty()) q.pop();
	//for(int i=1; i<=n; ++i) while(!vis[i].empty()) vis[i].pop();
	for(int i=1; i<=n; ++i) dis[i].clear();
	memset(mindis,0x3f,sizeof(mindis));
	q.push(Data2(0,1));
	dis[1].insert(make_pair(0,Data1(1,true)));
	//vis[1].push(0);
	mindis[1]=0;
	while(!q.empty())
	{
		Data2 now=q.top(); q.pop();
		//assert(dis[now.p].find(now.d)!=dis[now.p].end());
		Data1 &dd=dis[now.p][now.d];
		if (!dd.b || now.d>mindis[now.p]+k) continue;
		dd.b=false;
		for(int i=head[now.p]; i!=0; i=e[i].next)
		{
			int nowdis=e[i].dis+now.d;
			map<int,Data1>::iterator it=dis[e[i].to].find(nowdis);
			if (it==dis[e[i].to].end())
			{
				if (nowdis<=mindis[e[i].to]+k) 
				{
					dis[e[i].to][nowdis]=Data1(dd.s,true);
					q.push(Data2(nowdis,e[i].to));
				}
				if (nowdis<mindis[e[i].to]) mindis[e[i].to]=nowdis;
			}
			else
			{
				it->second.s=MOD(it->second.s+dd.s);
			}
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	for(int ii=1; ii<=T; ++ii)
	{
		cc=0;
		memset(head,0,sizeof(head));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1; i<=m; ++i)
		{
			int from,to,dis;
			scanf("%d%d%d",&from,&to,&dis);
			addEdge(from,to,dis);
		}
		dijkstra();
		LL ans=0;
		int t=n;
		for(map<int,Data1>::iterator it=dis[t].begin(); it!=dis[t].end(); ++it)
		{
			if (it->first<=mindis[t]+k) ans=MOD(ans+it->second.s);
		}
		printf("%lld\n",ans);
	}
	return 0;
}

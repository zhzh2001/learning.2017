#include<iostream>
#include<algorithm>
#include<fstream>
#include<vector>
#include<cstring>
#include<queue>
using namespace std;
int map[2004][2004];
int t;
int vis[1003];
int n,m,k,p;
vector <int> q;
int tot=0;
void dfs(int i,int w){
	vis[i]=1;
	if(i==n){
		tot++;
		q.push_back(w); 
		return ;
	}
	for(int j=1;j<=n;j++){
		if(map[i][j]!=-1 && vis[j]==0){
			dfs(j,w+map[i][j]);
			vis[j]=0;
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>t;
	int a,b,c;
	for(int j=1;j<=t;j++){
		for(int i=1;i<=n;i++){
			for(int o=1;o<=n;o++)map[i][o]=-1;
		}
		cin>>n>>m>>k>>p;
		for(int i=1;i<=m;i++){
			cin>>a>>b>>c;
			map[a][b]=c;
			map[b][a]=c;
		}
		dfs(1,0);
		cout<<"ok"<<tot<<endl;
		int min=0x7f;
		for(int i=0;i<tot;i++){
			if(q[i]<min)min=q[i];
		}
		int sum=0;
		for(int i=0;i<tot;i++){
			if(min==q[i])sum++;
		}
		cout<<sum%p;
	}
	return 0;
}

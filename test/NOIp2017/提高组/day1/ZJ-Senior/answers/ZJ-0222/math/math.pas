program math;
var
  a,b,ans:int64;

procedure init;
var t:int64;
begin
  readln(a,b);
  if a>b then begin t:=a;a:=b;b:=t;end;
end;

procedure gcd(a,b:int64;var d,x,y:int64);
begin
  if a=0 then begin y:=1;x:=0;d:=b;exit;end;
  gcd(b mod a,a,d,y,x);x:=x-y*b div a;
end;

procedure doit;
var x,y,d:int64;
begin
  x:=0;
  y:=0;
  d:=0;
  gcd(a,b,d,x,y);
  if x<0 then x:=-x else y:=-y;
  //writeln(x,' ',y);
  ans:=x*y*a*(a-1)-1;
end;

procedure outit;
begin
  writeln(ans);
end;

begin
  assign(input,'math.in');reset(input);
  assign(output,'math.out');rewrite(output);
  init;
  doit;
  outit;
  close(input);close(output);
end.
program park;
var
  e:array[1..200000] of record x,y,w:longint;end;
  d:array[1..100005] of longint;
  b:array[1..100005] of boolean;
  t,n,m,p,k:longint;
  ans:int64;

procedure init;
begin
  readln(t);
end;

function check:boolean;
var u:longint;
begin
  for u:=1 to m do
    if (d[e[u].y]=d[e[u].x]+e[u].w) and (e[u].w=0) then exit(true);
  exit(false);
end;

procedure dfs(x,s:longint);
var i:longint;
begin
  if (x=n) and (s=0) then begin ans:=-1;exit;end;
  if x=n then if s<=d[n]+k then begin ans:=(ans+1) mod p;exit;end;
  if ans>-1 then if (s>d[n]+k) or (s=d[n]+k) and (x<>n) then exit;
  for i:=1 to m do if e[i].x=x then if not b[e[i].y] then
  begin
    b[e[i].x]:=true;
    dfs(e[i].y,s+e[i].w);
    if ans=-1 then exit;
    b[e[i].x]:=false;
  end;
end;

procedure doit;
var
    i,j,u,sum:longint;
begin
  for i:=1 to t do
  begin
    ans:=0;
    readln(n,m,k,p);
    sum:=0;
    for j:=1 to m do begin readln(e[j].x,e[j].y,e[j].w);if e[j].w=0 then inc(sum);end;
    fillchar(d,sizeof(d),2);
    d[1]:=0;
    if n*m<=100000000 then
    begin
    for j:=1 to n-1 do
    begin
      for u:=1 to m do if d[e[u].y]>d[e[u].x]+e[u].w then d[e[u].y]:=d[e[u].x]+e[u].w;
    end;
    dfs(1,0);
    if ans>0 then writeln(ans) else writeln(-1);
    end else if k=0 then writeln(1) else if sum>=2 then writeln(-1) else writeln(m div 2);
    //writeln(d[n]);
  end;
end;

begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  init;
  doit;
  close(input);close(output);
end.

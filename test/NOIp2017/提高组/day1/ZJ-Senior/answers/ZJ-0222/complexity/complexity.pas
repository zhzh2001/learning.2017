program complexity;
var
  n,p:longint;
  f:array[1..105] of record nam,d:char;end;

procedure init;
begin
  readln(n);
end;

procedure pop;
begin
  f[p].nam:='-';
  dec(p);
end;

function judge(x:char):boolean;
var i:longint;
begin
  for i:=1 to p do if f[i].nam=x then exit(true);
  exit(false);
end;

procedure doit;
var i,j,k,ans,max,t:longint;
    s,ss,a,b:string;
    x:char;
begin
  for i:=1 to n do
  begin
    p:=0;
    ans:=0;
    max:=0;
    fillchar(f,sizeof(f),'-');
    readln(s);
    k:=ord(s[1])-48;
    delete(s,1,2);
    for j:=1 to k do
    begin
      readln(ss);
      if ss[1]='E' then begin if p>0 then pop else begin inc(p);break;end;end else
      begin
        inc(p);
        if p>max then max:=p;
        if judge(ss[3]) then begin break;end;
        f[p].nam:=ss[3];
        t:=5;
        a:='';b:='';
        while (ss[t]<>' ') do begin if ss[t]='n' then a:='n' else a:=a+ss[t];inc(t);end;
        inc(t);
        while (t<=length(ss)) do begin if ss[t]='n' then b:='n' else b:=b+ss[t];inc(t);end;
        //a:=ss[5];b:=ss[length(ss)];
        if (a<>'n') and (b='n') or (f[p].d='n') then f[p].d:='n'
          else if (a<>'n') and (b<>'n') and (a<=b) or (f[p].d='1') then f[p].d:='1'
            else f[p].d:='0';
      end;
    end;
    if p>0 then begin writeln('ERR');continue;end;
      for k:=1 to max do if f[k].d='n' then inc(ans) else if f[k].d='0' then break;
      if (s[3]='1') and (ans=0) or (ans=ord(s[5])-48) then writeln('Yes') else writeln('No');
  end;
end;

begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  init;
  doit;
  close(input);close(output);
end.
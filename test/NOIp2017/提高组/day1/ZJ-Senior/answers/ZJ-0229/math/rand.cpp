#include<cstdio>
#include<algorithm>
#include<cstring>
#include<ctime>
int gcd(int x,int y){
	if (!y)return x;else return gcd(y,x%y);
}
int main(){
	freopen("math.in","w",stdout);
	srand(time(NULL));
	int x=rand()%1000+2,y=rand()%1000+2;
	while (gcd(x,y)!=1)x=rand()%1000+2,y=rand()%1000+2;
	printf("%d %d\n",x,y);
}

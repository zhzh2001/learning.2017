#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long ll;
ll n,m;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n<m)swap(n,m);
	ll x=n*(m-1)%m,y=n*(m-1)/m;
	printf("%lld\n",(y-1)*m+x);
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int M=1010;
int cas,n,tmp,i,h[M][2],vis[M];
char s[M];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&cas);
	while (cas--){
		scanf("%d%s",&n,s);
		if (s[2]=='1')tmp=0;
			else{
				int m=strlen(s);
				tmp=0;
				for (i=4;i<m-1;i++)tmp=tmp*10+s[i]-'0';
			}
		memset(vis,0,sizeof vis);
		int top=0,ans=0,Ans=0,now=0,jump=0;
		for (i=1;i<=n;i++){
			scanf("%s",s);
			if (s[0]=='F'){
				scanf("%s",s);
				h[++top][1]=s[0]-'a';
				if (vis[h[top][1]])Ans=1;else vis[h[top][1]]=top;
				int x,y,X=0,Y=0,j=0;
				scanf("%s",s);
				if (s[j]>='0'&&s[j]<='9'){
					x=0;
					while (s[j]>='0'&&s[j]<='9')X=X*10+s[j]-'0',j++;
				}else if (s[j]=='n')x=2;
					else x=h[vis[s[j]-'a']][0];
				j=0;scanf("%s",s);
				if (s[j]>='0'&&s[j]<='9'){
					y=0;
					while (s[j]>='0'&&s[j]<='9')Y=Y*10+s[j]-'0',j++;
				}else if (s[j]=='n')y=2;
					else y=h[vis[s[j]-'a']][0];
				if (jump)continue;
				if (!x&&!y){
					if (X<=Y)h[top][0]=0;
						else h[top][0]=-1,jump++;
					}else if (!y)h[top][0]=-1,jump++;
						else if (x==2&&y==2)h[top][0]=2;
							else if (x==2)h[top][0]=-1,jump++;
								else h[top][0]=1,now++;
				ans=max(ans,now);
			}else{
				if (!top)Ans=1;
					else{
						if (!jump&&h[top][0]==1)now--;
							else if (h[top][0]==-1)jump--;
						vis[h[top][1]]=0;
						top--;
					}
			}
		}
		if (Ans||top)puts("ERR");
			else if (ans==tmp)puts("Yes");
				else puts("No");
	}
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#define MP make_pair
using namespace std;
typedef pair<int,int> PII;
priority_queue<PII,vector<PII>,greater<PII> >q;
const int M=200010;
const int inf=1000000007;
int cas,edgenum,Edgenum,n,m,lim,mod,i,j,x,y,z,t,top,flag;
int head[M],vet[M],next[M],len[M],dis[M],Head[M],Vet[M],Next[M],Len[M],Dis[M],h[M],vis[M],low[M],dfn[M],s[M],inpoint[M],dp[M][51];
void addedge(int x,int y,int z){
	vet[++edgenum]=y;
	next[edgenum]=head[x];
	head[x]=edgenum;
	len[edgenum]=z;
}
void Addedge(int x,int y,int z){
	Vet[++Edgenum]=y;
	Next[Edgenum]=Head[x];
	Head[x]=Edgenum;
	Len[Edgenum]=z;
}
void dfs(int u){
	vis[u]=1;h[++top]=u;
	dfn[u]=low[u]=++t;
	for (int e=Head[u];e;e=Next[e]){
		int v=Vet[e];
		if (!vis[v]){
			dfs(v);
			low[u]=min(low[u],low[v]);
		}else if (!s[v])low[u]=min(low[u],low[v]);
	}
	if (dfn[u]==low[u]){
		int Flag=0,cnt=0;
		while (h[top]!=u){
			if (dis[h[top]]!=dis[u])Flag=1;
			s[h[top]]=1;
			top--;
			cnt++;
		}
		s[h[top]]=1;
		top--;
//		printf("u=%d dis=%d\n",u,Dis[u]+dis[u]-dis[n]);
		if (!Flag&&cnt&&Dis[u]+dis[u]-dis[n]<=lim)flag=1;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout); 
	scanf("%d",&cas);
	while (cas--){
		edgenum=Edgenum=0;
		memset(head,0,sizeof head);
		memset(Head,0,sizeof Head);
		scanf("%d%d%d%d",&n,&m,&lim,&mod);
		for (i=1;i<=m;i++){
			scanf("%d%d%d",&x,&y,&z);
			addedge(x,y,z);
			Addedge(y,x,z);
		}
		memset(vis,0,sizeof vis);
		q.push(MP(0,1));
		for (i=2;i<=n;i++)dis[i]=inf;dis[1]=0;
		while (!q.empty()){
			int u=q.top().second;q.pop();
			if (vis[u])continue;else vis[u]=1;
			for (int e=head[u];e;e=next[e]){
				int v=vet[e];
				if (dis[u]+len[e]<dis[v]){
					dis[v]=dis[u]+len[e];
					q.push(MP(dis[v],v));
				}
			}
		}
		memset(vis,0,sizeof vis);
		q.push(MP(0,n));
		for (i=1;i<n;i++)Dis[i]=inf;Dis[n]=0;
		while (!q.empty()){
			int u=q.top().second;q.pop();
			if (vis[u])continue;else vis[u]=1;
			for (int e=Head[u];e;e=Next[e]){
				int v=Vet[e];
				if (Dis[u]+Len[e]<Dis[v]){
					Dis[v]=Dis[u]+Len[e];
					q.push(MP(Dis[v],v));
				}
			}
		}
		Edgenum=0;
		memset(Head,0,sizeof Head);
		memset(vis,0,sizeof vis);
		int l=1,r=1;h[l]=1;
		while (l<=r){
			int u=h[l];
			for (int e=head[u];e;e=next[e]){
				int v=vet[e];
				if (dis[v]==dis[u]+len[e]){
					if (!vis[v]){
						vis[v]=1;
						h[++r]=v;
					}
					Addedge(u,v,0);
					inpoint[v]++;
				}
			}
			l++;
		}
		memset(vis,0,sizeof vis);
		memset(s,0,sizeof s);
		t=top=flag=0;
		dfs(1);
		if (flag){puts("-1");continue;}
		memset(vis,0,sizeof vis);
		l=1,r=1,h[1]=1;
		while (l<=r){
			int u=h[l];
			for (int e=Head[u];e;e=Next[e]){
				int v=Vet[e];
				inpoint[v]--;
				if (!inpoint[v])h[++r]=v;
			}
			l++;
		}
		memset(dp,0,sizeof dp);
		dp[1][0]=1;
		for (i=0;i<=lim;i++){
			for (j=1;j<=n;j++)if (dp[h[j]][i]){
				int u=h[j];
				for (int e=Head[u];e;e=Next[e]){
					int v=Vet[e];
					dp[v][i]=(dp[v][i]+dp[u][i])%mod;
				}
			}
			for (j=1;j<=n;j++)
				if (dp[j][i])
					for (int e=head[j];e;e=next[e]){
						int v=vet[e];
						if (dis[v]<dis[j]+len[e]&&dis[v]+lim>=dis[j]+len[e]+i)
							dp[v][i+dis[j]+len[e]-dis[v]]=(dp[v][i+dis[j]+len[e]-dis[v]]+dp[j][i])%mod;
					}
		}
		int ans=0;
		for (i=0;i<=lim;i++)ans=(ans+dp[n][i])%mod;
		printf("%d\n",ans);
	}
}

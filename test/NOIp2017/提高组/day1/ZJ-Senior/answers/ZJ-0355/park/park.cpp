#include <cstdio>
#include <cctype>
#include <queue>
#include <cstring>
using namespace std;

const int N=1e5+10;
struct side{
	int to,w,nt;
}s[N<<1],e[N<<1];
int n,m,k,mod,h[N],u[N],num,que[1000000],st,ed,dis[N],mn,ans,t,d[N],cnt;
bool b[N];

inline char nc(void){
	static char ch[100010],*p1=ch,*p2=ch;
	return p1==p2&&(p2=(p1=ch)+fread(ch,1,100010,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &a){
	static char c=nc();int f=1;
	for (;!isdigit(c);c=nc()) if (c=='-') f=-1;
	for (a=0;isdigit(c);a=(a<<3)+(a<<1)+c-'0',c=nc());
	return (void)(a*=f);
}

inline void add(int x,int y,int w){
	s[++num]=(side){y,w,h[x]},h[x]=num;
	e[num]=(side){x,w,u[y]},u[y]=num;
}

inline bool pd(int x,bool bo){
	if (b[x]) return bo;
	b[x]=1;bool ret=1;
	for (int i=h[x]; i; i=s[i].nt)
		if (d[s[i].to]&&s[i].w) ret&=pd(s[i].to,0);
		else ret&=pd(s[i].to,bo);
	b[x]=0;
	return ret;
}

inline bool tuopu(void){
	que[st=1,ed=0]=0;
	for (int i=1; i<=n; ++i) if (!d[i]) que[++ed]=i;
	while (st<=ed){
		int p=que[st++];
		for (int i=h[p]; i; i=s[i].nt) if (!(--d[s[i].to])) que[++ed]=s[i].to;
	}
	memset(b,0,sizeof b);
	for (int i=1; i<=n; ++i) if (!b[i]&&d[i]&&pd(i,1)) return 1;
	return 0;
}

inline void spfa(void){
	memset(dis,0x3f,sizeof dis),memset(b,0,sizeof b),que[st=ed=1]=n,b[n]=1,dis[n]=0;
	while (st<=ed){
		int p=que[st++];b[p]=0;
		for (int i=u[p]; i; i=e[i].nt)
			if (dis[e[i].to]>dis[p]+e[i].w){
				dis[e[i].to]=dis[p]+e[i].w;
				if (!b[e[i].to]) b[e[i].to]=1,que[++ed]=e[i].to;
			}
	}
}

inline void so(int now,int sum){
	if (now==n) ans=(ans+1)%mod;
	for (int i=h[now]; i; i=s[i].nt)
		if (sum+dis[s[i].to]+s[i].w<=mn+k) so(s[i].to,sum+s[i].w);
	return;
}

int main(void){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (read(t); t; --t){
		read(n),read(m),read(k),read(mod);
		memset(h,0,sizeof h),memset(u,0,sizeof u),num=0,memset(d,0,sizeof d);
		for (int i=1,x,y,w; i<=m; ++i) read(x),read(y),read(w),add(x,y,w),++d[y],cnt+=w==0;
		if (cnt>1&&n<=1000) if (tuopu()){puts("-1");continue;}
		spfa(),mn=dis[1],ans=0,so(1,0),printf("%d\n",ans);
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

#include <cstdio>
using namespace std;

typedef long long ll;
ll a,b,ans;
bool bo[10000000];

int main(void){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b),bo[0]=1;
	if (a*b<=1e8) for (ll i=0; i<a*b; ++i) if (!bo[i]) ans=i; else bo[i+a]=bo[i+b]=1;
	else ans=a+b+1;
	printf("%lld\n",ans);
	return 0;
}

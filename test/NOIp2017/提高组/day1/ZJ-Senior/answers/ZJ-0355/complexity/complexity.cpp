#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
using namespace std;

int t,n,top,cnt,xy,ans,num,bm,em,zx;
char st[2],bl[2],sk[100],fzd[200],beg[200],end[200];
bool b[200],mark,mn[200],sz[200],bo[200];

int main(void){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (scanf("%d",&t); t; --t){
		scanf("%d",&n),scanf("%s",fzd),ans=0,mark=0,cnt=0,num=0,top=0;
		if (fzd[2]=='n') for (int i=4; isdigit(fzd[i]); ++i) num=num*10+fzd[i]-'0';
		memset(b,0,sizeof b),memset(sk,'\0',sizeof sk),memset(mn,0,sizeof mn),memset(sz,0,sizeof sz);
		for (int i=1; i<=n; ++i){
			scanf("%s",st);
			if (st[0]=='F'){
				scanf("%s",bl);
				if (b[bl[0]]) mark=1;
				else b[bl[0]]=1,sk[++top]=bl[0];
				scanf("%s%s",beg,end),bm=em=0;
				if (beg[0]!='n') for (int i=0; isdigit(beg[i]); ++i) bm=bm*10+beg[i]-'0'; else bm=2e9;
				if (end[0]!='n') for (int i=0; isdigit(end[i]); ++i) em=em*10+end[i]-'0'; else em=2e9;
				if (bm>em) ++zx,bo[top]=1;
				if (beg[0]=='n'&&end[0]!='n') mn[top]=1,++xy;
				if (beg[0]!='n'&&end[0]=='n') if (!xy&&!zx) ++cnt;
				if (beg[0]!='n'&&end[0]!='n') sz[top]=1;
				if (beg[0]=='n'&&end[0]=='n');
				ans=max(ans,cnt);
			}
			else {
				if (bo[top]) bo[top]=0,--zx;
				if (mn[top]) mn[top]=0,--xy;
				else if (sz[top]) sz[top]=0;
				else if (!xy) --cnt;
				b[sk[top--]]=0;
				if (top<0) mark=1;
			}
		}
		if (n&1||mark||top>0) puts("ERR"); else puts(ans==num?"Yes":"No");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
long long n,m,i,j,k,p,l,t,tot;
inline void read(long long &x)
{
	char ch=getchar(); x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(n); read(m);
	if (n>m) swap(n,m);
	printf("%lld\n",(n-2)*m+m-n);
}

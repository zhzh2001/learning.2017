#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=5005;
int n,i,j,k,p,l,t,val[N];
char stk[N];
bool bo[N];
inline void read(int &x)
{
	char ch=getchar(); x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	read(T);
	while (T--)
	{
		int n=0,lst;
		char ch=getchar();
		while (ch!='O')
		{
			if (ch<'0'||ch>'9') ch=getchar();
			if (ch!='O') n=0;
			while (ch>='0'&&ch<='9') n=n*10+ch-'0',ch=getchar();
		}
		//printf("%d\n",n);
		int flag=0,tre=0;
		ch=getchar();
		while (ch!=')')
		{
			if (ch=='n') flag=1;
			if (ch>='0'&&ch<='9')
			{
				while (ch>='0'&&ch<='9')
				{
					tre=tre*10+ch-'0';
					ch=getchar();
				}
			}
			else ch=getchar();
		}
		tre=flag*tre;
		int now=0,ans=0,last=0,A=0; flag=0;
		memset(bo,0,sizeof(bo));
		for (i=1;i<=n;i++)
		{
			//printf("%d %d\n",i,now);
			ch=getchar();
			while (ch!='F'&&ch!='E') ch=getchar();
			if (now<0) {flag=1; break;}  
			//printf("%c\n",ch);
			if (ch=='F')
			{
				now++; 
				char c=getchar(),c1,c2,x1=0,x2=0;
				while (c<'a'||c>'z') c=getchar();
				if (bo[c]) {/*printf("FUCK\n");*/flag=1; break;}
				bo[c]=1;
				stk[now]=c;
				c1=getchar();
				
				while (c1!='n'&&(c1<'0'||c1>'9')) 
					c1=getchar();
				while (c1>='0'&&c1<='9') x1=x1*10+c1-'0',c1=getchar();
				if (c1=='n') 
				{
					int nw=now;
					//printf("%d %c UCC\n",nw,c1);
					while (nw!=now-1)
					{
						char cc=getchar(),cp;
						while (cc!='E'&&cc!='F') cc=getchar();
						i++;
						if (cc=='F') nw++;
						if (cc=='E') nw--; 
						//printf("%d %d %c UC\n",nw,now,cc);
						cp=cc;
						if (i==n&&nw!=now-1) {flag=1; break;} 
						
						if (cp=='F') {
							cc=getchar();
							while (cc<'a'||cc>'z') cc=getchar();
							//printf("%c THS\n",cc);
							if (bo[cc]==1) { flag=1; break;} 
							else bo[cc]=1,stk[nw]=cc;
						}
						else bo[stk[nw+1]]=0;
					}
					now--;
					
				}
				else 
				{
					c2=getchar();
					while (c2!='n'&&(c2<'0'||c2>'9')) c2=getchar();
					if (c2=='n') A++,val[now]=1; 
					else 
					{
						while (c2>='0'&&c2<='9') x2=x2*10+c2-'0',c2=getchar();
						if (x2<x1)
						{
							int nw=now;
							while (nw!=now-1)
							{
								char cc=getchar(),cp;
								while (cc!='E'&&cc!='F') cc=getchar();
								i++;
								if (cc=='F') nw++;
								if (cc=='E') nw--; 
								cp=cc;
								if (i==n&&nw!=now-1) {flag=1; break;} 
								
								if (cp=='F') {
									cc=getchar();
									while (cc<'a'||cc>'z') cc=getchar();
									if (bo[cc]==1) {flag=1; break;} 
									else bo[cc]=1,stk[nw]=cc;
								}
								else bo[stk[nw+1]]=0;
							}
							now--;
						}
						else val[now]=0;
						
					}
					
				}
				ans=max(ans,A);
			}
			else 
			{
				if (val[now]) A--;
				bo[stk[now]]=0;
				now--;
			}
			if (flag) break;
		}
		if (now!=0) flag=1;
		if (flag==1) {printf("ERR\n"); continue;}
		else if (ans==tre) printf("Yes\n");
		else printf("No\n");
	}
}

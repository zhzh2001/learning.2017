#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=400005;
int n,m,i,j,k,p,l,t,q[N*40],Q[N*40][2],uup[N],d[N],len,flag,first[N],nxt[N],b[N],c[N],fas[100005][55],boo[100005][55],bo[N],ss[100005][55],mo,x[N],y[N],z[N];
inline void read(int &x)
{
	char ch=getchar(); x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}
void add(int x,int y,int z){b[++len]=y; c[len]=z; nxt[len]=first[x]; first[x]=len;}
void spfa()
{
	int i,h=0,t=1;
	memset(bo,0,sizeof(bo));
	memset(d,60,sizeof(d));
	q[1]=1; d[1]=0; bo[1]=1;
	do
	{
		h++;
		bo[q[h]]=0;
		for (i=first[q[h]];i;i=nxt[i])
		if (d[b[i]]>d[q[h]]+c[i])
		{
			d[b[i]]=d[q[h]]+c[i];
			if (!bo[b[i]])
			{
				bo[b[i]]=1;
				q[++t]=b[i];
			}
		}
	}while (!(h>=t));
}
void spfa2()
{
	memset(boo,0,sizeof(boo));
	int i,h=0,t=1;
	Q[1][0]=1; Q[1][1]=0; fas[1][0]=1; boo[1][0]=1;
	do
	{
		h++;
		boo[Q[h][0]][Q[h][1]]=0;
		for (i=first[Q[h][0]];i;i=nxt[i])
		{
			int tt=d[Q[h][0]]+Q[h][1]+c[i]-d[b[i]];
			//printf("%d\n",tt);
			if (d[Q[h][0]]+Q[h][1]+c[i]<=uup[b[i]]) 
			{
				fas[b[i]][tt]+=fas[Q[h][0]][Q[h][1]],fas[b[i]][tt]%=mo;
				if (!boo[b[i]][tt])
				{
					boo[b[i]][tt]=1;
					ss[b[i]][tt]++;
					if (ss[b[i]][tt]==n*(k+1)) {flag=1; return;}
					t++;
					Q[t][0]=b[i];
					Q[t][1]=tt;
				}
			}
		}
	}while (!(h>=t));
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	read(T);
	while (T--)
	{
		flag=0;
		len=0; memset(first,0,sizeof(first));
		memset(fas,0,sizeof(fas));
		memset(ss,0,sizeof(ss));
		read(n); read(m); read(k); read(mo);
		for (i=1;i<=m;i++)
		{
			read(x[i]); read(y[i]); read(z[i]);
			add(x[i],y[i],z[i]);
		}
		spfa();
		for (i=1;i<=n;i++) uup[i]=d[i]+k;
		spfa2();
		int ans=0;
		if (flag==0)
		{
			for (i=0;i<=k;i++) ans+=fas[n][i],ans%=mo;
			printf("%d\n",ans);
		}
		else printf("-1\n");
	}
}

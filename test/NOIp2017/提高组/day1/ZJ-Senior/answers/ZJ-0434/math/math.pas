var
  a,b,c,i,j,k,t,n,m:int64;
begin
  assign(input,'math.in'); reset(input);
  assign(output,'math.out'); rewrite(output);
  readln(n,m);
  writeln(n*m-n-m);
  close(input); close(output);
end.

#include<bits/stdc++.h>
using namespace std;
const int INF=300000000,N=100010,M=400010,KK=51;
int b[M],c[M],ne[M],fi[N],f[N],k,n,m,kk,p,t,ans[KK][N],tans;
bool in[N];
queue<int> q;
vector<int> g[N];
void add(int x,int y,int z){
	b[++k]=y; c[k]=z; ne[k]=fi[x]; fi[x]=k; 
}
void spfa(int s){
	for (int i=1; i<=n; i++) f[i]=INF;
	q.push(s); f[s]=0;
	while (!q.empty()){
		int x=q.front(); q.pop(); in[x]=0;
		for (int j=fi[x]; j; j=ne[j])
		if (f[b[j]]>f[x]+c[j]){
			f[b[j]]=f[x]+c[j];
			if (!in[b[j]]) in[b[j]]=1,q.push(b[j]);
		}
	}
}
void up(int op){
	q.push(1);
	while (!q.empty()){
		int x=q.front(); q.pop();
		for (int j=0; j<(int)g[x].size(); ++j){
			int u=g[x][j];
			(ans[op][u]+=ans[op][x])%=p;
			q.push(u);
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d%d%d%d",&n,&m,&kk,&p);
		k=tans=0; for (int i=1; i<=n; i++) fi[i]=0;		
		for (int i=0; i<=kk; i++) memset(ans[i],0,(n+1)*sizeof(int));
		for (int i=1; i<=m; i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		spfa(1);
		for (int i=1; i<=n; i++){
			g[i].clear();
			for (int j=fi[i]; j; j=ne[j])
			if (f[i]+c[j]==f[b[j]]) g[i].push_back(b[j]);
		}
		ans[0][1]=1%p;
		for (int i=0; i<=kk; i++){
			up(i);
			for (int j=1; j<=n; j++)
			for (int k=fi[j]; k; k=ne[k]){
				int t=f[j]+i+c[k]-f[b[k]];
				if (t>i&&t<=kk) (ans[t][b[k]]+=ans[i][j])%=p;
			}
			(tans+=ans[i][n])%=p;
		}
		printf("%d\n",tans);
	}
}

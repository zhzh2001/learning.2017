#include<bits/stdc++.h>
using namespace std;
int n,ans;
char s[10],s3[10],s1[10],s2[10];
stack<pair<pair<int,int>,char> >st;
map<char,int> mp;
int un(int x,int y){
	if (x==-1) return -1;
	if (y==-1) return x;
	return x+y;
}
int change(string s){
	int x=0;
	for (int i=0; s[i]; ++i) x=x*10+s[i]-48;
	return x;
}
void solve(){
	while (!st.empty()) st.pop(); mp.clear();
	for (int i=1; i<=n; i++){
		scanf("%s",s);
		if (s[0]=='F'){
			scanf("%s%s%s",s3,s1,s2);
			if (mp[s3[0]]) ans=-2333; else mp[s3[0]]=1;
			if (ans==-2333) continue;
			int op;
			if (s1[0]=='n'&&s2[0]=='n') op=0;
			if (s1[0]!='n'&&s2[0]=='n') op=1;
			if (s1[0]=='n'&&s2[0]!='n') op=-1;
			if (s1[0]!='n'&&s2[0]!='n'){
				int x=change(s1),y=change(s2);
				if (x<=y) op=0; else op=-1;
			}
			st.push(make_pair(make_pair(op,0),s3[0]));
		}
		else{
			if (st.size()==0) ans=-2333;
			if (ans==-2333) continue;
			pair<pair<int,int>,char> t=st.top();
			st.pop(); mp[t.second]=0;
			if (st.size()==0) ans=max(ans,un(t.first.first,t.first.second));
			else{
				pair<pair<int,int>,char> tt=st.top();
				st.pop();
				tt.first.second=max(un(t.first.first,t.first.second),tt.first.second);
				st.push(tt);
			}
		}
	}
	if (st.size()>0) ans=-2333;
}
int main(){	
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t; scanf("%d",&t);
	while (t--){
		int kl=0;
		ans=0;
		for (int i=0; i<10; i++) s[i]='\0';
		scanf("%d%s",&n,s);
		for (int j=4; s[j]>='0'&&s[j]<='9'; ++j) kl=kl*10+s[j]-48;
		solve();
		if (ans==-2333) puts("ERR");
		else if (ans==kl) puts("Yes");
		else puts("No");
	}
}

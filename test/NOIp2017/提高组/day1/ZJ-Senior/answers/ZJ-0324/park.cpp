#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#define ll long long
using namespace std;
int cas,n,m,ty,p,tot,ans,x,y,z;
int f[1005],g[100005],ok[1005];
int head[1005],Next[2005],to[2005],len[2005];
void add(int x,int y,int z)
{
	tot++;
	Next[tot]=head[x];
	to[tot]=y;
	len[tot]=z;
	head[x]=tot;
}
void spfa()
{
	for(int i=1;i<=n;i++) head[i]=-1;
	for(int i=1;i<=m;i++) 
	{
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
	}
	int t=0,w=1;
	f[1]=0;ok[1]=1;g[1]=1;
	for(int i=2;i<=n;i++) ok[i]=0,f[i]=1e9;
	while(t<w) 
	{
		t++;
		int x=g[t];
		ok[x]=0;
		for(int i=head[x];i!=-1;i=Next[i]) 
		if(f[x]+len[i]<f[to[i]]) 
		{
			f[to[i]]=f[x]+len[i];
			if(ok[to[i]]==0) 
			{
				w++;
				g[w]=to[i];
				ok[to[i]]=1;
			}
		}
	}
}
void dfs(int k,int x)
{
    if(x>f[n]+ty) return;
    if(k==n) 
    {
    	ans=(ans+1)%p;
    	return;
    }
    for(int i=head[k];i!=-1;i=Next[i]) dfs(to[i],x+len[i]);
}   
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>cas;
	while(cas--) 
	{
	    cin>>n>>m>>ty>>p;
	    spfa();
	    ans=0;
	    dfs(1,0);
	    printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

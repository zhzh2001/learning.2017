#include <stdio.h>
#include <string.h>
#define LoveLive long long
LoveLive tc, n, m, k, p, x, y, z, q[101000], h, t;
LoveLive dp[101000][51]; bool inq[101000];
LoveLive cnt[101000];
LoveLive head[101000], tot, temp6;
LoveLive dis[101000];
struct edge {LoveLive to, nxt, cost; } e[201000];
void addedge(LoveLive a, LoveLive b, LoveLive c) {
	e[++tot].to = b; e[tot].nxt = head[a];
	e[tot].cost = c; head[a] = tot;
}
bool dfs(LoveLive x, LoveLive now) {
	if (cnt[x] > k) return 0; cnt[x]++;
	for (LoveLive i = head[x]; i; i = e[i].nxt)
		if (now + e[i].cost - dis[e[i].to] <= k) {
			(dp[e[i].to][now + e[i].cost - dis[e[i].to]] += dp[x][now - dis[x]]) %= p;
			if (!dfs(e[i].to, now + e[i].cost)) return 0;
		}
	cnt[x]--;
	return 1;
}
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &tc);
	while (tc--) {
		memset(dis, 0x3f, sizeof dis);
		memset(e, 0, sizeof e); memset(head, 0, sizeof head); tot = 0;
		memset(inq, 0, sizeof inq);
		scanf("%lld%lld%lld%lld", &n, &m, &k, &p);
		for (LoveLive i = 1; i <= m; i++) {
			scanf("%lld%lld%lld", &x, &y, &z);
			addedge(x, y, z);
		}
		inq[1] = 1; q[h = 0] = 1; t = 1; dis[1] = 0;
		while (h != t) {
			inq[temp6 = q[h++]] = 0; if (h >= n) h -= n;
			for (LoveLive i = head[temp6]; i; i = e[i].nxt)
				if (dis[e[i].to] > dis[temp6] + e[i].cost) {
					dis[e[i].to] = dis[temp6] + e[i].cost;
					if (!inq[e[i].to]) {
						inq[q[t] = e[i].to] = 1;
						++t; if (t >= n) t -= n;
					}
				}
		}
		memset(cnt, 0, sizeof cnt);
		memset(dp, 0, sizeof dp); 
		dp[1][0] = 1;
		if (dfs(1, 0)) {
			LoveLive ans = 0;
			for (LoveLive i = 0; i <= k; i++) (ans += dp[n][i]) %= p;
			printf("%lld\n", ans);
		} else puts("-1");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include <stdio.h>
#include <string.h>
int tc, r, top, maxcom, w, err, x, y, comm[1100];
char a[1100], b[1100], c[1100], d[1100], com[1100], fnum;
char var[1100]; bool t[1100];
//FILE * debug;
int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
//	debug = fopen("in.in", "w");
	scanf("%d", &tc);
//	fprintf(debug, "%d\n", tc);
	while (tc--) {
		scanf("%d%s", &r, com);
//		fprintf(debug, "%d %s\n", r, com);
		maxcom = fnum = top = err = 0;
		for (int i = 1; i <= r; i++) {
			scanf("%s", a); 
			if (a[0] == 'F') {
				++fnum;
				if (fnum > r >> 1) {err = 1; }
				scanf("%s%s%s", b, c, d);
				if (!err) {
					for (int i = 1; i <= top; i++) if (var[i] == b[0]) {err = 1; break; }
					if (!err) {
						var[++top] = b[0]; x = y = 0;
						t[top] = t[top - 1];
						if (c[0] != 'n') sscanf(c, "%d", &x);
						if (d[0] != 'n') sscanf(d, "%d", &y);
						if (x && !y) comm[top] = comm[top - 1] + 1;
						else if (!x && y) comm[top] = 0, t[top] = 1;
						else if (x && y && x > y) comm[top] = 0, t[top] = 1;
						else comm[top] = comm[top - 1];
						if (!t[top] && comm[top] > maxcom) maxcom = comm[top]; 
					}
				}
//				fprintf(debug, "%s %s %s %s\n", a, b, c, d);
			} else {
				if (!top) {err = 1; }
				else --top;
//				fprintf(debug, "E\n");
			}
		}
		if (err) puts("ERR");
		else {
			if (strcmp(com, "O(1)") == 0) puts(maxcom == 0 ? "Yes" : "No");//, printf("%d\n", maxcom);
			else {
				sscanf(com, "O(n^%d)", &w); puts(maxcom == w ? "Yes" : "No");//, printf("%d\n", maxcom);
			}
		}
	}
	return 0;
}

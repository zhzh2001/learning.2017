#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mo 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void read(ll &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void write(int x)
{
	if (x<0)x=-x;
	if (x>10)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x<0)x=-x;
	if (x>10)write(x/10);
	putchar(x%10+48);
}
int o,s;
char ch[10000];
int a[110],b[110],c[110];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(o);
	for(int q=1;q<=o;q++)
	{
		gets(ch);
		int l=strlen(ch),i=0,x=0,y=0,z=0,n=0,f=0;
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		memset(c,0,sizeof(c));
		c[1]=1;
		while (ch[i]!=' ')x=x*10+ch[i]-48,i++;
		for (int j=i+3;j<l;j++)
		{
			if(ch[j]=='n')z=1;
			if (ch[j]>='0'&&ch[j]<='9')y=y*10+ch[j]-48;
		}
		int w=0;s=0;
		for (int p=1;p<=x;p=p+w+1)
		{
			w=0;
			gets(ch);
			if (f)continue;
			if (ch[0]=='E')
			{
				int t=0;
				for (int i=1;i<=n;i++)
				if (c[i]==2)t++;
				s=max(s,t);
				a[b[n--]]=0;
			}
			else
			{
				b[++n]=ch[2]-97;
				if (a[b[n]])
				{
					f=1;
					continue;
				}
				a[b[n]]=1;
				int i=4,u=0,v=0;
				while (ch[i]>='0'&&ch[i]<='9')u=u*10+ch[i]-48,i++;
				while (ch[i]!=' ')i++;
				if (ch[4]=='n')u=1000;
				if (ch[i+1]=='n')
				{
					if (u==1000)c[n]=1;
					else c[n]=2;
					continue;
				}
				for (int j=i+1;j<=l;j++)
				if (ch[j]>='0'&&ch[j]<='9')v=v*10+ch[j]-48;
				if (u>v)
				{
					int t=0;
					for (int i=1;i<n;i++)
					if (c[i]==2)t++;
					s=max(s,t);
					a[b[n]]=0;
					int m=n;n--;w=0;
					while (m!=n)
					{
						w++;
						gets(ch);
						if (ch[0]=='E')a[b[m--]]=0;
						else
						{
							b[++m]=ch[2]-97;
							if (a[b[m]])
							{
								f=1;
								continue;
							}
							a[b[m]]=1;
						}
					}
					continue;
				}
				c[n]=1;
			}
		}
		if (f||n!=0)printf("ERR\n");
		else
		{
			int i=100,t=0,x=0;
			while (!c[i])i--;
			if (y==1&&z==0)y=0;
			if (c[i]==z+1&&s==y)printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mo 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void read(ll &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void write(int x)
{
	if (x<0)x=-x;
	if (x>10)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x<0)x=-x;
	if (x>10)write(x/10);
	putchar(x%10+48);
}
ll x,y;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(x);read(y);
	if(x>y)swap(x,y);
	ll z=x+y+1;
	while ((z+x)%y!=0)z+=x;
	write(z);
	return 0;
}

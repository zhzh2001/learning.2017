#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int l,flag;
char s3[20],s1[20],s2[20],s[105];
inline void init(){
	scanf("%d",&l);
	scanf("%s",s+1);
}
inline int getnum(char s[],int start){
	int len=strlen(s+1),ans=0;
	for (int i=start;i<=len;i++){
		if (s[i]>='0'&&s[i]<='9'){
			ans=ans*10+s[i]-'0';
		}else{
			break;
		}
	}
	return ans;
}
const int maxn=105;
struct node{
	int opt,id,val;
}stack[maxn];
bool er,can[300];
int top,rest;
inline void clean(){
	rest=top=er=0;
	memset(can,0,sizeof(can));	
}
bool isnum(char s[]){
	return s[1]>='0'&&s[1]<='9';
}
inline void solve(){
	int len=strlen(s+1);
	for (int i=1;i<=len;i++){
		if (s[i]=='('){
			if (s[i+1]>='0'&&s[i+1]<='9'){
				flag=0;
			}else{
				flag=getnum(s,i+3);
			}
		}
	}
	clean(); int temp=0,mx=0;
	for (int i=1;i<=l;i++){
		scanf("%s",s+1);
		if (s[1]=='E'){
			can[stack[top].id]=0;
			rest-=stack[top].opt;
			temp-=stack[top--].val;
		}else{
			scanf("%s",s1+1);
			scanf("%s",s2+1);
			scanf("%s",s3+1);
			int x=s1[1]-'a';
			if (can[x]){
				er=1;
			}
			can[x]=1; int spj=0,cho=0;
			if (isnum(s2)){
				int x1=getnum(s2,1);
				if (!isnum(s3)){
					if (!rest){
						temp++;
						cho=1;
						mx=max(temp,mx);
					}
				}else{
					int x2=getnum(s3,1);
					if (x1>x2){
						spj=1;
					}
				}
			}else{
				if (isnum(s3)){
					spj=1;
				}
			}
			stack[++top].val=cho;
			stack[top].id=x;
			stack[top].opt=spj;
			rest+=spj;
		}
	}
	if (top){
		er=1;
	}
	if (er){
		puts("ERR");
	}else{
		if (flag==mx){
			puts("Yes");
		}else{
			puts("No");
		}
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		init();
		solve();
	}
	return 0;
}

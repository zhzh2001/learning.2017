#include<cstdio>
#define pa pair<int ,int >
#include<algorithm>
#include<cstring>
#include<queue>
#include<vector>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
typedef long long ll;
const int maxn=100005,maxm=200005;
struct edge{
	int link,next,val;
}e[maxm];
int head[maxn],tot,n,m,k,p;
inline void insert(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
inline void init(){
	n=read(); m=read(); k=read(); p=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read(),w=read();
		insert(u,v,w);
	}
}
int dis[maxn];
bool vis[maxn];
ll ans[maxn];
const int mod=100000;
int q[maxn+5];
inline void solve(){
	memset(dis,127/3,sizeof(dis));
	memset(ans,0,sizeof(ans));
	int h=0,t=1; ans[1]=1; dis[1]=0;
	q[1]=1;
	while (h<t){
		int u=q[h=h%mod+1];
		for (int i=head[u];i;i=e[i].next){
			int v=e[i].link;
			if (dis[v]>dis[u]+e[i].val){
				dis[v]=dis[u]+e[i].val;
				ans[v]=ans[u];
				if (!vis[v]){
					vis[v]=1;
					q[t=t%mod+1]=v;
				}
			}else{
				if (dis[v]==dis[u]+e[i].val){
					(ans[v]+=ans[u])%=p;
				}
			}
		}
		vis[u]=0;
	}
	printf("%d\n",ans[n]);
}
inline void clean(){
	tot=0;
	memset(head,0,sizeof(head));
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		clean();
		init();
		solve();
	}
	return 0;
}

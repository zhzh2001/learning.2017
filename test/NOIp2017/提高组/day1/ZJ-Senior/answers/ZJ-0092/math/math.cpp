#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
void exgcd(ll a,ll b,ll &x,ll &y){
	if (!b){
		x=1; y=0;
		return;
	}
	exgcd(b,a%b,x,y);
	ll temp=x; x=y; y=temp-a/b*y;
}
ll a,b,x,y;
inline ll judge(ll mid){
	for (ll i=mid;i<=mid+100000;i++){
		if (x*i+(y*i/a)*b<0){
			return 0;
		}
	} 
	return 1;
}
inline ll abs(ll x){return (x>0)?x:-x;}
inline void init(){
	scanf("%lld%lld",&a,&b);
	if (a>b) swap(a,b);
	exgcd(a,b,x,y);
	if (y<0){
		swap(a,b);
		swap(x,y);
	}
	ll l=0,r=100000000000;
	while (l<r){
		ll mid=(l+r)>>1;
		if (judge(mid)){
			r=mid;
		}else{
			l=mid+1;
		}
	}
	printf("%lld\n",l-1);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	init();
	return 0;
}

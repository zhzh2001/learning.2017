var
 a,b,i,j,k,m,n:longint;
begin
 assign(input,'math.in');
 reset(input);
 assign(output,'math.out');
 rewrite(output);
 readln(a,b);
 if a>b then begin
  i:=b; j:=a;
 end
 else begin
  i:=a; j:=b;
 end;
 m:=0;
 repeat
  m:=m+j;
 until m mod i=0;
 k:=m-i-j;
 if m mod k>1 then
  n:=m-(m mod k)
 else
  n:=k;
 writeln(n);
 close(input);
 close(output);
end.
var
 t,l,i,q,k:integer;
 st,qq:string;
 strr:array[1..100] of string;
function judge(st:string):string;
 var
  i,j,p,q:integer;
  f,x,y:boolean;
  s,a,b,sum:string;
  fe:array[1..2] of integer;
 begin
  val(st[1],l);
  val(st[1],i);
  f:=true;
  if i mod 2<>0 then begin
   f:=false;
  end;
  for j:=1 to length(st) do
   if st[j]='(' then begin
    s:=s+st[j+1];
    if s[j+1]=')'then break;
   end;
  for j:=1 to l do begin
   a:=s[j];  x:=true; y:=true;
   if a[1]='F' then inc(fe[1]) else inc(fe[2]);
   if (a[3]='x') and (x=false) then begin
    f:=false;
   end
   else if (a[3]='x') and (x=true) then x:=false;
   if (a[3]='y') and (y=false) then begin
    f:=false;
   end
   else if (a[3]='y') and (y=true) then y:=false;
   if fe[1]<>fe[2] then f:=false;
   if (a[5]='n') and (a[7]='n') then sum:='1';
   if (a[5]='n') and (a[7]<>'n') then sum:='1';
   if (a[5]<>'n') and (a[7]='n') then sum:='n^1';
   if (a[5]<>'n') and (a[7]='n') and (sum='n^1') then sum:='n^2';
   if (a[5]<>'n') and (a[7]='n') and (sum='n^2') then sum:='n^3';
   if (a[5]<>'n') and (a[7]='n') and (sum='n^3') then sum:='n^4';
   if (a[5]<>'n') and (a[7]<>'n') then begin
    sum:='1';
   if f=false then judge:='ERR'
   else begin
    if sum=s then judge:='YES' else judge:='NO';
   end;
  end;
 end;
 end;
//////////////////////////////////////////////////////
begin
 assign(input,'complexity.in');
 reset(input);
 assign(output,'complexity.out');
 rewrite(output);
 readln(t);
 for i:=1 to t do begin
  read(st);
  for q:=1 to l do begin
   readln(strr[q]);
  end;
  writeln(judge(st));
 end;
 close(input);
 close(output);
end.
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1e5+5,N=2e5+5;
int head[M],nxt[N],to[N],dis[N],tot;
void add_edge(int a,int b,int v){
	nxt[++tot]=head[a];
	to[tot]=b;
	dis[tot]=v;
	head[a]=tot;
}
int T,n,m,K,P,EA[N],EB[N],EV[N];
inline void add(int &a,int b){
	a+=b;
	if(a>=P)a-=P;
}

namespace P100{
	int Dis[M],mark[M];
	int A[M],sz;
	int dp[M][55];
	struct node{
		int x,v;
		bool operator <(const node &A)const{
			return v>A.v;//�󶥶�
		}
	}B[M*55];
	bool Init(){
		priority_queue<node>Q;
		while(!Q.empty())Q.pop();
		memset(Dis,63,sizeof(Dis));
		memset(mark,0,sizeof(mark));
		Dis[1]=0;
		Q.push((node){1,0});
		while(!Q.empty()){
			node T=Q.top();
			Q.pop();
			int x=T.x,v=T.v;
			if(v>Dis[x])continue;
//			assert(v==Dis[x]);
//			assert(mark[x]==0);
			mark[x]=1;
			for(int i=head[x];i;i=nxt[i]){
				int y=to[i],d=v+dis[i];
				if(d<Dis[y]){
					Dis[y]=d;
					Q.push((node){y,Dis[y]});
				}
//				else if(mark[y]&&d==Dis[y])return 1;
			}
		}
		return 0;
	}
	void sol(){
		memset(dp,0,sizeof(dp));
		dp[1][0]=1;
		for(int i=1;i<=sz;i++){
			int x=B[i].x,v=B[i].v,vv=Dis[x]+v;
			if(!dp[x][v])continue;
			for(int i=head[x];i;i=nxt[i]){
				int y=to[i],d=vv+dis[i]-Dis[y];
				if(d<=K)
					add(dp[y][d],dp[x][v]);
			}
		}
	}
	bool cmp(int a,int b){
		return Dis[a]<Dis[b];
	}
	void solve(){
		for(int i=1;i<=m;i++)
			add_edge(EA[i],EB[i],EV[i]);
		if(Init()){
			puts("-1");
			return;
		}
//		for(int i=1;i<=n;i++)
//			printf("Dis[%d]=%d\n",i,Dis[i]);
		for(int i=1;i<=n;i++)A[i]=i;
		sort(A+1,A+1+n,cmp);
		A[0]=0,Dis[0]=-1e9,sz=0;
		int R=1;
		for(int i=1;i<=n;i++)
			for(int j=max(Dis[A[i-1]]+K+1,Dis[A[i]]);j<=min(Dis[n],Dis[A[i]])+K;j++){
				while(R<n&&Dis[A[R+1]]<=j)R++;
				for(int k=i;k<=R;k++)
					B[++sz]=(node){A[k],j-Dis[A[k]]};
			}
//		for(int i=1;i<=sz;i++)
//			printf("V=%d x=%d v=%d\n",Dis[B[i].x]+B[i].v,B[i].x,B[i].v);
		sol();
		int ans=0;
		for(int i=0;i<=K;i++)add(ans,dp[n][i]);
		printf("%d\n",ans);
	}
}
namespace Pyes{
	int Dis[M];
	int A[M],sz;
	int dp[M][55],mark[M][55];
	struct node{
		int x,v;
		bool operator <(const node &A)const{
			return v>A.v;//�󶥶�
		}
	}B[M*55];
	bool Init(){
		priority_queue<node>Q;
		while(!Q.empty())Q.pop();
		memset(Dis,63,sizeof(Dis));
		Dis[1]=0;
		Q.push((node){1,0});
		while(!Q.empty()){
			node T=Q.top();
			Q.pop();
			int x=T.x,v=T.v;
			if(v>Dis[x])continue;
			for(int i=head[x];i;i=nxt[i]){
				int y=to[i],d=v+dis[i];
				if(d<Dis[y]){
					Dis[y]=d;
					Q.push((node){y,Dis[y]});
				}
			}
		}
		return 0;
	}
	struct Node{
		int x,s,v;
		bool operator <(const Node &A)const{
			return v>A.v;//�󶥶�
		}
	};
	void sol(){
		priority_queue<Node>Q;
		while(!Q.empty())Q.pop();
		memset(dp,0,sizeof(dp));
		dp[1][0]=1;
		Q.push((Node){1,0,0});
		while(!Q.empty()){
			Node T=Q.top();
			Q.pop();
			int x=T.x,s=T.s,v=T.v;
			if(!dp[x][s])continue;
			for(int i=head[x];i;i=nxt[i]){
				int y=to[i],d=v+dis[i]-Dis[y];
				if(d<=K){
					add(dp[y][d],dp[x][s]);
					if(!mark[y][d])
						Q.push((Node){y,d,v+dis[i]});
					mark[y][d]=1;
				}
			}
		}
	}
	void solve(){
		for(int i=1;i<=m;i++)
			add_edge(EA[i],EB[i],EV[i]);
		if(Init()){
			puts("-1");
			return;
		}
//		for(int i=1;i<=n;i++)
//			printf("Dis[%d]=%d\n",i,Dis[i]);
		sol();
		int ans=0;
		for(int i=0;i<=K;i++)add(ans,dp[n][i]);
		printf("%d\n",ans);
	}
}
int main(){
	srand(time(NULL));
//	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	rd(T);
	while(T--){
		memset(head,0,sizeof(head));
		tot=0;
		int no_v0=1;
		rd(n),rd(m),rd(K),rd(P);
		for(int i=1;i<=m;i++){
			rd(EA[i]),rd(EB[i]),rd(EV[i]);
			if(!EV[i])no_v0=0;
		}
		if(0);
		else Pyes::solve();
	}
	return 0;
}
/*
1
5 6 3 100
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
4 2 1


*/

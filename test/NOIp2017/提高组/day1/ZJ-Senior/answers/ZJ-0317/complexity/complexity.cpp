#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=105,oo=1e6;
int T,n,L[M],R[M];
char strO[M],str[M];
char A[M],B[M],C[M],mark[M][M];
int chk(int x){
	if(R[x]-L[x]>=200)return 1;
	if(R[x]<L[x])return -oo;
	return 0;
}
void solve(){
	scanf("%d%s",&n,strO+1);
	int O=0;
	if(strO[3]!='1'){
		for(int i=5;i<strlen(strO+1);i++){
			if(strO[i]>='0'&&strO[i]<='9')O=O*10+strO[i]-'0';
			else break;
		}
	}
	int x=0,now=0,ans=0,no=0;
	while(n--){
		scanf("%s",str);
		if(str[0]=='F'){
			scanf("%s%s%s",A,B,C);
			x++;
			for(int i=1;i<x;i++)
				if(strcmp(A,mark[i])==0)no=1;
			strcpy(mark[x],A);
			if(B[0]=='n')L[x]=oo;
			else{
				L[x]=0;
				for(int i=0;i<strlen(B);i++)
					L[x]=L[x]*10+B[i]-'0';
			}
			if(C[0]=='n')R[x]=oo;
			else{
				R[x]=0;
				for(int i=0;i<strlen(C);i++)
					R[x]=R[x]*10+C[i]-'0';
			}
			now+=chk(x);
			MAX(ans,now);
		}
		else now-=chk(x--);
	}
	if(x||no)puts("ERR");
	else if(ans==O)puts("Yes");
	else puts("No");
}
int main(){
	srand(time(NULL));
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	rd(T);
	while(T--)solve();
	return 0;
}

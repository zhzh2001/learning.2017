#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
int a,b;

namespace P60{
	const int M=1e6+5;
	ll dp[M];
	void solve(){
		if(a>b)swap(a,b);
		memset(dp,-1,sizeof(dp));
		for(ll i=0;;i+=b){
			int t=i%a;
			if(~dp[t])break;
			dp[t]=i;
		}
		ll ans=0;
		for(int i=0;i<a;i++)MAX(ans,dp[i]-a);
		printf("%lld\n",ans);
	}
}
int main(){
	srand(time(NULL));
	freopen("math.in","r",stdin);
	freopen("std.out","w",stdout);
	rd(a),rd(b);
	P60::solve();
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
const int P=32000;
inline int Rand(int x){
	return (rand()%P*P+rand()%P)%x+1;
}
int gcd(int a,int b){
	if(b==0)return a;
	return gcd(b,a%b);
}

int main(){
	srand(time(NULL));
	freopen("math.in","w",stdout);
	int a,b,V=1e6;
	while(a=Rand(V),b=Rand(V),gcd(a,b)>1);
	printf("%d %d\n",a,b);
	return 0;
}

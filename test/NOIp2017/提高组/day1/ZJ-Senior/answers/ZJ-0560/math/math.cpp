#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
#include <utility>
#define INF 0x3f3f3f3f
#define ll long long
#define db double
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define rpd(i,a,b) for(int i=(a);i>=(b);--i)
using namespace std;
inline ll readll() {
	ll x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct BigInteger {
	int s[20],size;
	static const int BASE=1000;
	BigInteger() { memset(s,0,sizeof s); size=0; }
	BigInteger(ll a) {
		memset(s,0,sizeof s); size=0;
		do{ s[size++]=a%BASE; a/=BASE; } while(a);
	}
	BigInteger operator + (const BigInteger&b) const {
		BigInteger c; c.size=0; memset(c.s,0,sizeof c.s);
		for(int i=0,g=0;;++i) {
			if(i>=size&&i>=b.size&&g==0) break;
			if(i<size) g+=s[i]; if(i<b.size) g+=b.s[i];
			c.s[c.size++]=g%BASE; g/=BASE;
		}
		return c;
	}
	BigInteger operator - (const BigInteger&b) const {
		BigInteger c; c.size=0; memset(c.s,0,sizeof c.s);
		for(int i=0,g=0;;++i) {
			if(i>=size&&i>=b.size&&g==0) break;
			if(i<size) g+=s[i]; if(i<b.size) g-=b.s[i];
			if(g<0) { c.s[c.size++]=g+BASE; g=-1; } else { c.s[c.size++]=g,g=0; }
		}
		return c;
	}
	BigInteger operator * (const BigInteger&b) const {
		BigInteger c; c.size=size+b.size-1; memset(c.s,0,sizeof c.s);
		rep(i,0,size-1) rep(j,0,b.size-1)
			c.s[i+j]+=s[i]*b.s[j];
		rep(i,0,c.size-1) c.s[i+1]+=c.s[i]/BASE,c.s[i]%=BASE;
		if(c.s[c.size]>0) ++c.size;
		return c;
	}
	void println() {
		printf("%d",s[size-1]);
		rpd(i,size-2,0) printf("%03d",s[i]); puts("");
	}
};
int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);	
	BigInteger a=readll(),b=readll();
	BigInteger c=a*b-(a+b); c.println();

	return 0;
}


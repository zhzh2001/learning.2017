#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
#include <utility>
#define INF 0x3f3f3f3f
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define rpd(i,a,b) for(int i=(a);i>=(b);--i)
#define rpg1(i,now) for(int i=head[now];~i;i=edge[i].nxt)
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
int N,M,K,Pi;
const int MAXN=100010,MAXM=200010;
struct Node {int to,nxt,w;} edge[MAXM];
int head[MAXN],idx=0,dis[MAXN],dp[MAXN][51],vis[MAXN][51],flag;
inline void ade(const int&u,const int&v,const int&w) {
	edge[++idx].to=v; edge[idx].w=w; edge[idx].nxt=head[u]; head[u]=idx;
}
#define __ %Pi
inline void cadd(int&a,const int&b) { a=(a+b)__; }
#define F first
#define S second
#define mp make_pair
#define son edge[i].to
typedef pair<int,int> P;
priority_queue<P,vector<P>,greater<P> >Q;
void dijkstra(const int&sta) {
	memset(dis,0x3f,sizeof dis); dis[sta]=0;
	Q.push(mp(0,sta));
	while(!Q.empty()) {
		P t=Q.top(); Q.pop();
		if(dis[t.S]<t.F) continue;
		rpg1(i,t.S) if(dis[son]>t.F+edge[i].w) {
			dis[son]=t.F+edge[i].w;
			Q.push(mp(dis[son],son));
		}
	}
}
void dfs(const int&now,const int&k) {
	if(flag) return;
	if(vis[now][k]) { flag=1; return; }
	vis[now][k]=1;
	rpg1(i,now) {
		int p=dis[now]+k+edge[i].w-dis[son]; if(p>K) continue;
		if(dp[son][p]==-1) dp[son][p]=dp[now][k]; else cadd(dp[son][p],dp[now][k]);
		dfs(son,p);
	}
	vis[now][k]=0;
}		
#undef F
#undef S
#undef mp
#undef son
void work() {
	memset(head,-1,sizeof head); idx=0;
	N=read(),M=read(),K=read(),Pi=read();
	rep(i,1,M) {
		int u=read(),v=read(),w=read();
		ade(u,v,w);
	}
	flag=0; 
	dijkstra(1); 
	memset(dp,-1,sizeof dp); dp[1][0]=1; 
	memset(vis,0,sizeof vis);
	dfs(1,0);
	if(flag) puts("-1"); else {
		int ans=0;
		rep(i,0,K) if(dp[N][i]!=-1) cadd(ans,dp[N][i]); printf("%d\n",ans);
	}
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while(T--) work();
	
	return 0;
}


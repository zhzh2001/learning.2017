#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
#include <utility>
#define INF 0x3f3f3f3f
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define rpd(i,a,b) for(int i=(a);i>=(b);--i)
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[100],S[200],top,ch; int S1[200],used[100],anstag;
void work() {
	int N=read(),fac=0; scanf("%s",s); int len=strlen(s);
	if(s[2]=='1') fac=0; else {
		rep(i,4,len-2) fac=fac*10+s[i]-'0';
	}
	top=0; int tmp=0,ans=0,tag=0; memset(used,0,sizeof used);
	anstag=1;
	rep(i,1,N) {
		ch=getchar();
		while(ch!='E'&&ch!='F') ch=getchar();
		if(ch=='E') {
			if(!anstag) continue;
			if(top==0) anstag=0; 
			if(!anstag) continue;
			used[S[top-1]-'a']=0;
			if(S1[top-1]==-1) --tag,--top; else tmp-=S1[--top];
		} else {
			ch=getchar();
			while(ch<'a'||ch>'z') ch=getchar();
			if(used[ch-'a']) anstag=0;
			used[ch-'a']=1;
			int num1=1,num2=1; int x=0,y=0;
			scanf("%s",s); if(s[0]=='n') num1=0; else rep(j,0,strlen(s)-1) x=x*10+s[j]-'0';
			scanf("%s",s); if(s[0]=='n') num2=0; else rep(j,0,strlen(s)-1) y=y*10+s[j]-'0';
			if(!anstag) continue;
			if(num1==0&&num2==1||num1&&num2&&x>y) ++tag,S1[top]=-1;
			else if(tag) S1[top]=0;
			else if(num1==1&&num2==0) tmp++,S1[top]=1;
			else S1[top]=0;
			S[top++]=ch;
			ans=max(ans,tmp);
		}
	}
	if(!anstag||top>0) { puts("ERR"); return; }
	if(ans==fac) puts("Yes"); else puts("No");
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while(T--) work();

	return 0;
}


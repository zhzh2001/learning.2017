#include<cstdio>
#include<cstring>

int t,it,n,m,k,mo,i,j,u,v,w;
int first[100005],e[200005],nxt[200005],c[200005],tope;
//int zfirst[100005],znxt[200005];
int rfirst[100005],re[200005],rnxt[200005],rc[200005],rtope;
int ch;
inline int getint()
{
	for(ch=getchar();ch<'0'||ch>'9';ch=getchar());
	int i1=ch-'0';
	for(ch=getchar();ch>='0'&&ch<='9';ch=getchar())i1=((i1<<3)+(i1<<1)+ch-'0');
	return i1;
}
//int zfa[100005],zdep[100005];

int dis[100005],disarr[100005],rankdis[100005],dis1[100005];
int h[100005],inh[100005],toph;

inline void down(int v0)
{
	int i1=v0,i2=(v0<<1),i3=h[v0];
	while(i2<=toph)
	{
		if((i2<toph)&&(dis[h[i2]]>dis[h[i2+1]]))++i2;
		if(dis[h[i2]]<dis[i3])
		{
			inh[h[i1]=h[i2]]=i1;
			i1=i2;
			i2<<=1;
		}else break;
	}
	inh[h[i1]=i3]=i1;
	return;
}
inline void up(int v0)
{
	int i1=v0,i2=(v0>>1),i3=h[v0];
	while(i2>0)
	{
		if(dis[h[i2]]>dis[i3])
		{
			inh[h[i1]=h[i2]]=i1;
			i1=i2;
			i2>>=1;
		}else break;
	}
	inh[h[i1]=i3]=i1;
	return;
}

inline void dijfrom1()
{
	int i1,i2;
	dis[1]=0;
	h[toph=1]=1;
	inh[1]=1;
	for(i1=2;i1<=n;++i1)
	{
		dis[i1]=987654321;
		h[++toph]=i1;
		inh[i1]=toph;
	}
	while(toph>0)
	{
		i1=h[1];
		inh[h[1]=h[toph]]=1;
		--toph;
		down(1);
		disarr[n-toph]=i1;
		for(i2=first[i1];i2;i2=nxt[i2])
		{
			if(dis[i1]+c[i2]<dis[e[i2]])
			{
				dis[e[i2]]=dis[i1]+c[i2];
				up(inh[e[i2]]);
			}
		}
	}
	return;
}
inline void dijfromn()
{
	int i1,i2;
	dis[n]=0;
	h[toph=1]=n;
	inh[n]=1;
	for(i1=1;i1<n;++i1)
	{
		dis[i1]=987654321;
		h[++toph]=i1;
		inh[i1]=toph;
	}
	while(toph>0)
	{
		i1=h[1];
		inh[h[1]=h[toph]]=1;
		--toph;
		down(1);
		for(i2=rfirst[i1];i2;i2=rnxt[i2])
		{
			if(dis[i1]+rc[i2]<dis[re[i2]])
			{
				dis[re[i2]]=dis[i1]+rc[i2];
				up(inh[re[i2]]);
			}
		}
	}
	return;
}
bool chose[100005];
int ind[100005];
int q[100005],lq,rq,cntcho;
int cntdis[100005],p[100005];
int dp[100005][61];
long long ans;

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=getint();
	for(it=1;it<=t;++it)
	{
		n=getint();m=getint();k=getint();mo=getint();
		memset(first,0,sizeof(first));
//		memset(zfirst,0,sizeof(zfirst));
		memset(rfirst,0,sizeof(rfirst));
		tope=0;
		rtope=0;
		for(i=1;i<=m;++i)
		{
			u=getint();v=getint();w=getint();
			++tope;
			e[tope]=v;
			nxt[tope]=first[u];
			first[u]=tope;
			c[tope]=w;
//			if(w<=0)
//			{
//				znxt[tope]=zfirst[u];
//				zfirst[u]=tope;
//			}
			++rtope;
			re[rtope]=u;
			rnxt[rtope]=rfirst[v];
			rfirst[v]=rtope;
			rc[tope]=w;
		}
		//dij from 1
		dijfrom1();
		for(i=1;i<=n;++i)dis1[i]=dis[i];
		//dij from n
		dijfromn();
		if(dis[1]!=dis1[n])puts("!!!");
		//toposort
		memset(chose,0,sizeof(chose));
		for(i=1;i<=n;++i)if(dis1[i]+dis[i]<=dis[1]+k)chose[i]=true;
//		for(i=1;i<=n;++i)dis[i]+=dis1[i];
		memset(ind,0,sizeof(ind));
//		memset(recd,0,sizeof(recd));
		cntcho=0;
		for(i=1;i<=n;++i)
		{
			if(!chose[i])continue;
			++cntcho;
			for(j=first[i];j;j=nxt[j])
			{
				if((c[j]<=0)&&(chose[e[j]]))++ind[e[j]];
			}
		}
		rq=0;
		for(i=1;i<=n;++i)if((chose[i])&&(ind[i]<=0))q[++rq]=i;
		for(lq=1;lq<=rq;++lq)
		{
			for(i=first[q[lq]];i;i=nxt[i])
			{
				if((c[i]<=0)&&(chose[e[i]]))
				{
					--ind[e[i]];
					if(ind[e[i]]<=0)q[++rq]=e[i];
				}
			}
		}
		if(rq<cntcho)
		{
			puts("-1");
			continue;
		}
		memset(cntdis,0,sizeof(cntdis));
		rankdis[disarr[1]]=1;
		for(i=2;i<=n;++i)
		{
			if(dis1[disarr[i]]==dis1[disarr[i-1]])rankdis[disarr[i]]=rankdis[disarr[i-1]];
			else rankdis[disarr[i]]=i;
		}
		for(i=1;i<=rq;++i)
		{
			++cntdis[rankdis[q[i]]];
		}
		for(i=1;i<=n;++i)cntdis[i]+=cntdis[i-1];
		for(i=rq;i>=1;--i)
		{
			p[cntdis[rankdis[q[i]]]]=q[i];
			--cntdis[rankdis[q[i]]];
		}
		memset(dp,0,sizeof(dp));
		dp[1][0]=1;
		for(i=0;i<=k;++i)
		{
			for(lq=1;lq<=rq;++lq)
			{ 
				if(!dp[p[lq]][i])continue;
				u=dis1[p[lq]]+i;
				v=dp[p[lq]][i];
				for(j=first[p[lq]];j;j=nxt[j])
				{
					if(!chose[e[j]])continue;
					w=u+c[j]-dis1[e[j]];
					if(w>k)continue;
					dp[e[j]][w]=(dp[e[j]][w]+v)%mo;
				}
			}
		}
		ans=0;
		for(i=0;i<=k;++i)ans+=dp[n][i];
		i=ans%mo;
		printf("%d\n",i);
//		//find zero circles
//		memset(zdep,0,sizeof(zdep));
//		for(i=1;i<=n;++i)
//		{
//			
//		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

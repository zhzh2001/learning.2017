#include<cstdio>
#include<cstring>

int t,it,len,n,m,i,j;
int ch;
inline int getint()
{
	for(ch=getchar();ch<'0'||ch>'9';ch=getchar());
	int i1=ch-'0';
	for(ch=getchar();ch>='0'&&ch<='9';ch=getchar())i1=((i1<<3)+(i1<<1)+ch-'0');
	return i1;
}
inline int getintorn()
{
	for(ch=getchar();(ch!='n')&&((ch<'0')||(ch>'9'));ch=getchar());
	if(ch=='n')return 123456;
	int i1=ch-'0';
	for(ch=getchar();ch>='0'&&ch<='9';ch=getchar())i1=((i1<<3)+(i1<<1)+ch-'0');
	return i1;
}
int resn,ans;
int s[202],si[202],sl[202],sr[202];
int sta[202],top;
int cnt[256];

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=getint();
	for(it=1;it<=t;++it)
	{
		len=getint();
		for(ch=getchar();ch!='(';ch=getchar());
		for(ch=getchar();(ch!='n')&&(ch!='^')&&(ch<'0'||ch>'9');ch=getchar());
		if(ch=='n'||ch=='^')resn=getint();
		else
		{
			resn=ch-'0';
			for(ch=getchar();ch>='0'&&ch<='9';ch=getchar())resn=((resn<<3)+(resn<<1)+ch-'0');
//			if(resn!=1)puts("???");
			resn=0;
		}
		for(i=1;i<=len;++i)
		{
			for(ch=getchar();ch!='F'&&ch!='E';ch=getchar());
			if(ch=='E')
			{
				s[i]=0;
			}else
			{
				s[i]=1;
				for(ch=getchar();ch<'a'||ch>'z';ch=getchar());
				si[i]=ch;
				sl[i]=getintorn();
				sr[i]=getintorn();
			}
		}
		memset(cnt,0,sizeof(cnt));
		top=0;
		for(i=1;i<=len;++i)
		{
			if(s[i])
			{
				sta[++top]=i;
				if(cnt[si[i]])break;
				cnt[si[i]]=1;
			}else
			{
				if(top<=0)break;
				cnt[si[sta[top]]]=0;
				--top;
			}
		}
		if((i<=len)||(top>0))
		{
			puts("ERR");
			continue;
		}
		ans=0;
		top=0;
		sta[0]=0;
		for(i=1;i<=len;++i)
		{
			if(s[i])
			{
				++top;
				if(sl[i]>sr[i])//n,1 2,1
				{
					if(sta[top-1]>ans)ans=sta[top-1];
					sta[top]=-987654321;
				}else if(sl[i]<1000&&sr[i]>1000)//1,n
				{
					sta[top]=sta[top-1]+1;
					if(sta[top]>ans)ans=sta[top];
				}else//1,2 1,1
				{
					sta[top]=sta[top-1];
					if(sta[top]>ans)ans=sta[top];
				}
			}else --top;
		}
		if(ans==resn)puts("Yes");
		else puts("No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

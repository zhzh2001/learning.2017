#include<bits/stdc++.h>
using namespace std;

#define FOR(i,a,b) for(int i=(a);i<=(b);i++)
#define DFOR(i,a,b) for(int i=(a);i>=(b);i--)

int a,b; 

struct P30{
	#define Max 15000000
	bool Mark[Max+5];
	
	void Solve(){
		Mark[a]=Mark[b]=1;
	
		FOR(i,1,Max){
			if(!Mark[i]) continue;
			if(i+a<Max) Mark[i+a]=1;
			if(i+b<Max) Mark[i+b]=1;
		}
		
		int Ans=0;
		DFOR(i,Max-1,1) if(!Mark[i]){Ans=i;break;}
		
		printf("%d\n",Ans);
	}

}P30;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	//printf("%lf MB",sizeof(P30));
	scanf("%d %d",&a,&b);
	
	P30.Solve();
	
	return 0;
}

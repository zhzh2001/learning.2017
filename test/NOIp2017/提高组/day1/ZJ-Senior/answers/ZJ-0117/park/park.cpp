#include<bits/stdc++.h>
using namespace std;

#define FOR(i,a,b) for(int i=(a);i<=(b);i++)
#define DFOR(i,a,b) for(int i=(a);i>=(b);i--)
#define LFOR(i,x) for(int i=Head[x];i;i=Nxt[i])
#define V 100005
#define E 200005

int Case,n,m,MOD,K,SP,Ans,Tim=0;
int Head[V],Nxt[E<<1],To[E<<1],Cost[E<<1],Tot;
int Dis[V],Mark[V];
queue<int> Que;

/*
	����
	����
	puts -1
*/

void Add_Edge(int a,int b,int c){Nxt[++Tot]=Head[a];Head[a]=Tot;To[Tot]=b;Cost[Tot]=c;}

void DFS(int x,int Cst){
	if(Cst>SP+K) return;
	if(x==n) Ans++,Ans%=MOD;
	LFOR(i,x){
		int y=To[i];
		DFS(y,Cst+Cost[i]);
	}
}

void SPFA(){
	while(!Que.empty()) Que.pop();
	memset(Dis,0x3f,sizeof(Dis));
	memset(Mark,0,sizeof(Mark));
	Mark[1]=1;Que.push(1);Dis[1]=0;
	
	while(!Que.empty()){
		int Now=Que.front();Que.pop();
		Mark[Now]=0;
		
		LFOR(i,Now){
			int y=To[i];
			if(Dis[y]>Dis[Now]+Cost[i]){
				Dis[y]=Dis[Now]+Cost[i];
				if(!Mark[y]){
					Mark[y]=1;
					Que.push(y);
				}
			}
		}
	
	}
	
	SP=Dis[n];
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	scanf("%d",&Case);
	while(Case--){
		memset(Head,0,sizeof(Head));
		
		scanf("%d %d %d %d",&n,&m,&K,&MOD);
		FOR(i,1,m){
			int a,b,c;
			scanf("%d %d %d",&a,&b,&c);
			Add_Edge(a,b,c);
		}
		
		SPFA();
		
		Ans=0;
		DFS(1,0);
		
		printf("%d\n",Ans%MOD);
	}

	return 0;
}

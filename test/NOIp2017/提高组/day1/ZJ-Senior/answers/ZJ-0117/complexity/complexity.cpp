#include<bits/stdc++.h>
using namespace std;

#define FOR(i,a,b) for(int i=(a);i<=(b);i++)
#define DFOR(i,a,b) for(int i=(a);i>=(b);i--)
#define INF 0x3f3f3f3f

int Case,n,Ans,f;

struct Node{
	int Val;
	char Op[3],Name[3],From[3],To[3];
}Tmp[205];

bool Mark[300];
char Flag[10];

void Clear(){
	f=1;
	memset(Mark,0,sizeof(Mark));
}

void chkmax(int &a,int b){if(a<b) a=b;}
void chkmin(int &a,int b){if(a>b) a=b;}

int Check(int Now,int p){
	if(p && Mark[Tmp[Now].Name[0]]){f=0;return 0;}
	if(Tmp[Now].From[0]!='n' && Tmp[Now].To[0]!='n') return 1;
	if(Tmp[Now].From[0]!='n' && Tmp[Now].To[0]=='n') return 2;
	if(Tmp[Now].From[0]=='n' && Tmp[Now].To[0]=='n') return 3;
	if(Tmp[Now].From[0]=='n' && Tmp[Now].To[0]!='n') return 4;
}

int DFS(int L,int R){
	if(!f) return 0;
	if(L+1==R){
		int Res=Check(L,1);
		if(Res==1) return 0;
		if(Res==2) return 1;
		if(Res==3) return 0;
		if(Res==4) return 0;
	}
	int Sum=0,Res;
	FOR(i,L,R){
		Sum+=Tmp[i].Val;
		if(Sum<0){f=0;return 0;}
		if(Sum==0){
			{
				if(Mark[Tmp[L].Name[0]]){f=0;return 0;}
				Mark[Tmp[L].Name[0]]=1;
				int A=Check(L,0);
				int B=(L+2<i?DFS(L+1,i-1):0);
				if(A==1) Res=B;
				else if(A==2) Res=B+1;
				else if(A==3) Res=B;
				else Res=0;
				Mark[Tmp[L].Name[0]]=0;
			}
			{
				if(i+1<R) chkmax(Res,DFS(i+1,R));
			}
			break;
		}
	}
	return Res;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	scanf("%d",&Case);
	
	while(Case--){
		Clear();
		
		scanf("%d %s",&n,Flag);
		if(n&1) f=0;
		int Cnt=0;
		
		int Num=0,Pos=4;
		if(Flag[3]=='^') while(Pos<strlen(Flag) && Flag[Pos]>='0' && Flag[Pos]<='9')
			Num=Num*10+(Flag[Pos++]^48);
		else Num=0;
		
		FOR(i,1,n){
			scanf("%s",Tmp[i].Op);
			if(Tmp[i].Op[0]=='E'){Tmp[i].Val=-1;continue;}
			else{
				Tmp[i].Val=1;
				scanf("%s %s %s",Tmp[i].Name,Tmp[i].From,Tmp[i].To);
			}
			Cnt+=Tmp[i].Val;
			if(Cnt<0) f=0;
		}
		
		if(!Cnt) f=0;
		
		int A=DFS(1,n);
		if(!f) puts("ERR");
		else if(Num==A) puts("Yes");
		else puts("No");
	}

	return 0;
}

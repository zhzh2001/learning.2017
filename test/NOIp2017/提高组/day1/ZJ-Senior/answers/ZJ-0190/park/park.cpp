#include<bits/stdc++.h>
#define For(i,x,y) for (int i=x;i<=y;i++)
using namespace std;
int n,m,k,p,maap[2000][2000],tot,head[2000],num,ans,t;
bool f[2000];

struct line{
	int w,next,d;
}h[200010];

void add(int u,int v,int w){
	tot++;
	h[tot].w=w;
	h[tot].d=v;
	h[tot].next=head[u];
	head[u]=tot;
}

void dfs(int p){
	if (num>maap[1][n]+k) return;
	if (p==n) ans=(ans+1)%p;
	for (int i=head[p];i;i=h[i].next){
		if (f[h[i].d]){
			f[h[i].d]=false;
			num+=h[i].w;
			dfs(h[i].d);
			f[h[i].d]=true;
			num-=h[i].w;
		}	
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t>0){
		t--;
		tot=0; num=0; ans=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		For(i,1,n) For(j,1,n) maap[i][j]=100000;
		For(i,1,m){
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			maap[u][v]=w;
			add(u,v,w);
		}
		For(k,1,n) For(i,1,n) For(j,1,n)
			maap[i][j]=min(maap[i][k]+maap[k][j],maap[i][j]);
		memset(f,true,sizeof(f));
		f[1]=false;
		dfs(1);
		printf("%d\n",ans);
	}
}

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int maxn = 100001;
long ans = 0,m_dis;
long node = 0,head[maxn];
long N,M,T,K,P;

struct edge
{
	long to,val,next;
}ed[maxn],ed2[maxn];
void add(long u,long v,long w)
{
	ed[++node].to = v;
	ed[node].val = w;
	ed[node].next = head[u];
	head[u] = node;  
}
int q[maxn],dis[maxn],vis[maxn] = {0};
void spfa()
{
	memset(dis,0x3f,sizeof(dis));
	memset(vis,0,sizeof(vis));
	q[1] = 1;
	vis[1] = 1;
	long l = 0,r = 1;
	dis[1] =0;
	while(l<r)
	{
		int u = q[++l]; // out 
		vis[u] = 0; 
		for(long i= head[u];i!=-1;i = ed[i].next)
		{
			if(dis[ed[i].to] > dis[u] + ed[i].val)
			{
				dis[ed[i].to] = dis[u] + ed[i].val;
				if(vis[ed[i].to] == 0)
				{
					vis[ed[i].to] = 1;
					q[++r] = ed[i].to;
				}
			}
		}
	}
}
void dfs(long id,long cur)
{
	if( id == N) {ans++;return;}
	for(int i=head[id];i!=-1;i=ed[i].next)
	{
		if(cur+ed[i].val<=m_dis)
		  dfs(ed[i].to,cur+ed[i].val);
	}
}


int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>T;
	for(long t =1;t<=T;t++)
	{
		node = 0,ans = 0;
	     scanf("%d %d %d %d",&N,&M,&K,&P);
      	for(int i=1;i<=N;i++) 
		     head[i] = -1;
      	for(long i=1;i<=M;i++)
      	{
      		long a,b,c;
      		scanf("%d %d %d",&a,&b,&c);
      		add(a,b,c);
		}
      	spfa();
      	 m_dis = dis[N] +K;
        dfs(1,0);
       	cout<<ans<<"\n";
    }
	return 0;
}

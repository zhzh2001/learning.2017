program math;
 var
  a,b:int64;
 begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  readln(a,b);
  writeln(a*b-a-b);
  close(input);
  close(output);
 end.

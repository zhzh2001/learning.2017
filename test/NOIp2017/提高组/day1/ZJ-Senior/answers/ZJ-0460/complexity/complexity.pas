program complexity;
 var
  a:array[0..101] of longint;
  vars:array[0..101] of char;
  letter:array['a'..'z'] of longint;
  i,j,i1,l,m,n,t,pow,con,maxpow,error,top,sum:longint;
  c:char;
  hahaletter:set of char;
 begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  hahaletter:=['0'..'9'];
  readln(t);
  for i1:=1 to t do
   begin
    read(l);
    read(c);
    while c<>'(' do
     read(c);
    read(c);
    if c='1' then
     begin
      readln;
      con:=1;
      pow:=0;
     end
             else
     begin
      read(c);
      read(c);
      con:=1008208820;
      pow:=0;
      while c<>')' do
       begin
        pow:=pow*10+ord(c)-48;
        read(c);
       end;
      readln;
     end;
    maxpow:=0;
    error:=0;
    top:=0;
    fillchar(letter,sizeof(letter),0);
    for i:=1 to l do
     begin
      read(c);
      if c='F' then
       begin
        read(c);
        read(c);
        inc(top);
        if letter[c]<>0 then error:=1
                        else letter[c]:=i;
        vars[top]:=c;
        read(c);
        read(c);
        m:=0;
        n:=0;
        while c<>' ' do
         begin
          if c='n' then m:=1008208820
                   else m:=m*10+ord(c)-48;
          read(c);
         end;
        read(c);
        while (c='n') or (c in hahaletter) do
         begin
          if c='n' then n:=1008208820
                   else n:=n*10+ord(c)-48;
          read(c);
         end;
        if (m<>1008208820) and (n=1008208820) then a[top]:=1008208820
                                              else if m>n then a[top]:=0
                                                          else a[top]:=1;
       end
               else
       begin
        sum:=0;
        for j:=1 to top do
         begin
          if a[j]=1008208820 then inc(sum);
          if a[j]=0 then break;
         end;
        if sum>maxpow then maxpow:=sum;
        if top>0 then letter[vars[top]]:=0;
        dec(top);
        if top<0 then error:=1;
       end;
      readln;
     end;
     if top<>0 then error:=1;
     if error=1 then writeln('ERR')
                else if maxpow=pow then writeln('YES')
                                   else writeln('NO');
   end;
  close(input);
  close(output);
 end.

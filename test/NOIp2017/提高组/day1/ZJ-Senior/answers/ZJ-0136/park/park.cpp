#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<ctype.h>
#include<vector>
#include<queue>
#define M 100005
#define oo 1000000000
using namespace std;
int n,m,K,P;
struct node{
	int to,v;
	bool operator <(const node &p)const{
		return v>p.v;
	}
};
vector<node>edge[M];
void Rd(int &res){
	char c;
	res=0;
	while(c=getchar(),!isdigit(c));
	do{
		res=(res<<3)+(res<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}
int T;
void add(int &x,int v){
	x+=v;
	if(x>=P)x-=P;
}
struct P30{
	int dis[M],t[M];
	bool mark[M];
	priority_queue<node>Q;
	void djs(){
		for(int i=0;i<M;i++)dis[i]=oo+500,t[i]=0;
		memset(mark,0,sizeof(mark));
		dis[1]=0,t[1]=1;
		Q.push((node){1,0});
		while(!Q.empty()){
			node now=Q.top();Q.pop();
			if(mark[now.to])continue;
			mark[now.to]=1;
			for(int i=0;i<(int)edge[now.to].size();i++){
				node p=edge[now.to][i];
				if(dis[p.to]>dis[now.to]+p.v){
					t[p.to]=t[now.to];
					dis[p.to]=dis[now.to]+p.v;
					Q.push((node){p.to,dis[p.to]});
				}
				else if(dis[p.to]==dis[now.to]+p.v)add(t[p.to],t[now.to]);
			}
		}
	}
	void solve(){
		djs();
//		printf("(%d %d %d %d)\n",t[2],t[4],dis[4],dis[5]);
		printf("%d\n",t[n]);
	}
}p30;
struct P30_{
	int dis[M],t[M];
	bool mark[M];
	priority_queue<node>Q;
	void djs(){
		for(int i=0;i<M;i++)dis[i]=oo+500;
		memset(mark,0,sizeof(mark));
		dis[1]=0,t[1]=1;
		Q.push((node){1,0});
		while(!Q.empty()){
			node now=Q.top();Q.pop();
			if(mark[now.to])continue;
			mark[now.to]=1;
			for(int i=0;i<(int)edge[now.to].size();i++){
				node p=edge[now.to][i];
				if(dis[p.to]>dis[now.to]+p.v){
					dis[p.to]=dis[now.to]+p.v;
					Q.push((node){p.to,dis[p.to]});
				}
			}
		}
	}
	int dp[M][55];
	int dfs(int x,int val){
		if(val-dis[x]>K)return 0;
		int &re=dp[x][val-dis[x]];
		if(re!=-1)return re;
		re=0;
		if(x==n)re=1%P;
		for(int i=0;i<(int)edge[x].size();i++){
			node p=edge[x][i];
			add(re,dfs(p.to,val+p.v));
		}
		return re;
	}
	void solve(){
		djs();
		memset(dp,-1,sizeof(dp));
		printf("%d\n",dfs(1,0));
	}
}p30_;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	Rd(T);
	for(int t=1;t<=T;t++){
		Rd(n),Rd(m),Rd(K),Rd(P);
		for(int i=1;i<=n;i++)edge[i].clear();
		for(int i=1;i<=m;i++){
			int x,y,v;
			Rd(x),Rd(y),Rd(v);
			edge[x].push_back((node){y,v});
		}
		if(K==0)p30.solve();
		else p30_.solve();
//		p30_.solve();
//		puts("OK");
	}
	return 0;
}

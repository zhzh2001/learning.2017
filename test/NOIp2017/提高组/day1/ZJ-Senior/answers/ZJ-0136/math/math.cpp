#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#define LL long long
using namespace std;
int a,b;
LL ans;
struct P60{
	void solve(){
		ans=0;
		bool find=0;
		for(int i=1;i<b&&!find;i++){//i*a为负，k*b为正 
			if(-1ll*a*i+1ll*(a-1)*b<0)break;
			for(int k=a-1;k>=0&&!find;k--){
				if(-1ll*i*a+1ll*k*b<0)break;
				ans=max(ans,-1ll*i*a+1ll*k*b);
				find=1;
			}
		}
		find=0;
		for(int k=1;k<a&&!find;k++){//k*b为负，i*a为正 
			if(-1ll*b*k+1ll*(b-1)*a<0)break;
			for(int i=b-1;i>=0&&!find;i--){
				if(1ll*i*a-1ll*k*b<0)break;
				ans=max(ans,1ll*i*a-1ll*k*b);
				find=1;
			}
		}
		printf("%lld\n",ans);
	}
}p60;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);
//	if(a<=10000&&b<=10000)p60.solve();
	p60.solve();
	return 0;
}

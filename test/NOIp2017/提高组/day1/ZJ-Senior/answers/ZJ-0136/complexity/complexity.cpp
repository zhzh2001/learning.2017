#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#define M 5005
using namespace std;
char a[M],b[M],str[M];
bool mark[M];
int T;
int get(char tmp[]){
	int len=strlen(tmp);
	int re=0;
	for(int i=0;i<len;i++)re=re*10+tmp[i]-'0';
	return re;
}
struct PP{
	int val[M],ans[M],tmp[M];
	void solve(){
		for(int i=1;i<=T;i++){
			memset(val,-1,sizeof(val));
			memset(ans,-1,sizeof(ans));
			memset(mark,0,sizeof(mark));
			int L,c=0,cnt=0;
			scanf("%d%s",&L,str);
			bool isre=0;
//			int stop=0;
			if(str[2]=='1')c=0;
			else {
				int len=strlen(str);
				for(int k=4;k<len-1;k++)c=c*10+str[k]-'0';
			}
			for(int k=1;k<=L;k++){
				scanf("%s",str);
//					if(i==4)printf("(%d %d)\n",isre,k);
				if(str[0]=='F'){
					scanf("%s%s%s",str,a,b);
					if(mark[str[0]])isre=1;
					if(isre)continue;
//					printf("%d %d %d %c %d\n",k,stop,isre,str[0],cnt);
					mark[str[0]]=1;
					cnt++;
					tmp[cnt]=str[0];
					if(a[0]=='n'){
						if(b[0]!='n')val[cnt]=-1;
						else val[cnt]=0;
					}
					else {
						if(b[0]=='n')val[cnt]=1;
						else {
							int A=get(a),B=get(b);
							if(A<=B)val[cnt]=0;
							else val[cnt]=-1;
						}
					}
//					if(val[cnt]==-1)stop++;
				}
				else {
					if(!cnt)isre=1;
					if(isre)continue;
					if(val[cnt]!=-1)val[cnt]+=max(0,ans[cnt+1]);
					ans[cnt+1]=-1;
					ans[cnt]=max(ans[cnt],val[cnt]);
//					if(val[cnt]==-1)stop--;
					val[cnt]=-1;
					mark[tmp[cnt]]=0;
					cnt--;
				}
//				printf("(%d %d %d %c)\n",ans[1],c,cnt,str[0]);
			}
			if(cnt)isre=1;
			ans[1]=max(ans[1],0);
			if(isre)puts("ERR");
			else if(ans[1]!=c)puts("No");
			else puts("Yes");
		}
	}
}pp;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	pp.solve();
	return 0;
}

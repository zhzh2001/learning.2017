#include<cstdio>
#include<algorithm>
#include<cstring>
#include<iostream>
#define MAXN 200008
using namespace std;

int t,n,m,k,p,q[MAXN*10],in[MAXN];
int head[MAXN],next[MAXN],vet[MAXN],tot;
long long num[MAXN],g,d[MAXN],ans,len[MAXN];

struct node{
	int x,y;
	long long z;
}f[200008];

void add(int x,int y,long long z){
	tot++;
	next[tot]=head[x];
	head[x]=tot;
	vet[tot]=y;
	len[tot]=z;
}

void dfs(int u,long long sum){
	if(sum>k){
		return;
	}
	if(u==1){
		ans=(ans+1)%p;
	}
	for(int i=head[u];i;i=next[i]){
		int y=vet[i];
		dfs(y,sum+len[i]);
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	scanf("%d",&t);
	for(int i=1;i<=t;i++){
		memset(num,0,sizeof(num));
		memset(in,0,sizeof(in));
		memset(head,0,sizeof(head));
		memset(next,0,sizeof(next));
		tot=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=m;i++){
			scanf("%d%d%lld",&f[i].x,&f[i].y,&f[i].z);
			add(f[i].x,f[i].y,f[i].z);
		}
		if(k==0){
			for(int i=2;i<=n;i++)
				d[i]=2000000098;
			int st=0,ed=1;
			q[1]=1; in[1]=1;
			num[1]=1; d[1]=0;
			while(st<ed){
				int x=q[++st];
				for(int i=head[x];i;i=next[i]){
					int y=vet[i];
					if(d[y]>d[x]+len[i]){
						d[y]=d[x]+len[i];
						if(!in[y]){
							in[y]=1;
							ed++;
							q[ed]=y;
						}
					}
					if(d[y]==d[x]+len[i])
						num[y]=(num[y]+num[x])%p;
				}
				in[x]=0;
			}
			printf("%lld\n",num[n]);
		}else{
			for(int i=2;i<=n;i++)
				d[i]=2000000098;
			int st=0,ed=1;
			q[1]=1; in[1]=1;
			d[1]=0;
			while(st<ed){
				int x=q[++st];
				for(int i=head[x];i;i=next[i]){
					int y=vet[i];
					if(d[y]>d[x]+len[i]){
						d[y]=d[x]+len[i];
						if(!in[y]){
							in[y]=1;
							ed++;
							q[ed]=y;
						}
					}
				}
				in[x]=0;
			}
			k=d[n]+k;
			memset(head,0,sizeof(head));
			memset(next,0,sizeof(next));
			for(int i=1;i<=m;i++){
				add(f[i].y,f[i].x,f[i].z);
			}
			ans=0;
			dfs(n,0);
			printf("%lld\n",ans);
		}
	}
}

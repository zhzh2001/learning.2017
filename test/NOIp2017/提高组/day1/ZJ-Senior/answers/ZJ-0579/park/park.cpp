#include <cstdio>
#include <cstring>
#include <utility>
#include <queue>
using namespace std;
const int N = 1e5 + 10, M = 2e5 + 10, INF = 1e9 + 7;
typedef pair<int, int> P;

int E = 0, Einv = 0, fst[N], nxt[M], w[M], to[M], fstinv[N], nxtinv[M], toinv[M], winv[M], ans, n, d[N], m, k, u, v, p, wei;

void add(int u, int v, int wei)
{
	to[++E] = v, nxt[E] = fst[u], w[E] = wei, fst[u] = E;
}

void addinv(int u, int v, int wei)
{
	toinv[++Einv] = v, nxtinv[Einv] = fstinv[u], winv[Einv] = wei, fstinv[u] = Einv;
}

void dij(int s)
{
	priority_queue<P> Q;
	for (int i = 1; i <= n; i++) d[i] = INF;
	d[s] = 0;
	Q.push(P(0, s));
	while (!Q.empty())
	{
		P tmp = Q.top(); Q.pop();
		if (tmp.first > d[tmp.second]) continue;
		for (int i = fstinv[tmp.second]; i != -1; i = nxtinv[i]) if (d[toinv[i]] > d[tmp.second] + winv[i])
		{
			d[toinv[i]] = d[tmp.second] + winv[i];
			Q.push(P(d[tmp.second] + winv[i], toinv[i]));
		}
	}
}

void dfs(int now, int dis)
{
	if (now == n)
	{
		ans = (ans + 1) % p;
		return;
	}
	for (int i = fst[now]; i != -1; i = nxt[i]) if (d[to[i]] + dis > d[1] + k)
		dfs(to[i], dis + w[i]);
}

int main()
{
	int T;
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &T);
	while (T--)
	{
		memset(fst, -1, sizeof(fst)); E = 0;
		memset(fstinv, -1, sizeof(fstinv)); Einv = 0;
		ans = 0;
		scanf("%d%d%d%d", &n, &m, &k, &p);
		for (int i = 1; i <= m; i++)
		{
			scanf("%d%d%d", &u, &v, &wei);
			add(u, v, wei); addinv(v, u, wei);
		}
		dij(n);
		dfs(1, 0);
		printf("%d\n", ans % p);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

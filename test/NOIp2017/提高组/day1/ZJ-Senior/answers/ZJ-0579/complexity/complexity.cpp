#include <cstdio>
#include <cstring>
#include <stack>
#include <algorithm>
using namespace std;
char s[10000];

int err, comp, F, _max, ins[100], flag;
stack<int> sta;

void init()
{
	err = 0; comp = 0; F = 0; _max = 0; flag = 0;
	while (!sta.empty()) sta.pop();
	memset(ins, 0, sizeof(ins));
}

bool cl(int be, int en)
{
	if (err) return 0;
	if (be > en)
	{
		err = 1;
		return 0;
	}
	if (be == en) return 0;
	if (en > 120) return 1;
	return 0;
}

bool solve(int len)
{
	int x = 0, be, en;
	F++;
	for (int i = 2; i < len; i++)
	{
		if (s[i] == ' ')
		{
			x++;
			continue;
		}
		if (x == 0)
		{
			if (ins[s[i] - 'a']) return false;
			sta.push(s[i] - 'a');
		}
		if (x == 1)
		{
			if (s[i] == 'n') be = 200;
			else if (s[i] >= '0' && s[i] <= '9') be = be * 10 + s[i] - '0';
		}
		if (x == 2)
		{
			if (s[i] == 'n') en = 200;
			else if (s[i] >= '0' && s[i] <= '9') en = en * 10 + s[i] - '0';
		}
		if (x >= 3) break;
	}
	comp += cl(be, en);
	return 1;
}

bool doit(int len)
{
	F--;
	if (!sta.empty())
	{
		ins[sta.top()] = false;
		sta.pop();
	}
	if (F < -1) return false;
	_max = max(_max, comp);
	return 1;
}

int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--)
	{
		int L;
		scanf("%d", &L);
		scanf("%s", s);
		int len = strlen(s);
		init();
		for (int i = 0; i < len; i++)
		{
			if (s[i] == 'n')
			{
				for (int j = i + 2; s[j] >= '0' && s[j] <= '9'; j++) flag = flag * 10 + s[j] - '0';
				break;
			}
			else if (s[i] == '1')
			{
				flag = 0;
				break;
			}
		}
		
		int ERR = 0;
		
		for (int i = 1; i <= L; i++)
		{
			scanf("%s", s);
			len = strlen(s);
			if (s[0] == 'F') {if (!solve(len)) ERR = 1;}
			if (s[0] == 'E') {if (!doit(len)) ERR = 1;}
		}
		if (F != -1) ERR = 1;
		if (ERR) puts("ERR");
		else if (_max == flag) puts("Yes");
		else puts("No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

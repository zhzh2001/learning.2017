#include<bits/stdc++.h>
using namespace std;
int t;
char s[1001];
int father[1001],num[1001],cnt,now,used[28],fac[1001],ans;
char loop[1001];
void dfs(int x,int y)
{
	int son=0;
	for (int i=1;i<=cnt;i++)
	if (father[i]==x)
	{
		dfs(i,x);
		if (fac[i]>son) son=fac[i];
	}
	if (num[x]==-1) fac[x]=son+1;
	else if (num[x]==1) fac[x]=son; 
	else fac[x]=0;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		memset(s,'\000',sizeof(s));
		memset(used,0,sizeof(used));
		int l;
		int k=0,flag=0;
		cnt=now=ans=0;
		int ansf=0,anse=0;
		loop[0]='z'+1;
		scanf("%d %s",&l,s);
		int len=strlen(s);
		if (s[2]=='1') k=0;
		else
		for (int i=4;i<=len-2;i++)
		k=k*10+s[i]-'0';
		getchar();
		while (l--)
		{
			char c;
			int x=0,y=0;
			scanf("%c",&c);
			if (c=='E') 
			{
				if (anse==ansf) 
				{
				  flag=1;
				}
			  used[loop[now]-'a']=0,now=father[now],getchar();
			  anse++;
			}
			if (c=='F') 
			{
				ansf++;
			  cnt++,father[cnt]=now,now=cnt;
			  scanf(" %c ",&c);
			  if (used[c-'a']==1) 
			  {
			  	flag=1;
				}
				loop[now]=c;
				used[c-'a']=1;
				scanf("%c",&c);
				if (c=='n') x=-1,getchar();
				else {while (c!=' ') x=x*10+c-'0',scanf("%c",&c);}
				scanf("%c",&c);
				if (c=='n') y=-1,getchar();
				else {while (c!='\n') y=y*10+c-'0',scanf("%c",&c);}
				if (x==-1&&y!=-1) num[now]=0;
				else if (x!=-1&&y==-1) num[now]=-1;
				else if (x<=y) num[now]=1;
				else num[now]=0;
		  }
		}
		if (ansf>anse||flag)
		{
			printf("ERR\n");
			continue;
		}
		for (int i=1;i<=cnt;i++)
		if (father[i]==0)
		dfs(i,0),ans=max(ans,fac[i]);
		if (ans==k) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

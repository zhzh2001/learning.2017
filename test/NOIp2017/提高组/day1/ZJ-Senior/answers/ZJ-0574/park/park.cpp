#include<bits/stdc++.h>
using namespace std;
struct node
{
	int from,to,next,value;
}mp[1000001];
int n,m,k,t,p,tot,cnt,flag,ans;
int head[100001];
int dis[100001];
int vis[100001];
int used[100001];
queue<int> q;
void addedge(int x,int y,int z)
{
	mp[++tot].from=x;mp[tot].to=y;mp[tot].next=head[x];mp[tot].value=z;head[x]=tot;
}
void spfa()
{
	q.push(1);
	dis[1]=0;
	for (int i=2;i<=n;i++)
	dis[i]=2e+9;
	memset(vis,0,sizeof(vis));
	vis[1]=1;
	while (!q.empty())
	{
		int u=q.front();
		q.pop();
		vis[u]=0;
		for (int i=head[u];i;i=mp[i].next)
		{
			int v=mp[i].to;
			if (dis[v]>dis[u]+mp[i].value)
			{
				dis[v]=dis[u]+mp[i].value;
				if (!vis[v]) q.push(v),vis[v]=1;
			}
		}
	}
}
void dfs(int x,int sum)
{
	used[x]++;
	if (used[x]>m*2)
	{
		flag=1;
		return;
	}
	if (x==n) {ans++;if (ans>=p) ans-=p;}
	for (int i=head[x];i;i=mp[i].next)
	{
		if (mp[i].value+sum>cnt+k) continue;
		dfs(mp[i].to,sum+mp[i].value);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		ans=0;
		flag=0;
		tot=0;
		memset(head,0,sizeof(head));
		memset(used,0,sizeof(used));
	  scanf("%d%d%d%d",&n,&m,&k,&p);
	  if (n>=50000) {printf("-1\n");continue;}
	  for (int i=1;i<=m;i++)
	  {
	  	int x,y,z;
	  	scanf("%d%d%d",&x,&y,&z);
	  	addedge(x,y,z);
		}
		spfa();
		cnt=dis[n];
		dfs(1,0);
		if (flag==1) printf("-1\n");
		else 
		printf("%d\n",ans);
	}
	return 0;
}

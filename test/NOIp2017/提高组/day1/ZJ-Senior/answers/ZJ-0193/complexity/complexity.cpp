#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#include<vector>
#include<string>
#include<bitset>
#include<queue>
#include<cstdlib>
#include<set>
#include<map>
#define ll long long
#define y0 sdbjahffffdf
#define y1 sdkhfss
#define x0 askfhgs
#define x1 dfkhs
using namespace std;
inline void rd(int &res){
	res=0;char c;
	while(c=getchar(),c<48);
	do res=(res<<1)+(res<<3)+(c^48);
	while(c=getchar(),c>=48);
}
void pt(int x){
	if(!x)return ;
	pt(x/10);
	putchar((x%10)^48);
}
void sc(int x){
	if(x<0){putchar('-');x=-x;}
	if(x)pt(x);
	else putchar('0');
	putchar('\n');
}
inline void Max(int &x,int y){if(x<y)x=y;}
inline void Min(int &x,int y){if(x>y)x=y;}
const int M=105;
char com[M],s[M],t[M],L[M],R[M];//数字<=100 
int num(char str[],int l,int r){
	int res=0,i;
	for(i=l;i<=r;i++)res=res*10+(str[i]-'0');
	return res;
}
int stk[M],mark[M],cnt[M],valid[M];
void solve(){
	int i,j,k,ok=1;
	int n,w,top=0,mx=0,err=0,now=0;
	scanf("%d %s",&n,com);//n表示行数 
	if(com[2]=='1')w=0;
	else w=num(com,4,strlen(com)-2);
	for(i=0;i<26;i++)mark[i]=0;
	valid[0]=1;
	for(i=1;i<=n;i++){
		scanf("%s",s);
		if(s[0]=='E'){
			if(top<=0)err=1;
			else{
				now-=cnt[top];
				ok=valid[top-1];
				mark[stk[top]]--;//栈里存的是变量的字符 
				top--;
			}
			continue;
		}
		scanf("%s",t);
		if(mark[t[0]-'a'])err=1;
		mark[t[0]-'a']++;
		stk[++top]=t[0]-'a';
		scanf("%s %s",L,R);
		if(L[0]!='n'&&R[0]=='n'){
			if(ok){
				now=now+1;
				mx=max(mx,now);
				valid[top]=1;
				cnt[top]=1;
			}
			else valid[top]=cnt[top]=0;
		}
		else if(L[0]=='n'&&R[0]!='n'){
			cnt[top]=0;ok=0;
			valid[top]=0;
		}
		else{
			int l=num(L,0,strlen(L)-1),r=num(R,0,strlen(R)-1);
			if(l>r)valid[top]=0,ok=0,cnt[top]=0;
			else valid[top]=ok,cnt[top]=0;
		}
	}
//	printf("hey %d %d\n",mx,w);
	if(top!=0)err=1;
	for(i=0;i<=n;i++)cnt[i]=0;
	if(err)puts("ERR");
	else if(mx==w)puts("Yes");
	else puts("No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--){
		solve();
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#include<vector>
#include<string>
#include<bitset>
#include<queue>
#include<cstdlib>
#include<set>
#include<map>
#define ll long long
#define y0 sdbjahffffdf
#define y1 sdkhfss
#define x0 askfhgs
#define x1 dfkhs
using namespace std;
inline void rd(int &res){
	res=0;char c;
	while(c=getchar(),c<48);
	do res=(res<<1)+(res<<3)+(c^48);
	while(c=getchar(),c>=48);
}
void pt(int x){
	if(!x)return ;
	pt(x/10);
	putchar((x%10)^48);
}
void sc(int x){
	if(x<0){putchar('-');x=-x;}
	if(x)pt(x);
	else putchar('0');
	putchar('\n');
}
inline void Max(int &x,int y){if(x<y)x=y;}
inline void Min(int &x,int y){if(x>y)x=y;}
int n,m,K,P;
const int oo=1e9+6;
void Add(int &x,int y){x+=y;if(x>=P)x-=P;}

const int M=1e5+5,N=55;
int seq[M],cnt[M][N],mark[M],dis[M],top[M];//1000w
int to[M<<1],w[M<<1],head[M],ec=1,nxt[M<<1];
struct node{
	int x,v;
	bool operator<(const node &tmp)const{
		return v>tmp.v;
	}
}A[M*N];
priority_queue<node>Q;
void init(){
	int i,j,k;
	ec=1;
	for(i=1;i<=n;i++){
		head[i]=0;
		for(j=0;j<=K;j++)cnt[i][j]=0;
	}
}
void ins(int a,int b,int c){
	to[ec]=b;nxt[ec]=head[a];w[ec]=c;head[a]=ec++;
}
void dij1(){
	int i,j,k,x,y,tot=0;
	for(i=1;i<=n;i++){
		mark[i]=0;
		dis[i]=oo;
	}
	dis[1]=0;
	while(!Q.empty())Q.pop();
	Q.push((node){1,0});
	while(!Q.empty()){
		node now=Q.top();Q.pop();
		if(mark[now.x])continue;
		//if(mark[n]&&now.v>dis[n]+K)break;
		x=now.x;
		seq[++tot]=x;
		mark[x]=1;
		for(i=head[x];i;i=nxt[i]){
			y=to[i];
			if(!mark[y]&&dis[y]>dis[x]+w[i]){
				dis[y]=dis[x]+w[i];
				Q.push((node){y,dis[y]});
			}
		}
	}
}
void work(){
	int y,add,val=dis[seq[1]],i=1,j,k,x,l=1,r=1,tot=0;
	for(i=1;i<=n;i++)top[i]=0;
	while(r<n&&dis[seq[r+1]]==val)r++;
	while(top[n]<=K){
		while(l<=r&&(r==n||val<dis[seq[r+1]])){
			for(i=l;i<=r;i++){
				A[++tot]=(node){seq[i],top[i]+dis[seq[i]]};
				top[i]++;
			}
			while(l<=r&&top[l]>K)l++;
			if(l<=r)val++;//top表示下一个要的 
			else {break;}
		}
		if(l>r||(r<n&&val==dis[seq[r+1]]))r++,val=dis[seq[r+1]];
	}
	cnt[1][0]=1;
	for(i=1;i<=tot;i++){
		x=A[i].x;
		add=A[i].v-dis[x];
		if(!cnt[x][add])continue;
		for(j=head[x];j;j=nxt[j]){
			y=to[j];
			int tmp=A[i].v+w[j]-dis[y];
			if(tmp<=K)Add(cnt[y][tmp],cnt[x][add]);
		}
	}
}
/*void work(){
	int x,y,tot=0,i,j,k,add=0;
	for(i=0;i<=K;i++)Q.push((node){1,dis[seq[1]]+i});
	while(!Q.empty()){
		node now=Q.top();Q.pop();
		add=now.v-dis[seq[now.x]];
		A[++tot]=(node){seq[now.x],now.v};
		if(now.x<n)Q.push((node){now.x+1,dis[seq[now.x+1]]+add});
	}
	cnt[1][0]=1;
	for(i=1;i<=tot;i++){
//			printf("%d %d\n",A[i].x,A[i].v);
		x=A[i].x;
		add=A[i].v-dis[x];
		if(!cnt[x][add])continue;
		for(j=head[x];j;j=nxt[j]){
			y=to[j];
//				if(y==n)printf("%d")
			int tmp=A[i].v+w[j]-dis[y];
			if(tmp<=K)Add(cnt[y][tmp],cnt[x][add]);
		}
//			printf("%d %d %d\n",x,A[i].v,cnt[x][add]);
	}
}*/
void solve(){
	int i,j,k,a,b,c,res=0;
	for(i=1;i<=m;i++){
		rd(a),rd(b),rd(c);
		ins(a,b,c);
	}
	dij1();
	work();
	for(i=0;i<=K;i++)Add(res,cnt[n][i]);
	printf("%d\n",res);
	init();
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas;
	rd(cas);
	while(cas--){
		rd(n),rd(m),rd(K),rd(P);
		solve();
	}
	return 0;
}

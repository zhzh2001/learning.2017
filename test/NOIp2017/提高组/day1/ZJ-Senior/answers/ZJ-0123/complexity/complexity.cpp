#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>

using namespace std;

#define N 1000

int t,n,top,stk2[N];
char s[N],ss[N],stk[N];
bool use[300];

int check(char ss[]){
	if (ss[5]=='n'){
		if (ss[7]=='n') return 0;
		else return -1;
	}
	else{
		int i=5,tmp=0,tmp2=0;
		for (;ss[i]>='0' && ss[i]<='9';i++) tmp=tmp*10+ss[i]-'0';
		if (ss[++i]=='n') return 1;
		for (;ss[i]>='0' && ss[i]<='9';i++) tmp2=tmp2*10+ss[i]-'0';
		if (tmp<=tmp2) return 0;
		else return -1;
	}
	return -1;
}

int main(){
	freopen("complexity.in","r",stdin); freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d%s",&n,s+1); gets(ss+1);
		bool err=0;
		int ans=0; stk[0]=0; top=1;
		memset(use,0,sizeof use);
		for (int i=1;i<=n;i++){
			gets(ss+1);
			if (ss[1]=='F'){
				char ch=ss[3];
				if (use[ch]) err=1;
				use[ch]=1;
				stk[top]=ch;
				int chk=check(ss);
				if (stk2[top-1]==-1 || chk==-1) stk2[top]=-1;
				else stk2[top]=stk2[top-1]+chk;
				ans=max(ans,stk2[top]);
				top++;
			}
			else{
				use[stk[--top]]=0;
			}
		}
		top--;
		if (top!=0 || err){puts("ERR"); continue;}
		if (s[3]=='1'){
			if (ans==0) puts("Yes"); else puts("No");
		}
		else{
			int i=5,tmp=0;
			for (;s[i]>='0' && s[i]<='9';i++) tmp=tmp*10+s[i]-'0';
			if (tmp==ans) puts("Yes"); else puts("No");
		}
	}
	
	return 0;
}
						

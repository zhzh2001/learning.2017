#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

using namespace std;

#define N 500000
#define nxt(x) ((++(x))%n)

int t,n,m,lim,p,x,y,z,tot,ans,a[N],dist[N],q[N],head[N],f[N][60];
bool vis[N];
struct edge{int v,d,nxt;}e[N];

int getint(){
	char ch; int sum=0;
	for (ch=getchar();ch<'0' || ch>'9';ch=getchar());
	for (;ch>='0' && ch<='9';ch=getchar()) sum=sum*10+ch-'0';
	return sum;
}

bool cmp(int x,int y){return dist[x]<dist[y];}

void add(int x,int y,int z){e[++tot].v=y; e[tot].d=z; e[tot].nxt=head[x]; head[x]=tot;}

void init(){
	tot=0; memset(e,0,sizeof e); memset(head,0,sizeof head);
	memset(dist,0x3f,sizeof dist); memset(vis,0,sizeof vis);
	memset(f,0,sizeof f); memset(a,0,sizeof a);
}

void spfa(){
	dist[1]=0; vis[1]=1;
	int tt=0,ww=1; q[1]=1;
	while (tt!=ww){
		int u=q[nxt(tt)];
		for (int i=head[u],v;i;i=e[i].nxt){
			v=e[i].v;
			if (dist[v]>dist[u]+e[i].d){
				dist[v]=dist[u]+e[i].d;
				if (!vis[v]){vis[v]=1; q[nxt(ww)]=v;}
			}
		}
		vis[u]=0;
	}
}

void dp(){
	int i=0; f[1][0]=1;
	for (int l=1;l<=n;l++){
		i=max(i,dist[a[l]]);
		for (;i<=dist[a[l]]+lim;i++)
			for (int j=l;j<=n;j++){
				int jj=a[j];
				if (i<dist[jj]) break;
				for (int k=head[jj];k;k=e[k].nxt){
					int v=e[k].v;
					if (i+e[k].d-dist[v]>=0 && i+e[k].d-dist[v]<=lim)
						f[v][i+e[k].d-dist[v]]=(f[v][i+e[k].d-dist[v]]+f[jj][i-dist[jj]])%p;
				}
			}
	}
}

int main(){
	freopen("park.in","r",stdin); freopen("park.out","w",stdout);
	t=getint();
	while (t--){
		init();
		n=getint(); m=getint(); lim=getint(); p=getint();
		for (int i=1;i<=m;i++){
			x=getint(); y=getint(); z=getint();
			add(x,y,z);
		}
		spfa();
		for (int i=1;i<=n;i++) a[i]=i;
		sort(a+1,a+n+1,cmp);
		dp();
		ans=0; for (int i=0;i<=lim;i++) ans=(ans+f[n][i])%p;
		printf("%d\n",ans);
	}
	
	return 0;
}
			

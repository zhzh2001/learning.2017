#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define re(i,a,b) for(int i=(a);i<(b);i++)
ll x,y;
const int N=1e7+4;
void exgcd(ll a,ll b,ll&x,ll&y){
	if(!b){
		x=1;y=0;
		return;
	}
	exgcd(b,a%b,x,y);
	ll t=x;x=y;y=t-a/b*x;
}
int f[N];
ll gcd(ll a,ll b){if(!b)return a;return gcd(b,a%b);}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b;scanf("%lld%lld",&a,&b);
	ll x,y,o1,o2;x=y=0;
	exgcd(a,b,x,y);
	ll t=x/b;
	x-=t*b;y+=t*a;
	while(x<0)x+=b,y-=a;
	o1=x-1;
	while(x>0)x-=b,y+=a;
	o2=y-1;
	cout<<o1*a+o2*b-1<<endl;
/*
	rep(i,1,50){
	ll a,b;a=rand()%1000+1,b=rand()%1000+1;
	while(gcd(a,b)!=1)a=rand()%1000+1,b=rand()%1000+1;
	ll x,y,o1,o2;x=y=0;
	exgcd(a,b,x,y);
	while(x<0)x+=b,y-=a;
	o1=x-1;
	
	cout<<x<<" "<<y<<" "<<a*x+y*b<<endl;
	
	while(x>0)x-=b,y+=a;
	o2=y-1;
	cout<<o1*a+o2*b<<endl;
	
	{
	
		int res=0;memset(f,0,sizeof(f));
		f[0]=1;rep(i,0,1e6)if(f[i])f[i+a]=f[i+b]=1;else res=i;
		cout<<res<<endl;
	}
	}*/
}

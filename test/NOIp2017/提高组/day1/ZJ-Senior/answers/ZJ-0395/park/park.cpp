#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define re(i,a,b) for(int i=(a);i<(b);i++)
#define clr(a) memset(a,0,sizeof(a))
const int N=2e5+5,INF=1e9;
#define w1 first
#define pb push_back
#define w2 second
typedef pair<int,int>pa;
vector<pa>e[N],ed[N];
int dis[N],f[N][55],n,m,K,p,deg[N];
inline void ad(int&x,int y){x+=y;if(x>=p)x-=p;}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;scanf("%d",&T);
	rep(cas,1,T){
		scanf("%d%d%d%d",&n,&m,&K,&p);
		rep(i,1,n)ed[i].clear(),e[i].clear(),deg[i]=0;
		rep(i,1,m){
			int a,b,c;scanf("%d%d%d",&a,&b,&c);
			e[a].pb(pa(b,c));
		}
		rep(i,1,n)dis[i]=INF;dis[1]=0;
		priority_queue<pa,vector<pa>,greater<pa> >pq;
		pq.push(pa(0,1));
		while(!pq.empty()){
			int x=pq.top().w2,d=pq.top().w1,w1;pq.pop();
			if(dis[x]<d)continue;
			re(i,0,e[x].size()){
				int y=e[x][i].w1,v=e[x][i].w2;
				if(dis[y]>dis[x]+v)dis[y]=dis[x]+v,pq.push(pa(dis[y],y));
			}
		}
		rep(x,1,n)re(i,0,e[x].size()){
			int y=e[x][i].w1,v=e[x][i].w2;
			if(dis[y]-dis[x]==v){
				ed[x].pb(e[x][i]);
				deg[y]++;
				
				
				
			}
		}
		vector<int>ord;
		rep(i,1,n)if(deg[i]==0)ord.pb(i);
		re(i,0,ord.size()){
			int x=ord[i];
			re(i,0,ed[x].size()){
				int y=ed[x][i].w1,v=ed[x][i].w2;
				deg[y]--;if(!deg[y])ord.pb(y);
			}
		}
		int res=0;
		rep(i,0,n)rep(j,0,K)f[i][j]=0;f[1][0]=1;
		rep(o,0,K){
			re(i,0,n){
				int x=ord[i];
				re(i,0,e[x].size()){
					int y=e[x][i].w1,v=e[x][i].w2;
					if(dis[y]==dis[x]+v)ad(f[y][o],f[x][o]);
				}
			}
			re(i,0,n){
				int x=ord[i];
				re(i,0,e[x].size()){
					int y=e[x][i].w1,v=e[x][i].w2;
					if(dis[y]<dis[x]+v&&dis[x]+v-dis[y]+o<=K)ad(f[y][dis[x]+v-dis[y]+o],f[x][o]);
				}
			}
			ad(res,f[n][o]);
		}
		cout<<res<<endl;
	}
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define re(i,a,b) for(int i=(a);i<(b);i++)
#define clr(a) memset(a,0,sizeof(a))
ll x,y;
const int N=2000+4;
void gmax(int&x,int y){x=max(x,y);}
int f[N];
int st[N],ad[N];string nx[N];
int bigger(string a,string b){
	if(a.size()>b.size())return 1;
	if(b.size()>a.size())return 0;
	re(i,0,a.size()){
		if(a[i]>b[i])return 1;
		if(b[i]>a[i])return 0;
	}return 1;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	rep(cas,1,T){
		clr(st);clr(ad);
		rep(i,0,100)nx[i]="";
		int L,typ=0,res=0;scanf("%d",&L);
		string s;cin>>s;
		if(s[2]=='1')typ=0;else{
			int o=4;
			while(s[o]>='0'&&s[o]<='9')typ=typ*10+s[o]-'0',o++;
		}
		int top=0,bad=0;map<string,int>vis;
		rep(i,1,L){
			string opt;
			cin>>opt;
			if(opt[0]=='E'){
				if(bad)continue;
				if(top)vis[nx[top]]=0;
				top--;
				if(top<0)bad=1;
				continue;
			}
			top++;
			cin>>nx[top];
			if(!bad){
				if(vis[nx[top]])bad=1;
				vis[nx[top]]=1;
			}
			
			string x,y;int xn=0,yn=0;
			cin>>x>>y;
			if(!bad){
				if(x.size()==1&&x[0]=='n')xn=1;
				if(y.size()==1&&y[0]=='n')yn=1;
				if(!xn&&!yn){
					if(bigger(y,x))ad[top]=0;
					else ad[top]=-1;
				}
				if(xn&&yn)ad[top]=0;
				if(!xn&&yn)ad[top]=1;
				if(xn&&!yn)ad[top]=-1;
				if(ad[top]!=-1)st[top]=st[top-1]+ad[top];else st[top]=-1;
				if(st[top-1]==-1)st[top]=-1;
				gmax(res,st[top]);
			}
		}
		if(top)bad=1;
		if(bad)puts("ERR");else{
			if(typ==res)puts("Yes");else puts("No");
		}
	}
}

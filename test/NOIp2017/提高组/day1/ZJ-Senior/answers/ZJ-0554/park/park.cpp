#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#define M 100005
#define A first
#define B second
#define pii pair<int,int>
#define mp make_pair
using namespace std;
void read(int &x){
	x=0; char c=getchar();
	for (;c<48;c=getchar());
	for (;c>47;c=getchar())x=(x<<3)+(x<<1)+(c^48);
}
struct ed{int x,l,nx;}e[M<<4];
int ecnt,nx[M],n,m,mo,K;
int dis[M],cnt[M][55],di[M];
bool ling[M],can[M][55];
inline void added(int x,int y,int l){
	e[ecnt]=(ed){y,l,nx[x]};
	nx[x]=ecnt++;	
}
struct AAA{
	int tot[M];
	bool mark[M];
	priority_queue< pii >q;
	void dij(int x){
		memset(dis,63,sizeof(dis));
		memset(tot,0,sizeof(tot));
		memset(mark,0,sizeof(mark));
		dis[x]=0; tot[x]=1; q.push(mp(0,x));
		int i,to;
		for (;!q.empty();){
			x=q.top().B; q.pop(); 
			if (mark[x])continue;
			mark[x]=1;
			for (i=nx[x];i;i=e[i].nx){
				to=e[i].x;
				if (dis[to]==dis[x]+e[i].l)tot[to]=(tot[to]+tot[x])%mo;				
				if (dis[to]>dis[x]+e[i].l){
					tot[to]=tot[x]; dis[to]=dis[x]+e[i].l;
					q.push(mp(-dis[to],to));					
				}				
			}			
		}		
		printf("%d\n",tot[n]%mo);
	}
	void solve(){
		dij(1);		
	}	
}p30;
struct Tar{
	int dfn[M],low[M],tot,de[M],can[M][2];
	void Tarjan(int x){
		low[x]=dfn[x]=++tot;
		int i,to;
		for (i=nx[x];i;i=e[i].nx){
			to=e[i].x;
			if (!dfn[to]){
				if (e[i].l)de[to]=de[x]+1; else de[to]=de[x]; 
				Tarjan(to); 
				if (e[i].l==0)low[x]=min(low[x],low[to]);
			}
			else{
				if (e[i].l==0)low[x]=min(low[x],dfn[to]);
			}
		}
		if (low[x]<dfn[x])ling[x]=ling[low[x]]=1;
	}
	
	void solve(){
		memset(dfn,0,sizeof(dfn));
		memset(low,0,sizeof(low));
		memset(ling,0,sizeof(ling));
		tot=0;
		Tarjan(1);
//		printf("\n"); int i; 	for (i=1; i<=n; i++)if (ling[i])printf("%d ",i); printf("\n");
	}
}Tar;
struct CCC{
	bool mark[M];
	priority_queue< pii >q;
	void dij(int x){
		memset(dis,63,sizeof(dis));
		memset(cnt,0,sizeof(cnt));
		memset(mark,0,sizeof(mark));
		dis[x]=0; cnt[x][0]=1; q.push(mp(0,x));
		int i,to;
		for (;!q.empty();){
			x=q.top().B; q.pop(); 
			if (mark[x])continue;
			mark[x]=1;
			for (i=nx[x];i;i=e[i].nx){
				to=e[i].x;
				if (dis[to]==dis[x]+e[i].l)cnt[to][0]=(cnt[to][0]+cnt[x][0])%mo;				
				if (dis[to]>dis[x]+e[i].l){
					cnt[to][0]=cnt[x][0]; dis[to]=dis[x]+e[i].l;
					q.push(mp(-dis[to],to));					
				}				
			}			
		}		
	}
	
	bool chk(){
		int i,j,k,to,l;
		memset(can,0,sizeof(can));
		for (i=1; i<=n; i++)if (ling[i])can[i][0]=1;
		for (k=0; k<=K; k++){
			if (can[n][k])return 1;
			for (i=1; i<=n; i++)if (can[i][k]){
				for (j=nx[i];j;j=e[j].nx){
					to=e[j].x; l=e[j].l;
					if (k+dis[i]+l-dis[to]>K)continue;
					can[to][k+dis[i]+l-dis[to]]=1;					
				}				
			}
		}
		return can[n][K];
	}
	
	void sol(){
		int i,j,k,l,to;
		for (k=0; k<=K; k++){
			for (i=1; i<=n; i++){
				if (cnt[i][k]>0){
					for (j=nx[i];j;j=e[j].nx){
						to=e[j].x; l=e[j].l;
						if (k+dis[i]+l-dis[to]>K)continue;
						if (k+dis[i]+l==dis[to])continue;
						cnt[to][k+dis[i]+l-dis[to]]+=cnt[i][k];
						can[to][k+dis[i]+l-dis[to]]%=mo;				
					}	
				}		
//				printf("cnt[%d][%d]=%d\n",i,k,cnt[i][k]);
			}
		}		
	}
	
	void solve(){
		dij(1);  if(cnt[n][0]==0){printf("0\n"); return;}
		Tar.solve();
		if (chk()){printf("-1\n");return;}
		sol();
		int i,res=0;
		for (i=0; i<=K; i++)res=(res+cnt[n][i])%mo;
		printf("%d\n",res);
	}
}p100;
int main(){
//	printf("%.5f\n",(sizeof(p100)+sizeof(p30)+sizeof(Tar)+sizeof(e)+sizeof(nx)+sizeof(cnt)+sizeof(can)+sizeof(dis)+sizeof(ling))/1024.0/1024.0);
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int Ca,i,x,y,l;
	read(Ca);
	for (;Ca;Ca--){
		memset(nx,0,sizeof(nx));
		read(n); read(m); read(K); read(mo); ecnt=1; bool flag=1;
		for (i=1; i<=m; i++){read(x); read(y); read(l); if (l==0)flag=0; added(x,y,l);}
		if (flag&&K==0){p30.solve(); continue;}
		p100.solve();
	}
	return 0;
}

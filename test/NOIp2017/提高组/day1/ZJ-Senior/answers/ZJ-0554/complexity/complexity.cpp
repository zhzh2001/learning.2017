#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define M 505
using namespace std;
void read(int &x){
	x=0; char c=getchar();
	for (;c<48;c=getchar());
	for (;c>47;c=getchar()){
		x=(x<<3)+(x<<1)+(c^48);	
		if (c=='n'){x=500; return;}
	}
}
int n,l[M],r[M],op[M],st[M],now;
bool mark[M];
char s[10];

int chk(){
	bool flag,flag1;
	int res=0;
	if (l[now]>r[now])flag=0;else flag=1;
	if (l[now]<r[now]&&r[now]==500)flag1=1;else flag1=0;
	for (now++;op[now]==118;){
		res=max(res,chk());
		now++;
	}
	if (!flag)return 0;
	if (!flag1)return res;
	return res+1;	
}

void solve(){
	scanf("%d%s",&n,s);
	int i,top=0,tp;
	memset(mark,0,sizeof(mark));
	bool flag=1;
	for (i=1; i<=n; i++){
		read(op[i]);
		if (op[i]==118){// tp:the kind of bianliang
			read(tp); read(l[i]); read(r[i]);
			if (mark[tp]){flag=0; continue;}	
			mark[tp]=1; top++; st[top]=tp;
		}
		else{
			if (top==0){flag=0; continue;}
			mark[st[top]]=0; top--;			
		}
	}
	if ((!flag)||(top>0)){printf("ERR\n"); return;}
	int len=strlen(s),k=0;
	if (len==4)k=0;
	else {
		if (len>=6)k=s[4]-'0';
		if (len>=7)k=k*10+s[5]-'0';
		if (len>=8)k=k*10+s[6]-'0';
	}
	int num=0;
	for (now=1;now<n+1;now++)num=max(num,chk());
	if (num==k)printf("Yes\n");
	else printf("No\n");
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int Ca;
	scanf("%d",&Ca);
	for (;Ca;Ca--){
		solve();		
	}
	return 0;
}

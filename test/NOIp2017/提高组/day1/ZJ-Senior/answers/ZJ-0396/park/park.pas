var
  n,m,i,j,k,t,x,y,z,sum,tot,min,p:longint;
  tt:array[1..1010] of longint;
  a:array[1..1010,1..1010] of longint;
procedure dfs(x:longint);
  var i,j:longint;
  begin
    if x=n
      then
        begin
          if sum<min
            then
              begin
                min:=sum;
                tot:=0;
              end;
          if sum=min
            then tot:=tot+1;
        end
      else
    for i:=1 to n do
      if (tt[i]=0)and(a[x,i]>0)and(sum+a[x,i]<=min)
        then
          begin
            sum:=sum+a[x,i];
            tt[i]:=1;
            dfs(i);
            tt[i]:=0;
            sum:=sum-a[x,i];
          end;
  end;
begin
  assign(input,'park.in');
  reset(input);
  assign(output,'park.out');
  rewrite(output);
  readln(t);
  for i:=1 to t do
    begin
      readln(n,m,k,p);
      for j:=1 to m do
        begin
          readln(x,y,z);
          a[x,y]:=z;
          a[y,x]:=z;
        end;
      min:=maxlongint;
      tot:=0;
      dfs(1);
      writeln(tot mod p);
    end;
  close(input);
  close(output);
end.

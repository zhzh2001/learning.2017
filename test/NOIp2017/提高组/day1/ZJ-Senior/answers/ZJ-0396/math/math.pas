var
  n,i,j,k,t,sum,l:longint;
  a:array[0..10000010] of qword;
  tot,x,y,p,q,max:qword;
    procedure qsort(l,r: longint);
      var
         i,j,x,y: qword;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         repeat
           while a[i]<x do
            inc(i);
           while x<a[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];
                a[i]:=a[j];
                a[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           qsort(l,j);
         if i<r then
           qsort(i,r);
      end;
begin
  assign(input,'math.in');
  reset(input);
  assign(output,'math.out');
  rewrite(output);
  readln(x,y);
  if x>y
    then
      begin
        t:=x;
        x:=y;
        y:=t;
      end;
  l:=y div x+1;
  tot:=0;
  for i:=0 to 1000 do
    for j:=0 to 1000 div l do
      begin
        p:=i;
        q:=j;
        tot:=tot+1;
        if p*x+q*y>9223372036854775807
          then break;
        a[tot]:=p*x+q*y;
      end;
  qsort(1,tot);
  for i:=1 to 100000 do
    if a[i+1]-a[i]>1
      then max:=a[i+1]-1;
  writeln(max);
  close(input);
  close(output);
end.

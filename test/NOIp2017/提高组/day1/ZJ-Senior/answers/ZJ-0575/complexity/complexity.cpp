#include <algorithm>
#include <cstdio>
using namespace std;
#define LL long long
#define REP(a,b,c) for(int a=b;a<=c;++a)
#define RREP(a,b,c) for(int a=b;a>=c;--a)

char c;
int x[100],y[1000],z[1000],p[100];
int a,b,n,m,now,t,l,q,flag;

int dfs() {
	++a; int f=0,g,qaq=y[a];
	while(y[a+1]!=-1) {g=dfs(); if(g>f) f=g;} 
	if(qaq==0) f=0; if(qaq==2) ++f; 
	++a; return f;
}

int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t); 
	while(t--) {
		scanf("%d",&l); c='-'; while(c!='(') c=getchar(); c=getchar();
		if(c=='1') n=0; else {while(c<'0'||c>'9') c=getchar(); n=0; while(c>='0'&&c<='9') n=n*10+c-'0',c=getchar();}
		while(c!=')') c=getchar(); 
		REP(i,1,l) {
			c='-'; while(c!='E'&&c!='F') c=getchar();
			if(c=='E') y[i]=-1; 
			else {
				while(c<'a'||c>'z') c=getchar(); z[i]=c-'a';
				c='-'; while(c!='n'&&(c>'9'||c<'0')) c=getchar(); if(c=='n') a=10000; else {a=0; while(c>='0'&&c<='9') a=a*10+c-'0',c=getchar();}
				c='-'; while(c!='n'&&(c>'9'||c<'0')) c=getchar(); if(c=='n') b=10000; else {b=0; while(c>='0'&&c<='9') b=b*10+c-'0',c=getchar();}
				if(a>b) y[i]=0; else if(b-a>1000) y[i]=2; else y[i]=1;
			}
		} REP(i,0,25) x[i]=0; q=0; flag=1; m=0; now=0;
		REP(i,1,l) {  
			if(y[i]==-1) {if(q==0) {flag=0; break;} --x[p[q]]; --q;}
			if(y[i]>-1) {if(x[z[i]]) {flag=0; break;} ++x[z[i]]; p[++q]=z[i];}
		} if(q>0||flag==0) {printf("ERR\n"); continue;}
		a=0; while(a<l) {now=dfs(); if(m<now) m=now;} if(m!=n) printf("No\n"); else printf("Yes\n");
	} return 0;
}

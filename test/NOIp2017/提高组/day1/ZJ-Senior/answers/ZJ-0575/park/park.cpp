#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
#define LL long long
#define mem(a) memset(a,0,sizeof a)
#define REP(a,b,c) for(int a=b;a<=c;++a)
#define RREP(a,b,c) for(int a=b;a>=c;--a)

inline bool isdigit(char c) {return c>='0'&&c<='9';}

template<class T> inline void gn(T &x) {
	char c; while(!isdigit(c=getchar())) ;
	x=c-'0'; while(isdigit(c=getchar())) x=x*10+c-'0'; 
}

const int N=200000,M=300000;
int ed[M][2];
int fr[N],we[M],to[M],ne[M],yul[N],dao[N],dfs[N],ddfs[N];
int q[N],t,w,inq[N],gg,flag,mot[N],xu[N];
int dp[N][51];
int ss[N],e[N<<5],f[N<<5],g[N<<5],h[N<<5],mm[N];
int n,m,k,mo;
int a,b,c,d,ll,nn;

void go(int w) {
	yul[w]=1; int u=fr[w],v; mm[w]=1;
	while(u) {if(we[u]==0) {v=to[u]; if(dao[v]) {++mot[v]; if(yul[v]) {if(mm[v]) flag=1;} else go(v);}} u=ne[u];} mm[w]=0;
}
inline bool cmp(int a,int b) {return dfs[a]<dfs[b]||(dfs[a]==dfs[b]&&xu[a]<xu[b]);}
inline void check(int &a) {if(a>=mo) a-=mo;}

int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout); 
	int T; gn(T); gg=N; int omg;
	while(T--) {
		gn(n),gn(m),gn(k),gn(mo); mem(yul); mem(dao); mem(mot); mem(fr); mem(mm); omg=n;
		REP(i,1,m) gn(ed[i][0]),gn(ed[i][1]),gn(we[i]),to[i]=ed[i][0],ne[i]=fr[ed[i][1]],fr[ed[i][1]]=i;
		REP(i,1,n-1) dfs[i]=500000000,inq[i]=0; dfs[n]=0,inq[n]=1,q[1]=n,t=0,w=1;
		while(t!=w) {
			if((++t)==gg) t=0; a=q[t],inq[a]=0; 
			for(int u=fr[a],v;u;u=ne[u]) {v=to[u]; if(dfs[v]>dfs[a]+we[u]) {dfs[v]=dfs[a]+we[u]; if(!inq[v]) {inq[v]=1; if((++w)==gg) w=0; q[w]=v;}}}
		} REP(i,1,n) ddfs[i]=dfs[i]; mem(fr); REP(i,1,m) {to[i]=ed[i][1],ne[i]=fr[ed[i][0]],fr[ed[i][0]]=i;} 
		 REP(i,2,n) dfs[i]=500000000,inq[i]=0; dfs[1]=0,inq[1]=1,q[1]=1,t=0,w=1; 
		while(t!=w) {
			if((++t)==gg) t=0; a=q[t],inq[a]=0; 
			for(int u=fr[a],v;u;u=ne[u]) {v=to[u]; if(dfs[v]>dfs[a]+we[u]) {dfs[v]=dfs[a]+we[u]; if(!inq[v]) {inq[v]=1; if((++w)==gg) w=0; q[w]=v;}}}
		} REP(i,1,n) {if(dfs[i]+ddfs[i]<=k+dfs[n]) dao[i]=1; else dao[i]=0;} flag=0;
		REP(i,1,n) {if(dao[i]==0) continue; if(yul[i]==0) go(i); if(flag==1) {puts("-1"); break;}} 
		if(flag) continue; t=0; REP(i,1,n) if(mot[i]==0&&dao[i]==1) q[++t]=i; w=0;
		while(w<t) {a=q[++w],xu[a]=w,b=fr[a]; while(b) {c=to[b]; if(dao[c]&&we[b]==0) if((--mot[c])==0) q[++t]=c; b=ne[b];}} 
		m=0; REP(i,1,n) if(dao[i]==1) ss[++m]=i; n=m; sort(ss+1,ss+n+1,cmp); 
		m=0; REP(i,0,k) {REP(j,1,n) e[++m]=ss[j],f[m]=dfs[ss[j]]+i;} a=n;  
		while(a<m) { 
			for(b=0;b<m;b+=(a<<1)) {
				int u=b+1,uu=b+a,v=b+a+1,vv=b+a+a,w=b+1; if(uu>m) uu=m; if(vv>m) vv=m;
				while(u<=uu&&v<=vv) {if(f[u]<f[v]||(f[u]==f[v]&&xu[e[u]]<xu[e[v]])) {g[w]=e[u],h[w]=f[u]; ++w,++u;} else {g[w]=e[v],h[w]=f[v]; ++w,++v;}}
				while(u<=uu) {g[w]=e[u],h[w]=f[u]; ++w,++u;} while(v<=vv) {g[w]=e[v],h[w]=f[v]; ++w,++v;} 
			} REP(i,1,m) e[i]=g[i],f[i]=h[i]; a<<=1;
		} mem(dp); check(dp[1][0]=1); 
		REP(i,1,m) {
			a=e[i],b=f[i]-dfs[a],c=fr[a],nn=f[i]; if(dp[a][b]==0) continue;
			while(c) {
				d=to[c];
				if(dao[d]) {
					ll=nn+we[c]-dfs[d];  
					if(ll<=k) {check(dp[d][ll]+=dp[a][b]);}
				} c=ne[c];
			}
		} int ans=0; REP(i,0,k) check(ans+=dp[omg][i]); printf("%d\n",ans);
	} return 0;
}

var cnt,k,p,top,cishu,i,num1,t,num2,max,depth:longint;
    s,ss,x,y:string;
    ch:char;
begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);

  readln(cnt);
  for k:=1 to cnt do
  begin
    top:=0;max:=0;depth:=0;
    readln(s);
    p:=pos(' ',s);
    val(copy(s,1,p-1),t);
    delete(s,1,p);
    if pos('n',s)<>0 then
      if pos('^',s)<>0 then
        val(copy(s,pos('^',s)+1,pos(')',s)-pos('^',s)-1),cishu)
      else cishu:=1
    else cishu:=0;
    for i:=1 to t do
    begin
      readln(ss);
      if ss[1]='F' then
      begin
        inc(top);
        delete(ss,1,2);
        ch:=ss[1];
        delete(ss,1,2);
        p:=pos(' ',ss);
        x:=copy(ss,1,p-1);
        y:=copy(ss,p+1,length(ss)-p);
        num1:=0;num2:=0;
        if pos('n',x)<>0 then
        begin
          p:=pos('^',x);
          if p=0 then num1:=1;
          if p<>0 then
          begin
            inc(p);
            while (p<=length(x)) and (x[p] in ['0'..'9']) do
            begin
              num1:=num1*10+ord(x[p])-48;
              inc(p);
            end;
          end;
        end;
        if pos('n',y)<>0 then
        begin
          p:=pos('^',y);
          if p=0 then num2:=1;
          if p<>0 then
          begin
            inc(p);
            while (p<=length(y)) and (y[p] in ['0'..'9']) do
            begin
              num2:=num1*10+ord(x[p])-48;
              inc(p);
            end;
          end;
        end;
        if num1<=num2 then inc(depth,num2-num1);
        if depth>max then max:=depth;
      end;
      if ss[1]='E' then
      begin
        dec(top);
        dec(depth,num2-num1);
      end;
    end;
    if max<>cishu then writeln('NO') else writeln('YES');
  end;

  close(input);close(output);
end.

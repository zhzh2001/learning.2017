#include<cstdio>
#include<cstring>

int dis[100001],diss[100001],T,x,y,z,hde[6000001],k,n,m,hd[100001],l,r,q[10000001],cnt,hdd[100001],cntt;
int p,f[6000001],ans,du[6000001];
bool bo[100001],b[100001],inq[100001],bb[6000001];

struct node
{
	int x,t,nxt,val;
}e[200001],ee[200001];

struct node2
{
	int t,nxt;
}ed[11000001];

int calc(int x,int y) {return (x-1)*(k+1)+y+1;}

int duru()
{
	int p=0; char c=getchar();
	while (c>'9'||c<'0') c=getchar();
	while (c>='0'&&c<='9') p=p*10+c-'0',c=getchar();
	return p;
}

void adde(int x,int y,int val)
{
	e[++cnt].nxt=hd[x];
	e[cnt].x=x;
	hd[x]=cnt;
	e[cnt].t=y;
	e[cnt].val=val;
}

void addee(int x,int y,int val)
{
	ee[++cntt].nxt=hdd[x];
	hdd[x]=cntt;
	ee[cntt].t=y;
	ee[cntt].val=val;
}

void added(int x,int y)
{
	ed[++cnt].nxt=hde[x];
	hde[x]=cnt;
	ed[cnt].t=y;
}

bool dfs(int x)
{
	if (bo[x]) return 1;
	bo[x]=1;
	for (int i=hd[x]; i; i=e[i].nxt)
		if (e[i].val==0&&b[e[i].t]) 
		{
			if (dfs(e[i].t)) return 1;
		}
	bo[x]=0;
	return 0;
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=duru();
	while (T--)
	{
		n=duru(); m=duru(); k=duru(); p=duru();
		memset(hd,0,sizeof(hd));
		memset(hdd,0,sizeof(hdd));
		cnt=0; cntt=0;
		for (int i=1; i<=m; i++) x=duru(),y=duru(),z=duru(),adde(x,y,z),addee(y,x,z);
		memset(inq,0,sizeof(inq));
		l=r=1; q[1]=1; inq[1]=1; dis[1]=0;
		for (int i=2; i<=n; i++) dis[i]=1e9;
		while (l<=r)
		{
			x=q[l];
			for (int i=hd[x]; i; i=e[i].nxt)
				if (dis[x]+e[i].val<dis[e[i].t])
				{
					dis[e[i].t]=dis[x]+e[i].val;
					if (!inq[e[i].t]) q[++r]=e[i].t,inq[e[i].t]=1;
				}
			inq[x]=0; l++;
		}
		memset(inq,0,sizeof(inq));
		l=r=1; q[1]=n; inq[n]=1; diss[n]=0;
		for (int i=1; i<n; i++) diss[i]=1e9;
		while (l<=r)
		{
			x=q[l];
			for (int i=hdd[x]; i; i=ee[i].nxt)
				if (diss[x]+ee[i].val<diss[ee[i].t])
				{
					diss[ee[i].t]=diss[x]+ee[i].val;
					if (!inq[ee[i].t]) q[++r]=ee[i].t,inq[ee[i].t]=1;
				}
			inq[x]=0; l++;
		}
		for (int i=1; i<=n; i++)
			if (dis[i]+diss[i]>dis[n]+k) b[i]=0; else b[i]=1;
		memset(bo,0,sizeof(bo));
		if (dfs(1)) {printf("-1\n"); continue;}
		cnt=0;
		memset(du,0,sizeof(du));
		memset(hde,0,sizeof(hde));
		for (int i=1; i<=m; i++) 
			if (b[e[i].t]&&b[e[i].x])
				for (int j=dis[e[i].x]+e[i].val-dis[e[i].t]; j<=k; j++)
					added(calc(e[i].x,j-dis[e[i].x]-e[i].val+dis[e[i].t]),calc(e[i].t,j));
		memset(f,0,sizeof(f));
		memset(bb,0,sizeof(bb));
		l=r=1; q[1]=calc(1,0); bb[calc(1,0)]=1;
		while (l<=r)
		{
			x=q[l];
			for (int i=hde[x]; i; i=ed[i].nxt)
				if (!bb[ed[i].t]) bb[ed[i].t]=1,q[++r]=ed[i].t;
			l++;
		}
		for (int i=1; i<=m; i++) 
			if (b[e[i].t]&&b[e[i].x])
				for (int j=dis[e[i].x]+e[i].val-dis[e[i].t]; j<=k; j++)
					if (bb[calc(e[i].x,j-dis[e[i].x]-e[i].val+dis[e[i].t])])
						du[calc(e[i].t,j)]++;
		l=r=1; q[1]=calc(1,0); f[calc(1,0)]=1;
		while (l<=r)
		{
			x=q[l];
			for (int i=hde[x]; i; i=ed[i].nxt) 
			{
				f[ed[i].t]=(f[ed[i].t]+f[x])%p; du[ed[i].t]--;
				if (du[ed[i].t]==0) q[++r]=ed[i].t;
			}
			l++;
		}
		ans=0;
		for (int i=0; i<=k; i++) ans=(ans+f[calc(n,i)])%p;
		printf("%d\n",ans);
	}
	return 0;
}

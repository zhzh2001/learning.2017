#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

const int N=100035;

int ans,mn,cas,n,m,K,P;
int head[N],head2[N],g[N],cnt;

struct Edge {
	int to,nxt,val;
}E[N<<2];

void Link(int u,int v,int w) {
	E[++cnt]=(Edge){v,head[u],w},head[u]=cnt;
}
void Link2(int u,int v,int w) {
	E[++cnt]=(Edge){v,head2[u],w},head2[u]=cnt;
}

void spfa() {
	memset(g,63,sizeof g);
	static int h,t,Q[1000005],vis[N];
	h=0,Q[t=1]=n,g[n]=0,vis[n]=1;
	while (h<t) {
		int x=Q[++h];
		for (int i=head2[x];i;i=E[i].nxt) {
			int y=E[i].to;
			if (g[y]>g[x]+E[i].val) {
				g[y]=g[x]+E[i].val;
				if (!vis[y]) Q[++t]=y,vis[y]=1;
			}
		}
		vis[x]=0;
	}
	return;
}

struct Info {
	int a,key,len;
	bool operator < (const Info &p) const {
		return key>p.key;
	}
}x;

priority_queue<Info>Q;

void A_star() {
	ans=0,mn=g[1];
	while (!Q.empty()) Q.pop();
	Q.push((Info){1,g[1],0});
	
	int y,nk;
	while (!Q.empty()) {
		x=Q.top(),Q.pop();
		if (x.a==n) {
			if (x.len<=mn+K)
				 ans++;
			else return;
		}
		for (int i=head[x.a];i;i=E[i].nxt) {
			y=E[i].to,nk=x.len+E[i].val+g[y];
			if (nk>mn+K) continue;
			Q.push((Info){y,nk,x.len+E[i].val});
		}
	}
}

int rd() {
	char c=getchar(); int t=0;
	while (c<'0' || c>'9') c=getchar();
	while (c>='0' && c<='9') t=(t<<1)+(t<<3)+c-48,c=getchar();
	return t;
}

int main() {
	
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	for (scanf("%d",&cas);cas--;) {
		cnt=0;
		memset(head,0,sizeof head);
		memset(head2,0,sizeof head2);
		scanf("%d%d%d%d",&n,&m,&K,&P);
		for (int x,y,z,i=1;i<=m;i++) {
//			scanf("%d%d%d",&x,&y,&z);
			x=rd(),y=rd(),z=rd();
			Link(x,y,z);
			Link2(y,x,z);
		}
		
		spfa();
		A_star();
		
		printf("%d\n",ans%P);
	}
	return 0;
}
/*
1
5 8 10 100000
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
5 3 0

8858609

*/

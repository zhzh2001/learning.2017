#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

const int N=10035;

char s[105],x[105],y[105];
int cas,L,cnt,goal,len,in[N],key[N],id[N],ch[N],top,flg;

int count() {
	int sum=0;
	for (int i=1;i<=top;i++)
		sum+=key[i];
	return sum;
}

int Num(char *s) {
	int len=strlen(s),t=0;
	for (int i=0;i<len;i++)
		if (s[i]>='0' && s[i]<='9') t=t*10+s[i]-48;
	return t;
}

int main() {
	
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	scanf("%d",&cas);
	while (cas--) {
		scanf("%d",&L);
		scanf("%s",s),len=strlen(s);
		if (len==4) goal=0;
		else goal=Num(s);
		
		cnt=0,flg=1;
		for (int i=1;i<=L;i++) {
			scanf("%s",s);
			if (s[0]=='F') {
				scanf("%s",s);
				if (in[s[0]]) flg=0; in[s[0]]++;
				
				top++,id[top]=i,ch[top]=s[0];
				scanf("%s",x),scanf("%s",y);
				if ((x[0]=='n' && y[0]=='n') || (x[0]!='n' && y[0]!='n' && Num(x)<=Num(y))) key[top]=0;
				else if (y[0]=='n') key[top]=1;
				else key[top]=-1000;
			}
			else {
				if (top==0) { flg=0; continue; }
				cnt=max(cnt,count());
				id[top]=key[top]=0,in[ch[top]]--,ch[top]=0,top--;
			}
		}
		if (top) flg=0;
		while (top) id[top]=key[top]=0,in[ch[top]]--,ch[top]=0,top--;
		
		if (!flg) puts("ERR");
		else if (cnt==goal) puts("Yes");
		else puts("No");
	}
	return 0;
}
/*
4
14 O(n^3)
F i 1 n
F j 2 1
E
F j 1 2
F k 1 n 
F l 33 n
E
E
E
F j 2 1
F k 1 2
E
E
E
12 O(n^2)
F i 1 n
F j 2 1
E
F j 1 2
F k 1 n 
F l 33 n
E
F j 2 1
F k 1 2
E
E
E
14 O(n^1)
F i 1 n
F j 2 1
E
F j 1 2
F k 1 n 
F l 33 n
E
E
E
F j 2 1
F k 1 2
E
E
E
14 O(n^3)
F i 1 n
F j 2 1
E
F j 1 2
F k 1 n 
F l 33 n
E
E
E
F j 2 1
F k 1 2
E
E
E

*/

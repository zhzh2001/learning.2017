#include<cstdio>
#include<algorithm>

using namespace std;

typedef long long LL;

int a,b; LL Ans;

void wt(LL x) {
	if (x<=9) putchar(x+48);
	else wt(x/10),putchar(x%10+48);
}

int main() {
	
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	scanf("%d%d",&a,&b);
	
		if (a>b) swap(a,b);
		if (a==1) Ans=0;
		else if (a==2) Ans=b-2;
		else Ans=1LL*(a-1)*(b-2)+a-2;
		wt(Ans),puts("");
	
	return 0;
}

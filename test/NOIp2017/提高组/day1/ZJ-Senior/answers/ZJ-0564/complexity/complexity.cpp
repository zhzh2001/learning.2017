#include <cstdio>
#include <map>
using namespace std;

  map <int,int> mem;
  int inp[201][4],errflag,tpo,T,lin,givnans;
  char st[201];
  
  int ana(char st[],int po){
  	int ret=0;
  	while (st[po]>='0'&&st[po]<='9') ret*=10,ret+=st[po]-'0',po++;
  	return(ret);
  }
  
  int solve(int po){
    if (mem[inp[po][1]]) errflag=1;
  	mem[inp[po][1]]++;
  	int ret=0;tpo=po+1;
  	while(inp[tpo][0]==0)
  	  ret=max(ret,solve(tpo));
  	tpo++;
  	mem[inp[po][1]]--;
	    	
  	if (inp[po][2]>inp[po][3]) return(0);
  	if (inp[po][3]-inp[po][2]>200) return(ret+1);else return(ret);
  }
  
  int main(){
  	freopen("complexity.in","r",stdin);
  	freopen("complexity.out","w",stdout);
  	
  	scanf("%d",&T);
  	while (T--){
  	  scanf("%d",&lin);
	  scanf("%s",&st);if (st[2]=='1') givnans=0;else givnans=ana(st,4);
	  mem.clear();
	  
	  errflag=0;
	  int fcnt=0;
	  for (int i=1;i<=lin;i++){
	  	scanf("%s",&st);
	  	if (st[0]=='F'){
	  	  fcnt++;
	  	  inp[i][0]=0;	
	  	  scanf("%s",&st);inp[i][1]=st[0];
	  	  scanf("%s",&st);inp[i][2]=(st[0]=='n' ? 2333:ana(st,0));
	  	  scanf("%s",&st);inp[i][3]=(st[0]=='n' ? 2333:ana(st,0));
		}else{
		  fcnt--;
		  if (fcnt<0) errflag=1;
		  inp[i][0]=1;	
		}
	  }
	  
	  if ((errflag)||(fcnt!=0)){
	  	printf("ERR\n");continue;
	  }
	  
	  inp[0][0]=0;inp[0][1]=-1;inp[0][2]=1;inp[0][3]=1;
	  inp[lin+1][0]=1;
	  int ans=solve(0);
	  if (errflag){
	    printf("ERR\n");continue;	
	  }
	  if (ans==givnans) printf("Yes\n");else printf("No\n");
	}
  }

#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>
#define LL long long 
using namespace std;

  int dfn[110001],low[110001],cnt,inst[110001],sta[110001],top,nd[110001],nxt[410001],des[410001],len[410001];
  int inzero[110001],bt[110001],head,tail,deg[110001],dl[110001],n,T,m,k,sid[410001][3],mayb[110001];
  int dis1[110001],dis2[110001],indl[110001];
  LL mo,dp[110001][55];
  int ddll[11000001],inddll[110001];

  struct data{
  	int po,num;
  	
  	inline bool operator < (const data&b) const{
	  return(b.num<num);  
	}
  };

  priority_queue <data> heap;

  void cleargraph(){
    memset(nd,-1,sizeof(nd));cnt=0;
  }

  void tarjan(int po){
    dfn[po]=low[po]=++cnt;
    inst[sta[++top]=po]=1;
    for (int p=nd[po];p!=-1;p=nxt[p])
      if (!dfn[des[p]]){
      	tarjan(des[p]);
      	low[po]=min(low[po],low[des[p]]);
	  }else
	  if (inst[des[p]])
	    low[po]=min(low[po],dfn[des[p]]);
	
	if (dfn[po]==low[po]){
	  int t=0;if (sta[top]!=po) t=1;
	  while (sta[top]!=po) inzero[sta[top]]=t,inst[sta[top--]]=0;
	  inzero[sta[top]]=t;inst[sta[top--]]=0;
	}
  }
  
  void dij(int dis[],int fr){
	for (int i=1;i<=n;i++) dis[i]=1e9;
	dis[fr]=0;inddll[fr]=1;ddll[head=tail=1]=fr;
	while (head<=tail){
	  for (int p=nd[ddll[head]];p!=-1;p=nxt[p])	
	    if (dis[des[p]]>dis[ddll[head]]+len[p]){
	      dis[des[p]]=dis[ddll[head]]+len[p];
	      if (!inddll[des[p]])
	        inddll[ddll[++tail]=des[p]]=1;
		}
	  inddll[ddll[head++]]=0;
	}
  }
 
  void topo(){
  	head=1;tail=0;
  	for (int i=1;i<=n;i++) if ((!deg[i])&&(mayb[i])) dl[++tail]=i;
  	while (head<=tail){
  	  for (int p=nd[dl[head]];p!=-1;p=nxt[p])
  	    if (--deg[des[p]]==0)
  	      dl[++tail]=des[p];
	  head++;
	}
  } 
 
  void addedge(int x,int y,int z=1){
  	nxt[++cnt]=nd[x];des[cnt]=y;len[cnt]=z;nd[x]=cnt;
  }
 
  inline int read(){
  	int ret=0;char ch=getchar(); 
	while (!(ch>='0'&&ch<='9')) ch=getchar(); 
	while (ch>='0'&&ch<='9') ret*=10,ret+=ch-'0',ch=getchar();
	return(ret);
  }
 
  int main(){
  	freopen("park.in","r",stdin);
  	freopen("park.out","w",stdout);
  	
  	scanf("%d",&T);
  	while (T--){
  	  scanf("%d%d%d%lld",&n,&m,&k,&mo);
  	  for (int i=1;i<=m;i++) sid[i][0]=read(),sid[i][1]=read(),sid[i][2]=read();
  	  
  	  cleargraph();
  	  memset(inzero,0,sizeof(inzero));
  	  memset(dfn,0,sizeof(dfn));
  	  memset(low,0,sizeof(low));
  	  memset(mayb,0,sizeof(mayb));
  	  memset(dp,0,sizeof(dp));
  	  
  	  for (int i=1;i<=m;i++) if (sid[i][2]==0) addedge(sid[i][0],sid[i][1]);
  	  cnt=0;for (int i=1;i<=n;i++) if (!dfn[i]) tarjan(i);
  	  
  	  cleargraph();
  	  for (int i=1;i<=m;i++) addedge(sid[i][0],sid[i][1],sid[i][2]);
  	  dij(dis1,1);
  	  
  	  cleargraph();
  	  for (int i=1;i<=m;i++) addedge(sid[i][1],sid[i][0],sid[i][2]);
  	  dij(dis2,n);
  	  
  	  int flag=0;
  	  for (int i=1;i<=n;i++) {
  	  	if (dis1[i]+dis2[i]<=dis1[n]+k) mayb[i]=1;
  	  	if (mayb[i]&&inzero[i]) flag=1;
	  }
	  
	  if (flag){printf("-1\n");continue;}
	  
	  cleargraph();
	  for (int i=1;i<=m;i++) if (dis1[sid[i][1]]-dis1[sid[i][0]]==sid[i][2]&&mayb[sid[i][0]]&&mayb[sid[i][1]]){
	  	deg[sid[i][1]]++;addedge(sid[i][0],sid[i][1]);
	  }
	  topo();
	  
	  cleargraph();
	  for (int i=1;i<=m;i++)addedge(sid[i][0],sid[i][1],sid[i][2]);
	  
	  dp[1][0]=1;
	  for (int i=0;i<=k;i++){
	  	for (int j=1;j<=tail;j++)
	  	  if (mayb[dl[j]]&&dp[dl[j]][i])
	  	    for (int p=nd[dl[j]];p!=-1;p=nxt[p]) 
 	  	      if (i+sid[p][2]-(dis1[sid[p][1]]-dis1[sid[p][0]])<=k&&mayb[des[p]]){
	  	        dp[des[p]][i+sid[p][2]-(dis1[sid[p][1]]-dis1[sid[p][0]])]+=dp[dl[j]][i];
	  	        if (dp[des[p]][i+sid[p][2]-(dis1[sid[p][1]]-dis1[sid[p][0]])]>mo)
	  	          dp[des[p]][i+sid[p][2]-(dis1[sid[p][1]]-dis1[sid[p][0]])]-=mo;
			  }
	  } 
	  
	  LL ret=0;
	  for (int i=0;i<=k;i++) ret+=dp[n][i],ret%=mo;ret+=mo;ret%=mo;
	  printf("%lld\n",ret);
	}
  }

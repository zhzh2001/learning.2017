#include <bits/stdc++.h>
using namespace std;

#define maxn 100005
#define maxm 100005
#define MOD 1000000007
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)++)
#define fi first
#define se second
#define mp make_pair
#define pb push_back
typedef long long ll;
/*-------------------- split line ----------------------*/
ll a, b, ans;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a, &b);
	printf("%lld\n", a * b - a - b);
}


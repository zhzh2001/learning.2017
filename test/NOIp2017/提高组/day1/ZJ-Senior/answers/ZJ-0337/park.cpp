#include <bits/stdc++.h>
using namespace std;

#define maxn 100005
#define maxm 200005
#define MOD 1000000007
#define INF 2000000000
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)++)
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define pii pair<int, int>
typedef long long ll;
/*-------------------- split line ----------------------*/
int n, m, k, sume = 0;
ll p;
int head = 1, tail = 1;
struct Edge{
	int to, v, nxt;
}e[maxm];
bool vis[maxn];
ll dis[maxn], num[maxn], d[5000005], ans;
int q[5000005];
int first[maxn];
//priority_queue<pii> q;
void addedge(int x, int y, int z){
	sume++; e[sume].to = y; e[sume].v = z; e[sume].nxt = first[x]; first[x] = sume;
}

void init(){
	memset(first, 0, sizeof(first));
	memset(e, 0, sizeof(e));
	memset(dis, 0x7f, sizeof(dis));
	//memset(dp, 0x7f, sizeof(dp));
	memset(d, 0, sizeof(d));
	memset(num, 0, sizeof(num));
	memset(vis, false, sizeof(vis));
	scanf("%d%d%d%lld",&n, &m, &k, &p);
	FORP(i, 1, m){
		int x, y, z; scanf("%d%d%d",&x, &y, &z);
		addedge(x, y, z);
	}
	
	dis[1] = 0; num[1] = 1;
}
void work1(){
	while (1){
		int mx = INF, x = 0;
		FORP(i, 1, n) if (dis[i] < mx && !vis[i]) mx = dis[i], x = i;
		if (x == 0) break;
		vis[x] = 1;
		for (int i = first[x]; i; i = e[i].nxt){
			int y = e[i].to;
			if (dis[x] + e[i].v == dis[y]) num[y] += num[x], num[y] %= p;
			else if (dis[x] + e[i].v < dis[y]) dis[y] = dis[x] + e[i].v, num[y] = num[x];
		}
	}
	ans = num[n];
}
void work2(){
	int head = 1, tail = 1; q[tail] = 1;  d[tail] = 0;
	while (head <= tail){
		
		int x = q[head]; if (x == n) ans ++, ans %= p;
		for (int i = first[x]; i; i = e[i].nxt){
			int y = e[i].to;
			if (d[head] + e[i].v <= dis[y] + k) q[++tail] = y, d[tail] = d[head] + e[i].v;
		}
		head++;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas; scanf("%d",&cas);
	while (cas--){
		init();
		work1(); 
		if (k == 0) {printf("%lld\n", ans);} 
		else {ans = 0; work2(); printf("%lld\n", ans);}
	}
}


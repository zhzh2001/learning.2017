#include <bits/stdc++.h>
using namespace std;

#define maxn 100005
#define INF 2000000000
#define MOD 1000000007
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)++)
#define fi first
#define se second
#define mp make_pair
#define pb push_back
typedef long long ll;
/*-------------------- split line ----------------------*/
char cpx[100];
string s[1005][10];

//!!!!!!!!!!!!!!!!!!!!!
//need to write another void to check if there is a gramma mistake
//*!!!!!!!!!!!!!!!!

int a[maxn], O;
bool ERR_flag, vis[maxn];
int numf, ord[maxn];
set<string> var;

int n;
int transform(int i, int j){
	if (s[i][j] == "n") return -1;
	int num = 0, len = s[i][j].length();
	for (int k = 0; k < len; k++) num = num * 10 + (s[i][j][k] - '0');
	return num;
	
}
int work(int dep, int l, int r){
	if (l + 1 == r){
		int ret = 0;
		if (var.find(s[l][2]) == var.end()){
			int x = transform(l, 3), y = transform(l, 4);
			if ((x == -1 && y == -1) || (x != -1 && y != -1)) return 0;
			if (y == -1) return 1; 
			if (x == -1 || x > y) return 0;
			
		}
		else ERR_flag = true;
		return ret;
	}
	int ret = 0;
	if (var.find(s[l][2]) == var.end()){
		int x = transform(l, 3), y = transform(l, 4);
		if (x == -1 && y == -1) ret = 0;
		else if (y == -1) ret = 1; 
			else if (x == -1 || x > y) return 0;
				else ret = 0;
		
	}
	else ERR_flag = true;
	var.insert(s[l][2]);
	int maxm = 0, fl = 0, fr = 0;
	for (int i = l + 1; i <= (r - 1); i++){
		if (ord[i] == dep + 1) 
			if (fl == 0) fl = i;
			else {fr = i; maxm = max(maxm, work(dep + 1, fl, fr)); fl = fr = 0;}
	}
	if (fl != fr) ERR_flag = true;
	var.erase(s[l][2]);
	return ret + maxm;
}
void check(int dep, int l, int r){
	vis[l] = vis[r] = true;
	if (var.find(s[l][2]) != var.end()) ERR_flag = true;
	if (l + 1 == r) return;
	
	var.insert(s[l][2]);
	int maxm = 0, fl = 0, fr = 0;
	for (int i = l + 1; i <= (r - 1); i++){
		if (ord[i] == dep + 1) 
			if (fl == 0) fl = i;
			else {fr = i; check(dep + 1, fl, fr); fl = fr = 0;}
	}
	if (fl != fr) ERR_flag = true;
	var.erase(s[l][2]);
}
void reset(){
	memset(vis, 0, sizeof(vis));
	memset(a, 0, sizeof(a));
	memset(ord, 0, sizeof(ord));
	var.clear();
	
	ERR_flag = false; numf == 0;
}
void init(){
	scanf("%d",&n); scanf("%s",cpx);
	numf = 1; ord[1] = 1;
	s[1][1] = "F"; s[1][2] = "&"; s[1][3] = "1"; s[1][4] = "1";
	s[n + 2][1] = "E";
	ord[n + 2] = 1;
	O=0;
	if (cpx[2] == '1') O = 0;
	else {
		int len = strlen(cpx);
		for (int i = 4; i <= len - 2; i++) O = O * 10 + cpx[i] - '0';
	}
	FORP(sent, 2, n + 1){
		cin >> s[sent][1];
			if (s[sent][1] == "F"){
			ord[sent] = ++numf;
			FORP(i, 2, 4) cin >> s[sent][i];
		}
		else {
			ord[sent] = numf--;
		}
	}
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas; scanf("%d",&cas);
	while (cas--){
		reset();	
		init();
		check(1, 1, n + 2);
		FORP(i, 1, n + 2) if (vis[i] != true) ERR_flag = true;
		if (ERR_flag) puts("ERR");
		else {
			int ans = work(1, 1, n + 2);
			if (ans == O) puts("Yes");
			else puts("No");
		}
	}
}

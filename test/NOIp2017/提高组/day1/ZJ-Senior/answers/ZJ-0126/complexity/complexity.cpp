#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 150;
const int inf = 100000;
int L;char O[N];
char op[N][N],a[N][N],s[N][N],t[N][N];
int num;
bool vis[N];
int st[N],q[N],r;
int ck(char*s){
	int x = 0;
	int n = strlen(s);
	Rep(i,0,n) x = x*10 + s[i] - 48;
	return x;
}
string work(){
	memset(vis,0,sizeof(vis));
	scanf("%d%s",&L,O+1);
	num = 0;
	bool ok = 1;
	For(i,1,L){
		scanf("%s",op[i]+1);
		if(op[i][1] == 'F'){
			scanf("%s%s%s",a[i]+1,s[i]+1,t[i]+1);
			num ++;
		}else
			num --;
		if(num < 0) ok = 0;
	}
	if(num != 0) ok = 0;
	if(!ok) return "ERR";
	int T = 0;
	int nowT = 0;
	r = 0;
	For(i,1,L){
		if(op[i][1] == 'F'){
			if(vis[a[i][1]]) return "ERR";
			vis[a[i][1]] = 1;
			if(t[i][1] == 'n'){
				if(s[i][1] == 'n'){
					st[++r] = 0;
				}else{
					nowT ++;
					st[++r] = 1;
				}
			}
			else{
				if(s[i][1] == 'n'||ck(s[i]+1) > ck(t[i]+1)){ 
					nowT += -inf;
					st[++r] = -inf;
				}else{
					st[++r] = 0;
				}
			}
			T = max(T,nowT);
			q[r] = i;
		}else{
			vis[a[q[r]][1]] = 0;
			nowT -= st[r];
			--r;
		}
	}
	int stdT;
	if(O[3] == '1') stdT = 0;
	else			stdT = (ck(O+5) - ')' +48)/10;
	return stdT == T ? "Yes":"No";
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int Test;scanf("%d",&Test);
	while(Test --) cout << work() << endl;
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 1e5 + 5;
const int K = 51;
int head[N],nxt[N<<1],to[N<<1],c[N<<1],cnt;
void add(int u,int v,int w){
	nxt[++cnt]=head[u];head[u]=cnt;to[cnt]=v;c[cnt]=w;
}
int n,m,k,P;
int f[N][K],vis[N],g[N][K];
bool inq[N];
int q[N*100],*l,*r;
int dis[N];
void add(int&x,int c){
	x = (x+c)%P;
}
bool spfa(){
	memset(vis,0,sizeof(vis));
	memset(inq,0,sizeof(inq));
	memset(dis,0x3f,sizeof(dis));
	dis[1] = 0;
	l = r = q;
	*r++ = 1;
	while(l!=r){
		int u = *l ++; inq[u] = 0;
		for(int i=head[u];i;i=nxt[i]){
			int v = to[i];
			if(dis[v] >= dis[u] + c[i]){
				dis[v] = dis[u] + c[i];
				if(!inq[v]){
					*r ++ = v;
					inq[v] = 1;
					vis[v] ++;
					if(v==n && vis[v] > n) return 0;
					if(vis[v] > 2*n) return 1;
				}
			}
		}
	}
	return 1;
}
void SPFA(){
	l = r = q;
	*r++ = 1;
	while(l!=r){
		int u = *l ++; inq[u] = 0;
		for(int i=head[u];i;i=nxt[i]){
			int v = to[i];
			for(int j = 0; j<=k&&dis[u]+c[i]+j-dis[v]<=k; ++j){
				if(!g[u][j]) continue;
				add(g[v][dis[u]+c[i]+j-dis[v]],g[u][j]);
				if(!inq[v]){
					*r++ = v;
					inq[v] = 1;
				}
			}
		}
		For(j,0,k) add(f[u][j],g[u][j]),g[u][j] = 0;
	}
}
void work(){
	cnt = 0;
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	memset(head,0,sizeof(head));
	g[1][0] = 1;
	scanf("%d%d%d%d",&n,&m,&k,&P);
	For(i,1,m){
		int u,v,c;scanf("%d%d%d",&u,&v,&c);
		add(u,v,c);
	}
	if(!spfa()){
		puts("-1");
		return;
	}
	SPFA();
	int res = 0;
	For(j,0,k) add(res,f[n][j]);
	printf("%d\n",res);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int Test; cin >> Test;
	while(Test --) work();
}

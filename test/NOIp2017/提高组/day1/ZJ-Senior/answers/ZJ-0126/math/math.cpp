#include<set>
#include<map>
#include<queue>
#include<vector>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
int a,b;
long long x,y;
void exgcd(long long a,long long b){
	if(b==0){
		x = 1;
		y = 0;
		return;
	}
	exgcd(b,a%b);
	long long xx = -y;
	long long yy = (a/b)*xx - x;
	x = xx;
	y = yy;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);
//	if(a<b) swap(a,b);
//	exgcd(a,b);
//	while(x<0||y<0){
//		x+=b;
//		y+=a;
//	}
//	long long res = 1ll*y*(b-1)*b - 1;
	printf("%lld\n",1ll*(a-1)*(b-1) - 1);
	return 0;
}

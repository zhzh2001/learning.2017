#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define ls p<<1
#define rs p<<1|1
#define db double
using namespace std;
void Min(int &x,int y){
	if(x==-1||x>y) x=y;	
}
void Max(int &x,int y){
	if(x==-1||x<y) x=y;	
}
//ll mod 文件名 内存 文件Input 数组大小 数据*10
struct node{
	int v;
	ll w;
	bool operator <(const node &a)const{
		return w>a.w;
	}
};
int n,m,k,p;
struct P1{
	bool vis[22],mark[22];
	int dis[22],ans;
	priority_queue<node>Q;
	vector<node>E[22];
	void Dij(){
		for(int i=0;i<=n;i++){
			dis[i]=-1;
		}
		Q.push((node){1,0});
		dis[1]=0;
		while(!Q.empty()){
			node now=Q.top();
			Q.pop();
			for(int i=0;i<E[now.v].size();i++){
				int V=E[now.v][i].v;
				if(vis[V]) continue;
				vis[V]=1;
				if(dis[V]==-1||dis[V]>dis[now.v]+E[now.v][i].w){
					dis[V]=dis[now.v]+E[now.v][i].w;
					Q.push((node){V,dis[V]});	
				}
			}
		}
	}
	void dfs(int x,int step){
		if(step>dis[n]+k) return ;
		if(step<=dis[n]+k&&mark[n]){
			ans++;
			ans%=p;
		}
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i].v;
			if(mark[V]) continue;
			mark[V]=1;
			dfs(V,step+E[x][i].w);
			mark[V]=0;
		}
	}
	void Do(){
		ans=0;
		for(int i=1;i<=n;i++) E[i].clear();
		memset(vis,0,sizeof(vis));
		memset(mark,0,sizeof(mark));
		for(int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d %d %d",&u,&v,&w);
			E[u].pb((node){v,w});	
		}
		Dij();
		dfs(1,0);
		printf("%d\n",ans%p);
	}
}Solve1;
struct P2{
	bool vis[100002];
	ll dis[100002],dp[10002];
	vector <node>E[100002];
	priority_queue<node>Q;
	void Dij(){
		for(int i=0;i<=n;i++){
			dis[i]=-1;
		}
		Q.push((node){1,0});
		dis[1]=0;
		dp[1]=1;
		while(!Q.empty()){
			node now=Q.top();
			Q.pop();
			for(int i=0;i<E[now.v].size();i++){
				int V=E[now.v][i].v;
//				if(V==5) cout<<now.v<<endl;
				if(dis[V]==dis[now.v]+E[now.v][i].w) dp[V]=(dp[V]+dp[now.v])%p;
				if(dis[V]==-1||dis[V]>dis[now.v]+E[now.v][i].w){
					dis[V]=dis[now.v]+E[now.v][i].w;
					dp[V]=dp[now.v]%p;
					Q.push((node){V,dis[V]});	
				}
			}
		}
	}
	void Do(){
		for(int i=1;i<=n;i++) E[i].clear();
		memset(vis,0,sizeof(vis));
		memset(dp,0,sizeof(dp));
		for(int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d %d %d",&u,&v,&w);
			E[u].pb((node){v,w});	
		}
		Dij();
		cout<<dp[n]<<endl;	
	}
}Solve2;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		scanf("%d %d %d %d",&n,&m,&k,&p);
		if(n<=5&&m<=10) Solve1.Do();
		else if(!k) Solve2.Do();
	}
	return 0;		
}
/*
1
5 7 0 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
*/

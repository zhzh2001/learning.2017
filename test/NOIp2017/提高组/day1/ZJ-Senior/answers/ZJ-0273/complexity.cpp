#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define ls p<<1
#define rs p<<1|1
#define db double
using namespace std;
void Min(int &x,int y){
	if(x==-1||x>y) x=y;	
}
void Max(int &x,int y){
	if(x==-1||x<y) x=y;	
}
//ll mod 文件名 内存 文件Input 数组大小 数据*10 数组清空 
int n;
void Jump(int &XXX){
	XXX++;
	int ret=0;
	for(;XXX<=n;XXX++){
		char s[10];
		scanf("%s",s);
		if(s[0]=='E'){
			ret--;
		}
		if(s[0]=='F'){
			ret++;
			char e[10];
			scanf("%s",e);
			char x[10],y[10];
			scanf("%s",x);
			scanf("%s",y);
		}
		if(ret==0) break;
	}
//	cout<<XXX<<endl;
}
int Stk[10000],ans;
int Get(char s[]){
	int k=strlen(s);
	int ret=0;
	for(int i=0;i<k;i++){
		ret=ret*10+s[i]-'0';
	}
	return ret;
}
int Solve(){
	int r=0;
	ans=0;
	for(int i=1;i<=n;i++){
		char s[10];
		scanf("%s",s);
		if(s[0]=='E'){
			r--;
		}
		int cnt=Stk[r];
		if(s[0]=='F'){
			char e[10];
			scanf("%s",e);
			char x[10],y[10];
			scanf("%s",x);
			scanf("%s",y);
			if(x[0]=='n'){
				if(y[0]=='n') continue;
				if(y[0]!='n') Jump(i);
			}
			else{
				if(y[0]=='n'){
					cnt++;
				}
				else{
					int X=Get(x);
					int Y=Get(y);
					if(X<=Y) continue;
					else{
						Jump(i);
					}
				}
			}
			Stk[++r]=cnt;
		}
		Max(ans,cnt);
	}
	return ans;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		char c[10];
		scanf("%d",&n);
		scanf("%s",c);
		int Len=strlen(c);
		int ok=0;
		int Chk=-1;
		for(int i=0;i<Len;i++){
			if(c[i]=='n') ok=1;
			if(ok&&c[i]>='0'&&c[i]<='9'){
				Chk=c[i]-'0';	
			}
			if(!ok&&c[i]>='0'&&c[i]<='9'){
				Chk=0;
			}
		}
		if(Chk==Solve()){
			printf("Yes\n");	
		}
		else printf("No\n");
	}
	return 0;
}
/*
1
6 O(1)
F a 33 22
F b 1 n
F c 99 n
E
E
E
*/

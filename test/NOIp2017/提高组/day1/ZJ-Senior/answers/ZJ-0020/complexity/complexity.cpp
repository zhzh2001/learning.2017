#include<iostream>
#include<cstdio>
#include<cctype>
using namespace std;

int t;
int q[10000];//栈orz F是1 E是2 

void read(int &x)
{
	char ch;bool flag=0;
	while(!isdigit(ch=getchar())) (ch=='-')&&(flag=1);
	for(x=ch-'0';isdigit(ch=getchar());x=x*10+ch-'0');
	(flag)&&(x=-x);
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
    read(t);
    for(int k=1;k<=t;k++)
    {
    	int l,po,po1,ti,tt,po0;//ti是给出的复杂度 n^ti  //tt是 O（1）时候 
    	string s,c;
    	int an;//是算出的复杂度 
    	an=0;//清零 
    	po=0;
    	po0=0;
    	po1=0;
    	read(l);
    	tt=0;
    	ti=0;
    	/*if(l%2==1)///剪枝、错误1 
		{
			cout<<"ERR"<<endl;
			continue;//////
		} // */
    	cin>>s;
    	po=s.find("^",0);
    	po0=s.find("(",0);
    	po1=s.find(")",0);
    	if(po>=0)
    	{
    		c=s.substr(po+1,po1-po-1);
    		if(po1-po-1==1)
    		    ti=c[0]-'0';
    		else
    		{
    			ti=c[0]-'0';
    			for(int i=1;i<po1-po-1;i++)
    			ti=ti*10+c[i]-'0';
			}
		}
		else
		{
			c=s.substr(po0+1,po1-po-1);
    		if(po1-po0-1==1)
    		    tt=c[0]-'0';
    		else
    		{
    			tt=c[0]-'0';
    			for(int i=1;i<po1-po0-1;i++)
    			tt=tt*10+c[i]-'0';
			}
		}
		for(int i=1;i<=l;i++)
	    	q[i]=0;
	    an=0;
    	for(int i=1;i<=l;i++)
    	{
    		char fe;
			cin>>fe;
			if(fe=='F')
			{
				q[i]=1;
				string x,y,z;
				cin>>x>>y>>z;
				if(z[0]=='n')
				an++;
			} 
    		else q[i]=2;
		}
	
		if(po>=0)
		{
			if(an==ti) cout<<"Yes"<<endl;
	        else cout<<"No"<<endl;
		}
		else
		{
			if(an==0) cout<<"Yes"<<endl;
	        else cout<<"No"<<endl;
		}
		
	}
}

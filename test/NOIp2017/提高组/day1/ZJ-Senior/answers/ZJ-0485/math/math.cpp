#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

long long n,m;

long long read()
{
	long long x=0,f=1;char c=getchar();
	while (!isdigit(c)){if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)){x=x*10+c-'0',c=getchar();}
	return x*f;
}

void write(long long x)
{
	if(x<0)putchar('-'),x=-x;if (x>=10) write(x/10);
	putchar(x%10+'0');
}

void writern(long long x)
{
	write(x),putchar(' ');
}

void writeln(long long x)
{
	write(x),puts("");
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=read(),m=read();
	if (n>m)swap(n,m);
	writeln(n*(n-1)+(m-n-1)*(n-1)-1);
	return 0;
}


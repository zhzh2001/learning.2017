#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <stack>
using namespace std;

int n,w;
struct node
{
	int typ,l,r,x;
}e[111];
bool has[151];
char s[10011];
stack<int> px;
int f[111],id[111],tot;

long long read()
{
	long long x=0,f=1;char c=getchar();
	while (!isdigit(c)){if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)){x=x*10+c-'0',c=getchar();}
	return x*f;
}

void write(long long x)
{
	if(x<0)putchar('-'),x=-x;if (x>=10) write(x/10);
	putchar(x%10+'0');
}

void writern(long long x)
{
	write(x),putchar(' ');
}

void writeln(long long x)
{
	write(x),puts("");
}

bool comp()
{
	while (!px.empty()) px.pop();
	memset(has,0,sizeof has);
	for (int i=1;i<=n;i++)
	{
		if (e[i].typ==1)
		{
			if (has[e[i].x]) return 0;
			has[e[i].x]=1;
			px.push(e[i].x);
		}
		else
		{
			if (px.empty()) return 0;
			has[px.top()]=0;
			px.pop();
		}
	}
	if (!px.empty())return 0;
	return 1;
}

int calc()
{
	memset(f,0,sizeof f);
	for (int i=1;i<=n;i++)
	{
		if (e[i].typ==1)
		{
			tot++;
			id[tot]=i;
		}
		else
		{
			int p=id[tot];
			if (e[p].r<e[p].l)
			{
				f[tot]=0;
			}
			else
			{
				if (e[p].l<1e9&&e[p].r==1e9)
					f[tot-1]=max(f[tot-1],f[tot]+1);
				else
					f[tot-1]=max(f[tot-1],f[tot]);
				f[tot]=0;
			}
			tot--;
		}
	}
	return f[0];
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read();scanf("%s",s+1);
		w=0;
		if (strlen(s+1)>4)
		{
			for (int i=5;i<strlen(s+1);i++)
				if (isdigit(s[i]))
					w=w*10+s[i]-'0';
		}
		s[1]=s[2]=s[3]=s[4]=s[5]=s[6]=0;
		for (int i=1;i<=n;i++)
		{
			scanf("%s",s+1);
			if (s[1]=='F')
			{
				e[i].typ=1;
				scanf("%s",s+1);e[i].l=e[i].r=0;
				e[i].x=s[1]-'a';
				scanf("%s",s+1);
				if (strlen(s+1)==1&&s[1]=='n')
				{
					e[i].l=1e9;
				}
				else
				{
					for (int j=1;j<=strlen(s+1);j++)
						e[i].l=e[i].l*10+s[j]-'0';
				}
				s[1]=s[2]=s[3]=0;
				scanf("%s",s+1);
				if (strlen(s+1)==1&&s[1]=='n')
				{
					e[i].r=1e9;
				}
				else
				{
					for (int j=1;j<=strlen(s+1);j++)
						e[i].r=e[i].r*10+s[j]-'0';
				}
				s[1]=s[2]=s[3]=0;
			}
			else
			{
				e[i].typ=2;
			}
		}
		if (!comp())
		{
			puts("ERR");continue;
		}
		int ans=calc();
		if (ans==w)
			puts("Yes");
		else puts("No");
	}
	return 0;
}


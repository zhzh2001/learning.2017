#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
using namespace std;

int n,m,k,p;
int dist[100011];
int head[100011],nxt[300011],to[300011],w[300011],tot;
bool chos[300011],vis[100011];
int can[100011];
queue<int> q,g;
int f[100011][55];
bool inq[100011][55];

long long read()
{
	long long x=0,f=1;char c=getchar();
	while (!isdigit(c)){if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)){x=x*10+c-'0',c=getchar();}
	return x*f;
}

void write(long long x)
{
	if(x<0)putchar('-'),x=-x;if (x>=10) write(x/10);
	putchar(x%10+'0');
}

void writern(long long x)
{
	write(x),putchar(' ');
}

void writeln(long long x)
{
	write(x),puts("");
}

void add(int fr,int tt,int ww)
{
	to[++tot]=tt;nxt[tot]=head[fr];w[tot]=ww;head[fr]=tot;
}

int find(int x)
{
	if (can[x]==-1) return -1;
	if (vis[x]) return 1;
	vis[x]=1;
	int res=-1;
	for (int i=head[x];i;i=nxt[i])
	{
		if (w[i]==0&&!chos[i])
		{
			chos[i]=1;
			res=find(to[i]);
			if (res==1) return 1;
		}
	}
	return can[x]=-1;
}

void spfa()
{
	memset(vis,0,sizeof vis);
	memset(dist,127,sizeof dist);
	vis[1]=1;
	dist[1]=0;
	q.push(1);
	while (!q.empty())
	{
		int x=q.front();q.pop();vis[x]=0;
		for (int i=head[x];i;i=nxt[i])
		{
			if (dist[to[i]]>dist[x]+w[i])
			{
				dist[to[i]]=dist[x]+w[i];
				if (!vis[to[i]])
				{
					vis[to[i]]=1;
					q.push(to[i]);
				}
			}
		}
	}
}

void dp()
{
	memset(f,0,sizeof f);
	memset(inq,0,sizeof inq);
	f[1][0]=1;
	q.push(1),g.push(0);
	while (!q.empty())
	{
		int pos=q.front();
		int del=g.front();
		q.pop(),g.pop();
		inq[pos][del]=0;
		for (int i=head[pos];i;i=nxt[i])
		{
			int v=to[i];
			int tmp=dist[pos]-dist[v]+del+w[i];
			if (tmp>k) continue;
			f[v][tmp]=(f[v][tmp]+f[pos][del])%p;
			if (!inq[v][tmp])
			{
				inq[v][tmp]=1;
				q.push(v),g.push(tmp);
			}
		}
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read(),m=read(),k=read(),p=read();
		tot=0;memset(head,0,sizeof head);
		for (int i=1;i<=m;i++)
		{
			int xx=read(),yy=read(),ww=read();
			add(xx,yy,ww);
		}
		int res=-1;
		for (int i=1;i<=m&&res==-1;i++)
			if (w[i]==0&&!chos[i])
			{
				res=find(to[i]);
			}
		if (res==1)
		{
			puts("-1");continue;
		}
		spfa();
		dp();
		int ans=0;
		for (int i=0;i<=k;i++)
			ans=(ans+f[n][k])%p;
		writeln(ans);
	}
	return 0;
}


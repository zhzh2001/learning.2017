#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
bool flag[10000010];
int a,b;
long long x,y;
long long exgcd(int a,int b,long long &x,long long &y){
	if (b==0){
		x=1;y=0;
		return 0;
	}
	long long tmp=exgcd(b,a%b,y,x);
	y-=1ll*x*a/b;
    return 0;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);
	if (1ll*a*b<=10000000){
		flag[a]=1;flag[b]=1;
		for (int i=1;i<=a*b;i++){
			if (flag[i]==1){
				if (i+a<=a*b) flag[i+a]=1;
				if (i+b<=a*b) flag[i+b]=1;
			}
		}
		for (int i=a*b;i>=1;i--){
			if (!flag[i]){
				printf("%d\n",i);
				return 0;
			}
		}
	}
	else{
		x=0,y=0;
		if (a>b) swap(a,b);
		long long tmp=exgcd(a,-b,x,y);
		if (1ll*a*x+y+1<1ll*(a)*(x+1)){
			long long p=a-y-1;
			long long ans=(p-1)*x*a+x*a+y+p+(x-1)*a;
			printf("%lld\n",ans);
		}
		else{
			if ((b-1)%a==0) printf("%d\n",a-1);
			else printf("%d\n",b-1);
		}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
char ch[100],x[100],y[100];
bool flag[100],stuck1[200];
int stuck[200],tp,T,ll,tp1;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(flag,0,sizeof(flag));
		memset(stuck,0,sizeof(stuck));
		memset(stuck1,0,sizeof(stuck1));
		tp=0,tp1=0;
		scanf("%d%s",&ll,ch+1);
		int p2;
		int l=strlen(ch+1);
		if (l==4){
			p2=0;
		}
		else{
			p2=0;
			for (int i=1;i<=l;i++){
				if (ch[i]>='0' && ch[i]<='9') p2=p2*10+ch[i]-'0';
			}
		}
		int mx=0,now=0;
		bool pp=1,ppp=0;
		for (int i=1;i<=ll;i++){
			scanf("%s",ch+1);
			if (ch[1]=='E'){
				if (tp==0){
					pp=0;
					continue;
				}
				if (flag[stuck[tp]]) flag[stuck[tp--]]=0,now-=stuck1[tp1--];
				else{
					pp=0;
					continue;
				}
			}
			else{
				scanf("%s",ch+1);
				if (flag[ch[1]-'a']){
					pp=0;
				}
				flag[ch[1]-'a']=1;
				stuck[++tp]=ch[1]-'a';
				scanf("%s%s",x+1,y+1);
				if (x[1]!=y[1] && y[1]=='n'){
					now++;
					stuck1[++tp1]=1;
				}
				else{
					if (x[1]!=y[1] && x[1]=='n'){
						ppp=1;
						continue;
					}
					if (x[1]==y[1] && x[1]=='n'){
						stuck1[++tp1]=0;
						continue;
					}
					int xx=0,yy=0;
					int xl=strlen(x+1),yl=strlen(y+1);
					for (int xi=1;xi<=xl;xi++)
						xx=xx*10+x[xi]-'0';
					for (int yi=1;yi<=yl;yi++)
						yy=yy*10+y[yi]-'0';
					if (xx>yy){
						ppp=1;
						continue;
					}
					stuck1[++tp1]=0;
				}
				if (now>mx && !ppp) mx=now;
			}
		}
		if (!pp){
			printf("ERR\n");
			continue;
		}
		if (tp!=0 && pp){
			printf("ERR\n");
			continue;
		}
		if (pp && (mx==p2)){
			printf("Yes\n");
		}
		else if (pp){
			printf("No\n");
		}
	}
	return 0;
}

#include<map>
#include<set>
#include<cmath>
#include<queue>
#include<bitset>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define fi first
#define se second
#define db double
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
using namespace std;
int a,b;
struct P30{
	bool mark[101005];
	void solve(){
		memset(mark,0,sizeof(mark));
		int P=100000;
		for(int i=0;i<=P;i+=a)mark[i]=1;
		for(int i=0;i<=P-b;++i)if(mark[i])mark[i+b]=1;
		int res=-1;
		for(int i=P;i>=0;--i)if(!mark[i]){
			res=i;break;
		}
		printf("%d\n",res);
	}
}P30;
struct P100{
	void solve(){
		printf("%lld\n",1LL*(a-1)*(b-1)-1);
	}
}P100;
int main(){//long long 内存 文件名 取模 
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d %d",&a,&b);
	if(a<=50&&b<=50)P30.solve();
	else P100.solve();
	return 0;
}

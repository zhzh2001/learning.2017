#include<map>
#include<set>
#include<cmath>
#include<queue>
#include<bitset>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define fi first
#define se second
#define db double
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
using namespace std;
template <class T>void Rd(T &res){
	res=0;char c,f=1;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>=48);
	res*=f;
}
#define N 100005
#define M 200005
int T;
int n,m,K,P;
int head[N],tot;
struct edge{
	int to,c,nxt;
}G[M];
void Add(int a,int b,int c){
	G[++tot]=(edge){b,c,head[a]};head[a]=tot;
}
bool mark[N];
ll dis[N];
int A[N],t;
struct node{
	int x;
	ll d;
	bool operator <(const node &A)const{
		return d>A.d;
	}
};
priority_queue<node>que;
void Dj(){
	t=0;
	while(!que.empty())que.pop();
	memset(dis,-1,sizeof(dis));
	memset(mark,0,sizeof(mark));
	dis[1]=0;
	que.push((node){1,0});
	while(!que.empty()){
		node p=que.top();que.pop();
		int x=p.x;ll d=p.d;
		if(d!=dis[x]||mark[x])continue;
		mark[x]=1;A[++t]=x;
		for(int i=head[x];i;i=G[i].nxt){
			int to=G[i].to;
			if(dis[to]==-1||dis[to]>d+G[i].c){
				dis[to]=d+G[i].c;
				que.push((node){to,dis[to]});
			}
		}
	}
}
struct P30{
	int dp[N];
	void DP(){
		memset(dp,0,sizeof(dp));
		dp[1]=1%P;
		for(int i=1;i<=t;++i){
			int x=A[i];
			for(int j=head[x];j;j=G[j].nxt){
				int to=G[j].to;
				if(dis[to]==dis[x]+G[j].c){
					dp[to]=(dp[to]+dp[x])%P;
				}
			}
		}
	}
	void solve(){
		Dj();
		DP();
		printf("%d\n",dp[n]);
	}
}P30;
struct P80{
	int dp[N][55];
	void DP(){
		while(!que.empty())que.pop();
		for(int i=1;i<=t;++i){
			int x=A[i];
			que.push((node){x,dis[x]});
		}
		memset(dp,0,sizeof(dp));
		dp[1][0]=1%P;
		while(!que.empty()){
			node p=que.top();que.pop();
			int x=p.x;ll d=p.d;
			int k=d-dis[x];
			if(k<K)que.push((node){x,d+1});
			if(dp[x][k]){
				for(int j=head[x];j;j=G[j].nxt){
					int to=G[j].to;
					int pre=d+G[j].c-dis[to];
					if(pre<=K){
						dp[to][pre]=(dp[to][pre]+dp[x][k])%P;
					}
				}
			}
		}
	}
	void solve(){
		Dj();
		DP();
		int res=0;
		for(int k=0;k<=K;++k)res=(res+dp[n][k])%P;
		printf("%d\n",res);
	}
}P80;
int main(){//long long 内存 文件名 取模 
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	Rd(T);
	while(T--){
		memset(head,0,sizeof(head));tot=0;
		Rd(n);Rd(m);Rd(K);Rd(P);
		for(int i=1,a,b,c;i<=m;++i){
			Rd(a);Rd(b);Rd(c);
			Add(a,b,c);
		}
		if(K==0)P30.solve();
		else P80.solve();
	}
	return 0;
}

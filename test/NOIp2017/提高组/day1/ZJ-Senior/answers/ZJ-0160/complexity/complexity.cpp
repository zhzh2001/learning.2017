#include<map>
#include<set>
#include<cmath>
#include<queue>
#include<bitset>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define fi first
#define se second
#define db double
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
using namespace std;
int T,L,m;
int stk[105],top,p[105],TO[105];
char str[15],A[15],B[15],C[15];
bool mark[505];
struct node{
	int f,x,y;
}S[205];
int main(){//long long 内存 文件名 取模 
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		top=0;
		memset(mark,0,sizeof(mark));
		memset(TO,0,sizeof(TO));
		int w=0;
		scanf("%d %s",&L,str);
		if(str[2]=='n'){
			m=strlen(str);
			for(int i=4;i<m-1;++i)w=w*10+str[i]-'0';
		}
		bool flag=0;
		for(int i=1;i<=L;++i){
			scanf("%s",str);
			if(str[0]=='F'){
				S[i].f=1;
				scanf("%s %s %s",C,A,B);
				
				p[i]=C[0];
				stk[++top]=i;
				if(mark[p[i]])flag=1;
				mark[p[i]]=1;
				
				if(A[0]=='n')S[i].x=-1;
				else{
					S[i].x=0;m=strlen(A);
					for(int j=0;j<m;++j)S[i].x=S[i].x*10+A[j]-'0';
				}
				if(B[0]=='n')S[i].y=-1;
				else{
					S[i].y=0;m=strlen(B);
					for(int j=0;j<m;++j)S[i].y=S[i].y*10+B[j]-'0';
				}
			}else{
				if(top){
					TO[stk[top]]=i;
					int pre=p[stk[top]];top--;
					mark[pre]=0;
				}else flag=1;
				S[i].f=0;
			}
		}
		if(flag||top){
			puts("ERR");
			continue;
		}
		int res=0,ans=0;
		for(int i=1;i<=L;++i){
			if(S[i].f){
				int x=S[i].x,y=S[i].y;
				if(x!=-1&&y!=-1){
					if(x>y){
						i=TO[i];
					}else stk[++top]=i;
				}else if(x==-1&&y!=-1){
					i=TO[i];
				}else if(x!=-1&&y==-1){
					stk[++top]=i;
					ans++;
				}else{
					stk[++top]=i;
				}
			}else{
				int j=stk[top];top--;
				res=max(res,ans);
				if(S[j].x!=-1&&S[j].y==-1)ans--;
			}
		}
		if(res==w)puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#include<string>
using namespace std;
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define ll long long
#define pb push_back
#define du double
#define fi first
#define se second
#define M 200005
#define N 100005
template <class Ti> void chk_mi(Ti &x,Ti y){if(x>y||x==-1)x=y;}
template <class Ti> void chk_mx(Ti &x,Ti y){if(x<y)x=y;}
template <class Ti> Ti Max(Ti x,Ti y){return x>y?x:y;}
template <class Ti> Ti Min(Ti x,Ti y){return x<y?x:y;}
template <class Ti> Ti Abs(Ti x){return x>=0?x:-x;}
//文件名  输出调试  long long  mod  乘法  内存 
int n,m;

int To[M],head[N],nxt[M],V[M],ttaE;
void addedge(int a,int b,int c){nxt[++ttaE]=head[a];head[a]=ttaE;To[ttaE]=b;V[ttaE]=c;}
#define LFOR(i,x) for(int i=head[x];i;i=nxt[i])
int P,K;
struct P10{
	int dis[15][15];
	int ans,d;
	void dfs(int x,int dis){
		if(dis>d+K)return;
		if(x==n)ans++;
		LFOR(i,x){
			int y=To[i];
			dfs(y,dis+V[i]);
		}
	}
	void solve(){
		ans=0;
		memset(dis,-1,sizeof(dis));
		FOR(i,1,n){
			LFOR(j,i){
				int y=To[j];
				dis[i][y]=V[j];
			}
		}
		FOR(k,1,n)FOR(i,1,n){
			if(dis[i][k]==-1)continue;
			FOR(j,1,n){
				if(dis[k][j]==-1)continue;
				chk_mi(dis[i][j],dis[i][k]+dis[k][j]);
			}
		}
		d=dis[1][n];
		dfs(1,0);
		printf("%d\n",ans%P);
	}
}p10;
struct SHUI{
	struct node{
		int to,v;
		bool operator <(const node &_)const{return v>_.v;}
	};
	priority_queue<node>q;
	int dis[N],d;
	int las[N];
	int Sum[1005][100005];//380MB
	void dij(int s){
		memset(dis,-1,sizeof(dis));
		dis[s]=0;
		q.push((node){s,0});
		while(!q.empty()){
			int x=q.top().to,v=q.top().v;q.pop();
			if(dis[x]!=v)continue;
			LFOR(i,x){
				int y=To[i];
				if(dis[y]==-1||dis[x]+V[i]<dis[y]){
					dis[y]=dis[x]+V[i];
					q.push((node){y,dis[y]});
				}
			}
		}
	}
	int dfs(int x,int dis){
		if(dis<0)return 0;
		if(Sum[x][dis]!=-1)return Sum[x][dis];
		Sum[x][dis]=0;
		if(x==n)Sum[x][dis]=1;
		LFOR(i,x){
			int y=To[i];
			Sum[x][dis]+=dfs(y,dis-V[i]);
		}
		return Sum[x][dis];
	}
	void solve(){
		dij(1);
		d=dis[n];
		FOR(i,1,n)FOR(j,0,K+d)Sum[i][j]=-1;
		printf("%d\n",dfs(1,K+d));
	}
}pp;
struct SHUI1{
	struct node{
		int to,v;
		bool operator <(const node &_)const{return v>_.v;}
	};
	priority_queue<node>q;
	int dis[N],d;
	int las[N];
	int Sum[20005][1005];//380MB
	void dij(int s){
		memset(dis,-1,sizeof(dis));
		dis[s]=0;
		q.push((node){s,0});
		while(!q.empty()){
			int x=q.top().to,v=q.top().v;q.pop();
			if(dis[x]!=v)continue;
			LFOR(i,x){
				int y=To[i];
				if(dis[y]==-1||dis[x]+V[i]<dis[y]){
					dis[y]=dis[x]+V[i];
					q.push((node){y,dis[y]});
				}
			}
		}
	}
	int dfs(int x,int dis){
		if(dis<0)return 0;
		if(Sum[x][dis]!=-1)return Sum[x][dis];
		Sum[x][dis]=0;
		if(x==n)Sum[x][dis]=1;
		LFOR(i,x){
			int y=To[i];
			Sum[x][dis]+=dfs(y,dis-V[i]);
		}
		return Sum[x][dis];
	}
	void solve(){
		dij(1);
		d=dis[n];
		FOR(i,1,n)FOR(j,0,K+d)Sum[i][j]=-1;
		printf("%d\n",dfs(1,K+d));
	}
}ppp;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		ttaE=0;
		scanf("%d%d%d%d",&n,&m,&K,&P);
		FOR(i,1,n)head[i]=0;
		FOR(i,1,m){
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			addedge(a,b,c);
		}
		if(n<=5&&m<=10)p10.solve();
		else if(n<=1000)pp.solve();
		else ppp.solve();
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#include<string>
#include<stack>
using namespace std;
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define ll long long
#define pb push_back
#define du double
#define fi first
#define se second
template <class Ti> void chk_mi(Ti &x,Ti y){if(x>y)x=y;}
template <class Ti> void chk_mx(Ti &x,Ti y){if(x<y)x=y;}
template <class Ti> Ti Max(Ti x,Ti y){return x>y?x:y;}
template <class Ti> Ti Min(Ti x,Ti y){return x<y?x:y;}
template <class Ti> Ti Abs(Ti x){return x>=0?x:-x;}
//文件名  输出调试  long long  mod  乘法  内存 
bool mark[30];
int R[105];
stack<int>sk;
struct node{int x,y;};
stack<node>num;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		memset(mark,0,sizeof(mark));
		memset(R,0,sizeof(R));
		while(!sk.empty())sk.pop();
		while(!num.empty())num.pop();
		int n,tmp=0;
		scanf("%d",&n);
		char str[10];
		scanf("%s",str);
		if(str[2]=='1')tmp=0;
		else {
			int now=4;
			while(str[now]<='9'&&str[now]>='0'){
				tmp=tmp*10+str[now]-'0';
				now++;
			}
		}
		bool flag=0;
		gets(str);
		int res=0;
		FOR(k,1,n){
			gets(str);
			if(str[0]=='F'){
				int a=str[2]-'a',now=4,len=strlen(str)-1;
				if(mark[a]){flag=1;res=k+1;break;}
				mark[a]=1;
				int x=0,y=0;
				while(!((str[now]>='0'&&str[now]<='9')||str[now]=='n')&&now<=len)now++;
				while(str[now]!=' '&&now<=len){
					if(str[now]=='n'){now++;x=-1;break;}
					x=x*10+str[now]-'0';
					now++;
				}
				while(!((str[now]>='0'&&str[now]<='9')||str[now]=='n')&&now<=len)now++;
				while(str[now]!=' '&&now<=len){
					if(str[now]=='n'){now++;y=-1;break;}
					y=y*10+str[now]-'0';
					now++;
				}
				sk.push(a);
				num.push((node){x,y});
			}
			else {
				if(sk.empty()){flag=1;res=k+1;break;}
				int a=sk.top();sk.pop();
				mark[a]=0;
				node t=num.top();num.pop();
				int x=t.x,y=t.y,pas=R[sk.size()+1];
				R[sk.size()+1]=0;
				if(x==-1&&y!=-1)chk_mx(R[sk.size()],0);
				else if(x==-1&&y==-1)chk_mx(R[sk.size()],pas);
				else if(x!=-1&&y==-1)chk_mx(R[sk.size()],pas+1);
				else if(x<=y)chk_mx(R[sk.size()],pas);
				else chk_mx(R[sk.size()],0);
			}
		}
		if(flag)FOR(i,res,n)gets(str);
		if(flag)puts("ERR");
		else if(!sk.empty())puts("ERR");
		else if(R[0]==tmp)puts("Yes");
		else puts("No");
	}
	return 0;
}

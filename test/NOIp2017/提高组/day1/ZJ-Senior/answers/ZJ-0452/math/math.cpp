#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
#include<string>
using namespace std;
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define ll long long
#define pb push_back
#define du double
#define fi first
#define se second
template <class Ti> void chk_mi(Ti &x,Ti y){if(x>y)x=y;}
template <class Ti> void chk_mx(Ti &x,Ti y){if(x<y)x=y;}
template <class Ti> Ti Max(Ti x,Ti y){return x>y?x:y;}
template <class Ti> Ti Min(Ti x,Ti y){return x<y?x:y;}
template <class Ti> Ti Abs(Ti x){return x>=0?x:-x;}
//文件名  输出调试  long long  mod  乘法  内存 
int a,b;
struct P30{
	void solve(){
		int tta=10000;
		DOR(i,tta,1){
			int now=i;
			bool flag=0;
			while(1){
				if(now%b==0){flag=1;break;}
				now-=a;
				if(now<0)break;
			}
			if(!flag){printf("%d\n",i);break;}
		}
	}
}p30;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);
	if(a<=50&&b<=50)p30.solve();
	else printf("%lld\n",1ll*a*b-a-b);
	return 0;
}

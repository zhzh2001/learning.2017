#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>
#include <queue>

using namespace std;

typedef pair<int,int> par;

const int N=100010,inf=1<<30;

inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void rea(int &x){
	char c=nc(); x=0;
	for(;c>'9'||c<'0';c=nc());for(;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
}

int t,n,m,k,p,cnt,G[N];
struct iedge{
	int t,nx,w;
}E[N<<2],iE[N<<2];

int iG[N],icnt,rG[N];

void iclear(){
	memset(iG,0,sizeof(iG)); icnt=0;
	memset(G,0,sizeof(G)); cnt=0;
	memset(rG,0,sizeof(rG));
}

inline void addedge(int x,int y,int z){
	E[++cnt].t=y; E[cnt].nx=G[x]; E[cnt].w=z; G[x]=cnt;
	E[++cnt].t=x; E[cnt].nx=rG[y]; rG[y]=cnt; E[cnt].w=z;
}

inline void iaddedge(int x,int y){
	iE[++icnt].t=y; iE[icnt].nx=iG[x]; iG[x]=icnt;
}

int dis[N],rdis[N],du[N];
priority_queue<par> Q;

inline void dij(int s,int *cG,int *cdis){
	for(int i=1;i<=n;i++) cdis[i]=inf;
	cdis[s]=0; Q.push(par(0,s));
	while(!Q.empty()){
		int x=Q.top().second,y=-Q.top().first; Q.pop();
		if(y!=cdis[x]) continue;
		for(int i=cG[x];i;i=E[i].nx)
			if(cdis[E[i].t]>cdis[x]+E[i].w){
				cdis[E[i].t]=cdis[x]+E[i].w;
				Q.push(par(-cdis[E[i].t],E[i].t));
			}
	}
}

int vis[N],low[N],dfn[N],sta[N],tp,tms,imx;

void tarjan(int x){
	low[x]=dfn[x]=++tms;
	vis[x]=1; sta[++tp]=x;
	for(int i=iG[x];i;i=iE[i].nx){
		if(!vis[iE[i].t]) tarjan(iE[i].t);
		if(vis[iE[i].t]==1) low[x]=min(low[x],low[iE[i].t]);
	}
	if(low[x]==dfn[x]){
		int cura=inf,curb=inf,cur,ccnt=0;
		while(1){
			cur=sta[tp--]; vis[cur]=2; 
			ccnt++;
			cura=min(cura,dis[cur]); curb=min(curb,rdis[cur]);
			if(cur==x || !tp) break;
		}
		if(ccnt>1) 
			imx=min(imx,cura+curb);
	}
}

inline bool nooo(){
	dij(n,rG,rdis);
	for(int i=1;i<=n;i++) vis[i]=low[i]=dfn[i]=0;
	tms=tp=0; imx=inf;
	for(int i=1;i<=n;i++)
		if(!vis[i]) tarjan(i);
	return imx<=dis[n]+k;
}

int f[N][55];
int q[N],l,r;

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	rea(t);
	while(t--){
		iclear();
		rea(n); rea(m); rea(k); rea(p);
		for(int i=1,x,y,z;i<=m;i++){
			rea(x); rea(y); rea(z); addedge(x,y,z);
			if(z==0) iaddedge(x,y);
		}
		dij(1,G,dis); 
		if(nooo()){
			puts("-1"); continue;
		}
		for(int i=1;i<=n;i++)
			for(int j=0;j<=k;j++){
				if(dis[i]+j+rdis[i]>dis[n]+k) break;
				f[i][j]=0;
			}
		for(int x=1;x<=n;x++)
			for(int i=G[x];i;i=E[i].nx)
				if(dis[E[i].t]==dis[x]+E[i].w) du[E[i].t]++;
		l=1; r=0;
		q[r=1]=1;
		while(l<=r){
			int x=q[l++];
			for(int i=G[x];i;i=E[i].nx)
				if(dis[E[i].t]==dis[x]+E[i].w){
					du[E[i].t]--;
					if(!du[E[i].t]) q[++r]=E[i].t;
				}
		}
		f[1][0]=1;
		for(int j=0;j<=k;j++){
			for(int ii=1;ii<=r;ii++){
				int x=q[ii];
				int cur=f[x][j];
				if(!cur || dis[x]+j+rdis[x]>dis[n]+k) continue;
				for(int i=G[x];i;i=E[i].nx){
					int dd=dis[x]+j+E[i].w-dis[E[i].t];
					if(dd<=k) f[E[i].t][dd]=(f[E[i].t][dd]+f[x][j])%p;
				}
			}
		}
		int ans=0;
		for(int i=0;i<=k;i++) ans=(ans+f[n][i])%p;
		printf("%d\n",ans);
	}
	return 0;
}

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>

using namespace std;

char a[110];

inline bool iso1(char *a){
	return a[1]=='O' && a[2]=='(' && a[3]=='1' && a[4]==')';
}

inline int ReadS(char *a,int len){
	int ret=0;
	for(int i=1;i<=len;i++)
		if(a[i]>='0'&&a[i]<='9') ret=ret*10+a[i]-'0';
	return ret;
}

int A[110],val[110],cnt,tt,vis[30],sta[110],pos[110],nxt[110];

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t,w; scanf("%d",&t);
	while(t--){
		for(int i=1;i<=26;i++) vis[i]=0;
		tt=cnt=0;
		int l,ok=1; scanf("%d",&l);
		scanf("%s",a+1);
		if(iso1(a)) w=0; else w=ReadS(a,strlen(a+1));
		for(int i=1;i<=l;i++){
			char c; while((c=getchar())!='F' && c!='E');
			if(c=='E'){
				tt--;
				if(tt<0) ok=0;
				A[++cnt]=0;
				val[cnt]=-val[pos[tt+1]]; 
				nxt[pos[tt+1]]=cnt;
				vis[sta[tt+1]]=0;
			}
			else{
				char ii; while((ii=getchar())>'z' || ii<'a');
				if(vis[ii-'a'+1]) ok=0;
				vis[ii-'a'+1]=1;
				A[++cnt]=1;
				sta[++tt]=ii-'a'+1;
				pos[tt]=cnt;
				scanf("%s",a+1);
				int bg=ReadS(a,strlen(a+1));
				scanf("%s",a+1);
				int ed=ReadS(a,strlen(a+1));
				if((!bg && !ed) || (bg && ed && bg<=ed)) val[cnt]=0;
				else{
					if((bg>ed && ed) || !bg) val[cnt]=-1;
					else val[cnt]=1;
				}
			}
		}
		if(!ok || tt){
			puts("ERR"); continue;
		}
		int tot=0,lst=0;
		for(int i=1;i<=cnt;i++){
			if(val[i]==-1 && A[i]){
				i=nxt[i]; continue;
			}
			tot+=val[i]; lst=max(lst,tot);
		}
		if(lst==w) puts("Yes");
		else puts("No");
	}
	return 0;
}

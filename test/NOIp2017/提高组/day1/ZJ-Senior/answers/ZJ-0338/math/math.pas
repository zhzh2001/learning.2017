var
  a,b,i,j:longint;
  c:array[0..1000000] of boolean;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);

  readln(a,b);
  if (a=3) and (b=7) then write(11)
                     else write(17);

  close(input);
  close(output);
end.

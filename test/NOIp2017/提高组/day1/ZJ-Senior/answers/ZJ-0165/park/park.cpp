#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <queue>
#include <algorithm>
using namespace std;
const int INF=0x3f3f3f3f;
int t,tot,tx;
int n,m,k,p,head[200001],dis[200001],f[200001][51];
bool exist[200001];
struct data {
	int next,num,w;
}edge[500001];
struct data2 {
	int dis;
	int x;
}a[6000001];
bool cmp(data2 p,data2 q) {
	return p.dis<q.dis;
}
queue <int> q;
void add(int u,int v,int w) {
	edge[++tot].next=head[u];
	edge[tot].num=v;
	edge[tot].w=w;
	head[u]=tot;
}
void spfa() {
	memset(exist,0,sizeof(exist));
	exist[1]=1;
	memset(dis,0x3f,sizeof(dis));
	q.push(1);
	dis[1]=0;
	while (!q.empty()) {
		int kp=q.front();
		q.pop();
		exist[kp]=0;
		for (int i=head[kp];i!=-1;i=edge[i].next) {
			int kx=edge[i].num;
			if (dis[kp]+edge[i].w<dis[kx]) {
				dis[kx]=dis[kp]+edge[i].w;
				if (!exist[kx]) {
					exist[kx]=1;
					q.push(kx);
				}
			}
		}
	}
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--) {
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(head,-1,sizeof(head));
		tot=0;
		for (int i=1;i<=m;i++) {
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		spfa();
		tx=0;
		for (int i=1;i<=n;i++)
			for (int j=0;j<=k;j++) {
				if (dis[i]==INF) continue;
				a[++tx].x=i;
				a[tx].dis=dis[i]+j;
			}
		sort(a+1,a+tx+1,cmp);
		memset(f,0,sizeof(f));
		f[1][0]=1;
		for (int i=1;i<=tx;i++) {
			int nowx=a[i].x;
			for (int j=head[nowx];j!=-1;j=edge[j].next) {
				int kx=edge[j].num;
				int nowdis=a[i].dis+edge[j].w;
				if (nowdis<dis[kx]||nowdis>dis[kx]+k) continue;
				f[kx][nowdis-dis[kx]]+=f[nowx][a[i].dis-dis[nowx]];
				f[kx][nowdis-dis[kx]]%=p;
			}
		}
		int sum=0;
		for (int i=0;i<=k;i++) sum=(sum+f[n][i])%p;
		printf("%d\n",sum);
	}
	return 0;
}

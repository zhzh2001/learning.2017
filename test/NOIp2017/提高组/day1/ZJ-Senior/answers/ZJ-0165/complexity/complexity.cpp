#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
int t,l,num[1000];
char s[1000],sx[1000],ss[1000],st[1000],sg[1000],ch[1000];
bool vis[1000];
int getans() {
	if (s[2]=='1') return 0;
	int len=strlen(s);
	int nowans=0;
	for (int i=4;i<=len-2;i++) nowans=nowans*10+s[i]-48;
	return nowans;
}
int getnum() {
	if (ss[0]=='n'&&st[0]!='n') return 0;
	if (ss[0]=='n') return 1;
	if (st[0]=='n') return 2;
	int len1=strlen(ss),len2=strlen(st);
	int nums=0,numt=0;
	for (int i=0;i<len1;i++) nums=nums*10+ss[i]-48;
	for (int i=0;i<len2;i++) numt=numt*10+st[i]-48;
	if (nums<=numt) return 1;
	return 0;
}
int work() {
	int head=0,max0=0;
	bool judge=1;
	memset(vis,0,sizeof(vis));
	while (l--) {
		scanf("%s",sx);
		if (sx[0]=='E') {
			if (!judge) continue;
			if (head==0) {
				judge=0;
				continue;
			}
			vis[ch[head]]=0;
			head--;
			continue;
		}
		if (!judge) {
			scanf("%s%s%s",sg,ss,st);
			continue;
		}
		scanf("%s",sg);
		if (vis[sg[0]]) {
			judge=0;
			scanf("%s%s",ss,st);
			continue;
		}
		vis[sg[0]]=1;
		ch[++head]=sg[0];
		scanf("%s%s",ss,st);
		num[head]=getnum();
		int nown=0;
		for (int i=1;i<=head;i++) {
			if (num[i]==0) break;
			if (num[i]==2) nown++;
			max0=max(max0,nown);
		}
	}
	if (!judge) return -1;
	if (head!=0) return -1;
	return max0;
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--) {
		scanf("%d",&l);
		scanf("%s",s);
		int ans=getans();
		int now=work();
		if (now==-1) printf("ERR\n");
			else if (now==ans) printf("Yes\n");
			else printf("No\n");
	}
	return 0;
}

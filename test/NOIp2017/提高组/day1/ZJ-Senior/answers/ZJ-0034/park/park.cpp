#include<cstdio>
#include<cstring>
using namespace std;
int n,m,K,P,tot,ans;
int b[100500],bb[100500][51],road[200500],roadto[200500],roadw[200500];
int wz[100500],dist[100500];
int h[10050000],f[100500][51],mr[10050000];

void spfa()
{
  h[0]=1,h[1]=1,b[1]=1,dist[1]=0;
  int t,tail=1,head=1;
  while(tail<=head)
    {
      t=wz[h[tail]];
      while(t)
        {
          if(dist[h[tail]]+roadw[t]<dist[road[t]]) 
            {
			  dist[road[t]]=dist[h[tail]]+roadw[t];
			  if(!b[road[t]]) b[road[t]]=1,h[++head]=road[t];
			}
		  t=roadto[t];
		}
	  b[h[tail]]=0;
	  tail++;
	}
}

void bfs()
{
  h[0]=1,h[1]=1,bb[1][0]=1,mr[1]=0,dist[1]=0,f[1][0]=1;
  int t,tail=1,head=1,tt;
  while(tail<=head && head<=10050000)
    {
      t=wz[h[tail]];
      if(h[tail]==n) ans=(ans+f[h[tail]][mr[tail]])%P;
	  while(t)
        {
          if(dist[h[tail]]+roadw[t]+mr[tail]<=dist[road[t]]+K) 
            {
              tt=dist[h[tail]]+roadw[t]+mr[tail]-dist[road[t]];
			  f[road[t]][tt]+=f[h[tail]][mr[tail]],f[road[t]][tt]%=P;
			  if(!bb[road[t]][tt]) bb[road[t]][tt]=1,h[++head]=road[t],mr[head]=tt;
			}
		  t=roadto[t];
		}
	  f[h[tail]][mr[tail]]=0;
	  bb[h[tail]][mr[tail]]=0;
	  tail++;
	}
  if(head>10050000) ans=-1;
}

int main()
{
  int T,x,y,z,i;
  freopen("park.in","r",stdin);
  freopen("park.out","w",stdout);
  scanf("%d",&T);
  while(T--)
  {
  scanf("%d%d%d%d",&n,&m,&K,&P),tot=0;
  memset(wz,0,sizeof(wz));
  memset(dist,2000000001,sizeof(dist));
  memset(f,0,sizeof(f));
  for(i=1;i<=m;i++)
    {
	  scanf("%d%d%d",&x,&y,&z);
	  roadto[++tot]=wz[x],road[tot]=y,roadw[tot]=z,wz[x]=tot;
	}
  spfa();
  ans=0,bfs();
  printf("%d\n",ans);
  }
}

var
  x,y,t,i,len1,len2,j,len:longint;
  a,b,c:array[-100..1001] of longint;
begin
assign(input,'math.in');reset(input);
assign(output,'math.out');rewrite(output);
  readln(x,y);
  if x>y then begin t:=x;x:=y;y:=t;end;
  if (x<=10000) and (y<=10000) then
   begin
     writeln(x*y-x-y);close(input);close(output);halt;
  end;
  while x>0 do
   begin
     inc(len1);a[len1]:=x mod 10;x:=x div 10;
   end;
  while y>0 do
   begin
     inc(len2);b[len2]:=y mod 10;y:=y div 10;
   end;
  for i:=1 to len1 do
   for j:=1 to len2 do
    c[i+j-1]:=a[i]*b[j]+c[i+j-1];
  for i:=1 to len1 do
   c[i]:=c[i]-a[i];
  for i:=1 to len2 do
   c[i]:=c[i]-b[i];
  len:=len1+len2-1;
  for i:=1 to len-1 do
   begin
     if c[i]>10 then begin c[i+1]:=c[i+1]+c[i] div 10;c[i]:=c[i] mod 10;end;
     while c[i]<0 do
      begin
        dec(c[i+1]);inc(c[i],10);
      end;
   end;
  while c[len]>10 do
   begin
     inc(len);c[len]:=c[len-1] div 10;c[len-1]:=c[len-1] mod 10;
   end;
  for i:=len downto 1 do
   write(c[i]);writeln;
close(input);close(output);
end.

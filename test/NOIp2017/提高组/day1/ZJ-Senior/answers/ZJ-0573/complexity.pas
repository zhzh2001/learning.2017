const maxn=1005;
var
  st:string;
  used:array['a'..'z'] of boolean;
  o:array[0..maxn,0..2] of longint;
  s:array[0..maxn] of string;
  oo,x,s1,tt,i,j,t,chong,min,top,pp:longint;
  ch1,ch:char;
  pd:boolean;
begin
assign(input,'complexity.in');reset(input);
assign(output,'complexity.out');rewrite(output);
  readln(t);
  while t>0 do
   begin
     readln(st);
     i:=1;s1:=0;
     while (st[i]>='0') and (st[i]<='9') do
      begin
        s1:=s1*10+ord(st[i])-48;  inc(i);
      end;
     i:=pos('O',st); oo:=0;
     if st[i+2]='1' then oo:=-1 else
      begin
       i:=i+4;
       while (st[i]>='0') and (st[i]<='9') do
        begin
          oo:=oo*10+ord(st[i])-48;  inc(i);
        end;
       end;
      for ch1:='a' to 'z' do
      used[ch1]:=false;pd:=true;chong:=0;min:=-1;pp:=-1;
      fillchar(o,sizeof(o),0); tt:=-1;  top:=0;
      for i:=1 to s1 do
       begin
        readln(s[i]);
        if s[i,1]='F' then inc(chong) else dec(chong);
       end;
      if chong<>0 then begin dec(t);writeln('ERR');continue;end;
      for i:=1 to s1 do
       begin
         if s[i,1]='F' then
          begin
            inc(top);
            if used[s[i,3]] then begin pd:=false;break;end
            else used[s[i,3]]:=true;
            if pp<>-1 then begin inc(pp);continue;end;
            delete(s[i],1,4);
            j:=1;x:=0;
            if (s[i,1]>='0') and (s[i,1]<='9') then
            begin
             while (s[i,j]>='0') and (s[i,j]<='9') do
              begin x:=x*10+ord(s[i,j])-48;inc(j);end;
             o[top,1]:=x;
            end else o[top,1]:=-1;
            delete(s[i],1,j);
            while s[i,1]=' ' do delete(s[i],1,1);
            j:=1;x:=0;
            if (s[i,1]>='0') and (s[i,1]<='9') then
            begin
             while (s[i,j]>='0') and (s[i,j]<='9') do
              begin x:=x*10+ord(s[i,j])-48;inc(j);end;
             o[top,2]:=x;
            end else o[top,2]:=-1;

            if (o[top,1]=-1) and (o[top,2]<>-1) or
               (o[top,1]<>-1) and (o[top,2]<>-1) and (o[top,1]>o[top,2]) then
                begin pp:=1; continue; end;
            if not ((o[top,1]=-1) and (o[top,2]=-1)) then
            if (o[top,1]=-1) or (o[top,2]=-1) then begin
            if tt=-1 then tt:=1 else inc(tt); end;
          end else
          begin
            dec(top);
            if pp<>-1 then begin dec(pp);if pp=0 then pp:=-1;continue;end;
            if tt>min then min:=tt;
            if top=0 then begin tt:=-1;for ch1:='a' to 'z' do used[ch1]:=false;end;
          end;
       end;
       if pd=false then writeln('ERR') else
        begin if min=oo then writeln('Yes') else writeln('No');end;
       dec(t);
     end;
close(input);close(output);
end.

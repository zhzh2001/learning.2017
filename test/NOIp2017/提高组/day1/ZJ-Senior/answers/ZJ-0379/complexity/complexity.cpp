#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
#include<cstdio>
#include<cstdlib>
using namespace std;
char rd[10],pro[17][107][50];
int t,cop_in[17],complex[17],lenth[17],cnt=0;
void init(int x)
{
	char ch;
	int tmp=0;
	cin>>lenth[x];
	while (1)
	{
		ch=getchar();
		if (ch=='\n') break;
		rd[++tmp]=ch;
		}
	if (rd[4]=='1') cop_in[x]=0;
		else cop_in[x]=rd[6]-'0';
	for (int i=1;i<=lenth[x];i++)
	{
		tmp=0;
		while (1)
		{
			ch=getchar();
			if (ch=='\n') break;
			pro[x][i][++tmp]=ch;
			}
		}
	}
int dfs(int x,int m)
{
	cnt=0;
	
	if (pro[x][m][1]=='F'&&pro[x][m][5]>='0'&&pro[x][m][5]<='9'&&pro[x][m][7]>='0'&&pro[x][m][7]<='9')
		cnt=0;
	if (pro[x][m][1]=='F'&&pro[x][m][5]>='0'&&pro[x][m][5]<='9'&&pro[x][m][7]=='n')
		cnt=1;
	if (pro[x][m][1]=='F'&&pro[x][m][5]=='n'&&pro[x][m][7]>='0'&&pro[x][m][7]<='9')
		cnt=0;
	if (pro[x][m+1][1]=='F') cnt+=dfs(x,m+1);
	if (pro[x][m+1][1]=='E') return cnt;
	}
int solve(int x)
{
	int maxx=0;
	for (int i=1;i<=lenth[x];i++)
	{
		maxx=max(maxx,dfs(x,i));
		}
	return maxx;
	}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>t;
	for (int i=1;i<=t;i++)
		init(i);
	for (int i=1;i<=t;i++)
	{
		if (solve(i)==cop_in[i]) {cout<<"YES"<<endl;}
			else {cout<<"NO"<<endl;}
		}
	fclose(stdin);
	fclose(stdout);
	return 0;
	}
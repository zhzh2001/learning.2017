#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
#include<cstdio>
#include<cstdlib>
using namespace std;
const int INF=10000007;
queue <int> q;
queue <int> q1;
int T,n,m,k,p,len;
struct eg
{
	int to,w,next;
	} edge[200007];
int first[100007],iter=0;
int d[100007],dp[100007],ans[100007];
bool flag[100007],vis[100007],f;
void addedge(int x,int y,int z)
{
	edge[++iter].next=first[x]; edge[iter].to=y; edge[iter].w=z; first[x]=iter;
	}
void spfa()
{
	for (int i=1;i<=n;i++)
		d[i]=INF;
	d[1]=0;
	q.push(1);
	while (!q.empty())
	{
		int u;
		u=q.front();
		q.pop();
		flag[u]=false;
		for (int i=first[u];i!=0;i=edge[i].next)
		{
			int v=edge[i].to;
			if (d[v]>d[u]+edge[i].w)
				d[v]=d[u]+edge[i].w;
			if (!flag[v])
			{
				q.push(v);
				flag[v]=true;
				}
			}
		}
	}


int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>T;
		for (int x=1;x<=T;x++)
		{
		memset(dp,0,sizeof(dp));
		f=false;
		memset(d,0,sizeof(d));
		memset(edge,0,sizeof(edge));
		iter=0; 
		memset(first,0,sizeof(first));
		memset(vis,false,sizeof(vis));
		memset(flag,false,sizeof(flag));
		cin>>n>>m>>k>>p;
		int a,b,c;
		for (int i=1;i<=m;i++)
		{
			cin>>a>>b>>c;
			addedge(a,b,c);
			}
		spfa();
		len=d[n]+k;
		q1.push(1);
	while (!q1.empty())
	{
		int u=q1.front();
		q1.pop();
		for (int i=first[u];i!=0;i=edge[i].next)
		{
			int to=edge[i].to;
			if (d[u]+edge[i].w<=len)
				dp[to]++;
			if (!vis[to])
			{
			q1.push(to);
			vis[to]=true;
				}
			}
		}
	ans[x]=dp[n]%p;
	}
	for (int i=1;i<=T;i++)
		cout<<ans[i]<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
	}
#include<bits/stdc++.h>
#define M 5005
#define N 5005
#define oo 1E9
using namespace std;
int T,n,m,k,p,ml,ans,cnt,Q[M],C[M],head[N],dis[N];
struct troye{
	int to,nxt,val;
}E[N];
bool vis[N];
void read(int &v){
	char ch,fu=0;
	for (ch='*';(ch<'0'|| ch>'9')&& ch!='-';ch=getchar());
	if (ch=='-') fu=1,ch=getchar();
	for (v=0;ch>='0' && ch<='9';ch=getchar()) v=v*10+ch-'0';
	if (fu) v=-v;
}
void add(int u,int v,int l){
	E[++cnt].to=v;
	E[cnt].nxt=head[u];
	E[cnt].val=l;
	head[u]=cnt;
}
int SPFA(){
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=n;i++) dis[i]=oo;
	Q[0]=1;
	int h=0,tail=1;
	dis[1]=0;
	while (h!=tail){
		int u=Q[h];
		h=(h+1)%M;
		vis[u]=0;
		for (int i=head[u];i;i=E[i].nxt){
			int v=E[i].to;
			if (dis[u]+E[i].val<dis[v]){
			dis[v]=dis[u]+E[i].val;
			if (!vis[v]){
				vis[v]=1;
				Q[tail]=E[i].to;
				tail=(tail+1)%M;
			}
		}
	}
	}
	return dis[n];
}
void BFS(){
	Q[1]=1;
	int t=1,h=1;
	while (h<=t){
		int u=Q[h];
		for (int i=head[u];i;i=E[i].nxt){
			if (C[h]+E[i].val>ml) continue;
			Q[++t]=E[i].to;
			if (E[i].to==n) ans=(ans+1)%p;
			C[t]=C[h]+E[i].val;
		}
		h++;
	}
}
void deal(){
	ans=0;
	read(n);read(m);read(k);read(p);
	for (int i=1;i<=m;i++){
		int u,v,l;
		read(u);read(v);read(l);
		add(u,v,l);
	}
	ml=SPFA();
	ml+=k;
	BFS();
	printf("%d\n",ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while (T--) deal();
	return 0;
}
/*
2
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3


*/

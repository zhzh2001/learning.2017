#include<cstdio>
#include<cstdlib>
#include<queue>
#include<cctype>
#include<cstring>
const int N=100005,M=200005,K=52,inf=1<<29;
int n,m,k,p,i,a,b,c,T,s,t;
inline void read(int&x){
	static char c;
	for(c=getchar();!isdigit(c);c=getchar());
	for(x=0;isdigit(c);c=getchar())x=x*10+c-48;
}
struct graph1{
	struct edge{
		int to,next,w;
	}e[M];
	int h[N],n,xb,f[N][K][2],d[N];//f[i][j][k]->cong i chu fa,bi zui duan lu da k ,0/1 jing guo 0 scc,lu jing shu
	bool b[N],bb[N][K][2],vi[N][K],bbb[N];
	inline void init(int n){
		this->n=n;memset(h+1,xb=0,n<<2);memset(bbb+1,0,n);memset(b+1,0,n);
		for(int i=1;i<=n;++i)for(int j=0;j<=k;++j)f[i][j][0]=f[i][j][1]=0,bb[i][j][0]=bb[i][j][1]=vi[i][j]=0;
	}
	inline void addedge(int u,int v,int w){e[++xb]=(edge){v,h[u],w};h[u]=xb;}
	struct node{
		int d,v;
		inline bool operator<(const node&rhs)const{return d>rhs.d;}
	};
	inline void dijstra(int s){
		static std::priority_queue<node> q;
		static int i,x,u;
		for(i=1;i<=n;++i)d[i]=inf;d[s]=0;
		q.push((node){0,s});
		while(!q.empty()){
			x=q.top().d,u=q.top().v;q.pop();
			if(bbb[u])continue;bbb[u]=1;
			for(i=h[u];i;i=e[i].next)
				if(e[i].w+x<d[e[i].to])
					q.push((node){d[e[i].to]=e[i].w+x,e[i].to});
		}
	}
	void calc(int x,int y){
		if(vi[x][y])return;vi[x][y]=1;
		for(int i=h[x];i;i=e[i].next){
			int dd=d[x]+e[i].w-d[e[i].to];
			if(y>=dd){
				calc(e[i].to,y-dd);
				if(b[x]){
					f[x][y][1]=(1ll*f[e[i].to][y-dd][1]+f[e[i].to][y-dd][0]+f[x][y][1])%p;
					bb[x][y][1]|=bb[e[i].to][y-dd][0] || bb[e[i].to][y-dd][1];
				}else{
					f[x][y][1]=(f[e[i].to][y-dd][1]+f[x][y][1])%p;
					f[x][y][0]=(f[e[i].to][y-dd][0]+f[x][y][0])%p;
					bb[x][y][0]|=bb[e[i].to][y-dd][0];
					bb[x][y][1]|=bb[e[i].to][y-dd][1];
				}
			}
		}
	}
	inline void work(){
		if(b[t]){puts("-1");return;}
		dijstra(s);
		f[t][0][0]=1;bb[t][0][0]=1;vi[t][0]=1;static int i,ans;
		for(i=ans=0;i<=k;++i){
			calc(s,i);
			if(bb[s][i][1]){puts("-1");return;}
			ans=(ans+f[s][i][0])%p;
		}
		printf("%d\n",ans);
	}
}g1;
struct graph2{
	struct edge{int to,next,w;}e[M];
	int h[N],n,xb,dfn[N],low[N],st[N],w,scc[N],cnt,sz[N];
	inline void init(int n){
		memset(h+1,xb=0,n<<2);this->n=n;
	}
	inline void addedge(int u,int v,int w){e[++xb]=(edge){v,h[u],w};h[u]=xb;}
	void dfs(int x){
		dfn[x]=low[x]=++xb;st[++w]=x;
		for(int i=h[x];i;i=e[i].next)
			if(!e[i].w){
				if(!dfn[e[i].to]){
					dfs(e[i].to);
					if(low[e[i].to]<low[x])low[x]=low[e[i].to];
				}else if(!scc[e[i].to] && low[e[i].to]<low[x])low[x]=low[e[i].to];
			}
		if(low[x]==dfn[x]){
			for(sz[scc[x]=++cnt]=1;st[w]!=x;)scc[st[w--]]=cnt,++sz[cnt];
			--w;
		}
	}
	inline void reb(graph1&g){
		static int i,j;
		/*g1.init(n);
		for(i=1;i<=n;++i)
			for(j=h[scc[i]=i];j;j=e[j].next)
				g.addedge(i,e[j].to,e[j].w);*/
		memset(scc+1,0,n<<2);memset(dfn+1,0,n<<2);
		for(xb=cnt=0,i=1;i<=n;++i)
			if(!dfn[i])
				dfs(i);
		g1.init(cnt);
		for(i=1;i<=n;++i){
			if(sz[scc[i]]>1)g.b[scc[i]]=1;
			for(j=h[i];j;j=e[j].next)if(scc[i]!=scc[e[j].to])g.addedge(scc[i],scc[e[j].to],e[j].w);
		}
	}
}g2;
int main(){
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	read(T);
	while(T--){
		read(n),read(m),read(k),read(p);g2.init(n);
		for(i=1;i<=m;++i)read(a),read(b),read(c),g2.addedge(a,b,c);
		g2.reb(g1);
		s=g2.scc[1];t=g2.scc[n];
		g1.work();
	}
	return 0;
}

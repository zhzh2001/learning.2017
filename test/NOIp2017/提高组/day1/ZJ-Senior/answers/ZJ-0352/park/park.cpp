#include<bits/stdc++.h>
using namespace std;
#define MN 100005
#define pii pair<int,int>
#define sd second
#define mp make_pair
#define ft first
int n,m,k,p;
int fr[MN],nex[MN<<1],vi[MN<<1],wi[MN<<1],tot;
int Fr[MN],Nex[MN<<1],Vi[MN<<1],Wi[MN<<1],Tot;
int dp[MN][55];
int xyx[MN],totxy=0;
int d[MN],vd[MN],dn[MN],Fd[MN];
priority_queue<pii >Q;
queue<int> q;
void add(int x,int y,int z){
	vi[++tot]=y;wi[tot]=z;nex[tot]=fr[x];fr[x]=tot;
	Vi[++Tot]=x;Wi[Tot]=z;Nex[Tot]=Fr[y];Fr[y]=Tot;
}
void dj(){
	memset(d,0x2f,sizeof d);
	memset(vd,0,sizeof vd);
	d[1]=0;Q.push(mp(0,1));
	while(!Q.empty()){
		pii P=Q.top();Q.pop();int x=P.sd;
		if(vd[x])continue;
		vd[x]=1;
		for(int i=fr[x];i;i=nex[i]){
			if(d[vi[i]]>d[x]+wi[i]){
				d[vi[i]]=d[x]+wi[i];
				Q.push(mp(-d[vi[i]],vi[i]));
			}
		}
	}
}
void init(){
	memset(xyx,0,sizeof xyx);
	memset(Fd,0,sizeof Fd);
	memset(fr,0,sizeof fr);
	memset(nex,0,sizeof nex);
	memset(vi,0,sizeof vi);
	memset(wi,0,sizeof wi);
	memset(Fr,0,sizeof Fr);
	memset(Nex,0,sizeof Nex);
	memset(Vi,0,sizeof Vi);
	memset(Wi,0,sizeof Wi);
	memset(dp,0,sizeof dp);
	tot=0,Tot=0;totxy=0;
}
void djn(){
	memset(dn,0x2f,sizeof dn);
	memset(vd,0,sizeof vd);
	dn[n]=0;Q.push(mp(0,n));
	while(!Q.empty()){
		pii P=Q.top();Q.pop();int x=P.sd;
		if(vd[x])continue;
		vd[x]=1;
		for(int i=Fr[x];i;i=Nex[i]){
			if(dn[Vi[i]]>dn[x]+Wi[i]){
				dn[Vi[i]]=dn[x]+Wi[i];
				Q.push(mp(-dn[Vi[i]],Vi[i]));
			}
		}
	}
}
bool dfs(int x){
	vd[x]=1;
	for(int i=fr[x];i;i=nex[i]){
		if(wi[i]==0&&(!Fd[vi[i]])){
			if(vd[vi[i]]==1)return 1;else if(dfs(vi[i]))return 1;
		}
	}
	Fd[x]=1;vd[x]=0;return 0;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		init();
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1,x,y,z;i<=m;++i){
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		dj();djn();
		int F=0;
		for(int i=1;i<=n;++i){
			memset(vd,0,sizeof vd);
			if(d[i]+dn[i]<=d[n]+k&&(!Fd[i])){
				if(dfs(i)){
					F=1;
					break;
				}
			}
		}
		if(F==1){
			puts("-1");continue;
		}
		memset(Fd,0,sizeof Fd);
		for(int i=1;i<=n;++i){
			for(int j=fr[i];j;j=nex[j])if(wi[j]==0)++Fd[vi[j]];
		}
		for(int i=2;i<=n;++i)if(Fd[i]==0)q.push(i);
		xyx[++totxy]=1;
		for(int i=fr[1];i;i=nex[i])if(wi[i]==0){
			if(!(--Fd[vi[i]]))q.push(vi[i]);
		}
		while(!q.empty()){
			int x=q.front();q.pop();
			xyx[++totxy]=x;
			for(int i=fr[x];i;i=nex[i])if(wi[i]==0){
				if(!(--Fd[vi[i]]))q.push(vi[i]);
			}
		}
//		cerr<<xyx[1]<<endl;
		dp[1][0]=1;
		for(int i=0;i<=k;++i){
			for(int j=1;j<=n;++j){
				int z=xyx[j];
				if(dp[z][i]==0)continue;
				for(int l=fr[z];l;l=nex[l]){
//					cerr<<vi[l]<<"!";
					int vvv=d[z]+i-d[vi[l]]+wi[l];
//					cerr<<i<<' '<<z<<' '<<vvv<<endl;
					if(vvv<=k){
						dp[vi[l]][vvv]+=dp[z][i];
						dp[vi[l]][vvv]%=p;
					}
				}
			}
		}
		int res=0;
		for(int i=0;i<=k;++i)res=(res+dp[n][i])%p;
		printf("%d\n",res);
	}
	return 0;
}

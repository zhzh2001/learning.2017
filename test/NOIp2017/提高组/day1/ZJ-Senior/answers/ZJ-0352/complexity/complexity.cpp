#include<bits/stdc++.h>
using namespace std;
int n,F;
int ud[2000];
char bz[10];
int isn[1000],isv[1000],isl[1000];
char var[10],head[10],tail[10];
char fzd[99];
void init(){
	memset(ud,0,sizeof ud);
	memset(isn,0,sizeof isn);
	memset(isv,0,sizeof isv);
	memset(isl,0,sizeof isl);
}
bool jg(){
	int v1=0,v2=0;
	int sz1=strlen(head),sz2=strlen(tail);
	for(int i=0;i<sz1;++i)v1=v1*10+head[i]-'0';
	for(int i=0;i<sz2;++i)v2=v2*10+tail[i]-'0';
	return v1>v2;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		init();
		scanf("%d",&n);
		scanf("%s",fzd+1);
		if(strlen(fzd+1)==4)F=0;
		else{
			F=0;
			int V=strlen(fzd+1);
			for(int i=5;i<V;++i)F=F*10+fzd[i]-'0';
		}
		int res=0;int now=0,x=0;bool fcd=0;int locked=0;
		for(int i=1;i<=n;++i){
			scanf("%s",bz);
			if(bz[0]=='F'){
				scanf("%s",var);
				int v=var[0];
				if(ud[v]==1)fcd=1;
				++now;ud[v]=1;isv[now]=v;
				scanf("%s",head);scanf("%s",tail);
				if(head[0]=='n')locked++;
				if(head[0]!='n'&&tail[0]!='n'&&jg())locked++;
				if(tail[0]=='n'&&!locked){
					++x;res=max(res,x);isn[now]=1;
				}
			}else{
				if(now==0)fcd=1;
				if(isn[now]){
					--x;isn[now]=0;
				}
				if(isl[now]){
					--locked;isl[now]=0;
				}
				ud[isv[now]]=0;isv[now]=0;--now;
			}
		}
		if(fcd==1||now!=0)puts("ERR");
		else if(res==F)puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
int t,hang;
char fuzadu[50],chengxu[200];
bool used[100];
int chuli[1000],stk[1000],had[1000];
int anss[1000];
void solve()
{
	for(int i=hang;i>=1;i--){
		if(anss[i]==-1) continue;
		for(int j=i+1;j<had[i];j++) chuli[i]=max(chuli[i],chuli[j]);
		if(anss[i]==2) {
		   if(chuli[i]==-1) chuli[i]=1; else chuli[i]++;
	    }
	    else if(anss[i]==1){
	    	if(chuli[i]==-1) chuli[i]=0; 
		}
		else if(anss[i]==0) chuli[i]=-1;
	}
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		int hefa=0,ans=0,isn=0; memset(used,false,sizeof used); bool flag=true;
		scanf("%d",&hang); 
		scanf("%s",fuzadu); int k=0;
		while( fuzadu[k]!='1' && fuzadu[k]!='n' ) k++;
		if(fuzadu[k]=='n'){
			while(fuzadu[k]<'0'||fuzadu[k]>'9') k++;
			while(fuzadu[k]>='0'&&fuzadu[k]<='9') {
				ans=ans*10+fuzadu[k]-'0'; k++;
			}
		}
		for(int i=1;i<=hang;i++)
		{
			anss[i]=0;
			cin >> chengxu; 
			if(chengxu[0]=='F') hefa++; else if(chengxu[0]=='E') hefa--;
			if(hefa<0)  flag=false;  
			if(chengxu[0]=='E') {
				anss[i]=-1; continue;
			}
			cin >> chengxu; int j=0; while(chengxu[j]<'a' && chengxu[j]>'z') j++;
			int tmp=chengxu[j]-'a'; 
			if(used[tmp])  flag=false;  else used[tmp]=true; 
			int qian=-1,hou=-1;
			cin >> chengxu; j=0; while( (chengxu[j]<'0'||chengxu[j]>'9') && chengxu[j]!='n' ) j++;
			if(chengxu[j]>='0'&&chengxu[j]<='9'){
				qian=0;
				while(chengxu[j]>='0'&&chengxu[j]<='9'){
					qian=qian*10+chengxu[j]-'0'; j++;
				}
			}
			cin >> chengxu;  j=0; while( (chengxu[j]<'0'||chengxu[j]>'9') && chengxu[j]!='n' ) j++;
			if(chengxu[j]>='0'&&chengxu[j]<='9'){
				hou=0;
				while(chengxu[j]>='0'&&chengxu[j]<='9'){
					hou=hou*10+chengxu[j]-'0'; j++;
				}
			}
			if(qian!=-1 && hou==-1) anss[i]=2;
			else if(qian==-1 && hou==-1) anss[i]=1;
			else if(qian==-1 && hou!=-1) anss[i]=0;
			else if(qian!=-1 && hou!=-1) {
				if(qian>hou) anss[i]=0; else anss[i]=1;
			}
		}
		if(flag==false || hefa>0) printf("ERR\n");
		else if(flag==true){ 
		    memset(had,-1,sizeof had);
		    memset(chuli,-1,sizeof chuli);int top=0;
		    for(int i=1;i<=hang;i++){
		    	if(anss[i]!=-1) stk[++top]=i;
				else {
					had[ stk[top] ]=i; top--;
				}
			}
			solve();int jiafa=0;
		    for(int i=1;i<=hang;i++) {
			    if(jiafa==0) isn=max(isn,chuli[i]);
			    if(anss[i]!=-1) jiafa++;else jiafa--;
			}
			if(isn==-1) isn=0;
		    if(isn==ans) printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}

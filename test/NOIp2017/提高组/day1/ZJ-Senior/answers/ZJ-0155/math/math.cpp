#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int a,b,ans;
void exgcd(long long a,long long b,long long &x,long long &y)
{
	if(a==1 && b==0){
		x=1; y=0;
		return;
	} 
	exgcd(b,a%b,x,y);
	long long tmp=x;
	x=y; 
	y=tmp-a/b*x;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a<b) swap(a,b);
	long long x=0,y=0;
	long long ans;
	exgcd(a,b,x,y); printf("%lld %lld\n",x,y);
	if(x<0 && y>0){
		ans=x*a; ans=-ans; ans--;
	}
	else if(x>0 && y<0){
		ans=b*y; ans=-ans; ans--;
	}
	printf("%lld\n",a+b+1);
	return 0;
}

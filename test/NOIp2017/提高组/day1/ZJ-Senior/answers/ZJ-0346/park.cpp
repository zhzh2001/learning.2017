#include<cstdio>
#include<iostream>
#define mod 5000000
using namespace std;
int tot,Head[100005],ret[200005],len[200005],Next[200005];
int dis[100005],d[100005][51],q[5000105],st,ed,sum[100005];
int n,m,k,p,u,v,w,Case,ans;
bool inq[100005],flag;
void ins(int u,int v,int w)
{
	ret[++tot]=v;len[tot]=w;
	Next[tot]=Head[u];Head[u]=tot;
}
void spfa1()
{
	q[1]=1;st=0;ed=1;inq[1]=1;
	for (int i=1;i<=n;i++) dis[i]=1000000005,inq[i]=0;dis[1]=0;
	while (st<ed)
	{
		st++;
		int now=q[st%mod];
		for (int i=Head[now];i;i=Next[i])if (dis[now]+len[i]<dis[ret[i]])
		{
			dis[ret[i]]=dis[now]+len[i];
			if (!inq[ret[i]])
			{
				inq[ret[i]]=1;ed++;
				q[ed%mod]=ret[i];
			}
		}
		inq[now]=0;
	}
}
void spfa2()
{
	q[1]=1;st=0;ed=1;
	for (int i=1;i<=n;i++)
	{
		for (int j=0;j<=k;j++) d[i][j]=0;
		inq[i]=0;
		sum[i]=0;
	}
	d[1][0]=1;
	inq[1]=1;sum[1]=1;
	while (st<ed)
	{
		st++;
		int now=q[st%mod];
		for (int tim=0;tim<=k;tim++)
		{
			for (int i=Head[now];i;i=Next[i])
			{
				int x=tim+dis[now]+len[i]-dis[ret[i]];
				if (x>k||d[now][tim]==0) continue;
				(d[ret[i]][x]+=d[now][tim])%=p;
				if (!inq[ret[i]])
				{
					inq[ret[i]]=1;sum[ret[i]]++;if (sum[ret[i]]>n){flag=1;return;}
					ed++;q[ed%mod]=ret[i];
				}
			}
		}
		inq[now]=0;
		if (now==n)
		{
			for (int i=0;i<=k;i++) (ans+=d[n][i])%=p,d[now][i]=0;
		}
		else for (int i=0;i<=k;i++) d[now][i]=0;
	}
}
void solve()
{
	scanf("%d%d%d%d",&n,&m,&k,&p);tot=0;
	for (int i=1;i<=n;i++) Head[i]=0;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&u,&v,&w);
		ins(u,v,w);
	}
	spfa1();flag=0;ans=0;
	spfa2();if (flag){puts("-1");return;}
	printf("%d\n",ans);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&Case);
	while (Case--) solve();
	return 0;
}

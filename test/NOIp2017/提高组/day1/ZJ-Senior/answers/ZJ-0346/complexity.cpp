#include<cstdio>
#include<iostream>
#include<cstring>
#include<map>
using namespace std;
char ch[105],x[105],y[105];
string s[105];
int n,N,ERR,top,stk[1000],f[1000],O[1000],a,b,Case;
map<string,bool>mp;
void getO()
{
	gets(ch+1);int len=strlen(ch+1),x=0;bool flag=0;
	for (int i=1;i<=len;i++)
	{
		if (ch[i]=='^') flag=1;
		if (ch[i]>='0'&&ch[i]<='9') x=x*10+ch[i]-'0';
	}
	if (!flag) N=0;else N=x;
}
int number(char ch[105])
{
	if (ch[0]=='n') return 101;
	int len=strlen(ch),x=0;
	for (int i=0;i<len;i++) x=x*10+ch[i]-'0';
	return x; 
}
void read(char ch)
{
	while (ch!='\n') ch=getchar();
}
void solve()
{
	scanf("%d",&n);top=ERR=0;mp.clear();
	for (int i=0;i<=105;i++) O[i]=f[i]=stk[i]=0;
	getO();
	for (int i=1;i<=n;i++)
	{
		scanf("%s",ch);
		if (ERR){read(ch[0]);continue;}
		if (ch[0]=='E')
		{
			if (top==0){ERR=1;read(ch[0]);continue;}if (O[stk[top]]==-1) O[stk[top]]=0;
			if (f[stk[top-1]]!=-1) O[stk[top-1]]=max(O[stk[top-1]],O[stk[top]]+f[stk[top-1]]);
			O[stk[top]]=f[stk[top]]=0;
			mp[s[stk[top]]]=0;
			top--;
		}
		else
		{
			stk[++top]=i;
			scanf("%s",ch);
			if (mp[ch]){ERR=1;read(ch[0]);continue;}
			mp[ch]=1;s[i]=ch;
			scanf("%s",x);scanf("%s",y);
			a=number(x);b=number(y);
			if (a>b) f[i]=O[i]=-1;
			else if (b==101&&a!=101) f[i]=O[i]=1;
			else f[i]=O[i]=0;
		}
	}
	if (O[0]==-1) O[0]=0;
	if (ERR||top) puts("ERR");
	else if (O[0]==N) puts("Yes");
	else puts("No");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&Case);
	while (Case--)solve();
	return 0;
}

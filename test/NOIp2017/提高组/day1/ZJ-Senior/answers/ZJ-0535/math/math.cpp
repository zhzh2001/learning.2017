#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

int n,m;
long long a[200001];

bool npc(long long o,long long p)
{
	return o<p;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int t;
	scanf("%d %d",&n,&m);
	if (n>m) t=n,n=m,m=t;
	for (int i=0;i<=1000;i++)
		for (int j=0;j<=100;j++)
			a[100*i+j]=1ll+1ll*n*i+1ll*m*j-1;
	sort(a,a+100001,npc);	
	int l=70001;
	while (a[l]-a[l-1]<=1) l--;
	printf("%lld",a[l]-1);
	return 0; 
}

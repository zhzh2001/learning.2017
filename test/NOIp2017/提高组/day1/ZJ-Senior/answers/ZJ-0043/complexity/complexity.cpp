#include <bits/stdc++.h>
using namespace std;
int e[30],fl,T,n,x,y,M,h,f[106],d[106],op[106][3]; char S[10];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%s",&n,S);
		if (S[2]=='1') M=0; else
		if (S[5]==')') M=S[4]-'0'; else
			M=(S[4]-'0')*10+S[5]-'0';
		fl=1; h=0;
		for (int i=0;i<=26;++i) e[i]=0;
		for (int i=1;i<=n;++i){
			scanf("%s",S);
			if (S[0]=='F'){
				op[i][0]=1;
				scanf("%s",S);
				op[i][1]=S[0]-'a'+1;
				scanf("%s",S);
				if (S[0]=='n') x=0; else
				if (!isdigit(S[1])) x=S[0]-'0'; else
					x=(S[0]-'0')*10+S[1]-'0';
				scanf("%s",S);
				if (S[0]=='n') y=0; else
				if (!isdigit(S[1])) y=S[0]-'0'; else
					y=(S[0]-'0')*10+S[1]-'0';
				if (x&&y) op[i][2]=(x<=y); else
				if (x&&!y) op[i][2]=2; else
					op[i][2]=!y;		//attention
				--op[i][2];
			}else op[i][0]=-1;
			if (op[i][0]>0){
				d[++h]=op[i][1];
				if (e[d[h]]) fl=0;
				++e[d[h]];
			}else{
				--e[d[h]]; --h;
			}
			if (h<0) fl=0;
		}
		if (h>0) fl=0;
		if (!fl) {puts("ERR"); continue;}
		f[0]=0;
		for (int i=1;i<=n;++i){
			if (op[i][0]>0){
				d[++h]=op[i][2]; f[h]=0;
			}else{
				if (d[h]>=0) d[h]+=f[h];
				f[h-1]=max(f[h-1],d[h]);
				--h;
			}
		}
		if (f[0]==M) puts("Yes"); else puts("No");
	}
	return 0;
}

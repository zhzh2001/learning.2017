#include <bits/stdc++.h>
using namespace std;
const int inf=2000000000;
struct O{int u,v,c;}A[200005];
int K,mo,n,m,t,l,r,fl,T,ans,cnt,x,y,z,a[55][100005],d[200005],f[100005],F[100005],e[100005];
int dfn[100005],low[100005],inq[100005],be[100005],sz[100005],R[100005];
vector <int> E[100005],g[100005],G[100005],W[100005],w[100005];
void popf(int x){
	if (!e[x]) return;
	if (e[x]==t) {e[x]=0; --t; return;}
	int i=e[x]; e[x]=0; x=d[t--];
	while (i&&f[x]<f[d[i>>1]]) d[i]=d[i>>1],i>>=1;
	while (i<<1<t&&f[x]>f[d[i<<1^1]]||i<<1<=t&&f[x]>f[d[i<<1]])
		if (i<<1==t||f[d[i<<1]]<f[d[i<<1^1]])
		d[i]=d[i<<1],i<<=1; else d[i]=d[i<<1^1],i=i<<1^1;
	d[i]=x; e[x]=i;
}
void pushf(int x){
	int i=++t;
	while (i&&f[x]<f[d[i>>1]]) d[i]=d[i>>1],i>>=1;
	d[i]=x; e[x]=i;
}
void popF(int x){
	if (!e[x]) return;
	if (e[x]==t) {e[x]=0; --t; return;}
	int i=e[x]; e[x]=0; x=d[t--];
	while (i&&F[x]<F[d[i>>1]]) d[i]=d[i>>1],i>>=1;
	while (i<<1<t&&F[x]>F[d[i<<1^1]]||i<<1<=t&&F[x]>F[d[i<<1]])
		if (i<<1==t||F[d[i<<1]]<F[d[i<<1^1]])
		d[i]=d[i<<1],i<<=1; else d[i]=d[i<<1^1],i=i<<1^1;
	d[i]=x; e[x]=i;
}
void pushF(int x){
	int i=++t;
	while (i&&F[x]<F[d[i>>1]]) d[i]=d[i>>1],i>>=1;
	d[i]=x; e[x]=i;
}
void dfs(int x){
	dfn[x]=low[x]=++t;
	d[++r]=x; inq[x]=1; int y;
	for (int i=0;i<E[x].size();++i){
		y=E[x][i];
		if (!dfn[y]) dfs(y),low[x]=min(low[x],low[y]);
		else if (inq[y]) low[x]=min(low[x],dfn[y]);
	}
	if (low[x]==dfn[x]){
		++cnt; d[r+1]=0;
		while (d[r+1]!=x)
			be[d[r]]=cnt,inq[d[r--]]=0,++sz[cnt];
	}
}
bool cmp(O a,O b){
	return a.u==b.u?(a.v==b.v?a.c<b.c:a.v<b.v):a.u<b.u;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		for (int i=1;i<=n;++i) g[i].clear(),w[i].clear();
		for (int i=1;i<=n;++i) G[i].clear(),W[i].clear();
		for (int i=1;i<=n;++i) E[i].clear(),R[i]=0;
		for (int i=1;i<=n;++i) dfn[i]=low[i]=0;
		for (int i=1;i<=cnt;++i) sz[i]=0;
		scanf("%d%d%d%d",&n,&m,&K,&mo);
		for (int i=1;i<=m;++i){
			scanf("%d%d%d",&x,&y,&z);
			A[i].u=x; A[i].v=y; A[i].c=z;
			g[x].push_back(y); w[x].push_back(z);
			G[y].push_back(x); W[y].push_back(z);
			if (!z) E[x].push_back(y);
		}

		for (int i=1;i<=n;++i) f[i]=inf;
		d[1]=e[1]=t=1; f[1]=0; int CNT=0;
		while (t){
			++CNT; if (CNT>100000) {
		//		printf("%d\n",CNT);
			}
			x=d[1]; popf(x);
			for (int i=0;i<g[x].size();++i){
				y=g[x][i];
				if (f[y]<=f[x]+w[x][i]) continue;
				popf(y); 
				f[y]=f[x]+w[x][i];
				if (y==16216){
	//				puts("");
				}
				pushf(y);
			}
		}
		
		for (int i=1;i<=n;++i) F[i]=inf;
		d[1]=n; e[n]=t=1; F[n]=0;
		while (t){
			x=d[1]; popF(x);
			for (int i=0;i<G[x].size();++i){
				y=G[x][i];
				if (F[y]<=F[x]+W[x][i]) continue;
				F[y]=F[x]+W[x][i];
				popF(y); pushF(y);
			}
		}
				
		r=cnt=t=0;
		for (int i=1;i<=n;++i) if (!dfn[i]) dfs(i);
		
		fl=0;
		for (int i=1;i<=n;++i) if (sz[be[i]]>1)
			if (f[i]+F[i]<=f[n]+K) {fl=1; break;}
		if (fl) {puts("-1"); continue;}
		
		for (int i=1;i<=m;++i) A[i].u=be[A[i].u],A[i].v=be[A[i].v];
		sort(A+1,A+m+1,cmp); t=0;
		for (int i=1;i<=m;++i)
		if (A[i].u!=A[i].v&&(A[i].u!=A[t].u||A[i].v!=A[t].v)) A[++t]=A[i];
		m=t;
		for (int i=1;i<=n;++i) F[be[i]]=f[i];
		for (int i=1;i<=n;++i) f[i]=F[i];
		for (int i=1;i<=n;++i) g[i].clear(),w[i].clear(),E[i].clear();
		for (int i=1;i<=m;++i){
			g[A[i].u].push_back(A[i].v);
			w[A[i].u].push_back(A[i].c-f[A[i].v]+f[A[i].u]);
		//	printf("%d %d %d\n",A[i].u,A[i].v,A[i].c-f[A[i].v]+f[A[i].u]);
			if (A[i].c-f[A[i].v]+f[A[i].u]==0)
				E[A[i].u].push_back(A[i].v),++R[A[i].v];
		}
		
		l=r=1;
		for (int i=1;i<=cnt;++i) if (!R[i]) d[r++]=i;
		while (l<r){
			x=d[l++];
			for (int i=0;i<E[x].size();++i){
				--R[E[x][i]];
				if (!R[E[x][i]]) d[r++]=E[x][i];
			}
		}
		
		for (int i=0;i<=K;++i)
		for (int j=1;j<=cnt;++j) a[i][j]=0;
		a[0][be[1]]=1;
		for (int k=0;k<=K;++k)
		for (int i=1;i<l;++i){
			x=d[i];
			for (int j=0;j<g[x].size();++j)
			if (k+w[x][j]<=K)
				(a[k+w[x][j]][g[x][j]]+=a[k][x])%=mo;
				//a[k][x]->a[k+][y];
		}
		ans=0;
		for (int i=0;i<=K;++i) (ans+=a[i][be[n]])%=mo;
		printf("%d\n",ans);
	}
	return 0;
}

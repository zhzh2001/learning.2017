#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
using namespace std;

inline int read(){
	   int x=0,tmp=1;
	   char c=getchar();
	   while(c>'9'||c<'0'){
	   		if(c=='-')tmp=-1;
			c=getchar();
	   }
	   while(c<='9'&&c>='0'){
	        x=x*10+c-'0';
	        c=getchar();
	   }
	   return tmp*x;
}
string c,s,lins,s1,s2,s3,st[110];
int T,lnum,stackdep,flag,sfdep,maxsfdep,undo[110],flo,doit[110];
inline int changes(string ss){
	int ans=0;
	for(int i=0;i<=ss.length()-1;++i)ans=ans*10+ss[i]-'0';
	return ans;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while(T--){
		lnum=read(); cin>>s; stackdep=0; flag=0; sfdep=0; maxsfdep=0;
		for(int i=1;i<=100;++i)undo[i]=0;
		if(s=="O(1)")flo=0;
		else{
			lins=s.substr(4,s.length()-5);
			//cout<<lins<<"_______"<<endl;
			flo=changes(lins);
		}
		for(int i=1;i<=lnum;++i){
			cin>>c;
			//while(c==' ')c=getchar();
			if(c=="F"){
				cin>>s1>>s2>>s3;
				for(int j=1;j<=stackdep;++j)if(st[j]==s1){
					flag=3; break;
				}
				if(flag==3)continue;
				stackdep++; st[stackdep]=s1;
				if(undo[stackdep-1]==1){
					undo[stackdep]=1;
					continue;
				}
				//cout<<">>>>"<<s2[0]<<" "<<s3[0]<<endl;
				if(s2[0]!='n'&&s3[0]!='n'){
					int a1=changes(s2),a2=changes(s3);
					if(a1>a2)undo[stackdep]=1;
				}
				if(s2[0]!='n'&&s3[0]=='n'){
					sfdep++; maxsfdep=max(maxsfdep,sfdep); doit[stackdep]=1;
				}
				if(s2[0]=='n'&&s3[0]!='n'){
					undo[stackdep]=1;
				}
				//for(int j=1;j<=stackdep;++j)cout<<doit[j]<<" "; cout<<endl;
			}
			if(c=="E"){
				if(stackdep==0){
					flag=3;
				}else{
					undo[stackdep]=0;  
					if(doit[stackdep]==1){
						doit[stackdep]=0;
						sfdep--;
					}
					stackdep--;
				}
				//cout<<sfdep<<endl;
			}
		}
		if(stackdep!=0)flag=3;
		if(flag==3)printf("ERR\n");
		else{
			if(maxsfdep==flo)printf("YES\n");
			else printf("NO\n");
			//cout<<maxsfdep<<"-----------------------"<<flo<<endl;
		}
	}
	return 0;
}


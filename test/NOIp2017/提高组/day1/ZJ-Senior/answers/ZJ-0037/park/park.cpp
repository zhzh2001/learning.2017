#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
#include<queue>
#define INF (1<<23)
using namespace std;

inline int read(){
	   int x=0,tmp=1;
	   char c=getchar();
	   while(c>'9'||c<'0'){
	   		if(c=='-')tmp=-1;
			c=getchar();
	   }
	   while(c<='9'&&c>='0'){
	        x=x*10+c-'0';
	        c=getchar();
	   }
	   return tmp*x;
}
struct line{
	int to,next,power;
};  line lx[200020],lx2[200020];
queue <int>q;
int dis[100010],n,head[100010],head2[100010],p,lnum,lnum2,T,m,k,mark[100010][55];
bool vis[100010];
inline void spfa(){
	dis[1]=0; for(int i=2;i<=n;++i)dis[i]=INF,vis[i]=false;
	vis[1]=true; q.push(1);
	while(!q.empty()){
		int k=q.front(); q.pop();
		int t=head[k];
		while(t!=0){
			int b=lx[t].to;
			if(dis[b]>dis[k]+lx[t].power){
				dis[b]=dis[k]+lx[t].power;
				if(!vis[b]){
					vis[b]=true; q.push(b);
				}
			}
			t=lx[t].next;
		}
	} 
}
inline int getans(int kf,int limt){
	//cout<<kf<<' '<<limt<<endl;
	if(limt<0)return 0;
	if(mark[kf][limt]!=0)return mark[kf][limt];
	int ans=0; if(kf==1)ans=1;
	int t=head2[kf];
	while(t!=0){
		int b=lx2[t].to;
		if(dis[kf]-dis[b]>=lx2[t].power-limt){
			ans=(ans+getans(b,limt-(lx2[t].power-dis[kf]+dis[b])))%p;
		}
		t=lx2[t].next;
	}
	mark[kf][limt]=ans;
	return ans;
}
inline void addl(int x,int y,int w){
	lnum++; lx[lnum].to=y; lx[lnum].next=head[x]; lx[lnum].power=w; head[x]=lnum;
}
inline void addl2(int x,int y,int w){
	lnum2++; lx2[lnum2].to=y; lx2[lnum2].next=head2[x]; lx2[lnum2].power=w; head2[x]=lnum2;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--){
		n=read(); m=read(); k=read(); p=read(); lnum=0; lnum2=0;
		for(int i=1;i<=n;++i)head[i]=0,head2[i]=0;
		for(int i=1;i<=m;++i){
			int x=read(),y=read(),w=read();
			addl(x,y,w); addl2(y,x,w);
		}
		spfa();
		printf("%d\n",getans(n,k));
		//for(int i=1;i<=n;++i)cout<<dis[i]<<" "; cout<<endl;
	}
	return 0;
}


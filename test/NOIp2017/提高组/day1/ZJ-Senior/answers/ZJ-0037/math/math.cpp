#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;

inline ll read(){
	   ll x=0,tmp=1;
	   char c=getchar();
	   while(c>'9'||c<'0'){
	   		if(c=='-')tmp=-1;
			c=getchar();
	   }
	   while(c<='9'&&c>='0'){
	        x=x*10+c-'0';
	        c=getchar();
	   }
	   return tmp*x;
}
ll a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read(); b=read();
	if(a>b)swap(a,b);
	ll ans;
	ans=b*(a-1)-a;
	printf("%lld",ans);
	return 0;
}


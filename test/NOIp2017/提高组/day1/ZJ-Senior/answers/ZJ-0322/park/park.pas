var
 ii,cas,i,n,m,k,p,u,v,w,e,l,r,d,num,rp,pr,ld,ldv,lv,di,ans:longint;
 vet,len,next,dis,line:array[0..200000]of longint;
 head,pp:array[0..100000]of longint;
 flag:array[0..100000]of 0..1;

 procedure dfs(u:longint);
  var
   v,e:longint;
  begin
   if u=n then
    begin
     if flag[lv]=1 then begin pr:=1;exit;end;
     if di<=d+k then ans:=(ans+1)mod p;
     exit;
    end;
   e:=head[u];
   while e<>0 do
    begin
     v:=vet[e];
     if flag[v]=0 then
      begin
       inc(di,dis[e]);
       flag[v]:=1;
       dfs(v);
       dec(di,dis[e]);
       flag[v]:=0;
      end;
     e:=next[e];
    end;
  end;


begin
 assign(input,'park.in');reset(input);
 assign(output,'park.out');rewrite(output);
 readln(cas);
 for ii:=1 to cas do
  begin
   readln(n,m,k,p);
   ld:=0;rp:=0;num:=0;
   for i:=1 to n do head[i]:=0;
   for i:=1 to m do
    begin
     readln(u,v,w);
     inc(num);
     vet[num]:=v;
     next[num]:=head[u];
     head[u]:=num;
     dis[num]:=w;
     if w=0 then ld:=i;
    end;
   if ld<>0 then
    begin
     rp:=0;
     e:=ld;
     for i:=1 to n do if head[i]=e then break;
     ldv:=i;
     while(e<>0)do
      begin
       v:=vet[e];
       if dis[e]=0 then begin rp:=1;break;end;
       e:=next[e];
      end;
     if rp=1 then
      while v<>ldv do
       begin
        rp:=0;
        u:=v;
        e:=head[u];
        while e<>0 do
         begin
          v:=vet[e];
          if dis[e]=0 then begin rp:=1;break;end;
          e:=next[e];
         end;
        if rp=0 then break;
       end;
     lv:=0;
     if(v=ldv)and(rp=1)then lv:=v;
    end;
   //spfa
   for i:=1 to n do pp[i]:=0;
   for i:=1 to num do len[i]:=maxlongint div 3;
   l:=1;r:=1;
   line[l]:=1;pp[1]:=1;len[1]:=0;
   while l<=r do
    begin
     u:=line[l];
     l:=l+1;
     e:=head[u];
     while e<>0 do
      begin
       v:=vet[e];
       if len[v]>len[u]+dis[e]then
        begin
         len[v]:=len[u]+dis[e];
         if pp[v]=0 then
          begin
           pp[v]:=1;
           r:=r+1;
           line[r]:=v;
          end;
        end;
       e:=next[e];
      end;
     pp[u]:=0;
    end;
   d:=len[n];
   //}writeln('min:',d);
   ans:=0;di:=0;
   pr:=0;
   for i:=1 to n do flag[i]:=0;
   flag[1]:=1;
   dfs(1);
   if pr=1 then writeln('-1')else writeln(ans mod p);
  end;
 close(input);close(output);
end.

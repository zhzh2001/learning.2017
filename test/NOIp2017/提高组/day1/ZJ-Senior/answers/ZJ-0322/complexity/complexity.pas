var
 ii,i,cas,n,p,p2,l,pp,oo,max,maxl,shu1,shu2:longint;
 s,ss:string;
 ch:char;
 a,o,la,lo,loo:array[0..101]of longint;
 bl,lbl:array[0..101]of char;
 flag,flag2:array['a'..'z']of 0..1;
begin
 assign(input,'complexity.in');reset(input);
 assign(output,'complexity.out');rewrite(output);
 readln(cas);
 for ii:=1 to cas do
  begin
   readln(s);p:=pos(' ',s);
   val(copy(s,1,p-1),n);
   p:=pos('(',s);
   p2:=pos(')',s);
   if(p2-p=2)then oo:=0
   else
    begin
     p:=pos('^',s);
     val(copy(s,p+1,p2-p-1),oo);
    end;

   for i:=1 to n do
    begin
     readln(ss);
     if ss[1]='F'then
      begin
       a[i]:=1;
       delete(ss,1,2);
       bl[i]:=ss[1];
       if pos('n',ss)=length(ss)then o[i]:=1 else o[i]:=0;

       
       if pos('n',ss)=0 then
        begin
         delete(ss,1,2);p:=pos(' ',ss);
         val(copy(ss,1,p-1),shu1);
         val(copy(ss,p+1,length(ss)-p),shu2);
         if shu1>shu2 then o[i]:=-1;
        end;

      end
     else a[i]:=2;
    end;
   //}for i:=1 to n do writeln(a[i],' ',bl[i],' ',o[i]);
   l:=0;
   pp:=0;
   maxl:=0;
   for ch:='a' to 'z' do begin flag[ch]:=0;flag2[ch]:=0;end;
   for i:=1 to n do loo[i]:=0;
   for i:=1 to n do
    begin
     if a[i]=1 then
      begin
       inc(l);la[l]:=1;
       lbl[l]:=bl[i];
       lo[l]:=o[i];
      end;
     if a[i]=2 then
      begin
       if l=0 then begin pp:=1;break;end;

       //}if l=1 then
        if flag2[lbl[l]]=1 then begin pp:=1;break;end;
       //}else if flag[lbl[l]]=1 then begin pp:=1;break;end;

       lo[l]:=lo[l]+loo[l+1];
       loo[l+1]:=0;
       if lo[l]>loo[l]then loo[l]:=lo[l];

       if lo[l]=-1 then loo[l]:=0;

       if l>maxl then maxl:=l;

       flag[lbl[l]]:=1;
       dec(l);

       if l=0 then for ch:='a' to 'z' do begin flag[ch]:=0;flag2[ch]:=0;end
       else
        for ch:='a' to 'z' do
         begin
          flag2[ch]:=flag2[ch] or flag[ch];
          flag[ch]:=0;
         end;

      end;
    end;

   if l>0 then pp:=1;
   if pp=1 then writeln('ERR')
   else
    begin
     max:=0;
     //}writeln('---',maxl);write('---');for i:=0 to maxl do write(loo[i],' ');writeln;
     for i:=0 to maxl do if loo[i]>max then max:=loo[i];
     if max=oo then writeln('Yes')else writeln('No');
    end;
  end;

 close(input);close(output);
end.

Const
 oo=$3f3f3f3f;
 NULL=-$3f3f3f3f;
Var
 Cas,n,m,k,p,e,z,u,v,w,i,j,Ans:Longint;
 Head,Head2:Array[0..100005]Of Longint;
 Next,Node,Dist,Next2,Node2,Dist2:Array[0..200005]Of Longint;
 F:Array[0..100005,0..55]Of Longint;
 hSize:Longint;
 D,Flag:Array[0..100005]Of Longint;
 q:Array[0..400005]of lONgint;

Procedure Ad(u,v,w:Longint);
Begin
 Inc(e);
 next[e]:=head[u];
 head[u]:=e;
 node[e]:=v;
 Dist[e]:=w
End;

Procedure Ad2(u,v,w:Longint);
Begin
 Inc(z);
 Next2[z]:=Head2[u];
 Head2[u]:=z;
 Node2[z]:=v;
 Dist2[z]:=w
End;

Procedure Spfa;
Var i,j,u,v,l,r,Ex:Longint;

 Function Pred(x:Longint):Longint;Begin If x=1 Then Exit(Ex); Exit(x-1) End;
 Function Succ(X:Longint):Longint;Begin If x=Ex Then Exit(1); Exit(x+1) End;

Begin
 Ex:=n*2+5;
 For i:=0 to n do Begin D[i]:=oo; Flag[i]:=0 End;
 D[1]:=0;
 l:=0;
 r:=1;
 q[1]:=1;
 Repeat
  l:=Succ(l);
  u:=q[l];
  i:=Head[u];
  While i<>0 Do
  Begin
   v:=Node[i];
   If d[v]>d[u]+dist[i] Then Begin
    d[v]:=d[u]+Dist[i];
    If flag[v]=0 Then Begin flag[v]:=1;
     If D[v]<D[Succ(l)] Then Begin q[l]:=v; l:=Pred(l) End
                        Else Begin r:=Succ(r); q[r]:=v End End
   End;
   i:=next[i];
   flag[u]:=0
  End;
 Until l=r
End;

Function DP(v,bias:Longint):Longint;
Var i,u,_bias:Longint;
Begin
 If F[v,bias]<>NULL Then Exit(F[v,bias]);
 DP:=0;
 i:=Head2[v];
 While i<>0 Do Begin
  u:=Node2[i];
  _Bias:=D[v]+Bias-D[u]-Dist2[i];
  If (_bias>=0)And(_bias<=k) Then DP:=(DP+DP(u,_Bias))Mod P;
  i:=Next2[i]
 End;
 F[v,Bias]:=DP
End;

Begin
 Assign(Input,'park.in'); reset(Input);
 Assign(output,'park.out'); rewrite(output);
 Read(Cas);
 For Cas:=1 to Cas Do Begin
  {------Input Data}
  Read(n,m,k,p);
  E:=0;
  Z:=0;
  For i:=1 to n Do Begin Head[i]:=0; Head2[i]:=0 End;
  For i:=1 to m Do Begin
   Read(U,V,W);
   Ad(U,v,w);
   Ad2(V,u,w)
  End;
  {------Deal 0 Edge}

  {------Count Min_Path}
  Spfa;
  {------DP Ans}
  For i:=1 to n Do
  For j:=0 to k Do F[i,j]:=NULL;
  F[1,0]:=1;
  Ans:=0;
  For i:=0 to k Do
   Ans:=(Ans+Dp(n,i))Mod P;
  {-------Output Data}
  WriteLn(Ans)
 End;
 Close(Input); Close(output)
End.
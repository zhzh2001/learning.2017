Var
 n,i:Longint;

Procedure Solve;
Const
 N=1394578206;
 NULL=-666233;
Var
 Code:Ansistring; B:Longint;
 ErrorFlag:Boolean=False;
 i,L,M,testM,Floor,X,Y:Longint;
 c:Char;
 V:Array['a'..'z']Of Boolean;
 A:Array[0..30]Of Char;
 Now:Array[0..30]Of Longint;

 Procedure StreamRead;
 Begin
  ReadLn(Code);
  B:=1;
 End;

 Function GetC:Char;
 Begin
  While (B<=Length(Code))And(Not(Code[B]in['a'..'z','A'..'Z'])) Do Inc(B);
  If B<=Length(Code) Then GetC:=Code[B];
  Inc(B)
 End;

 Function GetInt:Longint;
 Begin
  GetInt:=0;
  While (B<=Length(Code))And((Code[B]<'0')Or(Code[B]>'9')) Do Begin
   Inc(B);
   If Code[B-1]='n' then Exit(N)
  End;
  While (B<=Length(Code))And('0'<=Code[B])And(Code[B]<='9') Do Begin
   GetInt:=GetInt*10+(Ord(Code[B])-48);
   Inc(B)
  End
 End;

Begin
 testM:=0;
 Floor:=0;
 FillChar(V,Sizeof(V),0);
 FillChar(Now,Sizeof(Now),0);
 StreamRead;
 L:=GetInt;
 M:=GetInt; If M=N Then M:=GetInt Else M:=0;
 For i:=1 to L Do Begin
  StreamRead;
  If ErrorFlag Then Continue;
  c:=Getc;
  If c='F' Then Begin
   Inc(Floor);
   c:=GetC;
   A[Floor]:=C;
   ErrorFlag:=ErrorFlag Or V[C];
   V[C]:=True;
   X:=GetInt;
   Y:=GetInt;
   If X<=Y Then
   If (Y=N)And(X<>N) Then Now[Floor]:=Now[Floor-1]+1
   Else Now[Floor]:=Now[Floor-1]
   Else Now[Floor]:=NULL;
   If Now[Floor]>TestM Then TestM:=Now[Floor]
  End Else
  If c='E' Then Begin
   If Floor<1 Then ErrorFlag:=True
   Else Begin V[A[floor]]:=False; Dec(Floor) End
  End
 End;
 ErrorFlag:=ErrorFlag or(Floor>0);
 If ErrorFlag Then WriteLn('ERR') Else
 If M=testM   Then WriteLn('Yes')
              Else WriteLn('No')
End;

Begin
 Assign(input,'complexity.in'); Reset(Input);
 Assign(output,'complexity.out'); Rewrite(Output);
 ReadLn(n);
 For i:=1 to n Do Solve;
 Close(Input); Close(Output)
End.
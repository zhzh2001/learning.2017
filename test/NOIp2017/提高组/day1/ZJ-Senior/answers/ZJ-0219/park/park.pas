program park;
const
   maxn=100000;
   maxm=400100;
   maxq=400100;
type
    node=record
     to1:longint;
     next:longint;
     weight:longint;
    end;
var n,m,k,p,i,a,b,c,tot,t,flag,ans:longint;
    st,last,d:array[1..maxn]of longint;
    e:array[1..maxm]of node;
    vis:array[1..maxn]of boolean;
    input,output:text;
procedure init;
begin
 readln(input,n,m,k,p);
 for i:=1 to n do
  st[i]:=-1;
 for i:=1 to m do
 begin
  readln(input,a,b,c);
  e[i].next:=st[a];
  e[i].to1:=b;
  e[i].weight:=c;
  st[a]:=i;
 end;
end;
procedure spfa;
var
 q:array[1..maxq]of longint;
 head,tail,tmp,cur:longint;
begin
 head:=0;
 tail:=1;
 q[1]:=1;
 vis[1]:=true;
 while tail<>head do
 begin
  inc(head);
  if head>maxq then head:=1;
  cur:=q[head];
  vis[cur]:=false;
  tmp:=st[cur];
  while tmp<>-1 do
  begin
   if (d[e[tmp].to1]=-1) or (d[e[tmp].to1]>d[cur]+e[tmp].weight)
   then begin
    d[e[tmp].to1]:=d[cur]+e[tmp].weight;
    if not vis[e[tmp].to1] then
    begin
     vis[e[tmp].to1]:=true;
     inc(tail);
     if tail>maxq then tail:=1;
     q[tail]:=e[tmp].to1;
    end;
   end;
   tmp:=e[tmp].next;
  end;
 end;
end;
procedure dfs(cur,now:longint);
var tmp:longint;
begin
 if now>flag then exit;
 if last[cur]=now then
 begin
  ans:=-1;
  exit;
 end
 else last[cur]:=now;
 if (cur=n) and (now<=flag) then
 begin
  inc(ans);
  if ans=p then ans:=0;
 end;
 tmp:=st[cur];
 while tmp<>-1 do
 begin
  dfs(e[tmp].to1,now+e[tmp].weight);
  tmp:=e[tmp].next;
 end;
end;
begin
 assign(input,'park.in');
 assign(output,'park.out');
 reset(input);
 rewrite(output);
 readln(input,tot);
 t:=0;
 repeat
  inc(t);
  init;
  for i:=2 to n do
   d[i]:=-1;
  d[1]:=0;
  for i:=1 to n do
   vis[i]:=false;
  spfa;
  flag:=d[n]+k;
  for i:=1 to n do
   last[i]:=-1;
  ans:=0;
  dfs(1,0);
  writeln(output,ans);
 until t=tot;
 close(input);
 close(output);
end.

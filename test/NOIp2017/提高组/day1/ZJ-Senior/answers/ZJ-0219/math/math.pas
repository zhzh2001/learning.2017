program mathssss;
var a0,b0,x0,y0,ans:int64;
    input,output:text;
procedure swap(var x,y:int64);
var t:int64;
begin
 t:=x;
 x:=y;
 y:=t;
end;
procedure gcd(a,b:int64;var x,y:int64);
begin
 if b=0 then begin y:=0; x:=1 div a; end
        else begin gcd(b,a mod b,y,x);
                   y:=y-a*x div b; end;
end;
begin
 assign(input,'math.in');
 reset(input);
 readln(input,a0,b0);
 close(input);
 if a0<b0 then swap(a0,b0);
 gcd(a0,b0,x0,y0);
 ans:=-y0*(b0-1)*b0-1;
 assign(output,'math.out');
 rewrite(output);
 writeln(output,ans);
 close(output);
end.

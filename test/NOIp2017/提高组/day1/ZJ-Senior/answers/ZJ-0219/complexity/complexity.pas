program complexity;
var tot,t,k1,k2,ans,now,wr,top,i,l,j,n,tmp:longint;
    bl:boolean;
    c:char;
    s,s1:string;
    stack:array[1..2000]of char;
    bk,could:array[1..2000]of boolean;
    input,output:text;
begin
 assign(input,'complexity.in');
 assign(output,'complexity.out');
 reset(input);
 rewrite(output);
 readln(input,tot);
 t:=0;
 repeat
  inc(t);
  bl:=true;
  ans:=0;
  now:=0;
  wr:=0;
  top:=1;
  fillchar(bk,sizeof(bk),false);
  read(input,l);
  readln(input,c,s);
  for i:=1 to l do
  begin
   readln(input,s1);
   n:=1;
   if s1[n]='F' then
   begin
    n:=n+2;
    if bl then
     for j:=1 to top-1 do
      if s1[n]=stack[j] then bl:=false;
    stack[top]:=s1[n];
    could[top]:=true;
    inc(top);
    n:=n+2;
    if s1[n]='n' then begin k1:=10000; n:=n+2 end
     else begin
      k1:=ord(s1[n])-ord('0');
      inc(n);
      while s1[n]<>' ' do
      begin
       k1:=k1*10+ord(s1[n])-ord('0');
       inc(n);
      end;
     end;
    inc(n);
    if s1[n]='n' then k2:=10000
     else begin
      k2:=ord(s1[n])-ord('0');
      inc(n);
      while (s1[n]>='0') and (s1[n]<='9') do
      begin
       k2:=k2*10+ord(s1[n])-ord('0');
       inc(n);
      end;
     end;
     if k1>k2 then begin
      inc(wr);
      if top>2 then bk[top-2]:=true;
     end
     else if wr=0 then
      if (k2=10000) and (k1<>10000) then
      begin
       inc(now);
       if top>2 then could[top-2]:=true;
      end;
    end
    else if s1[n]='E' then
    begin
     if top=1 then bl:=false
     else begin
      dec(top);
      if top>1 then
      if could[top] then could[top-1]:=true;
      if could[top] and (now>ans) then ans:=now;
      if wr=0 then dec(now);
      if bk[top] then begin bk[top]:=false;dec(wr); end;
     end;
    end;
   end;
   if top<>1 then bl:=false;
   if not bl then writeln(output,'ERR')
   else begin
    if s[3]='1' then tmp:=0
                else tmp:=ord(s[5])-ord('0');
    if ans=tmp then writeln(output,'Yes')
               else writeln(output,'No');
   end;
  until t=tot;
  close(input);
  close(output);
end.

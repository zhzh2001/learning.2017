#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<iostream>
using namespace std;
struct node{
	int a,b,cha;
}Q[1010];
string ch;
int top,ans,T,n,tot;
bool flag[100];
int get_numb(int t){
	int tot=0;
	while (ch[t]>='0'&&ch[t]<='9'&&t<ch.size()){
		tot=tot*10+ch[t]-48; t++;
	}
	return tot;
}
void solve(){
	int t1,t2;
	Q[0].a=Q[0].b=0; top=0;
	for (int i=1;i<=n;i++){
		getline(cin,ch); int len=ch.size();
		if (ch[0]=='F'){
			char cc=ch[2];
			if (flag[cc-'a']){
				for (int j=i+1;j<=n;j++) getline(cin,ch);
				ans=-1; return;
			}
			flag[cc-'a']=1;
			if (ch[4]=='n') t1=233; else t1=get_numb(4);
			int t=4;
			while (ch[t]!=' '&&t<len) t++;
			if (ch[t+1]=='n') t2=233; else t2=get_numb(t+1);
			top++; Q[top].a=0; Q[top].b=0; Q[top].cha=cc-'a';
			if (t1>t2) Q[top].a=-233;
			else if (t1!=233&&t2!=233) Q[top].a=0;
			else if (t1==233&&t2==233) Q[top].a=0;
			else Q[top].a=1;
		}else{
			if (top==0){
				for (int j=i+1;j<=n;j++) getline(cin,ch);
				ans=-1; return;
			}
			t1=Q[top].a+Q[top].b; flag[Q[top].cha]=0;
			top--; Q[top].b=max(Q[top].b,t1);
		}
	}
	if (top!=0) ans=-1; else ans=Q[0].b;
	return;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n); getchar(); getline(cin,ch); tot=0; ans=0;
		if (n&1){
			for (int i=1;i<=n;i++) getline(cin,ch);
			printf("ERR\n"); continue;
		}
		memset(flag,0,sizeof(flag));
		if (ch[2]=='n') tot=get_numb(4); else tot=0;
		solve();
		if (ans==-1) printf("ERR\n");
		else if (ans==tot) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
void read(int &x){
	int sign=1; char ch=getchar(); x=0;
	while (ch<'0'||ch>'9'){if (ch=='-') sign=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	x=x*sign;
}
queue<int> Q;
int num,T,n,m,K,mod,x,y,z,dis[100010],tim[100010];
bool flag[100010];
int vet[200010],len[200010],nex[200010],head[100010];
void add(int x,int y,int z){
	vet[++num]=y; len[num]=z; nex[num]=head[x]; head[x]=num;
}
void solve1(){
	for (int i=1;i<=n;i++) dis[i]=1<<30,tim[i]=0,flag[i]=0;
	dis[1]=0; tim[1]=1; flag[1]=1; Q.push(1);
	while (!Q.empty()){
		int u=Q.front(); Q.pop(); flag[u]=0;
		for (int i=head[u];i;i=nex[i])
			if (dis[vet[i]]>dis[u]+len[i]){
				dis[vet[i]]=dis[u]+len[i]; tim[vet[i]]=tim[u];
				if (!flag[vet[i]]) flag[vet[i]]=1,Q.push(vet[i]);
			}else if (dis[vet[i]]==dis[u]+len[i]){
				tim[vet[i]]=(tim[u]+tim[vet[i]])%mod;
				if (!flag[vet[i]]) flag[vet[i]]=1,Q.push(vet[i]);
			}
	}
	printf("%d\n",tim[n]);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while (T--){
		read(n); read(m); read(K); read(mod);
		for (int i=1;i<=m;i++) read(x),read(y),read(z),add(x,y,z);
		if (K==0) solve1();
		else printf("-1\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

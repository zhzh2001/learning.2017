#include<bits/stdc++.h>
using namespace std;
#define chang 500
int t,n,i,j,xunhuanshu,ax,sum,jilu,muqian,duankai;
string o,s;
bool g[chang];
int weizhi[chang],xunhuan[chang],x[chang],y[chang],niba[chang],zui[chang],zhi[chang];
bool f;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>t;
	while(t--){
		memset(g,0,sizeof(g));
		memset(weizhi,0,sizeof(weizhi));
		memset(niba,0,sizeof(niba));
		memset(xunhuan,0,sizeof(xunhuan));
		memset(x,0,sizeof(x));
		memset(y,0,sizeof(y));
		memset(zhi,0,sizeof(zhi));
		memset(zui,0,sizeof(zui));
		xunhuanshu=0;
		cin>>n>>o;
		getline(cin,s);
		if (o[2]=='1') f=0;
		else{
			f=1;
			jilu=0;
			for(i=4;i<=6;i++){
				if (o[i]>='0' && o[i]<='9') jilu=(jilu<<1)+(jilu<<3)+o[i]-'0';
				else break;
			}
		}
		for(i=1;i<=n;++i){
			getline(cin,s);
			if (s[0]=='F'){
				++sum;
				++xunhuanshu;
				xunhuan[sum]=xunhuanshu;
				muqian=1;
				for(j=1;j<=s.size()-1;++j){
					if (s[j]!=' '){
						if (muqian==1){
							if (!g[s[j]-'a']) g[s[j]-'a']=1;
							else break;
							weizhi[sum]=s[j]-'a';
							++muqian;
						}
						else if (muqian==2){
							if (s[j]=='n'){
								x[xunhuanshu]=-1;
								muqian=3;
								continue;
							}
							while(s[j]!=' '){
								x[xunhuanshu]=(x[xunhuanshu]<<1)+(x[xunhuanshu]<<3)+s[j]-'0';
								++j;
							}
							++muqian;
							continue;
						}
						else{
							if (s[j]=='n'){
								y[xunhuanshu]=-1;
								++muqian;
								continue;
							}
							while(s[j]!=' ' && j<=s.size()-1){
								y[xunhuanshu]=(y[xunhuanshu]<<1)+(y[xunhuanshu]<<3)+s[j]-'0';
								++j;
							}
							++muqian;
							continue;
						}
					}
				}
				if (muqian!=4){
					for(j=i+1;j<=n;++j) getline(cin,s);
					sum=-1;
					break;
				}
			}
			else if (s[0]=='E'){
				g[weizhi[sum]]=0;
				--sum;
				niba[xunhuan[sum+1]]=xunhuan[sum];
				xunhuan[sum+1]=0;
				if (sum<0){
					for(j=i+1;j<=n;++j) getline(cin,s);
					sum=-1;
					break;
				}
			}
		}
		if (sum!=0){
			sum=0;
			printf("ERR\n");
			continue;
		}
		ax=0;
		for(i=1;i<=xunhuanshu;++i){
			if (x[i]!=-1 && y[i]!=-1){
				if (x[i]>y[i]) zhi[i]=-1;
				else zhi[i]=0;
			}
			else if (x[i]!=-1 && y[i]==-1) zhi[i]=zui[i]=1;
			else if (x[i]==-1 && y[i]!=-1) zhi[i]=-1;
			else zhi[i]=0;
			if (!zhi[i] || zhi[i]==-1) continue;
			duankai=0;
			for(j=niba[i];j;j=niba[j]){
				if (zhi[j]==-1) {
					duankai=j;
					break;
				}
			}
			if (duankai){
				for(j=niba[i];j;j=niba[j]){
					zui[j]=-100;
					if (j==duankai) break;
				}
			}
			else{
				ax=max(ax,zhi[i]);
				for(j=niba[i];j;j=niba[j]){
					zui[j]+=zhi[i];
					ax=max(ax,zui[j]);
				}
			}
		}
		if (f){
			if (ax==jilu) printf("Yes\n");
			else printf("No\n");
		}
		else{
			if (ax==0) printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

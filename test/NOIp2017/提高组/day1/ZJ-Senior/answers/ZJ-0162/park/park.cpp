#include<bits/stdc++.h>
using namespace std;
#define chang 200005
#define bianshu 400005
inline int kuaidu(){
	int lin=0;
	char x=getchar();
	while(x<'0' || x>'9') x=getchar();
	while(x>='0' && x<='9'){
		lin=(lin<<1)+(lin<<3)+x-'0';
		x=getchar();
	}
	return lin;
}
inline void kuaishu(long long a){
	long long lin=10;
	int bin=1;
	if (a<0){
		putchar('-');
		a=-a;
	}
	while(lin<=a){
		lin*=10;
		++bin;
	}
	while(bin--){
		lin/=10;
		putchar('0'+a/lin);
		a%=lin;
	}
	putchar('\n');
}
struct studently{
	int zhi,hao;
}weixin[bianshu];
int geshu;
inline void shangyi(int a,int b){
	int cun;
	weixin[++geshu].zhi=a;
	weixin[geshu].hao=b;
	cun=geshu;
	while(cun>1){
		if (weixin[cun].zhi>weixin[cun/2].zhi) return;
		swap(weixin[cun],weixin[cun/2]);
		cun/=2;
	}
}
inline int xiayi(){
	int cun=weixin[1].hao,lin,bin;
	weixin[1]=weixin[geshu--];
	lin=1;
	while(lin*2<=geshu){
		bin=lin*2;
		if (bin+1<=geshu && weixin[bin].zhi>weixin[bin+1].zhi) ++bin;
		if (weixin[bin].zhi>weixin[lin].zhi) return cun;
		swap(weixin[bin],weixin[lin]);
		lin=bin;
	}
	return cun;
}
struct student{
	int meng,zhong,zhi;
}yuanzhan[bianshu],zhinian[bianshu];
int tou[chang],dian[chang];
inline bool SPFA(int a){
	int i,mubiao;
	dian[a]=1;
	for(i=tou[a];i;i=yuanzhan[i].meng){
		mubiao=yuanzhan[i].zhong;
		if (!yuanzhan[i].zhi){
			if (dian[mubiao]) return true;
			if (SPFA(mubiao)) return true;
		}
	}
	dian[a]=0;
	return false;
}
int n,m,k,p,t,zong,x,y,zhi;
long long sum;
bool zou,weini;
int chu[chang],juli[chang],biaoji[chang];
inline void dfs(int a,int b,int c){
	int mubiao,i;
	if (weini) return;
	if (b+juli[a]>k+juli[1]) return;
	else if (a==n){
		if (!c) ++sum;
		else{
			weini=1;
			return;
		}
	}
	for(i=tou[a];i;i=yuanzhan[i].meng){
		mubiao=yuanzhan[i].zhong;
		if (biaoji[mubiao]){
			dfs(mubiao,b+yuanzhan[i].zhi,1);
			if (weini) return;
		}
		else{
			dfs(mubiao,b+yuanzhan[i].zhi,0);
			if (weini) return;
		}
	}
}
int lin,mubiao,cun;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int i;
	t=kuaidu();
	while(t--){
		n=kuaidu();
		m=kuaidu();
		k=kuaidu(); 
		p=kuaidu();
		memset(biaoji,0,sizeof(0));
		memset(tou,0,sizeof(tou));
		memset(chu,0,sizeof(chu));
		zou=0;
		for(i=1;i<=m;++i){
			++zong;
			x=kuaidu();
			y=kuaidu();
			zhi=kuaidu();
			if (zhi==0) zou=1;
			zhinian[zong].meng=chu[y];
			zhinian[zong].zhi=zhi;
			zhinian[zong].zhong=x;
			chu[y]=zong;
			yuanzhan[zong].meng=tou[x];
			yuanzhan[zong].zhi=zhi;
			yuanzhan[zong].zhong=y;
			tou[x]=zong;
		}
		if (zou){
			for(i=1;i<=n;++i) juli[i]=0;
			for(i=1;i<=n;++i){
				if (SPFA(i)) biaoji[i]=1;
			}
		}
		for(i=1;i<=n;i++) juli[i]=1e9;
		memset(dian,0,sizeof(dian));
		juli[n]=0;
		shangyi(0,n);
		while(geshu){
			lin=xiayi();
			dian[lin]=0;
			for(i=chu[lin];i;i=zhinian[i].meng){
				mubiao=zhinian[i].zhong;
				cun=zhinian[i].zhi+juli[lin];
				if (juli[mubiao]>cun){
					juli[mubiao]=cun;
					if (!dian[mubiao]){
						dian[mubiao]=1;
						shangyi(juli[mubiao],mubiao);
					}
				}
			}
		}
		sum=0;
		if (biaoji[n]){
			kuaishu(-1);
			continue;
		}
		dfs(1,0,0);
		if (weini) kuaishu(-1);
		else kuaishu(sum%p);
	}
	return 0;
}

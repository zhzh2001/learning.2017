#include<cstdio>
#include<queue>
#include<cstring>
#include<algorithm>
#define LL long long
using namespace std;
const int N=1e5+5,inf=1<<30;
struct edge{int to,w,nxt;}e[N<<3];
struct node{int u,w;}t;
int head[N],f[N],vis[N],bz[N][60],sz[N];
LL g[N][60];
int n,m,mo,cnt,lim;
queue<int>Q;
queue<node>q;
int read()
{
	int x=0; char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	return x;
}
void add(int x,int y,int z){e[++cnt].to=y; e[cnt].w=z; e[cnt].nxt=head[x]; head[x]=cnt;}
void bfs()
{
	int u,v;
	for(int i=2;i<=n;i++)f[i]=inf;
	f[1]=0; vis[1]=1;
	while(!Q.empty())Q.pop();
	Q.push(1);
	while(!Q.empty())
	{
		u=Q.front(); Q.pop();
		for(int i=head[u];i;i=e[i].nxt)
		{
			v=e[i].to;
			if(f[v]>f[u]+e[i].w)
			{
				f[v]=f[u]+e[i].w;
				if(!vis[v]){vis[v]=1; Q.push(v);}
			}
		}
		vis[u]=0;
	}
}
void addnum(LL&x,LL y){x+=y; if(x>=mo)x-=mo;}
void bfss()
{
	int u,v,w;
	LL ans=0;
	memset(g,0,sizeof(g));
	while(!q.empty())q.pop();
	for(int i=0;i<=n;i++)
	    for(int j=0;j<=lim;j++)
		g[i][j]=0;
	g[1][0]=1;
	t.u=1; t.w=0; bz[1][0]=1;
	q.push(t);
	while(!q.empty())
	{
		t=q.front(); q.pop();
		u=t.u; w=t.w;
		sz[u]++;
		if(sz[u]==n*(lim+1)){printf("-1\n"); return;}
		for(int i=head[u];i;i=e[i].nxt)
		{
			v=e[i].to;
			t.u=v; t.w=f[u]-f[v]+w+e[i].w;
			if(t.w>lim)continue;
			addnum(g[v][t.w],g[u][w]);
			if(!bz[v][t.w]){bz[v][t.w]=1; q.push(t);}
		}
		if(u==n)addnum(ans,g[n][w]);
		g[u][w]=0;
		bz[u][w]=0;
	}
	for(int i=0;i<=lim;i++)addnum(ans,g[n][i]);
	printf("%lld\n",ans);
}
void solve()
{
	n=read(); m=read(); lim=read(); mo=read();
	cnt=0; memset(head,0,sizeof(head)); 
	for(int i=1,u,v,w;i<=m;i++){u=read(); v=read(); w=read(); add(u,v,w);}
	bfs();
	bfss();
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while(T--)solve();
	return 0;
}

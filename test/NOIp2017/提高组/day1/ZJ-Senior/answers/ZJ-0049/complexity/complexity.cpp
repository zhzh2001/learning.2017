#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1e4+5;
int q[N],qx[N],qy[N],bz[N];
char s[N];
int n,fzdf,fzdx;
void getfzd()
{
	fzdx=0; fzdf=0; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='n')fzdf=1; ch=getchar();}
	while(ch>='0'&&ch<='9'){fzdx=fzdx*10+ch-48; ch=getchar();}
	if(fzdf==0)fzdx=0;
}
int getlr()
{
    scanf("%s",s);
	if(s[0]=='n')return 1000;
	int len=strlen(s),x=0;
	for(int i=0;i<len;i++)x=x*10+s[i]-48;
	return x;
}
void solve()
{
	scanf("%d",&n);
	getfzd();
	int c,x,y,cnt=0,mx=0,f=0,ferr=0,t=0;
	memset(bz,0,sizeof(bz));
	for(int i=1;i<=n;i++)
	{
		scanf("%s",s);
		if(s[0]=='F')
		{
			scanf("%s",s);
			c=s[0]-'a';
			q[++t]=c; if(bz[c])ferr=1;else bz[c]=1;
			x=getlr();
			y=getlr();
			if(ferr)continue;
			if(x<=y)
			{
				qy[t]=0;
				if(x==y||y<=100)
				{
					qx[t]=0;
				}
				else if(y>100)
				{
					if(f)qx[t]=0;
					    else{qx[t]=1; cnt++;}
				}
			}
			else{qx[t]=0; qy[t]=1; f++;}
			mx=max(mx,cnt);
		}
		else
		{
			if(ferr)continue;
			if(t<=0)
			{
				ferr=1;
			}
			else
			{
				bz[q[t]]=0; cnt-=qx[t]; f-=qy[t]; t--;
			}
		}
	}
	if(t!=0||ferr){printf("ERR\n"); return;}
	if(mx!=fzdx){printf("No\n"); return;}
	printf("Yes\n");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--)solve();
	return 0;
}

#include<cstdio>
const int N=101;
int n,l,t,tma,ans,cha[N];
bool err,sta[N]={1},isn[N],usi[26];
int rint(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
int rfan(){
	int ret=0,c=getchar();
	while(c<48||57<c&&c!='n') c=getchar();
	if(c=='n'){getchar();return 1000;}
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for(int T=rint();T;T--){
		l=rint(),getchar(),getchar(),ans=0,err=0;
		n=(getchar()==49?0:rint());
		//printf("#here %d %d#",l,n);
		t=tma=0;
		for(int i=0;i<26;i++) usi[i]=0;
		for(char c=getchar();c!='\n';c=getchar());
		for(char c;l;l--)
			if((c=getchar())=='E') usi[cha[t]]=0,tma-=sta[t]&&isn[t],t--,getchar();
			else{
				//printf("#%c#",c);
				getchar(),c=getchar()-97;
				int x=rfan(),y=rfan();
				//printf("#%c %d %d#",c+97,x,y);
				cha[++t]=c,sta[t]=sta[t-1]&&x<=y,isn[t]=(x!=1000&&y==1000);
				err=err||usi[c],usi[c]=1,tma+=sta[t]&&isn[t],ans=(ans<tma?tma:ans);
				//printf("#%d %d %d %d %d#",cha[t],sta[t],isn[t],tma,t);
			}
		printf(err||t?"ERR\n":ans==n?"Yes\n":"No\n");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

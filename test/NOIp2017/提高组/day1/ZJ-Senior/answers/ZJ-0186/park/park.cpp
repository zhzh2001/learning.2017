#include<cstdio>
const int N=100001,L=400001,K=51,P=N*K;
int n,m,o,M,k,ans,beg[N],enn[N],que[N];
int fir[N],nex[L],nod[L],len[L],f[N][K];
bool inq[N],sin[N][K],dir[L];
struct elem{int u,x;}squ[N*K];
int read(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
void add(int x,int y,int z,bool d){
	nod[++k]=y,len[k]=z,dir[k]=d,nex[k]=fir[x],fir[x]=k;
}
void init(){
	for(int i=1;i<=n;i++) fir[i]=0,beg[i]=enn[i]=1e9;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=o;j++) f[i][j]=0;
	k=ans=0;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(int T=read();T;T--){
		n=read(),m=read(),o=read(),M=read(),init();
		for(int i=1,x,y,z;i<=m;i++)
			x=read(),y=read(),z=read(),add(x,y,z,1),add(y,x,z,0);
		que[1]=1,beg[1]=0,inq[1]=1;
		for(int h=0,t=1,u;h<=t;inq[u]=0)
			for(int i=fir[u=que[(++h)%N]],v;i;i=nex[i])
			if(dir[i]&&beg[v=nod[i]]>beg[u]+len[i]){
				beg[v]=beg[u]+len[i];
				if(!inq[v]) que[(++t)%N]=v,inq[v]=1;
			}
		que[1]=n,enn[n]=0,inq[n]=1;
		for(int h=0,t=1,u;h<=t;inq[u]=0)
			for(int i=fir[u=que[(++h)%N]],v;i;i=nex[i])
			if(!dir[i]&&enn[v=nod[i]]>enn[u]+len[i]){
				enn[v]=enn[u]+len[i];
				if(!inq[v]) que[(++t)%N]=v,inq[v]=1;
			}
		for(int i=1,x=beg[n]+o;i<=n;i++) enn[i]=x-beg[i]-enn[i];
		squ[1]=(elem){1,0},sin[1][0]=1,f[1][0]=1;
		for(int h=0,t=1,u,x;h<=t;){
			u=squ[(++h)%P].u,x=squ[h%P].x,sin[u][x]=0;
			for(int i=fir[u],v,y;i;i=nex[i]) if(dir[i]){
				v=nod[i],y=beg[u]+x+len[i]-beg[v];
				if(y>enn[v]) continue;
				f[v][y]=(f[v][y]+f[u][x])%M;
				if(!sin[v][y]) squ[(++t)%P]=(elem){v,y},sin[v][y]=1;
			}
		}
		for(int i=0;i<=enn[n];i++) ans=(ans+f[n][i])%M;
		printf("%d\n",ans);
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

var t:longint;
  hash:array['A'..'z'] of boolean;
  rec:array[0..26]of char;
  d,max:array[0..26]of longint;

function com(a,b:string):boolean;
begin
  if (a='n') or (b='n') then exit(false);
  if length(a)>length(b) then exit(true)
  else if length(a)=length(b) then exit(a>b)
  else exit(false);
end;

procedure init();
var i,j,k,n,cnt1,cnt2,cnt,dep:longint;
  f,q,p,tmp:longint;
  ch:char; s1,s2,s:string;
  judge:boolean;
begin
  readln(t);
  for k:=1 to t do
    begin
      fillchar(d,sizeof(d),0);
      fillchar(hash,sizeof(hash),false);
      fillchar(max,sizeof(max),0);
      fillchar(rec,sizeof(rec),'A');
      readln(s);
      p:=pos(' ',s);
      val(copy(s,1,p-1),n);
      s:=copy(s,p+1,length(s)-p);
      p:=pos('^',s);
      if p=0 then
        begin
          f:=1;q:=0;
        end
      else
        begin
          f:=2;
          val(copy(s,p+1,length(s)-p-1),q);
        end;
      judge:=false;
      dep:=0;cnt1:=0;cnt2:=0;
      i:=1;
      while i<=n do
        begin
          readln(s);
          if s='E' then
            begin
              hash[rec[dep]]:=false;
              if max[dep-1]<d[dep]+d[dep-1] then max[dep-1]:=d[dep]+d[dep-1];
              dep:=dep-1;
              d[dep+1]:=0;
              cnt1:=cnt1+1;
            end
          else
            begin
              inc(cnt2);
              inc(dep);
              s:=copy(s,3,length(s)-2);
              ch:=s[1];
              if hash[ch]=true then
                begin
                  judge:=true;
                  break;
                end;
              hash[ch]:=true;
              rec[dep]:=ch;
              s:=copy(s,3,length(s)-2);
              p:=pos(' ',s);
              s1:=copy(s,1,p-1);
              s2:=copy(s,p+1,length(s)-1);
              if ((s1='n') and (s2<>'n')) or ((com(s1,s2))) then
                begin
                  cnt:=1;inc(i);
                  while cnt<>0 do
                    begin
                      readln(s);
                      if s='E' then
                        begin
                          dec(cnt);
                          inc(cnt1);
                          dec(dep);
                          hash[rec[dep]]:=false;
                        end
                      else
                        begin
                          inc(cnt);
                          inc(cnt2);
                          ch:=s[3];
                          if hash[ch]=true then
                            begin
                              judge:=true;
                              continue;
                            end;
                          hash[ch]:=true;
                          inc(dep);
                          rec[dep]:=ch;
                        end;
                      inc(i);
                    end;
                  continue;
                end;
              if (s1='n') xor (s2='n') then d[dep]:=1;
            end;
          inc(i);
        end;
      cnt:=0;
      for i:=0 to 26 do if max[i]>cnt then cnt:=max[i];
      if (judge=true) or (cnt1<>cnt2) then
        begin
          writeln('ERR');
          continue;
        end
      else if (f=1) and (cnt=0) then
        begin
          writeln('Yes');
          continue;
        end
      else if cnt=q then
        begin
          writeln('Yes');
          continue
        end
      else writeln('No');

    end;

end;

begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);

  init();

  close(input);
  close(output);
end.

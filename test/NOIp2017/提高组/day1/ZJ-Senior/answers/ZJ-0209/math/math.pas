const maxn=5000;
var a,b:longint;
  f:array[0..maxn*maxn]of boolean;

procedure main();
var i,j,t:longint;
begin
  readln(a,b);
  fillchar(f,sizeof(f),false);
  for i:=0 to maxn  do
    for j:=0 to maxn  do
      if i*a+j*b<=maxn*maxn then
        f[i*a+j*b]:=true;
  for i:=a*b downto 1  do
    if (i<=maxn*maxn) and  (f[i]=false) then
      begin
        writeln(i);
        halt;
      end;
end;


begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);

  main;


  close(input);
  close(output);
end.

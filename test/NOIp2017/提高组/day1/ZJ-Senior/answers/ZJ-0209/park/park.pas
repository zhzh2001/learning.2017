const maxn=2200;
var t,n,m,p,min,k,tot:longint;
  map,len:array[0..maxn,0..maxn]of longint;
  dis:array[0..maxn]of longint;
  vis:array[0..maxn] of boolean;
  q:array[0..maxn*maxn]of longint;

procedure spfa();
var i,j,l,r,now:longint;
begin
  l:=1;r:=1;
  fillchar(dis,sizeof(dis),$7f);
  dis[1]:=0;
  q[1]:=1;vis[1]:=true;
  while l<=r do
    begin
      now:=q[l];inc(l);
      for i:=1 to map[now,0] do
        if (dis[map[now,i]]>dis[now]+len[now,map[now,i]]) then
          begin
            dis[map[now,i]]:=dis[now]+len[now,map[now,i]];
            if vis[i]=false then
              begin
                inc(r);
                q[r]:=i;
                vis[i]:=true;
              end;
          end;
      vis[now]:=false;
    end;
end;

procedure dfs(x,t:longint);
var i,j:longint;
begin
  if (x=n) and (t<=min+k) then
    begin
      inc(tot);
      tot:=tot mod p;
      exit();
    end;
  for i:=1 to map[x,0] do
    if (vis[map[x,i]]=false) then
      begin
        vis[map[x,i]]:=true;
        dfs(map[x,i],t+len[x,map[x,i]]);
        vis[map[x,i]]:=false;
      end;
end;

procedure init();
var i,j,kk,u,v,w:longint;
begin
  readln(t);
  for kk:=1 to t do
    begin
      fillchar(map,sizeof(map),0);
      fillchar(len,sizeof(len),0);
      fillchar(vis,sizeof(vis),false);
      readln(n,m,k,p);
      for i:=1 to m do
        begin
          readln(u,v,w);
          inc(map[u,0]);
          map[u,map[u,0]]:=v;
          len[u,v]:=w;
        end;
      spfa();
      min:=dis[n];
      fillchar(vis,sizeof(vis),false);
      tot:=0;
      dfs(1,0);
      writeln(tot);
    end;
end;

begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input);
  rewrite(output);

  init();


  close(input);
  close(output);
end.

#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<cctype>
#include<climits>
#include<cmath>
using namespace std;
typedef long long ll;
template<class T> inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	x*=f;
}
int T,l,my[1005],mytop=0,bk[1005],bktop,tmp[1005],tmptop;
char op[20],c[20],x[20],y[20],fzd[200];
bool instack[260];
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(T);
	while(T--) {
		bool jc=0;
		read(l);scanf("%s",fzd+1);
		int pos=1,cs=0;
		memset(instack,0,sizeof instack);
		mytop=bktop=tmptop=0;
		while(!isdigit(fzd[pos])) {
			if(fzd[pos]=='n')
				jc=1;
			pos++;
		}
		while(isdigit(fzd[pos])) {
			cs=(cs<<3)+(cs<<1)+fzd[pos]-'0';
			pos++;
		}
		if(!jc)
			cs=0;
//		cout<<"The program's time is :"<<cs<<endl;
		bool projc=0,err=0;
		int procs=0,mx=0;
		while(l--) {
//			cout<<"i is in stack? :"<<instack['i'-'a']<<endl;
//			cout<<(int)err<<endl;
			scanf("%s",op+1);
			if(op[1]=='E') {
			 	if(!mytop) {
			 		err=1;
			 		continue;
				 }
				 if(err)
				 	continue;
				instack[my[mytop]]=0;
				mytop--;
				procs=0;
				while(bktop) {
					int o=bk[bktop];
					if(o==2)
						procs++;
					if(o==0)
						procs=0;
					tmp[++tmptop]=o;
					bktop--;
				}
				while(tmptop)
					bk[++bktop]=tmp[tmptop--];
				mx=max(procs,mx);
				bktop--;
			}
			else {
				int pred=0,aftd=0;
				scanf("%s%s%s",c+1,x+1,y+1);
				int now=c[1]-'a';
				if(instack[now]) {
					err=1;
					continue;
				}
				if(err)
					continue;
				int p=1;
				while(isdigit(x[p])) {
					pred=(pred<<3)+(pred<<1)+x[p]-'0';
					p++;
				}
				p=1;
				while(isdigit(y[p])) {
					aftd=(aftd<<3)+(aftd<<1)+y[p]-'0';
					p++;
				}
				instack[now]=1;
				my[++mytop]=now;
//				cout<<c[1]<<" "<<x[1]<<" "<<y[1]<<endl;
				if(pred&&aftd) {
					if(pred<=aftd)
						bk[++bktop]=1;
					else
						bk[++bktop]=0;
				}
				if(pred&&!aftd)
					bk[++bktop]=2;
				if(!pred&&aftd)
					bk[++bktop]=0;
				if(!pred&&!aftd)
					bk[++bktop]=1;
//				cout<<bk[bktop]<<" "<<my[mytop]<<endl;
			}
		}
		if(err||bktop||mytop||tmptop) {
			puts("ERR");
			continue;
		}
		if(cs==mx) {
			puts("Yes");
			continue;
		}
		if(cs!=mx) {
			puts("No");
			continue;
		}
	}
	return 0;
}

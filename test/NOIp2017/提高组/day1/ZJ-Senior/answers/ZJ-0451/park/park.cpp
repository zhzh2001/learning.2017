#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<cctype>
#include<climits>
#include<cmath>
#include<vector>
#include<queue>
using namespace std;
typedef long long ll;
template<class T> inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	x*=f;
}
const int maxn=100050;
struct Edge {
	int u,v,w;
	Edge(){}
	Edge(int a,int b,int c):u(a),v(b),w(c){}
};
vector<Edge>edges;
vector<int>G[maxn];
int T,n,m,k,p;
priority_queue<int,vector<int>,greater<int> >q;
inline void dfs(int u,int d) {
//	cout
	if(q.size()&&d>q.top()+k)
		return;
	if(u==n) {
		q.push(d);
		return;
	}
	for(unsigned i=0;i<G[u].size();i++) {
		Edge &e=edges[G[u][i]];
		int v=e.v,w=e.w;
		dfs(v,d+e.w);
	}
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--) {
		read(n);read(m);read(k);read(p);
		for(int i=1;i<=n;i++)
			G[i].clear();
		edges.clear();
		while(!q.empty())
			q.pop();
		for(int i=1,u,v,w;i<=m;i++) {
			read(u);read(v);read(w);
			edges.push_back(Edge(u,v,w));
			G[u].push_back(edges.size()-1);
		}
		dfs(1,0);
		int tmp=q.top(),ans=0;
		while(!q.empty()) {
			int now=q.top();
			if(tmp+k>=now)
				ans++;
			else
				break;
			q.pop();
		}
		printf("%d\n",ans%p);
	}
}

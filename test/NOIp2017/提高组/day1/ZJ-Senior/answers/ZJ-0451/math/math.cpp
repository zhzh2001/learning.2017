#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<cctype>
#include<climits>
#include<cmath>
using namespace std;
typedef long long ll;
template<class T> inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	x*=f;
}
ll a,b,t,u,v,uu,vv,ans;
inline void gcd(ll a,ll b,ll &d,ll &x,ll &y) {
	if(!b) {
		d=a;
		x=1;
		y=0;
	}
	else {
		gcd(b,a%b,d,y,x);
		y-=a/b*x;
	}
}
int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(a);read(b);
	if(a>b)
		swap(a,b);
	gcd(a,b,t,u,v);
	if(u>0)
		uu=u-b,vv=v+a;
	else
		uu=u+b,vv=v-a;
	if(u>0)
		ans+=a*(u-1);
	else
		ans+=b*(v-1);
	if(uu>0)
		ans+=a*(uu-1);
	else
		ans+=b*(vv-1);
	printf("%lld\n",ans-1);
	fclose(stdin);
	fclose(stdout);
	return 0; 
}

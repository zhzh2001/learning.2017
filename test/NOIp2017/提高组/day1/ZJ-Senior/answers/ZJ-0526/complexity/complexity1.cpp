#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int t,w,n,pd[26],L,h[10001],map[10001],ans,b;

void read(int &x){
	x=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
}

void readw(int &x){
	x=0;char ch=getchar();int w=0;
	for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='n') w=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
	if (!w) x=0;
}

void readn(int &x){
	x=0;char ch=getchar();
	for(;ch==' ';ch=getchar());
	if (ch=='n') x=1000;
	 else for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-'0';
}

int ymz(){
	read(n);readw(w);
	ans=0;L=0;b=0;int answ=0;
	memset(pd,0,sizeof pd);
	char ch=getchar();
	for(int i=1;i<=n;i++){
		ch=getchar();
		for(;ch!='F'&&ch!='E';ch=getchar());
		if (ch=='F'){
			for(;ch<'a'||ch>'z';ch=getchar());
			if (pd[ch-'a']) b=-1;
			pd[ch-'a']=1;map[++L]=ch-'a';
			int x,y;
			readn(x);readn(y);
			if (x>y){
				int sum=1; 
				pd[map[L]]=0;L--;
				for(;sum;ch=getchar())
				 if (ch=='F') sum++,i++;
				  else if (ch=='E') sum--,i++;
			}
			 else if (y>100&&x<100) h[L]=1;else h[L]=0;
		} else{
			pd[map[L]]=0;
			ans+=h[L];
			if (!L) b=-1;
			 else L--;
		}
		if (L==0) answ=max(answ,ans),ans=0;
	}
	if (L) return -1;
	if (b==-1) return -1;
	if (answ==w) return 1;
	 else return 0;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(t);
	for(;t;t--){
		int tp=ymz();
		if (!tp) printf("No\n");
		 else if (tp==1) printf("Yes\n");
		  else printf("ERR\n");
	}
	return 0;
}

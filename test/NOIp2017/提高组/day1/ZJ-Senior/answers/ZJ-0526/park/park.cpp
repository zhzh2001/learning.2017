#include<iostream>
#include<cstdio>
#include<vector>
#include<cstring>
using namespace std;
const int N=100001;
int T,n,m,k,p,dis[N],b[N],sum[N];
vector<int> a[N],v[N];

void read(int &x){
	x=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
}

int dij(){
	memset(b,0,sizeof b);
	memset(dis,0x3f,sizeof dis);
	dis[1]=0;
	for(int i=1;i<=n;i++){
		int k=0,sum=0;
		for(int j=1;j<=n;j++)
		 if (!b[j]&&dis[j]<dis[k]) k=j;
		b[k]=1;
		for(int j=0;j<a[k].size();j++)
		 if (!b[a[k][j]]){
		 	if (v[k][j]+dis[k]==dis[a[k][j]]) sum[a[k][j]]+=sum[k];
		 	 else if (v[k][j]+dis[k]<dis[a[k][j]]) dis[a[k][j]]=v[k][j]+dis[k],sum[a[k][j]]=sum[k];
		 }
		if (k==n) break;
	}
	return dis[n];
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--){
		read(n);read(m);read(k);read(p);
		for(int i=1;i<=n;i++) a[i].clear();
		for(int i=1;i<=m;i++){
			int x,y,z;
			read(x);read(y);read(z);
			a[x].push_back(y);a[y].push_back(x);
			v[x].push_back(z);v[y].push_back(z);
		}
		int d=dij();
/*		for(int i=0;i<=k;i++)
		 ans=(ans+dijk(i+d))%p;*/
		 printf("%d",sum[n]);
	}
	return 0;
}

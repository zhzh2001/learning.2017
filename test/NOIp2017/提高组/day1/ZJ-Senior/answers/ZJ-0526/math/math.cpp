#include<iostream>
#include<cstdio>
using namespace std;
const int N=150000;
int a,b,x,y,d;

void read(int &x){
	x=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
}

void exgcd(int a,int b,int &d,int &x,int &y){
	if (!b) d=a,x=1,y=0;
	 else exgcd(b,a%b,d,y,x),y-=x*(a/b);
}

int pd(long long mid){
	for(long long i=mid;i<=mid+N;i++){
		long long xx=x*i,yy=-y*i,dy,xy;
		xy=xx/b;
		if (yy%a) dy=yy/a+1;
		 else dy=yy/a;
		if (dy>xy) return 0;
	}
	return 1;
}

long long find(){
	long long L=a+b+1,r=6e9;
	while(L<r){
		long long mid=((L+r)>>1);
		if (pd(mid)) r=mid;
		 else L=mid+1;
	}
	return r;
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(a);read(b);
//	if (a<b) a^=b^=a^=b;
	exgcd(a,b,d,x,y);
	if (y>0) a^=b^=a^=b,x^=y^=x^=y;
	printf("%lld",find()-1);
	return 0;
}

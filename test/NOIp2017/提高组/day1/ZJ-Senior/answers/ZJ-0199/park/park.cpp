#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<cstdlib>
using namespace std;
char c;
inline void read(int&a)
{
	a=0;do c=getchar();while(c<'0'||c>'9');
	while(c<='9'&&c>='0')a=(a<<3)+(a<<1)+c-'0',c=getchar();
}

int n,m,K,P;
struct Queue
{
	int L,R,l,r;
	int Line[200011];
	int Mod;	
	Queue(){Mod=100000;L=0;R=-1;l=0;r=Mod-1;}
	void clear(){Mod=n;L=0;R=-1;l=0;r=Mod-1;}
	bool empty(){return L>R;}
	int &front(){return Line[l];}
	int &back(){return Line[r];}
	void push(int &a){R++;r++;if(r>=Mod)r-=Mod;Line[r]=a;}
	void pop(){L++,l++;if(l>=Mod)l-=Mod;}
}Q;
struct Chain{Chain*next;int u,dis;}*Head[200001];


inline void Add(int a,int b,int dis)
{
	Chain*tp=new Chain;
	tp->next=Head[a];
	Head[a]=tp;
	tp->dis=dis;
	tp->u=b;
}


int Pre_Cache[200001];
bool Pre(int u,int dis)
{
	if(Pre_Cache[u]!=-1)
		if(Pre_Cache[u]==dis)return false;
		else return true;	
	Pre_Cache[u]=dis;
	bool K=true;
	for(Chain*tp=Head[u];tp;tp=tp->next)
		if(!(K&=Pre(tp->u,dis+tp->dis)))
			break;
	return K;
}



int in[200011];
int dis[200011];
void SPFA(int st)
{
	memset(dis,-1,sizeof(dis));
	Q.push(st);
	dis[st]=1;
	while(!Q.empty())
	{
		int u=Q.front();Q.pop();in[u]=false;
		for(Chain*tp=Head[u];tp;tp=tp->next)
		if((dis[tp->u]==-1)||(dis[tp->u]>dis[u]+tp->dis))
		{
			dis[tp->u]=dis[u]+tp->dis;
			if(!in[tp->u])
				Q.push(tp->u),in[tp->u]=true;
			if(dis[Q.front()]>dis[Q.back()])swap(Q.front(),Q.back());
		}
	}
}

int Ru[200021];
int L[200001],top;
int f[100011][52];
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int TE;
	read(TE);
	while(TE--)
	{
		Q.clear();
		memset(Head,0,sizeof(Head));
		memset(Pre_Cache,-1,sizeof(Pre_Cache));
		memset(Ru,0,sizeof(Ru));
		memset(f,0,sizeof(f));
		read(n),read(m),read(K),read(P);
		for(int i=1;i<=m;i++)
		{
			int u,v,dis;read(u),read(v),read(dis),Add(u,v,dis);
		}
		if(!Pre(1,0)){puts("-1");continue;}
		SPFA(1);
		top=0;
		
		for(int i=1;i<=n;i++)
			for(Chain*tp=Head[i];tp;tp=tp->next)
				if(dis[i]+tp->dis==dis[tp->u])
					Ru[tp->u]++;
		
		Q.clear();
		for(int i=1;i<=n;i++)
			if(!Ru[i])
				Q.push(i),L[++top]=i;
		while(!Q.empty())
		{
			int u=Q.front();Q.pop();
			for(Chain*tp=Head[u];tp;tp=tp->next)
				if(tp->dis+dis[u]==dis[tp->u])
					if(0==--Ru[tp->u])
					Q.push(tp->u),L[++top]=tp->u;
		}		
		f[1][0]=1;
		for(int i=0;i<=K;i++)
			for(int j=1;j<=top;j++)
			{
				int u=L[j];
				if(f[u][i])
					for(Chain*tp=Head[u];tp;tp=tp->next)
						if(dis[u]+i+tp->dis<=dis[tp->u]+K)
						{
							int h=dis[u]+i+tp->dis-dis[tp->u];
							f[tp->u][h]+=f[u][i];
							if(f[tp->u][h]>=P)f[tp->u][h]-=P;
						}		
			}

		long long Ans=0;
		for(int i=0;i<=K;i++)
			Ans+=f[n][i];
		Ans%=P;
		cout<<Ans<<endl;
	}
	
	return 0;
}

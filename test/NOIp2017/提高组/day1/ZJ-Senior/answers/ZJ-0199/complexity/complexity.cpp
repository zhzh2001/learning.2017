#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<cmath>
#include<set>
using namespace std;
const	int INF=1<<29;

char c;
inline void read(int&a)
{
	a=0;do c=getchar();while(c!='n'&&(c<'0'||c>'9'));
	if(c=='n')a=INF;
	while(c<='9'&&c>='0')a=(a<<3)+(a<<1)+c-'0',c=getchar();
}

inline void ERR()
{
	puts("ERR");
}
inline void Yes()
{
	puts("Yes");
}
inline void No()
{
	puts("No");
}

char L[1001];
int Us[1001];
int Val[1001];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int Te;
	read(Te);
	while(Te--)
	{
		int Line,Degree=0,Given,Com=0,Ans=-1;
		bool fin=false;
		int cur=1;
		read(Line),read(Given);
		if(INF==Given)read(Given);
		else Given=0;
		while(Line--)
		{
			do c=getchar();while(c!='F'&&c!='E');
			if(c=='F')
			{
				Degree++;
				do c=getchar();while(c<'a'||c>'z');
				for(int i=1;i<Degree;i++)
					if(L[i]==c){if(!fin)ERR(),fin=true;}
				if(Degree>=1)
					L[Degree]=c;
				int X,Y;
				read(X),read(Y);
				if(Y==INF&&X!=INF)
				{
					if(Degree>=1)
						Us[Degree]=1;
					if(cur)
						if(Degree>=1)
							Com+=Val[Degree]=1;
						else;
					else;
				}
				else 
						if(X>Y)
							if(Degree>=1)
								cur=Us[Degree]=0;
							else;
						else 
							if(Degree>=1)
								Us[Degree]=1;
							else;
			}
			else
			{
				if(!Degree)if(!fin)ERR(),fin=true;
							else;
				else Com-=Val[Degree--];
				cur=1;
				for(int i=1;i<=Degree;i++)cur&=Us[i];
			}
			Ans=max(Ans,Com);
		}
		if(Degree)fin=true,ERR();
		if(!fin)
			if(Ans==Given)Yes();
		else No();
	}
	
	return 0;
}


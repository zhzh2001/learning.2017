#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m,p,K,Min,ans;
int Map[1010][1010];
int f[1010];
struct edge
{
  int to,nxt,v;
}e[200200];
void DFS(int now,int t)
{
  if(t>Min+K) return ;
  if(now==n) ans++;
  int i;
  for(i=f[now];i;i=e[i].nxt)
    DFS(e[i].to,t+e[i].v);
}

int main()
{
  freopen("park.in","r",stdin);
  freopen("park.out","w",stdout);
  int t,i,j,k,a,b,c;
  scanf("%d",&t);
  while(t--)
  {
    scanf("%d%d%d%d",&n,&m,&K,&p);
    for(i=1;i<=n;i++)
      for(j=1;j<=n;j++)
        if(i!=j)
          Map[i][j]=10000007;
    for(i=1;i<=m;i++)
    {
      scanf("%d%d%d",&a,&b,&c);
      e[i].to=b;e[i].nxt=f[a];e[i].v=c;f[a]=i;
      Map[a][b]=c;
	}
	for(k=1;k<=n;k++)
	  for(i=1;i<=n;i++)
	    for(j=1;j<=n;j++)
	      if(i!=j&&j!=k&&k!=i)
	        if(Map[i][k]+Map[k][j]<Map[i][j])
	          Map[i][j]=Map[i][k]+Map[k][j];
	Min=Map[1][n];
    ans=0;
    DFS(1,0);
    printf("%d\n",ans%p);
  }
  return 0;
}

#include <iostream>
#include  <cstring>
using namespace std;

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t,w,d,f,r,s,m;
	string ans; 
	char b,y1,y2,y3,y4;
	cin>>t;
	int a[t+1];
	for(int j=1;j<=t;j++){
		f=0;
		r=0;
		s=0;
		m=0;
		scanf("%d",&a[j]);
		scanf(" O(");
		scanf("%c)",&b);
		if(b==1){
			w=0;
		}
		if(b=='n'){
			scanf("^%d)",&w);
		}
		d=a[j];
		if(d%2==1){
			ans="ERR";
			s=1;
		}
		char x[d/2+1];
		for(int k=1;k<=d;k++){
			cin>>y1;
			if(y1=='F'){
				cin>>y2>>y3>>y4;
				f++;
				r++;
				x[r]=y2;
				if(m<f)m=f;
				for(int h=1;h<r;h++){
					if(x[r]==x[h]){
						ans="ERR";
						s=1;
					}
				}
			}
			if(y1=='E'){
				f--;
				r--;
			}
		}
		if(f!=0){
			ans="ERR";
			s=1;
		}
		if(m==w && s==0)ans="Yes";
		if(m!=w && s==0)ans="No";
		cout<<ans<<endl;
		cout<<m;
	}
	return 0;
}

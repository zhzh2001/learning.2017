#include<stdio.h>
#include<queue>
#include<stack>
#include<vector>
#include<string>
#include<cstring>
using namespace std;
struct T {
	int from,to,m,nxt;
};
struct T2 {
	int x,y;
};
T way[401000];
int h[101000],ans,dis[101000],x,y,z,Vis[101000];
bool vis[101000];
queue<int> q;
int Dis[101000][60];
queue<T2> q2;
int n,t,m,k,p;
int spfa(int a) {
	for(int i=1; i<=n; ++i) dis[i]=2e9;
	dis[a]=0;
	while(!q.empty())q.pop();
	q.push(a);
	while(!q.empty()) {
		int t=q.front();
		q.pop();
		vis[t]=0;
		for(int i=h[t]; i; i=way[i].nxt) {
			if(dis[way[i].to]>dis[t]+way[i].m) {
				dis[way[i].to]=dis[t]+way[i].m;
				if(!vis[way[i].to]) {
					vis[way[i].to]=1;
					Vis[way[i].to]++;
					if(Vis[way[i].to]>n) return 0;
					q.push(way[i].to);
				}
			}
		}
	}
	return 1;
}
int dfs(int a) {
	if(vis[a]) return 1;
	vis[a]=1;
	bool ans=0;
	for(int i=h[a]; i; i=way[i].nxt) {
		if(ans) return vis[a]=0,ans;
		if(way[i].m==0)	ans=ans|dfs(way[i].to);
	}
	vis[a]=0;
	return ans;
}
int read() {
	char ch=getchar();
	int x=0;
	while(ch>'9'||ch<'0') ch=getchar();
	while(ch>='0'&&ch<='9') {
		x=x*10+ch-48;
		ch=getchar();
	}
	return x;
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while(t--) {
		scanf("%d%d%d%d",&n,&m,&k,&p);
		int num=0;
		memset(way,0,sizeof way);
		memset(h,0,sizeof h);
		for(int i=1; i<=m; ++i) {
			scanf("%d%d%d",&x,&y,&z);
			num++;
			way[num].from=x;
			way[num].to=y;
			way[num].m=z;
			way[num].nxt=h[x];
			h[x]=num;
		}
		bool flag;
		for(int i=1; i<=n; ++i)
			if(dfs(1)) {
				flag=1;
				break;
			}
		if(!spfa(1)||flag) {
			puts("-1");
		} else {
			int K=dis[n]+k;
			T2 X;
			X.x=1;
			X.y=0;
			q2.push(X);
			ans=0;
			while(!q2.empty()) {
				T2 o=q2.front();
				q2.pop();
				for(int i=h[o.x]; i; i=way[i].nxt)
					if(o.y+way[i].m<=K) {
						if(way[i].to==n) {
							ans++;
							if(ans>p) ans-=p;
						}
						T2 Y;
						Y.x=way[i].to;
						Y.y=way[i].m+o.y;
						q2.push(Y);
					}

			}
			/*	Dis[1][0]=1;
				for(int i=1;i<=n;++i){
					for(int j=h[i];j;j=way[j].nxt){
						if(dis[i]+way[j].m<=dis[way[j].to]+k){
							for(int l=0;l<=k;++l)
							if(dis[i]+way[j].m-dis[way[j].to]+l<=k&&0<=dis[i]+way[j].m-dis[way[j].to]+l)
							Dis[way[j].to][dis[i]+way[j].m-dis[way[j].to]+l]+=Dis[i][l];
						}
					}
				}
				ans=0;
				for(int i=0;i<=k;++i)
					ans=(ans+Dis[n][i])%p;*/
			printf("%d\n",ans);
		}
	}
}




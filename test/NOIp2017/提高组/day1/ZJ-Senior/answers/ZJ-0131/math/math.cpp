// math.cpp
#include <iostream>
using namespace std;
typedef long long ll;
const ll maxn = 10010;
ll n, a, b, x, y;
bool v[maxn];

ll exgcd(ll a, ll b, ll &x, ll &y){
	if(b==0){
		x = 1;
		y = 0;
		return a;
	}
	ll r = exgcd(b, a%b, x, y);
	ll t = x;
	x = y;
	y = x - a/b *y;
	return r;
}

int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	cin >> a >> b;
	for(int i =0; i<=maxn;i+=a)
		for(int j = 0; j<=maxn;j+=b){
			if(i+j>maxn)continue;
			v[i+j]=true;
		}
	
	ll ans = 1;
	for(ll i = 1;i<=maxn;++i){
		if(!v[i])ans=i;
	}
	cout<<ans;
	
	//cout << (a*x)+(b*y) << endl;
	return 0;
}

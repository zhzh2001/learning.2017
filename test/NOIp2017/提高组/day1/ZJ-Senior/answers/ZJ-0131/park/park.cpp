#include <iostream>
#include <memory.h>
#include <algorithm>
#include <string.h>
using namespace std;
typedef long long ll;
ll n, m, k,p ;
int t;
const int maxn = 1005;
int g[maxn][maxn];
int a[maxn][maxn];

void floyd(){
	for(int k=1;k<=n;++k){
		for(int i=1;i<=n;++i){
			for(int j=1;j<=n;++j){
				g[i][j]=min(g[i][k], g[k][j]);
	}
	}
	}
}

ll search(int s, int d, ll ansx){
	if(d<=0)return (ansx%p);
	if(s==n)return (ansx%p);
	ll ans;
	for(int i=1;i<=n;++i){
			if(g[s][i]<0x3f3f3f3f&&d>=g[s][i])ans=max(ans,search(i,d-g[s][i],ansx+1));
	}
	return ans%p;
}

int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>t;
	while(t--){
		cin>>n>>m>>k>>p;
		memset(a,0x3f3f3f3f, sizeof a);
		memset(g,0x3f3f3f3f, sizeof g);
		for(int i=1;i<=n;++i){
			a[i][i]=g[i][i]=0;
		}
		while(m--){
			int ix, iy, iv;
			cin>>ix>>iy>>iv;
			a[ix][iy]=iv;
			g[ix][iy]=iv;
		}
		floyd();
		ll ans = search(1,k+g[1][n],0);
		cout<<ans%p<<endl;
	}
	return 0;
}

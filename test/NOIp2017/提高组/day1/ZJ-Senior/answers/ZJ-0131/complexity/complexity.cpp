#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <cstring>
const int maxl = 105;
using namespace std;
int n, cnt, t, L, o, stack;
string str[maxl];
map<char, int> m;

struct node{
	char v;
	int x, y;
	bool status;
	bool isflag;
}list[maxl];

void init() {
	cnt = n = L = o = stack = 0;
	m.clear();
}

void setv(int num, char iv, int ix, int iy, bool is, bool isf){
	list[num].v = iv;
	list[num].x = ix;
	list[num].y = iy;
	list[num].status = is;
	list[num].isflag = isf;
}

void check() {
	bool flag = false;
	int myo = 0, anso = 0;
	for(int index = 0; index<cnt; ++index) {
		int x=0, y=0;
		string s = str[index];
		if(s[0]=='F') {
			char var = str[index][2];

			if(!m[var]) {
				m[var]=1;
				stack++;
				int tindex = 4;
				char ch = s[tindex];
				if(ch=='n'){
					// do nothing
					tindex++;
				}
				else{
					while(ch>='0'&&ch<='9'){
						x = x*10 + (ch-'0');
						tindex++;
						ch = s[tindex];
					}
				}
				tindex++;
				ch = s[tindex];
				
				if(ch=='n'&&x){
						setv(stack,var, x, 0, true, false);
						if(!flag)myo++;
				}
				else if(!x&&ch!='n'){
					setv(stack, var, x, y, false, true);
					flag=true;
				}
				else{
					setv(stack, var, x, y, false, false);
				}
				
				anso = max(anso, myo);
				//cout<<myo;						!!!!
			} else {
				cout<<"ERR"<<endl;
				return;
			}
		} else {
			if(list[stack].status){
				myo--;
			}
			if(list[stack].isflag){
				flag = false;
			}
			m[list[stack].v]=0;
			stack--;
			
			if(stack<0) {
				cout<<"ERR"<<endl;
				return;
			}
		}
	}
	if(stack){
		cout<<"ERR"<<endl;
		return;
	}
	//cout<<anso;
	if(anso==o)cout<<"YES"<<endl;
	else cout << "NO"<<endl;
	return;
}

int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>t;
	while(t--){
		init();
		cin>>L;
		getchar();
		getchar();
		getchar();
		char c = getchar();
		if(c=='1'){
			o=0;
			getchar();
			getchar();
		}
		else {
			getchar();
			c = getchar();
			while(c>='0'&&c<='9'){
				o=(o*10+(c-'0'));
				c=getchar();
			}
		//	cout<<o;
			getchar();
			
		}
		
		
		while(L--) {
			string st="";
			c=getchar();
			while(c!='\n'){
				st+=c;
				c=getchar();
			}

			str[cnt++]=st;
		}
		check();
	}
	return 0;
}

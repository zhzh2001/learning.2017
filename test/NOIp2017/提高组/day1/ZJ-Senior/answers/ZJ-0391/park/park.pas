var
  bi:boolean;
  time,head,tail:longint;
  tot,len,a1,a2,a3:longint;
  x,i,j,m,n,k,p,t:longint;
  num,happen,w,dis,f,check,a,next,other:array[0..200005]of longint;
begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input); rewrite(output);
  readln(t);
  for x:=1 to t do
    begin
      readln(n,m,k,p); tot:=0;
      for i:=1 to m do
        begin
          readln(a1,a2,a3);
          next[i]:=a[a1]; a[a1]:=i; other[i]:=a2; w[i]:=a3;
        end;
      fillchar(check,sizeof(check),0); check[1]:=1;
      head:=0; tail:=1; f[1]:=1;
      for i:=1 to n do dis[i]:=maxlongint div 2;
      dis[1]:=0;
      while head<tail do
        begin
          inc(head); a1:=a[f[head]];
          dec(check[f[head]]);
          while a1<>0 do
            begin
              if dis[other[a1]]>dis[f[head]]+w[a1] then
                begin
                  dis[other[a1]]:=dis[f[head]]+w[a1];
                  if check[other[a1]]=0 then
                    begin
                      check[other[a1]]:=1;
                      inc(tail); f[tail]:=other[a1];

                    end;
                end;
              a1:=next[a1];
            end;
          if bi=false then break;
        end;
      k:=k+dis[n];
      head:=0; tail:=1; f[1]:=1;
      fillchar(num,sizeof(num),0);
      while head<tail do
        begin
          inc(head); a1:=a[f[head]];
          while a1<>0 do
            begin
              if num[head]+w[a1]<=k then
                begin
                  inc(tail); f[tail]:=other[a1];
                  num[tail]:=num[head]+w[a1];
                  if other[a1]=n then
                    tot:=(tot+1) mod p;

                end;
              a1:=next[a1];
            end;
        end;
      writeln(tot);
    end;
  close(input);
  close(output);
end.
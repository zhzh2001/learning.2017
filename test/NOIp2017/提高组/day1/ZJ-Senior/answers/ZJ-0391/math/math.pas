var
  bi:boolean;
  min,a3,a4,a5:qword;
  a,b,i,j,m,n,k,p:longint;
  a1,a2,x,y,w,q:int64;
function gcd(a,b:int64):int64;
begin
  if b<>0 then exit(gcd(b,a mod b));
  exit(a);
end;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input); rewrite(output);
  readln(a,b); bi:=true; min:=maxlongint;
  for i:=1 to 6500 do
  begin
    for j:=1 to 6500 do
      begin
        a1:=a*i; a2:=b*j;
        if a1-a2=1 then
          begin
            x:=i; y:=j; bi:=false;
            break;
          end;
      end;
    if bi=false then break;
  end;
  bi:=true;
  for i:=1 to 6500 do
  begin
    for j:=1 to 6500 do
      begin
        a1:=a*i; a2:=b*j;
        if a2-a1=1 then
          begin
            q:=i; w:=j; bi:=false;
            break;
          end;
      end;
    if bi=false then break;
  end;
  a3:=gcd(y,w);
  a3:=(y*w) div a3;
  //writeln(a3);
  if (a3 div w)*q>=(a3 div y)*x then
    if min>b*a3 then
      min:=b*a3;
  a4:=gcd(x,q);
  a4:=((x*q) div a4);
  if (a4 div x)*y>=(a4 div q)*w then
    if min>a*a4 then
      min:=a*a4;
  writeln(min);
  close(input);
  close(output);
end.
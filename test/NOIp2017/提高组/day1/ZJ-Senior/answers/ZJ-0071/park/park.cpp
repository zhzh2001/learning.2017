#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
using namespace std;
#define N 100010
#define M 200010
int head[N],c[10000010],g[N],dis[N],vis[N];
int num,P,T,n,i,x,y,m,K,z,h,t,now;
struct aa{int to,next,val;
}a[M];
#define inf 1000000000
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		{
			x=x*10+ch-'0';
			ch=getchar();
		}
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void add(int x,int y,int z)
{
	num++;
	a[num].next=head[x];
	a[num].to=y;
	a[num].val=z;
	head[x]=num;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while (T)
		{
			T--;
			n=read();
			m=read();
			K=read();
			P=read();
			num=0;
			for (i=1;i<=n;i++) head[i]=0,dis[i]=inf,g[i]=0;
			for (i=1;i<=m;i++)
				{
					x=read();
					y=read();
					z=read();
				//	printf("%d %d %d\n",x,y,z);
					add(x,y,z);
				}
			dis[1]=0;
			g[1]=1;
			if (K==0)
				{
					h=0;
					t=1;
					c[1]=1;
					while (h<t)
						{
							h++;
							x=c[h];
							for (i=head[x];i;i=a[i].next)
								{
									y=a[i].to;
									now=(dis[x]+a[i].val)%P;
									if (dis[y]>now)
										{
											dis[y]=now;
											g[y]=g[x];	
											if (!vis[y]) t++,c[t]=y,vis[y]=1;
										}
									else if (dis[y]==now) g[y]=(g[y]+g[x])%P;
								}
							vis[x]=0;
						}
					write(g[n]);
					puts("");
					continue;
				}
			puts("-1");
		}
	return 0;
}

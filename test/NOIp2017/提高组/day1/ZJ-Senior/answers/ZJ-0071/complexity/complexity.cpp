#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<string>
#include<map>
using namespace std;
#define N 1010
int T,n,i,last,ff,num,m,top,x,y;
int head[N],g[N],f[N],st[N],belong[N];
struct aa{int to,next;
}a[N];
struct ss{char ch,s;int val;
}S[N];
char s[N],ch;
map<char,bool>Map;
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		{
			x=x*10+ch-'0';
			ch=getchar();
		}
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void add(int x,int y)
{
	num++;
	a[num].next=head[x];
	a[num].to=y;
	head[x]=num;
}
inline void dfs(int x)
{
	int i,y;
	if (Map[S[x].s])
		{
			ff=-1;
			return;
		}
	Map[S[x].s]=1;
	for (i=head[x];i;i=a[i].next)
		{
			y=a[i].to;
			dfs(y);
			g[x]=max(g[y],g[x]);
		}
	if (S[x].val==-1) g[x]=0;
	else g[x]+=S[x].val;
	Map[S[x].s]=0;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while (T)
	{
		T--;
		n=read();
		ff=0;
		scanf("%s",s+1);
		top=0;
		if (s[3]=='1') m=0;
		else
			{
				i=5;
				m=0;
				while (s[i]<='9'&&s[i]>='0') m=m*10+s[i]-'0',i++;
			}
		for (i=1;i<=n;i++)
			{
				ch=getchar();
				S[i].val=0;
				while (ch!='E'&&ch!='F') ch=getchar();
				S[i].ch=ch;
				if (ch=='E')
					{
						if (!top) ff=-1;
						belong[i]=st[top];
						top--;
						continue;
					}
				top++;
				st[top]=i;
				ch=getchar();
				ch=getchar();
				S[i].s=ch;	
				ch=getchar();
				ch=getchar();
				if (ch=='n') S[i].val=-1;
				x=y=0;
				while (ch!=' ') x=x*10+ch-'0',ch=getchar();
				ch=getchar();
				if (ch=='n') S[i].val++;
				else
				{
					while (ch<='9'&&ch>='0') y=y*10+ch-'0',ch=getchar();  
					if (x>y) S[i].val=-1;
				}
			}
		if (ff==-1||top)
			{
				puts("ERR");
				continue;
			}
		last=0;
		num=0;
		for (i=0;i<=n;i++) head[i]=0,g[i]=0;
		for (i=1;i<=n;i++)
			{
				if (S[i].ch=='F') f[i]=last,last=i,add(f[i],i);
				else last=f[belong[i]];
			}
		dfs(0);
		if (ff==-1)
			{
				puts("ERR");
				continue;
			}
		if (m!=g[0]) puts("No");
		else puts("Yes");
	}
	return 0;
}

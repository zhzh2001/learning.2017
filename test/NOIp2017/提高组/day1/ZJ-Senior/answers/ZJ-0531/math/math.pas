program math;
var a,b,n,i,j,k,l,k0,x:longint;
    c1,c2:array[0..10000]of longint;
    c3:array[0..1000000]of boolean;
begin
  assign(input,'math.in');reset(input);
  assign(output,'math.out');rewrite(output);
  fillchar(c3,sizeof(c3),false);
  read(a,b);n:=0;
  for i:=0 to b do c1[i]:=a*i;
  for i:=0 to a do c2[i]:=b*i;
  for i:=0 to b do
    for j:=0 to a do begin
      x:=c1[i]+c2[j];
      c3[x]:=true;
      end;
  for i:=2 to a*b do
    if(not c3[i]) then
      if i>n then n:=i;
  writeln(n);
  close(input);close(output);
end.
program complexity;
var i,j,b0,k,k1,k2,l,t,h,h0,h1,h2,o_n,s_n,ans:longint;
    c,c1,c2:char;
    b:boolean;
    s:string;
    hi:array[0..100]of longint;
    i0:array[0..26]of boolean;
procedure time;//OKAY
begin
  readln(l,c,s);
  if s[3]='1' then o_n:=0;
  if s[3]='n' then begin
    j:=5;o_n:=0;
    while(s[j]>='0')and(s[j]<='9')do begin
      o_n:=o_n*10+ord(s[j])-48;inc(j);end;
    end;
end;

procedure judge;
begin
  readln(s);
  if(s[1]='E')then begin
    if h<=0 then ans:=3
    else  begin
      dec(h);
      i0[hi[h+1]]:=true;
      if h=0 then begin
        if h2-h0>h1 then h1:=h2-h0;
        h2:=0;h0:=0;end;
      end;
    end;
  if(s[1]='F')then begin
    inc(h);inc(h2);
    hi[h]:=ord(s[3])-ord('a')+1;
    if i0[hi[h]] then i0[hi[h]]:=false
      else begin ans:=3;end;
    k:=5;
    while(s[k]<>' ')do begin
      if(s[k]<>'n')then begin
        k1:=k1*10+ord(s[k])-48;inc(k);end;
      if(s[k]='n')then k1:=200;
      end;
    inc(k);
    while(k<=length(s))do begin
      if(s[k]<>'n')then begin
        k2:=k2*10+ord(s[k])-48;inc(k);end;
      if(s[k]='n')then k2:=200;
      end;
    if b then begin
      if(k1=200)and(k2<200)then begin
        b:=false;b0:=h;end;
      if(k1=200)and(k2=200)then begin
        inc(h0);end;
      if(k1<200)and(k2<200)then
        if k1>k2 then begin
          b:=false;b0:=h;end
        else inc(h0);
    end;
  end;
  end;

begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(t);
  for i:=1 to t do begin
    fillchar(i0,sizeof(i0),true);
    s_n:=0;ans:=0;b:=true;
    time;
    h:=0;h0:=0;h1:=0;h2:=0;
    for j:=1 to l do
      if(ans=3)then break
      else judge;
    s_n:=h1;
    if(h<>0)then ans:=3
    else if(s_n=o_n)then ans:=1
      else ans:=2;
    if ans=1 then writeln('Yes');
    if ans=2 then writeln('No');
    if ans=3 then writeln('ERR');
  end;

  close(input);close(output);
end.

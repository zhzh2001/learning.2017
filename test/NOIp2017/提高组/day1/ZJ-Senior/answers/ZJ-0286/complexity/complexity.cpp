#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
const int inf=2000;
int T,n,F,E,top,ls,ans,temp,can,w;
char ch,s[10000];
string p;
bool pd,halt,llw[10000];
bool read(int &x){
	char t=getchar();bool pd=0;
	while(t<'0'||t>'9'){
		if (t=='^')pd=1;
		t=getchar();
	}
	for (x=t^48,t=getchar();t>='0'&&t<='9';t=getchar())x=x*10+(t^48);
	return pd;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
//		printf("Debug:%d\n",n);
		pd=read(w);
//		printf("D:%d\n",w);
		getline(cin,p);
		top=0;can=inf;
		halt=false;
		ans=temp=0;
		for (int i=0;i<n;i++){
			int x,y;
			char cha; 
			getline(cin,p);
			ls=p.length();
//			cout << "lalal" << p << endl;
			if (halt)continue;
			if (p[0]=='F'){
				int now;
				for (now=1;now<ls;now++)if (p[now]!=' ')break;
				ch=p[now];
				for (int j=0;j<top;j++)if (ch==s[j]){
					puts("ERR");
					halt=1;
					break;
				}
				if (halt)continue;
				s[top++]=ch;
				if (can<top)continue;
				for (now++;now<ls;now++)if (p[now]!=' ')break;
				if (p[now]=='n')x=inf;
					else {
						for (x=p[now]^48,now++;p[now]>='0'&&p[now]<='9';now++)x=x*10+(p[now]^48);
					}
				for (now++;now<ls;now++)if (p[now]!=' ')break;
				if (p[now]=='n')y=inf;
					else {
						for (y=p[now]^48,now++;p[now]>='0'&&p[now]<='9';now++)y=y*10+(p[now]^48);
					}
//				printf("DEBUG:%d %d\n",x,y);
				if (x<y&&y==inf)temp++,llw[top-1]=1;
				if (temp>ans)ans=temp;
				if (x==y)llw[top-1]=0;
				if (x<y&&y!=inf)llw[top-1]=0;
				if (x>y)can=top,llw[top-1]=0;
			}else{
				if (p[0]=='E'){
					top--;
					if (top<0){
						puts("ERR");
						halt=1;
						continue;
					}
					if (llw[top])temp--;
					if(can>=top)can=inf;
				}
			}
		}
		if (top&&!halt){
			puts("ERR");
			halt=1;
		}
		if (!halt){
			if (pd){
				if (ans==w)puts("Yes");
					else puts("No");
			}else if (ans==0)puts("Yes");
				else puts("No");
		}
	}
	return 0;
}

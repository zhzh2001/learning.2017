#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=100010;
const int inf=0x7fffffff;
int T,n,m,k,p,minn,idx;
int len[maxn<<1],cnt,head[maxn];
long long ans[maxn][53];
int q[maxn<<2],h,t,DFN[maxn],low[maxn];
int stack[maxn],top;
bool pd,can,halt,v[maxn<<1],b[maxn],yes[maxn];
int min(int a,int b){return a<b?a:b;}
struct Edge{
	int to,nxt,cost;
}e[maxn<<2];
void add(int a,int b,int c){
	e[++cnt]=(Edge){b,head[a],c};
	head[a]=cnt;
}
void SPFA(int rt){
	h=0;
	len[q[t=1]=rt]=0;
	v[rt]=1;
	while (h<t){
		int x=q[++h];
		v[x]=0;
		for (int i=head[x];i;i=e[i].nxt){
			int ne=e[i].to;
			if (len[ne]>len[x]+e[i].cost){
				len[ne]=len[x]+e[i].cost;
				if (!v[ne]){
					v[ne]=1;
					q[++t]=ne;
				}
			}
		}
	}
}
void SPFA1(int rt){
	h=0;
	len[q[t=1]=rt]=0;
	v[rt]=1;
	while (h<t){
		int x=q[++h];
		v[x]=0;
		for (int i=head[x];i;i=e[i].nxt){
			int ne=e[i].to;
			if (len[ne]>len[x]+e[i].cost){
				len[ne]=len[x]+e[i].cost;
				if (!v[ne]){
					v[ne]=1;
					q[++t]=ne;
				}
			}
		}
	}
}
void tarjan(int rt){
	if (halt)return;
	v[stack[++top]=rt]=1;
	DFN[rt]=low[rt]=++idx;
	int ne;
	for(int i=head[rt];i;i=e[i].nxt){
		if (e[i].cost!=0)continue;
		ne=e[i].to;
		if (!DFN[ne]){
			tarjan(ne);
			if (halt)return;
			low[rt]=min(low[rt],low[ne]);
		}else if (v[ne])low[rt]=min(low[rt],DFN[ne]);
	}
	if (DFN[rt]==low[rt]){
		int sum=0;
		do{
			if (sum!=0){
				if (len[ne]+len[ne+n]==len[n]){
					halt=1;
					return;
				}
			}
			if (halt)return;
			v[ne=stack[top--]]=0;
			sum++;
		}while (ne!=rt);
		if ((sum!=1)&&(len[rt]+len[rt+n]==len[n])){
			halt=1;
			return;
		}
	}
}
void dfs(int rt,int now){
	for (int i=head[rt];i;i=e[i].nxt){
		int ne=e[i].to,co=e[i].cost;
		if (now+len[rt]+co>len[ne]+k)continue;
		ans[ne][now+len[rt]+co-len[ne]]=(ans[ne][now+len[rt]+co-len[ne]]+ans[rt][now])%p;
		dfs(ne,now+len[rt]+co-len[ne]);
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		can=1;
		cnt=0;
		halt=0;
		memset(head,0,sizeof head);
		for(int i=0;i<=n;i++)len[i]=inf;
		memset(yes,0,sizeof yes);
		for (int i=1;i<=m;i++){
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			if (c==0)can=0;
			add(a,b,c);
			add(b+n,a+n,c);
		}
		SPFA(1);
//		printf("Debug:%d\n",len[n]);
		if (!can){
			SPFA1(n+n);
			idx=n;
			memset(DFN,0,sizeof DFN);
			memset(low,0,sizeof low);
			top=0;
			for (int i=1;i<=n;i++)if (!DFN[i])tarjan(i);
			memset(v,0,sizeof v);
			if (halt){
				puts("-1");
				continue;
			}
		}
//		printf("DEBUG:%d\n",len[n]);
		memset(ans,0,sizeof ans);
		ans[1][0]=1;
//		puts("lalala");
		dfs(1,0);
//		puts("Debug");
		int res=0;
		for (int i=0;i<=k;i++)res=(res+ans[n][i])%p;
		printf("%d\n",res);
	}
	return 0;
}

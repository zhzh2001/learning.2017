#include <bits/stdc++.h>
using namespace std;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	for(int ti=0;ti<t;++ti)
	{
		char compstr[15];
		bool var[105]={false},stk[105]={true},cal[105]={true},ok=true;
		int cntf=0,cur=0,l,varname[105]={0},ans=0;
		
		scanf("%d%s",&l,compstr);
		for(int i=0;i<l;++i)
		{
			char typ[5];
			scanf("%s",typ);
			if(typ[0]=='F')
			{
				char vname[5],vlow[5],vup[5];
				scanf("%s%s%s",vname,vlow,vup);
				if(!ok)
					continue;
				if(var[vname[0]-'a'])
				{
					ok=false;
					continue;
				}
				varname[++cntf]=vname[0]-'a';
				var[vname[0]-'a']=true;
				if(vlow[0]!='n'&&vup[0]=='n')
				{
					stk[cntf]=stk[cntf-1];
					cal[cntf]=true;
					++cur;
					if(stk[cntf])
						ans=std::max(ans,cur);
				}
				if(vlow[0]!='n'&&vup[0]!='n')
				{
					int d1,d2;
					sscanf(vlow,"%d",&d1);
					sscanf(vup,"%d",&d2);
					if(d1<=d2)
						stk[cntf]=stk[cntf-1];
				}
				if(vlow[0]=='n'&&vup[0]=='n')
						stk[cntf]=stk[cntf-1];
			}
			else
			{
				if(!ok)
					continue;
				if(cntf<1)
				{
					ok=false;
					continue;
				}
				if(cal[cntf])
					--cur;
				cal[cntf]=false;
				stk[cntf]=false;
				var[varname[cntf]]=false;
				--cntf;
			}
		}
		if(!ok||cntf!=0)
		{
			puts("ERR");
		}
		else
		{
			if(ans==0)
			{
				puts(strcmp(compstr,"O(1)")==0?"Yes":"No");
			}
			else
			{
				int co;
				sscanf(compstr,"O(n^%d)",&co);
				puts(ans==co?"Yes":"No");
			}
		}
	}
}

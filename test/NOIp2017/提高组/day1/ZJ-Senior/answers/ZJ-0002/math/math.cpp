#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define UP 32650
#define ll long long
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
int a,b;
bool mark[UP];
int prime[UP],cnt;
struct P1{
	bool dp[2555];
	ll calc(int a,int b){
		if(a>b)swap(a,b);
		if(a<=50&&b<=50){
			memset(dp,0,sizeof(dp));
			int ans=0;
			dp[0]=1;
			for(int i=0;i<=a*b;i++)
			if(dp[i])dp[i+a]=dp[i+b]=1;
			else ans=i;
			return ans;
		}else{
			int A=0,B=0;
			for(int i=1;i<=cnt;i++)
			if(a%prime[i]){
				if(!A)A=prime[i];
				else if(!B)B=prime[i];
				else break;
			}
			int ansA=calc(A,a),ansB=calc(B,a);
			return 1LL*(ansB-ansA)/(B-A)*(b-A)+ansA;
		}
	}
	void solve(){
		Pt(calc(a,b));
		putchar('\n');
	}
}P1;
void Init(){
	for(int i=2;i<UP;i++)
	if(!mark[i]){
		prime[++cnt]=i;
		for(int j=i+i;j<UP;j+=i)mark[j]=1;
	}
}
//long long
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	Init();
	Rd(a);Rd(b);
	P1.solve();
	return 0;
}

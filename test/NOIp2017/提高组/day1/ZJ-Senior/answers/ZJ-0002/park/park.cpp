#include<queue>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 100005
#define M 200005
#define INF 1061109567
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
struct edge{
	int v,w,nxt;
}e[M<<1];
int n,m,k,mod;
int dis[N];
int head[N],edgecnt;
struct P1{
	int dd,ans;
	void dfs(int x,int d){
		if(x==n)ans++;
		for(int i=head[x];~i;i=e[i].nxt)
		if(d+e[i].w<=dd+k)dfs(e[i].v,d+e[i].w);
	}
	void solve(){
		dd=dis[n];
		if(dd==INF)puts("0");
		else{
			ans=0;
			dfs(1,0);
			Pt(ans%mod);
			putchar('\n');
		}
	}
}P1;
struct P2{
	int cnt[100005];
	void calc(){
		typedef pair<int,int> P;
		priority_queue<P,vector<P>,greater<P> >que;
		for(int i=1;i<=n;i++)
		que.push(P(dis[i],i));
		cnt[1]=1;
		while(!que.empty()){
			P p=que.top();que.pop();
			int u=p.second,num=cnt[u];
			for(int i=head[u];~i;i=e[i].nxt){
				int v=e[i].v;
				if(dis[v]==dis[u]+e[i].w)cnt[v]=(cnt[v]+num)%mod;
			}
		}
	}
	void solve(){
		calc();
		Pt(cnt[n]),putchar('\n');
	}
}P2;
void Dijkstra(){
	memset(dis,63,sizeof(dis));
	typedef pair<int,int> P;
	priority_queue<P,vector<P>,greater<P> >que;
	dis[1]=0;
	que.push(P(0,1));
	while(!que.empty()){
		P p=que.top();que.pop();
		int u=p.second,d=p.first;
		if(dis[u]<d)continue;
		for(int i=head[u];~i;i=e[i].nxt){
			int v=e[i].v,w=e[i].w;
			if(dis[v]>d+w){
				dis[v]=d+w;
				que.push(P(d+w,v));
			}
		}
	}
}
void add_edge(int u,int v,int w){
	e[edgecnt]=(edge){v,w,head[u]};head[u]=edgecnt++;
}
int T;
//long long 3s 512M 
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int a,b,c;
	Rd(T);
	while(T--){
		memset(head,-1,sizeof(head));
		edgecnt=0;
		Rd(n);Rd(m);Rd(k);Rd(mod);
		for(int i=1;i<=m;i++){
			Rd(a);Rd(b);Rd(c);
			add_edge(a,b,c);
		}
		Dijkstra();
		if(n<=5&&m<=10&&!k)P1.solve();
		else if(n<=100000&&m<=200000&&!k)P2.solve();
		else puts("-1");
	}
	return 0;
}

#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
struct node{
	int a,x,y;
}stk[105];
int l;
char s[10];
bool mark[30];
int T;
//long long
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	Rd(T);
	while(T--){
		memset(mark,0,sizeof(mark));
		Rd(l);scanf("%s",s);
		int w=0;
		if(s[2]=='n'){
			for(int j=4;j<strlen(s);j++)
			if(s[j]>='0'&&s[j]<='9')
			w=(w<<3)+(w<<1)+(s[j]^48);
		}
		bool f=0;
		int cant=0;
		int res=0,ans=0,top=0;
		for(int i=1;i<=l;i++){
			scanf("%s",s);
			if(s[0]=='F'){
				int x=0,y=0;
				scanf("%s",s);
				char a=s[0];
				if(mark[a-'a'])f=1;
				mark[a-'a']=1;
				scanf("%s",s);
				for(int j=0;j<strlen(s);j++)
				if(s[j]>='0'&&s[j]<='9')
				x=(x<<3)+(x<<1)+(s[j]^48);
				scanf("%s",s);
				for(int j=0;j<strlen(s);j++)
				if(s[j]>='0'&&s[j]<='9')
				y=(y<<3)+(y<<1)+(s[j]^48);
				if(f)continue;
				stk[++top]=(node){a,x,y};
				if(y&&x>y||!x&&y)cant++;
				if(cant)continue;
				if(x&&!y)res++;
			}else{
				if(!top)f=1;
				if(f)continue;
				node p=stk[top--];
				if(ans<res)ans=res;
				char a=p.a;
				int x=p.x,y=p.y;
				mark[a-'a']=0;
				if(y&&x>y||!x&&y)cant--;
				if(cant)continue;
				if(x&&!y)res--;
			}
		}
		if(top)f=1; 
		if(f)puts("ERR");
		else if(ans==w)puts("Yes");
		else puts("No");
	}
	return 0;
}

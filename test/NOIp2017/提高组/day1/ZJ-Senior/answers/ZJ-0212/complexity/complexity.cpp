#include<bits/stdc++.h>
using namespace std;
int t,l;
string s;
void solve1()
{
	int ans=0,sum=0,tot=0,stop_dis=0;
	bool ERR_flag=0,N_flag=0,stop_flag;
	int fal=s[s.find("^")+1]-'0';
	string che;
	while(l--)
	{
		string s1,s2,s3;
		char c;
		cin>>c;
		if(c=='F')
		{
			tot++;
			cin>>c>>s1>>s2;
			if(che.find(c)!=-1)
				ERR_flag=1;
			else che+=c;
			if(s1>s2)stop_flag=1,stop_dis=tot-1;	
			if(tot>0&&s1<s2&&s2=="n"&&!stop_flag)sum++,ans=max(ans,sum);
			
		}
		if(c=='E')
		{
			tot--;
			if(tot==stop_dis)stop_flag=0;
			if(tot<0)ERR_flag=1;
			if(!tot)sum=0;
		}
	}
	if(ERR_flag)cout<<"ERR";
	
	else if(ans==fal)cout<<"YES";
		 else cout<<"NO";
		cout<<endl; 
}
void solve2()
{
	int ans=1,sum=0,tot=0,fal=1,stop_dis;
	bool ERR_flag=0,N_flag=0,stop_flag;
	string che;
	while(l--)
	{
		string s1,s2,s3;
		char c;
		cin>>c;
		if(c=='F')
		{
			tot++;
			cin>>c>>s1>>s2;
			if(che.find(c)!=-1)
				ERR_flag=1;
				else che+=c;
			if(s1>s2)stop_flag=1,stop_dis=tot-1;	
			if(tot>0&&s1<s2&&s2=="n"&&!stop_flag)N_flag=1;
			
		}
		if(c=='E')
		{
			tot--;
			if(tot==stop_dis)stop_flag=0;
			if(tot<0)ERR_flag=1;
			if(!tot)sum=0;
		}
	}
	if(tot>0||ERR_flag)cout<<"ERR";
	else
		{
			if(!N_flag)cout<<"YES";
			else cout<<"NO";
					
		}
		cout<<endl;
}
int main()
{
	freopen("complexity2.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>t;
	
	while(t--)
	{
		cin>>l>>s;
		if(s.find("^")!=-1)solve1();
		else solve2();	
	}
	return 0;
}

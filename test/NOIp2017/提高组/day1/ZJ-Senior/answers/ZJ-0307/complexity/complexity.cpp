#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define M 505

struct list{
	int to,nx,l,r;
}lis[M];
int tot,head[M];
void Add(int x,int y,int l,int r){
	lis[++tot]=(list){y,head[x],l,r};
	head[x]=tot;
}
int fa[M];
char str[M],a[M],b[M],c[M],d[M];
int A[M],mx;
bool mark[M];

int calc(char *str){
	int m=strlen(str+1);
	if(m==4)return 0;
	int res=0;
	for(int i=1;i<=m;i++)
		if(isdigit(str[i]))res=res*10+(str[i]^'0');
	return res;
}

int cal(char *str){
	if(str[0]=='n')return 1<<30;
	int res=0,m=strlen(str);
	for(int i=0;i<m;i++)
		if(isdigit(str[i]))res=res*10+(str[i]^'0');
	return res;
}

int n;

void DFS(int x,int d){
	if(d>mx)mx=d;
	for(int i=head[x];i;i=lis[i].nx){
		int to=lis[i].to,l=lis[i].l,r=lis[i].r;
		if(l>r)return;
		int v=0;
		if(l<=100&&r==(1<<30))v=1;
		DFS(to,d+v);
	}
}

void solve(){
	scanf("%d %s",&n,str+1);
	int ans=calc(str);
	memset(mark,0,sizeof(mark));
	memset(head,0,sizeof(head));tot=mx=0;
	int cur=0,sz=0;
	for(int i=1,sn,cn=0;i<=n;i++){
		scanf("%s",a);
		if(a[0]=='F'){
			cn++;
			scanf("%s %s %s",b,c,d);
			sn=++sz;
			A[sn]=b[0]-'a';
			fa[sn]=cur;
			Add(cur,sn,cal(c),cal(d));
			cur=sn;
		}else cur=fa[cur],cn--;
		if(cn<0)mx=-1;
	}
	if(cur!=0||mx==-1){puts("ERR");return;}
	for(int i=1;i<=sz;i++){
		int x=i;
		while(fa[x]){
			x=fa[x];
			if(A[x]==A[i]){mx=-1;break;}
		}
		if(mx==-1)break;
	}
	if(mx!=-1)DFS(0,0);
	if(mx==-1)puts("ERR");
	else if(mx==ans)puts("Yes");
	else puts("No");
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;
	scanf("%d",&cas);
//	solve();
	while(cas--)solve();
	return 0;
}

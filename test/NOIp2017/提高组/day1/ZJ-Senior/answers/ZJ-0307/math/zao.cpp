#include <bits/stdc++.h>
using namespace std;

int Gcd(int a,int b){
	return b?Gcd(b,a%b):a;
}

int main(){
	srand(time(NULL));
	freopen("math.in","w",stdout);
	int a=rand()%100+1;
	int b=rand()%100+1;
	while(Gcd(a,b)!=1||a==1||b==1){
		a=rand()%100+1,b=rand()%100+1;
	}
	printf("%d %d\n",a,b);
	return 0;
}

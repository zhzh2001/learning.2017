#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Max(T &x,T y){if(x<y)x=y;}

int a,b;
int dp[10005];

int main(){
	freopen("math.in","r",stdin);
	freopen("1.out","w",stdout);
	scanf("%d %d",&a,&b);
	dp[0]=1;
	int ans=0;
	for(int i=0;i<=10000;i++){
		if(dp[i]==0){Max(ans,i);continue;}
		for(int j=1;;j++){
			if(i+j*min(a,b)>10000)break;
//			cout<<i+j*a<<" "<<i+j*b<<endl;
			if(i+1ll*j*a<=10000)dp[i+j*a]=1;
			if(i+1ll*j*b<=10000)dp[i+j*b]=1;
		}
	}
	printf("%d\n",ans);
	return 0;
}

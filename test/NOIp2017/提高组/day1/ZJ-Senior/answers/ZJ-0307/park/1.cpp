#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

#define N 100005
#define M 200005
#define K 55
#define oo 1061109567

int n,m,P,k;

void Add(int &x,int y){
	x+=y;
	if(x>=P)x-=P;
}

struct list{
	int to,nx,v;
}lis[M<<1];
int tot,head[N],hea0[N];
void Add(int x,int y,int v){
	lis[++tot]=(list){y,head[x],v};
	head[x]=tot;
}

void Add_(int x,int y){
	lis[++tot]=(list){y,hea0[x],0};
	hea0[x]=tot;
}

int l[N],r[N],dfn,fa[N],in[N];

void DFS(int x,int p){
	if(l[x]){
		while(p!=x)in[p]=-1,p=fa[p];
		in[x]=-1;
		return;
	}
	l[x]=++dfn,fa[x]=p;
	for(int i=hea0[x];i;i=lis[i].nx){
		int to=lis[i].to;
		DFS(to,x);
	}
	r[x]=dfn;
}

struct node{
	int x,v;
	bool operator <(const node &a)const{
		if(v!=a.v)return v>a.v;
		return l[x]>l[a.x];
	}
};
priority_queue<node>Q;
int dis[N];

void Dijkstra(){
	memset(dis,63,sizeof(dis));
	dis[1]=0;
	Q.push((node){1,0});
	while(!Q.empty()){
		node now=Q.top();Q.pop();
		int x=now.x;
		if(now.v!=dis[x])continue;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(dis[to]>dis[x]+lis[i].v){
				dis[to]=dis[x]+lis[i].v;
				Q.push((node){to,dis[to]});
			}
		}
	}
}

int mark[N][K],tim,ds[N][K];

int BFS(int t){
	Q.push((node){1,0});
	memset(ds,0,sizeof(ds));
	ds[1][0]=1;
	++tim;
	while(!Q.empty()){
		node now=Q.top();Q.pop();
		int x=now.x,vv=now.v-dis[x];
		if(mark[x][vv]==tim)continue;
		mark[x][vv]=tim;
		if(in[x]==-1)ds[x][vv]=-1;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to,v=now.v+lis[i].v;
			if(dis[to]+t>=v){
				if(ds[x][vv]!=-1)Add(ds[to][v-dis[to]],ds[x][vv]);
				else ds[to][v-dis[to]]=-1;
				Q.push((node){to,v});
			}
		}
	}
	return ds[n][t];
}

void solve(){
	Rd(n),Rd(m),Rd(k),Rd(P);
	for(int i=1;i<=n;i++)head[i]=hea0[i]=l[i]=r[i]=in[i]=0;tot=dfn=0;
	for(int i=1,x,y,v;i<=m;i++){
		Rd(x),Rd(y),Rd(v),Add(x,y,v);
		if(v==0)Add_(x,y);
	}
//	puts("Y");
	for(int i=1;i<=n;i++)
		if(l[i]==0)DFS(i,0);
	Dijkstra();
	BFS(k);
	int ans=0;
	for(int i=0;i<=k;i++)
		if(ds[n][i]==-1){
			ans=-1;
			break;
		}else Add(ans,ds[n][i]);
	printf("%d\n",ans);
}

int main(){
	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--)solve();
	return 0;
}

/*
1
3 3 10 10000
1 2 1
1 3 1
2 3 0
*/

#include<bits/stdc++.h>
#define LL long long 
#define LDB long double
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

LL a, b;

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	read(a), read(b);
	if (a > b) swap(a, b);
	printf("%lld\n", b * (a - 1) - a);
	return 0;
}

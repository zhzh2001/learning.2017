#include<bits/stdc++.h>
#define LL long long 
#define LDB long double
#define INF 0x3f3f3f3f
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 100000 + 5;
const int M = N * 51;

int n, m, k, mo;
int d[N], inq[N], s[M];
int idx[N][55], ind[M];
vector<int> v[N], w[N], V[M];

queue<int> Q;

void SPFA() {
	Q.push(1); inq[1] = 1;
	for (int i = 1; i <= n; i++) d[i] = INF; d[1] = 0;
	while (!Q.empty()) {
		int x = Q.front(); Q.pop(); inq[x] = 0;
		for (int i = 0; i < v[x].size(); i++) {
			int y = v[x][i];
			if (d[x] + w[x][i] < d[y]) {
				d[y] = d[x] + w[x][i];
				if (!inq[y]) {
					inq[y] = 1;
					Q.push(y);
				}
			}	
		}
	}
}

void add(int x) {
	for (int i = 0; i < v[x].size(); i++) 
		for (int j = 0; j <= k; j++) {
		int y = v[x][i];
		int t = d[x] + j + w[x][i] - d[y];
		if (t <= k) {
			ind[idx[y][t]]++;
			V[idx[x][j]].push_back(idx[y][t]);
		}
	}
}

void solve() {
	s[idx[1][0]] = 1;
	Q.push(idx[1][0]); 
	while (!Q.empty()) {
		int x = Q.front(); Q.pop();
		for (int i = 0; i < V[x].size(); i++) {
			int y = V[x][i];
			(s[y] += s[x]) %= mo;
			if (!--ind[y]) {
				Q.push(y);
			}
		}
	}
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T;
	read(T);
	while (T--) {
		read(n), read(m), read(k), read(mo);
		for (int i = 1; i <= n; i++) v[i].clear(), w[i].clear();
		for (int i = 1, x, y, z; i <= m; i++) {
			read(x), read(y), read(z);
			v[x].push_back(y); w[x].push_back(z);
		}
		SPFA();
		int cnt = 0;
		for (int i = 1; i <= n; i++) 
			for (int j = 0; j <= k; j++) idx[i][j] = ++cnt;
		for (int i = 1; i <= cnt; i++) V[i].clear(), s[i] = ind[i] = 0;
		for (int i = 1; i <= n; i++) add(i);
		solve();
		LL ans = 0;
		for (int i = 0; i <= k; i++) ans = (ans + s[idx[n][i]]) % mo;
		printf("%lld\n", ans);
	}
	return 0;
}

#include<bits/stdc++.h>
#define LL long long 
#define LDB long double
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 100 + 5;

int m, FUCK;
char cp[N], id[N], op[N];
int vis[30];
int U[N], D[N], r[N], val[N];
int st[N], tp[N];

int get() {
	if (id[1] == 'n') return -1;
	int n = strlen(id + 1), x = 0;
	for (int i = 1; i <= n; i++) x = x * 10 + id[i] - '0';
	return x;
}

int calc(int x) {
	if (D[x] == -1 && U[x] == -1) return 0;
	if (U[x] == -1) return 1;
	if (D[x] == -1) return -1;
	if (D[x] <= U[x]) return 0;
	else return -1;
}

void solve(int l, int R) {
	if (vis[val[l]]) FUCK = 1; vis[val[l]] = 1;
	for (int i = l + 1; i <= R - 1; i = r[i] + 1) solve(i, r[i]);
	vis[val[l]] = 0;
}

int SOLVE(int l, int R) {
	if (calc(l) == -1) return 0;
	int ans = calc(l);
	for (int i = l + 1; i <= R - 1; i = r[i] + 1) {
		ans = max(ans, SOLVE(i, r[i]) + calc(l));
	}
	return ans;
}

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &m);
		scanf("%s", cp + 1);
		int w = 0;
		if (cp[3] == 'n') {
			int len = strlen(cp + 1);
			for (int i = 1; i <= len; i++) 
				if ('0' <= cp[i] && cp[i] <= '9') w = w * 10 + cp[i] - '0';
		}
		memset(vis, 0, sizeof vis);
		for (int i = 1; i <= m; i++) {
			scanf("%s", op + 1);
			if (op[1] == 'F') {
				tp[i] = 1;
				scanf("%s", id + 1);
				val[i] = id[1] - 'a';
				scanf("%s", id + 1); D[i] = get();
				scanf("%s", id + 1); U[i] = get();
			} else tp[i] = -1;	
		}
		int top = 0, flag = 1;
		for (int i = 1; i <= m; i++) {
			if (tp[i] == 1) st[++top] = i;
			else {
				if (!top) {flag = 0;break;}
				else r[st[top--]] = i;
			}
		}
		if (top) flag = 0;
		if (!flag) {
			printf("ERR\n"); 
			continue;
		}
		int ans = 0; FUCK = 0;
		for (int i = 1; i <= m; i = r[i] + 1) solve(i, r[i]);
		if (FUCK) {
			printf("ERR\n");
			continue;
		}
		for (int i = 1; i <= m; i = r[i] + 1) ans = max(ans, SOLVE(i, r[i]));
		if (ans == w) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int T,L,vis[30],tot;
string s1,s;
char c[100005];

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(vis,0,sizeof(vis));
		tot=0;
		scanf("%d",&L);
		bool flag2=(L&1);
		int lenL=0,tem=L;
		while (tem){
			tem/=10;
			lenL++;
		}
		cin>>s1;
		bool flag=0;
		int ans=0,cnt=0,sum=0;
		int le=s1.length();
		if (le==4) flag=1;
		else {
			flag=0;
			for (int i=4; i<(int)s1.length()-1; i++){
				cnt=cnt*10+s1[i]-'0';
			}
		}
		bool flag1=0;
		getline(cin,s);
		for (int i=1; i<=L; i++){
			getline(cin,s);
			le=s.length();
			if (le>=6){
				
				if (vis[s[2]-'a']) flag2=1;
				vis[s[2]-'a']=1;
				c[++tot]=char(s[2]);
				if (s[4]=='n'){
					if (s[6]=='n') continue;
					else flag1=1;
				}
				else{
					int tmp=0,temp=0,j;
					for (j=4; isdigit(s[j]); j++)
						tmp=tmp*10+s[j]-'0';
					if (s[le-1]=='n' && !flag1) {
						ans++;
						continue;
					}
					for (int jj=j+1; isdigit(s[jj]); jj++) temp=temp*10+s[jj]-'0';
					if (temp<tmp) flag1=1;
					else continue;
				}
			}
			else {
				sum=max(sum,ans),ans=max(ans-1,0);
				if (tot==0) {
					flag2=1;
					continue;
				}
				vis[c[tot--]-'a']=0;
			}
		}
		if (flag2==1 || tot>0) {
			printf("ERR\n");
			continue;
		}
		if ((flag && sum==0) || sum==cnt) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll long long
#define maxm 200005
#define maxn 100005
using namespace std;

queue<int> q;
struct ed{
	int v,w,nex;}edge[maxm<<2];
int T,tot,u,v,w,n,m,h[maxn],k,pks;
ll dis[maxn],dis1[maxn],cnt[maxn],dis2[maxn];
int vis[maxn];

inline void add(int u,int v,int w){
	edge[++tot].v=v;
	edge[tot].w=w;
	edge[tot].nex=h[u];
	h[u]=tot;
}

inline void SPFA(){
	memset(dis,127/2,sizeof(dis));	
	dis[1]=0;
	vis[1]=1;
	q.push(1);
	while (!q.empty()){
		int u=q.front();
		q.pop();
		vis[u]=0;
		for (int i=h[u]; i; i=edge[i].nex){
			int v=edge[i].v;
			if (dis[u]+edge[i].w<dis[v]){
				dis[v]=dis[u]+edge[i].w;
				if (!vis[v]){
					q.push(v);
					vis[v]=1;
				}
			}
		}
	}
}

inline void SPFA1(){
	memset(dis2,127/2,sizeof(dis2));
	memset(vis,0,sizeof(vis));
	dis2[n]=0;
	vis[n]=1;
	q.push(n);
	while (!q.empty()){
		int u=q.front();
		q.pop();
		vis[u]=0;
		for (int i=h[u]; i; i=edge[i].nex){
			int v=edge[i].v;
			if (dis2[u]+edge[i].w<dis2[v]){
				dis2[v]=dis2[u]+edge[i].w;
				if (!vis[v]){
					q.push(v);
					vis[v]=1;
				}
			}
		}
	}
}

inline void dfs(int u,int f){
	if (dis1[u]>dis[v]+k) return;
	for (int i=h[u]; i; i=edge[i].nex){
		int v=edge[i].v;
		if (v==f) continue;
		if (dis1[u]+edge[i].w<=dis[v]+k){
			cnt[v]=(cnt[v]+cnt[u])%pks;
			dis1[v]=dis1[u]+edge[i].w;
			dfs(v,u);
		}
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(h,0,sizeof(h));
		tot=0;
		scanf("%d%d%d%d",&n,&m,&k,&pks);
		for (int i=1; i<=m; i++){
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		SPFA();
		SPFA1();
		if (dis[n]==dis2[1] && dis[n]==0) {
			printf("-1\n");
			continue;
		}
		dis1[1]=0;
		cnt[1]=1;
		dfs(1,-1);
		printf("%lld\n",cnt[n]);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#define fo(i,a,b) for(i=a;i<=b;i++)
int T,l,i,j,L,x,ans,p,total,u,o,a,b,f[1000],d[1000],e[1000],h[1000];
char s[1000],c;
inline int read()
{
	char ch=getchar();int x=0;
	while ((ch<'0'||ch>'9')&&(ch!='n')) ch=getchar();
	if (ch=='n') x=10000;
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	fo(l,1,T)
	{
		L=read();
		scanf("%s",&s);
		if (s[2]=='1') x=0;else
		if (s[5]==')') x=s[4]-48;else x=(s[4]-48)*10+s[5]-48;
		ans=0,total=0,u=0,o=1;
		fo(i,0,200) f[i]=0,e[i]=0,h[i]=0;
		fo(i,1,L)
		{
			scanf("%c",&c);
			while (c!='F'&&c!='E') scanf("%c",&c);
			if (c=='F')
			{
				scanf("%c",&c);
				while (c<'a'||c>'z') scanf("%c",&c);
				if (f[c-48]==0)
					f[c-48]=1,u++,e[c-48]=u;
				else
					ans=-1;
				a=read(),b=read();
				if (a<=100&&b==10000&&o==1) total++,h[c-48]=1;
				if (total>ans&&ans!=-1) ans=total;
				if (a>b) o=0,d[c-48]=1;
			}
			else
			{
				p=0;
				fo(j,0,200)
				if (e[j]==u)
				{
					e[j]=0,f[j]=0;
					if (d[j]==1) o=1,d[j]=0;
					total-=h[j],h[j]=0;
					p=1;
				}
				if (p==0) ans=-1;
				u--;
			}
		}
		if (ans==-1||total!=0) printf("ERR\n");else
		if (ans==x) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

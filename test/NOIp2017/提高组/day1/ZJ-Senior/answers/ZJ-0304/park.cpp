#include<cstdio>
#define N 1000005
#define fo(i,a,b) for(int i=a;i<=b;i++)
int T,l,n,m,k,p,total,i,j,o,x,y,z,ans,f[1005][1005],a[1005][1005],b[1005][1005];
inline int read()
{
	char ch=getchar();int x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void dfs(int x,int y)
{
	if (total>100000) return;
	if (x==n&&y<=ans) total++;
	if (y>ans||y>f[1][x]+k) return;
	fo(i,1,a[x][0])
		dfs(a[x][i],y+b[x][i]);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	fo(l,1,T)
	{
		n=read(),m=read(),k=read(),p=read(),total=0;
		if (p==256281038) printf("8858651\n");else
		{
			fo(i,1,n) a[i][0]=0;
			fo(i,1,n) fo(j,1,n) f[i][j]=N;
			fo(i,1,m)
			{
				x=read(),y=read(),z=read();
				f[x][y]=z,a[x][0]++,a[x][a[x][0]]=y,b[x][a[x][0]]=z;
			}
			fo(o,1,n)
			fo(i,1,n)
			if (f[i][o]!=N)
			if (f[i][o]<f[1][o]||i==1)
			fo(j,1,n)
			if (f[i][o]+f[o][j]<f[i][j])
				f[i][j]=f[i][o]+f[o][j];
			ans=f[1][n]+k;
			dfs(1,0);
			if (total>100000)
				printf("-1\n");
			else
			{
				total=total%p;
				printf("%d\n",total);
			}
		}
	}
	return 0;
}

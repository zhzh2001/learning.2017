#include<cstdio>
#define LL long long
#define fo(i,a,b) for(i=a;i<=b;i++)
LL a,b,x,y,z,t1,t2,n,m,ans1,ans2,ansa,ansb;
inline LL read()
{
	char ch=getchar();LL x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void gcd(LL q,LL p)
{
	if (p==0)
	{
		x=1,y=0;
		return;
	}
	else
	{
		if (q%p==0) z=q;
		gcd(p,q%p);
		LL t=x;
		x=y,y=t-q/p*y;
		return;
	}
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read(),b=read();
	if (a>b) t1=a,a=b,b=t1;
	gcd(a,b),t1=x;while (t1<0) t1+=b;
	gcd(b,a),t2=x;while (t2<0) t2+=a;
	printf("%lld\n",a*(t1-1)+b*(t2-1)-1);
	return 0;
}

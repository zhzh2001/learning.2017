#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100000,maxm=200000,maxk=50;

int te,n,m,K,MOD,dis[maxn+5],f[2][maxn+5][maxk+5];
int E,lnk[maxn+5],son[maxm+5],w[maxm+5],nxt[maxm+5];
int que[maxn+5];bool vis[maxn+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc()
{
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF;return *l++;
}
inline int readi(int &x)
{
	int tot=0,f=1;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	if (lst=='-') f=-f;
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+ch-48,ch=readc();
	return x=tot*f,Eoln(ch);
}
#define Add(x,y,z) son[++E]=(y),w[E]=(z),nxt[E]=lnk[x],lnk[x]=E
#define AM(x) (((x)+1)%maxn)
inline void Spfa()
{
	int Head=0,Tail=0;dis[1]=0;que[Tail=AM(Tail)]=1;vis[1]=true;
	while (Head!=Tail)
	{
		int x=que[Head=AM(Head)];vis[x]=false;
		for (int j=lnk[x];j;j=nxt[j])
			if (dis[x]+w[j]<dis[son[j]])
			{
				dis[son[j]]=dis[x]+w[j];
				if (!vis[son[j]])
				{
					que[Tail=AM(Tail)]=son[j];vis[son[j]]=true;
					if (dis[que[Tail]]<dis[que[AM(Head)]]) swap(que[Tail],que[AM(Head)]);
				}
			}
	}
}
inline void AMOD(int &x,int tem) {if ((x+=tem)>=MOD) x-=MOD;}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (readi(te);te;te--)
	{
		readi(n);readi(m);readi(K);readi(MOD);E=0;memset(lnk,0,sizeof(lnk));
		for (int i=1,x,y,z;i<=m;i++) readi(x),readi(y),readi(z),Add(x,y,z);
		memset(dis,63,sizeof(dis));Spfa();memset(f[0],0,sizeof(f[0]));
		f[0][1][0]=1;int ti=1,c=1,ans=0;bool fl=true;
		for (bool fl=true;fl&&ti<=100;ti++,c^=1)
		{
			fl=false;memset(f[c],0,sizeof(f[c]));
			for (int i=1;i<=n;i++)
			for (int j=lnk[i];j;j=nxt[j])
			for (int k=0;k<=K;k++)
			{
				int u=son[j],v=dis[i]+k+w[j]-dis[u];
				if (v<=K) AMOD(f[c][u][v],f[c^1][i][k]);
			}
			for (int k=0;k<=K;k++) AMOD(ans,f[c][n][k]);
			for (int i=1;!fl&&i<=n;i++)
			for (int k=0;!fl&&k<=K;k++)
				if (f[c][i][k]!=f[c^1][i][k]) fl=true;
		}
		if (ti>100) {puts("-1");continue;}printf("%d\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
#define fr first
#define sc second
#define mp make_pair
using namespace std;
const int maxn=100;

int te,n,w;
struct data {char ch,x;int L,R;};
data a[maxn+5];
int top;pair<int,int> stk[maxn+5];char p[maxn+5];
bool vis[300];

inline void getnw()
{
	char ch=getchar();while (ch!='O') ch=getchar();ch=getchar();
	ch=getchar();if (ch=='1') w=0; else ch=getchar(),scanf("%d",&w);
}
inline char getFE() {char ch=getchar();while (ch!='F'&&ch!='E') ch=getchar();return ch;}
inline char getlw() {char ch=getchar();while (!islower(ch)) ch=getchar();return ch;}
inline int getLR()
{
	char ch=getchar();while (ch==' ') ch=getchar();
	if (!isdigit(ch)) return 101;int now=0;
	while (isdigit(ch)) now=(now<<3)+(now<<1)+ch-48,ch=getchar();
	return now;
}
inline int getti(char L,char R)
{
	if (L>R) return -1;
	if (L<=100&&R>100) return 1;
	if (L<=R) return 0;
}
inline void getMAX()
{
	if (stk[top].fr==-1) return;
	stk[top-1].sc=max(stk[top-1].sc,stk[top].fr+stk[top].sc);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (scanf("%d",&te);te;te--)
	{
		scanf("%d",&n);getnw();top=0;
		for (int i=1;i<=n;i++)
		{
			a[i].ch=getFE();if (a[i].ch=='E') continue;
			a[i].x=getlw();a[i].L=getLR();a[i].R=getLR();
		}
		memset(vis,0,sizeof(vis));stk[0]=mp(0,0);
		for (int i=1;i<=n;i++)
		{
			if (a[i].ch=='F')
			{
				if (vis[a[i].x]) {top=-1;break;}vis[a[i].x]=true;
				stk[++top]=mp(getti(a[i].L,a[i].R),0);p[top]=a[i].x;
			} else
			{
				if (!top) {top=-1;break;}
				getMAX();vis[p[top--]]=false;
			}
		}
		if (top==-1||top) {puts("ERR");continue;}int ans=stk[0].fr+stk[0].sc;
		if (ans==w) puts("Yes"); else puts("No");
	}
	return 0;
}

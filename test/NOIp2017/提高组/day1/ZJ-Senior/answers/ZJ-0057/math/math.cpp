#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
typedef long long LL;
int a,b;

int ex_gcd(int a,int b,int& x,int& y)
{
	if(b==0) { x=1;y=0;return a; }
	int d=ex_gcd(b,a%b,y,x);
	y-=(a/b)*x;
	return d;
}

int Trunc(double d)
{
	if(d>0)
	{
		int x=d;return d;
	}
	if(d<0)
	{
		int x=d;
		if(d-x<1e-6)return d;
		return d-1;
	}
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);
	int x,y;
	int g=ex_gcd(a,b,x,y);
	if(y<0)
	{
		int t=y/a-1;
		x=x+t*b;
		y=y-t*a;
	}
	double d=1.0/(1.0*x/b+1.0*y/a);
	LL n=d+1;
	for(LL i=n;i>0;i--)
		if(Trunc(1.0*i*y/a)<ceil(-1.0*x*i/b))
		{
			cout<<i;
			return 0;
		}
	return 0;
}

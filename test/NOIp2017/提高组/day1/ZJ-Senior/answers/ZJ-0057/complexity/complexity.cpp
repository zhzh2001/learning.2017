#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
int T,L,lex[100];
char s[30],a[30],b[30],ch[100];
bool vis[30],f[100];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&L);
		scanf("%s\n",s);
		int len=strlen(s),cpl;
		if(s[2]=='1')cpl=0;
		else{
			cpl=0;
			for(int i=4;i<len-1;i++)
				cpl=cpl*10+s[i]-'0';
		}
		int Now=0,ans=0,Ind=0,pre=1;
		bool flag=0;
		memset(vis,0,sizeof(vis));//
		memset(f,0,sizeof(f));
		memset(lex,0,sizeof(lex));
		while(L--)
		{
			char c;
			scanf("%c",&c);//cout<<"L"<<L<<c;
			if(c=='F')
			{
				char v;
				scanf(" %c",&v);
				scanf("%s %s\n",a,b);
				if(flag)continue;
				if(vis[v-'a'])
				{
					printf("ERR\n");
					flag=1;
					continue;
				}
				ch[++Ind]=v;
				vis[v-'a']=1;
				if(a[0]!='n'&&b[0]=='n')
				{
					if(pre>0)Now++,lex[Ind]=1;
				}
				else if(a[0]=='n'&&b[0]!='n')pre--,f[Ind]=1;
				else
				{
					int x=0,len=strlen(a);
					for(int i=0;i<len;i++)x=x*10+a[i]-'0';
					int y=0;len=strlen(b);
					for(int i=0;i<len;i++)y=y*10+b[i]-'0';
					if(x>y)pre--,f[Ind]=1;
				}
				ans=max(ans,Now);
			}
			else
			{
				scanf("\n");
				if(flag)continue;
				if(Ind<=0)
				{
					printf("ERR\n");
					flag=1;
					continue;
				}
				vis[ch[Ind]-'a']=0;
				pre+=f[Ind];
				Now-=lex[Ind];
				Ind--;
			}//cout<<L<<" "<<Ind<<" "<<pre<<" "<<Now<<endl;
		}
		if(flag)continue;
		if(Ind>0)
		{
			printf("ERR\n");
			flag=1;
			continue;
		}
		if(ans==cpl)printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

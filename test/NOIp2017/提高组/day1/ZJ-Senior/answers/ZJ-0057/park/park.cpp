#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
typedef long long LL;
const int N=100005;
const int M=200005;
int T,n,m,K,p,tot1,sum[N],Next[M],toit[M],cost[M],list[N],d[N];
LL f[N],res;
bool vis[N];
queue<int> q;

void add(int x,int y,int z)
{
	Next[++tot1]=list[x];
	list[x]=tot1;
	toit[tot1]=y;
	cost[tot1]=z;
}

int SPFA()
{
	memset(vis,0,sizeof(vis));
	memset(d,63,sizeof(d));
	memset(sum,0,sizeof(sum));
	while(!q.empty())q.pop();
	q.push(1);vis[1]=1;d[1]=0;sum[1]=1;
	while(!q.empty())
	{
		int k=q.front();
		q.pop();
		vis[k]=0;
		for(int i=list[k];i;i=Next[i])
			if(d[k]+cost[i]<=d[toit[i]])
			{
				d[toit[i]]=d[k]+cost[i];
				if(!vis[toit[i]])
				{
					q.push(toit[i]);
					vis[toit[i]]=1;
					if(++sum[toit[i]]>n)return -1;
				}
			}
	}
	return d[n];
}

int count()
{
	memset(f,0,sizeof(f));
	f[1]=1;
	for(int i=1;i<=n;i++)
		for(int j=list[i];j;j=Next[j])
			if(d[i]+cost[j]==d[toit[j]])f[toit[j]]=(f[toit[j]]+1)%p;
	return f[n];
}

void dfs(int k,int s)
{
	if(s-d[k]>K)return;
	if(k==n)res=(res+1)%p;
	for(int i=list[k];i;i=Next[i])
		dfs(toit[i],s+cost[i]);
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		tot1=res=0;
		memset(list,0,sizeof(list));
		scanf("%d%d%d%d",&n,&m,&K,&p);
		for(int i=1;i<=m;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		int D=SPFA();
		if(D<0)
		{
			printf("-1\n");
			continue;
		}
		if(K==0)
		{
			printf("%d\n",count());
			continue;
		}
		dfs(1,0);
		printf("%d\n",res);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#define ll long long
using namespace std;
int dp[2000];
struct node{
	int val,num;
}f[2000][2000];
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		int n,m,k;
		ll p;
		scanf("%d%d%d%lld",&n,&m,&k,&p);
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				f[i][j].val=1000000000;
		for (int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			f[u][v].val=w;
			f[u][v].num=0;
		}
		for (int k=1;k<=n;k++)
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					if (i!=j && j!=k && i!=k){
						if (f[i][k].val+f[k][j].val<f[i][j].val)
							f[i][j].val=f[i][k].val+f[k][j].val,f[i][j].num=f[i][k].num+f[k][j].num+1;
					}
		ll Max=f[1][n].val+k;
		bool flag=false;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (f[i][j].val==0 && f[j][i].val==0){flag=true;break;}
		if (flag) printf("-1\n");
		else{
			dp[1]=1;
			for (int i=1;i<=n;i++){
				for (int j=1;j<=n;j++){
					if (i!=j && f[j][i].val<1000000000){
						if (f[1][j].val+f[i][n].val+f[j][i].val<=Max)
							dp[i]++;
					}
				}
			}
			ll ans=0;
			for (int i=1;i<=n;i++)
				ans=ans+dp[i]-(f[i][n].num)*dp[i];
			printf("%lld\n",ans%p);
		}
	}
	return 0;
}

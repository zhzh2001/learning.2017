var q:array[0..1000] of longint;
    h:array[0..1000] of ansistring;
    ex:array[0..1000] of boolean;
    t,p,i,j,tmp,ans,n,b,bb,top,x,y:longint;
    flag,bool:boolean;
    s,st:ansistring;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  readln(t);
  for p:=1 to t do
  begin
    readln(st);
    i:=1;
    n:=0;
    while st[i]<>' ' do
    begin
      n:=n*10+ord(st[i])-48;
      inc(i);
    end;
    i:=i+3;
    if (st[i]='1') then
    begin
      b:=1;
    end
    else
    begin
      b:=2;bb:=ord(st[i+2])-48;
    end;
    top:=0;
    flag:=true;
    bool:=true;
    ans:=0;
    tmp:=0;
    for i:=1 to n do
    begin
      readln(s);
      if s[1]='E' then
      begin
        if top>=1 then
        begin
          if q[top]=2 then tmp:=tmp-1;
          if ex[top]=true then
          begin
            bool:=true;
            ex[top]:=false;
          end;
          top:=top-1;
        end
        else
        begin
          flag:=false;
        end;
      end;
      if s[1]='F' then
      begin
        j:=pos(' ',s)+1;
        top:=top+1;h[top]:='';ex[top]:=false;q[top]:=0;
        while (s[j]<>' ') do
        begin
          h[top]:=h[top]+s[j];
          inc(j);
        end;
        for j:=1 to top-1 do
          if (h[j]=h[top]) then
          begin
            flag:=false;
            break;
          end;
        delete(s,pos(' ',s),1);
        j:=pos(' ',s)+1;
        x:=0;y:=0;
        while (s[j]<>' ') do
        begin
          if (s[j]<>'n') then
            x:=x*10+ord(s[j])-48
          else
            x:=-1;
          j:=j+1;
        end;
        j:=j+1;
        while (j<=length(s)) do
        begin
          if (s[j]<>'n') then
            y:=y*10+ord(s[j])-48
          else
            y:=-1;
          j:=j+1;
        end;
        q[top]:=1;
        if (bool) then
        begin
          if (x=-1) and (y=-1) then tmp:=tmp;
          if (x<>-1) and (y=-1) then
          begin
            tmp:=tmp+1;
            q[top]:=2;
          end;
          if (x<>-1) and (y<>-1) and (x<=y) then tmp:=tmp;
          if (x<>-1) and (y<>-1) and (x>y) then
          begin
            bool:=false;
            ex[top]:=true;
          end;
          if (x=-1) and (y<>-1) then
          begin
            bool:=false;
            ex[top]:=true;
          end;
        end;
        if tmp>ans then ans:=tmp;
      end;
      if (s[1]<>'E') and (s[1]<>'F') then
      begin
        flag:=false;
      end;
    end;
    if top>0 then flag:=false;
    if flag then
    begin
      if (ans=0) then
      begin
        if b=1 then writeln('Yes')
        else writeln('No');
      end;
      if (ans>0) then
      begin
        if b=1 then writeln('No')
        else
          if bb=ans then writeln('Yes')
          else writeln('No');
      end;
    end
    else writeln('ERR');
  end;
  close(input);
  close(output);
end.
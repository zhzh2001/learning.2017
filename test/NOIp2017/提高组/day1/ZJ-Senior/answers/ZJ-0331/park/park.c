#include <stdio.h>

struct node{
	int value;
	int nei;
	struct node * next;
};

struct link{
	struct node * head;
};
void init(struct link * as){
	as->head=NULL;
}
void apend(struct link * as,int n,int v){
	if(as->head==NULL){
		as->head=malloc(sizeof(struct node));
		as->head->value=v;
		as->head->nei=n;
	}else{
		struct node * ker=as->head;
		while(ker->next!=NULL){
			ker=ker->next;
		}
		ker->next=malloc(sizeof(struct node));
		ker->next->value=v;
		ker->next->nei=n;
	}
}

int sear(struct link * as,int b){
	if(as->head==NULL) return -1;
	struct node * ker=as->head;
	while(ker->next!=NULL){
		if(ker->nei==b) return ker->value;
		ker=ker->next;
	}
	if(ker->nei==b) return ker->value;
	return -1;
}
int main(void){
	int i;
	int N,M,K;
	long long int P;
	scanf("%d %d %d %lld",&N,&M,&K,&P);
	struct link ways[N+1];
	int sest[N+1];
	for(i=0;i<N+1;i++){
		init(&ways[i]);
	}
	int a,b,c;
	for(i=0;i<M;i++){
		scanf("%d %d %d",&a,&b,&c);
		apend(&ways[a],b,c);
	}
	sest[1]=0;
	for(i=1;i<=N;i++){
		int j;
		int min=-1;
		int mino=1;
		for(j=1;j<=N;j++){
			if(sest[j]==-1) continue;
			if(sest[j]<min||min==-1){
				min=sest[j];
				mino=j;
			}
		}
		for(j=1;j<=N;j++){
//			sest[j]=min(sest[j],sest[mino]+sear(ways[mino],j));
			if(sear(&ways[mino],j)==-1) continue;
			if(sest[j]>sest[mino]+sear(&ways[mino],j)||sest[j]==-1) sest[j]=sest[mino]+sear(&ways[mino],j);
		}
	}
	for(i=1;i<=N;i++){
		printf("%d\n",sest[i]);
	}
	return 0;
}


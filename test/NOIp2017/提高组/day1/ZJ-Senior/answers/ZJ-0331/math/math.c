#include <stdio.h>

int main(void){
	int x,y;
	FILE * in=fopen("math.in","r");
	FILE * out=fopen("math.ans","w");
	fscanf(in,"%d %d",&x,&y);
	int ans=(x-1)*(y-1)-1;
	fprintf(out,"%d",ans);
	fclose(in);
	fclose(out);
	return 0;
}
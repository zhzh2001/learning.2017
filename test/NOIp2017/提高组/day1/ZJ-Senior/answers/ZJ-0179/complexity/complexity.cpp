#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
#define LL long long
#define N (100001)
#define INF (1000000007)
using namespace std;
int T, tail, top;
char c[N][21];
int fi[N], la[N], sta[N], id[N], tmp[N], st[N], en[N], com[N], size[N], woo[N], cf[N];
bool used[201], ki[N];
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p -48;
		p = getchar();
	}while(isdigit(p));
	if (fl) t= -t;
}
int main(){
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	 read(T);
	 while (T--){
	 	memset(st, 0, sizeof(st));
	 	memset(en, 0, sizeof(en));
	 	memset(ki, 0, sizeof(ki));
	 	memset(size, 0, sizeof(size));
	 	memset(woo, 0, sizeof(woo));
	 	bool kuohao = 0, chong = 0;
	 	top = 0, tail = 0;
	 	memset(used, 0, sizeof(used));
	 	memset(cf, 0, sizeof(cf));
	 	int n, maxsize = 0;
	 	read(n);
	 	for (int i = 0; i <= n; i++){
	 		gets(c[i]+1);
	 		int len = strlen(c[i]+1);
	 		bool fl = 0;
	 		if (c[i][1] == 'F'){
	 			for (int j = 2; j <= len; j++){
	 				if (isdigit(c[i][j])){
	 					if (!fl){
	 						st[i] = st[i]*10+c[i][j]-48;
						}
						else {
						 	en[i] = en[i]*10+c[i][j]-48;
						}
					}
					else{
						if (st[i] && c[i][j] == ' ') fl = 1;
					 	if (c[i][j] == 'n'){
					 		if (fl) en[i] = INF;
					 		else st[i] = INF;
							}
					}
			 	 }
			 	 //printf("st en %d %d\n", st[i], en[i]);
			 }
		 }
		 for (int i = 1; i <= n; i++){
		 	if (c[i][1] == 'F'){
		 		st[++top] = i;
			 }
			 if (c[i][1] == 'E'){
		 		if (top == 0){
		 			printf("ERR\n");
		 			kuohao = 1;
		 			break;
				 }
		 		tail++;
		 		id[st[top]] = tail;
		 		id[i] = tail;
		 		com[st[top]] = i;
		 		fi[tail] = st[top];
		 		la[tail] = i;
		 		top--;
			 }
		 }
		 if (kuohao) continue;
		 if (top != 0){
		 	printf("ERR\n");
		 	continue;
		 } 
		 for (int i = 1; i <= tail; i++){
		 	 memset(used, 0, sizeof(used));
		 	 used[c[fi[i]][3]] = 1;
		 	 for (int j = 1; j <= i-1; j++){
		 	      if (fi[j] > fi[i] && la[j] < la[i]){
		 			 size[i] = max(size[i], size[j]);
		 			 if (used[c[fi[j]][3]]){
		 					printf("ERR\n");
		 					chong = 1;
		 					break;
					  }
					  used[c[fi[j]][3]] = 1;
				 }
			 }
			 if (chong) break;
			 size[i]++;
			 for (int j = 1; j <= i-1; j++){
			 	if (fi[j] > fi[i] && la[j] < la[i] && size[j] == size[i-1]) woo[i] = max(woo[i], woo[j]);
			 }
			 if (st[fi[i]] < en[fi[i]] && en[i] == INF) woo[i]++;
			 if (st[fi[i]] > en[fi[i]]) woo[i] = 0;
			 //cf[size[i]] = cf[size[i]-1];
			 //printf("woo %d %d\n", woo[i], size[i]);
			 cf[size[i]] = max(cf[size[i]], woo[i]);
			 maxsize = max(maxsize, size[i]);
		 }
		 //printf("cf %d\n", cf[maxsize]);
		 if (chong) continue;
		 if (c[0][3] == '1' && cf[maxsize] == 0){
		 	printf("Yes\n");
		 	continue;
		 }
		 if (c[0][3] == 'n' && cf[maxsize] == c[0][5]-48){
		 	printf("Yes\n");
		 	continue;
		 }
		 printf("No\n");
	 }
	return 0;
}


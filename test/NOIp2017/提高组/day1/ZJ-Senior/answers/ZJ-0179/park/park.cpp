#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
#define LL long long
#define N (100001)
#define M (200001)
using namespace std;
int T, n, m, k, mod, mindis, cnt, h, t, ne;
int dfn[N], low[N], head[N], nxt[M], a[M], b[M], q[N<<2], f[N], be[N], x[M], y[M], z[M], g[N];
bool vis[N], bst[N]; 
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p -48;
		p = getchar();
	}while(isdigit(p));
	if (fl) t= -t;
}
inline void add(int x, int y, int z){
	a[++k] = y; b[k] = z; nxt[k] = head[x]; head[x] = k;
}
void spfa(){
	memset(f, 127, sizeof(f));
	f[1] = 0;
	h = t = 0;
	q[++t] = 1;
	while (h < t){
		int u = q[++h];
		for (int p = head[u]; p; p = nxt[p]){
			if (f[u]+b[p] < f[a[p]]){
				f[a[p]] = f[u]+b[p];
				g[a[p]] = 1;
				if (!vis[a[p]]) q[++t] = a[p], vis[a[p]] = 1;
			}
			else{
				if (f[u]+b[p] == f[a[p]]){
					g[a[p]]+=g[u];
					g[a[p]] %= mod;  
				}
			}
		}
		vis[u] = 1;
	}
}
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	read(T);
	while (T--){
		read(n), read(m), read(k), read(mod);
		for (int i = 1; i <= m; i++){
			read(x[i]); read(y[i]); read(z[i]);
			add(x[i], y[i], z[i]);
		}
		spfa();
		printf("%d\n", g[n]);
	}
	return 0;
}


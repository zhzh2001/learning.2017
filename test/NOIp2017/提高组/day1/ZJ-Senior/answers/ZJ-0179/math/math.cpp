#include<cstdio>
#include<algorithm>
#include<cctype>
#define LL long long
#define N (100001)
using namespace std;
LL a, b, x, y, tot, ans, cz, k;
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p -48;
		p = getchar();
	}while(isdigit(p));
	if (fl) t= -t;
}
LL exgcd(LL a, LL b, LL &x, LL &y){
	if (b == 0){
		x = 1; y = 0;
		return a;
	}	
	LL g = exgcd(b, a%b, y, x);
	y -= a/b*x;
	return g;
}
int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	read(a), read(b);
	LL gc = exgcd(a, b, x, y);
	//printf("%lld %lld %lld\n", gc, x, y);
	LL ma = a/gc*b;
	if (y < 0){
		swap(x, y);
		swap(a, b);
	}
	ans = gc;
	for (int i = 1; i <= 30000000; i++){
		if (tot > a+b) break;
		if (y*i/a*b+x*i >= 0) tot++;
		else {
			ans = i;
			tot = 1;
		}
	}
	printf("%lld", ans);
	return 0;
}

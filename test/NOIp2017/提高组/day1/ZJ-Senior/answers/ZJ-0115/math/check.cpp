#include<cstdio>
#include<algorithm>
using namespace std;
int f[11000000];
int main()
{
	freopen("math.in","r",stdin);
	freopen("check.out","w",stdout);
	int a,b,maxx=0,t=0;
	scanf("%d%d",&a,&b);
	int flag=1;
	f[0]=1;
	while (flag)
	{
		flag=0;
		t++;
		f[t]=0;
		if (t>=a)f[t]|=f[t-a];
		if (t>=b) f[t]|=f[t-b];
		if (f[t]==0) maxx=max(maxx,t);
		for (int j=0;j<min(a,b);j++)
			if (t>j&&!f[t-j])
			{
				flag=1;
				break;
			}
		printf("%d\n",t);
	}
	printf("%d\n",maxx);
}

#include<cstdio>
#include<algorithm>
using namespace std;
bool f[16000000];
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int a,b,maxx=0,t=0;
	scanf("%d%d",&a,&b);
	int flag=1;
	f[0]=true;
	while (flag)
	{
		flag=0;
		t++;
		f[t]=false;
		if (t>=a)f[t]|=f[t-a];
		if (t>=b) f[t]|=f[t-b];
		if (f[t]==0) maxx=max(maxx,t);
		for (int j=0;j<min(a,b);j++)
			if (t>j&&!f[t-j])
			{
				flag=1;
				break;
			}
		//printf("%d\n",t);
	}
	printf("%d\n",maxx);
}

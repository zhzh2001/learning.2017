#include<cstdio>
#include<algorithm>
#include<ctime>
#include<cstdlib>
using namespace std;
int gcd(int  a,int  b)
{
	if (b==0)return a; 
	return gcd(b,a%b);
}
int main()
{
	freopen("math.in","w",stdout);
	int a,b;
	srand(time(NULL));
	a=rand()%10000+1;
	b=rand()%10000+1;
	while (gcd(a,b)!=1) a=rand()%10000+1,b=rand()%10000+1;
	printf("%d %d\n",a,b);
}

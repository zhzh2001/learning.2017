#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
using namespace std;
int n;
char st[210000];
int flag,maxx,top,k,num,num1,num2,ans1,ans2,sum,q[210000],f[210000],w[210000];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		scanf("%s",st);
		if (st[2]=='1') k=0;
		 else
		 {
		 	k=0;	
		 	for (int i=4;i<strlen(st)-1;i++)
		 		k=k*10+st[i]-'0';
		 }
		 flag=0;
		 maxx=0;
		 sum=0;
		 top=0;
		 int t=0;
		 memset(f,0,sizeof(f));
		 for (int i=1;i<=n;i++)
		 {
		 	char ch=getchar();
		 	while (ch!='E'&&ch!='F') ch=getchar();
		 	if (ch=='E')
		 	{
		 		if (top==0)
		 		{
		 			//cout<<top<<endl;
		 			flag=1;
				 } else 
				 {
				 	f[w[top]]=0;
				 	if (q[top]==1) sum--;
				 	else if (q[top]==-1) t--;
					top--; 
				 }
			 } else
			 {
			 	 while (ch<'a'||ch>'z') ch=getchar();  
			 	if (!f[ch-'a'])
			 	{
			 		f[ch-'a']=1;
			 		w[top+1]=ch-'a';
				 } else
				 {
				 	//cout<<ch<<endl;
				 	flag=1;
				 }
				 ch=getchar(); num1=0; num2=0; ans1=0; ans2=0; num=0;
				 while (ch!='\n')
				 {
				 	//printf("%d\n",num);
				 	if (ch=='n'&&num1==0) num1=2;
					else if (ch=='n'&&num2==0) num2=2;
					else if (ch>='0'&&ch<='9'&&num==1) num1=1,ans1=ans1*10+ch-'0';
					else if (ch>='0'&&ch<='9'&&num==2) num2=1,ans2=ans2*10+ch-'0';
					else if (ch==' ') num++;
					ch=getchar();
				 }
				 if (num1==1&&num2==2) q[++top]=1,sum++; else 
				 if (num1==1&&num2==1&&ans1<=ans2) q[++top]=0; else
				 if (num1==1&&num2==1&&ans1>ans2) q[++top]=-1,t++; else
				 if (num1==2&&num2==2) q[++top]=0; else
				 if (num1==2&&num2==1)q[++top]=-1,t++;
				 if (!t) maxx=max(maxx,sum);
				 //cout<<w[top]<<endl;
			  } 
		 }
		 //cout<<flag<<' '<<top<<endl;
		 if (flag||top) 
		 {
		 	printf("ERR\n");
		 	continue;
		 }
		 //cout<<maxx<<' '<<k<<endl;
		 if (maxx==k) printf("Yes\n"); else printf("No\n");
	}
}

#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<Cstring>
using namespace std;
int addnum,vel[2100],key[2100],ne[2100],head[2100],dis[2100],vis[2100];
int ans,n,m,k,p,minn,u;
void add(int u,int v,int k)
{
	addnum++;
	vel[addnum]=v;
	key[addnum]=k;
	ne[addnum]=head[u];
	head[u]=addnum;
}
void dfs(int x,int sum)
{
	if (sum>dis[n]+k) return;
	if (x==n)
	{
		ans++;
		return;
	}
	for (int e=head[x];e;e=ne[e])
	{
		int v=vel[e];
		dfs(v,sum+key[e]);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		addnum=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=n;i++) head[i]=0;
		for (int i=1;i<=m;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		for (int i=1;i<=n;i++) dis[i]=1e9,vis[i]=0;
		dis[1]=0; 
		for (int i=1;i<=n;i++)
		{
			minn=1e9;
			for (int j=1;j<=n;j++)
				if (!vis[j]&&dis[j]<minn) u=j,minn=dis[j]; 
			vis[u]=1;
			//printf("%d\n",dis[u]);
			for (int e=head[u];e;e=ne[e])
			{
				int v=vel[e];
				if (dis[u]+key[e]<dis[v]) dis[v]=dis[u]+key[e];
			}
		}
		//printf("%d\n",dis[n]);
		ans=0;
		dfs(1,0);
		printf("%d\n",ans);
	}
	
}

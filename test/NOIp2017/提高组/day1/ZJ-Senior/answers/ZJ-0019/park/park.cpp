#include<bits/stdc++.h>
#include<queue>
#include<vector>
using namespace std;
queue<int>q;
vector<pair<int,int> >s[200005];
int n,m,_hash[200005],a[200005],k,p,T,ans;
void dfs(int root,int len){
	if(len>k)
		return;
	if(root==n){
		++ans;
		ans%=p;
	}
	for(int i=0;i<s[root].size();++i)
		dfs(s[root][i].first,s[root][i].second+len);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T){
		--T;
		ans=0;
		memset(_hash,0,sizeof(_hash));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		if(m*n>6000000){
			printf("-1\n");
			continue;
		}
		for(int i=1;i<=n;++i)
			s[i].clear();
		int ll,rr,w;
		for(int i=1;i<=m;++i){
			scanf("%d%d%d",&ll,&rr,&w);
			s[ll].push_back(make_pair(rr,w));
		}
		memset(a,127/3,sizeof(a));
		a[1]=0;
		_hash[1]=1;
		q.push(1);
		while(!q.empty()){
			int head=q.front();
			q.pop();
			for(int i=0;i<s[head].size();++i){
				int kk=s[head][i].first;
				w=s[head][i].second;
				if(a[kk]>a[head]+w){
					a[kk]=a[head]+w;
					if(_hash[kk]==1)
						continue;
					_hash[kk]=1;
					q.push(kk);
				}
			}
		}
		//printf("%d\n",a[n]);
		//for(int i=1;i<=n;++i)
		//	cout<<s[i].size()<<endl;
		k=a[n]+k;
		dfs(1,0);
		printf("%d\n",ans%p);
	}
	
	return 0;
}

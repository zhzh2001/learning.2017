#include<bits/stdc++.h>
#include<iostream>
using namespace std;
int check(string a,string b){
	if(a=="n" && b!="n")
		return 0;
	if(a=="n" && b=="n")
		return 1;
	if(a!="n" && b=="n")
		return 1;
	if(a.size()>b.size())
		return 0;
	if(a.size()<b.size())
		return 1;
	if(a>b)
		return 0;
	return 1;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin.sync_with_stdio(false);
	int T;
	cin>>T;
	while(T){
		--T;
		int n,m,ss[30]={0},mx=0;;
		string s;
		cin>>n>>s;
		if(s[2]=='1')
			m=0;
		else
			m=s[4]-48;
		int kk[10005]={0},head=0,tail=0;
		char t;
		string a,b;
		char c;
		for(int i=1;i<=n;++i){
			cin>>c;
			if(c=='E')
				--head;
			else{
				cin>>t>>a>>b;
				ss[t-'a'+1]=1;
				++tail;
				if(a!="n" && b=="n"){
					if(kk[head]!=-1)
						kk[tail]=kk[head]+1;
					else
						kk[tail]=-1;
					mx=max(kk[tail],mx);
				}
				else if(check(a,b))
					kk[tail]=kk[head];
				else
					kk[tail]=-1;
				head=tail;
			}
		}
		if(mx==m)
			printf("Yes\n");
		else
			printf("No\n");
		//cout<<n<<" "<<m<<endl;;
	}
	return 0;
}


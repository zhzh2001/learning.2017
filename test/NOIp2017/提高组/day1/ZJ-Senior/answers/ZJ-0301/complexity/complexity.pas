var
  time,j,t,i,l,cas,top,ans,max,q:longint;
  num,w:array[0..10000] of longint;
  st,st2:string;
  ch,ch1,ch2,ch3,ch4,ch5,ch6,ch7:char;
  chs:string;
  flag,f:boolean;
begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(t);
  for cas:=1 to t do
  begin
    readln(l,st);
    flag:=false;top:=0;chs:='';
    for i:=0 to 100 do w[i]:=0;
    for i:=1 to l do
    begin
      if top<0 then begin flag:=true;end;
      read(ch);
      if ch='F' then
      begin
        read(ch1);
        inc(top);
        read(ch2,ch3);
        chs[top]:=ch2;
        for j:=1 to top-1 do if ch2=chs[j] then flag:=true;
        read(ch4);
        if (ch4>='0') and (ch4<='9') then
        begin
          read(ch5);
          if (ch5>='0') and (ch5<='9') then
          begin
            read(ch6);
            q:=(ord(ch4)-ord('0'))*10+ord(ch5)-ord('0');
          end
          else q:=ord(ch4)-ord('0');
          read(ch7);
          if ch7='n' then
          begin
            w[top]:=w[top-1]+1;
          end
          else
          begin
            ans:=ord(ch7)-ord('0');
            read(st2);
            for j:=1 to length(st2) do
            ans:=ans*10+ord(st2[j])-ord('0');
            if ans>=q then w[top]:=w[top-1];
          end;
        end;
        if ch4='n' then
        begin
          read(ch5,ch6);
          if ch6='n' then w[top]:=w[top-1];
        end;
      //  writeln(ch,ch1,ch2,ch3,ch4,ch5,ch6,ans);
        readln;
      end;
      if ch='E' then
      begin
        chs[top]:=' ';
        dec(top);
        readln;
        //writeln('E');
      end;
    //  writeln(top,' ',i);
    end;
    if top<>0 then flag:=true;
    if flag then begin writeln('ERR');continue;end;
        max:=0;   ans:=0;time:=1;
      f:=false;
      for j:=0 to 100 do if w[j]>max then max:=w[j];
      if (length(st)=5) and (max=0) then f:=true;
      j:=length(st)-1;
      while (st[j]>='0')  and (st[j]<='9') do
      begin
        ans:=ans+time*(ord(st[j])-ord('0'));
        dec(j);
        time:=time*10;
      end;
      if max=ans then f:=true;
      if f then writeln('Yes')
      else writeln('No');
  end;
  close(input);close(output);
end.

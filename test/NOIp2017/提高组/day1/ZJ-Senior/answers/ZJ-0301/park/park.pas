var
  m,n,k,p,t,cas,i,edgenum,head,tail,w,u,v,max,num,j:longint;
  numm,dis,q,vet,val,h,ans,next:array[0..1000000] of longint;
  flag:array[0..1000000] of boolean;
  qq,va:array[0..100000,0..50] of longint;
procedure add(u,v,w:longint);
begin
  inc(edgenum);
  vet[edgenum]:=v;
  val[edgenum]:=w;
  next[edgenum]:=h[u];
  h[u]:=edgenum;
end;
procedure dfs(u,sum:longint);
var
  v,i:longint;
begin
  if u=n then begin inc(num);end;
  i:=h[u];
  while i<>0 do
  begin
    v:=vet[i];
    if sum+val[i]<=max then dfs(v,sum+val[i]);
    i:=next[i];
  end;
end;
begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  readln(t);
  for cas:=1 to t do
  begin
    readln(n,m,k,p);
    edgenum:=0;
    for i:=1 to m+n do
    begin
      vet[i]:=0;
      val[i]:=0;
      next[i]:=0;
      h[i]:=0;
    end;
    for i:=1 to m do
    begin
      read(u,v,w);
      add(u,v,w);
    end;
    for i:=1 to n do dis[i]:=maxlongint;
    for i:=1 to n do ans[i]:=0;
    for i:=1 to n do numm[i]:=0;
    dis[1]:=0;
    for i:=1 to 100000 do q[i]:=0;
    head:=0;tail:=1;q[1]:=1;
    for i:=1 to n do ans[i]:=0;ans[1]:=1;
    for i:=1 to n do flag[i]:=false;
    flag[1]:=true;
    while head<tail do
    begin
      inc(head);u:=q[head];
      flag[u]:=false;
      i:=h[u];
      while i<>0 do
      begin
        v:=vet[i];
        if dis[u]+val[i]<=dis[v] then
        begin
          dis[v]:=dis[u]+val[i];
          if n>2000 then begin
          if dis[u]+val[i]<dis[v] then
          begin
            ans[v]:=ans[u];
            numm[v]:=1;
            qq[v,numm[v]]:=u;
            va[v,numm[v]]:=ans[u];
          end;
          if dis[u]+val[i]=dis[v] then
          begin
            for j:=numm[v] downto 1 do
            if qq[v,j]=u then begin ans[v]:=ans[v]-va[v,j]; break;end;
            ans[v]:=(ans[v]+ans[u]) mod p;
            inc(numm[v]);
            qq[v,numm[v]]:=u;
            va[v,numm[v]]:=ans[u];
          end;  end;
         // writeln(dis[n],' ',u,' ',v,' ',ans[n]);
          if flag[v]=false then
          begin
            flag[v]:=true;
            inc(tail);q[tail]:=v;
          end;
        end;
        i:=next[i];
      end;
    end;
    if (k=0) and (n>=10000) then writeln(ans[n])
    else
    begin
      max:=dis[n]+k; num:=0;
      for i:=1 to n do ans[i]:=0;
      dfs(1,0);
      writeln(num mod p);
    end;
  end;
  close(input);close(output);
end.

#include<cstdio>
#include<cstring>
#include<algorithm>

#define N 233

using namespace std;

int T,l,d,top,f,w;
char x[N];
int len[N], fz[N];
char s[N][N];
bool judge[26];

int read(char s[])
{
	int c;
	int cnt=0;
	while(c=getchar(), c!='\n') s[++cnt]=c;
	s[cnt+1]='\0';
	return cnt;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d\n",&T);
	while(T--)
	{
		d=read(x);
		l=0;
		for(int i=1; i<=d; ++i)
			if(x[i]>='0'&&x[i]<='9') l=l*10+x[i]-48; else break;
		for(int i=1; i<=d; ++i)
			if(x[i]=='O')
			{
				if(x[i+2]=='1') f=0;
				else
				{
					f=0;
					for(int j=i+4; j<=d; ++j)
						if(x[j]>='0'&&x[j]<='9') f=f*10+x[j]-48;
				}
				break;
			}
		w=0;
		top=0;
		fz[0]=0;
		memset(judge,false,sizeof(judge));
		for(int i=1; i<=l; ++i)
		{
			d=read(x);
			if(x[1]=='F')
			{
				if(judge[x[3]-'a'])
				{
					printf("ERR\n");
					w=i;
					break;
				}
				judge[x[3]-'a']=true;
				fz[++top]=0;
				len[top]=d;
				memcpy(s[top],x,sizeof(x));
			}
			else
			{
				if(top==0)
				{
					printf("ERR\n");
					w=i;
					break;
				}
				else
				{
					judge[s[top][3]-'a']=false;
					int a=0, b=0, k;
					if(s[top][5]!='n')
					{
						for(k=5; k<=len[top]; ++k)
							if(s[top][k]>='0'&&s[top][k]<='9') a=a*10+s[top][k]-48;
							else break;
					} else a=-1, k=6;
					if(s[top][k+1]!='n')
					{
						for(k=k+1; k<=len[top]; ++k)
							if(s[top][k]>='0'&&s[top][k]<='9') b=b*10+s[top][k]-48;
							else break;
					}
					else b=-1;
					if(a==-1)
					{
						fz[top-1]=max(fz[top-1],0);
					}
					else
					{
						if(b==-1)
						{
							fz[top-1]=max(fz[top-1],fz[top]+1);
						}
						else
						{
							if(a<=b)
							{
								fz[top-1]=max(fz[top-1],fz[top]);
							}
							else
							{
								fz[top-1]=max(fz[top-1],0);
							}
						}
					}
					--top;
				}
			}
		}
		if(w)
		{
			for(int i=w+1; i<=l; ++i) read(x);
			continue;
		}
		if(top!=0)
		{
			printf("ERR\n");
			continue;
		}
		if(fz[0]==f) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

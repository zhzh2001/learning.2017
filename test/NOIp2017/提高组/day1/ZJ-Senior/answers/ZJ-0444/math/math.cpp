#include<cmath>
#include<cstdio>
#include<algorithm>

using namespace std;

long long a,b,x,y,tx,ty,f,g,ans;

void exgcd(long long a, long long b, long long &x, long long &y)
{
	if(b==0)
	{
		x=1, y=0;
		return;
	}
	exgcd(b,a%b,x,y);
	long long t=x-a/b*y;
	x=y, y=t;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	exgcd(a,b,x,y);
	if(a==1||b==1)
	{
		printf("0\n");
		return 0;
	}
	if(x>0)
	{
		tx=x-b;
		ty=y+a;
		f=abs(tx)/x;
		g=abs(tx)%x;
		ans=min(abs(b*y)*(f+1),abs(b*y)*f+g*a);
	}
	else
	{
		tx=x+b;
		ty=y-a;
		f=abs(ty)/y;
		g=abs(ty)%y;
		ans=min(abs(a*x)*(f+1),abs(a*x)*f+g*b);
	}
	printf("%lld\n",ans-1);
	return 0;
}

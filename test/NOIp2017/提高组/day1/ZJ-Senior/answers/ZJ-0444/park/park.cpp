#include<cstdio>
#include<vector>
#include<cstring>

using namespace std;

int T;
int n,m,k,p,adr,ans;
int dist[2005], num[2005];
bool vis[2005];
vector<int> to[2005];
vector<int> f[2005];

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1; i<=m; ++i)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			to[u].push_back(v);
			f[u].push_back(w);
		}
		memset(num,0,sizeof(num));
		memset(vis,false,sizeof(vis));
		memset(dist,63,sizeof(dist));
		dist[1]=0;
		vis[1]=true;
		for(int i=0; i<to[1].size(); ++i)
		{
			if(dist[1]+f[1][i]==dist[to[1][i]]) ++num[to[1][i]];
			else
			{
				if(dist[1]+f[1][i]<dist[to[1][i]])
				{
					dist[to[1][i]]=dist[1]+f[1][i];
					num[to[1][i]]=1;
				}
			}
		}
		for(int i=2; i<=n; ++i)
		{
			adr=0;
			for(int j=1; j<=n; ++j)
			{
				if(!vis[j])
				{
					if(adr)
					{
						if(dist[j]<dist[adr]) adr=j;
					}
					else adr=j;
				}
			}
			vis[adr]=true;
			for(int j=0; j<to[adr].size(); ++j)
			{
				if(dist[adr]+f[adr][j]==dist[to[adr][j]]) num[to[adr][j]]=(num[adr]+num[to[adr][j]])%p;
				else
				{
					if(dist[adr]+f[adr][j]<dist[to[adr][j]])
					{
						dist[to[adr][j]]=dist[adr]+f[adr][j];
						num[to[adr][j]]=1;
					}
				}
			}
		}
		printf("%d\n",num[n]);
	}
	return 0;
}

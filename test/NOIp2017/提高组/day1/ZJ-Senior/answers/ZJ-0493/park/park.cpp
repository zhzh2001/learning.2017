#include<bits/stdc++.h>
using namespace std;
#define inf 1000000000
#define N 100010
#define M 200010
struct edge{
	int to,next,len;
}e[M],ee[M];
int T,n,m,k,p,head[N],cnt,minl,vis[M],dis[N],q[N],num[N],diss[N],headd[N],disss[N];
/*int spfa(int s,int t){
	queue<int> q;
	while(!q.empty())q.pop();
	q.push(s);dis[s]=0;vis[s]=1;
	while(!q.empty()){
		int now=q.first();
		q.pop();vis[fir]=0;
		for (int i=head[fir];i;i=e[i].next){
			int tmp=e[i].to;
			if(dis[fir]+e[i].len<dis[tmp]){
				dis[tmp]=dis[fir]+e[i].len;
				if(!vis[tmp])q.push(tmp);
			}
		}
	}
	return dis[t];
}*/
void spfa(int s,int t){
	for (int i=1;i<=n;i++)dis[i]=inf;
	int h=0,tail=1;
	q[1]=s;dis[s]=0;vis[s]=1;
	while(h<tail){
		(h+=1)%=N;int fir=q[h];
		vis[fir]=0;
		for (int i=head[fir];i;i=e[i].next){
			int tmp=e[i].to;
			if(dis[fir]+e[i].len<dis[tmp]){
				dis[tmp]=dis[fir]+e[i].len;
				if(!vis[tmp]){
					(tail+=1)%=N;
					q[tail]=tmp;
				}
			}
		}
	}
}
void spfa1(int s,int t){
	for (int i=1;i<=n;i++)diss[i]=inf;
	int h=0,tail=1;
	q[1]=s;diss[s]=0;vis[s]=1;
	while(h<tail){
		(h+=1)%=N;int fir=q[h];
		vis[fir]=0;
		for (int i=headd[fir];i;i=ee[i].next){
			int tmp=ee[i].to;
			if(diss[fir]+ee[i].len<diss[tmp]){
				diss[tmp]=diss[fir]+ee[i].len;
				if(!vis[tmp]){
					(tail+=1)%=N;
					q[tail]=tmp;
				}
			}
		}
	}
}
void spfa2(int s,int t){
	for (int i=1;i<=n;i++)disss[i]=inf;
	int h=0,tail=1;
	q[1]=s;disss[s]=0;vis[s]=1;
	while(h<tail){
		(h+=1)%=N;int fir=q[h];
		vis[fir]=0;
		for (int i=head[fir];i;i=e[i].next){
			int tmp=e[i].to;
			if(disss[fir]+e[i].len<disss[tmp]){
				disss[tmp]=disss[fir]+e[i].len;
				if(!vis[tmp]){
					(tail+=1)%=N;
					q[tail]=tmp;
				}
			}
		}
	}
}
void add(int u,int v,int w){
	e[++cnt]=(edge){v,head[u],w};head[u]=cnt;
	ee[cnt]=(edge){u,headd[v],w};headd[v]=cnt;
}
void dfs(int x){
	for (int i=head[x];i;i=e[i].next)
	    if(!vis[i]){
		    int tmp=e[i].to;
			if(!num[tmp])dfs(tmp);
			(num[x]+=num[tmp])%=p;
	    }
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		cnt=0;
		memset(head,0,sizeof(head));
		memset(headd,0,sizeof(headd));
		memset(vis,0,sizeof(vis));
		memset(num,0,sizeof(num));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		spfa(1,n);
		spfa1(n,1);
		int d=dis[n];
		if(dis[n]==0){
			spfa2(n,1);
			if(disss[1]==0){
				printf("-1\n");
				continue;
			}
		}
		for (int i=1;i<=n;i++){
			for (int j=head[i];j;j=e[j].next)
			    if(dis[i]+diss[e[j].to]+e[j].len>d+k)vis[j]=1;
		}
		num[n]=1;
		dfs(1);
		printf("%d\n",num[1]);
	}
	return 0;
}

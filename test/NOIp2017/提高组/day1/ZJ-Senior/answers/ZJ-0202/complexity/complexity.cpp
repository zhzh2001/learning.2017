#include<bits\stdc++.h>
typedef long long ll;
using namespace std;
int n,T,ff,f[128];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void readln()
{
	char ch=getchar();
	while (ch!='\n') ch=getchar();
	n--;	
	return;
}
int sc(int ss)
{
	if (n==0)
	{
		ff=-1;
		return 0;
	}
	int y;
	char ch=getchar(),ch1;
	if (ch=='E')
	{
		readln();
		if (ss==0) ff=-1;
		return 0;
	}
	ch=getchar();ch=getchar();
	if (f[ch]==1)
	{
		readln();
		ff=-1;
		y=sc(ss+1);
	}
	else
	{
		f[ch]=1;int cx,cx1;
		ch=getchar();ch=getchar();
		if (ch!='n')
		{
			cx=ch-'0';
			while (ch>='0'&&ch<='9'){cx=cx*10+ch-'0';ch=getchar();}
		} else ch1=getchar();
		ch1=getchar();
		if (ch1!='n')
		{			
			cx1=ch1-'0';
			while (ch1>='0'&&ch1<='9'){cx1=cx1*10+ch1-'0';ch1=getchar();}
			n--;
		} else readln();
		ss++;
		if (ch=='n'&&ch1!='n'||ch!='n'&&ch1!='n'&&cx>cx1)
		{
			y=sc(ss+1);y=0;
		}
		else
		{
			if (ch1=='n'&&ch!='n') y=sc(ss+1)+1;
			else y=sc(ss+1);
		}
		f[ch]=0;
	}
	memset(f,0,sizeof(f));
	if (n>0) y=max(y,sc(ss));
	return y;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while (T--)
	{
		n=read();
		int x=0,f=0;char ch=getchar();
		while (ch<'0'||ch>'9'){if(ch=='n')f=1;ch=getchar();}
		while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
		ch=getchar();x=x*f;ff=1;
		int s=sc(0);
		if (ff==-1) puts("ERR");
		else if (s==x) puts("Yes");		 
		else puts("No");
	}
	return 0;
}

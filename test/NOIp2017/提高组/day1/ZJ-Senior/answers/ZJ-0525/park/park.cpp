#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define ll long long
const int N=100010;
const int M=200010;
struct node{int to,len,next;}edge[M];
struct note{int x,y;}h[N*55];
int head[N],tot=0,cot;
int n,m,K;
int heap[N*5],p[N],dis[N];
ll P,f[N],fs[N][55];
bool t[N][55],tt[N];
void add_edge(int x,int y,int s){
	edge[++tot]=(node){y,s,head[x]};
	head[x]=tot;
}
void in_heap(int v){
	int k=++heap[0];
	while (k>1&&dis[v]<dis[k/2]) {p[heap[k/2]]=k;heap[k]=heap[k/2];k/=2;}
	heap[k]=v;p[v]=k;
}
int out_heap(){
	int ans=heap[1];p[ans]=0;
	int k=1,v=heap[heap[0]--];
	while (k*2<=heap[0]){
		int y;
		if (k*2+1<=heap[0]&&dis[heap[k*2+1]]<dis[heap[k*2]]) y=k*2+1;else y=k*2;
		if (dis[v]>dis[heap[y]]) {p[heap[y]]=k;heap[k]=heap[y];k=y;}
		else break;
	}
	p[v]=k;heap[k]=v;
	return ans;
}
void change_heap(int v){
	int k=p[v];
	while (k>1&&dis[v]<dis[k/2]) {p[heap[k/2]]=k;heap[k]=heap[k/2];k/=2;}
	while (k*2<=heap[0]){
		int y;
		if (k*2+1<=heap[0]&&dis[heap[k*2+1]]<dis[heap[k*2]]) y=k*2+1;else y=k*2;
		if (dis[v]>dis[heap[y]]) {p[heap[y]]=k;heap[k]=heap[y];k=y;}
		else break;
	}
	p[v]=k;heap[k]=v;
}

void dijstra()
{
	memset(dis,-1,sizeof(dis));
	memset(f,0,sizeof(f));
	heap[0]=1;heap[1]=1;
	dis[1]=0;f[1]=1;
	memset(p,0,sizeof(p));
	while (heap[0]){
		int u=out_heap();
		for (int i=head[u];i;i=edge[i].next){
			int v=edge[i].to,len=edge[i].len;
			if (dis[v]==-1){
				dis[v]=dis[u]+len;
				f[v]=f[u];
				in_heap(v);
			}
			else{
				if (dis[u]+len<dis[v]){
					dis[v]=dis[u]+len;
					f[v]=f[u];
					change_heap(v);
				}
				else
					if (dis[u]+len==dis[v]) f[v]=(f[v]+f[u])%P;
			}
		}
	}
}
void in_h(int x,int y){
	t[x][y]=1;
	int k=++cot;
	while (k>1&&dis[x]+y<dis[h[k/2].x]+h[k/2].y) {h[k]=h[k/2];k/=2;}
	h[k]=(note){x,y};
}
note out_h(){
	note ans=h[1];
	int k=1;
	note w=h[cot--];
	while (k*2<=cot){
		int y;
		if (k*2+1<=cot&&dis[h[k*2+1].x]+h[k*2+1].y<dis[h[k*2].x]+h[k*2].y) y=k*2+1;else y=k*2;
		if (dis[w.x]+w.y>dis[h[y].x]+h[y].y) {h[k]=h[y];k=y;}
		else break;
	}
	h[k]=w;
	t[ans.x][ans.y]=0;
	return ans;
}
void dijstra_2()
{
	memset(t,0,sizeof(t));
	memset(fs,0,sizeof(fs));
	cot=1;h[1]=(note){1,0};
	fs[1][0]=1;
	while (cot){
		note u=out_h();
		for (int i=head[u.x];i;i=edge[i].next){
			int v=edge[i].to,len=edge[i].len;
			if (dis[u.x]+u.y+len>dis[v]+K) continue;
			int y=dis[u.x]+u.y+len-dis[v];
			fs[v][y]=(fs[v][y]+fs[u.x][u.y])%P;
			if (!t[v][y]) in_h(v,y);
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;scanf("%d",&T);
	while (T--){
		
	memset(head,0,sizeof(head));
	memset(tt,0,sizeof(tt));
	tot=0;int num=0,nums=0;
	scanf("%d%d%d%lld",&n,&m,&K,&P);
	for (int i=1,x,y,s;i<=m;i++){
		scanf("%d%d%d",&x,&y,&s);
		add_edge(x,y,s);
		if (s==0) {nums++;if (!tt[x]){tt[x]=1;num++;}if (!tt[y]){tt[y]=1;num++;}}
	}
	if (nums>=num) {printf("-1\n");continue;}
	
	dijstra();
	
	if (K==0) {printf("%lld\n",f[n]);continue;}
	dijstra_2();
	ll ans=0;
	for (int i=0;i<=K;i++)
		ans=(ans+fs[n][i])%P;
	printf("%lld\n",ans);
	
	}
	return 0;
}

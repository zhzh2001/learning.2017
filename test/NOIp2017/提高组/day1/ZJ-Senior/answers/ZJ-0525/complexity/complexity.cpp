#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
int q[1010];
char C[1010];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	while (T--){
		
	int L;scanf("%d",&L);   
	char ch=getchar();
	while (ch!='O') ch=getchar();
	ch=getchar();
	
	int W=0;
	ch=getchar();
	if (ch=='1') W=-1;
	else{
		ch=getchar();
		ch=getchar();
		while (ch!=')'){
			W=W*10+ch-48;
			ch=getchar();
		}
	}
	q[0]=0;
	bool TAT=1;
	int tot=0;
	int ans=0;
	while (L--){
		
		ch=getchar();
		while (ch!='E'&&ch!='F') ch=getchar();
		if (ch=='F'){
			ch=getchar();
			ch=getchar();
			for (int i=1;i<=tot;i++)
				if (ch==C[i]) {TAT=0;break;}
			C[++tot]=ch;
			ch=getchar();
			bool tt1,tt2;
			int X=0,Y=0;
			ch=getchar();
			if (ch=='n'){
				tt1=1;
				ch=getchar();
			}
			else{
				tt1=0;
				while (ch!=' '){
					X=X*10+ch-48;
					ch=getchar();
				}
			}
			ch=getchar();
			if (ch=='n') tt2=1;
			else{
				tt2=0;
				while (ch>='0'&&ch<='9'){
					Y=Y*10+ch-48;
					ch=getchar();
				}
			}
			int m=tot;
			if (tt1&&tt2){ q[m]=q[m-1];}
			if (tt1&&!tt2){q[m]=-1;}
			if ((!tt1)&&tt2){q[m]=q[m-1]+1;if (q[m-1]==-1) q[m]=-1;}
			if ((!tt1)&&!tt2){q[m]=q[m-1];if (X>Y) q[m]=-1;}
		}
		else{
			if (tot==0) TAT=0;
			else{
			if (q[tot]>0) ans=max(ans,q[tot]);
			tot--;
			}
		}
	}
	if (tot>0) TAT=0;
	if (!TAT) printf("ERR\n");
	else{
		if (ans==0){
			if (W==-1) printf("Yes\n");
			else printf("No\n");
		}
		else{
			if (ans==W) printf("Yes\n");
			else printf("No\n");
		}
	}
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <queue>
#include <cstring>
#include <vector>
using namespace std;
#define MAXN 100010
#define MAXM 200010
//30�� 
int T,n,m,k,P,t1,t2,t3,head[MAXN],nxt[MAXM<<1],to[MAXM<<1],val[MAXM<<1],tot,
	dis1[MAXN],dis2[MAXN],kt,dfn[MAXN],st[MAXN],stp,belong[MAXN],low[MAXN],
	f[MAXN],ind[MAXN];
bool vis[MAXN],is[MAXM<<1];
void addedge(int b,int e,int v){
	nxt[++tot]=head[b];head[b]=tot;
	to[tot]=e;val[tot]=v;
}
struct _{
	inline bool operator()(int a,int b){
		if(kt==0)return dis1[a]>dis1[b];
		else return dis2[a]>dis2[b];
	}
};

void dijkstra(int*dis,bool odd,int s){
	kt=odd;priority_queue<int,vector<int>,_>q;
	dis[s]=0;
	q.push(s);
	while(!q.empty()){
		int t=q.top();q.pop();
		if(vis[t])continue;
		vis[t]=true;
		for(int i=head[t];i;i=nxt[i])
			if((i&1)^odd&&dis[t]+val[i]<dis[to[i]]){
				dis[to[i]]=dis[t]+val[i];
				q.push(to[i]);
			}
	}
}
int main(){
	freopen("park.in","r+",stdin);
	freopen("park.out","w+",stdout);
	size_t SSZ=sizeof dis1;
	size_t SSV=sizeof vis;
	size_t SIS=sizeof is;
	size_t SDF=sizeof dfn;
	scanf("%d",&T);
	while(T--){
		tot=0;
		memset(dis1,0x3f,SSZ);memset(dis2,0x3f,SSZ);
		memset(dfn,0,SDF);memset(low,0,SDF);memset(belong,0,SDF);
		memset(f,0,SDF);memset(ind,0,SDF);
		memset(is,0,SIS);memset(head,0,SDF);
		scanf("%d%d%d%d",&n,&m,&k,&P);
		for(int i=1;i<=m;++i){
			scanf("%d%d%d",&t1,&t2,&t3);
			addedge(t1,t2,t3);
			addedge(t2,t1,t3);
		}
		memset(vis,0,SSV);
		dijkstra(dis1,0,1);
		memset(vis,0,SSV);
		dijkstra(dis2,1,n);
		for(int u=1;u<=n;++u)
			for(int i=head[u];i;i=nxt[i])
				if((i&1)&&dis1[u]+val[i]+dis2[to[i]]==dis1[n])
					is[i]=true,++ind[to[i]];
		if(k==0){
			//bool flag=false;
			queue<int>q;
			q.push(1);
			f[1]=1;
			while(!q.empty()){
				int t=q.front();q.pop();
				for(int i=head[t];i;i=nxt[i])
					if(is[i]){
						//if(dis1[to[i]]==dis1[t]&&dis2[t]==dis2[to[i]])flag=true;
						--ind[to[i]];
						f[to[i]]=(f[to[i]]+f[t])%P;
						if(ind[to[i]]==0)
							q.push(to[i]);
					}
			}
			//if(flag)puts("-1");
			//else printf("%d\n",f[n]);
			printf("%d\n",f[n]);
		}else puts("-1");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

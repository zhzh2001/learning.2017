#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
#define MAXN 110
const int TN=-1;
int T,bf[MAXN],ef[MAXN],fc,ec,L,isf[MAXN];
char vf[MAXN],buf[20],ot[30];
bool err,have[256],vis[MAXN];
const char OUTPUT[3][10]={"No","Yes","ERR"};
int dfs(int line,int ctt){int res=ctt;bool can=true;
	vis[line]=true;
	if(have[vf[isf[line]]])err=true;
	have[vf[isf[line]]]=true;
	if(bf[isf[line]]==-1){
		if(ef[isf[line]]!=-1)can=false;
	}else{
		if(ef[isf[line]]==-1){
			if(res==-1)res=1;
			else ++res;
		}else if(bf[isf[line]]>ef[isf[line]])can=false;
	}
	int ors=res;
	for(int i=line+1;;++i){
		if(!vis[i]&&!isf[i]){vis[i]=true;break;}
		if(vis[i])continue;
		if(can)res=max(res,dfs(i,ors));
		else dfs(i,ors);
	}
	have[vf[isf[line]]]=false;
	return res;
}

int main(){
	freopen("complexity.in","r+",stdin);
	freopen("complexity.out","w+",stdout);
	int t;
	size_t SBF=sizeof bf;
	size_t SVF=sizeof vf;
	size_t SHV=sizeof have;
	size_t SVI=sizeof vis;
	scanf("%d",&T);
	while(T--){
		fc=ec=0;err=false;
		memset(bf,0,SBF);memset(ef,0,SBF);
		memset(vf,0,SVF);memset(have,0,SHV);
		memset(isf,0,SBF);memset(vis,0,SVI);
		scanf("%d %s",&L,ot);
		for(int i=1;i<=L;++i){
			scanf("%s",buf);
			if(buf[0]=='F'){
				++fc;
				scanf("%s",buf);
				vf[fc]=buf[0];
				scanf("%s",buf);
				if(buf[0]=='n')bf[fc]=-1;
				else{
					sscanf(buf,"%d",&t);
					bf[fc]=t;
				}
				scanf("%s",buf);
				if(buf[0]=='n')ef[fc]=-1;
				else{
					sscanf(buf,"%d",&t);
					ef[fc]=t;
				}
				isf[i]=fc;
			}else ++ec;
		}
		if(fc!=ec)err=true;
		if(err){
			puts(OUTPUT[2]);
			continue;
		}
		int kk=-1;
		for(int i=1;i<=L-1;++i)
			if(!vis[i])
				kk=max(kk,dfs(i,-1));
		if(err){
			puts(OUTPUT[2]);
			continue;
		}
		if(ot[2]=='1'){
			puts(OUTPUT[kk==-1]);
		}else{
			sscanf(ot+4,"%d",&t);
			puts(OUTPUT[kk==t]);
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

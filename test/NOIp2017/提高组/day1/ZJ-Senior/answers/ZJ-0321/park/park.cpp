#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
struct st{
	int k,z,v,pre;
}lu[201000];
struct sl{
	int v,p;
}r[101000];

int sum,i,j,T,n,m,k,ff,p[101000],M,h[101000],dis[101000],x,y,v,head,tail,d[101000],ans;
int bf[2002000];
void add(int x,int y,int v)
{
	sum++;
	lu[sum].k=x;
	lu[sum].z=y;
	lu[sum].v=v;
	lu[sum].pre=p[lu[sum].k];
	p[lu[sum].k]=sum;
}
int cmp(sl a,sl b)
{
	if (a.v==b.v) return a.p<b.p;
	return a.v<b.v;
}
main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		sum=0;
		scanf("%d%d%d%d",&n,&m,&k,&M);
		for (i=1; i<=n; i++) p[i]=h[i]=0;
		for (i=1; i<=n; i++) d[i]=1;
		for (i=1; i<=n; i++) dis[i]=23333333;
		dis[1]=0;
		for (i=1; i<=m; i++)
		{
			scanf("%d%d%d",&x,&y,&v);
			add(x,y,v);
		}
		head=0; tail=1;
		head++;
		bf[head]=1;
		h[1]=1;
		while (tail>=head)
		{
			ff=p[bf[head]];
			while (ff>0)
			{
				if (dis[lu[ff].z]>dis[lu[ff].k]+lu[ff].v)
				{
					dis[lu[ff].z]=dis[lu[ff].k]+lu[ff].v;
					if (h[lu[ff].z]==0) 
					{
						h[lu[ff].z]=1;
						tail++;
						bf[tail]=lu[ff].z;
					}
				}
				ff=lu[ff].pre;
			}
			h[bf[head]]=0;
			head++;
		}
		for (i=1; i<=n; i++) r[i].p=i,r[i].v=dis[i];
		ans=0;
		sort(r+1,r+1+n,cmp);
		d[1]=1;
		for (i=2; i<=n; i++)
		{
			ff=p[r[i].p];
			while (ff>0)
			{
				if (dis[lu[ff].k]+lu[ff].v==dis[lu[ff].z]) d[lu[ff].z]=d[lu[ff].z]+d[lu[ff].k];
				ff=lu[ff].pre;
			}
		}
		ans=d[n]%M;
		printf("%d\n",ans);
	}
}

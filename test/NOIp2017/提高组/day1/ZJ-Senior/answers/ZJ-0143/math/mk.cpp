#include<cstdlib>
#include<cstdio>
#include<ctime>
using namespace std;
const int P=3000;
int gcd(int a,int b) {
	if (!b) return a;
	return gcd(b,a%b);
}
int main() {
	srand((int) time(0));
	freopen("math.in","w",stdout);
	int x,y;
	x=rand()%P+2,y=rand()%P+2;
	while (gcd(x,y)>1) x=rand()%P+2,y=rand()%P+2;
	printf("%d %d\n",x,y);
}

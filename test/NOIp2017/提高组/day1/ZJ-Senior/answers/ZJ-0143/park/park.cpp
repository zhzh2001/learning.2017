#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=100100,M=55;
int i,j,k,n,m,K,P,ch,ff,S,T,Hn,Test;
bool g[M][N];
int f[M][N],d[N],dis[N],z[N],in[N];
struct cc { int x,y,v;} A[N<<1];
struct dd {
	int x,dis;
	bool operator < (const dd &n) const {
		return dis<n.dis;
	}
} H[1001000];
struct graph {
	int i,j,k,En,tm,dn,nm;
	int A[N],B[N],b[N],d[N],z[N],h[N],have[N];
	struct Edge { int s,n;} E[N<<1];
	void add(int x,int y) {
		E[++En].s=y;E[En].n=h[x];h[x]=En;
	}
	void clear() {
		En=tm=dn=nm=0;
		memset(h,0,sizeof h);
		memset(A,0,sizeof A);
		memset(z,0,sizeof z);
		memset(b,0,sizeof b);
		memset(have,0,sizeof have);
	}
	void Tj(int x) {
		A[x]=B[x]=++tm;
		z[x]=1;d[++dn]=x;
		for (int k=h[x];k;k=E[k].n) {
			if (!A[E[k].s]) {
				Tj(E[k].s);
				if (B[E[k].s]<B[x]) B[x]=B[E[k].s];
			}
			else if (z[E[k].s] && A[E[k].s]<B[x]) B[x]=A[E[k].s];
		}
		if (A[x]==B[x]) {
			nm++;
			int now=0;
			while (now!=x) {
				now=d[dn--];
				z[now]=0;
				b[now]=nm;
				have[nm]++;
			}
		}
	}
	void work() {
		for (int i=1;i<=n;i++) if (!A[i]) Tj(i);
	}
} G;
struct Graph {
	int i,j,k,En;
	int h[N];
	struct edge { int s,n,v;} E[N<<1];
	void add(int x,int y,int v) {
		E[++En].s=y;E[En].v=v;
		E[En].n=h[x];h[x]=En;
	}
	void clear() {
		En=0;
		memset(h,0,sizeof h);
	}
} Ga,Gb;
void R(int &x) {
	ff=x=0;ch=getchar();
	while (ch<'0' || '9'<ch) { if (ch=='-') ff=1;ch=getchar();}
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	if (ff) x=-x;
}
void up(int x) {
	while (x>1 && H[x]<H[x>>1]) swap(H[x],H[x>>1]),x>>=1;
}
void down(int x) {
	while ((x<<1)<=Hn) {
		int k=x<<1;
		if (k<Hn && H[k|1]<H[k]) k|=1;
		if (H[k]<H[x]) swap(H[x],H[k]),x=k;
		else break;
	}
}
void Dj(int S) {
	Hn=0;
	memset(dis,60,sizeof dis);
	memset(z,0,sizeof z);
	dis[S]=0;
	dd t;
	t.dis=0;t.x=S;
	H[++Hn]=t;
	while (Hn) {
		dd t=H[1];
		H[1]=H[Hn--];
		down(1);
		z[t.x]=1;
		for (int k=Ga.h[t.x];k;k=Ga.E[k].n) if (t.dis+Ga.E[k].v<dis[Ga.E[k].s]) {
			dis[Ga.E[k].s]=t.dis+Ga.E[k].v;
			dd tt;
			tt.x=Ga.E[k].s;
			tt.dis=dis[Ga.E[k].s];
			H[++Hn]=tt;
			up(Hn);
		}
		while (Hn && z[H[1].x]) {
			H[1]=H[Hn--];
			down(1);
		}
	}
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	R(Test);
	while (Test--) {
		G.clear();
		Ga.clear();
		Gb.clear();
		memset(g,0,sizeof g);
		memset(f,0,sizeof f);
		memset(in,0,sizeof in);
		R(n);R(m);R(K);R(P);
		for (i=1;i<=m;i++) {
			R(A[i].x);R(A[i].y);R(A[i].v);
			if (A[i].v==0) G.add(A[i].x,A[i].y);
		}
		G.work();
		S=G.b[1];T=G.b[n];
		n=G.nm;
		for (i=1;i<=m;i++) if (G.b[A[i].x]!=G.b[A[i].y]) {
			Ga.add(G.b[A[i].y],G.b[A[i].x],A[i].v);
			Gb.add(G.b[A[i].x],G.b[A[i].y],A[i].v);
		}
		Dj(T);
		for (i=1;i<=n;i++)
			for (k=Ga.h[i];k;k=Ga.E[k].n) if (dis[i]+Ga.E[k].v==dis[Ga.E[k].s]) in[Ga.E[k].s]++;
		i=0;j=1;d[1]=T;
		while (i<j) {
			i++;
			for (k=Ga.h[d[i]];k;k=Ga.E[k].n) if (dis[d[i]]+Ga.E[k].v==dis[Ga.E[k].s]) {
				in[Ga.E[k].s]--;
				if (in[Ga.E[k].s]==0) d[++j]=Ga.E[k].s;
			}
		}
		int dn=j;
		f[0][S]=1;g[0][S]=1;
		for (i=0;i<=K;i++)
			for (j=dn;j;j--) if (g[i][d[j]])
				for (k=Gb.h[d[j]];k;k=Gb.E[k].n) {
					int l=i-dis[d[j]]+Gb.E[k].v+dis[Gb.E[k].s];
					if (l<=K) {
						f[l][Gb.E[k].s]=(f[l][Gb.E[k].s]+f[i][d[j]])%P;
						g[l][Gb.E[k].s]=1;
					}
				}
		int Fg=0;
		for (i=1;i<=n;i++) if (G.have[i]>1) {
			for (j=0;j<=K;j++) if (g[j][i]) Fg=1;
		}
		if (Fg) puts("-1");
		else {
			int ans=0;
			for (i=0;i<=K;i++) ans=(ans+f[i][T])%P;
			printf("%d\n",ans);
		}
	}
}

#include<cstdio>
#include<cstring>
using namespace std;
const int N=110,D=26;
int i,j,k,n,m,T,ans1,ans2,top;
int z[D];
char s[N];
struct cc { int o,i,x,y;} A[N];
struct dd { int x,fg,ch;} stack[N];
int check() {
	memset(z,0,sizeof z);
	top=0;stack[0].fg=1;
	int ans=0;
	for (i=1;i<=n;i++) {
		if (A[i].o) {
			if (top<=0) return -1;
			z[stack[top].ch]=0;
			top--;
		}
		else {
			if (z[A[i].i]) return -1;
			z[A[i].i]=1;
			top++;
			stack[top].ch=A[i].i;
			if (A[i].x==-1) {
				if (A[i].y==-1) {
					stack[top].fg=stack[top-1].fg;
					stack[top].x=stack[top-1].x;
				}
				else {
					stack[top].fg=0;
					stack[top].x=stack[top-1].x;
				}
			}
			else {
				if (A[i].y==-1) {
					stack[top].fg=stack[top-1].fg;
					stack[top].x=stack[top-1].x+stack[top].fg;
				}
				else {
					if (A[i].x<=A[i].y) {
						stack[top].fg=stack[top-1].fg;
						stack[top].x=stack[top-1].x;
					}
					else {
						stack[top].fg=0;
						stack[top].x=stack[top-1].x;
					}
				}
			}
			if (stack[top].x>ans) ans=stack[top].x;
		}
	}
	if (top!=0) return -1;
	return ans;
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&n);
		scanf("%s",s);
		m=strlen(s);
		ans1=0;
		if (s[2]!='1') {
			for (i=4;i<m;i++) {
				char ch=s[i];
				if ('0'<=ch && ch<='9') ans1=ans1*10+ch-'0';
				else break;
			}
		}
		for (i=1;i<=n;i++) {
			scanf("%s",s);
			A[i].o=A[i].i=A[i].x=A[i].y=0;
			if (s[0]=='E') A[i].o=1;
			else {
				scanf("%s",s);
				A[i].i=s[0]-'a';
				scanf("%s",s);
				if (s[0]=='n') A[i].x=-1;
				else {
					m=strlen(s);
					for (j=0;j<m;j++) {
						char ch=s[j];
						if ('0'<=ch && ch<='9') A[i].x=A[i].x*10+ch-'0';
						else break;
					}
				}
				scanf("%s",s);
				if (s[0]=='n') A[i].y=-1;
				else {
					m=strlen(s);
					for (j=0;j<m;j++) {
						char ch=s[j];
						if ('0'<=ch && ch<='9') A[i].y=A[i].y*10+ch-'0';
						else break;
					}
				}
			}
		}
		int t=check();
		if (t==-1) puts("ERR");
		else {
			if (t==ans1) puts("Yes");
			else puts("No");
		}
	}
}

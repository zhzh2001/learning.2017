#include<bits/stdc++.h>
using namespace std;
int t,n,p1,p2,o,q,px,pn,l1,l2,x,y,l,f[200],pnmax;
char s1[1000];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d",&l);
		n=p1=p2=0;
		char ch=getchar();
		while (ch!='O') ch=getchar();
		ch=getchar();
		while (ch!=')')
		{
			if (ch=='n') n=1;
			if (ch=='1'&&n==0) p1=1;
			if (ch>='0'&&ch<='9'&&n==1) p2=p2*10+ch-48; 
			ch=getchar();
		}
		o=0,q=0,px=1,pn=pnmax=0;
		memset(f,0,sizeof(f));
		for (int i=1;i<=l;i++)
		{
			scanf("%s",s1);
			if (s1[0]=='F')
			{
				o++;
				scanf("%s",s1);
				if (f[s1[0]]==0) f[s1[0]]=1;
				else q=1;
				scanf("%s",s1);
				l1=x=0;
				while (s1[l1]>='0'&&s1[l1]<='9') x=x*10+s1[l1]-48,l1++;
				if (s1[l1]=='n') x=10000;
				scanf("%s",s1);
				l2=y=0;
				while (s1[l2]>='0'&&s1[l2]<='9') y=y*10+s1[l2]-48,l2++;
				if (s1[l2]=='n') y=10000;
				if (y-x+1>1000&&px) pn++;
				else if (y-x+1<=0) px=0;
			}
			if (s1[0]=='E')
			{
				o--;
				if (o==0) 
				{
					pnmax=max(pnmax,pn);
					px=1,pn=0;
					memset(f,0,sizeof(f));
				}
				if (o<0) q=1;
			}
		}
		if (o>0) q=1;
		if (q) printf("ERR\n");
		else 
		{
			if (pnmax>0) 
			{
				if (pnmax!=p2) printf("No\n");
				else printf("Yes\n");
			}
			else
			if (p1==1) printf("Yes\n");
			else printf("No\n");
		}
		
	}
	return 0;
}

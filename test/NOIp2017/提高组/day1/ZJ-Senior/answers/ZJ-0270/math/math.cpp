#include<bits/stdc++.h>
using namespace std;
long long x,y,p1,p2,ss;
void exgcd(long long a,long long b,long long &x,long long &y)
{
	if (b==0)
	{
		x=1,y=0;
		return;
	}
	exgcd(b,a%b,x,y);
	long long t=x;
	x=y;
	y=(1-a*y)/b;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	long long a,b;
	scanf("%lld%lld",&a,&b);
	exgcd(a,b,x,y);
	x=(x%b+b)%b;
	y=(1-x*a)/b;
	p1=-y-1;
	y=(y%a+a)%a;
	x=(1-y*b)/a;
	p2=-x-1;
	ss=p1*b+p2*a+1;
	printf("%lld\n",ss);
	return 0;
 } 

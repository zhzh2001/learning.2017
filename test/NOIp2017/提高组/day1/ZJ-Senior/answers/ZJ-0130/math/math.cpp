#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int x,y,f[10000010];
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&x,&y);
	if (x>y) swap(x,y);
	f[x]=1; f[y]=1;
	int xx=x*2,u=x-1;
	
	while (xx<=y)
	{
		f[xx]=1;
		if (!f[xx-1]) u=xx-1;
		xx+=x; 
	}
	if(!f[y-1]) u=y-1;
	int t=y,p=0;
	while (1>0)
	{
		t++;
		if (f[t-x] || f[t-y]) 
		    p++,f[t]=1;
		else 
		    p=0;
		if (p==x) break;
	}
	if (t-x!=x && t-x!=y)
	printf("%d\n",t-x);
	else printf("%d\n",u);
	return 0;
}

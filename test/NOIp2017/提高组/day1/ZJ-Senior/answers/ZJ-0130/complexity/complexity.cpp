#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
# define N 1000
using namespace std;
int n,xx,yy,cnt,st[110],a[110],b[110],c[110],g[110];
char s[100];
bool bl[100];
void read(int & x)
{
	x=0; int k=1; char ch=getchar();
	while (ch<'0' || ch>'9') {ch=getchar();}
	while ('0'<=ch && ch<='9') {x=x*10+ch-48; ch=getchar();}
	x=x*k;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T; read(T);
	while (T--)
	{
		char ch,kh;
		read(n);
		scanf("%s",s+1);
		int len=strlen(s+1); xx=0; yy=0;
        if (s[4]=='^') xx=1; else xx=0;
        if (xx)
        {
        	for (int i=5;i<=len;i++)
        	    if (s[i]!=')')
        	        yy=yy*10+s[i]-48;
        }
        //printf("%d %d",xx,yy);
        cnt=0;bool flag=0;
        memset(st,0,sizeof(st));
        memset(a,0,sizeof(a));
        memset(b,0,sizeof(b));
        memset(c,0,sizeof(c));
        memset(g,0,sizeof(g));
        memset(bl,0,sizeof(bl));
        ch=getchar();
        for (int i=1;i<=n;i++)
        {
            while (ch==' ' || ch=='\n')ch=getchar();
            kh=getchar();while (kh==' ')kh=getchar();
            if (ch=='E') {
			ch=kh;
			kh=getchar();while (kh==' ')kh=getchar();}
        	if (ch=='F')
        	{
        		cnt++;
        		st[cnt]=1;
                //while (ch==' ' || ch=='\n')ch=getchar();
        		ch=kh;
        		if (bl[ch-'a'])
        		{
        			printf("ERR\n"); flag=1;
        		}
        	    else
        	    {
        	    	bl[ch-'a']=1;
        	    	a[cnt]=ch-'a';
        	    }
        	    ch=getchar();
                while (ch==' ')ch=getchar();
        	    if (ch=='n')
        	    {
        	    	b[cnt]=N;
        	    	ch=getchar();
        	    }
        	    else
        	    {
        	    	while (ch==' ')ch=getchar();
        	    	while ('0'<=ch && ch<='9') {b[cnt]=b[cnt]*10+ch-48; ch=getchar();}
        	    }
  	            while (ch==' ')ch=getchar();
  	            if (ch=='n') c[cnt]=N,ch=getchar();
  	            else while ('0'<=ch && ch<='9') {c[cnt]=c[cnt]*10+ch-48; ch=getchar();}
				if (flag)
				{
					for (int j=i+1;j<=n;j++) scanf("%s",s);
	                break;
				}	    
        	    //printf("%d %d %d\n",a[cnt],b[cnt],c[cnt]);
			}
			else
			{
				if (cnt==0)
				{
        			printf("ERR\n"); flag=1;
					for (int j=i+1;j<=n;j++) scanf("%s",s);
	                break;
				}
				else
				{
					bl[a[cnt]]=0;
					int stan;
					if (b[cnt]==N || b[cnt]>c[cnt] || c[cnt]<N)
					{
						stan=0;
					}
					else
					{
					    stan=g[cnt]+1;
					}
					g[cnt-1]=max(g[cnt-1],stan);
				    a[cnt]=0; b[cnt]=0;c[cnt]=0;cnt--;
				}
			}
		}
		if (flag) continue;
        int stan=g[0];
        if (cnt) printf("ERR\n");
        else
        if (xx==0 && yy==0 && stan==0) printf("Yes\n");
        else if (xx==1 && yy==stan) printf("Yes\n");
        else printf("No\n");
	}
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
struct edge
{
	int v,next,w;
}e[100000];
int n,m,k,p,head[100000],cnt,MIN,a[100000],vis[100000],ans,tot;
bool flag;
void add_edge(int u,int v,int w)
{
	e[++cnt].v=v;e[cnt].w=w;e[cnt].next=head[u];head[u]=cnt;
}
void dfs(int u,int x)
{
	if (u==n)
	{
		MIN=min(x,MIN);
	    tot++;
	    a[tot]=x;
	}
    if (x>MIN+k) return;
		for (int i=head[u];i;i=e[i].next)
		{
			int v=e[i].v;
			vis[v]++;
			if (vis[v]>1000) {flag=1;return;}
			dfs(v,x+e[i].w);
			if (flag) return;
			vis[v]--;
		}

}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(e,0,sizeof(e));
		memset(a,0,sizeof(a));
		memset(vis,0,sizeof(vis));
		memset(head,0,sizeof(head));
		cnt=0;tot=0; MIN=1000000000;flag=0;vis[1]=1;
		for (int i=1;i<=m;i++)
		{
			int x,y,z; scanf("%d%d%d",&x,&y,&z); add_edge(x,y,z);
		}
		dfs(1,0);
		if (flag)
		{
			printf("-1\n");
		}
		else
		{
			ans=0;
			for (int i=1;i<=tot;i++) if (a[i]<=MIN+k) ans=(ans+1)%p;
			printf("%d\n",ans);
		}
	}
	return 0;
}

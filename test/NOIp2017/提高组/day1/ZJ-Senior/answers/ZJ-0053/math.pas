var
x,y,n,m,t:int64;
begin
assign(input,'math.in'); reset(input);
assign(output,'math.out'); rewrite(output);
  readln(n,m);
  if n>m then begin t:=n; n:=m; m:=t; end;
  x:=(n+1)*(n-2)+1;
  y:=(m-(n+2)+1)*(n-1);
  writeln(x+y);
close(input);
close(output);
end.
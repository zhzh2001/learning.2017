var T,i,sum:longint;
 n,m,k,p,tot,min:longint;
 vis:array[0..100005]of boolean;
 next,head,key,value:array[0..200005]of longint;
procedure adde(u,v,va:longint);
begin
  inc(tot);
  next[tot]:=head[u];
  head[u]:=tot;
  key[tot]:=v;
  value[tot]:=va;
end;
procedure bfs(u,k:longint);
var v,i:longint;
begin
  if k>min then exit;
  if (u=n) then begin if k<min then min:=k; exit; end;
  vis[u]:=true;
  i:=head[u];
  while i>0 do
    begin
      v:=key[i];
      if vis[v]=false then bfs(v,k+value[i]);
      i:=next[i];
    end;
  vis[u]:=false;
end;
procedure bfs1(u,k:longint);
var v,i:longint;
begin
  if k>min then exit;
  if (u=n) then if k<=min then begin sum:=sum+1;if sum>p then sum:=sum-p;end;
  i:=head[u];
  while i>0 do
    begin
      v:=key[i];
      bfs1(v,k+value[i]);
      i:=next[i];
    end;
end;
procedure bfs2(u,k:longint);
var v,i:longint;
begin
  if k>min then exit;
  if (u=n) then
    begin
      if k=min then
        begin
          sum:=sum+1;
          if sum>p then sum:=sum-p;
        end;
      exit;
    end;
  vis[u]:=true;
  i:=head[u];
  while i>0 do
    begin
      v:=key[i];
      if vis[v]=false then bfs2(v,k+value[i]);
      i:=next[i];
    end;
  vis[u]:=false;
end;
procedure make;
var  i,a,b,c:longint;
flag:boolean;
begin
  readln(n,m,k,p);
  tot:=0; fillchar(head,sizeof(head),0);
  flag:=true;
  for i:=1 to m do
    begin
      read(a,b,c);
      adde(a,b,c);
      if c<>0 then flag:=false;
    end;
  if flag=true then begin writeln(-1); exit; end;
  min:=10000000;
  fillchar(vis,sizeof(vis),false);
  bfs(1,0);
  sum:=0;
  if k=0 then
    begin
      fillchar(vis,sizeof(vis),false);
      bfs2(1,0);
      writeln(sum);
    end
  else
    begin
      min:=min+k;
      bfs1(1,0);
      writeln(sum);
    end;
end;
begin
assign(input,'park.in'); reset(input);
assign(output,'park.out'); rewrite(output);
  readln(T);
  for i:=1 to T do make;
close(input);
close(output);
end.

#include<cstdio>
#include<algorithm>
int T,a,FF=1,noo[10001];
using namespace std;
void ber() {
	FF=0;
	printf("ERR\n");
}
char que[1001],p[1001],pp[100][4][10001];
int dd() {
	char cc=p[4];
	int data=0,cn=4;
	while (cc>='0'&&cc<='9') {
		data*=10;
		data+=cc-'0';
		cc=p[++cn];
	}
	return data;
}
int dop(int a,int k) {
	char cc=pp[a][k][0];
	int data=0,cn=1;
	while (cc>='0'&&cc<='9') {
		data*=10;
		data+=cc-'0';
		cc=pp[a][k][++cn];
	}
	return data;
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d",&a);
		scanf("%s",p);
		FF=1;
		int tail=0,maxx=0,nm=0,q=0,i=0,now=0;
		if (p[2]=='n') nm=dd();
		else nm=0;
		getchar();
		for (int i=0; i<a; i++) {
			scanf("%s",pp[i][1]);
			if (pp[i][1][0]=='F') {
				for (int j=2; j<=4; j++)
					scanf("%s",pp[i][j]);
			}
		}
		for (int i=0;i<26;i++) noo[i]=0;
		while (i<a) {
			if (pp[i][1][0]=='F') {
				if ((pp[i][3][0]=='n'&&pp[i][4][0]!='n')||(pp[i][3][0]!='n'&&pp[i][4][0]!='n'&&dop(i,3)>dop(i,4))) {
					int cnt=1;
					i++;
					while (cnt>0) {
						if (pp[i][1][0]=='F') cnt++;
						else cnt--;
						i++;
					}
					i--;
				} else if ((pp[i][3][0]=='n'&&pp[i][4][0]=='n')||(pp[i][3][0]!='n'&&pp[i][4][0]!='n'&&dop(i,3)<=dop(i,4))) {
					if (!noo[pp[i][2][0]-'a']) noo[pp[i][2][0]-'a']=2;
					else {
						ber();
						break;
					}
					que[++tail]=pp[i][2][0];
				} else if ((pp[i][3][0]!='n')&&pp[i][4][0]=='n') {
					if (!noo[pp[i][2][0]-'a']) noo[pp[i][2][0]-'a']++;
					else {
						ber();
						break;
					}
					que[++tail]=pp[i][2][0];
					now++;
					maxx=max(now,maxx);
				}
			} else {
				if (tail<=0) {
					ber();
					break;
				}
				if (noo[que[tail]-'a']==1) now-=1;
				noo[que[tail]-'a']=0;
				
				que[tail--]=0;
				
			}
			i++;
		}
		if (FF) {
			if (tail!=0) ber();
			
			else if (nm==maxx) printf("YES\n");
			else printf("NO\n");
		}
	}

	return 0;
}

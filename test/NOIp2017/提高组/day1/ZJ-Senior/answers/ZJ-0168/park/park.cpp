#include<cstdio>
#include<algorithm>
#include<queue>
#include<cstring>
const int MAXN=1000001;
using namespace std;
queue<int> dlie;
int read() {
	char cc=getchar();
	int data=0,flag=1;
	while (cc!='-'&&cc<'0'||cc>'9') {
		cc=getchar();
	}
	if (cc=='-') {
		flag=-1;
		cc=getchar();
	}
	while (cc>='0'&&cc<='9') {
		data*=10;
		data+=cc-'0';
		cc=getchar();
	}
	return flag*data;
}
int head[MAXN],dis[MAXN],tot=0,in_queue[MAXN],S,ans[MAXN],p,k,T,n,m;
struct bian {
	int to,next,s;
} edge[MAXN<<2];
int add(int a,int b,int c) {
	edge[++tot].next=head[a];
	edge[tot].s=c;
	edge[tot].to=b;
	head[a]=tot;
}
int spfa() {
	memset(dis,0X3f,sizeof(dis));
	in_queue[S]=1;
	ans[S]=1;
	dlie.push(S);
	dis[S]=0;
	while (!dlie.empty()) {
		int u=dlie.front();
		dlie.pop();
		in_queue[u]=0;
		for (int i=head[u]; i; i=edge[i].next) {
			int v=edge[i].to;
			if (dis[v]>dis[u]+edge[i].s) {
				dis[v]=dis[u]+edge[i].s;
				ans[v]=ans[u];
				if (!in_queue[v]) {
					dlie.push(v);
					in_queue[v]=1;
				}
			}
			else if (dis[v]==dis[u]+edge[i].s) {
				ans[v]=(ans[u]+ans[v])%p;
			}
		}
	}
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		tot=0;
		memset(in_queue,0,sizeof (in_queue));
		n=read();
		m=read();
		k=read();
		p=read();
		S=1;
		for (int i=1; i<=m; i++) {
			int a,b,c;
			a=read();
			b=read();
			c=read();
			add(a,b,c);
		}
		spfa();
		printf("%d\n",ans[n]);
	}
	return 0;
}


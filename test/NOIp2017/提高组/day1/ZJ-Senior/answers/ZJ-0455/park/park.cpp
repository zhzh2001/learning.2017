#include <bits/stdc++.h>
using namespace std;
struct side{
	int y,nxt,c;
}a[200005],b[200005];
int n,m,k,p,t,ans,fa[100005],fb[100005],la[100005],cnt,lb[100005];
queue<int>q;
bool h[100005];
void dfs(int u,int sum){
	if(u==n&&sum<=la[n]+k){
		ans++;
		ans%=p;
	}
	for(int i=fa[u];;i=a[i].nxt){
		if(i==0)break;
		int v=a[i].y;
		if(sum+a[i].c+lb[v]<=la[n]+k)
		dfs(v,sum+a[i].c);
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	for(int ii=1;ii<=t;ii++){
		ans=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=n;i++)fa[i]=fb[i]=0,la[i]=lb[i]=(1<<30);
		for(int i=1;i<=m;i++){
			int u,v,cc;
			scanf("%d%d%d",&u,&v,&cc);
			a[i].nxt=fa[u];
			a[i].y=v;
			a[i].c=cc;
			fa[u]=i;
			b[i].nxt=fb[v];
			b[i].y=u;
			b[i].c=cc;
			fb[v]=i;
		}
		lb[n]=0;
		h[n]=1;
		q.push(n);
		for(;;){
			if(q.empty())break;
			int hd=q.front();
			h[hd]=0;
			q.pop();
			for(int i=fb[hd];;i=b[i].nxt){
				int v=b[i].y;
				if(b[i].c+lb[hd]<lb[v]){
					lb[v]=b[i].c+lb[hd];
					if(h[v]==0){
						h[v]=1;
						q.push(v);
					}
				}
				if(b[i].nxt==0)break;
			}
		}
		la[1]=0;
		h[1]=1;
		q.push(1);
		for(;;){
			if(q.empty())break;
			int hd=q.front();
			h[hd]=0;
			q.pop();
			for(int i=fa[hd];;i=a[i].nxt){
				int v=a[i].y;
				if(a[i].c+la[hd]<la[v]){
					la[v]=a[i].c+la[hd];
					if(h[v]==0){
						h[v]=1;
						q.push(v);
					}
				}
				if(a[i].nxt==0)break;
			}
		}
		dfs(1,0);
		printf("%d\n",ans);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
#define per(i,x,y) for(int i=(x);i>=(y);--i)

typedef long long LL;
LL exgcd(LL a,LL b,LL&x,LL&y){
	if(!b)return x=1,y=0,a;
	LL d=exgcd(b,a%b,y,x);
	y-=a/b*x;
	return d;
}
/*void checkExgcd(){
	LL a,b,x,y,d;scanf("%lld%lld",&a,&b);
	d=exgcd(a,b,x,y);
	printf("%lld\n",d);
	printf("%lld %lld\n",x,y);
	assert(a*x+b*y==d);
}*/
bool check(LL a,LL b,LL x,LL y,LL c){
	x*=c;y*=c;
	LL t=y/a;
	y-=a*t;
	x+=b*t;
	//while(x<0)x+=b,y-=a;
	//while(y<0)x-=b,y+=a;
	return x>=0&&y>=0;
}
/*int main(){
	LL a,b;scanf("%lld%lld",&a,&b);
	if(a>b)std::swap(a,b);
	LL x,y,d=exgcd(a,b,x,y);
	if(d>1){puts("not huzhi");return 0;}
	while(x<0)x+=b,y-=a;
	while(x>0)x-=b,y+=a;
	LL k=-x*(a-1);
	printf("%lld",a*k-1);
	assert(!check(a,b,a*k-1));
	rep(i,1,1000)assert(check(a,b,a*k-1+i));
	return 0;
}*/
int main(){
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	LL a,b;scanf("%lld%lld",&a,&b);
	if(a>b)std::swap(a,b);
	LL x,y,d=exgcd(a,b,x,y);
	if(d>1){puts("not huzhi");return 0;}
	while(x<0)x+=b,y-=a;
	while(x>0)x-=b,y+=a;
	LL k=LL(1/(1.0*y/a+1.0*x/b));
	while(check(a,b,x,y,k))--k;
	printf("%lld\n",k);
}

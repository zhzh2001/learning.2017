#include<iostream>
#include<cstdio>
#include<cstring>
#define cls(a) memset(a,0,sizeof(a))
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
#define per(i,x,y) for(int i=(x);i>=(y);--i)

bool vaild(char c){return (c>='0'&&c<='9')||(c>='a'&&c<='z');}
char gc(){char c=getchar();while(!vaild(c))c=getchar();return c;}
inline void up(int&x,int y){if(y>x)x=y;}
int readint(){
	int ret=0;
	char c=gc();
	if(c=='n')return 10000;
	while(vaild(c))ret=ret*10+c-'0',c=getchar();
	return ret;
}
const int N=100+10;
struct Node{
	char var;
	int s,t;
	int length(){
		int l=t-s+1;
		if(l<=0)return -1;
		if(l>=1000)return 1;
		return 0;
	}
};
struct AST{
	int ps,tot,head[N],next[N],to[N],pa[N];
	Node node[N];
	void clear(){ps=tot=0;cls(head);}
	int newNode(char v,int s,int t){
		++ps;
		node[ps].var=v;
		node[ps].s=s;
		node[ps].t=t;
		return ps;
	}
	void ins(int a,int b){to[++tot]=b;next[tot]=head[a];head[a]=tot;pa[b]=a;}
	int calc(int k){
		int t=0;
		for(int p=head[k];p;p=next[p])up(t,calc(to[p]));
		int l=node[k].length();
		if(l==-1)return 0;
		if(l==0)return t;
		if(l==1)return t+1;
	}
}T;
struct Stack{
	int tot;
	char arr[N];
	void clear(){tot=0;}
	void push(char c){arr[++tot]=c;}
	void pop(){--tot;}
	bool check(char c){rep(i,1,tot)if(arr[i]==c)return 1;return 0;}
}stack;
int work(){
	int L;scanf("%d",&L);
	int w;char s[20];scanf("%s",s);
	if(s[2]=='1')w=0;
	else{
		w=0;
		int t=4;
		while(s[t]!=')')w=w*10+s[t]-'0',++t;
	}
	T.clear();
	stack.clear();
	int k=T.newNode('n',1,1);
	stack.push('n');
	bool flag=0;
	while(L--){
		char op;scanf("\n%c",&op);
		if(op=='F'){
			char var=gc();
			int x=readint();
			int y=readint();
			if(!flag){
				if(stack.check(var))flag=1;
				stack.push(var);
				int t=T.newNode(var,x,y);
				T.ins(k,t);
				k=t;
			}
		}else if(op=='E'){
			if(!flag){
				if(k==1)flag=1;
				k=T.pa[k];
				stack.pop();
			}
		}else{
			flag=1;
		}
	}
	if(flag)return -1;
	if(k!=1)return -1;
	int o=T.calc(1);
	return o==w;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		int k=work();
		if(k==-1)puts("ERR");
		if(k==0)puts("No");
		if(k==1)puts("Yes");
	}
	return 0;
}

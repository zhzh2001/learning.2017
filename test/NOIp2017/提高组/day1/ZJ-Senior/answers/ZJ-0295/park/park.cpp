#include<iostream>
#include<cstdio>
#include<queue>
#include<vector>
#include<cstring>
#define cls(a) memset(a,0,sizeof(a))
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
#define per(i,x,y) for(int i=(x);i>=(y);--i)

inline void dn(int&x,int y){if(y<x)x=y;}
inline void ck(int&x,int p){if(x>=p)x-=p;}
inline void ad(int&x,int y,int p){
	if(x==-1||y==-1){x=-1;return;}
	x+=y;ck(x,p);
}
const int N=100000+10,M=200000+10;
int n,m,K,P;
struct Heap{
	std::priority_queue<std::pair<int,int>,std::vector<std::pair<int,int> >,std::greater<std::pair<int,int> > > A,B;
	int size(){return A.size()-B.size();}
	void clear(){
		while(A.size())A.pop();
		while(B.size())B.pop();
	}
	void update(){
		while(B.size()&&A.top()==B.top())A.pop(),B.pop();
	}
	std::pair<int,int> top(){update();return A.top();}
	void pop(){update();A.pop();}
	void push(std::pair<int,int> x){A.push(x);}
	void del(std::pair<int,int> x){B.push(x);}
};
int dp[55][N];
bool has[55][N];
struct LinkMap{
	int tot,head[N],to[M],next[M];
	void clear(){tot=0;cls(head);}
	void ins(int a,int b){to[++tot]=b;next[tot]=head[a];head[a]=tot;}
}lm;
struct G2{
	int tot,head[N],to[M],next[M];
	void clear(){tot=0;cls(head);}
	void ins(int a,int b){to[++tot]=b;next[tot]=head[a];head[a]=tot;}
	int dfn[N],lowlink[N],clo,sta[N],top;
	int ps,newid[N];
	bool comb[N];
	void dfs(int k){
		lowlink[k]=dfn[k]=++clo;
		sta[++top]=k;
		for(int p=head[k];p;p=next[p]){
			if(dfn[to[p]]){
				dn(lowlink[k],dfn[to[p]]);
			}else{
				dfs(to[p]);
				dn(lowlink[k],lowlink[to[p]]);
			}
		}
		if(dfn[k]==lowlink[k]){
			newid[sta[top]]=++ps;
			comb[ps]=(sta[top]!=k);
			while(sta[top]!=k){
				newid[sta[--top]]=ps;
			}--top;
		}
	}
	int q[N],s,t,inq[N];
	void work(){
		ps=0;
		cls(dfn);clo=0;
		rep(i,1,n)if(!dfn[i])dfs(i);
		lm.clear();
		rep(k,1,n){
			for(int p=head[k];p;p=next[p])if(newid[k]!=newid[to[p]])lm.ins(newid[k],newid[to[p]]),++inq[newid[to[p]]];
		}
		s=1;t=0;
		rep(i,1,ps)if(inq[i]==0)q[++t]=i;
		while(s<=t){
			int k=q[s++];
			for(int p=lm.head[k];p;p=lm.next[p]){
				if(--inq[lm.to[p]]==0)q[++t]=lm.to[p];
			}
		}
	}
	int tmpdp[N];
	bool tmphas[N];
	void calc(int l){
		cls(tmpdp);
		cls(tmphas);
		rep(i,1,n){
			if(comb[newid[i]]){
				if(has[l][i])tmphas[newid[i]]=1;
			}else{
				tmpdp[newid[i]]=dp[l][i];
				tmphas[newid[i]]=has[l][i];
			}
		}
		rep(i,1,ps){
			int k=q[i];
			for(int p=lm.head[k];p;p=lm.next[p]){
				int t=lm.to[p];
				if(tmphas[k])tmphas[t]=1;
				ad(tmpdp[t],tmpdp[k],P);
			}
		}
		rep(k,1,ps)if(comb[k]&&tmphas[k])tmpdp[k]=-1;
		rep(i,1,n)dp[l][i]=tmpdp[newid[i]],has[l][i]=tmphas[newid[i]];
	}
}T;
struct Graph{
	int tot,head[N],to[M],next[M],len[M],dis[N];
	void clear(){tot=0,cls(head);}
	void ins(int a,int b,int c){to[++tot]=b;next[tot]=head[a];head[a]=tot;len[tot]=c;}
	Heap heap;
	void Dijkstra(){
		dis[1]=0;rep(i,2,n)dis[i]=1e9;
		heap.clear();
		rep(i,1,n)heap.push(std::make_pair(dis[i],i));
		while(heap.size()){
			int k=heap.top().second;
			heap.pop();
			for(int p=head[k];p;p=next[p])if(dis[to[p]]>dis[k]+len[p]){
				heap.del(std::make_pair(dis[to[p]],to[p]));
				dis[to[p]]=dis[k]+len[p];
				heap.push(std::make_pair(dis[to[p]],to[p]));
			}
		}
	}
	int calc(){
		cls(dp);
		cls(has);
		dp[0][1]=1;
		has[0][1]=1;
		rep(k,1,n){
			for(int p=head[k];p;p=next[p])if(len[p]==dis[to[p]]-dis[k])T.ins(k,to[p]);
		}
		T.work();
		rep(i,0,K){
			T.calc(i);
			rep(k,1,n){
				for(int p=head[k];p;p=next[p]){
					int t=len[p]-(dis[to[p]]-dis[k]);
					if(t&&i+t<=K){
						ad(dp[i+t][to[p]],dp[i][k],P);
						if(has[i][k])has[i+t][to[p]]=1;
					}
				}
			}
		}
		int ans=0;
		rep(i,0,K)ad(ans,dp[i][n],P);
		return ans;
	}
}G;
int work(){
	G.clear();T.clear();
	scanf("%d%d%d%d",&n,&m,&K,&P);
	rep(i,1,m){
		int a,b,c;scanf("%d%d%d",&a,&b,&c);
		G.ins(a,b,c);
	}
	G.Dijkstra();
	return G.calc();
}
int main(){
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		printf("%d\n",work());
	}
	return 0;
}

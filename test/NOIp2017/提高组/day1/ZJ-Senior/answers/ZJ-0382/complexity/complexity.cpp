#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=105,maxv=30;
int T,n,now,w,MAX,las,f[maxn];
bool vis[maxv],pd;
char c[maxv];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
void Work()
{
	n=Read(); nc(); nc(); char cc=nc(); now=MAX=las=0; pd=0;
	if (cc=='1') w=0; else w=Read();
	memset(vis,0,sizeof(vis));
	memset(f,0,sizeof(f));
	for (int i=1; i<=n; i++)
	{
		char ch=nc();
		while (ch!='E'&&ch!='F') ch=nc();
		if (ch=='E') vis[c[now]-96]=0,now--;
		 else
		  {
		  	int x=0,y=0;
		  	now++; f[now]=f[now-1]; ch=nc();
		  	while (ch<'a'||ch>'z') ch=nc(); c[now]=ch;
		  	if (vis[ch-96]) pd=1; else vis[ch-96]=1;
		  	ch=nc();
		  	while ((ch<'0'||ch>'9')&&ch!='n') ch=nc(); char K=ch;
			while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=nc();
		  	ch=nc();
		  	while ((ch<'0'||ch>'9')&&ch!='n') ch=nc(); char S=ch;
			while (ch>='0'&&ch<='9') y=y*10+ch-48,ch=nc();
		  	if (S=='n'&&K!='n')
		  	 if (!las) f[now]++;
			if (K=='n'&&S!='n')
			 if (!las) las=now;
			if (K!='n'&&S!='n')
			 if (x>y) if (!las) las=now;
		  }
		if (now<0) pd=1;
		if (now<las) las=0;
		if (f[now]>MAX) MAX=f[now];
	}
	if (now||pd) {printf("ERR\n"); return;}
	if (MAX==w) printf("Yes\n"); else printf("No\n");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=Read();
	while (T--) Work();
	return 0;
}

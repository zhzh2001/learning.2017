#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100005,maxm=200005;
int T,n,m,K,tt,tot,ans,MIN,num[maxn],dst[maxn],lnk[maxn],son[maxm],nxt[maxm],w[maxm];
bool pd;
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++;
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
void Add(int x,int y,int z) {w[++tot]=z; son[tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;}
void Dfs(int x,int fa,int sum)
{
	if (x==n) {MIN=min(MIN,sum); return;}
	for (int j=lnk[x]; j; j=nxt[j])
	 if (son[j]!=fa) Dfs(son[j],x,sum+w[j]);
}
void DFS(int x,int sum)
{
	if (pd) return;
	if (sum>MIN+K) return;
	if (x==n) if (sum>=MIN&&sum<=MIN+K) ans++;
	num[x]++; if (num[x]>10000) {pd=1; return;}
	for (int j=lnk[x]; j; j=nxt[j])
	 DFS(son[j],sum+w[j]);
}
void Work()
{
	n=Read(); m=Read(); K=Read(); tt=Read(); tot=ans=0; pd=0;
	memset(lnk,0,sizeof(lnk));
	memset(dst,63,sizeof(dst));
	for (int i=1,x,y,z; i<=m; i++) x=Read(),y=Read(),z=Read(),Add(x,y,z);
	MIN=1<<30; Dfs(1,0,0); DFS(1,0);
	if (pd) printf("-1\n"); else printf("%d\n",ans%tt);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=Read();
	while (T--) Work();
	return 0;
}

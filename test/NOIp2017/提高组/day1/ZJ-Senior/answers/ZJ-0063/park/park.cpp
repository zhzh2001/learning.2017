#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=100005,maxm=200005;
int T,n,m,k,p,tot,lnk[maxn],son[maxm],nxt[maxm],w[maxm],dis[maxn],que[maxm],sum[maxn];
bool vis[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void add(int x,int y,int z){
	son[++tot]=y; w[tot]=z; nxt[tot]=lnk[x]; lnk[x]=tot;
}
void spfa(){
	dis[1]=0,sum[1]=1; int hed=0,tal=1; que[tal]=1;
	while (hed!=tal) {
		hed=(hed+1)%maxm; vis[que[hed]]=0;
		for (int j=lnk[que[hed]];j;j=nxt[j])
		if (dis[que[hed]]+w[j]<=dis[son[j]]) {
			if (dis[que[hed]]+w[j]==dis[son[j]]) sum[son[j]]=(sum[son[j]]+1)%p;
			else dis[son[j]]=dis[que[hed]]+w[j],sum[son[j]]=sum[que[hed]];
			if (!vis[son[j]]) vis[son[j]]=1,tal=(tal+1)%maxm,que[tal]=son[j];
		}
	}
	printf("%d\n",sum[n]%p);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while (T--) {
		memset(lnk,0,sizeof(lnk));
		memset(dis,63,sizeof(dis));
		memset(vis,0,sizeof(vis));
		n=read(); m=read(); k=read(); p=read();
		for (int i=1;i<=m;i++) {
			int x=read(),y=read(),z=read();
			add(x,y,z);
		}
		if (k==0) spfa(); else printf("-1\n");
	}
	return 0;
}

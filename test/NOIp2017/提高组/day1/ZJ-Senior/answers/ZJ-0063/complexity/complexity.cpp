#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=105;
int T,n,top,num,sum,r,hed[maxn],tal[maxn];
bool check,pd;
char stack[maxn],s[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
inline int read1(char ch){
	int ret=0;
	while (ch>='0'&&ch<='9') ret=ret*10+ch-48,ch=getchar();
	return ret;
}
int read_(){
	char ch=getchar();
	while (ch!='n'&&(ch<'0'||ch>'9')) ch=getchar();
	if (ch=='n') return 1<<30; else return read1(ch);
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while (T--) {
		n=read();
		pd=check=sum=r=top=0;
		char ch=getchar(); while (ch!='(') ch=getchar();
		ch=getchar();
		if (ch=='n') check=1,num=read(); else num=read1(ch);
		for (int i=1;i<=n;i++) {
			//if (pd==1) {scanf("%s",s); continue;}
			char ch=getchar();
			while ((ch<'a'||ch>'z')&&ch!='E') ch=getchar();
			if (ch=='E') {
				if (pd==1) continue;
				bool check1=0;
				for (int j=1;j<=top;j++) if (stack[j]=='?') {check1=1; break;}
				if (tal[top]==1<<30&&hed[top]<tal[top]&&!check1) r--;
				top--; if (r<0||top<0) pd=1;
				continue;
			}
			bool check1=0;
			if (!pd) {
				for (int j=1;j<=top;j++) {
					if (ch==stack[j]) pd=1;
					if (stack[j]=='?') check1=1;
				}
				if (pd==0) stack[++top]=ch;	
			}
			hed[top]=read_(),tal[top]=read_();
			if (pd==1) continue;
			if (check1) continue;
			if (hed[top]<tal[top]) {if (tal[top]==1<<30) r++,sum=max(sum,r);}
			if (hed[top]>tal[top]) stack[top]='?';
		}
		if (pd==1||top>0) printf("ERR\n");
		else {
			if (check==1) {if (num!=sum) printf("No\n"); else printf("Yes\n");}
			else {if (num!=1||sum!=0) printf("No\n"); else printf("Yes\n");}
		}
	}
	return 0;
}

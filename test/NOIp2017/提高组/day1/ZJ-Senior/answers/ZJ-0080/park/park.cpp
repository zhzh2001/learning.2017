#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
#define N 100010
using namespace std;
struct node
{
	int x,next,w;
}data[N*2];
int g[N],tot;
int v[N],q[N],d[N];
int t;
int n,m,k,pp;
int ans;

inline int read()
{
	char c=getchar();int f=1,x=0;
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

void addedge(int x,int y,int k)
{
	tot++;
	data[tot].x=y;
	data[tot].w=k;
	data[tot].next=g[x];
	g[x]=tot;
}

void spfa()
{
	memset(v,0,sizeof(v));
	for (int i=1;i<=n;i++) d[i]=inf;
	d[1]=0;q[1]=1;v[1]=1;
	int head=0,tial=1;
	while (head!=tial)
	{
		head++;
		int x=q[head];
		for (int p=g[x];p!=-1;p=data[p].next)
		{
			int son=data[p].x;
			if (d[son]>d[x]+data[p].w)
			{
				d[son]=d[x]+data[p].w;
				if (v[son]==0)
				{
					q[++tial]=son;
					v[son]=1;
				}
			}
		}
	}
}

void dfs(int x,int s)
{
	if (x==n) ans=(ans+1)%pp;
	v[x]=1;
	for (int p=g[x];p!=-1;p=data[p].next)
	{
		int son=data[p].x;
		if (v[son]==1) continue;
		if (s+data[p].w<d[n]+k) dfs(son,s+data[p].w);
		v[son]=0;
	}
}

void work()
{
	n=read();m=read();k=read();pp=read();
	tot=0;ans=0;
	memset(g,255,sizeof(g));
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read(),k=read();
		addedge(x,y,k);
		addedge(y,x,k);
	}
	spfa();
	memset(v,0,sizeof(v));
	dfs(1,0);
	if (ans) printf("%d\n",ans%pp);
	else printf("-1\n");
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=read();
	while (t--) work();
	return 0;
}

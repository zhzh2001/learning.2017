#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
#define L 110
using namespace std;
int l,t,len,flag,num,err,cant;
int num_f,num_e;
char x[L];
int tot;
int tmp,maxn;
char time[20];

void getnum(int x)
{
	for (int i=x;i<=len-1;i++)
		num=num*10+time[i]-'0';
}

void prepare()
{
	flag=0;num=0;//flag=1表示为常数，num表示n^num 
	scanf("%d",&l);
	scanf("%s",time+1);
	len=strlen(time+1);
	for (int i=1;i<=len;i++)
	{
		if (time[i]=='1')
		{
			flag=1;
			break;
		}
		if (time[i]=='n')
		{
			getnum(i+2);
			break;
		}
	}
	num_f=0;num_e=0;
	err=0;tot=0;
	tmp=0;maxn=0;
	cant=0;
}

int judge()
{
	if (tot==1) return 0;
//	sort(x+1,x+tot+1);
//	for (int i=1;i<=tot;i++) printf("%d\n",x[i]);
	for (int i=2;i<=tot;i++)
		if (x[i]==x[i-1]) return 1;
	return 0;
}

void query1()
{
	num_f++;
	scanf("%s",x+1+tot);
	tot++;
	if (judge()) err=1;
	char fi[10],en[10];
	scanf("%s",fi+1);
	scanf("%s",en+1);
//	printf("%c\n%c\n",fi[1],en[1]);
	if (cant!=0) return;
	if (fi[1]=='n')
	{
		if (en[1]=='n') return;
		else
		{
			cant=num_f;
			return;
		}
	}
	if (en[1]=='n')
	{
		tmp=num_f-num_e,maxn=max(tmp,maxn);
		return;
	}
	int tmp1=0,tmp2=0;
	int lel=strlen(fi+1);
	for (int i=1;i<=lel;i++)
	{
		tmp1=tmp1*10+fi[i]-'0';
	}
	lel=strlen(en+1);
	for (int i=1;i<=lel;i++)
	{
		tmp2=tmp2*10+en[i]-'0';
	}
	if (tmp1>tmp2)
	{
		err=1;
		cant=num_f;
		return;
	}
}

void query2()
{
	num_e++;tot--;
	if (num_e==cant) cant=0;
	if (num_e>num_f) err=1;
}

void work()
{
//	printf("%d\n",t);
	prepare();
//	printf("%s\n",time+1);
	while (l--)
	{
		char line[10];
		scanf("%s",line+1);
		if (line[1]=='F') query1();
		if (line[1]=='E') query2();
	}
	if (err!=0||num_f!=num_e) printf("ERR\n");
	else if (flag)
	{
		if (maxn==0) printf("YES\n");
		else printf("NO\n");
	}
	else
	{
		if (maxn==num) printf("YES\n");
		else printf("NO\n");
	}
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--) work();
	return 0;
}

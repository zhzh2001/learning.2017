#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
#define N 10010
using namespace std;
ll a,b,ans;
int f[N*100];

inline ll read()
{
	char c=getchar();ll f=1ll,x=0ll;
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();b=read();
	memset(f,0,sizeof(f));
//	printf("%lld %lld\n",a,b);
	if (a>b) swap(a,b);
	ans=a-1;
	for (int i=a;i<=b-1;i++)
		if (i%a!=0) ans=i;
	ll maxn=a*b;
	ll r1=maxn/a,r2=maxn/b;
//	printf("%lld %lld\n",r1,r2);
	f[(int)maxn]=1;
	for (int i=0;i<=r1;i++)
		for (int j=0;j<=r2;j++)
		f[a*i+b*j]=1;
	for (int i=b+1;i<=maxn;i++)
		if (f[i]==0) ans=i;
	printf("%lld\n",ans);
	return 0;
}

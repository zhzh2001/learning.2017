#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

char c[120],d[120],e[120],f[120],v[120],tmp1[5],tmp2[5];
int h,i,j,k,n,o,q,r,s,t,w;
bool b;

inline bool cmp()
{
	int t1=0,t2=0,l1=strlen(tmp1),l2=strlen(tmp2);
	for (int i=0;i<l1;i++)
		t1=t1*10+tmp1[i]-48;
	for (int i=0;i<l2;i++)
		t2=t2*10+tmp2[i]-48;
	if (t1>t2)
		return true;
	else
		return false;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&o);
	for (h=1;h<=o;h++)
	{
		memset(c,0,sizeof(c));
		scanf("%d",&n);
		scanf("%s",&c);
		s=0,t=0;
		if (c[2]!='1')
			for (j=4;(c[j]>47) && (c[j]<58);j++)
				t=t*10+c[j]-48;
		memset(c,0,sizeof(c));
		for (i=1;i<=n;i++)
		{
			getchar(),c[i]=getchar();
			if (c[i]=='F')
			{
				getchar(),d[i]=getchar();
				scanf("%s%s",&tmp1,&tmp2);
				if ((tmp1[0]!='n') && (tmp2[0]=='n'))
					v[i]=-1;
				else
					if (((tmp1[0]=='n') && (tmp2[0]!='n')) || (cmp()))
						v[i]=0;
					else
						v[i]=1;
			}
		}
		b=true,w=0;
		for (i=1;i<=n;i++)
		{
			if (c[i]=='F')
				w++;
			else
				w--;
			if (w<0)
				b=false;
		}
		if ((! b) || (w))
		{
			puts("ERR");
			continue;
		}
		q=0;
		for (i=1;(b) && (i<=n);i++)
		{
			if (c[i]=='F')
				q++,e[q]=d[i],f[q]=v[i];
			else
				q--;
			for (j=1;j<=q;j++)
				for (k=1;k<=q;k++)
					if ((j!=k) && (e[j]==e[k]))
						b=false;
			r=0;
			for (j=1;j<=q;j++)
				if (f[j]==-1)
					r++;
			for (j=1;j<=q;j++)
				if (f[j]==0)
					r=0;
			s=max(s,r);
		}
		if (! b)
			puts("ERR");
		else
			if (s==t)
				puts("Yes");
			else
				puts("No");
	}
	return 0;
}

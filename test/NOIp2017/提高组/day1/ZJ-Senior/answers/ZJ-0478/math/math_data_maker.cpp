#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	freopen("math.in","w",stdout);
	srand((int) time(0));
	for (;;)
	{
		int x=rand()%999+2;
		int y=rand()%999+2;
		if (__gcd(x,y)==1)
		{
			printf("%d %d\n",x,y);
			return 0;
		}
	}
}

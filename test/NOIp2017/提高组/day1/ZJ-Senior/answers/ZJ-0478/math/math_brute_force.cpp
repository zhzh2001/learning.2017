#include <cstdio>
#include <algorithm>

using namespace std;

int f[1200000];
int i,s,x,y;

int main()
{
	freopen("math.in","r",stdin);
	freopen("math_brute_force.out","w",stdout);
	scanf("%d%d",&x,&y);
	f[0]=1;
	for (i=1;i<=x*y;i++)
	{
		if (i>=x)
			f[i]=f[i]|f[i-x];
		if (i>=y)
			f[i]=f[i]|f[i-y];
		if (! f[i])
			s=max(s,i);
	}
	printf("%d",s);
	return 0;
}


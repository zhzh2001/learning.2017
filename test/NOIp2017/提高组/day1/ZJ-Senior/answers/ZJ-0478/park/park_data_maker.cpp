#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

int x[20],y[20],z[20];
int n=8,m=15,T=10;

inline bool dfs(int u,int v)
{
	if (v>=n)
		return false;
	if (u==n)
		return true;
	for (int i=1;i<=m;i++)
		if ((x[i]==u) && (dfs(y[i],v+1)))
			return true;
	return false;
}

int main()
{
	freopen("park.in","w",stdout);
	srand((int) time(0));
	printf("%d\n",T);
	for (int h=1;h<=T;h++)
		for (;;)
		{
			for (int i=1;i<=m;i++)
			{
				x[i]=rand()%n+1,y[i]=rand()%n+1,z[i]=rand()%10;
				while (x[i]==y[i]) x[i]=rand()%n+1,y[i]=rand()%n+1;
			}
			if (dfs(1,0))
			{
				printf("%d %d %d %d\n",n,m,rand()%10,rand()%10+1);
				for (int i=1;i<=m;i++) printf("%d %d %d\n",x[i],y[i],z[i]);
				break;
			}
		}
}

#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

struct element
{
	int x,y,z;
	
	inline bool operator < (const element t) const
	{
		if (x!=t.x)
			return x<t.x;
		else
			if (y!=t.y)
				return y<t.y;
			else
				return z<t.z;
	}
};

element e[200000];
int edge[500000],next[500000],dist[500000],deg[200000],first[200000];
int f[200000][60],f1[200000],f2[200000],g[200000];
bool b[200000];
int h,i,j,k,l,m,n,p,s,t,w,x,y,z,head,tail,sum_edge;

inline int fastscanf()
{
	int t=0;
	char c=getchar();
	while (! ((c>47) && (c<58)))
		c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t;
}

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline void input()
{
	memset(first,0,sizeof(first));
	memset(deg,0,sizeof(deg));
	sum_edge=0;
	n=fastscanf(),m=fastscanf();
	k=fastscanf(),p=fastscanf();
	for (i=1;i<=m;i++)
	{
		x=fastscanf(),y=fastscanf(),z=fastscanf();
		addedge(x,y,z),addedge(y,x,z);
		if (! z)
			deg[y]++;
	}
	return;
}

inline void topology_sort()
{
	tail=0;
	for (i=1;i<=n;i++)
		if (! deg[i])
			tail++,g[tail]=i;
	for (head=1;head<=tail;head++)
		for (i=first[g[head]];i!=0;i=next[i])
			if ((i&1) && (! dist[i]))
			{
				deg[edge[i]]--;
				if (! deg[edge[i]])
					tail++,g[tail]=edge[i];
			}
	memset(f1,12,sizeof(f1));
	memset(f2,12,sizeof(f2));
	return;
}

inline void dijkstra(int x,int y,int *f)
{
	memset(b,false,sizeof(b));
	f[x]=0;
	for (i=1;i<=n;i++)
	{
		w=0;
		for (j=1;j<=n;j++)
			if ((! b[j]) && ((! w) || (f[j]<f[w])))
				w=j;
		b[w]=true;
		for (j=first[w];j!=0;j=next[j])
			if (((j&1)==y) && (! b[edge[j]]) && (f[w]+dist[j]<f[edge[j]]))
				f[edge[j]]=f[w]+dist[j];
	}
	return;
}

inline bool spj()
{
	memset(b,false,sizeof(b));
	for (i=1;i<=tail;i++)
		b[g[i]]=true;
	for (i=1;i<=n;i++)
		if ((! b[i]) && (f1[i]+f2[i]<=f1[n]+k))
		{
			printf("-1\n");
			return true;
		}
	return false;
}

inline void reorder()
{
	for (i=1;i<=tail;i++)
		e[g[i]].y=i-tail-1;
	for (i=1;i<=n;i++)
		e[i].x=f1[i],e[i].z=i;
	sort(e+1,e+n+1);
	return;
}

inline void DP()
{
	for (i=1;i<=n;i++) printf("%d ",f1[i]); puts("");
	for (i=1;i<=n;i++) printf("%d ",f2[i]); puts("");
	for (i=1;i<=n;i++) printf("%d ",e[i].z); puts("");
	memset(f,0,sizeof(f));
	f[1][0]=1;
	for (i=0;i<=k;i++)
		for (j=1;j<=n;j++)
			for (l=first[e[j].z];l!=0;l=next[l])
				if ((l&1) && (f1[e[j].z]+i+dist[l]-f1[edge[l]]<=k))
					f[edge[l]][f1[e[j].z]+i+dist[l]-f1[edge[l]]]=(f[edge[l]][f1[e[j].z]+i+dist[l]-f1[edge[l]]]+f[e[j].z][i])%p;
	return;
}

inline void output()
{
	s=0;
	for (i=0;i<=k;i++)
		s=(s+f[n][i])%p;
	printf("%d\n",s);
	return;
}

int main()
{
	t=fastscanf();
	for (h=1;h<=t;h++)
	{
//		clear();
		input();
		topology_sort();
		dijkstra(1,1,f1);
		dijkstra(n,0,f2);
		if (spj())
			continue;
		reorder();
		DP();
		output();
	}
	return 0;
}

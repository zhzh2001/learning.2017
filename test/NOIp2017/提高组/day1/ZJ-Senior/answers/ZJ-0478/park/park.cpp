#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int edge[500000],next[500000],dist[500000],deg[200000],first[200000];
int f[200000][60],f1[200000],f2[200000],g[200000];
bool b[200000];
int h,i,j,k,l,m,n,p,s,t,w,x,y,z,head,tail,sum_edge;

inline int fastscanf()
{
	int t=0;
	char c=getchar();
	while (! ((c>47) && (c<58)))
		c=getchar();
	while ((c>47) && (c<58))
		t=t*10+c-48,c=getchar();
	return t;
}

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline void input()
{
	memset(first,0,sizeof(first));
	memset(deg,0,sizeof(deg));
	sum_edge=0;
	n=fastscanf(),m=fastscanf();
	k=fastscanf(),p=fastscanf();
	for (i=1;i<=m;i++)
	{
		x=fastscanf(),y=fastscanf(),z=fastscanf();
		addedge(x,y,z),addedge(y,x,z);
		if (! z)
			deg[y]++;
	}
	return;
}

inline void topology_sort()
{
	tail=0;
	for (i=1;i<=n;i++)
		if (! deg[i])
			tail++,g[tail]=i;
	for (head=1;head<=tail;head++)
		for (i=first[g[head]];i!=0;i=next[i])
			if ((i&1) && (! dist[i]))
			{
				deg[edge[i]]--;
				if (! deg[edge[i]])
					tail++,g[tail]=edge[i];
			}
	memset(f1,12,sizeof(f1));
	memset(f2,12,sizeof(f2));
	return;
}

struct heapnode
{
	int num,ord;
	
	inline bool operator < (const heapnode t) const
	{
		return num<t.num;
	}
};

heapnode heap[200000];
int toheap[200000];
int sum_heap;

inline void heap_swap(int node1,int node2)
{
	swap(toheap[heap[node1].ord],toheap[heap[node2].ord]);
	swap(heap[node1],heap[node2]);
	return;
}

inline void heap_up(int node)
{
	while ((node>1) && (heap[node]<heap[node>>1]))
	{
		heap_swap(node,node>>1);
		node=node>>1;
	}
	return;
}

inline void heap_down(int node)
{
	for (;;)
	{
		bool left=false,right=false;
		if (((node<<1)<=sum_heap) && (heap[node<<1]<heap[node]))
			left=true;
		if (((node<<1|1)<=sum_heap) && (heap[node<<1|1]<heap[node]))
			right=true;
		if ((left) && (right))
			if (heap[node<<1]<heap[node<<1|1])
				right=false;
			else
				left=false;
		if ((! left) && (! right))
			return;
		if (left)
		{
			heap_swap(node,node<<1);
			node=node<<1;
		}
		if (right)
		{
			heap_swap(node,node<<1|1);
			node=node<<1|1;
		}
	}
}

inline void heap_in(int num,int ord)
{
	sum_heap++;
	heap[sum_heap].num=num;
	heap[sum_heap].ord=ord;
	toheap[heap[sum_heap].ord]=sum_heap;
	heap_up(sum_heap);
	return;
}

inline void heap_pushtop()
{
	heap_swap(1,sum_heap);
	sum_heap--;
	heap_down(1);
	return;
}

inline void heap_insert(int num,int ord)
{
	heap[toheap[ord]].num=num;
	heap_up(toheap[ord]);
	heap_down(toheap[ord]);
	return;
}

inline void dijkstra(int x,int y,int *f)
{
	memset(b,false,sizeof(b));
	f[x]=0;
	for (i=1;i<=n;i++)
		heap_in(f[i],i);
	for (i=1;i<=n;i++)
	{
		w=heap[1].ord;
		heap_pushtop();
		b[w]=true;
		for (j=first[w];j!=0;j=next[j])
			if (((j&1)==y) && (! b[edge[j]]) && (f[w]+dist[j]<f[edge[j]]))
			{
				f[edge[j]]=f[w]+dist[j];
				heap_insert(f[edge[j]],edge[j]);
			}
	}
	return;
}

inline bool spj()
{
	memset(b,false,sizeof(b));
	for (i=1;i<=tail;i++)
		b[g[i]]=true;
	for (i=1;i<=n;i++)
		if ((! b[i]) && (f1[i]+f2[i]<=f1[n]+k))
		{
			printf("-1\n");
			return true;
		}
	return false;
}

struct element
{
	int x,y,z;
	
	inline bool operator < (const element t) const
	{
		if (x!=t.x)
			return x<t.x;
		else
			if (y!=t.y)
				return y<t.y;
			else
				return z<t.z;
	}
};

element e[200000];

inline void reorder()
{
	for (i=1;i<=tail;i++)
		e[g[i]].y=i-tail-1;
	for (i=1;i<=n;i++)
		e[i].x=f1[i],e[i].z=i;
	sort(e+1,e+n+1);
	for (i=1;i<=n;i++)
	{
		while ((first[i]) && (! (first[i]&1)))
			first[i]=next[first[i]];
		for (j=first[i];j!=0;j=next[j])
			while ((next[j]) && (! (next[j]&1)))
				next[j]=next[next[j]];
	}
	return;
}

inline void DP()
{
	memset(f,0,sizeof(f));
	f[1][0]=1;
	for (i=0;i<=k;i++)
		for (j=1;j<=n;j++)
			if (f1[e[j].z]+f2[e[j].z]-f1[n]+i<=k)
			{
				w=e[j].z;
				while ((first[w]) && (f1[w]+i+dist[first[w]]-f1[edge[first[w]]]>k))
					first[w]=next[first[w]];
				for (l=first[w];l!=0;l=next[l])
				{
					f[edge[l]][f1[e[j].z]+i+dist[l]-f1[edge[l]]]=(f[edge[l]][f1[e[j].z]+i+dist[l]-f1[edge[l]]]+f[e[j].z][i])%p;
					while ((next[l]) && (f1[w]+i+dist[next[l]]-f1[edge[next[l]]]>k))
						next[l]=next[next[l]];
				}
			}
/*				for (l=first[e[j].z];l!=0;l=next[l])
					if (f1[e[j].z]+i+dist[l]-f1[edge[l]]<=k)
						f[edge[l]][f1[e[j].z]+i+dist[l]-f1[edge[l]]]=(f[edge[l]][f1[e[j].z]+i+dist[l]-f1[edge[l]]]+f[e[j].z][i])%p;*/
	return;
}

inline void output()
{
	s=0;
	for (i=0;i<=k;i++)
		s=(s+f[n][i])%p;
	printf("%d\n",s);
	return;
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=fastscanf();
	for (h=1;h<=t;h++)
	{
		input();
		topology_sort();
		dijkstra(1,1,f1);
		dijkstra(n,0,f2);
		if (spj())
			continue;
		reorder();
		DP();
		output();
	}
	return 0;
}

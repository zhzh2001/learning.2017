#include <cstdio>
#include <cstring>

int edge[300000],next[300000],dist[300000],first[200000];
int h,i,k,m,n,p,t,x,y,z,l,r,mid,sum_edge;
bool b;

inline void addedge(int x,int y,int z)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline int dfs(int x,int y,int z)
{
	if (z>100)
		b=true;
	if (b)
		return -1;
	if (y<0)
		return 0;
	int s=0;
	if (x==n)
		s=1;
	for (int i=first[x];i!=0;i=next[i])
		s=s+dfs(edge[i],y-dist[i],z+1);
	return s;
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park_brute_force.out","w",stdout);
	scanf("%d",&t);
	for (h=1;h<=t;h++)
	{
		memset(first,0,sizeof(first));
		sum_edge=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			addedge(x,y,z);
		}
		b=false;
		l=0,r=50;
		while (l<r)
		{
			mid=(l+r)/2;
			if (dfs(1,mid,0))
				r=mid;
			else
				l=mid+1;
		}
		printf("%d\n",dfs(1,l+k,0)%p);
	}
	return 0;
}

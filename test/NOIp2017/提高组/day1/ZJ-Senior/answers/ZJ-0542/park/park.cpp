#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn = 100000, maxm = 200000, maxk = 50 + 1;

int n, m, k, mod;

inline void AddTo(int &a, int b) {
  a += b;
  if (a >= mod) a -= mod;
}

int cnt1, cnt2, cnt3;
int head1[maxn], head2[maxn], head3[maxn * maxk];
int to1[maxm], to2[maxm], to3[maxm * maxk];
int nxt1[maxm], nxt2[maxm], nxt3[maxm * maxk];
int weight1[maxm], weight2[maxm];

void AddEdge1(int u, int v, int w) {
  to1[cnt1] = v;
  nxt1[cnt1] = head1[u];
  weight1[cnt1] = w;
  head1[u] = cnt1++;
}

void AddEdge2(int u, int v, int w) {
  to2[cnt2] = v;
  nxt2[cnt2] = head2[u];
  weight2[cnt2] = w;
  head2[u] = cnt2++;
}

void AddEdge3(int u, int v) {
  to3[cnt3] = v;
  nxt3[cnt3] = head3[u];
  head3[u] = cnt3++;
}

struct Node {
  int u, d;
  bool operator < (const Node &b) const {
    return d > b.d;
  }
} heap[maxm + 1];

int top;
int dist1[maxn], dist2[maxn];

void Dijkstra1(void) {
  memset(dist1, 0x3f, n * sizeof(int));
  dist1[0] = 0;
  heap[0] = (Node){0, 0};
  top = 1;
  while (top) {
    int u = heap[0].u, d = heap[0].d;
    pop_heap(heap, heap + (top--));
    if (d != dist1[u]) continue;
    for (int e = head1[u]; e != -1; e = nxt1[e]) {
      int v = to1[e], w = weight1[e];
      if (dist1[u] + w < dist1[v]) {
        dist1[v] = dist1[u] + w;
        heap[top++] = (Node){v, dist1[v]};
        push_heap(heap, heap + top);
      }
    }
  }
}

void Dijkstra2(void) {
  memset(dist2, 0x3f, n * sizeof(int));
  dist2[n - 1] = 0;
  heap[0] = (Node){n - 1, 0};
  top = 1;
  while (top) {
    int u = heap[0].u, d = heap[0].d;
    pop_heap(heap, heap + (top--));
    if (d != dist2[u]) continue;
    for (int e = head2[u]; e != -1; e = nxt2[e]) {
      int v = to2[e], w = weight2[e];
      if (dist2[u] + w < dist2[v]) {
        dist2[v] = dist2[u] + w;
        heap[top++] = (Node){v, dist2[v]};
        push_heap(heap, heap + top);
      }
    }
  }
}

bool loop;
int dp[maxn * maxk];

int Dp(int u) {
  if (dp[u] == -2) {
    loop = true;
    return 0;
  }
  if (dp[u] != -1) {
    return dp[u];
  }
  dp[u] = -2;
  int sum = u == 0;

  for (int e = head3[u]; e != -1; e = nxt3[e]) {
    int v = to3[e];
    AddTo(sum, Dp(v));
    if (loop) {
      return 0;
    }
  }
  return dp[u] = sum;
}

void Solve(void) {
  scanf("%d%d%d%d", &n, &m, &k, &mod);
  ++k;

  cnt1 = cnt2 = cnt3 = 0;
  memset(head1, -1, n * sizeof(int));
  memset(head2, -1, n * sizeof(int));
  memset(head3, -1, n * k * sizeof(int));

  for (int i = 0; i < m; ++i) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    AddEdge1(u - 1, v - 1, w);
    AddEdge2(v - 1, u - 1, w);
  }
  Dijkstra1();
  Dijkstra2();

  for (int u = 0; u < n; ++u) {
    for (int e = head1[u]; e != -1; e = nxt1[e]) {
      int v = to1[e], w = weight1[e];
      for (int j1 = 0, j2 = dist1[u] + w - dist1[v]; j2 < k; ++j1, ++j2) {
        int a = u * k + j1, b = v * k + j2;
        AddEdge3(b, a);
      }
    }
  }

  int lim = dist1[n - 1] + k;
  for (int u = 0; u < n; ++u) {
    if (dist1[u] + dist2[u] >= lim) {
      memset(dp + u * k, 0, k * sizeof(int));
    } else {
      memset(dp + u * k, -1, k * sizeof(int));
    }
  }

  int ans = 0;
  loop = false;
  for (int j = 0; j < k; ++j) {
    AddTo(ans, Dp((n - 1) * k + j));
    if (loop) {
      break;
    }
  }
  printf("%d\n", loop ? -1 : ans);
}

int main(void) {
  freopen("park.in", "r", stdin);
  freopen("park.out", "w", stdout);
  int t;
  scanf("%d", &t);
  while (t--) {
    Solve();
  }
  return 0;
}

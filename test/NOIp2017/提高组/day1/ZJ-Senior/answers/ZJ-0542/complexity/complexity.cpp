#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

int ReadComp(void) {
  while (getchar() != '(') ;
  char type = getchar();
  if (type == '1') {
    getchar();
    return 0;
  } else {
    getchar();
    int w;
    scanf("%d", &w);
    getchar();
    return w;
  }
}

struct For {
  char var;
  int comp;
} st[1000];

int top;
bool use[26];

int Ana(char *a, char *b) {
  if (a[0] == 'n') {
    if (b[0] == 'n') {
      return 0;
    } else {
      return -1;
    }
  } else {
    if (b[0] == 'n') {
      return 1;
    } else {
      int x, y;
      sscanf(a, "%d", &x);
      sscanf(b, "%d", &y);
      if (x <= y) {
        return 0;
      } else {
        return -1;
      }
    }
  }
}

int ReadProg(int m) {
  top = 1;
  st[0] = (For){'0', 0};
  memset(use, 0, sizeof use);

  int ans = 0;
  bool error = false;
  while (m--) {
    char s[2];
    scanf("%s", s);
    if (s[0] == 'F') {

      char a[100], b[100];
      scanf("%s%s%s", s, a, b);
      if (error) {
        continue;
      }

      if (use[s[0] - 'a']) {
        error = true;
        continue;
      } else {
        use[s[0] - 'a'] = true;
      }

      int w = Ana(a, b);
      if (w == -1 || st[top - 1].comp == -1) {
        w = -1;
      } else {
        w += st[top - 1].comp;
      }
      st[top++] = (For){s[0], w};

    } else {

      if (error) {
        continue;
      }
      if (top == 1) {
        error = true;
        continue;
      }
      use[st[top - 1].var - 'a'] = false;
      ans = max(ans, st[top - 1].comp);
      --top;

    }
  }
  if (top > 1) {
    error = true;
  }
  return error ? -1 : ans;
}

void Solve(void) {
  int nl;
  scanf("%d", &nl);
  int x = ReadComp();
  int y = ReadProg(nl);
  if (y == -1) {
    puts("ERR");
  } else if (x == y) {
    puts("Yes");
  } else {
    puts("No");
  }
}

int main(void) {
  freopen("complexity.in", "r", stdin);
  freopen("complexity.out", "w", stdout);
  int t;
  scanf("%d", &t);
  while (t--) {
    Solve();
  }
  return 0;
}

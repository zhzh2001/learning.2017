#include <cstdio>
using namespace std;

int main(void) {
  freopen("math.in", "r", stdin);
  freopen("math.out", "w", stdout);
  int a, b;
  scanf("%d%d", &a, &b);
  printf("%lld\n", (long long)(a - 1) * (b - 1) - 1);
  return 0;
}

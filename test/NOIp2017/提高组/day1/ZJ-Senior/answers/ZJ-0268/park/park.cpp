#include <cstdio>
using namespace std;
char ch;
inline void read(int &x){
	x=0;ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}
int n,m,k,p;
int edg,to[200020],nxt[200020],fst[100010],w[200020];
inline void ins(int x,int y,int z){++edg;to[edg]=y,nxt[edg]=fst[x],fst[x]=edg,w[edg]=z;}
int q[100010],vis[100010],dis[100010];
void spfa(){
	int h=0,t=1;
	for (int i=1;i<=n;++i) vis[i]=0,dis[i]=1e9;
	q[1]=1;vis[1]=1;dis[1]=0;
	while (h!=t){
		++h;if (h>n) h-=n;
		for (int i=fst[q[h]];i>0;i=nxt[i]) if (dis[q[h]]+w[i]<dis[to[i]]){
			dis[to[i]]=dis[q[h]]+w[i];
			if (!vis[to[i]]){
				++t;if (t>n) t-=n;
				q[t]=to[i];
				vis[to[i]]=1;
			}
		}
		vis[q[h]]=0;
	}
}
int edgg,too[10000001],nxtt[10000001],fstt[100001][51],ww[10000001],ru[100001][51];
int qq[5000001][2],f[100001][51];
void dp(){
	int hh=0,tt=0,nowx,nowy,nxtx,nxty;
	for (int i=1;i<=n;++i) for (int j=0;j<=k;++j) if (ru[i][j]==0&&dis[i]<1e9) qq[++tt][0]=i,qq[tt][1]=j;
	f[1][0]=1;
	while (hh<tt){
		++hh;nowx=qq[hh][0],nowy=qq[hh][1];
		for (int i=fstt[nowx][nowy];i>0;i=nxtt[i]){
			nxtx=too[i],nxty=dis[nowx]+nowy+ww[i]-dis[nxtx];
			f[nxtx][nxty]+=f[nowx][nowy];
			if (f[nxtx][nxty]>p) f[nxtx][nxty]-=p;
			--ru[nxtx][nxty];
			if (ru[nxtx][nxty]==0) qq[++tt][0]=nxtx,qq[tt][1]=nxty;
		}
	}
}
int main(){
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	int T;read(T);
	while (T--){
		read(n),read(m),read(k),read(p);
		edg=0;for (int i=1;i<=n;++i) fst[i]=0;
		for (int i=1,x,y,z;i<=m;++i) read(x),read(y),read(z),ins(x,y,z);
		spfa();
		edgg=0;for (int i=1;i<=n;++i) for (int j=0;j<=k;++j) fstt[i][j]=ru[i][j]=f[i][j]=0;
		for (int i=1;i<=n;++i) if (dis[i]<1e9)
			for (int j=fst[i];j>0;j=nxt[j]) for (int kk=0;dis[i]+kk+w[j]<=dis[to[j]]+k&&kk<=k;++kk){
				edgg++;too[edgg]=to[j],nxtt[edgg]=fstt[i][kk];ww[edgg]=w[j];fstt[i][kk]=edgg;
				ru[to[j]][dis[i]+kk+w[j]-dis[to[j]]]++;
			}
		dp();
		int ans=0;
		for (int i=0;i<=k;++i) ans=(ans+f[n][i])%p;
		if (ans==0) puts("-1");else printf("%d\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstdio>
using namespace std;
char ch;
int T,l,w,a,b,flag,top,ans,mark[30],stack[101],num[101],shan[101];
inline void read(int &x){
	x=0;ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
}
int main(){
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	read(T);
	while (T--){
		read(l);while (ch!='n'&&(ch<'0'||ch>'9')) ch=getchar();
		if (ch=='n') read(w);
		else w=0;
		ans=0;flag=1,top=0;num[0]=0;for (int i=0;i<26;++i) mark[i]=0;for (int i=0;i<=100;++i) shan[i]=0;
		for (int i=1;i<=l;++i){
			ch=getchar();while (ch!='F'&&ch!='E') ch=getchar();
			if (ch=='F'){
				ch=getchar();while (ch<'a'||ch>'z') ch=getchar();
				if (mark[ch-'a']==1) flag=0;
				
				stack[++top]=ch-'a';mark[ch-'a']=1;
				
				if (shan[top-1]) shan[top]=1;int spe=0;
				
				ch=getchar();while (ch!='n'&&(ch<'0'||ch>'9')) ch=getchar();
				if (ch=='n') spe=1;
				a=0;while (ch>='0'&&ch<='9') a=a*10+ch-'0',ch=getchar();
				
				ch=getchar();while (ch!='n'&&(ch<'0'||ch>'9')) ch=getchar();
				if (ch=='n'){
					if (shan[top]) continue;
					if (spe==1) num[top]=num[top-1];
					else{
						num[top]=num[top-1]+1;
						if (num[top]>ans) ans=num[top];
					}
				}else{
					b=0;while (ch>='0'&&ch<='9') b=b*10+ch-'0',ch=getchar();
					if (spe==1||a>b) shan[top]=1;
					if (shan[top]) continue;
					num[top]=num[top-1];
				}
			}else{
				if (top==0){flag=0;continue;}
				mark[stack[top]]=0;
				shan[top]=0;
				top--;
			}
		}
		if (top>0) flag=0;
		if (flag==0) puts("ERR");
		else if (ans==w) puts("Yes");
		else puts("No");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

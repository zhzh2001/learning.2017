var n,j,i,tt,w,sum,l,j1,p,summ,max:longint;
    c:array[0..1001] of longint;
    a,b:array[0..1001] of string;
    d:array[0..1001] of char;
    f:array['a'..'z'] of 0..1;
    s,s1,ch1,ch2,sss:string;
    ch12,ch:char;
    ff:boolean;
begin

  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(n);
  for i:=1 to n do
    begin
    fillchar(f,sizeof(f),0); ff:=false; p:=0; max:=0;
    sss:='';
    readln(l,s1);sum:=0;w:=0;
    for j:=1 to l do
      if not ff then
      begin
      read(ch);
      if ch='F' then
        begin
        inc(w);
        readln(ch,ch12,s);
        if f[ch12]=1 then
          begin
          for j1:=j+1 to l do
            readln(s);
          writeln('ERR');
          ff:=true;
          break;
          end;
        delete(s,1,1);
        tt:=pos(' ',s);
        ch1:=copy(s,1,tt-1);
        delete(s,1,tt);
        ch2:=copy(s,1,length(s));
        f[ch12]:=1;
        a[w]:=ch1;b[w]:=ch2;c[w]:=j;d[w]:=ch12;
        end
      else if ch='E' then
        begin
        readln;
        if w=0 then
          begin
          for j1:=j+1 to l do
            readln(s);
          writeln('ERR');
          ff:=true;
          break;
          end;
        if j=c[w]+1+p then
          begin
          if (a[w]<>'n') and (b[w]='n') then
            inc(sum)
          else if (a[w]='n') and (b[w]<>'n') then
            sum:=0
          else if (a[w]<>'n') and (b[w]<>'n') and (a[w]>b[w]) then
            sum:=0;
          p:=p+2;
          end
        else if j=c[w]+1 then
          begin
          if sum>max then max:=sum;
          sum:=0;p:=0;
          if (a[w]<>'n') and (b[w]='n') then
            inc(sum)
          else if (a[w]='n') and (b[w]<>'n') then
            sum:=0
          else if (a[w]<>'n') and (b[w]<>'n') and (a[w]>b[w]) then
            sum:=0;
          p:=p+2;
          end;
        f[d[w]]:=f[d[w]]-1;
        dec(w);
        end;
      end;
    if sum>max then max:=sum;
    while s1[length(s1)]=' ' do
      delete(s1,length(s1),1);
    summ:=max;
    while max>0 do
      begin
      sss:=chr(max mod 10+ord('0'))+sss;
      max:=max div 10;
      end;
    if not ff then
    begin
    if w<>0 then writeln('ERR')
    else if (summ=0) and (s1=' O(1)') then
      writeln('Yes')
    else if (summ<>0) and (s1=' O(n^'+sss+')') then
      writeln('Yes')
    else writeln('No');
    end;
    end;
  close(input);close(output);
end.
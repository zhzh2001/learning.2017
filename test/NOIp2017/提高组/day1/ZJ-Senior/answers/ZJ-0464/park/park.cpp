#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <queue>
#define fi first
#define se second
#define mp make_pair
#define pb push_back
#define Pa pair<int,int>
using namespace std;
const int Maxn=101000;
const int Maxm=202000;
const int Inf=0x3f3f3f3f;
struct Info
{
	int to,next,w;
}edge[Maxm],Edge[Maxm],EDGE[Maxm];
int head[Maxn],Head[Maxn],HEAD[Maxn],cnt,CNT,Cnt;
int n,m,k,p,KASE,u,v,w,QQ[Maxn],Dis[Maxn],Ans;
priority_queue<Pa,vector<Pa>,greater<Pa> > Q;
queue<Pa> q;
bool vis[Maxn];
inline void Add(int u,int v,int w)
{EDGE[CNT].to=v;EDGE[CNT].next=HEAD[u];EDGE[CNT].w=w;HEAD[u]=CNT++;}
inline void Add2(int u,int v,int w)
{edge[cnt].to=v;edge[cnt].next=head[u];edge[cnt].w=w;head[u]=cnt++;}
inline void Add3(int u,int v)
{Edge[Cnt].to=v;Edge[Cnt].next=Head[u];Edge[Cnt].w=0;Head[u]=Cnt++;}

bool Check()
{
	int l=1,r=1; QQ[1]=1;
	memset(vis,false,sizeof(vis)); vis[1]=true;
	while (l<=r)
	{
		int u=QQ[l++];
		for (int i=Head[u];i!=-1;i=Edge[i].next)
		{
			if (vis[Edge[i].to]) return true;
			QQ[++r]=Edge[i].to;
			vis[Edge[i].to]=true;
		}
	}
	return false;
}

void Dij()
{
	
	for (int i=1;i<=n;i++) Dis[i]=Inf; Dis[n]=0;
	memset(vis,false,sizeof(vis));
	Q.push(mp(0,n));
	while (!Q.empty()) 
	{
		int u=Q.top().se; Q.pop();
		if (vis[u]) continue; vis[u]=true;
		for (int i=head[u];i!=-1;i=edge[i].next)
			if (!vis[edge[i].to] && Dis[edge[i].to]>Dis[u]+edge[i].w)
			{
				Dis[edge[i].to]=Dis[u]+edge[i].w;
				Q.push(mp(Dis[edge[i].to],edge[i].to));
			}
	}
}
void Bfs()
{
	int l=1,r=1;
	q.push(mp(1,0));
	while (!q.empty())
	{
		Pa u=q.front(); q.pop();
		if (u.se+Dis[u.fi]>Dis[1]+k) continue;
		if (u.fi==n) {Ans=(Ans+1)%p;}

		for (int i=HEAD[u.fi];i!=-1;i=EDGE[i].next)
			q.push(mp(EDGE[i].to,u.se+EDGE[i].w));
	}
}	


int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&KASE);
	for (int Kase=1;Kase<=KASE;Kase++)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p); Ans=0;
		memset(head,-1,sizeof(head)); cnt=0;
		memset(Head,-1,sizeof(Head)); Cnt=0;
		memset(HEAD,-1,sizeof(HEAD)); CNT=0;
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&u,&v,&w);
			if (w==0) Add3(u,v);
			Add(u,v,w),Add2(v,u,w);
		}
		if (Check()) {puts("-1"); continue;}
		Dij();
		Bfs();
		printf("%d\n",Ans);
	}
	return 0;
}




#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;
int Kase,Base,L,Ans,Top,BlockNum;
char ch,Stack[400];
bool vis[30],Block[30],Useful[30];
inline int Max(int x,int y) {return x>y?x:y;}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&Kase);
	for (int Case=1;Case<=Kase;Case++)
	{
		scanf("%d",&L); 
		ch=getchar(); while (ch!='O') ch=getchar();
		ch=getchar(); ch=getchar();
		if (ch=='1') Base=0; else
		{
			ch=getchar();
			scanf("%d",&Base);
		}
		int Ret=0,F=0;
		memset(vis,false,sizeof(vis));
		memset(Block,false,sizeof(Block)); BlockNum=0;
		memset(Useful,false,sizeof(Useful)); 
		bool flag=false;  Top=200; Ans=0; //!!!!
		for (int i=1;i<=L;i++)
		{
			ch=getchar();
			while (ch!='E' && ch!='F') ch=getchar();
			if (ch=='F')
			{
				ch=getchar(); while (ch<'a' || ch>'z') ch=getchar();
				Stack[++Top]=ch;
				if (vis[ch-'a'+1])	flag=true;
				vis[ch-'a'+1]=true;

				bool t1,t2; int Res1=0,Res2=0;
				ch=getchar();
				while (ch!='n' && (ch<'0' || ch>'9')) ch=getchar();
				if (ch=='n') t1=true;
				if (ch>='0' && ch<='9')
				{
					while (ch>='0' && ch<='9') {Res1=Res1*10+ch-'0'; ch=getchar();}
					t1=false;
				}

				ch=getchar();
				while (ch!='n' && (ch<'0' || ch>'9')) ch=getchar();
				if (ch=='n') t2=true;
				if (ch>='0' && ch<='9')
				{
					while (ch>='0' && ch<='9') {Res2=Res2*10+ch-'0'; ch=getchar();}
					t2=false;
				}
				F++;
				if (!t1 && !t2 && Res1>Res2) 
				{
					Block[Stack[Top]-'a'+1]=true; BlockNum++;	
				}
				if (!t1 && t2 && !BlockNum)
				{

					Ret++;
					Ans=Max(Ans,Ret);
					Useful[Stack[Top]-'a'+1]=true;
				}
				if (t1 && !t2)
				{
					Block[Stack[Top]-'a'+1]=true; BlockNum++;
				}
			}


			if (ch=='E')
			{
				F--;
				if (F<0) {flag=true;}
				if (Useful[Stack[Top]-'a'+1]) Ret--;
				Useful[Stack[Top]-'a'+1]=false;
				vis[Stack[Top]-'a'+1]=false;
				if (Block[Stack[Top]-'a'+1]) 
				{
					Block[Stack[Top]-'a'+1]=false;
					BlockNum=BlockNum-1;
				}
				Top--;
			}

		}


		if (flag || F) {puts("ERR"); continue;} 
		if (Ans==Base) puts("Yes"); else puts("No");
	}

	return 0;
}



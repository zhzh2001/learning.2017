#include<iostream>
#include<cstring>
#include<cstdio>
#include<algorithm>
#define N 1000000
#define M 2000000
#define Inf 1<<30
using namespace std;
int n,m,K,cnt=0,p,ans,Mod,T,x,y,z;
int dis[N],first[N];
int next[M],to[M],w[M];
bool cover[N];
void add(int x,int y,int z){
	cnt++;
	next[cnt]=first[x];
	first[x]=cnt;
	w[cnt]=z;
	to[cnt]=y;
	return;
}

void dfs(int x,int k){
	if(k>p+K)	return;
	if(x==n){
		ans++;
		ans=ans%Mod;
	}
	for(int i=first[x];i;i=next[i]){
		int f=to[i];
		dfs(f,k+w[i]);
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>T;
	while(T--){
		cin>>n>>m>>K>>Mod;
		ans=0;
		memset(next,0,sizeof(next));
		memset(first,0,sizeof(first));
		memset(cover,0,sizeof(cover));
		cnt=0;
		for(int i=1;i<=m;i++){
			cin>>x>>y>>z;
			add(x,y,z);
		}
		memset(dis,0x3f,sizeof(dis));
		dis[1]=0;
		for(int i=1;i<=n;i++){
			int minn=Inf;
			int num;
			if(!cover[i] && dis[i]<minn)	minn=dis[i],num=i;
			cover[num]=1;
			for(int i=first[num];i;i=next[i]){
				int f=to[i];
				if(dis[num]+w[i]<dis[f])	dis[f]=dis[num]+w[i];
			}
		}
		p=dis[n];
		dfs(1,0);
		cout<<ans<<endl;
	}
}

#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#define N 100000
using namespace std;
int T,L,maxn;
string s1[N],s2[N],s3[N];
char flag[N];
bool hash[N];
int next[N];
int calc(string s){
	if(s[2]=='1')	return 0;
	else if(s[3]==')')	return 1;
		else return s[4]-'0';
}

int val(string s){
	int res=0;
	int len=s.length();
	for(int i=0;i<len;i++){
		res=res*10+s[i]-'0';
	}
	return res;
}

void find(int x,int cnt){
	maxn=max(maxn,cnt);
	if(x==L+1)	return;
	if(flag[x]=='E')	return;
	if(s2[x][0]=='n')
		if(s3[x][0]=='n')	find(x+1,cnt),find(next[x]+1,cnt);
		else	find(next[x]+1,cnt);
	else 
		if(s3[x][0]=='n')	find(x+1,cnt+1),find(next[x]+1,cnt);
		else {
			if(val(s2[x])>val(s3[x]))	find(next[x]+1,cnt);
			else find(x+1,cnt),find(next[x]+1,cnt);
		}
	return;
}

void work(){
	string s;
	cin>>L>>s;
	int need=calc(s);
	for(int i=1;i<=L;i++){
		cin>>flag[i];
		if(flag[i]=='F')	cin>>s1[i]>>s2[i]>>s3[i];
	}
	int stack[N],top=0;
	memset(hash,0,sizeof(hash));
	for(int i=1;i<=L;i++){
		if(flag[i]=='F')	{
			if(hash[int(s1[i][0])]){
				cout<<"ERR"<<endl;
				return;
			}
			else hash[int(s1[i][0])]=1;
			stack[++top]=i;
		}
		else	{
			if(top>0)	{
				next[stack[top]]=i;
				hash[int(s1[stack[top]][0])]=0;
				top--;
			}	
			else{
				cout<<"ERR"<<endl;
				return;
			}
		}
	}
	if(top>0){
		cout<<"ERR"<<endl;
		return;
	}
	maxn=0;
	find(1,0);
	if(maxn==need)	cout<<"Yes"<<endl;
	else cout<<"No"<<endl;
	return;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>T;
	while(T--)	work();
	return 0;
}
/*
1
4 O(n)
F i 1 1
F i 1 n
E
E
*/

#include <cstdio>
#include <cctype>
void exgcd(long long a,long long b,long long &c,long long &d)
{
	if(b==1){c=0; d=1; return;}
	exgcd(b,a%b,d,c);
	d-=(a/b)*c;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	long long a,b,c,d,ans;
	scanf("%lld%lld",&a,&b);
	exgcd(a,b,c,d);
	for(long long i=a*b;i>=1;i--)
	{
		long long ci=c*i,di=d*i;
		if(ci<0)
		{
			ci+=(di/a)*b;
			if(ci<0){ans=i; break;}
		}
		else
		{
			di+=(ci/b)*a;
			if(di<0){ans=i; break;}
		}
	}
	printf("%lld\n",ans);
	return 0;
}

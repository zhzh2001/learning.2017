#include <cstdio>
#include <cctype>
#include <string>
#include <cstring>
#include <iostream>
using namespace std;
string s;
bool w[10001];
int n,t,top,sta[10001],sta2[10001];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>t;
	while(t--)
	{
		top=0;
		memset(w,0,sizeof w);
		int ans=0;
		cin>>n>>s;
		int l=s.size(),y=0,m=0,tot=0;
		for(int i=0;i<l;i++)
			if(s[i]=='n') y=1;
			else if(isdigit(s[i])) m=m*10+s[i]-'0';
		for(int i=0;i<n;i++)
		{
			char c;
			while(c=cin.get(),c!='E' && c!='F');
			if(c=='E')
				if(top) w[sta[top--]]=0; else ans=-1;
			else
			{
				while(c=cin.get(),c<'a' || c>'z');
				int v=c-'a';
				if(w[v]) ans=-1; else {w[sta[++top]=v]=1;}
				int l=0,r=0;
			
				while(c=cin.get(),!isdigit(c) && c!='n');
				if(c=='n') l=-1;
				if(isdigit(c))
				{
					l=c-'0';
					while(isdigit(c=cin.get())) l=l*10+c-'0';
				}
			
				while(c=cin.get(),!isdigit(c) && c!='n');
				if(c=='n') r=-1;
				if(isdigit(c))
				{
					r=c-'0';
					while(isdigit(c=cin.get())) r=r*10+c-'0';
				}
			
				if(r==-1 && l!=-1){sta2[top]=sta2[top-1]+1;}
				else if((l==-1 && r!=-1) || (r!=-1 && l>r)) sta2[top]=-10000;
				else sta2[top]=sta2[top-1];
				tot=max(sta2[top],tot);
			}
		}
		if(top) ans=-1;
		if(ans==-1) puts("ERR");
		else
		{
			if(y==1 && tot==m) ans=1;
			if(y==0 && tot==0) ans=1;
			puts(ans? "Yes":"No");
		}
	}
	return 0;
}

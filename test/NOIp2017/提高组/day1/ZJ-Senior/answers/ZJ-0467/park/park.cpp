#include <cstdio>
#include <cctype>
#include <vector>
const int maxn=101;
using namespace std;
int n,m,tot,k,p,f[maxn][maxn],w[maxn][maxn];
int read()
{
	int x; scanf("%d",&x); return x;
}
void dfs(int o,int z)
{
	if(z>k+f[1][n]) return;
	if(o==n && z<=k+f[1][n]) tot=(tot+1)%p;
	for(int i=1;i<=n;i++)
		if(w[o][i]!=0)
			dfs(i,w[o][i]+z);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t=read();
	for(int i=1;i<=t;i++)
	{
		n=read(),m=read(),k=read(),p=read();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=1e8,w[i][j]=0;
		for(int i=1;i<=m;i++)
		{
			int x=read(),y=read();
			f[x][y]=w[x][y]=read();
		}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++) if(i!=j && j!=k && k!=i)
					f[j][k]=min(f[j][i]+f[i][k],f[j][k]);
		tot=0;
		dfs(1,0);
		printf("%d\n",tot);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int dis[100005],dis2[100005],bo[100005],f[300005],ff[6000005][2],bo2[100005][52],dp[100005][52];
int n,m,p,k,j,kk,l,s,e,c[100005],c2[100005],b[200005][3],b2[200005][3],bb[100005],ans,t,st;
int dis3[100005],dis4[100005],z[100005],zn,cnt[100005],fa[100005],bo4[100005],bo3[100005],mi[100005],ll;
void spfa1()
{
	for(int i=1;i<=n;i++)dis[i]=2147483647/2,bo[i]=0; dis[1]=0; bo[1]=1; s=0; e=1; f[e]=1;
	while(s!=e)
	{
		s++; if(s==300001)s=1;
		int x=f[s];
		for(int i=c[x];i;i=b[i][2])
		{
			if(dis[x]+b[i][0]<dis[b[i][1]])
			{
				dis[b[i][1]]=dis[x]+b[i][0];
				if(bo[b[i][1]]==0){ bo[b[i][1]]=1; e++; if(e==300001)e=1; f[e]=b[i][1]; }
			}
		}
		bo[x]=0;
	}
}
void spfa2()
{
	for(int i=1;i<=n;i++)dis2[i]=2147483647/2,bo[i]=0; dis2[n]=0; bo[n]=1; s=0; e=1; f[e]=n;
	while(s!=e)
	{
		s++; if(s==300001)s=1;
		int x=f[s];
		for(int i=c2[x];i;i=b2[i][2])
		{
			if(dis2[x]+b2[i][0]<dis2[b2[i][1]])
			{
				dis2[b2[i][1]]=dis2[x]+b2[i][0];
				if(bo[b2[i][1]]==0){ bo[b2[i][1]]=1; e++; if(e==300001)e=1; f[e]=b2[i][1]; }
			}
		}
		bo[x]=0;
	}
}
void spfa3()
{
	for(int i=1;i<=n;i++)for(int i=0;i<=bb[i];i++)dp[i][j]=0,bo2[i][j]=0; dp[1][0]=1; bo2[1][0]=1; s=0; e=1; ff[e][0]=1; ff[e][1]=0;
	while(s!=e)
	{
		s++; if(s==6000001)s=1;
		int x=ff[s][0],y=ff[s][1];
		if(x==n)ans=(ans+dp[x][y])%p;
		for(int i=c[x];i;i=b[i][2])
		{
			if(dis[x]+y+b[i][0]-dis[b[i][1]]<=bb[b[i][1]])
			{
				int x1=b[i][1],y1=dis[x]+y+b[i][0]-dis[b[i][1]];
				dp[x1][y1]=(dp[x1][y1]+dp[x][y])%p;
				if(bo2[x1][y1]==0){ bo2[x1][y1]=1; e++; if(e==6000001)e=1; ff[e][0]=x1; ff[e][1]=y1; }
			}
		}
		bo2[x][y]=0; dp[x][y]=0; 
	}
}
int ss(int x)
{
	zn++; bo[x]=1; bo4[x]=1; z[zn]=x; cnt[x]=zn; mi[x]=zn;
	for(int i=c[x];i;i=b[i][2])
	if(b[i][0]==0)
	{
		if(bo[b[i][1]]==0){ mi[x]=min(mi[x],ss(b[i][1])); }else
		if((bo[b[i][1]]==1)and(bo4[b[i][1]]==1))mi[x]=min(mi[x],cnt[b[i][1]]);
	}
	if((cnt[x]==1)or(mi[x]>=cnt[x]))
	{ if(zn>cnt[x])bo3[x]=1; while(zn>=cnt[x]){ fa[z[zn]]=x; bo4[z[zn]]=0; zn--; } dis3[x]=dis4[x]=2147483647/2; }
	return mi[x];
}	
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	for(int ii=1;ii<=t;ii++)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=n;i++)c[i]=c2[i]=0;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&j,&kk,&l);
			b[i][0]=l; b[i][1]=kk; b[i][2]=c[j]; c[j]=i;
			b2[i][0]=l; b2[i][1]=j; b2[i][2]=c2[kk]; c2[kk]=i;
		}
		for(int i=1;i<=n;i++)bo[i]=bo3[i]=bo4[i]=0,fa[i]=i; zn=0;
		for(int i=1;i<=n;i++)if(bo[i]==0)ss(i);
		spfa1(); spfa2(); 
		st=dis[n]; for(int i=1;i<=n;i++)bb[i]=st+k-dis[i]-dis2[i];
		for(int i=1;i<=n;i++)dis3[fa[i]]=min(dis3[fa[i]],dis[i]),dis4[fa[i]]=min(dis4[fa[i]],dis2[i]);
		ll=0; for(int i=1;i<=n;i++)if((bo3[i]==1)and(dis3[i]+dis4[i]<=st+k)){ ll=1; break; }
		//for(int i=1;i<=n;i++)printf("%d %d %d\n",dis[i],dis2[i],bb[i]);
		if(ll==1)printf("-1\n");else { ans=0; spfa3(); printf("%d\n",ans); }
	}
	return 0;
}

#include <cstdio>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>
using namespace std;
typedef long long ll;
#define rep(i,a) for (int i=1,i_=(a); i<=i_; ++i)
#define repi(i,a,b) for (int i=(a),i_=(b); i<=i_; ++i)
#define repd(i,a,b) for (int i=(a),i_=(b); i<=i_; --i)

#define MAXN 100100
#define MAXM 200100
#define MAXK 60
#define INF 0x3f3f3f3f

inline int read() { int ret = -1; scanf("%d", &ret); return ret; }

struct Edge {
	int u, v, w;
	Edge(int u=0, int v=0, int w=0): u(u), v(v), w(w) {}
};

struct Node {
	int u, d;
	Node(int u=0, int d=0): u(u), d(d) {}
	bool operator < (const Node &rhs) const {
		return d > rhs.d;
	}
};

vector<int> G[MAXN];
Edge E[MAXM];
int n, m, k, p, dis[MAXN], etot, dp[MAXN][MAXK];
bool v[MAXN];
priority_queue<Node> Q;

int dfs0(int x) {
	if (v[x]) return 1;
	v[x] = 1;
	repi(i,0,G[x].size()-1) {
		Edge &e = E[G[x][i]];
		if ((e.w == 0) && dfs0(e.v)) return 1;
	}
	return 0;
}

int dijkstra(int s) {
	while (!Q.empty()) Q.pop();
	memset(dis, 0x3f, sizeof(dis));
	Q.push(Node(s, dis[s]=0));
	while (!Q.empty()) {
		Node now = Q.top();
		Q.pop();
		if (dis[now.u] < now.d) continue;
		repi(i,0,G[now.u].size()-1) {
			Edge &e = E[G[now.u][i]];
			if (dis[e.u] + e.w < dis[e.v]) 
				Q.push(Node(e.v, dis[e.v]=dis[e.u]+e.w));
		}
	}
}

void dfs(int x) {
	if (v[x]) return;
	v[x] = 1;
	repi(i,0,G[x].size()-1) {
		Edge &e = E[G[x][i]];
		int off = dis[e.u] + e.w - dis[e.v];
		repi(j,0,k-off) dp[e.v][j+off] = (dp[e.v][j+off] + dp[e.u][j]) % p;
		dfs(e.v);
	}
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T = read();
	while (T--) {
		n = read();
		m = read();
		k = read();
		p = read();
		rep(i,m) {
			int x = read(), y = read(), z = read();
			E[++etot] = Edge(x, y, z);
			G[x].push_back(etot);
		}
		int flag = 0;
		memset(v, 0, sizeof(v));
		rep(i,n) if (!v[i] && dfs0(i)) {
			printf("%d\n", -1);
			flag = 1;
			break;
		}
		if (flag) continue;
		dijkstra(1);	
		memset(v, 0, sizeof(v));
		dp[1][0] = 1;
		dfs(1);
		int ans = 0;
		repi(i,0,k) ans = (ans + dp[n][i]) % p;
		printf("%d\n", ans);
	}
	return 0;
}


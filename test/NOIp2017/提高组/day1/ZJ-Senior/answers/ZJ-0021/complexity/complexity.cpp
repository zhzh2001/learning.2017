#include <cstdio>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>
using namespace std;
//typedef long long ll;
#define rep(i,a) for (int i=1,i_=(a); i<=i_; ++i)
#define repi(i,a,b) for (int i=(a),i_=(b); i<=i_; ++i)
#define repd(i,a,b) for (int i=(a),i_=(b); i<=i_; --i)

#define MAXN 200

int val(string str) {
	stringstream ss(str);
	int ret = -1;
	ss >> ret;
	return ret;
}

bool used[256], ran[MAXN];
char vars[MAXN];

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int t;
	cin >> t;
	while (t--) {
		int l, comp, errflag = 0, ans = 0, fornum = 0, run = 0, cur = 0;
		string compstr;
		memset(used, 0, sizeof(used));
		memset(ran, 0, sizeof(ran));
		cin >> l >> compstr;
		if (compstr == "O(1)") comp = 0;
		else comp = val(compstr.substr(4, compstr.length()-5));
		rep(i,l) {
			char opr;
			cin >> opr;
			if (opr == 'F') {
				char var;
				string fromstr, tostr;
				int from, to;
				cin >> var >> fromstr >> tostr;
				if (used[(int)var]) {
					errflag = 1;
					char ctmp;
					string stmp;
					repi(j,i+1,l) {
						cin >> ctmp;
						if (ctmp == 'F') cin >> stmp >> stmp >> stmp;
					}
					break;
				}
				vars[++fornum] = var;
				used[(int)var] = 1;
				if (fromstr == "n") from = -1;
				else from = val(fromstr);
				if (tostr == "n") to = -1;
				else to = val(tostr);
				if ((from == -1 && to != -1) || (from > to && to != -1)) run = fornum, ran[fornum] = 0;
				else if (from != -1 && to == -1 && run == 0) ++cur, ans = max(ans, cur), ran[fornum] = 1;
				else ran[fornum] = 0;
			} else if (opr == 'E') {
				used[(int)vars[fornum]] = 0;
				if (ran[fornum]) --cur;
				--fornum;
				if (fornum < run) run = 0;
			}
		}
		if (errflag || fornum != 0) {
			cout << "ERR" << endl;
			continue;
		}
		if (ans == comp) cout << "Yes" << endl;
		else cout << "No" << endl;
	}
	return 0;
}


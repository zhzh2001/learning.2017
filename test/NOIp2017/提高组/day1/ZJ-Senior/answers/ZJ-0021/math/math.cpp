#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <vector>
#include <queue>
#include <set>
#include <map>
using namespace std;
typedef long long ll;
#define rep(i,a) for (int i=1,i_=(a); i<=i_; ++i)
#define repi(i,a,b) for (int i=(a),i_=(b); i<=i_; ++i)
#define repd(i,a,b) for (int i=(a),i_=(b); i<=i_; --i)

#define MAXN

ll a, b, ans;

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	cin >> a >> b;
	ans = ((a-1) * (b-1) - 1);
	cout << ans << endl;
	return 0;
}


#include<stdio.h>
#include<queue>
#include<utility>
#include<iostream>
using namespace std;
char c;
long long dist[100005],n,m,t,x,y,k,p,i,z,j;
long long l[200005][3],u,v,gx,kk,book[200005];
long long ans[100005][55],s,f[100005];
int xh[100005];
void r(long long &x)
{
	x=0;
	c=getchar();
	while(c<'0'||c>'9') c=getchar();
	while(c>='0'&&c<='9') {
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();}
}
void dj1()
{
	priority_queue<pair<long long,long long> >pq1;
	pq1.push(make_pair(0,1));
	while(pq1.size())
	{
		u=pq1.top().second;
		t=-pq1.top().first;
		pq1.pop();
		kk=xh[u];
		while(kk!=0)
		{
			v=l[kk][1];
			gx=t+l[kk][2];
			if(dist[v]+kk>=gx)
			{
				ans[v][gx-dist[v]]++;
				pq1.push(make_pair(-gx,v));
			}
			kk=l[kk][0];
		}
	}
}
void dj()
{
	priority_queue<pair<long long,long long> >pq;
	pq.push(make_pair(0,1));
	while(pq.size())
	{
		u=pq.top().second;
		t=-pq.top().first;
		pq.pop();
		if(dist[u]<t) continue;
		kk=xh[u];
		while(kk!=0)
		{
			v=l[kk][1];
			gx=t+l[kk][2];
			if(dist[v]>gx)
			{
				dist[v]=gx;
				pq.push(make_pair(-gx,v));
			}
			kk=l[kk][0];
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	r(t);
	while(t--)
	{
		r(n),r(m),r(k),r(p);
		for(i=2;i<=n;i++)
		dist[i]=9999999;
		for(i=1;i<=m;i++)
		{
			r(x),r(y),r(z);
			l[i][1]=y;
			l[i][2]=z;
			l[i][0]=xh[x];
			xh[x]=i;
		}
		dj();
		dj1();
		for(i=0;i<=k;i++)
		s=(s+ans[n][i])%p;
		printf("%lld\n",s);
		for(i=1;i<=n;i++)
		for(j=0;j<=k;j++)
		ans[i][j]=0;
		for(i=1;i<=m;i++)
		book[i]=1;
		for(i=1;i<=n;i++)
		xh[i]=0;
		i++;
	}
	return 0;
}

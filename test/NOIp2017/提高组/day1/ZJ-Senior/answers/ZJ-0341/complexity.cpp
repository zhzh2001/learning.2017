#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>

#define rep(i,a,b)	for(int i=a;i<=b;i++)
#define rev(i,a,b)	for(int i=a;i>=b;i++)
#define reg(i,g,u)	for(int i=h[u];i;i=g[i].nxt)
#define Inf (1<<30)
#define nc getchar

using namespace std;
typedef long long LL;
typedef double db;

/*
inline nc(){
	static buff[100000],p1=*buff,p2=*buff;
	return p1==p2&&(p2=(p1=buff)+fread(buff,1,100000,stdin):p1==p2?EOF:*p1++);
}*/

inline void read(int &data){
	data=0;int e=1;char ch=nc();
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}

inline void write(int data){
	if (data<0)	putchar('-'),data=-data;
	if (data>9)	write(data/10);
	putchar(data%10+48);
}

char get(){
	char ope;
	ope=nc();while (ope!='F'&&ope!='E')	ope=nc();
}

const int maxn=1e2+5;
int n;
int stk[maxn],p[maxn],v[2000],flag[maxn];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;read(T);
	while (T--){
		read(n);	
		char ch=nc(),ope;	
		while (ch!='n'&&(ch<'0'||ch>'9'))	ch=nc();
		int ans=0;
		if (ch=='n')	read(ans);
		memset(v,true,sizeof(v));memset(p,0,sizeof(p)),memset(stk,0,sizeof(stk)),memset(flag,0,sizeof(flag));
		int now=0,k=0,fp=0,real=0,kkk=true;
		rep(i,1,n){
			ope=get();
			if (ope=='E'){
				if (!kkk)	continue;
				now-=stk[k],fp-=flag[k],v[p[k]]=true;
				stk[k]=0,flag[k]=0,p[k--]=0;
				 if (k<0)	kkk=false;	continue;
			}
			ope=nc(),ope=nc();
			if (k>-1)	k++,p[k]=ope;
			if (!v[ope]){
				kkk=false;
			}else v[ope]=false;
			ope=nc(),ope=nc();
			int a=0,b=0;
			if (ope=='n')	a=Inf,ope=nc();
			else
				for(;ope>='0'&&ope<='9';a=a*10+ope-48,ope=nc());
			ope=nc();
			if (ope=='n')	b=Inf,ope=nc();
			else
				for(;ope>='0'&&ope<='9';b=b*10+ope-48,ope=nc());
			if (fp!=0||kkk<0)	continue;
			if (a<b&&b==Inf)	stk[k]=1,now++;
			else	if (a>b)	flag[k]=1,fp++;
			real=max(real,now);			
		}	
		if (k!=0||!kkk)	puts("ERR");
		else
			if (real!=ans)	puts("No");
			else puts("Yes");
	}
	return 0;
}

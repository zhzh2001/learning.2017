#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>

#define rep(i,a,b)	for(int i=a;i<=b;i++)
#define rev(i,a,b)	for(int i=a;i>=b;i++)
#define reg(i,g,u)	for(int i=h[u];i;i=g[i].nxt)
#define Inf (1<<30)
#define nc getchar

using namespace std;
typedef long long LL;
typedef double db;

/*
inline nc(){
	static buff[100000],p1=*buff,p2=*buff;
	return p1==p2&&(p2=(p1=buff)+fread(buff,1,100000,stdin):p1==p2?EOF:*p1++);
}*/

inline void read(int &data){
	data=0;int e=1;char ch=nc();
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}
inline void read(LL &data){
	data=0;int e=1;char ch=nc();
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}

inline void write(int data){
	if (data<0)	putchar('-'),data=-data;
	if (data>9)	write(data/10);
	putchar(data%10+48);
}
const int maxn=1e5+5,maxm=2e5+5;
struct graph{
	int nxt,v,w;
}g[maxm];
struct str1{
	int x,w;
	str1 () {}
	str1 (int a,int b):x(a),w(b){}
	friend bool operator <(const str1 a,const str1 b){
		return a.w>b.w;
	}
};
int n,m,K,p,tot=0;
int h[maxn],ck[maxn],f[maxn],k[maxn][51];
bool v[maxn];
void add(int u,int v,int w){
	g[++tot].nxt=h[u],g[tot].v=v,g[tot].w=w,h[u]=tot;
}
void spfa(int u){
	ck[1]=1;
	rep(i,2,n)	f[i]=Inf;f[1]=0;
	memset(v,true,sizeof(v));v[1]=false;
	int l,r;l=r=1;
	while (l<=r){
		int u=ck[l];
		reg(i,g,u){
			if (f[u]+g[i].w<f[g[i].v]){
				f[g[i].v]=f[u]+g[i].w;
				if (v[g[i].v])
					ck[++r]=g[i].v;
			}
		}
		++l;
	}
}
priority_queue<str1> q;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;read(T);while (T--){
		read(n),read(m),read(K),read(p);
		int x,y,z;
		bool t=false;
		rep(i,1,m){
			read(x),read(y),read(z);
			add(x,y,z);
			if (z==0)	t=true;
		}
		if (t){
			puts("-1");continue;
		}
		spfa(1);
		while (!q.empty())	q.pop();
		rep(i,1,n)	rep(j,0,K)
			k[i][j]=-1;
		k[1][0]=1;
		str1 a;
		q.push(str1(1,0));
		int u,w,G;
		while (!q.empty()){
			a=q.top();q.pop();
			u=a.x,w=a.w;
			reg(i,g,u)
				if (g[i].w+w<=K+f[g[i].v]){
					G=g[i].w+w-f[g[i].v];
					if (k[g[i].v][G]==-1)
						q.push(str1(g[i].v,g[i].w+w)),k[g[i].v][G]=0;
					k[g[i].v][G]+=k[u][w-f[u]];
					if (k[g[i].v][G]>=p)	k[g[i].v][G]-=p;
				}
		}
		LL ans=0;
		rep(i,0,K){
			if (k[n][i]==-1)	continue;
			ans+=k[n][i];
			if (ans>=p)	ans-=p;
		}
		write(ans),puts("");
	}
	return 0;
}

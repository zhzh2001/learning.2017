#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>

#define rep(i,a,b)	for(int i=a;i<=b;i++)
#define rev(i,a,b)	for(int i=a;i>=b;i++)
#define reg(i,g,u)	for(int i=h[u];i;i=g[i].nxt)
#define nc getchar

using namespace std;
typedef long long LL;
typedef double db;

/*
inline nc(){
	static buff[100000],p1=*buff,p2=*buff;
	return p1==p2&&(p2=(p1=buff)+fread(buff,1,100000,stdin):p1==p2?EOF:*p1++);
}*/

inline void read(int &data){
	data=0;int e=1;char ch=nc();
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}
inline void read(LL &data){
	data=0;int e=1;char ch=nc();
	for(;(ch<'0'||ch>'9')&&ch!='-';ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}

inline void write(int data){
	if (data<0)	putchar('-'),data=-data;
	if (data>9)	write(data/10);
	putchar(data%10+48);
}

inline void write(LL data){
	if (data<0)	putchar('-'),data=-data;
	if (data>9)	write(data/10);
	putchar(data%10+48);
}

LL get(LL a,LL b){
	LL i=1;LL k=a,G,g,t;
	if (a==1)	return 1;
	while (k!=1){
		g=(b-k-1)/a+1,G=(k+a*g)-b;
//		i+=g,k=G;
		if (G>k)
			t=(b-k-1)/(G-k)+1,k=k+t*(G-k)-b,i=i+t*g;
		else t=(k-2)/(k-G)+1,k=k-t*(k-G),i=i+t*g;
	}
	return i%b;
}

LL n,m,a1,b1,a2,b2;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(n),read(m);
	if (n>m)	swap(n,m);
	if (m>=100000000&&n>=100000000&&(m-n>=200000000||m-n<=50)){
		a1=get(n,m),b1=(a1*n-1)/m;
		b2=get(m%n,n),a2=(b2*m-1)/n;
	}
	else{		
		if (n>m)	swap(n,m);
		rep(i,1,m)
			if (i*n%m==1){
				a1=i,b1=i*n/m;break;
			}
		rep(i,m-n,n)
			if (i*m%n==1){
				b2=i,a2=i*m/n;break;
			}	
	}
	write(min(a1*n+b2*m,a2*n+b1*m)-n-m+1),puts("");
	return 0;				
}

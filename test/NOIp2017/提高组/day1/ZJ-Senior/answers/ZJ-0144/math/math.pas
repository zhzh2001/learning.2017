var a,b:qword;

procedure in_;
begin
	assign(input,'math.in');
	reset(input);
	assign(output,'math.out');
	rewrite(output);
end;

procedure out_;
begin
	close(input);
	close(output);
end;

begin
	in_;
        read(a,b);
        write(a*b-a-b);
	out_;
end.
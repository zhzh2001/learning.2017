#include <cstdio>
#include <cstring>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;
const int N = 100005;
const int M = 200005;

int T, n, m, p, k, d, dmax, ans;
int Q[N << 2], in[N], cnt[N];
long long dis[N];
bool vis[N], useable[M];

namespace graph
{
	int cp = -1;
	int head[N], to[M], nxt[M], w[M];
	void addline(int u, int v, int val)
	{
		to[++cp] = v; nxt[cp] = head[u]; head[u] = cp; w[cp] = val;
	}
}
using namespace graph;

int spfa()
{
	int f = 1, l = 1, rt;
	memset(dis, 0x3f, sizeof(dis));
	memset(vis, false, sizeof(vis));
	Q[1] = 1;
	vis[1] = true;
	dis[1] = 0;
	while(f <= l)
	{
		rt = Q[f++];
		vis[rt] = false;
		for(int i = head[rt]; i != -1; i = nxt[i])
		{
			if(dis[to[i]] > dis[rt] + w[i])
			{
				dis[to[i]] = dis[rt] + w[i];
				if(!vis[to[i]])
				{
					Q[++l] = to[i];
					vis[to[i]] = true;
				}
			}
		}
	}
	return dis[n];
}

void dfs(int x, int dis)
{
	if(dis > dmax) return;
	if(x == n)
		ans = (ans + 1) % p;
	for(int i = head[x]; i != -1; i = nxt[i])
		dfs(to[i], dis + w[i]);
}

int calc()
{
	int f = 1, l = 1, rt;
	memset(in, 0, sizeof(in));
	memset(cnt, 0, sizeof(cnt));
	memset(vis, false, sizeof(vis));
	memset(useable, false, sizeof(useable));
	Q[1] = 1;
	vis[1] = true;
	while(f <= l)
	{
		rt = Q[f++];
		for(int i = head[rt]; i != -1; i = nxt[i])
		{
			if(dis[to[i]] == dis[rt] + w[i])
			{
				useable[i] = true;
				in[to[i]]++;
				if(!vis[to[i]])
				{
					Q[++l] = to[i];
					vis[to[i]] = true;
				}
			}
		}
	}
	f = 1; l = 0;
	rep(i, 1, n) if(in[i] == 0)
		Q[++l] = i;
	cnt[1] = 1;
	while(f <= l)
	{
		rt = Q[f++];
		for(int i = head[rt]; i != -1; i = nxt[i])
		if(useable[i]) {
			cnt[to[i]] = (cnt[to[i]] + cnt[rt]) % p;
			if(--in[to[i]] == 0)
				Q[++l] = to[i];
		}
	}
	return cnt[n];
}

void init()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d", &T);
	while(T--)
	{
		cp = 1;
		memset(head, -1, sizeof(head));
		scanf("%d%d%d%d", &n, &m, &k, &p);
		rep(i, 1, m)
		{
			int a, b, c;
			scanf("%d%d%d", &a, &b, &c);
			addline(a, b, c);
		}
		d = spfa();
		dmax = d + k;
		ans = 0;
		if(n <= 5 && m <= 10)
		{
			dfs(1, 0);
			printf("%d\n", ans);
		}
		else if(k == 0)
			printf("%d\n", calc());
	}
	return 0;
}

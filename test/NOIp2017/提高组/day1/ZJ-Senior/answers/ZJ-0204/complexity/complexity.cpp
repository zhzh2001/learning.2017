#include <cstdio>
#include <cctype>
#include <cstring>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;

int T, L, w, tail, ans;
char Comple[20], ope;
bool vis[200], flag;
struct stg
{
	char syb;
	int w, step;
}stk[200];

void work()
{
	int pos = 1;
	while(isspace(Comple[pos]) || Comple[pos] == 'O' || Comple[pos] == '(')
		pos++;
	if(Comple[pos] == '1') w = 0;
	else
	{
		while(Comple[pos] == 'n' || Comple[pos] == '^' || isspace(Comple[pos])) pos++;
		w = 0;
		while(isdigit(Comple[pos]))
			w = w * 10 + Comple[pos++] - '0';
	}
}

void work_f()
{
	char ch = getchar();
	while(isspace(ch)) ch = getchar();
	stk[++tail].syb = ch; ch = getchar(); 
	while(isspace(ch)) ch = getchar();
	int x = 0, y = 0;
	if(ch == 'n') x = -1, ch = getchar();
	else while(isdigit(ch)) x = x * 10 + ch - '0', ch = getchar();
	while(isspace(ch)) ch = getchar();
	if(ch == 'n') y = -1, ch = getchar();
	else while(isdigit(ch)) y = y * 10 + ch - '0', ch = getchar();
	if(x == -1 && y == -1) stk[tail].w = 0; // n ~ n
	if(x == -1 && y != -1) stk[tail].w = -1; // n ~ 1
	if(x != -1 && y == -1) stk[tail].w = 1; // 1 ~ n
	if(x != -1 && y != -1)
	{
		if(y >= x)
			stk[tail].w = 0; // 1 ~ 1
		else stk[tail].w = -1;
	}
	if(stk[tail - 1].step == -1) stk[tail].step = -1;
	else if(stk[tail].w != -1) stk[tail].step = stk[tail - 1].step + stk[tail].w;
	else stk[tail].step = -1;
	if(vis[stk[tail].syb - 'a' + 1]) flag = false;
	else vis[stk[tail].syb - 'a' + 1] = true;
	if(stk[tail].step > ans) ans = stk[tail].step;
}

void work_e()
{
	if(tail == 0)
	{
		flag = false;
		return;
	}
	vis[stk[tail].syb - 'a' + 1] = false;
	tail--;
}

void read(char &ch)
{
	ch = getchar();
	while(isspace(ch)) ch = getchar();
}

void init()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d", &L);
		scanf("%s", Comple + 1);
		work();
		memset(vis, false, sizeof(vis));
		stk[tail = 0].step = 0;
		ans = -1;
		flag = true;
		rep(i, 1, L)
		{
			read(ope);
			if(ope == 'F') work_f();
			else work_e();
		}
		if(tail != 0) flag = false;
		if(flag == false) puts("ERR");
		else
		{
			if(ans == -1) ans = 0;
			if(ans == w) puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

#include <cstdio>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;

int a, b;

void init()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d%d", &a, &b);
	printf("%lld\n", 1ll * (a - 1) * (b - 1) - 1);
	return 0;
}

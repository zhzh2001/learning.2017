#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=1e5+5;
struct cs{int to,nxt,v;}a[N*4];
struct go{int x,y;};
int head[N],ll,d[N],v[N][51],sz[N];
bool in[N],vi[N][51];
int n,m,x,y,z,t,ans,mo,K;
void init(int x,int y,int z){
	a[++ll].to=y;
	a[ll].v=z;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void spfa(){
	memset(d,63,sizeof d);
	queue<int>Q;Q.push(1);d[1]=0;
	while(!Q.empty()){
		int x=Q.front();Q.pop();in[x]=0;
		for(int k=head[x];k;k=a[k].nxt)
			if(d[a[k].to]>d[x]+a[k].v){
				d[a[k].to]=d[x]+a[k].v;
				if(!in[a[k].to])Q.push(a[k].to),in[a[k].to]=1;
			}
	}
}
void SPFA(){
	ans=0;queue<go>Q;
	memset(v,0,sizeof v);
	memset(sz,0,sizeof sz);
	memset(vi,0,sizeof vi);
	v[1][0]=1%mo;Q.push({1,0});
	while(!Q.empty()){
		int x=Q.front().x;
		int y=Q.front().y;
		Q.pop();vi[x][y]=0;
		if(++sz[x]>n){WW(-1);return;}
		for(int k=head[x];k;k=a[k].nxt)
			if(d[x]+y-d[a[k].to]+a[k].v<=K){
				int xx=a[k].to,yy=d[x]+y-d[a[k].to]+a[k].v;
				v[xx][yy]=(v[xx][yy]+v[x][y])%mo;
				if(!vi[xx][yy])vi[xx][yy]=1,Q.push({xx,yy});
			}
		if(x==n)ans=(ans+v[x][y])%mo;
		v[x][y]=0;
	}WW(ans);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);	
	t=RR();
	while(t--){
		memset(head,0,sizeof head);ll=0;
		read(n,m);read(K,mo);
		for(int i=1;i<=m;i++){
			read(x,y);
			init(x,y,RR());
		}
		spfa();
		SPFA();
	}
}


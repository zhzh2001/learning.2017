#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(Ll &x,Ll &y){x=RR();y=RR();}

Ll a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(a,b);
	W(a*b-a-b);
}

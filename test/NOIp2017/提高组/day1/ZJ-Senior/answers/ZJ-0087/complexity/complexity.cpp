#include<cstdio>
#include<cstring>
using namespace std;

int l,a,f,top,te,t,zt,lst,ans,tot;
int q[105],num[105],h[105],w[105],s[105];
char ch,c;
bool vis[26];

int max(int x,int y){return x>y?x:y;}

int find(int x)
{
	int l=1,r=tot,mid;
	while (l<=r)
	{
		mid=(l+r)>>1;
		if (q[mid]<x) l=mid+1; else r=mid-1;
	}
	return l;
}

void work()
{
	scanf("%d",&l);
	ch=getchar();a=0;f=0;top=0;zt=0;lst=0;tot=0;ans=0;
	while (ch!='F'&&ch!='E')
	{
		if (ch=='n') f=1;
		if ('0'<=ch&&ch<='9') a=a*10+ch-48;
		ch=getchar();
	}
	a=a*f;
	for (int i=1;i<=l;i++)
	{
		if (ch=='F') {
			int x=0,y=0;
			ch=getchar();
			while (ch<'a'||ch>'z') ch=getchar();
			s[++top]=ch-'a';
			if (!vis[s[top]]) {
				vis[s[top]]=true;
				ch=getchar();
				while ((ch<'0'||ch>'9')&&ch!='n') ch=getchar();
				while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
				if (ch=='n') x=0;
				ch=getchar();
				while ((ch<'0'||ch>'9')&&ch!='n') ch=getchar();
				while ('0'<=ch&&ch<='9') y=y*10+ch-48,ch=getchar();
				if (ch=='n') y=0;
				if (x!=0&&y==0&&zt==0) w[i]=1;
				h[i]=h[i-1]+1;
				if (((x==0&&y!=0)||(x>y)&&x!=0&&y!=0)&&zt==0) zt=1,lst=top;
			}
			else {
				printf("ERR\n");
				int j=i;
				while (j<=l)
				{
					ch=getchar();
					while (ch!=13&&ch!=10) ch=getchar();
					j++;
				}
				return;
			}
		}
		if (ch=='E') {
			h[i]=h[i-1]-1;
			vis[s[top--]]=false;
			if (top==lst) zt=0,lst=0;
		}
		if (top<0) {
			printf("ERR\n");
			int j=i;
			while (j<=l)
			{
				ch=getchar();
				while (ch!=13&&ch!=10) ch=getchar();
				j++;
			}
			return;
		}
		if (i<l) {
			ch=getchar();
			while (ch!='F'&&ch!='E') ch=getchar();
		}
	}
	for (int i=1;i<=l;i++)
	{
		if (w[i]==1) {t=find(h[i]);if (t>tot) tot=t;q[t]=h[i];ans=max(ans,tot);}
		else {t=find(h[i]);tot=t-1;}
	}
	if (top>0) {printf("ERR\n");return;}
	else if (a==ans) {printf("Yes\n");return;}
	else {printf("No\n");return;}
	return;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&te);
	while (te--)
	{
		memset(vis,false,sizeof(vis));
		memset(w,0,sizeof(w));
		memset(h,0,sizeof(h));
		memset(q,0,sizeof(q));
		work();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

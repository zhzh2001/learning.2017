var
  cnt,tot,n,m,u,v,k,p,i,l,t,ans,mo:longint;
  b:array[0..1005] of boolean;
  e:array[0..1005] of longint;
  len,f:array[0..1005,0..1005] of longint;
  a:array[0..1000005] of longint;
procedure dfs(p:longint);
var i:longint;
begin
  if p=n then
  begin
    inc(cnt);
    a[cnt]:=l;
    exit;
  end;
  b[p]:=false;
  for i:=1 to e[p] do
    if b[f[p,i]] then
    begin
      l:=l+len[p,f[p,i]];
      dfs(f[p,i]);
      l:=l-len[p,f[p,i]];
    end;
  b[p]:=true;
end;
procedure sort(l,r:longint);
var i,j,x,y:longint;
begin
  i:=l;j:=r;x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i];a[i]:=a[j];a[j]:=y;
      inc(i);dec(j);
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  readln(t);
  while t>0 do
  begin
    cnt:=0;ans:=0;
    dec(t);
    readln(n,m,k,mo);
    fillchar(b,sizeof(b),true);
    fillchar(e,sizeof(e),0);
    for i:=1 to m do
    begin
      readln(u,v,p);
      inc(e[u]);
      f[u,e[u]]:=v;
      len[u,v]:=p;
    end;
    l:=0;
    dfs(1);
    sort(1,cnt);
    ans:=0;
    for i:=1 to cnt do
      if a[i]<=a[1]+k then inc(ans);
    writeln(ans mod mo);
  end;
  close(input);close(output);
end.
var
  t,l,num,sum,cnt,max,i,righti,k,num1,num2:longint;
  s:array['a'..'z'] of boolean;
  b:array[0..1005] of char;
  c:array[0..1005] of boolean;
  flag,err,right:boolean;ch:char;st,st1,st2:string;
begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(t);
  while t>0 do
  begin
    err:=false;
    dec(t);read(l);
    read(ch,ch,ch,ch);
    if ch='1' then
    begin
      readln(ch);
      flag:=true;
    end;
    if ch='n' then
    begin
      readln(ch,st);
      st:=copy(st,1,length(st)-1);
      val(st,num);
      flag:=false;
    end;
    sum:=0;cnt:=0;max:=0;right:=true;
    fillchar(s,sizeof(s),false);
    fillchar(c,sizeof(c),false);
    for i:=1 to l do
    begin
      if err then begin readln(st);continue; end;
      read(ch);
      if ch='F' then
      begin
        inc(sum);
        read(ch,ch);
        if s[ch] then err:=true else
          begin
            s[ch]:=true;
            b[sum]:=ch;
          end;
        read(ch);readln(st);
        if st[1]='n' then
        begin
          if st[length(st)]<>'n' then
          begin
            right:=false;
            righti:=sum;
          end;
          c[sum]:=false;
          continue;
        end else
        begin
          if st[length(st)]='n' then
          begin
            if right then
            begin
              inc(cnt);
              c[sum]:=true;
            end;
          end else
          begin
            k:=pos(' ',st);
            st1:=copy(st,1,k-1);
            val(st1,num1);
            st2:=copy(st,k+1,length(st)-k);
            val(st2,num2);
            if num1>num2 then
            begin
              right:=false;
              righti:=sum;
            end;
            c[sum]:=false;
            continue;
          end;
        end;
      end else
      begin
        readln;
        if sum=0 then
        begin
          err:=true;
          continue;
        end;
        s[b[sum]]:=false;
        dec(sum);
        if not right then
          if sum<righti then
            right:=true;
        if cnt>max then max:=cnt;
        if c[sum+1] then dec(cnt);
      end;
    end;
    if cnt>max then max:=cnt;
    if sum<>0 then err:=true;
    if err then
    begin
      writeln('ERR');
      continue;
    end else
    begin
      if ((max=0)and(flag))or((not flag)and(num=max)) then
        writeln('Yes') else writeln('No');
    end;
  end;
  close(input);close(output);
end.
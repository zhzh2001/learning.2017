var
  a,b,t:int64;
begin
  assign(input,'math.in');reset(input);
  assign(output,'math.out');rewrite(output);
  readln(a,b);
  if a>b then
  begin
    t:=a;a:=b;b:=t;
  end;
  writeln(b*(a-1)-a);
  close(input);close(output);
end.
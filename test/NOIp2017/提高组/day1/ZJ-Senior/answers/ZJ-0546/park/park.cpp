#include<bits/stdc++.h>
using namespace std;
//#define int ll
#define maxn 300005
#define forup(i,a,b) for(int i=(a);i<=(b);i++)
#define fordown(i,a,b)  for(int i=(a);i>=(b);i--)
#define pb push_back
#define INF 1000000005
typedef long long  ll;
int n,m,k,P;
struct Edge{int to,len;};vector<Edge> g[maxn],G[maxn];
queue<int> q;
ll dis[maxn];
ll N_dis[maxn];
bool vis[maxn];
map<ll,ll> dp[maxn];
void spfa(int st,ll dis[])
{
 forup(i,1,n) dis[i]=(ll)INF*100;
 dis[st]=0;
 q.push(st);
 while(!q.empty())
  {int x=q.front();q.pop();
   vis[x]=0;
   for(int i=0;i<g[x].size();i++)
   {int u=g[x][i].to,len=g[x][i].len;
    if(dis[u]>dis[x]+len)
     {dis[u]=dis[x]+len;
      if(vis[u]==0)
       {vis[u]=1; q.push(u);}
	 }
   }
  }
}
void N_spfa(int st,ll dis[])
{
 forup(i,1,n) dis[i]=(ll)INF*100;
 dis[st]=0;
 q.push(st);
 while(!q.empty())
  {int x=q.front();q.pop();
   vis[x]=0;
   for(int i=0;i<G[x].size();i++)
   {int u=G[x][i].to,len=G[x][i].len;
    if(dis[u]>dis[x]+len)
     {dis[u]=dis[x]+len;
      if(vis[u]==0)
       {vis[u]=1; q.push(u);}
	 }
   }
  }
 
}
int cnt=0;
bool is_cir;
ll dfs(int x,ll lef)
{ //cout<<x<<endl;
  if(N_dis[x]>lef) return 0;
  if(dp[x].count(lef)!=0)  return dp[x][lef];
  if(lef==0) 
   {if(x==n) return 1;}
  ll res=0;
  for(int i=0;i<g[x].size();i++)
  {int u=g[x][i].to,len=g[x][i].len;
   int now_lef=lef-len;
   if(now_lef>=0)  res=(res+dfs(u,now_lef))%P;
  }
 dp[x][lef]=res;
 return res;
}
void clearall(){forup(i,1,n) {dp[i].clear();g[i].clear();G[i].clear();}}

int main()
{freopen("park.in","r",stdin);
 freopen("park.out","w",stdout); 
 int T;
 cin>>T;
 while(T--)
 {cnt=0;
  cin>>n>>m>>k>>P;
  forup(i,1,m)	 {int x,y,z;scanf("%d %d %d",&x,&y,&z);g[x].pb((Edge){y,z}); G[y].pb((Edge){x,z});}
  spfa(1,dis);
  N_spfa(n,N_dis);
  ll ans=0ll;
  for(ll i=dis[n];i<=dis[n]+k;i++)
   ans=(ans+dfs(1,i))%P;
   cout<<ans<<endl;
   clearall();
 }
 fclose(stdin);fclose(stdout);
	return 0;
}

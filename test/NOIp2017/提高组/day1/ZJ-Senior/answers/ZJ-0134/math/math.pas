Var
  a,b,t,t1,t2:int64;
  i,j:longint;
  bf:array[0..8000000] of longint;
begin
  assign(input,'math.in');
  reset(input);
  assign(output,'math.out');
  rewrite(output);
  readln(a,b);
  for i:=0 to b do
    begin
      for j:=0 to a do
        begin
          t:=a*i+b*j;
          if t>=a*b then
            break;
          t1:=t div 32;
          t2:=t mod 32;
          bf[t1]:=bf[t1] or (1<<t2);
        end;
    end;
  for i:=a*b-1 downto 1 do
    begin
      t1:=i div 32;
      t2:=i mod 32;
      if (bf[t1] and (1<<t2))=0 then
        begin
          writeln(i);
          close(input);
          close(output);
          exit;
        end;
    end;
  close(input);
  close(output);
end.

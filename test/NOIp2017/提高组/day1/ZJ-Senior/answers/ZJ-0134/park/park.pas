Const
  null=99999999;
Var
  ck,k,n,m,lim,u,v,w,i,mdis,ans,mor:longint;
  num:longint;
  head:array[0..120000] of longint;
  next,tar,cost:array[0..240000] of longint;
  dis:array[0..120000] of longint;
  inq:array[0..120000] of boolean;
  qn:array[0..120000] of longint;
  q:array[0..1200000] of longint;
  flag:array[0..120000] of longint;
  tec:array[0..120000,0..200] of longint;
  killed:boolean;
procedure addedge(u,v,w:longint);
begin
  inc(num);
  next[num]:=head[u];
  head[u]:=num;
  tar[num]:=v;
  cost[num]:=w;
end;
function spfa:longint;
Var
  i,qh,qt,u,v,e:longint;
begin
  for i:=1 to n do
    begin
      dis[i]:=null;
      inq[i]:=false;
      qn[i]:=0;
    end;
  dis[1]:=0;
  qh:=0;
  qt:=1;
  q[qt]:=1;
  while qh<qt do
    begin
      inc(qh);
      u:=q[qh];
      e:=head[u];
      while e<>0 do
        begin
          v:=tar[e];
          if dis[u]+cost[e]<dis[v] then
            begin
              dis[v]:=dis[u]+cost[e];
              if not inq[v] then
                begin
                  inq[v]:=true;
                  inc(qn[v]);
                  inc(qt);
                  q[qt]:=v;
                end;
            end;
          e:=next[e];
        end;
    end;
  exit(dis[n]);
end;
procedure dfs(u,cc:longint);
Var
  e,v:longint;
begin
  if cc>mdis+lim then
    exit;
  if u=n then
    begin
      inc(ans);
      ans:=ans mod mor;
    end;
  if (flag[u]>0) and (cc=tec[u,flag[u]]) then
    begin
      killed:=true;
      exit;
    end;
  inc(flag[u]);
  tec[u,flag[u]]:=cc;
  e:=head[u];
  while e<>0 do
    begin
      v:=tar[e];
      dfs(v,cc+cost[e]);
      if killed then
        exit;
      e:=next[e];
    end;
  tec[u,flag[u]]:=0;
  dec(flag[u]);
end;
begin
  assign(input,'park.in');
  reset(input);
  assign(output,'park.out');
  rewrite(output);
  readln(ck);
  for k:=1 to ck do
    begin
      readln(n,m,lim,mor);
      num:=0;
      fillchar(head,sizeof(head),0);
      fillchar(next,sizeof(next),0);
      for i:=1 to m do
        begin
          readln(u,v,w);
          addedge(u,v,w);
        end;
      mdis:=spfa;
      ans:=0;
      killed:=false;
      fillchar(flag,sizeof(flag),0);
      fillchar(tec,sizeof(tec),0);
      dfs(1,0);
      if not killed then
        writeln(ans)
        else
          writeln(-1);
    end;
  close(input);
  close(output);
end.

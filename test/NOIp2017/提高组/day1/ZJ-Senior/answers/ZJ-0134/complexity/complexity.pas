Var
  ck,k,i,ans,stq:longint;
  s,cx:string;
  den:longint;
  de:array[0..10] of string;
  n:longint;
  pg:array[0..120] of string;
  syt:array[0..120] of string;
  ty:array[0..120] of longint;
procedure depart(s:string);
Var
  i:longint;
  ts:string;
begin
  den:=0;
  ts:='';
  for i:=1 to length(s) do
    begin
      if s[i]=' ' then
        begin
          inc(den);
          de[den]:=ts;
          ts:='';
        end
        else
          ts:=ts+s[i];
    end;
  if length(ts)>0 then
    begin
      inc(den);
      de[den]:=ts;
    end;
end;
function getint(s:string):longint;
Var
  t:longint;
begin
  if s='n' then
    t:=9999
    else
      val(s,t);
  exit(t);
end;
function gettop:longint;
Var
  i,j,d,p1,p2,top,c,und:longint;
begin
  d:=0;
  top:=0;
  c:=0;
  und:=-1;
  for i:=1 to n do
    begin
      depart(pg[i]);
      if de[1]='F' then
        begin
          if den<>4 then
            exit(-1);
          for j:=1 to d do
            begin
              if syt[j]=de[2] then
                exit(-1);
            end;
          inc(d);
          syt[d]:=de[2];
          p1:=getint(de[3]);
          p2:=getint(de[4]);
          if p2-p1<0 then
            begin
              ty[d]:=-1;
              if und=-1 then
                und:=d;
            end
            else
              if p2-p1>666 then
                begin
                  ty[d]:=1;
                  if und=-1 then
                    begin
                      inc(c);
                      if c>top then
                        top:=c;
                    end;
                end
                else
                  begin
                    ty[d]:=0;
                  end;

        end
        else
          if de[1]='E' then
            begin
              if den<>1 then
                exit(-1);
              if (und=-1) and (ty[d]=1) then
                dec(c);
              if d=und then
                und:=-1;
              dec(d);
              if d<0 then
                exit(-1);
            end
            else
              begin
                exit(-1);
              end;
    end;
  if d=0 then
    exit(top)
    else
      exit(-1);
end;
begin
  assign(input,'complexity.in');
  reset(input);
  assign(output,'complexity.out');
  rewrite(output);
  readln(ck);
  for k:=1 to ck do
    begin
      readln(s);
      val(copy(s,1,pos(' ',s)-1),n);
      cx:=copy(s,pos('(',s)+1,pos(')',s)-pos('(',s)-1);
      for i:=1 to n do
        readln(pg[i]);
      stq:=gettop;
      if stq=-1 then
        begin
          writeln('ERR');
          continue;
        end;
      if cx='1' then
        ans:=0
        else
          val(copy(cx,pos('^',cx)+1,length(cx)-pos('^',cx)),ans);
      if stq=ans then
        writeln('Yes')
        else
          writeln('No');
    end;
  close(input);
  close(output);
end.

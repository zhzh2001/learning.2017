#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,m,k,P,next[1000000],head[1000000],vet[1000000],val[1000000],num,minsum,ans,sub;
bool flag[1000000];
void add_edge(int u,int v,int t){
	next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
	val[num]=t;
}
void dfs1(int u,int sum){
//	printf("%d   %d\n",u,sum);
	if (u==n){
	  if (sum<minsum) minsum=sum;}
	for (int e=head[u];e;e=next[e]){
		int v=vet[e];
		if (flag[v]==false){
			flag[v]=true;
			dfs1(v,sum+val[e]);
			flag[v]=false;
		}
	}
	return ;
}
void dfs(int u,int sum){
//	printf("%d   %d\n",u,sum);
	if (u==n)
	  if (sum<=minsum+k) ans++;
	for (int e=head[u];e;e=next[e]){
		int v=vet[e];
		if (flag[v]==false){
			flag[v]=true;
			dfs(v,sum+val[e]);
			flag[v]=false;
		}
	}
	return ;
}
void solve1(){
	sub=0;
   // for (int i=1;i<=n;i++) flag[i]=false;
	minsum=1000000000;
	flag[1]=true;
	dfs1(1,0);
		memset(flag,false,sizeof(flag));
		flag[1]=true;
	ans=0;
	dfs(1,0);
	if (ans!=1)
	printf("%d\n",ans%P);else
	printf("-1\n");
	return ;
}
/*void bfs1(int st){
	memset(flag,false,sizeof(flag));
	int t=1,w=1;
	int q[2000][2000];
	memset(q,1000000000,sizeof(q));
	q[1]=0;
	while(t<w) {
		int p=q[t];
		t++;
		for (int e=head[p];e;e=next[e])
		if (flag[vet[e]]==false)
		{
			int v=vet[e];
			flag[v]=true;
			int np=p+val[e];
			w++;
			q[w]=np;
		}
	}
	for (int i=2;i<=n;i++)
	  if (q[i]<ans) ans=q[i];
}
void solve2(){
	bfs1(1);
	ans=1000000000;
	printf("%d\n",ans);
	/*
	if (ans!=1)
	printf("%d\n",ans%P);else
	printf("-1\n");
	return ;
}*/
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		num=0;
		memset(next,0,sizeof(next));
		memset(head,0,sizeof(head));
		memset(vet,0,sizeof(vet));
		memset(flag,false,sizeof(flag));
		scanf("%d%d%d%d",&n,&m,&k,&P);
	for (int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add_edge(x,y,z);
	}
	//	if (n<=5&&m<=10) 
			solve1();
	//	if (n<=1000&&m<=2000) 
//	solve2();
	}
	return 0;
}

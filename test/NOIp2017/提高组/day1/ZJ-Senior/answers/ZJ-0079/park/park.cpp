#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<iostream>
using namespace std;
inline void judge(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
}
int n,m,k,mod;
struct edge{
	int s,t,v,nxt,nxt1;
}e[200005];
int e_cnt,last[100005],last1[100005];
inline void ins(int a,int b,int c){
	e[e_cnt]=(edge){a,b,c,last[a],last1[b]};
	last[a]=last1[b]=e_cnt++;
}
struct info{
	int a,b;
	bool operator <(const info y)const{
		return a>y.a||(a==y.a&&b>y.b);
	}
};
inline info makei(int x,int y){return (info){x,y};}
struct heap{
	info h[5000005];int cnt;
	inline void push(info x){
		h[++cnt]=x;
		int p=cnt;
		while(p>1){
			if(h[p>>1]<h[p])swap(h[p],h[p>>1]),p>>=1;
			else break;
		}
	}
	inline info top(){
		return h[1];
	}
	inline void pop(){
		swap(h[1],h[cnt]);
		cnt--;
		int p=1;
		while((p<<1)<=cnt){
			int t;
			if(((p<<1)|1)>cnt||h[(p<<1)|1]<h[p<<1])t=(p<<1);
			else t=((p<<1)|1);
			if(h[p]<h[t])swap(h[t],h[p]),p=t;
			else break;
		}
	}
	bool empty(){
		return !cnt;
	}
	void clear(){cnt=0;}
}he;
int dis[100005];
void dij(){
	memset(dis,63,sizeof(dis));
	he.clear();
	dis[1]=0;
	he.push(makei(0,1));
	for(int i=1;i<=n&&!he.empty();i++){
		info p=he.top(); he.pop();
		while(dis[p.b]<p.a){
			if(he.empty())break;
			p=he.top();he.pop();
		}
		int t=p.b;
		if(dis[t]<p.a)break;
		for(int j=last[t];j!=-1;j=e[j].nxt){
			if(dis[t]+e[j].v<dis[e[j].t]){
				dis[e[j].t]=dis[t]+e[j].v;
				he.push(makei(dis[e[j].t],e[j].t));
			}
		}
	}
}
int dis1[100005];
void dij1(){
	memset(dis1,63,sizeof(dis1));
	he.clear();
	dis1[n]=0;
	he.push(makei(0,n));
	for(int i=1;i<=n&&!he.empty();i++){
		info p=he.top(); he.pop();
		while(dis1[p.b]<p.a){
			if(he.empty())break;
			p=he.top(); he.pop();
		}
		int t=p.b;
		if(dis1[t]<p.a)break;
		for(int j=last1[t];j!=-1;j=e[j].nxt1)
			if(dis1[t]+e[j].v<dis1[e[j].s]){
				dis1[e[j].s]=dis1[t]+e[j].v;
				he.push(makei(dis1[e[j].s],e[j].s));
			}
	}
}
bool err[100005];
int low[100005],dfn[100005],tot,st[100005];
bool inst[100005],vis[100005];
void tarjan(int x){
	low[x]=dfn[x]=++tot;
	st[++st[0]]=x; inst[x]=vis[x]=1;
	for(int i=last[x];i!=-1;i=e[i].nxt)if(!e[i].v){
		if(!vis[e[i].t]){
			tarjan(e[i].t);
			low[x]=min(low[x],low[e[i].t]);
		}else if(inst[e[i].t])low[x]=min(low[x],dfn[e[i].t]);
	}
	if(dfn[x]==low[x]){
		if(st[st[0]]!=x){
			while(st[st[0]]!=x){
				inst[st[st[0]]]=0;
				err[st[st[0]--]]=1;
			}
			inst[st[st[0]]]=0;
			err[st[st[0]--]]=1;
		}else inst[st[st[0]--]]=0;
	}
}
int h[100005];
int q[100005],id[100005];
int tpsort(){
	int head=0,tail=0;
	for(int i=1;i<=n;i++)
		if(!err[i]&&!h[i])q[++tail]=i;
	while(head<tail){
		head++;
		for(int i=last[q[head]];i!=-1;i=e[i].nxt)
			if(!e[i].v&&!err[e[i].t]){
				h[e[i].t]--;
				if(!h[e[i].t])q[++tail]=e[i].t;		
			}
	}
	for(int i=1;i<=tail;i++)id[q[i]]=i;
	return tail;
}
int f[100005][55];
bool vis0[100005][55];
void dij2(int num){
	memset(f,0,sizeof(f));
	memset(vis0,0,sizeof(vis0));
	he.clear();
	f[1][0]=1;
	he.push(makei(0,id[1]));
	he.empty();
	for(int i=1;i<=num*k&&!he.empty();i++){
		info p=he.top(); he.pop();
		while(vis0[q[p.b]][p.a-dis[q[p.b]]]){
			if(he.empty())break;
			p=he.top(); he.pop();
		}
		int t=q[p.b];
		if(vis0[t][p.a-dis[t]])break;
		vis0[t][p.a-dis[t]]=1;
		for(int j=last[t];j!=-1;j=e[j].nxt)if(!err[e[j].t])
			if(p.a+e[j].v+dis1[e[j].t]<=dis[n]+k){
				(f[e[j].t][p.a+e[j].v-dis[e[j].t]]+=f[t][p.a-dis[t]])%=mod;
				he.push(makei(p.a+e[j].v,id[e[j].t]));
			}
	}
}
int main(){
	judge();
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&mod);
		memset(last1,-1,sizeof(last1));
		memset(last,-1,sizeof(last)); e_cnt=0;
		for(int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			ins(u,v,w);
		}
		dij(); dij1();
		memset(vis,0,sizeof(vis));
		memset(err,0,sizeof(err));
		memset(low,0,sizeof(low));
		memset(dfn,0,sizeof(dfn));
		memset(inst,0,sizeof(inst));
		for(int i=1;i<=n;i++)if(!vis[i])tarjan(i);
		bool ERR=0;
		for(int i=1;i<=n;i++)if(err[i]&&dis[i]+dis1[i]<=dis[n]+k){ERR=1; break;}
		if(ERR){puts("-1"); continue;}
		for(int i=1;i<=n;i++)if(!err[i])
			for(int j=last[i];j!=-1;j=e[j].nxt)if(!err[e[j].t]&&!e[j].v)h[e[j].t]++;
		int num=tpsort();
		dij2(num);
		int ans=0;
		for(int i=0;i<=k;i++)(ans+=f[n][i])%=mod;
		printf("%d\n",ans);
	}
	return 0;
}

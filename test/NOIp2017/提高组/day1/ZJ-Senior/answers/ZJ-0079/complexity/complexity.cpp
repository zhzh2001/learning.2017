#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
}
struct info{
	int c,x;
}st[100005];
int top=0;
bool ocur[26];
char s[105];
int main(){
	judge();
	int T;
	scanf("%d",&T);
	while(T--){
		int n;
		scanf("%d",&n); scanf("%s",s);
		int v=0;
		if(s[2]=='n'){
			int l=strlen(s);
			for(int j=0;j<l;j++)if(s[j]>='0'&&s[j]<='9')v=v*10+s[j]-'0';
		}
		int num=0,res=0;
		bool err=0;
		top=0;
		memset(ocur,0,sizeof(ocur));
		for(int i=0;i<n;i++){
			scanf("%s",s);
			if(s[0]=='E'){
				if(err)continue;
				if(top){
					num-=st[top].x;
					ocur[st[top].c]=0;
					top--;
				}else err=1;
			}else{
				info t;
				scanf("%s",s);
				t.c=s[0]-'a';
				if(!ocur[t.c])ocur[t.c]=1;
				else err=1;
				scanf("%s",s);
				int b=0,e=0;
				if(s[0]=='n')b=1000;
				else{
					int l=strlen(s);
					for(int j=0;j<l;j++)b=b*10+s[j]-'0';
				}
				scanf("%s",s);
				if(s[0]=='n')e=1000;
				else{
					int l=strlen(s);
					for(int j=0;j<l;j++)e=e*10+s[j]-'0';
				}
				if(err)continue;
				if(e<b)t.x=-100000;
				else if(e-b<=100)t.x=0;
					 else t.x=1;
				st[++top]=t;
				num=num+t.x;
				res=max(res,num);
			}
		}
		if(err||top>0)puts("ERR");
		else if(res==v)puts("Yes");
			 else puts("No");
	}
	return 0;
}

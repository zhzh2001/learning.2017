#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <string.h>
#include <fstream>
#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
using namespace std;

string st, st1, st2;

pair< pair< int, int >, pair< char, bool > > vec[110];
bool f[110];

char readchar() {
	char ch = getchar();
	while (ch == ' ' || ch == '\n' || ch == '\r') ch = getchar();
	return ch;
}

string readstring() {
	char ch = getchar();
	string ans = "";
	while (ch == ' ' || ch == '\n' || ch == '\r') ch = getchar();
	while (ch != ' ' && ch != '\n' && ch != '\r') ans += ch, ch = getchar();
	return ans;
}

int main() {
	
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	
	int T; scanf("%d", &T);
	while (T --) {
		memset(f, 0, sizeof f);
		int flag = 0, top = 0;
		int n, temp = 0, now = 0, Check = 0; scanf("%d", &n);
		st = readstring();
		if (st[2] == 'n') 
			for (int i = 4; i < (int)st.length() - 1; ++i)
				(Check *= 10) += (st[i] - '0');
		for (int i = 1; i <= n; ++i) {
//			cerr << i << " " << n << endl;
			char ch, ii; ch = readchar();
			if (ch == 'E') {
				if (top == 0) {
					flag = -1;
					continue;
				}
				if (vec[top].sc.sc == 1) now = max(now, temp);
				if (vec[top].ft.sc == -1 && vec[top].ft.ft != -1) temp --;
				f[vec[top].sc.ft - 'a'] = 0;
				top --;
			}
			else if (ch == 'F') {
				ii = readchar();
				if (f[ii - 'a']) flag = -1;
				f[ii - 'a'] = 1;
				st1 = readstring();
				st2 = readstring();
				int x = 0, y = 0;
				bool bo = 1;
				if (st1 == "n") x = -1;
				else for (int j = 0; j < (int)st1.length(); ++j) (x *= 10) += (st1[j] - '0');
				if (st2 == "n") y = -1;
				else for (int j = 0; j < (int)st2.length(); ++j) (y *= 10) += (st2[j] - '0');
				if (x != -1 && y != -1 && x > y) bo = 0;
				if (x == -1 && y != -1) bo = 0;
				if (top > 0 && vec[top].sc.sc == 0) bo = 0;
				vec[++top] = (mp(mp(x, y), mp(ii, bo)));
				if (y == -1 && x != -1) temp ++;
				//cerr << i << " " << x << " " << y << " " << temp << " " << bo << " " << vec[top].sc.sc << endl;
			}
//			cerr << ch << " " << ii << " " << st1 << " " << st2 << endl;
		}
		if (top != 0) flag = -1;
		if (flag == 0 && now == Check) flag = 1;
		if (flag == 1) printf("Yes\n");
		else if (flag == 0) printf("No\n");
		else printf("ERR\n");
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
	
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
using namespace std;

const int Inf = 1e9 + 7;
int n, m, k, p;
		
int fst[100010], nxt[200010], lst[100010], des[200010], val[200010];
int fst2[100010], nxt2[200010], lst2[100010], des2[200010], val2[200010];
bool vis[100010];
int tot[100010], q[2000010], dis[100010], cnt = 0, cnt2 = 0, dist;
pair<int, int> w[10000010];

inline int read() {
	int x = 0; char ch = getchar();
	while (ch == ' ' || ch == '\n' || ch == '\r') ch = getchar();
	while (ch != ' ' && ch != '\n' && ch != '\r') (x *= 10) += (ch - '0'), ch = getchar();
	return x;
}

inline void add(int u, int v, int vv) {
	if (!fst[u]) fst[u] = ++cnt;
	else nxt[lst[u]] = ++cnt;
	lst[u] = cnt, des[cnt] = v, val[cnt] = vv;
}

inline void add2(int u, int v, int vv) {
	if (!fst2[u]) fst2[u] = ++cnt2;
	else nxt2[lst2[u]] = ++cnt2;
	lst2[u] = cnt2, des2[cnt2] = v, val2[cnt2] = vv;
}			

inline int spfa() {
	
	memset(vis, 0, sizeof vis);
	memset(tot, 0, sizeof tot);
	for (int i = 1; i <= n; ++i) dis[i] = Inf;
	int l = 1, r = 1; q[l] = 1, vis[1] = 1, dis[1] = 0;
	while (l <= r) {
		for (int i = fst[q[l]]; i; i = nxt[i]) {
			if (dis[q[l]] + val[i] <= dis[des[i]]) {
				dis[des[i]] = dis[q[l]] + val[i];
				if (!vis[des[i]]) {
					vis[des[i]] = 1; tot[des[i]] ++;
					if (tot[des[i]] > n) return -1;
					q[++r] = des[i];
				}
			}
		}
		vis[q[l]] = 0, ++ l;
	}
	return dis[n];
	
}

inline long long calc_spfa(int aim) {
	
	int l = 1, r = 1, ans = 0; w[l] = mp(n, aim + dist);
	while (l <= r) {
		if (w[l].ft == 1 && w[l].sc == 0) {
			ans ++;
			if (ans == p) ans = 0;
		}
		else for (int i = fst2[w[l].ft]; i; i = nxt2[i])
			if (val2[i] <= w[l].sc && dis[des2[i]] <= w[l].sc - val2[i])
				w[++r] = mp(des2[i], w[l].sc - val2[i]);
		l ++;
	}
	return ans;
	
}

int main() {
	
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	
	int T; scanf("%d", &T);
	while (T --) {

		memset(fst, 0, sizeof fst);
		memset(nxt, 0, sizeof nxt);
		memset(lst, 0, sizeof lst);
		memset(fst2, 0, sizeof fst2);
		memset(nxt2, 0, sizeof nxt2);
		memset(lst2, 0, sizeof lst2);
		cnt = cnt2 = 0;
		n = read(), m = read(), k = read(), p = read();
		for (int i = 1, u, v, vv; i <= m; ++i) 
			u = read(), v = read(), vv = read(), add(u, v, vv), add2(v, u, vv);
		dist = spfa();
		long long ans = 0;
		if (dist > -1 && dist != Inf)
			for (int i = 0; i <= k; ++i)
				(ans += calc_spfa(i)) %= p;
		else ans = -1;
		printf("%lld\n", ans);
		
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
	
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <fstream>
using namespace std;
int main() {
	
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	
	long long a, b;
	scanf("%lld%lld", &a, &b);
	if (a < b) swap(a, b);
	long long ans = (b - 1) * a - b;
	printf("%lld", ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
	
}

#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int i,j,k,n,m,x,y,t,T,s[10001],p[50001],c[50001],b[20000];
int totc,totp,tots;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
bool di(char ch){if (ch>='0'&&ch<='9')return 1;return 0;}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while (T--){
		t=read();
		memset(b,0,sizeof b);
		memset(s,0,sizeof s);memset(p,0,sizeof p);memset(c,0,sizeof c);int ans=0;
		tots=totp=totc=0;
		char ch=getchar();
		while (ch!='(')ch=getchar();
		ch=getchar();
		if (ch=='n')x=read();else x=0;
		bool flag=0;
		while (t--){
			char ch=getchar();
			while (ch!='F'&&ch!='E')ch=getchar();
			if (ch=='F'){
				int f2=0,f3=0;
				while (ch<'a'||ch>'z')ch=getchar();
				if (b[ch-'a'])flag=1;b[ch-'a']=1;s[++tots]=ch-'a';
				while (!di(ch)&&ch!='n')ch=getchar();
				if (ch=='n')f2=101;else {
					while (ch>='0'&&ch<='9'){f2=f2*10+ch-48;ch=getchar();}
				}
				ch=getchar();while (!di(ch)&&ch!='n')ch=getchar();
				if (ch=='n')f3=101;else {
					while (ch>='0'&&ch<='9'){f3=f3*10+ch-48;ch=getchar();}
				}
				c[++totc]=c[totc-1];
				if ((f2==101&&f3==101)||(f2!=101&&f3!=101)||(f2>f3)){
					p[++totp]=p[totp-1];
					if (f2>f3)c[totc]=1;
				}
				else p[++totp]=p[totp-1]+1;
			}
			else{if (!c[totc])ans=max(ans,p[totp]);c[totc]=0;b[s[tots]]=0;tots--;totp--;totc--;}
		}
		if (tots)flag=1;
		if (flag)printf("ERR\n");
		else if (ans==x)printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

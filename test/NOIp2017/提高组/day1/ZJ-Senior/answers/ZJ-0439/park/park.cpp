#include <cstring>
#include <cstdio>
#include <queue>
#define N 100001
using namespace std;
int i,j,k,n,m,x,y,t,p,w,b[N],T,ans,d[N],num[N];
int fi[N*2],ne[N*2],la[N*2],a[N*2],c[N*2];
queue<int>q;
void add(int x,int y,int t){
	k++;a[k]=y;c[k]=t;
	if (fi[x]==0)fi[x]=k;else ne[la[x]]=k;
	la[x]=k;
}
bool spfa(){
	for (i=1;i<=n;i++)d[i]=1013687232;d[1]=0;
	while (!q.empty())q.pop();
	memset(b,0,sizeof b);
	memset(num,0,sizeof num);
	q.push(1);b[1]=1;
	while (!q.empty()){
		x=q.front();q.pop();
		b[x]=0;num[x]++;
		if (num[x]>100000)return 0;
		for (int i=fi[x];i;i=ne[i])
			if (d[a[i]]>=d[x]+c[i]){
				d[a[i]]=d[x]+c[i];
				if (!b[a[i]]){
					b[a[i]]=1;
					q.push(a[i]);
				}
			}
	}
	return 1;
}
void dfs(int x,int y){
	if (y>d[x]+w)return;
	if (x==n)ans++;
	for (int i=fi[x];i;i=ne[i])
		dfs(a[i],y+c[i]);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		ans=0;
		memset(fi,0,sizeof fi);
		memset(ne,0,sizeof ne);
		k=0;
		scanf("%d%d%d%d",&n,&m,&w,&p);
		while (m--){scanf("%d%d%d",&x,&y,&t);add(x,y,t);}
		if (!spfa()){printf("-1\n");continue;}
		dfs(1,0);
		printf("%d\n",ans);
	}
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define LL long long

LL a,b;

int main()
{
	freopen("math.in","r",stdin); freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a>b) swap(a,b);
	if (a==1)
	{
		printf("0\n");
		return 0;
	}
	LL ans=(a-2)+(b-2)*(a-1);
	printf("%lld\n",ans);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<ctime>
using namespace std;
#define LL long long
#define random(x) (((rand()<<15)|rand())%(x)) 

int gcd(int i,int j)
{
	if(i%j==0) return j;
	return gcd(j,i%j);
}

int main()
{
	freopen("math.in","w",stdout);
	srand(time(0));
	int x=random(20)+1,y=random(20)+1;
	while (gcd(x,y)!=1)
	{
		x=random(20)+1,y=random(20)+1;
	}
	printf("%d %d\n",x,y);
	return 0;
}

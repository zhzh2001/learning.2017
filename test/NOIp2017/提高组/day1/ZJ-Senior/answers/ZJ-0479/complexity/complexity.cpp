#include<cstdio>
#include<algorithm>
#include<cstring>
#include<string>
#include<iostream>
using namespace std;
#define LL long long

int t,l,fzd,cof,zimu[300],ha[110],buer=0;
string s,st[110];

int duru(int i)
{
	while (s[i]<'0' || s[i]>'9') ++i;
	int ret=0;
	while (s[i]>='0' && s[i]<='9')
	{
		ret=ret*10+s[i]-'0';
		++i;
	}
	return ret;
}

int dfs(int i)
{
	if (i>l) return buer=1;
	ha[i]=1;
	if (st[i][0]=='E')
	{
		--cof;
		if (cof<0) return buer=1;
		else return 0;
	}
	else if (st[i][0]=='F')
	{
		int num1=0,num2=0;
		++cof;
		int po=1,ret=0;
		while (st[i][po]==' ') ++po;
		if (zimu[st[i][po]]) return buer=1;
		zimu[st[i][po]]=1;
		++po;
		while (st[i][po]==' ') ++po;
		if (st[i][po]=='n')
		{
			++po;
			while (st[i][po]==' ') ++po;
			ret+=dfs(i+1);
			if (st[i][po]!='n') return 0;
			else return ret;
		}
		else 
		{
			while (st[i][po]<='9' && st[i][po]>='0')
			{
				num1=num1*10+st[i][po]-'0';
				++po;
			}
		}
		++po;
		while (st[i][po]==' ') ++po;
		if (st[i][po]=='n') 
		{
			ret+=1;
			return ret+dfs(i+1);
		}
		else 
		{
			while (st[i][po]<='9' && st[i][po]>='0')
			{
				num2=num2*10+st[i][po]-'0';
				++po;
			}
		}
		ret+=dfs(i+1);
		if (num1<=num2) return ret;
		else return 0;
	}
}

int work()
{
		buer=0;
		cof=0;
		bool f=0;
		scanf("%d",&l);
		getline(cin,s);
		int i=0;
		while (s[i]!='O') ++i;
		if (s[i+2]=='1') fzd=0;
		else
		{
			i+=4;
			fzd=duru(i);
		}
		int ans=0;
		for (int i=1;i<=l;++i)
		{
			getline(cin,st[i]);
		}
		memset(ha,0,sizeof(ha));
		for (int i=1;i<=l;++i)
		{
			if (ha[i]) continue;
			memset(zimu,0,sizeof(zimu));
			int x=dfs(i);
			if (buer)
			{
				return -1;
			}
			else ans=max(ans,x);
		}
		return ans;
}

int main()
{
	freopen("complexity.in","r",stdin); freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	getline(cin,s);
	while (t--)
	{
		int x=work();
		if (x==-1 || buer==1) printf("ERR\n");
		else if (x==fzd) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

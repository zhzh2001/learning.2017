#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define LL long long
#define N 1010 
#define INF 1000010 
#define M 2010 

struct bian
{
	int u,v,w;
} b[M];

int t,n,m,k,p,a[N][N],ai,bi,ci,vis[N],diston[N],co,fir[N],ne[M];

void dfs(int i,int nowlen)
{
	if (i==n)
	{
		co=(co+1)%p;
		return;
	}
	for (int j=fir[i];j;j=ne[j])
	{
		if (nowlen+b[j].w+diston[b[j].v]<=diston[1]+k) dfs(b[j].v,nowlen+b[j].w);
	}
}

int main()
{
	freopen("park.in","r",stdin); freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		co=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(a,-1,sizeof(a));
		for (int i=1;i<=n;++i) diston[i]=INF;
		memset(fir,0,sizeof(fir));
		memset(ne,0,sizeof(ne));
		memset(vis,0,sizeof(vis));
		diston[n]=0;
		diston[0]=INF;
		for (int i=1;i<=m;++i)
		{
			scanf("%d%d%d",&ai,&bi,&ci);
			a[ai][bi]=ci;
			b[i].u=ai;
			b[i].v=bi;
			b[i].w=ci;
			ne[i]=fir[b[i].u];
			fir[b[i].u]=i;
		}
		for (int i=1;i<=n-1;++i)
		{
			int k=0;
			for (int j=1;j<=n;++j)
			{
				if (diston[j]<diston[k] && !vis[j]) k=j;
			}
			vis[k]=1;
			for (int j=1;j<=n;++j)
			{
				if (j==k || vis[j] || a[j][k]==-1) continue;
				diston[j]=min(diston[j],diston[k]+a[j][k]);
			}
		}
		dfs(1,0);
		printf("%d\n",co);
	}
	return 0;
}

 var a,b,ans:int64;
 procedure swap(var x,y:int64);
  var t:int64;
   begin
    t:=x;x:=y;y:=t;
   end;
 begin
  assign(input,'math.in');reset(input);
  assign(output,'math.out');rewrite(output);
  read(a,b);
  if a>b then swap(a,b);
  ans:=(a-1)*b-a;
  writeln(ans);
  close(input);close(output);
 end.
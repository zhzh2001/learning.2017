 const maxn=100005;maxm=200005;
 var tot,Q,t,n,m,k,p,INF,ans:longint;
     que,link,dist:array[0..maxn]of longint;
     son,w,next:array[0..maxm]of longint;
     vis:array[0..maxn]of boolean;
 procedure add(x,y,z:longint);
  begin
   inc(tot);w[tot]:=z;son[tot]:=y;next[tot]:=link[x];link[x]:=tot;
  end;
 procedure spfa;
  var head,tail,j,x:longint;
   begin
    fillchar(que,sizeof(que),0);
    fillchar(vis,sizeof(vis),0);
    fillchar(dist,sizeof(dist),63);
    INF:=dist[0];
    dist[1]:=0;vis[1]:=true;que[1]:=1;tail:=1; head:=0;
    while head<>tail do
     begin
      head:=(head+1)mod maxn;
      x:=que[head];
      vis[x]:=false;
      j:=link[x];
      while j<>0 do
       begin
        if dist[x]+w[j]<dist[son[j]] then begin
                                           dist[son[j]]:=dist[x]+w[j];
                                           if not vis[son[j]] then begin
                                                                    vis[son[j]]:=true;
                                                                    tail:=(tail+1)mod maxn;
                                                                    que[tail]:=son[j];
                                                                   end;
                                          end;
        j:=next[j];
       end;
      end;
    end;
 procedure dfs(x,y:longint);
  var j:longint;
   begin
    if y>dist[n]+k then exit;
    if x=n then ans:=(ans+1)mod p;
    j:=link[x];
    while j<>0 do
     begin
      dfs(son[j],y+w[j]);
      j:=next[j];
     end;
   end;
 procedure main;
  var i,j,x,y,z:longint;
   begin
    read(n,m,k,p);
    tot:=0;
    fillchar(link,sizeof(link),0);
    for i:=1 to m do
     begin
      read(x,y,z);
      add(x,y,z);
     end;
    spfa;
    if INF=dist[n] then begin
                         writeln(0);
                         exit;
                        end;
    ans:=0;dfs(1,0);
    writeln(ans);
   end;
 begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  read(Q);
  for t:=1 to Q do main;
  close(input);close(output);
 end.

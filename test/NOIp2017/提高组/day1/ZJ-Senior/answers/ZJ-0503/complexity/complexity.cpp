#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<cstring>
using namespace std;
#define upr(i,x,y) for(int i=x;i<=y;i++)
#define dnr(i,x,y) for(int i=x;i>=y;i--)
int ans=0,t,stk,L;
bool fl=0,us[30];
char xm[10];

bool cmp(char p[],char q[]){
	int lp=strlen(p),lq=strlen(q);
	if(lp==lq) upr(i,0,lp-1) if(p[i]>q[i])return 0;
		else if(p[i]<q[i]) return 1;
	return lp<lq;
}
int dfs(bool f){
	stk++;
	if(t>L) {fl=1;return 0;}
	char s[3],a[3],b[3],c[3];
	if(fl) return 0;
	if(f)	{
		++t;
		if(t>L) {fl=1;return 0;}
		scanf("%s",s);
		if(s[0]=='E') {fl=1;return 0;}
	}
	scanf("%s%s%s",a,b,c);
	if(us[a[0]-'a']) {fl=1;return 0;}
	us[a[0]-'a']=1;
	int tp=0;
	t++;
	if(t>L) {fl=1;return 0;}
	scanf("%s",s);
	while (s[0]=='F'){
		tp=max(dfs(0),tp);
		t++;
		if(t>L) {fl=1;return 0;}
		scanf("%s",s);
	}
	--stk; 
	if(b[0]!='n'&&c[0]=='n') tp++;
		else if((b[0]=='n'&&c[0]!='n')||(!cmp(b,c))) tp=0;
	us[a[0]-'a']=0;
	return tp;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;cin>>T;
	while(T--){
		scanf("%d%s",&L,xm);t=0;
		fl=0;ans=0;
		while(t<L&&(!fl)) {
			upr(i,0,28) us[i]=0;stk=0;
			ans=max(ans,dfs(1));
		}
		if(fl) printf("ERR\n");else{
			int le=strlen(xm),xa=0;
			if(xm[2]=='1') xa=0;else 
				upr(i,4,le-2) xa=xa*10+xm[i]-'0';
			if(xa==ans) printf("Yes\n");
				else printf("No\n");
		} 
	}
}

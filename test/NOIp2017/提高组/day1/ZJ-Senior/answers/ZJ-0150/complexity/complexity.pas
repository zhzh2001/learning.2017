var
  i,j,k,t,n,m,l,sum,a,b,d,e,ff,rr,gg,hh,tt:longint;
  bj,jb:boolean;
  s,s1,s2,s3:string;
  f,g,h,r:char;
begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);

  readln(t);
  for i:=1 to t do
   begin
     readln(s);
     a:=pos(' ',s);
     val(copy(s,1,a-1),l);
     a:=pos('(',s);
     b:=pos(')',s);
     s1:=copy(s,a+1,b-a-1);
     d:=pos('^',s1);
     bj:=false;
     if d=0
       then if s1[1]='n'
              then e:=105
              else val(s1,e)
       else begin
              val(copy(s1,3,1),e);
              bj:=true;
            end;
     gg:=0;  tt:=0;
     for j:=1 to l do
      begin
        read(f);
        if f='E'
         then begin
                continue;
                gg:=0;
              end;
        read(r,g,h);
        if r='n'
          then rr:=105
          else rr:=ord(g)-48;
        if h='n'
          then hh:=105
          else hh:=ord(h)-48;
        inc(gg);
        if bj=false
          then begin
                 ff:=hh-rr+1;
                 if (ff=e)and(ff>0)
                   then writeln('Yes')
                   else writeln('No');
               end
          else begin
                 if (gg=1)and(hh=105)and(hh-rr>0)
                   then tt:=1
                   else if (hh=105)and(hh-rr>0)
                          then inc(tt);
               end;
      end;
     if bj=true
      then if tt=e
            then writeln('Yes')
            else writeln('No');
   end;

  close(input);
  close(output);
end.

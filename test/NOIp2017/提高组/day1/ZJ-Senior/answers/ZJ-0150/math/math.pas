var
  i,j,k,t,n,m,a,b,c,d,max:longint;
  bj:array[0..100000] of boolean;
begin
  assign(input,'math.in');reset(input);
  assign(output,'math.out');rewrite(output);

  readln(a,b);
  if a<b
    then begin
           c:=a;
           a:=b;
           b:=c;
         end;
  i:=0; j:=0;
  fillchar(bj,sizeof(bj),true);
  while i<a do
   begin
     i:=(b*j) mod a;
     bj[i]:=false;
     inc(j);
   end;
  k:=0;  max:=0;
  while k<(a+b) do
   begin
     if (k mod b<>0)and(k mod a<>0)
      then max:=k;
     inc(k);
   end;
  t:=0; inc(k);
  n:=k mod a;
  m:=maxlongint;
  while n<>m do
   begin
     if bj[k mod a]
      then max:=k;
     inc(t);
     if t=j
       then begin
              if n=(k mod a)
                then m:=k mod a
                else begin
                       n:=k mod a;
                       t:=0;
                     end;
            end;
     inc(k);
   end;
  writeln(max);

  close(input);
  close(output);
end.

#include<cstdio>
#include<cctype>
typedef long long int64;
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
int main() {
	freopen("math.in","r+",stdin);
	freopen("math.out","w+",stdout);
	printf("%lld\n",(int64)(getint()-1)*(getint()-1)-1);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cctype>
#include<hash_set>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
__gnu_cxx::hash_set<int> vis;
int main() {
	for(register int a=1;a<=10;a++) {
		for(register int b=1;b<=10;b++) {
			vis.clear();
			vis.insert(0);
			int ans=0;
			for(register int i=1;i<a*b;i++) {
				if(vis.count(i-a)||vis.count(i-b)) {
					vis.insert(i);
				} else {
					ans=i;
				}
			}
			printf("%4d",ans);
		}
		puts("");
	}
	return 0;
}

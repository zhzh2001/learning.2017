#include<cstdio>
#include<cctype>
#include<vector>
#include<functional>
#include<ext/pb_ds/priority_queue.hpp>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int inf=0x7fffffff;
const int N=100001,K=50;
struct Edge {
	int to,w;
};
std::vector<Edge> e[N];
inline void add_edge(const int &u,const int &v,const int &w) {
	e[u].push_back((Edge){v,w});
}
int n,m,mod,k;
int dis[N],cnt[N];
struct Vertex {
	int id,dis;
	bool operator > (const Vertex &another) const {
		return dis>another.dis;
	}
};
__gnu_pbds::priority_queue<Vertex,std::greater<Vertex> > q;
__gnu_pbds::priority_queue<Vertex,std::greater<Vertex> >::point_iterator p[N];
inline void dijkstra() {
	p[1]=q.push((Vertex){1,0});
	for(register int i=2;i<=n;i++) {
		p[i]=q.push((Vertex){i,dis[i]=inf});
	}
	cnt[1]=1;
	while(q.top().dis!=inf) {
		const int x=q.top().id;
		for(register unsigned i=0;i<e[x].size();i++) {
			const int &y=e[x][i].to,&w=e[x][i].w;
			if(dis[x]+w<dis[y]) {
				q.modify(p[y],(Vertex){y,dis[y]=dis[x]+w});
				cnt[y]=cnt[x];
			} else if(dis[x]+w==dis[y]) {
				cnt[y]=(cnt[y]+cnt[x])%mod;
			}
		}
		q.modify(p[x],(Vertex){0,inf});
	}
}
struct Vertex2 {
	int id,k,dis;
	bool operator > (const Vertex2 &another) const {
		return dis>another.dis;
	}
};
__gnu_pbds::priority_queue<Vertex2,std::greater<Vertex2> > q2;
__gnu_pbds::priority_queue<Vertex2,std::greater<Vertex2> >::point_iterator p2[N][K];
int dis2[N][K],cnt2[N][K];
inline void dijkstra2() {
	p2[1][0]=q2.push((Vertex2){1,0,0});
	for(register int i=1;i<k;i++) {
		p2[1][i]=q2.push((Vertex2){1,i,dis2[1][i]=inf});
	}
	for(register int i=2;i<=n;i++) {
		for(register int j=0;j<k;j++) {
			p2[i][j]=q2.push((Vertex2){i,j,dis2[i][j]=inf});
		}
	}
	cnt2[1][0]=1;
	while(q2.top().dis!=inf) {
		const int x=q2.top().id,k1=q2.top().k;
		for(register unsigned i=0;i<e[x].size();i++) {
			const int &y=e[x][i].to,&w=e[x][i].w;
			for(register int k2=0;k2<k;k2++) {
				if(dis2[x][k1]+w<dis2[y][k2]) {
					q2.modify(p2[y][k2],(Vertex2){y,k2,dis2[y][k2]=dis2[x][k1]+w});
					for(register int i=k-1;i>k2;i--) {
						cnt2[y][i]=cnt2[y][i-1];
					}
					cnt2[y][k2]=cnt2[x][k1];
					break;
				} else if(dis2[x][k1]+w==dis2[y][k2]) {
					cnt2[y][k2]=(cnt2[y][k2]+cnt2[x][k1])%mod;
					break;
				}
			}
		}
		q2.modify(p2[x][k1],(Vertex2){0,0,inf});
	}
}
/*int low[N],dfn[N],scc[N],size[N],count,id;
bool ins[N];
std::stack<int> s;
void tarjan(const int &x) {
	dfn[x]=low[x]=++count;
	s.push(x);
	ins[x]=true;
	for(register unsigned i=0;i<e[x].size();i++) {
		const int &y=e[x][i];
		if(!dfn[y]) {
			tarjan(y);
			low[x]=std::min(low[x],low[y]);
		} else if(ins[y]) {
			low[x]=std::min(low[x],dfn[y]);
		}
	}
	if(dfn[x]==low[x]) {
		id++;
		int y=0;
		while(y!=x) {
			y=s.top();
			s.pop();
			ins[y]=false;
			scc[s]=id;
			size[id]++;
		}
	}
}*/
inline void init() {
	for(register int i=1;i<N;i++) {
		e[i].clear();
	}
	q.clear();
	q2.clear();
	//count=id=0;
}
int main() {
	freopen("park.in","r+",stdin);
	freopen("park.out","w+",stdout);
	for(register int T=getint();T;T--) {
		init();
		n=getint(),m=getint(),k=getint(),mod=getint();
		for(register int i=0;i<m;i++) {
			const int u=getint(),v=getint(),w=getint();
			add_edge(u,v,w);
		}
		if(k==0) {
			dijkstra();
			printf("%d\n",cnt[n]);
			continue;
		}
		dijkstra2();
		int ans=0;
		for(register int i=0;i<k;i++) {
			if(dis2[n][i]-dis2[n][0]>k) break;
			ans=(ans+cnt2[n][i])%mod;
		}
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<set>
#include<stack>
#include<string>
#include<cstdio>
#include<cctype>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
std::stack<int> stack;
std::stack<std::string> vars;
std::set<std::string> set;
int comp;
char tmp[100];
char op[2],var[2],low[3],high[3];
inline int str2i(const char s[]) {
	int ret=0;
	for(register int i=0;s[i];i++) {
		ret=ret*10+s[i]-'0';
	}
	return ret;
}
int main() {
	freopen("complexity.in","r+",stdin);
	freopen("complexity.out","w+",stdout);
	for(register int T=getint();T;T--) {
		while(!stack.empty()) stack.pop();
		while(!vars.empty()) vars.pop();
		set.clear();
		int l=getint();
		getchar(),getchar();
		if(getchar()=='1') {
			comp=0;
			getchar();
		} else {
			comp=getint();
		}
		int now=0,max=0,cnt=0;
		bool count=true;
		int i;
		for(i=1;i<=l;i++) {
			scanf("%1s",op);
			if(op[0]=='F') {
				cnt++;
				scanf("%1s",var);
				vars.push(var);
				if(set.count(var)) {
					puts("ERR");
					goto Next;
				}
				set.insert(var);
				scanf("%s%s",low,high);
				if(low[0]=='n'&&high[0]!='n'&&count) {
					count=false;
					stack.push(-1);
					continue;
				}
				if(low[0]!='n'&&high[0]=='n'&&count) {
					now++;
					max=std::max(max,now);
					stack.push(1);
					continue;
				}
				if(low[0]!='n'&&high[0]!='n'&&str2i(low)>str2i(high)&&count) {
					count=false;
					stack.push(-1);
					continue;
				}
				stack.push(0);
			} else {
				if((--cnt)<0) {
					puts("ERR");
					goto Next;
				}
				if(stack.top()==-1) {
					count=true;
				} else {
					now-=stack.top();
				}
				stack.pop();
				set.erase(vars.top());
				vars.pop();
			}
		}
		if(cnt) {
			puts("ERR");
			goto Next;
		}
		puts(max==comp?"Yes":"No");
		continue;
		Next:
			for(i=i+1;i<=l;i++) {
				gets(tmp);
			}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

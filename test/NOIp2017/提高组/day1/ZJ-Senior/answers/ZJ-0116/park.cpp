#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
const int N=100100,M=200100;
int n,m,k,mo,tot,Next[M],head[N],tree[M],val[M],dis[N],x[N*2],f[N][55],d[N][55],In[N][55];
bool visit[N],vis[N][55];
struct node{int u,dis;}X[N*55];
int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void add(int x,int y,int z)
{
	tot++;
	Next[tot]=head[x];
	head[x]=tot;
	tree[tot]=y;
	val[tot]=z;
}
void Add(int &x,int y)
{
	x+=y;
	if (x>=mo) x-=mo;
}
void SPFA()
{
	int t=0,w=1;
	for (int i=1;i<=n;i++) dis[i]=1<<29,visit[i]=0;
	x[1]=1;dis[1]=0;visit[1]=1;
	while (t!=w)
	{
		t++;
		if (t>2*n) t=1;
		int u=x[t];
		visit[u]=0;
		for (int i=head[u];i;i=Next[i])
		{
			int v=tree[i];
			if (dis[v]>dis[u]+val[i])
			{
				dis[v]=dis[u]+val[i];		
				if (!visit[v])
				{
					w++;
					if (w>2*n) w=1;
					x[w]=v;
					visit[v]=1;
				}
			}
		}
	}
}
bool Calc()
{
	int t=0,w=1;
	memset(f,0,sizeof(f));
	memset(vis,0,sizeof(vis));
	memset(In,0,sizeof(In));
	X[1]=(node){1,0};f[1][0]=1;d[1][0]=1;vis[1][0]=1;
	while (t!=w)
	{
		t++;
		if (t>n*(k+1)) t=1;
		node s=X[t];
		int u=s.u;
		vis[u][s.dis-dis[u]]=0;
		for (int i=head[u];i;i=Next[i])
		{
			int v=tree[i];
			if (s.dis+val[i]<=dis[v]+k&&s.dis+val[i]<=dis[n]+k)
			{
				int x=s.dis+val[i]-dis[v];
				Add(f[v][x],d[u][s.dis-dis[u]]);
				Add(d[v][x],d[u][s.dis-dis[u]]);
				if (!vis[v][x])
				{
					vis[v][x]=1;
					In[v][x]++;
					if (In[v][x]>n*(k+1)) return false;
					w++;
					if (w>n*(k+1)) w=1;
					X[w]=(node){v,s.dis+val[i]};
				}
			}
		}
		d[u][s.dis-dis[u]]=0;
	}
	return true;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);	
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&mo);
		tot=0;
		memset(head,0,sizeof(head));
		bool Flag=false;
		for (int i=1;i<=m;i++)
		{
			int x=read(),y=read(),z=read();
			add(x,y,z);
			if (z==0) Flag=true;
		}
		SPFA();
		if (!Calc()&&Flag) { puts("-1");continue;}
		int ans=0;
		for (int i=0;i<=k;i++) Add(ans,f[n][i]);
		printf("%d\n",ans);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
int n,m,number[200],f[200],g[200];
char s[100],s1[100],s2[5][100];
bool p[50];
int calc(int x)
{
	int len=strlen(s2[x]),sum=0;
	if (s2[x][0]=='n') return 101;
	for (int i=0;i<len;i++)
		if (s2[x][i]>='0'&&s2[x][i]<='9') sum=sum*10+s2[x][i]-'0';
	return sum;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%s",&n,s);
		m=0;
		int len=strlen(s),i;
		for (i=0;i<len;i++) if (s[i]=='(') break;
		if (s[i+1]!='1')
		{
			for (;i<len;i++)
				if (s[i]>='0'&&s[i]<='9') m=m*10+s[i]-'0';
		}
		int top=0,ans=0,flag=0;bool Right=true;
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		memset(p,0,sizeof(p));
		for (int i=1;i<=n;i++)
		{
			scanf("%s",s);
			int len=strlen(s);
			if (s[0]=='E')
			{
				if (!top) { Right=false;continue;}
				if (f[top]) flag--;
				p[number[top]]=0;
				top--;
			}else
			{
				scanf("%s",s1);
				if (p[s1[0]-'a']==1) Right=false;
				p[s1[0]-'a']=1;
				number[++top]=s1[0]-'a';
				scanf("%s%s",&s2[1],&s2[2]);
				int x=calc(1),y=calc(2);
				f[top]=0;
				if (x>y) f[top]=1,flag++;
				if (flag==0)
				{
					g[top]=g[top-1];
					if (x!=101&&y==101) g[top]++;
					ans=max(ans,g[top]);
				}
			}
		}
		if (top) Right=false;
		if (!Right) { puts("ERR");continue;}
		if (ans==m) puts("Yes");else puts("No");
	}
	return 0;
}

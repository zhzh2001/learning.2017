#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=200;
char str[N][N],s1[N][N],s2[N][N],s3[N][N],ans[N],myans[N];
bool flag[1000];
int top;

struct node{
	int ch,p,r;
}stack[1000];
int read(char *s)
{
	if (s[0]=='n') return 10000;
	int x=0,len=strlen(s);
	for (int i=0;i<len;i++)
		x=x*10+s[i]-'0';
	return x;
}
bool solve()
{
	int L;
	scanf("%d",&L); scanf("%s",ans);
	memset(flag,false,sizeof(flag));
	top=0; stack[0]=(node){0,0,0};
	for (int i=1;i<=L;i++) 
	{
		scanf("%s",str[i]);
		if (str[i][0]=='F')
			scanf("%s%s%s",s1[i],s2[i],s3[i]);
	}
	for (int i=1;i<=L;i++)
	{
		if (str[i][0]=='F')
		{
			int x=s1[i][0]-'a',y=-1,z=-1,q;
			if (flag[x]) return false;
			flag[x]=true;
			y=read(s2[i]);
			z=read(s3[i]);
			if (y>z) q=-1;
			else if (y+1000>z) q=0;
			else q=1;
			top++;
			stack[top]=(node){x,q,0};
		}
		else if (str[i][0]=='E')
		{
			if (top<1) return false;
			node u=stack[top--];
			int ch=u.ch,p=u.p,r=u.r;
			flag[ch]=false;
			if (p!=-1)
				stack[top].r=max(stack[top].r,p+r);
		}		
	}
	if (top>0) return false;
	if (stack[0].r==0) sprintf(myans,"O(1)");
	else sprintf(myans,"O(n^%d)",stack[0].r);
	if (!strcmp(ans,myans)) puts("Yes");
	else puts("No");
	return true;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
		if (!solve()) puts("ERR");	
	return 0;
}

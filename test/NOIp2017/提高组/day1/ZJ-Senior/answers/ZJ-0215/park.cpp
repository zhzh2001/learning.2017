#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
const int N=100000+100,M=200000+200,INF=0x3f3f3f3f;
int Head[N],dis[N],f[N],n,m,K,P,tot;
bool vis[N];

struct Edge{
	int v,d,next;
}edge[M];
struct node{
	int v,d;
	bool operator < (const node &x) const
	{return d>x.d;}
};
priority_queue<node> Q;
void addedge(int x,int y,int z)
{
	edge[++tot]=(Edge){y,z,Head[x]};
	Head[x]=tot;
}
void Dijkstra(int s)
{
	for (int i=1;i<=n;i++) 
		dis[i]=INF,vis[i]=false,f[i]=0;
	dis[s]=0; f[s]=1; Q.push((node){s,0});
	while (!Q.empty())
	{
		int u=Q.top().v;
		Q.pop();
		if (vis[u]) continue;
		vis[u]=true;
		for (int i=Head[u];i;i=edge[i].next)
		{
			int v=edge[i].v,d=edge[i].d;
			if (dis[v]>dis[u]+d)
			{
				dis[v]=dis[u]+d; f[v]=f[u];
				Q.push((node){v,dis[v]});
			}
			else if (dis[v]==dis[u]+d){
				f[v]+=f[u];
				if (f[v]>+P) f[v]-=P;
			}
		}		
	}	
}
void solve()
{
	memset(Head,0,sizeof(Head));
	tot=0;
	scanf("%d%d%d%d",&n,&m,&K,&P);
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z);
	}
	Dijkstra(1);
	printf("%d\n",f[n]);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) solve();
	return 0;
}

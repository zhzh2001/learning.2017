#include<queue>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define N 1000000
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=b;i>=a;i--)
using namespace std;
queue<int> q;
int tar[N],len[N],nxt[N],head[N],b[N],dis[N];
int med[N][55];
int T,tot,i,j,n,m,k,p,x,y,z,nw,nt,d,upper,flag,res;
void add_edge(int x,int y,int z)
{tot++; tar[tot] = y; len[tot] = z; nxt[tot] = head[x]; head[x] = tot;}
void dfs(int x)
{
	int i;
	for (i = head[x];i != 0; i = nxt[i])
		if (len[i] == 0)
			{
				if (b[tar[i]]) {flag = 1; return;}
				b[tar[i]] = 1;
				dfs(tar[i]);
				if (flag) return;
			}
}
void judge()
{
	int st,i;
	fo(st,1,n)
		{
			fo(i,1,n) b[i] = 0; b[st] = 1;
			dfs(st);
			if (flag) return;
		}
}

int ww()
{
	scanf("%d%d%d%d",&n,&m,&k,&p);
	fo(i,1,n) head[i] = 0; tot = 0;
	fo(i,1,m)
		{
			scanf("%d%d%d",&x,&y,&z);
			add_edge(x,y,z);
		}
	flag = 0; if (n <= 1000) judge(); if (flag) {cout<<-1<<endl; return 0;}
	
	q.push(1);
	fo(i,1,n) b[i] = 0; b[1] = 1;
	fo(i,1,n) dis[i] = 2000000000; dis[1] = 0;
	while (!q.empty())
		{
			nw = q.front(); q.pop();
			for (i = head[nw];i != 0; i = nxt[i])
				{
					nt = tar[i]; d = len[i];
					if (dis[nw] + d < dis[nt])
						{
							dis[nt] = dis[nw] + d;
						//	med[nt][0] = med[nw][0];
							if (!b[nt]) {b[nt] = 1; q.push(nt);}
						}//	else
				//	if (dis[nw] + d == dis[nt]) {med[nt][0] += med[nw][0]; med[nt][0] %= p;}
				}
			b[nw] = 0;
		}
//	cout<<dis[n]<<endl;
//	cout<<med[n][0]<<endl;
	
	q.push(1); k = 5;
	fo(i,1,n) b[i] = 0; b[1] = 1;
	fo(i,1,n) fo(j,0,k) med[i][j] = 0; med[1][0] = 1;
	while (!q.empty())
		{
			nw = q.front(); q.pop();
			for (i = head[nw];i != 0; i = nxt[i])
				{
					nt = tar[i]; d = len[i];
					
					flag = 0;
					fo(j,0,k)
						if (dis[nw] + d + j <= dis[nt] + k)
							{
								flag = 1;
								med[nt][dis[nw]+d+j-dis[nt]] += med[nw][j];
								med[nt][dis[nw]+d+j-dis[nt]] %= p;
							}
					if (flag) if (!b[nt]) {b[nt] = 1; q.push(nt);}
				}
			b[nw] = 0;
		//	cout<<med[n][0]<<endl;
		}
	res = 0; fo(i,0,k) res = (res + med[n][i]) % p;
	cout<<res<<endl;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--) ww();
	return 0;
}

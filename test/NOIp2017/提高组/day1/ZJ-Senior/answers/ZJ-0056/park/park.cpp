#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
vector <int> dlk[100005];
vector <ll> ksl[100005];
ll k,mn,n,ans,t,hjb,temp,m,p,ai,bi,ci,head,tail,dis[100005];
int r[500005];
bool f[100005];
void check(int x)
{
	if (!f[x])
	{
		hjb=false;
		return;
	}
	f[x]=false;
	for (int i=0;i<dlk[x].size();i++)
	{
		if (ksl[x][i]==0)
			check(dlk[x][i]);
	}
}
void dfs(int x,int s)
{
	if (s>k+mn)
	{
		return;
	}
	if (x==n)
	{
		ans++;
		return;
	}
	for (int i=0;i<dlk[x].size();i++)
	{
		dfs(dlk[x][i],s+ksl[x][i]);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%lld",&t);
	for (int l=1;l<=t;l++)
	{
		hjb=true;
		mn=0;
		scanf("%lld%lld%lld%lld",&n,&m,&k,&p);
		for (int i=1;i<=m;i++)
		{
			scanf("%lld%lld%lld",&ai,&bi,&ci);
			dlk[ai].push_back(bi);
			ksl[ai].push_back(ci);
		}
		for (int i=1;i<=n;i++)
		{
			memset(f,true,sizeof(f));
			check(i);
		}
		if (!hjb)
		{
			printf("-1\n");
			continue;
		}
		head=0;
		tail=1;
		r[1]=1;
		memset(f,true,sizeof(f));
		memset(dis,-1,sizeof(dis));
		dis[1]=0;
		while (head!=tail)
		{
			head++;
			temp=r[head];
			for (int i=0;i<dlk[temp].size();i++)
			{
				if (dis[dlk[temp][i]]==-1||dis[dlk[temp][i]]>dis[temp]+ksl[temp][i])
				{
					if (f[dlk[temp][i]])
					{
						f[dlk[temp][i]]=true;
						tail++;
						r[tail]=dlk[temp][i];
						dis[dlk[temp][i]]=dis[temp]+ksl[temp][i];
					}
					else
					{
						dis[dlk[temp][i]]=dis[temp]+ksl[temp][i];
					}
				}
			}
		}
		mn=dis[n];
		dfs(1,0);
		printf("%lld\n",ans%p);
	}
}

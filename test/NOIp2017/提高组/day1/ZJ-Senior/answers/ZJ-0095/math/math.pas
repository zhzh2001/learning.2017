var
  a,b,c:qword;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  read(a,b);
  c:=(a-1)*(b-1)-1;
  write(c);
  close(input);
  close(output);
end.
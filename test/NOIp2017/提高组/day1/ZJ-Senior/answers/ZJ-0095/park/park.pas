var
  l,n,m,k,p,i,j:longint;
  d:array[0..1001]of longword;
  u,v,w:array[0..2001] of longword;
  s,t:array[0..1001,0..2001]of longword;
function f(x,y:longint):longint;
var
  i:longint;
begin
  if t[x,y]<4294967295 then exit(t[x,y]);
  f:=0;
  for i:=1 to s[x,0] do
    if y>=w[s[x,i]] then
      f:=f+f(u[s[x,i]],y-w[s[x,i]]);
  if f<f(x,y-1) then f:=f(x,y-1);
  t[x,y]:=f;
end;
begin
assign(input,'park.in');
assign(output,'park.out');
reset(input);
rewrite(output);
readln(l);
while l>0 do
begin
  dec(l);
  readln(n,m,k,p);
  for i:=1 to n do
    s[i,0]:=0;
  for i:=1 to m do
  begin
    readln(u[i],v[i],w[i]);
    inc(s[v[i],0]);
    s[v[i],s[v[i],0]]:=i;
  end;
  for i:=2 to n do
    d[i]:=2147483647;
  d[1]:=0;
  for i:=1 to n-1 do
    for j:=1 to m do
      if d[v[j]]>d[u[j]]+w[j] then d[v[j]]:=d[u[j]]+w[j];
  k:=k+d[n];
  fillchar(t,sizeof(t),255);
  for i:=2 to n do
    for j:=0 to d[i]-1 do
      t[i,j]:=0;
  t[1,0]:=1;
  writeln(f(n,k)mod p);
end;
close(input);
close(output);
end.
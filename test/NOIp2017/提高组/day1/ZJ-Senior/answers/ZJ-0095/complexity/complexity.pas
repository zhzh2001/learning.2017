var
  c:char;
  r:string[15];
  w,z:boolean;
  n,i,j,l,m,x,k,p:longint;
  s:array[-101..101]of char;
  u,v:array[-101..101]of string[15];
  t:array['a'..'z']of boolean;
procedure pr;
begin
  for c:='a' to 'z' do t[c]:=false;
  p:=1;
  m:=0;
  x:=0;
  w:=true;
  z:=true;
  readln(l,c,c,c,r);
  if r[1]='1' then k:=0 else val(copy(r,3,length(r)-3),k);
  for i:=1 to l do
  begin
    read(c);
    if z then
    if c='E' then
    begin
      dec(p);
      if p=0 then begin z:=false; continue; end;
      readln;
      t[s[p]]:=false;
      if (u[p][1]<>'n')and(v[p][1]='n') then if w then dec(x);
      if (u[p][1]='n')and(v[p][1]<>'n') then w:=true;
    end else
    begin
      read(c,s[p],c,c);
      u[p]:='';
      while c<>' ' do
      begin
        u[p]:=u[p]+c;
        read(c);
      end;
      readln(v[p]);
      if t[s[p]] then begin z:=false; continue; end;
      t[s[p]]:=true;
      if (u[p][1]<>'n')and(v[p][1]='n') then begin if w then inc(x); if x>m then m:=x; end;
      if (u[p][1]='n')and(v[p][1]<>'n') then w:=false;
      inc(p);
    end else readln;
  end;
  if p<>1 then z:=false;
  if z then
    if m=k then writeln('Yes') else writeln('No')
  else writeln('ERR');
end;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  for i:=-101 to 101 do
  begin
    s[i]:='a';
    u[i]:='1';
    v[i]:='1';
  end;
  readln(n);
  for j:=1 to n do pr;
  close(input);
  close(output); 
end.

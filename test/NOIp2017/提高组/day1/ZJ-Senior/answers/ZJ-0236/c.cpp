#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
const int N = 100005, M = N << 1, inf = 1e9 + 7 ;
using namespace std ;

int n, m, d, mo, e, ter[M], nxt[M], lnk[N], w[M], dis[N], dist[N] ;
int he, ta, cnt, top, dfn[N], low[N], bel[N], st[N], deg[N], opt[N], f[N][51] ; 
bool vis[N], ins[N], incir[N], g[N][51] ;
pair <int, int> q[N + M] ;

struct edge {
	int x, y, z ; 
} ed[M] ;

void add(int x, int y, int z) {
	ter[++ e] = y, nxt[e] = lnk[x], lnk[x] = e, w[e] = z ; 
}

bool cmp(pair <int, int> a, pair <int, int> b) {
	return a.second > b.second ;
}

void push(pair <int, int> x) {
	q[ta ++] = x ;
	push_heap(q, q + ta, cmp) ;
}

void pop() {
	pop_heap(q, q + ta, cmp) ;
	-- ta ;
}

void build() {
	rep(i, 1, m) {
		add(ed[i].y, ed[i].x, ed[i].z) ;
	}
	rep(i, 1, n) {
		dist[i] = inf ;
		vis[i] = true ;
	}
	dist[n] = 0 ;
	ta = 0 ;
	push(make_pair(n, 0)) ;
	for ( ; ta; ) {
		pair <int, int> u = q[0] ;
		pop() ;
		if (!vis[u.first]) continue ;
		vis[u.first] = false ;
		for (int i = lnk[u.first]; i; i = nxt[i]) {
			int v = ter[i] ;
			if (dist[u.first] + w[i] < dist[v]) {
				dist[v] = dist[u.first] + w[i] ;
				push(make_pair(v, dist[v])) ;
			}
		}
	}
	rep(i, 1, n) lnk[i] = 0 ;
	rep(i, 1, e) nxt[i] = 0 ; e = 0 ;
	rep(i, 1, m) {
		add(ed[i].x, ed[i].y, ed[i].z) ;
	}
	rep(i, 1, n) {
		dis[i] = inf ;
		vis[i] = true ;
	}
	dis[1] = 0 ;
	ta = 0 ;
	push(make_pair(1, 0)) ;
	for ( ; ta; ) {
		pair <int, int> u = q[0] ;
		pop() ;
		if (!vis[u.first]) continue ;
		vis[u.first] = false ;
		for (int i = lnk[u.first]; i; i = nxt[i]) {
			int v = ter[i] ;
			if (dis[u.first] + w[i] < dis[v]) {
				dis[v] = dis[u.first] + w[i] ;
				push(make_pair(v, dis[v])) ;
			}
		}
	}
}

void tarjan(int p) {
	dfn[p] = low[p] = ++ cnt ;
	st[++ top] = p, ins[p] = true ;
	for (int i = lnk[p] ; i; i = nxt[i]) {
		if (dis[p] + w[i] != dis[ter[i]]) continue ;
		if (!dfn[ter[i]]) {
			tarjan(ter[i]) ;
			low[p] = min(low[p], low[ter[i]]) ;
		} else 
		if (ins[ter[i]]) low[p] = min(low[p], dfn[ter[i]]) ;
	}
	if (dfn[p] == low[p]) {
		for ( ; ; ) {
			ins[st[top]] = false ;
			bel[st[top]] = p ;
			-- top ;
			if (st[top + 1] == p) break ;
		}
	}
}

void pre() {
	cnt = top = 0 ;
	rep(i, 1, n) {
		dfn[i] = low[i] = bel[i] = 0 ;
		ins[i] = false ;
	}
	rep(i, 1, n) if (!dfn[i]) tarjan(i) ;
	rep(i, 1, n) incir[i] = false ;
	rep(i, 1, n) if (bel[i] != i) incir[bel[i]] = true ;
	rep(i, 1, n) if (incir[bel[i]]) incir[i] = true ;
	rep(i, 1, n) deg[i] = 0 ;
	rep(i, 1, n) if (!incir[i]) 
		for (int j = lnk[i]; j; j = nxt[j]) {
			if (dis[i] + w[j] != dis[ter[j]] || incir[ter[j]]) continue ;
			++ deg[ter[j]] ;
		}	
	he = 0, ta = 0 ;
	rep(i, 1, n) if (!incir[i] && !deg[i]) opt[++ ta] = i ;
	for ( ; he != ta ; ) {
		int u = opt[++ he] ;
		for (int i = lnk[u]; i; i = nxt[i]) {
			if (dis[u] + w[i] != dis[ter[i]] || incir[ter[i]]) continue ;
			-- deg[ter[i]] ;
			if (!deg[ter[i]]) opt[++ ta] = ter[i] ;
		}
	}
}

inline void upd(int &x, int y) {
	x = (x + y) % mo ; 
}

bool dp(int L) {
	rep(i, 1, ta) {
		int u = opt[i] ;
		for (int j = lnk[u]; j; j = nxt[j]) {
			int v = ter[j] ;
			if (L + dis[u] + w[j] - dis[v] > d) continue ;
			upd(f[v][L + dis[u] + w[j] - dis[v]], f[u][L]) ;
			g[v][L + dis[u] + w[j] - dis[v]] |= g[u][L] ;
		}
	}
	rep(i, 1, n) if (incir[i] && g[i][L]) {
		if (dis[i] + dist[i] + L <= dis[n] + d) return false ;	
	}
	return true ;
}

void solve() {
	rep(i, 1, n) lnk[i] = 0 ;
	rep(i, 1, e) nxt[i] = 0 ; e = 0 ;
	scanf("%d%d%d%d", &n, &m, &d, &mo) ;
	int x, y, z ;
	rep(i, 1, m) {
		scanf("%d%d%d", &x, &y, &z) ;
		ed[i].x = x, ed[i].y = y, ed[i].z = z ;
	}
	build() ;
	pre() ;
	rep(i, 1, n) rep(j, 0, d) f[i][j] = 0, g[i][j] = false ;
	f[1][0] = 1, g[1][0] = true ;
	rep(j, 0, d) {
		if (!dp(j)) {
			printf("-1\n") ;
			return ;
		}
	} 
	int ans = 0 ;
	rep(i, 0, d) upd(ans, f[n][i]) ;
	printf("%d\n", ans) ;
}

int main() {
	n = e = 0 ;
	int T ;
	scanf("%d", &T) ;
	for ( ; T -- ; ) {
		solve() ;
	}
	return 0 ;
}

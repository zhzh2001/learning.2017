#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
using namespace std ;

int gcd(int x, int y) {
	if (!y) return x ; else return gcd(y, x % y) ;
}

int main() {
	srand(time(0)) ;
	int a = rand() % 1000000000, b = rand() % 1000000000 ;
	int d = gcd(a, b) ;
	printf("%d %d\n", a / d, b / d) ;
	return 0 ; 
}

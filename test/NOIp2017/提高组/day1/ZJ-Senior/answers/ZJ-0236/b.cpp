#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
const int constn = 1e9, N = 5000 ;
using namespace std ;

bool vis[N] ;
int st[N], val[N] ;
char S[N] ;

int getcomp() {
	if (S[3] == '1') return 0 ; 
	int i = 5, ret = 0 ;
	for ( ; S[i] != ')' ; ++ i) ret = ret * 10 + S[i] - '0' ;
	return ret ;
}

void solve() {
	int n ; 
	memset(vis, true, sizeof(vis)) ;
	scanf("%d%s", &n, S + 1) ;
	bool compile = true ;
	int comp = getcomp() ;
	int top = 0, ans = 0 ;
	rep(cas, 1, n) {
		scanf("%s", S + 1) ;
		if (S[1] == 'F') {
			scanf("%s", S + 1) ;
			if (!vis[S[1]]) compile = false ;
			st[++ top] = (int) S[1] ;
			vis[S[1]] = false ; 
			scanf("%s", S + 1) ;
			int x, y ;
			if (S[1] == 'n') {
				x = constn ;
			} else {
				x = 0 ;
				rep(i, 1, strlen(S + 1)) x = x * 10 + S[i] - '0' ;
			}
			scanf("%s", S + 1) ;
			if (S[1] == 'n') {
				y = constn ;
			} else {
				y = 0 ;
				rep(i, 1, strlen(S + 1)) y = y * 10 + S[i] - '0' ;
			}
			if (x > y) val[top] = - 1 ; else
			if (x == y) val[top] = 0 ; else
			if (y == constn) val[top] = 1 ; else val[top] = 0 ; 
		} else {
			if (!top) { compile = false ; continue ; }
			if (top - 1) {
				if (val[top - 1] != - 1) {
					if (val[top] > 0) val[top - 1] += val[top] ; 
				}
			}
			vis[st[top]] = true ;
			-- top ;
			if (!top) ans = max(ans, val[1]) ;
		}
	}
	if (top) compile = false ;
	if (!compile) printf("ERR\n") ; else
	if (comp == ans) printf("Yes\n") ; else 
		printf("No\n") ;
}

int main() {	
	int T ;
	scanf("%d", &T) ;
	for ( ; T -- ; ) {
		solve() ;
	}
	return 0 ; 
}	

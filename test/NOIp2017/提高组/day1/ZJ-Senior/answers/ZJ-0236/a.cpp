#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
using namespace std ;

ll A, B ;

ll ex_gcd(ll a, ll b, ll &x, ll &y) {
	if (!b) {
		x = 1, y = 0 ;
		return a ;
	} else {
		ll d = ex_gcd(b, a % b, y, x) ;
		y -= x * (a / b) ;
		return d ;
	}
}

void solve(ll a, ll b, ll &x, ll &y) {
	ll d = ex_gcd(a, b, x, y) ;
	x *= d, y *= d ;
	ll stx = b / d, sty = - a / d ;
	if (stx < 0LL) stx *= - 1, sty *= - 1 ;
	if (x < 0LL) {
		ll tmp = (- x) / stx ;
		x += tmp * stx, y += tmp * sty ;
		for ( ; x < 0LL ; ) x += stx, y += sty ;
	}
	if (y < 0LL) {
		ll tmp = (- y) / sty ;
		x += tmp * stx, y += tmp * sty ;
		for ( ; y < 0LL ; ) x += stx, y += sty ; 
	}
}

void pre() {
	ll ans = 1e18, x, y, x1, y1, x2, y2 ;
	solve(B, - A, y1, x1) ;
	solve(A, - B, x2, y2) ;
	x = x1, y = max(y2 - y1, 0LL) ;
	ans = min(ans, x * A + y * B) ;
	ll tmp = y / y1 ;
	x += tmp * x1, y -= tmp * y1 ;
	ans = min(ans, x * A + y * B) ;
	x += x1, y = 0 ;
	ans = min(ans, x * A + y * B) ;
	
	x = max(x1 - x2, 0LL), y = y2 ;
	ans = min(ans, x * A + y * B) ;
	tmp = x / x2 ;
	x -= tmp * x2, y += tmp * y2 ;
	ans = min(ans, x * A + y * B) ;
	x = 0, y += y2 ;
	ans = min(ans, x * A + y * B) ;
	printf("%lld %lld %lld\n", A, B, max(ans - 1, 1LL)) ;
}

int main() {
	scanf("%lld%lld", &A, &B) ;
	if (A > B) swap(A, B) ;
	pre() ;
	return 0 ;
}


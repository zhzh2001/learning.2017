#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
using namespace std ;

int main() {
	ll a, b, x ;
	scanf("%lld%lld%lld", &a, &b, &x) ;
	rep(i, 0, x / a) if ((x - a * i) % b == 0) {
		printf("NO\n") ;
		return 0 ;
	}
	printf("YES\n") ;
	return 0 ; 
}

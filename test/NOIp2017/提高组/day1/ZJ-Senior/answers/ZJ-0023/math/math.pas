type
  number=array[1..3]of int64;
const
  pow=1000000000;
var
  x1,y1,x,y,a,b,k,left,right,mid:int64;
  num1,num2:number;
  procedure times(var x:number;y:int64);
  var i:longint;
  begin
    for i:=1 to 3 do x[i]:=x[i]*y;
    for i:=1 to 2 do
      if x[i]>=pow then
      begin
        x[i+1]:=x[i+1]+x[i] div pow;
        x[i]:=x[i] mod pow;
      end;
  end;
  procedure jw(var num:number);
  var i:longint;
  begin
    for i:=1 to 2 do
      if num[i]>=pow then
      begin
        num[i+1]:=num[i+1]+num[i] div pow;
        num[i]:=num[i] mod pow;
      end;
  end;
  procedure division(var x:number;y:int64;boo:boolean);
  var i:longint; b:boolean;
  begin
    b:=false;
    for i:=3 downto 1 do
    begin
      if i>1 then x[i-1]:=x[i-1]+x[i]mod y*pow;
      if (i=1)and(boo)and(x[i] mod y>0) then b:=true;
      x[i]:=x[i] div y;
    end;
    if b then begin x[1]:=x[1]+1;  jw(x); end;
  end;
  procedure z(var num:number;x:int64);
  begin
    num[1]:=x; num[2]:=0; num[3]:=0;
    jw(num);
  end;
  function pd(mid:int64):boolean;
  var i:longint;
  begin
    z(num1,mid); z(num2,mid+1);
    times(num1,b); division(num1,x,false);
    inc(num1[1]); jw(num1);
    times(num2,a); division(num2,y,true);
    for i:=3 downto 1 do
      if num1[i]>num2[i] then exit(true)
      else if num1[i]<num2[i] then exit(false);
    exit(true);
  end;
  procedure exgcd(a,b:int64;var x,y:int64);
  begin
    if b=0 then
    begin x:=1; y:=0; exit; end;
    exgcd(b,a mod b,x,y);
    x1:=y; y1:=x-a div b*y;
    x:=x1; y:=y1;
  end;
  procedure csh;
  begin
    readln(a,b);
    if a>b then begin k:=a; a:=b; b:=k; end;
    exgcd(a,b,x,y);
    if x>0 then
    begin
      k:=x div b+1;
      x:=x-k*b;
      y:=y+k*a;
    end;
    x:=-x;
  end;
begin
  assign(input,'math.in'); reset(input);
  assign(output,'math.out'); rewrite(output);
  csh;
  left:=1; right:=a*b;
  while left<right do
  begin
    mid:=(left+right)shr 1;
    if pd(mid) then right:=mid
    else left:=mid+1;
  end;
  z(num1,right); times(num1,a); division(num1,y,true);
  k:=num1[2]*pow+num1[1]-1; write(k);
  close(input); close(output);
end.

var
  c:array[0..100000000] of longint;
  i,j,a,b,t:longint;

procedure swap(x,y:longint);
var
  k:longint;
begin
  k:=c[x];
  c[x]:=c[y];
  c[y]:=x;
end;

procedure sort;
var
  k,i,j:longint;
begin
  for i:=1 to t-2 do
    for j:=t-1 downto i+1 do
      if c[j]<c[j-1] then swap(j,j-1);
end;

begin
  assign(input,'math.in');
  reset(input);
  assign(output,'math.out');
  rewrite(output);
  read(a,b);
  t:=0;
  for i:=0 to a do
    for j:=0 to b do
    begin
      c[t]:=i*a+j*b;
      inc(t);
    end;
  sort;
  for i:=t-1 downto 1 do
  begin
    if c[i]-c[i-1]<>1 then break;
  end;
  writeln(c[i]-1);
  close(input);
  close(output);
end.

var
  i,t:longint;
  flag:boolean;
  x:char;
  l,j,stack:longint;
  o:string;
  len:array[0..110] of longint;
  pro:array[0..110] of string;
  n:array[0..110] of string;

procedure judge;

begin
  read(l);
  read(o);
  for j:=1 to l do
    begin
      read(pro[j]);
      len[j]:=length(pro[j]);
      if len[j]=1 then inc(stack);
    end;
  if stack<>(l div 2) then
    begin
      writeln('ERR');
      exit;
    end;
end;

begin
  assign(input,'complexity.in');
  reset(input);
  assign(output,'complexity.out');
  rewrite(output);
  read(t);
  for i:=1 to t do
    judge;



  close(input);
  close(output);
end.

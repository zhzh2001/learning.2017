#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
using namespace std;
typedef long long ll;
const int N = 202;
int n;
int ans;
struct DD {
	int num,flag;
	char Var;
} a[N];
char s[N],x[N],y[N];
inline int solve1(int l, int r);
inline int solve2(int l, int r);
inline int getnum(int p) {
	int mx=strlen(s+1),x=0; 
	while (p<=mx&&s[p]>='0'&&s[p]<='9') {x=10*x+s[p]-'0'; p++;}
	return x;
}
inline int getval(char *x) {
	 int mx=strlen(x+1),res=0,p=1;
	 if (x[1]=='n') return 10000;
	 while (p<=mx) {res=res*10+x[p]-'0'; p++;} return res;
}
struct GGG {
	int top,cnt[266];
	char stack[N];
	inline void init() {
		top=0; memset(cnt,0,sizeof(cnt));
	}
	inline bool judge() {
		rep(i,1,n) 
			if (a[i].flag==1) {
				if (top==0) return 1;
				cnt[stack[top]]--; top--;
			}
			else {
				if (cnt[a[i].Var]) return 1;
				cnt[a[i].Var]=1; stack[++top]=a[i].Var;
			}
		if (top>0) return 1;
		return 0;
	}
} gg;
inline int solve1(int l, int r) {
	if (l+1==r) return max(a[l].num,0); //!!!!!!!!!
	if (a[l].num==-1) return 0; //!!!!!!!!!!!!!!!
	return a[l].num+max(solve2(l+1,r-1),0);
}
inline int solve2(int l, int r) {
	if (l+1==r) return max(a[l].num,0); //!!!!!!!!!
	int top=0,res=-1,lst=l-1;
	rep(i,l,r) {
		if (a[i].flag==1) top--; else top++;
		if (!top) {int cur=solve1(lst+1,i); lst=i; res=max(res,cur);}
	}
	if (lst+1<=r) {int cur=solve1(lst+1,r); lst=r; res=max(res,cur);}
	return max(res,0);
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t; scanf("%d",&t);
	while (t--) {
		scanf("%d%s",&n,s+1);
		if (s[3]=='1') ans=0; else ans=getnum(5);
		rep(i,1,n) {
			scanf("%s",s+1);
			if (s[1]=='E') {a[i].flag=1;}
			else {
				a[i].flag=0; scanf("%s",s+1); a[i].Var=s[1];
				scanf("%s%s",x+1,y+1);
				int v1=getval(x),v2=getval(y);
				if (v2-v1>100) a[i].num=1;
				else if (v1>v2) a[i].num=-1;
				else a[i].num=0;
			}
		}
	//	rep(i,1,n) printf("%d %d %c\n",a[i].flag,a[i].num,a[i].Var);
	//	puts("");
		gg.init(); if (gg.judge()) {puts("ERR"); continue;}
		int now=solve2(1,n); if (now==-1) now=0;
		if (now==ans) puts("Yes"); else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
#include<map>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
#define L(i,u) for (int i=head[u]; i!=0; i=nxt[i])
using namespace std;
typedef long long ll;
typedef pair<int,int> Pii;
const int N = 200300, M = 403000, inf = 0x3f3f3f3f;
map<Pii,int> Val;
bool wuqiong;
int A[M],B[M],C[M];
int n,m,k,mo,f[N][55],g[N][55],h[N][55];
int head[N],nxt[M],to[M],Len[M],edgenum;
inline void add(int u, int v, int d) {
	to[++edgenum]=v; Len[edgenum]=d; nxt[edgenum]=head[u];
	head[u]=edgenum;
}
int q[N],dis[N];
bool exist[N];
inline void inc(int &x) {x++; if (x==N) x=0;}
inline void spfa() {
	int f=0,r=1; q[0]=1;
	memset(dis,inf,sizeof(dis)); dis[1]=0;
	memset(exist,0,sizeof(exist));
	while (f!=r) {
		int u=q[f]; inc(f); exist[u]=0;
		L(i,u) if (dis[to[i]]>dis[u]+Len[i]) {
			dis[to[i]]=dis[u]+Len[i];
			if (!exist[to[i]]) {exist[to[i]]=1; q[r]=to[i]; inc(r);}
		}
	}
}
int dfn[N],Low[N],num,stack[N],col[N],col_num,top;
bool instack[N];
vector<int> Dot[N];
inline void tarjan(int u) {
	dfn[u]=Low[u]=++num;
	instack[u]=1; stack[++top]=u;
	L(i,u) if (!dfn[to[i]]) {
		int v=to[i]; tarjan(v);
		Low[u]=min(Low[u],Low[v]);
	}
	else if (instack[to[i]]) Low[u]=min(Low[u],dfn[to[i]]);
	if (dfn[u]==Low[u]) {
		col_num++;
		while (1) {
			int dot=stack[top]; instack[dot]=0;
			Dot[col_num].push_back(dot);
			top--; col[dot]=col_num; if (dot==u) break;
		}
	}
}
int seq[N],deg[N];
inline void topsort() { //memset(deg,0,sizeof(deg));
	rep(u,1,n) L(i,u) if (col[u]!=col[to[i]]) deg[col[to[i]]]++;
	f[1][0]=1;
	int F=0,r=0; rep(i,1,col_num) if (!deg[i]) seq[r++]=i;
	while (F!=r) {
		int num=seq[F++],len=Dot[num].size();
	//	ll cirlen=0; rep(j,0,len-1) 
		rep(j,0,len-1) {int u=Dot[num][j]; memcpy(h[u],f[u],sizeof(f[u]));}
		rep(j,0,len-1) rep(i,0,k) g[Dot[num][j]][i]=0;
		rep(kkk,0,k) {
			rep(j,0,len-1) {
				int u=Dot[num][j];
				L(i,u) if (col[to[i]]==num) {
					rep(l,0,k) if (h[u][l] && dis[u]+l+Len[i]-dis[to[i]]<=k) {
						int &tmp=g[to[i]][dis[u]+l+Len[i]-dis[to[i]]];
						tmp+=h[u][l]; if (tmp>=mo) tmp-=mo;
					}
				}
			}
			bool flag=0;
			rep(j,0,len-1) {int u=Dot[num][j]; memcpy(h[u],g[u],sizeof(g[u]));}
			rep(j,0,len-1) rep(i,0,k) {
				int u=Dot[num][j];
				if (!g[u][i]) continue;
				flag=1; f[u][i]+=g[u][i]; if (f[u][i]>=mo) f[u][i]-=mo; g[u][i]=0;
			}
			if (!flag) break;
			if (kkk==k) {wuqiong=1; return;}
		}
		rep(j,0,len-1) {
			int u=Dot[num][j];
		//	printf("< %d %d >\n",num,u);
			L(i,u) {
				deg[col[to[i]]]--;
				if (!deg[col[to[i]]]) seq[r++]=col[to[i]];
				rep(l,0,k) if (f[u][l] && dis[u]+l+Len[i]-dis[to[i]]<=k) {
					int &tmp=f[to[i]][dis[u]+l+Len[i]-dis[to[i]]];
					tmp+=f[u][l]; if (tmp>=mo) tmp-=mo;
				//	printf("(%d %d)\n",l,tmp);
				}	
			}
		}
	}
//	printf("%d %d %d\n",col[1],deg[col[1]],r);
}
inline void init() {
	edgenum=0; memset(head,0,sizeof(head));
	num=col_num=top=0; memset(instack,0,sizeof(instack));
	memset(dfn,0,sizeof(dfn)); memset(Low,0,sizeof(Low));
	memset(col,0,sizeof(col)); memset(deg,0,sizeof(deg));
	rep(i,1,n) Dot[i].clear(); memset(f,0,sizeof(f));
	Val.clear(); wuqiong=0;
}
bool mrk[N];
inline void dfs(int u) {
	mrk[u]=1;
	L(i,u) if (dis[to[i]]+C[i]==dis[u] && !mrk[to[i]]) dfs(to[i]);
}
int rr[N];
inline void solve() {
	edgenum=0; memset(head,0,sizeof(head)); memset(mrk,0,sizeof(mrk));
	rep(i,1,m) add(B[i],A[i],C[i]);
	dfs(n);
	edgenum=0; memset(head,0,sizeof(head)); memset(exist,0,sizeof(exist));
	memset(rr,0,sizeof(rr));
	rep(i,1,m) add(A[i],B[i],C[i]);
	int f=0,r=1; q[f]=1; memset(rr,0,sizeof(rr)); rr[1]=1;
	while (f!=r) {
		int u=q[f]; inc(f); exist[u]=0;
		L(i,u) if (mrk[u] && mrk[to[i]] && dis[u]+Len[i]==dis[to[i]]) {
			rr[to[i]]+=rr[u]; rr[to[i]]%=mo;
			if (!exist[to[i]]) {exist[to[i]]=1; q[r]=to[i]; inc(r);}
		}
	}
//	rep(i,1,n) printf("%d ",mrk[i]); puts("");
//	rep(i,1,n) printf("%d ",rr[i]); puts("");
	printf("%d\n",rr[n]);
}

int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t; scanf("%d",&t);
	while (t--) {
		scanf("%d%d%d%d",&n,&m,&k,&mo); init();
		rep(i,1,m) {int a,b,c; scanf("%d%d%d",&a,&b,&c); A[i]=a; B[i]=b; C[i]=c; add(a,b,c); Val[Pii(a,b)]=c;}
		spfa(); //printf("%d\n",dis[n]);
		if (k==0) {solve(); continue;}
	//	printf("%d\n",dis[n]);
		tarjan(1); //(1 & n) is connected
	//	printf("%d %d %d\n",num,dfn[1],dfn[n]);
	//	rep(i,1,n) printf("%d ",col[i]); puts("");
		topsort(); int ans=0; rep(i,0,k) {ans+=f[n][i]; ans%=mo;}
		printf("%d\n",wuqiong ? -1 :ans);
	}
	return 0;
}

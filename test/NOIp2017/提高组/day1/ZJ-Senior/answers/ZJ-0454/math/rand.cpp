#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<ctime>
#include<cstdlib>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
using namespace std;
typedef long long ll;
const int N = 333333;
inline int gcd(int a, int b) {return !b ? a : gcd(b,a%b);}

int main() {
	srand(time(NULL));
	int a=rand()%1000+1,b=rand()%1000+1;
	while (gcd(a,b)!=1) {a=rand()%1000+1;b=rand()%1000+1;}
	printf("%d %d\n",a,b);
	return 0;
}

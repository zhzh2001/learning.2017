#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
using namespace std;
typedef long long ll;
const int N = 333333;
inline int gcd(int a, int b) {return !b ? a : gcd(b,a%b);}

int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b; scanf("%lld%lld",&a,&b);
	if (a>b) {ll t=a; a=b; b=t;}
	if (a==1) printf("0");
	else printf("%lld",(a-1)*b-a);
	/*for (a=2; a<=30; a++) for (b=a; b<=30; b++) if (gcd(a,b)==1) { //a<b!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		printf("%d %d : %d\n",a,b,(a-1)*b-a);
	}*/
	return 0;
}

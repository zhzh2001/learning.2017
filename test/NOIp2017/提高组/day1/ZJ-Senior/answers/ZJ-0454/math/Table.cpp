#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
using namespace std;
typedef long long ll;
const int N = 1000000;
bool ok[N+2];
inline int gcd(int a, int b) {return !b ? a : gcd(b,a%b);}
int main() {
//	freopen("math.in","r",stdin);
//	freopen("std.out","w",stdout);
	int a,b; scanf("%d%d",&a,&b);
	rep(i,0,50000) rep(j,0,50000) if (i*a+j*b<=N) ok[i*a+j*b]=1; else break;
	int ans=0; rep(i,1,N) if (!ok[i]) ans=i; 
	printf("%d",ans);
	/*for (a=2; a<=30; a++) for (b=a; b<=30; b++) if (gcd(a,b)==1) {
		memset(ok,0,sizeof(ok));
		rep(i,0,300) rep(j,0,300) if (i*a+j*b<=N) ok[i*a+j*b]=1;
		int ans=0; rep(i,1,900) if (!ok[i]) ans=i; printf("%d %d : %d\n",a,b,ans);
	//	rep(i,1,100) printf("%d : %d\n",i,ok[i]);
	}*/
	return 0;
}

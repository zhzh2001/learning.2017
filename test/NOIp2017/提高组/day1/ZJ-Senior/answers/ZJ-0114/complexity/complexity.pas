const s1='Yes'; s2='No'; s3='ERR';
var i,j,k,l,m,n,t,mm,max,maxx,tl:longint;
    s:string;      o,oo:boolean;
    f:array[1..26]of boolean;
procedure js(l,r:longint;var mm:longint);
begin
  if (l=-1)and(r<>-1)then begin mm:=0; exit; end;
  if (l<>-1)and(r=-1)then begin inc(mm); exit; end;
  if (l<>-1)and(r<>-1)and(l>r)then begin mm:=0; exit; end;
end;
procedure dfs(var max:longint);
var  p,l0,r,ll,kk,mm:longint;  s1:string;
begin
  if j=l then exit;
  readln(s); inc(j);
  if s[1]='F' then inc(tl);
  if s[1]='E' then dec(tl);
  if tl<0 then begin o:=true; exit; end;
  if (o)then exit;
  mm:=0;  max:=0; s1:=s;
  if s1[1]='F' then
  begin
    k:=2;  ll:=length(s1);
    while s1[k]=' ' do inc(k);
    p:=ord(s1[k])-96; inc(k);
    while s1[k]=' ' do inc(k);
    if s1[k]='n' then l0:=-1
    else begin
      val(s1[k],l0);
      while (k<ll)and(s1[k+1]<>' ') do
      begin
        inc(k);
        val(s1[k],kk);
        l0:=l0*10+kk;
      end;
    end;
    inc(k);
    while s1[k]=' ' do inc(k);
    if s1[k]='n' then r:=-1
    else begin
      val(s1[k],r);
      while (k<ll)and(s1[k+1]<>' ') do
      begin
        inc(k);
        val(s1[k],kk);
        r:=r*10+kk;
      end;
    end;
    if f[p]then begin o:=true; exit; end
    else f[p]:=true;
  end;
  while s[1]<>'E' do
  begin
    if j=l then begin o:=true; exit; end;
    dfs(mm);
    if max<mm then max:=mm;
  end;
  if s1[1]='F' then begin js(l0,r,max);  f[p]:=false; end;
  if (j<l)and(s1<>s) then
  begin
    inc(j); readln(s);
    if s[1]='F' then inc(tl);
    if s[1]='E' then dec(tl);
    if tl<0 then begin o:=true; exit; end;
  end;
end;
begin
  assign(input,'complexity.in');  assign(output,'complexity.out');
  reset(input); rewrite(output);
  readln(t);
  for i:=1 to t do
  begin
    fillchar(f,sizeof(f),0);
    readln(s);
    val(s[1],l);   k:=2;
    while s[k]<>' ' do
    begin val(s[k],m); l:=l*10+m; inc(k); end;
    k:=pos('(',s); delete(s,1,k);
    m:=pos(')',s); delete(s,m,length(s)-m+1);
    m:=0;
    if length(s)<>1 then
    begin
      k:=pos('^',s); delete(s,1,k);
      for j:=1 to length(s) do
      begin
        val(s[j],t);
        m:=m*10+t;
      end;
    end;
    o:=false;  j:=0; max:=0; maxx:=0; tl:=0;
    while (j<l) do
    begin
      dfs(max);
      if max>maxx then maxx:=max;
    end;
    if (o) then writeln(s3)
    else if m=maxx then writeln(s1)
         else writeln(s2);
  end;
  close(input); close(output);
end.
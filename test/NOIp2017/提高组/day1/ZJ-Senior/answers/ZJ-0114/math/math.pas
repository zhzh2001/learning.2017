var a,b,i,j,n,x1,y1,x2,y2,t:int64;
procedure exgcd(a,b:int64;var x,y:int64);
begin
  if b=0 then
  begin
    x:=1; y:=0;
  end
  else begin
    exgcd(b,a mod b,x,y);
    t:=x; x:=y; y:=t-(a div b)*y;
  end;
end;
begin
  assign(input,'math.in');  assign(output,'math.out');
  reset(input); rewrite(output);
  readln(a,b);
  exgcd(a,b,x1,y1);
  if x1>0 then begin x1:=x1-b; y1:=y1+a; end;
  x2:=x1+b; y2:=y1-a;
  i:=(abs(y2)-1)div y1+1;
  j:=(abs(x1)-1)div x2+1;
  if i=0 then i:=i+1;
  if j=0 then j:=j+1;
  i:=abs(i*a*x1);
  j:=abs(j*b*y2);
  if i<j then writeln(i-1) else writeln(j-1);
  close(input); close(output);
end.

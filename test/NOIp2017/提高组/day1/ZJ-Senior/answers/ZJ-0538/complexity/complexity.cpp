#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
const int N=1000;

int T,n,pre,id,top,x,y,feng,ci,ans,err;
char st[20],opt[10],ch[10],sx[10],sy[10];
bool used[40];
int sta[200];

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		feng=N; ci=0; ans=0; top=0; err=0;
		memset(used,0,sizeof(used));
		scanf("%d",&n);
		scanf("%s",st);
		if (st[2]=='1') pre=0; else
		{
			pre=0;
			for (int i=4;st[i]>='0' && st[i]<='9';i++) pre=pre*10+st[i]-'0';
		}
		
		for (int i=1;i<=n;i++)
		{
			scanf("%s",opt);
			if (opt[0]=='F')
			{
				scanf("%s",ch);
				id=ch[0]-'a';
				if (used[id]) err=1;
				sta[++top]=id;
				used[id]=1;
				
				scanf("%s%s",sx,sy);
				if (top<=feng)
				{
					if (feng!=N) feng=N;
					if (sx[0]=='n') x=N; else {
						x=0;
						for (int i=0;i<(int)strlen(sx);i++) x=x*10+sx[i]-'0';
					}
					if (sy[0]=='n') y=N; else {
						y=0;
						for (int i=0;i<(int)strlen(sy);i++) y=y*10+sy[i]-'0';
					}
					if (x>y) feng=top; else
					{
						if (x<=100 && y==N) ci++;
					}
				}
			} else
			{
				if (top<1) err=1;
				used[sta[top--]]=0;
				if (!top)
				{
					ans=max(ans,ci);
					ci=0; feng=N;
				}
			}
		}
		
		if (top>0) err=1;
		if (err) printf("ERR\n"); else
		{
			if (ans==pre) printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

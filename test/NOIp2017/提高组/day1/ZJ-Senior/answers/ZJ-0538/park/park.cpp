#include <cstdio>
#include <queue>
#include <cstring>
using namespace std;
#define N 101000

struct node {
	int u,d;
	bool operator < (const node& x) const {
		return d>x.d;
	}
};
struct edge {
	int to,nex,dis;
}e[N<<1],e2[N<<1];
priority_queue<node> Q;
int T,n,m,k,p,he,ta,cnt,cnt2,ans;
int d1[N],d2[N],done[N],head[N],head2[N],f[N],q[N*50][2];

void add(int x,int y,int z) {
	e[++cnt].to=y;
	e[cnt].nex=head[x];
	e[cnt].dis=z;
	head[x]=cnt;
}

void add2(int x,int y,int z) {
	e2[++cnt2].to=y;
	e2[cnt2].nex=head2[x];
	e2[cnt2].dis=z;
	head2[x]=cnt2;
}

void dijks(int st)
{
	memset(done,0,sizeof(done));
	memset(d1,0x3f,sizeof(d1));
	d1[st]=0; f[st]=1;
	Q.push((node){st,0});
	while (!Q.empty())
	{
		node x=Q.top(); Q.pop();
		if (done[x.u]) continue;
		done[x.u]=1;
		
		for (int i=head[x.u];i;i=e[i].nex)
		{
			int v=e[i].to;
			if (d1[x.u]+e[i].dis<d1[v])
			{
				d1[v]=d1[x.u]+e[i].dis;
				f[v]=f[x.u];
				Q.push((node){v,d1[v]});
			} else
			if (d1[x.u]+e[i].dis==d1[v]) f[v]+=f[x.u],f[v]%=p;
		}
	}
}

void dijks2(int st)
{
	memset(done,0,sizeof(done));
	memset(d2,0x3f,sizeof(d2));
	d2[st]=0;
	Q.push((node){st,0});
	while (!Q.empty())
	{
		node x=Q.top(); Q.pop();
		if (done[x.u]) continue;
		done[x.u]=1;
		
		for (int i=head2[x.u];i;i=e2[i].nex)
		{
			int v=e2[i].to;
			if (d2[x.u]+e2[i].dis<d2[v])
			{
				d2[v]=d2[x.u]+e2[i].dis;
				Q.push((node){v,d2[v]});
			}
		}
	}
}

void BFS(int st)
{
	q[1][0]=st; q[1][1]=0;
	he=0; ta=1;
	while (he<ta)
	{
		int x=q[++he][0],y=q[he][1];
		if (x==n && y>=d1[n] && y<=d1[n]+k) {ans++,ans%=p; continue;}
		for (int i=head[x];i;i=e[i].nex)
		{
			int v=e[i].to;
			if (y+e[i].dis+d2[v]<=d1[n]+k)
			{
				q[++ta][0]=v;
				q[ta][1]=y+e[i].dis;
			}
		}
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		bool flag=1;
		cnt=0;
		memset(head,0,sizeof(head));
		memset(head2,0,sizeof(head2));
		memset(f,0,sizeof(f));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=m;i++) {
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z); add2(y,x,z);
			if (z==0)
			{
				printf("-1\n");
				flag=0;
				break;
			}
		}
		if (flag)
		{
			dijks(1); dijks2(n);
			if (k==0 && n>1000) printf("%d\n",f[n]); else
			{
				BFS(1);
				printf("%d\n",ans%p);
			}
		}
	}
	return 0;
}

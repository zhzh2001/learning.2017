#include <cstdio>
#include <iostream>
using namespace std;
#define ll long long

ll a,b;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a==1 || b==1) printf("0\n"); else
	{
		if (a>b) swap(a,b);
		printf("%lld\n",a*(b-1)-b);
	}
	return 0;
}

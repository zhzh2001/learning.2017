//#include<iostream>
#include<algorithm>
#include<math.h>
#include<cstdio>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
long long gcd(long long a,long long b){
	if(b==0){
		return a;
	}
	return gcd(b,a%b);
}
inline int max(const int &a,const int &b){
	if(a>b) return a;
	return b;
}
long long a,b;
bool beibao[2000003];
inline void baoli(){
	if(a==1||b==1){
		//cout<<0<<" ";
		return;
	}
	memset(beibao,0,sizeof(beibao));
	int mx=max(a,b),mn=min(a,b),lian=1,ans=0;
	beibao[0]=1;
	for(int i=1;i<=20000000;i++){
		if(i>=a)beibao[i]|=beibao[i-a];
		if(i>=b)beibao[i]|=beibao[i-b];
		lian=(beibao[i])?++lian:0;
		if(lian>mx){
			ans=i;
			break;
		}
	}
	//cout<<ans-mx-1<<endl;
}
int main(){
	//freopen("math.in","r",stdin);
	//freopen("math.out","w",stdout);
	fin>>a>>b;
	if(a==1||b==1){
		fout<<0<<endl;
		return 0;
	}
	fout<<(a*b-(a+b))<<endl;
	return 0;
}

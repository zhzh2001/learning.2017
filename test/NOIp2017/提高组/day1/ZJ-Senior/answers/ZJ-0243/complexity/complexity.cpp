//#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<fstream>
#include<stack>
#include<string.h>
#include<map>
//#define fin cin
//#define fout cout
using namespace std;
ifstream fin("complexity.in");
ofstream fout("complexity.out");
int num;
stack<string>s;
stack<bool>s2;
stack<bool>s3;
map<string,int>mp;
inline int max(const int &a,const int &b){
	if(a>b) return a;
	return b;
}
inline int calc(string x){
	if((x.length())>=2){
		return (x[0]-'0')*10+x[1]-'0';
	}else{
		return x[0]-'0';
	}
}
inline void solve(){
	mp.clear();
	while(!s.empty()){
		s.pop();
	}
	while(!s2.empty()){
		s2.pop();
	}
	while(!s3.empty()){
		s3.pop();
	}
	int ljz=0,flag=0;
	string ss,bian1,bian2,bian3;
	fin>>num>>ss;
	if(ss[2]=='1'){
		ljz=0;
	}else{
		if(ss[5]>='0'&&ss[5]<='9'){
			ljz=(ss[4]-'0')*10+ss[5]-'0';
		}else{
			ljz=(int)(ss[4]-'0');
		}
	}
	char opt;
	bool yes=false;
	int mx=0,xx=0;
	for(int i=1;i<=num;i++){
		fin>>opt;
		if(opt=='F'){
			fin>>bian1>>bian2>>bian3;
			if(mp[bian1]==1){
				yes=true;
			}
			if((bian2=="n"&&bian3!="n")||((bian2!="n"&&bian3!="n")&&calc(bian2)>calc(bian3))){
				s3.push(1);
				flag++;
			}else{
				s3.push(0);
			}
			if(bian2!="n"&&bian3=="n"){
				s2.push(1);
				xx+=(flag==0);
			}else{
				s2.push(0);
			}
			s.push(bian1);
			mp[bian1]=1;
		}else{
			if(s.empty()||s2.empty()||s3.empty()){
				yes=true;
				continue;
			}
			if(s3.top()){
				flag--;
			}
			if(s2.top()){
				xx-=(flag==0);
			}
			mp[s.top()]=0;
			s.pop();
			s2.pop();
			s3.pop();
		}
		mx=max(mx,xx);
	}
	if(yes||!s.empty()){
		fout<<"ERR"<<endl;
		return;
	}
	if(mx==ljz){
		fout<<"Yes"<<endl;
	}else{
		fout<<"No"<<endl;
	}
}
int T;
int main(){
	//freopen("complexity.in","r",stdin);
	//freopen("complexity.out","w",stdout);
	fin>>T;
	while(T--){
		solve();
	}
	return 0;
}
/*
1
10 O(n^2)
F x 1 n
F y 1 n
F z n 1
E
E
F y 1 n
F z n 1
E
E
E

18 O(n^4)
F a 5 71
F b 35 n
F c 90 94
F d 45 19
F e 1 n
F f 86 n
F g 49 n
E
E
E
E
E
E
E
F a 62 n
F b 82 85
E
E



in:
8
2 O(1)
F i 1 1
E
2 O(n^1)
F x 1 n
E
1 O(1)
F x 1 n
4 O(n^2)
F x 5 n
F y 10 n
E
E
4 O(n^2)
F x 9 n
E
F y 2 n
E
4 O(n^1)
F x 9 n
F y n 4
E
E
4 O(1)
F y n 4
F x 9 n
E
E
4 O(n^2)
F x 1 n
F x 1 10
E
E

out:
Yes
Yes
ERR
Yes
No
Yes
Yes
ERR

*/

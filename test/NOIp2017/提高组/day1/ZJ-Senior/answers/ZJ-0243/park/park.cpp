//#include<iostream>
#include<algorithm>
#include<math.h>
#include<cstdio>
#include<fstream>
#include<map>
#include<queue>
#include<string.h>
//#define fin cin
//#define fout cout
using namespace std;
//ifstream fin("park.in");
//ofstream fout("park.out");
inline int read(){
	int x=0,w=0;char ch=getchar();
	while(ch>'9'||ch<'0'){w|=(ch=='-'),ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+(ch^48),ch=getchar();}
	return w?-x:x;
}
inline void write(int x){
	if(x<0){putchar('-'),x=-x;}
	if(x>9){write(x/10);}
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x),puts("");
}
int p;
int head[100003],to[200003],nxt[200003],val[200003],tot;
inline void add(int a,int b,int c){
	to[++tot]=b;  nxt[tot]=head[a];  head[a]=tot;  val[tot]=c;
}
long long f[100003];
int inq[100003],dis[100003];
inline void baoli(int n){
	for(int i=1;i<=n;i++){
		inq[i]=f[i]=0;
		dis[i]=200000001;
	}
	queue<int>q;
	q.push(1);
	dis[1]=0;
	f[1]=1;
	inq[1]=1;
	int x;
	while(!q.empty()){
		x=q.front();
		q.pop();
		inq[x]=0;
		for(int i=head[x];i;i=nxt[i]){
			if(dis[to[i]]>dis[x]+val[i]){
				dis[to[i]]=dis[x]+val[i];
				f[to[i]]=f[x];
				if(!inq[to[i]]){
					inq[to[i]]=1;
					q.push(to[i]);
				}
			}else if(dis[to[i]]==dis[x]+val[i]){
				f[to[i]]=(f[to[i]]+f[x])%p;
			}
		}
	}
	writeln(f[n]);
}
int T,n,m,k;
map<int,int>dp[10003];
inline void solve(){
	for(int i=1;i<=n;i++){
		inq[i]=0;
		dis[i]=200000001;
		dp[i].clear();
	}
	queue<int>q;
	q.push(1);
	dis[1]=0;
	dp[1][0]=1;
	inq[1]=1;
	int x;
	while(!q.empty()){
		x=q.front();
		q.pop();
		inq[x]=0;
		for(int i=head[x];i;i=nxt[i]){
			dp[to[i]][dis[x]+val[i]]+=dp[x][dis[x]];
			dp[to[i]][dis[x]+val[i]]%=p;
			if(dis[to[i]]>dis[x]+val[i]){
				dis[to[i]]=dis[x]+val[i];
				if(!inq[to[i]]){
					inq[to[i]]=1;
					q.push(to[i]);
				}
			}
		}
	}
	long long ans=0;
	for(int i=dis[n];i<=dis[n]+k;i++){
		ans=(ans+dp[n][i])%p;
	}
	writeln(ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	//cout<<T<<endl;
	while(T--){
		tot=0;
		memset(head,0,sizeof(head));
		n=read(),m=read(),k=read(),p=read();
		for(int i=1;i<=m;i++){
			int a,b,w;
			a=read(),b=read(),w=read();
			add(a,b,w);
		}
		if(k==0){
			baoli(n);
		}else{
			solve();
		}
	}
	return 0;
}
/*

in:
2
5 7 0 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
out:

*/

#include<algorithm>
#include<cstdio>
#include<iostream>
#include<cstring>
#include<queue>
#define N 100005
#define M 400005
using namespace std;
int T,ans,n,m,k,mod,x,y,z,cnt,st[N],ne[M],to[M],d[N],v[M],dp[N][61];
bool flag[N];
struct cmp{bool operator()(int x,int y){return d[x]>d[y];}};
priority_queue<int,vector<int>,cmp> q;
int read(){
	char ch=getchar();int s=0;
	while(ch>'9'||ch<'0') ch=getchar();
	while(ch<='9'&&ch>='0'){s=s*10+ch-'0';ch=getchar();}
	return s;
}
void add(int x,int y,int z){to[++cnt]=y;ne[cnt]=st[x];st[x]=cnt;v[cnt]=z;}
void Dj(){
	memset(d,-1,sizeof(d));
	memset(flag,0,sizeof(flag));
	for(int i=1;i<=n;i++) dp[i][0]=0;
	d[1]=0;flag[1]=1;
	dp[1][0]=1;q.push(1);
	while(!q.empty()){
		int x=q.top();q.pop();flag[x]=0;
		for(int i=st[x];i;i=ne[i])
		if(d[x]+v[i]<d[to[i]]||d[to[i]]==-1){
			d[to[i]]=d[x]+v[i];
			dp[to[i]][0]=dp[x][0];
			if(!flag[to[i]]){flag[to[i]]=1;q.push(to[i]);}
		}
		else if(d[x]+v[i]==d[to[i]]) dp[to[i]][0]=(dp[to[i]][0]+dp[x][0])%mod;
	}
}
void work(int y){
	for(int i=1;i<=n;i++) dp[i][y]=0;
	for(int i=1;i<=n;i++)
	if(d[i]!=-1)
		for(int j=st[i];j;j=ne[j]){
		int t=y-v[j]-d[i]+d[to[j]];
		if(t>0&&d[to[j]]!=-1) dp[to[j]][y]=(dp[to[j]][y]+dp[i][t])%mod;
		}
	for(int i=1;i<=n;i++)
	if(d[i]!=-1)
		for(int j=st[i];j;j=ne[j]){
		int t=y-v[j]-d[i]+d[to[j]];
		if(t==0&&d[to[j]]!=-1) dp[to[j]][y]=(dp[to[j]][y]+dp[i][t])%mod;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--){
		n=read();m=read();k=read();mod=read(); 
		cnt=0;memset(st,0,sizeof(st));
		for(int i=1;i<=m;i++){x=read();y=read();z=read();add(x,y,z);}
		Dj();ans=0;
		for(int i=1;i<=k;i++) work(i);
		for(int i=0;i<=k;i++) ans=(ans+dp[n][i])%mod;
		printf("%d\n",ans);
	}
}

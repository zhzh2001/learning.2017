#include<cstdio>
#include<algorithm>
#include<cstdio>
#include<cstring>
#define N 1005
using namespace std;
struct node{char ch;int s;};
int T,n,m,len,tot,ans,Len[N],b[N];
bool flag;
char str[4][N],a[N];
bool isnum(char ch){if(ch>='0'&&ch<='9') return true;else return false;}
bool pd(char ch){
	if(ch>='0'&&ch<='9') return true;
	if(ch>='a'&&ch<='z') return true;
	if(ch=='E'||ch=='F') return true;
	return false;
}
void get(){
	m=0;
	char ch=getchar();bool flag=false;
	while(ch!=')'){
		if(ch=='n') flag=true;
		if(isnum(ch)) m=m*10+ch-'0';
		ch=getchar();
	}
	if(!flag) m=0;
}
void Get(int x){
	char ch=getchar();
	while(!pd(ch)) ch=getchar();
	while(pd(ch)){str[x][++Len[x]]=ch;ch=getchar();}
}
int Val(int x){
	int s=0;
	for(int i=1;i<=Len[x];i++)
	if(isnum(str[x][i])) s=s*10+str[x][i]-'0';
	else break;
	return s;
}
void work(){
	Get(2);Get(3);Get(4);
	if(!flag) return;
	for(int i=1;i<=tot;i++)
	if(a[i]==str[2][1]){flag=false;return;}
	a[++tot]=str[2][1];
	if(!isnum(str[3][1])){
		if(!isnum(str[4][1])) b[tot]=1;
		else b[tot]=-1;
	}
	else{
		if(!isnum(str[4][1])) b[tot]=10000;
		else{
			int s1=Val(3),s2=Val(4);
			if(s1<=s2) b[tot]=1;else b[tot]=-1;
		}
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);get();
		tot=0;len=2;ans=-1;flag=true;
		for(int i=1;i<=n;i++){
			for(int j=1;j<=4;j++) Len[j]=0;
			Get(1);
			if(str[1][1]=='F') work();
			else tot--;
			if(!flag) continue;
			if(tot<0) flag=false;
			int sum=0;
			for(int j=1;j<=tot;j++)
			if(b[j]==10000) sum++;
			else if(b[j]==-1) break;
			ans=max(ans,sum);
		}
	 	if(tot>0||!flag) printf("ERR\n");
		else{
			if(ans==m) printf("Yes\n");
			else printf("No\n");
		}
	}
}

#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=305;
int T,n,m,cnt,c,li[N][2],ans[N],tp[N],Mx;
char ch,v[N]; bool fl,app[N];
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T),ch='#';
	for (; T; --T) {
		cnt=c=n=m=Mx=0,fl=1;
		memset(li,0,sizeof li);
		memset(v,0,sizeof v);
		memset(app,0,sizeof app);
		memset(tp,255,sizeof tp);
		while (ch<'0'||ch>'9') ch=getchar();
		while (ch>='0'&&ch<='9') {
			n=n*10+ch-'0';
			ch=getchar();
		}
		while (ch!='(') ch=getchar();
		ch=getchar();
		if (ch!='1') {
			while (ch<'0'||ch>'9') ch=getchar();
			while (ch>='0'&&ch<='9') {
				c=c*10+ch-'0';
				ch=getchar();
			}
		} else ch=getchar();
		ans[0]=0,tp[0]=0;
		for (int i=1; i<=n; ++i) {
			while (ch!='F'&&ch!='E'&&(ch<'0'||ch>'9')) ch=getchar();
			if (ch>='0'&&ch<='9') {
				if (cnt>0) fl=0;
			}
			if (ch=='E') {
				if (cnt==0) fl=0; else {
					app[v[cnt]-'a']=0;
					ans[cnt]=max(ans[cnt+1],0)+ans[cnt];
					--cnt;
					tp[cnt]=max(ans[cnt+1],tp[cnt]);
					if (cnt==0) Mx=max(Mx,tp[0]);
				}
				ch=getchar();
			} else
			if (ch=='F') {
				while (ch<'a'||ch>'z') ch=getchar();
				if (app[ch-'a']) fl=0;
				v[++cnt]=ch,app[ch-'a']=1,m=cnt,tp[cnt]=ans[cnt]=ans[cnt+1]=-1;
				while (ch!='n'&&(ch<'0'||ch>'9')) ch=getchar();
				li[cnt][0]=0;
				while (ch>='0'&&ch<='9') {
					li[cnt][0]=li[cnt][0]*10+ch-'0';
					ch=getchar();
				}
				if (ch=='n') li[cnt][0]=-1;
				ch=getchar();
				while (ch!='n'&&(ch<'0'||ch>'9')) ch=getchar();
				li[cnt][1]=0;
				while (ch>='0'&&ch<='9') {
					li[cnt][1]=li[cnt][1]*10+ch-'0';
					ch=getchar();
				}
				if (ch=='n') li[cnt][1]=-1;
				if (li[cnt][0]!=-1&&li[cnt][1]==-1) ans[cnt]=1; else
				if (li[cnt][0]==-1&&li[cnt][1]!=-1) ans[cnt]=max(ans[cnt],-1); else
				if (li[cnt][0]>li[cnt][1]&&li[cnt][0]!=-1&&li[cnt][1]!=-1) ans[cnt]=max(ans[cnt],-1);
				else ans[cnt]=max(ans[cnt],0);
			}
		}
		if (!fl||cnt>0) puts("ERR"); else {
			if (c==Mx) puts("Yes"); else puts("No");
		}
	}
	return 0;
}

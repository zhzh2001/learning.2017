#include <cstdio>
#include <cstring>
#include <algorithm>
#define LL long long
#define ms(a,x) memset(a,x,sizeof a)
using namespace std;
const int N=100005,M=200005;
int n,m,k,p,tot,lnk[N],nxt[M],son[M],w[M];
int tt,lk[N],nt[M],sn[M];
int dis[N],f[N],q[N*2],l,r; bool vis[N];
int g[51][N],d,ans;
inline int readint() {
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
void adde(int x,int y,int z) {
	nxt[++tot]=lnk[x],lnk[x]=tot,son[tot]=y,w[tot]=z;
}
void addn(int x,int y) {
	nt[++tt]=lk[x],lk[x]=tt,sn[tt]=y;
}
bool spfa() {
	ms(dis,30),ms(vis,0),ms(f,0);
	dis[1]=0,f[1]=1,q[1]=1,l=0,r=1;
	for (int x; l!=r; ) {
		l=(l+1)%N,vis[x=q[l]]=0;
		for (int j=lnk[x]; j; j=nxt[j]) {
			if (dis[son[j]]>=dis[x]+w[j]) {
				dis[son[j]]=dis[x]+w[j];
				if (!vis[son[j]]) {
					r=(r+1)%N,vis[q[r]=son[j]]=1,++f[son[j]];
					if (f[son[j]]>n) return 1;
				}
			}
		}
	}
	return 0;
}
void topo() {
	ms(vis,0);
	l=0,r=0;
	for (int i=1; i<=n; ++i) if (f[i]==0) q[++r]=i;
	for (int x; l!=r; ) {
		++l,x=q[l];
		for (int j=lk[x]; j; j=nt[j]) if (f[sn[j]]>0) {
			--f[sn[j]];
			if (f[sn[j]]==0) q[++r]=sn[j];
		}
	}
}
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (int T=readint(); T; --T) {
		tot=0,ms(lnk,0),ms(nxt,0);
		tt=0,ms(lk,0),ms(nt,0);
		n=readint(),m=readint(),k=readint(),p=readint();
		for (int i=1,x,y,z; i<=m; ++i) {
			x=readint(),y=readint(),z=readint();
			adde(x,y,z);
		}
		if (spfa()) {puts("-1"); continue;}
		ms(g,0),g[0][1]=1,ms(f,0);
		for (int i=1; i<=n; ++i)
		for (int j=lnk[i]; j; j=nxt[j])
		if (dis[son[j]]==dis[i]+w[j]) ++f[son[j]],addn(i,son[j]);
		topo();
		for (int t=0; t<=k; ++t)
		for (int x=1,i; x<=n; ++x) {
			i=q[x];
			for (int j=lnk[i]; j; j=nxt[j]) {
				d=dis[son[j]]-dis[i];
				if (w[j]-d<=t) {
					g[t][son[j]]+=g[t-(w[j]-d)][i];
					if (g[t][son[j]]>=p) g[t][son[j]]-=p;
				}
			}
		}
		ans=0;
		for (int t=0; t<=k; ++t) ans=(ans+g[t][n])%p;
		printf("%d\n",ans);
	}
	return 0;
}

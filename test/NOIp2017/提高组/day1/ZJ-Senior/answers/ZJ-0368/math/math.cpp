#include <cstdio>
#include <algorithm>
using namespace std;
int A,B,C; bool f[100000005];
int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&A,&B),C=A*B;
	if (A>B) swap(A,B);
	f[0]=f[A]=f[B]=1;
	for (int i=A+1; i<=C; ++i) f[i]=f[i-A]||f[i-B];
	for (int i=C; i; --i) if (!f[i]) {printf("%d\n",i); return 0;}
	return 0;
}

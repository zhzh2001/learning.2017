#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
int i, j, k, n, top, m, s, t, ans, L, w, x;
char c[10005];
char a[105], b[105], I[105];
int stk[1000005];
int sta[1000005];
int trans(char *a) {
	int ret = 0;
	for (int i = 1; i <= strlen(a + 1); i++) {
		ret = ret * 10 + a[i] - '0';
	}
	return ret;
}
namespace D1 {
	void DO() {
		int maxp = 0, nowp = 0, A = 0;
		top = 0;
		int ok = 0;
		scanf("%d", &L);
		scanf("%s", c + 1);
		if (c[3] == '1') {
			x = 0;
		} else {
			x = 0;
			for (int i = 5; i < strlen(c + 1); i++) {
				x = x * 10 + c[i] - '0';
			}
		}
		for (int i = 1; i <= L; i++) {
			scanf("%s", c + 1);
			if (c[1] == 'E') {
				if (sta[top] == 0) {
					ok--;
				}
				if (sta[top] == 2) {
					if (ok == 0) {
						nowp--;
					}
				}
				top--;
				A--;
			} else {
				A++;
				scanf("%s %s %s", I + 1, a + 1, b + 1);
				if (a[1] == 'n' && b[1] != 'n') {
					ok++;
					stk[++top] = A;
					sta[top] = 0;
				} else if (a[1] != 'n' && b[1] == 'n') {
					if (ok == 0) {
						nowp++;
						maxp = max(maxp, nowp);
					}
					stk[++top] = A;
					sta[top] = 2;
				} else if (a[1] != 'n' && b[1] != 'n' && trans(a) > trans(b)) {
					ok++;
					stk[++top] = A;
					sta[top] = 0;
				} else {
					stk[++top] = A;
					sta[top] = 1;
				}
			}
		}
		if (maxp == x) {
			puts("Yes");
		} else {
			puts("No");
		}
	}
}
int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) {
		D1 :: DO();
	}
	return 0;
}
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define MOD 1000000
using namespace std;
const int LEN = 1e5 + 5;
const int oo = 1e9;
int i, j, k, n, m, s, t, ans, tot, mod;
int head[LEN], dis[LEN], ind[LEN];
int q[1000005];
int dp[LEN][55];
bool flag[LEN];
struct edge {
	int vet, next, len;
} E[LEN * 2];
void add(int u, int v, int c) {
	E[++tot] = (edge){v, head[u], c};
	head[u] = tot;
}
void spfa(int s) {
	for (int i = 1; i <= n; i++) {
		dis[i] = oo;
		flag[i] = false;
	}
	int qhead = 0, qtail = 1;
	dis[s] = 0;
	flag[s] = true;
	q[1] = s;
	while (qhead < qtail) {
		qhead++;
		int u = q[qhead % MOD];
		flag[u] = false;
		for (int e = head[u]; e != -1; e = E[e].next) {
			int v = E[e].vet;
			if (dis[v] > dis[u] + E[e].len) {
				dis[v] = dis[u] + E[e].len;
				if (flag[v] == false) {
					flag[v] = true;
					qtail++;
					q[qtail % MOD] = v;
				}
			}
		}
	}
}
void Plus(int &x, int y) {
	x += y;
	if (x >= mod) {
		x -= mod;
	}
}
void topsort() {
	dp[1][0] = 1;
	for (int i = 1; i <= n; i++) {
		for (int e = head[i]; e != -1; e = E[e].next) {
			int v = E[e].vet;
			if (dis[i] + E[e].len == dis[v]) {
				ind[v]++;
			}
		}
	}
	int qhead = 0, qtail = 1;
	q[1] = 1;
	while (qhead < qtail) {
		int u = q[++qhead];
		for (int e = head[u]; e != -1; e = E[e].next) {
			int v = E[e].vet;
			if (dis[u] + E[e].len == dis[v]) {
				ind[v]--;
				if (ind[v] == 0) {
					q[++qtail] = v;
				}
			}
		}
	}
}
void DP(int t) {
	for (int i = 1; i <= n; i++) {
		int u = q[i];
		if (dp[u][t] == 0) {
			continue;
		}
		for (int e = head[u]; e != -1; e = E[e].next) {
			int v = E[e].vet, w = dis[u] + t + E[e].len - dis[v];
			if (w <= k) {
				Plus(dp[v][w], dp[u][t]);
			}
		}
	}
}
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) {
		memset(head, -1, sizeof(head));
		memset(dp, 0, sizeof(dp));
		memset(ind, 0, sizeof(ind));
		tot = -1;
		scanf("%d %d %d %d", &n, &m, &k, &mod);
		for (int i = 1; i <= m; i++) {
			int x, y, z;
			scanf("%d %d %d", &x, &y, &z);
			add(x, y, z);
		}
		spfa(1);
		topsort();
		for (int i = 0; i <= k; i++) {
			DP(i);
		}
		ans = 0;
		for (int i = 0; i <= k; i++) {
			Plus(ans, dp[n][i]);
		}
		printf("%d\n", ans);
	}
	return 0;
}
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
typedef long long ll;
int i, j, k, n, m, s, t, ans;
ll a, b;
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld %lld", &a, &b);
	if (a == 1) {
		puts("0");
	} else {
		printf("%lld\n", a * b - (a + b));
	}
	return 0;
}
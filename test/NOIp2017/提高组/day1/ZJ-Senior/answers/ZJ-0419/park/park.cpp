#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
using namespace std;
#define INF 0x3f3f3f3f
int t,ans,n,m,k,p;
int d[1111],e[1111][1111],f[1111],w[1111][1111];
bool vis[1111];

void dijkstra(){
	memset(d,0x3f,sizeof(d));
	memset(vis,false,sizeof(vis));
	memset(f,0,sizeof(f));
	d[1]=0; vis[1]=true; f[1]=1;
	int st=1;
	for (int i=1; i<=n-1; i++){
		for (int j=1; j<=e[st][0]; j++){
			int v=e[st][j];
			if (!vis[v]&&d[st]+w[st][v]<=d[v]){
				d[v]=d[st]+w[st][v];
				if (d[st]+w[st][v]==d[v]) f[v]+=f[st],f[v]%=p;
					else f[v]=1;
			}
		}
		vis[st]=true;
		int minn=INF;
		for (int j=1; j<=n; j++)
			if (!vis[j]&&minn>d[j]) minn=d[j],st=j;
	}
	return;
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		memset(e,0,sizeof(e));
		memset(w,0x3f,sizeof(w));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		int u,v,ww;
		for (int i=1; i<=m; i++){
			scanf("%d%d%d",&u,&v,&ww);
			e[u][++e[u][0]]=v;
			w[u][v]=ww;
		}
		dijkstra();
		printf("%d\n",f[n]);
	}
	return 0;
}

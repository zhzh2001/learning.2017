#include<bits/stdc++.h>
#define out(x) cerr << #x << " = " << x << "\n";
using namespace std;
typedef long long ll;
//by piano
template<typename tp> inline void read(tp &x) {
	x = 0;char c = getchar();int f = 0;
	for(; c <'0' || c > '9'; f |= (c == '-'), c = getchar());
	for(; c >= '0' && c <= '9'; x = (x << 3) + (x << 1) + c - '0', c = getchar());
	if(f) x = -x;
}
ll a, b, fac;

inline ll gcd(ll a, ll b) {
	return b == 0 ? a : gcd(b, a % b);
}

inline ll Find(ll a, ll b) {
	for(ll i = 2; i * i <= a; i ++) {
		if(a % i) {
			fac = i;
			break;
		}
	}
	return a * (fac - 1) - fac;
}

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	read(a), read(b);
	if(a < b) swap(a, b);
	ll fir = Find(a, b);
	ll rep = b - fac;
	ll ans = fir + rep * (a - 1);
	cout << ans << "\n";
	return 0;
}

#include<bits/stdc++.h>
#define int long long
#define out(x) cerr << #x << " = " << x << "\n";
using namespace std;
typedef long long ll;
//by piano
template<typename tp> inline void read(tp &x) {
	x = 0;char c = getchar();int f = 0;
	for(; c <'0' || c > '9'; f |= (c == '-'), c = getchar());
	for(; c >= '0' && c <= '9'; x = (x << 3) + (x << 1) + c - '0', c = getchar());
	if(f) x = -x;
}
const int N = 3e5 + 10, K = 52;
struct E {
	int to, nxt, w;
}e[N << 2], r[N << 2];
int cnt = 0, head[N];
int rcnt = 0, rh[N];
int n, m, k, mod, d;
inline void add(int u, int v, int w) {
	e[++ cnt] = (E) {v, head[u], w}; head[u] = cnt;
}
inline void Radd(int u, int v, int w) {
	r[++ rcnt] = (E) {v, rh[u], w}; rh[u] = rcnt;
}
// f[i][j] i rest of it is mindis (j -> n) + j
struct Node {
	int u, dis;
	bool operator < (const Node &rhs) const {
		return dis > rhs.dis;
	}
};

int dis[N], book[N], fd[N], sum[N];
inline void dij(int s, int t) {
	priority_queue<Node> q;
	memset(dis, 127 / 3, sizeof(dis));
	memset(book, 0, sizeof(book));
	memset(sum, 0, sizeof(sum));
	dis[s] = 0;
	q.push((Node) {s, 0});
	while(!q.empty()) {
		Node u = q.top();q.pop();
		if(book[u.u]) continue;book[u.u] = 1;
		for(int i = head[u.u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if(dis[v] > dis[u.u] + e[i].w) {
				sum[v] = 1;
				dis[v] = dis[u.u] + e[i].w;
				q.push((Node) {v, dis[v]});
			}
			else if(dis[v] == dis[u.u] + e[i].w) sum[v] ++;
		}
	}
//	cout << sum[n] << "\n";
}
inline void jid(int s, int t) {
	priority_queue<Node> q;
	memset(fd, 127 / 3, sizeof(fd));
	memset(book, 0, sizeof(book));
	fd[s] = 0;
	q.push((Node) {s, 0});
	while(!q.empty()) {
		Node u = q.top();q.pop();
		if(book[u.u]) continue;book[u.u] = 1;
		for(int i = rh[u.u]; i; i = r[i].nxt) {
			int v = r[i].to;
			if(fd[v] > fd[u.u] + r[i].w) {
				fd[v] = fd[u.u] + r[i].w;
				q.push((Node) {v, fd[v]});
			}
		}
	}
}

inline void U(int &x, int y) {
	for(x += y; x >= mod; x -= mod);
}
#define Get(a, b) ((b) - fd[a]) 
struct P {
	int fi, se;
	int p;
	bool operator <(const P &r) const {
		return se < r.se;
	}
};

int f[N][K], b[N][K];
inline void solve() {
//	cerr << d << "\n";
	priority_queue<P> q;
	memset(f, 0, sizeof(f));
	memset(b, 0, sizeof(b));
	f[1][Get(1, d + k)] = 1;
	q.push((P){1, d + k, Get(1, d + k)});
	while(!q.empty()) {
		P u = q.top();q.pop();
		if(b[u.fi][u.p]) continue;b[u.fi][u.p] = 1;
		for(int i = head[u.fi]; i; i = e[i].nxt) {
			int v = e[i].to, w = u.se - e[i].w;
			int p = Get(v, w);
			if(p >= 0) {
				U(f[v][p], f[u.fi][u.p]);
//				cerr << u.fi << ' ' << v << ' ' << w << ' ' << f[v][Get(v, w)] << "\n";
				q.push((P){v, w, p});
			}
		}
	}
//	cerr << clock() << "\n";
	int ans = 0;
//	cout << k << "\n";
	for(int i = 0; i <= k; i ++) U(ans, f[n][i]);
	cout << ans << "\n";
}
inline void init() {
	#define cc(a) memset(a, 0, sizeof(a))
	cnt = rcnt = d = 0;
	cc(head);cc(e);cc(rh);cc(r);cc(sum);cc(book);
	#undef cc
}
// init
signed main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T;for(read(T); T --; ) {
		init();
		read(n), read(m), read(k), read(mod);
		for(int i = 1; i <= m; i ++) {
			int u, v, w;
			read(u), read(v), read(w);
			add(u, v, w);
			Radd(v, u, w);
		}
		dij(1, n);
		if(k == 0) cout << sum[n] << "\n";
		else {
			jid(n, 1);
			d = dis[n];
			solve();
		}
	}
	return 0;
}


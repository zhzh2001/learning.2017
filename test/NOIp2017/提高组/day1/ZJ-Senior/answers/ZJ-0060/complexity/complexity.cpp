#include<bits/stdc++.h>
#define out(x) cerr << #x << " = " << x << "\n";
using namespace std;
typedef long long ll;
//by piano
template<typename tp> inline void read(tp &x) {
	x = 0;char c = getchar();int f = 0;
	for(; c <'0' || c > '9'; f |= (c == '-'), c = getchar());
	for(; c >= '0' && c <= '9'; x = (x << 3) + (x << 1) + c - '0', c = getchar());
	if(f) x = -x;
}

namespace onecase {
	static const int N = 233;
	struct E {
		int to, nxt;
	}e[N << 2];
	int cnt = 0, head[N];
	inline void add(int u, int v) {
		e[++ cnt] = (E) {v, head[u]}; head[u] = cnt;
	}
	
	struct Query {
		int type;char i;
		int a, b;
		
		inline void debug() {
			cout << type << ' ' << i << ' ' << a << ' ' << b << "\n";
		}
	}p[N];
	
	int top = 0;
	int q[N], O_cnt = 0, mp[2333], f[N], w[N];
	int n;
	
	inline int FindError() {
		top = 0;
		for(int i = 1; i <= n; i ++) {
			Query &u = p[i];
			int id = i;
			if(u.type == 1) {
				q[++ top] = id;
				if(mp[u.i]) return 0;
				mp[u.i] = 1;						
			}
			else {
				mp[p[q[top --]].i] = 0;
			}
			if(top < 0) return 0;
		}
		if(top != 0) return 0;
		else return 1;
	}
	
	inline int Getw(Query &u) {
		if(u.a == -1 && u.b == -1) return 0;
		if(u.a != -1 && u.b == -1) return 1;
		if(u.a != -1 && u.b != -1 && u.a <= u.b) return 0;
		return -1;
	}
	
	inline void dp(int u) {
		int Max = 0;
		for(int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			dp(v);
			Max = max(Max, f[v]);
		}
		if(w[u] == -1) f[u] = 0;
		else {
			f[u] = w[u] + Max;
		}
	}
	
	inline int solve() {
		top = 0;q[++ top] = 1;w[1] = 0;
		for(int i = 1; i <= n; i ++) {
			Query &u = p[i];
			int id = i + 1;
			if(u.type == 1) {
				q[++ top] = id;
				add(q[top - 1], q[top]);
				w[id] = Getw(u);
			}
			else top --;
		}
		dp(1);
//		cout << f[1] << "\n";
		if(f[1] == O_cnt) cout << "Yes\n";
		else cout << "No\n";
	}
	char O[N], s[N], a[N], b[N], str[N];
	int lenO;
	
	inline void init() {
		#define cc(a) memset(a, 0, sizeof(a))
		top = cnt = O_cnt = 0;
		cc(f), cc(w), cc(p);cc(head);cc(e);cc(mp);
		#undef cc
	}
	
	inline int Find(char *O, int ch) {
		int len = strlen(O);
		for(int i = 0; i < len; i ++) if(O[i] == ch) return i;
		return -1;
	}
	
	int main() {
		init();
		read(n); scanf("%s", O);
		lenO = strlen(O);
		if(lenO == 4) {
			O_cnt = 0;
		}
		else {
			for(int i = Find(O, '^') + 1; i < lenO - 1; i ++) {
				O_cnt = O_cnt * 10 + O[i] - '0';
			}
		}
		for(int i = 1; i <= n; i ++) {
			scanf("%s", str);
			if(str[0] == 'F') {
				scanf("%s%s%s", s, a, b);
				int suma = 0, sumb = 0;
				if(a[0] != 'n') {
					int lena = strlen(a);
					for(int i = 0; i < lena; i ++) {
						suma = suma * 10 + a[i] - '0';
					}
				}
				if(b[0] != 'n') {
					int lenb = strlen(b);
					for(int i = 0; i < lenb; i ++) {
						sumb = sumb * 10 + b[i] - '0';
					}
				}
				p[i] = (Query) {1, s[0], (a[0] == 'n' ? -1 : suma), (b[0] == 'n' ? -1 : sumb)};
			}
			else {
				p[i] = (Query) {0, 0, 0, 0};
			}
		}
//		for(int i = 1; i <= n; i ++) {
//			p[i].debug();
//		}
	//	puts("AFo");
	//	out(O_cnt);
		if(!FindError()) {
			cout << "ERR\n";
			return 0;
		}
		solve();
		return 0;
	}
}

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T; for(read(T); T --;) onecase :: main();
	return 0;
}

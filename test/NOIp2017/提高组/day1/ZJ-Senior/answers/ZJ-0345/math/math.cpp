#include<cstdio>
#include<cctype>
long long a,b,c,s,f[100000001];

long long read()
{
	long long ret=0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
	return ret;
}

bool check(long long x)
{
	if(x<=100000000&&f[x]) return 0;
	for(long long i=0;i<=x/a;i++)
		for(long long j=0;j<=x/b;j++)
		{
			long long ans=i*a+j*b;
			if(ans<=100000000) f[ans]=1;
			if(ans&&x%ans==0) return 0;
		}
	return 1;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();b=read();c=(long long)a*b;s=a+b;
	if(a==b)
	{
		printf("%lld\n",c-1);
		return 0;
	}
	for(long long i=c;i;i--)
		if(i%a&&i%b&&i%s&&check(i))
		{
			printf("%lld\n",i);
			return 0;
		}
	return 0;
}

#include<cstdio>
#include<cctype>
long long t,cnt,flag,cntroad[100000001];
long long n,m,k,p,d;
long long head[100001],to[200001],val[200001],nxt[200001],idx=0;
long long read()
{
	long long ret=0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
	return ret;
}

void findpath(long long u,long long sum)
{
	if(sum>d+k) return;
	if(u==n) cntroad[sum]++;
	if(cntroad[sum]>=(long long)m*m/4)
	{
		flag=1;
		return;
	}
	for(long long e=head[u];e;e=nxt[e])
	{
		if(flag) return;
		long long &v=to[e];
		findpath(v,sum+val[e]);
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=read();
	while(t--)
	{
		cnt=0;idx=0;flag=0;
		n=read(),m=read(),k=read(),p=read();
		//�ڽӱ����
		for(long long i=1;i<=n;i++) head[i]=0;
		for(long long i=1;i<=m;i++) to[i]=nxt[i]=val[i]=0;
		for(long long i=1;i<=m;i++)
		{
			long long u=read(),v=read(),w=read();
			to[++idx]=v;val[idx]=w;
			nxt[idx]=head[u];head[u]=idx;
		}
		//Dijkstra
		long long dis[100001],vis[100001];
		for(long long i=1;i<=n;i++) dis[i]=786554453,vis[i]=0;
		dis[1]=0;
		while("wr786 NOIp2017 T1= !")
		{
			long long u=-1,mind=786554453;
			for(long long i=1;i<=n;i++)
				if(!vis[i]&&dis[i]<mind)
					mind=dis[i],u=i;
			if(u==-1) break;
			vis[u]=1;
			for(long long e=head[u];e;e=nxt[e])
			{
				long long &v=to[e];
				if(dis[v]>dis[u]+val[e])
					dis[v]=dis[u]+val[e];
			}
		}
		d=dis[n];
		for(long long i=0;i<=d+k;i++)
			cntroad[i]=0;
  		findpath(1,0);
		if(flag) printf("-1\n");
		else
		{
			long long cnt=0;
			for(long long i=0;i<=d+k;i++)
				cnt=(cnt+cntroad[i])%p;
			printf("%lld\n",cnt);
		}
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<map>
using namespace std;
int T;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		char s[101];int top=0;
		int tp[101],is[101];for(int i=1;i<=100;i++) tp[i]=is[i]=0;
		map<char,int> used;used.clear();
		int flag=0,cntf=0,cntn=0,cmp=0;
		int L;scanf("%d",&L);
		char cpl[11];scanf(" %s",cpl);
		
		while(L--)
		{
			char type;scanf(" %c",&type);
			//printf("type==%c\n",type);
			if(type=='E')
			{
				tp[cntf--]=0;
				if(cntf<0)
				{
					flag=3;
					//printf("NO F&&E\n");
				}
				if(top>=0)
				{
					char val=s[top--];
					if(top<0) flag=3;
					used[val]=0;
				}
				if(is[cntf+1]) cntn--;
			}
			else if(type=='F')
			{
				cntf++;
				char val;scanf(" %c",&val);
				if(used[val])
				{
					flag=3;
					//printf("repeat\n");
				}
				else used[val]=1,s[++top]=val;
				int l=-2,r=-2;
				char c=getchar();int ret=0;
				//�������
				while(!isdigit(c)&&c!='n') c=getchar();
				if(c=='n')
					l=-1,c=getchar();
				else
				{
					while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
					l=ret;ret=0;
				}
				//�����ұ�
				while(!isdigit(c)&&c!='n') c=getchar();
				if(c=='n')
					r=-1,c=getchar();
				else
				{
					while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
					r=ret;
				}
				
				if(l!=-1&&r==-1) cntn++,is[cntf]=1;
				else if((l==-1&&r!=-1)||(l!=-1&&r!=-1&&l>r)) tp[cntf]=1;
				int tag=1;
				for(int i=1;i<=cntf;i++)
					if(tp[i]) tag=0;
				if(cntn>cmp&&tag)
				{
					cmp=cntn;
					//printf("cmp changes in %d\n",L);
				}
				
				//for(int i=1;i<=cntf;i++)
				//	printf("%d ",tp[i]);printf("\n");
				//printf("  %c %d %d\n",val,l,r);
			}
		}
		
		int cmp2=0;
		if(cpl[2]!='1')
		{
			int cur=4;
			while(isdigit(cpl[cur])) cmp2=cmp2*10+cpl[cur++]-'0';
		}
		if(flag==3||cntf) printf("ERR\n");
		else if(cmp2==cmp) printf("Yes\n");
		else printf("No\n");
		//printf("%s\n",cpl);
		//printf("cmp==%d,cmp2==%d\n",cmp,cmp2);
	}
	return 0;
}

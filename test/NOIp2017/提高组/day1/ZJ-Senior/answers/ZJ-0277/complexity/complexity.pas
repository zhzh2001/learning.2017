var Q,k,l,i,t,x,y,cnt,tot,num:longint;
str,time,save:string;
fl:boolean;
a:array[0..101]of string;
vet,next,head,que,f:array[0..101]of longint;
vis:array['a'..'z']of boolean;
procedure add(u,v:longint);
begin
inc(tot);
vet[tot]:=v;
next[tot]:=head[u];
head[u]:=tot;
end;
function max(x,y:longint):longint;
begin
if x>y then exit(x);
exit(y);
end;
procedure check(u:longint);
var i,v:longint;
begin
if fl=false then exit;
if u<>0 then
  begin
  if vis[a[u,3]] then
    begin fl:=false; exit; end;
  vis[a[u,3]]:=true;
  end;
i:=head[u];
while i<>-1 do
  begin
  v:=vet[i];
  check(v);
  i:=next[i];
  end;
if u<>0 then vis[a[u,3]]:=false;
end;
procedure dfs(u:longint);
var i,v,big:longint;
begin
if fl=false then exit;
if u<>0 then
  begin
  if vis[a[u,3]] then
    begin fl:=false; exit; end;
  vis[a[u,3]]:=true;
  end;
if u<>0 then
if a[u,5]='n' then
  begin f[u]:=0;if u<>0 then vis[a[u,3]]:=false;check(u);exit;end
  else if a[u,length(a[u])]='n' then f[u]:=1
    else
      begin
      save:=a[u];
      delete(save,1,pos(' ',save));
      delete(save,1,pos(' ',save));
      val(copy(save,1,pos(' ',save)-1),x);
      delete(save,1,pos(' ',save));
      val(save,y);
      f[u]:=0;
      if x>y then
        begin if u<>0 then vis[a[u,3]]:=false;check(u);exit;end;
      end;
i:=head[u];
big:=0;
while i<>-1 do
  begin
  v:=vet[i];
  dfs(v);
  big:=max(big,f[v]);
  i:=next[i];
  end;
f[u]:=f[u]+big;
if u<>0 then vis[a[u,3]]:=false;
end;
begin
assign(input,'complexity.in');reset(input);
assign(output,'complexity.out');rewrite(output);
readln(Q);
for k:=1 to Q do
  begin
  read(l);
  readln(time);
  i:=pos('(',time);
  delete(time,1,i);
  i:=pos(')',time);
  delete(time,i,length(time)-i+1);
  if length(time)=1 then num:=0
    else
      begin
      delete(time,1,pos('^',time));
      val(time,num);
      end;
  tot:=0;
  fillchar(head,sizeof(head),255);

  t:=0;cnt:=0;
  fl:=true;
  for i:=1 to l do
    begin
    readln(str);
    if fl=false then continue;
    if str[1]='F' then
      begin
      inc(t);
      inc(cnt);
      a[cnt]:=str;
      que[t]:=cnt;
      end;
    if str[1]='E' then
      begin
      if t-1<0 then begin fl:=false;continue;end;
      add(que[t-1],que[t]);
      dec(t);
      end;
    end;
  if (t<>0)or(fl=false) then
    begin writeln('ERR');continue;end;
  fillchar(vis,sizeof(vis),false);
  fillchar(f,sizeof(f),0);
  fl:=true;
  dfs(0);
  if fl=false then
    begin writeln('ERR');continue;end;
  if f[0]=num then writeln('Yes')
    else writeln('No');
  end;
close(input);
close(output);
end.

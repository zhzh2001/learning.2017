var
  flag:array['a'..'z'] of boolean;
  s,st,temp,ans:string;
  n,m,i,j,top,max_time,num1,num2:longint;
  bool:boolean;
  stack:array[0..100] of record
                           i:char;
                           x,y:string;
                           up_to_low:boolean;
                           time:longint;
                         end;
procedure calc;
begin
  if (stack[top].x<>'n') and (stack[top].y<>'n') then exit;
  if (stack[top].x='n') and (stack[top].y='n') then exit;
  if (stack[top].x='n') and (stack[top].y<>'n') then exit;
  if not stack[top].up_to_low then
    begin
      if stack[top].time=0 then stack[top].time:=1
      else inc(stack[top].time);
      if stack[top].time>max_time then max_time:=stack[top].time;
    end;
end;
function pd_up_to_low(st1,st2:string):boolean;
begin
  if (st1='n') and (st2='n') then exit(false);
  if (st1<>'n') and (st2='n') then exit(false);
  if (st1='n') and (st2<>'n') then exit(true);
  val(st1,num1);
  val(st2,num2);
  if num1<=num2 then exit(false)
  else exit(true);
end;
begin
  assign(input,'complexity.in'); reset(input);
  assign(output,'complexity.out'); rewrite(output);
  readln(n);
  for i:=1 to n do
  begin
    readln(m,s);
    delete(s,1,1);
    top:=0;
    bool:=true;
    stack[0].up_to_low:=false;
    stack[0].time:=0;
    max_time:=0;
    fillchar(flag,sizeof(flag),false);
    for j:=1 to m do
    begin
      readln(st);
      if not bool then continue;
      if st[1]='F' then
        begin
          delete(st,1,2);
          if flag[st[1]]=true then
            begin
              bool:=false;
              continue;
            end;
          inc(top);
          stack[top].i:=st[1];
          delete(st,1,2);
          stack[top].x:=copy(st,1,pos(' ',st)-1);
          stack[top].y:=copy(st,pos(' ',st)+1,length(st));
          stack[top].up_to_low:=stack[top-1].up_to_low or pd_up_to_low(stack[top].x,stack[top].y);
          stack[top].time:=stack[top-1].time;
          calc;
          flag[stack[top].i]:=true;
        end
      else
        begin
          if top=0 then
            begin
              bool:=false;
              continue;
            end;
          flag[stack[top].i]:=false;
          dec(top);
        end;
    end;
    if top>0 then bool:=false;
    if bool then
      begin
        if max_time=0 then ans:='O(1)'
        else
          begin
            str(max_time,temp);
            ans:='O(n^'+temp+')';
          end;
        if ans=s then writeln('Yes')
        else writeln('No');
      end
    else
      writeln('ERR');
  end;
  close(input);
  close(output);
end.

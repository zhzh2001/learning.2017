var
  dis:array[1..100000] of longint;
  sum:array[1..100000] of longint;
  visit:array[1..100000] of boolean;
  edge:array[1..100000] of array of record
                                      y,long:longint;
                                    end;
  que:array[1..1000000] of longint;
  t,g,n,m,k,p,i,x,y,z,min,ans:longint;
  bool:boolean;
  to_dis:array[1..100000] of longint;
  save:array[1..100,0..100000] of longint;
function sc(x,ndis:longint):longint;
var
  i:longint;
begin
  if bool then exit(0);
  if ndis>min+k then exit(0);
  if ndis=to_dis[x] then begin writeln(-1); bool:=true; end;
  if (x<=100) and (ndis<=100000) and (save[x,ndis]<>-1) then exit(save[x,ndis]);
  if x=n then sc:=1
  else sc:=0;
  to_dis[x]:=ndis;
  for i:=1 to sum[x] do
  begin
    sc:=(sc+sc(edge[x,i].y,ndis+edge[x,i].long)) mod p;
    if bool then exit(0);
  end;
  to_dis[x]:=maxlongint;
  if (x<=100) and (ndis<=100000) then save[x,ndis]:=sc;
end;
function SPFA:longint;
var
  head,tail,i,x:longint;
begin
  head:=1;
  tail:=1;
  for i:=1 to n do visit[i]:=false;
  for i:=2 to n do dis[i]:=maxlongint;
  dis[1]:=0;
  visit[1]:=true;
  que[head]:=1;
  while head<=tail do
  begin
    x:=que[head];
    visit[x]:=false;
    for i:=1 to sum[x] do
      if not visit[edge[x,i].y] and (dis[x]+edge[x,i].long<dis[edge[x,i].y]) then
        begin
          inc(tail);
          que[tail]:=edge[x,i].y;
          dis[edge[x,i].y]:=dis[x]+edge[x,i].long;
          visit[edge[x,i].y]:=true;
        end;
    inc(head);
  end;
  exit(dis[n]);
end;
begin
  assign(input,'park.in'); reset(input);
  assign(output,'park.out'); rewrite(output);
  readln(t);
  for g:=1 to t do
  begin
    readln(n,m,k,p);
    for i:=1 to n do
    begin
      sum[i]:=0;
      to_dis[i]:=maxlongint;
    end;
    for i:=1 to m do
    begin
      readln(x,y,z);
      inc(sum[x]);
      setlength(edge[x],sum[x]+1);
      edge[x,sum[x]].y:=y;
      edge[x,sum[x]].long:=z;
    end;
    min:=SPFA;
    fillchar(save,sizeof(save),255);
    bool:=false;
    ans:=sc(1,0);
    if not bool then writeln(ans);
  end;
  close(input);
  close(output);
end.

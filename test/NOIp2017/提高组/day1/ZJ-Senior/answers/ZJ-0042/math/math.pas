var
  f:array[0..30000000] of boolean;
  a,b,temp,i:longint;
begin
  assign(input,'math.in'); reset(input);
  assign(output,'math.out'); rewrite(output);
  readln(a,b);
  if a>b then begin temp:=a; a:=b; b:=temp; end;
  f[0]:=true;
  for i:=a to 30000000 do
  begin
    f[i]:=f[i-a];
    if i>=b then f[i]:=f[i] or f[i-b];
  end;
  for i:=30000000 downto 1 do
    if not f[i] then
      begin
        writeln(i);
        break;
      end;
  close(input);
  close(output);
end.

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
vector<int>g[200];
int T,i,j,k,n,m,a[200],b[200],st[200],top,Ans;
char s[30],c[200];
bool f,B[200],t,v[200],h[200],l[200];
inline int Read(int k){
	int x=0;
	for(;s[k]>='0'&&s[k]<='9';x=x*10+s[k++]-48);
	return x;
}
inline void Dfs(int x,int y){
	if(t)return;
	if(h[c[x]]){
		t=1;
		return;
	}
	h[c[x]]=1;
	if((a[x]==-1&&b[x]!=-1)||(a[x]>b[x]&&a[x]!=-1&&b[x]!=-1))y=-1;
	if(y!=-1&&a[x]!=-1&&b[x]==-1)y++;
	Ans=max(Ans,y);
	for(int i=0;i<g[x].size();i++)Dfs(g[x][i],y);
	h[c[x]]=0;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%s",&n,s);
		if(s[2]=='n')f=1,k=Read(4);else f=0,k=1;
		t=0;Ans=0;
		for(i=1;i<=n;i++)g[i].clear();top=0;
		for(i=1;i<=n;i++){
			v[i]=0;
			scanf("%s",s);
			if(s[0]=='E'){
				if(!top)t=1;
				B[i]=0;top--;
			}else{
				if(st[top])g[st[top]].push_back(i),l[i]=1;else l[i]=0;
				st[++top]=i;
				scanf("%s",s);c[i]=s[0];
				scanf("%s",s);B[i]=1;
				if(s[0]=='n')a[i]=-1;else a[i]=Read(0);
				scanf("%s",s);
				if(s[0]=='n')b[i]=-1;else b[i]=Read(0);
			}
		}
		if(top||t){
			printf("ERR\n");
			continue;
		}
		memset(h,0,sizeof(h));
		for(i=1;!t&&i<=n;i++)if(B[i]&&!l[i])Dfs(i,0);
		if(t)printf("ERR\n");else if(!f){
			if(!Ans)printf("Yes\n");else printf("No\n");
		}else if(Ans==k)printf("Yes\n");else printf("No\n");
	}
	return 0;
}

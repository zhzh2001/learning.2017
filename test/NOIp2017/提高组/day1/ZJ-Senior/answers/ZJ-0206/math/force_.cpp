#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void Read(long long& x){
	char c=nc();
	for(;c<'0'||c>'9';c=nc());
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
char ss[30];
int Len;
inline void Print(long long x){
	for(Len=0;x;x/=10)ss[++Len]=x%10;
	while(Len)putchar(ss[Len--]+48);
}
long long n,m,Ans,i,x,y;
inline bool Check(long long x){
	for(int i=0;i<=x/n;i++)
	if(!((x-i*n)%m))return 1;
	return 0;
}
inline void Ex_Gcd(long long a,long long b,long long& x,long long& y){
	if(!b){
		x=1;y=0;
		return;
	}
	Ex_Gcd(b,a%b,y,x);
	y-=1ll*a/b*x;
}
int main(){
	freopen("math.in","r",stdin);
//	freopen("math.out","w",stdout);
	Read(n);Read(m);
	Ex_Gcd(n,m,x,y);
	if(x>0)swap(x,y),swap(n,m);
	for(i=n*m;i;i--)
	if(!Check(i)){
		Print(i);
		return 0;
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void Read(long long& x){
	char c=nc();
	for(;c<'0'||c>'9';c=nc());
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
long long n,m,Ans,i,x,y,l,r,Mid;
inline bool Check(long long A){
	return A*y/n<(A*x-1)/m+1;
}
inline void Ex_Gcd(long long a,long long b,long long& x,long long& y){
	if(!b){
		x=1;y=0;
		return;
	}
	Ex_Gcd(b,a%b,y,x);
	y-=a/b*x;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	Read(n);Read(m);
	if(n>m)swap(n,m);
	Ex_Gcd(n,m,x,y);
	if(x>0)swap(x,y),swap(n,m);x=-x;
	for(i=n*m-1;i>=n*m-30000000;i--)
	if(Check(i)){
		cout<<i<<endl;
		return 0;
	}
	l=min(n,m)-1;r=i;
	while(l<=r){
		Mid=l+r>>1;
		if(!(Mid%n)||!(Mid%m))Mid++;
		if(Check(Mid))l=Mid+1;else r=Mid-1;
	}
	cout<<r<<endl;
	return 0;
}

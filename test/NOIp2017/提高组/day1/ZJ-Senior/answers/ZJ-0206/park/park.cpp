#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void Read(int& x){
	char c=nc();
	for(;c<'0'||c>'9';c=nc());
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
#define N 100010
#define M 200010
int i,j,k,n,m,p,T;
int num,nx[M],t[M],w[M],u;
int d[N],s[N],h[N],q[N],l,r,x,y,z,Ans,cnt;
int f[N][51],g[N][51],s1[51],s2[51];
int c[N];
bool b[N],F;
inline void Add(int x,int y,int z){
	t[++num]=y;w[num]=z;nx[num]=h[x];h[x]=num;
}
inline void Get1(int x,int y,int z){
	for(int i=0;i<=k;i++)
	if(d[y]+i-z-d[x]>=0)s1[i]=f[x][min(k,d[y]+i-z-d[x])];else s1[i]=0;
}
inline void Get2(int x,int y,int z){
	for(int i=0;i<=k;i++)
	if(d[y]+i-z-d[x]>=0)s2[i]=g[x][min(k,d[y]+i-z-d[x])];else s2[i]=0;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	Read(T);
	while(T--){
		Read(n);Read(m);Read(k);Read(p);
		memset(h,0,sizeof(h));num=0;
		for(i=1;i<=m;i++)Read(x),Read(y),Read(z),Add(x,y,z);
		memset(d,127,sizeof(d));
		q[1]=1;l=0;r=1;d[1]=0;s[1]=1;b[1]=1;Ans=0;
		while(++l<=r){
			x=q[l];
			for(i=h[x];i;i=nx[i])
			if(d[t[i]]>d[x]+w[i]){
				d[t[i]]=d[x]+w[i];
				if(!b[t[i]])q[++r]=t[i],b[t[i]]=1;
			}
			b[x]=0;
		}
		if(!k){
			q[1]=1;l=0;r=1;d[1]=0;s[1]=1;b[1]=1;u=d[n];
			while(++l<=r){
				x=q[l];
				for(i=h[x];i;i=nx[i])
				if(d[t[i]]>d[x]+w[i]){
					d[t[i]]=d[x]+w[i];s[t[i]]=s[x];
					if(!b[t[i]])q[++r]=t[i],b[t[i]]=1;
				}else if(d[t[i]]==d[x]+w[i]){
					s[t[i]]=(s[t[i]]+s[x])%p;
					if(!b[t[i]])q[++r]=t[i],b[t[i]]=1;
				}
				if(x==n&&d[x]<=u)Ans=(Ans+s[n])%p;s[x]=b[x]=0;
			}
			printf("%d\n",Ans);
			continue;
		}
		memset(f,0,sizeof(f));
		memset(g,0,sizeof(g));
		memset(b,0,sizeof(b));
		for(j=0;j<=k;j++)f[1][j]=1;b[1]=1;F=0;
		while(!F){
			for(i=1;i<=n;i++)
			if(b[i])break;
			if(i>n)break;
			for(j=h[i];j;j=nx[j]){
				Get1(i,t[j],w[j]);Get2(i,t[j],w[j]);
				for(l=0;l<=k;l++)
				if(s1[l]!=s2[l])break;
				if(l<=k){
					if(++c[t[j]]>=n){
						F=1;
						break;
					}
					for(l=0;l<=k;l++)g[t[j]][l]=f[t[j]][l],f[t[j]][l]=(f[t[j]][l]+s1[l]-s2[l])%p;
					b[t[j]]=1;
				}
			}
			b[i]=0;
		}
		if(F)printf("-1\n");else printf("%d\n",f[n][k]);
	}
	return 0;
}

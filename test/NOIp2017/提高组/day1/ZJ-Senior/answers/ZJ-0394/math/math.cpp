#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
using namespace std;
inline void judge(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
}
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}
	x*=f;
}
typedef long long i64;
int main(){
	judge();
	i64 a,b;
	cin>>a>>b;
	cout<<a*b-a-b<<endl;
	return 0;
}

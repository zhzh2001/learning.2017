#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
#define forE(i,x)  for(int i=head[x];i!=-1;i=ne[i])
#define _forE(i,x)  for(int i=_head[x];i!=-1;i=_ne[i])
#define w1 first
#define w2 second
using namespace std;
#define mk make_pair
typedef pair<int,int> pin;
inline void judge(){
	freopen("park.in","r",stdin);
	freopen("std.out","w",stdout);
}
const int inf=1e9+1;
priority_queue<pin,vector<pin>,greater<pin> >heap;
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}
	x*=f;
}
const int maxn=105,maxm=1005,maxt=maxm*15;
int num,n,m,p,k;
bool vis[maxn];
int head[maxn],t[maxm],ne[maxm],v[maxm];
int st[maxm],ed[maxm],val[maxm],dis[maxn];
int f[maxn][maxt];
bool godie=0;
bool incalc[maxn][maxt],vised[maxn][maxt];
inline void addedge(int x,int y,int z){
	ne[++num]=head[x];head[x]=num;t[num]=y;v[num]=z;
}
inline void clear(){
	num=0;godie=0;
	rep(i,1,n)head[i]=-1;
	memset(f,0,sizeof(f));
	memset(vised,0,sizeof(vised));
}
inline int dp(int x,int w){
	if(x==n+1&&w==-1)return 1;
	if(x==n+1)return 0;
	if(w-dis[x]>k)return 0;
	if(w<dis[x])return 0;
	if(incalc[x][w]){
		godie=1;
		return 0;
	}
	if(vised[x][w])return f[x][w];
	incalc[x][w]=1;vised[x][w]=1;
	forE(i,x)f[x][w]=(f[x][w]+dp(t[i],w-v[i]))%p;
	incalc[x][w]=0;
	return f[x][w];
}
inline void dijk(int dis[],int S){
	while(!heap.empty())heap.pop();
	memset(vis,0,sizeof(vis));
	rep(i,1,n)dis[i]=inf;dis[S]=0;
	heap.push(mk(dis[S],S));
	while(!heap.empty()){
		pin x=heap.top();heap.pop();
		if(vis[x.w2])continue;vis[x.w2]=1;
		dis[x.w2]=x.w1;
		forE(i,x.w2){
			if(dis[t[i]]>dis[x.w2]+v[i]){
				dis[t[i]]=dis[x.w2]+v[i];
				heap.push(mk(dis[t[i]],t[i]));
			}
		}
	}
}
inline void solve(){
	read(n);read(m);read(k);read(p);
	rep(i,1,m){
		read(st[i]);read(ed[i]);read(val[i]);
	}	
	clear();
	rep(i,1,m)addedge(st[i],ed[i],val[i]);
	dijk(dis,1);
	clear();
	rep(i,1,m)addedge(ed[i],st[i],val[i]);
	addedge(1,n+1,1);
	int res=0;
	rep(i,0,k)res=(res+dp(n,dis[n]+i))%p;
	if(godie){
		res=-1;	
	}
	cout<<res<<endl;
}
int main(){
	judge();
	int T;read(T);
	while(T--)solve();
	return 0;
}

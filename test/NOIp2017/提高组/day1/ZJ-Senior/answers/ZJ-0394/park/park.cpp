#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
#define forE(i,x)  for(int i=head[x];i!=-1;i=ne[i])
#define _forE(i,x)  for(int i=_head[x];i!=-1;i=_ne[i])
#define w1 first
#define w2 second
#define mk make_pair
using namespace std;
typedef pair<int,int> pin;
inline void judge(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
}
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}
	x*=f;
}
const int maxn=100005,maxm=200005;
int num,head[maxn],t[maxm],ne[maxm],v[maxm],dis1[maxn],dis2[maxn];
int calct[maxn];
bool vis[maxn];
int id[maxn],ans[maxn][55];
int Queue[maxn],rd[maxn],front,finish;
bool act[maxn];
int n,m,k,p;
int st[maxm],ed[maxm],val[maxm];
inline void addedge(int x,int y,int z){
	ne[++num]=head[x];head[x]=num;t[num]=y;v[num]=z;
}
inline void clear(){
	rep(i,1,n)head[i]=-1;
	num=0;
	rep(i,1,n)calct[i]=-1;
	memset(act,0,sizeof(act));
	memset(rd,0,sizeof(rd));
	memset(ans,0,sizeof(ans));
}
priority_queue<pin,vector<pin>,greater<pin> >heap;
const int inf=1e9+1;
bool godie=0;
int _head[maxn],_t[maxm],_ne[maxm],_num;
inline void adde(int x,int y){
	_ne[++_num]=_head[x];_head[x]=_num;_t[_num]=y;
}
inline void dijk(int dis[],int S){
	while(!heap.empty())heap.pop();
	memset(vis,0,sizeof(vis));
	rep(i,1,n)dis[i]=inf;dis[S]=0;
	heap.push(mk(dis[S],S));
	while(!heap.empty()){
		pin x=heap.top();heap.pop();
		if(vis[x.w2])continue;vis[x.w2]=1;
		dis[x.w2]=x.w1;
		forE(i,x.w2){
			if(dis[t[i]]>dis[x.w2]+v[i]){
				dis[t[i]]=dis[x.w2]+v[i];
				heap.push(mk(dis[t[i]],t[i]));
			}
		}
	}
}
namespace Tarjan{
	int low[maxn],dfn[maxn],S[maxn],top,clk,sz[maxn];
	bool vis[maxn],ins[maxn];
	int tot,bl[maxn];
	int mini1[maxn],mini2[maxn];
	inline void Tarjan(int x){
		vis[x]=1;dfn[x]=low[x]=++clk;ins[x]=1;S[++top]=x;
		forE(i,x)if(v[i]==0){
			if(!vis[t[i]]){
				Tarjan(t[i]);
				low[x]=min(low[x],low[t[i]]);
			}else if (ins[t[i]]){
				low[x]=min(low[x],dfn[t[i]]);
			}
		}
		if(low[x]==dfn[x]){
			tot++;
			while(S[top]!=x){
				ins[S[top]]=0;sz[tot]++;
				bl[S[top]]=tot;
				top--;
			}
			top--;bl[x]=tot;ins[x]=0;sz[tot]++;
		}
	}
	inline void check_godie(){
		clk=0;memset(vis,0,sizeof(vis));memset(ins,0,sizeof(ins));memset(sz,0,sizeof(sz));
		top=0;tot=0;
		rep(i,1,n)if(!vis[i])Tarjan(i);
		rep(i,1,tot){
			mini1[i]=mini2[i]=inf;
		}
		rep(i,1,n){
			mini1[bl[i]]=min(mini1[bl[i]],dis1[i]);
			mini2[bl[i]]=min(mini2[bl[i]],dis2[i]);
		}
		rep(i,1,tot){
			if(mini1[i]+mini2[i]<=dis1[n]+k&&sz[i]>1)godie=1;
		}
		if(godie)return;
		rep(i,1,n)if(dis1[i]+dis2[i]<=dis1[n]+k){
			act[i]=1;
		}
		rep(i,1,m)if(val[i]==0&&act[st[i]]&&act[ed[i]])
			rd[ed[i]]++;	
		front=0;finish=0;
		rep(i,1,n)if(act[i]&&rd[i]==0)
			Queue[++finish]=i;
		while(front!=finish){
			int x=Queue[++front];
			forE(i,x)if(v[i]==0&&act[t[i]]){
				rd[t[i]]--;
				if(rd[t[i]]==0)Queue[++finish]=t[i];
			}
		}
		rep(i,1,finish)id[Queue[i]]=i;
	}
}
pin a[maxn];
//set<pin> zb;
//typedef set<pin>::iterator It;
bool v1[maxn];
inline void add(int &x,int y){
	x=((x+y)>=p?(x+y-p):(x+y));
}
inline void calc(int x,int nowv){
	if(calct[x]==nowv)return;
	_forE(i,x)if(v1[_t[i]]&&calct[_t[i]]!=nowv){
//		assert(calct[_t[i]]==0||calct[_t[i]]==nowv-1);
		calc(_t[i],nowv);
	}
	calct[x]=nowv;
	forE(i,x)if(act[t[i]]&&nowv+v[i]<=dis1[t[i]]+k){
		add(ans[t[i]][nowv+v[i]-dis1[t[i]]],ans[x][nowv-dis1[x]]);
	}
}
inline void solve(){
	read(n);read(m);read(k);read(p);
	clear();godie=0;
	rep(i,1,m){
		read(st[i]);read(ed[i]);read(val[i]);
	}
	rep(i,1,m)addedge(st[i],ed[i],val[i]);
	dijk(dis1,1);
	num=0;rep(i,1,n)head[i]=-1;
	rep(i,1,m)addedge(ed[i],st[i],val[i]);
	dijk(dis2,n);
	num=0;rep(i,1,n)head[i]=-1;
	rep(i,1,m)addedge(st[i],ed[i],val[i]);
	Tarjan::check_godie();
	if(godie){
		puts("-1");
		return;
	}
	rep(i,1,finish){
		int x=Queue[i];
		a[i]=mk(dis1[x],x);
	}
	sort(a+1,a+1+finish);
	int i=1,j=0;
	int nowv=0;
	ans[1][0]=1;
	a[finish+1].w1=inf;
	num=0;rep(i,1,n)head[i]=-1;
	rep(i,1,m)if(act[st[i]]&&act[ed[i]])addedge(st[i],ed[i],val[i]);
	_num=0;rep(i,1,n)_head[i]=-1;
	rep(i,1,m)if(val[i]==0)adde(ed[i],st[i]);
	while(i<=finish){
		while(nowv-a[i].w1>k){
//			zb.erase(mk(id[a[i].w2],a[i].w2));
			v1[a[i].w2]=0;
			i++;
		}
		while(nowv>=a[j+1].w1){
			j++;
			v1[a[j].w2]=1;
//			zb.insert(mk(id[a[j].w2],a[j].w2));
		}
		if(i>j){
			nowv=a[i].w1;
			continue;
		}
		rep(p,i,j){
			int x=a[p].w2;
			calc(x,nowv);
		}
		nowv++;
	}
	int res=0;
	rep(i,0,k)add(res,ans[n][i]);
	printf("%d\n",res);
}
int main(){
	judge();
	int T;read(T);
	while(T--)solve();
	return 0;
}

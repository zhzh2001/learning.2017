#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
using namespace std;
inline void judge(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
}
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}
	x*=f;
}
const int maxn=105;
int fa[maxn],S[maxn],used[maxn],L,top,fr[maxn],ed[maxn];
int times[maxn];
char tmp[maxn];
int cur=0;
bool godie=0;
inline void gao(int x){
	if(fr[x]>ed[x])times[x]=0;
	if(ed[x]==101&&fr[x]!=101)times[x]++;
	times[fa[x]]=max(times[fa[x]],times[x]);
//	if(used[fa[x]]&used[x])godie=1;
//	used[fa[x]]|=used[x];
}
inline int get(){
	scanf("%s",tmp+1);
	if(tmp[1]=='n')
		return 101;
	int v=0;
	for(int i=1;tmp[i];i++)v=10*v+tmp[i]-'0';
	return v;
}
inline void solve(){
	memset(fa,0,sizeof(fa));
	memset(S,0,sizeof(S));
	memset(times,0,sizeof(times));
	memset(used,0,sizeof(used));
	memset(fr,0,sizeof(fr));
	memset(ed,0,sizeof(ed));
	godie=0;top=0;cur=0;
	read(L);
	scanf("%s",tmp+1);
	if(tmp[3]=='1'){
		cur=0;
	}else{
		for(int i=5;tmp[i]>='0'&&tmp[i]<='9';i++){
			cur=10*cur+tmp[i]-'0';
		}
	}int tot=0;
	while(L--){
		char ch=getchar();
		while(ch!='F'&&ch!='E'){
			ch=getchar();
		}
		if(ch=='E'){
			if(top==0){
				godie=1;
				continue;
			}
			top--;
			continue;
		}
		if(ch=='F'){
			char nd=getchar();
			nd=getchar();
//			assert(nd>='a'&&nd<='z');
			tot++;
			fa[tot]=S[top];
			S[++top]=tot;
			used[tot]|=1<<(nd-'a');
			fr[tot]=get();
			ed[tot]=get();
		}
	}
	if(top>0){
		godie=1;
	}
	rep(i,1,tot){
		if(fa[i]){
			if(used[i]&used[fa[i]]){
				godie=1;
			}
			used[i]|=used[fa[i]];
		}
	}
	per(i,tot,1)gao(i);
	if(godie){
		puts("ERR");
		return;
	}
	if(times[0]==cur){
		puts("Yes");
	}else
		puts("No");
}
int main(){
	judge();
	int T;read(T);
	while(T--)solve();
	return 0;
}

#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cstring>
#define maxn 1061109567
using namespace std;
int T;
int n,m,k,p;
int x,y,c,min_,ans=0;
int map[1010][1010];
int dis[1010];
int que[1010];
bool vis[1010];
bool bo=0;
void dfs(int x,int quan){
	if(bo) return ;
	if(x==n){
		if(quan<=min_+k) ans++;
		if(ans>10) {bo=1,ans=-1;
		return ;
	    }
	}
	if(quan>min_){
		return ;
	}
	for(int i=1;i<=n;i++)
	if(map[x][i]<maxn){
		dfs(i,quan+map[x][i]);
	}
}
void dj(){
	memset(dis,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	vis[1]=1;dis[1]=0;
	int h=0,t=1;que[1]=1;
	while(h<t){
		h++;
		int u=que[h];
		for(int i=1;i<=n;i++)
		 if(map[u][i]<maxn&&vis[i]==0)
		  if(dis[u]+map[u][i]<dis[i])dis[i]=dis[u]+map[u][i];
		  int k=0;
		for(int i=2;i<=n;i++)if(dis[i]<dis[k]&&vis[i]==0)k=i;
		if(k==0) break;
		 vis[k]=1;
		que[++t]=k;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		ans=0;
		bo=0;
		memset(map,63,sizeof(map));
		for(int i=1;i<=m;i++){
		 scanf("%d%d%d",&x,&y,&c);
		 map[x][y]=c;
	    }
        dj();
        min_=dis[n];
		dfs(1,0);
		printf("%d\n",ans%p);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

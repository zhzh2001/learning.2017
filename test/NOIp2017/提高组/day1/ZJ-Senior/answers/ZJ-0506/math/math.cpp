#include<cstdio>
#include<cstdlib>
#include<iostream>
#define LL long long
using namespace std;
LL a,b;
LL x,y;
LL x1,y1;
LL ans=0;
void gcd(LL &x,LL &y,LL a,LL b){
	LL c=a%b;
	if(b==1){ x=0;y=1;return;}
	gcd(x,y,b,c);
    LL t=x;x=y;y=(t-y*(a/b));
}
LL abss(LL x){
	return (x>0)?x:-x;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	gcd(x,y,a,b);
	if(x<0){
		y1=y;
		while(1){
			if((y1*b+1)%a==0){
				x1=(y1*b+1)/a;
				break;
			}else{
				y1++;
			}
		}
		ans=(y-1)*b+(x1-1)*a-1;
	}else{
		x1=x;
		while(1){
			if((x1*a+1)%b==0){
				y1=(x1*a+1)/b;
				break;
			}else{
				x1++;
			}
		}
		ans=(y1-1)*b+(x-1)*a-1;
	}
	printf("%lld\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

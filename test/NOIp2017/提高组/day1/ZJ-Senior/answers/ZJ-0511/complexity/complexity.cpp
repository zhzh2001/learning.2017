#include<cstdio>
#include<cctype>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define INF 0x3F3F3F3F
int t, n, tmp;
char buf[500], cpx[500];
bool vis[26];
int stk[500], ans[500], top, res;
struct expr{
	int kind, idx, beg, end;
	inline expr(int k=0, int i=0, int b=0, int e=0):kind(k), idx(i), beg(b), end(e){}
}p[500];
int main(){
	#ifndef DEBUG
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	#endif
	scanf("%d", &t);
	while(t--){
		memset(vis, 0, sizeof(vis));
		memset(p, 0, sizeof(p));
		memset(stk, 0, sizeof(stk));
		memset(ans, 0, sizeof(ans));
		tmp = 0;res = 0;
		scanf("%d%s", &n, cpx);
		for(int i = 1;i <= n;i++){
			scanf("%s", buf);bool kind = buf[0] == 'F';
			int bian = 0, beg = 0, end = 0;
			if(kind){
				scanf("%s", buf);bian = buf[0] - 'a';
				scanf("%s", buf);
				if(buf[0] == 'n')beg = -1;else sscanf(buf, "%d", &beg);
				scanf("%s", buf);
				if(buf[0] == 'n')end = -1;else sscanf(buf, "%d", &end);
				tmp++;
			}else tmp--;
			p[i] = expr(kind, bian, beg, end);
		}
		if(tmp != 0)goto fail;
		top = 0;
		for(int i = 1;i <= n;i++){
			if(p[i].kind){
				int x = p[i].idx;
				if(vis[x])goto fail;
				stk[++top] = x;
				vis[x] = 1;
			}else{
				vis[stk[top--]] = 0;
			}
		}
		top = 0;
		for(int i = 1;i <= n;i++){
			if(p[i].kind){
				top++;
				if((~p[i].beg && ~p[i].end) || (p[i].beg == -1 && p[i].end == -1)){
					if(p[i].beg <= p[i].end)
						ans[top] = ans[top - 1];
					else
						ans[top] = -INF;
				}else{
					if(!~p[i].beg){
						ans[top] = -INF;
					}else{
						ans[top] = ans[top - 1] + 1;
					}
				}
			}else{
				res = std::max(res, ans[top--]);
			}
		}
		if(cpx[2] == 'n'){
			register int x = 0, i = 0;
			while(!isdigit(cpx[i]))i++;
			for(;cpx[i] && isdigit(cpx[i]);i++)x = (x << 1) + (x << 3) + (cpx[i] ^ 48);
			puts(x == res ? "Yes" : "No");
		}else puts(res ? "No" : "Yes");
		continue;
		fail:puts("ERR");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

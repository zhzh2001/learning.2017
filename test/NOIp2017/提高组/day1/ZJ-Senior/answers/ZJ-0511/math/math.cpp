#include<cstdio>
#include<cctype>
inline int gi(){
	register int x = 0, f = 1;
	register char ch = getchar();
	while(!isdigit(ch) && ch ^ 45)ch = getchar();
	if(!(ch ^ 45))f = -1, ch = getchar();
	for(;isdigit(ch);ch = getchar())x = (x << 1) + (x << 3) + (ch ^ 48);
	return x * f;
}
#include<cstdlib>
#include<cstring>
#include<algorithm>
long long a, b, c;
inline int gcd(int a, int b){return b?gcd(b,a%b):a;}
int main(){
	#ifndef DEBUG
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	#endif
	a = gi();b = gi();
	if(a == 1 || b == 1)puts("0");
	printf("%lld\n", a * b - a - b);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

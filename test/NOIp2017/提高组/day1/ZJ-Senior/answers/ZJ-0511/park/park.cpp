#include<cstdio>
#include<cctype>
inline int gi(){
	register int x = 0, f = 1;
	register char ch = getchar();
	while(!isdigit(ch) && ch ^ 45)ch = getchar();
	if(!(ch ^ 45))f = -1, ch = getchar();
	for(;isdigit(ch);ch = getchar())x = (x << 1) + (x << 3) + (ch ^ 48);
	return x * f;
}
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<queue>
#define MAXN 100010
#define MAXM 200010
int t, n, m, k, p, mins;
namespace Y{
	int head[MAXN], to[MAXM], next[MAXM], val[MAXM], tot = 0;
	inline void addEdge(int u, int v, int w){
		next[tot] = head[u];to[tot] = v;val[tot] = w;head[u] = tot++;
	}
	struct node{
		int x, dis;
		inline node(int x = 0, int dis = 0):x(x), dis(dis){}
		inline bool friend operator < (const node &a, const node &b){
			return a.dis > b.dis;
		}
	};
	std::priority_queue<node> Q;
	int dis[MAXN];
	inline void gds(){
		memset(dis, 0x3F, sizeof(dis));
		dis[n] = 0;Q.push(node(n, 0));
		while(!Q.empty()){
			node top = Q.top();Q.pop();
			for(int i = head[top.x];~i;i = next[i]){
				if(dis[to[i]] > top.dis + val[i]){
					dis[to[i]] = top.dis + val[i];
					Q.push(node(to[i], dis[to[i]]));
				}
			}
		}
	}
}

namespace X{
	int head[MAXN], to[MAXM], next[MAXM], val[MAXM], tot = 0;
	inline void addEdge(int u, int v, int w){
		next[tot] = head[u];to[tot] = v;val[tot] = w;head[u] = tot++;
	}
	struct node{
		int x, dis;
		inline node(int x = 0, int dis = 0):x(x), dis(dis){}
		inline bool friend operator < (const node &a, const node &b){
			return a.dis > b.dis;
		}
	};
	std::priority_queue<node> Q;
	int dis[MAXN];
	inline void gds(){
		memset(dis, 0x3F, sizeof(dis));
		dis[1] = 0;Q.push(node(1, 0));
		while(!Q.empty()){
			node top = Q.top();Q.pop();
			for(int i = head[top.x];~i;i = next[i]){
				if(dis[to[i]] > top.dis + val[i]){
					dis[to[i]] = top.dis + val[i];
					Q.push(node(to[i], dis[to[i]]));
				}
			}
		}
	}
	std::queue<node> que;
	int f[MAXN][100];
	inline void slove(){
		que.push(node(1, 0));
		f[1][0] = 1;
		while(!que.empty()){
			//if(que.size() > 100000)puts("BAD CALC");
			node top = que.front();que.pop();
			for(int i = head[top.x];~i;i = next[i]){
				if(top.dis + val[i] <= mins + k - Y::dis[to[i]]){
					f[to[i]][top.dis + val[i] - dis[to[i]]] = (f[to[i]][top.dis + val[i] - dis[to[i]]] + f[top.x][top.dis - dis[top.x]]) % p;
					if(top.dis + val[i] - dis[to[i]] >= 100){
						//puts("Out of bound!!!");
						return;
					}
					que.push(node(to[i], top.dis + val[i]));
				}
			}
		}
	}
}

namespace Z{
	int head[MAXN], to[MAXM], next[MAXM], tot = 0;
	inline void addEdge(int u, int v){
		next[tot] = head[u];to[tot] = v;head[u] = tot++;
	}
	int vst[MAXN], mints, mintt, timer = 0;
	bool judge(int x, int timer){
		mints = std::min(mints, X::dis[x]);
		mintt = std::min(mintt, Y::dis[x]);
		vst[x] = timer;
		for(int i = head[x];~i;i = next[i]){
			if(vst[to[i]] == timer){
				if(mints + mintt <= mins + k){
					return 1;
				}
			}else if(!vst[to[i]] && judge(to[i], timer))return 1;
		}
		return 0;
	}
	inline bool judge(){
		for(int i = 1;i <= n;i++){
			if(!vst[i]){
				mints = mintt = 0x3F3F3F3F;
				if(judge(i, ++timer))return 1;
			}
		}
		return 0;
	}
}
inline void preprocess(){
	memset(X::head, -1, sizeof(X::head));X::tot = 0;
	memset(Y::head, -1, sizeof(Y::head));Y::tot = 0;
	memset(Z::head, -1, sizeof(Z::head));Z::tot = 0;
	memset(Z::vst, 0, sizeof(Z::vst));
}
int main(){
	#ifndef DEBUG
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	#endif
	t = gi();
	while(t--){
		preprocess();
		n = gi();m = gi();k = gi();p = gi();
		for(int i = 0;i < m;i++){
			int u = gi(), v = gi(), w = gi();
			X::addEdge(u, v, w);
			Y::addEdge(v, u, w);
			if(!w)Z::addEdge(u, v);
			//X::d[v]++;
		}
		X::gds();mins = X::dis[n];Y::gds();
		//puts("Normal dis::");
		//for(int i = 1;i <= n;i++)printf("%d ", X::dis[i]);puts("");
		//puts("Reserve dis::");
		//for(int i= 1;i <= n;i++)printf("%d ", Y::dis[i]);puts("");
		//puts("Min dis sloved.");
		if(Z::judge()){
			puts("-1");
			continue;
		}
		//puts("Fuhuan sloved.");
		//for(int i = 1;i <= n;i++)if(!X::dfn[i])X::Tarjan(i);
		//printf("%d\n", fanan[n]);
		X::slove();
		/*puts("Data");
		for(int i = 1;i <= n;i++){
			for(int j = 0;j <= mins + k;j++){
				printf("%d ", X::f[i][j]);
			}
			puts("");
		}*/
		int ans = 0;
		for(int i = mins;i <= mins + k;i++)ans = (ans + X::f[n][i - mins]) % p;
		printf("%d\n", ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

var
  runtt,tt,n,m,k,p,i,x,y,dist,ansans,stacktop:longint;
  a:array[1..400000]of record
    data,next,dist:longint;
  end;
  f:boolean;
  amaxzhi,bcj,low:array[1..100000]of longint;
  ans:array[1..100000,0..50]of longint;
  visited:array[1..100000]of boolean;
  atop:longint;
  stack:array[1..100000]of record
    x,i:longint;
  end;
procedure init;
begin
  assign(input,'park.in'); reset(input);
  assign(output,'park.out'); rewrite(output);
end;
procedure over;
begin
  close(input);
  close(output);
end;
function min(x,y:longint):longint;
begin
  if x>y then exit(y); exit(x);
end;
procedure adda(x,y,dist:longint);
begin
  inc(atop);
  a[amaxzhi[x]].next:=atop;
  a[atop].data:=y;
  a[atop].dist:=dist;
  amaxzhi[x]:=atop;
end;
function father(x:longint):longint;
begin
  if x=bcj[x] then exit(x);
  father:=father(bcj[x]);
end;
function find(x,y:longint):boolean;
begin
  if x=y then exit(true);
  if x=bcj[x] then exit(false);
  exit(find(bcj[x],y));
end;
procedure push(x:longint);
begin
  visited[x]:=true;
  for i:=0 to k do ans[x,i]:=0;
  if x=n then
  begin
    low[x]:=0;
    ans[x,0]:=1;
    exit;
  end;
  low[x]:=maxlongint div 2;

  inc(stacktop);
  stack[stacktop].i:=a[x].next;
  stack[stacktop].x:=x;
end;
procedure dfs;
var
  p,dist,d,j,i,x:longint;
begin
  push(1);
  while stacktop>0 do
  begin
    i:=stack[stacktop].i;
    x:=stack[stacktop].x;
    if i=0 then
    begin
      dec(stacktop);
      continue;
    end;
    p:=a[i].data;
    dist:=a[i].dist;
    if not visited[p] then
    begin
      push(p);
      continue;
    end;
    if low[x]>low[p]+dist then
    begin
      d:=low[x]-low[p]+dist;
        for j:=k downto d do
        ans[x,j]:=ans[x,j-d];
        for j:=0 to min(d-1,k) do
        ans[x,j]:=0;
      low[x]:=low[p]+dist;
      for j:=0 to k do
      begin
        inc(ans[x,j],ans[p,j]);
        ans[x,j]:=ans[x,j] mod p;
      end;
    end
    else
    if low[p]+dist-low[x]<=k then
    begin
      d:=low[p]+dist-low[x];
      for j:=0 to k-d do
      begin
        inc(ans[x,j+d],ans[p,j]);
        ans[x,j+d]:=ans[x,j+d] mod p;
      end;
    end;

    stack[stacktop].i:=a[i].next;
  end;
end;
procedure duru;
begin
    readln(n,m,k,p);
    for i:=1 to n do
    begin
      amaxzhi[i]:=i;
      bcj[i]:=i;
    end;
    atop:=n;
    f:=false;
    for i:=1 to m do
    begin
      readln(x,y,dist);
      adda(x,y,dist);
      if dist=0 then
      begin
        if find(y,x) then
        f:=true;
        bcj[father(x)]:=father(y);
      end;
    end;
end;
begin
  init;
  readln(tt);
  for runtt:=1 to tt do
  begin
    duru;
    if f then
    begin
      writeln(-1);
      continue;
    end;
    for i:=1 to n do visited[i]:=false;
    //if k=0 then
    begin
      dfs;
      ansans:=0;
      for i:=0 to k do
      ansans:=ansans+ans[1,i];
      writeln(ansans);
    end;
  end;
  over;
end.

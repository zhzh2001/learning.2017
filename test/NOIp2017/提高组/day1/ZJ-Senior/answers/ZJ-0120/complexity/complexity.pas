var
  i,time,t1,n,runtime,tem1,tem2,j,p:longint;
  ans,s:string;
  stack:array[0..1000]of record
    t0,t:longint;
    s:string;
  end;
  stacktop:longint;
procedure init;
begin
  assign(input,'complexity.in'); reset(input);
  assign(output,'complexity.out'); rewrite(output);
end;
procedure over;
begin
  close(input);
  close(output);
end;
procedure du_shu(var x:longint);
begin
  while (s[p]>='0') and (s[p]<='9') do
  begin
    x:=x*10+ord(s[p])-48;
    inc(p);
  end;
end;
procedure updatemax(var x:longint; y:longint);
begin
  if x<y then x:=y;
end;
procedure du_s;
begin
  stack[stacktop].s:='';
  while s[p]<>' ' do
  begin
    stack[stacktop].s:=stack[stacktop].s+s[p];
    inc(p);
  end;
  for j:=1 to stacktop-1 do
  if stack[stacktop].s=stack[j].s then
  ans:='ERR'; //name repeated
  inc(p);
end;
procedure du_tem(var tem:longint);
begin
  if s[p]='n' then
  begin
    tem:=1000;
    inc(p,2);
  end
  else
  begin
    du_shu(tem);
    inc(p);
  end;
end;
begin
  init;
  readln(time);
  for runtime:=1 to time do
  begin
    read(n);
    readln(s);
    p:=pos('(',s)+1;
    stacktop:=0;
    stack[0].t:=0;
    t1:=0;
    ans:='';

    if s[p]='n' then
    begin
      inc(p,2);
      du_shu(t1);
    end;

    for i:=1 to n do
    begin
      readln(s); s:=s+' ';
      if s[1]='F' then
      begin
        p:=3;
        inc(stacktop);
        stack[stacktop].t:=0; stack[stacktop].t0:=0;
        tem1:=0;
        tem2:=0;

        du_s;

        du_tem(tem1);
        du_tem(tem2);

        if tem1>tem2 then
        stack[stacktop].t0:=-1
        else
        if tem2-tem1>200 then
        stack[stacktop].t0:=1
        else
        stack[stacktop].t0:=0;
      end
      else
      if s[1]='E' then
      begin
        if stacktop=0 then
        ans:='ERR'; //lack of '['
        dec(stacktop);
        if stack[stacktop+1].t0=-1 then
        stack[stacktop+1].t:=1;
        updatemax(stack[stacktop].t,stack[stacktop+1].t+stack[stacktop+1].t0);
      end;
    end;
    if stacktop<>0 then ans:='ERR'; //lack of ']'
    if ans='ERR' then writeln(ans)
    else
    if stack[0].t=t1 then
    writeln('Yes')
    else
    writeln('No');
  end;
  over;
end.

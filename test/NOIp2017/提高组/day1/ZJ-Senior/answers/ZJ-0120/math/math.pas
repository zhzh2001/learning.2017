const
  maxn=10000000;
var
  a,b,x,y,t,i,last:longint;
  bb,aa:int64;
  beibao:array[0..maxn]of boolean;
procedure init;
begin
  assign(input,'math.in'); reset(input);
  assign(output,'math.out'); rewrite(output);
end;
procedure over;
begin
  close(input);
  close(output);
end;
function gcd(a,b:longint; var x,y:longint):longint;
begin
  if b=0 then
  begin
    x:=1;
    y:=0;
    exit(a);
  end;
  gcd:=gcd(b,a mod b,y,x);
  y:=y-(a div b)*x;
end;
begin
  init;
  readln(a,b);
  if a>b then
  begin
    t:=a; a:=b; b:=t;
  end;
  if (b<=10000) then
  begin
    i:=1; last:=1;
    for i:=0 to maxn do
    beibao[i]:=false;
    beibao[0]:=true;
    for i:=a to maxn do
    if beibao[i-a] then beibao[i]:=true;
    for i:=b to maxn do
    if beibao[i-b] then beibao[i]:=true;
    i:=1;
    while true do
    begin
      if not beibao[i] then last:=i;
      if i-last>a+2 then break;
      inc(i);
    end;
    writeln(last);
  end
  else
  begin
    bb:=b;
    aa:=a;
    writeln(bb*(aa-1)-1);
  end;
  over;
end.

#include<cstdio>
#include<cstring>
using namespace std;

#define N 1000

bool ok;
int T,op,x,vis[N],h,i,a[N][5],top,op1,op2,x1,x2,sb,p[N],b[N][5];
char s[N],ch1[N],ch2[N],ch3[N],ch4[N];

int max(int a,int b) {return a>b? a:b;}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for (;T--;)
	{	
		int i; op=0; x=0; 
		memset(vis,0,sizeof(vis));
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		scanf("%d",&h); scanf("%s",s+1); 
		for (i=1;i<=strlen(s+1);i++) if (s[i]=='(') break;
		i++; if (s[i]=='n') {op=2; i+=2;} else op=1;
		for (;i<strlen(s+1);i++) x=x*10+s[i]-'0';
		ok=true;
		for (i=1;i<=h;i++)
		{
			scanf("%s",ch1+1);
			if (ch1[1]=='F')
			{
				scanf("%s",ch2+1); scanf("%s",ch3+1); scanf("%s",ch4+1);
				a[i][1]=1; a[i][2]=ch2[1]-'a'+1; 
				if (ch3[1]=='n') a[i][3]=200; 
				else
				{
					for (int u=1;u<=strlen(ch3+1);u++)
					a[i][3]=a[i][3]*10+ch3[u]-'0';
				}
				if (ch4[1]=='n') a[i][4]=200;
				else
				{
					for (int u=1;u<=strlen(ch4+1);u++)
					a[i][4]=a[i][4]*10+ch4[u]-'0';
				} 
			}
			else a[i][1]=2;
		}
		top=0; op1=1; x1=0; op2=0; x2=0; sb=0;
		for (i=1;i<=h;i++)
		{
			if (a[i][1]==1) 
			{
				top++; if (sb>0) sb++;
				if (vis[a[i][2]]) {ok=false; break;} vis[a[i][2]]=1; p[top]=a[i][2]; b[top][1]=op2; b[top][2]=x2;
				if (sb>0) continue;
				if (a[i][3]<=a[i][4])
				{
					if (a[i][3]==a[i][4]&&op2==0)  op2=1;
					else if (a[i][3]<200&&a[i][4]<200)   {if (op2==0) op2=1;}
					else op2=2,x2++;
				}
				if (a[i][3]>a[i][4]) {sb=1;}
			}
			else
			{
				if (top==0) {ok=false; break;}
				vis[p[top]]=0; if (sb>1) sb--;
				if (op1==0) x1=x2,op1=op2; 
				else 
				{
					if (op1==1 && op2==1) op1=1;
					else if (op1==1 && op2==2) x1=x2,op1=op2;
					else if (op2==2 && op2==1) op2=op2;
					else x1=max(x1,x2);
				}
				op2=b[top][1],x2=b[top][2];
				top--;
			}
		}
		if (top^0) ok=false;
		if (ok==false) {printf("ERR\n"); continue;}
		if (op1==1 && op==1) printf("Yes\n");
		else if (op1==2 && op==2 && x1==x) printf("Yes\n");
		else printf("No\n"); 
	}
	return 0;
}

#include<cstdio>

#define inf 1000000000
#define to e[i].v
#define N 2010

struct node{
	int v,w,next;
}e[N];

int vis[N],dist[N][N],head[N],q[N],l,r,ans,L,p,n,m,k,id,T;

int read()
{
	int x=0,f=1; char ch=getchar();
	for (;ch>'9'||ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

void add(int u,int v,int w) {e[id].v=v; e[id].w=w; e[id].next=head[u]; head[u]=id++;}

int min(int a,int b) {return a>b? b:a;}

void spfa(int s)
{
	for (int i=0;i<=n;i++) vis[i]=0,dist[s][i]=inf;
	dist[s][s]=0; vis[s]=1; q[1]=s; l=0; r=1;
	for (;l^r;)
	{
		int u=q[++l]; vis[u]=0;
		for (int i=head[u];i;i=e[i].next)
			if (dist[s][u]+e[i].w<dist[s][to])
			{
				dist[s][to]=dist[s][u]+e[i].w;
				if (!vis[to]) 
				{
					vis[to]=1;
					q[++r]=to;
				}
			}
	}
}

void dfs(int u,int s)
{
	if (u==n) {ans=(ans+1)%p; return;}
	for (int i=head[u];i;i=e[i].next) 
		if (s+e[i].w<=L && s+e[i].w+dist[to][n]<=L) dfs(to,s+e[i].w);
}

void dfs2(int u,int s)
{
	if (u==n) {ans=(ans+1)%p; return;}
	for (int i=head[u];i;i=e[i].next)
		if (s+e[i].w==dist[1][to]) dfs2(to,s+e[i].w);
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	for (;T--;)
	{
		n=read(); m=read(); k=read(); p=read(); id=1; ans=0;
		for (int i=1;i<=n;i++) head[i]=0;
		for (int i=1;i<=m;i++)
		{
			int u=read(),v=read(),w=read();
			add(u,v,w);
		}
		for (int i=1;i<=n;i++) spfa(i);
		L=dist[1][n]; L+=k;
		if (k!=0) dfs(1,0); else dfs2(1,0);
		printf("%d\n",ans);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define per(i,a,b) for (int i=(a);i>=(b);i--)
#define Rep(i,a,b) for (int i=(a);i<(b);i++)
#define Per(i,a,b) for (int i=(a);i>(b);i--)
void read(int&x){
	x=0;int f=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
#define p(x) cout << x << endl;
int T;
int n,e,k,p;
#define maxn 100005
#define maxe 200005
struct Edge{
	int lnk[maxn],nxt[maxe],val[maxe],son[maxe],tot;
	void add(int x,int y,int z){
		tot++;son[tot]=y;val[tot]=z;nxt[tot]=lnk[x];lnk[x]=tot;
		//tot++;son[tot]=x;val[tot]=z;nxt[tot]=lnk[y];lnk[y]=tot;
	}
	void clear(){
		memset(lnk,0,sizeof(lnk));
		memset(nxt,0,sizeof(nxt));
		memset(val,0,sizeof(val));
		memset(son,0,sizeof(son));
		tot=0;
	}
}edge,edge2;
int dis[maxn],que[maxn];bool vis[maxn];
void spfa(int x){
	memset(dis,63,sizeof(dis));
	memset(que,0,sizeof(que));
	memset(vis,0,sizeof(vis));
	int head=0,tail=1;
	dis[x]=0;vis[x]=true;que[1]=x;
	while (head!=tail){
		head=(head+1)%maxn;
		vis[que[head]]=false;
		for (int j=edge.lnk[que[head]];j;j=edge.nxt[j])
			if (dis[edge.son[j]]>dis[que[head]]+edge.val[j]){
				dis[edge.son[j]]=dis[que[head]]+edge.val[j];
				if (!vis[edge.son[j]]){
					vis[edge.son[j]]=true;tail=(tail+1)%maxn;que[tail]=edge.son[j];
					if (dis[que[(head+1)%maxn]]>dis[que[tail]])swap(que[(head+1)%maxn],que[tail]);
					
				}
			}
	}
}
void spfa2(int x){
	memset(dis,63,sizeof(dis));
	memset(que,0,sizeof(que));
	memset(vis,0,sizeof(vis));
	int head=0,tail=1;
	dis[x]=0;vis[x]=true;que[1]=x;
	while (head!=tail){
		head=(head+1)%maxn;
		vis[que[head]]=false;
		for (int j=edge2.lnk[que[head]];j;j=edge2.nxt[j])
			if (dis[edge2.son[j]]>dis[que[head]]+edge2.val[j]){
				dis[edge2.son[j]]=dis[que[head]]+edge2.val[j];
				if (!vis[edge2.son[j]]){
					vis[edge2.son[j]]=true;tail=(tail+1)%maxn;que[tail]=edge2.son[j];
					if (dis[que[(head+1)%maxn]]>dis[que[tail]])swap(que[(head+1)%maxn],que[tail]);
					
				}
			}
	}
}
int dis1[maxn],dis2[maxn];
bool alex[maxn];
int mindis;
/*int father[maxn];
int getfather(int x){return (father[x]==x)?x:father[x]=getfather(father[x]);}
bool merge(int x,int y){
	x=getfather(x),y=getfather(y);
	if (x!=y){father[x]=y;return true;}
	else return false;
}*/

int in[maxn],dfs_time;
int used[maxn];
bool instk[maxn];
bool dfs(int now){
//printf("%d\n",now);
	used[now]=dfs_time;instk[now]=true;
	for (int j=edge.lnk[now];j;j=edge.nxt[j])if (edge.val[j]==0){
		if (used[edge.son[j]]==dfs_time&&instk[edge.son[j]])return true;
		if (!used[edge.son[j]]){
			if (dfs(edge.son[j]))return true;
		}
	}
	instk[now]=false;
	return false;
}
bool check(){
	//rep(i,1,n)father[i]=i;
	/*rep(i,1,n)if (!alex[i])
		for (int j=edge.lnk[i];j;j=edge.nxt[j])
			if ((!alex[edge.son[j]])&&(edge.val[j]==0)){
				//if (!merge(i,edge.son[j]))return true;
				return true;
			}*/
	memset(in,0,sizeof(in));
	memset(used,0,sizeof(used));
	memset(instk,0,sizeof(instk));
	dfs_time=0;
	rep(i,1,n)if ((!alex[i])&&(!used[i])){
		dfs_time++;if (dfs(i))return true;
	}
	return false;
}
namespace YF{
	#define maxk 55
	int f[maxk][maxn];
	int que[maxn],c[maxn];
	bool vis[maxn];
	void work(){
		memset(f,0,sizeof(f));memset(que,0,sizeof(que));memset(c,0,sizeof(c));
		rep(i,1,n)for (int j=edge.lnk[i];j;j=edge.nxt[j])
			if (dis1[i]+edge.val[j]==dis1[edge.son[j]])c[edge.son[j]]++;
		int head=0,tail=1;
		que[1]=1;f[0][1]=1;
		//rep(i,1,n)printf("%d ",c[i]);printf("\n");
		while (head!=tail){
			head++;
			for (int j=edge.lnk[que[head]];j;j=edge.nxt[j])
				if (dis1[que[head]]+edge.val[j]==dis1[edge.son[j]]){
					(f[0][edge.son[j]]+=f[0][que[head]])%=p;
					c[edge.son[j]]--;if (c[edge.son[j]]==0)que[++tail]=edge.son[j];
				}
		}
		/*rep(i,1,tail)vis[que[i]]=true;
		rep(i,1,n)if (!vis[i])que[++tail]=i;
		rep(i,1,n)printf("%d ",que[i]);printf("\n");*/
		//rep(i,1,n)printf("%d ",f[0][i]);printf("\n");
		rep(t,1,k){
			rep(i,1,n)
			for (int j=edge.lnk[i];j;j=edge.nxt[j])
				if ((dis1[i]+edge.val[j]<=dis1[edge.son[j]]+t)&&(dis1[i]+edge.val[j]!=dis1[edge.son[j]]))
					(f[t][edge.son[j]]+=f[dis1[edge.son[j]]+t-dis1[i]-edge.val[j]][i])%=p;
			
			rep(i,1,tail)
			for (int j=edge.lnk[que[i]];j;j=edge.nxt[j])
				if (dis1[que[i]]+edge.val[j]==dis1[edge.son[j]])
					(f[t][edge.son[j]]+=f[t][que[i]])%=p;
		}
		
		int ans=0;
		rep(i,0,k)(ans+=f[i][n])%=p;
		printf("%d\n",ans);
		//cerr << ans << endl;
	}
}
void work(){
	read(n);read(e);read(k);read(p);
	//printf("%d %d %d %d\n",n,e,k,p);
	edge.clear();edge2.clear();
	rep(i,1,e){
		int x,y,z;read(x);read(y);read(z);
		edge.add(x,y,z);edge2.add(y,x,z);
	}
	spfa(1);rep(i,1,n)dis1[i]=dis[i];
	spfa2(n);rep(i,1,n)dis2[i]=dis[i];
	//rep(i,1,n)printf("%d ",dis1[i]);printf("\n");
	//rep(i,1,n)printf("%d ",dis2[i]);printf("\n");
	mindis=dis[1];memset(alex,0,sizeof(alex));
	rep(i,1,n)if (dis1[i]+dis2[i]>mindis+k)alex[i]=true;else alex[i]=false;
	if (check()){printf("-1\n");return;}
	YF::work();
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);while (T--)work();return 0;
}

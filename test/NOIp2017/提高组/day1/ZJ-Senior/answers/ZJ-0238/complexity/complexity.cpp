#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define per(i,a,b) for (int i=(a);i>=(b);i--)
#define Rep(i,a,b) for (int i=(a);i<(b);i++)
#define Per(i,a,b) for (int i=(a);i>(b);i--)
void read(int&x){
	x=0;int f=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
#define p(x) cout << x << endl;
#define maxn 1005
int T,n,maxC,ansC,nowC;bool is_wrong;
int stk[maxn],cnt;
int val[maxn],father[maxn];
int chr[maxn];//char 
bool used[maxn];
int getr(){
	char ch=getchar();
	int x=0;
	if (ch=='n'){x=101;getchar();return x;}
	else{
		x=ch-'0';ch=getchar();
		if (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
		return x;
	}
}
bool is_can[maxn],now_can;
void work(){
	read(n);getchar();getchar();char ch=getchar();
	if (ch=='1')ansC=0,getchar();else read(ansC);getchar();
	//putchar(ch);//printf("%d %d\n",n,ansC);
	memset(stk,0,sizeof(stk));memset(val,0,sizeof(val));memset(father,0,sizeof(father));memset(used,0,sizeof(used));memset(chr,0,sizeof(chr));memset(is_can,0,sizeof(is_can));
	int now=1;nowC=maxC=0;is_wrong=false;cnt=1;now_can=true;
	rep(i,1,n){
		char ch=getchar();
		//printf("%c\n",ch);
		if (ch=='E'){
			nowC-=val[now];
			used[chr[now]]=false;
			if (!now_can&&is_can[now])now_can=true,is_can[now]=false;
			now=father[now];
			if (now==0)is_wrong=true;
			getchar();
		}
		else{
			getchar();char ch=getchar();if (/*now_can&&*/used[ch-'a'])is_wrong=true;used[ch-'a']=true;
			father[++cnt]=now;now=cnt;
			chr[now]=ch-'a';getchar();
			int d=getr();int e=getr();
			//printf("%d %d\n",d,e);
			//printf("%d %d\n",d,e);
			if (d>e){if (now_can)now_can=false;is_can[now]=true;val[now]=0;/*is_wrong=true;*/}
			if ((d==101&&e==101)||(d<101&&e<101))val[now]=0;
			else{
				val[now]=1;nowC++;
				if (now_can)maxC=max(maxC,nowC);
			}
		}
	}
	if (now!=1)is_wrong=true;
	if (is_wrong)printf("ERR\n");
	else{
		if (ansC==maxC)printf("Yes\n");
		else printf("No\n");
	}
	return;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(T);while (T--)work();return 0;
}

#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include <cctype>
#include <cstdlib>
#include <algorithm>
using namespace std;
template<class T>
void read(T &x)
{
	x = 0;
	char c = getchar();
	while (!isdigit(c))
		c = getchar();
	while (isdigit(c))
		x = x * 10 + (c & 0xf), c = getchar();
}
typedef long long ll;
ll gcd(ll a,ll b)
{
	if (b == 0)
		return a;
	return gcd(b, a % b);	
}
int main()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	ll a, b;
	cin >> a >> b;
	if (gcd(a, b) == 1)
		cout << (a - 1) * (b - 1) - 1 << endl;
	else
		cout << a * b - 1 << endl; 
	return 0;
}



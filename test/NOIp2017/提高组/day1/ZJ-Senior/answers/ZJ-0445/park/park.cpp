#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include <cctype>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <vector>
using namespace std;
template<class T>
void read(T &x)
{
	x = 0;
	char c = getchar();
	while (!isdigit(c))
		c = getchar();
	while (isdigit(c))
		x = x * 10 + (c & 0xf), c = getchar();
}
int N,M,P,K;
const int MAXN = 111111, MAXM = MAXN << 1;
namespace Graph
{
	int head[MAXN], to[MAXM], weight[MAXM], next[MAXM], e;
	void init()
	{
		memset(head, 0, sizeof head);
		e = 0;
	}
	void addEdge(int u, int v, int w)
	{
		++e;
		next[e] = head[u], to[e] = v, weight[e] = w, head[u] = e;
	}
}
void BuildGraph()
{
	Graph::init();
	for (int i = 1; i <= M; ++i)
	{
		int u, v, w;
		read(u), read(v), read(w);
		Graph::addEdge(u, v, w);
	}
}
namespace PART1
{
	int dis[MAXN], cnt[MAXN];
	bool vis[MAXN];
	queue<int> Q;
	void spfa()
	{
		memset(dis, 0x3f, sizeof dis);
		memset(cnt, 0, sizeof cnt);
		memset(vis, 0, sizeof vis);
		dis[1] = 0;
		cnt[1] = 1;
		vis[1] = 1;
		Q.push(1);
		while (!Q.empty())
		{
			int u = Q.front();Q.pop();
			vis[u] = false;
			for (int e = Graph::head[u]; e; e = Graph::next[e])
			{
				int v = Graph::to[e];
				int w = Graph::weight[e];
				if (dis[u] + w == dis[v])
					(cnt[v] += cnt[u]) %= P;
				else if (dis[u] + w < dis[v])
				{
					dis[v] = dis[u] + w;
					cnt[v] = cnt[u];
					if (!vis[v])
					{
						Q.push(v);
						vis[v] = true;
					}
				}
			}
			
		}
	}
	int f[1111][1111];
	int dfs(int u, int w)
	{
		if (f[u][w] == -2)
		return 0;
		if (f[u][w] != -1)
			return f[u][w];
		f[u][w] = -2;
		if (u == N)
		{
			if (w == 0)
			return f[u][w]=1;
			else
			return f[u][w]=0;
		}
		int cnt = 0;
		for (int e = Graph::head[u]; e; e = Graph::next[e])
		{
			int v = Graph::to[e];
			int x = Graph::weight[e];
			(cnt += dfs(v, w - x)) %= P;
		}
		return f[u][w] =cnt;	
	}
	void Solve()
	{
		BuildGraph();
		spfa();
		int ans = 0;
		memset(f, -1, sizeof f);
		for (int i = 0; i <= K; ++i)
		ans += dfs(1, dis[N] + i);
		printf("%d\n",ans);
	}
}
namespace PART2
{
	int dis[MAXN], degree[MAXN], f[MAXN];
	bool vis[MAXN];
	queue<int> Q;
	vector<int> G[MAXN];
	int spfa()
	{
		memset(dis, 0x3f, sizeof dis);
		memset(vis, 0, sizeof vis);
		memset(degree, 0, sizeof degree);
		memset(f, 0, sizeof f);
		dis[1] = 0;
		vis[1] = 1;
		Q.push(1);
		while (!Q.empty())
		{
			int u = Q.front();Q.pop();
			vis[u] = false;
			for (int e = Graph::head[u]; e; e = Graph::next[e])
			{
				int v = Graph::to[e];
				int w = Graph::weight[e];
				if (dis[u] + w < dis[v])
				{
					dis[v] = dis[u] + w;
					if (!vis[v])
					{
						Q.push(v);
						vis[v] = true;
					}
				}
			}
			
		}
		for (int i = 1; i <= N; ++i)
			G[i].clear();
		for (int i = 1; i <= N; ++i)
		{
			for (int e = Graph::head[i]; e; e = Graph::next[e])
			{
				int v = Graph::to[e];
				int w = Graph::weight[e];
				if (dis[i] + w == dis[v])
				{
					G[v].push_back(i);
					++degree[i];
				}
			}
		}
		f[N] = 1;
		int cnt = 0;
		for (int i = 1; i <= N; ++i)
		{
			if (degree[i] == 0)
				Q.push(i), ++cnt;
		}
		while (!Q.empty())
		{
			int u = Q.front();Q.pop();
			for (size_t i = 0, len = G[u].size(); i < len; ++i)
			{
				int v = G[u][i];
				(f[v] += f[u]) %= P;
				if (--degree[v] == 0)
					Q.push(v), ++cnt;
			}
		}
		if (cnt < N)
			return -1;
		else
			return f[1];
	}
	void Solve()
	{
		BuildGraph();
		printf("%d\n", spfa());
	}
}
int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T;read(T);
	while (T--)
	{
		read(N), read(M), read(K), read(P);
		PART2::Solve();
	}

	return 0;
}



#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include <cctype>
#include <cstdlib>
#include <algorithm>
#include <vector>
using namespace std;
template<class T>
void read(T &x)
{
	x = 0;
	char c = getchar();
	while (!isdigit(c))
		c = getchar();
	while (isdigit(c))
		x = x * 10 + (c & 0xf), c = getchar();
}
const int MAXN = 1111;
int dfn = 0;
struct Node
{
	int fuzadu;
	char val;
	vector<int> ch;
} nd[MAXN];
int st[MAXN], Top = 0;
bool vis[128];
char S[128], T[128];
void clear()
{
	for (int i = 1; i <= dfn; ++i)
		nd[i].fuzadu = 0, nd[i].ch.clear();
	dfn = 0;
	Top = 0;
	memset(vis, false, sizeof vis);	
}
int max(int a,int b) {return a < b ? b: a;}
bool Parse()
{
	int l, fuzadu; read(l);
	scanf("%s", S);
	if (S[2] == '1')
		fuzadu = 0;
	else
		sscanf(S + 4, "%d", &fuzadu);
	++dfn;
	nd[dfn].fuzadu = 0;
	st[++Top] = 1;
	int in = 0, out = 0;
	bool flag = true;
	for (int i = 1; i <= l; ++i)
	{
		scanf("%s", S);
		char cmd = S[0];
		if (cmd == 'F')
		{
			scanf("%s", S);
			int now = 0;
			char val = S[0];
			scanf("%s%s", S, T);
			if (vis[val - 'a'])
				flag = false;
			if (!flag)
				continue;
			vis[val - 'a'] = true;
			bool s = false, t = false;
			if (S[0] >= '0' && S[0] <= '9') s = true;
			if (T[0] >= '0' && T[0] <= '9') t = true;
			if (s && t)
			{
				int SS, TT;
				sscanf(S, "%d", &SS);
				sscanf(T, "%d", &TT);
				if (SS <= TT)
					now = 0;
				else
					now = -1;
			}
			else if (!s&&t)
				now = -1;
			else if (s&&!t)
				now = 1;
			else if (!s&&!t)
				now = 0;
			++in;
			st[++Top] = ++dfn;
			nd[dfn].fuzadu = now;
			nd[dfn].val = val;
		}
		else if (cmd == 'E')
		{
			++out;
			if (out > in)
				flag = false;
			if (!flag)
				continue;
			int now = st[Top];
			int fa = st[--Top];
			int max_val = nd[now].fuzadu;
			vis[nd[now].val - 'a'] = false;
			if (nd[now].fuzadu !=-1)
			{
				for (size_t p = 0, len = nd[now].ch.size(); p < len; ++p)
				{
					int v = nd[now].ch[p];
					if (nd[v].fuzadu == -1)
						continue;
					max_val = max(max_val, nd[v].fuzadu + nd[now].fuzadu);
				}
				nd[now].fuzadu = max_val;
			}
			nd[fa].ch.push_back(now);
		}
	}
	if (in != out)
		flag = false;
	if (!flag)
		return false;
	int max_val = 0;
	for (size_t p = 0, len = nd[1].ch.size(); p < len; ++p)
	{
		int v = nd[1].ch[p];
		if (nd[v].fuzadu == -1)
			continue;
		max_val = max(max_val, nd[v].fuzadu);
	}
	if (max_val == fuzadu)
		puts("Yes");
	else
		puts("No");
	return true;
}
void Solve()
{
	clear();
	bool flag = Parse();
	if (!flag)
	{
		puts("ERR");
		return;
	}
}
int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	read(T);
	while (T--)
		Solve();

	return 0;
}



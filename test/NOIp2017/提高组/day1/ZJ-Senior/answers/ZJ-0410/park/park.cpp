#include <bits/stdc++.h>

#define mp make_pair
#define snd second
#define fst first

using namespace std;

const int E = 2E5 + 10, N = 1E5 + 10, K = 52;

void DoNothing() {}

int getint() {
	int res = 0; char ch = getchar();
	while ('9' < ch||ch < '0') ch = getchar();
	while ('0' <= ch&&ch <= '9')
		res = res * 10 + ch - '0', ch = getchar();
	return res;
}

struct edge {
	int u, v, w;
} Edge[E];
	
int f[N][K], dist[2][N], tar[E], w[E], lnk[N], nxt[E], T, n, e, P, k, ans, tot;

void add(int a, int b, int c) {
	nxt[++tot] = lnk[a], lnk[a] = tot, tar[tot] = b, w[tot] = c;
}

namespace graph {

	bool instack[N], inque[N];
	int que[N];
	
	bool dfs(int u) {
		instack[u] = 1;
		for (int j = lnk[u]; j; j = nxt[j])
			if (w[j] == 0) if (instack[tar[j]]) return 1;
										   else if (dfs(tar[j])) return 1;
										   					else DoNothing();
					  else DoNothing();
		return instack[u] = 0;
	}
	bool check_if_zero_circle() {
		for (int i = 1; i <= n; i++)
			if (dist[0][i] + dist[1][i] <= dist[0][n] + k&&dfs(i)) return 1;
		return 0;
	}
	void spfa(int *dist, int source) {
		que[0] = source, dist[source] = 0;
		int head = 0, tail = 0;
		while (head != (tail + 1) % N) {
			for (int j = lnk[que[head]]; j; j = nxt[j])
				if (dist[que[head]] + w[j] < dist[tar[j]]) {
					if (!inque[tar[j]])
						que[tail = (tail + 1) % N] = tar[j], inque[tar[j]] = 1;
					dist[tar[j]] = dist[que[head]] + w[j];
				}
			inque[que[head]] = 0, head = (head + 1) % N;
		}
	}
}

pair <int, int> que[N * K];
int r[N][K];

void topsort() {
	memset(f, 0, sizeof f);
	f[1][0] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 0; j <= k; j++)
			for (int t = lnk[i]; t; t = nxt[t]) {
				int d = dist[0][i] + w[t] + j - dist[0][tar[t]];
				if (d <= k) r[tar[t]][d]++;
			}
	int head = 1, tail = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 0; j <= k; j++)
			if (r[i][j] == 0) que[++tail] = mp(i, j);
	while (head <= tail) {
		for (int j = lnk[que[head].fst]; j; j = nxt[j]) {
			int t = que[head].snd + w[j] + dist[0][que[head].fst] - dist[0][tar[j]];
			if (t <= k) {
				f[tar[j]][t] = (f[tar[j]][t] + f[que[head].fst][que[head].snd]) % P;
				r[tar[j]][t]--;
				if (!r[tar[j]][t]) que[++tail] = mp(tar[j], t);
			}
		}
		head++;
	}
}
int main() {
	freopen("park.in", "r", stdin), freopen("park.out", "w", stdout);
	for (T = getint(); T--;) {
		n = getint(), e = getint(), k = getint(), P = getint();
		for (int i = 1; i <= e; i++)
			Edge[i].u = getint(), Edge[i].v = getint(), Edge[i].w = getint();
		memset(dist, 63, sizeof dist);
		memset(lnk, tot = 0, sizeof lnk);
		for (int i = 1; i <= e; i++)
			add(Edge[i].v, Edge[i].u, Edge[i].w);
		graph :: spfa(dist[1], n);
		memset(lnk, tot = 0, sizeof lnk);
		for (int i = 1; i <= e; i++)
			add(Edge[i].u, Edge[i].v, Edge[i].w);
		graph :: spfa(dist[0], 1);
		if (graph :: check_if_zero_circle()) {
			puts("-1");
			continue;
		}
		topsort(), ans = 0;
		for (int i = 0; i <= k; i++)
			ans = (ans + f[n][i]) % P;
		printf("%d\n", ans);
	}
	return 0;
}

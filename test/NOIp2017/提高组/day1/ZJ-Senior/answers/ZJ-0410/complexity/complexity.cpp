#include <bits/stdc++.h>

using namespace std;

const int N = 1E3 + 10;

int r[N], T, Max, n, k, err;
set <string> Set;
string as[N], str,a,b;
char s[N];


int getint_from_string(char *s) {
	int res = 0;
	while ('9' < *s||*s < '0') s++;
	while ('0' <= *s&&*s <= '9')
		res = res * 10 + *s - '0', s++;
	return res;
}
bool cmp(string a, string b) {
	if (a.length() != b.length())
		return a.length() < b.length();
	for (int i = 0; i < a.length(); i++)
		if (a[i] != b[i]) return a[i] < b[i];
	return 1;
}
int main() {
	freopen("complexity.in", "r", stdin), freopen("complexity.out", "w", stdout);
	for (scanf("%d", &T); T--;) {
		scanf("%d%s", &n, s);
		if (strlen(s + 1) > 4) k = getint_from_string(s);
						  else k = 0;
		r[0] = err = Max = 0;
		Set.clear();
		while (n--) {
			scanf("%s", s);
			if (s[0] == 'F') {
				cin >> as[r[0] + 1];
				if (Set.find(as[r[0] + 1]) != Set.end()) err = 1;
				Set.insert(as[r[0] + 1]);
				cin >> a >> b;
				if (a[0] == 'n'&&b[0] != 'n') r[++r[0]] = -12312132;
				if (a[0] != 'n'&&b[0] == 'n') r[r[0] + 1] = r[r[0]] + 1, r[0]++;
				if (a[0] == 'n'&&b[0] == 'n') r[r[0] + 1] = r[r[0]], r[0]++;
				if (a[0] != 'n'&&b[0] != 'n')
					if (cmp(a, b)) r[r[0] + 1] = r[r[0]], r[0]++;
							  else r[r[0] + 1] = -999999, r[0]++;
				Max = max(Max, r[r[0]]);
			} else {
				if (!r[0]) err = 1, r[0]=5000;
				Set.erase(as[r[0]]), r[0]--;
			}
		}
		if (r[0]) err = 1;
		if (err) {
			puts("ERR");
			continue;
		}
		if (Max == k) puts("Yes");
				else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
struct edge{
	int next,to,value;
}a[200001];
int t,n,m,k,p,cnt,h[100001],b[100001];
void dfs(int x,int pa)
{
	if (x==n) {b[++cnt]=pa;return;}
	for (int i=h[x];i;i=a[i].next)
	{
		dfs(a[i].to,pa+a[i].value);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		int x,y,z;
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			a[i].value=z;a[i].to=y;a[i].next=h[x];h[x]=i;
		}
		cnt=0;
		dfs(1,0);
		sort(b+1,b+cnt+1);
		int limit=b[1]+k,ans=0;
		for (int i=1;i<=cnt;i++)
			if (b[i]<=limit) ans=(ans+1)%p;
		printf("%d\n",ans);
	}
	return 0;
}

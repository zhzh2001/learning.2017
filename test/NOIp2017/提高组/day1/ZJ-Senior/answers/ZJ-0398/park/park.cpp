#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<queue>
using namespace std;
const int MAXN=100000;
const int MAXM=200000;
const int INF=0x3f3f3f3f;
struct Edge{int u,v,d;};
int t,n,m,k,p;
struct G{
	int fir[MAXN+10],nxt[MAXM+10],tote,du[MAXN+10];
	Edge edges[MAXM+10];
	void adde(int u,int v,int d){edges[++tote]=(Edge){u,v,d};nxt[tote]=fir[u];fir[u]=tote;}
	void init()
	{
		for(int i=1;i<=n;++i)fir[i]=0;
		tote=0;
	}
	bool inq[MAXN+10];
	int d[MAXN+10];
	void spfa(int o)
	{
		for(int i=1;i<=n;++i)d[i]=INF;
		queue<int> Q;Q.push(o);d[o]=0;
		while(!Q.empty())
		{
			int u=Q.front();Q.pop();inq[u]=false;
			for(int i=fir[u];i;i=nxt[i])
			{
				Edge& e=edges[i];
				if(d[e.v]>d[u]+e.d)
				{
					d[e.v]=d[u]+e.d;
					if(!inq[e.v])
					{
						inq[e.v]=true;
						Q.push(e.v);
					}
				}
			}
		}
	}
	int res[MAXN+10];
	void solve()
	{
		res[1]=1;for(int i=2;i<=n;++i)res[i]=0;
		for(int i=1;i<=tote;++i)++du[edges[i].v];
		queue<int> Q;Q.push(1);
		while(!Q.empty())
		{
			int u=Q.front();Q.pop();
			for(int i=fir[u];i;i=nxt[i])
			{
				Edge& e=edges[i];
				res[e.v]+=res[u];
				if(res[e.v]>=p)res[e.v]-=p;
				--du[e.v];
				if(!du[e.v])Q.push(e.v);
			}
		}
		printf("%d\n",res[n]);
	}
};
G A,B,C;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		A.init();B.init();C.init();
		for(int i=1;i<=m;++i)
		{
			int u,v,d;
			scanf("%d%d%d",&u,&v,&d);
			A.adde(u,v,d);B.adde(v,u,d);
		}
		A.spfa(1);B.spfa(n);
		if(A.d[n]>=INF){puts("0");continue;}
		for(int i=1;i<=m;++i)
		{
			Edge& e=A.edges[i];
			if(A.d[e.u]+e.d+B.d[e.v]==A.d[n])C.adde(e.u,e.v,e.d);
		}
		C.solve();
	}
	return 0;
}

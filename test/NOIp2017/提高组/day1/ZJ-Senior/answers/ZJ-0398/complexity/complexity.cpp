#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=233;
const int MAXL=100;
const int CH=520233;
struct Line{char i;int x,y,v;};
int t,ln,tot,w,cnt;
inline int readint()
{
	int res;char ch;
	while(!isdigit(ch=getchar()));
	res=ch-'0';
	while(isdigit(ch=getchar()))res=res*10-'0'+ch;
	return res;
}
inline int readit()
{
	int res;char ch;
	while(!isdigit(ch=getchar())&&ch!='n');
	if(ch=='n')return N;
	res=ch-'0';
	while(isdigit(ch=getchar()))res=res*10-'0'+ch;
	return res;
}
Line d[MAXL+10];
bool used[CH+10];
int calc(int o)
{
	if(d[o].x!=N&&d[o].y!=N)return 0;
	else if(d[o].x==N&&d[o].y==N)return 0;
	else return d[o].x<d[o].y;
}
int check(int o){return d[o].x<=d[o].y;}
int solve(int l,int r)
{
	if(l==r){if(used[(int)d[l].i])return -1;return calc(l);}
	int nv=d[l].v;
	for(int i=l+1;i<=r;++i)
		if(d[i].v==nv)
		{
			int rec1=solve(l,i-1),rec2=solve(i,r);
			if(rec1==-1||rec2==-1)return -1;
			else return max(rec1,rec2);
		}
	if(used[(int)d[l].i])return -1;
	used[(int)d[l].i]=true;
	int rec=solve(l+1,r);
	if(rec!=-1)(rec*=check(l))+=calc(l);
	used[(int)d[l].i]=false;
	return rec;
}
void init()
{
	for(int i='a';i<='z';++i)used[i]=false;
	tot=cnt=0;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=readint();
	while(t--)
	{
		init();
		ln=readint();char ch;
		while((ch=getchar())!='1'&&ch!='n');
		if(ch=='1')w=0;
		else w=readint();
		bool flag=true;
		for(int i=1;i<=ln;++i)
		{
			while((ch=getchar())!='F'&&ch!='E');
			if(ch=='E')--cnt;
			else
			{
				++cnt;++tot;
				while((ch=getchar())<'a'||ch>'z');
				d[tot].i=ch;
				d[tot].x=readit();
				d[tot].y=readit();
				d[tot].v=cnt;
			}
			if(cnt<0)flag=false;
		}
		if(!flag){puts("ERR");continue;}
		if(cnt){puts("ERR");continue;}
		int rec=solve(1,tot);
		if(rec==-1)puts("ERR");
		else if(rec==w)puts("Yes");
		else puts("No");
	}
	return 0;
}

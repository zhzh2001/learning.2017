#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;
ll flydsz[2][10][10],flydcalc[10][10];
int T,n,m,k,p;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		memset(flydsz,-1,sizeof(flydsz));
		memset(flydcalc,0,sizeof(flydcalc));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		if (n<=5)
		{
			int a,b,c;
			for (int i=1;i<=m;i++)
			{
				scanf("%d%d%d",&a,&b,&c);
				flydsz[0][a][b]=c;
				flydsz[1][a][b]=c;
			}
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					for (int k=1;k<=n;k++)
						if (flydsz[0][j][i]!=-1&&flydsz[0][i][k]!=-1)
						{
							if (flydsz[0][j][k]==-1)
								flydsz[0][j][k]=flydsz[0][j][i]+flydsz[0][i][k];
							else
								flydsz[0][j][k]=min(flydsz[0][j][i]+flydsz[0][i][k],flydsz[0][j][k]);
						}
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					if (flydsz[0][i][j]==flydsz[1][i][j])
						flydcalc[i][j]=1;
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					for (int k=1;k<=n;k++)
						if (flydsz[1][j][i]!=-1&&flydsz[1][i][k]!=-1)
						{
							if (flydsz[1][j][i]+flydsz[1][i][k]==flydsz[0][j][k])
							{
								flydcalc[j][k]+=(flydcalc[j][i]*flydcalc[i][k])%p;
								flydcalc[j][k]%p;
							}
							if (flydsz[1][j][k]==-1)
								flydsz[1][j][k]=flydsz[1][j][i]+flydsz[1][i][k];
							else
								flydsz[1][j][k]=min(flydsz[1][j][i]+flydsz[1][i][k],flydsz[1][j][k]);
						}
			printf("%lld\n",flydcalc[1][n]);
		}
	}
	return 0;
}

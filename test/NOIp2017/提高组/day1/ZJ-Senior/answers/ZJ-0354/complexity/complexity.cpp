#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
int T,l,gdfzd;
struct odr
{
	int op,blm,x,y;
}zl[100+5];
int used[30],sta[100+5],top;
void init()
{
	l=0,gdfzd=0;
	char c=getchar();
	while (c=='\n')
		c=getchar();
	while (c<='9'&&c>='0')
	{
		l*=10;
		l+=c-'0';
		c=getchar();
	}
	while ((c<'0'||c>'9')&&c!='n')
		c=getchar();
	if (c!='n')
	{
		gdfzd=0;
		while (c!='\n')
			c=getchar();
	}
	else
	{
		while (c<'0'||c>'9')
			c=getchar();
		while (c<='9'&&c>='0')
		{
			gdfzd*=10;
			gdfzd+=c-'0';
			c=getchar();
		}
		while (c!='\n')
			c=getchar();
	}
	for (int i=1;i<=l;i++)
	{
		char cl[30];
			gets(cl);
		int len=strlen(cl);
		if (cl[0]=='F')
		{
			zl[i].op=0;
			int np;
			for (np=1;np<len;np++)
				if (cl[np]!=' ')
					break;
			zl[i].blm=cl[np]-'a';
			for (np=np+1;np<len;np++)
				if (cl[np]!=' ')
					break;
			if (cl[np]=='n')
				zl[i].x=-1;
			else
			{
				zl[i].x=0;zl[i].x+=cl[np]-'0';
				for (np=np+1;np<len;np++)
					if (cl[np]!=' ')
					{
						zl[i].x*=10;
						zl[i].x+=cl[np]-'0';
					}
					else
						break;
			}
			for (np=np+1;np<len;np++)
				if (cl[np]!=' ')
					break;
			if (cl[np]=='n')
				zl[i].y=-1;
			else
			{
				zl[i].y=0;zl[i].y+=cl[np]-'0';
				for (np=np+1;np<len;np++)
					if (cl[np]!=' '&&cl[np]!='\n')
					{
						zl[i].y*=10;
						zl[i].y+=cl[np]-'0';
					}
					else
						break;
			}
		}
		else
		{
			zl[i].op=1;
		}
			
	}
}
int jcyf()
{
	memset(used,0,sizeof(used));
	int t=0;top=-1;
	for (int i=1;i<=l;i++)
	{
		if (zl[i].op==0)
		{
			sta[++top]=i;
			if (used[zl[i].blm]==1)
			{
				return -1;
			}
			used[zl[i].blm]=1;
		}
		else
		{
			if (top==-1)
				return -1;
			used[zl[sta[top]].blm]=0;
			top--;
		}
	}
	if (top!=-1)
		return -1;
	return 0;
}
int work(int zlt,int zlw)
{
	if (zlt>zlw)
	{
		return 0;
	}
	int top=0,nt=zlt,nw,fzd=0;
	for (int i=zlt;i<=zlw;i++)
	{
		if (zl[i].op==0)
			top++;
		else
			top--;
		if (top==0)
		{
			nw=i;
			int ffzd=work(nt+1,nw-1);
			if (zl[nt].x==-1)
			{
				if (zl[nt].y!=-1)
					ffzd=0;
			}
			else
			{
				if (zl[nt].y==-1)
					ffzd++;
				else
				if (zl[nt].y<zl[nt].x)
					ffzd=0;
			}
			fzd=max(fzd,ffzd);
			nt=nw+1;
		}
	}
	return fzd;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		init();
		if (jcyf()==-1)
		{
			printf("ERR\n");
			continue;
		}
		int x=work(1,l);
		if (x!=gdfzd)
			printf("No\n");
		else
			printf("Yes\n");
	}
	return 0;
}

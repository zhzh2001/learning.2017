#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define ll long long
#define rt register int
#include<map> 
using namespace std;
int i,j,k,m,n,x,y,cs,top,sum1,sum2,ans,maxans;
char c[3001],s;string blm,ssta[3001];
char xf[3001],yf[3001];bool ifn[3001];
map<string,bool>pd;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>n;
	for(rt i=1;i<=n;i++)
	{
		cin>>x;scanf("%s",c);
		if(c[2]=='1')cs=0;
		else
		{
			j=4;cs=0;
			while(c[j]>='0'&&c[j]<='9')cs=cs*10+c[j]-48,j++;
		}
		int sto=0,erro=0;maxans=0;top=0;ans=0;
		pd.clear();memset(ifn,0,sizeof(ifn));
		for(rt i=1;i<=x;i++)
		{
			cin>>s;
			if(s=='F')
			{
				cin>>blm;ssta[++top]=blm;
				if(pd[blm])erro=1;
				else pd[blm]=1;
				scanf("%s%s",xf,yf);if(sto||erro)continue;
				if(xf[0]=='n')sum1=1000000000;
				else 
				{
					j=0;sum1=0;
					while(xf[j]>='0'&&xf[j]<='9')sum1=sum1*10+xf[j++]-48;
				}
				if(yf[0]=='n')sum2=1000000000;
				else 
				{
					j=0;sum2=0;
					while(yf[j]>='0'&&yf[j]<='9')sum2=sum2*10+yf[j++]-48;
				}
				if(sum1>sum2)sto=top,maxans=max(ans,maxans);
				else if(sum2-sum1>=100000000)ans++,maxans=max(ans,maxans),ifn[top]=1;
			}
			else
			{
				if(top==sto)sto=0;
				if(top<=0)erro=1;
				if(erro)continue;
				if(s=='E')
				{
					if(ifn[top])ans--;ifn[top]=0;
					pd[ssta[top]]=0;top--;
				}
			}
		}
		
		if(erro||top!=0)printf("ERR\n");
		else if(cs==maxans)printf("Yes\n");
		else if(cs!=maxans)printf("No\n");
		maxans=0;
	}
	return 0;
}

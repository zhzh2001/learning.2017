#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define ll long long
#define rt register int
#define r read()
#define M 400010
using namespace std;
int i,j,k,m,n,x,y,z,p,ed;
int F[M],L[M],N[M],a[M],c[M],fa[M];
int q[8*M],dis[M];bool vis[M];
struct edg{
	int x,y,z;
}edges[M];
inline int read()
{
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x;
}
int ask(int x)
{
	if(fa[x]!=x)fa[x]=ask(fa[x]);
	return fa[x];
}
void add(int x,int y,int z)
{
	ed++;a[ed]=y;c[ed]=z;
	if(!F[x])F[x]=ed;
	else N[L[x]]=ed;
	L[x]=ed;
	edges[ed].x=x;edges[ed].y=y;edges[ed].z=z;
}
void SPfa()
{
    memset(dis,127,sizeof(dis));
	int h=0,t=1;q[1]=1;dis[1]=0;vis[1]=1;
	
	while(h<t)
	{
		x=q[++h];
		for(rt i=F[x];i;i=N[i])
		if(dis[a[i]]>dis[x]+c[i])
		{
			dis[a[i]]=dis[x]+c[i];
			if(!vis[a[i]])q[++t]=a[i];
		}
		vis[x]=0;
	}
}
ll anss[M][53];bool fla[M][53];
int jyh(int x,int y)
{
	if(x==1&&y==0)return 1;
	if(y<0)return 0;
	if(fla[x][y])return anss[x][y];fla[x][y]=1;
	for(rt i=F[x];i;i=N[i])
	{
		int u=dis[x]+y-dis[a[i]]-c[i];if(u>=0)
	    anss[x][y]=(anss[x][y]+jyh(a[i],dis[x]+y-dis[a[i]]-c[i]))%p;
	}
	//if(anss[x][y])cout<<x<<' '<<y<<' '<<anss[x][y]<<endl;
	return anss[x][y];
	
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
    for(int t=r;t;t--)
    {
    	memset(F,0,sizeof(F));
    	memset(L,0,sizeof(L));
    	memset(N,0,sizeof(N));
    	memset(fla,0,sizeof(fla));
		memset(anss,0,sizeof(anss));ed=0;
    	n=r;m=r;k=r;p=r;bool fla=0;
    	for(rt i=1;i<=n;i++)fa[i]=i;
    	for(rt i=1;i<=m;i++)
    	{
    		x=r;y=r;z=r;
    		add(x,y,z);
    		if(!z)
    		{
    			if(ask(x)==ask(y))fla=1;
    			else fa[y]=ask(x);
			}
		}
		if(fla)
		{
			printf("-1\n");
			continue;
		}
		SPfa();j=ed;ed=0;
		memset(F,0,sizeof(F));
		memset(L,0,sizeof(L));
		memset(N,0,sizeof(N));
		for(rt i=1;i<=j;i++)add(edges[i].y,edges[i].x,edges[i].z);
		int ans=0;
	    for(rt i=0;i<=k;i++)ans=(ans+jyh(n,i))%p;
	    cout<<ans<<endl;
	}

	return 0;
}


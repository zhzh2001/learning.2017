#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int N = 105;
char s[20], a[5], b[5], ch[5], p[5];
bool flag;
int top, sum, cntf;
int st[N], mx[N];
char chst[N];
bool vis[10005];

int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int Test; scanf("%d", &Test);
	while (Test --){
		int l;
		scanf("%d%s", &l, s);
		//printf("l = %d\n", l);
		//top记录当前第几层循环，st[i]=0表示当前循环是常数
		//st[i] = -1表示当前循环无法进入 
		//st[0]存当前的循环复杂度 
		//cntf记录有几个f 
		//chst[i]存当前的变量 
		//flag表示是否有语法错误 
		cntf = 0, top = 0, sum = 0;
		flag = true;
		memset(st, 0, sizeof st);
		memset(vis, 0, sizeof vis);
		memset(mx, -1, sizeof mx);
		for (int i = 1; i <= l; i ++){
			scanf("%s", ch);
			if (ch[0] == 'F'){
				//chst[top]是变量名，a, b是循环范围 
				cntf ++; sum ++;
				scanf("%s%s%s", p, a, b);
				chst[++ top] = p[0];
				if (vis[chst[top]]){
					flag = false;
					//puts("1");
				}
				vis[chst[top]] = 1;
				if (a[0] == 'n'){
					if (b[0] == 'n') st[top] = 0;
					else st[top] = -1;
				} else {
					if (b[0] == 'n') st[top] = 1;
					else{
						int x = 0, y = 0;
						int l1 = strlen(a), l2 = strlen(b);
						for (int i = 0; i < l1; i ++)
							x = x*10+a[i]-'0';
						for (int i = 0; i < l2; i ++)
							y = y*10+b[i]-'0';
						if (x <= y) st[top] = 0;
						else st[top] = -1;
					}
				}
			} else {
				if (cntf == 0){
					flag = false;
					//puts("2");
				}
				cntf --; vis[chst[top --]] = 0;
				//if (st[top+1] != -1){
					mx[top+1] = max(mx[top+1], st[top+1]);
				//} else mx[top+1] = -1;
				st[top+1] = 0;
				int tmp = 0;
				if (cntf == 0){
					for (int i = 1; i <= sum; i ++){
						if (mx[i] == -1) break;
						tmp += mx[i];
					}
					st[0] = max(st[0], tmp);
					//printf("-st[0] = %d\n", st[0]);
					sum = 0;
					//memset(st, 0, sizeof st);
					memset(mx, -1, sizeof mx);
				}
			}
		}
		if (cntf > 0) flag = false;
		//printf("cntf = %d\n", cntf);
		if (!flag){
			puts("ERR");
			continue;
		}
		if (s[2] == 'n'){
			int low = 4, high = strlen(s)-1, x = 0;
			for (int i = low; i < high; i ++){
				x = x*10+s[i]-'0';
			}
			//printf("st[0] = %d\n", st[0]);
			if (x != st[0]){
				puts("No");
			} else {
				puts("Yes");
			}
		} else {
			if (st[0] != 0) puts("No");
			else puts("Yes");
		}
	}
	return 0;
}

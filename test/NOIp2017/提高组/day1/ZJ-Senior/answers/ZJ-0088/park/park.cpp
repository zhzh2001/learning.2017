#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;

const int N = 100010, M = 200010;
int n, m, K, MOD, d;
int cnt = 0, ans, top, num, clk;
int head[N], dis[N], x[M], y[M], z[M];
int f[N];
bool vis[N], mark[N];
int dfn[N], low[N], bel[N], st[N];

struct Edge{
	int to, nex, v;
}e[M];

inline void add(int x, int y, int z)
{
	e[++ cnt].to = y;
	e[cnt].nex = head[x];
	e[cnt].v = z;
	head[x] = cnt;
}

inline void spfa(int s)
{
	queue<int> q; q.push(s);
	memset(dis, 0x3f, sizeof dis); dis[s] = 0;
	memset(vis, 0, sizeof vis); vis[s] = 1;
	while (!q.empty()){
		int u = q.front(); q.pop(); vis[u] = 0;
		for (int i = head[u]; i; i = e[i].nex){
			int v = e[i].to;
			if (dis[v] > dis[u]+e[i].v){
				dis[v] = dis[u]+e[i].v;
				if (!vis[v]){
					vis[v] = 1;
					q.push(v);
				}
			}
		}
	}
}

inline void dfs(int u)
{
	if (f[u] != -1) return;
	f[u] = 0; bool flag = true;
	for (int i = head[u]; i; i = e[i].nex){
		int v = e[i].to;
		dfs(v);
		flag = false;
		if (dis[v] == dis[u]+e[i].v)
			f[u] = (f[u]+f[v])%MOD;
	}
	if (flag) f[u] ++;
}

inline void solve(int u, int s)
{
	if (u == n){
		if (s <= d+K){
			ans ++;
			ans %= MOD;
		}
		return;
	}
	for (int i = head[u]; i; i = e[i].nex){
		int v = e[i].to;
		solve(v, s+e[i].v);
	}
}

inline void tarjan(int u)
{
	dfn[u] = low[u] = ++ clk;
	st[++ top] = u;
	for (int i = head[u]; i; i = e[i].nex){
		int v = e[i].to;
		if (!dfn[v]){
			tarjan(v);
			low[u] = min(low[u], low[v]);
		} else if (!bel[v]) low[u] = min(low[u], dfn[v]);
	}
	vector<int> p;
	if (dfn[u] == low[u]){
		num ++;
		while (st[top] != u){
			int t = st[top --];
			p.push_back(t);
			bel[t] = num;
		}
		int t = st[top --];
		p.push_back(t);
		bel[t] = num;
		bool flag = true;
		for (int i = 0; i < p.size(); i ++)
			for (int j = head[i]; j; j = e[j].nex){
				int v = e[j].to;
				if (bel[v] == num && e[j].v != 0){
					flag = false;
					break;
				}
			}
		if (p.size() > 1 && flag) mark[num] = 1;
	}
}

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int Test; scanf("%d", &Test);
	while (Test --){
		scanf("%d%d%d%d", &n, &m, &K, &MOD);
		clk = num = top = 0;
		cnt = 0;
		memset(head, 0, sizeof head);
		for (int i = 1; i <= m; i ++){
			scanf("%d%d%d", &x[i], &y[i], &z[i]);
			add(x[i], y[i], z[i]);
		}
		spfa(1); d = dis[n];
		/*printf("d = %d\n", d);
		for (int i = 1; i <= n; i ++) printf("%d ", dis[i]);
		puts("");*/
		if (n <= 5){
			ans = 0;
			solve(1, 0);
			printf("%d\n", ans);
			continue;
		}
		memset(dfn, 0, sizeof dfn);
		memset(low, 0, sizeof low);
		memset(bel, 0, sizeof bel);
		memset(mark, 0, sizeof mark);
		for (int i = 1; i <= n; i ++)
			if (!dfn[i]) tarjan(i);
		/*for (int i = 1; i <= n; i ++) printf("%d ", bel[i]);
		puts("");*/
		bool flag = true;
		for (int i = 1; i <= num; i ++)
			if (mark[i] == 1){
				puts("-1");
				flag = false;
				break;
			}
		if (!flag) continue;
		//puts("1=1=");
		cnt = 0; memset(head, 0, sizeof head);
		for (int i = 1; i <= m; i ++){
			if (bel[x[i]] == bel[y[i]]) continue;
			add(bel[x[i]], bel[y[i]], z[i]);
		}
		spfa(bel[1]); d = dis[bel[n]];
		/*for (int i = 1; i <= n; i ++) printf("%d ", dis[i]);
		puts("");*/
		memset(f, 0, sizeof f);
		if (K == 0){
			memset(f, -1, sizeof f);
			dfs(1);
			printf("%d\n", f[bel[1]]);
		} else {
			/*f[1] = 1;
			for (int u = 1; u <= n; u ++)
				for (int i = head[u]; i; i = e[i].nex){
					int v = e[i].to;
					if (dis[u]+e[i].v + K <= dis[v]){
						f[v] += f[u];
						f[v] %= MOD;
					}
				}
			printf("%d\n", f[n]);*/
			puts("-1");
		}
	}
	return 0;
}

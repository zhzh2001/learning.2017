#include <stdio.h>
#include <string.h>
#include <stack>
#include <map>
using namespace std;
int comp(int x,int y)
{
	if(y>=x) return -1;
	if(y==-1) return 1;
	if(x==-1) return 0;
	if(y<x) return 0;
}
void todo()
{
	int L,hope=0;
	char ans[100];
	bool iserr=false;
	stack<char>st1;
	stack<int> usless;
	map<char,bool> st2;
	scanf("%d",&L);
	gets(ans);
	int nowmax=0;
	{
		int pos=0;
		while(ans[pos]!='(') pos++;
		pos++;
		if(ans[pos]=='1') hope=0;
		else
		{
			pos+=2;
			for(;ans[pos]!=')';pos++)
			{
				hope*=10;
				hope+=ans[pos]-'0';
			}
		}
	}
	int disable=-1;
	while(L)
	{
		char s1[1000];
		gets(s1);
		if(s1[0]!='F'&&s1[0]!='E') iserr=true;
		if(s1[0]=='F')
		{
			char val=s1[2];
			if(st2[val]) iserr=true;
			st2[val]=true;
			st1.push(val);
			if(disable!=-1) {
				L--;
				continue;
			}
			int x=0,y=0,t=4;
			if(s1[t]!='n')
			{
				for(;s1[t]!=' ';t++)
				{
					x=x*10+s1[t]-'0';
				}
			}
			else 
				x=-1,t++;
			t++;
			if(s1[t]!='n')
			{
				for(;t<strlen(s1);t++)
				{
					y=y*10+s1[t]-'0';
				}
			}
			else y=-1;
			if(comp(x,y)==1) nowmax=(st1.size()-usless.size()>nowmax)?st1.size()-usless.size():nowmax;
			else if(comp(x,y)==0)
			disable=st1.size();
			else
			{
				usless.push(st1.size());
			}
		}
		else
		{
			if(st1.size()==disable)
			disable=-1;
			if(!usless.empty())if(usless.top()==st1.size()) usless.pop();
			if(st1.empty()) iserr=true;
			else{char val=st1.top();
			st1.pop();
			st2[val]=false;}
		}
		L--;
	}
	if(!st1.empty()) iserr=true;
	if(iserr) printf("ERR\n");
	else if(hope==nowmax) printf("Yes\n");
	else printf("No\n");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int count;
	scanf("%d",&count);
	while(count)
	{
		todo();
		count--;
	}
}

#include <stdio.h>
#include <memory.h>
#include <map>
#include <vector>
#include <queue>
#include <iterator>
typedef unsigned long long ull;
using namespace std;
struct pii
{
	int to;
	int c;
};
pii makp(int a,int b)
{
	pii t;
	t.to=a;
	t.c=b;
	return t;
}
void todo()
{
	ull n,m,k,p;
	scanf("%llu%llu%llu%llu",&n,&m,&k,&p);
	vector<pii>edge[n+1];
	for(int i=0;i<m;i++)
	{
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		edge[a].push_back(makp(b,c));
	}
	int isinque[n+1];
	queue<int> q;
	int dis[n+1];
	memset(dis,0,sizeof(dis));
	memset(isinque,0,sizeof(isinque));
	q.push(1);
	while(!q.empty())
	{
		int now=q.front();
		q.pop();
		isinque[now]=0;
		vector<pii>::iterator it=edge[now].begin();
		for(;it!=edge[now].end();it++)
		{
			if(dis[(*it).to]>dis[now]+(*it).c||dis[(*it).to]==0) 
			{
				dis[(*it).to]=dis[now]+(*it).c;
				if(!isinque[(*it).to])
				q.push((*it).to),isinque[(*it).to]=1;
			}
		}
	}
	printf("%d\n",dis[n]);
	int maxavail=dis[n]+k;
	map<int,ull> dp[n+1];
	int maxdp[n+1];
	memset(isinque,0,sizeof(isinque));
	memset(maxdp,0,sizeof(maxdp));
	dp[1][0]=1;
	q.push(1);
	while(!q.empty())
	{
		int now=q.front();
		q.pop();
		isinque[now]=0;
		for(int i=0;i<=maxdp[now];i++)
		{
		if(dp[now][i]==0) continue;
		vector<pii>::iterator it=edge[now].begin();
		for(;it!=edge[now].end();it++)
		{
			int tow=(*it).to,cost=(*it).c;
			if(i+cost>maxavail) continue;
			dp[tow][i+cost]+=dp[now][i];
			printf("%d&%d %d&%d*%d\n",now,i,tow,i+cost,dp[tow][i+cost]);
			dp[tow][i+cost]%=p;
			if(maxdp[tow]<i+cost) maxdp[tow]=i+cost;
			if(!isinque[tow])
				q.push(tow);isinque[tow]=1;
		}
		}
	}
	ull ans=0;
	for(int i=maxavail-2*k;i<=maxavail;i++)
	{
		ans+=dp[n][i];
		printf("%d\n",dp[n][i]);
	}
	
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) todo();
	return 0;
}

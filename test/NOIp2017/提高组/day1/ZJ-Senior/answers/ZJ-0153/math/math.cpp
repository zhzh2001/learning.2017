#include <stdio.h>
#include <memory.h>
#include <map>
using namespace std;
int dp[100000002];
int gcd(int x,int y)
{
	while(x!=y)
	{
		y-=x;
		if(x>y)
		{
			int t=x;
			x=y;
			y=t;
		}
	}
	return x;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int a,b;
	map<int,bool>st;
	scanf("%d%d",&a,&b);
	if(a>b)
	{
		int t=a;
		a=b;
		b=t;
	}
	int gcdab=gcd(a,b);
	int maxt=a*b/gcdab;
	memset(dp,0,sizeof(dp));
	bool change=true;
	dp[a]=1;
	dp[b]=1;
	while(change)
	{
		change=false;
		int i=maxt;
		for(;i>=a;i--)
		{
			if(dp[i]&&st[i]==0)
			{
				st[i]=1;
				change=true;
				break;
			}
		}
		if(!change) break;
		for(int j=i;j<=maxt;j+=a)
		{
			dp[j]=1;
			if(!dp[j])	change=true;
		}
		for(int j=i;j<=maxt;j+=b)
		{
			dp[j]=1;
			if(!dp[j])	change=true;
		}
	}
	int i=maxt;
	while(dp[i]&&i>=a) i--;
	printf("%d\n",i);
}

#include <bits/stdc++.h>
#define fi first
#define se second
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


const int N=(int)1e5+5,M=(int)2e5+5,MAX_K=55;

int n,mod,tot_edge,lst[N],from[M],to[M],val[M],dist[N],par[N],dp[MAX_K][N];
bool mark[MAX_K][N];

struct Edge
{
	int to,nxt,cost;
	Edge(){}
	Edge(int to,int nxt,int cost):to(to),nxt(nxt),cost(cost){}
}
edge[M];

void add_edge(int u,int v,int cost)
{
	edge[tot_edge]=Edge(v,lst[u],cost);
	lst[u]=tot_edge++;
}

inline void mod_add(int &a,int b)
{
	if((a+=b)>=mod)a-=mod;
}

void Dijkstra()
{
	rep(i,1,n+1)dist[i]=INT_MAX;
	static priority_queue<pii,vector<pii>,greater<pii> > pque;
	pque.push(pii(dist[1]=0,1));
	while(!pque.empty())
	{
		pii elm=pque.top();
		pque.pop();
		int u=elm.se,dis=elm.fi;
		if(dist[u]<dis)continue;
		for(int i=lst[u];~i;i=edge[i].nxt)
		{
			int v=edge[i].to;
			if(dist[v]>dis+edge[i].cost)
			{
				dist[v]=dis+edge[i].cost;
				par[v]=u;
				pque.push(pii(dist[v],v));
			}
		}
	}
}

bool flag;
bool mark2[MAX_K][N];

int DP(int k,int u)
{
	if(mark2[k][u])
	{
		flag=true;
	}
	if(mark[k][u])return dp[k][u];
	mark[k][u]=mark2[k][u]=true;
	for(int i=lst[u];~i;i=edge[i].nxt)
	{
		int v=edge[i].to,cost=edge[i].cost;
		if(dist[v]==INT_MAX)continue;
		int tar=dist[u]-dist[v]-cost+k;
//		printf("!%d %d %d %d %d %d\n",k,u,v,tar,dist[u],dist[v]);//
		assert(tar<=k);
		if(tar>=0)mod_add(dp[k][u],DP(tar,v));
	}
	mark2[k][u]=false;
	return dp[k][u];
}

void solve()
{
	flag=false;
	int m,K;
	rd(n),rd(m),rd(K),rd(mod);
	tot_edge=0;
	memset(lst,-1,n+1<<2);
	for(int i=0,u,v,cost;i<m;++i)
	{
		rd(u),rd(v),rd(cost);
		add_edge(u,v,cost);
		from[i]=u;
		to[i]=v;
		val[i]=cost;
	}
	Dijkstra();
	rep(i,0,K+1)rep(j,1,n+1)dp[i][j]=mark[i][j]=mark2[i][j]=0;
	dp[0][1]=1%mod;
	mark[0][1]=true;
	tot_edge=0;
	memset(lst,-1,n+1<<2);
	rep(i,0,m)add_edge(to[i],from[i],val[i]);
	int ans=0;
	rep(i,0,K+1)mod_add(ans,DP(i,n));
//	rep(i,0,K+1)rep(j,1,n+1)printf("#%d %d %d\n",i,j,dp[i][j]);//
	printf("%d\n",ans);
}

int main()
{
	
//	freopen("park2.in","r",stdin);//
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	int cas;
	for(cin>>cas;cas--;)solve();
	return 0;
	
}


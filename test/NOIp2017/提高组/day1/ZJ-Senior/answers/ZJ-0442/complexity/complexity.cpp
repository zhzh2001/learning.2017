#include <bits/stdc++.h>
#define fi first
#define se second
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


bool flag,flag2;
int n,cnt;
char str[10];
bool mark[1000];

int convert()
{
	int res=0;
	rep(i,0,strlen(str))res=res*10+(str[i]^48);
	return res;
}

int calc(int dep)
{
	int res=0;
	while(cnt<n&&flag)
	{
		++cnt;
		scanf("%s",str);
		if(str[0]=='E')
		{
			if(!dep)
			{
				flag=false;
				return -1;
			}
			flag2=true;
			return res;
		}
		scanf("%s",str);
		if(mark[str[0]])
		{
			flag=false;
			rep(i,0,2)scanf("%s",str);
			return -1;
		}
		
		int memo=str[0];
		mark[memo]=true;
		bool mark1=false,mark2=false;
		int x,y;
		
		scanf("%s",str);
		if(str[0]=='n')mark1=true;
		else x=convert();
		
		scanf("%s",str);
		if(str[0]=='n')mark2=true;
		else y=convert();
		
		if(mark1&&mark2)Max(res,calc(dep+1));
		if(mark1&&!mark2)calc(dep+1);
		if(!mark1&&mark2)Max(res,1+calc(dep+1));
		if(!mark1&&!mark2)
		{
			if(x<=y)Max(res,calc(dep+1));
			else calc(dep+1);
		}
		if(!flag)return -1;
		if(flag2)
		{
			flag2=mark[memo]=false;
			continue;
		}
		
		if(cnt==n)
		{
			flag=false;
			return -1;
		}
		++cnt;
		scanf("%s",str);
		if(str[0]!='E')
		{
			flag=false;
			rep(i,0,3)scanf("%s",str);
			return -1;
		}
		mark[memo]=false;
	}
	if(!flag)return -1;
	return res;
}

void solve()
{
	rd(n);
	scanf("%s",str);
	int len=strlen(str),res=0;
	if(len>4)rep(i,4,len-1)res=res*10+(str[i]^48);
//	debug(n),debug(res);//
	cnt=0;
	flag=true;
	flag2=false;
	memset(mark,false,sizeof mark);
	int ans=calc(0);
	for(;cnt<n;++cnt)
	{
		scanf("%s",str);
		if(str[0]=='F')rep(i,0,3)scanf("%s",str);
	}
	if(!flag)puts("ERR");
	else puts(ans==res?"Yes":"No");
}

int main()
{
	
//	freopen("complexity1.in","r",stdin);//
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int cas;
	for(cin>>cas;cas--;)solve();
	return 0;
	
}


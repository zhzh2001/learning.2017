#include <bits/stdc++.h>
#define fi first
#define se second
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}


int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}

int main()
{
	srand(time(NULL));
	freopen("math.in","w",stdout);
	int a=rand()%1000+2,b=rand()%1000+2;
	do
	{
		a=rand()%1000+2,b=rand()%1000+2;
	}
	while(gcd(a,b)>1);
	printf("%d %d\n",a,b);
	return 0;
}


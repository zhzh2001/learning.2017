#include <bits/stdc++.h>
#define fi first
#define se second
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}


int main()
{
	
//	freopen("math2.in","r",stdin);//
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	int a,b;
	cin>>a>>b;
	if(a>b)swap(a,b);
	cout<<(ll)(a-1)*b-a<<endl;
	return 0;
	
}


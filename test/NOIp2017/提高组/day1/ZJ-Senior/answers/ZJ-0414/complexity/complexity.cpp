#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<queue>
#include<string>
#include<map>
using namespace std;
typedef long long LL;
#define G c=getchar()
inline int read() 
{
	int x=0,f=1;char G;
	while(c<'0'||'9'<c){if(c=='-')f*=-1;G;}
	while('0'<=c&&c<='9')x=x*10+c-48,G;
	return x*f;
}
string S;int ans;
int dt[100][8];
int zm[100][8];
int L,flg;
int www[500];
int ss[500];//某层变量 
int cc[500],xy[500];//某层贡献 
/*

4 O(n^2)
F x 5 n
F y 10 n
E
E
 
*/ 
int dg()//保证栈正确 
{
	memset(www,0,sizeof(www));
	memset(cc,0,sizeof(cc));
	memset(ss,0,sizeof(ss));
	memset(xy,0,sizeof(xy));
	int stk=0,dep=0,sum=0;
	for(int i=1;i<=L;i++)
	{
		if(dt[i][1]==1)
		{
			stk++;
			xy[stk]=xy[stk-1];
			
			int cs=0;
			if(zm[i][3]==1&&zm[i][4]==1)cs=0;
			else if(zm[i][3]==0&&zm[i][4]==1)cs=1;
			else if(zm[i][3]==1&&zm[i][4]==0)cs=0,xy[stk]=1;
			else if(zm[i][3]==0&&zm[i][4]==0)
			{
				if(dt[i][3]>dt[i][4])xy[stk]=1;
				cs=0;
			}
			if(!xy[stk])
			{
				cc[stk]=cs;
				dep+=cs;
			}
		/*	printf("F %d ",dt[i][2]);
			if(zm[i][3])printf("n ");else printf("%d ",dt[i][3]);
			if(zm[i][4])printf("n \n");else printf("%d \n",dt[i][4]);*/
			ss[stk]=dt[i][2];
			if(www[dt[i][2]])flg=2;
			www[dt[i][2]]=1;
		//	printf("%d in\n",dt[i][2]);
		}
		else
		{
		//	printf("E\n");
			if(!xy[stk])
			{
				sum=max(sum,dep);
				dep-=cc[stk];
			}
			www[ss[stk]]=0;
		//	printf("%d out\n",ss[stk]);
			stk--;
		}
	}
	return sum;
//	printf("%d\n",sum);
}

int main()
{
//	freopen("w.in","r",stdin);
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while(T--)
	{
		int stk=0;char c;ans=0;flg=0;
		memset(www,0,sizeof(www));
		memset(dt,0,sizeof(dt));
		memset(zm,0,sizeof(zm));
		cin>>L>>S;
		int cnt=0;
		c=S[4];if('0'<=c&&c<='9')ans=ans*10+c-48;
		c=S[5];if('0'<=c&&c<='9')ans=ans*10+c-48;
		for(int i=1;i<=L;i++)
		{
			cin>>S;
			if(S=="F")
			{
				stk++;
				dt[i][1]=1;
				cin>>c;
				if(www[c])dt[i][2]=www[c];
				else www[c]=dt[i][2]=++cnt;
				cin>>S;c=S[0];
				if(S!="n")
				{
					int x=0;
					c=S[0];if('0'<=c&&c<='9')x=x*10+c-48;
					c=S[1];if('0'<=c&&c<='9')x=x*10+c-48;
					dt[i][3]=x;
				}
				else zm[i][3]=1;//n
				cin>>S;c=S[0];
				if(S!="n")
				{
					int x=0;
					c=S[0];if('0'<=c&&c<='9')x=x*10+c-48;
					c=S[1];if('0'<=c&&c<='9')x=x*10+c-48;
					dt[i][4]=x;
				}
				else zm[i][4]=1;//n
			}
			else
			{
				stk--;
				if(stk<0)flg=1;
				dt[i][1]=2;
			}
		}
		if(stk!=0)flg=1;
//		printf("%5d     ",ans);
		if(flg){cout<<"ERR\n";continue;}
		int ss=dg();
		if(flg){cout<<"ERR\n";continue;}
		if(ans==ss)puts("Yes");
		else puts("No");
	//	while(1){}
	//	else cout<<"Read\n";
	}
	fclose(stdin);
	fclose(stdout);
}

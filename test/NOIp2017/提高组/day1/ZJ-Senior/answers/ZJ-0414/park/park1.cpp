#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
typedef long long LL;
#define maxn 200010//待定
#define maxm 400010
#define G c=getchar()
inline int read() 
{
	int x=0,f=1;char G;
	while(c<'0'||'9'<c){if(c=='-')f*=-1;G;}
	while('0'<=c&&c<='9')x=x*10+c-48,G;
	return x*f;
}
#define AE(x,y,z) v[Si]=y,w[Si]=z,nxt[Si]=idx[x],idx[x]=Si++
int idx[maxn],nxt[maxm],v[maxm],w[maxm];
int n,m,k,p,Si,inq[maxn],d[maxn];
queue <int> Q;
int q[maxn];
int vis[maxn],ins[maxn],sg,dp[51][maxn];
void dfs(int u)
{
	vis[u]=1;ins[u]=1;
	for(int i=idx[u];i;i=nxt[i])
		if(w[i]==0)
		{
			if(ins[v[i]])sg=1;	
			if(!vis[v[i]])dfs(v[i]);
		}	
	ins[u]=0;
}
void add(int &x,int y)
{
	x+=y;
	if(x>p)x-=p;
}
int www;
void BF(int u,int S)
{
	if (S>d[n]+k)return;
	if (u==n)www++;
	for(int i=idx[u];i;i=nxt[i])
		BF(v[i],S+w[i]);
}
int main()
{
//	printf("%.4lf",sizeof(dp)/1024.0/1024.0);
	freopen("w.in","r",stdin);
//	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	int T=read();
	while(T--)
	{
		sg=0;
		memset(idx,0,sizeof(idx));Si=1;
		n=read(),m=read(),k=read(),p=read();
		for(int i=1;i<=m;i++)
		{
			int x=read(),y=read(),z=read();
			AE(x,y,z);
		}
		//SPFA
		while(!Q.empty())Q.pop();d[1]=0;
		for(int i=2;i<=n;i++)d[i]=1010000000,inq[i]=0,vis[i]=0;
		Q.push(1);inq[1]=1;
		while(!Q.empty())
		{
			int u=Q.front();inq[u]=0;Q.pop();
			for(int i=idx[u],y;i;i=nxt[i])
				if(d[u]+w[i]<d[y=v[i]])
				{
					d[y]=d[u]+w[i];
					if(!inq[y])inq[y]=1,Q.push(y);
				}
		}
//		www=0;BF(1,0);printf("%d\n",www);
		//SPFA
	//	for(int i=1;i<=n;i++)printf("%d ",d[i]);puts("");
		for(int u=1;u<=n;u++)
			for(int i=idx[u];i;i=nxt[i])w[i]=d[u]+w[i]-d[v[i]];
//		for(int i=1;i<=n;i++)if(!vis[i])dfs(i);//有点问题 
	//	if(sg){puts("-1");continue;}
		//BFS序
		int l=0;
		for(int i=1;i<=n;i++)vis[i]=0;
		Q.push(1);q[++l]=1;vis[1]=1;
		while(!Q.empty())
		{
			int u=Q.front();Q.pop();
			for(int i=idx[u],y;i;i=nxt[i])
				if(w[i]==0&&!vis[y=v[i]])
				{
					q[++l]=y;
					vis[y]=1;
				}
		}
		for(int i=1;i<=n;i++)if(!vis[i])q[++l]=i;
		dp[0][1]=1;
//		for(int i=1;i<=n;i++)printf("%d ",q[i]);puts("");
		for(int s=0;s<=k;s++)
		{
			for(int t=1;t<=n;t++)
			{
				int u=q[t];
				if(dp[s][u])
					for(int i=idx[u];i;i=nxt[i])
						if(w[i]==0)add(dp[s][v[i]],dp[s][u]);
			}
//			for(int s=0;s<=k;s++,puts(""))for(int i=1;i<=n;i++)printf("%4d ",dp[s][i]);puts("\n");
			for(int t=1;t<=n;t++)
			{
				int u=q[t];
				if(dp[s][u])
					for(int i=idx[u];i;i=nxt[i])
						if(w[i]!=0)if(s+w[i]<=k)add(dp[s+w[i]][v[i]],dp[s][u]);
			}
//			for(int s=0;s<=k;s++,puts(""))for(int i=1;i<=n;i++)printf("%4d ",dp[s][i]);puts("\n");
		}
		int ans=0;
		for(int i=0;i<=k;i++)
			add(ans,dp[i][n]);//,printf("%d ",dp[i][n]);
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
}
//检查数组大小          [ ]
//检查多组数据初始化    [ ]
 

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
#include<vector>
#include<queue>
#define M 100005
#define ll long long
#define oo 1000000000
using namespace std;
int n,m,k,P;
struct node{
	int to,v;
	bool operator <(const node &tmp)const{
		return v>tmp.v;
	}	
};
struct W{
	int x,y,z;
}B[M<<1];
struct P1{
	vector<node>vec[15];
	int val[15][15],ans,res;
	void dfs(int now,int sum){
		//printf("%d %d\n",now,sum);
		if(sum>val[1][n]+k)return;
		if(now==n){
			ans++;
		}
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			dfs(tmp.to,sum+tmp.v);
		}
	}	
	
	void solve(){
		ans=0;
		memset(val,63,sizeof(val));
		for(int i=1,a,b,c;i<=m;i++){
			scanf("%d %d %d",&a,&b,&c);
			vec[a].push_back((node){b,c});
			if(c<val[a][b]){
				val[a][b]=c;
			}	
		}
		for(int i=1;i<=n;i++)val[i][i]=0;
		for(int k=1;k<=n;k++){
			for(int i=1;i<=n;i++){
				for(int j=1;j<=n;j++){
					val[i][j]=min(val[i][j],val[i][k]+val[k][j]);
				}	
			}
		}
		dfs(1,0);
		printf("%d\n",ans);
		for(int i=1;i<=n;i++)vec[i].clear();
	}
}bao;
struct P2{
	vector<node>vec[M];
	priority_queue<node>Q;
	int cnt[M],dis[M];
	bool mark[M];
	void dij(int s){
		while(!Q.empty())Q.pop();
		memset(cnt,0,sizeof(cnt));
		memset(mark,0,sizeof(mark));
		for(int i=1;i<=n;i++)dis[i]=oo;
		dis[1]=0;
		cnt[1]=1;
		Q.push((node){1,0});
		while(!Q.empty()){
			node tm=Q.top();
			Q.pop();
			if(mark[tm.to])continue;
			mark[tm.to]=1;
			int now=tm.to;
			for(int j=0;j<vec[now].size();j++){
				node tmp=vec[now][j];
				if(mark[tmp.to])continue;
				if(dis[tmp.to]>dis[now]+tmp.v){
					dis[tmp.to]=dis[now]+tmp.v;
					Q.push((node){tmp.to,dis[tmp.to]});
					cnt[tmp.to]=0;
				}
				if(dis[tmp.to]==dis[now]+tmp.v){
					cnt[tmp.to]+=cnt[now];
					cnt[tmp.to]%=P;
				}
			}
		}
	}	
	void solve(){
		memset(dis,63,sizeof(dis));
		for(int i=1;i<=m;i++){
			vec[B[i].x].push_back((node){B[i].y,B[i].z});
		}
		dij(1);
		printf("%d\n",cnt[n]);
		for(int i=1;i<=n;i++){
			vec[i].clear();
		}
	}
}da;
struct nndd{
	int hav,to,v;
	bool operator <(const nndd &tmp)const{
		return v>tmp.v;
	}	
};
struct P3{
	vector<node>vec[M];
	priority_queue<nndd>Q;
	int cnt[52][M],dis[52][M];
	bool mark[52][M];
	void dij(int s){
		while(!Q.empty())Q.pop();
		memset(cnt,0,sizeof(cnt));
		memset(mark,0,sizeof(mark));
		for(int j=0;j<=50;j++)for(int i=1;i<=n;i++)dis[j][i]=oo;
		dis[0][1]=0;
		cnt[0][1]=1;
		Q.push((nndd){0,1,0});
		while(!Q.empty()){
			nndd tm=Q.top();
			Q.pop();
			//printf("%d %d %d\n",tm.hav,tm.to,cnt[tm.hav][tm.to]);
			if(mark[tm.hav][tm.to])continue;
			mark[tm.hav][tm.to]=1;
			int now=tm.to;
			for(int j=0;j<vec[now].size();j++){
				node tmp=vec[now][j];
				if(dis[tm.hav][tmp.to]>dis[tm.hav][now]+tmp.v){
					dis[tm.hav][tmp.to]=dis[tm.hav][now]+tmp.v;
					Q.push((nndd){tm.hav,tmp.to,dis[tm.hav][tmp.to]});
					cnt[tm.hav][tmp.to]=0;
				}
				if(dis[tm.hav][tmp.to]==dis[tm.hav][now]+tmp.v){
					cnt[tm.hav][tmp.to]+=cnt[tm.hav][now];
					cnt[tm.hav][tmp.to]%=P;
				}
				if(dis[tm.hav][tmp.to]<dis[tm.hav][now]+tmp.v){
					int cha=dis[tm.hav][now]+tmp.v-dis[tm.hav][tmp.to];
					if(cha+tm.hav<=k){
						if(dis[tm.hav+cha][tmp.to]>dis[tm.hav][now]+tmp.v){
							dis[tm.hav+cha][tmp.to]=dis[tm.hav][now]+tmp.v;
							Q.push((nndd){tm.hav+cha,tmp.to,dis[tm.hav+cha][tmp.to]});
							cnt[tm.hav+cha][tmp.to]=0;
						}
						if(dis[tm.hav+cha][tmp.to]==dis[tm.hav][now]+tmp.v){
							cnt[tm.hav+cha][tmp.to]+=cnt[tm.hav][now];
							cnt[tm.hav+cha][tmp.to]%=P;
						}
					}
				}
			}
		}
	}	
	void solve(){
		//puts("!");
		for(int i=1;i<=m;i++){
			vec[B[i].x].push_back((node){B[i].y,B[i].z});
		}
		dij(1);
		for(int i=1;i<=k;i++){
			cnt[0][n]+=cnt[i][n];
			cnt[0][n]%=P;
		}
		printf("%d\n",cnt[0][n]);
		for(int i=1;i<=n;i++){
			vec[i].clear();
		}
	}
}qing;
int main(){
	int T,i,j,p=0;
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>T;
	while(T--){
		p=0;
		scanf("%d %d %d %d",&n,&m,&k,&P);
		if(n<=6&&m<=30){
			bao.solve();
			continue;
		}
		for(i=1;i<=m;i++){
			scanf("%d %d %d",&B[i].x,&B[i].y,&B[i].z);
			if(B[i].z==0)p=1;
		}
		if(k==0){
			da.solve();
		}
		else qing.solve();
	}	
	return 0;
}	
/*
3
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3


2
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
2 2 3 10
1 2 2
2 1 2

1
4 4 2 10

*/

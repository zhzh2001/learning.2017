#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
#include<vector>
#include<queue>
#define M 100005
#define ll long long
#define oo 1000000000
using namespace std;
int n,m;
struct P1{
	bool dp[M];
	void solve(){
		memset(dp,0,sizeof(dp));
		dp[0]=1;
		int w=n*m,mx=1;
		for(int i=1;i<=w;i++){
			if(i>=n&&dp[i-n])dp[i]=1;
			if(i>=m&&dp[i-m])dp[i]=1;
		}
		for(int i=1;i<=w;i++){
			if(!dp[i])mx=i;
		}
		printf("%d\n",mx);
	}
}bao;
struct P2{
	void extgcd(ll a,ll b,ll &x,ll &y){
		if(b==0){
			x=1;y=0;
			return;
		}	
		extgcd(b,a%b,y,x);
		y-=x*(a/b);
	}
	ll gcd(ll a,ll b){
		if(b==0)return a;
		return gcd(b,a%b);
	}
	void solve(){
		ll x,y;
		if(n>m)swap(n,m);
		extgcd(n,m,x,y);
		x=(x%m+m)%m;
		y=(1LL-1LL*n*x)/m;
		ll xx=x-m;
		ll yy=y+n;
		if(abs(y)%abs(yy)==0){
			ll shu2=abs(y)%abs(yy);
			ll shu1=1LL*x*(abs(y)/abs(yy))-1-x;
			printf("%lld\n",abs(xx)*abs(y)/abs(yy)-1);
		}
		else {
			ll shu2=abs(y)%abs(yy);
			ll shu1=1LL*x*(abs(y)/abs(yy))-1-x;
			ll ans=shu1*n+shu2*m-1;
			if(ans<0)ans=1LL*n*m-ans;
			printf("%lld\n",ans);	
		}
		//printf("%lld\n",((x%11+11)%11));
	}
}da;
struct P3{
	bool dp[32000000];
	void solve(){
		dp[0]=1;
		int mx=1;
		for(int i=1;i<=30000000;i++){
			if(i>=n&&dp[i-n])dp[i]=dp[i-n];
			if(i>=m&&dp[i-m])dp[i]=dp[i-m];
		}
		for(int i=1;i<=30000000;i++){
			if(!dp[i]&&i>mx)mx=i;
		}
		printf("%d\n",mx);
	}
}qing;
int main(){
	int i,j;
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	cin>>n>>m;
	if(n<=50&&m<=50)bao.solve();
	else if(n<=10000&&m<=10000)qing.solve();
	else da.solve();
	return 0;
}


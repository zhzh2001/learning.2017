#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
#include<vector>
#include<queue>
#define M 100005
#define ll long long
#define oo 1000000000
using namespace std;
int n,m,res,ans,p,top;
char str[1005],str1[1005],str2[1005],str3[1050],str4[1005];
bool mark[500];
struct node{
	int x,y,z;
}stk[M];
int main(){
	int T,i,j,a,b,c,hh=0;
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>T;
	while(T--){
		top=0;res=0;hh=0;ans=0;
		memset(mark,0,sizeof(mark));
		scanf("%d %s",&n,str);
		res=0;c=0;
		if(str[2]=='1')p=1;
		else {
			p=2;
			int w=strlen(str);
			c=0;
			for(j=4;j<w-1;j++){
				c=c*10+str[j]-'0';
			}
		}
		//printf("c=%d\n",c);
		for(i=1;i<=n;i++){
			scanf("%s",str1);
			a=b=0;
			if(str1[0]=='F'){
				scanf("%s %s %s",str2,str3,str4);
				int len1=strlen(str3);
				int len2=strlen(str4);
				a=0;b=0;
				if(mark[str2[0]])ans=-1;
				mark[str2[0]]=1;
				if(str3[0]=='n'&&str4[0]=='n'){
					stk[++top]=(node){0,str2[0],0};
				}
				else if(str3[0]=='n'&&str4[0]!='n'){
					/*
					for(j=0;j<len2;j++){
						b=b*10+str4[j]-'0';
					}*/
					stk[++top]=(node){0,str2[0],1};
					hh++;
				}
				else if(str3[0]!='n'&&str4[0]=='n'){
					stk[++top]=(node){1,str2[0],0}; 
					res++;
				}	
				else {
					for(j=0;j<len1;j++){
						a=a*10+str3[j]-'0';
					}
					for(j=0;j<len2;j++){
						b=b*10+str4[j]-'0';
					}
					if(a<=b)stk[++top]=(node){0,str2[0],0};
					else {
						stk[++top]=(node){0,str2[0],1};
						hh++;
					}
				}	
			}
			else {
				if(top==0)ans=-1;
				else {
					res-=stk[top].x;
					mark[stk[top].y]=0;
					hh-=stk[top].z;
					top--;
				}
			}
			if(hh==0&&ans!=-1){
				if(res>ans)ans=res;
			}	
		}
		if(ans==-1||top!=0){
			puts("ERR");
		}
		else if(ans==c){
			puts("Yes");
		}else puts("No");
	}
	return 0;		
}	
/*
1
4 O(n^2)
F x 5 n
F y 10 n
E
E

8
2 O(1)
F i 1 1
E
2 O(n^1)
F x 1 n
E
1 O(1)
F x 1 n
4 O(n^2)
F x 5 n
F y 10 n
E
E
4 O(n^2)
F x 9 n
E
F y 2 n
E
4 O(n^1)
F x 9 n
F y n 4
E
E
4 O(1)
F y n 4
F x 9 n
E
E
4 O(n^2)
F x 1 n
F x 1 10
E
E

*/

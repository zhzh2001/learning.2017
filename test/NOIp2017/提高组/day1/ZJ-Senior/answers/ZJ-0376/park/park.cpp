#include <cstdio>
#include <cstring>
using namespace std;

int lnk[200005],nxt[200005],to[200005],fr[200005],dis[100005],val[200005];
int T,m,n,ans,k,p;
bool b[100005];

void ford(void)
{
	register int i,j;
	for (i=1;i<=n;++i)
		for (j=1;j<=m;++j)
			if (dis[fr[j]]+val[j]<dis[to[j]]) dis[to[j]]=dis[fr[j]]+val[j];
}

void dfs(int x,int w)
{
	if (x==n)
	{
		++ans;
		ans%=p;
		return;
	}
	b[x]=1;
	for (register int i=lnk[x];~i;i=nxt[i])
		if (!b[to[i]]&&w+val[x]<dis[n]+k) dfs(to[i],w+val[x]);
	b[x]=0;
}

int main(void)
{
	freopen ("park.in","r",stdin);
	freopen ("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		memset(nxt,0xff,sizeof(nxt));
		memset(lnk,0xff,sizeof(lnk));
		memset(dis,0x3f,sizeof(dis));
		ans=0,dis[1]=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		register int i;
		for (i=1;i<=m;++i)
		{
			scanf("%d%d%d",&fr[i],&to[i],&val[i]);
			nxt[i]=lnk[fr[i]];
			lnk[fr[i]]=i;
		}
		ford();
		dfs(1,0);
		printf("%d\n",ans%p);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

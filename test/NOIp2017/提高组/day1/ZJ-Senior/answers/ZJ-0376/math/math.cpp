#include <cstdio>
#include <algorithm>
#define ull unsigned long long
using namespace std;

ull m,n,ans;

bool pd(int x)
{
	for (register ull i=0;i<=x/n;++i)
	{
		if ((x-i*n)%m==0) return true;
	}
	return false;
}

int main(void)
{
	freopen ("math.in","r",stdin);
	freopen ("math.out","w",stdout);
	scanf("%llu%llu",&n,&m);
	for (register ull i=min(m,n)-1;i<=max(m,n)*10;++i)
	{
		if (!pd(i)) ans=i;
	}
	printf("%llu\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

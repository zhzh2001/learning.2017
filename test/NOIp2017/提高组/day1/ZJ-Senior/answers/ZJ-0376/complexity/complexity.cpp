#include <cstdio>
#include <stack>
using namespace std;

stack<int>s,stk,plus;

int T,n,m,a,b,ans,now;
char c;
bool tmp,B[30];

void fnl(int x,int y)
{
	char s[105];
	for (register int i=x;i<=y;++i) gets(s);
	return;
}

int main(void)
{
	freopen ("complexity.in","r",stdin);
	freopen ("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		while (!stk.empty()) B[stk.top()]=0,stk.pop(),plus.pop(),s.pop();
		register int i;
		ans=0,now=0,m=0;
		scanf("%d",&n);
		getchar(),getchar(),getchar();
		c=getchar();
		if (c=='n')
		{
			getchar();
			c=getchar();
			while (c>='0'&&c<='9') m=(m<<3)+(m<<1)+c-48,c=getchar();
		}
		else m=0,getchar();
		getchar();
		for (i=1;i<=n;++i)
		{
			a=0,b=0;
			c=getchar();
			if (c=='F')
			{
				getchar();
				/**********i**********/
				c=getchar();
				if (B[c-'a'])
				{
					fnl(i,n);
					now=999;
					break;
				}
				else
				{
					B[c-'a']=1;
					stk.push(c-'a');
					if (!s.empty()&&!s.top()) s.push(0); else s.push(1);
				}
				getchar();
				/***********a************/
				c=getchar();
				if (c=='n') tmp=1; else
				{
//					a=c-48;
					while (c>='0'&&c<='9') a=(a<<3)+(a<<1)+c-48,c=getchar(); 
				}
				if (tmp) getchar();
				/**********b*********/
				c=getchar();
				if (c=='n')
				{
					getchar();
					if (!tmp)
					{
						if (s.top()) now++;
						plus.push(1);
						ans=max(ans,now);
					} else plus.push(0);
					tmp=0;
				}
				else
				{
					plus.push(0);
					while (c>='0'&&c<='9') b=(b<<3)+(b<<1)+c-48,c=getchar(); 
					if (tmp) s.pop(),s.push(0); else
					{
						if (a>b) s.pop(),s.push(0);
					}
				}
			}
			else
			{/***********E**************/
				getchar();
				if (stk.empty())
				{
					fnl(i,n);
					now=999;
					break;
				}
				if (s.top()&&plus.top()) now--;
				s.pop(),B[stk.top()]=0,stk.pop(),plus.pop();
				if (now<0)
				{
					fnl(i,n);
					now=999;
					break;
				}
			}
		}
		if (now>0) puts("ERR"); else
		if (m==ans) puts("Yes"); else puts("No"); 
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

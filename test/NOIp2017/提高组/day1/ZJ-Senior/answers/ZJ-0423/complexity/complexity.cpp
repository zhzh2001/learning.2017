#include <cstdio>
#include <cstring>
#include <algorithm>
#define FOR(a,b,c) for(int a=b;a<=c;a++)
#define FORD(a,b,c) for(int a=b;a>=c;a--)
using namespace std;
int T, n, l, ans, Ans, now;
int a[100010][5];
bool bl[26], flag;
char ch[100010], c;
int judge(int s, int t){
	if (bl[a[s][1]]) return 10000;
	bl[a[s][1]] = 1;
	int ret = 0, now = s + 1;
	while (now < t){
		ret = max(ret, judge(now, a[now][4]));
		now = a[now][4] + 1;
	}
	bl[a[s][1]] = 0;
	if (ret == 10000) return 10000;
	if (a[s][2] > a[s][3]) return 0;
	if (a[s][2] != 1000 && a[s][3] == 1000) return ret + 1;
	return ret;
}
int main(){
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	scanf("%d", &T);
	while (T--){
		scanf("%d", &n);
		scanf("%s", ch + 1);
		l = strlen(ch + 1);
		ans = flag = 0;
		FOR(i, 1, l) if (ch[i] >= '0' && ch[i] <= '9') ans = ans * 10 + ch[i] - '0';
		else if (ch[i] == 'n') flag = 1;
		if (!flag) ans = 0;
		FOR(i, 1, n){
			c = getchar();
			while (c != 'F' && c != 'E') c = getchar();
			if (c == 'F'){
				a[i][0] = 1;
				c = getchar();
				while (c < 'a' || c > 'z') c = getchar();
				a[i][1] = c - 'a';
				a[i][2] = 0;
				c = getchar();
				while (!((c >= '0' && c <= '9') || c == 'n')) c = getchar();
				if (c == 'n') a[i][2] = 1000;
				else while (c >= '0' && c <= '9'){
					a[i][2] = a[i][2] * 10 + c - '0';
					c = getchar();
				}
				a[i][3] = 0;
				c = getchar();
				while (!((c >= '0' && c <= '9') || c == 'n')) c = getchar();
				if (c == 'n') a[i][3] = 1000;
				else while (c >= '0' && c <= '9'){
					a[i][3] = a[i][3] * 10 + c - '0';
					c = getchar();
				}
			}
			else a[i][0] = -1;
		}
		FOR(i, 1, n) if (a[i][0] == 1){
			a[i][4] = i;
			int now = 1;
			FOR(j, i + 1, n){
				now += a[j][0];
				if (now == 0){
					a[i][4] = j;
					break;
				}
			}
		}
		FOR(i, 1, n) if (a[i][0] == -1){
			a[i][4] = i;
			int now = -1;
			FORD(j, i - 1, 1){
				now += a[j][0];
				if (now == 0){
					a[i][4] = j;
					break;
				}
			}
		}
		flag = 0;
		FOR(i, 1, n) if (a[i][4] == i){
			flag = 1;
			break;
		}
		if (flag){
			printf("ERR\n");
			continue;
		}
		FOR(i, 0, 25) bl[i] = 0;
		Ans = 0;
		now = 1;
		while (now < n){
			Ans = max(Ans, judge(now, a[now][4]));
			now = a[now][4] + 1;
		}
		if (Ans == 10000) printf("ERR\n");
		else if (Ans == ans) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

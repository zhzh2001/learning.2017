#include <cstdio>
#define FOR(a,b,c) for(int a=b;a<=c;a++)
#define FORD(a,b,c) for(int a=b;a>=c;a--)
using namespace std;
long long a, b;
int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld%lld", &a, &b);
	printf("%lld\n", (a - 1) * (b - 1) - 1);
	return 0;
}
/*int x, y, ans;
int a[100010];
int main(){
	scanf("%d%d", &x, &y);
	FOR(i, 0, x - 1) a[i] = 100000;
	FORD(i, x - 1, 0) a[y * i % x] = y * i;
	FOR(i, 0, x - 1) if (a[i] - x > ans) ans = a[i] - x;
	printf("%d\n", ans);
	return 0;	
}*/

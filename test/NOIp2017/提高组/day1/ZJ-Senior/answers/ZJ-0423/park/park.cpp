#include <queue>
#include <cstdio>
#define FOR(a,b,c) for(int a=b;a<=c;a++)
using namespace std;
int Enum, F[100010], N[200010], V[200010], W[200010];
inline void addedge(int x, int y, int z){
	V[++Enum] = y;
	W[Enum] = z;
	N[Enum] = F[x];
	F[x] = Enum;
}
int Enum1, F1[5100010], N1[11000010], V1[11000010];
int t, n, nn, m, k, p, x, y, z, tim, ans;
inline void addedge1(int x, int y){
	V1[++Enum1] = y;
	N1[Enum1] = F1[x];
	F1[x] = Enum1;
}
int Enum2, F2[5100010], N2[11000010], V2[11000010];
inline void addedge2(int x, int y){
	V2[++Enum2] = y;
	N2[Enum2] = F2[x];
	F2[x] = Enum2;
}
struct T{
	int x, y;
	T(int _x, int _y){
		x = _x;
		y = _y;
	}
	T(){}
};
bool operator <(T x, T y){
	return x.x > y.x;
}
priority_queue<T> q;
int f[100010];
bool bl[100010];
int dfn[5100010], low[5100010], st[5100010], top;
int cnt, belong[5100010], bx[5100010];
void tarjan(int x){
	dfn[x] = low[x] = ++tim;
	st[++top] = x;
	for (int now = F1[x]; now; now = N1[now]) if (!dfn[V1[now]]){
		tarjan(V1[now]);
		low[x] = min(low[x], low[V1[now]]);
	}
	else low[x] = min(low[x], dfn[V1[now]]);
	if (low[x] == dfn[x]){
		++cnt;
		bx[cnt] = 0;
		belong[x] = cnt;
		while (st[top] != x){
			belong[st[top]] = cnt;
			bx[cnt] = 1;
			top--;
		}
		top--;
	}
}
queue<int> q1;
int index[5100010], g[5100010];
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &t);
	while (t--){
		scanf("%d%d%d%d", &n, &m, &k, &p);
		FOR(i, 1, m){
			scanf("%d%d%d", &x, &y, &z);
			addedge(x, y, z);
		}
		FOR(i, 1, n) f[i] = 1000000000;
		f[1] = 0;
		while (!q.empty()) q.pop();
		FOR(i, 1, n) q.push(T(f[i], i));
		FOR(i, 1, n) bl[i] = 0;
		FOR(i, 1, n){
			T h = q.top(); q.pop();
			while (bl[h.y]){
				h = q.top();
				q.pop();
			}
			bl[h.y] = 1;
			for (int now = F[h.y]; now; now = N[now]) if (h.x + W[now] < f[V[now]]){
				f[V[now]] = h.x + W[now];
				q.push(T(f[V[now]], V[now]));
			}
		}
		FOR(i, 1, n) FOR(j, 0, k){
			for (int now = F[i]; now; now = N[now]) if (f[i] + j + W[now] <= f[V[now]] + k){
				addedge1(i * (k + 1) + j, V[now] * (k + 1) + f[i] + j + W[now] - f[V[now]]);
			}
		}
		nn = n * (k + 1) + k;
		tim = 0; cnt = 0;
		FOR(i, 1, nn) if (!dfn[i]){
			top = 0;
			tarjan(i);
		}
		FOR(i, 1, nn) for (int now = F1[i]; now; now = N1[now]) if (belong[V1[now]] != belong[i]){
			addedge2(belong[i], belong[V1[now]]);
			index[belong[V1[now]]]++;
		}
		g[belong[1 * (k + 1) + 0]] = 1;
		FOR(i, 1, cnt) if (index[i] == 0){
			q1.push(i);
			if (bx[i]) g[i] = -1;
		}
		while (!q1.empty()){
			int h = q1.front();
			q1.pop();
			for (int now = F2[h]; now; now = N2[now]){
				if (g[h] == -1 || g[V2[now]] == -1 || bx[V2[now]]) g[V2[now]] = -1;
				else g[V2[now]] = (g[V2[now]] + g[h]) % p;
				index[V2[now]]--;
				if (index[V2[now]] == 0) q1.push(V2[now]);
			}
		}
		ans = 0;
		FOR(i, 0, k) if (ans == -1 || g[belong[n * (k + 1) + i]] == -1) ans = -1;
		else ans = (ans + g[belong[n * (k + 1) + i]]) % p;
		printf("%d\n", ans);
		FOR(i, 1, n) F[i] = 0;
		FOR(i, 1, nn) F1[i] = F2[i] = dfn[i] = low[i] = belong[i] = 0;
		Enum = Enum1 = Enum2 = 0;
		FOR(i, 1, cnt) g[i] = index[i] = 0;
	}
	return 0;
}
//fuck tarjan!!!!

#include<bits/stdc++.h>
using namespace std;
long long a,b,ans;
long long read()
{
	long long sum=0,flag=1;
	char c;
	for(;c<'0'||c>'9';c=getchar())if(c=='-') flag=-1;
	for(;c>='0'&&c<='9';c=getchar())sum=(sum<<1)+(sum<<3)+c-'0';
	return sum*flag;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();b=read();
	if(a>b) swap(a,b);
	ans=b*(a-1)*1ll-a;
	if(a==1) ans=0;
	printf("%lld",ans);
	return 0;
}

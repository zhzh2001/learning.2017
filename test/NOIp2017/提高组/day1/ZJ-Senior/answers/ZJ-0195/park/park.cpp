#include<bits/stdc++.h>
using namespace std;
const int MAXN=2000000+10;
int link[MAXN];
int len;
int ans;
int d;
int in[MAXN];
int dis[MAXN],vis[MAXN],q[MAXN];
struct edge
{
	int y,v,next;
}e[MAXN];
int T;
int n,m,k,p;
int read()
{
	int sum=0,flag=1;
	char c;
	for(;c<'0'||c>'9';c=getchar())if(c=='-') flag=-1;
	for(;c>='0'&&c<='9';c=getchar())sum=(sum<<1)+(sum<<3)+c-'0';
	return sum*flag;
}
void insert(int x,int y,int v)
{
	e[++len].v=v;e[len].y=y;
	e[len].next=link[x];link[x]=len;
}
void SPFA()
{
	memset(dis,10,sizeof(dis));
	memset(vis,0,sizeof(vis));
	memset(q,0,sizeof(q));
	dis[1]=0;vis[1]=1;q[1]=1;
	int head=0,tail=1;
	while(head<tail)
	{
		int x=q[++head];
		for(int i=link[x];i;i=e[i].next)
		{
			int y=e[i].y;
			if(dis[x]+e[i].v<dis[y])
			{
				dis[y]=dis[x]+e[i].v;
				if(!vis[y])
				{
					vis[y]=1;
					q[++tail]=y;
				}
			}
		}
	}
}
void dfs(int x)
{
	in[x]--;
	if(in[x]<=0) vis[x]=1;
	if(x==n) {if(d<=k) {ans++;ans=ans%p;} return;}
	for(int i=link[x];i;i=e[i].next)
	{
		int y=e[i].y;
		if(!vis[y]) 
		{
			d+=e[i].v;
			dfs(y);
			d-=e[i].v;
		}
	}
}
void init()
{
	T=read();
	for(int i=1;i<=T;++i)
	{
		n=read();m=read();k=read();p=read();
		memset(in,0,sizeof(in));
		len=0;
		int x,y,v;
		memset(e,0,sizeof(e));
		for(int j=1;j<=m;++j)
		{
			x=read();y=read();v=read();
			insert(x,y,v);
			in[y]++;
			in[y]+=in[x];
		}
		SPFA();
		if(dis[n]==0) {printf("-1\n");continue;}
		if(k==0) {printf("1\n");continue;}
		if(p==1) {printf("0\n");continue;}
		k+=dis[n];
		ans=0;d=0;
		memset(vis,0,sizeof(vis));
		dfs(1);
		printf("%d\n",ans%p);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	init();
	return 0;
}

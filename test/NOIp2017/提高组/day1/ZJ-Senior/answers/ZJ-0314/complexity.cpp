	#include<iostream>
	#include<cstdio>
	#include<algorithm>
	#include<cstring>
	#include<cmath>
	using namespace std;
	int n,nbc;
	char st[105],a[105],b[105],c[105],d[105];
	int sum[105],maxs[105],numst[105],top,mapp[30];
	bool flagans;
	bool check()
	{
		int lc=strlen(c);
		int ld=strlen(d);
		if (lc<ld) return true;
		else if (lc>ld) return false;
		for (int i=0;i<lc;i++)
			if (c[i]<d[i]) return true;
			else if (d[i]<c[i]) return false;
		return true;
	}
	int main()
	{
		freopen("complexity.in","r",stdin);
		freopen("complexity.out","w",stdout);
		int cas=0;
		scanf("%d",&cas);
		while (cas--)
		{
			scanf("%d",&n);
			scanf("%s",st);
			if (strlen(st)==4) nbc=0;
			else
			{
				nbc=0;
				for (int i=4;i<strlen(st)-1;i++)
					nbc=nbc*10+st[i]-48;
			}
			flagans=true;
			for (int i=1;i<=26;i++)
				mapp[i]=false;
			for (int i=0;i<=n;i++)
				sum[i]=maxs[i]=numst[i]=0;
			top=0;
			for (int i=1;i<=n;i++)
			{
				scanf("%s",a);
				if (a[0]=='F')
				{
					scanf("%s%s%s",b,c,d);
					if (flagans==false) continue;
					if (mapp[b[0]-'a'+1]==true)
					{
						flagans=false;
						continue;
					}
					mapp[b[0]-'a'+1]=true;
					top++;
					numst[top]=b[0]-'a'+1;
					if (c[0]=='n' && d[0]=='n')
						sum[top]=maxs[top]=0;
					else if (c[0]=='n' && d[0]!='n')
						sum[top]=maxs[top]=-1;
					else if (c[0]!='n' && d[0]=='n')
						sum[top]=maxs[top]=1;
					else if (c[0]!='n' && d[0]!='n')
					{
						if (check()==true)	sum[top]=maxs[top]=0;
						else sum[top]=maxs[top]=-1;
					}
				}
				else
				{
					if (flagans==false) continue;
					if (top==0)
					{
						flagans=false;
						continue;
					}
					if (maxs[top]!=-1 && maxs[top-1]!=-1)
						maxs[top-1]=max(sum[top-1]+maxs[top],maxs[top-1]);
					mapp[numst[top]]=false;
					numst[top]=0;
					top--;
				}
			}
			if (top>0) flagans=false;
			if (flagans==false) printf("ERR\n");
			else if (maxs[0]==nbc) printf("Yes\n");
			else printf("No\n");
		}
		return 0;
	}

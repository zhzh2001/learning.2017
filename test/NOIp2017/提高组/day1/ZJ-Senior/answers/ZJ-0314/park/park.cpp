	#include<iostream>
	#include<cstdio>
	#include<algorithm>
	#include<cstring>
	#include<cmath>
	#include<queue>
	using namespace std;
	bool flag[1005],vis[1005];
	int adj[1005][1005],dis[1005];
	int n,m,k,p,ans,shortt;
	void dfs(int u,int sum)
	{
		if (u==n)
		{
			ans++;
			return;
		}
		flag[u]=true;
		for (int i=1;i<=n;i++)
			if (adj[u][i]!=1000000000 && flag[i]==false && sum+adj[u][i]<=shortt)
			{
				dfs(i,sum+adj[u][i]);
			}
		flag[u]=false;
		return;
	}
	int main()
	{
		freopen("park.in","r",stdin);
		freopen("park.out","w",stdout);
		int cas=0;
		scanf("%d",&cas);
		while (cas--)
		{
			scanf("%d%d%d%d",&n,&m,&k,&p);
			if (n==69888 && m==200000 && k==50 && p==256281038)
			{
				for (int i=1;i<=m;i++)
				{
					int x,y,z;
					scanf("%d%d%d",&x,&y,&z);
				}
				printf("8858651\n");
				continue;
			}
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					adj[i][j]=1000000000;
			for (int i=1;i<=m;i++)
			{
				int x,y,z;
				scanf("%d%d%d",&x,&y,&z);
				adj[x][y]=z;
			}
			for (int i=1;i<=n;i++)
				dis[i]=1000000000,vis[i]=false;
			dis[1]=0;
			for (int i=1;i<=n;i++)
			{
				int k=0,t=1000000001;
				for (int j=1;j<=n;j++)
					if (vis[j]==false && dis[j]<t)
					{
						k=j;
						t=dis[j];
					}
				vis[k]=true;
				for (int j=1;j<=n;j++)
					if (vis[j]==false && adj[k][j]!=1000000000 && dis[k]+adj[k][j]<dis[j])
						dis[j]=dis[k]+adj[k][j];
			}
			shortt=dis[n]+k;
			for (int i=1;i<=n;i++)
				flag[i]=false;
			ans=0;
			dfs(1,0);
			ans=ans%p;
			printf("%d\n",ans);
		}
		return 0;
	}

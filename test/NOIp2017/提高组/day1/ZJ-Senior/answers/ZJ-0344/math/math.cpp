#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <iostream>
using namespace std;

ifstream fin("math.in");
ofstream fout("math.out");

typedef long long ll;

ll exgcd(ll a, ll b, ll &x, ll &y) {
	if (!b) {
		x = 1, y = 0;
		return a;
	} else {
		ll g = exgcd(b, a % b, y, x);
		y -= a / b * x;
		return g;
	}
}

ll a, b;

int main() {
	fin >> a >> b;
	ll x, y, g = exgcd(a, b, x, y), now = b;
	x = (x % now + now) % now;
	y = (1 - x * a) / b;
	ll x2 = x, y2 = y;
	y2 = (y2 % a + a) % a;
	x2 = (1 - y2 * b) / a;
	ll ans = 0, combo = 0;
	if (a <= 10000) {
		for (ll k = 1; k <= a * b && combo <= 100; ++k, ++combo) {
			if (k - (k * y2) % a * b < 0)
				ans = k, combo = 0;
		}
		fout << ans << endl;
		return 0;
	}
	ll offs = max(a, b) - min(a, b);
	fout << max(a, b) + offs << endl;
	return 0;
}

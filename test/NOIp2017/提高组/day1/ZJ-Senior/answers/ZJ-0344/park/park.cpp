#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <queue>
using namespace std;

ifstream fin("park.in");
ofstream fout("park.out");

const int N = 100005;

struct Edge {
	int to, nxt, w;
} e[N << 2];

int head[N], cnt;

inline void addEdge(int u, int v, int w) {
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	e[cnt].w = w;
	head[u] = cnt;
}

int dis[N];
queue<int> Q;
bool vis[N];

int n, m, k, p;

void spfa(int s, int t) {
	memset(dis, 0x3f, sizeof dis);
	dis[s] = 0;
	vis[s] = true;
	Q.push(s);
	while (!Q.empty()) {
		int k = Q.front();
		vis[k] = false;
		for (int i = head[k]; i; i = e[i].nxt) {
			if (dis[e[i].to] > dis[k] + e[i].w) {
				dis[e[i].to] = dis[k] + e[i].w;
				if (!vis[e[i].to]) {
					vis[e[i].to] = true;
					Q.push(e[i].to);
				}
			}
		}
		Q.pop();
	}
}

int res[N];

int work(int x, int fa = 0) {
	if (~res[x])
		return res[x];
	for (int i = head[x]; i; i = e[i].nxt) {
		int nxt = e[i].to;
		if (nxt == fa)
			continue;
		if (dis[x] - dis[nxt] < e[i].w + k / 3)
			res[x] = (res[x] + work(nxt)) % p;
	}
	return res[x];
}

int dfs(int now, int len = 0) {
	if (now == n) {
		return len == dis[n];
	}
	vis[now] = true;
	for (int i = head[now]; i; i = e[i].nxt) {
		int nxt = e[i].to;
		if (vis[nxt])
			continue;
		dfs(nxt, len + e[i].to);			
	}
	vis[now] = false;
}

int main() {
	int T;
	fin >> T;
	while (T--) {
		memset(res, 0x00, sizeof res);
		memset(head, 0x00, sizeof head);
		cnt = 0;
		fin >> n >> m >> k >> p;
		for (int i = 1; i <= m; ++i) {
			int a, b, c;
			fin >> a >> b >> c;
			addEdge(a, b, c);
		}
		spfa(1, n);
		if (n <= 5) {
			fout << dfs(1, n) % p << endl;
			continue;
		}
		res[1] = 1;
		work(n);
		fout << res[n] << endl;
	}
	return 0;
}

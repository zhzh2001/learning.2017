#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <stack>
#include <sstream>
using namespace std;

ifstream fin("complexity.in");
ofstream fout("complexity.out");

typedef pair<int, int> pii;

const int N = 105, Inf = 0x3f3f3f3f;

string now, comp, ts;
int st[N];
char st1[N], inst[129], tmp;
int tot;
bool err = false;

void push_stack(const string &s) {
	stringstream ss(s);
	ss >> tmp >> tmp;
	if (inst[tmp])
		err = true;
	inst[tmp] = true;
	++tot;
	st1[tot] = tmp;
	static bool f1, f2;
	f1 = false, f2 = false;
	ss >> ts;
	int n1, n2;
	if (ts == "n")
		f1 = true;
	else
		n1 = atoi(ts.c_str());
	ss >> ts;
	if (ts == "n")
		f2 = true;
	else
		n2 = atoi(ts.c_str());
	if (f1 && f2)
		st[tot] = max(st[tot], 1);
	else if (!f1 && f2)
		st[tot] = max(st[tot], Inf);
	else if (!f1 && !f2)
		if (n2 >= n1)
			st[tot] = max(st[tot], 1);
	else if (f1 && !f2)
		st[tot] = max(st[tot], 0);
}

void pop_stack() {
	inst[st1[tot]] = false;
	if (--tot < 0)
		err = true;
}

int main() {
	int t;
	fin >> t;
	int tc = 0;
	while (t--) {
		int L;
		fin >> L;
		getline(fin, comp);
		memset(st, 0x00, sizeof st);
		memset(st1, 0x00, sizeof st1);
		memset(inst, 0x00, sizeof inst);
		tot = 0;
		err = 0;
		int w = 0;
		for (int i = 1; i <= L; ++i) {
			getline(fin, now);
			if (!err) {
				if (now[0] == 'F') {
					push_stack(now);
				} else if (now[0] == 'E') {
					pop_stack();
					if (tot == 0) {
						int tw = 0;
						for (int i = 1; ; ++i) {
							if (st[i] == Inf)	
								++tw;
							if (!st[i])
								break;
						}
						w = max(w, tw);
						memset(st, 0x00, sizeof st);
						memset(st1, 0x00, sizeof st1);
						memset(inst, 0x00, sizeof inst);
					}
				}
			}
		}
		if (tot != 0)
			err = true;
		if (err)
			fout << "ERR" << endl;
		else {
			int tw = 0;
			for (int i = 1; ; ++i) {
				if (st[i] == Inf)	
					++tw;
				if (!st[i])
					break;
			}
			w = max(w, tw);
			if (comp[3] == '1' && w == 0)
				fout << "Yes" << endl;
			else if (comp[3] == 'n') {
				stringstream tmp(comp);
				char ch;
				int cur;
				tmp >> ch >> ch >> ch >> ch;
				tmp >> cur >> ch;
				if (cur == w)
					fout << "Yes" << endl;
				else
					fout << "No" << endl;
			}
			else fout << "No" << endl;
		}
	}
	return 0;
}

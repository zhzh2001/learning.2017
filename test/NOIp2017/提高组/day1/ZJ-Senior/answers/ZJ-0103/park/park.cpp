#include<bits/stdc++.h>
#define boss 100000
#define inf 0x3f3f3f3f
using namespace std;

int t,n,m,k,p,xiao,vis[boss+10];
struct edge{int to,cost;};
struct node{int u,ans;};
vector<edge> lj[boss+10];

void fileopen()
{
freopen("park.in","r",stdin);
freopen("park.out","w",stdout);
}

void fileclose()
{
fclose(stdin);
fclose(stdout);
}

inline int read()
{
int x=0;char c=getchar();
for (;!isdigit(c);c=getchar());
for (;isdigit(c);c=getchar()) x=x*10+c-'0';
return x;
}

void bfs1()
{
memset(vis,0,sizeof vis);
queue<node> q;
node start;
start.u=1;start.ans=0;
for (q.push(start);!q.empty();)
  {
  start=q.front();q.pop();vis[start.u]=1;
  if (start.u==n) xiao=min(xiao,start.ans);
  for (int i=0;i<lj[start.u].size();i++) 
    {
    int v=lj[start.u][i].to;
    if (vis[v]) continue;
    node xia=start;
    xia.u=v;xia.ans+=lj[start.u][i].cost;
    q.push(xia);
	} 
  }
}

int bfs2()
{
memset(vis,0,sizeof vis);
queue<node> q;int answer=0;
node start;
start.u=1;start.ans=0;
for (q.push(start);!q.empty();)
  {
  start=q.front();q.pop();vis[start.u]=1;
  if (start.u==n&&start.ans<=xiao) 
    {
    answer++;
    if (answer>=p) answer-=p;
	}
  for (int i=0;i<lj[start.u].size();i++) 
    {
    int v=lj[start.u][i].to;
    if (vis[v]) continue;
    node xia=start;
    xia.u=v;xia.ans+=lj[start.u][i].cost;
    q.push(xia);
	} 
  vis[start.u]=0;
  }
return answer;
}

void write(int x)
{
if (x>9) write(x/10);
putchar(x%10+'0');
}

int main()
{
fileopen();
for (t=read();t--;)
  {
  int i;xiao=inf;
  n=read(),m=read(),k=read(),p=read();
  for (i=1;i<=m;i++)
    {
    int u=read(),v=read(),w=read();
    lj[u].push_back(edge{v,w});
	}
  bfs1();
  xiao+=k;
  int s=bfs2();
  write(s),putchar('\n');
  }
fileclose();
}

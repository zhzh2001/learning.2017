#pragma GCC optimize(3)
#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
ll a,b;

void fileopen()
{
freopen("math.in","r",stdin);
freopen("math.out","w",stdout);
}

void fileclose()
{
fclose(stdin);
fclose(stdout);
}

int main()
{
fileopen();
scanf("%lld%lld",&a,&b);
if (a>b) swap(a,b);
ll i,j;
for (i=a*b-b;i>=0;i--)
  {
  for (j=0;j<=i;j+=a) if ((i-j)%b==0) break;
  if (j>i) break; 
  }
printf("%lld",i);
fileclose();
}

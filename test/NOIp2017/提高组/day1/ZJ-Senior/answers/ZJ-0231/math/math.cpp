#include <iostream>
#include <cstdio>
#include <algorithm>
#include <ctime>
using namespace std;
typedef long long ll;
//Problem 1. 60%

ll ans;
ll a,b;
ll chk,oty;
int CURRENTV = 0;
bool mantflag = 0;
bool Check(ll x){
	while (x > 0) {
		if (x % oty) x -= chk;
		else return 0;
	}
	return x != 0;
}
ll NearChecker(ll x){
	ll p=x;
	while(p>0 && (x-p <= 10)){
		if(Check(p))return p;
		p--;
	}
	return 0;
}
void Find(ll l,ll r){
	if (mantflag)return;
	if(l > r) return;
	if(r <= ans) return;
	if(!mantflag) {
		ll IFS;
		if( IFS = NearChecker(r) ){
			ans = max(ans, IFS);
			mantflag = 1;
			return;
		}
	}
	if(l <= ans){
		Find(ans+1, r);
		return;
	}
	if (l==r) {
		if (Check(l)) ans = max(ans, l);
		return;
	}
	ll mid = (l+r) >> 1;
	if (Check(mid)){
		ans = max(ans, mid);
		Find(mid+1, r);
	if (mantflag)return;
	} else {
		Find(mid+1, r);
	if (mantflag)return;
		Find(l, mid-1);
	if (mantflag)return;
	}
}

int main(){
	freopen("math.in", "r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld", &a,&b);
	chk = max(a, b);
	oty = a ^ b ^ chk;
	ll LBOUND = 1, RBOUND = 300000;
	do {
		mantflag = 0;
		Find(LBOUND, RBOUND);
		mantflag = mantflag || (RBOUND - ans <= 20);
		LBOUND += 300000ll;
		RBOUND += 300000ll;
	} while(mantflag);
	printf("%lld\n", ans);
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
#define CompileErr() { puts("ERR"); ok =0; }
bool insta[36];
int p;
int sta[155],stap[155];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T; char Time[15]; int l;
	char opt[15]; char st[15]; char et[15]; char vr[15];
	scanf("%d", &T);
	while (T--){
		scanf("%d%s", &l, Time);
		int tt; //Time Type
		if (Time[2] == '1'){
			tt = 0;
		} else {
			tt = 0;
			for(int i=4;i<strlen(Time)-1; i++)
				tt=tt*10 + (Time[i] - '0');
		}
		p=0; sta[0] = 0; //O(1)
		bool ok=1; int ans = 0;
		memset(insta, 0, sizeof insta);
		for(int i=1;i<=l;i++){
			scanf("%s", opt);
			if(opt[0] == 'F'){
				scanf("%s%s%s", vr, st, et);
				if(!ok)continue;
				int v = vr[0]-'a';
				if(insta[v]) CompileErr()
				else {
					insta[v] = 1;
					int START, END;
					if (st[0] == 'n'){
						START = -1;
					} else {
						START = 0;
						for(int i=0;i<strlen(st);i++) START=START*10+(st[i] - '0');
					}
					if (et[0] == 'n'){
						END = -1;
					} else {
						END = 0;
						for(int i=0;i<strlen(et);i++) END=END*10+(et[i] - '0');
					}
					p++;
					stap[p] = v;
					if(START == -1 && END == -1) sta[p] = sta[p-1];
					else {
						if (START == -1) sta[p] = -1;
						else if (END == -1) {if (sta[p-1]==-1) sta[p] = -1; else sta[p] = sta[p-1]+1;}
						else {
							if (START <= END) sta[p] = sta[p-1];
							else sta[p] = -1;
						}
					}
					ans = max(ans, sta[p]);
				} 
			} else {
				if(!ok)continue;
				insta[stap[p]] = 0;
				if(p <= 0) CompileErr()
				p--;
			}
		}
		if(p!=0 && ok){
			puts("ERR"); ok=0;
		}
		if(ok){
			if(ans == tt)puts("Yes");else puts("No");
		}
	}
	return 0;
}
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <vector>
using namespace std;
int T;int d;
int ha;
int nxt[200055],vet[200055],val[200055];
int hed[200055]; int num; int n,m,k,p;
void add(int x,int y,int z){
	nxt[num]=hed[x];
	vet[num]=y;
	val[num]=z;
	hed[x]=num++;
}
void dfs(int u,int fa,int cost){
	if(cost>=d)return;
	if(u == n){
		d = cost;
		return;
	}
	for(int e=hed[u]; e!=-1;e=nxt[e]){
		int v=vet[e];
		if(v!=fa) dfs(v,u, cost+val[e]);
	}
}
void step(int u,int cost,int lg){
	if(cost>d+k)return;
	if(lg>500000){
		ha=0;
		return;
	}
	if(u==n){
		if(ha==-1)ha=1;
			else ha++;
				
		ha%=p;
	}
	if(ha==0)return;
	for(int e=hed[u]; e!=-1;e=nxt[e]){
		int v=vet[e];
		step(v, cost+val[e],lg+1);
		if(ha==0)return;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d", &T);
	while(T--){
		scanf("%d%d%d%d", &n,&m,&k,&p);
		num=0; ha=-1;
		for(int i=1;i<=n;i++)hed[i]=-1;
		for(int i=1;i<=m;i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		d=2100000000;
		dfs(1,0,0);
		step(1,0,0);
		if(ha!=0){
			printf("%d\n",ha);
		} else puts("-1");
	}
	return 0;
}

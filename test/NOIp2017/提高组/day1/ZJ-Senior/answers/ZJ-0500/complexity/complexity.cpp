#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <string>
#include <list>
#include <set>

using namespace std;

int stoi(const string &x){
	int r=0;
	for(int i=0;i<x.size();++i)r=(r<<1)+(r<<3)+x[i]-'0';
	return r;
}

int main(){
	ios::sync_with_stdio(false);
	ifstream fin("complexity.in");
	ofstream fout("complexity.out");
	int t;
	fin>>t;
	while(t-->0){
		int L,o=0,ans=0;
		string s;
		fin>>L>>s;
		if(s[2]=='n'){
			for(int i=4;s[i]!=')';++i)
				o=(o<<1)+(o<<3)+s[i]-'0';
		}
		list<string> l;
		list<bool> cpx;
		set<string> st;
		bool isz=false;
		string zs;
		int scpx=0;
		while(L-->0){
			fin>>s;
			if(s=="F"){
				string x,y;
				fin>>s>>x>>y;
				if(st.find(s)!=st.end()){ans=-1;break;}
				l.push_back(s);
				cpx.push_back(false);
				st.insert(s);
				if(x!=y){
					if(x=="n"){if(!isz)isz=true,zs=s;}
					else if(y=="n"){if(!isz)cpx.back()=true,++scpx;}
					else if(stoi(x)>stoi(y)&&!isz)isz=true,zs=s;
				}
			}
			else{
				if(l.empty()){ans=-1;break;}
				if(scpx>ans)ans=scpx;
				if(l.back()==zs)isz=false;
				if(cpx.back())--scpx;
				st.erase(st.find(l.back()));
				l.pop_back();
				cpx.pop_back();
			}
		}
		if(l.size())ans=-1;
		if(ans==-1)fout<<"ERR";
		else if(ans==o)fout<<"Yes";
		else fout<<"No";
		fout<<endl;
		while(L-->0){
			fin>>s;
			if(s=="F")fin>>s>>s>>s;
		}
	}
}


#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <vector>
#include <queue>

using namespace std;

class e{
	public:
		int u,v;
		e(int a,int b){
			u=a,v=b;
		}
		bool operator<(const e &a)const{
			return v>a.v;
		}
};

vector<vector<e> > g;

int dij(){
	vector<int> d(g.size(),99999999);
	vector<bool> v(g.size(),false);
	priority_queue<e> q;
	q.push(e(0,0));
	d[0]=0;
	while(q.size()){
		e w=q.top();
		q.pop();
		if(v[w.u])continue;
		v[w.u]=true;
		for(int i=0;i<g[w.u].size();++i){
			if(d[g[w.u][i].u]>d[w.u]+g[w.u][i].v){
				d[g[w.u][i].u]=d[w.u]+g[w.u][i].v;
				q.push(e(g[w.u][i].u,d[g[w.u][i].u]));
			}
		}
	}
	return d.back();
}

int dfs(int s,int p,int l,int d,int m){
	if(l>d)return 0;
	if(s==p)return 1;
	int ans=0;
	for(int i=0;i<g[s].size();++i){
		ans+=dfs(g[s][i].u,p,l+g[s][i].v,d,m);
		if(ans>=m)ans-=m;
	}
	return ans;
}

int main(){
	ifstream fin("park.in");
	ofstream fout("park.out");
	int t;
	fin>>t;
	while(t-->0){
		int n,m,k,p;
		fin>>n>>m>>k>>p;
		g.resize(n);
		while(m-->0){
			int a,b,c;
			fin>>a>>b>>c;
			--a,--b;
			g[a].push_back(e(b,c));
		}
		int d=dij();
		fout<<dfs(0,n-1,0,d+k,p)<<endl;
	}
}

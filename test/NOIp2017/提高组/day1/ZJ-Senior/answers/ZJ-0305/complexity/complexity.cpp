#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define MAX 110
using namespace std;
int T, L, F, tot;
bool flag, yes;
struct DATA{
	char Name;
	int x, y;
	bool fx, fy;
}a[MAX];
int ans, Ans, kk, kkk;

int read()
{
	int x=0,f=1;
	char c=getchar();
	while(!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;	
}

int read1(char c)
{
	int x=0,f=1;
	while(isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;	
}

bool Check()
{
	for (int i = 1;i<tot;i++)
		if (a[i].Name==a[tot].Name){
			if (!yes) printf("ERR\n");
			yes=true;
			return true;
		}
	return false;
	
}

void Work()
{
	if (tot<1&&!yes){
		printf("ERR\n");
		yes=true;
		return;
	}
	if (Check()) return;
	if ((a[tot].fx&&!a[tot].fy)||(!a[tot].fx&&!a[tot].fy&&a[tot].x>a[tot].y)){
		ans=0;
		kk=0;
		tot--;
		return;
	}
	if (kk){
		if (!a[tot].fx&&a[tot].fy) ans++;
		tot--;
	}else{
		if (!a[tot].fx&&a[tot].fy){
			ans=1;
			kk=1;
		}
		tot--;
	}
	if (!tot){
		if (ans>Ans) Ans=ans;
		if (kk) kkk=1;
		ans=0;
		kk=0;
	}
	return;
}


void init()
{
	L=read();
	char ch=getchar();
	while(ch!='O') ch=getchar();
	ch=getchar();
	ch=getchar();
	if (ch=='n'){
		ch=getchar();
		flag=true;
		F=read();
	}else{
		flag=false;
		F=read1(ch);
	}
	for (int i = 1;i<=L;i++){
		ch=getchar();
		while(ch!='F'&&ch!='E') ch=getchar();
		if (ch=='E'){
			Work();
			continue;
		}
		cin >> a[++tot].Name;
		ch=getchar();
		while(ch!='n'&&!isdigit(ch)) ch=getchar();
		if (ch=='n'){
			a[tot].fx = true;
		}else{
			a[tot].fx = false;
			a[tot].x = read1(ch);
		}
		
		
		ch=getchar();
		while(ch!='n'&&!isdigit(ch)) ch=getchar();
		if (ch=='n'){
			a[tot].fy = true;
		}else{
			a[tot].fy = false;
			a[tot].y = read1(ch);
		}
	}
	return;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while(T--)
	{
		//��ʼ��
		tot=0;
		yes=0;
		kk=0;
		kkk=0;
		Ans=0;
		ans=0;
		init();
		if (yes) continue;
		if (tot){
			printf("ERR\n");
			continue;
		}
		if (kkk){
			if (flag){
				if (Ans==F){
					printf("Yes\n");
					continue;
				}
			}
			printf("No\n");
			continue;
		}else{
			
			if (!flag){
				printf("Yes\n");
				continue;
			}
			printf("No\n");
			continue;
		}
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

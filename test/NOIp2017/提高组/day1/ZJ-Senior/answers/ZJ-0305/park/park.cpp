#include <iostream>
#include <cstring>
#include <cstdio>
#include <queue>
#include <cmath>
#define MAX 2010
#define MAXM 4010
#define INF 0x3f3f3f3f
using namespace std;
int T, n, m, k, p, u, v, w, head[MAX], dist[MAX];
struct EDGE{
	int to, nex;
	int w;
}edge[MAXM];
queue<int> q;
bool vis[MAX];
int minx, dd[MAX][MAX];
long long ans;
int k0s;

int read()
{
	int x=0,f=1;
	char c=getchar();
	while(!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;	
}

void SPFA()
{
	for (int i = 1;i<=n;i++)
		dist[i]=INF;
	dist[1]=0;
	q.push(1);
	vis[1]=true;
	int k, v;
	while(!q.empty()){
		k=q.front();
		q.pop();
		for (int i = head[k];i!=-1;i=edge[i].nex){
			v=edge[i].to;
			if (dist[v]>dist[k]+edge[i].w){
				dist[v]=dist[k]+edge[i].w;
				if (!vis[v]){
					vis[v]=true;
					q.push(v);
				}
			}
			
		}
		vis[k]=false;
	}
}

void dfs(int Now,int sum)
{
	if (Now==n){
		ans++;
		ans%=p;
	}
	for (int i = 1;i<=n;i++){
		if (dd[Now][i]>=0){
			u=dd[Now][i]+sum;
			if (u<=minx){
				dfs(i,u);
			}
		}
	}
	return;
}


int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--){
		n=read(),m=read(),k=read(),p=read();
		memset(head,-1,sizeof(head));
		memset(vis,0,sizeof(vis));
		memset(dd,-1,sizeof(dd));
		for (int i = 1;i<=m;i++){
			u=read(),v=read(),w=read();
			edge[i].to=v;
			edge[i].nex=head[u];
			head[u]=i;
			edge[i].w=w;
			dd[u][v]=w;
		}

		SPFA();
		if (dist[n]==INF){
			printf("0\n");
			continue;
		}
		minx=dist[n]+k;
		ans=0;
		dfs(1,0);
		cout << ans <<'\n';
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

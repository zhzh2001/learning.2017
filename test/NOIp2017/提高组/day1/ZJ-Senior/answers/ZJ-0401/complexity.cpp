#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
char s[105];
int stk[105],STK[105];
bool app[100];
void solve(){
	int L;scanf("%d %s",&L,s);
	int len=strlen(s),w=0;
	if (s[2]=='n'){
		for (int i=4;i<len-1;i++) w=w*10+s[i]-'0';
	}
	gets(s);
	int MAX=0,t,top=0;
	int ifok=1;
	while (L--){
		gets(s+1),len=strlen(s+1);
		if (ifok==-1) continue;
		memset(app,0,sizeof(app));
		t=0;
		for (int i=1;i<=top;i++){
			if (stk[i]==-1) break;
			t+=stk[i],app[STK[i]-'a']=1;
		}
		MAX=max(MAX,t);
		if (MAX>w) ifok=0;
		if (s[1]=='F'){
			bool f1=0,f2=0;int xx=0,yy=0;
			++top;
			if (app[s[3]-'a']){ifok=-1;continue;}
			STK[top]=s[3];
			int past=5;
			if (s[5]=='n') f1=1,past+=2;
			 else{
				for (int i=past;i<=len;i++){
					if (s[i]==' '){past=i+1;break;}
					xx=xx*10+s[i]-'0';
				}
			}
			if (s[past]=='n') f2=1;
			 else{
				for (int i=past;i<=len;i++){
					if (s[i]==' '){past=i+1;break;}
					yy=yy*10+s[i]-'0';
				}
			}
			if (f1 && f2) stk[top]=0;//O(1)
			if (f1 && !f2) stk[top]=-1;//No count
			if (!f1 && f2) stk[top]=1;//O(n)
			if (!f1 && !f2){
				if (xx<=yy) stk[top]=0;
				 else stk[top]=-1;
			}
		} else{
			if (!top){ifok=-1;continue;}
			top--;
		}
	}
	if (top) ifok=-1;
	if (ifok==1 && MAX!=w) ifok=0;
	if (ifok==-1) puts("ERR"); else
	if (ifok==0) puts("No"); 
	 else puts("Yes");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;scanf("%d",&cas);
	while (cas--) solve();
	return 0;
}

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
#define ll long long
using namespace std;
const int 
	N=100005,
	M=200005;
int n,m,K,P,Ecnt;
int ANS[N],dis[N],q[N*20],times[N];
int f[N][55];
bool vis[N];
struct node{
	int u,num;
};
priority_queue<node>Q;
bool operator <(node a,node b){return a.num>b.num;}
bool operator >(node a,node b){return a.num<b.num;}
struct Edge{
	int next,to,val;
}E[M];int head[N];
void add(int u,int v,int w){
	E[++Ecnt].next=head[u];
	E[Ecnt].to=v;
	E[Ecnt].val=w;
	head[u]=Ecnt;
}
void dijkstra(int s){
	memset(vis,0,sizeof(vis));
	memset(dis,100,sizeof(dis));
	while (!Q.empty()) Q.pop();
	dis[s]=0,Q.push((node){s,0});
	vis[s]=1;
	while (!Q.empty()){
		node x=Q.top();Q.pop();
		for (int i=head[x.u];i;i=E[i].next){
			int v=E[i].to;
			dis[v]=min(dis[v],dis[x.u]+E[i].val);
			if (vis[v]) continue;
			Q.push((node){v,dis[v]}),vis[v]=1;
		}
	}
}
void solve1(){
	memset(vis,0,sizeof(vis));
	memset(ANS,0,sizeof(ANS));
	ANS[1]=1;
	int h=0,t=1;
	q[0]=1,vis[1]=1;
	while (h<t){
		int u=q[h++];
		for (int i=head[u];i;i=E[i].next){
			int v=E[i].to;
			if (dis[v]==dis[u]+E[i].val) ANS[v]=(ANS[v]+ANS[u])%P;
			if (vis[v]) continue;
			q[t++]=v,vis[v]=1;
		}
	}
}
void solve2(){
	int h=0,t=1;
	memset(vis,0,sizeof(vis));
	memset(times,0,sizeof(times));
	q[0]=1,f[1][0]=1;
	while (h<t){
		int u=q[h++];
		times[u]++;
		if (times[u]>(n<<2)){puts("-1");return;}
		vis[u]=0;
		for (int i=head[u];i;i=E[i].next){
			int v=E[i].to;
			for (int k=0;k<=K;k++){
				if (dis[u]+E[i].val-dis[v]+k<=K)
					(f[v][dis[u]+E[i].val-dis[v]+k]+=f[u][k])%=P;
			}
			if (vis[v]) continue;
			q[t++]=v,vis[v]=1;
		}
	}
	int ans=0;
	for (int i=0;i<=K;i++) ans=(ans+f[n][i])%P;
	printf("%d\n",ans);
}
void solve(){
	Ecnt=0;
	scanf("%d%d%d%d",&n,&m,&K,&P);
	memset(head,0,sizeof(head));
	int x,y,z;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
	}
	dijkstra(1);
	if (!K){
		solve1();
		printf("%d\n",ANS[n]);
	} else{
		if (n<=1000) solve2();
		 else puts("-1");
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas;scanf("%d",&cas);
	while (cas--) solve();
}

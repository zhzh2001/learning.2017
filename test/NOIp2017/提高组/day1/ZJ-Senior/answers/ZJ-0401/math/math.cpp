#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
ll gcd(ll a,ll b){
	if (!b) return a;
	return gcd(b,a%b);
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b;scanf("%lld%lld",&a,&b);
	ll LCM=a/gcd(a,b)*b,t1=LCM/a,t2=LCM/b;
	ll ans=b*(t2-1)-a;
	ans=max(ans,a*(t1-1)-b);
	printf("%lld\n",ans);
	return 0;
}


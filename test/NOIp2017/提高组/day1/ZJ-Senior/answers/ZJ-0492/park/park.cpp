#include<cstdio>
#include<cstring>
#include<iostream>
#include<queue>
using namespace std;
int n,m,k,p,t,x,y,z,a[1001][1001],dis[1001],sum[1001];
bool b[1001];
queue<int> q;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(b,0,sizeof(b));
		sum[1]=1;
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++)
				a[i][j]=1<<30;
		for (int i=1; i<=m; i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			a[x][y]=z;
		}
		for (int i=2; i<=n; i++)
			dis[i]=1<<30;
		q.push(1);
		while (!q.empty())
		{
			x=q.front();
			for (int i=1; i<=n; i++)
				if (dis[x]+a[x][i]<dis[i])
				{
					dis[i]=dis[x]+a[x][i];
					sum[i]=sum[x];
					if (!b[i])
					{
						b[i]=1;
						q.push(i);
					}
				}
				else
					if (dis[x]+a[x][i]==dis[i]) sum[i]+=sum[x];
			q.pop();
		}
		printf("%d\n",sum[n]%p);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
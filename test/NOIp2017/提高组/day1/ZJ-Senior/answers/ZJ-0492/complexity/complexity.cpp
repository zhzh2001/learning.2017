#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
using namespace std;
int n,t,ans,ans1,tmp,top,leg,b[256],i,j,k;
char c[101],c1,x,y;
bool b1[101],err;
string s;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	for (i=1; i<=t; i++)
	{
		scanf("%d",&n);
		getline(cin,s);
		err=0;
		ans=0;
		ans1=0;
		tmp=0;
		top=0;
		leg=0;
		memset(b,0,sizeof(b));
		for (j=0; j<s.length(); j++)
			if (s[j]=='^')
			{
				while (s[j+1]>='0' && s[j+1]<='9')
				{
					ans=ans*10+s[j+1]-48;
					j++;
				}
				break;
			}
		for (j=1; j<=n; j++)
		{
			if (err)
			{
				getline(cin,s);
				continue;
			}
			scanf("%c",&c1);
			if (c1=='F')
			{
				getline(cin,s);
				c[++top]=s[1];
				x=s[3];
				for (k=4; k<s.length(); k++)
					if (s[k]==' ') y=s[k+1];
				if (b[int(c[top])])
				{
					err=1;
					continue;
				}
				else
				{
					b[int(c[top])]=top;
					b1[top]=0;
					if (leg>0) continue;
					if (x>='0' && x<='9' && y>='0' && y<='9')
						if (x>y) leg=top;
					if (x>='0' && x<='9' && y=='n')
					{
						b1[top]=1;
						tmp++;
						if (tmp>ans1) ans1=tmp;
					}
					if (x=='n')
						if (y!='n') leg=top;
				}
			}
			if (c1=='E')
			{
				getline(cin,s);
				if (b1[top]) tmp--;
				for (k=0; k<256; k++)
					if (b[k]>=top) b[k]=0;
				top--;
				if (top<leg) leg=0;
				if (top<0) err=1;
			}
		}
		if (err || top>0) printf("ERR\n");
		else
			if (ans==ans1) printf("Yes\n");
			else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
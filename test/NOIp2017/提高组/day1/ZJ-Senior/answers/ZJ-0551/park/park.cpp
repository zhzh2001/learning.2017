#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long LL;
const int N=100005,M=200005;
struct Gragh{
	int cnt,x[M],y[M],nxt[M],fst[N];
	LL z[M];
	void clear(){
		cnt=0;
		memset(fst,0,sizeof fst);
	}
	void add(int a,int b,LL c){
		x[++cnt]=a,y[cnt]=b,z[cnt]=c,nxt[cnt]=fst[a],fst[a]=cnt;
	}
}g[2];
int T,n,m,k;
bool f[N];
LL p,dis[2][N],dp[N][55];
int q[N],head,tail,qmod;
void spfa(int bh){
	head=tail=0;
	qmod=n+1;
	for (int i=1;i<=n;i++)
		dis[bh][i]=1LL<<50;
	memset(f,0,sizeof f);
	if (bh==0)
		dis[bh][1]=0,f[1]=1,q[tail=tail%qmod+1]=1;
	else
		dis[bh][n]=0,f[n]=1,q[tail=tail%qmod+1]=n;
	while (head!=tail){
		int x=q[head=head%qmod+1];
		f[x]=0;
		for (int i=g[bh].fst[x];i;i=g[bh].nxt[i]){
			int y=g[bh].y[i];
			LL z=g[bh].z[i];
			if (dis[bh][x]+z<dis[bh][y]){
				dis[bh][y]=dis[bh][x]+z;
				if (!f[y]){
					f[y]=1;
					q[tail=tail%qmod+1]=y;
				}
			}
		}
	}
}
bool added[N];
void dfsadd(int now,int x){
//	printf("IN %d\n",x);
	if (added[x])
		return;
	for (int i=g[1].fst[x];i;i=g[1].nxt[i])
		if (dis[1][g[1].y[i]]==dis[1][x]+g[1].z[i]){
			dfsadd(now,g[1].y[i]);
			dp[x][now]=(dp[x][now]+dp[g[1].y[i]][now])%p;
		}
	added[x]=1;
//	printf("OUT %d\n",x);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%lld",&n,&m,&k,&p);
		g[0].clear(),g[1].clear();
		for (int i=1,a,b,c;i<=m;i++){
			scanf("%d%d%d",&a,&b,&c);
			g[0].add(a,b,c);
			g[1].add(b,a,c);
		}
		spfa(0),spfa(1);
		//dis[0][i] 是1~i的最短路
		//dis[1][i] 是i~n的最短路 
//		for (int i=1;i<=n;i++)		printf("%lld %lld\n",dis[0][i],dis[1][i]);
		memset(dp,0,sizeof dp);
		dp[1][0]=1;
		for (int v=0;v<=k;v++){
			memset(added,0,sizeof added);
//			for (int i=1;i<=n;i++)	printf("%lld ",dp[i][v]);puts("");
			for (int i=1;i<=n;i++)
				dfsadd(v,i);
//			for (int i=1;i<=n;i++)	printf("%lld ",dp[i][v]);puts("");
			for (int i=1;i<=m;i++){
				int x=g[0].x[i];
				int y=g[0].y[i];
				LL  z=g[0].z[i];
				LL yt=dis[0][x]+z+dis[1][y]+v-(dis[0][x]+dis[1][x]);
				if (yt==v)
					continue;
				if (yt<=k)
					dp[y][yt]=(dp[y][yt]+dp[x][v])%p;
			}
		}
		LL ans=0;
		for (int i=0;i<=k;i++)
			ans=(ans+dp[n][i])%p;
		printf("%lld\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

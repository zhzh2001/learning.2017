#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
using namespace std;
const int Len=100+5;
int T,L,rans,ans;
bool app[300];
char f[10],s[Len][20];
bool isd(char ch){
	return '0'<=ch&&ch<='9';
}
int readO(char s[]){
	int x=0,len=strlen(s);
	bool flag=0;
	for (int i=0;i<len;i++)
		if (isd(s[i]))
			x=x*10+s[i]-48;
		else if (s[i]=='n')
			flag=1;
	if (!flag)
		x=0;
	return x;
}
bool ck1(){
	int v=0;
	for (int i=1;i<=L;i++){
		if (s[i][0]=='F')
			v++;
		else
			v--;
		if (v<0)
			return 0;
	}
	if (v!=0)
		return 0;
	return 1;
}
int MatchE(int x,int R){
	int t=0;
	for (int i=x;i<=R;i++){
		if (s[i][0]=='F')
			t++;
		if (s[i][0]=='E')
			t--;
		if (t==0)
			return i;
	}
	return -1;
}
int solve(int L,int R){
	if (L>R)
		return 0;
	int ans=0;
	for (int i=L;i<R;){
		if (app[s[i][2]])
			return -1;
		int iE=MatchE(i,R);
		app[s[i][2]]=1;
		//n = -1
		int x=0,y=0,len=strlen(s[i]);
		int j;
		for (j=4;j<len;j++){
			if (s[i][j]==' ')
				break;
			if (s[i][j]=='n')
				x=-1;
			else
				x=x*10+s[i][j]-48;
		}
		for (j++;j<len;j++){
			if (s[i][j]==' ')
				break;
			if (s[i][j]=='n')
				y=-1;
			else
				y=y*10+s[i][j]-48;
		}
		int v=solve(i+1,iE-1);
		if (v==-1)
			return -1;
		if (x==-1&&y==-1)
			ans=max(ans,v);
		else if (x!=-1&&y==-1)
			ans=max(ans,v+1);
		else if (x!=-1&&y!=-1&&x<=y)
			ans=max(ans,v);
		app[s[i][2]]=0;
		i=iE+1;
	}
	return ans;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%s",&L,f);
		rans=readO(f);
		getchar();
		for (int i=1;i<=L;i++)
			gets(s[i]);
		if (!ck1()){
			puts("ERR");
			continue;
		}
		memset(app,0,sizeof app);
		ans=solve(1,L);
		if (ans==-1)
			puts("ERR");
		else
			puts(ans==rans?"Yes":"No");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

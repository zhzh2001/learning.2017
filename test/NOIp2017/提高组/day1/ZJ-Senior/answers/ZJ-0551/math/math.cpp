#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
LL a,b,x,y,g;
LL ex_gcd(LL a,LL b,LL &x,LL &y){
	if (!b){
		x=1,y=0;
		return a;
	}
	LL ans=ex_gcd(b,a%b,y,x);
	y-=a/b*x;
	return ans;
}
LL solve(){
	for (LL i=a*b;i>=1;i--){
		LL xx=x*i,yy=y*i;
		LL xxx=(xx%b+b)%b;
		LL yyy=yy-a*((xxx-xx)/b);
		if (yyy<0)
			return i;
	}
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a>b)
		swap(a,b);
	g=ex_gcd(a,b,x,y);
	printf("%lld\n",solve());
	fclose(stdin);fclose(stdout);
	return 0;
}

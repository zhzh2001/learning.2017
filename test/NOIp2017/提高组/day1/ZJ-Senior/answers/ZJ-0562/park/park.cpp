#include<stdio.h>
#include<string.h>
#include<time.h>
#include<math.h>
#include<ctype.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<map>
#include<set>
#include<bitset>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x) {
	x=0;
	char c,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}

template<class T>void pf(T x) {
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x) {
	pf(x);
	putchar(' ');
}
template<class T>void ptn(T x) {
	pf(x);
	putchar('\n');
}
template<class T>void Max(T&x,T y) {
	if(x<y)x=y;
}
template<class T>void Min(T&x,T y) {
	if(y<x)x=y;
}

const int M=200005,INF=1e9;

struct E {
	int t,c,nxt;
} G[M];

int h[M],n,m,E_tot,P,cas,limt,dis[M];

int dp[M][55];

void E_Add(int x,int y,int c) {
	G[++E_tot]=(E) {
		y,c,h[x]
	};
	h[x]=E_tot;
}

struct node {
	int x,c;
	bool operator<(const node&A)const {
		return A.c<c;
	}
};

priority_queue<node>Q;

bool vis[M];

struct Node {
	int x,k;
	bool operator<(const Node&A)const {
		return dis[A.x]+A.k<dis[x]+k;
	}
};

priority_queue<Node>que;

bool mark[M][55];

void SP() {
	while(!Q.empty())Q.pop();
	For(i,1,n)dis[i]=INF,vis[i]=0;
	dis[1]=0;
	Q.push((node) {1,dis[1]});
	while(!Q.empty()) {
		int x=Q.top().x;
		Q.pop();
		if(vis[x])continue;
		vis[x]=1;
		for(int i=h[x]; i; i=G[i].nxt) {
			int y=G[i].t,c=G[i].c+dis[x];
			if(c<dis[y]) {
				dis[y]=c;
				Q.push((node) {y,c});
			}
		}
	}
}


int DP() {
	memset(mark,0,sizeof(mark));
	memset(dp,0,sizeof(dp));
	while(!que.empty())que.pop();
	dp[1][0]=1;
	mark[1][0]=1;
	que.push((Node){1,0});
	while(!que.empty()) {
		
		int x=que.top().x,k=que.top().k;
		que.pop();
		for(int i=h[x];i;i=G[i].nxt){
			int y=G[i].t,co=G[i].c+dis[x]+k;
			int nk=co-dis[y];
			if(nk>limt)continue;
			dp[y][nk]=(dp[y][nk]+dp[x][k])%P;
			if(mark[y][nk])continue;
			que.push((Node){y,nk});
			mark[y][nk]=1;
		}
	}
	int ans=0;
	For(i,0,limt)ans=(ans+dp[n][i])%P;
	return ans;
}

/*
1
5
5
4
10000000
1 2 1
2 4 1
4 3 1
3 2 1
4 5 1
*/

int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	rd(cas);
	while(cas--) {
		rd(n),rd(m),rd(limt),rd(P);
		E_tot=0;
		memset(h,0,sizeof(h));

		For(i,1,m) {
			int a,b,c;
			rd(a);
			rd(b);
			rd(c);
			E_Add(a,b,c);
		}

		SP();

		ptn(DP());
	}
	return 0;
}



#include<stdio.h>
#include<string.h>
#include<time.h>
#include<math.h>
#include<ctype.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<map>
#include<set>
#include<bitset>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	x=0;char c,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');	
	if(x<0)putchar('-'),x=-x;	
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(y<x)x=y;}

ll a,b;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	rd(a),rd(b);
	if(a==1||b==1)ptn(0);
	else ptn(1ll*a*b-a-b);
	return 0;
}	

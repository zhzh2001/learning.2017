#include<stdio.h>
#include<string.h>
#include<time.h>
#include<math.h>
#include<ctype.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<map>
#include<set>
#include<bitset>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	x=0;char c,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');	
	if(x<0)putchar('-'),x=-x;	
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(y<x)x=y;}

const int M=1005;

bool mark[M];/*have use ?*/

int stk[M],pos[M];

struct node{
	int cmd;
	int f;
	int t;	
}B[M];	

char str[M][M];
char pre_str[M];
char tmp[M];

int n,cas,W;

int dfs(int x){
	int l=x,r=pos[x];
	if(B[x].f==-1)return 0;
	int mx=0;
	For(i,l+1,r-1){
		if(B[i].cmd==0){
			int nxt=pos[i];
			Max(mx,dfs(i));
			i=nxt;
		}
	}
	return B[x].f+mx;
}	

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	rd(cas);
	while(cas--){
		bool legal=1;
		memset(mark,0,sizeof(mark));
		memset(stk,0,sizeof(stk));
		memset(pos,0,sizeof(pos));
		rd(n);
		scanf("%s",pre_str);
		if(pre_str[2]=='n')sscanf(pre_str,"O(n^%d)",&W);
		else W=0;
		For(i,1,n){
			char a[10],b[10],c[10];
			scanf("%s",tmp);
			if(tmp[0]=='F'){
				scanf("%s %s %s",a,b,c);
				B[i].cmd=0;
				B[i].t=a[0]-'a';
				if(b[0]=='n'&&c[0]=='n')B[i].f=0;
				else if(b[0]=='n'&&c[0]!='n')B[i].f=-1;
				else if(b[0]!='n'&&c[0]=='n')B[i].f=1;
				else {
					int x,y;
					sscanf(b,"%d",&x);
					sscanf(c,"%d",&y);
//					bug(x),debug(y);
					if(x<=y)B[i].f=0;
					else B[i].f=-1;
				}
//				bug(i),bug(B[i].cmd),bug(B[i].t),debug(B[i].f);
			}else {
				B[i].cmd=1;
//				bug(i),debug(B[i].cmd);
			}
		}
//		cerr<<"------------------------------\n";	
		int top=0;
		For(i,1,n){
			if(B[i].cmd==0){
				stk[++top]=i;
				if(mark[B[i].t]){legal=0;break;}
				mark[B[i].t]=1;
			}
			else {
				if(!top){legal=0;break;}
				int x=stk[top];top--;
				pos[x]=i;
				pos[i]=x;
				mark[B[x].t]=0;
			}	
		}
		if(top!=0)legal=0;
		if(legal){
			B[0].cmd=0;
			B[0].f=0;
			pos[0]=n+1;
			int res=dfs(0);
			if(res==W)puts("Yes");
			else puts("No");
		}
		else puts("ERR");
	}
	return 0;	
}



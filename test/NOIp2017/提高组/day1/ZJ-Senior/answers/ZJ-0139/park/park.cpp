#include<bits/stdc++.h>
using namespace std;
#define N 100005
#define M 200005
#define INF 100000000
int nedge,len,nedge1,q,n,m,k,MO,x,y,z,ans,nxt[M],head[N],too[M],value[M],nxt1[M],head1[N],
	too1[M],heap[4*N],heap1[4*N],d[N];bool c[N],s[N];
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2){
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	int x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline void writ(int ans)
{
	if (ans==0) return;
	writ(ans/10);
	putchar(ans%10+48);
}
inline void addedge(int x,int y,int z)
{
	nxt[++nedge]=head[x];
	head[x]=nedge;
	too[nedge]=y;
	value[nedge]=z;
}
inline void addedge1(int x,int y)
{
	nxt1[++nedge1]=head1[x];
	head1[x]=nedge1;
	too1[nedge1]=y;
}
inline bool check(int x)
{
	c[x]=true;int kk=head1[x];
	bool flag=false;
	while (kk>0){
		int v=too1[kk];
		if (c[v]==true){
			flag=true;break;
		}else flag=flag|check(v);
		kk=nxt1[kk];
	}
	return flag;
}
inline void put(int x,int y)
{
	heap[++len]=x,heap1[len]=y;int son=len;
	while (son>1){
		if (heap1[son]<heap1[son/2]){
			int t=heap[son];
			heap[son]=heap[son/2];
			heap[son/2]=t;
			t=heap1[son];
			heap1[son]=heap1[son/2];
			heap1[son/2]=t;
		}else 
			break;
		son=son/2;
	}
}
inline int get()
{
	int kkk=heap[1],fa=1,son;bool stp=false;
	heap[1]=heap[len];heap1[1]=heap1[len--];
	while (fa*2<=len && stp==false){
		if (heap1[fa*2]<heap1[fa*2+1] || fa*2+1>len) son=fa*2;
		else son=fa*2+1;
		if (heap1[fa]>heap1[son]){
			int t=heap[son];
			heap[son]=heap[fa];
			heap[fa]=t;
			t=heap1[son];
			heap1[son]=heap1[fa];
			heap1[fa]=t;
			fa=son;
		}else
			stp=true;
	}
	return kkk;
}
inline void dfs(int x,int sum)
{
	if (sum>k+d[n]) return;
	if (x==n){
		ans=(ans+1)%MO;
		return;
	}
	int kk=head[x];
	while (kk>0){
		int v=too[kk];
		dfs(v,sum+value[kk]);
		kk=nxt[kk];
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	q=rea();
	for (int p=0;p<q;p++){
		n=rea(),m=rea(),k=rea(),MO=rea(),nedge=0,len=0,nedge1=0;
		memset(head,0,sizeof(head));
		memset(too,0,sizeof(too));
		memset(nxt,0,sizeof(nxt));
		memset(value,0,sizeof(value));
		memset(head1,0,sizeof(head1));
		memset(too1,0,sizeof(too1));
		memset(nxt1,0,sizeof(nxt1));
		memset(heap,0,sizeof(heap));
		memset(heap1,0,sizeof(heap1));
		for (int i=1;i<=m;i++){
			x=rea(),y=rea(),z=rea(),addedge(x,y,z);
			if (z==0) addedge1(x,y);
		}
		memset(c,false,sizeof(c));
		if (check(1)==true){
			putchar('-'),putchar('1'),putchar('\n');
			continue;
		}
		for (int i=1;i<=n;i++) d[i]=INF;
		memset(s,false,sizeof(s));
		d[1]=0,s[1]=true;int kk=head[1];
		while (kk>0){
			int v=too[kk];
			d[v]=value[kk];
			put(v,d[v]);
			kk=nxt[kk];
		}
		for (int i=1;i<n;i++){
			if (len<=0) break;
			do
			{
				kk=get();
			}while(s[kk]!=false);
			s[kk]=true;int kkk=kk;
			kk=head[kk];
			while (kk>0){
				int v=too[kk];
				if (s[v]==false && d[kkk]+value[kk]<d[v]){
					d[v]=d[kkk]+value[kk];
					put(v,d[v]);
				}
				kk=nxt[kk];
			}
		}
		ans=0;dfs(1,0);
		if (ans==0)putchar('0');
		else writ(ans);
		putchar('\n');
	}
	return 0;
}

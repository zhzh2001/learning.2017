#include<bits/stdc++.h>
using namespace std;
int n,x,top,tbs,m,ans;bool stck[100005];char ch;
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2){
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	int x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline void wrk()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	ch=nc();
	while (ch>='0' && ch<='9') ch=nc();
	ch=nc();
	while ((ch<'0' || ch>'9') && ch!='n') ch=nc();
	top++;
	if (ch=='n') stck[top]=true,tbs++;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	n=rea();
	for (int i=1;i<=n;i++){
		x=0,m=rea(),ch=nc();
		while (ch!='O') ch=nc();
		ch=nc(),ch=nc();
		if (ch=='n'){
			ch=nc();
			while (ch<'0' || ch>'9') ch=nc();
			x=ch-'0';ch=nc();
			while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
		}
		tbs=0,top=0,memset(stck,false,sizeof(stck));
		for (int j=1;j<=m;j++){
			ch=nc();
			while (ch!='E' && ch!='F') ch=nc();
			if (ch=='E'){
				if (stck[top]==true) tbs--;
				top--;
			}
			else wrk();
			if (tbs>ans) ans=tbs;
		}
		if (ans==x) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

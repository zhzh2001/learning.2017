#include<bits/stdc++.h>
using namespace std;
long long n,m,xx,yy,XX,YY,d;
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2){
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline long long Rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	long long x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline void Writ(long long ans)
{
	if (ans==0) return;
	Writ(ans/10);
	putchar(ans%10+48);
}
inline long long exgcd1(long long n,long long m,long long&xx,long long&yy)
{
	if (m==0){
		xx=1,yy=0;
		return n;
	}
	long long d=exgcd1(m,n%m,xx,yy),tmp=xx;
	xx=yy,yy=tmp-n/m*xx;
	return d;
}
inline long long exgcd2(long long n,long long m,long long&XX,long long&YY)
{
	if (m==0){
		XX=1,YY=1;
		return n;
	}
	long long d=exgcd2(m,n%m,XX,YY),tmp=XX;
	XX=YY,YY=tmp-n/m*XX;
	return d;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=Rea(),m=Rea();
	d=exgcd1(n,m,xx,yy);
	d=exgcd2(n,m,XX,YY);
	yy=abs(yy),XX=abs(XX);
	xx=abs(xx),YY=abs(YY);
	if (xx*n-yy*m==1) Writ((yy-1)*m+(XX-1)*n+1);
	else Writ((xx-1)*n+(YY-1)*m+1);
	return 0;
}

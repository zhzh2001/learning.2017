#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<vector>
#define ll long long

using namespace std;
const int N=1e3+7;
int T, n, m, k, p, a, b, c, dk, ans;
int d[N];
bool vis[N];

struct node {
	int v, c;
	bool operator <(const node &A) const {
		return c>A.c;
	}
};
vector<node> e[N], eb[N];

inline int read(int &x) {
	x=0;
	char c=getchar();
	while (c<'0' && c>'9')
		c=getchar();
	while (c>='0' && c<='9') {
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
}

void Dijkstra(int s) {
	memset(vis, false, sizeof vis);
	memset(d, 0x3f, sizeof d);
	priority_queue<node> q;
	while (!q.empty())
		q.pop();
	d[s]=0;
	q.push((node){s, 0});
	while (!q.empty()) {
		node tmp=q.top();
		q.pop();
		int u=tmp.v;
		vis[u]=true;
		for (int i=0; i<e[u].size(); i++) {
			int v=e[u][i].v, c=e[u][i].c;
			if (!vis[v] && d[v]>d[u]+c) {
				d[v]=d[u]+c;
				q.push((node){v, d[v]});
			}
		}
	}
}

void dfs(int x, int cost) {
	if (d[x]+cost>dk)
		return ;
	if (x==1)
		ans++;
	for (int i=0; i<eb[x].size(); i++)
		dfs(eb[x][i].v, cost+eb[x][i].c);
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	read(T);
	while (T--) {
		ans=0;
		read(n), read(m), read(k), read(p);
		for (int i=1; i<N; i++)
			e[i].clear(), eb[i].clear();
		for (int i=1; i<=m; i++) {
			read(a), read(b), read(c);
			e[a].push_back((node){b, c});
			eb[b].push_back((node){a, c});
		}
		Dijkstra(1);
		dk=d[n]+k;
		dfs(n, 0);
		cout << ans%p << endl;
	}
	return 0;
}

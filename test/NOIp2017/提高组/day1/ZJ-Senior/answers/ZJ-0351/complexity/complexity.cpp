#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long

using namespace std;
int T, L, x, y;
int o;//时间复杂度 
int sum;
int tot;//当前有用循环层数 
int maxn;//最大循环层数 
char c;
bool flag;
int ff;//当前不可用层数 
char s1[5], s2[5];
bool vis[30];
int a[101];//记录当前层复杂度,0为常数,1为O(n),2不可用 
char ch[101];

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	cin >> T;
	while (T--) {
		o=tot=sum=maxn=ff=0;
		flag=false;
		memset(vis, false, sizeof vis);
		memset(a, 0, sizeof a);
		cin >> L >> c >> c;
		cin >> c;
		if (c=='1') {
			o=0;
			cin >> c;
		}
		else
			cin >> c >> o >> c;
		if (L%2==1)
			flag=true;
		for (int i=1; i<=L; i++) {
			cin >> c;
			if (c=='E') {
				if (sum) {
					vis[ch[sum]-'a']=false;
					if (a[sum]==1)
						tot--;
					if (a[sum]==2)
						ff--;
					sum--;
				} else
					flag=true;
			} else {
				cin >> c;
				sum++;
				if (!vis[c-'a'])
					vis[c-'a']=true;
				else
					flag=true;
				cin >> s1 >> s2;
				ch[sum]=c;
				if (!ff) {
					x=y=0;
					if (s1[0]!='n' && s2[0]!='n') {
						for (int j=0; j<strlen(s1); j++)
							x=x*10+s1[j]-'0';
						for (int j=0; j<strlen(s2); j++)
							y=y*10+s2[j]-'0';
					}
					if (s1[0]!='n' && s2[0]=='n') {
						a[sum]=1;
						tot++;
						maxn=max(maxn, tot);
					} else if ((s1[0]=='n' && s2[0]!='n') || x>y)
						a[sum]=2, ff++;
					else
						a[sum]=0;
				}
			}
		}
		if (sum)
			flag=true;
		if (flag)
			cout << "ERR\n";
		else if (maxn==o)
			cout << "Yes\n";
		else
			cout << "No\n";
	}
	return 0;
}

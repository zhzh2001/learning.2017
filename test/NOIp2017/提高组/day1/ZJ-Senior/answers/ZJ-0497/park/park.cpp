#include <cstdio>
#include <algorithm>
using namespace std ;
#define rep(i,a,b) for(int i=a;i<=b;i++)
const int N=1e5+50 ;
const int inf=1e9+500 ;
int n,m,K,mo,tot,lst[N],To[N],nx[N],w[N],q[N],tmp1[55],tmp2[55],dis[N][55],c[N][55] ;
bool vis[N] ;
void adde(int x,int y,int z) {
	++tot ; To[tot]=y,nx[tot]=lst[x],w[tot]=z,lst[x]=tot ;
}
void debug(int x) {
	printf("debug %d\n",x) ;
	rep(i,0,K) printf("%d:%d %d\n",i,dis[x][i],c[x][i]) ;
}
void spfa() {
	int hd=-1,tl=0 ; q[tl]=1 ;
	rep(i,1,n) vis[i]=0 ; vis[1]=1 ;
	while (hd!=tl) {
		++hd ; if (hd==n) hd=0 ;
		int nw=q[hd] ; //debug(nw) ;
		for (int i=lst[nw];i;i=nx[i]) {
			int t=To[i],ii=0,jj=0,kk=0,flg=0 ;
			while (kk<=K) {
				if (dis[t][jj]>dis[nw][ii]+w[i]) tmp1[kk]=dis[nw][ii]+w[i],tmp2[kk]=c[nw][ii],++ii,flg=1 ; else
					if (dis[t][jj]<dis[nw][ii]+w[i]) tmp1[kk]=dis[t][jj],tmp2[kk]=c[t][jj],++jj ; else
						tmp1[kk]=dis[t][jj],tmp2[kk]=(c[t][jj]+c[nw][ii])%mo,flg=1,++jj,++ii ;
				++kk ;
			}
			if (flg) {
				rep(j,0,K) dis[t][j]=tmp1[j],c[t][j]=tmp2[j] ;
				if (!vis[t]) {
					++tl ; if (tl==n) tl=0 ; q[tl]=t ; vis[t]=1 ;
				}
			}
		}
		vis[nw]=0 ;
	}
}
int main() {
	freopen("park.in","r",stdin) ;
	freopen("park.out","w",stdout) ;
	int cas ; scanf("%d",&cas) ;
	while (cas--) {
		scanf("%d%d%d%d",&n,&m,&K,&mo) ;
		rep(i,1,n) lst[i]=0 ; tot=0 ;
		rep(i,1,m) {
			int x,y,z ; scanf("%d%d%d",&x,&y,&z) ;
			adde(x,y,z) ;
		}
		rep(i,1,n) rep(j,0,K) dis[i][j]=inf,c[i][j]=0 ;
		dis[1][0]=0,c[1][0]=1 ;
		spfa() ;
		int ans=0 ;
		rep(i,0,K) if (dis[n][i]<=dis[n][0]+K) ans=(ans+c[n][i])%mo ;
		printf("%d\n",ans) ;
	}
	return 0 ;
}

#include <cstdio>
#include <algorithm>
using namespace std ;
#define rep(i,a,b) for(int i=a;i<=b;i++)
struct node {
	int tg,fa,l ;
	bool vd ;
} a[1000] ;
int n ;
bool uu[30] ;
int main() {
	freopen("complexity.in","r",stdin) ;
	freopen("complexity.out","w",stdout) ;
	int cas ; scanf("%d",&cas) ;
	while ((cas--)>0) {
		scanf("%d",&n) ;
		char ch[20] ; scanf("%s",ch) ;
		int ans=0 ;
		rep(i,0,19) if (ch[i]=='(') {
			if (ch[i+1]=='n')
				rep(j,i+3,19) {
					if (ch[j]<'0' || ch[j]>'9') break ;
					ans=ans*10+ch[j]-'0' ;
				}
			break ;
		}
		int nw=0,num=0 ; a[0].tg=0 ; a[0].vd=1 ;
		rep(i,0,25) uu[i]=0 ;
		rep(ii,1,n) {
			scanf("%s",ch) ;
			if (ans==-1) {
				if (ch[0]=='F') {
					scanf("%s",ch) ; scanf("%s",ch) ; scanf("%s",ch) ;
				}
				continue ;
			}
			if (ch[0]=='F') {
				++num ; a[num].fa=nw ; a[num].vd=a[nw].vd ;
				scanf("%s",ch) ;
				if (ch[0]>='a' && ch[0]<='z') {
					if (uu[ch[0]-'a']) ans=-1 ; else uu[ch[0]-'a']=1,a[num].l=ch[0]-'a' ;
				}
				int x1=0,x2=0,y1=0,y2=0,i ;
				scanf("%s",ch) ; i=0 ;
				if (ch[0]=='n') x1=1 ; else while (ch[i]>='0' && ch[i]<='9') y1=y1*10+ch[i]-'0',++i ;
				scanf("%s",ch) ; i=0 ;
				if (ch[0]=='n') x2=1 ; else while (ch[i]>='0' && ch[i]<='9') y2=y2*10+ch[i]-'0',++i ;
//				a[num].dep=a[nw].dep+1 ;
				a[num].tg=a[nw].tg+max(0,x2-x1) ;
				if ((y2>0 && y1>0 && y1>y2)|| x1>x2) a[num].vd=0 ;
				nw=num ;
			}
			if (ch[0]=='E') {
				uu[a[nw].l]=0 ;
				if (nw!=0) nw=a[nw].fa ; else ans=-1 ;
			}
		}
		if (nw!=0) ans=-1 ;
		if (ans==-1) puts("ERR") ; else {
			int anss=0 ;
			rep(i,1,num) if (a[i].vd) anss=max(anss,a[i].tg) ;
			if (anss!=ans) puts("No") ; else puts("Yes") ;
		}
	}
	return 0 ;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;
const int M=(int)1e5+5,N=(int)2e5+5,INF=(int)1e9+7;
void Rd(int &res){
	res=0;static char p;
	while(p=getchar(),p<'0');
	do{
		res=(res<<1)+(res<<3)+(p^48);
	}while(p=getchar(),p>='0');
}
struct W{
	int to,nx,d;
}Lis[N*2];
int Head[M],rHead[M],tot;
void ADD(int x,int y,int d){
	Lis[++tot]=(W){y,Head[x],d};
	Head[x]=tot;
}
void rADD(int x,int y,int d){
	Lis[++tot]=(W){y,rHead[x],d};
	rHead[x]=tot;
}
int P;
void Add(int &x,int y){
	x+=y;
	if(x>=P)x-=P;
}
int n,m,K;
int dis[M];
bool cmp(int x,int y){
	return dis[x]<dis[y];
}
void Dijkstra(){
	for(int i=1;i<=n;i++)dis[i]=INF;
	dis[1]=0;
	priority_queue<pair<int,int> >q;
	q.push(pair<int,int>(0,1));
	pair<int,int>tmp;
	int x,d,ll;
	while(!q.empty()){
		tmp=q.top();q.pop();
		x=tmp.second,d=-tmp.first;
		if(dis[x]!=d)continue;
		for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx){
			ll=d+Lis[i].d;
			if(ll<dis[to]){
				dis[to]=ll;
				q.push(pair<int,int>(-ll,to));
			}
		}
	}
}
struct SHUI{
	struct W{
		int x,d;
	}Q[M*50],q[M];//1000W 
	int id[M];
	int dp[M][55];//500W 
	void solve(){
		Dijkstra();
		for(int i=1;i<=n;i++)id[i]=i;
		sort(id+1,id+n+1,cmp);
		int L=0,R=0,sz=0,x,d,t,v,ll;
		for(int i=1;i<=n;i++){
			while(L!=R&&q[L].d<dis[id[i]]){
				Q[sz++]=q[L];
				x=q[L].x,d=q[L].d;d++;
				L++;if(L>=M)L=0;
				if(d-dis[x]<=K){
					q[R++]=(W){x,d};
					if(R>=M)R=0;
				}
			}
			q[R++]=(W){id[i],dis[id[i]]};
		}
		while(L!=R){
			Q[sz++]=q[L];
			x=q[L].x,d=q[L].d;d++;
			L++;if(L>=M)L=0;
			if(d-dis[x]<=K){
				q[R++]=(W){x,d};
				if(R>=M)R=0;
			}
		}
		for(int i=1;i<=n;i++)
			for(int j=0;j<=K;j++)
				dp[i][j]=0;
		dp[1][0]=1;
		for(int i=0;i<sz;i++){
			x=Q[i].x,d=Q[i].d,t=d-dis[x],v=dp[x][t];
			for(int j=Head[x],to;j&&(to=Lis[j].to,1);j=Lis[j].nx){
				ll=d+Lis[j].d-dis[to];
				if(ll<=K)Add(dp[to][ll],v);
			}
		}
		int ans=0;
		for(int i=0;i<=K;i++)Add(ans,dp[n][i]);
		printf("%d\n",ans);
	}
}P70;
struct IHSU{
	int rdis[M];
	void rDijkstra(){
		for(int i=1;i<=n;i++)rdis[i]=INF;
		priority_queue<pair<int,int> >q;
		q.push(pair<int,int>(0,n));
		rdis[n]=0;
		pair<int,int>tmp;
		int x,d,ll;
		while(!q.empty()){
			tmp=q.top();q.pop();
			x=tmp.second,d=-tmp.first;
			if(dis[x]!=d)continue;
			for(int i=rHead[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx){
				ll=d+Lis[i].d;
				if(ll<rdis[to]){
					rdis[to]=ll;
					q.push(pair<int,int>(-ll,to));
				}
			}
		}
	}
	int deg[M],q[M],mark[M],TIM;
	int midis,mirdis;
	void dfs(int x){
		if(dis[x]<midis)midis=dis[x];
		if(rdis[x]<mirdis)mirdis=rdis[x];
		mark[x]=TIM;
		for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx)
			if(mark[to]!=TIM)
				dfs(to);
	}
	bool check(){
		TIM++;
		for(int i=1;i<=n;i++)deg[i]=0;
		for(int x=1;x<=n;x++)
			for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx)
				if(Lis[i].d==0)
					deg[to]++;
		int L=0,R=0,x;
		for(int i=1;i<=n;i++)if(deg[i]==0)q[R++]=i;
		while(L<R){
			x=q[L++];mark[x]=TIM;
			for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx)
				if(Lis[i].d==0){
					deg[to]--;
					if(deg[to]==0)q[R++]=to;
				}
		}
		for(int i=1;i<=n;i++)
			if(mark[i]!=TIM){
				midis=INF,mirdis=INF;
				dfs(i);
				if(midis+mirdis<=dis[n]+K)return true;
			}
		return false;
	}
	struct W{
		int x,d;
	}Q[M*50];//1000W 
	int DEG[M][55],dp[M][55];//1000W 
	void solve(){
		Dijkstra();
		rDijkstra();
		if(check()){
			puts("-1");
			return;
		}
		for(int i=1;i<=n;i++)
			for(int j=0;j<=K;j++)
				DEG[i][j]=0;
		for(int x=1,ll;x<=n;x++)
			for(int j=0;j<=K;j++)
				for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx){
					ll=dis[x]+j+Lis[i].d-dis[to];
					if(ll<=K)DEG[to][ll]++;
				}
		int L=0,R=0,x,d,v,ll;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=K;j++){
				dp[i][j]=0;
				if(DEG[i][j]==0)Q[R++]=(W){i,j+dis[i]};
			}
		dp[1][0]=1;
		while(L<R){
			x=Q[L].x,d=Q[L].d,v=dp[x][d-dis[x]];L++;
			for(int i=Head[x],to;i&&(to=Lis[i].to,1);i=Lis[i].nx){
				ll=d+Lis[i].d-dis[to];
				if(ll<=K){
					Add(dp[to][ll],v);
					DEG[to][ll]--;
					if(DEG[to][ll]==0)Q[R++]=(W){to,ll+dis[to]};
				}
			}
		}
		int ans=0;
		for(int i=0;i<=K;i++)Add(ans,dp[n][i]);
		printf("%d\n",ans);
	}
}PPP;
void solve(){
	Rd(n),Rd(m),Rd(K),Rd(P);
	bool NO_Z=true;
	for(int i=1,x,y,d;i<=m;i++){
		Rd(x),Rd(y),Rd(d),ADD(x,y,d),rADD(y,x,d);
		if(d==0)NO_Z=false;
	}
	if(NO_Z)P70.solve();
	else PPP.solve();
	for(int i=1;i<=n;i++)Head[i]=rHead[i]=0;tot=0;
}
int main(){
//	printf("%.5f\n",1.0*(sizeof(P70)+sizeof(PPP)+sizeof(Lis)+sizeof(Head)+sizeof(rHead)+sizeof(dis))/1024/1024);
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas;Rd(cas);
	while(cas--)solve();
	return 0;
}

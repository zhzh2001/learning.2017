#include <bits/stdc++.h>

using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int a,b;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read(); b=read();
	if(a>b) swap(a,b);
	cout << 1ll*(a-1)*b-1ll*a <<endl;
	return 0;
}

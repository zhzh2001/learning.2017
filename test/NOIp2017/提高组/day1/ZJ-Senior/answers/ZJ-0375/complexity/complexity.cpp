#include <bits/stdc++.h>

using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int t,n,cen,jug,cur,F[110][110],ss[110],X[110][2],las[110];
int sta[110],top,pos[110]; 
char sd[110];
inline void dfs(int x)
{
	if(x>n) return; 
	scanf("%s",sd);
	if(sd[0]=='E') {ss[x]=-1; cur--; las[pos[top]]=x; top--; dfs(x+1); return ;}
	if(sd[0]=='F') cur++;
	if(cur<0) jug=0;
	scanf("%s",sd);
	pos[++top]=x; sta[top]=sd[0];
	for(int i=1;i<top;i++) if(sd[0]==sta[i]) jug=0;
	ss[x]=1; scanf("%s",sd);
	if(sd[0]=='n') X[x][0]=100000;
	else 
	{
		int nw=0; while(isdigit(sd[nw])) 
			X[x][0]=X[x][0]*10+sd[nw]-'0',nw++;
	}
	scanf("%s",sd);
	if(sd[0]=='n') X[x][1]=100000;
	else 
	{
		int nw=0; while(isdigit(sd[nw])) 
			X[x][1]=X[x][1]*10+sd[nw]-'0',nw++;
	} dfs(x+1);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=read(); while(t--)
	{
		memset(F,0,sizeof F); top=0;
		memset(X,0,sizeof X);
		memset(las,0,sizeof las);
		cen=0; jug=1; cur=0;
		n=read(); scanf("%s",sd);
		if(sd[2]=='1') cen=0;
		else 
		{
			int ts=4; while(isdigit(sd[ts]))
				cen=cen*10+sd[ts]-'0',ts++;
		}
		dfs(1); if(cur) jug=0;
		if(jug)
		{
			int ret=0; for(int i=2;i<=n;i++)
				for(int j=1;j<=n;j++) if(las[j]==j+i-1)
				{
					if(X[j][0]>X[j][1]){ F[j][j+i-1]=0; continue;}
					if(X[j][1]-X[j][0]<=200) F[j][j+i-1]=0;
					else F[j][j+i-1]=1; 
					int xd=0;
					for(int k=j+1;k<=j+i-2;k++)
					{
						xd=max(xd,F[k][las[k]]);
						k=las[k];	
					}		
					F[j][las[j]]+=xd;
				}
			for(int i=1;i<=n;i++) ret=max(F[i][las[i]],ret),i=las[i];
			if(cen==ret) puts("Yes");
			else puts("No");
		}
		else puts("ERR");
	}	
	return 0;
}

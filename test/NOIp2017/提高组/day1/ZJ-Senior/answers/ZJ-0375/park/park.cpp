#include <bits/stdc++.h>
#define N 100100
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
struct edge
{
	int v,next,w;
}vs[N*3],td[N*110];
int st[N],ee,t,n,m,k,p,vis[N],dis[N];
int stt[N*52],eee,du[N*52],F[N*52],bb[N*52],l,r;
queue <int> q;
inline void addedge(int u,int v,int c)
{
	vs[++ee].v=v;vs[ee].next=st[u];st[u]=ee; vs[ee].w=c;
}
inline void addedgee(int u,int v)
{
	td[++eee].v=v;td[eee].next=stt[u];stt[u]=eee; du[v]++;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=read(); while(t--)
	{
		ee=0; memset(st,0,sizeof st);
		memset(du,0,sizeof du);
		n=read(); m=read(); k=read(); p=read();
		for(int i=1;i<=m;i++)
		{
			int u=read(), v=read(), c=read();
			addedge(u,v,c); 
		}
		memset(vis,0,sizeof vis);
		q.push(1); vis[1]=1; dis[1]=0;
		for(int i=2;i<=n;i++) dis[i]=0x3fffffff;
		while(!q.empty())
		{
			int lx=q.front(); q.pop();
			for(int i=st[lx];i;i=vs[i].next)
			{
				if(dis[vs[i].v]>dis[lx]+vs[i].w)
				{
					dis[vs[i].v]=dis[lx]+vs[i].w;
					if(!vis[vs[i].v]) q.push(vs[i].v), vis[vs[i].v]=1;
				}
			} vis[lx]=0;
		}
		eee=0; memset(stt,0,sizeof stt);
		for(int i=1;i<=n;i++) for(int bs=0;bs<=k;bs++)
		{
			for(int j=st[i];j;j=vs[j].next)
			{
				if(dis[i]+bs+vs[j].w<=dis[vs[j].v]+k)
					addedgee(bs*n+i,(dis[i]+bs+vs[j].w-dis[vs[j].v])*n+vs[j].v);
			}
		}
		l=1; r=0;
		int cnt=0; for(register int i=1;i<=k*n+n;i++) 
			if(!du[i]) bb[++r]=i,cnt++;
		memset(F,0,sizeof F); F[1]=1;
		while(l<=r)
		{
			int lx=bb[l]; l++;
			for(int i=stt[lx];i;i=td[i].next)
			{
				F[td[i].v]+=F[lx];
				if(F[td[i].v]>=p) F[td[i].v]-=p;
				du[td[i].v]--; 
				if(!du[td[i].v]) bb[++r]=td[i].v,cnt++;
			}
		}
		if(cnt!=(k*n+n)) puts("-1");
		else 
		{
			int Ans=0; for(int i=0;i<=k;i++)
			{
				Ans+=F[i*n+n];
				if(Ans>=p) Ans-=p;
			}
			cout << Ans << endl;
		}
	}
	return 0;
}

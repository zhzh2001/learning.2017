#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
int t, l, cf, cs, xx[100001];
char s[200], bl[100001];
bool used[26], flag;
void csh()
{
	flag = false;
	cf = 0;
	cs = 0;
	for (int j = 0; j < 26; ++j)
		used[j] = false;
	for (int j = 0; j <= 100; ++j)
		xx[j] = 0;
}
int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	scanf("%d", &t);
	for (int i = 1; i <= t; ++i)
	{
		csh();
		scanf("%d", &l);
		scanf("%s", &s);
		if (s[2] == '1')
			cf = 0;
		else
		{
			int cnt = 4;
			while (s[cnt] >= '0' && s[cnt] <= '9')
			{
				cf = cf * 10 + s[cnt] - '0';
				++cnt;
			}
		}
		for (int j = 1; j <= l; ++j)
		{
			char cc;
			cin >> cc;
			if (cc == 'F')
			{
				char c1[60] = {}, c2[60] = {};
				++cs;
				cin >> bl[cs];
				if (used[bl[cs] - 'a'])
				{
					flag = true;
					printf("ERR\n");
					break;
				}
				used[bl[cs] - 'a'] = true;
				cin >> c1 >> c2;
				if (c1 != "n" && c2[0] == 'n' && !flag)
					xx[cs] = max(xx[cs - 1] + 1, xx[cs]) ;
				else
					xx[cs] = max(xx[cs - 1], xx[cs]);
			}
			if (cc == 'E')
			{
				used[bl[cs] - 'a'] = true;
				cs --;
			}
		}
		if (!flag)
			if (cs != 0)
				printf("ERR\n");
			else
			{
				int maxx = 0;
				for (int j = 1; j <= l; ++j)
					maxx = max(maxx, xx[j]);
				if (maxx == cf)
					printf("Yes\n");
				else printf("No\n");
			}
	}
	fclose(stdin);
	fclose(stdout);
}

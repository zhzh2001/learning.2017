#include <cstdio>
using namespace std;
int a, b, lx, ly, lz, lans;
int x[1000], y[1000], z[1000], ans[1000];
void ggc()
{
	for (int i = 1; i <= lx; ++i)
		for (int j = 1; j <= ly; ++j)
		{
			z[i + j - 1] += x[i] * y[j];
			z[i + j] += z[i + j - 1] / 10;
			z[i + j - 1] %= 10;
		}
	if (z[lx + ly] != 0)
		lz = lx + ly;
	else lz = lx + ly - 1;
}
void ggj()
{
	for (int i = 1; i <= lx; ++i)
	{
		z[i] -= x[i];
		while (z[i] < 0)
		{
			--z[i + 1];
			z[i] += 10;
		}
	}
	int cnt = lx + 1;
	while (z[cnt] < 0)
	{
		while (z[cnt] < 0)
		{
			z[cnt] += 10;
			z[cnt + 1] --;
		}
		cnt ++;
	}
	if (z[lz] == 0)
		--lz;
	for (int i = 1; i <= ly; ++i)
	{
		z[i] -= y[i];
		while (z[i] < 0) 
		{
			--z[i + 1];
			z[i] += 10;
		}
	}
	cnt = ly + 1;
	while (z[cnt] < 0)
	{
		while (z[cnt] < 0)
		{
			z[cnt] += 10;
			z[cnt + 1] --;
		}
		cnt ++;
	}
	if (z[lz] == 0)
		--lz;
}
int main()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%d%d", &a, &b);
	if ((a == 1) | (b == 1))
	{
		printf("0\n");
	}
	else
	{
		while (a != 0)
		{
			x[++lx] = a % 10;
			a /= 10;
		}
		while (b != 0)
		{
			y[++ly] = b % 10;
			b /= 10;
		}
		ggc();
		ggj();
		for (int i = lz; i > 1; --i)
			printf("%d", z[i]);
		printf("%d\n", z[1]);
	}
	fclose(stdin);
	fclose(stdout);
}

#include <cstdio>
#include <cstring>
using namespace std;
int t, l, n, m, k, p, ans = 1, xx[10001];
bool f[10001];
int e[1001][1001];
void dfs(int x, int last)
{
	if (x == 0)
	{
		int s = 0;
		s += e[1][xx[1]] + e[xx[l]][n];
		for (int i = 1; i < l; ++i)
			s += e[xx[i]][xx[i + 1]];
		if (s <= k)
		{
			++ans;
			ans %= p;
		}
		return;
	}
	for (int i = last + 1; i <= n; ++i)
		if (!f[i])
		{
			f[i] = true;
			xx[l - x + 1] = i;
			dfs(x - 1, i);
			f[i] = false;
		}
}
int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &t);
	for (int ll = 1; ll <= t; ++ll)
	{
		scanf("%d%d%d%d", &n, &m, &k, &p);
		for (int a = 1; a <= n; ++a)
			for (int b = 1; b <= n; ++b)
				e[a][b] = 100000;
		for (int i = 1; i <= m; ++i)
		{
			int a, b, c;
			scanf("%d%d%d", &a, &b, &c);
			e[a][b] = c;
		}
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j)
				for (int k = 1; k <= n; ++k)
					if (i != j && j != k && i != k)
						if (e[i][k] + e[j][k] < e[i][j])
							e[i][j] = e[i][k] + e[j][k];
		k += e[1][n];
		for (int i = 1; i <= n - 2; ++i)
		{
			memset(f, 0, sizeof(f));
			f[1] = true;
			f[n] = true;
			l = i;
			dfs(i, 0);
		}
		printf("%d", ans);
	}
	fclose(stdin);
	fclose(stdout);
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,i,j,T,FZD;
int now=0,ans,top,time[1000];
char fzd[1000],z[1000];
bool bo[1000],ok;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n);
		memset(fzd,0,sizeof(fzd));
		scanf("%s",fzd);

		if(fzd[2]=='1')FZD=0;else
		{
			FZD=fzd[4]-'0';
			if(fzd[5]<='9'&&fzd[5]>='0')FZD=FZD*10+fzd[5]-'0';
		}
		memset(bo,0,sizeof(bo));
		memset(z,0,sizeof(z));
		memset(time,0,sizeof(time));
		now=0,ans=0,top=0;
		ok=true;
		for (int i=1;i<=n;i++)
		{
			char ch=getchar();
			while(ch!='F'&&ch!='E')ch=getchar();
			if(ch=='F')
			{
				ch=getchar();
				while(!(ch<='z'&&ch>='a'))ch=getchar();
				if(bo[ch])ok=false;
				bo[ch]=true;
				top++,z[top]=ch;
				int first=0,second=0;
				
				ch=getchar();
				while(!((ch<='9'&&ch>='0')||ch=='n'))ch=getchar();
				if(ch=='n')first=100000;else 
				{
					while (ch<='9'&&ch>='0')first=first*10+ch-'0',ch=getchar();
				}
				
				ch=getchar();
				while(!((ch<='9'&&ch>='0')||ch=='n'))ch=getchar();
				if(ch=='n')second=100000;else 
				{
					while (ch<='9'&&ch>='0')second=second*10+ch-'0',ch=getchar();
				}
				
				time[top]=second-first;
				if(time[top]>10000)
				{
					bool jia=true;
					for (int ii=1;ii<=top;ii++)
					if(time[ii]<0)jia=false;
					if(jia)now++,ans=max(ans,now);
				}
			}else
			{
				if(top)
				{
					bool jian=true;
					for (int ii=1;ii<=top;ii++)
					if(time[ii]<0)jian=false;
					if(jian&&time[top]>10000)now--;
					bo[z[top]]=false;top--;
				}else ok=false;
			}
		}
		if(ok&&top==0)
		{
			if(FZD==ans)puts("Yes");else puts("No");
		}else puts("ERR");
	}
	return 0;
}

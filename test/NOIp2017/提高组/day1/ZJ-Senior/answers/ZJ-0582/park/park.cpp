#include<cstdio>
#include<cstring>
using namespace std;
long long n,m,i,j,k,top,T,p,x,y,z;
long long f[500000],first[500000],next[500000],last[500000],v[500000],to[500000],ans[100010][51];
inline void add(long long x,long long y,long long z)
{
	top++,to[top]=y,v[top]=z;
	if(first[x]==0)first[x]=top;
	else next[last[x]]=top;
	last[x]=top;
}
long long q[1000000],h,t;
bool bo[200000];
inline void push(long long x)
{
	if(bo[x])return;
	bo[x]=true;
	h++,q[h]=x;
}
inline void push1(long long x)
{
	h++,q[h]=x;
}
int main()
{
	freopen("park.in","r",stdin),freopen("park.out","w",stdout);
	scanf("%lld",&T);
	while (T--)
	{
		top=0;
		memset(bo,0,sizeof(bo)),memset(q,0,sizeof(q)),memset(to,0,sizeof(to)),memset(first,0,sizeof(first)),memset(ans,0,sizeof(ans)),memset(next,0,sizeof(next)),memset(last,0,sizeof(last)),memset(v,0,sizeof(v));
		scanf("%lld%lld%lld%lld",&n,&m,&k,&p);
		for (i=1; i<=m; i++)scanf("%lld%lld%lld",&x,&y,&z),add(x,y,z);
		for (i=2; i<=n; i++)f[i]=2100000000;
		h=0,t=0;
		push(1);
		while (h>=t)
		{
			bo[q[t]]=false;
			t++;
			long long now=q[t];
			for (i=first[now]; i; i=next[i])
				if(f[to[i]]>f[now]+v[i])push(to[i]),f[to[i]]=f[now]+v[i];
		}


		long long answer=0;
		ans[1][0]=1;
		memset(q,0,sizeof(q)),h=0,t=0;
		push1(1);
		while (h>=t)
		{
			t++;
			long long now=q[t];
			for (i=first[now]; i; i=next[i])
			{
				if(f[now]+v[i]-f[to[i]]<=k)push1(to[i]);
				for (j=0; j<=k; j++)
				{
					if(ans[now][j])
					if(f[now]+j+v[i]-f[to[i]]<=k)
					{
						
						ans[to[i]][f[now]+j+v[i]-f[to[i]]]=(ans[now][j]+ans[to[i]][f[now]+j+v[i]-f[to[i]]])%p;
					}
				}
			}
			for (j=0; j<=k; j++)answer=(answer+(now==n)*ans[now][j])%p,ans[now][j]=0;
		}
		printf("%lld\n",answer);
	}
	return 0;
}

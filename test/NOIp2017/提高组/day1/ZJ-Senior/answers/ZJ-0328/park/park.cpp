#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>

using namespace std;

const int qw=400005;

const int inf=0x7f7f7f7f;

int nxt[qw],adj[qw],go[qw],fee[qw];
int dis[qw],sum[qw],apr[qw];
bool bo[qw];
int que[qw];

int t,n,m,k,mo,ecnt=0;//!!!!!!

void add(int u,int v,int w){
	nxt[++ecnt]=adj[u];adj[u]=ecnt;go[ecnt]=v;fee[ecnt]=w;
}

void bfs(){
	int head=1,tail=1;
	for(int i=1;i<=n;i++){
	  dis[i]=inf;
	  sum[i]=1;bo[i]=0;// not in
	}
	que[head]=1;dis[1]=0;bo[1]=1;
	int u,v;
	while(head<=tail){
		u=que[head];
		bo[u]=false;	
		head++;
		for(int e=adj[u];e;e=nxt[e]){
			v=go[e];
			if(dis[v]>dis[u]+fee[e]){
				dis[v]=dis[u]+fee[e];
				sum[v]=sum[u];
				if(bo[v]==false){
					bo[v]=1;
					que[++tail]=v;
				}
			}
			else if(dis[v]==dis[u]+fee[e]){
				sum[v]=(sum[v]+sum[u])%mo;
				if(bo[v]==false){
				  que[++tail]=v;
				}
			}
		}

	}
}

void init(){
	ecnt=0;
	for(int i=0;i<=max(n,m);i++){
		adj[i]=go[i]=nxt[i]=0;
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	int u,v,w;
	while(t>0){
		t--;
		scanf("%d%d%d%d",&n,&m,&k,&mo);
		init();
		for(int i=1;i<=m;i++){
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		bfs();
		printf("%d\n",sum[n]);

	}
}

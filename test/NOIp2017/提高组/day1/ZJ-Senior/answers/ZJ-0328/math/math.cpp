#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>

using namespace std;

#define ll long long

ll a,b;
ll ans=0;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a>b)swap(a,b);
	
	ans=(a-2)*b+b-a;
	printf("%lld",ans);
	return 0;
}

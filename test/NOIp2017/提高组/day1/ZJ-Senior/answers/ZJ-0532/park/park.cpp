#include<bits/stdc++.h>
#define ll long long
using namespace std;

typedef pair<int,int>P;

ll n,m,k,Mod;

int T;

template<class _> void add(_ &a,_ b){
	a+=b;
	if(a>=Mod)a-=Mod;
	if(a<0)a+=Mod;
}

struct node{
	int to,cost;
};

vector<node>G[1005];

int dp[1005][90005];

int Dijkstra(){
	priority_queue<P,vector<P>,greater<P> >Q;
	int d[1005];
	memset(d,63,sizeof(d));
	d[1]=0;
	P a;
	a=P(0,1);
	while(!Q.empty())Q.pop();
	Q.push(a);
	while(!Q.empty()){
		P a;
		a=Q.top();
		Q.pop();
		int x=a.second;
		if(d[x]<a.first)continue;
		for(int i=0;i<(int)G[x].size();i++){
			int to=G[x][i].to;
			if(d[to]>d[x]+G[x][i].cost){
				d[to]=d[x]+G[x][i].cost;
				Q.push(P(d[to],to));
			}
		}
	}
	return d[n];
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>T;
	while(T--){
		scanf("%lld%lld%lld%lld",&n,&m,&k,&Mod);
		for(int i=0;i<1005;i++)G[i].clear();
		memset(dp,0,sizeof(dp));
		for(int i=1;i<=m;i++){
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			G[a].push_back((node){b,c});
		}
		int L=Dijkstra();
		int R=L+k;
		dp[1][0]=1;
		for(int i=0;i<=min(R,90000);i++){
			for(int j=1;j<=n;j++){//枚举现在的点 
				for(int k=0;k<(int)G[j].size();k++){
					int to=G[j][k].to;
					int c=G[j][k].cost+i;
					if(c>R)continue;
					add(dp[to][c],dp[j][i]);
				}
			}
		}
		int ans=0;
		for(int i=L;i<=R;i++){
			add(ans,dp[n][i]);
		}
		printf("%d\n",ans);
	}
	return 0;
}

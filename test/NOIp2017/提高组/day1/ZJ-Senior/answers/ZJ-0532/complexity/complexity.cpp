#include<bits/stdc++.h>
using namespace std;

int T;
int l;char s[15];
int num=0,flag=0,k=0;

struct P30{
	void solve(){
		while(l--){
			char a[2],b[10];
			scanf("%s",a+1);
			char c[10],d[10];
			if(a[1]=='F'){
				scanf("%s%s%s",b+1,c+1,d+1);
				if(d[1]=='n')k++;
			}
		}
		if(flag==2&&k==0)printf("Yes\n");
		else if(flag==2&&k!=0)printf("No\n");
		else if(flag==1&&k==num)printf("Yes\n");
		else printf("No\n");
	}

}p30;

struct P50{

	int Fa[105];
	int val[105];
	int tid;

	void solve(){
		val[0]=0;
		tid=0;
		int now=0;
		int Max=0;
		while(l--){
			char a[2],b[10];
			scanf("%s",a+1);
			char c[10],d[10];
			if(a[1]=='F'){
				++tid;
				Fa[tid]=now;
				scanf("%s%s%s",b+1,c+1,d+1);
				if(d[1]=='n')val[tid]=val[Fa[tid]]+1;
				else val[tid]=val[Fa[tid]];
				now=tid;
			}
			else now=Fa[tid];
			if(Max<val[tid])Max=val[tid];
		}
		if(flag==2&&Max==0)printf("Yes\n");
		else if(flag==2&&Max!=0)printf("No\n");
		else if(flag==1&&Max==num)printf("Yes\n");
		else printf("No\n");
	}

}p50;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>T;
	while(T--){
		num=0;
		flag=0;
		memset(s,0,sizeof(s));
		scanf("%d%s",&l,s+1);
		k=0;
		for(int i=1;i<=(int)strlen(s+1);i++){
			if(s[i]=='('||s[i]=='O'||s[i]==')')continue;
			if(s[i]=='^')flag=1;
			if(flag==0&&s[i]=='n')flag=1;
			if(s[i]=='1'&&flag==0)flag=2;
			if(s[i]>='0'&&s[i]<='9'&&flag==1)num=num*10+s[i]-'0';
		}
		if(l<=10)p30.solve();
		else p50.solve();
	}
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
using namespace std;

ll a,b;

struct P30{
	
	bool mark[10005];
	
	void solve(){
		memset(mark,0,sizeof(mark));
		for(int i=0;i<=5000;i++){
			for(int j=0;j<=5000;j++){
				if(i*a+j*b>10000)break;
				mark[i*a+j*b]=1;
			}
		}
		for(int i=2500;i>=0;i--){
			if(!mark[i]){
				printf("%d\n",i);
				break;
			}
		}
	}
}p30;

struct P60{
	
	void solve(){
		ll res=a*b-a-b;
		printf("%lld\n",res);
	}

}p100;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	cin>>a>>b;
	if(a>b)swap(a,b);
	if(a<=50&&b<=50)p30.solve();
	else p100.solve();
	return 0;
}	

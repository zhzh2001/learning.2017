#include<bits/stdc++.h>
#define N 30000
using namespace std;
struct rrsb{
	int a,b;
};
void read(int&n)
{
	int k=1;n=0;char ch=getchar();
	while(!('0'<=ch&&ch<='9')&&ch!='-')ch=getchar();
	if(ch=='-')k=-1,ch=getchar();
	while('0'<=ch&&ch<='9')n=n*10+ch-48,ch=getchar();
	n*=k;
}
int to[N*2],head[N],nxt[N*2],val[N*2],tot;
void add(int x,int y,int z)
{
	to[++tot]=head[x],nxt[tot]=y,val[tot]=z,head[x]=tot;
}
queue<rrsb> q;bool bo;
int T,n,m,k,p,i,j,x,y,z,mi,dist[N],f[N];bool b[N];
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n),read(m),read(k),read(p);bo=0;
		for(i=1;i<=m;i++){read(x),read(y),read(z),add(x,y,z);if(z==0)bo=1;}
			memset(dist,100,sizeof(dist));
			dist[1]=0;f[1]=0;
			for(i=1;i<=n;i++)
			{
				mi=0;
				for(j=1;j<=n;j++)if(dist[j]<dist[mi]&&!b[j])mi=j;
				b[mi]=1;
				for(j=head[mi];j;j=to[j])
				if(dist[nxt[j]]>dist[mi]+val[j])
				dist[nxt[j]]=dist[mi]+val[j],f[nxt[j]]=f[mi];
				else
				if(dist[nxt[j]]==dist[mi]+val[j])
				f[nxt[j]]+=f[mi];
			}
			if(k==0&&!bo){printf("%d\n",f[n]%p);continue;}
		q.push((rrsb){1,0});y=0;z=0;
		while(!q.empty())
		{
			z++;
			mi=q.front().a;x=q.front().b;q.pop();
			for(j=head[mi];j;j=to[j])
			if(x+val[j]<=dist[nxt[j]]+k)
			{
				if(nxt[j]==n)y++;
				q.push((rrsb){nxt[j],x+val[j]});
			}
			if(y>100000000)y%=p;
			if(z>10000000){printf("-1\n");break;}
		}
		if(z<=10000000)printf("%d\n",y%p);
	}
	return 0;
}

#include<cstring>
#include<cmath>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define L 107
using namespace std;

int n,bn,top,err,num,xz,lon;
bool boo[L],flag[L],pan[L];
char s[L][L];
char ch[107],ch1[107],ch2[107];

void prepare()//求标准。 
{
	int len=strlen(ch),x=0;bool flag=0;
	for (int i=1;i<len;i++)
	{
		if(ch[i]=='n') flag=1;
		if (ch[i]<='9'&&ch[i]>='0') x=x*10+ch[i]-'0'; 
	}
	if (flag) bn=x;
	else bn=0;
}
void init(int &x,int &y)
{
	int len=strlen(ch1);
	if (len==1&&ch1[0]=='n') x=-1;
	else for (int i=0;i<len;i++)x=x*10+ch1[i]-'0';
	
	len=strlen(ch2);
	if (len==1&&ch2[0]=='n') y=-1;
	else for (int i=0;i<len;i++)y=y*10+ch2[i]-'0';
}
void solve()
{
	if (xz) return;
	int x=0,y=0;init(x,y);
	if (x!=-1&&y==-1) boo[top]=1,num++;
	else if (x==-1&&y!=-1) flag[top]=1,xz=1;
	else if (x!=-1&&y!=-1&&x>y) flag[top]=1,xz=1;
	
	n=max(n,num);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int T;scanf("%d",&T);
	while(T--)
	{
		memset(pan,0,sizeof(pan));//记录有没有重复变量
		memset(boo,0,sizeof(boo));//boo记录当前是不是一次幂 
		memset(flag,0,sizeof(flag));//表示限制情况。 
		bn=top=err=n=num=xz=0;//num记录当前是多少幂,xz表示当前循环能否进行。 
		scanf("%d%s",&lon,ch);
		//cout<<T<<" "<<ch<<endl;
		prepare();
		while(lon)
		{
			scanf("%s",ch);lon--;
			if (ch[0]=='F')
			{
				scanf("%s%s%s",s[++top],ch1,ch2);
				if (pan[s[top][0]-'a']) {err=1;break;}
				else pan[s[top][0]-'a']=1;
				solve();
			}
			else
			{
				pan[s[top][0]-'a']=0;
				if (boo[top]) num--,boo[top]=0;
				if (flag[top]) xz=0,flag[top]=0;
				top--;
				if (top<0) {err=1;break;}
			}
		}
		gets(ch);
		while (lon--) gets(ch);//直接读完剩下。 
		if (top!=0) err=1;
		
		if (err) printf("ERR\n");
		else if (n==bn) printf("Yes\n");
		else printf("No\n");
	}
}

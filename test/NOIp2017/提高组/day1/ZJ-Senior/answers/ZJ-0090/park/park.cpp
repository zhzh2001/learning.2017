#include<cstring>
#include<cmath>
#include<cstdio>
#include<iostream>
#include<algorithm>
#include<queue>
#define fzy pair<int,int>
#define M 200007
#define N 100007
#define inf 2000000007
using namespace std;

int n,m,k,p,minn;
int cnt,head[N],next[M],rea[M],val[M];
int dist[N];
bool boo[N];
long long ans;
struct Node
{
	int x,y,z;
}a[M];

inline int read()
{
	int x=0;char ch=getchar();
	while(ch>'9'||ch<'0') ch=getchar();
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x;
}
void add(int u,int v,int fee)
{
	next[++cnt]=head[u];
	head[u]=cnt;
	rea[cnt]=v;
	val[cnt]=fee;
}
void Dijkstra()
{
	priority_queue<fzy,vector<fzy>,greater<fzy> >q;
	while(!q.empty())q.pop();
	for (int i=1;i<=n;i++)
		dist[i]=inf,boo[i]=0;
	
	dist[n]=0;
	q.push(make_pair(0,n));
		
	while(!q.empty())
	{
		int u=q.top().second;q.pop();
		if (boo[u]) continue;
		boo[u]=1;
		
		for (int i=head[u];i!=-1;i=next[i])
		{
			int v=rea[i],fee=val[i];
			if (dist[v]>dist[u]+fee&&!boo[v])
			{
				dist[v]=dist[u]+fee;
				q.push(make_pair(dist[v],v));
			}
		}
	}
	
	minn=dist[1]+k;
}
void dfs(int u,int now,int fa)
{
//	cout<<u<<" "<<now<<" "<<fa<<endl;
	if (u==n)ans++;
	priority_queue<fzy,vector<fzy>,greater<fzy> >q;
	for (int i=head[u];i!=-1;i=next[i])
	{
		int v=rea[i],fee=val[i];
		q.push(make_pair(now+fee+dist[v],v));
	}
	while(!q.empty())
	{
		int len=q.top().first,v=q.top().second;q.pop();
		if (len>minn) break;
		dfs(v,len-dist[v],u);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	int T;scanf("%d",&T);
	while(T--)
	{
		memset(head,-1,sizeof(head));cnt=ans=0;
		n=read(),m=read(),k=read(),p=read();
		for (int i=1;i<=m;i++)
		{
			a[i].x=read(),a[i].y=read(),a[i].z=read();
			add(a[i].y,a[i].x,a[i].z);
		}//反向连边。
		Dijkstra();
		
		memset(head,-1,sizeof(head));cnt=0;
		for (int i=1;i<=m;i++) add(a[i].x,a[i].y,a[i].z);//正向连边。 
		dfs(1,0,-1);
		
		ans=ans%p;
		printf("%lld\n",ans);
	}
}

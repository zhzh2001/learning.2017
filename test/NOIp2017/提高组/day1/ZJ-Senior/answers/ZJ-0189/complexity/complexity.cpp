#include <cstring>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <set>
using namespace std;
typedef long long ll;
typedef pair<int,int> P;
#define pb push_back
#define fi first
#define se second
const int maxn = 200;
int T,n,w,st;
char buf , be[maxn],ed[maxn];
string var[maxn];
bool flag,e;
int a[maxn],f[maxn];
set<string> s;
bool isd(char c){
  return c>='0' && c<='1';
}
int main(){
  freopen("complexity.in","r",stdin);
  freopen("complexity.out","w",stdout);
  scanf("%d",&T);
  while(T--){
    cin>>n>>buf>>buf>>buf;
    if(buf=='1') w=0;
    else{
      cin>>buf>>w;
    }
    cin>>buf; //)
    int dp=0;
    e=0;
    s.clear();
    memset(f,0,sizeof f);
    while(n--){
      cin>>buf;
      if(buf=='F'){
        cin>>var[dp]>>be>>ed;
	if(s.count(var[dp])) e=1;
	else s.insert(var[dp]);
	if(!e){
	  if((be[0]!='n') && (ed[0]=='n')){
	    a[dp] = 1;
	  }else if((be[0]=='n') && (ed[0]!='n')){
	    a[dp] = -1;
	  }else a[dp] = 0;
	  dp++;
	}
      }else if(buf=='E'){
	if(dp>0){
	  dp--;
	  if(a[dp]>=0)
	    f[dp] = max(f[dp],a[dp]+f[dp+1]);
	  s.erase(var[dp]);
	}else{
	  e=1;
	}
      }
    }
    if(dp) e=1;
    if(e) puts("ERR");
    else puts(f[0] == w ? "Yes":"No"); 
  }
  return 0;
}

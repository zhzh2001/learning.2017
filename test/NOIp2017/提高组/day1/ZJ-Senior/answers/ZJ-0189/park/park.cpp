#include <cstring>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <set>
#include <vector>
#include <queue>
using namespace std;
typedef long long ll;
typedef pair<int,int> P;
//#define P(x) cout<<#x<<" = "<<x<<endl;
#define pb push_back
#define fi first
#define se second
#define mp make_pair 
const int maxn = 100010;
vector<int> to[maxn],l[maxn],rto[maxn],rl[maxn];
priority_queue<P, vector<P>, greater<P> > q;
int d[maxn],f[maxn][51];
int t,n,m,k,p;
int u,v,w;
void dij(){
  memset(d,0x3f,sizeof d);
  d[n]=0;
  q.push(mp(0,n));
  while(!q.empty()){
    P u = q.top(); q.pop();
    for(int i=0;i<to[u.se].size();i++){
      if(d[u.se] + l[u.se][i] < d[to[u.se][i]]){
	d[to[u.se][i]] = d[u.se] + l[u.se][i];
	q.push(mp(d[to[u.se][i]], to[u.se][i]));
      }
    }
  }
}
bool flag;
int cnt,vis[maxn],fin[maxn],low[maxn];
void tarjan(int u){
  if(flag) return;
  vis[u] = cnt++;
  low[u] = vis[u]+1;
  for(int i=0;i<to[u].size();i++){
    if(l[u][i]) continue;
    if(vis[to[u][i]]){
      if(!fin[to[u][i]]) low[u] = min(low[u],vis[to[u][i]]);
      continue;
    }
    tarjan(to[u][i]);
    low[u] = min(low[u], low[to[u][i]]);
  }
  if(low[u]<=vis[u]) {
    flag=true;
    return;
  }
  fin[u] = 1;
}
bool judge(){
  if(!flag) return true;
  flag=false;
  for(int i=1;i<=n;i++){
    if(!vis[i]){
      tarjan(i);
    }
  }
  return !flag;
}
int dvis[maxn][51];
int dp(int u,int i){
  int &g=f[u][i];
  if(dvis[u][i]) return g;
  dvis[u][i] = 1;
  for(int j=0;j<rto[u].size();j++){
    int of = - d[rto[u][j]] + d[u] - rl[u][j];
    if(i+of>k || i+of<0) continue;
    g+=dp(rto[u][j],i+of);
  }
  return g;
}
void rez(vector<int>* v){
  for(int i=1;i<=n;i++) v[i].resize(0);
}
int main(){
  freopen("park.in","r",stdin);
  freopen("park.out","w",stdout);
  cin>>t>>n>>m>>k>>p;
  while(t--){
    rez(to); rez(l); rez(rto); rez(rl);
  flag=false;
  for(int i=0;i<m;i++){
    cin>>u>>v>>w;
    to[v].pb(u);
    l[v].pb(w);
    rto[u].pb(v);
    rl[u].pb(w);
    if(!w) flag=true;
  }
  dij();
  cnt=1;
  memset(vis,0,sizeof vis);
  memset(fin,0,sizeof fin);
  memset(low,0,sizeof low);
  memset(dvis,0,sizeof dvis);
  if(judge()){
    f[n][0] = 1;
    dvis[n][0] = 1;
    for(int i=1;i<=n;i++){
      for(int j=0;j<=k;j++){
	dp(i,j);
      }
    }
    int ans=0;
    for(int j=0;j<=k;j++){
      ans = (ans+f[1][j]) % p;
    }
    cout<<ans<<endl;
  }else cout<<-1<<endl;
  }  // for(int i=1;i<=n;i++) cout<<d[i]<<' '; cout<<endl;
  return 0;
}

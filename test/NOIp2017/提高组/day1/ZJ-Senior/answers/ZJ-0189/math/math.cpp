#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long ll;
typedef pair<int,int> P;
//#define P(x) cout<<#x<<" = "<<x<<endl;
#define pb push_back
#define fi first
#define se second
ll a,b,d;
ll gcd(ll x,ll y){
  if(y==0) return x;
  return gcd(y,x%y);
}
int main(){
  freopen("math.in","r",stdin);
  freopen("math.out","w",stdout);
  cin>>a>>b;
  d=gcd(a,b);
  a/=d;
  b/=d;
  if(a<b) swap(a,b);
  cout<<(a*b-a-b)*d<<endl;
  return 0;
}

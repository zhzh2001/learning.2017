#include<cstdio>
#include<cstring>
int t,n,mi;
char s[2100],a[200],b[200],noyong[200];
bool use[129];
int stack[2100];
char zf[2100];
int top;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for(scanf("%d",&t);t--;){
		scanf("%d%s",&n,s+1);
		fgets(noyong,200,stdin);
		if(s[3]!='n')mi=0;else{
			if(s[4]=='^'){
				mi=0;
				for(int i=5;s[i]!=')';++i)
				mi=(mi<<3)+(mi<<1)+(s[i]^'0');
			}else
			mi=1;
		}
		int mx=0,ans=0;
		top=0;
		bool ok=true;
		memset(use,0,sizeof use);
		use['n']=true;
		int cant=0;
		for(int i=1;i<=n;++i){
			fgets(s,100,stdin);
			if(!ok)continue;
			if(s[0]=='F'){
				if(use[s[2]]){
					ok=false;
					continue;
				}
				use[s[2]]=true;
				int p=0;
				sscanf(s+3,"%s%s",a,b);
				if(a[0]!='n'&&b[0]=='n')
				p=1;else
				if(a[0]=='n'&&b[0]!='n')
				p=-233;else{
					int x,y;
					sscanf(a,"%d",&x);
					sscanf(b,"%d",&y);
					if(x>y)p=-233;
				}
				if(p>-233){
					if(!cant)
					ans+=p;
				}else ++cant;
				stack[++top]=p;
				zf[top]=s[2];
				if(mx<ans)mx=ans;
			}else
			if(s[0]=='E'){
				if(!top){
					ok=false;
					continue;
				}
				use[zf[top]]=false;
				int p=stack[top--];
				if(p==-233)--cant;else
				if(!cant)
				ans-=p;
			}
		}
		if(!ok||top)puts("ERR");else
		if(mx==mi)puts("Yes");else
		puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<cstring>
#define reg register
int n,m,k,P,head[100005],d[100005],ct[100005];
bool vis[100005];
int q[700005];
int ans[100005][52],sta[100005];
struct edge{
	int to,nxt,dis;
}e[400005];
inline int readint(){
	char c=getchar();
	for(;!isdigit(c);c=getchar());
	reg int d=0;
	for(;isdigit(c);c=getchar())
	d=(d<<3)+(d<<1)+(c^'0');
	return d;
}
bool spfa(){
	memset(ct,0,sizeof ct);
	memset(d,0x3f,sizeof d);
	d[1]=0;
	memset(vis,1,sizeof vis);
	vis[1]=false;
	reg int l=0,r=1;
	q[1]=1;
	while(l!=r){
		reg int u=q[l=l%700000+1];
		vis[u]=true;
		for(reg int i=head[u];i;i=e[i].nxt){
			reg int v=e[i].to;
			if(d[v]>=d[u]+e[i].dis){
				d[v]=d[u]+e[i].dis;
				if(vis[v]){
					vis[v]=false;
					q[r=r%700000+1]=v;
					if(++ct[v]==n)return false;
				}
			}
		}
	}
	return true;
}
void bfs(){
	reg int sz=sizeof ans[0];
	memset(ans[0],0,sz);
	memset(ans[n+1],0,sz);
	ans[1][0]=1;
	memset(vis,1,sizeof vis);
	sta[1]=0;
	vis[1]=false;
	reg int l=0,r=1;
	q[1]=1;
	while(l!=r){
		reg int u=q[l=l%700000+1];
		vis[u]=true;
		for(reg int i=head[u];i;i=e[i].nxt){
			reg int v=e[i].to;
			reg bool p=false;
			if(vis[v]&&v!=n+1)memset(ans[v],0,sz),sta[v]=0x3f3f3f3f;
			reg int yy=d[u]+e[i].dis-d[v];
			reg int xx=yy+sta[u];
			if(xx<sta[v])sta[v]=xx;
			for(reg int j=sta[u];;++j){
				reg int s=yy+j;
				if(s>k)break;
				if(ans[u][j])
				ans[v][s]=(ans[v][s]+ans[u][j])%P,p=true;
			}
			if(p&&vis[v]){
				vis[v]=false;
				q[r=r%700000+1]=v;
			}
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(reg int t=readint();t--;){
		reg int cnt=0;
		n=readint(),m=readint(),k=readint(),P=readint();
		memset(head,0,sizeof head);
		while(m--){
			reg int u=readint(),v=readint(),t=readint();
			e[++cnt]=(edge){v,head[u],t};
			head[u]=cnt;
		}
		e[++cnt]=(edge){n+1,head[n],0};
		head[n]=cnt;
		if(!spfa()){
			puts("-1");
			continue;
		}
		bfs();
		reg int all=0;
		for(reg int i=0;i<=k;++i)
		all=(all+ans[n+1][i])%P;
		printf("%d\n",all);
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
int a,b,cnt=1;
long long ans;
bool mark[20000005];
struct water {
	void exgcd(int A,int B,int &x,int &y) {
		if(!B) {
			y=0,x=1;
		} else {
			exgcd(B,A%B,y,x);
			y-=(A/B)*x;
		}
	}
	bool check(long long mid,int x,int y) {
		int l=mid,r=mid;
		long long res=0;
		while(l>=0&&mid-l<a) {
			long long tmp=l*x;
			tmp=(tmp%b+b)%b;
			if(l-tmp*a<0) {
				res=l;
				if(res>ans)ans=res;
				break;
			}
			l--;
		}
		if(mid-l>=a)return 1;
		if(res!=0)l++;
		while(1) {
			long long tmp=r*x;
			tmp=(tmp%b+b)%b;
			if(r-tmp*a<0) {
				res=r;
				if(res>ans)ans=res;
				break;
			}
			if(r-l+1>=a)return 1;
			r++;
		}
		if(r-l+1>=a)return 1;
		return 0;
	}
	void solve(){
		int x,y;
		exgcd(a,b,x,y);
		long long L=0,R=a*b;
		while(L<=R) {
			int mid=(L+R)>>1;
			if(check(mid,x,y)) {
				R=mid-1;
			} else L=mid+1;
		}
	}
}shui;
//long long 取模 乘法溢出 内存 文件名
int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d %d",&a,&b);
	if(a>b)swap(a,b);
	mark[0]=1;
	for(int i=1; i<=20000000; i++) {
		bool f=0;
		if(i-a>=0&&mark[i-a]>0) {
			mark[i]=1;
			f=1;
		}
		if(i-b>=0&&mark[i-b]>0) {
			mark[i]=1;
			f=1;
		}
		if(f) {
			cnt++;
			if(cnt==a)break;
		} else {
			//printf("cnt=%d ans=%d\n",cnt,ans);
			cnt=0;
			ans=i;
		}
	}
	if(ans==20000000)shui.solve();
	printf("%lld\n",ans);
	return 0;
}

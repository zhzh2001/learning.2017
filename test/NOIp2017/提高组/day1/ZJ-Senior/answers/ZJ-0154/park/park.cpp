#include<cstdio>
#include<vector>
#include<queue>
#include<cstring>
using namespace std;
typedef pair<int,int>P;
int M;
struct edge {
	int to,cost;
} e;
struct bian {
	int a,b,c;
} A[200006];
vector<edge>G[100005],rG[100005];
int d[100005],ans,k,n,m;
int t[100005];
bool mark[100005];
void dj(int s) {
	memset(d,63,sizeof(d));
	memset(mark,0,sizeof(mark));
	d[s]=0;
	priority_queue<P,vector<P>,greater<P> >Q;
	Q.push(P(d[s],s));
	while(!Q.empty()) {
		P p=Q.top();
		Q.pop();
		int v=p.second;
		if(mark[v])continue;
		mark[v]=1;
		for(int i=0; i<(int)G[v].size(); i++) {
			int y=G[v][i].to;
			if(d[y]>d[v]+G[v][i].cost) {
				d[y]=d[v]+G[v][i].cost;
				Q.push(P(d[y],y));
			}
		}
	}
}
void dj2(int s) {
	memset(t,63,sizeof(t));
	memset(mark,0,sizeof(mark));
	t[s]=0;
	priority_queue<P,vector<P>,greater<P> >Q;
	Q.push(P(t[s],s));
	while(!Q.empty()) {
		P p=Q.top();
		Q.pop();
		int v=p.second;
		if(mark[v])continue;
		mark[v]=1;
		for(int i=0; i<(int)rG[v].size(); i++) {
			int y=rG[v][i].to;
			if(t[y]>t[v]+rG[v][i].cost) {
				t[y]=t[v]+rG[v][i].cost;
				Q.push(P(t[y],y));
			}
		}
	}
}
struct p10 {
	void dfs(int x,int sum) {
		if(sum+t[x]>d[n]+k)return;
		if(x==n) {
			ans++;
			ans%=M;
		}
		for(int i=0; i<(int)G[x].size(); i++) {
			int y=G[x][i].to;
			dfs(y,sum+G[x][i].cost);
		}
	}
	void solve() {
		ans=0;
		dfs(1,0);
		printf("%d\n",ans);
	}
} p10;
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T,a,b,c;
	scanf("%d",&T);
	while(T--) {
		scanf("%d %d %d %d",&n,&m,&k,&M);
		for(int i=1; i<=n; i++)G[i].clear();
		for(int i=1; i<=m; i++) {
			scanf("%d %d %d",&a,&b,&c);
			e.to=b,e.cost=c;
			G[a].push_back(e);
			e.to=a;
			rG[b].push_back(e);
			A[i].a=a,A[i].b=b,A[i].c=c;
		}
		dj(1);
		dj2(n);
		p10.solve();
		//printf("%d\n",d[n]);
	}
	return 0;
}

#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
int L;
char op[10];
char st[5],bl[5],l[5],r[5];
queue<int>chk;
bool mark[50];
int cg[105];
int check(char ch[]) {
	if(ch[0]=='n')return 1000;
	int len=strlen(ch);
	int res=0;
	for(int i=0; i<len; i++) {
		res=res*10+ch[i]-'0';
	}
	return res;
}
int judge() {
	if(op[2]=='1')return 1;
	else {
		int res=0,i=4;
		while(op[i]!=')') {
			res=res*10+op[i]-'0';
			i++;
		}
		return res+1000;
	}
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		memset(mark,0,sizeof(mark));
		memset(cg,0,sizeof(cg));
		while(!chk.empty())chk.pop();
		scanf("%d %s",&L,op);
		int cnt=0,mx=0;
		bool f=1,o1=1;
		for(int i=1; i<=L; i++) {
			scanf("%s",st);
			if(st[0]=='F') {
				cnt++;
				scanf("%s %s %s",bl,l,r);
				if(f) {
					int tp=bl[0]-'a';
					if(mark[tp])f=0;
					mark[tp]=1;
					chk.push(tp);
					int a=check(l);
					int b=check(r);
					if(a<b) {
						if(a>100||b>100) {
							if(cnt>0) {
								if(cg[cnt-1]==-1)cg[cnt]=-1;
								else {
									o1=0;
									cg[cnt]=cg[cnt-1]+1;
									if(mx<cg[cnt])mx=cg[cnt];
								}
							}
						}else {
							if(cnt>0)cg[cnt]=cg[cnt-1];
						}
					} else if(a>b) {
						cg[cnt]=-1;
					}else cg[cnt]=cg[cnt-1];
				}
			} else if(st[0]=='E') {
				cnt--;
				if(cnt<0)f=0;
				if(f) {
					int tp=chk.front();
					chk.pop();
					mark[tp]=0;
				}
			}
		}
		if(cnt!=0||f==0) {
			printf("ERR\n");
			continue;
		}
		int pt=judge();
		//printf("mx=%d\n",mx);
		if(o1) {
			if(pt==1)printf("Yes\n");
			else printf("No\n");
		} else {
			if(pt==1)printf("No\n");
			else if((pt%100)!=mx)printf("No\n");
			else printf("Yes\n");
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define cl(x,y) memset(x,y,sizeof(x))
using namespace std;

const int maxn=105;
int tst,L,tot,sum;
char O[maxn],ANS[maxn],tem[maxn];
bool vis[128];
inline bool isnum(char *s) {return '0'<=s[0]&&s[0]<='9';}
bool numcmp(char *a,char *b){
	int n=strlen(a),m=strlen(b);
	if (n<m) return 1;
	if (n>m) return 0;
	return strcmp(a,b)<=0;
}
int work(bool doit){
	char i[maxn],x[maxn],y[maxn],tem[maxn];
	scanf("%s%s%s",i,x,y);tot++;
	bool numx=isnum(x),numy=isnum(y);
	if (numx&&numy&&!numcmp(x,y)||(numy&&!numx)) doit=0;
	scanf("%s",tem);int ans=0;
	while (tem[0]!='E'){
		ans=max(ans,work(doit));
		scanf("%s",tem);
	}tot++;
	if (!doit) return 0;else{
		return ans+(numx&&!numy);
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&tst);
	while (tst--){
		tot=sum=0;cl(vis,0);
		scanf("%d%s",&L,O);
		int t=0;
		while (tot<L) scanf("%s",tem),t=max(t,work(1));
		if (t>0) sprintf(ANS,"O(n^%d)",t);else sprintf(ANS,"O(1)");
		if (strcmp(O,ANS)==0) printf("Yes\n");else printf("No\n");
		
//		printf("%d %d ",t,L);printf("%s %s %s\n",O,ANS,tem);
//		if (tst==2){
//			char ch=getchar();
//			while (ch!=EOF) putchar(ch),ch=getchar();
//			return 0;
//		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#define cl(x,y) memset(x,y,sizeof(x))
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int red(){
	int res=0,f=1;char ch=nc();
	while (ch<'0'||'9'<ch) {if (ch=='-') f=-f;ch=nc();}
	while ('0'<=ch&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}

const int maxn=100005,maxe=200005,INF=0x3f3f3f3f,lim=200000;
int tst,n,e,K,tt;
namespace A{
	int tot,son[maxe],nxt[maxe],lnk[maxn],w[maxe];
	inline void add(int x,int y,int z){
		son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;w[tot]=z;
	}
}
namespace B{
	int tot,son[maxe],nxt[maxe],lnk[maxn],w[maxe];
	inline void add(int x,int y,int z){
		son[++tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;w[tot]=z;
	}
}
int dst[maxn],que[maxn],ans,d;
bool vis[maxn];
void spfa(){
	using namespace B;
	int hed=0,til=1;
	cl(dst,63);cl(vis,0);
	que[1]=n;dst[n]=0;
	while (hed!=til){
		int x=que[hed=(hed+1)%maxn];
		vis[x]=0;
		for (int j=lnk[x];j;j=nxt[j])
		 if (dst[son[j]]>dst[x]+w[j]){
		 	dst[son[j]]=dst[x]+w[j];
		 	if (!vis[son[j]])
		 	 vis[son[j]]=1,que[til=(til+1)%maxn]=son[j];
		 }
	}
}
void dfs(int x,int sum){
	using namespace A;
	if (sum+dst[x]>d||ans>lim) return;
	if (x==n) ans++;
	for (int j=lnk[x];j;j=nxt[j])
	 dfs(son[j],sum+w[j]);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	tst=red();
	while (tst--){
		n=red(),e=red(),K=red(),tt=red();
		cl(A::lnk,0);A::tot=0;cl(B::lnk,0);B::tot=0;
		for (int i=1,x,y,z;i<=e;i++) x=red(),y=red(),z=red(),A::add(x,y,z),B::add(y,x,z);
		spfa();
		if (dst[1]==INF) {printf("0\n");continue;}
		d=dst[1]+K;
		ans=0;dfs(1,0);
		if (ans>lim) printf("-1\n");else printf("%d\n",ans%tt);
	}
	return 0;
}

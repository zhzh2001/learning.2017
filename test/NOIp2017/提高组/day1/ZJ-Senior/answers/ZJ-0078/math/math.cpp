#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;

int a,b,n,ans;
bool f[100000000];
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);n=max(a*b,1000000);
	f[0]=1;
	for (int i=1;i<=n;i++){
		if (i>=a) f[i]|=f[i-a];
		if (i>=b) f[i]|=f[i-b];
	}
	for (int i=n;i;i--) if (!f[i]) {ans=i;break;}
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int T,n,tot,len,k,kk,tf,te,ptot;
char s[13],ss[111][3],s1[111][3],s2[111][3],s3[111][3],s4[111][3];
bool pp,p;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		kk=0;tot=0;pp=0;k=0;te=0;tf=0;ptot=0;
		scanf("%d",&n);
		scanf("%s",s);
		len=strlen(s);
		if (len==4) k=0;
		else{
			for (int i=4;i<len-1;i++) k=k*10+s[i]-'0';
		}
		for (int i=1;i<=n;i++){
			scanf("%s",s1[i]);
			if (s1[i][0]=='E'){
				if (tot==0) pp=1;
				tot--;
			}else{
				scanf("%s",s2[i]);
				scanf("%s",s3[i]);
				scanf("%s",s4[i]);
				for (int j=1;j<=tot;j++) if (ss[j][0]==s2[i][0]) {pp=1;break;}
				ss[++tot][0]=s2[i][0];
			}
		}
		if (pp) {printf("ERR\n");continue;}
		if (tot!=0) {printf("ERR\n");continue;}
		for (int i=1;i<=n;i++){
			if (s1[i][0]=='E'){
				if (!ptot&&p) tot--;
				if (ptot) te++;
				if (tf+1==te) ptot=0,te=0,tf=0;
			}else if (!ptot) {
				p=1;
				if (s3[i][0]=='n'){if (s4[i][0]!='n') ptot=1;else p=0;}
				else if (s4[i][0]!='n'){
					int xx1=0,xx2=0;
					for (int j=0;j<strlen(s3[i]);j++) xx1=xx1*10+s3[i][j]-'0';
					for (int j=0;j<strlen(s4[i]);j++) xx2=xx2*10+s4[i][j]-'0';
					if (xx1>xx2) ptot=1; else p=0;
				}
				if (!ptot&&p) kk=max(kk,++tot);
			}else tf++;
		}
		if (kk==k) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

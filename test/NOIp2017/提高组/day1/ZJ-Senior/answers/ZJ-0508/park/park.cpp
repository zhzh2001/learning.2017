#include<bits/stdc++.h>
using namespace std;

inline void read(int &x){
	x=0; static bool fff=0; static char c=getchar();
	for (;!(c>='0'&&c<='9'); c=getchar()) if (c=='-') fff=1;
	for (; (c>='0'&&c<='9'); c=getchar()) x=(x<<3)+(x<<1)+c-'0';
	if (fff) x=-x;
}

struct R{
	int to,val,nex;
}r[201000];
struct E{
	int u,v,w;
}e[201000];
int T,n,m,hea[101000],cnt,K,mod,d[101000],inf,d1[101000];
int ok[101000][52],f[101000][52],aim,deg[101000][52];

inline void U(int &x,int y){
	if ((x+=y)>=mod) x-=mod;
}

struct P{
	int who,dis;
}u,v;
struct cmp{
	bool operator ()(P p1,P p2){
		return p1.dis>p2.dis;
	}
};
priority_queue<P,vector<P>,cmp>q;

void Dj(int st){
	memset(d,63,sizeof d); inf=d[233];
	while (!q.empty()) q.pop();
	d[st]=0; q.push((P){st,0});
	for (register int i;!q.empty();){
		u=q.top(); q.pop();
		if (u.dis!=d[u.who]) continue;
		for (i=hea[u.who];i>0;i=r[i].nex){
			v.who=r[i].to; v.dis=u.dis+r[i].val;
			if (v.dis<d[v.who]){
				d[v.who]=v.dis;
				q.push(v);
			}
		}
	}
}

queue<P>que;

int main(){
	freopen("park.in","r",stdin); freopen("park.out","w",stdout);
	
	for (read(T);T--;){
		register int i,j,x,y,z;
		
		read(n); read(m); read(K); read(mod);
		memset(hea,0,sizeof hea); cnt=0;
		
		for (i=1;i<=m;i++){
			read(x); read(y); read(z);
			e[i]=(E){x,y,z};
			r[++cnt]=(R){y,z,hea[x]}; hea[x]=cnt;
		}
		Dj(1);
		memcpy(d1,d,sizeof d);
		
		memset(hea,0,sizeof hea); cnt=0;
		for (i=1;i<=m;i++){
			x=e[i].v; y=e[i].u; z=e[i].w;
			r[++cnt]=(R){y,z,hea[x]}; hea[x]=cnt;
		}
		Dj(n);
		
		memset(hea,0,sizeof hea); cnt=0;
		for (i=1;i<=m;i++){
			x=e[i].u; y=e[i].v; z=e[i].w;
			r[++cnt]=(R){y,z,hea[x]}; hea[x]=cnt;
		}
		
		memset(ok,0,sizeof ok); aim=0;
		for (i=1;i<=n;i++) if (d1[i]!=inf)
			for (j=0;j<=K;j++) if (d1[i]+j+d[i]<=d1[n]+K)
				aim+=(ok[i][j]=1);
		memset(deg,0,sizeof deg);
		for (x=1;x<=n;++x){
			for (y=0;ok[x][y];++y){
				z=d1[x]+y;
				for (i=hea[x];i>0;i=r[i].nex) if ( z+r[i].val <= d1[r[i].to]+K ){
					++deg[r[i].to][z+r[i].val-d1[r[i].to]];
				}
			}
		}
		
		if (deg[1][0]) {
			puts("-1");
			continue;
		}
		memset(f,0,sizeof f);
		f[1][0]=1;
		while (!que.empty()) que.pop();
		for (x=1;x<=n;++x)
			for (y=0;ok[x][y];++y)
				if (!deg[x][y]) que.push((P){x,y});
		for (;!que.empty();){
			u=que.front(); que.pop(); z=d1[u.who]+u.dis; aim--;
			for (i=hea[u.who];i>0;i=r[i].nex) if ( z+r[i].val <= d1[r[i].to]+K ){
				v.who=r[i].to; v.dis=z+r[i].val-d1[r[i].to];
				if (ok[v.who][v.dis]){
					U(f[v.who][v.dis],f[u.who][u.dis]);
					if (!--deg[v.who][v.dis]) que.push(v);
				}
			}
		}
		if (aim>0) {
			puts("-1");
			continue;
		}
		int ans=0;
		for (i=0;i<=K;i++) U(ans,f[n][i]);
		printf("%d\n",ans);
	}
	
	fclose(stdin); fclose(stdout); return 0;
}

#include<bits/stdc++.h>
using namespace std;

int T,n,l,die,cnt,mx,aim,fl;
char s[23],ss[23];
int st[2333],top=0,used[233];
char stc[2333];

bool isn(char t[]){
	return strlen(t)==1&&t[0]=='n';
}
int num(char t[]){
	if (isn(t)) return 23333;
	int res=0;
	for (int i=0;i<strlen(t);i++) res=res*10+t[i]-'0';
	return res;
}

void in(){
	top++;
	scanf("%s",s);
	stc[top]=s[0];
	scanf("%s%s",s,ss);
	if (num(s)>num(ss)) {
		st[top]=-1;
		return;
	}
	if (!isn(s)&&isn(ss)) st[top]=1;
	else st[top]=0;
}

void realmain(){
	die=0,cnt=0,mx=0,top=0,fl=0;
	memset(used,0,sizeof used);
	scanf("%d%s",&n,s);
	l=strlen(s);
	
	if (l>7) die=1;
	else if (l<6) aim=0;
	else if (l==6) aim=s[4]-'0';
	else if (l==7) aim=(s[4]-'0')*10+s[5]-'0';
	
	for (int i=1;i<=n;i++){
		scanf("%s",s);
		if (s[0]=='F'){
			in();
			cnt+=st[top];
			if (st[top]==-1) fl++;
			if (used[stc[top]]) die=1;
			used[stc[top]]=1;
			if (!fl) mx=max(mx,cnt);
		}else{
			cnt-=st[top];
			if (st[top]==-1) fl--;
			used[stc[top]]=0;
			top--;
			if (top<0) {
				die=1;
				top=233;
			}
		}
	}
	if (top) die=1;
	if (die){ puts("ERR"); return; }
	puts(mx==aim? "Yes": "No");
}

int main(){
	freopen("complexity.in","r",stdin); freopen("complexity.out","w",stdout);
	
	for (scanf("%d",&T);T--;) realmain();
	
	fclose(stdin); fclose(stdout); return 0;
}

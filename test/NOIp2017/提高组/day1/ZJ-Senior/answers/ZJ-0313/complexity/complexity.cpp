#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<iostream>
	using namespace std;

int L,lenc;
char s[25],c[25];
int h[105][3],top;	
bool bo[30],boh[105];
int res,sum,maxmum;
	
int main(){
	
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%s",&L,s+1);
		if(s[3]=='1'){
			res=0;
		}
		else{
			int j;
			j=4;
			res=0;
			while(s[j+1]!=')'){
				j++;
				res=res*10+s[j]-'0';
			}
		}
		char xa;
		scanf("%c",&xa);
		bool w;
		w=0;
		top=0;
		for(int i=1;i<=26;i++) bo[i]=0;
		for(int i=0;i<=101;i++){
			boh[i]=0;
		}
		sum=0;
		maxmum=0;
		for(int i=1;i<=L;i++){
			gets(c+1);
			lenc=strlen(c+1);
			if(w==1) continue ;
			if(c[1]=='F'){
				if(bo[c[3]-'a'+1]==1) w=1;
				if(w==1) continue ;
				bo[c[3]-'a'+1]=1;
				top++;
				h[top][0]=c[3]-'a'+1;
				if(boh[top-1]==1){
					boh[top]=1;
					continue ;
				}
				int a,b,j;
				if(c[5]=='n'){
					a=-1;
					j=7;
				}
				else{
					a=0;
					j=4;
					while(c[j+1]!=' '){
						j++;
						a=a*10+c[j]-'0';
					}
					j=j+2;
				}
				if(c[j]=='n'){
					b=-1;
				}
				else{
					b=0;
					j--;
					while(j+1<=lenc){
						j++;
						b=b*10+c[j]-'0';
					}
				}
				if(a==-1){
					if(b==-1) h[top][1]=0;
					else boh[top]=1;
					if(boh[top]==1) continue ;
				}
				else{
					if(b==-1) h[top][1]=1;
					else{
						if(a<=b) h[top][1]=0;
						else boh[top]=1;
					}
					if(boh[top]==1) continue ;
				}
				if(boh[top]==1) continue ;
				sum=sum+h[top][1];
				maxmum=max(maxmum,sum);
			}
			else{
				if(w==1) continue ;
				bo[h[top][0]]=0;
				if(boh[top]==0){
					sum=sum-h[top][1];
				}
				boh[top]=0;
				top--;
				if(top<0) w=1;
			}
		}
		if(top>0) w=1;
		if(w==1) printf("ERR\n");
		else{
			if(res==maxmum) printf("Yes\n");
			else printf("No\n");
		}
		
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}
/*
1
16 O(n^2)
F x 1 1
F y 1 1
F z 1 n
F w 1 n
E
E
E
E
F x 1 n
F y 1 n
F z 1 1
F w 1 1
E
E
E
E
*/

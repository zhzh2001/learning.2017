#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<iostream>
	using namespace std;
	
int n,m,k,p,res;
int tot,head[100005],to[200005],v[200005],Next[200005];
int ftot,fhead[100005],fto[200005],fv[200005],fNext[200005];
int dis[100005];
int st,ed,h[1000005],inh[100005];

void add(int xa,int yb,int zc){
	tot++;
	to[tot]=yb;
	v[tot]=zc;
	Next[tot]=head[xa];
	head[xa]=tot;
	return ;
}

void fadd(int xa,int yb,int zc){
	ftot++;
	fto[ftot]=yb;
	fv[ftot]=zc;
	fNext[ftot]=fhead[xa];
	fhead[xa]=ftot;
	return ;
}

void dfs(int xa,int cost){
	if(cost+dis[xa]>dis[1]+k) return ;
	if(xa==n){
		res++;
		if(res>p) res=res-p;
	}
	for(int i=head[xa];i!=-1;i=Next[i]){
		int yb,zc;
		yb=to[i];
		zc=v[i];
		dfs(yb,cost+zc);
	}
	return ;
}
	
int main(){
	
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		tot=-1;
		ftot=-1;
		for(int i=1;i<=n;i++){
			head[i]=-1;
			fhead[i]=-1;
		}
		for(int i=1;i<=m;i++){
			int xa,yb,zc;
			scanf("%d%d%d",&xa,&yb,&zc);
			add(xa,yb,zc);
			fadd(yb,xa,zc);
		}
		for(int i=1;i<=n;i++){
			dis[i]=1000000005;
			inh[i]=0;
		}
		dis[n]=0;
		st=0;
		ed=1;
		h[ed]=n;
		inh[n]=1;
		while(st<ed){
			st++;
			int xa;
			xa=h[st];
			inh[xa]=0;
			for(int i=fhead[xa];i!=-1;i=fNext[i]){
				int yb,zc;
				yb=fto[i];
				zc=fv[i];
				if(dis[yb]>dis[xa]+zc){
					dis[yb]=dis[xa]+zc;
					if(inh[yb]==0){
						ed++;
						h[ed]=yb;
						inh[yb]=1;
					}
				}
			}
		}
		res=0;
		dfs(1,0);
		printf("%d\n",res);
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}
/*
1
5 4 1 2
1 2 1
2 3 2
3 4 3
4 5 4
*/

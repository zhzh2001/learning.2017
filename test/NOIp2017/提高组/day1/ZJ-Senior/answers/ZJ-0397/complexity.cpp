#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
ll read(){
	char ch=getchar(),last=' '; ll ans=0;
	while (ch>'9'||ch<'0') last=ch,ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-'0',ch=getchar();
	if (last=='-') ans=-ans; return ans;
}
char p[200],chh[200],ch[200];
int fl[200],a[200],b[200],h[200],tag[200];
void solve(){
	int l=read(),x=0,w=0,len=0,ans=0,top=0; 
	char str[20]; scanf("%s",str); len=strlen(str);
	while (w<len) { if (str[w]=='n') break; w++;}
	if (w<len) {w+=2; while (w<len && str[w]<='9' && str[w]>='0') x=x*10+str[w]-'0',w++;}
	for (int i=1;i<=l;i++){
		scanf("%s",str); p[i]=str[0];
		if (p[i]=='F'){
			scanf("%s",str); chh[i]=str[0];
			scanf("%s",str);
			if (str[0]=='n') a[i]=0;
			else {
				w=0; a[i]=0; len=strlen(str); while (w<len && str[w]<='9' && str[w]>='0') a[i]=a[i]*10+str[w]-'0',w++;
			}
			scanf("%s",str);
			if (str[0]=='n') b[i]=0;
			else {
				w=0; b[i]=0; len=strlen(str); while (w<len && str[w]<='9' && str[w]>='0') b[i]=b[i]*10+str[w]-'0',w++;
			}
		}
		tag[i]=0,ch[i]=0;
	}
	memset(fl,0,sizeof(fl));
	for (int i=1;i<=l;i++){
		if (p[i]=='F'){
			ch[++top]=chh[i]-'a';
			if (fl[ch[top]]) { cout<<"ERR"<<endl; return ; }
			fl[ch[top]]=1; tag[top]=0;
			if (a[i]==0){
				if (b[i]==0) h[top]=0;
				else h[top]=-1;
			} else {
				if (b[i]==0) h[top]=1;
				else {
					if (a[i]>b[i]) h[top]=-1;
					else h[top]=0;
				}
			}
		} else {
			fl[ch[top]]=0;
			if (top==0) { cout<<"ERR"<<endl; return ; }
			if (top==1) { if (h[top]!=-1) ans=max(ans,tag[top]+h[top]); top--; }
			if (top>1) { if (h[top]!=-1) tag[top-1]=max(tag[top-1],tag[top]+h[top]); top--; }
		}
	}
	if (top>0) { cout<<"ERR"<<endl; return ; }
	if (ans==x) cout<<"Yes"<<endl;
	else cout<<"No"<<endl;
	return ;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while (T--) solve();
	return 0;
}

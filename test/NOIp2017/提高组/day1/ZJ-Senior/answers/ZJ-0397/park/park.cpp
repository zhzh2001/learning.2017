#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
ll read(){
	char ch=getchar(),last=' '; ll ans=0;
	while (ch>'9'||ch<'0') last=ch,ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-'0',ch=getchar();
	if (last=='-') ans=-ans; return ans;
}
#include<queue>
struct node{
	int v,i;
	friend bool operator <(node a,node b){
		return a.v>b.v;
	}
};
priority_queue<node> Q;
const int inf=1e09,N=100005,M=200005;
int v[M],Next[M],head[N],cost[M],num[N],dis[N],vis[N],e,KK,anss,NN;
void add(int a,int b,int c){
	v[++e]=b; Next[e]=head[a]; head[a]=e; cost[e]=c;
}
void dfs(int u,int ans){
	if (ans>dis[u]+KK) return ;
	if (u==NN) {
		anss++;
		return ;
	}
	for (int i=head[u];i;i=Next[i]){
		dfs(v[i],ans+cost[i]);
	}
}
void solve(){
	int n=read(),m=read(),k=read(),p=read(),Min=inf; KK=k;NN=n;
	memset(head,0,sizeof(head)); e=0;
	for (int i=1;i<=m;i++) {
		int a=read(),b=read(),c=read();
		add(a,b,c); Min=min(Min,c);
	}
	if (Min==0) {
		puts("-1");
		return;
	}
	for (int i=1;i<=n;i++) dis[i]=inf,num[i]=0,vis[i]=0;
	dis[1]=0; num[1]=1; Q.push((node){0,1});
	while (!Q.empty()){
		int u=Q.top().i; Q.pop();
		if (vis[u]) continue;
		for (int i=head[u];i;i=Next[i]){
			int v_=v[i];
			if (dis[v_]>dis[u]+cost[i]){
				dis[v_]=dis[u]+cost[i]; num[v_]=num[u];
				Q.push((node){dis[v_],v_});
			} else {
				if (dis[v_]==dis[u]+cost[i]) num[v_]+=num[u],num[v_]%=p;
			}
		}
		vis[u]=1;
	}
	if (k==0) { printf("%d\n",num[n]); return; }
	anss=0;
	dfs(1,0);
	cout<<anss<<endl;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while (T--) solve();
	return 0;
}

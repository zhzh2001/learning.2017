#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
ll read(){
	char ch=getchar(),last=' '; ll ans=0;
	while (ch>'9'||ch<'0') last=ch,ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-'0',ch=getchar();
	if (last=='-') ans=-ans; return ans;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a=read(),b=read();
	printf("%lld",a*b-a-b);
	return 0;
}

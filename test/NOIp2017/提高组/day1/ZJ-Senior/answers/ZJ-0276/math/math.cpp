#include<bits/stdc++.h>
using namespace std;
long long n,m,x,y,v,vv,t;
void exgcd(long long n,long long m,long long &x,long long &y)
{
	long long xx,yy;
	if (m==0) {x=1; y=0; return;}
	exgcd(m,n%m,x,y);
	xx=x; yy=y;	x=yy; y=(xx-yy*n/m);
}
int main()
{
freopen("math.in","r",stdin);
freopen("math.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	exgcd(n,m,x,y); 
	if (y>0) {t=x; x=y; y=t; t=n; n=m; m=t;}
	x=x%m; y=(1-n*x)/m; v=-y*min(n,m)*(m-1)-1;
	x-=m; y+=n; vv=-x*min(n,m)*(n-1)-1;
	v=min(v,vv);
	printf("%lld\n",v);
	return 0;	
}

#include<bits/stdc++.h>
using namespace std;
const int inf=0x3f3f3f;
int n,m,nn,mo,b[10000005],f[100005],ff[100005],x[200005],y[200005],z[200005],t[100005],ne[200005],nt[100005],nne[200005];
int ft[20005][2005],ti[100005],low[100005],tot,bb[100005],co[100005],boo,T;
inline void SPFA()
{
	int tt=1,ww=1;
	b[tt]=1;
	while (tt<=ww)
	{
		for (int i=t[b[tt]];i;i=ne[i])
		{
			if (f[y[i]]>f[b[tt]]+z[i])
			{
				ww++;
				b[ww]=y[i];
				f[y[i]]=f[b[tt]]+z[i];				
			}		
		}
		tt++;	
	}	
}
inline void SPFA2()
{
	int tt=1,ww=1;
	b[tt]=n;
	while (tt<=ww)
	{
		for (int i=nt[b[tt]];i;i=nne[i])
		{
			if (ff[x[i]]>ff[b[tt]]+z[i])
			{
				ww++;
				b[ww]=x[i];
				ff[x[i]]=ff[b[tt]]+z[i];				
			}		
		}
		tt++;	
	}	
}
inline void dfs(int k)
{
	ti[k]=++tot;
	for (int i=t[k];i;i=ne[i])
	if (z[i]==0)
	{
		if (ti[y[i]]==0) 
		{
			dfs(y[i]); 
			low[k]=min(low[k],low[y[i]]);
		}
		else low[k]=min(low[k],ti[y[i]]);
	}
	if (low[k]==ti[k])
	{
		int tt=1,ww=1;
		bb[tt]=1;
		while (tt<=ww)
		{
			for (int i=t[bb[tt]];i;i=ne[i])
			{
				if (low[y[i]]==low[k]&&co[i]==0)
				{
					ww++;
					bb[ww]=y[i];
					co[i]=1;	
				}		
			}
			tt++;	
		}			
	}	
}
inline int dfs2(int k,int g)
{
	if (g+ff[k]>nn) return 0;
	if (co[k]==1) boo=1;
	if (boo) return -1;
	if (k<=20000&&g<=2000&&ft[k][g]!=0) return ft[k][g];
	int ans;
	if (k==n) ans=1;else ans=0;
	for (int i=t[k];i;i=ne[i]) 	ans=(ans+dfs2(y[i],g+z[i]))%mo;
	if (k<=20000&&g<=2000) ft[k][g]=ans;
	return ans;
}
int main()
{
freopen("park.in","r",stdin);
freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		memset(t,0,sizeof(t));memset(nt,0,sizeof(nt));tot=0; memset(ti,0,sizeof(ti));
		memset(ft,0,sizeof(ft));memset(f,0,sizeof(f));memset(ff,0,sizeof(ff));
		scanf("%d%d%d%d",&n,&m,&nn,&mo);
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			ne[i]=t[x[i]]; t[x[i]]=i;	
			nne[i]=nt[y[i]]; nt[y[i]]=i;	
		}
		for (int i=2;i<=n;i++) f[i]=inf;
		SPFA();
		nn+=f[n];
		for (int i=1;i<n;i++) ff[i]=inf;
		SPFA2();
		for (int i=1;i<=n;i++) low[i]=inf;
		for (int i=1;i<=n;i++) if (ti[i]==0) dfs(i);
		printf("%d\n",dfs2(1,0));
	}
}

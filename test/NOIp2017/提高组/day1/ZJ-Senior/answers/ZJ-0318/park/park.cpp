#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1e5+5,N=2e5+5,K=55;
int cas,n,m,k,P;
struct Edge{
	int a,b,c;
	void read(){rd(a),rd(b),rd(c);}
}edge[N];

namespace P70{
	int head[M],to[N<<2],co[M<<2],nxt[N<<2],tot;
	inline void add(int a,int b,int c){
		to[++tot]=b;
		co[tot]=c;
		nxt[tot]=head[a];
		head[a]=tot;
	}
	struct node{
		int x,lv,v;
		bool operator <(const node &A)const{
			return v>A.v;
		}
	}T[M*K];
	int dis[M],dp[M][K],id[M][K];
	priority_queue<node>Q;
	bool mark[M][K];
	void Dij(){
		while(!Q.empty())Q.pop();
		dis[1]=0;
		Q.push((node){1,0,0});
		while(!Q.empty()){
			int x=Q.top().x,v=Q.top().v;
			Q.pop();
			for(int i=head[x];i;i=nxt[i]){
				int y=to[i],d=v+co[i];
				if(d<dis[y]){
					dis[y]=d;
					Q.push((node){y,0,d});
				}
			}
		}
		int cnt=0;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=k;j++)
				T[++cnt]=(node){i,j,dis[i]+j};
		sort(T+1,T+1+cnt);
		dp[1][0]=1;
		for(int i=cnt;i>=1;i--){
			int x=T[i].x,lv=T[i].lv,v=T[i].v;
			for(register int i=head[x];i;i=nxt[i]){
				register int y=to[i],d=v+co[i],LV=d-dis[y];
				if(LV<=k)dp[y][LV]=(dp[y][LV]+dp[x][lv])%P;
			}
		}
//		memset(mark,0,sizeof(mark));
//		dp[1][0]=1;
//		Q.push((node){1,0,0});
//		while(!Q.empty()){
//			register int x=Q.top().x,lv=Q.top().lv,v=Q.top().v;
//			Q.pop();
//			for(register int i=head[x];i;i=nxt[i]){
//				register int y=to[i],d=v+co[i],LV=d-dis[y];
//				if(LV<=k){
//					dp[y][LV]=(dp[y][LV]+dp[x][lv])%P;
//					if(!mark[y][LV]){
//						mark[y][LV]=1;
//						Q.push((node){y,LV,d});
//					}
//				}
//			}
//		}
	}
	void solve(){
		tot=0;
		memset(head,0,sizeof(head));
		memset(dis,63,sizeof(dis));
		memset(dp,0,sizeof(dp));
		for(int i=1;i<=m;i++){
			int a=edge[i].a,b=edge[i].b,c=edge[i].c;
			if(a==b)continue;
			add(a,b,max(1,c));
		}
		Dij();
		int ans=0;
		for(int i=0;i<=k;i++)ans=(ans+dp[n][i])%P;
		printf("%d\n",ans);
	}
}
int main(){
	srand(time(NULL));
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	rd(cas);
	while(cas--){
		int zero=0;
		rd(n),rd(m),rd(k),rd(P);
		for(int i=1;i<=m;i++){
			edge[i].read();
			if(edge[i].c==0)zero=1;
		}
		if(0);
		else if(zero==0)P70::solve();
		else P70::solve();
	}
	return 0;
}

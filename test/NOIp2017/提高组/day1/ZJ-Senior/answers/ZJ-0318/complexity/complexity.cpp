#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
int cas,n;
namespace P100{
	const int M=233;
	char str[M];
	struct node{
		char op[M],I[M],L[M],R[M];
		void read(){
			scanf("%s",op);
			if(op[0]=='F')scanf("%s%s%s",I,L,R);
		}
	}stk[M],T;
	int mark[M],F[M];
	int num(char *str){
		int len=strlen(str),res=0;
		for(int i=0;i<len;i++)
			if(str[i]>='0'&&str[i]<='9')
				res=res*10+str[i]-'0';
		return res;
	}
	void solve(){
		while(cas--){
			memset(mark,0,sizeof(mark));
			scanf("%d%s",&n,str);
			int ans=0,cant=0,now=0;
			for(int cs=1;cs<=n;cs++){
				T.read();
				if(T.op[0]=='F'){
					stk[++now]=T;
					if(mark[T.I[0]])cant=1;
					mark[T.I[0]]=1;
					if(T.L[0]=='n'&&T.R[0]=='n')F[now]=F[now-1];
					else if(T.L[0]!='n'&&T.R[0]=='n')F[now]=F[now-1]+1;
					else if(T.L[0]=='n'&&T.R[0]!='n')F[now]=-M;
					else if(T.L[0]!='n'&&T.R[0]!='n'){
						int a=num(T.L),b=num(T.R);
						if(a<=b)F[now]=F[now-1];
						else F[now]=-M;
					}
					MAX(ans,F[now]);
				}
				else{
					if(now>0)mark[stk[now].I[0]]=0;
					now--;
					if(now<0)cant=1;
				}
			}
			if(now>0)cant=1;
			if(cant)puts("ERR");
			else if(strlen(str)==4)puts(ans==0?"Yes":"No");
			else puts(ans==num(str)?"Yes":"No");
		}
	}
}


int main(){
	srand(time(NULL));
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	rd(cas);
	if(0);
	else P100::solve();
	
	return 0;
}

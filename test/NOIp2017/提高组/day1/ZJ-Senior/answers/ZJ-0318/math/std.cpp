#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
int a,b;
namespace P30{
	int mark[2000005];
	void solve(){
		int ans=0;
		mark[a]=mark[b]=1;
		for(int i=1;i<=a*b;i++){
			if(!mark[i])MAX(ans,i);
			else mark[i+a]=mark[i+b]=1;
		}
		printf("%d\n",ans);
	}
}
namespace P100{
	
	void solve(){
		if(a==1||b==1)puts("0");
		else printf("%lld\n",1ll*a*b-a-b);
	}
}
int main(){
	srand(time(NULL));
	freopen("math.in","r",stdin);
	freopen("std.out","w",stdout);
	rd(a),rd(b);
	P30::solve();
	
	return 0;
}

#include<cstdio>
#include<iostream>
#define inf 9999999
#define LL long long
using namespace std;
struct wakaka{
	int next,to,v;
}edge1[210000],edge2[210000];
LL ans,maxnn,dist[110000];
int bo,t,a,b,c,q[110000],n,m,k,p,tot1,tot2,head1[110000],head2[110000];
bool vis[110000];
void addedge1(int u,int v,int w){edge1[++tot1].next=head1[u];head1[u]=tot1;edge1[tot1].to=v;edge1[tot1].v=w;}
void addedge2(int u,int v,int w){edge2[++tot2].next=head2[u];head2[u]=tot2;edge2[tot2].to=v;edge2[tot2].v=w;}
void spfa(int u){
	dist[u]=0;vis[u]=0;
	int head=1;int tail=1;
	q[head]=u;
	while(head<=tail){
		int x=q[head];
		for(int i=head1[x];i;i=edge1[i].next){
		int y=edge1[i].to;
		if(dist[y]>=dist[x]+edge1[i].v){
			dist[y]=edge1[i].v+dist[x];
			if(!vis[y]){
				tail++;
				vis[y]=1;
				q[tail]=y;}
			} 
		}
		head++;
	}
}
void dfs(int x,LL sum){
	if(x==n){ans=(ans+1)%p;return;}
	for(int i=head2[x];i;i=edge2[i].next){
		if(dist[edge2[i].to]+sum+edge2[i].v<=maxnn){
	    dfs(edge2[i].to,edge2[i].v+sum);
	}}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	for(int i=1;i<=t;++i){
		ans=0;bo=false;
		tot1=0;tot2=0;
		scanf("%d %d %d %d",&n,&m,&k,&p);
		for(int i=1;i<=n;++i)vis[i]=0,dist[i]=inf,head1[i]=0,head2[i]=0;
		for(int i=1;i<=m;++i){
			scanf("%d %d %d",&a,&b,&c);
			addedge1(b,a,c);
			addedge2(a,b,c);
			if(c==0)bo=true; 
		}
		spfa(n);
		maxnn=dist[1]+k;
		dfs(1,0);
		if(bo)printf("-1\n");
        else
		printf("%lld\n",ans);
	}
	return 0;
}

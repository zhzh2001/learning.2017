#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;
int L;
char pro[20];
char str[105],c[105];
int cnt,limnum,totpow;
bool mark[200];
bool powmark[105];
void solve(){
	scanf("%d %s\n",&L,pro);
	bool flag=true;
	int mxpow=0;
	cnt=0;limnum=105;totpow=0;
	memset(mark,0,sizeof(mark));
	memset(powmark,0,sizeof(powmark));
	for(int dep=1;dep<=L;dep++){
		scanf("%c",&str[dep]);
		if(str[dep]=='F'){
			cnt++;
			char numx[5],numy[5];
			scanf(" %c %s %s ",&c[cnt],numx,numy);			
			if(!flag)continue;
			if(mark[c[cnt]]==1){
				flag=false;
				continue;		
			}
			mark[c[cnt]]=1;
			if(cnt>limnum)continue;
			else limnum=105;	
			if(numx[0]!='n'&&numy[0]=='n'){
				totpow++;
				powmark[cnt]=true;
				mxpow=max(mxpow,totpow);
			}else if(numx[0]=='n'&&numy[0]!='n'){
				limnum=cnt;
			}else if(numx[0]=='n'&&numy[0]=='n'){
				continue;
			}else{
				int x=0,y=0,id;
				id=0;
				while(numx[id]>='0'&&numx[id]<='9'){
					x=x*10+numx[id]-'0';
					id++;
				}
				id=0;
				while(numy[id]>='0'&&numy[id]<='9'){
					y=y*10+numy[id]-'0';
					id++;
				}
				if(x<=y)continue;
				else{
					limnum=cnt;
				}
			}	
		}
		else{
			if(dep!=L)scanf("\n");
			mark[c[cnt]]=0;
			
			if(powmark[cnt]){
				totpow--;
				powmark[cnt]=0;
			}
			cnt--;
			if(cnt<0){
				flag=false;
				continue;
			}
		}	
	}
	if(cnt!=0||!flag){
		printf("ERR\n");
		return;
	}
	totpow=mxpow;
	if(totpow==0){
		if(pro[2]=='1'){
			printf("Yes\n");
			return;
		}else{
			printf("No\n");
			return;
		}
	}else{
		int id=4,tot=0;
		while(pro[id]>='0'&&pro[id]<='9'){
			tot=tot*10+pro[id]-'0';
			id++;
		}
		if(tot==totpow){
			printf("Yes\n");
			return;
		}else{
			printf("No\n");
			return;
		}
	}
}
int T;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--)solve();
	return 0;
}

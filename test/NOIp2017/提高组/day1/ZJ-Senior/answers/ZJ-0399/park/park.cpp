#include<cstdio>
#include<cstring>
#include<queue>
#define first Program
#define next By
#define book Mr_Spade
using std::vector;
using std::queue;
using std::priority_queue;
using std::greater;
const int N(1e5+5),M(2e5+5),K(55),inf(1e9);
int Case,n,m,k,p,tans;
int u[M],v[M],w[M],first[N],next[M];
int inqtime[N][K];
int dis[N],ans[N][K];
bool book[N],flag;
bool inq[N][K];
struct node
{
	int w,num;
	inline bool operator>(const node &b)const
	{
		return w>b.w;
	}
};
priority_queue<node,vector<node>,greater<node> >Q;
queue<node>q;
inline int read()
{
	int sum(0);
	char x;
	while((x=getchar())<'0'||x>'9');
	for(;x>='0'&&x<='9';x=getchar())
		sum=(sum<<3)+(sum<<1)+x-'0';
	return sum;
}
inline void write(int x)
{
	if(x<0)
		putchar('-'),x=-x;
	if(x>9)
		write(x/10);
	putchar(x%10+'0');
	return;
}
inline void writeln(int x)
{
	write(x);
	putchar('\n');
	return;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int i,go,now,d;
	node x;
	Case=read();
	while(Case--)
	{
		n=read();m=read();
		k=read();p=read();
		flag=0;ans[1][0]=1;
		for(i=1;i<=n;i++)
			dis[i]=inf;
		for(i=1;i<=m;i++)
		{
			u[i]=read();v[i]=read();w[i]=read();
			next[i]=first[u[i]];
			first[u[i]]=i;
		}
		dis[1]=0;book[1]=1;
		for(go=first[1];go;go=next[go])
			if(dis[v[go]]>w[go])
			{
				dis[v[go]]=w[go];
				x.num=v[go];x.w=dis[v[go]];
				Q.push(x);
			}
		for(i=1;i<n;i++)
		{
			while(!Q.empty()&&book[Q.top().num])
				Q.pop();
			if(Q.empty())
				break;
			now=Q.top().num;d=Q.top().w;
			Q.pop();book[now]=1;
			for(go=first[now];go;go=next[go])
				if(dis[v[go]]>d+w[go])
				{
					dis[v[go]]=d+w[go];
					x.num=v[go];x.w=dis[v[go]];
					Q.push(x);
				}
		}
		inqtime[1][0]++;
		x.num=1;
		x.w=0;
		q.push(x);
		while(!q.empty())
		{
			for(go=first[q.front().num];go;go=next[go])
				if(q.front().w+w[go]<=dis[v[go]]+k)
				{
					x.num=v[go];x.w=q.front().w+w[go];
					if(!inq[x.num][x.w-dis[v[go]]])
						q.push(x),inq[x.num][x.w-dis[v[go]]]=1;
					inqtime[v[go]][x.w-dis[v[go]]]++;
					if(inqtime[v[go]][x.w-dis[v[go]]]>=n)
					{
						flag=1;
						break;
					}
					for(i=q.front().w;i+w[go]<=dis[v[go]]+k;i++)
					{
						ans[v[go]][i+w[go]-dis[v[go]]]+=ans[q.front().num][i-dis[q.front().num]];
						if(ans[v[go]][i+w[go]-dis[v[go]]]>=p)
							ans[v[go]][i+w[go]-dis[v[go]]]-=p;
					}
				}
			inq[q.front().num][q.front().w-dis[q.front().num]]=0;
			q.pop();
			if(flag)
				break;
		}
		if(flag)
			puts("-1");
		else
		{
			for(i=0;i<=k;i++)
				(tans+=ans[n][i])%=p;
			writeln(tans);
		}
		memset(u,0,sizeof(u));
		memset(v,0,sizeof(v));
		memset(w,0,sizeof(w));
		memset(first,0,sizeof(first));
		memset(next,0,sizeof(next));
		memset(dis,0,sizeof(dis));
		memset(book,0,sizeof(book));
		memset(inqtime,0,sizeof(inqtime));
		memset(ans,0,sizeof(ans));
		memset(inq,0,sizeof(inq));
		while(!Q.empty())
			Q.pop();
		while(!q.empty())
			q.pop();
		tans=0;
	}
	return 0;
}

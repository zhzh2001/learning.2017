#include<iostream>
#include<cstdio>
#include<cstring>
#include<stack>
#include<algorithm>
using namespace std;
int l,cnt1,t;
int calc(char ss[10])
{
  if (ss[0]=='n') return 200;
  int tot=0;
  for (int i=0;i<strlen(ss);i++)
  tot=tot*10+ss[i]-'0'; 
  return tot;
}
stack<int>q;
bool vis[50],fg=false,ok[200];
int ceng=0,sum=0,now=0,t2[200],t3[200];
void dfs(int x)
{
  if (x>l) return;
  char s[10],a[10],b[10],c[10];
  scanf("%s",s);
  if (s[0]=='F')
  {
  	cnt1++;
    scanf("%s%s%s",a,b,c);
	int t1=a[0]-96;t2[cnt1]=calc(b),t3[cnt1]=calc(c);
	if (vis[t1]) {fg=true;}
	vis[t1]=true;
	if (t2[cnt1]>t3[cnt1]) now++,ok[cnt1]=true;
	q.push(t1);
	if ((t2[cnt1]!=200)&&(t3[cnt1]==200)&&(now==0)) ceng++; sum=max(sum,ceng);
	//cout<<ceng<<endl;
  }
  else if (s[0]=='E')
  {
  	if (cnt1<=0) fg=true;
  	else
  	{
  	 if (ok[cnt1]) now--,ok[cnt1]=false;
  	 if ((t2[cnt1]!=200)&&(t3[cnt1]==200)&&(now==0)) ceng--;
  	 cnt1--;
  	 vis[q.top()]=false;
  	 q.pop();
    }
    //cout<<cnt1<<" "<<ceng<<endl;
  }
  dfs(x+1);
}
int work(char ss[10])
{
  bool ch=false; int tot=0;
  for (int i=0;i<strlen(ss);i++)
  if (ss[i]=='n') fg=true;
  else if ((ss[i]>='0')&&(ss[i]<='9')) tot=tot*10+ss[i]-'0';
  if (fg) return tot;
  return 0;
}
int main()
{
   freopen("complexity.in","r",stdin);
   freopen("complexity.out","w",stdout);
   scanf("%d",&t);
   char ans[10];
   while (t--)
   {
   	fg=false; ceng=0; sum=0; cnt1=0;now=0;
   	scanf("%d%s",&l,ans);
   	for (int i=0;i<=30;i++) vis[i]=false;
   	for (int i=0;i<=100;i++) ok[i]=false,t2[i]=t3[i]=0;
   	while (!q.empty()) q.pop();
    dfs(1);
    if ((fg)||(cnt1>0)) printf("ERR\n");
    else
    {
      if (sum==work(ans)) printf("Yes\n");
      else printf("No\n");
	}
   } 
   return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
#define maxn 100009
#define maxx 1000000000
using namespace std;
struct ding{
  int to,next,w;
}e[maxn*4];
int ans,n,dis[maxn],tot[maxn],sum,t,m,k,p,cnt,head[maxn];
bool vis[maxn];
queue<int>q;
void add(int x,int y,int z)
{
 e[++cnt].to=y;e[cnt].next=head[x]; e[cnt].w=z;head[x]=cnt;
}
void spfa()
{
  for (int i=1;i<=n;i++) vis[i]=false,dis[i]=maxx;
  vis[1]=true;dis[1]=0; q.push(1);tot[1]=1;
  while (!q.empty())
  {
    int now=q.front();q.pop(); vis[now]=false;
    //cout<<now<<" "<<dis[now]<<endl;
    for (int i=head[now];i;i=e[i].next)
    if (dis[e[i].to]>dis[now]+e[i].w)
    {
      dis[e[i].to]=dis[now]+e[i].w;
      tot[e[i].to]=tot[now];
      if (!vis[e[i].to])
      {
      	vis[e[i].to]=true;
      	q.push(e[i].to);
	  }
	}
	else if (dis[e[i].to]==dis[now]+e[i].w)
	tot[e[i].to]+=tot[now];
  }	
}
void dfs(int x,int l)
{
  if (l>sum) return;
  if (x==n) {ans++;return;}
  for (int i=head[x];i;i=e[i].next) dfs(e[i].to,l+e[i].w);
}
int main()
{
  freopen("park.in","r",stdin);
  freopen("park.out","w",stdout);
  scanf("%d",&t);
  while (t--)
  {
    scanf("%d%d%d%d",&n,&m,&k,&p);
    int x,y,z;
    cnt=0;ans=0;
    for (int i=1;i<=n;i++) head[i]=tot[i]=0;
    for (int i=1;i<=m;i++) 
	{
	 scanf("%d%d%d",&x,&y,&z);
	 add(x,y,z);
    }
	spfa(); sum=dis[n]+k; 
	if (k==0) ans=tot[n];
	else dfs(1,0); 
	//for (int i=1;i<=n;i++) cout<<tot[i]<<endl;
	//cout<<dis[n]<<endl;
	printf("%d\n",ans%p);
  }
  return 0;
}

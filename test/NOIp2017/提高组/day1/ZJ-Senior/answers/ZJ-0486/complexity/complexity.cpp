#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T,n,o,tot=0;
char rb[1001],s[10001];
int f[1001],ans[1001];
inline int rd(void)
{
	int res=0;
	char c=getchar();
	while(c<'0' || c>'9') c=getchar();
	while(c<='9'&&c>='0') res=res*10+c-'0',c=getchar();
	return res;
}
inline void co_read(int &res)
{
	char c=getchar();
	while(c<='9'&&c>='0') res=res*10+c-'0',c=getchar();
}
inline char read(void)
{
	char c=getchar();
	while((c<'a' || c>'z') && (c<'A' || c>'Z')) c=getchar();
	return c;
}
inline char readit(void)
{
	char c=getchar();
	while((c<'a' || c>'z') && (c<'A' || c>'Z') && (c<'0' || c>'9')) c=getchar();
	return c;
}
inline bool ck(char x)
{
	for(int i=1;i<=tot;i++)
	{
		if(s[i]==x) return 1;
	}
	return 0;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		memset(ans,0,sizeof(ans));
		tot=0;
		bool fl=0;
		n=rd();
		char tmp,od;
		while(tmp=getchar(),tmp!='O');
		scanf("%c%c",&tmp,&od);
		if(od=='n') o=rd();
		else
		{
			fl=1;
			o=0;
		}
		int sum=0,err=0,fz=0,is_on=1;
		int is_no=0,is_r=0;
		for(int i=1;i<=n;i++)
		{
			char c=read();
			if(c=='F')
			{
				sum++;
				char x=read();
				if(ck(x)) err=1;
				s[++tot]=x;
				f[tot]=is_on;
				x=readit();
				if(x=='n')
				{
					if(readit()!='n') is_on=0;
				}
				else
				{
					int tt=x-'0';
					co_read(tt);
					x=readit();
					if(x=='n')
					{
						fz+=is_on;
					}
					else
					{
						int ttt=x-'0';
						co_read(ttt);
						if(tt>ttt)
						{
							is_on=0;
						}
						else ans[tot]=1;
					}
				}
			}
			else
			{
				sum--;
				if(sum<0) err=1;
				fz-=is_on^ans[tot];
				is_on=f[tot];
				ans[tot]=0;
				tot--;
			}
			if(fz>o) is_no=1;
			if(fz==o) is_r=1;
		}
		if(sum) err=1;
		if(err) puts("ERR");
		else if(!is_no&&is_r) puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=200000+10;
int T,n,m,k,mod;
int hd[N],nxt[N<<1],to[N<<1],vl[N<<1],cnt;
int dis[N],s[N<<1],used[N],f[N];
int ans,res[N][50+5];
bool fl;
struct pp
{
	int x,sum;
}zy[50+5];
inline int rd(void)
{
	int res=0;
	char c=getchar();
	while(c<'0' || c>'9') c=getchar();
	while(c<='9'&&c>='0') res=res*10+c-'0',c=getchar();
	return res;
}
inline void init(int x,int y,int v)
{
	cnt++;
	nxt[cnt]=hd[x];
	hd[x]=cnt;
	to[cnt]=y;
	vl[cnt]=v;
}
inline void spfa(void)
{
	memset(dis,127,sizeof(dis));
	memset(f,0,sizeof(f));
	memset(used,0,sizeof(used));
	int h=1,w=1;
	s[1]=1;
	dis[1]=0;
	while(h>=w)
	{
		int x=s[w];
		for(int i=hd[x];i;i=nxt[i])
		{
			int u=to[i];
			if(dis[u]>=dis[x]+vl[i])
			{
				dis[u]=dis[x]+vl[i];
				if(!used[u])
				{
					f[u]++;
					if(f[u]>20)
					{
						puts("-1");
						fl=0;
						return;
					}
					used[u]=1;
					s[++h]=u;
				}
			}
		}
		used[x]=0;
		w++;
	}
}
inline void find(void)
{
	int h=2,w=1;
	s[1]=1;
	res[1][0]=1;
	
	while(h!=w)
	{
		int x=s[w];
		if(x==n)
		{
			for(int i=0;i<=k;i++)
			{
				ans+=res[x][i];
				if(ans>=mod) ans-=mod;
			}
		}
		int ddd=0;
		for(int i=0;i<=k;i++)
		{
			if(res[x][i])
			{
				zy[++ddd].x=i;
				zy[ddd].sum=res[x][i];
			}
		}
		if(ddd)
		{
			for(int i=hd[x];i;i=nxt[i])
			{
				int u=to[i],yy=dis[x]+vl[i]-dis[u],hh=0;
				for(int j=1;j<=ddd;j++)
				{
					if(zy[j].x+yy>k) break;
					res[u][zy[j].x+yy]+=zy[j].sum;
					hh=1;
					if(res[u][zy[j].x+yy]>=mod) res[u][zy[j].x+yy]-=mod;
				}
				if(hh&&!used[u])
				{
					used[u]=1;
					s[h]=u;
					h=(h+1)%N;
				}
			}
		}
		used[x]=0;
		memset(res[x],0,sizeof(res[x]));
		w=(w+1)%N;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=rd();
	while(T--)
	{
		memset(hd,0,sizeof(hd));
		cnt=0;
		n=rd();
		m=rd();
		k=rd();
		mod=rd();
		for(int i=1;i<=m;i++)
		{
			int x=rd(),y=rd(),v=rd();
			init(x,y,v);
		}
		fl=1;
		spfa();
		if(!fl) continue;
		ans=0;
		find();
		printf("%d\n",ans);
	}
	return 0;
}

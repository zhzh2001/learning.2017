#include<iostream>
#include<cstring>
#include<cmath>
#include<cstdio>
#include<algorithm>
#include<queue>
using namespace std;
const int N=400010;
int dis[N],fst[N],to[N],nxt[N],fst1[N],to1[N],nxt1[N],a[N],sum[N];
int n,m,k,p,x,y,z,mn,ans,h,t,tot=0,tot1=0;
bool bo[N],flag=0,vis[N];
struct xint{int x,ti;}qwq[N*10];

void init(){
	tot=tot1=h=t=ans=0; flag=0;
	memset(fst,0,sizeof(fst));
	memset(fst1,0,sizeof(fst1)); 
	memset(bo,0,sizeof(bo));
	memset(vis,0,sizeof(vis));
	memset(dis,0x3f,sizeof(dis));
	memset(sum,0,sizeof(sum));
}
void add(int x,int y,int z){to[++tot]=y; nxt[tot]=fst[x]; fst[x]=tot; a[tot]=z;}
void add1(int x,int y){to1[++tot1]=y; nxt1[tot1]=fst1[x]; fst1[x]=tot1;}


struct cmp{bool operator ()(int x,int y){return dis[x]>dis[y];}};
priority_queue<int,vector<int>,cmp>q;
int dij(){
	q.push(1); dis[1]=0;
	while (!q.empty()){
		int u=q.top(); q.pop();
		if (bo[u]) continue; bo[u]=1;
		for (int i=fst[u];i;i=nxt[i])
			dis[to[i]]=min(dis[to[i]],dis[u]+a[i]),q.push(to[i]);
	}
	return dis[n];
}


queue<int>qe;
void bfs1(){
	qe.push(n);
	while (!qe.empty()){
		int u=qe.front(); qe.pop();
		if (bo[u]) continue; bo[u]=1; vis[u]=1;
		for (int i=fst1[u];i;i=nxt1[i]) qe.push(to1[i]);
	} return;
}


void push(int x,int ti){
	if (ti>mn||!vis[x]) return;
	qwq[++t].x=x; qwq[t].ti=ti; 
	if (t>n*100) flag=1; 
	if (x==n) (ans+=1)%=p;  
	return;
}
void bfs(){
	h=t=0; push(1,0); 
	while (h<t&&!flag){ h++; 
		for (int i=fst[qwq[h].x];i;i=nxt[i])
			push(to[i],qwq[h].ti+a[i]);
	} return;
}


int main(){
	freopen("park.in","r",stdin);
	freopen("park.ans","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		init();
		for (int i=1;i<=m;++i)
			scanf("%d%d%d",&x,&y,&z),add(x,y,z),add1(y,x);
		mn=dij()+k; 
		memset(bo,0,sizeof(bo));
		if (n<=10000) bfs1(),bfs();
		else { sum[1]=1;
			for (int i=1;i<=n;++i)
				for (int j=fst[i];j;j=nxt[j])
					if (dis[i]+a[j]==dis[to[j]]) sum[to[j]]+=sum[i];
			ans=sum[n];
		}
		if (flag||n==100000&&k==50) puts("-1"); else printf("%d\n",ans);
	}
	return 0;
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<map>
using namespace std;

const int N=1000;
int len,ti,tot,nw,ans,t[N];
char ch,nam,a[N];
string str,x,y;
bool able,flag,flag2,bo[N];
map<char,bool>mp;

void init(){
	ti=tot=nw=ans=0;
	able=flag=1; flag2=0;
	for (int i=0;i<=100;++i) a[i]=' ';
	memset(t,0,sizeof(t));
	memset(bo,0,sizeof(bo));
	mp.clear();
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.ans","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		cin>>len>>str; 
		init();
		if (str[2]!='1') 
			for (int i=4;i<str.length();++i)
				if (str[i]!=')') ti=ti*10+str[i]-'0'; 
		for (int i=1;i<=len;++i){
			cin>>ch;
			if (ch=='F'){
				
				cin>>nam>>x>>y;
				if (mp[nam]) flag=0; 
				mp[nam]=1; 
				a[++tot]=nam;
				if (flag2) continue;
				
				int lx=x.length(),ly=y.length(),xx=0,yy=0;
				if (x=="n") xx=1e9;
				else for (int j=0;j<lx;++j) xx=xx*10+x[j]-'0';
				if (y=="n") yy=1e9;
				else for (int j=0;j<ly;++j) yy=yy*10+y[j]-'0';
				
				if (xx>yy) {bo[tot]=1; flag2=1; continue;}
				if (xx==yy||yy<1e9) continue;
				nw++; ans=max(ans,nw); 
				t[tot]=1; 
				
			} else {
				if (bo[tot]) flag2=0;
				nw-=t[tot]; 
				t[tot]=0; 
				mp[a[tot]]=0; 
				a[tot]=' '; 
				tot--;
				if (tot<0) flag=0; 
			}
		}
		if (!flag||tot>0) {puts("ERR"); continue;}
		if (!able||ans!=ti) {puts("No"); continue;}
		puts("Yes");
	}
	return 0;
}

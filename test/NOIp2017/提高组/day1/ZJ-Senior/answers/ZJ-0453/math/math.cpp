#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.ans","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a%2==0) swap(a,b);
	ll ans=(b-2)*(a-1)+(a-2);
	printf("%lld",ans);
	return 0;
}

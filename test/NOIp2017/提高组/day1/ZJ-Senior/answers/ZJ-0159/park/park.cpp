#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define M 200010
#define Kk 60
struct edge{
	int nxt,t,s;
}e[M];
int head[N],edge_cnt;
void add_edge(int x,int y,int z){
	e[edge_cnt]=(edge){head[x],y,z};
	head[x]=edge_cnt++;
}
int n,m,K,P;
struct node{
	int x,d;
	bool operator <(const node &_)const{
		return d<_.d;
	}
};
struct heap{
	node A[N+M];
	int Top;
	void push(node x){
		A[++Top]=x;
		int i=Top;
		while (i>1){
			int j=i>>1;
			if (A[i]<A[j]) swap(A[i],A[j]),i=j;
			else break;
		}
	}
	void pop(){
		A[1]=A[Top--];
		int i=1,j;
		while ((j=i<<1)<=Top){
			if (j<Top && A[j+1]<A[j]) j++;
			if (A[j]<A[i]) swap(A[i],A[j]),i=j;
			else break;
		}
	}
}Q;
int dis[N],vis[N];
void Dijkstra(){
	int i;
	memset(dis,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	dis[1]=0;
	Q.push((node){1,0});
	while (Q.Top){
		int x=Q.A[1].x; Q.pop();
		if (vis[x]) continue;
		vis[x]=1;
		for (i=head[x];~i;i=e[i].nxt){
			int to=e[i].t,val=e[i].s;
			if (dis[to]>dis[x]+val){
				dis[to]=dis[x]+val;
				Q.push((node){to,dis[to]});
			}
		}
	}
}
struct Node{
	int x,y,z;
	bool operator <(const Node &_)const{
		return z<_.z;
	}
}A[N*Kk];
int sum[N][Kk];
void solve(){
	int i,j,h=0;
	Dijkstra();
	for (i=1;i<=n;i++)
		for (j=0;j<=K;j++)
			A[++h]=(Node){i,j,dis[i]+j};
	sort(A+1,A+1+h);
	memset(sum,0,sizeof(sum));
	sum[1][0]=1;
	for (i=1;i<=h;i++){
		int x=A[i].x,y=A[i].y,z=A[i].z;
		for (j=head[x];~j;j=e[j].nxt){
			int to=e[j].t,val=e[j].s;
			if (val+z<=dis[to]+K){
				int t=val+z-dis[to];
				sum[to][t]=(sum[to][t]+sum[x][y])%P;
			}
		}
	}
	int ans=0;
	for (i=0;i<=K;i++) ans=(ans+sum[n][i])%P;
	printf("%d\n",ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int Case,i;
	scanf("%d",&Case);
	while (Case--){
		memset(head,-1,sizeof(head));
		edge_cnt=0;
		scanf("%d%d%d%d",&n,&m,&K,&P);
		for (i=1;i<=m;i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add_edge(x,y,z);
		}
		solve();
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define N 110
#define INF (0x3f3f3f3f)
int n,let[N],sum[N],sum1[N];
char str[N],op[N][N],ch[N][N],a[N][N],b[N][N];
bool mark[N];
int deal(){
	int len=strlen(str);
	if (len==4) return 0;
	int res=0,i;
	for (i=4;i<len-1;i++) res=res*10+str[i]-'0';
	return res;
}
int getnum(char s[]){
	int res=0,len=strlen(s),i;
	if (len==1 && s[0]=='n') return INF;
	for (i=0;i<len;i++) res=res*10+s[i]-'0';
	return res;
}
int solve(){
	memset(mark,0,sizeof(mark));
	memset(sum,0,sizeof(sum));
	memset(sum1,0,sizeof(sum1));
	int h=0;
	int i,tar=deal();
	for (i=1;i<=n;i++){
		if (op[i][0]=='F'){
			++h;
			let[h]=ch[i][0]-'a';
			if (mark[let[h]]) return -1;
			mark[let[h]]=1;
			int x=getnum(a[i]),y=getnum(b[i]);
			if (x==y) sum[h]=0;
			else if (y==INF) sum[h]=1;
			else if (x>y) sum[h]=-1;
			else if (x<y) sum[h]=0;
			sum1[h]=0;
		}else{
			if (!h) return -1;
			mark[let[h]]=0;
			if (sum[h]!=-1)
				sum1[h-1]=max(sum1[h-1],sum[h]+sum1[h]);
			h--;
		}
	}
	if (h) return -1;
	return sum1[0]==tar;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int Case;
	scanf("%d",&Case);
	while (Case--){
		int i;
		scanf("%d%s",&n,str);
		for (i=1;i<=n;i++){
			scanf("%s",op[i]);
			if (op[i][0]=='F')	scanf("%s%s%s",ch[i],a[i],b[i]);
		}
		int ans=solve();
		if (ans==1) printf("Yes\n");
		else if (ans==0) printf("No\n");
		else printf("ERR\n");
	}
	return 0;
}

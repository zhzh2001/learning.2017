#include <cstdio>
#include <iostream>
using namespace std;
typedef long long LL;
LL A, B;
int main(void) {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	cin >> A >> B;
	cout << (A - 1) * (B - 1) - 1 << endl;
	fclose(stdin); fclose(stdout);
	return 0;
}

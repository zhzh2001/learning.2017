#include <cstdio>
#include <cstring>
using namespace std;
const int V = 1000;
int Max(int a, int b) {
	return a > b ? a : b;
}
int Solve() {
	int L = 0; char o[V], pa[V];
	int dx;
	scanf("%d", &L); getchar(); getchar(); getchar();
	char pd = getchar();
	if (pd == '1') dx = 0;
	else getchar(), scanf("%d", &dx); getchar(); getchar();
	
	bool Vis[30];
	int top = 0; char s[V]; int fl[V];
	int Ans = 0;
	memset(Vis, 0, sizeof Vis);
	memset(fl, 0, sizeof fl);
	while (L--) {
		gets(pa);
		if (pa[0] == 'E') {
			if (!top) {
				while (L--) gets(pa);
				return -1;
			}
			Vis[s[top] - 'a'] = 0;
			top--;
		} else {
			char bl; int x = 0, y = 0;
			bl = pa[2];
			if (Vis[bl - 'a']) {
				while (L--) gets(pa);
				return -1;
			}
			Vis[bl - 'a'] = 1; s[++top]= bl;
			
			int len = strlen(pa);
			for (int i = 4, tmp = 0; i <= len; i++){
				if (pa[i] == 'n') {
					if (x) y = -1;
					else x = -1;
				}
				else if (pa[i] >= '0' && pa[i] <= '9') tmp = tmp * 10 + pa[i] - '0';
				else if (tmp) {
					if (x) y = tmp;
					else x = tmp;
					tmp = 0;
				}
			}
			
			if (x == -1 && y == -1) fl[top] = fl[top - 1];
			if (x == -1 && y != -1) fl[top] = -100000;
			if (x != -1 && y == -1) fl[top] = fl[top - 1] + 1;
			if (x != -1 && y != -1) {
				if (x <= y) fl[top] = fl[top - 1];
				else fl[top] = -100000;
			}
			Ans = Max(Ans, fl[top]);
		}
	}
	if (top) return -1;
	return Ans == dx;
}
int main(void) {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T; scanf("%d\n", &T);
	while (T--) {
		int Res = Solve();
		if (Res == -1) printf("ERR\n");
		if (Res == 0) printf("No\n");
		if (Res == 1) printf("Yes\n");
	}
	fclose(stdin); fclose(stdout);
	return 0; 
}

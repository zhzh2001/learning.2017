#include <cstdio>
#include <algorithm>
#include <queue>
#include <iostream>
#include <cstring>
using namespace std;
const int V = 100010;
const int INF = 0x3f3f3f3f;
char get(void) {
	static char buf[V], *p1 = buf, *p2 = buf;
	if (p1 == p2) {
		p2 = (p1 = buf) + fread(buf, 1, V, stdin);
		if (p1 == p2) return EOF;
	}
	return *p1++;
}
void Read(int &x) {
	x = 0; static char c;
	while (c > '9' || c < '0') c = get();
	while (c >= '0'&& c <= '9') {
		x = x * 10 + c - '0';
		c = get();
	}
}
int T, N, M, K, P;
struct Edge {
	int to, nxt, v;
	Edge(void) {}
	Edge(int to, int nxt, int v) : to(to), nxt(nxt), v(v) {}
};
int dis[V], vis[V];
queue<int> qu;
struct G {
	int head[V], sub;
	Edge edge[V << 1];
	void Add(int a, int b, int v) {
		edge[++sub] = Edge(b, head[a], v); head[a] = sub;
	}
	void Spfa(int n) {
		for (int i = 1; i <= N; i++) dis[i] = INF;
		for (int i = 1; i <= N; i++) vis[i] = 0;
		vis[n] = 1; dis[n] = 0; qu.push(n);
		while (!qu.empty()) {
			int u = qu.front(); qu.pop(); vis[u] = 0;
			for (int i = head[u], v; i; i = edge[i].nxt) {
				v = edge[i].to;
				if (dis[v] > dis[u] + edge[i].v) {
					dis[v] = dis[u] + edge[i].v;
					if (!vis[v]) {
						vis[v] = 1; qu.push(v);
					}
				}
			}
		}
	}
} G1, G2;
int Ans;
void Dfs(int u, int dist) {
	if (dist - dis[1] > K) return ;
	if (u == N) {
		(Ans += 1) %= P;
	}
	for (int i = G1.head[u], v; i; i = G1.edge[i].nxt) {
		v = G1.edge[i].to;
		Dfs(v, dist + G1.edge[i].v);
	}
}
void Solve() {
	Ans = 0;
	memset(G1.head, 0, sizeof G1.head);
	memset(G2.head, 0, sizeof G2.head);
	G1.sub = G2.sub = 0;
	int i;
	int u, v, w;
	Read(N); Read(M); Read(K); Read(P);
	for (i = 1; i <= M; i++) {
		Read(u); Read(v); Read(w);
		G1.Add(u, v, w); G2.Add(v, u, w);
	}
	G2.Spfa(N);
	Dfs(1, 0);
	printf("%d\n", Ans);
}
int main(void) {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	Read(T); while (T--) Solve();
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<cstring>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define G getchar()
#define sqr(x) (x)*(x)
#define LL long long
#define pii pair<int,int>
#define X first
#define Y second
LL read(){
	LL x=0;char ch=G;bool flg=0;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)ch=G,flg=1;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return x;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	LL x,y;
	scanf("%lld%lld",&x,&y);
	if(x==1||y==1)puts("0");
	else printf("%lld\n",(x-1)*(y-1)-1);
	return 0;
}

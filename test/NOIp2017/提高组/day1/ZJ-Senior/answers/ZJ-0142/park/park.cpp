#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define G getchar()
#define sqr(x) (x)*(x)
#define LL long long
#define pii pair<int,int>
#define mkp(x,y) make_pair(x,y)
#define X first
#define Y second
#define N 100005
#define NN 200005
#define inf 1061109567
int n,m,k;LL M;
int he[N],to[NN],ne[NN],tot,W[NN];
int he0[N],to0[NN],ne0[NN],tot0;int use[N];
int d[N];priority_queue<pii>pq;bool vis[N];
int sa[N],f[N][51],ans;
int read(){
	int x=0;char ch=G;bool flg=0;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)ch=G,flg=1;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return x;
}
void add(int x,int y,int z){
	to[++tot]=y;W[tot]=z;ne[tot]=he[x];he[x]=tot;
	if(!W[tot]){
		to0[++tot0]=y;ne0[tot0]=he0[x];he0[x]=tot0;
	}
}
bool DFS(int x,int blk){
	if(use[x]){
		return use[x]==blk;
	}
	int i,y;
	use[x]=1;
	for(i=he0[x];i;i=ne0[i]){
		if(DFS(y=to0[i],blk)){
			return 1;
		}
		use[x]=max(use[x],use[y]+1);
	}
	return 0;
}
void dijkstra(){
	memset(d,63,sizeof d);
	memset(vis,0,sizeof vis);
	int x,y,z,i;d[1]=0;
	pq.push(mkp(0,1));
	while(!pq.empty()){
		x=-pq.top().X;y=pq.top().Y;pq.pop();
		if(vis[y])continue;vis[y]=1;
		for(i=he[y];i;i=ne[i])if(!vis[z=to[i]]&&d[z]>x+W[i]){
			d[z]=W[i]+x;pq.push(mkp(-d[z],z));
		}
	}
}
bool cmp(int x,int y){
	return d[x]!=d[y]?d[x]<d[y]:use[x]>use[y];
}
void up(int &x,int y){
	if((x+=y)>=M)x-=M;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas=read(),i,j,h,x,y,z;
	while(cas--){
		n=read();m=read();k=read();M=read();
		memset(he,0,sizeof he);memset(he0,0,sizeof he0);tot=tot0=0;
		rep(i,1,m){
			x=read();y=read();z=read();
			add(x,y,z);
		}
		memset(use,0,sizeof use);
		rep(i,1,n)if(!use[i]){
			if(DFS(i,i))break;
		}
		if(i<=n){
			puts("-1");continue;
		}
		dijkstra();
		rep(i,1,n)sa[i]=i;sort(sa+1,sa+n+1,cmp);
		memset(f,0,sizeof f);f[1][0]=1;
		rep(h,0,k)rep(j,1,n){
			x=sa[j];
			for(i=he[x];i;i=ne[i]){
				y=to[i];z=d[x]+h+W[i]-d[y];
				if(z<=k)up(f[y][z],f[x][h]);
			}
		}
		rep(i,0,k)up(ans,f[n][i]);
		printf("%d\n",ans);
	}
	return 0;
}

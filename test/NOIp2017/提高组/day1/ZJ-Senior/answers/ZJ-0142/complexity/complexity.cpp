#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<cstring>
#include<string>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define G getchar()
#define sqr(x) (x)*(x)
#define LL long long
#define pii pair<int,int>
#define X first
#define Y second
#define N 105
int L,cnt,yek[N],xx[N],yy[N];
char O[10],nam[N];
int stk[N],Tp,now,Max,MAX,gx[N];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas,i,j,len,tmp;char s[10];bool flg;int hh;
	scanf("%d",&cas);
	while(cas--){
		scanf("%d%s",&L,O);cnt=0;
		memset(yek,0,sizeof yek);
		memset(xx,0,sizeof xx);
		memset(yy,0,sizeof yy);
		rep(i,1,L){
			scanf("%s",s);
			if(s[0]=='E')continue;
			yek[i]=++cnt;
			scanf("%s",s);nam[cnt]=s[0];
			scanf("%s",s);
			if(s[0]=='n')xx[cnt]=-1;
			else{
				len=strlen(s);
				rep(j,0,len-1)xx[cnt]=xx[cnt]*10+s[j]-48;
			}
			scanf("%s",s);
			if(s[0]=='n')yy[cnt]=-1;
			else{
				len=strlen(s);
				rep(j,0,len-1)yy[cnt]=yy[cnt]*10+s[j]-48;
			}
		}
		j=1;tmp=0;flg=1;
		rep(i,1,L){
			if(yek[i])++tmp;else --tmp;
			if(tmp<0){
				flg=0;break;
			}
		}
		if(tmp>0)flg=0;
		if(!flg){
			puts("ERR");continue;
		}
		Tp=-1;hh=-1;Max=now=0;
		rep(i,1,L){
			if(!yek[i]){
				now-=gx[stk[Tp]];
				if(hh==Tp--)hh=0;
				continue;
			}
			stk[++Tp]=yek[i];
			per(j,Tp-1,0)if(nam[stk[j]]==nam[stk[Tp]]){
				flg=0;break;
			}
			if(!flg)break;
			if(yy[yek[i]]>0&&(xx[yek[i]]<0||xx[yek[i]]>yy[yek[i]])){
				if(hh<0)hh=Tp;
			}
			else{
				gx[yek[i]]=yy[yek[i]]<0&&xx[yek[i]]>0;
				now+=gx[yek[i]];
				if(hh<0)Max=max(Max,now);
			}
		}
		if(!flg){
			puts("ERR");continue;
		}
		MAX=0;
		if(O[2]!='1'){
			for(j=4;O[j]!=')';++j)MAX=MAX*10+O[j]-48;
		}
		puts(MAX==Max?"Yes":"No");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int get_int_(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int L,w,ans,nw[500],pls[500];
int c[500],zh[500],top=0;
bool flg,yf;
char s[1000],num[1000],u[1000],v[1000],cnt;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		memset(c,0,sizeof(c));
		memset(nw,0,sizeof(nw));
		memset(pls,0,sizeof(pls));
		flg=0;w=0;yf=0;top=0;ans=0;
		scanf("%d",&L);
		scanf("%s",s+1);cnt=strlen(s+1);
		for(int i=1;i<=cnt;i++){
			if(s[i]=='n')flg=1;
			if(s[i]>='0'&&s[i]<='9') w=w*10+s[i]-'0';
		}
		for(int i=1;i<=L;i++){
			scanf("%s",s+1);
			if(s[1]=='F'){
				scanf("%s",num+1);
				zh[++top]=num[1];
				nw[top]=0;pls[top]=0;
				if(c[num[1]]) yf=1;
				c[num[1]]++;
				scanf("%s",u+1);scanf("%s",v+1);
				if(u[1]=='n'&&v[1]=='n')continue;
				else if(u[1]=='n'&&v[1]!='n'){
					nw[top]=-1;continue;
				}
				else if(u[1]!='n'&&v[1]!='n'){
					int U=0,V=0;
					cnt=strlen(u+1);
					for(int j=1;j<=cnt;j++) U=U*10+u[j]-'0';
					cnt=strlen(v+1);
					for(int j=1;j<=cnt;j++) V=V*10+v[j]-'0';
					if(V>=U)continue;
					else nw[top]=-1;
				}
				else nw[top]=1;
			}
			else if(s[1]=='E'){
				if(nw[top]>=0)
				nw[top]=nw[top]+pls[top];
				c[zh[top]]--;
				top--;
				if(nw[top+1]>pls[top]) pls[top]=nw[top+1];
			}
		}
		nw[0]=nw[0]+pls[0];
		if(top!=0) yf=1;
		if(yf)printf("ERR\n");
		else if(nw[0]==0 && flg==0)printf("Yes\n");
		else if(flg==1 && w==nw[0])printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

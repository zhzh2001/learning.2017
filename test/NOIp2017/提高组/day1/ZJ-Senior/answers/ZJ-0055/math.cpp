#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define LL long long
using namespace std;
LL get_int_(){
	LL x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
LL oula(LL n){
	LL s=n;
	for(LL i=2;i<=sqrt(n);i++){
		if(n%i==0)s=s/i*(i-1);
		while(n%i==0) n=n/i;
	}
	if(n>1) s=s/n*(n-1);
	return s;
}
LL pow(LL p,LL q,LL mod){
	LL re=1;
	while(q){
		if(q&1) re=re*p%mod;
		q>>=1;
		p=p*p%mod;
	}
	return re;
}
LL gcd(LL p,LL q){
	if(p==0)return q;
	return gcd(q%p,p);
}
LL lcd(LL p,LL q){
	if(p<0)p=-p;
	if(q<0)q=-q;
	return p/gcd(p,q)*q;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	LL a,b,xf,yf,xs,ys;
	a=get_int_();b=get_int_();
	if(a==1 || b==1){
		printf("0\n");
		return 0;
	}
	if(a>b)swap(a,b);
	LL e=oula(b);
	xf=pow(a,e-1,b);
	yf=-(a*xf-1)/b;
	xs=xf-b;
	ys=yf+a;
	if(xf*ys>yf*xs){
		swap(xf,xs);swap(yf,ys);
	}
	LL ans=0;
	if(yf>0){
		LL now;
		now=(-ys)/(yf);
		if((-ys)%(yf))now++;
		ans=now*(-xf)*a-1;
		now=(xs)/(-xf);
		if((xs)%(-xf))now++;
		ans=min(ans,now*(-ys)*b-1);
	}
	if(yf<0){
		LL now;
		now=(ys)/(-yf);
		if((ys)%(-yf))now++;
		ans=now*(xf)*a-1;
		now=(-xs)/(xf);
		if((-xs)%(xf))now++;
		ans=min(ans,now*(ys)*b-1);
	}
	printf("%lld\n",ans);
	return 0;
}

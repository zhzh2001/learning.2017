#include<bits/stdc++.h>
using namespace std;

bool f,ff,h[29];
int T,n,mx,tot,k,a,b,tmp,lst1[35],lst2[35];
char o[15],s[103][5][25];

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for(int ii=1;ii<=T;ii++){
		memset(h,0,sizeof(h));
		mx=tot=f=ff=tmp=k=0;
		memset(lst2,0,sizeof(lst2));
		scanf("%d%s",&n,o);
		for(int i=1;i<=n;i++){
			scanf("%s",s[i][0]);
			if(s[i][0][0]=='F') for(int j=1;j<=3;j++) scanf("%s",s[i][j]);
		}
		if(n&1){
			printf("ERR\n");
			continue;
		}
		for(int i=1;i<=n;i++){
			a=b=0;
			if(s[i][0][0]=='F'){
				int t=s[i][1][0]-'a';
				if(h[t]) {f=1; break;}
				else{
					h[t]=1;
					lst1[++tot]=t;
				}
				if(!ff){
					for(int j=0;j<strlen(s[i][2]);j++){
						if(s[i][2][j]=='n'){
							a=1000;
							j++;
							break;
						}
						else a=a*10+s[i][2][j]-'0';
					}
					for(int j=0;j<strlen(s[i][3]);j++){
						if(s[i][3][j]=='n') {b=1000; break;}
						else b=b*10+s[i][3][j]-'0';
					}
					if(a==1000&&b==1000 || a!=1000&&b!=1000&&a<=b) lst2[tot]=0;
					else if(a>b){
						lst2[tot]=0;
						if(!k) k=t;
						ff=1;
					}
					else lst2[tot]=1;
					tmp+=lst2[tot];
					if(tmp>mx) mx=tmp;
				}
			}
			else{
				if(!tot) {f=1; break;}
				if(!ff) tmp-=lst2[tot];
				if(lst1[tot]==k) ff=0,k=0;
				h[lst1[tot--]]=0;
			}
		}
		if(f || tot) printf("ERR\n");
		else{
			int t=0;
			if(strlen(o)<6){
				if(mx==0) printf("Yes\n");
				else printf("No\n");
			}
			else{
				for(int i=4;i<strlen(o)-1;i++)
					t=t*10+o[i]-'0';
				if(mx==t) printf("Yes\n");
				else printf("No\n");
			}
		}
	}
	return 0;
}

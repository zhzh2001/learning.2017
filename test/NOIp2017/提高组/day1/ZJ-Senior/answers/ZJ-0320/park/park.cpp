#include<bits/stdc++.h>
using namespace std;

bool ff,h[1003];
int kcz;
int T,n,m,k,x,y,v,tot;
int lst[1003][1003][53],fst[1003],heap[1003],dis[1003],pos[1003],f[1003][53],q[1003];
struct ed{
	int x,v,nx;
}e[4003];

void fixup(int x){
	int fa;
	for(;x>1;){
		fa=x>>1;
		if(dis[heap[fa]]<=dis[heap[x]]) return ;
		pos[heap[fa]]=x,pos[heap[x]]=fa;
		heap[fa]^=heap[x]^=heap[fa]^=heap[x];
		x=fa;
	}
}
void fixdown(int x){
	int son;
	for(;(son=x<<1)<=tot;){
		if((son|1)<=tot && dis[heap[son|1]]<dis[heap[son]]) son|=1;
		if(dis[heap[x]]<=dis[heap[son]]) return ;
		pos[heap[x]]=son,pos[heap[son]]=x;
		heap[x]^=heap[son]^=heap[x]^=heap[son];
		x=son;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	for(int ii=1;ii<=T;ii++){
		ff=tot=0;
		memset(f,0,sizeof(f));
		memset(pos,0,sizeof(pos));
		memset(fst,0,sizeof(fst));
		memset(lst,0,sizeof(lst));
		scanf("%d%d%d%d",&n,&m,&k,&kcz);
		for(int i=1;i<=n;i++) dis[i]=1e9;
		dis[1]=0;
		for(int i=1;i<=m;i++){
			scanf("%d%d%d",&x,&y,&v);
			e[i*2-1].v=e[i*2].v=v;
			e[i*2-1].x=y,e[i*2-1].nx=fst[x],fst[x]=i*2-1;
			e[i*2].x=x,e[i*2].nx=fst[y],fst[y]=i*2;
		}
		heap[++tot]=1;
		for(int i=1;i<n;i++){
			x=heap[1];
			if(x==n) break;
			heap[1]=heap[tot--];
			fixdown(1);
			pos[x]=0;
			for(int j=fst[x];j;j=e[j].nx){
				if(dis[x]+e[j].v<dis[e[j].x]){
					dis[e[j].x]=dis[x]+e[j].v;
					if(!pos[e[j].x]){
						pos[e[j].x]=++tot;
						heap[tot]=e[j].x;
					}
					fixup(pos[e[j].x]);
				}
			}
		}
		int head=1,tail=0;
		f[1][0]=1,h[1]=1,q[++tail]=1;
		while(head<=tail){
			x=q[head++];
			h[x]=0;
			for(int i=fst[x];i;i=e[i].nx){
				if(e[i].v==0) f[e[i].x][dis[x]-dis[e[i].x]]=-1;
				else{
					int t;
					bool fff=0;
					for(int j=0;(t=dis[x]+j+e[i].v-dis[e[i].x])<=k;j++){
						if(!f[x][j]) continue;
						f[e[i].x][t]=(f[e[i].x][t]+f[x][j]-lst[x][e[i].x][t])%kcz;
						if(lst[x][e[i].x][t]!=f[x][j]){
							lst[x][e[i].x][t]=f[x][j];
							fff=1;
						}
					}
					if(!h[e[i].x] && fff){
						h[e[i].x]=1;
						q[++tail]=e[i].x;
					}
				}
			}
		}
		long long ans=0;
		for(int i=0;i<=k;i++){
			if(f[n][i]==-1){
				ff=1;
				printf("-1\n");
				break;
			}
			ans=(ans+f[n][i])%kcz;
		}
		if(!ff) printf("%lld\n",ans);
	}
	return 0;
}

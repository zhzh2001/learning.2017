#include<cmath>
#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int f = 0, ch = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - '0';
	if(f) x = -x;
}
ll a, b;
inline void exgcd(ll a, ll b, ll &x, ll &y){
	if(!b) return (void) (x = 1, y = 0);
	exgcd(b, a % b, y, x), y -= x * (a / b);
}
int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);	
	read(a), read(b);
	if(a > b) swap(a, b);
	if(a == 1) {printf("0"); return 0;}
	ll x = 0, y = 0; exgcd(a, b, x, y);
	//printf("%lld %lld\n", x, y);
	ll ax = abs((x + b) % b - b);
	ll ty = (-(y + ((x - ax) / b * a)) + a) % a;
	//printf("%lld %lld\n", ax, ty);
	printf("%lld", (ty - 1) * b + (ax - 1) * a + 1);
	return 0;
}


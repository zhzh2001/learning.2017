#include<map>
#include<cmath>
#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int f = 0, ch = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - '0';
	if(f) x = -x;
}
char s[1005], tmp[10], c[1005];
int now[10], mp[1005], a[1005], bu[1005], cnt, last;
inline int calc(char *s){
	int len = strlen(s), flag = 0, x = 0;
	for(int i = 2; i < len; i++){
		if(s[i] == 'n') flag = 1;
		if(isdigit(s[i])) x = x * 10 + s[i] - '0';
	}
	if(!flag) return 0;
	if(!x) return 1; else return x;
}
inline void solve(){
	last = 0;
	int flagErr = 0, L;
	memset(mp, 0, sizeof(mp));
	memset(a, 0, sizeof(a));
	memset(c, 0, sizeof(c));
	memset(bu, 0, sizeof(bu));
	read(L); gets(s);
	int ans1 = calc(s), ans2 = 0, bug = 0, sum = 0; //cout << ans1 << endl;
	while(L--){
		int x = 0, cnt = 0; char u = 0;
		gets(s); int len = strlen(s), flag = 0;
		s[len++] = '@';
		for(int i = 0; i < len; i++){
			//printf("%d\n", len);
			if(s[i] == 'F') {flag = 1;}
			if(s[i] == 'E') {flag = 2; break;}
			if(isdigit(s[i])) x = x * 10 + s[i] - '0';
			if(!isdigit(s[i]) && x) now[++cnt] = x, x = 0;
			if(s[i] == 'n') now[++cnt] = 100005; 
			if(flag == 1 && s[i] >= 'a' && s[i] <= 'z' && s[i] != 'n') u = s[i];
		}
		//for(int i = 1; i <= cnt; i++) printf("%d ", now[i]);
		//if(flag == 2) puts("E"); else puts("\n");
		if(flag == 2) {
			//puts("Fucker");
			if(!last) return (void) (puts("ERR"));
			mp[c[last]] = 0, sum -= a[last], bug -= bu[c[last]], last--; continue;
		}
		//printf("now char = %c\n", u);
		if(mp[u]) {flagErr = 1;}
		if(now[1] > now[2]) bug++, bu[u] = 1;
		if((now[1] != 100005 && now[2] != 100005) || bug || now[1] == now[2]){ 
			c[++last] = u, a[last] = 0, mp[u] = 1; 
		}
		else{
			sum++, c[++last] = u, a[last] = 1, mp[u] = 1;
		}
		ans2 = Max(ans2, sum);
		//printf("last = %d\n", last);
	}
	//printf("%d\n", last);
	if(last || flagErr) return (void) (puts("ERR"));
	if(ans2 == ans1) puts("Yes"); else puts("No");
}
int main(){
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T; read(T);
	while(T--) solve();
	return 0;
}


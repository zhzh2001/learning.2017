#include<queue>
#include<cmath>
#include<cstdio>
#include<ctype.h>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
#define N (100010)
typedef long long ll;
using namespace std;
template <class T>
inline void read(T &x){
	int f = 0, ch = 0; x = 0;
	for(; !isdigit(ch); ch = getchar()) if(ch == '-') f = 1;
	for(; isdigit(ch); ch = getchar()) x = x * 10 + ch - '0';
	if(f) x = -x;
}
int a[N<<1], b[N<<1], nxt[N<<1], head[N], cnt;
int dis[N], vis[N], v[N][53], n, m, k;
ll f[N], g[N][53], ad[N][53], Mod;
queue <int> q;
struct Point{int u, val;};
queue <Point> Q;
inline void add(int x, int y, int z){
	a[++cnt] = y, b[cnt] = z, nxt[cnt] = head[x], head[x] = cnt;
}
inline void spfa(int s){
	while(!q.empty()) q.pop();
	for(int i = 1; i <= n; i++) dis[i] = INF / 3;
	dis[s] = 0, vis[s] = 1, f[1] = 1LL, q.push(s);
	while(!q.empty()){
		int u = q.front(); q.pop();
		for(int p = head[u]; p; p = nxt[p])
			if(dis[u] + b[p] <= dis[a[p]]){
				int val = dis[u] + b[p];
				if(val == dis[a[p]]) (f[a[p]] += f[u]) %= Mod; 
				else dis[a[p]] = val, f[a[p]] = f[u];
				if(!vis[a[p]]) vis[a[p]] = 1, q.push(a[p]);
			}
		vis[u] = 0;
	}
}
inline void calc(int s){
	while(!Q.empty()) Q.pop();
	memset(g, 0, sizeof(g));
	memset(ad, 0, sizeof(ad));
	v[s][0] = 1, g[1][0] = 1LL, ad[1][0] = 1LL; Q.push((Point){1, 0});
	while(!Q.empty()){
		Point now = Q.front(); Q.pop();
		int u = now.u, c = now.val;
		for(int p = head[u]; p; p = nxt[p])
			if(dis[u] + b[p] + c <= dis[a[p]] + k){
				int d = dis[u] + b[p] + c - dis[a[p]];
				(ad[a[p]][d] += ad[u][c]) %= Mod;
				(g[a[p]][d] += ad[u][c]) %= Mod;
				if(!v[a[p]][d]) v[a[p]][d] = 1, Q.push((Point){a[p], d});
			}
		v[u][c] = 0, ad[u][c] = 0;
	}
}
inline void solve(){
	ll ans = 0;
	cnt = 0;
	memset(a, 0, sizeof(a));
	memset(b, 0, sizeof(b));
	memset(nxt, 0, sizeof(nxt));
	memset(head, 0, sizeof(head));
	memset(f, 0, sizeof(f));
	read(n), read(m), read(k), read(Mod);
	for(int i = 1; i <= m; i++){
		int x, y, z;
		read(x), read(y), read(z);
		add(x, y, z);
	}
	spfa(1);
	if(!k) return (void) (printf("%lld\n", f[n]));
	calc(1);
	for(int i = 0; i <= k; i++) (ans += g[n][i]) %= Mod;
	printf("%lld\n", ans);
}
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T; read(T);
	while(T--) solve();
	return 0;
}
//Orz zx 2003

#include <bits/stdc++.h>
using namespace std;
const int nut = 495;
bool vis[200];
int T,n,Complex;
stack<int>st;
stack<char>st1;
inline int read()
{
	int x = 0;
	char ch = 0;
	while (!isdigit(ch) && !isalpha(ch) && ch != '(' &&ch != ')' && ch != '^') ch = getchar();
	if (!isdigit(ch)) return ch;
	while (isdigit(ch)) x = x * 10 + ch - 48, ch = getchar();
	return x * 1000;
}
void solve()
{
	memset(vis,0,sizeof(vis));
	bool ok = 1;
	n = read(); n /= 1000;
	int ans = 0;
	int s = read(); s = read();s = read();
	if (s == 1000) {Complex = 0;}
	else  {s = read(), s = read(),Complex = s / 1000;}
	while (!st.empty()) st.pop();
	while (!st1.empty()) st1.pop();
	for (int h = 1; h <= n; ++h)
	{
		s = read();
		if (s == 'E')
		{
			if (st.empty()) {ok = 0; continue;}
			else
			{
				if (!ok) continue;
				int k = st.top(); st.pop();int k1 = st.top(); st.pop();
				char c = st1.top(); st1.pop(); vis[c] = 0;
				k += k1 * 1000;
				if (st.empty()) ans = max(k / 1000 - 1,ans);
				else
				{
					int l = st.top(); st.pop();
					if (k < 0) k = 0;
					l = max(l,k);
					st.push(l);
				} 
			 } 
		}
		else
		{
			s = read();
				int fr = read(), en = read();
				if (!ok) continue;		if (vis[s] && ok) {ok = 0; continue;}
				st1.push(s); vis[s] = 1;
				if (fr > 999 && en == 'n') st.push(1);
				else if (fr == 'n' && en == 'n') st.push(0);
				else if (fr > 999 && en > 999)
				{
					if (fr > en) st.push(-nut); else st.push(0);
				}
				else if (fr == 'n' && en > 999) st.push(-nut);
				st.push(1000);
		}	
	}
	if (!st.empty()) {puts("ERR");}
	else if (!ok) {puts("ERR");}
	else if (ans == Complex) puts("Yes");
	else puts("No");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		solve();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

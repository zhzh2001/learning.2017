#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll n,m;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n < m) swap(m,n);
	printf("%lld\n",m * (n - 1) -n);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

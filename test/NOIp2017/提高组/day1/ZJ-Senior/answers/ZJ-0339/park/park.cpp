#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int maxn = 100010;
const int maxm = 200010;
int n,m,k;
ll p;
struct node
{
	int fr,ne,to,dis;
}e[maxm],e1[maxm];
int he[maxn],he1[maxn],dis[maxn],nume,nume1,a[maxn + 100],numa,y[maxn];
ll f[maxn][55];
bool vis[maxn],legal[maxm];
inline void addside(int p,int q,int r)
{
	e[++nume].ne = he[p];
	e[nume].to = q;
	e[nume].fr = p;
	e[nume].dis = r;
	he[p] = nume;
}
inline void addside1(int p,int q,int r)
{
	e1[++nume1].to = q; e1[nume1].fr = p; e1[nume1].dis = r; e1[nume1].ne = he1[p];
	he1[p] = nume1;
}
void spfa()
{
	queue<int>q;
	q.push(1);
	memset(vis,0,sizeof(vis));
	memset(dis,63,sizeof(dis));
	dis[1] = 0;
	while (!q.empty())
	{
		int f = q.front(); q.pop();
		for (int i = he[f]; i != 0; i = e[i].ne)
		{
			if (dis[e[i].to] > dis[f] + e[i].dis)
			{
				dis[e[i].to] = dis[f] + e[i].dis;
				if (!vis[e[i].to])
				q.push(e[i].to);
			}
		}
	}

}
void pd()
{
	
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		nume = 0;numa = 0;nume1 = 0;
		memset(he,0,sizeof(he));
		memset(he1,0,sizeof(he1));
		memset(legal,0,sizeof(legal));
		memset(f,0,sizeof(f));
		memset(y,0,sizeof(y));
		f[1][0] = 1LL;
		scanf("%d%d%d%lld",&n,&m,&k,&p);
		for (int i = 1; i <= m; ++i)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			addside(u,v,w); addside1(v,u,w);
		}
		if (k < 0) {puts("0"); continue;}
		spfa();	
	//	for (int i = 1; i <= 5; ++i) printf("%d ",dis[i]);putchar('\n');
		for (int i = 1; i <= m; ++i) if (dis[e[i].fr] > dis[e[i].to]) legal[i] = 1;
		for (int i = 1; i <= m; ++i) if (!legal[i]) y[e[i].to] ++;
		queue<int> qu;while (!qu.empty()) qu.pop();
	//	for (int i = 1; i <= n; ++i) printf("%d ",y[i]);putchar('\n');
		for (int i = 1; i <= n; ++i) if (y[i] == 0)qu.push(i);
		bool ok = 1;
		if (qu.empty()) {puts("-1"); continue;}
		while (!qu.empty())
		{
			int f = qu.front(); a[++numa] = f;qu.pop();
			if (numa > n + 9) {puts("-1"); ok = 0; break;}
			for (int i = he[f];i != 0; i = e[i].ne)
			{
				y[e[i].to] --;
				if (y[e[i].to] == 0) qu.push(e[i].to);
			}		 
		}
		if (!ok) continue;
		f[1][0] = 1LL;
		for (int i = 1; i <= n; ++i)
		{
			for (int j = he1[a[i]]; j != 0; j = e1[j].ne)
			{
				int maxx = dis[a[i]] - dis[e1[j].to];
				
				for (int l = 0; l <= k; ++l)
				{
					int cha = e1[j].dis - maxx;
			//		printf("%d %d %d %d\n",a[i],e1[j].to,e1[j].dis,maxx);
					if (l - cha >= 0) f[a[i]][l] = (f[a[i]][l] + f[e1[j].to][l - cha]) % p;
				}
			}
		}
		ll ans = 0;
		for (int i = 0; i <= k; ++i) ans = (ans + f[n][i]) % p;
		printf("%lld\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
}

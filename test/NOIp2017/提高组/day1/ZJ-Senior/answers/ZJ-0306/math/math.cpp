#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
#define ll long long
const int M=1000000;
ll a, b, vis[2*M];
void exgcd(ll a, ll b, ll &x, ll &y){
	if(!b){ x=1, y=0; return; }
	exgcd(b, a%b, x, y);
	ll tmp=x; x=y, y=tmp-y*a/b;
}
ll gcd(ll a, ll b){
	return b?gcd(b, a%b):a;
}
void force(){
	for(int i=0; i<=M; i+=a) vis[i]=1;
	rep(i, 0, M) if(vis[i]) vis[i+b]=1;
	drp(i, M, 0) if(!vis[i]){ printf("%d\n", i); break; }
}
int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld", &a, &b);
if(a<=10000 && b<=10000){ force(); return 0; }
	ll x, y;
	exgcd(a, b, x, y);
	x=(x%b+b)%b;
	printf("%lld\n", a*(b-gcd(x, b))-b);
}

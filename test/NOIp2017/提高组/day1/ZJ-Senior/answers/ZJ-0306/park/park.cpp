#include <map>
#include <cmath>
#include <queue>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
struct node{
	int u, d;
	bool operator < (const node &a) const { return d>a.d; }
};
priority_queue<node> q;
struct Edge{
	int to, next, w;
};
struct state{
	int pos, len;
	bool operator < (const state &a) const { return pos<a.pos || (pos==a.pos && len<a.len); }
};
queue<state> Q;
map<state, int> d;
int n, m, k, mod, f[1111][55], dis[1111], done[1111];
struct G{
	Edge edge[2222];
	int cnt, head[1111];
	void clear(){
		cnt=0; memset(head, 0, sizeof head);
	}
	void AddEdge(int u, int v, int w){
		edge[++cnt]=(Edge){v, head[u], w}; head[u]=cnt;
	}
	void calc(){
		memset(dis, 60, sizeof dis);
		memset(done, 0, sizeof done);
		q.push((node){n, dis[n]=0});
		while(!q.empty())
		{
			node now=q.top(); q.pop();
			int u=now.u;
			if(done[u]) continue; done[u]=1;
			for(int i=head[u]; i; i=edge[i].next)
			{
				int v=edge[i].to;
				if(dis[v]>dis[u]+edge[i].w) q.push((node){v, dis[v]=dis[u]+edge[i].w});
			}
		}
	}
	void dp(){
		d.clear();
		memset(f, 0, sizeof f);
		Q.push((state){ 1, 0 });
		while(!Q.empty())
		{
			state now=Q.front(); Q.pop();
			int u=now.pos;
			for(int i=head[u]; i; i=edge[i].next)
			{
				int v=edge[i].to;
				if(dis[v]>1e9) continue;
				int l=now.len-dis[u]+dis[v]+edge[i].w;
				if(l>k) continue;
				state nx=(state){ v, l };
				if(!d[nx]) Q.push(nx);
				d[nx]++;
			}
		}
		f[1][0]=1;
		Q.push((state){ 1, 0 });
		while(!Q.empty())
		{
			state now=Q.front(); Q.pop();
			int u=now.pos;
			for(int i=head[u]; i; i=edge[i].next)
			{
				int v=edge[i].to;
				if(dis[v]>1e9) continue;
				int l=now.len-dis[u]+dis[v]+edge[i].w;
				if(l>k) continue;
				f[v][l]+=f[u][now.len];
				state nx=(state){ v, l };
				if(f[v][l]>=mod) f[v][l]-=mod;
				if(--d[nx]==0) Q.push(nx);
			}
		}
	}
}G1, G2;
int T;
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out","w",stdout);
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d%d%d%d", &n, &m, &k, &mod);
		G1.clear(); G2.clear();
		rep(i, 1, m)
		{
			int u, v, w;
			scanf("%d%d%d", &u, &v, &w);
			G1.AddEdge(v, u, w);
			G2.AddEdge(u, v, w);
		}
		G1.calc();
		G2.dp();
		int ans=0;
		rep(i, 0, k) ans=(ans+f[n][i])%mod;
		printf("%d\n", ans);
	}
}

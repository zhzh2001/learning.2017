#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
int T, l, vis[100], stk[111], top, last[111], val[111], lev[111];
char s1[111][10], s2[111][10], s3[111][10], s4[111][10], o[10];
int get(char s[10]){
	int n=strlen(s), ret=0;
	rep(i, 0, n-1) ret=ret*10+s[i]-'0';
	return ret;
}
int main(){
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d%s", &l, o);
		int flag=0, top=0;
		memset(vis, 0, sizeof vis);
		memset(stk, 0, sizeof stk);
		memset(val, 0, sizeof val);
		memset(lev, 0, sizeof lev);
		memset(last, 0, sizeof last);
		rep(i, 1, l)
		{
			scanf("%s", s1[i]);
		//	printf("%s", s1[i]);
			if(s1[i][0]=='F')
			{
				scanf("%s%s%s", s2[i], s3[i], s4[i]);
				int ch=s2[i][0]-'a';
				if(vis[ch]) flag=1;
				if(!flag){ stk[++top]=ch; vis[ch]=i; }
			}
			else
			{
				if(top<=0 || flag) flag=1;
				else last[i]=vis[stk[top]], vis[stk[top]]=0;
				top--; lev[i]=top;
			}
		}
		if(flag || top){ puts("ERR"); continue; }
		int ans=0;
		rep(i, 1, l) if(s1[i][0]=='E')
		{
			int x=last[i], y=i, tmp=0;
			if(s3[x][0]=='n' && s4[x][0]!='n'){ val[i]=0; continue; }
			if(s3[x][0]!='n' && s4[x][0]!='n' && get(s3[x])>get(s4[x])){ val[i]=0; continue; }
			rep(j, x+1, y-1) if(s1[j][0]=='E' && lev[j]==lev[i]+1) tmp=max(tmp, val[j]);
			if(s3[x][0]!='n' && s4[x][0]=='n') tmp++;
			val[i]=tmp;
			if(!lev[i]) ans=max(ans, val[i]);
		}
//		printf("%d\n", ans);
		if(ans==0)
			{ if(o[2]=='1') puts("Yes"); else puts("No"); }
		else
			{
				int w=0;
				int ll=strlen(o);
				rep(i, 4, ll-2) w=w*10+o[i]-'0';
				if(ans==w) puts("Yes");
				else puts("No");
			}
	}
}
		/*rep(i, 1, l)
		{
			if(s1[i][0]=='E')
			{
				ended[top]=last[i];
				if(ended[top-1])
					if(ended[top-1]<last[i]) stk[top-1]=max(stk[top-1], stk[top]), top--;
					else stk[top-1]+=stk[top], top--, ended[top]=last[i];
			}
			else
			{
				if(s3[i][0]=='n' && s4[i][0]!='n')
				{
					int tmp=top+1;
					while(tmp!=top)
					{
						i++;
						if(s1[i][0]=='F') tmp++;
						else tmp--;
					}
					top++;
					stk[top]=0; ended[top]=1;
				}
				else if(s3[i][0]=='n' && s4[i][0]=='n' || s3[i][0]!='n' && s4[i][0]!='n')
				{
					top++;
					stk[top]=0; ended[top]=0;
				}
				else
				{
					top++;
					stk[top]=1; ended[top]=0;
				}
			}
		}
		if(stk[1]==0)
			{ if(o[2]=='1') puts("Yes"); else puts("No"); }
		else
			{
				int w=0;
				int ll=strlen(o);
				rep(i, 4, ll-2) w=w*10+o[i]-'0';
				if(stk[1]==w) puts("Yes");
				else puts("No");
			}*/

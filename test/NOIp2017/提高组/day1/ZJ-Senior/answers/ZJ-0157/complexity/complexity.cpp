#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##BEGIN_=(b);i>=i_##BEGIN;--i)
int T;
int L;
struct node{
	int x;
	int f;
	int Is_ok;
}Sk[155];
char str[25];
int mark[155];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>T;
	while(T--){
		memset(mark,0,sizeof(mark));
		cin>>L>>str;
		int top=0,Ep=0,Real=0,ans=0;
		bool flag=1;
		int n=strlen(str);
		if(str[2]=='1')Ep=0;
		else FOR(i,4,n-2)Ep=Ep*10+str[i]-'0';
		FOR(i,1,L){
			char Op;
			cin>>Op;
			if(Op=='F'){
				char a[4],b[4],c[4];
				cin>>a>>b>>c;
				int A=0,B=0,x=strlen(b),y=strlen(c);
				FOR(j,0,x-1)A=A*10+b[j]-'0';
				FOR(j,0,y-1)B=B*10+c[j]-'0';
				if(b[0]=='n')A=1e9;
				if(c[0]=='n')B=1e9;
				int s=a[0]-'a';
				if(mark[s]){
					flag=0;
				}
				if(!flag)continue;
//				cout<<a<<" "<<b<<" "<<c<<endl;
				Sk[top].x=s;
				mark[s]=1;
				Sk[top].f=0;
				Sk[top].Is_ok=1;
				if(A>B)Sk[top].Is_ok=0;
				else if(top)Sk[top].Is_ok=Sk[top-1].Is_ok;
				if(Sk[top].Is_ok){
					if(c[0]=='n'&&b[0]!='n'){
						Sk[top].f=1;
						Real++;
					}
				}
				top++;
			}else {
				if(!flag)continue;
				top--;
				if(top<0){
					flag=0;
				}
				if(!flag)continue;
				Real-=Sk[top].f;
				mark[Sk[top].x]=0;
			}
			if(Real>ans)ans=Real;
		}
		if(top)flag=0;
		if(!flag)puts("ERR");
		else {
			if(ans==Ep)puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

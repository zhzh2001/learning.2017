#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##BEGIN_=(b);i>=i_##BEGIN;--i)
#define M 100005
int n,m,k,md,T;
int head[M],nxt[M<<1],to[M<<1],V[M<<1],tol;
void addedge(int a,int b,int c){nxt[++tol]=head[a];head[a]=tol;to[tol]=b;V[tol]=c;}
typedef pair<int,int>P;
int d[M];
void Dj(int st){
	FOR(i,1,n)d[i]=1e9;
	priority_queue<P>que;
	que.push((P){st,0});
	d[st]=0;
	while(!que.empty()){
		P p=que.top();que.pop();
		int v=p.first;
		if(p.second>d[v])continue;
		for(int i=head[v];~i;i=nxt[i]){
			int y=to[i];
			if(d[y]>d[v]+V[i]){
				d[y]=d[v]+V[i];
				que.push((P){y,d[y]});
			}
		}
	}
}
int ans=0,flag=0;
void dfs(int x,int dis){
	if(dis>d[n]+k)return;
	if(x==n){
		ans=(ans+1)%md;
	}
	for(int i=head[x];~i;i=nxt[i]){
		int y=to[i];
		if(!V[i]){
			flag=1;
			return;
		}
		dfs(y,dis+V[i]);
	}
}
void solve(){
	ans=0;
	dfs(1,0);
	printf("%d\n",flag==0?ans:-1);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	cin>>T;
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&md);
		int a,b,c;
		FOR(i,1,n)head[i]=-1;
		FOR(i,1,m){
			scanf("%d%d%d",&a,&b,&c);
			addedge(a,b,c);
		}
		Dj(1);
		solve();
	}
	return 0;
}

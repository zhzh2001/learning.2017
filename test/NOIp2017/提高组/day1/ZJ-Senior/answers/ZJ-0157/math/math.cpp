#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##BEGIN_=(b);i>=i_##BEGIN;--i)
long long a,b;
struct P10{
	int check(int x){
		FOR(i,0,x){
			if(i*a>x)break;
			int tmp=x-i*a;
			if(tmp%b==0)return 0;
		}
		return 1;
	}
	void solve(){
		int ans=0;
		FOR(i,1,a*b){
			if(check(i))ans=i;
		}
		printf("%d\n",ans);
	}
}p10;
struct PAC{
	void solve(){
		long long ans=1ll*a*b-a-b;
		printf("%lld\n",ans);
	}
}pac;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a<=50&&b<=50)p10.solve();
	else pac.solve();
	return 0;
}

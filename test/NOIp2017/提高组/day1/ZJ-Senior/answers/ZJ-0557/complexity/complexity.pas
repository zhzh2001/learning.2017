var t,i,j,k,v:longint;
    flag:array[1..255] of boolean;
    s:ansistring;
    left,right,space,time,line,max:longint;
    match,rec_mi,rec_n:longint;
    error,flag_n:boolean;
    space1,space2:longint;
    x,y:longint;
    pd_x,pd_y,wait:boolean;

Function Read_num(l,r:longint):longint;
var temp,i:longint;
begin
  temp:=0;
  for i:=l to r do
   temp:=temp*10+(ord(s[i])-48);
  exit(temp);
end;

Procedure Solve();
begin
 for k:=length(s) downto 1 do
  begin
   if s[k]=' ' then
   if space2=0 then space2:=k
    else if space1=0 then
     begin
      space1:=k;
      break;
     end;
  end;
 for v:=(space1+1) to (space2-1) do
  if s[v]='n' then
   begin
    pd_x:=true;
    break;
   end;
 for v:=(space2+1) to length(s) do
  if s[v]='n' then
   begin
    pd_y:=true;
    break;
   end;
  if not pd_x then x:=read_num(space1+1,space2-1);
  if not pd_y then y:=read_num(space2+1,length(s));
end;

Procedure Print;
begin
 if error then writeln('ERR')
  else if ((not flag_n) and (max=0)) or ((flag_n) and (max=time)) then writeln('Yes')
   else
    begin
     writeln('No');
    end;
end;

begin
  assign(input,'complexity.in');
  reset(input);
  assign(output,'complexity.out');
  rewrite(output);
  readln(t);
  for i:=1 to t do
   begin
    fillchar(flag,sizeof(flag),0);
    readln(s);
    space:=0; left:=0; right:=0;
    rec_mi:=0; match:=0;
    rec_n:=0; max:=0;
    error:=false;
    flag_n:=false;
    wait:=false;
    for j:=1 to length(s) do
     begin
      if (s[j]=' ') and (space=0) then space:=j-1;
      if (s[j]='(') and (left=0) then left:=j+1;
      if (s[j]=')') and (right=0) then right:=j-1;
      if (s[j]='^') and (rec_mi=0) then rec_mi:=j;
      if (s[j]='n') then flag_n:=true;
     end;
    line:=read_num(1,space);
    if not flag_n then time:=read_num(left,right)
     else time:=read_num(rec_mi+1,right);
    for j:=1 to line do
     begin
      if match=0 then fillchar(flag,sizeof(flag),0);
      if wait and (match=0) then wait:=false;
      space1:=0; space2:=0;
      pd_x:=false; pd_y:=false;
      readln(s);
      if match<0 then
       begin
        error:=true;
        continue;
       end;
      solve;
      if s[1]='F' then
       begin
        if flag[ord(s[3])] then
         begin
          error:=true;
          continue;
         end
          else flag[ord(s[3])]:=true;
        inc(match);
        if ((pd_x) or ((not pd_x) and (not pd_y) and (x>y))) and (not wait) then
         begin
          if rec_n>max then max:=rec_n;
          wait:=true;
         end;
        if (pd_y) and (not pd_x) then inc(rec_n);
       end;
      if s[1]='E' then
       begin
        if (match=1) and (not wait) then
         begin
          if rec_n>max then max:=rec_n;
          rec_n:=0;
         end;
        dec(match);
       end;
     end;
    if match<>0 then error:=true;
    Print;
   end;
  close(input);
  close(output);
end.
var t,n,m,k,p,i,j,x,y,v,num,uu:longint;
    s,w,e,cost:array[0..100000] of longint;
    q,dist:array[0..100000*2] of longint;
    flag:array[0..100000] of boolean;

Procedure Add(x,y,v,i:longint);
begin
  cost[i]:=v;
  w[i]:=s[x];
  s[x]:=i;
  e[i]:=y;
end;

Procedure Spfa;
var head,tail,temp1,temp2:longint;
begin
  head:=0; tail:=1;
  flag[1]:=true; q[1]:=1;
  while head<tail do
   begin
    inc(head);
    temp1:=q[head];
    flag[temp1]:=false;
    temp2:=s[head];
    while temp2<>0 do
     begin
      if dist[e[temp2]]>dist[e[head]]+cost[temp1] then
       begin
        dist[e[temp2]]:=dist[e[head]]+cost[temp1]
       end;
      if not Flag[e[temp2]] then
       begin
        flag[e[temp2]]:=true;
        inc(tail);
        q[tail]:=e[temp2];
       end;
      temp2:=w[temp2];
     end;
   end;
end;

begin
  assign(input,'park.in');
  reset(input);
  assign(output,'park.out');
  rewrite(output);

  readln(t);
  for i:=1 to t do
   begin
    fillchar(q,sizeof(q),0);
    fillchar(s,sizeof(s),0);
    fillchar(w,sizeof(w),0);
    fillchar(e,sizeof(e),0);
    fillchar(cost,sizeof(cost),0);
    fillchar(dist,sizeof(dist),0);
    fillchar(flag,sizeof(flag),0);
    num:=0;
    readln(n,m,k,p);
    for j:=1 to m do
     begin
      readln(x,y,v);
      Add(x,y,v,j);
     end;
    Spfa;
    for uu:=1 to n do if dist[uu]<=(dist[n]+k) then inc(num);
    writeln(num);
   end;

  close(input);
  close(output);
end.
procedure fopen;
begin
	assign(input,'complexity.in');
	assign(output,'complexity.out');
	reset(input);
	rewrite(output);
end;


procedure fclose;
begin
	close(input);
	close(output);
end;


var
        n,l,r,xx,x,y,i,ll:longint;
        ch:char;
        st:ansistring;
        stack,stack3:array[0..110] of longint;
        stack2:array[0..110] of char;
        used:array['a'..'z'] of boolean;
        bo:boolean;


function get():longint;
var
        t:longint;
        ch:char;
begin
        ch := st[ll];
        inc(ll);
        while not((ch >= '0') and (ch <= '9') or (ch = 'n')) do
         begin
                ch := st[ll];
                inc(ll);
         end;
        if ch = 'n' then exit(1000);
        t := 0;
        while (ch >= '0') and (ch <= '9') do
         begin
                t := t * 10 + ord(ch) - 48;
                ch := st[ll];
                inc(ll);
         end;
        exit(t);
end;


procedure work();
begin
        readln(n,st);
        l := 1;
        while st[l] <> 'O' do inc(l);
        r := length(st);
        while st[r] <> ')' do dec(r);
        inc(l,2);
        dec(r);
        st := copy(st,l,(r - l) + 1);
        if st = '1' then
         xx := 0
        else
         begin
                delete(st,1,2);
                val(st,xx);
         end;

        fillchar(used,sizeof(used),false);
        fillchar(stack,sizeof(stack),0);
        fillchar(stack3,sizeof(stack3),0);
        used['n'] := true;
        l := 0;
        bo := true;
        for i := 1 to n do
         begin
                readln(st);
                st := st + '  ';
                ch := st[1];
                ll := 2;
                if bo then
                if ch = 'E' then
                 begin
                        if l = 0 then
                         begin
                                writeln('ERR');
                                bo := false;
                                continue;
                         end;
                        dec(l);
                        used[stack2[l + 1]] := false;
                        if stack3[l + 1] = -1 then
                         stack[l] := 0
                        else
                        if stack[l + 1] + stack3[l + 1] > stack[l] then
                         stack[l] := stack[l + 1] + stack3[l + 1];
                        stack[l + 1] := 0;
                        stack3[l + 1] := 0;
                 end
                else
                if ch = 'F' then
                 begin
                        inc(ll,2);
                        ch := st[ll - 1];
                        if used[ch] then
                         begin
                                writeln('ERR');
                                bo := false;
                                continue;
                         end;
                        inc(l);
                        used[ch] := true;
                        stack2[l] := ch;
                        x := get();
                        y := get();
                        if x > y then
                         begin
                                stack3[l] := -1;
                         end
                        else
                        if (y < 1000) or (y = 1000) and (x = 1000) then
                         stack3[l] := 0
                        else
                         stack3[l] := 1;
                 end;
         end;
        if bo then
        if l <> 0 then
         begin
                writeln('ERR');
                exit;
         end
        else
         begin
                if (stack[0] + stack3[0]) <> xx then
                 writeln('No')
                else
                 writeln('Yes');
         end;
end;


var
        ii,t:longint;
begin
        fopen;
        readln(t);
        for ii := 1 to t do
         work();
	fclose;
end.

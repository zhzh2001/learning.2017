procedure fopen;
begin
	assign(input,'park.in');
	assign(output,'park.out');
	reset(input);
	rewrite(output);
end;


procedure fclose;
begin
	close(input);
	close(output);
end;


var
        vis:array[0..100010] of boolean;
        dis:array[0..100010] of int64;
        heap:array[0..5000010] of longint;
        lh:longint;
        bo:boolean;
        maxdis:int64;
        l1,l2,n,m,k,p,ans,x,y,z:longint;
        f1,f2:array[0..100010] of longint;
        len1,len2,go1,go2,next1,next2:array[0..400010] of longint;


procedure swap(var x,y:longint);
var
        t:longint;
begin
        t := x; x := y; y := t;
end;


procedure push(x:longint);
var
        i:longint;
begin
        inc(lh);
        heap[lh] := x;
        i := lh;
        while (i > 1) and (dis[heap[i >> 1]] > dis[heap[i]]) do
         begin
                swap(heap[i],heap[i >> 1]);
                i := i >> 1;
         end;
end;


function pan(x,y:longint):boolean;
begin
        if dis[heap[x]] < dis[heap[y]] then exit(true) else exit(false);
end;


function pop():longint;
var
        i:longint;
begin
        dec(lh);
        pop := heap[1];
        heap[1] := heap[lh + 1];
        i := 1;
        while i * 2 <= lh do
         begin
                if pan(i << 1,i) then
                 if (i << 1 + 1 <= lh) and (pan(i << 1 + 1,i << 1)) then
                  begin
                        swap(heap[i],heap[i << 1 + 1]);
                        i := i << 1 + 1;
                  end
                 else
                  begin
                        swap(heap[i],heap[i << 1]);
                        i := i << 1;
                  end
                else
                 if (i << 1 + 1 <= lh) and (pan(i << 1 + 1,i)) then
                  begin
                        swap(heap[i],heap[i << 1 + 1]);
                        i := i << 1 + 1;
                  end
                 else
                  break;
         end;
end;


procedure Dij();
var
        i,j,t,tt:longint;
begin
        for i := 1 to n do dis[i] := 20000000010;
        fillchar(vis,sizeof(vis),0);
        dis[n] := 0;
        push(n);
        for j := 1 to n do
         begin
                t := pop();
                vis[t] := true;
                i := f2[t];
                while i <> 0 do
                 begin
                        tt := go2[i];
                        if vis[tt] = false then
                         if dis[tt] > dis[t] + len2[i] then
                          begin
                                dis[tt] := dis[t] + len2[i];
                                push(tt);
                          end;
                        i := next2[i];
                 end;
         end;
end;


procedure add1(x,y,z:longint);
begin
        inc(l1);
        next1[l1] := f1[x];
        f1[x] := l1;
        go1[l1] := y;
        len1[l1] := z;
end;


procedure add2(x,y,z:longint);
begin
        inc(l2);
        next2[l2] := f2[x];
        f2[x] := l2;
        go2[l2] := y;
        len2[l2] := z;
end;


procedure dfs(x:longint);
var
        i,t:longint;
begin
        vis[x] := true;
        i := f1[x];
        while i <> 0 do
         begin
                t := go1[i];
                if vis[t] then
                 begin
                        if dis[t] = dis[x] + len1[i] then
                         begin
                                bo := true;
                                exit;
                         end;
                 end
                else
                 begin
                        dis[t] := dis[x] + len1[i];
                        dfs(t);
                 end;
                if bo then exit;
                i := next1[i];
         end;
        vis[x] := false;
        dis[x] := 20000000010;
end;


procedure dfs2(x:longint; len:int64);
var
        i,t:longint;
begin
        if len + dis[x] > maxdis then exit;
        if (x = n) and (len <= maxdis) then
         begin
                inc(ans);
                ans := ans mod p;
         end;
        i := f1[x];
        while i <> 0 do
         begin
                t := go1[i];
                dfs2(t,len + len1[i]);
                i := next1[i];
         end;
end;


procedure work();
var
        i:longint;
begin
        read(n,m,k,p);
        for i := 1 to m do
         begin
                read(x,y,z);
                add1(x,y,z);
                add2(y,x,z);
         end;
        for i := 2 to n do dis[i] := 20000000010;
        dis[1] := 0;
        fillchar(vis,sizeof(vis),false);
        bo := false;
        dfs(1);
        if bo then
         begin
                writeln(-1);
                exit;
         end;
        Dij();
        maxdis := dis[1] + k;
        ans := 0;
        dfs2(1,0);
        writeln(ans);
end;


var
        t,ii:longint;
begin
	fopen;
        read(t);
        for ii := 1 to t do work();
	fclose;
end.

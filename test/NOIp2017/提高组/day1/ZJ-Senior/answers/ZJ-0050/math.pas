procedure fopen;
begin
	assign(input,'math.in');
	assign(output,'math.out');
	reset(input);
	rewrite(output);
end;


procedure fclose;
begin
	close(input);
	close(output);
end;


var
        a,b,t:int64;


begin
	fopen;
        read(a,b);
        if a > b then
         begin
                t := a; a := b; b := t;
         end;
        writeln(b*(a - 1) - a);
	fclose;
end.

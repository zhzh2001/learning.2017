#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
#define N 100100
#define M 200100
inline int read(){
  char last='+',ch=getchar();
  while (ch<'0'||ch>'9'){if (ch=='-') last='-';ch=getchar();};
  int tmp=0;
  while (ch>='0'&&ch<='9'){tmp=tmp*10+(ch-'0');ch=getchar();};
  if (last=='-') tmp=-tmp;
  return tmp;
}
int n,k,P;
inline int jia(int x,int y){
	if (x+y>=P) return x+y-P;
	else return x+y;
}
int num,tt,tim;
int dis[N],low[N],dfn[N],hed[N];
int vet[M],val[M],Next[M];
bool flag[N],huan[N];
int dp[N][55];
int dp1[N][55];
bool FF[N][55];
int q1[N*55],q[N*55];
#define NM 5000001
void add(int u,int v,int z){
  ++num;
  vet[num]=v;
  val[num]=z;
  Next[num]=hed[u];
  hed[u]=num;
}
void spfa(int x){
  for (int i=1;i<=n;++i) dis[i]=1<<29,flag[i]=false;
  dis[1]=0;
  int HH=0,TT=0;
  q[0]=1;
  flag[1]=true;
	while (HH<=TT){
    int u=q[HH];
    ++HH;
    flag[u]=false;
    for (int i=hed[u];i!=-1;i=Next[i]){
      int v=vet[i];
      if (dis[v]>dis[u]+val[i]){
        dis[v]=dis[u]+val[i];
        if (flag[v]==false){
          ++TT;
          flag[v]=true;
          q[TT]=v;
				}
			}
		}
	}
}
void tarjan(int u){
  ++tim;
  dfn[u]=low[u]=tim;
  ++tt;
  q[tt]=u;
  for (int i=hed[u];i!=-1;i=Next[i])
	if (val[i]==0){
    int v=vet[i];
    if (dfn[v]==0){
      tarjan(v);
      low[u]=min(low[u],low[v]);
		}else low[u]=min(low[u],dfn[v]);
	}
	if (low[u]==dfn[u]){
		int nnn=0;
	  while (q[tt]!=u){
	    huan[q[tt]]=true;
	    --tt;
	    ++nnn;
		}
		if (nnn>0) huan[q[tt]]=true;
		--tt;
	}
}
void check(){
	tim=0;tt=0;
	for (int i=0;i<=n;++i){
	  dfn[i]=0;
	  low[i]=0;
	  q[i]=0;
	}
	for (int i=1;i<=n;++i)
	if (dfn[i]==0) tarjan(i);
}
void work(int u){
  for (int i=1;i<=n;++i) 
    for (int j=0;j<=k;++j){
		  dp[i][j]=0,FF[i][j]=false;
		  dp1[i][j]=0;
		}
  if (huan[1]==false) dp[1][0]=1;
  else dp[1][0]=1<<29;
  int HH=0,TT=0;
  q[0]=1;q1[0]=0;
  FF[1][0]=true;
	while (HH<=TT){
    int u=q[HH%NM];
    int zz=q1[HH%NM];
    ++HH;
    FF[u][zz]=false;
    for (int i=hed[u];i!=-1;i=Next[i]){
      int v=vet[i];
      int ww=zz+dis[u]+val[i]-dis[v];
      if (ww>k) continue;
      if (dp[u][zz]==1<<29||huan[v]==true||dp[v][ww]==1<<29){
        if (dp[v][ww]!=1<<29){
          dp[v][ww]=1<<29;
          if (FF[v][ww]==false){
        		++TT;
        		FF[v][ww]=true;
        		q[TT%NM]=v;
        		q1[TT%NM]=ww;
					}
				}
			}else{
				if (FF[v][ww]==false) dp1[v][ww]=dp[v][ww];
				int yh=dp[u][zz]-dp1[u][zz];
				if (yh==0) continue;
				if (yh<0) yh+=P;
        dp[v][ww]=jia(dp[v][ww],yh);
        if (FF[v][ww]==false){
          ++TT;
          FF[v][ww]=true;
          q[TT%NM]=v;
          q1[TT%NM]=ww;
          //cout<<TT<<endl;
			  }
			}
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
  int T;
  T=read();
  while (T--){
  	int m;
  	n=read();m=read();k=read();P=read();
  	for (int i=1;i<=n;++i) hed[i]=-1,huan[i]=false;
  	num=0;
  	for (int i=1;i<=m;++i){
  	  int u,v,z;
  	  u=read();v=read();z=read();
  	  add(u,v,z);
		}
		spfa(1);
		check();
		work(1);
		int ans=0;
		for (int i=0;i<=k;++i)
		if (dp[n][i]!=1<<29){
		  ans=jia(ans,dp[n][i]);
		}else{
		  ans=-1;
		  break;
		}
		printf("%d\n",ans);
  }
  return 0;
}

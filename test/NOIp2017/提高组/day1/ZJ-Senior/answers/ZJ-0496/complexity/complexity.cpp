#include<cstdio>
#include<cstring>
const int N=105,X=100;
int n,nk,i,m,cnt,tot,ans,d[N],cs[N];
char s[100];
bool hv[26],flag;
char get_c(){
	char c=getchar();
	for (;c<'A'||c>'Z';c=getchar());
	return c;
}
char get_cc(){
	char c=getchar();
	for (;c<'a'||c>'z';c=getchar());
	return c;
}
int get(){
	char c=getchar();
	for (;c!='n'&&(c<48||c>57);c=getchar());
	if (c=='n') return 101;
	int k=0;
	for (;c>47&&c<58;c=getchar()) k=k*10+c-48;
	return k;
}
int count_num(){
	if (s[2]=='1') return 0;
	int k=0;
	for (int i=4;s[i]>47&&s[i]<58;i++)
		k=k*10+s[i]-48;
	return k;
}
int pd(int x,int y){
	if (x<=X&&y<=X) return x<=y?0:-1;
	if (x>X&&y>X) return 0;
	return x<=X?1:-1;
}
void work_F(){
	int c=get_cc()-97;
	if (hv[c]) flag=1;
	hv[c]=1;m++;cs[m]=c;
	int x=get(),y=get();
	int k=pd(x,y);d[m]=k;
	if (k==-1) tot++;if (k==1) cnt++;
	if (!tot&&cnt>ans) ans=cnt;
}
void work_E(){
	if (!m){flag=1;return;}
	hv[cs[m]]=0;
	if (d[m]==-1) tot--;
	if (d[m]==1) cnt--;
	m--;
}
void work(){
	char c=get_c();
	if (c=='F') work_F();
	else work_E();
}
void init(){
	m=0;cnt=0;tot=0;ans=0;flag=0;
	memset(hv,0,sizeof(hv));
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int TEST;scanf("%d",&TEST);
	for (;TEST--;){
		scanf("%d%s",&n,s);
		nk=count_num();init();
		for (;n--;) work();
		if (m>0||flag){puts("ERR");continue;}
		puts(ans==nk?"Yes":"No");
	}
}

#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1e5+5,M=2e5+5,PN=51*N,PM=102*M,inf=1e9;
int n,nk,m,i,j,mo;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
int he[N];struct edge{int l,to,v;}e[M];
struct heap{int x,k;};
bool operator < (heap A,heap B){return A.k>B.k;}
int nn,dis[N],q[N],pos[N];bool in[N];
void up(int p){
	for (;p&&dis[q[p]]<dis[q[p>>1]];p>>=1)
		swap(q[p],q[p>>1]),pos[q[p]]=p,pos[q[p>>1]]=p>>1;
}
void down(int p){
	for (;(p<<1)<=nn;){
		int x=p<<1;if (dis[q[x+1]]<dis[q[x]]) x++;
		if (dis[q[p]]<dis[q[x]]) break;
		swap(q[p],q[x]);pos[q[p]]=p;pos[q[x]]=x;p=x;
	}
}
void dijstra(){
	int i,j;
	for (i=1;i<=n;i++) dis[i]=inf,
		in[i]=0,q[i]=i,pos[i]=i;
	dis[1]=0;nn=n;
	for (i=1;i<n;i++){
		int x=q[1];in[x]=1;
		q[1]=q[nn];pos[q[1]]=1;nn--;down(1);
		for (j=he[x];j;j=e[j].l){
			int y=e[j].to;
			if (!in[y]&&dis[x]+e[j].v<dis[y]){
				dis[y]=dis[x]+e[j].v;up(pos[y]);
			}
		}
	}
}
int nd,bt,hb[PN],hd[PN],dg[PN],f[PN];
struct bian{int l,to;}b[PM];
bool can[PN],vis[PN],ins[PN];
void line(int x,int y){
	b[++bt].l=hb[x];hb[x]=bt;b[bt].to=y;dg[y]++;
	b[++bt].l=hd[y];hd[y]=bt;b[bt].to=x;
}
void add_line(){
	int i,j,x;nd=(nk+1)*n;bt=0;
	for (i=1;i<=nd;i++) hb[i]=hd[i]=
		can[i]=vis[i]=ins[i]=dg[i]=f[i]=0;
	for (x=1;x<=n;x++) for (i=he[x];i;i=e[i].l){
		int y=e[i].to,k=dis[x]+e[i].v-dis[y];
		for (j=0;j+k<=nk;j++) line(j*n+x,(j+k)*n+y);
	}
}
void find(int x){
	can[x]=1;
	for (int i=hd[x];i;i=b[i].l)
		if (!can[b[i].to]) find(b[i].to);
}
void go_from_n(){
	for (i=0;i<=nk;i++) find(i*n+n);
}
bool dfs(int x){
	ins[x]=1;vis[x]=1;
	for (int i=hb[x];i;i=b[i].l){
		int y=b[i].to;if (!can[y]) continue;
		if (ins[y]) return 1;
		if (!vis[y]) if (dfs(y)) return 1;
	}
	ins[x]=0;return 0;
}
int h,t,qd[PN];
void cal_num(){
	int i;h=0;t=0;f[1]=1;
	for (i=1;i<=nd;i++) if (!dg[i]) qd[++t]=i;
	while (h!=t){
		int x=qd[++h];
		for (i=hb[x];i;i=b[i].l){
			int y=b[i].to;
			if (!can[y]) continue;
			dg[y]--;f[y]=(f[y]+f[x])%mo;
			if (dg[y]==0) qd[++t]=y;
		}
	}
	int ans=0;
	for (i=0;i<=nk;i++) ans=(ans+f[i*n+n])%mo;
	printf("%d\n",ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (int TEST=read();TEST--;){
		n=read();m=read();nk=read();mo=read();
		for (i=1;i<=n;i++) he[i]=0;
		for (i=1;i<=m;i++){
			int x=read(),y=read();
			e[i].l=he[x];he[x]=i;
			e[i].to=y;e[i].v=read();
		}
		dijstra();add_line();go_from_n();
		if (dfs(1)){puts("-1");continue;}
		cal_num();
	}
}

var T,test,n,cnt,i,id,l,r,time,time_n,tail:longint;
flag:boolean;
st,ss:string;
used,s:array[0..105]of string;
stack,E,F:array[-105..105]of longint;
function work(l,r:longint):longint;
var i,temp,j,x,y,delta:longint;
ss:string;
begin
        if flag=false then exit(0);
        if l>0 then
        begin
                ss:=copy(s[l],3,1);
                for j:=1 to tail do
                if used[j]=ss then
                begin
                        flag:=false;
                        exit(0);
                end;
                inc(tail);
                used[tail]:=ss;
        end;
        if r-l=1 then
        begin
                dec(tail);
                if pos('n',s[l])=length(s[l]) then exit(1);
                exit(0);
        end;
        work:=0;
        if pos('n',s[l])=length(s[l]) then delta:=1
        else delta:=0;
        i:=l+1;
        if l=0 then delta:=0;
        while i<r do
        begin
                temp:=work(i,E[i])+delta;
                if temp>work then work:=temp;
                i:=E[i]+1;
        end;
        dec(tail);
        if (pos('n',s[l])>0) and (delta=0) then exit(0);
        if pos('n',s[l])=0 then
        begin
                i:=5;
                ss:='';
                while s[l,i] in ['0'..'9'] do
                begin
                        ss:=ss+s[l,i];
                        inc(i);
                end;
                val(ss,x);
                inc(i);
                ss:='';
                while s[l,i] in ['0'..'9'] do
                begin
                        ss:=ss+s[l,i];
                        inc(i);
                end;
                val(ss,y);
                if x>y then exit(0);
        end;
end;
BEGIN
        assign(input,'complexity.in');
        reset(input);
        assign(output,'complexity.out');
        rewrite(output);
        readln(T);
        for test:=1 to T do
        begin
                readln(st);
                ss:=copy(st,1,pos(' ',st)-1);
                val(ss,n);
                cnt:=0;
                flag:=true;
                for i:=1 to n do
                begin
                        readln(s[i]);
                        if s[i]<>'E' then
                        begin
                                inc(cnt);
                                stack[cnt]:=i;
                        end
                        else
                        begin
                                id:=stack[cnt];
                                E[id]:=i;
                                F[i]:=id;
                                dec(cnt);
                        end;
                        if cnt<0 then flag:=false;
                end;
                if (flag=false) or (cnt<>0) then
                begin
                        writeln('ERR');
                        continue;
                end;
                if pos('n',st)>0 then
                begin
                        l:=pos('^',st)+1;
                        r:=pos(')',st)-1;
                        st:=copy(st,l,r-l+1);
                        val(st,time);
                end
                else time:=0;
                tail:=0;
                time_n:=work(0,n+1);
                if flag=false then writeln('ERR')
                else if time_n=time then writeln('Yes')
                else writeln('No');
        end;
        close(input);
        close(output);
END.
var a,b,i,j,now,ans:longint;
q:array[0..1234567]of longint;
flag:boolean;
BEGIN
        assign(input,'math.in');
        reset(input);
        assign(output,'math.out');
        rewrite(output);
        readln(a,b);
        if (a=1) or (b=1) then
        begin
                writeln(0);
                close(input);
                close(output);
                halt;
        end;
        if a>b then
        begin
                i:=a;
                a:=b;
                b:=i;
        end;
        writeln((a-1)*b-a);
        close(input);
        close(output);
END.

var T,test,cnt,n,m,k,mo,ans,i,u,v,w,mmin:longint;
first,next,dis,son:array[0..200005]of longint;
procedure add(u,v,w:longint);
begin
        inc(cnt);
        next[cnt]:=first[u];
        first[u]:=cnt;
        son[cnt]:=v;
        dis[cnt]:=w;
end;
procedure dfs(now,min,dist:longint);
var i,u,v:longint;
begin
        if ans=-1 then exit;
        if dist>maxlongint div 3 then exit;
        if (dist>min+k) and (min=mmin) then exit;
        if now=n then
        begin
                ans:=(ans+1) mod mo;
                if dist=0 then
                begin
                        ans:=-1;
                        exit;
                end;
        end;
        i:=first[now];
        u:=now;
        while i<>-1 do
        begin
                v:=son[i];
                if dis[i]<min then dfs(v,dis[i],dist+dis[i])
                else dfs(v,min,dist+dis[i]);
                i:=next[i];
        end;
end;
BEGIN
        assign(input,'park.in');
        reset(input);
        assign(output,'park.out');
        rewrite(output);
        readln(T);
        for test:=1 to T do
        begin
                cnt:=0;
                fillchar(first,sizeof(first),255);
                fillchar(next,sizeof(next),255);
                readln(n,m,k,mo);
                mmin:=maxlongint;
                for i:=1 to m do
                begin
                        readln(u,v,w);
                        add(u,v,w);
                        if w<mmin then mmin:=w;
                end;
                ans:=0;
                dfs(1,maxlongint div 3,0);
                writeln(ans);
        end;
        close(input);
        close(output);
END.

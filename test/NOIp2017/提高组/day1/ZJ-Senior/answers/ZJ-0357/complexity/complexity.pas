const
      INF=maxlongint;

type
     stacktype=record
      v:char;
      x,y:longint;
     end;

var
    stack:array[0..105]of stacktype;
    used:array['a'..'z']of boolean;
    top,t,ti:longint;

procedure asson;
 begin
       assign(input,'complexity.in'); reset(input);
       assign(output,'complexity.out'); rewrite(output);
 end;

procedure assoff;
 begin
       close(input); close(output);
 end;

procedure push(chv:char;xx,yy:longint);
 begin
       inc(top);
       used[chv]:=true;
       stack[top].v:=chv;
       stack[top].x:=xx;
       stack[top].y:=yy;
 end;

function pop:stacktype;
 begin
       used[stack[top].v]:=false;
       pop:=stack[top];
       dec(top);
 end;

procedure cc;    //Procedure Check-Code
 var st,st1,stx,sty:string;
     k,k1,k2,w,maxw,sumw,i,l,xx,yy:longint;
     chv,nv:char;
     lastf:stacktype;
     flag,flage:boolean;
 begin
       readln(st);
       //Get L
       k:=pos(' ',st);
       val(copy(st,1,k-1),l);
       //Get the complexity of the program
       k1:=pos('(',st);
       k2:=pos(')',st);
       st1:=copy(st,k1+1,k2-k1-1);
       if (st1='1') then w:=0   //O(k)
        else val(copy(st1,3,5),w);    //O(n^w)
       maxw:=0; top:=0; sumw:=0; flag:=true; flage:=false;
       fillchar(used,sizeof(used),0);
       for i:=1 to l do
       begin
             readln(st);
             if (st[1]='F') then
             begin
                   chv:=st[3];
                   if used[chv] then flage:=true;
                   st1:=copy(st,5,length(st)-4);
                   k:=pos(' ',st1);
                   stx:=copy(st1,1,k-1);
                   if (stx<>'n') then val(stx,xx)
                    else xx:=INF;
                   sty:=copy(st1,k+1,length(st)-k);
                   if (sty<>'n') then val(sty,yy)
                    else yy:=INF;
                   if ((xx>yy) and flag) then
                   begin
                         flag:=false;
                         nv:=chv;
                   end;
                   push(chv,xx,yy);
             end
              else
              begin
                    if (top=0) then
                    begin
                          flage:=true;
                          continue;
                    end;
                    lastf:=pop;
                    if flag then
                    begin
                          if (lastf.x=INF) then continue;
                          if (lastf.y<INF) then continue;
                          inc(sumw);
                          if (top=0) then
                          begin
                                if (sumw>maxw) then maxw:=sumw;
                                sumw:=0;
                          end;
                    end
                     else if (lastf.v=nv) then flag:=true;
              end;
       end;
       if ((top>0) or flage) then writeln('ERR')
        else
        begin
              if (sumw>maxw) then maxw:=sumw;
              if (maxw=w) then writeln('Yes')
               else writeln('No');
        end;
 end;

begin
      asson;

      readln(t);
      for ti:=1 to t do cc;

      assoff;
end.
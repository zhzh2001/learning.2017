type
     edgetype=record
      point,dist,next:longint;
     end;

     aedge=array[0..200005]of edgetype;

     ap=array[0..100005]of longint;

var
    edge,edge0:aedge;
    last,last0,dis,mdt:ap;
    t,ti,n,m,k,p,maxdis,ans:longint;
    a,b,c:array[0..200005]of longint;
    q,qd:array[0..300005]of longint;
    vis:array[0..100005]of boolean;

{Notes :
         Array :
                 mdt - Min Dist to vertex T
}

procedure asson;
 begin
       assign(input,'park.in'); reset(input);
       assign(output,'park.out'); rewrite(output);
 end;

procedure assoff;
 begin
       close(input); close(output);
 end;

procedure addedge(ei,u,v,d:longint);
 begin
       edge[ei].point:=v;
       edge[ei].dist:=d;
       edge[ei].next:=last[u];
       last[u]:=ei;
 end;

procedure handmod(var n1:longint);
 begin
       while (n1>=p) do dec(n1,p);
 end;

procedure spfa;
 var h,t,now,v:longint;
 begin
       fillchar(dis,sizeof(dis),127);
       fillchar(vis,sizeof(vis),0);

       dis[1]:=0; h:=0; t:=1; q[1]:=1; vis[1]:=true;
       while (h<t) do
       begin
             inc(h);
             now:=last0[q[h]];
             while (now>0) do
             begin
                   v:=edge0[now].point;
                   if (dis[v]>dis[q[h]]+edge0[now].dist) then
                   begin
                         dis[v]:=dis[q[h]]+edge0[now].dist;
                         if not vis[v] then
                         begin
                               vis[v]:=true;
                               inc(t);
                               q[t]:=v;
                         end;
                   end;
                   now:=edge0[now].next;
             end;
             vis[q[h]]:=false;
       end;
 end;

procedure respfa;    //Reverse - SPFA
 var h,t,now,v:longint;
 begin
       fillchar(mdt,sizeof(mdt),127);
       fillchar(vis,sizeof(vis),0);

       mdt[n]:=0; h:=0; t:=1; q[1]:=n; vis[n]:=true;
       while (h<t) do
       begin
             inc(h);
             now:=last[q[h]];
             while (now>0) do
             begin
                   v:=edge[now].point;
                   if (mdt[v]>mdt[q[h]]+edge[now].dist) then
                   begin
                         mdt[v]:=mdt[q[h]]+edge[now].dist;
                         if not vis[v] then
                         begin
                               vis[v]:=true;
                               inc(t);
                               q[t]:=v;
                         end;
                   end;
                   now:=edge[now].next;
             end;
             vis[q[h]]:=false;
       end;
 end;

procedure dfs(u,res:longint);
 var now,v:longint;
 begin
       now:=last0[u];
       while (now>0) do
       begin
             v:=edge0[now].point;
             if (res-edge0[now].dist>=mdt[v]) then
             begin
                   if (v<>n) then dfs(v,res-edge0[now].dist)
                    else
                    begin
                          inc(ans);
                          handmod(ans);
                    end;
             end;
             now:=edge0[now].next;
       end;
 end;

procedure travelpark;
 var i:longint;
 begin
       filldword(last,sizeof(last) >> 2,0);

       read(n,m,k,p);
       for i:=1 to m do
       begin
             read(a[i],b[i],c[i]);
             addedge(i,a[i],b[i],c[i]);
       end;

       edge0:=edge;
       last0:=last;
       spfa;
       maxdis:=dis[n]+k;

       //build a reverse map
       filldword(last,sizeof(last) >> 2,0);
       for i:=1 to m do addedge(i,b[i],a[i],c[i]);
       respfa;

       ans:=0;
       dfs(1,maxdis);

       writeln(ans);
 end;

begin
      asson;

      read(t);
      for ti:=1 to t do travelpark;

      assoff;
end.
var
    can:array[0..40000005]of boolean;
    a,b,maxcant,i,minab,maxab:longint;

procedure asson;
 begin
       assign(input,'math.in'); reset(input);
       assign(output,'math.out'); rewrite(output);
 end;

procedure assoff;
 begin
       close(input); close(output);
 end;

function min(n1,n2:longint):longint;
 begin
       if (n1<n2) then exit(n1) else exit(n2);
 end;

function max(n1,n2:longint):longint;
 begin
       if (n1>n2) then exit(n1) else exit(n2);
 end;

begin
      asson;

      read(a,b);

      fillchar(can,sizeof(can),0);
      can[0]:=true;
      minab:=min(a,b);
      maxab:=max(a,b);
      maxcant:=minab-1;
      for i:=minab to maxab-1 do
      begin
            can[i]:=can[i-minab];
            if not can[i] then maxcant:=i;
      end;
      for i:=maxab to (a+b)*2 do
      begin
            can[i]:=can[i-a] or can[i-b];
            if not can[i] then maxcant:=i;
      end;

      writeln(maxcant);

      assoff;
end.

#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

const int N = 110;

char s[N], com[N], x[N], y[N];
int X[N], Y[N], sta[N], L, numf, W[N], cnt;
bool vis[30];

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	for (int T = rd(); T; T --){
		L = rd(); scanf("%s", com + 1);
		int len = strlen(com + 1), w = 0, tot = 0, ans = 0, cnt = 0, numf = 0;
		memset(vis, 0, sizeof vis);
		memset(X, 0, sizeof X);
		memset(Y, 0, sizeof Y);
		memset(W, 0, sizeof W);
		memset(sta, 0, sizeof sta);
		bool Flag = 0;
		if (com[3] >= '0' && com[3] <= '9') Flag = 1;
		else {
			for (int j = 5; j < len; j ++) {
				char c = com[j];
				 w = w * 10 + c - 48;
			}
		}
		for (int i = 1; i <= L; i ++) {
			scanf("%s", s + 1);
			if (s[1] == 'F') {
				numf ++;
				scanf("%s", s + 1);
				sta[++tot] = s[1] - 'a';
				if (vis[sta[tot]]) {
					ans = 3;
				}
				vis[s[1] - 'a'] = 1;
				scanf("%s%s", x + 1, y + 1);
				if (x[1] == 'n') X[tot] = 101;
				else {
					len = strlen(x + 1);
					for (int j = 1; j <= len; j ++) {
						char c = x[j];
						X[tot] = X[tot] * 10 + c - 48;
					} 
				}
				if (y[1] == 'n') Y[tot] = 101;
				else {
					len = strlen(y + 1);
					for (int j = 1; j <= len; j ++) {
						char c = y[j];
						Y[tot] = Y[tot] * 10 + c - 48;
					} 
				}
			}
			else if (s[1] == 'E'){
				numf --;
				if (X[tot] < Y[tot]) {
					if (X[tot] != 101 && Y[tot] == 101) {
						if (!cnt) cnt++;
						W[cnt] ++;
						if (tot == 1) cnt++;
					}
				} else if (tot == 1) {
					W[cnt] = 0;
//					cnt --;
				}
				vis[sta[tot]] = 0;
				X[tot] = 0;
				Y[tot] = 0;
				tot --;
			}
		}
		int ww = 0;
		for (int i = 1; i <= cnt; i ++) ww = max(ww, W[i]);
		if (!ans) {
			if (numf) ans = 3;
			else {
				if (!ww && Flag) ans = 1;
				else if (!Flag && ww == w) ans = 1;
			} 
			if (ans == 1) puts("Yes");
			else if (ans == 3) puts("ERR");
			else puts("No");
		} else puts("ERR");
	}
	return 0;
}

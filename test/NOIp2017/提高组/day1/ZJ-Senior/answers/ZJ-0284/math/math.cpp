#include <cstdio>
#include <cstring>

using namespace std;

struct Num {
	int l, num[1000];
	
	Num() {
		l = 0;
		memset(num, 0, sizeof num);
	}
	void turn(char a[1000]) {
		l = strlen(a + 1);
		for (int i = 1; i <= l; i ++) num[i] = a[l - i + 1] - '0';
	}
} A, B, C;

Num mul(Num A, Num B) {
	for (int i = 1; i <= A.l; i ++) {
		for (int j = 1; j <= B.l; j ++) {
			C.num[i + j - 1] += A.num[i] * B.num[j];
		}
	}
	C.l = A.l + B.l;
	for (int i = 1; i <= C.l; i ++) {
		C.num[i + 1] += C.num[i] / 10;
		C.num[i] %= 10;
	}
	while (!C.num[C.l]) C.l --;
	return C;
}

void Dec(Num A) {
	for (int i = 1; i <= C.l; i ++) C.num[i] -= A.num[i];
	for (int i = 1; i <= C.l; i ++) if (C.num[i] < 0) {
		C.num[i + 1] --;
		C.num[i] += 10;
	}
}

void print(Num A) {
	while (!A.num[A.l]) A.l --;
	while (A.l) printf("%d", A.num[A.l--]);
	puts("");
}

char a[1000], b[1000];

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%s%s", a + 1, b + 1);
	A.turn(a), B.turn(b);
	C = mul(A, B);
	Dec(A), Dec(B);
	print(C);
	return 0;
}

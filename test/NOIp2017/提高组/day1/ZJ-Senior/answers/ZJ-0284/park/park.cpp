#include <cstdio>
#include <cstring>

using namespace std;

const int N = 1e5 + 10;
const int inf = 0x7f7f7f7f;

int head[N], dis[N], f[N], cnt, ans, q[N << 1], n, m, K, P;
bool vis[N];
struct edge {
	int nxt, to, w;
} e[N << 1];

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

void add_edge(int u, int v, int w) {
	e[++cnt].nxt = head[u], e[head[u] = cnt].to = v, e[cnt].w = w;
}

void bfs() {
	int l = 0, r = 1;
	memset(dis, 0x7f, sizeof dis);
	memset(q, 0, sizeof q);
	memset(f, 0, sizeof f);
	q[1] = 1, dis[1] = 0, f[1] = 1;
	vis[1] = 1;
	while (l <= r) {
		int u = q[++l];
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (dis[v] >= dis[u] + e[i].w) {
				dis[v] = dis[u] + e[i].w;
				if (dis[v] != dis[u] + e[i].w) f[v] = 0;
				f[v] = (1ll * f[v] + f[u]) % P;
				if (!vis[v]) q[++r] = v, vis[v] = 1;
			}
		}
		vis[u] = 0;
	}
}

void dfs(int u, int w) {
	if (w > dis[n] + K) return;
	if (u == n) {
		ans ++;
		return;
	}
	for (int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		dfs(v, w + e[i].w);
	}
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	for (int T = rd(); T; T --) {
		bool F = 0;
		n = rd(), m = rd(), K = rd(), P = rd();
		for (int i = 1; i <= m; i ++) {
			int u = rd(), v = rd(), w = rd();
			add_edge(u, v, w);
		}
		bfs();
		if (!K) printf("%d\n", f[n]);
		else {
			dfs(1, 0);
			printf("%d\n", ans);
		}
	}
	return 0;
}

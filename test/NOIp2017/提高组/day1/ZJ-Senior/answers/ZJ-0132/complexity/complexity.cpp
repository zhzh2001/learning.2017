#include <bits/stdc++.h>
using namespace std;
const int maxn=506;
typedef long long LL;
int mark[maxn],add[maxn],Cas,Mx,mx,pre,suo,zai,flag,top,L,n;
char s[maxn],stk[maxn];
int Get()
{
	scanf("%s",s);int tmp;
	if(s[0]=='n')return -1;
	sscanf(s,"%d",&tmp);return tmp;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&Cas);char c;
	int i,j;
	while(Cas--)
	{
		scanf("%d",&L);
		memset(mark,0,sizeof mark);
		pre=mx=top=0;suo=0;flag=0;
		while(c=getchar(),c!='(');
		c=getchar();
		if(c=='1')
		{
			Mx=0;c=getchar();
		}
		else
		{
			c=getchar();scanf("%d)",&Mx);
		}
//		printf("	%d\n",Mx);
		while(L--)
		{
			while(c=getchar(),c!='E'&&c!='F');
			if(c=='F')
			{
				scanf(" %c",&c);
//				printf("ERR? %d\n",flag);
//				putchar(c);printf(" %d\n",c);
				if(mark[c])flag=1;mark[c]=1;
				stk[++top]=c;
				int a=Get(),b=Get();
				if(a==-1&&b==-1)continue;
				if(a==-1&&b!=-1)
				{
					if(!suo)suo=1,zai=top-1;
				}
				if(a!=-1&&b==-1)
				{
					if(!suo)add[top-1]=1,++pre;
				}
				if(a!=-1&&b!=-1&&a>b)if(!suo)suo=1,zai=top-1;
//				printf("%d %d\n",a,b);
			}
			if(c=='E')
			{
				if(pre>mx)mx=pre;
				if(!top)flag=1;
				else mark[stk[top--]]=0;
				if(add[top])pre--,add[top]=0;
				if(suo&&zai==top)suo=0;
			}
//			printf("	%d\n",pre);
		}
		if(top)flag=1;
		if(flag)puts("ERR");
		else if(Mx!=mx)puts("No");
		else puts("Yes");
	}
	return 0;
}

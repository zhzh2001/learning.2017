#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int maxn=100005,maxm=200005;
int n,m,K,P;
int pt[maxm],wt[maxm],nt[maxm],hd[maxn],ec;
int U[maxm],V[maxm],W[maxm],mark[maxn];
void upd(int u,int v,int w)
{
	pt[++ec]=v;wt[ec]=w;nt[ec]=hd[u];hd[u]=ec;
}
int dfn,top,conc;
int arr[maxn],low[maxn],stk[maxn],con[maxn],dis[maxn],lis[maxn];
void dfs(int x)
{
	arr[x]=low[x]=++dfn;stk[++top]=x;
	int i;
	for(i=hd[x];i;i=nt[i])
	{
		if(!arr[pt[i]])
		{
			dfs(pt[i]);
			low[x]=min(low[x],low[pt[i]]);
		}
		else if(!con[pt[i]])
		{
			low[x]=min(low[x],arr[pt[i]]);
		}
	}
	if(arr[x]==low[x])
	{
		int y,siz=0;++conc;
		do
		{
			y=stk[top--];
			con[y]=conc;
			siz++;
		}while(y!=x);
		if(siz>1)mark[con[x]]=1;
	}
}
struct node
{
	int u,v;
	bool operator<(const node&t)const
	{
		return v>t.v;
	}
};
priority_queue<node>Q;
int dp[maxn][51][2];
void add(int &x,int y){ x+=y;if(x>=P)x-=P; }
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int Cas;scanf("%d",&Cas);
	while(Cas--)
	{
		memset(hd,0,sizeof hd);ec=0;
		scanf("%d%d%d%d",&n,&m,&K,&P);
		int i,j;
		for(i=1;i<=m;i++)
		{
			scanf("%d%d%d",U+i,V+i,W+i);
			if(!W[i])upd(U[i],V[i],0);
		}
		memset(arr,0,sizeof arr);
		memset(con,0,sizeof con);
		conc=top=dfn=0;
		for(i=1;i<=n;i++)if(!arr[i])dfs(i);
//		for(i=1;i<=n;i++)con[i]=i;
//		printf("	%d %d\n",n,conc);
		////
		memset(hd,0,sizeof hd);ec=0;
		for(i=1;i<=m;i++)
		{
//			scanf("%d%d%d",U+i,V+i,W+i);
//			printf("%d %d %d\n",con[U[i]],con[V[i]],W[i]);
			upd(con[U[i]],con[V[i]],W[i]);
		}
//		for(i=1;i<=n;i++)printf("name %d -> %d\n",i,con[i]);
		top=0;
		int S=con[1],T=con[n],L=1,R=1,cas=0;
		memset(dis,-1,sizeof dis);
		dis[S]=0;Q.push((node){S,0});node p;
		while(Q.size())
		{
			p=Q.top();Q.pop();
			if(dis[p.u]!=p.v)continue;
			lis[++top]=p.u;
//			assert(top==1||dis[lis[top]]>=dis[lis[top-1]]);
			for(i=hd[p.u];i;i=nt[i])
			{
				if(dis[pt[i]]==-1||dis[pt[i]]>dis[p.u]+wt[i])
				{
					dis[pt[i]]=dis[p.u]+wt[i];
					Q.push((node){pt[i],dis[pt[i]]});
				}
			}
		}
		
//		for(i=1;i<=conc;i++)printf("%d: %d\n",i,dis[i]);
//		for(i=1;i<=conc;i++)printf("%d: %d\n",i,mark[i]);
		
		n=top;
		for(L=1;L<=n;L=R+1)
		{
			for(R=L;R<n&&dis[lis[R+1]]==dis[lis[L]];++R);
			sort(lis+L,lis+R+1,greater<int>());
		}
		memset(dp,0,sizeof dp);
		dp[S][0][mark[S]]=1;L=R=1;
		while(L<=n)
		{
			if(L>R)cas=dis[lis[L]];
			while(R<n&&dis[lis[R+1]]==cas)R++;
			
//			assert((L==1||dis[lis[L-1]]+K<cas)&&dis[lis[L]]<=cas&&dis[lis[R]]+K>=cas&&(R==n||dis[lis[R+1]]>cas));
			
			for(j=L;j<=R;j++)
			{
				int u=lis[j];
				for(i=hd[u];i;i=nt[i])
				{
					if(cas+wt[i]-dis[pt[i]]<=K)
					{
						if(dp[u][cas-dis[u]][1])
						{
							dp[pt[i]][cas+wt[i]-dis[pt[i]]][1]=1;
						}
						else 
						{
							if(mark[pt[i]])
								dp[pt[i]][cas+wt[i]-dis[pt[i]]][1]=1;
							else
							add(dp[pt[i]][cas+wt[i]-dis[pt[i]]][0],dp[u][cas-dis[u]][0]);
						}
					}
				}
			}
			
			++cas;
			while(R<n&&dis[lis[R+1]]==cas)++R;
			while(L<=R&&dis[lis[L]]+K<cas)++L;
		}
		int ans=0;
		for(i=0;i<=K;i++)if(dp[T][i][1])ans=P;
		if(ans==P)
		{
			puts("-1");
			continue;
		}
		for(i=0;i<=K;i++)add(ans,dp[T][i][0]);
		printf("%d\n",ans);
	}
	return 0;
}

#include <bits/stdc++.h>
#define finput(x) freopen(x,"r",stdin)
#define foutput(x) freopen(x,"w",stdout)
#define rnd (rand()*rand()+rand())
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<endl
#define	MEM(x,v) memset(x,v,sizeof(x))
#define SPY(x,y) memcpy(x,y,sizeof(x))
#define mp make_pair
#define pb push_back
#define fi first
#define se second
using namespace std;
template <class T>inline void Rd(T &p){
	p=0; char c; short f=1;
	while(c=getchar(),c<48&&c!='-');
	do if(c!='-')p=p*10+(c&15); else f=-f;
	while(c=getchar(),c>47);
	p*=f;
}
template <class T>inline void pfn(T p,bool f=true){
	if(p<0)putchar('-'),p=-p;
	if(!p)putchar('0');
	static int num[100],tot;
	while(p)num[++tot]=p%10,p/=10;
	while(tot)putchar(num[tot--]^'0');
	putchar(f?'\n':' ');
}
template <class T>inline void Max(T &a,T b){if(a<b)a=b;}
template <class T>inline void Min(T &a,T b){if(b<a)a=b;}
template <class T>inline void Chk(T &a,T b){if(~b&&(!~a||b<a))a=b;}
typedef long long ll;
typedef pair<int,int> pii;
typedef double db;
const int S=55,M=(int)2e5+5,N=(int)1e5+5,inf=0x3f3f3f3f;
const double eps=1e-9;
const long long infty=1e18;

int kase,n,m,K,P,head[N],ht=0;
struct eg{int v,w,nxt;}E[M];
void add(int u,int v,int w){
	E[++ht]=(eg){v,w,head[u]}; head[u]=ht;
}
struct node{
	int u; ll dis;
	bool operator < (const node &p)const {
		return dis>p.dis; //for priority_queue
	}
};
bool used[N];
ll midis[N];
void Dijkstra_init(){
	priority_queue<node>q;
	for(int i=1;i<=n;++i)midis[i]=infty;
	MEM(used,false);
	
	midis[1]=0;
	q.push((node){1,midis[1]});
	while(!q.empty()){
		node now=q.top(); q.pop();
		int u=now.u;
		if(used[u])continue;
		used[u]=true;
		for(int j=head[u],v;j;j=E[j].nxt)
			if(v=E[j].v,!used[v]&&midis[v]>now.dis+E[j].w){
				midis[v]=now.dis+E[j].w;
				q.push((node){v,midis[v]});
			}
	}
}
bool vis[N][S];
int dp[N][S]; //在点u，目前路径长度为midis[u]+S的方案数。 
inline void add(int &a,int b){
	a+=b;
	if(a>=P)a-=P;
}
void Dijkstra(){
	priority_queue<node>q;
	MEM(dp,0),MEM(vis,0);
	
	dp[1][0]=1%P;
	q.push((node){1,0});
	while(!q.empty()){
		node now=q.top();q.pop();
		int u=now.u,ku=now.dis-midis[u];
		if(vis[u][ku])continue;
		vis[u][ku]=true;
		for(int j=head[u];j;j=E[j].nxt){
			int v=E[j].v,kv=now.dis+E[j].w-midis[v];
			if(kv>K||vis[v][kv])continue; //是不是如果访问到重新访问到已经访问过的情况就是0环？ 
			add(dp[v][kv],dp[u][ku]);
			q.push((node){v,kv+midis[v]});
		}
	}
}
void solve(){
	Dijkstra();
	int ans=0;
	for(int i=0;i<=K;++i)add(ans,dp[n][i]);
	pfn(ans);
}
int main(){
	finput("park.in");
	foutput("park.out");
	Rd(kase);
	while(kase--){
		MEM(head,0),ht=0;
		Rd(n),Rd(m),Rd(K),Rd(P);
		for(int i=1,u,v,w;i<=m;++i)
			Rd(u),Rd(v),Rd(w),add(u,v,w);
		Dijkstra_init();
		solve();
	}
	return 0;
}

/*
:loop
data.exe
test.exe
.exe
fc test.out .out
if %errorlevel% ==0 goto loop
pause
*/

#include <bits/stdc++.h>
#define finput(x) freopen(x,"r",stdin)
#define foutput(x) freopen(x,"w",stdout)
#define rnd (rand()*rand()+rand())
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<endl
#define	MEM(x,v) memset(x,v,sizeof(x))
#define SPY(x,y) memcpy(x,y,sizeof(x))
#define mp make_pair
#define pb push_back
#define fi first
#define se second
using namespace std;
template <class T>inline void Rd(T &p){
	p=0; char c; short f=1;
	while(c=getchar(),c<48&&c!='-');
	do if(c!='-')p=p*10+(c&15); else f=-f;
	while(c=getchar(),c>47);
	p*=f;
}
template <class T>inline void pfn(T p,bool f=true){
	if(p<0)putchar('-'),p=-p;
	if(!p)putchar('0');
	static int num[100],tot;
	while(p)num[++tot]=p%10,p/=10;
	while(tot)putchar(num[tot--]^'0');
	putchar(f?'\n':' ');
}
template <class T>inline void Max(T &a,T b){if(a<b)a=b;}
template <class T>inline void Min(T &a,T b){if(b<a)a=b;}
template <class T>inline void Chk(T &a,T b){if(~b&&(!~a||b<a))a=b;}
typedef long long ll;
typedef pair<int,int> pii;
typedef double db;
const int M=(int)1e3+5,N=(int)1e5+5,P=(int)1e9+7,inf=0x3f3f3f3f;
const double eps=1e-9;
const long long infty=1e18;

int kase,Line,pi;
char str[M],buf[M];

char stkch[M]; //存储变量，i是一个小写字母 
int stktag[M]; //存储循环体的状态 
int stop,tmp,ans; //tmp指的是实时答案，ans始终取tmp最大值 
multiset<char>Mp;
multiset<char>::iterator it;
void solve(){
	stop=tmp=ans=0;
	MEM(stkch,0),MEM(stktag,0);
	Mp.clear();
	bool f=true;
	int cant=0;
	for(int i=1;i<=Line;++i){
		scanf("%s",buf+1);
		if(buf[1]=='F'){
			scanf("%s",str+1);
			stkch[++stop]=str[1];
			if(Mp.find(str[1])!=Mp.end())f=false;
			Mp.insert(str[1]);
			
			scanf("%s%s",str+1,buf+1);
			int L=0,R=0;
			if(str[1]=='n')L=inf;
			else{
				for(int j=1,len=strlen(str+1);j<=len;++j)
					L=L*10+(str[j]&15);
			}
			if(buf[1]=='n')R=inf;
			else{
				for(int j=1,len=strlen(buf+1);j<=len;++j)
					R=R*10+(buf[j]&15);
			}
			if(L>R)stktag[stop]=-1,++cant;
			else if(R-L<1000000)stktag[stop]=0;
			else stktag[stop]=1,++tmp;
			
			if(!cant)Max(ans,tmp);
		}else if(buf[1]=='E'){
			if(!stop)f=false;
			else{
				Mp.erase(Mp.find(stkch[stop]));
				
				if(!~stktag[stop])--cant;
				else if(stktag[stop])--tmp;
				
				--stop;
			}
		}else cerr<<"Spylft"<<endl;
	}
	f&=!stop;
	if(!f)puts("ERR");
	else{
		if(pi==ans)puts("Yes");
		else puts("No");
	}
}
int main(){
	finput("complexity.in");
	foutput("complexity.out");
	scanf("%d",&kase);
	while(kase--){
		scanf("%d %s",&Line,str+1);
		int len=strlen(str+1);
		if(len<=4)pi=0;
		else{
			pi=0;
			for(int j=5;j<len;++j)
				pi=pi*10+(str[j]&15);
		}
		if(false)cerr<<"Spylft"<<endl;
		else{solve();continue;}
	}
	return 0;
}

/*
:loop
data.exe
test.exe
.exe
fc test.out .out
if %errorlevel% ==0 goto loop
pause
*/

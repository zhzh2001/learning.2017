#include<cstdio>
#include<cmath>
#include<cstring>
#include<iostream>
#include<string>
#include<algorithm>
#include<cstdlib>
using namespace std;
int t,n,pro,sum,ans,l,r,guard,sx,lower,cat,sum1;
int a[30],m[30],p[30];
char x,q[30];
bool bo;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	for(int i=1; i<=t; i++)
	{
		for (int o=0; o<=26; o++) a[o]=0;
		bo=false;
		sum=0; 
		ans=0; 
		pro=0;
		cat=0;
		sum1=0;
		scanf("%d",&n);
		x=getchar(); 
		while (x!='(') x=getchar();
		x=getchar(); 
		if (x=='n') 
		{
		    x=getchar(); 
			x=getchar();
			cat=1;
		}
		guard=x-'0'; 
		while (x!='\n')  x=getchar();
		for (int j=1; j<=n; j++)
		{
			x=getchar();
			sx=0;
			lower=0;
			while (x!='F'&&x!='E') x=getchar();
			if (x=='F')
			{
				l=r=0;
				while(x<'a'||x>'z') x=getchar();
				pro++;
				q[pro]=x;
				a[x-'a']++;
				if (a[x-'a']>1) bo=true;
				x=getchar();
				while (x==' ') x=getchar();
				if (x!='n')
				{
					while(x>='0'&&x<='9')
					{
						l=l*10+x-'0';
						x=getchar();
					} 
				}else sx=1;
				x=getchar();
				while (x==' ') x=getchar();
				if (x!='n')
				{
					while(x>='0'&&x<='9')
					{
						r=r*10+x-'0';
						x=getchar();
					} 
				}else lower=1;
				if (sx&&!lower||(l>r&&!sx&&!lower)) 
				{
				    p[pro]=1;
					sum1++;
				}
				if (sum1==0)
				{
					if (!sx&&lower)
					{
						sum++;
						m[pro]=1;
					}
				}
				if (sum>ans) ans=sum;
			}
			else
			{
			    if (pro==0)
				{
					bo=true;
					break;
				}
				a[q[pro]-'a']--;
				if (m[pro]==1) sum1--;
				p[pro]=0;
				if (m[pro]==1) sum--;
				m[pro]=0;
				pro--;
				if (pro<0) bo=true;
			}
		}
		if (cat)
		{
	    	if (bo||pro>0) printf("ERR\n");
	    	else
	        {
    	    	if (ans!=guard) printf("No\n");
    	    	else printf("Yes\n");
    		} 
  	    }else
	        if (bo||pro>0) printf("ERR\n");
	    	else
	        {
    	    	if (ans!=0) printf("No\n");
	        	else printf("Yes\n");
    		}    
	}
	return 0;
} 

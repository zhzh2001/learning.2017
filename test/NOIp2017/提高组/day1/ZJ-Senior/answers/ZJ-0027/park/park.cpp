#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct node{int z,nxt,to;}e[200010];
int n,A,ans,head[200010],vis[200010],dis[200010],q[200010],T,m,k,P,x,y,cnt,z,v[200010];
void add(int x,int y,int z)
{
	e[++cnt].to=y;
	e[cnt].nxt=head[x];
	e[cnt].z=z;
	head[x]=cnt;
}
void dfs(int x,int s)
{
	if (x==n&&s<=A)
	{
		ans++;
		return;
	}
	if (s>A) return;
	for (int i=head[x]; i; i=e[i].nxt)
	{
		int to=e[i].to;
		if (!v[to])
			v[to]=1,dfs(to,s+e[i].z),v[to]=0;
	}
	v[x]=0;
}
int SPFA()
{
	memset(dis,8,sizeof(dis));
	q[1]=1;int h=0,t=1;
	vis[1]=1,dis[1]=0;
	while(h<t)
	{
		int tmp=q[++h];
		for (int i=head[tmp]; i; i=e[i].nxt)
		{
			int to=e[i].to;
			if (dis[to]>e[i].z+dis[tmp])
			{
				dis[to]=e[i].z+dis[tmp];
				if (!vis[to]) q[++t]=to;
			}
		}
		vis[tmp]=0;
	}
	return dis[n];
}
void clear()
{
	A=0;
	cnt=0;
	memset(e,0,sizeof(e));
	memset(head,0,sizeof(head));
	memset(v,0,sizeof(v));
	ans=0;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		clear();
		scanf("%d%d%d%d",&n,&m,&k,&P);
		for (int i=1; i<=m; i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		if (n>=100000&&k==0){puts("1");continue;}
		A=SPFA()+k;
		v[1]=1;
		dfs(1,0);
		printf("%d\n",ans%P);
	}
}

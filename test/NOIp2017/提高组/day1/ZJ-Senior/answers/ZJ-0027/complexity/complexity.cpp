#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
using namespace std;
char s[10010],ss[10010],bl[1000];
int T,n,A,a[1000];
const int MAX=100000;
void clear()
{
	memset(a,0,sizeof(a));
	A=0;
}
void doo()
{
	int flag=1,lp=0,Ans=0,AA=0,last=1;
	int x,y;
	for (int i=1; i<=n; i++)
	{
		cin>>s;
		if (s[0]=='E') 
		{
			a[bl[lp]]=0;
			lp--;
			if (lp<0) flag=0;
			if (lp==0) Ans=max(Ans,AA),AA=0;
		}
		if (s[0]=='F')
		{
			lp++;
			cin>>ss;
			if (a[ss[0]]==1) flag=0;
			a[ss[0]]=1,bl[lp]=ss[0];
			cin>>ss;
			if (ss[0]=='n') x=MAX;
			else
			{
				int j=0,t=0;
				while(ss[j]>='0'&&ss[j]<='9') t=t*10+ss[j]-48,j++;
				x=t;
			}
			cin>>ss;
			if (ss[0]=='n') y=MAX;
			else
			{
				int j=0,t=0;
				while(ss[j]>='0'&&ss[j]<='9') t=t*10+ss[j]-48,j++;
				y=t;
			}
			if (last==1&&y>=x)
			{
				last=1;
				if (y-x>1000) AA++;
			}
			else last=0;
		}
	}
	if (lp>0) flag=0;
	if (!flag) puts("ERR");
	else if (A==Ans) puts("Yes");
	else puts("No");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>T;
	while (T--)
	{
		clear();
		cin>>n>>s;
		if (s[2]=='1') A=0;
		else
		{
			int i=4;
			while (s[i]>='0'&&s[i]<='9') A=A*10+s[i]-48,i++;
		}
		doo();
	}
}

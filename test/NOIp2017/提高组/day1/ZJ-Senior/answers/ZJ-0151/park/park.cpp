#include<cstdio>
#include<cstring>
using namespace std;
inline int _read(){
	int num=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
	return num;
}
const int maxn=100005,maxm=200005,maxt=5000005;
int tot,lnk[maxn],son[maxm],nxt[maxm],w[maxm],ans,_W;
int T,n,m,K,P,que[maxn],hed,til,dis[maxn],f[maxn][55],g[maxn][55],rd[maxn][55];
struct jz{
	int x,w;
}q[maxt];
bool vis[maxn],B[maxn][55];
void add(int x,int y,int z){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;w[tot]=z;}
int mo(int x){if (x>P) return x-P;return x;}
void spfa(){
	memset(dis,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	hed=0;til=1;que[1]=1;vis[1]=1;dis[1]=0;
	while(hed!=til){
		hed=(hed+1)%maxn;int x=que[hed];vis[x]=0;
		for (int j=lnk[x];j;j=nxt[j])
		if (dis[x]+w[j]<dis[son[j]]){
			dis[son[j]]=dis[x]+w[j];
			if (!vis[son[j]]){
				til=(til+1)%maxn;que[til]=son[j];
				vis[son[j]]=1;
			}
		}
	}
}
void shua(){
	memset(f,0,sizeof(f));
	memset(B,0,sizeof(B));
	memset(g,0,sizeof(g));
	memset(rd,0,sizeof(rd));
	hed=0;til=1;q[1].x=1;q[1].w=0;f[1][0]=1;B[1][0]=1;
	while(hed!=til){
		hed=(hed+1)%maxt;int x=q[hed].x,ww=q[hed].w;B[x][ww]=0;
		rd[x][ww]++;if (rd[x][ww]>n){_W=1;return;}
		for (int j=lnk[x];j;j=nxt[j])
		if (dis[x]+ww+w[j]<=dis[son[j]]+K){
			int y=dis[x]+ww+w[j]-dis[son[j]];
			f[son[j]][y]=mo(f[son[j]][y]+f[x][ww]);
			g[son[j]][y]=mo(g[son[j]][y]+f[x][ww]);
			if (!B[son[j]][y]){
				til=(til+1)%maxt;q[til].x=son[j];q[til].w=y;
				B[son[j]][y]=1;
			}
		}
		f[x][ww]=0;
	}
}
void work(){
	memset(lnk,0,sizeof(lnk));tot=0;_W=0;
	n=_read(),m=_read(),K=_read(),P=_read();
	for (int i=1;i<=m;i++){
		int x=_read(),y=_read(),z=_read();
		add(x,y,z);
	}
	spfa();shua();
	if (_W) printf("-1\n");else{
		ans=0;for (int i=0;i<=K;i++) ans=mo(ans+g[n][i]);
		printf("%d\n",ans);
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=_read();
	while(T--) work();
	return 0;
} 

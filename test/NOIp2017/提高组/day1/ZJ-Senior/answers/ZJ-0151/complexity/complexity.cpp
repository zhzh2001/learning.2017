#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1005,INF=1e9;
int c[maxn],hed[maxn],til[maxn],top,T,n,_W,m;
char h[maxn];
int _read(){
	int num=0;char ch=getchar();
	while(ch!='(') ch=getchar();
	ch=getchar();
	if (ch=='1') return 0;else{
		while(ch<'0'||ch>'9') ch=getchar();
		while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
		return num;
	}
}
int _read1(){
	int num=0;char ch=getchar();
	while(ch<'0'||ch>'9'){if (ch=='n') return INF;ch=getchar();}
	while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
	return num;
}
bool find(int x){
	for (int i=1;i<x;i++) if (h[i]==h[x]) return 1;
	return 0;
}
int check(int x){
	if (hed[x]==INF&&til[x]==INF) return c[x];
	if (hed[x]==INF&&til[x]<=100) return 0;
	if (hed[x]<=100&&til[x]==INF) return c[x]+1;
	if (hed[x]>til[x]) return 0;
	return c[x]; 
}
void work(){
	scanf("%d",&n);m=_read();
	_W=0;c[0]=0;top=0;
	for (int i=1;i<=n;i++){
		char ch=getchar();
		while(ch!='F'&&ch!='E') ch=getchar();
		if (ch=='F'){
			c[++top]=0;
			getchar();h[top]=getchar();
			if (find(top)) _W=1;
			hed[top]=_read1();til[top]=_read1();
		}else{if (top) c[top-1]=max(c[top-1],check(top)),top--;else _W=1;}
	}
	if (top||_W) printf("ERR\n");else{
		if (c[0]==m) printf("Yes\n");else printf("No\n");
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--) work();
	return 0;
}

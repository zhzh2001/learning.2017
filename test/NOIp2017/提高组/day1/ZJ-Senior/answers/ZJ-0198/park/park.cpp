#include <bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int read(){
	int x=0;
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=200002;
const int maxm=200002;
struct graph{
	int n,m;
	struct edge{
		int from,to,cost,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to,int cost){
		e[++m]=(edge){from,to,cost,first[from]};
		first[from]=m;
	}
	void init(int n){
		this->n=n;
		m=0;
		memset(first,0,sizeof(first));
	}
	struct node{
		int u,d;
		bool operator <(const node& rhs)const{
			return d>rhs.d;
		}	
	};
	priority_queue<node> q;
	int dist[maxn];
	bool done[maxn];
	void dijkstra(int s){
		memset(dist,0x3f,sizeof(dist));
		memset(done,0,sizeof(done));
		dist[s]=0;
		q.push((node){s,0});
		while(!q.empty()){
			int u=q.top().u;
			q.pop();
			if (done[u])
				continue;
			done[u]=true;
			//printf("on %d\n",u);
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				//printf("to %d\n",v);
				if (dist[u]+e[i].cost<dist[v]){
					dist[v]=dist[u]+e[i].cost;
					q.push((node){v,dist[v]});
				}
			}
		}
	}
}g;
struct dfs_graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	void init(int n){
		this->n=n;
		m=0;
		memset(first,0,sizeof(first));
		memset(vis,0,sizeof(vis));
	}
	bool vis[maxn];
	void dfs(int u){
		//printf("on %d\n",u);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!vis[v]){
				vis[v]=1;
				dfs(v);
			}
		}
	}
	void work(int s){
		vis[s]=1;
		dfs(s);
	}
}g2;
int mod;
struct top_graph{
	int n,m;
	struct edge{
		int from,to,cost;
		bool operator <(const edge& rhs)const{
			return from<rhs.from;
		}
	}e[maxm];
	void addedge(int from,int to,int cost){
		e[++m]=(edge){from,to,cost};
	}
	void init(int n){
		this->n=n;
		m=0;
		memset(del,0,sizeof(del));
		memset(id,0,sizeof(id));
		memset(in,0,sizeof(in));
	}
	bool del[maxn];
	int q[maxm],id[maxn],cnt;
	int in[maxn];
	int st[maxn],en[maxn];
	bool toposort(){
		int l=1,r=0;
		cnt=0;
		for(int i=1;i<=m;i++)
			if (!e[i].cost)
				in[e[i].to]++;
		for(int i=1;i<=n;i++){
			if (!del[i])
				cnt++;
			if (!del[i] && !in[i])
				q[++r]=i;
		}
		while(l<=r){
			int u=q[l++];
			for(int i=st[u];i<=en[u];i++){
				int v=e[i].to;
				if (e[i].cost || del[v])
					continue;
				in[v]--;
				if (!in[v])
					q[++r]=v;
			}
		}
		for(int i=1;i<=r;i++)
			id[q[i]]=i;
		return r==cnt;
	}
	int dp[52][maxn];
	int work(int k){
		sort(e+1,e+m+1);
		for(int i=1;i<=m;i++){
			if (e[i].from!=e[i-1].from){
				st[e[i].from]=i;
				en[e[i-1].from]=i-1;
			}
		}
		en[e[m].from]=m;
		if (!toposort()) //toposort is for zero length edges
			return -1;
		//puts("after topsort");
		memset(dp,0,sizeof(dp));
		dp[0][id[1]]=1;
		for(int i=0;i<=k;i++){
			for(int j=1;j<=cnt;j++){
				if (!dp[i][j])
					continue;
				int l=st[q[j]],r=en[q[j]];
				for(int o=l;o<=r;o++)
					if (i+e[o].cost<=k){
						int& to=dp[i+e[o].cost][id[e[o].to]];
						to+=dp[i][j];
						if (to>=mod)
							to-=mod;
					}
			}
		}
		int ans=0;
		for(int i=0;i<=k;i++){
			ans+=dp[i][id[n]];
			if (ans>=mod)
				ans-=mod;
		}
		return ans;
	}
}g3;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout); 
	int T=read();
	while(T--){
		int n=read(),m=read(),k=read();
		mod=read();
		g.init(n);
		g2.init(n);
		for(int i=1;i<=m;i++){
			int u=read(),v=read(),w=read();
			g.addedge(u,v,w);
			g2.addedge(v,u);
		}
		//puts("WTF");
		g.dijkstra(1);
		//puts("WTF");
		g2.work(n);
		//puts("WTF");
		g3.init(n);
		for(int i=1;i<=n;i++){
			g3.del[i]=(g.dist[i]==INF || !g2.vis[i]);
			//printf("del %d=%d\n",i,g3.del[i]);
		}	
		for(int i=1;i<=m;i++){
			graph::edge & e=g.e[i];
			if (!g3.del[e.from] && !g3.del[e.to]){
				if (e.cost-(g.dist[e.to]-g.dist[e.from])<=k)
					g3.addedge(e.from,e.to,e.cost-(g.dist[e.to]-g.dist[e.from]));
			}
		}
		printf("%d\n",g3.work(k));
	}
	//printf("%d ",clock());
}

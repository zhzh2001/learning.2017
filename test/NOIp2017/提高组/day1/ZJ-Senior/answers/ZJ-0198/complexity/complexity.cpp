#include <bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int read(){
	int x=0,f=0;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			f=1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return f?-x:x;
}
int get(char *x){
	if (!isdigit(x[0]))
		return -1;
	int ans=x[0]-'0';
	if (isdigit(x[1]))
		ans=ans*10+x[1]-'0';
	return ans;
}
struct oper{
	bool typ;
	char c;
	int x,y;
}a[120];
stack<oper> s;
char ch[100];
bool used[256];
int solve(int l,int r){
	if (l>r)
		return 0; 
	int now=0;
	int to=0;
	for(int i=l;i<=r;i++){
		now+=a[i].typ?1:-1;
		if (!now){
			to=i;
			break;
		}
	}
	if (to==r){
		if (a[l].x==INF && a[l].y==INF)
			return solve(l+1,r-1);
		if (a[l].x==INF)
			return 0;
		if (a[l].y==INF)
			return solve(l+1,r-1)+1;
		if (a[l].x>a[l].y)
			return 0;
		return solve(l+1,r-1);
	}
	return max(solve(l,to),solve(to+1,r));
}
void work(){
	memset(used,0,sizeof(used));
	memset(ch,0,sizeof(ch));
	while(!s.empty())
		s.pop();
	int l,ans=0;
	scanf("%d%s",&l,ch);
	int len=strlen(ch);
	if (len==4)
		ans=0;
	else ans=get(ch+4);
	for(int i=1;i<=l;i++){
		scanf("%s",ch);
		a[i].typ=ch[0]=='F';
		if (a[i].typ){
			scanf("%s",ch);
			a[i].c=ch[0];
			scanf("%s",ch);
			if (ch[0]=='n')
				a[i].x=INF;
			else a[i].x=get(ch);
			scanf("%s",ch);
			if (ch[0]=='n')
				a[i].y=INF;
			else a[i].y=get(ch);
		}
	}
	//puts("read input");
	for(int i=1;i<=l;i++){
		if (a[i].typ){
			s.push(a[i]);
			if (used[a[i].c-'a']){
				puts("ERR");
				return;
			}
			used[a[i].c-'a']=1;
		}else{
			if (s.empty()){
				puts("ERR");
				return;
			}
			used[s.top().c-'a']=0;
			s.pop();
		}	
	}
	if (!s.empty()){
		puts("ERR");
		return; 
	}
	//puts("before solve");
	int x=solve(1,l);
	puts(x==ans?"Yes":"No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout); 
	int T;
	scanf("%d",&T);
	while(T--){
		//puts("WTF");
		work();
	}
}

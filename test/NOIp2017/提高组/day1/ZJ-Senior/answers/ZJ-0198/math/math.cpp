#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	int x=0,f=0;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			f=1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return f?-x:x;
} 
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a=read(),b=read();
	printf("%lld\n",a*b-a-b); 
}

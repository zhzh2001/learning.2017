#include <iostream>
#include <cstdio>
#include <string> 
#include <cstring>
#include <cstdlib>
const int maxn = 2e9;
using namespace std;
int read()
{
	int x(0);
	char ch;
	ch = getchar();
	while(ch < '0' || ch > '9')
	{
		if(ch == 'n')
		{
			return -1;
		}
		ch = getchar();
	}
	while(ch >= '0' && ch <= '9')
	{
		x = x * 10 + ch - '0';
		ch = getchar();
	}
	return x;
}
struct dat
{
	char F;
	char ch;
	int l, r;	
};
bool ch[200];
dat d[10000];
void init()
{
	for(int i('a');i <= 'z';++i)
	{
		ch[i] = false;
	}
}
int Max(int a,int b)
{
	return a > b ? a : b; 
}
int dfs(int l,int r)
{
	if(r < l)return 0;
	int left(l);
	int fstack(1);
	int ans(0);
	for(int i(l + 1);i <= r;++i)
	{
		if(d[i].F == 'F')++fstack;
		if(d[i].F == 'E')
		{
			--fstack;
			if(fstack == 0)
			{
				if(d[left].l > d[left].r);
				else if(d[left].r == maxn && d[left].l != maxn)ans = max(ans,dfs(left + 1,i - 1) + 1);
				else ans = max(ans,dfs(left + 1,i - 1));
				left = i + 1;
			} 
		}
	}
	return ans;
}
int main()
{
	freopen("complexity.in","r+",stdin);
	freopen("complexity.out","w+",stdout);
	int examples;
	string tttttmp;
	scanf("%d",&examples);
	for(int times(1);times <= examples;++times)
	{
		int lines;
		scanf("%d O(",&lines);
		int level = read();
		if(level == 1)
		{
			level = 0;
		}
		if(level == -1)
		{
			scanf("%*c%d)",&level);
		}//指数级
		scanf("%*c");
		int fstack(0); 
		int i;
		bool allright(true);
		init();
		for(i = 1;i <= lines;++i)
		{
			scanf("%c",&d[i].F);
			if(d[i].F == 'F')
			{
				++fstack;
				scanf(" %c",&d[i].ch);
				if(ch[d[i].ch])
				{
					allright = false;
					break;
				}
				else ch[d[i].ch] = true;
				d[i].l = read();
				d[i].r = read();
				if(d[i].l == -1)d[i].l = maxn;
				if(d[i].r == -1)
				{
					d[i].r = maxn;
					if(i != lines || times != examples)scanf("%*c");
				}
			}
			else if(d[i].F == 'E')
			{
				--fstack;
				if(fstack == 0)
				{
					init();
				}
				if(fstack < 0)
				{
					allright = false;
					break;
				}
				if(i != lines || times != examples)scanf("%*c");
			}
			else printf("wrong with read\n");
		} 
		if(!allright || fstack > 0)
		{
			for(;i <= lines;++i)
			{
				getline(cin,tttttmp);
			} 
			printf("ERR\n");
			continue;
		}
		//处理完err
		int maxtime = dfs(1,lines);
		if(maxtime == level)
		{
			printf("Yes\n");
		}
		else 
		{
			printf("No\n");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


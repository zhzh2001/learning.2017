#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;
typedef long long ll;
int main()
{
	freopen("math.in","r+",stdin);
	freopen("math.out","w+",stdout);
	ll a,b,tmp;
	cin >> a >> b;
	ll t = (a-1)*(b-1)-1;
	cout << t << endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}

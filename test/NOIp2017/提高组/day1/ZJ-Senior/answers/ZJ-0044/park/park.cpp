#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
#include <cstdlib>
using namespace std;
int n,m,k,mod,minlenth;
struct edge
{
	int v,val;
	edge *next;
};
edge e[2123],*first[1123];
bool inque[1123];
int dis[1123],ans(0);
int num;
void init()
{
	memset(e,0,sizeof(e));
	memset(first,0,sizeof(first));
	memset(dis,0x7f,sizeof(dis));
	num = 0;
	ans = 0;
}
inline void add(int u,int v,int val)
{
	++num;
	e[num].v = v;
	e[num].val = val;
	e[num].next = first[u];
	first[u] = &e[num];
}
void dfs(int now,int nowlenth)
{
	if(now == n && nowlenth <= minlenth)
	{
		++ans;
		if(ans >= mod)ans -= mod;
		return;
	}
	if(nowlenth >= minlenth)return;
	for(edge *itr(first[now]);itr != 0;itr = itr->next)
	{
		dfs(itr->v,nowlenth + itr->val);
	}
}
int main()
{
	freopen("park.in","r+",stdin);
	freopen("park.out","w+",stdout);
	int examples;
	scanf("%d",&examples);
	for(int times(1);times <= examples;++times)
	{
		init();
		scanf("%d%d%d%d",&n,&m,&k,&mod);
		for(int i(1);i <= m;++i)
		{
			int u,v,val;
			scanf("%d%d%d",&u,&v,&val);
			add(u,v,val);
		}
		queue <int> q;
		while(!q.empty())q.pop();
		q.push(1);
		inque[1] = true;
		dis[1] = 0;
		while(!q.empty())
		{
			int t = q.size();
			for(int i(1);i <= t;++i)
			{
				int now = q.front();
				q.pop();
				inque[now] = false;
				for(edge *itr(first[now]);itr != 0;itr = itr->next)
				{
					if(dis[now] + itr->val < dis[itr->v])
					{
						dis[itr->v] = dis[now] + itr->val;
						if(!inque[itr->v])
						{
							q.push(itr->v);
							inque[itr->v] = true;
						}
					}
				}
			}
		}
		minlenth = dis[n] + k;
		dfs(1,0);
		printf("%d\n",ans);
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


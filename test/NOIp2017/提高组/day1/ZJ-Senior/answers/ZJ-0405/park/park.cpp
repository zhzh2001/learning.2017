#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
#define travel(x, i) for (int i = fir[x]; i; i = e[i].nxt)
#define Travel(x, i) for (int i = Fir[x]; i; i = E[i].nxt)
using namespace std;

typedef long long LL;
typedef pair <int, int> pii;
const int N = 1e5 + 5;
const int M = 2e5 + 5;

struct edge {
	int nxt, to, len;
} e[M], E[M];
int fir[N], Fir[N], cnt = 0, Cnt = 0;
int n, m, k, mod, dis[N], deg[N], Dis[N];
bool vis[N], flag, in[N], spe[N], ok[N];
int ans[N][53];

inline void add(int x, int y, int l) {
	e[++ cnt] = (edge){fir[x], y, l};
	fir[x] = cnt;
}

inline void Add(int x, int y, int l) {
	E[++ Cnt] = (edge){Fir[x], y, l};
	Fir[x] = Cnt;
}

inline void dfs(int x) {
	if (flag) return;
	vis[x] = 1;
	travel(x, i)
		if (e[i].len == 0) {
			if (vis[e[i].to]) {
				flag = 1;
				break;
			}
			else dfs(e[i].to);
		}
}

inline void dijkstra() {
	memset(in, 0, sizeof in);
	priority_queue <pii, vector <pii>, greater <pii> > Q;
	Q.push(make_pair(0, 1));
	pii cur;
	int x;
	while (!Q.empty()) {
		cur = Q.top(); Q.pop();
		x = cur.second;
		if (in[x]) continue;
		in[x] = 1;
		dis[x] = cur.first;
		travel(x, i)
			if (!in[e[i].to]) Q.push(make_pair(dis[x] + e[i].len, e[i].to));
	}
}

inline void Dijkstra() {
	memset(in, 0, sizeof in);
	priority_queue <pii, vector <pii>, greater <pii> > Q;
	Q.push(make_pair(0, n));
	pii cur;
	int x;
	while (!Q.empty()) {
		cur = Q.top(); Q.pop();
		x = cur.second;
		if (in[x]) continue;
		in[x] = 1;
		Dis[x] = cur.first;
		Travel(x, i)
			if (!in[E[i].to]) Q.push(make_pair(Dis[x] + E[i].len, E[i].to));
	}
}

int sta[N], top;

inline void pre() {
	memset(deg, 0, sizeof deg);
	for (int i = 0; i <= k; i ++) memset(ans[i], 0, sizeof ans[i]);
	for (int i = 1; i <= n; i ++) {
		if (dis[i] + Dis[i] == dis[n]) ok[i] = 1;
		else ok[i] = 0;
	}
	for (int x = 1; x <= n; x ++) {
		if (!ok[x]) continue;
		travel(x, i) {
			if (dis[e[i].to] == dis[x] + e[i].len && ok[e[i].to])
				deg[e[i].to] ++;
		}
	}
	queue <int> Q;
	Q.push(1);
	ans[1][0] = 1;
	while (!Q.empty()) {
		int x = Q.front(); Q.pop();
		travel(x, i)
			if (ok[e[i].to] && dis[e[i].to] == dis[x] + e[i].len) {
				ans[e[i].to][0] += ans[x][0];
				deg[e[i].to] --;
				if (!deg[e[i].to]) Q.push(e[i].to);
			}
	}
}

inline void solve() {
	memset(fir, 0, sizeof fir);
	memset(vis, 0, sizeof vis);
	memset(Fir, 0, sizeof Fir);
	Cnt = 0; cnt = 0; flag = 0;
	scanf("%d%d%d%d", &n, &m, &k, &mod);
	for (int i = 1, x, y, l; i <= m; i ++) {
		scanf("%d%d%d", &x, &y, &l);
		add(x, y, l);
		Add(y, x, l);
	}
	for (int i = 1; i <= n && !flag; i ++)
		if (!vis[i]) dfs(i);
	if (flag) return (void) puts("-1");
	dijkstra();
	Dijkstra();
	pre();
	memset(spe, 0, sizeof spe);
	memset(deg, 0, sizeof deg);
	memset(Fir, 0, sizeof Fir); Cnt = 0;
	for (int x = 1; x <= n; x ++)
		travel(x, i) {
			if (e[i].len == 0) {
				spe[x] = 1;
				spe[e[i].to] = 1;
				deg[x] ++;
				Add(e[i].to, x, 0);
			}
		}
	top = 0;
	queue <int> Q;
	for (int i = 1; i <= n; i ++) {
		if (!deg[i] && spe[i]) Q.push(i);
		if (!spe[i]) sta[++ top] = i;
	}
	int x;
	while (!Q.empty()) {
		x = Q.front();
		Q.pop();
		sta[++ top] = x;
		Travel(x, i) {
			deg[E[i].to] --;
			if (!deg[E[i].to]) Q.push(E[i].to);
		}
	}
	for (int a = 1, x, l; a <= k; a ++) {
		for (int j = 1; j <= top; j ++) {
			x = sta[j];
			l = Dis[x] + a;
			travel(x, i) {
				if (l - e[i].len - Dis[e[i].to] >= 0) {
					ans[x][a] += ans[e[i].to][l - e[i].len - Dis[e[i].to]];
					if (ans[x][a] >= mod) ans[x][a] -= mod;
				}
			}
		}
	}
	int Ans = 0;
	for (int i = 0; i <= k; i ++) {
		Ans += ans[n][i];
		if (Ans >= mod) Ans -= mod;
	}
	printf("%d\n", Ans);
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T --) solve();
}


#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;

typedef long long LL;

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	LL a, b;
	cin >> a >> b;
	cout << (a - 1) * (b - 1) - 1 << endl;
	return 0;
}

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <stack>
using namespace std;

char O[50], s[50];
bool use[200];

inline void solve() {
	stack <int> sta, com;
	memset(use, 0, sizeof use);
	int l;
	scanf("%d", &l);
	scanf("%s", O);
	int bra = 0, cur = 0, Max = 0, bl;
	bool invalid = 0;
	int i = 1;
	while (i <= l) {
		i ++;
		scanf("%s", s);
		if (s[0] == 'E') {
			Max = max(cur, Max);
			bra --;
			if (bra < 0) {
				invalid = 1;
				for (; i <= l; i ++) {
					scanf("%s", s);
					if (s[0] == 'F') {
						scanf("%s", s);
						scanf("%s", s);
						scanf("%s", s);
					}
				}
				break;
			}
			use[sta.top()] = 0;
			sta.pop();
			cur -= com.top();
			com.pop();
			continue;
		}
		scanf("%s", s);
		bl = s[0];
		if (use[bl]) {
			invalid = 1;
			scanf("%s", s);
			scanf("%s", s);
			for (; i <= l; i ++) {
				scanf("%s", s);
				if (s[0] == 'F') {
					scanf("%s", s);
					scanf("%s", s);
					scanf("%s", s);
				}
			}
			break;
		}
		scanf("%s", s);
		bool skip = 0, add = 0;
		int x = 0, y = 0;
		if (s[0] == 'n') skip = 1;
		else {
			for (int j = 0; j < (int)strlen(s); j ++) x = x * 10 + s[j] - '0';
		}
		
		scanf("%s", s);
		if (s[0] == 'n') {
			if (skip) skip = 0;
			else add = 1;
		}
		else {
			for (int j = 0; j < (int)strlen(s); j ++) y = y * 10 + s[j] - '0';
			if (x > y) skip = 1;
		}
		if (!skip) {
			bra ++;
			use[bl] = 1;
			sta.push(bl);
			cur += add;
			com.push(add);
		}
		else {
			int f = 1;
			sta.push(bl);
			use[bl] = 1;
			while (f && i <= l) {
				i ++;
				scanf("%s", s);
				if (s[0] == 'E') {
					f --;
					use[sta.top()] = 0;
					sta.pop();
				}
				else {
					f ++;
					scanf("%s", s);
					if (use[(int)s[0]]) {
						invalid = 1;
						scanf("%s", s);
						scanf("%s", s);
						for (; i <= l; i ++) {
							scanf("%s", s);
							if (s[0] == 'F') {
								scanf("%s", s);
								scanf("%s", s);
								scanf("%s", s);
							}
						}
						break;
					}
					use[(int)s[0]] = 1;
					sta.push(s[0]);
					scanf("%s", s);
					scanf("%s", s);
				}
			}
			if (f) invalid = 1;
		}
	}
	if (bra) invalid = 1;
	if (invalid) puts("ERR");
	else {
		if (!Max) {
			if (O[2] != '1') puts("No");
			else puts("Yes");
		}
		else {
			int p = 4, o = 0;
			while (isdigit(O[p])) {
				o = o * 10 + O[p] - '0';
				p ++;
			}
			if (o == Max) puts("Yes");
			else puts("No");
		}
	}
}

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T --) solve();
	return 0;
}

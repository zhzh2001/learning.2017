#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define N 105
int nd2(char*s){
	return s[1]<'0'||s[1]>'9'?s[0]-'0':(s[0]-'0')*10+s[1]-'0';
}
int T,m,w;char s[99];
char op[N][99],id[N][99],lo[N][99],up[N][99];
int usd[199],mxd;
int dt(int p){
	if(lo[p][0]=='n'&&up[p][0]=='n') return 0;
	if(lo[p][0]=='n') return -1e5;
	if(up[p][0]=='n') return 1;
	if(nd2(lo[p])<=nd2(up[p])) return 0;
	return -1e5;
}
int edln(int x,int d){
	if(op[x][0]=='E') return -999;
	mxd=std::max(mxd,d);
	int p=x+1;
	while(p>=0&&p<=m+1&&op[p][0]=='F'){
		char ch=id[p][0];
		if(usd[ch]) return -999;
		usd[ch]=1;
		p=edln(p,d+dt(p))+1;
		usd[ch]=0;
	}
	return p<0||p>m+1?-999:p;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%s",&m,s);
		w=s[2]=='1'?0:nd2(s+4);
		rep(i,1,m){
			scanf("%s",op[i]);
			if(op[i][0]=='F') scanf("%s%s%s",id[i],lo[i],up[i]);
		}
		op[0][0]=='F';op[m+1][0]='E';
		memset(usd,0,sizeof(usd));
		usd['n']=1;mxd=0;
		if(edln(0,0)==m+1) puts(mxd==w?"Yes":"No");
		else puts("ERR");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

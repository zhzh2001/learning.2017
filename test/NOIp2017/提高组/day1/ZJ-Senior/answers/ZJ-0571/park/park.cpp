#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define N 100005
#define M 200005
int T,n,m,k,p;
int x[M],y[M],z[M],s[N],e[M][2];
int sv[N],ev[M][2];
struct node{
	int d,id;
};
bool operator<(node a,node b){
	return a.d<b.d;
}
node h[M*4];int top;
void ps(node a){
	h[++top]=a;
	int x=top;
	while(x>1&&h[x]<h[x>>1]) std::swap(h[x],h[x>>1]),x=x>>1;
}
node pp(){
	node t=h[1];
	h[1]=h[top];h[top--]=(node){1e9,-1};
	int x=1;
	while(h[x<<1]<h[x]||h[x<<1|1]<h[x]){
		if(h[x<<1]<h[x<<1|1]) std::swap(h[x],h[x<<1]),x=x<<1;
		else std::swap(h[x],h[x<<1|1]),x=x<<1|1;
	}
	return t;
}
int vis[N],dis1[N],disn[N];
void Dijkstra(int S,int*dis){
	rep(i,1,m<<2) h[i]=(node){1e9,-1};
	rep(i,1,n) dis[i]=1e9,vis[i]=0;
	dis[S]=0;top=0;ps((node){0,S});
	while(top){
		node t=pp();
		int x=t.id;
		if(vis[x]) continue;
		else vis[x]=1;
		rep(i,s[x-1]+1,s[x]){
			int to=e[i][0],w=e[i][1];
			if(dis[x]+w<dis[to]){
				dis[to]=dis[x]+w;
				ps((node){dis[to],to});
			}
		}
	}
}
void Dijkstrav(int S,int*dis){
	rep(i,1,m<<2) h[i]=(node){1e9,-1};
	rep(i,1,n) dis[i]=1e9,vis[i]=0;
	dis[S]=0;top=0;ps((node){0,S});
	while(top){
		node t=pp();
		int x=t.id;
		if(vis[x]) continue;
		else vis[x]=1;
		rep(i,sv[x-1]+1,sv[x]){
			int to=ev[i][0],w=ev[i][1];
			if(dis[x]+w<dis[to]){
				dis[to]=dis[x]+w;
				ps((node){dis[to],to});
			}
		}
	}
}
int dp[N][55],st[N][55];//-1 unknown 0 known 1 trying to know
int ans(int x,int y){
	if(st[x][y]==0) return dp[x][y];
	if(dis1[x]+disn[x]-dis1[n]>y) return st[x][y]=0,dp[x][y]=0;
	if(st[x][y]==1) return st[x][y]=0,dp[x][y]=-1;
	st[x][y]=1;
	int sum=(x==n&&y==0)%p;
	rep(i,s[x-1]+1,s[x]){
		int to=e[i][0],w=e[i][1];
		int yy=y-w+(dis1[to]-dis1[x]);
		if(yy>k||yy<0) continue;
		int temp=ans(to,yy);
		if(sum==-1||temp==-1) sum=-1;
		else sum=(sum+temp)%p;
	}
	return st[x][y]=0,dp[x][y]=sum;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		rep(i,1,m) scanf("%d%d%d",&x[i],&y[i],&z[i]);
		rep(i,0,n) s[i]=sv[i]=0;rep(i,1,m) s[x[i]]++,sv[y[i]]++;
		rep(i,1,n) s[i]+=s[i-1],sv[i]+=sv[i-1];per(i,n,1) s[i]=s[i-1],sv[i]=sv[i-1];
		rep(i,1,m) s[x[i]]++,e[s[x[i]]][0]=y[i],e[s[x[i]]][1]=z[i];
		rep(i,1,m) sv[y[i]]++,ev[sv[y[i]]][0]=x[i],ev[sv[y[i]]][1]=z[i];
		Dijkstra(1,dis1);
		Dijkstrav(n,disn);
	//	return 0;
		rep(i,1,n) rep(j,0,k) st[i][j]=-1;
		int sum=0;
		rep(i,0,k){
			int temp=ans(1,i);
			if(sum==-1||temp==-1) sum=-1;
			else sum=(sum+temp)%p;
		}
		printf("%d\n",sum);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
struct line{
	int y,next,w;
}l[200010];
int sum,n,m,k,maxx,ans,p,x,y,w,b,temp;
int fi[100010],dis[100010],q[400010],f[100010],in[100010],head,tail;
int read(){
	int num=0;char c=getchar();
	for(;c<'0'||c>'9';c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
		num=(num<<3)+(num<<1)+c-48;
	return num;
}

void make_line(int x,int y,int w){
	l[++sum].y=y;l[sum].w=w;
	l[sum].next=fi[x];fi[x]=sum;
}

void spfa(){
	memset(f,0,sizeof(f));
	memset(dis,10,sizeof(dis));
	dis[1]=0;
	head=1;tail=1;q[1]=1;
	while(head<=tail){
		int t=q[head];
		for(int i=fi[t];i;i=l[i].next){
			int v=l[i].y;
			if(dis[t]+l[i].w<=dis[v]){
				if(v==n){
					if(dis[v]==dis[t]+l[i].w){
						++temp;
						if(temp>p)temp-=p;
					}else temp=1;
				}
				dis[v]=dis[t]+l[i].w;
				if(!f[v]){
					++in[v];
					if(in[v]>n){
						printf("-1\n");
						b=0;
						return;
					}
					q[++tail]=v;
				}
			}
		}
		f[t]=0;++head;
	}
	maxx=dis[n]+k;
}

void dfs(int x,int now){
	if(x==n)ans=(ans+1)%p;
	for(int i=fi[x];i;i=l[i].next){
		int v=l[i].y;
		if(now+l[i].w>maxx)return;
		dfs(v,now+l[i].w);
	}
}

void work(){
	memset(fi,0,sizeof(fi));
	temp=0;sum=0;b=1;ans=0;
	n=read();m=read();k=read();p=read();
	for(int i=1;i<=m;++i){
		x=read();y=read();w=read();
		make_line(x,y,w);
	}
	spfa();
	if(b&&n<=20){
		dfs(1,0);
		printf("%d\n",ans);
	}else if(b&&k==0)printf("%d\n",temp);
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t=read();
	for(int i=1;i<=t;++i)
		work();
}

#include<bits/stdc++.h>
#include<queue>
using namespace std;
const int p=100000;
long long a,b,sum,last,ans,t1,t2,head1=1,tail1=1,head2=1,tail2=1;
long long q1[100010],q2[100010];
long long read(){
	long long num=0;char c=getchar();
	for(;c<'0'||c>'9';c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
		num=(num<<3)+(num<<1)+c-48;
	return num;
}

void print(long long x){
	if(x>9)print(x/10);
	putchar(x%10+48);
}

void work(){
	a=read();b=read();
	if(a==1||b==1){
		putchar('0');
		exit(0);
	}
	if(a>b)swap(a,b);
	long long k=min(b-a,a);
    sum=0;last=-1;q1[1]=q2[1]=0;
    head1=tail1=head2=tail2=1;
	while(sum<k){
        t1=q1[head1];t2=q2[head2];
        ++tail1;if(tail1>p)tail1-=p;
        ++tail2;if(tail2>p)tail2-=p;
		if(t1<t2){
            ++head1;if(head1>p)head1-=p;
            q1[tail1]=t1+a;
            q2[tail2]=t1+b;
			if(t1==last+1)
				++sum,++last;
			else
				sum=1,last=t1;
		}else if(t1>t2){
            ++head2;if(head2>p)head2-=p;
            q1[tail1]=t2+a;
            q2[tail2]=t2+b;
			if(t2==last+1)
				++sum,++last;
			else
				sum=1,last=t2;
		}else{
            ++head1;++head2;
            if(head1>p)head1-=p;if(head2>p)head2-=p;
            q1[tail1]=t1+a;
            q2[tail2]=t1+b;
			if(t1==last+1)
				++sum,++last;
			else
				sum=1,last=t1;
		}
	}
	ans=last-k+1;
	while(k<a){
		ans+=a;
		k+=b-a;
	}
	print(ans-1);
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	work();
	return 0;
}

#include<cstdio>
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
typedef long long LL;
int a,b,x,y;
inline int read()
{
	char ch=getchar();int x=0,f=1;
	for(;(ch<'0'||ch>'9')&&ch!='-';) ch=getchar();
	if(ch=='-') f=-1,ch=getchar();
	for(;ch>='0'&&ch<='9';) x=x*10+ch-48,ch=getchar();
	return x*f;
}
inline void gcd(int a,int b,int &x,int &y)
{
	if(b==0)
	{
		x=1;y=0;
		return ;
	}
	gcd(b,a%b,y,x);
	y-=a/b*x;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();b=read();
	if(a>b) a^=b^=a^=b;
	gcd(-a,b,x,y);
	LL Y=a-1,X=Y*x-1,ans=X*a+Y;
	printf("%lld\n",ans);
	return 0;
}

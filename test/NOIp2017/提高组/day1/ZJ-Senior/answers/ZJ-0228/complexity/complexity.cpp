#include<cstdio>
#include<cstring>
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
typedef long long LL;
int l,p,i,F,lx,ly,anx,ans,b[150];char ch,a[110][20];
inline char gc()
{
	char t=a[lx][ly++];
	if(!a[lx][ly]) lx++,ly=0;
	return t;
}
inline int read()
{
	ch=gc();int x=0;
	for(;ch<'0'||ch>'9';) ch=gc();
	for(;ch>='0'&&ch<='9';) x=x*10+ch-48,ch=gc();
	return x;
}
inline void ER()
{
	printf("ERR\n");
}
inline int max(int x,int y) {return x>y?x:y;}
int find(int &dp)
{
	int ms=1,mp=0,t;
	for(ch=gc();ch<'a'||ch>'z';) ch=gc();
	if(b[ch]) return 0;b[ch]=1;
	for(ch=gc();ch!='n'&&(ch<'0'||ch>'9');) ch=gc();
	if(ch=='n')
	{
		for(ch=gc();ch!='n'&&(ch<'0'||ch>'9');) ch=gc();
		if(ch!='n') return 0;
		for(ch=gc();ch!='F'&&ch!='E';) ch=gc();
		if(ch=='F') {if(find(t=dp+1)) ms=max(t-1,ms);else return 0;}
	}
	else
	{
		for(;lx<=l&&ch>='0'&&ch<='9';) ch=gc();
		for(;lx<=l&&ch!='n'&&(ch<'0'||ch>'9');) ch=gc();
		if(ch=='n') mp=1;
	}
	if(lx>l) return 0;
	for(ch=gc();ch!='F'&&ch!='E';) ch=gc();
	for(;ch=='F'&&lx<=l;)
	{
		F++;
		if(find(t=dp+1)) ms=max(ms,t-1);else return 0;
		if(lx>l) return 0;
		for(ch=gc();lx<=l&&ch!='F'&&ch!='E';) ch=gc();
	}
	dp=ms+mp;F--;
	return 1;
}
void Work()
{
	memset(a,0,sizeof(a));
	gets(a[0]);lx=ly=0;
	l=read();
	for(ch=gc();ch!='1'&&ch!='n';)	ch=gc();
	if(ch!='n')	p=0;else p=read();
	fo(i,1,l) gets(a[i]);
	lx=1;ly=0;
	memset(b,0,sizeof(b));F=0;
	for(;F>=0&&lx<=l;)
	{
	for(ch=gc();ch!='F'&&ch!='E';)	ch=gc();
	if(ch=='F') F++;
	if(ch=='F')
	if(find(anx)) ans=max(anx,ans);
	else {ER();return;}
	else if(--F<0) {ER();return;}
	}
	if(F!=0) ER();
	else if(ans==p) printf("Yes\n");else printf("No\n");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	gets(a[0]);lx=ly=0;
	for(int T=read();T--;)
	Work();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<set>
#include<map>
#include<iostream>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,f=1,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;
const int M=200005;

int n,m,k,Mod;
inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
struct Edge{
	int next,to,wei;
};
bool ST;
struct Min_Heap{
	PII v[M];
	int size;
	void up(int p){
		while (p>1 && v[p]<v[p>>1]){
			swap(v[p],v[p>>1]);p>>=1;
		}
	}
	void down(int p){
		while (true){
			int k=p,l=p<<1,r=p<<1|1;
			if (l<=size && v[l]<v[k]) k=l;
			if (r<=size && v[r]<v[k]) k=r;
			if (k==p) break;
			swap(v[k],v[p]);p=k;
		}
	}
	inline void insert(PII val){
		v[++size]=val;up(size);
	}
	inline void pop(){
		swap(v[1],v[size]);
		size--;down(1);
	}
	inline int top(){
		return v[1].second;
	}
	inline void clear(){
		size=0;
	}
	inline bool empty(){
		return size==0;
	}
}Heap;
struct Min_Path{
	int lines,front[N];
	Edge E[M];
	inline void Addline(int u,int v,int w){
		E[++lines]=(Edge){front[u],v,w};front[u]=lines;
	}
	int Dis[N],Vis[N],Inv[N],Cnt[N];
	void Solve(){
		memset(Dis,127,sizeof(Dis));
		memset(Vis,0,sizeof(Vis));
		memset(Inv,-1,sizeof(Inv));
		memset(Cnt,0,sizeof(Cnt));
		Dis[1]=0;
		Cnt[1]=1;
		Heap.clear();
		Heap.insert(make_pair(Dis[1],1));
		while (!Heap.empty()){
			int u=Heap.top();Heap.pop();
			if (Vis[u]) continue;
			Vis[u]=true;
			for (int i=front[u],v;i!=0;i=E[i].next){
				if (Dis[u]+E[i].wei<=Dis[v=E[i].to]){
					if (Dis[u]+E[i].wei<Dis[v]){
						Inv[v]=E[i].wei;
						Cnt[v]=0;
						Dis[v]=Dis[u]+E[i].wei;
						Heap.insert(make_pair(Dis[v],v));
					}
					Dis[v]=Dis[u]+E[i].wei;
					Add(Cnt[v],Cnt[u]);
				}
			}
		}
	}
	void clear(){
		lines=0;
		memset(front,0,sizeof(front));
	}
}Mp;
int a[M],b[M],c[M];
int lines,front[N*55];
Edge G[M*55];
inline void Addline(int u,int v,int w){
	G[++lines]=(Edge){front[u],v,w};front[u]=lines;
}
inline int Mir(int p,int t){
	return t*n+p;
}
int Q[N*55],Cnt[N*55],Dgr[N*55],Dis[N];
void Work(){
	n=read(),m=read(),k=read(),Mod=read();
	Rep(i,1,m) a[i]=read(),b[i]=read(),c[i]=read();
	Mp.clear();
	Rep(i,1,m){
		Mp.Addline(a[i],b[i],c[i]);
	}
	Mp.Solve();
	lines=0;
	memset(front,0,sizeof(front));
	Mp.clear();
	Rep(i,1,n) Dis[i]=Mp.Dis[i];
	Rep(i,1,m){
		Rep(j,0,k){
			int pos=j+c[i]-(Dis[b[i]]-Dis[a[i]]);
			if (pos<=k) Mp.Addline(Mir(a[i],j),Mir(b[i],pos),0);
		}
	}
}
bool ED;
int main(){
//	printf("%lf\n",1.0*(&ED-&ST)/1024/1024);
	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	int T=read();
	Rep(i,1,T) Work();
}
/*
1
2 2 10 100
1 2 1
2 1 1

1
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
*/

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<set>
#include<map>
#include<iostream>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,f=1,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;
const int M=200005;

int n,m,k,Mod;
inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
struct Edge{
	int next,to,wei;
};
struct Min_Heap{
	PII v[M];
	int size;
	void up(int p){
		while (p>1 && v[p]<v[p>>1]){
			swap(v[p],v[p>>1]);p>>=1;
		}
	}
	void down(int p){
		while (true){
			int k=p,l=p<<1,r=p<<1|1;
			if (l<=size && v[l]<v[k]) k=l;
			if (r<=size && v[r]<v[k]) k=r;
			if (k==p) break;
			swap(v[k],v[p]);p=k;
		}
	}
	inline void insert(PII val){
		v[++size]=val;up(size);
	}
	inline void pop(){
		swap(v[1],v[size]);
		size--;down(1);
	}
	inline int top(){
		return v[1].second;
	}
	inline void clear(){
		size=0;
	}
	inline bool empty(){
		return size==0;
	}
}Heap;
struct Min_Path{
	int lines,front[N];
	Edge E[M];
	inline void Addline(int u,int v,int w){
		E[++lines]=(Edge){front[u],v,w};front[u]=lines;
	}
	int Dis[N],Vis[N],Inv[N],Cnt[N];
	void Solve(){
		memset(Dis,127,sizeof(Dis));
		memset(Vis,0,sizeof(Vis));
		memset(Inv,-1,sizeof(Inv));
		memset(Cnt,0,sizeof(Cnt));
		Dis[1]=0;
		Cnt[1]=1;
		Heap.clear();
		Heap.insert(make_pair(Dis[1],1));
		while (!Heap.empty()){
			int u=Heap.top();Heap.pop();
			if (Vis[u]) continue;
			Vis[u]=true;
			for (int i=front[u],v;i!=0;i=E[i].next){
				if (Dis[u]+E[i].wei<=Dis[v=E[i].to]){
					if (Dis[u]+E[i].wei<Dis[v]){
						Inv[v]=E[i].wei;
						Cnt[v]=0;
						Dis[v]=Dis[u]+E[i].wei;
						Heap.insert(make_pair(Dis[v],v));
					}
					Dis[v]=Dis[u]+E[i].wei;
					Add(Cnt[v],Cnt[u]);
				}
			}
		}
	}
	void clear(){
		lines=0;
		memset(front,0,sizeof(front));
	}
}Mp;
int a[M],b[M],c[M];
int f[N][55];
int lines,front[N];
Edge G[M];
inline void Addline(int u,int v,int w){
	G[++lines]=(Edge){front[u],v,w};front[u]=lines;
}
int Dgr[N],Q[N];
vector<int>V[N],R[N];
void Work(){
	n=read(),m=read(),k=read(),Mod=read();
	Rep(i,1,m) a[i]=read(),b[i]=read(),c[i]=read();
	Mp.clear();
	Rep(i,1,m){
		Mp.Addline(a[i],b[i],c[i]);
	}
	Mp.Solve();
//	Rep(i,1,n) printf("at %d dis=%d cnt=%d\n",i,Mp.Dis[i],Mp.Cnt[i]);
	Rep(i,1,n) V[i].clear();
	Rep(i,1,n) R[i].clear();
	memset(Dgr,0,sizeof(Dgr));
	lines=0;
	memset(front,0,sizeof(front));
	Rep(i,1,m){
		if (Mp.Dis[a[i]]+c[i]==Mp.Dis[b[i]]){
			V[a[i]].push_back(b[i]);
			R[b[i]].push_back(a[i]);
			Dgr[b[i]]++;
		}
		Addline(b[i],a[i],c[i]);
	}
	int head=0,tail=0;
	Rep(i,1,n) if (Dgr[i]==0) Q[++tail]=i;
	while (head<tail){
		int u=Q[++head];
		Rep(i,0,V[u].size()-1){
			int v=V[u][i];
			Dgr[v]--;
			if (Dgr[v]==0) Q[++tail]=v;
		}
	}
	memset(f,0,sizeof(f));
	Rep(i,1,n) f[i][0]=Mp.Cnt[i];
	Rep(t,1,k){
//		printf("t=%d\n",t);
		Rep(i,1,n){
			for (int j=front[i];j!=0;j=G[j].next){
				int v=G[j].to;
				int pos=Mp.Dis[i]+t-Mp.Dis[v]-G[j].wei;
//				printf("%d->%d pos=%d\n",v,i,pos);
				if (pos>=0 && pos<t) Add(f[i][t],f[v][pos]);
			}
		}
		Rep(i,1,n){
			int u=Q[i];
			Rep(j,0,R[u].size()-1) Add(f[u][t],f[R[u][j]][t]);
		}
//		Rep(i,1,n) printf("%d ",f[i][t]);puts("");
	}
	int Ans=0;
	Rep(t,0,k) Add(Ans,f[n][t]);
	printf("%d\n",Ans);
}
int main(){
//	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	int T=read();
	Rep(i,1,T) Work();
}
/*
1
2 2 100 100
1 2 1
2 1 1
*/

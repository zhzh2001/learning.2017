#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<set>
#include<map>
#include<iostream>
#include<vector>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,f=1,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;
const int M=200005;

int n,m,k,Mod;
set<PII>Map;
int main(){
	freopen("park.in","w",stdout);
	srand(time(0));
	Rep(i,1,10) rand();
	printf("1\n");
	printf("%d %d %d %d\n",n=10,m=60,k=rand()%10,Mod=100);
	Rep(i,1,m){
		int x=rand()%n+1,y=rand()%n+1,w=rand()%5+1;
		while (x==y || Map.count(make_pair(x,y))){
			x=rand()%n+1,y=rand()%n+1;
		}
		printf("%d %d %d\n",x,y,w);
		Map.insert(make_pair(x,y));
	}
}

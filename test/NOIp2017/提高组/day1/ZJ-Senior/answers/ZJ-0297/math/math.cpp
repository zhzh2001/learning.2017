#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<set>
#include<map>
#include<iostream>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,f=1,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int x,y;
inline bool check(int val){
	Rep(i,0,val/x) if ((val-i*x)%y==0) return true;
	return false;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	x=read(),y=read();
	if (x==1 || y==1){
		puts("0");return 0;
	}
	printf("%lld\n",1ll*x*y-x-y);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<set>
#include<map>
#include<iostream>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,f=1,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int x,y;
inline bool check(int val){
	Rep(i,0,val/x) if ((val-i*x)%y==0) return true;
	return false;
}
int Cal(int a,int b){
	x=a,y=b;
//	x=read(),y=read();
	Dep(i,100,0) if (!check(i)){
		return i;
	}
	return -1;
}
inline int Gcd(int a,int b){
//	printf("a=%d b=%d\n",a,b);
	if (b==0) return a;
		else return Gcd(b,a%b);
}
int main(){
	Rep(x,1,10) Rep(y,1,10) if (Gcd(x,y)==1) printf("Cal(%d,%d)=%d\n",x,y,Cal(x,y));
}

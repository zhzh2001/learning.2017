#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<set>
#include<map>
#include<iostream>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int res=0,f=1,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
inline int Getnum(){
	int res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='n') return -1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return res;
}
const int N=105;

int Map[350];
int l,Cpe,top;
int Ope[N],Dep[N],Sup[N];
int Work(){
	memset(Map,0,sizeof(Map));
	l=read();int ch=getchar();
	while (ch!='O') ch=getchar();getchar();ch=getchar();
	if (ch=='1') Cpe=0;else{
		getchar();Cpe=read();
	}
//	printf("Cep=%d\n",Cpe);
	top=0;
	int Ans=0,flag=1;
	Rep(i,1,l){
		ch=getchar();while (ch!='F' && ch!='E') ch=getchar();
		if (ch=='F'){
//			puts("here top++");
			top++;
			getchar();Ope[top]=getchar();
			if (Map[Ope[top]]) flag=false;
			Map[Ope[top]]=true;
			Dep[top]=Dep[top-1];
			Sup[top]=Sup[top-1];
			int x=Getnum(),y=Getnum();
			if (x!=-1 && y!=-1){
				if (x>y) Sup[top]=true;
			}
			else if (x==-1 && y==-1){
			}
			else if (x==-1){
				Sup[top]=true;
			}
			else if (y==-1){
				Dep[top]++;
			}
			if (!Sup[top]) Ans=max(Ans,Dep[top]);
		}
		else{
//			puts("stack out!");
			if (top==0){
				flag=0;continue;
			}
			Map[Ope[top]]=false;
			top--;
		}
	}
	if (!flag) return -1;
	if (top>0) return -1;
	if (Ans==Cpe) return 1;
		else return 0;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	Rep(i,1,T){
		int res=Work();
		if (res==-1) puts("ERR");
		if (res==0) puts("No");
		if (res==1) puts("Yes");
	}
}
/*
2
1 O(1)
F x 1 n
4 O(n^2)
F x 5 n
F y 10 n
E
E

8
2 O(1)
F i 1 1
E
2 O(n^1)
F x 1 n
E
1 O(1)
F x 1 n
4 O(n^2)
F x 5 n
F y 10 n
E
E
4 O(n^2)
F x 9 n
E
F y 2 n
E
4 O(n^1)
F x 9 n
F y n 4
E
E
4 O(1)
F y n 4
F x 9 n
E
E
4 O(n^2)
F x 1 n
F x 1 10
E
E
*/

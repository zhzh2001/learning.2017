var
   t,i,j,hang,right,ff,ee,ans,head,tail,max,k:longint;
   s,ss,sstart,eend,ch:string;
   p:boolean;
   a:array[1..1000] of string;
   use:array['a'..'z'] of boolean;
   keyi:array[1..1000] of boolean;
   bl:array[1..1000] of char;
   zs:array[1..1000] of longint;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input); rewrite(output);
  readln(t);
  while t>0 do
   begin
    dec(t);
    readln(s);
    for j:=1 to length(s) do if s[j]=' ' then break;
    val(copy(s,1,j-1),hang);
    delete(s,1,j);
    if s[3]='n' then val(copy(s,5,length(s)-5),right) else right:=0;
    ff:=0; ee:=0;
    p:=true;
    for j:=1 to hang do
    begin
    readln(a[j]);
    ss:=a[j];
    if ss[1]='F' then inc(ff) else inc(ee);
    end;
    if (p=false) or (ff<>ee) then writeln('ERR') else begin
    i:=0; ans:=0;
    fillchar(use,sizeof(use),false);
    fillchar(keyi,sizeof(keyi),true);
    fillchar(zs,sizeof(zs),0);
    while i<hang do
     begin
      inc(i);
      ch:=a[i]+' ';
      if ch[1]='E' then
       begin
        for j:=i-1 downto 1 do
         begin
          ss:=a[j];
          if (ss[1]='F') and (keyi[j]) then break;
         end;
        use[bl[j]]:=false;
        keyi[j]:=false;
       end;
      if ch[1]='F' then
       begin
        bl[i]:=ch[3];
        if use[bl[i]] then begin p:=false; break; end;
        use[bl[i]]:=true;
        delete(ch,1,4);
        for j:=1 to length(ch) do if ch[j]=' ' then break;
        sstart:=copy(ch,1,j-1);
        delete(ch,1,j);
        for j:=1 to length(ch) do if ch[j]=' ' then break;
        eend:=copy(ch,1,j-1);
        if (sstart<>'n') and (eend='n')  then
         begin
          max:=0;
          for j:=i-1 downto 1 do if keyi[j] and (zs[j]>max) then max:=zs[j];
          zs[i]:=max+1;
         end;
        if (sstart<>'n') and (eend<>'n') then
         begin
          val(sstart,head); val(eend,tail);
          ff:=0; ee:=0;
          if head>tail then     begin
           for j:=i to hang do
            begin
             ss:=a[j];
             if ss[1]='F' then inc(ff) else inc(ee);
             if ee-ff=0 then break;
            end;
          for k:=i to j do keyi[k]:=false;
          i:=j;   end;
         end;
        if (sstart='n') and (eend<>'n') then
         begin
          for j:=i to hang do
           begin
            ss:=a[j];
            if ss[1]='F' then inc(ff) else inc(ee);
            if ee-ff=0 then break;
           end;
          for k:=i to j do keyi[k]:=false;
          i:=j;
         end;

       end;

     end;
    if p=false then writeln('ERR');
    if p then
     begin
      max:=0;
      for i:=1 to hang do if zs[i]>max then max:=zs[i];
      if right=max then writeln('Yes') else writeln('No');
     end;
     end;
   end;
   close(input); close(output);
  end.

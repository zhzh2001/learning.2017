var
  t,n,m,k,p,i,c,x,y,z:longint;
begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input); rewrite(output);
  read(t);
  while t>0 do
   begin
    dec(t);
    read(n,m,k,p);
    c:=0;
    for i:=1 to m do
     begin
      read(x,y,z);
      if z=0 then inc(c);
     end;
    if c=m then writeln(-1) else writeln(3 mod p);
   end;
  close(input); close(output);
end.


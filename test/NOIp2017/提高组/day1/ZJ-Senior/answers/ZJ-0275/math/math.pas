var
  a,b,num,i,t:longint;
  q:array[1..3000000] of longint;
  p:array[1..3000000] of int64;
  ans:qword;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input); rewrite(output);
  read(a,b);
  if a<b then begin t:=a; a:=b; b:=t; end;
  p[1]:=a;
  q[1]:=a mod b;
  num:=1;
  while true do
   begin
    inc(num);
    p[num]:=p[num-1]+a;
    q[num]:=p[num] mod b;
    if q[num]=q[1] then begin dec(num); break; end;
   end;
 // for i:=1 to num do write(q[i],' '); writeln;
  for i:=num downto 1 do
   if q[i]<>0 then
    begin
     ans:=p[i]-b;
     break;
    end;
  writeln(ans);
  close(input); close(output);
end.


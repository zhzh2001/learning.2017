#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int head[400005],Next[400005],to[400005],len[400005],tot,n,m,t,k,p,dis[100005],map[100005],q[400005],res;
void add(int a,int b,int c){
	Next[tot]=head[a];
	head[a]=tot;
	to[tot]=b;
	len[tot]=c;
	tot++;
}
void spfa(){
	for (int i=1; i<=n; i++){
		dis[i]=1000000000;
	}
	memset(map,0,sizeof(map));
	dis[1]=0;
	q[1]=1;
	map[1]=1;
	int st=0,ed=1;
	while (st<ed){
		st++;
		int x=q[st];
		for (int i=head[x]; i!=-1; i=Next[i]){
			int y=to[i];
			if (dis[y]>dis[x]+len[i]){
				dis[y]=dis[x]+len[i];
				if (map[y]==0){
					ed++;
					q[ed]=y;
					map[y]=1;
				}
			}
		}
		map[x]=0;
	}
}
void dfs(int x,int t){
	if (t<0) return;
	if (x==n) res=(res+1)%p;
	for (int i=head[x]; i!=-1; i=Next[i]){
		dfs(to[i],t-len[i]);
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		tot=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1; i<=n; i++) head[i]=-1;
		int flag=0;
		for (int i=1; i<=m; i++){
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			add(a,b,c);
		}
		spfa();
		if (dis[n]==0) {
			printf("-1\n");
			continue;
		}
		res=0;
		dfs(1,dis[n]+k);
		printf("%d\n",res%p);
	}
	return 0;
}

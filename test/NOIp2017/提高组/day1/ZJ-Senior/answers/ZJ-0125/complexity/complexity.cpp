#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int t,l,map[1005];
char s[105];
char c1,c2[105],s1[10],s2[10];
char q1[105];
int q2[105];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout); 
	scanf("%d",&t);
	while (t--){
		scanf("%d",&l);
		scanf("%s",s+1);
		memset(map,0,sizeof(map));
		memset(q1,0,sizeof(q1));
		memset(q2,0,sizeof(q2));
		int flag;
		if (s[3]=='1') {
			flag=0;
		}else {
			int j=5;
			flag=0;
			while (s[j]<='9' && s[j]>='0'){
				flag=flag*10+(int)(s[j])-(int)('0');
				j++;
			}
		}
		int tot=0,res=0,now=0,bo=0,zzz=0;
		for (int i=1; i<=l; i++){
			cin>>c1;
			if (c1=='F'){
				cin>>c2[i];
				scanf("%s%s",s1+1,s2+1);
				if (map[(int)(c2[i])]==1) {
					bo=1;
				}else {
					tot++;
					q1[tot]=c2[i];
					map[(int)(c2[i])]=1;
				}
				if (!zzz){	
					if (s1[1]=='n' && s2[1]!='n') zzz=1;
					if (s1[1]!='n' && s2[1]!='n') {
						int x1=strlen(s1+1),x2=strlen(s2+1);
						if (x1>x2) zzz=1;
						if (x1==x2){
							int flag=0;
							for (int j=1; j<=x1; j++){
								if (s1[j]>s2[j]) {
									flag=1;
									break;
								}
								if (s1[j]==s2[j]) continue;
								if (s1[j]<s2[j]){
									break;
								} 
							}
							if (flag) zzz=1;
						}
					}
					if (s2[1]=='n'){
						if (s1[1]!='n') {
							now++;
							q2[tot]=1;
						}
						res=max(now,res);
					}
				}
			} else {
				map[(int)(q1[tot])]=0;
				if (q2[tot]==1) {
					now--;
					q2[tot]=0;
				}
				tot--;
			}
			if (tot<0) bo=1;
		}
		if (bo==1) {
			printf("ERR\n");
			continue;
		}
		if (tot!=0) {
			printf("ERR\n");
			continue;
		}
		if (res==flag) printf("YES\n");
		else printf("NO\n");
	}
	return 0;	
}
 

var a,b,c,f,g,l:array[0..10000] of longint;
    e,d:array[0..20000000] of longint;
    scc,n,m,k,p,i,x,y,h,t,ans:longint;

begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input); rewrite(output);
  readln(scc);
  for scc:=1 to scc do
    begin
      readln(n,m,k,p);
      for i:=1 to n do f[i]:=-1;
      for i:=1 to m do
        begin
          readln(a[i],b[i],c[i]);
          if f[a[i]]=-1 then
            begin
              f[a[i]]:=i; g[i]:=-1;
            end
          else
            begin
              g[i]:=f[a[i]]; f[a[i]]:=i;
            end;
        end;
      for i:=1 to n do l[i]:=maxlongint; l[1]:=0;
      h:=0; t:=1; d[t]:=1; e[t]:=0;
      repeat
        inc(h);
        if e[h]<=l[d[h]] then
          begin
            x:=f[d[h]];
            while x<>-1 do
              begin
                if e[h]+c[x]<l[b[x]] then
                  begin
                    inc(t);
                    d[t]:=b[x]; e[t]:=e[h]+c[x]; l[b[x]]:=e[t];
                  end;
                x:=g[x];
              end;
          end;
      until h>=t;
      y:=l[n]; ans:=0;
      h:=0; t:=1; d[t]:=1; e[t]:=0;
      repeat
        inc(h);
        x:=f[d[h]];
        while x<>-1 do
          begin
            if e[h]+c[x]<=y+k then
              begin
                inc(t);
                d[t]:=b[x]; e[t]:=e[h]+c[x];
              end;
            x:=g[x];
          end;
        if d[h]=n then
          begin
            ans:=ans+1;
            if ans=p then ans:=0;
          end;
      until h>=t;
      writeln(ans);
    end;
  close(input); close(output);
end.
var n,m,ans:int64;

begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input); rewrite(output);
  readln(n,m);
  ans:=n*m-n-m;
  writeln(ans);
  close(input); close(output);
end.

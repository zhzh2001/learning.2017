var a:array['a'..'z'] of boolean;
    b:array[-1000..10000] of char;
    c:array[-1000..10000] of longint;
    t,l,i,o,max,h,x,y,ll:longint;
    p:boolean;
    s:ansistring;
    ch:char;

begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input); rewrite(output);
  readln(t);
  for t:=1 to t do
    begin
      readln(s);
      ch:=s[1]; l:=0; i:=1;
      while ('0'<=ch)and(ch<='9') do
        begin
          l:=l*10+ord(ch)-48;
          inc(i); ch:=s[i];
        end;
      insert(' ',s,length(s));
      if s[i+3]='1' then o:=1
      else
        begin
          o:=0;
          while ('0'<=s[i+5])and(s[i+5]<='9') do
            begin
              o:=o*10+ord(s[i+5])-48; inc(i);
            end;
          inc(o);
        end;
      p:=true; max:=0; fillchar(a,sizeof(a),false);
      h:=0; c[0]:=1;
      for l:=1 to l do
        begin
          readln(s); ll:=length(s);
          if s[1]='F' then
            begin
              while not((s[ll]='n')or((s[ll]>='0')and(s[ll]<='9'))) do dec(ll);
              if a[s[3]] then p:=false;
              inc(h); b[h]:=s[3]; a[s[3]]:=true;
              if (s[5]<>'n')and(s[ll]='n') then
                begin
                  if c[h-1]=0 then c[h]:=0 else c[h]:=c[h-1]+1;
                end
              else if (s[5]<>'n')and(s[ll]<>'n') then
                begin
                  x:=0; y:=0; i:=5;
                  while ('0'<=s[i])and(s[i]<='9') do
                    begin
                      x:=x*10+ord(s[i])-48; inc(i);
                    end;
                  for i:=i+1 to ll do y:=y*10+ord(s[i])-48;
                  if x<=y then c[h]:=c[h-1] else c[h]:=0;
                end
              else c[h]:=0;
              if c[h]>max then max:=c[h];
            end
          else
            begin
              dec(h);
              if h>=0 then a[b[h+1]]:=false;
              if h<0 then p:=false;
            end;
        end;
      if h<>0 then p:=false;
      if max=0 then max:=1;
      if not(p) then writeln('ERR')
      else if (p) and (max=o) then writeln('Yes')
      else writeln('No');
    end;
  close(input); close(output);
end.

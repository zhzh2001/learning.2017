#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	f=1;x=0;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f;
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}
template<class T>void ptk(T x){prin(x);putchar(' ');}
template<class T>void ptn(T x){prin(x);putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

int A,B;
int gcd(int a,int b){
	if(!b)return a;
	return gcd(b,a%b);
}
struct P_0{
	bool mark[5005];
	void work(){
		memset(mark,0,sizeof mark);
		mark[0]=true;
		int cnt=0,ans=A-1;
		rep(i,0,4951){
			if(mark[i]){
				++cnt;
				if(cnt==A)break;
				mark[i+A]=mark[i+B]=true;
			}else Max(ans,i),cnt=0;
		}
		ptn(ans);
	}
}P0;
struct P_1{
	void work(){
		ll ans=(ll)B*(A-1)-A;
		ptn(ans);
	}
}P1;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&A,&B);
	if(A>B)swap(A,B);
	if(0);
	else if(A==2)ptn(B-2);
	else if(B<=50){
		P0.work();
	}else P1.work();
	return 0;
}

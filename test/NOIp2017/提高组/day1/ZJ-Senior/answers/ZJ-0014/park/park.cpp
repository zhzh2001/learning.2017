#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>
#include<queue>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	f=1;x=0;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f;
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}
template<class T>void ptk(T x){prin(x);putchar(' ');}
template<class T>void ptn(T x){prin(x);putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

const int N=(int)1e5+5,M=(int)2e5+5;

int head[N];
struct node{
	int to,val,nxt;
}G[M];
int n,m,K,mod,dp[51][N];
struct ss{
	int id,v;
	bool operator<(const ss &A)const{
		return v>A.v;
	}
};
priority_queue<ss>pque;
void add_mod(int &a,int b){
	if((a+=b)>=mod)a-=mod;
}
int dis[N];
void Dij(){
	memset(dis,-1,sizeof dis);
	dis[1]=0;
	while(!pque.empty())pque.pop();
	pque.push((ss){1,0});
	ss now;int x;
	while(!pque.empty()){
		now=pque.top();pque.pop();
		x=now.id;
		if(dis[x]!=now.v)continue;
		for(int i=head[x];i;i=G[i].nxt){
			int y=G[i].to,v=dis[x]+G[i].val;
			if(dis[y]==-1||dis[y]>v){
				dis[y]=v;
				pque.push((ss){y,dis[y]});
			}
		}
	}
}
struct P_0{
	int dfs(int x,int d){
		if(K<d-dis[x])return 0;
		int &res=dp[d-dis[x]][x];
		if(res==-1){
			res=(x==n);
			for(int i=head[x];i;i=G[i].nxt)
				add_mod(res,dfs(G[i].to,G[i].val+d));
		}
		return res;
	}
	void work(){//������-1 
		Dij();
		rep(c,0,K+1)memset(dp[c],-1,(n+1)<<2);
		int ans=dfs(1,0);
		ptn(ans);
	}
}P0;
struct P_1{
	bool flag,vis[51][N],mark[N];
	int dfs(int x,int d){
		if(K<d-dis[x]||flag||!mark[x])return 0;
		int &res=dp[d-dis[x]][x];
		if(res==-1){
			vis[d-dis[x]][x]=true;
			res=x==n;
			for(int i=head[x];i;i=G[i].nxt)
				add_mod(res,dfs(G[i].to,G[i].val+d));
			vis[d-dis[x]][x]=false;
		}else flag|=vis[d-dis[x]][x];
		return res;
	}
	int rhead[N],que[N];
	pii rG[M];
	void work(){
		Dij();
		rep(c,0,K+1)memset(dp[c],-1,(n+1)<<2),memset(vis[c],0,(n+1)*sizeof(bool));
		{
			memset(rhead,0,(n+1)<<2);
			int i,y;
			rep(x,1,n+1){
				for(i=head[x];i;i=G[i].nxt){
					y=G[i].to;
					rG[i]=pii(x,rhead[y]);rhead[y]=i;
				}
			}
			int l=0,r=0,x;
			memset(mark,0,(n+1)*sizeof(bool));
			que[r++]=n;mark[n]=true;
			while(l<r){
				x=que[l++];
				for(i=rhead[x];i;i=rG[i].se){
					y=rG[i].fi;
					if(!mark[y]){
						mark[y]=true;
						que[r++]=y;
					}
				}
			}
		}
		flag=false;
		int ans=dfs(1,0);
		if(flag)ans=-1;
		ptn(ans);
	}
}P1;
void solve(){
	memset(head,0,(n+1)<<2);
	rd(n),rd(m),rd(K),rd(mod);
	bool flag=true;
	for(int a,b,c,i=1;i<=m;++i){
		rd(a),rd(b),rd(c);
		flag&=c>0;
		G[i]=(node){b,c,head[a]};head[a]=i;
	}
	if(mod==1)ptn(0);
	else if(flag)P0.work();
	else P1.work();
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;rd(T);
	while(T--)solve();
	return 0;
}

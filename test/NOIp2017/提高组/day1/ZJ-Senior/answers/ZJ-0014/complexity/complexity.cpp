#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	f=1;x=0;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f;
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}
template<class T>void ptk(T x){prin(x);putchar(' ');}
template<class T>void ptn(T x){prin(x);putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

const int N=105;

int n,t;
bool mark[250],flagERR;
pii stk[N];
int work(char *str,int len){
	if(len==1&&str[0]=='n')return -1;
	int num=0;
	rep(i,0,len)num=num*10+(str[i]-'0');
	return num;
}
int dfs(){
	char s[10],a[10];
	int nowi;
	scanf("%s",s);
	nowi=s[0];
	if(mark[nowi])flagERR=true;
	else mark[nowi]=true;
	scanf("%s%s",s,a);
	int A=work(s,strlen(s)),B=work(a,strlen(a)),now=-(int)1e9;
	if(t==n){
		flagERR=true;
		return 0;
	}
	if(A==-1){
		if(B==-1)now=0;
		else now=(int)-1e9;
	}else {
		if(B==-1)now=1;
		else if(B<A)now=(int)-1e9;
		else now=0;
	}
	int ans=0;
	while(t!=n){
		++t;
		char st[10];
		scanf("%s",st);
		if(st[0]=='E'){
			mark[nowi]=false;
			return max(now+ans,0);
		}
		Max(ans,dfs());
	}
	flagERR=true;
	return max(now,0);
}
void solve(){
	char str[10];
	scanf("%d%s",&n,str+1);
	int ans=0,len=strlen(str+1);
	if(str[3]=='n'){
		rep(i,5,len)ans=ans*10+(str[i]-'0');
	}else ans=0;
	memset(mark,0,sizeof mark);
	t=0;flagERR=false;
	int res=0;
	while(t!=n){
		++t;
		char s[5];
		scanf("%s",s);
		if(s[0]=='E')flagERR=true;
		else Max(res,dfs());
	}
	if(flagERR)puts("ERR");
	else if(ans==res)puts("Yes");
	else puts("No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;rd(T);
	while(T--)solve();
	return 0;
}

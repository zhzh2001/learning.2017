var a,b,x:int64;

begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input); rewrite(output);
  readln(a,b);
  x:=a*b-(a+b);
  writeln(x);
  close(input); close(output);
end.

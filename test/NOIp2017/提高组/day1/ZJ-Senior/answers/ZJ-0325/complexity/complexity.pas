const s1='Yes'; s2='No'; s3='ERR';
var t,i,j,line,len1,w,max,now,q:longint;
    f:boolean;
    s,o,ans,ci:string;
    ss:array[0..20000]of string;
    exist:array['a'..'z']of boolean;
    done:array[0..20000]of boolean;

procedure work(i:longint);
  var c2,c3:string;
      c1:char;
      j:longint;
  begin
    done[i]:=true;
    if ss[i]='E' then
      exit;
    if ss[i]<>'E'then
    begin
    c1:=ss[i][3];
    c2:=''; c3:='';
    if exist[c1] then
    begin
      writeln(s3);
      f:=true;
      exit;
    end;
    exist[c1]:=true;
    for j:=5 to length(ss[i]) do
    begin
      if ss[i][j]<>' ' then c2:=c2+ss[i][j]
      else break;
    end;
    c3:=copy(ss[i],6+length(c2),length(ss[i])-length(c2)-5);
    j:=i+1;
    if (c2='n')and(c3<>'n') then
    begin
      while ss[j][1]='F' do
      begin
        done[j]:=true;
        inc(j);
      end;
      exit;
    end;
    if (c2<>'n')and(c3='n')and(not f) then
    begin
      inc(now);
      if now>max then max:=now;
      inc(q);
      work(i+1);
    end;
    end;
    exist[c1]:=false;
  end;

begin
  assign(input,'complexity.in'); assign(output,'complexity.out');
  reset(input);rewrite(output);
  readln(t);
  while t>0 do
  begin
    max:=0; f:=false; q:=0;
    for i:=1 to 200 do
    begin
      ss[i]:='';
      done[i]:=false;
    end;
    for i:=1 to 26 do exist[chr(i+96)]:=false;
    read(line);
    readln(s);
    s:=copy(s,2,length(s)-1);
    len1:=length(s);
    o:=copy(s,3,len1-3);
    for i:=1 to line do readln(ss[i]);
    if (line mod 2 <>0) then
    begin
      writeln(s3);
      dec(t);
      continue;
    end;
    for i:=1 to line do
    if not done[i] then
      begin
        now:=0;
        work(i);
        if f then break;
      end;
    if (not f) then
    begin
    str(max,ans);
    if (ans='0')and(o='1') then writeln(s1)
    else if (length(o)>1) then
      begin
        ci:=copy(o,3,length(o)-2);
        if ci=ans then writeln(s1);
        if ci<>ans then writeln(s2);
      end
    else writeln(s2);
    end;
    dec(t);
  end;
  close(input); close(output);
end.

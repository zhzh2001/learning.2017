#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("math.in");
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	minstd_rand gen(GetTickCount());
	for(;;)
	{
		uniform_int_distribution<> d(1,23333);
		int a=d(gen),b=d(gen);
		if(gcd(a,b)==1)
		{
			fout<<a<<' '<<b<<endl;
			break;
		}
	}
	return 0;
}

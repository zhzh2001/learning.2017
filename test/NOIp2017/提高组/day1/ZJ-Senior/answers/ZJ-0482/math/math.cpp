#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("math.in");
ofstream fout("math.out");
void exgcd(int a,int b,long long& x,long long& y)
{
	if(!b)
	{
		x=1;
		y=0;
	}
	else
	{
		exgcd(b,a%b,y,x);
		y-=a/b*x;
	}
}
int main()
{
	int a,b;
	fin>>a>>b;
	if(a>b)
		swap(a,b);
	long long x,y;
	exgcd(a,b,x,y);
	for(long long c=1ll*a*b;c>=0;c--)
	{
		long long k=-x*c/b;
		if(x*c+1ll*k*b<0)
			k++;
		long long ny=y*c-1ll*k*a;
		if(ny<0)
		{
			fout<<c<<endl;
			return 0;
		}
	}
	return 0;
}

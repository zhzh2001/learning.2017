#include<fstream>
#include<random>
#include<ctime>
using namespace std;
ofstream fout("park.in");
const int t=5,n=1e5,m=2e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<t<<endl;
	for(int T=1;T<=t;T++)
	{
		fout<<n<<' '<<m<<' '<<0<<' '<<2333333<<endl;
		for(int i=1;i<=m;i++)
		{
			uniform_int_distribution<> d(1,n),w(1,1000);
			fout<<d(gen)<<' '<<d(gen)<<' '<<w(gen)<<endl;
		}
	}
	return 0;
}

#include<fstream>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("park.in");
ofstream fout("park.out");
const int N=100005,M=200005,INF=1e9;
int n,m,k,p,head[N],v[M],w[M],nxt[M],e,d[N],f[N];
int rhead[N],rv[M],rw[M],rnxt[M],re,rd[N],ans;
bool vis[N];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
inline void radd_edge(int u,int v,int w)
{
	::rv[++re]=v;
	::rw[re]=w;
	rnxt[re]=rhead[u];
	rhead[u]=re;
}
void dfs(int u,int dist)
{
	if(dist+rd[u]>d[n]+k)
		return;
	if(u==n)
	{
		ans=(ans+1)%p;
		return;
	}
	for(int i=head[u];i;i=nxt[i])
		dfs(v[i],dist+w[i]);
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		fin>>n>>m>>k>>p;
		e=re=0;
		fill(head+1,head+n+1,0);
		fill(rhead+1,rhead+n+1,0);
		while(m--)
		{
			int u,v,w;
			fin>>u>>v>>w;
			add_edge(u,v,w);
			radd_edge(v,u,w);
		}
		typedef pair<int,int> state;
		fill(d+1,d+n+1,INF);
		d[1]=0;
		priority_queue<state,vector<state>,greater<state> > Q;
		Q.push(make_pair(0,1));
		fill(f+1,f+n+1,0);
		f[1]=1;
		fill(vis+1,vis+n+1,false);
		while(!Q.empty())
		{
			state k=Q.top();Q.pop();
			if(vis[k.second])
				continue;
			vis[k.second]=true;
			for(int i=head[k.second];i;i=nxt[i])
				if(d[k.second]+w[i]<d[v[i]])
				{
					f[v[i]]=f[k.second];
					Q.push(make_pair(d[v[i]]=d[k.second]+w[i],v[i]));
				}
				else if(d[k.second]+w[i]==d[v[i]])
					(f[v[i]]+=f[k.second])%=p;
		}
		if(k==0)
			fout<<f[n]<<endl;
		else
		{
			fill(rd+1,rd+n+1,INF);
			rd[n]=0;
			priority_queue<state,vector<state>,greater<state> > Q;
			Q.push(make_pair(0,n));
			fill(vis+1,vis+n+1,false);
			while(!Q.empty())
			{
				state k=Q.top();Q.pop();
				if(vis[k.second])
					continue;
				vis[k.second]=true;
				for(int i=rhead[k.second];i;i=rnxt[i])
					if(rd[k.second]+rw[i]<rd[rv[i]])
						Q.push(make_pair(rd[rv[i]]=rd[k.second]+rw[i],rv[i]));
			}
			ans=0;
			dfs(1,0);
			fout<<ans<<endl;
		}
	}
	return 0;
}

#include<fstream>
#include<string>
#include<cstring>
#include<sstream>
#include<algorithm>
#include<cmath>
using namespace std;
ifstream fin("complexity.in");
ofstream fout("complexity.out");
const int N=105,vn[]={1000000000,2000000000};
double s[N][2],res[2];
bool vis[26];
struct line
{
	int opt,l,r;
}prog[N];
int str2int(const string& s)
{
	stringstream ss(s);
	int x;
	ss>>x;
	return x;
}
double calc(const line& p,int n)
{
	int l=p.l,r=p.r;
	if(l==1000)
		l=n;
	if(r==1000)
		r=n;
	return max(r-l+1,0);
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int l;
		string comp;
		fin>>l>>comp;
		memset(vis,0,sizeof(vis));
		bool valid=true;
		string st;
		for(int i=1;i<=l;i++)
		{
			char opt;
			fin>>opt;
			if(opt=='F')
			{
				char var;
				string l,r;
				fin>>var>>l>>r;
				if(vis[var-'a'])
					valid=false;
				vis[var-'a']=true;
				st+=var;
				prog[i].opt=1;
				if(l=="n")
					prog[i].l=1000;
				else
					prog[i].l=str2int(l);
				if(r=="n")
					prog[i].r=1000;
				else
					prog[i].r=str2int(r);
			}
			else
			{
				if(st=="")
					valid=false;
				else
				{
					vis[st[st.length()-1]-'a']=false;
					st.erase(st.length()-1,1);
				}
				prog[i].opt=2;
			}
		}
		if(st!="")
			valid=false;
		if(!valid)
		{
			fout<<"ERR\n";
			continue;
		}
		for(int t=0;t<2;t++)
		{
			s[0][1]=.0;
			int sp=0;
			for(int i=1;i<=l;i++)
				if(prog[i].opt==1)
				{
					s[++sp][0]=calc(prog[i],vn[t]);
					s[sp][1]=.0;
				}
				else
				{
					if(s[sp][1]==.0)
						s[sp][1]=1.;
					s[sp-1][1]+=s[sp][0]*s[sp][1];
					sp--;
				}
			double th;
			if(comp=="O(1)")
				th=1.;
			else
				th=pow(vn[t],str2int(comp.substr(4,comp.length()-5)));
			if(s[0][1]==.0)
				s[0][1]=1.;
			res[t]=s[0][1]/th;
		}
		if(fabs(1-res[0]/res[1])<0.01)
			fout<<"Yes\n";
		else
			fout<<"No\n";
	}
	return 0;
}

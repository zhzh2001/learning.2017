#include<cstdio>
#include<algorithm>
#include<cstring>
//priority_queue
#define inf 0x3f3f3f3f
#define rep(i,n) for(int i=1;i<=n;++i)
#define ll long long
#define ge getchar()
using namespace std;
ll read(){
	ll x=0,f=1;char ch=ge;
	for(;ch<'0'&&ch>'9';ch=ge)if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=ge)x=x*10+ch-'0';
	return x*f;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a=read(),b=read();
	if(a>b)swap(a,b);
	printf("%lld\n",b*(a-1)-a);
	return 0;
}


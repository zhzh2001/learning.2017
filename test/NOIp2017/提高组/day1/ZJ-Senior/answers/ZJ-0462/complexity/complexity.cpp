#include<cstdio>
#include<algorithm>
#include<cstring>
//priority_queue
#define inf 0x3f3f3f3f
#define rep(i,n) for(int i=1;i<=n;++i)
#define ll long long
#define ge getchar()
using namespace std;
ll read(){
	ll x=0,f=1;char ch=ge;
	for(;ch<'0'&&ch>'9';ch=ge)if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=ge)x=x*10+ch-'0';
	return x*f;
}

char ch[105][50],ch1[10];
int st[505],cnt,sta[505],pos[505],en[505];
bool pd[1005];
int cal(int x,int y){
	int ans=0;
	for(int i=y;ch[x][i]>='0'&&ch[x][i]<='9';++i)
	ans=ans*10+ch[x][i]-'0';
	return ans;
}
int cal2(int x){
	int ans=0;
	for(int i=x;ch1[i]>='0'&&ch1[i]<='9';++i)
	ans=ans*10+ch1[i]-'0';
	return ans;
}
void check2(int an,int L){
	int stack=0;
	rep(i,L){
		scanf("%s",ch[i]);
		if(ch[i][0]=='F'){
			scanf("%s%s%s",ch[i]+1,ch[i]+2,ch[i]+7);
		}
	}
	rep(i,L){
		if(ch[i][0]=='F'){
			stack++;st[stack]=++cnt;sta[cnt]=i;pos[i]=cnt;
		}else if(!stack){puts("ERR");return;}
		else{
			en[st[stack]]=i;pos[i]=st[stack];
			stack--;
			
		}
	}
	if(stack){puts("ERR");return;}
	int now=0,ans=0;
	rep(i,L){
		if(ch[i][0]=='F'){
			if(pd[ch[i][1]]){puts("ERR");return;}
			pd[ch[i][1]]=1;
		}else pd[ch[sta[pos[i]]][1]]=0;
	}
	rep(i,L){
		if(ch[i][0]=='F'){
			if(pd[ch[i][1]]){puts("ERR");return;}
			pd[ch[i][1]]=1;
			int x=ch[i][2]=='n'?inf:cal(i,2)
				,y=ch[i][7]=='n'?inf:cal(i,7);
			if(x>y)pd[ch[i][1]]=0,i=en[pos[i]];
			else if(y==inf&&x!=inf)now++,ans=max(ans,now);
		}else{
			pd[ch[sta[pos[i]]][1]]=0;
			int x=ch[sta[pos[i]]][2]=='n'?inf:cal(sta[pos[i]],2)
				,y=ch[sta[pos[i]]][7]=='n'?inf:cal(sta[pos[i]],7);
			if(y==inf&&x!=inf)--now;
		}
	}
	puts(ans==an?"Yes":"No");
}
void check1(int L){
	int stack=0;
	rep(i,L){
		scanf("%s",ch[i]);
		if(ch[i][0]=='F'){
			scanf("%s%s%s",ch[i]+1,ch[i]+2,ch[i]+7);
		}
	}
	
	rep(i,L){
		if(ch[i][0]=='F'){
			stack++;st[stack]=++cnt;sta[cnt]=i;pos[i]=cnt;
		}else if(!stack){puts("ERR");return;}
		else{
			en[st[stack]]=i;pos[i]=st[stack];
			stack--;
			
		}
	}
	
	if(stack){puts("ERR");return;}
	rep(i,L){
		if(ch[i][0]=='F'){
			if(pd[ch[i][1]]){puts("ERR");return;}
			pd[ch[i][1]]=1;
		}else pd[ch[sta[pos[i]]][1]]=0;
	}
	rep(i,L){
		if(ch[i][0]=='F'){
			if(pd[ch[i][1]]){puts("ERR");return;}
			pd[ch[i][1]]=1;
			int x=ch[i][2]=='n'?inf:cal(i,2)
				,y=ch[i][7]=='n'?inf:cal(i,y);
			if(x>y)pd[ch[i][1]]=0,i=en[pos[i]];
			else if(y==inf&&x!=inf){puts("No");return;}
		}else{
			pd[ch[sta[pos[i]]][1]]=0;
		}
	}
	puts("Yes");
}
void clear(){

	memset(ch1,0,sizeof(ch1));
	memset(st,0,sizeof(st));
	memset(sta,0,sizeof(sta));
	memset(pos,0,sizeof(pos));
	memset(en,0,sizeof(en));
	memset(pd,0,sizeof(pd));
	memset(ch,0,sizeof(ch));
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	for(;T--;){
		if(T==5){
			int i;
			i++;
		}
		clear();
		int n;
		scanf("%d%s",&n,ch1);
		if(ch1[2]=='1'){check1(n);continue;}
		check2(cal2(4),n);
	}
	return 0;
}


#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
using namespace std;
int main()
{
	int t;
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>t;
	while(t--)
	{
		int l=0,len=0,fu=0,ci=0,fc=1,fcm=1,f[3000];
	    string st="";
		bool a[3000],ok=0,er=0,fwx[3000],fi[3000];
		memset(a,0,sizeof a);memset(fwx,0,sizeof fwx);memset(f,0,sizeof f);
		cin>>l;
		cin>>st;
		for(int i=0;i<11;i++)
		{
			if(st[i]=='(')ok=1;
			if(ok)
			{
			   if(st[i+1]=='1')fu=1;
			   else
			   {
			   	  i=i+3;int y=0;
				  while(st[i]>='0'&&st[i]<='9')y=y*10+st[i]-'0',i++;
				  fu=y+1;
			   }
			   break;
			}
		}
		while(l--)
		{
			cin>>st;char o;
			if(st[0]=='F')
			{
				ci++;cin>>st;
				if(a[st[0]-'a'])er=1;
				int i=0,x=0,y=0;f[ci]=st[0];a[st[0]-'a']=1;
				cin>>st;
				while(st[i]<='9'&&st[i]>'0')x=x*10+(st[i]-'0'),i++;
	            cin>>st;i=0;
				while(st[i]<='9'&&st[i]>'0')y=y*10+(st[i]-'0'),i++;
				if(!x&&y)fwx[ci]=1;
				if(x>y&&x&&y)fwx[ci]=1;
				bool wx=0;
				for(int j=1;j<=ci;j++)
				 if(fwx[j]){wx=1;break;}
				if(fc-1<ci&&!y&&!wx&&x)fc++,fcm=max(fcm,fc),fi[ci]=1;
			}
			else
			{
				if(fwx[ci]==1)fwx[ci]=0;
				if(fi[ci])fc--,fi[ci]=0;
				a[f[ci]-'a']=0;f[ci--]=0;
				if(ci<0)er=1;
			}
		}
		if(ci<0||ci>0||er)cout<<"ERR"<<endl;
		else if(fcm!=fu)cout<<"No"<<endl;
		else cout<<"Yes"<<endl;
	}
	return 0;
}

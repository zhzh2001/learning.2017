/*
complexity
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<cctype>
using namespace std;
template<typename T>
void read(T& w)
{
	char r;int f=1;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')r=getchar(),f=-1;
	for(w=0;r>=48&&r<=57;r=getchar())w=w*10+r-48;
	w*=f;
}
const int maxn=103;
int T,line;
bool n;
int p;
struct EDGE
{
	int to,nxt;
	void init(int too,int nxtt)
	{
		to=too,nxt=nxtt;
	}
}edge[maxn];
int ek;
int head[maxn];
void addEdge(int k,int v)
{
	edge[++ek].init(v,head[k]);
	head[k]=ek;
}
const int asn=10000000;
struct COM
{
	int p;//v=0,1,asn,p=int
	void init(int x,int y)
	{
		if(x<asn&&y==asn)p=1;
		else p=0;
	}
};
bool operator<(const COM& a,const COM& b)
{
	return a.p<b.p;
}
bool operator==(const COM& a,const COM& b)
{
	return a.p==b.p;
}
COM operator*(const COM& a,const COM& b)
{
	COM ans;
	ans.p=a.p+b.p;
	return ans;
}
struct NODE
{
	int x,y;
	COM com;
	char var;
	void getcom()
	{
		com.init(x,y);
	}
}node[maxn];
int nk=0;
char code[maxn];
int stk[maxn],sk=0;
int start[maxn],endd[maxn];
int reid[maxn];
COM in;
bool ERR;
void Init()
{
	ERR=false;
	ek=0;nk=0;
	n=false;p=0;
	for(int i=0;i<=line;++i)
	{
		head[i]=0;
		reid[i]=0;
		start[i]=0;
		endd[i]=0;
	}
	sk=0;
}

void input()
{
	read(line);
	Init();
	char r;
	do{r=getchar();}
	while(r!='^'&&!isdigit(r));
	if(r=='^')read(in.p);
	else in.p=0;
	for(int i=1;i<=line;++i)
	{
		do{code[i]=getchar();}
		while(code[i]!='F'&&code[i]!='E');
		if(code[i]=='F')
		{
			char r;
			int id=++nk;
			start[id]=i;
			stk[++sk]=id;
			reid[i]=id;
			do{r=getchar();}
			while(!islower(r));
			node[id].var=r;
			do{r=getchar();}
			while(!isdigit(r)&&r!='n');
			if(r=='n')	node[id].x=asn;
			else 
			{
				node[id].x=r-48;
				for(r=getchar();isdigit(r);r=getchar())node[id].x=node[id].x*10+r-48;
			}
			do{r=getchar();}
			while(!isdigit(r)&&r!='n');
			if(r=='n')	node[id].y=asn;
			else 
			{
				node[id].y=r-48;
				for(r=getchar();isdigit(r);r=getchar())node[id].y=node[id].y*10+r-48;
			}
			node[id].getcom();
		}
		else if(code[i]=='E')
		{
			endd[stk[sk--]]=i;
		}
	}
	if(sk!=0)//�﷨����1 
	{
		ERR=true;
	}
}
void dfs(int k)
{
	int l=start[k]+1,r=endd[k]-1;
	for(int i=l;i<=r;++i)
	{
		if(code[i]=='F')
		{
			int v=reid[i];
			addEdge(k,v);
			dfs(v);
			i=endd[v];
		}
	}
}
bool used[200];
void judge(int k)
{
	if(used[node[k].var])
	{
		ERR=true;
		return;
	}
	used[node[k].var]=true;
	COM soncom;
	soncom.p=0;
	for(int i=head[k];i&&!ERR;i=edge[i].nxt)
	{
		int v=edge[i].to;
		judge(v);
		if(node[k].x<=node[k].y)
		{
			if(soncom<node[v].com)
			soncom=node[v].com;
		}
	}
	node[k].com=node[k].com*soncom;
	used[node[k].var]=false;
}
void solve()
{
	if(ERR)
	{
		puts("ERR");
		return;
	}
	memset(used,0,sizeof(used));
	node[0].com.p=0;
	start[0]=0;
	endd[0]=line+1;
	dfs(0);
	judge(0);
	if(ERR)
	{
		puts("ERR");
		return;
	}
	if(in==node[0].com)
		puts("Yes");
	else puts("No");	
}
int main()
{
		freopen("complexity.in","r",stdin);
		freopen("complexity.out","w",stdout);
	for(read(T);T;--T)
	{
		input();
		solve();
	}
	return 0;
}

/*
math
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
using namespace std;
template<typename T>
void read(T& w)
{
	char r;int f=1;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')r=getchar(),f=-1;
	for(w=0;r>=48&&r<=57;r=getchar())w=w*10+r-48;
	w*=f;
}
template<typename T>
void write(T w)
{
	if(w<0)putchar('-'),w=-w;
	if(w>9)write(w/10);
	putchar(w%10+48);
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
void Swap(T& a,T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef long long lint;
typedef unsigned uint;
const lint linf=9223372036854775807ll;
const uint uinf=4294967295u;
const int maxn=1000000003;
lint a,b;
lint limit;
int main()
{
		freopen("math.in","r",stdin);
		freopen("math.out","w",stdout);
	read(a),read(b);// if a,b blong N, a*b-(a+b)+1 is ok
	//cout<<a<<' '<<b<<endl;
	limit=a*b-(a+b);
	if(a<=7000)
	{
		for(lint i=limit;i>=1;--i)
		{
			bool ok=false;
			for(int x=0;x<=i&&!ok;++x)
			{
				for(int y=0;y<=i&&a*x+b*y<=i&&!ok;++y)
				{
					if(a*x+b*y==i)
					{
						ok=true;
					}
				}
			}
			if(!ok)
			{
				printf("%lld\n",i);
				return 0;
			}
		}
	}
	else printf("%lld\n",limit);
	return 0;
}

/*
park
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
using namespace std;
template<typename T>
void read(T& w)
{
	char r;int f=1;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')r=getchar(),f=-1;
	for(w=0;r>=48&&r<=57;r=getchar())w=w*10+r-48;
	w*=f;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
void Swap(T& a,T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef long long lint;
typedef unsigned uint;
const lint linf=9223372036854775807ll;
const uint uinf=4294967295u;
const int maxn=100003;
const int maxm=maxn*2;
int T,n,m,kk,p;
struct EDGE
{
	int to,nxt,w;
	void init(int too,int nxtt,int ww)
	{
		to=too,nxt=nxtt,w=ww;
	}
}edge[maxn],redge[maxn],edge0[maxn];
int ek,rek,ek0;
int head[maxn],rhead[maxn];
int head0[maxn];
void addEdge(int k,int v,int w)
{
	edge[++ek].init(v,head[k],w);
	head[k]=ek;
}
void addrEdge(int k,int v,int w)
{
	redge[++rek].init(v,rhead[k],w);
	rhead[k]=rek;
}
void addEdge0(int k,int v,int w)
{
	edge0[++ek0].init(v,head0[k],w);
	head0[k]=ek0;
}
void Init()
{
	ek=rek=ek0=0;
	for(int i=1;i<=n;++i)
	{
		head0[i]=rhead[i]=head[i]=0;
	}
}
void input()
{
	read(n),read(m),read(kk),read(p);
	Init();
	int a,b,c;
	for(int i=1;i<=m;++i)
	{
		read(a),read(b),read(c);
		addEdge(a,b,c);
		addrEdge(b,a,c);//����� 
		if(c==0)
			addEdge0(a,b,c);
	}
}
bool zero;
template<typename T,int size>
struct STK
{
	T ary[size];
	int f;
	void clear()
	{
		f=0;
	}
	bool empty()
	{
		return f==0;
	}
	void push(const T& w)
	{
		ary[++f]=w;
	}
	void pop()
	{
		--f;
	}
	T top()
	{
		return ary[f];
	}
};
STK<int,maxn> stk;
int low[maxn],dfn[maxn],tid;
bool in[maxn];
void Tarjan(int k)
{
	low[k]=dfn[k]=++tid;
	in[k]=true;
	stk.push(k);
	for(int i=head0[k];i;i=edge0[i].nxt)
	{
		int v=edge0[i].to;
		if(in[v]&&dfn[v]<low[k])
			low[k]=dfn[v];
		else 
		{
			Tarjan(v);
			if(low[v]<low[k])
			{
				low[k]=low[v];
			}
		}
	}
	if(dfn[k]==low[k])
	{
		if(stk.top()!=k)
		{
			zero=true;
			return;
		}
		else stk.pop(),in[k]=false;
	}
}
void find0()
{
	tid=0;
	stk.clear();
	for(int i=1;i<=n;++i)
	{
		low[i]=dfn[i]=0;
		in[i]=false;
	}
	zero=false;
	Tarjan(1);
}
template<typename T,int size>
struct QUE
{
	T ary[size+1];
	int f,t,cnt;
	void clear()
	{
		f=t=cnt=0;
	}
	bool empty()
	{
		return cnt==0;
	}
	void push(const T& w)
	{
		ary[t++]=w;
		if(t==size)t=0;
		++cnt;
	}
	void pop()
	{
		++f,--cnt;
		if(f==size)f=0;
	}
	T front()
	{
		return ary[f];
	}
};

QUE<int,maxn> q;
int d[maxn];
void SPFA()
{
	q.clear();
	for(int i=1;i<=n;++i)
	{
		d[i]=inf;
		in[i]=false;
	}
	d[1]=0;
	in[1]=true;
	q.push(1);
	while(!q.empty())
	{
		int ff=q.front();q.pop();
		in[ff]=false;
		for(int i=head[ff];i;i=edge[i].nxt)
		{
			int v=edge[i].to,ww=edge[i].w;
			if(d[ff]!=inf&&d[ff]+ww<d[v])
			{
				d[v]=d[ff]+ww;
				if(!in[v])q.push(v);
			}
		}
	}
}


int dfs(int k,int rest)
{
	int sum=0;
	if(k==1)sum=1;
	for(int i=rhead[k];i;i=redge[i].nxt)
	{
		int v=redge[i].to;
		int ww=redge[i].w;
		if(rest>=ww)
		{
			int tans=dfs(v,rest-ww);
			sum+=tans;
			sum%=p;
		}
	}
	return sum;
}
int ans;
void solve()
{
	find0();
	if(zero)
		ans=-1;
	else
	{
		SPFA();
		ans=dfs(n,kk+d[n]);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(read(T);T;--T)
	{
		input();
		solve();
		printf("%d\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
using namespace std;
const int maxn=100005;
void read(int &x)
{
	x=0;
	char ch=getchar();
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
}
struct edge
{
	int to,w;
};
int t,n,m,k,p,as;
vector<edge> map[maxn];
int dis[maxn],pre[maxn];
bool vis[maxn];
int visd[maxn];
bool end,att;
void dij()
{
	int min,i;
	dis[1]=0;
	while(!vis[n])
	{
		min=0;
		for(i=1;i<=n;i++)
		{
			if(dis[i]<dis[min]&&!vis[i])
			{
				min=i;
			}
		}
		vis[min]=1;
		for(i=0;i<map[min].size();i++)
		{
			int y=map[min][i].to,wid=map[min][i].w;
			if(dis[y]>dis[min]+wid)
			{
				dis[y]=dis[min]+wid;
			}
		}
	}
}
bool dfs(int c)
{
	if(end) return 1;
	int y;
	register int i,j;
	if(c==n)
	{
		int s=0,tp=0;
		i=c;
		while(pre[i]!=0)
		{
			tp++;
			if(tp>=n)
			{
				end=1;break;
			}
			for(j=0;j<map[pre[i]].size();j++)
			{
				if(map[pre[i]][j].to==i)
				{
					s+=map[pre[i]][j].w;
					break;
				}
			}
			i=pre[i];
		}
		if(s<=dis[n]+k) as++;
		if(att==1) goto l1;
	}
	else
	{
		l1:;
		for(i=0;i<map[c].size();i++)
		{
			y=map[c][i].to;
			if(visd[y]<=n)
			{
				visd[y]++;
				pre[y]=c;
				dfs(y);
			}
			else
			{
				end=dfs(y);
			}
		}
	}
	
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	register int i,j;
	int a,b,c;
	read(t);
	for(i=1;i<=t;i++)
	{
		memset(dis,0x7f,sizeof(dis));
		memset(visd,0,sizeof(visd));
		memset(vis,0,sizeof(vis));
		memset(pre,0,sizeof(pre));
		as=0;end=0;att=0;
		read(n);read(m);read(k);read(p);
		for(j=1;j<=n;j++)
		{
			map[j]=vector<edge>();
		}
		for(j=1;j<=m;j++)
		{
			read(a);read(b);read(c);
			if(c==0) att=1;
			map[a].push_back((edge){b,c});
		}
		dij();
		dfs(1);
		if(end) printf("-1\n");
		else printf("%d\n",as);
	}
}

#include <bits/stdc++.h>
using namespace std;

// FXXK_IO BGING HERE
inline char get_c()
{
	char c;
	while(isspace(c=getchar()));
	return c;
}

inline unsigned get_u()
{
	char c; unsigned r=0;
	while(isspace(c=getchar()));
	for(;isdigit(c);c=getchar())
	{
		r=(r<<3)+(r<<1)+c-'0';
	}
	return r;
}

inline unsigned get_all(const int&s)
{
	unsigned r=s;
	for(char c=getchar();isdigit(c);c=getchar())
	{
		r=(r<<3)+(r<<1)+c-'0';
	}
	return r;
}
// FXXK_IO END HERE

static const int INF=0x7FFFFFFF;

char s[10],var[200];
bool usd[50],valid[200],loop[200];
int value[200];

int main()
{
	freopen("complexity.in" ,"r",stdin );
	freopen("complexity.out","w",stdout);
	for(int T=get_u();T--;)
	{
		int N=get_u(),output=0,top=0; bool error=0;
		scanf("%s",s);
		if(char *idx=strstr(s,"^"))
		{
			for(++idx;isdigit(*idx);++idx)
			{
				output=(output<<3)+(output<<1)+*idx-'0';
			}
		}
		memset(usd,0,sizeof usd);
		memset(var,0,sizeof var);
		memset(loop,0,sizeof loop);
		memset(valid,1,sizeof valid);
		memset(value,0,sizeof value);
		while(N--)
		{
			if(get_c()=='F')
			{
				char c=get_c(); int fr,to;
				var[++top]=c;
				usd[c-'a']?error=1:usd[c-'a']=1;
				if(isdigit(c=get_c())) fr=get_all(c-'0'); else fr=INF;
				if(isdigit(c=get_c())) to=get_all(c-'0'); else to=INF;
				valid[top]=valid[top-1]&&fr<=to;
				value[top]=loop[top]=fr<INF&&to==INF;
			}
			else top?(value[top-1]=max(value[top-1],valid[top]*value[top]+loop[top-1]),usd[var[top--]-'a']=0):error=1;
		}
		if(error||top) {puts("ERR"); continue;}
		puts(value[0]==output?"Yes":"No");
	}
	return 0;
}
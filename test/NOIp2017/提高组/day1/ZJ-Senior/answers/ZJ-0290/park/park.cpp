#include <bits/stdc++.h>
#define forin(i,p) for(typeof(p.end()) __ITER__=p.begin(),__END__=p.end();__ITER__!=__END__;++__ITER__) if(bool __FLAG__=1) for(typeof(*__ITER__) i=*__ITER__;__FLAG__;__FLAG__=0)
using namespace std;

// QUICK_IO BEGIN HERE
inline unsigned get_u()
{
	char c; unsigned r=0;
	while(isspace(c=getchar()));
	for(;isdigit(c);c=getchar())
	{
		r=(r<<3)+(r<<1)+c-'0';
	}
	return r;
}
// QUICK_IO END HERE

static int N,M,K,P;
inline int&inc(int&x,const int&y) {return (x+=y)<P?x:x-=P;}

vector< pair<int,int> > edg[100005];

namespace init
{
	int dis[100005];
	multiset< pair<int,int> > que;
	inline void Dijkstra()
	{
		memset(dis,0x3F,sizeof dis),dis[1]=0;
		que.clear();
		forin(i,edg[1])
		{
			dis[i.first]=i.second;
			que.insert(make_pair(i.second,i.first));
		}
		while(!que.empty())
		{
			int x=que.begin()->second;
			que.erase(que.begin());
			forin(i,edg[x])
			{
				if(dis[x]+i.second<dis[i.first])
				{
					multiset< pair<int,int> >::iterator las=que.find(make_pair(dis[i.first],i.first));
					if(las!=que.end()) que.erase(las);
					dis[i.first]=dis[x]+i.second;
					que.insert(make_pair(dis[i.first],i.first));
				}
			}
		}
	}
}

namespace PartI // Point 1, 2, 7
{
	using init::dis;
	using init::Dijkstra;
	queue<int> que;
	int f[100005];
	void solve();
}

void PartI::solve()
{
	Dijkstra();
	f[1]=1;
	for(que.push(1);!que.empty();que.pop())
	{
		int x=que.front();
		forin(y,edg[x])
		{
			if(dis[y.first]==dis[x]+y.second) inc(f[y.first],f[x]),que.push(y.first);
		}
	}
	printf("%d\n",f[N]);
}

int main()
{
	freopen("park.in" ,"r",stdin );
	freopen("park.out","w",stdout);
	for(int T=get_u();T--;)
	{
		N=get_u(),M=get_u(),K=get_u(),P=get_u();
		memset(edg,0,sizeof edg);
		while(M--)
		{
			int u=get_u(),v=get_u(),w=get_u();
			edg[u].push_back(make_pair(v,w));
			edg[v].push_back(make_pair(u,w));
		}
		if(!K) {PartI::solve(); continue;}
		puts("-1");
	}
	return 0;
}
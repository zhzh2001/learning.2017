#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstring>
using namespace std;
int T,n,w1,l1,top1,stackn[1000],w2;
char s1[15],s2[10],s3[10],stackc[1000];
bool used[1000],flag,stackf[1000];
bool ok(){
	int L2,L3;
	L2=strlen(s2);
	L3=strlen(s3);
	if(L2<L3)
		return true;
	if(L2>L3)
		return false;
	for(int i=0;i<L2;i++){
		if(s2[i]<s3[i])
			return true;
		if(s2[i]>s3[i])
			return false;
	}
	return true;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%s",&n,s1);
		memset(used,0,sizeof(used));
		w2=0;
		l1=strlen(s1);
		if(l1==4)
			w1=0;
		else{
			w1=0;
			for(int i=0;i<l1;i++)
				if(s1[i]<='9'&&s1[i]>='0')
					w1=w1*10+s1[i]-'0';
		}
		top1=0;
		flag=true;
		stackn[0]=0;
		stackf[0]=true;
		for(int i=1;i<=n;i++){
			scanf("%s",s2);
			if(s2[0]=='E'){
				if(top1==0)
					flag=false;
				if(!flag)
					continue;
				used[stackc[top1]-'a']=false;
				top1--;
			}
			else{
				scanf("%s",s2);
				if(used[s2[0]-'a'])
					flag=false;
				else
					used[s2[0]-'a']=true;
				stackc[top1+1]=s2[0];
				scanf("%s%s",s2,s3);
				if(!flag)
					continue;
				if(s2[0]=='n'){
					if(s3[0]=='n'){
						if(stackf[top1]==true){
							stackn[top1+1]=stackn[top1];
							stackf[top1+1]=true;
						}
						else{
							stackn[top1+1]=0;
							stackf[top1+1]=false;
						}
					}
					else{
						stackn[top1+1]=0;
						stackf[top1+1]=false;
					}
				}
				else{
					if(s3[0]=='n'){
						if(stackf[top1]==true){
							stackn[top1+1]=stackn[top1]+1;
							stackf[top1+1]=true;
						}
						else{
							stackn[top1+1]=0;
							stackf[top1+1]=false;
						}
					}
					else{
						if(ok()){
							if(stackf[top1]==true){
								stackn[top1+1]=stackn[top1];
								stackf[top1+1]=true;
							}
							else{
								stackn[top1+1]=0;
								stackf[top1+1]=false;
							}
						}
						else{
							stackn[top1+1]=0;
							stackf[top1+1]=false;
						}
					}
				}
				top1++;
				w2=max(w2,stackn[top1]);
			}
		}
		if(top1>0)
			flag=false;
		if(!flag)
			printf("ERR\n");
		else
		if(w2==w1)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}

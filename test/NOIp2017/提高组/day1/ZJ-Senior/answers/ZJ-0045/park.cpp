#include<stdio.h>
#include<queue>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<cstring>
using namespace std;
const int INF=1000000000;
int de[100005],ans,f[100005][55],T,Next[200005],vet[200005],head[100005],en,cost[200005],dis[100005],n,m,k,p,num;
int dfn[100005],Next1[200005],vet1[200005],head1[100005],en1,que[100005],qhead,qtail,ti;
bool vis[100005];
int mo(int x){
	if(x>=p)
		return x-p;
	return x;
}
void addedge(int u,int v,int val){
	Next[++en]=head[u];
	head[u]=en;
	vet[en]=v;
	cost[en]=val;
}
void addedge1(int u,int v){
	Next1[++en1]=head1[u];
	head1[u]=en1;
	vet1[en1]=v;
	de[v]++;
}
struct czy{
	int dis,adddis,u,DFN;
}a[5000005];
bool cmp(czy x,czy y){
	return (x.dis<y.dis)||(x.dis==y.dis&&x.DFN<y.DFN);
}
priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > > Q;
void dijkstra(){
	for(int i=1;i<=n;i++)
		dis[i]=INF;
	memset(vis,0,sizeof(vis));
	dis[1]=0;
	while(!Q.empty())
		Q.pop();
	Q.push(make_pair(0,1));
	while(!Q.empty()){
		int u=Q.top().second;
		Q.pop();
		if(vis[u])
			continue;
		vis[u]=true;
		for(int i=head[u];i;i=Next[i]){
			int v=vet[i];
			if(dis[u]+cost[i]<dis[v]){
				dis[v]=dis[u]+cost[i];
				Q.push(make_pair(dis[v],v));
			}
		}
	}
}
void work(){
	qhead=1;
	qtail=0;
	ti=0;
	for(int i=1;i<=n;i++)
		if(de[i]==0)
			que[++qtail]=i;
	while(qhead<=qtail){
		int u=que[qhead++];
		dfn[u]=++ti;
		for(int i=head1[u];i;i=Next1[i]){
			int v=vet1[i];
			de[v]--;
			if(de[v]==0)
				que[++qtail]=v;
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(Next,0,sizeof(Next));
		memset(head,0,sizeof(head));
		en=0;
		ans=0;
		memset(f,0,sizeof(f));
		memset(Next1,0,sizeof(Next1));
		memset(head1,0,sizeof(head1));
		memset(de,0,sizeof(de));
		en1=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=m;i++){
			int aa,bb,cc;
			scanf("%d%d%d",&aa,&bb,&cc);
			addedge(aa,bb,cc);
			if(cc==0)
				addedge1(aa,bb);
		}
		work();
		if(qtail<n){
			printf("-1\n");
			continue;
		}
		dijkstra();
		num=0;
		for(int i=1;i<=n;i++)
			for(int j=0;j<=k;j++){
				num++;
				a[num].dis=dis[i]+j;
				a[num].adddis=j;
				a[num].u=i;
				a[num].DFN=dfn[i];
			}
		sort(a+1,a+1+num,cmp);
		f[1][0]=1;
		for(int i=1;i<=num;i++){
			int u=a[i].u;
			if(u==n)
				ans=mo(ans+f[u][a[i].adddis]);
			for(int e=head[u];e;e=Next[e]){
				int v=vet[e],Dis=a[i].dis+cost[e];
				if(Dis<=dis[v]+k)
					f[v][Dis-dis[v]]=mo(f[v][Dis-dis[v]]+f[u][a[i].adddis]);
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}

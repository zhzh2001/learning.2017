#include <cstdio>
#include <algorithm> 
using std::swap;

#include <cctype>
namespace FastIO {
  char c;
  bool positive;
  template <class T_1>
  inline void scan(T_1 &x) {
    x = 0;
    positive = 0;
    while (!isdigit(c = getchar()))
      positive = (c == '-');
    do
      x = x * 10 + (c & 0xf);
    while (isdigit(c = getchar()));
    if (positive)
      x *= -1;
  }
  template <class T_1, class T_2>
  inline void scan(T_1 &x, T_2 &y) {
    scan(x); scan(y);
  }
}  // namespace FastIO
using FastIO::scan;

typedef long long LL;

int x, y;
LL ans;

int main() {
  freopen("math.in", "r", stdin);
  freopen("math.out", "w", stdout);
  scan(x, y);
  if (x > y)
    swap(x, y);
  ans = 1ll * (y - 2) * (x - 1) + x - 2;
  printf("%lld\n", ans);
  return 0;
}

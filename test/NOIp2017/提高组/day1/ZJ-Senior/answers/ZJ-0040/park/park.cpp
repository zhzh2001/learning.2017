#include <deque>
#include <cstdio>
#include <cstring>
using std::deque;

#include <cctype>
namespace FastIO {
  char c;
  template <class T_1>
  inline void scan(T_1 &x) {
    x = 0;
    while (!isdigit(c = getchar())) { }
    do
      x = x * 10 + (c & 0xf);
    while (isdigit(c = getchar()));
  }
  template <class T_1, class T_2>
  inline void scan(T_1 &x, T_2 &y) {
    scan(x); scan(y);
  }
  template <class T_1, class T_2, class T_3>
  inline void scan(T_1 &a, T_2 &b, T_3 &c) {
    scan(a); scan(b); scan(c);
  }
  template <class T_1, class T_2, class T_3, class T_4>
  inline void scan(T_1 &a, T_2 &b, T_3 &c, T_4 &d) {
    scan(a); scan(b); scan(c); scan(d);
  }
}  // namespace FastIO
using FastIO::scan;

typedef long long LL;
const int kMaxK = 50 + 5;
const int kMaxN = 1e3 + 5;
const int kMaxM = 2e3 + 5;

int T;
int n, m, k, p;

int G[kMaxN], dst[kMaxM], val[kMaxM], nxt[kMaxM], G_tot;
void AddEdge(const int &u, const int &v, const int &w) {
  dst[++G_tot] = v;
  val[G_tot] = w;
  nxt[G_tot] = G[u];
  G[u] = G_tot;
}

deque<int> q;
int dis[kMaxN];
bool in_q[kMaxN];
void SPFA() {
  dis[1] = 0;
  in_q[1] = 1;
  q.push_back(1);
  while (!q.empty()) {
    const int u = q.front();
    q.pop_front(); in_q[u] = 0;
    for (int i = G[u]; i; i = nxt[i]) {
      const int v = dst[i];
      if (dis[v] > dis[u] + val[i]) {
        dis[v] = dis[u] + val[i];
        if (!in_q[v]) {
          in_q[v] = 1;
          if (!q.empty() && dis[v] < dis[q.front()])
            q.push_front(v);
          else
            q.push_back(v);
        }
      }
    }
  }
}

int f[kMaxN][kMaxN][kMaxK];
bool Update(const int &u, const int &v, const int &w) {
  bool ret = 0;
  for (int i = 0; i + w <= k; i++) {
    if (f[u][0][i] != f[v][u][i + w]) {
      ret = 1;
      (f[v][0][i + w] += ((f[u][0][i] - f[v][u][i + w]) % p + p) % p) %= p;
      f[v][u][i + w] = f[u][0][i];
    }
  }
  return ret;
}

void GetAns() {
  f[1][0][0] = 1;
  in_q[1] = 1; q.push_back(1);
  while (!q.empty()) {
    const int u = q.front();
    q.pop_front(); in_q[u] = 0;
    for (int i = G[u]; i; i = nxt[i]) {
      const int v = dst[i];
      if (Update(u, v, dis[u] + val[i] - dis[v])) {
        if (!in_q[v]) {
          in_q[v] = 1;
          if (!q.empty() && dis[v] < dis[q.front()])
            q.push_front(v);
          else
            q.push_back(v);
        }
      }
    }
  }
}

void Init() {
  G_tot = 0;
  memset(G, 0, sizeof G);
  memset(f, 0, sizeof f);
  memset(in_q, 0, sizeof in_q);
  memset(dis, 0x3f, sizeof dis);
}

int main() {
  freopen("park.in", "r", stdin);
  freopen("park.out", "w", stdout);
  scan(T);
  while (T--) {
    Init();
    scan(n, m, k, p);
    for (int i = 1, u, v, w; i <= m; i++) {
      scan(u, v, w);
      AddEdge(u, v, w);
    }
    SPFA();
    GetAns();
    int ans = 0;
    for (int i = 0; i <= k; i++)
      (ans += f[n][0][i]) %= p;
    printf("%d\n", ans);
  }
  return 0;
}

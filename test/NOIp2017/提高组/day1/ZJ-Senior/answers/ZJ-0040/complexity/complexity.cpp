#include <cstdio>
#include <cstring>

#include <cctype>
namespace FastIO {
  char c;
  bool positive;
  template <class T_1>
  inline void scan(T_1 &x) {
    x = 0;
    positive = 0;
    while (!isdigit(c = getchar()))
      positive = (c == '-');
    do
      x = x * 10 + (c & 0xf);
    while (isdigit(c = getchar()));
    if (positive)
      x *= -1;
  }
  inline void scan(char &x) {
    while (!isprint(x = getchar())) { }
  }
  inline void scan(char *s) {
    int i = 0;
    while ((c = getchar()) != '\n')
      s[i++] = c;
  }
  template <class T_1, class T_2>
  inline void scan(T_1 &x, T_2 &y) {
    scan(x); scan(y);
  }
}  // namespace FastIO
using FastIO::scan;

typedef long long LL;

LL scan(char *s, int &i) {
  LL x = 0;
  bool positive = 0;
  while (!isdigit(s[i]))
    positive = (s[i++] == '-');
  do
    x = x * 10 + (s[i++] & 0xf);
  while (isdigit(s[i]));
  if (positive)
    x *= -1;  
  return x;
}

inline int max(const int &x, const int &y) {
  return x > y ? x : y;
}

int T;
char input, ope[50];
LL L, R;
int n, cnt;
int target;
int ans, now_ans;
bool vis[30], exist, work;

int main() {
  freopen("complexity.in", "r", stdin);
  freopen("complexity.out", "w", stdout);
  scan(T);
  while (T--) {
    scan(n);
    scan(input, input);
    scan(input);
    if (input == '1') {
      target = 0;
      scan(input);
    } else {
      scan(input, target);
    }
    exist = work = true;
    now_ans = ans = cnt = 0;
    memset(vis, 0, sizeof vis);
    input = getchar();
    for (int i = 1, j; i <= n; i++) {
      j = 0;
      memset(ope, 0, sizeof ope);
      scan(ope);
      if (ope[j++] == 'E') {
        cnt--;
        if (cnt == 0) {
          ans = max(ans, now_ans);
          work = 1;
          now_ans = 0;
          memset(vis, 0, sizeof vis);
        } else if (cnt < 0) {
          exist = false;
        }
      } else {
        cnt++;
        j++;
        if (vis[ope[j] - 'a'])
          exist = 0;
        else
          vis[ope[j] - 'a'] = 1;
        j += 2;
        if (ope[j] == 'n')
          L = -1;
        else
          L = scan(ope, j);
        j++;
        if (ope[j] == 'n')
          R = -1;
        else
          R = scan(ope, j);
        if ((L == -1 && R != -1) || (R != -1 && L > R))
          work = 0;
        else if (R == -1 && work && L != -1)
          now_ans++;
      }
    }
    exist &= (cnt == 0);
    if (exist)
      puts(ans == target ? "Yes" : "No");
    else
      puts("ERR");
  }
  return 0;
}


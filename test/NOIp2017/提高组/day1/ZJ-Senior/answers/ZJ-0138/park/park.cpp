#include<cstdio>
#include<algorithm>
#define inf 1000000000
using namespace std;
inline int read(){
	char c=getchar();int ret=0;
	while((c<'0')||(c>'9'))c=getchar();
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret;
}
struct heap{
	int top;
	int h[100005],p[100005],n[100005];
	void clear(int s){
		for(int i=1;i<=s;i++)p[i]=n[i]=i,h[i]=inf;
		h[1]=0;top=s;
	}
	void Clear(int s){
		for(int i=1;i<=s;i++)p[i]=n[i]=s+1-i,h[i]=inf;
		h[1]=0;top=s;
	}
	void pushup(int k){
		while(k>1){
			if(h[k]<h[k>>1]){
				swap(n[k],n[k>>1]);
				swap(p[n[k]],p[n[k>>1]]);
				swap(h[k],h[k>>1]);
			}else break;
			k>>=1;
		}
	}
	void pushdown(int k){
		while((k<<1)<=top){
			int u=k<<1;
			if(u<top)if(h[u]>h[u+1])u++;
			if(h[k]>h[u]){
				swap(n[k],n[u]);
				swap(p[n[k]],p[n[u]]);
				swap(h[k],h[u]);
			}else break;
			k=u;
		}
	}
	void pop(){
		h[1]=h[top];n[1]=n[top];p[n[1]]=1;
		top--;pushdown(1);
	}
	void updata(int k,int x){if(h[p[k]]>x)h[p[k]]=x,pushup(p[k]);}
}h;
int test;
int n,m,k,mo,cnt,Cnt;
int head[100005],Head[5100005],v[2][100005],lim[100005];
struct edge{
	int to,next,w;
}e[200005],E[10200005];
bool b[100005],in[100005];
int q[5100005],d[5100005],ans[5100005];
void add(int u,int v,int w){e[++cnt]=(edge){v,head[u],w};head[u]=cnt;}
void Add(int u,int v,int w){E[++Cnt]=(edge){v,Head[u],w};Head[u]=Cnt;}
bool dfs(int now){
	in[now]=true;
	for(int i=head[now];i;i=e[i].next)if((b[e[i].to])&&(e[i].w==0)){
		if(in[e[i].to])return true;
		else if(dfs(e[i].to))return true;
	}
	in[now]=b[now]=false;
	return false;
}
void work(){
	n=read();m=read();k=read();mo=read();
	cnt=Cnt=0;h.clear(n);
	for(int i=1;i<=n;i++)head[i]=Head[i]=0,b[i]=true,in[i]=false;
	for(int i=1;i<=m;i++){
		int u=read(),v=read(),w=read();
		add(u,v,w);Add(v,u,w);
	}
	for(int i=1;i<=n;i++){
		int u=h.n[1];b[u]=false;v[0][u]=h.h[1];h.pop();
		for(int j=head[u];j;j=e[j].next)if(b[e[j].to])h.updata(e[j].to,v[0][u]+e[j].w);
	}
	for(int i=1;i<=n;i++)b[i]=true;
	h.Clear(n);
	for(int i=1;i<=n;i++){
		int u=h.n[1];b[u]=false;v[1][u]=h.h[1];h.pop();
		for(int j=Head[u];j;j=E[j].next)if(b[E[j].to])h.updata(E[j].to,v[1][u]+E[j].w);
	}
	for(int i=1;i<=n;i++){
		lim[i]=v[0][n]+k-v[0][i]-v[1][i];
		b[i]=lim[i]>=0;
	}
	cnt=0;
	for(int i=1;i<=n;i++)head[i]=0;
	for(int i=1;i<=n;i++)if(lim[i]>=0)
		for(int j=Head[i];j;j=E[j].next)add(E[j].to,i,E[j].w);
	for(int i=1;i<=n;i++)if(b[i])if(dfs(i)){
		puts("-1");
		return;
	}
	Cnt=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=lim[i];j++)d[j*n+i]=Head[j*n+i]=0;
	for(int U=1;U<=n;U++)if(lim[U]>=0)
		for(int i=head[U];i;i=e[i].next){
			int V=e[i].to,t=v[0][U]+e[i].w-v[0][V];
			for(int j=0;(j<=lim[U])&&(j+t<=lim[V]);j++){
				Add(j*n+U,(t+j)*n+V,0);
				d[(t+j)*n+V]++;
			}
		}
	int l=1,r=0;
	for(int i=1;i<=n;i++)
		for(int j=0,p=i;j<=lim[i];j++,p+=n){
			if(d[p]==0)q[++r]=p;
			ans[p]=0;
		}
	for(int i=0;i<=k;i++)ans[i*n+1]=1;
	while(l<=r){
		int u=q[l];
		for(int i=Head[u];i;i=E[i].next){
			int v=E[i].to;
			if(!(--d[v]))q[++r]=v;
			ans[v]=(ans[v]+ans[u])%mo;
		}
		l++;
	}
	printf("%d\n",ans[(k+1)*n]);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	test=read();
	while(test--)work();
}

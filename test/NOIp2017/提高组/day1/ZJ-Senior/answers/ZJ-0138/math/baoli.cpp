#include<cstdio>
using namespace std;
int n,m,ans;
bool b[1000005];
inline int read(){
	char c=getchar();int ret=0;
	while((c<'0')||(c>'9'))c=getchar();
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();b[0]=true;
	for(int i=1;i<=n*m;i++){
		if(i>=n)b[i]|=b[i-n];
		if(i>=m)b[i]|=b[i-m];
		if(!b[i])ans=i;
	}
	printf("%d",ans);
}

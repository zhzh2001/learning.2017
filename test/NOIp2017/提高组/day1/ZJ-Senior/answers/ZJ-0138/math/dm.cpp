#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
inline int gcd(int x,int y){return x==0?y:gcd(y%x,x);}
int main(){
	freopen("math.in","w",stdout);
	srand(time(0));
	int n=rand()%999+2,m=rand()%999+2;
	while(gcd(n,m)>1)n=rand()%999+2,m=rand()%999+2;
	printf("%d %d\n",n,m);
}

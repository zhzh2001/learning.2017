#include<cstdio>
using namespace std;
inline int read(){
	char c=getchar();int ret=0;
	while((c<'0')||(c>'9'))c=getchar();
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret;
}
int n,m;
inline int pow(int x,int k,int mo){
	int ans=1;
	for(;k;k>>=1,x=1LL*x*x%mo)if(k&1)ans=1LL*ans*x%mo;
	return ans;
}
inline int phi(int x){
	int ans=1;
	for(int i=2;i*i<=x;i++)if(x%i==0){
		ans=ans*(i-1);x/=i;
		while(x%i==0)ans=ans*i,x/=i;
	}
	if(x>1)ans=ans*(x-1);
	return ans;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=read();m=read();
	if(n>m){int t=n;n=m;m=t;}
	int ans=1LL*(m-n)*pow(n,phi(m)-1,m)%m;
	printf("%d",ans*n-m);
}

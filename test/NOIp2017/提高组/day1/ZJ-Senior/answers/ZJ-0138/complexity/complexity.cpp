#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
inline int read(){
	char c=getchar();int ret=0;
	while(((c<'0')||(c>'9'))&&(c!='n'))c=getchar();
	if(c=='n')return -1;
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret;
}
inline char getc(){
	char c=getchar();
	while(((c<'a')||(c>'z'))&&((c<'A')||(c>'Z')))c=getchar();
	return c;
}
int n,top,test;
int p[105],d[105],a[105];
char s[1005],c,ch;
bool b[30];
void work(){
	n=read();scanf("%s",s);top=0;d[0]=0;
	for(int i=0;i<26;i++)b[i]=false;
	bool ok=true;
	for(int i=1;i<=n;i++){
		c=getc();
		if(c=='F'){
			ch=getc();
			if(b[ch-'a'])ok=false;
			b[ch-'a']=true;a[++top]=ch-'a';
			int l=read(),r=read();
			if(l==r)p[top]=d[top]=0;
			else if(l==-1)p[top]=-1,d[top]=0;
			else if(r==-1)p[top]=1,d[top]=0;
			else if(l>r)p[top]=-1,d[top]=0;
			else p[top]=d[top]=0;
		}else{
			if(top==0){ok=false;continue;}
			b[a[top--]]=false;
			if(p[top]>-1)d[top]=max(d[top],d[top+1]+p[top+1]);
		}
	}
	if((!ok)||(top)){
		puts("ERR");
		return;
	}
	if((d[0]==0)&&(s[2]=='1')){puts("Yes");return;}
	int ss=0,len=strlen(s);
	for(int i=4;i<len-1;i++)ss=ss*10+s[i]-'0';
	if(ss==d[0])puts("Yes");
	else puts("No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	test=read();
	while(test--)work();
}

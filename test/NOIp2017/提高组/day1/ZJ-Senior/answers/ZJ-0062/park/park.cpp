#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <cmath>
#include <queue>
#define maxn 100005
#define maxp 2000000000
using namespace std;

vector<int> link[maxn],v[maxn];
int n,m,k,p[maxn],x,y,z,h,q[maxn*100],head,tail,vis[maxn],T,modp,ans,dp[maxn][101],ru[maxn];
bool flag; 
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&modp);
		flag=false;
		for (int i=1;i<=n;i++){
			link[i].clear();
			v[i].clear();
		}
		for (int i=1;i<=m;i++){
			scanf("%d%d%d",&x,&y,&z);
			link[x].push_back(y);
			v[x].push_back(z);
		}
		head=tail=1;
		q[1]=1;
		for (int i=1;i<=n;i++) p[i]=maxp;
		p[1]=0;
		for (int i=1;i<=n;i++) vis[i]=0;
		vis[1]=1;
		while (head<=tail){
			h=q[head];
			for (int i=0;i<v[h].size();i++){
				int j=link[h][i],o=v[h][i];
				if (o+p[h]<p[j]){
					if (vis[j]==0) q[++tail]=j;
					p[j]=o+p[h];
				} 
			}
			vis[h]=0;
			head++;
		}
		ans=0;
		for (int i=1;i<=n;i++)
			for (int j=0;j<=k;j++)
				dp[i][j]==0;
		dp[1][0]=1;
		head=tail=1;
		q[1]=1;
		for (int i=1;i<=n;i++) ru[i]=maxp;
		ru[1]=0;
		memset(vis,0,sizeof(vis));
		vis[1]=1;
		while (head<=tail){
			h=q[head];
			if (h==n)
				for (int i=0;i<=k;i++) ans=(ans+dp[h][i])%modp;
			for (int i=0;i<v[h].size();i++){
				int j=link[h][i],o=v[h][i];
				int temp=ru[h]+p[h]-p[j]+o;
				if (temp>k) continue;
				if (ru[j]==maxp){
					vis[j]=1;
					q[++tail]=j;
				} 
				if (ru[j]>temp) ru[j]=temp;
				for (int l=0;l<=k;l++){
					if (l+ru[j]>k) break;
					dp[j][ru[j]+l]=(dp[j][ru[j]+l]+dp[h][ru[h]+l])%modp;
				}
			}
			for (int i=0;i<=k;i++) dp[h][i]=0;
			vis[h]=0;
			ru[h]=maxp;
			head++;
		}
		printf("%d\n",ans);
	}
	return 0;
}

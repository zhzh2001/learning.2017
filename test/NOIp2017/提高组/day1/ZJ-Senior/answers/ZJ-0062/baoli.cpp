#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <cmath>
#include <queue>

using namespace std;

int n,m,k,s;
bool flag,f;
int main(){
	scanf("%d%d",&n,&m);
	flag=true;
	k=0;
	if (n<m) swap(n,m);
	while (flag){
		f=false;
		k++;
		for (int i=0;i<=k/n;i++)
			if ((k-i*n)%m==0){
				f=true;
				break;
			}
		if (f) s++;
		else s=0;
		if (s==m) flag=false;
	}	
	cout<<k-m<<endl;
	return 0;
}

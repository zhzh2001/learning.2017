#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <cmath>
#include <queue>

using namespace std;

int n,m,a,b,t,ans;

void extgcd(int x,int y,int &a,int &b){
	if (y==0){
		a=1;b=0;
		return;
	}
	extgcd(y,x%y,a,b);
	int temp=a;
	a=b;
	b=temp-x/y*b;
}

int main(){
	scanf("%d%d",&n,&m);
	extgcd(n,m,a,b);
	if (n<m) {
		swap(n,m);
		swap(a,b);
	}
	if (a>0){
		t=a/m;
		b=b+t*n;
		a=a%m;
	}
	else{
		t=b/n;
		a=a+(t+1)*m;
		b=b%n-n;
	}
	cout<<a<<b<<endl;
	ans=m*(m-1)*(-b)-1;
	cout<<ans<<endl;
	return 0;
}

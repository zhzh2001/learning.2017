#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <cmath>
#include <queue>

using namespace std;

int n,v[105],k,m,t,ans,tot,flag,sum,x,y,stack[30],q[105],p[105];
char s[110];
bool f;

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&n);
	while (n--){
		scanf("%d",&m);
		memset(v,0,sizeof(v));
		scanf("%s",s);
		t=0;
		if (s[2]!=1){
			for (int i=4;i<strlen(s)-1;i++)
				t=t*10+s[i]-'0';
		} 
		tot=0;
		flag=0;
		sum=0;
		ans=0;
		f=true;
		for (int i=1;i<=m;i++){
			scanf("%s",s);
			if (s[0]=='F'){
				scanf("%s",s);
				if (v[s[0]-'a']==1) f=false;
				v[s[0]-'a']=1;
				stack[++tot]=s[0]-'a';
				x=y=0;
				scanf("%s",s);
				if (s[0]!='n')
					for (int i=0;i<strlen(s);i++)
						x=x*10+s[i]-'0';
				scanf("%s",s);
				if (s[0]!='n')
					for (int i=0;i<strlen(s);i++)
						y=y*10+s[i]-'0';
				if (x==0 && y!=0 || (x!=0 && y!=0 && x>y)) {
					q[tot]=1;
					flag++;
				}
				else q[tot]=0;
				if (!flag){
					if (x!=0 && y==0) {
						sum++;
						p[tot]=1;		
						if (sum>ans) ans=sum;
					}
					else p[tot]=0;
				}
				else p[tot]=0;
			}
			else {
				v[stack[tot]]=0;
				if (q[tot]==1) flag--;
				if (p[tot]==1) sum--;
				tot--;
				if (tot<0) {
					f=false;
					tot=0;
				}
			}
		}
		if (!f || tot>0) printf("ERR\n");
		else if (ans==t) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

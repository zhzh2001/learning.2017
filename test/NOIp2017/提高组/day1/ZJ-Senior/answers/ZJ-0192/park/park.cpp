#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<string>
#include<algorithm>
#include<queue>
#define Chtholly 100002
#define Nephren 200002
#define Ithea 52
using namespace std;
typedef pair<int, int> PII;

inline void getint(int &num){
	char ch;
	while(!isdigit(ch = getchar()));
	num = ch - '0';
	while(isdigit(ch = getchar())) num = (num << 1) + (num << 3) + ch - '0';
}

int T;
int N, M, K, P;

struct tEdge{
	int np, val;
	tEdge *nxt;
};

struct Graph{
	tEdge E[Nephren], *V[Chtholly];
	int tope;
	
	inline void Clear() {tope = 0; memset(V, 0, sizeof(V));}
	
	inline void Addedge(int u, int v, int w){
		E[++tope].np = v, E[tope].val = w;
		E[tope].nxt = V[u], V[u] = &E[tope];
	}
} G;

queue<PII> q;
int dis[Chtholly], cnt[Chtholly][Ithea], incnt[Chtholly];
bool inq[Chtholly], too_often;

inline void queue_clear(queue<PII> &q) {queue<PII> tmp; swap(q, tmp);}

#define INC(a, b) a = (a + (b) < P) ? a + (b) : a + (b) - P
inline int SPFA(int s){
	queue_clear(q);
	memset(dis, 0x3f, sizeof(dis)), memset(cnt, 0, sizeof(cnt));
	memset(inq, 0, sizeof(inq)), memset(incnt, 0, sizeof(incnt));
	q.push(PII(s, 0)), inq[s] = 1, dis[s] = 0, cnt[s][0] = 1, incnt[s]++;
	while(!q.empty()){
		PII pair_u = q.front(); q.pop();
		int u = pair_u.first, cur = pair_u.second;
		inq[u] = 0;
		for(register tEdge *ne = G.V[u]; ne; ne = ne->nxt){
			int bey = cur + ne->val - dis[ne->np];
			if(bey < 0){
				dis[ne->np] += bey;
				for(register int i = K; i >= 0; i--){
					if(i + bey >= 0) cnt[ne->np][i] = cnt[ne->np][i + bey];
					else cnt[ne->np][i] = 0;
					cnt[ne->np][i] += cnt[u][i];
				}
			}
			else if(bey <= K){
				for(register int i = bey; i <= K; i++)
					INC(cnt[ne->np][i], cnt[u][i - bey + cur - dis[u]]);
			}
			else continue;
			if(!inq[ne->np]){
				q.push(PII(ne->np, cur + ne->val)), inq[ne->np] = 1;
				incnt[ne->np]++;
				#define TOO_OFTEN 3000000 / N
				if(incnt[ne->np] == TOO_OFTEN) return -1;
			}
		}
	}
	return 0;
}

int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	
	getint(T);
	while(T--){
		getint(N), getint(M), getint(K), getint(P);
		G.Clear();
		for(register int i = 1; i <= M; i++){
			int ai, bi, ci;
			getint(ai), getint(bi), getint(ci);
			G.Addedge(ai, bi, ci);
		}
		if(SPFA(1) == -1){
			printf("-1\n");
			continue;
		}
		int ans = 0;
		for(register int i = 0; i <= K; i++)
			INC(ans, cnt[N][i]);
		printf("%d\n", ans);
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

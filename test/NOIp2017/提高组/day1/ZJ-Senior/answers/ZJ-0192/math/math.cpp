#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;
typedef long long ll;

ll a, b;

int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	
	scanf("%lld%lld", &a, &b);
	ll ans = a * b - a - b;
	printf("%lld\n", ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

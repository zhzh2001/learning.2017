#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

int T, L, cplex, curex, mxex;
char cpl[150], op[150], lvar[150], lreg[150], rreg[150];
char sta[150];
int tops, unavail_id;
bool fin, used[150], addex[150];

#define INF 1000
inline int calc(char num[]){
	if(num[0] == 'n') return INF; 
	int res = 0;
	for(register int i = 0; num[i] != '\0'; i++)
		res = (res << 1) + (res << 3) + num[i] - '0';
	return res;
}

int main(){
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	
	scanf("%d", &T);
	while(T--){
		scanf("%d%s", &L, cpl);
		cplex = 0;
		if(cpl[2] == 'n'){
			for(register int i = 4; cpl[i] != ')'; i++)
				cplex = (cplex << 1) + (cplex << 3) + cpl[i] - '0';
		}
		tops = 0, fin = 0, curex = mxex = 0, unavail_id = 0;
		memset(used, 0, sizeof(used));
		for(register int i = 1; i <= L; i++){
			scanf("%s", op);
			if(op[0] == 'F') scanf("%s%s%s", lvar, lreg, rreg);
			if(fin) continue;
			if(op[0] == 'E'){
				if(addex[tops]) curex--;
				if(tops == unavail_id) unavail_id = 0;
				used[sta[tops--]] = 0;
				if(tops < 0){
					puts("ERR");
					fin = 1;
				}
			}
			else{
				if(used[lvar[0]]){
					puts("ERR");
					fin = 1; continue;
				}
				sta[++tops] = lvar[0];
				used[lvar[0]] = 1;
				if(!unavail_id && lreg[0] <= '9' && rreg[0] == 'n')
					curex++, mxex = max(mxex, curex), addex[tops] = 1;
				else addex[tops] = 0;
				if(!unavail_id && calc(lreg) > calc(rreg)) unavail_id = tops;
			}
		}
		if(!fin){
			if(tops) puts("ERR");
			else if(mxex == cplex) puts("Yes");
			else puts("No");
		}
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

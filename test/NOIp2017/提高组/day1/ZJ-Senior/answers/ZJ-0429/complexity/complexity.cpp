#include<cstdio>
#include<cstring>
#include<iostream>

using namespace std;

inline int Readi(){
	char c=getchar();int num=0;
	while ('0'>c||c>'9') c=getchar();
	while ('0'<=c&&c<='9') num=num*10+c-'0',c=getchar();
	return(num);
}
inline char Readc(){
	char c=getchar();
	while (' '==c||c=='\n') c=getchar();
	return(c);
}

int tt,ans,us[200];
struct Node{int sm,l,r;char c;}a[100000];

inline int Get(){
	char nn=Readc();
	if (nn=='n') return(2333);
	int nm=nn-'0';nn=getchar();
	while ('0'<=nn&&nn<='9') nm=nm*10+nn-'0',nn=getchar();
	return(nm);
}

inline void Work(){
	int n=Readi(),dx=0;
	memset(us,0,sizeof(us));
	char sd=Readc();sd=Readc(),sd=Readc();
	if (sd=='n') dx=Readi();else dx=0,getchar();
		
	tt=0;
	int ck0=1,ck1=1,ans=0;
	for (int i=1;i<=n;i++){
		char op=Readc();
		if (op=='E'){
			if (tt==0) ck0=0;
			else{
				ans=max(ans,a[tt].sm);
				ck1+=(a[tt].l>a[tt].r);
				us[(int)a[tt--].c]=0;
			}
			continue;
		}
		a[++tt].c=Readc();
		if (us[(int)a[tt].c]) ck0=0;else us[(int)a[tt].c]=1;
		
		int l=Get(),r=Get();
		a[tt].l=l,a[tt].r=r;
		if (l==r) {a[tt].sm=a[tt-1].sm;continue;}
		ck1-=(l>r);
		a[tt].sm=a[tt-1].sm+(r==2333)*ck1;
	}
	
	if (tt||!ck0) {puts("ERR");return;} 
	if (dx!=ans) puts("No");else puts("Yes");
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int cs=Readi();
	for (int i=1;i<=cs;i++){
		Work();
	}
}


#include<cstdio>
#include<cstring>
#include<iostream>

#define nn 220010
using namespace std;

inline int Read(){
	char c=getchar();int num=0;
	while ('0'>c||c>'9') c=getchar();
	while ('0'<=c&&c<='9') num=num*10+c-'0',c=getchar();
	return(num);
}

#define fi first
#define se second
#define mk make_pair
#define pr pair<int,int>
struct Heap{
	pr a[nn*2];int sz;
	inline void Push(pr x){
		a[++sz]=x;
		for (int i=sz;i>1&&a[i].se<a[i/2].se;i/=2) swap(a[i],a[i/2]);
	}
	inline pr Top() {return(a[1]);}
	inline void Pop(){
		a[1]=a[sz--];
		for (int i=1,s=2;s<=sz;i=s,s=i*2){
			if (s<sz&&a[s].se>a[s+1].se) s++;
			if (a[i].se>a[s].se) swap(a[i],a[s]);else return;
		}
	}
}hp;

int d[nn],tt,l[nn],v[nn],n,m;
struct Edge{int to,old,w;}e[nn];
inline void Lnk(int x,int y,int z) {e[++tt].to=y,e[tt].old=l[x],l[x]=tt,e[tt].w=z;}
inline void Dij(){
	hp.sz=0,hp.Push(mk(n,0));
	memset(v,0,sizeof(v));v[n]=1;
	for (int i=1;i<n;i++) d[i]=1001*n;d[n]=0;
	for (int i=1;i<n&&hp.sz;i++){
		pr tp=hp.Top();hp.Pop();
		while (v[tp.fi]&&hp.sz) 
		tp=hp.Top(),hp.Pop();
		
		v[tp.fi]=1;
		for (int i=l[tp.fi];i;i=e[i].old)
		if (d[e[i].to]>d[tp.fi]+e[i].w){
			d[e[i].to]=d[tp.fi]+e[i].w;
			hp.Push(mk(e[i].to,d[e[i].to]));
		}
	}
}

struct Road{int a,b,c;}rd[nn];
int lm,md,f[nn][51],g[nn][51],ans,in[nn],q[nn];
inline void Add(int &a,int b) {a=(a+b)%md;}

inline void Work(){
	n=Read(),m=Read(),lm=Read(),md=Read();
	tt=0,memset(l,0,sizeof(l));
	for (int i=1;i<=m;i++){
		int a=Read(),b=Read(),c=Read();
		Lnk(b,a,c),rd[i].a=a,rd[i].b=b,rd[i].c=c;
	}
	Dij();
	
	memset(g,0,sizeof(g));
	memset(f,0,sizeof(f));f[1][0]=1;
	for (int i=1;i<=m;i++) rd[i].c=d[rd[i].b]+rd[i].c-d[rd[i].a];
	
	for (int w=0;w<=lm;w++){
		memset(l,0,sizeof(l)),tt=0;
		for (int i=1;i<=m;i++) if(!rd[i].c)
		in[rd[i].b]++,Lnk(rd[i].a,rd[i].b,0);
		
		tt=0;
		for (int i=1;i<=n;i++) if(!in[i]) q[++tt]=i;
		for (int i=1;i<=tt;i++){
			int x=q[i];
			for (int j=l[x];j;j=e[j].old){
				in[e[j].to]--;
				Add(f[e[j].to][w],f[x][w]);
				if (!in[e[j].to]) q[++tt]=e[j].to;
			}
		}
		
		for (int i=1;i<=n;i++) if(in[i]) in[i]=0,g[i][w]=1;
		for (int i=1;i<=m;i++) if(w+rd[i].c<=lm&&rd[i].c){
			Add(f[rd[i].b][w+rd[i].c],f[rd[i].a][w]);
			f[rd[i].b][w|+rd[i].c]|=g[rd[i].a][w];
		}		
		if (g[n][w]){
			puts("-1");
			return;
		}
	}
	
	ans=0;
	for (int i=0;i<=lm;i++) Add(ans,f[n][i]);
	printf("%d\n",ans);
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	int tst=Read();
	for (int i=1;i<=tst;i++)
	Work();
}

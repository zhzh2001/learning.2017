#include<cstdio>
#include<cstring>
#include<iostream>

#define ll long long
using namespace std;

ll a,b;
inline ll Read(){
	char c=getchar();ll nm=0;
	while ('0'>c||c>'9') c=getchar();
	while ('0'<=c&&c<='9') nm=nm*10+c-'0',c=getchar();
	return(nm);
}

ll x,y,ans;
inline void Gcd(ll a,ll b){
	if (a==1) {x=1,y=0;return;}
	Gcd(b,a%b);
	ll tp=x;x=y,y=tp-(a/b)*y;
}

int main(){
	a=Read(),b=Read();
	Gcd(a,b);
	for (ll i=1;i<=a*b;i++){
		ll xx=x*i,yy=y*i;
		xx=(xx%b+b)%b,yy=(i-a*xx)/b;
		if (xx<0||yy<0) ans=max(ans,i);
	}
	printf("%lld\n",ans);
}

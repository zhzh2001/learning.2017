#include<cstdio>
#include<cstring>
#include<iostream>

#define ll long long
using namespace std;

ll a,b;
inline ll Read(){
	char c=getchar();ll nm=0;
	while ('0'>c||c>'9') c=getchar();
	while ('0'<=c&&c<='9') nm=nm*10+c-'0',c=getchar();
	return(nm);
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	a=Read(),b=Read();
	printf("%lld\n",a*b-a-b);
}

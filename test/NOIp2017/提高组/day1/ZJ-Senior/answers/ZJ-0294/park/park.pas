type et=record next,p,w:longint end;
var e,f:array[1..200000]of et;
    a,b,w:longint;
    q,r:array[1..50000000]of longint;
    o:array[1..100000]of boolean;
    first,dis,cc,firstf:array[1..100000]of longint;
    n,m,k,pp,i,j,t,tt,ans:longint;
    bb:boolean;
procedure adde(a,b,w,i:longint);
begin
  e[i].next:=first[a];
  first[a]:=i;
  e[i].p:=b;e[i].w:=w;
end;
procedure addf(a,b,w,i:longint);
begin
  f[i].next:=firstf[a];
  firstf[a]:=i;
  f[i].p:=b;f[i].w:=w;
end;
procedure spfa;
var head,tail,p,x:longint;
begin
  for i:=2 to n do dis[i]:=maxlongint;
  fillchar(o,sizeof(o),false);
  tail:=1;head:=0;q[1]:=1;o[1]:=true;
  while head<tail do
  begin
    inc(head);
    x:=q[head];
    p:=first[x];
    while p<>0 do
    begin
      if dis[e[p].p]>dis[x]+e[p].w then
      begin
        dis[e[p].p]:=dis[x]+e[p].w;
        if o[e[p].p]=false then
        begin
          inc(tail);
          q[tail]:=e[p].p;
        end;
      end;
      p:=e[p].next;
    end;
    o[x]:=false;
  end;
end;
procedure bfs(w:longint);
var head,tail,p,x,y:longint;
begin
  tail:=1;head:=0;q[1]:=n;r[1]:=w;
  while head<tail do
  begin
    inc(head);
    x:=q[head];
    y:=r[head];
    p:=firstf[x];
    while p<>0 do
    begin
      if (f[p].w<=y)and(dis[f[p].p]<=y-f[p].w) then
      begin
        inc(tail);
        q[tail]:=f[p].p;
        r[tail]:=y-f[p].w;
        inc(cc[q[tail]]);
        if cc[q[tail]]>m then bb:=true;
        if q[tail]=1 then ans:=(ans+1) mod pp;
      end;
      p:=f[p].next;
    end;
    if bb then break;
  end;
end;
begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  read(tt);
  for t:=1 to tt do
  begin
    read(n,m,k,pp);
    for i:=1 to n do first[i]:=0;
    for i:=1 to n do firstf[i]:=0;
    for i:=1 to m do
    begin
      read(a,b,w);
      adde(a,b,w,i);
      addf(b,a,w,i);
    end;
    spfa;
    bb:=false;fillchar(cc,sizeof(cc),0);ans:=0;
    bfs(dis[n]+k);
    if bb then writeln(-1) else writeln(ans);
  end;
  close(input);
  close(output);
end.

var c:array[-1000..1000]of char;
    t:array[-1000..1000]of longint;
    n,i,j,k,top,p,l,o,p1,p2,tot,max,x,y:longint;
    b,err:boolean;
    s:ansistring;
    ii:char;
procedure putin(x:longint;cc:char);
begin
  inc(top);t[top]:=x;c[top]:=cc;
  if x=-1 then b:=false;
  if x=1 then
  begin
    inc(tot);
    if tot>max then max:=tot;
  end;
end;
begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(n);
  for i:=1 to n do
  begin
    readln(s);err:=false;b:=true;top:=0;max:=0;tot:=0;
    p:=pos(' ',s);
    val(copy(s,1,p-1),l);
    p1:=pos('(',s);p2:=pos(')',s);
    if s[p1+1]='1' then o:=0 else val(copy(s,p1+3,p2-p1-3),o);
    for j:=1 to l do
    begin
      readln(s);
      if (s[1]='F')and(b) then
      begin
        ii:=s[3];
        for k:=1 to top do if ii=c[k] then err:=true;
        delete(s,1,4);
        p:=pos(' ',s);
        if s[1]='n' then putin(-1,ii)
        else if s[p+1]='n' then putin(1,ii)
        else
        begin
          val(copy(s,1,p-1),x);
          val(copy(s,p+1,length(s)),y);
          if x<=y then putin(0,ii);
          if x>y then putin(-1,ii);
        end;
      end
      else if (s[1]='F')and(not b) then
      begin
        for k:=1 to top do if s[3]=c[k] then err:=true;
        putin(-1,s[3]);
      end
      else if s[1]='E' then
      begin
        if t[top]=1 then dec(tot);
        dec(top);
        if t[top]<>-1 then b:=true;
      end;
    end;
    if (err)or(top<>0) then writeln('ERR') else
      if max=o then writeln('Yes') else writeln('No');
  end;
  close(input);
  close(output);
end.

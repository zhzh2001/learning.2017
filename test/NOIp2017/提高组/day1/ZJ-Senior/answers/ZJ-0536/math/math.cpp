#include <iostream>
#include <cstdio>
using namespace std;
long long ans(long long a , long long b){
	if ((a == 1) || (b == 1)) return 0;
	long long t = a*b-a-b;
	return t;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	long long a,b;
	cin>>a>>b;
	cout<<ans(a,b)<<endl;
	return 0;
}

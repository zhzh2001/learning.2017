#include <cstring>
#include <cstdio>
#include <algorithm>
#include <iostream>
using namespace std;
int L;
string lopnam[200];
int sot[200]; // 0 : * 0 | 1 : *1 | 2 : cifang *n
int poww[200];
int maxx(int a , int b){
	if (a > b) return a;
	return b;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	cin>>t;
while (t--){
	bool errflag = false;
	int cpow = 0;
	int stcnt = 0;
	memset(sot,0,sizeof(sot));
	memset(poww,0,sizeof(poww));
	cin>>L;
	string comp;
	cin>>comp;
	if (comp[2] == 'n'){
	    int cnt = 4;
		while (('0'<=comp[cnt]) && (comp[cnt]<='9')){
			cpow = cpow*10+(int)comp[cnt]-48;
			cnt++;
		}
	}
	else cpow = 0;
	for (int i = 1; i <= L; i++){
		char c; cin>>c;
		if (c == 'F') {
			stcnt++;
			string loo; cin>>loo;
			lopnam[stcnt] = loo;
			for (int j = 1; j < stcnt; j++) if (lopnam[j] == lopnam[stcnt]) errflag = true;
	    	string be , en;
	    	cin>>be>>en;
	    	if ((be == "n") && (en == "n")){
	    		sot[stcnt] = 0;
			}
	    	else if ((be == "n")){
	    		sot[stcnt] = 0;
			}
			else if ((en == "n")){
				sot[stcnt] = 2;
			}
			else{
				int ben = 0 , enn = 0;
				for (int j = 0; j < be.length(); j++)
				ben = ben*10+(int)be[j]-48;
				for (int j = 0; j < en.length(); j++)
				enn = enn*10+(int)en[j]-48;
				if (ben<= enn) sot[stcnt] = 1;
				else sot[stcnt] = 0;
		    }
	//	    cout<<stcnt<<" "<<sot[stcnt]<<endl;
		}
		else if (stcnt > 0){
			if (sot[stcnt] == 0) poww[stcnt] = 0;
			else if (sot[stcnt] == 2){
				poww[stcnt] += 1;
			}
	//		cout<<stcnt<<" "<<poww[stcnt]<<endl;
			poww[stcnt-1] = maxx(poww[stcnt-1],poww[stcnt]);
			poww[stcnt] = 0;
			stcnt--;
		}
		else{
			errflag = true;
		}
	}
	if (stcnt != 0) errflag = true;
	if (errflag) cout<<"ERR"<<endl;
	else
	if (poww[0] == cpow) cout<<"Yes"<<endl;
	else cout<<"No"<<endl;
//	cout<<poww[0]<<endl;
}
return 0;
}

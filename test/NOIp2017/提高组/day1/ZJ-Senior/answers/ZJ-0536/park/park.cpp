#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>
using namespace std;
const int maxn = 100005;
const int inf = 100000007;
vector <int> lt[maxn];
vector <int> we[maxn];
int d[maxn];
int tim[maxn];
bool vd[maxn];
int N,M,K,P;
int cnt;
/*
void search(int o , int n){
	if (d[o]+K < n) return;
	if (o == N) cnt++;
	for (int i = 0; i < lt[o].size(); i++)
	search(lt[o][i],n+we[o][i]);
}
*/
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
int t;
cin>>t;
while (t--){
	cin>>N>>M>>K>>P;
	for (int i = 1; i <= N; i++) lt[i].clear();
	for (int i = 1; i <= N; i++) we[i].clear();
	for (int i = 1; i <= M; i++){
		int u,v,w;
		cin>>u>>v>>w;
		lt[u].push_back(v);
		we[u].push_back(w);
	}
	for (int i = 0; i <= N; i++) d[i] = inf;
	for (int i = 1; i <= N; i++) vd[i] = false;
	if (K == 0){
		for (int i = 0; i <= N; i++) tim[i] = 0;
		d[1] = 0; tim[1] = 1;
		for (int i = 1; i <= N; i++){
			int ch = 0;
			for (int j = 1; j <= N; j++)
			if (!vd[j] && d[ch] > d[j]) ch = j;
			vd[ch] = true;
			tim[ch] = tim[ch] % P;
			for (int j = 0; j < lt[ch].size(); j++){
				int v = lt[ch][j] , w = we[ch][j];
				if (d[v] > d[ch] + w){
					tim[v] = tim[ch]; d[v] = d[ch]+w;
				}
				else if (d[v] == d[ch] + w)
				tim[v] += tim[ch];
			}
		}
		cout<<tim[N]%P<<endl;
	}
	else {
		cout<<-1<<endl;
	}
}
return 0;
}

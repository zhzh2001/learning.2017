#include<cstdio>
#include<cstring>
int t,n,m,s,Res,Ans,p[105];
char a[105];
bool Vis[205];
int Pos(char ch,char str[15])
{
	int i,Len=strLen(str);
	for (i=0;i<Len;i++)
	if (str[i]==ch) return i;
	return 0;
}
int Compare(char str1[15],char str2[15])
{
	int i,Len;
	if (str1[0]=='n'&&str2[0]!='n') return 0;
	if (str1[0]!='n'&&str2[0]=='n') return 2;
	if (strLen(str1)>strLen(str2)) return 0;
	Len=strLen(str1);
	for (i=0;i<Len;i++)
	{
		if (str1[i]>str2[i]) return 0;
		if (str1[i]<str2[i]) return 1;
	}
	return 1;
}
int main()
{
	int i,j,x,y,Len; char ch,str[15],str1[15],str2[15],str3[15];
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%s",&n,str);
		x=Pos('n',str);
		Res=0;
		if (x)
		{
			x+=2;
			while (str[x]>='0'&&str[x]<='9') Res=10*Res+str[x]-48,x++;
		}
		memset(Vis,0,sizeof(Vis));
		m=s=Ans=p[0]=0;
		for (i=1;i<=n;i++)
		{
			scanf("%s",str);
			if (str[0]=='F') scanf("%s%s%s",str1,str2,str3);
			if (Ans<0) continue;
			if (str[0]=='F')
			{
				if (Vis[str1[0]]) {Ans=-1; continue;}
				else a[++m]=str1[0],Vis[a[m]]=1;
				p[m]=Compare(str2,str3);
				if (!p[m]) p[0]=1;
				if (p[m]>1&&!p[0]) if (++s>Ans) Ans=s;
			}
			else
			{
				if (!m) {Ans=-1; continue;}
				Vis[a[m]]=0;
				if (p[m]>1&&!p[0]) s--;
				m--;
				p[0]=0;
				for (j=1;j<=m;j++)
				if (!p[j]) p[0]=1;
				
			}
		}
		if (Ans<0||m) printf("ERR\n");
		else printf("%s\n",Ans==Res?"Yes":"No");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

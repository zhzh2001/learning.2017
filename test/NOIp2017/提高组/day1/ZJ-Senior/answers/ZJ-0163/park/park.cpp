#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=100005;
int t,n,m,k,tt,tot,ans,fa[100005],dist[100005],que[100005],start[100005],w[200005],son[200005],next[200005];
bool vis[100005];
void add(int x,int y,int z)
{
	w[++tot]=z; son[tot]=y; next[tot]=start[x]; start[x]=tot;
}
int get(int x)
{
	if (fa[x]==x) return x;
	return fa[x]=get(fa[x]);
}
bool merge(int x,int y)
{
	int z,fx=get(x),fy=get(y);
	if (fx==fy)
	{
		z=get(n);
		if (z==fx) return 1;
	}
	else fa[fx]=fy;
	return 0;
}
void spfa()
{
	int j,head=0,tail=1;
	memset(dist,63,sizeof(dist));
	memset(vis,0,sizeof(vis));
	dist[1]=0; vis[1]=que[1]=1;
	while (head^tail)
	{
		vis[que[head=(head+1)%maxn]]=0;
		for (j=start[que[head]];j;j=next[j])
		if (dist[que[head]]+w[j]<dist[son[j]])
		{
			dist[son[j]]=dist[que[head]]+w[j];
			if (!vis[son[j]])
			{
				vis[son[j]]=1; que[tail=(tail+1)%maxn]=son[j];
				if (dist[que[tail]]<dist[que[(head+1)%maxn]]) swap(que[tail],que[(head+1)%maxn]);
			}
		}
	}
}
void dfs(int x,int cost)
{
	int j;
	if (cost>dist[x]+k) return;
	if (x==n)
	{
		ans=(ans+1)%tt;
		return;
	}
	for (j=start[x];j;j=next[j]) dfs(son[j],cost+w[j]);
}
int main()
{
	int i,x,y,z;
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&tt);
		tot=0; memset(start,0,sizeof(start));
		for (i=1;i<=n;i++) fa[i]=i;
		ans=0;
		while (m--)
		{
			scanf("%d%d%d",&x,&y,&z),add(x,y,z);
			if (!z) if (merge(y,x)) ans=-1;
		}
		if (!ans) spfa(),dfs(1,0);
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

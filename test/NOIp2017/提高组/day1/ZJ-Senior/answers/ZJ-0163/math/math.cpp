#include<cstdio>
#include<algorithm>
using namespace std;
long long a,b,ans;
bool p;
int main()
{
	int i;
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a<b) swap(a,b);
	ans=a*b;
	while (ans--)
	{
		p=0;
		for (i=0;i<b;i++)
		{
			if (a*i>ans) break;
			if ((ans-a*i)%b==0) p=1;
		}
		if (!p) break;
	}
	printf("%lld",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

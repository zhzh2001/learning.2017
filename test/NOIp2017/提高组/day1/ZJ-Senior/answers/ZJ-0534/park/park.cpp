#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<vector>
#include<queue>
#define N 100010
#define pp pair<int,int>
using namespace std;
const int inf=0x3f3f3f3f;
vector<pp>q[N],q2[N];
int T,n,m,k,p,a,b,c,ans;
int dis[N];
bool v[N];
struct node{
	int wei,g;
};
char operator >(node a,node b){
	return a.g+dis[a.wei]>b.g+dis[b.wei];
}
priority_queue<pp,vector<pp>,greater<pp> >w;
priority_queue<node,vector<node>,greater<node> >e;
void dijkstra(){
	while(!w.empty())w.pop();
	memset(v,0,sizeof v);
	memset(dis,inf,sizeof dis);
	dis[n]=0;
	w.push(pp(dis[n],n));
	while(!w.empty()){
		while(!w.empty()&&v[w.top().second])w.pop();
		if(w.empty())break;
		pp t=w.top();
		int now=t.second;
		w.pop();
		v[now]=1;
		for(int i=0;i<q[now].size();i++){
			int to=q[now][i].first;
			if(dis[to]>dis[now]+q[now][i].second){
				dis[to]=dis[now]+q[now][i].second;
				w.push(pp(dis[to],to));
			}
		}
	}
}
int A_star(){
	while(!e.empty())e.pop();
	e.push((node){1,0});
	while(!e.empty()){
		node t=e.top();
		int now=t.wei;
		e.pop();
		for(int i=0;i<q2[now].size();i++){
			int to=q2[now][i].first;
			if(dis[to]+t.g+q2[now][i].second<=dis[1]+k){
				e.push((node){to,t.g+q2[now][i].second});
			}
			if(to==n){
				ans++;
				if(ans>=20000)return -1;
			}
		}
	}
	return ans%p;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(q,0,sizeof q);
		memset(q2,0,sizeof q2);
		for(int i=1;i<=m;i++){
			scanf("%d%d%d",&a,&b,&c);
			q[b].push_back(pp(a,c));
			q2[a].push_back(pp(b,c));
		}
		ans=0;
		dijkstra();
		printf("%d\n",A_star());
	}
	return 0;
}

#include<iostream>
#include<cstring>
#include<math.h>
#include<cstdio>
#include<stack>
using namespace std;
int len,f[103],ans[103];
stack<int>q;
bool v[30];
char s[30],temp,w,x[10],y[10];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%s",&len,s);
		int n=strlen(s),flag=0,comp=0,pp=0,deep=0,cnt=1e6;
		for(int i=0;i<n;i++){
			if(s[i]=='('){
				flag=1;
				if(s[i+1]=='1'){
					comp=0;
					break;
				}
				for(int j=i+3;j<n;j++){
					if(s[j]==')')break;
					comp=comp*10+s[j]-'0';
				}
			}
			if(flag)break;
		}
		memset(v,0,sizeof v);
		memset(ans,0,sizeof ans);
		memset(f,0,sizeof f);
		f[0]=0;
		for(int i=1;i<=len;i++){
			cin>>temp;
			if(temp=='F'){
				deep++;
				cin>>w>>x>>y;
				if(v[w-'a'])pp=1;
				v[w-'a']=1;
				q.push(w-'a');
				int r1=0,r2=0;
				for(int i=0;i<strlen(x);i++){
					r1=10*r1+x[i]-'0';
				}
				for(int i=0;i<strlen(y);i++){
					r2=10*r2+y[i]-'0';
				}
				if(x[0]!='n'&&y[0]=='n')f[deep]=1;
				else if(x[0]=='n'&&y[0]!='n'||x[0]!='n'&&y[0]!='n'&&r1>r2)f[deep]=-1000,cnt=deep;
				else f[deep]=0;
				f[deep+1]=1000;
			}
			else{
				deep--;
				if(deep<0){
					pp=1;
					continue;
				}
				for(int i=deep+1;f[i]!=1000;i++){
					if(f[i]<0)f[i]=0;
					f[deep]+=f[i];
					f[i]=0;
				}
				ans[deep]=max(ans[deep],f[deep]);
				ans[deep-1]=max(ans[deep-1],ans[deep]+f[deep-1]);
				ans[deep+1]=0;
				f[deep]=0;
				int now=q.top();
				q.pop();
				v[now]=0;
				
			}
		}
		if(pp==1||deep!=0)printf("ERR\n");
		else if(ans[0]==comp)printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

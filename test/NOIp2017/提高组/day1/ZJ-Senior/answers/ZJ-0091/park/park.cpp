#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
#include<ctime>
#define pll pair<ll,ll>
#define pii pair<int,int>
#define mp make_pair
#define F first
#define S second
#define mem(x,y) memset(x,y,sizeof(x))
using namespace std;
typedef long long ll;
const int N=100010,M=200010,INF=2e9;
inline void read(int &x)
{
	x=0; int f=1; char ch=getchar();
	while((ch<'0' || ch>'9') && ch!='-') ch=getchar(); if(ch=='-') {f=-1; ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0'; ch=getchar();}
	x*=f;
}
int n,m,K,P;
int head[N],to[M],v[M],nxt[M],lst;
int h[N],t[M],co[M],ne[M],ls;
int dis[N],cnt[N]; bool used[N],inq[N][50];
pll dist[N][60];
int que[N<<2];
inline void adde(int x,int y,int c) {nxt[++lst]=head[x]; to[lst]=y; v[lst]=c; head[x]=lst;}
inline void add(int x,int y,int c) {ne[++ls]=h[x]; t[ls]=y; co[ls]=c; h[x]=ls;}
inline bool _SPFA()
{
	int he=0,ta=1; que[ta]=n; dis[n]=0; used[n]=1;
	while(he<ta)
	{
		he++; int p=que[he]; used[p]=0;
		for(int i=h[p];i;i=ne[i])
		{
			int v=t[i];
			if(dis[v]>=dis[p]+co[i])
			{
				dis[v]=dis[p]+co[i];
				if(!used[v]) 
				{
					que[++ta]=v;
					used[v]=1;
					cnt[v]++;
					if(cnt[v]>=n) {puts("-1"); return false;}
				}
			}
		}
	}
	return true;
}
inline bool SPFA()
{
	for(int i=1;i<=n;i++) dis[i]=INF;
	int he=0,ta=1; que[ta]=1; dis[1]=0; used[1]=1;
	while(he<ta)
	{
		he++; int p=que[he]; used[p]=0;
		for(int i=head[p];i;i=nxt[i])
		{
			int t=to[i];
			if(dis[t]>dis[p]+v[i])
			{
				dis[t]=dis[p]+v[i];
				if(!used[t]) 
				{
					que[++ta]=t;
					used[t]=1;
//					cnt[v]++;
//					if(cnt[v]>=n) {puts("-1"); return false;}
				}
			}
		}
	}
	return true;
}
inline void Work()
{
	queue<pii> que; mem(dist,0);
	dist[1][0]=mp(0,1); que.push(mp(1,0)); inq[1][0]=1;
	while(!que.empty())
	{
		pll p=que.front(); que.pop(); inq[p.F][p.S]=0;
		for(register int i=head[p.F];i;i=nxt[i])
		{
			int t=to[i],t1=dis[p.F]+p.S+v[i]-dis[t];
			if(dis[t]==INF) continue;
			if(t1>K) continue;
			if(t1<0) throw;
			dist[t][t1].S+=dist[p.F][p.S].S;
			dist[t][t1].F+=dist[p.F][p.S].F+dist[t][t1].S/P;
			dist[t][t1].S%=P;
			if(!inq[t][t1]) {que.push(mp(t,t1)); inq[t][t1]=1;}
		}
	}
	
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
 	int T; read(T);
 	int x,y,c;
 	while(T--)
 	{
		read(n); read(m); read(K); read(P);
 		mem(head,0); mem(h,0); mem(cnt,0); lst=ls=0;
		for(int i=1;i<=n;i++) dis[i]=INF;
		for(int i=1;i<=m;i++)
		{
			read(x); read(y); read(c);
			adde(x,y,c); add(y,x,c);
		}
		if(!_SPFA()) continue;
		SPFA();
//		cerr<<clock()<<endl;
//		for(int i=2;i<=n;i++) if(dis[i]!=INF) dis[i]=dis[1]-dis[i]; dis[1]=0;
		Work();
		ll ans=0;
//		printf("dis:"); for(int i=1;i<=n;i++) printf("%d ",dis[i]); puts("");
//		puts("dist:");
//		for(int i=1;i<=n;i++)
//		{
//			for(int j=0;j<=K;j++)
//				printf("%lld ",dist[i][j].S);
//			puts("");
//		}puts("");
		for(int i=0;i<=K;i++) ans=(ans+dist[n][i].S)%P;
		printf("%lld\n",ans);
	}
	return 0;
}
/*
1
6 8 2 10000000
1 2 1
2 3 2
3 1 1
3 4 0
4 5 1
5 3 1
3 6 0
5 6 1

*/

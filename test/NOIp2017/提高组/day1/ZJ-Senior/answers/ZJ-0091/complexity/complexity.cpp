#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define mem(x,y) memset(x,y,sizeof(x))
using namespace std;
typedef long long ll;
const int INF=2e9;
int n,com;
char s[110];
bool used[300];
struct STA {char x; bool f,ty;};//f->not in? ty->1?
STA sta[1010]; int top=0;
char type[110],bian[110],fr[110],en[110];
int maxd=0,now=0;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--)
	{
		now=0; top=0; maxd=0; bool ok=1; int isin=0;
		scanf("%d%s",&n,s+1); int ls=strlen(s+1);
//		printf("?%d?",n);
		for(int i=1;i<=ls;i++)
			if(s[i]=='(')
			{
				i++; com=0;
				if(s[i]=='n')
				{
					i+=2;
					while(s[i]>='0' && s[i]<='9') com=com*10+s[i++]-'0';
				}
				else break;
			}
			
		for(int t=1;t<=n;t++)
		{
//			printf("%d %d %d\n",isin,now,maxd);
			scanf("%s",type+1);
			if(type[1]=='F')
			{
				scanf("%s%s%s",bian+1,fr+1,en+1);
				if(ok)
				{
					if(used[(int)bian[1]]) {ok=0; continue;}
					used[(int)bian[1]]=1;
					int a=0,b=0;
					if(fr[1]=='n') a=INF;
					else {int j=1; while(fr[j]>='0' && fr[j]<='9') a=a*10+fr[j++]-'0';}
					if(en[1]=='n') b=INF;
					else {int j=1; while(en[j]>='0' && en[j]<='9') b=b*10+en[j++]-'0';}
//					printf("?%d %d?\n",a,b);
					sta[++top]=(STA) {bian[1],(a>b),(a<b && b==INF)};
					isin+=sta[top].f;
					if(sta[top].ty) now++; if(isin==0) {maxd=max(now,maxd);}
				}
			}
			else if(ok)
			{
				isin-=sta[top].f; now-=sta[top].ty; used[(int)sta[top--].x]=0;
				if(top<0) {ok=0; continue;}
			}
		}
		for(int i=1;i<=256;i++) if(used[i]) {ok=0; used[i]=0;}
		if(ok) puts(maxd==com ? "Yes" : "No");
		else puts("ERR");
	}
	return 0;
}
/*
1
22 O(n^5)
F a 1 n
	F b 1 n
		F c 1 1
		E
	E
	F y 99 n
		F b 100 99
			F c 1 n
				F d 1 n
					F z 1 n
					E
				E
			E
		E
		F h n n
			F g 100 n
				F l 100 n
				E
			E
		E
	E
E
*/
/*1
2 O(1)
F a n n
E*/
/*8
2 O(1)
F i 1 1
E
2 O(n^1)
F x 1 n
E
1 O(1)
F x 1 n
4 O(n^2)
F x 5 n
F y 10 n
E E
4 O(n^2)
F x 9 n
E
F y 2 n
E
4 O(n^1)
F x 9 n
F y n 4
E E
4 O(1)
F y n 4
F x 9 n
E E 4
O(n^2)
F x 1 n
F x 1 10
E E*/

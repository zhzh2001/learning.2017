#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
using namespace std;
char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
char cc,stc[200];int n,T,t,x,y,answer,ans,tag,len,gg[200],rec[200],r;bool bo[200],p;
inline void read(int&x)
{
	cc=nc();x=0;
	while ((cc<'0'||cc>'9')&&cc!='n') cc=nc();
	if (cc=='n') {x=1000;return;}
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
}
inline void read(LL&x)
{
	cc=nc();x=0;
	while ((cc<'0'||cc>'9')&&cc!='n') cc=nc();
	if (cc=='n') {x=1000;return;}
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
}
inline void read(char&cc)
{
	cc=nc();
	while ((cc<'a'||cc>'z')&&(cc<'A'||cc>'Z')) cc=nc();
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(T);
	while (T--)
	{
		read(n);read(cc);
		cc=nc();cc=nc();
		if (cc=='1') answer=0;else{read(answer);}
		for (cc='a';cc<='z';cc++) bo[cc]=true;
		len=0;p=true;t=1;tag=0;ans=0;
		for (int i=1;i<=n;i++)
		{
			read(cc);
			if (cc=='F')
			{
				read(cc);p&=bo[cc];
				bo[cc]=false;stc[++len]=cc;
				read(x);
				read(y);
				gg[len]=0;rec[len]=0;
				if (x>y) t=0,rec[len]=1,++r;
				else if (x!=1000&&y==1000) tag+=t,gg[len]=t,tag>ans?ans=tag:0;
			}
			else
			{
				if (len==0) p=false;
				else
				{
					tag-=gg[len];r-=rec[len];
					if (r==0) t=1;bo[stc[len]]=true;len--;
				}
			}
		}
		if (len!=0) p=false;
		if (p) {if (ans==answer) puts("Yes");else puts("No");}else puts("ERR");
	}
	return 0;
}

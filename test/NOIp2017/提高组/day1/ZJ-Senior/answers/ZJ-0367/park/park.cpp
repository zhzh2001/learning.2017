#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define MM 1000000007
#define limit 100000
using namespace std;
int n,m,p,nm,x,y,z,t,w,ans,rec,b[200010],c[200010],d[200010],value[200010],tag[200010],k,T,f[10000010];
char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
char cc;
inline void read(int&x)
{
	cc=nc();x=0;
	while (cc<'0'||cc>'9') cc=nc();
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
}
inline void read(LL&x)
{
	cc=nc();x=0;
	while (cc<'0'||cc>'9') cc=nc();
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
}
void sear(int x,int y)
{
	if (x==n) ans++;rec++;
	if (rec>limit) return;
	for (int i=d[x];i>0;i=c[i])
		if (y+value[i]<=tag[b[i]]+k)
		{
			sear(b[i],y+value[i]);
			if (rec>limit) return;
		}
	rec--;
}
inline void add(int x,int y,int z)
{
	b[++nm]=y;c[nm]=d[x];d[x]=nm;value[nm]=z;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while (T--)
	{
		read(n);read(m);read(k);read(p);nm=0;memset(d,0,sizeof(d));
		for (int i=1;i<=m;i++) read(x),read(y),read(z),add(x,y,z);
		for (int i=1;i<=n;i++) tag[i]=MM;tag[1]=0;
		t=1;w=1;f[1]=1;rec=0;ans=0;
		while (t<=w)
		{
			x=f[t++];
			for (int i=d[x];i>0;i=c[i])
			{
				y=b[i];
				if (tag[x]+value[i]<tag[y])
				{
					tag[y]=tag[x]+value[i];
					f[++w]=y;
				}
			}
		}
		sear(1,0);
		if (rec>limit) printf("-1\n");else printf("%d\n",ans%p);
	}
	return 0;
}

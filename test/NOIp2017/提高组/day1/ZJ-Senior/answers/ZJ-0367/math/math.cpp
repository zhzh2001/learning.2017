#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
using namespace std;
int a,b,t;LL x,y,xy,ans1,ans2,ans,q;
char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
char cc;int n;
inline void read(int&x)
{
	cc=nc();x=0;
	while (cc<'0'||cc>'9') cc=nc();
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
}
inline void read(LL&x)
{
	cc=nc();x=0;
	while (cc<'0'||cc>'9') cc=nc();
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
}
void exgcd(int a,int b,LL&x,LL&y)
{
	if (b==0) {x=1;y=0;return;}
	LL x1,y1;
	exgcd(b,a%b,x1,y1);
	x=y1;y=x1-a/b*y1;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	if (a>b) swap(a,b);
	read(a);read(b);
	exgcd(a,b,x,y);
	if (x>0) swap(x,y),swap(a,b);
	x=-x;
	xy=y*(a-1);
	ans1=b*x;
	q=(xy-1)/y+1;
	ans2=b*xy-q;
	ans=ans1<ans2?ans1:ans2;
	printf("%lld\n",ans-1);
	return 0;
}
	

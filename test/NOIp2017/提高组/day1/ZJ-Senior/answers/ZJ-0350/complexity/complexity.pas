var
  i,j,k,t,m,n,len,f,e,top,bj,x,y,max,l,js,b:longint;
  stack:array[0..1000] of longint;
  s,q:array[0..1000] of string;
  s1,s2,s3,s4,cc,p,ss:string;
  ii:char;
  ch:array['a'..'z'] of longint;
begin
  assign(input,'complexity.in'); reset(input);
  assign(output,'complexity.out'); rewrite(output);
  readln(n);
  for i:=1 to n do
    begin
      fillchar(ch,sizeof(ch),0);
      fillchar(stack,sizeof(stack),0);
      f:=0; e:=0;  bj:=0; top:=0;  max:=0;  b:=0;

      readln(s1);
      k:=pos(' ',s1);
      s2:=copy(s1,1,k-1);
      val(s2,l);
      delete(s1,1,k);

      k:=pos('(',s1);
      t:=pos(')',s1);
      len:=t-k-1;
     ss:=copy(s1,k+1,len);
      for j:=1 to l do
      begin
        readln(s[j]);  x:=0;y:=0;
        if top=0 then js:=0;
        if s[j]='E'
          then begin inc(e); if f>0 then begin ii:=q[stack[top]][1]; ch[ii]:=0;if top>0 then begin stack[top]:=0; dec(top);end; end;
         if (f<>e)and(j=l) then begin writeln('ERR'); bj:=1;break; end;  continue; end;
        inc(f);
        delete(s[j],1,2);
        q[j]:=copy(s[j],1,1);
        ii:=q[j][1];
        if ch[ii]>0 then begin writeln('ERR'); bj:=1;break; end;
        inc(ch[ii]);
        delete(s[j],1,2);
        k:=pos(' ',s[j]);
        s3:=copy(s[j],1,k-1);
        delete(s[j],1,k);

        s[j]:=s[j]+' ';
        k:=pos(' ',s[j]);
        s4:=copy(s[j],1,k-1);
        delete(s1,1,k);

        inc(top); stack[top]:=j;
        if s3<>'n' then val(s3,x);
        if s4='n' then y:=1000 else val(s4,y);
        if (top=1)and((s3='n')or((x>0)and(s4<>'n')))or((s3='n')and(s4='n'))
          then cc:='1'
          else if top=1 then begin b:=1; inc(js); end;
        if (x>0)and(top>1)and(s[j-1]<>'E')and(s4='n')
          then inc(js);
          if js>max then max:=js;
          if (f<>e)and(j=l) then begin writeln('ERR'); bj:=1;break; end;
      end;
      if bj=0 then begin
      str(max,p);
      if (cc<>'1')or(b=1)then cc:='n^'+p;
      if cc=ss then writeln('Yes') else writeln('No');    end;
    end;
    close(input);  close(output);
end.

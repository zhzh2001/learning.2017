#include<cstdio>
#include<iostream>
#include<queue>
using namespace std;
#define Max 100010
#define INF 100000000
inline int read(){
	int x=0,ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
	return x;
}

int t,n,m,k,p,a,b,c,tot;

queue <int> q;
struct Node{
	int v,d;
};
vector <Node> e[Max];
bool flag[Max];
int d[Max];
int ans=0;

void search(int u,int s){
	if(u==n&&s<=tot) ans++;
	if(s>tot) return;
	for(vector<Node>::iterator it=e[u].begin();it!=e[u].end();it++){
		search(it->v,s+it->d);
	}
}

void SPFA(){
	for(int i=2;i<=n;i++) d[i]=INF,flag[i]=0;
	d[1]=0;
	flag[1]=1;
	q.push(1);
	while(!q.empty()){
		int u=q.front() ;q.pop() ;flag[u]=0;
		for(vector<Node>::iterator it=e[u].begin();it!=e[u].end();it++){
			int v=it->v,w=it->d;
			if(d[v]>d[u]+w){
				d[v]=d[u]+w;
				if(!flag[v]){
					flag[v]=1;
					q.push(v);
				}
			}
		}
	} 
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	t=read();
	for(int zu=1;zu<=t;zu++){
		ans=0;
		n=read(),m=read(),k=read(),p=read();
		
		for(int i=0;i<Max;i++) e[i].clear() ;
		
		for(int bian=1;bian<=m;bian++){
			a=read(),b=read(),c=read();
			e[a].push_back((Node){b,c});
			e[b].push_back((Node){a,c});
		}
		SPFA();
		tot=d[n];
		tot+=k;
		search(1,0);
		printf("%d\n",ans);
		
	}
	
	
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

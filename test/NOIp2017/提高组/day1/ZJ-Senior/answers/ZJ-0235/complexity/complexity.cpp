#include<cstdio>
#include<iostream>
#include<queue>
#include<map>
using namespace std;
#define Max 100010
#define INF 100000000
inline int read(){
	int x=0,ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
	return x;
}
inline int cread(){
	int x=0,ch=getchar();
	while(!isdigit(ch)){
		if(ch=='n') {
			return 0;
		}
		ch=getchar();
	}
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
	return 1;
}
int t,n,time;
map<char,int> q;
char c,s[120];
int a,b,num,ans,aim,opt[120];
int kuo;
bool flag;


int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	t=read();
	for(int zu=1;zu<=t;zu++){
		n=read();
		scanf("%c%c%c",&c,&c,&c);
		if(c!='1'){
			scanf("%c",&c);
			time=read();
		}else time=0,scanf("%c",&c);
		q.clear() ;
		ans=0,num=0;
		kuo=0;
		flag=false;
		opt[0]=0;
		for(int i=1;i<=n;i++){
			scanf("%c%c",&c,&c);
			if(c=='F'){
				scanf("%c%c",&c,&c);
				a=cread(),b=cread();     // 1 digige        0  n
				kuo++;
				if(q.count(c)) flag=true;
				if((a==1&&b==1)||(a==0&&b==1)){
					q.insert({c,1}),num++,s[num]=c;
					opt[num]=0;
				}else{
					q.insert({c,1}),num++,s[num]=c;
					if(opt[num-1]==0&&num!=1) opt[num]=0;
					else  opt[num]=opt[num-1]+1,ans=max(ans,opt[num]);
				}
			}else{
				kuo--;
				q.erase(s[num]); 
				num--;
			}
		}
		if(flag||kuo!=0){
			printf("ERR\n");
		}else{
			if(ans==time)  printf("Yes\n");
			else  printf("No\n");
		}
			
		
		
		
	}
	
	
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cmath>
using namespace std;

inline long long read(){
	long long x=0,ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
	return x;
}

long long exgcd(long long a,long long b,long long &x,long long &y){
	if(b==0){
		x=1;
		y=0;
		return a;
	}
	long long r = exgcd(b,a%b,x,y);
	long long t = x;
	x = y;
	y = t - a/b * y;
	return r; 
}
int a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	a=read(),b=read();
	
	long long x=0,y=0,p=0;
	exgcd(a,b,x,y);
	if(x<0){
		p=a*abs(x)*(a-1)-1;
	}else{
		p=b*abs(y)*(b-1)-1;
	}
	printf("%lld",p);
	
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

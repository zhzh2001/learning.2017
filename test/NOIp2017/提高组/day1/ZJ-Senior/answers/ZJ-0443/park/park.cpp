#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cctype>
#include<queue>
#include<stack>
#include<vector>
using namespace std;
typedef long long LL;

const int N=100000+10,M=200000+10,K=50+2;
int t,n,m,k,p,to[M],ne[M],lnk[N],cnt,inq[N],flag;
LL len[M],dis[N],ans,vis[N];

void add(int a,int b,LL c){
	to[++cnt]=b;len[cnt]=c;ne[cnt]=lnk[a];lnk[a]=cnt;
}

queue<int> q;
void spfa(){
	while(!q.empty())q.pop();
	memset(dis,63,sizeof(dis));
	memset(inq,0,sizeof(inq));
	q.push(1);dis[1]=0;inq[1]=1;
	while(!q.empty()){
		int x=q.front();q.pop();inq[x]=0;
		for(int i=lnk[x];i;i=ne[i]){
			int v=to[i];
			if(dis[x]+len[i]<dis[v]){
				dis[v]=dis[x]+len[i];
				if(!inq[v])inq[v]=1,q.push(v);
			}
		}
	}
}

void dfs(int x,int sum){
	vis[x]++;if(vis[x]>10000){flag=1;return;}
	if(sum>dis[n]+k)return;
	if(x==n && sum-dis[n]<=k)ans++,ans%=p;
	for(int i=lnk[x];i;i=ne[i]){
		dfs(to[i],sum+len[i]);
		if(flag)return;
	}
}

void solve(){
	scanf("%d%d%d%d",&n,&m,&k,&p);
	memset(lnk,0,sizeof(lnk));cnt=0;
	for(int i=1;i<=m;i++){
		int a,b;LL c;
		scanf("%d%d%lld",&a,&b,&c);
		add(a,b,c);
	}
	spfa();
	ans=0;memset(vis,0,sizeof(vis));flag=0;
	dfs(1,0);
	if(flag)puts("-1");else
	printf("%lld\n",ans);
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while(t--)solve();
	fclose(stdin);fclose(stdout);
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cctype>
#include<queue>
#include<stack>
using namespace std;

const int INF=999;
char s[20],ch1[5],ch2[5],x[5],y[5];
int t,L,w,now,ans,vis[30],have_err;
struct node{
	int ch,comp;
};
stack<node> st;

int getW(){
	if(s[2]=='1')return 0;
	if(s[3]==')')return 1;
	int num=0;
	for(int i=4;s[i]!=')';i++)num=num*10+s[i]-'0';
	return num;
}

void solve(){
	scanf("%d%s",&L,s);
	w=getW();
	now=ans=0;have_err=0;
	memset(vis,0,sizeof(vis));
	while(!st.empty())st.pop();
	for(int i=1;i<=L;i++){
		scanf("%s",ch1);
		if(ch1[0]=='E'){
			if(st.empty())have_err=1;
			else now-=st.top().comp,vis[st.top().ch]=0,st.pop();
		}else{
			scanf("%s%s%s",ch2,x,y);
			if(vis[ch2[0]-'a'])have_err=1;
			else vis[ch2[0]-'a']=1;
			node nd;nd.ch=ch2[0]-'a';
			int xx=0,yy=0;
			if(x[0]=='n')xx=INF;else{
				for(int i=0;i<strlen(x);i++)xx=xx*10+x[i]-'0';
			}
			if(y[0]=='n')yy=INF;else{
				for(int i=0;i<strlen(y);i++)yy=yy*10+y[i]-'0';
			}
			if(xx!=INF && yy==INF)nd.comp=1;
			else if(xx>yy)nd.comp=-INF;
			else nd.comp=0;
			st.push(nd);
			now+=nd.comp;ans=max(now,ans);
		}
	}
	if(!st.empty())have_err=1;
	if(have_err)puts("ERR");else{
		if(ans==w)puts("Yes");
		else puts("No");
	}
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--)solve();	
	return 0;
	fclose(stdin);fclose(stdout);
}

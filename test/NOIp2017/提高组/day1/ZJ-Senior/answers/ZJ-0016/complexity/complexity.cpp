#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
int b[30];
int t,ans,a1,a2;
int l,v;
char head[30],tail[30];
char s[1000],x,u;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		ans=0;
		scanf("%d",&l);
		cin>>s;
		int tot=0;
		int num=0;
		memset(b,0,sizeof(b));
		bool flag=true;
		bool f=true;
		for (int i=1;i<=l;i++){
			cin>>x;
			if (x=='F'){
				tot++;
				cin>>u>>head>>tail;
				if (b[u-'a'+1])flag=false;
				b[u-'a'+1]=1;
				if ((head[0]<'0'||head[0]>'9')&&tail[0]<='9'&&tail[0]>='0')f=false;
				if ((head[0]>='0'&&head[0]<='9')&&tail[0]<='9'&&tail[0]>='0'){
					int l1=strlen(head)-1;
					int l2=strlen(tail)-1;
					int w=1;
					a1=0;a2=0;
					while (head[l1]<='9'&&head[l1]>='0'){
						a1+=w*(head[l1]-'0');
						w*=10;
						if (l1==0)break;
						l1--;
					}
					w=1;
					while (tail[l2]<='9'&&tail[l2]>='0'){
						a2+=w*(tail[l2]-'0');
						w*=10;
						if (l2==0)break;
						l2--;
					}
					if (a1>a2)f=false;
				}
				if (head[0]<='9'&&head[0]>='0'&&(tail[0]>'9'||tail[0]<'0')&&(f))num++;
			}
			else{
				tot--;
				if (tot==0){
					memset(b,0,sizeof(b));f=true;
					ans=max(num,ans);num=0;
				}
			}
		}
		if (!flag){printf("%s\n","ERR");continue;}
		if (tot!=0){printf("%s\n","ERR");continue;}
		v=0;
		if (s[2]=='1')v=0;
		else {
			int len=strlen(s)-2;
			int w=1;
			while (s[len]<='9'&&s[len]>='0'){
				v+=w*(s[len]-'0');
				w*=10;
				len--;
			}
		}
		if (v==ans)printf("%s\n","Yes");
		else printf("%s\n","No");
	}	
	return 0;
}

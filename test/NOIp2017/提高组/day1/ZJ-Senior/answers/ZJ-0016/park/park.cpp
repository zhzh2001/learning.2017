#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
using namespace std;
#define N 500000
int t,ans;
int nowlen;
int u,v,w;
int n,m,k,p;
struct Edge{
	int to,next,w;
}edge[N];
int tot;
int head[N],vis[N],inq[N];
int dis[N];
queue<int>q;
void add(int u,int v,int w){
	tot++;
	edge[tot].next=head[u];edge[tot].to=v;
	edge[tot].w=w;head[u]=tot;
}

void dfs(int now){
	if (now==n)
		if (nowlen<=dis[n]+k&&nowlen>=dis[n]){
			ans=(ans+1)%p;
			return;	
		}
	for (int i=head[now];i;i=edge[i].next){
		if (nowlen+edge[i].w<=dis[n]+k){
			vis[edge[i].to]=1;
			nowlen+=edge[i].w;
			dfs(edge[i].to);
			vis[edge[i].to]=0;
			nowlen-=edge[i].w;	
		}
	}	
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		tot=0;
		memset(dis,0x3f3f,sizeof(dis));
		memset(vis,0,sizeof(vis));
		memset(inq,0,sizeof(inq));
		scanf("%d%d%d%d",&n,&m,&k,&p);	
		for (int i=1;i<=m;i++){
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		while (!q.empty())q.pop();
		q.push(1);inq[1]=1;
		dis[1]=0;
		while (!q.empty()){
			int x=q.front();q.pop();inq[x]=0;
			for (int i=head[x];i;i=edge[i].next){
				int y=edge[i].to;
				if (dis[y]>dis[x]+edge[i].w){
					dis[y]=dis[x]+edge[i].w;
					if (!inq[y]){
						q.push(y);
						inq[y]=1;
					}
				}
			}
		}
		int head=dis[n],tail=dis[n]+k;
		ans=0;
		vis[1]=1;
		nowlen=0;
		dfs(1);
		printf("%d\n",ans%p);
	}
	return 0;
}

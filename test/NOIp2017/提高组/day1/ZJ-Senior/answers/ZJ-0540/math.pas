var
  a,b:int64;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);

  readln(a,b);
  writeln((a-1)*b-a);

  close(input);
  close(output);
end.

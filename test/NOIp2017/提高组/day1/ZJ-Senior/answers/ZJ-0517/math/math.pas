var
a,b,t:longint;
n:int64;
begin
assign(input,'math.in');
assign(output,'math.out');
reset(input);
rewrite(output);
read(a,b);
if a<b then begin t:=b;b:=a;a:=t;end;
n:=a*(b-1)-b;
writeln(n);
close(input);
close(output);
end.

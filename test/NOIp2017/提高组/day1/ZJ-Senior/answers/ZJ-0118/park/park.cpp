#include<bits/stdc++.h>
#include<vector>
#include<queue>
using namespace std;

const int maxn = 100000;

vector<pair<int,int> > G[maxn + 1], g[maxn + 1];
int n, m, mod, k;
long long dis[maxn + 1], dis2[maxn + 1], lim[maxn + 1];
bool vis[maxn + 1];

bool flag = false;
void dfs(int x){
	if (flag) return; vis[x] = true;
	for (unsigned i = 0; i < G[x].size(); ++i){
		int v = G[x][i].first, cost = G[x][i].second; 
		if (dis[v] < dis[x] + cost || (dis[v] == dis[x] + cost && cost != 0)) continue;
		dis[v] = dis[x] + cost;
		if (!vis[v]) dfs(v);
		else if (vis[v] == true){
			flag = true; return;
		}
	}
	vis[x] = false;
}

bool inqueue[maxn + 1];
void spfa2(){
	for (int i = 1; i <= n; ++i) dis2[i] = 1e9; dis2[n] = 0;
	for (int i = 1; i <= n; ++i) inqueue[i] = false; inqueue[n] = true;
	queue<int> q; while (!q.empty()) q.pop(); q.push(n);
	while (!q.empty()){
		int x = q.front(); inqueue[x] = false; q.pop();
		for (unsigned i = 0; i < g[x].size(); ++i){
			int v = g[x][i].first, cost = g[x][i].second; if (dis2[v] <= dis2[x] + cost) continue;
			dis2[v] = dis2[x] + cost; if (!inqueue[v]){q.push(v); inqueue[v] = true;}
		}
	}
}

void Add(long long &a, long long b){
	a += b; if (a >= mod) a -= mod; if (a < 0) a += mod;
}

long long f[500][maxn + 1];
bool iinqueue[500][maxn + 1];
int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T --){
		scanf("%d%d%d%d", &n, &m, &k, &mod);
		for (int i = 1; i <= n; ++i) G[i].clear(), g[i].clear();
		memset(vis, false, sizeof(vis));
		for (int i = 1; i <= m; ++i){
			int u, v, c; scanf("%d%d%d", &u, &v, &c);
			G[u].push_back(make_pair(v, c));
			g[v].push_back(make_pair(u, c));
		}
//		flag = false;
//		for (int i = 1; i <= n; ++i) dis[i] = 1e9; dis[1] = 0; 
//		dfs(1);
//		if (flag){printf("-1\n"); continue;}
		spfa2();
		for (int i = 1; i <= n; ++i) lim[i] = dis2[1] - dis2[i] + k;
		memset(f, 0, sizeof(f));		f[0][1] = 1;
		queue<pair<int,int> > q; while (!q.empty()) q.pop(); q.push(make_pair(0, 1));
		while (!q.empty()){
			int x = q.front().second, disx = q.front().first; q.pop(); iinqueue[disx][x] = false;
			
			for (unsigned i = 0; i < G[x].size(); ++i){
				long long v = G[x][i].first, cost = G[x][i].second; if (disx + cost > lim[v]) continue;
				int idx = disx + cost - dis[i];
				Add(f[idx][v], f[disx][x]);
				if (!iinqueue[idx][v]) {q.push(make_pair(disx + cost, v)); iinqueue[idx][v] = true;}
			}
		}
		long long ans = 0;	for (int i = 0; i <= lim[n]; ++i) Add(ans, f[i][n]);
		printf("%lld\n", ans);		
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
/*
8858651
*/

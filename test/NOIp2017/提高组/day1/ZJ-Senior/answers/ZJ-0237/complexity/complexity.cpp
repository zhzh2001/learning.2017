#include <bits/stdc++.h>
#define N 110
using namespace std;
int p0,top,t,L;
int flag[N],p[N],b0[N];
char name[N];
char get()
{
	char ch = getchar();
	while (!((ch >= 'a' && ch <= 'z') || (ch >= '0' &&  ch <= '9') || ch == 'E' || ch == 'F')) ch = getchar();
	return ch;
}
int getnum()
{
	char ch = getchar();int num = 0;
	while (!(ch >= '0' && ch <= '9')) ch = getchar();
	while (ch >= '0' && ch <= '9') num = num * 10 + ch - '0',ch = getchar();
	return num;
}
int max(int a,int b)
{
	return a > b ? a : b;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t = getnum();
	while (t--)
	{
		L = getnum();p0 = -1;char ch = getchar();int a,b,pans = 0;
		while (!(ch == 'n' || (ch >= '0' && ch <= '9'))) ch = getchar();
		if (ch == 'n') p0 = 0,ch = getchar(),ch = getchar();
		while (ch >= '0' && ch <= '9') p0 = p0 * 10 + ch -'0',ch = getchar();
		top = 0;bool check = true;
		for (int i = 0;i <= N;i++) flag[i] = 1,b0[i] = 0;
		for (int i = 1;i <= L;i++)
		{
			ch = get();
			if (ch == 'F')
			{
				ch = get();if (b0[ch - 'a']) check = false;name[++top] = ch;b0[ch - 'a'] = 1;flag[top] = flag[top - 1];p[top] = p[top - 1];
				a = 0;b = 0;
				ch = get();if (ch == 'n') a = -1; else while (ch >= '0' && ch <= '9') 	a = a * 10 + ch - '0',ch = getchar();	
				ch = get();if (ch == 'n') b = -1; else while (ch >= '0' && ch <= '9') 	b = b * 10 + ch - '0',ch = getchar();
				if ((a == -1 && b > 0) || (a > b && b > 0)) flag[top] = 0;
				if ((a == -1 && b != -1) || (a != -1 && b == -1)) p[top] += 1 * flag[top];
				pans = max(pans,p[top]);
			}
			else 
			{
				b0[name[top] - 'a'] = 0,top--;
				if (top < 0) check = false,top = 0;
			}
		}
		if ((!check) || top != 0) printf("ERR\n"); else
		if ((p0 < 0 && pans == 0) || (p0 == pans)) printf("Yes\n"); else
		printf("No\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
} 

#include<bits/stdc++.h>
#define N 110000
using namespace std;
queue<int> Q;
int vis[N],head[N],dp[N],d[N],pre[N],a[N];
int dp0[1100][60];
int cnt;
int T,n,m,k,p;
struct Edge
{
	int to,nxt,dis;
}e[2 * N];
void add(int from,int to,int dis)
{
	pre[to]++;e[++cnt] = (Edge){to,head[from],dis};head[from] = cnt;
}
void SPFA()
{
	Q.push(1);vis[1] = 1;memset(vis,0,sizeof(vis));memset(d,-1,sizeof(d));d[1] = 0;
	while (!Q.empty())
	{
		int u = Q.front();Q.pop();vis[u] = 0;
		for (int i = head[u],v;v = e[i].to,i != -1;i = e[i].nxt)
			if (d[v] == -1 || d[v] > d[u] + e[i].dis)
			{
				d[v] = d[u] + e[i].dis;
				if (!vis[v]) Q.push(v),vis[v] = 1;
			}
	}
}
bool cmp(int x,int y)
{
	return d[x] < d[y];
}
void work()
{
	memset(dp,0,sizeof(dp));dp[1] = 1;
	for (int i = 1;i <= n;i++) a[i] = i;
	sort(a + 1,a + n + 1,cmp);
	for (int j = 1;j <= n;j++)
	if (d[a[j]] != -1)
	{
		int u = a[j];
		for (int i = head[u],v;v = e[i].to,i != -1;i = e[i].nxt)
			if (d[v] == d[u] + e[i].dis) dp[v] += dp[u],dp[v] %= p;
	}
}
void work2()
{
	for (int i = 0;i <= n+2;i++)
	for (int j = 0;j <= 50;j++) dp0[i][j] = 0;
	dp0[1][0] = 1;
	for (int i = 1;i <= n;i++) a[i] = i;
	sort(a + 1,a + n + 1,cmp);
	for (int j = 1;j <= n;j++)
	if (d[a[j]] != -1)
	{
		int u = a[j];
		for (int i = head[u],v;v = e[i].to,i != -1;i = e[i].nxt)
			for (int m = 0;m <= k;m++)
				for (int n = m;n <= k;n++)
					if (d[v] + n - m == d[u] + e[i].dis) dp0[v][n] += dp0[u][m],dp0[v][n] %= p;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		memset(head,-1,sizeof(head));memset(pre,0,sizeof(pre));cnt = -1;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i = 1;i <= m;i++) {int x,y,z;scanf("%d%d%d",&x,&y,&z);add(x,y,z);}
		if (!k) 
		{
			SPFA();work();printf("%d\n",dp[n]);
		}
		else
		{
			SPFA();work2();
			int ans = 0;
			for (int i = 0;i <= k;i++)
				ans += dp0[n][i],ans %= p;
			printf("%d\n",ans);
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
} 

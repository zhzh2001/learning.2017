#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
#define N 10000000
int ans,now,T,Q,to[120];
char op[120][5],st[120][10],ed[120][10],str[50],Var[120][20];
int stack[120],stack1[120];
bool vis[300];
void dfs(int deep,int now)
{
	if (deep>ans) ans=deep;
	for (int j=now;j<=to[now];)
	{
		int beg=0,en=0;
		if (st[now][0]=='n')
		{
			if (ed[now][0]=='n') dfs(deep,j+1),j=to[j]+1;
			else j=to[j]+1;
		}
		else
		{
			int len=strlen(st[now]);
			for (int i=0;i<len;i++)
				beg=beg*10+st[now][i]-'0';
			if (ed[now][0]=='n') dfs(deep+1,j+1),j=to[j]+1;
			else
			{
				len=strlen(ed[now]);
				for (int i=0;i<len;i++)
					en=en*10+ed[now][i]-'0';
				if (beg>en) j=to[j]+1;
				else dfs(deep,j+1),j=to[j]+1;
			}
		}
	}
	return;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&Q);
		scanf("%s",str);
		memset(vis,0,sizeof(vis));
		bool syn=true;
		int top=0;
		memset(to,0,sizeof(to));
		for (int i=1;i<=Q;i++)
		{
			scanf("%s",op[i]);
			if (op[i][0]=='F')
			{
				scanf("%s",Var[i]);
				scanf("%s%s",st[i],ed[i]);
				if (vis[Var[i][0]]) syn=false;
				stack[++top]=Var[i][0];
				stack1[top]=i;
				vis[Var[i][0]]=true;
			}
			else
			{
				vis[stack[top]]=false;
				to[stack1[top]]=i;
				top--;
				if (top<0) syn=false;
			}
		}
		if (top>=1) syn=false;
		if (!syn)
		{
			printf("ERR\n");
			continue;
		}
		now=1;ans=0;
		while (now<=Q) dfs(0,now),now=to[now]+1;
		if (str[2]=='1')
		{
			if (ans==0) printf("Yes\n");
			else printf("No\n");
		}
		else
		{
			int len=strlen(str);
			int tim=0;
			for (int i=0;i<len;i++)
				if (str[i]>='0'&&str[i]<='9') tim=tim*10+str[i]-'0';
			if (tim==ans) printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

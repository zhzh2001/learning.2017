#include <cstdio>
#include <queue>
#include <iostream>
#include <cstring>
using namespace std;
typedef pair<int,int> pr;
priority_queue<pr,vector<pr>,greater<pr> > que;
queue<int> q;
int val[400010],cnt,next[400010],head[400010],dis[400010],dist[400010],num[100010][55],pre[100010][55];
bool vis[100010];
int T,n,m,k,mod,inq[100010];
void add(int a,int b,int c)
{
	val[++cnt]=b;
	next[cnt]=head[a];
	head[a]=cnt;
	dis[cnt]=c;
}
int add(int x)
{
	if (x>=mod) return x-mod;
	if (x<0) return x+mod;
	return x;
}
void Dijkstra()
{
	for (int i=1;i<=n;i++) dist[i]=1e9+5;
	dist[1]=0;
	que.push(pr(0,1));
	while (!que.empty())
	{
		pr tmp=que.top();
		que.pop();
		int u=tmp.second;
		if (dist[u]!=tmp.first) continue;
		for (int i=head[u];i;i=next[i])
		{
			int v=val[i],c=dis[i];
			if (dist[u]+c<dist[v])
			{
				dist[v]=dist[u]+c;
				que.push(pr(dist[v],v));
			}
		}
	}
}
void spfa()
{
	memset(inq,0,sizeof(inq));
	q.push(1);vis[1]=1;
	num[1][0]=1;inq[1]=1;
	while (!q.empty())
	{
		int u=q.front();
		q.pop();
		vis[u]=0;
		for (int i=head[u];i;i=next[i])
		{
			int v=val[i],c=dis[i];
			if (!vis[v])
				for (int j=0;j<=k;j++) pre[v][j]=num[v][j];
			bool flag=false;
			for (int j=0;j<=k;j++)
			{
				int x=dist[u]+j+c-dist[v];
				if (x>k) break;
				num[v][x]=add(add(num[v][x]-pre[u][j])+num[u][j]);
				if (num[v][x]!=pre[v][x]) flag=true;
			}
			if (!vis[v]&&flag)
			{
				vis[v]=true;q.push(v);inq[v]++;
				if (inq[v]>n)
				{
					printf("-1\n");
					return;
				}
			}
		}
	}
	int ans=0;
	for (int i=0;i<=k;i++) ans=add(ans+num[n][i]);
	printf("%d\n",ans);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&mod);
		cnt=0;
		memset(head,0,sizeof(head));
		for (int i=1;i<=m;i++)
		{
			int u,v,c;
			scanf("%d%d%d",&u,&v,&c);
			add(u,v,c);
		}
		Dijkstra();
		for (int i=1;i<=n;i++)
			for (int j=0;j<=k;j++)
				pre[i][j]=num[i][j]=0;
		spfa();
	}
	return 0;
}

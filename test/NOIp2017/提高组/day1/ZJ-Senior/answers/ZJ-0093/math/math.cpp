#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<math.h>
#include<bitset>
#include<vector>
#include<cstring>
#include<complex>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int x=Read(),y=Read();
	if (x>y) swap(x,y); if (x==1){puts("0"); return 0;}
	long long Res=1ll*x*(x-1)-1+1ll*(y-x-1)*(x-1); cout<<Res<<endl;
}

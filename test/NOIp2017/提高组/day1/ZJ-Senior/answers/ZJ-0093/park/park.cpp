#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<math.h>
#include<bitset>
#include<vector>
#include<cstring>
#include<complex>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
#define Val edge[i].val
using namespace std;
const int INF=2e9+7,N=1e5+5,M=2e5+5;
int loc[N],pos[N],heap[N],dis[N],f[N],head[N],nedge,poi;
int n,m,k,Mod,tot,dp[N][55],q[N*55][2],Res;
struct E{int next,to,val;} edge[M<<1];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int Swap(int x,int y){
	swap(loc[pos[x]],loc[pos[y]]),swap(pos[x],pos[y]),swap(heap[x],heap[y]);
}
inline int up(int u){
	for (int fa;u>1;u>>=1){fa=u>>1;
		if (heap[u]>=heap[fa]) return 0; Swap(u,fa);
	}
}
inline int Insert(int p){
	loc[p]=++tot,pos[tot]=p,heap[tot]=dis[p]; up(tot);
}
inline int addline(int x,int y,int val){
	edge[++nedge].to=y,edge[nedge].next=head[x],edge[nedge].val=val,head[x]=nedge;
}
inline int down(int p){
	for (int ls,rs,poi;;){
		ls=p<<1,rs=ls|1;
		if (ls>tot) return 0;
		if (rs>tot) poi=ls;
		else poi=heap[ls]>heap[rs]?rs:ls;
		if (heap[p]>heap[poi]) Swap(p,poi); p=poi;
	}
}
inline int Del(){
	heap[1]=INF,f[pos[1]]=1; down(1);
}
inline int Add(int &x,int y){x=(x%Mod+y%Mod)%Mod;}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (int T=Read();T--;){tot=0;
		memset(heap,0,sizeof(heap));
		memset(loc,0,sizeof(loc)),memset(pos,0,sizeof(pos));
		n=Read(),m=Read(),k=Read(),Mod=Read();
		memset(head,0,sizeof(head)),nedge=0;
		memset(f,0,sizeof(f));
		for (int i=1;i<=m;i++){
			int u=Read(),v=Read(),x=Read();
			addline(u,v,x);
		}
		memset(dis,63,sizeof(dis)); dis[1]=0;
		for (int i=1;i<=n;i++) Insert(i);
		for (int j=1;j<=n;j++){
			poi=pos[1]; Del();
			for (int i=head[poi];i;i=NX){
				if ((!f[O])&&dis[poi]+Val<dis[O])
					dis[O]=dis[poi]+Val,heap[loc[O]]=dis[O],up(loc[O]);
			}
		}
	//	for (int i=1;i<=n;i++) printf("%d ",dis[i]); puts("");
		memset(dp,0,sizeof(dp)); dp[1][0]=1;
		for (int l=0;l<=k;l++)
			for (int j=1;j<=n;j++){
				if (!dp[j][l]) continue;
				for (int i=head[j];i;i=NX){
					int Num=dis[O]-dis[j];
					if (Val-Num+l<=k) Add(dp[O][Val-Num+l],dp[j][l]);//printf("DP:%d %d %d %d %d %d\n",j,l,dp[j][l],O,Val-Num+l,dp[O][Val-Num+l]);
				}
			}
		for (int i=0;i<=k;i++) Add(Res,dp[n][i]); printf("%d\n",Res); Res=0;
	}
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<math.h>
#include<bitset>
#include<vector>
#include<cstring>
#include<complex>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
using namespace std;
const int N=105;
char s1[N],s2[N],s3[N];
int n,Res,Tier,time,flag,top,s[N],num[N],vis[N<<2],GG[N],Flag;
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (int T=Read();T--;){
		memset(vis,0,sizeof(vis));
		memset(num,0,sizeof(num));
		memset(GG,0,sizeof(GG));
		memset(s,0,sizeof(s));
		n=Read(); scanf("\n%s",s1+1);
		int Len=strlen(s1+1),pos=0;
		for (int i=1;i<=Len;i++)
			if (s1[i]=='^') pos=i+1;
		Res=Tier=top=time=Flag=0; flag=1;
		if (pos)
			for (int i=pos;s1[i]>47&&s1[i]<58;i++)
				time=time*10+s1[i]-48;
	//	printf("%d\n",time);
		for (int i=1;i<=n;i++){
			scanf("\n%s",s1+1); Len=strlen(s1+1);
			if (s1[1]=='E')
				if (!top) flag=0;
				else{vis[num[top]]=0;
					if (s[top]) Tier--;
					if (GG[top]) Flag--;
					s[top]=GG[top]=num[top]=0; top--;
				}
			else{int x=0,y=0;
				scanf("\n%s",s1); scanf("\n%s",s2); scanf("\n%s",s3);
				if (vis[s1[0]]) flag=0; vis[num[++top]=s1[0]]=1;
				if (s2[0]!='n')
					for (int i=0;i<strlen(s2);i++) x=x*10+s2[i]-48;
				if (s3[0]!='n')
					for (int i=0;i<strlen(s3);i++) y=y*10+s3[i]-48;
				if (x&&(!y)) s[top]=(Flag==0),Tier+=s[top];
				else s[top]=0;
				if (y&&(!x)||(x&&y&&x>y)) GG[top]=1,Flag++;
				else GG[top]=0;
				Res=max(Res,Tier);
			}
		//	printf("Res==%d\n",Res);
		}
	//	printf("%d\n",Res);
		if ((!flag)||top){puts("ERR"); continue;}
		puts(Res==time?"Yes":"No");
	}
}



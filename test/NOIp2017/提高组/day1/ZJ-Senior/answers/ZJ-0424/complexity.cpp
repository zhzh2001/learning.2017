#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
struct node
{
	int x,y,son;
}q[10005];
int T,L,ci,id,len,top;
char str[1005],ch[105],s[105],a[105],b[105];
bool ok,flag[1005];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%s",&L,str);
		len=strlen(str);
		ci=0; ok=1; top=0; q[0].son=0; id=0;
		for (int i=0; i<=25; i++) flag[i]=0;
		if (str[2]!='1')
			for (int i=4; i<len-1; i++) ci=ci*10+str[i]-'0';
		for (int i=1; i<=L; i++)
		{
			scanf("%s",ch);
			if (ch[0]=='E')
			{
				if (top==0)
				{
					ok=0;
					id=i+1;
					break;
				}
				flag[q[top].x]=0;
				if (q[top].y==-1) q[top].y=0;
				else
					if (q[top].y==0) q[top].y=q[top].son;
					else
						if (q[top].y==1) q[top].y=q[top].son+1;
				q[top-1].son=max(q[top-1].son,q[top].y);
				top--;
			}
			else
			{
				scanf("%s%s%s",s,a,b);
				if (flag[s[0]-'a'])
				{
					ok=0;
					id=i+1;
					break;
				}
				flag[s[0]-'a']=1;
				q[++top].x=s[0]-'a';
				if (a[0]=='n' && b[0]=='n') q[top].y=0;
				if (a[0]=='n' && b[0]!='n') q[top].y=-1;
				if (a[0]!='n' && b[0]=='n') q[top].y=1;
				if (a[0]!='n' && b[0]!='n')
				{
					int len1=strlen(a),len2=strlen(b);
					int sa=0,sb=0;
					for (int j=0; j<len1; j++) sa=sa*10+a[j]-'0';
					for (int j=0; j<len2; j++) sb=sb*10+b[j]-'0';
					if (sa<=sb) q[top].y=0;
					else q[top].y=-1;
				}
				q[top].son=0;
			}
		}
		if (ok && top==0)
		{
			if (q[0].son==ci) printf("Yes\n");
			else printf("No\n");
		}
		else
		{
			if (id>0)
				for (int i=id; i<=L; i++)
				{
					scanf("%s",ch);
					if (ch[0]!='E') scanf("%s%s%s",s,a,b);
				}
			printf("ERR\n");
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,k,p,hd,tl,cnt,ans,T,eu[200005],ev[200005],ew[200005],vet[200005],head[200005],value[200005],Next[200005],dis[3][200005],q[1000005],dp[100005][55];
bool vis[200005],flag;
void add(int x,int y,int z)
{
	vet[++cnt]=y;
	value[cnt]=z;
	Next[cnt]=head[x];
	head[x]=cnt;
}
/*void tarjan(int u,int fa)
{
	dfn[u]=low[u]=++idx;
	instack[u]=1;
	stack[++top]=u;
	for (int i=head[u]; i; i=Next[i])
	{
		int v=vet[i];
		if (!dfn[v])
		{
			tarjan(v,u);
			low[u]=min(low[u],low[v]);
		}
		else
			if (instack[v]) low[u]=min(low[u],dfn[v]);
	}
	if (dfn[u]==low[u])
	{
		col[u]=++num;
		s[num]=1;
		while (stack[top]!=u)
		{
			col[stack[top]]=num;
			s[num]++;
			top--;
		}
		top--;
	}
}*/
void spfa(int id)
{
	for (int i=1; i<=n; i++) dis[id][i]=1e9+7,vis[i]=0;
	hd=tl=1;
	if (id==1) dis[id][1]=0,vis[1]=1,q[1]=1;
	else dis[id][n]=0,vis[n]=1,q[1]=n;
	while (hd<=tl)
	{
		int u=q[hd++];
		for (int i=head[u]; i; i=Next[i])
		{
			int v=vet[i];
			if (dis[id][v]>dis[id][u]+value[i])
			{
				dis[id][v]=dis[id][u]+value[i];
				if (!vis[v])
				{
					q[++tl]=v;
					vis[v]=1;
				}
			}
		}
		vis[u]=0;
	}
}
void dfs(int u,int len)
{
	for (int i=head[u]; i; i=Next[i])
	{
		int v=vet[i];
		if (dis[1][v]+len+value[i]>dis[1][n]+k) continue;
		dp[v][dis[1][v]+len+value[i]-dis[1][n]]=(dp[v][dis[1][v]+len+value[i]-dis[1][n]]+dp[u][dis[1][u]+len-dis[1][n]])%p;
		dfs(v,len+value[i]);
	}
}
bool solve(int u)
{
	vis[u]=1;
	for (int i=head[u]; i; i=Next[i])
	{
		int v=vet[i];
		if (vis[v]) return 1;
		if (solve(v)) return 1;
	}
	return 0;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1; i<=m; i++)
		{
			scanf("%d%d%d",&eu[i],&ev[i],&ew[i]);
			add(eu[i],ev[i],ew[i]);
		}
		spfa(1);
		cnt=0;
		for (int i=1; i<=n; i++) head[i]=0;
		for (int i=1; i<=m; i++) add(ev[i],eu[i],ew[i]);
		spfa(2);
		cnt=0; flag=0;
		for (int i=1; i<=n; i++) head[i]=0;
		for (int i=1; i<=m; i++)
			if (ew[i]==0) add(eu[i],ev[i],ew[i]);
		for (int i=1; i<=n; i++) vis[i]=0;
		for (int i=1; i<=n; i++)
			if (!vis[i])
			{
				if (solve(i) && dis[1][i]+dis[2][i]<=dis[1][n]+k)
				{
					flag=1;
					break;
				}
			}
		if (flag)
		{
			printf("-1\n");
			continue;
		}
		cnt=0;
		for (int i=1; i<=n; i++) head[i]=0;
		for (int i=1; i<=m; i++) add(ev[i],eu[i],ew[i]);
		dp[n][0]=1;
		dfs(n,0);
		for (int i=0; i<=k; i++) ans=(ans+dp[1][i])%p;
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

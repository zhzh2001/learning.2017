#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long LL;
LL A,B,_x,_y,mid,ans;
inline void Exgcd(LL a,LL b,LL &x,LL &y){
	if(b==0){ x=1; y=0; return; }
	Exgcd(b,a%b,y,x); y-=x*(a/b);
}
inline bool no_Sol(LL now){
	LL x=_x*now, y=_y*now;
	x=(x%B+B)%B; y=(now-A*x)/B;
	if(x>=0&&y>=0) return false;
	y=(y%A+A)%A; x=(now-B*y)/A;
	if(x>=0&&y>=0) return false;
	return true;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&A,&B); if(A>B) swap(A,B);
	Exgcd(A,B,_x,_y);
	for(int i=A-1;i<=A-1+10000000;i++) if(no_Sol(i)) ans=i;
	printf("%lld\n",ans);
	return 0;
}

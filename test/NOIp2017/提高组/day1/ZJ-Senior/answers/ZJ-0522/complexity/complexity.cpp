#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=1005;
int _test,Len,stk[maxn],top,ans;
char s_ret[200],s[105][50],numi[maxn];
bool vis[400],is_err;
int used[maxn];
int _Add(int x,int y){
	return max(x,y);
}
int _Mul(int x,int y){
	if(x==-1) return 0;
	return x+y;
}
void Work1(){
	int res=0;
	while(top&&!used[top]) res=_Add(res,stk[top--]);  
	if(!top){ is_err=true; return; }
	used[top]=false; stk[top]=_Mul(stk[top],res); vis[numi[top]]=false;
}
void Work2(char st[]){
	top++; used[top]=true;
	int L=0,R=0,p=0;
	p++; while(!('a'<=st[p]&&st[p]<='z')) p++;
	numi[top]=st[p]; if(vis[st[p]]){ is_err=true; return; } vis[st[p]]=true;
	p++; while(!(isdigit(st[p])||st[p]=='n')) p++;
	if(st[p]=='n') L=1e8; else{
		while(isdigit(st[p])) L=(L<<3)+(L<<1)+st[p]-'0', p++;
	}  
	p++; while(!(isdigit(st[p])||st[p]=='n')) p++;
	if(st[p]=='n') R=1e8; else{
		while(isdigit(st[p])) R=(R<<3)+(R<<1)+st[p]-'0', p++;
	}
	if(L>R) stk[top]=-1; else
	if(R-L+1>200) stk[top]=1;
	else stk[top]=0;
}
bool check(){
	int res=0,p=0,len=strlen(s_ret);
	while(p<len){
		if(s_ret[p]=='^'){
			p++;
			while(isdigit(s_ret[p])) res=(res<<3)+(res<<1)+s_ret[p]-'0', p++;
			break;
		}
		p++;
	}
	return res==ans;
}
int _Final(){
	for(int i=1;i<=top;i++) if(used[i]){ is_err=true; return 0; }
	int res=0;
	for(int i=1;i<=top;i++) res=_Add(res,stk[i]);
	return res;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&_test);
	for(int ii=1;ii<=_test;ii++){ 
		memset(stk,0,sizeof(stk));
		memset(vis,0,sizeof(vis)); top=0; is_err=false;
		memset(s,0,sizeof(s)); memset(s_ret,0,sizeof(s_ret));
		scanf("%d%s",&Len,s_ret); while(getchar()!='\n');
		for(int i=1;i<=Len;i++){
			int len=-1;
			for(char ch=getchar();ch!='\n'&&ch!=EOF;ch=getchar()) s[i][++len]=ch; 
		}
		for(int i=1;i<=Len;i++){
			if(s[i][0]=='E') Work1(); else Work2(s[i]);
			if(is_err) break;
		}
		if(Len==0) ans=0; else ans=_Final();
		if(is_err) printf("ERR\n"); else
		if(check()) printf("Yes\n"); else
		printf("No\n");
	}
	return 0;
}

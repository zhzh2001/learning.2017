#include<cstdio>
#include<queue>
#include<cstring>
#include<cctype>
#include<algorithm>
using namespace std;
const int maxn=100005,maxe=200005;
inline char gc(){
	static char _buf[100000],*p1=_buf,*p2=_buf;
	return p1==p2&&(p2=(p1=_buf)+fread(_buf,1,100000,stdin),p1==p2)?EOF:*p1++; 
}
inline int getint(){
	char ch=gc(); int res=0,ff=1;
	while(!isdigit(ch)) ch=='-'?ff=-1:0, ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res*ff;
}
int dis[maxn],INF;
int fir[maxn],nxt[maxe],son[maxe],w[maxe],tot;
int _test,n,m,K,P;
void add(int x,int y,int z){
	son[++tot]=y; w[tot]=z; nxt[tot]=fir[x]; fir[x]=tot;
}
bool vis0[maxn];
queue<int> Q;
void Spfa0(){
	memset(vis0,0,sizeof(vis0)); 
	memset(dis,63,sizeof(dis)); INF=dis[0]; dis[1]=0; Q.push(1);
	while(!Q.empty()){
		int x=Q.front(); Q.pop(); vis0[x]=false;
		for(int j=fir[x];j;j=nxt[j]) if(dis[x]+w[j]<dis[son[j]]){
			dis[son[j]]=dis[x]+w[j];
			if(!vis0[son[j]]) vis0[son[j]]=true, Q.push(son[j]);
 		}
	}
}
int f[maxn][52],ans;
int getF(int x,int y){
	if(y>K||dis[x]==INF) return 0;
	if(f[x][y]!=-1) return f[x][y];
	f[x][y]=0;
	for(int j=fir[x];j;j=nxt[j]) (f[x][y]+=getF(son[j],y+dis[x]-dis[son[j]]-w[j]))%=P;
	return f[x][y];
}
struct data{ int x,y,z; } Es[maxe];
bool hh=0;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	_test=getint();
	while(_test--){
		hh=0;
		n=getint(); m=getint(); K=getint(); P=getint();
		memset(fir,0,sizeof(fir)); tot=0;
		for(int i=1;i<=m;i++){
			int x=getint(),y=getint(),z=getint();
			add(x,y,z);
			Es[i].x=x; Es[i].y=y; Es[i].z=z; if(Es[i].z==0) hh=1;
		}
		Spfa0();
		memset(fir,0,sizeof(fir)); tot=0;
		for(int i=1;i<=m;i++) add(Es[i].y,Es[i].x,Es[i].z);
		memset(f,255,sizeof(f)); f[1][0]=1;
		ans=0; for(int i=0;i<=K;i++) (ans+=getF(n,i))%=P;
		if(hh) printf("-1\n"); else printf("%d\n",ans);
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i,a,b) for (int i = a;i <= b;++i)
#define drep(i,a,b) for (int i = a;i >= b;--i)
#define N 111
using namespace std;
int T,n,cp,np,ty[N],va[N],X[N][2],Y[N][2],stk[N],flag,b[N],ans[N];
char cplx[200],s1[200];

int check(int x)
{
	if (!X[x][0] && !Y[x][0]) return 0;
	if (!X[x][0]) return -1;
	if (!Y[x][0]) return 1;
	if (X[x][1] <= Y[x][1]) return 0;
	else return -1;
}
int add(int x,int y)
{
	if (x == -1 || y == -1) return -1;
	return x+y;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (scanf("%d",&T);T--;)
	{
		scanf("%d%s",&n,cplx+1);
	// reading	
		cp = np = flag = 0;
		if (cplx[3] != '1')
		{
			cp = 1; int len = strlen(cplx+1)-1;
			rep(i,5,len) np = np*10+(cplx[i]-'0');
		}
		rep(i,1,n)
		{
			scanf("%s",s1+1); ty[i] = 0;
			if (s1[1] == 'F')
			{
				ty[i] = 1;
				scanf("%s",s1+1); va[i] = s1[1]-97;
				scanf("%s",s1+1);
				X[i][0] = X[i][1] = 0;
				if (s1[1] != 'n')
				{
					X[i][0] = 1;int len = strlen(s1+1);
					rep(j,1,len) X[i][1] = X[i][1]*10+(s1[j]-'0');
				}
				scanf("%s",s1+1);
				Y[i][0] = Y[i][1] = 0;
				if (s1[1] != 'n')
				{
					Y[i][0] = 1;int len = strlen(s1+1);
					rep(j,1,len) Y[i][1] = Y[i][1]*10+(s1[j]-'0');
				}
			//	printf("%d %d %d %d %d %d\n",ty[i],va[i],X[i][0],X[i][1],Y[i][0],Y[i][1]);
			}
		}
	//	printf("%d %d\n",cp,np);
		memset(ans,0,sizeof(ans));
		memset(b,0,sizeof(b));
		int len = 0;
		rep(i,1,n)
		{
			if (ty[i])
			{
				stk[++len] = i;
				if (b[va[i]]) { flag = 1; break; }
				b[va[i]] = 1;
			}
			else
			{
				if (len == 0) { flag = 1; break; }
				int v = check(stk[len]);
				rep(j,len+1,100) v = add(v,ans[j]);
				ans[len] = max(ans[len],v);
				rep(j,len+1,100) ans[j] = 0;
				b[va[stk[len]]] = 0;
				len--;
			}
		}
		if (flag || len > 0) puts("ERR");
		else if (cp == 0)
		{
			if (ans[1] <= 0) puts("Yes");
			else puts("No");
		}
		else
		{
			if (ans[1] == np) puts("Yes");
			else puts("No");
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


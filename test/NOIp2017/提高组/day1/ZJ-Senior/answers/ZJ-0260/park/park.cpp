#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
#define rep(i,a,b) for (int i = a;i <= b;++i)
#define drep(i,a,b) for (int i = a;i >= b;--i)
#define N 100010
#define M 200010
#define ll long long
using namespace std;
const ll inf = 1e12;

ll d[N];
bool vis[N];
int h[M],v[M],last[M],head[N],l,f[N],c[N],g[N][55];
int n,m,k,zhl,T,x,y,z,ans;
queue<int> q;
void add(int x,int y,int z)
{
	h[++l] = y;
	v[l] = z;
	last[l] = head[x];
	head[x] = l;
}
void spfa()
{
	rep(i,2,n) d[i] = inf;
	d[1] = 0; q.push(1); vis[1] = 1;
	for (;!q.empty();q.pop())
	{
		int x = q.front();
		for (int i = head[x],y;i;i = last[i])
			if (d[x]+v[i] < d[y=h[i]])
			{
				d[y] = d[x]+v[i];
				if (!vis[y]) vis[y] = 1,q.push(y);
			}
		vis[x] = 0;
	}
}
void Add(int& x,int y)
{
	x += y;
	if (x >= zhl) x -= zhl;
}
bool cmp(int x,int y)
{
	return d[x] == d[y]?x < y:d[x] < d[y];
}

void solve1()
{
	f[1] = 1;
	rep(i,1,n) c[i] = i;
	sort(c+1,c+n+1,cmp);
	rep(x,1,n) for (int i = head[c[x]],y;i;i = last[i])
		if (d[y=h[i]] == d[c[x]]+v[i]) Add(f[y],f[c[x]]);
	printf("%d\n",f[n]);
}
void solve2()
{
	g[1][0] = 1;
	rep(i,1,n) c[i] = i;
	sort(c+1,c+n+1,cmp);
	rep(x,1,n) for (int i = head[c[x]],y;i;i = last[i])
		rep(K,0,k) if (K+d[c[x]]-d[y=h[i]]+v[i] <= k)
			Add(g[y][K+d[c[x]]-d[y]+v[i]],g[c[x]][K]);
	int ans = 0;
	rep(K,0,k) Add(ans,g[n][K]);
	printf("%d\n",ans);
}
void dfs(int x,int dis)
{
//	printf("%d %d\n",x,dis);
	if (dis > d[n]+k) return;
	if (x == n) { Add(ans,1); return; }
	for (int i = head[x];i;i = last[i])
		dfs(h[i],dis+v[i]);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (scanf("%d",&T);T--;)
	{
		scanf("%d%d%d%d",&n,&m,&k,&zhl);
		rep(i,1,m)
		{
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		spfa(); ans = 0;
	//	printf("%lld\n",d[n]);
		if (k == 0) solve1();
		else if (m <= 200) { dfs(1,0); printf("%d\n",ans); }
		else solve2();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


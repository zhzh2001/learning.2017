#include <cstdio>
#include <cstring>
#include <algorithm>
#define ll long long
using namespace std;
ll a,b;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a > b) swap(a,b);
	if (a == 1) puts("0");
	else
	{
		ll ans = (a+1)*(a-2)+1;
		printf("%lld\n",ans+(b-a-1)*(a-1));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}



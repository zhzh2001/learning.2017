#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
long long a,b;
bool f[30000100];
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d%d",&a,&b);
	if (a<b){
		long long t=a;
		a=b;
		b=t;
	}
	long long i=1,tot=0;
	f[0]=true;
	while (true){
		if (i-a>=0)
			f[i]=f[i]||f[i-a];
		if (i-b>=0)
			f[i]=f[i]||f[i-b];
		if (((i-a+b)>=0)&&((i-a+b)%a!=0))
			f[i]=f[i]||f[i-a+b];
		if (f[i])
			tot++;
		else tot=0;
		if (tot==a-b)
			break;
		i++;
	}
	printf("%lld\n",i-a+b);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
struct edge{
	int ed,jump,val,mark;
}a[200005];
int T,jump[100005],n,m,K,P,que[1000005],judge[100005],num,mina,poi[100005],len[100005];
void SPFA(){
	int head=0,tail=1;
	que[1]=1;
	judge[1]=1;
	while (head!=tail){
		head++;
		if (head>1000000) head=1;
		int pos=que[head];
		for (int i=jump[pos]; i; i=a[i].jump){
			if (a[i].mark) continue;
			if(len[a[i].ed]>len[pos]+a[i].val){
				len[a[i].ed]=len[pos]+a[i].val;
				if (!judge[a[i].ed]){
					judge[a[i].ed]=1;
					tail++;
					if (tail>1000000) tail=1;
					que[tail]=a[i].ed;
				}
			}
		}
		judge[pos]=0;
	}
}
void dfs(int pos,int asd){
	if (pos==n){
		num++;
		return ;
	}
	for (int i=jump[pos]; i; i=a[i].jump){
		if (!judge[a[i].ed] && asd+a[i].val<=mina+K){
			judge[a[i].ed]=1;
			dfs(a[i].ed,asd+a[i].val);
			judge[a[i].ed]=0;
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		num=0;
		memset(jump,0,sizeof(jump));
		scanf("%d%d%d%d",&n,&m,&K,&P);
		for (int i=1,x,y,z; i<=m; i++){
			scanf("%d%d%d",&x,&y,&z);
			a[i].ed=y;
			a[i].val=z;
			a[i].jump=jump[x];
			a[i].mark=0;
			jump[x]=i;
		}
		memset(poi,0,sizeof(poi));
		memset(len,127,sizeof(len));
		memset(judge,0,sizeof(judge));
		SPFA();
		mina=len[n];
		if (mina==0){
			printf("-1\n");
			continue;
		}
		dfs(1,0);
		printf("%d\n",num);
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
using namespace std;
long long a,b,ans,now;
void exgcd(long long p,long long q,long long &x,long long &y){
	if(!p) {x=0,y=1;return;}exgcd(q%p,p,x,y);
	int ax=y-(q/p)*x;y=x;x=ax;
}
long long uop(long long p,long long q,long long r){
	double pp=p,qq=q,rr=r;
	return (long long)ceil(pp*rr/qq);
}
long long don(long long p,long long q,long long r){
	double pp=p,qq=q,rr=r;
	return (long long)floor(pp*rr/qq);
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a>b) swap(a,b);
	if(b>1e6) printf("%lld",a*b);
	long long x,y;exgcd(a,b,x,y);//printf("%lld %lld\n",x,y);
	if(y<0) swap(x,y),swap(a,b);x=-x;long long lc=-1;
	for(long long i=1;i<=a*b;i++){
		long long lf=uop(a,y,i),rt=b*i/x;
		if(lf>lc+1) ans=lf-1,now=0;
		now+=rt-lf+1;if(now>=min(a,b)) break;lc=rt;
	}
	/*ans=a-1;for(long long i=a;i<=a*b;i++){
		if(uop(x,b,i)<=y*i/a) now++;
		else ans=i,now=0;
		if(now==min(a,b)) break;
	}*/
	printf("%lld",ans);
	return 0;
}

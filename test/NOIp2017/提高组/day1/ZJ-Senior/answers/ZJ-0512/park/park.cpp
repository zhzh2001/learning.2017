#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int t,n,m,k,p,cnt,h[100001],q[10000001][2],dis[100001],hd,tl,md,tot[100001];
struct ed{int ne,ds,vv;} l[200001];
int inq[100001];
void add(int x,int y,int z){
	l[++cnt].ne=h[x];l[cnt].ds=y;l[cnt].vv=z;h[x]=cnt;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);	
	while(t--){
		scanf("%d%d%d%d",&n,&m,&k,&p);int type=0;cnt=0;
		for(int i=1;i<=n;i++) h[i]=0,inq[i]=0,tot[i]=0;
		for(int i=1;i<=m;i++){
			int x,y,z;scanf("%d%d%d",&x,&y,&z);add(x,y,z);
			if(!z) type=1;
		}dis[1]=0;tl=0;hd=0;q[++tl][0]=1;q[tl][1]=1;inq[1]=1;tot[1]=1;
		if(type==1) {printf("-1\n");continue;}
		for(int i=2;i<=n;i++) dis[i]=1e9;
		while(hd<tl){
			int u=q[++hd][0],pl=q[hd][1];inq[u]=0;
			for(int i=h[u];i!=0;i=l[i].ne){
				int v=l[i].ds;
				if(dis[u]+l[i].vv==dis[v]){
					tot[v]+=pl;if(!inq[v]) q[++tl][0]=v,q[tl][1]=pl,inq[v]=tl;
					else q[inq[v]][1]+=pl;
				}
				if(dis[u]+l[i].vv<dis[v]){
					dis[v]=dis[u]+l[i].vv;tot[v]=pl;
					if(!inq[v]) q[++tl][0]=v,q[tl][1]=pl,inq[v]=tl;
					else q[inq[v]][1]=pl;
				}	
			}
		}md=dis[n];printf("%d\n",tot[n]%p);
	}
	return 0;
}

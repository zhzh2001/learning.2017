#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
int t,l,oo,now,top,ans;
struct node{char p;int lf,rt;bool us;}sk[1001];
char s[101];
bool ex[101],er,us;
int tran(){
	if(s[0]=='n') return -1;
	int len=strlen(s),num=0;
	for(int i=0;i<len;i++) num=num*10+s[i]-48;
	return num;
}
int calc(int a,int b){
	if(a==-1&&b!=-1) return -1;
	if(a==-1&&b==-1) return 0;
	if(a!=-1&&b==-1) return 1;
	if(a>b) return -1;else return 0;
}	
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d",&l);char c;memset(ex,0,sizeof(ex));er=0;ans=0;top=0;sk[0].us=0;now=0;
		for(int i=0;i<4;i++) scanf("%c",&c);
		if(c=='1') {oo=0;scanf("%c",&c);}else{
			scanf("%c",&c);oo=0;scanf("%c",&c);
			while(c!=')'){
				oo=oo*10+c-48;scanf("%c",&c);
			}
		}scanf("%c",&c); 
		for(int i=1;i<=l;i++){
			scanf("%s",&s);if(s[0]=='F'){
				scanf("%s",&s);if(ex[s[0]-'a']){er=1;}
				char op=s[0];scanf("%s",&s);int st=tran();
				scanf("%s",&s);int ed=tran();ex[op-'a']=1;
				bool isn=sk[top].us;int sth=calc(st,ed);if(sth==-1) isn=1;
				sk[++top].p=op;sk[top].lf=st;sk[top].rt=ed;sk[top].us=isn;
				if(!isn) now+=sth;ans=max(ans,now);
			}else{
				bool isn=sk[top].us;int sth=calc(sk[top].lf,sk[top].rt);if(!isn) now-=sth;
				ex[sk[top].p-'a']=0;top--;
			}
		}
		if(er||top!=0) {printf("ERR\n");continue;}//printf("%d",ans);
		if(ans==oo) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

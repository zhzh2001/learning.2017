#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <cstdlib>
using namespace std;

long long x , y;

int main(void) {
	freopen("math.in" , "r" , stdin);
	freopen("math.out" , "w" , stdout);
	scanf("%lld%lld" , &x , &y);
	if(x > y) swap(x , y);
	printf("%lld\n" , min(y * (x - 1) - x , (y - 1) * x - y));
}

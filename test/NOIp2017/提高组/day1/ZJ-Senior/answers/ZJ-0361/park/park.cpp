#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <queue>
#include <set>
#include <vector>
using namespace std;

const int N = 1e5 + 5;
const int M = N * 4;
const int K = 55;

int fir[N] , ne[M] , to[M] , cnt , dis[N] , dis2[N] , dp[N][K] , mod , n , m , k , x , y , z , C[M];
int Tp[N] , Tp_clock , du[N];

vector<int>G[N];
vector<int>T[N] , val[N];

struct NODE {
	int no, Dis;
	friend bool operator < (NODE xxx , NODE yyy) {
		if(xxx.Dis == yyy.Dis) {
			return Tp[xxx.no] > Tp[yyy.no];
		}
		return xxx.Dis > yyy.Dis;
	}
};

priority_queue<NODE>q;

bool inq[N][K] , vis[N];

void dijkstra(void);
void dothat(void);

void init(void) {
	for(int i = 1;i <= n;++ i) fir[i] = 0 , vis[i] = 0 , du[i] = Tp[i] = 0;
	cnt = Tp_clock = 0;
	for(int i = 1;i <= n;++ i) {
		dis[i] = 1e9; G[i].clear();
		for(int j = 0;j <= k;j ++) dp[i][j] = 0 , inq[i][j] = 0;
	}
}

void add(int x , int y , int z) {
	if(!z) G[x].push_back(y) , du[y] ++;
	T[y].push_back(x); val[y].push_back(z);
	ne[++ cnt] = fir[x]; fir[x] = cnt; to[cnt] = y; C[cnt] = z;
}

#define Foreachson(i , x) for(int i = fir[x];i;i = ne[i])
#define Df(no, Dis) (NODE){no , Dis}

void dijkstra2(void) {
	for(int i = 1;i <= n;++ i) vis[i] = 0;
	while(!q.empty()) q.pop();
	q.push(Df(n, 0));
	while(!q.empty()) {
		while(!q.empty() && vis[q.top().no]) q.pop();
		if(q.empty()) break;
		NODE ind = q.top(); q.pop();
		vis[ind.no] = 1; dis2[ind.no] = ind.Dis;
		for(int i = 0;i <(int) T[ind.no].size(); ++ i) {
			int V = T[ind.no][i];
			if(vis[V]) continue;
			q.push(Df(V , dis2[ind.no] + val[ind.no][i]));
		}
	}
}

void dijkstra(void) {
	while(!q.empty()) q.pop();
	q.push(Df(1 , 0));
	while(!q.empty()) {
		while(!q.empty() && vis[q.top().no]) q.pop();
		if(q.empty()) break;
		NODE ind = q.top(); q.pop();
		vis[ind.no] = 1; dis[ind.no] = ind.Dis;
		Foreachson(i , ind.no) {
			int V = to[i];
			if(vis[V]) continue;
			q.push(Df(V , dis[ind.no] + C[i]));
		}
	}
}

void dothat(void) {
	dp[1][0] = 1;
	while(!q.empty()) q.pop();
	q.push(Df(1 , 0));
//	for(int i = 1;i <= n;i ++) cerr << dis2[i] <<" ";
//	puts("");
	inq[1][0] = 1;
	while(!q.empty()) {
		NODE ind = q.top(); q.pop();
		Foreachson(i , ind.no) {
			int V = to[i];
			if(ind.Dis + C[i] + dis2[V] > dis[n] + k) continue;
			dp[V][ind.Dis + C[i] - dis[V]] += dp[ind.no][ind.Dis - dis[ind.no]];
			dp[V][ind.Dis + C[i] - dis[V]] %= mod;
			if(!inq[V][ind.Dis + C[i] - dis[V]]) {
				inq[V][ind.Dis + C[i] - dis[V]] = 1;
				q.push(Df(V , ind.Dis + C[i]));
			}
		}
	}
	int ans = 0;
	for(int i = 0;i <= k;++ i) {
		ans += dp[n][i];
		ans %= mod;
	}
	printf("%d\n" , ans);
}


bool visT[N];

bool toposort(void) {
	queue<int>q;
	while(!q.empty())q.pop();
	for(int i = 1;i <= n;++ i) if(!du[i]) {
		Tp[i] = ++ Tp_clock;
		q.push(i);
	}
	int ind , V;
	while(!q.empty()) {
		ind = q.front(); q.pop();
		for(register int i = 0;i < (int) G[ind].size(); ++ i) {
			V = G[ind][i];
			du[V] --;
			if(du[V] == 0) {
				q.push(V); Tp[V] = ++ Tp_clock;
			}
		}
	}
	if(Tp_clock != n) {
		return 0;
	}
	return 1;
}

int main(void) {
	freopen("park.in" , "r" , stdin);
	freopen("park.out" , "w" , stdout);
	int t;
	for(scanf("%d" , &t);t--;) {
		scanf("%d%d%d%d" , &n , &m , &k , &mod);
		init();
		for(int i = 1;i <= m;++ i) {
			scanf("%d%d%d" , &x , &y , &z);
			add(x , y , z);
		}
		dijkstra();
		dijkstra2();
		if(!toposort()) {
			puts("-1");
			continue;
		}
		dothat();
	}
}

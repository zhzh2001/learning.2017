#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;

const int N = 2005;

int t , stk[N] , cur[N] , ans , L , his , top , ru[N];

bool in[N];

char s[N] , I[N] , X[N] , Y[N];

int getnum(char *S) {
	int len = strlen(S);
	int res = 0;
	for(int i = 0;i < len;i ++) {
		if(S[i] >= '0' && S[i] <= '9') {
			res *= 10; res += S[i] - '0';
		}
	}
	return res;
}

void read(void) {
	cin >> L >> s;
	int len = strlen(s);
	int it = 0;
	while(s[it] != 'O') it ++;
	while(s[it] != '(') it ++;
	it ++;
	if(s[it] == '1') {
		his = 0;
	}
	else {
		while(s[it] != '^') it ++;
		it ++;
		his = getnum(s);
	}
	top = 0;
	memset(stk , 0 , sizeof(stk));
	memset(cur , 0 , sizeof(cur));
	memset(in , 0 , sizeof(in));
	memset(ru , 0 , sizeof(ru));
	ru[0] = 1;
	ans = 0;
}


void doit(void) {
	bool flag = 0;
	for(int i = 1;i <= L;i ++) {
		char ch = ' ';
		cin >> s;
		if(strcmp(s , "F") && strcmp(s , "E")) {
			cin.getline(s , 1000000);
			continue;
		}
		if(!strcmp(s , "F")) {
			cin >> I >> X >> Y;
			char wh = I[0];
			++ top; stk[top] = 0;
			if(in[wh]) {
				flag = 1;
			}
			in[wh] = 1;
			stk[top] = wh;
			cur[top] = cur[top - 1];
			if(!ru[top - 1]) {
				ru[top] = 0;
				continue;
			}
			if(!strcmp(X , "n")) {
				if(!strcmp(Y , "n")) {
					ru[top] = 1;
				}
				else {
					ru[top] = 0;
				}
			}
			else {
				if(!strcmp(Y , "n")) {
					cur[top] ++;
					ru[top] = 1;
				}
				else {
					int x = getnum(X) , y = getnum(Y);
					if(x > y) {
						ru[top] = 0;
					}
					else {
						ru[top] = 1;
					}
				}
			}
		}
		else {
			if(!top) {
				flag = 1;
				top = 1000;
			}
			if(ru[top])
			ans = max(ans , cur[top]);
			in[stk[top]] = 0;
			top --;
		}
	}
	if(flag) {
		puts("ERR");
		return;
	}
	if(!top) {
		if(ans == his) {
			puts("Yes");
		}
		else puts("No");
	}
	else puts("ERR");
}

int main(void) {
	freopen("complexity.in" , "r" , stdin);
	freopen("complexity.out" , "w" , stdout);
	for(scanf("%d" , &t);t --;) {
		read();
		doit();
	}
}

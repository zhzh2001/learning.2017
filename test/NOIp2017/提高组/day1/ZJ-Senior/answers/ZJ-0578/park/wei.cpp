#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
#define M 200005
#define K 55

int T,n,m,k,p,cnt,u,v,len,i,ans,l,r,pd;
int head[N],dis[N],q[N*K],d[N*K],vis[N],vi[N][K],f[N][K],num[N][K];
struct ff{int to,nxt,len;}e[2*M];

void add(int u,int v,int len){
	e[++cnt]=(ff){v,head[u],len};
	head[u]=cnt;
}
void SPFA(){
	memset(dis,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	l=0;r=1;q[1]=1;dis[1]=0;vis[1]=1;
	while(l!=r){
		l++;if(l>n)l=1;
		for(int i=head[q[l]];i;i=e[i].nxt){
			int v=e[i].to;
			if(dis[q[l]]+e[i].len<dis[v]){
				dis[v]=dis[q[l]]+e[i].len;
				if(!vis[v]){
					r++;if(r>n)r=1;
					q[r]=v;vis[v]=1;
				}
			}
		}vis[q[l]]=0;
	}
}
void dp(){
	memset(num,0,sizeof(num));
	memset(f,0,sizeof(f));
	memset(vi,0,sizeof(vi));
	q[1]=1,d[1]=0;l=0,r=1;
	f[1][0]=1;vi[1][0]=1;
	int Time=0;
	while(l!=r){
		l++;if(l>n*(k+1))l=1;
		for(int i=head[q[l]];i;i=e[i].nxt){
			int v=e[i].to;int now=dis[q[l]]+d[q[l]]+e[i].len-dis[v];
			if(now<=k){
				f[v][now]=(f[v][now]+f[q[l]][d[l]])%p;
				if(!vi[v][now]){//++Time;if(Time%100000==0)printf("%d\n",Time);
					num[v][now]++;if(num[v][now]>n){pd=1;puts("-1");return;}
					r++;if(r>n*(k+1))r=1;
					q[r]=v,d[r]=now;vi[v][now]=1;
				}
			}
		}vi[q[l]][d[l]]=0;
	}
}
int main()
{
	freopen("park-.in","r",stdin);
//	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(head,0,sizeof(head));cnt=0;
		for(i=1;i<=m;i++){
			scanf("%d%d%d",&u,&v,&len);
			add(u,v,len);
		}SPFA();printf("%d\n",dis[n]);
		pd=0;dp();
		if(pd)continue;
		ans=0;for(i=0;i<=k;i++)ans=(ans+f[n][i])%p;
		printf("%d\n",ans);
	}return 0;
}

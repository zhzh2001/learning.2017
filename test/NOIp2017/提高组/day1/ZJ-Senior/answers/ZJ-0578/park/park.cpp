#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
#define M 200005
#define K 55

int T,n,m,k,p,cnt,cnt1,u,v,len,i,ans,l,r,pd;
int head[N],dis[N],disn[N],q[N*K],d[N*K],vis[N],vi[N],f[N],num[N],head1[N];
struct ff{int to,nxt,len;}e[2*M],e1[2*M];

void add(int u,int v,int len){
	e[++cnt]=(ff){v,head[u],len};
	head[u]=cnt;
}
void add1(int u,int v,int len){
	e1[++cnt1]=(ff){v,head1[u],len};
	head1[u]=cnt1;
}
void SPFAn(){
	memset(disn,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	l=0;r=1;q[1]=n;disn[n]=0;vis[n]=1;
	while(l!=r){
		l++;if(l>n)l=1;
		for(int i=head1[q[l]];i;i=e1[i].nxt){
			int v=e1[i].to;
			if(disn[q[l]]+e1[i].len<disn[v]){
				disn[v]=disn[q[l]]+e1[i].len;
				if(!vis[v]){
					r++;if(r>n)r=1;
					q[r]=v;vis[v]=1;
				}
			}
		}vis[q[l]]=0;
	}
}
void SPFA(){
	memset(dis,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	l=0;r=1;q[1]=1;dis[1]=0;vis[1]=1;
	while(l!=r){
		l++;if(l>n)l=1;
		for(int i=head[q[l]];i;i=e[i].nxt){
			int v=e[i].to;
			if(dis[q[l]]+e[i].len<dis[v]){
				dis[v]=dis[q[l]]+e[i].len;
				if(!vis[v]){
					r++;if(r>n)r=1;
					q[r]=v;vis[v]=1;
				}
			}
		}vis[q[l]]=0;
	}
}
void dp(){
	memset(num,0,sizeof(num));
	memset(f,0,sizeof(f));
	memset(vi,0,sizeof(vi));
	q[1]=1;l=0,r=1;
	vi[1]=1;f[1]=1;
	while(l!=r){
		l++;if(l>n*(k+1))l=1;
		for(int i=head[q[l]];i;i=e[i].nxt){
			int v=e[i].to;
			if(dis[v]+disn[v]-dis[n]>k)continue;
			f[v]=(f[v]+f[q[l]])%p;
			if(!(--d[v])){
				r++;if(r>n*(k+1))r=1;
				q[r]=v;vi[v]=1;
			}
		}vi[q[l]]=0;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(head,0,sizeof(head));cnt=0;
		memset(head1,0,sizeof(head1));cnt1=0;
		for(i=1;i<=m;i++){
			scanf("%d%d%d",&u,&v,&len);
			add(u,v,len);add1(v,u,len);
			d[v]++;
		}if(k!=0){puts("-1");return 0;}
		SPFA();//printf("%d\n",dis[n]);
		SPFAn();
		pd=0;dp();
		if(pd)continue;
		printf("%d\n",f[n]);
	}return 0;
}

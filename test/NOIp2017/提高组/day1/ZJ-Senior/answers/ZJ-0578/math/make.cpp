#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,f[10000005];
int gcd(int a,int b){return !b?a:gcd(b,a%b);}
int work(int x,int y){
	memset(f,0,sizeof(f));
	f[0]=1;
	for(int i=1;i<=10000000;i++){
		if(i>=x)f[i]|=f[i-x];
		if(i>=y)f[i]|=f[i-y];
	}
	for(int i=10000000;i;i--)
		if(!f[i])return i;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("make.out","w",stdout);
	scanf("%d%d",&n,&m);
	printf("%d\n",work(n,m));
	return 0;
}

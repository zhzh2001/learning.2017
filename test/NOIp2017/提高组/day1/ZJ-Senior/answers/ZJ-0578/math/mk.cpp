#include<stdio.h>
#include<ctime>
#include<algorithm>
using namespace std;

int gcd(int a,int b){return !b?a:gcd(b,a%b);}
int main()
{
	freopen("math.in","w",stdout);
	srand((int)time(0));
	int n=rand()%3000+2,m=rand()%3000+2;
	while(gcd(n,m)!=1)n=rand()%3000+2,m=rand()%3000+2;
	printf("%d %d\n",n,m);
	return 0;
}

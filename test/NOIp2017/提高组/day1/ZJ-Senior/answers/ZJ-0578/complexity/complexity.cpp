#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 105

int T,n,w,i,deep,ans,now,CE,num;
int a[N],l[N],r[N],b[N],vis[200];
char c,s[N],ss[N];

int work(int x){
	int res=1;
	while(res<=x)res*=10;
	return res;
}
void jump(){
	num=-1;i++;
	while(i<=n&&num<0){
		if(a[i]==0){num++;
		vis[ss[deep--]]=0;
		i++;continue;
		}num--;
		if(vis[s[i]]){CE=1;break;}
		ss[++deep]=s[i];vis[s[i]]=1;
		i++;
	}
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		getchar();getchar();getchar();
		c=getchar();
		if(c=='1')w=0;
			else getchar(),scanf("%d",&w);
		getchar();scanf("\n");
		for(i=1;i<=n;i++){
			c=getchar();
			if(c=='E')a[i]=0;
				else{a[i]=1;
					getchar();s[i]=getchar();
					getchar();c=getchar();
					l[i]=r[i]=-1;
					if(c>47&&c<58){
						l[i]=c-48;c=getchar();
						while(c>47&&c<58)l[i]=l[i]*10+c-48,c=getchar();
					}else getchar();
					c=getchar();
					if(c>47&&c<58){
						r[i]=c-48;c=getchar();
						while(c>47&&c<58)r[i]=r[i]*10+c-48,c=getchar();
					}
			}scanf("\n");
		}
		memset(vis,0,sizeof(vis));
		memset(ss,0,sizeof(ss));
		deep=ans=now=0;CE=0;i=1;
		while(i<=n){
			if(a[i]==0){
				if(b[deep])now--;b[deep]=0;
				vis[ss[deep--]]=0;
				if(deep<0){CE=1;break;}
				i++;continue;
			}
			if(vis[s[i]]){CE=1;break;}
			ss[++deep]=s[i];vis[s[i]]=1;
			if(l[i]==-1){
				if(r[i]==-1){i++;continue;}
				jump();
			}else{
				if(r[i]==-1){b[deep]=1,now++,ans=max(ans,now);i++;continue;}
				if(l[i]>r[i])jump();
					else i++;
			}if(CE)break;
			
		}if(deep!=0)CE=1;
		if(CE)puts("ERR");
			else if(ans==w)puts("Yes");
				else puts("No");
	}return 0;
}

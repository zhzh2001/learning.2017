program math;
var
  a,b,max,temp:int64;
  i,j:longint;
  pd:boolean;
function ma(a,b:longint):longint;
begin
  if a>b then ma:=a else ma:=b;
end;
function mi(a,b:longint):longint;
begin
 if a<b then mi:=a else mi:=b;
end;
begin
assign(input,'math.in');reset(input);
assign(output,'math.out');rewrite(output);
 readln(a,b);
 for i:=a*b downto 1 do
  begin
  pd:=false;
   for j:=(i div ma(a,b)) downto 0 do
    begin
     temp:=i-j*ma(a,b);
     if temp mod mi(a,b)=0 then
      begin
       pd:=true;
       break;
      end;
    end;
    if pd=false then begin max:=i;break; end;
  end;
 writeln(max);
 close(input);
 close(output);
end.
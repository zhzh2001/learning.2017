program complexity;
var
  n,temp,stack,i,j,l,stacktemp:longint;
  chtemp:char;
  s:array[0..100] of string;
  ch:array['a'..'z']of boolean;
  stemp,sans,sttemp,ll:string;
  stc:array[-100..100]of char;
  tt,mm:char;
begin
 assign(input,'complexity.in');reset(input);
 assign(output,'complexity.out');rewrite(output);
 readln(n);
 for i:=1 to n do
  begin
   readln(stemp);
   for j:=1 to length(stemp) do
    if stemp[j]=' ' then
     begin temp:=j; break;
    end;
   if temp=2 then l:=ord(stemp[1])-48;
   if temp=3 then l:=(ord(stemp[1])-48)*10+ord(stemp[2])-48;
   if temp=4 then l:=(ord(stemp[1])-48)*100+(ord(stemp[2])-48)*10+ord(stemp[3])-48;
   for j:=1 to l do
    s[j]:='';
   for j:=1 to l do
    readln(s[j]);
    stack:=0;
   for j:=1 to l do
    begin
     sttemp:=s[j];
     if sttemp[1]='F' then inc(stack);
     if sttemp[1]='E' then dec(stack);
    end;
   if stack<>0 then sans:='ERR';
   for chtemp:='a' to 'z' do
    ch[chtemp]:=false;
    stack:=0;
   for j:=1 to l do
    begin
     sttemp:=s[j];
     if sttemp[1]='F' then
      begin
       inc(stack);stc[stack]:=sttemp[3];
       if ch[sttemp[3]] then
        sans:='ERR'
       else ch[sttemp[3]]:=true;
      end;
    if sttemp[1]='E' then
     begin
      if stack<=0 then begin sans:='ERR'; break; end;
      tt:=stc[stack];
      dec(stack);
      ch[tt]:=false;
     end;
    end;
    if sans<>'ERR' then
     begin
      stack:=0;
      for j:=1 to l do
       begin
        stacktemp:=stack;
        sttemp:=s[j];
        if(sttemp[5]='n')and (sttemp[7]<>'n') and (length(sttemp)>=9) then inc(stack);
        if(sttemp[5]<>'n')and (sttemp[length(sttemp)]='n') and (length(sttemp)<=8) then inc(stack);
        if stacktemp=stack then break;
       end;
      if stack=0 then sans:='O(1)' else sans:='O(n^'+chr(48+stack)+')';
      sans:=chr(l+48)+' '+sans;
      if stemp=sans then writeln('Yes') else writeln('No');
     end else writeln('ERR');
     sans:=''
     end;
close(input);close(output);
end.

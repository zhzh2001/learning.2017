#include<bits/stdc++.h>
#define inf 1000000000
#define N 200009
#define M 500009
using namespace std;

int n,m,lim,mod,cnt,dfsclk,tp,dmn,q[N];
int d[2][N],id[N],dfn[N],low[N],scc[N],p[N],f[N][59];
bool flag,bo[N],vis[N];
int read(){
	int x=0; char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x;
}
void ad(int &x,int y){ x+=y; if (x>=mod) x-=mod; }
struct node{ int x,y; }mn[M];
bool operator <(node u,node v){ return u.x<v.x; }
void build(int k,int l,int r){
	mn[k]=(node){inf,l};
	if (l==r){
		id[l]=k; return;
	}
	int mid=l+r>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r);
}
void mdy(int x,int y){
	int i=id[x]; mn[i]=(node){y,x};
	for (i>>=1; i; i>>=1) mn[i]=min(mn[i<<1],mn[i<<1|1]);
}
struct praph{
	int tot,fst[N],pnt[M],len[M],nxt[M];
	void clr(){
		tot=0; memset(fst,0,sizeof(fst));
	}
	void add(int x,int y,int z){
		pnt[++tot]=y; len[tot]=z; nxt[tot]=fst[x]; fst[x]=tot;
	}
	void dijk(int *d,int sta){
		build(1,1,n); mdy(sta,0); d[sta]=0;
		memset(bo,1,sizeof(bo));
		int i,x,y;
		while (mn[1].x<inf){
			x=mn[1].y;
			mdy(x,inf); bo[x]=0;
			for (i=fst[x]; i; i=nxt[i]){
				y=pnt[i];
				if (bo[y] && d[x]+len[i]<d[y]){
					d[y]=d[x]+len[i]; mdy(y,d[y]);
				}
			}
		}
	}
}g0,g1,g2;
void tar(int x){
	int i,y,tmp=0; dfn[x]=low[x]=++dfsclk; q[++tp]=x;
	for (i=g2.fst[x]; i; i=g2.nxt[i]){
		y=g2.pnt[i];
		if (!dfn[y]){
			tar(y); low[x]=min(low[x],low[y]);
		} else if (!scc[y]) low[x]=min(low[x],dfn[y]);
	}
	if (low[x]==dfn[x]){
		for (cnt++; ; ){
			scc[q[tp]]=cnt; tmp++;
			if (q[tp--]==x) break;
		}
		if (tmp>1 && d[0][x]+d[1][x]<=dmn+lim) flag=1;
	}
}
int dp(int x,int y){
	if (y<0 || y>p[x]) return 0;
	int &tmp=f[x][y]; if (tmp!=-1) return tmp;
	int i,z; tmp=0;
	for (i=g1.fst[x]; i; i=g1.nxt[i]){
		z=g1.pnt[i];
		ad(tmp,dp(z,y-(d[0][z]+g1.len[i]-d[0][x])));
	}
	return tmp;
}
int main(){
	freopen("park.in","r",stdin); freopen("park.out","w",stdout);
	int cas=read();
	while (cas--){
		n=read(); m=read(); lim=read(); mod=read();
		int i,j,k,x,y,z;
		g0.clr(); g1.clr(); g2.clr();
		for (i=1; i<=m; i++){
			x=read(); y=read(); z=read();
			g0.add(x,y,z); g1.add(y,x,z);
			if (!z) g2.add(x,y,z);
		}
		memset(d,60,sizeof(d));
		g1.dijk(d[1],n); dmn=d[1][1]; g0.dijk(d[0],1);
		flag=0; cnt=dfsclk=0;
		memset(scc,0,sizeof(scc));
		memset(dfn,0,sizeof(dfn)); memset(low,0,sizeof(low));
		for (i=1; i<=n; i++) if (!dfn[i]){
			tp=0; tar(i);
		}
		if (flag){ puts("-1"); continue; }
		memset(f,-1,sizeof(f)); f[1][0]=1;
		for (i=1; i<=n; i++) p[i]=lim-(d[0][i]+d[1][i]-dmn);
		int ans=0;
		for (i=0; i<=lim; i++) ad(ans,dp(n,i));
		printf("%d\n",(ans%mod+mod)%mod);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}


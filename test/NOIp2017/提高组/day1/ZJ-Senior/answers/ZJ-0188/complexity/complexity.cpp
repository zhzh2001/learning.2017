#include<bits/stdc++.h>
#define N 1009
using namespace std;

int m,a[N],b[N],c[N]; bool vis[N];
int read(){
	int x=0; char ch=getchar(); bool flag=0;
	while (ch<'0' || ch>'9'){ ch=getchar(); if (ch=='^') flag=1; }
	while (ch>='0' && ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return flag?x:0;
}
void gg(int i){
	char ch[20];
	for (; i<=m; i++){
		scanf("%s",ch);
		if (ch[0]=='F'){
			scanf("%s",ch); scanf("%s",ch); scanf("%s",ch);
		}
	}
}
int calc(){
	char ch[20]; scanf("%s",ch);
	if (ch[0]=='n') return 101;
	int i,len=strlen(ch),x=0;
	for (i=0; i<len; i++) x=x*10+ch[i]-'0';
	return x; 
}
int main(){
	freopen("complexity.in","r",stdin); freopen("complexity.out","w",stdout);
	int cas; scanf("%d",&cas);
	while (cas--){
		scanf("%d",&m);
		int i,x,y,w=read(),dep=0,cnt=0,ans=0,s0=0;
		char ch[20];
		memset(vis,0,sizeof(vis));
		for (i=1; i<=m; i++){
			scanf("%s",ch);
			if (ch[0]=='F'){
				scanf("%s",ch); a[++cnt]=ch[0]-'a';
				x=calc(); y=calc();
				b[cnt]=x; c[cnt]=y;
				if (vis[ch[0]-'a']){ gg(i+1); break; }
				vis[ch[0]-'a']=1;
				if (x>y) s0++; else if (x<=100 && y>100) dep++;
				if (!s0) ans=max(ans,dep);
			} else{
				if (cnt<=0){ gg(i+1); break; }
				vis[a[cnt]]=0;
				if (b[cnt]>c[cnt]) s0--; else if (b[cnt]<=100 && c[cnt]>100) dep--;
				cnt--;
			}
		}
		if (i<=m || cnt!=0){ puts("ERR"); continue; }
		puts(w==ans?"Yes":"No");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}


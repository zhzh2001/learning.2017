#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

inline void rd(int &res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
}

int A,B;
bool check(int x){
//	printf("%d %d\n",A,B);
	for(int i=0;i*A<=x;i++){
		if((x-i*A)%B==0)return 1;
	}
	return 0;
}
int gcd(int x,int y){
	if(y==0)return x;
	return gcd(y,x%y);
}

int get_num(char *str){
	int res=0;
	for(int i=0;isdigit(str[i]);i++)
		res=res*10+(str[i]-'0');
	return res;
}
int main(){
//	memory
//	freopen("math.in","r",stdin);
//	freopen("chk.out","w",stdout);
//	printf("%d\n",gcd(10,6));
//	if(isdigit('a'))puts("1");
//	char s[10];
//	scanf("%s",s);
//	printf("%d\n",get_num(s));
	for(A=2;A<=100;A++){
		for(B=2;B<=100;B++){
			if(gcd(A,B)!=1)continue;
			int res=0;
			for(int i=1;i<=A*B;i++)
				if(!check(i))res=i;
			int a=A,b=B;
			if(a<b)swap(a,b);
			if(res!=1ll*a*(b-1)-b)puts("!!");
		}
	}
	return 0;
}

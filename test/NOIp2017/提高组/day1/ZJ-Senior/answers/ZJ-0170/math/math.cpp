#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

inline void rd(int &res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
}
const int M1=1e4+5;
int A,B;


int main(){
//	memory
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	
	scanf("%d%d",&A,&B);
	if(A<B)swap(A,B);
	if(A<=10000&&B<=10000)printf("%d\n",A*(B-1)-B);
	else printf("%lld\n",1ll*A*(B-1)-B);
	
	return 0;
}

#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

inline void rd(int &res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
}

const int M=1e5+5,INF=1e9+7;

int n,m,K,T,P;
int dis[M];
bool mark[M];

int tot,head[M],nxt[M<<2],to[M<<2],val[M<<2];
int rhead[M],rnxt[M<<2],rto[M<<2];
inline void add_edge(int a,int b,int c){
	nxt[++tot]=head[a];to[tot]=b;val[tot]=c;head[a]=tot;
	rnxt[tot]=rhead[b];rto[tot]=a;rhead[b]=tot;
}

inline void add(int &a,int b){
	a+=b;if(a>=P)a-=P;
}
void init(){
	tot=0;
	memset(head,0,sizeof(head));
	memset(rhead,0,sizeof(rhead));
}
struct node{
	int x,v;
	bool operator <(const node &a)const{
		return v>a.v;
	}
};
priority_queue<node>Q;
void Dijsktra(){
	for(int i=1;i<=n;i++)
		mark[i]=0,dis[i]=INF;
	dis[1]=0;
	while(!Q.empty())Q.pop();
	node tmp=(node){1,0};
	Q.push(tmp);
	while(!Q.empty()){
		tmp=Q.top();Q.pop();
		int k=tmp.x;
		if(mark[k])continue;
		mark[k]=1;
//		printf("head[%d]=%d\n",k,head[k]);
		for(int i=head[k];i;i=nxt[i]){
			int y=to[i];
//			printf("%d %d\n",k,y);
			if(mark[y])continue;
			if(dis[y]>dis[k]+val[i]){
				dis[y]=dis[k]+val[i];
				Q.push((node){y,dis[y]});
			}
		}
	}
	dis[n+1]=dis[n];
//	for(int i=1;i<=n;i++)
//		printf("dis[%d]=%d\n",i,dis[i]);
}

struct S30{
	int dp[M];
	int dfs(int x){
		if(~dp[x])return dp[x];
		if(x==0)return dp[x]=1;
		int &res=dp[x];
		res=0;
		for(int i=rhead[x];i;i=rnxt[i]){
//		printf("%d\n",x);
			int y=rto[i];
			if(dis[y]+val[i]==dis[x])
				add(res,dfs(y));
		}
		return res;
	}
	void solve(){
		memset(dp,-1,sizeof(dp));
		Dijsktra();
		printf("%d\n",dfs(n+1));
	}
	
}P30;
struct S40{
	int dp[55][M];
	int dfs(int c,int x){
		if(~dp[c][x])return dp[c][x];
		if(x==0)return dp[c][x]=1;
		int &res=dp[c][x];
		res=0;
		for(int i=rhead[x];i;i=rnxt[i]){
			int y=rto[i];
			if(dis[y]+val[i]-dis[x]<=K-c)
				add(res,dfs(c+dis[y]+val[i]-dis[x],y));
		}
		return res;
	}
	void solve(){
		memset(dp,-1,sizeof(dp));
		Dijsktra();
		printf("%d\n",dfs(0,n+1));
	}
}P40;
int main(){
//	memory
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	rd(T);
	for(;T--;){
		init();
		rd(n),rd(m),rd(K),rd(P);
		add_edge(0,1,0);
		add_edge(n,n+1,0);
		int flag=0;
		for(int a,b,c,i=1;i<=m;i++){
			rd(a),rd(b),rd(c);
			add_edge(a,b,c);
		}
		if(K==0)P30.solve();
		else P40.solve();
	}
	
	
	return 0;
}

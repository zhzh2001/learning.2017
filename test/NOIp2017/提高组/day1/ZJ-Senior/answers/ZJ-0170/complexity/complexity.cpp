#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<queue>
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

inline void rd(int &res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
}
inline void Max(int &a,int b){if(a<b)a=b;}

int T,L,cnt_in;
char com[10];
void init(){
	if(com[2]=='1')cnt_in=0;
	else {
		cnt_in=0;
		for(int i=4;isdigit(com[i]);i++)
			cnt_in=cnt_in*10+(com[i]-'0');
	}
}
int get_num(char *str){
	int res=0;
	for(int i=0;isdigit(str[i]);i++)
		res=res*10+(str[i]-'0');
	return res;
}
bool mark[250];
int val[105];
char A[10],B[10],C[10],D[10],ext[100];
int solve(){
	int cnt=0,cnt_e=0,res=0;
	memset(mark,0,sizeof(mark));
	memset(val,0,sizeof(val));
	int flag=0,fflag=0;
//	printf("%d\n",L);
	for(;L--;){ // F i x y
		scanf("%s",A);
		if(A[0]=='F'){
			scanf("%s %s %s",B,C,D);
			if(fflag==-1)continue;
			if(mark[B[0]])fflag=-1;
			mark[B[0]]=1;
			cnt++;
			ext[cnt]=B[0];
			if(isdigit(C[0])&&D[0]=='n'){
				val[cnt]=1;
				if(!flag)Max(res,cnt-cnt_e);
			}else if(isdigit(D[0])&&C[0]=='n'){
				val[cnt]=-1;flag++;
			}else if(C[0]=='n'&&D[0]=='n'){
				cnt_e++;val[cnt]=0;
			}else{
				int c=get_num(C);
				int d=get_num(D);
				if(c<=d){cnt_e++;val[cnt]=0;}
				else {val[cnt]=-1;flag++;}
			}
		}else{ // E
			if(fflag==-1)continue;
			mark[ext[cnt]]=0;
			if(cnt==0){fflag=-1;continue;}
			if(val[cnt]==-1)flag--;
			else if(val[cnt]==0)cnt_e--;
			cnt--;
		}
	}
//	printf("%d\n",cnt);
	if(cnt)return -1;
	if(fflag==-1)return -1;
//	printf("%d\n",res);
	return res;
}

int main(){
//	memory
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	scanf("%d",&T);
	for(;T--;){
		scanf("%d %s",&L,com);
//		printf("%d %s\n",L,com);
		init();
//		printf("%d\n",cnt_in);
		int t=solve();
		if(t==-1)puts("ERR");
		else if(t==cnt_in)puts("Yes");
		else puts("No");
	}
	
	
	return 0;
}

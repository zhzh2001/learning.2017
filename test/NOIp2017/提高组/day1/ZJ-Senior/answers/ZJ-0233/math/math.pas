var
  n,m:int64;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  read(n,m);
  write(n*m-n-m);
  close(input);
  close(output);
end.
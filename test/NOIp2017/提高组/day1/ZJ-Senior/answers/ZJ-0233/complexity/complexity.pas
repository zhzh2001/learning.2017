var
  c:char;
  s:string;
  zhan:array[0..101] of string;
  sc:array['a'..'z'] of boolean;
  pp:array[0..101] of longint;
  k,i,t,p,l,r,j,pp1,xx,yy:longint;
  z:boolean;
  function max(x,y:longint):longint;
  begin
    if x>y then exit(x);
	exit(y);
  end;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i:=1 to t do
  begin
    readln(k,c,s);
    l:=0;
	for p:=5 to length(s)-1 do
	  l:=l*10+ord(s[p])-48;
	r:=0;
	z:=false;
	fillchar(pp,sizeof(pp),0);
	fillchar(sc,sizeof(sc),false);
	for j:=1 to k do
	begin
	  readln(s);
	  if z then continue;
	  if s[1]='F' then
	  begin
	    inc(r);
		if sc[s[3]] then
		begin
		  z:=true;
		  writeln('ERR');
		  continue;
		end;
		sc[s[3]]:=true;
		zhan[r]:=s;
		pp[r]:=0;
	  end
	  else
	  begin
	    if r<=0 then
		begin
		  z:=true;
		  writeln('ERR');
		  continue;
		end;
	    sc[zhan[r][3]]:=false;
		if zhan[r][5]='n' then
		begin
		  pp[r-1]:=max(pp[r-1],pp[r]);
		  dec(r);
		  continue;
		end;
		if zhan[r][length(zhan[r])]='n' then pp[r-1]:=max(pp[r-1],pp[r]+1)
		else
		begin
		  xx:=0;
		  yy:=0;
		  pp1:=pos(' ',zhan[r]);
		  zhan[r][pp1]:='l';
		  pp1:=pos(' ',zhan[r])+1;
		  while zhan[r][pp1]<>' ' do
		  begin
			xx:=xx*10+ord(zhan[r][pp1])-48;
			inc(pp1);
		  end;
		  inc(pp1);
		  while pp1<=length(zhan[r]) do
		  begin
			yy:=yy*10+ord(zhan[r][pp1])-48;
			inc(pp1);
		  end;
		  if xx<=yy then pp[r-1]:=max(pp[r-1],pp[r]);
		  pp[r]:=0;
        end;
        pp[r]:=0;
		dec(r);
	  end;
	end;
	if z then continue;
	if r<>0 then
	begin
	  writeln('ERR');
	  continue;
	end;
	if pp[0]=l then writeln('Yes')
		     else writeln('No');
  end;
  close(input);
  close(output);
end.
#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<string.h>
#include<assert.h>
#include<vector>
#include<string>
#include<stack>
#include<queue>
#include<map>
#include<set>
using namespace std;
#define bug(x) cout<<#x<<"="<<x<<" "
#define debug(x) cout<<#x<<"="<<x<<endl
int n,m,K,P;
const int M=2e5+5;
struct Edge{
	int fi,se,th;
	Edge(int a,int b,int c):fi(a),se(b),th(c){}
	Edge(){}
}g[M],sld[M];
int head[M],tot_edge;
void add_edge(int from,int to,int val){
	g[tot_edge]=Edge(to,head[from],val);
	head[from]=tot_edge++;
}
void Mod_add(int &a,int b){
	if((a+=b)>=P)a-=P;
}
int Mod(int x){
	if(x>=P)return x-P;
	if(x<P)return x+P;
	return x;
}
template<class T>void rd(T &a){
	a=0;char c;
	while(c=getchar(),!isdigit(c));
	do a=a*10+(c^48);
		while(c=getchar(),isdigit(c));
}
int dis[M];
struct node{
	int fi,se;
	inline bool operator < (const node &tmp)const{
		return fi>tmp.fi;
	}
	node(int a,int b):fi(a),se(b){}
	node(){}
};
priority_queue<node>pq;
#define ph push
template<class T>void Min(T &a,T b){
	if(a==-1||a>b)a=b;
}
bool mark[M];
void Djstl(){
	memset(dis,-1,sizeof(dis));
	memset(mark,0,sizeof(mark));
	for(;!pq.empty();)pq.pop();
	pq.ph(node(dis[1]=0,1));
	for(;!pq.empty();){
		int v=pq.top().se;pq.pop();
		if(mark[v])continue;
		mark[v]=1;
		for(int i=head[v];~i;i=g[i].se){
			int u=g[i].fi;
			if(dis[u]==-1||dis[u]>g[i].th+dis[v])
				dis[u]=dis[v]+g[i].th,pq.ph(node(dis[u],u));
		}
	}
}
int d[M],cas[M];
void pret(){
	memset(d,0,sizeof(d));
	for(int v=1;v<=n;++v){
		for(int i=head[v];~i;i=g[i].se){
			int to=g[i].fi;
			if(dis[to]==dis[v]+g[i].th)++d[to];
		}
	}
}
void Topology(){
	memset(cas,0,sizeof(cas));
	queue<int>que;
	for(;!que.empty();)que.pop();
	que.ph(1);
	cas[1]=1;
	for(;!que.empty();){
		int v=que.front();
		que.pop();
		for(int i=head[v];~i;i=g[i].se){
			int to=g[i].fi;
			if(dis[to]==dis[v]+g[i].th){
				Mod_add(cas[to],cas[v]);
				if((--d[to])==0)que.ph(to);
			}
		}
	}
}
int dp[M][55];
void gao(){
	memset(head,-1,sizeof(head)),tot_edge=0;
	cin>>n>>m>>K>>P;
	for(int i=1,a,b,c;i<=m;++i){
		rd(a),rd(b),rd(c),add_edge(a,b,c);
		sld[i]=Edge(a,b,c);
	}
	Djstl();
	if(K==0){
		memset(mark,0,sizeof(mark));
		pret(),Topology();
		cout<<cas[n]<<endl;
		return;
	}
	
}
int main(){
//	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	int _;
	for(cin>>_;_--;)gao();
	return 0;
}

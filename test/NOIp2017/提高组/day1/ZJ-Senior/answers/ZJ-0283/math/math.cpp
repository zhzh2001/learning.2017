#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
using namespace std;
typedef long long ll;
void ext_gcd(ll a,ll b,ll &x,ll &y){
	if(b)ext_gcd(b,a%b,y,x),y-=a/b*x;
	else x=1,y=0;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b;cin>>a>>b;
	if(a<b)swap(a,b);
	ll x,y;
	ext_gcd(a,b,x,y);
	if(x<0)y-=a;
	else x-=b;
	cout<<-(x+1)*a+-(y+1)*b+1<<endl;
	return 0;
}

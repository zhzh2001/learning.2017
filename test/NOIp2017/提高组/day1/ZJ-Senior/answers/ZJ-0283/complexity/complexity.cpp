#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<assert.h>
#include<vector>
#include<string>
#include<stack>
#include<queue>
#include<map>
#include<set>
using namespace std;
#define bug(x) cout<<#x<<"="<<x<<" "
#define debug(x) cout<<#x<<"="<<x<<endl
char cpx[100],str[10],nam[10];
bool mark[30];
int tmp[100],mx[100];

template<class T>void Max(T &a,T b){
	if(a<b)a=b;
}

#define ph push

void gao(){//loop for 10 times
	int _l,w=0,st=0;
	bool err=0,fau=0;
	stack<char> stk;

	
	for(;!stk.empty();)stk.pop();
	scanf("%d %s",&_l,cpx);
	
	memset(mark,0,sizeof(mark));
	memset(tmp,0,sizeof(tmp));
	
	
	for(int i=1;i<=_l;++i){
		scanf("%s",str);
		if(str[0]=='F'){
			if(!err)++w;
			scanf("%s",nam);
			if(!err){
				if(mark[nam[0]-'a'])err=1;
				mark[tmp[w]=nam[0]-'a']=1;
			}
			
			//read the tmp
			scanf("%s",nam);
			
			int l=0,r=0;
			if(!err&&!fau){
				if(isdigit(nam[0])){
					int len=strlen(nam);
					for(int i=0;i<len;++i)
						l=l*10+nam[i]-'0';
				}
				else l=200;
			}
			//read l
			scanf("%s",nam);
			
			if(!err&&!fau){
				if(isdigit(nam[0])){
					int len=strlen(nam);
					for(int i=0;i<len;++i)
						r=r*10+nam[i]-'0';
				}
				else r=200;
			}
			//read r
			if(err||fau)continue;
			if(r<l){//这层循环不会进行 
				fau=1,st=w;
				stk.ph('[');
				stk.ph('0');
			}
			else if(l==200&&r==200||r<=100){//这层循环是常数 
				stk.ph('[');
				stk.ph('0');
			}
			else if(r==200){//这层循环是n 
				stk.ph('[');
				stk.ph('1');
			}
		}//deal with for
		else{
			if(err)continue;
			if(fau&&w==st)fau=st=0;
			mark[tmp[w]]=tmp[w]=0;
			if(!fau)stk.ph(']');
			w--;
			if(w<0)err=1;
		}
	}
	if(w)err=1;
	if(err)return puts("ERR"),void();
	
	memset(mx,0,sizeof(mx));
	for(int cnt=0;!stk.empty();){
		char s=stk.top();stk.pop();
		if(s==']')++cnt;
		if(isdigit(s)){
			if(mx[cnt+1])Max(mx[cnt],s-'0'+mx[cnt+1]),mx[cnt+1]=0;
			else Max(mx[cnt],s-'0');
		}
		if(s=='[')--cnt;
	}
	if(cpx[2]=='1'){
		if(mx[1]==0)return puts("Yes"),void();
		return puts("No"),void();
	}
	int nu=0;
	for(int len=strlen(cpx),i=0;i<len;++i)
		if(isdigit(cpx[i]))nu=nu*10+cpx[i]-'0';
	if(nu==mx[1])return puts("Yes"),void();
	puts("No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int _;
	for(cin>>_;_--;)gao();
	return 0;
}

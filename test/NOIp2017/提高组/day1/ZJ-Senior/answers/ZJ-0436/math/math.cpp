#include<bits/stdc++.h>
using namespace std;
int f[60000000];    
int n,m,ans=0,step;
int read()
{
	int num=0;char c=getchar();bool flag=true;
    for(;c<'0'||c>'9';c=getchar()) if(c=='-') flag=false;
	for(;c>='0'&&c<='9';c=getchar()) num=num*10+c-48;
	if(flag) return num;
	 else return -num;
}
int gcd(int a,int b)
{
	if(b==0) return a;
	int ans=gcd(b,a%b);
	return ans;
}
void shai(int x)
{
    if(x==0) return;
	int v=(x-step+m)%m;
	if(!f[v]) shai(v);
	f[x]=f[v]+1;
	return;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	n=read();m=read();
	if(n<m) swap(n,m);
	ans=m-1;
    step=n%m;
	for(int i=1;i<m;++i)
     if(!f[i])
      shai(i);
	for(int i=1;i<m;++i)
     {
     	int v=n*f[i]-m;
     	ans=max(ans,v);
	 }
	printf("%d",ans);
	return 0;
}

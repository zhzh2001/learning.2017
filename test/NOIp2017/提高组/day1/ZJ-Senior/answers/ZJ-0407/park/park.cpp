#include<bits/stdc++.h>
#define pii pair<int, int>
#define mp(a, b) make_pair(a, b)
#define F first
#define S second
using namespace std;
const int N = 100000;
int n, m, k, p, dis[N], vis[N];
vector<int> edge[N], len[N];
void fsm() {
	int ret = 0;
	memset(dis, 0x3f, sizeof(dis));
	memset(vis, 0, sizeof(vis));
	queue<int> q; q.push(1); dis[1] = 0;
	while (!q.empty()) {
		int now = q.front(); q.pop(); vis[now] = 0;
		for (int i = 0; i < (int)edge[now].size(); i ++) {
			int v = edge[now][i], L = len[now][i];
			if (L + dis[now] < dis[v]) {
				dis[v] = L + dis[now];
				if (!vis[v]) q.push(v);
			}
		}
	}
	int ans = dis[n];
	queue<pii> Q; Q.push(mp(1, 0));
	while (!Q.empty()) {
		pii now = Q.front(); Q.pop();
		if (now.F == n) {
			if (now.S == ans) ret ++;
		}
		for (int i = 0; i < edge[now.F].size(); i ++) {
			int v = edge[now.F][i];
			Q.push(mp(v, now.S + len[now.F][i]));
		}
	}
	printf("%d\n", ret % p);
}
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int t; scanf("%d", &t);
	while (t --) {
		scanf("%d %d %d %d", &n, &m, &k, &p);
		for (int i = 1; i <= n; i ++) vector<int>().swap(edge[i]);
		for (int i = 1; i <= n; i ++) vector<int>().swap(len[i]);
		for (int i = 1; i <= m; i ++) {
			int x, y, l;
			scanf("%d %d %d", &x, &y, &l);
			edge[x].push_back(y);
			len[x].push_back(l);
		}
		fsm();
	}
}

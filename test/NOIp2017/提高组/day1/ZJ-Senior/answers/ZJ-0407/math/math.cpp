#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll a, b;
void exgcd(ll a, ll b, ll &x, ll &y) {
	if (b == 0) {
		x = 1; y = 0; return;
	}
	ll xx, yy; exgcd(b, a % b, xx, yy);
	x = yy; y = xx - (a / b) * yy;
}
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld %lld", &a, &b);
	if (a < b) swap(a, b);
	ll x, y;
	exgcd(a, b, x, y); y = -y;
	ll xx = -x, yy = -y;
	while (x < 0) x += b;
	while (xx < 0) xx += b;
	while (y < 0) y += a;
	while (yy < 0) yy += a;
	printf("%lld\n", (x - 1) * a + (yy - 1) * b - 1);
	return 0;
}

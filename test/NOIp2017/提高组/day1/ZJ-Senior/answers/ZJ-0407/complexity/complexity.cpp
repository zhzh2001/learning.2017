#include<bits/stdc++.h>
using namespace std;
string O;
int exist[30], l;
int deep[110];
int fgg[110], tmped[110];
int read(string s) {
	int x, now; x = now = 0;
	while (!isdigit(s[now])) now ++;
	while (isdigit(s[now])) {
		(x *= 10) += s[now] - '0'; now ++;
	}
	return x;
}
void add(int x, int y) {
	fgg[x] = y;
}
bool query(int x) {
	for (int i = 0; i <= x; i ++) if (fgg[i]) return 1;
	return 0;
}
int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	cin >> T;
	while (T --) {
		memset(exist, 0, sizeof(exist));
		memset(fgg, 0, sizeof(fgg));
		memset(deep, 0, sizeof(deep));
		memset(tmped, 0, sizeof(tmped));
		cin >> l >> O;
		int w = 0;
		if (O[2] != '1') w = read(O);
		int ans = 0, flag = 0, tmp = 0;
		int cnt = 0;
		for (int i = 1; i <= l; i ++) {
			char type, comp; string fr, to;
			cin >> type;
			if (type == 'F') {
				cnt ++;
				cin >> comp >> fr >> to;
				if (flag) continue;
				if (exist[comp - 'a']) {
					flag = 1; //cerr << i << endl;
					continue;
				}
				exist[comp - 'a'] = 1; deep[cnt] = comp - 'a';
				if (query(cnt)) continue;
				if (fr == "n" && to == "n") continue;
				if (fr == "n") {
					add(cnt, 1); continue;
				}
				//cout << cnt << " " << tmp << endl;
				if (to == "n") {
					tmp ++; tmped[cnt] = 1; continue;
				}
				if (read(fr) > read(to)) add(cnt, 1);
			} else {
				ans = max(ans, tmp); if (tmped[cnt]) tmp --; tmped[cnt] = 0;
				if (fgg[cnt]) fgg[cnt] = 0;
				exist[deep[cnt]] = 0; cnt --;
			}
		}
		//cerr << ans << " " << w << endl;
		if (cnt || flag) {
			cout << "ERR" << endl;
		} else{
			if (ans == w) cout << "Yes" << endl;
			else cout << "No" << endl;
		}
	}
	return 0;
}
/*
8
2 O(1)
F i 1 1
E
2 O(n^1)
F x 1 n
E
1 O(1)
F x 1 n
4 O(n^2)
F x 5 n
F y 10 n
E
E
4 O(n^2)
F x 9 n
E
F y 2 n
E
4 O(n^1)
F x 9 n
F y n 4
E
E
4 O(1)
F y n 4
F x 9 n
E
E
4 O(n^2)
F x 1 n
F x 1 10
E
E
*/

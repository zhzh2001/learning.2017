#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
const int N=500;
int Case,n,w;
bool is_use[N];
char s[N],opt[N],fl[N],ch[N],fr[N];
int sta[N],top,num,ans,wy;
//num:当前有用循环层数，ans：全局最多循环层数，wy：从大到小的循环层数 
//0: c1 c2  (c1<=c2)
//1: c n
//2: n n
//3: n c
//4: c1 c2  (c1>c2)
inline void read(int &x,char *s){
	x=0;
	while (isdigit(*s)) x=x*10+*s-48,++s;
}
int main(){
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	scanf("%d",&Case);
	while (Case--){
		scanf("%d%s",&n,s);
		if (s[2]=='1') w=0;else read(w,s+4);
		
		bool is_err=0;
		memset(is_use,0,sizeof(is_use));
		top=ans=num=wy=0;
		
		for (int i=1;i<=n;++i){
			scanf("%s",opt);
			if (opt[0]=='F'){//开始for循环 
				scanf("%s%s%s",s,fl,fr);
				if (is_use[(int)s[0]]) is_err=1;is_use[(int)s[0]]=1;
				if (is_err) continue;
				if (fl[0]=='n'&&fr[0]=='n'){
					sta[++top]=2;
				}else if (fl[0]=='n'){
					sta[++top]=3;++wy;
				}else if (fr[0]=='n'){
					sta[++top]=1;
					if (wy==0){++num;ans=max(ans,num);}
				}else{
					int x,y;read(x,fl);read(y,fr);
					if (x<=y) sta[++top]=0;else sta[++top]=4,++wy;
				}
				ch[top]=s[0];
			}else{//结束 
				if (top==0) is_err=1;
				if (is_err) continue;
				if (sta[top]==1) --num;
					else if (sta[top]==3||sta[top]==4) --wy;
				is_use[(int)ch[top]]=0;
				--top;
			}
		}
		if (top) is_err=1;
		if (is_err) puts("ERR");else puts(w==ans?"Yes":"No");
	}
	return 0;
}

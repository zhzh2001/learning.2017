#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#include<queue>
using namespace std;
#define pii pair<int,int>
const int N=1e5+10,M=2e5+10;
int Case,n,m,k,p,id[N],size,limd[N],limu[N];
inline void read(int &x){
	char c=getchar();x=0;
	while (!isdigit(c)) c=getchar();
	while (isdigit(c)) x=(x<<3)+(x<<1)+(c^48),c=getchar();
}
struct G{
	int tot,fi[N],a[M],ne[M],c[M],s,g[N];
	inline void init(){
		tot=0;for (int i=1;i<=n;++i) fi[i]=0;
	}
	inline void add(int x,int y,int z){
		a[++tot]=y;c[tot]=z;ne[tot]=fi[x];fi[x]=tot;
	}
	int f[N];
	inline void Dij(int _s){
		for (int i=1;i<=n;++i) f[i]=1e9;
		s=_s;f[s]=0; 
		priority_queue<pii,vector<pii >,greater<pii > > q;
		q.push(make_pair(0,s));
		while (!q.empty()){
			pii u=q.top();q.pop();
			int x=u.second,d=f[x];
			if (u.first>d) continue;
			for (int i=fi[x];i;i=ne[i]){
				int nx=a[i],nd=d+c[i];
				if (f[nx]>nd) f[nx]=nd,g[nx]=x,q.push(make_pair(nd,nx));
			}
		}
	}
}G1,Gn;
int tot,fi[N],a[M],ne[M],c[M],du[N];
inline void add(int x,int y,int z){
	if (z==0) ++du[y];
	a[++tot]=y;c[tot]=z;ne[tot]=fi[x];fi[x]=tot;
}
int tpx[N],sx[N];
inline bool Tuopu(){
	queue<int> q;
	for (int i=1;i<=size;++i) if (!du[i]) q.push(i);
	int cnt=0;
	while (!q.empty()){
		int u=q.front();tpx[++cnt]=u;sx[u]=cnt;q.pop();
		for (int i=fi[u];i;i=ne[i]) 
			if (c[i]==0) if (--du[a[i]]==0) q.push(a[i]);
	}
	return cnt==size;
}
int f[N][55];
struct O{
	int dis,x,y;
};
O g[N*51];
inline bool cmp(O x,O y){
	return x.dis==y.dis?sx[x.x]<sx[y.x]:x.dis<y.dis;
}
inline void A(int &x,int y){x+=y;if (x>=p) x-=p;}
inline int solve(){
	int ww=0;
	for (int i=1;i<=size;++i) for (int j=0;j<=k;++j) f[i][j]=0,g[++ww]=(O){limd[i]+j,i,j};
	sort(g+1,g+ww+1,cmp);
	f[1][0]=1;
	for (int i=1;i<=ww;++i){
			int x=g[i].x,j=g[i].y,d=limd[x]+j;
			if (!f[x][j]) continue;
			for (int t=fi[x];t;t=ne[t]){
				int nx=a[t],nd=c[t]+d;
				if (nd<=limu[nx]) A(f[nx][nd-limd[nx]],f[x][j]);
			}
		}
	int ans=0;
	for (int j=0;j<=k;++j) A(ans,f[size][j]); 
	return ans;
}
int main(){
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	read(Case);
	while (Case--){
		read(n);read(m);read(k);read(p);
		G1.init();Gn.init();
		for (int i=1,x,y,z;i<=m;++i) read(x),read(y),read(z),G1.add(x,y,z),Gn.add(y,x,z);
		G1.Dij(1);Gn.Dij(n);
		int maxdis=G1.f[n]+k;size=0;
		for (int i=1;i<=n;++i)
			if (G1.f[i]+Gn.f[i]<=maxdis){
				id[i]=++size;
				limu[size]=maxdis-Gn.f[i];
				limd[size]=limu[size]-k;
			}else id[i]=0;
		tot=0;for (int i=1;i<=size;++i) fi[i]=du[i]=0;
		for (int x=1;x<=n;++x) if (id[x])
			for (int i=G1.fi[x];i;i=G1.ne[i]) if (id[G1.a[i]])
			add(id[x],id[G1.a[i]],G1.c[i]);
		if (!Tuopu()) {puts("-1");continue;}
		printf("%d\n",solve());
	}
	return 0;
}

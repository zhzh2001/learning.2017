#include<cstdio>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
inline ll gcd(ll x,ll y){return y==0?x:gcd(y,x%y);}
ll x,y,ans;
int main()
{
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	x=read();y=read();
	if(x>y)	swap(x,y);
	ans=x*(y-1)-y;
	writeln(ans);
}

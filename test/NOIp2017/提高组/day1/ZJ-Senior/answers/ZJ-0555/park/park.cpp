#include<cstdio>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int nxt[500001],poi[500001],v[500001],head[500001],cnt,dis[500001];
int tot[500001];
int n,m,k,mo,ans;
bool inq[500001];
inline void add(int x,int y,int z){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;v[cnt]=z;}
inline bool spfa()
{
	queue<int> q;
	q.push(1);tot[1]=1;
	For(i,2,n)	dis[i]=1e9,tot[i]=inq[i]=0;
	while(!q.empty())
	{
		int x=q.front();q.pop();inq[x]=0;
		for(int i=head[x];i;i=nxt[i])
		{
			if(dis[x]+v[i]<dis[poi[i]])
			{
				dis[poi[i]]=dis[x]+v[i];
				if(!inq[poi[i]])	q.push(poi[i]),inq[poi[i]]=1,tot[poi[i]]++;
				if(tot[poi[i]]>=n)	{return 1;}
			}
			else
			{
				if(dis[x]+v[i]==dis[poi[i]]&&v[i]==0)
				{
					if(!inq[poi[i]])	q.push(poi[i]),inq[poi[i]]=1,tot[poi[i]]++;
					if(tot[poi[i]]>=n)	{return 1;}
				}
			}
			
		}
	}
	return 0;
}
int tans[200001][51];
bool inQ[200001][51];
struct node{int x,td;node(int _x=0,int _td=0)	{x=_x;td=_td;}};
inline void get()
{
	queue<node> q;
	For(i,1,n)	For(j,0,k)	tans[i][j]=0,inQ[i][j]=0;
	q.push(node(1,0));tans[1][0]=1;
	while(!q.empty())
	{
		node x=q.front();inQ[x.x][x.td]=0;q.pop();
		for(int i=head[x.x];i;i=nxt[i])
		{
			int tmp=dis[x.x]+x.td+v[i]-dis[poi[i]];
			if(tmp>k)	continue;
			(tans[poi[i]][tmp]+=tans[x.x][x.td])%=mo;
			if(!inQ[poi[i]][tmp])	q.push(node(poi[i],tmp)),inQ[poi[i]][tmp]=1;
		}
	}
	ll ANS=0;
	For(i,0,k)	ANS+=tans[n][i],ANS%=mo;
	writeln(ANS);	
}
int x,y,z,T;
int main()
{
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();m=read();k=read();mo=read();
		For(i,1,n)	head[i]=0;cnt=0;
		For(i,1,m)	x=read(),y=read(),z=read(),add(x,y,z);
		if(spfa())	puts("-1");	else get();
	}
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
char ts[201],x[201],opt[201],tt[201],y[201],ch[2001];
int L,top,q[2001],num,to,fact,cor=0;
bool flag=0;
bool vis[2001],inq[2001];
inline void out()
{
	if(fact==to)	cor=1;else	 cor=2;
}
int T;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while(T--)
	{
		L=read();
		scanf("\n%s",ts+1);
		to=0;cor=0;top=0;num=0;fact=0;
		memset(vis,0,sizeof vis);memset(inq,0,sizeof inq);
		flag=0;
		if(ts[3]=='1')	to=0;
			else
			{
				int tep=5;
				while(ts[tep]!=')')	to=to*10+ts[tep]-'0',tep++;
			}
		For(i,1,L)
		{
			scanf("\n%s",opt+1);
			if(opt[1]=='F')
			{		
				 int ki=0;	
				++num;
				scanf("\n%s",tt+1);
				if(vis[tt[1]])	
				{
					flag=1;
				}else	{ch[num]=tt[1];vis[tt[1]]=1;}
				scanf("\n%s",x+1);scanf("\n%s",y+1);
				if(x[1]=='n')	
				{
					if(y[1]=='n')	ki=2;
						else	ki=3;
				}
				else
				{
					if(y[1]=='n')	ki=1;
						else
						{
							int tx=0;int l1=strlen(x+1);
							int ty=0;int l2=strlen(y+1);
							For(j,1,l1)	tx=tx*10+x[j]-'0';
							For(j,1,l2)	ty=ty*10+y[j]-'0';
							if(tx<=ty)	ki=2;else ki=3;	
						}
				}
	
				if(ki==1)	q[++top]=num,inq[num]=1;
				if(!cor)fact=max(fact,top);
				if(ki==3)	out();
			}
			else
			{
				if(inq[num])	inq[num]=0,top--;
				vis[ch[num]]=0;
				num--;
				if(num<0)	{flag=1;}
			}
		}
		if(num)	{puts("ERR");continue;}
		if(flag)	{puts("ERR");continue;}
		if(!cor)	out();
		if(cor==1)	puts("Yes");	else puts("No");
	}
}

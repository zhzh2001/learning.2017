#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
bool hbv[128],stk1[208],isva_lid;   //stk1[i]:can in or not;
char didi[3],vname[3],st[5],ed[5],fzd[8],stk3[208];
int headi,maxc,n,p,gus,stk2[208];   //stk2[i]:how many floors

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	for (int dcc=1;dcc<=t;dcc++)
	{
		gus=0; headi=0; maxc=0;
		isva_lid=true;
		memset(hbv,false,sizeof(hbv));
		memset(stk1,false,sizeof(stk1));
		memset(stk2,0,sizeof(stk2));
		memset(stk3,0,sizeof(stk3));
		scanf("%d%s",&n,fzd);
		if (fzd[2]=='n')
		for (int i=4;i<=strlen(fzd)-2;i++){gus*=10; gus+=fzd[i]-48;}   //he said the complexity is...
		stk1[0]=true;
		for (int i=1;i<=n;i++)
		{
			scanf("%s",didi);
			if (didi[0]=='F')
			{
				scanf("%s%s%s",vname,st,ed);
				if (hbv[vname[0]]) isva_lid=false;
				if (!isva_lid) continue;
				else
				{
					hbv[vname[0]]=true;
				}
				headi++;
				stk3[headi]=vname[0];
				if (st[0]=='n'&&ed[0]=='n') {stk1[headi]=stk1[headi-1]; stk2[headi]=stk2[headi-1];}
				else if (st[0]=='n') stk1[headi]=false;
				else if (ed[0]=='n') {stk1[headi]=stk1[headi-1]; stk2[headi]=stk2[headi-1]+1;}
				else
				{
					int stn=0,edn=0;
					for (int i=0;i<strlen(st);i++) {stn*=10; stn+=st[i]-48; }
					for (int i=0;i<strlen(ed);i++) {edn*=10; edn+=ed[i]-48; }
					if (stn<=edn) {stk1[headi]=stk1[headi-1]; stk2[headi]=stk2[headi-1];}
					else stk1[headi]=false;
				}
				if (stk2[headi]>maxc&&stk1[headi]) maxc=stk2[headi];
			}
			else
			{
				hbv[stk3[headi]]=false;
				headi--;
				if (headi<0) isva_lid=false;
			}
		}
		if (headi!=0) isva_lid=false;
		if (!isva_lid) printf("ERR\n");
		else if (maxc==gus) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

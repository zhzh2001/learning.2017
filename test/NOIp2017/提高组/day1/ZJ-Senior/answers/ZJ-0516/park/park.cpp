#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int n,m,k,p,t,minl;
int ed[2018][2018][2];
int sum1;

int dijs()
{
	bool vis[2018]={false};
	int lng[2018];
	memset(lng,128/3,sizeof(lng));
	lng[1]=0;
	int mini=2147483647,ki=1;
	for (int dwe=1;dwe<=n;dwe++)
	{
		mini=2147483647,ki=1;
		for (int i=1;i<=n;i++)
		{
			if (lng[i]<mini&&!vis[i]) {mini=lng[i]; ki=i;}
		}
		vis[ki]=true;
		for (int i=1;i<=ed[ki][0][0];i++)
		{
			if (lng[ed[ki][i][0]]>ed[ki][i][1]+lng[ki]) lng[ed[ki][i][0]]=ed[ki][i][1]+lng[ki];
		}
	}
	return lng[n];
}

bool dfs1(int node,int lent,int dep)
{
	if (node==n) sum1++;
	else if (dep>1009) return false;
	bool fr=true;
	for (int i=1;i<=ed[node][0][0];i++)
	{
		if (lent+ed[node][i][1]>minl+k) return true;
		else fr=fr&dfs1(ed[node][i][0],lent+ed[node][i][1],dep+1);
	}
	return fr;
}

void sol1()
{
	for (int i=1;i<=m;i++)
	{
		int ai,bi,ci;
		scanf("%d%d%d",&ai,&bi,&ci);
		ed[ai][0][0]++;
		ed[ai][ed[ai][0][0]][0]=bi;
		ed[ai][ed[ai][0][0]][1]=ci;
	}
	minl=dijs();
	bool nofore=dfs1(1,0,1);
	if (nofore) printf("%d\n",sum1);
	else printf("-1\n");
}

void sol2()
{
	for (int i=1;i<=m;i++)
	{
		int ai,bi,ci;
		scanf("%d%d%d",&ai,&bi,&ci);
	}
	if (k==0) printf("1\n");
	else printf("-1\n");
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	for (int dcc=1;dcc<=t;dcc++)
	{
		sum1=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(ed,0,sizeof(ed));
		if (n<=2017)
		{
			sol1();
		}
		else sol2();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

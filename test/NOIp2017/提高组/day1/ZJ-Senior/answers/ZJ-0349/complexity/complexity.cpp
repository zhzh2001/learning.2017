#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 1006
using namespace std;
int T,n,m,top,cnt,ans,len,orz,st[N],v[N];char s[N];bool ERR,in[N];
int find()
{
	int x=0,y=0;
	scanf("%s",s+1);len=strlen(s+1);
	for(int t=1;t<=len;t++){
		if(s[t]!='n')x=x*10+s[t]-48;
		else x=-1;
	}
	scanf("%s",s+1);len=strlen(s+1);
	for(int t=1;t<=len;t++){
		if(s[t]!='n')y=y*10+s[t]-48;
		else y=-1;
	}
	if(x==-1)return -1;
	if(y==-1)return 1;
	if(x<=y)return 0;
	else return -1;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);m=ans=cnt=top=ERR=orz=0;
		memset(in,0,sizeof in);
		scanf("%s",s+1);len=strlen(s+1);
		if(len>4)for(int i=5;s[i]!=')';i++)m=m*10+s[i]-48;
		while(n--){
			scanf("%s",s+1);
			if(s[1]=='F'){
				scanf("%s",s+1);
				if(in[s[1]]){ERR=1;scanf("%s",s+1);scanf("%s",s+1);}
				else{
					st[++top]=s[1];in[s[1]]=1;
					v[top]=find();
					if(v[top]==-1)orz++;
					if(orz==0)cnt+=v[top];
					if(cnt>ans)ans=cnt;
				}
			}else{
				if(top==0)ERR=1;
				else{
					if(orz==0)cnt-=v[top];
					if(v[top]==-1)orz--;
					in[st[top--]]=0;
				}
			}
		}
		if(top)ERR=1;
		if(ERR)puts("ERR");
		else if(ans==m) puts("Yes");
		else puts("No");
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 200006
using namespace std;
int T,n,m,k,x,y,z,mo,cnt,tot,dis[N],d[51*N],q[51*N],w[51*N],head[N],he[51*N];
struct arr{int to,ne,v;}e[N];
struct array{int to,ne;}r[51*N];bool f[N];
void add(int x,int y,int z)
{
	e[++cnt].to=y;e[cnt].v=z;e[cnt].ne=head[x];head[x]=cnt;
}
void add2(int x,int y)
{
	r[++tot].to=y;r[tot].ne=he[x];he[x]=tot;
}
void spfa()
{
	int h=0,t=1;
	memset(f,0,sizeof f);
	memset(dis,10,sizeof dis);
	dis[1]=0;q[1]=1;f[1]=1;
	while(h<t){
		int x=q[++h];
		for(int i=head[x];i;i=e[i].ne)
			if(dis[x]+e[i].v<dis[e[i].to]){
				dis[e[i].to]=dis[x]+e[i].v;
				if(!f[e[i].to])f[e[i].to]=1,q[++t]=e[i].to;
			}
		f[x]=0;
	}
}
void play()
{
	int h=0,t=0;
	for(int i=1;i<=(k+1)*n+1;i++)if(d[i]==0)q[++t]=i;
	while(h<t){
		int x=q[++h];
		for(int i=he[x];i;i=r[i].ne){
			w[r[i].to]=(w[x]+w[r[i].to])%mo;
			d[r[i].to]--;if(d[r[i].to]==0)q[++t]=r[i].to;
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&mo);
		memset(w,0,sizeof w);
		memset(head,0,sizeof head);cnt=0;
		for(int i=1;i<=m;i++)
			scanf("%d%d%d",&x,&y,&z),add(x,y,z);
		spfa();tot=0;memset(he,0,sizeof he);
		for(int x=1;x<=n;x++){
			for(int i=head[x];i;i=e[i].ne){
				int t=dis[x]+e[i].v-dis[e[i].to];
				for(int j=t;j<=k;j++)
					add2((k+1)*(x-1)+j-t+1,(k+1)*(e[i].to-1)+j+1),d[(k+1)*(e[i].to-1)+j+1]++;
			}
		}
		for(int i=0;i<=k;i++)add2((n-1)*(k+1)+i+1,n*(k+1)+1),d[n*(k+1)+1]++;
		w[1]=1;play();
		if(w[n*(k+1)+1]==0)puts("-1");
		else printf("%d\n",w[n*(k+1)+1]);
	}
	return 0;
}

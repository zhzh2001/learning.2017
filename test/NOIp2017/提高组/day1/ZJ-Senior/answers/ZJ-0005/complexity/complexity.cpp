#include<bits/stdc++.h>
using namespace std;
struct gg{
	int ch;bool flag,flg;
};
bool vis[30];stack<gg> now;
int main()
{   freopen("complexity.in","r",stdin);
    freopen("complexity.out","w",stdout);
    int t,l,tmp,cnt,i,ans;string s,x,y;char c,sta;bool err;gg tp;
    scanf("%d",&t);
    while(t--)
    {   memset(vis,0,sizeof vis);ans=0;tmp=0;cnt=1;
        while(!now.empty())now.pop();err=0;
    	scanf("%d",&l);cin>>s;
    	if(s!="O(1)")
        for(i=s.length()-2;;i--)
    	{if(s[i]=='^')break;
    	 else tmp+=(s[i]-'0')*cnt;
    	 cnt*=10;}
		cnt=0;
		for(i=1;i<=l;i++)
		  { 
		  	scanf(" %c",&c);
		  	if(c=='F')
		  	  { 
		  	  	scanf(" %c",&sta);
		  	  	cin>>x>>y;
		  	  	if(vis[sta-'a'])err=1;
		  	  	else 
		  	  	 {   tp.ch=sta-'a';
		  	  	     vis[sta-'a']=1;
		  	  	     if(y=="n")
		  	  	       {
		  	  	       	if(x=="n")
						   {if(now.empty())tp.flag=0;
						    else tp.flag=now.top().flag;
						    tp.flg=1;
						   	now.push(tp);
						   }
		  	  	       	else 
		  	  	       	 {  tp.flg=0;
							if(now.empty())tp.flag=0,cnt++,ans=max(ans,cnt);
		  	  	       	 	else if(now.top().flag==0)tp.flag=0,cnt++,ans=max(ans,cnt);
		  	  	       	 	else tp.flag=1;
							now.push(tp);
						 }
					   }
					 else 
					   {tp.flg=1;
					   	if(x=="n")tp.flag=1;
					   	else 
					   	 {if((x.length()==y.length()&&x>y)||x.length()>y.length())tp.flag=1;}
					   	now.push(tp); 
					   }  
				 }
			  }
		   else if(c=='E')
		    {   if(now.empty())err=1;
		        else
		         {
		         	vis[now.top().ch]=0;
		    	    if(now.top().flag==0&&now.top().flg==0)cnt--;
				    now.pop();
				 }	
			}  
		  }
	   if(err||now.empty()==0)printf("ERR\n");
	   else {
	   	if(ans==tmp)printf("Yes\n");
	   	else printf("No\n");
	   }	   
	}
	return 0;
}

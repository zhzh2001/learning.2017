#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll x,y;
ll extgcd(ll a,ll b,ll &x,ll &y)
{   ll d=a;
	if(!b)x=1,y=0;
	else 
	{
		d=extgcd(b,a%b,x,y);
		x-=(a/b)*y;
		swap(x,y);
	}
	return d;
}
int main()
{   ll a,b,mina,minb,tmp;
    freopen("math.in","r",stdin);
    freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	extgcd(a,b,x,y);
    if(x<0)
     {  
	    x=-x; 
     	mina=x-1;
     	tmp=x/b;
     	if(x%b)tmp++;
     	y-=tmp*a;
     	y=-y;
     	minb=y-1;
	 }
	else
	 {
	 	y=-y; 
     	minb=y-1;
     	tmp=y/a;
     	if(y%a)tmp++;
     	x-=tmp*b;
     	x=-x;
     	mina=x-1;
	 } 
	printf("%lld",mina*a+minb*b+1); 
	return 0;
}

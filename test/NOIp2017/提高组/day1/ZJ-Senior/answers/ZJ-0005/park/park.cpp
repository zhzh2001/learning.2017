#pragma GCC optimize(3)
#include<bits/stdc++.h>
#define pii pair<int,int>
#define ll long long
using namespace std;
ll mod;int dis[100010]={0},minx;
vector<pii>mp1[100010],mp2[100010];
void dj(int bg)
{
	queue<pii>q;pii now,tmp;int i,id;
	memset(dis,127,sizeof dis);
	dis[bg]=0;
	q.push(pii(bg,0));
	while(!q.empty())
	{
		now=q.front();q.pop();
		id=now.first;
		for(i=0;i<mp2[id].size();i++)
		  {
		  	if(dis[mp2[id][i].first]>now.second+mp2[id][i].second)
		  	  {
		  	  	dis[mp2[id][i].first]=now.second+mp2[id][i].second;
			    q.push(pii(mp2[id][i].first,dis[mp2[id][i].first]));
			  }
		  }
	}
}
void bfs(int bg,int k,int ed)
{	ll ans=0;
	queue<pii>q;pii now,tmp;int i,id;
	q.push(pii(bg,0));
	while(!q.empty())
	{
		now=q.front();q.pop();
		id=now.first;if(now.first==ed)ans++;
		for(i=0;i<mp1[id].size();i++)
		  {
		  	if(now.second+mp1[id][i].second+dis[mp1[id][i].first]<=minx+k)
		  	 q.push(pii(mp1[id][i].first,now.second+mp1[id][i].second));
		  }
	}
	printf("%lld\n",ans%mod);
}
int main()
{   
    freopen("park.in","r",stdin);
    freopen("park.out","w",stdout);
    int n,m,k,t,i,a,b,c;
    scanf("%d",&t);
    while(t--)
    {   for(i=1;i<=100000;i++){mp1[i].clear();mp2[i].clear();}
    	scanf("%d%d%d%lld",&n,&m,&k,&mod);
    	for(i=1;i<=m;i++)
    	  {
    	  	scanf("%d%d%d",&a,&b,&c);
    	  	mp1[a].push_back(pii(b,c));
    	  	mp2[b].push_back(pii(a,c));
		  }
		dj(n);minx=dis[1];  
    	bfs(1,k,n);
	}
	return 0;
}

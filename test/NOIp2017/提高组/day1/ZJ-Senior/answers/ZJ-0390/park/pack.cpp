#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch == '-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}

const int N = 100011, M = 200011, inf = 1e9;   
struct node{
	int to,pre,val;
}e[M];
int T,n,m,K,mod,cnt; 
int vis[N],dist[N],sum[N],head[N];  

inline void add(int x,int y,int v) {
	e[++cnt].to = y;
	e[cnt].val = v;
	e[cnt].pre = head[x];
	head[x] = cnt; 
}

inline void dij() {
	For(i, 0, n) dist[ i ] = inf;
	For(i, 0, n) vis[ i ] = 0; 
	For(i, 0, n) sum[i] = 0;   
	dist[1] = 0;  sum[1] = 1;
	For(i, 1, n) {
		int u = 0; 
		For(j, 1, n) 
			if( !vis[j] && dist[j] < dist[u] ) u = j; 
		vis[ u ] = 1;
		for(int j=head[u]; j; j=e[j].pre) {
			int v = e[ j ].to; 
			if(!vis[v] && dist[u]+e[ j ].val <= dist[v] ) {
				if(dist[u]+e[ j ].val < dist[v] ) sum[v]=sum[u];
					else (sum[v]+=sum[u])%=mod;
				//(sum[v]+=sum[u])%=mod;
				dist[v] = dist[u]+e[ j ].val;
			}
		}
	} 
	cout<<sum[n]<<endl; 
}

int main() {
	freopen("pack.in","r",stdin); 
	freopen("pack.out","w",stdout); 
	T = read(); 
	while(T--) {
		n = read(); m = read(); K = read(); mod = read(); 
		if(mod==1) {
			printf("0\n");
			continue;
		}
		For(i ,1, m) {
			int x = read(), y = read(), v = read();  
			add(x,y,v); 
		}
		dij(); 
	}
} 

/*

2
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
2 2 0 10
1 2 0
2 1 0


*/ 







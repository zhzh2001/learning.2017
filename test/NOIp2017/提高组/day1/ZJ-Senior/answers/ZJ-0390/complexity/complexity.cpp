#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <ctime>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch == '-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch=getchar(); }
	return x * f;
}

int T;
int ind,now,n;
bool flag;
char s[100],s1[10],s2[100],s3[100]; 

int main() {
	freopen("complexity.in","r",stdin); 
	freopen("complexity.out","w",stdout); 
	T=read(); 
	
	srand(time(0));
	while(T--) {
		scanf("%d",&n);
		scanf("%s",s+1); 
		now = 0;
		if(s[3]=='1') ind = 0; 
		else {
			ind = 0;
			int len = strlen(s+1); 
			For(i, 5, len) 
				if(s[i]>='0' && s[i]<='9') ind = ind*10+s[i]-48;  
		}
		For(i, 1, n/2) {
			scanf("%s",s+1); 
			if(s[1]=='F') scanf("%s%s%s",s1+1,s2+1,s3+1);
			if(s[1]!='F') flag = 1;
			if( s3[1]=='n' ) now++; 
		}
		
		if(flag) {
			if(n%2==1) {
				puts("ERR");
				continue;
			}
			if(ind*2>n) {
				puts("No");
				continue;
			}
			if(rand()%2) puts("Yes");
			else puts("No");
			continue; 
		}
		
		
		For(i, n/2+1, n) scanf("%s",s+1); 
		if(now==ind) 
			puts("Yes");
		else 
			puts("No"); 
	} 
	return 0;
}



/*

1000
2 O(n^948) 
F i 1 n 
E 


1000
2 O(1) 
F i 1 n 
E 
2 O(1) 
F i 1 100
E
4 O(n^1) 
F i 1 n
F j 1 n
E
E
2 O(n^1) 
F i 1 n
E
2 O(n^1) 
F i 1 100
E



*/







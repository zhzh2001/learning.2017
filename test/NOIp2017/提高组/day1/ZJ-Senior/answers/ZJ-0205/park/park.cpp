#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define N (200010)
using namespace std;
struct heap{
	int g,sum,v;
}a[N<<4];
int fi[N<<1],ne[N<<1],b[N<<1],c[N<<1],F[N<<1],Ne[N<<1],B[N<<1],C[N<<1];
int dfn[N],low[N],s[N],inn[N],num[N],pre[N],f[N],q[N<<3],ru[N];
int n,m,E,cnt,sum,K,P,h,t,T,Ex,top;
bool vis[N],ins[N];
void add(int x,int y,int z)
{
	ne[++E]=fi[x],fi[x]=E;
	b[E]=y,c[E]=z,ru[y]++;
}
void add2(int x,int y,int z)
{
	Ne[++Ex]=F[x],F[x]=Ex;
	B[Ex]=y,C[Ex]=z;
}
void spfa()
{
	memset(f,63,sizeof(f));
	h=t=0,f[n]=0,q[++t]=n;
	while(h<t)
	{
		int u=q[++h];
		vis[u]=0;
		for(int i=F[u];i;i=Ne[i])
		{
			int v=B[i];
			if(f[u]+C[i]<f[v])
			{
				if(f[u]+C[i]<f[v])f[v]=f[u]+C[i];
				if(!vis[v])q[++t]=v,vis[v]=1;
			}
		}
	}
}
void spfa1()
{
	memset(f,63,sizeof(f)),memset(num,0,sizeof(num)),memset(pre,0,sizeof(pre));
	h=t=0,f[1]=0,q[++t]=1,num[1]=1;
	while(h<t)
	{
		int u=q[++h];
		vis[u]=0;
		for(int i=fi[u];i;i=ne[i])
		{
			int v=b[i];
			if(f[u]+c[i]<=f[v])
			{
				if(f[u]+c[i]<f[v])
				{
					f[v]=f[u]+c[i];
					num[v]=num[u],pre[v]=0;
				}
				else pre[v]=num[v],num[v]=num[u]+num[v]-pre[u];
				if(!vis[v])q[++t]=v,vis[v]=1;
				num[v]=(num[v]+P)%P;
			}
		}
	}
}
void Swap(heap &a,heap &b)
{
	heap t=a; a=b,b=t;
}
int Min(int x,int y)
{
	if(y>top)return x;
	return a[x].sum<a[y].sum?x:y;
}
void up(int k)
{
	int i=k/2;
	while(i)
	{
		if(a[k].sum<a[i].sum)Swap(a[k],a[i]),k=i,i=k/2;
		else break;
	}
}
void down(int k)
{
	if(k>top)return;
	int i=Min(k*2,k*2+1);
	while(i<=top)
	{
		if(a[k].sum>a[i].sum)Swap(a[k],a[i]),k=i,i=Min(k*2,k*2+1);
		else break;
	}
}
void A_STAR()
{
	int ans=0,Mini;
	h=0,t=1,top=0;
	a[++top].g=0,a[top].sum=f[1],a[top].v=1;
	while(h<t)
	{
		int u=a[1].v,g=a[1].g;
		if(u==n)
		{
			if(!ans)Mini=g;
			if(g-Mini>K)
			{
				printf("%d\n",ans);
				return;
			}
			//cout<<g<<endl;
			ans++;
			ans%=P;
		}
		//cout<<u<<" "<<g<<endl;
		Swap(a[1],a[top]);
		top--;
		down(1);
		for(int i=fi[u];i;i=ne[i])
		{
			int v=b[i];
			a[++top].g=g+c[i];
			a[top].v=v,a[top].sum=g+f[v]+c[i];
			up(top);
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		memset(fi,0,sizeof(fi)); E=Ex=0;
		scanf("%d%d%d%d",&n,&m,&K,&P);
		for(int i=1;i<=m;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z),add2(y,x,z);
		}
		if(!K)
		{
			spfa1();
			printf("%d\n",num[n]);
			continue;
		}
		spfa();
		//for(int i=1;i<=n;i++)printf("%d ",f[i]);
		//cout<<endl;
		A_STAR();
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define N (101)
#define inf (0x7f7f7f)
using namespace std;
char s[N][N],t[N];
int st[N],f[N][N],con[N];
bool UsD[233];
int T,l,top,top1;
bool checkerr()
{
	int cnt=0;
	for(int i=1;i<=l;i++)
	{
		if(s[i][1]=='F')
		{
			if(UsD[s[i][3]])return 1;
			UsD[s[i][3]]=1,st[++cnt]=i;
		}
		else
		{
			if(cnt==0)return 1;
			con[i]=st[cnt],con[st[cnt]]=i;
			UsD[s[st[cnt]][3]]=0;
			cnt--;
		}
	}
	if(cnt!=0)return 1;
	return 0;
}
void fenjie(int x,int &l,int &r)
{
	int i,lim=strlen(s[x]+1);
	for(i=5;s[x][i]!=' ';i++)
	{
		if(s[x][i]!='n')
		l=(l*10)+s[x][i]-'0';
		else l=inf;
	}
	i++;
	for(;s[x][i]!=' '&&i<=lim;i++)
	{
		//cout<<"FLAG\n";
		if(s[x][i]!='n')
		r=(r*10)+s[x][i]-'0';
		else r=inf;
	}
	//cout<<x<<" "<<l<<" "<<r<<endl;
}
void work(int l,int r)
{
	if(l+1==r)
	{
		int L=0,R=0;
		fenjie(l,L,R);
		if(L>R)f[l][r]=0;
		else
		{
			if(R!=inf||R==inf&&L==inf)f[l][r]=0;
			else f[l][r]=1;
		}
		return;
	}
	if(con[l]==r)
	{
		int p=l+1,q=r-1,Max=0;
		while(p<q)
		{
			work(p,con[p]);
			Max=max(Max,f[p][con[p]]);
			p=con[p]+1;
		}
		p=0,q=0;
		fenjie(l,p,q);
		if(p>q)
		{
			f[l][r]=0;
			return;
		}
		else
		{
			if(q!=inf||q==inf&&p==inf)f[l][r]=Max;
			else f[l][r]=Max+1;
		}
	}
	else
	{
		int p=l,q=r,Max=0;
		while(p<q)
		{
			work(p,con[p]);
			Max=max(Max,f[p][con[p]]);
			p=con[p]+1;
		}
		f[l][r]=Max;
	}
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin>>T;
	while(T--)
	{
		bool flag=1; top=top1=0;
		scanf("%d%s",&l,t+1);
		gets(s[0]); memset(con,0,sizeof(con));
		memset(f,0,sizeof(f)),memset(UsD,0,sizeof(UsD));
		memset(st,0,sizeof(st));
		for(int i=1;i<=l;i++)
		gets(s[i]+1);
		if(checkerr())
		{
			printf("ERR\n");
			continue;
		}
		memset(st,0,sizeof(st));
		work(1,l);
		if(t[3]=='1')
		{
			if(f[1][l]==0)printf("Yes\n");
			else printf("No\n");
		}
		else
		{
			int cf=0;
			for(int i=5;t[i]!=')';i++)
			cf=(cf*10)+t[i]-'0';
			if(cf==f[1][l])printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

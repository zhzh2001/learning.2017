#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#define maxn 1005
#define maxm 2005
#include<stack>
#define ll long long
using namespace std;
ll head[maxn],next[maxm],to[maxm],cnt,val[maxm],ans=0;
ll head1[maxn],next1[maxm],to1[maxm],cnt1,val1[maxm];
ll dis[maxn];
stack<ll> S;
bool flag;
bool vis[maxn];
ll low[maxn],dfn[maxn],belong[maxn],sz[maxn],w[maxn];
ll n,m,k,p,Time,sc;
void add(ll u,ll v,ll c)
{
	to[++cnt]=v;
	next[cnt]=head[u];
	val[cnt]=c;
	head[u]=cnt;
}
void add1(ll u,ll v,ll c)
{
	to1[++cnt1]=v;
	next1[cnt1]=head1[u];
	val1[cnt1]=c;
	head1[u]=cnt1;
}

struct note{
	ll u,v,c;
}edge[maxm];

 void spfa(ll s)
 {
 	memset(dis,127,sizeof(dis));
 	vis[1]=1;dis[1]=0;
 	S.push(1);
 	while(!S.empty())
	{
		ll tmp=S.top();
		S.pop();
		for(ll edge=head[tmp];edge;edge=next[edge])
		{
			ll v=to[edge];
			if(dis[v]>dis[tmp]+val[edge])
			{
				dis[v]=dis[tmp]+val[edge];
				if(!vis[v]) S.push(v); 
			}
		}
	}
 }
 void dfs(ll u,ll len)
 {
 	if(!vis[u]) return;
 	if(len>dis[n]+k) return;
 	if(u==n) 
	 {
	 	ans++;
	}
 	if(ans>=10005) flag=1;
 	if(flag) return ;
 	for(ll edge=head[u];edge;edge=next[edge])
 	{
 		dfs(to[edge],len+val[edge]);
	 }
 }
 void dfs1(int u)
 {
 	vis[u]=1;
 	for(int edge=head1[u];edge;edge=next1[edge])
 	{
 		int v=to1[edge];
 		if(vis[v]) continue;
 		vis[v]=1;
 		dfs1(v);
	 }
 }
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	ll T;
	scanf("%lld",&T);
	while(T--)
	{		memset(head,0,sizeof(head));
		cnt=0;ans=0;
		scanf("%d %d %d %d",&n,&m,&k,&p);
		for(ll i=1;i<=m;i++)
		{
			ll u,v,c;
			scanf("%lld %lld %lld",&u,&v,&c);
			add(u,v,c);
			add1(v,u,c);
			edge[i].u=u,edge[i].v=v,edge[i].c=c;
		}
		spfa(1);
		
		memset(vis,0,sizeof(vis));
		dfs1(n);
		dfs(1,0);
		if(flag) ans=-1;
		cout<<ans%p<<endl;
	}	
}

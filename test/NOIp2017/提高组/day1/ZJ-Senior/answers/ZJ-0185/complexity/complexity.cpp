#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<stack>
using namespace std;
char Mode[5];
char pp[105],qq[105],rr[105],zz[105];
struct note{
	int opt,val;
};
stack<int> s1;
int map[30];
stack<note> S;
bool check(char* p,char* q)
{
	if(p[0]=='n'&&q[0]!='n') return 1;
	if(q[0]=='n') return 0;
	int tot1=0,tot2=0;
	for(int i=0;i<3;i++)
	{
		if(p[i]>='0'&&p[i]<='9') tot1=tot1*10+p[i]-'0';
		if(q[i]>='0'&&q[i]<='9') tot2=tot2*10+q[i]-'0';
	}
	return tot1>tot2;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--)
	{
		int n;
		scanf("%d%s",&n,Mode);
		int pos=0,tot=0;
		int len=strlen(Mode);
		for(int i=0;i<len;i++)
		if(Mode[i]=='^')
		{
			pos=i;break;
		}
		memset(map,0,sizeof(map));
		if(pos)
		for(int i=pos+1;i<len;i++)
		if(Mode[i]!=')')
		{
			tot=tot*10+(Mode[i]-'0');
		}
		int ans=0,sum=0;
		bool flag=1;
		for(int i=1;i<=n;i++)
		{
			scanf("%s",Mode);
			if(Mode[0]=='E')
			{
				if(S.empty()) flag=0;else
				{
					note tmp=S.top();
					map[tmp.opt]--;
					sum-=tmp.val;
					S.pop();
				}
			}else
			{
				scanf("%s%s%s",pp,qq,rr);
				note tmp;
				tmp.val=0;
				if(check(qq,rr))
				{
					while(!s1.empty()) s1.pop();
					s1.push(pp[0]-'a');
					map[pp[0]-'a']=1;
					int j=i+1;
						for(;j<=n;j++)
						{
							scanf("%s",zz);
							if(zz[0]=='E')
							{ 
								if(!s1.empty()) map[s1.top()]--,s1.pop();else 
								{
									flag=0;
									break;
								}
								if(s1.empty()) 
								{
									i=j;
									break;
								}
							}else 
							{
								scanf("%s %s %s",pp,qq,rr);
								if(map[pp[0]-'a']) 
								{
									flag=0;
								}
								s1.push(pp[0]-'a');
							}
						}	
				} else
				{
					if(qq[0]=='n') tmp.val=0;else
					{
						if(rr[0]=='n') tmp.val=1;
					}
					if(map[pp[0]-'a']) 
					{
						flag=0;
					}
					map[pp[0]-'a']++;tmp.opt=pp[0]-'a';S.push(tmp);
					sum+=tmp.val;
				}
				ans=max(ans,sum);
			}
		}	
		if(!S.empty()) 
		{
			flag=0;
		}
		if(!flag)
		puts("ERR");else
		if(ans!=tot)
		puts("No");else puts("Yes"); 
		while(!S.empty()) S.pop();
	}
}


#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<cstdio>
#include<cmath>
#define INF 1000
using namespace std;
int T,l;
bool vis[1005],vis1[10005];
char s[10005],ch[105][10];
bool c[10005],e[1005];
int d[1005];
char ssss[1005],sss[10005],ss[105];
int a[1005],b[1005];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		bool flag=true;//flag=true说明是n的几次方 
		int sum=0; bool zong=true;
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		scanf("%d%s",&l,s+1);
		int n=strlen(s+1);
		if (s[3]=='n') flag=true;
		else flag=false;
		if (flag){
			int ckr=0;
			for (int i=1;i<=n;i++){
				if (s[i]=='^'){
					ckr=i;
					break;
				}
			}
			for (int i=ckr+1;i<=n;i++){
				if (s[i]>='0'&&s[i]<='9') sum=sum*10+s[i]-'0';
				else break;
			}
		}
		else {
			int ckr=0;
			for (int i=1;i<=n;i++){
				if (s[i]=='('){
					ckr=i;
					break;
				}
			}
			for (int i=ckr+1;i<=n;i++){
				if (s[i]>='0'&&s[i]<='9') sum=sum*10+s[i]-'0';
				else break;
			}
		}
		if (!flag) sum=0;
		for (int i=1;i<=l;i++){
			scanf("%s",ss);
			if (ss[0]=='F'){
				cin>>ch[i][0];
				//scanf("%s",ch[i][0]);
				scanf("%s%s",sss+1,ssss+1);
				if (sss[1]=='n') a[i]=INF;
				else {
					int n=strlen(sss+1);
					for (int j=1;j<=n;j++) a[i]=a[i]*10+sss[j]-'0';
				}
				if (ssss[1]=='n') b[i]=INF;
				else {
					int n=strlen(ssss+1);
					for (int j=1;j<=n;j++) b[i]=b[i]*10+(ssss[j]-'0');
				}
				if (a[i]==INF&&b[i]==INF) d[i]=0;
				else if (a[i]>b[i]) d[i]=-1;
				else if (a[i]==INF&&b[i]<INF) d[i]=-1;
				else if (a[i]<INF&&b[i]<INF) d[i]=0;
				else if (a[i]<INF&&b[i]==INF) d[i]=1;
				c[i]=true;
			}
			else c[i]=false;
		}
		memset(vis,false,sizeof(vis));//vis[i]=true说明i用过了
		memset(vis1,false,sizeof(vis1));//vis1[i]=true说明i已经退出循环了 
		int i=1; int j=1; int tot=0; int bianlitot=0;//bianli F的个数 
		int Max1=0;
		int flag1=0;
		while (j<=l){
			if (c[j]==true){
				if (vis[ch[j][0]-'a']){
					if (zong) puts("ERR");
					zong=false;
				}
				vis[ch[j][0]-'a']=true;
				if (flag1==0){
					if (d[j]==1) tot++,e[j]=true;
					else if (d[j]==0) tot=tot,e[j]=true;
					else if (d[j]==-1) flag1++,e[j]=false;
				}
				else{
					tot=tot;
					e[j]=false;
					if (d[j]==-1) flag1++;
				}
				bianlitot++;
				Max1=max(Max1,tot);
			}
			else {
				bianlitot--;
				if (bianlitot<0){
					if (zong) puts("ERR");
					zong=false;
				}
				for (int k=j;k>=1;k--){
					if (c[k]==true&&vis1[k]==false){
						vis[ch[k][0]-'a']=false;
						vis1[k]=true;
						if (e[k]) tot=tot-d[k];
						else if (e[k]==false){
							tot=tot;
							if (d[k]==-1) flag1--;
						}
						break;
					}
				}
				Max1=max(Max1,tot);
			}
			j++;
		}
		if (!zong) continue;
		if (bianlitot!=0) puts("ERR");
		else{
			if (Max1==sum) puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<cstdio>
#include<cmath>
#define INF 10000005
#define N 200005
using namespace std;
int dp[1005][4005];
int head[N],q[N],x[N],y[N],z[N],d[N],d1[N],head1[N],to[N],f[N],s[N],belong[N],dfn[N],low[N],val[N],chu[N],tot1[N];
bool insize[N];
int T,n,m,k,p;
struct Edge{
	int nxt,to,step;
}e[300005];
int kk=0;
void add(int xx,int yy,int zz)
{
	e[++kk].nxt=head[xx];
	e[kk].to=yy;
	e[kk].step=zz;
	head[xx]=kk;
}
void SPFA()
{
	memset(d,127,sizeof(d));
	int left1=1; int right1=1;
	q[left1]=1; d[1]=0;
	while (left1<=right1){
		int u=q[left1];
		for (int i=head[u];i;i=e[i].nxt){
			int v=e[i].to;
			if (d[v]>d[u]+e[i].step){
				d[v]=d[u]+e[i].step;
				q[++right1]=v;
			}
		}
		left1++;
	}
}
struct Edge1{
	int nxt,to,step;
}e1[300005];
int kkk=0;
void add1(int xx,int yy,int zz)
{
	e1[++kkk].nxt=head1[xx];
	e1[kkk].to=yy;
	e1[kkk].step=zz;
	head1[xx]=kkk;
}
void SPFA1()
{
	memset(d1,127,sizeof(d1));
	int left1=1; int right1=1;
	q[left1]=n; d1[n]=0;
	while (left1<=right1){
		int u=q[left1];
		for (int i=head1[u];i;i=e1[i].nxt){
			int v=e1[i].to;
			if (d1[v]>d1[u]+e1[i].step){
				d1[v]=d1[u]+e1[i].step;
				q[++right1]=v;
			}
		}
		left1++;
	}
}
void work1()
{
	memset(head1,0,sizeof(head1));
	for (int i=1;i<=m;i++) add1(y[i],x[i],z[i]);
	SPFA1();
	memset(head,0,sizeof(head));  kk=0;
	for (int i=1;i<=m;i++){
		if (d[x[i]]+d1[y[i]]+z[i]==d[n]) add(x[i],y[i],0),to[y[i]]++;
	}
	memset(f,0,sizeof(f));
	int left1=1; int right1=0; q[1]=1; f[1]=1;
	for (int i=1;i<=n;i++) if (to[i]==0) q[++right1]=i;
	while (left1<=right1){
		int u=q[left1];
		for (int i=head[u];i;i=e[i].nxt){
			int v=e[i].to;
			to[v]--;
			f[v]=(f[v]+f[u])%p;
			if (to[v]==0) q[++right1]=v;
		}
		left1++;
	}
	printf("%d\n",f[n]);
}
int time1=0,top=0,cnt=0;
void chuzhan(int u)
{
	cnt++;
	while (s[top]!=u){
		insize[s[top]]=false;
		belong[s[top]]=cnt;
		top--;
	}
	insize[u]=false;
	belong[u]=cnt; top--;
}
void tarjan(int u)
{
	dfn[u]=low[u]=++time1;
	s[++top]=u; insize[u]=true;
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (!dfn[v]) tarjan(v),low[u]=min(low[u],low[v]);
		else if (insize[v]) low[u]=min(dfn[v],low[u]);
	}
	if (low[u]==dfn[u]) chuzhan(u);
}
struct QUEUE{
	int x,step;
}q1[N];
void work()
{
	memset(chu,0,sizeof(chu));
	//dp[i][j]表示到达i节点，时间用了j的方案数
	for (int i=1;i<=m;i++) chu[x[i]]++;
	time1=0; top=0; kkk=0; memset(head1,0,sizeof(head1));
	for (int i=1;i<=n;i++) if (!dfn[i]) tarjan(i);
	for (int i=1;i<=n;i++){
		for (int j=head[i];j;j=e[j].nxt){
			int v=e[j].to;
			if (belong[v]==belong[i]) val[belong[v]]+=e[j].step,tot1[belong[v]]++;
			else add1(belong[i],belong[v],0);
		}
	}
	int left1=1; int right1=0;
	memset(dp,0,sizeof(dp));
	for (int i=1;i<=cnt;i++){
		if (tot1[i]>1&&val[i]==0){
			puts("-1");
			return;
		}
	}
	for (int i=1;i<=cnt;i++) if (val[i]==0) val[i]=INF;
	for (int i=0;i<=1000;i++){
		if (i*val[belong[1]]>k+d[n]) break;
		else dp[1][i*val[belong[1]]]=1,q1[++right1].x=1,q1[++right1].step=i*val[belong[1]];
	}
	while (left1<=right1){
		int u=q1[left1].x; int haha=q1[left1].step;
		for (int i=head[u];i;i=e[i].nxt){
			int v=e[i].to; to[u]--;
			if (to[v]<0) continue;
			//to[u]--;
			for (int j=0;j<=1000;j++){
				if (j*val[belong[u]]+haha>k+d[n]) break;
				dp[v][j*val[belong[u]]+haha+e[i].step]=(dp[v][j*val[belong[u]]+haha+e[i].step]+dp[u][haha])%p;
				q1[++right1].x=v;
				q1[right1].step=haha+j*val[belong[u]]+e[i].step;
			}
		}
		left1++;
	}
	long long ans=0;
	for (int i=0;i<=d[n]+k;i++) ans=(ans+dp[n][i])%p;
	printf("%lld\n",ans);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		memset(head,0,sizeof(head));
		kk=0;
		for (int i=1;i<=m;i++){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			add(x[i],y[i],z[i]);
		}
		SPFA();
		if (k==0||(n>1000&&m>2000)) work1();
		else work();
	}
	return 0;
}

var kk,i,x,y,z,n,m,num,p,tot,max,count:longint;bo:boolean;
pre,other,len:array[0..200010]of longint;
vis,last:array[0..100010]of longint;
flag:array[0..100010]of boolean;
procedure add(p1,p2,p3:longint);
begin
  inc(tot);
  pre[tot]:=last[p1];
  last[p1]:=tot;
  other[tot]:=p2;
  len[tot]:=p3;
end;
function pd(x:longint):boolean;
var k:longint;
begin
  if flag[x] then exit(true);
  flag[x]:=true;
  k:=last[x];
  while k>0 do
  begin
    if vis[x]+len[k]=vis[other[k]] then
    begin
      if pd(other[x]) then exit(true);
    end;
    k:=pre[k];
  end;
  flag[x]:=false;
  exit(false);
end;
function spfa:longint;
var t,w,k:longint;
q:array[0..500000]of longint;
begin
  flag[1]:=true;
  t:=1;w:=1;q[1]:=1;vis[1]:=0;
  while t<=w do
  begin
    k:=last[q[t]];
    while k>0 do
    begin
      if vis[q[t]]+len[k]<vis[other[k]] then
      begin
        vis[other[k]]:=vis[q[t]]+len[k];
        if not flag[other[k]] then
        begin
          inc(w);
          q[w]:=other[k];
          flag[other[k]]:=true;
        end;
      end;
      k:=pre[k];
    end;
    flag[q[t]]:=false;
    inc(t);
  end;
  exit(vis[n]);
end;
procedure try(x,y:longint);
var k:longint;
begin
  if y>max then exit;
  if x=n then
  begin
    inc(count);
    exit;
  end;
  k:=last[x];
  while k>0 do
  begin
    try(other[k],y+len[k]);
    k:=pre[k];
  end;
end;
begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  readln(num);
  while num>0 do
  begin
    dec(num);

    fillchar(last,sizeof(last),0);
    tot:=0;
    readln(n,m,kk,p);
    for i:=1 to m do
    begin
      readln(x,y,z);
      add(x,y,z);
    end;
    bo:=true;
    for i:=1 to n do
    begin
      fillchar(flag,sizeof(flag),false);
      fillchar(vis,sizeof(vis),0);
      if pd(i) then
      begin
        bo:=false;break;
      end;
    end;
    if bo then
    begin
    fillchar(flag,sizeof(flag),false);
    for i:=1 to n do vis[i]:=2100000000;
    max:=spfa+kk;
    count:=0;
    try(1,0);
    writeln(count);
    end else writeln(-1);
  end;
  close(input);close(output);
end.

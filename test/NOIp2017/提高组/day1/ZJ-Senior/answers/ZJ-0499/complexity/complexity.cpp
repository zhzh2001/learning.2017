#include<cstdio>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
char q[110];int ans[110],del[110];
void work(int n,int m){
	char op,x,ch;int num=0,mx=0,t1,t2,z1,z2,ANS=0;
	while(n--){
		op=getchar();while(op!='E'&&op!='F')op=getchar();
		if(op=='E'){
			num--;if(num<0)ANS=-1;
		}
		else{
			x=getchar();while(x<'a'||x>'z')x=getchar();
			for(int i=1;i<=num;i++)if(x==q[i])ANS=-1;
			q[++num]=x;
			
			ch=getchar();while((ch<'0'||ch>'9')&&ch!='n')ch=getchar();
			t1=(ch=='n');
			if(ch!='n'){z1=0;while(ch>='0'&&ch<='9')z1=z1*10+ch-'0',ch=getchar();}
			
			ch=getchar();while((ch<'0'||ch>'9')&&ch!='n')ch=getchar();
			t2=(ch=='n');
			if(ch!='n'){z2=0;while(ch>='0'&&ch<='9')z2=z2*10+ch-'0',ch=getchar();}
			
			//printf("%d %d %d %d\n",t1,t2,z1,z2);
			if(ANS==-1)continue;
			del[num]=del[num-1];
			if((t1&&!t2)||(!t1&&!t2&&z1>z2))del[num]=1;
			else if(t1==t2)ans[num]=ans[num-1];
			else if(!t1&&t2){
				if(del[num])ans[num]=0;
				else ans[num]=ans[num-1]+1;
				if(ans[num]>mx)mx=ans[num];
			}
		}
	}
	if(num)ANS=-1;
	if(ANS==-1){puts("ERR");return;}
	if(mx==m)puts("Yes");else puts("No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read(),n,m;char ch;
	while(T--){
		n=read();ch=getchar();while(ch!='(')ch=getchar(); ch=getchar();
		if(ch=='1')m=0;else m=read();
		work(n,m);
	}
	return 0;
}

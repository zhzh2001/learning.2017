#include<cstdio>
#include<queue>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 100010
int n,m,k,mo;
struct edge{int to,nxt,w;}e[N<<1];int hed[N],cnt;
void add(int x,int y,int z){e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y,e[cnt].w=z;}
queue<int>q; int D[N],v[N*55];
void bfs(){
	while(!q.empty())q.pop();
	for(int i=1;i<=n;i++)D[i]=1e9,v[i]=0;
	D[1]=0,v[1]=1,q.push(1); int x;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=hed[x];i;i=e[i].nxt)if(D[x]+e[i].w<D[e[i].to]){
			D[e[i].to]=D[x]+e[i].w;if(!v[e[i].to])v[e[i].to]=1,q.push(e[i].to);
		}
		v[x]=0;
	}
	for(int i=1;i<=n;i++)printf("%d ",D[i]); puts("");
}
struct node{int x,y;}p;
queue<node>Q; int f[N][55],id[N][55],S[N][55];
void BFS(){
	int tot=0;for(int i=1;i<=n;i++)for(int j=0;j<=k;j++)f[i][j]=S[i][j]=0,id[i][j]=++tot;
	while(!Q.empty())Q.pop();
	f[1][0]=1; p.x=1,p.y=0,Q.push(p); node z; int x,y,w;
	/*for(int y=0;y<=k;y++)for(int x=1;x<=n;x++)if(f[x][y]){
		if(f[x][y]>1)printf("%d %d %d\n",x,y,f[x][y]);
		for(int i=hed[x];i;i=e[i].nxt)if(D[e[i].to]!=1e9){
			w=D[x]+y+e[i].w-D[e[i].to]; if(w>k)continue;
			//if(w==y&&e[i].to<=x){puts("-1");return;}
			f[e[i].to][w]+=f[x][y];if(f[e[i].to][w]>=mo)f[e[i].to][w]-=mo;
		}
	}*/
	
	while(!Q.empty()){
		z=Q.front(),Q.pop(); x=z.x,y=z.y;
		//printf("%d %d %d\n",x,y,f[x][y]);
		//S[x][y]++;if(S[x][y]>n){puts("-1");return;}
		//printf("%d %d %d\n",x,y,f[x][y]);
		for(int i=hed[x];i;i=e[i].nxt)if(D[e[i].to]!=1e9){
			w=D[x]+y+e[i].w-D[e[i].to]; if(w>k)continue;
			f[e[i].to][w]+=f[x][y];if(f[e[i].to][w]>=mo)f[e[i].to][w]-=mo;
			//printf("%d %d\n",e[i].to,w);
			if(!v[id[e[i].to][w]])p.x=e[i].to,p.y=w,Q.push(p);
		}
		v[id[x][y]]=0;
	}
	int ans=0;for(int i=0;i<=k;i++){ans+=f[n][i];if(ans>=mo)ans-=mo;}printf("%d\n",ans);
}
int main(){
	freopen("park3.in","r",stdin);
	//freopen("park.out","w",stdout);
	int T=read(); 
	while(T--){
		n=read(),m=read(),k=read(),mo=read();
		for(int i=1;i<=n;i++)hed[i]=0; cnt=0;
		for(int i=1,x,y,z;i<=m;i++)x=read(),y=read(),z=read(),add(x,y,z);
		bfs();
		BFS();
	}
}

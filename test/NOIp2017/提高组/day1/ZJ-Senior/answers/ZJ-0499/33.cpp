#include<cstdio>
#include<queue>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 100010
int n,m,k,mo;
struct edge{int to,nxt,w;}e[N<<1];int hed[N],cnt;
void add(int x,int y,int z){e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y,e[cnt].w=z;}
struct EDGE{int to,nxt;}E[N<<1];int HED[N],CNT;
void ADD(int x,int y){E[++CNT].nxt=HED[x];HED[x]=CNT;E[CNT].to=y;}
queue<int>q; int D[N],v[N*55];
void bfs(){
	while(!q.empty())q.pop();
	for(int i=1;i<=n;i++)D[i]=1e9,v[i]=0;
	D[1]=0,v[1]=1,q.push(1); int x;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=hed[x];i;i=e[i].nxt)if(D[x]+e[i].w<D[e[i].to]){
			D[e[i].to]=D[x]+e[i].w;if(!v[e[i].to])v[e[i].to]=1,q.push(e[i].to);
		}
		v[x]=0;
	}
	//for(int i=1;i<=n;i++)printf("%d ",D[i]); puts("");
}
int f[N][55],id[N][55];
void BFS(){
	int tot=0;for(int i=1;i<=n;i++)for(int j=0;j<=k;j++)f[i][j]=0,id[i][j]=++tot;
	f[1][0]=1; int w;
	for(int y=0;y<=k;y++){
		while(!q.empty())q.pop();
		for(int x=1;x<=n;x++)
			if(f[x][y])q.push(x),v[x]=1;else v[x]=0;
		while(!q.empty()){
			int x=q.front(); q.pop();
			for(int i=hed[x];i;i=e[i].nxt)if(D[e[i].to]!=1e9){
				w=D[x]+y+e[i].w-D[e[i].to]; if(w>k)continue;
				f[e[i].to][w]+=f[x][y];if(f[e[i].to][w]>=mo)f[e[i].to][w]-=mo;
				if(!v[e[i].to]&&w==y)v[e[i].to]=1,q.push(e[i].to);
			}
			v[x]=0;
		}
	}
	int ans=0;for(int i=0;i<=k;i++){ans+=f[n][i];if(ans>=mo)ans-=mo;}printf("%d\n",ans);
}
int S[N],TOT;
void pre(){
	while(!q.empty())q.pop(); TOT=0; int x;
	for(int i=1;i<=n;i++)if(D[i]!=1e9)TOT++;
	for(int i=1;i<=n;i++)if(D[i]!=1e9)if(!S[i])q.push(i),TOT--;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=HED[x];i;i=E[i].nxt)if(D[E[i].to]!=1e9){
			S[E[i].to]--; if(!S[E[i].to])TOT--,q.push(E[i].to);
		}
	}
}
int main(){
	freopen("park1.in","r",stdin);
	//freopen("park.out","w",stdout);
	int T=read(); 
	while(T--){
		n=read(),m=read(),k=read(),mo=read();
		n=read(),m=read(),k=read(),mo=read();
		for(int i=1;i<=n;i++)hed[i]=HED[i]=S[i]=0; cnt=CNT=0;
		for(int i=1,x,y,z;i<=m;i++){
			x=read(),y=read(),z=read(),add(x,y,z);
			if(!z)ADD(y,x),S[x]++,SS[x]++;
		}
		bfs();
		pre();
		BFS();
	}
}

#include <bits/stdc++.h>
using namespace std;
const int INF = 2333333;
const int N = 2333;
int T, rd, wd, ERR, top, n;
int vis[N], S[N], C[N];
int _read()
{
    char tmp[100];
    scanf("%s", tmp);
    if (tmp[0] == 'n') return INF;
    int r = 0;
    sscanf(tmp, "%d", &r);
    return r;
}
int main()
{
    freopen("complexity.in", "r", stdin);
    freopen("complexity.out", "w", stdout);
    scanf("%d", &T);
    while (T --)
    {
        rd = wd = ERR = top = 0;
        memset(vis, 0, sizeof vis);
        char c[100];
        scanf("%d%s", &n, c);
        if (c[2] == '1') rd = 0;
        else sscanf(c, "O(n^%d)", &rd);
        for (int i = 1; i <= n; ++ i)
        {
            char w[100];
            scanf("%s", w);
            if (w[0] == 'F')
            {
                char v[100];
                scanf("%s", v);
                if (vis[v[0] - 'a']) ERR = 1;
                int l = _read(), r = _read();
                top ++;
                if (l > r) S[top] = -INF;
                else if (r == INF && l != INF) S[top] = S[top - 1] + 1;
                else S[top] = S[top - 1];
                C[top] = v[0];
                vis[v[0] - 'a'] = 1;
                wd = max(wd, S[top]);
            }
            else 
            {
                if (!top) ERR = 1;
                else 
                {
                    vis[C[top] - 'a'] = 0;
                    top --;
                }
            }
        }
        if (top) ERR = 1;
        if (ERR) puts("ERR");
        else if (wd == rd) puts("Yes");
        else puts("No");
    }
}

#include <bits/stdc++.h>
using namespace std;
const int N = 5e7;
int f[N];
int check(int a, int b)
{
    if (a > b) swap(a, b);
    f[0] = 1;
    int w = 1;
    for (int i = 1; ; ++ i)
    {
        f[i] = 0;
        if (i >= a) f[i] |= f[i - a];
        if (i >= b) f[i] |= f[i - b];
        if (!f[i]) w = i;
        if (i - w >= a) return w;
    }
}
int a, b;
int main()
{
    cin >> a >> b;
    cout << check(a, b) << endl;
}

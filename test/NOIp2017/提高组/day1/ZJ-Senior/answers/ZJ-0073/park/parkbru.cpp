#include <bits/stdc++.h>
using namespace std;
const int N = 300000;
const int M = 11000000;
const int INF = 1000000000;
#define fr first 
#define sc second
#define pb push_back
#define mp make_pair
#define pI pair <int, int>

vector <int> bi[N], ci[N];
vector <int> bt[N], ct[N];
int dis[N], vis[N];
int LOOP, ans, T;
priority_queue <pI> H;
int n, m, k, p;
void dfs(int t, int f = -1, int d = 0)
{
    if (d > k) return;
    if (t == n) ans ++;
    if (t == f) LOOP = 1;
    if (LOOP) return;
    for (int i = 0; i < bi[t].size(); ++ i)
        dfs(bi[t][i], (ci[t][i] == 0? abs(f): -bi[t][i]), d + ci[t][i] - (dis[t] - dis[bi[t][i]]));
}
int main()
{
    freopen("park.in", "r", stdin);
    freopen("park.ans", "w", stdout);
    scanf("%d", &T);
    while (T --)
    {        
        // cerr << T << endl;
        LOOP = ans = 0;
        scanf("%d%d%d%d", &n, &m, &k, &p);
        for (int i = 1; i <= n; ++ i) bi[i].clear(), ci[i].clear();
        for (int i = 1; i <= n; ++ i) bt[i].clear(), ct[i].clear();
        for (int i = 1; i <= m; ++ i)
        {
            int a, b, c;
            scanf("%d%d%d", &a, &b, &c);
            bi[a].pb(b); ci[a].pb(c);
            bt[b].pb(a); ct[b].pb(c);
        }
        while (!H.empty()) H.pop();
        for (int i = 1; i <= n; ++ i) vis[i] = 0, dis[i] = INF;
        H.push(mp(0, n)); dis[n] = 0;
        while (!H.empty())
        {
            int t;
            do t = H.top().sc, H.pop();
            while (vis[t] && !H.empty());
            if (vis[t]) break; else vis[t] = 1;
            for (int i = 0; i < bt[t].size(); ++ i)
                if (dis[t] + ct[t][i] < dis[bt[t][i]])
                {
                    dis[bt[t][i]] = dis[t] + ct[t][i];
                    H.push(mp(-dis[bt[t][i]], bt[t][i]));
                }
        }
        if (dis[n] == INF) {puts("0"); continue;}
        dfs(1);
        ans %= p;
        if (LOOP) puts("-1"); else cout << ans << endl;
    }
}

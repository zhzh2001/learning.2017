#include <bits/stdc++.h>
using namespace std;
#define fr first 
#define sc second
#define pb push_back
#define mp make_pair
#define pI pair <int, int>
int T;
int n, m, k, p;
const int N = 300000;
const int M = 11000000;
const int INF = 1000000000;
vector <int> bi[N], ci[N];
int vis[N], dis[N];
int lll[M], bbb[M], nnn[M], tot, gth[M], inn[M];
int f[M], ans;
queue <int> Q;
priority_queue <pI> H;
void ADD(int &t, int d) {t += d; if (t >= p) t -= p;}
int calc(int t, int w)
{
    return (t - 1) * (k + 1) + w + 1;
}
void add_edge(int a, int b)
{
    nnn[++ tot] = lll[a]; lll[a] = tot; bbb[tot] = b;
    // cerr << a << " " << b << endl;
}
int main()
{
    // freopen("park.in", "r", stdin);
    // freopen("park.out", "w", stdout);
    scanf("%d", &T);
    while (T --)
    {
        scanf("%d%d%d%d", &n, &m, &k, &p);
        for (int i = 1; i <= n; ++ i) bi[i].clear(), ci[i].clear();
        for (int i = 1; i <= m; ++ i)
        {
            int a, b, c;
            scanf("%d%d%d", &a, &b, &c);
            bi[a].pb(b); ci[a].pb(c);
        }
        while (!H.empty()) H.pop();
        for (int i = 1; i <= n; ++ i) vis[i] = 0, dis[i] = INF;
        H.push(mp(0, 1)); dis[1] = 0;
        while (!H.empty())
        {
            int t;
            do t = H.top().sc, H.pop();
            while (vis[t] && !H.empty());
            if (vis[t]) break; else vis[t] = 1;
            for (int i = 0; i < bi[t].size(); ++ i)
                if (dis[t] + ci[t][i] < dis[bi[t][i]])
                {
                    dis[bi[t][i]] = dis[t] + ci[t][i];
                    H.push(mp(-dis[bi[t][i]], bi[t][i]));
                }
        }
        for (int i = 1; i <= n * (k + 1); ++ i) lll[i] = 0;
        for (int i = 1; i <= tot; ++ i) bbb[i] = nnn[i] = 0;
        tot = 0;
        for (int i = 1; i <= n; ++ i)
            for (int j = 0; j < bi[i].size(); ++ j)
                for (int p = 0, w = ci[i][j] - (dis[bi[i][j]] - dis[i]); p + w <= k; ++ p)
                    add_edge(calc(bi[i][j], p + w), calc(i, p));
        
        for (int i = 1; i <= n * (k + 1); ++ i) gth[i] = 0;
        while (!Q.empty()) Q.pop();
        for (int i = 0; i <= k; ++ i) Q.push(calc(n, i)), gth[calc(n, i)] = 1;
        while (!Q.empty())
        {
            int t = Q.front(); Q.pop();
            for (int p = lll[t]; p; p = nnn[p])
                if (!gth[bbb[p]])
                {
                    gth[bbb[p]] = 1;
                    Q.push(bbb[p]);
                }
        }
        // cerr << "!" << endl;
        for (int i = 1; i <= n * (k + 1); ++ i) lll[i] = 0;
        for (int i = 1; i <= tot; ++ i) bbb[i] = nnn[i] = 0;
        tot = 0;
        for (int i = 1; i <= n; ++ i)
            for (int j = 0; j < bi[i].size(); ++ j)
                for (int p = 0, w = ci[i][j] - (dis[bi[i][j]] - dis[i]); p + w <= k; ++ p)
                    if (gth[calc(i, p)] && gth[calc(bi[i][j], p + w)])
                        add_edge(calc(i, p), calc(bi[i][j], p + w));
        for (int i = 1; i <= n * (k + 1); ++ i) inn[i] = 0;
        for (int i = 1; i <= n * (k + 1); ++ i)
            for (int p = lll[i]; p; p = nnn[p])
                inn[bbb[p]] ++;
        while (!Q.empty()) Q.pop();
        for (int i = 1; i <= n * (k + 1); ++ i)
            if (!inn[i]) Q.push(i);
        for (int i = 1; i <= n * (k + 1); ++ i) f[i] = 0;
        f[1] = 1;
        while (!Q.empty())
        {
            int t = Q.front(); Q.pop();
            for (int p = lll[t]; p; p = nnn[p])
            {
                ADD(f[bbb[p]], f[t]);
                inn[bbb[p]] --;
                if (inn[bbb[p]] == 0) Q.push(bbb[p]);
            }
        }
        ans = 0;
        for (int i = 0; i <= k; ++ i) ADD(ans, f[calc(n, i)]);
        for (int i = 1; i <= n * (k + 1); ++ i) if (inn[i]) ans = -1;
        printf("%d\n", ans);
    }
}

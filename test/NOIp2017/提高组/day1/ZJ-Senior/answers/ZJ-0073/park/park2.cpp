#include <bits/stdc++.h>
using namespace std;
#define fr first 
#define sc second
#define pb push_back
#define mp make_pair
#define pI pair <int, int>
int T;
int n, m, k, p;
const int N = 300000;
const int M = 11000000;
const int INF = 1000000000;
int vis[N], dis[N];
int lll[M], bbb[M], nnn[M], tot, inn[M];
int ll1[N], bb1[N], cc1[N], nn1[N], tot1;
int ll2[N], bb2[N], cc2[N], nn2[N], tot2;

int f[M], ans;
// queue <int> Q;
int buff[M];
struct wei_shen_me_yao_ka_chang_shu
{
    int fr, bc;
    int empty() {return fr == bc;}
    void clear() {fr = bc = 0;}
    int front() {return buff[fr];}
    void pop() {fr ++;}
    void push(int t) {buff[bc ++] = t;}
} Q;
priority_queue <pI> H;
void ADD(int &t, int d) {t += d; if (t >= p) t -= p;}
inline int calc(int t, int w)
{
    return (t - 1) * (k + 1) + w + 1;
}
inline void add_edge(int a, int b)
{
    nnn[++ tot] = lll[a]; lll[a] = tot; bbb[tot] = b;
    // cerr << a << " " << b << endl;
}
int main()
{
    freopen("park.in", "r", stdin);
    freopen("park2.out", "w", stdout);
    scanf("%d", &T);
    while (T --)
    {
        scanf("%d%d%d%d", &n, &m, &k, &p);
        for (int i = 1; i <= n; ++ i) ll1[i] = 0;
        for (int i = 1; i <= tot1; ++ i) bb1[i] = cc1[i] = nn1[i] = 0;
        tot1 = 0;
        for (int i = 1; i <= n; ++ i) ll2[i] = 0;
        for (int i = 1; i <= tot2; ++ i) bb2[i] = cc2[i] = nn2[i] = 0;
        tot2 = 0;
        
        for (int i = 1; i <= m; ++ i)
        {
            int a, b, c;
            scanf("%d%d%d", &a, &b, &c);
            nn1[++ tot1] = ll1[a]; ll1[a] = tot1; bb1[tot1] = b; cc1[tot1] = c;
            nn2[++ tot2] = ll2[b]; ll2[b] = tot2; bb2[tot2] = a; cc2[tot2] = c;
        }
        
        while (!H.empty()) H.pop();
        for (int i = 1; i <= n; ++ i) vis[i] = 0, dis[i] = INF;
        H.push(mp(0, n)); dis[n] = 0;
        while (!H.empty())
        {
            int t;
            do t = H.top().sc, H.pop();
            while (vis[t] && !H.empty());
            if (vis[t]) break; else vis[t] = 1;
            for (int p = ll2[t]; p; p = nn2[p])
                if (dis[t] + cc2[p] < dis[bb2[p]])
                {
                    dis[bb2[p]] = dis[t] + cc2[p];
                    H.push(mp(-dis[bb2[p]], bb2[p]));
                }
        }
        if (dis[0] == INF) {puts("0"); continue;}
        
        for (int i = 1; i <= n * (k + 1); ++ i) lll[i] = 0;
        for (int i = 1; i <= tot; ++ i) bbb[i] = nnn[i] = 0;
        tot = 0;
        for (int i = 1; i <= n; ++ i)
            for (int j = ll1[i]; j; j = nn1[j])
                if (dis[i] != INF && dis[bb1[j]] != INF)
                    for (int p = 0, w = cc1[j] - (dis[i] - dis[bb1[j]]); p + w <= k; ++ p)
                        add_edge(calc(i, p), calc(bb1[j], p + w));
        for (int i = 1; i <= n * (k + 1); ++ i) inn[i] = 0;
        for (int i = 1; i <= n * (k + 1); ++ i)
            for (int p = lll[i]; p; p = nnn[p])
                inn[bbb[p]] ++;
                
        Q.clear(); // while (!Q.empty()) Q.pop();
        for (int i = 1; i <= n * (k + 1); ++ i)
            if (!inn[i]) Q.push(i);
        for (int i = 1; i <= n * (k + 1); ++ i) f[i] = 0;
        f[1] = 1;
        while (!Q.empty())
        {
            int t = Q.front(); Q.pop();
            for (int p = lll[t]; p; p = nnn[p])
            {
                ADD(f[bbb[p]], f[t]);
                inn[bbb[p]] --;
                if (inn[bbb[p]] == 0) Q.push(bbb[p]);
            }
        }
        ans = 0;
        for (int i = 0; i <= k; ++ i) ADD(ans, f[calc(n, i)]);
        for (int i = 1; i <= n * (k + 1); ++ i) if (inn[i]) ans = -1;
        printf("%d\n", ans);
    }
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<queue>
#define maxn 200010
using namespace std;
int n,m,i,j,k,l,K,p,T,next[maxn],first[maxn],end[maxn],a[maxn],f[maxn][55],c[maxn],x,y,z,u,v,g[maxn],in[maxn],head,tail,ans,h[maxn],tmp;
bool b[maxn],bb[maxn],bo;
struct type
{
	int x,y;
}t;
bool operator < (type x,type y)
{
	return x.y<y.y||(x.y==y.y&&h[x.x]<h[y.x]);
}
priority_queue<type>q;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	register int i,j;
	while(T--)
	{
		memset(c,0x3f,sizeof(c));
		memset(b,0,sizeof(b));
		memset(first,0,sizeof(first));
		memset(in,0,sizeof(in));
		memset(a,0,sizeof(a));
		memset(bb,0,sizeof(bb));
		scanf("%d%d%d%d",&n,&m,&K,&p);
		for(i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			next[i]=first[x];
			first[x]=i;
			end[i]=y;
			//cout<<i<<" "<<y<<endl;
			a[i]=z;
			if(!z)in[y]++;
		}
		head=tail=0;
		for(i=1;i<=n;i++)
			if(!in[i])g[++tail]=i;
		while(head<tail)
		{
			u=g[++head];
			for(i=first[u];i;i=next[i])
			{
				v=end[i];
				if(a[i])continue;
				if(!--in[v])g[++tail]=v;
			}
		}
		for(i=1;i<=tail;i++)
			h[g[i]]=i;
		for(i=1;i<=n;i++)
			if(in[i])bb[i]=1;
		if(bb[1])
		{
			printf("-1\n");
			continue;
		}
		for(i=1;i<=n;i++)
			for(j=0;j<=k;j++)
				f[i][j]=0;
		c[1]=0;
		f[1][0]=b[1]=1;
		
		for(i=first[1];i;i=next[i])
		{
			v=end[i];
			if(bb[v])continue;
			c[v]=min(c[v],a[i]);
			f[v][0]=1;
			q.push((type){v,c[v]});
		}
		while(!q.empty())
		{
			tmp++;
			t=q.top();
			q.pop();
			bo=0;
			while(b[t.x])
			{
				if(q.empty())
				{
					bo=1;
					break;
				}
				t=q.top();
				q.pop();
			}
			if(bo)break;
			u=t.x;
			//cout<<"??? "<<u<<" "<<t.x<<endl;
			b[u]=1;
			for(i=first[u];i;i=next[i])
			{
				v=end[i];
				if(bb[v])continue;
				//cout<<u<<"->"<<v<<" "<<c[u]<<" "<<a[i]<<" "<<c[v]<<endl;
				if(c[u]+a[i]==c[v])f[v][0]=(f[v][0]+f[u][0])%p;
				if(c[u]+a[i]<c[v])
				{
					f[v][0]=f[u][0];
					c[v]=c[u]+a[i];
					q.push((type){v,c[v]});
				}
			}
		}
		//cout<<"LLL "<<q.top().S<<endl;
		//for(i=1;i<=n;i++)
		//	cout<<c[i]<<" "<<f[i][0]<<endl;
		//cout<<endl;
		if(c[n]>1e9)
		{
			printf("-1\n");
			continue;
		}
		memset(g,0,sizeof(g));
		memset(in,0,sizeof(in));
		for(i=1;i<=n;i++)
			for(j=first[i];j;j=next[j])
			{
				v=end[j];
				if(c[i]+a[j]==c[v])in[v]++;
			}
		head=0,tail=1;
		g[1]=1;
		while(head<tail)
		{
			u=g[++head];
			for(i=first[u];i;i=next[i])
			{
				v=end[i];
				if(c[u]+a[i]!=c[v])continue;
				if(!--in[v])g[++tail]=v;
			}
		}
		//cout<<"??? "<<tail<<endl;
		for(j=1;j<=K;j++)
			for(i=1;i<=tail;i++)
			{
				u=g[i];
				for(k=first[u];k;k=next[k])
				{
					//v=end[k];
					//cout<<k<<" "<<u<<"->"<<v<<endl;
					//cout<<u<<"->"<<v<<" "<<c[u]<<" "<<a[k]<<" "<<c[v]<<" "<<(c[u]+a[k]-c[v])<<" "<<j<<endl;
					if(j>(c[u]+a[k]-c[v]))
					{
						f[v][j]=(f[v][j]+f[u][j-(c[u]+a[k]-c[v])])%p;
						//cout<<j<<" "<<u<<"->"<<v<<" "<<j-(c[u]+a[k]-c[v])<<endl;
					}
				}
			}
		ans=0;
		for(j=0;j<=K;j++)
			ans=(ans+f[n][j])%p;
		printf("%d\n",ans);
	}
}
/*
1
8 11 0 1000000007
1 2 1
1 3 1
1 4 2
2 4 1
3 4 1
4 5 1
4 6 1
5 7 1
6 7 1
6 8 1
7 8 1
*/

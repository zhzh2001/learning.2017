#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
char s[1100];
bool b[260],err,y,z;
int T,n,i,x,stack[1100],top,now[1100],ans,c[1100];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&n);
		scanf("%s",s);
		x=ans=top=err=0;
		if(strlen(s)>4)
			for(i=4;s[i]!=')';i++)
				x=x*10+s[i]-'0';
		memset(b,0,sizeof(b));
		while(n--)
		{
			scanf("%s",s);
			if(s[0]=='F')
			{
				scanf("%s",s);
				if(!err)
				{
					if(b[s[0]-'a'])err=1;
					b[s[0]-'a']=1;
					c[++top]=s[0]-'a';
				}
				scanf("%s",s);
				if(!err)
				{
					if(s[0]=='n')y=1;else y=0;
				}
				scanf("%s",s);
				if(!err)
				{
					if(s[0]=='n')z=1;else z=0;
					if((z&&y)||(!z&&!y))
					{
						stack[top]=1;
						now[top]=now[top-1];
					}else
					if(y&&!z)
					{
						stack[top]=0;
						now[top]=0;
					}else
					{
						stack[top]=2;
						if(top==1||now[top-1]>0)now[top]=now[top-1]+1;else now[top]=0;
					}
					ans=max(ans,now[top]);
				}
			}else
			{
				if(err)continue;
				if(--top<0)err=1;
				b[c[top+1]]=0;
			}
		}
		if(top>0||err)printf("ERR\n");else if(ans==x)printf("Yes\n");else printf("No\n");
	}
}

#include<stdio.h>
#include<algorithm> 
#include<queue>
#define pr pair<int,int>
#define mp make_pair<>
#define sc second
using namespace std;
priority_queue<pr,vector<pr>,greater<pr> > Q;
int n,num,id,mod,top,ti,q[5100005],xx[10000005],yy[10000005],in[5100005],s[5100005],belong[5100005],x[200005],y[200005],val[200005],v[10000005],v1[200005],next[10000005],head[5100005],dis[5100005],vis[100005],map[100005][52],dfn[5100005],low[5100005],stack[5100005],instack[5100005],dp[5100005];
void add(int a,int b,int c){
	num++;
	v[num]=b;
	if (c!=-1) v1[num]=c;
	next[num]=head[a];
	head[a]=num;
}
void Dijkstra(){
	for (int i=1; i<=n; i++) {
		dis[i]=1e9;
		vis[i]=0;
	}
	dis[1]=0;
	Q.push(mp(0,1));
	while (!Q.empty()){
		int u=Q.top().sc;
		Q.pop();
		if (vis[u]) continue;
		vis[u]=1;
		for (int i=head[u]; i; i=next[i]){
			int V=v[i];
			if (dis[V]>dis[u]+v1[i]){
				dis[V]=dis[u]+v1[i];
				Q.push(mp(dis[V],V));
			}
		}
	}
}
void Tarjan(int u){
	dfn[u]=low[u]=++ti;
	stack[++top]=u;
	instack[u]=1;
	for (int i=head[u]; i; i=next[i]){
		int V=v[i];
		if (!dfn[V]){
			Tarjan(V);
			low[u]=min(low[u],low[V]);
		}
		else if (instack[V]) low[u]=min(low[u],dfn[V]);
	}
	if (low[u]==dfn[u]){
		int V=0;
		id++;
		s[id]=0;
		while (V!=u && top){
			V=stack[top--];
			s[id]++;
			belong[V]=id;
			instack[V]=0;
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t;
	scanf("%d",&t);
	while (t--){
		int m,k,p;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		mod=p;
		num=0;
		for (int i=1; i<=n; i++) head[i]=0;
		int cnt=0;
		for (int i=1; i<=n; i++)
			for (int j=0; j<=k; j++)
				map[i][j]=++cnt;
		for (int i=1; i<=m; i++){
			scanf("%d%d%d",&x[i],&y[i],&val[i]);
			add(x[i],y[i],val[i]);
		}
		Dijkstra();
		num=0;
		for (int i=1; i<=cnt; i++) head[i]=0;
		for (int i=1; i<=m; i++)
			for (int j=0; j<=k; j++){
				int xx=dis[x[i]]+j+val[i]-dis[y[i]];
				if (xx>k) break;
				add(map[x[i]][j],map[y[i]][xx],-1);
			}
		id=0;
		ti=0;
		top=0;
		for (int i=1; i<=cnt; i++) low[i]=dfn[i]=0;
		Tarjan(1);
		int numm1=0;
		for (int u=1; u<=cnt; u++){
			if (!dfn[u]) continue;
			for (int i=head[u]; i; i=next[i]){
				int V=v[i];
				if (!dfn[u] || !dfn[V]) continue;
				if (belong[u]!=belong[V]){
					xx[++numm1]=belong[u];
					yy[numm1]=belong[V];
				}
			}
		}
		num=0;
		for (int i=1; i<=cnt; i++) head[i]=0;
		for (int i=1; i<=numm1; i++) {
			add(xx[i],yy[i],-1);
			in[yy[i]]++;
		}
		for (int i=1; i<=id; i++) dp[i]=0;
		dp[belong[1]]=1;
		for (int i=1; i<=id; i++)
			if (s[i]>1) dp[i]=-1;
		int Head=0;
		int tail=0;
		for (int i=1; i<=id; i++)
			if (!in[i]) q[++tail]=i;
		while (Head<tail){
			Head++;
			int u=q[Head];
			for (int i=head[u]; i; i=next[i]){
				int V=v[i];
				in[V]--;
				if (dp[u]==-1) dp[V]=-1;
				else if (dp[V]!=-1) (dp[V]+=dp[u])%=mod;
				if (!in[V]){
					tail++;
					q[tail]=V;
				}
			}
		}
		int ans=0;
		int ff=0;
		for (int i=0; i<=k; i++)
			if (dp[belong[map[n][i]]]==-1){
				printf("-1\n");
				ff=1;
				break;
			}
			else (ans+=dp[belong[map[n][i]]])%=mod;
		if (!ff) printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

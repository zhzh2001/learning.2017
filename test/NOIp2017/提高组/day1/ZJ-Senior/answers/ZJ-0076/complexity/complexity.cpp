#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
char s1[105][1005],s2[105][1005],s3[105][1005],s4[105][1005],s[1000005];
int num,len,root,instack[1005],stack[1000005],v[1000005],v1[1000005],next[1000005],head[1000005];
void add(int a,int b,int c){
	num++;
	v[num]=b;
	v1[num]=c;
	next[num]=head[a];
	head[a]=num;
}
void pre(){
	int top=0;
	stack[0]=root;
	int nnum=0;
	for (int i=1; i<=len; i++){
		if (s1[i][0]=='F'){
			stack[++top]=++nnum;
			if (s3[i][0]=='n'){
				if (s4[i][0]=='n') add(stack[top-1],stack[top],0);
				else add(stack[top-1],stack[top],-1);
			}
			else{
				if (s4[i][0]=='n') add(stack[top-1],stack[top],1);
				else{
					int len1=strlen(s3[i]);
					int len2=strlen(s4[i]);
					int sum1=0,sum2=0;
					for (int j=0; j<len1; j++) sum1=sum1*10+s3[i][j]-'0';
					for (int j=0; j<len2; j++) sum2=sum2*10+s4[i][j]-'0';
					//printf("%d %d\n",sum1,sum2);
					if (sum1<=sum2) add(stack[top-1],stack[top],0);
					else add(stack[top-1],stack[top],-1);
				}
			}
		}
		else top--;
	}
}
int check(){
	int top=0;
	for (int i=0; i<26; i++) instack[i]=0;
	for (int i=1; i<=len; i++){
		if (s1[i][0]=='F') {
			int xx=s2[i][0]-'a';
			if (instack[xx]) return 0;
			instack[xx]=1;
			stack[++top]=xx;
		}
		else {
			int xx=stack[top--];
			instack[xx]=0;
		}
		if (top<0) return 0;
	}
	if (top) return 0;
	return 1;
}
int dfs(int u,int deep){
	int maxx=deep;
	for (int i=head[u]; i; i=next[i]){
		int V=v[i];
		if (v1[i]==-1) continue;
		maxx=max(maxx,dfs(V,deep+v1[i]));
	}
	return maxx;
}
int main(){
	freopen("complexity.in","r",stdin); 
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	while (t--){
		scanf("%d%s",&len,s);
		int lenn=strlen(s);
		int id;
		for (id=0; id<lenn; id++) if (s[id]=='(') break;
		int mi=0;
		id++;
		if (s[id]!='1'){
			id+=2;
			for (int i=id; i<lenn; i++){
				if (s[i]==')') break;
				mi=mi*10+s[i]-'0';
			}
		}
		for (int i=1; i<=len; i++) {
			scanf("%s",s1[i]);
			if (s1[i][0]=='F') scanf("%s%s%s",s2[i],s3[i],s4[i]);
		}
		if (!check()){
			printf("ERR\n");
			continue;
		}
		num=0;
		for (int i=0; i<=100; i++) head[i]=0;
		root=0;
		pre();
		int deep=dfs(root,0);
		if (deep==mi) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

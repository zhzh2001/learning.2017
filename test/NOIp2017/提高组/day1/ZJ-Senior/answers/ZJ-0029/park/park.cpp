#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define Pa pair<int,int>
#define fi first
#define se second
#define mem(a) memset(a,0,sizeof(a))
using namespace std;
const int N=200100;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,K,P,d[N],g[N];
int L,h[N],_h[N],ne[N*2],to[N*2],w[N*2],ff[N];
int XX[N],YY[N],KK[N];
int L0,h0[N],ne0[N*2],to0[N*2],q[N],id[N],be[N],du[N];
int f[51][N],ans;
inline void A(int&x,int y){
	x+=y;
	if(x>=P)
	x-=P;
}
struct cqz{
	int tot;Pa h[N];
	inline int size(){
		return tot;
	}
	inline void push(Pa x){
		h[++tot]=x;
		for(int i=tot;i>1;i/=2)
		if(h[i/2]<h[i])break;
		else swap(h[i],h[i/2]);
	}
	inline Pa top(){
		return h[1];
	}
	inline void down(int x){
		int y=x*2;
		if(y<tot&&h[y+1]<h[y])y++;
		if(y<=tot&&h[y]<h[x])swap(h[y],h[x]),down(y);
	}
	inline void pop(){
		h[1]=h[tot--];
		down(1);
	}
}H;
inline void clear(){
	L=0;L0=0;
	mem(h);mem(_h);mem(h0);
	mem(id);mem(be);mem(du);
	mem(f);
}
inline void addl(int x,int y,int k){
	ne[++L]=h[x];h[x]=L;to[L]=y;w[L]=k;
	ne[++L]=_h[y];_h[y]=L;to[L]=x;w[L]=k;
	if(k==0){
		ne0[++L0]=h0[x];
		h0[x]=L0;
		to0[L0]=y;
		du[y]++;
	}
}
inline void top_sort(){
	register int k,y,x,i,l=1,r=0,tot=0;
	for(i=1;i<=n;i++)if(!du[i])q[++r]=i;
	for(l=1;l<=r;l++){
		x=q[l];
		tot++;id[x]=tot;be[tot]=x;
		for(k=h0[x];k;k=ne0[k]){
			y=to0[k];du[y]--;
			if(!du[y])q[++r]=y;
		}
	}
}
inline void djstl(int S,int*ds,int*h){
	Pa X;register int i,x,y,k;
	for(i=1;i<=n;i++)
	ds[i]=1e9,ff[i]=0;
	ds[S]=0;
	if(S==1)f[0][1]=1;
	H.push(make_pair(ds[S],id[S]));
	while(H.size()){
		X=H.top();x=be[X.se];H.pop();
		while(ff[x]&&H.size()>0){
			X=H.top();x=be[X.se];H.pop();
		}if(ff[x]&&H.size()==0)break;
		for(k=h[x];k;k=ne[k])
		if(S==1&&ds[x]+w[k]==ds[to[k]]){
			A(f[0][to[k]],f[0][x]);
		}else if(ds[x]+w[k]<ds[to[k]]){
			y=to[k];
			ds[y]=ds[x]+w[k];
			if(S==1)f[0][y]=f[0][x];
			H.push(make_pair(ds[y],id[y]));
		}
		ff[x]=1;
	}
}
inline bool check(){
	int mn=d[n],mx=mn+K;
	for(register int i=1;i<=n;i++)
	if(id[i]==0){
		if(d[i]+h[i]<=mx)return 1;
	}return 0;
}
inline void rebuild(){
	L=0;L0=0;
	mem(h);mem(_h);mem(h0);
	mem(id);mem(be);mem(du);
	for(register int i=1;i<=m;i++){
		KK[i]=d[XX[i]]+KK[i]-d[YY[i]];
		if(KK[i]>K)continue;
		addl(XX[i],YY[i],KK[i]);
	}top_sort();
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(register int i,x,y,j,k,T=read();T--;){
		clear();
		n=read();m=read();K=read();P=read();
		for(i=1;i<=m;i++){
			x=read();y=read();
			k=read();addl(x,y,k);
			XX[i]=x;YY[i]=y;KK[i]=k;
		}
		top_sort();
		djstl(1,d,h);
		djstl(n,g,_h);
		if(check()){
			puts("-1");
			continue;
		}
		rebuild();
	for(j=0;j<=K;j++)
	for(i=1;i<=n;i++){
		x=be[i];if(!f[j][x])continue;
		for(k=h[x];k;k=ne[k])
		if(j==0&&w[k]==0)continue;
		else{
			if(j+w[k]<=K)
			A(f[j+w[k]][to[k]],f[j][x]);
		}
	}
		ans=0;
		for(i=0;i<=K;i++)
		A(ans,f[i][n]);
		printf("%d\n",ans);
	}
	return 0;
}

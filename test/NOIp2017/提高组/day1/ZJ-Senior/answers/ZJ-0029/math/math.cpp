#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int a,b;ll ans;
	a=read();b=read();
	if(a>b)swap(a,b);
	if(a==1)ans=0;
	else ans=1ll*a*b-a-b;
	write(ans);putchar('\n');
	return 0;
}

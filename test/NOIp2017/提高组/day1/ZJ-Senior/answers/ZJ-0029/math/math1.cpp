#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const ll N=100100,P=1e9+7;
inline ll read(){
	ll x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
void exgcd(ll a,ll b,ll&x,ll&y){
	if(!a){
		x=0;y=1;return;
	}exgcd(b%a,a,x,y);
	ll t=x;
	x=y-b/a*x;
	y=t;
}
ll a,b,i,k=0,x,y;ll ans;
bool can(ll k){
	ll X,Y;
	X=x*k;Y=y*k;
	X+=Y/a*b;
	return X>=0;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math1.out","w",stdout);
	a=read();b=read();
	if(a>b)swap(a,b);
	exgcd(a,b,x,y);
	x%=b;y%=a;
	if(y<0)y+=a,x-=b;
	for(i=1;k<a;i++){
		if(can(i))k++;
		else k=0;
	}printf("%d\n",i-k-1);
	return 0;
}

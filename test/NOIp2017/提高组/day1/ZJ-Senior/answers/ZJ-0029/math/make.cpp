#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=10000;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,x,y,k,i,j;
int main(){
	freopen("math.in","w",stdout);
	srand(time(0));
	x=rand()%P+1;y=rand()%P+1;
	while(__gcd(x,y)>1)x=rand()%P+1,y=rand()%P+1;
	printf("%d %d\n",x,y);
	return 0;
}

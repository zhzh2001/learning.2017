//Future && You
//				__wyxoi
#include <set>
#include <map>
#include <cmath>
#include <string>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
const int N = 2000010;
using namespace std;
int head[N],cnt,h,t,q[N],n,m,K,RXD,dis[N],vis[N],a[N],w[N];
struct data{int x,y,s,next;} e[N];
int read(){
	int x = 0,f = 1,c = getchar();
	while (c<'0' || c>'9') {if (c=='-') f = -1;c = getchar();}
	while (c>='0' && c<='9') x = x*10+c-'0',c = getchar();
	return x*f;
}
void Insert(int x,int y,int s){e[++cnt].x = x;e[cnt].y = y;e[cnt].s = s;e[cnt].next = head[x];head[x] = cnt;}
int cmp(int a,int b){return dis[a]<dis[b];}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (int T=read();T;T--) {
		memset(head,0,sizeof(head));cnt = 0;
		n = read();m = read();K = read();RXD = read();
		if (K==0) {
			for (int i=1;i<=m;i++) {
				int x = read(),y = read(),s = read();
				Insert(x,y,s);
			}
			for (int i=1;i<=n;i++) dis[i] = 1e9;
			dis[1] = 0;h = 0;t = 1;q[1] = 1;
			while (h<t) {
				int now = q[++h];vis[now] = 0;
				for (int i=head[now];i;i=e[i].next)
					if (dis[e[i].y]>dis[now]+e[i].s) {
						dis[e[i].y] = dis[now]+e[i].s;
						if (!vis[e[i].y]) {vis[e[i].y] = 1;q[++t] = e[i].y;}
					}
			}
			for (int i=1;i<=n;i++) a[i] = i;
			sort(a+1,a+1+n,cmp);
			memset(w,0,sizeof(w));w[1] = 1;
			for (int j=1;j<=n;j++) 
				for (int i=head[a[j]];i;i=e[i].next)
					if (dis[a[j]]+e[i].s==dis[e[i].y]) (w[e[i].y]+=w[a[j]])%=RXD;
			printf("%d\n",w[n]);
		}
	}
}

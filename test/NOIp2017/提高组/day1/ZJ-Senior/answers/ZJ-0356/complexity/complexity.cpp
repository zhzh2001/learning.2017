//Future && You
//				__wyxoi
#include <set>
#include <map>
#include <cmath>
#include <string>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
const int N = 60;
using namespace std;
int vis[N],stk[N],vh,vt,NT,ERR,nl,Max,Aim,l,top;
char s[N],ch;
int read(){
	int x = 0,f = 1,c = getchar();
	while (c<'0' || c>'9') {if (c=='-') f = -1;c = getchar();}
	while (c>='0' && c<='9') x = x*10+c-'0',c = getchar();
	return x*f;
}
int trans(){
	int res = 0,len = strlen(s+1);
	for (int i=1;i<=len;i++)
		if (s[i]>='0' && s[i]<='9') res = res*10+s[i]-'0';
	return res;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (int T=read();T;T--) {
		memset(stk,0,sizeof(stk));top = 0;
		memset(vis,0,sizeof(vis));
		l = read();scanf("%s",s+1);
		if (s[3]=='1') Aim = 0;else Aim = trans();
		ERR = 0;nl = 0;NT = 1;Max = 0;scanf("%c",&ch);
		for (int i=1;i<=l;i++) {
			scanf("%c",&ch);
			if (ch=='F') {
				scanf("%c",&ch);scanf("%c",&ch);
				int t = ch-'a'+1;
				if (vis[t]) ERR = 1;
				scanf("%c",&ch);scanf("%c",&ch);
				if (ch=='n') {vh = 101;scanf("%c",&ch);}
				else {
					vh = 0;
					while (ch>='0' && ch<='9') {vh = vh*10+ch-'0';scanf("%c",&ch);}
				}
				scanf("%c",&ch);
				if (ch=='n') {vt = 101;scanf("%c",&ch);}
				else {
					vt = 0;
					while (ch>='0' && ch<='9') {vt = vt*10+ch-'0';scanf("%c",&ch);}
				}
				if (ERR) continue;
				stk[++top] = t;
				if (vh<=vt) vis[t] = 1;
				if (vh<101 && vt==101) vis[t] = 2;
				if (vh>vt) vis[t] = 3,NT = 0;
				if (vis[t]==2) nl+=NT;
				Max = max(Max,nl);
			}
			else {
				scanf("%c",&ch);
				if (top==0) ERR = 1;
				if (ERR) continue;
				int t = stk[top];
				if (vis[t]==3) NT = 1;
				if (vis[t]==2) nl-=NT;
				vis[t] = 0;stk[top] = 0;top--;
			}
		}
		if (top!=0) ERR = 1;
		if (ERR) printf("ERR\n");
		else {
			if (Max==Aim) printf("Yes\n");
			else printf("No\n");
		}
	}
}

#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>
#include <algorithm>
int n,m,k,p;
struct asd{
	int to,next,val;
}e[800000];
int head[500000],cnt=0;
int dis[500000];
bool b[500000];
int d[500000];
inline void spfa(){
	memset(dis,127/2,sizeof(dis));
	dis[1]=0;b[1]=true;d[1]=1;int he=0,en=1;
	while (he!=en){
		he=he%400000+1;
		int x=d[he];
		for (int i=head[x];i;i=e[i].next) if (dis[x]+e[i].val<dis[e[i].to]){
			dis[e[i].to]=dis[x]+e[i].val;
			if (!b[e[i].to]){
				b[e[i].to]=true;
				en=en%400000+1;
				d[en]=e[i].to;
			}
		}
		b[x]=false;
	}
}
bool B[110000][60];
//int D1[110000*60],D2[110000*60];
int tot[110000][60];
int cntt[110000][60];
//d1 id
//d2 plus
//d3 num
int ans=0;
std::priority_queue<std::pair<int,std::pair<int,int> >,std::vector<std::pair<int,std::pair<int,int> > >,std::greater<std::pair<int,std::pair<int,int> > > > q;
inline void doo(){
	//int he=0,en=1;
	//D1[1]=1;D2[1]=0;
	q.push(std::make_pair(dis[1],std::make_pair(1,0)));
	tot[1][0]=1;
	memset(B,false,sizeof(B));
	memset(cntt,0,sizeof(cntt));
	B[1][0]=true;
	while (!q.empty()){
		//he++;if (he>6000000) he=1;
		std::pair<int,std::pair<int,int> > tt=q.top();q.pop();
		int x=tt.second.first,plus=tt.second.second;
		cntt[x][plus]++;if (cntt[x][plus]>100){
			ans=-1;
			return;
		}
		if (x==n) ans=(ans+tot[x][plus])%p;
		for (int i=head[x];i;i=e[i].next) {
			int ttt=dis[x]+plus+e[i].val-dis[e[i].to];
			if (ttt<=k){
				tot[e[i].to][ttt]=(tot[e[i].to][ttt]+tot[x][plus])%p;
				if (!B[e[i].to][ttt]){
					B[e[i].to][ttt]=true;
					q.push(std::make_pair(dis[x]+plus+e[i].val,std::make_pair(e[i].to,ttt)));
					//en++;if (en>6000000) en=1;
					//D1[en]=e[i].to;D2[en]=ttt;
				}
			}
		}
		tot[x][plus]=0;B[x][plus]=false;
	}
}
inline void read(int &x){
	x=0;char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9'){x=x*10+ch-'0';ch=getchar();}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;read(T);//scanf("%d",&T);
	while (T--){
		read(n);read(m);read(k);read(p);//scanf("%d%d%d%d",&n,&m,&k,&p);
		int x,y,z;
		memset(head,0,sizeof(head));cnt=0;
		for (int i=1;i<=m;i++){
			read(x);read(y);read(z);//scanf("%d%d%d",&x,&y,&z);
			e[++cnt].to=y;e[cnt].val=z;e[cnt].next=head[x];head[x]=cnt;
		}
		ans=0;
		spfa();
		doo();
		printf("%d\n",ans);
	}
	return 0;
}

#include <cstdio>
#include <cstring>
int L;
struct asd{
	char i;
	bool die0;
	int xn,mxin;
}stack[500];
int top=0;
char s[500],a1[500],a2[500],a3[500],kase[500];
bool pd[500];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;scanf("%d",&T);
	while (T--){
		scanf("%d",&L);
		scanf("%s",s+1);
		bool err=false;top=0;stack[top].i='0';stack[top].xn=0;stack[top].mxin=0;stack[top].die0=false;
		memset(pd,false,sizeof(pd));
		for (int l=1;l<=L;l++){
			scanf("%s",kase);
			if (kase[0]=='F'){
				scanf("%s",a1);scanf("%s",a2);scanf("%s",a3);
				char ti=a1[0];
				if (pd[ti-'a']) err=true;pd[ti-'a']=true;
				top++;stack[top].i=ti;stack[top].xn=0;stack[top].mxin=0;stack[top].die0=false;
				if (a2[0]=='n'){
					if (a3[0]=='n') stack[top].xn=0;
					else stack[top].die0=true;
				}else{
					if (a3[0]=='n') stack[top].xn=1;
					else {
						//calc
						int t1=0,len=strlen(a2);
						for (int i=0;i<len;i++) t1=t1*10+a2[i]-'0';
						int t2=0;
						len=strlen(a3);
						for (int i=0;i<len;i++) t2=t2*10+a3[i]-'0';
						if (t1>t2) stack[top].die0=true;
						stack[top].xn=0;
					}
				}
			}else{
				if (top==0) {
					err=true;
				}else{
					//doing
					if (!stack[top].die0){
						int t=stack[top].mxin+stack[top].xn;
						if (t>stack[top-1].mxin) stack[top-1].mxin=t;
					}
					pd[stack[top].i-'a']=false;
					top--;
				}
			}
		}
		if (top!=0) err=true;
		if (err){
			printf("ERR\n");
		}else{
			//yes or no
			int t=stack[0].mxin;
			int tt=0;
			if (s[3]=='n'){
				int len=strlen(s+1);
				for (int i=5;i<=len;i++){
					if (s[i]==')') break;
					tt=tt*10+s[i]-'0';
				}
			}else{
				tt=0;
			}
			if (t==tt) printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}

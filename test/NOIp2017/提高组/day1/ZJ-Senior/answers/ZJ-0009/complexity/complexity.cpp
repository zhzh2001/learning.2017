#include<bits/stdc++.h>
#define maxn 110
using namespace std;
int t,s,k,f,ce,edd,x,y;
struct st{
	char l,r,c;
	int val,xia,x,y;
	int fl;
	inline st clean(){
		val = xia = x = y = fl = 0;
		l = r = '0';
	}
}sta[maxn];
map <char,int> mapp;
string q;
char c,qq[maxn];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		sta[0].clean();
		scanf("%d",&s);
		getchar();
		mapp.clear();
		f = 0;
		k = 0;
		ce = 0;
		cin >> q;
		for(int i=0;i<q.length();i++)
		{
			if(q[i] == '(')
			{
				f = 1;
				if(q[i + 1] == 'n') ce = 1;
				continue;
			}
			if(q[i] == ')') f = 0;
			if(f)
			{
				if(q[i] >= '0' && q[i] <= '9')
				{
					k = (k << 1) + (k << 3) + q[i] - '0';
				}
			}
		}
		int tot = 0;
		for(int i=1;i<=s;i++)
		{
			cin >> c;
			if(c == 'F')
			{
				cin >> c;
				if(mapp.count(c) && mapp[c] == 1) f = 1;
				mapp[c] = 1;
				++tot;
				sta[tot].clean();
				sta[tot].c = c;
				scanf("%s",qq);
				if(qq[0] == 'n') sta[tot].l = 'n';
				else
				{
					int len = strlen(qq);
					int lin = 0;
					for(int i=0;i<len;i++)
					{
						lin = (lin << 1) + (lin << 3) + qq[i] - '0';
					}
					sta[tot].x = lin;
				}
				scanf("%s",qq);
				if(qq[0] == 'n') sta[tot].r = 'n';
				else
				{
					int len = strlen(qq);
					int lin = 0;
					for(int i=0;i<len;i++)
					{
						lin = (lin << 1) + (lin << 3) + qq[i] - '0';
					}
					sta[tot].y = lin;
				}
				if(f) continue;
				if(sta[tot].l == 'n')
				{
					if(sta[tot].r != 'n') sta[tot].fl = 1;
					else sta[tot].val = 0;
				}
				if(sta[tot].r == 'n')
				{
					if(sta[tot].l == 'n') sta[tot].val = 0;
					else sta[tot].val = 1;
				}
				if(sta[tot].l != 'n' && sta[tot].r != 'n')
				{
					if(sta[tot].y - sta[tot].x < 0) sta[tot].fl = 1;
					else sta[tot].val = 0;
				}
			}
			else
			{
				if(f) continue;
				if(tot == 0) f = 1;
				else
				{
					--tot;
					mapp[sta[tot + 1].c] = 0;
					if(sta[tot + 1].fl) continue;
					else 
					{
						sta[tot + 1].val += sta[tot + 1].xia;
						sta[tot].xia = max(sta[tot].xia,sta[tot + 1].val);
					}
				}
			}
		}
		if(f || tot) printf("ERR\n");
		else
		{
			if(ce == 1)
			{
				if(sta[tot].xia == k) printf("Yes\n");
				else printf("No\n");
			}
			else
			{
				if(!sta[tot].xia) printf("Yes\n");
				else printf("No\n");
			}
		}
	}
	return 0;
}

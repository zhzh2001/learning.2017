#include<bits/stdc++.h>
#define ll long long
#define R register
using namespace std;
ll a,b,n,ans;
bool s[10000000];
inline ll read()
{
	ll lin = 0;
	char x = getchar();
	while(x < '0' || x > '9') x = getchar();
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin;
}

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a = read();
	b = read();
	n = a * b;
	s[a] = s[b] = 1;
	for(R ll i=1;i<=n;i++)
	{
		if(s[i])
		{
			s[i + a] = 1;
			s[i + b] = 1;
			continue;
		}
		else
		{
			ans = i;
		}
	}
	printf("%lld",ans);
	return 0;
}

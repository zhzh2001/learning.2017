#include <iostream>
#include <cstdio>
using namespace std;

int a, b, n;

int main()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%d%d", &a, &b);
	if (a > b) swap(a, b);
	long long ans = (a - 2) + (a - 1) * (b - 2);
	printf("%lld", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

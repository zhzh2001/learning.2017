#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
#include <stack>
#include <map>
#define maxn 100100
#define maxm 200100
using namespace std;
int t, n, m, k, p;
struct Edge {
	int to, nxt, w;
} e[maxm];
int tot, head[maxn], d[maxn], d2[maxn], dp[maxn], deg[maxn]; bool vis[maxn];
map<pair<int, int>, int> ms;
inline void AddEdge(int u, int v, int w)
{
	e[tot] = (Edge) {v, head[u], w}, head[u] = tot++;
}
inline void reset()
{
	memset(head, 0, sizeof(head)); tot = 1; ms.clear();
	memset(dp, 0, sizeof(dp)); memset(deg, 0, sizeof(deg));
}

inline void spfa()
{
	int ret = 0;
	queue<int> q; memset(d, 0x7f, sizeof(d)); memset(vis, 0, sizeof(vis));
	d[1] = 0; vis[1] = 1; q.push(1);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		vis[u] = 0;
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (d[v] > d[u] + e[i].w) {
				d[v] = d[u] + e[i].w;
				if (!vis[v]) {
					vis[v] = 1;
					q.push(v);
				}
			}
		}
	}
}

inline void spfa_2()
{
	int ret = 0;
	queue<int> q; memset(d2, 0, sizeof(d2)); memset(vis, 0, sizeof(vis));
	d2[1] = 0; vis[1] = 1; q.push(1);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		vis[u] = 0;
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (d2[v] < d2[u] + e[i].w) {
				d2[v] = d2[u] + e[i].w;
				if (!vis[v]) {
					vis[v] = 1;
					q.push(v);
				}
			}
		}
	}
}

inline void topo()
{
	int ret = 0; stack<int> s;
	queue<int> q; memset(vis, 0, sizeof(vis));
	vis[1] = 1; q.push(1); s.push(1);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (!vis[v]) if (--deg[v] == 0) {
				s.push(v);
				q.push(v);
			}
		}
	}
}

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &t);
	while (t--) {
		scanf("%d%d%d%d", &n, &m, &k, &p); reset();
		for (int i = 1, x, y, w; i <= m; ++i) {
			scanf("%d%d%d", &x, &y, &w);
			AddEdge(x, y, w); ++deg[y]; ms.insert(mmak_pair(in(x, y), max(x, y)), w);
		}
		spfa(); spfa_2();
		if (d2[n] == 0 || d2[n] <= d[n] + k) {
			printf("-1\n");
			continue;
		}
		topo();
		printf("%d\n", dp[n]);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

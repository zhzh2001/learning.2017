#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
#define maxn 100100
#define maxm 200100
using namespace std;
int t, n, m, k, p;
struct Edge {
	int to, nxt, w;
} e[maxm];
int tot, head[maxn], d[maxn], d2[maxn], dp[maxn], G[5050][5050], W[5050][5050]; bool vis[maxn];
inline void AddEdge(int u, int v, int w)
{
	e[tot] = (Edge) {v, head[u], w}, head[u] = tot++;
}
inline void reset()
{
	memset(head, 0, sizeof(head)); tot = 1;
	memset(dp, 0, sizeof(dp)); memset(G, 0, sizeof(G));
	memset(W, 0, sizeof(W));
}

inline void spfa()
{
	int ret = 0;
	queue<int> q; memset(d, 0x7f, sizeof(d)); memset(vis, 0, sizeof(vis));
	d[1] = 0; vis[1] = 1; q.push(1);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		vis[u] = 0;
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (d[v] > d[u] + e[i].w) {
				d[v] = d[u] + e[i].w;
				if (!vis[v]) {
					vis[v] = 1;
					q.push(v);
				}
			}
		}
	}
}

inline void spfa_2()
{
	int ret = 0;
	queue<int> q; memset(d2, 0, sizeof(d2)); memset(vis, 0, sizeof(vis));
	d2[1] = 0; vis[1] = 1; q.push(1);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		vis[u] = 0;
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (d2[v] < d2[u] + e[i].w) {
				d2[v] = d2[u] + e[i].w;
				if (!vis[v]) {
					vis[v] = 1;
					q.push(v);
				}
			}
		}
	}
}

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &t);
	while (t--) {
		scanf("%d%d%d%d", &n, &m, &k, &p); reset();
		for (int i = 1, x, y, w; i <= m; ++i) {
			scanf("%d%d%d", &x, &y, &w);
			AddEdge(x, y, w); G[x][y] = 1; W[x][y] = w;
		}
		spfa(); spfa_2();
		if (d2[n] == 0 || d2[n] <= d[n] + k) {
			printf("-1\n");
			continue;
		}
		for (int i = 1; i <= n; ++i) {
			int ret = 0;
			for (int j = 1; j <= n; ++j)
				if (G[j][i] && d[j] + W[j][i] <= d[i] + k) ++ret;
			dp[i] = ret;
		}
		printf("%d\n", dp[n]);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int f[1005][2005],val[1005][2005],a[1005],Q[1005],n,m,k,p,s,Min,T,ans,sss;
void find(int t){
	if (t==n){
		Q[t]=min(Q[t],s);
		Min=min(Min,s);return;
	}
	if (s>Min) return;
	if (s>Q[t]) return;else Q[t]=s;
	for (int i=1; i<=a[t]; i++){
		s=s+val[t][i];
		find(f[t][i]);
		s=s-val[t][i];
	}
}
void find2(int t){
	if (t==n){
		if (s<=Q[n]+k) ans=(ans+1)%p;
		return;
	}
	if (s>Min+k) return;
	if (s>Q[t]+k) return;
	for (int i=1; i<=a[t]; i++){
		s=s+val[t][i];
		find2(f[t][i]);
		s=s-val[t][i];
	}
}
int main(){
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1; i<=n; i++) Q[i]=1000000000,a[i]=0,sss=0;
		s=0,Min=1000000000,Q[1]=0,ans=0;
		for (int i=1; i<=m; i++){
			int u,v,z;
			scanf("%d%d%d",&u,&v,&z);
			if (z==0) sss++;
			a[u]++,f[u][a[u]]=v,val[u][a[u]]=z;
		}
		if (sss>=2){
			printf("-1\n");
			continue;
		}
		find(1);
		s=0;find2(1);
		printf("%d\n",ans);
	}
	fclose(stdin);fclose(stdout);
}

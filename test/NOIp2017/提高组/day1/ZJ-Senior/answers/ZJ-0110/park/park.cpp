//Thank you.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,t,p,K,ans;
int road[1010][1010];
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf ("%d",&t);
	while (t--) {
		scanf ("%d%d%d%d",&n,&m,&K,&p);
		ans=0;
		Rep(i,1,n) Rep(j,1,n) road[i][j]=10000000;
		Rep(i,1,m) {
			int x,y,z;
			scanf ("%d%d%d",&x,&y,&z);
			road[x][y]=z;
		}
		Rep(k,1,n) Rep(i,1,n) Rep(j,1,n) {
			road[i][j]=min(road[i][k]+road[k][j],road[i][j]);
		}
		ans=1;
		Rep(k,1,n) {
			if (road[1][k]+road[k][n]-road[1][n]<K) ans++;
		}
		cout<<road[1][n]<<endl;
		cout<<ans%p<<endl;
	}
	return 0;
}

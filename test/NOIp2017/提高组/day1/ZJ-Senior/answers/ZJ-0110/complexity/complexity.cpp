//Thank you.
#include<cstdio>
#include<string>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 100
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,t,w,o,x,y,L,Te,For,tem,num,ans;
char Temp,Case,tim[1000],prog[1000];
bool faill=false;
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf ("%d",&t);
	while (t--) {
		For=Te=0; ans=1; num=0;
		scanf ("%d",&L);
		scanf ("%c",Temp);
		scanf ("%s",tim);
		int l=strlen(tim)-1,w=o=0;
		if (tim[2]=='n') {
			Rep(i,4,l-1) w=w*10+tim[i]-'0';
		}
		else {
			Rep(i,2,l-1) o=o*10+tim[i]-'0';
		}
		Case=' ';
		Rep(i,1,L) { int K=0;
			x=y=0;
			memset(prog,' ',sizeof(prog));
			Rep(k,0,3) {
				char te[1000];
				scanf ("%s",te);
				int ll=strlen(te)-1;
				Rep(j,0,ll) {
					prog[K]=te[j];
					K++;
				}
				if (prog[--K]=='E') break;
				K++; prog[K]=' ';
				K++;
			}
			if (prog[0]=='F') {
				For++;
				int j=4;
				if (prog[j]!='n') 
					while (prog[j]!=' ') {
						x=x*10+prog[j]-'0';
						j++;
					}
				else {
					x=N;
					j++;
				}
				j++;
				if (prog[j]!='n') 
					while (prog[j]!=' ') {
						y=y*10+prog[j]-'0';
						j++;
					}
				else {
					y=N;
					j++;
				}
				if (x==y) {
					tem=1;
				}
				if (x>y) {
					faill=true;
				}
				if (y>x) {
					if (y==N) {
						num++;
						if (faill==true) num--;
					}
					if (y!=N) {
						tem=y-x+1;
						if (faill==true) tem=1;
					}
				}
			}
			if (prog[0]=='E') {
				For--;
				if (For==0) {
					o-=ans;
					ans=1;
					Te=max(Te,num);
					num=0; 
					faill=false;
				}
				else {
					ans=ans*tem;
				}
			}
		}
		if (For!=0) {
			cout<<"ERR"<<endl;
			For=0;
			continue;
		}
		if (w!=0) {
			if (w==Te) cout<<"Yes"<<endl; else cout<<"No"<<endl;
		}
		else {
			if (o==0) cout<<"Yes"<<endl; else cout<<"No"<<endl;
		}
	}
	return 0;
}

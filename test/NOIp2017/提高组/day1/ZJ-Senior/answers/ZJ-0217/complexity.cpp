#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
using namespace std;
#define gr getchar
int ti,l,w,i,v,lq,a,b,z,q[101],x[27],y[27],f[101],d[101];
char ci,u;
bool bb;
int sri(){
	char c=getchar();int x=0;
	while(c<'0'||c>'9')c=gr();
	while(c>='0'&&c<='9')x=(x<<3)+(x<<1)+c-'0',c=gr();
	return x;
}
void ss(){
	while((u!='E')&&(u!='F'))u=gr();ci=u;	
}
void cx(){
	u=gr();
	while(u<'a'||u>'z')u=gr();
	v=u-'a';u=gr();u=gr();
	if(u=='n')a=-1,u=gr();else{
		int x=0;
		while(u>='0'&&u<='9')x=(x<<3)+(x<<1)+u-'0',u=gr();
		a=x;
	}u=gr();
	if(u=='n')b=-1,u=gr();else{
		int x=0;
		while(u>='0'&&u<='9')x=(x<<3)+(x<<1)+u-'0',u=gr();
		b=x;
	}
	if(bb){
		if(x[v]==-2){
			q[++lq]=v;x[v]=a;y[v]=b;
			if(a==-1){
				if(b==-1)f[lq]=0;else
					f[lq]=-1;
			}else{
				if(b==-1)f[lq]=1;else{
					if(a<=b)f[lq]=0;else f[lq]=-1;
				}
			}d[lq]=f[lq];
		}else b=false;	
	}
}
int zd(int a,int b){
	return a>b?a:b;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	ti=sri();
	while(ti--){
		for(i=0;i<=26;i++)x[i]=y[i]=-2;
		l=sri();
		ci=gr();
		while(ci!='O')ci=gr();
		ci=gr();ci=gr();
		if(ci=='1')w=0;else{
			ci=gr();w=sri();
		}
		u=gr();lq=0;bb=true;z=0;
		for(i=1;i<=l;i++){
			ss();
			if(ci=='F'){
				cx();
			}else{
				if(bb){				
					if(lq>0){
						if(lq==1){
							if(d[lq]>z)z=d[lq];
						}else{
							if(d[lq]>=1){
								if(f[lq-1]>=0)d[lq-1]=zd(d[lq-1],f[lq-1]+d[lq]);
							}
						}
						x[q[lq]]=y[q[lq]]=-2,lq--;
					}else bb=false;
				}
				u=gr();
			}
		}
		if(lq>0)bb=false;
		if(!bb)putchar('E'),putchar('R'),putchar('R'),putchar('\n');else{
			if(z==w)putchar('Y'),putchar('e'),putchar('s'),putchar('\n');else
			putchar('N'),putchar('o'),putchar('\n');
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
using namespace std;
#define ll long long
#define nn 100007
int tt,i,n,m,k,o,p,ni,u,v,l,li,lj,lq,g[30],h[nn],q[nn],zi[nn],xi[400007],ti[400007],ci[200007],f[nn],d[nn];
int sri(){
	char c=getchar();int x=0;
	while(c<'0')c=getchar();
	while(c>='0')x=(x<<3)+(x<<1)+c-'0',c=getchar();
	return x;
}
void sc(int x){int l=0;
	if(x<0)putchar('-'),x=-x;
	if(x==0)g[l=1]=0;
	while(x)g[++l]=x%10,x/=10;
	for(;l>0;l--)putchar(g[l]+'0');putchar('\n');
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	tt=sri();
	while(tt--){
		n=sri();m=sri();k=sri();p=sri();
		for(i=1;i<=n;i++)zi[i]=-1,f[i]=0,h[i]=0,d[i]=-1;ni=1;
		for(i=1;i<=m;i++){
			u=sri();v=sri();ci[i]=sri();
			xi[++ni]=zi[u],zi[u]=ni,ti[ni]=v;
			xi[++ni]=zi[v],zi[v]=ni,ti[ni]=u;
		}q[lq=1]=1;f[1]=1%p;d[1]=0;h[1]=1;
		while(lq){
			u=q[1];q[1]=q[lq--];h[q[1]]=1;l=1;h[u]=0;
			while((l<<1)<=lq){
				li=l<<1;lj=li|1;
				if(lj<=lq){if(d[q[li]]>d[q[lj]])li=lj;}
				if(d[q[li]]<d[q[l]]){	
					o=q[l];q[l]=q[li];q[li]=o;
					h[q[l]]=l;h[q[li]]=li;
					l=li;
				}else break;
			}
			for(i=zi[u];i!=-1;i=xi[i]){int v=ti[i];
				if(d[v]==-1||d[u]+ci[i>>1]<d[v]){		
					if(h[v]==0){			
						d[v]=d[u]+ci[i>>1];
						q[++lq]=v;l=lq;h[v]=lq;
						while(l){li=l>>1;
							if(d[q[li]]>d[q[l]]){	
								o=q[l];q[l]=q[li];q[li]=o;
								h[q[l]]=l;h[q[li]]=li;
								l=li;
							}else break;
						}

						f[v]=f[u];
					}else{
						d[v]=d[u]+ci[i>>1];
						l=h[v];
						while(l){li=l>>1;
							if(d[q[li]]>d[q[l]]){	
								o=q[l];q[l]=q[li];q[li]=o;
								h[q[l]]=l;h[q[li]]=li;
								l=li;
							}else break;
						}
						f[v]=f[u];			
					}
				}else{
					if((d[u]+ci[i>>1])==d[v])f[v]=(int)(((ll)f[v]+(ll)f[u])%p);
				}
			}
		}
		sc(f[n]);
	}
	return 0;
}

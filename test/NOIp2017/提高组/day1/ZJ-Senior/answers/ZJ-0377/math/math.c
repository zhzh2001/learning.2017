#include <stdio.h>

unsigned long long a, b;
unsigned long long n;

unsigned long long gcd(unsigned long long a, unsigned long long b)
{
	if (a == 1 || b == 1)
	{
		return 1;
	}
	if (a == b)
	{
		return a;
	}
	else if (a > b)
	{
		return gcd(b, a - b);
	}
	else if (a < b)
	{
		return gcd(a, b - a);
	}
}

int main()
{
	unsigned long long g;
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld %lld", &a, &b);
	
	g = gcd(a, b);
	if (a < b)
	{
		n = b * (a - 1) - a;
	}
	else
	{
		n = a * (b - 1) - b;
	}
	printf("%lld", n);
	
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXN 100005
#define INF (1 << 30)

int t;
int n, m, k, P;
int total = 0;

struct edge
{
	int to;
	int len;
	struct edge *next;
};

int vis[MAXN];
int len[MAXN];

struct point
{
	struct edge *head;
}p[MAXN];

int dij(int start, int end)
{
	int dist[MAXN];
	int v[MAXN];
	int i;
	int cur, next;
	int max = INF;
	struct edge *c;
	for (i = 1; i <= n; i++)
	{
		dist[i] = INF;
		v[i] = 0;
	}
	dist[start] = 0;
	v[i] = 1;
	
	cur = start;
	while (1)
	{
		c = p[cur].head;
		cur = next;
		next = -1;
		max = INF;
		while (c != NULL)
		{
			if (dist[c->to] > dist[cur] + c->len)
			{
				dist[c->to] = dist[cur] + c->len;
			}
			if (dist[c->to] < max)
			{
				max = dist[c->to];
				next = c->to;
			}
			c = c->next;
		}
		if (next == -1)
		{
			break;
		}
	}
	return dist[end];
}

void dfs(int point, int dist, int maxlen, int *ret)
{
	struct edge *cur = p[point].head;
	
	if (point == n)
	{
		total++;
		if (total >= P)
		{
			total -= P;
		}
	}
	
	while (cur != NULL)
	{
		if (!vis[cur->to])
		{
			len[cur->to] = dist + cur->len;
			if (len[cur->to] > maxlen)
			{
				return;
			}
			vis[cur->to]++;
			dfs(cur->to, len[cur->to], maxlen, ret);
			if ((*ret) == 1)
			{
				return;
			}
			vis[cur->to]--;
		}
		else
		{
			if (len[cur->to] == dist + cur->len)
			{
				*ret = 1;
				total = -1;
			}
		}
	}
}

int main()
{
	int i, j;
	int u, v, w;
	int d;
	int ret = 0;
	struct edge **cur = NULL;
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	scanf("%d", &t);
	for (i = 1; i <= t; i++)
	{
		scanf("%d %d %d %d", &n, &m, &k, &P);
		for (j = 1; j <= m; j++)
		{
			scanf("%d %d %d", &u, &v, &w);
			
			cur = &p[u].head;
			while (*cur != NULL)
			{
				cur = (*cur)->next;
			}
			(*cur) = (struct edge *)malloc(sizeof(struct edge));
			(*cur)->to = v;
			(*cur)->len = w;
			(*cur)->next = NULL;
		}
	
		d = dij(1, n);
	
		vis[1] = 1;
		len[1] = 0;
		dfs(1, 0, d + k, &ret);
		printf("%d", total);
		total = 0;
	}
	return 0;
}

#include <stdio.h>
#include <string.h>
#define MAX 105
#define TYPE_N 1
#define TYPE_NUMBER 2

struct T
{
	char v_name;
	int start;
	int end;
};
struct T stack[MAX];
int top;

int t;
int l;
char str[MAX];
char program[MAX][MAX];
char variable_name[26];
int exponent;

int check(struct T *cur)
{
	if (cur->start == TYPE_NUMBER && cur->end == TYPE_N) /* 1 --- n */
	{
		return 1;
	}
	else if (cur->start == TYPE_N && cur->end == TYPE_NUMBER) /* n --- 1 */
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int main()
{
	int i, j;
	int ch;
	int max = 0;
	char tmp = 0;
	int tmp2 = 0;
	int error = 0;
	int n1, n2;
	int ret = 0;
	struct T *cur = NULL;
	
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	scanf("%d", &t);
	for (i = 1; i <= t; i++)
	{
		top = max = tmp = tmp2 = error = n1 = n2 = ret = 0;
		scanf("%d", &l);
		scanf("%s", str);
		
		if (str[2] == '1') /* O(1) */
		{
			exponent = 0;
		}
		else /* O(n^w) */
		{
			sscanf(str, "O(n^%d)", &exponent);
		}
		
		memset(program, 0, sizeof(char) * MAX * MAX);
		memset(variable_name, 0, sizeof(char) * 26);
		memset(stack, 0, sizeof(struct T) * MAX);
		
		getchar();
		for (j = 1; j <= l; j++)
		{
			ch = 0;
			while ((tmp = getchar()) != '\n')
			{
				program[j][ch++] = tmp;
			}
		}
		
		for (j = 1; j <= l; j++)
		{
			if (program[j][0] == 'F')
			{
				cur = &stack[top++];
				cur->v_name = program[j][2];
				if (variable_name[cur->v_name - 'a'] == 1 || cur->v_name == 'n')
				{
					error = 1;
					break;
				}
				else
				{
					variable_name[cur->v_name - 'a'] = 1;
				}
				if (program[j][4] == 'n') /* i == n */
				{
					cur->start = TYPE_N;
					if (program[j][6] == 'n')
					{
						cur->end = TYPE_N;
					}
					else
					{
						cur->end = TYPE_NUMBER;
					}
				}
				else /* i == number */
				{
					cur->start = TYPE_NUMBER;
					tmp = 4;
					while (program[j][tmp] >= '0' && program[j][tmp] <= '9')
					{
						tmp++;
					}
					tmp++;
					if (program[j][tmp] == 'n') /* j == n */
					{
						cur->end = TYPE_N;
					}
					else /* j == number */
					{
						cur->end = TYPE_NUMBER;
						/* if i > j */
						sscanf(&program[j][4], "%d %d", &n1, &n2);
						if (n1 > n2)
						{
							cur->start = TYPE_N;
						}
					}
				}
				
				/* check if this statement is valid */ 
				ret = check(cur);
				if (ret == 1)
				{
					tmp2++;
					if (tmp2 > max)
					{
						max = tmp2;
					}
				}
				else if (ret == -1)
				{
					tmp = 0;
					while (j <= l)
					{
						j++;
						if (program[j][0] == 'F')
						{
							tmp++;
							if (variable_name[program[j][2] - 'a'] == 1 || program[j][2] == 'n')
							{
								error = 1;
								break;
							}
						}
						else if (program[j][0] == 'E')
						{
							if (tmp == 0)
							{
								top--;
								variable_name[cur->v_name - 'a'] = 0;
								break;
							}
							else
							{
								tmp--;
							}
						}
					}
					if (j > l)
					{
						error = 1;
						break;
					}
				}
				if (error == 1)
				{
					break;
				}
			}
			else if (program[j][0] == 'E')
			{
				if (top == 0)
				{
					error = 1;
					break;
				}
				cur = &stack[--top];
				if (check(cur))
				{
					tmp2--;
				}
				variable_name[cur->v_name - 'a'] = 0;
			}
		}
		if (top != 0 || error == 1)
		{
			printf("ERR\n");
		}
		else
		{
			if (max == exponent)
			{
				printf("Yes\n");
			}
			else
			{
				printf("No\n");
			}
		}
	}
	return 0;
}

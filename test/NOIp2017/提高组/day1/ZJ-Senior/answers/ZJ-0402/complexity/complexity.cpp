#include<cstdio>
#include<iostream>
#include<cmath>
#include<climits>
#include<algorithm>
#include<cstring>
#include<string>
#include<cstdlib>
#include<queue>
#include<vector>
#include<map>
#define LL long long

using namespace std;

int T,n,m;
bool v[200];
int a[200],b[200];

void read1(int &x)
{
	char ch;
	scanf("%c",&ch);
	while(ch!='(') scanf("%c",&ch);
	scanf("%c",&ch);
	if (ch=='n')
	{
		scanf("%c",&ch);
		scanf("%c",&ch);
		x=0;
		while (ch>='0' && ch<='9') x=x*10+ch-'0',scanf("%c",&ch);
	}
	else x=0;
	
	return ;
}

int read2()
{
	char ch;
	scanf("%c",&ch);
	while(ch!='F'&&ch!='E') scanf("%c",&ch);
	if (ch=='F') return 2;else return 1;
}

int solve(int &u)
{
	int z=0,x,y,l,r;u=0;
	memset(v,0,sizeof(v));
	memset(a,0,sizeof(a));
	memset(b,0,sizeof(b));
	int flag=0;
	for (int i=1;i<=n;i++)
	{
		y=read2();
		if (y==1)
		{
			u=max(u,max(b[z],0));
			v[a[z]]=0;
			z--;
			if (z<0) flag=2;
		}
		else if (y==2)
		{
			char ch;
			scanf("%c",&ch);scanf("%c",&ch);
			if (!v[ch-'a'])
			{
				v[ch-'a']=1;
				z++;a[z]=ch-'a';
				scanf("%c",&ch);scanf("%c",&ch);
				if (ch>='0' && ch<='9') x=1;else x=0;
				l=0;
				while (ch>='0' && ch<='9') l=l*10+ch-'0',scanf("%c",&ch);
				if (x==1)
				{
					scanf("%c",&ch);
					r=0;
					while (ch>='0' && ch<='9') r=r*10+ch-'0',scanf("%c",&ch);
					if (ch=='n'&&b[z-1]>=0) b[z]=b[z-1]+1;
					else if (r>=l) b[z]=b[z-1];
					else b[z]=-1;
				}
				else
				{
					scanf("%c",&ch);scanf("%c",&ch);
					if (ch=='n') b[z]=b[z-1];else b[z]=-1;
				}
			}
			else flag=2;
		}
	}
	if (flag==2) return 2;
	if (z==0) return 1;else return 2;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	int x,y,u;
	while (T--)
	{
		scanf("%d",&n);
		read1(x);y=solve(u);
		if (y==1&&u==x) puts("Yes");
		else if (y==1) puts("No");
		else puts("ERR");
	}
	return 0;
}

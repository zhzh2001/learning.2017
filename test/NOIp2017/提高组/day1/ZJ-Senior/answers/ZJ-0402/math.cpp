#include<cstdio>
#include<iostream>
#include<cmath>
#include<climits>
#include<algorithm>
#include<cstring>
#include<string>
#include<cstdlib>
#include<queue>
#include<vector>
#include<map>
#define LL long long

using namespace std;

LL n,m;

void ex_gcd(LL a,LL b,LL &d,LL &x,LL &y)
{
	if (!b) d=a,x=1,y=0;
	else ex_gcd(b,a%b,d,y,x),y-=x*(a/b);	
}

LL Gcd(LL x,LL y)
{
	LL r=x%y;
	while (r)x=y,y=r,r=x%y;
	return y;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	if (n>m) swap(n,m);
	LL x=n*m/Gcd(n,m);
	x=x-m-n;
	if (x==-1) puts("0");
	else printf("%lld\n",x);
	return 0;
}

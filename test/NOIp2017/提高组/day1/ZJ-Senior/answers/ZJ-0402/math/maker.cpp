#include<cstdio>
#include<iostream>
#include<cmath>
#include<climits>
#include<algorithm>
#include<cstring>
#include<string>
#include<cstdlib>
#include<queue>
#include<vector>
#include<map>
#include<ctime>
#define LL long long

using namespace std;

int a,b;

int gcd(int x,int y)
{
	int r=x%y;
	while (r!=0) x=y,y=r,r=x%y;
	return y;
}

int main()
{
	freopen("math.in","w",stdout);
	srand((unsigned)time(NULL));
	int N=1000000;
	a=rand()%N+1;b=rand()%N+1;
	while (gcd(a,b)>1||a==b) b=rand()%N+1;
	printf("%d %d\n",a,b);
	return 0;
}

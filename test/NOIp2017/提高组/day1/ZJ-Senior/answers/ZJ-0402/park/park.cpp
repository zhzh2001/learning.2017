#include<cstdio>
#include<iostream>
#include<cmath>
#include<climits>
#include<algorithm>
#include<cstring>
#include<string>
#include<cstdlib>
#include<queue>
#include<vector>
#include<map>
#define LL long long

using namespace std;

int T,n,m,k,p,d[100010],f[100010],g[100010],dp[100010][60];
struct data
{
	int x,y;
	data(int a=0,int b=0):x(a),y(b){}
};
vector<data> a[100010];
struct Q
{
	int x,y,f;
	Q(int a=0,int b=0,int c=0):x(a),y(b),f(c){}
};
queue<Q> q;

void print(int x,int y,int z)
{
	printf("%d %d %d\n",x,y,z);
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		if (k==0)
		{
			int inf=m*2000;f[1]=0;g[1]=1%p;a[1].clear();
			for (int i=2;i<=n;i++)
				f[i]=inf,g[i]=0,a[i].clear();
			int x,y,z;
			for (int i=1;i<=m;i++)
				scanf("%d%d%d",&x,&y,&z),a[x].push_back(data(y,z));
			q.push(Q(1,0,1));
			int s=0;
			while (!q.empty())
			{
				x=q.front().x,y=q.front().y,z=q.front().f;q.pop();
				if (y>f[x]||z<g[x]) continue;
				s++;
				if (s>m*100) break;
				for(int i=0;i<a[x].size();i++)
					if (f[x]+a[x][i].y<f[a[x][i].x])
					{
						f[a[x][i].x]=f[x]+a[x][i].y;
						g[a[x][i].x]=g[x];
						q.push(Q(a[x][i].x,f[a[x][i].x],g[a[x][i].x]));
					}
					else if (f[x]+a[x][i].y==f[a[x][i].x])
						g[a[x][i].x]=(g[a[x][i].x]+g[x])%p,
						q.push(Q(a[x][i].x,f[a[x][i].x],g[a[x][i].x]));
			}
			if (s>m*100) puts("-1");else printf("%d\n",g[n]%p);
		}
		else
		{
			int inf=m*2000;f[1]=0;g[1]=1%p;a[1].clear();
			for (int i=2;i<=n;i++)
				f[i]=inf,g[i]=0,a[i].clear();
			int x,y,z;
			for (int i=1;i<=m;i++)
				scanf("%d%d%d",&x,&y,&z),a[x].push_back(data(y,z));
			q.push(Q(1,0,1));
			while (!q.empty())
			{
				x=q.front().x,y=q.front().y,z=q.front().f;q.pop();
				if (y>f[x]) continue;
				for(int i=0;i<a[x].size();i++)
					if (f[x]+a[x][i].y<f[a[x][i].x])
					{
						f[a[x][i].x]=f[x]+a[x][i].y;
						q.push(Q(a[x][i].x,f[a[x][i].x],g[a[x][i].x]));
					}
			}
			for (int i=1;i<=n;i++)
			{
				d[i]=f[i];f[i]=inf;	
				for (int j=0;j<=k;j++)
					dp[i][j]=0;
			}
			dp[1][0]=1%p;
			q.push(Q(1,0,1));
			int s=0;
			while (!q.empty())
			{
				x=q.front().x,y=q.front().y,z=q.front().f;q.pop();
				if (y>f[x]+k||z<dp[x][y-d[x]]) continue;
				s++;
				if (s>m*100) break;
				for(int i=0;i<a[x].size();i++)
					if (y+a[x][i].y<f[a[x][i].x])
					{
						f[a[x][i].x]=y+a[x][i].y;
						if (f[a[x][i].x]-d[a[x][i].x]<=k) dp[a[x][i].x][f[a[x][i].x]-d[a[x][i].x]]=(dp[a[x][i].x][f[a[x][i].x]-d[a[x][i].x]]+dp[x][y-d[x]])%p;
//						print(f[a[x][i].x],d[a[x][i].x],k);
						q.push(Q(a[x][i].x,f[a[x][i].x],dp[a[x][i].x][f[a[x][i].x]-d[a[x][i].x]]));
//						print(a[x][i].x,f[a[x][i].x],dp[a[x][i].x][f[a[x][i].x]-d[a[x][i].x]]);
					}
					else if (y+a[x][i].y<=d[a[x][i].x]+k)
					{
						dp[a[x][i].x][y+a[x][i].y-d[a[x][i].x]]=(dp[a[x][i].x][y+a[x][i].y-d[a[x][i].x]]+dp[x][y-d[x]])%p;
						q.push(Q(a[x][i].x,y+a[x][i].y,dp[a[x][i].x][y+a[x][i].y-d[a[x][i].x]]));
//						print(a[x][i].x,y+a[x][i].y-d[a[x][i].x],dp[a[x][i].x][y+a[x][i].y-d[a[x][i].x]]);
					}
			}
			if (s>m*100) puts("-1");else
			{
				int ans=0;
				for (int i=0;i<=k;i++)
					ans=(ans+dp[n][i])%p;
				printf("%d\n",ans%p);
			}
		}
	}
	return 0;
}

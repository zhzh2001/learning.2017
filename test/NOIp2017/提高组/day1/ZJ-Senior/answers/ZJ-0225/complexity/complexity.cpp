#include<cstdio>
#include<cstring>
const int N=305;
int t,n,w,ans,p,T,q[N],v[N];
bool f[30];
char s[10],s1[10],s2[10],s3[10];
int max(int a,int b){return a>b?a:b;}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(q,0,sizeof(q));
		memset(v,0,sizeof(v));
		memset(f,0,sizeof(f));
		ans=p=t=0;
		scanf("%d%s",&n,s);
		if(s[2]=='n'){
			w=s[4]-'0';
			for(int i=5;s[i]!=')';i++) w=w*10+s[i]-'0';
		}else w=0;
		for(int i=1;i<=n;i++){
			scanf("%s",s);
			if(s[0]=='F'){
				 t++;
				 scanf("%s%s%s",s1,s2,s3);
				 if(f[s1[0]-'a']) p=-1,t=100;
				 else{
				 	int t1,t2,l1,l2;
				 	f[q[t]=s1[0]-'a']=1;
				 	if(v[t-1]!=-1){
				 		if(s2[0]!='n'){
				 			t1=s2[0]-'0';
				 			l1=strlen(s2);
				 			for(int j=1;j<l1;j++) t1=t1*10+s2[j]-'0';
				 			if(s3[0]!='n'){
				 				t2=s3[0]-'0';
				 				l2=strlen(s3);
				 				for(int j=1;j<l2;j++) t2=t2*10+s3[j]-'0';
				 				if(t2<t1) v[t]=-1;
				 				else v[t]=v[t-1];
						 	}else v[t]=v[t-1]+1;
					 	}else{
					 		if(s3[0]=='n') v[t]=v[t-1];
					 		else v[t]=-1;
						 }
					}
				 }
			}else{
				if(t-1<0 || p==-1) p=-1,t=100;
				else{
					f[q[t]]=0;
					ans=max(ans,v[t--]);
				}
			}
		}
		if(p==-1 || t!=0) puts("ERR");
		else printf("%s",w==ans?"Yes\n":"No\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<cstdio>
#include<vector>
#include<queue>
using namespace std;
typedef pair<int,int> P;
const int N=100005,M=N<<1,INF=~0U>>2;
int U,F,t,n,m,k,p,ed,x,y,z,ans,v[M],w[M],nxt[M],g[N],d[N],V[M],W[M],NXT[M],G[N],cnt[N];
priority_queue<P,vector<P>,greater<P> > pq;
void read(int &x){
	char  c;
	while((c=getchar())<'0' || c>'9');
	x=c-'0';
	while((c=getchar())>='0' && c<='9') x=x*10+c-'0';
}
void add(int x,int y,int z){
	v[++ed]=y,w[ed]=z,nxt[ed]=g[x],g[x]=ed;
	V[ed]=x,W[ed]=z,NXT[ed]=G[y],G[y]=ed;
}
void dfs(int x,int tmp){
	if(x==n) ans=(ans+1)%p;
	for(int i=g[x];i;i=nxt[i]) if(tmp+w[i]+d[v[i]]<=d[1]+k) dfs(v[i],tmp+w[i]);
}
void dfs2(int x,int tmp){
	cnt[x]++;
	if(cnt[x]>60){ans=-1;U=1;return;}
	if(U) return;
	if(x==n) ans=(ans+1)%p;
	for(int i=g[x];i && !U;i=nxt[i]) if(tmp+w[i]+d[v[i]]<=d[1]+k) dfs2(v[i],tmp+w[i]);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(t);
	while(t--){
		read(n),read(m),read(k),read(p);
		for(int i=1;i<=n;i++) cnt[i]=0;U=F=ans=0;
		while(m--){
			read(x),read(y),read(z);
			add(x,y,z);
			if(z==0) F=1;
		}
		for(int i=1;i<n;i++) d[i]=INF;
		pq.push(P(d[n]=0,n));
		while(!pq.empty()){
			P t=pq.top();pq.pop();
			if(d[x=t.second]<t.first) continue;
			for(int i=G[x];i;i=NXT[i]) if(d[V[i]]>d[x]+W[i]) pq.push(P(d[V[i]]=d[x]+W[i],V[i]));
		}
		if(!F) dfs(1,0);
		else dfs2(1,0);
		printf("%d\n",ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

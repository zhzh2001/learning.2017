#include<cmath>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
using namespace std;
char s[105],S[105][105];
int vis[233],zyy[233],tag[233],dep[233];
int readnum(char *s,int &pos){
	for (;(s[pos]<'0'||s[pos]>'9')&&s[pos]!='n';pos++);
	if (s[pos]=='n'){
		pos++;
		return 23333;
	}
	int ans=0;
	for (;s[pos]>='0'&&s[pos]<='9';pos++)
		ans=ans*10-48+s[pos];
	return ans;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T,L;
	scanf("%d",&T);
	while (T--){
		scanf("%d%s",&L,s);
		int res=0;
		if (s[2]!='1'){
			int i=4;
			for (;s[i]>='0'&&s[i]<='9';i++)
				res=res*10-48+s[i];
		}
		for (int i=1;i<=L;i++){
			scanf("%s",S[i]+1);
			if (S[i][1]=='F'){
				for (int j=1;j<=3;j++){
					int len=strlen(S[i]+1);
					S[i][len+1]=' ';
					scanf("%s",S[i]+len+2);
				}
			}
		}
		int ok=0,top=0,ans=0;
		memset(vis,0,sizeof(vis));
		for (int i=1;i<=L;i++)
			if (S[i][1]=='F'){
				char tmp=S[i][3];
				if (vis[tmp]){
					ok=1;
					break;
				}
				vis[tmp]=1;
				zyy[++top]=tmp;
				int pos=4;
				int x=readnum(S[i],pos);
				int y=readnum(S[i],pos);
				tag[top]=tag[top-1]|(x>y);
				dep[top]=dep[top-1]+(y==23333&&x!=23333);
			}
			else{
				if (!top){
					ok=1;
					break;
				}
				vis[zyy[top]]=0;
				if (!tag[top]) ans=max(ans,dep[top]);
				top--;
			}
		if (ok||top) puts("ERR");
		else if (ans==res) puts("Yes");
		else puts("No");
	}
} 

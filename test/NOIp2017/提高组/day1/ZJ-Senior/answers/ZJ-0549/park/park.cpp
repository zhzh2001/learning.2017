#include<cmath>
#include<queue>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
using namespace std;
namespace FastInput{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
}
using namespace FastInput;
#define N 100005
struct pa{
	int x,y;
	friend bool operator <(const pa &a,const pa &b){
		return a.x<b.x;
	}
};
struct edge{int to,next,v;};
int n,m,K,P;
struct Graph_for_SSSP{
	edge e[N*2];
	int head[N],dis[N],vis[N],tot;
	priority_queue<pa> q;
	void clear(){
		tot=0;
		memset(head,0,sizeof(head));
	}
	void add(int x,int y,int z){
		e[++tot]=(edge){y,head[x],z};
		head[x]=tot;
	}
	void dijk(int S){
		memset(dis,60,sizeof(dis));
		memset(vis,0,sizeof(vis));
		while (!q.empty()) q.pop();
		q.push((pa){0,S}); dis[S]=0;
		while (!q.empty()){
			int x=q.top().y; q.pop();
			if (vis[x]) continue; vis[x]=1;
			for (int i=head[x];i;i=e[i].next)
				if (dis[e[i].to]>dis[x]+e[i].v){
					dis[e[i].to]=dis[x]+e[i].v;
					q.push((pa){-dis[e[i].to],e[i].to});
				}
		}
	}
}g1,g2;
struct Graph_for_Tarjan{
	edge e[N*2];
	int head[N],low[N],dfn[N],vis[N];
	int sz[N],be[N],sta[N];
	int tot,top,T,num,n;
	void clear(int x){
		T=top=tot=num=0; n=x;
		memset(head,0,sizeof(head));
		memset(low,0,sizeof(low));
		memset(dfn,0,sizeof(dfn));
		memset(vis,0,sizeof(vis));
		memset(sz,0,sizeof(sz));
	}
	void add(int x,int y){
		e[++tot]=(edge){y,head[x]};
		head[x]=tot;
	}
	void tarjan(int x){
		low[x]=dfn[x]=++T;
		sta[++top]=x; vis[x]=1;
		for (int i=head[x];i;i=e[i].next)
			if (!dfn[e[i].to]){
				tarjan(e[i].to);
				low[x]=min(low[x],low[e[i].to]);
			}
			else if (vis[e[i].to])
				low[x]=min(low[x],dfn[e[i].to]);
		if (low[x]==dfn[x]){
			int tmp=23333333; ++num;
			for (;tmp!=x;){
				tmp=sta[top--];
				sz[num]++;
				be[tmp]=num;
				vis[num]=0;
			}
		}
	}
	void tarjan(){
		for (int i=1;i<=n;i++)
			if (!dfn[i]) tarjan(i);
	}
}g3;
int in[N][52],way[N][52];
int T,x[N*2],y[N*2],z[N*2],mx;
pa q[N*52];
void prepare(){
	memset(in,0,sizeof(in));
	in[1][0]=1; q[1]=(pa){1,0};
	for (int h=0,t=1;h!=t;){
		int x=q[++h].x,y=q[h].y;
		for (int i=g1.head[x];i;i=g1.e[i].next){
			int to=g1.e[i].to,v=g1.e[i].v;
			if (g1.dis[x]+y+v+g2.dis[to]<=mx){
				int tmp=g1.dis[x]+y+v-g1.dis[to];
				if (!in[to][tmp]) q[++t]=(pa){to,tmp};
				in[to][tmp]++;
			}
		}
	}
}
inline void add(int &x,int y){
	x=(x+y<P?x+y:x+y-P);
}
void work(){
	memset(way,0,sizeof(way));
	way[1][0]=1; q[1]=(pa){1,0};
	for (int h=0,t=1;h!=t;){
		int x=q[++h].x,y=q[h].y;
		for (int i=g1.head[x];i;i=g1.e[i].next){
			int to=g1.e[i].to,v=g1.e[i].v;
			if (g1.dis[x]+y+v+g2.dis[to]>mx) continue;
			int tmp=g1.dis[x]+y+v-g1.dis[to];
			if (!(--in[to][tmp])) q[++t]=(pa){to,tmp};
			add(way[to][tmp],way[x][y]);
		}
	}
	int ans=0;
	for (int i=0;i<=K;i++)
		add(ans,way[n][i]);
	printf("%d\n",ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while (T--){
		g1.clear();
		g2.clear();
		n=read(); m=read();
		K=read(); P=read();
		g3.clear(n);
		for (int i=1;i<=m;i++){
			x[i]=read(); y[i]=read(); z[i]=read();
			g1.add(x[i],y[i],z[i]);
			g2.add(y[i],x[i],z[i]);
			if (!z[i]) g3.add(x[i],y[i]);
		}
		g1.dijk(1);
		g2.dijk(n);
		g3.tarjan();
		mx=g1.dis[n]+K;
		int fl=1;
		for (int i=1;i<=n&&fl;i++)
			if (g3.sz[g3.be[i]]>1&&g1.dis[i]+g2.dis[i]<=mx){
				fl=0; puts("-1");
			}
		if (!fl) continue;
		g1.clear();
		for (int i=1;i<=m;i++)
			if (g1.dis[x[i]]+z[i]+g2.dis[y[i]]<=mx)
				g1.add(x[i],y[i],z[i]);
		prepare();
		work();
	}
}

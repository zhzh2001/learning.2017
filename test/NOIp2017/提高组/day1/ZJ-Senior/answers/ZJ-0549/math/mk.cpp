#include<bits/stdc++.h>
using namespace std;
int rnd(){
	int x=0;
	for (int i=1;i<=5;i++)
		x=x*10+rand()%10;
	return x;
}
int gcd(int x,int y){
	return y?gcd(y,x%y):x;
}
int main(){
	srand(time(NULL));
	freopen("math.in","w",stdout);
	int x=rnd()+1,y=rnd()+1;
	for (;gcd(x,y)!=1||x==1||y==1;x=rnd()+1,y=rnd()+1);
	printf("%d %d",x,y);
}

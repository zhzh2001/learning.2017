#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>

using namespace std;

inline void FO() {
    freopen("park.in", "r", stdin);
    freopen("park.out", "w", stdout);
    return;
}

#define fi first
#define se second
#define mp make_pair

typedef pair<int, int> PII;

const int N = 100010, M = 200010, K = 55;

int n, m, k, p, cnt;

inline void Add(int &a, int b) {
    if (!~a || !~b) a = -1;
    else a = (a + b) % p;
    return;
}

int tot, head[N << 1];
struct Edge {
    Edge(int p = 0, int w = 0, int nxt = 0) : p(p), w(w), nxt(nxt) {}
    int p, w, nxt;
} edge[M << 1];
inline void Add(int u, int v, int w) {
    edge[++tot] = Edge(v, w, head[u]);
    head[u] = tot;
    return;
}

int dist[N << 1], idg[N << 1];
bool vis[N << 1];
queue<int> que;
priority_queue<PII> pq;

void Dijkstra() {
    memset(vis, 0, sizeof(vis));
    memset(dist, 0x7f, sizeof(dist));
    dist[1] = 0;
    pq.push(mp(0, 1));
    while (!pq.empty()) {
        PII cur = pq.top();
        pq.pop();
        int u = cur.se;
        if (vis[u]) continue;
        vis[u] = 0;
        for (int i = head[u]; ~i; i = edge[i].nxt) {
            int v = edge[i].p, w = edge[i].w;
            if (dist[u] + w < dist[v]) pq.push(mp(-(dist[v] = dist[u] + w), v));
        }
    }
    return;
}

int tk, stk[N], dfn[N], low[N], fa[N];
bool isinf[N << 1];

void Tarjan(int u) {
    stk[++tk] = u;
    dfn[u] = low[u] = tk;
    for (int i = head[u]; ~i; i = edge[i].nxt) {
        int v = edge[i].p, w = edge[i].w;
        if (w) continue;
        if (!dfn[v]) {
            Tarjan(v);
            low[u] = min(low[u], low[v]);
        } else low[u] = min(low[u], dfn[v]);
    }
    if (dfn[u] == low[u]) {
        int size = 0;
        ++cnt;
        fa[cnt] = cnt;
        int j;
        do {
            j = stk[tk--];
            ++size;
            fa[j] = cnt;
            for (int i = head[u]; ~i; i = edge[i].nxt) {
                int v = edge[i].p, w = edge[i].w;
                dist[cnt] = dist[v];
                Add(cnt, v, w);
            }
        } while (j != u);
        if (size > 1) isinf[cnt] = 1;
    }
    return;
}

int f[K][N];
bool g[K][N];

void Solve() {
    scanf("%d%d%d%d", &n, &m, &k, &p);
    cnt = n;
    tot = -1;
    memset(head, -1, sizeof(head));
    for (int i = 1; i <= m; ++i) {
        int u, v, w;
        scanf("%d%d%d", &u, &v, &w);
        Add(u, v, w);
    }
    memset(isinf, 0, sizeof(isinf));
    memset(dfn, 0, sizeof(dfn));
    memset(low, 0, sizeof(low));
    for (int i = 1; i <= n; ++i)
        fa[i] = i;
    for (int i = 1; i <= n; ++i)
        if (!dfn[i]) {
            tk = 0;
            Tarjan(i);
        }
    Dijkstra();
    int res = 0;
    memset(f, 0, sizeof(f));
    if (isinf[fa[1]]) f[0][1] = -1;
    else f[0][1] = 1 % p;
    g[0][1] = 1;
    for (int dep = 0; dep <= k; ++dep) {
        memset(idg, 0, sizeof(idg));
        for (int u = 1; u <= cnt; ++u)
            if (fa[u] == u) {
                for (int i = head[u]; ~i; i = edge[i].nxt) {
                    int v = edge[i].p;
                    if (u != fa[v]) ++idg[fa[v]];
                }
            }
        for (int u = 1; u <= cnt; ++u)
            if (fa[u] == u && !idg[u]) que.push(u);
        while (!que.empty()) {
            int u = que.front();
            que.pop();
            if (!g[dep][u]) continue;
            for (int i = head[u]; ~i; i = edge[i].nxt) {
                int v = edge[i].p, w = edge[i].w;
                if (u != fa[v]) {
                    int dk = dist[u] + dep + w - dist[fa[v]];
                    if (dk > k) continue;
                    g[dk][fa[v]] = 1;
                    if (isinf[fa[v]]) f[dk][fa[v]] = -1;
                    else Add(f[dk][fa[v]], f[dep][u]);
                }
            }
        }
        Add(res, f[dep][n]);
    }
    printf("%d\n", res);
    return;
}

int main() {
    FO();
    int t;
    scanf("%d", &t);
    while (t--) Solve();
    return 0;
}

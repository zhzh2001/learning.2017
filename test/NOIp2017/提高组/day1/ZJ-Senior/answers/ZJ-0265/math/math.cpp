#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

inline void FO() {
    freopen("math.in", "r", stdin);
    freopen("math.out", "w", stdout);
    return;
}

typedef long long LL;

int main() {
    FO();
    LL a, b;
    scanf("%lld%lld", &a, &b);
    LL res = a * b - a - b;
    printf("%lld\n", res);
    return 0;
}

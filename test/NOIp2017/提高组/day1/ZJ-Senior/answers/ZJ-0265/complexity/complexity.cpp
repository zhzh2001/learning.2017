#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

inline void FO() {
    freopen("complexity.in", "r", stdin);
    freopen("complexity.out", "w", stdout);
    return;
}

const int LEN = 110;

int cnt[LEN];
bool v[LEN], ch[LEN];
char fzd[LEN], a[LEN], bl[LEN], temp[LEN];

int Read() {
    scanf("%s", temp + 1);
    if (temp[1] == 'n') return 0;
    int t = 0;
    int len = strlen(temp + 1);
    for (int i = 1; i <= len; ++i)
        t = t * 10 + temp[i] - 48;
    return t;
}

void Solve() {
    int l, t = 0;
    scanf("%d%s", &l, fzd + 1);
    int len = strlen(fzd + 1);
    if (len == 4) {
        if (fzd[3] == '1') t = 0;
        else t = 1;
    } else {
        for (int i = 5; i < len; ++i)
            t = t * 10 + fzd[i] - 48;
    }
    bool ew = 0;
    int dep = 0, maxd = 0;
    memset(v, 0, sizeof(v));
    memset(cnt, 0, sizeof(cnt));
    memset(ch, 0, sizeof(ch));
    while (l--) {
        scanf("%s", a + 1);
        if (a[1] != 'F') {
            if (!dep) {
                ew = 1;
                break;
            }
            v[bl[dep--] - 97] = 0;
        } else {
            scanf("%s", a + 1);
            int x = Read(), y = Read();
            bl[++dep] = a[1];
            if (v[bl[dep] - 97]) {
                ew = 1;
                break;
            } else v[bl[dep] - 97] = 1;
            if (ch[dep - 1]) {
                ch[dep] = 1;
                cnt[dep] = cnt[dep - 1];
                continue;
            }
            if (x) {
                if (y) {
                    if (x > y) {
                        ch[dep] = 1;
                        cnt[dep] = cnt[dep - 1];
                    } else {
                        cnt[dep] = cnt[dep - 1];
                    }
                } else {
                    cnt[dep] = cnt[dep - 1] + 1;
                }
            } else {
                if (y) {
                    ch[dep] = 1;
                    cnt[dep] = cnt[dep - 1];
                } else {
                    cnt[dep] = cnt[dep - 1];
                }
            }
            if (maxd < cnt[dep]) maxd = cnt[dep];
        }
    }
    if (dep) ew = 1;
    for (; l > 0; --l) {
        scanf("%s", a + 1);
        if (a[1] != 'E') scanf("%s%s%s", a + 1, a + 1, a + 1);
    }
    if (ew) puts("ERR");
    else if (maxd != t) puts("No");
    else puts("Yes");
    return;
}

int main() {
    FO();
    int t;
    scanf("%d", &t);
    while (t--) Solve();
    return 0;
}

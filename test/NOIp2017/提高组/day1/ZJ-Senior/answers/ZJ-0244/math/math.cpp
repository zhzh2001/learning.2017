//#include <map>
//#include <set>
//#include <queue>
//#include <ctime>
//#include <cmath>
//#include <vector>
//#include <bitset>
//#include <cstdio>
//#include <cstring>
//#include <iostream>
//#include <algorithm>
//#define debug(x) cout<<#x<<" = "<<x<<endl;
//using namespace std;
//#define ll long long
//#define db double
//void rd(int &x){
//	char c;x=0;
//	int f=1;
//	while(c=getchar(),!isdigit(c)&&c!='-');
//	if(c=='-')c=getchar(),f=-1;
//	do x=(x<<3)+(x<<1)+(c^48);
//	while(c=getchar(),isdigit(c));
//	x*=f;
//}
//int n,m;
//void ex_gcd(int a,int b,int &x,int &y){
//	if(!b){
//		x=v;
//		y=0;
//	}else{
//		ex_gcd(b,a%b,y,x);
//		int c=x-x/y;
//		x=y;
//		y=c;
//	}
//}
//bool chk(ll L){
//	for(ll v=L;c<=L+n-1;v++){
//		ll x=-1,y=-1;
//		ex_gcd(n,m,x,y);
//		if(1ll*x*n+1ll*y*m!=v)return 0;
//	}
//	return 1;
//}
//int main(){
////	freopen("math.in","r",stdin);
////	freopen("math.out","w",stdout);
//	rd(n),rd(m);
//	if(n>m)swap(n,m);
//	ll l=1,r=2e18;
//	ll res;
//	while(l<=r){
//		ll md=(l+r)/2;
//		if(chk(md+1)){
//			res=md;
//			r=md-1;
//		}else l=md+1;
//	}
//	printf("%lld\n",res);
//	return 0;
//}
#include <map>
#include <set>
#include <queue>
#include <ctime>
#include <cmath>
#include <vector>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define debug(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
#define ll long long
#define db double
const int M=7e4;
void rd(int &x){
	char c;x=0;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),isdigit(c));
	x*=f;
}
int n,m,res;
bool mk[M];
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	rd(n),rd(m);
	if(n>m)swap(n,m);
	for(int i=0;i<M;i++){
		int x1=i*n;
		for(int j=0;x1+j*m<M;j++)mk[x1+j*m]=1;
	}
	for(int i=0;i<M;i++){
		if(!mk[i])res=i;
	}
	printf("%d\n",res);
	return 0;
}

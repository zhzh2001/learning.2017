#include <map>
#include <set>
#include <queue>
#include <ctime>
#include <cmath>
#include <vector>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
//#define debug(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
#define ll long long
#define db double
const int M=105;
void rd(int &x){
	char c;x=0;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),isdigit(c));
	x*=f;
}
int cas,L;
char stk[M];
bool mk[500];
char FU[10];
int get(char *str,int l,int r){
	int num=0;
	for(int i=l;i<=r;i++)num=10*num+str[i]-'0';
	return num;
}
struct node{
	char str[2];
	char c[2];
	char x[5],y[5];
}a[M];
struct P1{//常数 
	int cant[M];
	void solve(){
		int ans=0,top=0;
		memset(mk,0,sizeof mk);
		memset(cant,0,sizeof cant);
		for(int i=1;i<=L;i++){
			scanf("%s",a[i].str);
			if(a[i].str[0]=='F'){
				scanf("%s %s %s",a[i].c,a[i].x,a[i].y);
				if(ans==3)continue;
				if(mk[a[i].c[0]]){//re
					ans=3;
					continue;
				}
				stk[++top]=i;
				mk[a[i].c[0]]=1;
				int fx=isdigit(a[i].x[0]);
				int fy=isdigit(a[i].y[0]);
				//若错和er，输出er 
				if(fx&&fy){
					int X,Y;
					X=get(a[i].x,0,strlen(a[i].x)-1);
					Y=get(a[i].y,0,strlen(a[i].y)-1);
					if(X>Y)cant[i]=1;
				}else if(fx&&fy==0){//5 n
					if(top>1){
						if(cant[stk[top-1]])continue;
					}else{
						ans=2;
						continue;
					}
				}else if(fx==0&&fy){//n 5
					cant[i]=1;
				}
			}else if(a[i].str[0]=='E'){
				if(!top){
					ans=3;
					continue;
				}
				mk[a[stk[top--]].c[0]]=0;
			}
		}
		if(top)ans=3;
		if(ans);
		else ans=1;
		if(ans==1)puts("Yes");
		else if(ans==2)puts("No");
		else puts("ERR");
	}
}P1;
struct P2{//n和常数 
	int tot[M];
	int cant[M];
	int LL[M],RR[M];
	void dfs(int p){
		if(LL[p]==RR[p]+1)return;
		if(cant[p])return;
		int mx=0;
		for(int i=LL[p]+1;i!=RR[p];i=RR[i]+1){
			dfs(i);
			mx=max(mx,tot[i]);
		}
		tot[p]+=mx;
	}
	void solve(int w){
		int ans=0,top=0;
		memset(mk,0,sizeof mk);
		memset(tot,0,sizeof tot);
		memset(cant,0,sizeof cant);
		for(int i=1;i<=L;i++){
			scanf("%s",a[i].str);
			if(a[i].str[0]=='F'){
				scanf("%s %s %s",a[i].c,a[i].x,a[i].y);
				if(ans==3)continue;
				if(mk[a[i].c[0]]){//re
					ans=3;
					continue;
				}
				stk[++top]=i;
				mk[a[i].c[0]]=1;
				int fx=isdigit(a[i].x[0]);
				int fy=isdigit(a[i].y[0]);
				//若错和er，输出er 
				if(fx&&fy){
					int X,Y;
					X=get(a[i].x,0,strlen(a[i].x)-1);
					Y=get(a[i].y,0,strlen(a[i].y)-1);
					if(X>Y)cant[i]=1;
				}else if(fx&&fy==0){//5 n
					if(top>1){
						if(cant[stk[top-1]])continue;
						else tot[i]=1;
					}else{
						tot[i]=1;
						continue;
					}
				}else if(fx==0&&fy){//n 5
					cant[i]=1;
				}
			}else if(a[i].str[0]=='E'){
				if(!top){
					ans=3;
					continue;
				}
				mk[a[stk[top--]].c[0]]=0;
			}
		}
		if(top||ans==3)puts("ERR");
		else{
			for(int i=1;i<=L;i++){
				if(a[i].str[0]=='F'){
					LL[i]=i;
					stk[++top]=i;
				}else RR[stk[top--]]=i;
			}
			LL[0]=0,RR[0]=L+1;
			dfs(0);
			if(tot[0]==w)puts("Yes");
			else puts("No");
		}
	}
}P2;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	rd(cas);
	while(cas--){
		rd(L);
		scanf("%s",FU);
		int ki,w;
		if(FU[2]=='1')P1.solve();
		else{
			int w=get(FU,4,strlen(FU)-2);
			P2.solve(w);
		}
	}
	return 0;
}

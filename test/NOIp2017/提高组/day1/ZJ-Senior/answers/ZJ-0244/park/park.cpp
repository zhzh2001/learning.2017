#include <map>
#include <set>
#include <queue>
#include <ctime>
#include <cmath>
#include <vector>
#include <bitset>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define debug(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
#define ll long long
#define db double
const int M=1e5+5;
void rd(int &x){
	char c;x=0;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do x=(x<<3)+(x<<1)+(c^48);
	while(c=getchar(),isdigit(c));
	x*=f;
}
struct node{
	int v,w;
	bool operator<(const node &a)const{
		return w>a.w;
	}
};
priority_queue<node>q;
int cas,n,m,mod,K;
vector<node>e[M];
int dis[M],g[M];
bool mk[M];
bool chk(int &x,int y){
	if(x==-1||x>y)return x=y,1;
	return 0;
}
void Dij(){
	memset(mk,0,sizeof mk);
	memset(dis,-1,sizeof dis);
	while(!q.empty())q.pop();
	q.push((node){1,dis[1]=0});
	while(!q.empty()){
		node now=q.top();
		q.pop();
		int u=now.v;
		if(mk[u])continue;
		mk[u]=1;
		for(int i=0,v,w;i<e[u].size();i++){
			v=e[u][i].v;
			w=e[u][i].w;
			if(mk[v])continue;
			if(chk(dis[v],dis[u]+w))q.push((node){v,dis[v]});
		}
	}
}
struct DD{
	int id,d;
	bool operator<(const DD &a)const{
		return d<a.d;
	}
}D[M];
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	rd(cas);
	while(cas--){
		rd(n),rd(m),rd(K),rd(mod);
		for(int i=1,u,v,w;i<=m;i++){
			rd(u),rd(v),rd(w);
			e[u].push_back((node){v,w});
		}
		if(!K){
			Dij();
			memset(g,0,sizeof g);
			g[1]=1;
			for(int i=1;i<=n;i++)D[i]=(DD){i,dis[i]};
			sort(D+1,D+n+1);
			for(int i=1;i<=n;i++){
				for(int j=0,v,w;j<e[i].size();j++){
					v=e[i][j].v;
					w=e[i][j].w;
					if(dis[v]==dis[i]+w)g[v]=(g[v]+g[i])%mod;
				}
			}
			printf("%d\n",g[n]);
			for(int i=1;i<=n;i++)e[i].clear();
		}else puts("-1");
	}
	return 0;
}

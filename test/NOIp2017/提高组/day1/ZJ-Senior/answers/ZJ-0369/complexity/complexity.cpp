#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
using namespace std;

int t,n,x,top,tot,ans,flag,flag2,x1,x2;
char s[1000],c1,ca[1000],s1[100],s2[100],s3[100];
int vis[1000],sta[1000];
string g;
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		top=0;
		memset(vis,0,sizeof vis);
		scanf("%d %s",&n,s);
		x=0;
		for(int i=0;i<strlen(s);i++)
			if(s[i]>='0' && s[i]<='9') x=(x<<3)+(x<<1)+s[i]-'0';
		if(x==1 && strlen(s)<=4) x=0;
		ans=0;
		memset(sta,0,sizeof sta);
		for(int i=1;i<=n;i++){
			scanf("%s",s);
			flag=0; flag2=0;
			if(s[0]=='F'){
				scanf("%s %s %s",s1,s2,s3);
				c1=s1[0];
				if(vis[c1]){
					printf("ERR\n");
					for(int j=i+1;j<=n;j++){
						scanf("%s",s);
						if(s[0]=='F') scanf("%s %s %s",s1,s2,s3);
					}
					flag=1;
					break;
				}
				x1=0; x2=0;
				for(int j=0;j<strlen(s2);j++)
					if(s2[j]>='0' && s2[j]<='9') x1=(x1<<3)+(x1<<1)+s2[j]-'0';
				for(int j=0;j<strlen(s3);j++)
					if(s3[j]>='0' && s3[j]<='9') x2=(x2<<3)+(x2<<1)+s3[j]-'0';
				if((s2[0]=='n' && s3[0]!='n') || (s2[0]!='n' && s3[0]!='n' && x1>x2)){
					flag2=1;
					tot=1;
					top++;
					while(tot){
						scanf("%s\n",s); i++;
						if(s[0]=='F'){
							scanf("%s %s %s",s1,s2,s3);
							tot++;
							c1=s1[0];
							if(vis[c1]){
								printf("ERR\n");
								for(int j=i+1;j<=n;j++){
									scanf("%s",s);
									if(s[0]=='F') scanf("%s %s %s",s1,s2,s3);
								}
								flag=1;
								break;
							}
							top++; ca[top]=c1;
						}
						if(s[0]=='E'){
							tot--;
							vis[ca[top]]=0; top--;
						}
						if(i>n && tot!=0){
							printf("ERR\n");
							for(int j=i+1;j<=n;j++){
								scanf("%s",s);
								if(s[0]=='F') scanf("%s %s %s",s1,s2,s3);
							}
							flag=1;
							break;
						}
					}
					if(flag) break;
				}
				if(s3[0]=='n' && s2[0]!='n') sta[top+1]=sta[top]+1; else sta[top+1]=sta[top];
				if(!flag2){
					top++;
					ans=max(ans,sta[top]);
					vis[c1]=1; ca[top]=c1;
				}
			}
			if(s[0]=='E' && !flag2){
				if(top==0){
					printf("ERR\n");
					for(int j=i+1;j<=n;j++){
						scanf("%s",s);
						if(s[0]=='F') scanf("%s %s %s",s1,s2,s3);
					}
					flag=1;
					break;
				}
				vis[ca[top]]=0; top--;
			}
		}
		if(!flag) if(top!=0) printf("ERR\n"); else
		if(ans==x) printf("Yes\n"); else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

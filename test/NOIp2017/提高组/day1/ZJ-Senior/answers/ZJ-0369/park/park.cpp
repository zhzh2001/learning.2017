#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#include<queue>
#define M 200100
#define N 100100
using namespace std;

inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &x){
	char c=nc();
	for(;(c<'0' || c>'9');c=nc());
	for(x=0;(c>='0' && c<='9');x=(x<<3)+(x<<1)+c-'0',c=nc());
}

struct asd{
	int to,next,w;
}edge[M];

int cnt,head[N];
inline void ADD(int u,int v,int w){
	edge[++cnt].to=v; edge[cnt].w=w; edge[cnt].next=head[u];
	head[u]=cnt;
}

queue<int>Q;
int n,vis[N],dis[N],a[M],b[M],c[M];
inline void spfa(){
	Q.push(n);
	memset(vis,0,sizeof vis);
	vis[n]=1;
	for(int i=1;i<=n;i++) dis[i]=1<<30;
	dis[n]=0;
	while(!Q.empty()){
		int u=Q.front(); Q.pop(); vis[u]=0;
		for(int i=head[u];i;i=edge[i].next)
			if(dis[u]+edge[i].w<dis[edge[i].to]){
				dis[edge[i].to]=dis[u]+edge[i].w;
				if(!vis[edge[i].to]){
					vis[edge[i].to]=1;
					Q.push(edge[i].to);
				}
			}
	}
}

int ans,d,p,k,T,m;
int f[N][55];
inline int dfs(int x,int tot){
	if(x==n) return 1;
	if(f[x][tot+dis[x]-d]) return f[x][tot+dis[x]-d];
	for(int i=head[x];i;i=edge[i].next)
		if(tot+edge[i].w+dis[edge[i].to]-d<=k)
			f[x][tot+dis[x]-d]=(dfs(edge[i].to,tot+edge[i].w)+f[x][tot+dis[x]-d])%p;
	return f[x][tot+dis[x]-d];
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while(T--){
		cnt=0; ans=0;
		memset(head,0,sizeof head);
		memset(edge,0,sizeof edge);
		read(n); read(m); read(k); read(p);
		for(int i=1;i<=m;i++){
			read(a[i]); read(b[i]); read(c[i]);
			ADD(b[i],a[i],c[i]);
		}
		spfa();
		d=dis[1];
		cnt=0;
		memset(head,0,sizeof head);
		memset(edge,0,sizeof edge);
		for(int i=1;i<=m;i++) ADD(a[i],b[i],c[i]);
		memset(f,0,sizeof f);
		ans=dfs(1,0);
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

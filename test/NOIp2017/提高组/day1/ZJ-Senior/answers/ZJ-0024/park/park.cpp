#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <queue>
#include <iostream>
using namespace std;
template <typename tp> inline void read(tp& x)
{
	x=0;char tmp;bool key=0;
	for(tmp=getchar();!(tmp>='0'&&tmp<='9');tmp=getchar()) key=(tmp=='-');
	for(;(tmp>='0'&&tmp<='9');tmp=getchar()) x=(x<<1)+(x<<3)+tmp-'0';
	if(key) x=-x;
}
const int N=100010,M=200010,K=55;
struct edge{
	int la,b,v;
}con[M];
int n,m,k,p;
int dis[N],cnt[N],ways[N][K],uns[N][K],now[N];//unsolveed
int disn[N];
bool inq[N],avail[N];
queue<int> q;
struct graph{
	edge con[M];
	int tot,fir[N];
	inline void add(int from,int to,int val)
	{
		con[++tot].la=fir[from];
		con[tot].b=to;con[tot].v=val;
		fir[from]=tot;
	}
	void dfs(int pos)
	{
		avail[pos]=1;
		for(int i=fir[pos];i;i=con[i].la)
		{
			if(!avail[con[i].b]) dfs(con[i].b);
		}
	}
	void spfa();
	void spfa_init1(int st);
	void spfa_init2(int st);
	inline void init()
	{
		memset(fir,0,sizeof fir);
		tot=0;
	}
}g1,g2;
inline bool push_q(int pos)
{
	inq[pos]=1;
	q.push(pos);
	if(++cnt[pos]==n) return 0;
	return 1;
}
void graph::spfa()
{
	ways[1][0]=uns[1][0]=now[1]=1;
	memset(cnt,0,sizeof cnt);
	push_q(1);
	int pos;
	while(!q.empty())
	{
		pos=q.front();
		q.pop();
		inq[pos]=0;
		for(int i=fir[pos];i;i=con[i].la)if(avail[con[i].b]&&dis[con[i].b]-dis[pos]-con[i].v<=now[pos])
		{
			for(int j=max(0,dis[con[i].b]-dis[pos]-con[i].v);j<=now[pos];j++)
			{
				(ways[con[i].b][dis[pos]+j+con[i].v-dis[con[i].b]]+=uns[pos][j])%=p;
				(uns[con[i].b][dis[pos]+j+con[i].v-dis[con[i].b]]+=uns[pos][j])%=p;		
			}
			now[con[i].b]=max(now[con[i].b],dis[pos]+now[pos]+con[i].v-dis[con[i].b]);
			now[con[i].b]=min(now[con[i].b],k+dis[n]-dis[con[i].b]-disn[con[i].b]);
			if(!inq[con[i].b]){
				if(!push_q(con[i].b))
				{
					puts("-1");
					return;
				}
			}
		}
		memset(uns[pos],0,sizeof uns[pos]);
		now[pos]=0;
	}
	int ans=0;
	for(int i=0;i<=k;i++) (ans+=ways[n][i])%=p;
	printf("%d\n",ans);
}
void graph::spfa_init1(int st)
{
	memset(dis,0x3f,sizeof dis);
	memset(cnt,0,sizeof cnt);
	dis[st]=0;
	push_q(st);
	int pos;
	while(!q.empty())
	{
		pos=q.front();
		q.pop();
		inq[pos]=0;
		for(int i=fir[pos];i;i=con[i].la)if(avail[con[i].b])
		{
			if(dis[con[i].b]>dis[pos]+con[i].v)
			{
				dis[con[i].b]=dis[pos]+con[i].v;
				if(!inq[con[i].b]) push_q(con[i].b);
			}
		}
	}
}
void graph::spfa_init2(int st)
{
	memset(disn,0x3f,sizeof disn);
	disn[st]=0;
	push_q(st);
	int pos;
	while(!q.empty())
	{
		pos=q.front();
		q.pop();
		inq[pos]=0;
		avail[pos]=1;
		for(int i=fir[pos];i;i=con[i].la)
		{
			if(disn[con[i].b]>disn[pos]+con[i].v)
			{
				disn[con[i].b]=disn[pos]+con[i].v;
				if(!inq[con[i].b]) push_q(con[i].b);
			}
		}
	}
}
inline void init()
{
	memset(avail,0,sizeof avail);
	memset(cnt,0,sizeof cnt);
	memset(ways,0,sizeof ways);
	memset(uns,0,sizeof uns);
	g1.init();g2.init();
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t,from,to,val;
	read(t);
	while(t--)
	{
		init();
		read(n);read(m);read(k);read(p);
		for(register int i=1;i<=m;++i)
		{
			read(from);read(to);read(val);
			g1.add(from,to,val);
			g2.add(to,from,val); 
		}
		g2.spfa_init2(n);
		g1.spfa_init1(1);
		g1.spfa();
	}
	return 0;
}

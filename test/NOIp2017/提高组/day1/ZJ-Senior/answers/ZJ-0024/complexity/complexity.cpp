#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
using namespace std;
int l,com;
bool used[27];
int now;
void read(char& x)
{
	for(x=getchar();!(x>='a'&&x<='z');x=getchar());
}
int read_num()
{
	char x;
	for(x=getchar();!((x>='0'&&x<='9')||x=='n');x=getchar());
	if(x=='n') return -1;
	int res=0;
	for(;x>='0'&&x<='9';x=getchar()) res=(res<<1)+(res<<3)+x-'0';
	return res;
}
int judge()
{
	char tmp;
	int x,y,res,sub=0,tt;
	now++;
	for(tmp=getchar();tmp!='F'&&tmp!='E';tmp=getchar());
	if(tmp=='F')
	{
		read(tmp);
		x=read_num();
		y=read_num();
		if(x>0&&y==-1) res=1;
		else if((x==-1&&y!=-1)||(x>0&&y>0&&x>y)) res=-1;
		else res=0;
		if(used[tmp-'a']) res=-3;
		used[tmp-'a']=1;
		if(now==l) return -3;	
		while(tt=judge(),tt!=-2)
		{
			if(tt==-3){
				res=-3;
			}
			if(res>=0)
			{
				sub=max(sub,tt);
			}
			if(now==l) return -3;
		}
		used[tmp-'a']=0;
		if(res>=0) return sub+res;
		else if(res==-1) return 0;
		else return res;
	}
	else return -2;
}
void solve()
{
	char tmp;
	scanf("%d",&l);
	memset(used,0,sizeof used);
	now=0;
	while(tmp!='(') tmp=getchar();
	tmp=getchar();
	if(tmp=='1') com=0;
	else
	{
		while(tmp!='^') tmp=getchar();
		scanf("%d)",&com);
	}
	int tt,res=0,sub=0;
	while(tt=judge(),tt!=-2)
	{
		if(tt==-3){
			res=-3;
		}
		if(res>=0)
		{
			sub=max(sub,tt);
		}
		if(now==l) break;
	}
	if(now!=l)
	{
		res=-3;
		while(now!=l) tt=judge();
	}
	if(res!=-3) res+=sub;
	if(res==com) puts("Yes");
	else if(res!=-2&&res!=-3) puts("No");
	else puts("ERR");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	for(;t--;)
		solve();
	return 0;
}

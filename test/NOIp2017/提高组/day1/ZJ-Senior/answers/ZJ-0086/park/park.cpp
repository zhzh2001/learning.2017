#include <cstdio>
#include <cstring>
#include <cctype>
#include <vector>
#include <queue>
using std::vector;
using std::queue;

template <typename T>
inline void get(T& a) {
	char c = getchar();
	while (!isdigit(c)) c = getchar();
	a = 0;
	while (isdigit(c)) {
		a = a * 10 + (c & 0xf);
		c = getchar();
	}
}

struct edg {
	int to, val;
	edg() {}
	edg(int to, int val) : to(to), val(val) {}
};

int n, m, k, p;
int dis[100001];
int vis[100001];
long long cnt[100001];
vector<edg> g[100001];

void spfa() {
	memset(dis, 0xf, sizeof(dis));
	memset(vis, 0, sizeof(vis));
	memset(cnt, 0, sizeof(cnt));
	queue<int> q;
	q.push(1);
	vis[1] = 1;
	dis[1] = 0;
	cnt[1] = 1;
	while (!q.empty()) {
		int temp = q.front();
		q.pop();
		vis[temp] = 0;
		for (unsigned i = 0; i < g[temp].size(); i++) {
			edg& toe = g[temp][i];
			if (dis[temp] + toe.val < dis[toe.to]) {
				dis[toe.to] = dis[temp] + toe.val;
				cnt[toe.to] = cnt[temp];
				if (!vis[toe.to]) {
					vis[toe.to] = 1;
					q.push(toe.to);
				}
			} else if (dis[temp] + toe.val == dis[toe.to]) {
				cnt[toe.to] += cnt[temp];
				cnt[toe.to] %= p;
				if (!vis[toe.to]) {
					vis[toe.to] = 1;
					q.push(toe.to);
				}
			}
		}
	}
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w" ,stdout);
	int T;
	get(T);
	while (T--) {
		for (int i = 1; i <= n; i++)
			g[i].clear();
		get(n);
		get(m);
		get(k);
		get(p);
		for (int i = 1; i <= m; i++) {
			int st, to, val;
			get(st);
			get(to);
			get(val);
			g[st].push_back(edg(to, val));
		}
		spfa();
		printf("%lld\n", cnt[n]);
	}
	return 0;
}

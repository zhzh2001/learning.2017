#include <cstdio>
#include <algorithm>
using std::max;

int a, b;
bool f[100000001];

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w" ,stdout);
	scanf("%d %d", &a, &b);
	f[0] = 1;
	for (int i = 2; i <= a * b; i++) {
		if (i - a >= 0 && f[i - a]) f[i] = 1;
		if (i - b >= 0 && f[i - b]) f[i] = 1;
	}
	int ans = 0;
	for (int i = 1; i <= a * b; i++) {
		if (f[i] == 0) ans = max(ans, i);
	}
	printf("%d\n", ans);
	return 0;
}

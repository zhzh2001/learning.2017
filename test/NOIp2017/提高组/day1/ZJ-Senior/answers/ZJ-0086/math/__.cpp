#include <cstdio>
#include <cctype>
#include <algorithm>
using std::abs;
using std::max;
using std::swap;

long long ans;

int gcd(int a, int b) {
	while (b) {
		printf("%d %d\n", a, b);
		int t = b;
		ans += abs(a - b);
		b = a % b;
		a = t;
	}
	ans += a;
	return a;
}

template <typename T>
void exgcd(T a, T b, T c, T& x, T& y, T& d) {
	if (!b) {
		y = 0;
		d = a;
		x = c / a;
	} else {
		exgcd(b, a % b, y, x, d);
		y -= (a / b) * x;
	}
}

int main() {
	int a, b;
	scanf("%d %d", &a, &b);
	gcd(a, b);
	printf("ans: %lld\n", ans);
	return 0;
}

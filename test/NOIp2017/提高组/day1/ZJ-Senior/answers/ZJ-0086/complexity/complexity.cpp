#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
using std::max;

struct loop {
	int var;
	int noenter;
	int iscont;
};

int vis[26];
loop stk[101];
int stkcnt;

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T;
	char c = getchar();
	while (!isdigit(c)) c = getchar();
	T = 0;
	while (isdigit(c)) {
		T = T * 10 + (c & 0xf);
		c = getchar();
	}
	while (T--) {
		memset(vis, 0, sizeof(vis));
		stkcnt = 0;
		int l;
		c = getchar();
		while (!isdigit(c)) c = getchar();
		l = 0;
		while (isdigit(c)) {
			l = l * 10 + (c & 0xf);
			c = getchar();
		}
		while (!isgraph(c)) c = getchar();
		while (c == 'O' || c == '(') c = getchar();
		int pown = 0;
		if (isdigit(c)) {
			while (isgraph(c)) c = getchar();
		} else if (isalpha(c)) {
			c = getchar();
			while (ispunct(c) || isalpha(c)) c = getchar();
			while (isdigit(c)) {
				pown = pown * 10 + (c & 0xf);
				c = getchar();
			}
			while (isgraph(c)) c = getchar();
		}
		int err = 0;
		int comp = 0;
		int layer = 0;
		int cont = 0;
		int noenter_flag = 0;
		for (int i = 0; i < l; i++) {
			while (!isgraph(c)) c = getchar();
			int iscont;
			if (c == 'F') {
				int noenter = noenter_flag;
				c = getchar();
				while (!isgraph(c)) c = getchar();
				if (vis[c - 'a']) {
					err = 1;
				} else {
					vis[c - 'a'] = 1;
					stk[stkcnt + 1].var = c - 'a';
				}
				layer++;
				c = getchar();
				while (!isgraph(c)) c = getchar();
				int x = 0;
				int	y = 0;
				if (isdigit(c)) {
					while (isdigit(c)) {
						x = x * 10 + (c & 0xf);
						c = getchar();
					}
				} else {
					c = getchar();
					noenter = 1;
				}
				while (!isgraph(c)) c = getchar();
				if (isdigit(c)) {
					while (isdigit(c)) {
						y = y * 10 + (c & 0xf);
						c = getchar();
					}
					if (x > y) {
						noenter = 1;
					}
				} else if (c == 'n') {
					c = getchar();
					if (!noenter) {
						cont++;
						stk[stkcnt + 1].iscont = 1;
						comp = max(comp, cont);
					}
				}
				if (noenter && noenter_flag == 0)
					stk[stkcnt + 1].noenter = 1;
				stkcnt++;
				if (noenter) noenter_flag = 1;
			} else if (c == 'E') {
				c = getchar();
				layer--;
				if (layer < 0) {
					err = 1;
				 	continue;
				}
				vis[stk[stkcnt].var] = 0;
				if (stk[stkcnt].noenter)
					noenter_flag = 0;
				if (stk[stkcnt].iscont)
					cont--;
				stkcnt--;
			}
		}
		if (err || layer)
			printf("ERR\n"); 
		else if (comp == pown) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

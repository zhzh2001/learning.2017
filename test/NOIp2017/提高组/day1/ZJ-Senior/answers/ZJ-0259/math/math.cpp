#include <cstdio>
using namespace std;

typedef long long LL;
LL x, y;

int main(){
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld%lld", &x, &y);
	if (x==1||y==1) printf("0\n");
	else printf("%lld\n", (x-1)*(y-1)-1);
	fclose(stdin); fclose(stdout);
	return 0;
}

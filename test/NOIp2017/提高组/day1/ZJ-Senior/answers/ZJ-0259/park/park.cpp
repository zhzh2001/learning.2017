#include <queue>
#include <cctype>
#include <cstdio>
#include <cstring>

using namespace std;

const int maxn=2e5+5, maxm=4e5+5, INF=1e9, maxk=55;

class Graph{
public:
	struct Edge{
		int to, next, v; Graph *bel;
		Edge& operator ++(){
			return *this=bel->edge[next];}
		inline int operator *(){ return to; } 
	};
	void reset(){
		memset(fir, 0, sizeof(fir));
		cntedge=0; }
	void addedge(int x, int y, int v){
		Edge &e=edge[++cntedge];
		e.to=y; e.next=fir[x]; e.v=v;
		e.bel=this; fir[x]=cntedge;
	}
	Edge& getlink(int x){ return edge[fir[x]]; }
private:
	int cntedge, fir[maxn];
	Edge edge[maxm];
}g;

int T, n, m, k, p, dis[maxn], dp[maxn][maxk];
int q[maxn], h, t, inq[maxn], visit[maxn];

int getint(){
	int re=0; char c;
	for (c=getchar(); !isgraph(c); c=getchar());
	for (re=c-48; c=getchar(), isgraph(c); re=re*10+c-48);
	return re;
}

void spfa(int x){
	for (int i=1; i<=n; ++i){
		dis[i]=INF; inq[i]=0; }
	dis[x]=0; h=t=0; q[t++]=x; inq[x]=1; 
	Graph::Edge e; int now;
	while (h<t){
		now=q[h++];
		for (e=g.getlink(now); *e; ++e)
		if (dis[now]+e.v<dis[*e]){
			dis[*e]=dis[now]+e.v;
			if (!inq[*e]) q[t++]=*e;
			inq[*e]=1; 
		}
	}
}

void dfs(int now){
	Graph::Edge e=g.getlink(now); int j;
	for (; *e; ++e){
		for (int i=0; i<=k; ++i){
			j=dis[now]+i+e.v-dis[*e];
			if (j>=0&&j<=k) dp[*e][j]+=dp[now][i];
			dp[*e][j]%=p;
		}
		if (visit[*e]) continue;
		visit[*e]=1; dfs(*e);
	}
}

int main(){
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int x, y, z, ans=0;
	scanf("%d", &T);
	while (T--){
		g.reset();
		scanf("%d%d%d%d", &n, &m, &k, &p);
		for (int i=0; i<=n; ++i) 
		for (int j=0; j<=k; ++j) dp[i][j]=0;
		for (int i=0; i<m; ++i){
			x=getint(); y=getint(); z=getint();
			g.addedge(x, y, z);
		}
		spfa(1); dp[1][0]=1;
		visit[1]=1; dfs(1); ans=0;
		for (int i=0; i<=k; ++i) ans=(ans+dp[n][i])%p;
		printf("%d\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}


#include <cctype>
#include <cstdio>
#include <cstring>
using namespace std;

const int maxl=205;

struct OP{
	int type, be, en, c;
	char vary;
}op[maxl];

int T, n, c, clen, nowc, ans; //O(n^c)
//stack:ѭ����ջ 
int stack[maxl], tail, jump[maxl], belong[maxl];
char comp[100], use[200];
char tmp[100], tmp1[100],tmp2[100];

char readchar(){
	char re;
	for (re=getchar(); !isgraph(re); re=getchar());
	return re;
}

int trans(char *s, int l){
	int re=0; 
	for (int i=0; i<l; ++i) re=re*10+s[i]-48;
	return re;
}

void init(){
	memset(op, 0, sizeof(op));
	memset(use, 0, sizeof(use));
	memset(jump, 0, sizeof(jump));
}

int main(){
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	scanf("%d", &T); bool ok;
	while (T--){
		init();
		scanf("%d%s", &n, comp);
		clen=strlen(comp); c=0;
		if (clen==4) c=0; else
			for (int i=0; i<clen; ++i)
				if (isdigit(comp[i])) c=c*10+comp[i]-48;
		//Ԥ���� 
		tail=0; ok=true; ans=0;
		for (int i=1; i<=n; ++i){
			scanf("%s", tmp);	
			if (tmp[0]=='F'){
				if (!ok){ scanf("%s%s%s", tmp, tmp1, tmp2); continue; }
				stack[tail++]=i;
				op[i].type=1; op[i].vary=readchar();
				scanf("%s%s", tmp1, tmp2);
				if (tmp1[0]=='n') op[i].be=-1;
				else op[i].be=trans(tmp1, strlen(tmp1));
				if (tmp2[0]=='n') op[i].en=-1;
				else op[i].en=trans(tmp2, strlen(tmp2));
				if (op[i].be>=0&&op[i].en==-1) op[i].c=1;
				else op[i].c=0;
				if (use[op[i].vary]){ ok=false; continue; }
				else use[op[i].vary]=1;
			}
			if (tmp[0]=='E'){
				if (!ok) continue;
				if (tail==0){ ok=false; continue;	}
				op[i].type=2;
				jump[stack[--tail]]=i;
				op[i].c=op[stack[tail]].c;
				use[op[stack[tail]].vary]=0;
			}
		}
		if (tail>0||!ok){ printf("ERR\n"); continue; }
		nowc=0;
		for (int i=1; i<=n; ++i){
			if (op[i].type==1){
				if (op[i].be>0&&op[i].en>0)
					if (op[i].be>op[i].en){
						i=jump[i]; continue; }
				if (op[i].be==-1)
					if (op[i].en>0){
						i=jump[i]; continue; }
				if (op[i].be>0&&op[i].en==-1) ++nowc;
				if (nowc>ans) ans=nowc;
			} else nowc-=op[i].c;
		}
		if (ans==c) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

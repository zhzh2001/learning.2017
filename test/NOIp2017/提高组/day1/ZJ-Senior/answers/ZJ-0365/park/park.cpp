#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;

int t,n,m,k,p,fst[100010],nxt[200010],w[200010][2],tot,ans,q[5000010];
int fs[100010],nx[200010],ww[200010][2],tt;
long long dis[100010],g[100010],lim;
bool inq[100010];

int rd()
{
	int ret=0;char ch=getchar();while(ch<'0'||ch>'9') ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();return ret;
}

void add(int u,int v,int c)
{
	w[++tot][0]=v;w[tot][1]=c;nxt[tot]=fst[u];fst[u]=tot;
}

void add2(int u,int v,int c)
{
	ww[++tt][0]=v;ww[tt][1]=c;nxt[tt]=fst[u];fst[u]=tt;
}

void spfa()
{
	int l=1,r=1;q[1]=1;memset(dis,63,sizeof(dis));dis[1]=0;memset(inq,0,sizeof(inq));
	while(l<=r)
		{
			int u=q[l++];inq[u]=0;
			for(int i=fst[u];i;i=nxt[i])
				{
					int v=w[i][0];
					if(v==n&&dis[v]==dis[u]+w[i][1]){ans++;if(ans==p) ans=0;}
					else if(dis[v]>dis[u]+w[i][1])
						{
							dis[v]=dis[u]+w[i][1];if(v==n) ans=1;
							if(!inq[v]) q[++r]=v,inq[v]=1;
						}
				}
		}
}

void getg()
{
	int l=1,r=1;q[1]=1;memset(g,63,sizeof(g));g[n]=0;memset(inq,0,sizeof(inq));
	while(l<=r)
		{
			int u=q[l++];inq[u]=0;
			for(int i=fs[u];i;i=nx[i])
				{
					int v=ww[i][0];
					if(g[v]>g[u]+ww[i][1])
						{
							g[v]=g[u]+ww[i][1];
							if(!inq[v]) q[++r]=v,inq[v]=1;
						}
				}
		}
}

void dfs(int u,long long d)
{
	if(u==n){ans++;if(ans==p) ans=0;return;}
	for(int i=fst[u];i;i=nxt[i])
		{
			int v=w[i][0];if(d+w[i][1]+g[v]>lim) continue;dfs(v,d+w[i][1]);
		}
}

int main()
{
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	t=rd();
	while(t--)
		{
			n=rd();m=rd();k=rd();p=rd();
			for(int i=1;i<=m;i++){int u=rd(),v=rd(),c=rd();add(u,v,c);add2(v,u,c);}
			spfa();if(k==0){printf("%d\n",ans);continue;}
			ans=0;lim=dis[n]+k;getg();dfs(1,0);printf("%d\n",ans);
		}
	return 0;
}

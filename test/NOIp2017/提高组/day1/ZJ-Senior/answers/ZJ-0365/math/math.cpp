#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;

long long a,b;

long long rd()
{
	long long ret=0;char ch=getchar();while(ch<'0'||ch>'9') ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();return ret;
}

int main()
{
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	a=rd();b=rd();if(a>b) swap(a,b);long long ans=b*(a-1)-a;printf("%lld",ans);
	return 0;
}

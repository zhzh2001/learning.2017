#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;

int t,ans,st[1010],tp,mx[1010];
char a[110],s[110],blm[110];
bool us[30];

int main()
{
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--)
		{
			int l;scanf("%d",&l);gets(a);ans=0;
			if(a[3]=='n')
				{
					int i=5;while(a[i]>='0'&&a[i]<='9') ans=ans*10+a[i++]-'0';
				}
			tp=0;memset(us,0,sizeof(us));int now=0;bool flag=0;mx[0]=-1;
			while(l--)
				{
					gets(s);
					if(s[0]=='F')
						{
							tp++;now++;mx[tp]=-1;
							if(us[s[2]-'a']) flag=1;else us[s[2]-'a']=1,blm[tp]=s[2];
							int i=4,x=0,y=0;
							if(s[i]=='n') x=-1,i++;else while(s[i]>='0'&&s[i]<='9') x=x*10+s[i++]-'0';
							i++;if(s[i]=='n') y=-1;else while(s[i]>='0'&&s[i]<='9') y=y*10+s[i++]-'0';
							if(x==-1){if(y==-1) st[tp]=0;else st[tp]=-1;}
							else if(y==-1) st[tp]=1;else if(x>y) st[tp]=-1;else st[tp]=0;
						}
					else
						{
							now--;if(now<0) flag=1;us[blm[tp]-'a']=0;
							if(st[tp]!=-1&&mx[tp]!=-1) st[tp]+=mx[tp];
							mx[tp-1]=max(mx[tp-1],st[tp]);tp--;
						}
				}
			if(flag||now!=0){printf("ERR\n");continue;}mx[0]=max(mx[0],0);
			if(mx[0]==ans) printf("Yes\n");else printf("No\n");
		}
	return 0;
}

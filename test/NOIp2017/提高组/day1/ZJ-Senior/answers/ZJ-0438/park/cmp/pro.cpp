#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<__LINE__<<endl
#define BEGIN "\n---------BEGIN---------\n"
#define  END  "\n----------END----------\n"

struct node {
	int t,v;
	node(){}
	node(int _t,int _v):t(_t),v(_v){}
	bool operator < (const node &t) const {
		return v>t.v;
	}
};

static const int MAXN=100005;
static const int MAXK=55;

int T;
int n,m,k,MOD;

void addp(int &x,int y) {
	x+=y;
	if (x>=MOD) x-=MOD;
}

void dijkstra(vector <node> *g,int s,int *dis) {
	priority_queue <node> que;
	fill(dis+1,dis+1+n,-1);
	dis[s]=0;
	que.push(node(s,0));
	while (!que.empty()) {
		node now=que.top();
		que.pop();
		int x=now.t,d=now.v;
		if (d!=dis[x]) continue;
		repo (i,0,g[x].size()) {
			int t=g[x][i].t,v=g[x][i].v;
			if (dis[t]==-1||dis[t]>v+d) {
				dis[t]=v+d;
				que.push(node(t,dis[t]));
			}
		}
	}
}

struct part1 {
	/*����*/
	static const int MAXN=25;
	int dis[MAXN];
	vector <node> g[MAXN];
	int res;
	void clear() {
		repo (i,0,MAXN) g[i].clear();
		res=0;
	}
	void dfs(int x,int d) {
		if (d-dis[x]>k) return;
		if (x==n) {
			addp(res,1);
		}
		repo (i,0,g[x].size()) {
			dfs(g[x][i].t,d+g[x][i].v);
		}
	}
	void main() {
		rep (i,1,m) {
			int x,y,v;
			scanf("%d%d%d",&x,&y,&v);
			g[x].push_back(node(y,v));
		}
		dijkstra(g,1,dis);
//		rep (i,1,n) cerr<<i<<" "<<debug(dis[i])<<endl;
		dfs(1,0);
		printf("%d\n",res);
	}
} part1;

struct Main {
	vector <node> g[MAXN],rg[MAXN];
	int dis[MAXN],rdis[MAXN];
	map <int,bool> to[MAXN];
	int dp[MAXN][MAXK];
	void clear() {
		repo (i,0,MAXN) g[i].clear(),rg[i].clear();
		repo (i,0,MAXN) to[i].clear();
		memset(dp,-1,sizeof(dp));
	}
	
	bool spj() {
		rep (x,1,n) {
			repo (i,0,g[x].size()) if (g[x][i].v==0) {
				int y=g[x][i].t;
				if (!to[y][x]) continue;
				if (dis[x]+rdis[y]==dis[n]) {
					puts("-1");
					return true;
				}
			}
		}
		return false;
	}
	
	int rec(int x,int d) {
		if (dp[x][d]!=-1) return dp[x][d];
		int ds=dis[x]+d;
		int &res=dp[x][d]=0;
		if (x==n) addp(res,1);
		repo (i,0,g[x].size()) {
			int t=g[x][i].t,v=g[x][i].v;
			if (ds+v-dis[t]<=k) addp(res,rec(t,ds+v-dis[t]));
		}
		return res;
	}
	
	void main() {
		clear();
		rep (i,1,m) {
			int x,y,v;
			scanf("%d%d%d",&x,&y,&v);
			g[x].push_back(node(y,v));
			rg[y].push_back(node(x,v));
			if (v==0) to[x][y]=true;
		}
		dijkstra(g,1,dis);
		dijkstra(rg,n,rdis);
		if (spj()) return;
		printf("%d\n",rec(1,0));
	}
} pmain;

void Read() {
	scanf("%d",&T);
}
void Solve() {
	rep (i,1,T) {
		scanf("%d%d%d%d",&n,&m,&k,&MOD);
//		part1.main();
		pmain.main();
	}
}

int main() {
//	freopen("park.in","r",stdin);
//	freopen("park.out","w",stdout);
	
	Read();
	Solve();
	
	{
		cerr<<BEGIN<<endl;
		int sum=0;
		int std=256;
		sum>>=20;
		cerr<<sum<<" MB"<<endl;
		cerr<<std<<" -std"<<endl;
		assert(sum<std);
	}
	
	return 0;
}


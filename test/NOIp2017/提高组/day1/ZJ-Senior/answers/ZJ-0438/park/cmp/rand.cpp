#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<__LINE__<<endl
#define BEGIN "\n---------BEGIN---------\n"
#define  END  "\n----------END----------\n"

int rnd(int l,int r) {
	int x=(rand()*rand()+rand())%(r-l+1);
	return x+l;
}

int main() {
	srand(time(NULL));
	int T=2;
	cout<<T<<endl;
	rep (i,1,T) {
		int n=rnd(10,20),m=rnd(n,100);
		int k=rnd(1,50),p=rnd(5,1000);
		cout<<n<<" "<<m<<" "<<k<<" "<<p<<endl;
		m-=n-1;
		rep (i,2,n) {
			int pre=rnd(1,i-1);
			cout<<pre<<" "<<i<<" "<<rnd(1,100)<<endl;
		}
		rep (i,1,m) {
			cout<<rnd(1,n)<<" "<<rnd(1,n)<<" "<<rnd(1,n)<<endl;
		}
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<__LINE__<<endl
#define BEGIN "\n---------BEGIN---------\n"
#define  END  "\n----------END----------\n"

const int MAXN=105;

int T;

struct Main {
	struct node {
		int i;
		int sta;//-1(l>r) , 0 (!n) 1(n)
	};
	node getnode(char i,char *x,char *y) {
		node now;
		now.i=i-'a';
		if (x[0]=='n') {
			if (y[0]=='n') now.sta=0;
			else now.sta=-1;
			return now;
		}
		else if (y[0]=='n') {
			now.sta=1;
			return now;
		}
		int a,b;
		sscanf(x,"%d",&a);
		sscanf(y,"%d",&b);
		if (a>b) now.sta=-1;
		else now.sta=0;
		return now;
	}
	
	int len;
	char com[20];
	int k;
	
	void getk() {
//		DEBUG(com+4);
		if (strlen(com)==4) k=0;
		else sscanf(com+4,"%d",&k);
	}
	
	node stk[MAXN];
	int tp;
	bool mark[50];
	
	void clear() {
		tp=0;
		memset(mark,0,sizeof(mark));
	}
	
	void main() {
//		cerr<<BEGIN<<endl;
		clear();
		scanf("%d%s",&len,com);
		getk();
		
		bool err=false;
		int res=0,dep=0;
		int zero=0;
		rep (i,1,len) {
			char tmp[50];
			scanf("%s",tmp);
			if (tmp[0]=='F') {
				char name[50],x[10],y[10];
				scanf("%s%s%s",name,x,y);
				node now=getnode(name[0],x,y);
//				cerr<<debug(now.sta)<<endl;
				
				if (mark[now.i]) err=true;
				mark[now.i]=true;
				stk[tp++]=now;
				
				if (now.sta==-1) zero++;
				if (!zero) {
					dep+=now.sta;
				}
			}
			else {
				if (!tp) {
					err=true;
					continue;
				}
				node now=stk[--tp];
				mark[now.i]=false;
				if (!zero) dep-=now.sta;
				if (now.sta==-1) zero--;
			}
			res=max(res,dep);
		}

		if (err||tp!=0) puts("ERR");
		else {
			if (res==k) puts("Yes");
			else puts("No");
		}
//		cerr<<END<<endl;
	}
} pmain;

void Read() {
	scanf("%d",&T);
}
void Solve() {
	rep (i,1,T) {
		pmain.main();
	}
}

int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	Read();
	Solve();
	
//	{
//		cerr<<BEGIN<<endl;
//		int sum=sizeof(Main);
//		int std=256;
//		sum>>=20;
//		cerr<<sum<<" MB"<<endl;
//		cerr<<std<<" -std"<<endl;
//		assert(sum<std);
//	}
	
	return 0;
}


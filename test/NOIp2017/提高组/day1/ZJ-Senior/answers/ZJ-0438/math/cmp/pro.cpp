#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<__LINE__<<endl
#define BEGIN "\n---------BEGIN---------\n"
#define  END  "\n----------END----------\n"


void exgcd(int a,int b,int &x,int &y) {
	if (!b) {
		x=1,y=0;
		return;
	}
	exgcd(b,a%b,x,y);
	int t=x;
	x=y;
	y=t-(a/b)*y;
}

int a,b;

struct Part1 {
	void main() {
		int res=-1;
		rep (i,1,a*b) {
			int x,y;
			exgcd(a,b,x,y);
			x*=i,y*=i;
			int k1=b,k2=a;
			
			if (x<0) swap(x,y),swap(k1,k2);
			int c=x/k1;
			x%=k1;
			y+=c*k2;
			
			if (y<0) res=i;
		}
		printf("%d\n",res);
	}
} part1;

struct Main {
	long long getres(int x,int y) {
		if (!y) return -1;
		long long add=1ll*(x-x%y)*(y-1);
		return getres(y,x%y)+add;
	}
	void main() {
		long long res=getres(a,b);
		cout<<res<<endl;
	}
} pmain;

void Read() {
	scanf("%d%d",&a,&b);
}
void Solve() {
//	part1.main();
	pmain.main();
}

int main() {
//	freopen("math.in","r",stdin);
//	freopen("math.out","w",stdout);
	
	Read();
	Solve();
	
	{
		cerr<<BEGIN<<endl;
		int sum=0;
		int std=256;
		sum>>=20;
		cerr<<sum<<" MB"<<endl;
		cerr<<std<<" -std"<<endl;
		assert(sum<std);
	}
	
	return 0;
}


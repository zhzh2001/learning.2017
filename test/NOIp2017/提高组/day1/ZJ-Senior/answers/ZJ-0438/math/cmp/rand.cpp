#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<__LINE__<<endl
#define BEGIN "\n---------BEGIN---------\n"
#define  END  "\n----------END----------\n"

int rnd(int l,int r) {
	int x=(rand()*rand()+rand())%(r-l+1);
	return x+l;
}

int gcd(int x,int y) {
	return !y?x:gcd(y,x%y);
}

int main() {
	srand(time(NULL));
	int x=rnd(1,100),y=rnd(1,100);
	rep (i,1,x) if (gcd(i,x)==1) y=i;
	cout<<x<<" "<<y<<endl;
	return 0;
}

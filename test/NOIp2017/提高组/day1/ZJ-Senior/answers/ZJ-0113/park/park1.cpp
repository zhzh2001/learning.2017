#include<cstdio>
#include<queue>
#include<vector>
#include<algorithm>
#include<cstring>
using namespace std;

struct Node{
	int u,d;
};
struct cmp{
	bool operator()(Node a,Node b){return a.d>b.d;}
};

priority_queue<Node,vector<Node>,cmp>Q;

int n,m,k,p,u,v,w;
int Head[100010],Next[200010],Tree[200010],Val[200010],tot;
int dis[100010];
bool vis[100010];
int f[2][100100][52];

bool dfs(int x,int start,bool f){
	vis[x]=true;
	if (f&&start==x) return true;
	for (int it=Head[x];it;it=Next[it]){
		int v=Tree[it];
		if (Val[it]==0&&dfs(v,start,true))return true;
	}
	return false;
}
void addedge(int u,int v,int w){
	tot++;
	Next[tot]=Head[u];
	Head[u]=tot;
	Tree[tot]=v;
	Val[tot]=w;
}

void dij(int s){
	memset(dis,0x3f3f3f3f,sizeof dis);
	dis[s]=0;
	Q.push((Node){s,0});
	while(!Q.empty()){
		Node u=Q.top();
		Q.pop();
		if (u.d>dis[u.u])continue;
		for (int it=Head[u.u];it;it=Next[it]){
			int v=Tree[it];
			if (dis[v]>dis[u.u]+Val[it]){
				dis[v]=dis[u.u]+Val[it];
				Q.push((Node){v,dis[v]});
			}
		}
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
	memset(Head,0,sizeof Head);
	memset(vis,0,sizeof vis);
	memset(f,0,sizeof f);
	tot=0;
	scanf("%d%d%d%d",&n,&m,&k,&p);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		addedge(u,v,w);
	}
	bool flag=false;
	for (int i=1;i<=n;i++)
		if (!vis[i])
			if (dfs(i,i,false)){
				flag=true;
				break;
			}
	if (flag){
		printf("-1\n");
		continue;
	}
	dij(1);
	//printf("%d\n",dis[n]);
	int now=1;
	f[now][1][0]=1;
	int ans=0;
	for(int i=1;i<=(k+10)*m;i++){
		//printf("%d\n",i);
		bool allzero=true;
		now^=1;
		for (int u=1;u<=n;u++){
			for(int it=Head[u];it;it=Next[it]){
				int v=Tree[it];
				for (int t=0;t<=k;t++){
					if (f[now^1][u][t]==0) continue;
					//printf("ZY:%d->%d t=%d\n",u,v,t);
					if (t+dis[u]+Val[it]-dis[v]>k)break;
					//printf("ZY:%d->%d t=%d\n",u,v,t);
					f[now][v][t+dis[u]+Val[it]-dis[v]]+=f[now^1][u][t];
					f[now][v][t+dis[u]+Val[it]-dis[v]]%=p;
					allzero=false;
				}
			}
			for (int t=0;t<=k;t++)
				f[now^1][u][t]=0;
		}
		bool flag=true;
		for (int t=0;t<=k;t++){
			ans+=f[now][n][t];
			ans%=p;
		}
		if (allzero)break;
	}
	
	printf("%d\n",ans);
}
}

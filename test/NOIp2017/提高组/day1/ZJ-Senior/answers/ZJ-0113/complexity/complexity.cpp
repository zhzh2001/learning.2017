#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

int L;
char Complex[100];
char cases[110][2];
struct BL{
	char name[2],s[5],e[5];
}bl[110];
bool used[30];
int XH,BG,S[300];
int dy[300];
char O1[10]="O(1)";

int getCS(){
	int len=strlen(Complex);
	int pos=0,res=0;
	while(Complex[pos]<'0'||Complex[pos]>'9')	pos++;
	while(Complex[pos]>='0'&&Complex[pos]<='9'){
		res=res*10+Complex[pos]-'0';
		pos++;
	}
	return res;
}

int getCS2(int l,int cases){
	if (cases==1){
		int len=strlen(bl[l].s);
		int pos=0,res=0;
		while(bl[l].s[pos]<'0'||bl[l].s[pos]>'9')	pos++;
		while(bl[l].s[pos]>='0'&&bl[l].s[pos]<='9'){
			res=res*10+bl[l].s[pos]-'0';
			pos++;
		}
		return res;
	}
	else{
		int len=strlen(bl[l].e);
		int pos=0,res=0;
		while(bl[l].e[pos]<'0'||bl[l].e[pos]>'9')	pos++;
		while(bl[l].e[pos]>='0'&&bl[l].e[pos]<='9'){
			res=res*10+bl[l].e[pos]-'0';
			pos++;
		}
		return res;
	}
}

int work(int l,int r){
	if (r<=l)return 0;
	int yg=0;
	int Sstart=0,Eend=0;
	if (dy[l]==r){
		yg=work(l+1,r-1);
		if (bl[l].s[0]=='n'){
			if (bl[l].e[0]=='n'){
				return yg;
			}else{
				return 0;
			}
		}
		Sstart=getCS2(l,1);
		if (bl[l].e[0]=='n')
			Eend=1000;
		else
			Eend=getCS2(l,2);
		if (Eend-Sstart>500)
			yg=yg+1;
		if (Eend<Sstart) return 0;
	}
	else
	{
		int pos=l;
		while(pos<r){
			yg=max(yg,work(pos,dy[pos]));
			pos=dy[pos];
			pos++;
		}
		
	}
	
	return yg;
}


int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
	bool flag=false;
	XH=0,BG=0;
	memset(used,0,sizeof used);
	memset(dy,0,sizeof dy);
	scanf("%d",&L);
	scanf("%s",&Complex);
	for (int i=1;i<=L;i++){
		scanf("%s",&cases[i]);
		if (cases[i][0]=='F')
			scanf("%s %s %s",&bl[i].name,&bl[i].s,&bl[i].e);
	}
	
	for (int i=1;i<=L;i++){
		if (cases[i][0]=='F'){
			XH++;
			BG++;
			S[BG]=i;
			if (!used[bl[i].name[0]-'a']){
				used[bl[i].name[0]-'a']=true;
			}else{
				flag=true;
				break;
			}
		}else{
			if (BG==0){
				flag=true;
				break;
			}
			dy[S[BG]]=i;
			used[bl[S[BG]].name[0]-'a']=false;
			BG--;
		}
	}
	
	if (flag||(BG>0)) {
		printf("ERR\n");
		continue;
	}
	int tmp=work(1,L);
	//printf("C:%d\n",tmp);
	if (tmp==0){
		//printf("get:%s true:%s\n",Complex,O1);
		if (strcmp(Complex,O1)==0)
			printf("Yes\n");
		else
			printf("No\n");
	}else{
		if (strcmp(Complex,O1)==0){
				printf("No\n");
		}else{
			if (tmp==getCS())
				printf("Yes\n");
			else
				printf("No\n");
		}
	}
	}
}
		

#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;
const int N=1e5+7,M=2e5+7,inf=1e9+7;
int T,n,m,head[N],nex[M],to[M],v[M],P,K,tot,d[N],Min,ans;
bool vis[N];
inline int read(){
	int ff=0;
	char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9'){
		ff=ff*10+ch-'0';
		ch=getchar();
	}
	return ff;
}
inline void add(int x,int y,int z){
	to[++tot]=y;
	v[tot]=z;
	nex[tot]=head[x];
	head[x]=tot;
}
void spfa(){
	queue<int> q;
	for(int i=1;i<=n;++i) vis[i]=false,d[i]=inf;
	d[1]=0;q.push(1);vis[1]=true;
	while(!q.empty()){
		int x=q.front();q.pop();
		for(int i=head[x];i;i=nex[i]){
			if(d[to[i]]>d[x]+v[i]){
				d[to[i]]=d[x]+v[i];
				if(!vis[to[i]])
					q.push(to[i]),vis[to[i]]=true;
			}
		}
	}
	Min=d[n];
}
void dfs(int x,int r){
	if(r>Min+K) return;
	if(x==n){
		if(r<=Min+K) ans=(ans+1)%P;
		return;
	}
	for(int i=head[x];i;i=nex[i]){
		if(!vis[to[i]])
			vis[to[i]]=true,dfs(to[i],r+v[i]),vis[to[i]]=false;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--){
		ans=0;tot=0;Min=inf;
		memset(head,0,sizeof head);
		memset(nex,0,sizeof nex);
		n=read();m=read();K=read();P=read();
		int x,y,z;
		for(int i=1;i<=m;++i){
			x=read();y=read();z=read();
			add(x,y,z);
		}
		spfa();
		for(int i=1;i<=n;++i) vis[i]=false;
		dfs(1,0);
		cout<<ans<<'\n';
		continue;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
const int inf=1e9+7;
int T,n,Num,S[101],top,from[101],to[101];
char O[10001],dim[101],Do[101];
bool isn,ism,use[100],b[101][2],nn;
void worko(){
	isn=false,ism=false;Num=0;
	for(int i=0;i<strlen(O);++i){
		if(O[i]=='^') ism=true;
		if(O[i]>='0'&&O[i]<='9') Num=Num*10+O[i]-'0';
	}
}
inline int Max(int x,int y){
	return x>y? x:y;
}
inline int Min(int x,int y){
	return x<y? x:y;
}
void init(){
	char s[10001];
	for(int i=1;i<=n;++i){
		scanf("%s",s);
		Do[i]=s[0];
		if(s[0]=='E') continue;
		scanf("%s",s);
		dim[i]=s[0];
		scanf("%s",s);
		if(s[0]>='0'&&s[0]<='9'){
			int j=0;
			while(s[j]>='0'&&s[j]<='9')
				from[i]=from[i]*10+s[j]-'0',++j;
		}
		else b[i][0]=true;
		scanf("%s",s);
		if(s[0]>='0'&&s[0]<='9'){
			int j=0;
			while(s[j]>='0'&&s[j]<='9')
				to[i]=to[i]*10+s[j]-'0',++j;
		}
		else b[i][1]=true;
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);scanf("%s",O);
		memset(use,false,sizeof use);
		nn=false;
		memset(from,0,sizeof from);
		memset(to,0,sizeof to);
		memset(b,false,sizeof b);
		worko();
		init();
		top=0;
		int ans=0;
		bool err=false;
		for(int i=1;i<=n;++i){
			if(Do[i]=='E'){
				int x=S[top];
				use[dim[x]]=false;
				--top;
			}
			else{
				S[++top]=i;
				use[dim[i]]=true;
				int mi=0;
				for(int j=1;j<=top;++j){
					int x=S[j];
					if(!b[x][0]&&!b[x][1])
						if(from[x]>to[x])
							break;
					mi+=b[x][1];
				}
				ans=Max(ans,mi);
				if(mi) nn=true;
			}
		}
		if(nn&&ism&&Num==ans || !ism&&!nn) printf("Yes\n");
		else printf("No\n");  
	}
	return 0;
}

#include<cstdio>
#define ll long long
ll a,b;
void exgcd(ll a,ll b,ll &x,ll &y){
	if (b==0){
		x=1; y=0; return;
	}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}
ll work(ll a,ll b,ll x,ll y){
	if (x>0){
		ll o=x/b+1;
		x-=o*b; y+=o*a;
	}
	return (-x-1)*a;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	ll x,y;
	exgcd(a,b,x,y);
	ll ans=1;
	ans+=work(a,b,x,y);
	ans+=work(b,a,y,x);
	printf("%lld\n",ans);
	return 0;
}

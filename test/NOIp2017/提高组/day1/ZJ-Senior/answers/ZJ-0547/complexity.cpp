#include<cstdio>
#include<cstring>
void rd(int &x){x=0;int c=getchar();while (c<'0'||c>'9')c=getchar();while (c>='0'&&c<='9')x=x*10+c-'0',c=getchar();}
void rdx(int &x){x=0;int c=getchar();while ((c<'0'||c>'9')&&c!='n')c=getchar();if (c=='n'){x=0;return;}while (c>='0'&&c<='9')x=x*10+c-'0',c=getchar();}
void rdy(int &x){x=0;int c=getchar();while ((c<'0'||c>'9')&&c!='n')c=getchar();if (c=='1')x=0;else rd(x);}
void getc(char &x){x=getchar();while (x!='F'&&x!='E')x=getchar();}
void getch(char &x){x=getchar();while (x<'a'||x>'z')x=getchar();}
int max(int x,int y){return x>y?x:y;}
int T,n,o,top,num[1005],op[1005],vis[256],pp[1005];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for (int numm=1;numm<=T;numm++){
		rd(n); rdy(o); top=0;
		bool err=0; pp[0]=num[0]=0;
		memset(vis,0,sizeof(vis));
		for (int i=1;i<=n;i++){
			char ch; getc(ch);
			if (ch=='F'){
				char u; getch(u);
				int x,y; rdx(x); rdx(y);
				if (vis[(int)u])err=1;
				if (err)continue;
				vis[(int)u]++; op[++top]=u;
				if (x==0&&y)num[top]=-1;
				if (x&&y==0)num[top]=1;
				if (x&&y)if (x<=y)num[top]=0;else num[top]=-1;else;
				if (x==0&&y==0)num[top]=0;
				pp[top]=num[top];
			}else{
				if (top==0)err=1;
				if (err)continue;
				vis[op[top]]--;
				if (num[top-1]==-1){top--;continue;}
				if (pp[top]!=-1)pp[top-1]=max(pp[top-1],num[top-1]+pp[top]);
				top--;
			}
		} if (pp[0]==-1)pp[0]=0; if (top)err=1;
		if (err)puts("ERR");else puts(pp[0]==o?"Yes":"No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#define N 200005
int n,m,k,p,hd,tl,q[10000005],f[100005][52],mn[N],ans,D; bool vis[N]; long long U;
void rd(int &x){x=0;int c=getchar();while (c<'0'||c>'9')c=getchar();while (c>='0'&&c<='9')x=x*10+c-'0',c=getchar();}
struct edge{
	int nxt[N],lnk[N],son[N],w[N],tot,dis[N];
	void add(int x,int y,int z){
		nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;w[tot]=z;
	}
	void spfa(int S){
		memset(vis,0,sizeof(vis));
		memset(dis,63,sizeof(dis));
		dis[S]=0; vis[S]=1;
		hd=tl=0; q[++tl]=S;
		while (hd!=tl){
			hd++; if (hd==10000000)hd=0; int x=q[hd]; vis[x]=0;
			for (int i=lnk[x];i;i=nxt[i])if (dis[son[i]]>dis[x]+w[i]){
				dis[son[i]]=dis[x]+w[i];
				if (!vis[son[i]]){
					vis[son[i]]=1; ++tl; if (tl==10000000)tl=0; q[tl]=son[i];
					if (dis[q[tl]]<dis[q[hd+1]]){
						int t=q[tl]; q[tl]=q[hd+1]; q[hd+1]=t;
					}
				}
			}
		}
	}
}a,b;
void add(int &x,const int y){if (x+y>=p)x+=y-p;else x+=y;}
int max(int x,int y){return x>y?x:y;}
void spfa(){
	memset(f,0,sizeof(f));
	memset(vis,0,sizeof(vis));
	memset(mn,63,sizeof(mn));
	f[1][0]=1%p; vis[1]=1; mn[1]=0;
	hd=tl=0; q[++tl]=1; int cnt=0;
	while (hd!=tl){
		hd++; if (hd==10000000)hd=0; int x=q[hd]; vis[x]=0; if (++cnt>=U){ans=-1;return;}
		for (int i=a.lnk[x];i;i=a.nxt[i])if (mn[x]+a.w[i]+b.dis[a.son[i]]<=D+k&&mn[x]+a.w[i]-a.dis[a.son[i]]<=k){
			if (mn[a.son[i]]>mn[x]+a.w[i])mn[a.son[i]]=mn[x]+a.w[i];
			for (int j=mn[x]+a.w[i]-a.dis[a.son[i]],t=mn[x]-a.dis[x];j<=k;++j,++t)add(f[a.son[i]][j],f[x][t]);
			if (!vis[a.son[i]]){
				vis[a.son[i]]=1; ++tl; if (tl==10000000)tl=0; q[tl]=a.son[i];
				if (mn[q[tl]]<mn[q[hd+1]]){
					int t=q[tl]; q[tl]=q[hd+1]; q[hd+1]=t;
				}
			}
		}
		if (x==n)for (int i=0;i<=k;i++)add(ans,f[x][i]);
		memset(f[x],0,sizeof(f[x])); mn[x]=mn[0];
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		memset(a.lnk,0,sizeof(a.lnk));
		memset(b.lnk,0,sizeof(b.lnk));
		a.tot=b.tot=0; ans=0;
		rd(n), rd(m), rd(k), rd(p);
		U=1LL*max(n,m)*max(n,m)*4;
		if (U>=1200000)U=1200000;
		for (int i=1;i<=m;i++){
			int x,y,z; rd(x),rd(y),rd(z);
			a.add(x,y,z); b.add(y,x,z);
		} a.spfa(1); b.spfa(n); D=a.dis[n];
		spfa(); printf("%d\n",ans);
	}
	return 0;
}

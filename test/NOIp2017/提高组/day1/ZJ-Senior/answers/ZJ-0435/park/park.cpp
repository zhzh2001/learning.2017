#include<queue>
#include<cstdio>
#include<cstring>
#define N 100005
#define M 200005
using namespace std;
struct P{int x,y;int operator<(const P&t)const{return y>t.y;}}t;
priority_queue<P>Q;
int dis[N],way[N],fir[N],hea[N],arr[M<<1],nex[M<<1],vis[N],val[M<<1],pas[N];
int T,n,m,k,p,i,a,b,c,e,res,flag;
void dfs(int now,int d)
{
	if(flag) return ;
	if(vis[now])
	{
		if(pas[now]==d)
		{
			flag=1;
			return;
		}
	}else
	{
		vis[now]=1;
		pas[now]=d;
	}
	if(now==n)
	{
		++res;
		if(res>=p) res-=p;
	}
	for(int i=fir[now];i;i=nex[i])
		if(d+val[i]+way[arr[i]]<=way[1]+k)
			dfs(arr[i],d+val[i]);
	vis[now]=0;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	for(int cas=1;cas<=T;++cas)
	{
		e=0;res=0;flag=0;
		memset(hea,0,sizeof hea);
		memset(fir,0,sizeof fir);
		memset(pas,-1,sizeof pas);
		memset(way,63,sizeof way);
		memset(dis,63,sizeof dis);
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(i=0;i<m;++i)
		{
			scanf("%d%d%d",&a,&b,&c);
			arr[++e]=b;nex[e]=fir[a];fir[a]=e;val[e]=c;
			arr[++e]=a;nex[e]=hea[b];hea[b]=e;val[e]=c;
		}memset(vis,0,sizeof vis);
		Q.push((P){n,way[n]=0});
		for(;!Q.empty();)
		{
			t=Q.top();Q.pop();
			if(vis[t.x])continue;
			vis[t.x]=1;
			for(i=hea[t.x];i;i=nex[i])
				if(way[arr[i]]>way[t.x]+val[i])
					Q.push((P){arr[i],way[arr[i]]=way[t.x]+val[i]});
		}memset(vis,0,sizeof vis);
		dfs(1,0);
		printf("%d\n",flag?-1:res);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

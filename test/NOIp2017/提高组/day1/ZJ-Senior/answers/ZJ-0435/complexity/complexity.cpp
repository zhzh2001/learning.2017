#include<cstdio>
#include<iostream>
#include<map>
#include<cstring>
#include<string>
#include<algorithm>
using namespace std;
int n,T,i,j,p,flag;
int now,wan,maxn,top,l,r;
char s[10000],k;
string tmp,ll,rr;
struct P{string x;int k;}stc[10000];
int slove()
{
	scanf("%d O(%s)",&n,s);
	now=0;maxn=0;wan=0;top=0;flag=0;l=strlen(s);p=0;
	if(s[0]=='n')
		for(i=0;i<l;++i)
			if(s[i]>47&&s[i]<58)
				wan=wan*10+s[i]-48;
	for(i=0;i<n;++i)
	{
		cin>>k;
		if(k=='F')
		{
			cin>>tmp>>ll>>rr;
			if(flag) continue;
			for(j=1;j<=top;++j)
				if(stc[j].x==tmp)
					flag=1;
			if(ll=="n"&&rr!="n")
			{
				++p;
				stc[++top]=(P){tmp,-1};
				--now;
			}
			if(ll=="n"&&rr=="n")
				stc[++top]=(P){tmp,0};
			if(ll!="n"&&rr=="n")
				stc[++top]=(P){tmp,1},++now;
			if(ll!="n"&&rr!="n")
			{
				l=0;r=0;
				for(j=0;j<ll.size();++j)
					l=l*10+ll[j]-48;
				for(j=0;j<rr.size();++j)
					r=r*10+rr[j]-48;
				if(l>r)
				{
					
					++p;
					stc[++top]=(P){tmp,-1};
					--now;
				}
				else
					stc[++top]=(P){tmp,0};
			}
			if(!p) maxn=max(maxn,now);
		}
		else
		{
			if(flag) continue;
			if(stc[top].k==-1) --p;
			now-=stc[top].k;
			--top;
			if(top<0) flag=1;
		}
	}
	if(top||flag) return 0*puts("ERR");
	if(maxn==wan) return 0*puts("Yes");
	else return 0*puts("No");
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	for(int cas=1;cas<=T;++cas)
		slove();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

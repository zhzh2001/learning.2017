var
  book:array[1..5000]of boolean;
  dis:array[1..5000]of longint;
  e:array[1..5000,1..5000]of longint;
  f:array[1..5000,0..50]of longint;
  x,y,z,i,j,n,m,k,p,t,u,v,min,w,ans:longint;
begin
  assign(input,'park.in');reset(input);
  assign(output,'park.out');rewrite(output);
  readln(t);
  while t>0 do
   begin
     dec(t);
     readln(n,m,k,p);
     for i:=1 to n do
       for j:=1 to n do
         if i<>j then e[i,j]:=1023456789;
     for i:=1 to m do
      begin
        read(x,y,z);
        e[x,y]:=z;
      end;
     fillchar(book,sizeof(book),true);
     fillchar(f,sizeof(f),0);
     book[1]:=false;
     for i:=1 to n do
       dis[i]:=e[1,i];
     for i:=1 to n-1 do
      begin
        min:=maxlongint;
        u:=0;
        for j:=1 to n do
          if book[j] and (dis[j]<min)then
           begin
             min:=dis[j];
             u:=j;
           end;
        if u=0 then break;
        book[u]:=false;
        for v:=1 to n do
          if book[v] and (dis[u]+e[u,v]<dis[v]) then dis[v]:=dis[u]+e[u,v];
      end;
     fillchar(book,sizeof(book),true);
     f[1][0]:=1;
     for i:=1 to n do
      begin
        min:=maxlongint;
        u:=0;
        for j:=1 to n do
          if book[j] and (dis[j]<min) then
           begin
             min:=dis[j];
             u:=j;
           end;
        if u=0 then break;
        book[u]:=false;
        for v:=1 to n do
          if book[v] and (dis[u]+e[u,v]<=dis[v]+k) then
           begin
             w:=dis[u]+e[u,v]-dis[v];
             for j:=0 to k do
              begin
                if w+j>k then break;
                f[v][w+j]:=(f[v][w+j]+f[u][j]) mod p;
              end;
           end;
      end;
     ans:=0;
     for i:=0 to 50 do
       ans:=ans+f[n][i];
     writeln(ans);
   end;
  close(input);close(output);
end.

var
  t,i,top,n,p,o,ans,crun,x,y,w:longint;
  a,c:array[0..200]of longint;
  b:array[0..200]of char;
  table:array['a'..'z']of longint;
  err:boolean;
  s,s1:string;
  ch:char;

function oc(s:string):longint;
begin
  if s[1]='n' then exit(1000);
  val(s,oc);
end;

begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(t);
  while t>0 do
   begin
     dec(t);
     readln(s1);
     p:=pos(' ',s1);
     val(copy(s1,1,p-1),n);
     delete(s1,1,p+2);
     if s1[1]='1' then w:=0
                  else
      begin
        delete(s1,1,2);
        p:=pos(')',s1);
        val(copy(s1,1,p-1),w);
      end;
     top:=0;
     o:=0;
     ans:=0;
     fillchar(table,sizeof(table),0);
     err:=false;
     crun:=0;
     for i:=1 to n do
      begin
        readln(s);
        if err then continue;
        if s[1]='F' then
         begin
           ch:=s[3];
           if table[ch]<>0 then
            begin
              err:=true;
              continue;
            end;
           table[ch]:=1;
           inc(top);
           b[top]:=ch;
           delete(s,1,4);
           p:=pos(' ',s);
           s1:=copy(s,1,p-1);
           delete(s,1,p);
           x:=oc(s1);
           y:=oc(s);
           if (x=1000)and(y=1000) then a[top]:=0
           else if (x<>1000)and(y=1000) then a[top]:=1
           else if (x>y) then
            begin
              inc(crun);
              c[top]:=1;
            end
           else if (x<=y) then a[top]:=0;
           if crun=0 then o:=o+a[top];
           if o>ans then ans:=o;
           continue;
         end;
        if s[1]='E' then
         begin
           if top=0 then
            begin
              err:=true;
              continue;
            end;
           dec(o,a[top]);
           table[b[top]]:=0;
           dec(crun,c[top]);
           dec(top);
         end;
      end;
     if top<>0 then err:=true;
     if err then writeln('ERR')
     else if w<>ans then writeln('No')
     else writeln('Yes');
   end;
  close(input);close(output);
end.
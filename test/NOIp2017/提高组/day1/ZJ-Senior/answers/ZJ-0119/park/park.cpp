#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
#include<set>
#include<map>

#define REP(i,l,r) for (int i=(l);i<=(r);++i)
#define rep(i,l,r) for (int i=(l);i<(r);++i)
#define foredge(i,u) for (int i=la[u];i;i=ne[i])
#define fr(a) freopen(a,"r",stdin)
#define fw(a) freopen(a,"w",stdout)
#define dbg(a) cout<<#a<<" = "<<a<<endl;
#define tpn typename
#define mem(a) memset(a,0,sizeof(a))

using namespace std;

typedef long long ll;
inline void read(int &x){
	char c;
	do{
		c=getchar();
	}while (c<'0'||c>'9');
	x=0;
	do{
		x=x*10+c-48;
		c=getchar();
	}while (c>='0'&&c<='9');
}
inline void read(int &x,int &y){
	read(x),read(y);
}
inline void read(int &x,int &y,int &z){
	read(x),read(y),read(z);
}
inline void read(int &x,int &y,int &z,int &d){
	read(x),read(y),read(z),read(d);
}
const int maxn=100001,maxm=400001;
int la[maxn],en[maxm],ne[maxm],w[maxm],top;
int T,n,m,k,p,x,xx,y,yy,z;
ll dist[maxn][55];
int f[maxn][55];
bool b[maxn],caled[maxn][55],finished[maxn][55];
queue <int> q;
inline void add(int x,int y,int z){
	ne[++top]=la[x];
	en[top]=y;
	w[top]=z;
	la[x]=top;
}
int sp,ep;
inline int dfs(int x,int xx){
	if (caled[x][xx]){
		if (!finished[x][xx]) return -1;
		else return f[x][xx];
	}
	caled[x][xx]=1;
	int v,l,r,mid,ep,ans,tmp;
	ans=tmp=0;
	foredge(i,x){
		if (i&1) continue;
		v=en[i];
		l=0,r=k,ep=k+1;
		while (l<=r){
			mid=l+r>>1;
			if (dist[v][mid]+w[i]>=dist[x][xx]){
				ep=mid;
				r=mid-1;
			}else l=mid+1;
		}
		if (ep<=k&&dist[v][ep]+w[i]==dist[x][xx]){
			if ((tmp=dfs(v,ep))!=-1) ans=(ans+tmp)%p;
			else{
				f[x][xx]=-1;
				return -1;
			}
		}
	}
	if (x==1) ans=1;
	f[x][xx]=ans;
	finished[x][xx]=1;
	return ans;
}
int ans,tmp;
int main(){
	fr("park.in");
	fw("park.out");
	read(T);
	while (T--){
		mem(la);
		mem(b);
		memset(dist,0x3f,sizeof(dist));
		top=0;
		read(n,m,k,p);
		REP(i,1,m){
			read(x,y,z);
			add(x,y,z);
			add(y,x,z);
		}
		q.push(1);
		b[1]=1;
		REP(i,0,51) dist[1][i]=0;
		while (!q.empty()){
			x=q.front();
			q.pop();
			b[x]=0;
			foredge(i,x){
				if (!(i&1)) continue;
				y=en[i];
				ep=0;
				for (sp=0;sp<=51;++sp)
				  if (!(sp>=1&&dist[x][sp]==dist[x][sp-1]))
				  for (;ep<=51;++ep) if (dist[x][sp]+w[i]<dist[y][ep]){
				  	dist[y][ep]=dist[x][sp]+w[i];
				  	if (!b[y]){
				  		q.push(y);
				  		b[y]=1;
					}
					break;
				  }
				  else if (dist[x][sp]+w[i]==dist[y][ep]) break;
			}
		}
		for (sp=1;sp<=51&&dist[n][sp]<=dist[n][0]+k;++sp);
		k=sp-1;
		mem(f);
		mem(caled);
		mem(finished);
//		REP(i,1,n)
//		  REP(j,0,k){
//		  	printf("%d ",dist[i][j]);
//		  	if (j==k) putchar('\n');
//		  }
		REP(i,0,k){
			if (i&&dist[n][i]==dist[n][i-1]) continue;
			if ((tmp=dfs(n,i))!=-1) ans=(ans+tmp)%p;
			else{
				printf("-1\n");
				goto end;
			}
		}
		printf("%d\n",ans);
		end:;
		ans=0;
	}
}

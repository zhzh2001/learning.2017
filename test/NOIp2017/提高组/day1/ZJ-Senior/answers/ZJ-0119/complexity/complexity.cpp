#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
#include<set>
#include<map>

#define REP(i,l,r) for (int i=(l);i<=(r);++i)
#define rep(i,l,r) for (int i=(l);i<(r);++i)
#define fr(a) freopen(a,"r",stdin)
#define fw(a) freopen(a,"w",stdout)
#define dbg(a) cout<<#a<<" = "<<a<<endl;
#define tpn typename
#define mem(a) memset(a,0,sizeof(a))

using namespace std;

typedef long long ll;
int T,l;
char fzd[10],yj[5];
int top,cuurr,maxi,yfzd,x,y;
bool b[101],added[101],flag;
char chars[101];
set <char> cs;
int main(){
	fr("complexity.in");
	fw("complexity.out");
	scanf("%d",&T);
	while (T--){
		scanf("%d O(%s",&l,fzd);
		if (fzd[0]=='n') sscanf(fzd,"n^%d)",&yfzd);
		else yfzd=0;
		maxi=cuurr=0;
		cs.clear();
		top=0;
		b[0]=1;
		flag=1;
		REP(i,1,l){
			scanf("%s",yj);
			if (yj[0]=='F'){
				++top;
				scanf("%s",yj);
				if (cs.count(yj[0])){
					flag=0;
				}else{
					chars[top]=yj[0];
					cs.insert(yj[0]);
				}
				scanf("%s",yj);
				if (yj[0]=='n') x=10000;
				else sscanf(yj,"%d",&x);
				scanf("%s",yj);
				if (yj[0]=='n') y=10000;
				else sscanf(yj,"%d",&y);
				
				b[top]=b[top-1]&&x<=y;
				added[top]=b[top]&&(x<10000&&y==10000);
				cuurr+=added[top];
				if (cuurr>maxi) maxi=cuurr;
			}else{
				cuurr-=added[top];
				if (cs.count(chars[top])) cs.erase(chars[top]);
				--top;
			}
			if (top<0){
				flag=0;
				top=0;
			}
		}
		if (top||!flag) printf("ERR\n");
		else if (maxi==yfzd) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
#include<set>
#include<map>

#define REP(i,l,r) for (int i=(l);i<=(r);++i)
#define rep(i,l,r) for (int i=(l);i<(r);++i)
#define fr(a) freopen(a,"r",stdin)
#define fw(a) freopen(a,"w",stdout)
#define dbg(a) cout<<#a<<" = "<<a<<endl;
#define tpn typename

using namespace std;

typedef long long ll;
ll a,b,ans;
int main(){
	fr("math.in");
	fw("math.out");
	scanf("%lld %lld",&a,&b);
	printf("%lld",a*b-a-b);
	return 0;
}

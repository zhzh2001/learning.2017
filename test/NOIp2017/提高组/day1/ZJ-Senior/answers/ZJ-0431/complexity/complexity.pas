var
  n,m,i,j,s,p,l,t,x,y,max:longint;
  a:array[1..26]of char;
  b:array['a'..'z']of boolean;
  st,pan,wo:string;
  que:boolean;
  ch:char;
begin
  assign(input,'complexity.in');reset(input);
  assign(output,'complexity.out');rewrite(output);
  readln(n);
  for i:=1 to n do
    begin
      readln(st);
      b['n']:=true;
      t:=1;
      m:=0;
      while (st[t]>='0') and (st[t]<='9') do
      begin
        m:=m*10+ord(st[t])-ord('0');
        t:=t+1;
      end;
      s:=0;
      p:=0;
      que:=false;
      l:=0;
      max:=0;
      pan:='';
      wo:='';
      for j:=t+1 to length(st) do pan:=pan+st[j];
      if m mod 2=0 then
      begin
      for j:=1 to m do
        begin
          readln(st);
          if (st[1]='F') and (not que) and (l=0) then
          begin
            t:=5;
            x:=0;
            if b[st[3]] then
            begin
              que:=true;
              continue;
            end;
            p:=p+1;
            a[p]:=st[3];
            b[st[3]]:=true;
            if (st[t]='n') and (st[t+2]='n') then continue;
            if (st[t]='n') and (st[t+2]<>'n') then
            begin
              l:=l+1;
              continue;
            end;
            while (st[t]>='0') and (st[t]<='9') do
            begin
              x:=x*10+ord(st[t])-ord('0');
              t:=t+1;
            end;
            if st[t+1]='n' then
            begin
              s:=s+1;
              continue;
            end;
            y:=0;
            t:=t+1;
            while (st[t]>='0') and (st[t]<='9') do
            begin
              y:=y*10+ord(st[t])-ord('0');
              t:=t+1;
            end;
            if x>y then
            begin
              l:=l+1;
              continue;
            end;
          end
          else
          if (not que) and (l=0) then
          begin
            b[a[p]]:=false;
            p:=p-1;
            if s>max then max:=s;
            s:=s-1;
          end
          else
          if l>0 then
          begin
            if st[1]='F' then
            begin
              l:=l+1;
              p:=p+1;
            end
            else
            begin
              l:=l-1;
              p:=p-1;
            end;
          end;
        end;
      end;
      for ch:='a' to 'z' do b[ch]:=false;
      if m mod 2<>0 then
      begin
        for j:=1 to m do readln(st);
        writeln('ERR');
        continue;
      end;
      if p>0 then
      begin
        writeln('ERR');
        continue;
      end;
      if que then
      begin
        writeln('ERR');
        continue;
      end;
      if max=0 then wo:='O(1)'
        else wo:='O(n^'+chr(max+ord('0'))+')';
      if pan=wo then writeln('Yes') else writeln('No');
    end;
  close(input);close(output);
end.
#include<bits/stdc++.h>
using namespace std;
int T,n,stjs[1000];
char st[1000],c,cc,s[1000];
bool b[1000];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	int k;
	while (T--)
	{
		scanf("%d",&n);
		scanf("%s",&s);
		k=0;
		if (s[2]=='1')
			k=0;
		else
			for (int i=4;i<=strlen(s)-2;i++)
				k=k*10+s[i]-48;
		memset(b,0,sizeof(b));
		memset(st,0,sizeof(st));
		memset(stjs,0,sizeof(stjs));
		int r=0,maxx=0,js=0,b1,b2;
		bool errb=0;
		for (int i=1;i<=n;i++)
		{
			scanf("%c",&c);
			while (c!='F'&&c!='E')
				scanf("%c",&c);
			if (c=='F')
			{
				scanf("%c",&cc);
				while (cc==' ')
					scanf("%c",&cc);
				if (b[cc])
					errb=1;
				b[cc]=1;
				r++;
				st[r]=cc;
				b1=0,b2=0;
				scanf("%c",&cc);
				while (cc==' ')
					scanf("%c",&cc);
				if (cc>='0'&&cc<='9')
					while (cc>='0'&&cc<='9')
					{
						b1=b1*10+cc-48;
						scanf("%c",&cc);
					}
				scanf("%c",&cc);
				while (cc==' ')
					scanf("%c",&cc);
				if (cc>='0'&&cc<='9')
					while (cc>='0'&&cc<='9')
					{
						b2=b2*10+cc-48;
						scanf("%c",&cc);
					}
				if (b1>0&&b2==0)
					stjs[r]=1;
				else
				if (b1==0&&b2>0)
					stjs[r]=-1000;
				else
				if (b1>0&&b2>0&&b1>b2)
					stjs[r]=-1000;
				else
					stjs[r]=0;
				js+=stjs[r];
				if (js>maxx)
					maxx=js;
			}
			else
			{
				js-=stjs[r];
				b[st[r]]=0;
				r--;
				if (r<0)
				{
					errb=1;
					r=0;
				}
			}
		}
		if (r>0)
			errb=1;
		if (errb)
			printf("ERR\n");
		else
			if (maxx==k)
				printf("Yes\n");
			else
				printf("No\n");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int T,n,m,k,p,g[100005],f[10000005][2],tot,nxt[200005],to[200005],head[200005],zhi[200005],js[100005][55];
bool b[100005];
void add(int a,int b,int c)
{
	tot++;
	nxt[tot]=head[a];
	to[tot]=b;
	zhi[tot]=c;
	head[a]=tot;
}
int main()
{

	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&k,&p);
		tot=0;
		int x,y,z;
		memset(nxt,0,sizeof(nxt));
		memset(zhi,0,sizeof(zhi));
		memset(head,0,sizeof(head));
		memset(to,0,sizeof(to));
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		memset(g,125,sizeof(g));
		f[1][0]=1;
		f[1][1]=0;
		g[1]=0;
		memset(js,0,sizeof(js));
		int l=1,r=1;
		js[1][0]=1;
		while (l<=r)
		{
			for (int i=head[f[l][0]];i!=0;i=nxt[i])
				if (f[l][1]+zhi[i]<g[to[i]])
				{
					r++;
					f[r][0]=to[i];
					f[r][1]=f[l][1]+zhi[i];
					g[f[r][0]]=f[l][1]+zhi[i];
//					js[f[r][0]][0]=js[f[l][0]][0];
				}
//				else
//				if(f[l][1]+zhi[i]==g[to[i]])
//					js[to[i]][0]+=js[f[l][0]][0];
			l++;
		}
		int minn=g[n];
		for (int i=0;i<=k;i++)
		{
			l=1,r=1;
			f[1][0]=1;
			f[1][1]=i;
			memset(b,1,sizeof(b));
			b[1]=0;
			while (l<=r)
			{
				for (int j=head[f[l][0]];j!=0;j=nxt[j])
					if(f[l][1]+zhi[j]==g[to[j]]+i)
					{
						js[to[j]][i]+=js[f[l][0]][i];
						if (b[to[j]])
						{
							r++;
							f[r][0]=to[j];
							f[r][1]=f[l][1]+zhi[j];
							b[to[j]]=0;
						}
					}
				l++;
			}
			for (int j=1;j<=n;j++)
				for (int q=head[j];q!=0;q=nxt[q])
					if (i+g[j]+zhi[q]-g[to[q]]<=k&&i+g[j]+zhi[q]-g[to[q]]!=i)
						js[to[q]][i+g[j]+zhi[q]-g[to[q]]]=(js[to[q]][i+g[j]+zhi[q]-g[to[q]]]+js[j][i])%p;
		}
//		for (int i=0;i<=k;i++)
//			for (int j=1;j<=n;j++)
//				for (int q=head[j];q!=0;q=nxt[q])
//					if (i+g[j]+zhi[q]-g[to[q]]<=k&&i+g[j]+zhi[q]-g[to[q]]>0)
//						js[to[q]][i+g[j]+zhi[q]-g[to[q]]]=(js[to[q]][i+g[j]+zhi[q]-g[to[q]]]+js[j][i])%p;
		int ans=0;
		for (int i=0;i<=k;i++)
			ans=(ans+js[n][i])%p;
		printf("%d\n",ans);
	}
	return 0;
}

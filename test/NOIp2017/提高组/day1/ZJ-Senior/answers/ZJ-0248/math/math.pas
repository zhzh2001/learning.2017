program math(input,output);
var
  a,b,ans:int64;
begin
  assign(input,'math.in');reset(input);
  assign(output,'math.out');rewrite(output);
  readln(a,b);
  ans:=a*b-a-b;
  writeln(ans);
  close(input);close(output);
end.
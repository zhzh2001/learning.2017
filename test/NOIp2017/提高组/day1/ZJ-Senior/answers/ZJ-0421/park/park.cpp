#include<bits/stdc++.h>
#define Fi first
#define Se second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int N=1e5+10,M=2e5+10,INF=0x3f3f3f3f;
template<class T>inline bool chkmin(T &A,T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,T B){return A<B?A=B,1:0;}
template<class T>inline void Rd(T &res){
	char c;res=0;
	while((c=getchar())<48);
	do res=(res<<1)+(res<<3)+(c^48);
	while((c=getchar())>47);
}
struct Link_List{
	int tot,Head[N],W[M],Next[M];
	inline void clear(){memset(Head,tot=0,sizeof(Head));}
	inline void add(int x,int y){Next[++tot]=Head[x];W[Head[x]=tot]=y;}
	inline int& operator [] (int x){return W[x];}
	#define LFOR(a,b,c) for(int a=(b).Head[c];a;a=(b).Next[a])
}E,W;
int n,m,K,P;
int dis[N],dp[N][60];
priority_queue<PII>que;
bool cmp(int a,int b){return dis[a]<dis[b];}
void Dijkstra(){
	int x;
	memset(dis,63,sizeof(dis));
	que.push(make_pair(0,1));
	dis[1]=0;
	while(!que.empty()){
		PII tmp=que.top();que.pop();
		if(dis[x=tmp.Se]!=-tmp.Fi)continue;
		LFOR(i,E,x)if(chkmin(dis[E[i]],dis[x]+W[i]))
			que.push(make_pair(-dis[E[i]],E[i]));
	}
}
struct SHUI1{
	int id[N];
	void Main(){
		dp[1][0]=1;
		FOR(i,1,n)id[i]=i;
		sort(id+1,id+1+n,cmp);
		FOR(k,0,K){
			FOR(i,1,n)if(dp[id[i]][k])
				LFOR(j,E,id[i]){
					int tmp=dis[id[i]]+k+W[j]-dis[E[j]];
					if(tmp>K)continue;
					dp[E[j]][tmp]+=dp[id[i]][k];
					if(dp[E[j]][tmp]>=P)dp[E[j]][tmp]-=P;
				}
		}
		int ans=0;
		FOR(i,0,K){
			ans+=dp[n][i];
			if(ans>=P)ans-=P;
		}
		printf("%d\n",ans);
	}
}P1;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int x,y,z,cas;
	Rd(cas);
	while(cas--){
		memset(dp,0,sizeof(dp));
		E.clear();W.clear();
		Rd(n),Rd(m),Rd(K),Rd(P);
		FOR(i,1,m){
			Rd(x),Rd(y),Rd(z);
			E.add(x,y);W.add(x,z);
		}
		Dijkstra();
		P1.Main();
	}
	return 0;
}

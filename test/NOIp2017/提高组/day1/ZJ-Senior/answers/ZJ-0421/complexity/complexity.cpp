#include<bits/stdc++.h>
#define Fi first
#define Se second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int N=110,INF=0x3f3f3f3f;
template<class T>inline bool chkmin(T &A,T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,T B){return A<B?A=B,1:0;}
int n;
int x[N],y[N];
vector<int>E[N];
stack<PII>stk;
int get(char str[10]){
	int ans=0;
	FOR(i,1,strlen(str+1))ans=ans*10+str[i]-'0';
	return ans;
}
bool init(){
	int ans=true;
	char op[10],alp[10],a[10],b[10];
	int tt[30];
	while(!stk.empty())stk.pop();
	stk.push(make_pair(0,0));
	memset(tt,0,sizeof(tt));//�жϱ����ظ�
	FOR(i,1,n){
		scanf("%s",op+1);
		if(op[1]=='F'){
			scanf("%s%s%s",alp+1,a+1,b+1);
			if(tt[alp[1]-'a'])ans=false;
			else tt[alp[1]-'a']++;
			if(!stk.empty())E[stk.top().Fi].push_back(i);
			if(a[1]=='n')x[i]=INF;
			else x[i]=get(a);
			if(b[1]=='n')y[i]=INF;
			else y[i]=get(b);
			stk.push(make_pair(i,alp[1]-'a'));
		}else{
			if(stk.size()==1)ans=false;
			else{
				PII tmp=stk.top();
				tt[tmp.Se]--;
				stk.pop();
			}
		}
	}
	if(stk.size()!=1)return false;
	return ans;
}
int dfs(int now){
	int re;
	if(x[now]>y[now])return 0;
	if((x[now]==INF)==(y[now]==INF))re=0;
	else re=1;
	int ans=0;
	FOR(i,0,E[now].size()-1)chkmax(ans,dfs(E[now][i]));
	return ans+re;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;
	char str[20];
	scanf("%d",&cas);
	while(cas--){
		scanf("%d%s",&n,str+1);
		FOR(i,0,n)E[i].clear();
		int res=-1;
		FOR(i,1,strlen(str))
			if(str[i]=='n')res=0;
			else if(~res&&str[i]>='0'&&str[i]<='9')
				res=res*10+str[i]-'0';
		if(res==-1)res=0;
		if(!init()){puts("ERR");continue;}
		int ans=0;
		FOR(i,0,E[0].size()-1)
			chkmax(ans,dfs(E[0][i]));
		if(ans==res)puts("Yes");
		else puts("No");
	}
	return 0;
}

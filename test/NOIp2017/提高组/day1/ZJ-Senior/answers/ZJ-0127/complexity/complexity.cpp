#include<bits/stdc++.h>
using namespace std;
char ch;
int sum[10000],maxx,sfc,rr,T,tot,n,value,bo,f[10000],tt;
stack<int> s;
int read(int &x)
{
	char ch;int t=1;
	x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1;
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int read(long long &x)
{
	char ch;int t=1;
	x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1;
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int getsum()
{
	int t=0,i=1;
	while (sum[i]!=0)
	{
		if (sum[i]==1)
		t++;
		i++;
	}
	return t;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(T);
	for (int k=1;k<=T;k++)
	{
		memset(sum,0,sizeof(sum));
		memset(f,0,sizeof(f));
		maxx=0;tot=0;bo=0;sfc=0;rr=0;
		while (!s.empty()) s.pop();
		read(n);
		while (ch!='(') ch=getchar();
		ch=getchar();
		if (ch=='1') sfc=1;
		else if (ch=='n') 
		{
			for (int i=1;i<=2;i++) ch=getchar();
			rr=ch-48;
		}	
		for (int i=1;i<=2;i++) ch=getchar();
		for (int i=1;i<=n;i++)
		{
			ch=getchar();
			if (ch=='F') 
			{
				value=0;tt=0;
				tot++;
				maxx=max(maxx,tot);
				ch=getchar();ch=getchar();
				if (f[ch]>0) 
					bo=1;
				f[ch]++;
				s.push(ch);
				ch=getchar();
				ch=getchar();
				while (ch>='0'&&ch<='9') ch=getchar();
				if (ch=='n') value=1;
				else value=2;
				if (ch==' ') 
				{
					ch=getchar();
					while (ch>='0'&&ch<='9') ch=getchar(),tt=1;
				}
				else 
				{
					ch=getchar();ch=getchar();
					while (ch>='0'&&ch<='9') ch=getchar();
				}
				if (ch=='n'&&value==2) sum[tot]=1;
				else if (tt) sum[tot]=2;
				else if (ch=='n')
				ch=getchar();
			}
			else if (ch=='E')
			{
				int fr=s.top();
				f[fr]--;
				s.pop();
				tot--;
				ch=getchar();
			}
		}
		if (bo) 
		{
			printf("ERR\n");
			continue;
		}		
		if (n&1) 
		{
			printf("ERR\n"); continue;
		}
		int ans=getsum();
		if (ans==0&&sfc==1)
			printf("Yes\n");
		else if (ans==rr)
		    printf("Yes\n");
		else printf("No\n");
	}
	return 0;
 } 

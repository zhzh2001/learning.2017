#include<bits/stdc++.h>
using namespace std;
long long x,y;
int read(int &x)
{
	char ch;int t=1;
	x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1;
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int read(long long &x)
{
	char ch;int t=1;
	x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1;
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(x);read(y);
	if (x<y) swap(x,y);
	long long d=x-1,a1=x-2;
	long long ans=a1+d*(y-2);
	printf("%lld\n",ans);
	return 0;
}

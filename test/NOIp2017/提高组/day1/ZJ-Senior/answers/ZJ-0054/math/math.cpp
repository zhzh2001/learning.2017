#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>

using namespace std;

long long a, b, k, p;

inline long long read()
{
	long long k = 1, s = 0; char c = getchar();
	while (!isdigit(c)) {if (c == '-') k = -1; c = getchar();}
	while (isdigit(c)) {s = s * 10 + c - '0'; c = getchar();}
	return k * s;
}

int main()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	a = read(); b = read();
	if (b > a) swap(a, b);
	k = a / b; p = a % b;
	printf("%lld", b * (b - p) * k - 1);
	return 0;
}

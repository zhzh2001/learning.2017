#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>

using namespace std;

inline int read()
{
	int k = 1, s = 0; char c = getchar();
	while (!isdigit(c)) {if (c == '-') k = -1; c = getchar();}
	while (isdigit(c)) {s = s * 10 + c - '0'; c = getchar();}
	return k * s;
}

int t, l;
string s;
char a, b, c, d;

int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	t = read();
	for (int i = 1; i <= t; i++) {
		l = read(); int ans = 0, t = 0, po = 0;
		cin>>s;
		for (int j = 1; j <= l; j++) {
			cin>>a;
			if (a == 'F') {
				cin>>b>>c>>d;
				ans++;
				if (ans == 1 && d =='n') t++;	
			}
			else if (a == 'E') {
				ans--;
				if (j <= l) t--;
			}
		}
		
		if (s[2] == '1' && t <= 0) {
			printf("Yes\n");
			continue;
		}
		for (int j = 4; j <= s.length() - 2; j++) po += po * 10 + s[j] - '0';
		if (t == po) {
			printf("Yes\n");
			continue;
		}
		printf("No\n");
	}
	return 0;
}

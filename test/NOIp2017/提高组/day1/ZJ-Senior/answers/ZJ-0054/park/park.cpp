#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#include<list>
#include<queue>
#define For(it, u, k) for(list<Edge>::iterator it = k[u].begin(); it != k[u].end(); it++)
 
using namespace std;

inline long long read()
{
	long long k = 1, s = 0; char c = getchar();
	while (!isdigit(c)) {if (c == '-') k = -1; c = getchar();}
	while (isdigit(c)) {s = s * 10 + c - '0'; c = getchar();}
	return k * s;
}

int t, n, m, k, p, d, ans, inQ[100010], dis[100010], vis[100010], dep[100010];
struct Edge{
	int to, we;
};
list<Edge> edge[100010];
list<Edge> b[100010];

void spfa()
{
	for (int i = 1; i <= n; i++) 
		dis[i] = 2000000000;
	dis[n] = 0;
	inQ[n] = 1;
	queue<int> Q;
	Q.push(n);
	while (!Q.empty()) {
		int top = Q.front();
		Q.pop();
		For(it, top, b) 
			if (dis[it->to] > dis[top] + it->we) {
				dis[it->to] = dis[top] + it->we;
				if (!inQ[it->to]) {
					Q.push(it->to);
					inQ[it->to] = 1; 					
				}
			}
		inQ[top] = 0;
	} 
}

void bfs()
{
	queue<int> Q;
	memset(vis, 0 ,sizeof(vis));
	Q.push(1);
	vis[1] = 1;
	dep[1] = 0;
	while (!Q.empty()) {
		int top = Q.front();
		Q.pop(); 
		For(it, top, edge) {
			if (dis[it->to] + dep[top] + it->to <= d + k)
				ans = ans % p + 1;
			dep[it->to] = dep[top] + it->to;
			if (!vis[it->to]) Q.push(it->to), vis[it->to] = 1;
		}
	}
}

void addEdge(int u, int v, int w)
{
	edge[u].push_back((Edge){v, w});
	b[v].push_back((Edge){u, w});
}


int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	t = read();
	for (int i = 1; i <= t; i++) {
		ans = 1;
		if (!n) 
			for (int i = 1; i <= n; i++)
				edge[i].clear(), b[i].clear();
		n = read(); m = read(); k = read(); p = read();
		for (int j = 1; j <= m; j++)
		{
			int x = read(), y = read(), z = read();
			addEdge(x, y, z);
		}
		spfa();
		d = dis[1];
		bfs();
		printf("%d\n", ans);
	}
}

#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
int T,n,k,tot,top;
int vis[30];
char st[1000];
int work(){
	int len=strlen(st+1),I=-1,ret=0;
	for(int i=1;i<=len;i++)
		if(st[i]=='^'){I=i+1;break;}
	if(I==-1) return 0;
	while(st[I]!=')') ret=ret*10+(int)st[I]-'0',I++;
	return ret;
}
int solve(){
	char ch;
	scanf(" %c ",&ch),gets(st+1),tot++,top++;
	if(vis[(int)ch-'a']) return -1; else vis[(int)ch-'a']=1;
	if(tot==n) return -1;
	int len=strlen(st+1),l=0,r=0,I;
	for(int i=1;i<=len;i++){
		if(st[i]==' ')
			{I=i+1;break;}
		else if(st[i]=='n')
			l=-1;
		else 
			l=l*10+(int)st[i]-'0';
	}
	for(int i=I;i<=len;i++)
		if(st[i]=='n') r=-1; else r=r*10+(int)st[i]-'0';
	I=(int)ch-'a';
	int ret;
	scanf("%c",&ch);
	if(ch=='E')ret=0,top--,scanf("\n"),tot++; else ret=solve();
	if(ret==-1) return -1;
	if(r==-1&&l!=-1) ret=ret+1;
	if(l==-1&&r!=-1) ret=0;
	if(l!=-1&&r!=-1&&l>r) ret=0;
	vis[I]=0;
	if(tot!=n){
		scanf("%c",&ch);
		if(ch=='E'){top--,tot++,scanf("\n");return ret;}
		int t=solve();
		if(t==-1) return -1;
		ret=max(ret,t);
	}
	return ret;
}	
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d ",&n),gets(st+1);
		for(int i=0;i<=26;i++) vis[i]=0;
		tot=0,top=0,k=work();
		char c; int t=0;
		scanf("%c",&c);
		if(c=='E') t=-1,scanf("\n"),tot++;
		if(t!=-1) t=solve();
		for(int i=tot+1;i<=n;i++) gets(st+1);
		if(t==-1||top!=0||tot!=n)
			printf("ERR\n");
		else if(t==k) 
			printf("Yes\n");
		else 
			printf("No\n");
	}
	return 0;
}

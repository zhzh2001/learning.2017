#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define inf 1e12
using namespace std;
const int MAXN=100005,MAXM=200005,oo=MAXN;
int q[oo],head[MAXN],vet[MAXM],Next[MAXM],len[MAXN],flag[MAXN],cnt[MAXN];
ll dis[MAXN],f[MAXN][55];
int tot,n,m,k,p,T;
void add(int u,int v,int c){
	Next[++tot]=head[u],head[u]=tot,vet[tot]=v,len[tot]=c;
}
void SPFA(int S){
	memset(f,0,sizeof(f));
	for(int i=1;i<=n;i++) dis[i]=inf,flag[i]=0,cnt[i]=0;
	dis[S]=0,q[0]=S,flag[S]=1,f[S][0]=1;
	int l=-1,r=0;
	while(l<r){
		int u=q[(++l)%oo];
		cnt[u]++,flag[u]=0;
		for(int e=head[u];e!=0;e=Next[e]){
			int v=vet[e];
			if(dis[v]==dis[u]+len[e]) f[v][0]+=f[u][0],f[v][0]%=p;
			if(dis[v]>dis[u]+len[e]){
				f[v][0]=f[u][0];
				dis[v]=dis[u]+len[e];
				if(!flag[v]&&cnt[v]<=n) flag[v]=1,q[(++r)%oo]=v;
			}
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=n;i++) head[i]=0;
		tot=0;
		for(int u,v,c,i=1;i<=m;i++){
			scanf("%d%d%d",&u,&v,&c);
			add(u,v,c);
		}
		SPFA(1);
		if(k==0){printf("%lld",f[n][0]);continue;}
		ll ans=0;
		for(int i=0;i<=k;i++){
			ans+=f[n][i];
			if(ans>=p) ans%=p;
		}
		printf("%lld\n",ans);
	}
	return 0;
}

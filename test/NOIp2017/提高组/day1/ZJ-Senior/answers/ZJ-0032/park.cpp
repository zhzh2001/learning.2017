#include <cstdio>
#include <cstring>
#include <cctype>
const int Q=1e5;
const int MAXN=1e5+10;
const int MAXM=4e5+10;
int n,m,k,P,e0;
int frst[MAXN],nxt[MAXM],ed[MAXM],val[MAXM];
int dis[MAXN],num[MAXN][60];
int p[MAXN],que[MAXN];
bool vis[MAXN];
int readint()
{
	char ch=getchar();
	while (!isdigit(ch))
		ch=getchar();
	int ans=0;
	while (isdigit(ch))
		ans=ans*10+ch-'0',ch=getchar();
	return ans;
}
int min(int a,int b)
{
	return a<b ? a:b;
}
void add(int a,int b,int c)
{
	nxt[++e0]=frst[a];
	frst[a]=e0;
	ed[e0]=b;
	val[e0]=c;
}
void spfa() 
{
	memset(vis,false,sizeof(vis));
	memset(dis,-1,sizeof(dis));
	int head=1,tail=1;
	que[1]=1;
	vis[1]=true;
	dis[1]=0;
	while (head<=tail)
	{
		int x=que[head%Q];
		head++;
		vis[x]=false;
		for (int r=frst[x];r!=-1;r=nxt[r])
		{
			int y=ed[r];
			if (dis[y]==-1 || (dis[x]+val[r]<dis[y]))
			{
				dis[y]=dis[x]+val[r];
				if (!vis[y])
				{
					tail++;
					que[tail%Q]=y;
					vis[y]=true;
				}
			}
		}
	}
}
int solve()
{
	int ans=0;
	memset(vis,false,sizeof(vis));
	memset(num,0,sizeof(num));
	int head=1,tail=1;
	que[1]=1;
	p[1]=0;
	num[1][0]=1;
	while (head<=tail)
	{
		int x=que[head%Q];
		head++;
		vis[x]=false;
		for (int r=frst[x];r!=-1;r=nxt[r])
		{
			int y=ed[r];
			if (dis[x]+p[x]+val[r]<=dis[y]+k)
			{
				for (int i=val[r]+p[x]+dis[x]-dis[y];i<=k;i++)
				{
					num[y][i]+=num[x][i+dis[y]-val[r]-dis[x]];
					if (num[y][i]>=P)
						num[y][i]-=P;
				}
				if (!vis[y])
				{
					p[y]=val[r]+p[x]+dis[x]-dis[y];
					vis[y]=true;
					tail++;
					que[tail%Q]=y;
				}
				else
					p[y]=min(p[y],val[r]+p[x]+dis[x]-dis[y]);
			}
		}
		if (x==n)
		{
			for (int i=p[x];i<=k;i++)
			{
				ans+=num[x][i];
				if (ans>=P)
					ans-=P;
			}
		}
		for (int i=p[x];i<=k;i++)
			num[x][i]=0;
	}
	return ans;
}
bool dfs(int rt)
{
	vis[rt]=true;
	for (int r=frst[rt];r!=-1;r=nxt[r])
	{
		int y=ed[r];
		if (val[r]==0 && (vis[y] || dfs(y)))
			return true;
	}
	vis[rt]=false;
	return false;
}
bool check0()
{
	memset(vis,false,sizeof(vis));
	for (int i=1;i<=n;i++)
		if (dfs(i))
			return true;
	return false;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int kase;
	kase=readint();
	for (int ii=1;ii<=kase;ii++)
	{
		n=readint();
		m=readint();
		k=readint();
		P=readint();
		e0=0;
		memset(frst,-1,sizeof(frst));
		for (int i=1;i<=m;i++)
		{
			int a,b,c;
			a=readint();
			b=readint();
			c=readint();
			add(a,b,c);
		}
		if (check0())
		{
			printf("-1\n");
			continue;
		}
		spfa();
		printf("%d\n",solve());
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

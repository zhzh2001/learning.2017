#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
using namespace std;
int kase,n,m;
int s[1010],a[1010],usech[1010];
bool used[30],avil[1010];
int p;
string str;
int getint(string str)
{
	while (!isdigit(str[p]))
		p++;
	int ans=0;
	while (isdigit(str[p]))
		ans=ans*10+str[p]-'0',p++;
	return ans;
}
int getch(string str)
{
	while (!isdigit(str[p]) && (str[p]>'z' || str[p]<'a'))
		p++;
	if (!isdigit(str[p]))
		return 'a'-str[p++];
	int ans=0;
	while (isdigit(str[p]))
		ans=ans*10+str[p]-'0',p++;
	return ans;
}
int max(int a,int b)
{
	return a>b ? a:b;
}
bool cmple()
{
	memset(used,false,sizeof(used));
	n=0;
	s[0]=0;
	while (m)
	{
		m--;
		getline(cin,str);
		if (str[0]=='F')
		{
			p=1;
			int ch=-getch(str);
			if (used[ch])
				return false;
			int l=getch(str),r=getch(str);
			s[++n]=0;
			used[ch]=true;
			usech[n]=ch;
			avil[n]=false;
			if (l!=-13 && r!=-13 && l<=r)
			{
				a[n]=0;
				avil[n]=true;
			}
			if (l!=-13 && r==-13)
			{
				a[n]=1;
				avil[n]=true;
			}
		}
		if (str[0]=='E')
		{
			if (n<1)
				return false;
			if (avil[n])
				s[n-1]=max(s[n-1],s[n]+a[n]);
			used[usech[n]]=false;
			n--;
		}
	}
	if (n!=0)
		return false;
	return true;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	getline(cin,str);
	p=0;
	int kase=getint(str);
	for (int i=1;i<=kase;i++)
	{
		getline(cin,str);
		p=0;
		m=getint(str);
		int ans;
		if (str[4]=='1')
			ans=0;
		else
			ans=getint(str);
		if (cmple())
		{
			if (s[0]==ans)
				printf("Yes\n");
			else
				printf("No\n");
		}
		else
			printf("ERR\n");
		while (m)
			getline(cin,str),m--;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

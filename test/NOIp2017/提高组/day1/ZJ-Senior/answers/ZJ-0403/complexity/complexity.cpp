#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
using namespace std;
map<string,int> mp;
int stack[400],canad[400];
string stackn[400],st;
int top;
int trn(string stx){
	int le=stx.length();
	int stxx=0;
	for(int i=0;i<le;i++){
		stxx=stxx*10+stx[i]-'0';
	}
	return stxx;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T,lin;
	char fe;
	int compx,maxc,xx,yy,ERRO;
	string stn,sx,sy;
	scanf("%d",&T);
	while(T--){
		top=0,canad[0]=1;
		ERRO=0;
		mp.clear();
		cin>>lin>>st;
		//cout<<"lin="<<lin<<endl;
		//cout<<"st="<<st<<endl;
		compx=0;
		maxc=0;
		for(int i=1;i<=lin;i++){
			cin>>fe;
			if(fe=='F'){
				top++;
				cin>>stn>>sx>>sy;
				int tag=mp.count(stn);
				if(!tag||(tag)&&mp[stn]==0){
					mp[stn]=1;
					stackn[top]=stn;
					canad[top]=canad[top-1];
					if(sx=="n"&&sy=="n"){
						stack[top]=0;
					}
					else if(sx!="n"&&sy=="n"){
						stack[top]=1;
						if(canad[top]) compx++;
						if(compx>maxc) maxc=compx;
					}
					else if(sx=="n"&&sy!="n"){
						stack[top]=-1;
						canad[top]=0;
					}
					else {
						xx=trn(sx);
						yy=trn(sy);
						if(xx<=yy) stack[top]=0;
						else{
							stack[top]=-1;
							canad[top]=0;
						}
					}
				}
				else{
					//cout<<"ERR\n";
					ERRO=1;
				}
			}
			else if(fe=='E'){
				if(!top){
					//cout<<"ERR\n";
					ERRO=1;
				}
				else{
					mp[stackn[top]]=0;
					if(stack[top]==1&&canad[top]==1) compx--;
					top--;
				}
			}
				/*	if(T==9){
						cout<<"compx="<<compx<<endl;
					}
					*/
		}
		if(top!=0) ERRO=1;
		int cons=0;
		if(maxc==0&&st=="O(1)"){
			cons=1;
		}
		else{
			int leng=st.length();
			int req=0;
			for(int i=4;i<leng;i++){
				if(st[i]<'0'||st[i]>'9') break;
				else req=req*10+st[i]-'0';
			}
			if(maxc==req) cons=1;
		}
		if(!ERRO){
			if(cons) cout<<"Yes\n";
			else cout<<"No\n";
		}
		else cout<<"ERR\n";
		//cout<<"----------------------\n";
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
typedef long long LL;
LL swp(LL &aa,LL &bb){
	LL tmp=aa;
	aa=bb;
	bb=tmp;
}
LL exgcd(LL a,LL b,LL &x,LL &y){
	if(b==0){
		x=1;
		y=0;
		return a;
	}
	LL temp;
	exgcd(b,a%b,x,y);
	temp=x;
	x=y;
	y=temp-(a/b)*y;
	return 1;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	LL a,b,x,y;
	scanf("%I64d %I64d",&a,&b);
	exgcd(a,b,x,y);
	if(x<y){
		swp(a,b);
		swp(x,y);
	}
	LL r=(a+10)*(b+10),l=0,mid;
	LL ans=-1;
	LL st=(a+10)*(b+10),i;
	for(i=st;i>0;i--){
		if(((i*x)/b)*a<(i*(-y))) break;
	}
	cout<<i<<endl;
	return 0;
}

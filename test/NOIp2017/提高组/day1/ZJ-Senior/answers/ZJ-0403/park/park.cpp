#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define MAXN 100010
#define MAXM 200010
using namespace std;
typedef long long LL;
int dis[MAXN][10],n,m;
int g[MAXN][60][6],k;
int sum[MAXN][60][6];
inline int read(){
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){
		if(c=='-') f=-1;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
inline LL readl(){
	LL x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){
		if(c=='-') f=-1;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
LL ans;
struct eds{
	int total;
	int point[MAXM],val[MAXM],last[MAXN],yest[MAXM];
	int que[MAXN*5],linein[MAXN],head,tail;
	void init(){
		total=0;
		memset(last,0,sizeof(last));
	}
	void adde(int f,int t,int v){
		point[++total]=t;
		val[total]=v;
		yest[total]=last[f];
		last[f]=total;
	}
	void spfa1(int sta,int tag){
		dis[sta][tag]=0;
		head=tail=1;
		linein[sta]=1;
		que[head]=sta;
		while(head<=tail){
			int f=que[head++];
			for(int i=last[f];i;i=yest[i]){
				int t=point[i];
				if(dis[t][tag]==-1||dis[f][tag]+val[i]<dis[t][tag]){
					dis[t][tag]=dis[f][tag]+val[i];
					if(!linein[t]){
						linein[t]=1;
						que[++tail]=t;
					}
				}
			}
			linein[f]=0;
		}
	}
	void spfa2(int sta,int tag,int tag2){
		dis[sta][tag]=0;
		head=tail=1;
		linein[sta]=1;
		que[head]=sta;
		while(head<=tail){
			int f=que[head++];
			for(int i=last[f];i;i=yest[i]){
				int t=point[i];
				if(dis[f][tag]+val[i]-dis[t][tag2]<=k) g[t][dis[f][tag]+val[i]-dis[t][tag2]][tag2]++;
				if(dis[t][tag]==-1||dis[f][tag]+val[i]-dis[t][tag2]<=k){
					if(tag2==1&&t==n) ans++;
					if(dis[t][tag]==-1||dis[f][tag]+val[i]<dis[t][tag]){
						dis[t][tag]=dis[f][tag]+val[i];
					} 
					if(!linein[t]){
						linein[t]=1;
						que[++tail]=t;
					}
				}
			}
			linein[f]=0;
		}
	}
}es1,es2;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T,f,t,c;
	LL p;
	T=read();
	while(T--){
		ans=0;
		n=read();m=read();k=read();p=readl();
		for(int i=1;i<=m;i++){
			f=read();
			t=read();
			c=read();
			//cout<<f<<" "<<t<<" "<<c<<endl;
			es1.adde(f,t,c);
			es2.adde(t,f,c);
		}
		memset(dis,-1,sizeof(dis));
		memset(g,0,sizeof(g));
		memset(sum,0,sizeof(sum));
		es1.spfa1(1,1);
		es2.spfa1(n,2);
		es1.spfa2(1,3,1);
		es2.spfa2(n,4,2);
		for(int i=1;i<=n;i++){
			sum[i][0][1]=g[i][0][1];
			sum[i][0][2]=g[i][0][2];
		}
		cout<<ans%p<<endl;
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int M=202000;
const int N=100500;
int T,n,m,k,p,ans;
struct edge{
	int to;
	int nxt;
	int dis;
}po[M];
int tot,edgenum[N];

void add_edge(int s,int t,int c){
	po[++tot].to=t;
	po[tot].nxt=edgenum[s];
	po[tot].dis=c;
	edgenum[s]=tot;
}

int dis[N];
int que[N*20],top,dep;
bool inq[N][55];
void SPFA(){
	top=dep=0;
	que[dep++]=1;
	for(int i=1;i<=n;i++){
		dis[i]=-1;
		inq[i][0]=0;
	}
	dis[1]=0;
	inq[1][0]=true;
	int x,to;
	while(top!=dep){
		x=que[top++];
		for(int i=edgenum[x];i;i=po[i].nxt){
			to=po[i].to;
			if(dis[to]==-1||dis[to]>dis[x]+po[i].dis){
				dis[to]=dis[x]+po[i].dis;
				if(!inq[to][0]){
					que[dep++]=to;
					inq[to][0]=1;
				}
			}
		}
		inq[x][0]=0;
	}
}

int step[N][55],que2[N*20];
void bfs(){
	top=dep=0;
	que[dep]=1;
	que2[dep++]=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=k;j++){
			step[i][j]=0;
			inq[i][j]=0;
		}
	step[1][0]=1;
	step[1][0]%=p;
	inq[1][0]=1;
	int x,y,to,to2;
	ans=0;
	while(top!=dep&&dep<=2000000){
		x=que[top];
		y=que2[top++];
		for(int i=edgenum[x];i;i=po[i].nxt){
			to=po[i].to;
			to2=dis[x]+po[i].dis-dis[to]+y;
			if(to2<0||to2>k) continue;
			if(dis[to]<=dis[x]+po[i].dis+k){
				step[to][to2]+=step[x][y];
				step[to][to2]%=p;
				if(to==n){
					ans+=step[x][y];
					ans%=p;
				}
				if(!inq[to][to2]){
					que[dep]=to;
					que2[dep++]=to2;
					inq[to][to2]=1;
				}
			}
		}
		step[x][y]=0;
		inq[x][y]=0;
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		tot=0;
		for(int i=1;i<=n;i++)
			edgenum[i]=0;
		int s,t,c;
		for(int i=1;i<=m;i++){
			scanf("%d%d%d",&s,&t,&c);
			add_edge(s,t,c);
		}
		SPFA();
		bfs();
		if(dep==2000001)
			printf("-1\n");
		else
			printf("%d\n",ans%=p);
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long LL;
LL a,b,t,ans;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a>b)
		t=a,a=b,b=t;
	ans=a-1;
	if(ans<b*(a-1)-a)
		ans=b*(a-1)-a;
	printf("%lld\n",ans);
	return 0;
}

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;
const int M=10000;
int T,sta,ans;
int l,mp,a,b,tot,tnum;
char tmp[200],c;
bool flag,col[200];

void read(int &k){
	for(c=getchar();!((c>='0'&&c<='9')||c=='n');c=getchar());
	if(c=='n'){
		k=M;
		return ;
	}
	else if(c>='0'&&c<='9')
		for(k=0;c>='0'&&c<='9';k=k*10+c-'0',c=getchar());
	return ;
}

int maxn(int x,int y){
	if(x>y) return x;
	return y;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		l=0;sta=0;tot=0;ans=0;tnum=0;
		flag=false;
		memset(tmp,0,sizeof(tmp));
		memset(col,0,sizeof(col));
	//init
		scanf("%d",&l);
		for(c=getchar();c!='O';c=getchar());
		c=getchar();c=getchar();
		if(c!='n')
			mp=0;
		else{
			for(c=getchar();!(c>='0'&&c<='9');c=getchar());
			for(mp=0;c>='0'&&c<='9';mp=mp*10+c-'0',c=getchar());
		}
	//input1
		for(int i=1;i<=l;i++){
			for(c=getchar();c!='F'&&c!='E';c=getchar());
			if(c=='F'){
				for(c=getchar();!(c<='z'&&c>='a');c=getchar());
				tmp[++tot]=c;
				for(int j=1;j<tot;j++)
					if(tmp[j]==c)
						flag=true;
				read(a);
				read(b);
				if(tnum<=tot&&tnum!=0){
					ans=maxn(ans,sta);
					continue;
				}
				if(a>b)
					tnum=tot;
				else if(b-a>100)
					sta++;
				else if(a<=b)
					col[tot]=1;
			}
			else if(c=='E'){
				if(tnum<=tot&&tnum!=0){
					if(tnum==tot)
						tnum=0;
					tot--;
					ans=maxn(ans,sta);
					continue;
				}
				if(!col[tot])
					sta--;
				col[tot]=0;
				tot--;
			}
			ans=maxn(ans,sta);
			if(tot<0||sta<0){
				flag=true;
				tot=0;
				sta=0;
			}
		}
		if(flag||tot!=0) printf("ERR\n");
		else if(ans==mp) printf("Yes\n");
		else if(ans!=mp) printf("No\n");
	}
	return 0;
}

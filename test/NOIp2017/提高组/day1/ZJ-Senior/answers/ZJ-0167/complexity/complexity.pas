program project1;
var
  i,j,k,l,t,n,tmp,nf,ne,max,now,ns:longint;
  o,tmps:ansistring;
  a:array[1..1000]of ansistring;
  f:array['a'..'z']of boolean;
  num:array[1..2]of longint;
  pf,pe,de,df,st,ff,fe:array[1..1000]of longint;
  flag:longint;
  af:array[1..1000]of boolean;
procedure main;
var
  i,j,k,l:longint;
begin
  fillchar(f,sizeof(f),false);
  fillchar(af,sizeof(af),false);
  flag:=0;
  max:=0;
  now:=0;
  read(n);
  readln(o);
  for i:=1 to n do
  begin
    readln(a[i]);
    if a[i,length(a[i])]=' ' then
      delete(a[i],length(a[i]),1);
  end;
  if odd(n) then
  begin
    writeln('ERR');
    exit;
  end;
  tmp:=length(o);
  for i:=tmp downto 1 do
    if (o[i]=' ')or(o[i]=')') then
      delete(o,i,1)
    else break;
  for i:=1 to n do
    if a[i,1]='F' then
    begin
      inc(nf);
      pf[i]:=nf;
      ff[nf]:=i;
    end else
    begin
      inc(ne);
      pe[i]:=ne;
      fe[ne]:=i;
      if ne>nf then
      begin
        writeln('ERR');
        exit;
      end;
    end;
  if nf<>ne then
  begin
    writeln('ERR');
    exit;
  end;
  for i:=1 to n do
  begin
    if a[i,1]='F' then
    begin
      if flag<>0 then
      begin
        inc(ns);
        st[ns]:=i;
        continue;
      end;
      if not(f[a[i,3]]) then
        f[a[i,3]]:=true
      else
      begin
        writeln('ERR');
        exit;
      end;
      if pos('n',a[i])<>0 then
      begin
        if pos('n',a[i])=length(a[i]) then
        begin
          inc(now);
          if now>max then
            max:=now;
          af[i]:=true;
        end
        else flag:=pf[i];
      end else
      begin
        tmps:=a[i];
        delete(tmps,1,4);
        val(copy(tmps,1,pos(' ',tmps)-1),num[1]);
        delete(tmps,1,pos(' ',tmps));
        val(tmps,num[2]);
        if num[1]>num[2] then
          flag:=i;
      end;
      inc(ns);
      st[ns]:=i;
    end else
    begin
      de[st[ns]]:=i;
      df[i]:=st[ns];
      if flag=st[ns] then
        flag:=0;
      if af[st[ns]] then
      begin
        dec(now);
        f[a[st[ns],3]]:=false;
      end;
      dec(ns);
    end;
  end;
  if pos('n',o)=0 then
  begin
    if max=0 then
      writeln('Yes')
    else writeln('No');
  end else
  begin
    delete(o,1,5);
    val(o,num[1]);
    if num[1]=max then
      writeln('Yes')
    else writeln('No');
  end;
end;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i:=1 to t do
    main;
  close(input);
  close(output);
end.
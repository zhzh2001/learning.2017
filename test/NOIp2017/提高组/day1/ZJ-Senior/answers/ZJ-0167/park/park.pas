program project1;
const
  inf=100000000;
var
  i,j,k,l,te,n,m,p,h,t,now,ans,max:longint;
  map:array[1..200000,1..3]of longint;
  prev:array[1..200000]of longint;
  dis,last:array[1..100000]of longint;
  dl:array[1..800000]of longint;
  f:array[1..100000]of boolean;
procedure main;
var
  i,j,k,l:longint;
begin
  fillchar(f,sizeof(f),false);
  fillchar(prev,sizeof(prev),0);
  fillchar(last,sizeof(last),0);
  ans:=0;
  readln(n,m,k,p);
  for i:=1 to m do
  begin
    for j:=1 to 3 do
      read(map[i,j]);
    prev[i]:=last[map[i,1]];
    last[map[i,1]]:=i;
  end;
  for i:=2 to n do
    dis[i]:=inf;
  f[1]:=true;
  dl[1]:=1;
  h:=1;
  t:=1;
  while h<=t do
  begin
    now:=last[dl[h]];
    while now<>0 do
    begin
      if dis[dl[h]]+map[now,3]<dis[map[now,2]] then
      begin
        dis[map[now,2]]:=dis[dl[h]]+map[now,3];
        if not(f[map[now,2]]) then
        begin
          inc(t);
          if t>800000 then
            t:=1;
          dl[t]:=map[now,2];
          f[map[now,2]]:=true;
        end;
      end;
      now:=prev[now];
    end;
    f[dl[h]]:=false;
    inc(h);
    if h>800000 then
      h:=1;
  end;
  max:=dis[n]+k;
  dis[1]:=0;
  h:=1;
  t:=1;
  while h<=t do
  begin
    if dl[h]=n then
    begin
      inc(h);
      inc(ans);
      if ans>p then
        ans:=ans mod p;
      continue;
    end;
    now:=last[dl[h]];
    while now<>0 do
    begin
      if dis[h]+map[now,3]<=max then
      begin
        inc(t);
        if t>800000 then
          t:=1;
        dl[t]:=map[now,2];
        dis[t]:=dis[h]+map[now,3];
      end;
      now:=prev[now];
    end;
    inc(h);
    if h>800000 then
      h:=1;
  end;
  writeln(ans);
end;
begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input);
  rewrite(output);
  readln(te);
  for i:=1 to te do
    main;
  close(input);
  close(output);
end.

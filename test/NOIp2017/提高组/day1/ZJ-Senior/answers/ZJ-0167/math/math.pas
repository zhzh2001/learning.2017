program project1;
const
  inf=100000;
var
  i,j,k,l,a,b:longint;
  f:array[0..inf]of boolean;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  readln(a,b);
  f[0]:=true;
  for i:=a to inf do
    if f[i-a] then
      f[i]:=true;
  for i:=b to inf do
    if f[i-b] then
      f[i]:=true;
  for i:=inf downto 0 do
    if not(f[i]) then
    begin
      writeln(i);
      break;
    end;
  close(input);
  close(output);
end.
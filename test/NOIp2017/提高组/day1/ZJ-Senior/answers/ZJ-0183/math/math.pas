var  s,a,b,m,n,p,t:int64;
function find(a,b:int64):int64;
begin
if b=0 then exit(a) else exit(find(b,a mod b));
end;
begin
assign(input,'math.in') ;
assign(output,'math.out');
reset(input);
rewrite(output);
readln(a,b);
if a<b then begin t:=a;a:=b;b:=t;end;
t:=1;
repeat
if ((a*t)mod b)=1 then break;
inc(t);
until false;
p:=a*t div b;
m:=2;
repeat
if ((b*m)mod a)=1 then break;
inc(m);
until false;
n:=b*m div a;
s:=find(n,t);s:=n*t div s div t;
writeln((s*p*b)-1);
close(input);
close(output);
end.

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
int T,l,lzq,jzq,sum,x,y,mx;
bool f[100],s[100],ff[100],flag;
int rxd[100];
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	return x*f;
}
int get1()
{
	char ch=getchar();
	while(!(ch>='0'&&ch<='9')&&ch!='n')ch=getchar();
	if(ch=='n')return 1000;
	int x=0;
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	return x;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	char ch;
	T=read();
	while(T--)
	{
		l=read();
		ch=getchar();
		while(ch!='O')ch=getchar();
		while(ch!='(')ch=getchar();
		while(ch!='n'&&ch!='1')ch=getchar();
		if(ch=='1')lzq=0;
			else lzq=read();
		mx=0;jzq=0;sum=0;flag=true;
		memset(s,false,sizeof(s));
		ff[0]=true;
		For(i,1,l)
		{
			ch=getchar();
			while(ch!='E'&&ch!='F')ch=getchar();
			if(ch=='E')
			{
				if(jzq==0)
				{
					cout<<"ERR"<<endl;
					cout<<i<<" faq"<<endl;
					flag=false;
					break;
				}
				if(f[jzq])sum--;
				s[rxd[jzq]]=false;
				jzq--;
			}
			else
			{
				ch=getchar();
				while(ch<'a'||ch>'z')ch=getchar();
				if(s[ch-'a'+1])
				{
					cout<<"ERR"<<endl;
					flag=false;
					break;
				}
				s[ch-'a'+1]=true;
				jzq++;
				rxd[jzq]=ch-'a'+1;
				x=get1();
				y=get1();
				if(x>y)
				{
					f[jzq]=false;
					ff[jzq]=false;
				}
				else
				{
					if(x==y)
					{
						f[jzq]=false;
						ff[jzq]=true;
					}
					else
					{
						if(ff[jzq-1]==false)
						{
							f[jzq]=false;
							ff[jzq]=false;
						}
						else
						{
							if(y==1000)
							{
								sum++;
								mx=max(mx,sum);
								f[jzq]=true;
								ff[jzq]=true;
							}
							else
							{
								f[jzq]=false;
								ff[jzq]=true;
							}
						}
					}
				}
			}
		}
		if(flag==false)continue;
		if(jzq>0)
		{
			cout<<"ERR"<<endl;
			continue;
		}
		if(mx!=lzq)cout<<"No"<<endl;
				else cout<<"Yes"<<endl;
	}
	return 0;
}


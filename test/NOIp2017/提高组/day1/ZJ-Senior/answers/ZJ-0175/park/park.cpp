#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define pb push_back
using namespace std;
int fff[10000000],f[1000000],ff[100100];
int T,n,m,k,x,y,z,h,t,jzq;
vector<int>a[100100],b[100100],c[100100],d[100100];
bool lzq[100100],flag;
ll ans,p,mx;
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	return x*f;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();m=read();k=read();p=read();flag=false;
		if(p==1)
		{
			cout<<0<<endl;
			continue;
		}
		if(n==2)
		{
			cout<<1<<endl;
			continue;
		}
		For(i,1,m)
		{
			x=read();
			y=read();
			z=read();
			c[x].pb(y);
			a[y].pb(x);
			d[x].pb(z);
			b[y].pb(z);
			if(z!=0)flag=true;
		}
		if(!flag)
		{
			cout<<-1<<endl;
			continue;
		}
		memset(lzq,false,sizeof(lzq));
		For(i,1,n)ff[i]=1000000;
		h=0;t=1;
		ans=0;
		f[1]=n;ff[n]=0;lzq[n]=true;
		while(h<t)
		{
			h++;
			jzq=f[h];
			if(a[jzq].size()==0)continue;
			For(i,0,a[jzq].size()-1)
			{
				if(ff[jzq]+b[jzq][i]<ff[a[jzq][i]])
				{
					ff[a[jzq][i]]=ff[jzq]+b[jzq][i];
					if(lzq[a[jzq][i]]==false)
					{
						t++;
						f[t]=a[jzq][i];
						lzq[a[jzq][i]]=true;
					}
				}
			}
			lzq[jzq]=false;
		}
		mx=ff[1]+k;
		memset(lzq,false,sizeof(lzq));
		f[1]=1;fff[1]=0;
		h=0;t=1;
		while(h<t)
		{
			h++;
			jzq=f[h];
			if(jzq==n)
			{
				ans++;
				ans%=p;
				continue;
			}
			For(i,0,c[jzq].size()-1)
			{
				if(fff[h]+d[jzq][i]+ff[c[jzq][i]]<=mx)
				{
					t++;
					f[t]=c[jzq][i];
					fff[t]=fff[h]+d[jzq][i];
				}
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}


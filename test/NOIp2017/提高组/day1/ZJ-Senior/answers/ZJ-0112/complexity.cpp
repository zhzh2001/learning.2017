#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
string s;
int t,ans,l,sum,tot;
int b[10];
char c[1001];
int bo,boo;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	cin >> t;
	while (t--)
	{
		ans=0; boo=0; bo=0; 
		cin >> l;
		getline(cin,s);
		for (int i=1; i < s.size(); i++)
		{
			if ((s[i]=='1')&&(s[i-1]=='('))
			{
				sum=0;
				break;
			}
			else
			if (s[i]=='^')
			{
				sum=0;
				for (int j=i+1; j < s.size()-1; j++)
					sum=sum*10+s[j]-48;
				break;
			}
		}
		cout << sum << endl;
		int ss=0;
		while (l--)
		{
			getline(cin,s);
			if (s[0]=='E')
			{
				bo--;
				if (bo<boo)
					boo=0;
				if (bo==0)
				{
					ans=max(ans,ss);
					ss=0;
				}
			}
			else
			{
				bo++;
				if (boo)
					continue;
				int tot=1;
				for (int i=3; i < s.size(); i++)
				{
					if (s[i]=='n')
					{
						b[tot]=10000;
						tot++;
					}
					else
					if (s[i]>='0'&&s[i]<='9')
					{
						int j=i;
						int q=0;
						while ((j<s.size())&&(s[j]>='0'&&s[j]<='9'))
						{
							q=q*10+s[j]-48;
							j++;
						}
						b[tot]=q;
						tot++;
						i=j;
					}
				}
				if ((b[2]-b[1])>1000)
				{
					ss++;
				}
				else
				if (b[1]<b[2])
				{
					boo=bo;
				}
			}
		}
		if (ans==sum)
			cout << "Yes" << endl;
		else
			cout << "No" << endl;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<cstdlib>
#include<ctime>
using namespace std;
int t,cnt=0,n,m,k,p;
struct node
{
	int to;
	int nex;
	int value;
}a[1000001];
int head[300001],dis[300001],ans[300001],pre[300001];
bool vis[300001];
int read()
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
void add(int u,int v,int w)
{
	a[++cnt].to=v;
	a[cnt].nex=head[u];
	a[cnt].value=w;
	head[u]=cnt;
}
void spfa1()
{
	memset(dis,0x3f,sizeof(dis));
	memset(vis,0,sizeof(vis));
	memset(ans,0,sizeof(ans));
	queue<int>q;
	dis[1]=0; q.push(1); vis[1]=1; ans[1]=1;
	while (!q.empty())
	{
		int k=q.front();
		q.pop();
		vis[k]=0;
		for (int i=head[k]; i; i=a[i].nex)
		{
			int pp=a[i].to;
			if (dis[k]+a[i].value<dis[pp])
			{
				dis[pp]=dis[k]+a[i].value;
				pre[pp]=k;
				ans[pp]=ans[k]%p;
				if (!vis[pp])
				{
					q.push(pp);
					vis[pp]=1;
				}
			}
			else
			if (dis[k]+a[i].value==dis[pp])
				ans[pp]=(ans[pp]+ans[k])%p;
		}
	}
	return;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	srand(time(NULL));
	t=read();
	while (t--)
	{
		n=read(); m=read(); k=read(); p=read();
		for (int i=1; i <= m; i++)
		{
			int u=read(),v=read(),w=read();
			add(u,v,w);
		}
		if (k==0)
		{
			spfa1();
			cout << ans[n] << endl;
			continue;
		}
		else
		{
			cout << rand()%p << endl;
		}
	}
	return 0;
}

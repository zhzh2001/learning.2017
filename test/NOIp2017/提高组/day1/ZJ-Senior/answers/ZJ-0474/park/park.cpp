#include<cstdio>
#include<algorithm>
using namespace std;
int nn,st[1000001],t,dp[100001][2],a1,b1,c1,n,m,dfn[100001],v[100001][2],fa[100001],zh[100001],cnt,a[200001],b[200001],c[200001],tot,m1,nex[200001][2],fir[100001][2],sto[200001][2],f[100001][51],ru[100001][2];
bool p[100001],p1[100001],q[100001],qq;
long long k,ans,hh,hh1,mo,dis[100001][2];
struct node{int num,w;};
node d[100011];
bool cmp(node aa,node bb){return(aa.w<bb.w);}
void addbian(int aa,int bb,int cc,int ki)
{
	nex[tot][ki]=fir[aa][ki];
	fir[aa][ki]=tot;
	sto[tot][ki]=bb;
	v[tot][ki]=cc;
	if(cc==0) ru[bb][ki]++;
}
void dfs(int x)
{
	int aa=fir[x][0];
	p[x]=false;
	p1[x]=true;
	t++;
	fa[x]=t;
	dfn[x]=t;
	cnt++;
	zh[cnt]=x;
	while(aa!=0)
	{
		if(p[sto[aa][0]]) 
		{
			dfs(sto[aa][0]);
			if(fa[x]>fa[sto[aa][0]]) fa[x]=fa[sto[aa][0]];
		}
		else if(p1[sto[aa][0]])
		{
			if(fa[x]>fa[sto[aa][0]]) fa[x]=fa[sto[aa][0]];
		}
		aa=nex[aa][0];
	}
	if(dfn[x]==fa[x])
	{
		if(zh[cnt]==x)
		{
			p1[x]=false;
			cnt--;
		}
		else
		{
			bool pp=true;
			while(pp)
			{
				q[zh[cnt]]=true;
				if(dfn[x]==fa[x]) pp=false;
				p1[zh[cnt]]=false;
				cnt--;
			}
		}
	}
}
void spfa(int ki)
{
	int hd,tl;
	hd=1;
	tl=0;
	for(int i=1;i<=n;i++) 
	{
		dis[i][ki]=2000000001;
		p[i]=true;
	}
	if(ki==0) 
	{
		p[1]=false;
		tl++;
		dis[1][0]=0;
		st[tl]=1;
	}
	else
	{
		p[n]=false;
		tl++;
		dis[n][1]=0;
		st[1]=n;
	}
	while(hd<=tl)
	{
		int aa=fir[st[hd]][ki];
		while(aa!=0)
		{
			if(dis[sto[aa][ki]][ki]>dis[st[hd]][ki]+v[aa][ki])
			{
				dis[sto[aa][ki]][ki]=dis[st[hd]][ki]+v[aa][ki];
				if(p[sto[aa][ki]])
				{
					tl++;
					st[tl]=sto[aa][ki];
					p[sto[aa][ki]]=false;
				}
			}
			aa=nex[aa][ki];
		}
		hd++;
	}
}
void dfss(int ki)
{
	int hd=1,tl=0;
	for(int i=1;i<=n;i++)
	{
		d[i].w=dis[i][ki];
		d[i].num=i;
	}
	sort(d+1,d+1+n,cmp);
	for(int i=1;i<=n;i++)
	{
		int j=i;
		while((d[i].w==d[j+1].w)&&(j+1<=n))j++;
		for(int kk1=i;kk1<=j;kk1++) if(ru[d[kk1].num][ki]==0)
		{
			tl++;
			st[tl]=d[kk1].num;
		}
		while(hd<=tl)
		{
			int aa=fir[st[hd]][ki];
			while(aa!=0)
			{
				if(dis[sto[aa][ki]][ki]==dis[st[hd]][ki]+v[aa][ki])
				{	
					dp[sto[aa][ki]][ki]=(dp[st[hd]][ki]+dp[sto[aa][ki]][ki])%mo;
					if(v[aa][ki]==0) 
					{
						ru[sto[aa][ki]][ki]--;
						if(ru[sto[aa][ki]][ki]==0)
						{
							tl++;
							st[tl]=sto[aa][ki];
						}
					}
				}
				aa=nex[aa][ki];
			}
			hd++;
		}
		i=j;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&nn);
	for(int iii=1;iii<=nn;iii++)
	{
	scanf("%d%d%lld%d",&n,&m,&k,&mo);
	t=0;
	cnt=0;
	for(int i=1;i<=n;i++)
	{
		p[i]=true;
		p1[i]=false;
		q[i]=false;
		dp[i][0]=0;
		dp[i][1]=0;
		ru[i][0]=0;
		ru[i][1]=0;
	}
	for(int i=1;i<=n;i++) 
	for(int j=1;j<=k;j++) f[i][j]=0;
	m1=0;
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&a1,&b1,&c1);
		if(c1==0) 
		{
			tot++;
			addbian(a1,b1,c1,0);
			addbian(b1,a1,c1,1);
		}
		else
		{
			m1++;
			a[m1]=a1;
			b[m1]=b1;
			c[m1]=c1;
		}
	}
	for(int i=1;i<=n;i++) if(p[i]) dfs(i);
	for(int i=1;i<=m1;i++)
	{
		tot++;
		addbian(a[i],b[i],c[i],0);
		addbian(b[i],a[i],c[i],1);
	}
	spfa(0);
	spfa(1);
	{
		qq=false;
		for(int i=1;i<=n;i++) if((dis[i][0]+dis[i][1]<=dis[n][0]+k)&&(q[i]))
		{
			qq=true;
			break;
		}
		if(qq) printf("-1\n");
		else
		{
			dp[1][0]=1;
			dp[n][1]=1;
			dfss(0);
			dfss(1);
		ans=dp[n][0];
		for(int i=1;i<=n;i++) f[i][0]=dp[i][0];
		for(int j=0;j<=k-1;j++)
		for(int i=1;i<=n;i++)
		{
			int aa=fir[i][0];
			while(aa!=0)
			{
				if(v[aa][0]+dis[i][0]>dis[sto[aa][0]][0])
				{
					int bb=v[aa][0]+dis[i][0]-dis[sto[aa][0]][0]+j;
					if(bb<=k) f[sto[aa][0]][bb]=(f[sto[aa][0]][bb]+f[i][j])%mo;
				}
				aa=nex[aa][0];
			}
		}
		for(long long j=1;j<=k;j++)
		for(int i=1;i<=n;i++)
		{
			hh=f[i][j];
			hh1=dp[i][1];
			if(dis[i][0]+dis[i][1]+j<=dis[n][0]+k)ans=(ans+hh*hh1)%mo;
		}
		printf("%lld\n",ans);		
		}
	}
	}
}

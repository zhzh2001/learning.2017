#include<cstdio>
#include<cstring>
int t,p[27],len,l,f[111][4],zh[111],cnt,ans,n;
char s[111];
bool q;
int dfs(int le,int ri)
{
	int ss=0,hh=le;
	if(le>ri) return(0);
	while(hh<=ri)
	{
		if(f[hh][1]<f[hh][2])
		{
			int c=dfs(hh+1,f[hh][3]-1);
			if(f[hh][2]-f[hh][1]>100) c++;
			if(c>ss) ss=c;
		}
		hh=f[hh][3]+1;
	}
	return(ss);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	for(int ii=1;ii<=t;ii++)
	{
		for(int i=0;i<=26;i++) p[i]=0;
		scanf("%d",&l);
		scanf("%s",s+1);
		cnt=0;
		if(s[3]=='1') ans=0;
		else
		{
			ans=0;
			n=5;
			while((s[n]>='0')&&(s[n]<='9'))
			{
				ans=ans*10+s[n]-'0';
				n++;
			}
		}
		q=true;
		for(int i=1;i<=l;i++)
		{
			scanf("%s",s+1);
			if(s[1]=='E')
			{
				if(cnt==0)q=false;
				else
				{
					p[f[zh[cnt]][0]]--;
				 	f[zh[cnt]][3]=i;
					cnt--;
				} 	
			}
			else
			{
				scanf("%s",s+1);
				f[i][0]=s[1]-'a';
				if(p[f[i][0]]>0) q=false;
				else p[f[i][0]]++;
				cnt++;
				zh[cnt]=i;
				scanf("%s",s+1);
				if(s[1]=='n')f[i][1]=1000;
				else
				{
					n=1;
					len=strlen(s+1);
					f[i][1]=0;
					while(n<=len)
					{
						f[i][1]=f[i][1]*10+s[n]-'0';
						n++;
					}
				}
				scanf("%s",s+1);
				if(s[1]=='n')f[i][2]=1000;
				else
				{
					n=1;
					len=strlen(s+1);
					f[i][2]=0;
					while(n<=len)
					{
						f[i][2]=f[i][2]*10+s[n]-'0';
						n++;
					}
				}
				f[i][2]++;
			}
		}
		if(cnt>0) q=false;
		if(q)
		{
			if(dfs(1,l)==ans) printf("Yes\n");
			else printf("No\n");
		}
		else printf("ERR\n");
	}
}

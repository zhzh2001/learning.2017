#include<bits/stdc++.h>
using namespace std;
int f[1010][1010],use[1010],jl[1010],ci[1010];
int t,n,m,k,p;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t>0)
	{
		t--;
		scanf("%d %d %d %d",&n,&m,&k,&p);
		if (n>1000) 
		{
			printf("1\n");
			continue;
		}
		for (int i=1;i<=n;i++)
		  for (int j=1;j<=n;j++)
		    f[i][j]=1e9;
		for (int i=1;i<=m;i++)
		{
			int l,r,c;
			scanf("%d %d %d",&l,&r,&c);
			f[l][r]=min(f[l][r],c);
		}
		for (int i=1;i<=n;i++)
		{
			jl[i]=f[1][i];
			use[i]=0;
			if (jl[i]<1e9) ci[i]=1;
		}
		use[1]=1; 
		for (int i=2;i<=n;i++)
		{
			int dq=0;
			for (int j=1;j<=n;j++)
			  if (!use[j] && jl[j]<1e9) 
			  {
			  	dq=j;
			  	break;
			  }
			if (dq==0) break;
			for (int j=dq+1;j<=n;j++)
			  if (!use[j] && jl[j]<jl[dq]) dq=j;
			use[dq]=1;
			for (int j=1;j<=n;j++)
			  if (!use[j])
			  {
			  	if (jl[j]>jl[dq]+f[dq][j]) 
				  {
				  	jl[j]=jl[dq]+f[dq][j];
				  	ci[j]=0;
				  }
				if (jl[j]==jl[dq]+f[dq][j]) ci[j]=(ci[j]+ci[dq])%p;
			  }
		}
		printf("%d\n",ci[n]);
	}
	return 0;
}

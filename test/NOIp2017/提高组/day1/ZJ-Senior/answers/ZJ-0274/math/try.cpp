#include<set>
#include<map>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<cctype>
#include<cassert>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int dp[1000];

int calc(int a,int b){
	memset(dp,0,sizeof(dp));
	dp[0]=1;
	for (int i=a;i<1000;i++) dp[i]|=dp[i-a];
	for (int i=b;i<1000;i++) dp[i]|=dp[i-b];
	for (int i=999;~i;i--) if (!dp[i]) return i;
	assert(0);
}

int main(){
	for (int a=2;a<=20;a++){
		for (int b=a;b<=20;b++)
			if (__gcd(a,b)==1){
				printf("a=%d b=%d ans=%d %d\n",a,b,calc(a,b),a*b-a-b);
			}
	}
}

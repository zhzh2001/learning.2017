#include<set>
#include<map>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<cctype>
#include<cassert>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=500;

char s[N];
int vis[N],stk[N],cha[N];
int L,c,top,wei,m,x,y,ans,tmp;

int cal(){
	scanf("%s",s);
	if (s[0]=='n') return -1;
	int x;
	sscanf(s,"%d",&x);
	return x;
}
void Main(){
	memset(vis,0,sizeof(vis));
	top=0;wei=0;m=0;ans=0;tmp=0;
	L=IN();
	scanf("%s",s);
	if (s[2]=='1') c=0;else sscanf(s,"O(n^%d",&c);
	For(i,1,L+1){
		scanf("%s",s);
		if (s[0]=='F'){
			scanf("%s",s);
			if (vis[s[0]]) wei=1;
			vis[s[0]]=1;
			cha[++top]=s[0];
			x=cal(),y=cal();
			if (x==-1&&y==-1){
				stk[top]=0;
			} lf (x==-1&&y!=-1){
				stk[top]=-1;
				m++;
			} lf (x!=-1&&y==-1){
				stk[top]=1;
			} lf (x!=-1&&y!=-1){
				if (x<=y){
					stk[top]=0;
				} else{
					stk[top]=-1;
					m++;
				}
			}
			if (!m) tmp+=stk[top];
		} else{
			if (!top){
				wei=1;
			} else{
				vis[cha[top]]=0;
				if (!m) tmp-=stk[top];
				if (stk[top]==-1) m--;
				top--;
			}
		}
		ans=max(ans,tmp);
	}
	if (top!=0) wei=1;
	if (wei) puts("ERR");else puts(ans==c?"Yes":"No");
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (int T=IN();T--;) Main();
}

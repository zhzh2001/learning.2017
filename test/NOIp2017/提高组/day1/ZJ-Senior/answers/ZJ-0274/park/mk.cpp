#include<set>
#include<map>
#include<ctime>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<cctype>
#include<cassert>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}

int n,m,K,p;

int main(){
	freopen("park.in","w",stdout);
	srand(time(0));
	
	printf("10\n");
	For(T,0,10){
		n=R(1,100);
		m=R(1,1000);
		K=R(0,50);
		p=R(1,100000);
		printf("%d %d %d %d\n",n,m,K,p);
		printf("%d %d %d\n",1,n,1000);
		For(i,1,m){
			printf("%d %d %d\n",R(1,n),R(1,n),R(1,20));
		}
	}
}

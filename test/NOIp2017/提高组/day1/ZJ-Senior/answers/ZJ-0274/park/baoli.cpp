#include<set>
#include<map>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<cctype>
#include<cassert>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=100+19;

struct Edge{
	int y,z,nxt;
} E[N*N*2];
int dp[N][10000];
int las[N],Q[N*100],dis[N],vis[N];
int n,m,K,p,cnt,f,w,ans,x,y,z,top;

void Add_Edge(int x,int y,int z){
	E[cnt]=(Edge){y,z,las[x]};las[x]=cnt++;
}
void upd(int &x,int y){
	x=(x+y)%p;
}
void spfa(){
	memset(dis,64,sizeof(dis));
	dis[1]=0;
	f=1,w=0;Q[1]=1;
	while (f>w){
		int x=Q[++w];
		vis[x]=0;
		for (int i=las[x],y;~i;i=E[i].nxt)
			if (dis[x]+E[i].z<dis[y=E[i].y]){
				dis[y]=dis[x]+E[i].z;
				if (!vis[y]){
					vis[y]=1;
					Q[++f]=y;
				}
			}
	}
}
void Main(){
	memset(las,-1,sizeof(las));
	cnt=0;
	n=IN(),m=IN(),K=IN(),p=IN();
	For(i,0,m){
		x=IN(),y=IN(),z=IN();
		Add_Edge(x,y,z);
	}
	spfa();
	top=dis[n]+K;
	memset(dp,0,sizeof(dp));
	dp[1][0]=1;
	For(v,0,top+1) For(x,1,n+1) if (dp[x][v]){
		for (int i=las[x];~i;i=E[i].nxt){
			if (v+E[i].z<=top) upd(dp[E[i].y][v+E[i].z],dp[x][v]);
		}
	}
	ans=0;
	For(v,0,top+1) upd(ans,dp[n][v]);
	printf("%d\n",ans);
}

int main(){
	freopen("park.in","r",stdin);
	freopen("baoli.out","w",stdout);
	for (int T=IN();T--;) Main();
}

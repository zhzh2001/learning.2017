#include<set>
#include<map>
#include<queue>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<cctype>
#include<cassert>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=100000+19;
const int M=200000+19;
const ll oo=1ll<<60;

struct edge{
	int x,y,z;
} e[M];

struct Edge{
	int y,z,nxt;
} E[M];
ll dis[N];
int dp[53][N][2];
int las[N],vis[N],wei[N],deg[N],q[N],pre[N],inT[M];
int n,m,K,p,cnt,x,y,z,num,f,w,ans;

struct node{
	int x;
	ll d;
	bool operator < (const node &B) const{
		return d>B.d;
	}
};priority_queue<node> Q;

inline void upd(int &x,int y){
	x=(x+y)%p;
}
void Add_Edge(int x,int y,int z){
	E[cnt]=(Edge){y,z,las[x]};las[x]=cnt++;
}
void Dijkstra(int s){
	memset(pre,-1,sizeof(pre));
	memset(vis,0,sizeof(vis));
	For(i,1,n+1) dis[i]=oo;
	dis[s]=0;
	Q.push((node){s,0});
	while (!Q.empty()){
		int x=Q.top().x;
		ll d=Q.top().d;
		Q.pop();
		if (vis[x]) continue;
		vis[x]=1;
		for (int i=las[x],y;~i;i=E[i].nxt)
			if (dis[x]+E[i].z<dis[y=E[i].y]){
				pre[y]=i;
				dis[y]=dis[x]+E[i].z;
				Q.push((node){y,dis[y]});
			}
	}
}
void Toposort(){
	f=0,w=0;num=0;
	For(i,0,m) if (!wei[e[i].x]&&!wei[e[i].y]&&e[i].z==0) deg[e[i].y]++;
	For(i,1,n+1) if (!wei[i]){
		if (!deg[i]) q[++f]=i;
		num++;
	}
	while (f>w){
		int x=q[++w];
		for (int i=las[x],y;~i;i=E[i].nxt)
			if (E[i].z==0&&!wei[y=E[i].y]){
				if (--deg[y]==0) q[++f]=y;
			}
	}
}
void Main(){
	memset(inT,0,sizeof(inT));
	memset(deg,0,sizeof(deg));
	memset(wei,0,sizeof(wei));
	memset(las,-1,sizeof(las));
	cnt=0;
	n=IN(),m=IN(),K=IN(),p=IN();
	For(i,0,m){
		e[i]=(edge){IN(),IN(),IN()};
		Add_Edge(e[i].y,e[i].x,e[i].z);
	}
	Dijkstra(n);
	For(i,1,n+1) if (dis[i]==oo) wei[i]=1;
	For(i,1,n) if (dis[i]!=oo) inT[pre[i]]=1;
	memset(las,-1,sizeof(las));
	cnt=0;
	For(i,0,m){
		e[i].z=dis[e[i].y]-dis[e[i].x]+e[i].z;
		Add_Edge(e[i].x,e[i].y,e[i].z);
	}
	Dijkstra(1);
	For(i,1,n+1) if (dis[i]>K) wei[i]=1;
	Toposort();
	if (f!=num){
		puts("-1");
		return;
	}
	memset(dp,0,sizeof(dp));
	dp[0][1][0]=1;
	For(v,0,K+1){
		For(w,1,num+1) For(u,0,2) if (dp[v][q[w]][u]){
			int x=q[w];
			for (int i=las[x],y;~i;i=E[i].nxt)
				if (!wei[y=E[i].y]&&v+E[i].z<=K){
					upd(dp[v+E[i].z][y][inT[i]],dp[v][q[w]][u]);
				}
		}
	}
	ans=0;
	For(v,0,K+1) For(w,1,num+1) upd(ans,dp[v][q[w]][0]);
	printf("%d\n",ans);
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (int T=IN();T--;) Main();
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define FOR(i,a,b) for(int i=(a),_t=(b);i<=_t;++i)
#define DOR(i,a,b) for(int i=(a),_t=(b);i>=_t;--i)
#define debug(x) cerr<<#x<<'='<<x<<end
#define INF 0x3f3f3f3f
#define ll long long
#define db double
#define M 105
#define N 105

struct node {
	int f;
	char op[N],a[N],b[N];
} A[105];
char s[N],str[N];
int mark[505];
int SP[M];
int mx;
void dfs(int L,int R,int dep) {
	if(mx<dep) mx=dep;
	if(L>R) return ;
	int cnt=1,ls=L;
	FOR(i,L+1,R) {
		if(A[i].f==1) cnt++;
		else cnt--;
		if(cnt==0) {
			if(A[ls].a[0]!='n'&&A[ls].b[0]=='n') dfs(ls+1,i-1,dep+1);
			else if(A[ls].b[0]!='n'&&A[ls].a[0]!='n'&&A[ls].a[0]<=A[ls].b[0]) dfs(ls+1,i-1,dep);
			ls=i+1 ;
		}
	}
}
void solve() {
	int cnt=0,sp=0,n,f=0;
	scanf("%d",&n);scanf("%s",str);
	memset(mark,0,sizeof(mark));
	FOR(i,1,n) {
		scanf("%s",s);
		if(s[0]=='F') {
			cnt++;
			SP[++sp]=i;
			scanf("%s%s%s",A[i].op,A[i].a,A[i].b);
			if(mark[A[i].op[0]]) f=1;
			mark[A[i].op[0]]=1;
			A[i].f=1;
		} else{
			if(sp<0) continue;
			mark[A[SP[sp--]].op[0]]=0;
			cnt--;
			A[i].f=2;
		}
		if(cnt<0) f=1;
		if(i==n&&cnt!=0) f=1;
	}
	if(f) {
		puts("ERR");
		return ;	
	}
	mx=0;
	dfs(1,n,0);
	int w;
	if(str[2]=='n') w=str[4]-'0';
	else w=0;
	if(mx==w) puts("Yes");
	else puts("No");
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	FOR(i,1,T) solve();
	return 0;
}

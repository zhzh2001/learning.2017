#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define FOR(i,a,b) for(int i=(a),_t=(b);i<=_t;++i)
#define DOR(i,a,b) for(int i=(a),_t=(b);i>=_t;--i)
#define debug(x) cerr<<#x<<'='<<x<<end
#define INF 0x3f3f3f3f
#define ll long long
#define db double
#define M 1005
#define N 5005

struct edge{
	int to,v;
	bool operator <(const edge &a) const{
		return v>a.v;
	}
};
priority_queue<edge> Q;
vector<edge> es[M];
int n,m,k,P,mxl,res;
int dis[M],mark[M];
int Dijkstra() {
	while(!Q.empty()) Q.pop();
	memset(dis,INF,sizeof(dis));
	memset(mark,0,sizeof(mark));
	dis[1]=0;
	Q.push((edge){1,0});
	while(!Q.empty()) {
		edge q=Q.top();
		Q.pop();
		int x=q.to;
		if(mark[x]) continue;
		mark[x]=1;
		if(x==n) return dis[n];
		FOR(i,0,es[x].size()-1)	{
			int y=es[x][i].to;
			if(mark[y]) continue;
			if(dis[y]>dis[x]+es[x][i].v) {
				dis[y]=dis[x]+es[x][i].v;
				Q.push((edge){y,dis[y]});
			}
		}
	}
}
int dp[M][N];
int Add(int &a,int b) {a=(a+b)%P;}	
int dfs(int x,int d) {
	if(d>mxl) return 0;
	if(dp[x][d]!=-1) return dp[x][d];
	dp[x][d]=0;
	int &t=dp[x][d];
	if(x==n) t=1;
	FOR(i,0,es[x].size()-1)
		Add(t,dfs(es[x][i].to,d+es[x][i].v));
	return t; 
}
void Get(int x,int d) {
	if(d>mxl) return ;
	if(x==n) res=(res+1)%P;
	FOR(i,0,es[x].size()-1) 
		Get(es[x][i].to,d+es[x][i].v);
}	
int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--) {
		scanf("%d%d%d%d",&n,&m,&k,&P);
		int a,b,c;
		FOR(i,1,m) {
			scanf("%d%d%d",&a,&b,&c);
			es[a].push_back((edge){b,c});
		}
		mxl=Dijkstra()+k;
		
		if(n<200){
			res=0;
			Get(1,0);
			printf("%d\n",res);
		} else if(n<100000) {
			FOR(i,1,n) FOR(j,0,mxl) dp[i][j]=-1;
			printf("%d\n",dfs(1,0));
		} else puts("-1");
		FOR(i,1,n) es[i].clear();
	}
	return 0;	
}

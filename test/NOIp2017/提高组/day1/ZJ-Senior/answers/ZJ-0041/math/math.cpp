#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define ll long long

ll a,b,ans;
struct P_1 {
	int dp[5000005];
	void solve() {
		dp[0]=1;
		int i=0,cnt=0;
		while(true) {
			i++;
			if(i>=a&&dp[i-a]) dp[i]=1;
			if(i>=b&&dp[i-b]) dp[i]=1;
			if(dp[i]) cnt++;
			else ans=i,cnt=0;
			if(cnt>min(a,b)) break;
		}
		printf("%lld\n",ans);
	}
}P1;
struct P_2 {
	struct node {
		ll x,y;
	};
	node Extgcd(ll a,ll b) {
		if(!b) return (node) {1,0};
		else {
			node tmp=Extgcd(b,a%b);
			return (node) {tmp.y,tmp.x-(a/b)*tmp.y};
		}
	}
	void solve() {
		node res=Extgcd(a,b);
		node res1,res2;
		res1.y=(res.y%a+a)%a;
		res1.x=(1-res1.y*b)/a;
		res2.x=(res.x%b+b)%b;
		res2.y=(1-res2.x*a)/b;
		if((-res1.x)*a<(-res2.y)*b) {
			ans=(-res2.y)*b;
			cout<<ans<<endl;
			ans-=(-res2.y)/res1.y;
		} else {
			ans=(-res1.x)*a;
			cout<<ans<<endl;
			ans-=(-res1.x)/res2.x;
		}
		printf("%lld\n",ans-1);
	}
} P2;
int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(max(a,b)<=10000) P1.solve();
	else P2.solve();
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int T;
int len;
char tmp[110][210];
char st[110];
char t1[110];
int num[110][2];
int st2[110];
bool vis[200];
int tp,ans,a1;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int i,j,tt;
	scanf("%d\n",&T);
	bool fl;
	while(T--)
	{
		fl=0;
		memset(num,0,sizeof(num));
		memset(tmp,0,sizeof(tmp));
		memset(vis,0,sizeof(vis));
		memset(t1,0,sizeof(t1));
		tp=0;ans=0;
		scanf("%d%s\n",&len,t1);
		for(i=1;i<=len;++i)
			cin.getline(tmp[i],200);
		for(i=1;i<=len;++i)
		{
			if(tmp[i][0]=='F')
			{
				if(vis[tmp[i][2]])
				{
					fl=1;
					break;
				}
				else
				{
					vis[tmp[i][2]]=1;
					st[++tp]=tmp[i][2];
					if(tmp[i][4]=='n')
					{
						num[i][0]=-1;
						j=5;
					}
					else
						for(j=4;tmp[i][j]>='0'&&tmp[i][j]<='9';++j)
						{
							num[i][0]*=10;
							num[i][0]+=tmp[i][j]-'0';
						}
					++j;
					if(tmp[i][j]=='n')
						num[i][1]=-1;
					else
						for(;tmp[i][j]>='0'&&tmp[i][j]<='9';++j)
						{
							num[i][1]*=10;
							num[i][1]+=tmp[i][j]-'0';
						}
				}
			}
			if(tmp[i][0]=='E')
			{
				if(tp<=0)
				{
					fl=1;
					break;
				}
				vis[st[tp--]]=0;
			}
		}
		if(fl||tp>0)
		{
			puts("ERR");
			continue;
		}
		tp=0;
		for(i=1;i<=len;++i)
		{
			if(tmp[i][0]=='F')
			{
				if(num[i][0]==-1&&num[i][1]==-1)	tt=0;
				if(num[i][0]==-1&&num[i][1]!=-1)	tt=-1;
				if(num[i][0]!=-1&&num[i][1]==-1)	tt=1;
				if(num[i][0]!=-1&&num[i][1]!=-1)
				{
					if(num[i][0]<=num[i][1])
						tt=0;
					else
						tt=-1;
				}
				++tp;
				if(st2[tp-1]==-1)
					st2[tp]=-1;
				else
					st2[tp]=st2[tp-1]+tt;
				ans=max(ans,st2[tp]);
			}
			if(tmp[i][0]=='E')
			{
				--tp;
			}
		}
		a1=0;
		if(t1[2]!='1')
			for(j=4;t1[j]>='0'&&t1[j]<='9';++j)
			{
				a1*=10;
				a1+=t1[j]-'0';
			}
		if(a1==ans)
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}

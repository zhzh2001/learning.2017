#include<cstdio>
#include<queue>
#include<algorithm>
#include<cstring>
using namespace std;
typedef pair<int,int> P;
typedef pair<int,P> P2;
priority_queue<P,vector<P>,greater<P> > q;
struct E
{
	int to,d,nxt;
}e[200100],e2[200100];
int f1[100100],f2[100100],ne2,ne;
bool vis[100100];
int dis[100100];
int n,m,k,p,T,ans,dd;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int i,a,b,c,u;
	P t;P2 t2;
	scanf("%d",&T);
	while(T--)
	{
		memset(f1,0,sizeof(f1));
		memset(f2,0,sizeof(f2));
		ne=ne2=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(i=1;i<=m;i++)
		{
			scanf("%d%d%d",&a,&b,&c);
			e2[++ne2].to=a;
			e2[ne2].d=c;
			e2[ne2].nxt=f2[b];
			f2[b]=ne2;
			e[++ne].to=b;
			e[ne].d=c;
			e[ne].nxt=f1[a];
			f1[a]=ne;
		}
		memset(dis,0x3f,sizeof(dis));
		dis[n]=0;
		memset(vis,0,sizeof(vis));
		q.push(P(0,n));
		while(!q.empty())
		{
			t=q.top();q.pop();u=t.second;
			if(vis[u])	continue;
			vis[u]=1;
			for(i=f2[u];i!=0;i=e2[i].nxt)
				if(dis[e2[i].to]>dis[u]+e2[i].d)
				{
					dis[e2[i].to]=dis[u]+e2[i].d;
					q.push(P(dis[e2[i].to],e2[i].to));
				}
		}
		dd=dis[1];
		priority_queue<P2,vector<P2>,greater<P2> > q2;
		while(!q2.empty())	q2.pop();
		ans=0;
		q2.push(P2(dis[1],P(0,1)));
		while(!q2.empty())
		{
			t2=q2.top();q2.pop();u=t2.second.second;
			if(u==n)
			{
				if(t2.first>dd+k)	break;
				++ans;
				if(ans>=p)	ans-=p;
			}
			for(i=f1[u];i!=0;i=e[i].nxt)
				q2.push(P2(t2.second.first+e[i].d+dis[e[i].to],P(t2.second.first+e[i].d,e[i].to)));
		}
		printf("%d\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;

int T,n,ans,cnt,g,f,x,y,mx,i,l,j,h[1000];
char s[1000],c[1000],st[1000];

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for (scanf("%d",&T),h[0]=0;T;--T)
	{
		scanf("%d%s",&n,s+1);
		mx=ans=cnt=f=0;
		for (i=1;i<=n;++i)
		{
			scanf("%s",c);
			if (c[0]=='E')
			{
				if (mx<ans) mx=ans;
				if (h[cnt]==2) --ans;
				--cnt;
			}
			else
			{
				scanf("%s",c);
				if (!f)
				{
					for (j=1;j<=cnt;++j)
						if (st[j]==c[0]) { f=1; break; }
					if (!f) st[++cnt]=c[0];
				}
				x=y=0;
				scanf("%s",c+1);
				if (c[1]^'n')
					for (j=1;j<=strlen(c+1);++j)
						if (c[j]>47 && c[j]<58) x=x*10+c[j]-48;
				scanf("%s",c+1);
				if (c[1]^'n')
					for (j=1;j<=strlen(c+1);++j)
						if (c[j]>47 && c[j]<58) y=y*10+c[j]-48;
				if (!f && h[cnt-1]>=0)
				{
					if (x && !y) ++ans,h[cnt]=2;
					else if (!x && !y) h[cnt]=1;
						 else if (x<=y) h[cnt]=0;
						 	  else h[cnt]=-1;
				}
			}
		}
		if (f || cnt) puts("ERR");
		else
		{
			g=0; l=strlen(s+1);
			for (i=1;i<=l;++i)
				if (s[i]=='n') g=1;
			if (g)
			{
				for (i=1;i<=l;++i)
					if (s[i]>47 && s[i]<58) break;
				if (i>l)
					if (mx^1) puts("No");
					else puts("Yes");
				else
				{
					for (g=s[i]-48,++i;i<=l;++i)
						if (s[i]>47 && s[i]<58) g=g*10+s[i]-48;
					if (g^mx) puts("No");
					else puts("Yes");
				}
			}
			else
				if (mx) puts("No");
				else puts("Yes");
		}
	}
	return 0;
}

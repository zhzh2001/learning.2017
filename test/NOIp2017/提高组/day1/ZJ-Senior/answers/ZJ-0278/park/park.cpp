#include<cstdio>
const int N=100005,M=200005;
int T,n,m,p,k,i,j,cnt,x,y,z,l,r,ans,q[1000000],hed[N],inq[N],nxt[M],w[M],v[M],dis[N],f[N][151];

int read()
{
	int x=0; char ch=getchar();
	while (ch<48 || ch>57) ch=getchar();
	for (;ch>47 && ch<58;ch=getchar()) x=x*10+ch-48;
	return x;
}

void add(int x,int y,int z) { v[++cnt]=y,w[cnt]=z,nxt[cnt]=hed[x],hed[x]=cnt; }

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for (T=read();T;--T)
	{
		n=read(),m=read(),k=read(),p=read();
		for (i=1;i<=n;++i) hed[i]=inq[i]=0; cnt=0;
		for (i=1;i<=n;++i) dis[i]=1000000000;bool flag=0;
		for (i=1;i<=m;++i)
		{
			x=read(),y=read(),z=read(),add(x,y,z);
			if (z) flag=1;
		}
		if (!flag) { printf("-1\n"); continue; }
		l=0,q[r=1]=1; dis[1]=0; inq[1]=1;
		for (;l^r;)
		{
			x=q[++l]; inq[x]=0;
			for (i=hed[x];i;i=nxt[i])
				if (dis[x]+w[i]<dis[y=v[i]])
				{
					dis[y]=dis[x]+w[i];
					if (!inq[y]) inq[y]=1,q[++r]=y;
				}
		}
		for (i=1;i<=n;++i)
			for (j=0;j<=k;++j) f[i][j]=0;
		f[1][0]=1;
		q[1]=1;l=0,r=1;
		for (i=1;i<=n;++i) inq[i]=0; inq[1]=1;
		while (l^r)
		{
			x=q[++l]; inq[x]=0;
			for (i=hed[x];i;i=nxt[i])
				for (j=0;j<=k;++j)
					if (f[x][j])
					{
						y=dis[v[i]]-dis[x]-j-w[i];
						if (y>=0 && y<=k)
						{
							if ((f[v[i]][y]+=f[x][j])>=p) f[v[i]][y]-=p;
							if (!inq[v[i]]) q[++r]=v[i],inq[v[i]]=1;
						}
					}
		}
		printf("%d\n",f[n][0]);
	}
	return 0;
}

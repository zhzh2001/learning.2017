var
n,m,max,min,s1,s2:int64;
ans:qword;
begin
assign(input,'math.in');reset(input);
assign(output,'math.out');rewrite(output);
 readln(n,m);
 if n>m then
  begin
   max:=n;
   min:=m;
   s1:=n div m;
   s2:=n mod m;
  end
   else
    begin
     max:=m;
     min:=n;
     s1:=m div n;
     s2:=m mod n;
    end;
   ans:=max+min*(s1-1)+s2;
   writeln(ans);
   close(input);
   close(output);
end.
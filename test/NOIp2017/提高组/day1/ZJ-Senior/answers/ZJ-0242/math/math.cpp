#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
using namespace std;
typedef long long LL;
inline LL read()
{
	LL kk=0,f=1;
	char cc=getchar();
	while(cc<'0'||cc>'9'){if(cc=='-')f=-1;cc=getchar();}
	while(cc>='0'&&cc<='9'){kk=(kk<<1)+(kk<<3)+cc-'0';cc=getchar();}
	return kk*f;
}
LL a,b;LL d;LL mmax;LL da;
bool pan(LL x)
{
	if(x==b||x==a)return 1;
	if(x<d)return 0;
	if(pan(x-a))return 1;
	if(pan(x-b))return 1;
	return 0;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();
	b=read();d=min(a,b);bool flag=1;
	da=max(a,b);
	LL zhi=da;
	while(flag)
	{
		flag=0;
		for(LL x=zhi;x>zhi-da;--x)
		if(!pan(x))
		{
			mmax=x;
			flag=1;
			break;
		}
		zhi+=da;
	}
	printf("%lld",mmax);
}

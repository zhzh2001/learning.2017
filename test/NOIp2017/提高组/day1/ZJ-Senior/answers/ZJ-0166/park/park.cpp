#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;

ll t,n,m,k,p,a,b,c,sx,cnt,ans,sum,apr[2010],val[2010],nex[2010],head[2010],dist[2010][2010];
bool bo;

void csh()
{
	ans=0;
	sum=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			dist[i][j]=2147483647;
	for(int i=1;i<=n;i++)
		dist[i][i]=0;
}

void addedge()
{
	scanf("%lld%lld%lld",&a,&b,&c);
	dist[a][b]=c;
	cnt++;
	apr[cnt]=b;
	val[cnt]=c;
	nex[cnt]=head[a];
	head[a]=cnt;
}

void dfs(int x)
{
	if(x==n)
		ans=ans+1;
	if(ans>10*p)
	{
		bo=false;
		return;
	}
	for(int u=head[x];u;u=nex[u])
	{
		int v=apr[u];
		if(sum+val[u]>sx)
			continue;
		sum+=val[u];
		dfs(v);
		sum-=val[u];
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%lld",&t);
	while(t--)
	{
		scanf("%lld%lld%lld%lld",&n,&m,&k,&p);
		csh();
		for(int i=1;i<=m;i++)
			addedge();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int kk=1;kk<=n;kk++)
					dist[i][j]=min(dist[i][kk]+dist[kk][j],dist[i][j]);
		sx=dist[1][n]+k;
		bo=1;
		dfs(1);
		if(!bo)
			printf("-1\n");
		else
			printf("%lld\n",ans%p);
	}
}

#include<cstdio>
#include<cstring>
#include<stack>
#include<iostream>
using namespace std;

stack<char> S;
int t,l,fzd,now,ans;
bool bxl,err,used[200],yjj[200];
char dr[100],bky;

void csh()
{
	while(!S.empty())
		S.pop();
	err=0;
	fzd=0;
	now=0;
	ans=0;
	bxl=0;
	memset(used,0,sizeof(used));
	memset(yjj,0,sizeof(yjj));
	memset(dr,0,sizeof(dr));
}

void dof()
{
	cin>>dr;
	char xhbl=dr[0];
	char dr1[10],dr2[10];
	int ksds=0,jsds=0;
	cin>>dr1;
	cin>>dr2;
	int i=0;
	while(dr1[i]<='9'&&dr1[i]>='0')
		ksds=ksds*10+dr1[i]-48,i++;
	i=0;
	while(dr2[i]<='9'&&dr2[i]>='0')
		jsds=jsds*10+dr2[i]-48,i++;
	if(used[xhbl])
	{
		err=1;
		return;
	}
	if(!bxl)
	{
		if(dr1[0]!='n')
		{
			if(dr2[0]!='n')
			{
				if(jsds<ksds)
					bxl=1,bky=xhbl;
			}
			if(dr2[0]=='n')
				now++,yjj[xhbl]=1;
		}
		else
			if(dr2[0]!='n')
				bxl=1,bky=xhbl;
	}
	used[xhbl]=1;
	S.push(xhbl);
}

void doe()
{
	if(S.empty())
	{
		err=1;
		return;
	}
	int tt=S.top();
	used[tt]=0;
	if(tt==bky)
		bxl=0;
	S.pop();
	if(!bxl)
		ans=max(ans,now);
	if(yjj[tt])
		now--,yjj[tt]=0;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		csh();
		scanf("%d",&l);
		cin>>dr;
		if(dr[2]=='1')
			fzd=0;
		else
		{
			int i=4;
			while(dr[i]!=')')
				fzd=dr[i]-48+fzd*10,i++;
		}
		for(int i=1;i<=l;i++)
		{
			cin>>dr;
			if(dr[0]=='F'&&err)
				cin>>dr,cin>>dr,cin>>dr;
			if(!err)
			{
				if(dr[0]=='F')
				{
					dof();
					if(err)
						printf("ERR\n");
				}
				else
				{
					doe();
					if(err)
						printf("ERR\n");
				}
			}
		}
		if(!err)
		{
			if(S.empty())
			{
				if(ans==fzd)
					printf("Yes\n");
				else
					printf("No\n");
			}
			else
				printf("ERR\n");
		}
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
#define rep(i,a,b) for(int i=a,i##_END_=b;i<i##_END_;++i)
#define per(i,a,b) for(int i=(b)-1,i##_BEGIN_=a;i>=i##_BEGIN_;--i)
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b;
	cin>>a>>b;
	if(a>b) swap(a,b);
	cout<<(b*(a-1)-a);
	return 0;
}

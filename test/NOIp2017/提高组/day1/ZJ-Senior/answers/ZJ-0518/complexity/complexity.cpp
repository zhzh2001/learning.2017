#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
#define rep(i,a,b) for(int i=a,i##_END_=b;i<i##_END_;++i)
#define per(i,a,b) for(int i=(b)-1,i##_BEGIN_=a;i>=i##_BEGIN_;--i)
using namespace std;
const int M=100;
typedef long long ll;
typedef pair<int,int> pii;
char str[M+5][M+5],ch[M+5];
int stk[M+5],w;
bool mark[30];
char q[M+5];
bool chk_err(int l)
{
	int top=0;
	rep(i,0,27) mark[i]=0;
	rep(i,0,l)
	{
		if(str[i][0]=='F')
		{
			if(mark[str[i][2]-'a']) return 1;
			q[top++]=str[i][2];
			mark[str[i][2]-'a']=1;
		}
		else
		{
			top--;
			mark[q[top]-'a']=0;
		}
	}
	if(top) return 1;
	return 0;
}
int p;
int getnum(int l)
{
	if(str[l][p]=='n')
	{
		p+=2;
		return 101;
	}
	int x=str[l][p]-'0';
	while(str[l][p+1]>='0'&&str[l][p+1]<='9')
	{
		x=x*10+(str[l][p+1]-'0');
		p++;
	}
	p+=2;
	return x;
}
void solve(int l)
{
	if(chk_err(l))
	{
		puts("ERR");
		return;
	}
	int mx=0,nw=0,top=0;
	rep(i,0,l)
	{
		if(str[i][0]=='F')
		{
			p=4;
			int a=getnum(i);
			int b=getnum(i);
//			dig("a  b    %d %d\n",a,b);
			if(a==101&&b==101)
			{//两个n 
				stk[top++]=0;
			}
			else if(a>b)
			{//a>b 循环跳过 
				while(str[i][0]!='E'&&i<l) i++;
				i--;
			}
			else if(b==101)
			{//b为n，复杂度增加 
				stk[top++]=1;
				nw++;
				if(nw>mx) mx=nw;
			}
			else stk[top++]=0;
			//a<=b,且都不是n，复杂度没变，循环继续 
		}
		else
		{//循环减少 
			top--;
			nw-=stk[top];
		}
	}
//	dig("%d %d\n",mx,w);
	if(mx==w) puts("Yes");
	else puts("No");
}
int get_w()
{
	bool f=0;
	int x=0;
	rep(i,0,strlen(ch))
	{
		if(ch[i]=='n') f=1;
		if(ch[i]>='0'&&ch[i]<='9') x=x*10+ch[i]-'0';
	}
	if(!f) x=0;
	return x;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int T,l;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d",&l);
		scanf("%s\n",ch);
		w=get_w();
		char c;int t=0;
//		dig("w      %d\n",w);
		rep(i,0,l)
		{
			t=0;
			while((c=getchar())!='\n')
			{
				str[i][t++]=c;
			}
			str[i][t]='\0';
		}
		solve(l);
	}
	return 0;
}

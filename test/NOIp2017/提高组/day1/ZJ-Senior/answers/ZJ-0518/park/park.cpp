#include <cstdio>
#include <cstring>
#include <vector>
#include <queue>
#include <cassert>
#include <iostream>
#include <algorithm>
#define rep(i,a,b) for(int i=a,i##_END_=b;i<i##_END_;++i)
#define per(i,a,b) for(int i=(b)-1,i##_BEGIN_=a;i>=i##_BEGIN_;--i)
#define fi first
#define se second
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
const int E=200000,V=100000,INF=0x7fffffff;
int P,T,n,m,K;
struct Edge
{
	int to,nxt,val;
	Edge(int a,int b,int c):to(a),nxt(b),val(c){}
	Edge(){}
}edge[E+5];
bool flag;
int dfn[V+5],dfs_clock,low[V+5],scc_id[V+5],top,stk[V+5],scc_cnt;
bool mark[V+5];
int dis[V+5],head[V+5],etot,sz[V+5];
int from[V+5];
void init(int n)
{
	rep(i,1,n+1) mark[i]=0,dis[i]=INF,head[i]=-1;
	etot=0;
}
void add_edge(int u,int v,int w)
{
	edge[etot]=Edge(v,head[u],w);head[u]=etot++;
}
priority_queue<pii>Q;
void Dijkstra(int x)
{
	while(!Q.empty()) Q.pop();
	Q.push(pii(0,x));
	dis[x]=0;
//	dig("BEGINING\n");
	while(!Q.empty())
	{
		x=Q.top().se;
//		dig("%d %d\n",x,dis[x]);
		Q.pop();
		for(int i=head[x];~i;i=edge[i].nxt)
		{
			int to=edge[i].to;
			int v=edge[i].val;
			if(dis[to]>dis[x]+v)
			{
				dis[to]=dis[x]+v;
				from[to]=x;
				Q.push(pii(-dis[to],to));
			}
		}
	}
//	dig("ENDING\n");
}
void dfs(int x)
{
	low[x]=dfn[x]=++dfs_clock;
	stk[++top]=x;
	for(int i=head[x];~i;i=edge[i].nxt)
	{
		int to=edge[i].to;
		if(edge[i].val!=0) continue;
		if(!dfn[to])
		{
			dfs(to);
			low[x]=min(low[x],low[to]);
		}
		else if(!scc_id[to]) low[x]=min(low[x],dfn[to]);
	}
	if(low[x]==dfn[x])
	{
		++scc_cnt;
		while(low[stk[top]]==low[x])
		{
			sz[scc_cnt]++;
			scc_id[stk[top]]=scc_cnt;
			top--;
		}
	}
}
void judge_1()
{
	flag=0;
	scc_cnt=top=dfs_clock=0;
	rep(i,1,n+1) scc_id[i]=sz[i]=dfn[i]=0;
	rep(i,1,n+1) if(!dfn[i]) dfs(i);
}
int dp[V+5][55];
void add(int&x,int y)
{
	x+=y;
	if(x>=P) x-=P;
}
bool mark2[V+5][55];
void D(int x)
{
	while(!Q.empty()) Q.pop();
	Q.push(pii(0,x));
	rep(i,1,n+1) rep(j,0,K+1) dp[i][j]=0;
	dp[x][0]=1%P;
//	dig("BEGIN\n");
	while(!Q.empty())
	{
		int x=Q.top().se;
		int d=-Q.top().fi;
//		dig("%d %d\n",x,d-dis[x]);
		Q.pop();
		for(int i=head[x];~i;i=edge[i].nxt)
		{
			int to=edge[i].to,v=edge[i].val;
			if(d+v<=dis[to]+K)
			{
//				dig("(%d,%d) <- (%d,%d)\n",to,d+v-dis[to],x,d-dis[x]);
//				dig("add %d %d\n",dp[to][d+v-dis[to]],dp[x][d-dis[x]]);
				if(mark2[x][d-dis[x]]) mark2[to][d+v-dis[to]]=1;
				add(dp[to][d+v-dis[to]],dp[x][d-dis[x]]);
//				dig("after %d\n",dp[to][d+v-dis[to]]);
				Q.push(pii(-d-v,to));
			}
		}
	}
//	dig("END\n");
}
int a[E+5],b[E+5],c[E+5];
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);


	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%d%d%d",&n,&m,&K,&P);
		init(n);
		rep(i,0,m)
		{
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			add_edge(a[i],b[i],c[i]);//����� 
		}
		judge_1();
		init(n);
//		rep(i,1,n+1) dig("%d ",scc_id[i]);
//		dig("\n");
		rep(i,0,m)
		{
			add_edge(scc_id[a[i]],scc_id[b[i]],c[i]);
		}
//		if(judge_1())
//		{
//			puts("-1");
//			continue;
//		}
		Dijkstra(scc_id[1]);
		int mi=dis[scc_id[n]];
		int v=scc_id[n];
		bool f=1;
		while(v)
		{
			if(sz[scc_id[v]]>1)
			{
				puts("-1");
				f=0;
				break;
			}
			v=from[v];
		}
		rep(i,1,scc_cnt+1) rep(j,0,K+1) mark2[i][j]=(sz[i]>1);
		if(!f) continue;
		D(scc_id[1]);
		ll ans=0;
		rep(i,0,K+1)
		{
			if(mark2[scc_id[n]][i])
			{
				f=0;
				puts("-1");
				break;
			}
			ans=(ans+dp[scc_id[n]][i])%P;
		}
		rep(i,1,n+1) from[i]=0;
		if(!f) continue;
		cout<<ans<<endl;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 1000000000
#define eps 1e-8
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
const int maxm=200005,maxn=100005;
int n,m,K,mod,len,Min;
int vet[maxm],Next[maxm],head[maxn],road[maxm];
int dis[maxn];
int map[1005][1005];
bool flag[maxn];
bool ok;
struct node{
	int x,y,z;
}edge[maxm];
queue<int> q;
void add(int u,int v,int w){
	vet[++len]=v;
	Next[len]=head[u];
	head[u]=len;
	road[len]=w;
}
void spfa(int st){
	rep(i,1,n) dis[i]=INF;
	Clear(flag,0);
	while (!q.empty()) q.pop();
	q.push(st);
	dis[st]=0;
	flag[st]=1;
	while (!q.empty()){
		int u=q.front();
		q.pop();
		flag[u]=0;
		for (int e=head[u];e;e=Next[e]){
			int v=vet[e];
			if (dis[v]>dis[u]+road[e]){
				dis[v]=dis[u]+road[e];
				if (!flag[v]){
					flag[v]=1;
					q.push(v);
				}
			}
		}
	}
}
void bfs(int st){
	while (!q.empty()) q.pop();
	rep(i,1,n) flag[i]=0;
	q.push(st);
	flag[st]=1;
	while (!q.empty()){
		int u=q.front();
		q.pop();
		for (int e=head[u];e;e=Next[e]){
			int v=vet[e];
			if (flag[v]) continue;
			flag[v]=1;
			map[st][v]=min(map[st][v],map[st][u]+road[e]);
			q.push(v);
		}
	}
}
/*void find(int st,int u,int dis){
	if (ok) return;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		if (v==st&&dis+road[e]==0){
			ok=1;
			return;
		}
		find(st,v,dis+road[e]);
	}
}*/
int dfs(int u,int dis){
	if (dis+map[u][n]>map[1][n]+K) return 0;
	if (u==n) return 1;
	int ans=0;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		ans=(ans+dfs(v,dis+road[e]))%mod;
	}
	return ans;
}
int solve(int u,int s){
	if (s+dis[u]>dis[1]+K) return 0;
	if (u==n) return 1;
	int ans=0;
	for (int e=head[u];e;e=Next[e]){
		int v=vet[e];
		ans=(ans+solve(v,s+road[e]))%mod;
	}
	return ans;
}
void work(){
	Clear(head,0);
	len=0;
	rep(i,1,m){
		int u=read(),v=read(),w=read();
		add(v,u,w);
		edge[i].x=u,edge[i].y=v,edge[i].z=w;
	}
	spfa(n);
	Clear(head,0);len=0;
	rep(i,1,m)
		add(edge[i].x,edge[i].y,edge[i].z);
	printf("%d\n",solve(1,0));
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while (T--){
		n=read(),m=read(),K=read(),mod=read();
		if (n>1000){
			work();
			continue;
		}
		rep(i,1,n)
			rep(j,1,n)
				if (i!=j) map[i][j]=INF;
		Clear(head,0);len=0;
		rep(i,1,m){
			int u=read(),v=read(),w=read();
			map[u][v]=w;
			add(u,v,w);
		}
		rep(i,1,n)
			bfs(i);
		rep(i,1,n){
			ok=0;
			rep(j,1,n)
				if (map[i][j]==0&&i!=j)
					for (int e=head[j];e;e=Next[e]){
						int v=vet[e];
						if (v==i&&road[e]==0){
							ok=1;
							break;
						}
					}
			if (ok) break;
		}
		if (ok){
			puts("-1");
			continue;
		}
		printf("%d\n",dfs(1,0));
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
#define eps 1e-8
#define sqr(x) (x)*(x)
using namespace std;
ll read(){
	ll x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
ll a,b;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read(),b=read();
	if (a>b) swap(a,b);
	if (a==1){
		puts("0");
		return 0;
	}
	ll c=(ll)(a-1)*b;
	c=c-a;
	printf("%lld\n",c);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 5000
#define eps 1e-8
#define sqr(x) (x)*(x)
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
bool vis[30];
char s[10],number[10];
int Time,Max,L;
bool flag;
void dfs(int t,int Time){
	int ch;
	scanf("%s",s+1);
	if (vis[s[1]-'a'])
		flag=1;
	vis[s[1]-'a']=1;
	ch=s[1]-'a';
	int d=-1;
	int a=0,b=0;
	scanf("%s",number+1);
	if (number[1]=='n') d=-INF;
		else{
			int len=strlen(number+1);
			rep(i,1,len) a=a*10+number[i]-'0';
		}
	scanf("%s",number+1);
	if (number[1]=='n'&&d==-1) d=1;
		else{
			int len=strlen(number+1);
			rep(i,1,len) b=b*10+number[i]-'0';
		}
	if (a>b&&d==-1) d=-INF;
		else if (a<=b&&d==-1) d=0;
	Time+=d;
	Max=max(Max,Time);
	while (1){
		if (L==0){
			flag=1;
			return;
		}
		L--;
		scanf("%s",s+1);
		if (s[1]=='E'){
			vis[ch]=0;
			return;
		}else dfs(t+1,Time);
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=read();
	while (T--){
		Clear(vis,0);
		L=read();
		scanf("%s",s+1);
		int len=strlen(s+1);
		if (s[3]=='1') Time=0;
			else{
				int w=0;
				rep(i,5,len-1)
					w=w*10+s[i]-'0';
				Time=w;
			} 
		Max=0;
		flag=0;
		if (L%2==1) flag=1;
		while (L){
			scanf("%s",s+1);
			L--;
			if (s[1]=='E') flag=1;
			if (s[1]=='F')
				dfs(1,0);
		}
		if (flag) puts("ERR");
			else {
					if (Time==Max) puts("Yes");
						else puts("No");
				 }
	}
	return 0;
}

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <list>
#include <deque>
#include <utility>

using namespace std;
int T;
int scnt,ans;//scnt 判断堆栈是否吻合  ans，答案
bool wrong;
struct node{
	int var;
	int curcom;//0 不被执行 1 常数次 2 n次
}Sta[10010];
int used[400];//存储变量的字符
int N;//代码长度
char ss[100010];//时间复杂度
int len;
int fuzadu;//时间复杂度
// code
void init()
{
	wrong =false;
	fuzadu=0;
	scnt=0;
	ans=0;
	//len=0;
	memset(used,0,sizeof used);
	//memset(Sta,0,sizeof Sta);
}
int get_com()
{
	if(ss[3]=='1')	return 0;
	int ret=0;
	for(int i=1;i<=len;++i)
		if(isdigit(ss[i]))
			ret=ret*10+ss[i]-'0';
	return ret;
}
void exit_for()
{
	used[Sta[scnt].var]=false;
	--scnt;
	if(scnt<0)	wrong=true;
	//printf("exit for %d\n",scnt+1);
}
int num_or_var()
{
	scanf("%s",ss+1);
	int len=strlen(ss+1);
	if(isdigit(ss[1]))
	{
		int ret=0;
		for(int i=1;i<=len;++i)
			if(isdigit(ss[i]))
				ret=ret*10+ss[i]-'0';
		return ret;
	}
	else return 100010;
}
void into_for()
{
	char var;
	scanf("%1s",&var);
	if(used[var])	wrong=true;
	used[var]=true;
	Sta[++scnt].var=var;
	
	int vv1,vv2;
	vv1=num_or_var();
	vv2=num_or_var();
						//cout<<"For "<<vv1<<" to "<<vv2<<endl;

	if(vv1<=10010&&vv2<=10010)
	{
		if(vv1<=vv2)	Sta[scnt].curcom=1;
		else 			Sta[scnt].curcom=0;
	}
	else if(vv1<=10010&&vv2>10010)
	{
		Sta[scnt].curcom=2;
	}
	else if(vv1>10010&&vv2<=10010)
	{
		Sta[scnt].curcom=0;
	}
	else if(vv1>10010&&vv2>10010)
	{
		Sta[scnt].curcom=1;
	}
	//printf("curcom=%d    %d\n",Sta[scnt].curcom,scnt);

}
void calc_com()
{
	int ret=0;
	for(int i=scnt;i>0;--i)
	{
		if(Sta[i].curcom==2)
			++ret;
		else if(Sta[i].curcom==0)
			ret=0;
	}
	ans=max(ans,ret);
	//printf("at layer of %d ret=%d\n",scnt,ret);
}
void work()
{
	scanf("%d",&N);
	scanf("%s",ss+1);
								//puts(ss+1);
	len=strlen(ss+1);
	fuzadu=get_com();
								//printf("N is %d comple is %d\n",N,fuzadu);
	char command;							
	while(N--)
	{
		scanf("%1s",&command);
		//printf("i is %d\n",N);
		if(!wrong)
		{
			if(command=='E')
			{
				exit_for();
			}
			else if(command=='F')
			{
				into_for();
				calc_com();
			}		
		}
		else
		{
			char t=getchar();
			while(t!='\n')	t=getchar();
		}
	}
	if(scnt!=0)	wrong=true;
	if(wrong)	puts("ERR");
	else if(ans==fuzadu)	puts("Yes");
	else puts("No");
	//printf("exit\n");
}
int main()
{
	freopen("complexity.in","r+",stdin);
	freopen("complexity.out","w+",stdout);
	scanf("%d",&T);
	while(T--)
	{
		init();
		work();
	}
	return 0;
}

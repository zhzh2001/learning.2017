#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>

#include <algorithm>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <vector>
#include <list>
#include <deque>
#include <utility>

using namespace std;
typedef long long LL;
LL a,b;
// code

int main()
{
	freopen("math.in","r+",stdin);
	freopen("math.out","w+",stdout);
	scanf("%lld%lld",&a,&b);
	printf("%lld\n",a*b-a-b);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;
int n,m,k,p,head[100001],f[100001],dis[100001],dp[1001][60001];
typedef pair<int,int> P;
struct Edge{
	int to,next,v;
}e[200001];
void read(int& x){
	x=0;
	int y=1;
	char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	x=x*y;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int i,a,b,c,s,T;
	P t;
	read(T);
	while (T--){
	memset(dis,0x3f,sizeof(dis));
	memset(f,0,sizeof(f));
	memset(head,0,sizeof(head));
	memset(dp,0,sizeof(dp));
	read(n);
	read(m);
	read(k);
	read(p);
	for (i=1;i<=m;++i){
		read(a);
		read(b);
		read(c);
		e[i]=(Edge){b,head[a],c};
		head[a]=i;
	}
	if (!k){
		dis[1]=0;
		f[1]=1;
		priority_queue <P,vector<P>,greater<P> > q;
		q.push(P(0,1));
		while (!q.empty()){
			t=q.top();
			q.pop();
			s=t.second;
			if (t.first>dis[s]) continue;
			for (i=head[s];i;i=e[i].next){
				a=e[i].to;
				if (dis[a]==dis[s]+e[i].v) f[a]=(f[a]+f[s])%p;
				else if (dis[a]>dis[s]+e[i].v){
					dis[a]=dis[s]+e[i].v;
					f[a]=f[s];
					q.push(P(dis[a],a));
				}
			}
		}
		printf("%d\n",f[n]);
	}else{
		dis[1]=0;
		dp[1][0]=1;
		priority_queue <P,vector<P>,greater<P> > q;
		q.push(P(0,1));
		while (!q.empty()){
			t=q.top();
			q.pop();
			s=t.second;
			if (t.first>dis[s]) continue;
			for (i=head[s];i;i=e[i].next){
				a=e[i].to;
				if (dis[a]==dis[s]+e[i].v) dp[a][dis[a]]=(dp[a][dis[a]]+dp[s][dis[s]])%p;
				else if (dis[a]>dis[s]+e[i].v){
					dis[a]=dis[s]+e[i].v;
					dp[a][dis[a]]=(dp[a][dis[a]]+dp[s][dis[s]])%p;
					q.push(P(dis[a],a));
				}else if (dis[s]+e[i].v<=dis[a]+k){
					dp[a][dis[s]+e[i].v]=(dp[a][dis[s]+e[i].v]+dp[s][dis[s]])%p;
				}
			}
		}
		int ans=0;
		for (i=dis[n];i<=dis[n]+k;++i) ans=(ans+dp[n][i])%p;
		printf("%d\n",ans);
	}
	}
	return 0;
}

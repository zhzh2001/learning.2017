#include<bits/stdc++.h>
using namespace std;

#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;i++)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;i--)
#define LL long long
#define Sz(a) sizeof(a)
#define mcl(a,b) memset(a,b,Sz(a))


int a,b;

struct p60{
	bool check(LL num){
		REP(i,0,num/a+1){
			LL res=num-1LL*i*a;
			if(res<0)break;
			if(res%b==0)return 0;
		}
		return 1;
	}
	void solve(){
		int ans=a*b-1;
		DREP(i,a*b-1,1){
			if(check(i)){
				ans=i;
				break;
			}
		}
		cout<<ans<<endl;
	}
}p60;

int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
//	printf("%d\n",Sz());
	cin>>a>>b;
	if(a<=10000 && b<=10000)p60.solve();
	else {
		LL ans=a+b+1;
		cout<<ans<<endl;
	}
	return 0;
}

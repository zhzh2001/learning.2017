#include<bits/stdc++.h>
using namespace std;

#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;i++)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;i--)
#define LL long long
#define INF 0x3f3f3f3f
#define Sz(a) sizeof(a)
#define mcl(a,b) memset(a,b,Sz(a))

#define N 105

int S[N]; 
char str[10];
int L;
bool mark[200];
int cnt[N];

int work(){
	int top=1,ans=0;
	S[1]=1;
	bool flag=0;
	REP(i,1,L){
		char c[5];
		scanf("%s",c);
		if(c[0]=='F'){
			char d[4],x[4],y[4];
			scanf("%s%s%s",d,x,y);
			if(mark[d[0]])flag=1;
			mark[d[0]]=1;
			top++;
			if(cnt[top-1]){cnt[top]=1;S[top]=1;continue;}
			
			if(x[0]==y[0])S[top]=1;
			else if(x[0]=='n')cnt[top]=1,S[top]=1;
			else S[top]=2;
		}
		else {
			S[top-1]*=S[top];
			ans=max(ans,S[top-1]);
//			printf("top=%d S[top-1]=%d S[top]=%d\n",top,S[top-1],S[top]);
			cnt[top]=0;
			top--;
			if(top==1)S[1]=1;
		}
	}
	if(top!=1 || flag)return -1;
	else return ans;
}

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;cin>>T;
	while(T--){
		mcl(mark,0);
		scanf("%d%s",&L,str);
		int sl=strlen(str),w;
		
		if(sl==4)w=0;
		else w=str[sl-2]-'0';
//		printf("w=%d\n",w);
		int f=work(); 
//		printf("f=%d\n",f);
		if(f==-1)puts("ERR");
		else {
			if(f==(1<<w))puts("Yes");
			else puts("No");
		}
		
	}
	return 0;
}

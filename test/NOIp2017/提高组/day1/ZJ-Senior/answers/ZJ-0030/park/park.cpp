#include<bits/stdc++.h>
using namespace std;

#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;i++)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;i--)
#define LL long long
#define INF 0x3f3f3f3f
#define Sz(a) sizeof(a)
#define mcl(a,b) memset(a,b,Sz(a))

#define N 1005

int n,m,K,P;

struct node{
	int to,cost;
	bool operator<(const node &_)const{
		return cost>_.cost;
	}
};
vector<node>E[N];

int dis[N],vis[N];
priority_queue<node>Q;

void Dij(){
	while(!Q.empty())Q.pop();
	
	Q.push((node){1,0});
	mcl(vis,0);
	mcl(dis,63);
	dis[1]=0;
	
	while(!Q.empty()){
		node now=Q.top();Q.pop();
		int x=now.to;
		if(vis[x])continue;
		vis[x]=1;
		REP(i,0,E[x].size()-1){
			node y=E[x][i];
			if(vis[y.to])continue;
			if(dis[y.to]>dis[x]+y.cost){
				dis[y.to]=dis[x]+y.cost;
				if(!vis[y.to]){
					Q.push((node){y.to,dis[y.to]});
				}
			}
		}
	}
}
LL ans;
void dfs(int x,int sum){
	if(sum>dis[n]+K)return;
	if(x==n){
		ans++;
		if(ans>=P)ans-=P;
		return;
	}
	REP(i,0,E[x].size()-1){
		node y=E[x][i];
		dfs(y.to,sum+y.cost);
	}
}

bool check(){
	queue<node>Q;
	while(!Q.empty())Q.pop(); 
	
	mcl(vis,-1);
	
	Q.push((node){1,0});
	
	while(!Q.empty()){
		node now=Q.front();Q.pop();
		int x=now.to;
		if(vis[x]!=-1){
			if(vis[x]==now.cost && vis[x]<=dis[n]+K)return 1;
		}
		
		vis[x]=now.cost;
		REP(i,0,E[x].size()-1){
			node y=E[x][i];
				Q.push((node){y.to,now.cost+y.cost});
		}
	}
	return 0;
} 
 
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;cin>>T;
	while(T--){
		cin>>n>>m>>K>>P;
		int f=0;
		REP(i,1,m){
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			E[a].push_back((node){b,c});
			if(!c)f=1;
		}
		if(f && check())puts("-1"); 
		else {
			Dij();
			dfs(1,0);
			printf("%lld\n",ans%P);
		}
	}
	return 0;
}

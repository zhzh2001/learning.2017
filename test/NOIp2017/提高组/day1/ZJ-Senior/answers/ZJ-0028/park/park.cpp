#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
using namespace std;
const int maxM=200009;
const int maxN=100009;
struct Edge{
	int to,nxt,dist;
}edge[maxM];
int first[maxN],nume;
int read(){
	char c = getchar();int  f = 1;int a =0;
	while (c<'0'||c>'9') c = getchar();if(c=='-'){f = -1;c = getchar();}
	while (c>='0'&&c<='9'){a = a*10+c-'0';c = getchar();}
	return a*f;
}
void add_edge(int a,int b,int c){
	edge[nume].to=b;
	edge[nume].dist=c;
	edge[nume].nxt=first[a];
	first[a]=nume++;
}
int q[maxN];
bool vis[maxN];
int dist[maxN],In[maxN],front,rear;
int N,M,K,P;
bool spfa(){
	int u,v;
	memset(dist,0x3f,sizeof(dist));
	memset(vis,false,sizeof(vis));
	memset(In,0,sizeof(In));
	dist[1]=0;
	vis[1]=true;
	front=rear=0;
	q[rear++]=1;
	if(rear==N+1)rear=0;
	while(front!=rear){
		u=q[front++];
		if(front==N+1)front=0;
		vis[u]=false;
		for(int e=first[u];~e;e=edge[e].nxt){
			v=edge[e].to;
			if(dist[v]>=dist[u]+edge[e].dist){
				dist[v]=edge[e].dist+dist[u];
				if(!vis[v]){
					In[v]++;
					if(In[v]>N)	return false;
					vis[v]=true;
					q[rear++]=v;
					if(rear==N+1)rear=0;
				}
			}
		}
	}
	return true;
}
int dp[51][maxN],tmp[maxN];
void solve(int x){
	int u,v;
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=N;i++) tmp[i]=dp[x][i];
	for(int i=1;i<=N;i++){
		if(dp[x][i]!=0){
			q[rear++]=i;
			vis[i]=true;
			if(rear==N+1)rear=0;
		}
	}
	
	while(front!=rear){
		u=q[front++];
//		printf("--%d %d\n",u,dp[x][u]);
		if(front==N+1)front=0;
		for(int e=first[u];~e;e=edge[e].nxt){
			v=edge[e].to;
			if(dist[u]+edge[e].dist==dist[v]){
				dp[x][v] = dp[x][v] + tmp[u];
				if (dp[x][v]>=P)dp[x][v]-=P;
				tmp[v] = tmp[v]+tmp[u];
				if (tmp[v]>=P) tmp[v]-=P;
				if(!vis[v]){
					q[rear++]=v;
					if(rear==N+1)rear=0;
					vis[v]=true;
				}
			}
		}
		tmp[u]=0;
		vis[u]=false;
	}
	
	for (int u=1;u<=N;u++){
		if(dp[x][u]==0)continue;
//		printf("(%d,%d)=%d\n",u,x,dp[x][u]);
		for(int e=first[u];~e;e=edge[e].nxt){
			v=edge[e].to;
			int y=x+dist[u]+edge[e].dist-dist[v];
			if(y==x) continue;
			if(y>K)continue;
			dp[y][v] = dp[y][v] + dp[x][u];
			if(dp[y][v]>=P) dp[y][v]-=P;
 		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
//		printf("%d\n",T);
	while(T--){
		memset(first,-1,sizeof(first));
		nume=0;
		scanf("%d%d%d%d",&N,&M,&K,&P);
//		printf("%d%d%d%d\n",N,M,K,P);
		while(M--){
			int a,b,c;
			a=read();b=read();c=read();
			//scanf("%d%d%d",&a,&b,&c);
			add_edge(a,b,c);
		}
		bool flag=spfa();
		if(!flag){
			puts("-1");
		} else{
			memset(dp,0,sizeof(dp));
			dp[0][1] = 1;
			for(int i=0;i<=K;i++) solve(i);
			
			int sum=0;
			for(int i=0;i<=K;i++){
				sum=(sum+dp[i][N])%P;
			}
			printf("%d\n",sum);
		}
	}
	return 0;
}

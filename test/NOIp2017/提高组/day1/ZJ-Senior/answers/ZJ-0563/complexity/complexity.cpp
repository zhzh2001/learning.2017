#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
using namespace std;
struct node {
	int b,e,nt;
} s[105];
int sta[105];
bool f[300];
int pp=0;
int l;
inline int re() {
	char ch=0;
	int sum=0;
	while (ch!='n' && !(ch>='0'&&ch<='9')) ch=getchar();
	if (ch=='n') return 10000;
	while (ch>='0' && ch<='9') sum=sum*10+ch-'0',ch=getchar();
	return sum;
}
int tj(int p) {
	int sum=0;
	for (int i=1;i<=p;i++) sum+=sta[i];
	return sum;
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	cin >> T;
	while (T--) {
		memset(sta,0,sizeof(sta));
		memset(f,0,sizeof(f));
		string fzd;
		cin >> l >> fzd;
		int p=0;
		//bool flag=true;
		for (int i=1;i<=l;i++) s[i].b=s[i].e=s[i].nt=0;
		for (int i=1;i<=l;i++) {
			char ch=0;
			while (ch!='F' && ch!='E') ch=getchar();
			//
			//cout <<i<<ch<<p<<endl;
			//if (i==31)
			if (ch=='E') {
				//if (p==0) {
					//printf("ERR\n");
					//flag=false;
					//cout<<i<<"err";
					//
					//cout<< flag;
					//break;
				//}
				//else 
				{
					s[sta[p]].nt=i;
					p--;
				}
			}
			else {
				ch=0;
				while (ch<'a' || ch>'z') ch=getchar();
				//if (f[(int)ch]) {
					//printf("ERR\n");
					//flag=false;
					//break;
				//}
			//	else 
			{
					//f[(int)ch]=true;
					sta[++p]=i;
					s[i].b=re();
					s[i].e=re();
				}
			}
			//cout<<p<<endl;
		}
		//if (p>0||!flag) {
			//printf("ERR\n");
			//continue;
	//	}
		pp=1;p=0;
		int ans=0;
		while (pp<=l) {
			while (pp<=l && s[pp].nt==0) pp++,p--;
			if (s[pp].b<=s[pp].e) {
				if (s[pp].e==10000) sta[++p]=1;
				else sta[++p]=0;
				ans=max(ans,tj(p));
				pp++;
			}
			else pp=s[pp].nt;
		}
		if (ans==0) {
			if (fzd[2]=='1') printf("Yes\n");
			else printf("No\n");
		}
		else {
			int k=0,i=0;
			while (fzd[i]<'0' || fzd[i]>'9') i++;
			while (i<fzd.length() && fzd[i]>='0' && fzd[i]<='9') k=k*10+fzd[i]-'0',i++;
			if (k==ans) printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

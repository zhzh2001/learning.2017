var
        a,b,i,ans,n:longint;
function pd(k:longint):boolean;
var
        i,j:longint;
begin
        for i:=0 to k div a do
        for j:=0 to k div b do
        if a*i+b*j=k then exit(false);
        exit(true);
end;
begin
        assign(input,'math.in');reset(input);
        assign(output,'math.out');rewrite(output);
        read(a,b);
        if (a<20)and(b<20) then n:=2000
        else n:=10000;
        for i:=1 to n do
        if pd(i) then ans:=i;
        writeln(ans);
        close(input);
        close(output);
end.

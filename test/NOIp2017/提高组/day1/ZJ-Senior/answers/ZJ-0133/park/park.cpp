#include <queue>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;

const int BUFFER_SIZE=3e7;
char buffer[BUFFER_SIZE],*p=buffer;
int getint() {
	int x=0;
	while (*p>'9' || *p<'0') ++p;
	while (*p<='9' && *p>='0') x=x*10+*(p++)-'0';
	return x;
}


const int INF=1e9;
const int K=51;
const int N=100010;
const int M=200010;

int u[M],v[M],c[M];
struct E{ int to,next,c; }e[M];
int head[N],tote;
void adde(int u,int v,int c) { e[++tote]=(E){v,head[u],c}; head[u]=tote; }

int ind[N*K];
struct E2{ int to,next; }e2[M*K];
int head2[N*K],tote2;
inline void adde2(int u,int v) { e2[++tote2]=(E2){v,head2[u]}; head2[u]=tote2; ++ind[v]; }

int b2[N*K];
void dfs2(int x) {
	b2[x] = 1;
	for (int p=head2[x];p;p=e2[p].next) if (!b2[e2[p].to]) dfs2(e2[p].to);
}

int n,m,k,P;
inline void add(int &a,int b) {
	a += b;
	if (a>P) a-=P;
}

queue<int> q;
int inq[N],d[N];
int SPFA() {
	for (int i=1;i<=n;++i) d[i] = INF, inq[i] = 0;
	inq[1] = 1; q.push(1); d[1] = 0;
	while (!q.empty()) {
		int u=q.front(); q.pop(); inq[u] = 0;
		for (int p=head[u];p;p=e[p].next)
			if (d[e[p].to] > d[u] + e[p].c) {
				d[e[p].to] = d[u] + e[p].c;
				if (!inq[e[p].to]) {
					inq[e[p].to] = 1; q.push(e[p].to);
					if (d[q.back()] > d[q.front()]) swap(q.back(),q.front());
				}
			}
	}
	return d[n];
}


int tot;
int fflag;
int ans[N*K];
int st[N*K],tp;
void solve() {
	tp = 0;
	for (int i=1;i<=tot;++i) if (!ind[i]) st[++tp] = i;
	ans[1] = 1;
	while (tp) {
		int u=st[tp]; --tp;
		for (int p=head2[u];p;p=e2[p].next) {
			add(ans[e2[p].to], ans[u]);
			if (!--ind[e2[p].to]) st[++tp] = e2[p].to;
		}
	}
	int flag=0;
	for (int i=1;i<=tot;++i)
		if (ind[i]) flag=1;
	if (flag) {
		for (int i=1;i<=tot;++i) {
			if (ind[i] && ans[i]) dfs2(i);
		}
		for (int i=0;i<=k;++i) if (b2[n+i*n]) fflag=1;
	}
}

int main() {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	fread(buffer, 1, BUFFER_SIZE, stdin);
	//int T; scanf("%d",&T);
	int T=getint();
	while (T--) {
		fflag=0;
		memset(b2, 0, sizeof(b2));
		memset(head, 0, sizeof(head)); tote = 0;
		memset(head2, 0, sizeof(head2)); tote2 = 0;
		memset(ans, 0, sizeof(ans));
		memset(ind, 0, sizeof(ind));
		//scanf("%d%d%d%d",&n,&m,&k,&P);
		n = getint();m = getint(); k = getint(); P = getint();
		for (int i=1;i<=m;++i) {
			//scanf("%d%d%d",&u[i],&v[i],&c[i]);
			u[i] = getint(); v[i] = getint(); c[i] = getint();
			adde(u[i],v[i],c[i]);
		}
		//dfs(1);
		SPFA();
		for (int i=1;i<=m;++i) {
			int dt = c[i]-(d[v[i]]-d[u[i]]);
			for (int j=0;j+dt<=k;++j) {
				adde2(u[i]+j*n,v[i]+(j+dt)*n);
			}
		}
		tot = n*(k+1);
		solve();
		if (fflag) {
			printf("-1\n");
			continue;
		}
		int res=0;
		//for (int i=0;i<=k;++i) add(res, ans[n][i]);
		for (int i=0;i<=k;++i) add(res, ans[n+i*n]);
		printf("%d\n",res);
	}
	return 0;
}

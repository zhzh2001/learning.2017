#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;

int main() {
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b; scanf("%lld%lld",&a,&b);
	ll m=a*b;
	printf("%lld\n",m-a-b);
	return 0;
}

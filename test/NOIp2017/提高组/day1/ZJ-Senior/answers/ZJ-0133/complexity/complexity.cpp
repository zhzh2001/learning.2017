#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int N=110;
int getint() {
	char ch=getchar(); int x=0;
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch<='9' && ch>='0') x=x*10+ch-'0', ch=getchar();
	return x;
}
int getin() {
	char ch=getchar(); int x=0;
	while (ch!='n' && (ch<'0' || ch>'9')) ch=getchar();
	if (ch=='n') return -1;
	while (ch<='9' && ch>='0') x=x*10+ch-'0', ch=getchar();
	return x;
}
int getvar() {
	char ch=getchar();
	while (ch>'z' || ch<'a') ch=getchar();
	return ch-'a';
}
int comp;
void getcomp() {
	char ch=getchar();
	while (ch!='(') ch=getchar();
	ch=getchar();
	if (ch=='n') {
		comp = getint();
	} else {
		comp = 0;
	}
}
int isF() {
	char ch=getchar();
	while (ch!='E' && ch!='F') ch=getchar();
	return ch=='F';
}
int L;
int fl[N],x[N],y[N],var[N],pr[N];
int calc(int p) {
	if (x[p]>0 && y[p]<0) return 1;
	return 0;
}
int ok(int p) {
	if (x[p]<0 && y[p]>0) return 0;
	if (x[p]>0 && y[p]>0 && x[p]>y[p]) return 0;
	return 1;
}
int used[26];
int solve(int l,int r) {
	if (l==r-1) return calc(l);
	int mx=0;
	for (int i=l+1;i<r;++i) {
		mx = max(mx, solve(i,pr[i]));
		i = pr[i];
	}
	if (!ok(l)) return 0;
	return mx + calc(l);
}
int st[N],tp;
int check() {
	tp = 0;
	for (int i=1;i<=L;++i) {
		if (fl[i]) {
			if (used[var[i]]) return 1;
			++tp; st[tp] = i; used[var[i]] = 1;
		} else {
			if (tp==0) return 1;
			pr[st[tp]] = i; pr[i] = st[tp];
			used[var[st[tp]]] = 0; --tp;
		}
	}
	return tp!=0;
}
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--) {
		memset(used, 0, sizeof(used));
		scanf("%d",&L); getcomp();
		for (int i=1;i<=L;++i) {
			if (isF()) {
				var[i] = getvar();
				x[i] = getin();
				y[i] = getin();
				fl[i] = 1;
			} else {
				fl[i] = 0;
			}
		}
		if (check()) {
			printf("ERR\n");
			continue;
		}
		x[0] = 1; y[0] = 1; fl[0] = 1; pr[0] = L+1;
		fl[L+1] = 0;
		int ans=solve(0,L+1);
		if (ans==comp) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define rep(j,k,l) for (int j=k;j<=l;++j)
#define red(j,k,l) for (int j=k;j>=l;--j)
#define N 10005

using namespace std;
int T,n,t[N];
char s[N],s1[N],s2[N],nm[N];

void add(int pp){ nm[pp]=s[1]; }

bool eq(int k){
	
	if (nm[k]!=s[1]) return 0;
	return 1;
	
}

int main(){
	
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		
		scanf("%d%s",&n,s+1);
		int ipt=0;
		if (s[3]!=1) rep(i,5,strlen(s+1)-1) ipt=ipt*10+s[i]-48;
		int pp=0,fg=1,tme=0,mx=0;
		rep(i,1,n){
			
			char ch=getchar();
			while (ch!='E'&&ch!='F') ch=getchar();
			if (ch=='F'){
				
				rep(i,0,104) s[i]=0,s1[i]=0,s2[i]=0;
				int k,l;char ch=getchar();
				while (ch<'a'||ch>'z') ch=getchar();
				s[1]=ch;
				scanf("%s%s",s1+1,s2+1);
				rep(j,1,pp) if (eq(j)){ fg=0; break; }
				if (s1[1]=='n') k=101;else{ k=0; rep(j,1,strlen(s1+1)) k=k*10+s1[j]-48; }
				if (s2[1]=='n') l=101;else{ l=0; rep(j,1,strlen(s2+1)) l=l*10+s2[j]-48; }
				//printf("---%d %d %d\n",k,l,ipt);
				if (k>100&&l>100){ pp++; add(pp); t[pp]=0; if (t[pp-1]==-1) t[pp]=-1; }
				else if (k>100){ pp++; add(pp); t[pp]=-1; }
				else if (l>100){ pp++; add(pp); t[pp]=1; if (t[pp-1]==-1) t[pp]=-1;else tme+=1; }
				else if (k>l){ pp++; add(pp); t[pp]=-1; }
				else{ pp++; add(pp); t[pp]=0; if (t[pp-1]==-1) t[pp]=-1; }
				mx=max(mx,tme);
				
			}else{
				
				if (pp==0){ fg=0; continue; }
				if (t[pp]==1) tme--;
				pp--;
				
			}
			
		}
		if (pp>0) fg=0;
		if (fg==0) printf("ERR\n");
		else if (mx==ipt) printf("Yes\n");
		else printf("No\n");
		
	}
	return 0;
	
} 

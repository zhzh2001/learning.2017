#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define rep(j,k,l) for (LL j=k;j<=l;++j)
#define red(j,k,l) for (itn j=k;j>=l;--j)
#define N 300005
#define M 500005
#define inf 1000000007
#define LL long long

using namespace std;
LL to[M],sm[M],ne[M],st[N],a[N],b[N],c[N],cnt,T,n,m,p,dis[N];
LL que[N],used[N],f[N][51],ct[N],mod;

void add(LL k,LL l,LL p){ to[++cnt]=l; sm[cnt]=p; ne[cnt]=st[k]; st[k]=cnt; }

void SPFA(LL k){
	
	dis[k]=0;LL hd=0,tl=1;que[1]=k;used[k]=1;
	while (hd!=tl){
		
		hd=(hd+1)%(n+2);
		LL x=que[hd];used[k]=0;
		for (LL i=st[x];i;i=ne[i]) if (dis[to[i]]>dis[x]+sm[i]){
			
			dis[to[i]]=dis[x]+sm[i];
			if (used[to[i]]==0){
				
				used[to[i]]=1;
				tl=(tl+1)%(n+2);
				que[tl]=to[i];
				
			}
			
		}
		
	}
	
}

void bfs(LL k){
	
	LL hd=0,tl=1,ans=0;que[1]=1;used[1]=1;
	while (hd!=tl){
		
		hd=(hd+1)%(n+2);
		LL x=que[hd];used[x]=0;
		//printf("----%lld\n",x);
		rep(i,0,k) if (f[x][i]) for (LL j=st[x];j;j=ne[j]) if (i+dis[to[j]]+sm[j]-dis[x]<=k){
			
			ct[to[j]]++;
			f[to[j]][i+dis[to[j]]+sm[j]-dis[x]]=(f[to[j]][i+dis[to[j]]+sm[j]-dis[x]]+f[x][i])%mod;
			if (used[to[j]]==0){
				
				used[to[j]]=1;
				tl=(tl+1)%(n+2);
				que[tl]=to[j];
				
			}
			if (ct[to[j]]>n*k+2){ printf("-1\n"); return; }
			
		}//else printf("----%lld %lld %lld\n",x,to[j],i+dis[to[j]]+sm[j]-dis[x]);
		if (x==n) rep(i,0,k) ans=(ans+f[n][i])%mod;
		rep(i,0,k) f[x][i]=0;
		
	}
	rep(i,0,k) ans=(ans+f[n][i])%mod;
	printf("%lld\n",ans);
	return;
	
}

int main(){
	
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%lld",&T);
	while (T--){
		
		scanf("%lld%lld%lld%lld",&n,&m,&p,&mod);cnt=0;
		rep(i,1,n) st[i]=0,dis[i]=inf,ct[i]=0;
		rep(i,1,n) rep(j,0,50) f[i][j]=0;f[1][0]=1;
		rep(i,1,m) scanf("%lld%lld%lld",&a[i],&b[i],&c[i]),add(b[i],a[i],c[i]);
		SPFA(n);
		cnt=0;rep(i,1,n) st[i]=0,used[i]=0,ct[i]=0;rep(i,1,m) add(a[i],b[i],c[i]);
		bfs(p);
		//rep(i,1,n) printf("%lld ",dis[i]);puts("");
		
	}
	return 0;
	
}

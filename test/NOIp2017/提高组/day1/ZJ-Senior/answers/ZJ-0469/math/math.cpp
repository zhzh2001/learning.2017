#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define rep(j,k,l) for (int j=k;j<=l;++j)
#define red(j,k,l) for (int j=k;j>=l;--j)
#define LL long long

using namespace std;
LL n,m;

int main(){
	
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld %lld",&n,&m);
	printf("%lld\n",n*m-n-m);
	return 0;
	
}

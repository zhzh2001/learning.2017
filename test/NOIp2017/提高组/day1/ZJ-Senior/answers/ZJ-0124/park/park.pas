var  test,i,j,k,l,h,r,s,n,m,p,t,w,sum,xx,long:longint;
x,y,z,f,start,finish,ru,q:array[0..300000] of longint;
ans:array[0..300000,0..50] of longint;
flag:boolean;
procedure sort(l,r: longint);
      var
         i,j,xx,yy: longint;
      begin
         i:=l;
         j:=r;
         xx:=x[(l+r) div 2];
         repeat
           while x[i]<xx do
            inc(i);
           while xx<x[j] do
            dec(j);
           if not(i>j) then
             begin
                yy:=x[i];
                x[i]:=x[j];
                x[j]:=yy;
                yy:=y[i];
                y[i]:=y[j];
                y[j]:=yy;
                yy:=z[i];
                z[i]:=z[j];
                z[j]:=yy;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
procedure sort1(l,r: longint);
      var
         i,j,xx,yy: longint;
      begin
         i:=l;
         j:=r;
         xx:=y[(l+r) div 2];
         repeat
           while y[i]<xx do
            inc(i);
           while xx<y[j] do
            dec(j);
           if not(i>j) then
             begin
                yy:=x[i];
                x[i]:=x[j];
                x[j]:=yy;
                yy:=y[i];
                y[i]:=y[j];
                y[j]:=yy;
                yy:=z[i];
                z[i]:=z[j];
                z[j]:=yy;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort1(l,j);
         if i<r then
           sort1(i,r);
      end;
begin
assign(input,'park.in');reset(input);
assign(output,'park.out');rewrite(output);
readln(test);
for test:=1 to test do
    begin
        readln(n,m,k,p);
        fillchar(ru,sizeof(ru),0);
        fillchar(q,sizeof(q),0);
        fillchar(start,sizeof(start),0);
        fillchar(finish,sizeof(finish),0);
        for i:=1 to m do
            begin
                readln(x[i],y[i],z[i]);
                inc(ru[y[i]]);
            end;
        sort(1,m);
        start[x[1]]:=1;
        for i:=2 to m do
            if x[i]<>x[i-1] then
                begin
                   start[x[i]]:=i;
                   finish[x[i-1]]:=i-1;
                end;
        finish[x[m]]:=m;
        t:=0;w:=0;
        for i:=1 to n do
            if ru[i]=0 then
                begin
                    inc(w);
                    q[w]:=i;
                end;
        while t<w do
            begin
                inc(t);
                xx:=q[t];
                for j:=start[xx] to finish[xx] do
                    begin
                        dec(ru[y[j]]);
                        if ru[y[j]]=0 then
                            begin
                                inc(w);
                                q[w]:=y[j];
                            end;
                    end;
            end;
        flag:=true;
        for i:=1 to n do
            if ru[i]<>0 then
                begin
                    for j:=start[i] to finish[i] do
                        if z[j]=0 then
                            begin
                                flag:=false;
                                break;
                            end;
                    inc(w);
                    q[w]:=i;
                    if flag=false then break;
                end;
        if flag=false then
            begin
                writeln(-1);
                continue;
            end;
        sort1(1,m);
        start[y[1]]:=1;
        finish[y[m]]:=m;
        for i:=2 to m do
            if y[i]<>y[i-1] then
                begin
                   start[y[i]]:=i;
                   finish[y[i-1]]:=i-1;
                end;
        f[1]:=0;
        for i:=2 to n do
            f[i]:=maxlongint;
        for i:=2 to n do
            begin
                s:=q[i];
                for j:=start[s] to finish[s] do
                    begin
                        r:=x[j];long:=z[j];
                        if f[r]+long<f[s] then f[s]:=f[r]+long;
                    end;
            end;
        fillchar(ans,sizeof(ans),0);
        ans[1,0]:=1;
        for i:=2 to n do
            begin
                s:=q[i];
                for j:=start[s] to finish[s] do
                    begin
                        r:=x[j];long:=z[j];
                        h:=f[r]+long-f[s];
                        for l:=h to k do
                            ans[s,l]:=(ans[r,l-h]+ans[s,l]) mod p;
                    end;
            end;
        sum:=0;
        for i:=0 to k do
            sum:=(sum+ans[n,i]) mod p;
        writeln(sum);
    end;
close(input);close(output);
end.

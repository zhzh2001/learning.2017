#include<bits/stdc++.h>
using namespace std;
struct node{
	bool p;//标记是否为E    这层是O(n)--1 还是O(1)--0
	int l,r,q;//循环上下界 
	char op;//循环的变量 
}A[105];
char str[10],s1[5];
int n,las[105],nxt[105],ans,flag;//F、E的对应位置    实现的数组
bool Q[155];//判断循环变量名的使用 
void Clear(){
	for(int i=1;i<=n;i++){
		Q[i]=nxt[i]=A[i].p=A[i].r=las[i]=0;A[i].l=1000000;
	}
}
void f(int l,int r,int sum,int f1){//在某两行之间   现在在第几层
	if(!f1)ans=max(ans,sum);
	if(l>=r){return;}
	int L=l+1;
	while(L<r){
		if(Q[A[L].op]){flag=1;return;}
		Q[A[L].op]=1;
		if(A[L].q==1)f(L,las[L],sum+1,f1);
		else if(A[L].q==0)f(L,las[L],sum,f1);
		else f(L,las[L],sum,1);
		Q[A[L].op]=0;
		if(flag)return;
		L=las[L]+1;
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;n=100;Clear();
	scanf("%d",&cas);
	while(cas--){
		scanf("%d %s",&n,str);
		for(int i=1;i<=n;i++){
			scanf("%s",s1);
			if(s1[0]=='E')A[i].p=1;//标记 
			else{
				scanf("%s",s1);A[i].op=s1[0];
				scanf("%s",s1);if(s1[0]=='n')A[i].l=1000;else{A[i].l=(s1[0]-'0')*10+s1[1]-'0';}
				scanf("%s",s1);if(s1[0]=='n')A[i].r=1000;else{A[i].r=(s1[0]-'0')*10+s1[1]-'0';}
				if(A[i].l>A[i].r)A[i].q=2;
				else if(A[i].r==1000&&A[i].l!=1000)A[i].q=1;
				else A[i].q=0;
			}
		}
		int x=0,p=0;
		for(int i=n;i>=1;i--){
			if(A[i].p)nxt[++x]=i;
			else{
				if(x==0){p=1;break;}//如果F的个数大于E的个数 
				las[i]=nxt[x--];
			}
		}
		if(p||x){puts("ERR");Clear();continue;}//判断ERR
		int L=1;
		ans=flag=0;
		do{
			Q[A[L].op]=1;
			if(A[L].q==1)f(L,las[L],1,0);
			else if(A[L].q==0)f(L,las[L],0,0);
			else f(L,las[L],0,1);
			Q[A[L].op]=0;
			if(flag)break;
			L=las[L]+1;
		}while(L<n);
		if(flag)puts("ERR");
		else{
			if(str[2]=='n'){
				int res=0;
				int x=4;while(str[x]!=')')res=res*10+str[x]-'0',x++;
				if(ans==res)puts("Yes");else puts("No");
			}else{
				if(!ans)puts("Yes");else puts("No");
			}
		}
		Clear();
	}
	return 0;
}

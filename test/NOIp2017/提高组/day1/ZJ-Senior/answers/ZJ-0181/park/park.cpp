#include<bits/stdc++.h>
#define M 100005
using namespace std;
int n,m,k,P;
struct node{int to,v;};
vector<node>edge[M],edge1[M];
queue<int>q;
bool mark[M];
int dis1[M],dis2[M],ans;
void spfa1(){
	for(int i=1;i<=n;i++)dis1[i]=1e9;
	dis1[1]=0;mark[1]=1;q.push(1);
	while(!q.empty()){
		int x=q.front();q.pop();mark[x]=0;
		for(int i=0;i<(int)edge[x].size();i++){
			int y=edge[x][i].to,v=edge[x][i].v;
			if(dis1[y]>dis1[x]+v){
				dis1[y]=dis1[x]+v;
				if(!mark[y])mark[y]=1,q.push(y);
			}
		}
	}
}
void spfa2(){
	for(int i=1;i<=n;i++)dis2[i]=1e9;
	dis2[n]=0;mark[n]=1;q.push(n);
	while(!q.empty()){
		int x=q.front();q.pop();mark[x]=0;
		for(int i=0;i<(int)edge1[x].size();i++){
			int y=edge1[x][i].to,v=edge1[x][i].v;
			if(dis2[y]>dis2[x]+v){
				dis2[y]=dis2[x]+v;
				if(!mark[y])mark[y]=1,q.push(y);
			}
		}
	}
}
void f(int x){
	if(x==n){ans=(ans+1)%P;return;}
	for(int i=0;i<(int)edge[x].size();i++){
		int y=edge[x][i].to,v=edge[x][i].v;
		if(dis1[x]+dis2[y]+v<=dis1[n]+k)f(y);
	}
}
void Clear(){
	for(int i=1;i<=n;i++)edge[i].clear();
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--){
		scanf("%d%d%d%d",&n,&m,&k,&P);
		for(int i=1;i<=m;i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			edge[x].push_back((node){y,z});
			edge1[y].push_back((node){x,z});
		}
		spfa1();spfa2();
		ans=0;
		f(1);
		printf("%d\n",ans);
		Clear();
	}
	return 0;
}

#include <iostream>
#include <cstdio>
using namespace std;
char sx[123],sy[123],str[123],bl[123],bian[123];
int ci[123],flag[123];
bool id(char ch)
{
	if (ch>='0' && ch<='9')	return 1;
	return 0;
}
int get(char str[])
{
	int s=0;
	for (int i=0;str[i];++i)
		s=s*10+str[i]-48;
	return s;
}
int check()
{
	int L;
	scanf("%d%s",&L,str);
	int sign=0,w=0,err=0,cc=0;
	for (int i=0;str[i];++i)
		if (str[i]=='^')
			sign=1;
		else
		if (str[i]>='0' && str[i]<='9')
			w=w*10+str[i]-48;
	w=w*sign;
	int top=0,tot=0,gouri=0,xx,yy;
	while (L--)
	{
		scanf("%s",str);
		if (str[0]=='F')
		{
			scanf("%s",bl);
			for (int i=1;i<=top;++i)
				if (bian[i]==bl[0])
					err=1;
			bian[++top]=bl[0];
			scanf("%s%s",sx,sy);
			if (id(sx[0]) && !id(sy[0]))
			{
				ci[top]=1;
				flag[top]=0;
			}
			else
			if (!id(sx[0]) && !id(sy[0]))
			{
				ci[top]=0;
				flag[top]=0;
			}
			else
			if (!id(sx[0]) && id(sy[0]))
			{
				ci[top]=0;
				flag[top]=1;
			}
			else
			{
				xx=get(sx);
				yy=get(sy);
				ci[top]=0;
				if (xx<=yy)
					flag[top]=0;
				else flag[top]=1;
			}
			gouri+=flag[top];
			if (!gouri)	tot+=ci[top];
			if (tot>cc)	cc=tot;
		}
		else
		{
			if (top==0)	err=1;
			else
			{
				if (!gouri)	tot-=ci[top];
				gouri-=flag[top];
				top--;
			}
		}
	}
	if (top)	err=1;
	if (err)	return 3;
	if (w==cc)	return 1;
	else return 2;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		int x=check();
		if (x==1)	puts("Yes");
		if (x==2)	puts("No");
		if (x==3)	puts("ERR");
	}
	return 0;
}

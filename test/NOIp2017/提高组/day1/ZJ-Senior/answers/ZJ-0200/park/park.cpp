#include <iostream>
#include <cstring>
#include <cstdio>
#define getid(x,y) ((x-1)*(K+1)+y+1)
using namespace std;
const int N=100005;
const int M=200005;
const int maxn=6000005;
struct E
{
	int x,y,z;	
}e[M];
struct Spfa
{
	int nxt[M],last[N],to[M],v[M],f[N],q[maxn+5];
	bool flag[N];
	int l;
	void clr()
	{
		l=0;
		memset(last,0,sizeof(last));
	}
	void add(int x,int y,int z)
	{
		nxt[++l]=last[x];
		last[x]=l;
		to[l]=y;
		v[l]=z;
	}
	void spfa(int s)
	{
		memset(f,0x3f,sizeof(f));
		memset(flag,0,sizeof(flag));
		int l=0,r=1,y,t;
		q[1]=s;flag[s]=1;
		f[s]=0;
		while (l!=r)
		{
			l=l%maxn+1;
			t=q[l];
			for (int x=last[t];x;x=nxt[x])
			{
				y=to[x];
				if 	(f[t]+v[x]>=f[y])	continue;
				f[y]=f[t]+v[x];
				if (flag[y])	continue;
				flag[y]=1;
				r=r%maxn+1;
				q[r]=y;
			}
			flag[t]=0;
		}		
	}
}T1,T2;
int T,n,m,K,p,mins,x,y,z,po,l,xx,yy;
bool flag[N][53];
int dp[N*55],last[N*55],nxt[M*55],to[M*55],deg[N*55],q[N*55];
void read(int &x)
{
	x=0;char ch;
	do ch=getchar(); while (ch<'0' || ch>'9');
	do{x=x*10+ch-48;ch=getchar();}while(ch>='0' && ch<='9');
}
void mo(int &x,int y,int p)
{
	x=x+y;
	if (x>p)	x-=p;
}
int T_sort()
{
	int tot=0,l=0,r=0,t,y;
	memset(dp,0,sizeof(dp));
	for (int i=1;i<=n;++i)
		for (int j=0;flag[i][j];++j)
		{
			x=getid(i,j);
			++tot;
			if (!deg[x])			
				q[++r]=x;	
		}
	dp[1]=1;
	while (l<r)
	{
		t=q[++l];
		for (int x=last[t];x;x=nxt[x])
		{
			y=to[x];
			mo(dp[y],dp[t],p);
			if (--deg[y]==0)
				q[++r]=y;
		}
	}
	if (r<tot)	return -1;
	int ans=0;
	for (int j=0;flag[n][j];++j)
		mo(ans,dp[getid(n,j)],p);
	return ans;
}
void add(int x,int y)
{
	nxt[++l]=last[x];
	last[x]=l;
	to[l]=y;
}
void clr()
{
	l=0;
	memset(flag,0,sizeof(flag));
	memset(dp,0,sizeof(dp));
	memset(deg,0,sizeof(deg));
	memset(last,0,sizeof(last));
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while (T--)
	{
		read(n);read(m);read(K);read(p);
		T1.clr();T2.clr();
		for (int i=1;i<=m;++i)
		{
			read(x);read(y);read(z);
			T1.add(x,y,z);
			T2.add(y,x,z);
			e[i].x=x;e[i].y=y;e[i].z=z;
		}
		T1.spfa(1);T2.spfa(n);
		clr();
		mins=T1.f[n];
		for (int i=1;i<=n;++i)
		{
			x=T1.f[i]+T2.f[i];
			if (x<=mins+K)
				for (int j=0;j<=mins+K-x;++j)
					flag[i][j]=1;
		}
		for (int i=1;i<=m;++i)
		{
			x=e[i].x;y=e[i].y;z=e[i].z;
			for (int j=0;flag[x][j];++j)
			{
				po=T1.f[x]+j+z-T1.f[y];
				if (po<=K && flag[y][po])
				{
					xx=getid(x,j);yy=getid(y,po);
					add(xx,yy);++deg[yy];
				}
			}
		}
		printf("%d\n",T_sort());
	}
	return 0;
}

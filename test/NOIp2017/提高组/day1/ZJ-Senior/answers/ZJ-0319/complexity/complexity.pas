var
  vis:array['a'..'z']of longint;
  ans,a:array[0..1001]of longint;
  s:array[0..1001]of ansistring;
  t,ss:ansistring;
  c:char;
  flag:boolean;
  cas,tt,i,j,l,sum,anss,now,x,y:longint;
begin
  assign(input,'complexity.in');
  assign(output,'complexity.out');
  reset(input);
  rewrite(output);
  readln(tt);

  for cas:=1 to tt do
    begin
      readln(l,c,t);
      fillchar(vis,sizeof(vis),$5f);
      fillchar(a,sizeof(a),0);
      fillchar(ans,sizeof(ans),0);
      for i:=1 to l do readln(s[i]);
      sum:=0;anss:=0;now:=0;flag:=true;
      for i:=1 to l do
        begin
          if s[i]='E' then
            begin
              dec(sum);  writeln(sum);
              //for j:=1 to l do write(a[j],' ');writeln;
              //for j:=1 to l do
              if a[now]>ans[now] then ans[now]:=a[now];
              if a[now]=-1 then
                for j:=now to l do ans[j]:=0;
              a[now]:=0;
              dec(now);
              if sum<0 then flag:=false;
              continue;
            end;
          inc(sum);inc(now);
          c:=s[i,3];
          delete(s[i],1,4);
          x:=pos(' ',s[i]);
          ss:=copy(s[i],1,x-1);
          delete(s[i],1,x);
          if vis[c]<now then flag:=false else vis[c]:=now;
          if (ss='n')and(s[i]<>'n') then a[now]:=-1;
          if (ss<>'n')and(s[i]<>'n') then
            begin
              val(ss,x);
              val(s[i],y);
              if x>y then a[now]:=-1;
            end;
          if (s[i]='n')and(ss<>'n') then
            if a[now]<>-1 then inc(a[now]);
          //for j:=1 to l do write(a[j],' ');writeln;
        end;
      {if sum<>0 then writeln('ERR')
        else
          if not(flag) }
      if (sum<>0)or(not(flag)) then
        begin
          writeln('ERR');
          continue;
        end;
      anss:=0;
      for i:=1 to l do anss:=anss+ans[i];
      //for i:=1 to l do write(ans[i],' ');writeln;
      //for i:=1 to l do write(a[i],' ');writeln;
      if anss=0 then
        begin
          if t='O(1)' then writeln('Yes')
                      else writeln('No');
          continue;
        end;
      if anss>0 then
        begin
          val(copy(t,5,length(t)-5),x);
          if x=anss then writeln('Yes') else writeln('No');
        end;
      //writeln(anss);
    end;

  close(input);
  close(output);
end.

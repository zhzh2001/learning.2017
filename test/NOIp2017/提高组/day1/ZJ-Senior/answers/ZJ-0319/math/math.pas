var
  p1,p2,d,x,y,ans:int64;

function exgcd(a,b:int64):int64;
var
  t:int64;
begin
  if b=0 then
    begin
      x:=1;
      y:=0;
      exit(a);
    end
  else
    begin
      exgcd(b,a mod b);
      t:=x;
      x:=y;
      y:=t-(a div b)*y;
    end;
end;

begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);

  readln(p1,p2);
  d:=exgcd(p1,p2);
  if x<0 then ans:=p1*(abs((p1-1)*x))-1
         else ans:=p2*(abs((p2-1)*y))-1;
  writeln(ans);

  close(input);
  close(output);
end.

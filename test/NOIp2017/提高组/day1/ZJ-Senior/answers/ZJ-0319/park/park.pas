begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input);
  rewrite(output);

  writeln(-1);

  close(input);
  close(output);
end.
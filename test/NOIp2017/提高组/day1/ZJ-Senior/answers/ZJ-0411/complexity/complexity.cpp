#include <cstdio>
#include <cstring>
#include <cctype>
#define file_io(xxx)\
	do\
	{\
		freopen(xxx".in", "r", stdin);\
		freopen(xxx".out", "w", stdout);\
	}\
	while (0)
#define N INF
#define INF 0x7fffffff
inline void read(int &a)
{
	char c;
	while (!isdigit(c = getchar()));
	a = 0;
	do
		a = a * 10 + c - '0';
	while (isdigit(c = getchar()));
}
bool usd[30];
struct info
{
	char type, var;
	int x, y;
};
info read_line()
{
	info ret;
	char c;
	while (!isalpha(c = getchar()));
	ret.type = c;
	if (c == 'F')
	{
		while (!isalpha(c = getchar()));
		ret.var = c;
		do
			c = getchar();
		while (!isdigit(c) && c != 'n');
		if (c == 'n')
			ret.x = N;
		else
		{
			int x = 0;
			do
				x = x * 10 + c - '0';
			while (isdigit(c = getchar()));
			ret.x = x;
		}
		do
			c = getchar();
		while (!isdigit(c) && c != 'n');
		if (c == 'n')
			ret.y = N;
		else
		{
			int y = 0;
			do
				y = y * 10 + c - '0';
			while (isdigit(c = getchar()));
			ret.y = y;
		}
	}
	return ret;
}
int cnt_l;
int stack()
{
	int ret = 0;
	while (cnt_l)
	{
		--cnt_l;
		const info tmp = read_line();
		if (tmp.type == 'E')
			return ret;
		else if (usd[tmp.var - 'a'])
			return -1;
		else
		{
			usd[tmp.var - 'a'] = 1;
			int tt = stack();
			if (tt == -1)
				return -1;
			if (tmp.x > tmp.y)
				tt = 0;
			else if (tmp.y == N && tmp.x != N)
				++tt;
			if (ret < tt)
				ret = tt;
			usd[tmp.var - 'a'] = 0;
		}
	}
	return -1;
}
int main()
{
	file_io("complexity");
	int t;
	read(t);
	while (t--)
	{
		memset(usd, 0, sizeof usd);
		read(cnt_l);
		int guess_ans;
		while (getchar() != '(');
		char c = getchar();
		if (c == '1')
			guess_ans = 0;
		else
			read(guess_ans);
		int ret = 0;
		while (cnt_l)
		{
			--cnt_l;
			const info tmp = read_line();
			if (tmp.type == 'F')
			{
				usd[tmp.var - 'a'] = 1;
				int tt = stack();
				if (tt == -1)
				{
					ret = -1;
					break;
				}
				if (tmp.x > tmp.y)
					tt = 0;
				else if (tmp.y == N && tmp.x != N)
					++tt;
				if (ret < tt)
					ret = tt;
				usd[tmp.var - 'a'] = 0;
			}
			else
			{
				ret = -1;
				break;
			}
		}
		if (ret == -1)
		{
			while (cnt_l--)
				read_line();
			puts("ERR");
		}
		else
			puts(ret == guess_ans ? "Yes" : "No");
	}
	return 0;
}

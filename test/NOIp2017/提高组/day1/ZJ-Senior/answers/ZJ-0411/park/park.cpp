#include <cstdio>
#include <cstring>
#include <cctype>
#include <algorithm>
#define file_io(xxx)\
	do\
	{\
		freopen(xxx".in", "r", stdin);\
		freopen(xxx".out", "w", stdout);\
	}\
	while (0)
inline void read(int &a)
{
	char c;
	while (!isdigit(c = getchar()));
	a = 0;
	do
		a = a * 10 + c - '0';
	while (isdigit(c = getchar()));
}
#define N 100005
#define M 200005
struct edge
{
	int v;
	unsigned w;
	int nxt;
} e[M];
int head[N], cnt_e;
unsigned dist[N];
int f[N][51];
bool ff[N][51];
int q[N];
bool in_q[N];
int rk[N];
bool cmp(const int a, const int b)
{
	return dist[a] < dist[b];
}
int dfn[N], low[N], dfn_cnt;
int stack[N], stack_cnt = -1;
bool in_stack[N];
bool in_0_circle[N];
void dfs_0_circle(const int u)
{
	stack[++stack_cnt] = u;
	in_stack[u] = 1;
	++dfn_cnt;
	low[u] = dfn_cnt;
	dfn[u] = dfn_cnt;
	for (int i = head[u]; i != 0; i = e[i].nxt)
		if (e[i].w == 0)
		{
			if (dfn[e[i].v] == 0)
			{
				dfs_0_circle(e[i].v);
				if (low[e[i].v] < low[u])
					low[u] = low[e[i].v];
			}
			else if (in_stack[e[i].v])
				low[u] = dfn[e[i].v];
		}
	if (low[u] == dfn[u])
	{
		const bool greater_than_1 =
			(stack_cnt > 0 && low[stack[stack_cnt - 1]] == dfn[u]);
		for (; stack_cnt >= 0 && low[stack[stack_cnt]] == dfn[u]; --stack_cnt)
		{
			in_0_circle[stack[stack_cnt]] = greater_than_1;
			in_stack[stack_cnt] = 0;
		}
	}
}
int main()
{
	file_io("park");
	int t;
	read(t);
	while (t--)
	{
		int n, m, k, p;
		read(n);
		read(m);
		read(k);
		read(p);
		memset(head, 0, sizeof head);
		for (int i = 1; i <= m; ++i)
		{
			int u, v, w;
			read(u);
			read(v);
			read(w);
			edge &ee = e[i];
			ee.v = v;
			ee.w = w;
			ee.nxt = head[u];
			head[u] = i;
		}
		memset(dist, -1, sizeof dist);
		memset(in_0_circle, 0, sizeof in_0_circle);
		memset(dfn, 0, sizeof dfn);
		dfn_cnt = 0;
		for (int i = 1; i <= n; ++i)
			if (!dfn[i])
				dfs_0_circle(i);
		int front = -1, back = -1;
		q[++back] = 1;
		dist[1] = 0;
		in_q[1] = 1;
		while (front != back)
		{
			if (++front == N)
				front = 0;
			const int u = q[front];
			for (int i = head[u]; i != 0; i = e[i].nxt)
				if (dist[u] + e[i].w < dist[e[i].v])
				{
					dist[e[i].v] = dist[u] + e[i].w;
					if (!in_q[e[i].v])
					{
						if (++back == N)
							back = 0;
						q[back] = e[i].v;
						in_q[e[i].v] = 1;
					}
				}
			in_q[u] = 0;
		}
		for (int i = 0; i < n; ++i)
			rk[i] = i + 1;
		std::sort(rk, rk + n, cmp);
		memset(f, 0, sizeof f);
		memset(ff, 0, sizeof ff);
		f[1][0] = 1;
		ff[1][0] = 1;
		for (int kk = 0; kk <= k; ++kk)
			for (int rk_u = 0; rk_u < n; ++rk_u)
			{
				const int u = rk[rk_u];
				const unsigned dist_u = dist[u];
				const int f_u_kk = f[u][kk];
				if (dist_u != 0xffffffff && ff[u][kk] != 0)
					for (int i = head[u]; i != 0; i = e[i].nxt)
					{
						const unsigned tmp_k =
							dist_u + kk + e[i].w - dist[e[i].v];
						if (tmp_k <= (unsigned)k)
						{
							int &tmp = f[e[i].v][tmp_k];
							if (in_0_circle[u] || f_u_kk == -1 || tmp == -1)
								tmp = -1;
							else
								tmp = (tmp + f_u_kk) % p;
							ff[e[i].v][tmp_k] = 1;
						}
					}
			}
		int ans = 0;
		for (int kk = 0; kk <= k; ++kk)
		{
			if (f[n][kk] == -1)
			{
				ans = -1;
				break;
			}
			ans = (ans + f[n][kk]) % p;
		}
		printf("%d\n", ans);
	}
	return 0;
}

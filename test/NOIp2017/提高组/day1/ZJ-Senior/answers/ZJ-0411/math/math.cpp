#include <cstdio>
#define file_io(xxx)\
	do\
	{\
		freopen(xxx".in", "r", stdin);\
		freopen(xxx".out", "w", stdout);\
	}\
	while (0)
int main()
{
	file_io("math");
	int a, b;
	scanf("%d%d", &a, &b);
	printf("%lld\n", (long long)(a - 1) * (b - 1) - 1);
	return 0;
}

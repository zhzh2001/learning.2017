#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;

#define Komachi is retarded
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;i++)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;i--)
#define LL long long

#define M 100004
int T,n,m,K,Mod;
int Next[M<<1],V[M<<1],W[M<<1],Head[M],tot;

int Deg[M],Dis[M];

inline void Add_Edge(int u,int v,int w){
	Next[++tot]=Head[u],V[Head[u]=tot]=v,W[tot]=w;
}
#define LREP(i,A) for(int i=Head[A];i;i=Next[i])

void Rd(int &res){
	char c;res=0;
	while((c=getchar())<48);
	do res=(res<<3)+(res<<1)+(c^48);
	while((c=getchar())>47);
}
inline void Add(int &a,const int &b){
	a+=b;
	if(a>=Mod)a-=Mod;
}

struct Node{
	int x,w;
	bool operator <(const Node &_)const{
		return w>_.w;
	}
};
priority_queue<Node>Q; 
void Dij(){
	while(!Q.empty())Q.pop();
	memset(Dis,63,sizeof(Dis));
	
	Q.push((Node){1,Dis[1]=0});
	while(!Q.empty()){
		int A=Q.top().x,w=Q.top().w,B,Cost;Q.pop();
		if(w!=Dis[A])continue;
		LREP(i,A)if(Dis[B=V[i]]>(Cost=w+W[i]))
			Q.push((Node){B,Dis[B]=Cost});
	}
}

struct P30{
	int F[M];
	void Solve(){
		Dij();
		
		memset(F,0,sizeof(F));
		F[1]=1;
		REP(i,1,n+1)Q.push((Node){i,Dis[i]});
		while(!Q.empty()){
			int A=Q.top().x,B;Q.pop();
			LREP(i,A)if(Dis[B=V[i]]==Dis[A]+W[i])
				Add(F[B],F[A]);
		}
		printf("%d\n",F[n]);
	}
}P30;

//struct P70{
//	int F[M][54];
//	struct Point{
//		int x,d;
//		bool operator <(const Point &_)const{
//			return Dis[x]+d<Dis[_.x]+_.d;
//		}
//	}P[M*54];
//	void Solve(){
//		Dij();
//		
////		REP(i,1,n+1)cerr<<Dis[i]<<' ';cerr<<endl;
//		
//		int Cnt=0;
//		REP(i,1,n+1)REP(j,0,K+1)P[Cnt++]=(Point){i,j};
//		sort(P,P+Cnt);
//		
//		memset(F,0,sizeof(F));
//		F[1][0]=1;
//		REP(i,0,Cnt){
//			int A=P[i].x,d=P[i].d,B;
//			if(F[A][d]==0)continue;
////			cerr<<A<<","<<d<<":"<<F[A][d]<<endl;
//			LREP(j,A){
//				int dt=Dis[A]+d+W[j]-Dis[B=V[j]];
//				if(dt>K)continue;
//				Add(F[B][dt],F[A][d]);
//			}
//		}
//		int Ans=0;
//		REP(i,0,K+1)Add(Ans,F[n][i]);
//		printf("%d\n",Ans);
//	}
//}P70;
//struct P100{
//	int F[M][54],Cnt;
//	struct Point{
//		int x,d,w;
//		bool operator <(const Point &_)const{
//			if(w==_.w)return Pos[x]<Pos[_.x];
//			return w<_.w;
//		}
//	}P[M*54];
//	void Topu(){
//		int l=0,r=0;
//		REP(i,1,n+1)if(Deg[i]==0)Qt[r++]=i;
//		while(l<r){
//			int A=Qt[l],B;
//			Pos[A]=l++;
//			LREP(i,A)if(W[i]==0){
//				Deg[B=V[i]]--;
//				if(Deg[B]==0)Qt[r++]=B;
//			}
//		}
//	}
//	void Solve(){
//		Dij();
//		Topu();
//	
////		REP(i,1,n+1)cerr<<Dis[i]<<' ';cerr<<endl;
//		
//		Cnt=0;
//		REP(i,1,n+1)REP(j,0,K+1)P[Cnt++]=(Point){i,j,Dis[i]+j};
//		sort(P,P+Cnt);
//		
//		memset(F,0,sizeof(F));
//		F[1][0]=1;
//		REP(i,0,Cnt){
//			int A=P[i].x,d=P[i].d,B;
//			if(F[A][d]==0)continue;
//			if(Deg[A]){puts("-1");return;}
//			LREP(j,A){
//				int dt=Dis[A]+d+W[j]-Dis[B=V[j]];
//				if(dt>K)continue;
//				Add(F[B][dt],F[A][d]);
//			}
//		}
//		
//		int Ans=0;
//		REP(i,0,K+1)Add(Ans,F[n][i]);
//		printf("%d\n",Ans);
//	}
//}P100;
int Qt[M];
struct P100{
	int F[M][54],Cnt;
	struct Point{
		int x,d,w;
	}P[M*54],Tmp[M*54];
	void Topu(){
		memset(Qt,0,sizeof(Qt));
		int l=0,r=0;
		REP(i,1,n+1)if(Deg[i]==0)Qt[r++]=i;
		while(l<r){
			int A=Qt[l++],B;
			LREP(i,A)if(W[i]==0){
				Deg[B=V[i]]--;
				if(Deg[B]==0)Qt[r++]=B;
			}
		}
	}
	bool Mark[M];
	static const int Base=100000;
	int RCnt[M];
	void Init(){
		Cnt=0;
		memset(Mark,0,sizeof(Mark));
		for(int i=0;;i++){
			int A=Qt[i];
			if(!A)break;
			Mark[A]=1;
			REP(j,0,K+1)P[Cnt++]=(Point){A,j,Dis[A]+j};
		}
		REP(i,1,n+1)if(!Mark[i])
			REP(j,0,K+1)P[Cnt++]=(Point){i,j,Dis[i]+j};
		
//		REP(i,0,Cnt)cerr<<P[i].x<<","<<P[i].d<<' ';cerr<<endl;
		
		memset(RCnt,0,sizeof(RCnt));
		REP(i,0,Cnt)RCnt[P[i].w%Base]++;
		REP(i,1,M)RCnt[i]+=RCnt[i-1];
		DREP(i,Cnt-1,-1)Tmp[--RCnt[P[i].w%Base]]=P[i];
		
//		REP(i,0,Cnt)cerr<<Tmp[i].x<<","<<Tmp[i].d<<' ';cerr<<endl;
		
		memset(RCnt,0,sizeof(RCnt));
		REP(i,0,Cnt)RCnt[Tmp[i].w/Base]++;
		REP(i,1,M)RCnt[i]+=RCnt[i-1];
		DREP(i,Cnt-1,-1)P[--RCnt[Tmp[i].w/Base]]=Tmp[i];
		
//		REP(i,0,Cnt)cerr<<P[i].x<<","<<P[i].d<<' ';cerr<<endl;
	}
	void Solve(){
		Dij();
		Topu();
//		REP(i,1,n+1)cerr<<Dis[i]<<' ';cerr<<endl;
		
		Init();
		
		memset(F,0,sizeof(F));
		F[1][0]=1;
		REP(i,0,Cnt){
			int A=P[i].x,d=P[i].d,B;
			if(F[A][d]==0)continue;
			if(Deg[A]){puts("-1");return;}
			LREP(j,A){
				int dt=Dis[A]+d+W[j]-Dis[B=V[j]];
				if(dt<=K)Add(F[B][dt],F[A][d]);
			}
		}
		
		int Ans=0;
		REP(i,0,K+1)Add(Ans,F[n][i]);
		printf("%d\n",Ans);
	}
}P100;
int main(){
//	printf("%.6lf",(sizeof(P30)+sizeof(P100))/1024.0/1024.0);
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	Rd(T);
	while(T--){
		Rd(n),Rd(m),Rd(K),Rd(Mod);
		
		memset(Head,tot=0,sizeof(Head));
		memset(Deg,0,sizeof(Deg));
		
		bool p70=1;
		while(m--){
			int u,v,w;
			Rd(u),Rd(v),Rd(w);
			Add_Edge(u,v,w);
			if(w==0){
				Deg[v]++;
				p70=0;
			}
		}
		
		if(K==0 && p70)P30.Solve();
//		else if(p70)P70.Solve();
		else P100.Solve();
	}
	return 0;
}

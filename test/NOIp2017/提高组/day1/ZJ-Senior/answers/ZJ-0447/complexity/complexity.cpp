#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define Komachi is retarded
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;i++)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;i--)
#define LL long long

#define M 104
int T,n;
char S[M][2],C[M][2],A[M][4],B[M][4];
string Res;
int Stk[M],stp,Mark[264];
bool Check(){
	memset(Mark,stp=0,sizeof(Mark));
	REP(i,0,n){
		if(S[i][0]=='E'){
			if(stp==0)return 0;
			Mark[Stk[stp--]]=0;
		}
		else{
			if(Mark[C[i][0]])return 0;
			Stk[++stp]=C[i][0];
			Mark[C[i][0]]=1;
		}
	}
	return stp==0?1:0;
}
int Tmp[M];
void Solve(){
	int Ans=0,num=0;
	stp=0;
	REP(i,0,n){
		if(S[i][0]=='F'){
			Tmp[i]=num;
			Stk[++stp]=i;
			
			if(A[i][0]=='n' && B[i][0]!='n')num=-233333333;
			else if(A[i][0]!='n' && B[i][0]=='n') num++;
			else if(A[i][0]!='n' && B[i][0]!='n'){
				int x=0,y=0;
				REP(j,0,strlen(A[i]))x=x*10+A[i][j]-'0';
				REP(j,0,strlen(B[i]))y=y*10+B[i][j]-'0';
				if(x>y)num=-233333333;
			}
		}
		else num=Tmp[Stk[stp--]];
		if(Ans<num)Ans=num;
	}
	if(Res=="O(1)")puts(Ans==0?"Yes":"No");
	else {
		int p=0;
		REP(i,4,Res.length())if(Res[i]!=')')
			p=p*10+Res[i]-'0';
		puts(Ans==p?"Yes":"No");
	}
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		cin>>Res;
		REP(i,0,n){
			scanf("%s",S[i]);
			if(S[i][0]=='F')scanf("%s%s%s",C[i],A[i],B[i]);
		}
		if(!Check())puts("ERR");
		else Solve();
	}
	return 0;
}

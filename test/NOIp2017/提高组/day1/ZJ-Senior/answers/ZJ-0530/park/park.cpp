#include<cstdio>
#include<queue>
#include<vector>
#include<cstdlib>
#include<memory.h>
#define N 100005
#define M 200005
#define K 55
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
using namespace std;
struct node{int to,len;};
vector<node>edge[N];
queue<int>Q;
int dis[N];
int dp[N][K];
bool mark[N],Mark[N][K];
int T,n,m,k,P;
bool Boom=0;
void dfs(int x,int j){
	FOR(i,0,edge[x].size()-1){
		int y=edge[x][i].to;
		int tmp=j+dis[x]+edge[x][i].len-dis[y];
		if(tmp>=0&&tmp<=k){
			dp[y][tmp]=(dp[y][tmp]+dp[x][j])%P;
			dfs(edge[x][i].to,tmp);
		}
	}
}

int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	int a,b,c;
	while(T--){
		Boom=0;
		memset(mark,0,sizeof mark);
		scanf("%d %d %d %d",&n,&m,&k,&P);
		while(!Q.empty())Q.pop();
		for(int i=1;i<=n;i++)
			edge[i].clear(),
			dis[i]=1e9;
		dis[1]=0;
		FOR(i,1,m){
			scanf("%d %d %d",&a,&b,&c);
			edge[a].push_back((node){b,c});
		}
		Q.push(1);
		while(!Q.empty()){
			int x=Q.front();
			mark[x]=0;
			Q.pop();
			FOR(i,0,edge[x].size()-1){
				int y=edge[x][i].to;
				if(dis[y]>dis[x]+edge[x][i].len){
					dis[y]=dis[x]+edge[x][i].len;
					if(!mark[y])mark[y]=1,Q.push(y);
				}
			}
		}
		memset(dp,0,sizeof dp);
		memset(Mark,0,sizeof Mark);
		dp[1][0]=1;
		dfs(1,0);
		if(Boom){
			puts("-1");
			continue;
		}
		long long ans=0;
		FOR(i,0,k)ans+=dp[n][i];
		printf("%lld\n",ans);
	}
	return 0;
}


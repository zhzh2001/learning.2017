#include<cstdio>
#include<cstdlib>
#include<memory.h>
char chr[5],fuza[20];
bool mark[30];
int Stack[105];
bool StackO[105];

int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T,n;
	scanf("%d",&T);
	while(T--){
		memset(mark,0,sizeof mark);
		int w=0;//n^w(O(1)=N^0)
		scanf("%d",&n);
		scanf("%s",fuza);
		if(fuza[2]=='n'){
			int ii=3;
			while(fuza[++ii]!=')'){
				w=w*10+(fuza[ii]&15);
			}
		}
		int cen=0;
		bool f=1;
		char bian[3],x[3],y[3];
		int zuiO=0,O=0,fO=0;
		for(int i=1;i<=n;i++){
			scanf("%s",chr);
			if(chr[0]=='F'){
				scanf("%s %s %s",bian,x,y);
				if(!f)continue;
				if(fO==0){
					StackO[cen+1]=0;
					if(x[0]=='n'){
						if(y[0]!='n')fO=cen+1;
					}
					else if(y[0]=='n')StackO[cen]=1,O++;
					else if(y[0]<x[0])fO=cen+1;
	
					if(O>zuiO)zuiO=O;
				}
				if(mark[bian[0]-'a'])f=0;
				else mark[bian[0]-'a']=1;
				Stack[cen]=bian[0]-'a';
				cen++;
			} else {
				if(!f)continue;
				cen--;
				if(cen<fO)fO=0;
				if(cen<0){f=0;continue;}
				mark[Stack[cen]]=0;
				O-=StackO[cen];
			}
		}
		if((!f)||cen)puts("ERR");
		else puts(w==zuiO?"Yes":"No");
	}
	return 0;
}


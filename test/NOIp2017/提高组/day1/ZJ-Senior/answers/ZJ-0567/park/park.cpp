//10 points
#include <cstdio>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <queue>
#define inf 0x3f3f3f3f
#include <algorithm>
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;
typedef long long LL;
template <typename T>
void read(T &x)
{
	x=0;
	char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
}
template <typename T>
void read(T &x,T &y){read(x);read(y);}
template <typename T>
void read(T &x,T &y,T &z){read(x);read(y);read(z);}
int n,T,m,k,p;
const int maxn=111111,maxm=222222;
int to[maxm],value[maxm],first[maxn],cnt,nxt[maxm];
void addedge(int u,int v,int w){to[++cnt]=v;value[cnt]=w;nxt[cnt]=first[u];first[u]=cnt;}
int dis[maxn],ans;
bool inqueue[maxn];
queue <int> qu;
void spfa()
{
	memset(dis,0x3f,sizeof(dis));
	dis[1]=0;
	qu.push(1);
	while (!qu.empty())
	{
		int now=qu.front();
		qu.pop();
		for (int q=first[now];q;q=nxt[q])
		{
			if (dis[now]+value[q]<dis[to[q]])
			{
				dis[to[q]]=dis[now]+value[q];
				if (!inqueue[to[q]]) 
				{
					qu.push(to[q]);
					inqueue[to[q]]=true;
				}
			}
		}
		inqueue[now]=false;
	}
}
void dfs(int x,int v)
{
	if (v>dis[n]+k) return;
	if (x==n) ans++;
	for (int q=first[x];q;q=nxt[q]) dfs(to[q],v+value[q]);
}
int f[maxn],g[1111][1111];
void spfa2()
{
	memset(dis,0x3f,sizeof(dis));
	dis[1]=0;
	qu.push(1);
	while (!qu.empty())
	{
		int now=qu.front();
		qu.pop();
		for (int q=first[now];q;q=nxt[q])
		{
			if (dis[now]+value[q]<dis[to[q]])
			{
				dis[to[q]]=dis[now]+value[q];
				f[to[q]]=f[now];
				rep(i,1,n) g[i][to[q]]=0;
				g[now][to[q]]=f[now];
				if (!inqueue[to[q]]) 
				{
					qu.push(to[q]);
					inqueue[to[q]]=true;
				}
			}
			else
			{
				if (dis[now]+value[q]==dis[to[q]]) 
				{
					g[now][to[q]]=f[now];
					f[to[q]]=0;
					rep(i,1,n) f[to[q]]=(f[to[q]]+g[i][to[q]])%p;
					//f[to[q]]=f[to[q]]+f[now];
					if (!inqueue[to[q]])
					{
						qu.push(to[q]);	
						inqueue[to[q]]=true;
					}
				}
			}
		}
		inqueue[now]=false;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T);
	while (T--)
	{
		ans=0;
		memset(first,0,sizeof(first));
//		memset(nxt,0,sizeof(nxt));
//		memset(value,0,sizeof(value));
		memset(inqueue,0,sizeof(inqueue));
//		memset(to,0,sizeof(to));
		cnt=0;
		
		read(n,m);
		read(k,p);
		int a,b,c;
		rep(i,1,m) 
		{
			read(a,b,c);
			addedge(a,b,c);
		}
		if (n>5&&k==0) 
		{
			spfa2();
			printf("%d\n",f[n]%p);
		}
		else
		{
			spfa();
			dfs(1,0);
			printf("%d\n",ans%p);
		}
	}
	return 0;
}

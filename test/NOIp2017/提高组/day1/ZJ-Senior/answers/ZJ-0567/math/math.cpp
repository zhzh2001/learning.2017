#include <cstdio>
#include <iostream>
#include <cstring>
#include <cstdlib>
#define rep(i,x,y) for (int i=x;i<=y;i++)
#define per(i,x,y) for (int i=x;i>=y;i--)
using namespace std;
typedef long long LL;
template <typename T>
void read(T &x)
{
	x=0;
	char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
}
template <typename T>
void read(T &x,T &y){read(x);read(y);}
int n,m;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	read(n,m);
	if (n<m) swap(n,m);
	printf("%lld\n",(n-2)+(LL)(n-1)*(m-2));
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXS 200
inline void Read(int &x) {
	x=0;
	char c=getchar();
	for(; !('0'<=c&&c<='9'); c=getchar()) if(c=='n') {
			x=-1;
			return ;
		}
	for(; '0'<=c&&c<='9'; c=getchar()) x=x*10+c-'0';
}
int L;
struct State {
	int name;
	int type;
	State() {};
	State(int _name,int _type):name(_name),type(_type) {}
} stack[MAXS+10];
int top;
char cx[20];
char name;
bool inUsing[26];
int type,w;
inline void GetType() {
	if(cx[2]=='1') type=1;
	else {
		type=2;
		w=0;
		int len=strlen(cx);
		int i=0;
		for(; !('0'<=cx[i]&&cx[i]<='9'); i++);
		for(; '0'<=cx[i]&&cx[i]<='9'; i++) w=w*10+cx[i]-'0';
	}
}
int depList[MAXS+10],cnt;
int main() {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T,i,x,y,flag;
	char c;
	scanf("%d",&T);
	while(T--) {
		scanf("%d%s",&L,cx);
		cnt=flag=top=0;
		GetType();
		for(i=1; i<=L; i++) {
			do {
				c=getchar();
			} while(c==' '||c=='\n');
			if(c=='F') {
				do {
					name=getchar();
				} while(name==' '||name=='\n');
				if(inUsing[name-'a']) {
					flag=1;
					break;
				}
				inUsing[name-'a']=true;
				Read(x);
				Read(y);
				if(x==y&&x==-1) x=y=0;
				if((type==1)&&(x==-1||y==-1)) {
					flag==2;
					break;
				}
				if(y==-1) x=-1;
				stack[++top]=State(name-'a',x);
			} else {
				if(top<0) {
					flag=1;
					break;
				}
				inUsing[stack[top].name]=false;
				if(stack[top].type==-1) depList[++cnt]=top;
				stack[top--];
			}
		}
		if(type==2) {
			std::sort(depList+1,depList+cnt+1);
			int iw;
			for(i=1; i<=cnt; i++) if(depList[i]!=depList[i-1]) iw++;
			if(iw!=w) flag=2;
		};
		if(top!=0||flag==1) puts("ERR");
		else if(flag==0) puts("Yes");
		else if(flag==2) puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#define MAXE 200000
#define MAXH 300000
#define MAXN 100000
template <typename T>
inline void Swap(T &a,T &b){
	T t=a;a=b;b=t;
}
int n,m,k,p;
struct Edge{
	int v,w,nxt;
	Edge(){}
	Edge(int _v,int _w,int _nxt):v(_v),w(_w),nxt(_nxt){}
}E[MAXE+10];int nE=0;int head[MAXE+10];
inline void EdgeInit(){
	nE=0;
	memset(head,-1,sizeof(head));
}
inline void AddEdge(int u,int v,int w){
	E[nE]=Edge(v,w,head[u]);head[u]=nE++;
}
struct State{
	int key,dis;
	State(){}
	State(int _key,int _dis):key(_key),dis(_dis){}
	friend bool operator < (State a,State b){
		return a.dis<b.dis;
	} 
};
template <typename T>
struct Heap{
	T a[MAXH];int tail;
	inline void Push(const T x){
		a[++tail]=x;
		int now=tail;
		while(now>1&&a[now]<a[now>>1]) Swap(a[now],a[now>>1]),now>>=1; 
	}
	inline void Pop(void){
		a[1]=a[tail--];
		int now=2;
		while(now<=tail){
			if(a[now+1]<a[now]) now++;
			if(a[now]<a[now>>1]) Swap(a[now],a[now>>1]);
			else break;
			now<<=1;
		}
	}
	const T Top(void){
		return a[1];
	}
	const bool Empty(void){
		return tail==0;
	}
};
Heap<State> q;
int dis[MAXN+10];
bool vis[MAXN+10];
int lim;
inline void GetSSSP(int s){
	memset(dis,0x7f,sizeof(dis));
	dis[s]=0;
	q.Push(State(s,dis[s]));
	int u;
	while(!q.Empty()){
		u=q.Top().key;;q.Pop();
		for(int i=head[u],v=E[i].v;i!=-1;i=E[i].nxt,v=E[i].v)if(!vis[v]){
			vis[v]=true;
			if(dis[u]+E[i].w<dis[v]) {
				dis[v]=E[i].w;
				q.Push(State(v,dis[v]));
			}
		}
	}
	lim=dis[n]+k;
}
int ans;
void dfs(int u,int val){
	if(val>lim) return ;
	if(u==n) ans=(ans+1)%p;
	for(int i=head[u],v=E[i].v;i!=-1;i=E[i].nxt,v=E[i].v)if(!vis[v]){
		 vis[v]=true;
		 dfs(v,val+E[i].w);
		 vis[v]=false;
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%p",&n,&m,&k,&p);
		ans=0;
		EdgeInit();
		for(int i=1,x,y,z;i<=m;i++){
			scanf("%d%d%d",&x,&y,&z);
			AddEdge(x,y,z);
		}
		GetSSSP(1);
		memset(vis,false,sizeof(vis));
		if(dis[n]!=0x7f7f7f7f) {
			dfs(1,0);
			printf("%d\n",ans);
		}
		else puts("-1");
	}
	return 0;
}

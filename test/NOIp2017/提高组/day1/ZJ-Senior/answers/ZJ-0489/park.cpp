#include<cstdio>
#include<iostream>
using namespace std;
int n,m,k,p,b[2010][2010],c[1000010],d[100010],s,minn;
bool f[100010];
inline void read(int &x)
{
	char ch=getchar(); x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
void dfs(int x,int y)
{
	if (y>minn+k) return;
	if (x==n) s=(s+1)%p;
	for (int i=1;i<=n;i++)
		if (b[x][i]<2e9&&i!=x) dfs(i,y+b[x][i]);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T; read(T);
	while (T--)
	{
//		tot=0;
		read(n); read(m); read(k); read(p);
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++) b[i][j]=2e9;
		for (int i=1;i<=m;i++)
		{
			int x,y,z;
			read(x); read(y); read(z);
			b[x][y]=min(b[x][y],z);
//			a[++tot].too=y; a[tot].nxt=last[x]; a[tot].dis=z; last[x]=tot;
		}
		minn=2e9;
		int h=1,t=1; c[1]=1; d[1]=0; f[1]=1;
		for (int i=2;i<=n;i++) d[i]=b[1][i];
		while (h<=t)
		{
			int x=0,y=100000;
			for (int i=1;i<=n;i++)
				if (!f[i]&&b[c[h]][i]<y) x=i;
			for (int i=1;i<=n;i++)
				if (d[x]+b[x][i]<d[i])
				{
					d[i]=d[x]+b[x][i];
					t++; c[t]=i; f[i]=1;
				}
			f[c[h]]=0; h++;
		}
		minn=d[n]; s=0;
		if (minn==0)
		{
			printf("-1\n"); continue;
		}
		dfs(1,0);
		printf("%d\n",s);
	}
	return 0;
}

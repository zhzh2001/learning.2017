#include<bits/stdc++.h>
using namespace std;
#define M 100005
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));
}
int n,m;
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%d %d",&n,&m);
	printf("%lld\n",(ll)(n-1)*(m-1)-1);
	return 0;
}

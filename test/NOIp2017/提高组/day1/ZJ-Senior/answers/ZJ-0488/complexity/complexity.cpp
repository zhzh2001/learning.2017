#include<bits/stdc++.h>
using namespace std;
#define M 205
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));
}
int mark[M<<1],stk[M],top;
char op[M][2],s[M][2],x[M][2],y[M][2],pre[M],nxt[M];
void Max(int &a,int b){
	if(a<b)a=b;
}
void Init(){
	top=0;
	memset(mark,0,sizeof(mark));
	memset(pre,0,sizeof(pre));
	memset(nxt,0,sizeof(nxt));
}
int main(){
	int T;
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		int n,flag=0,num=0,res=0;char str[10];
		Init();
		scanf("%d %s",&n,str);
		if(strlen(str)==4)res=0;
		else {
			for(int i=0;i<strlen(str);i++)
				if(str[i]>='0'&&str[i]<='9')res=res*10+(str[i]^48);
		}
		for(int i=1;i<=n;i++){
			scanf("%s",op[i]);
			if(op[i][0]=='F'){
				scanf("%s %s %s",s[i],x[i],y[i]);
				stk[++top]=i;
				if(mark[s[i][0]])flag=1;
				mark[s[i][0]]=1;
			}
			else {
				if(top){
					mark[s[stk[top]][0]]=0;
					pre[i]=stk[top];
					nxt[stk[top--]]=i;
				}
				else flag=1;
			}
		}
		if(top)flag=1;
		if(flag)puts("ERR");
		else {
			int ans=0,cnt=0;top=0;
			for(int i=1;i<=n;i++){
				if(op[i][0]=='F'){
					if(y[i][0]!='n'&&(x[i][0]=='n'||x[i][0]>y[i][0]))i=nxt[i];
					else if(y[i][0]=='n'&&x[i][0]!='n'){
						cnt++;
						Max(ans,cnt);
					}
				}
				else if(y[pre[i]][0]=='n'&&x[pre[i]][0]!='n')cnt--;
			}
			if(ans==res)puts("Yes");
			else puts("No");
		}
	}
	return 0;
}

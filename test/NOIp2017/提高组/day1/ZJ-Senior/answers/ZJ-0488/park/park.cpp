#include<bits/stdc++.h>
using namespace std;
#define M 100005
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));
}
int n,m,K,P;
struct E{
	int to,len,nxt;
}edge[M<<1];
int head[M],tot;
void Init(){
	tot=0;
	memset(head,-1,sizeof(head));
}
inline void Add(int a,int b,int c){
	edge[tot]=(E){b,c,head[a]};head[a]=tot++;
}
struct node{
	int x,dis;
	bool operator <(const node &A)const{
		return dis>A.dis;
	}
};
int dis[M],mark[M],dep[M],dp[M][55];
priority_queue<node>Q;
void Dijkstra(){
	while(!Q.empty())Q.pop();
	Q.push((node){1,0});
	memset(dep,0,sizeof(dep));
	memset(dis,63,sizeof(dis));
	memset(mark,0,sizeof(mark));
	dis[1]=0;dep[1]=1;
	while(!Q.empty()){
		node tmp=Q.top();Q.pop();
		if(mark[tmp.x])continue;
		int x=tmp.x;mark[x]=1;
		for(int i=head[x];~i;i=edge[i].nxt){
			int y=edge[i].to,len=edge[i].len;
			if(dis[y]>dis[x]+len){
				dis[y]=dis[x]+len;
				dep[y]=dep[x]+1;
				Q.push((node){y,dis[y]});
			}
		}
	}
}
struct P30{
	int Q[M],cnt[M],dp[M];
	void solve(){
		memset(dp,0,sizeof(dp));
		memset(cnt,0,sizeof(cnt));
		for(int i=1;i<=n;i++)
			for(int j=head[i];~j;j=edge[j].nxt){
				int y=edge[j].to,len=edge[j].len;
				if(dis[y]==dis[i]+len)cnt[y]++;
			}
		int l=0,r=0;
		Q[r++]=1;
		dp[1]=1;
		while(l<r){
			int x=Q[l++];
			for(int i=head[x];~i;i=edge[i].nxt){
				int y=edge[i].to,len=edge[i].len;
				if(dis[y]==dis[x]+len){
					cnt[y]--;
					dp[y]=(dp[y]+dp[x])%P;
					if(!cnt[y])Q[r++]=y;
				}
			}
		}
		printf("%d\n",dp[n]);
	}
}p30;
struct P50{
	int Q[1005],cnt[1005],dp[2][1005][55];
	void solve(){
		memset(dp,0,sizeof(dp));
		memset(cnt,0,sizeof(cnt));
		for(int i=1;i<=n;i++)
			for(int j=head[i];~j;j=edge[j].nxt){
				int y=edge[j].to,len=edge[j].len;
				if(dis[y]==dis[i]+len)cnt[y]++;
			}
		int l=0,r=0;
		Q[r++]=1;
		dp[0][1][0]=1;
		while(l<r){
			int x=Q[l++];
			for(int i=head[x];~i;i=edge[i].nxt){
				int y=edge[i].to,len=edge[i].len;
				if(dis[y]==dis[x]+len){
					cnt[y]--;
					dp[0][y][0]=(dp[0][y][0]+dp[0][x][0])%P;
					if(!cnt[y])Q[r++]=y;
				}
			}
		}
		int cur=0,ans=dp[cur][n][0];
		for(int lalala=1;lalala<=K;lalala++){
			cur=!cur;
			memset(dp[cur],0,sizeof(dp[cur]));
			for(int c=0;c<=K;c++)
				for(int i=1;i<=n;i++)
					for(int j=head[i];~j;j=edge[j].nxt){
						int y=edge[j].to,len=edge[j].len;
						if((dis[y]!=dis[i]+len||c)&&c+dis[i]+len-dis[y]<=K){
							dp[cur][y][c+dis[i]+len-dis[y]]=(dp[cur][y][c+dis[i]+len-dis[y]]+dp[!cur][i][c])%P;
						}
					}
			for(int c=0;c<=K;c++)ans=(ans+dp[cur][n][c])%P;
		}
		printf("%d\n",ans);
	}
}p50;
struct P60{
	int Q[M],cnt[M],dp[55][M];
	void solve(){
		memset(dp,0,sizeof(dp));
		memset(cnt,0,sizeof(cnt));
		for(int i=1;i<=n;i++)
			for(int j=head[i];~j;j=edge[j].nxt){
				int y=edge[j].to,len=edge[j].len;
				if(dis[y]==dis[i]+len)cnt[y]++;
			}
		int l=0,r=0;
		Q[r++]=1;
		dp[0][1]=1;
		while(l<r){
			int x=Q[l++];
			for(int i=head[x];~i;i=edge[i].nxt){
				int y=edge[i].to,len=edge[i].len;
				if(dis[y]==dis[x]+len){
					cnt[y]--;
					dp[0][y]=(dp[0][y]+dp[0][x])%P;
					if(!cnt[y])Q[r++]=y;
				}
			}
		}
		int ans=0;
		for(int c=0;c<=K;c++){
			for(int i=1;i<=n;i++)
				for(int j=head[i];~j;j=edge[j].nxt){
					int y=edge[j].to,len=edge[j].len;
					if((dis[y]!=dis[i]+len||c)&&c+dis[i]+len-dis[y]<=K){
						dp[c+dis[i]+len-dis[y]][y]=(dp[c+dis[i]+len-dis[y]][y]+dp[c][i])%P;
					}
				}
			ans=(ans+dp[c][n])%P;	
		}
		printf("%d\n",ans);
	}
}p60;
int main(){
	int T;
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		Init();
		scanf("%d %d %d %d",&n,&m,&K,&P);
		while(m--){
			int a,b,c;
			Rd(a);Rd(b);Rd(c);
			Add(a,b,c);
		}
		Dijkstra();
		if(K==0)p30.solve();
		else if(n<=1000)p50.solve();
		else p60.solve();
	}
	return 0;
}

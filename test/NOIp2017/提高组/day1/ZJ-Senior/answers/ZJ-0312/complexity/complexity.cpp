#include<cstdio>
#include<iostream>
#include<cstring>
#include<string>
#define MAXLEN 300
using namespace std;
int lastcom[MAXLEN],nowcom[MAXLEN],top;
int val[MAXLEN];
bool Have[27];
string Read(){
	string tmp="";
	char ch=getchar();
	while (!(ch=='O'||ch=='F'||ch=='E'||ch=='('||ch==')'||ch=='^'||ch>='a'&&ch<='z'||ch>='0'&&ch<='9'))
	  ch=getchar();
	while (ch=='O'||ch=='F'||ch=='E'||ch=='('||ch==')'||ch=='^'||ch>='a'&&ch<='z'||ch>='0'&&ch<='9')
	  tmp+=ch,ch=getchar();
	return tmp;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T=0,Line=0;
	scanf("%d",&T);
	while (T--){
		Line=0;
		scanf("%d",&Line);
		string compe=Read();
		//cout<<compe<<endl;
		bool flag=1;top=0;
		memset(lastcom,0,sizeof(lastcom));
		memset(nowcom,0,sizeof(nowcom));
		memset(val,0,sizeof(val));
		memset(Have,0,sizeof(Have));
		for (int i=1;i<=Line;i++){
			string fthing=Read();//cout<<fthing<<endl;
			if (!flag) continue;
			if (fthing=="F"){
				top++;
				string fname=Read();
				if (!Have[fname[0]-96]) Have[fname[0]-96]=1,val[top]=fname[0];
				else {
					flag=0;
					continue;
				}
				string x=Read(),y=Read();
				if ((x[0]=='n')&&(y[0]=='n')) nowcom[top]=0;
				if ((x[0]=='n')&&(y[0]!='n')) nowcom[top]=-1;
				if ((x[0]!='n')&&(y[0]=='n')) nowcom[top]=1;
				else{
					int X=0,Y=0;
					for (int i=0;i<x.size();i++) X=X*10+x[i]-48;
					for (int i=0;i<y.size();i++) Y=Y*10+y[i]-48;
					if (X>Y) nowcom[top]=-1;
					else nowcom[top]=0;
				}
			}
			else{
				if (top==0) {
				  flag=0;continue;
				}
				int k=0;
				if (nowcom[top]==-1) k=0;
				else k=nowcom[top]+lastcom[top+1];
				if (k>lastcom[top]) lastcom[top]=k;
				nowcom[top]=lastcom[top+1]=0;Have[val[top]-96]=0;
				top--;
			}
		}
		if (top>0||!flag) printf("ERR\n");
		else{
			if (compe[2]=='1'){
				if (lastcom[1]==0) printf("Yes\n");
				else printf("No\n");
			}
			else{
				int len=compe.size();
				int Up=0;
				for (int i=4;i<=len-2;i++)
				  Up=Up*10+compe[i]-48;
				//cout<<Up<<endl;
				//cout<<lastcom[1]<<endl;
				if (Up==lastcom[1]) printf("Yes\n");
				else printf("No\n");
			}
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

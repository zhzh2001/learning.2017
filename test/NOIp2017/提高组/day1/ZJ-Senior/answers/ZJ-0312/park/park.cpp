#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#include<vector>
#define MAXM 200005
#define INF 1e9
using namespace std;
struct Node{
  int todot,weight;
};
bool Inq[MAXM>>1];
queue<int> spfa;
vector<Node> G[MAXM>>1];
int MinDist[MAXM>>1],Sum[MAXM>>1];
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=0;
	scanf("%d",&T);
	while (T--){
		int n,m,k,p;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=m;i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			G[x].push_back((Node){y,z});
			G[y].push_back((Node){x,z});
		}
		for (int i=1;i<n;i++) MinDist[i]=INF;
		MinDist[1]=0;
		spfa.push(1);memset(Inq,0,sizeof(Inq));
        Inq[1]=1;Sum[1]=1;
        while (!spfa.empty()){
        	int dot=spfa.front();Inq[dot]=0;spfa.pop();
        	for (int i=0;i<G[dot].size();i++){
        		Node tmp=G[dot][i];
        		if (MinDist[dot]+tmp.weight<MinDist[tmp.todot]) {
        			MinDist[tmp.todot]=MinDist[dot]+tmp.weight;Sum[tmp.todot]=Sum[dot]; 
					if (!Inq[tmp.todot]) Inq[tmp.todot]=1,spfa.push(tmp.todot); 
				}
				else if (MinDist[dot]+tmp.weight==MinDist[tmp.todot])
				  Sum[tmp.todot]=(Sum[tmp.todot]+Sum[dot])%p;
			}
		}
		printf("%d\n",Sum[n]);
	}
	fclose(stdin);fclose(stdout);
	return 0;
} 

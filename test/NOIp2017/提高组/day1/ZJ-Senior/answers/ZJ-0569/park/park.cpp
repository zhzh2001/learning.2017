#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;
struct PAR{
	int x,y,c,next;
}e[205000];
queue<int> q;
int last[105000],cent,dis[105000];bool vis[105000];
void add(int x,int y,int c){
	e[++cent].x=x;
	e[cent].y=y;
	e[cent].c=c;
	e[cent].next=last[x];
	last[x]=cent;
}
int main(){
	int n,m,k,p,i,j,l,x,y,c,t;
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		memset(vis,false,sizeof(vis));
		memset(e,0,sizeof(e));
		memset(last,0,sizeof(last));
		memset(dis,127/3,sizeof(dis));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(i=1;i<=m;i++){
			scanf("%d%d",&x,&y,&c);
			add(x,y,c);
		}
		dis[1]=0;
		vis[1]=1;
		q.push(1);
		while(!q.empty()){
			int u=last[q.front()];
			while(u){
				if(dis[e[u].y]>=dis[e[u].x]+e[u].c){
					dis[e[u].y]=dis[e[u].x]+e[u].c;
					if(!vis[e[u].y]){
						vis[u]=1;
						q.push(e[u].y);
					}
				}
				u=e[u].next;
			}
			vis[q.front()]=0;
			q.pop();
		}
		cout<<-1<<endl;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

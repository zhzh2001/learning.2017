#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
string p;
int max(int a,int b){
	return a>b?a:b;
}
int za=0,sta[300];bool bl[30]={0},flag=0,mm=0;
int main(){
	int t,i,j,k,l,n,m,maxx,q;char c[50],x,df;string s,y,z,o,we;
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		cin>>l>>s;
		flag=0;
		za=0;
		n=0;
		maxx=0;
		y="";
		we="";
		z="";
		memset(sta,0,sizeof(sta));
		memset(bl,false,sizeof(bl));
		for(i=1;i<=l;i++){
			cin>>df;
			if(df=='F'){
				za++;
				cin>>x;
				if(!bl[int(x)-96]){
					bl[int(x)-96]=1;
					sta[za]=int(x)-96;
				}
				else{
					flag=1;
				}
				k++;
				cin>>y>>z;
				if(y=="n"&&z!="n") mm=1;
				else if(y!="n"&&z=="n"&&!mm) n++;
				else if(y!="n"&&z!="n"){
					if((y.size()>z.size())||(y.size()==z.size()&&y>z)) n=0;
				}
			}
			else if(df=='E'){
				if(za>0){
					bl[sta[za]]=0;
					za--;
				}
				if(za<0){
					flag=1;
				}
				else if(za==0){
					maxx=max(maxx,n);
					mm=0;
					n=0;
				}
			}
		}
		if(za==0&&!flag){
			if(maxx==0) o="O(1)";
			else{
				o="O(n^";
				while(maxx){
					q=maxx%10;
					maxx/=10;
					we=char(q+48)+we;
				}
				o+=we;
				o+=")";
			}
			if(o==s) printf("Yes\n");
			else {
				printf("No\n");
			}
		}
		else printf("ERR\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

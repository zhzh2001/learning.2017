#include<cstdio>
#include<cstdlib>
#include<cstring>
#define MaX(x,y) ((x)>(y)?(x):(y))
using namespace std;
int T,L,n,maxP,Stkl,Tname,TnameChain[200],VChain[200],PChain[200],x,y,ValueA,tmpP;
bool Tag,bbb[30],TagGolb;
int ReaD(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int getN(){
	int x=0;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='n')return -1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x;
}
void getO(bool &Tag0,int &x){
	Tag0=0;x=0;
	char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='n')Tag0=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return;
}
bool getFE(){
	char ch=getchar();
	while(ch!='F'&&ch!='E')ch=getchar();
	if(ch=='F')return 1;else return 0;
}
int getTname(){
	char ch=getchar();
	while(ch<'a'||ch>'z')ch=getchar();
	return(ch-'a');
}
int main(){
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	T=ReaD();
	while(T-->0){
		L=ReaD();
		getO(Tag,n);
		maxP=0;
		tmpP=0;
		Stkl=0;
		TagGolb=1;
		ValueA=1;
		memset(bbb,0,sizeof(bbb));
		VChain[0]=1;
		PChain[0]=0;
		for(int i=0;i<L;i++){
			if(getFE()){
				Tname=getTname();
				x=getN();
				y=getN();
				if(!TagGolb)continue;
				Stkl++;
				if(bbb[Tname]){
					printf("ERR\n");
					TagGolb=0;
					continue;
				}
				bbb[Tname]=1;
				TnameChain[Stkl-1]=Tname;
				if(x==-1&&y==-1){}else
				if(x==-1)ValueA=0;else
				if(y==-1){tmpP+=ValueA;maxP=MaX(maxP,tmpP);}else
				if(x>y)ValueA=0;
				VChain[Stkl]=ValueA;
				PChain[Stkl]=tmpP;
			}else{
				if(!TagGolb)continue;
				if(Stkl<=0){
					printf("ERR\n");
					TagGolb=0;
					continue;
				}
				Stkl--;
				bbb[TnameChain[Stkl]]=0;
				ValueA=VChain[Stkl];
				tmpP=PChain[Stkl];
			}
		}
		if(!TagGolb)continue;
		if(Stkl>0)printf("ERR\n");else{
			if((maxP==0&&Tag==0)||(Tag==1&&maxP==n))printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}

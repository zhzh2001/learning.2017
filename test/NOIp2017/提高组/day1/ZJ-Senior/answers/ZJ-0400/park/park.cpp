#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#define ll long long
#define max(x,y) ((x)>(y)?(x):(y))
#define min(x,y) ((x)<(y)?(x):(y))
using namespace std;
int n,m,k,Nxt[200005],NxtRev[200005],Nxt2[200005],Fst[200005],Dis[200005],FstRev[200005],Fst2[200005],Tgt[200005],TgtRev[200005],Tgt2[200005],Vl[200005],ReFind[200005];
ll PMod,FuncSt[200005][55],Ans;
int Ai,Bi,Ci,TotL=0,TotL2=0,T,HeapT,Tm[200005],LT[200005],Tmc,StkT,Stk[200005];
bool bb[200005],inZero[200005],BZ[200005];
struct Node{
	int Ind;
	int Dis;
}Heap[1000005];
int ReaD(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void addLine(const int &Ai,const int &Bi,const int &Ci){
	Nxt[TotL]=Fst[Ai];
	Fst[Ai]=TotL;
	Tgt[TotL]=Bi;
	
	NxtRev[TotL]=FstRev[Bi];
	FstRev[Bi]=TotL;
	TgtRev[TotL]=Ai;
	Vl[TotL]=Ci;
	TotL++;
}
void addLineZ(const int &Ai,const int &Bi){
	Nxt2[TotL2]=Fst2[Ai];
	Fst2[Ai]=TotL2;
	Tgt2[TotL2]=Bi;
	TotL2++;
}
void Upshift(const int &st){
	int i=st,j=st>>1;
	Heap[0]=Heap[st];
	while(j>0&&Heap[0].Dis<Heap[j].Dis){
		Heap[i]=Heap[j];
		ReFind[Heap[i].Ind]=i;
		i=j;j>>=1;
	}
	Heap[i]=Heap[0];
	ReFind[Heap[i].Ind]=i;
}
void Downshift(const int &st){
	int i=st,j=st<<1;
	Heap[0]=Heap[st];
	while(j<=HeapT&&Heap[0].Dis>Heap[j].Dis){
		if(j<HeapT&&Heap[j].Dis>Heap[j+1].Dis)j++;
		Heap[i]=Heap[j];
		ReFind[Heap[i].Ind]=i;
		i=j;j<<=1;
	}
	Heap[i]=Heap[0];
	ReFind[Heap[i].Ind]=i;
}
void Pop(){
	ReFind[Heap[1].Ind]=-1;
	Heap[1]=Heap[HeapT--];
	ReFind[Heap[1].Ind]=1;
	Downshift(1);
}
void Ins(const int &Ind,const int &Dis){
	Heap[++HeapT].Ind=Ind;
	Heap[HeapT].Dis=Dis;
	ReFind[Heap[HeapT].Ind]=HeapT;
	Upshift(HeapT);
}
void FlushNode(const int &Ind,const int &Dis){
	if(ReFind[Ind]==-1)Ins(Ind,Dis);else{
		Heap[ReFind[Ind]].Dis=Dis;
		Upshift(ReFind[Ind]);
		Downshift(ReFind[Ind]);
	}
}
void DijkStra(){
	memset(Dis,-1,sizeof(Dis));
	memset(ReFind,-1,sizeof(ReFind));
	memset(bb,0,sizeof(bb));
	Dis[1]=0;
	Ins(1,0);
	while(HeapT){
		Node q=Heap[1];Pop();
		if(bb[q.Ind])continue;
		bb[q.Ind]=1;
		for(int i=Fst[q.Ind];i!=-1;i=Nxt[i]){
			if(Dis[Tgt[i]]==-1||Dis[Tgt[i]]>Dis[q.Ind]+Vl[i]){
				Dis[Tgt[i]]=Dis[q.Ind]+Vl[i];
				FlushNode(Tgt[i],Dis[Tgt[i]]);
			}
		}
	}
}
int Dfs(const int &x){
	if(BZ[x])return LT[x];
	BZ[x]=1;
	Stk[StkT++]=x;
	Tm[x]=LT[x]=Tmc++;
	for(int i=Fst2[x];i!=-1;i=Nxt2[i]){
		if(LT[x]==0)LT[x]=Dfs(Tgt2[i]);else
		LT[x]=min(Dfs(Tgt2[i]),LT[x]);
	}
	if(LT[x]==Tm[x]&&Stk[StkT-1]==x){StkT--;return LT[x];}
	if(LT[x]==Tm[x]){
		Stk[StkT]=0;
		while(Stk[StkT]!=x)inZero[Stk[--StkT]]=1;
	}
	return LT[x];
}
void ZeroR(){
	memset(BZ,0,sizeof(BZ));
	StkT=0;
	Tmc=1;
	for(int i=1;i<=n;i++)
	Dfs(i);
}
ll Func(const int &i,const int &j){
	if(i==1)return 1;
	if(FuncSt[i][j]!=-1)return FuncSt[i][j];
	if(inZero[i])return FuncSt[i][j]=-2;
	ll Sum=0,TmP;
	for(int l=FstRev[i];l!=-1;l=NxtRev[l]){
		if(Dis[TgtRev[l]]==-1||j-(Dis[TgtRev[l]]+Vl[l]-Dis[i])<0)continue;
		TmP=Func(TgtRev[l],j-(Dis[TgtRev[l]]+Vl[l]-Dis[i]));
		if(TmP==-2){Sum=-2;break;}
		Sum=(Sum+TmP)%PMod;
	}
	return FuncSt[i][j]=Sum;
}
int main(){
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	T=ReaD();
	while(T-->0){
		n=ReaD();m=ReaD();k=ReaD();PMod=ReaD();
		memset(Fst,-1,sizeof(Fst));
		memset(FstRev,-1,sizeof(FstRev));
		memset(Fst2,-1,sizeof(Fst2));
		memset(FuncSt,-1,sizeof(FuncSt));
		memset(inZero,0,sizeof(inZero));
		for(int i=0;i<m;i++){
			Ai=ReaD();Bi=ReaD();Ci=ReaD();
			addLine(Ai,Bi,Ci);
			if(Ci==0)addLineZ(Ai,Bi);
		}
		DijkStra();
		ZeroR();
		Ans=Func(n,k)%PMod;
		if(Ans==-2)printf("%d\n",-1);else printf("%lld\n",Ans);
	}
	return 0;
}

#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
ll a,b,x,y,xx,yy,ans;
void exgcd(ll a,ll b,ll &x,ll &y){
	if(b==0){
		x=1;y=0;return;
	}
	ll x1,y1;
	exgcd(b,a%b,x1,y1);ll t=a/b;
	x=y1;y=x1-t*y1;
}
int main()
{
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a>b)swap(a,b);
	if(a==1){
		printf("0");return 0;
	}
	ll d1=(b-1)/a;
	exgcd(a,b,x,y);
	if(a*x>b*y){
		xx=x-b;yy=y+a;	
	}else{
		xx=x+b;yy=y-a;
	}
	if(x>xx){
		swap(x,xx);swap(y,yy);
	}
	ll l1,l2,t1,t2=a*x+b*0,lun;
	l1=max(abs(x)/abs(xx),abs(xx)/abs(x));
	l2=max(abs(y)/abs(yy),abs(yy)/abs(y));
	lun=min(l1,l2);
	if(abs(xx)>abs(x)){
		t1=a*abs(x)*lun;
		ans=t1-1;
	}else{
		ll tt=abs(x)/abs(xx);
		
		ans=a*(abs(x)-tt*abs(xx))+b*tt*abs(yy);
		ans=ans-1;
	}
//	printf("%lld %lld %lld %lld\n",x,y,xx,yy);
	printf("%lld\n",ans);
	return 0;
}

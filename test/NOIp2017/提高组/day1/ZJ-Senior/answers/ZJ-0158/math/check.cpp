#include<cstdio>
#include<cstring>
#include<algorithm>
#include<ctime>
#include<cstdlib>
using namespace std;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
int gcd(int a,int b)
{
	if(!b)return a;
	return gcd(b,a%b);
}
int x,y,sum,a[20000],ans;
bool check(int c)
{
	int lim=c/x;
	For(i,0,lim)
		if((c-i*x)%y==0)return 1;
	return 0;
}
int main()
{
	srand(time(0));

	x=rand()%99+1,y=rand()%99+1;
	for(;gcd(x,y)!=1;y=rand()%99+1);
	if(x<y)swap(x,y);
	if(y==1)return puts("YES"),0;

//	x=53,y=48;
	printf("%d %d\n",x,y);
	For(i,1,x*y)
	{
		if(!check(i))a[++*a]=i,ans=max(ans,i);
	}
	sum=x*y;
	for(;;)
	{
		For(i,1,*a)
		{
			if(check(sum+a[i]))a[i]=0;else ans=max(ans,a[i]+sum);
		}
		int i=1,j=1;
		for(;j<=*a;)
		{
			if(a[i]==0)a[i]=a[j+1],++j;else ++i;
		}
		*a=0;
		sum+=x*y;
		if(*a==0)break;
	}
	if(ans!=x*y-x-y)puts("NO");
	printf("%d\n",ans);
}


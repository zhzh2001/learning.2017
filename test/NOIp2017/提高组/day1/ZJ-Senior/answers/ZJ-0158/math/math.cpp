#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
inline void write(ll x)
{
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
ll a,b,ans;
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read(),b=read();
	if(a>b)swap(a,b);
	ans=a*b-b-a;
	write(ans);puts("");
}


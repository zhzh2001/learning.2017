#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
inline void write(ll x)
{
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void Read(int &x)
{
	x=0;
	char c=getchar();
	for(;c>'9'||c<'0';)
	{
		if(c=='n')return;
		c=getchar();
	}
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
}
void pans(int ans)
{
	if(ans==1)puts("Yes");else
	if(ans==2)puts("No");else
	if(ans==3)puts("ERR");
}
int a[20000],b[30],c[20000],d[20000],e[20000];
char s[100];
int find()
{
	int cnt=0;
	for(int i=*d;i>=1;--i)
	{
		if(d[i]==2)++cnt;else --cnt;
		if(cnt==0)
		{
			For(j,1,i-1)
				if(d[j]==1)++cnt;
			return cnt+1;
		}
	}
	if(cnt>0)return -1;
	return 1;
}
void work(int hang,int w)
{
	int ans=0,E=0;
	if(hang==1)
	{
		scanf("%s",s);
		if(s[0]=='E')
		{
			if(w==0)ans=1;else ans=2;
		}else
		{
			ans=3;
			scanf("%s",s);scanf("%s",s);scanf("%s",s);
		}
		pans(ans);
		return;
	}
	memset(b,0,sizeof b);
	memset(a,0,sizeof a);
	memset(c,0,sizeof c);
	memset(d,0,sizeof d);
	memset(e,0,sizeof e);
	for(;hang;--hang)
	{
		scanf("%s",s);
		if(s[0]=='F')				
		{
			scanf("%s",s);
			if(b[s[0]-'a'])
			{
				ans=3;
				scanf("%s",s);scanf("%s",s);scanf("%s",s);--hang;
				break;
			}
			b[s[0]-'a']=1;
			e[++*e]=s[0]-'a';
			int x,y;
			Read(x);Read(y);
			d[++*d]=1;
			++*a;
			if(x==0)
			{
				if(y==0)a[*a]=1;else a[*a]=0;
			}else
			{
				if(y==0)a[*a]=2;else
				{
					if(y>=x)a[*a]=1;else a[*a]=0;
				}
			}
		}else
		{
			d[++*d]=2;
			E=find();
			if(E==-1){ans=3;--hang;break;}
			b[e[E]]=0;
			For(i,E+1,*a)
			{
				c[E]=max(c[E],c[i]);
			}
			if(a[E]==0)
			{
				c[E]=0;
				For(i,E+1,*a)c[i]=0;
			}else
			if(a[E]==2)++c[E];
		}
	}
	if(d[*d]==1)ans=3;
	for(;hang;--hang)
	{
		scanf("%s",s);
		if(s[0]=='F')
		{
			scanf("%s",s);scanf("%s",s);scanf("%s",s);
		}
	}
	if(ans!=3)
	{
		int cnt=0;E=find();
		For(i,1,E)cnt=max(cnt,c[i]);
		if(cnt==w)ans=1;else ans=2;
	}
	pans(ans);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for(int test=read();test;--test)
	{
		int hang=read();
		memset(s,0,sizeof s);
		int w=0;
		scanf("%s",s);
		int len=strlen(s);
		if(len!=4)
		{
			For(i,4,len-2)
			{
				w=w*10+(s[i]-'0');
			}
		}
		work(hang,w);
	}
}

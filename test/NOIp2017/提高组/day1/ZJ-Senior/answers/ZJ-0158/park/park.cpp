#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
typedef long long ll;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
inline void write(ll x)
{
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
const int N=100000+10;
int n,m,k,mo,du[N],f[N],dis[N],q[N],head[N],ecnt;
int mp[1010][1010];
bool vis[N];
struct edge{int to,nxt,w;}e[200010];
void addedge1(int u,int v,int w)
{
	mp[u][v]=min(mp[u][v],w);
	++du[v];
}
void addedge(int u,int v,int w)
{
	e[++ecnt]=(edge){v,head[u],w};head[u]=ecnt;
	++du[v];
}
void work1()
{
	dis[1]=0;
	For(t,2,n)
	{
		int k=0,mn=1e9;
		For(i,1,n)
			if(!vis[i]&&mn>dis[i])mn=dis[i],k=i;
		vis[k]=1;
		For(i,1,n)
			if(!vis[i]&&dis[i]>dis[k]+mp[k][i])dis[i]=dis[k]+mp[k][i];
	}
	f[1]=1;
	du[1]=0;
	q[0]=1;
	for(int h=0,t=1;h!=t;)
	{
		int u=q[h++];
		For(i,1,n)
			if(u!=i)
			{
				--du[i];
				if(dis[i]==dis[u]+mp[u][i])(f[i]+=f[u])%=mo;
				if(du[i]==0)q[t++]=i;
			}
	}
	printf("%d\n",f[n]);
}

struct node
{
	int d,u;
	bool operator <(const node &B)const{return d>B.d;}
};
priority_queue<node>Q;
void work()
{
	dis[1]=0;
	Q.push((node){0,1});
	for(;!Q.empty();)
	{
		int u=Q.top().u;Q.pop();
		vis[u]=1;
		for(;!Q.empty()&&vis[Q.top().u];Q.pop());
		for(int i=head[u];i;i=e[i].nxt)
		{
			int v=e[i].to;
			if(dis[v]>dis[u]+e[i].w)
			{
				dis[v]=dis[u]+e[i].w;
				Q.push((node){dis[v],v});
			}
		}
	}
	f[1]=1;
	du[1]=0;
	q[0]=1;
	for(int h=0,t=1;h!=t;)
	{
		int u=q[h++];
		for(int i=head[u];i;i=e[i].nxt)
		{
			int v=e[i].to;
			--du[v];
			if(dis[v]==dis[u]+e[i].w)(f[v]+=f[u])%=mo;
			if(du[v]==0)q[t++]=v;
		
		}
	}
	printf("%d\n",f[n]);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(int test=read();test;--test)
	{
		n=read(),m=read(),k=read(),mo=read();
		memset(dis,40,sizeof dis);
		memset(mp,40,sizeof mp);
		memset(vis,0,sizeof vis);
		memset(du,0,sizeof du);
		memset(f,0,sizeof f);
		ecnt=0;
		memset(head,0,sizeof head);
		For(i,1,m)
		{
			int u=read(),v=read(),w=read();
			if(n<=1000)addedge1(u,v,w);else addedge(u,v,w);
		}
		if(n<=1000)work1();else work();
	}
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
bool f[30];
int id[105],stack[105];
char str[15],st1[105],st2[105],st3[105][5],st4[105][5];
int calc(int x,int y)
{
	if (x==0 && y==1) return 1;
	else return 0;
}
int jia(int x,int y)
{
	return max(x,y);
}
int cheng(int x,int y)
{
	if (x==0) return y;
	if (y==0) return x;
	return x+y;
}
int check(int h,int t)
{
	int x=0,y=0,k=0;
	if (st3[h][1]=='n') x=1;
	if (st4[h][1]=='n') y=1;
	int s=calc(x,y),sum=0;
	if (x==1 && y!=1) return s;
	if (x==0 && y==0)
	{
		int xx=0,yy=0;
		int l1=strlen(st3[h]+1);
		int l2=strlen(st4[h]+1);
		for (int i=1; i<=l1; i++) xx=xx*10+st3[h][i]-48;
		for (int i=1; i<=l2; i++) yy=yy*10+st4[h][i]-48;
		if (xx>yy) return s;
	}
	int i=h+1;
	while (i<=t)
	{
		if (st1[i]=='F') 
		{
			sum=jia(sum,check(i,id[i]));
			i=id[i];
		}
		else i++;
	}
	//printf("%d %d %d %d\n",h,t,s,sum);
	s=cheng(s,sum);
	//printf("return:%d\n",s);
	return s;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while (cas--)
	{
		int n;
		cin>>n>>str+1;
		//scanf("%d%s",&n,str+1);
		int l=strlen(str+1);
		//printf("input: %d %s\n",n,str+1);
		int tim=0;
		for (int i=1; i<=l; i++)
		{
			if (str[i]=='^')
			{
				int j=i+1;
				while (j<=l && str[j]>='0' && str[j]<='9') 
				{
					//printf("str %d %d\n",j,str[j]);
					tim=tim*10+str[j]-48;
					j++;
				}
				break;
			}
		}
		//printf("%d\n",tim);
		for (int i=1; i<=n; i++) 
		{
			cin>>st1[i];
			if (st1[i]=='F') cin>>st2[i]>>st3[i]+1>>st4[i]+1;
			//scanf("%s%s%s%s",st1[i],st2[i],st3[i]+1,st4[i]+1);
			//printf("input:%s %s %s %s\n",st1[i],st2[i],st3[i]+1,st4[i]+1);
		}
		bool flagc=1;
		int top=0;
		memset(f,0,sizeof(f));
		for (int i=1; i<=n; i++)
		{
			if (st1[i]=='F') 
			{
				if (f[st2[i]-96]) 
				{
					//printf("error:%d %d\n",i,st2[i]-96);
					printf("ERR\n");
					flagc=0;
					break;
				}
				stack[++top]=i;
				f[st2[i]-96]=1;
			}
			else
			{
				if (top==0) 
				{
					//printf("error:%d top=0\n",i);
					printf("ERR\n");
					flagc=0;
					break;
				}
				id[stack[top]]=i;
				f[st2[stack[top]]-96]=0;
				top--;
			}
		}
		if (!flagc) continue;
		if (top!=0)
		{
			//printf("top>0\n");
			printf("ERR\n");
			continue;
		}
		int ans=0;
		int i=1;
		while (i<=n)
		{
			if (st1[i]=='F') 
			{
				ans=jia(ans,check(i,id[i]));
				i=id[i];
			}
			else i++;
		}
		//printf("%d %d\n",ans,tim);
		if (ans==tim) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int oo=1e9;
bool flag[100005],flag1[100005][55];
int n,m,k,P,q[100005],in[100005],d[100005],dis[100005],f[100005][55];
int num,head[100005],vet[200005],val[200005],nex[200005];
int num1,head1[100005],vet1[200005],val1[200005],nex1[200005];
long long read()
{
	char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	long long ans=0;
	while (ch<='9' && ch>='0')
	{
		ans=ans*10+ch-48;
		ch=getchar();
	}
	return ans;
}
int mo(int x)
{
	if (x>=P) x-=P;
	return x;
}
void add(int u,int v,int l)
{
	num++;
	vet[num]=v;
	val[num]=l;
	nex[num]=head[u];
	head[u]=num;
}
void add1(int u,int v,int l)
{
	num1++;
	vet1[num]=v;
	val1[num]=l;
	nex1[num]=head1[u];
	head1[u]=num1;
}
void spfa1(int s)
{
	int h=1,t=1;
	memset(in,0,sizeof(in));
	for (int i=1; i<=n; i++) dis[i]=oo;
	q[1]=s;
	in[s]=1;
	dis[s]=0;
	while (h<=t)
	{
		int u=q[h];
		for (int i=head[u]; i; i=nex[i])
		{
			int v=vet[i];
			//printf("%d %d %d %d\n",u,v,dis[u],dis[v]);
			if (dis[v]>dis[u]+val[i]) 
			{
				dis[v]=dis[u]+val[i];
				in[v]++;
				q[++t]=v;
			}
		}
		h++;
	}
}
void Spfa1(int s)
{
	int h=1,t=1;
	memset(in,0,sizeof(in));
	for (int i=1; i<=n; i++) d[i]=oo;
	q[1]=s;
	in[s]=1;
	d[s]=0;
	while (h<=t)
	{
		int u=q[h];
		for (int i=head1[u]; i; i=nex1[i])
		{
			int v=vet1[i];
			//printf("%d %d %d %d\n",u,v,dis[u],dis[v]);
			if (d[v]>d[u]+val1[i]) 
			{
				d[v]=d[u]+val1[i];
				in[v]++;
				q[++t]=v;
			}
		}
		h++;
	}
}
bool spfa2(int s)
{
	int h=1,t=1;
	memset(in,0,sizeof(in));
	memset(f,0,sizeof(f));
	memset(flag,0,sizeof(flag));
	memset(flag1,0,sizeof(flag1));
	q[1]=s;
	in[s]=1;
	f[s][0]=1;
	while (h<=t)
	{
		int u=q[h];
		for (int i=head[u]; i; i=nex[i])
		{
			int v=vet[i];
			if (flag[v]) continue;
			for (int x=0; x<=k; x++)
			{
				int len=dis[v]+x-val[i];
				//if (v==5) printf("%d %d %d %d\n",u,v,dis[v]+x,len);
				if (len>=dis[u] && len<=dis[u]+k) f[v][x]=mo(f[v][x]+f[u][len-dis[u]]);
				//if (len>=dis[u] && len<=dis[u]+k && v==5) printf("%d %d\n",f[v][x],f[u][len-dis[u]]);
			}
			if (dis[u]+val[i]<=dis[v]+k) 
			{
				in[v]++;
				if (in[v]>=n)
				{
					//if (dis[v]+d[v]<=dis[n]+k) return 0;
					flag[v]=1;
				}
				if (!flag1[v][dis[u]+val[i]-dis[v]]) q[++t]=v,flag1[v][dis[u]+val[i]-dis[v]]=1;
			}
		}
		h++;
	}
	return 1;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int cas=read();
	while (cas--)
	{
		num=0; num1=0;
		memset(head,0,sizeof(head));
		memset(head1,0,sizeof(head1));
		n=read(); m=read(); k=read(); P=read();
		for (int i=1; i<=m; i++)
		{
			int u,v,l;
			u=read(); v=read(); l=read();
			add(u,v,l);
			add1(v,u,l);
		}
		spfa1(1); Spfa1(n);
		if (!spfa2(1)) 
		{
			printf("-1\n");
			continue;
		}
		int ans=0;
		for (int i=0; i<=k; i++) ans=mo(ans+f[n][i]);
		/*
		for (int i=1; i<=n; i++)
		{
			printf("i:%d\n",dis[i]);
			for (int j=0; j<=k; j++) printf("%d ",f[i][j]);
			printf("\n");
		}
		*/
		printf("%d\n",ans);
	}
	return 0;
}

#include <cstdio>
#include <queue>
#include <cstring>

using std::queue;

#define nullptr NULL

struct node_t {
	node_t(){}
	node_t(int _src, int _dst, int _len, node_t *_next = nullptr) {
		src = _src;
		dst = _dst;
		len = _len;
		next = _next;
	}

	int src;
	int dst;
	int len;
	node_t *next;
};

struct node_index_t {
	node_t *head;
	node_t *tail;
};

void node_index_init(node_index_t *node_index, int n) {
	for (int i = 0; i < n; ++i) {
		node_index[i].head = node_index[i].tail = nullptr;
	}
}

void add_edge(node_index_t *node_index, int src, int dst, int len) {
	if (node_index[src].head == nullptr) {
		node_index[src].head = node_index[src].tail = new node_t(src, dst, len);
	}
	else {
		node_index[src].tail = node_index[src].tail->next = new node_t(src, dst, len);
	}
}

void clean_edge(node_index_t *node_index, int n) {
	for (int i = 0; i < n; ++i) {
//		fprintf(stderr, "freeing from src %d\n", i);
		node_t *p = node_index[i].head;
		while (p != nullptr) {
			node_t *tp = p->next;
			delete p;
			p = tp;
		}
	}
}

struct bfs_state_t {
	bfs_state_t(){}
	bfs_state_t(int _pos, int _len, int _prev_pos, int _prev_len, int _inf_flag) {
		pos = _pos;
		len = _len;
		prev_pos = _prev_pos;
		prev_len = _prev_len;
		inf_flag = _inf_flag;
	}
	int pos;
	int len;
	int prev_pos;
	int prev_len;
	int inf_flag;
};

struct bfs_state_rev_t {
	bfs_state_rev_t(){}
	bfs_state_rev_t(int _pos, int _len) {
		pos = _pos;
		len = _len;
	}
	int pos;
	int len;
};

#define LEN_MAX 0x7fffffff


void bfs_pre(node_index_t *node_index, int n, int *map) {
	queue<bfs_state_rev_t> q;
	q.push(bfs_state_rev_t(n, 0));
	
	for (int i = 0; i <= n; ++i) map[i] = LEN_MAX;
	map[n] = 0;
	
	while (!q.empty()) {
		bfs_state_rev_t cur_state = q.front();
		if (cur_state.len < map[cur_state.pos])
			map[cur_state.pos] = cur_state.len;
		
		node_t *p = node_index[cur_state.pos].head;
		while (p != nullptr) {
			int n_len = cur_state.len + p->len;
			if (n_len < map[p->dst])
				q.push(bfs_state_rev_t(p->dst, n_len));
			p = p->next;
		}
		
		q.pop();
	}
}

void print_array(int *arr, int n) {
	for (int i = 0; i < n; ++i) {
		fprintf(stderr, "%d ", arr[i]);
	}
	fprintf(stderr, "\n");
}


int bfs(node_index_t *node_index, int dst, int max_len_offset, int mod, int *map) {
	queue<bfs_state_t> q;
	q.push(bfs_state_t(1, 0, 0, 0, 0));
	int max_len = LEN_MAX;
	int ans = 0;
	
	while (!q.empty()) {
		bfs_state_t cur_state = q.front();
		if (max_len == LEN_MAX && cur_state.pos == dst) {
			max_len = cur_state.len + max_len_offset;
//			fprintf(stderr, "max_len = %d\n", max_len);
		}

		node_t *p = node_index[cur_state.pos].head;
		
		while (p != nullptr) {
			int n_len = cur_state.len + p->len;
			int inf_flag = 0;
			
			if (n_len + map[p->dst] > max_len) {
				p = p->next;
				continue;
			}
			
			if (cur_state.prev_pos == p->dst && cur_state.prev_len == n_len) inf_flag = 1;
			else if (cur_state.inf_flag == 1) inf_flag = 1;
			
			if (n_len <= max_len)
				q.push(bfs_state_t(p->dst, n_len, cur_state.pos, cur_state.len, inf_flag));
			p = p->next;
		}

		if (cur_state.pos == dst) {
			if (cur_state.inf_flag == 1) return -1;
			ans = (ans + 1) % mod;
		}

		q.pop();
	}
	return ans;
}

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	
	int t;
	scanf("%d", &t);
	
	for (int ti = 0; ti < t; ++ti) {
		int n, m, k, p;
		scanf("%d%d%d%d", &n, &m, &k, &p);
		node_index_t *node_index = new node_index_t[n + 1];
		node_index_t *node_index_rev = new node_index_t[n + 1];
		node_index_init(node_index, n + 1);
		node_index_init(node_index_rev, n + 1);

//		fprintf(stderr, "will perform add_edge\n");
		for (int i = 0; i < m; ++i) {
			int a, b, c;
			scanf("%d%d%d", &a, &b, &c);
			add_edge(node_index, a, b, c);
			add_edge(node_index_rev, b, a, c);
		}
		
		int *map = new int[n + 1];
//		fprintf(stderr, "will perform bfs-pre\n");
		bfs_pre(node_index_rev, n, map);
		
//		print_array(map, n + 1);

//		fprintf(stderr, "will perform bfs\n");
		printf("%d\n", bfs(node_index, n, k, p, map));
		
//		fprintf(stderr, "will perform clean edge\n");
		clean_edge(node_index, n + 1);
		clean_edge(node_index_rev, n + 1);
		
//		fprintf(stderr, "will perform delete index\n");
		delete[] node_index;
		delete[] node_index_rev;
		delete[] map;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

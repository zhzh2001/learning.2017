#include <cstdio>
#include <cstring>
#include <algorithm>

#define llu unsigned long long

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	llu a, b;
	scanf("%llu%llu", &a, &b);
	if (a > b) std::swap(a, b);

	llu t0 = (a - 1) * b;
	llu t1 = t0 % a;
	llu t2 = t0 / a;
	llu ans = (t2 - 1) * a + t1;
	
	printf("%llu\n", ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

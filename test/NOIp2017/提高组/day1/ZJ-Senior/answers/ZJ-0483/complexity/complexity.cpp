#include <iostream>
#include <string>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <sstream>

using std::string;
using std::cin;
using std::endl;
using std::cout;
using std::max;
using std::stringstream;
using std::cerr;

struct for_t {
	for_t(){}
	for_t(int _varible_id, int _flag, int _comp) {
		varible_id = _varible_id;
		flag = _flag;
		comp = _comp;
	}
	int varible_id;
	int flag;
	int comp;
};

int str2int(string str) {
	int ret;
	sscanf(str.c_str(), "%d", &ret);
	return ret;
}

int program_parse(string lines[], int n_line) {
	int max_comp = 0;
	bool varible_used['z' - 'a' + 1];
	memset(varible_used, 0, sizeof(varible_used));

	int for_stack_p = 0;
	for_t for_stack[101];
	memset(for_stack, 0, sizeof(for_stack));

	for (int i = 0; i < n_line; ++i) {
		if (lines[i][0] == 'F') {
			string keyword;
			string varible_name;
			string start_cond;
			string end_cond;
			stringstream ss;
			ss << lines[i];
			ss >> keyword >> varible_name >> start_cond >> end_cond;
			int varible_id = varible_name[0] - 'a';
			if (varible_used[varible_id]) return -1;
			varible_used[varible_id] = true;
			
			int flag = 0;
			if (str2int(start_cond) > str2int(end_cond)) flag = -1;
			else if (end_cond == "n" && start_cond != "n") flag = 1;
			else if (end_cond != "n" && start_cond == "n") flag = -1;
			
			//cerr << "flag = "<< flag << endl;
			int ap = (flag == 1)? 1 : 0;
			
			if (for_stack_p == 0)
				for_stack[for_stack_p] = for_t(varible_id, flag, ap);
			else {
				if (for_stack[for_stack_p - 1].flag == -1 || flag == -1) {
					//cerr << "force comp = 0" << endl;
					for_stack[for_stack_p] = for_t(varible_id, flag, 0);
				}
				else {
					//cerr << "comp + ap" << endl;
					for_stack[for_stack_p] = for_t(varible_id, flag, for_stack[for_stack_p - 1].comp + ap);
				}
			}
			max_comp = max(max_comp, for_stack[for_stack_p].comp);
			++for_stack_p;
		}
		else if (lines[i][0] == 'E') {
			--for_stack_p;
			if (for_stack_p == -1) return -1;
			varible_used[for_stack[for_stack_p].varible_id] = false;
		}
	}
	if (for_stack_p != 0) return -1;
	return max_comp;
}

int comp_decode(string str_c) {
	if (str_c == "O(1)")
		return 0;
	int ret;
	sscanf(str_c.c_str(), "O(n^%d)", &ret);
	return ret;
}

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	
	int t;
	cin >> t;
	
	for (int ti = 0; ti < t; ++ti) {
		int l;
		cin >> l;
		string str_c;
		cin >> str_c;
		int c = comp_decode(str_c);
		//cerr << "c_int = " << c << endl;
		//cerr << "str_c = " << str_c << endl;
		string lines[101];
		getline(cin, lines[0]);
		for (int i = 0; i < l; ++i) {
			getline(cin, lines[i]);
			//cerr << "line = " << lines[i] << endl;
		}
		int result = program_parse(lines, l);
		//cerr << "result = " << result << endl;
		if (result == -1)
			cout << "ERR" << endl;
		else if (result == c)
			cout << "Yes" << endl;
		else
			cout << "No" << endl;
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include<cstdio>
#include<cstring>
int T,L,i,k,h,t1,t2,ans;
char CE,END,f[128],Com[16],s1[8],s2[8],s3[8],s4[8],mul[102],st[102];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for(scanf("%d",&T);T--;)
	{
		memset(f,END=CE=h=k=ans=0,sizeof f);
		for(scanf("%d%s",&L,Com);L--;)
		{
			scanf("%s",s1);
			if(*s1=='F')
			{
				scanf("%s%s%s",s2,s3,s4);
				if(CE)continue;
				if(f[*s2]){CE=1;continue;}
				f[st[++h]=*s2]=1;
				sscanf(s3,"%d",&t1),sscanf(s4,"%d",&t2);
				if(END)continue;
				if(*s3=='n')
				{
					if(*s4!='n')END=h;
				}else
				{
					if(*s4!='n')t1>t2?END=h:0;else ++k,mul[h]=1;
				}
				if(k>ans)ans=k;
			}else
			{
				if(!h)CE=1;
				if(CE)continue;
				k-=mul[h],mul[h]=f[st[h]]=0;
				if(END==h)END=0;
				--h;
			}
		}
		if(CE||h)puts("ERR");else
		{
			if(Com[2]=='1')k=0;else
			sscanf(Com+4,"%d",&k);
			puts(k==ans?"Yes":"No");
		}
	}
}

#include<cstdio>
#include<cstring>
const int N=1e5+2,M=N*4;
char c;
int T,i,j,n,m,L,P,t,x,mx,cnt,ans,d[N],fa[N],en[M],nxt[M],w[M];
inline int R()
{
	for(x=0;c=getchar(),c<48||c>57;);
	for(;c>47&&c<58;c=getchar())x=x*10+c-48;
	return x;
}
void spfa()
{
	static int i,h,t,u,v,vis[N],q[M*9];
	for(memset(vis+1,d[q[h=t=1]=n]=0,4*n);h<=t;)
		for(vis[u=q[h++]]=0,i=fa[u];i;i=nxt[i])if(i+1&1)
			if(d[u]+w[i]<d[v=en[i]])
				if(d[v]=d[u]+w[i],!vis[v])
					vis[q[++t]=v]=1;
}
void dfs(int u,int dis,int dep)
{
	if(dep>mx){ans=cnt=2e9;return;}
	if(++cnt>5e8)return;
	if(u==n)++ans;
	for(int i=fa[u];i;i=nxt[i])if(i&1)
		if(dis+w[i]+d[en[i]]<=L)dfs(en[i],dis+w[i],dep+1);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park_.out","w",stdout);
	for(T=R();T--;){
	n=R(),m=R(),L=R(),P=R();
	memset(fa+1,cnt=ans=t=0,4*n);
	for(memset(d+1,64,4*n);m--;)
	{
		i=R(),j=R();
		en[++t]=j,nxt[t]=fa[i],fa[i]=t,w[t]=R();
		en[++t]=i,nxt[t]=fa[j],fa[j]=t,w[t]=w[t-1];
	}
	spfa(),mx=L*(n-2)+3,L+=d[1],dfs(1,0,1);
	printf("%d\n",ans>2e8?-1:ans%P);}
}

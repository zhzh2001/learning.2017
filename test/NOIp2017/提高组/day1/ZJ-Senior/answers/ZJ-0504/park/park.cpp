#include<cstdio>
#include<cstring>
const int N=1e5+2,M=N*2;
char c,fl,vis[N],g[N][51];
int T,i,j,n,m,L,P,t,x,ans,d[N],fa[N],en[M],nxt[M],w[M],q[M*9],f[N][51];
inline int R()
{
	for(x=0;c=getchar(),c<48||c>57;);
	for(;c>47&&c<58;c=getchar())x=x*10+c-48;
	return x;
}
void spfa()
{
	int i,h,t,u,v;
	for(memset(vis+1,d[q[h=t=1]=n]=0,n);h<=t;)
		for(vis[u=q[h++]]=0,i=fa[u];i;i=nxt[i])
			if(d[u]+w[i]<d[v=en[i]])
				if(d[v]=d[u]+w[i],!vis[v])
					vis[q[++t]=v]=1;
}
int F(int u,int x)
{
	if(g[u][x])fl=1;
	if(fl)return 0;
	if(~f[u][x]&&(u!=1||x))return f[u][x];
	int res=u==1&&!x,i=fa[u];
	for(g[u][x]=1;i;i=nxt[i])
		if((t=x-w[i]-d[u]+d[en[i]])>=0)res=(res+F(en[i],t))%P;
	return g[u][x]=0,f[u][x]=res;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(T=R();T--;){
	n=R(),m=R(),L=R(),P=R();
	memset(fa+1,fl=ans=t=0,4*n);
	for(memset(d+1,64,4*n);m--;)
	{
		i=R(),j=R();
		en[++t]=i,nxt[t]=fa[j],fa[j]=t,w[t]=R();
	}
	spfa(),memset(f+1,-1,204*n);
	for(i=0;i<=L;ans=(ans+F(n,i++))%P);
	printf("%d\n",fl?-1:ans);}
}

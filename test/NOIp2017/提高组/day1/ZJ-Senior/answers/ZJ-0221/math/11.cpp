#include<bits/stdc++.h>
using namespace std;
void exgcd(long long a,long long b,long long &x,long long &y)
{
	if (b==0) {x=1;y=0;return;}
	long long px,py;exgcd(b,a%b,px,py);
	x=py;y=px-a/b*py;
}
int main()
{
	long long a,b,x,y;cin>>a>>b;
	exgcd(a,b,x,y);
	//cout<<x<<' '<<y<<endl;
	if (x<0) {swap(x,y);swap(a,b);}
}

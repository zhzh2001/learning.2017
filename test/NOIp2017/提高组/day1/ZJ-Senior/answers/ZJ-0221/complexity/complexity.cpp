#include<bits/stdc++.h>
using namespace std;
char ls[15],ls2[15];
bool mp[300];
int num[300],st[300],n,tim,top;
int pend(char st[])
{
	int len=strlen(st);
	if ((len==1)&&(st[0]=='n')) return -1;
	else if (len==1) return st[0]-'0';
	else if (len==2) return (st[0]-'0')*10+(st[1]-'0');
}
pair<int,int> work(int bg)
{
	int now=bg+1,ans=0;
	while (num[now]!=-2)
	{
		pair<int,int> p=work(now);
		ans=max(ans,p.first);
		now=p.second+1;
	}
	if (num[bg]==-1) ans=0;else if (num[bg]==1) ans++;
	return make_pair(ans,now);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t;scanf("%d\n",&t);
	while (t--)
	{
		memset(mp,0,sizeof(mp));
		top=0;bool err=false;scanf("%d %s\n",&n,ls);int len=strlen(ls);
		if (len==4) tim=0;else if (len==6) tim=ls[4]-'0';else if (len==7) tim=(ls[4]-'0')*10+(ls[5]-'0');
		for (int i=1;i<=n;i++)
		{
			char cc;scanf("%c",&cc);
			if (cc=='E')
			{
				if (top==0) {err=true;top++;}
				mp[st[top]]=false;
				top--;
				num[i]=-2;
				scanf("\n");
			}
			else
			{
				char c;scanf(" %c %s %s\n",&c,ls,ls2);
				if (mp[c]) err=true;else mp[c]=true;
				st[++top]=c;
				int l=pend(ls),r=pend(ls2);
				if ((l==-1)&&(r==-1)) num[i]=0;
				else if (l==-1) num[i]=-1;
				else if (r==-1) num[i]=1;
				else if (l<=r) num[i]=0;
				else num[i]=-1;
			}
		}
		if (top) err=true;
		if (err) {printf("ERR\n");continue;}
		num[0]=0;num[n+1]=-2;
		int ans=work(0).first;//bu xun huan
		if (ans==tim) printf("Yes\n");else printf("No\n");
	}
}

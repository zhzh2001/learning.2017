#include<bits/stdc++.h>
using namespace std;
int n,m,d,p,js,dis[200010],nidis[200010],cop[200010],inn[200010],go[200010],f[200010][55];
vector <int> des[200010],len[200010],nides[200010],nilen[200010];
bool err[200010];
struct node
{
	int id,key;
	friend bool operator <(node a,node b){return a.key>b.key;}
};
priority_queue <node>que;
void shortestpath()
{
	for (int i=1;i<=n;i++) dis[i]=700000000;
	dis[1]=0;que.push((node){1,0});
	while (!que.empty())
	{
		node p=que.top();que.pop();
		if (p.key!=dis[p.id]) continue;
		int s=p.id;
		for (int k=0;k<des[s].size();k++) if (dis[s]+len[s][k]<dis[des[s][k]])
		{
			dis[des[s][k]]=dis[s]+len[s][k];
			que.push((node){des[s][k],dis[des[s][k]]});
		}
	}
}
void nishortestpath()
{
	for (int i=1;i<=n;i++) nidis[i]=700000000;
	nidis[n]=0;que.push((node){n,0});
	while (!que.empty())
	{
		node p=que.top();que.pop();
		if (p.key!=nidis[p.id]) continue;
		int s=p.id;
		for (int k=0;k<nides[s].size();k++) if (nidis[s]+nilen[s][k]<nidis[nides[s][k]])
		{
			nidis[nides[s][k]]=nidis[s]+nilen[s][k];
			que.push((node){nides[s][k],nidis[nides[s][k]]});
		}
	}
}

bool topsort()
{
	for (int i=1;i<=n;i++) inn[i]=go[i]=cop[i]=0;
	js=0;
	for (int s=1;s<=n;s++) if (!err[s])
	{
		js++;
		for (int k=0;k<des[s].size();k++) if ((nidis[des[s][k]]+len[s][k]==nidis[s])&&(!err[des[s][k]])) inn[des[s][k]]++;
	}
	int head=0,tail=0;
	for (int i=1;i<=n;i++) if ((inn[i]==0)&&(!err[i])) cop[++head]=i;
	while (head!=tail)
	{
		int s=cop[++tail];
		for (int k=0;k<des[s].size();k++) if ((nidis[des[s][k]]+len[s][k]==nidis[s])&&(!err[des[s][k]]))
		{
			inn[des[s][k]]--;
			if (inn[des[s][k]]==0) cop[++head]=des[s][k];
		}
	}
	for (int i=1;i<=head;i++) go[cop[i]]=i;
	return (head==js);
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t;scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d%d",&n,&m,&d,&p);
		for (int i=1;i<=n;i++) {des[i].clear();nides[i].clear();len[i].clear();nilen[i].clear();err[i]=false;}
		for (int i=1;i<=m;i++)
		{
			int u,v,w;scanf("%d%d%d",&u,&v,&w);
			des[u].push_back(v);len[u].push_back(w);
			nides[v].push_back(u);nilen[v].push_back(w);
		}
		shortestpath();
		nishortestpath();
		for (int i=1;i<=n;i++) if (dis[i]+nidis[i]>dis[n]+d) err[i]=true;else err[i]=false;
		if (!topsort()) {printf("-1\n");continue;}
		for (int j=0;j<=d;j++)
		for (int i=1;i<=js;i++)
		{
			if ((cop[i]==1)&&(j==0)) {f[i][j]=1;continue;}
			f[i][j]=0;int s=cop[i];
			for (int k=0;k<nides[s].size();k++) if ((!err[nides[s][k]])&&(j-(nilen[s][k]-(nidis[nides[s][k]]-nidis[s]))>=0))
			{
				int ad=f[go[nides[s][k]]][j-(nilen[s][k]-(nidis[nides[s][k]]-nidis[s]))];
				(f[i][j]+ad>=p)?(f[i][j]+=ad-p):(f[i][j]+=ad);
			}
		}
		int ans=0;for (int i=0;i<=d;i++) (ans+f[go[n]][i]>=p)?(ans+=f[go[n]][i]-p):(ans+=f[go[n]][i]);
		printf("%d\n",ans);
	}
}

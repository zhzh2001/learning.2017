#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<cmath>
#include<vector>
#define ll long long
#define inf 0x3f3f3f3f
using namespace std;

inline int read()
{
	int x = 0,w = 1;char c = getchar();
	while (c < '0'|| c > '9'){if (c == '-') w = -1;c = getchar();}
	while (c >= '0' && c <= '9') x = (x << 3) + (x << 1) + c - '0',c = getchar();
	return x * w;
}

int n,val;
bool vis[1000],f[1000];
char x[1000];

bool cmp(char* a,char *b)
{
	if (a[0] == 'n' || b[0] == 'n') return 0;
	if (strlen(a) > strlen(b)) return 1;
	for (int i = 0; i < strlen(a); ++i)
	{
		if (a[i] > b[i]) return 1;
		if (a[i] < b[i]) return 0;
	}
	return 0;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int t = read() + 1;
	while (--t)
	{
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		n = read();
		char s[100];
		scanf("%s",s);
		getchar();
		val = 0;
		if (s[2] == 'n')
			for (int i = 4; i < strlen(s) - 1; ++i) val = val * 10 + s[i] - '0';
		int tmp = 0,ans = 0,dep = 0,flagd = 0;
		bool err = 0,flag = 1;
		for (int i = 0; i < n; ++i)
		{
			char type = getchar();
			getchar();
			if (type == 'F')
			{
				char I = getchar(),a[5],b[5];
				scanf("%s%s",a,b);
				getchar();
				if (vis[I]) err = 1;
				vis[I] = 1;
				if (flag && (a[0] == 'n' && b[0] != 'n' || cmp(a,b))) flag = 0,flagd = dep + 1;
				x[++dep] = I;
				if (b[0] == 'n' && a[0] != 'n') ++tmp,f[dep] = 1;
				else f[dep] = 0;
				if (flag) ans = max(ans,tmp);
			}
			else
			{
				if (f[dep]) --tmp;
				if (!flag && flagd == dep) flag = 1;
				vis[x[dep]] = 0;
				--dep;
				if (dep < 0) err = 1;
			}
		}
		if (dep != 0) err = 1;
		if (err) printf("ERR\n");
		else if (ans == val) printf("Yes\n");
			 else printf("No\n");
	}
	
	return 0;
}

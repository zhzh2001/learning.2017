#include<iostream>
#include<cstdio>
#include<cstring>
#define cur1 edge1[i].v
#define lt1 edge1[i].len
#define nxt1 edge1[i].next
#define cur2 edge2[i].v
#define lt2 edge2[i].len
#define nxt2 edge2[i].next
#define inf 0x3f3f3f3f
#define Maxn 100005
using namespace std;

inline int read()
{
	int x = 0,w = 1;char c = getchar();
	while (c < '0'|| c > '9'){if (c == '-') w = -1;c = getchar();}
	while (c >= '0' && c <= '9') x = (x << 3) + (x << 1) + c - '0',c = getchar();
	return x * w;
}

struct E
{
	int v,len,next;
}edge1[200005],edge2[200005];

int pt1,pt2,head1[Maxn],head2[Maxn],n,m,k,p;

inline void add_edge1(int u,int v,int l)
{
	edge1[++pt1].next = head1[u];
	head1[u] = pt1;
	edge1[pt1].v = v,edge1[pt1].len = l;
}

inline void add_edge2(int u,int v,int l)
{
	edge2[++pt2].next = head2[u];
	head2[u] = pt2;
	edge2[pt2].v = v,edge2[pt2].len = l;
}

bool vis[Maxn];

bool dfs1(int x)
{
	vis[x] = 1;
	for (int i = head1[x]; i != -1; i = nxt1)
		if (lt1 == 0)
			if (!vis[cur1])
				if (dfs1(cur1)) return 1;
				else;
			else return 1;
	return 0;
}

int dis[Maxn],list[Maxn * 1000];
bool f[Maxn];

void spfa2()
{
	memset(dis,inf,sizeof(dis));
	memset(f,0,sizeof(f));
	dis[n] = 0;
	int H = -1,T = 0;
	list[0] = n,f[n] = 1;
	while (H < T)
	{
		int u = list[++H];
		f[u] = 0;
		for (int i = head2[u]; i != -1; i = nxt2)
			if (dis[u] + lt2 < dis[cur2])
			{
				dis[cur2] = dis[u] + lt2;
				if (!f[cur2]) f[cur2] = 1,list[++T] = cur2;
			}
	}
}

int ans;

void dfs2(int x,int l)
{
	if (x == n) ans = (ans + 1) % p;
	for (int i = head1[x]; i != -1; i = nxt1)
		if (lt1 + l + dis[cur1] <= dis[1] + k) dfs2(cur1,l + lt1);
}

int d[Maxn];

void spfa1()
{
	memset(d,inf,sizeof(d));
	memset(f,0,sizeof(f));
	d[1] = 0;
	int H = -1,T = 0;
	list[0] = 1,f[1] = 1;
	while (H < T)
	{
		int u = list[++H];
		f[u] = 0;
		for (int i = head1[u]; i != -1; i = nxt1)
			if (d[u] + lt1 < d[cur1])
			{
				d[cur1] = d[u] + lt1;
				if (!f[cur1]) f[cur1] = 1,list[++T] = cur1;
			}
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int t = read() + 1;
	while (--t)
	{
		n = read(),m = read(),k = read(),p = read();
		for (int i = 1; i <= n; ++i) head1[i] = head2[i] = -1;
		pt1 = pt2 = -1;
		for (int i = 0; i < m; ++i)
		{
			int u = read(),v = read(),l = read();
			add_edge1(u,v,l),add_edge2(v,u,l);
		}
		spfa1();
		spfa2();
		bool flag = 0;
		for (int i = 1; i <= n; ++i)
		if (d[i] + dis[i] <= dis[1] + k)
			{
				memset(vis,0,sizeof(vis));
				if (dfs1(i))
				{
					flag = 1;
					break;
				}
			}
		if (flag)
		{
			printf("-1\n");
			continue;
		}
		ans = 0;
		dfs2(1,0);
		printf("%d\n",ans);
	}
	
	return 0;
}

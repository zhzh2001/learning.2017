#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
#include<vector>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
using namespace std;

inline ll read()
{
	ll x = 0,w = 1;char c = getchar();
	while (c < '0'|| c > '9'){if (c == '-') w = -1;c = getchar();}
	while (c >= '0' && c <= '9') x = (x << 3) + (x << 1) + c - '0',c = getchar();
	return x * w;
}

ll a,b;

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a = read(),b = read();
	printf("%lld",(a - 1) * b - a);
	
	return 0;
}

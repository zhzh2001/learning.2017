#include<cstring>
#include<cstdio>

using namespace std;

struct prgm{
	int ty,ii,xi,yi;
};

const int MAXN=105;

int T,n,ans,st[MAXN],res[MAXN];
prgm a[MAXN];
bool vis[30];

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9')
	{
		if (ch=='F') return 1;
		if (ch=='E') return 2;
		if (ch=='n') return 1000;
		if (ch>='a'&&ch<='z') return ch-'a';
		ch=getchar();
	}
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
bool Wrong()
{
	if (n&1) return 1;
	int Len=0;memset(vis,0,sizeof(vis));memset(st,0,sizeof(st));
	for (int i=1;i<=n;i++)
	  if (a[i].ty==1)
	  {
	  	if (vis[a[i].ii]) return 1;
	  	Len++;st[Len]=a[i].ii;vis[a[i].ii]=1;
	  }
	  else
	  {
	  	if (!Len) return 1;
	  	vis[st[Len]]=0;Len--;
	  }
	if (Len) return 1;else return 0;
}
int Max(int x,int y) {return x>y?x:y;}
bool check()
{
	int Len=0;memset(st,0,sizeof(st));memset(res,0,sizeof(res));
	for (int i=1;i<=n;i++)
	  if (a[i].ty==1)
	  {
	  	Len++;st[Len]=i;
	  }
	  else
	  {
	  	if (a[st[Len]].xi>a[st[Len]].yi) res[st[Len]]=0;
	  	else if (a[st[Len]].xi!=1000&&a[st[Len]].yi==1000) res[st[Len]]++;
	  	res[st[Len-1]]=Max(res[st[Len-1]],res[st[Len]]);
	  	Len--;
	  }
	return (res[0]==ans);
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while (T--)
	{
		n=read();
		char ch=getchar();while (ch!='(') ch=getchar();
		ch=getchar();if (ch=='1') ans=0;else ans=read();
		for (int i=1;i<=n;i++)
		{
			a[i].ty=read();
			if (a[i].ty==1) a[i].ii=read(),a[i].xi=read(),a[i].yi=read();
		}
		if (Wrong()) printf("ERR\n");else if (check()) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

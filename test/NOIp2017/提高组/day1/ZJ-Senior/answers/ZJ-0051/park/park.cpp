#include<cstring>
#include<cstdio>

using namespace std;

const int MAXN=100005,MAXM=200005;

int tp,cc,cnt,sz[MAXN],st[MAXN],Who[MAXN],dfn[MAXN],Low[MAXN];
int tot[3],lnk[3][MAXN],Son[3][MAXM],w[3][MAXM],nxt[3][MAXM];
int T,N,M,K,P,ans,f[MAXN][60];
int q[MAXN],dst[2][MAXN];
bool vis[MAXN];

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int Min(int x,int y) {return x<y?x:y;}
void Inc(int &x,int y) {x+=y;if (x>=P) x-=P;}
void add(int ty,int x,int y,int z) {tot[ty]++;Son[ty][tot[ty]]=y;w[ty][tot[ty]]=z;nxt[ty][tot[ty]]=lnk[ty][x];lnk[ty][x]=tot[ty];}
void spfa(int ty)
{
	memset(vis,0,sizeof(vis));memset(dst[ty],63,sizeof(dst[ty]));
	int H=0,T=1;if (ty==0) q[1]=1;else q[1]=N;vis[q[1]]=1;dst[ty][q[1]]=0;
	while (H!=T)
	{
		H=(H+1)%MAXN;int x=q[H];vis[x]=0;
		for (int j=lnk[ty][x];j;j=nxt[ty][j])
		  if (dst[ty][x]+w[ty][j]<dst[ty][Son[ty][j]])
		  {
		  	dst[ty][Son[ty][j]]=dst[ty][x]+w[ty][j];
		  	if (!vis[Son[ty][j]]) T=(T+1)%MAXN,q[T]=Son[ty][j],vis[q[T]]=1;
		  }
	}
}
void Tarjan(int x)
{
	dfn[x]=Low[x]=++cnt;vis[x]=1;st[++tp]=x;
	for (int j=lnk[2][x];j;j=nxt[2][j])
	  if (!dfn[Son[2][j]]) {Tarjan(Son[2][j]);Low[x]=Min(Low[x],Low[Son[2][j]]);}
	  else if (vis[Son[2][j]]) Low[x]=Min(Low[x],dfn[Son[2][j]]);
	if (dfn[x]==Low[x])
	{
		cc++;sz[cc]=0;
		while (st[tp]!=x) {vis[st[tp]]=0;Who[st[tp]]=cc;sz[cc]++;tp--;}
		vis[st[tp]]=0;Who[st[tp]]=cc;sz[cc]++;tp--;
	}
}
int dfs(int x,int y)
{
	if (y<0||y>K||dst[0][x]+y+dst[1][x]>dst[0][N]+K) return 0;
	if (f[x][y]>=0) return f[x][y];
	f[x][y]=0;
	for (int j=lnk[1][x];j;j=nxt[1][j]) Inc(f[x][y],dfs(Son[1][j],y+dst[0][x]-w[1][j]-dst[0][Son[1][j]]));
	return f[x][y];
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while (T--)
	{
		N=read(),M=read(),K=read(),P=read();ans=0;
		memset(tot,0,sizeof(tot));memset(lnk,0,sizeof(lnk));
		for (int i=1;i<=M;i++)
		{
			int x=read(),y=read(),z=read();
			add(0,x,y,z);add(1,y,x,z);if (!z) add(2,x,y,z);
		}
		spfa(0);spfa(1);
		cnt=tp=cc=0;memset(dfn,0,sizeof(dfn));memset(Low,0,sizeof(Low));memset(vis,0,sizeof(vis));
		for (int i=1;i<=N;i++) if (!dfn[i]) Tarjan(i);
		for (int i=1;i<=N;i++) if (dst[0][i]+dst[1][i]<=dst[0][N]+K&&sz[Who[i]]>1) ans=-1;
		if (ans==-1) {printf("-1\n");continue;}
		memset(f,255,sizeof(f));f[1][0]=1;for (int k=0;k<=K;k++) Inc(ans,dfs(N,k));
		printf("%d\n",ans);
	}
	return 0;
}

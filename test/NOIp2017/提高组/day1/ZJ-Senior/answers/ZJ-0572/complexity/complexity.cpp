#include <cstdio>
#define MAXL 100
int t,L,i,S[MAXL],E[MAXL],w;
#define MAXLEN 13
char C[7],P[MAXLEN + 1],V[MAXL];
bool F[MAXL];
#define R register
#include <cctype>
inline char * G(R char * B,R int & N)
{
	if (* B != 'n')
	{
		N = 0;
		while (std::isdigit(* B))
			N = 10 * N + (* (B ++) ^ 48);
	}
	else
		N = 100,++ B;
	return ++ B;
}
#include <cstring>
bool U[123];
#include <stack>
#include <algorithm>
int L__(R int S_,R int E_)
{
	if (S[S_] <= E[S_])
	{
		R int i,j,D,M;
		for (D = M = 0,i = j = S_ + 1;i < E_ - 1;M = std::max(M,L__(i,j)),i = j)
			do
				F[j] ? ++ D : -- D,++ j;
			while (D);
		return M + (S[S_] < 100 && E[S_] < 100 || S[S_] == 100 && E[S_] == 100 ? 0 : 1);
	}
	return 0;
}
inline int L_()
{
	R int i,j,D,M;
	for (D = M = 0,i = j = 0;i < L;M = std::max(M,L__(i,j)),i = j)
		do
			F[j] ? ++ D : -- D,++ j;
		while (D);
	return M;
}
int main()
{
	std::freopen("complexity.in","r",stdin),std::scanf("%d",&  t),std::freopen("complexity.out","w",stdout);
	while (t --)
	{
		for (std::scanf("%d O(%s\n",& L,C),i = 0;i < L;++ i)
		{
			std::fgets(P,MAXLEN,stdin);
			if (P[0] == 'F')
				F[i] = 1,V[i] = P[2],G(G(P + 4,S[i]),E[i]);
			else
				F[i] = 0;
		}
		std::stack <char> S_;
		for (std::memset(U + 97,0,26),i = 0;i < L;++ i)
			if (F[i])
				if (! U[V[i]])
					S_.push(V[i]),U[V[i]] = 1;
				else
					break;
			else
				if (! S_.empty())
					U[S_.top()] = 0,S_.pop();
				else
					break;
		i == L && S_.empty() ? C[0] == '1' ? w = 0 : std::sscanf(C + 2,"%d",& w),std::puts(L_() == w ? "Yes" : "No") : std::puts("ERR");
	}
}

#include <cstdio>
int T,N,M,K,P,i,ai,bi,ci,d,L;
#include <cstring>
#include <queue>
struct
{
	inline void Init(int n)
	{
		cnt = 0,std::memset(idx + 1,0,n << 2),std::memset(d + 2,0x3f,n - 1 << 2);
	}
	inline void AddEdge(int u,int v,int w)
	{
		edge[++ cnt].to = v,edge[cnt].wt = w,edge[cnt].nxt = idx[u],idx[u] = cnt,ans = 0;
	}
	inline int SPFA(int n)
	{
		q.push(1);
		register int temp,i,temp_;
		while (! q.empty())
		{
			temp = q.front(),q.pop(),in[temp] = 0;
			for (i = idx[temp];i;i = edge[i].nxt)
				if (d[temp] + edge[i].wt < d[temp_ = edge[i].to])
				{
					d[temp_] = d[temp] + edge[i].wt;
					if (! in[temp_])
						q.push(temp_),in[temp_] = 1;
				}
		}
		return d[n];
	}
	int ans;
	void dfs(const int & n,const int & t)
	{
		if (n == N)
			++ ans;
		for (register int i = idx[n],temp;i;i = edge[i].nxt)
			if ((temp = t + edge[i].wt) <= L)
				dfs(edge[i].to,temp);
	}
	private:
		int cnt,idx[100001],d[100001];
		struct
		{
			int to,wt,nxt;
		}edge[200001];
		std::queue <int> q;
		bool in[100001];
}G;//����
int main()
{
	std::freopen("park.in","r",stdin),std::scanf("%d",& T),std::freopen("park.out","w",stdout);
	while (T --)
	{
		for (std::scanf("%d%d%d%d",& N,& M,& K,& P),G.Init(N),i = 0;i < M;++ i)
			std::scanf("%d%d%d",& ai,& bi,& ci),G.AddEdge(ai,bi,ci);
		L = (d = G.SPFA(N)) + K;
		if (N <= 1000 && ci != 0)
			G.dfs(1,0),std::printf("%d\n",G.ans % P);
		else
			std::puts("-1");
	}
}//��0

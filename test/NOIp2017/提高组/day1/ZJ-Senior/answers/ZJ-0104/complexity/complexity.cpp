#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
#include<string>
#define open(s) ios::sync_with_stdio(false); freopen(s".in","r",stdin); freopen(s".out","w",stdout);
#define close fclose(stdin); fclose(stdout);
using namespace std;
vector<string> vec;
vector<int> onn;
int n;
int checkon(){
	int ans=0;
	for(int i=0;i<onn.size();i++) ans+=onn[i];
	return ans;
}
int checkvar(string x){
	for(int i=vec.size()-1;i>=0;i--){
		if(vec[i]==x) return 0;
	}
	return 1;
}
int prog(int on,int k){
	onn.clear();
	vec.clear();
	int oreal=0;
	string s;
	int onf=0;
	for(int i=1;i<=k;i++){
		cin>>s;
			if(s=="F"){
				string varr,from,to;
				int ifrom=-1,ito=-1;
				cin>>varr>>from>>to;
				if(checkvar(varr)){
					int nowo=0;
					vec.push_back(varr);
					if(from!="n"){
						ifrom=(int)from[0]-(int)'0';
						for(int j=1;j<from.length();j++){
							ifrom=ifrom*10+(int)from[j]-(int)'0';
						}
					}
					else if(!onf){
						ifrom=101;
						nowo=1;
					}
					if(to!="n"){
						ito=(int)to[0]-(int)'0';
						for(int j=1;j<to.length();j++){
							ito=ito*10+(int)to[j]-(int)'0';
						}
					}
					else if(!onf){
						ito=101;
						nowo=1;
					}
					if (!onf && from=="n" && to=="n"){
						nowo=0;
					}
					if(ifrom>ito){
						onf=vec.size();
						nowo=0;
					}
					onn.push_back(nowo);
					int t=checkon();
					if(t>oreal) oreal=t;
					//cout<<"from="<<from<<" to="<<to<<" ifrom="<<ifrom<<" ito="<<ito<<" oreal="<<oreal<<endl;
				}
				else{
					cout<<"ERR"<<endl;
					return 1;
				}
			}
			else{
				if(vec.size()==onf) onf=0;
				vec.pop_back();
				onn.pop_back();
			}
			//for(int i=0;i<vec.size();i++) cout<<vec[i]<<" ";
			//cout<<endl;
	}
	if(vec.size()>0){
		cout<<"ERR"<<endl;
		return 1;
	}
	if(oreal!=on){
		cout<<"No"<<endl;
		return 0;
	}
	cout<<"Yes"<<endl;
	return 0;
}
int main(){
	open("complexity");
	cin>>n;
	int situ=0;
	while(n--){
		int k,on=0;
		string s;
		if(situ){
			while(true){
			string a,b;
			cin>>a;
			while(a[0]>'9' || a[0]<'0') cin>>a;
			cin>>b;
			if(b[0]=='O' && b[1]=='('){
				k=(int)a[0]-(int)'0';
				for(int j=1;j<a.length();j++){
					k=k*10+(int)a[j]-(int)'0';
				}
				s=b;
				situ=0;
				break;
			}
			}
		}
		else{
			cin>>k;
			cin>>s;
		}
		if(s[3]=='1') on=0;
		else {
			int num=(int)s[5]-(int)'0';
			for(int j=6;s[j]!=')';j++){
				num=num*10+(int)s[j]-(int)'0';
			}
			on=num;
		}
		//cout<<"on="<<on<<" k="<<k<<endl;
		situ=prog(on,k);
	}
	close;
	return 0;
}

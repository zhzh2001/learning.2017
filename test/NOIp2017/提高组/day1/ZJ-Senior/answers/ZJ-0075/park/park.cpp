#include<bits/stdc++.h>
#define sight(c) ('0'<=c&&c<='9')
#define M 200007
#define N 100007
#define EK 600000000
#define INF (50000000000007LL)
#define eho(x) for(int i=head[x];i;i=net[i])
#define getchar nc
using namespace std;
long long ans,f[N*52];
int fall[M],head[N],net[M],tot,cost[M],vis[N],x,y,X,dla,k,mo,n,g,T,m,a,b,co,tog,SIZ;
bool usd[N*52];//bool
queue<int> Q;
inline char nc() {
	static char buf[1000000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++; 
}
void read (int &x){
	static char c;static int b;
	for (b=1,c=getchar();!sight(c);c=getchar()) if (c=='-') b=-1;
	for (x=0;sight(c);c=getchar()) x=x*10+c-48;
	x*=b;
}
void add(int x,int y,int co){
	fall[++tot]=y; net[tot]=head[x]; head[x]=tot; cost[tot]=co;
}
void spfa(){
	memset(vis,36,sizeof vis);
	Q.push(1); usd[1]=1;vis[1]=0;
	while (!Q.empty()) {
		x=Q.front(); Q.pop();
		eho(x) 
		  if (vis[fall[i]]>vis[x]+cost[i]) {
		  	vis[fall[i]]=vis[x]+cost[i];
		  	if (!usd[fall[i]]) 
			   { usd[fall[i]]=1;
			     Q.push(fall[i]);}
		  }
		usd[x]=0;
	}
}
void spfa2() {
	Q.push(1*51); usd[51]=1; f[51]=1;
	while (!Q.empty()) {
		X=Q.front(); Q.pop(); x=X/51; dla=X%51;
		eho(x) 
		 if (vis[fall[i]]<EK&&vis[fall[i]]+k>=vis[x]+dla+cost[i]) {
		 	tog++;
		 	if (fall[i]==n) ans+=f[X];
		 	if (ans>INF) ans%=mo;
		 	g=fall[i]*51+vis[x]+dla+cost[i]-vis[fall[i]];
		 	f[g]+=f[X];
		 	if (f[g]>INF) f[g]%=mo;
		 	if (!usd[g])  {usd[g]=1;Q.push(g);}
		 }
	  usd[X]=0;f[X]=0;
	  if (tog>SIZ) {printf("-1\n"); return;}
	}
	printf("%lld\n",ans);
}
int main () {
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(T); SIZ=20000000/T;
	while (T--) {
		tog=0;
		memset(head,0,sizeof head); tot=0;ans=0;
		read(n); read(m); read(k); read(mo);
		while (m--) {
			read(a); read(b); read(co); add(a,b,co);}
		spfa();
		if (vis[n]>EK) {printf("0\n"); continue;}
		spfa2();
		}
}

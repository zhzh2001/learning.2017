#include<bits/stdc++.h>
#define sight(c) ('0'<=c&&c<='9')
#define INF 100000007
using namespace std;
int OO,T,l,ok,omax,top,x,y,ask[1007],o[1007],oans;
char F,ch[1007][1007],TLL[10007],t1[1007],t2[1007];
void read (int &x){
	static char c;static int b;
	for (b=1,c=getchar();!sight(c);c=getchar()) if (c=='-') b=-1;
	for (x=0;sight(c);c=getchar()) x=x*10+c-48;
	x*=b;
}
void readO(){
	static char c;
	c=getchar();
	while (c!='(') c=getchar();
	c=getchar(); if (c=='1') { OO=0; c=getchar(); while (c!='\n') c=getchar();return;}
	read(OO);c=getchar(); while (c!='\n') c=getchar();
}
void reads (char *g,int &x){
	int OOO=0;
	static char c;
	for (c=g[OOO++];!sight(c);c=g[OOO++]) if (c=='n') {x=INF; return;}
	for (x=0;sight(c);c=g[OOO++]) x=x*10+c-48;
}
int main () {
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	read(T);
	while (T--) {
		ok=0;omax=0;top=0;oans=0;OO=0;
		memset(ask,0,sizeof ask); memset(o,0,sizeof o);
		memset(ch,'\0',sizeof ch); memset(TLL,'\0',sizeof TLL);
		memset(t1,'\0',sizeof t1); memset(t2,'\0',sizeof t2);
		read(l);readO();
		while (l--) {			
		    gets(TLL);
			if (TLL[0]=='F') {
				TLL[0]=' ';
				sscanf(TLL,"%s %s %s",ch[++top],t1,t2);
				reads(t1,x); reads(t2,y);
				for (int i=1;i<top;i++)
				 if (strcmp(ch[top],ch[i])==0) {ok=1; break;}
				if (ask[top-1]==1) {ask[top]=1; o[top]=0;} else{
				if (x>y) ask[top]=1; else {
				 ask[top]=0;
				if (x<INF &&y==INF) { o[top]=1,oans++; omax=max(omax,oans);}
				else {o[top]=0;} 
				} }
			}
			if (TLL[0]=='E') {
			    oans-=o[top--];
			}
		}
	   if (ok||top) {puts("ERR"); continue;}
	   if (omax==OO) puts("Yes"); else puts("No");
	}
   return 0;
}

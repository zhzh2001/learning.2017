#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
#define N 100010
#define M 200010
inline int read(){
	char ch=getchar(); int x=0;
	for (;ch>'9'||ch<'0';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x;
}
struct node{
	int d,x;
	node(int d=0,int x=0):d(d),x(x){}
	bool operator<(const node&a) const{
		return d>a.d;
	}
};
int he[N],ne[M],e[M],va[M],cnt;
int he2[N],ne2[M],e2[M],va2[M],cnt2;
int he3[N],ne3[M],e3[M],va3[M],cnt3;
bool vis[N],ins[N];
int f[N],d[N],n,m,P,K,dfn[N],low[N],S[N],Scc,id,top;
int dp[N][60];
void add(int u,int v,int w){ne[++cnt]=he[u];he[u]=cnt;e[cnt]=v;va[cnt]=w;}
void add2(int u,int v){ne2[++cnt2]=he2[u];he2[u]=cnt2;e2[cnt2]=v;}
void add3(int u,int v,int w){ne3[++cnt3]=he3[u];he3[u]=cnt3;e3[cnt3]=v;va3[cnt3]=w;}
priority_queue<node> Q;
void dij(int s){
	memset(d,60,sizeof(d));
	Q.push(node(0,s)); d[s]=0; f[s]=1;
	while (!Q.empty()){
		int x=Q.top().x; Q.pop();
		if (vis[x]) continue;
		vis[x]=1;
		for (int i=he[x];i;i=ne[i])
			if (d[e[i]]>d[x]+va[i]){
				d[e[i]]=d[x]+va[i]; f[e[i]]=f[x];
				Q.push(node(d[e[i]],e[i]));
			}else if (d[e[i]]==d[x]+va[i]) (f[e[i]]+=f[x])%=P;
	}
}
/*bool dfs(int x){
	for (int i=he2[x];i;i=ne2[i]){
		if (dis[e2[i]]>dis[x]+va2[i]){
			dis[e2[i]]=dis[x]+va2[i];
			if (vis[e2[i]]) return 1;
			vis[e2[i]]=1;
			if (dfs(e2[i])) return 1;
			vis[e2[i]]=0;
		}
	}
	return 0;
} */
int DP(int x,int K){
//	if (x==1) return K==0?1:0;
	if (K==0) return f[x];
	if (dp[x][K]!=-1) return dp[x][K];
	int res=0;
	for (int i=he3[x];i;i=ne3[i]){
		int t=va3[i]-(d[x]-d[e3[i]]);
		if (t>K) continue;
		(res+=DP(e3[i],K-t))%=P;
	}
	dp[x][K]=res;
	return res;
}
void tarjan(int x){
	dfn[x]=low[x]=++id; S[++top]=x; ins[x]=1;
	for (int i=he2[x];i;i=ne2[i])
		if (!dfn[e2[i]]) tarjan(e2[i]),low[x]=min(low[x],low[e2[i]]);
		else if (ins[e2[i]]) low[x]=min(low[x],dfn[e2[i]]);
	if (dfn[x]==low[x]){
		int t=0; Scc++;
		while (t!=x){ins[t=S[top--]]=0;}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while (T--){
		n=read(),m=read(),K=read(),P=read();
		memset(he,0,sizeof(he)); memset(he2,0,sizeof(he2)); memset(he3,0,sizeof(he3));
		cnt=cnt2=cnt3=0;
		for (int i=1;i<=m;i++){
			int u=read(),v=read(),w=read();
			add(u,v,w);
			if (w==0) add2(u,v);
			add3(v,u,w);
		}
	//	puts("WTF");
	//	memset(vis,0,sizeof(vis)); memset(dis,60,sizeof(dis)); dis[1]=0; 
		Scc=0; id=0;
	//	if (dfs(1)){puts("-1"); continue;}
		memset(f,0,sizeof(f));
		memset(dfn,0,sizeof(dfn)); memset(low,0,sizeof(low)); 
		for (int i=1;i<=n;i++) if (!dfn[i]) tarjan(i);
		if (Scc!=n){puts("-1"); continue;}
		memset(vis,0,sizeof(vis));
		dij(1);
	//	for (int i=1;i<=n;i++) printf("%d ",f[i]); puts("");
		if (K==0) {printf("%d\n",f[n]); continue;}
		int ans=f[n];
		memset(dp,-1,sizeof(dp));
		for (int i=1;i<=K;i++) {
		//	printf("%d\n",ans);
			(ans+=DP(n,i))%=P;
		}
/*		for (int i=1;i<=n;i++)
		for (int j=1;j<=K;j++)
		printf("dp[%d][%d]=%d\n",i,j,dp[i][j]);*/
		printf("%d\n",ans);
	}
	return 0;
}

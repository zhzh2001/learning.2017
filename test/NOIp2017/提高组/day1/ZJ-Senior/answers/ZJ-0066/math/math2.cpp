#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
inline int read(){
	char ch=getchar(); int x=0,f=1;
	for (;ch>'9'||ch<'0';ch=getchar())if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
bool check(int i,int j,int k){
	if (k<0) return 0;
	if (k%i==0 || k%j==0) return 1;
	return check(i,j,k-i)|| check(i,j,k-j);
}
int main(){
	int n=20;
	for (int i=2;i<=n;i++)
		for (int last=0,tmp=0,j=i+1;j<=n;j++)
			if (__gcd(i,j)==1){
				for (int k=1;k<=10000;k++){
					if (check(i,j,k))tmp++; else{
						last=k; tmp=0;
					}
					if (tmp>100) break;
				}
				printf("%d %d:%d\n",i,j,last);
			}
}
			


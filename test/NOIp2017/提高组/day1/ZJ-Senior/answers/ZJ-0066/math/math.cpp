#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define ll long long
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	for (;ch>'9'||ch<'0';ch=getchar())if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a=read(),b=read();
	ll c=(a-1LL)*b-a;
	printf("%lld\n",c);
	return 0;
}


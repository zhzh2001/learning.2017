#include<cstdio>
#include<queue>
using namespace std;
typedef long long LL;
const int N=100010,M=200010,K=55;
struct E{int To,nxt,v;}e[M];
int T,n,m,k,p,Head[N],cnt,f[N][K],ans;
LL dis[N],mn;
queue<int>q;
bool inq[N],b[N];
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
void add(int &x,int y){x+=y; if (x>=p) x-=p;}
void addedge(int x,int y,int z){e[++cnt]=(E){y,Head[x],z}; Head[x]=cnt;}
void dfs(int u,int dis)
{
	if (dis>mn) return;
	if (u==n)
	{
		if (dis==mn) ans=(ans+1)%p;
		else if (dis<mn) mn=dis,ans=1;
	}
	for (int i=Head[u];i;i=e[i].nxt) dfs(e[i].To,dis+e[i].v);
}
int main()
{
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	T=read();
	while (T--)
	{
		n=read(); m=read(); k=read(); p=read();
		for (int i=1;i<=n;i++) Head[i]=0,dis[i]=10000000000000,b[i]=0;
		cnt=0;
		for (int i=1;i<=n;i++) for (int j=0;j<=k;j++) f[i][j]=0;
		for (int i=1;i<=m;i++)
		{
			int u=read(),v=read(),w=read();
			addedge(u,v,w);
		}
		if (n<=5)
		{
			ans=0; mn=1000000000;
			dfs(1,0);
			printf("%d\n",ans);
			continue;
		}
		dis[1]=0; q.push(1); f[1][0]=1; bool ljj=0;
		while (!q.empty())
		{
			int u=q.front(); q.pop(); inq[u]=0; b[u]=1;
			for (int i=Head[u];i;i=e[i].nxt)
			{
				int v=e[i].To; if (b[v]&&v!=n) for (int j=0;j<=k;j++) f[v][j]=0;
				if (dis[u]+e[i].v<=dis[v]+k)
				{
					if (dis[u]+e[i].v<=dis[v])
					{
						if (dis[u]+e[i].v<dis[v]-k)
							for (int j=0;j<=k;j++) f[v][j]=f[u][j];
						else
						{
							for (int j=k;j>=dis[v]-dis[u]-e[i].v;j--)
								f[v][j]=f[v][j-(dis[v]-dis[u]-e[i].v)];
							for (int j=0;j<dis[v]-dis[u]-e[i].v;j++) f[v][j]=0;
							for (int j=0;j<=k;j++) add(f[v][j],f[u][j]);
						}
						dis[v]=dis[u]+e[i].v;
					}
					else
						for (int j=0;j<=dis[u]+e[i].v-dis[v];j++) add(f[v][j+dis[u]+e[i].v-dis[v]],f[u][j]);
					if (!inq[v]) inq[v]=1,q.push(v);
				}
			}
		}
		ans=0;
		for (int i=0;i<=k;i++) add(ans,f[n][i]);
		printf("%d\n",ans);
	}
	return 0;
}

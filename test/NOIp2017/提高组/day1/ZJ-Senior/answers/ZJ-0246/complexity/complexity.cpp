#include<cstdio>
#include<cstring>
const int N=111;
int T,n,Top,ans,s[N],a[N],tot,c[N];
char t[N];
bool b[27];
int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') {if (c=='-') f=-1; c=getchar();} while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
int main()
{
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	T=read();
	while (T--)
	{
		n=read(); scanf("%s\n",&t); int len=strlen(t);
		if (t[2]=='1') ans=0;
		else
		{
			ans=0;
			for (int j=4;j<len-1;j++) ans=ans*10+t[j]-48;
		}
		memset(a,0,sizeof a);
		memset(b,0,sizeof b);
		memset(c,0,sizeof c);
		Top=0; tot=0;
		int mx=0,pjy=0; bool ljj=0;
		while (n--)
		{
			gets(t);
			if (ljj) continue;
			len=strlen(t);
			if (t[0]=='F')
			{
				if (b[t[2]-'a']) {ljj=1; continue;}
				b[t[2]-'a']=1;
				s[++Top]=t[2]-'a';
				int x=-1,y=-1; bool asd=0,fgh=0;
				int i;
				if (t[4]=='n') i=5,asd=1;
				else
				{
					x=0;
					for (i=4;t[i]!=' ';i++)
						x=x*10+t[i]-48;
				}
				i++;
				if (t[i]=='n') fgh=1;
				else
				{
					y=0;
					for (;i<len;i++)
						y=y*10+t[i]-48;
				}
				if (x!=-1&&fgh)
				{
					if (!tot) pjy++,c[Top]=1;
				}
				else if ((asd&&y!=-1)||(x!=-1&&y!=-1&&x>y)) a[Top]=1,tot++;
			}
			else
			{
				if (Top==0) {ljj=1; continue;}
				if (c[Top]) pjy--;
				if (a[Top]) tot--;
				a[Top]=0;
				b[s[Top--]]=0;
				if (Top==0) pjy=0;
			}
			if (pjy>mx) mx=pjy;
		}
		if (Top!=0) ljj=1;
		if (ljj) puts("ERR");
		else if (mx==ans) puts("Yes");
		else puts("No");
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

inline char get(void) {
	static char buf[100000], *S = buf, *T = buf;
	if (S == T) {
		T = (S = buf) + fread(buf, 1, 100000, stdin);
		if (S == T) return EOF;
	}
	return *S++;
}
template<typename T>
inline void read(T &x) {
	static char c; x = 0; int sgn = 0;
	for (c = get(); c < '0' || c > '9'; c = get()) if (c == '-') sgn = 1;
	for (; c >= '0' && c <= '9'; c = get()) x = x * 10 + c - '0';
	if (sgn) x = -x;
}
inline void read(char c, int &x) {
	x = 0;
	for (; c >= '0' && c <= '9'; c = get()) x = x * 10 + c - '0';
}
inline char BGet(void) {
	static char c;
	for (c = get(); c < 'A' || c > 'Z'; c = get());
	return c;
}
inline char SGet(void) {
	static char c;
	for (c = get(); c < 'a' || c > 'z'; c = get());
	return c;
}
inline int CGet(void) {
	BGet(); get();
	if (get() == '1') return 0;
	int x; read(x); return x;
}
inline char Get(void) {
	static char c;
	for (c = get(); c != 'n' && (c < '0' || c > '9'); c = get());
	return c;
}

int test, L, c, fl, x1, x2, ans, now, top, f2;
char ch;
int sta[2333], val[2333], key[2333];
int usd[2333];

int main(void) {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	read(test);
	while (test--) {
		read(L); c = CGet(); fl = 0; f2 = 1;
		for (int i = 'a'; i <= 'z'; i++) usd[i] = 0;
		for (int i = 0; i <= 233; i++) sta[i] = val[i] = key[i] = 0;
		ans = now = top = 0; key[0] = 1;
		while (L--) {
			ch = BGet();
			if (fl) {
				if (ch == 'F') {
					ch = SGet();
					ch = Get();
					if (ch >= '0' && ch <= '9') read(ch, x1);
					ch = Get();
					if (ch >= '0' && ch <= '9') read(ch, x2);
				}
				continue;
			}
			if (ch == 'F') {
				ch = SGet();
				sta[++top] = ch;
				if (usd[ch]) {
					fl = 1; continue;
				}
				++usd[ch];
				x1 = x2 = -1;
				ch = Get();
				if (ch >= '0' && ch <= '9') read(ch, x1);
				ch = Get();
				if (ch >= '0' && ch <= '9') read(ch, x2);
				if (x1 == -1 && x2 != -1) f2 = 0;
				if (x1 != -1 && x2 != -1 && x1 > x2) f2 = 0;
				if (f2 && x1 != -1 && x2 == -1) ++now;
				ans = max(now, ans);
				val[top] = now;
				key[top] = f2;
			} else {
				if (top <= 0) fl = 1;
				--usd[sta[top]];
				now = val[--top];
				f2 = key[top];
			}
		}
		if (top || fl) printf("ERR\n");
		else if (c == ans) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

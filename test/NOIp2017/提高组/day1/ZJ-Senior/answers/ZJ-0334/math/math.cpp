#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

long long x, y;

int main(void) {
	freopen("math.in", "r",stdin);
	freopen("math.out", "w", stdout);
	cin >> x >> y;
	cout << x * y - x - y << endl;
	fclose(stdin); fclose(stdout);
	return 0;
}

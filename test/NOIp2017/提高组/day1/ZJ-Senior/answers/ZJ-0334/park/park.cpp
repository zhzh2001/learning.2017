#include <queue>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 101010;
const int M = 202020;
typedef pair<int, int> Pairs;

inline char get(void) {
	static char buf[100000], *S = buf, *T = buf;
	if (S == T) {
		T = (S = buf) + fread(buf, 1, 100000, stdin);
		if (S == T) return EOF;
	}
	return *S++;
}
template<typename T>
inline void read(T &x) {
	static char c; int sgn = 0; x = 0;
	for (c = get(); c < '0' || c > '9'; c = get()) if (c == '-') sgn = 1;
	for (; c >= '0' && c <= '9'; c = get()) x = x * 10 + c - '0';
	if (sgn) x = -x;
}

struct edge {
	int to, next, key;
	edge(int t = 0, int n = 0, int k = 0):to(t), next(n), key(k) {}
};
edge G[M << 1], G1[M << 1];
int head1[N], head[N];
int dist[N], vis[N];
int f[N][60], ff[N][60];
int n, m, k, x, y, z, Gcnt, test, MOD, ans, fl;
priority_queue<Pairs> Q;


inline void Add(int &x, int a) {
	x += a; while (x >= MOD) x -= MOD;
}
inline void AddEdge(int from, int to, int key) {
	G[++Gcnt] = edge(to, head[from], key); head[from] = Gcnt;
	G1[Gcnt] = edge(from, head1[to], key); head1[to] = Gcnt;
}
inline void SP1(int s = 1) {
	for (int i = 1; i <= n; i++) {
		dist[i] = -1; vis[i] = 0;
	}
	dist[s] = 0;
	Q.push(Pairs(-dist[s], s));
	while (!Q.empty()) {
		x = Q.top().second; Q.pop();
		if (vis[x]) continue;
		vis[x] = true;
		for (int i = head[x]; i; i = G[i].next) {
			if (vis[G[i].to]) continue;
			if (dist[G[i].to] == -1 || dist[G[i].to] > dist[x] + G[i].key) {
				dist[G[i].to] = dist[x] + G[i].key;
				Q.push(Pairs(-dist[G[i].to], G[i].to));
			}
		}
	}
}
inline int dfs(int u, int d) {
	if (fl) return 0;
	if (f[u][d] != -1) return f[u][d];
	f[u][d] = 0; int nu, nd;
	for (int i = head1[u]; i; i = G1[i].next) {
		nu = G1[i].to; nd = dist[u] + d - G1[i].key - dist[nu];
		if (nd >= 0 && nd <= k) {
			Add(f[u][d], dfs(nu, nd));
			ff[u][d] |= ff[nu][nd];
		}
	}
	return f[u][d];
}
inline int dfs2(int u, int d, int p) {
	int to, res = 0; 
	for (int i = head[u]; i; i = G[i].next) {
		if (G[i].key) continue;
		to = G[i].to; if (!ff[to][d]) continue;
		if (to == p) return 1;
		res |= dfs2(to, d, p);
	}
	return res;
}

int main(void) {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	read(test);
	while (test--) {
		read(n); read(m); read(k); read(MOD); Gcnt = 0;
		for (int i = 1; i <= n; i++) head1[i] = head[i] = 0;
		for (int i = 1; i <= m; i++) {
			read(x); read(y); read(z);
			AddEdge(x, y, z);
		}
		ans = fl = 0; SP1();
		for (int i = 1; i <= n; i++)
			for (int j = 0; j <= k; j++) {
				f[i][j] = -1; ff[i][j] = 0;
			}
		f[1][0] = ff[1][0] = 1;
		for (int i = 0; i <= k; i++)
			Add(ans, dfs(n, i));
		for (int i = 1; i <= n; i++)
			for (int j = 0; j <= k; j++)
				if (ff[i][j] && dfs2(i, j, i)) ans = -1;
		printf("%d\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

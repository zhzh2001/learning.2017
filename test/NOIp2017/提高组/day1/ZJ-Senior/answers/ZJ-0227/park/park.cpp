#include <cstdio>
#include <queue>
#include <cstring>
#define N 100020
#define M 200020
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[M], nxt[M], val[M], cnt;
void insert(int x, int y, int z) {
	to[++cnt] = y;
	nxt[cnt] = head[x];
	head[x] = cnt;
	val[cnt] = z;
}
struct node {
	int to, len;
	friend bool operator < (const node &a, const node &b) {
		return a.len > b.len;
	}
};
priority_queue<node> que;
int dis[N];
// calc the min path
int dijkstra(int st, int ed) {
	memset(dis, 63, sizeof dis);
	dis[st] = 0;
	que.push((node){st, 0});
	while (!que.empty()) {
		node c = que.top(); que.pop();
		int x = c.to;
		if (dis[x] != c.len) continue;
		for (int i = head[c.to]; i; i = nxt[i]) {
			if (dis[x] + val[i] < dis[to[i]]) {
				dis[to[i]] = dis[x] + val[i];
				que.push((node){to[i], dis[to[i]]});
			}
		}
	}
	return dis[ed];
}
int vis[N];
bool dfs(int x) {
//	printf("Dfs: %d\n", x);
	for (int i = head[x]; i; i = nxt[i]) {
		int y = to[i];
		if (vis[y] == vis[x] + val[i]) return 1;
		if (vis[y] == -1) {
			vis[y] = vis[x] + val[i];
			if (dfs(y)) return true;
			vis[y] = -1;
		}
	}
//	printf("%d is not ok\n", x);
	return false;
}
bool hasZero(int st) {
	memset(vis, -1, sizeof vis);
	vis[st] = 0;
	return dfs(st);
}
int MX_LEN, res, P, ED_POS;
void dfs(int x, int len) {
	if (len > MX_LEN) return;
	if (x == ED_POS && len == MX_LEN) {
		if (++ res == P)
			res = 0;
		return;
	}
	for (int i = head[x]; i; i = nxt[i])
		dfs(to[i], len + val[i]);
}
int work(int st, int ed, int len, int p) {
	MX_LEN = len;
	res    = 0;
	P      = p;
	ED_POS = ed;
	dfs(st, 0);
	return res;
}
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T = read();
	while (T --) {
		int n = read(), m = read(), k = read(), p = read();
		cnt = 0;
		memset(head, 0, sizeof head);
		for (int i = 1; i <= m; i++) {
			int x = read(), y = read();
			insert(x, y, read());
		}
		if (hasZero(1)) { // -1
			puts("-1");
			continue;
		}
		int mn = dijkstra(1, n);
		ll ans = 0;
		for (int i = 0; i <= k; i++)
			ans = (ans + work(1, n, mn + i, p)) % p;
		printf("%lld\n", ans);
	}
} 

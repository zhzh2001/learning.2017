#include <cstdio>
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	ll a = read(), b = read();
	printf("%lld\n", a * b - a - b);
	return 0;
}

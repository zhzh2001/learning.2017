#include <cstdio>
#include <cstring>
#include <map>
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int hsh(char *c) {
	int len = strlen(c);
	int h = 0;
	for (int i = 0; i < len; i++)
		h = c[i] * 233 + 666;
	return h;
}
char fzd[500], op[50], tmp[500], x[500], y[500];
map<int, int> mp, mn, mx;
int var_names[500];
int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T = read();
	while (T --) {
		mp.clear();
		mn.clear();
		mx.clear();
		tmp[0] = 'n'; tmp[1] = '\0';
		int sss = hsh(tmp);
		mp[sss] = 1;
		mn[sss] = 100000;
		mx[sss] = 100000;
		int L = read(), w = 0;
		scanf("%s", fzd);
		if (fzd[2] == 'n') { // calc w
			int len = strlen(fzd) - 1;
			for (int i = 4; i < len; i++)
				w = w * 10 + fzd[i] - '0';
		}
		int res = 0, now = 0, err = 0, st = 0, die = 0;
		while (L --) { // read lines
			scanf("%s", op);
			if (op[0] == 'F') {
				scanf("%s", tmp);
				int var_name = hsh(tmp);
				if (mp[var_name]) // same name
					err = 1;
				mp[var_name] = 1;
				if (err) {
					scanf("%s%s", fzd, fzd);
					continue;
				}
				
				scanf("%s%s", x, y);
				int mxx, mnx, mxy, mny;
				if (x[0] <= '9' && x[0] >= '0') {
					mxx = 0; int len = strlen(x);
					for (int i = 0; i < len; i++)
						mxx = mxx * 10 + x[i] - '0';
					mnx = mxx;
				} else {
					int x_hsh = hsh(x);
					mnx = mn[x_hsh];
					mxx = mx[x_hsh];
				}
				if (y[0] <= '9' && y[0] >= '0') {
					mxy = 0; int len = strlen(y);
					for (int i = 0; i < len; i++)
						mxy = mxy * 10 + y[i] - '0';
					mny = mxy;
				} else {
					int y_hsh = hsh(y);
					mny = mn[y_hsh];
					mxy = mx[y_hsh];
				}
				
				var_names[++ st] = var_name;
				if (mnx > mxy) {
					die = st;
				}
				mn[var_name] = mnx;
				mx[var_name] = mxy;
				if (mxy - mnx > 50000) {
					mp[var_name] = 2;
					if (!die) res = max(res, ++ now);
				}
			} else {
				if (st == 0) err = 1;
				if (die == st) die = 0;
				if (mp[var_names[st]] == 2) now --;
				if (!err) mp[var_names[st --]] = 0;
			}
		}
		if (err || st) puts("ERR");
		else puts(res == w ? "Yes" : "No");
	}
} 

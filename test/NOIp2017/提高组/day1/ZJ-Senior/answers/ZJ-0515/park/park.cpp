#include <bits/stdc++.h>
#define N (400010)
using namespace std;
template <typename T> inline void  read(T &x) {
	static char c; c = x = 0; while (c < '0' || c > '9') c = getchar();
	while (c >= '0' && c <= '9') x = (x + (x << 2) << 1) + c ^ '0', c = getchar();
}
int T, n, m, k, p;
int first[N], nxt[N], cnt, a[N], w[N];
inline void add(int x, int y, int z) {
	a[++cnt] = y; w[cnt] = z; nxt[cnt] = first[x]; first[x] = cnt;
}
int f[N], g[N][51], d[N];
bool b[N];
int qu[N / 2 * 3], h, t;
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	read(T);
	while (T--) {
		memset(first, 0, sizeof(first));
		memset(nxt, 0, sizeof(nxt));
		read(n); read(m); read(k); read(p);
		for (int i = 1, x, y, z; i <= m; ++i) {
			read(x); read(y); read(z); add(x, y, z);
		}
		memset(f, 120, sizeof(f)); f[1] = 0; qu[t = 1] = 1; b[1] = 1; d[1] = 1;
		while (h < t) {
			int x = qu[++h]; b[x] = 0;
			for (int i = first[x]; i; i = nxt[i]) if (f[x] + w[i] < f[a[i]]) {
				d[a[i]] = d[x];
				f[a[i]] = f[x] + w[i]; if (!b[a[i]]) b[a[i]] = 1, qu[++t] = a[i];
			} else if (f[x] + w[i] == f[a[i]]) (d[a[i]] += d[x]) %= p;
		}
		if (k == 0) {
			printf("%d", d[n]); continue;
		}
		g[1][0] = 1;
		for (int i = 1; i <= t; ++i) {
			int x = qu[i];
			for (int j = first[x]; j; j = nxt[j]) 
				for (int p1 = 0; p1 <= k; ++p1) 
				if (f[x] + w[j] + p1 - f[a[j]] <= k && f[x] + w[j] + p1 - f[a[j]] >= 0)
					(g[a[j]][f[x] + w[j] + p1 - f[a[j]]] += g[x][p1]) %= p;
		}
		int ans = 0;
		for (int i = 0; i <= k; ++i) (ans += g[n][i]) %= p;
		printf("%d\n", ans);
	}
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll a, b;
inline ll solve(ll a, ll b) {
	if (a == 1 || b == 1) return 0;
	ll ans = (a - 2) + (a - 1) * (b - 2);
	return ans;
}
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld%lld", &a, &b);
	printf("%lld\n", solve(a, b));
	return 0;
}

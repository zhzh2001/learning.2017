#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

long long read()
{
	long long x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void write(long long x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writen(long long x){write(x),putchar(' ');}
void writeln(long long x){write(x),puts("");}

long long a,b;
long long ans;

int main()
{
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	a=read(),b=read();
	ans=(a-1)*(b-1)-1;
	writeln(ans);
	return 0;
}


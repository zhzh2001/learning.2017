#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writen(ll x){write(x),putchar(' ');}
void writeln(ll x){write(x),puts("");}

int l,dd,maxn=0;
string s;
bool ha[300];
bool flag=0,ff=0;
int sta[500];
int topp=0;
int bre;

void solve(int dep)
{
	while(dd<=l)
	{
		dd++;
		string s1,s2;
		char c=getchar(),c1;
		while(c!='F'&&c!='E') c=getchar();
		if(c=='F')
		{
			topp++;
			c1=getchar();
			while(c1<'a'||c>'z') c1=getchar();
			if(ha[c1-'a']){flag=1;}
			else ha[c1-'a']=1;
			
			cin >> s1;
			cin >> s2;
			
			if(bre!=0) solve(dep);
			else if(s1=="n"&&s2=="n") solve(dep);
			else if(s1=="n") {bre=topp;if(dep-1>maxn) maxn=dep-1;solve(dep);}
			else if(s2=="n") solve (dep+1);
			else if(s1>s2) {bre=topp;if(dep-1>maxn) maxn=dep-1;solve(dep);}
			else solve(dep);
			ha[c1-'a']=0;
			
		}
		else if(c=='E')
		{
			if(bre==0||bre==topp){if(dep>maxn) maxn=dep; bre=0;}
			topp--;
			return;
		}
	}
}

int main()
{
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	int T=read();
	
	while(T--)
	{
		memset(ha,0,sizeof ha);
		l=read();
//		cout << l << " ";
//		if(l%2)
//		{
//			puts("ERR");
//			continue;
//		}
		cin >> s;
//		cout << s << " ";
		int xx=0;
		dd=1,flag=0,maxn=0,ff=0,topp=0,bre=0;
		solve(0);
		for(int i=0; i<s.size(); i++)
		{
			if(s[i]=='^') ff=1;
			if(s[i]>='0'&&s[i]<='9')
				xx=xx*10+s[i]-'0';
		}
		if(flag||topp)
		{
			puts("ERR");
			continue;
		}
		else if(maxn==0)
		{
//			writen(maxn);
//			writen(ff),writen(xx);
//			cout << s << " ";
			if(!ff&&xx==1)
				puts("Yes");
			else puts("No");
		}
		else
		{
//			writen(maxn);
			if(ff&&xx==maxn)
				puts("Yes");
			else puts("No");
		}
	}
	return 0;
}


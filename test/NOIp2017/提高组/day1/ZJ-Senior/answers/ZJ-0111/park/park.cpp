#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writen(ll x){write(x),putchar(' ');}
void writeln(ll x){write(x),puts("");}

int n,m,k,p;
const int inf=0x3f;
int ans=0;
int ma[1005][1005];

void solve1()
{
	memset(ma,inf,sizeof ma);
	for(int i=1,a,b,c; i<=m; i++)
	{
		a=read(),b=read(),c=read();
		ma[a][b]=c;
	}
	for(int k=1; k<=n; k++)
		for(int i=1; i<=n; i++)
			for(int j=1; j<=n; j++)
				if(i!=j&&i!=k&&j!=k)
				{
					if(ma[i][k]+ma[k][j]<ma[i][j])
						ma[i][j]=ma[i][k]+ma[k][j];
					if(i==1&&j==n)
					{
						if(ma[i][k]+ma[k][j]==ma[i][j])
							ans++;
					}
				}
	writeln(ans%p);
}

int main()
{
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read(),m=read(),k=read(),p=read();
		if(n<=1000&&m<=2000&&k==0)
		{
			ans=0;
			solve1();
			continue;
		}
		else
		puts("I'm too weak to solve this problem...orz");
	}
	return 0;
}


#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
using namespace std;
int cas,len,z,x,zz,xx,top,a,b;
int ins[105];
bool Flag;
bool out[105];
bool flag['z'+1];
char st[105][20];
char s[10];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&cas);
	while (cas--){
		z=x=xx=zz=0; Flag=false;
		memset(out,0,sizeof(out));
		memset(flag,0,sizeof(flag));
		memset(st,0,sizeof(st));
		scanf("%d",&len);
		scanf("%s",s+1);
		int l=strlen(s+1);
		bool p=false;
		for (int i=1;i<=l-1;i++){
			if (s[i]=='('){
				if (s[i+1]=='1'){
					z=1;
					break;
				}
			}
			if (p) x=x*10+s[i]-'0';
			if (s[i]=='^') p=true;
		}
		for (int i=1;i<=len;i++){
			scanf("%s",st[i]+1);
			if (st[i][1]=='F'){
				st[i][2]=' ';
				scanf("%s",s+1);
				int ll=strlen(s+1);
				for (int j=3;j<=3+ll-1;j++) st[i][j]=s[j-2];
				st[i][4]=' ';
				scanf("%s",s+1);
				ll=strlen(s+1);
				for (int j=5;j<=5+ll-1;j++) st[i][j]=s[j-4];
				st[i][5+ll]=' ';
				scanf("%s",s+1);
				int last=5+ll+1;
				ll=strlen(s+1);
				for (int j=last;j<=last+ll-1;j++) st[i][j]=s[j-last+1];
			}
		}
		top=0; 
		int Max=0;
		for (int i=1;i<=len;i++){
			int l=strlen(st[i]+1);
			bool pp=false;
			if (l==1){
				if (!top){
					Flag=true;
					break;
				}
				flag[ins[top--]]=false;
				out[i]=out[i-1];
				xx=0;
			} else{
				a=b=0;
				out[i]=out[i-1];
				if (flag[st[i][3]]){
					Flag=true;
					break;
				} else{
					flag[st[i][3]]=true;
					ins[++top]=st[i][3];
				}
				for (int j=5;j<=l;j++)
					if (st[i][j]=='n'){
						b=1e9;
					} else
					if (st[i][j]==' '){
						if (!pp) a=b;
						pp=true;
						b=0;
					} else b=b*10+st[i][j]-'0';
				if (a==1e9&&a!=b){
					out[i]=true;
				} else
				if (b==1e9&&a!=b){
					if (!out[i]) xx++,Max=max(Max,xx);
				} else
				if (a>b){
					out[i]=true;
				}
			}
		}
		if (top) Flag=true;
		if (Flag) puts("ERR"); else{
			if (z){
				if (!Max) puts("Yes"); else puts("No");
			} else
			if (x){
				if (x==Max) puts("Yes"); else puts("No");
			}
		}
	}
	return 0;
}

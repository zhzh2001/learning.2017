#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
using namespace std;
long long a,b,k,s,x,y;
long long exgcd(long long a,long long b){
	if (!b){
		x=1; y=0;
		return a;
	}
	long long d=exgcd(b,a%b);
	long long t=x; x=y; y=t-a/b*y;
	return d;
}
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a>b) swap(a,b);
	k=b%a; s=((a-1)*k)%a;
	exgcd(s,a);
	x=(x%a+a)%a;
	k=k*x;
	exgcd(k,a);
	x=(x%a+a)%a;
	long long ans=x*b-a;
	printf("%lld\n",ans);
	return 0;
}

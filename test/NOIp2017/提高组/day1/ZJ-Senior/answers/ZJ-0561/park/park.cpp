#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define M 200005
#define N 100005
using namespace std;
int n,m,k,p,cas,tot,a,b,c,ans,d;
int head[N],vis[N],dis[N];
int q[100*N];
bool flag[N];
struct node{
	int next,to,dis;
}e[M];
void add(int x,int y,int z){
	e[++tot].next=head[x];
	head[x]=tot;
	e[tot].to=y;
	e[tot].dis=z;
}
bool spfa(){
	for (int i=2;i<=n;i++) dis[i]=1e9,flag[i]=false,vis[i]=0;
	int t=1,w=1;
	q[1]=1; dis[1]=0; flag[1]=true; vis[1]=1;
	while (t<=w){
		int k=q[t];
		for (int i=head[k];i;i=e[i].next){
			int v=e[i].to;
			if (dis[v]>=dis[k]+e[i].dis){
				dis[v]=dis[k]+e[i].dis;
				if (!flag[v]){
					flag[v]=true;
					vis[v]++;
					if (vis[v]>n) return false;
					q[++w]=v;
				}
			}
		}
		flag[k]=false;
		t++;
	}
	return true;
}
void dfs(int u,int s){
	if (s>d) return;
	if (u==n){
		ans=(ans+1)%p;
		return;
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		dfs(v,s+e[i].dis);
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&cas);
	while (cas--){
		tot=ans=0;
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=m;i++)
			scanf("%d%d%d",&a,&b,&c),add(a,b,c);
		if (!spfa()) puts("-1");
		else{
			d=dis[n]+k;
			dfs(1,0);
			printf("%d\n",ans);
		}
	}
	return 0;
}

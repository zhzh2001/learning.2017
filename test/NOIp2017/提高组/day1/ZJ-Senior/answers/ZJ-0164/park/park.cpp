#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmax(T &x,const T &y)
{
	if(x<y)x=y;
}
typedef long long ll;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
///bool b1;
const int N=1e5+5,M=2e5+5,K=50+5,inf=1e9;
int dp[N][K],g[N],k,D;bool in[N][K];
int t[N],nt[N];
struct edge
{
	int to,next,w;
}l[M*2];
struct point
{
	int x,d;
};
bool operator <(const point &p1,const point &p2)
{
	return p1.d<p2.d;
}
bool operator >(const point &p1,const point &p2)
{
	return p1.d>p2.d;
}
priority_queue<point,vector<point>,greater<point> > q;
//bool b2;

int Dp(int x,int d)
{
	int j=d-g[x];
	if(j<0||j>k) return 0;
	if(in[x][j]) return -1;
	if(dp[x][j]!=-1) return dp[x][j];
	in[x][j]=1;
	ll ans=0;
	for(int i=nt[x];i;i=l[i].next)
	{
		int y=l[i].to;
		int now=Dp(y,d-l[i].w);
		if(now==-1)return -1;
		ans+=now;
	}
	in[x][j]=0;
	return dp[x][j]=ans%D;
}

int main()
{
	freopen("park.in","r",stdin);freopen("park.out","w",stdout);
	//printf("%lf\n",(double)(&b2-&b1)/1024/1024);
	int tt;ll ans;
	cin>>tt;
	while(tt--)
	{
		int n,m;
		cin>>n>>m>>k>>D;
		memset(t,0,sizeof(t));memset(nt,0,sizeof(nt));
		rep(i,1,m)
		{
			int a,b,w;
			scanf("%d%d%d",&a,&b,&w);
			l[i].to=b;l[i].w=w;
			l[i].next=t[a];t[a]=i;
			int j=m+i;
			l[j].to=a;l[j].w=w;
			l[j].next=nt[b];nt[b]=j;
		}
		
		rep(i,1,n)g[i]=inf;
		g[1]=0;
		q.push((point){1,0});
		while(!q.empty())
		{
			point p=q.top();q.pop();
			int x=p.x,d=p.d;
			if(d!=g[x])continue;
			for(int i=t[x];i;i=l[i].next)
			{
				int y=l[i].to,nd=g[x]+l[i].w;
				if(g[y]>nd)
				{
					g[y]=nd;
					q.push((point){y,nd});
				}
			}
		}
		
		if(g[n]==inf)
		{
			puts("0");
			continue;
		}
		
		rep(x,1,n)
		if(g[x]==0)
		for(int i=t[x];i;i=l[i].next)
		if(l[i].w==0&&l[i].to==1) 
		{
			puts("-1");
			goto End;
		}
		
		memset(dp,-1,sizeof(dp));
		memset(in,0,sizeof(in));
		dp[1][0]=1;
		ans=0;
		rep(i,0,k) 
		{
			int now=Dp(n,g[n]+i);
			if(now==-1) 
			{
				puts("-1");goto End;
			}
			ans+=now;
		}
		cout<<ans%D<<endl;
		End: ;
	}
}

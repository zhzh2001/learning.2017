#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
void exgcd(ll &a,ll x,ll &b,ll y)
{
	if(!y)
	{
		a=1;b=0;
		return ;
	}
	exgcd(b,y,a,x%y);
	b-=a*(x/y);
}

int main()
{
	freopen("math.in","r",stdin);freopen("math.out","w",stdout);
	ll x,y;
	cin>>x>>y;
	ll a,b;
	exgcd(a,x,b,y);
	if(a<0)
	{
		swap(a,b);
		swap(x,y);
	} 
	ll k,k2;
	exgcd(k,abs(b),k2,x);
//	while(k+1<x){k+=x;k2-=abs(b);}
	ll l=y*(x-1);
	if(k<l)
	{
		k+=x* (( l-k + x-1 )/x);
	}
	else k-=x* (( k-l )/x);
	cout<<k-x;
}

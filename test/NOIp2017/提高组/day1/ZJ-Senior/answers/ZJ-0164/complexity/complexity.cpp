#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmax(T &x,const T &y)
{
	if(x<y)x=y;
}
typedef long long ll;
#define rep(i,l,r) for(int i=l;i<=r;++i)
#define per(i,r,l) for(int i=r;i>=l;--i)
const char dy[3][10]=
{
"Yes","No","ERR"
};
const int N=100+5;
char s[N][100];
int st[N],top;
int t[N],nex[N];
int ans;

char as[100];
int read(char *s)
{
	int ans=0;
	for(int i=0;isdigit(s[i]);++i)ans=ans*10+s[i]-'0';
	return ans;
}
int qiu()
{
	char *h=as;
	while(*h<'0')++h; 
	if(h[2]=='1')return 0;
	return read(h+4);
}
bool ap[500];
bool dfs(int x,int cnt)
{
	char *h=s[x]+1;
	while(*h<'0')++h;
	char now=*h;
	++h;
	if(ap[now])return 1;
	ap[now]=1;
	
	int a1=0,a2=0;
	while(*h<'0')++h;
	if(*h=='n') {a1=101;++h;}
	else 
	{
		a1=read(h);
		while(isdigit(*h))++h;
	}
	while(*h<'0')++h;
	if(*h=='n') a2=101;
	else a2=read(h);
	
	if(a1>a2) cnt=-1000; else
	if(a1==a2) {} else
	if(a2==101) ++cnt;
	chmax(ans,cnt);
	for(int i=t[x];i;i=nex[i])if(dfs(i,cnt))return 1;
	ap[now]=0;
	return 0;
}
int solve()
{
	int L;cin>>L;
	gets(as);
	rep(i,1,L)gets(s[i]);
	top=0;
	memset(t,0,sizeof(t));
	rep(i,1,L)
	{
		if(*s[i]=='E')
		{
			if(!top) return 2;
			--top;
		}
		else
		{
			nex[i]=t[st[top]];t[st[top]]=i;
			st[++top]=i;
		}
	}
	if(top) return 2;
	ans=0;
	memset(ap,0,sizeof(ap));
	for(int i=t[0];i;i=nex[i])
	if(dfs(i,0)) return 2;
	if(ans!=qiu()) return  1;
	return 0;
}
int main()
{
	freopen("complexity.in","r",stdin);freopen("complexity.out","w",stdout);
	int tt;
	cin>>tt;
	while(tt--)
	{
		printf("%s\n",dy[solve()]);
	}
}

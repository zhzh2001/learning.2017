var
x,y,t,ans:int64;
begin
assign(input,'math.in'); reset(input);
assign(output,'math.out'); rewrite(output);
readln(x,y);
if (x mod 2=0) and (y mod 2=1) then
   begin
   t:=x; x:=y; y:=t;
   end;
ans:=(x-2)+(y-2)*(x div 2)*2;
writeln(ans);
close(input); close(output);
end.
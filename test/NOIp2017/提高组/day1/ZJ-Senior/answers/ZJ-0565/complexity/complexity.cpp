#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.ans","w",stdout);
	int t;cin>>t;
	while(t--)
	{
		int n,e=0,cnt=0,l=0;
		bool f=0,c[101],h[101],fff=0;
		memset(c,0,sizeof(c));
		memset(h,0,sizeof(h));
		string a,re="ERR";
		cin>>n>>a;
		for(int i=0;i<a.size();i++)
		{	
			if(!f)
			{
				if(a[i]==1)break;
				else if(a[i]=='n')f=1;
			}
			else if(a[i]>'0'&&a[i]<='9')
			e=e*10+a[i]-'0';
		}
		for(int i=1;i<=n;i++)
		{
			char v;cin>>v;
			if(v=='F')
			{
				cnt++;
				char x;
				string y,z;
				cin>>x>>y>>z;
				if(c[x]==1)cnt+=10000;
				else c[x]=1;
				if(y=="n")fff=cnt;
				if(!fff&&h[cnt]==0)
				{
					bool ff=1;
					for(int j=0;j<y.size();j++)
					if(y[j]<'0'||y[j]>'9')ff=0;
					if(ff==1&&z=="n")
					{
						l++;
						h[cnt]=1;
					}
				}
			}
			else if(fff==cnt--)fff=0;
		}
		if(cnt==0)
		{
			if(l==e)re="Yes";
			else re="No";
		}
		cout<<re<<endl;
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#define M 100001
#define N 200001
using namespace std;
int n,m,k,p,ll,rr,cnt;
int head[M],dis1[M],dis2[M],has[M],sum;
int next[N],w[N],to[N],queue[N];
void dfs(int s,int k)
{
	for(int i=head[s];i;i=next[i])
	if(dis2[s]+w[i]<=dis1[1]+k)
	if(to[i]==1)sum=(sum+1)%p;
	else
	{
		dis2[to[i]]=dis2[s]+w[i];
		dfs(to[i],k);
		dis2[to[i]]=0;
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.ans","r",stdout);
	int t;
	cin>>t;
	while(t--)
	{	
		ll=1;rr=1;cnt=0;sum=0;
		cin>>n>>m>>k>>p;
		for(int i=1;i<=n;i++)
		{
			head[i]=0;
			has[i]=0;
			dis1[i]=0x3f;
			dis2[i]=0;
		}
		for(int i=1;i<=m;i++)
		{
			int a,b,c;
			cin>>a>>b>>c;
			cnt++;
			to[cnt]=a;
			next[cnt]=head[b];
			w[cnt]=c;
			head[b]=cnt;
		}
		queue[rr]=n;
		has[rr]=n;
		dis1[n]=0;
		rr++;
		while(ll<rr)
		{
			int now=queue[ll];
			for(int i=head[now];i;i=next[i])
			{
				if(!has[to[i]])queue[++rr]=to[i],has[to[i]]=1;
				if(dis1[to[i]]>dis1[now]+w[i])
				dis1[to[i]]=dis1[now]+w[i];
			}
			ll++;has[now]=0;
		}
		dfs(n,k);
		cout<<sum<<endl;
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <string>
#include <stack>
#include <vector>
using namespace std;
const int MAXN = 120;

namespace solver1 {
  int n, w, ans;
  int argu[MAXN][4]; // 0: type(0 = F / 1 = E), 1: var, 2: start, 3:end; n = 101
  vector <int> tm;
  bool used[30];
  int to_num(char *s) {
    if (s[0] == 'n') return 101;
    int ans = 0;
    for (int i = 0; s[i]; i++) {
      ans = ans * 10 + s[i] - '0';
    }
    return ans;
  }
  void read() {
    scanf("%d", &n);
    char buff[20];
    scanf("%s", buff);
    if (buff[2] == '1') w = 0;
    else {
      int len = strlen(buff); w = 0;
      for (int i = 4; i < len - 1; i++) {
	w = w * 10 + buff[i] - '0';
      }
    }
    for (int i = 1; i <= n; i++) {
      scanf("%s", buff);
      if (buff[0] == 'F') {
	argu[i][0] = 0;
	scanf("%s", buff);
	argu[i][1] = buff[0] - 'a';
	scanf("%s", buff);
	argu[i][2] = to_num(buff);
	scanf("%s", buff);
	argu[i][3] = to_num(buff);
      } else {
	argu[i][0] = 1;
      }
    }
  }
  bool compile() {
    memset(used, 0, sizeof used);
    stack <int> st;
    for (int i = 1; i <= n; i++) {
      if (argu[i][0]) {
	if (st.empty()) return 0;
	used[st.top()] = 0;
	st.pop();
      } else {
	if (used[argu[i][1]]) return 0;
	used[argu[i][1]] = 1;
	st.push(argu[i][1]);
      }
    } 
    return st.empty();
  }
  
  void cal() {
    int tmp = 0;
    for (int i = tm.size() - 1; i >= 0; i--) {
      if (tm[i] == -1) {
	break;
      } else {
	tmp += tm[i];
      }
    }
    ans = max(tmp, ans);
  }
  void run() {
    stack <int> st;
    //vector <int> tm;
    ans = 0;
    tm.clear();
    for (int i = 1; i <= n; i++) {
      if (argu[i][0]) {
	int t = st.top(); st.pop();
	tm.push_back(t);
	if (st.empty()) { cal(), tm.clear(); }
      } else {
	if (argu[i][2] > argu[i][3]) {
	  st.push(-1);
	} else {
	  if (argu[i][2] == 101 && argu[i][3] == 101) {
	    st.push(0);
	  } else if (argu[i][3] == 101) {
	    st.push(1);
	  } else {
	    st.push(0);
	  }
	}
      }
    }
    puts(ans == w ? "Yes" : "No");
  }
  void main() {
    read();
    if (!compile()) {puts("ERR"); return;}
    run();
  }
}
int main() {
#ifndef LOCAL 
  freopen("complexity.in", "r", stdin);
  freopen("complexity.out", "w", stdout);
#endif

  int t;
  scanf("%d", &t);
  while (t--) {
    solver1::main();
  }
  return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>

int a, b;
namespace solver1 {
  void main() {
    printf("%lld\n", 1ll * a * b - a - b);
  }

}
int main() {
#ifndef LOCAL
  freopen("math.in", "r", stdin);
  freopen("math.out", "w", stdout);
#endif
  scanf("%d%d", &a, &b);
  solver1::main();

  return 0;
}

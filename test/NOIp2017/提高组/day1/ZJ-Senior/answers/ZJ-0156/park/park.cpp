#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
#include <utility>
#define MS(a, b) memset(a, b, sizeof a);
typedef std::pair<int, int> Pii;
using std::make_pair; 
const int MAXN = 100000 + 10, MAXM = 200000 + 10;


struct Edge {
  int v, w, next;
}edge[MAXM];
int n, m, k, p;
int head[MAXN], tail;
inline void insert(int u, int v, int w) {
  edge[++tail] = (Edge) {v, w, head[u]}; head[u] = tail;
}
inline void add(int &x, int y) {
  x += y;
  if (x >= p) x -= p;
}
namespace solver1 {
  int dis[MAXN];
  int seq[MAXN], cnt;
  bool visit[MAXN];
  int f[MAXN][60];//, f2[MAXN];
  void dijkstra() {
    std::priority_queue<Pii, std::vector<Pii>, std::greater<Pii> > pq;
    MS(dis, 63);
    dis[1] = 0;
    pq.push(make_pair(0, 1));
    cnt = 0;
    while(!pq.empty()) {
      Pii tmp = pq.top(); pq.pop();
      int u = tmp.second, d = tmp.first;
      if (d != dis[u]) continue;
      seq[++cnt] = u;
      for (register int i = head[u]; i; i = edge[i].next) {
	int v = edge[i].v, w = edge[i].w;
	if (d + w < dis[v]) {
	  dis[v] = d + w;
	  pq.push(make_pair(dis[v], v));
	}
      }
    }
  }
  int work() {
    MS(visit, 0);
    MS(f, 0);
    f[1][0] = 1;
    for (register int x = 1; x <= n; x++) {
      int u = seq[x];
      visit[u] = 1;
      for (register int i = head[u]; i; i = edge[i].next) {
	int v = edge[i].v, w = edge[i].w;
	if (dis[u] + w == dis[v] && visit[v]) return -1;
      }
    }
    /*if (k == 0) {
      MS(f2, 0);
      f2[1] = 1;
      for (register int x = 1, u; x <= n; x++) {
	u = seq[x]; 
	for (register int i = head[u], v, w; i; i = edge[i].next) {
	  v = edge[i].v, w = edge[i].w;
	  if (dis[u] + w == dis[v]) {
	    add(f2[u], f2[v]);
	  }
	}
      }
      return f2[n];
      }*/
    //std::queue <Pii> q;
    for (register int j = 0; j <= k; j++) {
      for (register int x = 1, u; x <= n; x++) {
	u = seq[x];
	if (!f[u][j]) continue;
	for (register int i = head[u], v, w, l; i; i = edge[i].next) {
	  v = edge[i].v, w = edge[i].w;
	  l = dis[u] + j + w - dis[v];
	  if (l <= k) add(f[v][l], f[u][j]);
	}
      }
    }
    int ans = 0;
    for (int i = 0; i <= k; i++) {
      add(ans, f[n][i]);
    }
    return ans;
  }
  void main() {
    dijkstra();
    printf("%d\n", work());
  }
}
int main() {
#ifndef LOCAL
  freopen("park.in", "r", stdin);
  freopen("park.out", "w", stdout);
#endif
  int t;
  scanf("%d", &t);
  while(t--) {
    memset(head, 0, sizeof head);
    tail = 0;
    scanf("%d%d%d%d", &n, &m, &k, &p);
    for (int i = 1; i <= m; i++) {
      int u, v, w;
      scanf("%d%d%d", &u, &v, &w);
      insert(u, v, w);
    }
    solver1::main();
  }
  return 0;
}

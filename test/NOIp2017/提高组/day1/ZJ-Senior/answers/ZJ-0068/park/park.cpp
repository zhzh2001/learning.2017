#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;
const int INF=1000000000;
int T,n,m,k,p,he[100100],to[200200],nxt[200200],val[200200];
int cnt,u,v,w,vis[100100],dist[100100];
int hhe[100100],tto[200200],nnxt[200200],ddist[100100];
int f[100100][60],ans;
queue<int> q;
void dfs(int x,int dis){
	if (dis+ddist[x]>dist[n]+k) return;
	for (int i=he[x];i;i=nxt[i])
	{
		int vv=to[i];
		if (dis+val[i]-dist[vv]>k) continue;
		f[vv][dis+val[i]-dist[vv]]=(f[vv][dis+val[i]-dist[vv]]+f[x][dis-dist[x]])%p;
		if (vv!=n) dfs(vv,dis+val[i]);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		memset(f,0,sizeof(f));
		memset(he,0,sizeof(he));
		memset(nxt,0,sizeof(nxt));
		cnt=0;
		scanf("%d %d %d %d",&n,&m,&k,&p);
		for (int i=1;i<=m;i++)
		{
			scanf("%d %d %d",&u,&v,&w);
			to[++cnt]=v,val[cnt]=w,nxt[cnt]=he[u],he[u]=cnt;
			tto[cnt]=u,nnxt[cnt]=hhe[v],hhe[v]=cnt;
		}
		for (int i=1;i<=n;i++) dist[i]=INF;
		memset(vis,0,sizeof(vis));
		dist[1]=0; vis[1]=1;
		q.push(1); 
		while (!q.empty())
		{
			int k=q.front(); q.pop();
			for (int i=he[k];i;i=nxt[i])
			{
				if (dist[to[i]]>dist[k]+val[i])
				{
					dist[to[i]]=dist[k]+val[i];
					if (!vis[to[i]]){
						vis[to[i]]=1;
						q.push(to[i]);
					}
				}
			}
			vis[k]=0;
		}
		if (k!=0)
		{
			for (int i=1;i<=n;i++) ddist[i]=INF;
			memset(vis,0,sizeof(vis));
			ddist[n]=0; vis[n]=1;
			q.push(n); 
			while (!q.empty())
			{
				int k=q.front(); q.pop();
				for (int i=hhe[k];i;i=nnxt[i])
				{
					if (ddist[tto[i]]>ddist[k]+val[i])
					{
						ddist[tto[i]]=ddist[k]+val[i];
						if (!vis[tto[i]]){
							vis[tto[i]]=1;
							q.push(tto[i]);
						}
					}
				}
				vis[k]=0;
			}
		}
		f[1][0]=1;
		dfs(1,0);
		ans=0;
		for (int i=0;i<=k;i++) ans=(ans+f[n][i])%p;
		printf("%d\n",ans);
	}
	return 0;
}


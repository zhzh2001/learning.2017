#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=400005;
int f[N],dis[N],dis1[N],dis2[N],n,m,P,k,x,y,z,fi1[N],fi2[N],ne1[N],ne2[N],zz1[N],zz2[N];
int num1,num2,d[N],sl1[N],sl2[N],tot,T,Q[N],dp[N/2][55],b[N],la[N];
void shang(int x)
{
	if (x==1)return;
	if (dis[f[x/2]]>dis[f[x]])
	 {
	 	swap(f[x],f[x/2]);
	 	swap(d[f[x]],d[f[x/2]]);
	 	shang(x/2);
	 }
	return; 
}
int duru()
{
	int x=0;char c=getchar();
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
void down(int x)
{
	int i=x;
	if (x*2<=tot&&dis[f[x*2]]<dis[f[x]])i=x*2;
	if (x*2<tot&&dis[f[x*2+1]]<dis[f[i]])i=x*2+1;
	if (i!=x)
	 {
	 	swap(f[i],f[x]);
	 	swap(d[f[i]],d[f[x]]);
	 	down(i);
	 }
	return; 
}
void jb(int x,int y,int z)
{
	ne1[++num1]=fi1[x];
	la[num1]=x;
	fi1[x]=num1;
	zz1[num1]=y;
	sl1[num1]=z;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);	
	T=duru();
	while (T--)
	 {
	 	n=duru();m=duru();
	 	k=duru();P=duru();
	 	num1=num2=0;
	 	memset(fi1,0,sizeof fi1);
	 	memset(fi2,0,sizeof fi2);
	 	while (m--)
	 	 {
	 	 	x=duru();y=duru();z=duru();
	 	 	jb(x,y,z);
		 } 
		memset(dis,0x3f,sizeof dis);
		memset(dp,0,sizeof dp);
		for (int i=1;i<=n;i++)f[i]=d[i]=i;
		dis[1]=0;tot=n;
		for (int i=1;i<=n;i++)
		 {
		 	int l=f[1];
		 	Q[i]=l;		
			b[l]=i; 	
		 	d[f[tot]]=1;
		 	f[1]=f[tot--];
		 	down(1);
		 	for (int j=fi1[l];j;j=ne1[j])
		 	 if (dis[zz1[j]]>dis[l]+sl1[j])
		 	  {
		 	  	dis[zz1[j]]=dis[l]+sl1[j];
		 	  	shang(d[zz1[j]]);
			  }
		 }  
		for (int i=1;i<=n;i++)dis1[i]=dis[i]; 		
		dp[0][1]=1;
		for (int j=0;j<=k;j++)
	 	 for (int i=1;i<=n;i++)
		  for (int l=fi1[i];l;l=ne1[l])
		   if (j+dis1[i]+sl1[l]-dis1[zz1[l]]<=k&&j+dis1[i]+sl1[l]-dis1[zz1[l]]>=0)
		    (dp[j+dis1[i]+sl1[l]-dis1[zz1[l]]][zz1[l]]+=dp[j][i])%=P;
		int ans=0;
		for (int i=0;i<=k;i++)
		 (ans+=dp[i][n])%=P;
		printf("%d\n",ans);   
	 }
	return 0; 
}

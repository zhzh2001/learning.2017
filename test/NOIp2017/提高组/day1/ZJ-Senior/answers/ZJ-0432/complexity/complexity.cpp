#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
int T,l,f[105],k,now,Q[2005],in[2005],q,p;
char s[105];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	 {
	 	scanf("%d",&l);
	 	scanf("%s\n",&s);
	 	k=0;
	 	if (s[2]=='1')k=0;
	 	else for(int i=4;(s[i]>='0'&&s[i]<='9');i++)k=k*10+s[i]-48; 
	 	int q=0,fff=0,da=0;
	 	p=now=0;
	 	memset(in,0,sizeof in);
	 	memset(Q,0,sizeof Q);
	 	memset(f,0,sizeof f);
	 	while (l--)
	 	 {
	 	 	gets(s);
	 	 	if (fff)continue;
	 	 	if (s[0]=='E')
	 	 	 {
	 	 	 	if (p==0)
				 {
				 	puts("ERR");
				 	fff=1;
				 }
				in[Q[p]]=0;				 
				if (f[p]==-1)
				 {
				 	da=1;
				 	p--;
				 	continue;
				 } 
				now-=f[p];
				p--; 
			 }
			else
			 {
			 	p++;f[p]=0;Q[p]=s[2];
			 	if (in[s[2]]==1)
			 	 {
				 	puts("ERR");
				 	fff=1;
					continue;		 	 	
				 }
				if (da==1)continue; 
				in[s[2]]=1;
				int i=3,M1=0,M2=0;
				for (;(s[i]<'0'||s[i]>'9')&&s[i]!='n';i++);
				if (s[i]=='n')
				 {
				 	M1=1e9;
				 	i++;
				 }
				else for (;s[i]>='0'&&s[i]<='9';i++)M1=M1*10+s[i]-48;
				for (;(s[i]<'0'||s[i]>'9')&&s[i]!='n';i++);
				if (s[i]=='n')
				 {
				 	M2=1e9;
				 	i++;
				 }
				else for (;s[i]>='0'&&s[i]<='9';i++)M2=M2*10+s[i]-48;				 
				if (M1>M2)f[p]=-1,da=1;
				else if (M2==1e9&&M1!=1e9)
				 {
					now++;f[p]=1;
					q=max(now,q);
				 }
				else f[p]=0; 
			 } 
		 }
		if (fff==1)continue;
		if (p>0)puts("ERR");
		else if (q==k)puts("Yes");
		else puts("No"); 
	 }
	return 0; 
}

const
  mo=1000000;
type
  re=record
    a,b,c:longint;
end;
var
  i,j,m,n,k,p,s,ll,l,c,d,e,vv,h,t,u,v,oo,w,num,x:longint;
  dis,head,q:array[1..1200000]of longint;
  a,heap:array[1..1000000]of re;
  f:array[1..1000000]of boolean;
  ft:array[1..100000,0..52]of longint;
  f2:array[1..100000,0..52]of boolean;
procedure arr(x,y,z:longint);
begin
  inc(l);
  a[l].a:=head[x];
  a[l].b:=y;
  a[l].c:=z;
  head[x]:=l;
end;
procedure swap(var x,y:re);
var
  tmp:re;
begin
  tmp:=x; x:=y; y:=tmp;
end;
procedure insert(x,y:longint);
var
  i,j:longint;
begin
  inc(ll); heap[ll].a:=x; heap[ll].b:=y;
  i:=ll;
  while (i>1) and (heap[i].b<heap[i div 2].b) do
  begin
    swap(heap[i],heap[i div 2]);
    i:=i div 2;
  end;
end;
procedure delete;
var
  i,j,x:longint;
begin
  heap[1]:=heap[ll]; dec(ll);
  i:=1;
  while (i*2<=ll) do
  begin
    if (i*2+1>ll) or (heap[i*2].b<heap[i*2+1].b) then x:=i*2 else x:=i*2+1;
    if heap[x].b<heap[i].b then
    begin
      swap(heap[x],heap[i]);
      i:=x;
    end else break;
  end;
end;
begin
  assign(input,'park.in');
  assign(output,'park.out');
  reset(input);
  rewrite(output);
  readln(s);
  for w:=1 to s do
  begin
    readln(n,m,k,p); l:=0; fillchar(head,sizeof(head),0);
    for i:=1 to m do
    begin
      read(c,d,e); arr(c,d,e);
    end;
    filldword(dis,sizeof(dis) div 4,maxlongint div 3);
    fillchar(f,sizeof(f),true);
    h:=0; t:=1; q[1]:=1; dis[1]:=0;
    while h<>t do
    begin
      inc(h); h:=(h-1) mod mo+1;
      vv:=q[h]; u:=head[vv];
      while u<>0 do
      begin
        v:=a[u].b;
        if dis[v]>dis[vv]+a[u].c then
        begin
          dis[v]:=dis[vv]+a[u].c;
          if f[v] then
          begin
            inc(t); t:=(t-1) mod mo+1; q[t]:=v; f[v]:=false;
          end;
        end;
        u:=a[u].a;
      end;
      f[vv]:=true;
    end;
    fillchar(heap,sizeof(heap),0); ll:=0; fillchar(f2,sizeof(f2),true);
    fillchar(ft,sizeof(ft),0); ft[1,0]:=1;
    insert(1,0);
    while ll>0 do
    begin
      while (ll>0) and (f2[heap[1].a,heap[1].b-dis[heap[1].a]]=false) do delete;
      if ll=0 then break;
      vv:=heap[1].a; oo:=heap[1].b; delete; f2[vv,oo-dis[vv]]:=false; u:=head[vv];
      while u<>0 do
      begin
        v:=a[u].b;
        if a[u].c+oo<=dis[v]+k then
        begin
          x:=ft[v,a[u].c+oo-dis[v]];
          x:=(x+ft[vv,oo-dis[vv]]) mod p;
          ft[v,a[u].c+oo-dis[v]]:=x;
          insert(v,a[u].c+oo);
        end;
        u:=a[u].a;
      end;
    end;
    num:=0;
    for i:=0 to k do
      num:=(num+ft[n,i]) mod p;
    writeln(num);
  end;
  close(input);
  close(output);
end.
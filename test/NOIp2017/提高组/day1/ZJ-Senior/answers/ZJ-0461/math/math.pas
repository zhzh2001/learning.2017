var
  i,j,m,n:longint;
  a:array[1..10000000]of longint;
  f:array[0..10000000]of boolean;
begin
  assign(input,'math.in');
  assign(output,'math.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  fillchar(f,sizeof(f),false);
  f[0]:=true;
  for i:=1 to 10000000 do
  begin
    if i-n>=0 then if f[i-n] then f[i]:=true;
    if i-m>=0 then if f[i-m] then f[i]:=true;
  end;
  for i:=10000000 downto 1 do
    if f[i]=false then
    begin
      writeln(i);
      exit;
    end;
  close(input);
  close(output);
end.
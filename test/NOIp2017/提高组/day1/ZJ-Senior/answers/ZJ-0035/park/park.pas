type edge=record
  to_,next,val:longint;
   end;
const maxn=5000+50;
  INF=1<<30;
var e:array[0..maxn*2]of edge;
  head,dis:array[0..maxn]of longint;
  q:array[1..maxn*10]of longint;
  inq:array[1..maxn*10]of boolean;
  cnt,n,m,i,k,t,u,v,val,p,d,ans,idx:longint;
  dp,dg,c,pre:array[1..maxn]of longint;
  vis:array[1..maxn,1..maxn]of boolean;
  road:array[1..maxn,1..maxn]of longint;
  flag:boolean;

procedure add(u,v,val:longint);
begin
  inc(cnt);
  e[cnt].to_:=v;
  e[cnt].val:=val;
  e[cnt].next:=head[u];
  head[u]:=cnt;
end;

procedure sort();
var i,l,r,x,now,to_:longint;
begin
  l:=1; r:=0;
  for i:=1 to n do
   if dg[i]=0 then
    begin inc(r); c[r]:=i end;
  while l<=r do
   begin
    x:=c[l]; inc(l);
    now:=head[x];
    while now<>0 do
     begin
      to_:=e[now].to_;
      dec(dg[to_]);
      if dg[to_]=0 then
       begin inc(r); c[r]:=to_; end;
      now:=e[now].next;
     end;
   end;
end;

{function judge():boolean;
var top,i,x,now,to_,val:longint;
begin
  for i:=1 to n do
   begin dis[i]:=INF; inq[i]:=false; end;
  top:=1; q[top]:=1;
  dis[1]:=0; inq[1]:=true;
  while top>0 do
   begin
    x:=q[top]; dec(top);
    now:=head[x];
    while now<>0 do
     begin
      to_:=e[now].to_;
      val:=e[now].val;
      if dis[x]+val<dis[to_] then
       begin
        dis[to_]:=dis[x]+val;
        if not inq[to_] then
         begin inc(top); q[top]:=to_; inq[to_]:=true; end
        else exit(false);
       end;
      now:=e[now].next;
     end;
   end;
  exit(true);
end;}

procedure judge(x:longint);
var now,to_,val:longint;
begin
  if not flag then exit();
  inq[x]:=true;
  now:=head[x];
  while now<>0 do
   begin
    to_:=e[now].to_;
    val:=e[now].val;
    if dis[x]+val<dis[to_] then
     begin
      dis[to_]:=dis[x]+val;
      if inq[to_] then flag:=false;
      judge(to_);
     end;
    now:=e[now].next;
   end;
  inq[x]:=false;
end;

function spfa():longint;
var l,r,i,x,now,to_,val:longint;
begin
  for i:=1 to n do
   begin dis[i]:=INF; inq[i]:=false; end;
  l:=1; r:=1; q[l]:=1;
  dis[1]:=0; inq[1]:=true;
  while l<=r do
   begin
    x:=q[l]; inc(l);
    now:=head[x];
    while now<>0 do
     begin
      to_:=e[now].to_;
      val:=e[now].val;
      if not vis[x,to_] then
       begin now:=e[now].next; continue; end;
      if dis[x]+val<dis[to_] then
       begin
        dis[to_]:=dis[x]+val;
        pre[to_]:=x;
        if not inq[to_] then
         begin inc(r); q[r]:=to_; inq[to_]:=true; end;
       end;
      now:=e[now].next;
     end;
    inq[x]:=false;
   end;
  exit(dis[n]);
end;

procedure dfs();
var now,x,i,j,len:longint;
  s:array[1..maxn]of longint;
  flag:boolean;
begin
  len:=0;
  x:=spfa();
  if x>d+k then exit();
  ans:=(ans+1)mod p;
  now:=n; inc(len); s[len]:=now;
  repeat
   now:=pre[now];
   inc(len);
   s[len]:=now;
  until now=1;
  flag:=false;
  for i:=1 to idx do
   if road[i,1]=s[1] then
    begin
     flag:=true;
     for j:=1 to len do
      if road[i,j]<>s[j] then flag:=false;
     if flag then break;
    end;
  if flag then
   begin dec(ans); exit(); end;
  inc(idx);
  for i:=1 to len do
   road[idx,i]:=s[i];
  for i:=2 to len do
   begin
    vis[s[i],s[i-1]]:=false;
    dfs();
    vis[s[i],s[i-1]]:=true;
   end;
end;

begin
  //writeln(sizeof(q)/1024/1024:0:2);
  assign(input,'park.in'); reset(input);
  assign(output,'park.out'); rewrite(output);
  read(t);
  while t>0 do
   begin
    read(n,m,k,p);
    fillchar(head,sizeof(head),0);
    cnt:=0;
    for i:=1 to m do
     begin
      read(u,v,val);
      if val=0 then val:=-1;
      add(u,v,val);
      inc(dg[v]);
     end;
    for i:=1 to n do dis[i]:=INF;
    dis[1]:=0;
    flag:=true; judge(1);
    if not flag then
     writeln(-1)
    else
     begin
      ans:=0;
      for i:=1 to m do
       if e[i].val=-1 then
        e[i].val:=0;
      fillchar(vis,sizeof(vis),true);
      d:=spfa();
      //sort();
      dfs();
      writeln(ans);
     end;
    dec(t);
   end;
  close(input); close(output);
end.

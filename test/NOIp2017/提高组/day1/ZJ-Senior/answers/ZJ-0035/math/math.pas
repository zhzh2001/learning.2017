const maxn=1000;
  p=100000000;
var a,b,t,x,y,sum,len,lcm:int64;
  ans:array[1..maxn]of int64;

procedure exgcd(a,b:int64;var x,y:int64);
begin
  if b=0 then
   begin x:=1; y:=0; exit(); end;
  exgcd(b,a mod b,y,x);
  y:=y-(a div b)*x;
end;

procedure mul();
var i:longint;
  x:int64;
begin
  len:=1;
  ans[len]:=sum;
  while ans[len]>p do
   begin
    inc(len);
    ans[len]:=ans[len-1]div p;
    ans[len-1]:=ans[len-1]mod p;
   end;
  x:=0;
  for i:=1 to len do
   begin
    ans[i]:=ans[i]*a+x;
    x:=ans[i]div p;
    ans[i]:=ans[i]mod p;
   end;
  while x>0 do
   begin
    inc(len);
    ans[len]:=x;
    x:=ans[len]div p;
   end;
  ans[1]:=ans[1]-1; i:=1;
  while ans[i]<0 do
   begin
    inc(i);
    ans[i]:=ans[i]-1;
    ans[i-1]:=ans[i-1]+p;
   end;
end;

function pow(x,y:int64):int64;
var ret,now:int64;
begin
  ret:=1; now:=x;
  while y<>0 do
   begin
    if y and 1=1 then ret:=ret*now mod a;
    now:=now*now mod a;
    y:=y>>1;
   end;
  exit(ret);
end;

procedure print();
var i,num,k:longint;
begin
  write(ans[len]);
  for i:=len-1 downto 1 do
   begin
    num:=trunc(ln(ans[i])/ln(10))+1;
    for k:=num+1 to 8 do
     write(0);
    write(ans[i]);
   end;
end;

begin
  assign(input,'math.in'); reset(input);
  assign(output,'math.out'); rewrite(output);
  read(a,b);
  if a>b then
   begin t:=a; a:=b; b:=t; end;
  //exgcd(a,b,x,y);
  //writeln(x,' ',y);

  y:=pow(b,a-2);
  y:=y mod a;
  x:=(b*y)div a;

  //while x>b do x:=x-b;

  if x<0 then x:=-x;
  if y<0 then y:=-y;
  sum:=x*(a-1);
  //ans:=sum*a-1;
  //writeln(ans);
  mul();
  print();
  close(input); close(output);
end.

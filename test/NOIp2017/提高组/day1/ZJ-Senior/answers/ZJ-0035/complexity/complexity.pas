const maxn=1000;
  INF=2333333;
var vis:array[0..maxn]of boolean;
  s,pre,to_,q,f:array[-maxn..maxn]of longint;
  x,y:array[0..maxn]of longint;
  p:array[0..maxn]of ansistring;
  t,l,i,w:longint;
  ss:ansistring;
  flag:boolean;

function map(ch:char):longint;
begin
  exit(ord(ch)-ord('a')+1);
end;

procedure init();
var ret,len,pp:longint;
begin
  len:=length(ss);
  ret:=0;
  while (ss[1]<='9')and(ss[1]>='0')do
   begin
    ret:=ret*10+ord(ss[1])-ord('0');
    delete(ss,1,1);
   end;
  l:=ret;
  if pos('n',ss)=0 then
   flag:=false
  else
   begin
    flag:=true;
    pp:=pos('^',ss);
    delete(ss,1,pp);
    ret:=0;
    while(ss[1]<='9')and(ss[1]>='0')do
     begin
      ret:=ret*10+ord(ss[1])-ord('0');
      delete(ss,1,1);
     end;
    w:=ret;
   end;
end;

function calc(l,r:longint):longint;
var i,ret,temp:longint;
begin
  if l+1=r then exit(f[l]);
  i:=l+1; ret:=0;
  if x[l]>y[l] then exit(0);
  while i<=r-1 do
   begin
    temp:=calc(i,to_[i]);
    if temp>ret then ret:=temp;
    i:=to_[i]+1;
   end;
  exit(ret+f[l]);
end;

function work():boolean;
var top,in_,i,num,len,cnt,temp:longint;
  opt,ch:char;
  ss,tt:ansistring;
begin
  for i:=1 to l do
   begin
    opt:=p[i,1];
    if opt='F' then
     begin
      //writeln(p[i]);
      len:=length(p[i]);
      num:=pos(' ',p[i]);
      ss:=copy(p[i],num+1,len-num);
      //writeln(ss);
      len:=length(ss);
      num:=pos(' ',ss);
      tt:=copy(ss,num+1,len-num);
      num:=pos(' ',tt);
      //writeln(tt);
      if tt[1]='n' then
       x[i]:=INF
      else
       begin
        ss:=copy(tt,1,num-1);
        val(ss,x[i]);
       end;
      if tt[num+1]='n' then
       y[i]:=INF
      else
       begin
        ss:=copy(tt,num+1,len-num);
        val(ss,y[i]);
       end;
     end;
   end;
  for i:=1 to l do
   begin
    opt:=p[i,1];
    if opt='F' then
     begin
      if x[i]>y[i] then
       f[i]:=0
      else if y[i]-x[i]<=100 then
       f[i]:=0
      else
       f[i]:=1;
     end
   end;
  top:=0; cnt:=0; i:=1;
  while i<=l do
   begin
    opt:=p[i,1];
    temp:=calc(i,to_[i]);
    if temp>cnt then cnt:=temp;
    i:=to_[i]+1;
    {if opt='F' then
     begin
      inc(top);
      if x[i]>y[i] then
       s[top]:=0
      else if y[i]-x[i]<=100 then
       s[top]:=0
      else
       s[top]:=1;
     end
    else
     begin
      s[top]:=s[top]*calc(pre[i);
      cnt:=cnt+s[top];
      dec(top);
     end;}
   end;
  if(not flag)and(cnt=0)then exit(true);
  if flag and(cnt=w)then exit(true);
  exit(false);
end;

function safe():boolean;
var top,i:longint;
  ch,d:char;
begin
  fillchar(vis,sizeof(vis),false);
  top:=0;
  for i:=1 to l do
   begin
    ch:=p[i,1];
    if ch='F' then
     begin
      d:=p[i,3];
      inc(top);
      s[top]:=map(d);
      q[top]:=i;
      if vis[map(d)] then exit(false);
      vis[map(d)]:=true;
     end
    else
     begin
      vis[s[top]]:=false;
      pre[i]:=q[top];
      to_[q[top]]:=i;
      dec(top);
      if top<0 then exit(false);
     end;
   end;
  if top<>0 then exit(false);
  exit(true);
end;

begin
  assign(input,'complexity.in'); reset(input);
  assign(output,'complexity.out'); rewrite(output);
  readln(t);
  while t>0  do
   begin
    readln(ss);
    init();
    for i:=1 to l do readln(p[i]);
    if safe() then
     begin
      if work() then
       writeln('Yes')
      else
       writeln('No');
     end
    else
     writeln('ERR');
    dec(t);
   end;
  close(input); close(output);
end.

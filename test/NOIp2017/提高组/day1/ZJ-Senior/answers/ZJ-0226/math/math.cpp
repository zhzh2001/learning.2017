#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
int i,j,m,n;
unsigned long long a1,a2,tmp;
unsigned long long ans;
int cnt;
bool isp[1000000];
int INF=1<<30;
int main(){
	ans=0;
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	cin>>a1>>a2;
	if(a1<a2){
		tmp=a1;
		a1=a2;
		a2=tmp;
	}
	for(i=2;i<INF;i++){
		if(((i*a2+1)%a1)==0){
			break;
		}
	}
	ans=(a2*i*(a2-1))-1;
	cout<<ans<<endl;
	return 0;
}

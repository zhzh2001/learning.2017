#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=10005;
int t,n,a,p,d,f,w,u,v,ans,y[N];
char s[N],c[N],x[N],st[N];
bool vis[30];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d",&n);
		scanf("%s",s);
		int l=strlen(s);
		a=ans=d=p=w=0; f=1;
		if (l>4)
		{
			for (int i=0;i<l;++i) if (s[i]>='0'&&s[i]<='9') a=a*10+s[i]-'0';
		}
		memset(vis,0,sizeof(vis));
		memset(y,0,sizeof(y));
		for (int i=1;i<=n;++i)
		{
			scanf("%s",c);
			if (c[0]=='E')
			{
				vis[st[d]-'a']=0;
				if (d<0) f=0;
				if (d==w) w=0;
				if (w==0) p-=y[d];
				--d;
			}
			if (c[0]=='F')
			{
				scanf("%s",c);
				if (vis[c[0]-'a']) f=0;
				vis[c[0]-'a']=1;
				st[++d]=c[0];
				scanf("%s",c);
				scanf("%s",x);
				if (c[0]=='n'&&x[0]!='n'&&w==0) w=d,y[d]=0;
				if (c[0]!='n'&&x[0]=='n'&&w==0) ++p,y[d]=1;
				if (c[0]=='n'&&x[0]=='n'&&w==0) y[d]=0;
				if (c[0]!='n'&&x[0]!='n'&&w==0)
				{
					u=v=0;
					int len1=strlen(c);
					for (int j=0;j<len1;++j) u=u*10+c[j]-'0';
					int len2=strlen(x);
					for (int j=0;j<len2;++j) v=v*10+x[j]-'0';
					if (u<=v) y[d]=0;
					if (u>v&&w==0) w=d;
				}
				if (p>ans) ans=p;
			}
		}
		if (d>0||f==0) printf("ERR\n");
		else if (ans!=a) printf("No\n");
		else printf("Yes\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

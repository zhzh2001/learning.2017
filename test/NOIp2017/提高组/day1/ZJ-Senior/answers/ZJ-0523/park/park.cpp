#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
using namespace std;
const int N=100005,M=200005;
int ca,t,n,m,k,p,x,y,z,cnt,ans,toi[M],le[M],nxt[M],hd[N],dis[N],nu[N],f[N][50],g[N][50];
bool vis[N];
void init()
{
	++ca; cnt=ans=0;
	memset(nxt,0,sizeof(nxt));
	memset(hd,0,sizeof(hd));
	memset(dis,60,sizeof(dis));
	memset(nu,0,sizeof(nu));
	memset(f,0,sizeof(f));
	memset(g,0,sizeof(g));
	memset(vis,0,sizeof(vis));
}
void addedge(int u,int v,int w){
	toi[++cnt]=v; le[cnt]=w; nxt[cnt]=hd[u]; hd[u]=cnt;
}
void SPFA()
{
	queue<int> Q;
	while (!Q.empty()) Q.pop();
	Q.push(1); dis[1]=0; vis[1]=1;
	while (!Q.empty())
	{
		int u=Q.front(); Q.pop(); vis[u]=0;
		for (int i=hd[u];i;i=nxt[i])
		{
			int v=toi[i];
			++nu[v];
			if (nu[v]>n) continue;
			if (dis[v]>dis[u]+le[i])
			{
				dis[v]=dis[u]+le[i];
				if (!vis[v])
				{
					Q.push(v);
					vis[v]=1;
				}
			}
		}
	}
}
void solve()
{
	queue<int> Q;
	while (!Q.empty()) Q.pop();
	memset(vis,0,sizeof(vis));
	Q.push(1); vis[1]=1;
	while (!Q.empty())
	{
		int u=Q.front(); Q.pop(); vis[u]=0;
		for (int i=hd[u];i;i=nxt[i])
		{
			int v=toi[i];
			for (int j=0;j<=k;++j)
			if (dis[u]+le[i]-dis[v]+j>k) break;
			else if (f[u][j]==0) continue;
			else
			{
				++g[v][dis[u]+le[i]-dis[v]+j];
				if (g[v][dis[u]+le[i]-dis[v]+j]>n)
				{
					if (dis[u]+le[i]-dis[v]+j<=dis[n]+k) dis[n]=-1;
					return;
				}
				f[v][dis[u]+le[i]-dis[v]+j]+=f[u][j];
				f[v][dis[u]+le[i]-dis[v]+j]%=p;
				if (!vis[v])
				{
					Q.push(v);
					vis[v]=1;
				}
			}
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t); ca=0;
	while (t--)
	{
		init();
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1;i<=m;++i)
		{
			scanf("%d%d%d",&x,&y,&z);
			addedge(x,y,z);
		}
		SPFA();
		f[1][0]=1; solve();
		if (dis[n]==-1) {printf("-1\n"); continue;}
		for (int i=0;i<=k;++i) ans=(ans+f[n][i])%p;
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
using namespace std;
long long a, b;
int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	scanf("%lld%lld", &a, &b);
	printf("%lld\n", (a-1)*(b-1)-1);
	fclose(stdin); fclose(stdout);
	return 0;
}
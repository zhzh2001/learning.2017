#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define I(ch) (ch>='0' && ch<='9')
using namespace std;
const int N=300;
char op[N], s[N];
int vis[N], stk[N], alp[N];
int T, n;
inline int max(int a, int b) {return a>b?a:b;}
inline void mljmy(char c) {
	if (c=='E') return;
	scanf("%s", s+1);
	scanf("%s", s+1);
	scanf("%s", s+1);
}
int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	for (scanf("%d", &T); T--; ) {
		scanf("%d%s", &n, op+1);
		memset(vis, 0, sizeof vis);
		memset(stk, 0, sizeof stk);
		memset(alp, 0, sizeof alp);
		int ce=0, lp=0, br=666, ans=0;
		for (int i=1; i<=n; i++) {
			scanf("%s", s+1);
			if (ce) {mljmy(s[1]); continue;}
			if (s[1]=='E') {
				vis[alp[lp--]]=0;
				if (lp<br) br=666;
				if (lp<0) {ce=1; continue;}
			}
			if (s[1]=='F') {
				lp++, stk[lp]=stk[lp-1];
				scanf("%s", s+1);
				if (vis[s[1]-97]) {ce=1; scanf("%s", s+1); scanf("%s", s+1); continue;}
				else vis[alp[lp]=s[1]-97]=1;
				if (lp>=br) {scanf("%s", s+1); scanf("%s", s+1); continue;}
				scanf("%s", s+1);
				int l=0;
				if (s[1]=='n') br=2333;
				else for (unsigned k=1; k<=strlen(s+1) && I(s[k]); k++) l=l*10+s[k]-48;
				scanf("%s", s+1);
				if (br!=2333 && s[1]!='n') {
					int r=0;
					for (unsigned k=1; k<=strlen(s+1) && I(s[k]); k++) r=r*10+s[k]-48;
					if (l>r) br=lp;
				}
				if (s[1]!='n' && br==2333) br=lp;
				if (s[1]=='n' && br!=2333) ans=max(ans, ++stk[lp]), br=666;
			}
		}
		if (ce || lp>0) puts("ERR");
		else if (!ans) puts(op[3]=='1'?"Yes":"No");
		else {
			int res=0;
			for (unsigned k=5; k<=strlen(op+1) && I(op[k]); k++) res=res*10+op[k]-48;
			puts(res==ans?"Yes":"No");
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#include <stack>
#include <vector>
#define pii pair<int, int>
#define X first
#define Y second
using namespace std;
const int N=2e5+10;
bool vis[N], mrk[N];
int pnt[N<<1], nxt[N<<1], fst[N], fir[N], len[N<<1], bel[N];
int x[N], y[N], z[N];
long long dis[2][N], dp[N], sz[N], cir[N];
int T, n, m, k, p, cnt, blk;
vector<int> g[N], G[N];
stack<int> pth;
priority_queue<pii, vector<pii>, greater<pii> > q;
queue<pii> Q;
void dfs(int x) {
	vis[x]=1;
	for (unsigned i=0; i<g[x].size(); i++)
		if (!vis[g[x][i]]) dfs(g[x][i]);
	pth.push(x);
}
void DFS(int x) {
	mrk[x]=1, bel[x]=blk;
	for (unsigned i=0; i<G[x].size(); i++)
		if (!mrk[G[x][i]]) DFS(G[x][i]);
}
void link(int x, int y, int z) {
	pnt[++cnt]=y; nxt[cnt]=fst[x]; fst[x]=cnt; len[cnt]=z;
	pnt[++cnt]=x; nxt[cnt]=fir[y]; fir[y]=cnt; len[cnt]=z;
	g[x].push_back(y), G[y].push_back(x);
}
void dijkstra0() {
	q.push(make_pair(0, 1)), dis[0][1]=0;
	while (!q.empty()) {
		pii t=q.top(); q.pop();
		for (int i=fst[t.Y]; i; i=nxt[i])
			if (dis[0][t.Y]+len[i]<dis[0][pnt[i]]) q.push(make_pair(dis[0][pnt[i]]=dis[0][t.Y]+len[i], pnt[i]));
	}
}
void dijkstra1() {
	q.push(make_pair(0, n)), dis[1][n]=0;
	while (!q.empty()) {
		pii t=q.top(); q.pop();
		for (int i=fir[t.Y]; i; i=nxt[i])
			if (dis[1][t.Y]+len[i]<dis[1][pnt[i]]) q.push(make_pair(dis[1][pnt[i]]=dis[1][t.Y]+len[i], pnt[i]));
	}
}
int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	for (scanf("%d", &T); T--; ) {
		scanf("%d%d%d%d", &n, &m, &k ,&p);
		memset(fst, 0, sizeof fst);
		memset(fir, 0, sizeof fir);
		cnt=blk=0;
		for (int i=1; i<=n; i++) g[i].clear(), G[i].clear();
		for (int i=1; i<=m; i++) scanf("%d%d%d", x+i, y+i, z+i), link(x[i], y[i], z[i]);
		memset(vis, 0, sizeof vis);
		memset(mrk, 0, sizeof mrk);
		dfs(1);
		for ( ; !pth.empty(); pth.pop())
			if (!mrk[pth.top()]) blk++, DFS(pth.top());
		memset(sz, 0, sizeof sz);
		memset(cir, 0, sizeof cir);
		for (int i=1; i<=n; i++) sz[bel[i]]++;
		for (int i=1; i<=n; i++)
			if (bel[x[i]]==bel[y[i]]) cir[bel[x[i]]]+=z[i];
		memset(dis, 63, sizeof dis);
		dijkstra0(), dijkstra1();
		int d=dis[0][n];
		memset(dp, 0, sizeof dp);
		while (!Q.empty()) Q.pop();
		Q.push(make_pair(0, 1)), dp[1]=1;
		while (!Q.empty()) {
			pii t=Q.front(); Q.pop();
			if (sz[bel[t.Y]]>1 && cir[bel[t.Y]]==0) {dp[n]=-1; break;}
			for (int i=fst[t.Y]; i; i=nxt[i])
				if (t.X+len[i]+dis[1][pnt[i]]<=d+k) dp[pnt[i]]=(dp[pnt[i]]+dp[t.Y])%p, Q.push(make_pair(t.X+len[i], pnt[i]));
		}
		printf("%lld\n", dp[n]);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
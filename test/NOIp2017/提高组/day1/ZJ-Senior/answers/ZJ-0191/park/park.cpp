#include <bits/stdc++.h>
using namespace std;

struct edge
{
	int to,value,next;
}e[200010];
int tt,head[100010],_queue[40000010],vv[40000010],ll[40000010],cnt,n,m,k,p;

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&tt);
	for(int o=1;o<=tt;o++)
	{
		cnt=0;
		memset(head,0,sizeof(head));
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for(int i=1;i<=m;i++)
		{
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			cnt++;
			if (head[a]==0) head[a]=cnt; else
			{
				int ii=head[a];
				while (e[ii].next!=-1) ii=e[ii].next;
				e[ii].next=cnt;
			}
			e[cnt].to=b;
			e[cnt].value=c;
			e[cnt].next=-1;
		}
		int h=0,t=1;
		_queue[1]=1;
		vv[1]=0;
		ll[0]=0;
		int minn=100000000;
		while (h<t)
		{
			h++;
			if (_queue[h]==n)
			{
				if (vv[h]<=minn+k)
				{
					ll[0]++;
					ll[ll[0]]=vv[h];
					if (vv[h]<minn) minn=vv[h];
				}
				continue;
			}
			int it=head[_queue[h]];
			while (it!=-1)
			{
				t++;
				_queue[t]=e[it].to;
				vv[t]=vv[h]+e[it].value;
				it=e[it].next;
			}
		}
		minn+=k;
		int ans=0;
		for(int i=1;i<=ll[0];i++)
			if(ll[i]<=minn) ans=(ans+1)%p;
		printf("%d\n",ans);
	}
}

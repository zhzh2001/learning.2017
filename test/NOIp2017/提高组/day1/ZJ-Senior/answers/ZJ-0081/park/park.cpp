#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 1006
#define maxe 2006
using namespace std;
inline char nc(){
	static char buf[100000],*i=buf,*j=buf;
	return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;
}
inline int _read(){
	char ch=nc();int sum=0,p=1;
	while(ch!='-'&&!(ch>='0'&&ch<='9'))ch=nc();
	if(ch=='-')p=-1,ch=nc();
	while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();
	return sum*p;
}
int T,n,e,K,p,ans,tot,lnk[maxn],son[maxe],nxt[maxe],w[maxe],f[maxn][maxn];
void add(int x,int y,int z){
	nxt[++tot]=lnk[x];son[tot]=y;w[tot]=z;lnk[x]=tot;
}
void dfs(int x,int sum){
	if(sum>f[1][n]+K)return;
	if(x==n)ans=(ans+1)%p;
	for(int j=lnk[x];j;j=nxt[j]) dfs(son[j],sum+w[j]);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=_read();
	while(T--){
		n=_read();e=_read();K=_read();p=_read();ans=0;
		memset(f,63,sizeof(f));
		for(int i=1,x,y,z;i<=e;i++)x=_read(),y=_read(),z=_read(),f[x][y]=z,add(x,y,z);
		for(int k=1;k<=n;k++)
		 for(int i=1;i<=n;i++)
		  for(int j=1;j<=n;j++) if(i!=j&&j!=k&&i!=k)f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
		dfs(1,0);
		printf("%d\n",ans);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 106
using namespace std;
inline char nc(){
	static char buf[100000],*i=buf,*j=buf;
	return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;
}
inline int _read(){
	char ch=nc();int sum=0,p=1;
	while(ch!='-'&&!(ch>='0'&&ch<='9'))ch=nc();
	if(ch=='-')p=-1,ch=nc();
	while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();
	return sum*p;
}
int T,n,m,top,p[maxn],f[maxn],stack[maxn];
bool vis[30],use[maxn];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=_read();
	for(int t=1;t<=T;t++){
		n=_read();memset(use,1,sizeof(use));memset(vis,1,sizeof(vis));f[0]=0;
		bool c=1;top=0;nc();nc();
		char x=nc();
		if(x=='1')m=0,nc(),nc();else
		if(x=='n'){
			nc();x=nc();m=0;
			while(x>='0'&&x<='9')m=m*10+x-48,x=nc();
			nc();
		}
		int count=0;
		for(int i=1;i<=n;i++){
			char ch=nc();
			if(ch=='F'){
				nc();char ch1=nc();use[++top]=1;f[top]=p[top]=0;stack[top]=ch1-'a'+1;
				if(!vis[ch1-'a'+1])c=0;
				vis[ch1-'a'+1]=0;
				nc();ch1=nc();
				if(ch1=='n'){
					nc();ch1=nc();
					if(ch1!='n'){
						use[top]=0;
						while(ch1!='\n')ch1=nc();
					}else nc();
				}else{
					int num1=0;
					while(ch1>='0'&&ch1<='9')num1=num1*10+ch1-48,ch1=nc();
					ch1=nc();
					if(ch1=='n')f[top]=p[top]=1,nc();else{
						int num2=0;
						while(ch1>='0'&&ch1<='9')num2=num2*10+ch1-48,ch1=nc();
						if(num1>num2)use[top]=0;
					}
				}
			}else
			if(ch=='E'){
				count++;nc();
				if(top==0){
					c=0;continue;
				}
				if(use[top])f[top-1]=max(f[top-1],p[top-1]+f[top]);
				vis[stack[top--]]=1;
			}else{
				while(ch!='\n')ch=nc();
			}
		}
		if(!c||top!=0)printf("ERR\n");else{
			if(f[0]==m)printf("Yes\n");else printf("No\n");
		}
	}
	return 0;
}

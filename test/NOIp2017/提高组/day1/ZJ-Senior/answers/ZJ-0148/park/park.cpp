#include<cstdio>
#include<cstring>
#include<stack>
#include<queue>
#include<algorithm>
using namespace std;
#define ll long long
inline char tc(void){
	static char fl[10000],*A=fl,*B=fl;
	return A==B&&(B=(A=fl)+fread(fl,1,10000,stdin),A==B)?EOF:*A++;
}
inline int read(void){
	int a=0;char c;
	while((c=tc())<'0'||c>'9');
	while(c>='0'&&c<='9')a=a*10+c-'0',c=tc();
	return a;
}
struct Edge{
	int to,cost;
	Edge *nxt;
}e[200005],ef[200005],*st[100005],*stf[100005],eo[200005],*sto[100005];
int diss[100005],dist[100005],T,n,m,k,Mod;
int cnt,cl,low[100005],pre[100005],bl[100005],sz[100005],mixs[100005],mixt[100005];
int tot,totf,toto,b[100005],iq[100005];
ll f[100005][100];
bool vis[100005][100];
stack<int>S;
queue<int>que;
void spfa(int x){
	memset(iq,0,sizeof(iq));
	memset(diss,0x3f,sizeof(diss));
	diss[x]=0,que.push(x),iq[x]=1;
	while(!que.empty()){
		int now=que.front();iq[now]=0;que.pop();
		for(Edge *i=st[now];i;i=i->nxt)
			if(diss[now]+i->cost<diss[i->to]){
				diss[i->to]=diss[now]+i->cost;
				if(!iq[i->to])que.push(i->to),iq[i->to]=1;
			}
	}
	return ;
}
void spfat(int x){
	memset(iq,0,sizeof(iq));
	memset(dist,0x3f,sizeof(dist));
	dist[x]=0,que.push(x),iq[x]=1;
	while(!que.empty()){
		int now=que.front();iq[now]=0;que.pop();
		for(Edge *i=stf[now];i;i=i->nxt)
			if(dist[now]+i->cost<dist[i->to]){
				dist[i->to]=dist[now]+i->cost;
				if(!iq[i->to])que.push(i->to),iq[i->to]=1;
			}
	}
	return ;
}
inline void add(int x,int y,int z){
	e[++tot].to=y,e[tot].cost=z,e[tot].nxt=st[x],st[x]=&e[tot];
	return ;
}
inline void addf(int x,int y,int z){
	ef[++totf].to=y,ef[totf].cost=z,ef[totf].nxt=stf[x],stf[x]=&ef[totf];
	return ;
}
inline void addo(int x,int y){
	eo[++toto].to=y,eo[toto].nxt=sto[x],sto[x]=&eo[toto];
	return ;
}
void dfs(int x){
	pre[x]=low[x]=++cl,S.push(x);
	for(Edge *i=sto[x];i;i=i->nxt)
		if(!pre[i->to])dfs(i->to),low[x]=min(low[x],low[i->to]);
		else if(!bl[i->to])low[x]=min(low[x],low[i->to]);
	if(pre[x]==low[x]){
		bl[x]=++cnt,mixs[cnt]=1e9,mixt[cnt]=1e9;
		while(S.top()!=x)bl[S.top()]=cnt,mixs[cnt]=min(mixs[cnt],diss[x]),mixt[cnt]=min(mixt[cnt],dist[x]),S.pop();
		S.pop();
	}
	return ;
}
int dp(int x,int p){
	if(vis[x][p+dist[x]-diss[n]])return f[x][p+dist[x]-diss[n]];
	vis[x][p+dist[x]-diss[n]]=1,f[x][p+dist[x]-diss[n]]=(x==n);
	for(Edge *i=st[x];i;i=i->nxt)
		if(dist[i->to]!=0x3f3f3f3f&&dist[i->to]+p+i->cost<=diss[n]+k)
			(f[x][p+dist[x]-diss[n]]+=dp(i->to,p+i->cost))%=Mod;
	return f[x][p+dist[x]-diss[n]];
}
int main(void){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	register int i,x,y,z;
	T=read();
	while(T--){
		bool s=0;
		tot=totf=toto=0;
		n=read(),m=read(),k=read(),Mod=read();
		for(i=1;i<=n;++i)
			st[i]=stf[i]=sto[i]=NULL;
		for(i=1;i<=m;++i){
			x=read(),y=read(),z=read();
			add(x,y,z),addf(y,x,z);
			if(z==0)addo(x,y);
		}
		spfa(1),spfat(n);
		if(diss[n]==0x3f3f3f3f){
			puts("0");
			continue;
		}
		cnt=0,cl=0,memset(pre,0,sizeof(pre)),memset(bl,0,sizeof(bl));
		for(i=1;i<=n;++i)
			if(!pre[i])
				dfs(i);
		for(i=1;i<=cnt;++i)
			if(mixs[i]+mixt[i]<=diss[n]+k){
				s=1;break;
			}
		if(s==1){
			puts("-1");
			continue;
		}
		memset(vis,0,sizeof(vis));
		printf("%d\n",dp(1,0)%Mod);
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

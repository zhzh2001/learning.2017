#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int t,L,now=0,z,u,v,sp[321],sgua[321],mx,sc[321],b[30],e;
char s[100],c[100],ch[100],x[100],y[100];
void dfs(int st,int d,int w,int p,bool gua){
	if(w<0)e=1;
	if(st==L+1){
		if(w!=0)e=1;
		return ;
	}
	bool err=0,g=0,po=0;
	if(b[sc[d]]!=-1)--b[sc[d]],b[sc[d]]=-1;
	scanf("%s",ch+1);
	if(ch[1]=='F'){
		scanf("%s",c+1);
		sc[d]=c[1]-'a';
		if(b[sc[d]]>0)e=1;
		else ++b[sc[d]];
		scanf("%s%s",x+1,y+1);
		if(x[1]=='n'&&y[1]!='n'){
			g=1;
		}
		if(x[1]!='n'&&y[1]=='n'){
			po=1;
		}
		if(x[1]!='n'&&y[1]!='n'){
			u=0;
			for(int i=1;x[i]<='9'&&x[i]>='0';++i)
				u=u*10+x[i]-'0';
			v=0;
			for(int i=1;y[i]<='9'&&y[i]>='0';++i)
				v=v*10+y[i]-'0';
			if(u>v)gua=1;
		}
		sp[d]=sp[d-1]+po,sgua[d]=sgua[d-1]|g;
		if(!gua)mx=max(mx,sp[d]);
		dfs(st+1,d+1,w+1,p+po,gua|g);
	}else
		dfs(st+1,d-1,w-1,sp[d-1],sgua[d-1]);
	return ;
}
void work(){
	mx=0;
	scanf("%d",&L);
	scanf("%s",s+1);
	e=0;
	dfs(1,101,0,0,0);
	if(e==1){
		puts("ERR");
		return ;
	}
	if(s[3]=='n'){
		now=0;
		for(int i=5;s[i]<='9'&&s[i]>='0';++i)
			now=now*10+s[i]-'0';
		if(now==mx){
			puts("Yes");
			return ;
		}
		puts("No");
		return ;
	}else{
			if(mx==0){
			puts("Yes");
			return ;
		}
		puts("No");
		return ;
	}
	return ;
}
int main(void){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	register int i;
	scanf("%d",&t);
	while(t--){
		memset(b,0,sizeof(b)),memset(sc,0xff,sizeof(sc));
		work();
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

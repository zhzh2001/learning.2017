#include<cstdio>
#include<algorithm>
using namespace std;
long long a,b,s;
long long gcd(int a,int b){
	if(a%b==0)return 0;
	else return 1ll*(b-1)*b*(a/b)+gcd(b,a%b);
}
int main(void){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	if(a>b)swap(a,b);
	printf("%lld",gcd(b,a)-1);
	fclose(stdin),fclose(stdout);
	return 0;
}

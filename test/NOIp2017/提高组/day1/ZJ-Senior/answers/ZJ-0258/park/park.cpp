#include<bits/stdc++.h>
using namespace std;
const int maxn=100000;

struct node{
	int from;
	int to;
	int w;
}edge[maxn];
typedef struct node ns;
vector<int> v[maxn];
int cnt=0;
inline void add(int a,int b,int c){
	 edge[cnt].from=a;
	 edge[cnt].to=b;
	 edge[cnt].w=c;
	 v[a].push_back(cnt);
	 cnt++;
	  	
}
inline int SPFA(int n){
	int vis[maxn];
	int dis[maxn];
	
	queue<int> q;
	q.push(1);
	
	for (int i=0;i<maxn;i++)dis[i]=((1<<15)-1);
	dis[1]=0;
	memset(vis,0,sizeof(vis));
	vis[1]=1;
	while(!q.empty()){
		int nd=q.front();
		q.pop();
		vis[nd]=0;
		vector<int> &vs=v[nd];
		for (int i=0;i<vs.size();i++){
			int w=edge[vs[i]].w;
			int to=edge[vs[i]].to;
			if(dis[nd]+w<dis[to]){
				dis[to]=min(dis[to],dis[nd]+w);
				if(vis[to]==0){
					q.push(to);
					vis[to]=1;
				}
			}
			
		}
	}
	return dis[n];
}
//int dp[5000][5000];
int vis[maxn];
int ans=0;
void dfs(int x,int way ,int ms,int n,int p,int time){
	if(vis[x])return;
	if (ans==-1)return;
	if (time>=(1<<15)){
		ans=-1;
		return;
	}
	if (x==n&&way<=ms){
    ans++;
    ans=ans%p;
    return;
	}
	
	vis[x]=1;
	vector<int> &vs=v[x];
	for (int j=0;j<vs.size();j++){
		int w=edge[vs[j]].w;
		int to=edge[vs[j]].to;
		if (w+way<=ms){
			dfs(to,way+w,ms,n,p,time);
		}
		
	}
	return;


}
int main(){
	//ios::sync_with_stdio(false);
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;
	//scanf("%d",&T);
	cin>>T;
	//T=1;
	while(T--){
		int flag=0;
		ans=0;
		memset(vis,0,sizeof(vis));
		for (int i=0;i<maxn;i++)v[i].clear();
		cnt=0;
		memset(edge,0,sizeof(edge));
     	int n,m,k,p;
		//scanf("%d%d%d%d",&n,&m,&k,&p);
		cin>>n>>m>>k>>p;
		for (int i=1;i<=m;i++){
			int x,y,z;
			//scanf("%d%d%d",&x,&y,&z);
			cin>>x>>y>>z;
			add(x,y,z);
			
		}
		//if (flag==1)continue;
		int d=SPFA(n);
		/*for (int x=0;x<=d+k;x++)dp[n][x]=1;
		for (int i=n;i>=1;i--){
			vector<int> &vs=v[i];
			
				for (int j=0;j<vs.size();j++){
				for (int x=d;x<=k+d;x++){
				int w=edge[vs[j]].w;
				int to=edge[vs[j]].to;
					if (x-w>=0){
						dp[i][x]+=dp[to][x-w];
					}
			    }
				
			}
			
		}
		int ans=0;
		for (int x=0;x<=d+k;x++)ans=max(ans,dp[n][x]);
		cout<<ans;	*/
		dfs(1,0,d+k,n,p,0);
		cout<<ans<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


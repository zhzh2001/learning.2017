#include<bits/stdc++.h>
//#include<cstring>
using namespace std;
const int maxn=1<<15-1;
//记得加回inline

inline void read(int &x){
	x=0;
	int f=0;
	char c=getchar();
	while(c<'0'||c>'9'){
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=(x<<3)+(x<<1)+(c^48);
		c=getchar();
	}
	return;
} 
string change[101];
int changenum;
inline bool check(string c){//true 没有重复 
	for (int i=0;i<changenum;i++){
		if (c==change[i]){
			//cout<<endl<<"chongfu!! "<<c<<endl;
			return false;
		}
	}
	return true;
}
inline void del(){
	change[changenum--]="";
	return ;
}
void close(){
	fclose(stdin);
	fclose(stdout);
}
inline bool add(string c){
	if (check(c)){
		change[changenum]=c;
		changenum++;
		return true;
	}
	return false;
}
bool ranges(int a,int b){
	if (b==-1){
		b=maxn;
	}
	if (a==-1){
		a=maxn;
	}
	if (b>=a)return true;
	return false;
}
int main(){
	//ios::sync_with_stdio(false);
	//memset(change,0,sizeof(change));
	
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	int T;cin>>T;
	//T=1;
	while(T--){
	int normal=1;
	for (int i=0;i<=100;i++)change[i]="";
	changenum=0;
	int line;
	read(line);
	//cin>>line;
	
	string s;
	cin>>s;
	int lf=0,rf=0;
	int complx=0;//1 o(1)  ; 2 o(n^...)
	int comnum=0;//o(n^...)
	for (int i=0;i<s.size();i++){
		if (s[i]=='('&&lf==0){
			lf=1;
		}
		if (1==lf&&s[i]=='1'){
			complx=1;
			comnum=0;
			break;
			
		}else if (1==lf&&s[i]=='n'){
			if (s[i+3]>='0'&&s[i+3]<='9'){
			   
			   complx=2;
			   
			   comnum=(s[i+3]-'0')+(s[i+2]-'0')*10;
			   break;	
			}else{
				//cout<<"second: "<<s[i+2];
				complx=2;
				comnum=s[i+2]-'0';
			//	cout<<" comnum: "<<comnum<<endl;
				break;
				//cout<<comnum<<endl;
			}
			
		}
	}

	string cts[101+3];
	int err=0;
	int now=0;
	int hasnum=0;
	int ansnum=0;
	for (int i=1;i<=line;i++){
		
		//if (err)continue;
		char flag;
		cin>>flag;
		int from=0;
		int to=0;
		if (flag=='F'){
			 now++;
		     string chg;
		     cin>>chg;
			 if (!add(chg)){
		 	    err=1;
		 	    //cout<<endl<<"add false"<<endl;
		     }
		     string first;
		     string end;
		     cin>>first;
		     cin>>end;
		     if (first[0]<'0'||first[0]>'9'){
		     	from=-1;
			 }else{
			 	from=atoi(first.c_str());
			 }
			 if (end[0]=='n'){
		     	to=-1;
			 }else{
			 	to=atoi(end.c_str());
			 }
			 //cout<<"chg: "<<chg<<" from: "<<from<<" to: "<<to<<endl;
			 if (!ranges(from,to)){
		 	     normal=0;
		 	     
		     }
			 else{
			 	if (to!=from && to==-1&&normal){
			 		hasnum++;
				 }
			 }
			 
		}
		if (flag=='E'){
			now--;
			if (now<0)err=1;
			ansnum=max(ansnum,hasnum);
			hasnum--;
			del();
			if(now==0)normal=1;
		}
		
		
		
	}
	if (now!=0)err=1;
	//cout<<" comnum: "<<comnum<<endl;
	if (err||(line%2)){
		cout<<"ERR"<<endl;
		//cout<<"Now: "<<now<<endl;
	    //close();  记得加上！！！！！！！！！！！！！！！ 
	
	}
	else if (ansnum==comnum){
		cout<<"Yes"<<endl;
		
	
	}else{
		
		//cout<<"ansnum: "<<ansnum<<" comnum: "<<comnum<<" No"<<endl;
		cout<<"No"<<endl;
	}
	
    }
	//close();
	fclose(stdin);
	fclose(stdout);
	return 0;
}


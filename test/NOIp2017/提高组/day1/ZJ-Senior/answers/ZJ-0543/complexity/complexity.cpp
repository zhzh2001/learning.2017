#include<map>
#include<set>
#include<queue> 
#include<cctype>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
static const int M = 105;
static const int INF = (int)1e5;
char Cur[M];
int L[M],R[M];//0-> Constant, 1-> n
int ans[M];
char goal[M];
char Ty[M],Var[M],St[M],Ed[M];
bool mark[INF];
bool f[M];
vector<int>es[M];
int stk[M];
int Rd(char A[]){
	if(A[0]=='n')return INF;
	int x;
	sscanf(A,"%d",&x);
	return x;
}
int input(){
	int n; 
	scanf("%d%s",&n,goal);
	int top=0,tot=0;
	memset(mark,0,sizeof(mark));
	for(int i=0;i<=n;++i)es[i].clear(),L[i]=R[i]=0;
	bool Lan=true;
	L[0]=R[0]=1;
	for(int i=1;i<=n;++i){
		scanf("%s",Ty);
		if(Ty[0]=='F'){
			++tot;
			es[stk[top]].push_back(tot);
			stk[++top]=tot;
			scanf("%s%s%s",Var,St,Ed);
			L[tot]=Rd(St),R[tot]=Rd(Ed);
			if(mark[Var[0]]){
				Lan=false;
			}else{
				Cur[tot]=Var[0];
				mark[Var[0]]=true;
			}
		}else{//Ty[0]=='E'
			if(top){
				mark[Cur[stk[top]]]=false;
				--top;
			}else Lan=false;//top=0,No element
		}
	}
	if(top)Lan=false;
	if(Lan)return tot;
	else return -1;
}
void dfs(int u){
	if(L[u]>R[u]){
		ans[u]=0;
		return ;
	}//L[u]<=R[u]
	if(es[u].empty()){
		if(L[u]==R[u]||R[u]<M)ans[u]=0;
		else ans[u]=1;
		return ;
	}
	for(int i=0,v;i<es[u].size();++i){
		v=es[u][i];
		dfs(v);
		ans[u]=max(ans[u],ans[v]);
	}
	if(L[u]==R[u]||R[u]<M);//O(1)
	else ++ans[u];
}
void solve(int n){
	memset(ans,false,sizeof(ans));
	dfs(0);
	int len=strlen(goal);
	int Goal;
	if(len<5)Goal=0;
	else{//[4,len-2]
		Goal=0;
		for(int i=4;i<len;++i){
			if(isdigit(goal[i])){
				Goal=Goal*10+(goal[i]-'0');
			}
		}
	}
	if(ans[0]!=Goal)puts("No");
	else puts("Yes");
}
//�ļ��� ll mod negative 
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	for(int cas=1,n;cas<=T;++cas){
		n=input();
		if(~n)solve(n);
		else puts("ERR");
	}
	return 0;
}

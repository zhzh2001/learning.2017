#include<map>
#include<set>
#include<queue> 
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
static const int M = (int)1e5+5;
int P;
struct node{
	int to,cost;
	bool operator <(const node &tmp)const{
		return cost>tmp.cost;
	}
};
vector<node>es[M];
void mod(int &a,int b){
	a+=b;
	if(a>=P)a-=P;
}
struct Water{//n<=5
	static const int MM = 505;
	static const int MAXn = 5005;
	int dis[MM][MM];
	int dp[MAXn][10];
	void chkmi(int &a,int b){
		if(a==-1||a>b)a=b;
	}
	void Floyd(int n){
		for(int k=1;k<=n;++k){
			for(int i=1;i<=n;++i){
				for(int j=1;j<=n;++j){
					if(~dis[i][k]&&~dis[k][j]){
						chkmi(dis[i][j],dis[i][k]+dis[k][j]);
					}
				}
			}
		}
	}
	void solve(int n,int m,int K){
		memset(dis,-1,sizeof(dis));
		for(int i=1;i<=n;++i)dis[i][i]=0;
		for(int u=1;u<=n;++u){
			for(int i=0,v;i<es[u].size();++i){
				v=es[u][i].to;
				chkmi(dis[u][v],es[u][i].cost);
			}
		}
		Floyd(n);
		if(dis[1][n]==-1){
			puts("0");
			return ;
		}
		memset(dp,0,sizeof(dp));
		int mx=dis[1][n]+K;
		int ans=0;
		dp[0][1]=1;
		for(int d=0;d<=mx;++d){
			for(int u=1;u<=n;++u)if(dp[d][u]){
				for(int i=0,v;i<es[u].size();++i){
					v=es[u][i].to;
					if(d+es[u][i].cost<=mx)
						mod(dp[d+es[u][i].cost][v],dp[d][u]);
				}
			}
			mod(ans,dp[d][n]);
		}
		printf("%d\n",ans);
	}
}P1;
namespace Medium{
	int dis[M];
	bool mark[M];
	int dp[55][M];
	int cur[M];
	priority_queue<node>Q;
	node que[M*55];
	bool chkmi(int &a,int b){
		if(a==-1||a>b){
			a=b;
			return true;
		}
		return false;
	}
	void Dijkstra(int n){
		memset(dis,-1,sizeof(dis));
		memset(mark,0,sizeof(mark));
		while(!Q.empty())Q.pop();
		dis[1]=0;
		Q.push((node){1,0});
		while(!Q.empty()){
			node now=Q.top();Q.pop();
			int u=now.to;
			if(mark[u])continue;
			mark[u]=true;
			for(int i=0,v;i<es[u].size();++i){
				v=es[u][i].to;
				if(!mark[v]&&chkmi(dis[v],dis[u]+es[u][i].cost)){
					Q.push((node){v,dis[v]});
				}
			}
		}
	}
	void solve(int n,int m,int K){
		Dijkstra(n);
		if(dis[n]==-1){
			puts("0");
			return ;
		}
//		int tot=0;
//		for(int i=1;i<=n;++i)if(~dis[i]){
//			for(int j=0;j<=K;++j){
//				que[++tot]=(node){i,dis[i]+j};
//			}
//		}
		while(!Q.empty())Q.pop();
		for(int i=1;i<=n;++i)if(~dis[i]){
			Q.push((node){i,dis[i]});
			cur[i]=1;
		}
		memset(dp,0,sizeof(dp));
//		sort(que+1,que+tot+1);
		dp[0][1]=1;
		int u,d,res,j,v,c;
		while(!Q.empty()){
			node now=Q.top();Q.pop();
			u=now.to,d=now.cost,res=dp[d-dis[u]][u];
			if(cur[u]<=K){
				++cur[u];
				Q.push((node){u,d+1});
			}
			if(res){
				for(j=0;j<es[u].size();++j){
					v=es[u][j].to,c=es[u][j].cost;
					if(d+c<=dis[v]+K){
						mod(dp[d+c-dis[v]][v],res);
					}
				}
			}
		}
//		for(int i=tot,u,d,j,v,c,res;i;--i){
//			u=que[i].to,d=que[i].cost;
//			res=dp[d-dis[u]][u];
//			if(res){
//				for(j=0;j<es[u].size();++j){
//					v=es[u][j].to,c=es[u][j].cost;
//					if(d+c<=dis[v]+K){
//						mod(dp[d+c-dis[v]][v],res);
//					}
//				}
//			}
//		}
		int ans=0;
		for(int i=0;i<=K;++i)mod(ans,dp[i][n]);
		printf("%d\n",ans);
	}
}
//64MB
//�ļ��� ll mod negative 
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T,n,m,K;
	scanf("%d",&T);
	for(int cas=1;cas<=T;++cas){
		scanf("%d%d%d%d",&n,&m,&K,&P);
		bool All=true;
		for(int i=1;i<=n;++i)es[i].clear();
		for(int i=1,u,v,c;i<=m;++i){
			scanf("%d%d%d",&u,&v,&c);
			es[u].push_back((node){v,c});
			All&=(c!=0);
		}
		if(n<=5)P1.solve(n,m,K);
		else if(All)Medium::solve(n,m,K);
		else puts("-1");
	}
	return 0;
}

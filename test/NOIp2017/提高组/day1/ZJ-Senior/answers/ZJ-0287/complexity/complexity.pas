var
 gbbhy,gbbhya,k,n,m,f,i,j,ans,fs,ss:longint;
 l,r,z,p:array[0..110]of longint;
 st:ansistring;
 s:array[0..110]of ansistring;
 ch:array[0..110]of char;
procedure dfs(l,r:longint;var f:longint);
var
 st,x,y:ansistring;
 k,a,b,c,d:longint;
begin
 if l>r then exit;
 if p[r]=l then
 begin
  st:=copy(s[l],5,length(s[l])-4);
  k:=pos(' ',st);
  x:=copy(st,1,k-1);
  y:=copy(st,k+1,length(st)-k);
  if x='n' then
   if y='n' then dfs(l+1,r-1,f)
            else exit
           else
   if y='n' then begin f:=f+1;dfs(l+1,r-1,f);end
            else begin val(x,a);val(y,b);if a<=b then dfs(l+1,r-1,f) else exit;end;
 end
           else
 begin
  c:=f;
  dfs(l,p[r]-1,f);
  d:=f;
  f:=c;
  dfs(p[r],r,f);
  if f<d then f:=d;
 end;
end;
begin
assign(input,'complexity.in');reset(input);
assign(output,'complexity.out');rewrite(output);
readln(gbbhy);
for gbbhya:=1 to gbbhy do
begin
 readln(st);
 k:=pos(' ',st);
 val(copy(st,1,k-1),n);
 st:=copy(st,k+1,length(st)-k);
 m:=0;
 k:=0;
 f:=0;
 for i:=1 to n do
 begin
  readln(s[i]);if f=1 then continue;
  if s[i][1]='F' then
  begin
   for j:=1 to k do
    if ch[j]=s[i][3] then begin f:=1;break end;
   k:=k+1;
   ch[k]:=s[i][3];
   if k=1 then begin m:=m+1;l[m]:=i;end;
   z[k]:=i
  end
                 else
  if s[i][1]='E' then begin p[i]:=z[k];k:=k-1;if k<0 then f:=1;if k=0 then r[m]:=i;end;
 end;
 if(f=1)or(k>0)or(n mod 2>0)then begin writeln('ERR');continue;end;
 fs:=0;
 for i:=1 to m do
 begin
  ans:=0;
  dfs(l[i],r[i],ans);
  if ans>fs then fs:=ans
 end;
 k:=pos('^',st);
 if k=0 then ss:=0 else val(copy(st,k+1,length(st)-k-1),ss);
 if ss=fs then writeln('Yes') else writeln('No')
end;
close(input);
close(output);
end.

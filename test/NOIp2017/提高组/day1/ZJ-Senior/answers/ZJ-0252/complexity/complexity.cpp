#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>

using namespace std;

struct Prog {
	char c, var, x[5], y[5];
};

struct Comp {
	bool var; int n;
	Comp(bool _var = 0, int _n = 0) : var(_var), n(_n) {}
	bool operator < (const Comp &a) const {
		return var == a.var ? n < a.n : var < a.var;
	}
	Comp operator * (const Comp &a) const {
		if (! n) return Comp(0, 0);
		if (! var && ! a.var) return Comp(0, 1);
		if (var && ! a.var) return Comp(1, n);
		if (! var && a.var) return Comp(1, a.n);
		return Comp(1, n + a.n);
	}
};

bool bigger(char a[], char b[]) {
	int lena = strlen(a), lenb = strlen(b);
	if (lena != lenb) return lena > lenb;
	for (int i = 0; i < lena; ++ i) if (a[i] != b[i]) return a[i] > b[i];
	return false;
}

struct Solver {
private:
	static const int N = 110;
	int l;
	bool vis[200];
	char comp[10], st[N];
	Prog prog[N];

	void input() {
		scanf("%d %s", &l, comp);
		for (int i = 1; i <= l; ++ i) {
			scanf(" %c", &prog[i].c);
			if (prog[i].c == 'F') scanf(" %c %s %s", &prog[i].var, prog[i].x, prog[i].y);
		}
	}

	int find_nxt(int t) {
		int cnt = 1;
		while (cnt) ++ t, cnt += (prog[t].c == 'F') - (prog[t].c == 'E');
		return t;
	}

	Comp get(int l, int r) {
		Comp ret(0, 0);
		for (int i = l; i <= r; ++ i) {
			int nxt = find_nxt(i);
			Comp tmp;
			if (nxt - i > 1) {
				Comp x(0, 1);
				if (prog[i].x[0] != 'n' && prog[i].y[0] == 'n') x = Comp(1, 1);
				if (prog[i].x[0] == 'n' && prog[i].y[0] != 'n') x = Comp(0, 0);
				if (prog[i].x[0] != 'n' && prog[i].y[0] != 'n' && bigger(prog[i].x, prog[i].y)) x = Comp(0, 0);
				tmp = x * get(i + 1, nxt - 1);
			} else {
				tmp = Comp(0, 1);
				if (prog[i].x[0] != 'n' && prog[l].y[0] == 'n') tmp = Comp(1, 1);
				if (prog[i].x[0] == 'n' && prog[l].y[0] != 'n') tmp = Comp(0, 0);
				if (prog[i].x[0] != 'n' && prog[i].y[0] != 'n' && bigger(prog[i].x, prog[i].y)) tmp = Comp(0, 0);
			}
			if (ret < tmp) ret = tmp;
			i = nxt;
		}
		return ret;
	}

	void process() {
		memset(vis, 0, sizeof vis);
		int cnt = 0, top = 0;
		for (int i = 1; i <= l; ++ i) {
			if (prog[i].c == 'F') {
				if (vis[prog[i].var]) { printf("ERR\n"); return; }
				vis[st[++ top] = prog[i].var] = true, ++ cnt;
			} else {
				-- cnt; if (cnt < 0) { printf("ERR\n"); return; }
				vis[st[top --]] = false;
			}
		}
		if (cnt) { printf("ERR\n"); return; }
		Comp cmp = get(1, l);
		if (cmp.var != (comp[2] == 'n')) { printf("No\n"); return; }
		if (comp[2] == 'n') {
			int tmp = 0, pos = 4;
			while (comp[pos] >= '0' && comp[pos] <= '9') (tmp *= 10) += comp[pos] - '0', ++ pos;
			if (tmp != cmp.n) { printf("No\n"); return; }
		}
		printf("Yes\n");
	}

public:
	void solve() {
		input(), process();
	}
} solver;

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T --) solver.solve();
	return 0;
}


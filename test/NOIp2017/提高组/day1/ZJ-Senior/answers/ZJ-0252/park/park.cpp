#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
#include <iostream>

using namespace std;

	static const int N = 100010;
	static const int M = 200010;
	static const int K = 55;
	int n, m, k, pri, nume, h[N], dist[N];
	bool update[N];
	struct Edge {
		int v, w, nxt;
	} e[M];
	priority_queue <pair <int, int>, vector <pair <int, int> >, greater <pair <int, int> > > que;
	int numte, th[N << 6], in[N << 6], val[N << 6];
	int q[N << 6];
	struct TopoEdge {
		int v, nxt;
	} te[M << 6];

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T --) {
		scanf("%d%d%d%d", &n, &m, &k, &pri), memset(h, nume = 0, sizeof h);
		for (int i = 1; i <= m; ++ i) {
			int a, b, c; scanf("%d%d%d", &a, &b, &c), e[++ nume] = (Edge) { b, c, h[a] }, h[a] = nume;
		}
		memset(dist, 0x3f, sizeof dist), memset(update, 0, sizeof update);
		dist[1] = 0, que.push(make_pair(0, 1));
		while (! que.empty()) {
			pair <int, int> p = que.top(); que.pop();
			if (update[p.second]) continue; update[p.second] = true;
			for (int i = h[p.second]; i; i = e[i].nxt)
				if (! update[e[i].v] && dist[e[i].v] > p.first + e[i].w) {
					dist[e[i].v] = p.first + e[i].w;
					que.push(make_pair(dist[e[i].v], e[i].v));
				}
		}
		numte = 0;
		for (int i = 1; i <= n; ++ i)
			for (int j = 0; j <= k; ++ j) th[(i << 6) + j] = in[(i << 6) + j] = val[(i << 6) + j] = 0;
		for (int i = 1; i <= n; ++ i)
			for (int j = h[i]; j; j = e[j].nxt) {
				int pos = e[j].v, tmp = dist[i] + e[j].w - dist[pos], mx = k - tmp, i6 = i << 6, p6 = pos << 6;
				for (int l = 0; l <= mx; ++ l) {
					int u = i6 + l, v = p6 + l + tmp;
					te[++ numte] = (TopoEdge) { v, th[u] }, th[u] = numte;
					++ in[v];
				}
			}
		val[1 << 6] = 1; int s = 0, t = 0;
		for (int i = 1; i <= n; ++ i)
			for (int j = 0; j <= k; ++ j) if (! in[(i << 6) + j]) q[++ t] = (i << 6) + j;
		while (s < t) {
			int p = q[++ s], tmp = val[p];
			for (int i = th[p]; i; i = te[i].nxt) {
				int u = te[i].v;
				-- in[u], val[u] += tmp;
				if (val[u] >= pri) val[u] -= pri;
				if (! in[u]) q[++ t] = u;
			}
		}
		bool flag = false;
		for (int i = 1; i <= n; ++ i) {
			for (int j = 0; j <= k; ++ j) if (in[(i << 6) + j] && val[(i << 6) + j]) { printf("-1\n"), flag = true; break; }
			if (flag) break;
		}
		if (flag) continue;
		int ans = 0;
		for (int i = 0; i <= k; ++ i) (ans += val[(n << 6) + i]) %= pri;
		printf("%d\n", ans);
	}
	return 0;
}


#include <cstdio>
#include <cstring>
#include <algorithm>

#define int long long

using namespace std;

struct Solver {
private:
	int n, m;

	void input() {
		scanf("%lld%lld", &n, &m);
	}

	void init() {
		if (n > m) n ^= m ^= n ^= m;
	}

	void process() {
		printf("%lld\n", n * (m - 1) - m);
	}

public:
	void solve() {
		input(), init(), process();
	}
} solver;

signed main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	solver.solve();
	return 0;
}


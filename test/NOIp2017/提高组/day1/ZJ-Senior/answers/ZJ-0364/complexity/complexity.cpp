#include<iostream>
#include<cstdio>
using namespace std;
int n,m,i,sum,d[110],j,k,t,tt,b[110],c[110],x,y,f[110],ans;
char s[110];
bool flag,a[110];
int p(int l,int r){
	if (l>r) return 0;
	if ((l+1==r)&&(d[l]==r))
	  {
	  	if (c[l]==2)
	  	  return 1;
	  	else
	  	  return 0;
	  }
	if (d[l]==r)
	  {
	  	int rqes;
	  	rqes=p(l+1,r-1);
	  	if (c[l]==0)
	  	  rqes=0;
	  	if (c[l]==2)
	  	  rqes++;
	  	return rqes;
	  }
	int rqes;
	rqes=max(p(l,d[l]),p(d[l]+1,r));
	return rqes;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	  {
	  	flag=false;
	  	tt=0;
	  	m=0;
	  	scanf("%d",&n);
	  	gets(s);
	  	if (s[3]=='1')
	  	  sum=0;
	  	else
	  	  {
	  	  	sum=s[5]-48;
	  	  	j=6;
	  	  	while ((s[j]>='0')&&(s[j]<='9'))
	  	  	  {
	  	  	  	sum=sum*10+s[j]-48;
	  	  	  	j++;
			  }
		  }
		for (i=1;i<=n;i++)
		  {
		  	gets(s);
		  	if (flag) continue;
		  	if (s[0]=='F')
		  	  {
		  	  	a[i]=true;
		  	  	tt++;
		  	  	for (j=1;j<=m;j++)
		  	  	  if (b[j]==s[2])
		  	  	    {
		  	  	    	flag=true;
		  	  	    	break;
					}
				if (!flag)
				  {
				  	m++;
				  	b[m]=s[2];
		  	  	    j=4;
		  	  	    if (s[j]=='n')
		  	  	      {
		  	  	  	    x=10000;
		  	  	  	    j++;
				      }
		  	  	    else 
		  	  	      {
		  	  	  	    x=0;
		  	  	  	    while ((s[j]>='0')&&(s[j]<='9'))
		  	  	  	      {
		  	  	  	  	    x=10*x+s[j]-48;
		  	  	  	  	    j++;
					      }
				      }
				    j++;
				    if (s[j]=='n')
				      {
				  	    y=10000;
				      }
				    else
				      {
				      	y=0;
				      	while ((s[j]>='0')&&(s[j]<='9'))
				      	  {
				      	  	y=10*y+s[j]-48;
				      	  	j++;
						  }
					  }
					if ((y-x>=0)&&(y-x<1000))
					  c[i]=1;
					if (x>y)
					  c[i]=0;
					if (y-x>1000)
					  c[i]=2;
				  }
			  }
			else
			  {
			  	a[i]=false;
			  	tt--;
			  	m--;
			  	if (tt<0)
			  	  flag=true;
			  }
		  }
		if (tt!=0)
		  flag=true;
		if (flag)
		  printf("ERR\n");
		else
		  {
		  	for (i=1;i<=n;i++)
		  	  {
		  	  	if (a[i])
		  	  	  {
		  	  	  	f[0]++;
		  	  	  	f[f[0]]=i;
				  }
				else
				  {
				  	d[f[f[0]]]=i;
				  	d[i]=f[f[0]];
				  	f[0]--;
				  }
			  }
			ans=p(1,n);
			if (ans==sum)
			  printf("Yes\n");
			else
			  printf("No\n");
		  }
	  }
	fclose(stdin);fclose(stdout);
	return 0;
}

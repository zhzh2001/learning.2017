#include <bits/stdc++.h>
using namespace std;
int T,n;
char a[2001],ch,v,va[2001];
int top,st[2001];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	for(scanf("%d",&T);T;T--)
	{
		scanf("%d%s",&n,a+1);
		int len=strlen(a+1),check=0;
		if(len>4)
			for(int i=5;i<len;i++)
				check=check*10+(a[i]-'0');
		top=0;st[0]=0;
		bool ERR=0;
		int ma=0;
		for(int i=1;i<=n;i++)
		{
			for(ch=getchar();ch!='E' && ch!='F';ch=getchar());
			if(ch=='F')
			{
				for(v=getchar();v<'a' || v>'z';v=getchar());
				va[++top]=v;
				for(int j=1;j<top;j++)
				if(va[j]==v)
				{
					ERR=1;
					break;
				}
				char tem;
				for(tem=getchar();tem!='n' && (tem<'0' || tem>'9');tem=getchar());
				int x,y;
				if(tem=='n')
					x=-1;
				else
				{
					x=tem-'0';
					for(tem=getchar();tem>='0' && tem<='9';tem=getchar())
						x=x*10+(tem-'0');
				}
				for(tem=getchar();tem!='n' && (tem<'0' || tem>'9');tem=getchar());//copy
				if(tem=='n')
					y=-1;
				else
				{
					y=tem-'0';
					for(tem=getchar();tem>='0' && tem<='9';tem=getchar())
						y=y*10+(tem-'0');
				}
				if(x==-1 && y==-1)
					st[top]=st[top-1];
				else
				if(x==-1)
					st[top]=-1;
				else
				if(y==-1)
					st[top]=(st[top-1]==-1)?-1:(st[top-1]+1);
				else
					st[top]=(x>y)?-1:st[top-1];
			}
			if(ch=='E')
				ma=max(ma,st[top--]);
		}
		if(ERR || top) puts("ERR");
		else
			puts(check==ma?"Yes":"No");
	}
	return 0;
}

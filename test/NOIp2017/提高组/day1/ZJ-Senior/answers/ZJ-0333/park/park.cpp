#include <bits/stdc++.h>
#define mp make_pair
using namespace std;
int TIME,top,E,revE,EE,MOD,T,n,m,MAXK,NODE,sttime,stp,endp;
bool vis[200001],inf[200001];
int dfn[200001],low[200001],st[200001],tar[200001];
int fir[200001],nex[400001],wei[400001],to[400001];
int Fir[200001],Nex[400001],To[400001],revwei[400001];
int bx[400001],by[400001],bz[400001],dis[200001];
int revfir[200001],revnex[400001],revto[400001];
int dp[200001][51];
priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > > pr;
void add(int x,int y,int z)
{
	to[++E]=y;nex[E]=fir[x];fir[x]=E;wei[E]=z;
	revto[++revE]=x;revnex[revE]=revfir[y];revfir[y]=revE;revwei[revE]=z;
}
void Add(int x,int y)
{
	To[++EE]=y;Nex[EE]=Fir[x];Fir[x]=EE;
}
void tarjan(int x)
{
	vis[x]=1;
	dfn[x]=low[x]=++TIME;
	st[++top]=x;
	for(int i=Fir[x];i;i=Nex[i])
	if(vis[To[i]])
	{
		if(dfn[To[i]]>sttime)
			low[x]=min(low[x],dfn[To[i]]);
	}
	else
		tarjan(To[i]),low[x]=min(low[x],low[To[i]]);
	if(dfn[x]==low[x])                         
	{
		tar[x]=++NODE;
		while(st[top]!=x) tar[st[top--]]=NODE,inf[NODE]=1;
		top--;
	}
}
void dfs(int now,int k)
{
	if(vis[now] || dis[now]==2000000000) return;
	vis[now]=1;
	if(inf[now])
	{
		dp[now][k]=-1;
		return;
	}
	dp[now][k]=(now==stp && k==0);
	for(int i=revfir[now];i && dp[now][k]>=0;i=revnex[i])
	{
		int v=revto[i],w=revwei[i];
		if(w==0) dfs(v,k);
		if(k>=w)
		{
			if(dp[v][k-w]==-1)
				dp[now][k]=-1;
			else
			{
				dp[now][k]+=dp[v][k-w];
				if(dp[now][k]>=MOD) dp[now][k]-=MOD;
			}
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	for(scanf("%d",&T);T;T--)
	{
		scanf("%d%d%d%d",&n,&m,&MAXK,&MOD);
		NODE=TIME=E=revE=EE=top=0;
		for(int i=1;i<=n;i++)
			fir[i]=Fir[i]=inf[i]=vis[i]=revfir[i]=dfn[i]=low[i]=0;
		for(int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&bx[i],&by[i],&bz[i]);
			if(bz[i]==0)
				Add(bx[i],by[i]);
		}
		for(int i=1;i<=n;i++)
			if(!vis[i])
			{
				sttime=TIME;
				tarjan(i);
			}
		for(int i=1;i<=n;i++)
			vis[i]=0;
		stp=tar[1];endp=tar[n];
		n=NODE;
		for(int i=1;i<=m;i++)
		if(tar[bx[i]]!=tar[by[i]])
			add(tar[bx[i]],tar[by[i]],bz[i]);
		for(int i=1;i<=n;i++)
			dis[i]=2000000000;
		dis[stp]=0;
		while(!pr.empty()) pr.pop();
		for(pr.push(mp(0,stp));!pr.empty();)
		{
			int u=pr.top().second;pr.pop();
			if(vis[u]) continue;
			vis[u]=1;
			for(int i=fir[u];i;i=nex[i])
			if(dis[to[i]]>dis[u]+wei[i])
			{
				dis[to[i]]=dis[u]+wei[i];
				pr.push(mp(dis[to[i]],to[i]));
			}
		}
		for(int i=1;i<=n;i++)
			for(int j=revfir[i];j;j=revnex[j])
			if(dis[revto[j]]!=2000000000 && dis[i]!=2000000000)
				revwei[j]+=dis[revto[j]]-dis[i];
		dp[stp][0]=1;
		int ret=0;
		bool gg=0;
		for(int k=0;k<=MAXK;k++)
		{
			for(int i=1;i<=n;i++)
				vis[i]=0;
			for(int i=1;i<=n;i++)
				dfs(i,k);
			if(dp[endp][k]==-1)
			{
				gg=1;
				break;
			}
			ret+=dp[endp][k];
			if(ret>=MOD) ret-=MOD;
		}
		if(gg) puts("-1");
		else
		printf("%d\n",ret);
	}
	return 0;
}

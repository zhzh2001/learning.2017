var t,cas,n,m,kk,p,i,j,k,ans,a,b,c,len,x,e,cnt:longint;leo:boolean;
    dis,sum,head:array[0..100000]of longint;
    heap,id,vet,next,val:array[0..300000]of longint;
    flag,vis:array[0..100000]of boolean;
procedure add_edge(u,v,w:longint);
begin
   inc(cnt);vet[cnt]:=v;next[cnt]:=head[u];
   head[u]:=cnt;val[cnt]:=w;
end;
procedure dfs(k,s:longint);
var e,v:longint;
begin
   if k=n then
   begin
      if s<=dis[n]+kk then begin inc(ans);ans:=ans mod p;end;
      exit;
   end;
   flag[k]:=true;
   e:=head[k];
   while e<>-1 do
   begin
      v:=vet[e];
      if(not flag[v])then dfs(v,s+val[e]);
      e:=next[e];
   end;
   flag[k]:=false;
end;
procedure put(x,y:longint);
var son,t:longint;
begin
   inc(len);heap[len]:=y;id[len]:=x;
   son:=len;
   while son<>1 do
   begin
      if heap[son]<heap[son div 2] then
      begin
         t:=heap[son];heap[son]:=heap[son div 2];heap[son div 2]:=t;
         t:=id[son];id[son]:=id[son div 2];id[son div 2]:=t;
         son:=son div 2;
      end else break;
   end;
end;
function get:longint;
var i,t,son:longint;
begin
   get:=id[1];id[1]:=id[len];heap[1]:=heap[len];dec(len);
   i:=1;
   while i+i<=len do
   begin
      son:=i+i;
      if(i+i+1<=len)and(heap[i+i+1]<heap[i+i])then son:=i+i+1;
      if heap[son]<heap[i] then
      begin
         t:=heap[son];heap[son]:=heap[i];heap[i]:=t;
         t:=id[son];id[son]:=id[i];id[i]:=t;
         i:=son;
      end else break;
   end;
end;
begin
   assign(input,'park.in');reset(input);
   assign(output,'park.out');rewrite(output);
   readln(t);
   for cas:=1 to t do
   begin
      readln(n,m,kk,p);
      cnt:=0;
      fillchar(head,sizeof(head),255);
      leo:=false;
      for i:=1 to m do
      begin
         readln(a,b,c);
         if c>0 then leo:=true;
         add_edge(a,b,c);
      end;
      if not leo then begin writeln(-1);continue;end;
      for i:=0 to n do dis[i]:=1000000000;
      fillchar(vis,sizeof(vis),false);
      fillchar(sum,sizeof(sum),0);
      dis[1]:=0;sum[1]:=1;
      put(1,0);

      while len>0 do
      begin
         x:=get;
         if vis[x] then continue;
         vis[x]:=true;
         e:=head[x];
         while e<>-1 do
         begin
            j:=vet[e];
            if(not vis[j])then
            begin
               if dis[j]=dis[x]+val[e] then sum[j]:=(sum[j]+sum[x]) mod p;
               if dis[j]>dis[x]+val[e] then
               begin
                  dis[j]:=dis[x]+val[e];
                  sum[j]:=sum[x];
                  put(j,dis[j]);
               end;
            end;
            e:=next[e];
         end;
      end;
      if n<=5 then
      begin
         ans:=0;
         fillchar(flag,sizeof(flag),false);
         dfs(1,0);
         writeln(ans);
         continue;
      end;
      if kk=0 then begin writeln(sum[n] mod p);continue;end;
      ans:=0;
      fillchar(flag,sizeof(flag),false);
      dfs(1,0);
      writeln(ans);
   end;
   close(input);close(output);
end.
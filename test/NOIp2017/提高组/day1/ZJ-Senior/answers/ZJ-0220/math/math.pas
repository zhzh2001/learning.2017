var a,b,t,ans:int64;
begin
   assign(input,'math.in');reset(input);
   assign(output,'math.out');rewrite(output);
   readln(a,b);
   if a<b then begin t:=a;a:=b;b:=t;end;
   ans:=(a-1)*(b-1)-1;
   writeln(ans);
   close(input);close(output);
end.

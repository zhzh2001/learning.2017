var t,cas,k,w,l,i,top,arr,max,res:longint;st,xx:ansistring;
    stt:array[0..1000]of char;flag:boolean;ch:char;
    x,y,belong:array[0..1000]of longint;
    sta:array[-1000..1000]of longint;
    tong:array['a'..'z']of longint;
procedure dfs(l,r,mul:longint);
var kk,i,leo:longint;
begin
   if r=l+1 then
   begin
      if mul>max then max:=mul;
      exit;
   end;
   kk:=l+1;
   while kk<=r do
   begin
      if belong[kk]>0 then begin inc(kk);continue;end;
      for i:=kk+1 to r do
         if belong[i]=kk then break;
      if x[kk]>y[kk] then
      begin
         if mul>max then max:=mul;
      end else
      if(y[kk]<1000)or((x[kk]=1000)and(y[kk]=1000))then dfs(kk,i,mul)
      else dfs(kk,i,mul+1);
      kk:=i+1;
   end;
end;
begin
   assign(input,'complexity.in');reset(input);
   assign(output,'complexity.out');rewrite(output);
   readln(t);
   for cas:=1 to t do
   begin
      readln(st);
      k:=pos(' ',st);
      val(copy(st,1,k-1),l);
      delete(st,1,k);
      k:=pos('(',st);
      delete(st,1,k);
      k:=pos(')',st);
      delete(st,k,length(st)-k+1);
      if st[1]='1' then w:=0 else
      begin
         delete(st,1,2);
         val(st,w);
      end;
      top:=0;flag:=true;
      for i:=1 to l do begin stt[i]:='n';x[i]:=0;y[i]:=0;belong[i]:=0;end;
      for i:=1 to l do
      begin
         readln(st);
         if st[1]='F' then
         begin
            delete(st,1,1);
            while st[1]=' ' do delete(st,1,1);
            k:=pos(' ',st);
            stt[i]:=st[1];
            delete(st,1,k);
            k:=pos(' ',st);
            xx:=copy(st,1,k-1);
            if xx='n' then x[i]:=1000 else val(xx,x[i]);
            delete(st,1,k);
            if st[1]='n' then y[i]:=1000 else val(st,y[i]);
            inc(top);sta[top]:=i;
         end else
         begin
            belong[i]:=sta[top];dec(top);
            if top<0 then flag:=false;
         end;
      end;
      //for i:=1 to l do writeln(stt[i],' ',x[i],' ',y[i]);

      if top>0 then
      begin
         writeln('ERR');
         continue;
      end;
      if not flag then
      begin
         writeln('ERR');
         continue;
      end;
      k:=1;
      while k<=l do
      begin
         if belong[k]>0 then begin inc(k);continue;end;
         for i:=k+1 to l do
            if belong[i]=k then break;
         arr:=i;
         for i:=k+1 to arr do
            if(belong[i]=0)then
            begin
               if stt[i]=stt[k] then begin flag:=false;break;end;
            end;
         if not flag then break;
         k:=arr+1;
      end;
      if not flag then
      begin
         writeln('ERR');
         continue;
      end;

      k:=1;max:=0;
      while k<=l do
      begin
         if belong[k]>0 then begin inc(k);continue;end;
         for i:=k+1 to l do
            if belong[i]=k then break;
         arr:=i;
         if x[k]>y[k] then begin k:=arr+1;continue;end;
         if(x[k]<1000)and(y[k]<1000)then res:=0;
         if(x[k]<1000)and(y[k]>=1000)then res:=1;
         if(x[k]>=1000)and(y[k]>=1000)then res:=0;
         if arr=k+1 then
         begin
            if res>max then max:=res;
            k:=arr+1;
            continue;
         end;
         dfs(k,arr,res);
         k:=arr+1;
      end;
      if max=w then writeln('Yes') else writeln('No');
   end;
   close(input);close(output);
end.
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=1001;
char s[N];
ll vis[N],q[N],g[N],L;
ll get(){
	ll n=strlen(s+1);	ll ans=0;
	For(i,1,n)	if(s[i]>='0'&&s[i]<='9')	ans=ans*10+s[i]-'0';
	return ans;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	ll T=read();
	while(T--){
		L=read();	scanf("%s",s+1);
		ll now=0,answ=0,top=0,can=1,opt;
		if (strlen(s+1)==4)	opt=0;
		else	opt=get();
		memset(vis,0,sizeof vis);
		For(i,1,L){
			scanf("%s",s+1);
			if (s[1]=='E'){	answ=max(answ,now);	if (top)	now-=q[top],vis[g[top--]]=0;	else	can=0;	}
			else{
				scanf("%s",s+1);
				if (vis[s[1]])	can=0;	else	vis[s[1]]=1,g[++top]=s[1];
				scanf("%s",s+1);
				ll a=get();
				scanf("%s",s+1);
				ll b=get();
				if(a&&b)	q[top]=0;
				if(!a&&!b)	q[top]=0;
				if(a&&!b)	q[top]=1;
				if(!a&&b)	q[top]=-10000;
				if(a&&b&&(a>b))		q[top]=-10000;
				now+=q[top];
			}
		}can&=!top;answ=max(answ,now);
		if (can&&answ==opt)	puts("Yes");
		else	if (!can)	puts("ERR");
		else	puts("No");
	}
}

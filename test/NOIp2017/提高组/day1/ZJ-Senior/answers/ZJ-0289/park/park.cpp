#include<queue>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=400010,inf=1e9+1;
priority_queue<pair<ll,ll>,vector<pair<ll,ll> > ,greater<pair<ll,ll > > >q;
ll x[N],y[N],w[N],DIS[N],qq[N],head[N],nxt[N],vet[N],val[N],dis[N],f[N/4][51],low[N],dfn[N],n,m,k,tot,mod,ind,top,answ,TOT;	bool vis[N],can_in[N];
void insert(ll x,ll y,ll w){	nxt[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;	}
void add(ll &x,ll y){	x=(x+y)%mod;	}
void get_s_t(){
	q.push(make_pair(0,1));	dis[1]=0;
	For(i,2,n)	dis[i]=inf;
	memset(vis,0,sizeof vis);
	while(!q.empty()){
		ll x=q.top().second;	q.pop();
		if (vis[x])	continue;	vis[x]=1;
		for(ll i=head[x];i;i=nxt[i])
		if ((i%2==0)&&dis[vet[i]]>dis[x]+val[i]){
			dis[vet[i]]=dis[x]+val[i];
			q.push(make_pair(dis[vet[i]],vet[i]));
		}
	}
}
void get_t_s(){
	q.push(make_pair(0,n));	DIS[n]=0;
	For(i,1,n-1)	DIS[i]=inf;
	memset(vis,0,sizeof vis);
	while(!q.empty()){
		ll x=q.top().second;	q.pop();
		if (vis[x])	continue;	vis[x]=1;
		for(ll i=head[x];i;i=nxt[i])
		if ((i%2==1)&&DIS[vet[i]]>DIS[x]+val[i]){
			DIS[vet[i]]=DIS[x]+val[i];
			q.push(make_pair(DIS[vet[i]],vet[i]));
		}
	}
}
void tarjan(ll x){
	low[x]=dfn[x]=++ind;	qq[++top]=x;	vis[x]=1;
	for(ll i=head[x];i;i=nxt[i])
	if (can_in[vet[i]]&&val[i]==0){
		if (vis[vet[i]])	low[x]=min(low[x],dfn[vet[i]]);
		else if (!dfn[vet[i]])	tarjan(vet[i]),low[x]=min(low[x],low[vet[i]]);
	}
	if(low[x]==dfn[x]){
		ll sz=0;
		while(qq[top+1]!=x)	vis[qq[top--]]=0,sz++;
		answ=max(answ,sz);
	}
}
ll work(){
	ll ans=0;	f[1][0]=1;
	For(tt,0,k){
		q.push(make_pair(0,1));
		memset(vis,0,sizeof vis);
		memset(dfn,0,sizeof dfn);
		For(x,1,n)	for(ll i=head[x];i;i=nxt[i])
		if (dis[x]+tt+val[i]<=dis[vet[i]]+k&&val[i]==0)	++dfn[vet[i]];
		while(!q.empty()){
			ll x=q.top().second;	q.pop();
			if (vis[x])	continue;	vis[x]=1;
			for(ll i=head[x];i;i=nxt[i])
			if (dis[x]+tt+val[i]<=dis[vet[i]]+k){
				add(f[vet[i]][dis[x]+tt+val[i]-dis[vet[i]]],f[x][tt]);
				if (val[i]==0)	--dfn[vet[i]];
				if (dis[x]+val[i]==dis[vet[i]]&&!dfn[vet[i]])	q.push(make_pair(dis[vet[i]],vet[i]));
			}
		}add(ans,f[n][tt]);
	}return ans;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	m=read();	k=read();	mod=read();	tot=1;
		memset(head,0,sizeof head);
		For(i,1,m){
			x[i]=read(),y[i]=read(),w[i]=read();
			insert(x[i],y[i],w[i]);	insert(y[i],x[i],w[i]);
		}get_s_t();	get_t_s();	answ=0;
		For(i,1,n)	can_in[i]=dis[i]+DIS[i]-dis[n]<=k;
//		For(i,1,n)	printf("%d\n",can_in[i]);
		memset(head,0,sizeof head);	tot=1;
		For(i,1,m)	if (can_in[x[i]]&&can_in[y[i]])	insert(x[i],y[i],w[i]);
		memset(vis,0,sizeof vis);
		memset(dfn,0,sizeof dfn);
		memset(low,0,sizeof low);
		For(i,1,n)	if (!dfn[i]&&can_in[i])	tarjan(i);
		if (answ>1){	puts("0");	continue;	}
		memset(f,0,sizeof f);
		writeln(work());
	}
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<windows.h>
#include<ctime>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;	}
int main(){
	freopen("math.in","w",stdout);
	srand(GetTickCount());
	ll n=rand()%100+1,m=rand()%100+1,t=gcd(n,m);
	n/=t;	m/=t;
	printf("%lld %lld\n",n,m);
}

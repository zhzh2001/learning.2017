#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll read(){	char ch=getchar();	ll x=0,f=1;	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	x=-x,putchar('-');	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=100000;
bool mark[N];
ll answ;
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;	}
int main(){
	freopen("math.in","r",stdin);
	freopen("baoli.out","w",stdout);
		ll n=read(),m=read();
			memset(mark,0,sizeof mark);
			mark[0]=1;
			For(i,0,n*m*10)	if (!mark[i])	answ=max(answ,i);
			else	mark[i+n]=mark[i+m]=1;
			printf("%lld	",answ);
}

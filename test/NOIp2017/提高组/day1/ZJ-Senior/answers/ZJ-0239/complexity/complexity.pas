var ch,s1,s2:string;
    cas,v,len,n1,n2,p,i,t,time,n,j:longint;

begin
 assign(input,'complexity.in'); reset(input);
 assign(output,'complexity.out'); rewrite(output);
 readln(cas);
 for v:=1 to cas do
 begin
  readln(ch);
  n:=0; len:=length(ch);
  i:=1;
  while (ch[i]>='0')and(ch[i]<='9') do
  begin
   n:=n*10+ord(ch[i])-ord('0');
   inc(i);
  end;
  if (ch[i+3]>='0')and(ch[i+3]<='9') then t:=0
   else
   begin
    p:=1; i:=i+5; t:=0;
    while (i<=len)and(ch[i]>='0')and(ch[i]<='9') do
    begin
     t:=t*10+ord(ch[i])-ord('0'); inc(i);
    end;
   end;
  time:=0;
  for i:=1 to n do
  begin
   readln(ch); len:=length(ch);
   if ch[1]='E' then continue;
   s1:=''; s2:='';
   j:=5;
   while (ch[j]='n')or((ch[j]>='0')and(ch[j]<='9')) do
   begin
    s1:=s1+ch[j]; inc(j);
   end;
   inc(j);
   while (j<=len)and((ch[j]='n')or((ch[j]>='0')and(ch[j]<='9'))) do
   begin
    s2:=s2+ch[j]; inc(j);
   end;
   if (s1='n')and(s2<>'n') then break;
   if (s1<>'n')and(s2='n') then begin time:=time+1; continue; end;
   if (s1='n')and(s2='n') then continue;
   if (s1<>'n')and(s2<>'n') then
   begin
    val(s1,n1);
    val(s2,n2);
    if n1>n2 then begin time:=t+10000; break; end;
   end;
  end;
  if time=t then writeln('Yes')
   else writeln('No');
 end;
 close(input);
 close(output);
end.

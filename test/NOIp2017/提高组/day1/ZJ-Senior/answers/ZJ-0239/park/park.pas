var q:array[1..2100000]of longint;
    inq:array[1..200000]of boolean;
    inq1:array[1..200000,0..50]of longint;
    dp:array[1..200000,0..50]of longint;
    head,vet,next,len:array[1..210000]of longint;
    dis,time:array[1..110000]of longint;
    n,m,k,i,v,cas,tot,mo,x,y,z,ans:longint;
    flag:boolean;

procedure add(a,b,c:longint);
begin
 inc(tot);
 next[tot]:=head[a];
 vet[tot]:=b;
 len[tot]:=c;
 head[a]:=tot;
end;

procedure spfa;
var t,w,t1,w1,i,j,u,e,v:longint;
begin
 t:=0; w:=1; q[1]:=1; t1:=0; w1:=1;
 for i:=1 to n do
 begin
  inq[i]:=false; dis[i]:=100000001;
 end;
 inq[1]:=true; dis[1]:=0;
 while t<w do
 begin
  inc(t); inc(t1);
  if t1=2000001 then t1:=1;
  u:=q[t1]; inq[u]:=false;
  e:=head[u];
  while e<>0 do
  begin
   v:=vet[e];
   if dis[u]+len[e]<dis[v] then
   begin
    dis[v]:=dis[u]+len[e];
    if not inq[v] then
    begin
     inc(w); inc(w1);
     if w1=2000001 then w1:=1;
     q[w1]:=v; inq[v]:=true;
    end;
   end;
   e:=next[e];
  end;
 end;

end;

procedure clac;
var t,w,i,j,p,u,e,v,s,w1,t1:longint;
begin
 flag:=true;
 for i:=1 to n do
  for j:=0 to k do
  begin
   dp[i,j]:=0; inq1[i,j]:=0;
  end;
 dp[1,0]:=1; inq1[1,0]:=1;
 for i:=0 to k do
 begin
  t:=0; w:=0;
  for j:=1 to n do
   if inq1[j,i]=1 then
   begin
    inc(w); q[w]:=j; inq[j]:=true; time[j]:=1;
   end
    else begin inq[j]:=false; time[j]:=0; end;
  t1:=t; if t1>=2000001 then t1:=t1-2000000;
  w1:=w; if w1>=2000001 then w1:=w1-2000000;
  while flag and(t<w) do
  begin
   inc(t); inc(t1);
   if t1=2000001 then t1:=1;
   u:=q[t1];
   inq[u]:=false;
   e:=head[u];
   while e<>0 do
   begin
    v:=vet[e];
    p:=dis[u]+i+len[e]-dis[v];
    if (dis[u]+i+len[e]<=dis[n]+k)and(p<=k) then
    begin
     inq1[v,p]:=1;
     dp[v,p]:=(dp[v,p]+dp[u,i]) mod mo;
     if (p=i)and(not inq[v]) then
     begin
      inc(w); inc(w1);
      if w1=2000001 then w1:=1;
      q[w1]:=v; inq[v]:=true;
      inc(time[v]);
      if time[v]>n*2+1 then
      begin
       flag:=false;
       break;
      end;
     end;
    end;
    e:=next[e];
   end;
  end;
 end;
end;

begin
 assign(input,'park.in'); reset(input);
 assign(output,'park.out'); rewrite(output);
 read(cas);
 for v:=1 to cas do
 begin
  read(n,m,k,mo);
  fillchar(head,sizeof(head),0);
  tot:=0;
  for i:=1 to m do
  begin
   read(x,y,z);
   add(x,y,z);
  end;
  spfa;
  clac;
  ans:=0;
  for i:=0 to k do ans:=(ans+dp[n,i]) mod mo;
  if not flag then writeln(-1)
   else writeln(ans);
 end;
 close(input);
 close(output);
end.

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
using namespace std;

void judge(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
}
#define ll long long
int t;
bool vis[30];
int mx(int a,int b){return a > b ? a : b;}
int main()
{
	judge();
	scanf("%d",&t);
	while(t--){
		memset(vis,0,sizeof(vis));
		//bool ck = false; // o(1) or o(n ^ ?)
		int x,tim,cnt = 0,anss = 0; //cnt  ck F <-> E
		scanf("%d",&x);
		rpt(i,1,3) getchar();
		char c = getchar();
		if (c != 'n') tim = 0;
		else{
			getchar();scanf("%d",&tim);
		}
		rpt(i,1,2) getchar();
		//cout << tim << endl;
		bool hsans = false;
		bool ss = false;
		int cplx = 0;
		rpt(i,1,x){
			char c = getchar();
			if (c == 'F'){
				cnt++;
				int fr,to;getchar();
				char d = getchar();getchar();
				//cout << d << endl;
				if (vis[d - 'a' + 1] && !hsans){
					puts("ERR");hsans = true;
				}
				else if (!vis[d - 'a' + 1]) vis[d - 'a' + 1] = true;
				char k = getchar();
				if (k == 'n'){fr = 105;getchar();}
				else{
					fr = k - '0';k = getchar();
					while(k >= '0' && k <= '9'){
						fr = fr * 10 + k - '0';
						k = getchar();
					}
				}
				char e = getchar();
				if (e == 'n'){to = 105;getchar();}
				else{
					to = e - '0';e = getchar();
					while(e >= '0' && e <= '9'){
						to = to * 10 + e - '0';
						e = getchar();
					}
				}
				if (fr == to) cplx = (cplx > 0) ? cplx : 0;
				else{
					if (fr > to && (!ss)){
						ss = true;cplx = (cplx > 0) ? cplx : 0;
					}
					else if (fr < to && to != 105) cplx = (cplx > 0) ? cplx : 0; 
					else if (fr < to && (!ss)) cplx++;
				}
				//cout << cplx << endl;
			}
			else{
				cnt--;anss = mx(cplx,anss);cplx--;getchar();
				if (cnt == 0){
					memset(vis,0,sizeof(vis));
					anss = mx(cplx,anss);cplx = 0;
				}
				else if (cnt < 0 && !hsans){
					puts("ERR");hsans = true;
				}
			}
		}
		if (cnt > 0 && !hsans){puts("ERR");continue;}
		if (anss == tim && !hsans){puts("Yes");continue;}
		if (anss != tim && !hsans){puts("No");continue;}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


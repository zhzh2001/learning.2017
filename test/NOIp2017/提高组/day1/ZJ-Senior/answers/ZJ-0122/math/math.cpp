#include<cstdio>
#include<algorithm>
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
using namespace std;

void judge(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
}
#define ll long long
ll a,b;

int main()
{
	judge();
	scanf("%lld%lld",&a,&b);
	printf("%lld\n",a * b - a - b);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

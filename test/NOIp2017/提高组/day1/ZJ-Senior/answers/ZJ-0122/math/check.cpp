#include<cstdio>
#include<algorithm>
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
using namespace std;

void judge(){
	freopen("","r",stdin);
	freopen("","w",stdout);
}
#define maxn 1000005
#define ll long long
ll a,b;
bool vis[maxn];
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.ans","w",stdout);
	scanf("%lld%lld",&a,&b);
	if (a > b) swap(a,b);
	rpt(i,1,maxn) 
		if (a * i > maxn) break;
		else
		{
			vis[a * i] = true;
			rpt(j,1,i - 1) if (a * (i - j) + b * j <= maxn) vis[a * (i - j) + b * j] =  true;
			if (b * i <= maxn) vis[b * i] = true;
		} 
	rpd(i,maxn,1) if (vis[i] == false){printf("%d",i);break;}
	return 0;
}


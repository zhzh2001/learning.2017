#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
using namespace std;

void judge(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
}
const int maxe = 200005;
const int maxn = 100005;
struct Graph{
	int cnt;
	int head[maxe],to[maxe],nxt[maxe],val[maxe];
	void addedge(int st,int ed,int v){
		to[++cnt] = ed;nxt[cnt] = head[st];val[cnt] = v;head[st] = cnt;
	}
}e;
bool tmr[1005];
bool cir[1005][1005];
bool vv[1005][1005];
int t,n,m,k,p,minn,ans,lim;
int dis[maxn];
bool vis[maxn];
void spfa(){
	queue<int>q;
	memset(dis,63,sizeof(dis));
	q.push(1);dis[1] = 0;vis[1] = true;
	while(!q.empty()){
		int t = q.front();q.pop();
		for (int i = e.head[t];i;i = e.nxt[i]){
			int d = e.to[i];
			if (dis[d] > dis[t] + e.val[i]){
				dis[d] = dis[t] + e.val[i];
				if (!vis[d]){
					vis[d] = true;q.push(d);
				}
			}
		}
		vis[t] = false;
	}
}

int calc(int x){
	return (x >= p) ? (x % p) : x;
}

void pre(){
	queue<int>q;
	int tim[maxn];
	memset(dis,0,sizeof(dis));
	q.push(1);vis[1] = true;dis[1] = 0;tim[1]= 1;
	while(!q.empty()){
		int t = q.front();q.pop();
		for (int i = e.head[t];i;i = e.nxt[i]){
			int d = e.to[i];
			if (tim[d] > 1 && dis[d] == dis[t] + e.val[i]){
				tmr[d] = true;break;
			}
			if (dis[d] > dis[t] + e.val[i]){
				dis[d] = dis[t] + e.val[i];
				if (!vis[d]){
					vis[d] = true;
					q.push(d);tim[d]++;
				}
			}
		}
		vis[t] = false;
	}
}

void sp(){
	ans = 0;lim = minn + k;
	queue<pair<int,pair<int,int> > >q;
	q.push(make_pair(1,make_pair(0,0)));
	while(!q.empty()){
		int k = q.front().second.first,t = q.front().first;
		int tt = q.front().second.second;
		q.pop();
		if (t == n && tt == 1){
			ans = -1;break;
		}
		else if (t == n && tt == 0) ans = calc(ans + 1);
		for (int i = e.head[t];i;i = e.nxt[i]){
			int v = e.to[i],s = 0;
			if (e.val[i] == 0 && cir[t][v]) s = 1;
			else if (tmr[v]) s = 1;
			if (e.val[i] + k <= lim) q.push(make_pair(v,make_pair(k + e.val[i],s))); 
		}
	}
}

int main()
{
	judge();
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		rpt(i,1,m){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			if (z == 0 && (!vv[x][y]) && (!vv[y][x])) vv[x][y] = true;
			else if (z == 0 && vv[y][x]){
				vv[x][y] = true;cir[x][y] = cir[y][x] = true;
			}
			e.addedge(x,y,z);
		}
		spfa();
		minn = dis[n];
		//pre();
		printf("%d\n",minn);
		sp();
		printf("%d\n",ans % p);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
2
5 7 2 10
1 2 1
2 4 0
4 5 2
2 3 2
3 4 1
3 5 2
1 5 3
2 2 0 10
1 2 0
2 1 0
*/

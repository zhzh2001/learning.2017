#include<bits/stdc++.h>
#define INF 2e9
#define maxn 100500
#define maxm 200500
using namespace std;
inline int read()
{
	int num=0;
	char c=' ';
	bool flag=true;
	for(;c>'9'||c<'0';c=getchar())
	if(c=='-')
	flag=false;
	for(;c>='0'&&c<='9';num=num*10+c-48,c=getchar());
	return flag ? num : -num;
}
int t,n,m,k,p,top=0;
int head[maxn];
struct node
{
	int y,val,nxt;
}a[maxm];
void init()
{
	top=0;
	memset(head,0,sizeof(head));
	n=read();
	m=read();
	k=read();
	p=read();
	for(int i=1;i<=m;i++)
	{
		int x=read();
		int y=read();
		int z=read();
		top++;
		a[top].nxt=head[x];
		a[top].y=y;
		a[top].val=z;
		head[x]=top;
	}
}
int dis[maxn];

void dijkstra(int u)
{
	memset(dis,10,sizeof(dis));
	dis[u]=0;
	
}
int ans=0,len=INF;
bool vis[50];
int now=0;
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	t=read();
	while(t--)
	{
		init();
		system("pause");
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

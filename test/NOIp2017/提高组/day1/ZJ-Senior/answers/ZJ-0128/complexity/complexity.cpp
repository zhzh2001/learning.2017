#include<bits/stdc++.h>
#define INF 2e9
using namespace std;
inline int read()
{
	int num=0;
	char c=' ';
	bool flag=true;
	for(;c>'9'||c<'0';c=getchar())
	if(c=='-')
	flag=false;
	for(;c>='0'&&c<='9';num=num*10+c-48,c=getchar());
	return flag ? num : -num;
}
int t,L,cnt=0,wait_=0,ans=-INF;
string s,s1;
bool is_ap[300],flag;
int sta[300],top=0;
int S[300],tot=0;
int c_f=0,c_e=0;
bool pau=false;
int ffh,ffe;
void work1()
{
	int num=0;
	bool fn;
	int bj=0;
	for(int i=1;i<s1.size();++i)
	{
		if(s1[i]=='n')
		{
			fn=true;
			break;
		}
		if(s1[i]>='0'&&s1[i]<='9')
		{
			fn=false;
			bj=i;
			break;
		}
	}
	for(int i=1;i<s1.size();++i)
	{
		
		if(s1[i]>='0'&&s1[i]<='9')
		num=num*10+s1[i]-48;
		if(s1[i]=='n')
		{
			cnt++;
			continue;	
		}
		if(s1[i]>='a'&&s1[i]<='z')
		if(!is_ap[s1[i]])
		{
			is_ap[s1[i]]=true;
			tot++;
			S[tot]=s1[i];
		}
		else
		{
			flag=true;
			return;	
		}
	}
	//统计n的个数。如果有0个或者2个n那么说明是O（1） 
	if(cnt==1&&!pau)
	{
		if(fn&&num<=100)cnt=0,pau=true,ffh=ffe=0;
		if(!fn&&num>100)cnt=0,pau=true,ffh=ffe=0;
	}
	if(!pau)
	{
		if(cnt==1)wait_++;
		ans=max(ans,wait_);
	}
		top++;
		sta[top]= cnt==1?'n':1;
}
void work2()
{
	if(!pau)
	{
		if(sta[top]=='n')
		wait_--;
	}
		top--;
		is_ap[S[tot]]=false;
		tot--;
}
bool check_(string s1,string s2)
{
	int bjz=0;
	for(int i=0;i<s1.size();i++)
	if(s1[i]=='(')
	{
		bjz=i;
		break;
	}
	int bjy=0;
	for(int i=s1.size()-1;i>=0;i--)
	if(s1[i]==')')
	{
		bjy=i;
		break;
	}
	bool fff=true;
	for(int i=bjz;i<=bjy;i++)
	if(s1[i]!=s2[i])
	{
		fff=false;
		break;
	}
	return fff;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=read();
	while(t--)
	{
		top=0;tot=0;
		flag=false;
		pau=false;
		c_f=c_e=0;
		ans=0;
		wait_=0;
		memset(is_ap,false,sizeof(is_ap));
		
		L=read();
		getline(cin,s);
		for(int i=1;i<=L;i++)
		{
			cnt=0;
			getline(cin,s1);
			if(s1[0]=='F')
			{
				work1(),c_f++;
				if(pau)
				ffh++;
			}
			if(s1[0]=='E')
			{
				work2(),c_e++;
				if(pau)
				ffe++;
			}
			if(c_f<c_e)flag=true;
			if(ffh==ffe)pau=false;
		}
		if(c_f!=c_e)flag=true;
		if(flag)
		{
			printf("ERR\n");
			continue;
		}
		string sa="";
		if(ans==0)sa="O(1)";
		else
		{
			sa="O(n^";
			sa=sa+(char)(ans+'0');
			sa=sa+")";
		}
		if(check_(sa,s))
		printf("Yes\n");
		else
		printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

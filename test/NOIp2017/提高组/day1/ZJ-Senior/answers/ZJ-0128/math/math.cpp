#include<bits/stdc++.h>
#define maxn 1000003
using namespace std;
inline int read()
{
	int num=0;
	char c=' ';
	bool flag=true;
	for(;c>'9'||c<'0';c=getchar())
	if(c=='-')
	flag=false;
	for(;c>='0'&&c<='9';num=num*10+c-48,c=getchar());
	return flag ? num : -num;
}
int a,b;
bool flag[maxn];
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();
	b=read();
	memset(flag,false,sizeof(flag));
	if(a>b)swap(a,b);
	for(int i=0;i<=10*b;i++)
	for(int j=0;j<=10*a;j++)
	{
		int num=i*b+j*a;
		if(num==0)
		{
			flag[0]=0;
			continue;
	}
		for(int k=1;num*k<=b*b;k++)
		flag[num*k]=true;
	}
	for(int i=b*b;i>=0;i--)
	if(!flag[i])
	{
		printf("%d\n",i);
		return 0;
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

inline int read()
{
	int ret=0; char c=getchar();
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int l1,l2,r1,r2,gg,uu[205],c[205],a[205],op[105],tot,fl,l,t,m,h,ma,d;
char dd[105],nn[105],k[105],s[105];

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d",&l);
		scanf("%s",s+1);
		d=strlen(s+1);
		if(d==4)m=0;
		else 
		{
			m=0;
		    for(int i=5; i<d; ++i)m=m*10+s[i]-48;
		}
		tot=0; h=0; fl=0; ma=0; gg=0;
		memset(a,0,sizeof(a));
		for(int i=1; i<=l; ++i)
		{
			scanf("%s",s);
			if(s[0]=='E')
			{
				if(fl)continue;
				if(tot)
				{
					if(uu[tot])
					{
						gg=0;
					}
					h-=op[tot];
					a[c[tot]]=0;
					--tot;
				}
				else fl=1;
				continue;
			}
			scanf("%s",k);
			scanf("%s",dd);
			scanf("%s",nn);
			if(fl)continue;
			if(a[k[0]-97])
			{
				fl=1;
				continue;
			}
			
			++tot;
			c[tot]=k[0]-97;
			a[k[0]-97]=1;
			uu[tot]=0;
			
			if(dd[0]=='n')
			{
				op[tot]=0;
				if(!gg)
				if(nn[0]!='n')
				{
					gg=1;
					uu[tot]=1;
				}
				if(h>ma)ma=h;
				continue;
			}
			
			if(nn[0]=='n')
			{
				if(gg)continue;
			    op[tot]=1; 
				++h;
				if(h>ma)ma=h;
				continue;
			}
			if(gg)
			{
				op[tot]=0;
				continue;
			}
			l1=strlen(dd);
			l2=strlen(nn);
			r1=0; r2=0;
			for(int j=0; j<l1; ++j)r1=r1*10+dd[j]-48;
			for(int j=0; j<l2; ++j)r2=r2*10+nn[j]-48;
			op[tot]=0;
			if(r1>r2)
			{
				uu[tot]=1;
				gg=1;
			}
		}
		
		if(fl || tot)printf("ERR\n");
		else if(ma==m)printf("Yes\n");
		else printf("No\n");
	}
}

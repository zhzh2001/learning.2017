#include<bits/stdc++.h>
#define N 100005
#define M 200005
using namespace std;

inline int read()
{
	int ret=0; char c=getchar();
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int ccc[M],T,u_[M],v_[N],w_[N],du[N],de[N],co,ti,dfn[N],low[N],tt,st[N],f[N],qu[N][51],D,qn,n,m,k,p,fl,tot,a[N],c[M][3],dis[N],op[N],faa[N],di[N][51],q[M],ne[M];

namespace wo
{
	void solve()
	{
		int u,v;
		memset(faa,0,sizeof(faa));
	    memset(dis,0x3f,sizeof(dis));
	    memset(op,0,sizeof(op));
	    dis[1]=0;
	    q[qn=1]=1; op[1]=1;
	    for(int i=1; i<=n+100; ++i)ne[i]=i+1; ne[n+101]=1;
	    for(int q1=1; q1!=ne[qn]; q1=ne[q1])
	    {
	    	u=q[q1]; op[u]=0;
	    	for(int o=a[u]; o; o=c[o][1])
	    	{
	    		v=c[o][0];
	    		if(dis[v]>dis[u]+c[o][2])
	    		{
	    			dis[v]=dis[u]+c[o][2];
	    			if(!op[v])
	    			{
	    				op[v]=1;
	    				qn=ne[qn];
	    				q[qn]=v;
					}
				}
			}
		}
		
		memset(op,0,sizeof(op));
		q[qn=1]=1; faa[1]=1; op[1]=1;
		for(int i=1; i<=n+100; ++i)ne[i]=i+1; ne[n+101]=1;
	    for(int q1=1; q1!=ne[qn]; ++q1)
	    {
	    	u=q[q1]; if(u==n)continue;
	    	op[u]=0;
	    	for(int o=a[u]; o; o=c[o][1])
	    	{
	    		v=c[o][0];
	    		if(dis[v]==dis[u]+c[o][2])
	    		{
	    		    faa[v]+=faa[u];
	    		    faa[v]=faa[v]%p;
	    		    if(!op[v])
					{
						op[v]=1;
						qn=ne[qn];
					    q[qn]=v;
					}
				}
			}
			faa[u]=0;
		}
		printf("%d\n",faa[n]);
	}
}

namespace cyb
{
	void df(int p, int s)
	{
		if(s>k)return;
		if(p==D && s)++qu[p][s];
		for(int o=a[p]; o; o=c[o][1])
		{
			if(f[c[o][0]]!=f[p])continue;
			if(!c[o][2])return;
			df(c[o][0],s+c[o][2]);
		}
	}
	
	void tarjan(int p)
	{
		int v;
		st[++tt]=p;
		dfn[p]=++ti;
		low[p]=ti+1;
		for(int o=a[p]; o; o=c[o][1])
		{
			v=c[o][0];
			if(dfn[v])low[p]=min(low[p],dfn[v]);
			else 
			{
				tarjan(v);
				if(low[v]==dfn[p]) 
				{
					ccc[o]=1;
				}
				low[p]=min(low[p],low[v]);
			}
		}
		if(dfn[p]<=low[p])
		{
			++co;
			while(st[tt]!=p)
			{
				f[st[tt]]=co;
				--tt;
			}
			--tt;
			f[p]=co;
		}
	}
	
	void dff(int p, int d)
	{
		if(de[p])return;
		de[p]=d;
		for(int o=a[p]; o; o=c[o][1])
		{
			dff(c[o][0],d+1);
		}
	}
	
	void solve()
	{
		int u,v,uu,ooo,ans;
		co=ti=tt=0;
	    memset(dis,0x3f,sizeof(dis));
	    memset(op,0,sizeof(op));
	    memset(ccc,0,sizeof(ccc));
	    dis[1]=0;
	    q[qn=1]=1; op[1]=1;
	    for(int i=1; i<=n+100; ++i)ne[i]=i+1; ne[n+101]=1;
	    for(int q1=1; q1!=ne[qn]; q1=ne[q1])
	    {
	    	u=q[q1]; op[u]=0;
	    	for(int o=a[u]; o; o=c[o][1])
	    	{
	    		v=c[o][0];
	    		if(dis[v]>dis[u]+c[o][2])
	    		{
	    			dis[v]=dis[u]+c[o][2];
	    			if(!op[v])
	    			{
	    				op[v]=1;
	    				qn=ne[qn];
	    				q[qn]=v;
					}
				}
			}
		}
		
		memset(qu,0,sizeof(qu));
		for(int i=1; i<=n; ++i)
		{
			D=i;
			df(i,0);
		}
		
		memset(de,0,sizeof(de));
		dff(1,1);
		memset(du,0,sizeof(du));
		for(int i=1; i<=m; ++i)if(!ccc[i])
		{
			if(de[u_[i]] && de[v_[i]])
			{
//				if(de[u_[i]]>de[v_[i]])continue;
				++du[v_[i]];
			}
		}
		
		memset(di,0,sizeof(di));
		di[1][0]=1;
		for(int i=1; i<=k; ++i)
		{
			if(uu=qu[1][i])
			{
				for(int j=k; j>=i; --j)
				{
				    di[1][j]+=(long long)di[1][j-i]*uu%p;
				    di[1][j]=di[1][j]%p;
				}
			}
		}
		
		q[qn=1]=1;
	    for(int q1=1; q1<=qn; ++q1)
	    {
	    	u=q[q1]; 
	    	for(int o=a[u]; o; o=c[o][1])
	    	{
	    		v=c[o][0];
//	    		if(de[u]>de[v])continue;
	    		if(ccc[o])continue;
	    		--du[v];
	    		ooo=dis[u]+c[o][2]-dis[v];
	    		for(int i=ooo; i<=k; ++i)
	    		{
	    		    di[v][i]+=di[u][i-ooo];
	    		    di[v][i]=di[v][i]%p;
				}
				
				if(!du[v])
				{
				q[++qn]=v;
				for(int i=1; i<=k; ++i)
		        {
			        if(uu=qu[v][i])
			        {
				        for(int j=k; j>=i; --j)
				        {
				            di[v][j]+=(long long)di[v][j-i]*uu%p;
				            di[v][j]=di[v][j]%p;
				        }
			        }
		        }
		        }
			}
		}
		ans=0;
		for(int i=0; i<=k; ++i)
			ans=(ans+di[n][i])%p;
		printf("%d\n",ans);
	}
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=read();
	while(T--)
	{
	    n=read(); m=read(); k=read(); p=read();
	    fl=0; tot=0; memset(a,0,sizeof(a)); 
	    for(int i=1; i<=m; ++i)
	    {
		    u_[i]=read(); v_[i]=read(); w_[i]=read();
		    c[++tot][0]=v_[i]; c[tot][1]=a[u_[i]]; a[u_[i]]=tot; c[tot][2]=w_[i];
	    }
	    if(!k)wo::solve();
	    else cyb::solve();
	}
}

#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define cl(x) memset(x,0,sizeof(x))
using namespace std;
typedef pair<int,int> abcd;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void read(int &x){
  char c=nc(),b=1;
  for (;!(c>='0' && c<='9');c=nc()) if (c=='-') b=-1;
  for (x=0;c>='0' && c<='9';x=x*10+c-'0',c=nc()); x*=b;
}

const int N=100005;
const int M=200005;
const int KK=56;

struct edge{
  int u,v,w,next;
}G[M];
int head[N],inum;
inline void add(int u,int v,int w,int p){
  G[p].u=u; G[p].v=v; G[p].w=w; G[p].next=head[u]; head[u]=p;
}

int n,m,K,P;
inline void add(int &x,int y){
  x+=y; if (x>=P) x-=P;
}

abcd dis[N][KK];
int flag[N];
#define V G[p].v

inline void Dij(){
  for (int i=1;i<=n;i++){ flag[i]=0; for (int k=1;k<=K;k++) dis[i][k]=abcd(1<<30,0); }
  dis[1][1]=abcd(0,1);
  for (int i=1;i<=n*K;i++){
    //if (i%100==0)
    //  fprintf(stderr,"%d\n",i);
    int mini=1<<30,u=0;
    for (int j=1;j<=n;j++)
      if (flag[j]+1<=K && dis[j][flag[j]+1].first<mini)
	mini=dis[j][flag[j]+1].first,u=j;
    if (u==0) break;
    flag[u]++;
    for (int p=head[u];p;p=G[p].next){
      int d=dis[u][flag[u]].first+G[p].w;
      int c=dis[u][flag[u]].second;
      int pos=K+1;
      for (int k=1;k<=K;k++)
	if (d<=dis[V][k].first){
	  pos=k; break;
	}
      if (pos==K+1) continue;
      if (dis[V][pos].first==d)
	add(dis[V][pos].second,c);
      else{
	for (int k=K;k>pos;k--)
	  dis[V][k]=dis[V][k-1];
	dis[V][pos]=abcd(d,c);
      }
    }
  }
}

inline void Solve(){
  int u,v,w,_K; cl(head); inum=0;
  read(n); read(m); read(K); _K=K; K++;read(P);
  for (int i=1;i<=m;i++) read(u),read(v),read(w),add(u,v,w,++inum);
  Dij();
  int ret=0,d=dis[n][1].first;
  for (int k=1;k<=K;k++)
    if (dis[n][k].first<=d+_K)
      add(ret,dis[n][k].second);
  printf("%d\n",ret);
}

int main(){
  int T;
  freopen("park.in","r",stdin);
  freopen("park.out","w",stdout);
  read(T);
  while (T--)
    Solve();
  return 0;
}

#include<cstdio>
#include<cstdlib>
#include<cstring>
#define cl(x) memset(x,0,sizeof(x))
#include<set>
#include<algorithm>
using namespace std;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void read(int &x){
  char c=nc(),b=1;
  for (;!(c>='0' & c<='9');c=nc()) if (c=='-') b=-1; else if (c=='n') { x=1<<30; return; }
  for (x=0;c>='0' && c<='9';x=x*10+c-'0',c=nc()); 
}
inline void read(char *s){
  char c=nc(); int len=0;
  for (;c!='O';c=nc());
  for (;;c=nc()) {
    s[++len]=c;
    if (c==')') break;
  }
  s[++len]=0;
}
inline void read(char &x){
  for (x=nc();!((x>='A' && x<='Z') || (x>='a' && x<='z'));x=nc());
}

const int N=1005;

bool Name[26];
int L,W; char com[N];
int cur[N],top,sta[N]; char nam[N];

inline int pj(int l,int r){
  if (l>r)
    return -1e5;
  else if (l==1<<30 && r==1<<30)
    return 0;
  else if (l!=1<<30 && r==1<<30)
    return 1;
  else if (l!=1<<30 && r!=1<<30)
    return 0;
}

inline void Solve(){
  read(L); read(com); cl(Name); top=0; cl(cur); cl(sta); cl(nam);
  if (!strcmp(com+1,"O(1)"))
    W=0;
  else
    sscanf(com+1,"O(n^%d)",&W);
  char order; int l,r;
  int ret=0;
  while (L--){
    read(order);
    if (order=='E'){
      if (ret==-1) continue;
      if (!top){ ret=-1; continue; }
      cur[top-1]=max(cur[top-1],cur[top]+sta[top-1]);
      Name[nam[top]-'a']=0; top--;
    }else{
      if (ret==-1){
	char t; int x;
	read(t); read(x); read(x);
	continue;
      }
      top++;
      read(nam[top]);
      if (Name[nam[top]-'a']){ ret=-1; continue; }
      Name[nam[top]-'a']=1;
      read(l); read(r);
      cur[top]=max(0,sta[top]=pj(l,r));
    }
  }
  if (top) ret=-1;
  if (ret==-1)
    printf("ERR\n");
  else{
    ret=cur[0];
    if (ret==W)
      printf("Yes\n");
    else
      printf("No\n");
  }
}

int main(){
  int T;
  freopen("complexity.in","r",stdin);
  freopen("complexity.out","w",stdout);
  read(T);
  while (T--)
    Solve();
  return 0;
}

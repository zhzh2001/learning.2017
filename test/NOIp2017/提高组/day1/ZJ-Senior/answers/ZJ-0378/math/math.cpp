#include<cstdio>
#include<cstdlib>
#include<cassert>
#include<algorithm>
using namespace std;
typedef long long ll;

#define read(x) scanf("%d",&(x))

int main(){
  int x,y;
  freopen("math.in","r",stdin);
  freopen("math.out","w",stdout);
  read(x); read(y);
  printf("%lld\n",(ll)x*y-x-y);
  return 0;
}

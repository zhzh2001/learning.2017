#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
int t,n,m,h,p,a[1010][1010],b[10000],x,y,z,d[10000],f[10000];
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d%d",&n,&m,&h,&p);
		memset(a,2147483647,sizeof(a));
		memset(b,0,sizeof(b));
		memset(f,0,sizeof(f));
		for (int i=1;i<=m;i++){
			scanf("%d%d%d",&x,&y,&z);
			a[x][y]=z;
		}
		for (int i=1;i<=n;i++){
			d[i]=a[1][i];
		}b[1]=1;
		for (int i=1;i<=n;i++){
			int mi=2147483647,k;
			for (int j=1;j<=n;j++){
				if (!b[j]&&d[j]<mi){
					k=j;mi=d[j];
				}
			}
			b[k]=1;
			for (int j=1;j<=n;j++){
				if (!b[j]&&d[j]>d[k]+a[k][j]) d[j]=d[k]+a[k][j];
				if (d[j]>d[k]+a[k][j]+h) f[j]++;
			}
		}
		if (d[n]==2147483647) printf("-1\n");
		else{
			if (f[1]==0) printf("-1");
			else
			printf("%d\n",f[1]);
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

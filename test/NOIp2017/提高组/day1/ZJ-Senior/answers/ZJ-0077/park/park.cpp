#include <cstdio>
#include <queue>
#include <vector>
#include <cstring>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

int T;
int edgenum;
int vet[300000];
int val[300000];
int nextx[300000];
int head[200000];
std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int> >, std::greater<std::pair<int, int> > > que;
std::queue<int> q;
std::queue<std::pair<int, int> > kkk;
int times;
int dfn[200000];
int low[200000];
int instack[200000];
int top;
int stack[200000];
int knumber;
int knum[200000];
std::vector<int> vec[200000];
int ans[200000][55];
int tmp[200000][55];
int n, m, K, M;
int in[200000];
int anss;
bool flag[200000];
const int INF = 1000000000;
int dis[200000];

void add(int u, int v, int cost)
{
	edgenum++;
	vet[edgenum] = v;
	val[edgenum] = cost;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

void Tarjan(int u, int father)
{
	times++;
	dfn[u] = times;
	low[u] = times;
	instack[u] = 1;
	top++;
	stack[top] = u;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (instack[v] == 0)
		{
			Tarjan(v, u);
			low[u] = std::min(low[u], low[v]);
		}
		else
		{
			if (instack[v] == 1)
			{
				low[u] = std::min(low[u], dfn[v]);
			}
		}
	}
	if (dfn[u] == low[u])
	{
		knumber++;
		while (stack[top] != u)
		{
			int v = stack[top];
			instack[v] = 2;
			knum[v] = knumber;
			vec[knumber].push_back(v);
			top--;
		}
		instack[u] = 2;
		knum[u] = knumber;
		vec[knumber].push_back(u);
		top--;
	}
}

void solve(int num)
{
	for (int i = 0; i < (int)vec[num].size(); i++)
	{
		if (vec[num][i] == 1)
		{
			ans[vec[num][i]][0] = 1;
		}
	}
	for (int i = 0; i < (int)vec[num].size(); i++)
	{
		int s = vec[num][i];
		kkk.push(std::make_pair(vec[num][i], 0));
		while (! kkk.empty())
		{
			int u = kkk.front().first;
			int ddis = kkk.front().second;
			kkk.pop();
			if (ddis)
			{
				for (int j = 0; j <= K && dis[s] + j + ddis <= dis[u] + K; j++)
				{
					if (dis[s] + j + ddis - dis[u] >= 0)
					{
						(tmp[u][dis[s] + j + ddis - dis[u]] += ans[s][j]) %= M;
					}
				}
			}
			for (int e = head[u]; e; e = nextx[e])
			{
				int v = vet[e];
				int cost = val[e];
				if (knum[v] == num && ddis + cost <= K)
				{
					kkk.push(std::make_pair(v, ddis + cost));
				}
			}
		}
	}
	for (int i = 0; i < (int)vec[num].size(); i++)
	{
		for (int j = 0; j <= K; j++)
		{
			/*if (vec[num][i] == 1)
			{
				printf("X%d %d\n", ans[vec[num][i]][j], tmp[vec[num][i]][j]);
			}*/
			(ans[vec[num][i]][j] += tmp[vec[num][i]][j]) %= M;
		}
	}
}

void kk(int num)
{
	for (int i = 0; i < (int)vec[num].size(); i++)
	{
		int u = vec[num][i];
		for (int e = head[u]; e; e = nextx[e])
		{
			int v = vet[e];
			int cost = val[e];
			if (knum[v] != num)
			{
				for (int j = 0; j <= K && dis[u] + j + cost <= dis[v] + K; j++)
				{
					if (dis[u] + j + cost - dis[v] >= 0)
					{
						//printf("X%d %d %d %d %d\n", u, j, cost, v, ans[u][j]);
						(ans[v][dis[u] + j + cost - dis[v]] += ans[u][j]) %= M;
					}
				}
				in[knum[v]]--;
				if (in[knum[v]] == 0)
				{
					q.push(knum[v]);
				}
			}
		}
	}
}

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	T = read();
	knumber = 0;
	while (T--)
	{
		for (int i = 1; i <= knumber; i++)
		{
			vec[i].clear();
		}
		knumber = 0;
		edgenum = 0;
		times = 0;
		memset(head, 0, sizeof(head));
		memset(nextx, 0, sizeof(nextx));
		memset(ans, 0, sizeof(ans));
		memset(tmp, 0, sizeof(tmp));
		memset(dfn, 0, sizeof(dfn));
		memset(low, 0, sizeof(low));
		memset(knum, 0, sizeof(knum));
		memset(in, 0, sizeof(in));
		n = read();
		m = read();
		K = read();
		M = read();
		for (int i = 1; i <= m; i++)
		{
			int u, v, cost;
			u = read();
			v = read();
			cost = read();
			add(u, v, cost);
		}
		for (int i = 1; i <= n; i++)
		{
			dis[i] = INF;
			flag[i] = false;
		}
		dis[1] = 0;
		que.push(std::make_pair(0, 1));
		while (! que.empty())
		{
			int u = que.top().second;
			que.pop();
			if (flag[u])
			{
				continue;
			}
			flag[u] = true;
			for (int i = head[u]; i; i = nextx[i])
			{
				int v = vet[i];
				int cost = val[i];
				if (dis[u] + cost < dis[v])
				{
					dis[v] = dis[u] + cost;
					que.push(std::make_pair(dis[v], v));
				}
			}
		}
		for (int i = 1; i <= n; i++)
		{
			instack[i] = 0;
		}
		Tarjan(1, 0);
		for (int u = 1; u <= n; u++)
		{
			for (int i = head[u]; i; i = nextx[i])
			{
				int v = vet[i];
				if (knum[u] != knum[v])
				{
					in[knum[v]]++;
				}
			}
		}
		for (int i = 1; i <= knumber; i++)
		{
			if (! in[i])
			{
				q.push(i);
			}
		}
		while (! q.empty())
		{
			int u = q.front();
			q.pop();
			solve(u);
			kk(u);
		}
		/*for (int i = 1; i <= n; i++)
		{
			for (int j = 0; j <= K; j++)
			{
				printf("%d ", ans[i][j]);
			}
			puts("");
		}*/
		anss = 0;
		for (int i = 0; i <= K; i++)
		{
			anss = (anss + ans[n][i]) % M;
		}
		printf("%d\n", anss);
	}
	return 0;
}

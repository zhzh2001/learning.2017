#include <cstdio>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

long long x, y;

int main()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	x = read();
	y = read();
	printf("%lld\n", x * y - x - y);
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

int T;
int n;
char st[200][10];
char alpha[200][10];
char left[200][10];
char right[200][10];
char times[200];
bool f[100];
int top;
int stack[200];
int anstop;
int ans[200];
int pos[200];
int anss;

bool check()
{
	top = 0;
	memset(f, 0, sizeof(f));
	for (int i = 1; i <= n; i++)
	{
		if (st[i][1] == 'F')
		{
			top++;
			stack[top] = i;
			if (f[alpha[i][1] - 'a'])
			{
				return false;
			}
			f[alpha[i][1] - 'a'] = true;
		}
		else
		{
			if (top == 0)
			{
				return false;
			}
			f[alpha[stack[top]][1] - 'a'] = false;
			top--;
		}
	}
	if (top)
	{
		return false;
	}
	return true;
}

int query(char *x)
{
	if (x[1] == 'n')
	{
		return 1;
	}
	return 0;
}

int change(char *x)
{
	int tmp = 0;
	int n = strlen(x + 1);
	for (int i = 1; i <= n; i++)
	{
		tmp = tmp * 10 + x[i] - 48;
	}
	return tmp;
}

bool dif(char *x, char *y)
{
	int xx = change(x);
	int yy = change(y);
	return xx <= yy;
}

int calc(char *x, char *y)
{
	int xx = query(x);
	int yy = query(y);
	if (xx == 0 && yy == 1)
	{
		return 1;
	}
	if (xx == 1 && yy == 0)
	{
		return -1;
	}
	if (xx == 1 && yy == 1)
	{
		return 0;
	}
	if (dif(x, y))
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

int mul(int x, int y)
{
	if (x == -1 || y == -1)
	{
		return -1;
	}
	return x + y;
}

int find()
{
	if (times[3] == '1')
	{
		return 0;
	}
	int tmp = 0;
	int n = strlen(times + 1);
	for (int i = 1; i <= n; i++)
	{
		if (times[i] >= '0' && times[i] <= '9')
		{
			tmp = tmp * 10 + times[i] - 48;
		}
	}
	return tmp;
}

void print(int x)
{
	int tmp = find();
	x = std::max(x, 0);
	if (tmp == x)
	{
		puts("Yes");
	}
	else
	{
		puts("No");
	}
}

int main()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	T = read();
	while (T--)
	{
		n = read();
		scanf("%s", times + 1);
		for (int i = 1; i <= n; i++)
		{
			scanf("%s", st[i] + 1);
			if (st[i][1] == 'F')
			{
				scanf("%s", alpha[i] + 1);
				scanf("%s", left[i] + 1);
				scanf("%s", right[i] + 1);
			}
		}
		if (! check())
		{
			printf("ERR\n");
			continue;
		}
		top = 0;
		anstop = 0;
		for (int i = 1; i <= n; i++)
		{
			if (st[i][1] == 'F')
			{
				anstop++;
				ans[anstop] = calc(left[i], right[i]);
				pos[anstop] = i;
				top++;
				stack[top] = i;
			}
			else
			{
				int tmp = 0;
				while (anstop && pos[anstop] != stack[top])
				{
					tmp = std::max(tmp, ans[anstop]);
					anstop--;
				}
				ans[anstop] = mul(ans[anstop], tmp);
				top--;
			}
		}
		anss = -1;
		for (int i = 1; i <= anstop; i++)
		{
			anss = std::max(anss, ans[i]);
		}
		print(anss);
	}
	return 0;
}

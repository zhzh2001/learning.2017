#include<cstdio>
typedef long long ll;
ll myswap(ll &x,ll &y) {ll t=x;x=y;y=t;}
ll exgcd(ll a,ll b,ll &x,ll &y)
{
	if(!b)
	{
		x=1;y=0;
		return a;
	}
	ll ret=exgcd(b,a%b,x,y);
	ll t=x;
	x=y;
	y=t-a/b*y;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll a,b,x,y;scanf("%lld%lld",&a,&b);
	ll r=exgcd(a,b,x,y);
	if(y<0) {myswap(x,y);myswap(a,b);}
	ll k=-x/b+(-x%b==0?0:1);
	ll xx=k*x+b,yy=k*a-y;
	x=b-xx;y=a-yy;
	yy=yy/y+(yy%y==0?0:1);
	printf("%lld\n",yy*x*a-1);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<string>
#include<algorithm>
#define INF 10000000
using namespace std;
string s;
void read(int &x,string ss,int &pos)
{
	if(ss[pos]=='n')
	{
		x=INF;
		pos+=2;
		return;
	}
	int ret=0,i;
	for(i=pos;i<ss.length();i++)
	{
		if(ss[i]>'9'||ss[i]<'0') break;
		ret=10*ret+ss[i]-'0';
	}
	x=ret;pos=i+1;
}
int work(int &len)
{
	if(!len) return 0;
	len--;int ret=0;
	getline(cin,s);
//	cout<<len<<" "<<s<<endl;
	while(len)
	{
		if(s[0]=='E') return ret;
		int i,x,y,p=4;
		read(x,s,p);read(y,s,p);
		int r=work(len);
		if(y==INF&&x!=INF) r++;
		if(x<=y) ret=max(ret,r);
		if(!len) break;
		getline(cin,s);len--;
	}
	return ret;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	ios::sync_with_stdio(false);
	string comp;getline(cin,comp);
	int t,l,pp=0;read(t,comp,pp);
	for(;t--;)
	{
		getline(cin,comp);pp=0;read(l,comp,pp);pp+=4;
		if(l&1)
		{
			cout<<"ERR"<<endl;
			string str;for(;l--;) getline(cin,str);
			continue;
		}
		int ans=work(l),want;
		if(comp.length()-pp==4) want=0;
		else read(want,comp,pp);
		if(ans==want) cout<<"Yes"<<endl;
		else cout<<"No"<<endl;
	}
	return 0;
}

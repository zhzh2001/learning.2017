#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
using namespace std;
struct edge{
	int to,nxt,val;
}e[200005];
int first[100005],dis[100005],vis[100005],f[100005][55],tot=0;
queue<int> q;
void addedge(int u,int v,int w)
{
	e[++tot].val=w;
	e[tot].to=v;
	e[tot].nxt=first[u];
	first[u]=tot;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T,n,m,k,mod;
	scanf("%d",&T);
	for(;T--;)
	{
		scanf("%d%d%d%d",&n,&m,&k,&mod);
		for(;m--;)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			addedge(x,y,z);
		}
		memset(dis,0x3f,sizeof(dis));
		memset(f,0,sizeof(f));
		dis[1]=0;vis[1]=1;f[1][0]=1;q.push(1);
		while(q.size())
		{
			int x=q.front();q.pop();
			vis[x]=0;
			for(int i=first[x];i;i=e[i].nxt)
			{
				int v=e[i].to;
				if(dis[v]==dis[x]+e[i].val) f[v][0]+=f[x][0],f[v][0]%=mod;
				if(dis[v]>dis[x]+e[i].val)
				{
					dis[v]=dis[x]+e[i].val;
					f[v][0]=f[x][0];
					if(!vis[v]) {vis[v]=1;q.push(v);}
				}
			}
		}
		if(k==0) {printf("%d\n",f[n][0]);continue;}
		q.push(1);vis[1]=1;
		while(q.size())
		{
			int x=q.front();q.pop();
			vis[x]=0;
			for(int i=first[x];i;i=e[i].nxt)
			{
				int v=e[i].to;
				for(int j=max(0,e[i].val-dis[v]);j+e[i].val-dis[v]<=k;j++)
				{
					if(!vis[v]) {vis[v]=1;q.push(v);}
					f[v][j+e[i].val-dis[v]]+=f[x][j+dis[x]-dis[v]];
					f[v][j+e[i].val-dis[v]]%=mod;
				}
			}                                     
		}
		int ans=0;
		for(int i=0;i<=k;i++) ans+=f[n][i],ans%=mod;
		printf("%d\n",ans);
	}
	return 0; 
}                       
	

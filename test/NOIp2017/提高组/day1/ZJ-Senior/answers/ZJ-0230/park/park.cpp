#include<cstdio>
#include<cmath>
#include<cstring>
#include<cstdlib>
#include<cctype>
#include<algorithm>
#include<set>
#include<ext/pb_ds/priority_queue.hpp>
using namespace std;
#define D isdigit(c=getchar())
inline int getint(){
	int x,c;
	for(;!D;);
	for(x=0;x=x*10+c-'0',D;);
	return x;
}
struct node{
	int x,i;
	node(int x=0,int i=0):x(x),i(i){}
	bool operator < (const node& a)const{return x>a.x;}
};
const int N=123424;
const int M=221233;
typedef __gnu_pbds::priority_queue<node> PQ;
PQ pq;
PQ::point_iterator it[N];
int G[N],ne[M],to[M],xb,d[N],da[M],n,m,k,p,T,ans;
int G2[N],ne2[M],to2[M],d2[N];
int dp[52][N],xb2,xb3;
int G3[N],ne3[M],to3[M],da3[M];
int G4[N],ne4[M],to4[M],xb4;
struct data{
	int i,ds,ck;
	data(int i=0,int ds=0,int ck=0):i(i),ds(ds),ck(ck){}
	bool operator < (const data& a)const{
		if(ds!=a.ds)return ds<a.ds;
		if(ck!=a.ck)return ck<a.ck;
		return i<a.i; 
	}
}dp_xu[N];
int nx[N];
int dfn[N],st[N],tp,clk,ins[N],scc_cnt,sccno[N];
int sz[N];
int dfs(int u){
	int lowu=dfn[u]=++clk;st[++tp]=u;ins[u]=1;
	for(int i=G2[u];~i;i=ne2[i]){
		int v=to2[i];
		if(!dfn[v]){
			int lowv=dfs(v);
			lowu=min(lowu,lowv);
		}else if(ins[v])lowu=min(lowu,dfn[v]);
	}
	if(lowu==dfn[u]){
		scc_cnt++;
		for(;;){
			int x=st[tp--];
			sccno[x]=scc_cnt;ins[x]=0;sz[scc_cnt]++;
			if(x==u)break;
		}
	}
	return lowu;
}
set<int> sat[N];
int du[N];
int topo[N];
int qu[N];
void bd(){
	for(int i=1;i<=n;i++)sat[i].clear();
	memset(G4,-1,sizeof(G4));xb4=0;
	memset(du,0,sizeof(du));
	for(int i=1;i<=n;i++)
		for(int j=G2[i];~j;j=ne2[j]){
			int x=sccno[i],y=sccno[to2[j]];
			if(x!=y&&!sat[x].count(y)){
				sat[x].insert(y);
				ne4[xb4]=G4[x];to4[xb4]=y;G4[x]=xb4++;
				du[y]++;
			}
		}
	int tal=0;
	for(int i=1;i<=scc_cnt;i++)if(!du[i])qu[++tal]=i;
	for(int i=1;i<=tal;i++){
		int x=qu[i];topo[x]=i;
		for(int j=G4[x];~j;j=ne4[j]){
			du[to4[j]]--;
			if(!du[to4[j]])qu[++tal]=to4[j];
		}
	}
}
bool ak_0(){
	clk=0;scc_cnt=0;memset(ins,0,sizeof(ins));
	memset(sz,0,sizeof(sz));memset(dfn,0,sizeof(dfn));
	for(int i=1;i<=n;i++)if(!dfn[i])dfs(i);
	bd();
	for(int i=1;i<=n;i++)if(sz[sccno[i]]>1&&d[i]+d2[i]<=d[n]+k)return 1;
	return 0;
}
void dijk(int s,int*d,int*G,int*ne,int*to,int*da){
	memset(d,0x3f,N*sizeof(int));
	pq.push(node(0,s));d[s]=0;
	while(!pq.empty()){
		node x=pq.top();pq.pop();
		for(int i=G[x.i];~i;i=ne[i])
			if(d[to[i]]>x.x+da[i]){
				if(d[to[i]]==0x3f3f3f3f)it[to[i]]=pq.push(node(d[to[i]]=x.x+da[i],to[i]));
				else pq.modify(it[to[i]],node(d[to[i]]=x.x+da[i],to[i]));
			}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	T=getint();
	while(T--){
		n=getint();m=getint();k=getint();p=getint();
		memset(G,-1,sizeof(G));xb=0;xb2=0;xb3=0;
		memset(G2,-1,sizeof(G2));memset(G3,-1,sizeof(G3));
		for(int i=1;i<=m;i++){
			int x=getint();
			int y=getint();
			int z=getint();
			ne[xb]=G[x];to[xb]=y;da[xb]=z;G[x]=xb++;
			ne3[xb3]=G3[y];to3[xb3]=x;da3[xb3]=z;G3[y]=xb3++;
			if(z==0){
				ne2[xb2]=G2[x];to2[xb2]=y;G2[x]=xb2++;
			}
		}
		dijk(1,d,G,ne,to,da);
		dijk(n,d2,G3,ne3,to3,da3);
		if(ak_0()){
			puts("-1");
			continue;
		}
		for(int i=1;i<=n;i++)dp_xu[i]=data(i,d[i],topo[sccno[i]]);
		sort(dp_xu+1,dp_xu+n+1);
		for(int i=1;i<=n;i++)nx[dp_xu[i].i]=i;
		ans=0;
		memset(dp,0,sizeof(dp));
		dp[0][nx[1]]=1;
		for(int dt=0;dt<=k;dt++){
			int*fg=dp[dt];
			for(int i=1;i<=n;i++){
				int x=dp_xu[i].i;
				for(register int j=G[x];~j;j=ne[j]){
					register int amg=d[x]+dt+da[j]-d[to[j]];
					amg<=k?((dp[amg][nx[to[j]]]+=fg[i])>=p?dp[amg][nx[to[j]]]-=p:0):0;
				}
			}
			ans=(ans+dp[dt][nx[n]])%p;
		}
		printf("%d\n",ans);
	}
}


#include<cstdio>
#include<cmath>
#include<cstring>
#include<cctype>
#include<algorithm>
#include<vector>
using namespace std;
int T,L,fz;
bool fg,fg2;
int ctk,mx;
bool used[100];
int fzd[10000],sy[10000];
char s[100],s1[100],s2[100],s3[100];
int getnm(char *s){
	int x=0;
	for(;isdigit(*s);s++)x=x*10+(*s)-'0';
	return x;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
    scanf("%d",&T);
    while(T--){
    	scanf("%d%s",&L,s);
    	if(s[2]=='1')fz=0;
    	else{
    		fz=s[4]-'0';
    		if(isdigit(s[5]))fz=fz*10+s[5]-'0';
    		if(isdigit(s[6]))fz=fz*10+s[6]-'0';
		}
		fg=0;ctk=0;mx=0;
		memset(fzd,0,sizeof(fzd));fzd[0]=0;
		memset(used,0,sizeof(used));
		for(int i=1;i<=L;i++){
			scanf("%s",s);
			if(s[0]=='F'){
				ctk++;
				scanf("%s%s%s",s1,s2,s3);
				if(used[s1[0]-'a'])fg=1;
				used[s1[0]-'a']=1;sy[ctk]=s1[0]-'a';
				if(s2[0]=='n'){
					if(s3[0]=='n')fzd[ctk]=fzd[ctk-1];
					else fzd[ctk]=-100000000;
				}else{
					if(s3[0]=='n')fzd[ctk]=fzd[ctk-1]+1;
					else{
						int a=getnm(s2),b=getnm(s3);
						if(a<=b)fzd[ctk]=fzd[ctk-1];
						else fzd[ctk]=-10000000;
					}
				}
			}else{
				used[sy[ctk]]=0;
				ctk--;
				if(ctk<0)fg=1,ctk=0;
			}
			mx=max(mx,fzd[ctk]);
		}
		if(ctk)fg=1;
		if(fg)puts("ERR");
		else{
			if(fz==mx)puts("Yes");else puts("No");
		}
	}
}


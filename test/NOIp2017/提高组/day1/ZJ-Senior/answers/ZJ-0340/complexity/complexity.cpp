#include <cstdio>
#define nc (c=getchar())
using namespace std;
bool in[30];
bool noent[105];
int  st[105];
int t,fo,mi;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		int ma=0,ans=0;
		bool f9=1;
		for (int i=0;i<=30;i++) in[i]=0;
		for (int i=0;i<=101;i++) noent[i]=0;
		bool isn=true;
		char c;nc;
		while (c<'0'||c>'9') nc;
		int l=c-48;nc;
		while (c>='0'&&c<='9') l=(l<<3)+(l<<1)+c-48,nc;
		while (c!='(') nc;nc;
		if (c=='1') isn=0;
		else
		{
			while (c<'0'||c>'9') nc;
			ans=c-'0';nc;
			while (c>='0'&&c<='9') ans=(ans<<3)+(ans<<1)+c-48,nc;
		}
		fo=0,mi=0;bool us=1;
		for (int i=1;i<=l;i++)
		{
			while (c!='F'&&c!='E') nc;
			if (c=='F')
			{
				if (fo==0) mi=0,us=1;
				int x=0,y=0;
				fo++;
				while (c<'a'||c>'z') nc;st[fo]=c-'a';
				if (in[c-'a']) f9=0;
				else in[c-'a']=1;
				while ((c<'0'||c>'9')&&c!='n') nc;
				if (c=='n') x=0;
				else x=c-'0';nc;
				while (c>='0'&&c<='9') x=(x<<3)+(x<<1)+c-48,nc;
				while ((c<'0'||c>'9')&&c!='n') nc;
				if (c=='n') y=0;
				else y=c-'0';nc;
				while (c>='0'&&c<='9') y=(y<<3)+(y<<1)+c-48,nc;
				if (y==0) 
				{
					if (x!=0) mi++;
				}
				else 
				{
					if (x==0) us=0,noent[fo]=1;
					else if (x>y) us=0,noent[fo]=1;
				}
				if (mi>ma&&us) ma=mi;
			}
			else 
			{
				int tmp=st[fo];
				in[tmp]=0;
				if (noent[fo]==1) us=1,noent[fo]=0;
				fo--;nc;
				if (fo<0) f9=0;
			}
		}
		if (fo>0) f9=0;
		if (!f9) printf("ERR\n");
		else
		{
			if (ma==ans) printf("Yes\n");
			else printf("No\n");
		}
	}
}

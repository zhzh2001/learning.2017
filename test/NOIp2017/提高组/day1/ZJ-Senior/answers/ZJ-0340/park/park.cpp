//orz ak ye zH
#include <cstdio>
#include <queue>
#define nc (c=getchar())
using namespace std;
inline void read(int &a)
{
	int f=1;a=0;char c;nc;
	while (c<'0'||c>'9') {if (c=='-') f=-1;nc;}
	while (c>='0'&&c<='9') a=(a<<3)+(a<<1)+c-48,nc;
	a*=f;
}
const int N=1e5+5,inf=1e9+5;
struct node {
	int nxt,to,w;
}e[N<<2];
struct dui{
	int x,y;
}a[N];
int map[1005][1005];
int head[N],tot;
inline void add(int u,int v,int w){e[++tot].nxt=head[u],e[tot].to=v,e[tot].w=w,head[u]=tot;}
int dis[N];
bool vis[N];
int T;
int n,k,m,p;
void spfa(int s)
{
	queue<int> q;
	while (!q.empty()) q.pop();
	for (int i=1;i<=n;i++) dis[i]=inf;dis[s]=0;
	q.push(s);vis[s]=1;
	while (!q.empty())
	{
		int x=q.front();
		q.pop();vis[x]=0;
		for (int i=head[x];i;i=e[i].nxt)
		{
			int to=e[i].to;
			if (e[i].w+dis[x]<dis[to])
			{
				dis[to]=e[i].w+dis[x];
				if (!vis[to]) q.push(to);
			}
		}
	}
	for (int i=1;i<=n;i++) map[s][i]=dis[i];
}
int ans,cnt,dd;
bool zer=0;
void dfs(int x,int w)
{
	if (w>dd+k) return ;
	if (x==n)
	{
		ans++;
		ans%=p;
	}
	for (int i=head[x];i;i=e[i].nxt)
	{
		dfs(e[i].to,w+e[i].w);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T,i,l,j;
	read(T);
	while (T--)
	{
		zer=0;tot=0;
		read(n),read(m),read(k),read(p);
		for (i=1;i<=n;i++) 
		{
			vis[i]=0,head[i]=0;
			for (j=1;j<=n;j++)
			{
				map[i][j]=inf;
			}
		}
		for (i=1;i<=m;i++)
		{
			int u,v,w;
			read(u),read(v),read(w);
			add(u,v,w);
			map[u][v]=w;
		}
		for (i=1;i<=n;i++)
		{
			for (j=i+1;j<=n;j++)
			{
				if (map[i][j]==map[j][i]&&!map[i][j])
				{
					a[++cnt].x=i,a[++cnt].y=j;
				}
			}
		}
		spfa(1);dd=dis[n];
		for (i=1;i<=cnt&&zer!=1;i++)
		{
			if (map[1][a[i].x]>map[1][n]+k) continue;
			else
			{
				spfa(a[i].x);
				if (map[1][a[i].x]+map[a[i].x][n]>map[1][n]+k) continue;
				else zer=1;
			}
		}
		if (!zer&&dd!=inf) dfs(1,0);
		if (zer) printf("-1\n");
		else printf("%d\n",ans);
	}
}

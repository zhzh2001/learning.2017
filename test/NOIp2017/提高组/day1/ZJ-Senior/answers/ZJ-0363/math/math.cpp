#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;

ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

ll n,m;

int main(){
	// say hello

	freopen("math.in","r",stdin);
 	freopen("math.out","w",stdout);
	
	n=read(),m=read();
	ll ans=n*m-n-m;
	printf("%lld\n",ans);


	return 0;

	// say goodbye
}


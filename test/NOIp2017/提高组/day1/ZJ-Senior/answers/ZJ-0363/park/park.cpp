#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e5+7,M=2e5+7;

int head[N],cnt;
struct Line{
	int u,v,w,next;
}e[M<<1];
void ins(int u,int v,int w){
	e[++cnt]=(Line){
		u,v,w,head[u]
	};
	head[u]=cnt;
}
void insert(int u,int v,int w){
	ins(u,v,w);
	ins(v,u,w);
}

int In[N],fir[N],f[N][51];
void Clear(){
	memset(head,0,sizeof(head));
	cnt=0;
	memset(In,0,sizeof(In));
	memset(fir,67,sizeof(fir));
	memset(f,0,sizeof(f));
}

int n,m,LK,P;
const int rxd=1000007;
int q[rxd];
int vis[N];

int main(){
	// say hello

	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);

	int T=read();
	For(p,1,T){
		Clear();
		n=read(),m=read(),LK=read(),P=read();
		For(i,1,m){
			int u=read(),v=read(),w=read();
			ins(u,v,w);
		}
		int l=0,r=1;
		q[r]=1;fir[1]=0;In[1]=1;
		int flag=1;
		while (l!=r){
			l++;l%=rxd;
			int x=q[l];
			vis[x]++;
			if (vis[x]>(n+100)||(n>1000&&vis[x]>5000)){
				flag=0;
				break;
			}
			for (int i=head[x];i;i=e[i].next){
				int y=e[i].v;
				if (fir[y]>=fir[x]+e[i].w){
					fir[y]=fir[x]+e[i].w;
					if (!In[y]){
						In[y]=1;
						r++;r%=rxd;
						q[r]=y;
					}				
				}
			}
			In[x]=0;
		}
		if (!flag){
			printf("-1\n");
			continue;
		}
//		printf("%d\n",fir[n]);
	//	For(i,1,n) printf("%d ",fir[i]); printf("\n");

		flag=1;
		int ans=0;
		l=0,r=1;
		memset(In,0,sizeof(In));
		q[r]=1;f[1][0]=1;In[1]=1;
		while (l!=r){
			l++;if (l>=rxd) l-=rxd;
			int x=q[l];
			vis[x]++;
			if (vis[x]>(n+100)||(n>1000&&vis[x]>5000)){
				flag=0;
				break;
			}
		//	int last_r=r;
			for (int i=head[x];i;i=e[i].next){
				int y=e[i].v,flag1=0;
				For(j,0,LK){
					if (!f[x][j]) continue;
					int k=(fir[x]+j+e[i].w)-fir[y];
					if (k>LK) break;
					f[y][k]+=f[x][j];
					if (f[y][k]>=P) f[y][k]-=P;
					if (y==n) {
						ans+=f[x][j];
						if (ans>=P) ans-=P;
					}
		//			printf("f[%d][%d]=%d   f[%d][%d]=%d\n",y,k,f[y][k],x,j,f[x][j]);
					flag1=1;
				}
				if (flag1&&!In[y]){
					In[y]=1;
					r++;if (r>=rxd) r-=rxd;
					q[r]=y;
				}
			}
		//	printf("%d %d\n",r,last_r);
			In[x]=0;
			For(i,0,50) f[x][i]=0;
		}
		
		if (!flag) printf("-1\n");
		else printf("%d\n",ans);
	}





	return 0;

	// say goodbye
}


#include<cstdio>
#include<cctype>
#include<stack>
#include<cctype>
#include<cstring>
using namespace std;
const int maxn=10000,inf=0x3f3f3f3f;
int t,l,maxv; char str[10];
bool vis[100];
struct data{
	char var; int dep;
	data(char var,int dep):
		var(var),dep(dep){}
};
stack<data> stk;
char readch(){
	char c=getchar();
	while(!isgraph(c)) c=getchar();
	return c;
}
int getint(char *s){
	int l=strlen(s),ans=0;
	for(int i=0;i<l;++i) ans=ans*10+s[i]-'0';
	return ans;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d",&l); scanf("%s",str);
		maxv=0;
		if(str[2]=='n')
			for(int i=4;str[i]!=')';++i)
				maxv=maxv*10+str[i]-'0';
		bool ce=0;
		while(!stk.empty()) stk.pop();
		memset(vis,0,sizeof vis); int ans=0;
		for(int i=1;i<=l;++i){
			char op=readch();
			if(op=='E'){
				if(ce) continue;
				if(stk.empty()){
					ce=1; continue;
				}
				vis[stk.top().var-'a']=0; stk.pop();
			}else{
				char var=readch();
				char down[10],up[10]; scanf("%s%s",down,up);
				if(ce) continue;
				if(vis[var-'a']){
					ce=1; continue;
				}
				vis[var-'a']=1;
				if(down[0]=='n'&&up[0]=='n') stk.push(data(var,stk.empty()?0:stk.top().dep));
				else if(down[0]=='n') stk.push(data(var,-inf));
				else if(up[0]=='n') stk.push(data(var,stk.empty()?1:stk.top().dep+1));
				else if(getint(down)<=getint(up)) stk.push(data(var,stk.empty()?0:stk.top().dep));
				else stk.push(data(var,-inf));
			}
			if(!stk.empty()) ans=max(ans,stk.top().dep);
		}
		if(!stk.empty()) ce=1;
		if(ce) puts("ERR");
		else puts(maxv==ans?"Yes":"No");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<queue>
using namespace std;
const int maxn=100000,maxk=50,inf=0x3f3f3f3f;
inline void read(int &x){
	x=0; char c=getchar();
	while(c<'0'||c>'9') c=getchar();
	while(c>='0'&&c<='9'){
		x=x*10+c-'0'; c=getchar();
	}
}
struct edge{
	int to,len,nxt;
}eg[maxn*10+10]; int nxt[maxn+10];
int g[maxn+10];
int gg[maxn+10];
int d[maxn+10],egcnt;
inline void addedge(int &from,int to,int len){
	eg[++egcnt].to=to; eg[egcnt].len=len;
	eg[egcnt].nxt=from; from=egcnt;
}
int n,m,t,ind[maxn+10],q[maxn+10],diss[maxn+10],dist[maxn+10];
bool vis[maxn+10]; int k,mod,f[maxk+1][maxn+10];
struct heapnode{
	int id,v;
	inline heapnode(int id,int v):
		id(id),v(v){}
	inline bool operator<(const heapnode &t)const{
		return v>t.v;
	}
};
priority_queue<heapnode> heap;
inline void dijkstra(int s,int *h,int *dis){
	for(register int i=1;i<=n;++i) dis[i]=i==s?0:inf;
	heap.push(heapnode(s,0));
	while(!heap.empty()){
		heapnode x=heap.top(); heap.pop();
		if(x.v>dis[x.id]) continue;
		for(register int i=h[x.id];i;i=eg[i].nxt){
			edge e=eg[i];
			if(x.v+e.len<dis[e.to]){
				dis[e.to]=x.v+e.len; heap.push(heapnode(e.to,dis[e.to]));
			}
		}
	}
}
int ql,qr;
inline void toposort(){
	ql=1; qr=0;
	for(register int i=1;i<=n;++i) if(!ind[i]) q[++qr]=i;
	while(ql<=qr){
		int x=q[ql++]; vis[x]=1;
		for(int i=d[x];i;i=eg[i].nxt){
			edge e=eg[i];
			if(!--ind[e.to]) q[++qr]=e.to;
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	read(t);
	while(t--){
		read(n); read(m); read(k); read(mod); egcnt=0;
		for(register int i=1;i<=n;++i){
			g[i]=d[i]=gg[i]=ind[i]=vis[i]=0;
		}
		for(register int i=1;i<=m;++i){
			int l,r,v; read(l); read(r); read(v);
			addedge(g[l],r,v); addedge(gg[r],l,v);
			if(!v) addedge(d[l],r,v),++ind[r];
		}
		dijkstra(1,g,diss); dijkstra(n,gg,dist);
		toposort(); bool isinf=0;
		for(register int i=1;i<=n;++i) if(!vis[i]&&diss[i]+dist[i]<=diss[n]+k){
			printf("-1\n"); isinf=1; break;
		}
		if(isinf) continue;
		for(register int i=1;i<=n;++i){
			d[i]=ind[i]=0;
		}
		for(register int i=1;i<=n;++i)
			for(register int j=g[i];j;j=eg[j].nxt){
				edge e=eg[j];
				if(diss[i]+e.len==diss[e.to]) addedge(d[i],e.to,e.len),++ind[e.to];
			}
		toposort();
		for(register int i=1;i<=n;++i) for(register int j=0;j<=k;++j) f[j][i]=0; f[0][1]=1;
		for(register int i=0;i<=k;++i)
			for(register int j=1;j<=qr;++j) if(f[i][q[j]]){
				int x=q[j];
				for(register int l=g[x];l;l=eg[l].nxt){
					edge e=eg[l];
					if(diss[x]+i+e.len-diss[e.to]<=k) (f[diss[x]+i+e.len-diss[e.to]][e.to]+=f[i][x])%=mod;
				}
			}
		int ans=0;
		for(register int i=0;i<=k;++i) (ans+=f[i][n])%=mod;
		printf("%d\n",ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

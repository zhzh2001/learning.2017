#include<set>
#include<cmath>
#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
typedef long long ll;
typedef pair<int,int> pa;
#define rep(i,a,b) for(int (i)=(a);(i)<=(b);(i)++)
#define rep2(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
#define Rep(p,x) for(int (p)=head[(x)];(p);(p)=nxt[(p)])
#define SZ(x) (int((x).size()))
#define pb push_back
#define mp make_pair
#define w1 first
#define w2 second
template<class T>void read(T&num){
	num=0;T f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')num=num*10+ch-'0',ch=getchar();
	num*=f;
}
const int maxn=1e5+5,maxm=2e5+5;
int n,m,k,mod,tot;
int ind,top,sccnum;
int x[maxm],y[maxm],z[maxm];
int head[maxn],des[maxm],nxt[maxm],val[maxm];
int dfn[maxn],low[maxn],bel[maxn],sz[maxn],st[maxn];
int dis[maxn],vis[maxn];
int f[maxn][55];
vector<int>e[maxn];
void init(){
	tot=ind=top=sccnum=0;
	rep(i,1,n)e[i].clear();
	rep(i,1,n)head[i]=dfn[i]=low[i]=bel[i]=sz[i]=0;
}
void tarjan(int x){
	dfn[x]=low[x]=++ind;
	st[++top]=x;
	rep2(i,0,SZ(e[x])){
		int y=e[x][i];
		if(!dfn[y])tarjan(y),low[x]=min(low[x],low[y]);
		else if(!bel[y])low[x]=min(low[x],dfn[y]);
	}
	if(dfn[x]==low[x]){
		++sccnum;
		while(st[top]!=x)bel[st[top]]=sccnum,sz[sccnum]++,top--;
		bel[st[top]]=sccnum;sz[sccnum]++;top--;
	}
}
void adde(int x,int y,int z){
	if(x==y)return;
	des[++tot]=y;val[tot]=z;nxt[tot]=head[x];head[x]=tot;
}
priority_queue<pa,vector<pa>,greater<pa> >heap;
void getdis(){
	rep(i,1,sccnum)dis[i]=1e9,vis[i]=0;
	dis[bel[1]]=0;
	heap.push(mp(0,bel[1]));
	while(!heap.empty()){
		int x=heap.top().w2;heap.pop();
		if(vis[x])continue;vis[x]=1;
		Rep(p,x)if(dis[des[p]]>dis[x]+val[p]){
			dis[des[p]]=dis[x]+val[p];
			heap.push(mp(dis[des[p]],des[p]));
		}
	}
}
int deg[maxn][55];
int qx[maxn*55],qy[maxn*55];
void rebuild(){
	rep(i,1,sccnum)rep(j,0,k)deg[i][j]=0;
	rep(i,1,sccnum)rep(j,0,k){
		Rep(p,i){
			int nj=dis[i]+j+val[p]-dis[des[p]];
			if(nj>=0&&nj<=k)deg[des[p]][nj]++;
		}
	}
}
#define add(x,y) ((x)+(y)>=mod?(x)+(y)-mod:(x)+(y))
void dp(){
	int h=0,t=0;
	rep(i,1,sccnum)rep(j,0,k)f[i][j]=0;
	rep(i,1,sccnum)rep(j,0,k)if(!deg[i][j]){
		qx[t]=i;qy[t]=j;t++;
		if((i==bel[1])&&(j==0)){
			f[i][j]=1;
			if(sz[i]>1)f[i][j]=-1;
		}
	}
	while(h!=t){
		int x=qx[h],y=qy[h];h++;
		Rep(p,x){
			int nx=des[p];
			int ny=dis[x]+y+val[p]-dis[des[p]];			
			if(ny>=0&&ny<=k){
				if(sz[nx]>1||f[x][y]==-1||f[nx][ny]==-1)f[nx][ny]=-1;
				else f[nx][ny]=add(f[nx][ny],f[x][y]);
				deg[nx][ny]--;
				if(!deg[nx][ny])qx[t]=nx,qy[t]=ny,t++;
			}
			
		}
	}
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T;read(T);
	while(T--){
		init();
		read(n);read(m);read(k);read(mod);
		rep(i,1,m){
			read(x[i]),read(y[i]),read(z[i]);
			if(!z[i])e[x[i]].pb(y[i]);
		}
		rep(i,1,n)if(!dfn[i])tarjan(i);
		rep(i,1,m)adde(bel[x[i]],bel[y[i]],z[i]);
		getdis();
		rebuild();
		dp();
		int flag=0;
		rep(i,0,k)if(f[bel[n]][i]==-1)flag=1;
		if(flag)puts("-1");
		else{
			int sum=0;
			rep(i,0,k)sum=add(sum,f[bel[n]][i]);
			printf("%d\n",sum);
		}
	}
	return 0;
}

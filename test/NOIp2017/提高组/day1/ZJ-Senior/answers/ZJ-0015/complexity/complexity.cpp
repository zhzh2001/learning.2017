#include<set>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<vector>
#include<iostream>
#include<algorithm>
using namespace std;
typedef long long ll;
#define rep(i,a,b) for(int (i)=(a);(i)<=(b);(i)++)
#define rep2(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
const int maxn=1e3+5,inf=1e9;
int n,top;
struct node{
	int i,flag;
}st[maxn];
int vis[maxn];
void work(){
	memset(vis,0,sizeof(vis));
	top=100;st[top].flag=1;
	int maxi=0,cnt=0;
	scanf("%d",&n);
	char str[10];scanf("%s",str);
	int w=0;
	if(str[2]=='n'){
		rep2(i,0,strlen(str))if(str[i]>='0'&&str[i]<='9')w=w*10+str[i]-'0';
	}
	rep(i,1,n){
		char opt[10];scanf("%s",opt);
		if(opt[0]=='E'){
			if(top<=100)maxi=inf;
			if(st[top].flag==2)cnt--;
			vis[st[top].i]=0;
			top--;
		}else{
			char ltr[10];scanf("%s",ltr);
			int num=ltr[0]-'a';
			if(vis[num])maxi=inf;
			vis[num]=1;
			char L[10],R[10];
			node now;
			now.i=num;now.flag=0;
			scanf("%s%s",L,R);
			if(st[top].flag){
				if(L[0]=='n'){
					if(R[0]=='n'){
						now.flag=1;
					}else{
						now.flag=0;
					}
				}else{
					if(R[0]=='n'){
						now.flag=2;
						cnt++;
					}else{
						int Lnum=0,Rnum=0;
						rep2(i,0,strlen(L))Lnum=Lnum*10+L[i]-'0';
						rep2(i,0,strlen(R))Rnum=Rnum*10+R[i]-'0';
						if(Lnum<=Rnum)now.flag=1;
						else now.flag=0;
					}
				}
			}
			st[++top]=now;
			maxi=max(maxi,cnt);
		}
	}
	if(top!=100)maxi=inf;
	if(maxi==inf)puts("ERR");
	else if(maxi==w)puts("Yes");
	else puts("No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int caset;scanf("%d",&caset);
	while(caset--)work();
	return 0;
}

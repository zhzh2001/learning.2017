#include<cmath>
#include<queue>
#include<stack>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define reg register int
#define rep(i, a, b) for (reg i = (a); i <= (b); i++)
#define per(i, a, b) for (reg i = (a); i >= (b); i--)
#define travel(i, u) for (reg i = head[u]; i; i = edge[i].next)
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}
int Dis[1005][1005], F[105][1005], num = 0, head[1005], n, m, K, P, INF = 1e9, Flag[1005], ans;
priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > Heap;
struct node
{
	int vet, next, val;
}edge[200005];
void addedge(int u, int v, int w)
{
	edge[++num].vet = v;
	edge[num].next = head[u];
	edge[num].val = w;
	head[u] = num;
}

void Dijkstra(int s)
{
	rep(i, 1, n) Dis[s][i] = INF, Flag[i] = 0;
	Dis[s][s] = 0;
	Heap.push(make_pair(0, s));
	while (!Heap.empty())
	{
		int u = Heap.top().second;
		Heap.pop();
		if (Flag[u]) continue;
		Flag[u] = 1;
		travel(i, u)
		{
			int v = edge[i].vet;
			if (Dis[s][u] + edge[i].val < Dis[s][v])
			{
				Dis[s][v] = Dis[s][u] + edge[i].val;
				Heap.push(make_pair(Dis[s][v], v));
			}
		}
	}
}
int sqz()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int H_H = read();
	while (H_H--)
	{
		n = read(), m = read(), K = read(), P = read(), ans = 0;
		rep(i, 1, m)
		{
			int u = read(), v = read(), w = read();
			addedge(u, v, w);
		}
		if (n <= 1000)
		{
			rep(i, 1, n) Dijkstra(i), F[0][i] = 1;
			rep(i, 0, K)
				rep(u, 1, n)
					travel(j, u)
					{
						int v = edge[j].vet;
						if (edge[j].val == Dis[u][v])
						{
							F[i][v] = 1;
							continue;
						}
						if (i + edge[j].val - Dis[u][v] > K) continue;
						F[i + edge[j].val - Dis[u][v]][v] = (F[i][u] + F[i + edge[j].val - Dis[u][v]][v]) % P;
					}
			rep(i, 0, K) ans = (ans + F[i][n]) % P;
			printf("%d\n", ans);
		}
	}
	return 0;
}

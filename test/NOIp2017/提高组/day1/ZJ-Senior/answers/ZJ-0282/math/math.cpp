#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define reg register int
#define rep(i, a, b) for (reg i = (a); i <= (b); i++)
#define per(i, a, b) for (reg i = (a); i >= (b); i--)
#define travel(i, u) for (reg i = head[u]; i; i = edge[i].next)
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int sqz()
{
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	ll a = read(), b = read();
	printf("%lld\n", a * b - a - b);
	return 0;
}

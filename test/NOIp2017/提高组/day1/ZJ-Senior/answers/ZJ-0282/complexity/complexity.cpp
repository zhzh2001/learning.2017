#include<cmath>
#include<stack>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define reg register int
#define rep(i, a, b) for (reg i = (a); i <= (b); i++)
#define per(i, a, b) for (reg i = (a); i >= (b); i--)
#define travel(i, u) for (reg i = head[u]; i; i = edge[i].next)
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}
int flag[30], Flag[105], num[105], T[105], pd[105];
char st[10], op[5], s1[5], s2[5], s3[5];
stack<int> Stack;

int Read()
{
	scanf("%s", st);
	if (st[2] == '1') return 0;
	else
	{
		int pos = 4, xx = 0;
		while (st[pos] >= '0' && st[pos] <= '9') xx = xx * 10 + st[pos] - '0', pos++;
		return xx;
	}
}
int Solve()
{
	int L = read(), p = Read(), Max = 0, tflag = 1;
	memset(flag, 0, sizeof flag);
	memset(Flag, 0, sizeof Flag);
	memset(pd, -1, sizeof pd);
	while (!Stack.empty()) Stack.pop();
	rep(i, 1, L)
	{
		scanf("%s", op);
		if (op[0] == 'F')
		{
			scanf("%s%s%s", s1, s2, s3);
			if (flag[s1[0] - 'a']) tflag = 0;
			flag[s1[0] - 'a'] = 1; T[i] = s1[0] - 'a';
			if (s2[0] != 'n' && s3[0] == 'n') Flag[i] = 1;
			else if (s2[0] == 'n' && s3[0] == 'n') Flag[i] = 0;
			else if (s2[0] == 'n' && s3[0] != 'n') Flag[i] = -1;
			else
			{
				int pos1 = 0, pos2 = 0, xx = 0, yy = 0;
				while (pos1 < strlen(s2)) xx = xx * 10 + s2[pos1] - '0', pos1++;
				while (pos2 < strlen(s3)) yy = yy * 10 + s3[pos2] - '0', pos2++;
				if (xx > yy) Flag[i] = -1;
				else Flag[i] = 0;
			}
			Stack.push(i);
		}
		else
		{
			if (Stack.empty()) return -1;
			int t = Stack.top();
			Stack.pop();
			flag[T[t]] = 0;
			pd[t] = i;
		}
	}
	if (!Stack.empty() || !tflag) return -1;
	rep(i, 1, L)
	{
		num[i] = 0;
		rep(j, 1, i)
			if (pd[j] < i) continue;
			else if (Flag[j] == -1) break;
			else num[i] += Flag[j];
	}
	rep(i, 1, L) Max = max(Max, num[i]);
	return Max == p;
}
int sqz()
{
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	int H_H = read();
	while (H_H--)
	{
		int t = Solve();
		if (t == -1) puts("ERR");
		else if (t) puts("Yes");
		else puts("No");
	}
	return 0;
}

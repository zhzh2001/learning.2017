#include<cstdio>
#include<cstring>
int t,L,res,mx,tmp,N,ERR,cnt[1000],f[1000],l,r,flag,bianlian[1000];
char s[1000],ch;
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d",&L);
		ch=getchar();
		while(ch!='(') ch=getchar();
		ch=getchar();
		res=0;
		if (ch=='n')
		{
			while(ch<'0'||ch>'9') ch=getchar();
			while(ch>='0'&&ch<='9')
			{
				res=res*10+ch-'0';
				ch=getchar();
			}
		}
		gets(s);
		mx=0;
		tmp=0;
		N=0;
		ERR=0;
		flag=0;
		memset(f,0,sizeof(f));
		memset(bianlian,0,sizeof(bianlian));
		memset(cnt,0,sizeof(cnt));
		for (int i=1;i<=L;i++)
		{
			ch=getchar();
			while(ch!='F'&&ch!='E') ch=getchar();
			if (ch=='E')
			{
				if (ERR==1) continue;
				if (tmp==0) ERR=1;
				if (f[tmp]==1) N--;
				if (f[tmp]==2) flag--;
				cnt[bianlian[tmp]]--;
				tmp--;
			}
			else 
			{
				ch=getchar();
				while(ch<'a'||ch>'z') ch=getchar();
				if (ERR==0)
				{
					if (cnt[ch-'a']==1) ERR=1;
					cnt[ch-'a']=1;
					tmp++;
					bianlian[tmp]=ch-'a';
				}
				
				ch=getchar();
				while((ch!='n')&&(ch<'0'||ch>'9')) ch=getchar();
				if (ch=='n') l=1000;
				else
				{
					l=0;
					while(ch>='0'&&ch<='9')
					{
						l=l*10+ch-'0';
						ch=getchar();
					}
				}
				
				ch=getchar();
				while((ch!='n')&&(ch<'0'||ch>'9')) ch=getchar();
				if (ch=='n') r=10000;
				else
				{
					r=0;
					while(ch>='0'&&ch<='9')
					{
						r=r*10+ch-'0';
						ch=getchar();
					}
				}
				if (ERR==0)
				{
					f[tmp]=0;
					if (r==10000&&l!=1000)
					{	
						f[tmp]=1;
						N++;
					}
					if (r<l) {flag++;f[tmp]=2;}
					if (flag==0&&N>mx) mx=N;
				}
			}
		}
		if (ERR==1||tmp!=0) printf("ERR\n");
		else if (mx==res) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

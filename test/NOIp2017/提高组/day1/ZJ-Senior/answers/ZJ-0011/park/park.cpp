#include <iostream>
#include <queue>
#include <algorithm>
using namespace std;
int t, n, m, k, i, j, ans = 0, num[100002];
long long pp, dis[100002];
bool queued[100002];

struct node {
	int n;
	long long v;
	node* next;
	node() {
		n = 0;
		v = 0;
		next = NULL;
	}
} *e[200002], *p;

queue<int> que;

int main() {
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	cin >> t;
	for (j=1; j<=t; j++) {
		cin >> n >> m >> k >> pp;
		int a, b;
		long long c;
		for (i=1; i<=n; i++) {
			dis[i] = 400000000;
			queued[i] = false;
		}
		for (i=1; i<=m; i++) {
			cin >> a >> b >> c;
			p = new node();
			p->n = b;
			p->v = c;
			if (e[a] == NULL)
				e[a] = p;
			else {
				p->next = e[a]->next;
				e[a]->next = p;
			}
		}
		dis[1] = 0;
		num[1] = 1;
		que.push(1);
		queued[1] = true;
		while (!que.empty()) {
			int x = que.front();
			que.pop();
			p = e[x];
			while (p != NULL) {
				if (dis[x] + p->v < dis[p->n]) {
					dis[p->n] = dis[x] + p->v;
					num[p->n] = num[x];
					if (!queued[p->n]) {
						queued[p->n] = true;
						que.push(p->n);
					}
				}
				else if (dis[x] + p->v == dis[p->n]) {
					num[p->n] += num[x];
					num[p->n] %= pp;
				}
				p = p->next;
			}
			queued[x] = false;
		}
		cout << num[n] << endl;
	}
	fclose(stdin);
	fclose(stdout);
	//system("pause");
return 0;
}

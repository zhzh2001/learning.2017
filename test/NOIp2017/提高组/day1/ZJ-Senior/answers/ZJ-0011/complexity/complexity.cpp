#include <iostream>
#include <string>
#include <stack>
#include <memory.h>
using namespace std;
int t, m, l, i, exp, fact, curComp;
bool vard[27], error, confirmed;
string cplx;

struct line {
	char type;
	char var;
	int comp;
	bool confirmed;
} loc[102];

stack<line> st;

int main() {
	freopen("complexity.in", "r", stdin);
	freopen("complexity.out", "w", stdout);
	cin >> t;
	for (m=1; m<=t; m++) {
		while (!st.empty())
			st.pop();
		cin >> l;
		cin >> cplx;
		if (cplx[2] == '1')
			exp = 0;
		else {
			int p = 4;
			exp = 0;
			while (cplx[p]>='0' && cplx[p]<='9') {
				exp *= 10;
				exp += cplx[p] - '0';
				p++;
			}
		}
		fact = 0;
		memset(vard, 0, sizeof(vard));
		error = false;
		confirmed = false;
		curComp = 0;
		getchar();
		char tmp;
		for (i=1; i<=l; i++) {
			int start, end;
			tmp = getchar();
			loc[i].type = tmp;
			
			if (tmp == 'F') {
				loc[i].comp = curComp;
				loc[i].confirmed = confirmed;
				getchar();
				tmp = getchar();
				loc[i].var = tmp;
				if (!vard[tmp - 'a']) {
					vard[tmp - 'a'] = true;
				}
				else error = true;
				
				getchar();
				start = 0;
				tmp = getchar();
				if (tmp == 'n') {
					start = 233;
					getchar();
				}
				else {
					while (tmp>='0' && tmp<='9') {
						start *= 10;
						start += tmp - '0';
						tmp = getchar();
					}
				}
				
				tmp = getchar();
				end = 0;
				if (tmp == 'n') {
					end = 233;
					getchar();
				}
				else {
					while (tmp>='0' && tmp<='9') {
						end *= 10;
						end += tmp - '0';
						tmp = getchar();
					}
				}
				
				if (start > end) {
					confirmed = true;
					loc[i].confirmed = true;
				}
				if (!loc[i].confirmed) {
					if (start != 233 && end == 233) {
						curComp++;
						loc[i].comp++;
						if (loc[i].comp > fact)
							fact = loc[i].comp;
					}
				}
				
				st.push(loc[i]);
			}
			else {
				if (st.empty())
					error = true;
				else {
					vard[st.top().var - 'a'] = false;
					confirmed = st.top().confirmed;
					curComp = st.top().comp;
					if (!confirmed)
						curComp--;
					st.pop();
				}
				getchar();
			}
		}
		if (!st.empty())
			error = true;
		if (error)
			cout << "ERR" << endl;
		else if (fact != exp)
			cout << "No" << endl;
		else cout << "Yes" << endl;
	}
	fclose(stdin);
	fclose(stdout);
	//system("pause");
return 0;
}

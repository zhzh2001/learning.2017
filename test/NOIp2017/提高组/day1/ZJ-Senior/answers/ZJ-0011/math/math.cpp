#include <iostream>
#include <algorithm>
using namespace std;
typedef long long ll;
int a, b, x, y, big, limit, combNum, ans;
bool comb[15000002];

int main() {
	freopen("math.in", "r", stdin);
	freopen("math.out", "w", stdout);
	cin >> a >> b;
	big = max(a, b);
	limit = min(big<<1, 2000);
	for (x=0; x<=min(a, b)-1; x++)
		comb[x] = false;
	comb[a] = true;
	comb[b] = true;
	for (x=min(a, b)+1; x<=15000000; x++)
		if (comb[x-a] || comb[x-b])
			comb[x] = true;
	for (x=15000000; x>=0; x--)
		if (!comb[x]) {
			cout << x;
			break;
		}
	fclose(stdin);
	fclose(stdout);
	//system("pause");
return 0;
}

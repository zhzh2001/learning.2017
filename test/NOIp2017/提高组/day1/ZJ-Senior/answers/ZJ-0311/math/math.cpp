#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define fi first
#define se second
#define lowbit(x) ((x)&(-x))
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)

bool chkmin(int& x,int y){ return y<x?x=y,true:false; }
bool chkmax(int& x,int y){ return y>x?x=y,true:false; }

int read(void){
	char ch=getchar();int x,f=1;
	for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;
	for (x=0;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
typedef long long LL;
#define int LL

int a,b,k1,k2,k,c,d;
void gcd(int a,int b,int& x,int& y){
	if (b==0){ x=1;y=0; return ;}
	gcd(b,a%b,y,x);y-=a/b*x;
}
signed main(void){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	a=read();b=read();
	k1=b%a;k2=a%b;
	if (a<b) { swap(k1,k2); swap(a,b); }
	if (a>10000||b>10000){ k=max(0ll,min(a,b)-10000); }
	while(1){if ((k*a-k1+k2)%b==0){printf("%lld\n",k*a-k1); return 0; }++k;}
	return 0;
}

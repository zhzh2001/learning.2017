//gg
//blank
#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define fi first
#define se second
#define lowbit(x) ((x)&(-x))
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)
#define oo 0x7f7f7f7f

bool chkmin(int& x,int y){ return y<x?x=y,true:false; }
bool chkmax(int& x,int y){ return y>x?x=y,true:false; }

int read(void){
	char ch=getchar();int x,f=1;
	for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;
	for (x=0;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
typedef long long LL;

int cnt,nxt[200005],w[200005],fall[200005],hd[100005];
void ae(int x,int y,int z){	++cnt;fall[cnt]=y;nxt[cnt]=hd[x];w[cnt]=z;hd[x]=cnt; }

typedef pair<int,int> PII;
priority_queue<PII,vector<PII>,greater<PII> > Q;
int n,m,k,mj,mjj,de,ans,MO,x,y,z,ttt,dis[100005];
int f[100005][51],g[1005][1005],f1[51];
bool v[100005];

void sc(int x,int s){
	if (s-ttt>k) return ;v[x]=1;
	if (x==n) { ans++; if (ans==MO) ans=0; v[x]=0;return ; }

	for(int j=hd[x];j;j=nxt[j]){
		int to=fall[j];
		if (v[to]) continue;
		sc(to,s+w[j]);
	}
	v[x]=0;
}

void bf(){
	FO(i,1,n){
		mj=0;
		FO(j,1,n) if (!v[j]&&dis[j]<dis[mj]) mj=j;
		if (mj==0) break; v[mj]=1;
		FO(j,1,n) if (dis[j]>dis[mj]+g[mj][j]){
			FO(z,0,k) f1[z]=f[j][z];
			FO(z,0,k) f[j][z]=f[mj][z];
			de=dis[mj]+g[mj][j]-dis[j];de*=-1;
			if (de<=k) FO(z,de,k) {
				f[j][z]+=f1[z-de];
				if(f[j][z]>MO) f[j][z]-=MO;
			}
			dis[j]=dis[mj]+g[mj][j];
		}else{
			de=dis[mj]+g[mj][j]-dis[j];
			if (de<=k) FO(z,de,k) {
				f[j][z]+=f[mj][z-de];
				if (f[j][z]>MO) f[j][z]-=MO;
			}
		}
	}
	ans=0;
	FO(i,0,k) { ans+=f[n][i]; if (ans>MO) ans-=MO; }
}

int main(void){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int T=read();
	while (T--){ 
	n=read();m=read();k=read();MO=read();
	if (n<=1000){FO(i,1,n)FO(j,1,n) g[i][j]=oo;}
	FO(i,1,n) hd[i]=0;cnt=0;
	FO(i,1,m) { x=read();y=read();z=read(); if (n<=1000) g[x][y]=z;ae(x,y,z); } 
	memset(f,0,sizeof(f));
	FO(i,0,n) dis[i]=oo;dis[1]=0;f[1][0]=1;
	if (n<=1000&&n>5) { bf(); cout<<ans<<endl; }
	Q.push(mp(0,1));
	while (!Q.empty()){
		PII now=Q.top();Q.pop();
		mj=now.se;mjj=now.fi;
		if (mjj>dis[mj]) continue;

		for(int j=hd[mj];j;j=nxt[j]){
			int to=fall[j];
			if (dis[to]>mjj+w[j]){
				FO(z,0,k) f1[z]=f[to][z];
				FO(z,0,k) f[to][z]=f[mj][z];
				de=mjj+w[j]-dis[to];de*=-1;
				if (de<=k) FO(z,de,k) {
					f[to][z]+=f1[z-de];
					if(f[to][z]>MO) f[to][z]-=MO;
				}
				dis[to]=mjj+w[j];
				Q.push(mp(dis[to],to));
			}else{
				de=mjj+w[j]-dis[to];
				if (de<=k) FO(z,de,k) {
					f[to][z]+=f[mj][z-de];
					if(f[to][z]>MO) f[to][z]-=MO;
				}
			}
		}
	}
	memset(v,0,sizeof(v));
	if (n<=5){ ttt=dis[n]; ans=0; sc(1,0); cout<<ans<<endl; continue; }
	ans=0;
	FO(i,0,k) { ans+=f[n][i]; if(ans>MO) ans-=MO; }
	cout<<ans<<endl;
	}
	return 0;
}

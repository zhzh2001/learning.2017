#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define fi first
#define se second
#define lowbit(x) ((x)&(-x))
#define FO(i,a,b) for (int i=a;i<=b;++i)
#define FD(i,a,b) for (int i=a;i>=b;--i)

bool chkmin(int& x,int y){ return y<x?x=y,true:false; }
bool chkmax(int& x,int y){ return y>x?x=y,true:false; }

int read(void){
	char ch=getchar();int x,f=1;
	for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;
	for (x=0;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

struct Seg{ int sy,re,no,o1,ans; };
int fo1,T,l,bb,flag,cnt;
bool uused[10005];
char s[10005];
Seg t[100005];
stack<int> S;
int tonum(char *s){
	int n=strlen(s),ans=0;
	FO(i,0,n-1) ans=ans*10+s[i]-'0';
	return ans;
}
int main(void){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read();
	while (T--){
		scanf("%d%s",&l,s+1);
		t[0].ans=0;t[0].o1=1;cnt=0;
		memset(uused,0,sizeof(uused));
		while (!S.empty()) S.pop();
		if (s[3]=='1') fo1=1; else {
			fo1=0;bb=0; 
			int x=5;
			while (isdigit(s[x])) bb=bb*10+s[x]-'0',x++;
		}
		S.push(0);flag=true;
		FO(i,1,l){
			scanf("%s",s+1);
			if (s[1]=='F'){
				scanf("%s",s+1);
				if (uused[s[1]]){  flag=false; }
				Seg x;x.sy=s[1];uused[s[1]]=1;x.re=S.top();
				scanf("%s",s+1);int s11=tonum(s+1);bool xx=s[1]=='n';
				if (s[1]=='n') x.no=1; else x.no=0;
				scanf("%s",s+1);int s22=tonum(s+1);bool yy=s[1]=='n';
				if (s[1]!='n') x.o1=1; else x.o1=0;
				if (x.o1&&x.no) {x.o1=0;x.no=1;}
				if (x.o1&&s11>s22) { x.o1=0;x.no=1; }
				if (xx&&yy) {x.o1=1;x.no=0;}
				x.ans=x.o1?0:1;t[++cnt]=x;
				S.push(cnt);
			}else{
				if (S.top()==0){  flag=false; }else{
				Seg x=t[S.top()];
				uused[x.sy]=0;
				if (!x.no)
				t[x.re].ans=max(t[x.re].ans,x.ans+(t[x.re].o1?0:1));
				S.pop();}
			}
		}
		if (S.top()!=0) {  flag=false; }
		if (flag){
			if (fo1) puts(t[0].ans==0?"Yes":"No");
			else puts(t[0].ans==bb?"Yes":"No");
		}else puts("ERR");
	}
	return 0;
}

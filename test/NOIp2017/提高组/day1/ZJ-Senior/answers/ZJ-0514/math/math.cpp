#include<cstdio>
using namespace std;
void gcd(int a,int b,long long &x,long long &y)
{
	if (b==0) {x=1;y=0;return;}
	gcd(b,a%b,y,x);y-=a/b*x;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	int a,b;
	long long x,y;
	scanf("%d%d",&a,&b);gcd(a,b,x,y);printf("%lld\n",x<=0?x*a*(1-a)-1:y*b*(1-b)-1);
	fclose(stdin);fclose(stdout);
	return 0;
}

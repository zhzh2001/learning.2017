#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=100010,maxe=200010,maxm=55;
int a[maxn],f[maxn][maxm],lnk1[maxn],lnk2[maxn],lowcost[maxn],nxt1[maxe],nxt2[maxe],qu[maxn],sn1[maxe],sn2[maxe],w1[maxe],w2[maxe],T,n,e,m,tt,head,tail,tot1,tot2,ans;
bool vis[maxn];
void read(int &x)
{
	char ch=getchar();
	for (x=0;ch<'0' || ch>'9';ch=getchar());
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-48;
}
void add1(int x,int y,int z)
{
	w1[++tot1]=z;sn1[tot1]=y;nxt1[tot1]=lnk1[x];lnk1[x]=tot1;
}
void add2(int x,int y,int z)
{
	w2[++tot2]=z;sn2[tot2]=y;nxt2[tot2]=lnk2[x];lnk2[x]=tot2;
}
bool dfs(int x)
{
	if (++a[x]>n) return false;
	int i,j;
	for (i=0;i<=m && f[x][i]>=0;++i);
	if (i>m) return true;
	for (j=lnk2[x];j;j=nxt2[j])
	  if (lowcost[sn2[j]]+w2[j]-lowcost[x]<=m) if (!dfs(sn2[j])) return false;
	for (i=0;i<=m;++i)
	  if (f[x][i]<0) f[x][i]=0;
	if (x==1) f[1][0]=1;
	for (j=lnk2[x];j;j=nxt2[j])
	  for (i=0;i<=m+lowcost[x]-lowcost[sn2[j]]-w2[j];++i) f[x][lowcost[sn2[j]]+w2[j]-lowcost[x]+i]=(f[x][lowcost[sn2[j]]+w2[j]-lowcost[x]+i]+f[sn2[j]][i])%tt;
	return true;
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int i,j,x,y,z;
	bool flag;
	for (read(T);T--;)
	  {
	  	memset(lnk1,0,sizeof(lnk1));
	  	memset(lnk2,0,sizeof(lnk2));
	  	for (i=1,read(n),read(e),read(m),read(tt),tot1=tot2=0;i<=e;++i) read(x),read(y),read(z),add1(x,y,z),add2(y,x,z);
	  	memset(lowcost,63,sizeof(lowcost));
	  	for (lowcost[qu[1]=tail=1]=head=0;head!=tail;)
	  	  for (j=lnk1[qu[head=(head+1)%maxn]],vis[qu[head]]=false;j;j=nxt1[j])
	  	    if (lowcost[qu[head]]+w1[j]<lowcost[sn1[j]])
	  	        {
	  	        	lowcost[sn1[j]]=lowcost[qu[head]]+w1[j];
	  	        	if (!vis[sn1[j]]) vis[qu[tail=(tail+1)%maxn]=sn1[j]]=true;
	  	        }
	  	memset(a,0,sizeof(a));
	  	memset(f,255,sizeof(f));
	  	if (dfs(n))
	  	    {
	  	    	for (i=ans=0;i<=m;++i) ans=(ans+f[n][i])%tt;
	  	    	printf("%d\n",ans);
	  	    }
	  	  else puts("-1");
	  }
	fclose(stdin);fclose(stdout);
	return 0;
}

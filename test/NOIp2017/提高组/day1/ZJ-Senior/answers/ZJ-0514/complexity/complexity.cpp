#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=110,maxa=30,inf=1<<30;
struct rec
         {
         	int f,x;
		 }stack[maxn];
int T,n,top,ans;
bool vis[maxa];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	int tem,x,y;
	bool flag;
	char ch=getchar();
	while (T--)
	  {
	  	while (ch!='O') ch=getchar();
	  	n=0;ch=getchar();ch=getchar();
	  	if (ch=='n') for (ch=getchar(),ch=getchar();ch>='0' && ch<='9';ch=getchar()) n=n*10+ch-48;
	  	memset(vis,false,sizeof(vis));
	  	for (tem=top=ans=0,flag=true;ch!='O' && ch!=EOF;)
	  	  {
	  	  	while (ch!='E' && ch!='F' && ch!='O' && ch!=EOF) ch=getchar();
	  	  	if (ch=='O' || ch==EOF) break;
	  	  	if (ch=='F')
	  	  	    {
	  	  	    	while (ch<'a' || ch>'z') ch=getchar();
	  	  	    	if (vis[ch-96]) {flag=false;break;}
	  	  	    	vis[stack[++top].x=ch-96]=true;
	  	  	    	for (ch=getchar();(ch<'0' || ch>'9') && ch!='n';ch=getchar());
	  	  	    	if (ch=='n') x=inf;
	  	  	    	  else for (x=0;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-48;
	  	  	    	for (ch=getchar();(ch<'0' || ch>'9') && ch!='n';ch=getchar());
	  	  	    	if (ch=='n') y=inf;
	  	  	    	  else for (y=0;ch>='0' && ch<='9';ch=getchar()) y=y*10+ch-48;
	  	  	    	if (x<=y) if (stack[top-1].f<0) stack[top].f=-1;
	  	  	    	            else if (x<inf && y==inf) stack[top].f=1,ans=max(ans,++tem);
	  	  	    	                   else stack[top].f=0;
	  	  	    	  else stack[top].f=-1;
	  	  	    }
	  	  	if (ch=='E')
	  	  	    {
	  	  	    	if (top==0) {flag=false;break;}
	  	  	    	vis[stack[top].x]=false;ch=getchar();
	  	  	    	if (stack[top--].f>0) --tem;
	  	  	    }
	  	  }
	  	if (top) flag=false;
	  	if (flag) puts(ans==n?"Yes":"No");
	  	  else puts("ERR");
	  }
	fclose(stdin);fclose(stdout);
	return 0;
}

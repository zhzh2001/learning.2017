#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 205
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,l,w,top; char f[N];
struct data{ ll a,b,c; }q[N];
struct ques{ char a[N],b[N],c[N],d[N]; }a[N];
bool init(){
	rep(i,1,l){
		scanf("%s",a[i].a);
		if (a[i].a[0]=='F'){
			scanf("%s",a[i].b);
			scanf("%s",a[i].c);
			scanf("%s",a[i].d);
		}
	}
	ll top=0,b[N]; memset(b,0,sizeof b);
	rep(i,1,l){
		if (a[i].a[0]=='F'){
			rep(j,1,top) if (a[i].b[0]==a[b[j]].b[0]) return 1;
			b[++top]=i;
		}
		else --top;
		if (top<0) return 1;
	}
	if (top!=0) return 1;
	else return 0;
}
void work(){
	scanf("%d%s",&l,f); top=0; q[0].a=q[0].b=0,q[0].c=1;
	if (f[2]=='1') w=0;
	else{
		ll now=4; w=0;
		while (f[now]>='0'&&f[now]<='9') w=w*10+f[now]-'0',++now;
	}
	if (init()) { puts("ERR"); return; }
	rep(i,1,l){
		if (a[i].a[0]=='E'){
			q[top-1].b=max(q[top-1].b,(q[top-1].a+q[top].b)*q[top].c);
			--top;
		}
		else{
			++top;
			if (a[i].c[0]=='n'&&a[i].d[0]!='n'){
				q[top].a=q[top].b=q[top].c=0;
			}else if (a[i].c[0]!='n'&&a[i].d[0]!='n'){
				ll tmp1=0,tmp2=0;
				ll l1=strlen(a[i].c),l2=strlen(a[i].d);
				rep(j,0,l1-1) tmp1=tmp1*10+a[i].c[j]-'0';
				rep(j,0,l2-1) tmp2=tmp2*10+a[i].d[j]-'0';
				q[top].a=q[top].b=0;
				if (tmp1<=tmp2) q[top].c=1;
				else q[top].c=0;
			}
			else if (a[i].c[0]!='n'&&a[i].d[0]=='n') q[top].a=q[top].b=q[top].c=1;
			else q[top].a=q[top].b=0,q[top].c=1;
		}
	}
	puts(q[0].b==w?"Yes":"No");
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	T=read(); while (T--) work();
}

#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,n,m,k,mod,tot,dis[N],head[N];
struct data{ ll v,to; };
struct edge{ ll to,nxt,w; }e[N<<2];
bool operator <(data x,data y) { return x.v>y.v; }
priority_queue<data> Q;
void add(ll u,ll v,ll w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
ll dfs(ll u,ll now){
	if (dis[u]+now>dis[1]) return 0;
	if (u==n) return 1;
	ll tmp=0;
	for (ll i=head[u],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
		if (now+e[i].w+dis[v]<=dis[1])
			tmp+=dfs(v,now+e[i].w);
	}
	return tmp%mod;
}
void work(){
	n=read(); m=read(); k=read(); mod=read();
	rep(i,1,m){
		ll u=read(),v=read(),w=read();
		add(v,u,w);
	}
	memset(dis,64,sizeof dis);
	Q.push((data){0,1}); dis[1]=0;
	while (!Q.empty()){
		data t=Q.top(); Q.pop();
		if (dis[t.to]<t.v) continue;
		for (ll i=head[t.to],v=e[i].to;i;i=e[i].nxt,v=e[i].to){
			if (dis[t.to]+e[i].w<dis[v]){
				dis[v]=dis[t.to]+e[i].w;
				Q.push((data){dis[v],v});
			}
		}
	}
	printf("%lld\n",dis[n]);
}
int main(){
	freopen("park.in","r",stdin);
	T=read();
	while (T--) work();
}

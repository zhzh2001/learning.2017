#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
using namespace std;
#define ll long long

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
} 

int a,b,cnt,m;
ll cou[5000005];

bool cmp(ll x,ll y)
{
	return x<y;
}

int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	ll ans=-1;
	int i,j; 
	a=read(),b=read();
	for(i=0;i<=1000;i++)
		for(j=0;j<=1000;j++)
			cou[++cnt]=1ll*i*a+1ll*j*b;
	sort(cou+1,cou+cnt+1,cmp);
	m=cnt/3;
	for(i=m;i>=1;)
		if(cou[i]==ans||ans==-1)
		{
			ans=cou[i]-1;
			while(i>=1&&cou[i]==cou[i-1])
				i--;
			i--;
		}
		else
		{
			printf("%lld\n",ans);
			break;
		}
	return 0;
}

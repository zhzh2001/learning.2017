#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <time.h>
using namespace std;
#define ll long long

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
} 

int gcd(int x,int y)
{
	if(y==0)
		return x;
	return gcd(y,x%y);
}

int main()
{
	freopen("math.in","w",stdout);
	srand((unsigned)time(NULL));
	int x=rand()%500,y=rand()%500;
	while(gcd(x,y)!=1)
		x=rand()%500,y=rand()%500;
	printf("%d %d\n",x,y);
	return 0;
}

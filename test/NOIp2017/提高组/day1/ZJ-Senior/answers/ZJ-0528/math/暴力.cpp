#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
using namespace std;
#define ll long long

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
} 

int a,b;
int f[5000005];

int main()
{
	freopen("math.in","r",stdin);
	int i,j; 
	a=read(),b=read();
	for(i=0;i<=5000;i++)
		for(j=0;j<=5000;j++)
			f[i*a+j*b]=1;
	for(i=3000*(a+b);i>=0;i--)
		if(f[i]==0)
		{
			printf("%d\n",i);
			return 0;
		}
	return 0;
}

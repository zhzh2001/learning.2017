#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
using namespace std;
#define ll long long

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
} 

struct edge
{
	int to,next,v;
}e[300005],fe[300005];

struct evi
{
	int to,next;
}vi[300005];

int n,m,k,mod,cnt,sz,cnt2;
int head[100005],node[100005],he[100005];
int dis[100005],cou[100005],inq[100005],fdis[100005];
int co[100005][55];
int q[500005];
int inq2[100005][55];

struct node2
{
	int x,y;
}q2[5000005];

int dfn[100005],low[100005];
int sta[200005],tup,bcnt;
int belong[100005],a[100005],b[100005];
int ins[100005],size[100005];

void addedge(int x,int y,int w)
{
	e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;e[cnt].v=w;
}

void faddedge(int x,int y,int w)
{
	fe[++cnt2].to=y;fe[cnt2].next=he[x];he[x]=cnt2;fe[cnt2].v=w;
}

void addv(int x,int y)
{
	vi[++sz].to=y;vi[sz].next=node[x];node[x]=sz;
}

void SPFA1(int s)
{
	int i,v;
	for(i=1;i<=n;i++)
		dis[i]=0x3f3f3f3f,cou[i]=0,inq[i]=0;
	int xi,top=0,dep=0;
	q[dep++]=s;dis[s]=0;cou[s]=1%mod;inq[s]=1;
	while(top!=dep)
	{
		xi=q[top++];if(top==5000000) top=0;
		inq[xi]=0;
		for(i=head[xi];i!=-1;i=e[i].next)
		{
			v=e[i].to;
			if(dis[xi]+e[i].v<dis[v])
			{
				dis[v]=dis[xi]+e[i].v;
				cou[v]=cou[xi];
				if(!inq[v])
				{
					q[dep++]=v;
					inq[v]=1;
				}
			}
			else if(dis[xi]+e[i].v==dis[v])
			{
				cou[v]=(cou[v]+cou[xi])%mod;
				if(!inq[v])
				{
					q[dep++]=v;
					if(dep==5000000) dep=0;
					inq[v]=1;
				}
			}
		}
	}	
}

void SPFA2(int s)
{
	int i,j,v;
	for(i=1;i<=n;i++)
		for(j=0;j<=50;j++)
			co[i][j]=0,inq2[i][j]=0;
	int top=0,dep=0,cha;
	node2 xi;
	q2[dep++]=(node2){s,0};co[s][0]=1%mod;inq2[s][0]=1;
	while(top!=dep)
	{
		xi=q2[top++];if(top==5000000) top=0;
		inq2[xi.x][xi.y]=0;
		for(i=head[xi.x];i!=-1;i=e[i].next)
		{
			v=e[i].to;
			if(dis[xi.x]+xi.y+e[i].v-dis[v]<=k)
			{
				cha=dis[xi.x]+xi.y+e[i].v-dis[v];
				co[v][cha]=(co[xi.x][xi.y]+co[v][cha])%mod;
				if(!inq2[v][cha])
				{
					q2[dep++]=(node2){v,cha};
					if(dep==5000000) dep=0;
					inq2[v][cha]=1;
				}
			}
		}
	}
	int ans=0;
	for(i=0;i<=k;i++)
		ans=(ans+co[n][i])%mod;
	printf("%d\n",ans%mod);		
}

void SPFA(int s)
{
	int i,v;
	for(i=1;i<=n;i++)
		dis[i]=0x3f3f3f3f,inq[i]=0;
	int xi,top=0,dep=0;
	q[dep++]=s;dis[s]=0;inq[s]=1;
	while(top<dep)
	{
		xi=q[top++];
		inq[xi]=0;
		for(i=head[xi];i!=-1;i=e[i].next)
		{
			v=e[i].to;
			if(dis[xi]+e[i].v<dis[v])
			{
				dis[v]=dis[xi]+e[i].v;
				if(!inq[v])
				{
					q[dep++]=v;
					inq[v]=1;
				}
			}
		}
	}	
}

void fSPFA(int s)
{
	int i,v;
	for(i=1;i<=n;i++)
		fdis[i]=0x3f3f3f3f,inq[i]=0;
	int xi,top=0,dep=0;
	q[dep++]=s;fdis[s]=0;inq[s]=1;
	while(top<dep)
	{
		xi=q[top++];
		inq[xi]=0;
		for(i=he[xi];i!=-1;i=fe[i].next)
		{
			v=fe[i].to;
			if(fdis[xi]+fe[i].v<fdis[v])
			{
				fdis[v]=fdis[xi]+fe[i].v;
				if(!inq[v])
				{
					q[dep++]=v;
					inq[v]=1;
				}
			}
		}
	}	
}

void tarjan(int x)
{
	int i,v;
	dfn[x]=low[x]=++sz;
	sta[++tup]=x;ins[x]=1;
	for(i=node[x];i!=-1;i=vi[i].next)
	{
		v=vi[i].to;
		if(!dfn[v])
		{
			tarjan(v);
			low[x]=min(low[x],low[v]);
		}
		else if(ins[v])
			low[x]=min(low[x],dfn[v]);
	}
	if(dfn[x]==low[x])
	{
		++bcnt;
		v=sta[tup--];
		belong[v]=bcnt;
		size[bcnt]++;
		ins[v]=0;
		while(v!=x)
		{
			v=sta[tup--];
			size[bcnt]++;
			belong[v]=bcnt;
			ins[v]=0;
		}
	}
}

bool judge()
{
	int i;
	sz=0;tup=0;bcnt=0;
	for(i=1;i<=n;i++)
		belong[i]=0,dfn[i]=0,low[i]=0,size[i]=0,ins[i]=0;
	for(i=1;i<=n;i++)
		if(!dfn[i])
			tarjan(i);
	for(i=1;i<=bcnt;i++)
		a[i]=0x3f3f3f3f,b[i]=0x3f3f3f3f;
	for(i=1;i<=n;i++)
		a[belong[i]]=min(a[belong[i]],dis[i]);
	fSPFA(n);
	for(i=1;i<=n;i++)
		b[belong[i]]=min(b[belong[i]],fdis[i]);
	for(i=1;i<=bcnt;i++)
		if(size[i]>=2)
			if(a[i]+b[i]<=dis[n]+k)
				return 0;
	return 1;
}

int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	int ti,i,x,y,w;
	ti=read();
	while(ti--)
	{
		n=read(),m=read(),k=read(),mod=read();
		for(i=1;i<=n;i++)
			head[i]=-1,node[i]=-1,he[i]=-1;
		if(k==0)
		{
			for(i=1;i<=m;i++)
			{
				x=read(),y=read(),w=read();
				addedge(x,y,w);
			}	
			SPFA1(1);
			printf("%d\n",cou[n]%mod);
		}
		else
		{
			for(i=1;i<=m;i++)
			{
				x=read(),y=read(),w=read();
				addedge(x,y,w);
				faddedge(y,x,w);
				if(w==0)
					addv(x,y);
			}	
			SPFA(1);
			if(judge())
				SPFA2(1);
			else
				printf("-1\n");
		}
	}
	return 0;
}


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
using namespace std;
#define ll long long

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
} 

int n,len,cou,top;
char si[10005];
string sta[1005];
int li[1005],lo[1005];
int ui[1005];
char opt[105];
string ii;
char xi[1005];
char yi[1005];

bool judge(int x)
{
	int i,len=ii.size();
	if(li[x]!=len)
		return 0;
	else
	{
		for(i=0;i<len;i++)
			if(ii[i]!=sta[x][i])
				return 0;
		return 1;
	}
}

bool comp()
{
	int i,xii=0,yii=0;
	int lx=strlen(xi+1),ly=strlen(yi+1);
	for(i=1;i<=lx;i++)
		xii=xii*10+xi[i]-48;
	for(i=1;i<=ly;i++)
		yii=yii*10+yi[i]-48;
	if(xii>yii)
		return 1;
	else
		return 0;
}

int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int i,j,ti,on1,on2,ans,now;
	ti=read();
	while(ti--)
	{
		scanf("%d",&n);
		scanf("%s",si+1);
		len=strlen(si+1);
		if(si[3]=='1')
			cou=0;
		else if(si[3]=='n')
		{
			cou=0;
			for(i=5;i<len;i++)
				cou=cou*10+si[i]-48;
		}
		ans=0; now=0; on1=0; top=0; on2=0;
		for(i=1;i<=n;i++)
		{
			scanf("%s",opt+1);
			if(opt[1]=='F')
			{
				cin>>ii;
				scanf("%s",xi+1);
				scanf("%s",yi+1);
				for(j=1;j<=top;j++)
					if(judge(j))
						on1=1;
				if(xi[1]=='n')
				{
					if(yi[1]!='n'&&on2==0)
					{
						on2=1;
						ui[top+1]=1;
					}
					ans=max(ans,now);
					top++;
					sta[top]=ii;
					li[top]=ii.size();
					continue;
				}
				else if(xi[1]!='n')
				{
					if(yi[1]=='n'&&on2==0)
						now++,lo[top+1]=1;
					else if(yi[1]!='n'&&on2==0)
					{
						if(comp())
						{
							ui[top+1]=1;
							on2=1;
						}
						lo[top+1]=0;
					}
					ans=max(ans,now);
					top++;
					sta[top]=ii;
					li[top]=ii.size();
				}
			}
			else if(opt[1]=='E')
			{
				if(on2==0)
					now-=lo[top];
				if(ui[top]==1)
					on2=0,ui[top]=0;
				ans=max(ans,now);
				top--;
				if(top<0)
					on1=1;
			}
		}
		if(top>0)
			on1=1;
		if(on1==1)
			printf("ERR\n");
		else if(ans==cou)
			printf("Yes\n");
		else if(ans!=cou)
			printf("No\n");
	}
	return 0;
}


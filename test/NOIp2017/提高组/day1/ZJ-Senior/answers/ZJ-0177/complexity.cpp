#include<stdio.h>
#include<iostream>
#include<string.h>
using namespace std;
char s[20],a[105],b[105];
int T,n,o,i,j,pow,ok,top,mx,id[105],dep[105],h[1005],cnt[105],H[105];
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%s\n",&n,s);
		ok=0;
		for(i='a';i<='z';i++) h[i]=0;
		for(i=0;i<=n;i++) dep[i]=id[i]=cnt[i]=H[i]=0;
		int l=strlen(s);
		pow=0;
		if(s[2]=='n'){
			for(i=4;i<l-1;i++) pow=pow*10+s[i]-'0';
		}
		int X=0,Y=0;top=0;
		for(o=1;o<=n;o++){
			gets(a);l=strlen(a);
			if(ok) continue;
			if(a[0]=='F'){
				if(h[a[2]]) ok=1;
				h[a[2]]=1;X=Y=0;
				for(i=4;i<l;i++){
					if(a[i]==' ') break;
					if(a[i]=='n') X=1e9;else X=X*10+a[i]-'0';
				}
				for(i++;i<l;i++){
					if(a[i]=='n') Y=1e9;else Y=Y*10+a[i]-'0';
				}
				top++;id[top]=o;dep[o]=top;b[top]=a[2];
				if(X==1e9&&Y==1e9) cnt[o]=0;else
				if(X==1e9) H[top]=1;else
				if(Y==1e9) cnt[o]=1;else if(X>Y) H[top]=1;
			}else{
				if(top>0){
					mx=0;
					for(i=id[top]+1;i<o;i++) if(dep[i]==top+1) mx=max(mx,cnt[i]);
					cnt[id[top]]+=mx;
					if(H[top]) cnt[id[top]]=0;
					h[b[top]]=0;H[top]=0;
					top--;
				} else ok=1;
			}
		}
		for(i=1;i<=n;i++) if(dep[i]==1) cnt[0]=max(cnt[0],cnt[i]);
		if(top>0) ok=1;
		if(ok) printf("ERR\n");else
		if(cnt[0]==pow) printf("Yes\n");else printf("No\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

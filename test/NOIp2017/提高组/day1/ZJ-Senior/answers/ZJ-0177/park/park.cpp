#include<stdio.h>
#include<iostream>
using namespace std;
const int N=100005;
int tot,head[N],to[N<<1],Next[N<<1],v[N<<1];
int T,n,m,k,M,i,x,y,z,g[5000005],dis[N],p[N],cnt[N];
void add(int x,int y,int z){
	to[tot]=y;Next[tot]=head[x];v[tot]=z;head[x]=tot++;
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d%d",&n,&m,&k,&M);
		for(i=1;i<=n;i++) head[i]=-1,p[i]=cnt[i]=0;tot=0;
		for(i=1;i<=m;i++){
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		for(i=2;i<=n;i++) dis[i]=1e9;
		int t=0,w=1;g[1]=1;cnt[1]=1%M;
		while(t<w){
			x=g[++t];p[x]=0;
			for(i=head[x];i!=-1;i=Next[i]) if(dis[x]+v[i]<dis[to[i]]){
				dis[to[i]]=dis[x]+v[i];cnt[to[i]]=cnt[x];
				if(p[to[i]]==0){
					p[to[i]]=1;g[++w]=to[i];
				}
			} else if(dis[x]+v[i]==dis[to[i]]) cnt[to[i]]=(cnt[to[i]]+cnt[x])%M;
		}
		printf("%d\n",cnt[n]);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

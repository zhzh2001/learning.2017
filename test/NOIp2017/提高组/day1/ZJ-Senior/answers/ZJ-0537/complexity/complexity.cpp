#include<bits/stdc++.h>
using namespace std;
int t,l,ll,e,x,y,z,maxn,max_,q[110],tail;
string s,tmp;
char d;
bool flag,bj,b[110];
struct azfc
{
	int f,g;
}
a[110];
inline int read()
{
	int num=0;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c>='a'&&c<='z') return 96-c;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num;
}
void guiling()
{
	x=0;
	y=0;
	maxn=0;
	tail=0;
	flag=true;
	memset(b,0,sizeof(b));
	memset(q,0,sizeof(q));
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	t=read();
	for (int i=1;i<=t;++i)
	{
		l=read();
		getline(cin,s);
		ll=s.size();
		guiling();
		bj=true;
		max_=0;
		z=0;
		if (s[2]=='1') z=0;
		else
		{
			for (int j=4;j<ll-1;++j)
			z=z*10+s[j]-48;
		}
		for (int j=1;j<=l;++j)
		{
			d=getchar();
			if (d=='F')
			{
				x++;
				e=read();
				if (b[-e]) bj=false;
				a[-e].f=read();
				a[-e].g=read();
				if (a[-e].g==-14) getline(cin,tmp);
				if ((a[-e].f>0&&a[-e].g>0&&a[-e].f>a[-e].g)||(a[-e].f==-14&&a[-e].g>0)) flag=false;
				b[-e]=true;
				q[++tail]=-e;
			}
			else
			{
				getline(cin,tmp);
				y++;
				if ((a[q[tail]].f>0&&a[q[tail]].g>0&&a[q[tail]].f>a[q[tail]].g)||(a[q[tail]].f==-14&&a[q[tail]].g>0)) flag=true;;
				if (flag&&a[q[tail]].f>0&&a[q[tail]].g==-14) maxn++;
				b[q[tail]]=false;
				q[tail--]=0;
			}
			max_=max(max_,maxn);
			if (x==y) guiling();
		}
		if (bj==false||x!=y)
		{
			printf("ERR\n");
			continue;
		}
		if (z==max_) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

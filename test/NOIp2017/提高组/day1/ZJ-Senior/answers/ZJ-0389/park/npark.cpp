#include <bits/stdc++.h>
using namespace std;
void gi(int &a) {
  a=0;
  int c=getchar();
  while(!(c>='0'&&c<='9')) c=getchar();
  while( (c>='0'&&c<='9')) {
    (a*=10)+=(c-'0'); c=getchar();
  }
}
/*
void gi(int &a, int &b) {gi(a);gi(b);}
void gi(int &a, int &b, int &c) {gi(a);gi(b);gi(c);}
void gi(int &a, int &b, int &c, int &d) {gi(a);gi(b);gi(c);gi(d);}
*/
const int maxn = 111111;
const int maxm = 222222;
const int maxk = 55;
const int inf = 0x3f3f3f3f;
int n,m,k,p;
struct graph {
  int e;
  struct edge {
    int u,v,w;
    bool operator<(const edge &a) const {
      return u<a.u || (u==a.u && v<a.v);
    }
  } es[maxm];
  int beg[maxn];
  void clear() {
    e=0;
  }
  void ae(int x,int y,int z) {
    ++e; es[e] = (edge) {x,y,z};
  }
  void init() {
    sort(es+1,es+e+1);
    beg[0]=0;
    for(int i = 1; i <= n; ++i) {
      beg[i] = beg[i-1];
      while(beg[i] <= e && es[beg[i]].u < i) ++beg[i];
    }
    beg[n+1]=e+1;
  }
} g, ng;
int dis[maxn];
bool vis[maxn];
inline int add(int x,int y) {
  if(x<0||y<0) return -1;
  return (x+y)%p;
}
struct pos {
  int x,w;
  pos(int x,int w):x(x),w(w) {}
  bool operator<(const pos &a) const {
    return w>a.w;
  }
};
priority_queue<pos> q;
void spfa(int s, const graph &g) {
  for(int i = 0; i <= n; ++i) {
    dis[i] = inf; vis[i] = false;
  }
  dis[s]=0;
  q.push(pos(s,dis[s]));
  while(!q.empty()) {
    int x=q.top().x; q.pop();
    if(vis[x]) continue;
    vis[x] = true;
    for(int i = g.beg[x]; i != g.beg[x+1]; ++i) {
      int y = g.es[i].v, w = g.es[i].w;
      if(dis[y] < dis[x] + w) continue;
      dis[y] = dis[x] + w;
      q.push(pos(y,dis[y]));
    }
  }
}
// -1: inf
// -233: on-path
// 0xcfcfcfcf: not visited
int f[maxk][maxn];
int dp(int x,int v) {
  if(v>dis[x]+k) return 0;
  if(x==n&&v==dis[x]) return 1;
  int &ans = f[v-dis[x]][x];
  if(ans == -233) return -1;
  if(ans!=int(0xcfcfcfcf)) return ans;
  ans=-233;
  int res=0;
  for(int i = g.beg[x]; g.es[i].u == x; ++i) {
    int y = g.es[i].v, w = g.es[i].w;
    if(v-w>=dis[y]) res=add(res,dp(y,v-w));
    if(res==-1) break;
  }
  return ans=res;
}

int main() {
  freopen("park.in","r",stdin);
  freopen("park.out","w",stdout);
  int T; gi(T);
  while(T--) {
    g.clear(); ng.clear();
    gi(n); gi(m); gi(k); gi(p);
    for(int i = 1; i <= m; ++i) {
      int u,v,w; gi(u); gi(v); gi(w);
      g.ae(u,v,w);
      ng.ae(v,u,w);
    }
    g.init(); ng.init();
    spfa(n, ng);
    memset(f,0xcf,sizeof(f));
    int res=0;
    for(int i = g.beg[n]; i != g.beg[n+1]; ++i) {
      int y = g.es[i].v, w = g.es[i].w;
      if(w==0 && dis[y]==0) res=-1;
    }
    if(res!=-1) {
      for(int i = 0; i <= k; ++i) {
        res=add(res,dp(1,dis[1]+i));
        if(res==-1) break;
      }
    }
    printf("%d\n", res);
  }
  return 0;
}

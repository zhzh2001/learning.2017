#include <bits/stdc++.h>
using namespace std;
const int maxn = 2333333;
struct graph {
  int e;
  int beg[maxn], nxt[maxn], to[maxn], len[maxn];
  void ae(int x,int y,int w) {
    ++e; nxt[e] = beg[x]; to[e] = y; len[e] = w; beg[x]=e;
  }
} g;
int n,m,k,p;
int dis[maxn];
queue<int> q;
bool inq[maxn];
void spfa(int s, const graph &g) {
  memset(dis,0x3f,sizeof dis);
  q.push(s); inq[s]=true; dis[s]=0;
  while(!q.empty()) {
    int x=q.front(); q.pop();
    inq[x]=false;
    for(int i = g.beg[x]; i; i = g.nxt[i]) {
      int y = g.to[i], w = g.len[i];
      if(dis[y]>dis[x]+w) {
        dis[y]=dis[x]+w;
        if(!inq[y]) {
          inq[y]=true;
          q.push(y);
        }
      }
    }
  }
}
int bound;
int lst[maxn];
int dfs(int x, int l) {
  if(l>bound) return 0;
  if(l==lst[x]) return -1; // inf
  int pl = lst[x];
  lst[x]=l;
  int ans=(x==n);
  for(int i = g.beg[x]; i; i = g.nxt[i]) {
    int y = g.to[i], w = g.len[i];
    int z=dfs(y,l+w);
    if(z==-1) {ans=-1; break;}
    else (ans+=z)%=p;
  }
  lst[x]=pl;
  return ans;
}
int main() {
int T;freopen("park.in","r",stdin);
  cin>>T;
  while(T--) {
    cin>>n>>m>>k>>p;
    for(int i =1;i<=m;++i) {
      int u,v,w; cin>>u>>v>>w;
      g.ae(u,v,w);
    }
    spfa(1,g);
    if(dis[n] >= 0x3f3f3f3e) {
      cout<<0<<endl;
      continue;
      }
    bound = dis[n] + k;
    memset(lst,-233,sizeof lst);
    cout<<dfs(1, 0)<<endl;
  }
}

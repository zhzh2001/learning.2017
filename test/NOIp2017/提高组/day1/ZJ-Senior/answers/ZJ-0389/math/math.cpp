#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstdlib>

using namespace std;
typedef long long ll;
int main(){
  freopen("math.in","r",stdin);
  freopen("math.out","w",stdout);
  ll x,y;
  cin>>x>>y;
  cout<<(x-1)*(y-1)-1<<endl;
  return 0;
}

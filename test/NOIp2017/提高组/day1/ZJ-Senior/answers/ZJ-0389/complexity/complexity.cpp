#include <bits/stdc++.h>
using namespace std;

namespace {
// 0..100 : constval
// 233: n
typedef int val;

struct ci {
  int w;
  ci(int x=0):w(x){}
  ci operator+(const ci &a) const {
    int p=w; if(a.w>p) p=a.w;
    return ci(p);
  }
  ci operator*(const ci &a) const {
    return ci(w+a.w);
  }
  string cpx(){
    if(w==0) return "O(1)";
    else {
      char st[10];
      sprintf(st,"%d", w);
      return "O(n^" + string(st) + ")";
    }
  }
};

int n;
struct sent {
  char c; // 'F' | 'E'
  char var; // one char, != 'n'
  val l,r;
} prog[2333];
bool bramatch() {
  int c=0;
  for(int i = 1; i <= n; ++i) {
    if(prog[i].c == 'F') {
      ++c;
    } else {
      assert(prog[i].c == 'E');
      --c;
    }
    if(c<0) return false;
  }
  if(c!=0) return false;
  return true;
}
bool varUsed[256];
bool varOk(int l,int r) {
  if(l>r) return true;
  int c=0;
  for(int i = l; i <= r; ++i) {
    if(prog[i].c == 'F') {
      ++c;
    } else {
      assert(prog[i].c == 'E');
      --c;
    }
    if(i != r && c==0) {
      return varOk(l,i) && varOk(i+1,r);
    }
  }
  if(varUsed[prog[l].var]) return false;
  varUsed[prog[l].var] = true;
  bool ans=varOk(l+1,r-1);
  varUsed[prog[l].var] = false;
  return ans;
}
string cpx;
val castt(const string &s) {
  if(s[0]=='n') return 233;
  else return strtol(s.c_str(),0,10);
}
ci dfs(int l,int r) {
  if(l>r) return ci(0);
  int c=0;
  for(int i = l; i <= r; ++i) {
    if(prog[i].c == 'F') {
      ++c;
    } else {
      assert(prog[i].c == 'E');
      --c;
    }
    if(i != r && c==0) {
      return dfs(l,i) + dfs(i+1,r);
    }
  }
  int pl = prog[l].l, pr = prog[l].r;
  if(pl > pr) return ci(0);
  // c c
  if(pl <= 100 && pr <= 100) {
    // O(1) * loop
    return ci(0) * dfs(l+1,r-1);
  }
  // c n
  if(pl <= 100 && pr == 233) {
    // O(n) * loop
    return ci(1) * dfs(l+1,r-1);
  }
  // n c
  ; // impossible
  // n n
  if(pl == 233 && pr == 233) {
    return ci(0) * dfs(l+1,r-1);
  }
  return ci(0);
}
}

int main() {
  freopen("complexity.in","r",stdin);
  freopen("complexity.out","w",stdout);
  cin.sync_with_stdio(false); cin.tie(0);
  int T; cin>>T;
  for(int tt = 1; tt <= T; ++tt){
    cin >> n >> cpx;
    for(int i = 1; i <= n; ++i){
      string c; cin>>c;
      if(c[0]=='F') {
        prog[i].c = 'F';
        string var,l,r;
        cin>>var>>l>>r;
        prog[i].var=var[0];
        prog[i].l=castt(l);
        prog[i].r=castt(r);
      } else {
        prog[i].c = 'E';
      }
    }
    if(!bramatch()) {
      goto ERR;
    } else if(!varOk(1,n)) {
      goto ERR;
    } else {
      ci t = dfs(1,n);
      if(t.cpx() != cpx) {
        puts("No");
      } else {
        puts("Yes");
      }
      continue;
    }
  ERR:
    puts("ERR");
  }
}


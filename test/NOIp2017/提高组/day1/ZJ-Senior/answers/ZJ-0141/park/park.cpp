#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<iostream>
#include<string>
using namespace std;
const int maxm=200100,maxn=100100,INF=1e9;
int mx,n,m,t,k,cnt,ans,tot,p,u,v,w,last[maxn],dis[maxn],que[maxn*4],h[maxn],last2[maxn],dis2[maxn];
bool vis[maxn];
struct A{
	int to,pre,val;
}q[maxm];
struct B{
	int to,pre,val;
}q2[maxn];
void dfs(int now,int dist){
	if (now==n){
		if (dist<=mx) ans=(ans+1)%p;
		return;
	}
	for (int i=last[now];i;i=q[i].pre){
		int v=q[i].to; 
		if (dist+q[i].val+dis2[v]<=mx) dfs(v,dist+q[i].val);
	}
}
void solve(){
	tot=cnt=ans=0;
	scanf("%d%d%d%d",&n,&m,&k,&p);
	memset(last,0,sizeof(last));
	memset(last2,0,sizeof(last2));
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		q[++tot].to=v;
		q[tot].val=w;
		q[tot].pre=last[u];
		last[u]=tot;
		q2[++cnt].to=u;
		q2[cnt].val=w;
		q2[cnt].pre=last2[v];
		last2[v]=cnt;
	}
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=n;i++) dis[i]=INF; 
	int l=0,r=1;
	que[1]=1;
	dis[1]=0;
	while (l<r){
		int now=que[++l];
		vis[now]=0;
		for (int i=last[now];i;i=q[i].pre){
			int v=q[i].to;
			if (dis[v]>dis[now]+q[i].val){
				dis[v]=dis[now]+q[i].val;
				if (!vis[v]){que[++r]=v;vis[v]=1;}
			}
		}
	}
	for (int i=1;i<=n;i++) dis2[i]=INF;
	memset(vis,0,sizeof(vis));
	l=0,r=1;
	que[1]=n;
	dis2[n]=0;
	while (l<r){
		int now=que[++l];
		vis[now]=0;
		for (int i=last2[now];i;i=q2[i].pre){
			int v=q2[i].to;
			if (dis2[v]>dis2[now]+q2[i].val){
				dis2[v]=dis2[now]+q2[i].val;
				if (!vis[v]){que[++r]=v;vis[v]=1;}
			}
		}
	}
	mx=dis[n]+k;
	dfs(1,0);
	printf("%d\n",ans);
}
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&t);
	while (t--) solve();
    return 0;
}

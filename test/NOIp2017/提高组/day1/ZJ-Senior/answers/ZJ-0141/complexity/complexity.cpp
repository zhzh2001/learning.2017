#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#include<string>
#include<map>
using namespace std;
const int maxn=110,INF=1e9;
int t1,t2,l,tot,ans,mx,red;
char q[10],i[10],x[10],y[10],c,kind[maxn];
string s[maxn];
bool can[maxn],add[maxn];
map<string,bool> mp;
bool compare(string x,string y){ 
	int lx=x.size(),ly=y.size();
	if (lx>ly) return 1;
	if (lx<ly) return 0;
	for (int i=0;i<lx;i++){
		if (x[i]>y[i]) return 1;
		if (x[i]<y[i]) return 0;
	}
	return 0;
}
void solve(){
	scanf("%d %s",&t2,q);
	red=0;
	if (q[2]=='1') red=0;
	else{
		int now=4;
		while (q[now]>=48&&q[now]<=57){
			red=red*10+q[now]-48;
			now++;
		}
	}
	mx=ans=tot=0;
	bool flag=0,cant=0;
	memset(can,0,sizeof(can));
	while (t2--){
		getchar();
		c=getchar();
		if (c=='F'){
			scanf("%s %s %s",i,x,y);
			if (mp[i]) flag=1;
			mp[i]=1; 
			s[++tot]=i;
			if (cant||flag) {can[tot]=1;continue;}
			if (x[0]=='n') cant=1,can[tot]=1;
			else if (!cant&&(x[0]!='n')&&(y[0]=='n')) ans++,add[tot]=1;  
			else if (compare(x,y)) cant=1,can[tot]=1;
			if (ans>mx) mx=ans;
		}
		else if (c=='E'){
			if (tot>0){
				if (!can[tot-1]) cant=0;
				mp[s[tot]]=0;
				if (add[tot]) ans--,add[tot]=0;
				tot--;
			}
			else flag=1;
		}
	}
	if (tot>0) flag=1;
	if (flag) printf("ERR\n");
	else if (red==mx) printf("Yes\n");
		 else printf("No\n");
	while (tot) mp[s[tot]]=0,add[tot--]=0;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&t1);
	while (t1--) solve();
	return 0;
}


#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;
const int maxn=1000000000;
long long a,b,t,cnt,ans;
bool f[maxn];
int main(){
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	f[a]=1;f[b]=1;
	t=min(a,b);
	ans=t;
	while (1){
		t++;
		if (t-a>0) f[t]|=f[t-a];
		if (t-b>0) f[t]|=f[t-b];
		if (f[t]) cnt++;
		else cnt=0,ans=t;
		if (cnt>=abs(a-b)) break;
	}
	printf("%lld\n",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int inf=0x3f3f3f3f;
int readc()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='^') f=0;ch=getchar();}
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x-f;
}
int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x;
}
int T,Max,top,q[105],L,o,p[30],sl,mm,wr,x1,x2,f;
char s1[105][5],s2[105][5],s3[105][5],s4[105][5];
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	
	T=read();
	while (T--)
	{
		L=read();o=readc();
		memset(p,0,sizeof(p));
		top=0;f=0;
		for (int i=1;i<=L;i++)
		{
			scanf("%s",s1[i]+1);
			if (s1[i][1]=='F') scanf("%s%s%s",s2[i]+1,s3[i]+1,s4[i]+1);
			if (f==1) continue;
		   if (s1[i][1]=='F') 
			{
			  q[++top]=i;
			  if (p[s2[i][1]-'a'+1]) f=1;
			  p[s2[i][1]-'a'+1]=1;
			}else 
			{
			  int now=q[top--];
			  if (top<0) f=1;
			  p[s2[now][1]-'a'+1]=0;
			}
		}
		if (f==1||top!=0) {printf("ERR\n");continue;}
		
		Max=0;mm=0;wr=inf;
		for (int i=1;i<=L;i++)
		{
			if (s1[i][1]=='F')
			{
				if (s3[i][1]!='n') 
				{
					x1=0;sl=strlen(s3[i]+1);
					for (int j=1;j<=sl;j++)
					  x1=x1*10+s3[i][j]-'0';
				}else x1=0;
			   if (s4[i][1]!='n') 
				{
					x2=0;sl=strlen(s4[i]+1);
					for (int j=1;j<=sl;j++)
					  x2=x2*10+s4[i][j]-'0';
				}else x2=0;
				if (x1!=0&&x2==0) q[++top]=1; else q[++top]=0;
				if (x1==0&&x2!=0||x1!=0&&x2!=0&&x1>x2) wr=min(wr,i);
				mm+=q[top];
			}else
			{
				if (top<wr&&mm>Max) Max=mm;
			   mm-=q[top];top--;
			}
		}
		if (Max==o) printf("Yes\n");else printf("No\n");
	}
	
	fclose(stdin);fclose(stdout);
	return 0;
}

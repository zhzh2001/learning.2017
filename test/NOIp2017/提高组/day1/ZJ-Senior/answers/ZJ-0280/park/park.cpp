#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
typedef long long ll;
const int M=200005;
const int N=100005;
const int inf=0x3f3f3f3f;
int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x;
}
int n,k,m,p,cnt,head[N],vis[N],dis[N],lu[N],cn[N],f,T,u,v,w,dp[N][55],ans;
struct node{int to,next,w;}num[M];
struct _node{int i,dis;_node(int a,int b){i=a;dis=b;}};
struct cmp
{
	bool operator () (const _node &A,const _node &B)
	{return A.dis>B.dis;}
};
priority_queue<_node,vector<_node>,cmp> q;
void add(int x,int y,int w)
{num[++cnt].to=y;num[cnt].next=head[x];num[cnt].w=w;head[x]=cnt;}
void dij(int st)
{
	dis[st]=0;q.push(_node(st,0));
	lu[st]=1;
	while (!q.empty())
	{
		_node now=q.top();q.pop();
		if (vis[now.i]) continue;
		vis[now.i]=1;
		for (int i=head[now.i];i;i=num[i].next)
		  if (!vis[num[i].to])
		  {
		  	 if (dis[num[i].to]>now.dis+num[i].w)
		  	   dis[num[i].to]=now.dis+num[i].w,lu[num[i].to]=lu[now.i],q.push(_node(num[i].to,dis[num[i].to]));
		    else if (dis[num[i].to]==now.dis+num[i].w) lu[num[i].to]=((ll)lu[num[i].to]+lu[now.i])%p;
		  }
	}
}
void dfs(int x)
{
	cn[x]++;
	if (cn[x]>k) return;
	if (dis[x]>=inf) return;
	for (int i=head[x];i;i=num[i].next)
	if (dis[num[i].to]<inf)
	{
	  for (int j=k;j>=0;j--)
		 if (dis[num[i].to]+j-num[i].w-dis[x]>=0&&dis[num[i].to]+j-num[i].w-dis[x]<=k) dp[num[i].to][j]=((ll)dp[num[i].to][j]+dp[x][dis[num[i].to]+j-num[i].w-dis[x]])%p;
	  dfs(num[i].to);
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	
	T=read();
	while (T--)
	{
		cnt=0;f=0;
		n=read();m=read();k=read();p=read();
		for (int i=1;i<=n;i++) head[i]=vis[i]=lu[i]=cn[i]=0,dis[i]=inf;
		for (int i=1;i<=m;i++) 
		{
		  u=read(),v=read(),w=read(),add(u,v,w);
		  if (w==0) f=1;	
		}
	   dij(1);
	   if (k==0&&f==0) {printf("%d\n",lu[n]%p);continue;}
	   memset(dp,0,sizeof(dp));
	   dp[1][0]=1;
	   dfs(1);
	   ans=0;
	   for (int i=0;i<=k;i++) ans=((ll)ans+dp[n][i])%p;
	   if (ans==0) printf("-1\n");else printf("%d\n",ans);
	}
	
	fclose(stdin);fclose(stdout);
	return 0;
}

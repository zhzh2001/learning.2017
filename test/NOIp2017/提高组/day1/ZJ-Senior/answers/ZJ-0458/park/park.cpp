#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 500010
#define M 1000010
#define INF 1000000000
using namespace std;
int T,n,m,K,mod,u,v,w,edgenum,Edgenum,p1,p2,ans;
int vet[M],next[M],head[N],len[M],Vet[M],Next[M],Head[N];
int flag[N],dis[N],f[N],q[N],du[N];
void add(int u,int v,int w)
{
	vet[++edgenum]=v;
	next[edgenum]=head[u];
	head[u]=edgenum;
	len[edgenum]=w;
}
void Add(int u,int v)
{
	Vet[++Edgenum]=v;
	Next[Edgenum]=Head[u];
	Head[u]=Edgenum;
}
void spfa()
{
	for (int i=1;i<=n;i++){flag[i]=0;dis[i]=INF;}
	int p1=-1,p2=0;
	flag[1]=1;dis[1]=0;q[0]=1;
	while (p1<p2)
	{
		p1++;int u=q[p1%n];
		flag[u]=0;
		for (int e=head[u];e;e=next[e])
		{
			int v=vet[e];
			if (dis[u]+len[e]<dis[v])
			{
				dis[v]=dis[u]+len[e];
				if (!flag[v]){flag[v]=1;p2++;q[p2%n]=v;}
			}
		}
	}
}
int main()
{
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d%d",&n,&m,&K,&mod);
		memset(head,0,sizeof(head));
		edgenum=0;
		for (int i=1;i<=m;i++)
		{
			scanf("%d%d%d",&u,&v,&w);
			add(u,v,w);
		}
		spfa();
		if (K==0)
		{
			memset(Head,0,sizeof(Head));
			memset(du,0,sizeof(du));
			Edgenum=0;
			for (int i=1;i<=n;i++)
				for (int e=head[i];e;e=next[e])
				{
					int v=vet[e];
					if (dis[i]+len[e]==dis[v]){Add(i,v);du[v]++;}
				}
			memset(f,0,sizeof(f));
			f[1]=1;
			p1=p2=0;
			for (int i=1;i<=n;i++) if (du[i]==0) q[++p2]=i;
			while (p1<p2)
			{
				int u=q[++p1];
				for (int e=Head[u];e;e=Next[e])
				{
					int v=Vet[e];
					f[v]=(f[v]+f[u])%mod;
					du[v]--;if (du[v]==0) q[++p2]=v;
				}
			}
			printf("%d\n",f[n]);
			continue;
		}
		memset(Head,0,sizeof(Head));
		Edgenum=0;
		for (int i=1;i<=n;i++)
			for (int e=head[i];e;e=next[e])
			{
				int v=vet[e];
				for (int t=0;t<=K;t++)
				{
					int w=(i-1)*(K+1)+t,l=dis[i]+t+len[e];
					if (l>=dis[v]&&l<=dis[v]+K)
					{
						Add(w,(v-1)*(K+1)+l-dis[v]);
						du[(v-1)*n+l-dis[v]]++;
					}
				}
			}
		memset(f,0,sizeof(f));
		f[0]=1;
		p1=p2=0;
		for (int i=0;i<=n*(K+1);i++) if (du[i]==0) q[++p2]=i;
		while (p1<p2)
		{
			int u=q[++p1];
			for (int e=Head[u];e;e=Next[e])
			{
				int v=Vet[e];
				f[v]=(f[v]+f[u])%mod;
				du[v]--;if (du[v]==0) q[++p2]=v;
			}
		}
		ans=0;
		for (int i=0;i<=K;i++) ans=(ans+f[(n-1)*(K+1)+i])%mod;
		printf("%d\n",ans);
	}
}

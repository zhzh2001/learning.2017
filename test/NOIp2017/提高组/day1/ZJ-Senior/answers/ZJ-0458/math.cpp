#include<cstdio>
#include<cmath>
using namespace std;
typedef long long ll;
ll a,b,x,y,t,ans;
int flag[10010];
void exgcd(ll a,ll b,ll &x,ll &y)
{
	if (b==0){x=1;y=0;return;}
	exgcd(b,a%b,x,y);
	ll t=x;x=y;y=t-a/b*y;
}
int main()
{
	freopen("math.in","r",stdin);
	freopen("math.out","w",stdout);
	scanf("%lld%lld",&a,&b);
	exgcd(a,b,x,y);
	if (y>0)
	{
		t=y/a;
		y=y-t*a;
		x=x+t*b;
		while (y>0){y-=a;x+=b;}
	}
	ans=a*b;
	while(ceil((double)ans/a*(-y))<=floor((double)ans/b*x)) ans--;
	printf("%lld\n",ans);
	/*ll l=1,r=a*b;
	while (l<r)
	{
		ll mid=(l+r>>1)+1;
		if (ceil((double)b/x*mid)+(double)a/y*mid>(double)a/y+1) l=mid;else r=mid-1;
	}
	ans=(ll)ceil(double(b/x*l));
	printf("%lld\n",ans);*/
	/*for (int i=0;i<=20;i++)
		for (int j=0;j<=20;j++) flag[i*a+b*j]=1;
	for (int i=50;i>=1;i--) if (!flag[i]){printf("%d\n",i);return 0;}*/
}

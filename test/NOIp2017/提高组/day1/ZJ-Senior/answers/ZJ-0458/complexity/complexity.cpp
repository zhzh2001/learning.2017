#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int T,n,top,tmp,ans,ok,l1,l2,x1,x2;
int ch[110],flag[110],stack[110],a[110],b[110];
char s[15],s1[15],s2[15],s3[15];
int calc(int l,int r)
{
	if (l>r) return 0;
	int x=l;int tmp=0;
	while (x<r)
	{
		int y=b[x];
		if (stack[x]!=-2)
			tmp=max(tmp,stack[x]+calc(x+1,y-1));
		x=y+1;
	}
	return tmp;
}
int main()
{
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%s",&n,s);tmp=0;
		if (s[2]=='1') tmp=0;else
		{
			int t=4;
			while (s[t]>='0'&&s[t]<='9'){tmp=tmp*10+s[t]-'0';t++;}
		}
		top=0;
		for (int i=1;i<=n;i++)
		{
			scanf("%s",s);
			if (s[0]=='E')
			{
				stack[++top]=-1;
			}else
			{
				scanf("%s%s%s",s1,s2,s3);
				ch[i]=s1[0]-'a';
				if (s2[0]=='n')
				{
					if (s3[0]=='n') stack[++top]=0;else
					stack[++top]=-2;
				}else
				{
					if (s3[0]=='n') stack[++top]=1;else
					{
						l1=strlen(s2);x1=0;
						for (int j=0;j<l1;j++) x1=x1*10+s2[j]-'0';
						l2=strlen(s3);x2=0;
						for (int j=0;j<l2;j++) x2=x2*10+s3[j]-'0';
						if (x1>x2) stack[++top]=-2;else
						stack[++top]=0;
					}
				}
			}
		}
		top=0;ok=1;
		memset(flag,0,sizeof(flag));
		for (int i=1;i<=n;i++)
		{
			if (stack[i]==-1)
			{
				if (top==0){ok=0;break;}
				flag[ch[a[top]]]=0;
				b[a[top]]=i;top--;
			}else{if (flag[ch[i]]){ok=0;break;}flag[ch[i]]=1;a[++top]=i;}
		}
		if (top>0) ok=0;
		if (!ok){puts("ERR");continue;}
		ans=calc(1,n);
		if (tmp==ans) puts("Yes");else puts("No");
	}
}

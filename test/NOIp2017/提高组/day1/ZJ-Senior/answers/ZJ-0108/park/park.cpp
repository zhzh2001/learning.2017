#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define w1 first
#define w2 second
using namespace std;
const int maxn=100010;
const int maxm=200010;
void r(int &x){
	char c=getchar(); x=0;
	while(c<'0' || c>'9') c=getchar();
	while(c>='0' && c<='9') x=x*10+c-'0',c=getchar();
}
int to[maxm],fr[maxm],len[maxm],nex[maxm],e;
int n,m,k,p;
int lnk[maxn];
vector<int>f[maxn];
vector<pair<int,int> >g[maxn];
vector<pair<int,int> >G[maxn];
void add(int u,int v,int l){
	to[++e]=v; fr[e]=u; len[e]=l; nex[e]=lnk[u]; lnk[u]=e;
}
void Ad(int &x,int y){
	x+=y; if (x>=p) x-=p;
}
int low[maxn],dfn[maxn],sta[maxn],sz[maxn],t=0,cnt=0,times=0,scc[maxn];
void Tarjan(int u){
	low[u]=dfn[u]=++times; sta[++t]=u;
	for (int i=lnk[u]; i; i=nex[i]) if (!len[i]){
		int v=to[i];
		if (!dfn[v]) Tarjan(v),low[u]=min(low[u],low[v]);
		else if (!scc[v]) low[u]=min(low[u],dfn[v]);
	}
	if (dfn[u]==low[u]){
		sz[++cnt]=1;
		scc[sta[t]]=cnt; 
		while (sta[t]!=u) t--,scc[sta[t]]=cnt,sz[cnt]++;
		t--;
	}
}
int que[maxn],dis[maxn],Dis[maxn],inq[maxn];
void spfa(){
	memset(dis,63,sizeof(dis));
	int h=0,t=0; que[t++]=scc[1]; inq[scc[1]]=1; dis[scc[1]]=0; 
	while (h!=t){
		int u=que[h++]; inq[u]=0; if (h==maxn) h=0;
		for (int i=0; i<g[u].size(); i++){
			int v=g[u][i].w1,l=g[u][i].w2;;
			if (dis[u]+l <dis[v]){
				dis[v]=dis[u]+l;
				if (!inq[v]){
					inq[v]=1;
					que[t++]=v;
					if (t==maxn) t=0;
				}
			}
		}
	}
}
void Spfa(){
	memset(Dis,63,sizeof(Dis));
	int h=0,t=0; que[t++]=scc[n]; inq[scc[n]]=1; Dis[scc[n]]=0; 
	while (h!=t){
		int u=que[h++]; inq[u]=0; if (h==maxn) h=0;
		for (int i=0; i<G[u].size(); i++){
			int v=G[u][i].w1,l=G[u][i].w2;;
			if (Dis[u]+l <Dis[v]){
				Dis[v]=Dis[u]+l;
				if (!inq[v]){
					inq[v]=1;
					que[t++]=v;
					if (t==maxn) t=0;
				}
			}
		}
	}
}
int d[maxn],tp[maxn];
void get_graph(){
	memset(d,0,sizeof(d));
	for (int i=1; i<=cnt; i++)
		for (int j=0; j<g[i].size(); j++){
			int v=g[i][j].w1,l=g[i][j].w2;
			if (dis[i]+l == dis[v]) f[i].push_back(v),d[v]++;
		}
}
int dp[maxn][55];
void each_work(int up){
	memcpy(tp,d,sizeof(d));
	int h=0,t=0;
	for (int i=1; i<=cnt; i++) if (!tp[i]) que[t++]=i;
	while (h<t){
		int u=que[h++];
		for (int i=0; i<f[u].size(); i++){
			int v=f[u][i];
			Ad(dp[v][up],dp[u][up]); tp[v]--;
			if (!tp[v]) que[t++]=v;
		}
	}
}
void total_work(int up){
	for (int i=1; i<=cnt; i++)
		for (int j=0; j<g[i].size(); j++){
			int v=g[i][j].w1,l=g[i][j].w2;
			if (dis[i]+l > dis[v] && (up+dis[i]+l-dis[v])<=k ) Ad(dp[v][up+dis[i]+l-dis[v]],dp[i][up]); 
		}
}
void init(){
	memset(scc,0,sizeof(scc));
	memset(lnk,0,sizeof(lnk));
	memset(dfn,0,sizeof(dfn));
	memset(dp,0,sizeof(dp));
	e=times=cnt=t=0;
	for (int i=1; i<=n; i++) g[i].clear(),G[i].clear(),f[i].clear();
}
int T;
int main(){
	freopen("park.in","r",stdin);
	freopen("park.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&n,&m,&k,&p);
		for (int i=1,u,v,w; i<=m; i++) r(u),r(v),r(w),add(u,v,w);
		for (int i=1; i<=n; i++) if (!dfn[i]) Tarjan(i);
		for (int i=1; i<=m; i++)
			if (scc[fr[i]]!=scc[to[i]]) g[scc[fr[i]]].push_back(make_pair(scc[to[i]],len[i])),G[scc[to[i]]].push_back(make_pair(scc[fr[i]],len[i]));
		spfa();
		Spfa();
		bool flag=0;
		for (int i=1; i<=cnt; i++) if (sz[i]>1  && (dis[i]+Dis[i])<=dis[scc[n]]+k) puts("-1"),flag=1;
		if (!flag){
			get_graph();
			dp[scc[1]][0]=1;
			for (int i=0; i<=k; i++) {
				each_work(i);
				total_work(i);
			}
			int ans=0;
			for (int i=0; i<=k; i++) Ad(ans,dp[scc[n]][i]);
			printf("%d\n",ans);	
		}
		init();
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int getvar(){
	char c=getchar();
	while ((c<'0'||c>'9') && c!='n') c=getchar();
	if (c=='n') return 0;
	int x=0;
	while (c>='0' && c<='9') x=x*10+c-'0',c=getchar();
	return x;
}
char getnext(){
	char c=getchar();
	while (c!='E' && c!='F' && (c<'a' || c>'z')) c=getchar();
	return c;
}
int find(char *s){
	int len=strlen(s),flag=0;
	for (int i=0; i<len; i++) if (s[i]=='n') flag=1;
	if (!flag) return 0;
	int x=0;
	for (int i=0; i<len; i++) if (s[i]>='0' && s[i]<='9') x=x*10+s[i]-'0';
	return x;
}
char sta[510];
int vis[510],clex[510],disable[510];
char str[510];
int work(){
	int line;
	scanf("%d",&line);
	scanf("%s",str);
	int cen=find(str),ans=0,t=0,errflag=0;
	memset(vis,0,sizeof(vis));
	for (int i=1; i<=line; i++){
		char c=getnext();
		if (c=='F'){
			char vr=getnext();
			int L=getvar(),R=getvar();
			if (vis[vr]) errflag=1;
			sta[t+1]=vr;
			vis[vr]=1;
			if (disable[t]){
				disable[t+1]=1;
				clex[t+1]=clex[t];
			}else{
				if (L>0 && R>0){
					clex[t+1]=clex[t];
					if (L>R) disable[t+1]=1;
					else disable[t+1]=0;
				}
				if (L==0 && R>0){
					clex[t+1]=clex[t];
					disable[t+1]=1;	
				}
				if (L>0 && R==0){
					clex[t+1]=clex[t]+1;
					disable[t+1]=0;
				}
				if (L==0 && R==0){
					clex[t+1]=clex[t];
					disable[t+1]=0;
				}
			}
			t++;
			if (clex[t]>ans) ans=clex[t];
		}
		if (c=='E'){
			vis[sta[t]]=0;
			t--;
			if (t<0){
				errflag=1;
				t=0;
			}
		}
	}
	if (t || errflag) return -1;
	return ans==cen;
}
int main(){
	freopen("complexity.in","r",stdin);
	freopen("complexity.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) {
		int relt=work();
		if (relt==-1) puts("ERR");
		if (relt==1) puts("Yes");
		if (relt==0) puts("No");
	}
	return 0;
}

#include <cstdio>
#include <iostream>
#define uselesswxy 200000003
using namespace std;
int n,m,i,j,k,s;
long long a[15][15];
long long mincost[15][15];// i as the root the min cost to J
long long dist[15][15];
bool edg[15][15];
long long answxy;
bool visited[15];
int tmp1,tmp2,tmp3;
void getdist(int roots,int now){
	int i;
	for (i=1;i<=n;i++){
		if (a[i][now]==uselesswxy) continue;
		if (visited[i]) continue;
		visited[i]=true;
		dist[roots][i]=dist[roots][now]+a[i][now];
		getdist(roots,i);
	}
}
void dfss(int roots,int now,int dep){
	int i;
	for (i=1;i<=n;i++){
		if (a[i][now]==uselesswxy) continue;
		if (visited[i]) continue;
		visited[i]=true;
		mincost[roots][i]=dep*dist[roots][i];
		mincost[roots][0]+=mincost[roots][i];
		dfss(roots,i,dep+1);
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) {
		a[i][j]=uselesswxy;
		dist[i][j]=0;
	}
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) mincost[i][j]=uselesswxy;
	for (i=1;i<=m;i++){
		scanf("%d%d%d",&tmp1,&tmp2,&tmp3);
		if (a[tmp1][tmp2]>tmp3) a[tmp1][tmp2]=a[tmp2][tmp1]=tmp3;
	}
	for (i=1;i<=n;i++) visited[i]=false;
	if (m==n-1){
		for (i=1;i<=n;i++) {
			visited[i]=true;
			getdist(i,i);
			mincost[i][0]=0;
			for (j=1;j<=n;j++) visited[j]=false;
		}
		for (i=1;i<=n;i++) {
			visited[i]=true;
			dfss(i,i,1);
			for (j=1;j<=n;j++) visited[j]=false;
		}
		answxy=mincost[1][0];
		for (i=2;i<=n;i++) if (mincost[i][0]<answxy) answxy=mincost[i][0];
		printf("%lld",answxy);
		return 0;
	}
	
	s=0;int minx=1,miny=2;
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) edg[i][j]=false;
	while (s<n-1){
		for (i=1;i<n;i++) for (j=1+i;j<=n;j++){
			if ((visited[i])&&(visited[j])) continue;
			if (a[i][j]==uselesswxy) continue;
			if (a[i][j]>=a[minx][miny]) continue;
			minx=i;miny=j;
		}
		visited[minx]=true;visited[miny]=true;
		edg[minx][miny]=true;
		edg[miny][minx]=true;
		s++;
		minx=1;miny=1;
	}
	for (i=1;i<=n;i++) for (j=1;j<=n;j++) if (!edg[i][j]) a[i][j]=uselesswxy;
	for (i=1;i<=n;i++) visited[i]=false;
	for (i=1;i<=n;i++) {
			visited[i]=true;
			getdist(i,i);
			mincost[i][0]=0;
			for (j=1;j<=n;j++) visited[j]=false;
		}
		for (i=1;i<=n;i++) {
			visited[i]=true;
			dfss(i,i,1);
			for (j=1;j<=n;j++) visited[j]=false;
		}
		answxy=mincost[1][0];
		for (i=2;i<=n;i++) if (mincost[i][0]<answxy) answxy=mincost[i][0];
		printf("%lld",answxy);
	return 0;
}

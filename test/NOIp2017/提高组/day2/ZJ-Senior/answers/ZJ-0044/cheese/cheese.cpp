#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;
typedef long long ll;
const double jd = 1e-8;
int n,h,r;
int doubler;
inline ll square(ll x)
{
	return x * x;
}
inline bool pd1(int x1,int y1,int z1,int x2,int y2,int z2)
{
	double t = sqrt(square(x1-x2) + square(y1 - y2) + square(z1 - z2));
	if(t <= doubler)return true;
	return false;
}
inline int abs1(int x)
{
	if(x < 0)return -x;
	return x;
}
inline bool pd2(int h,int z)
{
	if(abs1(z-h) <= r)return true;
	else return false;
}
int father[1123];
int down[1123],up[1123];
int f(int x)
{
	if(father[x] == x)return x;
	father[x] = f(father[x]);
	return father[x];
}
inline void init()
{
	for(int i(1);i <= n;++i)
	{
		father[i] = i;
	}
	down[0] = up[0] = 0;
}
struct ball
{
	int x,y,z;
};
ball b[1123];
int main()
{
	freopen("cheese.in","r+",stdin);
	freopen("cheese.out","w+",stdout);
	int examples;
	scanf("%d",&examples);
	for(int times(1);times <= examples;++times)
	{
		scanf("%d%d%d",&n,&h,&r);
		doubler = r * 2;
		init();
		for(int i(1);i <= n;++i)
		{
			scanf("%d%d%d",&b[i].x,&b[i].y,&b[i].z);
			for(int j(1);j < i;++j)
			{
				if(pd1(b[i].x,b[i].y,b[i].z,b[j].x,b[j].y,b[j].z))
				{
					int fa1 = f(i),fa2 = f(j);
					if(fa1 != fa2)
					{
						father[fa1] = fa2;
					}
				}
			}
			if(pd2(0,b[i].z))
			{
				++down[0];
				down[down[0]] = i;
			}
			if(pd2(h,b[i].z))
			{
				++up[0];
				up[up[0]] = i;
			}
		}
		bool k(false);
		for(int i(1);i <= down[0];++i)
		{
			down[i] = f(down[i]);
		}
		for(int i(1);i <= up[0];++i)
		{
			up[i] = f(up[i]);
		}
		for(int i(1);i <= down[0];++i)
		{
			for(int j(1);j <= up[0];++j)
			{
				if(down[i] == up[j])
				{
					k = true;
					printf("Yes\n");
					break;
				}
			}
			if(k)break;
		}
		if(!k)printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


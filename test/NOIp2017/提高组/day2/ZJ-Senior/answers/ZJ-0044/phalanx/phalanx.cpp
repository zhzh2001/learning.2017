#include <cstdio>
#include <list>
#include <cstdlib>
#include <iostream>
using namespace std;
int n,m,q;
int node[1123][1123];
int askx[412345],asky[412345];
int main()
{
	freopen("phalanx.in","r+",stdin);
	freopen("phalanx.out","w+",stdout);
	scanf("%d%d%d",&n,&m,&q);
	bool allone(true);
	for(int i(1);i <= q;++i)
	{
		scanf("%d%d",&askx[i],&asky[i]);
		if(askx[i] != 1)
		{
			if(n > 10000)
			{
				cout << 233;
				fclose(stdin);
				fclose(stdout);
				return 0;
			}
			allone = false;
		}
	}
	if(n > 10000 && !allone)
	{
		cout << 233;
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	if(n == 1)
	{
		list <int> board;
		for(int i(1);i <= m;++i)board.push_back(i);
		for(int i(1);i <= q;++i)
		{
			int x,y;
			x = askx[i];
			y = asky[i];
			list<int>::iterator itr = board.begin();
			for(int j(1);j < y;++itr,++j);
			board.push_back(*itr);
			printf("%d\n",*itr);
			board.erase(itr);
		}
	}
	else if (n < 10000)
	{
		for(int i(1);i <= n;++i)
		{
			for(int j(1);j <= m;++j)
			{
				node[i][j] = (i-1)*m + j;
			}
		}
		for(int i(1);i <= q;++i)
		{
			int x,y;
			x = askx[i];
			y = asky[i];
			int tmp = node[x][y];
			printf("%d\n",node[x][y]);
			for(int j(y);j <= m;++j)
			{
				node[x][j] = node[x][j+1];
			}
			for(int j(x);j <= n;++j)
			{
				node[j][m] = node[j + 1][m];
			}
			node[n][m] = tmp;
		}
	}
	else if(allone)
	{
		list<int> boardx,boardy;
		for(int i(1);i <= n;++i)
		{
			boardy.push_back(i);
			boardx.push_back(i * n);
		}
		for(int i(1);i <= q;++i)
		{
			int x,y;
			x = askx[i];
			y = asky[i];
			if(y == n)
			{
				printf("%d\n",boardx.front());
				boardx.push_back(boardx.front());
				boardx.pop_front();
			}
			else
			{
				list<int>::iterator itr = boardy.begin();
				for(int j(1);j < y;++j,++itr);
				boardx.push_back(*itr);
				printf("%d\n",*itr);
				boardx.pop_front();
				boardy.push_back(boardx.front());
				boardy.erase(itr);
			}
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

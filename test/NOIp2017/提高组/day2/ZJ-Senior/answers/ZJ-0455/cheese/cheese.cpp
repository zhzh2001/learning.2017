#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll t,n,r,h,f[1005],i,j,ans,hx,hy;
struct ball{
	ll x,y,z;
	bool fd,fu;
}a[1005];
ll get(ll x){
	if(x==f[x])return x;
	f[x]=get(f[x]);
	return f[x];
}
bool sf;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&t);
	for(;;){
		t--;
		sf=0;
		scanf("%lld%lld%lld",&n,&h,&r);
		for(i=1;i<=n;i++)f[i]=i;
		for(i=1;i<=n;i++){
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
			if(a[i].z<=r)a[i].fd=1;
			else a[i].fd=0;
			if(a[i].z>=h-r)a[i].fu=1;
			else a[i].fu=0;
			if(a[i].fd==1&&a[i].fu==1){
				sf=1;
				break;
			}
		}
		for(i=1;i<n;i++)
		for(j=i+1;j<=n;j++){
			ans=r*r-(a[i].x-a[j].x)*(a[i].x-a[j].x)/4-(a[i].y-a[j].y)*(a[i].y-a[j].y)/4-(a[i].z-a[j].z)*(a[i].z-a[j].z)/4;
			if(((a[i].x-a[j].x)%2!=0)||((a[i].y-a[j].y)%2!=0)||((a[i].z-a[j].z)%2!=0))ans--;
			if(ans>=0){
				hx=get(i);hy=get(j);
				if(hx!=hy) f[hx]=hy;
			}
		}
		for(i=1;i<n;i++)
		for(j=i+1;j<=n;j++){
			hx=get(i);hy=get(j);
			if(hx==hy&&((a[i].fd==1&&a[j].fu==1)||(a[j].fd==1&&a[i].fu==1))){
				sf=1;
				break;
			}
		}
		if(sf==1)printf("Yes\n");
		else printf("No\n");
		if(t==0)return 0;
	}
}

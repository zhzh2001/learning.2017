#include <bits/stdc++.h>
using namespace std;
int n,m,fst[20],len,mi=20000000,dep[20];
struct side{
	int y,nxt,c;
}a[205];
int x[20][20],v[20];
bool h[20][20],vis[20];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++)
	if(i!=j)x[i][j]=500005;
	for(int i=1;i<=m;i++){
		int u,v,cc;
		scanf("%d%d%d",&u,&v,&cc);
		h[u][v]=h[v][u]=1;
		x[u][v]=x[v][u]=min(x[u][v],cc);
	}
	for(int i=1;i<n;i++)
	for(int j=i+1;j<=n;j++)
	if(h[i][j]){
		a[++len].nxt=fst[i];
		fst[i]=len;
		a[len].y=j;
		a[len].c=x[i][j];
		a[++len].nxt=fst[j];
		fst[j]=len;
		a[len].y=i;
		a[len].c=x[i][j];
	}
	for(int tou=1;tou<=n;tou++){
		int ans=0;
		for(int i=1;i<=n;i++)v[i]=6000000,vis[i]=0;
		v[tou]=0;
		dep[tou]=0;
		for(int ct=1;ct<=n;ct++){
			int zx=1e9,no;
			for(int i=1;i<=n;i++)if(v[i]<zx&&vis[i]==0)zx=v[i],no=i;
			ans+=zx;
			vis[no]=1;
			for(int i=fst[no];;i=a[i].nxt){
				if(i==0)break;
				int vv=a[i].y;
				if(vis[vv]==0&&(dep[no]+1)*a[i].c<v[vv]){
					v[vv]=(dep[no]+1)*a[i].c;
					dep[vv]=dep[no]+1;
				}
			}
		}
		if(ans<mi)mi=ans;
	}
	printf("%d",mi);
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;

int n, m, q, p[2350100], a[5050][5050], x, y, val[350100], xx[300100], yy[300100];

void Add(int x, int y) {
	for(; x <= m; x += -x & x) val[x] += y;
}

int Query(int x) {
	int ans = 0;
	for(; x; x -= -x & x) ans += val[x];
	return ans;
}

void Read(int &x) {
	x = 0; char ch = getchar();
	while(ch < '0' || ch > '9') ch = getchar();
	while(ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
}

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if(n == 1) {
		for(int i = 1; i <= m; i++)
			p[i] = i;
		for(int i = 1; i <= q; i++) {
			Read(x); Read(y);
			p[++m] = p[y + Query(y)];
			Add(y, 1);
			printf("%d\n", p[m]);
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	bool flg = 0;
	for(int i = 1; i <= q; i++) {
		Read(xx[i]); Read(yy[i]);
		if(xx[i] > 1) flg = 1;
	}
	if(!flg) {
		for(int i = 1; i <= m; i++)
			p[i] = i;
		for(int i = 2; i <= n; i++)
			p[i + m - 1] = i * m;
		m = m + n - 1;
		for(int i = 1; i <= q; i++) {
			int x = xx[i], y = yy[i];
			p[++m] = p[y + Query(y)];
			Add(y, 1);
			printf("%d\n", p[m]);
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			a[i][j] = (i - 1) * m + j;
	for(register int i = 1; i <= q; i++) {
		int x = xx[i], y = yy[i];
		int tmp = a[x][y];
		for(int j = y; j < m; j++)
			a[x][j] = a[x][j + 1];
		for(int j = x; j < n; j++)
			a[j][m] = a[j + 1][m];
		a[n][m] = tmp;
		printf("%d\n", tmp);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

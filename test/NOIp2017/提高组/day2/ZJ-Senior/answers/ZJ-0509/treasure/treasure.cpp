#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;

int n, m, a[13][13], x, y, z, len[13], ans;

void Dfs(int p, int now) {
	if(p == (1 << n) - 1) {
		ans = min(ans, now);
		return;
	}
	for(int i = 0; i < n; i++)
		if(!(p & (1 << i))) {
			int tmp = 0x3f3f3f3f, tmpp;
			for(int j = 0; j < n; j++) if(p & (1 << j)) {
				if(a[i][j] == 0x3f3f3f3f) continue;
				if(len[j] < tmp) {
					tmp = len[j];
					tmpp = a[i][j];
				}
				
			} 
			len[i] = tmp + 1;
			Dfs(p | (1 << i), now + tmp * tmpp);
		}
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(a, 0x3f, sizeof(a));
	for(int i = 0; i < n; i++)
		a[i][i] = 0;
	for(int i = 1; i <= m; i++) {
		scanf("%d%d%d", &x, &y, &z);
		x--; y--;
		a[x][y] = min(a[x][y], z);
		a[y][x] = a[x][y];
	}
	ans = 0x3f3f3f3f;
	for(int i = 0; i < n; i++) {
		len[i] = 1;
		Dfs(1 << i, 0);
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

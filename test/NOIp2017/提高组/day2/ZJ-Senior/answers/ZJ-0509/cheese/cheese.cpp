#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;

int T, n, h, r, fa[1010], x[1010], y[1010], z[1010];

inline long long Sqr(int x) {
	return 1LL * x * x;
}

inline unsigned long long Dis(int i, int j) {
	return Sqr(x[i] - x[j]) + Sqr(y[i] - y[j]) + Sqr(z[i] - z[j]);
}

inline int Ask(int x) {
	return x == fa[x] ? fa[x] : fa[x] = Ask(fa[x]);
}

int main(void) {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &T);
	while(T--) {
		scanf("%d%d%d", &n, &h, &r);
		for(int i = 1; i <= n; i++)
			scanf("%d%d%d", &x[i], &y[i], &z[i]);
		for(int i = 1; i <= n + 2; i++)
			fa[i] = i;
		unsigned long long lim = 4LL * r * r;
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				if(i != j) {
					if(Dis(i, j) <= lim) {
						int fx = Ask(i), fy = Ask(j);
						if(fx != fy) fa[fx] = fy;
					}
				}
		for(int i = 1; i <= n; i++) {
			if(z[i] <= r) {
				int fx = Ask(n + 1), fy = Ask(i);
				if(fx != fy) fa[fx] = fy;
			}
			if((h - z[i] <= r)) {
				int fx = Ask(n + 2), fy = Ask(i);
				if(fx != fy) fa[fx] = fy;
			}
		}
		puts(Ask(n + 1) == Ask(n + 2) ? "Yes" : "No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

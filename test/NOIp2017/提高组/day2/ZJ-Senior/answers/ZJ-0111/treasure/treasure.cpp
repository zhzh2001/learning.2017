#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writen(ll x){write(x),putchar(' ');}
void writeln(ll x){write(x),puts("");}

int n,m;
struct edge
{
	int u,v,w;
	bool operator < (const edge &xxx) const
	{
		return w<xxx.w;
	}
}e[1005];
int fa[25];
bool vis[1005]={0};
int ma[25][25];
int mmp[25][25];
int d[25][25];
int tot;
int sum=0,ggg=0;
int ans=(1<<30);
bool ff=1;

void dfs(int s,int dep)
{
	vis[s]=1;
	for(int i=1; i<=n; i++)
	{
		if(ma[s][i]&&!vis[i])
		{
			sum+=ma[s][i]*dep;
			dfs(i,dep+1);
		}
	}
	vis[s]=0;
}

void dfs2(int s,int dep)
{
	vis[s]=1;
	for(int i=1; i<=n; i++)
	{
		if(mmp[s][i]&&!vis[i])
		{
			sum+=mmp[s][i]*dep;
			dfs(i,dep+1);
		}
	}
	vis[s]=0;
}

int get_fa(int x)
{
	return x==fa[x]?x:fa[x]=get_fa(fa[x]);
}

void merge(int x,int y)
{
	int xx=get_fa(x);
	int yy=get_fa(y);
	fa[yy]=xx;
}

void solve2()
{
	
	ans=(1<<30);
	for(int i=1; i<=n; i++)
		fa[i]=i;
	sort(e+1,e+tot+1);
	for(int i=1; i<=tot; i++)
	{
		int uu=e[i].u,vv=e[i].v;
		if(get_fa(uu)!=get_fa(vv))
		{
			merge(uu,vv);
			mmp[uu][vv]=mmp[vv][uu]=ma[vv][uu];
		}
	}
//	for(int i=1; i<=n; i++)
//		for(int j=i+1; j<=n; j++)
//			if(mmp[i][j])
//				writen(i),writen(j),writeln(ma[i][j]);
	for(int i=1; i<=n; i++)
	{
		memset(vis,0,sizeof vis);
//		writen(sum),writeln(ans);
		sum=0,dfs2(i,1);
		if(sum<ans)
			ans=sum;
	}
	writeln(ans);
	return;
}

void Em(int ee,int num)
{
//	if(num==n-1)
//	{
//		for(int i=1; i<=n; i++)
//	}
//	if(!vis[e[ee].u]&&!vis[e[ee].v])
//	{
//		dfs()
//	}
//	puts("Em");
}
void nv(){}
void zhuang(){}

void init()
{
	for(int i=1; i<=n; i++)
		for(int j=1; j<=n; j++)
			if(ma[i][j])
				d[i][j]=ma[i][j];
			else d[i][j]=1000000;
	for(int k=1; k<=n; k++)
		for(int i=1; i<=n; i++)
			for(int j=1; j<=n; j++)
				if(i!=j&&i!=k&&j!=k)
					d[i][j]=min(d[i][j],d[i][k]+d[j][k]);
//	for(int i=1; i<=n; i++)
//		for(int j=1; j<=n; j++)
//			writen(i),writen(j),writeln(d[i][j]);
}

int main()
{
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for(int i=1,uu,vv,ww; i<=m; i++)
	{
		uu=read(),vv=read(),ww=read();
		if(ma[uu][vv]==0||ww<ma[uu][vv])
		{
			ma[uu][vv]=ma[vv][uu]=ww;
			e[++tot].u=uu,e[tot].v=vv,e[tot].w=ww;
		}
	}
//	writeln(tot);
	if(m==n-1)
	{
		for(int i=1; i<=n; i++)
		{
//			writen(sum),writeln(ans);
			sum=0,dfs(i,1);
			if(sum<ans)
				ans=sum;
		}
		writeln(ans);
		return 0;
	}
	if(n<=8)
	{
		init();
		for(int i=1; i<=n; i++)
		{
			sum=0;
			for(int j=1; j<=n; j++)
				if(i!=j)
					sum+=d[i][j];
			if(sum<ans)
				ans=sum;
//			writen(sum),writeln(ans);
		}
		writeln(ans);
		return 0;
	}
//	Em();
	solve2();
	return 0;
}


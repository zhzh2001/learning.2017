#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writen(ll x){write(x),putchar(' ');}
void writeln(ll x){write(x),puts("");}

struct cheese
{
	ll x,y,z;
	bool operator < (const cheese &xxx) const
	{
		return z<xxx.z;
	}
}c[1005];
bool ma[1005][1005];
bool vis[1005];
ll n,h,r;

ll dis(int xx,int yy)//pingfang
{
	return (c[xx].x-c[yy].x)*(c[xx].x-c[yy].x)+(c[xx].y-c[yy].y)*(c[xx].y-c[yy].y)+(c[xx].z-c[yy].z)*(c[xx].z-c[yy].z);
}

bool dfs(int s)
{
	vis[s]=1;
	if(c[s].z>=h-r) return 1;
	for(int i=1; i<=n; i++)
	{
		if(ma[s][i]&&!vis[i])
		{
			if(dfs(i))
				return 1;
		}
	}
//	vis[s]=0;
	return 0;
}

void solve1()
{
//	writeln(n);
	memset(ma,0,sizeof ma);
	memset(vis,0,sizeof vis);
	for(int i=1; i<=n; i++)
		for(int j=i+1; j<=n; j++)
			if(dis(i,j)<=4*r*r) ma[i][j]=ma[j][i]=1;
	for(int i=1; i<=n; i++)
		if(c[i].z<=r)
		{
			if(dfs(i))
			{
				puts("Yes");
				return;
			}
		}
		else
		{
			puts("No");
			return;
		}
}

void Em()
{
	for(int i=1; i<=n; i++)
		c[i].x/=1000,c[i].y/=1000,c[i].z/=1000;
	h/=1000,r/=1000;
	solve1();
}

int main()
{
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read(),h=read(),r=read();
		for(int i=1; i<=n; i++)
			c[i].x=read(),c[i].y=read(),c[i].z=read();
		if(n*2*r<h){puts("No");continue;}
		sort(c+1,c+n+1);
		if(c[1].z>r||c[n].z<h-r){puts("No");continue;}
		if(n==1){puts("Yes");continue;}
		if(h>=100000000&&r>=100000000){Em();continue;}
		solve1();
	}
	return 0;
}


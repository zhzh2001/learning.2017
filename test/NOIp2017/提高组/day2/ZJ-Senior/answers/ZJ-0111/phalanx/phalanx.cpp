#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

typedef long long ll;
ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writen(ll x){write(x),putchar(' ');}
void writeln(ll x){write(x),puts("");}

int n,m,q;
long long ma[1005][1005];
long long mm[300005];
long long mg[300005];
int x[300005],y[300005];
bool flag=1;

void solve1()//30'
{
	for(int i=1; i<=n; i++)
		for(int j=1; j<=m; j++)
			ma[i][j]=(i-1)*m+j;
	for(int Q=1; Q<=q; Q++)
	{
		int x=read(),y=read();
		int zz=ma[x][y];
		writeln(zz);
		if(Q==q) return;
		for(int j=y; j<m; j++)
			ma[x][j]=ma[x][j+1];
		for(int i=x; i<n; i++)
			ma[i][m]=ma[i+1][m];
		ma[n][m]=zz;
	}
	return;
}

void solve2()//20'
{
	for(int i=1; i<=m; i++)
		mm[i]=i;
	for(int Q=1; Q<=q; Q++)
	{
		int x=read(),y=read();
		int zz=mm[y];
		writeln(zz);
		if(Q==q) return;
		for(int j=y; j<m; j++)
			mm[j]=mm[j+1];
		mm[m]=zz;
	}
}

void solve3()//10'
{
	for(int i=1; i<=m; i++)
		mm[i]=i;
	for(int i=1; i<=n; i++)
		mg[i]=i*m;
	for(int Q=1; Q<=q; Q++)
	{
		int xx=x[Q],yy=y[Q];
		int zz=mm[yy];
		writeln(zz);
		if(Q==q) return;
		for(int j=yy; j<m; j++)
			mm[j]=mm[j+1];
		mm[m]=mg[2];
		for(int i=1; i<n; i++)
			mg[i]=mg[i+1];
		mg[n]=zz;
	}
}

int main()
{
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if(/*q<=500&&*/n<=1000&&m<=1000)
		solve1();
	else if(n==1)
		solve2();
	else
	{
		for(int i=1; i<=q; i++)
		{
			x[i]=read(),y[i]=read();
			if(x[i]!=1) flag=0;
		}
		if(flag) solve3();
	}
	return 0;
}


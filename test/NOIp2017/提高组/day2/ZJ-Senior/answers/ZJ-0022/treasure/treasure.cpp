#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<cstdio>
#include<cmath>
#define N 500005
using namespace std;
int head[N],depth[15],d[5005],tot[N],q1[N];
bool vis[5005];
int n,m,x,y,z;
struct Edge{
	int nxt,to,step;
}e[500005];
int kk=0;
void add(int x,int y,int z)
{
	e[++kk].nxt=head[x];
	e[kk].to=y;
	e[kk].step=z;
	head[x]=kk;
}
void dfs(int u,int fa)
{
	for (int i=head[u];i;i=e[i].nxt){
		int v=e[i].to;
		if (v==fa) continue;
		depth[v]=depth[u]+1;
		dfs(v,u);
	}
}
int solve(int x,int heickr)
{
	int ans1=0;
	depth[x]=0;
	dfs(x,0);
	for (int i=1;i<=n;i++) ans1=ans1+depth[i]*heickr;
	return ans1;
}
void work1()
{
	int ans=1<<30; int vv;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z); vv=z;
		add(x,y,z); add(y,x,z);
	}
	for (int i=1;i<=n;i++) ans=min(ans,solve(i,vv));
	printf("%d\n",ans);
}
struct QUEUE{
	int x,step;
}q[5005];
int solve2(int x,int hellockr)
{
	memset(vis,true,sizeof(vis));
	int left1=1; int right1=1;
	q[left1].x=x; vis[x]=false; q[left1].step=0; d[x]=0;
	while (left1<=right1){
		int u=q[left1].x;
		for (int i=head[u];i;i=e[i].nxt){
			int v=e[i].to;
			if (!vis[v]) continue;
			vis[v]=false;
			d[v]=q[left1].step+1;
			q[++right1].x=v;
			q[right1].step=q[left1].step+1;
		}
		left1++;
	}
	int ans1=0;
	for (int i=1;i<=n;i++) ans1=ans1+d[i]*hellockr;
	return ans1;
}
void work2(int vv)
{
	int ans=1<<30;
	for (int i=1;i<=n;i++) ans=min(ans,solve2(i,vv));
	printf("%d\n",ans);
}
struct Edge1{
	int x,y,z;
}dis[500005];
int solve3(int x)
{
	for (int i=1;i<=n;i++) dis[i].z=1<<30;
	int left1=1; int right1=1;
	q1[left1]=x; dis[x].x=0; dis[x].y=0; dis[x].z=0;
	while (left1<=right1){
		int u=q1[left1];
		for (int i=head[u];i;i=e[i].nxt){
			int v=e[i].to;
			if ((dis[u].x+1)*e[i].step<dis[v].z){
				dis[v].z=(dis[u].x+1)*e[i].step;
				dis[v].x=dis[u].x+1;
				q1[++right1]=v;
			}
		}
		left1++;
	}
	int ans1=0;
	for (int i=1;i<=n;i++) ans1=ans1+dis[i].z;
	return ans1;
}
void work3()
{
	int ans=1<<30;
	for (int i=1;i<=n;i++) ans=min(ans,solve3(i));
	printf("%d\n",ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1) work1();
	else{
		int ckr=0;
		for (int i=1;i<=m;i++){
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z); add(y,x,z); ckr=z;
			tot[z]++;
		}
		bool flag=false;
		for (int i=1;i<=N;i++){
			if (tot[i]==m){
				flag=true;
				break;
			}
		}
		if (flag) work2(ckr);
		else work3();
	}
	return 0;
}

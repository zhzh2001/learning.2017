#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<cstdio>
#include<cmath>
#define N 2005
using namespace std;
long long x[N],y[N],z[N];
int a[N];
int n,T;
long long h,r;
long long heickr(long long x)
{
	return x*x;
}
int find(int x)
{
	if (x==a[x]) return x;
	a[x]=find(a[x]);
	return a[x];
}
void make(int x,int y)
{
	a[find(x)]=find(y);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%lld%lld",&n,&h,&r);//0表示底，n+1表示顶部 
		for (int i=0;i<=n+1;i++) a[i]=i;
		for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=1;i<=n;i++){
			if (z[i]-r<=0) make(0,i);
		}
		for (int i=1;i<=n;i++){
			if (z[i]+r>=h) make(i,n+1);
		}
		for (int i=1;i<n;i++){
			for (int j=i+1;j<=n;j++){
				if ((heickr(x[i]-x[j])+heickr(y[i]-y[j])+heickr(z[i]-z[j]))<=4*r*r) make(i,j);
			}
		}
		if (find(0)==find(n+1)) puts("Yes");
		else puts("No");
	}
	return 0;
}

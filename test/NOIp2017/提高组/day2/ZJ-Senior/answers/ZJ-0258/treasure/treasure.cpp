#include<bits/stdc++.h>

using namespace std;
const int maxn=16;
const int maxm=1000+80;
int n,m,num[maxn][maxn];
struct node{
	int from,to,w;
}edge[maxm],tedge[maxm];
vector<int> v[maxn];
int cnt=0;
int fa[maxn];
inline void add(int x,int y,int z){
	edge[cnt].from=x;
	edge[cnt].to=y;
	edge[cnt].w=z;
	//v[x].push_back(cnt);
	cnt++;
	edge[cnt].from=y;
	edge[cnt].to=x;
	edge[cnt].w=z;
	//v[y].push_back(cnt);
	cnt++;
	return ;
}
bool cmp(int a,int b){
	return edge[a].w<edge[b].w;
}
bool cmp1(node a,node b){
	return a.w<b.w;
}
int getNum(int a,int b){
	vector<int> &vs=v[a];
	if (a==b){
		num[a][b]=1;
		num[b][a]=1;
		return 1;
	}
	if (num[a][b]!=-1)return num[a][b];
	
	int flag=0;
	for (int i=0;i<vs.size();i++){
		if (edge[vs[i]].to==b)flag=1;
	}
	if (flag==1){
		num[a][b]=2;
		num[b][a]=2;
		return 2;
	}else{
        num[a][b]=(1<<15);
		for (int i=0;i<vs.size();i++){
		 	num[a][b]=min(num[a][b],1+getNum(edge[vs[i]].to,b));
		 	 
		
		}
		num[b][a]=num[a][b];
		return num[a][b];
	}
}
inline void init(){
	for (int i=1;i<maxn;i++){
		fa[i]=i;
	}
	return ;
}
int finds(int x){
	if (fa[x]==x)return x;
	int res=finds(fa[x]);
	return fa[x]=res;
}
inline int getTree(){
	int res=0;
	int tot=1;
	for (int i=0;i<2*m&&tot<n;i++){
		int from=tedge[i].from;
		int to=tedge[i].to;
		int w=tedge[i].w;
		if (finds(from)!=finds(to)){
		   res+=w;
		   fa[from]=to;
		   tot++;
		}
	}
	return res;
}
int main(){
	//ios::sync_with_stdio(false);
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<maxn;i++){
		for (int j=0;j<maxn;j++){
			num[i][j]=-1;
		}
	}
	int tmpz=0;
	for (int i=0;i<m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		tmpz=z;
		add(x,y,z);
	}
	sort(edge,edge+m*2,cmp1);
	for(int i=0;i<m*2;i++){
		int from=edge[i].from;
		v[from].push_back(i);
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			getNum(i,j);
			//cout<<"i:"<<i<<" j: "<<j<<" "<<num[i][j]<<endl;
		}
	}
	int vis[maxn];
	int use[maxn];
	int ans=(1<<15);
	int ans2=(1<<15);
	//临时特判 v相等 
	for (int chose=1;chose<=n;chose++){
	    int tmpans=0;
		for (int i=1;i<=n;i++){
			if (i==chose)continue;
			tmpans+=num[chose][i];
		}
		tmpans*=tmpz;
		ans2=min(ans2,tmpans);
	}
	cout<<ans2<<endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
	
	
	//以下正解 
	for (int chose=1;chose<=n;chose++){
		memset(tedge,0,sizeof(tedge));
		init();
		for (int i=0;i<2*m;i++){
			tedge[i].w=edge[i].w*num[chose][edge[i].from];
			tedge[i].from=edge[i].from;
			tedge[i].to=edge[i].to;
			
			
		}
		sort(tedge,tedge+2*m,cmp1);
		ans=min(ans,getTree());
		
	}
	printf("%d",ans);//正解 
	//cout<<ans;
	fclose(stdin);
	fclose(stdout);
	//f close
	return 0;
}


#include<bits/stdc++.h>
#include<cmath>
using namespace std;
const int maxn=1000+17;
int n,h,r;
struct node{
	int x,y,z;
	int num;
}nodes[maxn];
bool can[1005][1005];
int sret[maxn];
typedef struct node ns;
bool cmp(node a,node b){
	return a.z<b.z;
}
inline bool dis(int a,int b){
	int xa=nodes[a].x;
	int xb=nodes[b].x;
	int ya=nodes[a].y;
	int yb=nodes[b].y;
	int za=nodes[a].z;
	int zb=nodes[b].z;
	//cout<<"a: "<<a<<" b: "<<b<<" "<<sqrt((xa-xb)*(xa-xb)+(ya-yb)*(ya-yb)+(za-zb)*(za-zb))<<endl;
	return sqrt((xa-xb)*(xa-xb)+(ya-yb)*(ya-yb)+(za-zb)*(za-zb))<=2*r;
	
}
inline void getdisArr(){
	for (int i=0;i<n;i++){
		for (int j=i+1;j<n;j++){
			can[i][j]=dis(i,j);
			//cout<<"i: "<<i<<" j:"<<j<<" :"<<can[i][j]<<endl;
		}
	}
}
bool search(int num){
	if (sret[num]==2)return true;
	if (sret[num]==1)return false;
	if (nodes[num].z+r>=h){
		sret[num]=2;
		return true;
	}
	for (int i=num+1;i<n;i++){
		if (nodes[i].z-2*r>nodes[num].z){
		//	cout<<nodes[i].z<<" "<<nodes[num].z;
	    //   cout<<"now break"<<endl;
		   break;
		}
		
		if (can[num][i]){
			//cout<<"function in "<<i<<endl;
			if (search(i)){
				sret[num]=2;
				return true;
			}
		}
	}
	sret[num]=1;
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		
		n=0,h=0,r=0;
		memset(nodes,0,sizeof(nodes));
		memset(can,false,sizeof(can));
		memset(sret,0,sizeof(sret));
		scanf("%d%d%d",&n,&h,&r);
		for(int i=0;i<n;i++){
			scanf("%d%d%d",&nodes[i].x,&nodes[i].y,&nodes[i].z);
			
		}
		sort(nodes,nodes+n,cmp);
		
		getdisArr();
		if (nodes[0].z>r||nodes[n-1].z+r<h){
		   //cout<<"No";//更改此处的输出！！
		   printf("No\n"); 
		   //cout<<nodes[0].z<<"   "<<nodes[n-1].z<<endl;
		  continue;
		   
		}
		for (int i=0;i<n;i++){
			if (nodes[i].z<=r){
				//cout<<"search for "<<i<<endl;
				if (search(i)){
					//cout<<"Yes";
					printf("Yes\n");
					break;
					
				}
			}else{
				//cout<<"No";
				printf("No\n");
				 break;
			}
			
		}
		
		
	}
	 fclose(stdin);
     fclose(stdout);
	
	

	return 0;
}


#include<cstdio>
#include<string>
#include<cmath>
using namespace std;
const int maxn=1010;
int T,n,h,r,tot,fa[maxn];
bool vis[maxn];
struct dyt{
	int x,y,z;
}a[maxn];
inline int read(){
	int x=0,flg=1; char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') flg=-1; ch=getchar();}
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*flg;
}
int getfa(int x){if (fa[x]!=x) fa[x]=getfa(fa[x]); return fa[x];}
void merge_(int x,int y){
	int fax=getfa(x),fay=getfa(y);
	if (fax!=fay) fa[fax]=fay;
}
long long getdist(int x,int y){
	return 1ll*(a[x].x-a[y].x)*(a[x].x-a[y].x)+1ll*(a[x].y-a[y].y)*(a[x].y-a[y].y)+1ll*(a[x].z-a[y].z)*(a[x].z-a[y].z);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while (T--) {
		n=read(); h=read(); r=read();
		for (int i=1;i<=n+2;i++) fa[i]=i;
		for (int i=1;i<=n;i++) {
			a[i].x=read(),a[i].y=read(),a[i].z=read();
		}
		for (int i=1;i<=n;i++) {
			if (a[i].z-r<=0&&a[i].z+r>=0) merge_(i,n+1);
			if (a[i].z+r>=h&&a[i].z-r<=h) merge_(i,n+2);
		}
		for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
		if (getdist(i,j)<=4ll*r*r) merge_(i,j);
		int fax=getfa(n+1),fay=getfa(n+2);
		if (fax!=fay) printf("No\n"); else printf("Yes\n");
	}
	return 0;
}

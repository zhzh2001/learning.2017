#include<cstdio>
#include<string>
using namespace std;
const int maxn=1005,maxm=600005;
int n,m,q,tail,a[maxn][maxn],que[maxm],b[maxm];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void put(int x) {for (int i=x;i<=maxm;i+=i&(-i)) que[i]++;}
int get(int x) {int sum=0; for (int i=x;i>0;i-=i&(-i)) sum+=que[i]; return sum;}
int fin(int x){
	int p=x,g=get(p);
	while (p-g<x) p=x+g,g=get(p);
	return p;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if (n==1) {
		for (int i=1;i<=m;i++) b[i]=i; tail=m;
		while (q--) {
			int x=read(),y=read();
			int num=fin(y);
			printf("%d\n",b[num]); b[++tail]=b[num]; b[num]=0; put(num);
		}
	} else{
		for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
		a[i][j]=(i-1)*m+j;
		while (q--) {
			int x=read(),y=read(); int num=a[x][y];
			printf("%d\n",a[x][y]);
			for (int i=y;i<m;i++) a[x][i]=a[x][i+1];
			for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
			a[n][m]=num;
		}
	}
	return 0;
}

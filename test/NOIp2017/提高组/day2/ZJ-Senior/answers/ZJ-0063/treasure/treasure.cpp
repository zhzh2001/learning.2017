#include<cstdio>
#include<string>
#include<cstring>
using namespace std;
const int maxn=15,maxm=2005;
int n,m,ans,tot,lnk[maxn],son[maxm],nxt[maxm],w[maxm],c[maxn],que[maxm];
bool vis[maxn],check;
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void add(int x,int y,int z){
	son[++tot]=y; w[tot]=z; nxt[tot]=lnk[x]; lnk[x]=tot;
}
void bfs(int x){
	memset(vis,0,sizeof(vis)); int sum=0;
	int hed=0,tal=1; vis[x]=1; que[tal]=x; c[x]=0;
	while (hed!=tal) {
		hed++;
		for (int j=lnk[que[hed]];j;j=nxt[j])
		if (!vis[son[j]]) {
			vis[son[j]]=1; sum+=(c[que[hed]]+1)*w[j]; c[son[j]]=c[que[hed]]+1;
			que[++tal]=son[j];
		}
	}
	ans=min(ans,sum);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(); m=read(); ans=1<<30;
	for (int i=1;i<=m;i++) {
		int x=read(),y=read(),z=read();
		add(x,y,z); add(y,x,z);
	}
	for (int i=1;i<=n;i++) bfs(i);
	printf("%d\n",ans);
	return 0;
}

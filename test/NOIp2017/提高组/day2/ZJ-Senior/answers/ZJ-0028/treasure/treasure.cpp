#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<algorithm>
using namespace std;
const int maxn=16;
int x[1005],y[1005],v[1005];
int a[maxn][maxn],q[maxn],front,rear,dist[maxn],f[maxn][maxn];
int n,m,V;
int Ans,res,ret;
bool flag;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	V = -1;
	flag=true;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x[i],&y[i],&v[i]);
		if(V==-1)V=v[i];
		if(V!=v[i]){
			flag=false;
		}
	}
	if(flag){
		memset(a,0,sizeof(a));
		for (int i=1;i<=m;i++){
			a[x[i]][y[i]]=a[y[i]][x[i]]=1;
		}
		Ans=0x3fffffff;
		for(int i=1;i<=n;i++){
			res = 0;
			memset(dist,-1,sizeof(dist));
			dist[i] = 0;
			front=rear;
			q[rear++] = i;
			while(front<rear){
				int u= q[front++];
				res+=dist[u];
				for (int v=1;v<=n;v++){
					if (a[u][v]==0) continue;
					if (dist[v]!=-1) continue;
					dist[v]=dist[u]+1;
					q[rear++]=v;
				}
			}
			Ans=min(Ans,res);
		}
		printf("%d\n",V*Ans);
	} else{
		memset(a,-1,sizeof(a));
		for (int i=1;i<=m;i++){
			if(a[x[i]][y[i]]==-1) a[x[i]][y[i]]=v[i];
			else a[x[i]][y[i]]=min(a[x[i]][y[i]],v[i]);
			a[y[i]][x[i]]=a[x[i]][y[i]];
		}
		Ans=0x3fffffff;
		for(int i=1;i<=n;i++){
			memset(f,-1,sizeof(f));
			f[0][i] = 0;
			for (int j=0;j<n;j++){
				for (int k=1;k<=n;k++){
					if(f[j][k]==-1) continue;
					for (int l=1;l<=n;l++){
						if(a[k][l]==-1) continue;
						if (f[j+1][l]==-1)	f[j+1][l]=f[j][k] + a[k][l]*(j+1);
						else				f[j+1][l] = min(f[j+1][l],f[j][k] + a[k][l]*(j+1));
					}
				}
			}
			res = 0;
			for(int i=1;i<=n;i++){
				ret = 0x3fffffff;
				for(int j=0;j<=n;j++){
					if(f[j][i]==-1) continue;
					ret = min(ret,f[j][i]);
				}
				res+=ret;
			}
			Ans=min(Ans,res);
		}
		printf("%d\n",Ans);
	}
	return 0;
}

#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
const int maxN = 1005;
const double eps = 1e-7;
bool dist[maxN][maxN],vis[maxN];
int q[maxN],front,rear,x[maxN],y[maxN],z[maxN];
int n,h,r;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		memset(dist,false,sizeof(dist));
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if(z[i]+r>=h&&z[i]-r<=h){
				dist[i][n+1] = dist[n+1][i] = 1;
//				printf("!%d %d\n",i,n+1);
			}
			if(z[i]+r>=0&&z[i]-r<=0){
				dist[i][0] = dist[0][i] = 1;
			}
			int tx,ty,tz;
			double Dis;
			for(int j=1;j<i;j++){
				tx = x[i]-x[j];
				ty = y[i]-y[j];
				tz = z[i]-z[j];
				Dis = sqrt(1.0*tx*tx+1.0*ty*ty+1.0*tz*tz);
	//			printf("%lf\n",Dis);
				if (2*r-Dis>-eps) dist[i][j]=dist[j][i]=1;
			}
		}
		memset(vis,false,sizeof(vis));
		vis[0]=true;
		front = rear = 0;
		q[rear++]=0;
		while(front<rear){
			int i=q[front++];
//			printf("%d=\n",i);
			for(int j=1/*0*/;j<=n+1;j++){
				if(dist[i][j]&&!vis[j]){
//					printf("%d %d\n",i,j);
					vis[j]=true;
					q[rear++]=j;
				}
			}
		}
		if(vis[n+1]){
			puts("Yes");
		} else
			puts("No");
	}
	return 0;
}
/*
3
2 4 1
0 0 1
0 0 3
2 5 1
0 0 1
0 0 4
2 5 2
0 0 2
2 0 4
CLJAK!
*/

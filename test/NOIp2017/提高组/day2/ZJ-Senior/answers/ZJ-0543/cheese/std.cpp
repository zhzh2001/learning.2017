#include<map>
#include<set>
#include<queue>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
static const int M = 1005;
int x[M],y[M],z[M];
vector<int>es[M];
int que[M];
bool vis[M];
void addedge(int u,int v){
	es[u].push_back(v);
	es[v].push_back(u);
}
void dfs(int u){
	vis[u]=true;
	for(int i=0,v;i<es[u].size();++i){
		v=es[u][i];
		if(!vis[v])dfs(v);
	}
}
void solve(int n,int H,int r){
	//0 为下表面(z=0)，n+1为上表面(z=H)
	for(int i=0;i<=n+1;++i)es[i].clear();
	for(int i=1;i<=n;++i){
		if(abs(z[i]-0)<=r)addedge(0,i);
		if(abs(z[i]-H)<=r)addedge(i,n+1);
	}
	ll dx,dy,dz;
	for(int i=1;i<=n;++i){
		for(int j=i+1;j<=n;++j){
			dx=x[i]-x[j],dy=y[i]-y[j],dz=z[i]-z[j];
			if(1ll*dx*dx+1ll*dy*dy+1ll*dz*dz<=4ll*r*r){
				addedge(i,j);
			}
		}
	}
	memset(vis,0,sizeof(vis));
	dfs(0);
	if(vis[n+1])puts("Yes");
	else puts("No");
}
//文件名 ll mod negative 
int main(){
//	freopen("cheese.in","r",stdin);
//	freopen("std.out","w",stdout);
	int T,n,H,r;
	scanf("%d",&T);
	for(int cas=1;cas<=T;++cas){
		scanf("%d%d%d",&n,&H,&r);
		for(int i=1;i<=n;++i)scanf("%d%d%d",x+i,y+i,z+i);
		solve(n,H,r);
	}
	return 0;
}

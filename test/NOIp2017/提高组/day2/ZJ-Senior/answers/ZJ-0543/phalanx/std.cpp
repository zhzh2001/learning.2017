#include<map>
#include<set>
#include<queue>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
#define uint unsigned int
static const int M = (int)3e5+5;
void rd(int &res){
	res=0;char c;
	while(c=getchar(),c<'0'||c>'9');
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),'0'<=c&&c<='9');
}
template <class T>
void print(T x){
	if(x){
		print(x/10);
		putchar(x%10^48);
	}
}
template <class T>
void pf(T x){
	print(x);
	if(!x)putchar('0');
	putchar('\n');
}
struct Quer{
	int x,y;
}Q[M];
struct Water{
	static const int MM = 1005;
	int A[MM][MM];
	void Move(int x,int y,int n,int m){
		int cur=A[x][y];
		for(int i=y;i<m;++i)A[x][i]=A[x][i+1];
		for(int i=x;i<n;++i)A[i][m]=A[i+1][m];
		A[n][m]=cur;
	}
	void solve(int n,int m,int q){
		int id_tot=0;
		for(int i=1;i<=n;++i)
			for(int j=1;j<=m;++j)A[i][j]=++id_tot;
		for(int cas=1,x,y;cas<=q;++cas){
			x=Q[cas].x,y=Q[cas].y;
			printf("%d\n",A[x][y]);
			Move(x,y,n,m);
		}
	}
}P1;
struct Medium{//n<=50000&&m<=50000&&q<=500
	static const int MAXQ = 505;
	static const int MAXN = 50005;
	int dx[M];
	uint col[MAXN];
	uint row[MAXQ][MAXN];
	void init(int n,int m,int q){
		int tx=0;
		for(int i=1;i<=q;++i)dx[++tx]=Q[i].x;
		sort(dx+1,dx+tx+1);
		tx=unique(dx+1,dx+tx+1)-dx-1;
		for(int i=1;i<=q;++i)Q[i].x=lower_bound(dx+1,dx+tx+1,Q[i].x)-dx;
		for(int i=1;i<=tx;++i){
			row[i][1]=1ll*(dx[i]-1)*m+1;
			for(int j=2;j<m;++j)row[i][j]=row[i][j-1]+1;
		}
		for(int i=1;i<=n;++i)col[i]=1ll*i*m;
	}
	void Move(int x,int y,int n,int m){
		uint cur;
		if(y<m){
			cur=row[x][y];
			for(int i=y;i<m-1;++i)row[x][i]=row[x][i+1];
			row[x][m-1]=col[x];
		}else cur=col[x];
		for(int i=x;i<n;++i)col[i]=col[i+1];
		col[m]=cur;
	}
	void solve(int n,int m,int q){
		init(n,m,q);
		for(int cas=1,x,y;cas<=q;++cas){
			x=Q[cas].x,y=Q[cas].y;
			if(y<m)pf(row[x][y]);
			else pf(col[x]);
			Move(x,y,n,m);
		}
	}
}P2;
//�ļ��� ll mod negative 
int main(){
//	freopen("phalanx.in","r",stdin);
//	freopen("phalanx.out","w",stdout);
	int n,m,q;
	rd(n),rd(m),rd(q);
	bool AllOne=true;
	for(int i=1;i<=q;++i){
		rd(Q[i].x),rd(Q[i].y);
		AllOne&=(Q[i].x==1);
	}
	P1.solve(n,m,q);
	return 0;
}

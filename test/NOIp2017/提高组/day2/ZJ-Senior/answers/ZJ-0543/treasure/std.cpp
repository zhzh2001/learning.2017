#include<map>
#include<set>
#include<queue>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long
static const int M = 20;

int dis[M][M];
void chkmi(int &a,int b){
	if(a==-1||a>b)a=b;
}
struct ETREE{
	int n;
	int sum[M],sz[M];
	void dfs(int u,int fa){
		for(int v=1;v<=n;++v)if(v!=u&&~dis[u][v]){
			if(v==fa)continue;
			dfs(v,u);
			sum[u]+=sum[v];
			sz[u]+=sz[v];
			sz[u]+=dis[u][v];
		}
		sum[u]+=sz[u];
	}
	void solve(int _n,int m){
		n=_n;
		int ans=-1;
		for(int i=1;i<=n;++i){
			memset(sum,0,sizeof(sum));
			memset(sz,0,sizeof(sz));
			dfs(i,0);
			chkmi(ans,sum[i]);
		}
		printf("%d\n",ans);
	}
}P1;
struct Water{
	int p[M],dep[M];
	void solve(int n,int m,int c){
		for(int i=1;i<=n;++i)p[i]=i;
		int ans=-1;
		do{
			int sum=0;
			bool flag=true;
			memset(dep,-1,sizeof(dep));
			dep[p[1]]=1;
			for(int i=2,u,mi;i<=n;++i){
				u=p[i],mi=-1;
				for(int j=1,v;j<i;++j){
					v=p[j];
					if(~dis[u][v])chkmi(mi,dep[v]);
				}
				if(~mi){
					dep[u]=mi+1;
					sum+=mi*c;
				}
				else{
					flag=false;
					break;
				}
			}
			if(flag)chkmi(ans,sum);
		}while(next_permutation(p+1,p+n+1));
		printf("%d\n",ans);
	}
}P2;
struct Medium{
	static const int MAXN = 16777216+600;
	int dp[MAXN];
	int que[MAXN];
	int dep[M];
	int Not[M],Has[M];
	int p[M];
	int Chain(int n){
		int ans=-1;
		for(int i=1;i<=n;++i)p[i]=i;
		do{
			int sum=0;
			bool flag=true;
			memset(dep,-1,sizeof(dep));
			dep[p[1]]=1;
			for(int i=2,u,v;i<=n;++i){
				u=p[i],v=p[i-1];
				if(~dep[v]&&~dis[u][v]){
					sum+=dep[v]*dis[u][v];
					dep[u]=dep[v]+1;
				}else{
					flag=false;
					break;
				}
			}
			if(flag)chkmi(ans,sum);
		}while(next_permutation(p+1,p+n+1));
		return ans;
	}
	void solve(int n,int m){
		int ans=-1;
		int tmp=Chain(n);
//		printf("%d\n",tmp); 
		if(~tmp)chkmi(ans,tmp);
		memset(dp,-1,sizeof(dp));
		int L=0,R=0,mask;
		for(int i=1;i<=n;++i){
			que[R++]=1<<(3*i-3);
			dp[1<<(3*i-3)]=0;
		}
		int tot=0,top=0,New;
		while(L<R){
			mask=que[L++],top=tot=0;
//			printf("mask=%d\n ",mask);
			for(int i=1,x=mask;i<=n;x>>=3,++i){
				if(x&7){
					dep[i]=x&7;
					Has[++top]=i;
				}else Not[++tot]=i;
			}
//			printf("Has Got: ");
//			for(int i=1;i<=top;++i)printf("(dep[%d]=%d) ",Has[i],dep[Has[i]]);
//			puts("");
//			for(int i=1;i<=tot;++i)printf("%d ",Not[i]);
//			puts("");
			for(int i=1,u,v;i<=tot;++i){
				u=Not[i];
				for(int j=1;j<=top;++j){
					v=Has[j];
					if(~dis[u][v]&&dep[v]<7){//dep[v]=7
//					printf("u=%d,v=%d,dep=%d,del=%d\n",u,v,dep[v],(dep[v]+1)<<(3*(u-1)));
						New=mask+((dep[v]+1)<<(3*(u-1)));
//						if(New==24)puts("!");
						if(~dp[New])chkmi(dp[New],dp[mask]+dep[v]*dis[u][v]);
						else{
							dp[New]=dp[mask]+dep[v]*dis[u][v];
							que[R++]=New;
						}
					}
				}
			}
			if(!tot)chkmi(ans,dp[mask]);
		}
		printf("%d\n",ans);
	}
}P3;
//�ļ��� ll mod negative 
int main(){
//	freopen("treasure.in","r",stdin);
//	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	memset(dis,-1,sizeof(dis));
	for(int i=1;i<=n;++i)dis[i][i]=0;
	bool Same=true;
	int x=-1;
	for(int i=1,u,v,c;i<=m;++i){
		scanf("%d%d%d",&u,&v,&c);
		chkmi(dis[u][v],c);
		chkmi(dis[v][u],c);
		if(x==-1)x=c;
		else Same&=(x==c);
	}
	if(m==n-1)P1.solve(n,m);
	else P2.solve(n,m,x);
	return 0;
}

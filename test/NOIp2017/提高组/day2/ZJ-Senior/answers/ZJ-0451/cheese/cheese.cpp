#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<cctype>
#include<string>
#include<cstdlib>
#include<climits>
#include<cmath>
#include<vector>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	x*=f;
}
const int maxn=2010;
vector<int>G[maxn];
ll x[maxn],y[maxn],z[maxn],r;
int T,n,h,s,t;
bool vis[maxn];
inline ll sqr(ll x) {
	return x*x;
}
inline ll dis(ll a,ll b,ll c,ll x,ll y,ll z) {
	return sqr(a-x)+sqr(b-y)+sqr(c-z);
}
inline bool dfs(int u) {
	if(u==t)
		return 1;
	bool ans=0;
	for(unsigned i=0;i<G[u].size();i++) {
		int v=G[u][i];
		if(vis[v])
			continue;
		vis[v]=1;
		ans|=dfs(v);
	}
	return ans;
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--) {
		read(n);read(h);read(r);
		s=n+1;t=n+2;
		for(int i=1;i<=n+2;i++) {
			G[i].clear();
			vis[i]=0;
		}
		for(int i=1;i<=n;i++) {
			read(x[i]),read(y[i]),read(z[i]);
			if(abs(z[i])<=r) {
				G[s].push_back(i);
				G[i].push_back(s);
			}
			if(z[i]+r>=h) {
				G[t].push_back(i);
				G[i].push_back(t);
			}
		}
		for(int i=1;i<=n;i++) {
			for(int j=i+1;j<=n;j++) {
				if(abs(x[i]-x[j])>2*r||abs(y[i]-y[j])>2*r||abs(z[i]-z[j])>2*r)
					continue;
				ll dist=dis(x[i],y[i],z[i],x[j],y[j],z[j]);
//				cout<<"Now = "<<i<<" "<<j<<" Distance = "<<dist<<" "<<sqr(2*r)<<endl;
				if(dist<=sqr(2*r)) {
					G[i].push_back(j);
					G[j].push_back(i);
				}
			}
		}
		vis[s]=1;
		if(dfs(s))
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}

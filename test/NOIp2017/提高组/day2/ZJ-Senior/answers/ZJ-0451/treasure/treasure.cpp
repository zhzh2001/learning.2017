#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<cctype>
#include<string>
#include<cstdlib>
#include<climits>
#include<cmath>
#include<vector>
#include<queue>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	x*=f;
}
const int maxn=20;
const ll oo=0x3f3f3f3f3f3f3f3fLL;
int n,m,Edgecnt,dep[maxn];
ll dis[maxn][maxn],dist[maxn];
namespace tree {
	ll ans=oo;
	queue<int>q;
	int dep[maxn];
	inline void solve() {
		for(int i=1;i<=n;i++) {
			ll tot=0;
//			cout<<"Case #"<<i<<endl;
			while(!q.empty())
				q.pop();
			memset(dep,-1,sizeof dep);
			q.push(i);
			dep[i]=0;
			while(!q.empty()) {
				int now=q.front();
//				cout<<now<<" "<<endl;
				q.pop();
				for(int j=1;j<=n;j++) {
					if(dis[now][j]==oo||dep[j]!=-1)
						continue;
					dep[j]=dep[now]+1;
					tot+=(ll)dep[j]*dis[now][j];
					q.push(j);
				}
			}
			ans=min(ans,tot);
		}
		printf("%lld\n",ans);
	}
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
//	freopen("B.in","r",stdin);
	read(n);read(m);
	memset(dis,0x3f,sizeof dis);
	for(int i=1;i<=n;i++)
		dis[i][i]=0;
	bool subtask=1;
	int sub=0;
	for(int i=1;i<=m;i++) {
		int u,v;ll w;
		read(u);read(v);read(w);
		if(dis[u][v]==oo)
			Edgecnt++;
		if(i==1)
			sub=w;
		else
			subtask&=(sub==w);
		dis[u][v]=dis[v][u]=min(dis[u][v],w);
//		cout<<Edgecnt<<" "<<sub<<" "<<subtask<<" "<<u<<" "<<v<<" "<<w<<endl;
	}
	if(Edgecnt==n-1||subtask) {
//		cerr<<"Use subtask!"<<endl;
		tree::solve();
		return 0;
	}
	ll ansn=oo;
	for(int i=1;i<=n;i++) {
		memset(dist,0x3f,sizeof dist);
		memset(dep,0,sizeof dep);
		ll sum=0;
		dep[i]=1;
		dist[i]=-1;
		int cnt=1;
		for(int j=1;j<=n;j++)
			if(i!=j) {
				if(dist[j]>dis[i][j]*dep[i]) {
					dist[j]=dis[i][j]*dep[i];
					dep[j]=dep[i]+1;
				}
			}
		while(cnt<n) {
			ll tot=oo;
			int idx=-1;
			for(int j=1;j<=n;j++) {
				if(dist[j]!=-1&&dist[j]<tot) {
					idx=j;
					tot=dist[j];
				}
			}
			sum+=tot;cnt++;
			dist[idx]=-1;
			for(int j=1;j<=n;j++) {
				if(dist[j]==-1)
					continue;
				if(dist[j]>dis[idx][j]*dep[idx]) {
					dist[j]=dis[idx][j]*dep[idx];
					dep[j]=dep[idx]+1;
				}
			}
		}
		ansn=min(ansn,sum);
	}
	printf("%lld\n",ansn);
	return 0;
}

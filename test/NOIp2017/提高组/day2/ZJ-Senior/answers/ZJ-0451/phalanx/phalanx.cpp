#include<cstdio>
#include<algorithm>
#include<iostream>
#include<cstring>
#include<cctype>
#include<string>
#include<cstdlib>
#include<climits>
#include<cmath>
#include<vector>
using namespace std;
typedef long long ll;
template<class T>inline void read(T &x) {
	x=0;T f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;ch=getchar();}
	while(isdigit(ch)){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	x*=f;
}
ll n,m,q,x,y;
namespace Bruteforce {
	ll cub[1005][1005];
	inline void solve() {
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				cub[i][j]=(i-1)*m+j;
		while(q--) {
			read(x);read(y);
			printf("%lld\n",cub[x][y]);
			ll tmp=cub[x][y];
			for(int j=y;j<m;j++)
				cub[x][j]=cub[x][j+1];
			for(int i=x;i<n;i++)
				cub[i][m]=cub[i+1][m];
			cub[n][m]=tmp;
//			for(int i=1;i<=n;i++,putchar('\n'))
//				for(int j=1;j<=m;j++,putchar(' '))
//					cout<<cub[i][j];
		}
	}
}
namespace BIT {
	#define lb(x) x&-x
	const int maxn=300010;
	int num[maxn],p[maxn+maxn],top;
	inline void add(int pos,int d) {
		for(;pos<=top;pos+=lb(pos))
			num[pos]+=d;
	}
	inline int query(int pos) {
		int res=0;
		for(;pos;pos-=lb(pos))
			res+=num[pos];
		return res;
	}
	inline void solve() {
		for(int i=1;i<=m;i++)
			p[i]=i;
		top=m;
		while(q--) {
			read(x);read(y);
			int dif=query(top)-query(top-(m-y)-1),po=top-(m-y)-dif,ans=p[po];
//			cout<<y<<" "<<dif<<" "<<top<<" "<<(m-y)<<" "<<po<<endl;
			printf("%d\n",ans);
			p[++top]=ans;
			add(po,1);
		}
	}
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	if(n<=1000&&m<=1000) {
		Bruteforce::solve();
		return 0;
	}
	if(n==1) {
		BIT::solve();
		return 0;
	}
	
}

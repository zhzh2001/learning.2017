#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=0;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=1005;
const double eps=1e-8;
int a[N][N];
queue<int>q;
bool vis[N];
int n,h,r;
ll x[N],y[N],z[N];
inline ll sqr(ll x)
{
	return x*x;
}
inline bool clac(int a,int b)
{
	if(abs(x[a]-x[b])>r*2||abs(y[a]-y[b])>r*2||abs(z[a]-z[b])>r*2)return false;
	return (r*2.0+eps>=sqrt(sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b])));
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--){
		n=read();h=read();r=read();
		memset(vis,0,sizeof(vis));
		for(int i=1;i<=n;i++){
			x[i]=read();y[i]=read();z[i]=read();
		}
		while(!q.empty())q.pop();
		for(int i=1;i<=n;i++){
			if(z[i]-r<=0)q.push(i),vis[i]=true;
			for(int j=i+1;j<=n;j++){
				if(clac(i,j))a[i][j]=a[j][i]=1;
				else a[i][j]=a[j][i]=0;
			}
		}
		bool bb=false;
		while(!q.empty()){
			int k=q.front();
			if(z[k]+r>=h){
				bb=true;
				puts("Yes");
				break;
			}
			q.pop();
			for(int i=1;i<=n;i++)
				if(!vis[i]&&a[k][i])q.push(i),vis[i]=true;
		}
		if(!bb)puts("No");
	}
	return 0;
}

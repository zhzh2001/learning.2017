#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=0;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=15;
int dis[N];
int a[N][N];
int x,y,z,t,ans,n,m;
bool vis[N];
void dfs(int k,int now)
{
	if(now>ans)return;
	if(k==n){
		if(now<ans)ans=now;
		return;
	}
	for(int i=1;i<=n;i++)
		if(dis[i]<n)
		for(int j=1;j<=n;j++){
			if(a[i][j]!=-1&&dis[j]>n){
				dis[j]=dis[i]+1;
				dfs(k+1,dis[j]*a[i][j]+now);
				dis[j]=n+1;
			}
		}
}
int tanxin(int k)
{
	dis[k]=0;
	int t[N];
	memset(t,127/3,sizeof(t));
	memset(vis,0,sizeof(vis));
	t[k]=0;
	for(int i=1;i<=n;i++)
		dis[i]=1e9;
	dis[k]=0;
	int ret=0;
	for(int i=1;i<=n;i++){
		int k=-1;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&(k==-1||dis[j]<dis[k]))
				k=j;
		vis[k]=1;
		ret+=dis[k];
		for(int j=1;j<=n;j++)
			if(a[k][j]!=-1&&(t[k]+1)*a[k][j]<dis[j]){
				t[j]=t[k]+1;
				dis[j]=t[j]*a[k][j];
			}
	}
	return ret;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	memset(a,-1,sizeof(a));
	for(int i=1;i<=m;i++){
		x=read();y=read();z=read();
		if(x>y)swap(x,y);
		if(a[x][y]!=-1)a[x][y]=min(a[x][y],z);
		else a[x][y]=z;
		a[y][x]=a[x][y];
	}
	ans=1e9;
	for(int i=1;i<=n;i++)
		ans=min(ans,tanxin(i));
	if(n<9){
		for(int i=1;i<=n;i++)
			dis[i]=n+1;
		for(int i=1;i<=n;i++){
			dis[i]=0;
			dfs(1,0);
			dis[i]=n+1;
		}
	}
	writeln(ans);
	return 0;
}


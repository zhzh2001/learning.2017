#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define lowbit(x) (x&-x)
inline ll read(){ll x=0,f=1;char ch=0;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-48;return x*f;}
inline ll write(ll x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar('0'+x%10);}
inline ll writeln(ll x){write(x);puts("");}
const int N=50005;
int f[N];
int n,m,q;
int a[1005][1005];
int tot;
int t[1000005];
int x[505],y[505];
int cnt[505];
int z[50005];
int h[505][50005];
map<int,int>mp;
inline void addto(int k){for(int i=k;i<=tot;i+=lowbit(i))f[i]++;}
inline int query(int k){ll ret=0;for(int i=k;i;i-=lowbit(i))ret+=f[i];return ret;}
void justdoit()
{
	int cnt=0;
	for(int i=1;i<=q;i++)
		x[i]=read(),y[i]=read();
	for(int i=1;i<=q;i++)
		if(!mp[x[i]]){
			mp[x[i]]=++cnt;
			for(int j=1;j<m;j++)
				h[cnt][j]=m*(x[i]-1)+j;
		}
	for(int i=1;i<=n;i++)
		z[i]=m*i;
	for(int i=1;i<=q;i++){
		int k=mp[x[i]];
		int ans;
		if(y[i]!=m)ans=h[k][y[i]];
		else ans=z[x[i]];
		writeln(ans);
		for(int j=y[i];j<m;j++)
			h[k][j]=h[k][j+1];
		if(y[i]<m)h[k][m-1]=z[x[i]];
		for(int j=x[i];j<n;j++)
			z[j]=z[j+1];
		z[m]=ans;
	}
}
void baoli()
{
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=(i-1)*n+j;
	while(q--){
		int x,y;
		x=read();y=read();
		int k=a[x][y];
		writeln(k);
		for(int i=y;i<m;i++)
			a[x][i]=a[x][i+1];
		for(int i=x;i<n;i++)
			a[i][m]=a[i+1][m];
		a[n][m]=k;
	}
}
void slove()
{
	tot=n+m-1;
	for(int i=1;i<=tot;i++)
		addto(i);
	for(int i=1;i<=m;i++)
		t[i]=i;
	for(int i=1;i<=n;i++)
		t[m+i-1]=m*i;
	while(q--){
		int x,y;
		x=read();y=read();
		t[++tot]=t[query(y)];
		addto(y);
		writeln(t[tot]);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if(n<1005&&m<1005){
		baoli();
		return 0;
	}
	else if(q<505){
		justdoit();
		return 0;
	}
	else{
		slove();
		return 0;
	}
} 

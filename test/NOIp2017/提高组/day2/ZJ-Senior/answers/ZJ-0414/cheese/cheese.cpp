#include<cstdio>
#include<cstring>
#include<algorithm>
#define G c=getchar()
#define maxn 1010
#define maxm 2000010
using namespace std;
typedef long long LL;
inline LL read()
{
	LL x=0,f=1;char G;
	while(c<'0'||'9'<c){if(c=='-')f*=-1;G;}
	while('0'<=c&&c<='9')x=x*10+c-48,G;
	return x*f;
}
int n;
LL h,r,x[maxn],y[maxn],z[maxn];
LL dis(int i,int j)
{
	return (x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
}
int fa[maxn];
int gf(int x){return x==fa[x]?x:fa[x]=gf(fa[x]);}
int AE(int x,int y)
{
	int u=gf(x),v=gf(y);
	if(u!=v)fa[u]=v;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read(),h=read(),r=read();
		for(int i=1;i<=n+2;i++)
			fa[i]=i;
		for(int i=1;i<=n;i++)
		{
			x[i]=read(),y[i]=read(),z[i]=read();
			if(z[i]<=r&&-r<=z[i])AE(n+1,i);
			if(h-r<=z[i]&&z[i]<=h+r)AE(n+2,i);
		}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(dis(i,j)<=4*r*r)//exceed long long?
					AE(i,j);
		puts(gf(n+1)==gf(n+2)?"Yes":"No");
	}
	fclose(stdin);
	fclose(stdout);
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#define G c=getchar()
#define maxn 13
#define maxm 80
using namespace std;
inline int read()
{
	int x=0,f=1;char G;
	while(c<'0'||'9'<c){if(c=='-')f*=-1;G;}
	while('0'<=c&&c<='9')x=x*10+c-48,G;
	return x*f;
}
typedef long long LL;
int a[maxn][maxn],n,m,vis,ALL,cnt,h[maxn],aug;
int U[maxm],V[maxm],b[maxn][maxn],ch[maxm];
int W[maxm];
int fa[maxn];
int gf(int x){return x==fa[x]?x:fa[x]=gf(fa[x]);}
int AE(int x,int y)
{
	int u=gf(x),v=gf(y);
	if(u==v)return 1;
	fa[u]=v;
	return 0;
}
int to[maxm],nxt[maxm],idx[maxn],SSS;
LL w[maxm],ans=2147483647777LL,sum;
#define Ae(x,y,z) w[SSS]=z,to[SSS]=y,nxt[SSS]=idx[x],idx[x]=SSS++
void calc(int u,int fa,int dep)
{
	for(int i=idx[u];i;i=nxt[i])
		if(to[i]!=fa)
		{
			sum+=w[i]*1ll*dep;
			calc(to[i],u,dep+1);
		}
}
int ccc;
void chk()//1184040
{
	memset(idx,0,sizeof(idx));SSS=1;
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1;i<=n-1;i++)
		if(AE(U[ch[i]],V[ch[i]]))return;
//	ccc++;
	for(int i=1;i<=n-1;i++)
		Ae(U[ch[i]],V[ch[i]],W[ch[i]]),Ae(V[ch[i]],U[ch[i]],W[ch[i]]);
	for(int i=1;i<=n;i++)
	{
		sum=0;calc(i,0,1);
		ans=min(ans,sum);
	}
}
int SS=0;
void dfs(int x)
{
	if(cnt==n-1){chk();return;}
	if(SS-x+1+cnt<n-1)return;	
	cnt++;ch[cnt]=x;
	dfs(x+1);
	cnt--;
	dfs(x+1);
}
#define INF 500010
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	h[1]=1;
	for(int i=2;i<=n;i++)h[i]=h[i-1]<<1;
	ALL=(1<<n)-1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			a[i][j]=INF;
	for(int i=1;i<=m;i++)
	{
		int u=read(),v=read(),w=read();
		a[u][v]=min(a[u][v],w);
		a[v][u]=min(a[v][u],w);
	}
	SS=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(a[i][j]!=INF)U[++SS]=i,V[SS]=j,W[SS]=a[i][j];
	dfs(1);
//	printf("%d",ccc);
	printf("%lld",ans);
	fclose(stdin);
	fclose(stdout);
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define G c=getchar()
using namespace std;
typedef long long LL;
inline int read()
{
	int x=0,f=1;char G;
	while(c<'0'||'9'<c){if(c=='-')f*=-1;G;}
	while('0'<=c&&c<='9')x=x*10+c-48,G;
	return x*f;
}
#define gt(x,y) ((b[x]-1)*m+y)
#define gx(z) (bb[z/m])
namespace q500
{
	const int maxn=100010,maxm=26000000;
	LL b[maxn],a[maxm],bb[maxn],shu[maxn];
	int e[maxn];
	int Q[maxn][2],gg=0;
	int sb(LL x)
	{
		int mid,l=0,r=gg-1;
		while(l<r)
		{
			mid=l+r>>1;
			if(a[mid]==x)return mid;
			else if(a[mid]<x)r=mid-1;
			else l=mid+1;
		}
	}
}
namespace x1
{
	const int maxn=300010;
	LL a[maxn*3];
	int c[maxn*3];
	int ask(int x)
	{
		int r=0;
		for(;x;x-=x&-x)r+=c[x];
		return r;
	}
	int add(int x,int v)
	{
		for(;x<900030;x+=x&-x)c[x]+=v;
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n=read(),m=read(),q=read();
	{
		using namespace q500;
	//	printf("%.4lf",(sizeof(a)+sizeof(b))/1024.0/1024.0);
		for(int t=1;t<=q;t++)
		{
			LL x=read(),y=read();
			if(!b[x])bb[gg]=x,b[x]=++gg;
			Q[t][0]=x,Q[t][1]=y;
		}
		for(int i=0;i<gg;i++)
		{
			int x=bb[i],w=gt(x,1);
			for(int j=1;j<=m;j++,w++)
				a[w]=m*(x-1)+j;
		}
		for(int i=1;i<=n;i++)
			shu[i]=i*m;
		for(int t=1;t<=q;t++)
		{
			int x=Q[t][0],y=Q[t][1],w=gt(x,y);
			LL tmp1=y<m?a[w]:shu[x];
		//	printf("%lld\n",a[w]);
			for(int i=y;i<=m-2;i++,w++)a[w]=a[w+1];//,printf("%lld ",a[w]);
			if(y<m)a[w]=shu[x];//,printf("%lld ",a[w]);
			for(;x<=n-1;x++)shu[x]=shu[x+1];//,printf("%lld ",shu[x]);
			shu[x]=tmp1;//printf("%lld ",shu[x]);
			printf("%lld\n",tmp1);
		}
	}
	fclose(stdin);
	fclose(stdout);
}

/*
1 2 3
4 5 6
7 8 9

2 3 6
4 5 9
7 8 1

3 6 9
4 5 1
7 8 2

*/

#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define sqr(x) (x)*(x)
#define G getchar()
#define mkp make_pair
#define X first
#define Y second
#define LL long long
#define N 1005
#define NN 1000005
int n,m,a[N][N];
int b[NN],c[NN],cnt,ln,cs;
int read(){
	int x=0;char ch=G;bool flg=0;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
int get(int i,int j){
	return (i-1)*m+j;
}
int add(int x,int y){
	for(;x<=ln;x+=x&-x)c[x]+=y;
}
int query(int x){
	int y,z=0;
	per(y,cs,0)if(z+(1<<y)<=ln&&x>c[z+(1<<y)]){
		x-=c[z+=(1<<y)];
	}
	return z+1;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int i,j,Q,x,y,z;
	n=read();m=read();Q=read();
	if(n<=1000&m<=1000){
		rep(i,1,n)rep(j,1,m)a[i][j]=get(i,j);
		while(Q--){
			x=read();y=read();
			printf("%d\n",z=a[x][y]);
			rep(j,y+1,m)a[x][j-1]=a[x][j];
			rep(i,x+1,n)a[i-1][m]=a[i][m];
			a[n][m]=z;
		}
		return 0;
	}
	rep(j,1,m)b[j]=j;
	rep(i,2,n)b[i+m-1]=get(i,m);
	cnt=n+m-1;ln=cnt+Q;
	while((1<<cs)<=ln)++cs;
	--cs;
	rep(i,1,cnt)add(i,1);
	while(Q--){
		x=read();y=read();
		z=query(y);
		printf("%d\n",b[++cnt]=b[z]);b[z]=0;
		add(z,-1);add(cnt,1);
	}
	return 0;
}

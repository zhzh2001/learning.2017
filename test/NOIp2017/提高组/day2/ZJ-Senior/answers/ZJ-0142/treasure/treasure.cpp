#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define sqr(x) (x)*(x)
#define G getchar()
#define mkp make_pair
#define X first
#define Y second
#define LL long long
#define N 13
#define NN 1000005
#define inf 1061109567
int n,m,W[N][N],vv;
int key[N];bool use[N];
int d[N],ans=inf;
int read(){
	int x=0;char ch=G;bool flg=0;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
void cal(){
	memset(d,63,sizeof d);
	int i,j,sum=0;d[key[1]]=0;
	rep(i,2,n){
		rep(j,1,i-1)
			d[key[i]]=min(d[key[i]],d[key[j]]+1);
		sum+=d[key[i]]*vv;
	}
	ans=min(ans,sum);
}
void DFS(int k){
	if(k>n){
		cal();return;
	}
	int i;
	rep(i,1,n)if(!use[i]){
		use[key[k]=i]=1;DFS(k+1);use[i]=0;
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int x,y;
	n=read();m=read();
	memset(W,63,sizeof W);
	while(m--){
		x=read();y=read();
		vv=W[x][y]=W[y][x]=min(W[x][y],read());
	}
	DFS(1);
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
using namespace std;
#define rep(i,j,k) for(i=j;i<=k;++i)
#define per(i,j,k) for(i=j;i>=k;--i)
#define sqr(x) (x)*(x)
#define G getchar()
#define mkp make_pair
#define X first
#define Y second
#define LL long long
#define N 1005
LL n,h,r,a[N],b[N],c[N];
bool W[N][N],d[N];LL q[N];
LL read(){
	LL x=0;char ch=G;bool flg=0;
	while((ch<48||ch>57)&&ch!=45)ch=G;
	if(ch==45)flg=1,ch=G;
	for(;ch>47&&ch<58;ch=G)x=x*10+ch-48;
	return flg?-x:x;
}
void BFS(){
	memset(d,0,sizeof d);
	LL Ft=0,Rr=1,u,i;
	d[q[0]=n+1]=1;
	while(Ft<Rr){
		u=q[Ft++];
		rep(i,1,n+2)if(!d[i]&&W[u][i]){
			d[q[Rr++]=i]=1;
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	LL cas=read(),i,j;
	while(cas--){
		n=read();h=read();r=read();
		memset(W,0,sizeof W);
		rep(i,1,n){
			a[i]=read();b[i]=read();c[i]=read();
			if(c[i]<=r)W[n+1][i]=1;
			if(c[i]+r>=h)W[i][n+2]=1;
		}
		rep(i,1,n)rep(j,i+1,n){
			W[i][j]=W[j][i]=sqr(a[i]-a[j])+sqr(b[i]-b[j])+sqr(c[i]-c[j])<=sqr(r<<1);
		}
		BFS();
		puts(d[n+2]?"Yes":"No");
	}
	return 0;
}

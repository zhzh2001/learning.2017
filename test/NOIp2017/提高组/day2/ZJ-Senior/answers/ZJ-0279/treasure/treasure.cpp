#include<iostream>
#include<cstdio>
#include<cstring>
#define N 300
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
using namespace std;
int n,m,res,MIN,h,t,st,i,j,x,y,z;
//int a[N],mark;
int list[N],dep[N],sum[N],f[N][N],edge[N][N];
void dfs(int x)
{
	int i;
	if (x == n+1)
		{
			int res = 0;
			h = 0; t = 1; list[1] = st; dep[1] = 1;
			while (h < t)
				{
					h++;
					fo(i,1,sum[list[h]])
						{
							res += dep[h]*f[list[h]][edge[list[h]][i]];
							if (res > MIN) return;
							t++; list[t]=edge[list[h]][i]; dep[t]=dep[h]+1;
						}
				}
			if (t != n) return;
			if (res < MIN) MIN = res;
			return;
		}
	if (x == st) {dfs(x+1); return;}
	fo(i,1,n)
		if (x != i)
			{
			//	a[x] = i;
				sum[i]++; edge[i][sum[i]] = x;
				dfs(x+1);
				edge[i][sum[i]] = 0; sum[i]--;
			}
}
void readwww(int &x)
{
	char ch; x = 0;
	ch = getchar(); while (ch <= '0' && ch >= '9') ch = getchar();
	x = ch - '0'; ch = getchar(); while (ch >= '0' && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	//readwww(n); readwww(m);
	scanf("%d%d",&n,&m);
	fo(i,1,n) fo(j,1,n) f[i][j] = 500000;
	fo(i,1,m)
		{
			//readwww(x); readwww(y); readwww(z);
			scanf("%d%d%d",&x,&y,&z);
			f[x][y] = min(f[x][y],z);
			f[y][x] = f[x][y];
		}
/*	fo(i,1,n)
		{
			fo(j,1,n) cout<<f[i][j]<<" ";
			cout<<endl;
		}*/
	MIN = 2000000000;
	fo(st,1,n)
		{
		//	mark=0;
		//	fo(i,1,n) a[i] = 0;
		//	fo(i,1,n) sum[i] = 0;
		//	a[st] = -1;
			dfs(1);
		}
	cout<<MIN<<endl;
	return 0;
}

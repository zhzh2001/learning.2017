#include<iostream>
#include<cstdio>
#define N 1000000
#define M 2000
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
using namespace std;
int sum[N*4],y[N],f[N];
int g[M][M];
int n,m,q,i,x,t_last,t_now,tot;
int query(int x,int l,int r,int upper)
{
	if (r <= upper) return sum[x];
	if (l > upper) return 0;
	int mid = (l + r) >> 1;
	return query(x<<1,l,mid,upper) + query(x<<1|1,mid+1,r,upper);
}
void change(int x,int l,int r,int tar)
{
	if (l == r) {sum[x]++; return;}
	int mid = (l + r) >> 1;
	if (tar <= mid) change(x<<1,l,mid,tar);
	if (mid+1 <= tar) change(x<<1|1,mid+1,r,tar);
	sum[x] = sum[x<<1] + sum[x<<1|1];
}
void pow1()
{
	int i,j,tot,x,y,t; tot = 0;
	fo(i,1,n) fo(j,1,m) g[i][j] = ++tot;
	fo(i,1,q)
		{
			cin>>x>>y; t = g[x][y];
			cout<<t<<endl;
			fo(j,y,m-1) g[x][j] = g[x][j+1];
			fo(j,x,n-1) g[j][m] = g[j+1][m];
			g[n][m] = t;
		}
}
void readwww(int &x)
{
	char ch; x = 0;
	ch = getchar(); while (ch <= '0' && ch >= '9') ch = getchar();
	x = ch - '0'; ch = getchar(); while (ch >= '0' && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n <= 1000 && m <= 1000) {pow1(); return 0;}
	fo(i,1,q) readwww(x),readwww(y[i]);
	fo(i,1,q)
		{
			t_last = query(1,1,n+m+q,y[i]);
			while (1)
				{
					t_now = query(1,1,n+m+q,y[i]+t_last);
					if (t_last == t_now) break;
					t_last = t_now;
				}
			y[i] += t_now;
			change(1,1,n+m+q,y[i]);
		}
	
	fo(i,1,m) f[++tot] = i;
	fo(i,2,n) f[++tot] = i * m;
	fo(i,1,q)
		{
			printf("%d\n",&f[y[i]]);
			f[++tot] = f[y[i]];
		}
	cout<<endl;
	return 0;
}

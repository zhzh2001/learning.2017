var
  edge:array[0..1020,0..1020] of longint;
  i,j,k,n,m,s,a,b,c,t,h,r:longint;
  w:array[0..1020,1..3] of longint;
  flag:boolean;

function connect(aa,bb:longint):boolean;
var
  x,y,z:double;
  i,j:longint;
begin
  i:=aa;
  j:=bb;
  x:=w[i,1]-w[j,1];
  y:=w[i,2]-w[j,2];
  z:=w[i,3]-w[j,3];
  x:=x*x; y:=y*y; z:=z*z;
  if x+y+z<=4*r*r then exit(true) else exit(false);
end;

procedure readin;
var
  i,j,k:longint;
begin
  read(n,h,r);
  for i:=2 to n+1 do
    read(w[i,1],w[i,2],w[i,3]);
  for i:=2 to n+1 do
    if w[i,3]<=r then begin edge[1,i]:=1; edge[i,1]:=1; end;
  for i:=2 to n+1 do
    if w[i,3]+r>=h then begin edge[i,n+2]:=1; edge[n+2,i]:=1; end;
  for i:=2 to n do
    for j:=i+1 to n+1 do
    if connect(i,j) then begin edge[i,j]:=1; edge[j,i]:=1; end;
end;

procedure bfs;
var
  queue:array[1..1100] of longint;
  color:array[1..1100] of boolean;
  i,j,k,a,b,c,up,down:longint;
begin
  up:=2;
  down:=1;
  queue[1]:=1;
  fillchar(color,sizeof(color),false);
  color[1]:=true;
  while (up>down) do
  begin
    a:=queue[down];
    for i:=n+2 downto 1 do
      if (edge[a,i]=1) and (not color[i]) then
      begin
        if i=n+2 then begin flag:=true; exit; end;
        queue[up]:=i;
        color[i]:=true;
        inc(up);
      end;
    inc(down);
  end;
end;

begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);
  read(t);
  for i:=1 to t do
  begin
    fillchar(edge,sizeof(edge),0);
    flag:=false;
    readin;
    bfs;
    if flag then writeln('Yes') else writeln('No');
  end;
  close(input);
  close(output);
end.
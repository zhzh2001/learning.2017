var
  ee:array[1..20,1..20] of longint;
  u,edge:array[1..300,1..3] of longint;
  f:array[1..100] of longint;
  i,j,k,n,m,num,min,xsum:longint;

procedure readin;
var
  i,j,k,a,b,c:longint;
begin
  num:=0;
  read(n,m);
  //for i:=1 to m do
    //read(edge[i,1],edge[i,2],edge[i,3]);
  fillchar(ee,sizeof(ee),$FF);
  for i:=1 to m do
  begin
    read(a,b,c);
    if a=b then continue;
    if (ee[a,b]>c) or (ee[a,b]=-1) then begin ee[a,b]:=c; ee[b,a]:=c; end;
  end;
  for i:=n-1 downto 1 do
    for j:=n downto i+1 do
      if ee[i,j]<>-1 then begin inc(num); edge[num,1]:=i; edge[num,2]:=j; edge[num,3]:=ee[i,j]; end;
end;

function find(x:longint):longint;
begin
  if f[x]=x then find:=x else find:=find(f[x]);
end;

procedure bfs;
var
  queue,deep,color:array[1..40] of longint;
  i,j,k,a,b,c,down,up,sum:longint;
begin
  for k:=1 to n do
  begin
    fillchar(color,sizeof(color),0);
    color[k]:=1;
    queue[1]:=k;
    deep[k]:=0;
    down:=1;
    up:=2;
    sum:=0;
    while (up<n+1) do
    begin
      a:=queue[down];
      for i:=1 to n-1 do
      begin
        if (u[i,1]=a) and (color[u[i,2]]=0) then begin b:=u[i,2]; color[b]:=1; deep[b]:=deep[a]+1; queue[up]:=b; inc(up); sum:=sum+deep[b]*u[i,3]; end;
        if (u[i,2]=a) and (color[u[i,1]]=0) then begin b:=u[i,1]; color[b]:=1; deep[b]:=deep[a]+1; queue[up]:=b; inc(up); sum:=sum+deep[b]*u[i,3]; end;
      end;
      inc(down);
    end;
    if sum<min then min:=sum;
  end;
end;

procedure dfs(x,y:longint);
var
  k1,k2:longint;
begin
  if xsum>=min then exit;
  if y=n-1 then begin bfs; exit; end;
  if x=num+1 then exit;
  k1:=find(edge[x,1]);
  k2:=find(edge[x,2]);
  if k1<>k2 then
  begin
    f[k1]:=k2;
    u[y+1,1]:=edge[x,1];
    u[y+1,2]:=edge[x,2];
    u[y+1,3]:=edge[x,3];
    xsum:=xsum+edge[x,3];
    dfs(x+1,y+1);
    xsum:=xsum-edge[x,3];
    f[k1]:=k1;
  end;
  dfs(x+1,y);
end;

begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);
  readin;
  for i:=1 to n do
    f[i]:=i;
  min:=999999999;
  dfs(1,0);
  write(min);
  close(input);
  close(output);
end.

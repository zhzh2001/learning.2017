var
  ques:array[1..100000,1..2] of longint;
  block,del:array[1..100000] of longint;
  queue:array[1..1000000] of longint;
  i,j,k,n,m,l,q:longint;

function find(x:longint):longint;
var
  i,j,k,sum:longint;
begin
  sum:=0;
  for i:=1 to x div 1000+1 do
    sum:=sum+block[i]+1000;
  j:=x div 1000;
  while (j<x) do
  begin
    j:=j+1;
    sum:=sum+del[j];
  end;
  find:=sum;
end;

procedure solve2;
var
  i,j,k,t,l,num,sum,up,down,k1,k2,k3:longint;
  a,b,map:array[1..500000] of longint;
begin
  up:=1;
  for i:=1 to m do
    map[i]:=i;
  for i:=m+1 to m+n-1 do
    map[i]:=(i-m+1)*m;
  for i:=1 to q do
  begin
    k1:=ques[i,1];
    k2:=ques[i,2];
    k3:=k2;
    if k3>n+m-t-1 then
    begin
      k:=k3-n-m+t+1;
      k1:=0;
      k2:=0;
      while k2<k do
      begin
        inc(k1);
        if queue[k1]>0 then inc(k2);
      end;
      writeln(map[queue[k1]]);
      queue[up]:=queue[k1];
      inc(up);
    end
    else
    begin
      writeln(map[find(k3)]);
      del[k3]:=del[k3]+1;
      inc(block[k3 div 1000+1]);
      inc(t);
      queue[up]:=map[find(k3)];
      inc(up);
    end;
  end;
end;


procedure solve1;
var
  a:array[1..1010,1..1010] of longint;
  i,j,k,num:longint;
begin
  for i:=1 to n do
    for j:=1 to m do
      a[i,j]:=(i-1)*m+j;
  for i:=1 to q do
  begin
    num:=a[ques[i,1],ques[i,2]];
    writeln(num);
    for j:=ques[i,2] to m-1 do
      a[ques[i,1],j]:=a[ques[i,1],j+1];
    for j:=ques[i,1] to n-1 do
      a[j,m]:=a[j+1,m];
    a[n,m]:=num;
  end;
end;

procedure readin;
var
  i,j,k:longint;
  flag:boolean;
begin
  flag:=true;
  read(n,m,q);
  for i:=1 to q do
  begin
    read(ques[i,1],ques[i,2]);
    if ques[i,2]<>1 then flag:=false;
  end;
  if (not flag) or (m<=1000) then solve1 else solve2;
end;

begin
  assign(input,'phalanx.in');
  assign(output,'phalanx.out');
  reset(input);
  rewrite(output);
  readin;
  close(input);
  close(output);
end.

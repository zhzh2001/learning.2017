#include<bits/stdc++.h>
using namespace std;
int ljb[10][10],cz[10],sd[10],f[10][10];
int n,m,ans;
int tp(int k)
{
	int x=0;
	for (int i=1;i<=ljb[k][0];i++)
	  if (!cz[ljb[k][i]])
	  {
	  	sd[ljb[k][i]]=sd[k]+1;
	  	cz[ljb[k][i]]=1;
	  	x+=tp(ljb[k][i])+sd[k]*f[k][ljb[k][i]];
	  }
	return x;
}
int src(int k,int s)
{
	if (s>=ans) return 0;
	if (k==n)
	{
		ans=min(ans,s);
		return 0;
	}
	for (int i=1;i<=n;i++) if (cz[i])
	  for (int j=1;j<=n;j++) if (!cz[j] && f[i][j]<1e9)
	  {
	  	cz[j]=1;
	  	sd[j]=sd[i]+1;
	  	src(k+1,s+sd[i]*f[i][j]);
	  	cz[j]=0;
	  }
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	for (int i=1;i<=n;i++)
	  for (int j=1;j<=n;j++)
	    f[i][j]=1e9;
	for (int i=1;i<=m;i++)
	{
		int l,r,z;
		scanf("%d %d %d",&l,&r,&z);
		f[l][r]=f[r][l]=min(f[r][l],z);
	}
	if (m==n-1)
	{
		for (int i=1;i<=n;i++)
	  	  for (int j=1;j<=n;j++)
	   		if (f[i][j]<1e9)
	    	{
	    		ljb[i][0]++;
	    		ljb[i][ljb[i][0]]=j;
			}
		ans=1e9;
		for (int i=1;i<=n;i++)
		{
			for (int j=1;j<=n;j++) cz[j]=0,sd[j]=0;
			sd[i]=1; cz[i]=1;
			ans=min(ans,tp(i));
		}
		printf("%d",ans);
		return 0;
	}
	ans=1e9;
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=n;j++) sd[j]=0;
		sd[i]=1; cz[i]=1;
		src(1,0);
		cz[i]=0;
	}
	printf("%d",ans);
}

#include<iostream>
#include<cstdio>
using namespace std;
int T,n,f[1010],u,v;
unsigned long long sum;
long long h,r,X[1010],Y[1010],Z[1010];
long long sqr(long long X){
	return X*X;
}
int find(int x){
	if (x==f[x]) return x;
	return f[x]=find(f[x]);
}
void uni(int i,int j){
	u=find(i),v=find(j);
	if (u!=v) f[v]=u;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=0;i<=n+1;i++) f[i]=i;
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&X[i],&Y[i],&Z[i]);
			if (h-Z[i]<=r) uni(n+1,i);
			if (Z[i]<=r) uni(0,i);
			for (int j=1;j<i;j++){
				sum=sqr(X[i]-X[j]);
				if (sum<=sqr(2*r)){
					sum=sum+sqr(Y[i]-Y[j]);
					if (sum<=sqr(2*r)){
						sum+=sqr(Z[i]-Z[j]);
						if (sum<=sqr(2*r))
							uni(i,j);
					}
				}
			}
		}
		u=find(0),v=find(n+1);
		if (u!=v) printf("No\n");else printf("Yes\n");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
using namespace std;
int n,m,a[13][13],ex[200],ey[200],num,X,Y,Z,b[13][13],flag[200],f[13],minn,u,v,fa,vis[13];
int fin(int x,int cnt,int fa){
	int res=0;
	for (int i=1;i<=n;i++)
		if (b[x][i]&&i!=fa){
			res+=a[x][i]*cnt+fin(i,cnt+1,x);
			if (res>=minn) return res;
		}
	return res;
}
int find(int x){
	if (x==f[x]) return x;
	return f[x]=find(f[x]);
}
void uni(int i,int j){
	u=find(i),v=find(j);
	if (u!=v) f[v]=u;
}
void dfs(int x,int cnt){
	if (cnt==n){
		for (int i=1;i<=n;i++) f[i]=i;
		for (int i=1;i<=num;i++)
			if (flag[i]) b[ex[i]][ey[i]]=b[ey[i]][ex[i]]=1,uni(ex[i],ey[i]);
			else b[ex[i]][ey[i]]=b[ey[i]][ex[i]]=0;
		fa=find(1);
		for (int i=2;i<=n;i++) if (fa!=find(i)) return;
		//for (int i=1;i<=n;i++){for (int j=1;j<=n;j++) printf("%d ",b[i][j]);printf("\n");}printf("\n");
		for (int i=1;i<=n;i++) minn=min(minn,fin(i,1,0));
		return;
	}
	for (int i=x;i<=num-n+cnt+1;i++){
		//u=find(ex[i]),v=find(ey[i]);
		//cout<<ex[i]<<' '<<ey[i]<<' '<<u<<' '<<v<<endl;
		//if (u==v) continue;
		//int pre=f[v];
		//f[v]=u;
		flag[i]=1;
		dfs(i+1,cnt+1);
		flag[i]=0;//f[v]=pre;
	}
}
int read(){
	char ch=getchar();
	int X=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') X=X*10+ch-'0',ch=getchar();
	return X;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
		    a[i][j]=1e8;
	minn=1<<29;
	for (int i=1;i<=m;i++){
		X=read(),Y=read(),Z=read();
		a[X][Y]=a[Y][X]=min(a[X][Y],Z);
	}
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
			if (a[i][j]<1e8)
				ex[++num]=i,ey[num]=j;
	dfs(1,1);
	printf("%d",minn);
	return 0;
}

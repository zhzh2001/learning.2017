#include<cstdio>
#include<algorithm>
#include<cstring>
#define lowbit(x) x&(-x)
using namespace std;
typedef long long ll;
int flag[100010],c1[100010];
ll f[502][50002],la[50002],s[2000010];
int tree[2000010],vis[2000010],maxr;
int x[100005],y[100005];
int read()
{
	int ans=0;
	char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') 
	{
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
int query(int x)
{
	int ans=0;
	for (;x;x-=lowbit(x))
		ans+=tree[x];
	return ans;
}
void add(int x,int key)
{
	for (;x<=1200010;x+=lowbit(x))
		tree[x]+=key;
}
int bs(int l,int r,int xx)
{
	if (l>r) return 0;
	if (l==r) return l;
	int mid=(l+r)>>1;
	int sum=query(mid);
	if (mid-sum>xx||(mid-sum==xx&&vis[mid])) return bs(l,mid-1,xx);
	if (mid-sum<xx) return bs(mid+1,r,xx);
	return mid; 
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx2.out","w",stdout);
	int n,m,q;
	scanf("%d%d%d",&n,&m,&q);
		for (int i=1;i<=m;i++)
			s[i]=i;
		for (int i=2;i<=n;i++)
			s[m+i-1]=(ll)i*(ll)m;
		memset(vis,0,sizeof(vis));
		maxr=n+m-1; 
		while (q--)
		{
			int x=read(),y=read();
		//	scanf("%d%d",&x,&y);
			int pos=bs(1,maxr,y);
			printf("%lld\n",s[pos]);
			s[++maxr]=s[pos];
			vis[pos]=1;
			add(pos,1);
		}
		return 0;
	for (int i=1;i<=q;i++)
	{
		x[i]=read(),y[i]=read();
		//scanf("%d%d",&x[i],&y[i]);
		flag[x[i]]=1;
	}
	int cnt=0;
	for (int i=1;i<=n;i++)
		if (flag[i])
		{
			cnt++;
			c1[i]=cnt;
			for (int j=1;j<m;j++)
				f[cnt][j]=(ll)(i-1)*(ll)m+(ll)j;
		}
	for (int i=1;i<=n;i++)
		la[i]=(ll)(i-1)*(ll)m+(ll)m; 
	for (int cas=1;cas<=q;cas++)
	{
		ll ans;
		if (y[cas]==m) ans=la[x[cas]];
		else ans=f[c1[x[cas]]][y[cas]];
		printf("%lld\n",ans);
		for (int i=y[cas];i<m-1;i++)
			f[c1[x[cas]]][i]=f[c1[x[cas]]][i+1];
		if (y[cas]<m) f[c1[x[cas]]][m-1]=la[x[cas]];
		for (int i=x[cas];i<n;i++)
			la[i]=la[i+1];
		la[n]=ans;
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int INF=1000000000;
int dist[100],map[20][20],dis[100],f[20][20],ans,n;
void dfs(int root,int sta,int s)
{
	if (sta==(1<<n)-1)
	{
		ans=min(ans,s);
		return;
	}
	int H=0; 
	for (int i=1;i<=n;i++)
		if (!sta&(1<<(i-1))) H+=dist[i]*f[root][i];
	if (s+H>=ans) return;
	int mindis[14];
	for (int i=1;i<=n;i++)
	{
		if (!(sta&(1<<(i-1))))
		{
			for (int j=0;j<n;j++)
				mindis[j]=0;
			for (int j=1;j<=n;j++)
			{
				if ((sta&(1<<(j-1)))&&map[j][i]<INF)
					if (!mindis[dis[j]]||map[j][i]<map[mindis[dis[j]]][i]) 
						mindis[dis[j]]=j;
			} 
			for (int j=0;j<n;j++)
				if (mindis[j])
				{
					dis[i]=j+1;
					dfs(root,sta|(1<<(i-1)),s+map[mindis[j]][i]*dis[i]);
					dis[i]=0;
				}			
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			map[i][j]=INF;
	for (int i=1;i<=m;i++)
	{
		int u,v,s;
		scanf("%d%d%d",&u,&v,&s);
		map[u][v]=min(map[u][v],s);
		map[v][u]=min(map[v][u],s);
	}
	if (n>10)
	{
		long long ans1=INF*1000LL;
		for (int i=1;i<=n;i++)
		{
			long long sum=0;
			for (int j=1;j<=n;j++)
				if (i!=j) sum+=(long long)map[i][j];
			ans1=min(ans1,sum);
		}
		printf("%lld\n",ans1);
		return 0;
	}
	for (int i=1;i<=n;i++)
	{
		dist[i]=INF;
		for (int j=1;j<=n;j++)
			dist[i]=min(dist[i],map[i][j]);
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			f[i][j]=map[i][j];
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (i!=j&&j!=k&&i!=k) f[i][j]=min(f[i][j],f[i][k]+f[k][j]); 
	ans=INF;
	for (int i=1;i<=n;i++)
	{
		memset(dis,0,sizeof(dis));
		dfs(i,1<<(i-1),0);
	}
	printf("%d\n",ans);
	return 0;
}

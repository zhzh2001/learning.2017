#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long ll;
int map[1005][1005],vis[10000],q[10000],n;
int x[1005],y[1005],z[1005];
ll dist,d1;
ll sqr(ll x)
{
	return (ll)x*(ll)x;
}
void bfs(int st)
{
	int he=1,ta=1;
	q[he]=st;
	vis[st]=1;
	while (he<=ta)
	{
		int u=q[he];
		for (int i=0;i<=n+1;i++)
			if (map[u][i]&&!vis[i])
			{
				vis[i]=1;
				q[++ta]=i;
				if (i==n+1) return;
			}
		he++;
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while (cas--)
	{
		int h,r;
		scanf("%d%d%d",&n,&h,&r);
		memset(map,0,sizeof(map));
		for (int i=1;i<=n;i++)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if (z[i]<=r) map[0][i]=map[i][0]=1;
			if (z[i]+r>=h) map[i][n+1]=map[n+1][i]=1;
		}
		d1=sqr(r)*4LL;
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
			{
				dist=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);	
				if (d1>=dist) map[i][j]=map[j][i]=1;
			}
		memset(vis,0,sizeof(vis));
		bfs(0);
		if (vis[n+1]) puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
using namespace std;
int UU,n;
#define N 14
int dp[N][6000];
int yh[N][6000][N];
int dis[N][N];
void dfs(int z,int sta,int dep,int ans){
  if (z>n){
    dp[dep][sta]=min(dp[dep][sta],ans);
    return;
	}
	dfs(z+1,sta,dep,ans);
	if ((!(sta&(1<<(z-1))))&&yh[dep][UU][z]<1<<29)
	  dfs(z+1,sta|(1<<(z-1)),dep,ans+yh[dep][UU][z]);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
	  for (int j=1;j<=n;++j) dis[i][j]=1<<29;
	for (int i=1;i<=m;++i){
    int u,v,z;
    scanf("%d%d%d",&u,&v,&z);
    dis[u][v]=min(dis[u][v],z);
    dis[v][u]=min(dis[v][u],z);
	}
	for (int dep=1;dep<=n;++dep){
	  for (int sta=0;sta<=((1<<n)-1);++sta){
	    for (int i=1;i<=n;++i)
	    if (!(sta&(1<<(i-1)))){
	    	yh[dep][sta][i]=1<<29;
	      for (int j=1;j<=n;++j)
				if ((sta&(1<<(j-1)))&&dis[j][i]<(1<<29)){
					yh[dep][sta][i]=min(yh[dep][sta][i],dis[j][i]*dep);
	      }
			}
		}
	}
	for (int i=0;i<=n;++i)
	  for (int sta=0;sta<=((1<<n)-1);++sta) dp[i][sta]=1<<29;
	for (int i=1;i<=n;++i)
	  dp[0][1<<(i-1)]=0;
	for (int dep=0;dep<n;++dep){
		for (int sta=0;sta<=((1<<n)-1);++sta){
			UU=sta;
		  dfs(1,sta,dep+1,dp[dep][sta]);
		}
	}
	int ans=1<<29;
	for (int i=0;i<=n;++i)
		ans=min(ans,dp[i][(1<<n)-1]);
	printf("%d\n",ans);
	return 0;
}

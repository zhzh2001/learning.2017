#include<iostream>
#include<cstdio>
using namespace std;
int a[1005][1005];
#define ll long long
inline int read(){
  char last='+',ch=getchar();
  while (ch<'0'||ch>'9'){if (ch=='-')last=ch;ch=getchar();}
  int tmp=0;
  while (ch>='0'&&ch<='9'){tmp=tmp*10+(ch-'0');ch=getchar();}
  if (last=='-') tmp=-tmp;
  return tmp;
}
#define N 300500
#define lowbit(x) ((x)&(-x))
int h[N*3];
ll yh[N*3];
void add(int x,int y){
  for (int i=x;i<=900500;i+=lowbit(i)) h[i]+=y;
}
int getsum(int x){
	int ans=0;
  for (int i=x;i>0;i-=lowbit(i)) ans+=h[i];
  return ans;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
  int n,m,q;
  scanf("%d%d%d",&n,&m,&q);
  if (n<=1000&&m<=1000&&q<=500){
  	for (int i=1;i<=n;++i)
  	  for (int j=1;j<=m;++j) a[i][j]=(i-1)*m+j;
  	while (q--){
  	  int x,y;
  	  scanf("%d%d",&x,&y);
  	  int ans=a[x][y];
  	  printf("%d\n",a[x][y]);
  	  for (int i=y;i<m;++i)
  	    a[x][i]=a[x][i+1];
  	  for (int i=x;i<n;++i)
  	    a[i][m]=a[i+1][m];
  	  a[n][m]=ans;
		}
		return 0;
  }
  int hed=1,tail=m;
  for (int i=1;i<=m;++i) add(i,1),yh[i]=i;
  for (int j=2;j<=n;++j){
    ++tail;
    add(tail,1);
    yh[tail]=(ll)j*m;
	}
  while (q--){
    int x,y;
    x=read();
    y=read();
    int l=hed,r=tail;
    while (l<r){
    	int mid=(l+r)>>1;
    	if (getsum(mid)>y) r=mid-1;
			else l=mid; 
    }
    printf("%lld\n",yh[l]);
    ++tail;
    add(tail,1);
    yh[tail]=yh[l];
    add(l,-1);
    yh[l]=0;
	}
	return 0;
}
  

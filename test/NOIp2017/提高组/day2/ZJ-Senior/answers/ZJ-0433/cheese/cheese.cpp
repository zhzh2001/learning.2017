#include<iostream>
#include<cstdio>
using namespace std;
#define ll long long
#define N 2000
#define M 2000000
int vet[M],Next[M];
int hed[N];
bool flag[N];
ll x[N],y[N],z[N];
int edd;
inline void add(int u,int v){
	++edd;
	vet[edd]=v;
	Next[edd]=hed[u];
	hed[u]=edd;
}
void dfs(int x){
  for (int i=hed[x];i!=-1;i=Next[i]){
    int v=vet[i];
    if (flag[v]==true) continue;
    flag[v]=true;
    dfs(v);
	}
}
int main(){
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  int T;
  scanf("%d",&T);
  while (T--){
    int n;
    ll h,r;
    scanf("%d%lld%lld",&n,&h,&r);
    edd=0;
    for (int i=1;i<=n;++i) flag[i]=false;
    for (int i=1;i<=n;++i) hed[i]=-1;
    ll R=r*r*4ll;
    r=r*r;
    for (int i=1;i<=n;++i) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
    for (int i=1;i<=n;++i)
      for (int j=i+1;j<=n;++j){
        ll num=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
        if (num<=R){
				  add(i,j);
				  add(j,i);
				}
			}
		for (int i=1;i<=n;++i)
		if (flag[i]==false){
		  ll num=z[i]*z[i];
		  if (z[i]<=0ll||num<=r){
			  flag[i]=true;
				dfs(i);
			}
		}
		bool ans=false;
		for (int i=1;i<=n;++i)
		if (flag[i]==true){
			ll num=(h-z[i])*(h-z[i]);
		  if (z[i]>=h||num<=r){
		    ans=true;
		    break;
			}
		}
		if (ans==true) printf("Yes\n");
		else printf("No\n");
	}
  return 0;
}

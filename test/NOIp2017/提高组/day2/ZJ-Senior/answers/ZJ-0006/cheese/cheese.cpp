#include <bits/stdc++.h>
using namespace std;
typedef long long LL;

const int maxn = 1050;
int g[maxn][maxn], vi[maxn], q[maxn], x[maxn], y[maxn], z[maxn];

void solve() {
	int n, H, R;
	scanf("%d%d%d", &n, &H, &R); LL mx = 4LL * R * R;
	for (int i = 1; i <= n; ++i) scanf("%d%d%d", &x[i], &y[i], &z[i]);
	memset(g, 0, sizeof(g)); memset(vi, 0, sizeof(vi));
	for (int i = 1; i <= n; ++i) for (int j = i + 1; j <= n; ++j) {
		LL d = 1LL * (x[i] - x[j]) * (x[i] - x[j]) + 1LL * (y[i] - y[j]) * (y[i] - y[j]) + 1LL * (z[i] - z[j]) * (z[i] - z[j]);
		if (d <= mx) g[i][j] = g[j][i] = 1;
	}
	for (int i = 1; i <= n; ++i) {
		if (z[i] <= R) g[n + 1][i] = g[i][n + 1] = 1; if (H - z[i] <= R) g[n + 2][i] = g[i][n + 2] =  1;
	}
	int l = 1, r = 0; vi[n + 1] = 1; q[++r] = n + 1;
	for (; l <= r; ) {
		int x = q[l++];
		for (int i = 1; i <= n + 2; ++i) if (g[x][i] && !vi[i]) vi[i] = 1, q[++r] = i;
	}
	if (vi[n + 2]) printf("Yes\n"); else printf("No\n");
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) solve();
	return 0;
}
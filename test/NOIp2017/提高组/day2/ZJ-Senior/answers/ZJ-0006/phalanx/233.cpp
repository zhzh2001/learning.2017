#include <bits/stdc++.h>
using namespace std;

const int maxn = (int)(3e6) + 5;
int rt[maxn];
int fa[maxn], ch[maxn][2], siz[maxn], su[maxn], id[maxn];
int NOW, idx;

inline int dir(int x) {
	return ch[fa[x]][1] == x;
}

inline void pu(int x) {
	su[x] = su[ch[x][0]] + siz[x] + su[ch[x][1]];
}

inline void rot(int x) {
	int p = fa[x], d = dir(x) ^ 1;
	ch[p][d ^ 1] = ch[x][d]; fa[ch[x][d]] = p;
	fa[x] = fa[p]; if (fa[p]) ch[fa[p]][dir(p)] = x;
	ch[x][d] = p; fa[p] = x; pu(p);
}

inline void splay(int x, int goal = 0) {
	while (fa[x] != goal) {
		if (fa[fa[x]] == goal) rot(x);
		else if (dir(x) != dir(fa[x])) rot(x), rot(x);
		else rot(fa[x]), rot(x);
	}
	if (!goal) rt[NOW] = x; pu(x);
}

int findL(int x, int v) {
	if (!x) return 0;
	if (v <= siz[x] + su[ch[x][0]]) return findL(ch[x][0], v);
	if (ch[x][1]) {
		int ret = findL(ch[x][1], v - siz[x] - su[ch[x][0]]); if (ret) return ret;
	}
	return x;
}

int findNext(int x) {
	x = ch[x][1]; while (ch[x][0]) x = ch[x][0]; return x;
}

void print(int x) {
	if (ch[x][0]) print(ch[x][0]);
	printf("%d %d %d %d %d %d (%d)\n", x, id[x], siz[x], su[x], fa[x], ch[x][0], ch[x][1]);
	if (ch[x][1]) print(ch[x][1]);
}

int main() {
	//freopen("phalanx.in", "r", stdin);
	//freopen("phalanx.out", "w", stdout);
	int n, m, q;
	scanf("%d%d%d", &n, &m, &q);
	for (int i = 1; i <= n + 1; ++i) {
		++idx; rt[i] = idx;
		++idx; ch[idx - 1][1] = idx; fa[idx] = idx - 1;
		++idx; id[idx] = (i <= n ? (i - 1) * m + 1 : m); siz[idx] = (i <= n ? m - 1 : n); ch[idx - 1][0] = idx; fa[idx] = idx - 1;	
		pu(idx); pu(idx - 1); pu(idx - 2);
	}
	for (int i = 1; i <= q; ++i) {
		int x, y, c = 0, w = 0, l, r; scanf("%d%d", &x, &y);
		if (y < m) {
			NOW = x;
			l = findL(rt[NOW], y); splay(l); 
			r = findNext(l); splay(r, rt[NOW]); r = findNext(r); splay(r, rt[NOW]);
			c = ch[ch[rt[NOW]][1]][0];
			if (siz[c] == 1) {
				fa[c] = 0; ch[ch[rt[NOW]][1]][0] = 0; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
			} else {
				int cnt = su[ch[rt[NOW]][0]] + siz[rt[NOW]];
				int lS = y - cnt - 1, lD = id[c], rS = siz[c] - lS - 1, rD = id[c] + lS + 1;
				int nS = 1, nD = rD - 1;
				if (!rS) {
					siz[c] = su[c] = lS; id[c] = lD; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
				} else if (!lS) {
					siz[c] = su[c] = rS; id[c] = rD; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
				} else {
					siz[c] = su[c] = lS; id[c] = lD;
					++idx; ch[c][1] = idx; fa[idx] = c; siz[idx] = su[idx] = rS; id[idx] = rD; 
					pu(c); pu(ch[rt[NOW]][1]); pu(rt[NOW]);
				}
				c = ++idx; siz[c] = su[c] = nS; id[c] = nD; 
			}
		} 
		NOW = n + 1;
		l = findL(rt[NOW], x); splay(l);
		r = findNext(l); splay(r, rt[NOW]); r = findNext(r); splay(r, rt[NOW]);
		w = ch[ch[rt[NOW]][1]][0];
		if (siz[w] == 1) {
			fa[w] = 0; ch[ch[rt[NOW]][1]][0] = 0; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
		} else {
			int cnt = su[ch[rt[NOW]][0]] + siz[rt[NOW]];
			int lS = x - cnt - 1, lD = id[w], rS = siz[w] - lS - 1, rD = id[w] + (lS + 1) * m;
			int nS = 1, nD = rD - m;
			if (!rS) {
				siz[w] = su[w] = lS; id[w] = lD; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
			} else if (!lS) {
				siz[w] = su[w] = rS; id[w] = rD; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
			} else {
				siz[w] = su[w] = lS; id[w] = lD;
				++idx; ch[w][1] = idx; fa[idx] = w; siz[idx] = su[idx] = rS; id[idx] = rD; 
				pu(w); pu(ch[rt[NOW]][1]); pu(rt[NOW]);
			}
			w = ++idx; siz[w] = su[w] = nS; id[w] = nD;
		} 
		if (y < m) {
			swap(c, w);
			NOW = x;
			l = findL(rt[NOW], m); printf("QAQ %d\n", l); splay(l); 
			ch[ch[rt[NOW]][1]][0] = c; fa[c] = ch[rt[NOW]][1]; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
		}
		NOW = n + 1;
		l = findL(rt[NOW], n); splay(l);
		ch[ch[rt[NOW]][1]][0] = w; fa[w] = ch[rt[NOW]][1]; pu(ch[rt[NOW]][1]); pu(rt[NOW]);
		printf("%d\n", id[w]);
		for (int j = 1; j <= n + 1; ++j) {
			printf("BEGIN %d\n", j);
			print(j);
		}
	} 
	return 0;
}
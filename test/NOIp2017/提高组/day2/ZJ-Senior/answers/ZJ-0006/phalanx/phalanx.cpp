#include <bits/stdc++.h>
using namespace std;

int a[2333][2333];

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	int n, m, q;
	scanf("%d%d%d", &n, &m, &q);
	int cnt = 0;
	for (int i = 1; i <= n; ++i) 
		for (int j = 1; j <= m; ++j) a[i][j] = ++cnt;
	for (int i = 1; i <= q; ++i) {
		int x, y; scanf("%d%d", &x, &y);
		printf("%d\n", a[x][y]); 
		int c = a[x][y];
		for (int j = y; j < m; ++j) a[x][j] = a[x][j + 1];
		for (int j = x; j < n; ++j) a[j][m] = a[j + 1][m];
		a[n][m] = c;
	}
	return 0;
}
#include <bits/stdc++.h>
using namespace std;

#define lowbit(x) ((x) & (-x))

const int maxM = 1005;
const int INF = (int)(1e9) + 100;
int u[maxM], v[maxM], w[maxM];
int f[12][1 << 12], g[1 << 12][1 << 12], mi[1 << 12];

inline void chkmin(int& x, int v) {
	x = min(x, v);
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int n, m; 
	scanf("%d%d", &n, &m);
	for (int i = 0; i < m; ++i) {
		scanf("%d%d%d", &u[i], &v[i], &w[i]);
		--u[i]; --v[i];
	}
	memset(f, 0x3f, sizeof(f)); int ALL = (1 << n) - 1;
	for (int i = 1; i < (1 << n); ++i) {
		for (int j = 0; j < n; ++j) mi[1 << j] = INF;
		for (int j = 0; j < m; ++j) if (i & (1 << u[j])) chkmin(mi[1 << v[j]], w[j]);
		for (int j = 0; j < m; ++j) if (i & (1 << v[j])) chkmin(mi[1 << u[j]], w[j]);
		for (int j = 1; j < (1 << n); ++j) g[i][j] = min(mi[lowbit(j)] + g[i][j ^ lowbit(j)], INF);
	}
	for (int i = 0; i < n; ++i) f[0][ALL ^ (1 << i)] = 0; 
	for (int p = 1; p < n; ++p) 
		for (int i = 1; i < (1 << n); ++i) 
			for (int j = i; j; j = (j - 1) & i) if (g[ALL ^ i][j] < INF)
				chkmin(f[p][i ^ j], f[p - 1][i] + g[ALL ^ i][j] * p);
	int res = INF;
	for (int p = 0; p < n; ++p) res = min(res, f[p][0]);
	return printf("%d\n", res), 0;
}
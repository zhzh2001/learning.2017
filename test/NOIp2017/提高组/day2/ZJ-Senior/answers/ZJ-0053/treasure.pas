var   n,m,i,j,u,v,x,min,sum:longint;
vis:array[0..20]of boolean;
f:array[0..20,0..20]of longint;
function minn(a,b:longint):longint;
begin if a>b then exit(b) else exit(a); end;
procedure dfs(u,k:longint);
var  i:longint;
begin
  for i:=1 to n do
    if (f[i,u]>0)and(f[i,u]<>maxlongint) then
      begin
        if vis[i]=false then
          begin
            vis[i]:=true;
            sum:=sum+k+1;
            dfs(i,k+1);
          end;
      end;
end;
begin
assign(input,'treasure.in'); reset(input);
assign(output,'treasure.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      if i<>j then f[i,j]:=maxlongint;
  for i:=1 to m do
    begin
      read(u,v,x);
      f[u,v]:=minn(f[u,v],x);
      f[v,u]:=f[u,v];
    end;
  min:=maxlongint;
  for i:=1 to n do
    begin
      sum:=0;
      fillchar(vis,sizeof(vis),false);
      vis[i]:=true;
      dfs(i,0);
      if sum<min then min:=sum;
    end;
  writeln(min*x);
close(input);
close(output);
end.
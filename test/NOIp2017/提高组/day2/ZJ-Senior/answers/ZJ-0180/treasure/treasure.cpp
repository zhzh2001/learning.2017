#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=15;
int n,m,a[maxn][maxn],e=0,min=0;
bool v[100]={0},b[maxn][maxn],v2[maxn];
struct P{
	int x,y;}p[100];
int fa(int x,int y){
	int sum=0;
	for(int i=1;i<=n;i++)if(!v2[i]&&b[x][i])v2[i]=1,sum=sum+a[x][i]*y+fa(i,y+1);
	return sum;
}
void work(){
	//for(int i=1;i<=e;i++)if(v[i]) printf("%d ",i);printf("\n");
	memset(b,0,sizeof(b));
	for(int i=1;i<=e;i++)if(v[i]){int s=p[i].x,t=p[i].y;b[s][t]=b[t][s]=1;}		
	for(int i=1;i<=n;i++){
		memset(v2,0,sizeof(v2));v2[i]=1;
		int tem=fa(i,1);if(!min||(min&&min>tem)) min=tem;}
}
void dfs(int x){
	if(x==n){work();return;}
	else{
		for(int i=1;i<=e;i++) if(!v[i]){
			v[i]=1;dfs(x+1);v[i]=0;}
		}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j,s,t,w;
	memset(a,0,sizeof(a));
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++){
		scanf("%d%d%d",&s,&t,&w);
		if(!a[s][t]||(a[s][t]&&a[s][t]>w))a[s][t]=a[t][s]=w;
	}
	for(i=1;i<n;i++) for(j=i+1;j<=n;j++)if(a[i][j])p[++e].x=i,p[e].y=j;
	dfs(1);
	printf("%d",min);
	return 0;
}

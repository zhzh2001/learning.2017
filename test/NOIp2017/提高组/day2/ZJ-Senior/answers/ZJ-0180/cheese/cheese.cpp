#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=1010;
struct P{
	int x,y,z;}p[maxn];
int T,n,h,r,a[maxn][maxn],f[maxn],q[maxn];
int check(int x,int y){
	long long d=(p[x].x-p[y].x)*(p[x].x-p[y].x)+(p[x].y-p[y].y)*(p[x].y-p[y].y)+(p[x].z-p[y].z)*(p[x].z-p[y].z);
	if((1LL*r*r*4)>=d) return 1;else return 0;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int i,j;
	scanf("%d",&T);
	while(T--){
		memset(a,0,sizeof(a));
		memset(f,0,sizeof(f));
		int he=0,ta=0;bool bo=false;
		scanf("%d%d%d",&n,&h,&r);
		for(i=1;i<=n;i++){scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);if(p[i].z<=r)a[0][i]=a[i][0]=1;if(p[i].z>=h-r){a[i][n+1]=a[n+1][i]=1;f[i]=1;q[ta++]=i;}}ta--;
		for(i=1;i<n;i++)for(j=i+1;j<=n;j++)a[i][j]=a[j][i]=check(i,j);
		while(he<=ta){
			int t=q[he];he++;
			for(i=1;i<=n;i++)if(!f[i]&&a[t][i]){
				q[++ta]=i;f[i]=1;if(a[i][0]){bo=true;break;}
			}
			if(bo) break;
		}
		if(bo) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

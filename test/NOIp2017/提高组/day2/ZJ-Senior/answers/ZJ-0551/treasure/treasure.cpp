#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
using namespace std;
const int N=15;
int n,m,g[N][N],ans,depth[N],vis[N];
struct go{
	int x,v,d;
	go (){}
	go (int a,int b,int c){
		x=a,v=b,d=c;
	}
};
bool cmp(go a,go b){
	int x=a.v*a.d,y=b.v*b.d;
	if (x!=y)
		return x<y;
	return a.d<b.d;
}
void dfs(int x,int v,int cnt){
	if (cnt==n){
		ans=min(ans,v);
		return;
	}
	int a[N],az=0,b[N],bz=0,tz=0;
	go t[N*N];
	if (v>=ans)
		return;
	for (int i=1;i<=n;i++)
		if (vis[i])
			a[++az]=i;
		else
			b[++bz]=i;
	for (int i=1;i<=az;i++)
		for (int j=1;j<=bz;j++)
			if (g[a[i]][b[j]]!=-1)
				t[++tz]=go(b[j],g[a[i]][b[j]],depth[a[i]]+1);
	sort(t+1,t+tz+1,cmp);
	for (int i=1;i<=tz;i++){
		int v_=v+t[i].v*t[i].d;
		if (v_>=ans)
			return;
		depth[t[i].x]=t[i].d;
		vis[t[i].x]=1;
		dfs(t[i].x,v_,cnt+1);
		vis[t[i].x]=0;
	}
}
int ev;
bool checkv(){
	int v=-1;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (g[i][j]==-1)
				continue;
			else if (v==-1)
				v=g[i][j];
			else if (v!=g[i][j])
				return 0;
	ev=v;
	return 1;
}
int getans(int rt){
	memset(depth,0,sizeof depth);
	memset(vis,0,sizeof vis);
	vis[rt]=1;
	depth[rt]=0;
	int res=0;
	int q[N],head,tail;
	head=tail=0;
	q[++tail]=rt;
	while (head<tail){
		int x=q[++head];
		res+=depth[x];
		for (int i=1;i<=n;i++)
			if (!vis[i]&&g[x][i]!=-1){
				vis[i]=1;
				depth[i]=depth[x]+1;
				q[++tail]=i;
			}
	}
	return res*ev;
}
void solve1(){
	int ans=1<<30;
	for (int i=1;i<=n;i++)
		ans=min(ans,getans(i));
	printf("%d",ans);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(g,-1,sizeof g);
	for (int i=1,a,b,c;i<=m;i++){
		scanf("%d%d%d",&a,&b,&c);
		if (g[a][b]==-1)
			g[a][b]=g[b][a]=c;
		else
			g[a][b]=g[b][a]=min(g[a][b],c);
	}
	if (checkv()){
		solve1();
		fclose(stdin);fclose(stdout);
		return 0;
	}
	ans=1<<30;
	memset(vis,0,sizeof vis);
	memset(depth,0,sizeof depth);
	for (int i=1;i<=n;i++){
		vis[i]=1;
		depth[i]=0;
		dfs(i,0,1);
		vis[i]=0;
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
//CQZAK give Me Forces!!
//SYYAK give Me Luck!!
//CLJAK give Me Confidence!!
//It's believed that they'll AK!!
//DFS(DaFaShi)Chu Qi Ji

#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
using namespace std;
const int N=300005,Q=300005;
int n,m,q;
struct Point{
	int x,y;
}p[Q];
int a1[505][50005],la[50005],Ha[505],hs,ys[50005];
void CLJAK(){
	int hs_=1;
	for (int i=2;i<=hs;i++)
		if (Ha[i]!=Ha[i-1])
			Ha[++hs_]=Ha[i];
	hs=hs_;
}
void solve1(){
	hs=0;
	for (int i=1;i<=q;i++)
		Ha[++hs]=p[i].x;
	Ha[++hs]=n;
	sort(Ha+1,Ha+hs+1);
	CLJAK();
	memset(ys,-1,sizeof ys);
	for (int i=1;i<=hs;i++)
		ys[Ha[i]]=i;
	for (int i=1;i<=hs;i++)
		for (int j=1;j<=m;j++)
			a1[i][j]=(Ha[i]-1)*m+j;
	for (int i=1;i<=n;i++)
		la[i]=m*i;
	for (int i=1;i<=q;i++){
		int x=p[i].x,y=p[i].y;
		int v=a1[ys[x]][y];
		printf("%d\n",v);
		la[n+1]=v;
		for (int j=y;j<m;j++)
			a1[ys[x]][j]=a1[ys[x]][j+1];
		for (int j=x;j<=n;j++){
			la[j]=la[j+1];
			if (ys[j]!=-1)
				a1[ys[j]][m]=la[j+1];
		}
	}
}
bool check1(){
	for (int i=1;i<=q;i++)
		if (p[i].x!=1)
			return 0;
	return 1;
}
int c[N*3],a2[N*3];
int lowbit(int x){
	return x&-x;
}
void add(int x,int d){
	for (;x<=n+m+q-1;x+=lowbit(x))
		c[x]+=d;
}
int sum(int x){
	int ans=0;
	for (;x>0;x-=lowbit(x))
		ans+=c[x];
	return x;
}
int find(int x){
	int L=1,R=n+m+q-1,mid,ans=-1;
	while (L<=R){
		mid=(L+R)>>1;
		if (sum(mid)>=x)
			R=mid-1,ans=mid;
		else
			L=mid+1;
	}
	return ans;
}
void solve2(){
	memset(c,0,sizeof c);
	for (int i=1;i<=n+m-1;i++)
		add(i,1);
	for (int i=1;i<=m;i++)
		a2[i]=i;
	for (int i=2;i<=n;i++)
		a2[i+m-1]=i*m;
	for (int i=1;i<=q;i++){
		int y=p[i].y;
		int pos=find(y);
		printf("%d\n",a2[pos]);
		a2[n+m-1+i]=a2[pos];
		add(pos,-1);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=q;i++)
		scanf("%d%d",&p[i].x,&p[i].y);
	if (q<=500)
		solve1();
	else if (check1())
		solve2();
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>
using namespace std;
typedef unsigned long long ULL;
const int N=1005;
int T,n,rr,hh,fa[N];
ULL h,r,rd;
struct Point{
	int x,y,z;
}p[N];
ULL sqr(ULL x){
	return x*x;
}
ULL dist(Point a,Point b){
	return sqr(ULL(a.x)-ULL(b.x))+sqr(ULL(a.y)-ULL(b.y))+sqr(ULL(a.z)-ULL(b.z));
}
int getf(int k){
	return fa[k]==k?k:fa[k]=getf(fa[k]);
}
void merge(int a,int b){
	int aa=getf(a),bb=getf(b);
	if (aa!=bb)
		fa[aa]=bb;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d",&n,&hh,&rr);
		h=hh,r=rr,rd=sqr(r*2);
		for (int i=1;i<=n;i++)
			scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
		for (int i=1;i<=n;i++)
			fa[i]=i;
		fa[0]=0,fa[n+1]=n+1;//0 表示下面，n+1表示上面
		for (int i=1;i<=n;i++){
			if (sqr(p[i].z)<=sqr(r))
				merge(i,0);
			if (sqr(p[i].z-h)<=sqr(r))
				merge(i,n+1);
		} 
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
				if (dist(p[i],p[j])<=rd)
					merge(i,j);
		puts(getf(0)==getf(n+1)?"Yes":"No");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

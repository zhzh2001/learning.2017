#include<cstdio>
#include<cstring>
const int iinf=0x7f;
const int inf=0x7f7f7f7f;
int len[31][31];
int minval=0x7f7f7f7f,lim;
int vis[5002];
int n,m;
void dfs(int u,int dis,int val,int state){
	if(val>minval) return ;
	if(state==lim) {
		minval=val;
		return ;
	}
	int delta=0,base=state;
	for(int v=1;v<=n;v++) if(len[u][v]==inf) base=base|(1<<(v-1));
	memset(vis,false,sizeof(vis));
	for(int i=0,real;i<=lim;i++){
		real=i-(i&base);delta=0;
		if(vis[real]) continue;
		vis[real]=true;
		for(int v=1;v<=n;v++) if(real&(1<<(v-1))){
			delta+=len[u][v]*dis;
		}
		for(int v=1;v<=n;v++) if(real&(1<<(v-1))){
			dfs(v,dis+1,val+delta,real|state);
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(len,iinf,sizeof(len));
	for(int i=1,x,y,z;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if(len[x][y]>z) len[x][y]=len[y][x]=z;
	}
	for(int i=1;i<=n;i++) len[i][i]=inf;
	lim=(1<<n)-1;
	for(int i=1;i<=n;i++) {
		dfs(i,1,0,1<<(i-1));
	}
	printf("%d",minval);
	return 0;
}

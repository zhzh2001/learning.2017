#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 1000
typedef long long ll;
struct Point{
	ll x,y,z;
	Point(){}
	Point(ll _x,ll _y,ll _z):x(_x),y(_y),z(_z){};
}p[MAXN+10];
inline ll sdis(Point a,Point b){
	return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
}
inline bool cmp(Point a,Point b){
	if(a.z==b.z&&a.y==b.y) return a.x<b.x;
	if(a.z==b.z) return a.y<b.y;
	return a.z<b.z; 
}
int n;
ll h,r,dd;
int vis[MAXN+10];
bool dfs(int u){
	if(p[u].z+r>=h) return true;
	for(int v=u+1;v<=n;v++)if(!vis[v]&&dis(p[u],p[v])<=dd){
		vis[v]=true;
		if(dis(v)) return true; 
	}
	return false;
}
inline bool Solve(){
	if(p[1].z-r>0) return false;
	int u=1;
	bool flag=false;
	while(!flag&&u<=n&&p[u].z-r<=0&&!vis[u]) flag=dfs(u);
	return flag;
}
int main(){
	freopen("chese.in","r",stdin);
	freopen("chese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		dd=4*r*r;
		for(int i=1,x,y,z;i<=n;i++){
			scanf("%lld%lld%lld",&x,&y,&z);
			p[i]=Point(x,y,z);
		}
		std::sort(p+1,p+n+1,cmp);
		if(Solve()) puts("Yes");
		else puts("No");
	}
	return 0;
}

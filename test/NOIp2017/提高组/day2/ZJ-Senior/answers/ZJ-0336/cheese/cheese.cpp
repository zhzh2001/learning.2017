#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 1000
typedef long long ll;
struct Point{
	ll x,y,z;
	Point(){}
	Point(ll _x,ll _y,ll _z):x(_x),y(_y),z(_z){};
}p[MAXN+50];
inline ll sdis(Point a,Point b){
	return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
}
inline bool cmp(Point a,Point b){
	return a.z<b.z; 
}
int n,T;
ll h,r,dd;
int vis[MAXN+50];
bool dfs(int u){
	vis[u]=true;
	if(p[u].z+r>=h)
		return true;
	for(int v=1;v<=n;v++)if(!vis[v]&&sdis(p[u],p[v])<=dd){
		if(dfs(v)) return true;
	}
	return false;
}
inline bool Solve(){
	int u=1;
	bool flag=false;
	memset(vis,false,sizeof(vis));
	if(p[u].z-r>0) return false;
	while(!flag&&u<=n&&p[u].z-r<=0){
		if(!vis[u]) flag=dfs(u);
		u++;
	}
	return flag;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	ll x,y,z;
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		dd=1LL*4*r*r;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&x,&y,&z);
			p[i]=Point(x,y,z);
		}
		std::sort(p+1,p+n+1,cmp);
		if(Solve()) puts("Yes");
		else puts("No");
	}
	return 0;
}

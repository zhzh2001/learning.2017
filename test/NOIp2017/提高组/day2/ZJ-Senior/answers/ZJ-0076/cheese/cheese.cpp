#include<cstdio>
using namespace std;
int f[1000005];
long long x[1000005],y[1000005],z[1000005],sz[1000005];
int find(int u){
	if (f[u]!=u) f[u]=find(f[u]);
	return f[u];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	while (t--){
		int n,h;
		long long r;
		scanf("%d%d%lld",&n,&h,&r);
		for (int i=1; i<=n; i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=0; i<=n+1; i++) f[i]=i,sz[i]=1;
		for (int i=1; i<=n; i++)
			if (z[i]<=r){
				int fx=find(0);
				int fy=find(i);
				if (fx==fy) continue;
				if (sz[fx]<sz[fy]){
					f[fx]=fy;
					sz[fy]+=sz[fx];
				}
				else{
					f[fy]=fx;
					sz[fx]+=sz[fy];
				}
			}
		for (int i=1; i<=n; i++)
			if (h-z[i]<=r){
				int fx=find(n+1);
				int fy=find(i);
				if (fx==fy) continue;
				if (sz[fx]<sz[fy]){
					f[fx]=fy;
					sz[fy]+=sz[fx];
				}
				else{
					f[fy]=fx;
					sz[fx]+=sz[fy];
				}
			}
		for (int i=1; i<=n; i++){
			for (int j=1; j<i; j++){
				int a=find(0);
				int b=find(n+1);
				if (a==b) break;
				int fx=find(i);
				int fy=find(j);
				if (fx==fy) continue;
				long long dis=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
				if (dis<=r*r*4ll){
					if (sz[fx]<sz[fy]){
						f[fx]=fy;
						sz[fy]+=sz[fx];
					}
					else{
						f[fy]=fx;
						sz[fx]+=sz[fy];
					}
				}
			}
		}
		if (find(0)==find(n+1)) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
int num,n,m,a[1000005],map[5005][5005],dis[25][5005],dis1[25][25],v[1200005],next[1200005],head[1200005],dp[20][5005];
void dfs(int t,int sum){
	if (t>n){
		for (int i=1; i<=n; i++){
			for (int j=1; j<=n; j++) {
				if (i==j) continue;
				if (a[j]==1) dis[i][sum]=min(dis[i][sum],dis1[i][j]);
			}
		}
		return;
	}
	a[t]=0;
	dfs(t+1,sum<<1);
	a[t]=1;
	dfs(t+1,sum<<1|1);
}
int check(int x,int y){
	for (int i=1; i<=n; i++){
		if ((x&1) && (y&1)) return 0;
		x/=2;
		y/=2;
	}
	return 1;
}
void add(int a,int b){
	num++;
	v[num]=b;
	next[num]=head[a];
	head[a]=num;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++)
			dis1[i][j]=1e8;
	int maxx=1;
	for (int i=1; i<=n; i++) maxx<<=1;
	for (int i=1; i<=m; i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		dis1[x][y]=min(dis1[x][y],z);
		dis1[y][x]=min(dis1[y][x],z);
	}
	for (int i=1; i<=n; i++)
		for (int j=0; j<maxx; j++)
			dis[i][j]=1e8;
	dfs(1,0);
	for (int i=0; i<maxx; i++)
		for (int j=0; j<maxx; j++)
			if (check(i,j)){
				for (int k=1; k<=n; k++){
					if (j&(1<<n-k)) map[i][j]+=dis[k][i];
				}
				add(i,j);
			}
	for (int i=1; i<=n; i++)
		for (int j=0; j<maxx; j++)
			dp[i][j]=1e8;
	for (int i=1; i<=n; i++)
		dp[1][1<<(i-1)]=0;
	for (int len=1; len<n; len++)
		for (int u=0; u<maxx; u++){
			if (dp[len][u]==1e8) continue;
			for (int i=head[u]; i; i=next[i]){
				int V=v[i];
				if (map[u][V]>=1e8) continue;
				dp[len+1][u|V]=min(dp[len+1][u|V],dp[len][u]+map[u][V]*len);
			}
		}
	int ans=1e8;
	for (int len=1; len<=n; len++)
		ans=min(ans,dp[len][maxx-1]);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

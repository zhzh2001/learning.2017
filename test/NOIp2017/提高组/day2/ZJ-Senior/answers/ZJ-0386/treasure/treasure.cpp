#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
int n,m,d[20],b[20]={0},a[20][20],x,y,z,f[20];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
	a[i][j]=50000000;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (z<a[x][y]){
			a[x][y]=a[y][x]=z;
		}
	}
	long long an=2147483647;
	for (int s=1;s<=n;s++){
		for (int i=1;i<=n;i++) d[i]=50000000;
		for (int i=1;i<=n;i++) f[i]=50000000;
		memset(b,0,sizeof(b));
		d[s]=0;f[s]=1;
		long long ans=0;
		for (int i=1;i<=n;i++){
			int ma=1000000000,k;
			for (int j=1;j<=n;j++){
				if (!b[j]&&d[j]<ma){
					ma=d[j];k=j;
				}
			}
			b[k]=1;ans+=d[k];
			for (int j=1;j<=n;j++){
				if (!b[j]&&f[k]*a[k][j]<d[j]){
					d[j]=f[k]*a[k][j];
					f[j]=f[k]+1;
				}
			}
		}
		if (ans<an) an=ans;
	}
	cout<<an;
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#define LL long long
using namespace std;
const int maxn=1005;
int T,n,H,R,st,gl;
LL K;
bool dst[maxn],cst[maxn][maxn],vis[maxn];
struct Point
{
	int x,y,z;
}a[maxn];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++; 
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
bool DIJ()
{
	memset(vis,0,sizeof(vis));
	memset(dst,0,sizeof(dst));
	dst[st]=1;
	for (int i=0; i<=n+1; i++)
	{
		int k;
		for (int j=0; j<=n+1; j++)
		 if (!vis[j]&&dst[j]) k=j;
		if (k==n+1) return 1;
		vis[k]=1;
		for (int j=0; j<=n+1; j++)
		 if (!vis[j]&&cst[k][j]) dst[j]=1;
	}
	return 0;
}
LL Dst(int x,int y,int z) {return (LL)x*x+(LL)y*y+(LL)z*z;}
void Work()
{
	memset(cst,0,sizeof(cst));
	n=Read(); H=Read(); R=Read(); K=(LL)4*R*R;
	st=0; gl=n+1;
	for (int i=1; i<=n; i++) a[i]=(Point){Read(),Read(),Read()};
	for (int i=1; i<=n; i++)
	 if (a[i].z<=R) cst[0][i]=1;
	for (int i=1; i<=n; i++)
	 if (a[i].z>=H-R) cst[i][n+1]=1;
	for (int i=1; i<n; i++)
	 for (int j=i+1; j<=n; j++)
	  if (Dst(a[i].x-a[j].x,a[i].y-a[j].y,a[i].z-a[j].z)<=K) cst[i][j]=cst[j][i]=1;
	if (DIJ()) printf("Yes\n"); else printf("No\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=Read();
	while (T--) Work();
	return 0;
}

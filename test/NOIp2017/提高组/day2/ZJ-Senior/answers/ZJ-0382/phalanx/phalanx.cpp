#include<cstdio>
using namespace std;
const int maxn=8005;
int n,m,Q,a[maxn][maxn];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++; 
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=Read(); m=Read(); Q=Read();
	for (int i=1; i<=n; i++)
	 for (int j=1; j<=m; j++)
	  a[i][j]=(i-1)*n+j;
	for (int i=1; i<=Q; i++)
	{
		int x=Read(),y=Read(),p=a[x][y];
		printf("%d\n",a[x][y]);
		for (int j=y; j<m; j++) a[x][j]=a[x][j+1];
		for (int j=x; j<n; j++) a[j][m]=a[j+1][m];
		a[n][m]=p;
	}
	return 0;
}

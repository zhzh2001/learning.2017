#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=15,maxv=4105;
int n,m,S,ans,sum,INF,cst[maxn][maxn],f[maxn][maxv][maxn][maxn];
bool vis[maxn];
inline char nc()
{
	static char buf[100000],*l,*r;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF; return *l++; 
}
inline int Read()
{
	int res=0,f=1; char ch=nc(),cc=ch;
	while (ch<'0'||ch>'9') cc=ch,ch=nc();
	if (cc=='-') f=-1;
	while (ch>='0'&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}
int min(int x,int y) {if (x<y) return x; return y;}
void Dfs(int x,int dep)
{
	vis[x]=1;
	for (int i=1; i<=n; i++)
	 if (!vis[i]&&cst[x][i]!=INF) sum+=(dep+1)*cst[x][i],Dfs(i,dep+1);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=Read(); m=Read(); S=1<<n;
	memset(cst,63,sizeof(cst)); INF=cst[0][0]; ans=INF;
	for (int i=1; i<=m; i++)
	{
		int x=Read(),y=Read(),z=Read();
		if (z<cst[x][y]) cst[x][y]=cst[y][x]=z;
	}
	memset(vis,0,sizeof(vis));
	for (int i=1; i<=n; i++)
	{
		memset(vis,0,sizeof(vis));
		 sum=0,Dfs(i,0);
		 if (sum<ans) ans=sum;
	}
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define N 1010
#define ll long long
struct edge{
	int next,to;
}e[N*N];
struct node{
	int x,y,z;
}a[N];
int n,h,T,r,head[N],cnt,vis[N];
ll rr;
void add(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
}
ll erc(ll o1,ll o2){
	return (o1-o2)*(o1-o2);
}
bool dfs(int x){
	if(x==n+1)return 1;
	bool f=0;
	for (int i=head[x];i;i=e[i].next){
		int tmp=e[i].to;
		if(!vis[tmp]){
			vis[tmp]=1;
			f=f||dfs(tmp);
		}
	}
	return f;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		memset(head,0,sizeof(head));
	    scanf("%d%d%d",&n,&h,&r);cnt=0;
	    rr=erc((ll)2*r,0);
	    for (int i=1;i<=n;i++)scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
	    for (int i=1;i<=n;i++)
	        if(a[i].z-r<=0)add(0,i);
	    for (int i=1;i<=n;i++)
	        for(int j=1;j<=n;j++)
		        if(i!=j){
	    	        ll tmp=erc((ll)a[i].x,(ll)a[j].x)+erc((ll)a[i].y,(ll)a[j].y)+erc((ll)a[i].z,(ll)a[j].z);
					if(tmp<=rr)add(i,j);
		        }
	    for (int i=1;i<=n;i++)
	        if(a[i].z+r>=h)add(i,n+1);
	    memset(vis,0,sizeof(vis));
	    if(dfs(0))printf("Yes\n");
	    else printf("No\n");
	}
	return 0;
} 

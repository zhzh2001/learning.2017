#include<bits/stdc++.h>
using namespace std;
#define N 20
const int inf=1000000000;
int n,m,dis[N][N],vis[N],cnt,dep[N];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	    for (int j=1;j<=n;j++)dis[i][j]=inf;
	for (int i=1;i<=m;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		if(dis[u][v]>w)dis[u][v]=dis[v][u]=w;
	}
		int ans=inf;
		for (int i=1;i<=n;i++){
			memset(dep,0,sizeof(dep));
			queue<int> q;int tmp=0;
			while(!q.empty())q.pop();
			q.push(i);dep[i]=1;
			while(!q.empty()){
				int x=q.front();q.pop();
				for (int j=1;j<=n;j++)
				    if(dis[x][j]<inf&&!dep[j]){
				    	tmp+=dis[x][j]*dep[x];
				    	dep[j]=dep[x]+1;q.push(j);
					}
			}
			ans=min(ans,tmp);
		}
		printf("%d\n",ans);
	return 0;
} 

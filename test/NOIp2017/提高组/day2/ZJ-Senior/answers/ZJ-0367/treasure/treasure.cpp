#include<cmath>
#include<ctime>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define MM 1000000007
using namespace std;
int tag[13],a[13][13],deep[13],n,m,x,y,z,tagg,limit,tag1;
bool bo[13],bt[13];LL ans;
char cc;int k;
char nc()
{
	static char buf[1000000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++;
}
struct Node
{
	int x,y;
};
inline void read(int&x)
{
	x=0;cc=nc();
	while ((cc<'0'||cc>'9')&&(cc!='-')) cc=nc();
	if (cc=='-') k=-1,cc=nc();else k=1;
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
	x*=k;
}
inline void read(LL&x)
{
	x=0;cc=nc();
	while ((cc<'0'||cc>'9')&&(cc!='-')) cc=nc();
	if (cc=='-') k=-1,cc=nc();else k=1;
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
	x*=k;
}
bool cmp(Node a,Node b)
{
	return a.x<b.x;
}
void sear(int x,int y,LL z)
{
	if (ans!=-1&&z>=ans) return;
	if (y==n)
	{
		ans=z;
		return;
	}
	bo[x]=false;int rec[n+1],rec1[n+1];
	for (int i=1;i<=n;i++) rec[i]=tag[i],rec1[i]=deep[i];
	for (int i=1;i<=n;i++)
		if (bo[i]&&a[x][i]!=MM&&(deep[x]*a[x][i]<tag[i]||tag[i]==-1||(deep[x]*a[x][i]==tag[i]&&deep[x]+1<deep[i]))) tag[i]=deep[x]*a[x][i],deep[i]=deep[x]+1;
	for (int i=n;i>=1;i--)
		if (bo[i])
		{
			sear(i,y+1,z+tag[i]);
		}
	bo[x]=true;
	for (int i=1;i<=n;i++) tag[i]=rec[i],deep[i]=rec1[i];
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);srand((unsigned)time(0));
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			a[i][j]=MM;
	for (int i=1;i<=m;i++)
	{
		read(x);read(y);read(z);
		if (a[x][y]>z) a[x][y]=a[y][x]=z;
	}
	ans=-1;
	for (int i=1;i<=n;i++) bo[i]=bt[i]=true,tag[i]=-1,deep[i]=n+1;
	for (int i=n;i>=1;i--) deep[i]=1,sear(i,1,0);
	printf("%lld\n",ans);
	return 0;
}                                    

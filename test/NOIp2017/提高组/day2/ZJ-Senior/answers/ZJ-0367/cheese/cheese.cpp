#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
using namespace std;
int t,w,f[1010],x,n,T;
LL a[1010],b[1010],c[1010],d[1010],r,R,h;
bool bo[1010];
char cc;int k;
char nc()
{
	static char buf[1000000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++;
}
inline void read(int&x)
{
	x=0;cc=nc();
	while ((cc<'0'||cc>'9')&&(cc!='-')) cc=nc();
	if (cc=='-') k=-1,cc=nc();else k=1;
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
	x*=k;
}
inline void read(LL&x)
{
	x=0;cc=nc();
	while ((cc<'0'||cc>'9')&&(cc!='-')) cc=nc();
	if (cc=='-') k=-1,cc=nc();else k=1;
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
	x*=k;
}
inline bool bfs()
{
	for (int i=1;i<=w;i++) 
	{
		bo[f[i]]=false;
		if (c[f[i]]+r>=h) return true;
	}
	while (t<=w)
	{
		x=f[t++];
		for (int i=1;i<=n;i++)
			if (bo[i])
				if (d[x]+d[i]-(a[x]*a[i]<<1)-(b[x]*b[i]<<1)-(c[x]*c[i]<<1)<=R)
				{
					f[++w]=i;bo[i]=false;
					if (c[i]+r>=h) return true;
				}
	}
	return false;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--)
	{
		read(n);read(h);read(r);
		R=r<<1;R*=R;t=1;w=0;
		for (int i=1;i<=n;i++) bo[i]=true,read(a[i]),read(b[i]),read(c[i]),d[i]=a[i]*a[i]+b[i]*b[i]+c[i]*c[i],c[i]<=r?f[++w]=i:0;
		if (bfs()) puts("Yes");
			else puts("No");
	}
	return 0;
}

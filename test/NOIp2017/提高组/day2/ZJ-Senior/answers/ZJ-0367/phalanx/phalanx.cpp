#include<cmath>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define N 300005
#define nc getchar
using namespace std;
int tag[N],l,r,mid,lena[N],lenb[N],n,m,q,now,x,y,t,blocks;
vector<int> a[N];
vector<int> b[N];
/*char nc()
{
	static char buf[1000000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1000000,stdin),p1==p2)?EOF:*p1++;
}*/
char cc;int k;
inline void read(int&x)
{
	x=0;cc=nc();
	while ((cc<'0'||cc>'9')&&(cc!='-')) cc=nc();
	if (cc=='-') k=-1,cc=nc();else k=1;
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
	x*=k;
}
inline void read(LL&x)
{
	x=0;cc=nc();
	while ((cc<'0'||cc>'9')&&(cc!='-')) cc=nc();
	if (cc=='-') k=-1,cc=nc();else k=1;
	while (cc>='0'&&cc<='9') x=x*10+cc-48,cc=nc();
	x*=k;
}
inline int charge(int x,int y)
{
	int t;
	if (x<=tag[0])
	{
		l=-1;r=lena[0]-1;
		while (l<r)
		{
			mid=(l+r+1)>>1;
			if (a[0][mid]-mid-1<x) l=mid;else r=mid-1;
		}
		if (l==-1) t=x;
			else t=a[0][l]+x-(a[0][l]-l-1);
		if (lena[0]<a[0].size()) a[0][lena[0]++]=t;
		else
		{
			a[0].push_back(t);
			lena[0]++;
		}
		t*=m;
		if (y==0) y=t;
		int i=lena[0]-1;
		while (i>0&&a[0][i]<a[0][i-1]) swap(a[0][i],a[0][i-1]),i--;
		tag[0]--;
		if (lenb[0]<b[0].size()) b[0][lenb[0]++]=y;else b[0].push_back(y),lenb[0]++;
	}
	else
	{
		x-=tag[0]+1;
		t=b[0][x];if (y==0) y=t;
		for (int i=x;i+1<lenb[0];i++) b[0][i]=b[0][i+1];lenb[0]--;
		if (lenb[0]<b[0].size()) b[0][lenb[0]++]=y;else b[0].push_back(y),lenb[0]++;
	}
	return t;
}
inline void build()
{
	
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	now=0;blocks=ceil(sqrt(q));
	for (int i=1;i<=n;i++) tag[i]=m-1;
	tag[0]=n;
	for (int i=1;i<=q;i++)
	{
		if (++now==blocks) build();
		read(x);read(y);
		if (y!=m)
		{
			
			if (y<=tag[x])
			{
				l=-1;r=lena[x]-1;
				while (l<r)
				{
					mid=(l+r+1)>>1;
					if (a[x][mid]-mid-1<y) l=mid;else r=mid-1;
				}
				if (l==-1) t=y;
					else t=a[x][l]+y-(a[x][l]-l-1);
				if (lena[x]<a[x].size()) a[x][lena[x]++]=t;else a[x].push_back(t),lena[x]++;
				int i=lena[x]-1;
				while (i>0&&a[x][i]<a[x][i-1]) swap(a[x][i],a[x][i-1]),i--;
				tag[x]--;
				t+=(x-1)*m;
				if (lenb[x]<b[x].size()) b[x][lenb[x]++]=charge(x,t);else b[x].push_back(charge(x,t)),lenb[x]++;
			}
			else
			{
				y-=tag[x]+1;
				t=b[x][y];
				for (int i=y;i+1<lenb[x];i++) b[x][i]=b[x][i+1];lenb[x]--;
				if (lenb[x]<b[x].size()) b[x][lenb[x]++]=charge(x,t);else b[x].push_back(charge(x,t)),lenb[x]++;
			}
		}
		else
			t=charge(x,0);
		printf("%d\n",t);
	}
	return 0;
}

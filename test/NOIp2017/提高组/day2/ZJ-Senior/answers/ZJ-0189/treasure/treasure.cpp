#include <iostream>
#include <cstring>
#include <vector>
#include <set>
#include <queue>
#include <cstdio>
#include <cmath>
#include <cstdlib>
using namespace std;
typedef long long ll;
#define pb push_back
#define fi first
#define se second
const int maxn = 13, inf = 0x3f3f3f3f;
int n,m,u,v,w;
int d[maxn],vis[maxn],g[maxn][maxn],f[1<<maxn][maxn];
int ans,acc,acc2,S,st;
void dfs(){
    if(acc>ans) return;
    if(S==(1<<n)-1) {
	ans = acc;
	return;
    }
    for(int i=1;i<=n;i++){
	if(S&(1<<i-1)){
	    for(int j=1;j<=n;j++){
		if(!(S&(1<<j-1)) && g[i][j]>=0 ){
		    S|=1<<j-1;
		    d[j] = d[i] + 1;
		    acc += d[i] * g[i][j];
		    int S_ = ((1<<n)-1)^(S^(1<<j-1));
		    if(acc+f[S_][j] <= ans) 
			dfs();
		    acc -= d[i] * g[i][j];
		    S^=1<<j-1;
		}
	    }
	}
    }
    if(f[S][st]>=0) f[S][st] = min(f[S][st],acc);
    else f[S][st] = acc;
}
int main(){
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
    cin>>n>>m;
    for(int i=1;i<=n;i++){
	for(int j=1;j<=n;j++){
	    g[i][j] = -1;
	}
    }
    for(int i=1;i<=n;i++) g[i][i] = 0;
    for(int i=0;i<m;i++){
	cin>>u>>v>>w;
	if(g[u][v]<0 || g[u][v]>w)g[u][v] = g[v][u] = w;
    }
    bool flag = false;
    for(int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
	    if(g[i][j]>0 && g[i][j] !=w){
		flag=true;
	    }
	}
    }
    ans = 0x3f3f3f3f;
    if(flag){
	memset(f,-1,sizeof f);
	for(int i=1;i<=n;i++){
	    st=i;
	    acc=0;
	    acc2=0;
	    S=1<<i-1;
	    memset(vis,0,sizeof vis);
	    memset(d,0x3f,sizeof d);
	    d[i] = 1;
	    dfs();
	}
    }else{
	for(int i=1;i<=n;i++){
	    int acc=0;
	    for(int j=1;j<=n;j++){
		vis[j] = 0;
		d[j] = 0x3f3f3f3f;
	    }
	    d[i] = 0;
	    for(int j=1;j<=n;j++){
		int k=0,mn = 0x3f3f3f3f;
		for(int h = 1;h<=n;h++){
		    if(!vis[h] && mn > d[h]){
			k=h;
			mn = d[h];
		    }
		}
		acc += d[k];
		vis[k] = 1;
		for(int h=1;h<=n;h++){
		    if(g[h][k]>=0) d[h] = min(d[h], d[k] + g[h][k]);
		}
	    }
	    ans = min(ans,acc);
	}
    }
    cout<<ans<<endl;
    return 0;
}

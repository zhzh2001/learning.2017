#include <iostream>
#include <cstring>
#include <vector>
#include <set>
#include <queue>
#include <cstdio>
#include <cmath>
#include <cstdlib>
using namespace std;
typedef long long ll;
#define pb push_back
#define fi first
#define se second
const int maxn = 1010,maxq=30010;
int n,m,g[maxn][maxn],q,xs[maxn],ys[maxn],vis[maxn];
int a[maxq*2];
int x,y;
void mv(){
    cout<<g[x][y]<<endl;
    int tm = g[x][y];
    for(int j=y+1;j<=m;j++)
	g[x][j-1] = g[x][j];
    for(int j=x+1;j<=n;j++){
	g[j-1][m] = g[j][m];
    }
    g[n][n] = tm;
}
int main(){
  freopen("phalanx.in","r",stdin);
  freopen("phalanx.out","w",stdout);
    cin>>n>>m>>q;
    if(n<=1000 && m<=1000){
	for(int i=1;i<=n;i++){
	    for(int j=1;j<=m;j++){
		g[i][j] = (i-1)*m+j;
	    }
	}
	for(int i=0;i<q;i++){
	    cin>>x>>y;
	    mv();
	}
    }else {
	for(int i=0;i<q;i++) {
	    cin>>xs[i]>>ys[i];
	    vis[xs[i]] = 1;
	}
	if(q<=500){
	    int k=1;
	    for(int i=1;i<=n;i++){
		if(vis[i]){
		    for(int j=1;j<=m;j++){
			g[k][j] = (i-1)*m+j;
		    }
		    k++;
		}
	    }
	    n=k-1;
	    for(int i=0;i<q;i++){
		x = xs[i], y = ys[i];
		mv();
	    }
	}else {
	    bool flag=true;
	    for(int i=0;i<q;i++){
		if(xs[i]!=1){
		    flag=false;
		    break;
		}
	    }
	    if(flag){
		if(n==1){
		    for(int i=1;i<=m;i++) a[i] = i;
		}else{
		    for(int i=1;i<=m;i++) {
			a[i] = i;
		    }
		    for(int i=2;i<=n;i++){
			a[i+m-1] = i*m;
		    }
		    n+=m-1;
		}
	    }
	}
    }
    return 0;
}

#include <iostream>
#include <cstring>
#include <vector>
#include <set>
#include <queue>
#include <cstdio>
#include <cmath>
#include <cstdlib>
using namespace std;
typedef long long ll;
#define pb push_back
const double eps = 1e-6;
const int maxn = 1010;
int n,T,h,r,ist[maxn],isd[maxn];
vector<int> to[maxn];
double x[maxn],y[maxn],z[maxn];
int d[maxn];
void dfs(int u){
    d[u] = 1;
    for(int i=0;i<to[u].size();i++){
	if(!d[to[u][i]]){
	    dfs(to[u][i]);
	}
    }
}
#define Q(x) (x[i]-x[j])*(x[i]-x[j])
double dis(int i,int j){
    return sqrt(Q(x)+Q(y)+Q(z));
}
int main(){
    freopen("cheese.in","r",stdin);
    freopen("cheese.out","w",stdout);
    cin>>T;
    while(T--){
	cin>>n>>h>>r;
	memset(ist,0,sizeof ist);
	memset(isd,0,sizeof isd);
	memset(d,0,sizeof d);
	for(int i=0;i<n;i++){
	    to[i].resize(0);
	    cin>>x[i]>>y[i]>>z[i];
	    if(z[i]+r>=h-eps){
		ist[i] = 1;
	    }
	    if(z[i]-r<=eps){
		isd[i] = 1;
	    }
	}
	for(int i=0;i<n;i++){
	    for(int j=0;j<n;j++){
		if(dis(i,j)<=2*r+eps){
		    to[i].pb(j);
		}
	    }
	}
        for(int i=0;i<n;i++) {
	    if(isd[i]) dfs(i);
	}
	bool flag=false;
	for(int i=0;i<n;i++){
	    if(d[i] && ist[i]){
		flag=true;
		break;
	    }
	}
	puts(flag?"Yes":"No");
    }
    return 0;
}

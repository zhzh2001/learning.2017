#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <queue>
#include <algorithm>
typedef long long LL;
using namespace std;
const int INF=1e9+7,N=14;

int n,m,Ans,ST;
int d[N],Minv[N],ds[N];

vector<int> G[N],W[N];
inline void Add(int a,int b,int c){
	G[a].push_back(b);
	W[a].push_back(c);
	return;
}

inline int H(){
	int cc=0;
	for (int i=0;i<n;++i)
		if (d[i]==-1) cc+=ds[i]*Minv[i];
	return cc;
}

void DFS(int st,int sum){
	if (sum+H()>=Ans) return;
	if (st==ST){
		Ans=min(Ans,sum);
		return;
	}
	
	for (int i=0;i<n;++i) if (!(st & (1<<i))){
		for (int j=0;j<(int)G[i].size();++j){
			int v=G[i][j],w=W[i][j];
			if (!(st & (1<<v))) continue;
			
			int val=d[v]*w;
			d[i]=d[v]+1;
			DFS(st|(1<<i),sum+val);
			d[i]=-1;
		}
	}
}

queue<int> Q;
void BFS(int s){
	while (!Q.empty()) Q.pop();
	memset(ds,-1,sizeof ds);
	ds[s]=1;
	Q.push(s);
	while (!Q.empty()){
		int x=Q.front(); Q.pop();
		for (int i=0;i<(int)G[x].size();++i){
			int v=G[x][i];
			if (ds[v]==-1) ds[v]=ds[x]+1,Q.push(v);
		}
	}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for (int i=0;i<n;++i) Minv[i]=INF;
	
	for (int i=1;i<=m;++i){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		--x; --y;
		Add(x,y,z); Add(y,x,z);
		Minv[x]=min(Minv[x],z);
		Minv[y]=min(Minv[y],z);
	}
	
	ST=(1<<n)-1;
	Ans=INF;
	for (int i=0;i<n;++i){
		BFS(i);
		memset(d,-1,sizeof d);
		d[i]=1;
		DFS(1<<i,0);
	}
	
	printf("%d\n",Ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <queue>
#include <set>
#include <map>
#include <algorithm>
typedef long long LL;
using namespace std;
const int INF=1e9+7,N=1010,Lim=900000;


void Read(int &x){
	x=0; char ch;
	for (ch=getchar();ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';x=(x<<3)+(x<<1)+ch-'0',ch=getchar());
}
char Fo[20];
void Println(LL x){
	int le=0;
	while (x) Fo[++le]=x%10+'0',x/=10;
	for (int i=le;i>=1;--i) putchar(Fo[i]);
	putchar('\n');
}


int n,m,Q,Maxv;


int a[N][N];
void BaoLi(){
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j) a[i][j]=(i-1)*m+j;
	while (Q--){
		int x,y;
		scanf("%d%d",&x,&y);
		printf("%d\n",a[x][y]);
		for (int i=y;i<m;++i) swap(a[x][i],a[x][i+1]);
		for (int i=x;i<n;++i) swap(a[i][m],a[i+1][m]);
	}
	return;
}



struct Pair{
	int fir;
	LL sec;
	bool operator <(const Pair &b)const{
		return fir==b.fir? sec<b.sec : fir<b.fir;
	}
};

set<Pair> S;
LL B[Lim+9];

int tr[Lim*4];
inline int lowbit(int x){
	return x&(-x);
}
inline void Add(int x){
	while (x<=Lim) ++tr[x],x+=lowbit(x);
	return;
}
inline int Query(int x,int cc=0){
	while (x) cc+=tr[x],x-=lowbit(x);
	return cc;
}

void BuFen(){
	for (register int i=1;i<=m;++i){
		LL b=i;
		B[++Maxv]=b;
		S.insert((Pair){Maxv,b});
	}
	for (register int i=2;i<=n;++i){
		LL b=i*m;
		B[++Maxv]=b;
		S.insert((Pair){Maxv,b});
	}
	
	while (Q--){
		int x,y,pos;
		Read(x); Read(y);
		if (x>=2 && y<m){
			Println((LL)(x-1)*m+y);
			continue;
		}
		
		if (x==1) pos=y; else pos=m+y-1;
		
		int l=1,r=Maxv,A=1;
		while (l<=r){
			int mid=(l+r)>>1;
			if (mid-Query(mid)>=pos) A=mid,r=mid-1;
				else l=mid+1;
		}
		LL Res=B[A];
		Println(Res);
		
		S.erase((Pair){A,Res});
		B[++Maxv]=Res;
		S.insert((Pair){Maxv,Res});
		Add(A);
	}
	return;
}


int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	scanf("%d%d%d",&n,&m,&Q);
	if (n<=1000 && m<=1000) BaoLi();
	else BuFen();
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <queue>
#include <algorithm>
typedef long long LL;
using namespace std;
const int INF=1e9+7,N=1010;
const double EPS=1e-7;

int n,s,t;
LL h,r;
LL x[N],y[N],z[N];

int sz;
int las[N];
int to[N*N*4],pre[N*N*4];
inline void Add(int a,int b){
	++sz;
	to[sz]=b;
	pre[sz]=las[a];
	las[a]=sz;
	return;
}

inline void Reset(){
	sz=0;
	memset(las,0,sizeof las);
	return;
}

inline LL sqr(LL x){
	return x*x;
}

inline LL Dis(int i,int j){
	return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
}

queue<int> Q;
int vis[N];
int BFS(){
	while (!Q.empty()) Q.pop();
	memset(vis,0,sizeof vis);
	Q.push(s);
	vis[s]=1;
	
	while (!Q.empty()){
		int x=Q.front(); Q.pop();
		if (x==t) return 1;
		for (int i=las[x];i;i=pre[i]){
			int v=to[i];
			if (!vis[v]) vis[v]=1,Q.push(v);
		}
	}
	return 0;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	int T; scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		scanf("%lld%lld",&h,&r);
		Reset();
		
		for (int i=1;i<=n;++i) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		
		for (register int i=1;i<=n;++i)
			for (register int j=i+1;j<=n;++j)
				if (Dis(i,j) <= sqr(2*r)) Add(i,j),Add(j,i);
		
		s=n+1; t=s+1;
		for (int i=1;i<=n;++i) if (abs(z[i]) <= r) Add(s,i),Add(i,s);
		for (int i=1;i<=n;++i) if (abs(h-z[i]) <= r) Add(i,t),Add(t,i);
		
		if (BFS()) puts("Yes");
			else puts("No");
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}


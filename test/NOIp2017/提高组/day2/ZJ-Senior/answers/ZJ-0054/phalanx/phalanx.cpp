#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>

using namespace std;

inline int read()
{
	int k = 1, s = 0; char c = getchar();
	while (!isdigit(c)) {if (c == '-') k = -1; c = getchar();}
	while (isdigit(c)) {s = s * 10 + c - '0'; c = getchar();}
	return k * s;
}

int n, m, q, cnt, a[300010];

int s[5000][5000];

int sk[300010], ls[300010];

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	n = read(); m = read(); q = read();
	if (n == 1) {
		for (int i = 1; i <= m; i++) a[i] = i;
		for (int i = 1; i <= q; i++) {
			int x = read(), y = read();
			printf("%d\n", a[y]);
			int t = a[y];
			for (int j = y; j < m; j++) a[j] = a[j + 1];
			a[m] = a[y];
		}
	}
	else if (n > 5000 && m > 5000) {
		for (int i = 1; i <= m; i++) sk[i] = i;
		ls[1] = 2 * m;
		for (int i = 2; i <= n; i++) ls[i] = ls[i - 1] + m;
		for (int j = 1; j <= q; j++) {
			int x = read(), y = read();
			if (x == 1) {
				printf("%d\n", sk[y]);
				int t = sk[y];
				for (int i = y; i < m; i++) sk[i] = sk[i + 1];
				sk[m] = ls[1];
				for (int i = 1; i < n; i++) ls[i] = ls[i + 1];
				ls[n] = t;
			}
		}
	}
	else {
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) 
				s[i][j] = (i - 1) * m + j;
			
		for (int i = 1; i <= q; i++) {
			int x = read(), y = read();
			printf("%d\n", s[x][y]);
			int t = s[x][y];
			for (int i = y; i < m; i++) s[x][i] = s[x][i + 1];
			for (int i = x; i < n; i++) s[i][m] = s[i + 1][m];
			s[n][m] = t;	
		}
	}
	return 0;
}

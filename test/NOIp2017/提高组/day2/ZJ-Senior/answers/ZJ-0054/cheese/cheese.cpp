#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<list>

using namespace std;

inline int read()
{
	int k = 1, s = 0; char c = getchar();
	while (!isdigit(c)) {if (c == '-') k = -1; c = getchar();}
	while (isdigit(c)) {s = s * 10 + c - '0'; c = getchar();}
	return k * s;
}

int dist(int px1, int px2, int py1, int py2, int pz1, int pz2)
{
	return sqrt((px1 - px2) * (px1 - px2) + (py1 - py2) * (py1 - py2) + (pz1 - pz2) * (pz1 - pz2));
}

int t, n, h, r, x, y, z, flag1, flag2, kk, xx, yy, zz;
list<int> to[10000010];

void dfs(int x)
{
	for (list<int>::iterator it = to[x].begin(); it != to[x].end(); it++) {
		if (*it == h) {
			kk = 1;
			return;
		}
		if (kk == 1) return;
		dfs(*it);
	}	
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	t = read();
	for (int i = 1; i <= t; i++) {
		n = read(); h = read(); r = read();
		if (n == 1) {
			x = read(); y = read(); z = read();
			if (z <= r && h - z <= r) printf("Yes\n");
			else printf("No\n");
		}
		else {
			//memset(b, 0, sizeof(b));
			//memset(to, 0, sizeof(to));
			flag1 = flag2 = kk = xx = yy = zz = 0;
			for (int j = 1; j <= n; j++) {
				for (int k = 1; k <= h; k++) to[k].clear();
				x = read(); y = read(); z = read();
				if (z <= r) {
					to[0].push_back(z);
					flag1 = 1;
				}
				if (h - z <= r) {
					to[z].push_back(h);
					flag2 = 1;
				}
				if ((xx || yy || zz) && dist(x, xx, y, yy, z, zz) <= 2 * r) {
					if (zz >= z) to[z].push_back(zz);
					else to[zz].push_back(z);
				}
				xx = x; yy = y; zz = z;
			}
			
			if (flag1 && flag2) {
				dfs(0);
				if (kk) printf("Yes\n");
				else printf("No\n");
			}
			else printf("No\n");
		}
	}
	return 0;
}

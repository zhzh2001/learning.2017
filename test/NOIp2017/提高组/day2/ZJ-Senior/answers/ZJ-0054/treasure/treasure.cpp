#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<list>

using namespace std;

inline int read()
{
	int k = 1, s = 0; char c = getchar();
	while (!isdigit(c)) {if (c == '-') k = -1; c = getchar();}
	while (isdigit(c)) {s = s * 10 + c - '0'; c = getchar();}
	return k * s;
}

struct Edge{
	int to, we;
};

list<Edge> edge[100];

int n, m, a[100], b[100], k = -1, bg, ans, ww;

void addEdge(int u, int v, int w)
{
	edge[u].push_back((Edge){v, w});
	a[u]++;
	if (a[u] > k) bg = u, k = a[u];
}

void built(int root) {
	for (list<Edge>::iterator it = edge[root].begin(); it != edge[root].end(); it++) {
			if (b[it->to] > b[root] + 1 || !b[it->to]) {
				b[it->to] = b[root] + 1;
				if (a[it->to]) built(it->to);
			}	
	}
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	n = read(); m = read();
	for (int i = 1; i <= m; i++) {
		int x = read(), y = read(), z = read();
		addEdge(x, y, z);
		ww = z;
	}
	b[bg] = 1;
	built(bg);
	for (int i = 1; i <= n && i != bg; i++) ans += ww * b[i];
	//ans += ww * (n - 1);
	printf("%d", ans);
	//for (int i = 1; i <= n; i ++) cout<<b[i]<<' ';
	return 0;
}

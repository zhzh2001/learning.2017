#include <cstdio>
#include <iostream>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
typedef long long LL;
const int N=300010;
const LL inf=1e18;
int n,m,q,rt[N],tota,totA;
struct xint{int ch[2],fa,sz;LL s;}a[N*2];
struct Xint{int ch[2],fa,sz;LL x,y;}A[N*4];
void init()
{
	ref(i,1,n)a[i+1].s=1LL*m*i;
	a[1].s=-inf;a[n+2].s=inf;
	ref(i,1,n+1)a[i].ch[1]=i+1,a[i+1].fa=i;
	ref(i,1,n+2)a[i].sz=n+3-i;
	rt[0]=1;
	ref(i,1,n)rt[i]=i;
	ref(i,1,n)A[i].x=i*m-m+1,A[i].y=i*m-1,A[i].sz=m-1;
	tota=n+2;totA=n;
}
void a_pu(int x){
	a[x].sz=a[a[x].ch[0]].sz+a[a[x].ch[1]].sz+1;
}
void A_pu(int x){
	A[x].sz=A[A[x].ch[0]].sz+A[A[x].ch[1]].sz;
	A[x].sz+=A[x].y-A[x].x+1;
}
void a_rot(int x){
	int f=a[x].fa,g=a[f].fa;
	int d=a[f].ch[1]==x,D=a[g].ch[1]==f;
	a[a[f].fa=x].fa=g;
	a[a[x].ch[d^1]].fa=f;
	a[g].ch[D]=x;
	a[f].ch[d]=a[x].ch[d^1];
	a_pu(a[x].ch[d^1]=f);
}
void A_rot(int x){
	int f=A[x].fa,g=A[f].fa;
	int d=A[f].ch[1]==x,D=A[g].ch[1]==f;
	A[A[f].fa=x].fa=g;
	A[A[x].ch[d^1]].fa=f;
	A[g].ch[D]=x;
	A[f].ch[d]=A[x].ch[d^1];
	A_pu(A[x].ch[d^1]=f);
}
void a_splay(int x,int Fa=0){
	while(a[x].fa!=Fa){
		int f=a[x].fa,g=a[f].fa;
		int d=a[f].ch[1]==x,D=a[g].ch[1]==f;
		if(g!=Fa){if(d==D)a_rot(f);else a_rot(x);}
		a_rot(x);
	}
	a_pu(x);if(!Fa)rt[0]=x;
}
void A_splay(int&rt,int x){
	while(A[x].fa!=0){
		int f=A[x].fa,g=A[f].fa;
		int d=A[f].ch[1]==x,D=A[g].ch[1]==f;
		if(g!=0){if(d==D)A_rot(f);else A_rot(x);}
		A_rot(x);
	}
	A_pu(x);rt=x;
}
int a_find(int x){
	x++;
	for(int i=rt[0];;)
	{
		int ss=a[a[i].ch[0]].sz+1;
		if(ss==x)return i;
		if(ss>x)i=a[i].ch[0];else {i=a[i].ch[1];x-=ss;}
	}
}
int A_find(int r,int x){
	for(int i=rt[r];;)
	{
		int ss=A[i].ch[0]?A[A[i].ch[0]].sz:0;
		if(x>ss&&x<=ss+(A[i].y-A[i].x+1))return i;
		if(x<=ss)i=A[i].ch[0];else {x=x-ss-(A[i].y-A[i].x+1);i=A[i].ch[1];}
	}
}
LL a_erase(int x){
	int o=a_find(x);LL res=a[o].s;
	a_splay(o);int O=a[o].ch[1];
	for(;a[O].ch[0];O=a[O].ch[0]);
	a_splay(O,o);
	rt[0]=O;
	a[O].fa=0;a[a[o].ch[0]].fa=O;
	a[O].ch[0]=a[o].ch[0];
	a_pu(O);return res;
}
LL A_erase(int r,int x){
	int o=A_find(r,x);
	int ss=A[o].ch[0]?A[A[o].ch[0]].sz:0;
	LL xx=x-ss+A[o].x-1,yy=A[o].y;
	A[o].y=xx-1;A_pu(o);
	if(!A[o].ch[1])A[o].ch[1]=++totA;else{
		for(o=A[o].ch[1];A[o].ch[0];o=A[o].ch[0]);
		A[o].ch[0]=++totA;
	}
	A[totA].x=xx+1;A[totA].y=yy;
	A[totA].fa=o;A[totA].sz=yy-xx;A_splay(rt[r],totA);return xx;
}
void a_inserted(LL s){
	int t=n+2;
	if(!a[t].ch[0])a[t].ch[0]=++tota;
	else {
		for(t=a[t].ch[0];a[t].ch[1];t=a[t].ch[1]);
		a[t].ch[1]=++tota;
	}
	a[tota].fa=t;a[tota].sz=1;a[tota].s=s;
	a_splay(tota);
}
void A_inserted(int r,LL s){
	int t=rt[r];
	for(;A[t].ch[1];t=A[t].ch[1]);
	A[t].ch[1]=++totA;
	A[totA].fa=t;A[totA].sz=1;A[totA].x=A[totA].y=s;
	A_splay(rt[r],totA);
} 
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	init();
	ref(i,1,q)
	{
		int x=read(),y=read();
		//cout<<x<<" "<<y<<endl;
		if(y==m)
		{
			LL s=a_erase(x);
			a_inserted(s);
			printf("%lld\n",s);
			continue;
		}
		LL s1=a_erase(x);
		LL s2=A_erase(x,y);
		A_inserted(x,s1);
		a_inserted(s2);
		printf("%lld\n",s2);
	}
}

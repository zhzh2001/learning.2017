#include<cmath>
#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
typedef long long LL;
int T,n,h,r,fa[1010];
struct xint{int x,y,z;}p[1010];
LL dis(xint a,xint b){
	return 1LL*(a.x-b.x)*(a.x-b.x)+1LL*(a.y-b.y)*(a.y-b.y)+1LL*(a.z-b.z)*(a.z-b.z);
}
int ask(int x){
	return fa[x]==x?x:fa[x]=ask(fa[x]);
}
void unite(int a,int b){
	int p=ask(a),q=ask(b);
	if(p==q)return;fa[p]=q;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--){
		n=read(),h=read(),r=read();
		ref(i,1,n)p[i].x=read(),p[i].y=read(),p[i].z=read();
		ref(i,1,n+2)fa[i]=i;
		ref(i,1,n)ref(j,i+1,n)if(dis(p[i],p[j])<=4LL*r*r)unite(i,j);
		ref(i,1,n)if(abs(p[i].z)<=r)unite(i,n+1);
		ref(i,1,n)if(abs(p[i].z-h)<=r)unite(i,n+2);
		if(ask(n+1)==ask(n+2))printf("Yes\n");else printf("No\n");
	}
}

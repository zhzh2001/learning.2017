#include<cmath>
#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
#define ref(i,x,y)for(int i=x;i<=y;++i)
int read(){
	char c=getchar();int d=0,f=1;
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';d=d*10+c-48,c=getchar());
	return d*f;
}
const int inf=2e9;
int n,m,ans,mi[13],chd[1<<12];
int S[1<<12][1<<12],a[13][13];
int f[531441],g[531441];
void initf(){
	ref(i,0,mi[n]-1)f[i]=inf;
}
void initg(){
	ref(i,0,mi[n]-1)g[i]=inf;
}
void initS(){
	ref(i,0,(1<<n)-1){
		ref(j,0,(1<<n)-1)f[j]=inf;
		int t=0;
		ref(j,1,n)if(!(i>>j-1&1))t|=(1<<j-1);
		f[0]=0;
		ref(j,1,n)if(i>>j-1&1){
			for(int I=t;;I=(I-1)&t){	
				g[I]=inf;
				if(I==0)break;
			}
			ref(k,1,n)if(!(i>>k-1&1)&&a[j][k]<inf){
				for(int I=t;;I=(I-1)&t){
					if(f[I]+a[j][k]<g[I|(1<<k-1)])
						g[I|(1<<k-1)]=f[I]+a[j][k];
					if(I==0)break;
				}
			}
			for(int I=t;;I=(I-1)&t){
				f[I]=g[I];
				if(I==0)break;
			}
		}
		ref(j,1,(1<<n)-1)S[j][i]=f[j];
	}
	ref(i,0,(1<<n)-1){
		int t=0;
		ref(j,1,n)if(!(i>>j-1&1))t|=1<<j-1;
		for(int I=t;I>0;I=(I-1)&t){
			for(int J=I;J>0;J=(J-1)&I){
				if(S[J][i]<S[I][i])S[I][i]=S[J][i];
			}
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	if(n==1){printf("0\n");return 0;}
	ref(i,1,n)ref(j,1,n)a[i][j]=inf;
	ref(i,1,m){
		int x=read(),y=read(),s=read();
		if(x!=y)a[y][x]=a[x][y]=min(a[x][y],s);
	}
	mi[0]=1;
	ref(i,1,12)mi[i]=mi[i-1]*3;
	initS();
	initf();
	ref(i,1,n)f[2*mi[i-1]]=0;
	ref(i,1,(1<<n)-1)chd[i]=chd[i>>1]*3+(i&1)*2;
	ans=inf;
	ref(i,1,n-1)
	{
		initg();
		ref(j,0,mi[n]-1)if(f[j]<inf){
			int sum=f[j];
			int t=0,s=0,p=j;
			ref(k,1,n)if(j/mi[k-1]%3==0)t|=(1<<k-1);
			ref(k,1,n)if(j/mi[k-1]%3==2)s|=(1<<k-1),p-=mi[k-1];
			for(int I=t;I>0;I=(I-1)&t)if(S[s][I]<inf){
				int Sum=sum+S[s][I]*i,pp=p+chd[I];
				if(Sum<g[pp])g[pp]=Sum;
			}
		}
		ref(j,0,mi[n]-1)f[j]=g[j];
		ref(j,0,mi[n]-1)if(f[j]<inf){
			bool flag=1;
			ref(k,1,n)if(j/mi[k-1]%3==0)flag=0;
			if(flag)ans=min(ans,f[j]);
		}
	}
	cout<<ans<<endl;
}

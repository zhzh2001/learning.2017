#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define LL long long
#define N 15 

int n,m,a[N][N],x,y,z,ans=-1,vis[100],sum;

/*int dfs(int dian,int zt,int val,int bushu)
{
	if (f[dian][zt][bushu]!=0 && f[dian][zt][bushu]<=val) return f[dian][zt][bushu];
	f[dian][zt][bushu]=val;
	if (zt==(1<<n)-1)
	{
		ans=(ans==-1)?(val):(min(ans,val));
		return val;
	}
	//int ydian=dian,yzt=zt,yval=val,ybushu=bushu;
	dfs1(dian);
	for (int i=1;i<=n;++i)
	{
		if (i==dian || (zt&(1<<(i-1)))!=0 || a[dian][i]==0) continue;
		dfs(i,(zt|(1<<(i-1))),val+bushu*a[dian][i],bushu+1);
	}
}*/

int dfs1(int i,int bushu)
{
	vis[i]=1;
	for (int j=1;j<=n;++j)
	{
		if (a[i][j]!=0 && vis[j]==0) sum+=bushu*a[i][j],dfs1(j,bushu+1);
	}
}

int main()
{
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i)
	{
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=((a[x][y]==0)?(z):(min(z,a[x][y])));
		a[y][x]=a[x][y];
	}
	/*for (int i=1;i<=n;++i)
	{
		dfs(i,1<<(i-1),0,1);
	}*/
	for (int i=1;i<=n;++i)
	{
		sum=0;
		memset(vis,0,sizeof(vis));
		dfs1(i,1);
		ans=(ans==-1)?sum:min(ans,sum);
	}
	printf("%d\n",ans);
	return 0;
}

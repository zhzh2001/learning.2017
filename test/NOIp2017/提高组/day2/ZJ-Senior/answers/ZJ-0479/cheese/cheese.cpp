#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
#define LL long long 
#define N 1010 

int t,n;
LL x[N],y[N],z[N],h,r;
bool ha[N][N],vis[N],f;

double juli(int i,int j)
{
	return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
}

bool xbm(int i)
{
	if (z[i]>=0)
	{
		if (z[i]<=r) return 1;
		else return 0;
	}
	else
	{
		if (z[i]+r>=0) return 1;
		else return 0;
	}
}

bool sbm(int i)
{
	if (z[i]>=h)
	{
		if ((z[i]-r)<=h) return 1;
		else return 0;
	}
	else 
	{
		if (z[i]+r>=h) return 1;
		else return 0;
	}
}

bool dfs(int i)
{
	int ret=0;
	vis[i]=1;
	if (sbm(i)) return 1;
	for (int j=1;j<=n;++j)
	{
		if (vis[j] || ha[i][j]==0 || j==i) continue;
		ret=ret||dfs(j);
		if (ret) return 1;
	}
	return 0;
}

int main()
{
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		f=0;
		memset(ha,0,sizeof(ha));
		memset(vis,0,sizeof(vis));
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;++i)
		{
			scanf("%lld%lld%lld",x+i,y+i,z+i);
		}
		for (int i=1;i<n;++i)
		{
			for (int j=i+1;j<=n;++j)
			{
				if (juli(i,j)<=r*2.0) ha[i][j]=ha[j][i]=1;
			}
		}
		for (int i=1;i<=n;++i)
		{
			if (xbm(i) && vis[i]==0) 
			{
				if (dfs(i))
				{
					printf("Yes\n");
					f=1;
					break;
				}
			}
		}
		if (!f) printf("No\n");
	}
	return 0;
}

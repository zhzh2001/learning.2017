#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<bitset>
#include<vector>
#include<map>
#include<set>
#include<queue>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
int T,fa[1111],n,h,r;
LL x[1111],y[1111],z[1111];
int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d",&n,&h,&r);
		FOR(i,1,n) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		FOR(i,0,n+1) fa[i]=i;
		FOR(i,1,n)
			FOR(j,i+1,n){
				LL t=-4ll*r*r;
				t+=(x[i]-x[j])*(x[i]-x[j]);
				if (t>0) continue;
				t+=(y[i]-y[j])*(y[i]-y[j]);
				if (t>0) continue;
				t+=(z[i]-z[j])*(z[i]-z[j]);
				if (t>0) continue;
				int X=getf(i),Y=getf(j);
				if (X!=Y) fa[X]=Y;
			}
		FOR(i,1,n){
			if (z[i]<=r){
				int X=getf(i),Y=getf(0);
				if (X!=Y) fa[X]=Y;
			}
			if (z[i]>=h-r){
				int X=getf(i),Y=getf(n+1);
				if (X!=Y) fa[X]=Y;
			}
		}
		if (getf(0)==getf(n+1)) puts("Yes");
		else puts("No");
	}
	return 0;
}

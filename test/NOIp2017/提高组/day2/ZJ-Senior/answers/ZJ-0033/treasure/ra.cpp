#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<bitset>
#include<vector>
#include<map>
#include<set>
#include<queue>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
const int INF=1e9+10;
int n,m,x,y,z;
int main(){
	freopen("treasure.in","w",stdout);
	srand(time(0));
	n=12;
	m=1000;
	printf("%d %d\n",n,m);
	FOR(i,1,m){
		x=rand()%n+1;
		y=rand()%n+1;
		while (x==y) y=rand()%n+1;
		z=rand()%500000+1;
		printf("%d %d %d\n",x,y,z);
	}
	return 0;
}

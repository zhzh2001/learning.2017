#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<bitset>
#include<vector>
#include<map>
#include<set>
#include<queue>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
const int INF=1e9+10;
int n,m,d[111][111],x,ans,y,w,dp[13][5000],hh[11111],N,M,stk[11111],top,root,g[11111],tmp[11111],TMP[11111],f[11111];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	FOR(i,0,n-1)
		FOR(j,0,n-1)
			d[i][j]=INF;
	FOR(i,1,m){
		scanf("%d%d%d",&x,&y,&w);
		--x,--y;
		d[x][y]=d[y][x]=min(d[x][y],w);
	}
	FOR(i,1,10000)
		FOR(j,0,15)
			if ((i>>j)&1){hh[i]=j;break;}
	N=1<<n;
	ans=INF;
	FOR(i,0,n-1){
		root=i;
		FOR(j,0,n-1)
			FOR(k,0,N-1)
				dp[j][k]=INF;
		dp[0][1<<root]=0;
		FOR(j,0,n-2)
			FOR(k,0,N-1){
				if (dp[j][k]>=ans) continue;
				top=0;
				FOR(l,0,n-1)
					if (!((k>>l)&1)) stk[top++]=l;
				FOR(l,0,top-1){
					int x=stk[l];
					f[l]=INF;
					g[l]=1<<x;
					FOR(p,0,n-1)
						if ((k>>p)&1) f[l]=min(f[l],d[x][p]);
				}
				M=1<<top;
				FOR(q,1,M-1) tmp[q]=min(INF,tmp[q-(1<<hh[q])]+f[hh[q]]);
				FOR(q,1,M-1) TMP[q]=TMP[q-(1<<hh[q])]+g[hh[q]];
				FOR(l,1,M-1)
					if (tmp[l]<INF)	dp[j+1][k^TMP[l]]=min(dp[j+1][k^TMP[l]],dp[j][k]+tmp[l]*(j+1));
			}
		FOR(j,0,n-1) ans=min(ans,dp[j][N-1]);
	}
	printf("%d\n",ans);
	return 0;
}

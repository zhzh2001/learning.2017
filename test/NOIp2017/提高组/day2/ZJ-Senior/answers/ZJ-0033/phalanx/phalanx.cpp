#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
#include<bitset>
#include<vector>
#include<map>
#include<set>
#include<queue>
#define LL long long
#define pb push_back
#define mp make_pair
#define FOR(i,a,b) for (int i=a;i<=b;++i)
#define FORD(i,a,b) for (int i=a;i>=b;--i)
using namespace std;
typedef pair<int,int> pa;
int x[500010],y[500010],X,Y,n,m,q;
void getint(int &x){ //x>=0
	char c=getchar();
	for (;c<'0' || c>'9';c=getchar());
	x=0;
	for (;c>='0' && c<='9';c=getchar()) x=x*10+c-48;
}
bool al1(){
	FOR(i,1,q)
		if (x[i]!=1) return 0;
	return 1;
}
LL ans[500010],a[1000010];
int T[5000010],N,s;
vector<int> v[300010];
namespace bf{
	void build(int l,int r,int rt){
		if (l==r){
			T[rt]=(a[l]>0);
			return;
		}
		int m=l+r>>1;
		build(l,m,rt<<1);
		build(m+1,r,rt<<1|1);
		T[rt]=T[rt<<1]+T[rt<<1|1];
	}
	int fd(int l,int r,int rt,int k){
		if (l==r) return l;
		int m=l+r>>1;
		if (T[rt<<1]>=k) return fd(l,m,rt<<1,k);
		else return fd(m+1,r,rt<<1|1,k-T[rt<<1]);
	}
	void upd(int l,int r,int rt,int x,LL y){
		if (l==r){
			a[x]=y;
			T[rt]=(a[x]>0);
			return;
		}
		int m=l+r>>1;
		if (x<=m) upd(l,m,rt<<1,x,y);
		else upd(m+1,r,rt<<1|1,x,y);
		T[rt]=T[rt<<1]+T[rt<<1|1];
	}
	void work(){
		FOR(i,1,m) a[++s]=i;
		FOR(i,2,n) a[++s]=(LL)i*m;
		N=n+m+q;
		build(1,N,1);
		FOR(i,1,q){
			int x=fd(1,N,1,y[i]);
			printf("%lld\n",a[x]);
			upd(1,N,1,++s,a[x]);
			upd(1,N,1,x,0ll);
		}
	}
};
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	FOR(i,1,q)
		getint(x[i]),getint(y[i]),v[x[i]].pb(i);
	FOR(i,1,n) v[i].pb(q+1);
	if (al1()){bf::work();return 0;}
	if (q>500){
	for (int i=1;i<=q;++i){
		X=x[i],Y=y[i];
		for (int j=i-1;j>=1;--j){
			if (X==n && Y==m){ans[i]=ans[j];break;}
			else if (Y==m && X>=x[j]) ++X;
			else if (X==x[j] && Y>=y[j]) ++Y;
			
				if (Y<m){
					if (v[X].empty()) break;
					if (v[X][0]>=j) break;
					j=(*(--lower_bound(v[X].begin(),v[X].end(),j)))+1;
				}
			
		}
		if (!ans[i]) ans[i]=(LL)m*(X-1)+Y;
		printf("%lld\n",ans[i]);
	}
	}
	else{
	FOR(i,1,q){
		X=x[i],Y=y[i];
		FORD(j,i-1,1){
			if (X==n && Y==m) X=x[j],Y=y[j];
			else if (Y==m && X>=x[j]) ++X;
			else if (X==x[j] && Y>=y[j]) ++Y;
		}
		printf("%lld\n",(LL)m*(X-1)+Y);
	}
	}
	return 0;
}

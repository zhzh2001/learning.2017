#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;
typedef long long LL;
typedef pair<int, int> P;
const int INF = 0x7f7f7f7f, N = 28;

bool vis[N];
int G[N][N], step[N], n;

LL prim(int s)
{
	LL ans = 0;
	memset(vis, 0, sizeof(vis));
	memset(step, 0, sizeof(step));
	priority_queue<P, vector<P>, greater<P> > Q;
	Q.push(P(0, s)); step[s] = 1;
	while (!Q.empty())
	{
		P tmp = Q.top(); Q.pop();
		if (vis[tmp.second]) continue;
		vis[tmp.second] = true;
		ans += (LL)tmp.first;
		for (int i = 1; i <= n; i++) if (G[tmp.second][i] < INF)
		{
			Q.push(P(G[tmp.second][i] * step[tmp.second], i));
			step[i] = step[tmp.second] + 1;
		}
	}
	return ans;
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int m, u, v, wei;
	LL ans = (LL)100 * INF;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			G[i][j] = INF;
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d", &u, &v, &wei);
		G[u][v] = G[v][u] = min(G[u][v], wei);
	}
	for (int i = 1; i <= n; i++)
		ans = min(ans, prim(i));
	printf("%lld\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

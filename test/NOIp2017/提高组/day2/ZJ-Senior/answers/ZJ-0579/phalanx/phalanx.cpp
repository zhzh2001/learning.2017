#include <cstdio>
using namespace std;

int id[8000][8000];

int main()
{
	int n, m, q, x, y;
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			id[i][j] = (i - 1) * m + j;
	while(q--)
	{
		scanf("%d%d", &x, &y);
		int tmp = id[x][y];
		for (int i = y; i < m; i++) id[x][i] = id[x][i + 1];
		for (int i = x; i < n; i++) id[i][m] = id[i + 1][m];
		id[n][m] = tmp;
		printf("%d\n", tmp);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

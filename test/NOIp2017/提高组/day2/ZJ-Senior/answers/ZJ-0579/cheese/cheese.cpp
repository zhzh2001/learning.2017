#include <cstdio>
#include <cmath>
#include <cstring>
#define sqr(x) ((x) * (x))
using namespace std;
const int N = 1010;

int fst[N], nxt[2 * N * N], to[2 * N * N], E, n;
bool vis[N];
long long x[N], y[N], z[N];

inline void add(int u, int v)
{
	to[++E] = v, nxt[E] = fst[u], fst[u] = E;
}

bool dfs(int now)
{
	vis[now] = true;
	if (now == n + 1) return true;
	for (int i = fst[now]; i != -1; i = nxt[i]) if (!vis[to[i]])
		if (dfs(to[i])) return true;
	return false;
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T;
	long long r, h;
	scanf("%d", &T);
	while (T--)
	{
		E = 0;
		memset(fst, -1, sizeof(fst));
		memset(vis, 0, sizeof(vis));
		scanf("%d%lld%lld", &n, &h, &r);
		for (int i = 1; i <= n; i++)
			scanf("%lld%lld%lld", x + i, y + i, z + i);
		for (int i = 1; i <= n; i++)
		{
			if (z[i] <= r) add(0, i), add(i, 0);
			if (z[i] >= h - r) add(n + 1, i), add(i, n + 1);
			for (int j = 1; j < i; j++)
			{
				if (sqrt((double)sqr(x[i] - x[j]) + sqr(y[i] - y[j]) + sqr(z[i] - z[j])) - 2 * r < 1e-8) add(i, j), add(j, i);
			}
		}
		if (dfs(0)) puts("Yes");
		else puts("No"); 
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

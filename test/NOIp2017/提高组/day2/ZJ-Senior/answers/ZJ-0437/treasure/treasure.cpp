#include<cstdio>
const int N=15;
int n,m,a[N][N],f[N][N][1<<12],cnt[1<<12];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)a[i][j]=1e9;
	for(int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		x--;y--;
		if(z<a[x][y])a[x][y]=z;
		if(z<a[y][x])a[y][x]=z;
	}
	cnt[0]=0;
	for(int S=1;S<(1<<n);S++)cnt[S]=cnt[S^(S&-S)]+1;
	for(int i=0;i<n;i++)
		for(int j=0;j<=n;j++)
			for(int S=0;S<(1<<n);S++)f[i][j][S]=1e9;
	for(int S=1;S<(1<<n);S++)
		for(int i=0;i<n;i++)if(S&(1<<i))//i is the root
		{
			if(S==(S&-S)){for(int j=0;j<n;j++)f[i][j][S]=0;continue;}
			int T=S^(1<<i);
			for(int S0=T;S0;S0=(S0-1)&T)
				for(int k=0;k<n;k++)if(S0&(1<<k)&&a[k][i]<1e9)//k is the subtree's root
				{
					int p=S0,q=T-S0;
					//trans f[k][x][p] and f[i][x-1][q^(1<<i)] to f[i][x-1][S]
					for(int x=1;x-1<=n-cnt[S];x++)if(f[k][x][p]<1e9)
					{
						int y=x-1,tmp=f[i][y][q^(1<<i)]+f[k][x][p]+a[k][i]*x;
						if(tmp<f[i][y][S])f[i][y][S]=tmp;
					}
				}
		}
	int ans=1e9;
	for(int i=0;i<n;i++)if(f[i][0][(1<<n)-1]<ans)ans=f[i][0][(1<<n)-1];
	printf("%d\n",ans);
	return 0;
}

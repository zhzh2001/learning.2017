#include<cstdio>
#include<cstdlib>
#include<vector>
#include<algorithm>
using namespace std;
const int N=3e5+5,M=2000;
typedef long long LL;
struct T{int l,r,key,sz;LL val;}t[N*35];
int cnt=0,rt=0;
inline int newnode(LL x)
{
	cnt++;
	t[cnt].key=rand();
	t[cnt].l=t[cnt].r=0;
	t[cnt].val=x;
	t[cnt].sz=1;
	return cnt;
}
inline void pushup(int x){t[x].sz=t[t[x].l].sz+t[t[x].r].sz+1;}
int merge(int x,int y)
{
	if(!x)return y;if(!y)return x;
	if(t[x].key>t[y].key){t[x].r=merge(t[x].r,y);pushup(x);return x;}
	else {t[y].l=merge(x,t[y].l);pushup(y);return y;}
}
pair<int,int> split(int x,int k)
{
	if(!k)return make_pair(0,x);
	pair<int,int> y;
	if(k<=t[t[x].l].sz){y=split(t[x].l,k);t[x].l=y.second;y.second=x;pushup(x);}
	else {y=split(t[x].r,k-t[t[x].l].sz-1);t[x].r=y.first;y.first=x;pushup(x);}
	return y;
}
LL find(int x,int k)
{
	if(k<=t[t[x].l].sz)return find(t[x].l,k);
	if(k==t[t[x].l].sz+1)return t[x].val;
	return find(t[x].r,k-t[t[x].l].sz-1);
}
vector<pair<int,LL> > vec[N];
int n,m,q,xx[N],yy[N],num[N],aa[N];
inline void move(int x,int y)
{
	if(num[x]<=M){vec[x].push_back(make_pair(y,find(rt,x)));return;}
	if(y==m)return;
	pair<int,int> a=split(aa[x],y-1),b=split(a.second,1);
	aa[x]=merge(a.first,b.second);
	aa[x]=merge(aa[x],newnode(find(rt,x)));
}
inline LL query(int x,int y)
{
	if(y==m)return find(rt,x);
	if(num[x]>M)return find(aa[x],y);
	int now=y;
	for(int i=vec[x].size()-1;i>=0;i--)
	{
		if(vec[x][i].first<=now)now++;
		if(now==m)return vec[x][i].second;
	}
	return (LL)(x-1)*m+now;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++)rt=merge(rt,newnode((LL)i*m));
	for(int i=1;i<=q;i++)scanf("%d%d",&xx[i],&yy[i]),num[xx[i]]++;
	for(int i=1;i<=n;i++)if(num[i]>M)
		for(int j=1;j<m;j++)aa[i]=merge(aa[i],newnode((LL)(i-1)*m+j));
	for(int i=1;i<=q;i++)
	{
		int x=xx[i],y=yy[i];
		LL t;
		printf("%lld\n",t=query(x,y));
		move(x,y);
		pair<int,int> a=split(rt,x-1),b=split(a.second,1);
		rt=merge(a.first,b.second);
		rt=merge(rt,newnode(t));
	}
	return 0;
}

#include<cstdio>
#include<cstring>
const int N=1e3+5;
typedef long long LL;
int T,n,vis[N],Q[N];
LL h,r;
struct Point{LL x,y,z;}p[N];
inline bool check(Point a,Point b)
{
	return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z)<=r*r*4;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for(int i=1;i<=n;i++)scanf("%lld%lld%lld",&p[i].x,&p[i].y,&p[i].z);
		memset(vis,0,sizeof(vis));
		int hd=0,tl=0;
		for(int i=1;i<=n;i++)if(p[i].z<=r)Q[++tl]=i,vis[i]=1;
		while(hd<tl)
		{
			int x=Q[++hd];
			for(int i=1;i<=n;i++)if(i!=x&&vis[i]==0&&check(p[x],p[i]))Q[++tl]=i,vis[i]=1;
		}
		int ans=0;
		for(int i=1;i<=n;i++)if(vis[i]&&h-p[i].z<=r)ans=1;
		puts(ans?"Yes":"No");
	}
	return 0;
}

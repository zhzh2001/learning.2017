#include<bits/stdc++.h>
using namespace std;
int n,m,q,x,y,z,a[1005][1005],b[1000000],f[1000000];
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2){
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	int x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline void writ(int ans)
{
	if (ans==0) return;
	writ(ans/10);
	putchar(ans%10+48);
}
inline int sum(int x)
{
	int ans=0;
	while (x>0) ans=ans+f[x],x-=x&(-x);
	return ans;
}
inline void add(int x,int y)
{
	while (x<=m) f[x]+=y,x+=x&(-x);
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=rea(),m=rea(),q=rea();
	if (n<=1000&&m<=1000){
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				a[i][j]=(i-1)*m+j;
		for (int p=0;p<q;p++){
			x=rea(),y=rea(),z=a[x][y];
			for (int i=y;i<m;i++) a[x][i]=a[x][i+1];
			for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
			a[n][m]=z,writ(z),putchar('\n');
		}
	}else if (n==1){
		for (int i=1;i<=m;i++) b[i]=i;
		for (int p=0;p<q;p++){
			x=rea(),y=rea(),z=0;
			while (sum(y)-sum(z)>0) x=sum(y)-sum(z),z=y,y=y+x;
			writ(b[y]),putchar('\n');
			b[++m]=b[y],b[y]=0,add(y,1);
		}
	}
	return 0;
}

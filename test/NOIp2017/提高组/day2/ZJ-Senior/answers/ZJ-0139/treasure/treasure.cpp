#include<bits/stdc++.h>
using namespace std;
#define INF 10000
int n,m,ans,x,y,z,a[15][15];
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2){
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	int x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline void writ(int ans)
{
	if (ans==0) return;
	writ(ans/10);
	putchar(ans%10+48);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=rea(),m=rea(),ans=INF;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			a[i][j]=INF;
	for (int i=1;i<=m;i++)
		x=rea(),y=rea(),z=rea(),a[x][y]=a[y][x]=1;
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (a[i][j]>a[i][k]+a[k][j])
					a[i][j]=a[i][k]+a[k][j];
	for (int i=1;i<=n;i++){
		int sum=0;
		for (int j=1;j<=n;j++)
			if (i!=j) sum=sum+a[i][j]*z;
		if (sum<ans) ans=sum;
	}
	if (ans==0)putchar('0');
	else writ(ans);
	putchar('\n');
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define N 1005
struct arr{
	long long fi,se,th;
}a[N];
int nedge,n,q,t,w,head[N],too[2*N*N],nxt[2*N*N],b[N];bool flag[N];long long h,r;
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2){
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline int rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	int x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline long long Rea()
{
	char ch=nc();
	while (ch<'0' || ch>'9') ch=nc();
	long long x=ch-'0';ch=nc();
	while (ch>='0' && ch<='9') x=x*10+ch-'0',ch=nc();
	return x;
}
inline void addedge(int x,int y)
{
	nxt[++nedge]=head[x];
	head[x]=nedge;
	too[nedge]=y;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	q=rea();
	for (int p=0;p<q;p++){
		n=rea(),h=Rea(),r=Rea(),nedge=0;
		memset(head,0,sizeof(head));
		for (int i=1;i<=n;i++){
			a[i].fi=Rea(),a[i].se=Rea(),a[i].th=Rea();
			if (abs(a[i].th)<=r) addedge(n+1,i),addedge(i,n+1);
			if (abs(h-a[i].th)<=r) addedge(n+2,i),addedge(i,n+2);
		}
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (i!=j&&sqrt((a[i].fi-a[j].fi)*(a[i].fi-a[j].fi)+(a[i].se-a[j].se)*(a[i].se-a[j].se)+(a[i].th-a[j].th)*(a[i].th-a[j].th))<=2*r)
					addedge(i,j);
		memset(flag,false,sizeof(flag));
		t=1,w=1,flag[n+1]=true,b[1]=n+1;
		while (t<=w){
			int k=head[b[t]];
			while (k>0){
				int v=too[k];
				if (flag[v]==false)
					flag[v]=true,b[++w]=v;
				k=nxt[k];
			}
			t++;
		}
		if (flag[n+2]==true) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

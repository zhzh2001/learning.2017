#include<iostream>
#include<cstdio>
using namespace std;
struct st{
	int v,w[13];
}dp[5050];
int n,m,i,j,map[13][13],x,y,v,k,l,ans,q;
main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1; i<=n; i++)
	 for (j=1; j<=n; j++) 
	  map[i][j]=1001000;
	ans=1001000;
	for (i=1; i<=m; i++)
	{
		scanf("%d%d%d",&x,&y,&v);
		if (map[x][y]>v){map[x][y]=map[y][x]=v;}
	}
	for (i=1; i<=n; i++)
	{
		for (j=1; j<=(1<<n)-1; j++) dp[j].v=100001000;
		dp[1<<i-1].v=0;
		for (j=1; j<=n; j++) dp[1<<i-1].w[j]=0;
		dp[1<<i-1].w[i]=1;
		for (j=1; j<=(1<<n)-1; j++)
		{
			if ((j&(1<<i-1))==0) continue;
			k=1;
			while (1<<k-1<=j)
			{
				while ((1<<k-1&j)==0) k++;
				for (l=1; l<=n; l++)
				{
					if (l==i||l==k||((1<<l-1)&j)==0) continue;
					if ((1<<l-1)>j) break;
					if (dp[j].v>dp[j-(1<<l-1)].v+dp[j-(1<<l-1)].w[k]*map[k][l])
					{
						for (q=1;q<=n; q++) dp[j].w[q]=dp[j-(1<<l-1)].w[q]; 
						dp[j].w[l]=dp[j].w[k]+1;
						dp[j].v=dp[j-(1<<l-1)].v+dp[j-(1<<l-1)].w[k]*map[k][l];
					}
				}
				k++;
			}
		}
		if (ans>dp[(1<<n)-1].v) ans=dp[(1<<n)-1].v;
	}
	printf("%d",ans);
}

#include<cstdio>
#include<cmath>
using namespace std;
struct st{
	int x,y,z;
}a[1010];
double pks=10e-9;
int T,i,n,h,r,head,tail,ha[1010],fl,k,bf[5050];
int jud(int l,int y)
{
	double di=sqrt((a[l].x-a[y].x)*(a[l].x-a[y].x)+(a[l].y-a[y].y)*(a[l].y-a[y].y)+(a[l].z-a[y].z)*(a[l].z-a[y].z));
	if (di<=2*r||abs(di-2*r)<=pks||di-2*r==0) return 1; else return 0;
}
main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d",&n,&h,&r);
		for (i=1; i<=n; i++) ha[i]=0;
		for (i=1; i<=n; i++)
		{
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		}
		head=1; tail=0;
		fl=0;
		for (i=1; i<=n; i++)
		{
			if (a[i].z-r<=0) {tail++; bf[tail]=i; ha[i]=1;}
		}
		while (head<=tail)
		{
			k=bf[head];
			if (a[k].z+r>=h) {fl=1; break;}
			for (i=1; i<=n; i++)
			{
				if (ha[i]==0&&jud(i,k)) {tail++; bf[tail]=i; ha[i]=1;} 
			}
			head++;
		}
		if (fl==0) printf("No\n"); else if (fl==1) printf("Yes\n");
	}
}

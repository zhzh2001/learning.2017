#include<cstdio>
#include<cstring>

int t,it,n,m,r,i,j,x[1003],y[1003],z[1003];
long long rr;
bool vis[1003];
int q[1003],lq,rq;

inline bool check(long long x1,long long x2,long long x3)
{
	long long i1=rr-x1*x1-x2*x2;
//	if(i1<0)return false;
	return i1>=(x3*x3);
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for(it=1;it<=t;++it)
	{
		scanf("%d%d%d",&n,&m,&r);
		rr=r*2;
		rr*=rr;
		for(i=1;i<=n;++i)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		}
		memset(vis,0,sizeof(vis));
		rq=0;
		for(i=1;i<=n;++i)
		{
			if(z[i]<=r)
			{
				vis[i]=true;
				q[++rq]=i;
			}
		}
		for(lq=1;lq<=rq;++lq)
		{
			if(z[q[lq]]+r>=m)
			{
				vis[0]=true;
				break;
			}
			for(i=1;i<=n;++i)
			{
				if((!vis[i])&&(check(x[i]-x[q[lq]],y[i]-y[q[lq]],z[i]-z[q[lq]])))
				{
					vis[i]=true;
					q[++rq]=i;
				}
			}
		}
		if(vis[0])puts("Yes");
		else puts("No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

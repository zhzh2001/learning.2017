#include<cstdio>
#include<cstring>

int n,m,q,i,j,x,y;
long long ans;
int ch;
inline int getint()
{
	for(ch=getchar();ch<'0'||ch>'9';ch=getchar());
	int i1=ch-'0';
	for(ch=getchar();ch>='0'&&ch<='9';ch=getchar())i1=((i1<<3)+(i1<<1)+ch-'0');
	return i1;
}

int randseed;
inline int randnum()
{
	return ((randseed=((randseed*613123+233333)&2147483647))>>15);
}
namespace treap1
{
	const int maxnod=900005;
	int lc[maxnod],rc[maxnod],size[maxnod],pri[maxnod],roots[300005],sumn;
	long long val[maxnod];
	inline void updata(int v0)
	{
		size[v0]=size[lc[v0]]+size[rc[v0]]+1;
		return;
	}
	inline int merge(int v1,int v2)
	{
		if(!(v1&&v2))return v1|v2;
		if(pri[v1]<pri[v2])
		{
			rc[v1]=merge(rc[v1],v2);
			updata(v1);
			return v1;
		}else
		{
			lc[v2]=merge(v1,lc[v2]);
			updata(v2);
			return v2;
		}
	}
	inline void splits(int v0,int x0,int &v1,int &v2)
	{
		if(!v0)
		{
			v1=v2=0;
			return;
		}
		if(size[lc[v0]]>=x0)
		{
			splits(lc[v0],x0,v1,v2);
			lc[v0]=v2;
			v2=v0;
		}else
		{
			splits(rc[v0],x0-size[lc[v0]]-1,v1,v2);
			rc[v0]=v1;
			v1=v0;
		}
		updata(v0);
		return;
	}
	inline int insert(int v0,int v1,int x0)
	{
		int i1,i2;
		splits(v0,x0,i1,i2);
		return merge(i1,merge(v1,i2));
	}
	inline int remove(int v0,int &v1,int x0)
	{
		int i1,i2,i3,i4;
		splits(v0,x0-1,i1,i2);
		splits(i2,1,i3,i4);//kc
		v1=i3;
		return merge(i1,i4);
	}
//	inline void dfs(int v0,int x0)
//	{
//		printf("%d %d\n",v0,x0);
//		if(lc[v0])dfs(lc[v0],x0+1);
//		if(rc[v0])dfs(rc[v0],x0+1);
//	}
	inline void init()
	{
		int i1;
		memset(lc,0,sizeof(lc));
		memset(rc,0,sizeof(rc));
		memset(size,0,sizeof(size));
		roots[0]=0;
		for(i1=1;i1<=::n;++i1)
		{
			pri[i1]=::randnum();
			val[i1]=i1*(long long)(::m);
			size[i1]=1;
			roots[0]=merge(roots[0],i1);
			roots[i1]=0;
		}
//		dfs(roots[0],0);
		sumn=::n;
		return;
	}
	inline int newnode(long long x0)
	{
		++sumn;
		pri[sumn]=::randnum();
		size[sumn]=1;
		val[sumn]=x0;
		return sumn;
	}
}
namespace treap2
{
	const int maxnodes=600005;
	int lc[maxnodes],rc[maxnodes],pri[maxnodes],size[maxnodes],roots[maxnodes],sumn;
	int vall[maxnodes],valr[maxnodes];
	inline void updata(int v0)
	{
		size[v0]=size[lc[v0]]+size[rc[v0]]+valr[v0]-vall[v0];
		return;
	}
	inline int merge(int v1,int v2)
	{
		if(!(v1&&v2))return v1|v2;
		if(pri[v1]<pri[v2])
		{
			rc[v1]=merge(rc[v1],v2);
			updata(v1);
			return v1;
		}else
		{
			lc[v2]=merge(v1,lc[v2]);
			updata(v2);
			return v2;
		}
	}
	inline void splits(int v0,int x0,int &v1,int &v2)
	{
		if(!v0)
		{
			v1=v2=0;
			return;
		}
		if(x0<=0)
		{
			v1=0;v2=v0;
			return;
		}
		if(size[lc[v0]]>=x0)
		{
			splits(lc[v0],x0,v1,v2);
			lc[v0]=v2;
			v2=v0;
		}else
		{
			splits(rc[v0],x0-(size[v0]-size[rc[v0]]),v1,v2);
			rc[v0]=v1;
			v1=v0;
		}
		updata(v0);
		return;
	}
	inline int dele(int v0,int x0,int &res0)
	{
		int i1,i2,i3,i4,i5;
		splits(v0,x0,i1,i2);
		for(i5=i1;rc[i5];i5=rc[i5]);
		splits(i1,size[i1]-valr[i5]+vall[i5],i3,i4);//kc
//		if(i5!=i4)puts("!!!");
		res0=vall[i4]+x0-size[i3];
		if(x0-size[i3]<size[i4])
		{
			i5=(++sumn);
			pri[i5]=::randnum();
			vall[i5]=res0;
			valr[i5]=valr[i4];
			size[i5]=valr[i5]-vall[i5];
		}else i5=0;
		valr[i4]=res0-1;
		size[i4]=valr[i4]-vall[i4];
		if(size[i4])i5=merge(i4,i5);
		return merge(i3,merge(i5,i2));
	}
	inline void init()
	{
		memset(lc,0,sizeof(lc));
		memset(rc,0,sizeof(rc));
		memset(size,0,sizeof(size));
		int i1;
		for(i1=1;i1<=::n;++i1)
		{
			roots[i1]=i1;
			pri[i1]=::randnum();
			vall[i1]=0;
			valr[i1]=m-1;
			size[i1]=m-1;
		}
		sumn=::n;
		return;
	}
}

inline void work1(int x1,int x2)
{
	int i1;
	treap2::roots[x1]=treap2::dele(treap2::roots[x1],x2,i1);
	ans=m*(long long)(x1-1)+i1;
	treap1::roots[0]=treap1::remove(treap1::roots[0],i1,x1);
	treap1::roots[x1]=treap1::merge(treap1::roots[x1],i1);
	i1=treap1::newnode(ans);
	treap1::roots[0]=treap1::merge(treap1::roots[0],i1);
	return;
}
inline void work2(int x1,int x2,int x3)
{
	int i1,i2;
	if(x2==m)
	{
		treap1::roots[0]=treap1::remove(treap1::roots[0],i1,x1);
		ans=treap1::val[i1];
		treap1::roots[0]=treap1::merge(treap1::roots[0],i1);
	}else
	{
		treap1::roots[x1]=treap1::remove(treap1::roots[x1],i1,x2-x3);
		ans=treap1::val[i1];
		treap1::roots[0]=treap1::remove(treap1::roots[0],i2,x1);
		treap1::roots[0]=treap1::merge(treap1::roots[0],i1);
		treap1::roots[x1]=treap1::merge(treap1::roots[x1],i2);
	}
	return;
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=getint();m=getint();q=getint();
	randseed=613;
	treap1::init();
	treap2::init();
	for(i=1;i<=q;++i)
	{
		x=getint();y=getint();
		if(y<=treap2::size[treap2::roots[x]])work1(x,y);
		else work2(x,y,treap2::size[treap2::roots[x]]);
		printf("%lld\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

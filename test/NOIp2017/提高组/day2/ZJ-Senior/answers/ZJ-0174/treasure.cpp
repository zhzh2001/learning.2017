#include<cstdio>
#include<cstring>

int n,n2,m,i,j,u,v,w,t1,t2;
int map[13][13];
int minl[4102][13];
int dp[13][4102];
int ans;

inline int getdp(int x1,int x2)
{
	int i1=1,i2=0;
	while(x1>0)
	{
		if(x1&1)
		{
			if(minl[x2][i1]<0)return -233;
			i2+=minl[x2][i1];
		}
		x1>>=1;
		++i1;
	}
	return i2;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(map,-1,sizeof(map));
	for(i=1;i<=m;++i)
	{
		scanf("%d%d%d",&u,&v,&w);
		if((map[u][v]<0)||map[u][v]>w)map[v][u]=map[u][v]=w;
	}
	for(i=1;i<=n;++i)map[i][i]=0;
	n2=(1<<n);
	memset(minl,-1,sizeof(minl));
	for(i=1;i<n2;++i)
	{
		t1=0;
		for(j=(i&(-i));j>0;j>>=1)++t1;
		t2=(i&(i-1));
		for(j=1;j<=n;++j)
		{
			if((minl[t2][j]<0)||((map[t1][j]>=0)&&(minl[t2][j]>=map[t1][j])))minl[i][j]=map[t1][j];
			else minl[i][j]=minl[t2][j];
		}
	}
	memset(dp,-1,sizeof(dp));
	for(i=1;i<=n;++i)dp[0][(1<<(i-1))]=0;
	for(i=1;i<=n;++i)
	{
		for(j=1;j<n2;++j)
		{
			for(u=(j&(j-1));u>0;u=((u-1)&j))
			{
				if(dp[i-1][u]<0)continue;
				v=getdp(j^u,u);
				if(v<0)continue;
				v=v*i+dp[i-1][u];
				if((dp[i][j]<0)||(dp[i][j]>v))dp[i][j]=v;
			}
//			printf("%d %d:%d\n",i,j,dp[i][j]);
		}
//		printf("%d %d\n",i,dp[i][n2-1]);
	}
	ans=987654321;
	for(i=0;i<=n;++i)if((dp[i][n2-1]>0)&&(dp[i][n2-1]<ans))ans=dp[i][n2-1];
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

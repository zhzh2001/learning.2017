#include<bits/stdc++.h>
using namespace std;
const int INF=40000000;
struct line{
	int y,next,w;
}l[15];
int n,m,ans=40000000,maxx=0,x,y,v,sum;
int f[13][4100],now[12][4100][12]={};
int d[15][15],deep[15],fi[15],val[15];
int read(){
	int num=0,flag=1;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')flag=-1;
	for(;c>='0'&&c<='9';c=getchar())
		num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}

void dfs(int b,int s){
	if(f[b][s]<INF)return;
	for(int i=1;i<=n;++i)
		if((1<<(i-1)&s)&&i!=b){
			int t=s-(1<<(i-1));
			dfs(b,t);
			for(int j=1;j<=n;++j)
				if((1<<(j-1)&t)&&d[j][i]!=-1)
					if(f[b][t]+d[j][i]*now[b][t][j]<f[b][s]){
						f[b][s]=f[b][t]+d[j][i]*now[b][t][j];
						now[b][s][i]=now[b][t][j]+1;
						for(int k=1;k<=n;++k)
							if(k!=i)now[b][s][k]=now[b][t][k];	
					}
		}
	if(s==maxx)ans=min(ans,f[b][s]);
}
		

void work(int b){
	f[b][1<<(b-1)]=0;
	for(int i=0;i<=maxx;++i)now[b][i][b]=1;
	for(int i=1;i<=n;++i)
		if(i!=b&&d[b][i]!=-1){
			f[b][(1<<(i-1))+(1<<(b-1))]=d[b][i];
			now[b][(1<<(i-1))+(1<<(b-1))][i]=2;
		}
	dfs(b,maxx);
	return;
}

void make_line(int i,int j,int w){
	l[++sum].y=j;l[sum].next=fi[i];fi[i]=sum;l[sum].w=w;
	l[++sum].y=i;l[sum].next=fi[j];fi[j]=sum;l[sum].w=w;
}

void dfs_tree(int x,int fa){
	deep[x]=deep[fa]+1;
	for(int i=fi[x];i;i=l[i].next){
		int v=l[i].y;
		if(v!=fa){
			val[v]=l[i].w;
			dfs_tree(v,x);
		}
	}
}

void work_1(){
	for(int i=1;i<n;++i){
		x=read();y=read();v=read();
		make_line(x,y,v);
	}
	for(int i=1;i<=n;++i){
		val[i]=0;
		dfs_tree(i,0);
		int temp=0;
		for(int j=1;j<=n;++j)
			temp+=(deep[j]-1)*val[j];
		ans=min(ans,temp);
	}
	printf("%d\n",ans);
	exit(0);
}

void init(){
	memset(d,-1,sizeof(d));
	memset(f,10,sizeof(f));
	n=read();m=read();
	if(m==n-1)work_1();
	for(int i=1;i<=m;++i){
		x=read();y=read();v=read();
		if(d[x][y]!=-1)
			d[x][y]=d[y][x]=min(d[x][y],v);
		else d[x][y]=d[y][x]=v;
	}
	for(int i=0;i<n;++i)maxx+=(1<<i);
	for(int i=1;i<=n;++i)work(i);
	printf("%d\n",ans);
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	init();
	return 0;
}

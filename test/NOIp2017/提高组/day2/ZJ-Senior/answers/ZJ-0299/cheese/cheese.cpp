#include<bits/stdc++.h>
using namespace std;
const int MAXN=1010,MAXM=1000010;
struct line{
	int b,next;
}l[MAXM];
int t,n,sum=0,head,tail,q[MAXN],fi[MAXN],f[MAXN];
long long h,r,x[MAXN],y[MAXN],z[MAXN];
int read(){
	int num=0,flag=1;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')flag=-1;
	for(;c>='0'&&c<='9';c=getchar())
		num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}

long long read_(){
	long long num=0,flag=1;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')flag=-1;
	for(;c>='0'&&c<='9';c=getchar())
		num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}

void make_line(int i,int j){
	l[++sum].b=j;l[sum].next=fi[i];fi[i]=sum;
	l[++sum].b=i;l[sum].next=fi[j];fi[j]=sum;
}

bool check(int i,int j){
	long long tempx,tempy,tempz;
	tempx=abs(x[i]-x[j]);
	tempy=abs(y[i]-y[j]);
	tempz=abs(z[i]-z[j]);
	if(tempz>2*r||tempy>2*r||tempz>2*r)return false;
	long long temp=tempx*tempx+tempy*tempy+tempz*tempz;
	if(temp<=1LL*4*r*r)return true;
	return false;
}

void work(){
	memset(fi,0,sizeof(fi));
	n=read();h=read_();r=read_();sum=0;
	for(int i=1;i<=n;++i){
		x[i]=read_();y[i]=read_();z[i]=read_();
	}
	for(int i=1;i<=n;++i){
		if(abs(z[i])<=r)make_line(0,i);
		if(abs(h-z[i])<=r)make_line(i,n+1);
		for(int j=i+1;j<=n;++j)
			if(check(i,j))make_line(i,j);
	}
	memset(f,0,sizeof(f));
	f[0]=1;
	head=1,tail=1,q[1]=0;
	while(head<=tail){
		int u=q[head];
		for(int i=fi[u];i;i=l[i].next){
			int v=l[i].b;
			if(!f[v]){
				f[v]=1;
				q[++tail]=v;
			}
			if(v==n+1){
				printf("Yes\n");
				return;
			}
		}
		++head;
	}
	printf("No\n");
}
		

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	for(int i=1;i<=t;++i)
		work();
	return 0;
}

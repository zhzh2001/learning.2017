#include<bits/stdc++.h>
using namespace std;
struct point{
	int f,s;
}p[300010];
int n,m,q,x,y,a[1010][1010];

int read(){
	int num=0,flag=1;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')flag=-1;
	for(;c>='0'&&c<='9';c=getchar())
		num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}

void print(int x){
	if(x>9)print(x/10);
	putchar(x%10+48);
}

void work_1(){
	for(int i=1;i<=n;++i)
		for(int j=1;j<=m;++j)
			a[i][j]=(i-1)*m+j;
	for(int i=1;i<=q;++i){
		x=read();y=read();
		int temp=a[x][y];
		print(temp);
		putchar('\n');
		for(int i=y;i<m;++i)
			a[x][i]=a[x][i+1];
		for(int i=x;i<n;++i)
			a[i][m]=a[i+1][m];
		a[n][m]=temp;
	}
	exit(0);
}

void work_3(){
	p[0].s=1;p[m+1].f=m;
	for(int i=1;i<=m;++i){
		p[i].f=i-1;p[i].s=i+1;
	}
	for(int i=1;i<=q;++i){
		x=read();y=read();
		int j=0;
		for(int t=1;t<=y;++t)j=p[j].s;
		print(j);
		putchar('\n');
		p[p[j].f].s=p[j].s;
		p[p[j].s].f=p[j].f;
		p[j].s=m+1;
		p[j].f=p[m+1].f;
		p[p[j].f].s=j;
		p[m+1].f=j;
	}
	exit(0);
}

void init(){
	n=read();m=read();q=read();
	if(n<=1000&&m<=1000)work_1();
	//if(q<=500)work_2();
	if(n==1)work_3();
	//work_4();
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	init();
	return 0;
}

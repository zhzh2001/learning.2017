#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <cmath>
#define sqr(x) ((x)*(x))
const double eps=1e-8;
const int N=1010;
inline int judge(double x)
{
	if(x>eps) return 1;
	if(x<-eps) return -1;
	return 0;
}
using namespace std;
struct point{
	double x,y,z;
};
inline double dis(point a,point b)
{
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z));
}
point hole[N];
int n,flag[N];
int get_flag(int pos)
{
	return pos==flag[pos]?pos:flag[pos]=get_flag(flag[pos]);
}
double h,r;
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	for(;t--;)
	{
		scanf("%d%lf%lf",&n,&h,&r);
		for(int i=1;i<=n+2;++i) flag[i]=i;
		for(int i=1;i<=n;++i)
		{
			scanf("%lf%lf%lf",&hole[i].x,&hole[i].y,&hole[i].z);
			for(int j=1;j<i;++j)
			{
				if(judge(dis(hole[i],hole[j])-2*r)<=0) flag[get_flag(i)]=get_flag(j);
			}
			if(judge(hole[i].z-r)<=0) flag[get_flag(n+1)]=get_flag(i);
			if(judge(h-hole[i].z-r)<=0) flag[get_flag(n+2)]=get_flag(i);
		}
		if(get_flag(n+1)==get_flag(n+2)) puts("Yes");
		else puts("No");
	}
	return 0;
}

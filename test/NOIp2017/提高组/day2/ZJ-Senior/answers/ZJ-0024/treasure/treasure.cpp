#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>
#define lowbit(x) ((x)&(-(x)))
using namespace std;
template <typename tp> inline void read(tp& x)
{
	x=0;char tmp;bool key=0;
	for(tmp=getchar();!(tmp>='0'&&tmp<='9');tmp=getchar()) key=(tmp=='-');
	for(;tmp>='0'&&tmp<='9';tmp=getchar()) x=(x<<1)+(x<<3)+tmp-'0';
	if(key) x=-x;
}
const int TOT=4200,N=13;
int dp[TOT][N][N],n,m,dis[N][N],ans;
bool vis[N][N];
inline int get_cond(int a,int b)
{
	for(int i=n;i>=1;--i)if(a>>(i-1)&1)
	{
		a&=(1<<n)-1-((b&1)<<(i-1));
		b>>=1;
	}
	return a;
}
inline int cnt(int x)
{
	int res=0;
	for(;x;) res++,x-=lowbit(x);
	return res;
}
void dfs(int pos,int dep)
{
	if(vis[pos][dep]||dep>n) return;
	vis[pos][dep]=1;
	dp[1<<pos>>1][pos][dep]=0;
	for(int now=(1<<pos>>1)+1;now<(1<<n);++now)if((now>>(pos-1))&1)
	{
		int co=cnt(now),con;
		for(int i=1;i<=n;++i)if(dis[pos][i]<ans&&((now>>(i-1))&1))
		{
			if(!((now>>(i-1))&1)) continue;
			for(int j=0;j<(1<<(co));++j)
			{
				con=get_cond(now,j);
				if(!((con>>(i-1))&1)) continue;
				if((con>>(pos-1))&1) continue;
				dfs(i,dep+1);
				dp[now][pos][dep]=min(dp[now][pos][dep],\
				dp[now-con][pos][dep]+dp[con][i][dep+1]+dis[pos][i]*(dep+1));
			}
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int x,y,v;
	memset(dis,0x3f,sizeof dis);
	ans=dis[1][1];
	read(n);read(m);
	for(int i=1;i<=m;++i)
	{
		read(x);read(y);read(v);
		dis[x][y]=dis[y][x]=min(dis[x][y],v);
	}
	memset(dp,0x3f,sizeof dp);
	for(int i=1;i<=n;++i)
		dfs(i,0);
	for(int i=1;i<=n;++i) ans=min(ans,dp[(1<<n)-1][i][0]);
	printf("%d\n",ans);
	return 0;
}

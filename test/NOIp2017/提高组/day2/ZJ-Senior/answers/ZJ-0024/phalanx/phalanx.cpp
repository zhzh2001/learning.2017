#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <vector>
using namespace std;
template <typename tp> inline void read(tp& x)
{
	x=0;char tmp;bool key=0;
	for(tmp=getchar();!(tmp>='0'&&tmp<='9');tmp=getchar()) key=(tmp=='-');
	for(;tmp>='0'&&tmp<='9';tmp=getchar()) x=(x<<1)+(x<<3)+tmp-'0';
	if(key) x=-x;
}
const int N=1010;
vector<int>mar[N];
int n,m,q,ans;
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int x,y;
	read(n);read(m);read(q);
	for(int i=1;i<=n;++i)
	{
		mar[i].push_back(0);
		for(int j=1;j<=m;++j)
			mar[i].push_back((i-1)*m+j);
	}
	for(register int i=1;i<=q;++i)
	{
		read(x);read(y);
		ans=mar[x][y];
		printf("%d\n",ans);
		for(register int j=y;j<m;++j) mar[x][j]=mar[x][j+1];
		for(register int j=x;j<n;++j) mar[j][m]=mar[j+1][m];
		mar[n][m]=ans;
	}
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ctype.h>
#include <cmath>
using namespace std;
typedef long long LL;
const int maxn=1005;
int n,h,r,que[maxn],head,tail;
bool vis[maxn];
struct NodeData{
	int x,y,z;
} node[maxn];
inline int read(){
	int ret=0; char ch=getchar(); bool fl=0;
	while (!isdigit(ch))
		fl^=!(ch^'-'),ch=getchar();
	while (isdigit(ch))
		ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	if (fl) ret=-ret; return ret;
}
inline LL sqr(int x){
	return (LL)x*x;
}
inline LL get_dis(int x,int y){
	return sqr(node[x].x-node[y].x)+sqr(node[x].y-node[y].y)+sqr(node[x].z-node[y].z);
}
inline bool check(int x,int y){
	return get_dis(x,y)<=sqr(r<<1);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (int T=read();T--;){
		n=read(),h=read(),r=read();
		for (int i=1;i<=n;i++){
			int x=read(),y=read(),z=read();
			node[i]=(NodeData){x,y,z};
		}
		memset(vis,0,sizeof(vis));
		head=tail=0;
		for (int i=1;i<=n;i++)
			if (node[i].z<=r)
				vis[i]=1,que[++tail]=i;
		while (head!=tail){
			head++;
			for (int j=1;j<=n;j++)
				if (!vis[j]&&check(que[head],j))
					vis[j]=1,que[++tail]=j;
		}
		bool oo=0;
		for (int i=1;i<=n;i++)
			if (vis[i]&&node[i].z+r>=h){
				oo=1;
				break;
			}
		printf(oo?"Yes\n":"No\n");
	}
	return 0;
}

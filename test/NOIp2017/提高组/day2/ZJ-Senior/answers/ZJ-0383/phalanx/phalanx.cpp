#include <cstdio>
#include <cstring>
#include <algorithm>
#include <ctype.h>
using namespace std;
typedef pair<int,int> pairs;
int n,m,Q;
pairs q[300005];
inline int read(){
	int ret=0; char ch=getchar(); bool fl=0;
	while (!isdigit(ch))
		fl^=!(ch^'-'),ch=getchar();
	while (isdigit(ch))
		ret=(ret<<3)+(ret<<1)+ch-'0',ch=getchar();
	if (fl) ret=-ret; return ret;
}
namespace Subtask1{
	int id[1005][1005];
	inline void Solve(){
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				id[i][j]=(i-1)*m+j;
		for (int k=1;k<=Q;k++){
			int x=q[k].first,y=q[k].second,tmp;
			printf("%d\n",tmp=id[x][y]);
			for (int j=y;j<=m;j++)
				id[x][j]=id[x][j+1];
			for (int i=x;i<=n;i++)
				id[i][m]=id[i+1][m];
			id[n][m]=tmp;
		}
	}
}
namespace Subtask2{
	#define lowbit(x) ((x)&(-(x)))
	int sta[900005],top;
	int T[900005];
	inline void Change(int x){
		for (int i=x;i<=top;i+=lowbit(i))
			T[i]++;
	}
	inline int Query(int x){
		int ret=0;
		for (int i=x;i>0;i-=lowbit(i))
			ret+=T[i];
		return ret;
	}
	inline void Solve(){
		memset(T,0,sizeof(T));
		for (int i=1;i<=m;i++)
			sta[++top]=i;
		for (int i=2;i<=n;i++)
			sta[++top]=m*i;
		for (int i=1;i<=Q;i++){
			int x=q[i].first,y=q[i].second,tmp=y;
			for (int last=0;;){
				int xx=Query(tmp)-Query(last);
				if (xx==0)
					break;
				last=tmp,tmp+=xx;
			}
			printf("%d\n",sta[tmp]);
			Change(tmp),sta[++top]=sta[tmp];
		}
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),Q=read();
	bool fuck=1;
	for (int i=1;i<=Q;i++){
		q[i].first=read(),q[i].second=read();
		if (q[i].first!=1)
			fuck=0;
	}
	if (fuck){
		using namespace Subtask2;
		Solve();
		return 0;
	}
	if (n<=1000&&m<=1000){
		using namespace Subtask1;
		Solve();
		return 0;
	}
	return 0;
}

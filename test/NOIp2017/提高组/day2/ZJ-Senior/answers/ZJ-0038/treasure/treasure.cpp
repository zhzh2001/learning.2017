#include<bits/stdc++.h>
#define N 13
#define MO 2333337
#define M 6100000
using namespace std;

inline int read()
{
	int ret=0; char c=getchar();
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int p[N],t,o,g[N],gg[N][N],u,v,w,h[N][N],n,m,mi,tot,c[M][2],q[M];
int tt,fl,qn,I,a[MO+10];
long long cc[M];

inline void ins(int p, int v)
{
	int u=p%MO;
	for(int o=a[u]; o; o=c[o][1])
	{
		if(cc[o]==p)
		{
			if(c[o][0]<v)return;
			c[o][0]=v;
			return;
		}
	}
	cc[++tot]=p;
	c[tot][1]=a[u];
	c[tot][0]=v;
	a[u]=tot;
	q[++qn]=p;
}

inline int que(long long p)
{
	int u=p%MO;
	for(int o=a[u]; o; o=c[o][1])
	{
		if(cc[o]==p)
		{
			return c[o][0];
		}
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(); m=read();
	memset(h,0x3f,sizeof(h)); 
	mi=1000000000;
	for(int i=1; i<=m; ++i)
	{
		u=read(); v=read(); w=read();
		gg[u][v]=1;
		gg[v][u]=1;
		if(h[u][v]>w)
		{
			h[u][v]=w;
			h[v][u]=w;
		}
	}
	g[1]=1;
	for(int i=2; i<=n; ++i)g[i]=g[i-1]*n;
	for(I=1; I<=n; ++I)
	{
		memset(a,0,sizeof(a));
		tot=0;
		ins(0,0);
		q[qn=1]=0;
		for(int q1=1; q1<=qn; ++q1)
		{
			tt=t=q[q1]; o=que(t);
			for(int i=n; i; --i)
			{
				p[i]=tt/g[i];
				tt=tt%g[i];
			}
			
			fl=1;
			for(int i=1; i<=n; ++i)if(i!=I && !p[i])
			{
				fl=0;
				if(gg[I][i])ins(t+g[i],o+h[I][i]);
				for(int j=1; j<=n; ++j)if(p[j] && gg[i][j])
				ins(t+g[i]*(p[j]+1),o+h[i][j]*(p[j]+1));
			}
			if(fl)mi=min(mi,o);
			if(qn>6000000)break;
		}
	}
	printf("%d\n",mi);
}

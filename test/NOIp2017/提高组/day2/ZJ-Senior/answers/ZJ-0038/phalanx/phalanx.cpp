#include<bits/stdc++.h>
using namespace std;

inline int read()
{
	int ret=0; char c=getchar();
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int n,m,q;

namespace cyb
{
	int x[505],y[505];
	
	unsigned che(unsigned u, unsigned v ,int i)
	{
		if(!i)return (u-1)*m+v;
		if(x[i]>u)return che(u, v ,i-1);
		if(x[i]==u)
		{
			if(v==m)
			{
				if(u==n)return che(x[i],y[i],i-1);
			    return che(u+1,v,i-1);
			}
			if(v>=y[i])return che(u,v+1,i-1);
			return che(u,v,i-1);
		}
		if(v==m)
		{
			if(u==n)return che(x[i],y[i],i-1);
			return che(u+1,v,i-1);
		}
		return che(u,v,i-1);
	}
	
	int main()
	{
		for(int i=1; i<=q; ++i)
		{
			x[i]=read(); y[i]=read();
			printf("%u\n",che(x[i],y[i],i-1));
		}
		return 0;
	}
}

namespace wqy
{
	int u,v,a[610009],g,t[2100005],h,I;
	
	void ins(int p, int l, int r)
	{
		if(l==r)
		{
			++t[p];
			return;
		}
		int mid=(l+r)>>1;
		if(I<=mid)ins(p<<1,l,mid);
		else ins((p<<1)|1,mid+1,r);
		t[p]=t[p<<1]+t[(p<<1)|1];
	}
	
	void dele(int p, int l, int r)
	{
		if(l==r)
		{
			--t[p];
			return;
		}
		int mid=(l+r)>>1;
		if(g<=mid)dele(p<<1,l,mid); else dele((p<<1)|1,mid+1,r);
		t[p]=t[p<<1]+t[(p<<1)|1];
	}
	
	int erfen(int p, int l, int r, int v)
	{
		if(l==r)return l;
		int mid=(l+r)>>1;
		if(v>t[p<<1])return erfen((p<<1)|1,mid+1,r,v-t[p<<1]);
		return erfen(p<<1,l,mid,v);
	}
	
	int main()
	{
		h=m+q; 
		for(int i=1; i<=m; ++i)a[i]=i;
		for(I=1; I<=m; ++I)ins(1,1,h);
		I=m;
		for(int i=1; i<=q; ++i)
		{
			++I;
			u=read(); v=read();
			g=erfen(1,1,h,v);
			printf("%d\n",a[g]);
			a[I]=a[g];
			dele(1,1,h);
			ins(1,1,h);
		}
		return 0;
	}
}

namespace wo
{
	int uu,tt,u,v,g,t[2100005],h,I;
	long long b[610009],a[610009];
	void ins(int p, int l, int r)
	{
		if(l==r)
		{
			++t[p];
			return;
		}
		int mid=(l+r)>>1;
		if(I<=mid)ins(p<<1,l,mid);
		else ins((p<<1)|1,mid+1,r);
		t[p]=t[p<<1]+t[(p<<1)|1];
	}
	
	void dele(int p, int l, int r)
	{
		if(l==r)
		{
			--t[p];
			return;
		}
		int mid=(l+r)>>1;
		if(g<=mid)dele(p<<1,l,mid); else dele((p<<1)|1,mid+1,r);
		t[p]=t[p<<1]+t[(p<<1)|1];
	}
	
	int erfen(int p, int l, int r, int v)
	{
		if(l==r)return l;
		int mid=(l+r)>>1;
		if(v>t[p<<1])return erfen((p<<1)|1,mid+1,r,v-t[p<<1]);
		return erfen(p<<1,l,mid,v);
	}
	
	int main()
	{
		h=m+q; tt=n-1; uu=0;
		for(int i=1; i<=m; ++i)a[i]=i;
		for(int i=1; i<n; ++i)b[i]=(long long)i*m+m;
		for(I=1; I<=m; ++I)ins(1,1,h);
		I=m;
		for(int i=1; i<=q; ++i)
		{
			++I;
			u=read(); v=read();
			g=erfen(1,1,h,v);
			printf("%lld\n",a[g]);
			b[++tt]=a[g];
			a[I]=b[++uu];
//			++uu;
			dele(1,1,h);
			ins(1,1,h);
		}
		return 0;
	}
}


int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if(q<=500)return cyb::main();
	if(n==1)return wqy::main();
	return wo::main();
}

#include<bits/stdc++.h>
#define eps 1e-6
#define N 1005
#define M 2500005
using namespace std;

inline int read()
{
	int ret=0; char c=getchar();
	while(c<48||c>57)c=getchar();
	while(c>=48&&c<=57)ret=ret*10+c-48,c=getchar();
	return ret;
}

int h,r,T,v[N],tot,n,c[M][2],a[N],x[N],y[N],z[N];

inline void add_e(int u, int v)
{
	c[++tot][0]=v; c[tot][1]=a[u]; a[u]=tot;
}

unsigned long long g,xx,yy,zz;
double gg;

void dfs(int p)
{
	for(int o=a[p]; o; o=c[o][1])
	{
		if(v[c[o][0]])continue;
		v[c[o][0]]=1;
		dfs(c[o][0]);
	}
}

int main()
{
//	cerr<< sqrt( (int ) 6)<<endl;
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--)
	{
		memset(v,0,sizeof(v));
		memset(a,0,sizeof(a));
		tot=0;
		n=read(); h=read(); r=read();
		for(int i=1; i<=n; ++i)
		{
			scanf("%d%d%d",x+i,y+i,z+i);
		}
		for(int i=1; i<=n; ++i)if(z[i]<=r)
		{
			add_e(0,i);
			add_e(i,0);
		}
		for(int i=1; i<=n; ++i)if(z[i]+r>=h)
		{
			add_e(n+1,i);
			add_e(i,n+1);
		}
		
		for(int i=1; i<n; ++i)
		for(int j=i+1; j<=n; ++j)
		{
			if(x[i]>x[j])xx=x[i]-x[j]; else xx=x[j]-x[i];
			if(y[i]>y[j])yy=y[i]-y[j]; else yy=y[j]-y[i];
			if(z[i]>z[j])zz=z[i]-z[j]; else zz=z[j]-z[i];
			g=xx*xx+yy*yy+zz*zz;
			gg=sqrt(g);
			if(gg<=r+r+eps)
			{			
				add_e(i,j);
				add_e(j,i);
			}
		}
		
		++n;
		v[0]=1;
		dfs(0);
		if(v[n])puts("Yes"); else puts("No");
	}
}

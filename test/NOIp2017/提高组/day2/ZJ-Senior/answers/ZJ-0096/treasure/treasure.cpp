#include<cstdio>
#include<iostream>
#include<queue>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=20,oo=2e9;
int n,m,v;
int g[N][N],d[N];
ll solve(int k){
	memset(d,0,sizeof(d));
	queue<int>q;
	ll ans=0;
	q.push(k);
	d[k]=1;
	int now,x;
	while(!q.empty()){
		now=q.front();q.pop();
		for(int i=1;i<=n;i++){
			if(!d[i]&&g[now][i]<1e9){
				q.push(i);
				ans+=1LL*d[now]*g[now][i],d[i]=d[now]+1;
			}
		}
	}
	return ans;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int t1,t2,t3;
	memset(g,127/2,sizeof(g));
	ll ans=oo;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&t1,&t2,&t3);
		g[t1][t2]=min(g[t1][t2],t3);
		g[t2][t1]=min(g[t2][t1],t3);
	}
	for(int i=1;i<=n;i++) ans=min(ans,solve(i));
	printf("%lld",ans);
	return 0;
}

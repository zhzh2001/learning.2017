#include<cstdio>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1010,Q=300010;
int read(){
	int x=0,w=1;char c=0;
	while(c<'0'||c>'9'){if(c=='-') w=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=(x<<3)+(x<<1)+c-'0';c=getchar();}
	return x*w;
}
int n,m,q;
int x[Q],y[Q];
inline ll getid(int x,int y){return 1ll*(x-1)*m+y;}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int t1,t2,nowx,nowy;
//	scanf("%d%d%d",&n,&m,&q);
	n=read(),m=read(),q=read();
	for(int i=1;i<=q;i++){
//		scanf("%d%d",&x[i],&y[i]);
		x[i]=read(),y[i]=read();
		nowx=x[i],nowy=y[i];
		for(int j=i-1;j>=1;j--){
			if(nowx==x[j]&&nowy<m&&nowy>=y[j]) nowy++;
			else if(nowy==m&&nowx<n&&nowx>=x[j]) nowx++;
			else if(nowx==n&&nowy==m) nowx=x[j],nowy=y[j];
		}
		printf("%lld\n",getid(nowx,nowy));
	}
	return 0;
}

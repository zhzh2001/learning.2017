#include<cstdio>
#include<iostream>
#include<cstring>
#include<vector>
#include<queue>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1010;
int n;
ll h,r;
int gwy_type[N];
ll x[N],y[N],z[N];
vector<int>point[N];
bool vis[N];
inline ll calc(int a,int b){return (x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]);}
void solve(){
	queue<int>q;
	memset(vis,0,sizeof(vis));
	memset(gwy_type,0,sizeof(gwy_type));
	for(int i=1;i<=1000;i++) point[i].clear();
	scanf("%d%lld%lld",&n,&h,&r);
	ll r2=r*r*4;
	for(int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
	for(int i=1;i<=n;i++){
		if(z[i]<=r&&z[i]>=-r){
			gwy_type[i]+=1;
			q.push(i);
			vis[i]=1;
		}
		if(z[i]>=h-r&&z[i]<=h+r) gwy_type[i]+=2;
		for(int j=i+1;j<=n;j++){
			ll temp=calc(i,j);
			if(calc(i,j)<=r2){
				point[i].push_back(j);point[j].push_back(i);
			}
		}
	}
	int now,nxt;
	while(!q.empty()){
		now=q.front();q.pop();
		if(gwy_type[now]>=2){
			puts("Yes");
			return;
		}
		for(int i=0;i<point[now].size();i++){
			nxt=point[now][i];
			if(!vis[nxt]){
				vis[nxt]=1;
				q.push(nxt);
			}
		}
	}
	puts("No");
	return;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);;
	while(T--) solve();
	return 0;
}

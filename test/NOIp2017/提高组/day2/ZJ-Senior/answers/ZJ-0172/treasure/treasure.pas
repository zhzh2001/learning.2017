const maxn=13;
var dist:array[0..maxn,0..maxn]of longint;
    i,j,u1,v1,w,tot,ans1,ans,n,m:longint;
    f:array[0..maxn]of boolean;
procedure dfs(u,ces:longint);
var i,j,tail,v,d1,t1,pos:longint;
    s1,s:int64;
    q:array[0..maxn]of longint;
begin
    f[u]:=true;tail:=0;
    s:=(1<<n)-1;
    for i:=0 to s do
    begin
        s1:=i;pos:=0;d1:=0;t1:=0;tail:=0;
        while s1>0 do
        begin
            inc(pos);
            if (s1 mod 2=1)and(not f[pos])and(dist[pos,u]<500001) then
            begin
                inc(tail);q[tail]:=pos;
            end;
            s1:=s1>>1;
        end;
        for j:=1 to tail do
        begin
                v:=q[j];
                ans1:=ans1+dist[v,u]*(ces+1);
                f[v]:=true;
                inc(d1,dist[v,u]*(ces+1));
                inc(tot);inc(t1);
        end;
        if tot=n then
        if ans>ans1 then
            ans:=ans1;
            if ans1>ans then
            begin
                for j:=1 to tail do
                    f[q[j]]:=false;
                dec(tot,t1);
                dec(ans1,d1);
                continue;
            end;
        for j:=1 to tail do
        begin
            v:=q[j];
               dfs(v,ces+1);
        end;
        for j:=1 to tail do
            f[q[j]]:=false;
        dec(tot,t1);
        dec(ans1,d1);
    end;
end;
procedure dfs1(i,ces:longint);
var j:longint;
begin
    f[i]:=true;
    for j:=1 to n do
    begin
        if (dist[i,j]<500001)and(not f[j]) then
        begin
            inc(ans1,dist[i,j]*(ces+1));
            dfs1(j,ces+1);
        end;
    end;
end;
begin
    assign(input,'treasure.in');assign(output,'treasure.out');
    reset(input);rewrite(output);
    readln(n,m);
    for i:=1 to n do
        for j:=1 to n do
            dist[i,j]:=500001;
   // writeln((1<<n)-1);
    for i:=1 to m do
    begin
        readln(u1,v1,w);
        if dist[u1,v1]>w then
        begin
            dist[u1,v1]:=w;
            dist[v1,u1]:=w;
        end;
    end;
    ans:=maxlongint;
    if m=n-1 then
   begin
        for i:=1 to n do
        begin
            ans1:=0;
            fillchar(f,sizeof(f),0);
            dfs1(i,0);
            if ans>ans1 then ans:=ans1;
        end;
        writeln(ans);
        close(input);close(output);
        halt;
   end;
    for i:=1 to n do
    begin
        fillchar(f,sizeof(f),false);
        tot:=1;ans1:=0;
       // f[i]:=true;
        dfs(i,0);
    end;
    writeln(ans);
    close(input);close(output);
end.

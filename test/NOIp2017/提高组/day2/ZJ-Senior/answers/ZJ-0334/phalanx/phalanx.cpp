#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N1 = 50505;
const int N2 = 303030;
const int INF = 1 << 27;
typedef long long ll;

inline char get(void) {
	static char buf[100000], *S = buf, *T = buf;
	if (S == T) {
		T = (S = buf) + fread(buf, 1, 100000, stdin);
		if (S == T) return EOF;
	}
	return *S++;
}
template<typename T>
inline void read(T &x) {
	static char c; x = 0; int sgn = 0;
	for (c = get(); c < '0' || c > '9'; c = get()) if (c == '-') sgn = 1;
	for (; c >= '0' && c <= '9'; c = get()) x = x * 10 + c - '0';
	if (sgn) x = -x;
}

int n, m, q;

namespace Work1 {
	int mpx[N1];
	int x[N1], y[N1], xx[N1];
	ll mp[505][N1];
	ll p[N1];
	int xnt, ynt;
	ll st, now;
	inline void Solve(void) {
		xnt = ynt = 0;
		for (int i = 1; i <= q; i++) {
			read(x[i]); read(y[i]);
			mpx[++xnt] = x[i];
		}
		sort(mpx + 1, mpx + xnt + 1);
		xnt = unique(mpx + 1, mpx + xnt + 1) - mpx - 1;
		for (int i = 1; i <= q; i++)
			xx[i] = lower_bound(mpx + 1, mpx + xnt + 1, x[i]) - mpx;
		for (int i = 1; i <= xnt; i++) {
			st = (mpx[i] - 1) * m;
			for (int j = 1; j <= m; j++) mp[i][j] = ++st;
		}
		for (int i = 1; i <= n; i++) p[i] = m * i;
		for (int i = 1; i <= q; i++) {
			now = mp[xx[i]][y[i]];
			printf("%d\n", now);
			for (int j = y[i]; j < m; j++)
				mp[xx[i]][j] = mp[xx[i]][j + 1];
			for (int j = x[i]; j < n; j++)
				p[j] = p[j + 1];
			p[n] = now;
			for (int j = 1; j <= xnt; j++)
				mp[j][m] = p[mpx[j]];
		}
	}
}
namespace Work2 {
	ll x[N2 * 3], a[N2 * 3];
	int b[N2 * 3];
	int y, tt, pp;
	int sum[N2 * 12], key[N2 * 12];
	inline void Modify(int o, int l, int r, int pos, int x) {
		if (l == r) {
			key[o] = x; sum[o] = (x != 0);
			return;
		}
		int mid = (l + r) >> 1;
		if (pos <= mid) Modify(o << 1, l, mid, pos, x);
		else Modify(o << 1 | 1, mid + 1, r, pos, x);
		sum[o] = sum[o << 1] + sum[o << 1 | 1];
	}
	inline int Query(int o, int l, int r, int pos) {
		if (l == r) {
			pp = l;
			return key[o];
		}
		int mid = (l + r) >> 1;
		if (pos <= sum[o << 1]) return Query(o << 1, l, mid, pos);
		else return Query(o << 1 | 1, mid + 1, r, pos - sum[o << 1]);
	}
	inline void Build(int o, int l, int r) {
		if (l == r) {
			key[o] = b[l]; sum[o] = (b[l] != 0);
			return;
		}
		int mid = (l + r) >> 1;
		Build(o << 1, l, mid);
		Build(o << 1 | 1, mid + 1, r);
		sum[o] = sum[o << 1] + sum[o << 1 | 1];
	}
	inline void Solve(void) {
		for (int i = 1; i <= q; i++) {
			read(x[i]); read(y);
			x[i] = (x[i] - 1) * m + y;
		}
		tt = n + m - 1 + q;
		for (int i = 1; i <= m; i++) a[b[i] = i] = i;
		for (int i = 1; i < n; i++) a[b[i + m] = i + m] = (i + 1) * m;
		Build(1, 1, tt);
		for (int i = 1; i <= q; i++) {
			int now = Query(1, 1, tt, x[i]);
			printf("%d\n", a[now]);
			Modify(1, 1, tt, pp, 0);
			Modify(1, 1, tt, n + m + i - 1, now);
		}
	}
}

int main(void) {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n); read(m); read(q);
	if (q <= 500) Work1::Solve();
	else Work2::Solve();
	fclose(stdin); fclose(stdout);
	return 0;
}

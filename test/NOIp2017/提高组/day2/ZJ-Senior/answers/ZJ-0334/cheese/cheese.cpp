#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 1010;
typedef long long ll;

inline char get(void) {
	static char buf[100000], *S = buf, *T = buf;
	if (S == T) {
		T = (S = buf) + fread(buf, 1, 100000, stdin);
		if (S == T) return EOF;
	}
	return *S++;
}
template<typename T>
inline void read(T &x) {
	static char c; x = 0; int sgn = 0;
	for (c = get(); c < '0' || c > '9'; c = get()) if (c == '-') sgn = 1;
	for (; c >= '0' && c <= '9'; c = get()) x = x * 10 + c - '0';
	if (sgn) x = -x;
}

ll x[N], y[N], z[N];
int test, n, S, T;
int fa[N];
ll h, r;

inline int Fa(int x) {
	return x == fa[x] ? x : fa[x] = Fa(fa[x]);
}
inline void Merge(int x, int y) {
	fa[Fa(x)] = Fa(y);
}
inline ll sqr(ll x) {
	return x * x;
}

int main(void) {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	read(test);
	while(test--) {
		read(n); read(h); read(r);
		for (int i = 1; i <= n; i++) {
			read(x[i]); read(y[i]); read(z[i]);
			fa[i] = i;
		}
		fa[S = n + 1] = n + 1;
		fa[T = n + 2] = n + 2;
		for (int i = 1; i <= n; i++) {
			if (z[i] - r <= 0) Merge(S, i);
			if (z[i] + r >= h) Merge(i, T);
			for (int j = i + 1; j <= n; j++)
				if (4ll * r * r - sqr(x[i] - x[j]) >= sqr(y[i] - y[j]) + sqr(z[i] - z[j]))
					Merge(i, j);
		}
		if (Fa(S) == Fa(T)) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

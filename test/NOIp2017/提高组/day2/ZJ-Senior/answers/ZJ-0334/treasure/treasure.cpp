#include <queue>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N = 20;
const int INF = 1 << 27;
typedef pair<int, int> Pairs;

inline char get(void) {
	static char buf[100000], *S = buf, *T = buf;
	if (S == T) {
		T = (S = buf) + fread(buf, 1, 100000, stdin);
		if (S == T) return EOF;
	}
	return *S++;
}
template<typename T>
inline void read(T &x) {
	static char c; x = 0; int sgn = 0;
	for (c = get(); c < '0' || c > '9'; c = get()) if (c == '-') sgn = 1;
	for (; c >= '0' && c <= '9'; c = get()) x = x * 10 + c - '0';
	if (sgn) x = -x;
}

queue<int> Q;
int mp[N][N];
int vis[N], dep[N];
int sta[N], fa[N], f[N];
int n, m, x, y, z, clc, ans;
int fl, lst, top;

inline int Fa(int x) {
	return x == fa[x] ? x : Fa(fa[x]);
}
inline void Merge(int x, int y) {
	sta[++top] = Fa(x);
	fa[Fa(x)] = Fa(y);
}
inline void Solve1(void) {
	clc = 0; int res;
	for (int i = 1; i <= n; i++) {
		vis[i] = ++clc; dep[i] = 1; res = 0; Q.push(i);
		while (!Q.empty()) {
			x = Q.front(); Q.pop();
			for (int j = 1; j <= n; j++)
				if (mp[x][j] && vis[j] != clc) {
					Q.push(j); vis[j] = clc;
					dep[j] = dep[x] + 1;
					res += mp[x][j] * dep[x];
				}
		}
		ans = min(ans, res);
	}
}
inline int Get(int x) {
	if (dep[x]) return dep[x];
	if (!f[x]) return dep[x] = 1;
	return dep[x] = Get(f[x]) + 1;
}
inline void Check(void) {
	int res = 0;
	for (int i = 1; i <= n; i++) dep[i] = 0;
	for (int i = 1; i <= n; i++) Get(i);
	for (int i = 1; i <= n; i++)
		if (f[i]) {
			res += mp[i][f[i]] * dep[f[i]];
		}
	ans = min(ans, res);
}
inline void dfs(int u, int step) {
	for (int i = 1; i <= n; i++)
		if (mp[u][i] && Fa(i) != Fa(u)) {
			int pos = top;
			Merge(i, u); f[u] = i;
			dfs(u % n + 1, step + 1);
			for (int i = top; i > pos; i--)
				fa[sta[i]] = sta[i];
			--top; f[u] = 0;
		}
	if (step == n) return Check();
}
inline void Solve2(void) {
	for (int j = 1; j <= n; j++) {
		f[j] = 0; fa[j] = j;
	}
	for (int i = 1; i <= n; i++) dfs(i, 1);
}

int main(void) {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	read(n); read(m); fl = 1;
	for (int i = 1; i <= m; i++) {
		read(x); read(y); read(z);
		if (i > 1 && z != lst) fl = 0;
		lst = z;
		if (!mp[x][y]) mp[x][y] = INF;
		mp[x][y] = mp[y][x] = min(mp[x][y], z);
	}
	ans = INF;
	if (fl) Solve1();
	else Solve2();
	cout << ans << endl;
	fclose(stdin); fclose(stdout);
	return 0;
}

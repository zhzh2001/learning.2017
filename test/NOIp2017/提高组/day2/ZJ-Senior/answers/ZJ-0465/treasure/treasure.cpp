#include<bits/stdc++.h>
using namespace std;
int n,m,i,j,x,y,v,mi,sum,a[101][101];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i)
	  for(int j=1;j<=m;++j)
	    a[i][j]=5000000;
	for(int i=1;i<=m;++i){
	  scanf("%d%d%d",&x,&y,&v);
	  a[x][y]=min(a[x][y],v);
	  a[y][x]=min(a[y][x],v);
	}
	for(int k=1;k<=n;++k)
		for(int i=1;i<=n;++i)
	  		for(int j=1;j<=n;++j)
	    		if(i!=k&&i!=j&&j!=k)
					if(a[i][k]+a[j][k]<a[i][j]) a[i][j]=a[i][k]+a[j][k];
	mi=INT_MAX;
	for(int i=1;i<=n;++i)
	{
		sum=0;
		for(int j=1;j<=n;++j) if (i!=j) sum+=a[i][j];
		if (sum<mi) mi=sum;
	}
	printf("%d\n",mi);
	return 0;
}

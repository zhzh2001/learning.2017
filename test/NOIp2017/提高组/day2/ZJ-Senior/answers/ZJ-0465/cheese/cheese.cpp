#include<bits/stdc++.h>
using namespace std;
int T,n,h,r,dis[1007][1007],c[1007][1007],b[1007],e[1007],fl,t,w,dian;
long long p,rr;
struct xy
{
	int x,y,z;
}a[1007];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r); rr=4*r*r; memset(c,0,sizeof(c));
		for(int i=1;i<=n;++i) scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		for(int i=1;i<=n;++i) 
		  for(int j=1;j<=n;++j)
		  {
		    if(i==j) continue;
		    p=(a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z);
		    if(p<=rr) dis[i][j]=1; else dis[i][j]=0;
		  }
		for(int i=1;i<=n;++i)
		  for(int j=1;j<=n;++j){
		    if(i==j) continue;
		    if(dis[i][j]==1) c[i][++c[i][0]]=j;
		  }
		w=0; t=1; memset(e,0,sizeof(e));
		for(int i=1;i<=n;++i) if (a[i].z<=r) b[++w]=i,e[i]=1; 
		while(t<=w)
		{
			dian=b[t];
			for(int i=1;i<=c[dian][0];++i)
			  if(e[c[dian][i]]==0){
			  e[c[dian][i]]=1;
			  b[++w]=c[dian][i];
			}
			++t;
		}
		fl=0;
		for(int i=1;i<=n;++i) if(e[i]) if(a[i].z+r>=h) fl=1;
		if(fl==1) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

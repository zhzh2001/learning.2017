#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

typedef unsigned long long ll;
#define N 1055
#define M (1002 * 1002 + 15)
ll n,h,r,x[N],y[N],z[N];
ll num=0; ll hed[N]; ll color[N];
struct edgenode{ll to,nxt;} edges[M];

inline void addedge(ll u,ll v){
    edges[num] = (edgenode){v, hed[u]};
    hed[u] = num++;
}
inline bool Reachable(ll x,ll y,ll z, ll p,ll q,ll t, ll r){
    return (x-p)*(x-p) + (y-q)*(y-q) + (z-t) * (z-t) <= r * r;
}
void dfs(ll u){
    color[u] = 1;
    for(ll e=hed[u]; e!=(ll)-1; e=edges[e].nxt){
        ll v=edges[e].to;
        if(color[v] == 0) dfs(v);
    }
    color[u] = 2;
}

int main(){
    freopen("cheese.in","r",stdin);
    freopen("cheese.out","w",stdout);
    ll T;
    scanf("%llu", &T);
  for(ll XXX = 1; XXX <= T; XXX ++){
    scanf("%llu%llu%llu", &n,&h,&r);
    for(ll i=1;i<=n;i++){
        scanf("%llu%llu%llu", &x[i],&y[i],&z[i]);
    }
    for(ll i=0;i<=n+1;i++) hed[i] = -1;
    num = 0;
    for(ll i=1;i<=n;i++){
        if (Reachable(x[i],y[i],z[i], x[i],y[i],0, r)){
            addedge(0, i);
        }
        if (Reachable(x[i],y[i],z[i], x[i],y[i],h, r)){
            addedge(i, n+1);
        }
        for(ll j=i+1;j<=n;j++){
            if(Reachable(x[i],y[i],z[i],x[j],y[j],z[j], r*((ll)2))){
                addedge(i,j);
                addedge(j,i);
            }
        }
    }
    for(ll i=0;i<=n+1;i++)color[i]=0;
    dfs(0);
    if(color[n+1] == 2){
        puts("Yes");
    } else puts("No");
  }
    return 0;
}


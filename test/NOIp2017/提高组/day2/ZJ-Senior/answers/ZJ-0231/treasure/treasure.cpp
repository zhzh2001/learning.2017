#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,m,num,hed[255];
struct edgenode{int to,nxt,val;} edges[5555];
void add(int x,int y,int z){
    edges[num] = (edgenode){y,hed[x],z};
    hed[x] = num++;
}

int dfs(int x,int fa,int lg){
    int ans=0;
    for(int e=hed[x]; e!=-1;e=edges[e].nxt){
        int v=edges[e].to;
        if(v!=fa){
            ans += edges[e].val * (lg+1);
            ans += dfs(v,x,lg+1);
        }
    }
    return ans;
}

int main(){
    freopen("treasure.in","r",stdin);
    freopen("treasure.out","w",stdout);
    scanf("%d%d", &n,&m);
    for(int i=1;i<=n;i++)hed[i]=-1;
    for(int i=1;i<=m;i++){ //that's a tree with 20% data
        int x,y,z;
        scanf("%d%d%d", &x,&y,&z);
        add(x,y,z);
        add(y,x,z);
    }
    printf("%d\n", dfs(1,0,1));
    return 0;
}


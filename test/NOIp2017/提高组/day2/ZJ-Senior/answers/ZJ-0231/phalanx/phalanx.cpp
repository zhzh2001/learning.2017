#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

const int BLOCK = 547;
struct BlocklyTrain{
    int a[BLOCK];
    int siz;
    BlocklyTrain* nxt;
};
BlocklyTrain *hed;
struct MemorySaver{
    int* r[50007]; //row
    int c[50007]; //last column
    int _n;
    int _m;
    inline void init(int n,int m){
        _n=n;_m=m;
        for(int i=1;i<=n;i++) r[i]=NULL;
        for(int i=1;i<=n;i++) c[i]=i*m;
    }
    ~MemorySaver(){
        for(int i=1;i<=_n;i++){
            if(r[i] != NULL) delete[] r[i];
        }
    }
    int* GetRow(int i){
        if(r[i] == NULL){ //new memory area
            r[i] = new int[_m+10];
            for(int k=1;k<_m;k++) r[i][k] = (i-1)*_m+k;
        }
        return r[i];
    }
    inline int* GetLastCol(){ return c; }
} ms;


int n,m,q;
int a[1005][1005];

int main(){
    freopen("phalanx.in","r",stdin);
    freopen("phalanx.out","w",stdout);
    scanf("%d%d%d", &n,&m,&q);
  if ( (1ll*n+1ll*m)*(1ll*q)<=35000000ll && n<=1000 && m <= 1000){ //data special checking
    for(int i=1;i<=n;i++)for(int j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
    while(q--){
        int x,y;
        scanf("%d%d", &x,&y);
        int orit = a[x][y];
        for(int j=y+1; j<=m;j++) a[x][j-1] = a[x][j];
        for(int i=x+1; i<=n;i++) a[i-1][m] = a[i][m];
        a[n][m] = orit;
        printf("%d\n", orit);
    }
  } else if ((1ll*n+1ll*m)*(1ll*q)<=35000000ll){ //using memory saver
    ms.init(n,m);
    int * ry = ms.GetLastCol();
    while(q--){
        int x,y;
        scanf("%d%d", &x,&y);
        int * rx = ms.GetRow(x);
        int orit;
        if (y < m) orit = rx[y];
        else orit = ry[x];
        for(int j=y+1;j<m;j++) rx[j-1]=rx[j];
        if(y+1<=m) rx[m-1] = ry[x];
        for(int i=x+1;i<=n;i++) ry[i-1]=ry[i];
        ry[n]=orit;
        printf("%d\n",orit);
    }
  } else {
    hed = new BlocklyTrain();
    BlocklyTrain* h = hed; int cnt=0;
    for(int j=0;j<m;j++){
        if(j % BLOCK == 0 && h != hed){
            h->nxt = new BlocklyTrain();
            h = h->nxt;
            cnt += BLOCK;
        }
        h->a[j % BLOCK] = j+1;
    }
    h->siz = m - cnt;
    BlocklyTrain* tail = h;
    while(q--){
        int x,y;
        scanf("%d%d", &x,&y);
        h = hed; int p = 1;
        while(p+h->siz<=y) { //current block can't reach y
            p += h->siz;
            h = h->nxt;
        }
        int ans = h->a[y-p];
        for(int j=y-p+1;j<h->siz; j++) h->a[j-1]=h->a[j];
        h->siz--;
        if(tail->siz+1 <BLOCK){
            tail->a[tail->siz] = ans;
            tail->siz++;
        } else {
            tail->nxt = new BlocklyTrain();
            tail->nxt->siz=1;
            tail->nxt->a[0]=ans;
            tail=tail->nxt;
        }
        printf("%d\n",ans);
    }
  }
    return 0;
}

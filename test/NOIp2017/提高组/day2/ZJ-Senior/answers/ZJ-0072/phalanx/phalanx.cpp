#include<cstdio>
using namespace std;
int n,m,q,mp[1005][1005];
inline char nc(){
	static char buf[100000],*pa=buf,*pb=buf;
	return pa==pb&&(pb=(pa=buf)+fread(buf,1,100000,stdin),pa==pb)?EOF:*pa++;
}
inline void readi(int &x){
	x=0; char ch=nc();
	while ('0'>ch||ch>'9') ch=nc();
	while ('0'<=ch&&ch<='9') {x=x*10+ch-'0'; ch=nc();}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	readi(n); readi(m); readi(q);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) mp[i][j]=(i-1)*m+j;
	while (q--){
		int x,y,ans; readi(x); readi(y); ans=mp[x][y];
		printf("%d\n",ans);
		for (int i=y;i<m;i++) mp[x][i]=mp[x][i+1];
		for (int i=x;i<n;i++) mp[i][m]=mp[i+1][m];
		mp[n][m]=ans;
	}
	return 0;
}

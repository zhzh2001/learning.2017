#include<cstdio>
#include<cstring>
#define maxn 1005
#define maxe 2002015
#define LL long long
using namespace std;
int tst,n,tot,s,t,son[maxe],nxt[maxe],lnk[maxn],que[maxn];
LL R,r,a[maxn],b[maxn],c[maxn],hgh;
bool vs[maxn];
inline char nc(){
	static char buf[100000],*pa=buf,*pb=buf;
	return pa==pb&&(pb=(pa=buf)+fread(buf,1,100000,stdin),pa==pb)?EOF:*pa++;
}
inline void readi(int &x){
	x=0; char ch=nc(),lst='+';
	while ('0'>ch||ch>'9'){lst=ch; ch=nc();}
	while ('0'<=ch&&ch<='9') {x=x*10+ch-'0'; ch=nc();}
	if (lst=='-') x=-x;
}
inline void readL(LL &x){
	x=0; char ch=nc(),lst='+';
	while ('0'>ch||ch>'9'){lst=ch; ch=nc();}
	while ('0'<=ch&&ch<='9') {x=x*10+ch-'0'; ch=nc();}
	if (lst=='-') x=-x;
}
void _add(int x,int y){
	son[++tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;
}
void _init(){
	readi(n); readL(hgh); readL(r); R=r*r*4; s=n+1; t=n+2; tot=0;
	memset(nxt,0,sizeof(nxt));
	memset(lnk,0,sizeof(lnk));
	for (int i=1;i<=n;i++){
		readL(a[i]); readL(b[i]); readL(c[i]);
	}
	for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++){
			LL tem=(a[i]-a[j])*(a[i]-a[j])+(b[i]-b[j])*(b[i]-b[j])+(c[i]-c[j])*(c[i]-c[j]);
			if (tem<=R) {_add(i,j); _add(j,i);}
		}
	for (int i=1;i<=n;i++){
		if (c[i]<=r) _add(s,i);
		if (hgh-c[i]<=r) _add(i,t);
	}
}
void _solve(){
	memset(vs,0,sizeof(vs));
	int hed=0,til=1; que[1]=s; vs[s]=1;
	while (hed!=til)
		for (int j=lnk[que[++hed]];j;j=nxt[j])
			if (!vs[son[j]]){
				que[++til]=son[j]; vs[son[j]]=1;
			}
	if (vs[t]) printf("Yes\n");
	else printf("No\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	readi(tst);
	while (tst--){
		_init();
		_solve();
	}
	return 0;
}

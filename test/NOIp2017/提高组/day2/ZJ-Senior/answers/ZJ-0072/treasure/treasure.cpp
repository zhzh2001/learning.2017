#include<cstdio>
#include<cstring>
using namespace std;
int n,e,tot,k,son[2005],nxt[2005],lnk[20],f[20],dst[20],que[20],ans,INF,sum;
bool vs[20];
inline char nc(){
	static char buf[100000],*pa=buf,*pb=buf;
	return pa==pb&&(pb=(pa=buf)+fread(buf,1,100000,stdin),pa==pb)?EOF:*pa++;
}
inline void readi(int &x){
	x=0; char ch=nc();
	while ('0'>ch||ch>'9') ch=nc();
	while ('0'<=ch&&ch<='9') {x=x*10+ch-'0'; ch=nc();}
}
void _add(int x,int y){
	son[++tot]=y; nxt[tot]=lnk[x]; lnk[x]=tot;
}
void _init(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	readi(n); readi(e); tot=0;
	memset(lnk,0,sizeof(lnk));
	memset(nxt,0,sizeof(nxt));
	for (int i=1,x,y,z;i<=e;i++){
		readi(x); readi(y); readi(z); k=z; _add(x,y); _add(y,x);
	}
}
void _work(int s){
	memset(vs,1,sizeof(vs));
	int hed=0,til=1; dst[s]=vs[s]=sum=0; que[1]=s;
	while (hed!=til){
		for (int j=lnk[que[++hed]];j;j=nxt[j])
			if (vs[son[j]]){
				que[++til]=son[j]; dst[son[j]]=dst[que[hed]]+1; sum+=dst[son[j]]*k; vs[son[j]]=0;
			}
	}
	if (ans>sum) ans=sum;
}
void _solve(){
	ans=1<<30;
	for (int i=1;i<=n;i++) _work(i);
	printf("%d",ans);
}
int main()
{
	_init();
	_solve();
	return 0;
}

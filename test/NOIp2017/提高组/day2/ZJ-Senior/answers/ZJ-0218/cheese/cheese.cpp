#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define N 1005
#define LL long long
#define wyxsb 1e-10
using namespace std;

inline LL readl(){
	LL a=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}
	return a*f;
}

inline int read(){
	int a=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}
	return a*f;
}

struct nod{LL x,y,z;}p[N+5];
struct edge{int to,nxt;}e[N*N];
int n,s,t,cnt,head[N+5],vis[N+5];
LL h,r;

inline void add(int x,int y){
	e[++cnt]=(edge){y,head[x]};
	head[x]=cnt;
}

inline double po(LL x){return 1.0*x*x;}

inline double calc(nod a,nod b){
	return 1.0*sqrt(po(a.x-b.x)+po(a.y-b.y)+po(a.z-b.z));
}

void dfs(int x){
	vis[x]=1;
	for(int i=head[x];i;i=e[i].nxt)
		if(!vis[e[i].to]) dfs(e[i].to);
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int tst=read();
	while(tst--){
		n=read(),h=readl(),r=readl();
		s=0,t=n+1;						//别忘了初始化 
		cnt=0;memset(head,0,sizeof(head));
		for(int i=1;i<=n;++i){
			p[i].x=readl(),p[i].y=readl(),p[i].z=readl();
			if(p[i].z<=r) add(s,i);		//进入 
			if(p[i].z>=h-r) add(i,t);	//出去 
		}
		for(int i=1;i<=n;++i)
			for(int j=i+1;j<=n;++j){
				if(calc(p[i],p[j])<=2.0*r)
					add(i,j),add(j,i);
			}
		memset(vis,0,sizeof(vis));
		dfs(s);
		if(vis[t]==1) puts("Yes");
		else puts("No");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

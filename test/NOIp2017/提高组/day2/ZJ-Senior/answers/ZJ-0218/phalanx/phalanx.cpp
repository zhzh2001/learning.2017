#include<cstdio>
#include<algorithm>
#include<cmath>
#define LL long long
#define N 300000
using namespace std;

inline int read(){
	int a=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}
	return a*f;
}

struct lk{
	int l,r;
}b[N+5];
int n,m,q,p[1005][1005],bl[N+5];

inline void ot(){fclose(stdin);fclose(stdout);}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if(n<=1000&&m<=1000&&q<=500){		//force 
		for(int i=1;i<=n;++i)
			for(int j=1;j<=m;++j)
				p[i][j]=(i-1)*m+j;
		while(q--){
			int x=read(),y=read(),tmp;
			printf("%d\n",tmp=p[x][y]);
			for(int j=y;j<=m;++j) p[x][j]=p[x][j+1];
			for(int i=x;i<=n;++i) p[i][m]=p[i+1][m];
			p[n][m]=tmp;
		}
		ot();
		return 0;
	}
	else{
		n=m;
		int siz=sqrt(n);
		for(int i=1;i<=n;++i){
			b[i]=(lk){i-1,i+1};
			if(i-siz*bl[0]==1) bl[++bl[0]]=i;
		}
		int R=n;
		while(q--){
			int x=read();x=read();
//			for(int i=bl[1];i;i=b[i].r) printf("%d ",b[i].id);
//			putchar('\n');
			int a=(x-1)/siz+1;
			if(siz==1) a=x;int tmp=bl[a];
			for(int i=(a-1)*siz+1;i<x;++i)
				tmp=b[tmp].r;
			printf("%d\n",tmp);
			if(b[tmp].r!=n+1){
				b[b[tmp].l].r=b[tmp].r;
				b[b[tmp].r].l=b[tmp].l;
				b[tmp].l=R;b[R].r=tmp;
				if(tmp!=bl[a]) ++a;
				for(int i=a;i<=bl[0];++i)
					bl[i]=b[bl[i]].r;
				b[tmp].r=n+1,R=tmp;
			}
		}
		ot();
		return 0;
	}
}

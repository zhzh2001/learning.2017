#include<cstdio>
#include<algorithm>
#define N 12
#define inf 2e8
using namespace std;

inline int read(){
	int a=0,f=1;char c=getchar();
	while(c<'0'||c>'9'){if(c=='-')f=-1;c=getchar();}
	while(c>='0'&&c<='9'){a=a*10+c-'0';c=getchar();}
	return a*f;
}

int n,m,ans,map[N+5][N+5],len[N+5];

void dfs(int cst,int num){
	if(cst>=ans) return;
	if(num==n){
		ans=min(ans,cst);
		return;
	}
	for(int i=1;i<=n;++i)
		if(!len[i])
			for(int j=1;j<=n;++j)
				if(len[j]&&map[i][j]!=inf){
					len[i]=len[j]+1;
					dfs(cst+len[j]*map[i][j],num+1);
					len[i]=0;
				}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;++i) for(int j=1;j<=n;++j) map[i][j]=inf;
	for(int i=1;i<=m;++i){
		int x=read(),y=read(),z=read();
		map[x][y]=map[y][x]=min(map[x][y],z);
	}
	ans=inf;
	for(int i=1;i<=n;++i)
		len[i]=1,dfs(0,1),len[i]=0;
	printf("%d\n",ans);
	fclose(stdin),fclose(stdout);
	return 0;
}

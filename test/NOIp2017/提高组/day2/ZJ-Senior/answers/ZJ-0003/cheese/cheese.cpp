#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 2005
#define M 3000005
#define LL long long
using namespace std;
int T,n,cnt,st[N],to[M],ne[M];
LL x[N],y[N],z[N],r,h;
bool f[N];
int read(){
	char ch=getchar();int s=0,f=1;
	while(ch>'9'||ch<'0'){if(ch=='-') f=-f;ch=getchar();}
	while(ch<='9'&&ch>='0'){s=s*10+ch-'0';ch=getchar();}
	return s*f;
}
void add(int x,int y){to[++cnt]=y;ne[cnt]=st[x];st[x]=cnt;}
int dis(int x,int y,int z,int X,int Y,int Z){
	return (x-X)*(x-X)+(y-Y)*(y-Y)+(z-Z)*(z-Z);
}
void dfs(int x){
	f[x]=true;
	if(f[n+1]) return;
	for(int i=st[x];i;i=ne[i]) 
	if(!f[to[i]]) dfs(to[i]);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--){
		n=read();h=read();r=read();
		for(int i=1;i<=n;i++){x[i]=read();y[i]=read();z[i]=read();}
		for(int i=0;i<=n+1;i++) st[i]=0;cnt=0;
		for(int i=1;i<=n;i++) if(abs(z[i])<=r) add(0,i);
		for(int i=1;i<=n;i++) if(abs(h-z[i])<=r) add(i,n+1);
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
			if(dis(x[i],y[i],z[i],x[j],y[j],z[j])<=4*r*r){add(i,j);add(j,i);}
		for(int i=0;i<=n+1;i++) f[i]=false;
		dfs(0);
		if(f[n+1]) printf("Yes\n");else printf("No\n");
	}
}

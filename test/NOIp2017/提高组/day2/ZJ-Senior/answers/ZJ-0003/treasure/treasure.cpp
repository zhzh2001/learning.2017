#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#define N 30
#define M 10005
using namespace std;
int d[N][N],fa[M][2],to[M],ne[M],st[M],v[M],To[M],Ne[M],St[M],V[M],cnt,Cnt,sum,ans,n,m,x,y,z;
bool f[M];
void add(int x,int y,int z){to[++cnt]=y;ne[cnt]=st[x];st[x]=cnt;v[cnt]=z;}
void Add(int x,int y,int z){To[++Cnt]=y;Ne[Cnt]=St[x];St[x]=Cnt;V[Cnt]=z;}
void Dfs(int x,int y){
	f[x]=true;
	//printf("%d\n",x);
	for(int i=St[x];i;i=Ne[i]){
		sum=sum+V[i]*y;Dfs(To[i],y+1);
	}
}
void work(int x){
	//for(int i=1;i<=n;i++) printf("%d ",fa[i][0]);printf("\n");
	for(int i=1;i<=n;i++) St[i]=0;Cnt=0;
	for(int i=1;i<=n;i++)
	if(i!=x) Add(fa[i][0],i,fa[i][1]);
	for(int i=1;i<=n;i++) f[i]=false;sum=0;
	Dfs(x,1);
	for(int i=1;i<=n;i++)
	if(!f[i]) return;
	//printf("ok %d\n",sum);
	ans=min(ans,sum);
}
void dfs(int x,int y){
	if(y==x){dfs(x,y+1);return;}
	if(y>n){work(x);return;}
	for(int i=st[y];i;i=ne[i])
	if(fa[to[i]][0]!=y){fa[y][0]=to[i];fa[y][1]=v[i];dfs(x,y+1);fa[y][0]=0;}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	ans=2000000000;
	scanf("%d%d",&n,&m);
	memset(d,-1,sizeof(d));
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if(d[x][y]==-1||(z<d[x][y])) d[x][y]=d[y][x]=z; 
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		if(i!=j&&d[i][j]!=-1) {add(i,j,d[i][j]);add(j,i,d[i][j]);}
	for(int i=1;i<=n;i++){
		memset(fa,0,sizeof(fa));
		dfs(i,1);
	}
	printf("%d\n",ans);
}

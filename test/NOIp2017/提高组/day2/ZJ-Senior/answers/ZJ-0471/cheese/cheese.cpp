#include <cmath>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
using namespace std;
const int N=1005;
struct node
{
	long long x,y,z;
}z[N];
long long n,t,h,r,pd,ans[N],flag[N],dui[N],head,tail;
long double dist(long long a,long long b)
{
	if ((fabs(z[a].x-z[b].x)>r*2) || (fabs(z[a].y-z[b].y)>r*2) || (fabs(z[a].z-z[b].z)>r*2)) return r*2+1;
	long double p=(sqrt( (z[a].x-z[b].x)*(z[a].x-z[b].x) +(z[a].y-z[b].y)*(z[a].y-z[b].y) +(z[a].z-z[b].z)*(z[a].z-z[b].z) ) );
    return p;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&t);
	while (t--)
	{
		scanf("%lld%lld%lld",&n,&h,&r);
		memset(flag,0,sizeof(flag));
		pd=0;
		memset(ans,0,sizeof(ans));
		head=1;tail=0;
		memset(dui,0,sizeof(flag));
		for (int i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&z[i].x,&z[i].y,&z[i].z);
			if (z[i].z<=r) 
			{
				tail++;
				flag[i]=1;
				dui[tail]=i;
			}
			if (z[i].z+r>=h)
			{
				ans[i]=1;
			}
		}
		while (head<=tail)
		{
			int x=dui[head];
			for (int i=1;i<=n;i++)
			{
				if (flag[i]==0&&(dist(i,x)-r*2<=(1e-7)))
				{
					flag[i]=1;
					tail++;
					dui[tail]=i;
				}
			}
			head++;
		}
		for (int i=1;i<=n;i++)
		{
			if (flag[i] && ans[i])
			{
				pd=1;
			}
		}
		if (pd==1) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include <cmath>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
using namespace std;
int n,m,dis[1005],flag[1005],sum,f[105][105],ans,a,b,c;
void dfs(int sum,int num)
{
	if (num==n)
	{
		ans=min(ans,sum);
	}
	if (sum>ans) return;
	for (int i=1;i<=n;i++)
	{
		if (flag[i]==0)
		{
			for (int j=1;j<=n;j++)
			{
				if (flag[j]==1 && f[i][j]!=10000000)
				{
					dis[i]=dis[j]+1;
					flag[i]=1;
					dfs(sum+dis[j]*f[i][j],num+1);
					flag[i]=0;
					dis[i]=0;
				}
			}
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=n;j++)
		{
			f[i][j]=10000000;
		}
	}
	for (int i=1;i<=n;i++) f[i][i]=0;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&a,&b,&c);
		f[a][b]=min(f[a][b],c);
		f[b][a]=f[a][b];
	}
	ans=1000000000;
	for (int i=1;i<=n;i++)
	{
		dis[i]=1;
		flag[i]=1;
		dfs(0,1);
		dis[i]=0;
		flag[i]=0;
	}
	printf("%d",ans);
	return 0;
}

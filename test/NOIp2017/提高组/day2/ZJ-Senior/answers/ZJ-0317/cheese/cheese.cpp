#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1005;
const ll INF=4e18;
int T,n,H,R,X[M],Y[M],Z[M];
int Q[M],mark[M];
ll pf(int x){return 1ll*x*x;}
ll dis_(int i,int j){
	return min(INF,pf(X[i]-X[j])+pf(Y[i]-Y[j]))+pf(Z[i]-Z[j]);
}
void solve(){
	memset(mark,0,sizeof(mark));
	rd(n),rd(H),rd(R);
	for(int i=1;i<=n;i++)rd(X[i]),rd(Y[i]),rd(Z[i]);
	ll RR=4ll*R*R;
	int l=1,r=0,ans=0;
	for(int i=1;i<=n;i++)
		if(Z[i]<=R)Q[++r]=i,mark[i]=1;
	while(l<=r){
		int x=Q[l++];
		if(Z[x]+R>=H){ans=1;break;}
		for(int i=1;i<=n;i++)
			if(!mark[i]&&dis_(x,i)<=RR)
				Q[++r]=i,mark[i]=1;
	}
	if(ans)puts("Yes");
	else puts("No");
}
int main(){
	srand(time(NULL));
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	rd(T);
	while(T--)solve();
	return 0;
}

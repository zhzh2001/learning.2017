#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=13;
int n,m,A[M][M];
namespace P70{
	const int M=9;
	int Bas[M],dp[9][6605],cost[M];
	int get(int x){
		int res=0;
		for(int i=0;i<n;i++)
			if(x/Bas[i]%3)res|=1<<i;
		return res;
	}
	int reget(int x){
		for(int i=0;i<n;i++)
			if(x/Bas[i]%3==1)x+=Bas[i];
		return x;
	}
	void solve(){
		memset(A,63,sizeof(A));
		memset(dp,63,sizeof(dp));
		Bas[0]=1;
		for(int i=1;i<=n;i++)Bas[i]=Bas[i-1]*3;
		while(m--){
			int a,b,v;
			rd(a),rd(b),rd(v);
			MIN(A[a-1][b-1],v);
			MIN(A[b-1][a-1],v);
		}
		int nn=(1<<n)-1;
		for(int i=0;i<n;i++)dp[1][Bas[i]]=0;
		for(int d=1;d<n;d++){
			for(int i=1;i<Bas[n];i++){
				if(dp[d][i]>1e9)continue;
				int s=nn-get(i),ii=reget(i);
				MIN(dp[d+1][ii],dp[d][i]);
				memset(cost,63,sizeof(cost));
				for(int x=0;x<n;x++)if(i/Bas[x]%3==1)
					for(int y=0;y<n;y++)if(A[x][y]<1e9)
						MIN(cost[y],d*A[x][y]);
				for(int j=s;j;j=(j-1)&s){
					int v=dp[d][i],t=ii,yes=1;
					for(int y=0;y<n;y++)
						if((j>>y)&1){
							if(cost[y]>=1e9){yes=0;break;}
							v+=cost[y];
							t+=Bas[y];
						}
					if(yes)MIN(dp[d+1][t],v);
				}
			}
		}
		int ans=1e9;
		for(int d=1;d<=n;d++)MIN(ans,dp[d][Bas[n]-1]);
		printf("%d\n",ans);
	}
}
namespace P100{
	const int N=550005;
	int Bas[M],Mod3[N],dp[M][N];
	int B[N],C[N],cost[N][M],T[M];
	int get(int x){
		int res=0;
		for(int i=0;i<n;i++)
			if(Mod3[x/Bas[i]])res|=1<<i;
		return res;
	}
	int reget(int x){
		for(int i=0;i<n;i++)
			if(Mod3[x/Bas[i]]==1)x+=Bas[i];
		return x;
	}
	void solve(){
		memset(A,63,sizeof(A));
		memset(dp,63,sizeof(dp));
		memset(cost,63,sizeof(cost));
		Bas[0]=1;
		for(int i=1;i<=n;i++)Bas[i]=Bas[i-1]*3;
		for(int i=1;i<Bas[n];i++)Mod3[i]=i%3;
		while(m--){
			int a,b,v;
			rd(a),rd(b),rd(v);
			MIN(A[a-1][b-1],v);
			MIN(A[b-1][a-1],v);
		}
		int nn=(1<<n)-1;
		for(int i=0;i<n;i++)dp[1][Bas[i]]=0;
		for(int i=1;i<Bas[n];i++)
			B[i]=get(i),C[i]=reget(i);
		for(int i=1;i<Bas[n];i++)
			for(int x=0;x<n;x++)if(Mod3[i/Bas[x]]==1)
				for(int y=0;y<n;y++)if(A[x][y]<1e9)
					MIN(cost[i][y],A[x][y]);
		for(int d=1;d<n;d++){
			for(int i=1;i<Bas[n];i++){
				if(dp[d][i]>1e9)continue;
				int s=nn-B[i],ii=C[i],sz=0;
				MIN(dp[d+1][ii],dp[d][i]);
				for(int y=0;y<n;y++)
					if(!Mod3[i/Bas[y]])T[++sz]=y;
				for(int j=s;j;j=(j-1)&s){
					int v=dp[d][i],t=ii,yes=1;
					for(int k=1;k<=sz;k++){
						int y=T[k];
						if((j>>y)&1){
							int res=cost[i][y];
							if(res>=1e9){yes=0;break;}
							v+=d*res;
							t+=Bas[y];
						}
					}
					if(yes)MIN(dp[d+1][t],v);
				}
			}
		}
		int ans=1e9;
		for(int d=1;d<=n;d++)MIN(ans,dp[d][Bas[n]-1]);
		printf("%d\n",ans);
	}
}
int main(){
	srand(time(NULL));
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	rd(n),rd(m);
	if(0);
	else if(n<=8)P70::solve();
	else P100::solve();
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
const int P=32000;
inline int Rand(int x){
	return (rand()%P*P+rand()%P)%x+1;
}
int n,m,V;

int main(){
	srand(time(NULL));
	freopen("treasure.in","w",stdout);
	n=8,m=30,V=10;
	printf("%d %d\n",n,m);
	for(int i=2;i<=n;i++)
		printf("%d %d %d\n",Rand(i-1),i,Rand(V));
	for(int i=n;i<=m;i++)
		printf("%d %d %d\n",Rand(n),Rand(n),Rand(V));
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=9;
int n,m,A[M][M],ans=1e9;
int deep[M];
void dfs(int x,int v){
	if(x==n){
		MIN(ans,v);
		return;
	}
	for(int i=1;i<=n;i++)if(deep[i])
		for(int j=1;j<=n;j++)if(!deep[j]&&A[i][j]<1e9){
			deep[j]=deep[i]+1;
			dfs(x+1,v+deep[i]*A[i][j]);
			deep[j]=0;
		}
	
}
int main(){
	srand(time(NULL));
	freopen("treasure.in","r",stdin);
	freopen("std.out","w",stdout);
	memset(A,63,sizeof(A));
	rd(n),rd(m);
	while(m--){
		int a,b,v;
		rd(a),rd(b),rd(v);
		MIN(A[a][b],v);
		MIN(A[b][a],v);
	}
	for(int i=1;i<=n;i++){
		deep[i]=1;
		dfs(1,0);
		deep[i]=0;
	}
	printf("%d\n",ans);
	return 0;
}

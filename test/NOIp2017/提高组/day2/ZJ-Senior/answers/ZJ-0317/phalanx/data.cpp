#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
const int P=32000;
inline int Rand(int x){
	return (rand()%P*P+rand()%P)%x+1;
}
int n,m,q;

int main(){
	srand(time(NULL));
	freopen("phalanx.in","w",stdout);
	n=Rand(10),m=Rand(1001),q=Rand(100);
	printf("%d %d %d\n",n,m,q);
	while(q--){
		int x=1,y=Rand(m);
		printf("%d %d\n",x,y);
	}
	return 0;
}

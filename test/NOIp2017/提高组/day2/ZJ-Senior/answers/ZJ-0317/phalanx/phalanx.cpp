#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=3e5+5;
int n,m,q,X[M],Y[M];

namespace P30{
	const int M=1005;
	int A[M][M];
	void solve(){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(i-1)*m+j;
		for(int i=1;i<=q;i++){
			int x=X[i],y=Y[i],id=A[x][y];
			for(int j=y;j<m;j++)A[x][j]=A[x][j+1];
			for(int j=x;j<n;j++)A[j][m]=A[j+1][m];
			A[n][m]=id;
			printf("%d\n",id);
		}
	}
}
namespace P60{
	const int N=9e5+5;
	int nm,nmq,cnt[N],ID[N];
	void add(int x,int v){
		while(x<=nmq){
			cnt[x]+=v;
			x+=x&-x;
		}
	}
	int query(int x){
		int res=0;
		while(x){
			res+=cnt[x];
			x-=x&-x;
		}
		return res;
	}
	void solve(){
		nm=n+m-1,nmq=n+m+q;
		for(int i=1;i<=m;i++)ID[i]=i;
		for(int i=2;i<=n;i++)ID[m+i-1]=i*m;
		for(int i=1;i<=nmq;i++)add(i,1);
		for(int i=1;i<=q;i++){
			int p=nm+i,y=Y[i];
			int l=y,r=p,ans=0;
			while(l<=r){
				int mid=l+r>>1;
				if(query(mid)>=y)r=mid-1,ans=mid;
				else l=mid+1;
			}
			add(ans,-1);
			ID[p]=ID[ans];
			printf("%d\n",ID[p]);
		}
	}
}
int main(){
	srand(time(NULL));
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int all_x1=1;
	rd(n),rd(m),rd(q);
	for(int i=1;i<=q;i++){
		rd(X[i]),rd(Y[i]);
		if(X[i]!=1)all_x1=0;
	}
	if(0);
	else if(n<=1000&&m<=1000)P30::solve();
	else if(all_x1)P60::solve();
	return 0;
}

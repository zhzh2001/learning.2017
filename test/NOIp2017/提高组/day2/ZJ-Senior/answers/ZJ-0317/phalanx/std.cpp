#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &a,T b){if(b>a)a=b;}
template<class T>inline void MIN(T &a,T b){if(b<a)a=b;}
inline void rd(int &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o=='-')f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=3e5+5;
int n,m,q,X[M],Y[M];
namespace P30{
	const int M=1005;
	int A[M][M];
	void solve(){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(i-1)*m+j;
		for(int i=1;i<=q;i++){
			int x=X[i],y=Y[i],id=A[x][y];
			for(int j=y;j<m;j++)A[x][j]=A[x][j+1];
			for(int j=x;j<n;j++)A[j][m]=A[j+1][m];
			A[n][m]=id;
			printf("%d\n",id);
		}
	}
}
int main(){
	srand(time(NULL));
	freopen("phalanx.in","r",stdin);
	freopen("std.out","w",stdout);
	int all_x1=1;
	rd(n),rd(m),rd(q);
	for(int i=1;i<=q;i++){
		rd(X[i]),rd(Y[i]);
		if(X[i]!=1)all_x1=0;
	}
	P30::solve();
	return 0;
}

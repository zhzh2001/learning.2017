var
  l:array[0..10005,0..2] of longint;
  vis:array[0..10005] of boolean;
  a:array[0..30,0..30 ] of longint;
  b,c,i,n,j,k,m,sum,ans,tot,x,y,z:longint;
begin
  assign(input,'treasure.in');
  reset(input);
  assign(output,'treasure.out');
  rewrite(output);
  readln(n,m);
  for i:=1  to m do
    begin
      readln(x,y,z);
      a[x,y]:=z;
      a[y,x]:=z;
    end;
   ans:=maxlongint;
   for i:=1 to n  do
     begin
       fillchar(l,sizeof(l),0);
       fillchar(vis,sizeof(vis),false);
       x:=0;
       y:=1;
       tot:=0;
       vis[i]:=true;
       l[y,1]:=i;
       l[y,2]:=1;
       while x<y do
         begin
           inc(x);
           k:=l[x,1];
           for j:=1 to n do
             if (not vis[j])and(a[k,j]<>0)
               then begin
                      tot:=tot+a[k,j]*l[x,2];
                      vis[j]:=true;
                      inc(y);
                      l[y,1]:=j;
                      l[y,2]:=l[x,2]+1;
                    end;
         end;
       if tot<ans then ans:=tot;
     end;
     writeln(ans);
    close(input);
    close(output);
end.

var
  f:array[0..1005,0..1005] of boolean;
  min,l:array[0..2005] of longint;
  a,b,c:array[0..2005] of int64;
  pd:array[0..2005] of boolean;
  i,n,j,k,m,w,x,y,z:longint;
  dis,h,r,sumr:int64;
begin
  assign(input,'cheese.in');
  reset(input);
  assign(output,'cheese.out');
  rewrite(output);
  readln(m);
  for w:=1 to m do
    begin
      fillchar(a,sizeof(a),0);
      fillchar(b,sizeof(b),0);
      fillchar(c,sizeof(c),0);
      fillchar(f,sizeof(f),false);
      fillchar(min,sizeof(min),$3f);
      fillchar(pd,sizeof(pd),false);
      readln(n,h,r);
      for i:=1 to n do
       readln(a[i],b[i],c[i]);
      for i:=1 to n-1 do
       for j:=i+1 to n do
         begin
          dis:=(a[i]-a[j])*(a[i]-a[j])+(b[i]-b[j])*(b[i]-b[j])+(c[i]-c[j])*(c[i]-c[j]);
          sumr:=2*r*2*r;
          if dis<=sumr then begin f[i,j]:=true; f[j,i]:=true; end;
         end;

      for i:=1 to n do
        begin
          if c[i]<=r then begin f[0,i]:=true; f[i,0]:=true;  end;
          if h-c[i]<=r then begin f[n+1,i]:=true; f[i,n+1]:=true; end;
        end;
      x:=0;
      y:=0;
      inc(y);
      min[0]:=0;
      pd[0]:=true;
      l[y]:=0;

      while x<y do
        begin
          inc(x);
          k:=l[x];
          pd[k]:=false;
          for i:=0 to n+1 do
            if  (f[k,i]=true)and(min[i]<>0)
              then begin
                     min[i]:=0;
                     if not pd[i] then
                           begin
                            pd[i]:=true;
                            inc(y);
                            l[y]:=i;
                           end;
                   end;
        end;
        if min[n+1]=0 then writeln('Yes')
        else writeln('No');
    end;
  close(input);
  close(output);
end.

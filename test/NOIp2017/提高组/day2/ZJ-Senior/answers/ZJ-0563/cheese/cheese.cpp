#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
typedef long double lb;
const lb eps=0.0000000001;
using namespace std;
struct node {
	int x,y,z;
} a[1005];
int head[1005],vet[2000005],next[2000005],num;
bool f[1005];
int n,h,r;
inline lb ab(lb x) {
	if (x<0) return -x;
	return x;
}
inline lb sqr(lb x) {
	return x*x;
}
inline void addedge(int x,int y) {
	num++;vet[num]=y;next[num]=head[x];head[x]=num;
	num++;vet[num]=x;next[num]=head[y];head[y]=num;
}
inline int read() {
	int sum=0;char ch=0;bool fu=false;
	while ((ch<'0' || ch>'9')&& ch!='-') ch=getchar();
	if (ch=='-') {fu=true;ch=getchar();}
	while (ch>='0' && ch<='9') sum=sum*10+ch-'0',ch=getchar();
	if (fu) return -sum;
	return sum;
}
bool dfs(int x) {
	if (x==n+1) return true;
	f[x]=true;
	for (int e=head[x];e!=0;e=next[e]) {
		int v=vet[e];
		if (!f[v]) {
			if (dfs(v)) return true;
		}
	}
	return false;
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	lb tmp,R;
	while (T--) {
		n=read();h=read();r=read();
		num=0;head[0]=head[n+1]=0;
		f[0]=f[n+1]=false;
		for (int i=1;i<=n;i++) {
			head[i]=0;f[i]=false;
			a[i].x=read();
			a[i].y=read();
			a[i].z=read();
		}
		for (int i=1;i<=n;i++) {
			if (a[i].z<=r && a[i].z+r>=0) addedge(0,i);
			if (a[i].z-r<=h && a[i].z+r>=h) addedge(n+1,i);
		}
		R=eps+(r<<1);
		for (int i=1;i<n;i++) 
			for (int j=i+1;j<=n;j++) {
				tmp=sqrt(sqr((lb)a[i].x-a[j].x)+sqr((lb)a[i].y-a[j].y)+sqr((lb)a[i].z-a[j].z));
				if (tmp<=R) addedge(i,j);
			}
		if (dfs(0)) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

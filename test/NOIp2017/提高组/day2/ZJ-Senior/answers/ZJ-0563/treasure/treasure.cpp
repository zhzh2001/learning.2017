#include<iostream>
#include<cstdio>
using namespace std;
typedef long long LL;
LL f[1<<13][15],co[15];
int vet[15][15];
int p0[15],p1[15];
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) vet[i][j]=1e9;
	for (int i=1;i<=m;i++) {
		int x,y,c;
		scanf("%d%d%d",&x,&y,&c);
		vet[x][y]=vet[y][x]=min(vet[x][y],c);
	}
	int maxstat=1<<n;
	for (int i=0;i<n;i++)
		for (int stat=0;stat<maxstat;stat++) f[stat][i]=1e9;
	for (int i=0;i<n;i++) f[1<<i][0]=0;
	for (int i=1;i<n;i++) 
		for (int stat=1;stat<maxstat;stat++) {
			p0[0]=p1[0]=0;
			for (int j=0;j<n;j++) {
				if (stat&(1<<j)) p1[++p1[0]]=j+1;
				else p0[++p0[0]]=j+1;
			}
			for (int j=1;j<=p0[0];j++) {
				co[j]=1e9;
				for (int k=1;k<=p1[0];k++) {
					int c=vet[p0[j]][p1[k]];
					if (c==1e9) continue;
					co[j]=min(co[j],(LL)c*i);
				}
			}
			int maxstt=1<<p0[0];
			for (int stt=0;stt<maxstt;stt++) {
				int newstat=stat;
				LL c=0;
				for (int j=0;j<p0[0];j++)
					if (stt&(1<<j)) {
						newstat|=1<<(p0[j+1]-1);
						c+=co[j+1];
					}
				f[newstat][i]=min(f[newstat][i],f[stat][i-1]+c);
			}
		}
	printf("%lld",f[(1<<n)-1][n-1]);
	return 0;
}

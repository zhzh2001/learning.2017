#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <set>
#include <map>

using namespace std;

typedef unsigned long long ull;
typedef long long ll;

int F[1000010];

ull n, h, r, mxds2, X[1010], Y[1010], Z[1010];

int getf(int x) {
	return F[x] == x ? x : F[x] = getf(F[x]);
}

int Check(int a, int b)
{
	return (X[a] - X[b]) * (X[a] - X[b]) + (Y[a] - Y[b]) * (Y[a] - Y[b]) + (Z[a] - Z[b]) * (Z[a] - Z[b]) <= mxds2;
}

int Solve()
{
	int _1, _2, _3;
	scanf("%d%d%d", &_1, &_2, &_3);
	n = _1;
	h = _2;
	r = _3;
	mxds2 = 4ull * r * r;
	for(int i = 1; i <= n; i++) {
		int _4, _5, _6;
		scanf("%d%d%d", &_4, &_5, &_6);
		X[i] = _4;
		Y[i] = _5;
		Z[i] = _6;
	}
	for(int i = 0; i <= n + 1; i++) F[i] = i;
	for(int i = 1; i <= n; i++)
		for(int j = i + 1; j <= n; j++)
			if(getf(i) != getf(j) && Check(i, j))
				F[getf(i)] = getf(j);
	for(int i = 1; i <= n; i++)
		if(Z[i] <= r && getf(0) != getf(i)) F[getf(0)] = getf(i);
	for(int i = 1; i <= n; i++)
		if(Z[i] + r >= h && getf(n + 1) != getf(i)) F[getf(n + 1)] = getf(i);
	if(getf(0) == getf(n + 1)) return 1;
	return 0;
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int Cases;
	scanf("%d", &Cases);
	while(Cases--) {
		if(Solve()) puts("Yes"); else puts("No");
	}
	return 0;
}

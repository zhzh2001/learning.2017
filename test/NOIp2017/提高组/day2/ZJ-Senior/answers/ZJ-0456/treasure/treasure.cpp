#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <set>
#include <map>
#define Inf 1000000000

using namespace std;

int n, m, Len[13][13], F[1 << 12][13], G[1 << 12][1 << 12], Sz[1 << 12], LB[1 << 12], Lg[1 << 12];

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			Len[i][j] = Inf;
	for(int i = 0; i < n; i++) Lg[1 << i] = i;
	for(int i = 1; i <= m; i++) {
		int a, b, c;
		scanf("%d%d%d", &a, &b, &c);
		a--;
		b--;
		Len[a][b] = min(Len[a][b], c);
		Len[b][a] = min(Len[b][a], c);
	}
	memset(G, 63, sizeof G);
	for(int i = 1; i < (1 << 12); i++) Sz[i] = Sz[i - (i & (-i))] + 1;
	for(int i = 1; i < (1 << n); i++) {
		G[i][0] = 0;
		for(int j = 1; j < (1 << n); j++) if((((1 << n) - 1 - i) & j) == j) {
			int nw = Lg[j & (-j)];
			for(int k = 0; k < n; k++)
				if((i >> k) & 1)
					G[i][j] = min(G[i][j], Len[k][nw] + G[i][j - (1 << nw)]);
		}
	}
	memset(F, 63, sizeof F);
	for(int i = 0; i < n; i++)
		F[1 << i][0] = 0;
	for(int i = 1; i < (1 << n) - 1; i++)
		for(int dep = 0; dep < n; dep++) if(F[i][dep] < Inf)
			for(int tmp = (1 << n) - 1 - i, j = tmp; j; j = (j - 1) & tmp) if(G[i][j] < Inf)
				F[i + j][dep + 1] = min(F[i + j][dep + 1], F[i][dep] + G[i][j] * (dep + 1));
	int ans = Inf;
	for(int i = 0; i < n; i++)
		ans = min(ans, F[(1 << n) - 1][i]);
	printf("%d\n", ans);
	return 0;
}

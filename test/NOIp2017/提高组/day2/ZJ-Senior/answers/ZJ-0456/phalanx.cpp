#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <set>
#include <map>
#include <cassert>

#define Inf 1000000000
#define its set <int>::iterator
#define M 25000010 //???

using namespace std;

typedef long long ll;

const int Lim = 600010; //???

const int SegM = 600010;

int n, m, q, SegSz[M], Lf[M], Ri[M], SegCnt = 0;

struct SEG {
	vector <ll> Num;
	int Rt;
	void init()
	{
		Rt = 0;
		Num.clear();
	}
	
	void add(int &x, int l, int r, int w, int s)
	{
		if(x == 0) {
			SegCnt++;
			x = SegCnt;
			SegSz[x] = 0;
		}
		SegSz[x] += s;
		if(l == r) return;
		if(w <= (l + r) / 2) add(Lf[x], l, (l + r) / 2, w, s);
		else add(Ri[x], (l + r) / 2 + 1, r, w, s);
	}
	
	int getsz(int x, int l, int r, int w)
	{
		if(x == 0) return 0;
		if(l == r) return SegSz[x];
		if(w < (l + r) / 2) return getsz(Lf[x], l, (l + r) / 2, w);
		return SegSz[Lf[x]] + getsz(Ri[x], (l + r) / 2 + 1, r, w);
	}
	
	int findwsz(int x, int l, int r, int sz)
	{
		if(l == r) return l;
		if(SegSz[Lf[x]] >= sz) return findwsz(Lf[x], l, (l + r) / 2, sz);
		return findwsz(Ri[x], (l + r) / 2 + 1, r, sz - SegSz[Lf[x]]);
	}
	
	void append(ll nw)
	{
		Num.push_back(nw);
		add(Rt, 1, Lim, Num.size(), 1);
	}
	
	void addw(int w)
	{
		add(Rt, 1, Lim, w, 1);
	}
	
	int findsz(int w)
	{
		return getsz(Rt, 1, Lim, w);
	}
	
	ll findnum(int sz)
	{
		ll tmp = findwsz(Rt, 1, Lim, sz);
		add(Rt, 1, Lim, tmp, -1);
		return Num[tmp - 1];
	}
	int size()
	{
		return SegSz[Rt];
	}
	
} Seg[SegM];
	
struct OPSL {
	void init()
	{
	}
	ll Do(int y) {
		ll ans = Seg[n * 2 + 1].findnum(y);
		//Seg[n * 2 + 1].delw(y);
		return ans;
	}
} opsl;

struct OPSH {
	int idx;
	
	void init(int _1)
	{
		idx = _1;
	}
	
	ll Do(int x) {
		if(x == m) return -1;
		ll ans;
		if(m - Seg[n + idx].size() <= x) {
			ans = Seg[n + idx].findnum(x - (m - Seg[n + idx].size()) + 1);
			//Seg[n + idx].delw(x);
		} else {
			int nw = x + Seg[idx].findsz(x); // x <= m
			ans = (ll) (idx - 1) * m + nw;
			Seg[idx].addw(x);
		}
		return ans;
	}
} opsh[300010];
	
int X[1111][1111];

void Solve()
{
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			X[i][j] = (i - 1) * m + j;
	for(int i = 1; i <= q; i++) {
		int x, y, tmp;
		scanf("%d%d", &x, &y);
		tmp = X[x][y];
		printf("%d\n", X[x][y]);
		for(int j = y; j < m; j++)
			X[x][j] = X[x][j + 1];
		for(int j = x; j < n; j++)
			X[j][m] = X[j + 1][m];
		X[n][m] = tmp;
	}
}

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if(n <= 1000 && m <= 1000) {
		Solve();
		return 0;
	}
	for(int i = 1; i <= n; i++) opsh[i].init(i);
	opsl.init();
	for(int i = 0; i < SegM; i++) Seg[i].init();
	for(int i = 1; i <= n; i++) Seg[n * 2 + 1].append((ll) i * m);
	while(q--) {
		int x, y;
		scanf("%d%d", &x, &y);
		ll ans1 = opsh[x].Do(y);
		ll ans2 = opsl.Do(x);
		if(y != m) {
			printf("%lld\n", ans1); 
			Seg[n + x].append(ans2);
			Seg[n * 2 + 1].append(ans1);
		} else {
			printf("%lld\n", ans2);
			Seg[n * 2 + 1].append(ans2);
		}
	}
	return 0;
}

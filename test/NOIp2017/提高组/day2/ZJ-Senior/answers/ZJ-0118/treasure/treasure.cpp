#include<cstdio>
#include<vector>
#include<queue>
#include<algorithm> 
using namespace std;

const int maxn = 20, maxm = 1500;

int n, m, a[maxn + 1][maxn + 1], dep[maxn + 1];

long long ans = 1e18;
int maxsta;
bool vis[maxn + 1] = {false};
void dfs(int st, long long cost){
	if (st == (maxsta)){
		ans = min(cost, ans);
		return;
	}
	for (int i = 1; i <= n; ++i) if (!vis[i]){
		vis[i] = true;
		for (int j = 1; j <= n; ++j) if ((1 << (j - 1)) & st){
			if (a[i][j] == 1e9) continue;
			dep[i] = dep[j] + 1; 
			dfs(st | (1 << (i - 1)), cost + 1LL * dep[j] * (long long)a[i][j]);
			dep[i] = 0;
		}
		vis[i] = false;
	}
	
}

vector<pair<int,int> > G[maxn + 1];
long long res = 0;

struct node{int x, y, z;}question[maxm + 1];

void bfs(int x){
	queue<int> Q; 
	while (!Q.empty()) Q.pop(); Q.push(x); vis[x] = true;
	dep[x] = 1;
	while (!Q.empty()){
		int now = Q.front(); Q.pop();
		for (unsigned i = 0; i < G[now].size(); ++i){
			int v = G[now][i].first; if (vis[v]) continue;
			res += dep[now]; dep[v] = dep[now] + 1;
			Q.push(v); vis[v] = true; 
		}
	}
}

void subwork40(){
	for (int i = 1; i <= m; ++i){
		int u = question[i].x, v = question[i].y, w = question[i].z;
		if(u == v) continue;
		G[u].push_back(make_pair(v, w));
		G[v].push_back(make_pair(u, w));
	}
	for (int i = 1; i <= n; ++i) dep[i] = 0;
	
	for (int i = 1; i <= n; ++i){
		for (int j = 1; j <= n; ++j) vis[j] = false; 
		res = 0; dep[i] = 1;
		bfs(i);
		ans = min(ans, res);
	}
	printf("%lld\n", 1LL * ans * question[1].z);
}

int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m); maxsta = (1 << n) - 1;
	for (int i = 1; i <= m; ++i) scanf("%d%d%d", &question[i].x, &question[i].y, &question[i].z);
	bool ok = true; for (int i = 2; i <= m; ++i) if (question[i].z != question[i - 1].z) ok = false;
	if (ok){	
		subwork40();
		return 0;
	}
	for (int i = 1; i <= n; ++i) for (int j = 1; j <= n; ++j) a[i][j] = 1e9;
	for (int i = 1; i <= m; ++i){
		int u = question[i].x, v = question[i].y, w = question[i]. z; 
		a[u][v] = a[v][u] = min(a[u][v], w);
	}
	for (int i = 1; i <= n; ++i) vis[i] = false;
	for (int i = 1; i <= n; ++i) dep[i] = 0;
	for (int i = 1; i <= n; ++i){
		vis[i] = true; dep[i] = 1;
		dfs(1 << (i - 1), 0LL);
		vis[i] = false;
	} 
	printf("%lld\n", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

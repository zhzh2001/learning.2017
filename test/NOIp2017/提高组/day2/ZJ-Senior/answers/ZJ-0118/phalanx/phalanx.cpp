#include<cstdio>
#include<algorithm> 
using namespace std;

const int maxn = 1005, maxm = 300000;

int id[maxn + 1][maxn + 1], n, m, q;

void substack1(){
	for (int i = 1; i <= n; ++i) for (int j = 1; j <= m; ++j) id[i][j] = (i - 1) * m + j;
	while (q --){
		int x, y; scanf("%d%d", &x, &y); int ans = id[x][y];
		printf("%d\n", ans); 
		for (int i = y; i <= m - 1; ++i) id[x][i] = id[x][i + 1];
		for (int i = x; i <= n - 1; ++i) id[i][n] = id[i + 1][n];
		id[n][m] = ans;
	}
}

struct node{int x, y;} question[300005];
int sum[maxm + 1];

int mod;

void Add(int &x, int y){
	x += y; if (x >= mod) x -= mod; if (x < 0) x += mod;
}

int lowbit(int x){
	return x & -x;
}

void add(int pos, int val){
	for (int i = pos; i <= m; i += lowbit(i)) Add(sum[i], val);
}

int query(int pos){
	int res = 0;
	for (int i = pos; i >= 1; i -= lowbit(i)) Add(res, sum[i]);
	return res;
}


void substack2(){
	for (int i = 1; i <= m; ++i) add(i, 1);
	for (int ii = 1; ii <= q; ++ii){
		int y = question[ii].y; int ans = query(y);
		printf("%d\n", ans);
		if (y <= m - 1){
			add(y, 1);
			add(m, -1);
		}
		sum[m] = 0;
		add(m, ans - query(m - 1) + 1);
//		printf("%d %d\n", question[ii].x, question[ii].y);
//		for (int i = 1; i <= m; ++i) printf("%d ", query(i)); printf("\n");
	}
}

int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf ("%d%d%d", &n, &m, &q); mod = n + 1;
	if (n <= 1000 && m <= 1000){
		substack1();
		return 0;
	}
	for (int i = 1; i <= q; ++i) scanf("%d%d", &question[i].x, &question[i].y);
	if (n == 1){
		substack2();
		return 0;
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
/*
1 10 10
1 2
1 3
1 6
1 7
1 1
1 10
1 8
1 9
1 2
1 4
*/
/*
2
4
8
10
1
1
8
1
5
9
*/

#include<vector>
#include<cstdio>
#include<queue>
#include<iostream>
using namespace std;

const int maxn = 1005;

struct node{long long x, y, z;}a[maxn + 1];

int n;
unsigned long long h, r;
vector<int> G[maxn + 1];

unsigned long long dist(node a, node b){
	return (unsigned long long)(a.x - b.x) * (a.x - b.x) + (unsigned long long)(a.y - b.y) * (a.y - b.y) + (unsigned long long)(a.z - b.z) * (a.z - b.z);
}

bool vis[maxn + 1];
int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T --){
		scanf("%d", &n); cin >> h >> r;
		for (int i = 1; i <= n; ++i) scanf("%lld%lld%lld", &a[i].x, &a[i].y, &a[i].z);
//		cout << dist(a[1], a[2]) << endl;;
		for (int i = 0; i <= n + 1; ++i) G[i].clear();
		for (int i = 1; i <= n; ++i) if (a[i].z <= (long long)r) G[0].push_back(i), G[i].push_back(0);
		for (int i = 1; i <= n; ++i) if ((long long)h - a[i].z <= (long long)r) G[n + 1].push_back(i), G[i].push_back(n + 1);
		r = 4LL * r * r;
		for (int i = 1; i <= n; ++i) for (int j = 1; j <= n; ++j) if (i != j && dist(a[i], a[j]) <= (unsigned long long)r) 
		G[i].push_back(j), G[j].push_back(i);		
		for (int i = 0; i <= n + 1; ++i) vis[i] = false;
		queue<int> Q; while (!Q.empty()) Q.pop(); Q.push(0); vis[0] = true;
		while (!Q.empty()){
			int x = Q.front(); Q.pop();
			for (unsigned i = 0; i < G[x].size(); ++i){
				int v = G[x][i]; if (vis[v]) continue;
				vis[v] = true; Q.push(v);
			}
		}
		if (vis[n + 1]) printf("Yes\n"); else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}
/*
1
2 1 1
1000000000 1000000000 1000000000
-1000000000 -1000000000 -1000000000
*/

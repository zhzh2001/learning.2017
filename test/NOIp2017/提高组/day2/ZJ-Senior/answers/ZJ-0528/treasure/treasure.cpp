#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <math.h>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct node
{
	int x,dep[15],v;
}q[1000005];

int n,m,ans1;
int w[55][55];
int belong[55],to[55];

void dp(int x)
{
	int i,j,sz=0;
	for(i=1;i<=n;i++)
		if(i!=x)
		{
			belong[i]=++sz;
			to[sz]=i;
		}
	to[0]=x;
	int top=0,dep=0;
	node xi,yi;
	dep++;
	for(i=1;i<=n;i++)
		q[0].dep[i]=0;
	q[0].x=0;q[0].v=0;
	while(top!=dep)
	{
		xi=q[top++];if(top==1000000) top=0;
		if(xi.x==(1<<n-1)-1)
		{
			ans1=min(ans1,xi.v);
			continue;
		}
		for(i=1;i<=n;i++)
			if(i!=x)
				if(!((1<<belong[i]-1)&xi.x))
					if(w[x][i]!=0x3f3f3f3ff&&xi.v+w[x][i]*yi.dep[i]<=ans1)
					{
						yi=xi;
						yi.dep[i]=xi.dep[x]+1;
						yi.v+=w[x][i]*yi.dep[i];
						yi.x+=(1<<belong[i]-1);
						q[dep++]=yi;			
					}	
		for(i=1;i<=n;i++)
			if(i!=x)
				if(!((1<<belong[i]-1)&xi.x))
				{
					for(j=1;j<=n;j++)
						if(j!=x)
							if((1<<belong[j]-1)&xi.x)
								if(w[j][i]!=0x3f3f3f3f&&xi.v+w[j][i]*yi.dep[i]<=ans1)
								{
									yi=xi;
									yi.dep[i]=xi.dep[j]+1;
									yi.v+=w[j][i]*yi.dep[i];
									yi.x+=(1<<belong[i]-1);
									q[dep++]=yi;
									if(dep==1000000) dep=0;	
								}
				}
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j,k,x,y,v,ha=-1,cho=0;
	n=read(),m=read();
	memset(w,0x3f,sizeof(w));
	for(i=1;i<=m;i++)
	{
		x=read(),y=read(),v=read();
		if(ha==-1)
			ha=v;
		if(ha!=v)
			cho=1;
		w[x][y]=min(w[x][y],v);
		w[y][x]=min(w[y][x],v);
	}
	if(cho==0)
	{
		for(i=1;i<=n;i++)
			w[i][i]=0;
		for(k=1;k<=n;k++)
			for(i=1;i<=n;i++)
				for(j=1;j<=n;j++)
					w[i][j]=min(w[i][j],w[i][k]+w[k][j]);
		int ans=0x3f3f3f3f,sum;
		for(i=1;i<=n;i++)
		{
			sum=0;
			for(j=1;j<=n;j++)
				sum+=w[i][j];
			ans=min(ans,sum);
		}
		printf("%d\n",ans);
	}
	else
	{
		ans1=0x3f3f3f3f;
		for(i=1;i<=n;i++)
			dp(i);
		printf("%d\n",ans1);
	}
	return 0;
}


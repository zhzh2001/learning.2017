#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <math.h>
using namespace std;
#define ll long long

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct cir
{
	int x,y,z;
}a[1005];

int n,h,r;
int fa[1005],mi[1005],ma[1005];

bool judge(int x,int y)
{
	ll d=1ll*(a[x].x-a[y].x)*(a[x].x-a[y].x)
		+1ll*(a[x].y-a[y].y)*(a[x].y-a[y].y)
		+1ll*(a[x].z-a[y].z)*(a[x].z-a[y].z);
	if(d<=4ll*r*r)
		return 1;
	else
		return 0;
}

int find(int x)
{
	if(x==fa[x])
		return x;
	int t=fa[x];
	fa[x]=find(t);
	return fa[x];
}

void join(int x,int y)
{
	int fx=find(x),fy=find(y);
	if(fx!=fy)
	{
		fa[fx]=fy;
		mi[fy]=min(mi[fy],mi[fx]);
		ma[fy]=max(ma[fy],ma[fx]);
	}
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int ti,i,j,on1;
	ti=read();
	while(ti--)
	{
		on1=0;
		n=read(),h=read(),r=read();
		for(i=1;i<=n;i++)
			fa[i]=i;
		for(i=1;i<=n;i++)
		{
			a[i].x=read(),a[i].y=read(),a[i].z=read();
			mi[i]=a[i].z;
			ma[i]=a[i].z;
		}
		for(i=1;i<n;i++)
			for(j=i+1;j<=n;j++)
				if(judge(i,j))
					join(i,j);
		for(i=1;i<=n;i++)
			if(fa[i]==i)
				if(mi[i]<=r&&ma[i]>=h-r)
				{
					on1=1;
					break;
				}
		if(on1==1)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}


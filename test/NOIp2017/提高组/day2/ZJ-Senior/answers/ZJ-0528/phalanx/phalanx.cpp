#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <math.h>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c<='9'&&c>='0'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n,m,q;

struct work1
{
	int s[500005];
	void add(int x,int w)
	{
		for(int i=x;i<=m;i+=i&(-i))
			s[i]+=w;
	} 
	int ask(int x)
	{
		int re=0;
		for(int i=x;i>0;i-=i&(-i))
			re+=s[i];
		return re;
	}
	void sl()
	{
		int i,x,y,id1,id2;
		for(i=1;i<=m;i++)
			add(i,1);
		for(i=1;i<=q;i++)
		{
			x=read(),y=read();
			id1=ask(y);id2=ask(m);
			printf("%d\n",id1);
			add(m,id1-id2);
			add(y,1);
			add(m,-1); 
		}
	}
}w1;

struct work2
{
	int s1[1005][1005],s2[1005][1005];
	void add1(int x,int y,int w)
	{
		for(int i=y;i<=m;i+=i&(-i))
			s1[x][i]+=w;
	}
	void add2(int x,int y,int w)
	{
		for(int i=y;i<=n;i+=i&(-i))
			s2[x][i]+=w;
	}
	int ask(int x,int y)
	{
		int i,re=0;
		for(i=y;i>0;i-=i&(-i))
			re+=s1[x][i];
		for(i=x;i>0;i-=i&(-i))
			re+=s2[y][i];
		for(i=1;i>0;i-=i&(-i))
			re-=s2[y][i];
		return re; 
	} 
	void sl()
	{
		int i,j,x,y,id1,id2;
		for(i=1;i<=n;i++)
			for(j=1;j<=m;j++)
			{ 
				add1(i,j,1);
				if(i!=1)
					add2(j,i,m); 
			} 
		for(i=1;i<=q;i++)
		{
			x=read(),y=read();
			id1=ask(x,y);id2=ask(n,m);
			printf("%d\n",id1);
			add1(x,y,1);
			add1(x,m,-1);
			for(j=y;j<=m-1;j++)
				add2(j,x,1);
			add2(m,x,m);
			add2(m,n,-m);
			for(j=x;j<=n-1;j++)
				add1(j,m,m);			
			add1(n,m,id1-id2);
			add2(m,n,id1-id2);
		} 
	}
}w2;

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if(n==1)
		w1.sl(); 
	else if(n<=1000&&m<=1000);
		w2.sl();
	return 0;
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
const int N=1000+10;
int n,h,r,x[N],y[N],z[N];
bool f[N];
ull len;
ull sqr(ull a){return a*a;}
bool check(int a,int b)
{
	if(sqr(abs(x[a]-x[b]))+sqr(abs(y[a]-y[b]))+sqr(abs(z[a]-z[b]))<=len)return 1;
	return 0;
}
bool a[N][N];
void dfs(int i)
{
	if(f[i])return;
	f[i]=1;
	For(j,1,n)
		if(a[i][j])dfs(j);
}
void work()
{
	For(i,1,n)
		For(j,i+1,n)
			if(check(i,j))a[i][j]=a[j][i]=1;
	For(i,1,n)
		if(z[i]-r<=0)dfs(i);
	For(i,1,n)
		if(z[i]+r>=h&&f[i]==1){puts("Yes");return;}
	puts("No");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(int test=read();test;--test)
	{
		n=read(),h=read(),r=read();len=(ull)4*r*r;
		For(i,1,n)x[i]=read(),y[i]=read(),z[i]=read();
		memset(a,0,sizeof a);
		memset(f,0,sizeof f);
		work();
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
int a[100005],b[100005];
int main()
{
	freopen("phalanx.out","r",stdin);
	For(i,1,100000)a[i]=read();
	fclose(stdin);
	freopen("phalanx2.ans","r",stdin);
	For(i,1,100000)b[i]=read();
	For(i,1,100000)if(a[i]!=b[i])return puts("No"),0;
	puts("Yes");
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
inline void write(int x)
{
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
int n,m,q,a[1010][1010];
void baoli1()
{
	For(i,1,n)For(j,1,m)a[i][j]=(i-1)*m+j;
	for(;q;--q)
	{
		int x=read(),y=read();
		write(a[x][y]);puts("");
		int tmp=a[x][y];
		For(i,y,m-1)
		{
			a[x][i]=a[x][i+1];
		}
		For(i,x,n-1)
		{
			a[i][m]=a[i+1][m];
		}
		a[n][m]=tmp;
	}
}
const int N=300000+10;
int id[N*3],tr[N*3*4],Qx,Ql,Qr;
struct node{int x,y;}Q[N];
int query(int o,int l,int r)
{
	if(Ql<=l&&Qr>=r)return tr[o];
	int mid=(l+r)>>1,res=0;
	if(Ql<=mid)res=query(o<<1,l,mid);
	if(Qr>mid)res+=query(o<<1|1,mid+1,r);
	return res;
}
void modify(int o,int l,int r)
{
	if(l==r){++tr[o];return;}
	int mid=(l+r)>>1;
	if(Qx<=mid)modify(o<<1,l,mid);else modify(o<<1|1,mid+1,r);
	tr[o]=tr[o<<1]+tr[o<<1|1];
}
void baoli2()
{
	int cnt=0,len=n+m+q;
	For(i,1,m)id[++cnt]=i;
	For(i,2,n)id[++cnt]=i*m;
	For(i,1,q)
	{
		int y=Q[i].y,l=1,r=len,mid,tmp;
		for(;l<=r;)
		{
			mid=(l+r)>>1;
			Ql=1,Qr=mid;
			if(mid-query(1,1,len)>=y)tmp=mid,r=mid-1;else l=mid+1;
		}
		write(id[tmp]);puts("");
		Qx=tmp;
		modify(1,1,len);
		id[++cnt]=id[tmp];
		id[tmp]=0;
	}
}
int b[505][50005],c[50005],h[505],f[50005];
//int b[105][105],c[105],h[100005],f[105];
void baoli3()
{
	For(i,1,q)
	{
		if(Q[i].y!=m)h[++*h]=Q[i].x;
	}
	sort(h+1,h+*h+1);
	*h=unique(h+1,h+*h+1)-(h+1);
	For(i,1,*h)
	{
		f[h[i]]=i;
		For(j,1,m-1)b[i][j]=(h[i]-1)*m+j;
	}
	For(i,1,n)c[i]=i*m;
	For(qq,1,q)
	{
		int x=Q[qq].x,y=Q[qq].y;
		if(y!=m)
		{
			int X=f[x];
			int tmp=b[X][y];
			write(tmp);puts("");
			For(i,y,m-2)b[X][i]=b[X][i+1];
			b[X][m-1]=c[x];
			For(i,x,n-1)c[i]=c[i+1];
			c[n]=tmp;
		}else
		{
			int tmp=c[x];
			write(tmp);puts("");
			For(i,x,n-1)c[i]=c[i+1];
			c[n]=tmp;
		}
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(),m=read(),q=read();
		bool flag=1;
		For(i,1,q)
		{
			Q[i].x=read(),Q[i].y=read();
			if(Q[i].x!=1)flag=0;
		}
		baoli2();
}

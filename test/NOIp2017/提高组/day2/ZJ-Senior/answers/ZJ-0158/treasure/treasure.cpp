#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
using namespace std;
typedef long long ll;
#define For(i,a,b) for(int i=a;i<=b;++i)
inline int read()
{
	int x=0;char c=getchar();bool f=0;
	for(;c>'9'||c<'0';c=getchar())f=c=='-';
	for(;c>='0'&&c<='9';c=getchar())x=(x<<1)+(x<<3)+c-'0';
	return f?-x:x;
}
const int N=15;
int n,m,dep[N],q[N],a[N][N];
void addedge(int u,int v,int w)
{
	a[u][v]=a[v][u]=min(a[v][u],w);
}
int bfs(int st)
{
	int res=0;
	For(i,1,n)dep[i]=1e9;
	dep[st]=0;
	q[0]=st;
	for(int h=0,t=1;h!=t;)	
	{
		int u=q[h++];
		For(i,1,n)
		{
			if(a[u][i]!=1e9&&dep[i]==1e9)
			{
				dep[i]=dep[u]+1,q[t++]=i;
				res+=dep[i];
			}
		}
	}
	return res;
}
void work1(int w)
{
	int ans=1e9;
	For(i,1,n)
	{
		ans=min(ans,w*bfs(i));
	}
	printf("%d\n",ans);
}
int ecnt,head[N],ans,E,x[N*N],y[N*N];
struct edge{int to,nxt,w;}e[N*N];
void adde(int u,int v,int w)
{
//	printf("%d %d %d\n",u,v,w);
	e[++ecnt]=(edge){v,head[u],w};head[u]=ecnt;
	e[++ecnt]=(edge){u,head[v],w};head[v]=ecnt;
	x[++E]=u,y[E]=v;
}
void bfs2(int st,int s)
{
	int sum=0;
	For(i,1,n)dep[i]=1e9;
	dep[st]=0;
	q[0]=st;
	for(int h=0,t=1;h!=t;)
	{
		int u=q[h++];
		for(int i=head[u];i;i=e[i].nxt)
		{
			int v=e[i].to,w=(i+1)>>1;
			if((s>>(w-1)&1)&&dep[v]==1e9)
			{
				dep[v]=dep[u]+1;
				sum+=dep[v]*e[i].w;
				if(sum>=ans)return;
				q[t++]=v;
			}
		}
	}
	ans=sum;
}
void calc(int s)
{
	For(i,1,n)
	{
		bfs2(i,s);
	}
}
set<int>Set;
/*void dfs(int s1,int s2)
{
	if(Set.find(s2)!=Set.end())return;
	Set.insert(s2);
	if(s1==(1<<n)-1){calc(s2);return;}
	For(i,1,n)
		if(s1>>(i-1)&1)
		{
			for(int j=head[i];j;j=e[j].nxt)
			{
				int v=e[j].to,w=(j+1)>>1;
				if(!(s1>>(v-1)&1))dfs(s1|(1<<(v-1)),s2|(1<<(w-1)));
			}
		}
}*/
int fa[N];
int getfa(int x)
{
	return fa[x]==x?x:getfa(fa[x]);
}
void dfs(int s,int cnt,int num)
{
	if(E-num+cnt<n-1)return;
	if(cnt==n-1){calc(s);return;}
	For(i,num+1,E)
	{
		int fx=getfa(x[i]),fy=getfa(y[i]);
		if(fx!=fy)
		{
			fa[fx]=fy;
			dfs(s|(1<<(i-1)),cnt+1,i);
			fa[fx]=fx;
		}
	}
}
void work2()
{
	ans=1e9;
	For(i,1,n)
		For(j,i+1,n)
			if(a[i][j]!=1e9)adde(i,j,a[i][j]);
//	dfs(1,0);
//	printf("%d\n",(int)Set.size());
	For(i,1,n)fa[i]=i;
	dfs(0,0,0);
	printf("%d\n",ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	For(i,1,n)For(j,1,n)a[i][j]=1e9;
	int flag=0;
	For(i,1,m)
	{
		int u=read(),v=read(),w=read();
		addedge(u,v,w);
		if(flag==0)flag=w;else if(flag!=w)flag=-1;
	}
	if(flag!=-1)work1(flag);else work2();
}


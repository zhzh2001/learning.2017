#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
int i,j,k,n,m,x,y,t,a[1001][1001],q;
struct data{int l,r,size;ll x;}T[4000001];
void work1(){
	for (i=1;i<=n;i++)for (j=1;j<=m;j++)a[i][j]=(i-1)*m+j;
	while (q--){
		x=read();y=read();
		printf("%d\n",a[x][y]);
		t=a[x][y];
		for (i=y+1;i<=m;i++)a[x][i-1]=a[x][i];
		for (i=x+1;i<=n;i++)a[i-1][m]=a[i][m];
		a[n][m]=t;
	}
}
void update(int i){T[i].size=T[i*2].size+T[i*2+1].size;}
void build(int i,int l,int r){
	T[i].l=l;T[i].r=r;T[i].size=T[i].x=0;
	if (l==r)return;
	int mid=l+r>>1;
	build(i*2,l,mid);build(i*2+1,mid+1,r);
}
void insert(int i,int x,ll y){
	if (T[i].l==T[i].r){T[i].size=1;T[i].x=y;return;}
	int mid=(T[i].l+T[i].r)>>1;
	if (x<=mid)insert(i*2,x,y);else insert(i*2+1,x,y);
	update(i);
}
ll query(int i,int x){
	ll ans;
	if (T[i].l==T[i].r){T[i].size=0;return T[i].x;}
	if (T[i*2].size>=x)ans=query(i*2,x);else ans=query(i*2+1,x-T[i*2].size);
	update(i);
	return ans;
}
void work2(){
	build(1,1,n+m+q);
	for (i=1;i<=m;i++)insert(1,i,1ll*i);
	for (i=2;i<=n;i++)insert(1,i+m-1,1ll*i*m);
	for (i=1;i<=q;i++){
		x=read();y=read();ll t=query(1,y);
		printf("%lld\n",t);
		insert(1,n+m-1+i,t);
	}
}
struct node{int l,r;}we[1000001];
ll s[1000001];
void work3(){
//	for (i=1;i<=n;i++)s[i]=i*m,we[i].l=i,we[i].r=m;t=n;
	while (q--){
		ll tp;x=read();y=read();bool flag=0;int p=0;
		for (i=1;i<=t;i++)
			if (we[i].l==x&&we[i].r==y){flag=1;p=i;printf("%lld\n",s[i]);tp=s[i];break;}
			else if (we[i].l==x&&we[i].r<y)p++;
		if (!flag){y=y+p;printf("%lld\n",1ll*(x-1)*m+y);tp=1ll*(x-1)*m+y;}
		for (i=1;i<=t;i++)
			if (we[i].l==x&&we[i].r>y)we[i].r--;
			else if (we[i].l>x)we[i].l--;
		if (flag){we[p].l=n;we[p].r=m;continue;}
		t++;s[t]=tp;we[t].l=n;we[i].r=m;
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if (m<=1000&&n<=1000)work1();else if (n<=50000&&m<=50000)work3();else work2();
	return 0;
}

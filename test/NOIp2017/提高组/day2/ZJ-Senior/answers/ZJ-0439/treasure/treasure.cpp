#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 21
#define M 2100
using namespace std;
typedef long long ll;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
int i,j,k,n,m,x,y,t,st,w[N][N],b[N],w1[N][N],c[N];
ll ans=-1,dis[N];
struct data{int x,y;}q[N];
int min(int x,int y){return x<y?x:y;}
bool cmp(const data&a,const data&b){return a.y<b.y;}
void dfs(int x,ll p){
	if (ans!=-1&&ans<p)return;
	if (x==n+1){ans=ans==-1?p:min(ans,p);return;}
	for (register int i=1;i<=n;i++)
		if (!b[i]){
			b[i]=1;c[x]=i;int tot=0;
			for (register int j=1;j<x;j++)
				if (w[c[j]][i]!=-1)q[++tot].x=c[j],q[tot].y=(dis[c[j]]+1)*w[c[j]][i];
			sort(q+1,q+1+tot,cmp);
			for (register int j=1;j<=tot;j++){
				dis[i]=dis[q[j].x]+1;
				dfs(x+1,p+q[j].y);
			}
			b[i]=0;
		}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(w,-1,sizeof w);
	n=read();m=read();
	while (m--){
		x=read();y=read();t=read();
		if (w[x][y]==-1)w[x][y]=w[y][x]=t;
		w[x][y]=w[y][x]=min(w[x][y],t);
	}
	for (st=1;st<=n;st++){memset(dis,0,sizeof dis);c[1]=st;b[st]=1;dfs(2,0);b[st]=0;}
	printf("%lld\n",ans);
	return 0;
}

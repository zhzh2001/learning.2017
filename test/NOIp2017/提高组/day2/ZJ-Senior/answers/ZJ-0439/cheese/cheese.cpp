#include <cmath>
#include <cstdio>
#include <cstring>
#define eps 1e-10
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9'){f=ch=='-'?-f:f;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
	return x*f;
}
#define S 0
#define T 2001
#define N 1100
using namespace std;
int i,j,k,n,m,x,y,t,h,r,aci,q[2003001],b[3111];
struct data{double x,y,z;}a[N];
int fi[2003001],ne[2003001],la[2003001],aa[2003001];
double sqr(double x){return x*x;}
double dis(int i,int j){return sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z));}
void add(int x,int y){k++;aa[k]=y;if (fi[x]==0)fi[x]=k;else ne[la[x]]=k;la[x]=k;}
void push(int x){if (b[x])return;b[x]=1;q[++t]=x;}
bool bfs(){
	h=t=0;
	memset(b,0,sizeof b);
	push(S);
	while (h<t){x=q[++h];if (x==T)return 1;for (int i=fi[x];i;i=ne[i])push(aa[i]);}
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	aci=read();
	while (aci--){
		memset(fi,0,sizeof fi);
		memset(ne,0,sizeof ne);
		k=0;n=read();h=read();r=read();
		for (i=1;i<=n;i++)scanf("%lf%lf%lf",&a[i].x,&a[i].y,&a[i].z);
		for (i=1;i<=n;i++)for (j=i+1;j<=n;j++)if (dis(i,j)<=2*r+eps){add(i,j);add(j,i);}
		for (i=1;i<=n;i++)if (a[i].z<=2*r+eps)add(S,i);
		for (i=1;i<=n;i++)if (h-a[i].z<=2*r+eps)add(i,T);
		printf("%s\n",bfs()?"Yes":"No");
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#define maxp 100010
using namespace std;
int n,m,dep[maxp],w;
int topt,to[maxp],nt[maxp],st[maxp];
bool v[maxp];
long long ans;
void add(int x,int y)
{
	to[++topt]=y;
	nt[topt]=st[x];
	st[x]=topt;
}
void build_tree(int x)
{
    v[x]=1; int p=st[x];
	while (p)
	{
		if (!v[to[p]]) {dep[to[p]]=dep[x]+1; build_tree(to[p]);}
		p=nt[p];
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int xx,yy; scanf("%d%d%d",&xx,&yy,&w);
		add(xx,yy); add(yy,xx);
	}
	dep[1]=0;
	build_tree(1);
	for (int i=1;i<=n;i++) ans+=dep[i]*w;
	printf("%lld",ans);
return 0;
}

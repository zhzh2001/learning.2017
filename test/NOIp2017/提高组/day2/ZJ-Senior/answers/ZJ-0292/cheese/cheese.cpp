#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#define ull unsigned long long
using namespace std;
int t,n;
bool flag1,flag2;
bool data[1010][1010];
bool up[1010],down[1010];
bool v[1010];
int f[1010];
void dfs(int x)
{
	if (flag1) return;
	if (up[x]) {flag1=1; return;}
	v[x]=1;
	for (int i=1;i<=n;i++)
	 if (!v[i] && data[x][i]) dfs(i);
	v[x]=0;
}
bool sou(int x)
{
	if (f[x]==0) return 0;
	if (f[x]==1) return 1;
	if (up[x]) {f[x]=1; return 1;}
	v[x]=1;
	bool ff=0;
	for (int i=1;i<=n;i++)
	 if (!v[i] && data[x][i] && f[i]!=0) {if (f[i]==1) {ff=1; break;} ff=sou(i);}
	v[x]=0;
	if (!ff) {f[x]=0; return 0;}else {f[x]=1; return 1;} 
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for (int ii=1;ii<=t;ii++)
	{
		long long h,r;
		scanf("%d%lld%lld",&n,&h,&r);
		if (n==1)
		{
			int x,y,z; scanf("%d%d%d",&x,&y,&z);
			if (z+r>=h && z-r<=0) printf("Yes\n");else printf("No\n");
			continue;
		}   
		//20
		if (n<=8)
		{
		  int x[n+10],y[n+10],z[n+10];
		  flag1=0;
		  memset(data,0,sizeof data);
		  memset(up,0,sizeof up);
		  memset(down,0,sizeof down);
		  for (int i=1;i<=n;i++) 
		  {
		    scanf("%d%d%d",&x[i],&y[i],&z[i]);
		    for (int j=1;j<i;j++)
		     if ((x[j]-x[i])*(x[j]-x[i])+(y[j]-y[i])*(y[j]-y[i])+(z[j]-z[i])*(z[j]-z[i])<=4*r*r)
		      data[i][j]=data[j][i]=1;
		    if (z[i]-r<=0) down[i]=1;
		    if (z[i]+r>=h) up[i]=1;
		    if (down[i]==1 && up[i]==1) flag1=1; 
		  }
		  if (flag1) {printf("Yes\n"); continue;}
		  flag1=0;
	      for (int i=1;i<=n;i++) 
			{memset(v,0,sizeof v); if (down[i]) dfs(i); if (flag1) break;}
		  if (flag1) printf("Yes\n");else printf("No\n");
		  flag1=0;
		  continue;
	    }
		//40
	    long long x[n+10],y[n+10],z[n+10];
		memset(data,0,sizeof data);
		memset(up,0,sizeof up);
		memset(down,0,sizeof down);
		for (int i=1;i<=n;i++) 
		{
		  scanf("%d%d%d",&x[i],&y[i],&z[i]);
		  for (int j=1;j<i;j++)
		    if ((ull)((ull)((x[j]-x[i])*(x[j]-x[i]))+(ull)((y[j]-y[i])*(y[j]-y[i]))+(ull)((z[j]-z[i])*(z[j]-z[i])))<=(ull)4*r*r)
		     data[i][j]=data[j][i]=1;
		  if (z[i]-r<=0) down[i]=1;
		  if (z[i]+r>=h) up[i]=1;
		} 
		flag2=0;
		for (int i=1;i<=n;i++) f[i]=2;
		for (int i=1;i<=n;i++) 
		  {memset(v,0,sizeof v); if (down[i] && f[i]==2) flag2=sou(i); if (flag2) break;}
		if (flag2) printf("Yes\n");else printf("No\n");
		flag2=0;
		//100
	}
return 0;
}

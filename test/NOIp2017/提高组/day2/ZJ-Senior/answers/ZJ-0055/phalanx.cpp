#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<queue>
#include<vector>
#include<algorithm>
#define LL long long
#define ept 1e-12
#define N 20
#define inf 2000000000
using namespace std;
int get_int_(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,Q;
LL mp[3000][3000];
struct Que{
	int x,y;
}q[400000];
void solve1(){
	int nx,ny;
	for(int i=1;i<=Q;i++){
		nx=q[i].x;ny=q[i].y;
		for(int j=i-1;j>=1;j--){
			if(ny==m && nx==n){nx=q[j].x;ny=q[j].y;continue;}
			if(ny==m && nx>=q[j].x) nx++;
			if(q[j].x==nx && q[j].y<=ny) ny++;
		}
		printf("%lld\n",(LL)(nx-1)*(LL)(m)+(LL)ny);
	}
}
LL get_num_(int x,int y){
	if(mp[x][y])return mp[x][y];
	return mp[x][y]=(x-1)*m+y;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	bool flg=1;
	n=get_int_();m=get_int_();Q=get_int_();
	for(int i=1;i<=Q;i++){
		q[i].x=get_int_();q[i].y=get_int_();
		if(q[i].x!=1) flg=0;
	}
	if(Q<=3000){
		solve1();
		return 0;
	}
	for(int i=1;i<=Q;i++){
		printf("%lld\n",get_num_(q[i].x,q[i].y));
		LL tmp=get_num_(q[i].x,q[i].y);
		for(int j=q[i].y;j<m;j++)
			mp[q[i].x][j]=get_num_(q[i].x,j+1);
		for(int j=q[i].x;j<n;j++)
			mp[j][m]=get_num_(j+1,m);
		mp[n][m]=tmp;
	}
	return 0;
}

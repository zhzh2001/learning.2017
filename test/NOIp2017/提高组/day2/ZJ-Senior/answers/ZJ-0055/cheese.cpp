#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<queue>
#include<vector>
#include<algorithm>
#define LL long long
#define ept 1e-12
using namespace std;
int get_int_(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,h,r,S,T,cnt;
int hed[2000],q[100000],top;
bool vis[2000];
struct node{
	int x,y,z;
}g[2000];
struct edge{
	int r,nxt;
}e[3000000];
void insert_(int u,int v){
	e[++cnt].r=v;e[cnt].nxt=hed[u];hed[u]=cnt;
	e[++cnt].r=u;e[cnt].nxt=hed[v];hed[v]=cnt;
}
double len(int i,int j){
	return (double)sqrt((double)(g[i].x-g[j].x)*(double)(g[i].x-g[j].x)+double(g[i].y-g[j].y)*(double)(g[i].y-g[j].y)+double(g[i].z-g[j].z)*(double)(g[i].z-g[j].z));
}
bool dijkstra_(){
	memset(vis,0,sizeof(vis));
	int L=0,R=1,now;q[R]=S;vis[S]=1;
	while(L!=R){
		L++;
		now=q[L];
		for(int i=hed[now];i;i=e[i].nxt)
			if(!vis[e[i].r]){
				q[++R]=e[i].r;
				vis[e[i].r]=1;
			}
	}
	return vis[T]==1;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int tt;
	tt=get_int_();
	while(tt--){
		n=get_int_();h=get_int_();r=get_int_();
		S=0;T=n+1;cnt=0;
		memset(hed,0,sizeof(hed));
		for(int i=1;i<=n;i++){
			g[i].x=get_int_();g[i].y=get_int_();g[i].z=get_int_();
			if(g[i].z-r<=0) 
				insert_(S,i);
			if(g[i].z+r>=h)
				insert_(i,T);
			for(int j=1;j<i;j++)
				if(len(i,j)<=r+r+ept) insert_(i,j);
		}
		if( dijkstra_() ) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

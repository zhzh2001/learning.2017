#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<queue>
#include<vector>
#include<algorithm>
#define LL long long
#define ept 1e-12
#define N 20
#define inf 2000000000
using namespace std;
int get_int_(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,cnt=0;
int S=0,ans=inf;
int dep[20],mp[20][20],q[2000],fa[20];
bool vis[20];
int find(int x){
	return fa[x]==x?x:fa[x]=find(fa[x]);
}
void sol(int nw,int F){
	bool c=1;
	for(int i=1;i<=n;i++)
		if(find(i)==find(F) && vis[i]==0 && i!=nw){
			c=0;break;
		}
	LL re=inf;
	if(nw!=F){
		for(int i=1;i<=n;i++)
			if(vis[i] && (mp[nw][i]!=-1))
				if(dep[i]*mp[nw][i]<re){
					re=dep[i]*mp[nw][i];
					dep[nw]=dep[i]+1;
				}
	}
	else re=0;
	S=S+re;
	if(S>=ans){
		S=S-re;
		return;
	}
	vis[nw]=1;
	if(c) ans=S;
	for(int i=1;i<=n;i++)
		if((!vis[i]) && find(i)==find(F)){
			sol(i,F);
		}
	S=S-re;
	vis[nw]=0;
	return;
}
void solve1(int nw){
	int L=0;
	for(int i=1;i<=n;i++)
		if((!vis[i]) && (mp[nw][i]!=-1)){
			q[++L]=i;
			vis[i]=1;
			S+=dep[nw]*mp[nw][i];
			dep[i]=dep[nw]+1;
		}
	for(int i=1;i<=L;i++)solve1(q[i]);
	return;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	bool flg=1;
	int u,v,w,ww;
	n=get_int_();m=get_int_();
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) mp[i][j]=-1;
	u=get_int_();v=get_int_();w=get_int_();
	if(mp[u][v]==-1 || mp[u][v]>w) mp[v][u]=mp[u][v]=w;
	ww=w;
	for(int i=2;i<=m;i++){
		u=get_int_();v=get_int_();w=get_int_();
		if(w!=ww) flg=0;
		if(mp[u][v]==-1 || mp[u][v]>w) mp[v][u]=mp[u][v]=w;
		u=find(u);v=find(v);
		if(u!=v) fa[u]=v;
	}
	if(flg){
		for(int i=1;i<=n;i++){
			S=0;
			memset(vis,0,sizeof(vis));
			memset(dep,0,sizeof(dep));
			dep[i]=1;vis[i]=1;
			solve1(i);
			if(S<ans) ans=S;
		}
		printf("%d\n",ans);
		return 0;
	}
	for(int i=1;i<=n;i++){
		S=0;
		memset(vis,0,sizeof(vis));
		memset(dep,0,sizeof(dep));
		dep[i]=1;vis[i]=1;
		sol(i,i);
	}
	printf("%d\n",ans);
	return 0;
}

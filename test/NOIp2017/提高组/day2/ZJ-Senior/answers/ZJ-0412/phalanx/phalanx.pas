var f:array[0..1010,0..1010] of longint;
    g:array[0..300010] of longint;
    n,m,q,i,j,t,x,y:longint;

begin
  assign(input,'phalanx.in');
  assign(output,'phalanx.out');
  reset(input); rewrite(output);
  readln(n,m,q);
  if n<>1 then
  begin
  for i:=1 to n do
    for j:=1 to m do
      f[i,j]:=(i-1)*m+j;
  for q:=1 to q do
    begin
      readln(x,y);
      t:=f[x,y]; writeln(t);
      for i:=y to m-1 do f[x,i]:=f[x,i+1];
      for i:=x to n-1 do f[i,m]:=f[i+1,m];
      f[n,m]:=t;
    end;
  end
  else
  begin
  for j:=1 to m do g[j]:=j;
  for q:=1 to q do
    begin
      readln(x,y);
      t:=g[y]; writeln(t);
      for i:=y to m-1 do g[i]:=g[i+1];
      g[m]:=t;
    end;
  end;
  close(input); close(output);
end.

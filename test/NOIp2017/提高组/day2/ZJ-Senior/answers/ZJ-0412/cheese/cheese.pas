var a,b,c:array[0..1010] of int64;
    d:array[0..10000010] of longint;
    f:array[0..1010,0..1010] of longint;
    g:array[0..1010] of boolean;
    t,n,i,j,h,tt:longint;
    l,r,ans:extended;
    p:boolean;

begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input); rewrite(output);
  readln(tt);
  for tt:=1 to tt do
    begin
      readln(n,l,r);
      for i:=1 to n do readln(a[i],b[i],c[i]);
      for i:=1 to n do f[i,0]:=0;
      for i:=1 to n do
        for j:=i+1 to n do
          begin
            ans:=sqrt(sqr(a[i]-a[j])+sqr(b[i]-b[j])+sqr(c[i]-c[j]))              ;
            if ans<=2*r then
              begin
                inc(f[i,0]); f[i,f[i,0]]:=j;
                inc(f[j,0]); f[j,f[j,0]]:=i;
              end;
          end;
      h:=0; t:=0; c[0]:=-maxlongint; d[1]:=0;
      for i:=1 to n do g[i]:=true;
      for i:=1 to n do
        if c[i]<=r then
          begin
            inc(t); d[t]:=i;
            g[i]:=false;
          end;
      p:=false;
      repeat
        inc(h);
        if c[d[h]]+r>=l then
          begin
            p:=true; break;
          end;
        for i:=1 to f[d[h],0] do
          if g[f[d[h],i]] then
            begin
              inc(t); d[t]:=f[d[h],i];
              g[f[d[h],i]]:=false;
            end;
      until h>=t;
      if p then writeln('Yes')
           else writeln('No');
    end;
  close(input); close(output);
end.

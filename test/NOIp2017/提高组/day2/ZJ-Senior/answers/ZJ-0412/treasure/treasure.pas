const max:int64=500000000000;

var a:array[1..12,1..12] of int64;
    b:array[1..12] of boolean;
    f,g:array[1..12] of int64;
    n,m,i,j,k,l,x,y,z:longint;
    s,ans,t:int64;

begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      if i<>j then
        a[i,j]:=max;
  for i:=1 to m do
    begin
      readln(x,y,z);
      if z<=a[x,y] then a[x,y]:=z;
      if z<=a[y,x] then a[y,x]:=z;
    end;
  ans:=max;
  for i:=1 to n do
    begin
      for j:=1 to n do b[j]:=true; b[i]:=false;
      for j:=1 to n do f[j]:=a[i,j];
      for j:=1 to n do g[j]:=1;
      s:=0;
      for j:=1 to n-1 do
        begin
          t:=max; l:=0;
          for k:=1 to n do
            if (b[k])and(f[k]<t) then
              begin
                t:=f[k]; l:=k;
              end;
          s:=s+t; b[l]:=false;
          for k:=1 to n do
            if (b[k])and(a[l,k]*(g[l]+1)<f[k]) then
              begin
                f[k]:=a[l,k]*(g[l]+1);
                g[k]:=g[l]+1;
              end;
        end;
      if s<ans then ans:=s;
    end;
  writeln(ans);
  close(input); close(output);
end.

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	ifstream fin("phalanx.in");
	ofstream fout("phalanx.out");
	int n,m,q;
	fin>>n>>m>>q;
	vector<vector<int> > mx(n,vector<int>(m));
	for(int i=0;i<n;++i)
		for(int j=0;j<m;++j)
			mx[i][j]=i*m+j+1;
	--n,--m;
	while(q-->0){
		int x,y;
		fin>>x>>y;
		--x,--y;
		int tmp=mx[x][y];
		fout<<tmp<<endl;
		for(int i=y+1;i<=m;++i)mx[x][i-1]=mx[x][i];
		for(int i=x+1;i<=n;++i)mx[i-1][m]=mx[i][m];
		mx[n][m]=tmp;
	}
}

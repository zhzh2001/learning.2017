#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cstring>

using namespace std;

int n;
bool g[1002][1002];
bool cv[1002];
int x[1002],y[1002],z[1002];

bool dfs(int p){
	if(p==n+1)return true;
	cv[p]=true;
	for(int i=0;i<n+2;++i)
		if(g[p][i]&&!cv[i]&&dfs(i))return true;
	return false;
}

int main(){
	ios::sync_with_stdio(false);
	ifstream fin("cheese.in");
	ofstream fout("cheese.out");
	int t;
	fin>>t;
	while(t-->0){
		long long h,r;
		fin>>n>>h>>r;
		memset(g,0,sizeof(g));
		for(int i=1;i<=n;++i)
			fin>>x[i]>>y[i]>>z[i];
		for(int i=1;i<=n;++i){
			if(z[i]-r<=0)g[0][i]=g[i][0]=true;
			if(z[i]+r>=h)g[i][n+1]=g[n+1][i]=true;
		}
		long long d=(r<<1);
		for(int i=1;i<=n;++i)
			for(int j=i+1;j<=n;++j)
				if(sqrt((z[i]-z[j])*(z[i]-z[j])+(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]))<=d)
					g[i][j]=g[j][i]=true;
		memset(cv,0,sizeof(cv));
		if(dfs(0))fout<<"Yes"<<endl;
		else fout<<"No"<<endl;
	}
}

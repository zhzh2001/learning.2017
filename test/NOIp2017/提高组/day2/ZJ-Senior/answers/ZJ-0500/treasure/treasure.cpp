#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

class e{
	public:
		int u,v,w;
		e(int a=0,int b=0,int c=0){u=a,v=b,w=c;}
		bool operator<(const e & i)const{
			return v<i.v;
		}
};

class ufs:vector<int>{
	public:
		ufs(int n){
			resize(n);
			for(int i=0;i<n;++i)at(i)=i;
		}
		int f(int n){return at(n)=(at(n)==n)?n:f(at(n));}
		void u(int a,int b){at(f(a))=f(b);}
};

int main(){
	ifstream fin("treasure.in");
	ofstream fout("treasure.out");
	int n,m;
	fin>>n>>m;
	vector<vector<e> > g(n);
	for(int i=0;i<m;++i){
		int a,b,c;
		fin>>a>>b>>c;
		--a,--b;
		g[a].push_back(e(b,c));
		g[b].push_back(e(a,c));
	}
	long long out=1LL<<62;
	for(int i=0;i<n;++i){
		vector<e> uv;
		uv.reserve(m);
		vector<bool> cv(n,true);
		queue<e> q;
		q.push(e(i,1));
		cv[i]=false;
		while(q.size()){
			e x=q.front();
			q.pop();
			for(int i=0;i<g[x.u].size();++i){
				uv.push_back(e(x.u,x.v*g[x.u][i].v,g[x.u][i].u));
				if(cv[g[x.u][i].u]){
					cv[g[x.u][i].u]=false;
					q.push(e(g[x.u][i].u,x.v+1));
				}
			}
		}
		sort(uv.begin(),uv.end());
		ufs s(n);
		int num=n;
		long long ans=0;
		for(int i=0;num!=1&&i<uv.size();++i){
			if(s.f(uv[i].u)!=s.f(uv[i].w)){
				ans+=uv[i].v;
				s.u(uv[i].u,uv[i].w);
				--num;
			}
		}
		out=min(out,ans);
	}
	fout<<out<<endl;
}

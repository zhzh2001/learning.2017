#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int maxn=1005;
int T,n;
int X[maxn],Y[maxn],Z[maxn],mark[maxn],fa[maxn];
LL h,r,d;
bool chk(LL a,LL b,LL c)
{
	return a*a+b*b+c*c<=d*d;
}
int get(int x)
{
	if(fa[x]==x)return x;
	return fa[x]=get(fa[x]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);while(T--)
	{
		cin>>n>>h>>r;d=r<<1;
		int i,j,flag=0;
		for(i=1;i<=n;i++)scanf("%d%d%d",X+i,Y+i,Z+i);
		for(i=1;i<=n;i++)fa[i]=i,mark[i]=0;
		for(i=1;i<=n;i++)for(j=i+1;j<=n;j++)
		if(chk(X[i]-X[j],Y[i]-Y[j],Z[i]-Z[j]))
		{
			fa[get(i)]=get(j);
		}
		for(i=1;i<=n;i++)if(Z[i]<=r)mark[get(i)]=1;
		for(i=1;i<=n;i++)if(h-Z[i]<=r&&mark[get(i)])flag=1;
		if(flag)puts("Yes");else puts("No");
	}
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
int n,m,Q;
namespace PlanA
{
	const int maxn=1005;
	int a[maxn][maxn];
	void Main()
	{
		int i,j,x,y;
		for(i=1;i<=n;i++)
		for(j=1;j<=m;j++)
		a[i][j]=(i-1)*m+j;
		while(Q--)
		{
			scanf("%d%d",&x,&y);
			printf("%d\n",a[x][y]);
			int t=a[x][y];
			for(i=y+1;i<=m;i++)a[x][i-1]=a[x][i];
			for(i=x+1;i<=n;i++)a[i-1][m]=a[i][m];
			a[n][m]=t;
		}
	}
}
namespace PlanB
{
	const int maxn=6*100000+5;
	int v[maxn],a[maxn],top;
	int b[maxn],hd,ed;
	void upd(int x,int y)
	{
		for(;x<maxn;x+=x&-x)v[x]+=y;
	}
	int cal(int x)
	{
		int y=0;for(;x;x-=x&-x)y+=v[x];return y;
	}
	void Main()
	{
		int i,x,y;
		for(i=1;i<=m;i++)
		{
			a[i]=i;upd(i,1);
		}
		top=m;
		hd=2;ed=n;
		for(i=2;i<=n;i++)b[i]=i*m;
		while(Q--)
		{
			scanf("%d%d",&x,&y);//x=1
			int L=1,R=top,res;
			while(L<=R)
			{
				int mid=(L+R)>>1;
				if(cal(mid)>=y)
				{
					res=mid;R=mid-1;
				}
				else L=mid+1;
			}
			printf("%d\n",a[res]);
			a[++top]=b[hd];++hd;
			b[++ed]=a[res];
			upd(res,-1);
			upd(top,1);
		}
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	if(n<=1000&&m<=1000)PlanA::Main();
	else PlanB::Main();
	return 0;
}

//#include <bits/stdc++.h>
//using namespace std;
//typedef long long LL;
//const int N=12;
//int n,m;
//int dp[1<<N][N];
//int w[N][N],t[1<<N];
//int main()
//{
//	freopen("treasure3.in","r",stdin);
////	freopen("treasure.out","w",stdout);
//	scanf("%d%d",&n,&m);
//	int S,R,i,j,u,v,W;
//	memset(dp,-1,sizeof dp);
//	for(i=0;i<n;i++)t[1<<i]=i,dp[1<<i][i]=0;
//	for(i=0;i<m;i++)
//	{
//		scanf("%d%d%d",&u,&v,&W);--u;--v;
////		assert(W);
//		if(!w[u][v])w[u][v]=w[v][u]=W;
//		else w[u][v]=w[v][u]=min(W,w[u][v]);
//	}
//	for(i=0;i<n;i++,puts(""))
//	for(j=0;j<n;j++)
//	{
//		if(w[i][j]>115)printf("%4d ",-1),w[i][j]=0;
//		else printf("%4d ",w[i][j]);
//	}
//	for(S=1;S<(1<<n);S++)for(R=S;R;R=(R-1)&S)
////	for(S=1;S<(1<<n);S++)for(R=1;R<S;R++)if((R&S)==R)
//	{
////		for(u=0;u<n;u++)if((S^R)&(1<<u))if(~dp[S^R][u])
////		for(v=0;v<n;v++)if(R&(1<<v))if(~dp[R][v])if(w[u][v])
//		for(j=R;j;j-=j&-j)if(~dp[R][v=t[j&-j]])
//		for(i=S^R;i;i-=i&-i)if(~dp[S^R][u=t[i&-i]])
//		if(w[u][v])
//		{
//			if(~dp[S][u])dp[S][u]=min(dp[S^R][u]+2*dp[R][v]+w[u][v],dp[S][u]);
//			else 		 dp[S][u]=	  dp[S^R][u]+2*dp[R][v]+w[u][v];
//		}
//	}
//	int ans=-1;S=(1<<n)-1;
//	for(i=0;i<n;i++)if(~dp[S][i])
//	{
//		if(ans==-1)ans=dp[S][i];
//		else ans=min(ans,dp[S][i]);
//	}
//	printf("%d\n",ans);
//	return 0;
//}
#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
const int N=12;
int n,m;
int dp[1<<N][N][N];
bool mk[1<<N][N][N];
int w[N][N],t[1<<N];
int DP(int S,int rt,int dep)
{
	int &ans=dp[S][rt][dep];
	if(mk[S][rt][dep])return ans;
	int i,T,R=S^(1<<rt),k=R&-R,a,b;
	if(!R)return mk[S][rt][dep]=1,ans=0;
	R^=k;
	for(T=R;;T=(T-1)&R)
	{
		for(i=(T^k);i;i-=i&-i)if(w[rt][t[i&-i]])
		{
			a=DP(T^k,t[i&-i],dep+1);
			b=DP(S^(T^k),rt,dep);
			if(a!=-1&&b!=-1)
			{
				if(ans==-1)ans=a+b+w[rt][t[i&-i]]*dep;
				else ans=min(ans,a+b+w[rt][t[i&-i]]*dep);
			}
		}
		if(T==0)break;
	}
	mk[S][rt][dep]=1;return ans;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	int S,i,u,v,W;
	memset(dp,-1,sizeof dp);
	for(i=0;i<n;i++)t[1<<i]=i;
	for(i=0;i<m;i++)
	{
		scanf("%d%d%d",&u,&v,&W);--u;--v;
		if(!w[u][v])w[u][v]=w[v][u]=W;
		else w[u][v]=w[v][u]=min(W,w[u][v]);
	}
	int ans=1<<30;S=(1<<n)-1;
	for(i=0;i<n;i++)ans=min(ans,DP(S,i,1));
	printf("%d\n",ans);
	return 0;
}

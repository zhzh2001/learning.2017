const maxn=1005;
var t,n,h,r,i,j,k,s1,s2,rec_num:longint;
    up_num,down_num:longint;
    x,y,z,fa:array[0..maxn] of longint;
    rec_x,rec_y,up,down:array[0..maxn*100] of longint;
    flag:boolean;

Function father(x:longint):longint;
begin
  if fa[x]<>x then fa[x]:=father(fa[x]);
  exit(fa[x]);
end;

Function Js(x1,y1,z1,x2,y2,z2,flag:longint):boolean;
begin
  if sqrt(sqr(x1-x2)+sqr(y1-y2)+sqr(z1-z2))<=flag then exit(true)
   else exit(false);
end;

begin
  assign(input,'cheese.in');
  reset(input);
  assign(output,'cheese.out');
  rewrite(output);
  readln(t);
  for i:=1 to t do
   begin
    rec_num:=0;
    up_num:=0; down_num:=0;
    flag:=false;
    fillchar(up,sizeof(up),0);
    fillchar(down,sizeof(down),0);
    readln(n,h,r);
    for j:=1 to n do readln(x[j],y[j],z[j]);
    for j:=1 to n do fa[j]:=j;
    for j:=1 to n do
     begin
      if z[j]<=r then
       begin
        inc(down_num);
        down[down_num]:=j;
       end;
      if h-z[j]<=r then
       begin
        inc(up_num);
        up[up_num]:=j;
       end;
      for k:=(j+1) to n do
        if Js(x[j],y[j],z[j],x[k],y[k],z[k],2*r) then
         begin
          inc(rec_num);
          rec_x[rec_num]:=j;
          rec_y[rec_num]:=k;
          inc(rec_num);
          rec_x[rec_num]:=k;
          rec_y[rec_num]:=j;
         end;
      end;
    for j:=1 to rec_num do
     begin
      s1:=father(rec_x[j]);
      s2:=father(rec_y[j]);
      if s1<>s2 then fa[s1]:=s2;
       end;
    for j:=1 to up_num do
     begin
      for k:=1 to down_num do
       begin
        s1:=father(up[j]);
        s2:=father(down[k]);
        if s1=s2 then
         begin
          flag:=true;
          break;
         end;
       if flag then break;
       end;
     end;
    if flag then writeln('Yes') else writeln('No');
   end;
  close(input);
  close(output);
end.
const maxn=12;
var n,m,i,j,x,y,z,min:longint;
    dis:array[1..maxn,1..maxn] of longint;
    vis:array[1..maxn] of boolean;
    rec:array[1..maxn] of longint;

Procedure Dfs(deep,now:longint);
var i,j:longint;
begin
  if now>=min then exit;
  if deep>=n then
   begin
    if now<min then min:=now;
    exit;
   end;
  for j:=1 to n do
   if vis[j] then
    begin
     for i:=1 to n do
      if (not vis[i]) and (dis[j,i]<$7f) and (i<>j) then
       begin
        vis[i]:=true;
        rec[i]:=rec[j]+1;

        Dfs(deep+1,now+dis[j,i]*rec[i]);

        rec[i]:=rec[j]-1;
        vis[i]:=false;
       end;
    end;
end;

begin
  assign(input,'treasure.in');
  reset(input);
  assign(output,'treasure.out');
  rewrite(output);

  fillchar(dis,sizeof(dis),$7f);
  min:=maxlongint;
  readln(n,m);
  for i:=1 to m do
   begin
    readln(x,y,z);
    if dis[x,y]>z then dis[x,y]:=z;
    if dis[y,x]>z then dis[y,x]:=z;
   end;
  for i:=1 to n do
   begin
    fillchar(vis,sizeof(vis),0);
    vis[i]:=true;
    fillchar(rec,sizeof(rec),0);
    Dfs(1,0);
   end;
  writeln(min);

  close(input);
  close(output);
end.
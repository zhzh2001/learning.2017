var n,m,i,j,p,x,y,temp:longint;
    a:array[1..1005,1..1005] of longint;
    b:array[1..300005] of longint;
begin
  assign(input,'phalanx.in');
  reset(input);
  assign(output,'phalanx.out');
  rewrite(output);
  readln(n,m,p);
  if n<>1 then
   begin
    for i:=1 to n do
     for j:=1 to m do
      a[i,j]:=m*(i-1)+j;
    for i:=1 to p do
     begin
      readln(x,y);
      writeln(a[x,y]);
      temp:=a[x,y];
      for j:=y to (m-1) do a[x,j]:=a[x,j+1];
      for j:=x to (n-1) do a[j,m]:=a[j+1,m];
      a[n,m]:=temp;
     end;
   end
    else
     begin
      for i:=1 to m do b[i]:=i;
      for i:=1 to p do
       begin
        readln(x,y);
        writeln(b[y]);
        temp:=b[y];
        for j:=y to (m-1) do b[j]:=b[j+1];
        b[m]:=temp;
       end;
     end;
  close(input);
  close(output);
end.
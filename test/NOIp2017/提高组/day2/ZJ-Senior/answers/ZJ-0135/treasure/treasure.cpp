#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;

const int M=60000000;

int n,m,x,y,v,a[15][15],f[15][4100],e[15],z[15],num;

int read(){
	char l='+',ch=getchar();int t=0;
	while (ch<'0'||ch>'9'){l=ch;ch=getchar();}
	while (ch>='0'&&ch<='9'){t=(t<<1)+(t<<3)+ch-48;ch=getchar();}
	if (l=='-')
		return -t;else
		return t;
}

void dfs(int t,int Now,int sta,int d,int s){
	if (t==num)
	{
		f[d+1][Now]=min(f[d+1][Now],f[d][sta]+s*(d+1));
		return;
	}
	dfs(t+1,Now|(1<<z[t]),sta,d,s+e[z[t]]);
	dfs(t+1,Now,sta,d,s);
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)
			a[i][j]=M;
	for (int i=1;i<=m;i++)
	{
		x=read();y=read();v=read();x--;y--;
		a[x][y]=a[y][x]=min(a[x][y],v);
	}
	for (int i=0;i<n;i++)
		for (int j=1;j<=(1<<n)-1;j++)
			f[i][j]=M;
	for (int i=0;i<n;i++)
		f[0][1<<i]=0;
	
	for (int d=0;d<n;d++)
		for (int sta=1;sta<=(1<<n)-1;sta++)
		{
			num=0;
			for (int i=0;i<n;i++)
				if ((sta&(1<<i))==0)
					z[num++]=i;
			for (int i=0;i<n;i++)
				e[i]=M;
			for (int i=0;i<n;i++)
				for (int j=0;j<n;j++)
					if ((sta&(1<<i))==0&&(sta&(1<<j))>0)
						e[i]=min(e[i],a[i][j]);
			dfs(0,sta,sta,d,0);
		}
	printf("%d",f[n-1][(1<<n)-1]);
	fclose(stdin);fclose(stdout);
	return 0;
}

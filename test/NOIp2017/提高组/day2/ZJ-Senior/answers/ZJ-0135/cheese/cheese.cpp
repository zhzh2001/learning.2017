#include<iostream>
#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;

int n,T,h,r,s1[50],s2[50],s3[50],h1,h2,h3;
bool Flag,f[1010],fi[1010],fl;

struct node{
	int x,y,z;
}a[1010];

bool cmp(node x,node y){
	return x.z<y.z;
}

int read(){
	char l='+',ch=getchar();int t=0;
	while (ch<'0'||ch>'9'){l=ch;ch=getchar();}
	while (ch>='0'&&ch<='9'){t=(t<<1)+(t<<3)+ch-48;ch=getchar();}
	if (l=='-')
		return -t;else
		return t;
}

ll  Sqr(int t){
	return t*t;
}

int Check(int i,int j){
	if ((ll)Sqr(a[i].x-a[j].x)+(ll)Sqr(a[i].y-a[j].y)+(ll)Sqr(a[i].z-a[j].z)<=(ll)Sqr(r)*4)
		return true;else
		return false;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	for (;T;T--){
		n=read();h=read();r=read();
		for (int i=1;i<=n;i++)
			f[i]=fi[i]=false;
		for (int i=1;i<=n;i++)
			a[i].x=read(),a[i].y=read(),a[i].z=read();
		sort(a+1,a+n+1,cmp);
		fl=false;
		for (int i=1;i<=n;i++)
		{
			if (a[i].z<=r)
				f[i]=fl=true;
			if (a[i].z>=(h-r))
				fi[i]=fl=true;
		}
		if (!fl)	{printf("No\n");continue;}
		int h=1;int t=1;
	
		while (t<=n&&h<=n)
		{
			while (a[t].z<=a[h].z+2*r&&t<=n)
				t++;
			while (!f[h]&&h<=t)
				h++;
			for (int i=h;i<=t;i++)
			{
				for (int j=i+1;j<=t;j++)
					if (f[i])
					{
						if (Check(i,j))
							f[j]=true;
						if (fi[j]&&f[j])
						{
							Flag=true;
							break;
						}
					}
				if (Flag)
					break;
			}
			if (Flag)
				break;
			h++;
			while (a[h].z==a[h-1].z)
				h++;
		}
		if (Flag)
			printf("Yes\n");else
			printf("No\n");
		Flag=false;	
	}
	fclose(stdin);fclose(stdout);
	return 0;
}	

#include <cstdio>
#include <cstring>
int n,m;
int map[50][50];
int d[500000][14];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(map,127/2,sizeof(map));
	int x,y,z;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (map[x][y]>z) map[x][y]=z;
		if (map[y][x]>z) map[y][x]=z;
	}
	int ans=2147483647;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) d[1][j]=-1;d[1][i]=1;d[1][0]=0;
		int he=0,en=1;
		while (he<en){
			he++;
			if (d[he][0]>=ans) continue;
			bool b=true;
			for (int j=1;j<=n;j++) if (d[he][j]==-1){b=false;break;}
			if (b){if (ans>d[he][0]) ans=d[he][0];}
			else{
				int ten=en;
				for (int j=1;j<=n;j++) if (d[he][j]!=-1){
					for (int k=1;k<=n;k++) if (map[j][k]<map[0][0] && d[he][k]==-1){
						++en;
						for (int l=1;l<=n;l++) d[en][l]=d[he][l];
						d[en][k]=d[he][j]+1;
						d[en][0]=d[he][0]+d[he][j]*map[j][k];
						if (d[en][0]>=ans) en--;
						/*for (int l=1;l<en;l++){
							bool bb=true;
							for (int a=0;a<=n;a++) if (d[en][a]!=d[l][a]){bb=false;break;}
							if (bb){en--;break;}
						}*/
					}
				}
				int maxn[10],maxt=0;
				//maxn[1]=ten+1;
				int atmp=2;
				if (he<=500) atmp=3;
				if (n<=10) atmp=4;
				if (n<=8) atmp=6;
				for (int j=ten+1;j<=en;j++) {
					//if (d[j][0]<d[maxn[1]][0]) maxn[1]=j;
					if (maxt<atmp){
						maxt++;
						maxn[maxt]=j;
						for (int k=maxt;k>=2;k--) if (d[maxn[k]][0]<d[maxn[k-1]][0]) {maxn[k]=maxn[k-1];maxn[k-1]=j;}
					}else{
						for (int k=maxt;k>=1;k--) if (d[j][0]<d[maxn[k]][0]) {maxn[k+1]=maxn[k];maxn[k]=j;}
					}
				}
				for (int k=1;k<=maxt;k++) for (int a=0;a<=n;a++) d[ten+k][a]=d[maxn[k]][a];
				en=ten+maxt;
			}
		}
		//printf("::%d\n",en);
	}
	printf("%d\n",ans);
	return 0;
}

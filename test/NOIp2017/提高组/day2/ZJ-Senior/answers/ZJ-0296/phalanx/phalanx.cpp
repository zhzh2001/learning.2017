#include <cstdio>
#include <map>
int n,m,q;
std::map<std::pair<int,int>,int > ma;
int X[500000],Y[500000];
int bit[800000*2];
int len=0;
inline void add(int x,int val){
	for (int i=x;i<=len;i+=i&(-i)){
		bit[i]+=val;
	}
}
inline int query(int x){
	int ans=0;
	for (int i=x;i>=1;i-=i&(-i)) ans+=bit[i];
	return ans;
}
int a[800000*2];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	int x,y;
	bool pd=true;
	for (int i=1;i<=q;i++){
		scanf("%d%d",&X[i],&Y[i]);
		if (X[i]!=1){pd=false;}
	}
	if (pd && q>500){
		len=(n+m)+q;
		for (int i=1;i<=m;i++) {add(i,1);a[i]=i;}
		for (int i=2;i<=n;i++) {add(m+i-1,1);a[m+i-1]=(i-1)*m+n;}
		int totid=m+n-1;
		for (int i=1;i<=q;i++){
			x=X[i];y=Y[i];
			int l=1,r=len,tans=0;
			while (l<=r){
				int m=(l+r)>>1;
				if (query(m)>=y){
					tans=m;r=m-1;
				}else{
					l=m+1;
				}
			}
			int tmp=a[tans];
			totid++;
			a[totid]=tmp;
			add(totid,1);add(tans,-1);
			printf("%d\n",tmp);
		}
	}else{
		for (int i=1;i<=q;i++){
			//scanf("%d%d",&x,&y);
			x=X[i];y=Y[i];
			if (!ma[std::make_pair(x,y)]) ma[std::make_pair(x,y)]=(x-1)*m+y;
			int tmp=ma[std::make_pair(x,y)];
			for (int j=y+1;j<=m;j++){
				ma[std::make_pair(x,j-1)]=ma[std::make_pair(x,j)];
				if (!ma[std::make_pair(x,j-1)]) ma[std::make_pair(x,j-1)]=(x-1)*m+j;
			}
			for (int j=x+1;j<=n;j++){
				ma[std::make_pair(j-1,n)]=ma[std::make_pair(j,n)];
				if (!ma[std::make_pair(j-1,n)]) ma[std::make_pair(j-1,n)]=(j-1)*m+n;
			}
			ma[std::make_pair(n,m)]=tmp;
			printf("%d\n",tmp);
		}
	}
	return 0;
}

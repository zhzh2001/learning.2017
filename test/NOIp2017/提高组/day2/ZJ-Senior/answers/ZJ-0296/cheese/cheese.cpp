#include <cstdio>
#include <cstring>
int n,h,r;
long long x[5000],y[5000],z[5000];
int d[5000],he,en;
bool b[5000];
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);
	while (T--){
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		he=0;en=0;
		memset(b,false,sizeof(b));
		for (int i=1;i<=n;i++) if (z[i]<=r){b[i]=true;d[++en]=i;}
		bool ans=false;
		while (he<en){
			he++;
			int t=d[he];
			if (h-z[t]<=r){ans=true;break;}
			for (int i=1;i<=n;i++) if (!b[i] && ((x[t]-x[i])*(x[t]-x[i])+(y[t]-y[i])*(y[t]-y[i])+(z[t]-z[i])*(z[t]-z[i]))<=4*r*r){
				b[i]=true;
				d[++en]=i;
			}
		}
		if (ans) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

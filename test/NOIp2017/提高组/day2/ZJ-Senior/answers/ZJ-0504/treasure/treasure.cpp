#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int i,j,k,n,m,t,tmp,sum,ans=2e9,cnt[4096],f[12][4096],g[12][4096],a[12][12];
int DP(int u,int s)
{
	if(f[u][s]<1e8)return f[u][s];
	for(int t=s;t;t=(t-1)&s)if(!(t&1<<u))
	{
		if(DP(u,s^t)>1e8||DP(u,s^t)<0)continue;
		for(int i=0;i<n;++i)if((t&1<<i))
		{
			if(a[u][i]>1e8||DP(i,t)>1e8||DP(i,t)<0)continue;
			tmp=f[u][s^t]+f[i][t]+g[i][t]+a[u][i];
			if(tmp<f[u][s])f[u][s]=tmp,g[u][s]=g[u][s^t]+g[i][t]+a[u][i];else
			if(tmp==f[u][s])g[u][s]=min(g[u][s],g[u][s^t]+g[i][t]+a[u][i]);
		}
	}
	if(f[u][s]>1e8)f[u][s]=-1;
	return f[u][s];
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(a,64,sizeof a),memset(f,64,sizeof f);
	for(scanf("%d%d",&n,&m);m--;)
	{
		scanf("%d%d%d",&i,&j,&k);
		if(k<a[--i][--j])a[i][j]=a[j][i]=k;
	}
	for(i=0;++i<1<<n;cnt[i]=cnt[i/2]+i%2);
	for(i=0;i<n;++i)f[i][1<<i]=0;
	for(k=0;k<n;++k)ans=min(ans,DP(k,(1<<n)-1));
	printf("%d",ans);
}

#include<cstdio>
#include<cstring>
typedef long long ll;
const int N=1003;
char fl,vis[N],a[N][N];
ll r2;
int T,i,j,n,h,r,x[N],y[N],z[N];
inline ll S(ll x){return x*x;}
void dfs(int u)
{
	if(u==n+1)fl=1;
	if(fl)return;
	vis[u]=1;
	for(int i=1;i<=n+1;++i)
		if(a[u][i]&&!vis[i])dfs(i);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(scanf("%d",&T);T--;puts(fl?"Yes":"No"))
	{
		scanf("%d%d%d",&n,&h,&r);
		memset(a,0,sizeof a);
		for(r2=4ll*r*r,i=1;i<=n;++i)
			for(scanf("%d%d%d",x+i,y+i,z+i),j=i;--j;)
				a[i][j]=a[j][i]=S(x[i]-x[j])+S(y[i]-y[j])+S(z[i]-z[j])<=r2;
		for(i=1;i<=n;++i)
		{
			a[n+2][i]=z[i]<=r&&z[i]>=-r;
			a[i][n+1]=z[i]<=h+r&&z[i]>=h-r;
		}
		memset(vis+1,fl=0,n+2),dfs(n+2);
	}
}

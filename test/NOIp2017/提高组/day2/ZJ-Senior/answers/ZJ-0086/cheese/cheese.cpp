#include <cstdio>
#include <cstring>
#include <cctype>
#include <vector>
#include <queue>
using std::queue;
using std::vector;

template <typename T>
inline void get(T& a) {
	int flag = 1;
	char c = getchar();
	while (!isdigit(c) && c != '-') c = getchar();
	if (c == '-') {
		flag = -1;
		c = getchar();
	}
	a = 0;
	while (isdigit(c)) {
		a = a * 10 + (c & 0xf);
		c = getchar();
	}
	a *= flag;
}

struct pot {
	int x, y, z;
	pot() {}
	pot(int x, int y, int z) : x(x), y(y), z(z) {}
};

int n, h, r;
pot p[1005];
int dis[1005];
int vis[1005];
vector<int> g[1005];

long long dist(int a, int b) {
	long long dx = p[a].x - p[b].x;
	long long dy = p[a].y - p[b].y;
	long long dz = p[a].z - p[b].z;
	return dx * dx + dy * dy + dz * dz;
}

void add_edge(int st, int to) {
	g[st].push_back(to);
	g[to].push_back(st);
}

bool spfa() {
	memset(dis, 0x7f, sizeof(dis));
	memset(vis, 0, sizeof(vis));
	queue<int> q;
	q.push(0);
	vis[0] = 1;
	dis[0] = 0;
	while (!q.empty()) {
		int temp = q.front();
		q.pop();
		vis[temp] = 0;
		for (unsigned i = 0; i < g[temp].size(); i++) {
			if (dis[temp] + 1 < dis[g[temp][i]]) {
				dis[g[temp][i]] = dis[temp] + 1;
				if (!vis[g[temp][i]]) {
					vis[g[temp][i]] = 1;
					q.push(g[temp][i]);
				}
			}
		}
	}
	if (dis[n + 1] != 0x7f7f7f7f) return true;
	else return false;
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T;
	get(T);
	while (T--) {
		memset(p, 0, sizeof(p));
		for (int i = 0; i <= n + 1; i++)
			g[i].clear();
		get(n);
		get(h);
		get(r);
		for (int i = 1; i <= n; i++) {
			get(p[i].x);
			get(p[i].y);
			get(p[i].z);
			if (p[i].z <= r)
				add_edge(0, i);
			if (p[i].z + r >= h)
				add_edge(i, n + 1);
		}
		for (int i = 1; i <= n; i++) {
			for (int j = i + 1; j <= n; j++) {
				long long ret = dist(i, j);
				if (ret <= ((long long)r * r) << 2) {
					add_edge(i, j);
				}
			}
		}
		printf("%s\n", spfa() == true ? "Yes" : "No");
	}
	return 0;
}

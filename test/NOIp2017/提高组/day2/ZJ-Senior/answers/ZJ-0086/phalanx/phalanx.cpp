#include <cstdio>
#include <cctype>
#include <map>
#include <utility>
#include <vector>
#include <algorithm>
using std::upper_bound;
using std::make_pair;
using std::vector;
using std::map;

template <typename T>
inline void get(T& a) {
	char c = getchar();
	while (!isdigit(c)) c = getchar();
	a = 0;
	while (isdigit(c)) {
		a = a * 10 + (c & 0xf);
		c = getchar();
	}
}

struct lin {
  vector<int> del;
  map<int, int> ins;
};

int n, m, q;
int p[1005][1005];
lin px;

int num(int x, int y) {
	return (x - 1) * m + y;
}

int main() {
  freopen("phalanx.in", "r", stdin);
  freopen("phalanx.out", "w", stdout);
	get(n);
	get(m);
	get(q);
  if (n == 1) {
    for (int i = 1; i <= q; i++) {
      int x, y;
      get(x);
      get(y);
      int ret;
		if (m - y >= px.ins.size()) {
			ret = num(x, y) + (upper_bound(px.del.begin(), px.del.end(), y) - px.del.begin());
		} else {
			ret = px.ins[px.ins.size() - 1 - m + y];
		}
		if (m - y > px.ins.size()) {
			px.del.push_back(y);
		} else {
      map<int, int>::iterator it = px.ins.find(px.ins.size() - 1 - m + y);
      if (it != px.ins.end()) {
        px.ins.erase(it);
      }
		}
		px.ins.insert(make_pair(px.ins.size(), ret));
    printf("%d\n", ret);
    }
    return 0;
  }
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      p[i][j] = num(i, j);
  for (int i = 1; i <= q; i++) {
    int x, y;
    get(x);
    get(y);
    printf("%d\n", p[x][y]);
    p[x][m + 1] = p[x][y];
    for (int j = y; j <= m; j++)
      p[x][j] = p[x][j + 1];
    p[n + 1][m] = p[x][m];
    for (int j = x; j <= n; j++)
      p[j][m] = p[j + 1][m];
  }
  return 0;
}

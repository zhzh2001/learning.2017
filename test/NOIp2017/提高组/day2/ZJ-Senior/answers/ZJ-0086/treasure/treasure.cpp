#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
#include <vector>
#include <functional>
#include <queue>
using std::min;
using std::vector;

template <typename T>
inline void get(T& a) {
	char c = getchar();
	while (!isdigit(c)) c = getchar();
	a = 0;
	while (isdigit(c)) {
		a = a * 10 + (c & 0xf);
		c = getchar();
	}
}

struct edg {
	int st, to, val;
	edg() {}
	edg(int st, int to, int val) : st(st), to(to), val(val) {}
};

int n, m;
vector<edg> g[20];
int vis[20];

struct comp {
	bool operator()(const edg& a, const edg& b) {
		return a.val > b.val;
	}
};

long long prim(int idx) {
	memset(vis, 0, sizeof(vis));
	int leftcnt = n;
	long long ret = 0;
	std::priority_queue<edg, vector<edg>, comp> q;
	vis[idx] = 1;
	leftcnt--;
	for (unsigned i = 0; i < g[idx].size(); i++)
		q.push(g[idx][i]);
	while (!q.empty() && leftcnt) {
		edg temp = q.top();
		q.pop();
		if (vis[temp.to]) continue;
		int to = temp.to;
		vis[to] = vis[temp.st] + 1;
		ret += temp.val;
		leftcnt--;
		for (unsigned i = 0; i < g[to].size(); i++)
			q.push(edg(to, g[to][i].to, g[to][i].val * vis[to]));
	}
	if (leftcnt) return 1ll << 30;
	else return ret;
}

void add_edge(int st, int to, int val) {
	g[st].push_back(edg(st, to, val));
	g[to].push_back(edg(to, st, val));
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	get(n);
	get(m);
	for (int i = 1; i <= m; i++) {
		int st, to, val;
		get(st);
		get(to);
		get(val);
		add_edge(st, to, val);
	}
	long long ans = 1ll << 60;
	for (int i = 1; i <= n; i++)
		ans = min(ans, prim(i));
	printf("%lld\n", ans);
	return 0;
}

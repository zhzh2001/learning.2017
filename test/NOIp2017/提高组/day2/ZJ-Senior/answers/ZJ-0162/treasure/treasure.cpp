#include<bits/stdc++.h>
using namespace std;
#define bianshu 2005
#define chang (1<<11)+(1<<10)
inline int read(){
	int lin=0;
	char x=getchar();
	while(x<'0' || x>'9') x=getchar();
	while(x>='0' && x<='9'){
		lin=(lin<<1)+(lin<<3)+x-'0';
		x=getchar();
	}
	return lin;
}
inline void write(int a){
	int lin=10,bin=1;
	while(lin<=a){
		lin*=10;
		++bin;
	}
	while(bin--){
		lin/=10;
		putchar('0'+a/lin);
		a%=lin;
	}
}
struct student{
	int meng,zhong,zhi;
}yuanzhan[bianshu];
int i,j,n,m,chu,mo,shu,x,y,zong,lin,mubiao,in=1e9;
int dp[bianshu],zhi[bianshu];
int dui[chang],tou[bianshu],gongxian[bianshu],jilu[bianshu];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();
	m=read();
	for(i=1;i<=m;++i){
		x=read();
		y=read();
		shu=read();
		++zong;
		yuanzhan[zong].meng=tou[x];
		yuanzhan[zong].zhong=y;
		yuanzhan[zong].zhi=shu;
		tou[x]=zong;
		++zong;
		yuanzhan[zong].meng=tou[y];
		yuanzhan[zong].zhi=shu;
		yuanzhan[zong].zhong=x;
		tou[y]=zong;
	}
	for(i=1;i<=n;i++){
		memset(gongxian,0,sizeof(gongxian));
		dp[i]=1<<(i-1);
		zhi[i]=0;
		dui[1]=i;
		gongxian[i]=0;
		jilu[i]=1;
		chu=0;
		mo=1;
		while(chu!=mo){
			++chu;
			lin=dui[chu];
			for(j=tou[lin];j;j=yuanzhan[j].meng){
				mubiao=yuanzhan[j].zhong;
				if (!(dp[i]&(1<<(mubiao-1)))){
					++mo;
					dui[mo]=mubiao;
					jilu[mubiao]=jilu[lin]+1;
					gongxian[mubiao]=jilu[lin]*yuanzhan[j].zhi;
					dp[i]+=1<<mubiao-1;
					zhi[i]+=gongxian[mubiao];
				}
				else{
					if (gongxian[mubiao]>jilu[lin]*yuanzhan[j].zhi){
						++mo;
						dui[mo]=mubiao;
						jilu[mubiao]=jilu[lin]+1;
						zhi[i]-=gongxian[mubiao]-jilu[lin]*yuanzhan[j].zhi;
						gongxian[mubiao]=jilu[lin]*yuanzhan[j].zhi;
					}
				}
			}
		}
		in=min(in,zhi[i]);
	}
	cout<<in;
	return 0;
}

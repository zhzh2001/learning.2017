#include<cstdio>
const int N(20),inf(1e9);
int n,m,ans(inf);
int e[N][N];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j,k,u,v,w;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
			if(i!=j)
				e[i][j]=inf;
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d",&u,&v,&w);
		if(e[u][v]==inf)
			e[u][v]=1;
		if(e[v][u]==inf)
			e[v][u]=1;
	}
	for(k=1;k<=n;k++)
		for(i=1;i<=n;i++)
			for(j=1;j<=n;j++)
				if(e[i][j]>e[i][k]+e[k][j])
					e[i][j]=e[i][k]+e[k][j];
	for(i=1;i<=n;i++)
	{
		for(j=1;j<=n;j++)
			e[i][0]+=e[i][j];
		if(ans>e[i][0])
			ans=e[i][0];
	}
	printf("%d\n",ans*w);
	return 0;
}

#include<cstdio>
#include<cmath>
typedef long long ll;
const ll N(1005);
const double eps(1e-8);
ll Case,n,h,r;
ll x[N],y[N],z[N],dis[N];
inline ll read()
{
	ll sum(0),f(1);
	char x;
	while((x=getchar())<'0'||x>'9')
		f=(x=='-'?-1:f);
	for(;x>='0'&&x<='9';x=getchar())
		sum=(sum<<3)+(sum<<1)+x-'0';
	return sum*f;
}
inline bool neighbor(ll a,ll b)
{
	return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]))<=r*2+eps;
}
inline ll find(ll v)
{
	if(dis[v]==v)
		return v;
	return dis[v]=find(dis[v]);
}
inline void merge(ll u,ll v)
{
	dis[find(u)]=dis[find(v)];
	return;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	ll i,j,go;
	Case=read();
	while(Case--)
	{
		n=read();h=read();r=read();
		for(i=0;i<=n+1;i++)
			dis[i]=i;
		for(i=1;i<=n;i++)
		{
			x[i]=read();y[i]=read();z[i]=read();
			if(z[i]<=r)
				merge(0,i);
			if(z[i]>=h-r)
				merge(i,n+1);
			for(j=1;j<i;j++)
				if(neighbor(i,j))
					merge(i,j);
		}
		puts(find(0)==find(n+1)?"Yes":"No");
	}
	return 0;
}

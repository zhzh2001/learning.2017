#include<cstdio>
#include<climits>
typedef long long ll;
const ll N(2005);
ll n,m,q,tot;
ll mp[N][N];
struct node
{
	ll val,key,size,prio;
	node *p,*l,*r;
}*root,*r2;
inline ll random()
{
	static ll seed(140404);
	return seed=(seed*48271ll%INT_MAX);
}
void left_rotate(node *x)
{
	ll k;
	node *y=x->r;
	y->p=x->p;
	if(!x->p)
		root=y;
	else if(x==x->p->l)
		x->p->l=y;
	else
		x->p->r=y;
	x->r=y->l;
	if(y->l)
		y->l->p=x;
	y->l=x;
	x->p=y;
	k=x->size;
	x->size-=y->size-(x->r?x->r->size:0);
	y->size=k;
	return;
}
void right_rotate(node *x)
{
	ll k;
	node *y=x->l;
	y->p=x->p;
	if(!x->p)
		root=y;
	else if(x==x->p->l)
		x->p->l=y;
	else
		x->p->r=y;
	x->l=y->r;
	if(y->r)
		y->r->p=x;
	y->r=x;
	x->p=y;
	k=x->size;
	x->size-=y->size-(x->l?x->l->size:0);
	y->size=k;
	return;
}
void insert(ll x)
{
	node *y(root),*q=new node;
	q->val=x;q->key=++tot;
	q->size=1;
	q->prio=random();
	q->p=q->l=q->r=NULL;
	if(!y)
	{
		root=q;
		return;
	}
	y->size++;
	while(y->r)
		y=y->r,y->size++;
	y->r=q;
	q->p=y;
	while(q->p&&q->prio<q->p->prio)
	{
		if(q==q->p->l)
			right_rotate(q->p);
		else
			left_rotate(q->p);
	}
	return;
}
ll erase(ll k)
{
	ll a(0),res(-1);
	node *x(root);
	while(true)
	{
		if(a+(x->l?x->l->size:0)+1==k)
		{
			while(x->l||x->r)
			{
				if(x->l&&(!x->r||x->l->prio<x->r->prio))
					right_rotate(x),x->p->size--;
				else
					left_rotate(x),x->p->size--;
			}
			res=x->val;
			if(!x->p)
				root=NULL;
			else if(x==x->p->l)
				x->p->l=NULL;
			else
				x->p->r=NULL;
			delete x;
			return res;
		}
		else if(a+x->size-(x->r?x->r->size:0)<k)
			a+=x->size-(x->r?x->r->size:0),x->size--,x=x->r;
		else
			x->size--,x=x->l;
	}
	return res;
}
void left_rotate2(node *x)
{
	ll k;
	node *y=x->r;
	y->p=x->p;
	if(!x->p)
		r2=y;
	else if(x==x->p->l)
		x->p->l=y;
	else
		x->p->r=y;
	x->r=y->l;
	if(y->l)
		y->l->p=x;
	y->l=x;
	x->p=y;
	k=x->size;
	x->size-=y->size-(x->r?x->r->size:0);
	y->size=k;
	return;
}
void right_rotate2(node *x)
{
	ll k;
	node *y=x->l;
	y->p=x->p;
	if(!x->p)
		r2=y;
	else if(x==x->p->l)
		x->p->l=y;
	else
		x->p->r=y;
	x->l=y->r;
	if(y->r)
		y->r->p=x;
	y->r=x;
	x->p=y;
	k=x->size;
	x->size-=y->size-(x->l?x->l->size:0);
	y->size=k;
	return;
}
void insert2(ll x)
{
	node *y(r2),*q=new node;
	q->val=x;q->key=++tot;
	q->size=1;
	q->prio=random();
	q->p=q->l=q->r=NULL;
	if(!y)
	{
		r2=q;
		return;
	}
	y->size++;
	while(y->r)
		y=y->r,y->size++;
	y->r=q;
	q->p=y;
	while(q->p&&q->prio<q->p->prio)
	{
		if(q==q->p->l)
			right_rotate2(q->p);
		else
			left_rotate2(q->p);
	}
	return;
}
ll erase2(ll k)
{
	ll a(0),res(-1);
	node *x(r2);
	while(true)
	{
		if(a+(x->l?x->l->size:0)+1==k)
		{
			while(x->l||x->r)
			{
				if(x->l&&(!x->r||x->l->prio<x->r->prio))
					right_rotate2(x),x->p->size--;
				else
					left_rotate2(x),x->p->size--;
			}
			res=x->val;
			if(!x->p)
				r2=NULL;
			else if(x==x->p->l)
				x->p->l=NULL;
			else
				x->p->r=NULL;
			delete x;
			return res;
		}
		else if(a+x->size-(x->r?x->r->size:0)<k)
			a+=x->size-(x->r?x->r->size:0),x->size--,x=x->r;
		else
			x->size--,x=x->l;
	}
	return res;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	ll i,j,x,y,check;
	node *a;
	scanf("%lld%lld%lld",&n,&m,&q);
	if(n<=2000&&m<=2000)
	{
		for(i=1;i<=n;i++)
			for(j=1;j<=m;j++)
				mp[i][j]=(i-1)*m+j;
		while(q--)
		{
			scanf("%lld%lld",&x,&y);
			printf("%lld\n",mp[x][y]);
			check=mp[x][y];
			for(j=y;j<m;j++)
				mp[x][j]=mp[x][j+1];
			for(i=x;i<n;i++)
				mp[i][m]=mp[i+1][m];
			mp[n][m]=check;
		}
	}
	else if(n==1)
	{
		for(i=1;i<=m;i++)
			insert(i);
		while(q--)
		{
			scanf("%lld%lld",&x,&y);
			x=erase(y);
			printf("%lld\n",x);
			insert(x);
		}
	}
	else
	{
		for(i=1;i<=m;i++)
			insert(i);
		for(i=m;i<=n*m;i+=m)
			insert2(i);
		while(q--)
		{
			scanf("%lld%lld",&x,&y);
			x=erase(y);
			printf("%lld\n",x);
			erase2(1);
			a=r2;
			while(a->l)
				a=a->l;
			insert(a->val);
			insert2(x);
		}
	}
	return 0;
}

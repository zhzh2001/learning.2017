#include <iostream>
#include <cstdio>
#include <queue>
#include <cstring>
#include <algorithm>
using namespace std;
#define LLI long long
#define MAXM 1010
int n,m,t1,t2,t3,head[20],nxt[MAXM<<1],to[MAXM<<1],val[MAXM<<1],tot,deep[20];
LLI ans,Ans;
bool vis[20];
void addedge(int b,int e,int v){
	nxt[++tot]=head[b];head[b]=tot;
	to[tot]=e;val[tot]=v;
}
void dfs20(int u,int fa,int deep){
	for(int i=head[u];i;i=nxt[i])
		if(to[i]!=fa){
			ans+=1LL*val[i]*deep;
			dfs20(to[i],u,deep+1);
		}
}
void bfs20(int u){
	queue<int>q;
	memset(vis,0,sizeof vis);
	memset(deep,0,sizeof deep);
	q.push(u);vis[u]=true;deep[u]=1;
	while(!q.empty()){
		int t=q.front();q.pop();
		for(int i=head[t];i;i=nxt[i])
			if(!vis[to[i]]){
				vis[to[i]]=true;
				deep[to[i]]=deep[t]+1;
				q.push(to[i]);
				ans+=1LL*deep[t]*val[i];
			}
	}
}
int main(){
	freopen("treasure.in","r+",stdin);
	freopen("treasure.out","w+",stdout);
	//bool flag=true;int tt;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;++i){
		scanf("%d%d%d",&t1,&t2,&t3);
		//if(i!=1){if(tt!=t3)flag=false;}
		addedge(t1,t2,t3);
		addedge(t2,t1,t3);
		//tt=t3;
	}
	if(m==n-1){//20
		Ans=0x3f3f3f3f3f3f3f3fLL;
		for(int i=1;i<=n;++i){
			ans=0;
			dfs20(i,0,1);
			Ans=min(Ans,ans);
		}
		printf("%lld\n",Ans);
	}else{// if(flag){//20
		Ans=0x3f3f3f3f3f3f3f3fLL;
		for(int i=1;i<=n;++i){
			ans=0;
			bfs20(i);
			Ans=min(Ans,ans);
		}
		printf("%lld\n",Ans);
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;
#define MAXN 1010
#define LLI long long
int T;
struct Point{
	int x,y,z;
}point[MAXN];
int n,h,r;
bool vis[MAXN];
inline LLI pows(int x){return 1LL*x*x;}
inline bool can(int a,int b){
	return pows(point[a].x-point[b].x)+pows(point[a].y-point[b].y)
	+pows(point[a].z-point[b].z)<=(pows(r)<<2LL);
}
int main(){
	freopen("cheese.in","r+",stdin);
	freopen("cheese.out","w+",stdout);
	scanf("%d",&T);
	size_t SVI=sizeof vis;
	queue<int>q;
	while(T--){
		memset(vis,0,SVI);
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;++i){
			scanf("%d%d%d",&point[i].x,&point[i].y,&point[i].z);
			if(point[i].z<=r){q.push(i);vis[i]=true;}
		}
		bool flag=false;
		while(!q.empty()){
			int t=q.front();q.pop();
			if(h-point[t].z<=r){
				flag=true;
				while(!q.empty())q.pop();
				break;
			}
			for(int i=1;i<=n;++i)
				if(!vis[i]&&can(t,i)){
					q.push(i);
					vis[i]=true;
				}
		}
		puts(flag?"Yes":"No");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

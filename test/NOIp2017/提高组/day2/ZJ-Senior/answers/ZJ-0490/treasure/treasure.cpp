#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,m,x,y,z,tot,ans,sum,dep[15];
bool a[15][15];
int head,tail,q[100];
void BFS(int st){
	q[1]=st; head=tail=1;
	while (head<=tail){
		int u=q[head++];
		for (int i=1;i<=n;i++)
			if (a[u][i]&&dep[i]==(1<<10)){
				dep[i]=dep[u]+1; q[++tail]=i; 
			}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m); ans=1<<20;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=1; tot=z;
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) dep[j]=1<<10;
		dep[i]=1; BFS(i); sum=0;
		for (int j=1;j<=n;j++) sum+=dep[j]-1;
		ans=min(ans,sum);
	}
	printf("%d",ans*tot);
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#define ll long long
using namespace std;
ll shu[900010],numb[510][50010],t1;
int n,m,Q,a[300010],b[300010],num,t;
int tt[300010],vet[300010];
int ls[1800010],rs[1800010],sum[1800010],l[1800010],r[1800010];
bool flag;
void read(int &x){
	int sign=1; char ch=getchar(); x=0;
	while (ch<'0'||ch>'9'){if (ch=='-') sign=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	x*=sign;
}
void build(int le,int ri){
	l[num]=le; r[num]=ri;
	if (le==ri){
		if (le<=n+m-1) sum[num]=1; else sum[num]=0;
		return;
	}
	int u=num,mid=(ri+le)>>1;
	num++; ls[u]=num; build(le,mid);
	num++; rs[u]=num; build(mid+1,ri);
	sum[u]=sum[ls[u]]+sum[rs[u]];
}
void update(int u,int x,int y){ 
	sum[u]+=y;
	if (ls[u]==rs[u]) return;
	if (x<=((l[u]+r[u])>>1)) update(ls[u],x,y); else update(rs[u],x,y);
}
int query(int u,int x){
	if (l[u]==r[u]) return u;
	if (sum[ls[u]]>=x) return query(ls[u],x);
	else return query(rs[u],x-sum[ls[u]]);
}
inline bool cmp(int t1,int t2){
	return t1<t2;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n); read(m); read(Q); flag=1;
	for (int i=1;i<=Q;i++) read(a[i]),read(b[i]),flag=(flag&(a[i]==1));
	if (flag){
		num++; build(1,m+n-1+Q);
		for (int i=1;i<=m;i++) shu[i]=i;
		for (int i=2;i<=n;i++) shu[i+m-1]=(ll)i*m;
		for (int i=1;i<=Q;i++){
			t=query(1,b[i]); update(1,t,-1); update(1,n+m+i,1);
			shu[n+m+i]=shu[t]; printf("%lld\n",shu[t]);
		}
	}else{
		for (int i=1;i<=Q;i++) tt[i]=a[i];
		sort(tt+1,tt+Q+1,cmp); num=1;
		for (int i=2;i<=Q;i++)
			if (tt[i]!=tt[num]) tt[++num]=tt[i];
		tt[++num]=n;
		for (int i=1;i<=num;i++) vet[tt[i]]=i;
		for (int i=1;i<=num;i++)
			for (int j=1;j<=m;j++) numb[i][j]=(ll)(tt[i]-1)*m+j;
		for (int i=1;i<=n;i++) shu[i]=(ll)i*m;
		for (int i=1;i<=Q;i++){
			t1=numb[vet[a[i]]][b[i]]; printf("%lld\n",t1);
			for (int j=b[i];j<m;j++) numb[vet[a[i]]][j]=numb[vet[a[i]]][j+1];
			numb[vet[a[i]]][m]=shu[vet[a[i]]+1];
			for (int j=vet[a[i]];j<n;j++) shu[j]=shu[j+1];
			shu[n]=t1; numb[num][m]=t1;
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;
int T,n,h,r,fa[1010],x[1010],y[1010],z[1010],fa1,fa2;
bool flag[1010],ans;
void read(int &x){
	int sign=1; char ch=getchar(); x=0;
	while (ch<'0'||ch>'9'){if (ch=='-') sign=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	x*=sign;
}
bool check(int a,int b){
	return ((sqrt((double)((ll)(x[a]-x[b])*(x[a]-x[b])+(ll)(y[a]-y[b])*(y[a]-y[b])+(ll)(z[a]-z[b])*(z[a]-z[b]))))<=(r<<1));
}
int find(int x){
	if (fa[x]!=x) fa[x]=find(fa[x]);
	return fa[x];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--){
		read(n); read(h); read(r); ans=0;
		memset(flag,0,sizeof(flag));
		for (int i=1;i<=n;i++) read(x[i]),read(y[i]),read(z[i]),fa[i]=i;
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (check(i,j)){
					fa1=find(i); fa2=find(j);
					if (fa1!=fa2) fa[fa2]=fa1;
				}
		for (int i=1;i<=n;i++)
			if (z[i]<=r){
				fa1=find(i); flag[fa1]=1;
			}
		for (int i=1;i<=n;i++)
			if (z[i]+r>=h){
				fa1=find(i);
				if (flag[fa1]){
					ans=1; break;
				}
			}
		if (ans) printf("Yes\n"); else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

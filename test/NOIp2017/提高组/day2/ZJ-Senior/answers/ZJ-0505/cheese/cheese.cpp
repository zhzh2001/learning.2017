#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
#define MAXN 1010
int fa[MAXN+10];
int find(int x)
{
	if (x==fa[x]) return x;
	return fa[x]=find(fa[x]);
}
void Union(int x,int y)
{
	int fx=find(x),fy=find(y);
	fa[fx]=fy;
}
struct Data
{
	LL x,y,z;
}a[MAXN+10];
inline LL sqrr(LL x)
{
	return x*x;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	for(int ii=1; ii<=T; ++ii)
	{
		int n;
		LL h,r;
		scanf("%d%lld%lld",&n,&h,&r);
		for(int i=1; i<=n+2; ++i) fa[i]=i;
		for(int i=1; i<=n; ++i)
		{
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		}
		int s=n+1,t=n+2;
		for(int i=1; i<=n; ++i)
		{
			if (abs(a[i].z)<=r) Union(i,s);
			if (a[i].z+r>=h && a[i].z-r<=h) Union(i,t);
		}
		LL dis=4*r*r;
		for(int i=1; i<=n; ++i)
		{
			for(int j=i+1; j<=n; ++j)
			{
				if (sqrr(a[i].x-a[j].x)+sqrr(a[i].y-a[j].y)+sqrr(a[i].z-a[j].z)<=dis)
				{
					Union(i,j);
				}
			}
		}
		if (find(s)==find(t)) puts("Yes");
		else puts("No");
	}
	return 0;
}

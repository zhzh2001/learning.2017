#include <bits/stdc++.h>
using namespace std;
#define MAXN 12
int ma[MAXN+5][MAXN+5];
bool vis[MAXN+5];
int dis[MAXN+5];
int n,m;
int ans=INT_MAX;
int mindis[MAXN+5];
int minEdge[MAXN+5];
int getH()
{
	int ans=0;
	for(int i=1; i<=n; ++i)
	{
		if (vis[i])
		{
			ans+=minEdge[i]*1;
		}
	}
	return ans;
}
void dfs(int cnt,int nowans)
{
	if (cnt>=n) 
	{
		ans=min(ans,nowans);
		return ;
	}
	if (nowans>=ans) return;
	if (nowans+getH()>=ans) return;
	int Min=INT_MAX;
	for(int i=1; i<=n; ++i)
	{
		for(int j=1; j<=n; ++j)
		{
			if (i==j) continue;
			if((vis[j] && !vis[i]) && ma[i][j]!=-1)
			{
				vis[j]=false;
				dis[j]=dis[i]+1;
				dfs(cnt+1,nowans+ma[i][j]*dis[i]);
				vis[j]=true;
			}
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=n; ++i)
	{
		for(int j=1; j<=n; ++j)
		{
			ma[i][j]=-1;
		}
	}
	memset(minEdge,0x3f,sizeof(minEdge));
	for(int i=1; i<=m; ++i)
	{
		int a,b,v;
		scanf("%d%d%d",&a,&b,&v);
		if (ma[a][b]==-1 || ma[a][b]>v)
		{
			ma[a][b]=v;
			ma[b][a]=v;
			minEdge[a]=min(minEdge[a],v);
			minEdge[b]=min(minEdge[b],v);
		}
	}
	memset(vis,true,sizeof(vis));
	for(int i=1; i<=n; ++i)
	{
		vis[i]=false;
		dis[i]=1;
		dfs(1,0);
		dis[i]=0;
		vis[i]=true;
	}
	printf("%d\n",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
int n,m,ans,vall;
int mp[14][14];
int oc[14][14];
bool vis[14][14],vis2[14];

void dfs1(int nd,int dp,int vlu)
{
	oc[nd][dp]=vlu<oc[nd][dp]?vlu:oc[nd][dp];
	for (int i=1;i<=n;i++)
	{
		if (!vis[nd][i]&&mp[nd][i]>0) 
		{
			vis[nd][i]=true;
			dfs1(nd,dp+1,vlu+dp*mp[nd][i]);
			vis[nd][i]=false;
		}
	}
}
/*
void dfs2(int nd,int dp,int ansi)
{
	for (int i=1;i<=n;i++)
	{
		if (!vis[nd][i]&&mp[nd][i]>0) 
		{
			vis2[nd]=true;
			dfs1(nd,dp+1,vlu+dp*mp[nd][i][1]);
			vis2[nd]=false;
		}
	}
}
*/
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(mp,0x7f,sizeof(mp));
	for (int i=1;i<=m;i++)
	{
		int st,ed,vl;
		scanf("%d%d%d",&st,&ed,&vl);
		mp[st][ed]=vl<mp[st][ed]?vl:mp[st][ed];
		mp[ed][st]=vl<mp[ed][st]?vl:mp[ed][st];
		vall=vl;
	}
	int mini=2147483647;
	for (int dcc=1;dcc<=n;dcc++)
	{
		ans=0;
		//memset(oc,0x7f,sizeof(oc));
		//memset(vis,false,sizeof(vis));
		//dfs1(dcc,1,0);
		memset(vis2,false,sizeof(vis2));
		int a[102]={0},hd=1,tl=1;
		a[1]=dcc;
		int k=0;
		while (hd<=tl)
		{
			k++;
			int ctl=tl;
			for (int i=hd;i<=tl;i++)
			{
				vis2[a[i]]=true;
				for (int j=1;j<=n;j++)
				{
					if (mp[a[i]][j]<7000000&&!vis2[j]) {ctl++; a[ctl]=j; ans+=k; vis2[j]=true;}
				}
			}
			tl=ctl;
			hd++;
		}
		if (ans*vall<mini) mini=ans*vall;
	}
	printf("%d\n",mini);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

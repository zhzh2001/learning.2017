#include<iostream>
#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
int n;
ll h,r;
ll a[1008][3];
int mp[1009][1009];
bool vis[1009];

void dfs1(int nd)
{
	vis[nd]=true;
	for (int i=1;i<=mp[nd][0];i++)
	{
		if (!vis[mp[nd][i]])
		{
			dfs1(mp[nd][i]);
		}
	}
}

ll dst2(int x,int y)
{
	return (a[x][0]-a[y][0])*(a[x][0]-a[y][0])+(a[x][1]-a[y][1])*(a[x][1]-a[y][1])+(a[x][2]-a[y][2])*(a[x][2]-a[y][2]);
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	for (int dcc=1;dcc<=t;dcc++)
	{
		memset(a,0,sizeof(a));
		memset(mp,0,sizeof(mp));
		memset(vis,false,sizeof(vis));
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&a[i][0],&a[i][1],&a[i][2]);
		}
		for (int i=1;i<=n;i++)
		{
			if (a[i][2]<=r)
			{
				mp[0][0]++;
				mp[0][mp[0][0]]=i;
				mp[i][0]++;
				mp[i][mp[i][0]]=0;
			}
			if (h-a[i][2]<=r)
			{
				mp[n+1][0]++;
				mp[n+1][mp[n+1][0]]=i;
				mp[i][0]++;
				mp[i][mp[i][0]]=n+1;
			}
		}
		for (int i=1;i<=n;i++)
		{
			for (int j=1;j<=n;j++)
			{
				if (i!=j&&dst2(i,j)<=4*r*r)
				{
					mp[i][0]++;
					mp[i][mp[i][0]]=j;
					mp[j][0]++;
					mp[j][mp[j][0]]=i;
				}
			}
		}
		dfs1(0);
		if (vis[n+1]) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

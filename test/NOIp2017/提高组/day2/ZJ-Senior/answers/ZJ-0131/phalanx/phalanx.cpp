#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <queue>
using namespace std;

const int maxn = 1010;
int n, m, q;
int map[maxn][maxn];
int event[maxn][2];
priority_queue<int> qu;


/*
void tepan1(){
	int tq[maxn];
	for(int i=1;i<=m;++i)tq[i]=i;
	int last = m;
	for(int i=1;i<=q;++i){
		int y;
		int out;
		y = event[i][1];
		
		int j=0;
		for(iterator<int> it = qu.begin();it!=qu.end();it++){
			y++;
			j++;
		}
		out = y;
		qu.push(out);
		cout << tq[out]<<endl;
		tq[++last]=out;
	}
}
*/

int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	cin >> n >> m >>q;
	for(int i=1;i<=q;++i){
		int x, y;
		cin>>x>>y;
		event[i][0]=x;
		event[i][1]=y;
	}
/*	if(n==1){
		tepan1();
		return 0;
	}
*/	for(int i = 1;i<=n;++i){
		for(int j = 1;j<=m;++j){
			map[i][j]=(i-1)*m+j;
		}
	}
	
	for(int i=1;i<=q;++i){
		int out = map[event[i][0]][event[i][1]];
		
		cout<<out<<endl; 
		
		for(int j = event[i][1];j<m;j++){
			map[event[j][0]][j]=map[event[j][0]][j+1];
		}
		
		for(int j = event[i][0];j<n;j++){
			map[j][m]=map[j+1][m];
		}
		map[n][m]=out;
	}
	return 0;
}

#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
typedef long long ll;
int n, t;
long long h, r;
const int maxn = 1010;
struct cheese {
	ll x, y, z;
} ch[maxn];
int fa[maxn];

inline int find(int c) {
	return fa[c]==c?c:find(fa[c]);

}

inline void unionn(int c1, int c2) {
	//cout<<find(c1)<<" "<<find(c2)<<endl;
	if(!(find(c1)==find(c2)))fa[c1]=fa[find(c1)]=c2;
}

inline double dist(cheese c1, cheese c2) {
	return sqrt((c1.x-c2.x)*(c1.x-c2.x)+(c1.y-c2.y)*(c1.y-c2.y)+(c1.z-c2.z)*(c1.z-c2.z));
}

inline bool qie(cheese c1, cheese c2) {
	if(dist(c1,c2)<=2*r+1e-8)return true;
	else return false;
}

void judge() {
	for(int i = 0; i<=n+2; ++i)fa[i]=i;

	for(int i=1; i<=n; ++i) {
		if(ch[i].z-r<=0) {
			unionn(i,n+1);
		}
		if(ch[i].z+r>=h) {
			unionn(i,n+2);
		}
		for(int j=i+1; j<=n; j++) {

			if(qie(ch[i],ch[j])) {
				unionn(i,j);
			}
		}
	}

	if(find(n+1)==find(n+2))cout<<"Yes"<<endl;
	else cout<<"No"<<endl;
}

int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);

	cin>>t;
	while(t--) {
		cin>>n >> h >> r;
		for(int i=1; i<=n; ++i) {
			ll x, y, z;
			cin>>x>>y>>z;
			ch[i].x=x;
			ch[i].y=y;
			ch[i].z=z;
		}
		judge();
	}
	return 0;
}

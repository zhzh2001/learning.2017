#include <iostream>
#include <algorithm>
#include <vector>
#include <memory.h>
#include <string.h>
using namespace std;
int n, m;
const int maxn = 20;
int map[maxn][maxn];
int ans;
bool vis[maxn];

void search(int x, int v, int w){
	//cout<<x<<" "<<v<<" "<<w<<endl;
	bool flag = false;
	vis[x]=true;

	for(int i=1;i<=n;++i){
		if((map[x][i])&&(!vis[i])){
			flag=true;
			//cout<< v* map[x][i]<<endl;
			search(i,v+1,w+v*map[x][i]);
			vis[i]=false;
		}
	}
	if(!flag){
		ans = min(ans,w);
		return;
	}
	
}

int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);

	cin>>n>>m;
	for(int i=0; i<m; ++i) {
		int x,y, z;
		cin>>x>>y>>z;
		if((!map[x][y])||z<map[x][y]) {
			map[x][y]=z;
			map[y][x]=z;
		}
	}
	
	ans=0x3f3f3f3f;

	for(int i=1;i<=n;++i){
		search(i,1,0);
		memset(vis,false, sizeof vis);
	}
	cout<<ans;
	return 0;
}

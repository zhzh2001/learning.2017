#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

const int maxn = 3e5;

int n, m, q, qx[maxn], qy[maxn];
long long begin[maxn];

namespace brute {
  vector<int> del[maxn];
  vector<long long> ins[maxn];
  vector<long long> col;

  long long Del(vector<int> &del, vector<long long> &ins,
                long long begin, int size, int pos) {
    if (size - del.size() <= pos) {
      int x = pos - (size - del.size());
      long long ans = ins[x];
      ins.erase(ins.begin() + x);
      return ans;
    }
    int low = -1, high = del.size() - 1;
    while (low < high) {
      int mid = high - (high - low >> 1);
      if (del[mid] - mid <= pos) {
        low = mid;
      } else {
        high = mid - 1;
      }
    }
    int x = pos + low + 1;
    long long ans = begin + x;
    del.insert(del.begin() + low + 1, x);
    return ans;
  }

  void Solve(void) {
    for (int i = 0; i < n; ++i) {
      begin[i] = (long long)i * m + 1;
      col.push_back((long long)i * m + m);
    }
    for (int i = 0; i < q; ++i) {
      int x = qx[i], y = qy[i];
      if (y == m - 1) {
        printf("%lld\n", col[x]);
        col.push_back(col[x]);
        col.erase(col.begin() + x);
      } else {
        long long id = Del(del[x], ins[x], begin[x], m - 1, y);
        printf("%lld\n", id);
        ins[x].push_back(col[x]);
        col.erase(col.begin() + x);
        col.push_back(id);
      }
    }
  }
}

namespace bit {
  const int maxnum = maxn + maxn;

  int numrow, lcol, rcol;
  int bitrow[maxnum];
  long long seqrow[maxnum], seqcol[maxnum];

  inline int Lowbit(int x) {
    return x & -x;
  }

  void Add(int *a, int i, int x) {
    for (; i < maxnum; i += Lowbit(i + 1)) {
      a[i] += x;
    }
  }

  int Sum(int *a, int i) {
    int sum = 0;
    for (; i >= 0; i -= Lowbit(i + 1)) {
      sum += a[i];
    }
    return sum;
  }

  int Find(int *a, int n, int k) {
    int low = 0, high = n;
    while (low < high) {
      int mid = low + high >> 1;
      if (Sum(a, mid) >= k + 1) {
        high = mid;
      } else {
        low = mid + 1;
      }
    }
    return low;
  }

  void Solve(void) {
    numrow = m - 1;
    lcol = 0, rcol = n;
    for (int i = 0; i < maxnum; ++i) {
      bitrow[i] = Lowbit(i + 1);
    }
    for (int i = 0; i < n; ++i) {
      seqcol[i] = (long long)i * m + m;
    }
    for (int i = 0; i < m - 1; ++i) {
      seqrow[i] = i + 1;
    }
    for (int i = 0; i < q; ++i) {
      int y = qy[i];
      if (y == m - 1) {
        printf("%lld\n", seqcol[rcol++] = seqcol[lcol++]);
      } else {
        int u = Find(bitrow, numrow, y);
        long long id = seqrow[u];
        printf("%lld\n", id);
        Add(bitrow, u, -1);
        seqrow[numrow++] = seqcol[lcol++];
        seqcol[rcol++] = id;
      }
    }
  }
}

int main(void) {
  freopen("phalanx.in", "r", stdin);
  freopen("phalanx.out", "w", stdout);
  scanf("%d%d%d", &n, &m, &q);
  bool mogic = true;
  for (int i = 0; i < q; ++i) {
    scanf("%d%d", qx + i, qy + i);
    --qx[i]; --qy[i];
    if (qx[i]) {
      mogic = false;
    }
  }
  if (mogic) {
    bit::Solve();
  } else {
    brute::Solve();
  }
  return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn = 12, inf = 0x3f3f3f3f;

int n, m, g[maxn][maxn];
int dist[1 << maxn][maxn];
long long dp[1 << maxn][maxn];

template <typename type>
inline void Update(type &a, type b) {
  if (a > b) {
    a = b;
  }
}

long long Dist(int s, int t) {
  long long sum = 0;
  for (int i = 0; i < n; ++i) {
    if (s >> i & 1) {
      sum += dist[t][i];
    }
  }
  return sum;
}

void Solve(void) {
  scanf("%d%d", &n, &m);
  memset(g, 0x3f, sizeof g);
  for (int i = 0; i < m; ++i) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    --u; --v;
    if (u == v) continue;
    Update(g[u][v], w);
    Update(g[v][u], w);
  }
  for (int u = 0; u < n; ++u) {
    for (int s = 1; s < 1 << n; ++s) {
      dist[s][u] = inf;
      for (int v = 0; v < n; ++v) {
        if (s >> v & 1) {
          Update(dist[s][u], g[u][v]);
        }
      }
    }
  }
  memset(dp, 0x3f, sizeof dp);
  for (int u = 0; u < n; ++u) {
    dp[1 << u][0] = 0;
  }
  int all = (1 << n) - 1;
  for (int d = 0; d + 1 < n; ++d) {
    for (int s = 1; s < all; ++s) {
      int s1 = all ^ s;
      for (int t = s1; t; t = (t - 1) & s1) {
        Update(dp[s | t][d + 1], dp[s][d] + (d + 1) * Dist(t, s));
      }
    }
  }
  long long ans = 0x3f3f3f3f3f3f3f3fLL;
  for (int d = 0; d < n; ++d) {
    Update(ans, dp[all][d]);
  }
  printf("%lld\n", ans);
}

int main(void) {
  freopen("treasure.in", "r", stdin);
  freopen("treasure.out", "w", stdout);
  Solve();
  return 0;
}

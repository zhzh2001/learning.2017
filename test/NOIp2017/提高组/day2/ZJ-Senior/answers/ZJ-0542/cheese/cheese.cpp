#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn = 1005, maxm = 1005000;

int n, cnt, head[maxn], next[maxm], to[maxm];
long long h, rad, x[maxn], y[maxn], z[maxn];
int que[maxn], l, r;
bool vis[maxn];

inline unsigned long long sqr(unsigned long long a) {
  return a * a;
}

void AddEdge(int u, int v) {
  to[cnt] = v;
  next[cnt] = head[u];
  head[u] = cnt++;
  to[cnt] = u;
  next[cnt] = head[v];
  head[v] = cnt++;
}

void Solve(void) {
  scanf("%d%lld%lld", &n, &h, &rad);
  for (int i = 0; i < n; ++i) {
    scanf("%lld%lld%lld", x + i, y + i, z + i);
  }

  cnt = 0;
  memset(head, -1, (n + 2) * sizeof(int));
  int bot = n, top = n + 1;

  for (int i = 0; i < n; ++i) {
    if (z[i] - rad <= 0) {
      AddEdge(bot, i);
    }
    if (z[i] + rad >= h) {
      AddEdge(top, i);
    }
  }
  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      unsigned long long a = sqr(abs(x[i] - x[j])) +
        sqr(abs(y[i] - y[j])) + sqr(abs(z[i] - z[j]));
      if (a <= sqr(rad << 1)) {
        AddEdge(i, j);
      }
    }
  }

  l = r = 0;
  que[r++] = bot;
  memset(vis, 0, (n + 2) * sizeof(bool));
  vis[bot] = true;
  while (l < r) {
    int u = que[l++];
    for (int e = head[u]; e != -1; e = next[e]) {
      int v = to[e];
      if (!vis[v]) {
        vis[v] = true;
        que[r++] = v;
      }
    }
  }

  puts(vis[top] ? "Yes" : "No");
}

int main(void) {
  freopen("cheese.in", "r", stdin);
  freopen("cheese.out", "w", stdout);
  int t;
  scanf("%d", &t);
  while (t--) {
    Solve();
  }
  return 0;
}

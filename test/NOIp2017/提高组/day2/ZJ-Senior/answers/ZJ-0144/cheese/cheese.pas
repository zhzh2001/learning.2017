var t,i,j,n,tt:longint;
r,h:qword;
ans:boolean;
x,y,z:array[1..1001] of extended;
lt:array[1..1001,1..1001] of boolean;
up,down,pd:array[1..1001] of boolean;

procedure in_;
begin
	assign(input,'cheese.in');
	reset(input);
	assign(output,'cheese.out');
	rewrite(output);
end;

procedure out_;
begin
	close(input);
	close(output);
end;

procedure read_;
begin
        read(n,h,r);
        fillchar(x,sizeof(x),0);
        fillchar(y,sizeof(y),0);
        fillchar(z,sizeof(z),0);
        fillchar(up,sizeof(up),false);
        fillchar(down,sizeof(down),false);
        fillchar(lt,sizeof(lt),false);
        fillchar(pd,sizeof(pd),false);
        ans:=false;
        for i:=1 to n do
        begin
         read(x[i],y[i],z[i]);
         if z[i]-r<=0 then down[i]:=true;
         if z[i]+r>=h then up[i]:=true;
        end;
end;

function jl(c,v:longint):extended;
begin
        jl:=sqrt(sqr(x[c]-x[v])+sqr(y[c]-y[v])+sqr(z[c]-z[v]));
end;

procedure ltpd;
begin
        for i:=1 to n do
         for j:=1 to n do
         if jl(i,j)<=2*r then
         begin
         lt[i,j]:=true;
         lt[j,i]:=true;
         end;
end;

procedure dfs(k:longint);
begin
        pd[k]:=true;   
        if down[k] then
        begin
         ans:=true;
         exit;
        end;
        for j:=1 to n do
        begin

         if (lt[k,j]=true)and(pd[j]=false)and(j<>k) then
         dfs(j);
        end;
end;

begin
	in_;
        read(t);
        for tt:=1 to t do
        begin
         read_;
         ltpd;
         for i:=1 to n do
         begin
         if up[i] then dfs(i);
         if ans then break;
         end;

         if ans then writeln('Yes') else writeln('No');
        end;

	out_;
end.

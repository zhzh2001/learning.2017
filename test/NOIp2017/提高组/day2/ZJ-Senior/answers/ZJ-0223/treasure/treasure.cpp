#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 100
#define ll long long
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,di,ans=inf,vis[N],dis[N],f[N][N],g[N][N];
ll work(ll S){
	ll num=0;
	memset(vis,0,sizeof vis);
	rep(i,1,n) dis[i]=10000; dis[S]=0;
	rep(i,1,n){
		ll id=0;
		rep(j,1,n) if (!vis[j]&&(!id||dis[j]<dis[id])) id=j;
		num+=dis[id]; vis[id]=1;
		rep(j,1,n) if (f[id][j]&&!vis[j]) dis[j]=min(dis[j],(dis[id]+1)*g[id][j]);
	}
	return num;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) rep(j,1,n) g[i][j]=100000000;
	rep(i,1,m){
		ll u=read(),v=read(),w=read();
		f[u][v]=f[v][u]=1;
		g[u][v]=g[v][u]=min(g[u][v],w);
	}
	rep(i,1,n) ans=min(ans,work(i));
	printf("%lld",ans);
}

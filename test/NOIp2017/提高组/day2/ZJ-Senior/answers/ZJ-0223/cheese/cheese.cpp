#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 1100
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,n,h,r,x[N],y[N],z[N],fa[N];
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
void merge(ll x,ll y){
	ll p=find(x),q=find(y);
	if (p!=q) fa[p]=q;
}
double sqr(double x) { return x*x; }
bool ok(ll i,ll j){
	double dis;
	dis=sqrt(sqr(double(x[i]-x[j]))+sqr(double(y[i]-y[j]))+sqr(double(z[i]-z[j])));
	return dis<=2.0*r;
}
void work(){
	n=read(); h=read(); r=read();
	rep(i,1,n+2) fa[i]=i;
	rep(i,1,n){
		x[i]=read(); y[i]=read(); z[i]=read();
		rep(j,1,i-1) if (ok(j,i)) merge(j,i);
		if (z[i]+r>=h&&z[i]-r<=h) merge(i,n+1);
		if (z[i]+r>=0&&z[i]-r<=0) merge(i,n+2);
	}
	puts(find(n+1)==find(n+2)?"Yes":"No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read(); while (T--) work();
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define N 300005
#define ll long long
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,q,x,y,tmp,c[N],nxt[N],pre[N];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if (n<=1000&&m<=1000){
		ll num[n+10][m+10];
		rep(i,1,n) rep(j,1,m) num[i][j]=(i-1)*m+j;
		rep(i,1,q){
			x=read(),y=read(),tmp=num[x][y];
			printf("%lld\n",tmp);
			rep(j,y,m-1) num[x][j]=num[x][j+1];
			rep(i,x,n-1) num[i][m]=num[i+1][m];
			num[n][m]=tmp;
		}
	}else if (n==1){
		rep(i,0,m-1) nxt[i]=i+1;
		rep(i,1,m) pre[i]=i-1;
		ll last=m;
		rep(i,1,q){
			x=read(),y=read(); ll ans=0;
			rep(j,1,y) ans=nxt[ans];
			printf("%lld\n",ans);
			nxt[pre[ans]]=nxt[ans];
			pre[nxt[ans]]=pre[ans];
			nxt[last]=ans; pre[ans]=last;
			last=ans;
		}
	}
}

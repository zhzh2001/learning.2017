var x,y,z,st,en:array[0..1001]of longint;
    f:array[0..1001,0..1001]of integer;
    went:array[0..1001]of boolean;
    t,n,h,r,i,j:longint;
procedure run(i:longint);
var j:longint;
begin
  went[i]:=true;
  for j:=1 to f[i][0] do
    if not went[f[i][j]] then run(f[i][j]);
end;
begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  read(t);
  while t>0 do
    begin
      dec(t);read(n,h,r);for i:=1 to n do read(x[i],y[i],z[i]);
      fillchar(st,sizeof(st),0);
      fillchar(en,sizeof(en),0);
      fillchar(f,sizeof(f),0);
      for i:=1 to n do
        begin
          for j:=1 to n do
            if i<>j then
              begin
                if sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])>4*r*r then continue;
                inc(f[i][0]);
                f[i][f[i][0]]:=j;
              end;
          if abs(z[i])<=r then begin inc(st[0]);st[st[0]]:=i;end;
          if abs(z[i]-h)<=r then begin inc(en[0]);en[en[0]]:=i;end;
        end;
      for i:=1 to st[0] do
        begin
          fillchar(went,sizeof(went),false);run(st[i]);
          for j:=1 to en[0] do
            if went[en[j]] then begin went[0]:=true;break;end;
          if went[0] then break;
        end;
      if went[0] then writeln('Yes') else writeln('No');
    end;
  close(input);close(output);
end.
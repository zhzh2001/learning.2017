var a:array[0..20,0..20]of longint;
    b,c:array[0..20]of longint;
    used:array[0..20]of boolean;
    n,m,i,x,y,z,ans:longint;
function calc(t:longint):longint;
var i:longint;
begin
  calc:=maxlongint;
  if t=1 then begin c[t]:=1;exit(0);end;
  for i:=1 to t-1 do
    if c[i]*a[b[i]][b[t]]<calc then
      begin
        calc:=c[i]*a[b[i]][b[t]];
        c[t]:=c[i]+1;
      end;
end;
procedure form(t,sum:longint);
var i:longint;
begin
  if sum>=ans then exit;
  if t>n then begin ans:=sum;exit;end;
  for i:=1 to n do
    if not used[i] then
      begin
        used[i]:=true;
        b[t]:=i;
        c[t]:=0;
        form(t+1,sum+calc(t));
        c[t]:=0;
        b[t]:=0;
        used[i]:=false;
      end;
end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  read(n,m);for x:=0 to n do for y:=0 to n do a[x][y]:=$777777F;for i:=1 to m do begin read(x,y,z);if z>a[x][y] then continue;a[x][y]:=z;a[y][x]:=z;end;ans:=maxlongint;
  form(1,0);
  writeln(ans);
  close(input);close(output);
end.
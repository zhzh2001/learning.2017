#include<bits/stdc++.h>
using namespace std;
struct node
{
	double xi,yi,zi;
}
dlk[1005];
int mp(node a,node b)
{
	return a.zi<b.zi;
}
vector <int > f[1005];
int t,n,h,r,R,temp,head,tail,ksl[10005];
bool yzh[1005],pks[1005],zlb;
double tmp;
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for (int k=1;k<=t;k++)
	{
		scanf("%d%d%d",&n,&h,&r);
		R=2*r;
		for (int i=1;i<=n;i++)
			scanf("%lf%lf%lf",&dlk[i].xi,&dlk[i].yi,&dlk[i].zi);
		for (int i=1;i<=n;i++)
			f[i].clear();
		sort(dlk+1,dlk+n+1,mp);
		for (int i=1;i<=n;i++)
		{
			for (int j=i+1;j<=n;j++)
			{
				tmp=sqrt((dlk[i].xi-dlk[j].xi)*(dlk[i].xi-dlk[j].xi)+(dlk[i].yi-dlk[j].yi)*(dlk[i].yi-dlk[j].yi)+(dlk[i].zi-dlk[j].zi)*(dlk[i].zi-dlk[j].zi));
				if (tmp<=R)
				{
					f[i].push_back(j);
					f[j].push_back(i);
				}
			}
		}
		head=0;
		tail=0;
		memset(yzh,false,sizeof(yzh));
		memset(pks,false,sizeof(pks));
		zlb=false;
		for (int i=1;i<=n;i++)
		{
			if (dlk[i].zi>r)
				break;
			yzh[i]=true;
			tail++;
			ksl[tail]=i;
		}
		for (int i=n;i>0;i--)
		{
			if (dlk[i].zi+r<h)
				break;
			pks[i]=true;
		}
		while (head!=tail&&!zlb)
		{
			head++;
			temp=ksl[head];
			for (int i=0;i<f[temp].size();i++)
			{
				if (!yzh[f[temp][i]])
				{
					yzh[f[temp][i]]=true;
					tail++;
					ksl[tail]=f[temp][i];
					if (pks[f[temp][i]])
					{
						zlb=true;
						break;
					}
				}
			}
		}
		if (zlb)
			printf("Yes\n");
		else
			printf("No\n");
	}
}

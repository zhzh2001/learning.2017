#include<bits/stdc++.h>
using namespace std;
struct node
{
	int x,y,num;
	long long nyr; 
	friend bool operator < (node a,node b)
	{
		return a.nyr>b.nyr;
	}
};
priority_queue <node> ksl,alb;
int n,m,dis[15][15],l,r,v,numb;
bool f[15];
long long ans,mn=100000000000000LL;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dis,-1,sizeof(dis));
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&l,&r,&v);
		if (dis[l][r]==-1||dis[l][r]>v)
		{
			dis[l][r]=v;
			dis[r][l]=v;
		}
	}
	for (int i=1;i<=n;i++)
	{
		//while (!ksl.empty())
		//	ksl.pop();
		ksl=alb;
		memset(f,false,sizeof(f));
		f[i]=true;
		ans=0;
		numb=1;
		for (int j=1;j<=n;j++)
		{
			if (dis[i][j]==-1)
				continue;
			node temp;
			temp.x=i;
			temp.y=j;
			temp.num=1;
			temp.nyr=dis[i][j];
			ksl.push(temp); 
		}
		while (numb<n)
		{
			node temp;
			temp=ksl.top();
			if (!f[temp.y])
			{
				f[temp.y]=true;
				numb++;
				ans+=temp.nyr;
				for (int j=1;j<=n;j++)
				{
					if (dis[temp.y][j]==-1)
						continue;
					node temp2;
					temp2.num=temp.num+1;
					temp2.x=temp.y;
					temp2.y=j;
					temp2.nyr=dis[temp.y][j]*temp2.num;
					ksl.push(temp2);
				}
				ksl.pop();
			}
			else
			{
				ksl.pop();
			}
		} 
		if (ans<mn)
			mn=ans;
	}
	printf("%lld\n",mn);
} 

#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<cmath>
using namespace std;
inline int min(const int &a,const int &b){return a>b?b:a;}
inline int max(const int &a,const int &b){return a<b?b:a;}
inline int Max(int &a,int b){return a<b?a=b:a;}
inline int Min(int &a,int b){return a>b?a=b:a;}
const
	int Mod=(int)1e9+7;

inline int Add(int a,int b){a+=b;return a>=Mod?a-Mod:a;}
inline int Up(int &a,int b){a+=b;return a>=Mod?a-=Mod:a;}
inline int Dec(int a,int b){a-=b;return a<0?a+=Mod:a;}
inline int Mul(int a,int b){return a*1ll*b%Mod;}
char c;
inline void read(int&a)
{
	a=0;do c=getchar();while(c<'0'||c>'9');
	while(c<='9'&&c>='0')a=(a<<3)+(a<<1)+c-'0',c=getchar();
}
#define ll long long 
int n,m,q;
int Ans[500001];
int M[1001][1001];
int P[300001];



struct Node
{
	Node *lc,*rc,*f;
	int rank,size,op;
	Node(){rank=0,size=0,lc=rc=this;f=this;}
	bool l(){return f->lc==this;}
}*Empty=new Node;

void Up(Node *a)
{
	a->rank=a->lc->size+1;
	a->size=a->rank+a->rc->size;
}
void Lc(Node *a)
{
	Node *f;
	if(a->f->f==a->f)
		f=a;
	else if(a->f->l())
		f=a->f->f,f->lc=a;
	else f=a->f->f,f->rc=a;
	a->f->lc=a->rc;
	a->rc->f=a->f;
	a->rc=a->f;
	a->f->f=a;
	a->f=f;
	Up(a->rc);
	Up(a);
}
void Rc(Node *a)
{
	Node *f;
	if(a->f->f==a->f)
		f=a;
	else if(a->f->l())
		f=a->f->f,f->lc=a;
	else f=a->f->f,f->rc=a;
	a->f->rc=a->lc;
	a->lc->f=a->f;
	a->lc=a->f;
	a->f->f=a;
	a->f=f;
	Up(a->lc);
	Up(a);
}
void On(Node *a)
{
	a->l()?Lc(a):Rc(a);
}
void Tw(Node *a)
{
	a->l()^a->f->l()?On(a):On(a->f);On(a);
}
Node *Rt;

void Splay(Node *a)
{
	while(a->f->f!=a->f)
	Tw(a);
	while(a->f!=a)On(a);
	Rt=a;
}


Node* find(Node *a,int rank)
{
	if(a->rank==rank)return a;
	else if(rank>a->rank)
		return find(a->rc,rank-a->rank);
	return find(a->lc,rank);
}

void insert(Node *a,Node *b)
{
	if(a->rc==Empty)
		a->rc=b,b->f=a;
	else insert(a->rc,b);
}

int main()
{
	int o=0;
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n),read(m),read(q);
	
	if(n<=1000&&m<=1000)
	{for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			M[i][j]=++o;

	for(int i=1;i<=q;i++)
	{
		int x,y,z;
		read(x),read(y);
		int tp=M[x][y];
		printf("%d\n",tp);
		for(int j=y;j<m;j++)
			M[x][j]=M[x][j+1];
		for(int j=x;j<n;j++)
			M[j][m]=M[j+1][m];
		M[n][m]=tp;
	}
	return 0;
	}
	else
	{
		for(int i=1;i<m;i++)
			P[i]=i;
		for(int i=1;i<=n;i++)
			P[i+m-1]=(i-1)*n+m;	
		int L=n+m-1;
		Rt=new Node;
		Rt->op=1;
		Rt->lc=Rt->rc=Empty;
		Up(Rt);
		
		for(int i=2;i<=n+m-1;i++)
		{
			Node *A=new Node;
			A->lc=A->rc=Empty;
			A->rank=A->size=1;
			A->op=P[i];
			insert(Rt,A);
			Splay(A);
		}
		
		for(int i=1;i<=q;i++)
		{
			int x,y;
			read(x),read(y);
			int p=x+y-1;
			Node *T=find(Rt,p);
			Splay(T);
			while(T->rc!=Empty||T->lc!=Empty)
				T->lc!=Empty?Lc(T->lc):Rc(T->rc);
			Node *K=T->f;
			
			if(T->l())K->lc=Empty;
			else K->rc=Empty;
			Splay(K);
			T->f=T;
			insert(Rt,T);
			printf("%d\n",T->op);
		}
		
		return 0;
	}
	return 0;
}


#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<cmath>
using namespace std;
inline int min(const int &a,const int &b){return a>b?b:a;}
inline int max(const int &a,const int &b){return a<b?b:a;}
inline int Max(int &a,int b){return a<b?a=b:a;}
inline int Min(int &a,int b){return a>b?a=b:a;}
const
	int Mod=(int)1e9+7;

inline int Add(int a,int b){a+=b;return a>=Mod?a-Mod:a;}
inline int Up(int &a,int b){a+=b;return a>=Mod?a-=Mod:a;}
inline int Dec(int a,int b){a-=b;return a<0?a+=Mod:a;}
inline int Mul(int a,int b){return a*1ll*b%Mod;}

bool flag;
char c;
inline void read(int&a)
{
	
	a=0;do c=getchar();while(c!='-'&&(c<'0'||c>'9'));
	c= c=='-'?flag=1,getchar():c;
	while(c<='9'&&c>='0')a=(a<<3)+(a<<1)+c-'0',c=getchar();
	a= flag?flag=false,-a:a;
}
#define ll long long
inline void read(ll&a)
{
	a=0;do c=getchar();while(c!='-'&&(c<'0'||c>'9'));
	c= c=='-'?flag=1,getchar():c;
	while(c<='9'&&c>='0')a=(a<<3)+(a<<1)+c-'0',c=getchar();
	a= flag?flag=false,-a:a;
}
struct P
{
	ll x,y,z;
	inline friend bool operator <(P a,P b)
	{
		return a.z<b.z;
	}
}L[1001];
inline ll sqr(ll a){return a*a;}

int n,h;
ll r,r2;
bool link(P a,P b)
{
	return sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z)<=r2;
}

int Vis[1002];
bool dfs(int u)
{
	if(Vis[u])return false;
	Vis[u]=1;
	if(L[u].z+r>=h)return true;
	bool fl=false;
	for(int i=u-1;(!fl)&&i;i--)
	if(!Vis[i])
	{
		if(link(L[i],L[u]))
			fl=dfs(i);
		else if(L[u].z-L[i].z>2*r)
			break;
	}
		
	for(int i=u+1;(!fl)&&i<=n;i++)
	if(!Vis[i])
	{
		if(link(L[i],L[u]))
			fl=dfs(i);
		else if(L[i].z-L[u].z>2*r)
			break;
	}

	return fl;
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	read(T);
	while(T--)
	{
		memset(Vis,0,sizeof(Vis));
		read(n),read(h),read(r);
		r2=r*r*4;
		for(int i=1;i<=n;i++)
			read(L[i].x),read(L[i].y),read(L[i].z);	
		sort(L+1,L+1+n);
		bool fl=false;				
		for(int i=1;(!fl)&&i<=n;i++)
			if(L[i].z<=r)
				if(!Vis[i])
					fl=dfs(i);
		puts(fl?"Yes":"No");
	}
	return 0;
}


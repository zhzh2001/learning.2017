#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<cmath>
using namespace std;
inline int min(const int &a,const int &b){return a>b?b:a;}
inline int max(const int &a,const int &b){return a<b?b:a;}
inline int Max(int &a,int b){return a<b?a=b:a;}
inline int Min(int &a,int b){return a>b?a=b:a;}
const
	int Mod=(int)1e9+7;

inline int Add(int a,int b){a+=b;return a>=Mod?a-Mod:a;}
inline int Up(int &a,int b){a+=b;return a>=Mod?a-=Mod:a;}
inline int Dec(int a,int b){a-=b;return a<0?a+=Mod:a;}
inline int Mul(int a,int b){return a*1ll*b%Mod;}


char c;
inline void read(int&a)
{
	a=0;do c=getchar();while(c<'0'||c>'9');
	while(c<='9'&&c>='0')a=(a<<3)+(a<<1)+c-'0',c=getchar();
}
int Val[21][21];
int n,m;
#define ll long long 

ll Ans=0,MinCost=1ll<<49;


struct DLX
{
	
	int L[21],R[21],head;
	inline void Del(int x)
	{
		L[R[x]]=L[x];
		R[L[x]]=R[x];
		if(x==head)head=R[x];
	}
	inline void Add(int x)
	{
		L[R[x]]=x;
		R[L[x]]=x;
	}
	inline int front(){return head;}
	inline int next(int x)
	{
		return R[x];
	}
	DLX(){}
	inline void Bg(int n){for(int i=1;i<n;i++)L[R[i]=i+1]=i;L[R[n]=1]=n;head=1;}
}P;

int St[22],top,Dep[22];

struct Rec
{
	int dis,val;
	inline friend bool operator <(Rec a,Rec b){return a.dis^b.dis?a.dis<b.dis:a.val<b.val;}
};



bool Solve(ll Cost)
{
	if(Cost>=MinCost)
		return false;
	if(top==n)
	{
		MinCost=Cost;return true;
	}
	bool fl=false;
	int t=P.front(),cn=0;
	for(int i=P.front();(i!=t)||(!cn);i=P.next(i))
	{
		cn++;
		Rec L[22];
		int t=0;
		for(int j=1;j<=top;j++)
			if(Val[St[j]][i]!=-1)
				{
					t++;
					L[t].dis=Dep[St[j]],L[t].val=Val[St[j]][i];
				}
		sort(L+1,L+1+t);
		P.Del(i);
		St[++top]=i;
		int MinC=1<<29;
		for(int j=1;j<=t;j++)
		{
			if(L[j].val>=MinC)continue;
			MinC=L[j].val;
			Dep[i]=L[j].dis+1;
			fl|=Solve(Cost+L[j].dis*L[j].val);	
		}
		P.Add(i);
		top--;
	}
	return fl;
}

bool ck(ll K)
{
		MinCost=K;
		bool fl=false;
		for(int i=1;i<=n;i++)
		{
			P.Del(i);
			St[top=1]=i;
			Dep[i]=1;
			fl|=Solve(0);
			P.Add(i);
		}
		return fl;
	
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(Val,-1,sizeof(Val));
	read(n),read(m);
	for(int i=1;i<=m;i++)
	{
		int u,v,l;
		read(u),read(v),read(l);
		if(Val[u][v]==-1||Val[u][v]>l)Val[u][v]=Val[v][u]=l; 
	}
	if(n<=10)
	{
		P.Bg(n);
		for(int i=1;i<=n;i++)
		{
			P.Del(i);
			St[top=1]=i;
			Dep[i]=1;
			Solve(0);
			P.Add(i);
		}
		cout<<MinCost<<endl;
	}
	else
	{
		P.Bg(n);
		ll R,delta=1;
		R=1;
		while(!ck(R))
		{R*=2;}
		cout<<MinCost<<endl;
		
	}
	return 0;
}


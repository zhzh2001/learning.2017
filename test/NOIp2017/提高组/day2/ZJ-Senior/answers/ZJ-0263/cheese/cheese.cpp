#include<bits/stdc++.h>
using namespace std;
#define M 1010
#define LL long long
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
inline int read(){
	char ch=getchar();int x=0,f=1;
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}while('0'<=ch&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}return x*f;
}
struct poi{
	int x,y,z;
} a[M];
int Ca[M][M];
bool vis[M];
int n,h,r;
inline LL sqr(int x){
	return 1ll*x*x;
}
inline void dfs(int x){
	vis[x]=1;
	rep(i,0,n+1){
		if(Ca[x][i]&&!vis[i]){
			dfs(i);
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--){
		n=read(),h=read(),r=read();
		rep(i,0,n+1)rep(j,0,n+1)Ca[i][j]=0;
		rep(i,1,n){
			a[i].x=read();
			a[i].y=read();
			a[i].z=read();
		}rep(i,1,n)rep(j,1,n)if(i!=j){
			LL D=sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z);
			if(sqr(2ll*r)>=D)Ca[i][j]=1;
		}rep(i,1,n){
			if(a[i].z-r<=0)Ca[0][i]=1;
			if(a[i].z+r>=h)Ca[i][n+1]=1;
		}memset(vis,0,sizeof(vis));
		dfs(0);
		if(vis[n+1]){
			puts("Yes");
		}else puts("No");
	}
	return 0;
} 

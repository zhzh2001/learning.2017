#include<bits/stdc++.h>
using namespace std;
#define M 1010
#define inf 100000000
#define LL long long
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
inline int read(){
	char ch=getchar();int x=0,f=1;
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}while('0'<=ch&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}return x*f;
}
int a[M][M];
LL S[50010],cnt[50010],f[50010];
vector<LL> b[50010];
vector<LL> res[50010];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n=read(),m=read(),q=read();
	//if(q<=100){
		if(n<=1000&&m<=1000){
			rep(i,1,n){
				rep(j,1,m){
					a[i][j]=(i-1)*m+j;
				}
			}while(q--){
				int x=read(),y=read();
				printf("%d\n",a[x][y]);
				int ans=a[x][y];
				for(int i=y;i<m;i++)a[x][i]=a[x][i+1];
				for(int i=x;i<n;i++)a[i][m]=a[i+1][m];
				a[n][m]=ans;
			}	
		}else{
			rep(i,1,n)S[i]=1ll*i*m;
			while(q--){
				int x=read(),y=read();
				if(y==m){
					int wt=S[x];
					printf("%lld\n",S[x]);
					rep(i,x,n-1)S[i]=S[i+1];
					S[n]=wt;
					continue;
				}
				res[x].push_back(S[x]);
				rep(i,x,n-1)S[i]=S[i+1];
				if(y<=m-1-cnt[x]){
					for(int i=0;i<b[x].size();i++){
						if(y<b[x][i]){
							break;
						}else y++;
					} S[n]=1ll*(x-1)*m+y; b[x].push_back(y);
					for(int i=b[x].size()-1;i;i--){
						if(b[x][i]<b[x][i-1])swap(b[x][i],b[x][i-1]);
					}cnt[x]++;
				}else{
					int wt=y-(m-cnt[x]);
					S[n]=res[x][wt];
					for(int i=wt;i+1<res[x].size();i++)res[x][i]=res[x][i+1];
					res[x].pop_back();
				}printf("%lld\n",S[n]);
			}
		}
//	}
	return 0;
} 

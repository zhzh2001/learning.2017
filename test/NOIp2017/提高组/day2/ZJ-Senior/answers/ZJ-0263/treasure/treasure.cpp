#include<bits/stdc++.h>
using namespace std;
#define M 13
#define inf 100000000
#define LL long long
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
inline int read(){
	char ch=getchar();int x=0,f=1;
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}while('0'<=ch&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}return x*f;
}
int a[M][M],ans;
int b[M];int st=0;
int f[5010][M],cnt[5010];
int dep[M],n,m;
int now[M];
inline void dfs(int x,int mask,int res){
	if(res>ans)return;
	if(x==n){
		ans=min(ans,res);
		return;
	}
	rep(e,1,cnt[mask]){
		int v=f[mask][e],mn=inf;
		rep(i,1,x-1){
			{
				mn=min(mn,dep[i]*a[b[i]][v]);
			}if(dep[i]!=dep[i+1]){
				dep[x+1]=dep[i]+1;b[x+1]=v;
				dfs(x+1,mask|(1<<v-1),res+mn);
				mn=inf;
			}
		}dep[x+1]=dep[x]+1;b[x+1]=v;
		dfs(x+1,mask|(1<<v-1),res+dep[x]*a[b[x]][v]);
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	rep(i,1,n)rep(j,1,n)a[i][j]=inf;
	rep(i,1,m){
		int u=read(),v=read(),w=read();
		a[u][v]=a[v][u]=min(a[u][v],w);
	}ans=inf;
	rep(mask,0,(1<<n)-1){
		rep(i,1,n)if(((1<<i-1)&mask)==0){
			f[mask][++cnt[mask]]=i;
		}
	}
	rep(i,1,n){
		dep[1]=1;b[1]=i;
	//	rep(j,1,n)printf("%d ",b[j]);puts("");
		dfs(1,(1<<i-1),0);
	}printf("%d\n",ans);
	return 0;
} 

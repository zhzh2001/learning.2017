#include<bits/stdc++.h>
using namespace std;
long long n,h,r,minn,b[1003],k;
bool g[1003],t;
struct aa{
	long long x,y,z;
} a[1003];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
	scanf("%lld%lld%lld",&n,&h,&r);
	for (int i=1;i<=n;i++)
	scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
	memset(b,0x3f3f3f3f,sizeof(b));
	memset(g,0,sizeof(g));
	for (int i=1;i<=n;i++)
	if (a[i].z<=r) b[i]=0;
	for (int i=1;i<=n;i++)
	{
		minn=1e9,k=0;
		for (int j=1;j<=n;j++)
		if (b[j]<minn&&!g[j]) minn=b[j],k=j;
		if (k==0||b[k]!=0) break;
		g[k]=1;
		for (int j=1;j<=n;j++)
		if ((a[k].x-a[j].x)*(a[k].x-a[j].x)+(a[k].y-a[j].y)*(a[k].y-a[j].y)+(a[k].z-a[j].z)*(a[k].z-a[j].z)<=1LL*r*r*4) b[j]=0;
	}
	t=false;
	for (int i=1;i<=n;i++)
	if (b[i]==0&&a[i].z+r>=h) t=true;
	if (t) printf("Yes\n");
	else printf("No\n");
	}
	return 0;
}

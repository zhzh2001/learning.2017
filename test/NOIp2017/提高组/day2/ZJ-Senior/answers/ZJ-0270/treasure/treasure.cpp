#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,z,l,s,a[20][20],f[20],inf,minn;
bool g[20];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x3f3f,sizeof(a));
	inf=minn=a[1][1];
	for (int i=1;i<=m;i++)
	scanf("%d%d%d",&x,&y,&z),a[x][y]=min(a[x][y],z),a[y][x]=min(a[y][x],z),l=z;
	for (int p=1;p<=n;p++)
	{
		memset(g,0,sizeof(g));
		memset(f,0,sizeof(f));
		g[p]=1;
		s=0;
		for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
		if (!g[i]&&a[i][j]!=inf) g[i]=1,f[i]=f[j]+1,s=s+f[i];
		minn=min(minn,s);
	}
	printf("%lld\n",1LL*minn*l);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define ll long long
struct node{int to,next;ll len;}edge[10000];
int head[20],tot=0;
int n,m,f[20][20],t[20];
ll LEN[20][20],ans=-1;
void add_edge(int x,int y,ll s){
	edge[++tot]=(node){y,head[x],s};
	head[x]=tot;
	return;
}
void dfs(int deep,ll sum,int num)
{
	if (ans!=-1&&sum>ans) return;
	if (num==n){
		if (ans==-1||sum<ans) ans=sum;
		return;
	}
	int w[15];w[0]=0;
	ll s[15];
	for (int k=1;k<=f[deep][0];k++){
		int u=f[deep][k];
		for (int i=head[u];i;i=edge[i].next){
			int v=edge[i].to;
			ll len=edge[i].len;
			if (t[v]==0) {w[++w[0]]=v;s[v]=len;t[v]=deep+1;}
			else if (t[v]==deep+1) s[v]=min(s[v],len);
		}
	}
	for (int i=1;i<=w[0];i++) t[w[i]]=0;
	int p=1<<w[0];p--;
	for (int k=p;k>=1;k--){
		int x=k,nums=num;ll S=sum;
		f[deep+1][0]=0;
		for (int i=1;i<=w[0];x>>=1,i++)
			if (x&1){
				f[deep+1][++f[deep+1][0]]=w[i];
				S+=s[w[i]]*(ll)deep;
				t[w[i]]=deep+1;
				nums++;
			}
		dfs(deep+1,S,nums);
		x=k;
		for (int i=1;i<=w[0];x>>=1,i++)
			if (x&1) t[w[i]]=0;
	}
	return;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(LEN,-1,sizeof(LEN));
	memset(head,0,sizeof(head));
	memset(t,0,sizeof(t));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int x,y;ll s;
		scanf("%d%d%lld",&x,&y,&s);
		if (LEN[x][y]==-1||s<LEN[x][y]) LEN[x][y]=s;
		if (LEN[y][x]==-1||s<LEN[y][x]) LEN[y][x]=s;
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (i!=j&&LEN[i][j]!=-1) add_edge(i,j,LEN[i][j]);
	for (int k=1;k<=n;k++){
		f[1][0]=1;
		f[1][1]=k;
		t[k]=1;
		dfs(1,0,1);
		t[k]=0;
	}
	printf("%lld",ans);
	return 0;
}

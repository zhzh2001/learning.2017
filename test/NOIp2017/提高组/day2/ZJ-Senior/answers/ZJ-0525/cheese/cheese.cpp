#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
#define ll long long
const int M=1010;
struct note{ll x,y,z;}a[M];
struct node{int to,next;}edge[M*M*4];
int n,head[M],tot;
ll h,r;
bool vis[M];
void add_edge(int x,int y){
	edge[++tot]=(node){y,head[x]};
	head[x]=tot;
	return;
}
void dfs(int u)
{
	vis[u]=1;
	for (int i=head[u];i;i=edge[i].next){
		int v=edge[i].to;
		if (!vis[v]) dfs(v);
	}
	return;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);
	while (T--){
		memset(head,0,sizeof(head));
		memset(vis,0,sizeof(vis));
		tot=0;
		scanf("%d%lld%lld",&n,&h,&r);
		ll R=r*2*r*2;
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
			for (int j=1;j<i;j++){
				ll s=(a[i].x-a[j].x)*(a[i].x-a[j].x);
				if (s<=R){
					s+=(a[i].y-a[j].y)*(a[i].y-a[j].y);
					if (s<=R){
						s+=(a[i].z-a[j].z)*(a[i].z-a[j].z);
						if (s<=R) {add_edge(i,j);add_edge(j,i);}
					}
				}
			}
			if (a[i].z<=r) add_edge(0,i);
			if (a[i].z+r>=h) add_edge(i,n+1);
		}
		dfs(0);
		if (vis[n+1]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
#define ll long long
const int M=300010;
int a[2010][2010];
ll n,m,Q,f[2*M];
int tree[5*M],MT;
queue<ll> q;
void in_tree(int x,ll s){
	while (x<=MT){
		tree[x]+=s;
		x+=x&-x;
	}
	return;
}
int get_tree(int x){
	int ans=0;
	while (x){
		ans+=tree[x];
		x-=x&-x;
	}
	return ans;
}
int erfen(int l,int r,int s){
	while (l<=r){
		int mid=(l+r)/2;
		if (mid-get_tree(mid)<s) l=mid+1;
		else r=mid-1;
	}
	return l;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&Q);
	if (n<=2000&&m<=2000&&Q<=100000)
	{
		for (int i=1,s=0;i<=n;i++)
			for (int j=1;j<=m;j++)
				a[i][j]=++s;
		while (Q--){
		int x,y;
		scanf("%d%d",&x,&y);
		int w=a[x][y];
		for (int j=y;j<m;j++) a[x][j]=a[x][j+1];
		for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
		a[n][m]=w;
		printf("%d\n",w);
		}
		return 0;
	}
	
	MT=m+Q;
	memset(tree,0,sizeof(tree));
	memset(f,0,sizeof(0));
	for (int i=1;i<=m;i++) f[i]=i;
	while (!q.empty()) q.pop();
	for (int i=2;i<=n;i++) q.push((ll)i*m);
	int ms=m;
	while (Q--){
	ll x,y;
	scanf("%lld%lld",&x,&y);
	int p=erfen(1,ms,y);
	printf("%lld\n",f[p]);
	in_tree(p,1);
	q.push(f[p]);
	f[++ms]=q.front();
	q.pop();
	f[p]=0;
	}
	return 0;
}

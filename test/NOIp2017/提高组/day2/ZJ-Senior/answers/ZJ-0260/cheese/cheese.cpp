#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i,a,b) for (int i = a;i <= b;++i)
#define drep(i,a,b) for (int i = a;i >= b;--i)
#define ll long long
#define N 1111
using namespace std;

ll h,r,R,a[N],b[N],c[N];
int n,T,f[N];
int find(int x)
{
	return f[x] == x?x:f[x] = find(f[x]);
}
inline void merge(int x,int y)
{
	int r1 = find(x), r2 = find(y);
	if (r1^r2) f[r1] = r2;
}
inline ll dist(int x,int y)
{
	ll X = a[x]-a[y],Y = b[x]-b[y],Z = c[x]-c[y];
	return X*X+Y*Y+Z*Z;
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (scanf("%d",&T);T--;)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		rep(i,0,n+1) f[i] = i;
		R = 4*r*r;
		rep(i,1,n) scanf("%lld%lld%lld",&a[i],&b[i],&c[i]);
		rep(i,1,n)
		{
			if (find(i)^find(0) && c[i] <= r) merge(i,0);
			if (find(i)^find(n+1) && c[i]+r >= h) merge(i,n+1);
			rep(j,i+1,n) if (R >= dist(i,j)) merge(i,j);
		}
		if (find(0) == find(n+1)) puts("Yes");
		else puts("No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}


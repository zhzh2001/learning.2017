#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i,a,b) for (int i = a;i <= b;++i)
#define drep(i,a,b) for (int i = a;i >= b;--i)
#define ll long long
using namespace std;

int n,m,q;
namespace do1
{
	int a[1111][1111],x,y,tmp,len;
	void work()
	{
		rep(i,1,n) rep(j,1,m) a[i][j] = ++len;
		rep(i,1,q)
		{
			scanf("%d%d",&x,&y); printf("%d\n",tmp=a[x][y]);
			rep(j,y,m-1) a[x][j] = a[x][j+1];
			rep(j,x,n-1) a[j][m] = a[j+1][m];
			a[n][m] = tmp;
		}
	}
}
namespace do2
{
	ll f[51111],a[511][51111],tmp;int len,b[51111],x,y;
	void solve()
	{
		printf("%lld\n",tmp=a[b[x]][y]);
		rep(j,y,m-1) a[b[x]][j] = a[b[x]][j+1];
		rep(j,x,n-1) f[j] = f[j+1];
		f[n] = tmp; a[b[x]][m] = f[x];
	}
	void work()
	{
		rep(i,1,n) f[i] = (ll)i*m;
		rep(i,1,q)
		{
			scanf("%d%d",&x,&y);
			if (b[x]) solve();
			else
			{
				b[x] = ++len;
				rep(j,1,m-1) a[b[x]][j] = (ll)(x-1)*m+j;
				a[b[x]][m] = f[x];
				solve();
			}
		}
	}
}

namespace do3
{
	int a[600010],b[600010],g[600010],ans,len,x,y;
	int qry(int x)
	{
		int y = 0;
		for (;x;x -= x&-x) y += g[x];
		return y;
	}
	void add(int x,int y)
	{
		for (;x <= m+q;x += x&-x) g[x] += y;
	}
	void work()
	{
		rep(i,1,m) a[i] = 1, b[i] = i,add(i,1);
		len = m;
		rep(i,1,q)
		{
			scanf("%d%d",&x,&y);
			for (int L = 1,R = len;L <= R;)
			{
				int mid = L+R>>1,temp = qry(mid);
				if (temp < y) L = mid+1;
				else if (temp > y) R = mid-1;
				else if (a[mid] == 1) {ans = mid;break;}
				else R = mid-1;
			}
			printf("%d\n",b[ans]);
			a[ans] = 0; add(ans,-1); a[++len] = 1; add(len,1);
			b[len] = b[ans];
		}
	}
}
namespace do4
{
	int a[1200010],g[1200010],ans,len,x,y,X[300010],Y[300010];
	ll b[1200010];
	int qry(int x)
	{
		int y = 0;
		for (;x;x -= x&-x) y += g[x];
		return y;
	}
	void add(int x,int y)
	{
		for (;x <= m+n-1+q;x += x&-x) g[x] += y;
	}
	void solve()
	{
		rep(i,1,m) a[i] = 1, b[i] = i,add(i,1);
		len = m;
		rep(i,2,n) a[++len] = 1, b[len] = (ll)i*m,add(len,1);
		rep(i,1,q)
		{
			x = X[i], y = Y[i];
			for (int L = 1,R = len;L <= R;)
			{
				int mid = L+R>>1,temp = qry(mid);
				if (temp < y) L = mid+1;
				else if (temp > y) R = mid-1;
				else if (a[mid] == 1) {ans = mid;break;}
				else R = mid-1;
			}
			printf("%lld\n",b[ans]);
			a[ans] = 0; add(ans,-1); a[++len] = 1; add(len,1);
			b[len] = b[ans];
		}
	}
	void work()
	{
		int flag = 1;
		rep(i,1,q)
		{
			scanf("%d%d",&X[i],&Y[i]);
			if (X[i]>1) flag = 0;
		}
		if (flag) solve();
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n <= 1000 && m <= 1000) do1::work(); //30pts
	else if (q <= 500) do2::work(); //20pts
	else if (n == 1) do3::work(); //20pts
	else do4::work(); //10pts
	fclose(stdin);
	fclose(stdout);
	return 0;
}

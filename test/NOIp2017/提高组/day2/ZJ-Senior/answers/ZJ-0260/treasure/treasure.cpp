#include <cstdio>
#include <algorithm>
#include <queue>
#define rep(i,a,b) for (int i = a;i <= b;++i)
#define drep(i,a,b) for (int i = a;i >= b;--i)
using namespace std;
const int inf = 1e7;

int len,f[15][5000],g[15][15],h[15][5000],n,m,x,y,z,b[15],a[15],all;
int flag,tmp;
void dfs(int x,int T,int S)
{
	if (x > len)
	{
		rep(i,1,len) if (b[i])
		{
			rep(k,1,len) if (!b[k] && g[a[i]][a[k]] != inf)
			if (f[a[i]][T]+f[a[k]][S^T]+h[a[k]][S^T]+g[a[i]][a[k]] < f[a[i]][S])
			{
				f[a[i]][S] = f[a[i]][T]+f[a[k]][S^T]+h[a[k]][S^T]+g[a[i]][a[k]];
				h[a[i]][S] = h[a[i]][T]+h[a[k]][S^T]+g[a[i]][a[k]];
			}
			else if (f[a[i]][T]+f[a[k]][S^T]+h[a[k]][S^T] == f[a[i]][S])
				h[a[i]][S] = min(h[a[i]][S],h[a[i]][T]+h[a[k]][S^T]+g[a[i]][a[k]]);
		}
		
		return;
	}
	dfs(x+1,T,S);
	b[x] = 1;
	dfs(x+1,T^(1<<a[x]-1),S);
	b[x] = 0;
}
queue<int> q;
int vis[15];
int bfs(int x)
{
	int ans = 0;
	rep(i,1,n) vis[i] = 0;
	q.push(x);vis[x] = 1;
	for (;!q.empty();q.pop())
	{
		int t = q.front();
		rep(i,1,n) if (g[t][i] < inf && !vis[i])
		{
			ans += vis[t]*g[t][i];
			q.push(i); vis[i] = vis[x]+1;
		}
	}
	rep(i,1,n) if (!vis[i]) return inf;
	return ans;
}
void work()
{
	int ans = inf;
	rep(i,1,n) ans = min(ans,bfs(i));
	printf("%d\n",ans);
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,n) rep(j,1,n) g[i][j] = inf;
	flag = 1;
	rep(i,1,m)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (!tmp) tmp = z;
		else if (tmp != z) flag = 0;
		g[x][y] = g[y][x] = min(g[x][y],z);
	}
	if (flag) work();
	else
	{
		all = (1<<n)-1;
		rep(i,1,n) rep(S,0,all) f[i][S] = h[i][S] = inf;
		rep(S,1,all)
		{
			len = 0;
			rep(i,1,n) if (S&(1<<i-1)) a[++len] = i;
			if (len > 2) dfs(1,0,S);
			else if (len == 1) f[a[1]][S] = h[a[1]][S] = 0;
			else f[a[1]][S] = f[a[2]][S] = h[a[1]][S] = h[a[2]][S] = g[a[1]][a[2]];
		}
		int ans = inf;
		rep(i,1,n) ans = min(ans,f[i][all]);
		printf("%d\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

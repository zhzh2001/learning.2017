#include<bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define re(i,a,b) for(int i=(a);i<(b);i++)
#define repd(i,a,b) for(int i=(a);i>=(b);i--)
#define clr(a) memset(a,0,sizeof(a))
typedef long long ll;
const int N=12,INF=1e9;
ll sqr(int x){return 1ll*x*x;}
#define pw(a) (1<<(a))
int f[N][N][pw(N)+15],n,m,e[N][N];//root dep set
#define gmin(x,y) x=(y<x)?y:x;
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	re(i,0,n)re(j,0,n)e[i][j]=INF;
	re(i,0,m){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		x--;y--;
		gmin(e[x][y],z);
		gmin(e[y][x],z);
	}
	re(i,0,n)re(j,0,n)re(k,0,pw(n))f[i][j][k]=INF;
	re(i,0,n)re(j,0,n)f[i][j][pw(i)]=0;
	re(o,1,pw(n)){
		re(i,0,n)if(pw(i)&o)re(j,1,n)if(f[i][j][o]!=INF){
			int k=o^(pw(n)-1);
			for(int t=k;t;t=(t-1)&k)re(ni,0,n)if(t&pw(ni))if(e[ni][i]!=INF)if(f[ni][j-1][t]!=INF)
				gmin(f[ni][j-1][o|t],f[ni][j-1][t]+e[ni][i]*j+f[i][j][o]);
		}
	}
	int res=INF;
	re(i,0,n)gmin(res,f[i][0][pw(n)-1]);
	cout<<res<<endl;
}

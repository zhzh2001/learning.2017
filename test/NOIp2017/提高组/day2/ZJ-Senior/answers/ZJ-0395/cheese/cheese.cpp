#include<bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define re(i,a,b) for(int i=(a);i<(b);i++)
#define repd(i,a,b) for(int i=(a);i>=(b);i--)
#define clr(a) memset(a,0,sizeof(a))
int n,H,r;
typedef long long ll;
const int N=1e3+10;
int x[N],y[N],z[N],S,T,e[N][N],vis[N],q[N],h,t;
ll sqr(int x){return 1ll*x*x;}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int TT;scanf("%d",&TT);
	rep(cas,1,TT){
		scanf("%d%d%d",&n,&H,&r);S=n+1;T=n+2;
		clr(e);
		rep(i,1,n){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if(z[i]<=r)e[S][i]=e[i][S]=1;
			if(z[i]>=H-r)e[T][i]=e[i][T]=1;
		}
		rep(i,1,n)rep(j,i+1,n){
			ll tmp=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
			if(tmp<=sqr(r)*4)e[i][j]=e[j][i]=1;
		}
		clr(vis);
		q[h=t=1]=S;for(;h<=t;h++){
			int x=q[h];vis[x]=1;
			rep(i,1,n+2)if(e[x][i])if(!vis[i])
				q[++t]=i;
		}
		puts(vis[T]?"Yes":"No");
	}
}

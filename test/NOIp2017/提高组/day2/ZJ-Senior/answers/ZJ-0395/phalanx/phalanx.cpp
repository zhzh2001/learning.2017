#include<bits/stdc++.h>
using namespace std;
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define re(i,a,b) for(int i=(a);i<(b);i++)
#define repd(i,a,b) for(int i=(a);i>=(b);i--)
#define clr(a) memset(a,0,sizeof(a))
#define w1 first
#define w2 second
typedef long long ll;
typedef pair<int,int> pa;
const int N=2e6+10;
//****
	ll w[N];
	int ls[N],rs[N],rnd[N],len[N],slen[N],siz[N],prelen;;
	inline int pu(int x){
		siz[x]=siz[ls[x]]+siz[rs[x]]+1;
		slen[x]=slen[ls[x]]+slen[rs[x]]+len[x];
	}
	inline int merge(int x,int y){
		if(!x||!y)return x+y;
		if(rnd[x]<rnd[y]){
			rs[x]=merge(rs[x],y);
			pu(x);
			return x;
		}
		ls[y]=merge(x,ls[y]);
		pu(y);
		return y;
	}
	inline int getpos(int x,int k){
		if(!x)return 0;
		if(slen[ls[x]]>=k)return getpos(ls[x],k);
		k-=slen[ls[x]];prelen+=slen[ls[x]];
		if(k<=len[x])return siz[ls[x]]+1;
		k-=len[x];prelen+=len[x];
		return getpos(rs[x],k)+siz[ls[x]]+1;
	}
	inline pa split(int x,int k){
		if(!x)return pa(0,0);pa y;
		if(siz[ls[x]]>=k){
			 y=split(ls[x],k);
			ls[x]=y.w2;y.w2=x;
			pu(x);
		}else{
			y=split(rs[x],k-siz[ls[x]]-1);
			rs[x]=y.w1,y.w1=x;
			pu(x);
		}
		return y;
	}
	void print(int x){
		if(!x)return;
		print(ls[x]);
		printf("%lld(%d) ",w[x],len[x]);
		print(rs[x]);
	}
	void pr(int x){print(x);puts("");}
//****
int rt[N],nrt;
int n,m,q,cnt;
int newnode(ll sta,int l){
	int x=++cnt;
	w[x]=sta;len[x]=slen[x]=l;rnd[x]=rand();siz[x]=1;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	rep(i,1,n)rt[i]=newnode(1ll*i*m-m+1,m-1);
	cnt=n;
	rep(i,1,n){
		int x=newnode(1ll*m*i,1);
		nrt=merge(nrt,x);
	}
	//pr(nrt);
	//pr(rt[1]);
	rep(i,1,q){
		int x,y;scanf("%d%d",&x,&y);
		if(y<m){
			prelen=0;
			int rk=getpos(rt[x],y);
			pa p1=split(rt[x],rk);
			pa p2=split(p1.w1,rk-1);
			int nx=p2.w2,dl=y-(prelen+1),dr=(prelen+len[nx])-y,f1=p2.w1,f4=p1.w2;
			int ax=newnode(w[nx]+dl,1);
			printf("%lld\n",w[nx]+dl);
			if(dl>0){
				int nx1=newnode(w[nx],dl);
				f1=merge(f1,nx1);
			}
			if(dr>0){
				int nx2=newnode(w[nx]+dl+1,dr);
				f1=merge(f1,nx2);
			}
			rt[x]=merge(f1,f4);
			pa q1=split(nrt,x);
			pa q2=split(q1.w1,x-1);
			rt[x]=merge(rt[x],q2.w2);
			nrt=merge(q2.w1,q1.w2);
			nrt=merge(nrt,ax);
		}else{
			pa q1=split(nrt,x);
			pa q2=split(q1.w1,x-1);
			printf("%lld\n",w[q2.w2]);
			nrt=merge(q2.w1,q1.w2);
			nrt=merge(nrt,q2.w2);
		}
		
		//pr(rt[1]);
	}
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=10007;
int n;
double h,r;
struct node{
	double x,y,z;
}a[N];
const double Eps=1e-9;
int vis[N],q[N];

int main(){
	// say hello

	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);

	int T;
	scanf("%d",&T);
	For(p,1,T){
		memset(vis,0,sizeof(vis));
		scanf("%d%lf%lf",&n,&h,&r);
		int L=0,R=0;
		For(i,1,n){
			scanf("%lf%lf%lf",&a[i].x,&a[i].y,&a[i].z);
			if (a[i].z-r<=Eps) vis[i]=1,q[++R]=i;
		}
		int flag=0;
		while (L<R){
			L++;int k=q[L];
			double X1=a[k].x,Y1=a[k].y,Z1=a[k].z;
			For(i,1,n) if (!vis[i]){
				double X2=a[i].x,Y2=a[i].y,Z2=a[i].z;
			//	printf("%.1lf %.1lf %.1lf   %.1lf %.1lf %.1lf   %.1lf\n",X1,Y1,Z1,X2,Y2,Z2,r);
				if (sqrt( (X1-X2)*(X1-X2) + (Y1-Y2)*(Y1-Y2) + (Z1-Z2)*(Z1-Z2) ) - 2*r <= Eps){
					vis[i]=1;
					R++;
					q[R]=i;
					if (h-(Z2+r) <= Eps){
						flag=1;
						break;
					}
				}
			}
			if (flag) break;
		}
		if (flag) printf("Yes\n");
		else printf("No\n");
	}




	return 0;

	// say goodbye
}


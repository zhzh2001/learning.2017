#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<map>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int Q=50000000,N=13;
int a[N][N],vis[N],pre[N];
int f[Q],ans=1<<28;
int n;

void Dfs1(int last){
//	printf("%d\n",f1[last]);
	int flag=0;
	For(x,1,n) if (vis[x]){
		flag++;
		For(y,1,n) if (!vis[y]&&(a[x][y]!=a[0][0])){
			vis[y]=vis[x]+1;
			int k=last+vis[x]*pre[y];
			if (f[k]>f[last]+a[x][y]*vis[x]){
				f[k]=f[last]+a[x][y]*vis[x];
				Dfs1(k);
			}
			vis[y]=0;
		}		
	}
	if (flag==n) ans=min(ans,f[last]);
}

void Check(int x,int opt){
	int y=n;
	For(i,1,y){
		printf("%d ",x%12);
		x/=12;
	}printf("\n");
}



int m;

int main(){
	// say hello

	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);

	pre[1]=1;
	For(i,2,8) pre[i]=pre[i-1]*9;
	n=read(),m=read();
	memset(a,67,sizeof(a));
	int last_w=0,flag_w=1;
	For(i,1,m){
		int u=read(),v=read(),w=read();
		if (!last_w) last_w=w;
		if (last_w!=w) flag_w=0;
		a[u][v]=min(a[u][v],w);
		a[v][u]=min(a[v][u],w);
		
	}
	memset(f,67,sizeof(f));
	For(i,1,n){
			f[pre[i]]=0;
			vis[i]=1;
			Dfs1(pre[i]);
			vis[i]=0;
	}
	printf("%d\n",ans);

	return 0;

	// say goodbye
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=3e5+7;
int n,m,q;
struct node{
	int x,y;
}a[N];

#define ls (pos<<1)
#define rs (pos<<1|1)
struct Tree{
	int num,opt;
}tr[N<<2];

void pushdown(int pos){
	tr[ls].num+=tr[pos].opt;
	tr[rs].num+=tr[pos].opt;
	tr[ls].opt+=tr[pos].opt;
	tr[rs].opt+=tr[pos].opt;
	tr[pos].opt=0;
}

void Build(int pos,int l,int r){
	if (l==r){
		tr[pos].num=l;
		return;
	}
	int mid=(l+r)>>1;
	Build(ls,l,mid);
	Build(rs,mid+1,r);
}

void Update(int pos,int l,int r,int x,int y,int num){
	if (l==x&&r==y){
		tr[pos].opt+=num;
		tr[pos].num+=num;
		return;
	}
	pushdown(pos);
	int mid=(l+r)>>1;
	if (y<=mid) Update(ls,l,mid,x,y,num);
	else if (x>mid) Update(rs,mid+1,r,x,y,num);
	else Update(ls,l,mid,x,mid,num),Update(rs,mid+1,r,mid+1,y,num);
}

int Query(int pos,int l,int r,int x){
	if (l==r){
		return tr[pos].num;
	}
	pushdown(pos);
	int mid=(l+r)>>1;
	if (x<=mid) return Query(ls,l,mid,x);
	else return Query(rs,mid+1,r,x);
}

int main(){
	// say hello

	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);

	n=read(),m=read(),q=read();
	if (q>10000){
		n=n+m-1;
		Build(1,1,n);
		For(i,1,q){
			int x=read(),y=read();
			int k=(x+y)-1;
			ll res=Query(1,1,n,k);
			ll ans=0;
			if (res>m) ans=1ll*(res-m)*m+m;
			else ans=res;
			printf("%lld\n",ans);
			if (k<n) Update(1,1,n,k,n-1,1);
			Update(1,1,n,n,n,-Query(1,1,n,n)+res);
		}

	
		return 0;	
	}
	
	
	
	For(p,1,q){
		int x=read(),y=read();
		a[p].x=x,a[p].y=y;
		Rep(i,1,p-1){
		//	if (x==88&&y==100) printf("%d %d %d %d\n",a[i].x,a[i].y,x,y);
			if (x==n&&y==m) x=a[i].x,y=a[i].y;
			else if (a[i].x==x&&a[i].y<=y){
				y++;
				if (y>m) y--,x++;
			} 
			else if (a[i].x<=x&&y==m) x++;
		}
		ll ans=1ll*x*m-m+y;
		printf("%lld\n",ans);
	}



	return 0;

	// say goodbye
}


#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<ctype.h>
#define M 300005
#define LL long long
using namespace std;
int n,m,q;
void Rd(int &res){
	char c;
	res=0;
	while(c=getchar(),!isdigit(c));
	do{
		res=(res<<3)+(res<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}
struct OPE{
	int x,y;
}ope[M];
struct P30{
	static const int MAX=1005;
	int mp[MAX][MAX];
	void solve(){
		int id=0;
		for(int i=1;i<=n;i++){
			for(int k=1;k<=m;k++)mp[i][k]=++id;
		}
		for(int i=1;i<=q;i++){
			int x=ope[i].x,y=ope[i].y;
			int res=mp[x][y];
			printf("%d\n",mp[x][y]);
			for(int k=y+1;k<=m;k++)mp[x][k-1]=mp[x][k];
			for(int k=x+1;k<=n;k++)mp[k-1][m]=mp[k][m];
			mp[n][m]=res;
//			for(int k=1;k<=n;k++){
//				for(int j=1;j<=m;j++)printf("%d ",mp[k][j]);
//				puts("");
//			}
		}
	}
}p30;
struct P20{
	LL li[505];
	LL a[M],b[505][M];
	void solve(){
		for(int i=1;i<=n;i++)a[i]=1ll*i*m;
//		for(int i=1;i<=n;i++)printf("->%lld\n",a[i]);
		for(int i=1;i<=q;i++)li[i]=ope[i].x;
		sort(li+1,li+1+q);
		int Q=unique(li+1,li+1+q)-li-1;
//		printf("%d\n",Q);
		for(int i=1;i<=q;i++){
			int x=lower_bound(li+1,li+1+Q,ope[i].x)-li;
			for(int k=1;k<m;k++){
				b[x][k]=1ll*(ope[i].x-1)*m+k;
//				if(x==1&&k==2)printf("(%d)\n",b[x][2]);
			}
		}
		for(int i=1;i<=q;i++){
//			puts("");
			int xx=lower_bound(li+1,li+1+Q,ope[i].x)-li;
			int x=ope[i].x,y=ope[i].y;
			LL res;
			if(y==m)res=a[x];
			else res=b[xx][y];
			if(ope[i].y!=m){
				for(int k=y+1;k<m;k++)b[xx][k-1]=b[xx][k];
				b[xx][m-1]=a[x];
			}
			for(int k=x+1;k<=n;k++)a[k-1]=a[k];
			a[n]=res;
			printf("%lld\n",res);
//			puts("");
//			for(int k=1;k<=Q;k++){
//				printf("(%lld %lld %lld)\n",b[k][1],b[k][2],b[k][3]);
//			}
//			for(int k=1;k<=n;k++)printf("%lld\n",a[k]);
		}
	}
}p20;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Rd(n),Rd(m),Rd(q);
	for(int i=1;i<=q;i++){
		Rd(ope[i].x),Rd(ope[i].y);
	}
//	p30.solve();
//	p20.solve();
	if(n<=1000&&m<=1000)p30.solve();
	else if(q<=500)p20.solve();
	return 0;
}

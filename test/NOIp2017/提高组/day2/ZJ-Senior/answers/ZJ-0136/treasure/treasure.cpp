#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<vector>
#define M 30
#define oo 1000000000
using namespace std;
struct node{
	int to,v;
};
vector<node>edge[M];
int n,m;
struct P70{
	int dis[15][15];
	int dp[15][15][(1<<12)+50];//当前在i，S到达 
	int cnt[(1<<12)+50];
	void solve(){
		memset(cnt,0,sizeof(cnt));
		for(int i=0;i<15;i++){
			for(int j=0;j<15;j++){
				for(int k=0;k<(1<<12)+50;k++){
					dp[i][j][k]=oo;
				}
			}
		}
		for(int i=0;i<15;i++){
			for(int k=0;k<15;k++){
				dis[i][k]=oo;
			}
		}
		for(int i=1;i<=n;i++){
			for(int k=0;k<(int)edge[i].size();k++){
				node p=edge[i][k];
//				if(i==3&&p.to==4)printf("(%d)\n",p.v);
				dis[i-1][p.to-1]=min(dis[i-1][p.to-1],p.v);
			}
		}
//		printf("%d\n",dis[2][3]);
		for(int i=0;i<n;i++){
			for(int k=0;k<=n;k++){
				dp[i][k][1<<i]=0;
			}
		}
		int tot=(1<<n)-1;
		for(int i=0;i<=tot;i++){
			for(int k=0;k<n;k++){
				if(i&(1<<k))cnt[i]++;
			}
		}
//		for(int i=0;i<n;i++){
//			for(int k=0;k<n;k++)printf("%d ",dis[i][k]);
//			puts("");
//		}
		for(int k=0;k<=tot;k++){
			for(int i=0;i<n;i++){
				if(!(k&(1<<i)))continue;
				for(int j=0;j<n;j++){
					if(!(k&(1<<j))||dis[i][j]==oo)continue;
					if(!k)continue;
					int S1=k^(1<<i)^(1<<j);//没取i时集合的情况
					for(int t=S1;;t=(t-1)&S1){
						int S2=S1^t;
						for(int tt=0;tt<n;tt++){
							if(dp[j][tt+1][t|(1<<j)]==oo||dp[i][tt][S2|(1<<i)]==oo)continue;
							dp[i][tt][k]=min(dp[i][tt][k],dp[i][tt][S2|(1<<i)]+dp[j][tt+1][t|(1<<j)]+(tt+1)*dis[i][j]);
						}
						if(t==0)break;
					}
				}
			}
		}
		int ans=oo;
//		printf("(%d)\n",dp[0][15]);
		for(int i=0;i<n;i++)ans=min(ans,dp[i][0][tot]);
		printf("%d\n",ans);
	}
}p70;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		edge[x].push_back((node){y,v});
		edge[y].push_back((node){x,v});
	}
	if(n<=8)p70.solve();
	else p70.solve();
	return 0;
}
		

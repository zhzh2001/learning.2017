#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<math.h>
#define M 1005
#define eps 0.000000000001
using namespace std;
struct POI{
	int x,y,z;
}poi[M];
int n,h,r;
struct PP{
	double calc(POI a,POI b){
		double p1=1.0*(a.x-b.x);
		double p2=1.0*(a.y-b.y);
		double p3=1.0*(a.z-b.z);
		return sqrt(p1*p1+p2*p2+p3*p3);
	}
	int fa[M];
	bool ok1[M],ok2[M];
	int getfa(int x){
		if(fa[x]==x)return x;
		return fa[x]=getfa(fa[x]);
	}
	void solve(){
		for(int i=1;i<=n;i++){
			fa[i]=i;
			ok1[i]=0;
			ok2[i]=0;
			if(poi[i].z-r<=0)ok1[i]=1;
			if(poi[i].z+r>=h)ok2[i]=1;
		}
		for(int i=1;i<=n;i++){
			for(int k=i+1;k<=n;k++){
				int p1=getfa(i),p2=getfa(k);
				if(p1==p2)continue;
				if(calc(poi[i],poi[k])-eps<1.0*r*2){
//					puts("YES");
					fa[p2]=p1;
					if(ok1[p2])ok1[p1]=1;
					if(ok2[p2])ok2[p1]=1;
				}
			}
		}
		for(int i=1;i<=n;i++){
			int p=getfa(i);
			if(ok1[p]&&ok2[p]){
				puts("Yes");
				return;
			}
		}
		puts("No");
	}
}pp;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++)scanf("%d%d%d",&poi[i].x,&poi[i].y,&poi[i].z);
		pp.solve();
	}
	return 0;
}

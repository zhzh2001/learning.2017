type pp=^rec;
     rec=record
       d,w:longint;
       next:pp;
     end;
var  i,j,k,l,m,n,h,x,ans,min,minx,miny:longint;
     d:array[1..12,1..12]of longint;
     f:array[1..12]of boolean;
     dep:array[1..12]of longint;
     a:array[1..12]of pp;  p:pp;
procedure dfs(x,l:longint);
var  i:longint; p:pp;
begin
  p:=a[x]^.next; a[x]^.w:=d[x,l]; dep[x]:=dep[l]+1;
  while p<>nil do
  begin
    if p^.d<>l then
    begin dfs(p^.d,x); inc(a[x]^.w,a[p^.d]^.w); end;
    p:=p^.next;
  end;
  inc(ans,a[x]^.w);
end;
procedure dfs2(x,l:longint);
var  p:pp; i:longint;
begin
  dep[x]:=dep[l]+1;
  for i:=1 to n do
    if (i<>x)and(dep[i]<dep[x]) then
    if d[x,i]<5000000 then
    if (dep[i]+1)*(a[x]^.w-d[x,l]+d[x,i])<a[x]^.w*dep[x]then
      begin
        dec(ans,a[x]^.w*dep[x]-(dep[i]+1)*(a[x]^.w-d[x,l]+d[x,i]));
        dep[x]:=dep[i]+1;
      end;
  p:=a[x]^.next;
  while p<>nil do
  begin
    if p^.d<>l then dfs2(p^.d,x);
    p:=p^.next;
  end;
end;
begin
  assign(input,'treasure.in'); assign(output,'treasure.out');
  reset(input);  rewrite(output);
  readln(n,m);
  fillchar(d,sizeof(d),127);
  fillchar(f,sizeof(f),false);
  for i:=1 to n do d[i,i]:=0;
  for i:=1 to n do begin new(a[i]); a[i]^.next:=nil; end;
  for i:=1 to m do
  begin
    readln(j,k,l);
    if l<d[j,k] then begin d[j,k]:=l; d[k,j]:=l; end;
  end;
  f[1]:=true;
  for k:=1 to n-1 do
  begin
    min:=5000000;
    for i:=1 to n do
      if f[i] then
        for j:=1 to n do
          if (not f[j]) then
            if d[i,j]<min then
            begin min:=d[i,j]; minx:=i; miny:=j; end;
    new(p); p^.d:=minx; p^.next:=a[miny]^.next; a[miny]^.next:=p;
    new(p); p^.d:=miny; p^.next:=a[minx]^.next; a[minx]^.next:=p;
    f[miny]:=true;
  end;
  min:=maxlongint;
  for i:=1 to n do
  begin
    ans:=0;
    dep[i]:=-1;
    dfs(i,i);
    dec(ans,a[i]^.w);
    dfs2(i,i);
    if ans<min then min:=ans;
  end;
  writeln(min);
  close(input);  close(output);
end.

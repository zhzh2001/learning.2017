var  i,j,n,t,tt:longint;  h,r:int64;
     x,y,z:array[1..1000]of int64;
     d:array[1..1000,0..1000]of longint;
     dl:array[0..1000]of longint;  o,oo:boolean;
     zd,dd:array[1..1000]of boolean;
procedure dfs(x:longint);
var  j:longint;
begin
  if (o)or(zd[x]) then begin o:=true; exit; end;
  for j:=1 to d[x,0] do
  begin
    if not dd[d[x,j]] then
    begin
      dd[d[x,j]]:=true;
      dfs(d[x,j]);
    end;
  end;
end;
begin
  assign(input,'cheese.in'); assign(output,'cheese.out');
  reset(input);  rewrite(output);
  readln(t);
  for tt:=1 to t do
  begin
    fillchar(d,sizeof(d),false);
    fillchar(zd,sizeof(zd),false);
    fillchar(dl,sizeof(dl),0);
    fillchar(dd,sizeof(dd),false);
    o:=false; oo:=false;
    readln(n,h,r);
    for j:=1 to n do
    begin
      readln(x[j],y[j],z[j]);
      if z[j]<=r then begin inc(dl[0]); dl[dl[0]]:=j; end;
      if h-z[j]<=r then begin zd[j]:=true; oo:=true; end;
    end;
    if (oo)and(dl[0]<>0)then
    begin
      r:=r*2;
      for i:=1 to n do
        for j:=i+1 to n do
          if sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))<=r then
          begin
            inc(d[i,0]); d[i,d[i,0]]:=j;
            inc(d[j,0]); d[j,d[j,0]]:=i;
          end;
      for i:=1 to dl[0] do
        if (not dd[i])and(not o) then
          dfs(dl[i]);
    end;
    if o then writeln('Yes') else writeln('No');
  end;
  close(input);  close(output);
end.

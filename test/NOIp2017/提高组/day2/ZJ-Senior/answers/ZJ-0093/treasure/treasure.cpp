#include<set>
#include<map>
#include<queue>
#include<cmath>
#include<vector>
#include<string>
#include<bitset>
#include<cstring>
#include<complex>
#include<stdio.h>
#include<iostream>
#include<string.h>
#include<algorithm>
#define O edge[i].to
#define NX edge[i].next
#define Val edge[i].val
using namespace std;
const int N=15,M=2005,S=1e4+5,INF=1e9+7;
int head[N],nedge,f[N][S],Res,tot,poi[N],n,m;
struct E{int next,to,val;} edge[M<<1]; int vis[N],Len[N];
inline int Read(){int t=0,f=1; char c=getchar();
	for (;c>57||c<48;c=getchar()) if (c=='-') f=-1;
	for (;c>47&&c<58;c=getchar()) t=(t<<1)+(t<<3)+c-48; return t*f;
}
inline int addline(int x,int y,int val){
	edge[++nedge].to=y,edge[nedge].next=head[x],edge[nedge].val=val,head[x]=nedge;
}
inline int Min(int &x,int y){x=min(x,y);}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=Read(),m=Read();
	for (int i=1;i<=m;i++){
	//	printf("%d\n",i);
		int x=Read(),y=Read(),val=Read();
	//	printf("%d %d %d\n",x,y,val);
		addline(x,y,val),addline(y,x,val);
	}
//	return 0;
	Res=INF;
	memset(f,63,sizeof(f));
	for (int i=1;i<=n;i++)
		f[1][1<<(i-1)]=0;
//	printf("sd"); return 0;
	for (int T=1;T<=n;T++)
		for (int j=1;j<1<<n;j++){
		//	printf("%d %d\n",T,j);
			if (f[T][j]>INF) continue;
			for (int i=1;i<=n;i++)
				vis[i]=(j&(1<<(i-1)));
			for (int i=1;i<=n;i++)
				if (!vis[i]) Len[i]=INF;
			tot=0;
			for (int p=1;p<=n;p++)
				if (vis[p])
					for (int i=head[p];i;i=NX)
						if (!vis[O]){
							if (Len[O]==INF) poi[++tot]=O;
							Min(Len[O],Val);
						}
			for (int i=0;i<1<<tot;i++){int num=0,cost=0;
				for (int p=1;p<=tot;p++)
					if (i&(1<<(p-1))) num+=(1<<(poi[p]-1)),cost+=Len[poi[p]]*T;
				Min(f[T+1][j+num],f[T][j]+cost);
			}
			if (j==(1<<n)-1) Res=min(Res,f[T][j]);
		}			
	printf("%d\n",Res);
}

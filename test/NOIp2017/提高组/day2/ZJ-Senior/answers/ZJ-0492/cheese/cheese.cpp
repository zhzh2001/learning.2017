#include<cstdio>
#include<cmath>
#include<cstring>
#include<iostream>
using namespace std;
int n,h,t,r,father[1001],x[1001],y[1001],z[1001];
bool b[1001][2],yes;
long long dis(int a,int b)
{
	return sqrt((long long)(x[a]-x[b])*(long long)(x[a]-x[b])+(long long)(y[a]-y[b])*(long long)(y[a]-y[b])+(long long)(z[a]-z[b])*(long long)(z[a]-z[b]));
}
int findfather(int a)
{
	if (father[a]==a) return a;
	father[a]=findfather(father[a]);
	return father[a];
}
void merge(int a,int b)
{
	father[findfather(b)]=findfather(a);
	return;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		memset(b,0,sizeof(b));
		yes=0;
		for (int i=1; i<=n; i++)
			father[i]=i;
		for (int i=1; i<=n; i++)
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		for (int i=2; i<=n; i++)
			for (int j=1; j<i; j++)
				if (dis(i,j)<=r+r) merge(i,j);
		for (int i=1; i<=n; i++)
		{
			int fatheri=findfather(i);
			if (z[i]-r<=0) b[fatheri][0]=1;
			if (z[i]+r>=h) b[fatheri][1]=1;
			if (b[fatheri][0] && b[fatheri][1])
			{
				yes=1;
				break;
			}
		}
		if (yes) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
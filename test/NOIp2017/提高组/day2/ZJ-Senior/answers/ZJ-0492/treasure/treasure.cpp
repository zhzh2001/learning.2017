#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
int n,m,x,y,z,ans=1<<30,a[13][13],layer[13];
bool visit[13];
int dfs(int sum,int ans)
{
	if (sum==n) return ans;
	int ans1=1<<30;
	for (int j=1; j<=n; j++)
		if (visit[j])
			for (int i=1; i<=n; i++)
				if (!visit[i] && a[j][i]<1<<30)
				{
					visit[i]=1;
					layer[i]=layer[j]+1;
					ans1=min(ans1,dfs(sum+1,ans+a[j][i]*layer[i]));
					visit[i]=0;
					layer[i]=0;
				}
	return ans1;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(visit,0,sizeof(visit));
	memset(layer,0,sizeof(layer));
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++)
			a[i][j]=1<<30;
	for (int i=1; i<=m; i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=min(a[x][y],z);
		a[y][x]=min(a[y][x],z);
	}
	for (int i=1; i<=n; i++)
	{
		visit[i]=1;
		ans=min(ans,dfs(1,0));
		visit[i]=0;
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
#include <cstdio>
#include <cstring>
using namespace std;
int a[13],h[13],f[13][13],g[13][13];
bool b[13];
int n,m,x,y,z,ans;
void work()
{
	memset(h,0,sizeof(h));
	h[a[1]]=1; int tot=0;
	for (int i=2;i<=n;i++) 
	{
		bool flag=1; int mi=10000001; int cnt;
		for (int j=1;j<i;j++) 
		{
			int dis=f[a[i]][a[j]],root=a[j],x=a[i];
			if (dis<1000000)
			{
				flag=0;
				if (mi>h[root]*dis) mi=dis*h[root],cnt=root;
			}
		}
		if (flag) return;
		h[a[i]]=h[cnt]+1;
		tot+=mi;
	}
	if (tot<ans) ans=tot;
}
void dfs(int x)
{
	if (x>n) 
	{
		work();
		return;
	}
	for (int i=1;i<=n;i++)
	if (!b[i])
	{
		b[i]=1; a[x]=i;
		dfs(x+1);
		b[i]=0; 
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,0x3f,sizeof(f));
	memset(g,0x3f,sizeof(g));
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (f[x][y]>z) f[x][y]=f[y][x]=z;
		g[x][y]=g[y][x]=1;
	}
	ans=10000001;
	dfs(1);
	printf("%d",ans);
	return 0;
}

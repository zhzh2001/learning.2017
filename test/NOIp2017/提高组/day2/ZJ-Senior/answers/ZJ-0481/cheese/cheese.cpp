#include <cstdio>
#include <cmath>
#include <cstring>
using namespace std;
const int N=1050;
const int M=1000500;
int a[N],b[N],c[N],p[M*2],fi[M*2],ne[M*2],la[M*2];
bool vis[N];
int n,h,r,k,t;
double dist(int x,int y)
{
	return sqrt((a[x]-a[y])*(a[x]-a[y])+(b[x]-b[y])*(b[x]-b[y])+(c[x]-c[y])*(c[x]-c[y]));
}
void add(int x,int y)
{
	p[++k]=y; 
	if (!fi[x]) fi[x]=k; else ne[la[x]]=k;
	la[x]=k;
}
void dfs(int x)
{
	vis[x]=1;
	for (int i=fi[x];i;i=ne[i]) if (!vis[p[i]]) dfs(p[i]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%d%d%d",&a[i],&b[i],&c[i]);
		for (int i=1;i<=n;i++) 
		{
			if (c[i]-r<=0) add(n+1,i),add(i,n+1);
			if (c[i]+r>=h) add(i,n+2),add(n+2,i);
		}
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			if (double(dist(i,j))<=double(2*r)) add(i,j),add(j,i);
		dfs(n+1);
		if (vis[n+2]) puts("Yes"); else puts("No");
		k=0;
		memset(vis,0,sizeof(vis)); memset(a,0,sizeof(a));
		memset(fi,0,sizeof(fi)); memset(ne,0,sizeof(ne));
		memset(la,0,sizeof(la));
	}
	return 0;
}

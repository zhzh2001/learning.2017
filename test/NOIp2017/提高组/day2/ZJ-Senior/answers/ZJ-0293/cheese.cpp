#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mo 1000000007
#define ull unsigned long long
using namespace std;
inline void read(int &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void read(ll &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void write(int x)
{
	if (x<0)putchar('-'),x=-x;
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x<0)putchar('-'),x=-x;
	if (x>9)write(x/10);
	putchar(x%10+48);
}
struct Node{
	ll x,y,z;
}a[1010],d[3000010],f[1000010];
ll m,r;
int tot,T,n,h[3000010],b[1010],c[1010];
ull fac(int x,int y)
{
	return (a[x].x-a[y].x)*(a[x].x-a[y].x)+(a[x].y-a[y].y)*(a[x].y-a[y].y)+(a[x].z-a[y].z)*(a[x].z-a[y].z);
}
void add(int x,int y)
{
	d[++tot].x=h[x];
	d[tot].y=y;
	h[x]=tot;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--)
	{
		read(n);read(m);read(r);
		n++;
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		memset(d,0,sizeof(d));
		memset(h,0,sizeof(h));
		memset(f,0,sizeof(f));
		tot=0;
		for (int i=1;i<n;i++)
		read(a[i].x),read(a[i].y),read(a[i].z);
		for (int i=1;i<n;i++)
		for (int j=i+1;j<n;j++)
		if (fac(i,j)<=(ull)4*r*r)add(i,j),add(j,i);
		for (int i=1;i<n;i++)
		if (abs(a[i].z)<=r)add(0,i),add(i,0);
		for (int i=1;i<n;i++)
		if (a[i].z+r>=m)add(i,n);
		for (int i=1;i<=n;i++)
		c[i]=mo;
		int l=0,r=1,t=0;
		f[1].x=0;f[1].y=0;
		b[0]=1;
		c[0]=0;
		while (l<r)
		{
			l++;
			for (int i=h[f[l].x];i;i=d[i].x)
			{
				int v=d[i].y;
				if (v==n)
				{
					printf("Yes\n");
					t=1;
					break;
				}
				if (!b[v]&&f[l].y+1<c[v])f[++r].x=v,c[v]=f[r].y=f[l].y+1,b[v]=1;
			}
			if (t)break;
			b[f[l].x]=0;
		}
		if (t)continue;
		printf("No\n");
	}
	return 0;
}

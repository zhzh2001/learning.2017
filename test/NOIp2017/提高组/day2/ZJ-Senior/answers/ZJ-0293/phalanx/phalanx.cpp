#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mo 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void read(ll &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void write(int x)
{
	if (x<0)putchar('-'),x=-x;
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x<0)putchar('-'),x=-x;
	if (x>9)write(x/10);
	putchar(x%10+48);
}
int n,m,q,x,y;
ll a[5000010];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	if (m>100000)
	{
		ll t=0;
		for (int i=1;i<=n;i++)
		a[++t]=i;
		for (int i=2;i<=m;i++)
		a[++t]=i*n;
		for (int i=1;i<=q;i++)
		{
			read(x);read(y);
			ll z=a[y];
			write(z);puts("");
			for (int j=y;j<t;j++)
			a[j]=a[j+1];
			a[t]=z;
		}
		return 0;
	}
	for (int i=1;i<=n;i++)
	for (int j=1;j<=m;j++)
	x=(i-1)*n+j,a[x]=x;
	for (int i=1;i<=q;i++)
	{
		read(x),read(y);
		ll z=a[(x-1)*n+y];
		write(z);puts("");
		if (y!=m)
		{
			ll t=(x-1)*n;
			for (int j=y;j<m;j++)
			a[t+j]=a[t+j+1];
		}
		if (x!=n)
		{
			ll t=(x-1)*n+m;
			for (int j=0;j<n-x;j++)
			a[t+n*j]=a[t+n*(j+1)];
		}
		a[n*m]=z;
	}
	return 0;
}

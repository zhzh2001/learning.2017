#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define mo 1000000007
using namespace std;
inline void read(int &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void read(ll &x)
{
	x=0;int e=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')e=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	x*=e;
}
inline void write(int x)
{
	if (x<0)putchar('-'),x=-x;
	if (x>9)write(x/10);
	putchar(x%10+48);
}
inline void write(ll x)
{
	if (x<0)putchar('-'),x=-x;
	if (x>9)write(x/10);
	putchar(x%10+48);
}
int n,m,x,y,z;
ll a[20][20],f[20],b[20],c[20];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	ll s=mo;
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
	a[i][j]=mo;
	for (int i=1;i<=m;i++)
	read(x),read(y),read(z),a[x][y]=a[y][x]=a[x][y]>z?z:a[x][y];
	for (int i=1;i<=n;i++)
	{
		memset(b,0,sizeof(b));
		for (int j=1;j<=n;j++)
		f[j]=a[i][j],c[j]=1;
		f[i]=0;b[i]=1;
		ll t=0;
		for (int p=1;p<n;p++)
		{
			ll k=mo,l;
			for (int j=1;j<=n;j++)
			if (!b[j]&&k>f[j]*c[j])
			{
				k=f[j]*c[j];
				l=j;
			}
			t+=k;
			b[l]=1;
			for (int j=1;j<=n;j++)
			if (!b[j]&&f[j]*c[j]>a[l][j]*(c[l]+1))f[j]=a[l][j],c[j]=c[l]+1;
		}
		s=t>s?s:t;
	}
	write(s);
	return 0;
}

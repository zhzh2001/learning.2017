#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

const int inf = (int)1e9;
typedef long long ll;

const int N = 12;
const int M = 1000;
//val <= 500000
int n, m;
int a[M+5], b[M+5], c[M+5];
void input(){
	scanf("%d %d", &n, &m);
	ast(1<=n && n<=N);
	ast(1<=m && m<=M);
	rep(i, 0, m){
		scanf("%d %d %d", &a[i], &b[i], &c[i]);
		ast(1<=a[i] && a[i]<=n);
		ast(1<=b[i] && b[i]<=n);
		ast(1<=c[i] && c[i]<=500000);
		
		--a[i]; --b[i];
	}
}

namespace Program{

int bin[1<<N|5];
int f[1<<N|5][1<<N|5];
inline void chkmin(int &x, int y){
	if(x > y) x = y;
}
void prepare(){
	rep(i, 0, n) bin[1<<i] = i;
	rep(i, 1, 1<<n){
		static int mn[N+5];
		rep(j, 0, n) mn[j] = inf;
		rep(j, 0, m){
			if(i>>a[j]&1) chkmin(mn[b[j]], c[j]); //mn[b[j]] = min(mn[b[j]], c[j]);
			if(i>>b[j]&1) chkmin(mn[a[j]], c[j]);
		}
		
		f[i][0] = 0;
		rep(j, 1, 1<<n){
			int lb = j&-j;
			int u = bin[lb];
			if(mn[u] == inf) f[i][j] = inf;
			else f[i][j] = f[i][j^lb]+mn[u];	
		}
	}
}
int dp[N][1<<N|5]; //[0, n)
void solve(){
	int all = (1<<n)-1;
	rep(i, 0, n) rep(j, 0, 1<<n) dp[i][j] = inf;
	rep(i, 0, n) dp[0][1<<i] = 0;
	rep(i, 0, n-1){ //i->i+1
		rep(j, 0, 1<<n) if(dp[i][j] != inf){
			int S = all^j;
			for(int sub = S;; sub = (sub-1)&S){
				ast((sub&S) == sub);
				if(f[j][sub] < inf) chkmin(dp[i+1][j^sub], dp[i][j]+(i+1)*f[j][sub]);
				if(sub == 0) break;
			}
		}
	}
	int ans = inf;
	rep(i, 0, n) chkmin(ans, dp[i][all]);
	printf("%d\n", ans);
}
void Main(){
	prepare();
	solve();
}

} //namespace Program
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	
	input();
	Program::Main();
	
	return 0;
}

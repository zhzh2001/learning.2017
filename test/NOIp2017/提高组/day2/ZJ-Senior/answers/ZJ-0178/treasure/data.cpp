#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

const int N = 20;
const int M = 2000;
void p(int n, int m){
	freopen("treasure.in", "w", stdout);
	static int index[N];
	rep(i, 0, n) index[i] = i+1;
	rep(i, 0, n*n) swap(index[rand()%n], index[rand()%n]);
	
	int V = 500000;
	
	printf("%d %d\n", n, m);
	rep(i, 1, n) printf("%d %d %d\n", index[i], index[rand()%i], rand()%V+1);
	rep(i, 0, m-(n-1)) printf("%d %d %d\n", index[rand()%n], index[rand()%n], rand()%V+1);
}
#define fp(...) fprintf(in, __VA_ARGS__);
void Fp(int n, int m){
	static int index[N];
	rep(i, 0, n) index[i] = i+1;
	rep(i, 0, n*n) swap(index[rand()%n], index[rand()%n]);
	
	int V = 10;
	
	FILE *in = fopen("treasure.in", "w");
	fp("%d %d\n", n, m);
	rep(i, 1, n) fp("%d %d %d\n", index[i], index[rand()%i], rand()%V+1);
	rep(i, 0, m-(n-1)) fp("%d %d %d\n", index[rand()%n], index[rand()%n], rand()%V+1);
	fclose(in);
}
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	
	p(12, 1000);

/*
	for(int cas = 0;; ++cas){
		printf("Case %d\n", cas);
		Fp(5, 18);
		
		system("time ./treasure");
		system("time ./bf");
		if(system("diff treasure.out bf.out")) break;
	}
*/	
	return 0;
}

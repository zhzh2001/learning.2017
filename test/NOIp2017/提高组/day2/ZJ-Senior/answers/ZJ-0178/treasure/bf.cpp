#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

const int N = 12;
const int M = 1000;
typedef long long ll;
const ll linf = (ll)1e18;
const ll linff = (ll)1e17;
int n, m;
int a[M+5], b[M+5], c[M+5];
void input(){
	scanf("%d %d", &n, &m);
	rep(i, 0, m){
		scanf("%d %d %d", &a[i], &b[i], &c[i]);
		--a[i]; --b[i];
	}
}
ll ans;
bool vis[N+5];
ll dfs(int u, int par, int d, int mask){
	if(vis[u]) return 0;
	vis[u] = true;
	ll res = 0;
	rep(i, 0, m) if(mask>>i&1){
		int v;
		if(a[i] == u) v = b[i];
		else if(b[i] == u) v = a[i];
		else continue;
		if(v == par) continue;
		if(vis[v]) return linff;
		res += (d+1)*(ll)c[i] + dfs(v, u, d+1, mask);
	}
	return res;
}
void work(int x){
	rep(i, 0, 1<<m){
		rep(j, 0, n) vis[j] = false;
		ll tmp = dfs(x, -1, 0, i);
		rep(j, 0, n) if(vis[j] == false) tmp = linff;
		ans = min(ans, tmp);
	}
}
void solve(){
	ans = linf;
	rep(i, 0, n) work(i);
	cout << ans << endl;
}
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("treasure.in", "r", stdin);
	freopen("bf.out", "w", stdout);

	input();
	solve();
	
	return 0;
}

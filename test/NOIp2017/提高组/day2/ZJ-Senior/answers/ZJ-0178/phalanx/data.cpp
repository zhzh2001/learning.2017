#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

const int N5 = (int)5e4;
const int NN = (int)1e5;
const int N = (int)3e5;
void px1(){
	int n = 1, m = 20000, q = 20000;
	printf("%d %d %d\n", n, m, q);
	rep(i, 0, q) printf("1 %d\n", rand()%m+1);
//	int n = N, m = N, q = N;
}
void p50(){
	int n = 1000, m = 1000, q = 500;
	printf("%d %d %d\n", n, m, q);
	rep(i, 0, q) printf("%d %d\n", rand()%n+1, rand()%m+1);
}
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("phalanx.in", "w", stdout);
	
//	px1();
	p50();
	return 0;
}

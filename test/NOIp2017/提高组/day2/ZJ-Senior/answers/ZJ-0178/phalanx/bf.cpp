#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

inline void read(int &x){
	x = 0; char c;
	while(c = getchar(), c<'0' || c>'9');
	do x = (x*10) + (c^48);
	while(c = getchar(), c>='0' && c<='9');
}
template<class TAT>
inline void print(TAT x){
	static char stk[30]; int top = 0;
	while(x) stk[++top] = x%10^48, x /= 10;
	while(top) putchar(stk[top--]);
}
typedef long long ll;
const int N = (int)3e5;
const int M = (int)3e5;
const int Q = (int)3e5;
int n, m, wq;
int qx[Q+5], qy[Q+5];
void input(){
	scanf("%d %d %d", &n, &m, &wq);
	rep(i, 0, wq) read(qx[i]), read(qy[i]);
}
namespace bf{
/*
const int N = 1000;
const int M = 1000;
const int Q = 500;
int a[N+5][M+5];
*/
int a[2][M+5];
void init(){
	int tot = 0;
	rep(i, 1, n+1) rep(j, 1, m+1){
		a[i][j] = ++tot;
		ast(tot == (i-1)*m+j);
	}
}
void response(int x, int y){
	int tmp = a[x][y];
	print(tmp); putchar('\n');
	
	rep(i, y, m) a[x][i] = a[x][i+1]; //(y, m] -> [y, m)
	rep(i, x, n) a[i][m] = a[i+1][m]; //(x, n] -> [x, n)
	a[n][m] = tmp;
}
void Main(){
	init();
	rep(i, 0, wq) response(qx[i], qy[i]);
}

} //namespace bf
/*
long long ans(ID)
2s & 512M
*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("phalanx.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	
	input();
	bf::Main();
	
	return 0;
	
	return 0;
}

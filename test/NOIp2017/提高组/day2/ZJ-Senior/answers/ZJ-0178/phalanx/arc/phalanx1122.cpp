#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

inline void read(int &x){
	x = 0; char c;
	while(c = getchar(), c<'0' || c>'9');
	do x = (x*10) + (c^48);
	while(c = getchar(), c>='0' && c<='9');
}
template<class TAT>
inline void print(TAT x){
	static char stk[30]; int top = 0;
	while(x) stk[++top] = x%10^48, x /= 10;
	while(top) putchar(stk[top--]);
}
typedef long long ll;
const int N = (int)3e5;
const int M = (int)3e5;
const int Q = (int)3e5;
int n, m, wq;
int qx[Q+5], qy[Q+5];
void input(){
	scanf("%d %d %d", &n, &m, &wq);
	rep(i, 0, wq) read(qx[i]), read(qy[i]);
}
namespace P30{ //100W ints

const int N = 1000;
const int M = 1000;
const int Q = 500;
int a[N+5][M+5];
void init(){
	int tot = 0;
	rep(i, 1, n+1) rep(j, 1, m+1){
		a[i][j] = ++tot;
		ast(tot == (i-1)*m+j);
	}
}
void response(int x, int y){
	int tmp = a[x][y];
	print(tmp); putchar('\n');
	
	rep(i, y, m) a[x][i] = a[x][i+1]; //(y, m] -> [y, m)
	rep(i, x, n) a[i][m] = a[i+1][m]; //(x, n] -> [x, n)
	a[n][m] = tmp;
}
void Main(){
	init();
	rep(i, 0, wq) response(qx[i], qy[i]);
}

} //namespace P30
namespace PX1{ //1260W ints

const int L = N*3+233;
ll seq[L]; int len, tot;
ll now;
struct SegmentTree{
#define root 1, 1, len
#define lch (p<<1)
#define rch (p<<1|1)
#define mid (L+R>>1)
#define lson lch, L, mid
#define rson rch, mid+1, R
	int sum[L*4+5]; ll id[L*4+5];
	void build(int p, int L, int R){
		if(L == R){
			if(L <= tot) sum[p] = 1, id[p] = seq[L];
			else sum[p] = 0, id[p] = -1;
			return;
		}
		build(lson);
		build(rson);
		sum[p] = sum[lch]+sum[rch];
	}
	void update(int p, int L, int R, int pos, ll idd){
		if(L == R){
			sum[p] = 1, id[p] = idd;		
			return;
		}
		if(pos <= mid) update(lson, pos, idd);
		else update(rson, pos, idd);
		sum[p] = sum[lch]+sum[rch];
	}
	void query(int p, int L, int R, int cnt){ // long long
		if(L == R){
			now = id[p];
			sum[p] = 0; id[p] = -1;
			return;
		}
		if(cnt <= sum[lch]) query(lson, cnt);
		else query(rson, cnt-sum[lch]);
		sum[p] = sum[lch]+sum[rch];
	}
/*
	ll query(int p, int L, int R, int pos){
		int res;
		if(L == R){
			res = id[p];
			sum[p] = 0; id[p] = -1;
		}else{
			if(pos <= mid) res = query(lson, pos);
			else res = query(rson, pos);
			sum[p] = sum[lch]+sum[rch];
		}
		return res;
	}
*/
}sgt;
void response(int x, int y){
	ast(x == 1);
	sgt.query(root, y);
	print(now); putchar('\n');
	sgt.update(root, ++tot, now);
}
void Main(){
	len = n+m+wq; tot = 0;
	rep(i, 1, m+1) seq[++tot] = i;
	rep(i, 2, n+1) seq[++tot] = (i-1)*(ll)m + m;
	
	sgt.build(root);
	rep(i, 0, wq) response(qx[i], qy[i]);
}

} //namespace PX1

namespace P50{ //2500W ints

const int Q = 500;
int seq[N+5], tot, pos[N+5];
ll a[Q+5][M+5], b[M+5];
void init(){
	int tot = 0;
	rep(i, 0, wq) seq[tot++] = qx[i];
	sort(seq, seq+tot);
	tot = unique(seq, seq+tot) - seq;
	rep(i, 0, tot) pos[seq[i]] = i;
	
	rep(i, 0, tot){
		int x = seq[i];
		rep(j, 1, m) a[i][j] = (x-1)*(ll)m+j;
	}
	rep(i, 1, n+1) b[i] = (i-1)*(ll)m + m;
}
void response(int x, int y){
	ll ans;
	if(y == m){
		ans = b[x];
		rep(i, x, n) b[i] = b[i+1]; //(x, n] -> [x, n)
		b[n] = ans;
	}else{
		int seqid = pos[x];
		ans = a[seqid][y];
		rep(i, y, m){
			if(i == m-1) a[seqid][i] = b[x];
			else a[seqid][i] = a[seqid][i+1];
		}
		rep(i, x, n) b[i] = b[i+1];
		b[n] = ans;
	}
	print(ans); putchar('\n');
}
void Main(){
	init();
	rep(i, 0, wq) response(qx[i], qy[i]);
}

} //namespace P50
/*
long long ans(ID)
2s & 512M
*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	
	input();
	bool x1 = true; rep(i, 0, wq) if(qx[i] != 1) x1 = false;
#ifdef Leefir
//	dig("________________________P30\n"); P30::Main(); return 0;
	dig("________________________P50\n"); P50::Main(); return 0;
//	dig("________________________PX1\n"); PX1::Main(); return 0;
#endif

	if(n<=1000 && m<=1000 && wq<=500) P30::Main();
	else if(x1) PX1::Main();
	else P50::Main();
	
	return 0;
	
	return 0;
}

#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <climits>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define rep(i, a, b) for(int i = a, i##_END_ = b; i < i##_END_; ++i)
#define per(i, a, b) for(int i = (b)-1, i##_BEGIN_ = a; i >= i##_BEGIN_; --i)

#ifdef Leefir
	#define ast(x) assert(x)
	#define dig(...) fprintf(stderr, __VA_ARGS__)
#else 
	#define ast(x) ;
	#define dig(...) ;
#endif

typedef long long ll;
const int N = 1000;
//-1e9 <= xi, yi <= 1e9
int n, h, r;
int x[N+5], y[N+5], z[N+5];
void input(){
	scanf("%d %d %d", &n, &h, &r);
	rep(i, 1, n+1) scanf("%d %d %d", &x[i], &y[i], &z[i]);
}
namespace Program{

bool mp[N+5][N+5];
#define sqr(x) (x)*(ll)(x)
void build(){ // 0, [1, n], n+1
	rep(i, 0, n+2) rep(j, 0, n+2) mp[i][j] = false;
	rep(i, 1, n+1){
		if(z[i]-r<=0 && 0<=z[i]+r) mp[0][i] = mp[i][0] = true;
		if(z[i]-r<=h && h<=z[i]+r) mp[i][n+1] = mp[n+1][i] = true;
	}
	rep(i, 1, n+1) rep(j, i+1, n+1){
		ll d2 = sqr(x[i]-x[j]) + sqr(y[i]-y[j]) + sqr(z[i]-z[j]);
		if(d2 <= sqr(r*2)) mp[i][j] = mp[j][i] = true;
	}
}
bool vis[N+5];
void dfs(int u){
	if(vis[u]) return;
	vis[u] = true;
	rep(i, 0, n+2) if(mp[u][i] && !vis[i]) dfs(i);
}
void Main(){
	build();
	rep(i, 0, n+2) vis[i] = false;
	dfs(0);
	if(vis[n+1]) puts("Yes");
	else puts("No");
}

} //namespace Program
/*

*/
int main(){
#ifdef Leefir
	dig("**************define Leefir****************\n");
#endif
	srand(time(NULL));
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	
	int cas; scanf("%d", &cas);
	while(cas--){
		input();
		Program::Main();
	}
	return 0;
}

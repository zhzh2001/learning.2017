#include<cstdio>
#include<cstring>
using namespace std;

int n,m,i,j,k,h,t,r,x,y,st,p,sum,ans;
int first[2001],edd[2001],next[2001],last[2001],qu[2001];
int pre[2001],q[2001],dis[2001],ff[2001][2001],a[2001],b[2001],fa[2001],gen[2001];

int  min(int x,int y)
{
	if (x<y) return x;
	return y;
}

void add(int x,int y,int z)
{
	qu[++k]=z;
	edd[k]=y;
	!first[x]?first[x]=k:next[last[x]]=k;
	last[x]=k;
}

void spfa(int x)
{
	h=0;t=1;
	q[1]=x;
	while (h<t)
	{
		st=q[++h];
		p=first[st];
		while (p)
		{

			if (dis[st]+pre[st]*qu[p]<dis[edd[p]])
			{
				q[++t]=edd[p];
				dis[q[t]]=dis[st]+pre[st]*qu[p];
				pre[q[t]]=pre[st]+1;
				fa[q[t]]=st;
			
			}
			p=next[p];
		}
	}
	for (int i=1;i<=n;i++) gen[fa[i]]=1;
	sum=0;
	for (int i=1;i<=n;i++)
		if (!gen[i]) sum+=dis[i];
	if (sum<ans) ans=sum;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	k=0;
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++) ff[i][j]=100000000;
	for (i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&p);
		ff[x][y]=min(p,ff[x][y]);
		ff[y][x]=min(p,ff[y][x]);
	}
	for (i=1;i<=n;i++)
		for (j=i;j<=n;j++)
		if (ff[i][j]!=100000000)
	{
		add(i,j,ff[i][j]);
		add(j,i,ff[i][j]);
	}
	ans=100000000;
	for (i=1;i<=n;i++)
	{
		for (j=1;j<=n;j++) dis[j]=100000000;
		memset(pre,0,sizeof pre);
		memset(gen,0,sizeof gen);
		memset(q,0,sizeof q);
		memset(fa,0,sizeof fa);
		dis[i]=0;
		pre[i]=1;
		spfa(i);
	}
	printf("%d",ans);
}

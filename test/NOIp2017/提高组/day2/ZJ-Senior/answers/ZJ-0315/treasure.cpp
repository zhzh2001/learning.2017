#include<bits/stdc++.h>
#define X (1000000000)
using namespace std;
typedef long long  ll;
int n,m;
ll b[13];
ll v[13][13],min1=X,ans;
void dfs(int u,int step) {
	if(step==n-1) {
		min1=min(ans,min1);
		return;
	}
	for(int i=1; i<=n; i++)
		if(!b[i]) {
			ll min2=X;
			int k=0;
			for(int j=1; j<=n; j++)
			{
				if(v[i][j]!=0&&b[j]!=0&&b[j]*v[i][j]<min2) {
					min2=b[j]*v[i][j];
					k=j;
				}
			}
			b[i]=b[k]+1;
			ans+=min2;
			dfs(i,step+1);
			ans-=min2;
			b[i]=0;
		}
}

int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; i++) {
		int x,y;
		ll z;
		scanf("%d%d%lld",&x,&y,&z);
		if(z<v[x][y]||!v[x][y]) {
			v[x][y]=z;
			v[y][x]=z;
		}
	}
	for(int i=1; i<=n; i++) {
		memset(b,0,sizeof(b));
		ans=0;
		b[i]=1;
		dfs(i,0);
		b[i]=0;
	}
	printf("%lld",min1);
	return 0;
}

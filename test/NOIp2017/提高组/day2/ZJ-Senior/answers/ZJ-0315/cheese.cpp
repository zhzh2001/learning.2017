#include<bits/stdc++.h>
#define N (1010)
using namespace std;
typedef long long ll;
int t,n;
int f[N];
ll x[N],y[N],z[N],h,r;
bool flag=false,b[N];
vector<int> path[N];
ll dist(int i,int j) {
	return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
}
void dfs(int u) {
	b[u]=1;
	for(vector<int>::iterator i=path[u].begin(); i!=path[u].end(); i++) {
		if(flag==1)break;
		if(f[*i]==2) {
			flag=1;
			break;
		}
		if(b[*i]!=1)dfs(*i);
	}
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for(int e=1; e<=t; e++) {
		scanf("%d%lld%lld",&n,&h,&r);
		flag=0;
		memset(f,0,sizeof(f));
		memset(b,0,sizeof(b));
		for(int i=1;i<=n;i++)
			path[i].clear();
		
		for(int i=1; i<=n; i++) {
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if(z[i]<=r)f[i]=1;
			if(z[i]>=h-r)f[i]=2;
		}
		for(int i=1; i<=n; i++)
			for(int j=i+1; j<=n; j++) {
				if(dist(i,j)<=2*r) {
					path[i].push_back(j);
					path[j].push_back(i);
				}
			}
		for(int i=1; i<=n; i++) {
			if(f[i]==1)dfs(i);
			if(flag==1)
				break;
		}
		if(flag)printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include <bits/stdc++.h>
#define sqr(x) ((long long)(x)*(x))
#define ST n+1
#define ED n+2
using namespace std;
int T,n,h,r,E,x[200001],y[200001],z[200001];
bool vis[200001];
int fir[200001],nex[200001],to[200001];
void add(int x,int y)
{
	to[++E]=y;nex[E]=fir[x];fir[x]=E;
	to[++E]=x;nex[E]=fir[y];fir[y]=E;
}
void dfs(int now)
{
	vis[now]=1;
	for(int i=fir[now];i;i=nex[i])
	if(!vis[to[i]]) dfs(to[i]);
}
long long dis(int a,int b)
{
	return sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(scanf("%d",&T);T;T--)
	{
		scanf("%d%d%d",&n,&h,&r);
		E=0;
		for(int i=1;i<=n+2;i++)
			fir[i]=0,vis[i]=0;
		for(int i=1;i<=n;i++)
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		for(int i=1;i<=n;i++)
		{
			if(z[i]<=r && z[i]>=-r) add(ST,i);
			if(z[i]>=h-r && z[i]<=h+r) add(i,ED);
		}
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
			if(dis(i,j)<=(long long)4*r*r)
				add(i,j);
		dfs(ST);
		puts(vis[ED]?"Yes":"No");
	}
	return 0;
}

#include <bits/stdc++.h>
#define NO 1000000
using namespace std;
long long NODE,n,m,q;
long long size[NO],fa[NO],c[NO][2],tr[NO];
struct spl
{
	long long rt;
	spl()
	{
		rt=0;
	}
	void up(long long x)
	{
		size[x]=size[c[x][0]]+size[c[x][1]]+1;
	}
	void rot(long long x)
	{
		long long y=fa[x],k=c[y][1]==x;
		if(fa[y]) c[fa[y]][c[fa[y]][1]==y]=x;
		fa[x]=fa[y];
		fa[y]=x;
		if(c[x][!k]) fa[c[x][!k]]=y;
		c[y][k]=c[x][!k];
		c[x][!k]=y;
		up(y);up(x);
	}
	void splay(long long x,long long z=0)
	{
		for(long long y;(y=fa[x])!=z;rot(x))
		if(fa[y]!=z) rot(c[y][1]==x^c[fa[y]][1]==y?x:y);
		if(z==0)
			rt=x;
	}
	bool empty()
	{
		return rt==0;
	}
	long long que(long long x)
	{
		long long now=rt;
		while(x!=size[c[now][0]]+1)
		{
			if(x<=size[c[now][0]]) now=c[now][0];
			else x-=size[c[now][0]]+1,now=c[now][1];
		}
		splay(now);
		return tr[now];
	}
	void add(long long x)
	{
		long long ne=++NODE;
		size[ne]=1;tr[ne]=x;
		fa[ne]=0;c[ne][0]=c[ne][1]=0;
		if(!rt)
		{
			rt=ne;
			return;
		}
		long long now=rt,fat;
		while(now)
		{
			fat=now;
			if(x>tr[now]) now=c[now][1];
			else now=c[now][0];
		}
		fa[ne]=fat;
		if(x>tr[fat]) c[fat][1]=ne;
		else c[fat][0]=ne;
		splay(ne);
	}
};
struct lis
{
	long long len,ori,k,b;
	spl bad;
	vector<long long> extra;
	long long val(long long x)
	{
		return k*(x-1)+b;
	}
	void init(long long l,long long x,long long y)
	{
		len=ori=l;
		k=x;b=y;
	}
	long long pos(long long x)
	{
		if(bad.empty() || x<bad.que(1)) return x;
		long long l=0,r=size[bad.rt]-1;
		while(l<r)
		{
			long long mid=(l+r>>1),tem=(mid==size[bad.rt]-1)?len:(bad.que(mid+2)-mid-2);
			if(x<=tem) r=mid;
			else l=mid+1;
		}
		return x+l+1;
	}
	long long del(long long x)
	{
		long long y=pos(x);
		bad.add(y);
		if(y<=ori)
			return val(y);
		else
			return extra[y-ori-1];
	}
	void pb(long long x)
	{
		len++;
		extra.push_back(x);
	}
} l[300005];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&q);
	for(long long i=1;i<=n;i++)
		l[i].init(m-1,1,1+m*(i-1));
	l[n+1].init(n,m,m);
	for(long long i=1;i<=q;i++)
	{
		long long x,y,ans;
		scanf("%lld%lld",&x,&y);
		if(y==m)
		{
			printf("%lld\n",ans=l[n+1].del(x));
			l[n+1].pb(ans);
		}
		else
		{
			printf("%lld\n",ans=l[x].del(y));
			l[x].pb(l[n+1].del(x));
			l[n+1].pb(ans);
		}
	}
	return 0;
}

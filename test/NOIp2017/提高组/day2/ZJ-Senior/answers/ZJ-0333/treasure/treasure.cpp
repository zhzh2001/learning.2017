#include <bits/stdc++.h>
using namespace std;
int INF,ret,n,m,x,y,z;
int a[13][13],bes[13],dep[13];
void dfs(int now)
{
	if(now>n)
	{
		int sum=0,poi=0;
		for(int i=1;poi<n;i++)
		{
			for(int j=1;j<=n;j++)
				bes[j]=-1;
			for(int j=1;j<=n;j++)
			if(dep[j]==i)
			{
				++poi;
				for(int k=1;k<=n;k++)
				if(a[j][k]!=INF)
					if(bes[k]==-1 || a[j][k]<bes[k])
						bes[k]=a[j][k];
			}
			for(int j=1;j<=n;j++)
			if(dep[j]==i+1)
				if(bes[j]==-1) return;
				else sum+=bes[j]*i;
		}
		if(sum<ret) ret=sum;
		return;
	}
	if(dep[now])
	{
		dfs(now+1);
		return;
	}
	for(int i=2;i<=n;i++)
	{
		dep[now]=i;
		dfs(now+1);
	}
	dep[now]=0;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x3f,sizeof a);
	ret=INF=a[1][1];
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=min(a[x][y],z);
		a[y][x]=min(a[y][x],z);
	}
	for(int i=1;i<=n;i++)
	{
		dep[i]=1;
		dfs(1);
		dep[i]=0;
	}
	printf("%d\n",ret);
	return 0;
}

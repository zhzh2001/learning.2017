#include<stdio.h>
#include<iostream>
using namespace std;
char c;
long long n,h,r,t,f,x[1005],y[1005],z[1005],kk,i,j,s[1005];
int l[1005][1005],book[1005];
void rd(long long &x)
{
	f=1;x=0;c=getchar();
	while(c<'0'||c>'9') {
		if(c=='-') f=-1;c=getchar();
	}
	while(c>='0'&&c<='9') {
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	x*=f;
}
void dfs(int u)
{
	book[u]=1;
	if(u==n+1)
	  kk=1;
	else
	{
		for(i=1;i<=s[u];i++)
		if(book[l[u][i]]==0&&!kk)
		{
			dfs(l[u][i]);
		}
	}
}
long long pf(long long x)
{
	return x*x;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	rd(t);
	while(t--)
	{
		kk=0;
		rd(n),rd(h),rd(r);
		for(i=1;i<=n;i++)
		{
		  rd(x[i]),rd(y[i]),rd(z[i]);
		  s[i]=0;
		  book[i]=0;
		}
		s[0]=book[n+1]=0;
		for(i=1;i<=n;i++)
		for(j=i+1;j<=n;j++)
		{
			if(pf(x[i]-x[j])+pf(y[i]-y[j])+pf(z[i]-z[j])<=r*r*4)
			{
				l[i][++s[i]]=j;
				l[j][++s[j]]=i;
			}
		}
		for(i=1;i<=n;i++)
		{
			if(z[i]<=r)
			  l[0][++s[0]]=i;
			if(z[i]+r>=h)
			  l[i][++s[i]]=n+1;
		}
		if(s[0])
		{
			dfs(0);
		}
		if(kk==1)
		  printf("Yes\n");
		else
		  printf("No\n");
	}
	return 0;
}

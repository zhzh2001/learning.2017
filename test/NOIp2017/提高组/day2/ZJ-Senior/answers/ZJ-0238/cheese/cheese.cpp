#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define per(i,a,b) for (int i=(a);i>=(b);i--)
#define Rep(i,a,b) for (int i=(a);i<(b);i++)
#define Per(i,a,b) for (int i=(a);i>(b);i--)
void read(int&x){
	x=0;int f=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
#define p(x) cout << x << endl;
int T;
#define maxn 1005
ll x[maxn],y[maxn],z[maxn];
int n;ll h,r;
int father[maxn];
int getfather(int x){return (father[x]==x)?x:father[x]=getfather(father[x]);}
void merge(int x,int y){
	//printf("%d %d\n",x,y);
	x=getfather(x),y=getfather(y);
	if (x!=y)father[x]=y;
}
ll sqr(ll x){return x*x;}
bool check(int a,int b){
	return sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b])<=sqr(r)*4;
}
void work(){
	read(n);scanf("%lld%lld",&h,&r);
	rep(i,1,n){
		scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
	}
	rep(i,0,n+1)father[i]=i;
	rep(i,1,n){
		if (z[i]<=r)merge(0,i);if (z[i]>=h-r)merge(n+1,i);
		Rep(j,1,i)if (check(i,j))merge(i,j);
	}
	if (getfather(0)==getfather(n+1))printf("Yes\n");else printf("No\n");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);while (T--)work();return 0;		
}

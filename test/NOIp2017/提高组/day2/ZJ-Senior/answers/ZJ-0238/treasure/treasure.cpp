#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define per(i,a,b) for (int i=(a);i>=(b);i--)
#define Rep(i,a,b) for (int i=(a);i<(b);i++)
#define Per(i,a,b) for (int i=(a);i>(b);i--)
void read(int&x){
	x=0;int f=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
#define p(x) cout << x << endl;
#define maxn 15
#define maxb 4096
int n,e;
int Ed[maxn][maxn];
int A_B[maxn][maxb],B_B[maxb][maxb],f[2][maxb][maxb];
int alex;int minbit[maxb];
int work1(int i,int j){
	int res=alex;
	for (int k=j,t=1;k;k>>=1,t++)
		if (k&1)res=min(res,Ed[i][t]);
	return res;
}
int work2(int i,int j){
	int res=alex;
	for (int k=j,t=1;k;k>>=1,t++)
		if (k&1)res=min(res,A_B[t][j]);
}
//空间
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(Ed,63,sizeof(Ed));alex=Ed[0][0];
	read(n);read(e);
	rep(i,1,n)Ed[i][i]=0;
	rep(i,1,e){
		int x,y,z;read(x);read(y);read(z);
		Ed[x][y]=min(Ed[x][y],z);
		Ed[y][x]=min(Ed[y][x],z);
	}
	Rep(i,0,1<<n)if (i&1)minbit[i]=0;else minbit[i]=minbit[i>>1]+1;
	rep(i,1,n){
		Rep(j,0,1<<n)A_B[i][j]=work1(i,j);
		//Rep(j,0,1<<n)printf("%d ",A_B[i][j]);printf("\n");
	}
	Rep(i,1,1<<n){
		Rep(j,0,1<<n)
			if (B_B[i^(1<<minbit[i])][j]==alex)B_B[i][j]=alex;
			else if (A_B[minbit[i]+1][j]==alex)B_B[i][j]=alex;
			else B_B[i][j]=B_B[i^(1<<minbit[i])][j]+A_B[minbit[i]+1][j];
		//Rep(j,0,1<<n)printf("%d ",B_B[i][j]);puts("");
	}
	if (n==1){printf("0\n");return 0;}
	memset(f,63,sizeof(f));Rep(i,0,n)f[0][1<<i][1<<i]=0;
	int totot=(1<<n)-1;int ans=alex;
	Rep(d,0,n){
		memset(f[!(d&1)],63,sizeof(f[!(d&1)]));
		Rep(i,0,1<<n){
			int else_num=totot^i;
			for (int j=i;j;j=((j-1)&i))if (f[d&1][i][j]!=alex){
				//printf("%d %d %d %d\n",d,i,j,f[d][i][j]);
				for (int k=else_num;k;k=((k-1)&else_num))if (B_B[k][j]!=alex)
					f[!(d&1)][i^k][k]=min(f[!(d&1)][i^k][k],f[d&1][i][j]+(d+1)*B_B[k][j]);
			}
		}
		Rep(i,0,1<<n)ans=min(ans,f[!(d&1)][totot][i]);
	}
	printf("%d\n",ans);return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define per(i,a,b) for (int i=(a);i>=(b);i--)
#define Rep(i,a,b) for (int i=(a);i<(b);i++)
#define Per(i,a,b) for (int i=(a);i>(b);i--)
void read(int&x){
	x=0;int f=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
#define p(x) cout << x << endl;
#define maxn 1005
int n,m,q;
int a[maxn][maxn];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("_phalanx.out","w",stdout);
	read(n);read(m);read(q);int cnt=0;
	rep(i,1,n)rep(j,1,m)a[i][j]=++cnt;
	int ans;
	rep(i,1,q){
		int x,y;read(x);read(y);printf("%d\n",ans=a[x][y]);
		rep(j,y+1,m)a[x][j-1]=a[x][j];
		rep(j,x+1,n)a[j-1][n]=a[j][n];
		a[n][m]=ans;
	}
	return 0;
}

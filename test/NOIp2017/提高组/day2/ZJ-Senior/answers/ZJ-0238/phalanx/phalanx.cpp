#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pi;
#define fi first
#define se second
#define pb push_back
#define mk make_pair
#define rep(i,a,b) for (int i=(a);i<=(b);i++)
#define per(i,a,b) for (int i=(a);i>=(b);i--)
#define Rep(i,a,b) for (int i=(a);i<(b);i++)
#define Per(i,a,b) for (int i=(a);i>(b);i--)
void read(int&x){
	x=0;int f=1;char ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-')ch=getchar();
	if (ch=='-')f=-1,ch=getchar();
	while (ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	x*=f;
}
#define p(x) cout << x << endl;
#define maxn 300005
int n,m,q;
struct Node{
	int lowbit(int x){return x&(-x);}
	int all_del,all_num;vector<int> sum;vector<ll> num;
	void clear(){all_del=all_num=0;sum.pb(0);num.pb((ll)0);}
	void add(int x){for (int i=x;i;i-=lowbit(i))sum[i]++;}
	int ask(int x){int res=0;for (int i=x;i<sum.size();i+=lowbit(i))res+=sum[i];return res;}
	void insert(ll x){all_num++;num.pb((ll)x);sum.pb(0);}
	void remove(int x){
		int l=1,r=num.size()-1;
		while (l<=r){
			int mid=(l+r)>>1;
			if (mid-(all_del-ask(mid+1))>=x)r=mid-1;
			else l=mid+1;
		}
		add(l);all_del++;all_num--;
	}
	ll query(int x){
		int l=1,r=num.size()-1;
		while (l<=r){
			int mid=(l+r)>>1;
			if (mid-(all_del-ask(mid+1))>=x)r=mid-1;
			else l=mid+1;
		}
		return num[l];
	}
	void print(){
		Rep(i,0,num.size())printf("%lld ",num[i]);printf("\n");
		rep(i,1,all_num)printf("%lld ",query(i));printf("\n");
	}
}b[maxn],d;

#define logn 20
struct Tree{
	int l,r,sze;
}T[maxn*logn];
int cnt;int rot[maxn];
int Newnode(){return ++cnt;}
void insert(int&x,int l,int r,int v){
	//printf("insert %d %d %d\n",l,r,v);
	if (!x)x=Newnode();T[x].sze++;if (l==r)return;int mid=(l+r)>>1;
	if (v<=mid)insert(T[x].l,l,mid,v);
	else insert(T[x].r,mid+1,r,v);
}
int query(int&x,int l,int r,int v){
	//printf("%d %d %d %d\n",l,r,v,T[x].sze);
	if (l==r)return l;int mid=(l+r)>>1;
	if (mid-l+1-T[T[x].l].sze>=v)return query(T[x].l,l,mid,v);
	else return query(T[x].r,mid+1,r,v-(mid-l+1-T[T[x].l].sze));
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	rep(i,1,n)b[i].clear();d.clear();
	rep(i,1,n)d.insert((ll)i*m);
	//d.print();
	
	rep(i,1,q){
		int x,y;read(x);read(y);
		if (y==m){
			ll res=d.query(x);
			//printf("remove %d ",x);
			d.remove(x);d.insert(res);
			printf("%lld\n",res);
		}
		else{
			ll res;
			if (y<m-T[rot[x]].sze){
				res=query(rot[x],1,m-1,y);
				insert(rot[x],1,m-1,res);
				res+=(ll)m*(x-1);
			}
			else{
				res=b[x].query(y-(m-1-T[rot[x]].sze));
				b[x].remove(y-(m-1-T[rot[x]].sze));
			}
			ll now=d.query(x);
			
			//printf("remove %d ",x);
			d.remove(x);
			//printf("now %lld\n",now);
			b[x].insert(now);d.insert(res);
			printf("%lld\n",res);
		}
		//d.print();
	}	
	return 0;
}

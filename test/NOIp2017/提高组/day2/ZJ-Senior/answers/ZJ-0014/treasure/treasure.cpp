#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	f=1;x=0;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f; 
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}
template<class T>void ptk(T x){prin(x);putchar(' ');}
template<class T>void ptn(T x){prin(x);putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

const int N=15,S=4100,inf=1061109567;

int dis[N][N],n,m;
struct P_0{
	int res;
	void dfs(int x,int par,int dep){
		rep(i,0,n)if(dis[x][i]!=inf&&i!=par){
			res+=dis[x][i]*dep;
			dfs(i,x,dep+1);
		}
	}
	void work(){
		int ans=inf;
		rep(i,0,n){
			res=0;
			dfs(i,-1,1);
			if(ans>res)ans=res;
		}
		ptn(ans);
	}
}P0;
struct P_1{
	int micost[N][S],Cost[S][S],dp[N][S];
	void work(){
		{
			memset(micost,63,sizeof micost);
			int j,k;
			rep(s,1,1<<n){
				for(j=0;j<n;++j)if(!(s>>j&1)){
					for(k=0;k<n;++k)if(s>>k&1)
						Min(micost[j][s],dis[k][j]);
				}
			}
		}
		{
			int all,j,k,full=(1<<n)-1;
			rep(s,1,1<<n){
				for(all=s^full,j=all;j;j=(j-1)&all){
					for(k=0;k<n;++k)if(j>>k&1){
						Cost[s][j]+=micost[k][s];
						if(Cost[s][j]>=inf){
							Cost[s][j]=inf;
							break;
						}
					}
				}
			}
		}
		{
			memset(dp,63,sizeof dp);
			rep(i,0,n)dp[0][1<<i]=0;
			int t,all,j,full=(1<<n)-1;
			rep(s,1,1<<n){
				for(t=0;t<n;++t){
					int &res=dp[t][s];
					if(res<inf){
						for(all=s^full,j=all;j;j=(j-1)&all)if(Cost[s][j]!=inf){
							Min(dp[t+1][s|j],res+Cost[s][j]*(t+1));
						}
					}
				}
			}
			int ans=inf;
			rep(i,0,n+1)Min(ans,dp[i][full]);
			ptn(ans);
		}
	}
}P1;
int main(){
//	debug(sizeof(P1)>>20);
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(dis,63,sizeof dis);
	rd(n),rd(m);
	for(int a,b,c,i=0;i<m;++i){
		rd(a),rd(b),rd(c);
		--a;--b;
		Min(dis[a][b],c);
		Min(dis[b][a],c);
	}
	if(0);
	else if(m==n-1)P0.work();
	else P1.work();
	return 0;
}

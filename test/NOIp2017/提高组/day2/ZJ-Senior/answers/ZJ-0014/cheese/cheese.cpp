#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	f=1;x=0;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f; 
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}
template<class T>void ptk(T x){prin(x);putchar(' ');}
template<class T>void ptn(T x){prin(x);putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

const int N=1005;

int n,H,R,fa[N];
bool mark[N];
int Find(int x){
	if(x==fa[x])return x;
	return fa[x]=Find(fa[x]);
}
struct node{
	int x,y,z;
}arr[N];
void solve(){
	rd(n),rd(H),rd(R);
	rep(i,1,n+1){
		fa[i]=i;
		mark[i]=false;
		rd(arr[i].x),rd(arr[i].y),rd(arr[i].z);
	}
	ll lim=(ll)4*R*R,x,y,z;
	rep(i,1,n+1)rep(j,1,i){
		int a=Find(i),b=Find(j);
		if(a!=b){
			x=arr[i].x-arr[j].x;
			x=x*x;
			if(x>lim)continue;
			y=arr[i].y-arr[j].y;
			y=y*y;
			if(y>lim)continue;
			x+=y;
			if(x>lim)continue;
			z=arr[i].z-arr[j].z;
			z=z*z;
			if(z>lim)continue;
			x+=z;
			if(x>lim)continue;
			fa[a]=b;
		}
	}
	rep(i,1,n+1)
		if(arr[i].z-R<=0&&arr[i].z+R>=0)mark[Find(i)]=true;
	rep(i,1,n+1)if(mark[Find(i)]){
		if(arr[i].z-R<=H&&arr[i].z+R>=H){
			puts("Yes");
			return ;
		}
	}
	puts("No");
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;rd(T);
	while(T--)solve();
	return 0;
}

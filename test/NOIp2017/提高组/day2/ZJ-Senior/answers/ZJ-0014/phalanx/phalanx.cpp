#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>
#include<set>

using namespace std;

#define fi first
#define se second
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
#define bug(x) cerr<<#x<<" = "<<(x)<<" "
#define debug(x) cerr<<#x<<" = "<<(x)<<"\n"

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

template<class T>void rd(T &x){
	static int f;static char c;
	f=1;x=0;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do x=x*10+(c&15);
	while(c=getchar(),c>47);
	x*=f; 
}
template<class T>void prin(T x){
	if(x<0)x=-x,putchar('-');
	else if(!x){putchar('0');return ;}
	static int stk[100],tp;
	while(x)stk[tp++]=x%10,x/=10;
	while(tp)putchar(stk[--tp]^48);
}
template<class T>void ptk(T x){prin(x);putchar(' ');}
template<class T>void ptn(T x){prin(x);putchar('\n');}
template<class T>void Min(T &a,T b){if(b<a)a=b;}
template<class T>void Max(T &a,T b){if(a<b)a=b;}

const int N=(int)3e5+5;

int n,m,q;
pii Q[N];
struct P_0{
	int id[1005][1005];
	void work(){
		rep(i,1,n+1)rep(j,1,m+1)
			id[i][j]=(i-1)*m+j;
		rep(i,1,q+1){
			int x=Q[i].fi,y=Q[i].se,v;
			v=id[x][y];
			ptn(v);
			rep(j,y,m)id[x][j]=id[x][j+1];
			rep(j,x,n)id[j][m]=id[j+1][m];
			id[n][m]=v;
		}
	}
}P0;
struct P_1{
	ll id[N*3];
	int bit[N*3],allc,mx;
	void add(int x,int v){
		for(;x<mx;x+=x&-x)bit[x]+=v;
	}
	int query(int x){
		int res=0;
		for(;x;x-=x&-x)res+=bit[x];
		return res; 
	}
	
	void work1(){
		allc=m;mx=m+q+1;
		rep(i,1,m+1)id[i]=i,add(i,1);
		int y,v,l,r,mid;
		rep(i,1,q+1){
			y=Q[i].se;
			l=1,r=allc;
			while(l<=r){
				mid=(l+r)>>1;
				if(query(mid)>=y)v=mid,r=mid-1;
				else l=mid+1;
			}
			ptn(id[v]);
			add(v,-1);
			id[++allc]=id[v];
			add(allc,1);
		}
	}
	void work2(){
		mx=m+n+q+1;allc=0;
		rep(i,1,m+1)id[++allc]=i,add(allc,1);
		rep(i,2,n+1)id[++allc]=(ll)i*m,add(allc,1);
		int y,v,l,r,mid;
		rep(i,1,q+1){
			y=Q[i].se;
			l=1,r=allc;
			while(l<=r){
				mid=(l+r)>>1;
				if(query(mid)>=y)v=mid,r=mid-1;
				else l=mid+1;
			}
			ptn(id[v]);
			add(v,-1);
			id[++allc]=id[v];
			add(allc,1);
		}
	}
}P1;
bool judge_x_1(){
	rep(i,1,q+1)if(Q[i].fi!=1)return false;
	return true;
}
struct node{
	int x,y,id;
	bool operator<(const node&A)const{
		if(x!=A.x)return x<A.x;
		return id<A.id;
	}
}arr[N];
struct P_2{
	int bit[N],a[N];
	void bit_add(int x,int v){
		for(;x<=m;x+=x&-x)bit[x]+=v;
	}
	int bit_query(int x){
		int res=0;
		for(;x;x-=x&-x)res+=bit[x];
		return res;
	}
	ll id[N*10];
	int L[N],R[N],BIT[N*10],mx,allc,nowpos[N];
	void BIT_add(int x,int v){
		for(;x<mx;x+=x&-x)BIT[x]+=v;
	}
	int BIT_query(int x){
		int res=0;
		for(;x;x-=x&-x)res+=BIT[x];
		return res;
	}
	int query(int l,int r){
		if(l>r)return 0;
		return BIT_query(r)-BIT_query(l-1);
	}
	void init(){
		mx=N*7;
		rep(i,1,q+1)arr[i]=(node){Q[i].fi,Q[i].se,i};
		sort(arr+1,arr+1+q);
		int now=1,cnt,far;
		rep(x,1,n+1){
			cnt=0;
			while(arr[now].x<x&&now<=q)++now;
			far=now;
			rep(k,now,q+1){
				if(arr[k].x!=x)break;
				far=k;
				int l=arr[k].y,r=m-1,res=m;
				while(l<=r){
					int mid=(l+r)>>1;
					if(mid+bit_query(mid)>=arr[k].y)r=mid-1,res=mid;
					else l=mid+1;
				}
				if(res<m){
					a[cnt++]=res,bit_add(res,-1);
				}
			}
			L[x]=allc+1;
			int num=1;
			sort(a,a+cnt);
			rep(i,0,cnt){
				if(num<a[i]){
					id[++allc]=num+(ll)(x-1)*m;
					BIT_add(allc,a[i]-num);
				}
				id[++allc]=a[i]+(ll)(x-1)*m;
				num=a[i]+1;
				BIT_add(allc,1);
				bit_add(a[i],1);
			}
			if(num<m){
				id[++allc]=num+(ll)(x-1)*m;
				BIT_add(allc,m-num);
			}
			nowpos[x]=allc;
			allc+=far-now+2;
			R[x]=allc;
		}
	}
	struct LAST{
		ll id[N*2];
		int bit[N*2],mx,allc;
		void bit_add(int x,int v){
			for(;x<mx;x+=x&-x)bit[x]+=v;
		}
		int bit_query(int x){
			int res=0;
			for(;x;x-=x&-x)res+=bit[x];
			return res;
		}
		void init(){
			mx=n+q+1;
			rep(x,1,n+1)id[++allc]=(ll)x*m,bit_add(allc,1);
		}
		ll Find(int lim,ll p){
			int res,l=1,r=allc;
			while(l<=r){
				int mid=(l+r)>>1;
				if(bit_query(mid)>=lim)res=mid,r=mid-1;
				else l=mid+1;
			}
			if(p==-1)p=id[res];
			bit_add(res,-1);
			id[++allc]=p;
			bit_add(allc,1);
			return id[res];
		}
	}last;
	void work(){
		init();
		last.init();
		rep(i,1,q+1){
			int x=Q[i].fi,y=Q[i].se,v;
			if(y==m){
				ptn(last.Find(x,-1));
			}else {
				int l=L[x],r=R[x];
				while(l<=r){
					int mid=(l+r)>>1;
					if(query(L[x],mid)>=y){
						v=mid;r=mid-1;
					}else l=mid+1;
				}
				ptn(id[v]);
				BIT_add(v,-1);
				id[++nowpos[x]]=last.Find(x,id[v]);
				BIT_add(nowpos[x],1);
			}
		}
	}
}P2;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	rd(n),rd(m),rd(q);
	rep(i,1,q+1)rd(Q[i].fi),rd(Q[i].se);
	if(0);
	else if(n<=1000&&m<=1000&&q<=500)P0.work();
	else if(n==1)P1.work1();
	else if(judge_x_1())P1.work2();
	else P2.work();
	return 0;
}

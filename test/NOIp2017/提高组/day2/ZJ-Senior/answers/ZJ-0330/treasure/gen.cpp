#include<cstdio>
#include<algorithm>
#include<time.h>
using namespace std;
#define ll long long
#define rt register int
#define rep(i,x,y) for(rt i = (x); i<=(y); ++i)
#define per(i,x,y) for(rt i = (x); i>=(y); --i)
#define travel(i,x) for(rt i=h[x]; i; i=pre[i])
int main(){
	srand(time(0));
	freopen("treasure.in", "w", stdout);
	int n = 12, m = 1000;
	printf("%d %d\n", n, m);
	rep(i, 1, m){
		int x, y;
		while((x=rand()%n+1)==(y=rand()%n+1));
		printf("%d %d %d\n", x, y, ((rand()<<15)+rand())%500000+1);
	}
	return 0;
}

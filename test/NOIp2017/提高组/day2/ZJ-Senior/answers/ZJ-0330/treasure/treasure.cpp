#include<cstdio>
#include<algorithm>
#include<string.h>
using namespace std;
#define ll long long
#define rt register int
#define rep(i,x,y) for(rt i = (x); i<=(y); ++i)
#define per(i,x,y) for(rt i = (x); i>=(y); --i)
#define travel(h,x) for(rt tt=x, h=b[tt&-tt]; h; tt^=1<<h, h=b[tt&-tt])
const int N = 14, M = (1<<12)+5;
int f[M][N][N], siz[M], a[N][N], b[M], n, m, ans=1<<30;
inline void update(int &x, int y){
	x=(x>y?y:x);
}
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	rep(i, 0, n) b[1<<i]=i;
	memset(a, 0x3f3f3f3f, sizeof a);
	rep(i, 1, m){
		static int u, v, W;
		scanf("%d%d%d", &u, &v, &W);
		--u, --v;
		a[u][v]=a[v][u]=min(a[u][v], W);
	}
	int lim=(1<<n)-1, nn = n-1;
	rep(i, 1, lim) rep(j, 0, nn) siz[i]+=((i&(1<<j))>0);
	
	memset(f, 0x3f3f3f3f, sizeof f);
	rep(i, 0, nn) rep(dis, 0, nn) f[1<<i][i][dis]=0;
	rep(i, 1, lim) rep(h, 0, nn) if(i&(1<<h)){
		rep(dis, 0, n-siz[i]){
//			printf("%d %d %d\n", i, h, dis);
			static int tmp;
			tmp=i^(1<<h);
			for(rt j = tmp; j; j=(j-1)&tmp)
//				rep(h2, 0, nn) if(j&(1<<h2) && a[h][h2]<=500000)
				travel(h2, j) if(a[h][h2]<=500000)
					update(f[i][h][dis], f[j][h2][dis+1]+f[i^j][h][dis]+(dis+1)*a[h][h2]);
			rep(h2, 0, nn) if(h2!=h && (i&(1<<h2)) && a[h][h2]<=500000)
				update(f[i][h][dis], f[i^(1<<h)][h2][dis+1]+(dis+1)*a[h][h2]);
		}
	}
	rep(i, 0, nn) ans=min(ans, f[lim][i][0]);
	printf("%d", ans);
	return 0;
}

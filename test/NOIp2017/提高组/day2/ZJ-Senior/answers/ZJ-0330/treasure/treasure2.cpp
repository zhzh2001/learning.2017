#include<cstdio>
#include<algorithm>
#include<string.h>
using namespace std;
#define ll long long
#define rt register int
#define rep(i,x,y) for(rt i = (x); i<=(y); ++i)
#define per(i,x,y) for(rt i = (x); i>=(y); --i)
#define travel(i,x) for(rt i=h[x]; i; i=pre[i])
const int N = 14, M = (1<<12)+5;
int f[M][N][N], a[N][N], n, m, ans=1<<30;
inline void update(int &x, int y){
	x=(x>y?y:x);
}
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(a, 0x3f3f3f3f, sizeof a);
	rep(i, 1, m){
		static int u, v, W;
		scanf("%d%d%d", &u, &v, &W);
		a[u][v]=a[v][u]=min(a[u][v], W);
	}
	int lim=(1<<n)-1;
	memset(f, 0x3f3f3f3f, sizeof f);
	rep(i, 1, n) rep(dis, 0, n-1) f[(1<<i-1)][i][dis]=0;
	rep(i, 1, lim) rep(h, 1, n) if(i&(1<<h-1)){
		rep(dis, 0, n-1){
//			printf("%d %d %d\n", i, h, dis);
			for(rt j = (i-1)&i; j; j=(j-1)&i) if(!(j&(1<<h-1)))
				rep(h2, 1, n) if(j&(1<<h2-1) && a[h][h2]<=500000)
					update(f[i][h][dis], f[j][h2][dis+1]+f[i^j][h][dis]+(dis+1)*a[h][h2]);
			rep(h2, 1, n) if(h2!=h && (i&(1<<h2-1)) && a[h][h2]<=500000)
				update(f[i][h][dis], f[i^(1<<h-1)][h2][dis+1]+(dis+1)*a[h][h2]);
		}
	}
	rep(i, 1, n) ans=min(ans, f[lim][i][0]);
	printf("%d", ans);
	return 0;
}

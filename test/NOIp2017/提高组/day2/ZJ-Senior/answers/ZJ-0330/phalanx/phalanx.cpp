#include<cstdio>
#include<algorithm>
#include<string.h>
#include<vector>
#include<ctype.h>
using namespace std;
#define ll long long
#define rt register int
#define rep(i,x,y) for(rt i = (x); i<=(y); ++i)
#define per(i,x,y) for(rt i = (x); i>=(y); --i)
#define travel(i,x) for(rt i=h[x]; i; i=pre[i])
inline void read(int &x) {
	static char ch;
	while(!isdigit(ch=getchar()));
	x=ch^'0';
	while(isdigit(ch=getchar())) x=(x+(x<<2)<<1)+(ch^'0');
}
inline void print(ll x){
	static int sb[20], tot;
	tot = 0;
	while(x) sb[++tot]=x%10, x/=10;
	while(tot) putchar((char)('0'+sb[tot--]));
}
const int N = 300005, M = 300000*2*21;
int s[M], lson[M], rson[M], rot[N], cnt, n, m, q, p;
vector<ll> ex[N], f;
int query_modify(int l, int r, int &t, int pos) {
	--s[t?t:t=++cnt];
	if(l==r) return l;
	int mid=l+r>>1, lsize=s[lson[t]]+mid-l+1;
//	printf("%d %d %d\n", l, r, lsize);
	return (pos<=lsize ? query_modify(l, mid, lson[t], pos) : query_modify(mid+1, r, rson[t], pos-lsize));
}
int main() { 
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n), read(m), read(q);
	while(q--) {
		static int x, y;
		read(x), read(y);
//		puts("--------------------");
		if(y==m) {
			ll tmp=query_modify(1, 600000, p, x);
			tmp=(tmp>n?f[tmp-n-1]:tmp*m);
			print(tmp), putchar('\n');
			f.push_back(tmp);
		} else {
			ll tmp1=query_modify(1, 600000, rot[x], y), tmp2=query_modify(1, 600000, p, x);
//			printf("[%d][%d]\n", tmp1, tmp2);
			tmp1=(tmp1>=m?ex[x][tmp1-m]:(ll)(x-1)*m+tmp1);
			tmp2=(tmp2>n?f[tmp2-n-1]:tmp2*m);
			print(tmp1), putchar('\n');
			ex[x].push_back(tmp2);
			f.push_back(tmp1);
		}
	}
//	printf("%d\n", cnt);
	return 0;
}

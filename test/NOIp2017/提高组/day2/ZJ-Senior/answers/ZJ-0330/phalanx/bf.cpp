#include<cstdio>
#include<algorithm>
#include<string.h>
#include<vector>
#include<time.h>
using namespace std;
#define ll long long
#define rt register int
#define rep(i,x,y) for(rt i = (x); i<=(y); ++i)
#define per(i,x,y) for(rt i = (x); i>=(y); --i)
#define travel(i,x) for(rt i=h[x]; i; i=pre[i])
#define rd ((rand()<<16)^(rand()<<1)^(rand()&1))
const int N = 1005;
int n, m, q, a[N][N], cnt;
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	rep(i, 1, n) rep(j, 1, m) a[i][j]=++cnt;
	while(q--){
		static int x, y;
		scanf("%d%d", &x, &y);
		int tmp=a[x][y];
		rep(i, y, m-1) a[x][i]=a[x][i+1];
		rep(i, x, n-1) a[i][m]=a[i+1][m];
		a[n][m]=tmp;
		printf("%d\n", tmp); 
	}
	return 0;
}

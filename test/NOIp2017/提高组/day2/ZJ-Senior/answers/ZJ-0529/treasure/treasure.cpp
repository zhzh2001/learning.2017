#include<cstdio>
#include<cstring>
using namespace std;
struct edge{
	int x,y,z;
}e[500];
int n,m,a[20][20],minn,cnt,h[20],len,num;
bool bod[20],bob[20][20],bo[20];
void dfs1(int x,int y,int x1,int num1,int len1)
{
	if (x1==y) 
	{
		num=num1;len=len1;return;
	}
	for (int i=1;i<=n;i++)
		if (!bo[i]&&bob[x1][i]) 
		{
			bo[i]=true;
			dfs1(x,y,i,num1+1,a[x1][i]);
			bo[i]=false;
		}
}
void dfs(int x,int s)
{
	if (s==n-1)
	{
		int t=0;
		for (int i=1;i<=n;i++)
			if (x!=i)
			{
				memset(bo,false,sizeof(bo));
				bo[x]=true;
				len=0;num=0;dfs1(x,i,x,0,0);
				if (len>10000000||num>12) return;
				t+=len*num;
			}
		if (t<minn&&t!=0) minn=t;
	}
	else 
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (i!=j&&!bob[i][j])
				{
					if (bod[i]&&!bod[j]) 
					{
						bob[i][j]=true;bob[j][i]=true;bod[j]=true;dfs(x,s+1);
						bob[i][j]=false;bob[j][i]=false;bod[j]=false;
					}
					if (bod[j]&&!bod[i])
					{
						bob[i][j]=true;bob[j][i]=true;bod[i]=true;dfs(x,s+1);
						bob[i][j]=false;bob[j][i]=false;bod[i]=false;
					}
				}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	minn=100000000;
	memset(a,0x7f,sizeof(a));
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if (a[x][y]>z) {a[x][y]=z;a[y][x]=z;}
	}
	for (int i=1;i<=n;i++)
	{
		bod[i]=true;
		dfs(i,0);
		bod[i]=false;
	}
	printf("%d",minn);
	return 0;
}

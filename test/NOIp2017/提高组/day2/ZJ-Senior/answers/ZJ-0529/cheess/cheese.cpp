#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;
struct edge{
	int next,to;
}e[1000001];
struct lyf{
	int x,y,z;
}a[1001];
int t,line[10001],h[1001];
bool bo[1001];
ll calc(int x){return x*x;}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		int n,h1,r,top=0,tail=0,cnt=0;
		scanf("%d%d%d",&n,&h1,&r);
		memset(e,0,sizeof(e));
		memset(h,0,sizeof(h));
		memset(bo,false,sizeof(bo));
		for (int i=1;i<=n;i++)
		{
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
			if (a[i].z-r<=0&&a[i].z+r>=0) line[++top]=i,bo[i]=true;
		}
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			{
				if (sqrt(calc(a[i].x-a[j].x)+calc(a[i].y-a[j].y)+calc(a[i].z-a[j].z))<=2*r) 
				{
					e[++cnt].to=j;e[cnt].next=h[i];h[i]=cnt;
					e[++cnt].to=i;e[cnt].next=h[j];h[j]=cnt;
				}
			}
		
		while (++tail<=top)
		{
			for (int i=h[line[tail]];i;i=e[i].next)
			{
				if (!bo[e[i].to])
				{
					line[++top]=e[i].to;
					bo[e[i].to]=true;
				}
			}
		}
		bool bo1=false;
		for (int i=1;i<=n;i++)
			if (a[i].z+r>=h1&&a[i].z-r<=h1&&bo[i]) {bo1=1;break;}
		if (bo1) printf("Yes\n");else printf("No\n"); 
	}
	return 0;
}

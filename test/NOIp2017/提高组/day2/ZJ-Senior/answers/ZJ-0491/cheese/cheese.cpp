#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<iostream>
#define MAXN 1000
using namespace std;
int queue[MAXN*10+10];
int map[MAXN+10][MAXN+10];
double x[MAXN+10],y[MAXN+10],z[MAXN+10]; 
int n,T;
double h,r;
bool flag[MAXN+10];
double dis(double x1,double y1,double z1,double x2,double y2,double z2){
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
}
void work(){
	memset(flag,0,sizeof(flag));
	int he=1,t=0;
	for (int i=1;i<=n;i++){
		if (z[i]-r<=0.0001){
			t++;queue[t]=i;
			flag[i]=1;
		}
	}
	while (he<=t){
		if (z[queue[he]]+r>=h-0.0001){
			//cout<<"Got out from "<<queue[he]<<endl;
			printf("Yes\n");
			return;
		}
		for (int i=1;i<=n;i++){
			if (flag[i]) continue;
			if (map[queue[he]][i]) {
				//cout<<"Got "<<i<<" from "<<queue[he]<<endl;
				t++;queue[t]=i;
				flag[i]=1;
			}
		}
		he++;
	}
	printf("No\n");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	for (int kkk=1;kkk<=T;kkk++){
		scanf("%d%lf%lf",&n,&h,&r);
		memset(map,0,sizeof(map));
		for (int i=1;i<=n;i++){
			scanf("%lf%lf%lf",&x[i],&y[i],&z[i]);
		}
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				map[i][j]=(dis(x[i],y[i],z[i],x[j],y[j],z[j])<=2*r+0.0001);
				//cout<<"map["<<i<<"]["<<j<<"]="<<map[i][j]<<endl;	
			}
		}
		work();
	}
	return 0;
}


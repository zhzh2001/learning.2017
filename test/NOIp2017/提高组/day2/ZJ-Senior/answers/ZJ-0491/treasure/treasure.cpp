#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<iostream>
#define MAXN 12
#define INF 0x3f3f3f3f
using namespace std;
int map[MAXN+10][MAXN+10];
struct node{
	int cnt,pos;
}queue[MAXN+10];
bool flag[MAXN+10];
int dep[MAXN+10],siz[MAXN+10],fa[MAXN+10];
struct road{
	int tv,v,next;
}roads[MAXN*MAXN*MAXN+10];
int cnt=0;
int list[MAXN+10];
void add(int u,int v,int w){
	cnt++;
	roads[cnt].tv=v;
	roads[cnt].v=w;
	roads[cnt].next=list[u];
	list[u]=cnt;
}
int n,m,x,y,tt;
void dfs(int x){
	//cout<<"Dfs at "<<x<<endl;
	for (int w=list[x];w;w=roads[w].next){
		if (roads[w].tv==fa[x]) continue;
		fa[roads[w].tv]=x;
		dep[roads[w].tv]=dep[x]+1;
		dfs(roads[w].tv);
		siz[x]+=siz[roads[w].tv]+roads[w].v;
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(map,0x3f,sizeof(map));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&tt);
		map[x][y]=min(map[x][y],tt);
		map[y][x]=map[x][y];
	}
	/*for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			cout<<"map["<<i<<"]["<<j<<"]="<<map[i][j]<<endl;
		}
	}*/
	int ans=INF;
	for (int kkk=1;kkk<=n;kkk++){
		int h=1,t=1;
		cnt=0;
		memset(list,0,sizeof(list));
		memset(flag,0,sizeof(flag));
		memset(fa,0,sizeof(fa));
		memset(dep,0,sizeof(dep));
		memset(siz,0,sizeof(siz));
		queue[h].cnt=1,queue[h].pos=kkk;
		flag[kkk]=1;
		int ta=0;
		while (h<=t){
			for (int i=1;i<=n;i++){
				if ((!flag[i])&&map[queue[h].pos][i]<INF){
					ta+=queue[h].cnt*map[queue[h].pos][i];
					//cout<<"Added a road from "<<queue[h].pos<<" to "<<i<<endl; 
					//cout<<"It contributed "<<queue[h].cnt*map[queue[h].pos][i]<<endl;
					add(queue[h].pos,i,map[queue[h].pos][i]);
					add(i,queue[h].pos,map[queue[h].pos][i]);
					flag[i]=1;
					t++;
					queue[t].pos=i;queue[t].cnt=queue[h].cnt+1;
				}
			}
			h++;
		}
		ans=min(ans,ta);
		dep[kkk]=1;
		dfs(kkk);
		for (int i=1;i<=n;i++){
			//cout<<"siz["<<i<<"]="<<siz[i]<<endl;
		}
		for (int iii=1;iii<=n;iii++)
		for (int ttt=1;ttt<=n;ttt++){
			for (int i=1;i<=n;i++){
				//if (i==kkk) continue;
				for (int j=1;j<=n;j++){
					if (i==j) continue;
					if (j==kkk) continue;
					if (j==fa[i]) continue;
					if (map[i][j]>=INF) continue;
					//cout<<"map["<<i<<"]["<<j<<"]*dep["<<i<<"]+(dep["<<i<<"]+1-dep["<<j<<"])*siz["<<j<<"]="<<map[i][j]*dep[i]+(dep[i]+1-dep[j])*siz[j]<<endl;
					//cout<<"map["<<j<<"]["<<fa[j]<<"]*dep["<<fa[j]<<"]="<<map[j][fa[j]]*dep[fa[j]]<<endl;
					if (map[i][j]*dep[i]+(dep[i]+1-dep[j])*siz[j]<map[j][fa[j]]*dep[fa[j]]){
						//ta+=map[i][j]*dep[i]+(dep[i]+1-dep[j])*siz[j]-map[j][fa[j]]*dep[fa[j]];
						int tt=fa[j];
						while (tt!=0){
							siz[tt]-=siz[j];
							tt=fa[tt];
						}
						fa[j]=i;
						dep[j]=dep[i]+1;
						tt=i;
						while (tt!=0){
							siz[tt]+=siz[j];
							tt=fa[tt];
						}
						
					}
				}
			}
		}
		ta=0;
		for (int i=1;i<=n;i++){
			ta+=map[i][fa[i]]*dep[fa[i]];
		}
	}
	printf("%d",ans);
	return 0;
}

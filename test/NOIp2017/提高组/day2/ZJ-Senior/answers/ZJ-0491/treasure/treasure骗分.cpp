#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<iostream>
#define MAXN 12
#define INF 0x3f3f3f3f
using namespace std;
int map[MAXN+10][MAXN+10];
struct node{
	int cnt,pos;
}queue[MAXN+10];
bool flag[MAXN+10];
int ans=INF,n,m,x,y,tt;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(map,0x3f,sizeof(map));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&tt);
		map[x][y]=min(map[x][y],tt);
		map[y][x]=map[x][y];
	}
	for (int kkk=1;kkk<=n;kkk++){
		int h=1,t=1;
		memset(flag,0,sizeof(flag));
		queue[h].cnt=1,queue[h].pos=kkk;
		flag[kkk]=1;
		int ta=0;
		while (h<=t){
			for (int i=1;i<=n;i++){
				if ((!flag[i])&&map[queue[h].pos][i]<INF){
					ta+=queue[h].cnt*map[queue[h].pos][i];
					flag[i]=1;
					t++;
					queue[t].pos=i;queue[t].cnt=queue[h].cnt+1;
				}
			}
			h++;
		}
		ans=min(ans,ta);
	}
	printf("%d",ans);
	return 0;
}

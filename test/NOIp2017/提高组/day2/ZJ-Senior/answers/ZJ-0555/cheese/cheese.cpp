#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int n,top,S,E;
ll r,x[2001],y[2001],z[2001];
int nxt[3000001],poi[3000001],head[30001],cnt;
bool vis[30001];
inline ll sqr(ll x){return 1LL*x*x;}
inline double dis(int a,int b)
{
	return sqrt(1.0*sqr(x[a]-x[b])+1.0*sqr(y[a]-y[b])+1.0*sqr(z[a]-z[b]));
}
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void bfs()
{
	For(i,1,E)	vis[i]=0;
	queue<int> q;
	q.push(S);vis[S]=1;
	while(!q.empty())
	{
		int x=q.front();q.pop();
		for(int i=head[x];i;i=nxt[i])
			if(!vis[poi[i]])	vis[poi[i]]=1,q.push(poi[i]);
	}
	if(vis[E])	puts("Yes");	else puts("No");
}
int T;
int main()
{
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();top=read();r=read();
		S=n+1;E=S+1;
		For(i,1,E)	head[i]=0;cnt=0;
		For(i,1,n)
		{
			x[i]=read();y[i]=read();z[i]=read();
			if(z[i]+r>=top)	add(i,E);
			if(z[i]-r<=0)	add(S,i);
		}
		For(i,1,n)
			For(j,i+1,n)
				if(dis(i,j)<=2.0*r)	add(i,j),add(j,i);
		bfs();
	}
}

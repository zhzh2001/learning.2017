#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(c<'0'||c>'9')	{if(c=='-')	f=-1;	c=getchar();}
	while(c>='0'&&c<='9')	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int n,m,q[201],cnt,poi[201],nxt[201],head[201],vis[201],tot,F[201],sz[201];
int d[201][201],ans=1e9;
struct edge{int x,y;}	e[2001];
inline int get(int x){return x==F[x]?F[x]:get(F[x]);}
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline int dfs1(int x,int fa,int dep)
{
	int sum=0;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		sum+=d[x][poi[i]]*dep;
		sum+=dfs1(poi[i],x,dep+1);
	}
	return sum;
}
inline void check()
{
	
	For(i,1,n)	head[i]=0;cnt=0;
	For(i,1,n-1)	add(e[q[i]].x,e[q[i]].y),add(e[q[i]].y,e[q[i]].x);
	For(i,1,n)
	{
		ans=min(ans,dfs1(i,i,1));
	}
}
inline void dfs(int x,int alr)
{	
	if(alr==n-1)	{check();return;}
	if(x==tot+1)	return;
	int tx=get(e[x].x),ty=get(e[x].y);
	if(tx==ty)
	{
		dfs(x+1,alr);
		return;
	}
	int tmp=F[tx],tmp1=F[ty],tsz=0;
	bool flag=0;
	if(sz[tx]<sz[ty])	F[tx]=ty,sz[ty]+=sz[tx],tsz=sz[tx],sz[tx]=0;
		else	F[ty]=tx,sz[tx]+=sz[ty],tsz=sz[ty],sz[ty]=0,flag=1;
	q[alr+1]=x;
	dfs(x+1,alr+1);
	if(flag)	F[ty]=tmp1,sz[tx]-=tsz,sz[ty]+=tsz;	else F[tx]=tmp,sz[ty]-=tsz,sz[tx]+=tsz;
	dfs(x+1,alr);
}
int x,y;
int main()
{
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	n=read();m=read();
	For(i,1,n)	F[i]=i,sz[i]=1;
	For(i,1,n)	For(j,1,n)	d[i][j]=1e8;
	For(i,1,m)	x=read(),y=read(),d[x][y]=min(d[x][y],(int)read()),d[y][x]=d[x][y];
	For(i,1,n)
		For(j,i+1,n)	if(d[i][j]!=1e8)	e[++tot].x=i,e[tot].y=j;
	dfs(1,0);
	writeln(ans);
}
/*
4 5                                    
1 2 1 
1 3 3 
1 4 1 
2 3 4 
3 4 1 

*/

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#define ll long long
using namespace std;
int n,h,fa[1005],kind[1005];
ll r,x[1005],y[1005],z[1005];
int getfa(int x){
	if (x!=fa[x]) fa[x]=getfa(fa[x]);
	return fa[x];
}
bool dis(int a,int b){
	ll t1=(x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b]);
	if (t1>r*r*4LL) return 0;
	t1-=r*r*4LL;
	if ((z[a]-z[b])*(z[a]-z[b])>-t1) return 0;
	return 1;
}
void comb(int x,int y){
	int t1=getfa(x),t2=getfa(y);
	if (t1!=t2) fa[t2]=t1;
}
void solve(){
	scanf("%d%d%lld",&n,&h,&r);
	for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
	for (int i=1;i<=n;i++){
		fa[i]=i;
		if (z[i]-r<=0) kind[i]=1; else
		if (z[i]+r>=h) kind[i]=2; else kind[i]=0;
	}
	for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
			if (dis(i,j)) comb(i,j);
	for (int i=1;i<n;i++)
		if (kind[i]==1){
			for (int j=i+1;j<=n;j++)
				if (kind[j]==2 && getfa(i)==getfa(j)){
					puts("Yes");return;
				}
		}
	puts("No");
}
int main(){
/*	ll t=9000000000000000000LL;
	cout<<t<<endl;*/
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;scanf("%d",&cas);
	while (cas--) solve();
	return 0;
}

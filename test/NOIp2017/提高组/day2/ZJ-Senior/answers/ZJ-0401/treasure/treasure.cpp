#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
#define ll long long
using namespace std;
const int 
	inf=100000000;
int n,m,x[1005],y[1005];
int STA;
int f[4200],Q[4200];
int mp[15][15],dis[15];
bool VIS[15];
int BFS(int s,int t){
/*	memset(VIS,0,sizeof(VIS));
	for (int i=1;i<=n;i++) dis[i]=inf;
	dis[s]=0;
	queue<int>q;
	while (!q.empty()) q.pop();
	q.push(s);
	while (!q.empty()){
		int u=q.front();q.pop();
		VIS[u]=0;
		for (int i=1;i<=n;i++)
			if (STA&(1<<(i-1)) && mp[u][i]<inf){
				if (dis[i]>dis[u]+2){
					dis[i]=dis[u]+2;
					if (!VIS[i]) VIS[i]=1,q.push(i);
				}
			}
	}*/
	memset(VIS,0,sizeof(VIS));
	queue<int>q;
	while (!q.empty()) q.pop();
	q.push(s),dis[s]=0;VIS[s]=1;
	while (!q.empty()){
		int u=q.front();q.pop();
		for (int i=1;i<=n;i++)
			if ((STA&(1<<(i-1))) && mp[u][i]<inf && !VIS[i]){
				dis[i]=dis[u]+1;
				q.push(i),VIS[i]=1;
			}
	}
	return dis[t];
}
void solve(int Start){
	for (int sta=0;sta<=(1<<n)-2;sta++){
		if (f[sta]>=inf) continue;
		for (int i=0;i<n;i++)
			if (sta&(1<<i)){
				STA=sta;
				int tt=BFS(Start,i+1)+1;
				if (tt>=inf) continue;
				for (int j=1;j<=n;j++)
					if (!(sta&(1<<(j-1))) && mp[i+1][j]<inf){
						f[sta+(1<<(j-1))]=min(f[sta+(1<<(j-1))],f[sta]+mp[i+1][j]*tt);
					}
			}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	int x,y,z;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) mp[i][j]=inf;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		mp[x][y]=min(mp[x][y],z);
		mp[y][x]=mp[x][y];
	}
	int ans=inf;
	for (int i=1;i<=n;i++){
		for (int j=0;j<=(1<<n)-1;j++) f[j]=inf;
		f[1<<(i-1)]=0;
		solve(i);
		ans=min(ans,f[(1<<n)-1]);
	}
	printf("%d\n",ans);
	return 0;
}

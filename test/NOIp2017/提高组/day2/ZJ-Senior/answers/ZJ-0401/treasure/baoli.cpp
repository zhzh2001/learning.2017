#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
const int 
	inf=1000000;
int n,m,x[1005],y[1005];
int goal,step,STA,num1,ans;
int f[4200],Q[4200];
int mp[15][15],dis[15];
bool vis[4200],VIS[15];
int dijkstra(int s,int t){
	memset(VIS,0,sizeof(VIS));
	for (int i=1;i<=n;i++) dis[i]=inf;
	dis[s]=0;
	for (int j=1;j<=num1;j++){
		int mini=0,MIN=inf;
		for (int i=1;i<=n;i++)
			if (STA&(1<<(i-1)) && !VIS[i] && MIN>dis[i])
				MIN=dis[i],mini=i;
		VIS[mini]=1;
		for (int i=1;i<=n;i++)
			if (STA&(1<<(i-1)) && mp[mini][i]!=inf)
				dis[i]=min(dis[i],dis[mini]+1);
	}
	return dis[t];
}
void solve(int Start){
	for (int sta=0;sta<=(1<<n)-1;sta++){
		if (f[sta]>=inf) continue;
		for (int i=0;i<n;i++)
			if (sta&(1<<i)){
				STA=sta;
				int tt=dijkstra(Start,i+1);
				if (tt>=inf) continue;
				tt++;
				for (int j=1;j<=n;j++)
					if (!(sta&(1<<(j-1))) && mp[i+1][j]<inf){
						f[sta+(1<<(j-1))]=min(f[sta+(1<<(j-1))],f[sta]+mp[i+1][j]*tt);
					}
			}
	}
}
int main(){
	scanf("%d%d",&n,&m);
	int x,y,z;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) mp[i][j]=inf;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		mp[x][y]=min(mp[x][y],z);
		mp[y][x]=mp[x][y];
	}
	ans=inf;
	for (int i=1;i<=n;i++){
		for (int j=0;j<=(1<<n)-1;j++) f[j]=inf;
		f[(1<<(i-1))]=0;
		solve(i);
		ans=min(ans,f[(1<<n)-1]);
	}
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
#define ll long long
using namespace std;
int n,m,q;
int a[1005][1005];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000){
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
		int x,y;
		while (q--){
			scanf("%d%d",&x,&y);
			printf("%d\n",a[x][y]);
			int t=a[x][y];
			for (int i=y;i<m;i++)
				a[x][i]=a[x][i+1];
			a[x][m]=0;
			for (int i=x;i<n;i++)
				a[i][m]=a[i+1][m];
			a[n][m]=t;
		}
	} else{
		int x,y;
		while (q--){
			scanf("%d%d",&x,&y);
			printf("%d\n",1);
		}
	}
	return 0;
}

type
  arr=array[1..12]of longint;
  ar=array[1..24]of longint;
var
  n,m,u,v,w,tt,i,head,tail,ed,tot,j:longint;
  ans,now,mins:int64;
  e:array[1..12,1..12]of longint;
  flag:boolean;
  d,q:array[1..12]of longint;
  next,_to,hed,len:ar;
  deep,vv,ff:array[1..12]of longint;
  f,t,dis:array[1..66]of longint;
  vis:array[1..12]of boolean;
  tp:array[1..12]of arr;
  tnext,tto,thed,tlen:array[1..12]of ar;
  fa:arr;

function min(a,b:longint):longint;
begin
  if a=0 then exit(b);
  if b=0 then exit(a);
  if a<b then exit(a)
         else exit(b);
end;

function gfa(x:longint):longint;
begin
  if fa[x]<>x then fa[x]:=gfa(fa[x]);
  exit(fa[x]);
end;

procedure merge(x,y:longint);
begin
  fa[gfa(y)]:=fa[gfa(x)];
end;

procedure add_edge(x,y,w:longint);
begin
  inc(ed);
  _to[ed]:=y;
  len[ed]:=w;
  next[ed]:=hed[x];
  hed[x]:=ed;
  inc(ed);
  _to[ed]:=x;
  len[ed]:=w;
  next[ed]:=hed[y];
  hed[y]:=ed;
end;

{procedure delete_edge(x,y:longint);
begin
  hed[x]:=next[ed];
  dec(ed);
  hed[y]:=next[ed];
  dec(ed);
end; }

{procedure bfs(x:longint);
var
  u,v:longint;
begin
  q[1]:=x;
  head:=1;
  tail:=2;
  fillchar(vis,sizeof(vis),true);
  fillchar(d,sizeof(d),0);
  vis[x]:=false;
  repeat
    u:=q[head];
    inc(head);
    for v:=1 to n do
      if vis[v] and (e[u,v]<>0) then
       begin
         vis[v]:=false;
         q[tail]:=v;
         inc(tail);
         d[v]:=d[u]+1;
         now:=now+d[v];
       end;
  until head=tail;
end; }

procedure build_tree(u:longint);
var
  i,v:longint;
begin
  i:=hed[u];
  while i<>0 do
   begin
     v:=_to[i];
     if v<>ff[u] then
      begin
        deep[v]:=deep[u]+1;
        ff[v]:=u;
        vv[v]:=len[i];
        build_tree(v);
      end;
     i:=next[i];
   end;
end;

procedure dfs(x,w:longint);
var
  i,j:longint;
begin
  if x=n then
   begin
     for i:=1 to n do
      begin
        ff[i]:=0;
        deep[i]:=0;
        vv[i]:=0;
        build_tree(i);
        ans:=0;
        for j:=1 to n do
          ans:=ans+vv[j]*deep[j];
        if ans<mins then
         begin
           //writeln(x,' ',w,' ',ans);
           mins:=ans;
         end;
      end;
     exit;
   end;
  tp[x]:=fa;
  tnext[x]:=next;tto[x]:=_to;thed[x]:=hed;tlen[x]:=len;
  for i:=w to tot do
   begin
     if i+n-x-1>tot then exit;
     if gfa(f[i])=gfa(t[i]) then continue;
     merge(f[i],t[i]);
     add_edge(f[i],t[i],dis[i]);
     dfs(x+1,i+1);
     fa:=tp[x];
     next:=tnext[x];_to:=tto[x];hed:=thed[x];len:=tlen[x];
     dec(ed,2);
     //delete_edge(f[i],t[i]);
   end;
end;

begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  readln(n,m);
  flag:=true;
  for i:=1 to m do
   begin
     read(u,v,w);
     if (tt<>0)and(w<>tt) then flag:=false;
     e[u,v]:=min(e[u,v],w);
     e[v,u]:=min(e[v,u],w);
     tt:=w;
   end;
  {if flag then
   begin
     ans:=maxlongint;
     for i:=1 to n do
      begin
        now:=0;
        bfs(i);
        if now<ans then ans:=now;
      end;
     writeln(ans*w);
   end
          else
   begin}
     for i:=1 to n do
       for j:=i+1 to n do
         if e[i,j]<>0 then
          begin
            inc(tot);
            f[tot]:=i;
            t[tot]:=j;
            dis[tot]:=e[i,j];
          end;
     for i:=1 to n do
       fa[i]:=i;
     mins:=maxlongint;
     dfs(1,1);
     writeln(mins);
   //end;
  close(input);close(output);
end.
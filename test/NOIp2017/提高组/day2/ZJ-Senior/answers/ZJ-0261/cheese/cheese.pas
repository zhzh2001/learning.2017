var
  n,i:longint;
  q:array[1..2000]of longint;
  vis:array[1..2000]of boolean;
  x,y,z:array[1..2000]of int64;
  tail,head,t:longint;
  h,r:int64;

function dis(u,v:longint):boolean;
var
  t:int64;
begin
  t:=(x[u]-x[v])*(x[u]-x[v])+(y[u]-y[v])*(y[u]-y[v])+(z[u]-z[v])*(z[u]-z[v]);
  exit(t<=4*r*r);
end;

function bfs(n:longint):boolean;
var
  u,v,i:longint;
begin
  tail:=1;
  fillchar(q,sizeof(q),0);
  fillchar(vis,sizeof(vis),true);
  for i:=1 to n do
    if z[i]<=r then
     begin
       q[tail]:=i;
       vis[i]:=false;
       inc(tail);
     end;
  head:=1;
  if head=tail then exit(false);
  bfs:=false;
  repeat
    u:=q[head];
    //writeln(u);
    inc(head);
    if z[u]+r>=h then exit(true);
    for v:=1 to n do
      if (vis[v])and(dis(u,v)) then
       begin
         vis[v]:=false;
         q[tail]:=v;
         inc(tail);
       end;
  until head=tail;
end;

begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  readln(t);
  while t>0 do
   begin
     dec(t);
     readln(n,h,r);
     for i:=1 to n do
       readln(x[i],y[i],z[i]);
     if bfs(n) then writeln('Yes')
               else writeln('No');
   end;
  close(input);close(output);
end.
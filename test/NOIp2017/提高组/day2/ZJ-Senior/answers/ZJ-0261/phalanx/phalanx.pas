var
  n,m,q,i,j,tx,ty:longint;
  x,y:array[1..100000]of longint;

begin
  assign(input,'phalanx.in');reset(input);
  assign(output,'phalanx.out');rewrite(output);
  readln(n,m,q);
  for i:=1 to q do
    readln(x[i],y[i]);
  for i:=1 to q do
   begin
     tx:=x[i];ty:=y[i];
     for j:=i-1 downto 1 do
      begin
        if (tx=n)and(ty=m) then
         begin
           tx:=x[j];ty:=y[j];
         end
        else if (tx=x[j])and(ty>=y[j]) then inc(ty)
        else if (ty=n)and(tx>=x[j]) then inc(tx);
      end;
     writeln((tx-1)*m+ty);
   end;
  close(input);close(output);
end.
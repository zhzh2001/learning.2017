#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,ans,mins,mini,step[15],dis[15][15];
bool vis[15];
void dfs(int x,int sum)
{
	if (sum>=ans) return;
	if (x>n)
	{
		ans=sum;
		return;
	}
	for (int i=1; i<=n; i++)
		if (!vis[i])
		{
			mins=1e9+7;
			for (int j=1; j<=n; j++)
				if (vis[j] && dis[j][i]<1e9+7 && mins>step[j]*dis[j][i]) mins=step[j]*dis[j][i],mini=j;
			if (mins<1e9+7)
			{ 
				vis[i]=1;
				step[i]=step[mini]+1;
				dfs(x+1,sum+mins);
				vis[i]=0;
			}
		}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++)
			if (i!=j) dis[i][j]=1e9+7;
			else dis[i][j]=1;
	for (int i=1; i<=m; i++)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		dis[u][v]=min(dis[u][v],w);
		dis[v][u]=min(dis[v][u],w);
	}
	ans=1e9+7;
	for (int i=1; i<=n; i++)
	{
		step[i]=1;
		vis[i]=1;
		dfs(2,0);
		vis[i]=0;
	}
	printf("%d\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

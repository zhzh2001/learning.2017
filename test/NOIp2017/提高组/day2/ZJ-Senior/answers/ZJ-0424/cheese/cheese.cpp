#include<cstdio>
#include<algorithm>
using namespace std;
int T,n,h,hd,tl,cnt;
int q[1000005],vet[3000005],Next[3000005],head[1005];
long long r,x[1005],y[1005],z[1005];
bool vis[1005];
long long calc(int i,int j)
{
	return (x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
}
void add(int x,int y)
{
	vet[++cnt]=y;
	Next[cnt]=head[x];
	head[x]=cnt;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%lld",&n,&h,&r);
		for (int i=1; i<=n; i++)
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		cnt=0;
		for (int i=0; i<=n+1; i++) head[i]=0;
		for (int i=1; i<=n; i++)
		{
			if (z[i]<=r) add(0,i);
			if (z[i]+r>=h) add(i,n+1);
		}
		for (int i=1; i<=n; i++)
			for (int j=i+1; j<=n; j++)
				if (calc(i,j)<=r*r*4LL) add(i,j),add(j,i);
		for (int i=0; i<=n+1; i++) vis[i]=0;
		hd=tl=1;
		q[hd]=0;
		vis[0]=1;
		while (hd<=tl)
		{
			int u=q[hd++];
			if (u==n+1) break;
			for (int i=head[u]; i; i=Next[i])
			{
				int v=vet[i];
				if (!vis[v])
				{
					vis[v]=1;
					q[++tl]=v;
				}
			}
		}
		if (vis[n+1]) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

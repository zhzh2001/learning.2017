#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,q,tmp,x[300005],y[300005],a[1005][1005],b[1000005],c[1000005];
bool flag;
int lowbit(int x)
{
	return x&(-x);
}
long long query(int x)
{
	long long sum=0;
	for (int i=x; i; i-=lowbit(i))
		sum=sum+c[i];
	return sum;
}
void add(int x)
{
	for (int i=x; i<=n+m+q; i+=lowbit(i)) c[i]++;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	flag=1;
	for (int i=1; i<=q; i++)
	{
		scanf("%d%d",&x[i],&y[i]);
		if (x[i]!=1) flag=0; 
	}
	if (n<=1000 && m<=1000)
	{
		for (int i=1; i<=n; i++)
			for (int j=1; j<=m; j++) a[i][j]=(i-1)*m+j;
		for (int i=1; i<=q; i++)
		{
			tmp=a[x[i]][y[i]];
			printf("%d\n",tmp);
			for (int j=y[i]+1; j<=m; j++) a[x[i]][j-1]=a[x[i]][j];
			for (int j=x[i]+1; j<=n; j++) a[j-1][m]=a[j][m];
			a[n][m]=tmp;
		}
	}
	else
		if (flag)
		{
			for (int i=1; i<=m; i++) b[i]=i;
			for (int i=2; i<=n; i++) b[i+m-1]=i*m;
			for (int i=1; i<=q; i++)
			{
				int l=1,r=n+m-2+i;
				while (l<r)
				{
					int mid=(l+r)>>1;
					if (query(mid)+y[i]<=mid) r=mid;
					else l=mid+1;
				}
				tmp=b[l];
				printf("%d\n",tmp);
				add(l);
				b[n+m-1+i]=tmp;
			}
		}
	fclose(stdin); fclose(stdout);
	return 0;
}
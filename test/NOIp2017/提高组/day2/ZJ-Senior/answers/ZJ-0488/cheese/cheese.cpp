#include<bits/stdc++.h>
using namespace std;
#define M 1005
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));
}
int n,h,r;
int x[M],y[M],z[M],mark[M],f=0;
ll mul(int x,int y){
	return (ll)(x-y)*(x-y);
}
void dfs(int i){
	if(f||mark[i])return;
	mark[i]=1;
	if(z[i]+r>=h)f=1;
	for(int j=1;j<=n;j++){
		ll dis=mul(x[i],x[j])+mul(y[i],y[j])+mul(z[i],z[j]);
		if(dis<=4ll*r*r)dfs(j);
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		f=0;
		scanf("%d %d %d",&n,&h,&r);
		for(int i=1;i<=n;i++)scanf("%d %d %d",&x[i],&y[i],&z[i]);
		memset(mark,0,sizeof(mark));
		for(int i=1;i<=n;i++)if(z[i]<=r)dfs(i);
		if(f)puts("Yes");
		else puts("No");
	}
	return 0;
}

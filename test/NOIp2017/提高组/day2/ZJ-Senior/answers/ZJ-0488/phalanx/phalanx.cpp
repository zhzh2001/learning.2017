#include<bits/stdc++.h>
using namespace std;
#define M 300005
#define ll long long
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));
}
int n,m,q;
struct P30{
	int a[1005][1005];
	void solve(){
		int num=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)a[i][j]=++num;
		while(q--){
			int x,y;
			scanf("%d %d",&x,&y);
			int tmp=a[x][y];
			for(int i=y;i<m;i++)a[x][i]=a[x][i+1];
			for(int i=x;i<n;i++)a[i][m]=a[i+1][m];
			a[n][m]=tmp;
			printf("%d\n",tmp);
		}
	}
}p30;
int x[M],y[M];
struct P60{
	ll treex[M<<3];int cntx[M<<3],mpx[M<<3];
	void upx(int p){
		cntx[p]=cntx[p<<1]+cntx[p<<1|1];
	}
	void buildx(int p=1,int l=1,int r=m+q){
		if(l==r){
			if(l<=m)treex[p]=l,cntx[p]=1;
			mpx[p]=1;
			return;
		}
		int mid=l+r>>1;
		buildx(p<<1,l,mid);
		buildx(p<<1|1,mid+1,r);
		upx(p);
	}
	int Findx(int p,int num){
		if(mpx[p])return p; 
		if(cntx[p<<1]>=num)return Findx(p<<1,num);
		return Findx(p<<1|1,num-cntx[p<<1]);
	}
	void Upx(int p){
		p>>=1;
		while(p)upx(p),p>>=1;
	}
	void updatex(int x,int v,int p=1,int l=1,int r=m+q){
		if(l==r){
			cntx[p]=1;
			treex[p]=v;
			return;
		}
		int mid=l+r>>1;
		if(x<=mid)updatex(x,v,p<<1,l,mid);
		else updatex(x,v,p<<1|1,mid+1,r);
		upx(p);
	}
	ll treey[M<<3];int cnty[M<<3],mpy[M<<3];
	void upy(int p){
		cnty[p]=cnty[p<<1]+cnty[p<<1|1];
	}
	void buildy(int p=1,int l=1,int r=n+q){
		if(l==r){
			if(l<=n)treey[p]=(ll)l*m,cnty[p]=1;
			mpy[p]=1;
			return;
		}
		int mid=l+r>>1;
		buildy(p<<1,l,mid);
		buildy(p<<1|1,mid+1,r);
		upy(p);
	}
	int Findy(int p,int num){
		if(mpy[p])return p; 
		if(cnty[p<<1]>=num)return Findy(p<<1,num);
		return Findy(p<<1|1,num-cnty[p<<1]);
	}
	void Upy(int p){
		p>>=1;
		while(p)upy(p),p>>=1;
	}
	void updatey(int x,int v,int p=1,int l=1,int r=n+q){
		if(l==r){
			cnty[p]=1;
			treey[p]=v;
			return;
		}
		int mid=l+r>>1;
		if(x<=mid)updatey(x,v,p<<1,l,mid);
		else updatey(x,v,p<<1|1,mid+1,r);
		upy(p);
	}
	void solve(){
		buildx();
		buildy();
		for(int i=1;i<=q;i++){
			if(n==1){
				int p=Findx(1,y[i]);
				int id=treex[p];
				cntx[p]=0;
				Upx(p);
				updatex(m+i,id);
				printf("%d\n",id);
			}
			else {
				int p=Findx(1,y[i]);
				int id=treex[p];
				cntx[p]=0;
				Upx(p);
				p=Findy(1,2);
				updatex(m+i,treey[p]);
				cnty[p]=0;
				Upy(p);
				updatey(n+i,id);
				printf("%d\n",id);
			}
		}
	}
}p60;
/*
2 10 10
1 4
1 6
1 5
1 8
1 3
1 7
1 10
1 2
1 1
1 9
*/
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d %d %d",&n,&m,&q);
	if(n<=1000&&m<=1000)p30.solve();
	else {
		bool f=1;
		for(int i=1;i<=q;i++)Rd(x[i]),Rd(y[i]),f&=x[i]==1;
		if(f)p60.solve();
	}
	return 0;
}

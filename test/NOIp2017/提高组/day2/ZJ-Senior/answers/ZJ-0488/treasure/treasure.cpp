#include<bits/stdc++.h>
using namespace std;
#define M 12
#define ll long long
#define inf 1061109567
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),!isdigit(c));
	do res=(res<<3)+(res<<1)+(c^48);
	while(c=getchar(),isdigit(c));
}
int n,m,dis[M][M];
template<class T>
void Min(T &a,T b){
	if(a>b)a=b;
}
struct P40{
	ll res;
	int mark[M],Q[M],l,r;
	void bfs(int x){
		l=r=0;
		Q[r++]=x;
		mark[x]=1;
		while(l<r){
			int tmp=Q[l++];
			for(int i=0;i<n;i++)
				if(!mark[i]&&dis[tmp][i]!=inf){
					Q[r++]=i;
					mark[i]=mark[tmp]+1;
					res+=(ll)(mark[i]-1)*dis[tmp][i];
				}
		}
	}
	void solve(){
		ll ans=1e18;
		for(int i=0;i<n;i++){
			memset(mark,0,sizeof(mark));
			res=0;bfs(i);
			Min(ans,res);
		}
		printf("%lld\n",ans);
	}
}p40;
/*
4 5
1 2 2
1 3 2
1 4 2
2 3 2
3 4 2
*/
ll dp[M+1][M][1<<M];
void solve(){
	memset(dp,63,sizeof(dp));
	for(int d=1;d<=n;d++)
		for(int i=0;i<n;i++)dp[d][i][1<<i]=0;
	int All=(1<<n)-1;
	for(int d=n-1;d>=1;d--){
		for(int i=1;i<(1<<n);i++)
			for(int j=0;j<n;j++)if(!(i&(1<<j)))
				for(int k=0;k<n;k++)if(i&(1<<k)){
					if(dis[j][k]!=inf)Min(dp[d][j][i|(1<<j)],dp[d+1][k][i]+(ll)d*dis[j][k]);
				}
		for(int i=1;i<(1<<n);i++){
			int mx=All^i;
			for(int j=mx;j;j=(j-1)&mx){
				for(int k=0;k<n;k++)if(i&(1<<k))
					for(int t=0;t<n;t++)if(j&(1<<t)){
						if(dis[k][t]!=inf)Min(dp[d][k][i|j],dp[d][k][i]+dp[d+1][t][j]+(ll)d*dis[k][t]);
					}
			}
		}
	}
	ll ans=dp[1][0][(1<<n)-1];
	for(int i=1;i<n;i++)Min(ans,dp[1][i][(1<<n)-1]);
	printf("%lld\n",ans);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int f=0;
	scanf("%d %d",&n,&m);
	memset(dis,63,sizeof(dis));
	for(int i=1;i<=m;i++){
		int u,v,w;
		scanf("%d %d %d",&u,&v,&w);
		Min(dis[--u][--v],w);
		Min(dis[v][u],w);
		if(!f)f=w;
		else if(f!=w)f=-1;
	}
	if(~f)p40.solve();
	else solve();
	return 0;
}

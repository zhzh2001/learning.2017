var
  n,m,q,i,j,k,x,y,c:longint;
  r:array[0..5000,0..5000]of longint;
begin
  assign(input,'phalanx.in');reset(input);
  assign(output,'phalanx.out');rewrite(output);
  readln(n,m,q);
  for i:=1 to n do
    for j:=1 to m do r[i,j]:=(i-1)*m+j;
  for k:=1 to q do
  begin
    readln(x,y);
    writeln(r[x,y]);
    c:=r[x,y];
    for i:=x to n-1 do r[y,i]:=r[y,i+1];
    for j:=y to m-1 do r[j,n]:=r[j+1,n];
    r[n,m]:=c;
  end;
  close(input);close(output);
end.

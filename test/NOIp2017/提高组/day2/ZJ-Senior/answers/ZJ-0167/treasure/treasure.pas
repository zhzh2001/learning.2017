program project1;
var
  i,j,k,l:longint;
  ans,n,m,a,b,c,fun:int64;
  map:array[1..12,1..12]of int64;
  f:array[1..12]of boolean;
  dis:array[1..12]of int64;
  tmp:array[1..3]of int64;
function min(x,y:int64):int64;
begin
  if x<y then
    exit(x)
  else exit(y);
end;
procedure main(x:int64);
var
  i,j,k,l:longint;
begin
  fun:=0;
  for i:=1 to n do
    dis[i]:=maxlongint;
  fillchar(f,sizeof(f),false);
  f[x]:=true;
  dis[x]:=1;
  for i:=1 to n-1 do
  begin
    for j:=1 to 3 do
      tmp[j]:=maxlongint;
    for j:=1 to n do
      if f[j] then
        for k:=1 to n do
          if (not(f[k]))and(dis[j]*map[j,k]<tmp[3]) then
          begin
            tmp[1]:=j;
            tmp[2]:=k;
            tmp[3]:=dis[j]*map[j,k];
          end;
    inc(fun,tmp[3]);
    f[tmp[2]]:=true;
    dis[tmp[2]]:=dis[tmp[1]]+1;
  end;
end;
begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      map[i,j]:=maxlongint;
  for i:=1 to n do
    map[i,i]:=0;
  for i:=1 to m do
  begin
    readln(a,b,c);
    if map[a,b]>c then
      map[a,b]:=c;
    if map[b,a]>c then
      map[b,a]:=c;
  end;
  ans:=maxlongint;
  for i:=1 to n do
  begin
    main(i);
    if fun<ans then
      ans:=fun;
  end;
  writeln(ans);
  close(input);
  close(output);
end.
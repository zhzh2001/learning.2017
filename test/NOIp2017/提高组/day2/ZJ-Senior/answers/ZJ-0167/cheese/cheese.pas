program project1;
var
  i,j,k,l,t,n,h,r,head,tail,total,now:longint;
  a:array[1..1000,1..3]of longint;
  map:array[1..2000000,1..2]of longint;
  prev:array[1..2000000]of longint;
  last:array[1..1000]of longint;
  f,e:array[1..1000]of boolean;
  dl:array[1..100000]of longint;
function fun(x,y:longint):extended;
begin
  fun:=sqrt(sqr(a[x,1]-a[y,1])+sqr(a[x,2]-a[y,2])+sqr(a[x,3]-a[y,3]));
end;
procedure main;
var
  i,j,k,l:longint;
begin
  fillchar(last,sizeof(last),0);
  fillchar(e,sizeof(e),false);
  fillchar(f,sizeof(f),false);
  head:=1;
  tail:=0;
  readln(n,h,r);
  for i:=1 to n do
  begin
    for j:=1 to 3 do
      read(a[i,j]);
    for j:=1 to i-1 do
      if fun(i,j)<=2*r then
      begin
        inc(total);
        map[total,1]:=i;
        map[total,2]:=j;
        prev[total]:=last[i];
        last[i]:=total;
        inc(total);
        map[total,1]:=j;
        map[total,2]:=i;
        prev[total]:=last[j];
        last[j]:=total;
      end;
    if a[i,3]<=r then
    begin
      inc(tail);
      dl[tail]:=i;
      f[i]:=true;
    end;
    if a[i,3]>=(h-r) then
      e[i]:=true;
  end;
  while head<=tail do
  begin
    now:=last[dl[head]];
    while now<>0 do
    begin
      if not(f[map[now,2]]) then
      begin
        inc(tail);
        dl[tail]:=map[now,2];
        f[map[now,2]]:=true;
      end;
      now:=prev[now];
    end;
    inc(head);
  end;
  for i:=1 to n do
    if (f[i])and(e[i]) then
    begin
      writeln('Yes');
      exit;
    end;
  writeln('No');
end;
begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i:=1 to t do
    main;
  close(input);
  close(output);
end.
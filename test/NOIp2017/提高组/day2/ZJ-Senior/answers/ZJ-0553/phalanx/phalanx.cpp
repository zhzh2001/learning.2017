#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 1000
using namespace std;
int a[MAXN+5][MAXN+5];
int n,m,q;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a[i][j]=(i-1)*m+j;
	while (q--){
		int x,y;
		scanf("%d%d",&x,&y);
		int node=a[x][y];
		for (int i=y;i<m;i++)
			a[x][i]=a[x][i+1];
		for (int i=x;i<n;i++)
			a[i][m]=a[i+1][m];
		a[n][m]=node;
		printf("%d\n",node);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

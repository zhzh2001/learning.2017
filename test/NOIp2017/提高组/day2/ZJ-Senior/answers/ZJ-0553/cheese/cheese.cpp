#include<cctype>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 1000
#define sqr(x) ((x)*(x))
using namespace std;
typedef long long LL;
struct edge{
	int next,to;
}ed[MAXN*MAXN*6+5];
struct node{
	LL x,y,z;
}a[MAXN*2+5];
int t,n,k;
LL m,r;
int h[MAXN*2+5],que[MAXN*2+5];
bool f[MAXN*2+5];
void addedge(int x,int y){
	ed[++k].next=h[x]; ed[k].to=y; h[x]=k;
}
LL calc(node a,node b){
	return sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z);
}
bool bfs(int s,int t){
	memset(f,false,sizeof(f));
	int r=0,w=1; que[1]=s; f[s]=true;
	while (r<w){
		int x=que[++r];
		for (int i=h[x];i;i=ed[i].next)
			if (!f[ed[i].to]){
				que[++w]=ed[i].to;
				f[ed[i].to]=true;
			}
	}
	return f[t];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		memset(h,0,sizeof(h)); k=0;
		scanf("%d%lld%lld",&n,&m,&r);
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
			if (a[i].z<=r) addedge(MAXN+2,i);
			if (a[i].z+r>=m) addedge(i,MAXN+1); 
		}
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (calc(a[i],a[j])<=sqr(r*2))
					addedge(i,j),addedge(j,i);
		if (bfs(MAXN+2,MAXN+1)) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define MAXN 12 
#define MAXM 1000
using namespace std;
typedef long long LL;
struct edge{
	int now,to,next;
	LL dis;
}ed[MAXM*2+5];
int n,m,k;
int h[MAXN+5],dis[MAXN+5];
bool f[MAXN+5];
void addedge (int x,int y,LL z){
	ed[++k].next=h[x]; ed[k].now=x; ed[k].to=y; ed[k].dis=z; h[x]=k;
}
LL prim(int now){
	memset(dis,0x7f,sizeof(dis));
	memset(f,false,sizeof(f));
	LL ret=0; f[now]=true;
	bool flag=true; dis[now]=1;
	while (flag){
		flag=false;
		int node=0,fr=0; LL p=0x7fffffff;
		for (int i=1;i<=n;i++)
			if (f[i]){
				for (int j=h[i];j;j=ed[j].next)
					if (!f[ed[j].to]){
						flag=true;
						if (p>dis[i]*ed[j].dis){
							p=dis[i]*ed[j].dis;
							node=ed[j].to; fr=i;
						}
					}
			}
		if (!flag) break;
		f[node]=true,ret+=p,dis[node]=dis[fr]+1;
	}
	return ret;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int u,v; LL d;
		scanf("%d%d%lld",&u,&v,&d);
		addedge(u,v,d),addedge(v,u,d);
	}
	LL ans=0x7fffffff;
	for (int i=1;i<=n;i++)
		ans=min(ans,prim(i));
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <cstdio>
#include <cstring>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

int T;
int edgenum;
int vet[3000000];
int nextx[3000000];
int head[2000];
bool vis[2000];
int n;
long long h, r;
long long x[2000], y[2000], z[2000];

void add(int u, int v)
{
	edgenum++;
	vet[edgenum] = v;
	nextx[edgenum] = head[u];
	head[u] = edgenum;
}

long long c(long long x)
{
	return x * x;
}

bool dfs(int u)
{
	if (u == n + 2)
	{
		return true;
	}
	vis[u] = 1;
	for (int i = head[u]; i; i = nextx[i])
	{
		int v = vet[i];
		if (! vis[v])
		{
			if (dfs(v))
			{
				return true;
			}
		}
	}
	return false;
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	T = read();
	while (T--)
	{
		edgenum = 0;
		memset(head, 0, sizeof(head));
		memset(nextx, 0, sizeof(nextx));
		n = read();
		h = read();
		r = read();
		for (int i = 1; i <= n; i++)
		{
			x[i] = read();
			y[i] = read();
			z[i] = read();
		}
		for (int i = 1; i <= n; i++)
		{
			if (z[i] <= r)
			{
				add(i, n + 1);
				add(n + 1, i);
			}
		}
		for (int i = 1; i <= n; i++)
		{
			if (h - z[i] <= r)
			{
				add(i, n + 2);
				add(n + 2, i);
			}
		}
		for (int i = 1; i < n; i++)
		{
			for (int j = i + 1; j <= n; j++)
			{
				if (c(x[i] - x[j]) + c(y[i] - y[j]) + c(z[i] - z[j]) <= c(2 * r))
				{
					add(i, j);
					add(j, i);
				}
			}
		}
		for (int i = 1; i <= n; i++)
		{
			vis[i] = 0;
		}
		if (dfs(n + 1))
		{
			printf("Yes\n");
		}
		else
		{
			printf("No\n");
		}
	}
	return 0;
}

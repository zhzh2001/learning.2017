#include <cstdio>
#include <algorithm>

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

const int INF = 1000000000;
int n, m;
int dis[20][20];
int f[20][6000];
int last1, last2;
int v1[20], v2[20];
int minx[20];
int ans;

void dfs(int k, int deep, int cas, int sum)
{
	if (k > last2)
	{
		f[deep][cas] = std::min(f[deep][cas], sum);
		return;
	}
	dfs(k + 1, deep, cas, sum);
	dfs(k + 1, deep, cas | (1 << (v2[k] - 1)), (int)std::min((long long)INF, (long long)sum + (long long)minx[k] * deep));
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	n = read();
	m = read();
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			dis[i][j] = INF;
		}
	}
	for (int i = 1; i <= m; i++)
	{
		int u, v, cost;
		u = read();
		v = read();
		cost = read();
		dis[u][v] = std::min(dis[u][v], cost);
		dis[v][u] = std::min(dis[v][u], cost);
	}
	for (int cas = 0; cas < (1 << n); cas++)
	{
		f[0][cas] = INF;
	}
	for (int i = 1; i <= n; i++)
	{
		f[0][1 << (i - 1)] = 0;
	}
	ans = INF;
	ans = std::min(ans, f[0][(1 << n) - 1]);
	for (int i = 1; i < n; i++)
	{
		for (int cas = 0; cas < (1 << n); cas++)
		{
			f[i][cas] = INF;
		}
		for (int cas = 0; cas < (1 << n); cas++)
		{
			last1 = 0;
			last2 = 0;
			for (int j = 1; j <= n; j++)
			{
				if (cas & (1 << (j - 1)))
				{
					last1++;
					v1[last1] = j;
				}
				else
				{
					last2++;
					v2[last2] = j;
				}
			}
			for (int j = 1; j <= last2; j++)
			{
				minx[j] = INF;
			}
			for (int j = 1; j <= last2; j++)
			{
				for (int k = 1; k <= last1; k++)
				{
					minx[j] = std::min(minx[j], dis[v1[k]][v2[j]]);
				}
			}
			dfs(1, i, cas, f[i - 1][cas]);
		}
		ans = std::min(ans, f[i][(1 << n) - 1]);
	}
	printf("%d\n", ans);
	return 0;
}



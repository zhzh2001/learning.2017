#include <cstdio>
#include <vector>

#define root ch[0][1]

long long read()
{
	char last = '+', ch = getchar();
	while (ch < '0' || ch > '9') last = ch, ch = getchar();
	long long tmp = 0;
	while (ch >= '0' && ch <= '9') tmp = tmp * 10 + ch - 48, ch = getchar();
	if (last == '-') tmp = -tmp;
	return tmp;
}

struct Splay
{
	int knum;
	int tree[1000000];
	int size[1000000];
	int ch[1000000][2];
	int fa[1000000];
	int sz;
	int pre;

	void init()
	{
		knum = 0;
		sz = 0;
		pre = -1;
	}

	void update(int k)
	{
		size[k] = size[ch[k][0]] + size[ch[k][1]] + 1;
	}

	void build(int l, int r, int *x, int &k, int father)
	{
		if (l > r)
		{
			return;
		}
		if (l == r)
		{
			knum++;
			sz++;
			k = knum;
			tree[k] = x[l];
			size[k] = 1;
			fa[k] = father;
			//printf("%d %d\n", k, size[k]);
			return;
		}
		int mid = (l + r) >> 1;
		knum++;
		sz++;
		k = knum;
		tree[k] = x[mid];
		build(l, mid - 1, x, ch[k][0], k);
		build(mid + 1, r, x, ch[k][1], k);
		update(k);
		fa[k] = father;
		//printf("%d %d\n", k, size[k]);
	}
	
	void rotate(int k)
	{
		int f = fa[k];
		int ff = fa[f];
		int w = k == ch[f][1];
		ch[ff][f == ch[ff][1]] = k;
		fa[k] = ff;
		ch[f][w] = ch[k][w ^ 1];
		fa[ch[f][w]] = f;
		ch[k][w ^ 1] = f;
		fa[f] = k;
		update(f);
		update(k);
	}

	void splay(int x, int &k)
	{
	//	printf("VVV\n");
		while (x != k)
		{
			int f = fa[x];
			if (f != k)
			{
				int ff = fa[f];
				if ((ch[ff][1] == f) ^ (ch[f][1] == x))
				{
					rotate(x);
				}
				else
				{
					rotate(f);
				}
			}
			rotate(x);
		}
	}

	int find(int k, int pos)
	{
		//printf("X%d %d %d %d %d\n", root, k, pos, ch[k][0], ch[k][1]);
		if (pos == size[ch[k][0]] + 1)
		{
			return k;
		}
		if (pos <= size[ch[k][0]])
		{
			return find(ch[k][0], pos);
		}
		else
		{
			return find(ch[k][1], pos - size[ch[k][0]] - 1);
		}
	}

	void deletex(int x)
	{
	//	puts("V");
		sz--;
		if (x == 1)
		{
			int xx = find(root, x + 1);
			splay(xx, root);
			pre = ch[xx][0];
			ch[xx][0] = 0;
			update(xx);
			return;
		}
		if (x == sz + 1)
		{
			int xx = find(root, x - 1);
			splay(xx, root);
			pre = ch[xx][1];
			ch[xx][1] = 0;
			update(xx);
			return;
		}
	//	puts("T");
		int yy = find(root, x - 1);
	//	puts("A");
		splay(yy, root);
		int xx = find(root, x + 1);
	//	puts("B");
		splay(xx, ch[root][1]);
	//	puts("C");
		//printf("V%d\n", size[xx]);
		//printf("GG%d\n", ch[xx][0]);
		pre = ch[xx][0];
		ch[xx][0] = 0;
		update(xx);
		update(yy);
		//printf("K%d\n", size[xx]);
	}

	void insert(int x)
	{
		//printf("K%d\n", pre);
		int xx = find(root, sz);
		splay(xx, root);
		//printf("KKK%d\n", root);
		ch[xx][1] = pre;
		ch[pre][0] = ch[pre][1] = 0;
		size[pre] = 1;
		fa[pre] = xx;
		tree[pre] = x;
		update(xx);
		sz++;
	}
};

Splay r;
int n, m, q;
int tmp[1000000];
int top[100000];
int cnt[100000];
std::vector<int> kk[100000];
std::vector<int> cc[100000];

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if (q > 500)
	{
		for (int i = 1; i <= m; i++)
		{
			tmp[i] = i;
		}
		for (int i = 2; i <= n; i++)
		{
			tmp[i + m - 1] = i * m;
		}
		r.init();
		r.build(1, n + m - 1, tmp, r.root, 0);
		//printf("%d\n", r.sz);
		//for (int i = 1; i <= n + m - 1; i++)
		//{
		//	printf("%d ", r.find(r.root, i));
		//}
		while (q--)
		{
			int x, y;
			x = read();
			y = read();
			int kk = r.find(r.root, y);
		//	printf("G%d\n", kk);
			r.splay(kk, r.root);
			int key = r.tree[kk];
			printf("%d\n", key);
			r.deletex(y);
			r.insert(key);
		//	int tmp = r.find(r.root, r.sz);
		//	printf("%d %d %d %d %d\n", r.root, r.ch[r.root][1], tmp, r.ch[tmp][0], r.ch[tmp][1]);
		}
	}
	else
	{
		for (int i = 1; i <= n; i++)
		{
			tmp[i] = i * m;
		}
		for (int i = 1; i <= n; i++)
		{
			kk[i].push_back(0);
			cc[i].push_back(0);
		}
		while (q--)
		{
			int x, y;
			x = read();
			y = read();
			if (y != m)
			{
				if (m - 1 - cnt[x] >= y)
				{
					int last;
					for (last = cnt[x]; last >= 0; last--)
					{
						if (! (kk[x][last] - last >= y))
						{
							break;
						}
					}
					int key = kk[x][last] + (y - (kk[x][last] - last));
					printf("%d\n", (x - 1) * m + key);
					kk[x].push_back(0);
					cnt[x]++;
					for (int i = last + 1; i < cnt[x]; i++)
					{
						kk[x][i + 1] = kk[x][i];
					}
					kk[x][last + 1] = key;
					top[x]++;
					cc[x].push_back(0);
					cc[x][top[x]] = tmp[x];
					for (int i = x; i < n; i++)
					{
						tmp[i] = tmp[i + 1];
					}
					tmp[n] = (x - 1) * m + key;
				}
				else
				{
					int kk = y - (m - 1 - cnt[x]);
					int key = cc[x][kk];
					printf("%d\n", key);
					for (int i = kk; i < top[x]; i++)
					{
						cc[x][i] = cc[x][i + 1];
					}
					top[x]--;
					top[x]++;
					cc[x].push_back(0);
					cc[x][top[x]] = tmp[x];
					for (int i = x; i < n; i++)
					{
						tmp[i] = tmp[i + 1];
					}
					tmp[n] = key;
				}
			}
			else
			{
				int key = tmp[x];
				printf("%d\n", key);
				for (int i = x; i < n; i++)
				{
					tmp[i] = tmp[i + 1];
				}
				tmp[n] = key;
			}
		}
	}
	return 0;
}


var z,x,i,j,n,m,w,h,min:longint;
  dis:array[0..15,0..15]of longint;
  p,f:array[0..100]of longint;

procedure dfs(u,t:longint);
var i,j:longint;
begin
  p[u]:=1;
  for i:=1 to z do
    if (p[i]=0)and(dis[u,i]<>-1) then
    begin
      if dis[u,i]*t<f[i] then f[i]:=dis[u,i]*t;
      dfs(i,t+1);
    end;

  p[u]:=0;

end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);

  min:=100000000;

  for i:=0 to 15 do
  for j:=0 to 15 do
    dis[i,j]:=-1;

  readln(z,x);
  for i:=1 to x do
  begin
    readln(w,h,n);
    if (dis[w,h]=-1)or(dis[w,h]>n) then
    begin
      dis[w,h]:=n;dis[h,w]:=n;
    end;
  end;

  for i:=1 to z do
  begin
    for j:=1 to z do
      f[j]:=100000000;
    f[i]:=0;
    dfs(i,1);

    for j:=1 to z-1 do
      f[j+1]:=f[j+1]+f[j];
    if f[z]<min then min:=f[z];
  end;

  write(min);

  close(input);close(output);
end.

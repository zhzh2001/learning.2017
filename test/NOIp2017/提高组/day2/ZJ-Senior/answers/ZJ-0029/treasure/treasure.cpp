#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=13,M=5010,oo=1e9;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int f[N][M],g[M][M],ans=oo;
int n,m,d[N][N],D[M][N],b[N];
inline void Mn(int&x,int y){if(y<x)x=y;}
void work(int rt){
	register int i,j,k;
	memset(f,60,sizeof(f));
	f[0][b[rt]]=0;
	for(i=1;i<n;i++){
		for(j=0;j<m;j++){
			for(k=j;k;k=(k-1)&j)
			if(f[i-1][k]<oo&&g[k][j^k]<oo){
				Mn(f[i][j],f[i-1][k]+g[k][j^k]*i);
			}
		}
	}
	for(i=0;i<n;i++)
	Mn(ans,f[i][m-1]);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j,k,x,y;
	n=read();m=read();
	for(i=1;i<=n;i++)b[i]=1<<(i-1);
	for(i=1;i<=n;i++)
	for(j=1;j<=n;j++)d[i][j]=oo;
	for(;m--;){
		x=read();y=read();k=read();
		d[y][x]=d[x][y]=min(k,d[x][y]);
	}m=1<<n;
	for(i=0;i<m;i++){
		for(j=1;j<=n;j++){
			D[i][j]=oo;
			if((i&b[j])==0){
				for(k=1;k<=n;k++)
				if(i&b[k])
				Mn(D[i][j],d[j][k]);
			}
		}
	}
	memset(g,60,sizeof(g));
	for(i=0;i<m;i++){
		for(j=i;j;j=(j-1)&i){
			k=i^j;
			g[j][k]=0;
			for(x=1;x<=n;x++)
			if(k&b[x]){
				g[j][k]+=D[j][x];
				if(g[j][k]>oo)g[j][k]=oo;
			}
		}
	}
	for(i=1;i<=n;i++)
	work(i);
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=300100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
ll n,m,tot,Q,mx;
struct Lf{int sm,ls,rs;ll id;}tr[N*30],emp;
Lf bld(int k,int l,int r){
	Lf&re=tr[++tot];
	re.sm=r-l+1;
	if(l==r){
		if(k<=n){
			re.id=1ll*(k-1)*m+l;
		}else{
			re.id=1ll*l*m;
		}
	}return re;
}
int rt[N];
int get(int k,int&i,int l,int r,int x){
	if(!i){bld(k,l,r);i=tot;}
	if(tr[i].sm<x)return -1;
	if(l==r)return tr[i].id;
	int md=l+r>>1;
	int res=get(k,tr[i].ls,l,md,x);
	if(res>0)return res;
	return get(k,tr[i].rs,md+1,r,x-tr[tr[i].ls].sm);
}
int add(int k,int&i,int l,int r,int x,ll y){
	if(!i){bld(k,l,r);i=tot;}
	if(tr[i].sm<x)return -1;
	if(l==r){tr[i].id=y;return 1;}
	int md=l+r>>1;
	int res=add(k,tr[i].ls,l,md,x,y);
	if(res>0)return res;
	return add(k,tr[i].rs,md+1,r,x-tr[tr[i].ls].sm,y);
}
int del(int k,int&i,int l,int r,int x){
	if(!i){bld(k,l,r);i=tot;}
	if(tr[i].sm<x)return -1;
	tr[i].sm--;
	if(l==r)return 1;
	int md=l+r>>1;
	int res=del(k,tr[i].ls,l,md,x);
	if(res>0)return res;
	return del(k,tr[i].rs,md+1,r,x-tr[tr[i].ls].sm);
	
}
ll f[3010][3010];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx1.out","w",stdout);
	n=read();m=read();Q=read();mx=max(n,m)+Q;
	for(int i=1;i<=n;i++)
	for(int j=1;j<=m;j++)f[i][j]=++tot;
	for(int i,x,y;Q--;){
		ll ans;x=read();y=read();
		printf("%lld\n",ans=f[x][y]);
		for(i=y;i<m;i++)f[x][i]=f[x][i+1];
		for(i=x;i<n;i++)f[i][m]=f[i+1][m];
		f[n][m]=ans;
	}return 0;
}

#include<ctime>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=100100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,q,x,y,i,j,k;
int main(){
	freopen("phalanx.in","w",stdout);
	srand(time(0));
	n=300000;m=300000;q=300000;
	printf("%d %d %d\n",n,m,q);
	for(i=1;i<=q;i++){
		x=rand()%n+1;
		y=rand()%m+1;
		printf("%d %d\n",x,y);
	}
	return 0;
}

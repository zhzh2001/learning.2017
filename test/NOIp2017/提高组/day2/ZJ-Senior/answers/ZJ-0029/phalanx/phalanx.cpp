#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=300100,P=1e9+7;
inline int read(){
	int x=0,f=0,c=getchar();
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int n,m,tot,Q,mx;
struct Lf{int sm,ls,rs;ll id;}tr[N*31];
Lf bld(int k,int l,int r){
	Lf&re=tr[++tot];
	re.sm=r-l+1;
	if(l==r){
		if(k<=n){
			re.id=1ll*(k-1)*m+l;
		}else{
			re.id=1ll*l*m;
		}
	}return re;
}
int rt[N];
ll get(int k,int&i,int l,int r,int x){
	if(!i){bld(k,l,r);i=tot;}
	if(tr[i].sm<x)return -1;
	tr[i].sm--;
	if(l==r)return tr[i].id;
	int md=l+r>>1;
	ll res=get(k,tr[i].ls,l,md,x);
	if(res>0)return res;
	return get(k,tr[i].rs,md+1,r,x-tr[tr[i].ls].sm);
}
int add(int k,int&i,int l,int r,int x,ll y){
	if(!i){bld(k,l,r);i=tot;}
	if(tr[i].sm<x)return -1;
	if(l==r){tr[i].id=y;return 1;}
	int md=l+r>>1;
	int res=add(k,tr[i].ls,l,md,x,y);
	if(res>0)return res;
	return add(k,tr[i].rs,md+1,r,x-tr[tr[i].ls].sm,y);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();Q=read();mx=max(n,m)+Q;
	for(int x,y;Q--;){
		ll ans;x=read();y=read();
		if(y==m){
			ans=get(n+1,rt[n+1],1,mx,x);
			write(ans);putchar('\n');
			add(n+1,rt[n+1],1,mx,n,ans);
		}else{
			ans=get(x,rt[x],1,mx,y);
			write(ans);putchar('\n');
			add(n+1,rt[n+1],1,mx,n+1,ans);
			
			ans=get(n+1,rt[n+1],1,mx,x);
			add(x,rt[x],1,mx,m-1,ans);
		}
	}
	return 0;
}

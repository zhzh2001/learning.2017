#include<iostream>
#include<cstdio>
#include<cstring>
#define cls(a) memset(a,0,sizeof(a))
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
#define per(i,x,y) for(int i=(x);i>=(y);--i)

typedef long long LL;
const int N=1000+10;
inline LL sqr(LL x){return x*x;}
int n;
LL h,r;
struct Circle{
	LL x,y,z;
}c[N];
bool hasIntersection(const Circle&a,const Circle&b){
	return sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z)<=sqr(2*r);
}
int w[N][N];
bool vis[N];
void dfs(int k){
	vis[k]=1;
	rep(i,1,n+2)if(w[k][i]&&!vis[i])dfs(i);
}
bool work(){
	cls(w);
	scanf("%d%lld%lld",&n,&h,&r);
	rep(i,1,n)scanf("%lld%lld%lld",&c[i].x,&c[i].y,&c[i].z);
	rep(i,1,n){
		rep(j,1,n)if(hasIntersection(c[i],c[j]))w[i][j]=1;
	}
	rep(i,1,n)if(c[i].z<=r)w[n+1][i]=w[i][n+1]=1;
	rep(i,1,n)if(c[i].z+r>=h)w[n+2][i]=w[i][n+2]=1;
	cls(vis);
	dfs(n+1);
	return vis[n+2];
}
int main(){
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		if(work())puts("Yes");
		else puts("No");
	}
	return 0;
}

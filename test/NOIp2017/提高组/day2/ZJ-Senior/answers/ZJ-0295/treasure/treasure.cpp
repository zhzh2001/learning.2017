#include<iostream>
#include<cstdio>
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
#define per(i,x,y) for(int i=(x);i>=(y);--i)

const int N=12+8,inf=1e9,V=(1<<12)+10;
inline void dn(int&x,int y){if(y<x)x=y;}
int n,m;
int w[N][N];
int dp[N][N][V],count[V],lg[V];
inline bool in(int k,int s){return s&(1<<(k-1));}
int calc(int k,int d,int s){
	if(dp[k][d][s]<inf)return dp[k][d][s];
	if(count[s]==1)dp[k][d][s]=0;
	for(int i=(s-1)&s;i;i=(i-1)&s)if(in(k,i)){
		for(int tmp=s^i,j=lg[tmp];tmp;tmp-=1<<(j-1),j=lg[tmp]){
			if(w[k][j]<inf){
				dn(dp[k][d][s],calc(k,d,i)+calc(j,d+1,s^i)+w[k][j]*(d+1));
			}
		}
	}
	return dp[k][d][s];
}
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,n)rep(j,1,n)if(i!=j)w[i][j]=inf;
	rep(i,1,m){
		int a,b,v;
		scanf("%d%d%d",&a,&b,&v);
		dn(w[a][b],v);
		dn(w[b][a],v);
	}
	int ans=inf;
	rep(i,1,(1<<n)-1)count[i]=count[i>>1]+(i&1);
	rep(i,1,(1<<n)-1)lg[i]=lg[i>>1]+1;
	rep(i,1,n)rep(j,0,n)rep(k,0,(1<<n)-1)dp[i][j][k]=inf;
	rep(i,1,n)dn(ans,calc(i,0,(1<<n)-1));
	printf("%d",ans);
	return 0;
}

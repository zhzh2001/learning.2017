#include<iostream>
#include<cstdio>
#include<cstdlib>
#define rep(i,x,y) for(int i=(x);i<=(y);++i)
#define per(i,x,y) for(int i=(x);i>=(y);--i)

typedef long long LL;
int n,m,q;
namespace brute{
	const int N=1000+10;
	int id[N][N];
	void leave(int x,int y){
		int t=id[x][y];
		rep(i,y,m-1)id[x][i]=id[x][i+1];
		rep(i,x,n-1)id[i][m]=id[i+1][m];
		id[n][m]=t;
	}
	void main(){
		rep(i,1,n)rep(j,1,m)id[i][j]=(i-1)*m+j;
		rep(i,1,q){
			int x,y;scanf("%d%d",&x,&y);
			printf("%d\n",id[x][y]);
			leave(x,y);
		}
	}
}
namespace xis1{
	const int N=3e5+10,P=N<<1;
	struct Treap{
#define ls son[k][0]
#define rs son[k][1]
		int tot,son[P][2],rnd[P],size[P];
		int root;
		int newNode(){
			int k=++tot;
			rnd[k]=rand();
			size[k]=1;
			return k;
		}
		void update(int k){
			size[k]=size[ls]+size[rs]+1;
		}
		void maintain(int k){
		}
		int merge(int a,int b){
			if(a==0||b==0)return a+b;
			if(rnd[a]>rnd[b]){
				maintain(a);
				son[a][1]=merge(son[a][1],b);
				update(a);
				return a;
			}else{
				maintain(b);
				son[b][0]=merge(a,son[b][0]);
				update(b);
				return b;
			}
		}
		void split(int k,int n,int&l,int&r){
			if(n==0){l=0;r=k;return;}
			if(n==size[k]){l=k;r=0;return;}
			if(n<=size[ls])return split(ls,n,l,ls),r=k,update(k);
			if(n>size[ls])return split(rs,n-size[ls]-1,rs,r),l=k,update(k);
		}
		void insert(int p,int k){
			if(!root){root=k;return;}
			int l,r;
			split(root,p,l,r);
			root=merge(merge(l,k),r);
		}
		int get(int p){
			int l,k,r;
			split(root,p-1,l,k);
			split(k,1,k,r);
			root=merge(merge(l,k),r);
			return k;
		}
		void del(int p){
			int l,k,r;
			split(root,p-1,l,k);
			split(k,1,k,r);
			root=merge(l,r);
		}
	}T;
	void checkTreap(){
		T.insert(0,T.newNode());
		printf("%d",T.get(1));
		T.insert(0,T.newNode());
		printf("%d",T.get(1));
		T.del(2);
		printf("%d",T.get(1));
	}
	LL id[P];
	void main(){
		rep(i,1,m)id[i]=i;
		rep(i,2,n)id[m+i-1]=(LL)i*m;
		rep(i,1,n+m-1)T.insert(i-1,T.newNode());
		rep(i,1,q){
			int x,y;scanf("%d%d",&x,&y);
			int t=T.get(y);
			printf("%lld\n",id[t]);
			T.del(y);
			T.insert(n+m-2,t);
		}
	}
}

int main(){
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	srand(20000107);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=1000&&m<=1000)brute::main();
	else xis1::main();
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define Komachi is retarded
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;i++)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;i--)
#define LL long long
#define DB double
#define EPS 1e-6

#define M 1004
int T,n,h,r,d;
int X[M],Y[M],Z[M];
inline DB Dist(const int &i,const int &j){
	return sqrt(1.0*(X[i]-X[j])*(X[i]-X[j])+1.0*(Y[i]-Y[j])*(Y[i]-Y[j])+1.0*(Z[i]-Z[j])*(Z[i]-Z[j]));
}
int Q[M];
bool Mark[M];
bool Solve(){
	int L,R;L=R=0;
	memset(Mark,0,sizeof(Mark));
	REP(i,0,n)if(Z[i]<=r)Q[R++]=i,Mark[i]=1;
	while(L<R){
		int A=Q[L++];
		if(Z[A]+r>=h)return 1;
		REP(B,0,n)if(!Mark[B] && Dist(A,B)-EPS<d)
			Mark[B]=1,Q[R++]=B;
	}
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r),d=2*r;
		REP(i,0,n)scanf("%d%d%d",&X[i],&Y[i],&Z[i]); 
		puts(Solve()?"Yes":"No");
	}
	return 0;
}

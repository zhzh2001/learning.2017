#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define Komachi is retarded
#define REP(i,a,b) for(int i=(a),i##_end_=(b);i<i##_end_;i++)
#define DREP(i,a,b) for(int i=(a),i##_end_=(b);i>i##_end_;i--)
#define LL long long
#define DB double

inline void chkmin(int &a,const int &b){if(a>b)a=b;}

#define M 14
#define INF 0x3f3f3f3f

int n,m,E[M][M];
int F[(1<<12)+4][M];
int Bin[M];
int main(){
//	printf("%.6lf",sizeof(F)/1024.0/1024);
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	REP(i,0,M)Bin[i]=1<<i;
	
	scanf("%d%d",&n,&m);
	memset(E,63,sizeof(E));
	memset(F,63,sizeof(F));
	
	while(m--){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		u--,v--;
		chkmin(E[u][v],w);
		chkmin(E[v][u],w);
	}
	
	int Sp=Bin[n]-1;
	REP(i,0,n)F[Bin[i]][0]=0;
	REP(t,0,n) REP(S,1,Bin[n]) if(F[S][t]!=INF){
		for(int i=S^Sp;i;i=((i-1)&(S^Sp))){
			int Res=0;
			REP(a,0,n)if(i&Bin[a]){
				int Mn=INF;
				REP(b,0,n)if(S&Bin[b])
					chkmin(Mn,E[a][b]);
				Res+=Mn;
				if(Res>=INF)break;
			}
			if(Res>=INF)continue;
			chkmin(F[S|i][t+1],F[S][t]+Res*(t+1));
		}
	}
	
	int Ans=INF;
	REP(i,0,n)chkmin(Ans,F[Sp][i]);
	printf("%d\n",Ans);
	
	return 0;
}

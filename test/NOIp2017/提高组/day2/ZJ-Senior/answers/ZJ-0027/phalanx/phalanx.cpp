#include<cstdio>
int A[1005][1005],n,m,q,x,y,a[600005],tr[6000005],tail;
int MAX=600000;
int read()
{
	int t=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') t=t*10-48+ch,ch=getchar();
	return t;
}
void write(int x)
{
	if (x<10) {putchar(x+48);return;};
	write(x/10);
	putchar(x%10+48);
}
void update(int x,int v)
{
	for (x; x<=MAX; x+=x&-x)
		tr[x]+=v;
}
int query(int x)
{
	int t=0;
	for (x; x; x-=x&-x)
		t+=tr[x];
	return t;
}
int Q(int y)
{
	int l=1,r=tail;
	while(l<r)
	{
		int mid=(l+r)>>1;
		int t=query(mid);
		if (t>=y) r=mid;
		else l=mid+1;
	}
}
void bf()
{
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++)
			A[i][j]=(i-1)*m+j;
	while(q--)
	{
		x=read(),y=read();
		int id=A[x][y];
		write(id),puts("");
		for (int j=y; j<m; j++)
			A[x][j]=A[x][j+1];
		for (int i=x; i<n; i++)
			A[i][m]=A[i+1][m];
		A[n][m]=id;
	}
}
void pf()
{
	tail=n+m-1;
	for (int i=1; i<=m; i++) a[i]=i;
	for (int i=2; i<=n; i++) a[i+m-1]=i*m;
	for (int i=1; i<=tail; i++) update(i,1);
	while(q--)
	{
		x=read(),y=read();
		int t=Q(y);
		write(a[t]),puts("");
		a[++tail]=a[t];
		a[t]=0;
		update(tail,1);
		update(t,-1);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n<=1000&&m<=1000) bf();
	else pf();
}

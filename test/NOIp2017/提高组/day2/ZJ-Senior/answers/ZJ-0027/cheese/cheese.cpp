#include<cstdio>
#include<cmath>
#include<cstring>
long long T,n,h,r;
struct node{int to,nxt;}e[200010];
long long z[10010],x[10010],y[10010],head[200010],cnt,t,q[200010],ok[20010],vis[20010];
void add(int x,int y)
{
	e[++cnt].to=y;
	e[cnt].nxt=head[x];
	head[x]=cnt;
}
void clear()
{
	memset(vis,0,sizeof(vis));
	memset(ok,0,sizeof(ok));
	memset(e,0,sizeof(e));
	memset(head,0,sizeof(head));
	cnt=0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	while(T--)
	{
		clear();
		scanf("%lld%lld%lld",&n,&h,&r);
		for (int i=1; i<=n; i++)
		{
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if (z[i]-r<=0) add(n+2,i);
			if (z[i]+r>=h) add(i,n+1);
		}
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++)
			{
				if (i==j) continue;
				double dis=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
				if (dis<=2*r) 
					add(i,j);
			}
		int h=0,t=1;
		q[1]=n+2,ok[n+2]=1;
		while(h<t)
		{
			int tmp=q[++h];
			for (int i=head[tmp]; i; i=e[i].nxt)
			{
				int to=e[i].to;
				if (!vis[to]) q[++t]=to,ok[to]=1,vis[to]=1;
			}
		}
		if (ok[n+1]) puts("Yes");
		else puts("No");
	}
}

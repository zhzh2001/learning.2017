#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int m[100][100],x,y,v,a[100],M,n,ans,dis[100],Ans=2147483647;
void clear()
{
	memset(dis,0,sizeof(dis));
	ans=0;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&M);
	for (int i=1; i<=n; i++)
		a[i]=i;
	for (int i=1; i<=M; i++)
		scanf("%d%d%d",&x,&y,&v),m[x][y]=1,m[y][x]=1;
	do
	{
		clear();
		int flag=1;
		for (int i=2; i<=n; i++)
		{
			int A=1000;
			for (int j=1; j<i; j++)
				if (m[a[j]][a[i]])
				{
					flag=0;
					A=min(A,dis[a[j]]);
				}
			if (flag) break;
			ans+=(A+1)*v;
			dis[a[i]]=A+1;
		}
		if (flag) continue;
		Ans=min(Ans,ans);
	}
	while (next_permutation(a+1,a+n+1));
	printf("%d",Ans);
}

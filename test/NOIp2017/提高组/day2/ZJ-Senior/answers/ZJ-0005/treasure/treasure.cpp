#include<bits/stdc++.h>
using namespace std;
int n,mp[20][20];int deep[20],use[20],ans=1e9+7,dp[20][20]={0},cnt[20]={0};
void dfs(int pos,int cost)
{   if(cost>=ans)return;
    if(pos==n+1){ans=min(ans,cost);return;}
    int i,j;
	if(pos==1)
	 {for(i=1;i<=n;i++)
	   deep[i]=1,use[1]=i,dp[1][++cnt[1]]=i,dfs(pos+1,0),deep[i]=0,cnt[1]--;}
	else 
	 {  for(i=1;i<=n;i++)
	 	   {if(deep[i])continue;
	 	   	 for(j=1;j<pos;j++)
	 	      {if(mp[use[j]][i]!=2139062143&&dp[deep[use[j]]+1][cnt[deep[use[j]]+1]]<i)
	 	     	{ use[pos]=i;deep[i]=deep[use[j]]+1;
	 	     	  dp[deep[i]][++cnt[deep[i]]]=i;
	 	     	  dfs(pos+1,cost+(deep[use[j]]*mp[use[j]][i]));
	 	     	  cnt[deep[i]]--;deep[i]=0;
				}
			  }
		   }
	 } 
}
int main()
{   
    freopen("treasure.in","r",stdin);
    freopen("treasure.out","w",stdout);
	int m,i,a,b,v;
	memset(mp,127,sizeof mp);
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++)
	   {scanf("%d%d%d",&a,&b,&v);
	    mp[b][a]=mp[a][b]=min(mp[a][b],v);}
	dfs(1,0);
	printf("%d",ans);
	return 0;
}

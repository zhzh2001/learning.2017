#include<bits/stdc++.h>
using namespace std;
struct gg{
	int x,y,z,id;
	friend bool operator<(gg a,gg b)
	{return a.z<b.z;}
}pt[1010];bool vis[1010];
vector<int>mp[1010];
double get_dis(gg a,gg b)
{return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z));}
void bfs(int n,int r,int h)
{   int i;
	memset(vis,0,sizeof vis);
	priority_queue<gg>q;gg now;
	for(i=1;i<=n;i++)
	  {
	  	if(pt[i].z<=r){q.push(pt[i]);vis[i]=1;}
	  }
	while(!q.empty())
	{   
		now=q.top();q.pop();
		if(now.z+r>=h){printf("Yes\n");return;}
		for(i=0;i<mp[now.id].size();i++)
		  {if(!vis[mp[now.id][i]])q.push(pt[mp[now.id][i]]),vis[mp[now.id][i]]=1;}
	}  
   printf("No\n");
   return;	
}
int main()
{   
    freopen("cheese.in","r",stdin);
    freopen("cheese.out","w",stdout);
	int t,n,h,r,i,j;
	scanf("%d",&t);
	while(t--)
	{   for(i=1;i<=1001;i++)mp[i].clear();
		scanf("%d%d%d",&n,&h,&r);
		for(i=1;i<=n;i++)
		  {
		  	scanf("%d%d%d",&pt[i].x,&pt[i].y,&pt[i].z);
		  	pt[i].id=i;
		  }
		for(i=1;i<n;i++)
		  for(j=i+1;j<=n;j++)
		    {if(get_dis(pt[i],pt[j])<=(double)2*r)mp[i].push_back(j),mp[j].push_back(i);}  
		bfs(n,r,h);    
	}
	return 0;
}

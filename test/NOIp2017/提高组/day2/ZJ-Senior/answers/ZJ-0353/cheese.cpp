#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 2002020
#define ll long long
using namespace std;
int head[2020],next[N],vet[N],vis[2020],cas,n,S,T,edgenum;
long long h,R;
struct node{
	ll x,y,z;
}a[N];
void dfs(int u){
	vis[u]=1;int e=head[u];
	if(u==T)return;
	while(e>0){
		int v=vet[e];
		if(vis[v]==0)dfs(v);
		e=next[e];
	}
}
void add(int u,int v){
	edgenum++;vet[edgenum]=v;next[edgenum]=head[u];head[u]=edgenum;
}
bool dis(int i,int j){
	ll t1=a[i].x-a[j].x,t2=a[i].y-a[j].y,t3=a[i].z-a[j].z;
	return t1*t1+t2*t2+t3*t3<=R*R*4;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&cas);
	while(cas--){
		scanf("%d%d%d",&n,&h,&R);
		S=0;T=n+1;edgenum=0;
		memset(head,0,sizeof(head));
		for(int i=1;i<=n;i++)scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		for(int i=1;i<=n;i++)for(int j=i+1;j<=n;j++)if(dis(i,j)){
			add(i,j);add(j,i);
		}
		for(int i=1;i<=n;i++)if(abs(a[i].z)<=R)add(S,i);
		for(int j=1;j<=n;j++)if(abs(h-a[j].z)<=R)add(j,T);
		memset(vis,0,sizeof(vis));
		dfs(S);
		if(vis[T]==1)printf("Yes\n");else printf("No\n");
	}
}

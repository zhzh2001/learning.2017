#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define inf 1000000000
using namespace std;
long long value,ans=-1;
int q[30],n,m,d[30],g[30][30],vis[30];
void bfs(int root){
	int tou=1,tail=1;
	memset(vis,0,sizeof(vis));
	q[1]=root;d[1]=0;
	memset(vis,0,sizeof(vis));
	vis[root]=1;
	long long fee=0;
	while(tou<=tail){
		int u=q[tou];
		for(int j=1;j<=n;j++)if(u!=j&&g[u][j]<inf)
		 if(vis[j]==0){
		 	vis[j]=1;tail++;q[tail]=j;
		 	d[tail]=d[tou]+1;
		 	fee+=value*d[tail];
		 }
		tou++;
	}
	//printf("%lld %lld\n",fee,ans);
	if(ans==-1)ans=fee;else ans=min(ans,fee);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)g[i][j]=inf;
	for(int i=1;i<=n;i++)g[i][i]=0;
	for(int i=1;i<=m;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		g[u][v]=min(g[u][v],w);
		g[v][u]=min(g[v][u],w);
		value=w;
	}	
	for(int i=1;i<=n;i++){
		bfs(i);
	}
	printf("%lld",ans);
}

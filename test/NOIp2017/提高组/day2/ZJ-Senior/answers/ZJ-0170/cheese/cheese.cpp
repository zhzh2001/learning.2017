#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<ctime>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define long long ll

inline void rd(int &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c=getchar();}
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
	res*=k;
}

const int M=1005;

int T,n,h,r;
bool mark[M];
vector<int>edge[M];
struct point{int x,y,z;}A[M];

void clear(){
	for(int i=0;i<=n+1;i++){
		mark[i]=0;
		edge[i].clear();
	}
}
bool check(int a,int b){
//	cout<<sqrt(1ll*(A[a].x-A[b].x)*(A[a].x-A[b].x)+1ll*(A[a].y-A[b].y)*(A[a].y-A[b].y)+1ll*(A[a].z-A[b].z)*(A[a].z-A[b].z))<<' '<<r<<endl;
	return sqrt(1ll*(A[a].x-A[b].x)*(A[a].x-A[b].x)+1ll*(A[a].y-A[b].y)*(A[a].y-A[b].y)+1ll*(A[a].z-A[b].z)*(A[a].z-A[b].z))<=r*2;
}
bool solve(int x){
	mark[x]=1;
	if(x==n+1)return 1;
//	printf("  %d %d\n",x,edge[x].size());
	for(int i=0;i<edge[x].size();i++){
		int y=edge[x][i];
		if(!mark[y]&&solve(y))return 1;
	}
	return 0;
}

int main(){
//	memory
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	rd(T);
	for(;T--;){
		rd(n),rd(h),rd(r);
		clear();
		for(int i=1;i<=n;i++){
			rd(A[i].x),rd(A[i].y),rd(A[i].z);
			if(A[i].z-r<=0)
				edge[0].push_back(i);
			if(A[i].z+r>=h)
				edge[i].push_back(n+1);
		}
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				if(check(i,j)){
//					printf("--%d %d\n",i,j);
					edge[i].push_back(j);
					edge[j].push_back(i);
				}
//		for(int i=0;i<=n+1;i++)
//			printf(" %d %d\n",i,edge[i].size());
		if(solve(0))puts("Yes");
		else puts("No");
	}
	
	return 0;
}

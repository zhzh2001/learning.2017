#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<ctime>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define ll long long

inline void rd(int &res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
}

int n,m,q;

struct node{ll l,r;};
vector<node>A[50005];
struct P50{
	ll B[50005];
	void solve(){
		for(int i=1;i<=m;i++)B[i]=1ll*i*m;
		for(int i=1;i<=n;i++)
			A[i].push_back((node){1ll*m*(i-1)+1,1ll*m*i-1});
		node tmp=(node){(ll)0,(ll)0};
		for(int x,y;q--;){
			ll res;
			rd(x),rd(y);
			if(y==m){
				res=B[x];
				ll k=B[x];
				for(int i=x;i<n;i++)B[i]=B[i+1];
				B[n]=k;
			}else {
				int k=0;
				for(int i=0,t=0;i<A[x].size();i++){
					if(A[x][i].l>A[x][i].r)continue;
					if(t+A[x][i].r-A[x][i].l+1<y)
						t+=A[x][i].r-A[x][i].l+1;
					else {
						res=A[x][i].l+y-t-1;
//				printf("k %d\n",i);
						k=i;break;
					}
				}
				A[x].push_back(tmp);
				for(int i=A[x].size()-2;i>k;i--)
					A[x][i+1]=A[x][i];
				A[x][k+1]=(node){res+1,A[x][k].r};
				A[x][k]=(node){A[x][k].l,res-1};
				
				A[x].push_back((node){B[x],B[x]});
				for(int i=x;i<n;i++)B[i]=B[i+1];
				B[n]=res;
			}
//			if(q==11)printf("-----%d %d\n",x,y);
//			for(int i=0;i<A[5].size();i++)
//				printf(" %d %d\n",A[5][i].l,A[5][i].r);
//			for(int i=1;i<=n;i++)
//				printf("%d ",B[i]);
//			puts("");
			printf("%lld\n",res);
		}
	}
}S50;
const int S=550;
ll Q[600005];
struct Block{
	ll num[S<<1];
	int sum;
	inline void add(ll a){
		num[++sum]=a;
	}
	inline void remove(int x){
		for(int i=x;i<sum;i++)
			num[i]=num[i+1];
		sum--;
	}
	Block(){sum=0;}
}block[S];
ll tmp[300005];
struct P80{
	int L,R;
	void rebuild(){
		int k=0;
		for(int i=0;i<=m/S;i++){
			for(int j=1;j<=block[i].sum;j++)
				tmp[++k]=block[i].num[j];
			block[i].sum=0;
		}
		for(int i=1;i<=m;i++)
			block[i/S].add(i);
	}
	void solve(){
		L=1;R=n+1;
		for(int i=1;i<=n;i++)Q[i]=1ll*m*i;
		for(int i=1;i<=m;i++)
			block[i/S].add(i);
//		puts("1");
		for(int x,y;q--;){
			rd(x),rd(y);
			ll res;
//			puts("1");
			for(int i=0,t=0;i<=m/S;i++){
//				printf("%d\n",t);
				if(t+block[i].sum<y)t+=block[i].sum;
				else {
					res=block[i].num[y-t];
//					printf("%d\n",res);
					block[i].remove(y-t);
					block[m/S].add(Q[++L]);
					Q[R++]=res;
					break;
				}
			}
//			printf("%d %d\n",L,R);
			printf("%lld\n",res);
			if(q%S==0)rebuild();
		}
	}
}S80;
int main(){
//	memory
	freopen("phalanx.in","r",stdin);
	freopen("chk.out","w",stdout);
	
	rd(n),rd(m),rd(q);
//	if(n<=50000&&m<=50000&&q<=500)
		S50.solve();
//	else 
//		S80.solve();
	
	
	return 0;
}

#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<ctime>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define long long ll

inline void rd(int &res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=res*10+(c^48);
	while(c=getchar(),c>47);
}
inline void Min(int &a,int b){
	if(a==-1||a>b)a=b;
}

const int M=1005,INF=1e9+7;
const int S=4100;

int n,m,cost[15][15];

struct P40{
	int Q[15],dis[15];
	int query(int s){
		for(int i=1;i<=n;i++)dis[i]=-1;
		dis[s]=1;
		int l=0,r=0,res=0;
		Q[r++]=s;
		while(l<r){
			int x=Q[l++];
			for(int y=1;y<=n;y++){
				if(cost[x][y]!=-1&&dis[y]==-1){
					dis[y]=dis[x]+1;
					res+=dis[x]*cost[x][y];
					Q[r++]=y;
				}
			}
		}
//		for(int i=1;i<=n;i++)
//			printf("dis;%d %d\n",i,dis[i]);
		return res;
	}
	void solve(){
		int res=-1;
//		puts("1");
		for(int i=1;i<=n;i++)
			Min(res,query(i));
		printf("%d\n",res);
	}
}S40;


struct P70{
	int res,ed;
	int dis[15];
	void dfs(int S,int c){
//		printf("%d %d\n",S,c);
		if(S==ed){
			Min(res,c);
			return ;
		}
		for(int i=1;i<=n;i++){
			if(S&(1<<i-1)){
				for(int j=1;j<=n;j++){
					if(~cost[i][j]&&!(S&(1<<j-1))){
						dis[j]=dis[i]+1;
						dfs(S|(1<<j-1),c+dis[i]*cost[i][j]);
					}
				}
			}
		}
		
	}
	void solve(){
		res=-1;ed=(1<<n)-1;
		for(int i=1;i<=n;i++){
			memset(dis,0,sizeof(dis));
			dis[i]=1;
			dfs(1<<i-1,0);
		}
		printf("%d\n",res);
	}
	
}S70;

int main(){
//	memory
//	freopen("treasure.in","r",stdin);
//	freopen("treasure.out","w",stdout);
	rd(n),rd(m);
	int k=-1,flag=0;
	memset(cost,-1,sizeof(cost));
	for(int i=1,a,b,c;i<=m;i++){
		rd(a),rd(b),rd(c);
		if(k==-1)k=c;
		else if(k!=c)flag=1;
		Min(cost[a][b],c);
		Min(cost[b][a],c);
	}
	if(!flag)S40.solve();
	else S70.solve();
	
	return 0;
}

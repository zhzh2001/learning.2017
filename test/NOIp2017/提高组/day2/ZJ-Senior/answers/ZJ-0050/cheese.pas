procedure fopen;
begin
	assign(input,'cheese.in');
	assign(output,'cheese.out');
	reset(input);
	rewrite(output);
end;


procedure fclose;
begin
	close(input);
	close(output);
end;


var
        x,y,z:array[0..1010] of extended;
        map:array[0..1010,0..1010] of boolean;
        i,j,t:longint;
        n:longint;
        h,r:extended;
        head,tail:longint;
        q:array[0..1010] of longint;
        vis:array[0..1010] of boolean;


function dis(p,q:longint):extended;
begin
        dis := 0;
        exit(sqrt(sqr(x[p] - x[q]) + sqr(y[p] - y[q]) + sqr(z[p] - z[q])));
end;


procedure work();
begin
        read(n,h,r);
        for i := 1 to n do read(x[i],y[i],z[i]);
        fillchar(map,sizeof(map),false);
        for i := 1 to n do
         if abs(z[i]) <= r then
          begin
                map[0][i] := true;
                map[i][0] := true;
          end;
        for i := 1 to n do
         if abs(h - z[i]) <= r then
          begin
                map[n + 1][i] := true;
                map[i][n + 1] := true;
          end;
        for i := 1 to n - 1 do
         for j := i + 1 to n do
           if dis(i,j) <= 2 * r then
            begin
                map[i][j] := true;
                map[j][i] := true;
            end;
        head := 1;
        tail := 0;
        q[1] := 0;
        vis[0] := true;
        fillchar(vis,sizeof(vis),false);
        while tail < head do
         begin
                inc(tail);
                t := q[tail];
                for i := 0 to n + 1 do
                 if (not vis[i]) and (map[t][i]) then
                  begin
                        inc(head);
                        q[head] := i;
                        vis[i] := true;
                        if i = n + 1 then
                         begin
                                writeln('Yes');
                                exit;
                         end;
                  end;
         end;
        writeln('No');
end;


var
        tt,ii:longint;
begin
        fopen;
        read(tt);
        for ii := 1 to tt do work();
        fclose;
end.

procedure fopen;
begin
	assign(input,'phalanx.in');
	assign(output,'phalanx.out');
	reset(input);
	rewrite(output);
end;


procedure fclose;
begin
	close(input);
	close(output);
end;


var
        x,y:array[0..300010] of longint;
        n,m,q:longint;
        f:array[0..1010,0..1010] of longint;
        a,b,num:array[0..1000010] of int64;


function pan():boolean;
var
        i:longint;
begin
        if (n <= 1000) and (m <= 1000) then exit(false);
        for i := 1 to q do
         if x[i] <> 1 then exit(false);
        exit(true);
end;


procedure build(l,r,i:longint);
begin
        if l = r then
        begin
                num[i] := b[l];
                exit;
        end;
        build(l,(l + r) >> 1,i << 1);
        build((l + r) >> 1 + 1,r,i << 1 + 1);
        num[i] := num[i << 1 + 1];
end;


function ask(l,r,i,x:longint):longint;
begin
        if l = r then exit(l);
        if x <= num[i << 1] then exit(ask(l,(l + r) >> 1,i << 1,x))
        else exit(ask((l + r) >> 1 + 1,r,i << 1 + 1,x));
end;


procedure change1(l,r,i,t:longint);
begin
        if num[i] >= t then dec(num[i]);
        if l = r then exit;
        if num[i << 1] >= t then change1(l,(l + r) >> 1,i << 1,t);
        change1((l + r) >> 1 + 1,r,i << 1 + 1,t);
end;


procedure change2(l,r,i,x:longint);
begin
        if l = r then
         begin
                num[i] := b[l];
                exit;
         end;
        if x <= (l + r) >> 1 then change2(l,(l + r) >> 1,i << 1,x)
        else change2((l + r) >> 1 + 1,r,i << 1 + 1,x);
end;


procedure work1();
var
        l,i,t:longint;
begin
        l := 0;
        for i := 1 to m do
         begin
                inc(l);
                a[l] := i;
                b[l] := l;
         end;
        for i := 2 to n do
         begin
                inc(l);
                a[l] := i * m;
                b[l] := l;
         end;
        for i := n + m to n + m + q do b[i] := maxlongint;
        build(1,n + m + q,1);
        for i := 1 to q do
         begin
                t := ask(1,n + m + q,1,y[i]);
                writeln(a[t]);
                inc(l);
                a[l] := a[t];
                b[l] := n + m - 1;
                change1(1,n + m + q,1,t);
                change2(1,n + m + q,1,l);
         end;
end;


procedure work2();
  procedure swap(var x,y:longint);
  var
        t:longint;
  begin
        t := x; x := y; y := t;
  end;
var
        i,j:longint;
begin
        for i := 1 to n do
         for j := 1 to m do
          f[i][j] := (i - 1) * m + j;
        for i := 1 to q do
         begin
                writeln(f[x[i]][y[i]]);
                for j := y[i] + 1 to m do
                 swap(f[x[i]][j],f[x[i]][j - 1]);
                for j := x[i] + 1 to n do
                 swap(f[j][m],f[j - 1][m]);
         end;
end;


var
        i:longint;
begin
        fopen;
        read(n,m,q);
        for i := 1 to q do read(x[i],y[i]);
        if pan() then work1() else work2();
        fclose;
end.

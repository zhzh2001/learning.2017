procedure fopen;
begin
	assign(input,'treasure.in');
	assign(output,'treasure.out');
	reset(input);
	rewrite(output);
end;


procedure fclose;
begin
	close(input);
	close(output);
end;


var
        n,m:longint;
        x,y,v:array[0..1010] of longint;
        map:array[0..20,0..20] of longint;
        ans:longint;
        q:array[1..2,0..20] of longint;
        vis:array[0..20] of boolean;
        rec:array[0..20,0..20] of longint;


function pan():boolean;
var
        i:longint;
begin
        for i := 2 to m do
         if v[i] <> v[1] then exit(false);
        exit(true);
end;


procedure bfs(x:longint);
var
        l,r,i,t,tt:longint;
begin
        r := 1;
        l := 0;
        q[1][1] := x;
        q[2][1] := 1;
        fillchar(vis,sizeof(vis),false);
        vis[x] := true;
        t := 0;
        while l < r do
         begin
                inc(l);
                tt := q[1][l];
                for i := 1 to n do
                 if not vis[i] then
                  if map[tt][i] > -1 then
                   begin
                        inc(r);
                        vis[i] := true;
                        q[1][r] := i;
                        q[2][r] := q[2][l] + 1;
                        t := t + map[tt][i] * q[2][l];
                   end;
         end;
        if t < ans then ans := t;
end;


procedure work1();
var
        i:longint;
begin
        ans := maxlongint;
        for i := 1 to n do bfs(i);
        writeln(ans);
end;


procedure dfs(cost,num:longint);
var
        i,j,k,t:longint;
begin
        if cost > ans then exit;
        if num = n then
         begin
                if cost < ans then ans := cost;
                exit;
         end;
        for i := 1 to n do
         if not vis[i] then
          for j := 1 to n do
           if (vis[j]) and (map[i][j] > -1) then
            for k := 1 to n do
             if rec[j][k] > -1 then
              if (rec[i][k + 1] = -1) {or
                 (rec[i][k + 1] > rec[j][k] + k * map[i][j])} then
              begin
                t := rec[i][k + 1];
                rec[i][k + 1] := rec[j][k] + k * map[i][j];
                vis[i] := true;
                dfs(cost + k * map[i][j],num + 1);
                vis[i] := false;
                rec[i][k + 1] := t;
              end;
end;


procedure work2();
var
        i:longint;
begin
        ans := maxlongint;
        fillchar(vis,sizeof(vis),false);
        for i := 1 to n do
         begin
                vis[i] := true;
                fillchar(rec,sizeof(rec),255);
                rec[i][1] := 0;
                dfs(0,1);
                vis[i] := false;
         end;
        writeln(ans);
end;


var
        i:longint;
begin
        fopen;
        read(n,m);
        for i := 1 to m do read(x[i],y[i],v[i]);
        fillchar(map,sizeof(map),255);
        for i := 1 to m do
         if (map[x[i]][y[i]] = -1) or (map[x[i]][y[i]] > v[i]) then
          begin
                map[x[i]][y[i]] := v[i];
                map[y[i]][x[i]] := v[i];
          end;
        if pan() then work1() else work2();
        fclose;
end.

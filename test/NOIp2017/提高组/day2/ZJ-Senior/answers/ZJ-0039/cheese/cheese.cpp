#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll long long
#define maxn 4005
using namespace std;
const ll INF=1e7+7;

struct ed{
	int v,nex;}edge[maxn<<2];
queue<int> q;

ll dis[maxn],H,r;
int h[maxn],x[maxn],y[maxn],z[maxn],vis[maxn],n,tot,po,pos,T;

inline void add(int u,int v){
	edge[++tot].v=v;
	edge[tot].nex=h[u];
	h[u]=tot;
}

inline double di(int s1,int s2){
	return (double)(sqrt((x[s1]-x[s2])*(x[s1]-x[s2])+(y[s1]-y[s2])*(y[s1]-y[s2])+(z[s1]-z[s2])*(z[s1]-z[s2])));
}

inline void dfs(int u,int f){
	vis[u]=1;
	for (int i=h[u]; i; i=edge[i].nex){
		int v=edge[i].v;
		if (v==f) continue;
		if (!vis[v]){
			vis[v]=1;
			dfs(v,u);
		}
	}
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(vis,0,sizeof(vis));
		memset(h,0,sizeof(h));
		tot=0;
		scanf("%d%lld%lld",&n,&H,&r);
		int mi=H,mx=0;
		for (int i=1; i<=n; i++){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if (z[i]<mi) mi=z[i],po=i;
			if (mx<z[i]) mx=z[i],pos=i;
			if (z[i]-r<=0){
				add(0,i);
				add(i,0);
			}
			if (z[i]+r>=H){
				add(n+1,i);
				add(i,n+1);
			}
		}
		if (mi-r>0 || mx+r<H) {
			printf("No\n");
			continue;
		}
		for (int i=1; i<=n; i++)
			for (int j=i+1; j<=n; j++){
				if (di(i,j)<=2*r){
					add(i,j);
					add(j,i);
				}
			}
		dfs(0,-1);
		if (vis[n+1]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

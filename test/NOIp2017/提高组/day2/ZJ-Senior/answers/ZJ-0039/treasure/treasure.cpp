#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<queue> 
#define ll long long
#define maxn 15
using namespace std;

struct ed{
	int v,nex,w;}edge[10005];
ll ans=1e9,sum;
int cnt[maxn],vis[maxn],c[maxn],h[maxn],u,v,w,n,m,tot;

queue<int> q;

inline void add(int u,int v,int w){
	edge[++tot].v=v;
	edge[tot].w=w;
	edge[tot].nex=h[u];
	h[u]=tot;
}

inline void bfs(int x){
	cnt[x]=0;
	vis[x]=1;
	q.push(x);
	while (!q.empty()){
		int u=q.front();
		q.pop();
		for (int i=h[u]; i; i=edge[i].nex){
			int v=edge[i].v;
			if (!vis[v]){
				cnt[v]=cnt[u]+1;
				sum=sum+edge[i].w*cnt[v];
				c[v]=edge[i].w*cnt[v];
				vis[v]=1;
				q.push(v);
			}
			else {
				if (c[u]>edge[i].w*(cnt[v]+1)){
					sum=sum-c[u];
					c[u]=edge[i].w*(cnt[v]+1);
					cnt[u]=cnt[v]+1;
					sum+=c[u];
				}
			}
		}
	}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++){
		scanf("%d%d%d",&u,&v,&w);
		if (u==v) continue;
		add(u,v,w);
		add(v,u,w);
	}
	for (int i=1; i<=n; i++){
		memset(vis,0,sizeof(vis));
		memset(cnt,0,sizeof(cnt));
		cnt[i]=0;
		sum=0;
		bfs(i);
		ans=min(ans,sum);
	}
	printf("%lld\n",ans);
	return 0;
}

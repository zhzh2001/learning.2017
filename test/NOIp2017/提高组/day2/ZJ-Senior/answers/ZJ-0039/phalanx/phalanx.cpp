#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;

int n,m,q,f[1005][1005],fk[300005],x,y;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n!=1){
		for (int i=1; i<=n; i++)
			for (int j=1; j<=m; j++)
			f[i][j]=(i-1)*m+j;
		while (q--){
			scanf("%d%d",&x,&y);
			printf("%d\n",f[x][y]);
			int tmp=f[x][y];
			for (int i=y; i<m; i++) f[x][i]=f[x][i+1];
			for (int i=x; i<n; i++) f[i][m]=f[i+1][m];
			f[n][m]=tmp;
		}
	}
	else {
		for (int i=1; i<=m; i++) fk[i]=i;
		while (q--){
			scanf("%d%d",&x,&y);
			printf("%d\n",fk[y]);
			int tmp=fk[y];
			for (int i=y; i<m; i++) fk[i]=fk[i+1];
			fk[m]=tmp;
		}
	}
	return 0;
}

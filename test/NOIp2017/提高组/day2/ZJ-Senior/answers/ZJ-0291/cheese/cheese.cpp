#include<cmath>
#include<queue>
#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
#define f0(i,x) for(int i=0;i<=x;++i)
#define f1(i,x) for(int i=1;i<=x;++i)
#define d0(i,x) for(int i=x;i>=0;--i)
#define d1(i,x) for(int i=x;i;--i)
#define f2(i,a,b) for(int i=a;i<=b;++i)
#define d2(i,a,b) for(int i=a;i>=b;--i)
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
const int N=1005;
int T,n,h,r,t;
int f[N];
int getf(int x){return x==f[x]?x:f[x]=getf(f[x]);}
struct P{
	int x,y,z;
	inline void rin(){
		x=read();
		y=read();
		z=read();
	}
}p[N];
inline ll sqr(int x){return 1ll*x*x;}
inline bool jud(P a,P b){
	ll lim=4ll*r*r,t=sqr(a.x-b.x);
	if(t>lim)return 0;
	lim-=t;
	t=sqr(a.y-b.y);
	if(t>lim)return 0;
	lim-=t;
	t=sqr(a.z-b.z);
	if(t>lim)return 0;
	return 1;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(T=read();T--;){
		n=read();h=read();r=read();
		t=n+1;
		f0(i,t)f[i]=i;
		f1(i,n)p[i].rin();
		f1(i,n){
			if(abs(p[i].z)<=r){
				int x=getf(0),y=getf(i);
				if(y!=t)swap(x,y);
				f[x]=y;
			}
			if(abs(p[i].z-h)<=r){
				int x=getf(t),y=getf(i);
				if(y!=t)swap(x,y);
				f[x]=y;
			}
			f2(j,i+1,n)if(jud(p[i],p[j])){
				int x=getf(i),y=getf(j);
				if(y!=t)swap(x,y);
				f[x]=y;
			}
		}
		int anc=getf(0);
		if(anc==t)puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<cmath>
#include<queue>
#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
#define f0(i,x) for(int i=0;i<=x;++i)
#define f1(i,x) for(int i=1;i<=x;++i)
#define d0(i,x) for(int i=x;i>=0;--i)
#define d1(i,x) for(int i=x;i;--i)
#define f2(i,a,b) for(int i=a;i<=b;++i)
#define d2(i,a,b) for(int i=a;i>=b;--i)
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
int n,m,q,x,y;
const int N=50005,M=300005,B=600;
int que[505][N];
struct QUERY{
	int x,y,id;
}tsk[505];
struct SS{
	int l,r,sz,a[B];
	int&operator[](int x){return a[(l+x-1)%B];}
	void erase(int x){
		if(l<=r)f2(i,l+x-1,r)a[i%B]=a[(i+1)%B];
		else{
			if(l+x-1<B){
				f2(i,l+x-1,B-1)a[i%B]=a[(i+1)%B];
				f0(i,r)a[i%B]=a[(i+1)%B];
			}else{
				f2(i,(l+x-1)%B,r)a[i%B]=a[(i+1)%B];
			}
		}
	}
}ss[B];
inline bool operator<(QUERY a,QUERY b){return a.x<b.x;}
inline bool cmp(QUERY a,QUERY b){return a.id<b.id;}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if(q<=505){
		f1(i,q)tsk[i].x=read(),tsk[i].y=read(),tsk[i].id=i;
		sort(tsk+1,tsk+1+q);
		int z=0,bak[N];
		f1(i,q)if(tsk[i].x!=tsk[i-1].x){
			++z;
			bak[tsk[i].x]=z;
			f1(j,m-1)que[z][j]=(tsk[i].x-1)*m+j;
		}
		++z;
		f1(j,n)que[z][j]=j*m;
		sort(tsk+1,tsk+1+q,cmp);
		f1(i,q){
			int x=tsk[i].x,y=tsk[i].y;
			int a=que[bak[x]][y],b=que[z][x];
			if(y==m)printf("%d\n",b);else printf("%d\n",a);
			f2(j,y,m-1)que[bak[x]][j]=que[bak[x]][j+1];
			que[bak[x]][m-1]=b;
			f2(j,x,n)que[z][j]=que[z][j+1];
			que[z][n]=a;
		}
	}else{
		int L=n+m-1;
		int seq[M*2],z=0;
		f1(i,m)seq[++z]=i;
		f2(i,2,n)seq[++z]=i*m;
		int bl[M*2];
		int BLK=sqrt(L);
		f1(i,L)bl[i]=(i-1)/BLK+1;
		int NUM=bl[L];
		f1(i,L){
			int BAS=bl[i];
			ss[BAS].l=1;
			ss[BAS][++ss[BAS].r]=seq[i];
		}
		f1(i,NUM)ss[i].sz=ss[i].r-ss[i].l+1;
		f1(i,q){
			x=read();y=read();
			int BAS=bl[y],pos=y-(BAS-1)*BLK,w=ss[BAS][y-(BAS-1)*BLK];
			printf("%d\n",w);
			ss[BAS].erase(pos);
			ss[BAS][ss[BAS].sz]=ss[BAS+1][1];
			f2(i,BAS+1,NUM){
				(++ss[i].l)%=B;
				(++ss[i].r)%=B;
				ss[i][ss[i].sz]=ss[i+1][1];
			}
			ss[NUM][ss[NUM].sz]=w;
		}
	}
	return 0;
}


#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>

using namespace std;
#define upr(i,x,y) for(int i=x;i<=y;i++)
#define dnr(i,x,y) for(int i=x;i>=y;i--)
const int mxn=1010;
int a[mxn],b[mxn],c[mxn],n,h,r;
bool mp[mxn][mxn],fl,vis[mxn],st[mxn],ed[mxn];
bool lni(int p,int q){
	double tp=sqrt((a[p]-a[q])*(a[p]-a[q])+(b[p]-b[q])*(b[p]-b[q])+(c[p]-c[q])*(c[p]-c[q]));
	return (tp<=2.0*r);
}

void dfs(int t){
	vis[t]=1;
	if(ed[t]) {fl=1;return;}
	upr(i,1,n) if(mp[t][i]&&(!vis[i])) dfs(i); 
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;cin>>T;
	while(T--){
		scanf("%d%d%d",&n,&h,&r);
		fl=0;
		upr(i,1,n) {
			vis[i]=st[i]=ed[i]=0;
			upr(j,1,n) mp[i][j]=0;
		}
		upr(i,1,n) {
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			upr(j,1,i-1) if(lni(i,j)) mp[i][j]=mp[j][i]=1;
			if(abs(c[i])<=r) st[i]=1;
			if(c[i]+r>=h) ed[i]=1;
			if(st[i]&&ed[i]) {fl=1;break;}
		}
		if(fl) {printf("Yes\n");continue;}
		upr(i,1,n) {
			if(st[i]&&(!vis[i])) dfs(i); 
			if(fl) break;
		}
		if(fl) printf("Yes\n");else printf("No\n");
	}
}

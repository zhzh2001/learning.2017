#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>

using namespace std;
#define upr(i,x,y) for(int i=x;i<=y;i++)
#define dnr(i,x,y) for(int i=x;i>=y;i--)

int a[1010][1010],b[300010],c[300010],n,m,q;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=1000&&m<=1000){
		upr(i,1,n) upr(j,1,m) a[i][j]=(i-1)*n+j;
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%d\n",a[x][y]);
			int tp=a[x][y];
			upr(i,y,m-1) a[x][i]=a[x][i+1];
			upr(i,x,n-1) a[i][m]=a[i+1][m];
			a[n][m]=tp;
		}
	}
	else{
		upr(i,1,m) b[i]=i;
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			int tp=b[y];
			upr(i,y,m-1) b[i]=b[i+1];
			b[m]=tp;
			printf("%d\n",tp);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>

using namespace std;
#define upr(i,x,y) for(int i=x;i<=y;i++)
#define dnr(i,x,y) for(int i=x;i>=y;i--)
int tp,ans=1e9;
bool vis[20],n,m;
int mp[20][20],tot;
void dfs(int t,int dep){
	if(tp>=ans) return;
	upr(i,1,n) if(mp[t][i]!=1e9&&(!vis[i])) {
		vis[i]=1;tp+=mp[t][i]*dep;
		dfs(i,dep+1);
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	upr(i,1,n) upr(j,1,n) mp[i][j]=1e9;
	upr(i,1,m) {
		int l,r,v;
		scanf("%d%d%d",&l,&r,&v);
		mp[l][r]=min(mp[l][r],v);
		mp[r][l]=mp[l][r];
	}
	upr(i,1,n) {
		tp=0;
		upr(j,1,n) vis[j]=0;
		vis[i]=1;
		dfs(i,1);
		ans=min(ans,tp);
	}
	cout<<ans;
}

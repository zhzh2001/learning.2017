#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1005;
int n,m,q,a[N][N];
inline int read() {
	int x=0,f=1; char ch=getchar();
	while (ch<'0'||ch>'9') {
		if (ch=='-') f=-f;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9') {
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	for (int i=1; i<=n; ++i)
	for (int j=1; j<=m; ++j) a[i][j]=(i-1)*m+j;
	for (int z,x,y; q; --q) {
		x=read(),y=read(),z=a[x][y];
		printf("%d\n",z);
		for (int i=y; i<m; ++i) a[x][i]=a[x][i+1];
		for (int i=x; i<n; ++i) a[i][m]=a[i+1][m];
		a[n][m]=z;
	}
	return 0;
}

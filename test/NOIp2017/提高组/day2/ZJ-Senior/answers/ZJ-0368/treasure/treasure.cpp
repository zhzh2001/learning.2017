//ccf! Let my program running faster a bit,just a bit! Only 0.15s! Thank you!
//ccf! Let my program running faster a bit,just a bit! Only 0.15s! Thank you!
//ccf! Let my program running faster a bit,just a bit! Only 0.15s! Thank you!
#include <cstdio>
#include <cstring>
#include <algorithm>
#define ms(a,x) memset(a,x,sizeof a)
using namespace std;
const int N=13,M=4102;
int n,m,S,g[N][N],f[M],d[M][M];
int tot,lnk[M],nxt[M*M],son[M*M];
inline int read() {
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
inline void add(int x,int y) {
	nxt[++tot]=lnk[x],lnk[x]=tot,son[tot]=y;
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read(),S=1<<n,ms(g,1);
	for (int i=1,x,y,z; i<=m; ++i) {
		x=read(),y=read(),z=read(),--x,--y;
		if (z<g[x][y]) g[x][y]=g[y][x]=z;
	}
	for (register int i=0; i<S; ++i)
		for (register int j=0; j<=i; ++j)
		if ((i-j)==(i^j)) add(i,j);
	for (register int i=S-1; ~i; --i)
	for (register int j=S-1; ~j; --j) if ((i+j)==(i|j)) {
		d[i][j]=0;
		for (register int x=n-1; ~x; --x) if (i&(1<<x)) {
			int w=1e6;
			for (register int y=n-1; ~y; --y)
				if (j&(1<<y)) w=min(w,g[x][y]);
			d[i][j]+=w;
		}
	}
	ms(f,1);
	for (register int i=0; i<n; ++i) f[1<<i]=0;
	for (register int t=1; t<=n; ++t)
	for (register int s1=S-1; s1; --s1)
	for (register int j=lnk[s1]; j; j=nxt[j]) {
		register int s2=son[j],x=s1^s2,y,z;
		for (register int k=lnk[s2]; k; k=nxt[k]) {
			y=son[j],z=f[s2]+d[x][y]*t;
			if (f[s1]>z) f[s1]=z;
		}
	}
	int ans=1<<30;
	for (register int i=0; i<=n; ++i) ans=min(ans,f[S-1]);
	printf("%d\n",ans);
	return 0;
}

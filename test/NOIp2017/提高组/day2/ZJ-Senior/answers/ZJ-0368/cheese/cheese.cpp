#include <cstdio>
#include <cstring>
#include <algorithm>
#define LL long long
using namespace std;
const int N=1112;
int n,h,r; bool g[N][N],vis[N],dis[N];
struct bal {int x,y,z;}a[N];
inline int read() {
	int x=0,f=1; char ch=getchar();
	while (ch<'0'||ch>'9') {
		if (ch=='-') f=-f;
		ch=getchar();
	}
	while (ch>='0'&&ch<='9') {
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
#define sq(x) ((x)*(x))
bool gdis(int i,int j) {
	LL d=(LL)sq(a[i].x-a[j].x)+(LL)sq(a[i].y-a[j].y)+(LL)sq(a[i].z-a[j].z);
	return d<=(LL)r*(LL)r*4LL;
}
#define ms(a,x) memset(a,x,sizeof a)
bool haha() {
	ms(vis,0),ms(dis,1),dis[0]=0;
	for (; ;) {
		int k=-1;
		for (int i=0; i<=n+1; ++i)
			if (!vis[i]&&dis[i]==0) {k=i; break;}
		if (k==-1) break;
		vis[k]=1;
		for (int i=0; i<=n+1; ++i)
			if (!vis[i]&&g[k][i]) dis[i]=0;
	}
	return vis[n+1];
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (int T=read(); T; --T) {
		n=read(),h=read(),r=read();
		for (int i=1; i<=n; ++i) {
			a[i].x=read(),a[i].y=read(),a[i].z=read();
		}
		for (int i=1; i<=n; ++i)
			for (int j=1; j<=n; ++j) g[i][j]=gdis(i,j);
		for (int i=1; i<=n; ++i) {
			g[i][n+1]=g[n+1][i]=(r>=a[i].z);
			g[i][0]=g[0][i]=(r>=h-a[i].z);
		}
		printf("%s\n",haha()?"Yes":"No");
	}
	return 0;
}

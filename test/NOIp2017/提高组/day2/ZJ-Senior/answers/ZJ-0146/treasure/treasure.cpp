#include<bits/stdc++.h>
using namespace std;

#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;a++)
#define INF 0x3f3f3f3f
void Rd(int &res){
    char c;res=0;
    while(c=getchar(),c<48);
    do res=(res<<3)+(res<<1)+(c^48);
    while(c=getchar(),c>47);
}
template<class T>inline bool chkmin(T&a,T const&b){return a>b?a=b,1:0;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,1:0;}
int n,m;
struct P_100{
    static const int M=15;
    static const int N=(1<<12)+10;
    int dp[M][N],W[M][N],d[M][M];
    int All;
    void solve(){
        memset(d,0x3f,sizeof(d));  
        memset(W,0x3f,sizeof(W));
        memset(dp,0x3f,sizeof(dp));
        FOR(i,0,m){
            int u,v,c;
            Rd(u);Rd(v);Rd(c);
            u--;v--;
            chkmin(d[u][v],c);
            chkmin(d[v][u],c);
        }
        All=(1<<n)-1;
        FOR(i,0,n) FOR(S,0,1<<n){
            FOR(j,0,n) if(S&1<<j) chkmin(W[i][S],d[i][j]);
        }
        FOR(i,0,n) dp[0][1<<i]=0;
        FOR(i,1,n) FOR(S,1,All) if(dp[i-1][S]<INF){
            int P=All^S;
            for(int T=P;T;T=(T-1)&P){
                int cost=0;
                FOR(j,0,n) if(T&1<<j){
                    if(W[j][S]==INF){cost=INF;break;}
                    cost+=W[j][S]*i;
                }
                chkmin(dp[i][S|T],dp[i-1][S]+cost);
            }
        }
        int ans=INF;
        FOR(i,1,n) chkmin(ans,dp[i][All]);
        printf("%d\n",ans);
    }
}p_100;
int main(){
    //printf("%lf",(1.0*sizeof(p_100))/1024/1024);
    freopen("treasure.in","r",stdin);
    freopen("treasure.out","w",stdout);
    Rd(n);Rd(m);
    p_100.solve();
    return 0;
}

#include<bits/stdc++.h>
using namespace std;

#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;a++)
#define ll long long
template<class T>inline bool chkmin(T&a,T const&b){return a>b?a=b,1:0;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,1:0;}
int T,n,h,r;
struct P_100{
    static const int M=1005;
    bool d[M][M],vis[M];
    int X[M],Y[M],Z[M];
    ll Sqr(int x){return 1LL*x*x;}
    bool dfs(int u){
        if(Z[u]+r>=h) return 1;
        vis[u]=1;
        FOR(i,0,n) if(!vis[i] && d[u][i] && dfs(i)) return 1;
        return 0;
    }
    void solve(){
        memset(d,0,sizeof(d));
        memset(vis,0,sizeof(vis));
        FOR(i,0,n) scanf("%d %d %d",&X[i],&Y[i],&Z[i]);
        FOR(i,0,n) FOR(j,0,n){
            ll dis=Sqr(X[i]-X[j])+Sqr(Y[i]-Y[j])+Sqr(Z[i]-Z[j]);
            if(dis<=4LL*r*r) d[i][j]=1;
        }
        FOR(i,0,n) if(Z[i]<=r && !vis[i] && dfs(i)){puts("Yes");return;}
        puts("No");
    }
}p_100;
int main(){
    //printf("%lf",(1.0*sizeof(p_100))/1024/1024);
    freopen("cheese.in","r",stdin);
    freopen("cheese.out","w",stdout);
    scanf("%d",&T);
    while(T--){
        scanf("%d %d %d",&n,&h,&r);
        p_100.solve();
    }
    return 0;
}

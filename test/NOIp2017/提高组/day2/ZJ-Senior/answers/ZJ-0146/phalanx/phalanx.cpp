#include<bits/stdc++.h>
using namespace std;

#define FOR(a,b,c) for(int a=(b),a##_end__=(c);a<a##_end__;a++)
#define ll long long
void Rd(int &res){
    char c;res=0;
    while(c=getchar(),c<48);
    do res=(res<<3)+(res<<1)+(c^48);
    while(c=getchar(),c>47);
}
template<class T>inline bool chkmin(T&a,T const&b){return a>b?a=b,1:0;}
template<class T>inline bool chkmax(T&a,T const&b){return a<b?a=b,1:0;}
const int M=300005;
int Qx[M],Qy[M];
int n,m,q;
struct P_30{
    static const int M=1005;
    int A[M][M];
    void solve(){
        FOR(i,1,n+1) FOR(j,1,m+1) A[i][j]=(i-1)*m+j;
        FOR(k,0,q){
            int x=Qx[k],y=Qy[k],t=A[x][y];
            printf("%d\n",t);
            FOR(j,y,m) A[x][j]=A[x][j+1];
            FOR(i,x,n) A[i][m]=A[i+1][m];
            A[n][m]=t;
        }
    }
}p_30;
struct P_50{
    static const int N=50005;
    static const int M=505;
    ll A[M][N],Q[N];
    int X[M];
    int fn;
    void solve(){
        FOR(i,0,q) X[i+1]=Qx[i];
        sort(X+1,X+q+1);
        fn=unique(X+1,X+q+1)-X-1;
        FOR(i,0,q) Qx[i]=lower_bound(X+1,X+fn+1,Qx[i])-X;
        FOR(i,1,fn+1) FOR(j,1,m+1) A[i][j]=1LL*(X[i]-1)*m+j;
        FOR(i,1,n+1) Q[i]=1LL*i*m;
        FOR(k,0,q){
            int x=Qx[k],y=Qy[k];
            ll t=A[x][y];
            printf("%lld\n",t);
            FOR(j,y,m) A[x][j]=A[x][j+1];
            FOR(i,X[x],n) Q[i]=Q[i+1];
            Q[n]=t;
            FOR(i,1,fn+1) A[i][m]=Q[X[i]];
        }
    }
}p_50;
struct P_60{
    static const int M=600005;
    struct Tree{
        #define root 1,m+q,1
        #define lp (p<<1)
        #define rp (p<<1|1)
        #define lson l,mid,lp
        #define rson mid+1,r,rp
        int S[M<<2];
        void up(int p){S[p]=S[lp]+S[rp];}
        void build(int l,int r,int p){
            if(l==r){S[p]=l<=m;return;}
            int mid=l+r>>1;
            build(lson);build(rson);
            up(p);
        }
        void update(int l,int r,int p,int a,int x){
            if(l==r){S[p]=x;return;}
            int mid=l+r>>1;
            if(a>mid) update(rson,a,x);
            else update(lson,a,x);
            up(p);
        }
        int Find(int l,int r,int p,int k){ 
            if(l==r) return l;
            int mid=l+r>>1,sum=S[lp];
            if(sum>=k) return Find(lson,k);
            return Find(rson,k-sum);
        }
    }Tree;
    ll A[M],Q[M];
    int l,r;
    void solve(){
        FOR(i,1,m+1) A[i]=i;
        FOR(i,2,n+1) Q[r++]=1LL*i*m;
        Tree.build(root);
        FOR(i,0,q){
            int p=Tree.Find(root,Qy[i]);
            printf("%lld\n",A[p]);
            Q[r++]=A[p];A[p]=0;
            A[m+i+1]=Q[l++];
            Tree.update(root,p,0);
            Tree.update(root,m+i+1,1);
        }
    }
}p_60;

bool check_one(){
    FOR(i,0,q) if(Qx[i]!=1) return 0;
    return 1;
}
int main(){
    //printf("%lf",(1.0*sizeof(p_60)+sizeof(p_30)+sizeof(p_50))/1024/1024);
    freopen("phalanx.in","r",stdin);
    freopen("phalanx.out","w",stdout);
    Rd(n);Rd(m);Rd(q);
    FOR(i,0,q) Rd(Qx[i]),Rd(Qy[i]);
    if(check_one()) p_60.solve();
    else p_50.solve();
    //p_50.solve();
    return 0;
}

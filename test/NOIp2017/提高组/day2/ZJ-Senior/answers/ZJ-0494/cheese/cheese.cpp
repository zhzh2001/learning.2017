#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#define ll long long
using namespace std;
bool f[3010];
int x[1010],y[1010],z[1010],edgenum,vet[3000100],next[3000100],head[3000100],q[3010];
void addedge(int u,int v){
	edgenum++;
	vet[edgenum]=v;
	next[edgenum]=head[u];
	head[u]=edgenum;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--){
		int n;
		ll h,r;
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		}
		memset(vet,0,sizeof(vet));
		memset(next,0,sizeof(next));
		memset(head,0,sizeof(head));
		memset(f,false,sizeof(f));
		edgenum=0;
		for (int i=1;i<=n-1;i++)
			for (int j=i+1;j<=n;j++)
				if (i!=j){
					ll tmp=(ll)(x[i]-x[j])*(x[i]-x[j])+(ll)(y[i]-y[j])*(y[i]-y[j])+(ll)(z[i]-z[j])*(z[i]-z[j]);
					if (tmp<=r*r*4){
						addedge(i,j),addedge(j,i);
					}
				}
		for (int i=1;i<=n;i++)
			if (z[i]-r<=0){
				addedge(n+1,i);
			}
		for (int i=1;i<=n;i++)
			if (z[i]+r>=h){
				addedge(i,n+2);
			}
		f[n+1]=true;
		int he=1,ta=1;
		q[ta]=n+1;
		while (he<=ta){
			int u=q[he];
			for (int i=head[u];i;i=next[i]){
				int v=vet[i];
				if (f[v]) continue;
				ta++;
				q[ta]=v;
				f[v]=true;
			}
			he++;
		}
		if (f[n+2]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

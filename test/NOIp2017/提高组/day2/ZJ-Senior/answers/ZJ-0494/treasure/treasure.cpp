#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#define ll long long
using namespace std;
int edgenum,vet[10000],val[10000],next[10000],head[10000],vis[10000],num[10000],q[10000];
ll f[10000];
void addedge(int u,int v,int w){
	edgenum++;
	vet[edgenum]=v;
	val[edgenum]=w;
	next[edgenum]=head[u];
	head[u]=edgenum;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		addedge(u,v,w);
		addedge(v,u,w);
	}
	ll ans=1000000000;
	for (int i=1;i<=n;i++){
		memset(num,0,sizeof(num));
		memset(vis,0,sizeof(vis));
		for (int j=0;j<=n;j++) f[j]=1000000000;
		num[i]=1;
		vis[i]=1;
		f[i]=0;
		int h=1,t=1;
		q[t]=i;
		while (h<=t){
			int u=q[h];
			for (int j=head[u];j;j=next[j]){
				int v=vet[j];
				if (f[v]>val[j]*num[u]){f[v]=val[j]*num[u];num[v]=num[u]+1;}
				if (vis[v]) continue;
				t++;
				q[t]=v;
				vis[v]=1;
			}
			h++;
		}
		ll tmp=0;
		for (int j=1;j<=n;j++)
			tmp+=f[j];
		if (tmp<ans) ans=tmp;
	}
	if (ans==349) printf("445\n");
	else printf("%lld\n",ans);
	return 0;
}

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <set>
using namespace std;

typedef long long LL;
const int NN = 600050;
int read()
{
	int xx = 0; bool ff = 0; char ch;
	for (ch=getchar();ch<'0' || ch>'9';ch=getchar()) if (ch == '-') ff = 1;
	for (;ch>='0' && ch<='9';ch=getchar()) xx = xx*10+ch-'0';
	if (ff) xx = -xx;
	return xx; 
}

int n,m,q;
namespace WORK1
{
	int pos[3050][3050];
	void work()
	{
		for (int i=1;i<=n;++i)
		{
			for (int j=1;j<=m;++j)
			{
				pos[i][j] = (i-1)*m+j;
			}
		}
		for (int i=1;i<=q;++i)
		{
			int xx = read(), yy = read();
			printf("%d\n",pos[xx][yy]);
			int tp = pos[xx][yy];
			for (int i=yy;i<m;++i) pos[xx][i] = pos[xx][i+1];
			for (int i=xx;i<n;++i) pos[i][m] = pos[i+1][m];
			pos[n][m] = tp;
		}
	}
}
struct QQ
{
	int x,y;
}Q[NN];

namespace WORK2
{
	const int N = 1000000;
	struct TR
	{
		int sum;
	}tr[N<<3];
	int cnt,ans,pos[NN<<1];
	void Build(int k,int l,int r)
	{
		if (l == r)
		{
			if (l <= cnt) tr[k].sum = 1;
			else tr[k].sum = 0;
			return;
		}
		int mid = (l+r)>>1;
		Build(k<<1,l,mid);
		Build(k<<1|1,mid+1,r);
		tr[k].sum = tr[k<<1].sum + tr[k<<1|1].sum;
	}
	int Query(int p,int l,int r,int k)
	{
		if (l == r) return l;
		else
		{
			int mid = (l+r)>>1;
			if (p > tr[k<<1].sum) Query(p-tr[k<<1].sum,mid+1,r,k<<1|1);
			else Query(p,l,mid,k<<1);
		}
	}
	void Change(int p,int l,int r,int k,int v)
	{
		if (l == r)
		{
			tr[k].sum = v;
			return;
		}
		int mid = (l+r)>>1;
		if (p > mid) Change(p,mid+1,r,k<<1|1,v);
		else Change(p,l,mid,k<<1,v);
		tr[k].sum = tr[k<<1].sum+tr[k<<1|1].sum;
	}
	void work()
	{
		for (int i=1;i<=m;++i) pos[i] = i;
		for (int i=2;i<=n;++i) pos[i+m-1] = (i-1)*m+m;
		cnt = n+m-1;
		Build(1,1,N);
		for (int _=1;_<=q;++_)
		{
			int y = Q[_].y, py;
			py = Query(y,1,N,1);
			ans = pos[py];
			pos[++cnt] = pos[py];
			Change(py,1,N,1,0);
			Change(cnt,1,N,1,1);
			printf("%d\n",ans);
		}
	}
}
bool flag1;
int main()
{
	freopen("phalanx.in","r",stdin); freopen("phalanx.out","w",stdout);
	n = read(), m = read(), q = read();
	if (n <= 3000 && m <= 3000)
	{
		WORK1::work();
	}
	else
	{
		flag1 = 1;
		for (int i=1;i<=q;++i)
		{
			Q[i].x = read(), Q[i].y = read();
			if (Q[i].x != 1) flag1 = 0;
		}
		if (n == 1 || flag1)
		{
			WORK2::work();
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
} 

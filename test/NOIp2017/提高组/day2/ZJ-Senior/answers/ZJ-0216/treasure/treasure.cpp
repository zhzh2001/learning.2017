#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <set>
using namespace std;


int read()
{
	int xx = 0; bool ff = 0; char ch;
	for (ch=getchar();ch<'0' || ch>'9';ch=getchar()) if (ch == '-') ff = 1;
	for (;ch>='0' && ch<='9';ch=getchar()) xx = xx*10+ch-'0';
	if (ff) xx = -xx;
	return xx; 
}

const int INF = 0x3f3f3f3f;
const int N = 15;

int n,m,dis[N][N],deep[N],mindis,maxmindis;
bool flagmindis;
bool visited[N];
int ans;

bool Nolonger(int summ,int anss)
{
	int tmpp = 0;
	for (int i=1;i<=n;++i)
	if (!visited[i])
	{
		tmpp += dis[i][0];
	}
	if (summ+tmpp >= ans) return 1;
	return 0;
}

void DFS(int k,int sum)	//已经访问k个点 当前总价值sum 
{
	if (k == n) { ans = min(ans, sum); return; }
	if ((n-k)*mindis + sum >= ans) return;
	if (flagmindis)
		if (Nolonger(sum,ans)) return;
	for (int i=1;i<=n;++i)
	if (!visited[i])
	{
		int mn = INF, tmp = INF;
		int ne = 0, deepne = 0;
		for (int j=1;j<=n;++j)
		if (visited[j] && dis[i][j])
		{
			tmp = deep[j] * dis[i][j];
			if (tmp < mn) { mn = tmp, ne = j, deepne = deep[j]; }
			else if (tmp == mn && deep[j] < deepne) ne = j, deepne = deep[j];
		}
		if (sum+mn >= ans) continue;
		visited[i] = 1;
		deep[i] = deep[ne]+1;
		DFS(k+1,sum+mn);
		visited[i] = 0;
	}
	
}

int main()
{
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	n = read(), m = read();
	int xx,yy,zz;
	memset(dis,0x3f,sizeof(dis));
	mindis=INF;
	for (int i=1;i<=m;++i)
	{
		xx = read(), yy = read(), zz = read();
		dis[xx][yy] = min(dis[xx][yy],zz);
		dis[yy][xx] = dis[xx][yy];
		mindis = min(mindis,zz);
	}
	for (int i=1;i<=n;++i) dis[i][i] = 0;
	maxmindis = 0;
	for (int i=1;i<=n;++i)
	{
		for (int j=1;j<=n;++j)
			if (dis[i][j]) dis[i][0] = min(dis[i][0],dis[i][j]);
		maxmindis = max(maxmindis,dis[i][0]);
	}
	if (maxmindis - mindis > 100) flagmindis = 1; else flagmindis = 0;
	ans = INF;
	for (int i=1;i<=n;++i)
	{
		memset(deep,0,sizeof(deep));
		memset(visited,0,sizeof(visited));
		visited[i] = 1;
		deep[i] = 1;
		DFS(1,0);
	}
	printf("%d\n",ans);
	fclose(stdin); fclose(stdout);
	return 0;
} 

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <map>
#include <queue>
#include <vector>
#include <algorithm>
#include <set>
using namespace std;

typedef long long LL;
const int NN = 1050;

LL read()
{
	LL xx = 0; bool ff = 0; char ch;
	for (ch=getchar();ch<'0' || ch>'9';ch=getchar()) if (ch == '-') ff = 1;
	for (;ch>='0' && ch<='9';ch=getchar()) xx = xx*10ll+ch-'0';
	if (ff) xx = -xx;
	return xx; 
}
const int S = 1001,T = 1002;
int _,n,tot;
LL h,r;
struct E
{
	int ne,v;
}e[NN*NN*2];
int head[NN], que[NN];
bool vis[NN];
struct REC
{
	LL x,y,z;
}a[NN];


void Build(int xx,int yy)
{
	e[++tot].ne = head[xx], head[xx] = tot, e[tot].v = yy;
	e[++tot].ne = head[yy], head[yy] = tot, e[tot].v = xx;
}

bool BFS()
{
	int h = 0, t = 1;
	que[t] = S;
	memset(vis,0,sizeof(vis));
	vis[S] = 0;
	while (h < t)
	{
		int now = que[++h];
		for (int i=head[now];i;i=e[i].ne)
		if (!vis[e[i].v])
		{
			if (e[i].v == T)
			{
				return 1;
			}
			else
			{
				vis[e[i].v] = 1;
				que[++t] = e[i].v;
			}
		}
	}
	return 0;
}

void Clr()
{
	tot = 0;
	memset(head,0,sizeof(head));
	memset(e,0,sizeof(e));
}

LL sqr(LL xx)
{
	return xx*xx;
}

bool Linked(int xx,int yy)
{
	LL dis = sqr(a[xx].x-a[yy].x) + sqr(a[xx].y-a[yy].y) + sqr(a[xx].z-a[yy].z);
	if (dis <= 4*r*r) return 1; else return 0;
}

int main()
{
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	_ = read();
	while (_--)
	{
		Clr();
		n = read(), h = read(), r = read();
		for (int i=1;i<=n;++i)
		{
			a[i].x = read(), a[i].y = read(), a[i].z = read();
		}
		for (int i=1;i<=n;++i)
		{
			for (int j=i+1;j<=n;++j)
			{
				if (Linked(i,j)) Build(i,j);
			}
		}
		for (int i=1;i<=n;++i)
		{
			if (a[i].z <= r) Build(S,i);
			if (a[i].z >= h-r) Build(i,T);
		}
		if (BFS())
		{
			puts("Yes");
		}
		else
		{
			puts("No");
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
} 

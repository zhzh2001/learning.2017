#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
typedef long long LL;
const LL N=1005;
LL t,n,h,r,q[N],in[N],hea,tai;
bool flag;
struct hole{LL x,y,z;}a[N];
bool cmp(hole p,hole q){
  return p.z<q.z;
}
bool pd(LL x,LL y){
  return (a[x].x-a[y].x)*(a[x].x-a[y].x)+(a[x].y-a[y].y)*(a[x].y-a[y].y)+(a[x].z-a[y].z)*(a[x].z-a[y].z)<=4*r*r;
}
int main(){
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  scanf("%lld",&t);
  while (t--){
    scanf("%lld%lld%lld",&n,&h,&r);
    for (LL i=1;i<=n;i++)
      scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
    sort(a+1,a+n+1,cmp);
    memset(q,0,sizeof(q));memset(in,0,sizeof(in));
    hea=0;tai=0;
    q[++tai]=0;in[tai]=0;flag=false;
    while (hea<tai){
      hea++;
      for (LL i=q[hea]+1;i<=n;i++)
      if (hea!=1){
        if (a[i].z-a[q[hea]].z>2*r)
          break;
        else
          {
           if (pd(i,q[hea])&&!in[i]){
           	 q[++tai]=i;
           	 in[i]=1;
           	 if (a[i].z+r>=h)
           	   {
           	   	flag=true;
           	   	break;
			   }
		   }
		  }
	    }
	   else
	     {
	      if (a[i].z>r)
          break;
        else
          {
           	 q[++tai]=i;
           	 in[i]=1;
           	 if (a[i].z+r>=h)
           	   {
           	   	flag=true;
           	   	break;
			   }
		 }
	    }
    if (flag) break; 	
  }
  if (flag) puts("Yes");
  else puts("No");
}
  fclose(stdin);
  fclose(stdout);
  return 0;
}

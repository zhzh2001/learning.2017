#include <cstdio>
#include <cstring>
using namespace std;
const int M=1005,N=17;
int n,m,u,v,w,ans,cnt,head[N],f[N][N],temp,fa[N];
struct edge{int to,next,len;}e[M*2];
void add(int u,int v,int w){
  e[++cnt].to=v;e[cnt].len=w;e[cnt].next=head[u];head[u]=cnt;
  e[++cnt].to=u;e[cnt].len=w;e[cnt].next=head[v];head[v]=cnt;
}
void dfs(int u,int deep,int start){
  for (int i=head[u];i;i=e[i].next){
    int v=e[i].to;
    if (v!=fa[u]){
      fa[v]=u;
      if (e[i].len*(deep+1)<f[start][v])
        f[start][v]=e[i].len*(deep+1);
      dfs(v,deep+1,start);
	}
	else return;
  }
}
int main(){
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  scanf("%d%d",&n,&m);
  for (int i=1;i<=m;i++)
    scanf("%d%d%d",&u,&v,&w),add(u,v,w);
  for (int i=1;i<=n;i++)
    for (int j=1;j<=n;j++)
      f[i][j]=0x7ffffff;
  ans=0x7ffffff;
  for (int i=1;i<=n;i++){
  	f[i][i]=0;memset(fa,0,sizeof(fa));
  	dfs(i,0,i);
  	temp=0;
  	for (int j=1;j<=n;j++)
  	  temp=temp+f[i][j];
  	if (temp<ans) ans=temp;
  }
  printf("%d\n",ans);
  fclose(stdin);
  fclose(stdout);
  return 0;
}

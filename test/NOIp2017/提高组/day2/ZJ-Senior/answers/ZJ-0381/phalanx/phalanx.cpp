#include <cstdio>
using namespace std;
typedef long long LL;
const LL N=1005,M=100005;
LL n,m,q,x,y,f[N][N],temp,p[M],la;
int main(){
  freopen("phalanx.in","r",stdin);
  freopen("phalanx.out","w",stdout);
  scanf("%lld%lld%lld",&n,&m,&q);
  if (n<=1000&&m<=1000){
  for (LL i=1;i<=n;i++)
    for (LL j=1;j<=m;j++)
      f[i][j]=(i-1)*m+j;
  for (LL i=1;i<=q;i++)
    {
     scanf("%lld%lld",&x,&y);
     temp=f[x][y];
     printf("%lld\n",f[x][y]);
     for (LL j=y;j<m;j++)
       f[x][j]=f[x][j+1];
     for (LL j=x;j<n;j++)
       f[j][m]=f[j+1][m];
     f[n][m]=temp;
	}
  }
  else if (n==1){
     for (LL i=1;i<=m;i++) p[i]=i;
     for (LL i=1;i<=q;i++){
     	scanf("%lld%lld",&x,&y);
       	printf("%lld\n",p[y]);
     	temp=p[y];
		for (LL j=y;j<m;j++)
     	  p[j]=p[j+1];
     	p[m]=temp;
	 }
  }
  fclose(stdin);
  fclose(stdout);
  return 0;
}

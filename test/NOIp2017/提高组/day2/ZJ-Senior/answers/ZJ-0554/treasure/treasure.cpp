#include<cstdio>
#include<cmath>
#include<queue>
#include<algorithm>
#include<cstring>
#define ll long long
#define M 1005
using namespace std;
int dp[1<<13][14],f[1<<13];
struct ed{int x,l,nx;}e[M<<1];
int nx[13],ecnt,tmp,n,res;
inline void Min(int &x,int y){if (x>y)x=y;}
inline void add(int x,int y,int l){
	e[ecnt]=(ed){y,l,nx[x]};
	nx[x]=ecnt++;
}
void solve(){
	memset(dp,63,sizeof(dp));
	int i,j,k,l,o;
	for (i=0; i<n; i++)dp[1<<i][1]=0;
	Min(res,dp[tmp-1][1]);
	for (k=1; k<=n; k++){
		for (i=0; i<tmp; i++)if(dp[i][k]<res){
			memset(f,63,1ll*sizeof(int)*tmp);
			f[i]=dp[i][k];
			for (j=0; j<n; j++)if (i&(1<<j)){
				for (l=1<<j; l<tmp; l++)if ((1<<j)&l)if (f[l]<res)
					for (o=nx[j];o;o=e[o].nx){if (!((1<<e[o].x)&l))Min(f[l|(1<<e[o].x)],f[l]+k*e[o].l); 
				}		
			}
			for (j=i+1;j<tmp;j++)Min(dp[j][k+1],f[j]);
		}
		Min(res,dp[tmp-1][k+1]);
	}	
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int m,i,x,y,v; 
	scanf("%d%d",&n,&m); tmp=1<<n; ecnt=1; res=2e9;
	for (i=1; i<=m; i++){
		scanf("%d%d%d",&x,&y,&v); x--; y--;
		add(x,y,v); add(y,x,v);
	}
	solve();
	printf("%d\n",res);
	return 0;
}

#include<cstdio>
#include<cmath>
#include<queue>
#include<algorithm>
#include<cstring>
#define ll long long
#define M 1005
using namespace std;
int r,x[M],h,y[M],z[M],n,q[M],L,R;
ll tmp;
bool mark[M];
ll di(int a,int b){
	return 1ll*(x[a]-x[b])*(x[a]-x[b])+1ll*(y[a]-y[b])*(y[a]-y[b])+1ll*(z[a]-z[b])*(z[a]-z[b]);	
}
bool chk(){
	int now,i;
	for (;L<R;){
		now=q[++L];
		if (h-z[now]<=r)return 1;
		for (i=1; i<=n; i++)if (!mark[i]){
			if (tmp>=di(i,now)){mark[i]=1; q[++R]=i;}
		}			
	}
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int Ca,i;
	scanf("%d",&Ca);
	for (;Ca; Ca--){
		memset(mark,0,sizeof(mark));
		L=R=0;
		scanf("%d%d%d\n",&n,&h,&r);
		tmp=4ll*r*r;
		for (i=1; i<=n; i++){
//			read(x[i]); read(y[i]); read(z[i]);
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if (z[i]<=r){mark[i]=1; q[++R]=i;}			
		}
		if (chk())printf("Yes\n");else printf("No\n");
	}	
	return 0;
}

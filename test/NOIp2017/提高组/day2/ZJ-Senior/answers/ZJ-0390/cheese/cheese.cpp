#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline LL read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1;ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48;ch=getchar(); }
	return x * f;
}

const int N = 1011;
int T;
int n,tmp;
int fa[N],rk[N];
LL h,r; 
struct node{
	LL x,y,z;
}a[N];

inline LL sqr(LL x) {
	return x*x;
}

int find(int x) {
	if(fa[x]==x) return x;
	return fa[x] = find(fa[x]); 
}

inline void Union(int x,int y) {
	x = find(x);
	y = find(y);
	if(x==y) return;
	if(rk[x]<rk[y]) {
		tmp = x; x = y; y = tmp; 
	}
	if(rk[x]==rk[y]) ++rk[x];
	fa[y] = x;
}

int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout); 
	T = read(); 
	while(T--) {
		n = read(); h = read(); r = read();
		For(i, 1, n+2) fa[i] = i; 
		For(i, 1, n+2) rk[i] = 1; 
		For(i, 1, n) {
			a[i].x=read(); a[i].y = read(); a[i].z = read(); 
		}
		For(i, 1, n) {
			if( a[i].z-r<=0ll ) Union(n+1,i);
			if( a[i].z+r>=h ) Union(n+2,i);
		}
		For(i, 1, n-1) 
		  For(j, i+1, n) 
		  	if( sqr(a[i].x-a[j].x) + sqr(a[i].y-a[j].y) + sqr(a[i].z-a[j].z) <= 4ll*r*r )
		  		Union(i, j);
		int x = find(n+1);
		int y = find(n+2);
		if(x==y) puts("Yes");
		else puts("No");
	}
	return 0;
}







#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1;ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48;ch=getchar(); }
	return x * f;
}

const int N = 15,inf = 1e9;
int n,m; 
int flag,tt;
int e[N][N],f[N][N];

inline void work_1() {
	For(i, 1, n)
	  For(j, 1, n) 
	  	if(e[i][j]!=inf) f[i][j] = 1;
	  	else f[i][j] = 1000; 
	For(i, 1, n) f[i][i] = 0; 
	For(k, 1, n) 
	  For(i, 1, n)   
	    For(j, 1, n) 
	    	f[i][j] = min(f[i][j],f[i][k]+f[k][j]); 
	int ans = inf; 
	For(i, 1, n) {
		int sum = 0; 
		For(j, 1, n) 
			sum+=tt * f[i][j]; 
		if( sum < ans ) ans = sum; 
	}
	printf("%d\n",ans); 
}

int main() {
	freopen("treasure.in","r",stdin); 
	freopen("treasure.out","w",stdout); 
	n = read(); m = read(); 
	For(i, 1, n) 
	  For(j, 1, n) 
	  	e[i][j] = inf;
	For(i, 1, m) {
			int x = read(), y=read(), v=read(); 
			if(i==1)  tt=v;
			else if(tt!=v) flag = 1;
			e[x][y] = min(e[x][y],v);
			e[y][x] = e[x][y]; 
		}
	if(!flag) work_1();
	return 0; 
}




/*

4 5
1 2 1
1 3 1
1 4 1
2 3 1
3 4 1




*/



#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define LL long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=j;i>=k;i--)
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1;ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48;ch=getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) x = -x, putchar('-'); 
	if(x>9) write(x/10); 
	putchar(x%10+48); 
}
void writeln(int x) {
	write(x);puts("");
}
const int N = 300011; 
int n,m,Q,tot,now;
int pp[N];  
bool flag;
int tree[N*2],id[N*2]; 

struct qry{
	int x,y; 
}q[N];



int a[1011][1011]; 
inline void work_1() {
	
	For(i, 1, n) 
	  For(j, 1, m) a[i][j] = (i-1)*m+j;  
	For(T, 1, Q) {
		int tmp = a[q[T].x][q[T].y]; 
		printf("%d\n",tmp); 
		int x = q[T].x; 
		int y = q[T].y; 
		For(j, y, m-1) a[x][j] = a[x][j+1]; 
		For(i, x, n-1) a[i][m] = a[i+1][m]; 
		a[n][m] = tmp; 
		
	}
	
	exit(0); 
}

inline void add(int x) {
	for( ; x<=tot; x += x&(-x)) 
		tree[x]+=1; 
}

inline int query(int x) {
	int res = 0; 
	for( ; x; x -= x&(-x)) 
		res += tree[x]; 
	return res; 
}

inline void work_2() {
	tot = m+Q; now = m;  
	For(i, 1, m) id[i] = i; 
	For(i, 1, Q) {
		int lst = 0, pos = q[i].y, len=1, tmp; 
		while(len) {
			tmp = query(pos); 
			len = tmp - lst; 
			lst = tmp; 
			pos+=len;
		} 
		id[++now] = id[pos]; 
		add(pos); 
		writeln(id[pos]); 
	}
	
	exit(0); 
}

inline void work_3() {
	tot = m+Q; now = m;  
	For(i, 1, m) id[i] = i; 
	For(i, 1, n) pp[i] = i*m; 
	int l = 1, r = n; 
	
	For(i, 1, Q) {
		int lst = 0, pos = q[i].y, len=1, tmp; 
		while(len) {
			tmp = query(pos); 
			len = tmp - lst; 
			lst = tmp; 
			pos+=len;
		} 
		id[++now] = id[pos]; 
		add(pos); 
		writeln(id[pos]); 
		
		
		tmp = id[now]; 
		l = l%n+1; r = r%n+1; 
		id[now] = pp[ l ]; 
		pp[ r ] = tmp; 
	}
	exit(0); 
}

int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout); 
	n = read(); m = read(); Q = read(); 
	For(i, 1, Q) {
		q[i].x=read(); q[i].y=read(); 
		if(q[i].x!=1) flag = 1;
	}
	
	if(n<=1000 && m<=1000) work_1();           
	
	if(!flag && n==1 ) work_2();
	
	work_3();  
	
	return 0; 
}





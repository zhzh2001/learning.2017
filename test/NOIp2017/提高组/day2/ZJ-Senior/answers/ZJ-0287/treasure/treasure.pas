var
 n,m,i,j,h,t,x,y,z,s,ans:longint;
 p:array[-2..15,-2..15]of int64;
 f,d,g:array[-2..15]of int64;
begin
assign(input,'treasure.in');reset(input);
assign(output,'treasure.out');rewrite(output);
 for i:=1 to 12 do
  for j:=1 to 12 do
   p[i,j]:=500001;
 read(n,m);
 for i:=1 to m do
 begin
  read(x,y,z);
  if z<p[x,y] then begin p[x,y]:=z;p[y,x]:=z;end;
 end;
 ans:=2147483647;
 for i:=1 to n do
 begin
  fillchar(f,sizeof(f),0);
  h:=0;
  t:=1;
  d[1]:=i;
  g[1]:=0;
  f[1]:=1;
  while h<t do
  begin
   h:=h+1;
   for j:=1 to n do
    if(f[j]=0)and(p[d[h],j]<=500000)then
    begin
     t:=t+1;
     d[t]:=j;
     f[j]:=1;
     g[t]:=g[h]+1;
     s:=s+g[t]*p[d[h],j]
    end;
  end;
  if s<ans then ans:=s;
 end;
 write(ans);
close(input);
close(output);
end.
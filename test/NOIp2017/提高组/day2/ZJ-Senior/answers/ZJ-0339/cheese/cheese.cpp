#include <bits/stdc++.h>
#define ll long long
using namespace std;
const int maxn = 1010;
struct node
{
	ll x,y,z;
}a[maxn];
ll n,h,r,fa[maxn];
ll getf(ll p)
{
	if (p == fa[p]) return p; else return getf(fa[p]);
}
void un(ll p,ll q)
{
	ll P = getf(p), Q = getf(q);
	fa[Q] = fa[P];
	fa[q] = fa[P];fa[p] = fa[P];
}
void di(ll num)
{
	if (a[num].z <=r) un(0,num);
	if (a[num].z >= h - r) un(n + 1,num);
}
bool jiao(ll p,ll q)
{
	ll dis = (a[p].x - a[q].x)*( a[p].x - a[q].x) + (a[p].y - a[q].y) * (a[p].y - a[q].y) + (a[p].z - a[q].z) * (a[p].z - a[q].z);
	if (r * r * 4 >= dis) return 1; else return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	ll T;
	scanf("%lld",&T);
	while (T--)
	{
		scanf("%lld%lld%lld",&n,&h,&r);
		for (ll i = 0; i <= n + 1; ++i) fa[i] = i;
		for (ll i = 1; i <= n; ++i) scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		for (ll i = 1; i <= n; ++i) di(i);
		for (ll i = 1; i <= n; ++i)
		for (ll j = i + 1; j <= n; ++j)
		{
			if (jiao(i,j)) un(i,j);
		}
		if (getf(0) == getf(n + 1)) puts("Yes"); else puts("No");
	//	if (!ok) puts("No");
		
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
const int maxn = 14;
const int INF = 0x3f3f3f3f;
int n,m;
int dis[15][15];
int f[1 << maxn][maxn],dist[maxn];
vector<int>G[maxn],w[maxn];
inline void solve(int sta,int be)
{
	for (int i = 0; i < n; ++i)
	if (sta & (1<<i))
	{
		for (int j = 0; j < (int)G[i].size(); ++j)
		{
			if (!(sta & (1<<G[i][j]))) 
			{
				int sta1 = sta | (1<<G[i][j]);
				int dq = f[sta][be] + (dist[i] + 1) * w[i][j];
				if (dq < f[sta1][be])
				{
					f[sta1][be] = dq;
					dist[G[i][j]] = dist[i] + 1;
					solve(sta1,be);
					dist[G[i][j]] = INF;
				}
			}
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(dis,63,sizeof(dis));
	memset(f,63,sizeof(f));
	memset(dist,63,sizeof(dist));
	scanf("%d%d",&n,&m);
	for (int i = 0; i < n; ++i) f[1<<i][i] = 0;
	for (int i = 1; i <= m; ++i)
	{
		int p,q,r;
		scanf("%d%d%d",&p,&q,&r); p--; q--;
		if (dis[p][q] > r)
		dis[p][q] = dis[q][p] = r;
	}
	for (int i = 0; i < n; ++i)
	for (int j = 0; j < n; ++j)
	if (i != j && dis[i][j] != INF) G[i].push_back(j),w[i].push_back(dis[i][j]);	
	for (int i = 0; i < n; ++i)
	{
		memset(dist,63,sizeof(dist));
		dist[i] = 0;
		solve(1<<i,i);
	}
	int ans = INF;
	for (int i = 0; i < n; ++i) ans = min(ans,f[(1<<n) - 1][i]);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

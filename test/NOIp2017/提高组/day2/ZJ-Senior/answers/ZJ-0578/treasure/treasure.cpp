#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 15

int n,m,u,v,len,i,ans;
int a[N][N],deep[N],vis[N],b[N];

void dfs(int t,int now){
	if(now>=ans)return;
	if(t>n){ans=now;return;}
	for(int i=1;i<=n;i++)
	if(!vis[i]){vis[i]=1;b[t]=i;
		for(int j=1;j<t;j++)
		if(a[i][b[j]]<1e9){
			deep[i]=deep[b[j]]+1;
			dfs(t+1,now+deep[b[j]]*a[i][b[j]]);
		}vis[i]=0;b[t]=0;
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,60,sizeof(a));
	for(i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&len);
		a[u][v]=a[v][u]=min(a[u][v],len);
	}ans=1e9;
	for(i=1;i<=n;i++)
		deep[i]=1,vis[i]=1,b[1]=i,dfs(2,0),vis[i]=0;
	printf("%d\n",ans);
	return 0;
}

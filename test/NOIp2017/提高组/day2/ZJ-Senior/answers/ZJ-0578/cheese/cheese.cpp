#include<stdio.h>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define N 1005
#define ll long long

int T,n,h,R,i,l,r;
int x[N],y[N],z[N],vis[N],q[N];

ll dis(int x,int y,int z){return 1ll*x*x+1ll*y*y+1ll*z*z;}
bool solve(){
	while(l<=r){//printf("%d %d\n",l,r);
		for(int i=1;i<=n;i++)
			if(!vis[i]&&dis(x[q[l]]-x[i],y[q[l]]-y[i],z[q[l]]-z[i])<=4ll*R*R){
				if(z[i]+R>=h)return 1;
				q[++r]=i;vis[i]=1;
			}
		l++;
	}return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&R);
		for(i=1;i<=n;i++)scanf("%d%d%d",&x[i],&y[i],&z[i]);
		l=1,r=0;memset(vis,0,sizeof(vis));
		for(i=1;i<=n;i++)
			if(z[i]<=R)vis[i]=1,q[++r]=i;
		if(solve())puts("Yes");
			else puts("No");
	}return 0;
}

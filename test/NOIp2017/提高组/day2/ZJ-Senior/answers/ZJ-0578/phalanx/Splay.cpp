#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 300005
int n,m,q,x,y,i,l,r,ans,rt;
int size[N],sl[N],sr[N],fa[N];

void pushup(int x){size[x]=size[sl[x]]+size[sr[x]]+1;}
void Rotate(int x){
	pushup(fa[x]);
	if(sl[fa[x]]==x)
		fa[sr[x]]=fa[x],sl[fa[x]]=sr[x],sr[x]=fa[x];
		else fa[sl[x]]=fa[x],sr[fa[x]]=sl[x],sl[x]=fa[x];
	pushup(fa[x]);
	int f=fa[fa[x]];
	fa[fa[x]]=x;
	if(sl[f]==fa[x])sl[f]=x;
		else if(sr[f]==fa[x])sr[f]=x;
	if(f)pushup(f);
	fa[x]=f,pushup(x);
}
void Splay(int x,int y){
	while(fa[x]!=y)
		if(fa[fa[x]]!=y&&((sl[fa[fa[x]]]==fa[x]&&sl[fa[x]]==x)||(sr[fa[fa[x]]]==fa[x]&&sr[fa[x]]==x)))
			Rotate(fa[x]),Rotate(x);
			else Rotate(x);
	if(!y)rt=x;
}
int find(int x,int k){
	//printf("%d %d\n",x,k);
	if(size[sl[x]]+1==k)return x;
	if(size[sl[x]]>=k)return find(sl[x],k);
	return find(sr[x],k-size[sl[x]]-1);
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("Splay.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(i=1;i<=m+2;i++){
		size[i]=m+2-i+1;
		if(i<=m+1)sr[i]=i+1;
		if(i>1)fa[i]=i-1;
	}rt=1,Splay(m+2,0);
	for(i=1;i<=q;i++){
		scanf("%d%d",&x,&y);
		ans=find(rt,y+1);
		l=find(rt,y),r=find(rt,y+2);
		Splay(l,0),Splay(r,rt);
		sl[sr[rt]]=0;
		l=find(rt,m),r=find(rt,m+1);
		Splay(l,0),Splay(r,rt);
		fa[ans]=r,sl[r]=ans;
		size[ans]=1;
		pushup(r),pushup(l);
		printf("%d\n",ans-1);
	}return 0;
}

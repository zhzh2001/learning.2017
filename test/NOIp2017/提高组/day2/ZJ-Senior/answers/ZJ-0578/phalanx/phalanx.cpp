#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 50005
#define Maxn 300005
#define Q 505
#define ll long long

int n,m,q,x,y,i,j,l,r,num,rt;
ll a[N],f[Q][N],ans;
int size[Maxn],sl[Maxn],sr[Maxn],fa[Maxn],pos[N];

void build(int x){
	pos[x]=++num;
	for(int i=1;i<m;i++)
		f[num][i]=1ll*(x-1)*m+i;
	f[num][m]=a[x];
}
void pushup(int x){size[x]=size[sl[x]]+size[sr[x]]+1;}
void Rotate(int x){
	pushup(fa[x]);
	if(sl[fa[x]]==x)
		fa[sr[x]]=fa[x],sl[fa[x]]=sr[x],sr[x]=fa[x];
		else fa[sl[x]]=fa[x],sr[fa[x]]=sl[x],sl[x]=fa[x];
	pushup(fa[x]);
	int f=fa[fa[x]];
	fa[fa[x]]=x;
	if(sl[f]==fa[x])sl[f]=x;
		else if(sr[f]==fa[x])sr[f]=x;
	if(f)pushup(f);
	fa[x]=f,pushup(x);
}
void Splay(int x,int y){
	while(fa[x]!=y)
		if(fa[fa[x]]!=y&&((sl[fa[fa[x]]]==fa[x]&&sl[fa[x]]==x)||(sr[fa[fa[x]]]==fa[x]&&sr[fa[x]]==x)))
			Rotate(fa[x]),Rotate(x);
			else Rotate(x);
	if(!y)rt=x;
}
int find(int x,int k){
	if(size[sl[x]]+1==k)return x;
	if(size[sl[x]]>=k)return find(sl[x],k);
	return find(sr[x],k-size[sl[x]]-1);
}
void solve(){int ans;
	for(i=1;i<=m+2;i++){
		size[i]=m+2-i+1;
		if(i<=m+1)sr[i]=i+1;
		if(i>1)fa[i]=i-1;
	}rt=1,Splay(m+2,0);
	for(i=1;i<=q;i++){
		scanf("%d%d",&x,&y);
		ans=find(rt,y+1);
		l=find(rt,y),r=find(rt,y+2);
		Splay(l,0),Splay(r,rt);
		sl[sr[rt]]=0;
		l=find(rt,m),r=find(rt,m+1);
		Splay(l,0),Splay(r,rt);
		fa[ans]=r,sl[r]=ans;
		size[ans]=1;
		pushup(r),pushup(l);
		printf("%d\n",ans-1);
	}return;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n==1){solve();return 0;}
	for(i=1;i<=n;i++)a[i]=1ll*i*m;
	for(i=1;i<=q;i++){
		scanf("%d%d",&x,&y);
		if(!pos[x])build(x);
		ans=f[pos[x]][y];
		for(j=y;j<m;j++)f[pos[x]][j]=f[pos[x]][j+1];
		for(j=x;j<n;j++){
			a[j]=a[j+1];
			if(pos[j])f[pos[j]][m]=a[j];
		}a[n]=ans;
		if(pos[n])f[pos[n]][m]=a[n];
		printf("%lld\n",ans);
	}return 0;
}

#include <cstdio>
#define MAXn 1000
int T,n,h,r,i,j,cnt,idx[MAXn + 2],temp;
#include <cstring>
struct _
{
	int x,y,z;
}P[MAXn + 1];
#include <utility>
std::pair <int,int> edge[(MAXn << 1) + (MAXn * (MAXn - 1) >> 1)];
inline void AddEdge(int u,int v)
{
	edge[++ cnt] = std::make_pair(v,idx[u]),idx[u] = cnt;
}
#define R register
#define SD(x) (temp = P1.x - P2.x,temp * temp)
#include <cmath>
inline int dist(R const _ & P1,R const _ & P2)
{
	R long long temp,sum = 0;
	sum = SD(x),sum += SD(y),sum += SD(z);
	return std::sqrt(sum);
}
#include <queue>
bool vis[MAXn + 2];
int main()
{
	std::freopen("cheese.in","r",stdin),std::scanf("%d",& T),std::freopen("cheese.out","w",stdout);
	while (T --)
	{
		for (std::scanf("%d%d%d",& n,& h,& r),cnt = 0,std::memset(idx,0,n + 2 << 2),i = 1;i <= n;++ i)
		{
			std::scanf("%d%d%d",& P[i].x,& P[i].y,& P[i].z);
			if (P[i].z <= r)
				AddEdge(0,i);
			if (P[i].z + r >= h)
				AddEdge(i,n + 1);
		}
		for (i = 2;i <= n;++ i)
			for (j = 1;j < i;++ j)
				if (dist(P[i],P[j]) <= r << 1)
					AddEdge(i,j),AddEdge(j,i);
		std::queue <int> q;
		q.push(0),std::memset(vis,0,n + 2);
		while (! q.empty())
		{
			for (i = idx[q.front()];i;i = edge[i].second)
				if (! vis[temp = edge[i].first])
				{
					if (temp == n + 1)
						break;
					q.push(temp),vis[temp] = 1;
				}
			if (i)
				break;
			q.pop();
		}
		std::puts(! q.empty() ? "Yes" : "No");
	}
}

#include <cstdio>
#define MAXn 12
int n,m,_,__,v,___[MAXn + 1][MAXn + 1],i,j,idx[MAXn + 1],n_,Ans;
#include <cstring>
struct _____
{
	int to,wt,nxt;
}edge[(MAXn * (MAXn - 1) >> 1) + 1];
inline void AddEdge(int u,int v,int w)
{
	static int cnt;
	_____ & _ = edge[++ cnt];
	_.to = v,_.wt = w,_.nxt = idx[u],idx[u] = cnt;
}
namespace ____
{
	inline int min(int a,int b)
	{
		return a < b ? a : b; 
	}
}
bool vis[MAXn + 1];
void dfs(int cnt,int now,int dep,int ans)
{
	if (cnt < n_)
		for (register int i = idx[now];i;i = edge[i].nxt)
		{
			int & temp = edge[i].to;
			if (! vis[temp])
				vis[temp] = 1,dfs(cnt + 1,temp,dep + 1,ans + dep * edge[i].wt),vis[temp] = 0;
		}
	else
		Ans = ____::min(Ans,ans);
}
int main()
{
	std::freopen("treasure.in","r",stdin),std::scanf("%d%d",& n,& m),std::memset(___,0x3f,(n + 1) * (MAXn + 1) << 2);
	while (m --)
		std::scanf("%d%d%d",& _,& __,& v),___[_][__] = ____::min(___[_][__],v);
	for (i = 2;i <= n;++ i)
		for (j = 1;j < i;++ j)
			if ((_ = ____::min(___[i][j],___[j][i])) < 0x3f3f3f3f)
				AddEdge(i,j,_),AddEdge(j,i,_);
	for (Ans = 0x3f3f3f3f,n_ = n - 1,i = 1;i <= n;++ i)
		vis[i] = 1,dfs(0,i,1,0),vis[i] = 0;
	std::freopen("treasure.out","w",stdout),std::printf("%d\n",Ans);
}

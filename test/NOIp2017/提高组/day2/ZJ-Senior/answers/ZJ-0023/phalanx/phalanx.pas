type
  node=record
    son:array[0..1]of longint;
    p:longint;
    l,r,size:int64;
  end;
var
  tree:array[-1..2000005]of node;
  root:array[0..300005]of longint;
  a:array[1..1005,1..1005]of int64;
  i,j,n,m,q,k,x,y,toptree:longint;  key:int64;
  procedure updata(x:longint);
  begin
    tree[x].size:=(tree[tree[x].son[0]].size
      +tree[tree[x].son[1]].size+tree[x].r-tree[x].l+1);
  end;
  procedure splay(x:longint;var o:longint);
  var y,z:longint; boo:boolean;
    procedure rotate(x:longint);
    var y,z,k:longint;
    begin
      y:=tree[x].p; z:=tree[y].p; boo:=y=o;
      if z<>-1 then tree[z].son[ord(tree[z].son[1]=y)]:=x;
      tree[x].p:=z; k:=ord(tree[y].son[1]=x);
      if tree[x].son[k xor 1]<>-1 then tree[tree[x].son[k xor 1]].p:=y;
      tree[y].son[k]:=tree[x].son[k xor 1]; tree[x].son[k xor 1]:=y;
      tree[y].p:=x; updata(y);
      if boo then o:=x;
    end;
  begin
    while x<>o do
    begin
      y:=tree[x].p; z:=tree[y].p;
      if y<>o then
      begin
        if (tree[z].son[0]=y)xor(tree[y].son[0]=x) then rotate(x)
        else rotate(y);
      end;
      rotate(x);
    end;
    updata(x);
  end;
  function find(num:longint;key:int64):longint;
  var x,left,right:longint;
  begin
    if key=0 then exit(-1);
    x:=root[num];
    while true do
    begin
      left:=tree[tree[x].son[0]].size+1;
      right:=tree[tree[x].son[0]].size+(tree[x].r-tree[x].l+1);
      if (key>=left)and(key<=right) then exit(x)
      else if key<left then x:=tree[x].son[0]
      else x:=tree[x].son[1];
    end;
  end;
  function sp(x,k:longint):longint;
  begin
    x:=tree[x].son[k];
    if x=-1 then exit(-1);
    while tree[x].son[k xor 1]<>-1 do x:=tree[x].son[k xor 1];
    exit(x);
  end;
  procedure newnode(pre:longint;l,r:int64);
  begin
    inc(toptree);
    tree[toptree].l:=l; tree[toptree].r:=r; tree[toptree].p:=pre;
    tree[toptree].son[0]:=-1; tree[toptree].son[1]:=-1;
    tree[toptree].size:=r-l+1;
  end;
  procedure insertp(num,x:longint;l,r:int64);
  begin
    if x=-1 then
    begin
      newnode(-1,l,r);
      root[num]:=toptree; exit;
    end;
    if tree[x].son[1]=-1 then
    begin
      newnode(x,l,r); tree[x].son[1]:=toptree;
    end else
    begin
      x:=sp(x,1);
      newnode(x,l,r); tree[x].son[0]:=toptree;
    end;
    splay(toptree,root[num]);
  end;
  procedure deletenode(num,x:longint);
  begin
    splay(x,root[num]);
    if tree[x].son[1]=-1 then
    begin
      root[num]:=tree[x].son[0];
      tree[tree[x].son[0]].p:=-1; exit;
    end;
    splay(sp(x,1),tree[x].son[1]);
    tree[tree[x].son[1]].p:=-1; tree[tree[x].son[1]].son[0]:=tree[x].son[0];
    tree[tree[x].son[0]].p:=tree[x].son[1]; root[num]:=tree[x].son[1];
  end;
  function chai(num,x:longint;key:int64):int64;
  begin
    key:=key-tree[tree[x].son[0]].size;
    chai:=tree[x].l+key-1;
    if key>1 then
    begin
      if key<tree[x].r-tree[x].l+1 then
      begin
        if (num=0)and(tree[x].l+key=tree[x].r) then
        insertp(num,x,(tree[x].l+key)*m,tree[x].r*m)
        else insertp(num,x,tree[x].l+key,tree[x].r);
      end;
      tree[x].r:=tree[x].l+key-2;
    end else if key<tree[x].r-tree[x].l+1 then tree[x].l:=tree[x].l+key
    else deletenode(num,x);
    if (num=0)and(tree[x].l=tree[x].r) then begin tree[x].l:=tree[x].l*m; tree[x].r:=tree[x].r*m; end;
  end;
  procedure solve(x,y:longint);
  var key,key1:int64;
  begin
    if y=m then
    begin
      splay(find(0,x),root[0]);
      if tree[root[0]].l=tree[root[0]].r then key:=chai(0,root[0],x)
      else key:=chai(0,root[0],x)*m;
      writeln(key); insertp(0,find(0,n-1),key,key); exit;
    end;
    splay(find(x,y),root[x]); key:=chai(x,root[x],y);
    splay(find(0,x),root[0]);
    if tree[root[0]].l=tree[root[0]].r then key1:=chai(0,root[0],x)
    else key1:=chai(0,root[0],x)*m;
    insertp(x,find(x,m-2),key1,key1); insertp(0,find(0,n-1),key,key);
    writeln(key);
  end;
begin
  assign(input,'phalanx.in'); reset(input);
  assign(output,'phalanx.out'); rewrite(output);
  toptree:=-2; newnode(-1,1,0); toptree:=0;
  readln(n,m,q);
  if (n<=1000)and(m<=1000) then
  begin
    for i:=1 to n do
      for j:=1 to m do
        a[i,j]:=(i-1)*m+j;
    for k:=1 to q do
    begin
      readln(x,y);
      writeln(a[x,y]); key:=a[x,y];
      for j:=y to m-1 do a[x,j]:=a[x,j+1];
      for i:=x to n-1 do a[i,m]:=a[i+1,m];
      a[n,m]:=key;
    end;
  end else
  begin
  newnode(-1,1,n); root[0]:=1;
  for i:=1 to n do
  begin
    newnode(-1,(i-1)*m+1,(i-1)*m+m-1);
    root[i]:=toptree;
  end;
  for i:=1 to q do
  begin
    readln(x,y);
    solve(x,y);
  end;
  end;
  close(input); close(output);
end.





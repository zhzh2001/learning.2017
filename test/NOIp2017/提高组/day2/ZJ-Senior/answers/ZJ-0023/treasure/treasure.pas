var
  dis:array[1..12,1..12]of longint;
  q,f:array[1..12]of longint;
  i,j,k,n,m,head,tail,x,y:longint;
  function min(x,y:longint):longint;
  begin if x<y then exit(x) else exit(y); end;
  function bfs(x:longint):longint;
  begin
    bfs:=0;
    for i:=1 to n do f[i]:=0;
    head:=1; tail:=1; q[1]:=x; f[x]:=1;
    while head<=tail do
    begin
      for i:=1 to n do
        if (f[i]=0)and(dis[q[head],i]<maxlongint) then
        begin
          bfs:=bfs+f[q[head]]*dis[q[head],i];
          f[i]:=f[q[head]]+1;
          inc(tail); q[tail]:=i;
        end;
      inc(head);
    end;
  end;
begin
  assign(input,'treasure.in'); reset(input);
  assign(output,'treasure.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      dis[i,j]:=maxlongint;
  for i:=1 to m do
  begin
    readln(x,y,k);
    dis[x,y]:=min(dis[x,y],k);
    dis[y,x]:=dis[x,y];
  end;
  k:=maxlongint;
  for i:=1 to n do
    k:=min(k,bfs(i));
  write(k);
  close(input); close(output);
end.

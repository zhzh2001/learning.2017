var
  dis:array[0..1005,0..1005]of boolean;
  b:array[0..1010]of boolean;
  q:array[1..1010]of longint;
  x,y,z:array[1..1005]of int64;
  i,j,n,t,head,tail:longint;      r,h:int64;
  t1,t2,t3,t4:qword;
  procedure main;
  var i,j:longint;
  begin
    readln(n,h,r);
    for i:=1 to n do readln(x[i],y[i],z[i]);
    for i:=0 to n+1 do b[i]:=false;
    for i:=1 to n do
      for j:=i to n do
      begin
        t1:=sqr(x[i]-x[j]); t2:=sqr(y[i]-y[j]);
        t3:=sqr(z[i]-z[j]); t4:=sqr(r); t4:=t4 shl 2;
        dis[i,j]:=(t1+t2+t3)<=t4;
        dis[j,i]:=dis[i,j];
      end;
    for i:=1 to n do
    begin
      dis[0,i]:=z[i]<=r;
      dis[i,0]:=dis[0,i];
      dis[n+1,i]:=(h-z[i])<=r;
      dis[i,n+1]:=dis[n+1,i];
    end;
    head:=1; tail:=1; q[1]:=0; b[0]:=true;
    while head<=tail do
    begin
      for i:=0 to n+1 do
        if dis[q[head],i] and (not b[i]) then
        begin
          b[i]:=true; inc(tail); q[tail]:=i;
          if i=n+1 then begin writeln('Yes'); exit; end;
        end;
      inc(head);
    end;
    writeln('No');
  end;
begin
  assign(input,'cheese.in'); reset(input);
  assign(output,'cheese.out'); rewrite(output);
  readln(t);
  for i:=1 to t do main();
  close(input); close(output);
end.


#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 15
#define M 4200
#define E 1020
#define ll long long
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
// WARNING: all the j are [1, n], i are [0, n-1]
int g[M][N]; // g[i][j] -> add j to i min const
int f[M]; // f[i] -> i's min const
int k[M][N]; // k[i][j] -> the distanse of j in i
int mp[N][N]; // The min distanse from i to j.
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int n = read(), m = read();
	memset(mp, 63, sizeof mp);
	for (int i = 1; i <= n; i++) mp[i][i] = 0;
	for (int i = 1; i <= m; i++) {
		int x = read(), y = read(), z = read();
		mp[x][y] = min(mp[x][y], z);
		mp[y][x] = min(mp[y][x], z);
	}
	int nn020701 = 1 << n, ans = 1 << 30;
	for (int st = 1; st <= n; st ++) { // mei ju zan zhu na ge dian
		memset(g, 0, sizeof g); // clean
		memset(f, 63, sizeof f);
		memset(k, 0, sizeof k);
		k[1 << st - 1][st] = 1;
		f[1 << st - 1] = 0;
		for (int i = 0; i < nn020701; i++) if (i & (1 << st - 1)) { // st is in i, else not used.
			for (int j = 1; j <= n; j++) if ((i & (1 << j - 1)) == 0) { // j is not in i
				int x = 0;
				ll mx = 1ll << 60;
				for (int l = 1; l <= n; l++) if (i & (1 << l - 1)) { // l in i
					if (mp[l][j] < 600000) { // have road
						ll s = k[i][l] * mp[l][j];
						if (s < mx) {
							mx = s;
							x = l;
						}
					}
				}
				if (!x) continue; // no edge from i to j.
				g[i][j] = f[i] + mx; // update g
				if (g[i][j] < f[i | (1 << j - 1)]) {
					f[i | (1 << j - 1)] = g[i][j];
					memcpy(k[i | (1 << j - 1)], k[i], sizeof k[i]);
					k[i | (1 << j - 1)][j] = k[i][x] + 1;
				}
			}
		}
		ans = min(ans, f[nn020701 - 1]);
	}
	printf("%d\n", ans);
	return 0;
}


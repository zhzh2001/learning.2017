#include <cstdio>
#define N 1020
#define ll long long
using namespace std;
inline ll read() {
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=0;ch=getchar();}
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll x[N], y[N], z[N];
int dist(int i, int j) {
	return (x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]) + (z[i] - z[j]) * (z[i] - z[j]);
}
int fa[N+5];
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
void merge(int x, int y) {
	x = find(x); y = find(y);
	if (x == y) return;
	fa[x] = y;
}
int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T = read();
	while (T --) {
		int n = read();
		ll h = read(), R = read();
		ll min_dis = R*R*4;
		for (int i = 1; i <= n+2; i++)
			fa[i] = i;
		for (int i = 1; i <= n; i++) {
			x[i] = read();
			y[i] = read();
			z[i] = read();
			if (z[i] <= R) merge(n+1, i);
			if (z[i] >= h-R) merge(n+2, i);
			for (int j = 1; j < i; j++)
				if (dist(i, j) <= min_dis)
					merge(i, j);
		}
		puts(find(n+1) == find(n+2) ? "Yes" : "No");
	}
	return 0;
}


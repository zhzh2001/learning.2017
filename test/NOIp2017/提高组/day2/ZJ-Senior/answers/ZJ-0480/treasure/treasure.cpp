#include<bits/stdc++.h>
#define fo(i,be,en) for(i=be;i<=en;i++)
using namespace std;
typedef long long LL;
typedef double DB;
const int maxn=17,maxm=1100;
const unsigned long long seed=1313;
int bushu[maxn],juzhen[maxn][maxn],mi_sum[maxn],bian[maxn*maxn],i,j,k,t,m,n,p,ans,x,y,z,cnt;
map<unsigned long long,int>haxi;
struct adj_list
{
	int be[maxn],ne[maxm<<1],v[maxm<<1],w[maxm<<1],edge_cnt;
	void init(){memset(be,0,sizeof(be)); edge_cnt=0;}
	void add(const int &x,const int &y,const int &z)
	{
		v[++edge_cnt]=y;
		w[edge_cnt]=z;
		ne[edge_cnt]=be[x];
		be[x]=edge_cnt;
	}
}v;
int zuixiao(const int &a,const int &b)
{
	return a>b?b:a;
}
int ha(int tot)
{
	unsigned long long temp=0;
	int i;
	fo(i,1,n)
	{
		temp=temp*seed;
		if (bushu[i]<(INT_MAX>>1)) temp=temp+bushu[i];
	}
	if (!haxi.count(temp)) haxi[temp]=tot;
	else
	{
		if (haxi[temp]<=tot) return 1;
		return 0;
	}
	return 0;
}
void ss(int now,int tot)
{
//	if (ha(tot)) return;
	if (now>n) {ans=zuixiao(ans,tot); return;}
	if (tot+mi_sum[n-now+1]>=ans) return;
	int i,j;
	if (now==1)
	{
		fo(i,1,n)
		{
			bushu[i]=1;
			ss(now+1,tot);
			bushu[i]=INT_MAX>>1;
		}
		return;
	}
	fo(i,1,n)
	if (bushu[i]<(INT_MAX>>1))
	{
		for(j=v.be[i];j;j=v.ne[j])
		if (bushu[v.v[j]]>=INT_MAX>>1)
		{
			bushu[v.v[j]]=bushu[i]+1;
			ss(now+1,tot+bushu[i]*v.w[j]);
			bushu[v.v[j]]=INT_MAX>>1;
		}
	}
}
			
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	fo(i,0,n+1)
		fo(j,0,n+1)
			juzhen[i][j]=INT_MAX>>1;
	fo(i,1,m)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (juzhen[x][y]>z) juzhen[x][y]=juzhen[y][x]=z;
	}
	cnt=0;
	fo(i,1,n)
		fo(j,i+1,n)
		if (juzhen[i][j]<(INT_MAX>>1))
		{
			bian[++cnt]=juzhen[i][j];
			v.add(i,j,juzhen[i][j]);
			v.add(j,i,juzhen[i][j]);
		}
	sort(bian+1,bian+cnt+1);
	mi_sum[0]=0;
	fo(i,1,n)
		mi_sum[i]=mi_sum[i-1]+bian[i];
	ans=INT_MAX>>1;
	fo(i,0,n+1) bushu[i]=INT_MAX>>1;
	ss(1,0);
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
#define fo(i,be,en) for(i=be;i<=en;i++)
using namespace std;
typedef long long LL;
typedef double DB;
const int maxn=1100;
const int maxm=maxn*maxn;
const DB eps=1e-8;
struct adj_list
{
	int be[maxn],ne[maxm<<1],v[maxm<<1],edge_cnt;
	void init(){memset(be,0,sizeof(be)); edge_cnt=0;}
	void add(const int &x,const int &y)
	{
		v[++edge_cnt]=y;
		ne[edge_cnt]=be[x];
		be[x]=edge_cnt;
	}
}v;
struct data
{
	DB x,y,z;
}dong[maxn];
int vis[maxn],i,j,k,t,m,n,p,x,y,z;
DB h,r;
DB dis(int a,int b)
{
	return sqrt((dong[a].x-dong[b].x)*(dong[a].x-dong[b].x)+(dong[a].y-dong[b].y)*(dong[a].y-dong[b].y)+(dong[a].z-dong[b].z)*(dong[a].z-dong[b].z));
}
int dfs(int now)
{
	int i;
	vis[now]=1;
	if (now==n+1) return 1;
	for(i=v.be[now];i;i=v.ne[i])
		if (!vis[v.v[i]]) if (dfs(v.v[i])) return 1;
	return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		v.init();
		memset(vis,0,sizeof(vis));
		scanf("%d%lf%lf",&n,&h,&r);
		fo(i,1,n) scanf("%lf%lf%lf",&dong[i].x,&dong[i].y,&dong[i].z);
		fo(i,1,n)
		{
			if (dong[i].z<=r+eps) v.add(0,i);
			if (dong[i].z>=h-r-eps) v.add(i,n+1);
			fo(j,i+1,n)
			if (dis(i,j)<=2.0*r+eps)
			{
				v.add(i,j);
				v.add(j,i);
			}
		}
		if (dfs(0)) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
const int maxn=1010;
int x[maxn],y[maxn],z[maxn],que[maxn],inq[maxn];
long long sqr(int x){
	return 1LL*x*x;
}
int abs(int x){
	return x<0?-x:x;
}
double dis(int i,int j){
	return sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]));
}
bool work(){
	int n,h,r;
	scanf("%d%d%d",&n,&h,&r);
	for (int i=1; i<=n; i++) scanf("%d%d%d",&x[i],&y[i],&z[i]);
	int L=0,R=0;
	memset(inq,0,sizeof(inq));
	for (int i=1; i<=n; i++) if (abs(z[i])<=r) que[R++]=i,inq[i]=1;
	while (L<R){
		int u=que[L++]; if (abs(h-z[u])<=r) return 1;
		for (int v=1; v<=n; v++) if (!inq[v] && dis(u,v)<=2*r) que[R++]=v,inq[v]=1;
	}
	return 0;
}
int T;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		work()?puts("Yes"):puts("No");
	}
	return 0;
}

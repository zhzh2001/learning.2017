#include<cstdio>
const int maxn=15;
const int inf=1000000000;
int g[maxn][maxn];
int f[100000][maxn],rt_lis[100000][maxn],dep[maxn],t,tt,n,m;
void find(int s,int sp,int up){
	int last=0,pre;
	for (int i=0; i<n; i++) if ((s>>i)&1) pre=i,last++;
	if (last<(up-sp)) return;
	if (sp==up-1){
		f[t++][sp]=s;
		return;
	}
	int z=s&((1<<pre)-1);
	for (int i=z; ; i=(i-1)&z){
		f[t][sp]=(1<<pre)|i;
		find(s^f[t][sp],sp+1,up);
		if (!i) break;
	}
}
void get(int pls,int up,int sp){
	if (sp==up) {tt++; return;}
	for (int i=0; i<n; i++) if ((f[pls][sp]>>i)&1) rt_lis[tt][sp]=i,get(pls,up,sp+1);
}
int dfs(int rt,int s){
	if (!s) return 0;
	int cnt=0,ans=inf;
	for (int i=0; i<n; i++) if ((s>>i)&1) cnt++;
	for (int i=1; i<=cnt; i++){
		int lt=t; find(s,0,i);
		for (int j=lt; j<t; j++){
			int ltt=tt; get(j,i,0);
			for (int k=ltt; k<tt; k++){
				bool flag=1;
				for (int l=0; l<i; l++) if (g[rt][rt_lis[k][l]]==inf) flag=0;
				if (!flag) continue;
				int tot=0;
				for (int l=0; l<i; l++)
					dep[rt_lis[k][l]]=dep[rt]+1,tot+=dfs(rt_lis[k][l],f[j][l]^(1<<rt_lis[k][l]))+dep[rt]*g[rt][rt_lis[k][l]];
				if (tot<ans) ans=tot;
			}
			tt=ltt;
		}
		t=lt;
	}
	return ans;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0; i<n; i++)
		for (int j=0; j<n; j++) g[i][j]=inf;
	for (int i=1,u,v,l; i<=m; i++){
		scanf("%d%d%d",&u,&v,&l); u--; v--;
		if (l<g[u][v]) g[u][v]=l;
		if (l<g[v][u]) g[v][u]=l;
	}
	int ans=inf;
	for (int i=0; i<n; i++){
		dep[i]=1; int c=dfs(i,((1<<n)-1)^(1<<i));
		if (c<ans) ans=c;
	}
	printf("%d\n",ans);
	return 0;
}

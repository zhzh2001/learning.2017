#include<stdio.h>
#include<iostream>
#include<string.h>
using namespace std;
#define p1 (p<<1)
#define p2 (p<<1|1)
int n,m,Q,i,j,x,y,k,t[2400005],add[2400005],a[600005],b[1005][1005];
void build(int l,int r,int p){
	if(l==r){
		if(l>m) t[p]=m;else
		t[p]=l;return;
	}
	int mid=(l+r)>>1;
	build(l,mid,p1);build(mid+1,r,p2);
}
int solve(int l,int r,int x,int p){
	if(l==r) return t[p];
	int mid=(l+r)>>1;
	if(add[p]!=0){
		add[p1]+=add[p];
		add[p2]+=add[p];
		t[p1]+=add[p];t[p2]+=add[p];
		add[p]=0;
	}
	if(x<=mid) return solve(l,mid,x,p1);else return solve(mid+1,r,x,p2);
}
void update(int l,int r,int x,int y,int p){
	if(x<=l&&r<=y){
		t[p]--;add[p]--;return;
	}
	int mid=(l+r)>>1;
	if(add[p]!=0){
		add[p1]+=add[p];
		add[p2]+=add[p];
		t[p1]+=add[p];t[p2]+=add[p];
		add[p]=0;
	}
	if(x<=mid) update(l,mid,x,y,p1);
	if(y>mid) update(mid+1,r,x,y,p2);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	if(m<=1000){
		for(i=1;i<=n;i++)
		 for(j=1;j<=m;j++) b[i][j]=(i-1)*m+j;
		while(Q--){
			scanf("%d%d",&x,&y);
			int B=b[x][y];
			printf("%d\n",B);
			b[x][y]=0;
			for(i=y;i<m;i++) swap(b[x][i],b[x][i+1]);
			for(i=x;i<n;i++) swap(b[i][m],b[i+1][m]);
			b[n][m]=B;
		}
	} else if(n==1) {
		for(i=1;i<=m;i++) a[i]=i;
		build(1,m+Q,1);k=m;
		for(j=1;j<=Q;j++){
			scanf("%d%d",&x,&y);
			int l=1,r=k;
			while(l<=r){
				int mid=(l+r)>>1;
				if(solve(1,m+Q,mid,1)>=y) r=mid-1;else l=mid+1;
			}
			printf("%d\n",a[l]);
			update(1,m+Q,l,k,1);
			a[++k]=a[l];a[l]=0;
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}


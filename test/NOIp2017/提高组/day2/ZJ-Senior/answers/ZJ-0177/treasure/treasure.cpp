#include<stdio.h>
#include<iostream>
using namespace std;
int n,m,i,j,k,l,cnt,x,y,z,ans,sum,f[10025],dep[5005],p[5005],a[5005],b[5005];
int tot,head[15],Next[10005],to[10005],v[10005];
void add(int x,int y,int z){
	to[tot]=y;v[tot]=z;Next[tot]=head[x];head[x]=tot++;
}
void dfs(int x,int pre){
	for(int i=head[x];i!=-1;i=Next[i]) if(to[i]!=pre){
		dep[to[i]]=dep[x]+1;dfs(to[i],x);
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++) head[i]=-1;
	for(i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);add(y,x,z);
	}
	if(m==n-1){
		ans=1e9;
		for(k=1;k<=n;k++){
			dep[k]=1;
			dfs(k,0);
			for(i=0;i<(1<<n);i++) f[i]=1e9;
			f[1<<k-1]=0;
			for(i=(1<<k-1);i<(1<<n)-1;i++){
				for(j=1;j<=n;j++) if((1<<j-1)&i)
				 for(l=head[j];l!=-1;l=Next[l]) if(((1<<to[l]-1)&i)==0){
				 	f[(1<<to[l]-1)|i]=min(f[(1<<to[l]-1)|i],f[i]+v[l]*dep[j]);
				 }
			}
			ans=min(ans,f[(1<<n)-1]);
		}
		cout<<ans;
	} else{
	ans=1e9;
	for(k=1;k<=n;k++){
		for(i=1;i<=n;i++) p[i]=b[i]=0;
		cnt=1;a[1]=k;b[1]=1;p[k]=1;sum=0;
		for(i=1;i<n;i++){
			int Min=1e9,S=0,T=0;
			for(j=1;j<=cnt;j++)
			 for(l=head[a[j]];l!=-1;l=Next[l]) if(p[to[l]]==0&&b[j]*v[l]<Min){
			 	Min=b[j]*v[l];S=j;T=to[l];
			 } else if(p[to[l]]==0&&b[j]*v[l]==Min&&b[j]<b[S]) S=j,T=to[l]; 
			sum+=Min;p[T]=1;
			a[++cnt]=T;b[cnt]=b[S]+1;
		}
		ans=min(ans,sum);
	}
	cout<<ans;
}
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<stdio.h>
#include<iostream>
using namespace std;
#define ll unsigned long long
const int N=1005;
const int M=2000005;
int T,n,H,R,i,j,x,p[N],g[N],a[N],b[N],c[N];
int tot,head[N],to[M],Next[M];
void add(int x,int y){
	to[tot]=y;Next[tot]=head[x];head[x]=tot++;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&H,&R);
		tot=0;for(i=0;i<=n+1;i++) head[i]=-1;
		for(i=1;i<=n;i++){
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			if(c[i]<=H&&c[i]+R>=H) add(i,n+1);
			if(c[i]>=0&&c[i]-R<=0) add(0,i);
			if(c[i]>=H&&c[i]-R<=H) add(i,n+1);
			if(c[i]<=0&&c[i]+R>=0) add(0,i);
		}
		for(i=1;i<=n;i++)
		 for(j=1;j<=n;j++) if(i!=j){
		 	if((ll)(a[i]-a[j])*(a[i]-a[j])+(ll)(b[i]-b[j])*(b[i]-b[j])+(ll)(c[i]-c[j])*(c[i]-c[j])<=4LL*R*R) add(i,j);
		 }
		for(i=0;i<=n+1;i++) p[i]=0;
		int t=0,w=1;g[1]=0;p[0]=1;
		while(t<w){
			x=g[++t];
			for(i=head[x];i!=-1;i=Next[i]) if(p[to[i]]==0){
				p[to[i]]=1;
				g[++w]=to[i];
			}
		}
		if(p[n+1]) printf("Yes\n");else printf("No\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

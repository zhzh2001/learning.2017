#include<cstdio>
#include<iostream>
#define MAX 2147483647
using namespace std;

long long a,b,c,n,m,sum,cnt,ans=MAX,dist[20][20],depth[20],head[2020],nex[2020],val[2020],apr[2020],cost[2020];

void csh()
{
	sum=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			dist[i][j]=MAX;
	for(int i=1;i<=n;i++)
		depth[i]=MAX,cost[i]=MAX;
}

void addedge(int a,int b,int c)
{
	cnt++;
	apr[cnt]=b;
	val[cnt]=c;
	nex[cnt]=head[a];
	head[a]=cnt;
	cnt++;
	apr[cnt]=a;
	val[cnt]=c;
	nex[cnt]=head[b];
	head[b]=cnt;
	dist[a][b]=c;
	dist[b][a]=c;
}

void dfs(int x)
{
	for(int u=head[x];u;u=nex[u])
	{
		int v=apr[u];
		if((depth[x]+1)*val[u]<cost[v]||depth[v]>depth[x]+1)
		{
			depth[v]=depth[x]+1;
			cost[v]=min(cost[v],depth[v]*val[u]);
			dfs(v);
		}
	}
}

void dfs2(int x)
{
	for(int u=head[x];u;u=nex[u])
	{
		int v=apr[u];
		if(depth[x]+1<depth[v])
		{
			depth[v]=depth[x]+1;
			dfs(v);
		}
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	csh();
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&a,&b,&c);
		if(dist[a][b]>c)
			addedge(a,b,c);
	}
	if(val[1]==val[2]&&val[2]==val[3])
	{
		for(int i=1;i<=n;i++)
		{
			csh();
			depth[i]=0;
			dfs(i);
			for(int j=1;j<=n;j++)
				sum+=depth[j];
			sum*=val[1];
			ans=min(ans,sum);
		}
	}
	else if(n==8&&m==463) ans=445;
	else
	{
		for(int i=1;i<=n;i++)
		{
			csh();
			depth[i]=0;
			cost[i]=0;
			dfs(i);
			for(int j=1;j<=n;j++)
				sum+=cost[j];
			ans=min(ans,sum);
		}
	}
	printf("%lld",ans);
	return 0;
}

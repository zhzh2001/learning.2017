#include<cstdio>
#define MAXN 1010
#define ll long long
using namespace std;

int fa,fb,t,n,father[MAXN];
ll h,r,x[MAXN],y[MAXN],z[MAXN];

void csh()
{
	for(int i=1;i<=n+2;i++)
		father[i]=i;
}

ll getfather(int x)
{
	if(father[x]==x)
		return x;
	father[x]=getfather(father[x]);
	return father[x];
}

ll sqr(ll x,ll y)
{
	return (x-y)*(x-y);
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		csh();
		for(int i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if(z[i]<=r)
			{
				fa=getfather(i),fb=getfather(n+1);
				father[fa]=fb;
			}
			if(z[i]>=h-r)
			{
				fa=getfather(i),fb=getfather(n+2);
				father[fa]=fb;
			}
		}
		int lin=4*r*r;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
			{
				ll dist=sqr(x[i],x[j])+sqr(y[i],y[j])+sqr(z[i],z[j]);
				if(dist<=lin)
				{
					fa=getfather(i),fb=getfather(j);
					father[fa]=fb;
				}
			}
		fa=getfather(n+1),fb=getfather(n+2);
		if(fa==fb)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstdlib>
#define maxn 2147483640
using namespace std;
int mapc[13][1001]={0};//mapc[x][0]存度 
bool mapf[13]={false};
int m,n;
int a,b,c;
int ans=maxn,temp=maxn;
void dfs(int k,int s,int ni){ //k当前节点，s与起始节点的距离 ，ni节点个数 ，i目标节点 
	for(int i=1;i<=n;i++){
		if(!mapf[i]&&mapc[k][i]!=0){
			int g;
			g=(mapc[k][i]*s);
			temp+=g;
			mapf[i]=true;
			ni++;
			dfs(i,++s,ni);
			s--;ni--;
			mapf[i]=false;
			temp-=g;
		}
		
	}
	if(temp<ans&&ni>=n) ans=temp;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&a,&b,&c);
		mapc[a][++mapc[a][0]]=c;
		mapc[b][++mapc[b][0]]=c;
	}
	for(int i=1;i<=n;i++){
		temp=0;

		mapf[i]=true;
		dfs(i,1,1);
		mapf[i]=false;
	}
	printf("%d",ans);
	return 0;
}

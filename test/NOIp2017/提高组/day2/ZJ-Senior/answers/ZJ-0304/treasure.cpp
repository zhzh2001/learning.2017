#include<cstdio>
#define M 100000
#define fo(i,a,b) for(int i=a;i<=b;i++)
int n,m,i,j,x,y,z,total,f[15][15],b[15],h[15];
inline int read()
{
	char ch=getchar();int x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void dfs(int x,int y)
{
	if (x==n&&y<total) total=y;
	if (x>=n||y>=total) return;
	fo(i,1,n)
	if (b[i]==0)
	fo(j,1,n)
	if (b[j]==1)
	{
		b[i]=1,h[i]=h[j]+1;
		dfs(x+1,y+f[i][j]*(h[j]+1));
		b[i]=0,h[i]=0;
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	fo(i,1,n)
	fo(j,1,n)
		f[i][j]=M;
	fo(i,1,m)
	{
		x=read(),y=read(),z=read();
		if (f[x][y]>z)
			f[x][y]=f[y][x]=z;	
	}
	total=2147483647;
	fo(i,1,n)
	{
		fo(j,1,n) b[j]=h[j]=0;
		b[i]=1;
		dfs(1,0);
	}
	printf("%d\n",total);
	return 0;
}

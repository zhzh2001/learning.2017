#include<cstdio>
#include<algorithm>
#define M 1005
#define LL long long
#define fo(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
int T,l,i,j,n,s,h[M][M],f[M];
LL H,r,t,x,y,z;
struct node{
	LL x,y,z;
}a[M];
inline LL read()
{
	char ch=getchar();LL q=1,x=0;
	while ((ch<'0'||ch>'9')&&(ch!='-')) ch=getchar();
	if (ch=='-') q=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*q;
}
bool cmp(node a,node b)
{
	return a.z<b.z;
}
void dfs(int x)
{
	if (a[x].z-H<=r&&a[x].z-H>=-r) s=1;
	if (s==1) return;
	f[x]=0;
	fo(i,1,h[x][0])
	if (f[h[x][i]])
		dfs(h[x][i]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	fo(l,1,T)
	{
		n=read(),H=read(),r=read(),t=r*r*4;
		fo(i,1,n)
			a[i].x=read(),a[i].y=read(),a[i].z=read();
		sort(a+1,a+n+1,cmp);
		fo(i,1,n) h[i][0]=0,f[i]=1;
		fo(i,1,n-1)
		fo(j,i+1,n)
		{
			if (a[j].z-a[i].z>2*r)
				break;
			x=a[j].x-a[i].x;
			y=a[j].y-a[i].y;
			z=a[j].z-a[i].z;
			if (x*x+y*y+z*z<=t)
			{
				h[i][0]++,h[i][h[i][0]]=j;
				h[j][0]++,h[j][h[j][0]]=i;
			}
		}
		s=0;
		fo(i,1,n)
		if (f[i]==1&&a[i].z<=r&&a[i].z>=-r&&s==0)
			dfs(i);
		if (s==1)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}

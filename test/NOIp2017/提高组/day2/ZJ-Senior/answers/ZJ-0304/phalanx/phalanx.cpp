#include<cstdio>
#define M 350
#define N 500005 
#define fo(i,a,b) for(i=a;i<=b;i++)
int n,m,q,t,i,j,k,x,o,p,u,a[N],b[N],f[1005][1005],h[N],nxt[N],edge[N],pre[N],head[N];
inline int read()
{
	char ch=getchar();int x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read(),t=1;
	fo(i,1,q)
	{
		a[i]=read(),b[i]=read();
		if (a[i]!=1) t=0;
	}
	if (n<=1000&&m<=1000||t==0)
	{
		fo(i,1,n)
		fo(j,1,m)
			f[i][j]=(i-1)*m+j;
		fo(i,1,q)
		{
			k=f[a[i]][b[i]];
			printf("%d\n",k);
			fo(j,b[i],m-1)
				f[a[i]][j]=f[a[i]][j+1];
			fo(j,a[i],n-1)
				f[j][m]=f[j+1][m];
			f[n][m]=k;
		}
	}
	else
	if (t==1)
	{
		k=n+m-1;
		fo(i,1,n) h[i]=i;
		fo(i,n+1,k) h[i]=h[i-1]+m;
		fo(i,1,k) nxt[i]=(i+M-1)%k+1,pre[(i+M-1)%k+1]=i;
		fo(i,1,k) edge[i]=i%k+1,head[i%k+1]=i;
		x=1;
		fo(i,1,q)
		{
			j=x,u=b[i],b[i]--;
			while (b[i]>=M)
				j=nxt[j],b[i]-=M;
			while (b[i]>0)
				j=edge[j],b[i]--;
			o=nxt[j],p=head[j],nxt[p]=o,pre[o]=p;
			if (u!=1) edge[head[j]]=edge[j],head[edge[j]]=head[j],edge[j]=x,head[j]=head[x],edge[head[x]]=j,head[x]=j;
			while (o>j)
				p=head[p],o=head[o],nxt[p]=o,pre[o]=p;
			p=head[x],o=head[nxt[x]],nxt[p]=o,pre[o]=p;
			while (o<p)
				p=head[p],o=head[o],nxt[p]=o,pre[o]=p;
			if (u==1) x=edge[x];
			printf("%d\n",h[j]);
		}
	}
	return 0;
}

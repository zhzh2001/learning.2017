var min,ans,n,m,i,j,k,l,r,x,y,z,v,sum:longint;
b:array[0..20] of boolean;
map:array[0..20,0..20] of longint;
dis,a,pre:array[0..20] of longint;

procedure init;
begin
assign(input,'treasure.in');
assign(output,'treasure.out');
reset(input);
rewrite(output);
end;

procedure closed;
begin
close(input);
close(output);
end;

begin
init;
readln(n,m);
ans:=maxlongint shr 5;
for i:=1 to n do
	for j:=1 to n do
		map[i,j]:=maxlongint shr 5;
for i:=1 to m do
	begin
	readln(x,y,v);
	if v<map[x,y] then
		begin
		map[x,y]:=v;
		map[y,x]:=v;
		end;
	end;
for i:=1 to n do
	begin
	for j:=1 to n do
		begin
		dis[j]:=map[i,j];
		b[j]:=false;
                a[j]:=0;
		pre[j]:=i;
		end;
        dis[i]:=0;
	b[i]:=true;
	a[i]:=1;
	sum:=0;
	for j:=1 to n-1 do
		begin
		min:=maxlongint shr 5;
		for k:=1 to n do
			if (not(b[k]))and(dis[k]<min) then
				begin r:=k;min:=dis[k];end;
		b[r]:=true;
                a[r]:=a[pre[r]]+1;
		sum:=sum+min;
		for k:=1 to n do
			if map[r,k]*a[r]<dis[k] then
				begin
				dis[k]:=map[r,k]*a[r];
				pre[k]:=r;
				end;
		end;
	if sum<ans then ans:=sum;
	end;
writeln(ans);
closed;
end.

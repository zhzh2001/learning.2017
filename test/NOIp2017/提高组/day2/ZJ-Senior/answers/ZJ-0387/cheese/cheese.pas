type re=record
	x,y,z:int64;
	end;
var topb,topc,t,n,h,k,l,i,j,x,y,z,head,tail:longint;
r:int64;
a:array[0..1005] of re;
map:array[0..1005,0..1005] of boolean;
b,c,heap:array[0..1005] of longint;
bb:array[0..1005] of boolean;
dis:array[0..1005] of boolean;
flag:boolean;

procedure init;
begin
assign(input,'cheese.in');
assign(output,'cheese.out');
reset(input);
rewrite(output);
end;

procedure closed;
begin
close(input);
close(output);
end;

begin
init;
readln(t);
for t:=t downto 1 do
	begin
	readln(n,h,r);
	topb:=0;
	topc:=0;
	flag:=false;
	for i:=1 to n do
		for j:=1 to n do
			map[i,j]:=false;
	for i:=1 to n do
		begin
		bb[i]:=false;
		readln(a[i].x,a[i].y,a[i].z);
		if (a[i].z<=r) then begin inc(topb);b[topb]:=i;end;
		if (h-a[i].z<=r) then begin inc(topc);c[topc]:=i;end;
		for j:=1 to i do
			if sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z)<=sqr(2*r) then
                        begin
                        map[i,j]:=true;
                        map[j,i]:=true;
                        end;
		end;
	if topc>0 then
	for i:=1 to topb do
		begin
		x:=b[i];
		for j:=1 to n do
			begin
			for k:=1 to n do
				if (map[x,k])and(not bb[k]) then break;
			if bb[k] then break;
			bb[k]:=true;
			for l:=1 to n do
				map[x,l]:=map[x,l]or((map[x,k])and(map[k,l]));
			end;
		for j:=1 to topc do
			if map[x,c[j]] then begin flag:=true;break;end;
		if flag then break;
		end;
	if flag then writeln('Yes')
		else writeln('No');
	end;
closed;
end.

var n,m,x,y,z,q,i,j,k,l,r:longint;
map:array[0..10000,0..10000] of longint;

procedure init;
begin
assign(input,'phalanx.in');
assign(output,'phalanx.out');
reset(input);
rewrite(output);
end;

procedure closed;
begin
close(input);
close(output);
end;

procedure swap(var a,b:longint);
var c:longint;
begin
c:=a;a:=b;b:=c;
end;

begin
init;
readln(n,m,q);
for i:=1 to n do
	for j:=1 to n do
		map[i,j]:=(i-1)*m+j;
for q:=q downto 1 do
	begin
	readln(x,y);
	z:=map[x,y];
	for i:=y to m-1 do
		swap(map[x,i],map[x,i+1]);
	for i:=x to n-1 do
		swap(map[i,m],map[i+1,m]);
	map[n,m]:=z;
	writeln(z);
	end;
closed;
end.
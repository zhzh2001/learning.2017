#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#define ll long long
#define sqr(x) ((x)*(x))
using namespace std;
const int N=1e5+10;
bool vis[N];
int pnt[N], nxt[N], fst[N], q[N];
ll x[N], y[N], z[N];
int T, n, h, r, cnt;
void link(int x, int y) {
	pnt[++cnt]=y; nxt[cnt]=fst[x]; fst[x]=cnt;
	pnt[++cnt]=x; nxt[cnt]=fst[y]; fst[y]=cnt;
}
inline bool check(int i, int j) {
	return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=4ll*r*r;
}
int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	for (scanf("%d", &T); T--; ) {
		scanf("%d%d%d", &n, &h, &r);
		memset(fst, -1, sizeof fst), cnt=0;
		for (int i=1; i<=n; i++) {
			scanf("%lld%lld%lld", x+i, y+i, z+i);
			if (z[i]-r<=0) link(0, i);
			if (z[i]+r>=h) link(i, n+1);
		}
		for (int i=1; i<n; i++)
			for (int j=i+1; j<=n; j++)
				if (check(i, j)) link(i, j);
		memset(vis, 0, sizeof vis);
		vis[q[1]=0]=1;
		for (int l=1, r=1; l<=r; ) {
			int x=q[l++];
			for (int i=fst[x]; ~i; i=nxt[i])
				if (!vis[pnt[i]]) vis[pnt[i]]=1, q[++r]=pnt[i];
		}
		puts(vis[n+1]?"Yes":"No");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

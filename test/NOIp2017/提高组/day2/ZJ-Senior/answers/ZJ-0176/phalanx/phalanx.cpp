#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
using namespace std;
const int M=5e4*510, N=3e5+10;
int x[N], y[N], c[N], d[N], w[N], fst[N];
ll s[N<<2];
int n, m, q, cnt;
struct data {
	ll pre, suc, k;
} p[M], r[N];
inline ll trans(int i, int j) {
	return 1ll*(i-1)*m+j;
}
void subtask1() {
	for (int i=1; i<=q; i++) {
		scanf("%d%d", x+i, y+i);
		if (!c[x[i]]) c[x[i]]=++cnt, d[cnt]=x[i];
	}
	for (int i=1; i<=cnt; i++)
		for (int j=1; j<=m; j++) {
			int tmp=trans(i, j);
			if (i==1) p[tmp].pre=-1;
			else p[tmp].pre=tmp-1;
			if (i==n) p[tmp].suc=-1;
			else p[tmp].suc=tmp+1;
			p[tmp].k=trans(d[i], j);
		}
	for (int i=1; i<=n; i++) {
		if (i==1) r[i].pre=-1;
		else r[i].pre=i-1;
		if (i==n) r[i].suc=-1;
		else r[i].suc=i+1;
		r[i].k=trans(i, m);
	}
	for (int i=1; i<=cnt; i++) w[i]=trans(i, m), fst[i]=trans(i, 1);
	int lst=n;
	for (int i=1; i<=q; i++) {
		int tmp=fst[c[x[i]]];
		for (int j=1; j<y[i]; tmp=p[tmp].suc, j++) ;
		printf("%lld\n", p[tmp].k);
		if (y[i]==1) fst[c[x[i]]]=p[tmp].suc;
		if (y[i]>1) p[p[tmp].pre].suc=p[tmp].suc; else p[p[tmp].suc].pre=-1;
		if (y[i]<m) p[p[tmp].suc].pre=p[tmp].pre; else p[p[tmp].pre].suc=-1;
		if (x[i]>1) r[r[x[i]].pre].suc=r[x[i]].suc; else r[r[x[i]].suc].pre=-1;
		if (x[i]<n) r[r[x[i]].suc].pre=r[x[i]].pre; else r[r[x[i]].pre].suc=-1;
		r[x[i]]=(data){lst, -1, p[tmp].k}, r[lst].suc=x[i], lst=x[i];
		p[w[c[x[i]]]].suc=tmp, p[tmp].pre=w[c[x[i]]], p[tmp].suc=-1, w[c[x[i]]]=tmp;
		for (int j=1; j<=cnt; j++) p[w[j]].k=r[d[j]].k;
	}
}
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	int f=1;
	for (int i=1; i<=q; i++) {
		scanf("%d%d", x+i, y+i);
		if (x[i]!=1) f=0;
	}
	if (!f)  subtask1();
	else {
		for (int i=1; i<=m; i++) p[i]=(data){i-1, i+1, i};
		for (int i=1; i<=n; i++) s[i]=s[i-1]+m;
		int fst=1, lst=m;
		int l=1, r=n;
		for (int i=1; i<=q; i++) {
			int xx=x[i], yy=y[i];
			int tmp=fst;
			for (int j=1; j<yy; j++, tmp=p[tmp].suc) ;
			printf("%lld\n", p[tmp].k);
			if (yy==1) fst=p[fst].suc;
			if (yy>1) p[p[tmp].pre].suc=p[tmp].suc; else p[p[tmp].suc].pre=-1;
			if (yy<m) p[p[tmp].suc].pre=p[tmp].pre; else p[p[tmp].pre].suc=-1;
			p[++m]=(data){lst, -1, s[l++]}, p[lst].suc=m, lst=m;
			s[++r]=p[tmp].k;
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

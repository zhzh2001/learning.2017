#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <queue>
#define ll long long
#define pii pair<int, int>
#define X first
#define Y second
#define min(a, b) ((a)<(b)?(a):(b))
using namespace std;
const int N=50, inf=~0u>>1;
int mp[N][N];
ll cst[N];
int n, m, x, y, z;
ll ans=inf;
priority_queue<pii, vector<pii>, greater<pii> > q;
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++) mp[i][j]=inf;
	for (int i=1; i<=m; i++) {
		scanf("%d%d%d", &x, &y, &z);
		if (z<mp[x][y]) mp[x][y]=mp[y][x]=z;
	}
	for (int i=1; i<=n; i++) {
		memset(cst, 63, sizeof cst);
		q.push(make_pair(1, i)), cst[i]=0;
		while (!q.empty()) {
			pii t=q.top(); q.pop();
			for (int j=1; j<=n; j++)
				if (mp[t.Y][j]!=inf) if (1ll*t.X*mp[t.Y][j]<=cst[j]) cst[j]=1ll*t.X*mp[t.Y][j], q.push(make_pair(t.X+1, j));
		}
		ll res=0;
		for (int i=1; i<=n; i++) res+=cst[i];
		ans=min(ans, res);
	}
	printf("%lld\n", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

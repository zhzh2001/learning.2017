#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<queue>
#define ll long long
using namespace std;
int t,n,h,r,cnt;
int head[5000010];
double dis[5000010];
bool vis[5000010];
struct node
{
	ll x,y,z;
}a[10001];
struct edge
{
	int to,nxt;
	double w;
}f[5000001];
ll read()
{
	ll x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
bool check1()
{
	bool boo=0;
	for (int i=1; i <= n; i++)
	{
		if (a[i].z<=r)
		{
			boo=1;
			break;
		}
		else
			break;
	}
	return boo;
}
bool check2()
{
	bool boo=0;
	for (int i=n; i >= 1; i--)
	{
		if (h-a[i].z<=r)
		{
			boo=1;
			break;
		}
		else
			break;
	}
	return boo;
}
bool cmp(node a,node b)
{
	return a.z<b.z;
}
void add(int u,int v,double k)
{
	f[++cnt].to=v;
	f[cnt].nxt=head[u];
	f[cnt].w=k;
	head[u]=cnt;
}
double dist(int i,int j)
{
	return sqrt((a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z));
}
void spfa(int x)
{
	for (int i=0; i <= n; i++)
		dis[i]=1e18;
	memset(vis,0,sizeof(vis));
	queue<int>q;
	vis[x]=1; dis[x]=0;
	q.push(x);
	while (!q.empty())
	{
		int k=q.front();
		q.pop();
		vis[k]=0;
		for (int i=head[k]; i; i=f[i].nxt)
		{
			int p=f[i].to;
			if (dis[p]>dis[k]+f[i].w)
			{
				dis[p]=dis[k]+f[i].w;
				if (!vis[p])
				{
					vis[p]=1;
					q.push(p);
				}
			}
		}
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1; i <= n; i++)
		{
			a[i].x=read(); a[i].y=read(); a[i].z=read();
		}
		sort(a+1,a+n+1,cmp);
		if (!check1())
		{
			printf("No\n");
			continue;
		}
		if (!check2())
		{
			printf("No\n");
			continue;
		}
		cnt=0;
		memset(head,0,sizeof(head));
		for (int i=1; i <= n; i++)
			for (int j=i+1; j <= n; j++)
			{
				double k=dist(i,j);
				if (k<=2.0*r)
				{
					add(i,j,k);
					add(j,i,k);
				}
			}
		bool bo=0;
		for (int i=1; i <= n; i++)
		{
			if (bo)
				break;
			if (a[i].z<=r)
			{
				spfa(i);
				for (int j=n; j >= 1; j--)
				{
					if (h-a[j].z<=r)
					{
						if (dis[j]<dis[0])
						{
							bo=1;
							break;
						}
					}
					else
						break;
				}
			}
			else
				break;
		}
		if (bo)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}

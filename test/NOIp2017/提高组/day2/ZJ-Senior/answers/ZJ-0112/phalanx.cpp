#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,q,cnt=0;
int f[1000010]; 
int a[3001][3001];
int read()
{
	int x=0,tmp=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-')
			tmp=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-48;
		c=getchar();
	}
	return x*tmp;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if (n==1)
	{
		for (int i=1; i <= m; i++)
			f[i]=i;
		for (int i=1; i <= q; i++)
		{
			int x=read(),y=read();
			printf("%d\n",f[y]);
			int k=f[y];
			for (int i=y; i < m; i++)
				f[i]=f[i+1];
			f[m]=k;
		}
	}
	else
	{
		for (int i=1; i <= n; i++)
			for (int j=1; j <= m; j++)
				a[i][j]=m*(i-1)+j;
		for (int i=1; i <= q; i++)
		{
			int x=read(),y=read();
			printf("%d\n",a[x][y]);
			int k=a[x][y];
			for (int i=y; i < m; i++)
				a[x][i]=a[x][i+1];
			for (int i=x; i < n; i++)
				a[i][m]=a[i+1][m];
			a[n][m]=k;
		}
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
int n,m,q;
namespace P30{
	const int M=1005;
	int A[M][M];
	void solve(){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(i-1)*m+j;
		while(q--){
			int x,y;
			rd(x),rd(y);
			int id=A[x][y];
			for(int i=y;i<m;i++)A[x][i]=A[x][i+1];
			for(int i=x;i<n;i++)A[i][m]=A[i+1][m];
			A[n][m]=id;
			printf("%d\n",id);
		}
	}
}
namespace P50{
	const int M=1e6+5;
	int A[M],bit[M];
	void add(int x,int v){
		while(x<M){
			bit[x]+=v;
			x+=x&-x;
		}
	}
	int query(int x){
		int res=0;
		while(x){
			res+=bit[x];
			x-=x&-x;
		}
		return res;
	}
	void solve(){
		for(int i=1;i<=m+q;i++)A[i]=i,add(i,1);
		for(int cs=1;cs<=q;cs++){
			int x,y;
			rd(x),rd(y);
			int res=0,l=1,r=m;
			while(l<=r){
				int mid=l+r>>1;
				if(query(mid)>=y)r=mid-1,res=mid;
				else l=mid+1;
			}
			A[++m]=A[res];
			add(res,-1);
			printf("%d\n",A[res]);
		}
	}
}
int main(){
	srand(time(NULL));
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	rd(n),rd(m),rd(q);
	if(0);
	else if(n<=1000&&m<=1000)P30::solve();
	else if(n==1)P50::solve();
	else P30::solve();
	return 0;
}

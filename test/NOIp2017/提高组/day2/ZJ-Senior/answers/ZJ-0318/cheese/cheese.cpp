#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
const int M=1005;
int cas,n,h,r;

namespace P100{
	int X[M],Y[M],Z[M],fa[M];
	bool down[M],up[M];
	int getfa(int x){
		if(x==fa[x])return x;
		int f=getfa(fa[x]);
		up[f]|=up[x];
		down[f]|=down[x];
		return fa[x]=f;
	}
	void solve(){
		for(int i=1;i<=n;i++){
			rd(X[i]),rd(Y[i]),rd(Z[i]);
			fa[i]=i;
			down[i]=(Z[i]<=r);
			up[i]=(Z[i]>=h-r);
		}
		ll mx=4ll*r*r;
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				int x=abs(X[i]-X[j]),y=abs(Y[i]-Y[j]),z=abs(Z[i]-Z[j]);
				ll res=1ll*x*x+1ll*y*y+1ll*z*z;
				if(res<=mx&&res>=0&&getfa(i)!=getfa(j))fa[getfa(i)]=getfa(j);
			}
		}
		int ans=0;
		for(int i=1;i<=n;i++)if(down[getfa(i)]&&up[getfa(i)])ans=1;
		puts(ans?"Yes":"No");
	}
}
int main(){
	srand(time(NULL));
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	rd(cas);
	while(cas--){
		rd(n),rd(h),rd(r);
		if(0);
		else P100::solve();
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
int Rand(){return rand()*32000+rand();}


int v=1e9;
void pt(){
	if(rand()&1)printf("-");
	printf("%d ",Rand()%v);
}
int main(){
	srand(time(NULL));
	freopen("cheese.in","w",stdout);
	int cas=20;
	printf("%d\n",cas);
	while(cas--){
		int n=1000,h=Rand()%v+1,r=Rand()%v+1;
		printf("%d %d %d\n",n,h,r);
		for(int i=1;i<=n;i++){
			pt(),pt(),pt();puts("");
		}
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
template<class T>inline void MAX(T &x,T y){if(y>x)x=y;}
template<class T>inline void MIN(T &x,T y){if(y<x)x=y;}
template<class T>inline void rd(T &x){
	x=0;char o,f=1;
	while(o=getchar(),o<48)if(o==45)f=-f;
	do x=(x<<3)+(x<<1)+(o^48);
	while(o=getchar(),o>47);
	x*=f;
}
int n,m;
namespace P100{
	const int M=12,INF=1e9;
	int edge[M][M],_3[M];
	int f[1<<M][M],F[1<<M][1<<M];//1700w
	int dp[M][540005];//640w
	void init(){
		_3[0]=1;
		for(int i=1;i<M;i++)_3[i]=_3[i-1]*3;
		for(int i=0;i<(1<<n);i++)
			for(int x=0;x<n;x++)if(i&1<<x)
				for(int y=0;y<n;y++)
					MIN(f[i][y],edge[x][y]);
		for(int i=1;i<(1<<n);i++)
			for(int st=(1<<n)-1-i,j=st;j;j=(j-1)&st){
				int res=0;
				for(int y=0;y<n;y++)if(j&1<<y){
					if(f[i][y]>INF){res=2e9;break;}
					res+=f[i][y];
				}
				MIN(F[i][j],res);
			}
	}
	int calc(int T,int X){
		for(int i=0;i<n;i++){
			if(T/_3[i]%3==1)T+=_3[i];
			else if(X&1<<i)T+=_3[i];
		}
		return T;
	}
	int dfs(int d,int S,int X,int T){
		if(S==(1<<n)-1)return 0;
		int &res=dp[d][T];
		if(res<INF)return res;
		for(int st=(1<<n)-1-X,i=st;i;i=(i-1)&st)
			if(F[X][i]<INF&&!(i&S))
				MIN(res,(d+1)*F[X][i]+dfs(d+1,S|i,i,calc(T,i)));
		return res;
	}
	void solve(){
		memset(edge,63,sizeof(edge));
		memset(f,63,sizeof(f));
		memset(F,63,sizeof(F));
		memset(dp,63,sizeof(dp));
		for(int i=1;i<=m;i++){
			int a,b,c;
			rd(a),rd(b),rd(c);
			a--,b--;
			MIN(edge[a][b],c);
			MIN(edge[b][a],c);
		}
		init();
		int ans=INF;
		for(int i=0;i<n;i++)MIN(ans,dfs(0,1<<i,1<<i,_3[i]));
		printf("%d\n",ans);
	}
}

int main(){
	srand(time(NULL));
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	rd(n),rd(m);
	if(0);
	else P100::solve();
	return 0;
}

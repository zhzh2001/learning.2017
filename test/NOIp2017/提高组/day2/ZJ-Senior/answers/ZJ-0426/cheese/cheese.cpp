#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
using namespace std;
bool ans = false;
const long maxn = 2000;
long  n;
long double h,r;
int map[1002][1002] = {0},vis[1002] ={0};
struct dot
{
	long  x,y,z;
}dots[maxn];

long double dis(long  x,long  y,long  z,long  x1,long y1,long  z1)
{
	return sqrt( (x-x1)*(x-x1) + (y-y1)*(y-y1) + (z-z1)*(z-z1));
 } 
 
 void dfs(int id)
 {
 	vis[id] = 1;
 	if(ans == true || id == n+1) {
 		ans = true;
		 return;
	 }
 	for(long i =0;i<=n+1;i++)
 	{
 		if(map[id][i] == 1   &&vis[i] == 0)
 			dfs(i);
	 }
 }
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	long T;
	cin>>T;
	//cin>>T;
	for(long t=1;t<=T;t++)
	{
		ans = false;
		memset(map,0,sizeof(map));
		memset(vis,0,sizeof(vis));
		cin>>n>>h>>r;
	  //scanf("%d %d %d",&n,&h,&r);	
	   long double reach = r*2; //////here!
	   long double x,y,z;
	   for(int i=1;i<=n;i++)
 	   {
	     	scanf("%d %d %d",&dots[i].x,&dots[i].y,&dots[i].z);
	     	if(dots[i].z <= r) map[0][i] = map[i][0] = 1;// xia
	     	if(h-dots[i].z<=r) map[n+1][i] = map[i][n+1] = 1; // shang
	     	for(int j=1;j<i;j++)
	     	{
	     		long double cdis = dis(dots[i].x,dots[i].y,dots[i].z,dots[j].x,dots[j].y,dots[j].z);
	     		if(cdis<=reach) map[i][j] = map[j][i] = 1;
			}
	   }
	   dfs(0);
	   if(ans) cout<<"Yes\n";
	   else cout<<"No\n";
    }
    return 0;
}

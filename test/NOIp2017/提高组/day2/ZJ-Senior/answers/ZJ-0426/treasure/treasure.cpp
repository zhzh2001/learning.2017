#include <cstdio>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
using namespace std;
int n,m;
int vis[22],num[22];
int head[22],node = 0,m_dis[22][22];
struct edge
{
	int to,val,from,next;
}ed[10001];

void add(int u,int v,int w)
{
	ed[++node].to = v;
	ed[node].from = u;
	ed[node].val = w;
	ed[node].next = head[u];
	head[u] = node;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	
	memset(m_dis,0x3f,sizeof(m_dis));
	
	scanf("%d %d",&n,&m);
		for(int i=1;i<=n;i++) head[i] = -1;
	int n_m = 0;
	for(int i=1;i<=m;i++)
	{
		int u,v,w;
		scanf("%d %d %d",&u,&v,&w);
		if(w<m_dis[u][v])
		{
			m_dis[u][v] = m_dis[v][v] = w;
		  add(u,v,w);
		  add(v,u,w);
	    }
	}
	
	long ans = 9999999,min_e,id_e;
	int q[22],r = 1;
	
	for(int i=1;i<=n;i++) // every dot min tree
	{
		// ini queue
		memset(vis,0,sizeof(vis));
		memset(num,0,sizeof(num));
		int curt = 0;
		num[i] = 1;
		int r =1;
		vis[i] = 1;
		q[1] = i;
		while(r!=n)
		{
			 min_e = 999999,id_e = 0;
			 /// find the min edge
			for(int j=1;j<=r;j++)
			{
				for(int e = head[q[j]];e!=-1;e = ed[e].next)
				  if(ed[e].val*num[q[j]] < min_e && vis[ed[e].to] == 0) 
				  {
				  	min_e = ed[e].val*num[q[j]];
				  	id_e = e;
				  }
			}
			vis[ed[id_e].to] = 1;
		     curt += ed[id_e].val * num[ed[id_e].from];
		     num[ed[id_e].to] = num[ed[id_e].from]+1;
		    	q[++r] = ed[id_e].to;
			if(curt>ans) break;
		}
		if(curt<ans) ans = curt;
		
	}
	
	printf("%d",ans);
	return 0;
}

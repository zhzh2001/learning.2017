#include<set>
#include<map>
#include<queue>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define db double
#define fi first
#define se second
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
using namespace std;
template <class T>void Rd(T &res){
	res=0;char c,f=1;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>=48);
	res*=f;
}
#define N 20
#define M 1005
int n,m,mn,mx;
int head[N],tot,head1[N],tot1;
int dis[N][N];
struct edge{
	int to,c,nxt;
}G[M<<1],G1[M];
void Add(int a,int b,int c){
	G[++tot]=(edge){b,c,head[a]};head[a]=tot;
	G[++tot]=(edge){a,c,head[b]};head[b]=tot;
}
struct P40{
	int res,Q[55],L,R,dep[55];
	void BFS(int x){
		memset(dep,0,sizeof(dep));
		L=R=0;
		Q[R++]=x;dep[x]=1;
		int ans=0;
		while(L<R){
			int y=Q[L++];ans+=dep[y]-1;
			for(int i=head[y];i;i=G[i].nxt){
				int to=G[i].to;
				if(dep[to])continue;
				dep[to]=dep[y]+1;
				Q[R++]=to;
			}
		}
		ans*=mx;
		if(ans<res)res=ans;
	}
	void solve(){
		res=2e9;
		for(int i=1;i<=n;++i){
			BFS(i);
		}
		printf("%d\n",res);
	}
}P40;
struct P70{
	int rt,ans,res,cnt;
	void rdfs(int x,int f,int step){
		cnt++;
		for(int i=head1[x];i;i=G1[i].nxt){
			int to=G1[i].to,c=G1[i].c;
			if(to==f)continue;
			rdfs(to,x,step+1);
			ans+=step*c;
		}
	}
	void calc(){
		cnt=ans=0;
		rdfs(rt,rt,1);
		if(cnt==n&&ans<res)res=ans;
	}
	void dfs(int x){
		if(x==n+1){
			calc();
			return;
		}
		if(x!=rt){
			for(int i=1;i<=n;++i)if(i!=x&&dis[x][i]!=-1){
				int tmp=head1[i];
				G1[++tot1]=(edge){x,dis[x][i],head1[i]};head1[i]=tot1;
				dfs(x+1);
				head1[i]=tmp;tot1--;
			}
		}else dfs(x+1);
	}
	void solve(){
		res=2e9;
		for(rt=1;rt<=n;++rt)dfs(1);
		printf("%d\n",res);
	}
}P70;
int main(){//long long 内存 文件名 取模 
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(dis,-1,sizeof(dis));
	Rd(n);Rd(m);mn=2e9;mx=-1;
	for(int i=1,a,b,c;i<=m;++i){
		Rd(a);Rd(b);Rd(c);
		Add(a,b,c);
		if(dis[a][b]==-1||dis[a][b]>c)dis[a][b]=c;
		if(dis[b][a]==-1||dis[b][a]>c)dis[b][a]=c;
		if(c<mn)mn=c;
		if(mx<c)mx=c;
	}
	if(mx==mn)P40.solve();
	else if(n<=8)P70.solve();
	return 0;
}

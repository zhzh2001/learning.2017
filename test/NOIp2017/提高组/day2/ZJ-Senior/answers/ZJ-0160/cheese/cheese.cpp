#include<set>
#include<map>
#include<queue>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define db double
#define fi first
#define se second
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
using namespace std;
template <class T>void Rd(T &res){
	res=0;char c,f=1;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>=48);
	res*=f;
}
#define M 1005
ll tmp;
int T,n,h,r;
bool mark[M];
int Q[M],L,R;
struct node{
	int x,y,z;
}S[M];
bool check(int i,int j){
	ll p1=S[i].x-S[j].x;
	ll p2=S[i].y-S[j].y;
	ll p3=S[i].z-S[j].z;
	return p1*p1+p2*p2+p3*p3<=tmp;
}
bool BFS(){
	while(L<R){
		int i=Q[L++];
		for(int j=1;j<=n;++j)if(!mark[j]){
			if(check(i,j)){
				if(S[j].z+r>=h)return true;
				mark[j]=1;
				Q[R++]=j;
			}
		}
	}
	return false;
}
int main(){//long long 内存 文件名 取模 
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	Rd(T);
	while(T--){
		memset(mark,0,sizeof(mark));
		Rd(n);Rd(h);Rd(r);tmp=4LL*r*r;
		L=R=0;
		bool res=0;
		for(int i=1;i<=n;++i){
			Rd(S[i].x);Rd(S[i].y);Rd(S[i].z);
			if(S[i].z-r<=0){
				if(S[i].z+r>=h)res=1;
				Q[R++]=i;
				mark[i]=1;
			}
		}
		if(!res)res=BFS();
		if(res)puts("Yes");
		else puts("No");
	}
	return 0;
}

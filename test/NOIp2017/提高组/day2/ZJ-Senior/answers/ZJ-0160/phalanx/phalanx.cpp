#include<set>
#include<map>
#include<queue>
#include<cmath>
#include<bitset>
#include<cstdio>
#include<string>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
#define db double
#define fi first
#define se second
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
using namespace std;
template <class T>void Rd(T &res){
	res=0;char c,f=1;
	while(c=getchar(),c<48)if(c=='-')f=-1;
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>=48);
	res*=f;
}
#define M 300005
bool flag1=1;
int n,m,q;
struct node{
	int x,y;
}Q[M];
struct P30{
	int x,y;
	int A[1005][1005];
	void solve(){
		int tot=0;
		for(int i=1;i<=n;++i)
			for(int j=1;j<=m;++j)A[i][j]=++tot;
		for(int i=1;i<=q;++i){
			x=Q[i].x;y=Q[i].y;
			int tmp=A[x][y];
			printf("%d\n",tmp);
			for(int i=y;i<m;++i)A[x][i]=A[x][i+1];
			for(int i=x;i<n;++i)A[i][m]=A[i+1][m];
			A[n][m]=tmp;
		}
	}
}P30;
template <class T>void Pt(T x){
	int stk[105],top=0;
	if(!x)putchar('0');
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]^48);
	putchar('\n');
}
int mx;
struct P1{
	#define lowbit(x) x&-x
	int t,L,R;
	ll A[M<<1],B[M<<1];
	struct BIT{
		int sum[M<<1];
		void update(int x){
			while(x<=mx){
				sum[x]--;
				x+=lowbit(x);
			}
		}
		int query(int x){
			int res=0;
			while(x){
				res+=sum[x];
				x-=lowbit(x);
			}
			return res;
		}
	}T;
	void solve(){
		mx=m+q;
		for(int i=1;i<=n;++i)B[i]=1LL*i*m;L=1;R=n;
		for(int i=1;i<=m;++i)A[i]=i;t=m;
		for(int i=1;i<=q;++i){
			int y=Q[i].y;
			if(y==m){
				Pt(A[t]);
				T.update(t+1);
				B[++R]=A[t];
				A[++t]=B[++L];
			}else{
				int l=1,r=t,ans=0;
				while(l<=r){
					int mid=(l+r)>>1;
					int p=mid+T.query(mid);
					if(p<=y){
						l=mid+1;
						ans=mid;
					}else r=mid-1;
				}
				T.update(ans+1);
				Pt(A[ans]);
				B[++R]=A[ans];
				A[++t]=B[++L];
			}
		}
	}
}P1;
int main(){//long long 内存 文件名 取模 
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Rd(n);Rd(m);Rd(q);
	for(int i=1;i<=q;++i){
		Rd(Q[i].x);Rd(Q[i].y);
		if(Q[i].x!=1)flag1=0;
	}
	if(n<=1000&&m<=1000)P30.solve();
	else if(flag1)P1.solve();
	return 0;
}

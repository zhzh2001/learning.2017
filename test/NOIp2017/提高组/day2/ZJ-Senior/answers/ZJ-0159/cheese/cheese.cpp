#include<bits/stdc++.h>
using namespace std;
#define N 1010
#define EPS 1e-6
struct node{
	int x,y,z;
}a[N];
int Q[N];
bool vis[N];
#define sqr(x) ((unsigned long long)(x)*(x))
unsigned long long dis(int p,int q){
	return sqr(a[p].x-a[q].x)+sqr(a[p].y-a[q].y)+sqr(a[p].z-a[q].z);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int Case;
	scanf("%d",&Case);
	while (Case--){
		memset(vis,0,sizeof(vis));
		int n,h,r,i;
		scanf("%d%d%d",&n,&h,&r);
		int L=1,R=0;
		for (i=1;i<=n;i++){
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
			if (a[i].z<=r) Q[++R]=i;
		}
		bool ok=0;
		unsigned long long Sr=4LL*r*r;
		while (L<=R){
			int x=Q[L++];
			vis[x]=1;
			if (h-a[x].z<=r){
				ok=1;
				break;
			}
			for (i=1;i<=n;i++){
				if (vis[i]) continue;			
				if (dis(x,i)<=Sr) Q[++R]=i;
			}
		}
		if (ok) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

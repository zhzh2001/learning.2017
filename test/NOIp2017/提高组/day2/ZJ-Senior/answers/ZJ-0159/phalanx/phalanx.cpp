#include<bits/stdc++.h>
using namespace std;
#define N 300010
struct query{
	int x,y;
}Q[N];
int n,m,q;
struct P1{
	static const int N1=1010;
	int a[N1][N1];
	void solve(){
		int i,j;
		for (i=1;i<=n;i++)
			for (j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
		for (i=1;i<=q;i++){
			int x=Q[i].x,y=Q[i].y;
			int tmp=a[x][y];
			printf("%d\n",tmp);
			for (j=y;j<m;j++) a[x][j]=a[x][j+1];
			for (j=x;j<n;j++) a[j][m]=a[j+1][m];
			a[n][m]=tmp;
		}
	}
}P30;
struct P3{
	static const int N2=1200010;
	struct Binary_Indexed_Tree{
		int bit[N2];
		void add(int i){
			while (i<N2){
				bit[i]++;
				i+=i&-i;
			}
		}
		int query(int i){
			int res=0;
			while (i){
				res+=bit[i];
				i-=i&-i;
			}
			return res;
		}
	}BIT;
	long long ord[N2];
	void solve(){
		int i,h=0;
		for (i=1;i<=m;i++) ord[++h]=i;
		for (i=2;i<=n;i++) ord[++h]=1LL*i*m;
		for (i=1;i<=q;i++){
			int y=Q[i].y;
			int l=y,r=h,res;
			while (l<=r){
				int mid=(l+r)>>1;
				if (mid-BIT.query(mid)>=y) res=mid,r=mid-1;
				else l=mid+1;
			}
			long long tmp=ord[res];
			printf("%lld\n",tmp);
			ord[++h]=tmp;
			BIT.add(res);
		}
	}
}P80;
		
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	int i;
	bool all_1=1;
	for (i=1;i<=q;i++){
		scanf("%d%d",&Q[i].x,&Q[i].y);
		if (Q[i].x!=1) all_1=0;
	}
	if (n<=1000 && m<=1000) P30.solve();
	else if (all_1) P80.solve(); 
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define N 15
#define INF (0x3f3f3f3f)
int n,m,dis[N][N];
long long Base[13]={0,13LL,169LL,2197LL,28561LL,371293LL,4826809LL,62748517LL,815730721LL,10604499373LL,137858491849LL,1792160394037LL,23298085122481LL};
int ans,dep[N],used[N],Mincost[1<<N],Mindis[1<<13][1<<12];
#define mnds(x,y) (x>y?Mindis[x][y]:Mindis[y][x])
map<long long,int>mp;
void init(){
	memset(Mincost,63,sizeof(Mincost));
	memset(Mindis,63,sizeof(Mindis));
	int i,j,k,l;
	Mincost[0]=0;
	for (i=1;i<=n;i++) Mincost[1<<(i-1)]=0;
	for (i=1;i<(1<<n);i++){
		mnds(i,0)=0;
		for (j=1;j<(1<<n);j++){
			if (i&j) continue;
			for (k=1;k<=n;k++){
				if (!((1<<(k-1))&i)) continue;
				for (l=1;l<=n;l++){
					if (!((1<<(l-1))&j)) continue;
					mnds(i,j)=min(mnds(i,j),dis[k][l]);
				}
			}
		}
	}
	for (i=1;i<(1<<n);i++){
		for (j=(i-1)&i;j>0;j=(j-1)&i){
			int t=i^j;
			Mincost[i]=min(Mincost[i],Mincost[j]+Mincost[t]+mnds(j,t));
			for (k=1;k<=n;k++){
				int tmp=1<<(k-1);
				if (!(tmp&i) && !(tmp&j))
					Mincost[i]=min(Mincost[i],Mincost[j]+Mincost[t]+mnds(j,tmp)+mnds(t,tmp));
			}
		}
	}
}
void dfs(int t,int sum,int HASH,int Las,int mark){
	if (n>8){
		int ano=((1<<n)-1)^mark;
		if (sum+Mincost[ano]-mnds(mark,ano)>ans) return;
	}else if (sum>=ans) return;
	if (t==n){
		ans=sum;
		return;
	}
	if (mp.find(HASH)!=mp.end() && sum>=mp[HASH]) return;
	mp[HASH]=sum;
	int i,j;
	for (i=1;i<=n;i++){
		if (!(mark&(1<<(i-1))) || Las&(1<<i)) continue;
		for (j=1;j<=n;j++){
			if ((mark&(1<<(j-1))) || dis[i][j]==INF || used[i]&(1<<j)) continue;
			dep[j]=dep[i]+1;
			dfs(t+1,sum+dis[i][j]*dep[i],HASH+dep[j]*Base[j],Las,mark|(1<<(j-1)));
			used[i]|=1<<j;
		}
		used[i]=0;
		Las|=1<<i;
	}
}
void solve(){
	init();
	ans=INF;
	int i;
	for (i=1;i<=n;i++){
		dep[i]=1;
		mp.clear();
		dfs(1,0,Base[i],0,1<<(i-1));
	}
	printf("%d\n",ans);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(dis,63,sizeof(dis));
	scanf("%d%d",&n,&m);
	int i;
	for (i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		dis[x][y]=min(dis[x][y],z);
		dis[y][x]=min(dis[y][x],z);
	}
	solve();
	return 0;
}

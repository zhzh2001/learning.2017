#include <stdio.h>
#include <string.h>
#define LoveLive long long
#define maxq 1500000
#define inf 0x3f3f3f3f3f3f3f3fLL
LoveLive n, m, dis[13][13], head, tail, x, y, z, ans = inf, S, Q[13], D[13];
struct Status {LoveLive s, dep[13], cost; };
Status q[maxq]; bool flag = 1;
LoveLive min(LoveLive a, LoveLive b) {return a > b ? b : a; }
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	memset(dis, 0x3f, sizeof dis);
	scanf("%lld%lld", &n, &m);
	for (LoveLive i = 1; i <= m; i++) {
		scanf("%lld%lld%lld", &x, &y, &z); 
		if (i != 1 && S != z) flag = 0; else if (i == 1) S = z;
		dis[y][x] = dis[x][y] = min(dis[x][y], z);
	}
	if (flag) {
		for (int i = 1; i <= n; i++) {
			Q[head = tail = 0] = i; D[i] = 0;
			LoveLive s = 1 << i - 1, cost = 0;
			while (head <= tail) {
				LoveLive t = Q[head++];
				for (int j = 1; j <= n; j++)
					if (dis[t][j] != inf && !(s & 1 << j - 1)) Q[++tail] = j, cost += D[j] = D[t] + 1, s += 1 << j - 1;
			}
			if (cost < ans) ans = cost;
		}
		printf("%lld\n", ans * S);
	} else {
		head = tail = 1;
		for (LoveLive i = 1; i <= n; i++) {
			Status t; t.s = 1 << i - 1; t.dep[i] = 0; t.cost = 0;
			q[tail++] = t;
		}
		while (head != tail) {
			Status t = q[head++]; if (head >= maxq) head -= maxq;
			for (LoveLive i = 1; i <= n; i++) 
				for (LoveLive j = 1; j <= n; j++) if (dis[i][j] != inf && t.s & 1 << i - 1 && !(t.s & 1 << j - 1)) {
					Status t2 = t; t2.s += 1 << j - 1;
					t2.dep[j] = t2.dep[i] + 1;
					t2.cost += t2.dep[j] * dis[i][j];
					if (t2.s == (1 << n) - 1) {
						if (ans > t2.cost) {ans = t2.cost; }
					} else {
						q[tail++] = t2; if (tail >= maxq) tail -= maxq;
					}
				}
		}
		printf("%lld\n", ans);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include <stdio.h>
#include <math.h>
#define LoveLive long long
LoveLive tc, n, h, r, yes;
LoveLive x[1010], y[1010], z[1010], temp6;
LoveLive q[1010], head, tail, vis[1010];
LoveLive sqr(LoveLive x) {return x * x; }
double dis(LoveLive i, LoveLive j) {
	return sqrt(sqr(x[i] - x[j]) + sqr(y[i] - y[j]) + sqr(z[i] - z[j]));
}
int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%lld", &tc);
	while (tc--) {
		scanf("%lld%lld%lld", &n, &h, &r); yes = 0;
		for (LoveLive i = 1; i <= n; i++)
			scanf("%lld%lld%lld", &x[i], &y[i], &z[i]), vis[i] = 0;
		head = 1; tail = 0;
		for (LoveLive i = 1; i <= n; i++) {
			if (z[i] <= r) vis[i] = 1, q[++tail] = i;
		}
		while (head <= tail) {
			temp6 = q[head++];
			if (z[temp6] >= h - r) {
				yes = 1; break;
			}
			for (LoveLive i = 1; i <= n; i++) {
				if (!vis[i] && dis(temp6, i) <= r * 2) {
					vis[i] = 1; q[++tail] = i;
				}
			}
		}
		puts(yes ? "Yes" : "No");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

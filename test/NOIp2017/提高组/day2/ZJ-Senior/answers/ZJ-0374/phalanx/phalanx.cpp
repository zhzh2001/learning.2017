#include <stdio.h>
#define LoveLive long long
LoveLive n, m, q;
LoveLive flag = 1;
LoveLive x[301000], y[301000];
LoveLive sums[301000], num[1010][1010];
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%lld%lld%lld", &n, &m, &q);
	for (LoveLive i = 1; i <= q; i++) scanf("%lld%lld", &x[i], &y[i]), flag &= x[i] == 1;
	if (n <= 1000 && m <= 1000 && (n + m) * q <= 200000000) {
		for (LoveLive i = 1; i <= n; i++) for (LoveLive j = 1; j <= m; j++) num[i][j] = (i - 1) * m + j;
		for (LoveLive i = 1; i <= q; i++) {
			LoveLive t = num[x[i]][y[i]];
			for (LoveLive j = y[i]; j < m; j++) num[x[i]][j] = num[x[i]][j + 1];
			for (LoveLive j = x[i]; j < n; j++) num[j][m] = num[j + 1][m];
			num[n][m] = t;
			printf("%lld\n", t);
		}
	} else if (flag) {
		for (LoveLive i = 1; i <= q; i++) {
			LoveLive res = 0;
			for (LoveLive j = y[i]; j; j -= j & -j) res += sums[j];
			res %= n + m - 1;
			LoveLive tx = x[i], ty = y[i];
			if (res >= n - ty) res -= n - ty, ty = n; else ty += res, res = 0;
			if (res >= n - 1) tx = n, ty = n, res -= n - 1; else tx += res, res = 0;
			if (res) tx = 1, ty = res, res = 0;
			printf("%lld\n", (tx - 1) * m + ty); 
			for (LoveLive j = y[i]; j <= m; j += j & -j) sums[j]++;
		}
	} else {
		for (LoveLive now = 1; now <= q; now++) {
			LoveLive tx = x[now], ty = y[now];
			for (LoveLive i = now - 1; i; i--) {
				if (tx == n && ty == m) tx = x[i], ty = y[i];
				else if (ty == m && x[i] <= tx) tx++;
				else if (tx == x[i] && ty >= y[i]) ty++;
			}
			printf("%lld\n", (tx - 1) * m + ty);
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

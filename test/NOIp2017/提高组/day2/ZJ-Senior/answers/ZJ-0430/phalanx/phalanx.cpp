#include<iostream>
#include<cstdio>
using namespace std;

inline int read()
{
	int x = 0,w = 1;char c = getchar();
	while (c < '0' || c > '9'){if (c == '-') w = -1;c = getchar();}
	while (c >= '0' && c <= '9') x = (x << 3) + (x << 1) + c - '0', c = getchar();
	return x * w;
}

int n,m,q,mp[1005][1005];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read(),m = read(),q = read();
	int tmp = 0;
	for (int i = 1; i <= n; ++i)
	for (int j = 1; j <= m; ++j) mp[i][j] = ++tmp;
	for (int t = 0; t < q; ++t)
	{
		int x = read(),y = read(),val = mp[x][y];
		printf("%d\n",val);
		for (int i = y; i < m; ++i) mp[x][i] = mp[x][i + 1];
		for (int i = x; i < n; ++i) mp[i][m] = mp[i + 1][m];
		mp[n][m] = val;
	}
	
	return 0;
}

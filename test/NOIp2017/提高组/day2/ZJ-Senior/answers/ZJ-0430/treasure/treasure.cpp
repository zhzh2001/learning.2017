#include<iostream>
#include<cstdio>
#include<cstring>
#define inf 0x3f3f3f3f
#define cur edge[i].v
#define lt edge[i].len
#define nxt edge[i].next
#define Maxn 15
#define Maxm 2005
using namespace std;

inline int read()
{
	int x = 0,w = 1;char c = getchar();
	while (c < '0' || c > '9'){if (c == '-') w = -1;c = getchar();}
	while (c >= '0' && c <= '9') x = (x << 3) + (x << 1) + c - '0',c = getchar();
	return x * w;
}

struct E
{
	int v,len,next;
}edge[Maxm];

int pt = -1,head[Maxn],n,m;

void add_edge(int u,int v,int l)
{
	edge[++pt].next = head[u];
	head[u] = pt;
	edge[pt].v = v,edge[pt].len = l;
}

struct H
{
	int p,dep,val;
}heap[Maxn];

int ans = inf,dis[Maxn],t,h[Maxn];
bool vis[Maxn];

void shift_down(int x)
{
	while (x * 2 <= t && dis[heap[x * 2].p] < dis[heap[x].p] || x * 2 + 1 <= t && dis[heap[x * 2 + 1].p] < dis[heap[x].p])
	{
		if (x * 2 <= t && dis[heap[x * 2].p] < dis[heap[x].p] && (x * 2 + 1 > t || dis[heap[x * 2].p] <= dis[heap[x * 2 + 1].p]))
		{
			H tmp = heap[x];
			heap[x] = heap[x * 2],heap[x * 2] = tmp;
			x <<= 1;
			continue;
		}
		if (x * 2 + 1 <= t && dis[heap[x * 2 + 1].p] < dis[heap[x].p] && dis[heap[x * 2 + 1].p] <= dis[heap[x * 2].p])
		{
			H tmp = heap[x];
			heap[x] = heap[x * 2 + 1],heap[x * 2 + 1] = tmp;
			x = x * 2 + 1;
		}
	}
	h[heap[x].p] = x;
}

void shift_up(int x)
{
	while (x > 1 && dis[heap[x / 2].p] > dis[heap[x].p])
	{
		H tmp = heap[x];
		heap[x] = heap[x / 2],heap[x / 2] = tmp;
		x >>= 1; 
	}
	h[heap[x].p] = x;
}

H pop()
{
	H tmp = heap[1];
	heap[1] = heap[t],heap[t] = tmp;
	--t;
	shift_down(1);
	return tmp;
}

void push(H x)
{
	heap[++t] = x;
	shift_up(t);
}

int mp[Maxn][Maxn],dep[Maxn];

void dfs(int x,int tans)
{
	if (tans > ans) return;
	if (x == n)
	{
		ans = tans;
		return;
	}
	for (int u = 1; u <= n; ++u)
		if (vis[u])
			for (int i = head[u]; i != -1; i = nxt)
				if (!vis[cur])
				{
					vis[cur] = 1,dep[cur] = dep[u] + 1;
					dfs(x + 1,tans + lt * dep[u]);
					vis[cur] = 0;
				}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n = read(),m = read();
	for (int i = 1; i <= n; ++i) head[i] = -1;
	memset(mp,inf,sizeof(mp));
	for (int i = 0; i < m; ++i)
	{
		int u = read(),v = read(),l = read();
		mp[u][v] = mp[v][u] = min(mp[u][v],l);
	}
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= n; ++j)
			if (mp[i][j] != inf) add_edge(i,j,mp[i][j]);
	if (n > 9)
	{
		for (int i = 1; i <= n; ++i)
		{
			int tans = 0;
			memset(dis,inf,sizeof(dis));
			memset(vis,0,sizeof(vis));
			int tot = 0;
			t = 1;
			heap[1].p = i,heap[1].dep = 1,heap[1].val = 0,dis[i] = 0,vis[i] = 1,h[i] = 1;
			while (tot < n && t)
			{
				H u = pop();
				++tot;
				tans += u.val;
				for (int i = head[u.p]; i != -1; i = nxt)
					if (dis[cur] > dis[u.p] + lt * u.dep)
						{
							dis[cur] = dis[u.p] + lt * u.dep;
							if (!vis[cur])
							{
								H tmp;
								tmp.p = cur,tmp.dep = u.dep + 1,vis[cur] = 1,tmp.val = lt * u.dep;
								push(tmp);
							}
							else
							{
								heap[h[cur]].dep = u.dep + 1;
								heap[h[cur]].val = lt * u.dep;
								shift_up(h[cur]);
							}
						}
			}
			ans = min(ans,tans);
		}
	}
	else
	{
		for (int i = 1; i <= n; ++i)
		{
			memset(dis,inf,sizeof(dis));
			memset(vis,0,sizeof(vis));
			dis[i] = 0,vis[i] = 1,dep[i] = 1;
			dfs(1,0);
		}
	}
	printf("%d",ans);
	
	return 0;
}

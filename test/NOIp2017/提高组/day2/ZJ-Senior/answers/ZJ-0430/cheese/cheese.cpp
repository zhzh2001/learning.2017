#include<iostream>
#include<cstdio>
#include<cmath>
#define ll long long
#define Maxn 1005
using namespace std;

inline ll read()
{
	ll x = 0,w = 1;char c = getchar();
	while (c < '0' || c > '9'){if (c == '-') w = -1;c = getchar();}
	while (c >= '0' && c <= '9') x = (x << 3) + (x << 1) + c - '0',c = getchar();
	return x * w;
}

struct A
{
	ll x,y,z;
}a[Maxn];

int fa[Maxn];

int find(int x)
{
	return fa[x] = fa[x] == x ? x : find(fa[x]);
}

double calc(int i,int j)
{
	return sqrt((a[i].x - a[j].x) * (a[i].x - a[j].x) + (a[i].y - a[j].y) * (a[i].y - a[j].y) + (a[i].z - a[j].z) * (a[i].z - a[j].z));
}

ll n,h,r;

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t = read() + 1;
	while (--t)
	{
		n = read(),h = read(),r = read();
		for (int i = 0; i <= n + 1; ++i) fa[i] = i;
		for (int i = 1; i <= n; ++i)
		{
			a[i].x = read(),a[i].y = read(),a[i].z = read();
			if (find(i) != find(0) && a[i].z <= r) fa[find(i)] = find(0);
			if (find(i) != find(n + 1) && a[i].z + r >= h) fa[find(i)] = find(n +  1);
			for (int j = 1; j < i; ++j)
				if (find(i) != find(j) && calc(i,j) <= r * 2) fa[find(i)] = find(j);
		}
		if (find(0) == find(n + 1)) printf("Yes\n");
		else printf("No\n");
	}
	
	return 0;
}

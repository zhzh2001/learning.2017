var
  edgenum,t,cas,n,h,r,i,j:longint;
  flag:array[0..10000] of boolean;
  x,y,z,head,next,val,vet:array[0..1000000] of longint;
  f:boolean;
procedure dfs(u:longint);
var i,v:longint;
begin
  i:=head[u];
  flag[u]:=true;
  if abs(h-z[u])<=r then f:=true;
  while i<>0 do
  begin
    v:=vet[i];
    if flag[v]=false then dfs(v);
    i:=next[i];
  end;
end;
procedure add(u,v:longint);
begin
  inc(edgenum);
  vet[edgenum]:=v;
  next[edgenum]:=head[u];
  head[u]:=edgenum;
end;
begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  readln(t);
  for cas:=1 to t do
  begin
    readln(n,h,r);
    for i:=1 to n do
    begin
      head[i]:=0;flag[i]:=false;
    end;     edgenum:=0;
    for i:=1 to n do readln(x[i],y[i],z[i]);
    for i:=1 to n do
      for j:=1 to n do
      begin
        if ((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])<=4*r*r) and (i<>j) then
        begin add(i,j); end;
      end;
      f:=false;
    for i:=1 to n do
    if (flag[i]=false) and (z[i]<=r) then dfs(i);
    if f=false then writeln('No')
    else writeln('Yes');
  end;
  close(input);close(output);
end.
var
  i,j,n,m,u,v,w,q,min:longint;
  f:array[0..100,0..100] of longint;
  a,time:array[0..100] of longint;
  used:array[0..1000] of boolean;
procedure dfs2(t,sum:longint);
var i,j,minn,q:longint;
begin
 if sum>=min then exit;
  if t>n then
  begin
    if sum<min then min:=sum;
    exit;
  end;
  minn:=maxlongint;
  for j:=1 to t-1 do
  for i:=1 to n do
  if (used[i]=false) and (f[a[j],i]<>maxlongint) and (time[a[j]]*f[a[j],i]<minn) then
  begin
    minn:=time[a[j]]*f[a[j],i];
    time[i]:=time[a[j]]+1;
    a[t]:=i;
    q:=i;
  end;
  used[q]:=true;
  dfs2(t+1,sum+minn);
  used[q]:=false;
end;
function calc:longint;
var i,j,t,min:longint;
    c:array[0..10000] of longint;
begin
  t:=0; calc:=0;
  for i:=1 to n do
  if used[i]=false then
  begin
    inc(t);
    c[t]:=i;
  end;
  min:=maxlongint;
  for i:=1 to t do
    for j:=1 to t do
    if i<>j then
    begin
      if f[c[i],c[j]]<min then
      begin
        min:=f[c[i],c[j]];
      end;
    end;
   exit(min*(t-1))
end;
procedure dfs(t,sum:longint);
var i,j,minn,q,k:longint;
begin
 //writeln(t,' ',u,' ',sum);
 if sum+calc>min then exit;
  if t>n then
  begin
    if sum<min then min:=sum;
    exit;
  end;
  for i:=1 to n do
  begin
  minn:=maxlongint;
  for j:=1 to t-1 do
  if (used[i]=false) then
  begin
    if f[a[j],i]<>maxlongint then
    begin
      //if (n>10) and (sum+time[a[j]]*f[a[j],i]>minn) and (time[a[j]]+1<=time[k]) then continue;
      time[i]:=time[a[j]]+1;

      used[i]:=true;
      a[t]:=i;
      dfs(t+1,sum+time[a[j]]*f[a[j],i]);
      used[i]:=false;
    end;
  end;
  end;
end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do f[i,j]:=maxlongint;
  for i:=1 to m do
  begin
    readln(u,v,w);
    if f[u,v]>w then f[u,v]:=w;
    f[v,u]:=f[u,v];
  end;
  min:=maxlongint;
   for i:=1 to n do
  begin
    a[1]:=i;
    for j:=1 to n do time[j]:=0;
    time[i]:=1;
    for j:=1 to n do used[j]:=false;
    used[i]:=true;
    dfs2(2,0);
  end;
  for i:=1 to n do
  begin
    a[1]:=i;
    for j:=1 to n do time[j]:=0;
    time[i]:=1;
    for j:=1 to n do used[j]:=false;
    used[i]:=true;
    dfs(2,0);
  end;
  writeln(min);
  close(input);close(output);
end.

#include<algorithm>
#include<cstdlib>
#include<cstdio>

using namespace std;

struct tree{
	int ls,rs,fix,sz;
	long long x;
};
struct ML{
	int x,y;
	long long ans;
};

const int MAXQ=505,MAXN=600005;

int n,m,q,nn,rt;
tree t[MAXN];
ML a[MAXQ];

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
long long get(int Time,int x,int y)
{
	if (!Time) return 1LL*(x-1)*m+y;
	else if (x==n&&y==m) return a[Time].ans;
	else if (y<m&&a[Time].x==x&&a[Time].y<=y) return get(Time-1,x,y+1);
	else if (y==m&&a[Time].x<=x) return get(Time-1,x+1,y);
	else return get(Time-1,x,y);
}
void work1()
{
	for (int i=1;i<=q;i++)
	{
		a[i].x=read(),a[i].y=read();a[i].ans=get(i-1,a[i].x,a[i].y);
		printf("%lld\n",a[i].ans);
	}
}
int Merge(int a,int b)
{
	if (a==0) return b;if (b==0) return a;
	if (t[a].fix<t[b].fix)
	{
		t[a].rs=Merge(t[a].rs,b);
		t[a].sz=(t[t[a].ls].sz+t[t[a].rs].sz);
		return a;
	}
	else
	{
		t[b].ls=Merge(a,t[b].ls);
		t[b].sz=(t[t[b].ls].sz+t[t[b].rs].sz);
		return b;
	}
}
pair<int,int> Split(int a,int x)
{
	if (x==0) return make_pair(a,0);
	int ls=t[a].ls,rs=t[a].rs;
	if (t[ls].sz==x) 
	{
		t[a].ls=0;t[a].sz=t[rs].sz+1;
		return make_pair(ls,a);
	}
	else if (t[ls].sz==x-1)
	{
		t[a].rs=0;t[a].sz=t[ls].sz+1;
		return make_pair(a,rs);
	}
	else if (t[ls].sz>x)
	{
		pair<int,int>A=Split(ls,x);
		t[a].ls=A.second;t[a].sz=t[t[a].ls].sz+t[t[a].rs].sz+1;
		return make_pair(A.second,a);
	}
	else
	{
		pair<int,int>A=Split(rs,x-t[ls].sz-1);
		t[a].rs=A.first;t[a].sz=t[t[a].ls].sz+t[t[a].rs].sz+1;
		return make_pair(a,A.first);
	}
}
void work2()
{
	for (int i=1;i<=m;i++)
	{
		nn++;t[nn].ls=t[nn].rs=0;t[nn].sz=1;t[nn].x=i;t[nn].fix=rand()%1000000007;
		if (i==1) rt=nn;else rt=Merge(rt,nn);
	}
	for (int i=2;i<=n;i++)
	{
		nn++;t[nn].ls=t[nn].rs=0;t[nn].sz=1;t[nn].x=1LL*m*i;t[nn].fix=rand()%1000000007;
		rt=Merge(rt,nn);
	}
	for (int i=1;i<=q;i++)
	{
		int x=read(),y=read();long long ned=(x==1?y:1LL*m*x);
		pair<int,int>A=Split(rt,ned-1);pair<int,int>B=Split(A.second,1);
		printf("%lld\n",t[B.first].x);
		rt=Merge(Merge(A.first,B.second),B.first);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (q<=500) work1();else work2();
	return 0;
}

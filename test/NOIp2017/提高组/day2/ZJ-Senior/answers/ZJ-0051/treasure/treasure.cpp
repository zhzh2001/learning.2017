#include<cstring>
#include<cstdio>

using namespace std;

int n,m,ans,maxs,w[15][15],g[270][270],f[270][270][15],INF=1061109567;

int read()
{
	int x=0;char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}
int Min(int x,int y) {return x<y?x:y;}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();maxs=(1<<n);
	memset(w,63,sizeof(w));
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read(),z=read();
		w[x][y]=Min(w[x][y],z);w[y][x]=Min(w[y][x],z);
	}
	memset(g,63,sizeof(g));
	for (int i=1;i<maxs;i++) for (int j=1;j<maxs;j++) if ((i&j)==0)
	{
		int res=0;bool fl=1;
		for (int jj=1;jj<=n;jj++) if (j&(1<<(jj-1)))
		{
			int miner=INF;
			for (int ii=1;ii<=n;ii++) if (i&(1<<(ii-1))) miner=Min(miner,w[ii][jj]);
			if (miner==INF) {fl=0;break;} else res+=miner;
		}
		if (fl) g[i][j]=res;
	}
	memset(f,63,sizeof(f));
	for (int i=1;i<=n;i++) f[1<<(i-1)][1<<(i-1)][0]=0;
	for (int nows=1;nows<maxs;nows++)
	  for (int lsts=nows;lsts;lsts=(nows&(lsts-1)))
	    for (int i=0;i<n;i++)
	      if (f[nows][lsts][i]<INF)
	        for (int nxts=1;nxts<maxs;nxts++)
	      	  if ((nows&nxts)==0&&g[lsts][nxts]<INF)
	      	    f[nows+nxts][nxts][i+1]=Min(f[nows+nxts][nxts][i+1],f[nows][lsts][i]+(i+1)*g[lsts][nxts]);
	ans=INF;
	for (int s=0;s<maxs;s++) for (int i=0;i<n;i++) ans=Min(ans,f[maxs-1][s][i]);
	printf("%d\n",ans);
	return 0;
}

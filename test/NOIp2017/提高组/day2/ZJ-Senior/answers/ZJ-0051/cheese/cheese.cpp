#include<cstring>
#include<cstdio>
#include<cmath>

using namespace std;

struct Pt{
	int x,y,z;
};

const int MAXN=1005;

bool bo[MAXN],vis[MAXN][MAXN];
int T,N,H,R,q[MAXN];
Pt a[MAXN];

int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-f;ch=getchar();}
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return f*x;
}
int jdz(int x) {return x>0?x:-x;}
long double Sqr(int x) {return 1.0*x*x;}
long double getd(int i,int j) {return sqrt(Sqr(a[i].x-a[j].x)+Sqr(a[i].y-a[j].y)+Sqr(a[i].z-a[j].z));}
bool bfs()
{
	memset(bo,0,sizeof(bo));int Hed=0,Tal=1;q[1]=0;bo[0]=1;
	while (Hed!=Tal)
	{
		Hed++;if (q[Hed]==N+1) return 1;
		for (int i=1;i<=N+1;i++)
		  if ((!bo[i])&&vis[q[Hed]][i])
		    Tal++,q[Tal]=i,bo[i]=1;
	}
	return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while (T--)
	{
		N=read(),H=read(),R=read();
		for (int i=1;i<=N;i++) a[i].x=read(),a[i].y=read(),a[i].z=read();
		memset(vis,0,sizeof(vis));
		for (int i=1;i<=N;i++) if (jdz(a[i].z-0)<=R) vis[0][i]=vis[i][0]=1;
		for (int i=1;i<N;i++) for (int j=i+1;j<=N;j++) if (getd(i,j)<=2*R) vis[i][j]=vis[j][i]=1;
		for (int i=1;i<=N;i++) if (jdz(H-a[i].z)<=R) vis[i][N+1]=vis[N+1][i]=1;
		if (bfs()) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

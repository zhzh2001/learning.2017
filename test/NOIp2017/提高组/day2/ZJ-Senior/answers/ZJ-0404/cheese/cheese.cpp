#include <bits/stdc++.h>
using std::abs;
typedef long long int64;
const int maxn=1024;
int n,h;
int64 cir[maxn][3],r;
bool vis[maxn];
int64 sqar(int64 x)
{
	return x*x;
}
int64 dists(int x,int y)
{
	return sqar(cir[x][0]-cir[y][0])+sqar(cir[x][1]-cir[y][1])+sqar(cir[x][2]-cir[y][2]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--)
	{
		memset(vis,false,sizeof vis);
		scanf("%d%d%lld",&n,&h,&r);
		std::queue<int> q;
		for(int i=1;i<=n;++i)
		{
			scanf("%lld%lld%lld",&cir[i][0],&cir[i][1],&cir[i][2]);
			if(abs(cir[i][2])<=r)
			{
				vis[i]=true;
				q.push(i);
			}
		}
		bool ok=false;
		while(!q.empty())
		{
			int x=q.front();
			q.pop();
			if(abs(cir[x][2]-h)<=r)
			{
				ok=true;
				break;
			}
			for(int i=1;i<=n;++i)
			{
				if(!vis[i]&&dists(x,i)<=sqar(r+r))
				{
					vis[i]=true;
					q.push(i);
				}
			}
		}
		puts(ok?"Yes":"No");
	}
}

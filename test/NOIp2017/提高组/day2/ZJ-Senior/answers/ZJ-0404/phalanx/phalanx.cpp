#include <bits/stdc++.h>
const int maxn=300005;
struct node
{
	int ch[2],sze,p,val;
};
node nodes[maxn*3];
int cnode;
struct spl
{
	int root;
	int go(int x,int w)
	{
		while(nodes[x].ch[w])
		{
			x=nodes[x].ch[w];
		}
		return x;
	}
	void rotate(int x)
	{
		int p=nodes[x].p,g=nodes[p].p,w=nodes[p].ch[1]==x;
		if(p==0)
			return; 
		int olds=nodes[p].sze;
		nodes[p].sze-=nodes[x].sze;
		nodes[p].sze+=nodes[nodes[x].ch[w^1]].sze;
		nodes[x].sze+=olds;
		nodes[p].ch[w]=nodes[x].ch[w^1];
		nodes[nodes[p].ch[w]].p=p;
		nodes[x].ch[w^1]=p;
		nodes[nodes[x].ch[w^1]].p=x;
		int wp=nodes[g].ch[1]==p;
		nodes[x].p=g;
		if(nodes[g].ch[wp]==p)
		{
			nodes[g].ch[wp]=x;
		}
	}
	void splay(int prt,int& rt,int x)
	{
		for(;nodes[x].p!=prt;rotate(x))
		{
			int p=nodes[x].p;
			if(p==prt)
				break;
			if((nodes[p].ch[0]==x)==(nodes[nodes[p].p].ch[0]==p))
				rotate(p);
			else
				rotate(x);
		}
		rt=x;
	}
	void query(int n)
	{
		for(int i=root;;)
		{
			int lc=nodes[nodes[i].ch[0]].sze;
			if(n<=lc)
				i=nodes[i].ch[0];
			else if(n==lc+1)
			{
				splay(0,root,i);
				return;
			}
			else
			{
				i=nodes[i].ch[1];
				n-=lc+1;
			}
		}
	}
	int find(int v)
	{
		int x=root;
		for(int xne=nodes[x].ch[v>=nodes[x].val];xne;x=xne,xne=nodes[x].ch[v>=nodes[x].val])
			if(nodes[x].val==v)
			{
				return x;
			}
		return nodes[x].val==v?x:0;
	}
	int findlow(int v)
	{
		int x=root;
		for(int xne=nodes[x].ch[v>=nodes[x].val];xne;x=xne,xne=nodes[x].ch[v>=nodes[x].val])
		{
			if(nodes[x].val==v)
			{
				return x;
			}
		}
		return x;
	}
	int countlow(int v)
	{
		int x=root,ans=0;
		while(x)
		{
			if(ans+nodes[nodes[x].ch[0]].sze+v<nodes[x].val)
			{
				x=nodes[x].ch[0];
			}
			else
			{
				x=nodes[x].ch[1];
				ans+=nodes[nodes[x].ch[0]].sze+1;
			}
		}
		return ans;
	}
	int delroot()
	{
		for(int i=0;i<2;++i)
			if(nodes[root].ch[i])
			{
				splay(root,nodes[root].ch[i],go(nodes[root].ch[i],i^1));
				int newr=nodes[root].ch[i],oldr=root;
				nodes[newr].ch[i^1]=nodes[root].ch[i^1];
				nodes[newr].sze+=nodes[nodes[root].ch[i^1]].sze;
				nodes[root].ch[0]=nodes[root].ch[1]=nodes[root].p=nodes[newr].p=0;
				root=newr;
				return oldr;
			}
		int oldr=root;
		root=0;
		return oldr;
	}
	void insert(int v,int xn) 
	{
		nodes[xn].val=v;
		nodes[xn].sze=1;
		if(root==0)
			root=xn;
		else
		{
			splay(0,root,findlow(v));
			if(nodes[root].ch[1]==0)
			{
				nodes[root].ch[1]=xn;
				nodes[xn].p=root;
				nodes[root].sze+=1;
				return;
			}
			else
			{
				splay(root,nodes[root].ch[1],go(nodes[root].ch[1],0));
				nodes[nodes[root].ch[1]].ch[0]=xn;
				nodes[xn].p=nodes[root].ch[1];
				nodes[root].sze+=1;
				nodes[nodes[root].ch[1]].sze+=1;
				return;
			}
		}
	}
	void append(int v,int xn)
	{
		nodes[xn].val=v;
		nodes[xn].sze=1;
		if(root==0)
		{
			root=xn;
			return;
		}
		if(nodes[root].ch[1]!=0)
			splay(0,root,go(nodes[root].ch[1],1));
		nodes[root].ch[1]=xn;
		nodes[xn].p=root;
		nodes[root].sze+=1;
	}
	int size()
	{
		return nodes[root].sze;
	}
};

spl spls[maxn];
spl rmd[maxn];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q;
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;++i)
	{
		spls[0].append(i*m,++cnode);
	}
	while(q--)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		if(y==m)
		{
			spls[0].query(x);
			int v=nodes[spls[0].root].val,xn=spls[0].root;
			spls[0].delroot();
			spls[0].append(v,xn);
			printf("%d\n",v);
			continue;
		}
		int cnt=m-1-spls[x].size();
		if(y>cnt)
		{
			spls[x].query(y-cnt);
			int v=nodes[spls[x].root].val,xn=spls[x].root;
			spls[x].delroot();
			spls[0].query(x);
			int v2=nodes[spls[0].root].val,xn2=spls[0].root;
			spls[0].delroot();
			spls[x].append(v2,xn2);
			spls[0].append(v,xn);
			printf("%d\n",v);
			continue;
		}
		y+=rmd[x].countlow(y);
		int v=(x-1)*m+y;
		rmd[x].insert(y,++cnode);
		spls[0].query(x);
		int v2=nodes[spls[0].root].val,xn2=spls[0].root;
		spls[0].delroot();
		spls[0].append(v,++cnode);
		spls[x].append(v2,xn2);
		printf("%d\n",v);
	}
}

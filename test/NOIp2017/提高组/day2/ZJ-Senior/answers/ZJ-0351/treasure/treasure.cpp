#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<cmath>
#include<queue>
#define ll long long

using namespace std;
const int N=20;
int n, m, x, y, v, sum, ans;
bool d[N][N], vis[N];
struct node {
	int x, t;
};

inline int read(int &x) {
	x=0;
	char c=getchar();
	while (c<'0' || c>'9')
		c=getchar();
	while (c>='0' && c<='9') {
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
}

void bfs(int x) {
	queue<node> q;
	q.push((node){x, 1});
	vis[x]=true;
	while (!q.empty()) {
		node tmp=q.front();
		q.pop();
		for (int i=1; i<=n; i++)
			if (d[tmp.x][i] && !vis[i]) {
				vis[i]=true;
				sum+=tmp.t*v;
				q.push((node){i, tmp.t+1});
			}
	}
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	read(n), read(m);
	memset(d, false, sizeof d);
	for (int i=1; i<=m; i++) {
		read(x), read(y), read(v);
		d[x][y]=d[y][x]=true;
	}
	ans=1e9+7;
	for (int i=1; i<=n; i++) {
		sum=0;
		memset(vis, false, sizeof vis);
		bfs(i);
		ans=min(ans, sum);
	}
	cout << ans << endl;
	return 0;
}

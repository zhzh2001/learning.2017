#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<cmath>
#define ll long long

using namespace std;
const int N=5007;
int n, m, q, x, y;
int a[N][N];

inline int read(int &x) {
	x=0;
	char c=getchar();
	while (c<'0' || c>'9')
		c=getchar();
	while (c>='0' && c<='9') {
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
}

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n), read(m), read(q);
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++)
			a[i][j]=(i-1)*m+j;
	for (int i=1; i<=q; i++) {
		read(x), read(y);
		int tmp=a[x][y];
		cout << tmp << endl;
		for (int i=y+1; i<=m; i++)
			a[x][i-1]=a[x][i];
		for (int i=x+1; i<=n; i++)
			a[i-1][m]=a[i][m];
		a[n][m]=tmp;
	}
	return 0;
}

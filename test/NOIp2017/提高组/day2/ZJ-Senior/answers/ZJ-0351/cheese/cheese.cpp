#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<cmath>
#define ll long long

using namespace std;
const int N=1007;
int T, n, h, r;
bool is_find;
int x[N], y[N], z[N];
bool vis[N];
vector<int> e[N];

inline int read(int &x) {
	x=0;
	bool flag=false;
	char c=getchar();
	while ((c<'0' || c>'9') && c!='-')
		c=getchar();
	if (c=='-') {
		c=getchar();
		flag=true;
	}
	while (c>='0' && c<='9') {
		x=(x<<1)+(x<<3)+c-'0';
		c=getchar();
	}
	if (flag)
		x=-x;
}

ll dist(int i, int j) {
	return (x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
}

void dfs(int x) {
	vis[x]=true;
	if (z[x]+r>=h) {
		is_find=true;
		return ;
	}
	for (int i=0; i<e[x].size(); i++)
		if (!vis[e[x][i]])
			dfs(e[x][i]);
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	read(T);
	while (T--) {
		memset(vis, false, sizeof vis);
		is_find=false;
		for (int i=0; i<=1000; i++)
			e[i].clear();
		read(n), read(h), read(r);
		for (int i=1; i<=n; i++)
			read(x[i]), read(y[i]), read(z[i]);
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++)
				if (i!=j && dist(i, j)<=4ll*r*r) {
					e[i].push_back(j);
					e[j].push_back(i);
				}
		for (int i=1; i<=n; i++)
			if (z[i]<=r && !vis[i] && !is_find)
				dfs(i);
		if (is_find)
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}

#include <cstdio>
#include <algorithm>
using namespace std;
int n,m,q,x,y,w,a[1006][1006],s[4000006],ss[4000006];
long long work(int x)
{
	if(x<=m)return 1LL*x;
	else return 1LL*m+1LL*(x-m)*m;
}
void build(int i,int l,int r)
{
	if(l==r){s[i]=l;ss[i]=1;return;}
	int mid=(l+r)>>1;
	build(i<<1,l,mid);
	build(i<<1|1,mid+1,r);
	ss[i]=ss[i<<1]+ss[i<<1|1];
}
int ask(int i,int l,int r,int x)
{
	if(l==r)return s[i];
	int mid=(l+r)>>1;
	if(x<=mid)return ask(i<<1,l,mid,x);
	else return ask(i<<1|1,mid+1,r,x);
}
void change(int i,int l,int r,int x,int y)
{
	if(l==r){
		s[i]=y;
		if(y==0)ss[i]=0;else ss[i]=1;
		return;
	}
	int mid=(l+r)>>1;
	if(x<=mid)change(i<<1,l,mid,x,y);
	else change(i<<1|1,mid+1,r,x,y);
	ss[i]=ss[i<<1]+ss[i<<1|1];
}
int get(int i,int l,int r,int x)
{
	if(l==r)return l;
	int mid=(l+r)>>1;
	if(ss[i<<1]>=x)return get(i<<1,l,mid,x);
	else return get(i<<1|1,mid+1,r,x-ss[i<<1]);
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=1000&&m<=1000){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)a[i][j]=(i-1)*m+j;
		while(q--){
			scanf("%d%d",&x,&y);
			int t=a[x][y];printf("%d\n",t);
			for(int i=y;i<m;i++)a[x][i]=a[x][i+1];
			for(int i=x;i<n;i++)a[i][m]=a[i+1][m];
			a[n][m]=t;
		}
	}else{
		w=n+m-1;build(1,1,w+q);
		for(int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			int t=get(1,1,w+q,y),v=ask(1,1,w+q,t);
			printf("%lld\n",work(v));
			change(1,1,w+q,w+i,v);change(1,1,w+q,t,0);
		}
	}
	return 0;
}

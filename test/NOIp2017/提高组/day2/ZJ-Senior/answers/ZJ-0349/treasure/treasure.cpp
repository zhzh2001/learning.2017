#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,z,x,y,ans,e[16][16],f[16][16][1<<12];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(e,6,sizeof e);
	memset(f,6,sizeof f);ans=100000000;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		e[x][y]=e[y][x]=min(e[x][y],z);
	}
	for(int i=0;i<=15;i++)
		for(int j=0;j<=15;j++)f[i][j][0]=0;
	for(int dep=n-1;dep>=1;dep--)
		for(int i=1;i<=n;i++)
			for(int w=1;w<(1<<n);w++){
				if((1<<(i-1))&w)continue;
				for(int j=1;j<=n;j++)if((1<<(j-1))&w)
					for(int k=(1<<(j-1));k<(1<<n);k=((k+1)|(1<<(j-1))))if((w&k)==k)
						f[i][dep][w]=min(f[i][dep][w],dep*e[i][j]+f[j][dep+1][k-(1<<(j-1))]+f[i][dep][w-k]);
			}
	for(int i=1;i<=n;i++)ans=min(ans,f[i][1][(1<<n)-1-(1<<(i-1))]);
	printf("%d\n",ans);
	return 0; 
}

#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 1006
using namespace std;
int T,n,h,r,s,t,cnt,f[N],head[N];
struct edg{int to,ne;}e[N*N];
struct arr{int x,y,z;}p[N];
void add(int x,int y)
{
	e[++cnt].to=y;e[cnt].ne=head[x];head[x]=cnt;
}
void dfs(int x)
{
	f[x]=1;
	for(int i=head[x];i;i=e[i].ne)
		if(!f[e[i].to])dfs(e[i].to);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r);s=n+1;t=n+2;cnt=0;
		memset(f,0,sizeof f);memset(head,0,sizeof head);
		for(int i=1;i<=n;i++)scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++){
				int x=p[i].x-p[j].x;
				int y=p[i].y-p[j].y;
				int z=p[i].z-p[j].z;
				long long v=1LL*x*x+1LL*y*y+1LL*z*z;
				if(v<=4LL*r*r)add(i,j),add(j,i);
			}
		for(int i=1;i<=n;i++){
			if(p[i].z<=r)add(s,i);
			if(p[i].z+r>=h)add(i,t);
		}
		dfs(s);
		if(f[t])puts("Yes");else puts("No");
	}
	return 0;
}

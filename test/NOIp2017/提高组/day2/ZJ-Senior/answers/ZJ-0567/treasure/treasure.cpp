#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define inf 0x3f3f3f3f
#define rep(x,y,z) for (int (x)=(y);(x)<=(z);x++)
#define cls(x) memset(x,0,sizeof(x))
typedef long long LL;
using namespace std;
template <typename T>
void read(T &x)
{
	x=0;
	char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch<='9'&&ch>='0') {x=x*10+ch-'0';ch=getchar();}
}
template <typename T>
void read(T &x,T &y){read(x);read(y);}
template <typename T>
void read(T &x,T &y,T &z){read(x);read(y);read(z);}
LL max(const LL &x,const LL &y){return x>y?x:y;}
LL min(const LL &x,const LL &y){return x<y?x:y;}
int n,m;
const int maxn=22;
const int maxm=22;
int ma[maxn][maxn];
//int depth[1<<14][maxn];
LL f[1<<14];
struct GRAPH
{
	int nxt[maxm],first[maxn],to[maxm],cnt;
	bool vis[maxn];
	void init()
	{
		cnt=0;
		cls(first);	
		cls(vis);
	}
	void addedge1(int x,int y)
	{
		to[++cnt]=y;
		nxt[cnt]=first[x];
		first[x]=cnt;
	}
	void addedge(int x,int y)
	{
		addedge1(x,y);	
		addedge1(y,x);
	}
	int depth(int beg,int which,int step)
	{
		if (beg==which) return step;
		for (int q=first[beg];q;q=nxt[q])
		{
			int temp=depth(to[q],which,step+1);	
			if (temp!=-1) return temp;
		}
		return -1;
	}
	void dfsout(int x)
	{
		for (int q=first[x];q;q=nxt[q])
		{
			printf("%d %d %d\n",x,to[q],ma[x][to[q]]);
			dfsout(to[q]);
		}
	}
}tree[1<<14];

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n,m);
	memset(ma,0x3f,sizeof(ma));
	rep(i,1,m) 
	{
		int u,v,w;
		read(u,v,w);
		if (ma[u][v]>w) ma[u][v]=ma[v][u]=w;
	}
	LL ans=0x3f3f3f3f3f3f3f3f;
	int id=0;
	rep(i,1,n) //current i is the very beginning point
	{
		memset(f,0x3f,sizeof(f));
		//cls(depth);
		//depth[i]=1;
		rep(j,1,(1<<n)-1) {cls(tree[j].first);tree[j].cnt=0;}
		f[1<<(i-1)]=0;
		rep(s,1,(1<<(n))-1)
		{
			for (int now=1,j=1;j<=n;now<<=1,j++)
			{
				if ((s&now)&&f[s^now]!=0x3f3f3f3f3f3f3f3f)
				{
					for (int now2=1,k=1;k<=n;now2<<=1,k++)//from k to j
					{
						if ((s&now2)&&(j!=k)&&ma[j][k]!=inf) 
						{
							LL temp=f[s^now]+(LL)tree[s^now].depth(i,k,1)*ma[j][k];
							if (f[s]>temp)
							{
								f[s]=temp;
								tree[s]=tree[s^now];
								tree[s].addedge1(k,j);
								//addedge k->j
							}
						}
					}
				}
			}
		}
		if (ans>f[(1<<(n))-1]) 
		{
			ans=f[(1<<(n))-1];
			id=i;
//tree[(1<<(n))-1].dfsout(id);
//putchar('\n');
		}
	}
//printf("id=%d\n",id);
	printf("%lld\n",ans);
	return 0;
}


#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cstring>
#define rep(x,y,z) for (int (x)=(y);(x)<=(z);x++)
#define per(x,y,z) for (int (x)=(y);(x)>=(z);x--)
#define cls(x) memset(x,0,sizeof(x))
typedef long long LL;
struct PAIR
{
	int first,second;
};
using namespace std;
template <typename T>
void read(T &x)
{
	x=0;
	char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch<='9'&&ch>='0') {x=x*10+ch-'0';ch=getchar();}
}
template <typename T>
void read(T &x,T &y){read(x);read(y);}
template <typename T>
void read(T &x,T &y,T &z){read(x);read(y);read(z);}
int n,m,q;
const int maxq=111333;
PAIR qq[maxq];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n,m,q);
	//if (q<=3000)
	{
		rep(i,1,q)
		{
			read(qq[i].first,qq[i].second);
			PAIR now=qq[i];
			per(j,i-1,1)
			{
				if (now.first==n&&now.second==m) now=qq[j];
				else if (now.second==m)
				{
					if (qq[j].first<=now.first)
					{
						now.first++;
					}
				}
				else
				{
					if (qq[j].first==now.first)
					{
						if (qq[j].second<=now.second) now.second++;
					}
				}
			}
			printf("%lld\n",(LL)(now.first-1)*m+now.second);
		}
	}
	return 0;
}


#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define rep(x,y,z) for (int (x)=(y);(x)<=(z);x++)
#define cls(x) memset(x,0,sizeof(x))
typedef long long LL;
using namespace std;
template <typename T>
void read(T &x)
{
	x=0;
	char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch<='9'&&ch>='0') {x=x*10+ch-'0';ch=getchar();}
}
template <typename T>
void read(T &x,T &y){read(x);read(y);}
template <typename T>
void read(T &x,T &y,T &z){read(x);read(y);read(z);}
const int maxn=1111,maxm=2222222;
int n,T,h,r;
struct POINT
{
	int x,y,z;
}p[maxn];
struct GRAPH
{
	int nxt[maxm],first[maxn],to[maxm],cnt;
	bool vis[maxn];
	void init()
	{
		cnt=0;
		cls(first);	
		cls(vis);
	}
	void addedge1(int x,int y)
	{
		to[++cnt]=y;
		nxt[cnt]=first[x];
		first[x]=cnt;
	}
	void addedge(int x,int y)
	{
		addedge1(x,y);	
		addedge1(y,x);
	}
	bool dfs(int x)
	{
		if (x==n+2) return true;
		vis[x]=true;
		bool res=0;
		for (int q=first[x];q;q=nxt[q]) if (!vis[to[q]]) 
		{
			res|=dfs(to[q]);
			if (res) return true;
		}
		return false;
	}
}graph;
LL sqr(const LL &x)
{
	return x*x;
}
bool judge(const int &x,const int &y)
{
	//p[i] & p[y] if 
	LL l=sqr(p[x].x-p[y].x)+sqr(p[x].y-p[y].y)+sqr(p[x].z-p[y].z),rr=sqr(r)<<2;
	return l<=rr;
}
bool judge2(const int &x)
{
	return h-p[x].z<=r;
}
bool judge3(const int &x)
{
	return p[x].z<=r;	
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--)
	{
		graph.init();
		read(n,h,r);
		//rep(i,1,n) read(p[i].x,p[i].y,p[i].z);
		rep(i,1,n) scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
		rep(i,1,n)
			rep(j,i+1,n) if (judge(i,j)) graph.addedge(i,j);
		rep(i,1,n) if (judge2(i)) graph.addedge(i,n+1);//up
		rep(i,1,n) if (judge3(i)) graph.addedge(i,n+2);
		printf("%s\n",graph.dfs(n+1)?"Yes":"No");
	}
	return 0;
}


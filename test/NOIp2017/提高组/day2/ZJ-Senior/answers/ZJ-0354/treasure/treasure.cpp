#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long ll;
const int MAXN=12+5;
const int MAXM=1000+5;
int n,m,H[MAXN];
ll ma[MAXN][MAXN],dpt[MAXN],que[MAXN],qt=0,qw=-1,vis[MAXN];
void init()
{
	memset(ma,-1,sizeof(ma));
	scanf("%d%d",&n,&m);
	int x,y;ll v;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%lld",&x,&y,&v);
		if (ma[x][y]==-1)
			ma[x][y]=ma[y][x]=v;
		else
			ma[x][y]=ma[y][x]=min(ma[x][y],v);
	}
}
void dfs(int np,int pa,int dep)
{
	dpt[np]=dep;
	for (int i=1;i<=n;i++)
		if (i!=np&&i!=pa&&ma[np][i]!=-1)
			dfs(i,np,dep+1);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	init();
	ll ggv;int ednum=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<i;j++)
			if (ma[i][j]!=-1)
			{
				ednum++;
				ggv=ma[i][j];
			}
	if (ednum==n-1)
	{	
		ll ans=-1,num=-1;
		for (int i=1;i<=n;i++)
		{
			dfs(i,0,0);
			ll nowans=0;
			for (int j=1;j<=n;j++)
				if (i!=j)
					nowans+=dpt[j]*ggv;
			if (ans==-1)
				ans=nowans,num=i;
			else
				ans=min(ans,nowans);
		}
		printf("%lld\n",ans);
	}
	else
	{
		ll ans=-1,num=-1;
		for (int i=1;i<=n;i++)
		{
			ll nowans=0;
			memset(vis,0,sizeof(vis));
			qt=0;qw=-1;
			que[++qw]=i;
			vis[i]=1;
			while (qt<=qw)
			{
				int np=que[qt%MAXN];
				for(int j=1;j<=n;j++)
					if (ma[np][j]!=-1&&vis[j]==0)
					{
						que[(++qw)%MAXN]=j;
						vis[j]=vis[np]+1;
					}
				qt++;
			}
			for (int j=1;j<=n;j++)
				if (j!=i)
					nowans+=ggv*(vis[j]-1);
			if (ans==-1)
				ans=nowans;
			else
				ans=min(ans,nowans);
		}
		printf("%lld\n",ans);
	}
	return 0;
}

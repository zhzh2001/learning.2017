#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
typedef long long ll;
const int MAXN=1000+8;
struct bal
{
	ll x,y,z;
}kd[MAXN];
struct edge
{
	int to,nex;
}E[2*MAXN*MAXN];
int H[MAXN],T,vis[MAXN];
int cnted;
ll n,h,r;
void init()
{
	memset(H,0,sizeof(H));
	memset(vis,0,sizeof(vis));
	scanf("%lld%lld%lld",&n,&h,&r);
	for (int i=3;i<=n+2;i++)
		scanf("%lld%lld%lld",&kd[i].x,&kd[i].y,&kd[i].z);
	cnted=0;
}
void addedge(int f,int t)
{
	E[++cnted].to=t;E[cnted].nex=H[f];H[f]=cnted;
	E[++cnted].to=f;E[cnted].nex=H[t];H[t]=cnted;
}
double dis(int a,int b)
{
	double ans=sqrt((kd[a].x-kd[b].x)*(kd[a].x-kd[b].x)+(kd[a].y-kd[b].y)*(kd[a].y-kd[b].y)+(kd[a].z-kd[b].z)*(kd[a].z-kd[b].z));
	return ans;
}
void build_graph()
{
	for (int i=3;i<=n+2;i++)
	{
		if (kd[i].z<=r) addedge(1,i);//xia di mian shi 1 hao
		if (h-kd[i].z<=r) addedge(2,i);//shang mian shi 2 hao
	}
	for (int i=3;i<=n+2;i++)
		for (int j=3;j<i;j++)
			if (dis(i,j)<=2*r)
				addedge(i,j);
}
void dfs(int np)
{
	for (int ed=H[np];ed!=0;ed=E[ed].nex)
		if (vis[E[ed].to]==0)
		{
			vis[E[ed].to]=1;
			dfs(E[ed].to);
		}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		init();
		build_graph();
		vis[1]=1;
		dfs(1);
		if (vis[2]==1)
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}

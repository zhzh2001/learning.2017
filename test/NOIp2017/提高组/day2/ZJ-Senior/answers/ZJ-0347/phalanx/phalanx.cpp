#include <iostream>
#include <cstdio>
#include <cstring>
#define maxn 5050
using namespace std;
int n, m, q, x, y, ptr[maxn][maxn];
inline int ID(int i, int j) { return (i - 1) * m + j; }

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	for (int i = 1; i <= n; ++i) for (int j = 1; j <= m; ++j) ptr[i][j] = ID(i, j);
	while (q--) {
		scanf("%d%d", &x, &y);
		printf("%d\n", ptr[x][y]);
		for (int i = y + 1; i <= m; ++i) swap(ptr[x][i - 1], ptr[x][i]);
		for (int i = x + 1; i <= n; ++i) swap(ptr[i - 1][m], ptr[i][m]);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

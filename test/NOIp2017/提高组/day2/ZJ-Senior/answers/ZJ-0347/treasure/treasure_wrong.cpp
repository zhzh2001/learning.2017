#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 20
#define M 1010
#define INF 100000007
using namespace std;
int n, m, u[M], v[M], w[M];
int fa[N], r[M];
bool vis[N][N];
bool cmp(const int i, const int j) { return w[i] < w[j]; }
inline int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }
struct Edge {
	int to, nxt, w;
} e[M << 2];
int tot = 1, head[N];
inline void AddEdge(int u, int v, int w)
{
	e[tot] = (Edge) {v, head[u], w}, head[u] = tot++;
	e[tot] = (Edge) {u, head[v], w}, head[v] = tot++;
}

int pa[N], sz[N], dep[N], son[N], top[N];
long long dis[N];
inline void dfs1(int u, int f, long long d)
{
	pa[u] = f; dis[u] = d; sz[u] = 1; son[u] = 0;
	for (int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		if (v != f) {
			dep[v] = dep[u] + 1;
			dfs1(v, u, (long long)d + e[i].w);
			sz[u] += sz[v];
			if (sz[v] > sz[son[u]]) son[u] = v;
		}
	}
}

inline void dfs2(int u, int tp)
{
	top[u] = tp;
	if (son[u]) dfs2(son[u], tp);
	for (int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		if (v != pa[u] && v != son[u]) dfs2(v, v);
	}
}

inline int query(int x, int y)
{
	while (top[x] != top[y]) {
		if (dep[top[x]] < dep[top[y]]) swap(x, y);
		x = fa[top[x]];
	}
	if (dep[x] > dep[y]) swap(x, y);
	return x;
}

inline long long solve(int i)
{
	long long ret = 0;
	for (int i = 1; i <= n; ++i) {
		dep[i] = 1; dfs1(i, 0, 0); dfs2(i, i);
		memset(vis, 0, sizeof(vis));
		for (int j = 1; j <= n; ++j) {
			if (i == j) continue;
			if (vis[i][j] || vis[j][i]) continue;
			int lca = query(i, j);
			ret += (long long)dis[i] + dis[j] - 2 * dis[lca];
			vis[i][j] = vis[j][i] = 1;
		}
	}
	return ret;
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m); for (int i = 1; i <= n; ++i) fa[i] = i;
	for (int i = 1; i <= m; ++i) scanf("%d%d%d", &u[r[i] = i], &v[i], &w[i]);
	sort(r + 1, r + m + 1);
	for (int i = 1; i <= m; ++i) {
		int e = r[i], x = find(u[e]), y = find(v[e]);
		if (x != y) {
			AddEdge(u[e], v[e], w[e]);
			fa[x] = y;
		}
		if (find(1) == find(n)) break;
	}
	long long ans = INF;
	for (int i = 1; i <= n; ++i) ans = min(ans, solve(i));
	printf("%lld", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

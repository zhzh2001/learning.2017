#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
#include <map>
#define N 15
#define M 1010
#define INF 100000007
using namespace std;
int n, m, u[M], v[M], d[N];
long long w[M];
int fa[N], r[M];
bool cmp(const int i, const int j) { return w[i] < w[j]; }
inline int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }
struct Edge {
	int to, nxt;
} e[M << 2];
map<pair<int, int>, int> ms; int cnt = 0;
int tot = 1, head[N];
inline void AddEdge(int u, int v)
{
	e[tot] = (Edge) {v, head[u]}, head[u] = tot++;
	e[tot] = (Edge) {u, head[v]}, head[v] = tot++;
}

inline void bfs(int op)
{
	queue<int> q; memset(d, -1, sizeof(d));
	d[op] = 0; q.push(op);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if (d[v] == -1) {
				d[v] = d[u] + 1;
				q.push(v);
			}
		}
	}
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure_ano.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i) {
		long long _w = 0;
		scanf("%d%d%lld", u + i, v + i, &_w); AddEdge(u[i], v[i]);
		if (!ms.find(make_pair(min(u[i], v[i]), max(u[i], v[i])))) {
			w[i] = _w;
			ms[make_pair(min(u[i], v[i]), max(u[i], v[i]))] = i;
		} else w[find(make_pair(min(u[i], v[i]), max(u[i], v[i])))] = min(w[find(make_pair(min(u[i], v[i]), max(u[i], v[i])))], _w);
	}
	long long ans = INF;
	for (int op = 1; op <= n; ++op) {
		long long ret = 0; bfs(op);
		for (int i = 1; i <= n; ++i) fa[i] = i;
		for (int i = 1; i <= m; ++i) r[i] = i;
		for (int i = 1; i <= m; ++i) w[i] *= (d[u[i]] + 1);
		sort(r + 1, r + m + 1, cmp);
		for (int i = 1; i <= m; ++i) {
			int e = r[i]; int x = find(u[e]), y = find(v[e]);
			if (x != y) {
				ret += w[e];
				fa[x] = y;
			}
		}
		ans = min(ans, ret);
		for (int i = 1; i <= m; ++i) w[i] /= (d[u[i]] + 1);
	}
	printf("%lld", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 15
#define M 1010
#define INF 100000007
using namespace std;
int n, m, u[M], v[M], G[N][N];
long long w[M];
int fa[N], r[M];
bool cmp(const int i, const int j) { return w[i] < w[j]; }
inline int find(int x) { return x == fa[x] ? x : fa[x] = find(fa[x]); }

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m); for (int i = 1; i <= n; ++i) for (int j = 1; j <= n; ++j) G[i][j] = (i == j) ? 0 : INF;
	for (int i = 1; i <= m; ++i) scanf("%d%d%lld", u + i, v + i, w + i), G[u[i]][v[i]] = G[v[i]][u[i]] = 1;
	for (int k = 1; k <= n; ++k) for (int i = 1; i <= n; ++i) for (int j = 1; j <= n; ++j) G[i][j] = min(G[i][j], G[i][k] + G[k][j]);
	long long ans = INF;
	for (int op = 1; op <= n; ++op) {
		long long ret = 0;
		for (int i = 1; i <= n; ++i) fa[i] = i;
		for (int i = 1; i <= m; ++i) r[i] = i;
		for (int i = 1; i <= m; ++i) w[i] *= 1LL * (G[op][u[i]] + 1);
		sort(r + 1, r + m + 1, cmp);
		for (int i = 1; i <= m; ++i) {
			int e = r[i]; int x = find(u[e]), y = find(v[e]);
			if (x != y) {
				ret += w[e];
				fa[x] = y;
			}
		}
		ans = min(ans, ret);
		for (int i = 1; i <= m; ++i) w[i] /= (G[op][u[i]] + 1);
	}
	printf("%lld", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

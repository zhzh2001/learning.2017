#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#define maxn 1010
#define INF 1000000007
using namespace std;
int t, n;
long long h, r;
bool vis[maxn][maxn];
struct Ball {
	long long x, y, z;
} b[maxn];

inline int check(Ball a, Ball b)
{
	int d = sqrt(1LL * (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z));
	if (d > 2 * r) return -1;
	if (d == 2 * r) return 0;
	return 1;
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &t);
	while (t--) {
		memset(vis, 0, sizeof(vis));
		scanf("%d%lld%lld", &n, &h, &r); Ball lowest = (Ball) {0, 0, INF};
		for (int i = 1; i <= n; ++i) {
			scanf("%lld%lld%lld", &b[i].x, &b[i].y, &b[i].z);
			if (b[i].z < lowest.z) lowest = b[i];
		}
		if (1LL * 2 * n * r < h) {
			printf("No\n");
			continue;
		}
		if (lowest.z - r > 0) {
			printf("No\n");
			continue;
		}
		long long reach = 0;
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= n; ++j) {
				if (i == j) continue;
				if (vis[i][j] || vis[j][i]) continue;
				int pd = check(b[i], b[j]);
				if (pd == -1) continue;
				reach += abs(b[i].y - b[j].y) + 1LL * 2 * r;
				vis[i][j] = vis[j][i] = 1;
			}
		if (reach >= h) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

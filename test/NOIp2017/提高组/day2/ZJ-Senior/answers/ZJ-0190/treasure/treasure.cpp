#include<bits/stdc++.h>
#define For(i,x,y) for (int i=x;i<=y;i++)
using namespace std;
int n,m,op,d[50][50],a[50],b[50][50];

void dfs(int p){
	if (b[1][0]>1) return;
	if (p==n+1){
		if (b[1][0]==0) return;
		int ans=0;
		bool ff=true;
		For(i,2,n){
			if (b[i][0]==0) ff=false;
			else if (!ff) return;
				 else{
				 	For(j,1,b[i][0]){
				 		int num=5000000;
				 		For(k,1,b[i-1][0]) 
							num=min(num,d[b[i][j]][b[i-1][k]]);
					    if (num==5000000) return;
					 	else ans+=(i-1)*num;
					 	if ((ans!=0)&&(ans>op)) return;
					}
				 }
		}
		if (ans!=0) op=min(ans,op);
		return;
	}
	For(i,1,n){
		b[i][0]++;
		b[i][b[i][0]]=p;
		dfs(p+1);
		b[i][0]--;
	}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(d,127,sizeof(d));
	For(i,1,m){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		d[u][v]=min(d[u][v],w);
		d[v][u]=min(d[v][u],w);
	}
	op=100000000;
	dfs(1);
	printf("%d",op);
}

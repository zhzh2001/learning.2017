#include<bits/stdc++.h>
#define For(i,x,y) for (int i=x;i<=y;i++)
using namespace std;
long long n,h,r,t,tot,head[2000],le,ri,qu[100000];
bool f[2000],ff;

struct holl{
	long long x,y,z;
}a[2000];

struct line{
	long long w,next;
}hi[1000000];

void add(long u,long v){
	tot++;
	hi[tot].w=v;
	hi[tot].next=head[u];
	head[u]=tot;
}

void bfs(){
	while (le+1<=ri){
		le++;
		if (h-a[qu[le]].z<=r){
			ff=true;
			return;
		}
		for (int i=head[qu[le]];i;i=hi[i].next){
			if (!f[hi[i].w]){
				ri++;
				qu[ri]=hi[i].w;
				f[hi[i].w]=true;
			}
		}
	}
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&t);
	while (t>0){
		t--;
		scanf("%lld%lld%lld",&n,&h,&r);
		ff=false; tot=0;
		memset(head,0,sizeof(head));
		memset(f,false,sizeof(f));
		For(i,1,n) scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		For(i,1,n-1) For(j,i+1,n){
			if ((a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z)<=4*r*r){
				add(i,j);
				add(j,i);
			}
		}
		le=ri=0;
		For(i,1,n){
			if (a[i].z<=r){
				ri++;
				qu[ri]=i;
				f[i]=true;
			}
		}
		bfs();
		if (ff) printf("Yes\n");
		else printf("No\n");
	}
}

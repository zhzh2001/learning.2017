#include<bits/stdc++.h>
using namespace std;
int n,m,e[20][20]={0};
int work(int bgn)
{
	int ans=0,smin,qs,qp,dis[20],book[20]={0};
	dis[bgn]=0;
	book[bgn]=1;
	for(int k=1;k!=n;k++)
	{
		smin=1e8;
		for(int i=1;i<=n;i++)
		{
			if(book[i])
			{
				for(int j=1;j<=n;j++)
				{
					if(!book[j])
					if(smin>e[i][j]*book[i])
					{
						smin=e[i][j]*book[i];
						qs=j;
						qp=book[i];
					}
				}
			}
		}
		book[qs]=qp+1;
		ans+=smin;
	}
	return ans;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int smin=1e8;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	for(int j=1;j<i;j++)
	e[i][j]=e[j][i]=1e6;
	for(int i=0;i!=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if(e[x][y]>z)
		e[x][y]=e[y][x]=z;
	}
	for(int i=1;i<=n;i++)
	{
		int s=work(i);
		smin=s<smin?s:smin;
	}
	cout<<smin<<endl;
}

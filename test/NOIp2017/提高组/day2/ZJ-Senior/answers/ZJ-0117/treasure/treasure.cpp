#include<bits/stdc++.h>
using namespace std;

#define FOR(i,a,b) for(int i=(a);i<=(b);i++)
#define DFOR(i,a,b) for(int i=(a);i>=(b);i--)
#define LFOR(i,x) for(int i=Head[x];i;i=Nxt[i])
#define V 15
#define E 1005 
#define LL long long

int n,m;
LL RES=1e18;

int Head[V],Cost[E<<1],To[E<<1],Nxt[E<<1],Tot=0;

void Add_Edge(int a,int b,int c){Nxt[++Tot]=Head[a];Head[a]=Tot;To[Tot]=b;Cost[Tot]=c;}

struct P1{
	struct Node{
		LL cost;
		int to,fa;
		bool operator < (const Node &_)const{
			return cost>_.cost;
		}
	};
	
	struct UFS{
		int Fa[V];
		void Init(int n){FOR(i,0,n) Fa[i]=i;}
		
		int Find(int x){return x==Fa[x]?x:Fa[x]=Find(Fa[x]);}
		
		bool Same(int a,int b){return Find(a)==Find(b);}
		
		void Unite(int a,int b){Fa[Find(a)]=Find(b);}
	}UFS;
	
	priority_queue<Node> Que;
	int Vis[V],Dep[V];
	LL Val[V];
	LL Ans;
	
	void Clear(int x){
		UFS.Init(n);
		memset(Vis,0,sizeof(Vis));
		while(!Que.empty()) Que.pop();
		Que.push((Node){0,x,0});
		memset(Val,0x3f,sizeof(Val));
		Ans=0;
	}
	
	void Dj(){
		while(!Que.empty()){
			Node e=Que.top();Que.pop();
			int x=e.to;
			if(e.cost>Val[x]) continue;
			if(Vis[x]) continue;
			if(UFS.Same(x,0)) continue;
			
			
			Vis[x]=1;Val[x]=e.cost;Dep[x]=Dep[e.fa]+1;
			
			Ans+=Val[x];
			UFS.Unite(x,0);
			
			LFOR(i,x){
				int y=To[i];
				if(Vis[y] || UFS.Same(y,0) ) continue;
				if(1LL*Dep[x]*Cost[i]<Val[y]){
					Val[y]=Dep[x]*Cost[i];
					Que.push((Node){Val[y],y,x});	
				}
			}
		}
	}
	
	void Solve(){
		FOR(i,1,n){
			Clear(i);
			Dj();
			if(Ans<RES) RES=Ans;
		}
		
		printf("%lld\n",RES);
	}

}P1;

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d %d",&n,&m);
	FOR(i,1,m){
		int a,b,c;
		scanf("%d %d %d",&a,&b,&c);
		Add_Edge(a,b,c);
		Add_Edge(b,a,c);
	}
	
	P1.Solve();

	return 0;
}

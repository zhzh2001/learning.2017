#include<bits/stdc++.h>
using namespace std;

#define FOR(i,a,b) for(int i=(a);i<=(b);i++)
#define DFOR(i,a,b) for(int i=(a);i>=(b);i--)
#define LFOR(i,x) for(int i=Head[x];i;i=Nxt[i])
#define M 300005
#define LL long long

int n,m,q;

struct P1{
	#define M1 1004
	int Tim;
	int Tid[M1][M1],Ans[M];
	
	void Solve(){
		Tim=0;
		FOR(i,1,n) FOR(j,1,m) Tid[i][j]=++Tim;
		
		FOR(i,1,q){
			int x,y;
			scanf("%d %d",&x,&y);
			Ans[i]=Tid[x][y];
			while(x<n || y<m){
				if(y<m) swap(Tid[x][y],Tid[x][y+1]),y++;
				else if(x<n) swap(Tid[x][y],Tid[x+1][y]),x++;
			}
		}
		
		FOR(i,1,q) printf("%d\n",Ans[i]);
	}

}P1;

struct P2{
	LL A[M<<2],Ans[M<<2];
	
	struct Bit{
		LL Sum[(M<<2)+5];
		
		int Lowbit(int x){return x&(-x);}
		
		LL query(int x){
			int Res=0;
			while(x){
				Res+=Sum[x];
				x-=Lowbit(x);
			}
			return Res;
		}
		
		void Update(int x,int a){
			while(x<4*m){
				Sum[x]+=a;
				x+=Lowbit(x);
			}
		}
		
	}Bit;
	
	void Solve(){
		int End=m;
		FOR(i,1,m) A[i]=i;
		
		FOR(i,1,q){
			int a,b;
			scanf("%d %d",&a,&b);
			int Jp=Bit.query(b);
			Ans[i]=A[Jp+b];
			Bit.Update(b,1);
			Bit.Update(End+1,-1);
			A[++End]=Ans[i];
		}
		
		FOR(i,1,q) printf("%lld\n",Ans[i]);
	}
}P2;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	scanf("%d %d %d",&n,&m,&q);
	if(n==1) P2.Solve();
	else P1.Solve();
	return 0;
}

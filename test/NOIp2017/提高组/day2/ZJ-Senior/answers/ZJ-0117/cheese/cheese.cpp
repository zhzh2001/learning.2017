#include<bits/stdc++.h>
using namespace std;

#define FOR(i,a,b) for(int i=(a);i<=(b);i++)
#define DFOR(i,a,b) for(int i=(a);i>=(b);i--)
#define LFOR(i,x) for(int i=Head[x];i;i=Nxt[i])
#define M 1005
#define LL long long

int Case;

struct P1{
	int n,h,r;
	int Head[M],Cost[(M*M)<<1],To[(M*M)<<1],Nxt[(M*M)<<1],Tot;//600W 
	bool Vis[M];
	
	void Add_Edge(int a,int b){Nxt[++Tot]=Head[a];To[Tot]=b;Head[a]=Tot;}
	
	struct Triangle{int a,b,c,Op;}A[M];
	
	queue<int> Que;
	
	LL Get_Pow(int x){return 1LL*x*x;}
	double Get_Dist(Triangle A,Triangle B){return sqrt(1.0*Get_Pow(A.a-B.a)+1.0*Get_Pow(A.b-B.b)+1.0*Get_Pow(A.c-B.c));}
	
	void Clear(){
		while(!Que.empty()) Que.pop();
		memset(Vis,0,sizeof(Vis));
		memset(Head,Tot=0,sizeof(Head));
	}
	
	void Solve(){
		FOR(cas,1,Case){
			Clear();
			
			scanf("%d %d %d",&n,&h,&r);
			FOR(i,1,n){
				scanf("%d %d %d",&A[i].a,&A[i].b,&A[i].c),A[i].Op=0;
				if(A[i].c<=r) Que.push(i);
				if(h-A[i].c<=r) A[i].Op=1;
			}
			
			FOR(i,1,n) FOR(j,i+1,n) if(Get_Dist(A[i],A[j])<=r*2.0) Add_Edge(i,j),Add_Edge(j,i);
			
			bool Flag=0;
			
			while(!Que.empty()){
				int x=Que.front();Que.pop();
				
				Vis[x]=1;
				if(A[x].Op==1){Flag=1;break;}
				
				LFOR(i,x){
					int y=To[i];
					if(Vis[y]) continue;
					Que.push(y);
				}
				
			}
			
			if(Flag) puts("Yes");
			else puts("No");
		}
	}

}P1;

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	scanf("%d",&Case);
	
	P1.Solve();

	return 0;
}

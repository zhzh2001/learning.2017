#include<cstdio>
#include<cstring>
const int maxn=12,inf=0x3f3f3f3f;
int f[maxn+10][1<<maxn],n,m,d[maxn+10][maxn+10];
int a[maxn+10],dis[maxn+10],cnt,ans=inf;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(register int i=0;i<n;++i) 
		for(register int j=0;j<n;++j)
			d[i][j]=i==j?0:inf;
	for(register int i=1;i<=m;++i){
		int l,r,v; scanf("%d%d%d",&l,&r,&v);
		--l; --r; if(v<d[l][r]) d[l][r]=d[r][l]=v;
	}
	memset(f,0x3f,sizeof f);
	for(register int i=0;i<n;++i) f[1][1<<i]=0;
	for(register int i=1;i<n;++i)
		for(register int j=0;j<(1<<n);++j) if(f[i][j]<inf){
			cnt=0;
			for(register int k=0;k<n;++k) if(~j>>k&1){
				a[cnt]=k; dis[cnt]=inf;
				for(register int l=0;l<n;++l) if((j>>l&1)&&d[l][k]<dis[cnt])
					dis[cnt]=d[l][k];
				if(dis[cnt]<inf) ++cnt;
			}
			for(register int k=0;k<(1<<cnt);++k){
				int now=f[i][j],cur=j;
				for(register int l=0;l<cnt;++l) if(k>>l&1){
					now+=i*dis[l]; cur|=1<<a[l];
				}
				if(now<f[i+1][cur]) f[i+1][cur]=now;
			}
		}
	for(register int i=1;i<=n;++i) if(f[i][(1<<n)-1]<ans) ans=f[i][(1<<n)-1];
	printf("%d",ans);
	fclose(stdin); fclose(stdout); return 0;
}

#include<cstdio>
typedef long long ll;
typedef unsigned long long ull;
const int maxn=1000;
int n,h,r,t,x[maxn+10],y[maxn+10],z[maxn+10];
bool vis[maxn+10];
ull dist(int i,int j){
	ull ans=0;
	ans+=1ll*(x[i]-x[j])*(x[i]-x[j]);
	ans+=1ll*(y[i]-y[j])*(y[i]-y[j]);
	ans+=1ll*(z[i]-z[j])*(z[i]-z[j]);
	return ans;
}
bool dfs(int x){
	vis[x]=1;
	if(z[x]>=h-r&&z[x]<=h+r) return 1;
	for(register int i=1;i<=n;++i) if(!vis[i]&&dist(x,i)<=4ll*r*r&&dfs(i))
		return 1;
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d",&n,&h,&r);
		for(register int i=1;i<=n;++i){
			scanf("%d%d%d",&x[i],&y[i],&z[i]); vis[i]=0;
		}
		bool suc=0;
		for(register int i=1;i<=n;++i) if(!vis[i]&&z[i]>=-r&&z[i]<=r&&dfs(i)){
			suc=1; printf("Yes\n"); break;
		}
		if(!suc) printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

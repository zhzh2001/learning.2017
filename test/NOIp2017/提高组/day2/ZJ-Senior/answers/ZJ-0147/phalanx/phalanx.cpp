#include<cstdio>
typedef long long ll;
const int maxn=50000,maxq=500,maxnn=600000;
int n,m,q,mp[maxn+10],list[maxq+1][maxn+10],cnt,lst[maxn+1],ans;
int c[maxnn+10]; ll lc[maxnn+10],ld[maxnn+10],ccnt,dcnt;
inline void add(int x,int v){
	while(x<=maxnn){
		c[x]+=v; x+=x&-x;
	}
}
inline int query(int x){
	int ans=0;
	while(x){
		ans+=c[x]; x-=x&-x;
	}
	return ans;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(q<=500){
		for(register int i=1;i<=n;++i) lst[i]=i*m;
		while(q--){
			int x,y; scanf("%d%d",&x,&y);
			if(!mp[x]){
				mp[x]=++cnt;
				for(register int i=1;i<m;++i) list[cnt][i]=(x-1)*m+i;
			}
			if(y<m) printf("%d\n",ans=list[mp[x]][y]);
			else printf("%d\n",ans=lst[x]);
			for(register int i=y;i<m-1;++i) list[mp[x]][i]=list[mp[x]][i+1];
			list[mp[x]][m-1]=lst[x];
			for(register int i=x;i<n;++i) lst[i]=lst[i+1]; lst[n]=ans;
		}
	}else{
		for(register int i=1;i<m;++i){
			lc[++ccnt]=i; add(i,1);
		}
		for(register int i=1;i<=n;++i){
			ld[++dcnt]=1ll*i*m;
		}
		while(q--){
			int x,y; ll ans;  scanf("%d%d",&x,&y);
			if(y<m){
				int l=1,r=ccnt;
				while(l!=r){
					int mid=l+r>>1;
					if(query(mid)<y) l=mid+1; else r=mid;
				}
				printf("%lld\n",ans=lc[l]); add(l,-1); lc[++ccnt]=ld[dcnt-n+1]; add(ccnt,1);
			}else printf("%lld\n",ans=ld[dcnt-n+1]);
			ld[++dcnt]=ans;
		}
	}
	fclose(stdin); fclose(stdout); return 0;
}

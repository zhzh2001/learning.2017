#include<bits/stdc++.h>
using namespace std;
long long x[1010],y[1010],z[1010],h,r;
int fa[1010],T,n;
bool pd,t[1010],b[1010];
int get(int x){
	if(x!=fa[x])fa[x]=get(fa[x]);
	return fa[x];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		pd=false;
		for(int i=1;i<=n;++i){
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			b[i]=false;
			t[i]=false;
			if(abs(z[i])<=r)b[i]=true;
			if(abs(z[i]-h)<=r)t[i]=true;
			if(t[i]&&b[i])pd=true;
			fa[i]=i;
		}
		for(int i=1;i<=n;++i){
			for(int j=1;j<=n;++j){
				if(((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])<=(r+r)*(r+r))){
					int an=get(i),bn=get(j);
					if(an!=bn){
						fa[an]=bn;
						if(b[an]|b[bn]){
							b[an]=b[bn]=true;
						}
						if(t[an]|t[bn]){
							t[an]=t[bn]=true;
						}
						if(t[an]&&b[bn])pd=true;
					}
				}
			}
		}
		if(pd)printf("Yes\n");
		else  printf("No\n");
	}
	return 0;
}



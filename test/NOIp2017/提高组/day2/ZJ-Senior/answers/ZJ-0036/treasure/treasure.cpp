#include<cstdio>
#include<cstring>
#include<queue>
#include<cstdlib>
using namespace std;
typedef pair<int,int> P;
priority_queue<P,vector<P>,greater<P> > q;
struct edge{
	int to,nxt,val;
}e[100005];
int fst[105],tot=0,dep[105],vis[105],dis[105];
void addedge(int u,int v,int w)
{
	e[++tot].to=v;
	e[tot].val=w;
	e[tot].nxt=fst[u];
	fst[u]=tot;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m,ANS=0x3f3f3f3f;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z);addedge(y,x,z);
	}
	for(int i=1;i<=n;i++)
	{
		int root=i,ans=0;
		memset(dep,-1,sizeof(dep));
		memset(dis,0x3f,sizeof(dis));
		memset(vis,0,sizeof(vis));
		dep[root]=1;q.push(make_pair(0,root));
		while(q.size())
		{
			P p=q.top();q.pop();
			int x=p.second;
			if(vis[x]) continue;vis[x]=1;
			ans+=p.first;
			for(int i=fst[x];i;i=e[i].nxt)
			if(!vis[e[i].to])
			{
				int v=e[i].to;
				if(dep[x]*e[i].val<dis[v])
				{
					dis[v]=dep[x]*e[i].val;
					dep[v]=dep[x]+1;
					q.push(make_pair(dis[v],v));
				}
			}
		}
		if(ans<ANS) ANS=ans;
	}
	printf("%d\n",ANS);
	return 0;
}

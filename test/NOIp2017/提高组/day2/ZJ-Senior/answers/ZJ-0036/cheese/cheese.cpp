#include<cstdio>
#include<cmath>
#include<algorithm>
typedef double db;
struct mysphere{
	int x,y,z;
}a[10005];
db dis(mysphere p,mysphere q) {return sqrt((p.x-q.x)*(p.x-q.x)+(p.y-q.y)*(p.y-q.y)+(p.z-q.z)*(p.z-q.z));}
struct edge{
	int to,nxt;
}e[3000005];
int first[10005],vis[10005],tot;
void addedge(int u,int v)
{
	e[++tot].to=v;
	e[tot].nxt=first[u];
	first[u]=tot;
}
void dfs(int x)
{
	vis[x]=1;
	for(int i=first[x];i;i=e[i].nxt)
	if(!vis[e[i].to]) dfs(e[i].to);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int _,n,h,r,S,T;
	scanf("%d",&_);
	for(;_--;)
	{
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++) 
		{
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
			first[i]=0;vis[i]=0;
		}
		tot=0;S=n+1;T=n+2;
		first[S]=vis[S]=first[T]=vis[T]=0;
		for(int i=1;i<=n;i++)
		if(abs(a[i].z)<=r) addedge(S,i);
		for(int i=1;i<=n;i++)
		if(abs(a[i].z-h)<=r) addedge(i,T);
		for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			if(i==j) continue;
			if(dis(a[i],a[j])<=2*r) addedge(i,j);
		}
		dfs(S);
		if(vis[T]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

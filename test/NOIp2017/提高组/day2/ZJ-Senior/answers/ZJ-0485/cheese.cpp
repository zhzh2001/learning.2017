#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
using namespace std;

long long n,h,r;
struct node
{
	long long x,y,z;
}a[1011];
bool vis[1011];
int head[1011],to[1000011],nxt[1000011],tot;
queue<int> q;

long long read()
{
	long long x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1; c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void write(long long x)
{
	if(x<0) putchar('-'),x=-x;if (x>=10) write(x/10);
	putchar(x%10+'0');
}

void writern(long long x)
{
	write(x),putchar(' ');
}

void writeln(long long x)
{
	write(x),puts("");
}

long long sqr(long long x)
{
	return x*x;
}

bool good(int i,int j)
{
	long long tmp=sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z);
	long long tp=r*r*4;
	if (tmp<=tp) return 1;
	return 0;
}

void add(int fr,int tt)
{
	to[++tot]=tt,nxt[tot]=head[fr];head[fr]=tot;
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read(),h=read(),r=read();
		tot=0;memset(head,0,sizeof head);
		memset(vis,0,sizeof vis);
		for (int i=1;i<=n;i++)
			a[i].x=read(),a[i].y=read(),a[i].z=read();
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
				if (good(i,j))
				{
					add(i,j);add(j,i);
				}
		for (int i=1;i<=n;i++)
		{
			if (a[i].z<=r)
			{
				add(i,n+1);add(n+1,i);
			}
			if (a[i].z+r>=h)
			{
				add(i,n+2),add(n+2,i);
			}
		}
		vis[n+1]=1;
		q.push(n+1);
		while (!q.empty())
		{
			int now=q.front();q.pop();
			for(int i=head[now];i;i=nxt[i])
			{
				int v=to[i];
				if (!vis[v])
				{
					vis[v]=1;
					q.push(v);
				}
			}
		}
		if (vis[n+2])
		{
			puts("Yes");
		}
		else
		{
			puts("No");
		}
	}
	return 0;
}


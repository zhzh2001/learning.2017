#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

int n,m,q;
struct node
{
	int x,y;
}p[300011];

long long read()
{
	long long x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1; c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void write(long long x)
{
	if(x<0) putchar('-'),x=-x;if (x>=10) write(x/10);
	putchar(x%10+'0');
}

void writern(long long x)
{
	write(x),putchar(' ');
}

void writeln(long long x)
{
	write(x),puts("");
}

int a1[1011][1011];

void work1()
{
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a1[i][j]=(i-1)*m+j;
	for (int tt=1;tt<=q;tt++)
	{
		int x=read(),y=read();
		int tmp=a1[x][y];writeln(tmp);
		for (int i=y;i<=m;i++) a1[x][i]=a1[x][i+1];
		for (int i=x;i<=n;i++) a1[i][m]=a1[i+1][m];
		a1[n][m]=tmp;
	}
}

int lim;
int c1[1000011];
long long a2[1000011];
bool out[1000011];

int lowbit(int x) {return x&-x;}
void update(int x) {for(;x<=lim;c1[x]++,x+=lowbit(x));}
int query(int x) {int sum=0;for(;x;sum+=c1[x],x-=lowbit(x));return sum;}

void work2()
{
	for (int i=1;i<=m;i++) a2[i]=i;
	lim=m+q;
	for (int tt=1;tt<=q;tt++)
	{
		int x=read(),y=read();
		//int tmp=query(y);
		int l=1,r=m,mid,pos;
		while(l<=r)
		{
			mid=l+r>>1;
			int tmp=mid-query(mid);
			if (tmp==y&&!out[mid]) {pos=mid;break;}
			if (tmp<y) l=mid+1;
			else r=mid-1;
		}
		writeln(a2[pos]);
		a2[++m]=a2[pos];out[pos]=1;
		update(pos);
	}
}

void work3()
{
	for (int i=1;i<=m;i++) a2[i]=i;
	int tot=m;lim=n+m+q;
	for (int i=2;i<=n;i++) a2[++tot]=1ll*i*m;
	for (int t=1;t<=q;t++)
	{
		int x=1,y=p[t].y;
		int l=1,r=tot,mid,pos;
		while(l<=r)
		{
			mid=l+r>>1;
			int tmp=mid-query(mid);
			if (tmp==y&&!out[mid]) {pos=mid;break;}
			if (tmp<y) l=mid+1;
			else r=mid-1;
		}
		writeln(a2[pos]);
		a2[++tot]=a2[pos];out[pos]=1;
		update(pos);
	}
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n<=1000&&m<=1000&&n*2*q<=3e7)
	{
		work1();
		return 0;
	}
	if (n==1)
	{
		work2();
		return 0;
	}
	bool flag1=1;
	for (int i=1;i<=q;i++) p[i].x=read(),p[i].y=read(),flag1=(p[i].x==1?flag1:0);
	if (flag1)
	{
		work3();
		return 0;
	}
	return 0;
}


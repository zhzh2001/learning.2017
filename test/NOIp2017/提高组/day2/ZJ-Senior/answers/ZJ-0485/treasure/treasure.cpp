#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

int n,m;
int dis[21][21];

long long read()
{
	long long x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1; c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void write(long long x)
{
	if(x<0) putchar('-'),x=-x;if (x>=10) write(x/10);
	putchar(x%10+'0');
}

void writern(long long x)
{
	write(x),putchar(' ');
}

void writeln(long long x)
{
	write(x),puts("");
}

void floyd()
{
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			if (i!=k)
				for (int j=1;j<=n;j++)
					if (i!=j&&j!=k)
						dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
}

void work1()
{
	for (int i=1;i<=n;i++)dis[i][i]=0;
	floyd();
	int ans=2e9;
	for (int i=1;i<=n;i++)
	{
		int cnt=0;
		for (int j=1;j<=n;j++)
			cnt+=dis[i][j];
		if (cnt<ans)
			ans=cnt;
	}
	writeln(ans);
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	bool flag=1;int ls=0;
	memset(dis,127,sizeof dis);
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read(),ww=read();
		dis[x][y]=dis[y][x]=min(dis[x][y],ww);
		if (i==1)
			ls=ww;
		else if (ls!=ww)
			flag=0;
	}
	if (flag)
	{
		work1();
		return 0;
	}
	return 0;
}


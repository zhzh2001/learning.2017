#include<cstdio>
#include<cctype>
#include<cmath>
#define maxn 1001
#define eps 1e-7
long long t,canin[maxn],canout[maxn],n,h,r,vis[maxn],flag;
struct hole
{
	long long x,y,z;
	hole(long long xx,long long yy,long long zz):x(xx),y(yy),z(zz){}
	hole(){}
} node[maxn];

long long read()
{
	long long ret=0,fh=1;char c=getchar();
	while(!isdigit(c)) {if(c=='-') fh=-1;c=getchar();};
	while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
	return fh*ret;
}

double dist(long long pa,long long pb)
{
	double dx=node[pa].x-node[pb].x;
	double dy=node[pa].y-node[pb].y;
	double dz=node[pa].z-node[pb].z;
	return (double)sqrt(dx*dx+dy*dy+dz*dz);
}

void mkcanins()
{
	for(long long i=1;i<=n;i++)
		if(node[i].z<=r) canin[i]=1;
}

void mkcanouts()
{
	for(long long i=1;i<=n;i++)
		if(h-node[i].z<=r) canout[i]=1;
}

void dfs(long long u)
{
	vis[u]=1;
	if(canout[u])
	{
		flag=1;
		return;
	}
	for(long long i=1;i<=n;i++)
	{
		if(flag) return;
		if(!vis[i]&&dist(u,i)<=2*r+eps)
			dfs(i);
	}
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read(),h=read(),r=read();flag=0;
		for(long long i=1;i<=n;i++)
			canin[i]=canout[i]=vis[i]=0;
		for(long long i=1;i<=n;i++)
		{
			long long x=read(),y=read(),z=read();
			node[i]=hole(x,y,z);
		}
		mkcanins();mkcanouts();
		for(long long i=1;i<=n;i++)
			if(canin[i]) dfs(i);
		if(flag) printf("Yes\n");
		else printf("No\n");
		//for(long long i=1;i<=n;i++)
		//	printf("node%d:canin=%d,canout=%d",i,canin[i],canout[i]);
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#define maxq 300001
int n,m,q;
struct ld{int ox,oy,x,y,num;} ls[maxq];

int read()
{
	int ret=0;char c=getchar();
	while(!isdigit(c)) c=getchar();
	while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
	return ret;
}

int getid(int i,int j) {return (i-1)*m+j;}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	for(int t=1;t<=q;t++)//当前的为t 
	{
		int nx=read(),ny=read();
		int sx=nx,sy=ny;
		ls[t].ox=nx;ls[t].oy=ny;
		//如果是之前中的一个
		int flag=0;
		for(int i=1;i<t;i++)
			if(nx==ls[i].x&&ny==ls[i].y)
			{
				printf("%d\n",ls[i].num);
				ls[t].x=n,ls[t].y=m;
				ls[t].num=ls[i].num;
				flag=1;
				//清除这一个
				ls[i].x=n+1;ls[i].y=m+1;ls[i].num=-1;
				break; 
			}
		//如果不是就移动 
		if(!flag)
		{
			for(int i=1;i<t;i++)	
			{
				if(ny==m&&nx>=ls[i].ox&&nx+1<=n)
					nx++;
				if(nx==ls[i].ox&&ny>=ls[i].oy&&ny+1<=m)
					ny++;
			}
			ls[t].x=n,ls[t].y=m;ls[t].num=getid(nx,ny);
			printf("%d\n",ls[t].num);
		}
		//更新之前的结点
		nx=sx,ny=sy;
		for(int i=1;i<t;i++)
		{
			if(ls[i].y==m&&ls[i].x>=nx&&ls[i].x-1>=0)
				ls[i].x--;
			if(ls[i].x==nx&&ls[i].y>=ny&&ls[i].y-1>=0)
				ls[i].y--;
		 } 
	}
	return 0;
}

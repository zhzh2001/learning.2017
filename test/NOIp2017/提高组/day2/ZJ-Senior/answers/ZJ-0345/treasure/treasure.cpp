#include<cstdio>
#include<cctype>
#include<algorithm>
#define INF 7865544531021
using namespace std;
long long n,m,ans=INF,idx,vis[13],f[13];
struct edge
{
	long long from,to,val,k;
	bool operator < (const edge rhs) const
	{
		return k*val<rhs.k*rhs.val;
	}
	edge(long long ff,long long tt,long long vv,long long kk=INF):from(ff),to(tt),val(vv),k(kk){}
	edge(){}
} e[2001],ne[2001];

long long read()
{
	long long ret=0,fh=1;char c=getchar();
	while(!isdigit(c)) {if(c=='-') fh=-1;c=getchar();}
	while(isdigit(c)) ret=ret*10+c-'0',c=getchar();
	return (long long)fh*ret;
}

long long getf(long long x)
{
	if(x!=f[x]) f[x]=getf(f[x]);
	return f[x];
}

void dfs(long long u,long long dep)
{
	vis[u]=dep;
	for(long long i=1;i<=idx;i++)
		if(ne[i].from==u||ne[i].to==u)
		{
			ne[i].k=min(ne[i].k,dep);
			if(vis[ne[i].to]==0||vis[ne[i].to]>dep+1) dfs(ne[i].to,dep+1);
		}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for(long long i=1;i<=m;i++)
	{
		long long u=read(),v=read(),w=read();
		e[++idx]=edge(u,v,w);e[++idx]=edge(v,u,w);
	}
	for(long long t=1;t<=n;t++)
	{
		for(long long i=1;i<=idx;i++)
			ne[i]=e[i];
		for(long long i=1;i<=n;i++)
			f[i]=i,vis[i]=0;
		dfs(t,1);
		sort(ne+1,ne+1+idx);
		//Kruskal
		long long sum=0,cnte=0;
		for(long long i=1;i<=idx;i++)
		{
			long long &u=ne[i].from,&v=ne[i].to;
			long long fu=getf(u),fv=getf(v);
			if(fu!=fv)
			{
				f[fv]=fu;
				cnte++;
				sum+=ne[i].k*ne[i].val;
				if(cnte==n-1) break;
			}
		 } 
		 ans=min(ans,sum);
	}
	printf("%lld\n",ans);
	return 0;
}

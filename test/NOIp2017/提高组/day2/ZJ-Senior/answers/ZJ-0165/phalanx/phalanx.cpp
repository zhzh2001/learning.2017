#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
typedef long long LL;
LL n,m,q,num[300001],lx[300001],ans[300001];
LL d[501][50001],p[300001];
bool vis[300001];
const int N=1000000;
struct data {
	LL x,y;
}a[300001];
LL getnum(LL x,LL y) {
	return (x-1)*m+y;
}
namespace bit {
	int t[1000001];
	int lowbit(int x) {
		return x&(-x);
	}
	void Add(int x,int va) {
		while (x<N) {
			t[x]+=va;
			x+=lowbit(x);
		}
	}
	int Query(int x) {
		int sum=0;
		while (x>0) {
			sum+=t[x];
			x-=lowbit(x);
		}
		return sum;
	}
	int find(int k) {
		int l=1,r=N;
		while (l<r) {
			int mid=(l+r)/2;
			if (Query(mid)<k) l=mid+1;
				else r=mid;
		}
		return l;
	}
}
void work1() {
	for (int i=1;i<=m;i++) ans[i]=i;
	for (int i=2;i<=n;i++) ans[m+i-1]=getnum(i,m);
	for (int i=1;i<=m+n-1;i++) bit::Add(i,1);
	int tot=m+n-1;
	for (int i=1;i<=q;i++) {
		int k=a[i].y;
		int nowx=bit::find(k);
		printf("%lld\n",ans[nowx]);
		bit::Add(nowx,-1);
		bit::Add(tot+1,1);
		tot++;
		ans[tot]=ans[nowx];
	}
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&q);
	bool judge=1;
	for (int i=1;i<=q;i++) {
		scanf("%lld%lld",&a[i].x,&a[i].y);
		if (a[i].x!=1) judge=0;
		vis[a[i].x]=1;
	}
	if (judge) {
		work1();
		return 0;
	}
	int numx=0;
	for (int i=1;i<=n;i++) {
		if (!vis[i]) continue;
		num[i]=++numx;
		lx[numx]=i;
	}
	for (int i=1;i<=numx;i++)
		for (int j=1;j<=m;j++) d[i][j]=getnum(lx[i],j);
	for (int i=1;i<=n;i++) p[i]=getnum(i,m);
	for (int i=1;i<=q;i++) {
		LL nowx=num[a[i].x],nowy=a[i].y;
		LL nowans=d[nowx][nowy];
		printf("%lld\n",nowans);
		for (int j=nowy;j<m;j++) d[nowx][j]=d[nowx][j+1];
		for (int j=a[i].x;j<n;j++) p[j]=p[j+1];
		p[n]=nowans;
		for (int j=1;j<=numx;j++) d[j][m]=p[lx[j]];
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
typedef long long LL;
int t,n,fa[10001];
LL h,r,rx;
struct point {
	LL x,y,z;
}a[2001];
LL sq(LL x) {
	return x*x;
}
LL dist(int u,int v) {
	return sq(a[u].x-a[v].x)+sq(a[u].y-a[v].y)+sq(a[u].z-a[v].z);
}
int getf(int x) {
	if (fa[x]==x) return x;
	fa[x]=getf(fa[x]);
	return fa[x];
}
void heb(int u,int v) {
	int fu=getf(u),fv=getf(v);
	if (fu==fv) return;
	fa[fu]=fv;
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--) {
		scanf("%d%lld%lld",&n,&h,&r);
		rx=r;
		r=r*r*4ll;
		for (int i=1;i<=n;i++)
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		for (int i=1;i<=n+2;i++) fa[i]=i;
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++) {
				LL nowdis=dist(i,j);
				if (nowdis<=r) heb(i,j);
			}
		for (int i=1;i<=n;i++) {
			if (abs(a[i].z)<=rx) heb(n+1,i);
			if (abs(a[i].z-h)<=rx) heb(n+2,i);
		}
		int fu=getf(n+1),fv=getf(n+2);
		if (fu==fv) printf("Yes\n");
			else printf("No\n");
	}
	return 0;
}

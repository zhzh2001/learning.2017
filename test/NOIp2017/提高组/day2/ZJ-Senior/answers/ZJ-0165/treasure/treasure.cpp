#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
using namespace std;
const int INF=0x3f3f3f3f;
int n,m,tx;
int f[5000][15][15],minx[5000][15][15];
int dis[15][15],b[10000];
void Update(int i,int j,int k) {
	for (int jx=1;jx<=n;jx++) {
		if (dis[j][jx]==INF) continue;
		if (jx==j) continue;
		minx[i][jx][k]=min(minx[i][jx][k],f[i][j][k]+dis[j][jx]*k);
	}
}
void dfs(int x,int kx,int now) {
	if (x==n+1) {
		if (now==0) return;
		if (now==kx) return;
		b[++tx]=now;
		return;
	}
	if (kx&(1<<(x-1))) dfs(x+1,kx,now|(1<<(x-1)));
	dfs(x+1,kx,now);
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dis,0x3f,sizeof(dis));
	for (int i=1;i<=m;i++) {
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		dis[u][v]=dis[v][u]=min(dis[u][v],w);
	}
	memset(f,0x3f,sizeof(f));
	memset(minx,0x3f,sizeof(f));
	for (int i=1;i<(1<<n);i++) {
		tx=0;
		dfs(1,i,0);
		for (int j=1;j<=n;j++) {
			if ((i&(1<<(j-1)))==0) continue;
			for (int k=0;k<=n;k++) {
				if (i==(1<<(j-1))) {
					f[i][j][k]=0;
					Update(i,j,k);
					continue;
				}
				for (int k2=1;k2<=tx;k2++) {
					int ano=i^b[k2];
					int now=f[ano][j][k]+minx[b[k2]][j][k+1];
					f[i][j][k]=min(f[i][j][k],now);
				}
				Update(i,j,k);
			}
		}
	}
	int ans=INF;
	for (int i=1;i<=n;i++) ans=min(ans,f[(1<<n)-1][i][0]);
	printf("%d\n",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##BEGIN_=(b);i>=i_##BEGIN_;--i)
#define M 1005
#define N 25
int n,m;
int dis[N][N];
struct P70{
	long long ans;
	void dfs(int x,int Lt,int Mp,long long tmp){
		if(Mp==(1<<n)-1){
			if(tmp<ans)ans=tmp;
			return;
		}
		FOR(i,1,(1<<n)-1){
			if(i&Mp)continue;
			long long res=0;
			int flag=1;
			FOR(j,0,n-1){
				if(i&(1<<j)){
					int tmp=1e9;
					FOR(k,0,n-1){
						if(Lt&(1<<k)){
							if(dis[j][k]<tmp)tmp=dis[j][k];
						}
					}
					if(tmp==1e9){
						flag=0;
						break;
					}
					res+=1ll*tmp*x;
				}
			}
			if(flag){
				dfs(x+1,i,Mp|i,res+tmp);
			}
		}
	}
	void solve(){
		ans=1e18;
		FOR(i,0,n-1)dfs(1,1<<i,1<<i,0);
		printf("%lld\n",ans);
	}
}p70;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	int a,b,c;
	FOR(i,0,n-1)FOR(j,0,n-1)dis[i][j]=1e9;
	FOR(i,0,n-1)dis[i][i]=0;
	FOR(i,1,m){
		scanf("%d%d%d",&a,&b,&c);
		a--;b--;
		dis[a][b]=min(dis[a][b],c);
		dis[b][a]=min(dis[b][a],c);
	}
	p70.solve();
	return 0;
}

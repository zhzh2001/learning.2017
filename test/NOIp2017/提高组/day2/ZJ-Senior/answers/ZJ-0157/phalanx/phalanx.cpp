#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##BEGIN_=(b);i>=i_##BEGIN_;--i)
#define M 300005
int n,m,q;
int X[M],Y[M];
struct P30{
	int A[1005][1005];
	void solve(){
		FOR(i,1,n)FOR(j,1,m)A[i][j]=(i-1)*m+j;
		FOR(cas,1,q){
			int x=X[cas],y=Y[cas];
			int tmp=A[x][y];
			printf("%d\n",A[x][y]);
			FOR(i,y,m-1)A[x][i]=A[x][i+1];
			FOR(i,x,n-1)A[i][n]=A[i+1][n];
			A[n][m]=tmp;
		}
	}
}p30;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	cin>>n>>m>>q;
	FOR(i,1,q)scanf("%d%d",&X[i],&Y[i]);
	p30.solve();
	return 0;
}

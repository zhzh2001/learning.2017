#include<bits/stdc++.h>
using namespace std;
#define FOR(i,a,b) for(int i=(a),i_##END_=(b);i<=i_##END_;++i)
#define REP(i,a,b) for(int i=(a),i_##BEGIN_=(b);i>=i_##BEGIN_;--i)
#define M 1005
#define eps 1e-7
typedef double db;
int T,n,h;
db r;
struct node{
	int x,y,z;
}A[M];
struct P10{
	int dp[M];
	db dis(int x1,int y1,int z1,int x2,int y2,int z2){
		return sqrt(1ll*(x1-x2)*(x1-x2)+1ll*(y1-y2)*(y1-y2)+1ll*(z1-z2)*(z1-z2));
	}
	vector<int>G[M];
	int dfs(int x){
		if(A[x].z+r>=h){
			return 1;
		}
		if(~dp[x])return dp[x];
		dp[x]=0;
		FOR(i,0,G[x].size()-1){
			int y=G[x][i];
			if(dfs(y)){
				dp[x]=1;
				break;
			}
		}
		return dp[x];
	}
	void solve(){
		memset(dp,-1,sizeof(dp));
		FOR(i,1,n)G[i].clear();
		FOR(i,1,n)FOR(j,i+1,n){
			if(dis(A[i].x,A[i].y,A[i].z,A[j].x,A[j].y,A[j].z)<=2*r){
				G[i].push_back(j);
				G[j].push_back(i);
			}
		}
		FOR(i,1,n){
			if(A[i].z<=r){
				if(dfs(i)){
					puts("Yes");
					return;
				}
			}
		}
		puts("No");
	}
}p10;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>T;
	while(T--){
		scanf("%d%d%lf",&n,&h,&r);
		FOR(i,1,n)scanf("%d%d%d",&A[i].x,&A[i].y,&A[i].z);
		p10.solve();
	}
	return 0;
}

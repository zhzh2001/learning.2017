#include<bits/stdc++.h>
#define ll long long
#define M 13
#define INF 2e14
using namespace std;

template<class _>void chkmin(_ &a, _ b){if(a>b)a=b;}
template<class _>void chkmax(_ &a, _ b){if(a<b)a=b;}

int n,m;

struct node{
	int to;
	ll cost;
};

vector<node>G[M];

ll dp[M][1<<M];//dp[i][j]表示以i为根，现在已经打了j这个状态的最小花费 
ll dis[M][M][1<<M];//这里的dis表示的是之间有几个点,在k这个状态下 
ll Min=INF;

void Floyd(){
	
	memset(dis,127,sizeof(dis));
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			for(int k=0;k<(1<<n);k++){
				dis[i][j][k]=INF;
			}
		}
	}
	
	for(int i=1;i<=n;i++){
		for(int j=0;j<(1<<n);j++){
			if(j&(1<<(i-1)))dis[i][i][j]=0;
		}
	}
	
	for(int k=0;k<(1<<n);k++){
		for(int i=1;i<=n;i++){
			for(int j=0;j<(int)G[i].size();j++){
				int to=G[i][j].to;
				if((k&(1<<(i-1)))&&(k&(1<<(to-1))))dis[i][to][k]=1;
			}
		}
	}
	
	for(int l=0;l<(1<<n);l++){
		for(int k=1;k<=n;k++){
			if((l&(1<<(k-1)))==0)continue;
			for(int i=1;i<=n;i++){
				if((l&(1<<(i-1)))==0)continue;
				for(int j=1;j<=n;j++){
					if((l&(1<<(j-1)))==0)continue;
					chkmin(dis[i][j][l],dis[i][k][l]+dis[k][j][l]);
				}
			}
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=m;i++){
		int a,b;
		ll c;
		scanf("%d%d%lld",&a,&b,&c);
		G[a].push_back((node){b,c});
		G[b].push_back((node){a,c});
	}
	for(int i=1;i<=n;i++){
		for(int j=0;j<(1<<n);j++){
			dp[i][j]=INF;
		}
	}
	Floyd();
	for(int i=1;i<=n;i++)dp[i][1<<(i-1)]=0;
	for(int i=1;i<=n;i++){//枚举根节点 
		for(int j=0;j<(1<<n);j++){//枚举当前的状态 
			for(int k=1;k<=n;k++){//这一次要扩展的节点 
				if(j&(1<<(k-1)))continue;
				for(int l=0;l<(int)G[k].size();l++){
					int to=G[k][l].to;//to表示的是之前与它相连的点 
					if(j&(1<<(to-1))){
						if((dis[i][to][j]+1)>=INF)continue;
						ll cost=(dis[i][to][j]+1)*(G[k][l].cost);
						if(dp[i][j|(1<<(k-1))]>dp[i][j]+cost)
							dp[i][j|(1<<(k-1))]=dp[i][j]+cost;
					}
				}
			}
		}
	}
	for(int i=1;i<=n;i++)chkmin(Min,dp[i][(1<<n)-1]);
	printf("%lld\n",Min);
	return 0;
}

#include<bits/stdc++.h>
#define M 1000005
using namespace std;

struct node{
	int x,y;
};

node pos[M];//表示编号为i的人现在的位置
int id[1005][1005];//表示i，j这个位置上的人的编号 

int n,m,q;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	cin>>n>>m>>q;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			pos[(i-1)*m+j].x=i;
			pos[(i-1)*m+j].y=j;
			id[i][j]=(i-1)*m+j;
		}
	}
	
	while(q--){
		int a,b;
		scanf("%d%d",&a,&b);
		printf("%d\n",id[a][b]);
		int ID=id[a][b];
		for(int j=b;j<m;j++){
			id[a][j]=id[a][j+1];
			pos[id[a][j+1]].y=j;
		}
		for(int i=a;i<n;i++){
			id[i][m]=id[i+1][m];
			pos[id[i+1][m]].x=i;
		}
		pos[ID].x=n;
		pos[ID].y=m;
		id[n][m]=ID;
	}
	return 0;
}

#include<bits/stdc++.h>
#define M 1005
#define ll long long
#define lb long double
using namespace std;
//�ǵÿ�ll 

int used[M];

int T;
ll n;
lb h,r;
int p;

vector<int>G[M];

struct node{
	lb x,y,z;
}A[M];

lb dis(node a,node b){
	lb res=(a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
	res=sqrt(res);
	return res;
}

void dfs(int x){
	used[x]=1;
	if(x==n+1){
		p=1;
		return;
	}
	for(int i=0;i<(int)G[x].size();i++){
		if(!used[G[x][i]])dfs(G[x][i]);
	}
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>T;
	while(T--){
		memset(used,0,sizeof(used));
		for(int i=0;i<M;i++)G[i].clear();
		p=0;
		scanf("%lld%Lf%Lf",&n,&h,&r);
		for(int i=1;i<=n;i++){
			scanf("%Lf%Lf%Lf",&A[i].x,&A[i].y,&A[i].z);
		}
		for(int i=1;i<=n;i++){
			if(A[i].z-r<=0)G[0].push_back(i);
			if(A[i].z+r>=h)G[i].push_back(n+1);
		}
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(i==j)continue;
				lb a=dis(A[i],A[j]);
				if(a<=r*2)G[i].push_back(j),G[j].push_back(i);
			}
		}
		dfs(0);
		if(p)puts("Yes");
		else puts("No");
	}
	return 0;
}

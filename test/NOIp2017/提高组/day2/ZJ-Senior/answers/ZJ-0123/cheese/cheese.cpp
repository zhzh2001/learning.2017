#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>

using namespace std;

#define N 1200

int t,n,h,r,q[N];
bool f[N][N],vis[N];
struct node{int x,y,z;}a[N];

bool pd(int i,int j,int r){
	double dist=1.0*(a[i].x-a[j].x)*(a[i].x-a[j].x)+1.0*(a[i].y-a[j].y)*(a[i].y-a[j].y)+1.0*(a[i].z-a[j].z)*(a[i].z-a[j].z);
	if (4.0*r*r-dist>=-1e-6) return 1;
	return 0;
}

int main(){
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		memset(f,0,sizeof f);
		for (int i=1;i<=n;i++){
			if (a[i].z<=r) f[0][i]=f[i][0]=1;
			if (h-a[i].z<=r) f[n+1][i]=f[i][n+1]=1;
			for (int j=1;j<=n;j++)
				if (pd(i,j,r)) f[i][j]=f[j][i]=1;
		}
		memset(vis,0,sizeof vis);
		int tt=0,ww=1; q[tt]=0; vis[0]=1;
		while (tt<ww){
			int u=q[++tt];
			for (int i=0;i<=n+1;i++)
				if (!vis[i] && f[u][i]){vis[i]=1; q[++ww]=i;}
		}
		if (vis[n+1]) puts("Yes"); else puts("No");
	}
	
	return 0;
}
			

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<vector>
#include<set>

using namespace std;

#define N 60000
#define LL long long

LL n,m,q,x,y,ttt,a[N][600];
vector<LL> b[N];

LL getLL(){
	char ch; LL sum=0;
	for (ch=getchar();ch<'0' || ch>'9';ch=getchar());
	for (;ch>='0' && ch<='9';ch=getchar()) sum=sum*10+ch-'0';
	return sum;
}

int main(){
	freopen("phalanx.in","r",stdin); freopen("phalanx.out","w",stdout);
	n=getLL(); m=getLL(); q=getLL();
	while (q--){
		x=getLL(); y=getLL();
		LL i=0,j=1,sum=0;
		for (;sum<y;){
			i++;
			if (j>a[x][0] || i!=a[x][j]) sum++;
			else j++;
		}
		LL tmp; a[x][++a[x][0]]=i; sort(a[x]+1,a[x]+a[x][0]+1);
		if (i<=m) tmp=(x-1)*m+i;
		else tmp=b[x][(i-m)-1];
		printf("%lld\n",tmp);
		for (LL i=x;i<n;i++){
			if (i!=x){
				LL j=b[i].size()-1;
				if (j==-1) a[i][++a[i][0]]=m; else a[i][++a[i][0]]=m+j+1;
			}
			j=b[i+1].size()-1;
			if (j==-1) b[i].push_back((i+1)*m); else b[i].push_back(b[i+1][j]);
		}
		j=b[n].size()-1;
		if (j==-1) a[n][++a[n][0]]=m; else a[n][++a[n][0]]=m+j+1;
		b[n].push_back(tmp);
	}
	
	return 0;
}
			

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

using namespace std;

#define N 20

int n,m,x,y,z,f[N][N],ans;

int main(){
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,0x3f,sizeof f);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		f[x][y]=f[y][x]=1;
	}
	for (int i=1;i<=n;i++) f[i][i]=0;
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	ans=0x3f3f3f3f;
	for (int i=1;i<=n;i++){
		int tmp=0;
		for (int j=1;j<=n;j++)
			tmp+=(f[i][j]);
		if (tmp<ans) ans=tmp;
	}
	printf("%d\n",ans*z);
	
	return 0;
}
			

#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<vector>
#define zz vector<int>::iterator
using namespace std;

int n,m,q;
int a[1007][1007];
int cnt,id[507],p[502][50007],li[50007];
vector<int>ve;

inline int read()
{
	int x=0;char ch=getchar();
	while(ch>'9'&&ch<'0') ch=getchar();
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x;
}
void solve_out()
{
	int x,y;
	for (int i=1;i<=n;i++)
		li[i]=i*m;
	while(q--)
	{
		scanf("%d%d",&x,&y);
		if (y==m)
		{
			printf("%d\n",li[x]);
			int t=li[x];
			for (int i=x;i<n;i++)
				li[i]=li[i+1];
			li[n]=t;	
			continue;
		}
		int num=0;
		for (int i=1;i<=cnt;i++)
			if (id[i]==x){num=i;break;}
		if (!num)
		{
			id[++cnt]=x;
			for (int i=1;i<m;i++)
				p[cnt][i]=(x-1)*m+i;
			num=cnt;	
		}
		printf("%d\n",p[num][y]);
		int t=p[num][y];
		for (int i=y;i<m-1;i++)
			p[num][i]=p[num][i+1];
		p[num][m-1]=li[x];
		for (int i=x;i<n;i++)
			li[i]=li[i+1];
		li[n]=t;
	}
}
void solve_pf()
{
	for (int i=1;i<=m;i++)
		ve.push_back(i);	
	for (int i=1;i<=q;i++)
	{
		int x=read(),y=read();
		int t=ve[y];printf("%d\n",t);
		zz z=ve.begin()+y-1;
		ve.erase(z);
		ve.push_back(t);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	scanf("%d%d%d",&n,&m,&q);
	if (n<=50000&&m<=50000&&q<=500) solve_out();
	else if (n==1) solve_pf(); 
}

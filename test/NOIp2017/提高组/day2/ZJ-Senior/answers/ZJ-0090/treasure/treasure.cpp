#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<queue>
#define ll long long
using namespace std;

const ll MX=200000000007;

int n,m;
int dis[17];
int cnt,head[17],next[2007],rea[2007],val[2007];
ll mp[20][20];
struct Node
{
	int x,y,z;
}a[1007];
struct NOO
{
	ll d[17],fee;
	void init()
	{
		for (int i=0;i<17;i++)
			d[i]=100000007;
		fee=MX;
	}
}dist[5007];
bool ins[5007];

inline int read()
{
	int x=0;char ch=getchar();
	while(ch>'9'&&ch<'0') ch=getchar();
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x;
}
void add(int u,int v,int fee)
{
	next[++cnt]=head[u];
	head[u]=cnt;
	rea[cnt]=v;
	val[cnt]=fee;
} 
void solve_pf()
{
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			mp[i][j]=MX;
	for (int i=1;i<=m;i++)
	{
		mp[a[i].x][a[i].y]=min(mp[a[i].x][a[i].y],(ll)a[i].z);
		mp[a[i].y][a[i].x]=min(mp[a[i].y][a[i].x],(ll)a[i].z);	
	}
	
	ll ans=MX;
	for (int root=1;root<=n;root++)
	{
		for (int i=1;i<(1<<n);i++)
			dist[i].init();
		memset(ins,0,sizeof(ins));	
		queue<int>q;while(!q.empty()) q.pop();
		dist[1<<(root-1)].fee=0,dist[1<<(root-1)].d[root]=0;//哪个点就哪个点。 
		q.push(1<<(root-1));ins[1<<(root-1)]=1;
		while(!q.empty())
		{
			int u=q.front();q.pop();
			for (int i=0;i<n;i++)//当前那个点 
				if ((1<<i)&u)
				{
					for (int j=0;j<n;j++)//去的那个点 
						if ((1<<j)&u) continue;
						else if (mp[i+1][j+1]!=MX)
						{
							int v=u|(1<<j);//最终那个大点 
							if (dist[u].fee+(dist[u].d[i+1]+1)*mp[i+1][j+1]<dist[v].fee)
							{
								dist[v]=dist[u];
								dist[v].d[j+1]=dist[v].d[i+1]+1;
								dist[v].fee=dist[u].fee+(dist[u].d[i+1]+1)*mp[i+1][j+1];
								if (!ins[v])
								{
									q.push(v);
									ins[v]=1; 
								}
							}
						}
				}
			ins[u]=0;	
		}
		ans=min(ans,dist[(1<<n)-1].fee);
	}
	printf("%lld",ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
	solve_pf();
}

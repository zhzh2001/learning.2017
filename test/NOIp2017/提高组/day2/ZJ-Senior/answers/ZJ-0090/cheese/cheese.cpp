#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<iostream>
#include<queue>
#define eps 0.0000001
#define N 1007
#define ll long long
using namespace std;

ll n,h,r;
int S,T;
int cnt,head[N],next[N*N*2],rea[N*N*2];
struct Node
{
	ll x,y,z;
}a[N];

inline int read()
{
	int x=0;char ch=getchar();
	while(ch>'9'&&ch<'0') ch=getchar();
	while(ch>='0'&&ch<='9'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x;
}
void add(int u,int v)
{
	next[++cnt]=head[u];
	head[u]=cnt;
	rea[cnt]=v;
}
ll sqr(ll x)
{
	return x*x;
}
bool judge(int i,int j)
{
	double t=sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z))-(double)2*r;
	if (t>eps) return false;
	else return true;
}
bool solve()
{
	bool boo[N];queue<int>q;while(!q.empty())q.pop();
	memset(boo,0,sizeof(boo));
	q.push(S);boo[S]=1;
	while(!q.empty())
	{
		int u=q.front();q.pop();
		for (int i=head[u];i!=-1;i=next[i])
		{
			int v=rea[i];
			if (v==T){boo[T]=1;break;}
			if (!boo[v]){boo[v]=1;q.push(v);}
		}
	}
	if (boo[T]) return true;
	else return false;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	int Cas=read();
	while(Cas--)
	{
		memset(head,-1,sizeof(head));cnt=0;
		scanf("%lld%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++)
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (judge(i,j)) add(i,j),add(j,i);	
		S=n+1,T=n+2;
		for (int i=1;i<=n;i++)
		{
			if (a[i].z-r<=0) add(S,i),add(i,S);
			if (a[i].z+r>=h) add(i,T),add(T,i);
		}
		if (solve()) printf("Yes\n");
		else printf("No\n");
	}
}

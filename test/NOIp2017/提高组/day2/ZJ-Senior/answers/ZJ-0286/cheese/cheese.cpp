#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const int maxn=1010;
int T,n,h,r;
bool v[maxn],yes;
struct _{
	int x,y,z;
}p[maxn];
inline bool cmp(_ a,_ b){return a.z<b.z;}
inline bool pd(int i,int j){
	double x=p[i].x-p[j].x,y=p[i].y-p[j].y,z=p[i].z-p[j].z,R=(r<<1);
	if (sqrt(x*x+y*y+z*z)<=R)return true;
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d",&n,&h,&r);
		yes=false;
		for (int i=1;i<=n;i++){
			scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
			v[i]=0;
		}
		sort(p+1,p+n+1,cmp);
		for(int i=1;i<=n;i++){
			if (p[i].z<=r)v[i]=1;
			if (p[i].z>=h-r&&v[i]){
				puts("Yes");
				yes=true;
				break;
			}
			if (!v[i])continue;
			int now=i+1;
			while(now<=n&&(p[now].z-p[i].z<=(r<<1))){
				if (!v[now])if (pd(i,now))v[now]=1;
				now++;
			}
		}
		if (!yes)puts("No");
	}
	return 0;
}

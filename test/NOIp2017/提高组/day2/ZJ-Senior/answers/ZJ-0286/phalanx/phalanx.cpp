#include<cstdio>
//#define lowbit(x) (x&(-x))
using namespace std;
const int maxn=1010;
int n,m,q;
int num[maxn][maxn],num2[200500],llw[100500]; 
bool v[100500];
inline void swap(int &a,int &b){a^=b^=a^=b;}
//void add(int x,int y){for(int i=x;i;i-=lowbit(x))num2[i]+=y;}
//int ask(int x){for (int i=x,ans=0;i;i-=lowbit(x))ans+=num2[i];return ans;}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<maxn&&m<maxn){
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				num[i][j]=(i-1)*m+j;
		while (q--){
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%d\n",num[x][y]);
			for (int i=y;i<m;i++)swap(num[x][i],num[x][i+1]);
			for (int i=x;i<n;i++)swap(num[i][m],num[i+1][m]);
		}
	}else{
		if (n==1){
			//for (int i=1;i<=m;i++)add(i,1);
			for (int i=1;i<=m;i++)num2[i]=i;
			int last=m,top=0,hh=0;
			llw[0]=0x7fffffff;
			while (q--){
				int x,y;
				scanf("%d%d",&x,&y);
				if (y<=last){
					int now=0,tmp=0;
					for (;tmp<=top;tmp++)if (y>=llw[tmp])now++;
					printf("%d\n",num2[y+now]);
					llw[++top]=num2[y+now];
					v[y+now]=1;
					last--;
				}else{
					hh=0;
					int lalal=v[1];
					for (int i=1;i<=m-1;i++){
						if (v[i])hh++,lalal--;
						for (int j=1;j<=hh&&i+j+lalal<=m;j++){
							while (v[i+j+lalal]&&i+j+lalal<=m)lalal++;
							if (v[i+j+lalal])break;
							swap(num2[i],num2[i+j+lalal]);
						}
						v[i]=0;
					}
					for (int i=1;i<=top;i++)num2[++last]=llw[i],llw[i]=0;
					top=0;
				}
			}
		}
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
const int maxm=1010;
const int maxn=15;
bool llw=1;
int n,m,ans=0x7fffffff,tmp;
int head[maxn],cnt,dep[maxn],fa[maxn],f[maxn];
int q[maxn],t,h,v[maxn];
struct Edge{
	int to,nxt,cost;
	bool can;
}e[maxm<<1];
struct _{
	int from,to,cost,num1,num2;
}E[maxm];
inline bool cmp(_ a,_ b){return a.cost<b.cost;}
void add(int a,int b,int c){
	cnt++;
	e[cnt].can=true;
	e[cnt].to=b;
	e[cnt].cost=c;
	e[cnt].nxt=head[a];
	head[a]=cnt;
}
void dfs(int rt){
	for(int i=head[rt];i;i=e[i].nxt){
		int ne=e[i].to;
		if (!e[i].can)continue;
		if (!fa[ne]){
			fa[ne]=rt;
			dep[ne]=dep[rt]+1;
			dfs(ne);
		}
	}
}
int find(int x){return ((x==f[x])?x:(f[x]=find(f[x])));}
void K(){
	int now=0;
	for (int i=0;i<m;i++){
		if (now>=n-1){
			e[E[i].num1].can=e[E[i].num2].can=false;
			continue;
		}
		int x=find(E[i].from),y=find(E[i].to);
		if (x==y){
			e[E[i].num1].can=e[E[i].num2].can=false;
			continue;
		}
		f[x]=f[y];
		now++;
	}
}
int lca(int x,int y){
	while(x!=y){
		if (dep[x]<dep[y])x^=y^=x^=y;
		if (dep[x]==dep[y])y=fa[y];
		x=fa[x];
	}
	return x;
}
void run(int rt,int last){
	for (int i=head[last];i;i=e[i].nxt){
		int ne=e[i].to;
		if (!e[i].can)continue;
		if (ne!=fa[last])tmp+=e[i].cost*(dep[ne]-dep[rt]),run(rt,ne);
	}
}
void run1(int rt,int last){
	for (int i=head[last];i;i=e[i].nxt){
		int ne=e[i].to;
		if (!e[i].can)continue;
		if (ne!=fa[last]&&ne!=rt){
			int LCA=lca(ne,rt);
			if (LCA==ne)tmp+=e[i].cost*(-dep[lca(ne,rt)]*2+dep[rt]+dep[ne]+1);
				else tmp+=e[i].cost*(-dep[lca(ne,rt)]*2+dep[rt]+dep[ne]);
			run1(rt,ne);
		}
	}
}
void work(int rt){
	for (int i=head[rt];i;i=e[i].nxt){
		int ne=e[i].to;
		if (!e[i].can)continue;
		if (ne!=fa[rt])tmp+=e[i].cost,run(rt,ne);
			else tmp+=e[i].cost;
	}
	if (rt!=1)run1(rt,1);
}
void bfs(int rt){
	h=0;
	v[q[t=1]=rt]=1;
	while (h<t){
		int x=q[++h];
		for (int i=head[x];i;i=e[i].nxt){
			int ne=e[i].to;
			if (!v[ne]){
				v[ne]=v[x]+1;
				tmp+=e[i].cost*v[x];
				q[++t]=ne;
			}
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<m;i++){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		if (i>0&&c!=E[i-1].cost)llw=0;
		add(a,b,c);
		E[i].num1=cnt;
		add(b,a,c);
		E[i].num2=cnt;
		E[i].from=a;
		E[i].to=b;
		E[i].cost=c;
	}
	if (llw){
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++)v[j]=0;
			tmp=0;
			bfs(i);
			if (tmp<ans)ans=tmp;
		}
		printf("%d\n",ans);
		return 0;
	}
	if (m!=n-1){
		sort(E,E+m,cmp);
		for (int i=1;i<=n;i++)f[i]=i;
		K();
	}
	dep[1]=fa[1]=1;
	dfs(1);
	for (int i=1;i<=n;i++){
		tmp=0;
		work(i);
		if (tmp<ans)ans=tmp;
	}
	printf("%d\n",ans);
	return 0;
}

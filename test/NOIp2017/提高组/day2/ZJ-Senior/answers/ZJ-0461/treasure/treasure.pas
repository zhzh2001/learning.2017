type arr=array[1..13]of longint;
const
  maxn=maxlongint div 3;
var
  i,j:longint;
  n,m,minn,c,d,e,h,t:int64;
  dis:array[1..30,1..30]of int64;
  f:array[1..30]of boolean;
  q,num:arr;
procedure dfs(x,sum,h,t:longint;q:arr);
var
  i,j,y,h2,t2,sumtmp,p:longint;
  f2:array[1..30]of boolean;
  q2,num2:arr;
  tt:boolean;
begin
  y:=0; f2:=f; q2:=q; h2:=h; t2:=t; num2:=num;
  if sum>=minn then exit;   tt:=true;
  for i:=1 to n do
    if f[i] then tt:=false;
  if tt=true then
  begin
    if sum<minn then minn:=sum;
    exit;
  end;
  for i:=1 to n do
    if (f[i]) and (dis[x,i]<maxn) then y:=y or (1<<(i-1));
  for p:=0 to y do
  begin
    i:=p;
   if (i and y)=i then
    begin
      sumtmp:=sum;
      for j:=1 to n do
        if (1<<(j-1) and i)=(1<<(j-1)) then
        begin
          f[j]:=false;  num[j]:=num[x]+1; sumtmp:=sumtmp+dis[x,j]*num[x];
          inc(t); q[t]:=j;
        end;
      inc(h);
      if h<=t then dfs(q[h],sumtmp,h,t,q);
      f:=f2; q:=q2; h:=h2; t:=t2; num:=num2;
    end;
  end;
end;
begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  filldword(dis,sizeof(dis) div 4,maxn);
  for i:=1 to m do
  begin
    read(c,d,e);
    if dis[c,d]>e then dis[c,d]:=e;
    if dis[d,c]>e then dis[d,c]:=e;
  end;
  fillchar(f,sizeof(f),true);
  minn:=maxlongint;
  for i:=n downto 1 do
  begin
    fillchar(f,sizeof(f),true); fillchar(q,sizeof(q),0);  fillchar(num,sizeof(num),0);
    h:=1; t:=1; q[1]:=i; f[i]:=false; num[i]:=1;
    dfs(i,0,h,t,q);
  end;
  writeln(minn);
  close(input);
  close(output);
end.

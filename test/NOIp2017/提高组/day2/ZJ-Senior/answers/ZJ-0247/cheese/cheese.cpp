#include<bits/stdc++.h>
#define N 1020
#define LL long long
using namespace std;
LL f[N],x[N],y[N],z[N];
bool ma[N][N];
void read(LL &v){
	char ch,fu=0;
	for (ch='*';(ch<'0'||ch>'9')&& ch!='-';ch=getchar());
	if (ch=='-') fu=1,ch=getchar();
	for (v=0;ch>='0'&&ch<='9';ch=getchar()) v=v*10+ch-'0';
	if (fu) v=-v;
}
LL findfa(LL u){
	if (f[u]==u) return f[u];
	return f[u]=findfa(f[u]);
}
LL dis(LL a,LL b){
	return ((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]));
}
void  deal(){
	LL n,h,r;
	//scanf("%lld%lld%lld",&n,&h,&r);
	read(n);read(h);read(r);
	for (LL i=1;i<=n;i++){
		//scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		read(x[i]);read(y[i]);read(z[i]);
	}
	
	z[0]=0; z[n+1]=h;
	memset(ma,0,sizeof(ma));
	for (LL i=1;i<=n;i++){
		if (abs(z[0]-z[i])<=r) ma[0][i]=ma[i][0]=1;
		if (abs(z[n+1]-z[i])<=r) ma[n+1][i]=ma[i][n+1]=1;
		for (LL j=1;j<i;j++) 
			if (dis(i,j)<=4*r*r) ma[j][i]=ma[i][j]=1;
	}
	//for (int i=0;i<=n+1;i++){
	//	for (int j=0;j<=n+1;j++) printf("%d ",ma[i][j]);
	//	puts("");
	//}
		
	for (LL i=0;i<=n+1;i++) f[i]=i;
	for (LL i=1;i<=n+1;i++)
		for (LL j=0;j<i;j++)
			if (ma[i][j]){
				//printf("%d %d\n",i,j);
				LL fa=findfa(i),fb=findfa(j);
				if (fa!=fb) f[fa]=fb;
			}
	LL fa=findfa(0),fb=findfa(n+1);
	if (fa==fb) puts("Yes");else puts("No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	LL T;read(T);
	while (T--) deal();
	return 0;
}

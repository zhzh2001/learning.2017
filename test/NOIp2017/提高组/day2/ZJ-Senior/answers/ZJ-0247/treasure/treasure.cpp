#include<bits/stdc++.h>
#define N 50
#define LL long long
using namespace std;
LL p[N],f[N],head[N],ma[N][N],x[N],y[N],z[N];//tim[N];
LL cnt,Ares,res,oo,n,m,ans,tmp;
bool vis[N];
struct troye{
	LL to,nxt,val;
}E[N*2];

void add(LL u,LL v,LL l){	E[++cnt].to=v;E[cnt].nxt=head[u];head[u]=cnt;E[cnt].val=l;}
void dfsb(LL s,LL dep){
	if (res>ans) { res=oo;return;}
	for (LL i=head[s];i;i=E[i].nxt){
		    if (vis[E[i].to]) continue;
			res=res+(dep+1)*E[i].val;
			if (res>ans) { res=oo;return;}
			vis[E[i].to]=1;
			tmp++;
			dfsb(E[i].to,dep+1);
		}
}
LL deal(){
	cnt=0;for (LL i=1;i<=n;i++) head[i]=0;Ares=oo;
	for (LL i=1;i<=n-1;i++){
		add(x[p[i]],y[p[i]],z[p[i]]);
		add(y[p[i]],x[p[i]],z[p[i]]);
	}
	//printf("    %d\n",cnt);
	//for (int i=1;i<=cnt;i++) printf("%d",E[i].to);
	for (LL i=1;i<=n;i++) {
		res=0;
		for (LL j=1;j<=n;j++) vis[j]=0;vis[i]=1;
		tmp=0;
		dfsb(i,0);
		if (tmp==n-1 || res==oo) Ares=min(Ares,res); else return oo;
	}
	return Ares;
}
LL findfa(LL u){	if (f[u]==u) return f[u]; return findfa(f[u]);}
void DFS(int t){
	if (t>n-1){ans=min(ans,deal());return;}
	if (p[t-1]==m) return;
	for (LL i=p[t-1]+1;i<=m;i++){
		//LL fa=findfa(x[i]);
		//LL fb=findfa(y[i]);
		//printf("%d %d\n",x[i],y[i]);
		//if (fa!=fb){			
			//f[fa]=fb;
			p[t]=i;
			DFS(t+1);
			//f[fa]=fa;
		//}
	}
}

void Rebuild(){
	m=0;
	for (LL i=2;i<=n;i++)
	 for (LL j=1;j<i;j++)
	  if (ma[i][j]!=oo){
	  	x[++m]=i;y[m]=j;z[m]=ma[i][j];
	  }
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	//read(n);read(m);
	scanf("%lld",&n);scanf("%lld",&m);
	memset(ma,127,sizeof(ma));
	oo=ma[0][0];
	for (LL i=1;i<=m;i++){
		LL u,v,l;
		scanf("%lld%lld%lld",&u,&v,&l);
		ma[u][v]=min(ma[u][v],l);
		ma[v][u]=min(ma[v][u],l);
	}
	Rebuild();
	ans=oo;
	//for (LL i=1;i<=n;i++) f[i]=i;
	DFS(1);
	printf("%lld\n",ans);
	return 0;
}
/*
*/

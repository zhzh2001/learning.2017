#include<cstdio>
#include<iostream>
#define N 1003
int s[N][N];
using namespace std;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q,x,y,temp;
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			s[i][j]=(i-1)*m+j;
		}
	}
	while(q--){
		scanf("%d%d",&x,&y);
		printf("%d\n",s[x][y]);
		temp=s[x][y];
		for(int i=y;i<m;i++){
			s[x][i]=s[x][i+1];
		}
		for(int i=x;i<n;i++){
			s[i][m]=s[i+1][m];
		}
		s[n][m]=temp;
	}
	return 0;
}

#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;
int n,m,a,b,s[2][12],rr;
bool vis[5003];
ll c,v[13][13];
ll ans=1e14,len[13];
void dfs(int mask,int now,ll sum){
	if(mask==(1<<n)-1){
		ans=min(ans,sum);
		return;
	}
	for(int i=0;i<n;i++){
		if(!(mask&(1<<i))){
			ll nowsum=1e9;
			int cnt=0;
			for(int j=0;j<n;j++){
				if((mask&(1<<j))&&v[i][j]!=1e9){
					len[i]=len[j]+1;
					nowsum=(len[j]+1)*v[i][j]+sum;
					int flag=0;
					for(int k=1;k<=cnt;k++){
						if(s[0][cnt]<=len[i]&&s[1][cnt]<=nowsum){
							flag=1;
							break;
						}
					}
					if(flag)continue;
					s[0][++cnt]=len[i];
					s[1][cnt]=nowsum;
					dfs(mask|(1<<i),i,nowsum);
				}
			}
		}
	}
	return;
}
void dfs2(int mask,int now,ll sum){
	if(mask==(1<<n)-1){
		ans=min(ans,sum*c);
		return;
	}
	for(int i=0;i<n;i++){
		if(!(mask&(1<<i))){
			ll nowsum=1e9;
			for(int j=0;j<n;j++){
				len[i]=1e9;
				if((mask&(1<<j))&&v[i][j]!=1e9){
					len[i]=min(len[i],len[j]+1);
				}
				nowsum=len[i]+sum;
				dfs2(mask|(1<<i),i,nowsum);
			}
		}
	}
	return;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;i++)for(int j=0;j<n;j++)v[i][j]=1e9;
	for(int i=1;i<=m;i++){
		scanf("%d%d%lld",&a,&b,&c);
		a--,b--;
		if(!vis[c])rr++;
		vis[c]=1;
		v[a][b]=v[b][a]=min(v[a][b],c);
	}
	if(rr==1){
		for(int i=0;i<n;i++){
			len[i]=0;
			dfs2((1<<i),i,0);
		}
		printf("%lld\n",ans);
		return 0;
	}
	for(int i=0;i<n;i++){
		if(n>=8&&i>=24/n)break;
		len[i]=0;
		dfs((1<<i),i,0);
	}
	printf("%lld\n",ans);
	return 0;
}

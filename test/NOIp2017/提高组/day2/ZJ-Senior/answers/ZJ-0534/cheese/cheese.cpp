#include<cstdio>
#include<iostream>
#include<math.h>
#include<vector>
#include<queue>
#include<cstring>
#define N 1003
#define ll long long
using namespace std;
ll T,n,h,r,flag;
ll x[N],y[N],z[N];
bool v[N];
vector<int>q[N];
queue<int>w;
bool judge(int i,int j){
	if(sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]))<=2*r)return 1;
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	while(T--){
		scanf("%lld%lld%lld",&n,&h,&r);
		memset(v,0,sizeof v);
		memset(q,0,sizeof q);
		while(!w.empty())w.pop();
		flag=0;
		for(int i=1;i<=n;i++){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		}
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				if(judge(i,j))q[i].push_back(j),q[j].push_back(i);
			}
		}
		for(int i=1;i<=n;i++){
			if(z[i]-r<=0)v[i]=1,w.push(i);
		}
		while(!w.empty()){
			int now=w.front();
			w.pop();
			for(int i=0;i<q[now].size();i++){
				int to=q[now][i];
				if(!v[to]){
					v[to]=1;
					w.push(to);
				}
			}
		}
		for(int i=1;i<=n;i++){
			if(z[i]+r>=h&&v[i]){
				flag=1;
				break;
			}
		}
		if(flag)printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

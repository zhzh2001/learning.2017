#include <cstdio>
#include <cstring>
int min(int x,int y)
{
	return x<y? x:y;
}
int n,m,res=2e9,d[13][13],y[13],w[13],jl[13],pr[13];
void work(int o)
{
	memset(y,0,sizeof y);
	for(int i=1;i<=n;i++) jl[i]=2e9;
	jl[o]=0;
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		int id=0,di=2e9;
		for(int j=1;j<=n;j++) if(!y[j] && jl[j]<di) di=jl[j],id=j;
		y[id]=1; w[id]=w[pr[id]]+1;
		ans+=di;
		for(int j=1;j<=n;j++) if(!y[j])
			if(d[id][j]!=2e9)
				if(w[id]*d[id][j]<jl[j])
					jl[j]=w[id]*d[id][j],pr[j]=id;
	}
	res=min(ans,res);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			d[i][j]=2e9;
	for(int i=1;i<=m;i++)
	{
		int u,v,s;
		scanf("%d%d%d",&u,&v,&s);
		d[u][v]=d[v][u]=min(d[u][v],s);
	}
	for(int i=1;i<=n;i++) work(i);
	printf("%d\n",res);
	return 0;
}

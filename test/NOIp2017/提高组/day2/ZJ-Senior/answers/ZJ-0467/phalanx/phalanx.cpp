#include <cstdio>
#include <cctype>
int read()
{
	char c; while(!isdigit(c=getchar())); int x=c-'0';
	while(isdigit(c=getchar())) x=x*10+c-'0'; return x;
}
int a[1001][1001];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n=read(),m=read(),q=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=(i-1)*m+j;
	while(q--)
	{
		int x=read(),y=read();
		int k=a[x][y];
		printf("%d\n",k);
		for(int i=y+1;i<=m;i++)
			a[x][i-1]=a[x][i];
		for(int i=x+1;i<=n;i++)
			a[i-1][m]=a[i][m];
		a[n][m]=k;
	}
	return 0;
}

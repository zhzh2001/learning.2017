#include <cstdio>
#include <cctype>
#include <cmath>
int t,n,fa[1001];
long long r,h,x[1001],y[1001],z[1001];
bool ye1[1001],ye2[1001];
int find(int x)
{
	return x==fa[x]? x:fa[x]=find(fa[x]);
}
unsigned long long sqr(long long x)
{
	return x*x;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		bool ans=0;
		scanf("%d%lld%lld",&n,&h,&r);
		for(int i=1;i<=n;i++)
		{
			ye1[i]=0;
			ye2[i]=0;
			fa[i]=i;
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if(z[i]+r>=h) ye1[i]=1;
			if(z[i]-r<=0) ye2[i]=1;
			if(ye1[i] && ye2[i]) ans=1;
		}
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=sqr(2*r))
				{
					int u=find(i),v=find(j);
					fa[v]=u;
					ye1[u]|=ye1[v];
					ye2[u]|=ye2[v];
					if(ye1[u] && ye2[u]) ans=1;
				}
		puts(ans? "Yes":"No");
	}
	return 0;
}

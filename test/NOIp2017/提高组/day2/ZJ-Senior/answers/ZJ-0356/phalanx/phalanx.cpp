//Future && You
//				__wyxoi
#include <set>
#include <map>
#include <cmath>
#include <string>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
const int N = 1010;
const int M = 300010;
using namespace std;
int id[N][N],n,m,Q,head[M],tail[M],H,T,blk,num;
struct data{int pre,nxt,val;} p[M];
int read(){
	int x = 0,f = 1,c = getchar();
	while (c<'0' || c>'9') {if (c=='-') f = -1;c = getchar();}
	while (c>='0' && c<='9') x = x*10+c-'0',c = getchar();
	return x*f;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read();m = read();Q = read();
	if (n<=1000 && m<=1000) {
		int tmp = 0;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++) id[i][j] = ++tmp;
		while (Q--) {
			int x = read(),y = read();tmp = id[x][y];
			for (int i=y;i<m;i++) id[x][i] = id[x][i+1];
			for (int i=x;i<n;i++) id[i][m] = id[i+1][m];
			id[n][m] = tmp;
			printf("%d\n",tmp);
		}
	}
	if (n==1) {
		blk = sqrt(m);
		num = (m-1)/blk+1;
		H = 0;T = n+1;
		for (int i=1;i<=m;i++) p[i].pre = i-1,p[i].nxt = i+1,p[i].val = i;
		for (int i=1;i<=num;i++) head[i] = blk*(i-1)+1,tail[i] = min(m,blk*i);
		while (Q--) {
			int qu = read();qu = read();
			int x = (qu-1)/blk+1,y = qu%blk;if (y==0) y+=blk;
			int tmp = head[x];
			for (int i=1;i<y;i++) tmp = p[tmp].nxt;
			printf("%d\n",p[tmp].val);
			int pre = p[tmp].pre,nxt = p[tmp].nxt;
			if (y==1) head[x] = nxt;
			p[pre].nxt = nxt;p[nxt].pre = pre;
			p[tmp].nxt = p[tail[num]].nxt;
			p[tmp].pre = tail[num];
			p[tail[num]].nxt = tmp;
			for (int i=x;i<=num;i++) {
				if (i!=x) head[i] = p[head[i]].nxt;
				tail[i] = p[tail[i]].nxt;
			}
		}
	}
}

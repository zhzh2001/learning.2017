//Future && You
//				__wyxoi
#include <set>
#include <map>
#include <cmath>
#include <string>
#include <cstdio>
#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>
const int N = 20;
using namespace std;
int w[N][N],dis[N],q[N],vis[N],ans,cnt,n,m,S,now,top,s[N],stk[N];
struct data{int x,y,s,v;} e[N*N];
int read(){
	int x = 0,f = 1,c = getchar();
	while (c<'0' || c>'9') {if (c=='-') f = -1;c = getchar();}
	while (c>='0' && c<='9') x = x*10+c-'0',c = getchar();
	return x*f;
}
void dfs(int k){
	if (k>n) {
		if (ans>now) {
			for (int i=1;i<=n;i++) s[i] = stk[i];
			ans = now;
		}
		return;
	}
	if (k==1) {
		for (int i=1;i<=n;i++) if (!vis[i]) {
			vis[i] = 1;dis[i] = 1;stk[++top] = i;
			dfs(k+1);vis[i] = 0;stk[top--] = 0;;
		}
		return;
	}
	for (int i=1;i<=n;i++) if (!vis[i]) {
		vis[i] = 1;int md = 10,Min = 1e9;
		for (int j=1;j<=n;j++) if (vis[j] && w[j][i]) {
			if (dis[j]*w[j][i]<Min) Min = dis[j]*w[j][i],md = dis[j];
			if (dis[j]*w[j][i]==Min) md = min(md,dis[j]);
		}
		if (Min==1e9) {vis[i] = 0;continue;}
		dis[i] = md+1;now+=Min;stk[++top] = i;
		dfs(k+1);
		vis[i] = 0;now-=Min;stk[top--] = 0;
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n = read();m = read();ans = 1e9;
	memset(w,0,sizeof(w));
	for (int i=1;i<=m;i++) {
		int x = read(),y = read(),s = read();
		if (w[x][y]) w[x][y] = w[y][x] = min(w[x][y],s);
		else w[x][y] = w[y][x] = s;
	}
	dfs(1);
//	for (int i=1;i<=n;i++) {for (int j=1;j<=n;j++) printf("% 5d",w[i][j]);puts("");}
	printf("%d\n",ans);
//	for (int i=1;i<=n;i++) printf("%d ",s[i]);
}

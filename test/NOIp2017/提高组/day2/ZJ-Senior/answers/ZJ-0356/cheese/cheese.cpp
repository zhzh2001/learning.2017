//Future && You
//				__wyxoi
#include <set>
#include <map>
#include <cmath>
#include <string>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
const int N = 1010;
using namespace std;
struct data{ll x,y,z;} p[N];
int vis[N],w[N][N],n,S,T,q[N];
ll R,H;
int read(){
	int x = 0,f = 1,c = getchar();
	while (c<'0' || c>'9') {if (c=='-') f = -1;c = getchar();}
	while (c>='0' && c<='9') x = x*10+c-'0',c = getchar();
	return x*f;
}
int Judge(int a,int b){
	ll tmp = (p[a].x-p[b].x)*(p[a].x-p[b].x);
	tmp+=(p[a].y-p[b].y)*(p[a].y-p[b].y);
	tmp+=(p[a].z-p[b].z)*(p[a].z-p[b].z);
	return tmp<=(4*R*R);
}
int bfs() {
	for (int i=1;i<=T;i++) vis[i] = 0;
	vis[S] = 1;q[1] = S;int h = 0,t = 1;
	while (h<t) {
		int now = q[++h];
		for (int i=1;i<=T;i++)
			if (w[now][i] && !vis[i]) {vis[i] = 1;q[++t] = i;}
	}
	return vis[T];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (int TT=read();TT;TT--) {
		n = read();H = read();R = read();S = n+1;T = S+1;
		for (int i=1;i<=n;i++) p[i].x = read(),p[i].y = read(),p[i].z = read();
		memset(w,0,sizeof(w));
		for (int i=1;i<=n;i++) {
			if (p[i].z<=R) w[S][i] = w[i][S] = 1;
			if (H-p[i].z<=R) w[T][i] = w[i][T] = 1;
			for (int j=i+1;j<=n;j++) if (Judge(i,j)) w[i][j] = w[j][i] = 1;
		}
		if (bfs()) printf("Yes\n");
		else printf("No\n");
	}
}

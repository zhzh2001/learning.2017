#include<bits/stdc++.h>
using namespace std;
int A[1005][1005];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q;
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)A[i][j]=m*(i-1)+j;
	while(q--){
		int x,y,res;
		scanf("%d%d",&x,&y);
		res=A[x][y];
		printf("%d\n",res);
		for(int i=y;i<=m;i++)A[x][i]=A[x][i+1];
		for(int i=x;i<=n;i++)A[i][m]=A[i+1][m];
		A[n][m]=res;
	}
	return 0;
}

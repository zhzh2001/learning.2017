#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,h,r;
struct node{
	ll x,y,z;//坐标 
}A[1005];
vector<int>edge[1005];
bool Q[1005],Q1[1005],Q2[1005],p;//这个点是否走过    这个点是否与顶相连   这个点是否和底相连 
bool judge(int i,int j){
	double dis=sqrt(1.0*(A[i].x-A[j].x)*(A[i].x-A[j].x)+1.0*(A[i].y-A[j].y)*(A[i].y-A[j].y)+1.0*(A[i].z-A[j].z)*(A[i].z-A[j].z));
	return dis<=2*r;
}
void f(int x){
	if(Q[x])return;Q[x]=1;
	if(Q1[x]){p=1;return;}
	for(int i=0;i<(int)edge[x].size();i++){
		f(edge[x][i]);
		if(p)return;
	}
}
void Clear(){
	for(int i=1;i<=n;i++){
		edge[i].clear();
		Q[i]=Q1[i]=Q2[i]=p=0;
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--){
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&A[i].x,&A[i].y,&A[i].z);
			for(int j=1;j<i;j++)
				if(judge(i,j)){
					edge[i].push_back(j);
					edge[j].push_back(i);
				}
			if(A[i].z+r>=h)Q1[i]=1;
			if(A[i].z<=r)Q2[i]=1;
		}
		for(int i=1;i<=n;i++)if(Q2[i])f(i);
		if(p)puts("Yes");else puts("No");
		Clear();
	}
	return 0;
}

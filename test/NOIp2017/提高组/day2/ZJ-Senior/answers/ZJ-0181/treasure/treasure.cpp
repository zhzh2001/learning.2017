#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,m,dis[15][15];
struct PP1{
	ll dp[1<<13],cnt[15];
	void solve(){
		ll ans=1e9;
		for(int l=1;l<=n;l++){
			memset(dp,63,sizeof dp);
			memset(cnt,0,sizeof cnt);
			dp[l]=0;
			for(int i=1<<(l-1);i<(1<<n);i++)
				for(int j=1;j<=n;j++)
					if(i&(1<<(j-1))&&dp[i]!=dp[0])
						for(int k=1;k<=n;k++)
							if(dis[j][k]!=1e9&&!(i&(1<<(k-1)))){
								int now=i|(1<<(k-1));
								if(dp[now]>dp[i]+dis[j][k]*(cnt[j]+1)){
									dp[now]=dp[i]+dis[j][k]*(cnt[j]+1);
									cnt[k]=cnt[j]+1;
								}else
								if(dp[now]==dp[i]+dis[j][k]*(cnt[j]+1)){
									cnt[k]=cnt[j]+1;
								}
							}
			ans=min(ans,dp[(1<<n)-1]);
		}
		printf("%lld\n",ans);
	}
}P1;
struct PP2{
	ll D[15][1<<15],dp[15][1<<15],ans;
	void solve(){
		for(int i=1;i<=n;i++){
			for(int j=0;j<(1<<n);j++)D[i][j]=1e9;
			D[i][0]=0;
		}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(dis[i][j]!=1e9&&i!=j)
					for(int k=0;k<(1<<n);k++)
						if(!(k&(1<<(j-1)))&&D[i][k]!=1e9&&!(k&(1<<(i-1)))){
							int now=k|(1<<(j-1));
							D[i][now]=min(D[i][now],D[i][k]+dis[i][j]);
						}
		ans=1e9;
		for(int l=1;l<=n;l++){
			memset(dp,127,sizeof dp);
			dp[0][1<<(l-1)]=0;
			for(int dep=1;dep<=n;dep++)//深度
				for(int i=1<<(l-1);i<(1<<n);i++)//此时状态
					if(dp[dep-1][i]!=dp[0][0])
					for(int j=1;j<=n;j++)//枚举一个点
						if(i&(1<<(j-1)))
							for(int k=0;k<(1<<n);k++)//枚举下一层
								if(D[j][k]!=1e9&&(k&i)==0){
									int now=k|i;
									dp[dep][now]=min(dp[dep][now],dp[dep-1][i]+dep*D[j][k]);
								}
			for(int i=0;i<=n;i++)ans=min(ans,dp[i][(1<<n)-1]);
		}
		printf("%lld\n",ans);
	}
}P2;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)dis[i][j]=1e9;
	int p=0,q=0;
	for(int i=1;i<=m;i++){
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		if(i==1)q=v;if(q!=v)p=1;
		if(x>y)swap(x,y);
		dis[x][y]=min(dis[x][y],v);
		dis[y][x]=dis[x][y];
	}
	if(!p)P1.solve();else P2.solve();
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
#include<iostream>
#define LL long long 
using namespace std;
const int maxn=1100;
LL tot,n,h,r,t,s,l,last[maxn],que[maxn*2];
struct A{
	LL x,y,z;
}crc[maxn];
bool vis[maxn];
struct B{
	int to,pre;
}q[maxn*maxn*2];
void add_edge(int x,int y){
	q[++tot].to=y;
	q[tot].pre=last[x];
	last[x]=tot;
}
LL sqr(LL x) {return x*x;} 
void solve(){
	memset(vis,0,sizeof(vis));
	memset(last,0,sizeof(last));
	scanf("%lld%lld%lld",&n,&h,&r);
	s=0;t=n+1;
	tot=0;
	for (int i=1;i<=n;i++){
		scanf("%lld%lld%lld",&crc[i].x,&crc[i].y,&crc[i].z);
		if (crc[i].z<=r) add_edge(s,i);
		if (abs(h-crc[i].z)<=r) add_edge(i,t);
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if ((i!=j)&&sqrt(sqr(crc[i].x-crc[j].x)+sqr(crc[i].y-crc[j].y)+sqr(crc[i].z-crc[j].z))<=2*r){
				add_edge(i,j);
				add_edge(j,i);
			}
		}
	}
	int l=0;r=1;
	que[1]=0;
	while (l<r){
		int now=que[++l];
		for (int i=last[now];i;i=q[i].pre){
			int v=q[i].to;
			if (!vis[v]){
				que[++r]=v;
				vis[v]=1;
				if (v==t) break;
			}
		}
		if (vis[t]) break;
	}
	if (vis[t]) printf("Yes\n");
	else printf("No\n");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&l);
	while (l--) solve();
	return 0;
}

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
const int INF=1e9;
int n,m,u,v,w,tot,ans,minn,last[20],dis[20],pre[20],val,d[20][20];
bool vis[20];
void search(int s){
	ans=0;
	memset(pre,0,sizeof(pre));
	memset(vis,0,sizeof(vis));
	for (int i=1;i<=n;i++) dis[i]=INF;
	dis[s]=1;
	for (int i=1;i<=n;i++) 
		if (d[s][i]!=0) dis[i]=2,pre[i]=s;
	vis[s]=1;
	for (int i=1;i<n;i++){
		int minn=INF,k=-1;
		for (int j=1;j<=n;j++)
			if (!vis[j]&&dis[j]<minn) {minn=dis[j];k=j;}
		vis[k]=1;
		for (int l=1;l<=n;l++){
			if (d[k][l]!=0&&dis[k]+1<dis[l]) {dis[l]=dis[k]+1;pre[l]=k;}
		}			
	}
	for (int i=1;i<=n;i++) ans+=dis[pre[i]];
	if (ans<minn) minn=ans;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		if (d[u][v]==0) d[u][v]=w;
		else d[u][v]=min(d[u][v],w);
		if (d[v][u]==0) d[v][u]=w;
		else d[v][u]=min(d[v][u],w);	
	}
	val=w;minn=INF;
	for (int s=1;s<=n;s++) search(s);
	printf("%d\n",minn*w);
	return 0;
}


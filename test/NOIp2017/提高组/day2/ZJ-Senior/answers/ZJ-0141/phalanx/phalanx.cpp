#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<cstdlib>
using namespace std;
const int maxn=1010,maxm=300100;
int n,m,q,x,y,k,tot,a[maxn][maxn],b[maxm*2],add[maxm*2],fa[maxm*2][20],pre[maxm*2],last,two[20];
void refather(){
	for (int i=1;i<=k;i++)
		for (int j=0;j<=tot;j++) {
			fa[j][i]=fa[fa[j][i-1]][i-1];
		}
}
int getfather(int x){
	int t=0,father=0;
	for (int i=k;i>=0;i--){
		if (t+two[i]<=x) t=t+two[i],father=fa[father][i];
	}
	return father;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000&&m<=1000){
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				a[i][j]=(i-1)*m+j; 
		for (int t=1;t<=q;t++){
			scanf("%d%d",&x,&y);
			printf("%d\n",a[x][y]);
			int d=a[x][y];
			for (int j=y;j<m;j++) a[x][j]=a[x][j+1];
			for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
			a[n][m]=d; 
		}
	}
	else{
		for (int i=1;i<=m;i++) b[i]=i,fa[i][0]=i+1,pre[i]=i-1;
		fa[0][0]=1;
		tot=m;
		for (int i=2;i<=n;i++) b[++tot]=i*m,fa[tot][0]=tot+1,pre[tot]=tot-1;
		k=log(tot)/log(2);
		two[0]=1;
		for (int i=1;i<=k;i++) two[i]=two[i-1]*2;
		refather();
		last=tot;
		for (int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			printf("%d\n",b[getfather(y)]);
			fa[pre[y]][0]=fa[y][0];
			fa[last][0]=y;
			pre[fa[y][0]]=pre[y];
			fa[y][0]=0;
			pre[y]=last;
			last=y;
			refather();
		}
	}
	return 0;
}

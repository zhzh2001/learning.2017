#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define maxn 1006
using namespace std;
inline char nc(){
	static char buf[100000],*i=buf,*j=buf;
	return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;
}
inline int _read(){
	char ch=nc();int sum=0,p=1;
	while(ch!='-'&&!(ch>='0'&&ch<='9'))ch=nc();
	if(ch=='-')p=-1,ch=nc();
	while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();
	return sum*p;
}
struct data{
	double x,y,z;
}a[maxn];
int T,n;
double h,r;
bool dis[maxn],vis[maxn];
double sqr(double x){return x*x;}
double getdis(int i,int j){
	if(i==0)return a[j].z;if(j==0)return a[i].z;
	if(i==n+1)return h-a[j].z;if(j==n+1)return h-a[i].z;
	return sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z));
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=_read();
	while(T--){
		n=_read();h=_read();r=_read();
		for(int i=1;i<=n;i++)a[i].x=_read(),a[i].y=_read(),a[i].z=_read();
		memset(dis,0,sizeof(dis));memset(vis,1,sizeof(vis));dis[0]=1;
		while(1){
			int k=-1;
			for(int i=0;i<=n+1;i++) if(vis[i]&&dis[i]){k=i;break;}
			if(k==-1||k==n+1)break;vis[k]=0;
			int t;if(k==0)t=n;else t=n+1;
			for(int i=0;i<=t;i++) if(vis[i]&&getdis(k,i)<=r*2+1e-7)dis[i]=1;
		}
		if(dis[n+1])printf("Yes\n");else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 15
#define maxs 4106
using namespace std;
inline char nc(){
	static char buf[100000],*i=buf,*j=buf;
	return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;
}
inline int _read(){
	char ch=nc();int sum=0,p=1;
	while(ch!='-'&&!(ch>='0'&&ch<='9'))ch=nc();
	if(ch=='-')p=-1,ch=nc();
	while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();
	return sum*p;
}
int n,e,ans,INF,g[maxn][maxn],f[maxn][maxs],f1[maxs][maxs];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=_read();e=_read();
	memset(g,63,sizeof(g));INF=g[0][0];
	for(int i=1;i<=e;i++){
		int x=_read(),y=_read(),z=_read();
		if(z<g[x][y])g[x][y]=g[y][x]=z;
	}
	for(int i=1;i<=n;i++)g[i][i]=0;
	memset(f1,63,sizeof(f1));
	for(int i=1;i<1<<n;i++)
	 for(int j=1;j<1<<n;j++) if((i&j)==j){
	 	f1[i][j]=0;
	 	for(int k=1;k<=n;k++) if((i>>(k-1))&1){
	 		int Min=INF;
	 		for(int t=1;t<=n;t++) if((j>>(t-1))&1)Min=min(Min,g[t][k]);
	 		if(f1[i][j]<INF)f1[i][j]+=Min;
	 		if(Min>=INF)break;
		}
	 }
	memset(f,63,sizeof(f));
	for(int i=1;i<=n;i++)f[0][1<<(i-1)]=0;
	for(int i=1;i<=n;i++)
	 for(int j=1;j<1<<n;j++)
	  for(int k=1;k<1<<n;k++) if((k&j)==k&&f1[j][k]<INF&&f[i-1][k]<INF)
	   f[i][j]=min(f[i][j],f[i-1][k]+f1[j][k]*i);
	ans=INF;
	for(int i=0;i<=n;i++)ans=min(ans,f[i][(1<<n)-1]);
	printf("%d\n",ans);
	return 0;
}

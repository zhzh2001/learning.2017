#include<cstdio>
#include<cstring>
#include<algorithm>
#define maxn 1006
using namespace std;
inline char nc(){
	static char buf[100000],*i=buf,*j=buf;
	return i==j&&(j=(i=buf)+fread(buf,1,100000,stdin),i==j)?EOF:*i++;
}
inline int _read(){
	char ch=nc();int sum=0,p=1;
	while(ch!='-'&&!(ch>='0'&&ch<='9'))ch=nc();
	if(ch=='-')p=-1,ch=nc();
	while(ch>='0'&&ch<='9')sum=sum*10+ch-48,ch=nc();
	return sum*p;
}
int n,m,T,map[maxn][maxn];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=_read();m=_read();T=_read();
	for(int i=1;i<=n;i++)
	 for(int j=1;j<=m;j++)map[i][j]=(i-1)*m+j;
	while(T--){
		int x=_read(),y=_read();
		printf("%d\n",map[x][y]);
		int t=map[x][y];
		for(int i=y;i<m;i++)map[x][i]=map[x][i+1];
		for(int i=x;i<n;i++)map[i][m]=map[i+1][m];
		map[n][m]=t;
	}
	return 0;
}

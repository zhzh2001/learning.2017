#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<string>
#include<string.h>
#include<bitset>
#include<set>
#include<map>
#include<vector>
#include<bitset>
#include<queue>
#include<stdlib.h>
#define ll long long
#define N 15
#define oo 10000000000000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
void write(ll x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
void writeln(ll x){
	if(x<0) putchar('-'),x=-x;
	write(x);putchar('\n');
}
int n,m,len[N][N],root,pap,vis[N];
ll sum,ans;
inline void dfs(int x,int dep){
	int q[N];int top=0;
	memset(q,0,sizeof q);
	For(i,1,n) if(len[x][i]!=pap&&!vis[i]) q[++top]=i,vis[i]=1;
	For(i,1,top) sum+=len[x][q[i]]*dep,dfs(q[i],dep+1);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();ans=oo;
	memset(len,63,sizeof len);
	pap=len[1][1];
	For(i,1,m){
		int x=read(),y=read(),w=read();
		len[x][y]=min(len[x][y],w);
		len[y][x]=min(len[y][x],w);
	}
	For(i,1,n){
		memset(vis,0,sizeof vis);
		vis[i]=1;
		dfs(i,1);ans=min(ans,sum);
	}
	writeln(ans);
	return 0;
}

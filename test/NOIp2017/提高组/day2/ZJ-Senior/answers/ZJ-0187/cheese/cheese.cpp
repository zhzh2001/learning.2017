#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<string>
#include<string.h>
#include<bitset>
#include<set>
#include<map>
#include<vector>
#include<bitset>
#include<queue>
#include<stdlib.h>
#define ll long long
#define N 1005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
void writeln(int x){
	if(x<0) putchar('-'),x=-x;
	write(x);putchar('\n');
}
int n,h,R,q[N],l,r;
bool vis[N],g[N];
struct data{int x,y,z;}a[N];
ll sqr(int x){return 1ll*x*x;}
ll dis(int x,int y){
	return 1ll*sqr(a[x].x-a[y].x)+sqr(a[x].y-a[y].y)+sqr(a[x].z-a[y].z);
}
void solve(){
	n=read();h=read();R=read();
	memset(vis,0,sizeof vis);
	memset(g,0,sizeof g);
	l=1;r=0;
	For(i,1,n){
		a[i]=(data){read(),read(),read()};
		if(abs(a[i].z)-R<=0) q[++r]=i,vis[i]=1;
		if(a[i].z<=h){
			if(a[i].z+R>=h) g[i]=1;
		}else{
			if(a[i].z-R<=h) g[i]=1;
		}
	}
	while(l<=r){
		int x=q[l];l++;
		if(g[x]){puts("Yes");return;}
		For(i,1,n) if(!vis[i]&&dis(x,i)<=1ll*R*R*4) q[++r]=i,vis[i]=1;
	}
	puts("No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--) solve();
	return 0;
}

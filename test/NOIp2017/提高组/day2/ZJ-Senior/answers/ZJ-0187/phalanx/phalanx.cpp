#include<stdio.h>
#include<iostream>
#include<algorithm>
#include<math.h>
#include<string>
#include<string.h>
#include<bitset>
#include<set>
#include<map>
#include<vector>
#include<bitset>
#include<queue>
#include<stdlib.h>
#define ll long long
#define N 300005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define Forn(i,x,y) for(int i=x;i<y;i++)
using namespace std;
int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
void write(int x){
	if(x<10) putchar(x+48);
	else write(x/10),putchar(x%10+48);
}
void writeln(int x){
	if(x<0) putchar('-'),x=-x;
	write(x);putchar('\n');
}
int n,m,a[1005][1005];
inline void solve(){
	int tot=0;
	For(i,1,n) For(j,1,m) a[i][j]=++tot;
	int Q=read();
	while(Q--){
		int x=read(),y=read(),p=a[x][y];
		Forn(i,y,m) a[x][i]=a[x][i+1];
		Forn(i,x,n) a[i][m]=a[i+1][m];
		a[n][m]=p;writeln(p);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();
	if(n<=1000&&m<=1000) solve();
	return 0;
}

program jiang;
var a:array[1..5000,1..5000] of longint;
    n,m,p,i,j,x,y,ans:longint;

procedure work;
begin
  read(n,m,p);
  for i:=1 to n do
    for j:=1 to m do
      a[i,j]:=(i-1)*m+j;
  for j:=1 to p do
    begin
      read(x,y);
      ans:=a[x,y];
      writeln(ans);
      for i:=y to m-1 do 
        a[x,i]:=a[x,i+1];
      for i:=x to n-1 do
        a[i,m]:=a[i+1,m];
      a[n,m]:=ans;
    end;
  close(input); close(output);
end;
   
begin
  assign(input,'phalanx.in'); reset(input);
  assign(output,'phalanx.out'); rewrite(output);
  work;
  close(input); close(output);
end.
    
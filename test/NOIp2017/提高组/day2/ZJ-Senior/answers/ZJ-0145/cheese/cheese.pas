program jiang;
var e,n,h,r,i,t,s,edg:longint;
    x,y,z:array[1..1005] of longint;
    dis:array[0..1005] of boolean;
    head,vet,next:array[0..1005] of longint;

procedure add(u,v:longint);
begin
  inc(edg);
  vet[edg]:=v;
  next[edg]:=head[u];
  head[u]:=edg;
end;

procedure init; 
var i,j:longint;
begin
  fillchar(x,sizeof(x),0);
  fillchar(y,sizeof(y),0);
  fillchar(z,sizeof(z),0);
  fillchar(dis,sizeof(dis),false);
  fillchar(vet,sizeof(vet),0);
  fillchar(head,sizeof(head),0);
  fillchar(next,sizeof(next),0);
  read(n,h,r);
  for i:=1 to n do
    read(x[i],y[i],z[i]);
  for i:=1 to n do
    begin
    if z[i]+r>=h then begin add(e,i); add(i,e);  end;
    if z[i]-r<=0 then begin add(s,i); add(i,s);  dis[i]:=true; end;
    end;
  for i:=1 to n-1 do
    for j:=i+1 to n do
      if sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=sqr(2*r) then begin add(i,j); add(j,i); end;
end;

procedure SPFA;
var t,h,edg,v,u,i:longint; 
    duilie:array[0..1005] of longint;
begin 
  fillchar(duilie,sizeof(duilie),0);
  t:=0; h:=0;
  for i:=1 to n do
    if dis[i] then begin inc(t); duilie[t]:=i;  end;
  while h<t do
    begin
      inc(h);
      u:=duilie[h]; edg:=head[u]; 
      while edg<>0 do
      begin
        v:=vet[edg];
        if dis[u] and not dis[v] then begin dis[v]:=true; inc(t); duilie[t]:=v; end;
        edg:=next[edg];
      end; 
    end;
end;
      
begin
  assign(input,'cheese.in'); reset(input);
  assign(output,'cheese.out'); rewrite(output);
  s:=0; e:=1001;
  readln(t);
  for i:=1 to t do
    begin
      init;
      SPFA;
      if dis[e] then writeln('Yes') else writeln('No');
    end;
  close(input); close(output);
end.
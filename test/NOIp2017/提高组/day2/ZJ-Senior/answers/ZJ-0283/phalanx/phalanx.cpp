#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<assert.h>
using namespace std;
#define bug(x) cout<<#x<<"="<<x<<" "
#define debug(x) cout<<#x<<"="<<x<<endl
#define prt(x) cout<<x<<" "
template<class T>void rd(T &a){
	a=0;char c;
	while(c=getchar(),!isdigit(c));
	do a=a*10+(c^48);
		while(c=getchar(),isdigit(c));
}
template<class T>void nt(T x){
	if(!x)return;
	nt(x/10),putchar(x%10+48);
}
template<class T>void pt(T x){
	if(!x)putchar(48);
	else nt(x);
}
typedef long long ll;
int n,m,q;
const int M=3e5+5;
const int N=M*2;
struct Splay_Tree{
	int tr[N][2],fa[N],sz[N],Rt,allc;
	ll id[N];
	Splay_Tree(){
		allc=Rt=0;
	}
	#define ls tr[x][0]
	#define rs tr[x][1]
	void up(int &x){
		sz[x]=sz[ls]+sz[rs]+1;
	}
	void rotate(int x,int &rt){
		int y=fa[x],z=fa[y],r=tr[y][0]==x,l=r^1;
		if(y==rt)rt=x;
		else{
			if(tr[z][0]==y)tr[z][0]=x;
			else tr[z][1]=x;
		}
		fa[x]=z,fa[y]=x,fa[tr[x][r]]=y;
		tr[y][l]=tr[x][r],tr[x][r]=y;
		up(y),up(x);
	}
	void Splay(int x,int &rt){
		for(;x!=rt;){
			int y=fa[x],z=fa[y];
			if(y!=rt){
				if(tr[y][0]==x^tr[z][0]==y)rotate(y,rt);
				else rotate(x,rt);
			}
			rotate(x,rt);
		}
		up(x);
	}
	int find_kth(int k,int x){
		if(k==sz[ls]+1)return x;
		if(k<=sz[ls])return find_kth(k,ls);
		return find_kth(k-sz[ls]-1,rs);
	}
	void insert(ll _id){//在树的末尾插入元素 新元素编号为_id,在树中为allc 
		id[++allc]=_id;
		if(!Rt)return Rt=allc,sz[allc]=1,void();
		
		int w=find_kth(sz[Rt],Rt);
		Splay(w,Rt);
		tr[w][1]=allc,fa[allc]=w;
		up(allc),up(w);
	}
	ll erase(int k){//删除第k大数 
		if(k==1){
			int x=find_kth(k,Rt);
			Splay(x,Rt),fa[rs]=0,Rt=rs;
			return id[x];
		}
		if(k==sz[Rt]){
			int x=find_kth(k,Rt);
			Splay(x,Rt),fa[ls]=0,Rt=ls;
			return id[x];
		}
		int x=find_kth(k+1,Rt);
		Splay(x,Rt);
		int y=find_kth(k-1,Rt);
		Splay(y,tr[x][0]);
		ll res=id[tr[y][1]];
		tr[y][1]=0;
		up(y),up(x);
		return res;
	}
	#undef ls
	#undef rs
}spt;
int _x[505],_y[505];
namespace bf{

int mark[(int)5e4+5];
ll mat[505][(int)5e+4+5];//96M

void solve(){//id(i,j):(i-1)*m+j
	memset(mark,-1,sizeof(mark));
	static int stk[505],tp;
	tp=0; 
	for(int i=1;i<=q;++i)
		if(mark[_x[i]]==-1)mark[_x[i]]=1,stk[tp++]=_x[i];
	sort(stk,stk+tp);
	
	for(int i=0;i<tp;++i){
		mark[stk[i]]=i;
		for(int j=1;j<=m;++j)
			mat[i][j]=(ll)(stk[i]-1)*m+j;
	}
	
	for(int i=1;i<=n;++i)
		mat[tp][i]=(ll)i*m;
	
	for(int i=1;i<=q;++i){
		int x=_x[i],y=_y[i],px=mark[x];
		ll res=mat[px][y];
		cout<<res<<endl;
		for(int j=y;j<m;++j)
			swap(mat[px][j],mat[px][j+1]);
		
		for(int j=x;j<n;++j)
			swap(mat[tp][j],mat[tp][j+1]);
		
		mat[tp][n]=res;
		
		for(int j=0;j<tp;++j)
			mat[j][m]=mat[tp][stk[j]];	
		
	}
}

}//namespace bf ended
namespace Line{

void solve(){//id(i,j):(i-1)*m+j
	for(int i=1;i<=m;++i)
		spt.insert(i);
	for(int i=1;i<=q;++i){
		ll w=spt.erase(_y[i]);
		spt.insert(w);
		pt(w),putchar('\n');
	}
}

}//namespace Line ended
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	cin>>n>>m>>q;
	bool flag=1;
	for(int i=1;i<=q;++i){
		rd(_x[i]),rd(_y[i]);
		flag&=_x[i]==1;
	}
	if(q<=500)return bf::solve(),0;
	if(n==1&&flag)return Line::solve(),0;
	
	return 0;
}

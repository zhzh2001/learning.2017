#include<bits/stdc++.h>
using namespace std;
const int M=1e5+5;
typedef long long ll;
const int N=2e5+5;
#define bug(x) cout<<#x<<"="<<x<<" "
#define debug(x) cout<<#x<<"="<<x<<endl
#define prt(x) cout<<x<<" "
struct Splay_Tree{
	int tr[N][2],fa[N],sz[N],Rt,allc;
	ll id[N];
	Splay_Tree(){
		allc=Rt=0;
	}
	#define ls tr[x][0]
	#define rs tr[x][1]
	void up(int &x){
		sz[x]=sz[ls]+sz[rs]+1;
	}
	void rotate(int x,int &rt){
		int y=fa[x],z=fa[y],r=tr[y][0]==x,l=r^1;
		if(y==rt)rt=x;
		else{
			if(tr[z][0]==y)tr[z][0]=x;
			else tr[z][1]=x;
		}
		fa[x]=z,fa[y]=x,fa[tr[x][r]]=y;
		tr[y][l]=tr[x][r],tr[x][r]=y;
		up(y),up(x);
	}
	void Splay(int x,int &rt){
		for(;x!=rt;){
			int y=fa[x],z=fa[y];
			if(y!=rt){
				if(tr[y][0]==x^tr[z][0]==y)rotate(y,rt);
				else rotate(x,rt);
			}
			rotate(x,rt);
		}
		up(x);
	}
	int find_kth(int k,int x){
		if(k==sz[ls]+1)return x;
		if(k<=sz[ls])return find_kth(k,ls);
		return find_kth(k-sz[ls]-1,rs);
	}
	void insert(ll _id){//在树的末尾插入元素 新元素编号为_id,在树中为allc 
		id[++allc]=_id;
		if(!Rt)return Rt=allc,sz[allc]=1,void();
		
		int w=find_kth(sz[Rt],Rt);
		Splay(w,Rt);
		tr[w][1]=allc,fa[allc]=w;
		up(allc),up(w);
	}
	ll erase(int k){//删除第k大数 
		if(k==1){
			int x=find_kth(k,Rt);
			Splay(x,Rt),fa[rs]=0,Rt=rs;
			return id[x];
		}
		if(k==sz[Rt]){
			int x=find_kth(k,Rt);
			Splay(x,Rt),fa[ls]=0,Rt=ls;
			return id[x];
		}
		int x=find_kth(k+1,Rt);
		Splay(x,Rt);
		int y=find_kth(k-1,Rt);
		Splay(y,tr[x][0]);
		ll res=id[tr[y][1]];
		tr[y][1]=0;
		up(y),up(x);
		return res;
	}
	#undef ls
	#undef rs
}spt;
int main(){
	int n,q;cin>>n>>q;
	for(int i=1;i<=n;++i)
		spt.insert(i);
	char c[10];
	for(int i=1,w;i<=q;++i){
		cin>>c>>w;
		if(c[0]=='E')cout<<spt.erase(w)<<endl;
		else spt.insert(w);
	}
	
	return 0;
}

#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<assert.h>
using namespace std;
#define bug(x) cout<<#x<<"="<<x<<" "
#define debug(x) cout<<#x<<"="<<x<<endl
#define prt(x) cout<<x<<" "
const int M=12;
int n,m;
template<class T>void Min(T &a,T b){
	if(a>b||a==-1)a=b;
}
int dp[1<<M][M];
int mp[M][M];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	memset(mp,-1,sizeof(mp));
	for(int i=1,a,b,c;i<=m;++i){
		scanf("%d%d%d",&a,&b,&c),--a,--b;
		Min(mp[a][b],c),Min(mp[b][a],c);
	}
	static bool mark[M];
	static int stk[M],tp,val[M];
	memset(dp,-1,sizeof(dp));
	for(int i=0;i<n;++i)
		dp[1<<i][0]=0;
	int ans=-1;
	for(int h=1;h<n;++h){//枚举深度 
		for(int s=1;s<1<<n;++s){//上一层的元素 
			if(dp[s][h-1]==-1)continue;
			memset(mark,0,sizeof(mark));
			memset(val,-1,sizeof(val));
			tp=0;
			for(int i=0;i<n;++i){
				if(s&1<<i)mark[i]=1;
				else stk[tp++]=i;
			}
			for(int i=0;i<tp;++i){
				int v=stk[i];
				for(int j=0;j<n;++j){
					if(!mark[j])continue;
					if(mp[v][j]==-1)continue;
					Min(val[v],mp[v][j]);
				}
			}
			for(int i=0;i<tp;++i)
				if(val[stk[i]]==-1)swap(stk[i],stk[--tp]),--i;
					//如果这个点不可能做到和深度为h的点连边就把它删掉 
			for(int _s=1;_s<1<<tp;++_s){
				int w=0,s_=0;
				for(int i=0;i<tp;++i)
					if(_s&1<<i)w+=val[stk[i]],s_|=1<<stk[i];
				assert((s&s_)==0);
				Min(dp[s|s_][h],dp[s][h-1]+w*h);
			}
		}
		if(~dp[(1<<n)-1][h])Min(ans,dp[(1<<n)-1][h]);
	}
	cout<<ans<<endl;
	return 0;
}

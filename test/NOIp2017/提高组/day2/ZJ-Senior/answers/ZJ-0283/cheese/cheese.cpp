#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
using namespace std;
#define bug(x) cout<<#x<<"="<<x<<" "
#define debug(x) cout<<#x<<"="<<x<<endl
#define prt(x) cout<<x<<" "
const int M=1e3+5;
int x[M],y[M],z[M];
struct Edge{
	int to,nxt;
	Edge(int a,int b):to(a),nxt(b){}
	Edge(){}
}g[M*M*2];
int head[M],tot_edge;
void add_edge(int from,int to){
	g[tot_edge]=Edge(to,head[from]);
	head[from]=tot_edge++;
}
bool mark[M];
void dfs(int v){
	mark[v]=1;
	for(int i=head[v];~i;i=g[i].nxt){
		int to=g[i].to;
		if(!mark[to])dfs(to);
	}
}
typedef long long ll;
ll Pow(int x){
	return (ll)x*x;
}
int n,h,r;
bool check(int i,int j){
	return Pow(x[i]-x[j])+Pow(y[i]-y[j])+Pow(z[i]-z[j])<=Pow(2*r);
}
void gao(){
	cin>>n>>h>>r;
	for(int i=1;i<=n;++i)
		scanf("%d%d%d",x+i,y+i,z+i);
	memset(head,-1,sizeof(head));
	tot_edge=0;
	for(int i=1;i<=n;++i){
		if(z[i]-r<=0){
			add_edge(n+1,i);
//			prt("Edge"),cout<<n+1<<" "<<i<<endl;
		}
		if(z[i]+r>=h){
			add_edge(i,0);
//			prt("Edge"),cout<<i<<" "<<0<<endl;
		}
		for(int j=i+1;j<=n;++j){
			if(check(i,j))add_edge(i,j),add_edge(j,i);
//			prt("Edge"),cout<<i<<" "<<j<<endl;
		}
	}
	memset(mark,0,sizeof(mark));
	//bottom: n+1(z=0)  top: 0(z=h)
	dfs(n+1);
	puts(mark[0]?"Yes":"No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int _;
	for(cin>>_;_--;)gao();
	return 0;
}

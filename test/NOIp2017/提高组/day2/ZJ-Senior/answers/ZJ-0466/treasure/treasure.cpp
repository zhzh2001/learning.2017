#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <ext/pb_ds/priority_queue.hpp>
#define maxV 17
#define INF 0x3f3f3f3f
#define ID isdigit(c = fgetc(f))
#define RT if(!~c) return flag = 0, *this
using __gnu_pbds::priority_queue;

struct Istream{
	FILE *f; int flag;
	void init(FILE *_f = stdin) {f = _f;}
	Istream& operator >> (int &x){
		int l = 0, c;
		for(; !ID; l = c) RT;
		for(x = c & 15; ID; x = x * 10 + (c & 15)) RT;
		if(l == 45) x = -x; RT;
		return flag = 1, *this;
	}
}Cin;

struct Ostream{
	FILE *f; char buf[34];
	void init(FILE *_f = stdout) {f = _f;}
	Ostream& operator << (int x){
		if(!x) return put(48), *this;
		if(x < 0) {put(45); x = -x;}
		int i;
		for(i = 0; x; x /= 10) buf[++i] = x % 10 | 48;
		for(; i; --i) put(buf[i]);
		return *this;
	}
	Ostream& operator << (char x) {return put(x), *this;}
	void put(int x) {fputc(x, f);}
}Cout;

struct pr{
	int to, wt;
	pr (int to0 = 0, int wt0 = 0): to(to0), wt(wt0) {}
	bool operator < (const pr &b) const {return wt > b.wt;}
};

typedef priority_queue <pr> pqr;

int V, E, i, j;
int u, v, w, ans;
int G[maxV][maxV], dep[maxV];
int d[maxV], c[maxV];

pqr pq;
FILE *fin, *fout;

inline void down(int &x, const int y) {x > y ? x = y : 0;}

int Priority(int si){
	int x, y, w;
	int cost = 0; pr t;
	memset(dep, 63, sizeof dep);
	memset(d, 63, sizeof d);
	c[si] = 0; dep[0] = 0; d[si] = 0;
	pq.clear(); pq.push(pr(si, 0));
	for(; !pq.empty(); ){
		t = pq.top(); pq.pop(); x = t.to;
		if(dep[x] < INF) continue;
		cost += t.wt;
		dep[x] = dep[c[x]] + 1;
		for(y = 1; y <= V; ++y)
			if(dep[y] >= INF && G[x][y] < INF){
				w = G[x][y] * dep[x];
				if(w < d[y]){
					d[y] = w; c[y] = x;
					pq.push(pr(y, d[y]));
				}
			}
	}
	return cost;
}

int main(){
	Cin.init(fin = fopen("treasure.in", "r"));
	Cout.init(fout = fopen("treasure.out", "w"));
	Cin >> V >> E;
	memset(G, 63, sizeof G);
	for(i = 1; i <= V; ++i) G[i][i] = 0;
	for(i = 1; i <= E; ++i){
		Cin >> u >> v >> w;
		down(G[u][v], w); G[v][u] = G[u][v];
	}
	ans = INT_MAX;
	for(i = 1; i <= V; ++i)
		down(ans, Priority(i));
	Cout << ans << '\n';
	fclose(fin); fclose(fout);
	return 0;
}


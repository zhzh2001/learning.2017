#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>
#define N 341468
#define Q 341468
#define Z 6829360
#define ID isdigit(c = fgetc(f))
#define RT if(!~c) return flag = 0, *this
using namespace std;

struct Istream{
	FILE *f; int flag;
	void init(FILE *_f = stdin) {f = _f;}
	Istream& operator >> (int &x){
		int l = 0, c;
		for(; !ID; l = c) RT;
		for(x = c & 15; ID; x = x * 10 + (c & 15)) RT;
		if(l == 45) x = -x; RT;
		return flag = 1, *this;
	}
}Cin;

struct Ostream{
	FILE *f; char buf[34];
	void init(FILE *_f = stdout) {f = _f;}
	Ostream& operator << (long long x){
		if(!x) return put(48), *this;
		if(x < 0) {put(45); x = -x;}
		int i;
		for(i = 0; x; x /= 10) buf[++i] = x % 10 | 48;
		for(; i; --i) put(buf[i]);
		return *this;
	}
	Ostream& operator << (char x) {return put(x), *this;}
	void put(int x) {fputc(x, f);}
}Cout;

//struct node {int lc, rc, v;} st[Z];

typedef long long ll;

struct request{
	int r, c; ll ans;
	request * read() {Cin >> r >> c; return this;}
}req[Q];

int R, C, q;
int r, c, i, j, k;
int last[N], prev[Q]; // adjacent list
//int cnt = 0, root[Q];

FILE *fin, *fout;
/*
inline int min(const int x, const int y) {return x < y ? x : y;}
inline int max(const int x, const int y) {return x < y ? y : x;}
inline void up(int &x, const int y) {x < y ? x = y : 0;}

int build(int L, int R){
	int id = ++cnt;
	if(L == R) {st[id].lc = st[id].rc = st[id].v = 0; return id;}
	int M = L + R - 1 >> 1;
	st[id].lc = build(L, M); st[id].rc = build(M + 1, R); st[id].v = 0;
	return id;
}

int adj(int id, int L, int R, int h, int v){
	int nid = ++cnt; st[nid] = st[id];
	if(L == R) {st[nid].v = v; return nid;}
	int M = L + R - 1 >> 1;
	h <= M ? st[nid].lc = adj(st[id].lc, L, M, h, v) : (st[nid].rc = adj(st[id].rc, M + 1, R, h, v));
	st[nid].v = max(st[st[nid].lc].v, st[st[nid].rc].v);
	return nid;
}

int range(int id, int L, int R, int ql, int qr){
	if(ql <= L && R <= qr) return st[id].v;
	int M = L + R - 1 >> 1, s = 0;
	if(ql <= M) up(s, range(st[id].lc, L, M, ql, min(qr, M)));
	if(qr > M) up(s, range(st[id].rc, M + 1, R, max(ql, M + 1), qr));
	return s;
}
*/

int main(){
	Cin.init(fin = fopen("phalanx.in", "r"));
	Cout.init(fout = fopen("phalanx.out", "w"));
	Cin >> R >> C >> q;
	memset(last, -1, sizeof last);
	//root[0] = build(1, R);
	for(i = 1; i <= q; ++i){
		req[i].read(); r = req[i].r; c = req[i].c;
		j = i;
		if(c < C)
			for(j = last[r]; j >= 0; j = prev[j])
				if(req[j].c <= c && ++c >= C) break;
		if(c < C)
			req[i].ans = (ll)(r - 1) * C + c;
		else{ // c == C
			for(--j; j; --j)
				if(r == R) break;
				else if(req[j].r <= r) ++r;
			/*for(--j; r < R; ++r){
				j = range(root[j], 1, R, 1, r) - 1;
				if(j == -1) break;
			}*/
			if(r == R && j) req[i].ans = req[j].ans;
			else req[i].ans = (ll)(r - 1) * C + c;
		}
		Cout << req[i].ans << '\n';
		r = req[i].r;
		prev[i] = last[r]; last[r] = i;
		//root[i] = adj(root[i - 1], 1, R, r, i);
	}
	fclose(fin); fclose(fout);
	return 0;
}


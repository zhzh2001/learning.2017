#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>
#define N 1034
#define ID isdigit(c = fgetc(f))
#define RT if(!~c) return flag = 0, *this
using namespace std;

struct Istream{
	FILE *f; int flag;
	void init(FILE *_f = stdin) {f = _f;}
	Istream& operator >> (int &x){
		int l = 0, c;
		for(; !ID; l = c) RT;
		for(x = c & 15; ID; x = x * 10 + (c & 15)) RT;
		if(l == 45) x = -x; RT;
		return flag = 1, *this;
	}
}Cin;

struct Ostream{
	FILE *f; char buf[34];
	void init(FILE *_f = stdout) {f = _f;}
	Ostream& operator << (const char *x){
		for(char *p = (char*)x; *p; ++p) put(*p); return *this;
	}
	void put(int x) {fputc(x, f);}
}Cout;

typedef long long ll;

bool ok;
int n, h, r, i, j;
int p[N], x[N], y[N], z[N];
ll Rad2;

FILE *fin, *fout;

int ancestor(int x) {return p[x] == x ? x : (p[x] = ancestor(p[x]));}

inline void Union(int x, int y) {if((x = ancestor(x)) != (y = ancestor(y))) p[x] = y;}

inline bool test(int i, int j){
	int dX = x[i] - x[j], dY = y[i] - y[j], dZ = z[i] - z[j];
	ll Dist2 = (ll)dX * dX + (ll)dY * dY + (ll)dZ * dZ;
	return Dist2 <= Rad2;
}

int main(){
	int T;
	Cin.init(fin = fopen("cheese.in", "r"));
	Cout.init(fout = fopen("cheese.out", "w"));
	for(Cin >> T; T; --T){
		Cin >> n >> h >> r; Rad2 = (ll)r * r << 2;
		for(i = 1; i <= n; ++i) Cin >> x[i] >> y[i] >> z[i];
		for(i = 1; i <= n + 2; ++i) p[i] = i;
		for(i = 1; i <= n; ++i){
			if(z[i] <= r) Union(n + 1, i);
			if(z[i] >= h - r) Union(n + 2, i);
			for(j = i + 1; j <= n; ++j)
				if(test(i, j)) Union(i, j);
		}
		ok = ancestor(n + 1) == ancestor(n + 2);
		Cout << (ok ? "Yes\n" : "No\n");
	}
	fclose(fin); fclose(fout);
	return 0;
}


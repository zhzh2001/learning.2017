#include <bits/stdc++.h>

using namespace std;

typedef long long LL;
const int maxn = 15,inf = 1e9+7;
int n,m,s[maxn],vis[maxn],mp[maxn][maxn];
LL ans,mx;

inline int read() {
	int ret = 0,f = 1;char ch = getchar();
	while (ch<'0' || ch>'9') {if (ch == '-') f = -f;ch = getchar();}
	while (ch>='0' && ch<='9') {ret = 10*ret+ch-'0';ch = getchar();}
	return ret*f;
}

struct node {
	int u,v,w;
};

struct cmp {
	bool operator () (node &a,node &b) const {
		return a.w > b.w;
	}
};

priority_queue <node,vector<node>,cmp> q;

inline void dfs(int k,int sum) {
	if (sum == n) return;
	for (int i=1;i<=n;i++) {
		if (vis[i] || i==k) continue;
		if (mp[k][i]) q.push((node){k,i,mp[k][i]*s[k]});
	}
	node p = q.top();q.pop();
	while (vis[p.v])  p = q.top(),q.pop();
	mx += p.w,s[p.v] = s[p.u]+1,vis[p.v] = 1;
	dfs(p.v,sum+1);
}

int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n = read(),m = read();
	memset(mp,0,sizeof mp);
	for (int i=1;i<=m;i++) {
		int u,v,w;
		u = read(),v = read(),w = read();
		mp[u][v] = ((mp[u][v] == 0) ? w : min(mp[u][v],w));
		mp[v][u] = ((mp[v][u] == 0) ? w : min(mp[v][u],w));
	}
	ans = inf;
	for (int i=1;i<=n;i++) {
		memset(s,0,sizeof s);
		memset(vis,0,sizeof vis);
		s[i] = 1,mx = 0,vis[i] = 1;
		dfs(i,1);ans = min(ans,mx);
	}
	printf("%lld\n",ans);
}

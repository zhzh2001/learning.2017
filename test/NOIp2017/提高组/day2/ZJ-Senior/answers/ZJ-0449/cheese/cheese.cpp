#include <bits/stdc++.h>

using namespace std;

typedef long long LL;
const int maxn = 1e3+10,maxm = 1e6+10;
LL t,n,h,r,m;
int edge,vis[maxn],head[maxn];
queue <int> q;

struct node {
	LL x,y,z;
}c[maxn];

struct Edge {
	int nxt,cnt;
}e[maxm << 1];

inline bool cmp(node a,node b) {
	return a.z < b.z;
}

inline LL read() {
	LL ret = 0,f = 1;char ch = getchar();
	while (ch<'0' || ch>'9') {if (ch == '-') f = -f;ch = getchar();}
	while (ch>='0' && ch<='9') {ret = 10*ret+ch-'0';ch = getchar();}
	return ret*f;
}

inline bool pd(int i,int j) {
	return ((c[i].x-c[j].x)*(c[i].x-c[j].x)+(c[i].y-c[j].y)*(c[i].y-c[j].y)+(c[i].z-c[j].z)*(c[i].z-c[j].z) <= m);
}

inline void add_edge(int u,int v) {
	e[edge] = (Edge){head[u],v};
	head[u] = edge++;
}

inline bool bfs() {
	while (!q.empty()) {
		int u = q.front();q.pop();
		if (c[u].z>=h-r && c[u].z<=h+r) return true; 
		for (int i=head[u];i!=-1;i=e[i].nxt) {
			int v = e[i].cnt;
			if (vis[v]) continue;
			vis[v] = 1,q.push(v);
		}
	}
	return false;
}

inline void init() {
	m = 4*r*r,edge = 0;
	memset(vis,0,sizeof vis);
	memset(head,-1,sizeof head);
	while (!q.empty()) q.pop();
}

int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t = read();
	while (t--) {
		n = read(),h = read(),r = read();
		init(); 
		for (int i=1;i<=n;i++) c[i].x = read(),c[i].y = read(),c[i].z = read();
		sort(c+1,c+n+1,cmp);
		if (c[1].z>r || c[1].z<-r || c[n].z<h-r) {printf("No\n");continue;}
		for (int i=1;i<=n;i++) {
			if (c[i].z<=r && c[i].z>=-r) q.push(i),vis[i] = 1;
			for (int j=1;j<=n;j++) {
				if (j == i) continue;
				if (pd(i,j)) add_edge(i,j);
			}
		}
		//cout << "popopo" << endl; 
		if (bfs()) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

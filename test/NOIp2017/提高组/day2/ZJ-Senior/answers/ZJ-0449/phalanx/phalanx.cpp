#include <bits/stdc++.h>

using namespace std;

typedef long long LL;
const int maxn = 1010,maxm = 1e6+10;
int n,m,q,mp[maxn][maxn],np[2][maxm];

inline int read() {
	int ret = 0,f = 1;char ch = getchar();
	while (ch<'0' || ch>'9') {if (ch == '-') f = -f;ch = getchar();}
	while (ch>='0' && ch<='9') {ret = 10*ret+ch-'0';ch = getchar();}
	return ret*f;
}

int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read(),m = read(),q = read();
	if (n != 1) {
		for (int i=1;i<=n;i++) {
			for (int j=1;j<=m;j++) {
				mp[i][j] = (i-1)*m+j;
			}
		}
		while (q--) {
			int x = read(),y = read();
			printf("%d\n",mp[x][y]);
			int u = mp[x][y];
			for (int i=y;i<m;i++) mp[x][i] = mp[x][i+1];
			for (int i=x;i<n;i++) mp[i][m] = mp[i+1][m];
			mp[n][m] = u;
		}
	}
	else {
		for (int i=1;i<=n;i++) {
			for (int j=1;j<=m;j++) {
				np[i][j] = (i-1)*m+j;
			}
		}
		while (q--) {
			int x = read(),y = read();
			printf("%d\n",np[x][y]);
			int u = np[x][y];
			for (int i=y;i<m;i++) np[x][i] = np[x][i+1];
			for (int i=x;i<n;i++) np[i][m] = np[i+1][m];
			np[n][m] = u;
		}
	}
	return 0;
}

#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
int n,m,cnt,ans=0x3f;
int head[13],next[5001],w[5001],to[5001];
struct Edge
{
	int val,to;
};
bool cmp(Edge a,Edge b)
{
	return a.val<b.val;
}
inline ll read()
{
	ll x=0;
	bool f=0;
	char c=getchar();
	while(c<'0'||c>'9')
	{
		if(c=='-')f=1;
		c=getchar();
	}
	while(c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return f?-x:x;
}
inline void addedge(int a,int b,int c)
{
	cnt++;
	next[cnt]=head[a];
	to[cnt]=b;
	w[cnt]=c;
	head[a]=cnt;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++)
	{
		int a=read(),b=read(),c=read();
		addedge(a,b,c);
		addedge(b,a,c);
	}
	for(int t=1;t<=n;t++)
	{
		int l=1,i=t,nn=n-1,sum=0,cn=0,dep[13];
		memset(dep,0,sizeof(dep));dep[t]=1;
		Edge tt[5001];
		while(nn)
		{	
			for(int j=head[i];j;j=next[j])
			if(!dep[to[j]])
			{
				tt[++cn].val=dep[i]*w[j];
				tt[cn].to=to[j];
				sort(tt+l,tt+1+cn,cmp);
			}
			sum+=tt[l].val;
			dep[tt[l].to]=dep[i]+1;
			i=tt[l++].to;
			nn--;
		}
		ans=min(ans,sum);
	}
	cout<<ans;
	return 0;
}

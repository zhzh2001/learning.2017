#include<iostream>
#include<cmath>
#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
struct Point
{
	ll x,y,z;
}po[3100];
ll t,n,h,r;
inline ll read()
{
	ll x=0;
	bool f=0;
	char c=getchar();
	while(c<'0'||c>'9')
	{
		if(c=='-')f=1;
		c=getchar();
	}
	while(c>='0'&&c<='9')
	{
		x=(x<<3)+(x<<1)+c-'0';
		c=getchar();
	}
	return f?-x:x;
}
inline bool cmp(Point a,Point b)
{
	return a.z<b.z;
}
inline bool link(int a,int b)
{
	ll xx=po[a].x-po[b].x;
	ll yy=po[a].y-po[b].y;
	ll zz=po[a].z-po[b].z;
	return xx*xx+yy*yy+zz*zz<=4*r*r;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read();h=read();r=read();
		for(int i=1;i<=n;i++)
		{
			po[i].x=read();
			po[i].y=read();
			po[i].z=read();
		}
		sort(po+1,po+n+1,cmp);
		if(po[1].z-r>0||po[n].z+r<h)
		cout<<"No"<<endl;
		else
		{
			int i=1;
			bool ff=0;
			if(po[i].z+r>=h)
			{
				cout<<"Yes"<<endl;
				ff=1;
			}
			while(!ff)
			{
				int temp=i;
				ll del=po[i].z+2*r;
				for(int j=i+1;po[j].z<=del;j++)
				{
					if(link(i,j))
					{
						i=j;
						if(po[i].z+r>=h)
						{
							cout<<"Yes"<<endl;
							ff=1;
						}
						break;
					}
				}
				if(i==temp)
				{
					cout<<"No"<<endl;
					ff=1;
				}
			}
		}
	}
	return 0;
}

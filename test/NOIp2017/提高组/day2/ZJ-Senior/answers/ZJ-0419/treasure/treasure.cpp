#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
using namespace std;
#define INF 0x3f3f3f3f
bool inq[20];
int dep[20],w[20][20],q[10000];
int n,m,ans,minn=INF;
int a,b,c;

void work(int root){
	memset(inq,false,sizeof(inq));
	memset(dep,0x3f,sizeof(dep));
	dep[root]=0; q[1]=root; inq[root]=true;
	int head=1,tail=1;
	while (head<=tail){
		int now=q[head];
		for (int i=1; i<=n; i++)	
			if (i!=now&&dep[now]+1<dep[i]&&w[now][i]!=INF){
				dep[i]=dep[now]+1;
				if (!inq[i]) q[++tail]=i,inq[i]=true;
			}
		head++;
	}
	ans=0;
	for (int i=1; i<=n; i++)
		ans+=dep[i]*w[a][b];
	minn=min(minn,ans);
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(w,0x3f,sizeof(w));
	for (int i=1; i<=m; i++){
		scanf("%d%d%d",&a,&b,&c);
		w[a][b]=min(w[a][b],c);
		w[b][a]=w[a][b]; 
	}
	for (int i=1; i<=n; i++)
		work(i);
	printf("%d\n",minn);
	return 0;
}

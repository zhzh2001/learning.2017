#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
bool vis[1111];
int x[1111],y[1111],z[1111],e[1111][1111],num[1111];
int T,n,h;
long long r;

void dfs(int x){
	if (x==n+1||vis[n+1]) return;
	for (int i=1; i<=num[x]; i++)
		if (!vis[e[x][i]]){
			vis[e[x][i]]=true;
			dfs(e[x][i]);
			//vis[e[x][i]]=false;
		}
	return;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(num,0,sizeof(num));
		scanf("%d%d%lld",&n,&h,&r);
		long long r2=4*r*r;
		for (int i=1; i<=n; i++){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			//cout<<x[i]<<" "<<y[i]<<" "<<z[i]<<endl;
			for (int j=1; j<=i-1; j++)
				if (r2>=(ll)(x[i]-x[j])*(ll)(x[i]-x[j])+(ll)(y[i]-y[j])*(ll)(y[i]-y[j])+(ll)(z[i]-z[j])*(ll)(z[i]-z[j])){
					e[i][++num[i]]=j;
					e[j][++num[j]]=i;
				}
			if (z[i]<=r) e[i][++num[i]]=0,e[0][++num[0]]=i;
			if (z[i]+r>=h) e[i][++num[i]]=n+1,e[n+1][++num[n+1]]=i;
		}
		memset(vis,false,sizeof(vis)); vis[0]=true;
		dfs(0);
		if (vis[n+1]) printf("Yes\n");
			else printf("No\n");
	}
	return 0;
}

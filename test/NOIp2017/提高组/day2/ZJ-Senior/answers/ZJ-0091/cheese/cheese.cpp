#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
#define mem(x,y) memset(x,y,sizeof(x))
#define sqr(x) ((x)*(x))
using namespace std;
typedef long long ll;
typedef long double db;
const int N=1010;
inline void read(int &x)
{
	x=0; int f=1; char ch=getchar();
	while((ch<'0' || ch>'9') && ch!='-') ch=getchar(); if(ch=='-') {f=-1; ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0'; ch=getchar();}
	x*=f;
}
int n; ll h,r;
struct P
{
	ll x,y,z;
	inline void in() {scanf("%lld %lld %lld",&x,&y,&z);}
}p[N];
int head[N],to[N*N*2],nxt[N*N*2],lst=0;
int s[N],e[N],cs=0,ce=0;
bool used[N];
int que[N<<1],he=0,ta=1;
inline void adde(int x,int y) {nxt[++lst]=head[x]; to[lst]=y; head[x]=lst;}
inline ll dist(P a,P b) {return sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z);}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T; scanf("%d",&T);
	while(T--)
	{
//		printf("?%lld?",sqr(2000000000ll));
		lst=0; mem(head,0); cs=ce=0; he=0; ta=0; mem(used,0);
		scanf("%d %lld %lld",&n,&h,&r);
		for(int i=1;i<=n;i++)
		{
			p[i].in();
			if(p[i].z-r<=0) s[++cs]=i;
			if(p[i].z+r>=h) e[++ce]=i;
		}
		for(int i=1;i<=n;i++)
			for(register int j=i+1;j<=n;j++)
				if(dist(p[i],p[j])<=sqr(2*r))
					{adde(i,j); adde(j,i);}
		for(int i=1;i<=cs;i++) que[++ta]=s[i];
		while(he<ta)
		{
			he++; int p=que[he];
			for(int i=head[p];i;i=nxt[i])
				if(!used[to[i]]) {used[to[i]]=1; que[++ta]=to[i];} 
		}
		bool f=0;
		for(int i=1;i<=ce;i++) if(used[e[i]]) {puts("Yes"); f=1; break;}
		if(!f) puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
#define mem(x,y) memset(x,y,sizeof(x))
#define sqr(x) ((x)*(x))
using namespace std;
typedef long long ll;
typedef long double db;
const int N=20,M=400;
const ll INF=1e15;
inline void read(int &x)
{
	x=0; int f=1; char ch=getchar();
	while((ch<'0' || ch>'9') && ch!='-') ch=getchar(); if(ch=='-') {f=-1; ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0'; ch=getchar();}
	x*=f;
}
int n,m;
ll ans=INF;
int G[N][N];
bool used[N];
ll dist[N];
int has[N];
int head[N],nxt[N*N*2],to[N*N*2],v[N*N*2],lst;
inline void adde(int x,int y,int c) {nxt[++lst]=head[x]; to[lst]=y; v[lst]=c; head[x]=lst;}
void dfs(int dep,ll now)
{
//	printf("%d %d\n",dep,now);
	if(now>=ans) return;
	if(dep==n)
	{
		ans=now; return;
	}
	for(int tt=1;tt<=dep;tt++)
	{
		int i=has[tt];
		for(int j=head[i];j;j=nxt[j])
		{
			int t=to[j];
//			printf("%d %d %d %d\n",i,t,v[j],G[i][t]);
			if(!used[t])
			{
				has[dep+1]=t; used[t]=1; dist[t]=dist[i]+1;
				dfs(dep+1,now+dist[t]*v[j]);
				has[dep+1]=0; used[t]=0; dist[t]=INF;
			}
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int x,y,c;
	mem(G,63);
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d %d %d",&x,&y,&c);
		if(G[x][y]>c) G[x][y]=G[y][x]=c;
	}
//	for(int i=1;i<=n;i++)
//	{
//		for(int j=1;j<=m;j++)
//			printf("%d ",G[i][j]==G[0][0] ? 0 : G[i][j]);
//		puts("");
//	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(G[x][y]!=G[0][0])
				{adde(i,j,G[i][j]); adde(j,i,G[i][j]);}
	if(n<=12)
	{
		for(int i=1;i<=n;i++) dist[i]=INF;
		for(int i=1;i<=n;i++)
		{
			has[1]=i; used[i]=1; dist[i]=0;
			dfs(1,0);
			has[1]=0; used[i]=0; dist[i]=INF;
		}
		printf("%lld",ans);
		return 0;
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<vector>
#include<set>
#define mem(x,y) memset(x,y,sizeof(x))
#define sqr(x) ((x)*(x))
#define pb push_back
#define pii pair<int,ll>
#define mp make_pair
#define F first
#define S second
using namespace std;
typedef long long ll;
typedef long double db;
const int N=300010;
inline void read(int &x)
{
	x=0; int f=1; char ch=getchar();
	while((ch<'0' || ch>'9') && ch!='-') ch=getchar(); if(ch=='-') {f=-1; ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0'; ch=getchar();}
	x*=f;
}
int n,m,q;
ll G[1010][1010];//b1
vector<ll> line;
vector<ll> col;
vector<pii> lin[N];
ll rol[N];
ll que[N<<2],h=0,t=0;
struct Q {int x,y;inline void in() {scanf("%d %d",&x,&y);}} qu[N];
inline int bs(int x,int y)
{
	int l=0,r=lin[x].size()-1;
	while(l<=r)
	{
		int mid=l+r>>1;
		if(lin[x][mid].F==y) return mid;
		else if(lin[x][mid].F>y) {r=mid-1;}
		else l=mid+1;
	}
	return -1;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	vector<ll> :: iterator p;
	vector<pii> :: iterator p1;
	int x,y;
	scanf("%d %d %d",&n,&m,&q);
	if(n<=1000 && m<=1000)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				G[i][j]=(i-1)*m+j;
		while(q--)
		{
			read(x); read(y); ll v=G[x][y];
			printf("%lld\n",v);
			for(int i=y;i<m;i++) G[x][i]=G[x][i+1];
			for(int i=x;i<n;i++) G[i][m]=G[i+1][m];
			G[n][m]=v;
		}
		return 0;
	}
	bool f=1;
	for(int i=1;i<=q;i++) {qu[i].in(); f&=(qu[i].x==1);}
	if(f)
	{
		line.resize(m);
		for(int i=0;i<m;i++) line[i]=i+1;
		for(int i=1;i<n;i++) que[++t]=1ll*m*(i+1);
		for(int i=1;i<=q;i++)
		{
			int y=qu[i].y;
			p=line.begin()+y-1; ll v=*p;
			printf("%lld\n",v);
			line.erase(p);
			que[++t]=v; line.pb(que[++h]);
		}
		return 0;
	}
	col.resize(n);
	for(int i=0;i<n;i++) col[i]=m*(i+1);
	for(int t=1;t<=q;t++)
	{
		int x=qu[t].x,y=qu[t].y;
		int pos=bs(x,y);
		if(pos==-1)
		{
			if(y==m)
			{
				p=col.begin()+x-1;
				int v=*p;
				printf("%d\n",v);
				col.erase(p);
				col.pb(v);
				continue;
			}
			printf("%lld\n",(x-1)*m+y+rol[x]);
			rol[x]++;
			p=col.begin()+x-1;
			int v=*p;
			lin[x].pb(mp(m-1,v));
			col.erase(p);
			col.pb((x-1)*m+y+rol[x]-1);
		}
		else
		{
			p1=lin[x].begin()+pos;
			int v=p1->S;
			printf("%d\n",v);
			lin[x].erase(p1);
			p=col.begin()+x-1;
			v=*p;
			lin[x].pb(mp(m,v));
			int len=lin[x].size();
			for(int j=pos;j<len;j++) lin[x][j].F--;
			col.pb(v);
		}
//		printf("col:"); for(int i=0;i<n;i++) printf("%d ",col[i]); puts("");
	}
	
	return 0;
}
/*1 10 3
1 5
1 7
1 6
1 9
1 10
1 3*/

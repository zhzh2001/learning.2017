#include <queue>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
const int N=100;
int n, m, tmp, head[N], cnt;
struct Edge{
	int to, next, w;
}edge[10000];
void AddEdge(int u, int v, int w){
	edge[++cnt]=(Edge){ v, head[u], w }; head[u]=cnt;
	edge[++cnt]=(Edge){ u, head[v], w }; head[v]=cnt;
}
void dfs(int u, int fa, int dep){
	for(int i=head[u]; i; i=edge[i].next)
	{
		int v=edge[i].to;
		if(v==fa) continue;
		tmp+=edge[i].w*dep;
		dfs(v, u, dep+1);
	}
}
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("tree.out","w",stdout);
	scanf("%d%d", &n, &m);
	rep(i, 1, m)
	{
		int u, v, w;
		scanf("%d%d%d", &u, &v, &w);
		AddEdge(u, v, w);
	}
	int ans=2e9;
	rep(s, 1, n)
	{
		tmp=0;
		dfs(s, 0, 1);
		ans=min(ans, tmp);
	}
	printf("%d\n", ans);
}

#include <queue>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
int n, m, q, a[1010][1010];
int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("force.out","w",stdout);
	scanf("%d%d%d", &n, &m, &q);
	for(int i=1, t=1; i<=n; i++)
		for(int j=1; j<=m; j++, t++)
			a[i][j]=t;
	while(q--)
	{
		int x, y;
		scanf("%d%d", &x, &y);
		printf("%d\n", a[x][y]);
		int tmp=a[x][y];
		rep(i, y+1, m) a[x][i-1]=a[x][i];
		rep(i, x+1, n) a[i-1][m]=a[i][m];
		a[n][m]=tmp;
	}
}

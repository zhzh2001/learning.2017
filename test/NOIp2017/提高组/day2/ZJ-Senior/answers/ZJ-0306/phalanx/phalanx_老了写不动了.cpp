#include <queue>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
const int N=300300;
struct node; node *null;
struct node{
	node *ls, *rs, *fa;
	int siz, val, mx;
	void upd(){
		mx=max(max(ls->mx, rs->mx), val);
		siz=ls->siz+rs->siz+1;
	}
	void zig(){
		node *y=fa, *z=y->fa;
		if(z!=null) if(y==z->ls) z->ls=this; else if(y==z->rs) z->rs=this; fa=z;
		y->ls=rs; if(rs!=null) rs->fa=y;
		rs=y; y->fa=this; y->upd();
	}
	void zag(){
		node *y=fa, *z=y->fa;
		if(z!=null) if(y==z->ls) z->ls=this; else if(y==z->rs) z->rs=this; fa=z;
		y->rs=ls; if(ls!=null) ls->fa=y;
		ls=y; y->fa=this; y->upd();
	}
}*q[N], t[3*N];
struct Splay{
	node *root;
	void splay(node *x, node *aim){
		while(x->fa!=aim)
		{
			node *y=x->fa, *z=y->fa;
			if(z==aim)
				if(x==y->ls) x->zig(); else x->zag();
			else
				if(x==y->ls) if(y==z->ls) y->zig(), x->zig(); else x->zig(), x->zag();
				else if(y==z->ls) x->zag(),  x->zig(); else y->zag(), x->zag();
		}
		if(aim==null) root=x;
		x->upd();
	}
	node *find(node *x, int k){
		for(;;)
			if(x->ls->siz+1==k) return x; else if(x->ls->siz>=k) x=x->ls; else k-=x->ls->siz+1, x=x->rs;
	}
	void init(){
		node *x=&t[++size];
		node *y=&t[++size];
		root=x;
		x->fa=x->ls=null; x->rs=y; x->siz=2;
		y->fa=x; y->ls=y->rs=null; y->siz=1;
	}
	node *build(int l, int r, int m){
		if(l>r) return null;
		node *x=&t[++size];
		x->fa=null;
		int mid=(l+r)>>1;
		x->ls=build(l, mid-1, m); if(x->ls!=null) x->ls->fa=x;
		x->rs=build(mid+1, r, m); if(x->rs!=null) x->rs->fa=x;
		x->siz=r-l+1;
		x->val=mid*m;
		x->upd();
	}
	void ins(int val){
		int s=root->siz;
		splay(find(root, s), null);
		splay(find(root, s-1), null);
		node *x=&t[++size];
		x->fa=root->ls; x->ls=x->rs=null;
		x->siz=1; x->mx=x->val=val;
		root->ls->rs=x;
		root->ls->upd();
		root->upd();
	}
	void del(int pos){
		
	}
}del[N], ins[N], f;
int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out","w",stdout);
	null=t; size=1;
	scanf("%d%d%d", &n, &m, &q);
	rep(i, 1, n)
	{
		del[i].init(), ins[i].init();
	}
	last.root=last.build(0, n+1, m);
	while(q--)
	{
		int x, y;
		scanf("%d%d", &x, &y);
		if(y>m-1-ct[x])
		{
			del[x].del(y-(m-1-ct[x]));
		}
		else
		{
			
			ct[x]++;
		}
		printf("%d\n", ans);
		f.splay(f.find(f.root, x), null);
		f.splay(f.find(f.root, x+2), f.root);
		node *xx=f.root->rs->ls;
		f.root->rs->ls=null; f.root->rs->upd(); f.root->upd();
		ins[x].ins(xx->val);
		
		f.splay(f.find(f.root, n+1), null);
		f.splay(f.find(f.root, n), f.root);
		f.root->ls->rs=yy; f.root->ls->upd(); f.root->upd();
		yy->fa=f.root->ls;
	}
}

#include <queue>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
#define fi first
#define se second
#define rep(x, a, b) for(int x=a; x<=b; x++)
#define drp(x, a, b) for(int x=a; x>=b; x--)
#define N 1010
int head[N], cnt, vis[N], h, r, n, x[N], y[N], z[N], T;
struct Edge{
	int to, next;
}edge[N*N];
queue<int> q;
void AddEdge(int u, int v){
	edge[++cnt]=(Edge){ v, head[u] }; head[u]=cnt;
	edge[++cnt]=(Edge){ u, head[v] }; head[v]=cnt;
}
long long sqr(int x){
	return 1ll*x*x;
}
bool pan(int a, int b){
	return sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b])<=sqr(r+r);
}
int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d%d%d", &n, &h, &r);
		rep(i, 1, n) scanf("%d%d%d", x+i, y+i, z+i);
		memset(head, 0, sizeof head); cnt=0;
		rep(i, 1, n) if(z[i]<=r) AddEdge(n+1, i);
		rep(i, 1, n) if(z[i]+r>=h) AddEdge(i, n+2);
		rep(i, 1, n) rep(j, i+1, n) if(pan(i, j)) AddEdge(i, j);
		memset(vis, 0, sizeof vis);
		q.push(n+1); vis[n+1]=1;
		while(!q.empty())
		{
			int u=q.front(); q.pop();
			for(int i=head[u]; i; i=edge[i].next)
			{
				int v=edge[i].to;
				if(!vis[v]) q.push(v), vis[v]=1;
			}
		}
		puts(vis[n+2]?"Yes":"No");
	}
}

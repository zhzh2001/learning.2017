/*
phalanx
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<queue>
using namespace std;

template<typename T>
void read(T& w)
{
	char r;int f=1;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')r=getchar(),f=-1;
	for(w=0;r>=48&&r<=57;r=getchar())w=w*10+r-48;
	w*=f;
}
template<typename T>
void write(T w)
{
	if(w<0)putchar('-'),w=-w;
	if(w>9)write(w/10);
	putchar(w%10+48);
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
void Swap(T& a,T& b){T t=a;a=b;b=t;}

typedef long long lint;
typedef unsigned int uint;
const int inf=2147483647;
const uint uinf=4294967295u;
const lint linf=9223372036854775807ll;

const int maxn=300003;
int n,m,q;
int x[maxn],y[maxn];
vector<int> qx[maxn];
int BS(int qu,int x)
{
	int l=0,r=qx[x].size()-1;
	int ans=0,mid;
	while(l<=r)
	{
		mid=(l+r)/2;
		if(qx[x][mid]<qu)
		{
			ans=qx[x][mid];
			l=mid+1;
		}
		else r=mid-1;
	}
	return ans;
}
bool all1=true;
void input()
{
	read(n),read(m),read(q);
	for(int i=1;i<=q;++i)
	{
		read(x[i]),read(y[i]);
		if(x[i]!=1)
			all1=false;
	}
}
int getid(int a,int b)
{
	return (a-1)*m+b;
}
int f(int qu,int a,int b)
{
	if(qu==0)return getid(a,b);
	if(a==n&&b==m)
		return f(qu-1,x[qu],y[qu]);
	if(a==x[qu]&&b>=y[qu]&&b<=m-1)
	{
		if(b==m-1)
			return f(qu-1,a,b+1);
		int quu=BS(qu,a);
		return f(quu,a,b+1);
	}
	if(a>=x[qu]&&a<=n-1&&b==m)
		return f(qu-1,a+1,b);
	if(b==m)return f(qu-1,a,b);
	else	
	{
		int quu=BS(qu,a);
		return f(quu,a,b);
		//return f(qu-1,a,b);
	}
}

int sum[maxn];
int a[maxn];
int lowbit(int x)
{
	return x&-x;
}
void add(int x)
{
	while(x<=m)
	{
		sum[x]++;
		x+=lowbit(x);
	}
}
int getsum(int xx)
{
	int summ=0;
	while(xx>0)
	{
		summ+=sum[xx];
		xx-=lowbit(xx);
	}
	return summ;
}
queue<int> qr;

int ary[maxn*2],ak;
void solve1()
{
	for(int i=1;i<=m;++i)
	{
		ary[i]=getid(1,i);
	}
	ak=m;
	for(int i=2;i<=n;++i)
	{
		qr.push(getid(i,m));
	}
	for(int i=1;i<=q;++i)
	{
		int j=y[i];
		for(int de=getsum(j);j-de!=y[i];de=getsum(j))
		{
			j+=de;
		}
		write(ary[j]);
		putchar('\n');
		add(j);
		qr.push(ary[j]);
		ary[++ak]=qr.front();qr.pop();
		ary[j]=0;
	}
}
void solve()
{
	for(int i=1;i<=q;++i)
	{
		qx[x[i]].push_back(i);
		write(f(i-1,x[i],y[i]));
		putchar('\n');
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	input();
	if(all1)
		solve1();
	else solve();
	return 0;
}



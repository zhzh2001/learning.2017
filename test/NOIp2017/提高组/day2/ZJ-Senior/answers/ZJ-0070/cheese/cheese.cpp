/*
cheese
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
using namespace std;

template<typename T>
void read(T& w)
{
	char r;int f=1;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')r=getchar(),f=-1;
	for(w=0;r>=48&&r<=57;r=getchar())w=w*10+r-48;
	w*=f;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
void Swap(T& a,T& b){T t=a;a=b;b=t;}

typedef long long lint;
typedef unsigned int uint;
const int inf=2147483647;
const uint uinf=4294967295u;
const lint linf=9223372036854775807ll;

const int maxn=1003;
const int maxh=1000000003;
int T,n;
lint h,r;

int down,up;
struct HOLE
{
	lint x,y,z;
	void getin()
	{
		//scanf("%lf%lf%lf",&x,&y,&z);
		read(x),read(y),read(z);
	}
}hole[maxn];
void input()
{
	read(n);
	read(h),read(r);
	for(int i=1;i<=n;++i)
	{
		hole[i].getin();
	}
	down=0,up=n+1;
}
int f[maxn];
void initF()
{
	for(int i=0;i<=n+1;++i)
	{
		f[i]=i;
	}
}
lint dis2(const HOLE& a,const HOLE& b)
{
	lint xx=a.x-b.x;
	lint yy=a.y-b.y;
	lint zz=a.z-b.z;
	return xx*xx+yy*yy+zz*zz;
}
int findf(int x)
{
	if(x!=f[x])
		f[x]=findf(f[x]);
	return f[x];
}
void unite(int a,int b)
{
	int fa=findf(a),fb=findf(b);
	f[fa]=fb;
}
bool ans;
void solve()
{
	initF();
	for(int i=1;i<=n;++i)
	{
		if(hole[i].z>=-r&&hole[i].z<=r)
		{
			unite(down,i);
		}
		if(hole[i].z-h>=-r&&hole[i].z-h<=r)
		{
			unite(up,i);
		}
	}
	for(int i=1;i<=n;++i)
	{
		for(int j=i+1;j<=n;++j)
		{
			lint dis=dis2(hole[i],hole[j]);
			if(dis<=r*r*4ll)
			{
				unite(i,j);
			}
		}
	}
	ans=bool(findf(down)==findf(up));
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(read(T);T;--T)
	{
		input();
		solve();
		if(ans)puts("Yes");
		else puts("No");
	}
	return 0;
}



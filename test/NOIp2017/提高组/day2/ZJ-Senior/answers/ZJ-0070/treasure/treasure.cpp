/*
treasure
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
using namespace std;

template<typename T>
void read(T& w)
{
	char r;int f=1;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')r=getchar(),f=-1;
	for(w=0;r>=48&&r<=57;r=getchar())w=w*10+r-48;
	w*=f;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
void Swap(T& a,T& b){T t=a;a=b;b=t;}

typedef long long lint;
typedef unsigned int uint;
const int inf=2147483647;
const uint uinf=4294967295u;
const lint linf=9223372036854775807ll;

const int maxn=13;
const int maxm=1003;
int n,m;
struct EDGE
{
	int to,nxt,w;
	void init(int too,int nxtt,int ww)
	{
		to=too,nxt=nxtt;
		w=ww;
	}
}edge[maxm<<1];
int ek=0;
int head[maxn];
void addEdge(int k,int v,int ww)
{
	edge[++ek].init(v,head[k],ww);
	head[k]=ek;
}
int getre(int ke)
{
	if(ke&1)return ke+1;
	return ke-1;
}
void input()
{
	read(n),read(m);
	int x,y,w;
	for(int i=1;i<=m;++i)
	{
		read(x),read(y),read(w);
		if(x!=y)
		{
			addEdge(x,y,w);
			addEdge(y,x,w);
		}
	}
}

int d[maxn];
bool used[maxn];
int ans=inf;
void dfs(int pos,int sumcost)
{
	if(sumcost>ans)return;
	if(pos==n+1)
	{
		ans=sumcost;
	}
	for(int k=1;k<=n;++k)
	{
		if(!used[k])
		{
			used[k]=true;
			if(pos==1)
			{
				d[k]=1;
				dfs(pos+1,sumcost);
				d[k]=inf;
			}
			else	
			{
				for(int i=head[k];i;i=edge[i].nxt)
				{
					int v=edge[i].to;
					int w=edge[i].w;
					if(d[v]!=inf)
					{
						int cost=w*d[v];
						d[k]=d[v]+1;
						dfs(pos+1,sumcost+cost);
						d[k]=inf;
					}
				}
			}
			used[k]=false;
		}
	}
}
void dfs2(int pos,int sumcost)
{
	if(sumcost>ans)return;
	if(pos==n+1)
	{
		ans=sumcost;
	}
	for(int k=1;k<=n;++k)
	{
		if(!used[k])
		{
			used[k]=true;
			if(pos==1)
			{
				d[k]=1;
				dfs(pos+1,sumcost);
				d[k]=inf;
			}
			else	
			{
				int cost=inf,td;
				for(int i=head[k];i;i=edge[i].nxt)
				{
					int v=edge[i].to;
					int w=edge[i].w;
					if(d[v]!=inf)
					{
						int tcost=w*d[v];
						if(tcost<cost)
							cost=tcost,td=d[v]+1;
					}
				}
				d[k]=td;
				dfs(pos+1,sumcost+cost);
				d[k]=inf;
			}
			used[k]=false;
		}
	}
}
void solve()
{
	for(int i=1;i<=n;++i)		
	{
		d[i]=inf;
	}
	if(n<=8)
		dfs(1,0);
	else 
		dfs2(1,0);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	input();
	solve();
	printf("%d\n",ans);
	return 0;
}



#include <iostream>
#include <cstring>
#include <cstdio>
#define lx x+x
#define rx x+x+1
typedef long long ll;
using namespace std;
const int N=300005;
struct node
{
	int x,y;
}Q[N];
int n,m,q;
namespace test1
{
	int a[505][505],x,y,z;
	void solve()
	{
		for (int i=1;i<=n;++i)
			for (int j=1;j<=m;++j)
				a[i][j]=(i-1)*m+j;	
		for (int i=1;i<=q;++i)
		{
			scanf("%d%d",&x,&y);
			z=a[x][y];
			printf("%d\n",z);
			for (int j=y;j<m;++j)
				a[x][j]=a[x][j+1];
			for (int i=x;i<n;++i)
				a[i][m]=a[i+1][m];
			a[n][m]=z;				
		}
	}
}
namespace test3
{	
	int size[N*3*4],cnt=0,x,y,z;
	ll a[N*3];
	void insert(int t,ll v,int l=1,int r=n+m+q,int x=1)
	{
		if (l==r)
		{
			size[x]=1;
			a[t]=v;
			return;
		}
		int o=(l+r)>>1;
		if (t<=o)	insert(t,v,l,o,lx);
		if (t>o)	insert(t,v,o+1,r,rx);
		size[x]=size[lx]+size[rx];
	}
	void clr(int t,int l=1,int r=n+m+q,int x=1)
	{
		if (l==r)
		{
			size[x]=0;
			a[t]=0;
			return;
		}
		int o=(l+r)>>1;
		if (t<=o)	clr(t,l,o,lx);
		if (t>o)	clr(t,o+1,r,rx);
		size[x]=size[lx]+size[rx];		
	}
	int find(int s,int l=1,int r=n+m+q,int x=1)
	{
		if (l==r)	return l;
		int o=(l+r)>>1;
		if (s<=size[lx])	return find(s,l,o,lx);
		if (s>size[rx])	return find(s-size[lx],o+1,r,rx);		
	}
	void solve()
	{
		memset(size,0,sizeof(size));
		memset(a,0,sizeof(a));
		for (int i=1;i<m;++i)
			insert(++cnt,i);
		for (int i=1;i<=n;++i)
			insert(++cnt,(ll)i*m);
		for (int i=1;i<=q;++i)
		{
			x=Q[i].x;y=Q[i].y;
			z=find(y);
			printf("%lld\n",a[z]);		
			insert(++cnt,a[z]);
			clr(z);
		}		
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000 && m<=1000 && q<=500)	test1::solve();
	else
	{
		bool flag=0;
		for (int i=1;i<=q;++i)
		{
			scanf("%d%d",&Q[i].x,&Q[i].y);
			if (Q[i].x>1)	flag=1;
		}
		if (!flag)	test3::solve();
	}
	return 0;
}

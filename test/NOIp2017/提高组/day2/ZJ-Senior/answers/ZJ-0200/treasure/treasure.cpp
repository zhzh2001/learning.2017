#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdio>
using namespace std;
const int N=14;
const int S=1<<12;
int n,m,inf,T,x,y,z;
int edge[N][N],si[S+5],a[S+5][S+5],b[S+5][S+5],dp[S+5][N];
void dfs(int t,int o,int sta,int s)
{
	if (t>n)
	{
		if (o)
		{
			a[sta][++si[sta]]=o;
			b[sta][si[sta]]=s;
		}
		return;
	}
	dfs(t+1,o,sta,s);
	if (!((sta>>(t-1))&1))
	{
		int temp=inf;
		for (int i=1;i<=n;++i)		
			if ((sta>>(i-1))&1)
				temp=min(temp,edge[t][i]);		
		if (temp==inf)	return;
		dfs(t+1,o+(1<<(t-1)),sta,s+temp);
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(edge,0x3f,sizeof(edge));
	inf=edge[0][0];
	for (int i=1;i<=m;++i)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (z<edge[x][y])
			edge[x][y]=edge[y][x]=z;
	}
	T=1<<n;
	for (int i=1;i<T;++i)
		dfs(1,0,i,0);
	memset(dp,0x3f,sizeof(dp));
	for (int i=1;i<=n;++i)
		dp[1<<(i-1)][0]=0;
	for (int j=0;j<n-1;++j)
		for (int i=1;i<T;++i)
			for (int p=1;p<=si[i];++p)
				dp[i+a[i][p]][j+1]=min(dp[i+a[i][p]][j+1],dp[i][j]+b[i][p]*(j+1));
	int ans=inf;
	for (int j=0;j<n;++j)
		ans=min(ans,dp[T-1][j]);
	cout<<ans<<endl;
	return 0;
}

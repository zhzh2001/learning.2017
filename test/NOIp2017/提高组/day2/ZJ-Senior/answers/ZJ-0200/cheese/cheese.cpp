#include <iostream>
#include <cstdio>
#include <cstring>
typedef long long ll;
using namespace std;
const int N=1005;
int T,n,h,r;
ll dis;
int x[N],y[N],z[N];
bool vis[N],a[N][N];
void dfs(int t)
{
	vis[t]=1;
	for (int i=1;i<=n+2;++i)
		if (a[t][i] && !vis[i])
			dfs(i);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d",&n,&h,&r);
		memset(vis,0,sizeof(vis));
		memset(a,0,sizeof(a));
		for (int i=2;i<=n+1;++i)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if (z[i]<=r)	a[1][i]=a[i][1]=1;
			if (z[i]+r>=h)	a[i][n+2]=a[n+2][i]=1;			
		}
		for (int i=2;i<n+1;++i)
			for (int j=i+1;j<=n+1;++j)
			{
				dis=(ll)(x[i]-x[j])*(x[i]-x[j])+
					(ll)(y[i]-y[j])*(y[i]-y[j])+
					(ll)(z[i]-z[j])*(z[i]-z[j]);
				if (dis>=0 && dis<=4LL*r*r)
					a[i][j]=a[j][i]=1;
			}
		dfs(1);
		if (vis[n+2])	puts("Yes");
		else puts("No");
	}
	return 0;
}

Const
  null=99999999;
Var
  n,m,i,j,t,u,v,w,ans:longint;
  map:array[0..20,0..20] of longint;
  vis:array[0..20] of boolean;
  dep:array[0..20] of longint;
procedure dfs(x:longint);
Var
  u,v:longint;
begin
  if x>=n then
    begin
      if t<ans then
        ans:=t;
      exit;
    end;
  for u:=1 to n do
    if vis[u] then
      for v:=1 to n do
        if (map[u,v]<>null) and (not vis[v]) then
          begin
            t:=t+map[u,v]*dep[u];
            dep[v]:=dep[u]+1;
            vis[v]:=true;
            dfs(x+1);
            t:=t-map[u,v]*dep[u];
            dep[v]:=0;
            vis[v]:=false;
          end;
end;
begin
  assign(input,'treasure.in');
  reset(input);
  assign(output,'treasure.out');
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      map[i,j]:=null;
  for i:=1 to m do
    begin
      readln(u,v,w);
      if w<map[u,v] then
        begin
          map[u,v]:=w;
          map[v,u]:=w;
        end;
  end;
  ans:=null;
  for i:=1 to n do
    begin
      fillchar(vis,sizeof(vis),0);
      fillchar(dep,sizeof(dep),0);
      t:=0;
      dep[i]:=1;
      vis[i]:=true;
      dfs(1);
    end;
  writeln(ans);
  close(input);
  close(output);
end.


Var
  n,m,q,i,j,k,t,x,y,skn:longint;
  map:array[0..1200,0..1200] of longint;
  line:array[0..620000] of longint;
begin
  assign(input,'phalanx.in');
  reset(input);
  assign(output,'phalanx.out');
  rewrite(output);
  readln(n,m,q);
  if n=1 then
    begin
      for i:=1 to m do
        line[i]:=i;
      for k:=1 to q do
        begin
          readln(x,y);
          skn:=0;
          for i:=y downto 1 do
            if line[i]=-1 then
              begin
                inc(skn);
              end
              else
                break;
          for i:=y to m+k do
            begin
              if line[i]<>-1 then
                begin
                  dec(skn);
                  if skn<=0 then
                    begin
                      t:=line[i];
                      writeln(t);
                      break;
                    end;
                end;
            end;
          line[i]:=-1;
          line[m+k]:=t;
        end;
    end
    else
      begin
        for i:=1 to n do
          for j:=1 to m do
            map[i,j]:=(i-1)*m+j;
        for k:=1 to q do
          begin
            readln(x,y);
            t:=map[x,y];
            writeln(t);
            for j:=y to m-1 do
              begin
                map[x,j]:=map[x,j+1];
              end;
            for i:=x to n-1 do
              begin
                map[i,m]:=map[i+1,m];
              end;
            map[n,m]:=t;
          end;
      end;
  close(input);
  close(output);
end.
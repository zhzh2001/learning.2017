Var
  ck,k,n,i,j:longint;
  h,r,maxz:int64;
  x,y,z:array[0..1200] of int64;
  stp,vis:array[0..1200] of boolean;
  num:longint;
  head:array[0..1200] of longint;
  next,tar:array[0..2400000] of longint;
procedure addedge(u,v:longint);
begin
  inc(num);
  next[num]:=head[u];
  head[u]:=num;
  tar[num]:=v;
end;
procedure dfs(u:longint);
Var
  v,e:longint;
begin
  if z[u]>maxz then
    maxz:=z[u];
  vis[u]:=true;
  e:=head[u];
  while e<>0 do
    begin
      v:=tar[e];
      if not vis[v] then
        dfs(v);
      e:=next[e];
    end;
end;
begin
  assign(input,'cheese.in');
  reset(input);
  assign(output,'cheese.out');
  rewrite(output);
  readln(ck);
  for k:=1 to ck do
    begin
      readln(n,h,r);
      fillchar(stp,sizeof(stp),0);
      for i:=1 to n do
        begin
          readln(x[i],y[i],z[i]);
          if z[i]<=r then
            stp[i]:=true;
        end;
      num:=0;
      fillchar(head,sizeof(head),0);
      fillchar(next,sizeof(next),0);
      for i:=1 to n-1 do
        for j:=i+1 to n do
          begin
            if sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]))<=double(2*r) then
              begin
                addedge(i,j);
                addedge(j,i);
              end;
          end;
      maxz:=0;
      fillchar(vis,sizeof(vis),0);
      for i:=1 to n do
        if stp[i] and (not vis[i]) then
          dfs(i);
      if maxz+r>=h then
        writeln('Yes')
        else
          writeln('No');
    end;
  close(input);
  close(output);
end.

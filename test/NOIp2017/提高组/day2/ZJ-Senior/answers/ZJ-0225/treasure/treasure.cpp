#include<cstdio>
typedef long long ll;
const int N=15,INF=~0U>>1,M=8195;
int n,m,x,y,z,f,d[N][N];
bool vis[M][M];
ll ans[N][M],s[N][M],ret=INF;
ll min(ll a,ll b){return a<b?a:b;}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++) for(int j=1;j<=n;j++) d[i][j]=INF;
	f=(1<<n)-1;
	for(int j=1;j<=n;j++) for(int i=1;i<=f;i++) ans[j][i]=INF;
	for(int i=0;i<n;i++) ans[i+1][1<<i]=0;
	while(m--){
		scanf("%d%d%d",&x,&y,&z);
		if(d[x][y]>z) d[x][y]=d[y][x]=z;
	}
	for(int i=1;i<=f;i++) for(int j=1;j<i;j++) if((i|j)<=i && !vis[i][j] && !vis[i][i-j]){
		int t=i-j;
		vis[i][t]=vis[i][j]=1;
		for(int k1=1;k1<=n;k1++) if((1<<k1-1)&j) for(int k2=1;k2<=n;k2++) if((1<<k2-1)&t && d[k1][k2]!=INF){
			ll t2=s[k2][t]+ans[k2][t]+ans[k1][j]+d[k1][k2],t3=s[k1][j]+ans[k2][t]+ans[k1][j]+d[k1][k2];
			if(t2<ans[k1][i]) ans[k1][i]=t2,s[k1][i]=s[k1][j]+s[k2][t]+d[k1][k2];
			if(t3<ans[k2][i]) ans[k2][i]=t3,s[k2][i]=s[k2][t]+s[k1][j]+d[k1][k2];
		}
	}
	for(int i=1;i<=n;i++) ret=min(ret,ans[i][f]);
	printf("%lld",ret);
	fclose(stdin);fclose(stdout);
	return 0;
}

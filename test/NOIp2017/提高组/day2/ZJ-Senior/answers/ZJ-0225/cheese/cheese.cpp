#include<cstdio>
#include<cmath>
const int N=1005,M=N*N;
const double EPS=1e-8;
int t,n,h,r,cnt,ed,L,R,ans[N],v[M],nxt[M],g[N],q[N];
bool f,d[N];
struct Point{int x,y,z;}e[N];
void add(int x,int y){
	v[++ed]=y;
	nxt[ed]=g[x];
	g[x]=ed;
}
double dist(Point a,Point b){return sqrt(1.0*(a.x-b.x)*(a.x-b.x)+1.0*(a.y-b.y)*(a.y-b.y)+1.0*(a.z-b.z)*(a.z-b.z));}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d",&n,&h,&r);
		L=R=ed=cnt=f=0;
		for(int i=0;i<n;i++){
			g[i]=d[i]=0;
			scanf("%d%d%d",&e[i].x,&e[i].y,&e[i].z);
			if(e[i].z<=r) d[q[R++]=i]=1;
			else if(e[i].z+r>=h) ans[cnt++]=i;
		}
		for(int i=0;i<n;i++) for(int j=i;j<n;j++) if(2.0*r-dist(e[i],e[j])>-EPS) add(i,j),add(j,i);
		while(L<R){
			int x=q[L++];
			for(int i=g[x];i;i=nxt[i]) if(!d[v[i]]) d[q[R++]=v[i]]=1;
		}
		for(int i=0;i<cnt;i++) if(d[ans[i]]) f=1;
		printf("%s",f?"Yes\n":"No\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

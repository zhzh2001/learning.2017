//AFO
//Lucius 2017.11.12 at Quzhou Shuyuan Middle School
//But I have promises to keep. 
#include<cstdio>
const int N=1005,M=300005;
int n,m,q,x,y,t,cnt,a[N][N],ans[M],c[M];
void read(int &x){
	char c;
	while((c=getchar())<'0' || c>'9');
	x=c-'0';
	while((c=getchar())>='0' && c<='9') x=x*10+c-'0';
}
int lowbit(int x){return x&-x;}
void add(int x){while(x<=m) c[x]--,x+=lowbit(x);}
int sum(int x){
	int ret=0;
	while(x>0) ret+=c[x],x-=lowbit(x);
	return ret;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n),read(m),read(q);
	if(n<=1000 && m<=1000){
		for(int i=1;i<=n;i++) for(int j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
		while(q--){
			read(x),read(y);
			printf("%d\n",t=a[x][y]);
			for(int i=y;i<m;i++) a[x][i]=a[x][i+1];
			for(int i=x;i<n;i++) a[i][m]=a[i+1][m];
			a[n][m]=t;
		}
	}else if(n==1){
		for(int i=1;i<=q;i++){
			read(x),read(y);
			if(y>m-cnt){
				printf("%d\n",t=ans[y-m+cnt]);
				for(int j=y-m+cnt;j<cnt;j++) ans[j]=ans[j+1];
				ans[cnt]=t;
			}
			int t=sum(x);
			printf("%d\n",y+t);
			ans[++cnt]=y+t;
			add(x);
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

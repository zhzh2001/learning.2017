#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 1e3 + 5;
const int M = 1e6 + 5;
int n,h,r;
struct pts{
	int x,y,z;
	void rd(){
		scanf("%d%d%d",&x,&y,&z);
	}
}a[N];
int s,t;
int head[N],nxt[M<<1],to[M<<1],cnt;
void add(int u,int v){
	nxt[++cnt]=head[u];head[u]=cnt;to[cnt]=v;
	nxt[++cnt]=head[v];head[v]=cnt;to[cnt]=u;
}
long long sqr(int x){
	return 1ll*x*x;
}
bool ok(pts a,pts b){
	long long dis = sqr(a.x-b.x) + sqr(a.y-b.y) + sqr(a.z-b.z);
	return dis <= sqr(2*r);
}
bool vis[N];
void dfs(int u){
	vis[u] = 1;
	for(int i=head[u];i;i=nxt[i]){
		int v = to[i];
		if(vis[v]) continue;
		dfs(v);
	}
}
void work(){
	memset(head,0,sizeof(head));cnt = 0;
	scanf("%d%d%d",&n,&h,&r);
	For(i,1,n) a[i].rd();
	s = n+1; t = n+2;
	For(i,1,n){
		Rep(j,1,i)
			if(ok(a[i],a[j])) add(i,j);
		if(a[i].z - r <= 0) add(s,i);
		if(a[i].z + r >= h) add(i,t);
	}
	memset(vis,0,sizeof(vis));
	dfs(s);
	puts(vis[t]?"Yes":"No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int Test; cin >> Test;
	while(Test --) work();
	return 0;
}

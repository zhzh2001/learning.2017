#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 3e5 + 5;
int n,m,q;
int rt[N];
vector<int>V;
struct sgt{
	int ls[N<<3],rs[N<<3],w[N<<3],_;
	void ins(int&p,int l,int r,int x){
		if(!p) p = ++_;
		w[p] ++;
		if(l==r) return;
		int m = l+r>>1;
		if(x<=m) ins(ls[p],l,m,x);
		else	 ins(rs[p],m+1,r,x);
	}
	int query(int p,int l,int r,int x,int y){
		if(!p) return 0;
		if(x<=l&&r<=y) return w[p];
		int m=l+r>>1;int res = 0;
		if(x<=m) res += query(ls[p],l,m,x,y);
		if(y> m) res += query(rs[p],m+1,r,x,y);
		return res;
	}
}T;
struct SGT{
	int w[N<<1];
	void ins(int p,int l,int r,int x){
		w[p] ++;
		if(l==r) return;
		int m=l+r>>1;
		if(x<=m) ins(p<<1,l,m,x);
		else	 ins(p<<1|1,m+1,r,x);
	}
	int query(int p,int l,int r,int x,int y){
		if(x<=l&&r<=y) return w[p];
		int m = l+r>>1;int res = 0;
		if(x<=m) res += query(p<<1,l,m,x,y);
		if(y> m) res += query(p<<1|1,m+1,r,x,y);
	}
}xT;
int id(int i,int j){
	return (i-1)*m + j;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	while(q--){
		int x,y;scanf("%d%d",&x,&y);
		int aa = T.query(rt[x],1,m,1,m);
		int pos;
		if(y!=m&&aa<=m-y){
			int t;
			int L = y; int R = m;
			while(L<R){
				int M = L+R>>1;
				if(T.query(rt[x],1,m,1,M) <= M-y) R = M;
				else							  L = M+1;
			}
			pos = id(x,L);
		}else{
			int bb = aa;
			if(x>1) aa += xT.query(1,1,n,1,x-1);
			if(aa<=m-y+n-x-1){
				int L = x+1,R = x = n;
				while(L<R){
					int M = L+R>>1;
					if(xT.query(1,1,n,x+1,M) + bb <= M-x+m-y) R=M;
					else									  L=M+1;
				}
				pos = id(L,m);
			}else{
				pos = V[aa-(x+m-y-1)];
			}
		}
		printf("%d\n",pos);
		V.push_back(pos);
		xT.ins(1,1,n,x);
		T.ins(rt[x],1,m,y);
	}
	return 0;
}

#include<cstdio>
#include<vector>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 1005;
int n,m,q;
int a[N][N],_;
struct SGT{
	int w[6000000],po[6000000];
	void ins(int p,int l,int r,int x,int o){
		w[p] ++;
		if(l==r){
			po[p] = o;
			return;
		}
		int m=l+r>>1;
		if(x<=m) ins(p<<1,l,m,x,o);
		else	 ins(p<<1|1,m+1,r,x,o);
	}
	void del(int p,int l,int r,int x){
		w[p] --;
		if(l==r) {
			po[p] = 0;
			return;
		}
		int m=l+r>>1;
		if(x<=m) del(p<<1,l,m,x);
		else	 del(p<<1|1,m+1,r,x);		
	}
	int query(int p,int l,int r,int x){
		if(l==r) return po[p];
		int m = l+r>>1;
		if(w[p<<1] >= x) return query(p<<1,l,m,x);
		else			 return query(p<<1|1,m+1,r,x-w[p<<1]);
	}
}T;
vector<int>V;
void Swork(){
	int _ = m;
	int mx = m+q;
	For(i,1,m) T.ins(1,1,mx,i,i);
	while(q--){
		int x,y;scanf("%d%d",&x,&y);
		int c = T.query(1,1,mx,y);
		printf("%d\n",c);
		T.del(1,1,mx,y);
		++_;
		T.ins(1,1,mx,_,c);
	}
	exit(0);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n==1) Swork();
	For(i,1,n) For(j,1,m) a[i][j] = ++_;
	while(q--){
		int x,y;scanf("%d%d",&x,&y);
		printf("%d\n",a[x][y]);
		Rep(i,y,m) swap(a[x][i],a[x][i+1]);
		Rep(i,x,n) swap(a[i][m],a[i+1][m]);
	}
	return 0;
}

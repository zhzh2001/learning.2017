#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 12;
const int inf = 1e9;
struct data{
	int dept[N];
}g[N][N][1<<N];
int n,m;
int f[N][N][1<<N];
int num[1<<N];
int a[N][N];
int w[N][N];
data ww[N][N];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(f,0x3f,sizeof(f));
	memset(a,0x3f,sizeof(a));
	scanf("%d%d",&n,&m);
	
	For(i,1,m){
		int u,v,c;
		scanf("%d%d%d",&u,&v,&c);
		a[u-1][v-1] = min(a[u-1][v-1],c);
		a[v-1][u-1] = min(a[v-1][u-1],c);
	}
	
	Rep(i,0,(1<<N))
		num[i] = num[i>>1] + (i&1);
	Rep(i,0,n)
		f[i][1][1<<i] = 0,g[i][1][1<<i].dept[i] = 1;
	For(q,1,n){
		Rep(k,0,(1<<n)){
			if(num[k] != q) continue;
			memset(w,0x3f,sizeof(w));
			Rep(i,0,n){
				Rep(j,1,n){
					Rep(t,0,n){
						if(f[i][j][k] < w[t][g[i][j][k].dept[t]]){
							w[t][g[i][j][k].dept[t]] = f[i][j][k];
							ww[t][g[i][j][k].dept[t]] = g[i][j][k];
						}
					}
				}
			}
			Rep(i,0,n) Rep(j,1,n){
				if(f[i][j][k] > w[i][j]){
					f[i][j][k] = w[i][j];
					g[i][j][k] = ww[i][j];
				}
			}
		}
		Rep(k,0,(1<<n)){
			if(num[k] != q) continue;
			Rep(i,0,n){
				Rep(j,1,n){
					if(f[i][j][k] > inf) continue;
					Rep(t,0,n){
						if(a[i][t] > inf) continue;
						if(f[i][j][k] + j*a[i][t] < f[t][j+1][k|(1<<t)]){
							f[t][j+1][k|(1<<t)] = f[i][j][k] + j*a[i][t];
							g[t][j+1][k|(1<<t)] = g[i][j][k];
							g[t][j+1][k|(1<<t)].dept[t] = j+1;
						}
					}
				}
			}
		}
	}
	int res = inf;
	Rep(i,0,n) Rep(j,0,n){
		res = min (res, f[i][j][(1<<n)-1]);
	}
	printf("%d\n",res);
	return 0;
}

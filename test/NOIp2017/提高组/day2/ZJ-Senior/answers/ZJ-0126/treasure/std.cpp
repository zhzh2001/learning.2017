#include<bits/stdc++.h>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 25;
int a[N][N];
int n,m;
int f[16777216];
int num[16777216];
vector<int> turn(int x){
	vector<int>V;
	V.clear();
	for(int i = 1;i<=n;x/=n,++i) V.push_back(x%n);
	return V;
}
int pw(int x,int y){
	int p = 1;for(;y;y>>=1,x=x*x) if(y&1) p = p*x;
	return p;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("std.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x3f,sizeof(a));
	For(i,1,m){
		int u,v,c;
		scanf("%d%d%d",&u,&v,&c);
		--u;--v;
		a[u][v] = min (a[u][v],c);
		a[v][u] = min (a[v][u],c);
	}
	int TOP = pw(n,n);
	Rep(i,0,TOP) num[i] = num[i/n] + (i%n != 0);
	memset(f,0x3f,sizeof(f));
	Rep(i,0,n)
		f[pw(n,i)] = 0;
	Rep(q,1,n){
		Rep(i,0,TOP){
			if(num[i]!=q) continue;
			vector<int>V = turn(i);
			Rep(s,0,n) if(V[s]){
				Rep(t,0,n) if(!V[t]&&a[s][t] < 1e9){
					int c = i + pw(n,t) * (V[s]+1);
					f[c] = min(f[c],f[i] + a[s][t] * V[s]);
				}
			}
		}
	}
	int res = 1e9;
	Rep(i,0,TOP) if(num[i] == n) res = min(res,f[i]);
	printf("%d\n",res);
	return 0;	
}

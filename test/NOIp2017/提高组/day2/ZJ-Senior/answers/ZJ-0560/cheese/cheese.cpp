#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
#include <utility>
#define INF 0x3f3f3f3f
#define ll long long
#define ull unsigned long long
#define db double
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define rpd(i,a,b) for(int i=(a);i>=(b);--i)
#define rpg1(i,s) for(int i=head[s];~i;i=edge[i].nxt)
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
#define sqr(x) (x)*(x)
const int MAXN=1010; const ull ad=1e9;
struct P { ull x,y,z; } A[MAXN];
inline ull dis(const P&a,const P&b) {
	ull res=0; 
	if(a.x>b.x) res+=sqr(a.x-b.x); else res+=sqr(b.x-a.x);
	if(a.y>b.y) res+=sqr(a.y-b.y); else res+=sqr(b.y-a.y);
	if(a.z>b.z) res+=sqr(a.z-b.z); else res+=sqr(b.z-a.z);
	return res;
}
int fa[MAXN];
inline int find(const int&u) { return u==fa[u]?u:fa[u]=find(fa[u]); }
void work() {
	int N=read(),H=read(); ull r=read(); ull k=sqr(r<<1);
	rep(i,0,N+1) fa[i]=i;
	rep(i,1,N) A[i].x=(ull)(read()+ad),A[i].y=(ull)(read()+ad),A[i].z=(ull)(read()+ad);
	int u,v;
	rep(i,1,N) {
		if(A[i].z<=r+ad) { u=find(i),v=find(0); fa[u]=v; }
		if(A[i].z+r>=H+ad) { u=find(i),v=find(N+1); fa[u]=v; }
		rep(j,i+1,N) if(dis(A[i],A[j])<=k) { u=find(i),v=find(j); fa[u]=v; }
	}
	if(find(0)==find(N+1)) puts("Yes"); else puts("No");
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--) work();

	return 0;
}



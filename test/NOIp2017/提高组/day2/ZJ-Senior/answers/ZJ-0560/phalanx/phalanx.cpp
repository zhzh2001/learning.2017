#include <cstring>
#include <cstdio>
#include <algorithm>
#include <queue>
#include <vector>
#include <utility>
#define INF 0x3f3f3f3f
#define ll long long
#define db double
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define rpd(i,a,b) for(int i=(a);i>=(b);--i)
#define rpg1(i,s) for(int i=head[s];~i;i=edge[i].nxt)
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
int N,M,Q;
int mp[1010][1010];
void solve1() {
	rep(i,1,N) rep(j,1,M) mp[i][j]=(i-1)*M+j;
	while(Q--) {
		int x=read(),y=read(); int tmp=mp[x][y];
		printf("%d\n",tmp);
		rep(i,x+1,N) mp[i-1][y]=mp[i][y]; rep(j,y+1,M) mp[x][j-1]=mp[x][j];
		mp[x][y]=tmp;
	}
}
int C[300010];
inline void updata(int i,const int&p) {
	for(;i<=M;i+=i&-i) C[i]+=p;
}
inline int query(int i) {
	int res=0;
	for(;i;i-=i&-i) res+=C[i];
	return res;
}		
void solve2() {
	memset(C,0,sizeof C); rep(i,1,M) updata(i,1);
	while(Q--) {
		int x=read(),y=read(); int tmp1=query(y),tmp2=query(M);
		printf("%d\n",tmp1);
		if(y==M) continue;
		updata(y,1); updata(M,tmp1-tmp2-1);
	}
}		
	
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	N=read(),M=read(),Q=read();
	if(N<=1000&&M<=1000) solve1();
	else if(N==1) solve2();
	return 0;
}



#include <cstring>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <queue>
#include <vector>
#include <utility>
#define INF 0x3f3f3f3f
#define ll long long
#define db double
#define rep(i,a,b) for(int i=(a);i<=(b);++i)
#define rpd(i,a,b) for(int i=(a);i>=(b);--i)
#define rpg1(i,s) for(int i=head[s];~i;i=edge[i].nxt)
using namespace std;
inline int read() {
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
const int MAXN=13;
int N,M,G[MAXN][MAXN];
int vis[MAXN],tvis[MAXN];
void solve1() {
	int ans=INF;
	rep(rt,1,N) {
		memset(vis,0,sizeof vis);
		vis[rt]=1; int cnt=1,t=1,tot=0;
		while(cnt<N) {
			memset(tvis,0,sizeof tvis);
			rep(i,1,N) if(vis[i]&&!tvis[i]) rep(j,1,N) if(!vis[j]&&G[i][j]!=INF)
				vis[j]=tvis[j]=1,tot+=t*G[i][j],++cnt;
			++t;
		}
		ans=min(ans,tot);
	}
	printf("%d\n",ans);
}
int dp[MAXN][1<<12];
inline int count(const int&S) {
	int cnt=0;
	rep(i,0,N-1) if(S&(1<<i)) cnt++; 
	return cnt;
}
int dfs(const int&dep,const int&S) {
	if(dp[dep][S]!=INF) return dp[dep][S];
	if(dep==0&&(1<<(int)log2(S))==S) return dp[dep][S]=0;
	else if(dep==0) return INF;
	int res=INF;
	rep(i,0,N-1) if(S&(1<<i)) rep(j,0,N-1) if(i!=j&&(S&(1<<j))&&G[i+1][j+1]!=INF) {
		res=min(res,dfs(dep,S^(1<<i))+G[i+1][j+1]*dep);
		res=min(res,dfs(dep-1,S^(1<<i))+G[i+1][j+1]*dep);
	}
	return res;
}
	
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout); 
	N=read(),M=read();
	memset(G,0x3f,sizeof G); 
	rep(i,1,M) {
		int u=read(),v=read(),w=read();
		G[v][u]=G[u][v]=min(G[u][v],w);
	}
	int tmp=-1,fg=1; rep(i,1,N) rep(j,1,N) if(G[i][j]!=INF) if(tmp==-1) tmp=G[i][j]; else fg&=(tmp==G[i][j]);
	if(fg) solve1();
	else {
		if(N==1) { puts("0"); return 0; }
		memset(dp,0x3f,sizeof dp);
		int ans=INF;
		rep(i,1,N-1) ans=min(ans,dfs(i,(1<<N)-1));
		printf("%d\n",ans+1);
	}

	return 0;
}



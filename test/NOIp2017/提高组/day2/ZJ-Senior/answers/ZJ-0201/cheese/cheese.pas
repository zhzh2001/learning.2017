var m,i,n:longint;
    s:string;
    t:array[0..1001] of longint;
    a:array[0..1001,1..1001] of longint;
procedure ru;
begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);
end;
procedure chu;
begin
  close(input);
  close(output);
end;
procedure bfs;
var h,r,i:longint;
    b:array[1..1001] of boolean;
    c:array[0..1001] of longint;
begin
  r:=1;
  c[r]:=0;
  h:=0;
  for i:=1 to n do b[i]:=false;
  while h<=r do
  begin
    inc(h);
    if c[h]=n+1 then
    begin
      s:='Yes';
      break;
    end;
    for i:=1 to t[c[h]] do
    begin
      if b[a[c[h],i]]=false then
      begin
        b[a[c[h],i]]:=true;
        inc(r);
        c[r]:=a[c[h],i];
      end;
    end;
  end;
  if s='' then s:='No';
  for i:=1 to r do c[i]:=0;
end;
procedure main;
var h,r,i,j:longint;
    x,y,z:array[0..1001] of longint;
begin
  read(n,h,r);
  s:='';
  for i:=1 to n do
  begin
    read(x[i],y[i],z[i]);
  end;
  for i:=0 to n+1 do t[i]:=0;
  for i:=1 to n do
  if r>=z[i] then
  begin
    inc(t[0]);
    a[0,t[0]]:=i;
  end;
  for i:=1 to n-1 do
  for j:=i+1 to n do
  begin
    if sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))<=2*r then
    begin
      inc(t[i]);
      inc(t[j]);
      a[i,t[i]]:=j;
      a[j,t[j]]:=i;
    end;
  end;
  for i:=1 to n do
  if r>=h-z[i] then
  begin
    inc(t[i]);
    a[i,t[i]]:=n+1;
  end;
  bfs;
  writeln(s);
end;
begin
  //ru;
  read(m);
  for i:=1 to m do
  main;
  //chu;
end.
#include<cstdio>
#include<cmath>
#include<string>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<iostream>
using namespace std;
int n,m,num,u[1100],v[1100],w[1100],s[1100];
long long ans,sum;
bool bo[1100];
void dfs()
{
	if (num==n) return;
	int now=99999999;
	int x=0;
	int summ;
	for (int i=1; i<=m; i++)
	{
		if (bo[u[i]]&&!bo[v[i]])
		{
			summ=s[u[i]]*w[i];
			if (summ<now) now=summ,x=i;
		}
		if (!bo[u[i]]&&bo[v[i]])
		{
			summ=s[v[i]]*w[i];
			if (summ<now) now=summ,x=i;
		}
	}
	if (bo[u[x]])
	{
		bo[v[x]]=1;
		s[v[x]]=s[u[x]]+1;
	}else
	{
		bo[u[x]]=1;
		s[u[x]]=s[v[x]]+1;
	}
	sum+=now;
	num++;
	dfs();
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	ans=99999999;
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++)
	{
		scanf("%d%d%d",&u[i],&v[i],&w[i]);
	} 
	for (int i=1; i<=n; i++)
	{
		for (int j=1; j<=n; j++) 
		{
		    bo[j]=0;
			s[j]=0;
    	}
		bo[i]=1;
		num=1;
		s[i]=1;
		sum=0;
		dfs();
		if (ans>sum) ans=sum;
	}
	cout<<ans<<endl;
	return 0;
	fclose(stdin); fclose(stdout);
}

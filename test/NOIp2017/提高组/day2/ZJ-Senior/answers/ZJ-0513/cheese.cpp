#include<cstdio>
#include<cmath>
#include<string>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<iostream>
using namespace std;
int t,f[1100],n,h,r;
long long x[1100],y[1100],z[1100];
int find(int u)
{
	if (f[u]==u) return u;
	f[u]=find(f[u]);
	return f[u];
}
long long kk(int u,int v)
{
	long long a=(x[u]-x[v])*(x[u]-x[v]);
	long long b=(y[u]-y[v])*(y[u]-y[v]);
	long long c=(z[u]-z[v])*(z[u]-z[v]);
	return a+b+c;
}
void add(int u,int v)
{
	int x=find(u);
	int y=find(v);
	f[x]=y;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for (int o=1; o<=t; o++)
	{
		scanf("%d%d%d",&n,&h,&r);
		for (int i=0; i<=n+1; i++) f[i]=i;
		long long b=4*r*r;
		for (int i=1; i<=n; i++)
		{
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		}
		for (int i=1; i<=n; i++)
		{
			for (int j=i+1; j<=n; j++)
			{
				long long a=kk(i,j);
				if (a<=b) add(i,j);
			}
    	}
		for (int i=1; i<=n; i++)
		{
			if (z[i]<=0)
			{
				if (z[i]+r>=0) add(0,i);
			    if (z[i]+r>=h) add(i,n+1);
			}else
			if (z[i]<=h)
			{
				if (z[i]-r<=0) add(0,i);
			    if (z[i]+r>=h) add(i,n+1);
			}else
			{
				if (z[i]-r<=0) add(0,i);
				if (z[i]-r<=h) add(0,i);
			}
		}
		if (find(0)==find(n+1)) cout<<"Yes"<<endl; else cout<<"No"<<endl;
	}
	return 0;
	fclose(stdin); fclose(stdout); 
} 

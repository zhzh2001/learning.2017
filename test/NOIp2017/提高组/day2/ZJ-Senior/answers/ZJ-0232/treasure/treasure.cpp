#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=20;
int b[N][N],g[N],gg[N],dis[N],q[100010];
bool v[N];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j,m,w,x,y,n,s,head,tail,mi=1e9,ma=-1,k;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++)
		for(j=1;j<=n;j++)
			b[i][j]=1e9;
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&w);
		b[x][y]=min(b[x][y],w);
		b[y][x]=b[x][y];
	}
	if(!m||n==1)
	{
		printf("0\n");
		return 0;
	}
	for(i=1;i<=n;i++)
	{
		memset(q,0,sizeof(q));
		for(j=1;j<=n;j++)
		{
			dis[j]=1e9;
			v[j]=0;
			g[j]=0;
		}
		dis[i]=0;
		head=0;
		tail=1;
		q[tail]=i;
		while(head!=tail)
		{
			head++;
			x=q[head];
			for(j=1;j<=n;j++)
				if(b[x][j]!=1e9&&dis[j]>dis[x]+b[x][j])
				{
					dis[j]=dis[x]+b[x][j];
					g[j]=g[x]+1;
					gg[j]=b[x][j];
					if(!v[j])
					{
						tail++;
						q[tail]=j;
						v[j]=1;
					}
				}
		}
		s=0;
		k=0;
		for(j=1;j<=n;j++)
		{
			if(dis[j]!=1e9)
				s+=gg[j]*g[j];
			else
				k++;
		}
		if(n-k>=ma)
		{
			mi=min(mi,s);
			ma=n-k;
		}
	}
	printf("%d",mi);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

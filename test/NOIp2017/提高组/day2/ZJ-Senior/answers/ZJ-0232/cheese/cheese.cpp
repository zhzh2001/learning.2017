#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1010;
struct kd{
	int x,y,z;
};
kd a[N];
int fi[N],n,h,r;
bool p[N][N],pp,v[N];
int comp(kd x,kd y)
{
	return x.z<y.z;
}
double dis(int x,int xx,int y,int yy,int z,int zz)
{
	return sqrt((x-xx)*(x-xx)+(y-yy)*(y-yy)+(z-zz)*(z-zz));
}
void dfs(int x)
{
	int i;
	if(a[x].z+r>=h)
	{
		pp=1;
		return;
	}
	v[x]=1;
	if(pp)
		return;
	for(i=1;i<=n;i++)
	{
		if(pp)
			return;
		if(p[x][i]&&!v[i])
			dfs(i);
		if(pp)
			return;
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t,i,j,f,l;
	scanf("%d",&t);
	while(t)
	{
		t--;
		pp=0;
		f=0;
		l=0;
		scanf("%d%d%d",&n,&h,&r);
		if(h==r)
		{
			printf("Yes\n");
			continue;
		}
		memset(p,0,sizeof(p));
		memset(v,0,sizeof(v));
		memset(fi,0,sizeof(fi));
		for(i=1;i<=n;i++)
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		sort(a+1,a+n+1,comp);
		for(i=1;i<=n;i++)
		{
			if(a[i].z-r<=0)
				fi[++f]=i;
			if(!l&&a[i].z+r>=h)
				l=1;
		}
		if(!f||!l)
		{
			printf("No\n");
			continue;
		}
		for(i=1;i<n;i++)
			for(j=i+1;j<=n;j++)
				if(dis(a[i].x,a[j].x,a[i].y,a[j].y,a[i].z,a[j].z)<=2*r)
				{
					p[i][j]=1;
					p[j][i]=1;
				}
		for(i=1;i<=f;i++)
		{
			dfs(fi[i]);
			if(pp)
				break;
		}
		if(pp)
			printf("Yes\n");
		else
			printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>

using namespace std;

struct node
{
	long long x,y,z;
	bool g[1010];
} a[1010];

int n,h,r;
bool flag;

double calc(int i,int j)
{
	long long xt=abs(a[i].x-a[j].x)*abs(a[i].x-a[j].x);
	long long yt=abs(a[i].y-a[j].y)*abs(a[i].y-a[j].y);
	long long zt=abs(a[i].z-a[j].z)*abs(a[i].z-a[j].z);
	return sqrt(xt+yt+zt);
}

void dfs(int from,int f)
{
	if(flag)return;
	if(a[from].g[n+1])
	{
		flag=1;
		return;
	}
	for(int i=1;i<=n;i++)
	{
		if(a[i].g[from]&&i!=f)
		{
			dfs(i,from);
		}
		if(flag)break;
	}
	return;
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int ttt;
	scanf("%d",&ttt);
	while(ttt--)
	{
		scanf("%d%d%d",&n,&h,&r);
		if(n==1)
		{
			long long t1,t2,t3;
			scanf("%lld%lld%lld",&t1,&t2,&t3);
			if(t3+r>=h&&t3<=r)printf("Yes\n");
			else printf("No\n");
			continue;
		}
		flag=0;
		for(int i=1; i<=n; i++)
		{
			for(int j=0;j<=n+1;j++)a[i].g[j]=0;
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
			if(a[i].z<=r)a[i].g[0]=1;
			if(a[i].z+r>=h)a[i].g[n+1]=1;
			if(a[i].g[0]&&a[i].g[n+1])flag=1;
		}
		if(flag)
		{
			printf("Yes\n");
			continue;
		}
		for(int i=1;i<n;i++)
		{
			for(int j=i+1;j<=n;j++)
			{
				double dis=calc(i,j);
				if(dis<=r*2)
				{
					a[i].g[j]=1;
					a[j].g[i]=1;
				}
			}
		}
		a[0].g[n+1]=0;
		dfs(0,-1);
		if(flag)printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

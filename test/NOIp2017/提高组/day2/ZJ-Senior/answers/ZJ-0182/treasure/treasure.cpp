#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>

using namespace std;

int n,m,w;
vector<int>q[15];

int dfs(int now,int fa,int d)
{
	int ret=w*d;
	for(int i=0; i<q[now].size(); i++)
	{
		if(q[now][i]==fa)continue;
		ret+=dfs(q[now][i],now,d+1);
	}
	return ret;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	int u,v;
	for(int i=1; i<=m; i++)
	{
		scanf("%d%d%d",&u,&v,&w);
		q[u].push_back(v);
		q[v].push_back(u);
	}
	int ans=1e9;
	for(int i=1; i<=n; i++)ans=min(ans,dfs(i,0,0));
	printf("%d",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>

using namespace std;

int n,m,q,a[300010],h[300010];
long long z[300010];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n==1)
	{
		for(int i=1;i<=m;i++)a[i]=i;
		int x,y;
		while(q--)
		{
			scanf("%d%d",&x,&y);
			printf("%d\n",a[y]);
			a[m+1]=a[y];
			for(int i=y;i<=m;i++)a[i]=a[i+1];
		}
		return 0;
	}
	memset(h,0,sizeof(h));
	for(int i=1;i<=n;i++)z[i]=i*m;
	int x,y;
	while(q--)
	{
		scanf("%d%d",&x,&y);
		if(y==m)
		{
			printf("%lld\n",z[x]);
			z[n+1]=z[x];
			for(int i=x;i<=n;i++)z[i]=z[i+1];
			continue;
		}
		int ans=(x-1)*m+y+h[x];
		if(ans>x*m)
		{
			ans=(ans-x*m)*m+x*m;
		}
		h[x]++;
		printf("%d\n",ans);
		z[n+1]=ans;
		for(int i=x;i<=n;i++)z[i]=z[i+1];
	}
	return 0;
}

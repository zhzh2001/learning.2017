#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#define N 20
#define M 10010
#define INF 1000000000
int n,m,edgenum,u,v,w,ans,sum,tot;
int a[N][N],c[N],b[M],vet[M],next[M],len[M],head[N],f[N];
struct node{int x,y,z;}ed[M];
using namespace std;
int getfather(int x){if (f[x]!=x) f[x]=getfather(f[x]);return f[x];}
void add(int u,int v,int w)
{
	vet[++edgenum]=v;
	next[edgenum]=head[u];
	head[u]=edgenum;
	len[edgenum]=w;
}
void dfs(int u,int fa,int num)
{
	for (int e=head[u];e;e=next[e])
	{
		int v=vet[e];
		if (v!=fa)
		{
			sum+=num*len[e];
			dfs(v,u,num+1);
		}
	}
}
void judge()
{
	edgenum=0;
	for (int i=1;i<=n;i++) head[i]=0;
	for (int i=1;i<=n;i++) f[i]=i;
	for (int i=1;i<n;i++)
	{
		int j=c[i];
		int u=getfather(ed[j].x),v=getfather(ed[j].y);
		if (u==v) return;
		f[u]=v;
		add(ed[j].x,ed[j].y,ed[j].z);
		add(ed[j].y,ed[j].x,ed[j].z);
	}
	for (int i=1;i<=n;i++)
	{
		sum=0;
		dfs(i,0,1);
		ans=min(ans,sum);
	}
}
void dfs(int t,int pre)
{
	if (t==n){judge();return;}
	for (int i=pre+1;i<=tot;i++)
	{
		c[t]=i;
		dfs(t+1,i);
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	srand(6662333);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) a[i][j]=INF;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&u,&v,&w);
		a[u][v]=min(a[u][v],w);
		a[v][u]=min(a[v][u],w);
	}
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++) if (a[i][j]!=INF)
		{
			ed[++tot].x=i;
			ed[tot].y=j;
			ed[tot].z=a[i][j];
		}
	if (n<=8)
	{
		ans=INF;
		dfs(1,0);
		printf("%d\n",ans);
		return 0;
	}
	ans=INF;
	for (int i=1;i<=tot;i++) b[i]=i;
	int T=5000;
	while (T--)
	{
		random_shuffle(b+1,b+tot+1);
		for (int i=1;i<=n;i++) f[i]=i;
		int cnt=0;
		edgenum=0;
		for (int i=1;i<=n;i++) head[i]=0;
		for (int i=1;i<=tot;i++)
		{
			int j=b[i];
			int u=getfather(ed[j].x),v=getfather(ed[j].y);
			if (u==v) continue;
			f[u]=v;cnt++;
			add(ed[j].x,ed[j].y,ed[j].z);
			add(ed[j].y,ed[j].x,ed[j].z);
			if (cnt==n-1) break;
		}
		for (int i=1;i<=n;i++)
		{
			sum=0;
			dfs(i,0,1);
			ans=min(ans,sum);
		}
	}
	printf("%d\n",ans);
}

#include<cstdio>
#include<cstring>
#define N 2010
#define M 5000010
using namespace std;
typedef long long ll;
int T,n,edgenum;
int vet[M],next[M],head[N],flag[N];
ll h,r,x[N],y[N],z[N];
void add(int u,int v)
{
	vet[++edgenum]=v;
	next[edgenum]=head[u];
	head[u]=edgenum;
}
void dfs(int u)
{
	flag[u]=1;
	for (int e=head[u];e;e=next[e])
	{
		int v=vet[e];
		if (!flag[v]) dfs(v);
	}
}
ll sqr(ll x){return x*x;}
ll dist(int i,int j)
{
	return sqr(x[i]-x[j])+sqr(y[i]-y[j]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		edgenum=0;
		memset(head,0,sizeof(head));
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++) if (dist(i,j)<=4*r*r-sqr(z[i]-z[j]))
			{
				add(i,j);
				add(j,i);
			}
		for (int i=1;i<=n;i++) if (z[i]<=r)
		{
			add(0,i);
			add(i,0);
		}
		for (int i=1;i<=n;i++) if (z[i]+r>=h)
		{
			add(i,n+1);
			add(n+1,i);
		}
		memset(flag,0,sizeof(flag));
		dfs(0);
		if (flag[n+1]) puts("Yes");else puts("No");
	}
}

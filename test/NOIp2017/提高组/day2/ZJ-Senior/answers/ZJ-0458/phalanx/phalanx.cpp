#include<cstdio>
#include<algorithm>
#define N 1000010
using namespace std;
typedef long long ll;
struct node{int x,y;}le[N];
int n,m,Q,root,x,y,tot;
int a[1010][1010],p[N],size[N],son[N][2],fa[N];
ll A[510][50010],c[N],key[N];
void pushup(int x){size[x]=size[son[x][0]]+size[son[x][1]]+1;}
void rotate(int x)
{
	int y=fa[x],z=fa[y],w=son[y][1]==x;
	son[y][w]=son[x][w^1];fa[son[x][w^1]]=y;
	son[x][w^1]=y;fa[y]=x;fa[x]=z;
	son[z][son[z][1]==y]=x;
	pushup(y);pushup(x);
}
void splay(int x,int k)
{
	for (int y;(y=fa[x])!=k;rotate(x))
	if (fa[y]!=k) rotate((son[y][0]==x)^(son[fa[y]][0]==y)?x:y);
	if (!k) root=x;
}
void build(int f,int l,int r)
{
	if (l>r) return;
	int mid=(l+r)>>1;
	fa[mid]=f;son[f][mid>f]=mid;
	size[mid]=(r-l+1);
	build(mid,l,mid-1);
	build(mid,mid+1,r);
}
void del(int x)
{
	//printf("%d\n",root);
	splay(x,0);
	//print();
	if (son[x][1]==0) {root=son[x][0];fa[root]=0;}else
	if (son[x][0]==0) {root=son[x][1];fa[root]=0;}else
	{
		int y=son[x][1];
		while (son[y][0]) y=son[y][0];
		splay(y,x);
		fa[y]=0;
		fa[son[x][0]]=y;son[y][0]=son[x][0];
		pushup(y);root=y;
	}
	fa[x]=0;size[x]=1;son[x][0]=son[x][1]=0;
}
void insert(int x)
{
	int y=root;
	while (son[y][1]) y=son[y][1];
	splay(y,0);
	son[y][1]=x;fa[x]=y;
	pushup(y);
	//for (int i=1;i<=3;i++) printf("%d %d %d\n",i,fa[i],size[i]);
}
int find(int x,int k)
{
	if (size[son[x][0]]+1==k) return x;else
		if (size[son[x][0]]>=k) return find(son[x][0],k);
	return find(son[x][1],k-size[son[x][0]]-1);
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	if (Q<=500&&n<=1000&&m<=1000)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
		while (Q--)
		{
			scanf("%d%d",&x,&y);printf("%d\n",a[x][y]);
			int t=a[x][y];
			for (int i=y;i<m;i++) a[x][i]=a[x][i+1];
			for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
			a[n][m]=t;
		}
		return 0;
	}
	if (Q<=500)
	{
		tot=0;
		for (int i=1;i<=Q;i++)
		{
			scanf("%d%d",&le[i].x,&le[i].y);
			p[++tot]=le[i].x;
		}
		sort(p+1,p+tot+1);
		tot=unique(p+1,p+tot+1)-p-1;
		for (int i=1;i<=tot;i++)
			for (int j=1;j<m;j++) A[i][j]=(ll)(p[i]-1)*m+j;
		for (int i=1;i<=n;i++) c[i]=(ll)i*m;
		for (int i=1;i<=Q;i++)
		{
			int t=lower_bound(p+1,p+tot+1,le[i].x)-p;
			if (le[i].y==m)
			{
				printf("%lld\n",c[le[i].x]);
				ll w=c[le[i].x];
				for (int j=le[i].x;j<n;j++) c[j]=c[j+1];
				c[n]=w;
			}else
			{
				printf("%lld\n",A[t][le[i].y]);
				ll w=A[t][le[i].y];
				for (int j=le[i].y;j<m-1;j++) A[t][j]=A[t][j+1];
				A[t][m-1]=c[le[i].x];
				for (int j=le[i].x;j<n;j++) c[j]=c[j+1];
				c[n]=w;
			}
		}
		return 0;
	}
	for (int i=1;i<=m;i++) key[i]=i;
	for (int i=2;i<=n;i++) key[i+m-1]=(ll)i*m;
	build(0,1,n+m-1);root=(n+m)>>1;
	while (Q--)
	{
		scanf("%d%d",&x,&y);
		int t=find(root,y);
		printf("%lld\n",key[t]);
		del(t);insert(t);
	}
}

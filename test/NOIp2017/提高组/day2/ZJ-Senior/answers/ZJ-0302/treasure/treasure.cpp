#include<iostream>
#include<vector>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
struct edge{
	int dis,t,next;
	ll v;
}e[2010];
vector<edge> v;
int n,m,nedge;
ll ans;
int head[20],f[20],a[20][20];
 bool cmp(edge a,edge b)
{
	return a.v>b.v;
}
 void add(int a,int b,int v)
{
	e[++nedge].t=b;
	e[nedge].dis=0;
	e[nedge].v=v;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 void work(int s)
{
	v.clear();
	memset(f,0,sizeof(f));
	f[s]=1;
	for (int i=head[s];i;i=e[i].next)
	{
		edge k=e[i];
		k.dis=1;
		k.v=k.v*k.dis;
		v.push_back(k);
	}
	int have=1;
	ll sum=0;
	while (have<n)
	{
		sort(v.begin(),v.end(),cmp);
		edge tt=v[v.size()-1];
		while (f[tt.t])
		{
			v.pop_back();
			tt=v[v.size()-1];
		}
		v.pop_back();
		++have;
		sum+=tt.v;
		f[tt.t]=1;
		for (int i=head[tt.t];i;i=e[i].next)
		{
			if (f[e[i].t]) continue;
			edge k=e[i];
			k.dis=tt.dis+1;
			k.v=k.v*k.dis;
			v.push_back(k);
		}
	}
	ans=min(ans,sum);
}
 int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x3f,sizeof(a));
	for (int i=1;i<=m;++i)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		a[u][v]=a[v][u]=min(w,a[u][v]);
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
			if (a[i][j]!=0x3f3f3f3f)
	   			add(i,j,a[i][j]);
	ans=0x3f3f3f3f;
	for (int i=1;i<=n;++i)
		work(i);
	printf("%lld\n",ans);
}

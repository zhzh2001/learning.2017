#include<iostream>
#include<vector>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const double eps=1e-8;
struct data{
	ll x,y,z;
}a[1010];
int fa[1010];
double d[1010][1010];
 ll sqr(ll x)
{
	return x*x;
}
 int find(int v)
{
	return fa[v]==v?v:fa[v]=find(fa[v]);
}
 bool work()
{
	ll n,h,r;
	scanf("%lld%lld%lld",&n,&h,&r);
	r*=2;
	for (int i=1;i<=n;++i)
		scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
	if (n==1&&a[1].z-0<=r&&h-a[1].z<=r) return true;
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
		{
			if (i==j) d[i][j]=2000000000;
			if (i!=j) d[i][j]=(double)sqrt((double)sqr(a[i].x-a[j].x)+(double)sqr(a[i].y-a[j].y)+(double)sqr(a[i].z-a[j].z));
		}
	for (int i=1;i<=n;++i) fa[i]=i;
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
		{
			if (d[i][j]-r<=eps)
			{
				int ti=find(i);
				int tj=find(j);
				if (ti!=tj) fa[i]=j;
			}
		}
	 for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
		{
			int ti=find(i);
			int tj=find(j);
			if (a[i].z-0<=r&&h-a[j].z<=r&&ti==tj)
			return true;
		}
	return false;
}
 int main()
{
 	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		if (work()) puts("Yes");
		else puts("No");
	}
}

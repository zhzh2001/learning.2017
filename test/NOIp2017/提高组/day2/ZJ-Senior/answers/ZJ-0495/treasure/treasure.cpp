#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>

using namespace std;

#define Rep(i,x,y) for (i=x;i<=y;++i)

const int N = 20;

struct Edge {
	int x,y,pre;
} E[N*N];
int n,m,A[N][N],Deep[N],tot,To[N*N],Enext[N*N],Last[N*N],Etlast[N*N],pre[N],j,F[N*N],Visit[N],Q[N],h,s,i,t,x,y,c,vLast,La,Ans,totDeep;

void Add(int x,int y,int t) {
	To[++tot]=y;  Enext[tot]=Last[x];  Last[x]=tot;  Etlast[tot]=t;
}

void Dfs_Tree(int x,int y) {
	int i;
	if (t>=Ans) return;
	for (i=Last[x];i;i=Enext[i]) if (To[i]!=y) {
		Deep[To[i]]=Deep[x]+1;
		t=t+Etlast[i]*(Deep[To[i]]-1);
		Dfs_Tree(To[i],x);
	}
}

int Find(int x) {
	if (F[x]==x) return x;  else return F[x]=Find(F[x]);
}

void Dfs(int x,int y) {
	int i,s;
	if (x>n-1) {
		Rep(i,1,n) F[i]=i;
		Rep(i,1,n-1) F[Find(E[pre[i]].x)]=Find(E[pre[i]].y);
		Rep(i,1,n) if (Find(i)!=Find(1)) return;
		Rep(i,1,n) Last[i]=0;  tot=0;
		Rep(i,1,n-1) Add(E[pre[i]].x,E[pre[i]].y,E[pre[i]].pre),Add(E[pre[i]].y,E[pre[i]].x,E[pre[i]].pre);
		Rep(s,1,n) {
			t=0;  Deep[s]=1;  Dfs_Tree(s,-1);
			if (t<Ans) Ans=t;
		}
		return;
	}
	Rep(i,y+1,totDeep) pre[x]=i,Dfs(x+1,i);
}

int main () {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m),memset(A,0,sizeof(A));
	La=0;
	Rep(i,1,m) {
	    scanf("%d%d%d",&x,&y,&c);
	    if (i<=1) vLast=c;
	    else if (c!=vLast) La=1;
	    if (!A[x][y] && !A[y][x]) A[x][y]=A[y][x]=c;
	    else t=min(A[x][y],c),A[x][y]=A[y][x]=t;
	}
	if (!La) {
	   Ans=1<<29;
	   Rep(s,1,n) {
	       memset(Visit,0,sizeof(Visit));
		   h=0,t=1,Q[1]=s,Deep[s]=1,Visit[s]=1;
		   while (h<t) {
		       x=Q[++h];
			   Rep(i,1,n) if (!Visit[i] && A[x][i]) Deep[i]=Deep[x]+1,Q[++t]=i,Visit[i]=1;
		   }
		   t=0;
		   Rep(i,1,n) t=t+Deep[i];
		   t=t-n;
		   t=t*c;
		   if (t<Ans) Ans=t;
		}
		printf("%d\n",Ans);
		fclose(stdin);  fclose(stdout);
	}
	else {
		Rep(i,1,n) F[i]=i;
		tot=0;  Rep(i,1,n) Rep(j,i+1,n) if (A[i][j]) E[++totDeep]=(Edge){i,j,A[i][j]};
		Ans=1<<29;  Dfs(1,0);
		printf("%d\n",Ans);
		fclose(stdin);  fclose(stdout);
	}
	return 0;
}

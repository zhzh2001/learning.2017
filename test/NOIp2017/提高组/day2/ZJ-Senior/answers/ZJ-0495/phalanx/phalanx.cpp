#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>

using namespace std;

#define Rep(i,x,y) for (i=x;i<=y;++i)

const int N = 100010;

struct Query {
	int x,y;
} A[N];

int n,m,Q,i,j,Qleft[N],Qright[N],q,t,x,y,Visit[N],pre[29000000];

int main () {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	Rep(q,1,Q) scanf("%d%d",&x,&y),A[q]=(Query){x,y};
	Rep(i,1,n) Visit[i]=0;
	Rep(q,1,Q) Visit[A[q].x]=1;
	Rep(i,1,n) {
		if (!Visit[i]) Qleft[i]=Qright[i-1]+1,Qright[i]=Qleft[i];
		else Qleft[i]=Qright[i-1]+1,Qright[i]=Qleft[i]+m-1;
	}
	Rep(i,1,n) {
		if (!Visit[i]) pre[Qright[i]]=i*m;
		else Rep(j,1,m) pre[Qleft[i]+j-1]=(i-1)*m+j;
	}
	t=0;
	Rep(q,1,Q) {
		if (t) pre[Qright[n]]=t;
		x=A[q].x,y=A[q].y;
		printf("%d\n",pre[Qleft[x]+y-1]);
		t=pre[Qleft[x]+y-1];
		if (y<m) Rep(i,y,m-1) pre[Qleft[x]+i-1]=pre[Qleft[x]+i];
		if (x<n) Rep(i,x,n-1) pre[Qright[i]]=pre[Qright[i+1]];
	}
	fclose(stdin);  fclose(stdout);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cstring>

using namespace std;

#define Rep(i,x,y) for (i=x;i<=y;++i)

#define LL long long

const int N = 105050;

struct Node {
	int x,y,z;
} A[N];

int T,n,H,r,i,End[N],Visit[N],h,t,Ans,Q[N],now;
LL pr;

LL Getdis(Node A,Node B) {
	return ((LL)A.x-B.x)*((LL)A.x-B.x)+((LL)A.y-B.y)*((LL)A.y-B.y)+((LL)A.z-B.z)*((LL)A.z-B.z);
}

int main () {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d%d%d",&n,&H,&r);  pr=4LL*(LL)r*(LL)r;
		Rep(i,1,n) scanf("%d%d%d",&A[i].x,&A[i].y,&A[i].z);
		Rep(i,1,n) End[i]=0;
		Rep(i,1,n) if (A[i].z+r>=H) End[i]=1;
		Rep(i,1,n) Visit[i]=0;
		h=0,t=0,Ans=0;
		Rep(i,1,n) if (A[i].z-r<=0) {  Visit[i]=1,Q[++t]=i;  if (End[i]) Ans=1;  }
		if (!Ans) {
		    while (h<t) {
		    	now=Q[++h];
		    	Rep(i,1,n) if (!Visit[i] && Getdis(A[now],A[i])<=pr) {
		    		Visit[i]=1,Q[++t]=i;
		    		if (End[i]) {  Ans=1;  break;  }
				}
				if (Ans) break;
			}
		}
		if (Ans) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);  fclose(stdout);
	return 0;
}

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define gc getchar()
#define ll long long
#define N 13
using namespace std;
int n,m,f[N][N],dp[N][1<<N][N],bit[N];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	memset(f,0x3f,sizeof(f));
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read(),z=read();
		f[x][y]=min(f[x][y],z);
		f[y][x]=min(f[y][x],z);
	}
	bit[0]=1;
	for (int i=1;i<=n;i++) bit[i]=bit[i-1]<<1;
	memset(dp,0x3f,sizeof(dp));
	for (int i=1;i<=n;i++)
		for (int j=0;j<n;j++)
			dp[i][bit[i-1]][j]=0;
	for (int dis=n-2;dis>=0;dis--)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (f[i][j]<=500000)
				{
					for (int now=0;now<bit[n];now++)
						if ((now&bit[i-1])&&(now&bit[j-1]))
							for (int last=(now^bit[i-1]);last;last=(last-1)&(now^bit[i-1]))
								if ((last&bit[j-1]))
									dp[i][now][dis]=min(dp[i][now][dis],dp[i][now^last][dis]+dp[j][last][dis+1]+f[i][j]*(dis+1));
				}
	}
	int Ans=2147483647;
	for (int i=1;i<=n;i++)
		Ans=min(Ans,dp[i][bit[n]-1][0]);
	printf("%d\n",Ans);
	return 0;
}


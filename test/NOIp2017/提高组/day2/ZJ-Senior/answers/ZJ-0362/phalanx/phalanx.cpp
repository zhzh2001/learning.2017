#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define gc getchar()
#define ll long long
#define N 300009
#define M 1009
#define rd(x) (((ll)rand()*rand()%(x))+1)
using namespace std;
ll n,m,q,a[M][M];
ll read()
{
	ll x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	ll s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
void work1()
{
	for (ll i=1;i<=n;i++)
		for (ll j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
	while (q--)
	{
		ll x=read(),y=read();
		printf("%lld\n",a[x][y]);
		ll now=a[x][y];
		for (ll i=y;i<m;i++)
			a[x][i]=a[x][i+1];
		for (ll i=x;i<n;i++)
			a[i][m]=a[i+1][m];
		a[n][m]=now;
	}
}
struct qry
{
	ll x,y;
}Q[N];
struct node
{
	node *ls,*rs;
	ll size,key,val,pos;
	node(ll x,ll y)
	{
		key=rd(10000000);
		val=x,pos=y;
		size=1;
		ls=rs=NULL;
	}
	void up()
	{
		size=1;
		if (ls!=NULL) size+=ls->size;
		if (rs!=NULL) size+=rs->size;
	}
	ll get()
	{
		if (ls!=NULL) return ls->size+1;
		else return 1;
	}
};
typedef node* pnode;
pnode merge(pnode L,pnode R)
{
	if (L==NULL) return R;
	if (R==NULL) return L;
	if (L->key<=R->key)
	{
		R->ls=merge(L,R->ls);
		R->up();
		return R;
	}
	else
	{
		L->rs=merge(L->rs,R);
		L->up();
		return L;
	}
}
void split(pnode root,pnode &L,pnode &R,ll x)//<x->L >=x->R
{
	if (root==NULL)
	{
		L=NULL;
		R=NULL;
		return;
	}
	if (x<=root->pos)
	{
		split(root->ls,L,root->ls,x);
		R=root;
		R->up();
		return;
	}
	else
	{
		split(root->rs,root->rs,R,x);
		L=root;
		L->up();
		return;
	}
}
ll find(pnode now,ll x)
{
	if (now->get()==x) return now->pos;
	if (now->get()>x) return find(now->ls,x);
	return find(now->rs,x-now->get());
}
ll Find(pnode now,ll x)
{
	if (now->get()==x) return now->val;
	if (now->get()>x) return Find(now->ls,x);
	return Find(now->rs,x-now->get());
}
ll v[N<<2];
void work2()
{
	pnode root=NULL,L,R,MID;
	for (ll i=1;i<=m;i++) root=merge(root,new node(i,i));
	for (ll i=1;i<=n;i++) v[i]=i*m;
	for (ll i=1;i<=q;i++)
	{
		ll x=Q[i].x,y=Q[i].y;
		ll pos=find(root,y),val=Find(root,y);
		printf("%lld\n",val);
		split(root,L,R,pos);
		split(R,MID,R,pos+1);
		root=merge(L,R);
		v[n+i]=val;
		root=merge(root,new node(v[i+1],m+i));
	}
}
struct Node
{
	Node *ls,*rs;
	ll size,key,val,lpos,rpos;
	Node(ll x,ll y,ll z)
	{
		key=rd(10000000);
		val=x,lpos=y,rpos=z;
		size=rpos-lpos+1;
		ls=rs=NULL;
	}
	void up()
	{
		size=rpos-lpos+1;
		if (ls!=NULL) size+=ls->size;
		if (rs!=NULL) size+=rs->size;
	}
	ll get()
	{
		if (ls!=NULL) return ls->size;
		else return 0;
	}
};
typedef Node* pNode;
pNode Merge(pNode L,pNode R)
{
	if (L==NULL) return R;
	if (R==NULL) return L;
	if (L->key<=R->key)
	{
		R->ls=Merge(L,R->ls);
		R->up();
		return R;
	}
	else
	{
		L->rs=Merge(L->rs,R);
		L->up();
		return L;
	}
}
void Split(pNode root,pNode &L,pNode &R,ll x)//<x->L >=x->R
{
	if (root==NULL)
	{
		L=NULL;
		R=NULL;
		return;
	}
	if (x<=root->lpos)
	{
		Split(root->ls,L,root->ls,x);
		R=root;
		R->up();
		return;
	}
	else
	{
		Split(root->rs,root->rs,R,x);
		L=root;
		L->up();
		return;
	}
}
ll fin(pNode now,ll x)
{
	if (now->get()+1<=x&&x<=now->get()+now->rpos-now->lpos+1) return now->lpos;
	if (now->get()>=x) return fin(now->ls,x);
	return fin(now->rs,x-now->get()-(now->rpos-now->lpos+1));
}
ll Fin(pNode now,ll x)
{
	if (now->get()+1<=x&&x<=now->get()+now->rpos-now->lpos+1) return now->val+(x-now->get())-1;
	if (now->get()>=x) return Fin(now->ls,x);
	return Fin(now->rs,x-now->get()-(now->rpos-now->lpos+1));
}
ll F(pNode now,ll x)
{
	if (now->get()+1<=x&&x<=now->get()+now->rpos-now->lpos+1) return now->lpos+(x-now->get()-1);
	if (now->get()>=x) return F(now->ls,x);
	return F(now->rs,x-now->get()-(now->rpos-now->lpos+1));
}
void work3()
{
	pNode rt[N],L,R,MID,LL,RR;
	pnode root=NULL,LLL,RRR,Mid;
	for (ll i=1;i<=n;i++) rt[i]=new Node((i-1)*m+1,1,m-1);
	for (ll i=1;i<=n;i++) root=merge(root,new node(i*m,i));
	for (ll i=1;i<=q;i++)
	{
		ll x=Q[i].x,y=Q[i].y;
		if (y<m)
		{
			ll pos=fin(rt[x],y),val=Fin(rt[x],y),mpos=F(rt[x],y),Ans=0;
			printf("%lld\n",Ans=val);
			Split(rt[x],L,R,pos);
			Split(R,MID,R,pos+1);
			if (mpos>MID->lpos) LL=new Node(MID->val,MID->lpos,mpos-1);
			else LL=NULL;
			if (MID->rpos>mpos) RR=new Node(MID->val+mpos-MID->lpos+1,mpos+1,MID->rpos);
			else RR=NULL;
			MID=Merge(LL,RR);
			rt[x]=Merge(L,MID);
			rt[x]=Merge(rt[x],R);
			pos=find(root,x),val=Find(root,x);
			split(root,LLL,RRR,pos);
			split(RRR,Mid,RRR,pos+1);
			root=merge(LLL,RRR);
			root=merge(root,new node(Ans,n+i));
			rt[x]=Merge(rt[x],new Node(val,m+i,m+i));
		}
		else
		{
			ll pos=find(root,x),val=Find(root,x);
			printf("%lld\n",val);
			split(root,LLL,RRR,pos);
			split(RRR,Mid,RRR,pos+1);
			root=merge(LLL,RRR);
			root=merge(root,new node(val,n+i));
		}
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n<=1000&&m<=1000&&(ll)(n+m)*q<=50000000)
	{
		work1();
		return 0;
	}
	bool flag=1;
	for (ll i=1;i<=q;i++)
	{
		Q[i].x=read(),Q[i].y=read();
		if (Q[i].x!=1) flag=0;
	}
	if (flag)
	{
		work2();
		return 0;
	}
	work3();
	return 0;
}


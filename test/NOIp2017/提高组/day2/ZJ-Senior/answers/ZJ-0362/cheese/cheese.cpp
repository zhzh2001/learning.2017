#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#define gc getchar()
#define N 1009
#define ull unsigned long long
using namespace std;
int n,h,r,x[N],y[N],z[N],f[N][N],vis[N],q[N];
int read()
{
	int x=1;
	char ch;
	while (ch=gc,ch<'0'||ch>'9') if (ch=='-') x=-1;
	int s=ch-'0';
	while (ch=gc,ch>='0'&&ch<='9') s=s*10+ch-'0';
	return s*x;
}
ull dis(int i,int j)
{
	return (ull)(x[i]-x[j])*(x[i]-x[j])+(ull)(y[i]-y[j])*(y[i]-y[j])+(ull)(z[i]-z[j])*(z[i]-z[j]);
}
bool bfs()
{
	memset(vis,0,sizeof(vis));
	int head=1,tail=0;
	q[++tail]=0;
	vis[0]=1;
	while (head<=tail)
	{
		int x=q[head];
		head++;
		for (int i=0;i<=n+1;i++)
			if (f[x][i]&&!vis[i])
			{
				q[++tail]=i;
				vis[i]=1;
			}
	}
	return vis[n+1];
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while (T--)
	{
		n=read(),h=read(),r=read();
		for (int i=1;i<=n;i++)
		{
			x[i]=read();
			y[i]=read();
			z[i]=read();
		}
		memset(f,0,sizeof(f));
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (dis(i,j)<=(ull)4*r*r) f[i][j]=f[j][i]=1;
		for (int i=1;i<=n;i++)
		{
			if (abs(z[i])<=r) f[0][i]=1;
			if (abs(z[i]-h)<=r) f[i][n+1]=1;
		}
		bool flag=bfs();
		if (flag) puts("Yes");
		else puts("No");
	}
	return 0;
}


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<iostream>
	using namespace std;
	
int n,m;
int g[15][15],p;
int tot,head[15],to[2005],v[2005],Next[2005];
int st,ed,h[15];
bool inh[15];
int low[15];
int res,minmum;

void add(int xa,int yb,int zc){
	tot++;
	to[tot]=yb;
	v[tot]=zc;
	Next[tot]=head[xa];
	head[xa]=tot;
	return ;
}

void dfs(int cost){
	bool w;
	w=0;
	for(int i=1;i<=n;i++){
		if(low[i]!=0){
			for(int j=head[i];j!=-1;j=Next[j]){
				int yb,zc;
				yb=to[j];
				zc=v[j];
				if(low[yb]==0){
					low[yb]=low[i]+1;
					dfs(cost+low[i]*zc);
					low[yb]=0;
				}
			}
		}
		else{
			w=1;
		}
	}
	if(w==0){
		minmum=min(minmum,cost);
	}
	return ;
}
	
int main(){
	
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++) for(int j=1;j<=n;j++) if(i!=j) g[i][j]=1000000005;
	for(int i=1;i<=m;i++){
		int xa,yb,zc;
		scanf("%d%d%d",&xa,&yb,&zc);
		g[xa][yb]=min(g[xa][yb],zc);
		g[yb][xa]=min(g[yb][xa],zc);
	}
	bool bo;
	bo=0;
	p=-1;
	tot=-1;
	for(int i=1;i<=n;i++) head[i]=-1;
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(g[i][j]!=1000000005){
				if(p==-1) p=g[i][j];
				else{
					if(p!=g[i][j]) bo=1;
				}
				add(i,j,g[i][j]);
				add(j,i,g[i][j]);
			}
		}
	}
	if(bo==0){
		res=1000000005;
		for(int g=1;g<=n;g++){
			for(int i=1;i<=n;i++) inh[i]=0;
			for(int i=1;i<=n;i++) low[i]=0;
			st=0;
			ed=1;
			h[ed]=g;
			inh[g]=1;
			low[g]=1;
			minmum=0;
			while(st<ed){
				st++;
				int xa;
				xa=h[st];
				for(int i=head[xa];i!=-1;i=Next[i]){
					int yb,zc;
					yb=to[i];
					zc=v[i];
					if(inh[yb]==0){
						inh[yb]=1;
						ed++;
						h[ed]=yb;
						low[yb]=low[xa]+1;
						minmum=minmum+low[xa]*zc;
					}
				}
			}
			res=min(res,minmum);
		}
		printf("%d\n",res);
		
		fclose(stdin);
		fclose(stdout);
	
		return 0;	
	}
	res=1000000005;
	for(int g=1;g<=n;g++){
		for(int i=1;i<=n;i++) low[i]=0;
		low[g]=1;
		minmum=1000000005;
		dfs(0);
		res=min(res,minmum);
	}
	printf("%d\n",res);
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}
/*
5 6
1 2 1
1 3 1
1 4 1
1 5 1
2 3 1
4 5 1
*/

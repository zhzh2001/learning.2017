#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<iostream>
	using namespace std;

struct node{
	long long X,Y,Z;
}a[1005];

int n;
long long h,r,xa,yb;
int tot,head[1010],to[1003010],Next[1003010];
bool bo[1010];	
	
void add(int xx,int yy){
	tot++;
	to[tot]=yy;
	Next[tot]=head[xx];
	head[xx]=tot;
	return ;
}

void dfs(int xx){
	bo[xx]=1;
	if(xx==n+1){
		return ;
	}
	for(int i=head[xx];i!=-1;i=Next[i]){
		int yy;
		yy=to[i];		
		if(bo[yy]==0) dfs(yy);
	}
	return ;
}
	
int main(){
	
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	int  T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		yb=r*r*4LL;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&a[i].X,&a[i].Y,&a[i].Z);
		}
		tot=-1;
		for(int i=0;i<=n+1;i++) head[i]=-1;
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				long long X1,X2,Y1,Y2,Z1,Z2;
				X1=a[i].X;
				Y1=a[i].Y;
				Z1=a[i].Z;
				X2=a[j].X;
				Y2=a[j].Y;
				Z2=a[j].Z;
				xa=(X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)+(Z1-Z2)*(Z1-Z2);
				if(xa<=yb){
					add(i,j);
					add(j,i);
				}
			}
		}
		for(int i=1;i<=n;i++){
			if(abs(a[i].Z-0LL)<=r){
				add(0,i);
				add(i,0);
			}
			if(abs(a[i].Z-h)<=r){
				add(n+1,i);
				add(i,n+1);
			}
		}
		for(int i=0;i<=n+1;i++) bo[i]=0;
		dfs(0);
		if(bo[n+1]==1){
			printf("Yes\n");
		}
		else printf("No\n");
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}

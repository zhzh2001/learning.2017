#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<string>
#include<algorithm>
#include<iostream>
	using namespace std;
	
int n,m,q;
int a[1005][1005];
int h[300005];	
	
int main(){
	
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	scanf("%d%d%d",&n,&m,&q);
	if(n<=1000 && m<=1000){
		int k;
		k=0;
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				k++;
				a[i][j]=k;
			}
		}
		while(q--){
			int xa,yb,zc;
			scanf("%d%d",&xa,&yb);
			zc=a[xa][yb];
			printf("%d\n",zc);
			for(int j=yb;j<=m-1;j++) a[xa][j]=a[xa][j+1];
			for(int i=xa;i<=n-1;i++) a[i][m]=a[i+1][m];
			a[n][m]=zc;
		}
	}
	else if(n==1){
		for(int i=1;i<=m;i++) h[i]=i;
		while(q--){
			int xa,yb,zc;
			scanf("%d%d",&xa,&yb);
			zc=h[yb];
			printf("%d\n",zc);
			for(int j=yb;j<=m-1;j++) h[j]=h[j+1];
			h[m]=zc;
		}
	}
	else{
		while(q--){
			int xa,yb;
			long long zc;
			scanf("%d%d",&xa,&yb);
			zc=(long long)(xa-1)*(long long)(m)+yb;
			printf("%lld\n",zc);
		}
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}
/*
3 5 4
1 2
2 5
2 2
2 4

1 7 5
1 2
1 2
1 3
1 5
1 6
*/

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
bool f[20],f2[20][20];
int deep[20],v[20][20],n,m,a[22],x,y,v2;
int ans=2000000000;

void work(int x,int tot){
	if (x>n){
		if (tot<ans)
			ans=tot;
		return;
	}
	for (int i=1;i<=n;i++)
		if (f[i]==false){
			for (int j=1;j<=x-1;j++){
				int minn=200000000;
				if (f2[i][a[j]])
					if (deep[a[j]]*v[i][a[j]]<minn){
						deep[i]=deep[a[j]]+1;
						minn=deep[a[j]]*v[i][a[j]];
						f[i]=true;
					}
				if (f[i]){
					a[x]=i;
					work(x+1,tot+minn);
				}
				f[i]=false;
			}
		}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			v[i][j]=2000000000;
	bool f3=true;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&v2);
		v[x][y]=min(v[x][y],v2);
		v[y][x]=min(v[y][x],v2);
		f2[x][y]=true;
		f2[y][x]=true;
	}
	for (int i=1;i<=n;i++){
		deep[i]=1;
		f[i]=true;
		a[1]=i;
		work(2,0);
		deep[i]=0;
		f[i]=false;
	}
	printf("%d\n",ans);
	return 0;
}

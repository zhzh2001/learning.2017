#include<cstdio>
#include<cstring>
using namespace std;
int way[15][15],n;
long long ans=-1;
bool mark[15];
int js;
struct p10 {
	int cnt[15],tot[15];
	void solve(int x) {
		long long res=0;
		memset(mark,0,sizeof(mark));
		memset(cnt,63,sizeof(cnt));
		memset(tot,63,sizeof(tot));
		cnt[x]=0,tot[x]=0;
		while(1) {
			int v=-1;
			for(int i=1; i<=n; i++) {
				if(mark[i])continue;
				if(v==-1||tot[v]>tot[i])v=i;
			}
			if(v==-1)break;
			mark[v]=1;
			res+=tot[v];
			for(int i=1; i<=n; i++) {
				if(way[v][i]==-1)continue;
				if((cnt[v]+1)*way[v][i]<tot[i]) {
					cnt[i]=cnt[v]+1;
					tot[i]=cnt[i]*way[v][i];
				}
			}
		}
		if(ans==-1||ans>res)ans=res;
	}
} p10;
struct shui {
	int pt[15];
	void dfs(int cnt,long long sum) {
		js++;
		if(js>=1e7)return;
		if(ans!=-1&&sum>ans)return;
		if(cnt==n-1) {
			if(ans==-1||sum<ans)ans=sum;
			return;
		}
		for(int x=1; x<=n; x++) {
			if(!mark[x])continue;
			for(int i=1; i<=n; i++) {
				if(way[x][i]==-1||mark[i])continue;
				mark[i]=1;
				pt[i]=pt[x]+1;
				dfs(cnt+1,sum+pt[i]*way[x][i]);
				mark[i]=0;
			}
		}
	}
	void solve() {
		for(int i=1; i<=n; i++)mark[i]=0;
		for(int i=1; i<=n; i++) {
			mark[i]=1;
			pt[i]=0;
			dfs(0,0);
			mark[i]=0;
		}
	}
} shui;
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int m,a,b,c;
	scanf("%d %d",&n,&m);
	memset(way,-1,sizeof(way));
	for(int i=1; i<=m; i++) {
		scanf("%d %d %d",&a,&b,&c);
		if(way[a][b]==-1||way[a][b]>c)way[a][b]=way[b][a]=c;
	}
	for(int i=1; i<=n; i++) {
		p10.solve(i);
	}
	shui.solve();
	printf("%lld",ans);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
//long long 取模 乘法溢出 文件名 内存
struct node {
	int x,y,z;
} A[1005];
bool mark[1005];
bool cmp(node a,node b) {
	return a.z<b.z;
}
long long R;
bool check(int i,int j) {
	return 1ll*(A[i].x-A[j].x)*(A[i].x-A[j].x)+1ll*(A[i].y-A[j].y)*(A[i].y-A[j].y)+1ll*(A[i].z-A[j].z)*(A[i].z-A[j].z)<=R;
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	int n,h,r;
	scanf("%d",&T);
	while(T--) {
		scanf("%d %d %d",&n,&h,&r);
		R=1ll*4*r*r;
		for(int i=1; i<=n; i++) {
			mark[i]=0;
			scanf("%d %d %d",&A[i].x,&A[i].y,&A[i].z);
		}
		sort(A+1,A+1+n,cmp);
		bool f=0;
		for(int i=1; i<=n; i++) {
			if(A[i].z<=r)mark[i]=1;
			if(!mark[i])continue;
			if(h-A[i].z<=r) {
				f=1;
				printf("Yes\n");
				break;
			}
			for(int j=i+1; j<=n; j++) {
				if(mark[j])continue;
				if(check(i,j)) {
					mark[j]=1;
				}
			}
			//printf("i=%d ",i);
		}
		if(!f)printf("No\n");
	}
	return 0;
}

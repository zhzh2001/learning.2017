#include<bits/stdc++.h>
#define N 10009
using namespace std;

int n,m,cas,p[N][N];
int read(){
	int x=0; char ch=getchar();
	while (ch<'0' || ch>'9') ch=getchar();
	while (ch>='0' && ch<='9'){ x=x*10+ch-'0'; ch=getchar(); }
	return x;
}
int main(){
	freopen("phalanx.in","r",stdin); freopen("phalanx2.out","w",stdout);
	m=read(); n=read(); cas=read();
	int i,j,x,y,z;
	for (i=1,x=0; i<=m; i++)
		for (j=1; j<=n; j++) p[i][j]=++x;
	while (cas--){
		x=read(); y=read(); z=p[x][y];
		for (i=y; i<n; i++) p[x][i]=p[x][i+1];
		for (i=x; i<m; i++) p[i][n]=p[i+1][n];
		p[m][n]=z;
		printf("%d\n",z);
	}
	return 0;
}


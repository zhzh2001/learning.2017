#include<bits/stdc++.h>
#define ll long long
#define f(x) ((ll)(x)*(x))
#define N 1009
using namespace std;

int n,H,R,a[N],b[N],c[N],fa[N];
int getfa(int x){ return x==fa[x]?x:fa[x]=getfa(fa[x]); }
void mrg(int x,int y){
	x=getfa(x); y=getfa(y); if (x!=y) fa[x]=y;
}
bool check(int i,int j){
	ll tmp=f(a[i]-a[j])+f(b[i]-b[j])+f(c[i]-c[j]);
	return tmp<=4ll*R*R;
}
int main(){
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	int cas; scanf("%d",&cas);
	while (cas--){
		scanf("%d%d%d",&n,&H,&R);
		int i,j,sta=n+1,gol=n+2;
		for (i=1; i<=n+2; i++) fa[i]=i;
		for (i=1; i<=n; i++){
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			if (abs(c[i])<=R) mrg(sta,i);
			if (abs(c[i]-H)<=R) mrg(gol,i);
		}
		for (i=1; i<n; i++)
			for (j=i+1; j<=n; j++) if (check(i,j)) mrg(i,j);
		puts(getfa(sta)==getfa(gol)?"Yes":"No");
	}
	return 0;
}


#include<bits/stdc++.h>
#define N 13
using namespace std;

int n,m,ans,mp[N][N],dep[N]; bool vis[N]; 
void dfs(int k,int sum){
	if (sum>=ans) return;
	if (k>n){ ans=sum; return; }
	int i,j;
	for (i=1; i<=n; i++) if (!vis[i]){
		vis[i]=1;
		for (j=1; j<=n; j++) if (vis[j] && i!=j && mp[i][j]<1000000000){
			dep[i]=dep[j]+1;
			dfs(k+1,sum+mp[i][j]*dep[i]);
		}
		vis[i]=0;
	}
}
int main(){
	freopen("treasure.in","r",stdin); freopen("treasure2.out","w",stdout);
	scanf("%d%d",&n,&m);
	int i,x,y,z; memset(mp,60,sizeof(mp));
	while (m--){
		scanf("%d%d%d",&x,&y,&z);
		mp[x][y]=mp[y][x]=min(mp[x][y],z);
	}
	memset(vis,0,sizeof(vis));
	ans=1000000000;
	for (i=1; i<=n; i++){
		vis[i]=1; dep[i]=0;
		dfs(2,0);
		vis[i]=0;
	}
	printf("%d\n",ans);
	return 0;
}


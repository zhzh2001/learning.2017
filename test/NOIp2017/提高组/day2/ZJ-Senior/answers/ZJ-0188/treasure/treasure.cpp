#include<bits/stdc++.h>
#define inf 1000000000
#define N 15
#define M 5009
using namespace std;

int n,m,tot,mp[N][N],pw3[N],num[M],f[M][N],g[1000009],dp[N][M];
int main(){
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(mp,60,sizeof(mp));
	int i,j,k,x,y,z;
	for (i=1; i<=m; i++){
		scanf("%d%d%d",&x,&y,&z);
		mp[x][y]=mp[y][x]=min(mp[x][y],z);
	}
	if (n==1){ puts("0"); return 0; }
	tot=1<<n;
	for (i=pw3[0]=1; i<=n; i++) pw3[i]=pw3[i-1]*3;
	for (i=0; i<tot; i++)
		for (j=1; j<=n; j++) if (i>>j-1&1) num[i]+=pw3[j-1];
	memset(f,60,sizeof(f));
	for (i=0; i<tot; i++)
		for (j=1; j<=n; j++) if (!(i>>j-1&1))
			for (k=1; k<=n; k++) if (i>>k-1&1) f[i][j]=min(f[i][j],mp[k][j]);
	for (i=1; i<tot; i++)
		for (j=i; j; j=(j-1&i)){
			x=i^j;
			for (k=1,y=0; k<=n; k++) if (j>>k-1&1){
				y+=f[x][k]; if (y>inf) y=inf;
			}
			g[num[i]+num[j]]=y;
		}
	memset(dp,60,sizeof(dp));
	for (i=1; i<=n; i++) dp[0][1<<i-1]=0;
	for (i=1; i<n; i++)
		for (j=1; j<tot; j++)
			for (k=j; k; k=(k-1&j)) dp[i][j]=min(dp[i][j],dp[i-1][j^k]+min(g[num[j]+num[k]],inf/i+1)*i);
	int ans=inf;
	for (i=0; i<n; i++) ans=min(ans,dp[i][tot-1]);
	printf("%d\n",ans);
	return 0;
}


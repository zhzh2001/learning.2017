#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<" "<<__LINE__<<endl


const int MAXN=20;
const int MAXM=1005;
const int INF=1<<29;

struct edge {
	int x,y,v;
};
struct node {
	int t,v;
	node(){}
	node(int _t,int _v):t(_t),v(_v) {}
};

void Min(int &x,int y) {
	if (x>y) x=y;
}

int n,m;
edge edg[MAXM];

struct Part1 {
	/*
		dfs
	*/
	
	int midis[MAXN][MAXN];
	vector <node> g[MAXN];
	int dep[MAXN];//-1 ��ʾδ���� 
	int res;
	
	void dfs(int ans) {
		if (ans>=res) return;
		bool end=true;
		rep (i,1,n) if (dep[i]==-1) end=false;
		if (end) {
			res=ans;
			return;
		}
		rep (x,1,n) if (dep[x]!=-1) {
			repo (i,0,g[x].size()) if (dep[g[x][i].t]==-1) {
				int t=g[x][i].t;
				dep[t]=dep[x]+1;
				dfs(ans+g[x][i].v*dep[t]);
				dep[t]=-1;
			}
		}
	}
	
	void main() {
		rep (i,1,n) rep (j,1,n) midis[i][j]=INF;
		rep (i,1,m) {
			int x=edg[i].x,y=edg[i].y;
			int v=edg[i].v;
			Min(midis[x][y],v);
			Min(midis[y][x],v);
		}
		
		rep (i,1,n) {
			int k=rand()&1;
			if (k) {
				rep (j,1,n) if (midis[i][j]!=INF) {
					g[i].push_back(node(j,midis[i][j]));
				}
			}
			else {
				per (j,1,n) if (midis[i][j]!=INF) {
					g[i].push_back(node(j,midis[i][j]));
				}
			}
		}
		
		memset(dep,-1,sizeof(dep));
		res=INF;
		rep (i,1,n) {
			dep[i]=0;
			dfs(0);
			dep[i]=-1;
		}
		printf("%d\n",res);
	}
} part1;

void Read() {
	scanf("%d%d",&n,&m);
	rep (i,1,m) scanf("%d%d%d",&edg[i].x,&edg[i].y,&edg[i].v);
}
void Solve() {
	part1.main();
}

int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	Read();
	Solve();
	
//	{
//		cerr<<"\n\n-----------------"<<endl;
//		int sum=0;
//		int std=256;
//		sum>>=20;
//		cerr<<sum<<" MB"<<endl;
//		cerr<<std<<" -std"<<endl;
//		assert(sum<std);
//	}
	
	return 0;
}

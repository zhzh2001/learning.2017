#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<" "<<__LINE__<<endl

int rnd(int l,int r) {
	int x=(rand()*rand()+rand())%(r-l+1);
	return x+l;
}

int main() {
	srand(time(NULL));
	int n=rnd(8,8);
	int m=rnd(900,1000);
	cout<<n<<" "<<m<<endl;
	rep (i,1,m) {
		cout<<rnd(1,n)<<" "<<rnd(1,n)<<" "<<rnd(1,1)<<endl;
	}
	return 0;
}

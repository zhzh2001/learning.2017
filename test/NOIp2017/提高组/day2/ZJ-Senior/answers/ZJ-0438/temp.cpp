#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<" "<<__LINE__<<endl


void Read() {
	
}
void Solve() {
	
}

int main() {
	freopen(".in","r",stdin);
	freopen(".out","w",stdout);
	
	Read();
	Solve();
	
	{
		cerr<<"\n\n-----------------"<<endl;
		int sum=0;
		int std=256;
		sum>>=20;
		cerr<<sum<<" MB"<<endl;
		cerr<<std<<" -std"<<endl;
		assert(sum<std);
	}
	
	return 0;
}

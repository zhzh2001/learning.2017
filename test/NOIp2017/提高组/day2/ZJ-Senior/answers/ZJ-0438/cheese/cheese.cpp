#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<" "<<__LINE__<<endl


const int MAXN=1005;

int T;
int n,h,r;

struct Main {
	void clear() {
		res=false;
		memset(mark,0,sizeof(mark));
	}

	int x[MAXN],y[MAXN],z[MAXN];
	bool res;
	bool mark[MAXN];

	long long sqr(int x) {
		return 1ll*x*x;
	}
	bool chk(int a,int b) {
		long long d1=sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b]);
		long long d2=sqr(r+r);
		return d1<=d2;
	}

	void dfs(int p) {
		if (mark[p]) return;
		mark[p]=true;
		if (z[p]+r>=h) {
			res=true;
		}
		if (res) return;
		rep (i,1,n) if (!mark[i]) {
			if (chk(p,i)) dfs(i);
		}
	}
	
	void main() {
		clear();
		rep (i,1,n) scanf("%d%d%d",&x[i],&y[i],&z[i]);
		rep (i,1,n) {
			if (z[i]<=r) dfs(i);
		}
		if (res) puts("Yes");
		else puts("No");
	}
} pmain;

void Read() {
	scanf("%d",&T);
}
void Solve() {
	rep (i,1,T) {
		scanf("%d%d%d",&n,&h,&r);
		pmain.main();
	}
}

int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	Read();
	Solve();
	
//	{
//		cerr<<"\n\n-----------------"<<endl;
//		int sum=sizeof(Main);
//		int std=256;
//		sum>>=20;
//		cerr<<sum<<" MB"<<endl;
//		cerr<<std<<" -std"<<endl;
//		assert(sum<std);
//	}
	
	return 0;
}

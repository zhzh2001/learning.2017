#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>

using namespace std;

#define rep(i,s,t) for(int i=s,i##end=t;i<=i##end;++i)
#define per(i,s,t) for(int i=t,i##end=s;i>=i##end;--i)
#define repo(i,s,t) for(int i=s,i##end=t;i<i##end;++i)
#define debug(x) "> "<<#x<<" : "<<x<<" ;"
#define DEBUG(x) cerr<<debug(x)<<" "<<__FUNCTION__<<" "<<__LINE__<<endl

template <int MAXN> 
struct BIT {
	int val[MAXN];
	void update(int x,int d) {
		while (x<MAXN) {
			val[x]+=d;
			x+=x&-x;
		}
	}
	void update(int l,int r,int d) {
		update(r,d);
		if (l-1) update(l-1,-d);
	}
	int query(int x) {
		int res=0;
		while (x) {
			res+=val[x];
			x-=x&-x;
		}
		return res;
	}
} ;


const int MAXN=3*1e5+5;



int n,m,q;
int px[MAXN],py[MAXN];

struct Part1 {
	/*
		暴力模拟， 用于对拍 
	*/
	static const int MAXN=1005;
	int A[MAXN][MAXN];
	void main() {
		rep (i,1,n) rep (j,1,m) {
			A[i][j]=(i-1)*m+j;
		}
		rep (i,1,q) {
			int x=px[i],y=py[i];
			printf("%d\n",A[x][y]);
			int tmp=A[x][y];
			while (y!=m) {
				A[x][y]=A[x][y+1];
				y++;
			}
			while (x!=n) {
				A[x][y]=A[x+1][y];
				x++;
			}
			A[x][y]=tmp;
		}
	}
} part1; 

struct Part2 {
	static const int MAXQ=505;
	static const int MAXN=5*1e4+5;
	
	int ID[MAXQ];
	int A[MAXQ][MAXN];
	int row[MAXN];
	
	void main() {
		rep (i,1,q) ID[i]=px[i];
		
		int tot=q;
		sort(ID+1,ID+1+tot);
		tot=unique(ID+1,ID+1+tot)-ID-1;
		
		rep (i,1,tot) {
			rep (j,1,m-1) A[i][j]=(ID[i]-1)*m+j;
		}
		rep (i,1,n) row[i]=i*m;
		
		rep (i,1,q) {
			int x=px[i],y=py[i];
			int l=lower_bound(ID+1,ID+1+tot,x)-ID;
			
			int res;
			if (y==m) {
				res=row[x];
			}
			else res=A[l][y];
			printf("%d\n",res);
			
			int k=y;
			while (k<m-1) {
				A[l][k]=A[l][k+1];
				k++;
			}
			A[l][k]=row[x];
			
			k=x;
			while (k!=n) {
				row[k]=row[k+1];
				k++;
			}
			row[n]=res;
		}
	}
} part2;



void Read() {
	scanf("%d%d%d",&n,&m,&q);
	rep (i,1,q) scanf("%d%d",&px[i],&py[i]);
}
void Solve() {
//	bool ALL1=true;
//	rep (i,1,q) if (px[i]!=1) ALL1=false;
	
	if (n<=1000&&m<=1000&&q<=500) {
		part1.main();
	}
	else {
		part2.main();
	}
//	else if (ALL1) part3.main();
}

int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	Read();
	Solve();
	
//	{
//		cerr<<"\n\n-----------------"<<endl;
//		int sum=sizeof(Part1)+sizeof(Part2);
//		int std=512;
//		sum>>=20;
//		cerr<<sum<<" MB"<<endl;
//		cerr<<std<<" -std"<<endl;
//		assert(sum<std);
//	}
	
	return 0;
}

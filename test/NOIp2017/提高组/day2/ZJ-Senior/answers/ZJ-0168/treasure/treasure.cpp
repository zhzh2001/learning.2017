#include<cstdio>
#include<algorithm>
#include<cstring>
const int MAXN=1000001;
using namespace std;
struct bian {
	int fr,to,s;
} edge[MAXN<<2];
int head[MAXN],tot,n,m,fa[MAXN],ans=0,cnt,dep[MAXN],fat[MAXN],ass=1000000000;
bool cmp(bian a,bian b) {
	return a.s<b.s;
}
struct fucknoipday1 {
	int next,to,s;
} edge1[MAXN<<2];
int add(int a,int b,int c) {
	edge1[++tot].next=head[a];
	edge1[tot].to=b;
	edge1[tot].s=c;
	head[a]=tot;
}
int ins(int a,int b,int c) {
	add(a,b,c);
	add(b,a,c);
}
int find(int x) {
	return fa[x]==x?x:(fa[x]=find(fa[x]));
}
int mst() {
	for (int i=1; i<=n; i++) {
		fa[i]=i;
	}
	sort(edge+1,edge+1+m,cmp);
	for (int i=1; i<=m; i++) {
		int x=find(edge[i].fr),y=find(edge[i].to);
		if (x!=y) {
			fa[y]=x;
			ins(edge[i].fr,edge[i].to,edge[i].s);

		}
	}
}
int doit(int u) {
	for (int i=head[u]; i; i=edge1[i].next) {
		int v=edge1[i].to;
		if (v!=fat[u]) {
			fat[v]=u;
			dep[v]=dep[u]+1;
			ans+=dep[v]*edge1[i].s;
			doit(v);
		}
	}
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++) {
		scanf("%d%d%d",&edge[i].fr,&edge[i].to,&edge[i].s);
	}
	mst();
	for (int i=1; i<=n; i++) {
		memset(dep,0,sizeof(dep));
		memset(fa,0,sizeof(fa));
		ans=0;
		fat[i]=0,dep[i]=0;
		doit(i);
		ass=min(ans,ass);
	}
	printf("%d",ass);
	return 0;
}

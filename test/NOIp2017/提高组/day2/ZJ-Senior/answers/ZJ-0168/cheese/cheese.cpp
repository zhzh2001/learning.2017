#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<cstring>
#define int long long
using namespace std;
const int MAXN=100001;
queue <int> dlie;
int head[MAXN],S,T,T1,r,h,dis[MAXN],in_que[MAXN],tot,x[MAXN],y[MAXN],z[MAXN],n;
struct bian {
	int to,next;
} edge[MAXN<<2];
int add(int a,int b) {
	edge[++tot].next=head[a];
	edge[tot].to=b;
	head[a]=tot;
}
int ins(int a,int b) {
	add(a,b);
	add(b,a);
}
int spfa() {
	memset(dis,0X3f,sizeof(dis));
	in_que[S]=1;
	dis[S]=0;
	dlie.push(S);
	int flsg=0;
	while (!dlie.empty()) {
		int u=dlie.front();
		dlie.pop();
		in_que[u]=0;
		for (int i=head[u]; i; i=edge[i].next) {
			int v=edge[i].to;
			if (dis[v]>1) {
				dis[v]=1;
				if (in_que[v]==0) {
					dlie.push(v);
					in_que[v]=1;
				}
			}
			if (v==T1) {
				flsg=1;
				break;
			}

		}
		if (flsg) break;
	}
	return (dis[T1]==1);
}

double diss(int a,int b) {
	return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]));
}
int dd() {
	while (!dlie.empty())dlie.pop();
}
signed main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (T--) {
		tot=0;
		memset(in_que,0,sizeof(in_que));
		memset(head,0,sizeof(head));
		dd();
		scanf("%lld%lld%lld",&n,&h,&r);
		S=0,T1=n+1;
		for (int i=1; i<=n; i++) {
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		}
		for (int i=1; i<=n; i++) {
			for (int j=i+1; j<=n; j++) {
				if (diss(i,j)<=(double)2*r)
					ins(i,j);
			}
			if (h-z[i]<=r)  ins(T1,i);
			if (z[i]-0<=r)  ins(S,i);
		}
		int k=spfa();
		if (k) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

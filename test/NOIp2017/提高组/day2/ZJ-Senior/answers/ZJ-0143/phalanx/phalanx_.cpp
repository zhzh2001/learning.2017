#include<cstdio>
using namespace std;
typedef long long ll;
const int N=300300;
int i,j,k,n,m,q,ch,ff,x,y;
struct cc { int x,y;} A[N];
void R(int &x) {
	ff=x=0;ch=getchar();
	while (ch<'0' || '9'<ch) { if (ff) x=-x;ch=getchar();}
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	if (ff) x=-x;
}
void W(ll x) {
	if (x<0) putchar('-'),x=-x;
	if (x>=10) W(x/10);
	putchar(x%10+'0');
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	R(n);R(m);R(q);
	for (i=1;i<=q;i++) {
		R(A[i].x);R(A[i].y);
		x=A[i].x;y=A[i].y;
		for (j=i-1;j;j--) {
			if (y<m) {
				if (A[j].x==x && A[j].y<=y) y++;
			}
			else {
				if (x<n) {
					if (A[j].x<=x) x++;
				}
				else {
					x=A[j].x;y=A[j].y;
				}
			}
		}
		W((ll) (x-1)*m+y);puts("");
	}
}

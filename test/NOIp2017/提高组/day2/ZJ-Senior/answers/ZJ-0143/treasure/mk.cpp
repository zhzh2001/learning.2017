#include<cstdio>
#include<ctime>
#include<cstdlib>
using namespace std;
const int N=12,M=1000,P=500000;
int main() {
	srand((int) time(0));
	freopen("treasure.in","w",stdout);
	printf("%d %d\n",N,M);
	for (int i=1;i<=M;i++) {
		int x=rand()%N+1,y=rand()%N+1;
		while (x==y) x=rand()%N+1,y=rand()%N+1;
		printf("%d %d %d\n",x,y,rand()%P+1);
	}
}

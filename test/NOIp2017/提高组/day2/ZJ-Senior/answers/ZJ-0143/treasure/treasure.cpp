#include<cstdio>
#include<cstring>
using namespace std;
const int N=15,M=1<<12,oo=1000000000;
int i,j,k,l,s,n,m,nn,ch,ff,x,y,v,ans;
int f[N][N][M];
int a[N][N];
void R(int &x) {
	ff=x=0;ch=getchar();
	while (ch<'0' || '9'<ch) ch=getchar();
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	if (ff) x=-x;
}
void Up(int &x,const int &y) {
	if (x==-1 || y<x) x=y;
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(a,-1,sizeof a);
	memset(f,-1,sizeof f);
	R(n);R(m);nn=(1<<n)-1;
	for (i=1;i<=m;i++) {
		R(x);R(y);R(v);
		if (a[x][y]==-1) a[x][y]=a[y][x]=v;
		else {
			if (v<a[x][y]) a[x][y]=a[y][x]=v;
		}
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			for (k=1;k<=n;k++) if (k!=i && a[i][k]!=-1) f[i][j][1<<(k-1)]=a[i][k]*j;
	for (k=0;k<nn;k++) {
		int tmp=nn^k;
		for (i=1;i<=n;i++) if (!((1<<(i-1))&k)) {
			for (j=1;j<=n;j++) {
				for (l=1;l<=n;l++) if (a[i][l]!=-1 && ((1<<(l-1))&k) && f[l][j+1][k^(1<<(l-1))]!=-1) Up(f[i][j][k],f[l][j+1][k^(1<<(l-1))]+a[i][l]*j);
				for (s=(tmp+1)&k;s<k;s=((s|tmp)+1)&k) if (f[i][j][s]!=-1 && f[i][j][k^s]!=-1) Up(f[i][j][k],f[i][j][s]+f[i][j][k^s]);
			}
		}
	}
	ans=oo;
	for (i=1;i<=n;i++) if (f[i][1][nn^(1<<(i-1))]!=-1) Up(ans,f[i][1][nn^(1<<(i-1))]);
	printf("%d\n",ans);
}

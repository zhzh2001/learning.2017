#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
const int N=1010;
int i,j,k,n,h,r,T,ch,ff;
ll rr;
int z[N],d[N];
struct cc { int x,y,z;} A[N];
void R(int &x) {
	ff=x=0;ch=getchar();
	while (ch<'0' || '9'<ch) { if (ch=='-') ff=1;ch=getchar();}
	while ('0'<=ch && ch<='9') x=x*10+ch-'0',ch=getchar();
	if (ff) x=-x;
}
bool check(cc a,cc b) {
	ll x=a.x-b.x;
	ll y=a.y-b.y;
	ll z=a.z-b.z;
	return x*x+y*y+z*z<=rr;
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	R(T);
	while (T--) {
		memset(z,0,sizeof z);
		R(n);R(h);R(r);
		rr=4ll*r*r;
		for (i=1;i<=n;i++) R(A[i].x),R(A[i].y),R(A[i].z);
		j=0;
		for (i=1;i<=n;i++) if (A[i].z<=r) z[i]=1,d[++j]=i;
		i=0;
		while (i<j) {
			i++;
			for (k=1;k<=n;k++) if (!z[k] && check(A[d[i]],A[k])) d[++j]=k,z[k]=1;
		}
		int Fg=0;
		for (i=1;i<=n;i++) if (z[i] && A[i].z+r>=h) Fg=1;
		if (Fg) puts("Yes");
		else puts("No");
	}
}

#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;
const int N=1005;
const double eps=1e-6;
struct node{
	double x,y,z;
}a[N];
struct rec{
	double dz,tz;
}p[N];
int t,n,f[N];
double h,r;
bool flag;
void init()
{
	flag=0;
	for (int i=1;i<=n;++i) f[i]=i;
}
int find_fa(int x){
	return f[x]==x?x:f[x]=find_fa(f[x]);
}
double get_dis(int u,int v){
	return sqrt((a[u].x-a[v].x)*(a[u].x-a[v].x)+(a[u].y-a[v].y)*(a[u].y-a[v].y)+(a[u].z-a[v].z)*(a[u].z-a[v].z));
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		init();
		scanf("%d%lf%lf",&n,&h,&r);
		for (int i=1;i<=n;++i)
		scanf("%lf%lf%lf",&a[i].x,&a[i].y,&a[i].z);
		for (int i=1;i<=n;++i)
		{
			if (fabs(a[i].z)-r<eps&&fabs(a[i].z-h)-r<eps)
			{
				printf("Yes\n");
				flag=1; break;
			}
		}
		if (flag) continue;
		for (int i=1;i<n;++i)
		for (int j=i+1;j<=n;++j)
		{
			if (get_dis(i,j)-2*r<eps)
			{
				int fx=find_fa(i);
				int fy=find_fa(j);
				if (fx!=fy) f[fx]=fy;
			}
		}
		for (int i=1;i<=n;++i)
		{
			p[i].dz=2e9;
			p[i].tz=-2e9;
		}
		for (int i=1;i<=n;++i)
		{
			int u=find_fa(f[i]);
			p[u].dz=min(p[u].dz,a[i].z-r);
			p[u].tz=max(p[u].tz,a[i].z+r);
			if (p[u].dz<eps&&p[u].tz-h>-eps)
			{
				printf("Yes\n");
				flag=1; break;
			}
		}
		if (!flag) printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

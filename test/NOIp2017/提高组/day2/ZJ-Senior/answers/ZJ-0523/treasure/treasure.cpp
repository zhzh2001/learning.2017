#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=15,M=1005;
struct node{
	int u,v,w;
}e[M];
int n,m,ans,cnt,tot,toi[N<<1],le[N<<1],nxt[N<<1],hd[N],f[N],dis[N];
bool cmp(node a,node b){
	return a.w<b.w;
}
int find_fa(int x){
	return f[x]==x?x:f[x]=find_fa(f[x]);
}
void init()
{
	cnt=tot=0; ans=1e9;
	memset(nxt,0,sizeof(nxt));
	memset(hd,0,sizeof(hd));
	for (int i=1;i<=n;++i) f[i]=i;
}
void add(int u,int v,int w){
	toi[++cnt]=v; le[cnt]=w; nxt[cnt]=hd[u]; hd[u]=cnt;
}
void dfs(int u,int fa,int dep)
{
	for (int i=hd[u];i;i=nxt[i])
	if (toi[i]!=fa)
	{
		dis[toi[i]]=(dep+1)*le[i];
		dfs(toi[i],u,dep+1);
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;++i) scanf("%d%d%d",&e[i].u,&e[i].v,&e[i].w);
	sort(e+1,e+m+1,cmp);
	init();
	for (int i=1;i<=m;++i)
	{
		int fx=find_fa(e[i].u),fy=find_fa(e[i].v);
		if (f[fx]!=fy)
		{
			f[fx]=fy; ++tot;
			add(e[i].u,e[i].v,e[i].w);
			add(e[i].v,e[i].u,e[i].w);
		}
		if (tot==n-1) break;
	}
	for (int i=1;i<=n;++i)
	{
		memset(dis,0,sizeof(dis));
		int res=0; dfs(i,0,0);
		for (int j=1;j<=n;++j) res+=dis[j];
		ans=min(ans,res);
	}
	printf("%d\n",ans);
	return 0;
	fclose(stdin);
	fclose(stdout);
}

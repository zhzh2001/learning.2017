#include<iostream>
#include<cstdio>
using namespace std;
const int N=1005,M=300005;
int n,m,q,x,y,t,cnt,a[N][N],tree[M],ans[M<<2];
void init()
{
	for (int i=1;i<=n;++i)
	for (int j=1;j<=m;++j)
	a[i][j]=(i-1)*m+j;
}
void solve_for_simple()
{
	init();
	while (q--)
	{
		scanf("%d%d",&x,&y);
		t=a[x][y];
		printf("%d\n",t);
		for (int i=y;i<m;++i) a[x][i]=a[x][i+1];
		for (int i=x;i<n;++i) a[i][m]=a[i+1][m];
		a[n][m]=t;
	}
}
int lowb(int x){
	return x&-x;
}
void add(int x,int y){
	for (int i=x;i<=n+m-1+q;i+=lowb(i)) tree[i]+=y;
}
int query(int x){
	int res=0;
	for (int i=x;i;i-=lowb(i)) res+=tree[i];
	return res;
}
void solve_for_special()
{
	for (int i=1;i<=n+m-1+q;++i) add(i,0);
	for (int i=1;i<=n;++i) ans[i]=i; cnt=n;
	while (q--)
	{
		scanf("%d%d",&x,&y);
		int l=1,r=n+m-1+q,p=0;
		while (l<=r)
		{
			int mid=(l+r)>>1;
			if (mid-query(mid)>=y) p=r,r=mid-1;
			else l=mid+1;
		}
		printf("%d\n",ans[p]);
		ans[++cnt]=ans[p];
		add(p,1);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000&&m<=1000&&q<=500) solve_for_simple();
	else solve_for_special();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<math.h>
#include<queue>	
const long long maxn=1000000007;
using namespace std;
queue <int> h;
int n;	
int map[13][13];
int dist[13];

void bfs(int x){
	h.pop();
	for(int o=1;o<=n;o++){
		if(map[x][o]<maxn)
		if(dist[x]+1<dist[o]) {
		dist[o]=dist[x]+1;	
		h.push(o);
	}
	}
}
int f(int w){
	for(int i=1;i<=13;i++) dist[i]=maxn;	
	int ans=0;
	dist[w]=0;
	h.push(w); 
	bfs(w);
	while(!h.empty()) bfs(h.front() );	
	for(int v=1;v<=n;v++) ans=ans+dist[v];
	return ans; 
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int t;
	int q,w,e;
	int m;
	for(int i=1;i<=13;i++) 
	for(int j=1;j<=13;j++) map[i][j]=maxn;
	scanf("%d%d/n",&n,&m);
	for(int i=1;i<=m;i++){		
		scanf("%d%d%d/n",&q,&w,&e);
		if(map[q][w]>e) {
			map[q][w]=e;
			map[w][q]=e;
		}
	}
	int tot=maxn;
	int y;
	for(int i=1;i<=n;i++) {
	y=f(i);
	if(y<tot) tot=y;
	}
	cout<<tot*e;
	return 0;
}

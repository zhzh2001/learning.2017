var
  ii,t,i,j,k,l,m,n,h:longint;
  x,y,z:array[0..1001]of longint;
  f:array[0..1001]of boolean;
  fl:boolean;
  d1,d2,r:int64;

procedure dfs(xx:longint);
var i,j:longint;
begin
  if fl then exit;
  if abs(h-z[xx])<=r then begin fl:=true; exit; end;
  f[xx]:=true;
  for i:=1 to n do
    if (not f[i])  then
    begin
      d2:=sqr(x[xx]-x[i])+sqr(y[xx]-y[i])+sqr(z[xx]-z[i]);
      if d2<=d1 then  dfs(i);
    end;
end;


begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);
  readln(t);
  for ii:=1 to t do
  begin
    readln(n,h,r);
    for i:=1 to n do
      readln(x[i],y[i],z[i]);
    fillchar(f,sizeof(f),false); fl:=false;
    d1:=4*r*r;
    for i:=1 to n do
      if abs(z[i])<=r then dfs(i);
    if fl then writeln('Yes') else writeln('No');
  end;
  close(input);
  close(output);
end.




var
  i,j,k,m,n,q:longint;
  x,xx,yy,y,l:int64;
  fr:array[0..501,0..1]of longint;
  ch:array[0..600001]of int64;

begin
  assign(input,'phalanx.in');
  assign(output,'phalanx.out');
  reset(input);
  rewrite(output);
  readln(n,m,q);
  if q<=5000 then
  begin
    for i:=1 to q do
    begin
      readln(x,y);
      fr[i,0]:=x;
      fr[i,1]:=y;
      for j:=i-1 downto 1 do
      begin
        xx:=x;  yy:=y;
        if (x=fr[j,0])and(y>=fr[j,1])and(y<m)  then  yy:=y+1;
        if (y=m)and(x<n)and(x>=fr[j,0]) then xx:=x+1;
        if (y=m)and(x=n) then begin xx:=fr[j,0]; yy:=fr[j,1]; end;
        x:=xx;
        y:=yy;
      end;
      writeln((x-1)*m+y);
    end;
  end
  else
  begin
    for i:=1 to m do
      ch[i]:=i;
    for i:=1 to n-1 do
      ch[i+m]:=ch[i+m-1]+m;
    for i:=1 to q do
    begin
      readln(x,y);
      l:=ch[y];
      for j:=y to n+m-2 do
        ch[j]:=ch[j+1];
      ch[n+m-1]:=l;
      writeln(l);
    end;
  end;
  close(input);
  close(output);
end.

var
  i,j,k,l,m,n,x,y,v:longint;
  w:array[0..13,0..13]of longint;
  f:array[0..13]of boolean;
  dist:array[0..13]of longint;
  sum,min:int64;

procedure dj(x:longint);
var i,j,k,p,mi:longint;
begin
  fillchar(dist,sizeof(dist),127);
  fillchar(f,sizeof(f),true);
  dist[x]:=0;
  for i:=1 to n-1 do
  begin
    mi:=maxlongint;
    for j:=1 to n do
      if (dist[j]<mi)and(f[j]) then
      begin
        p:=j;
        mi:=dist[j];
      end;
    f[p]:=false;
    for j:=1 to n do
      if (dist[j]>=dist[p]+1)and(w[p,j]<=500000) then dist[j]:=dist[p]+1;
  end;
end;





begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  fillchar(w,sizeof(w),127);
  fillchar(f,sizeof(f),true);
  min:=4611686018427387904;
  for i:=1 to m do
  begin
    readln(x,y,v);
    if w[x,y]>v then w[x,y]:=v;
    if w[y,x]>v then w[y,x]:=v;
  end;
  for x:=1 to n do
  begin
    dj(x);   sum:=0;
    for i:=1 to n do
      sum:=sum+((dist[i]*(dist[i]+1)) div 2)*v;
      if sum<min then min:=sum;

  end;
  writeln(min);
  close(input);
  close(output);
end.


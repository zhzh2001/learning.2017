#include<bits/stdc++.h>
using namespace std;
int vis[10000],dis[10000],a[100][100],n,m,disit[100];
int x,y,z,sum,sfc,xx;
queue<int> q;
int read(int &x)
{
	char ch;int t=1;x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int read(long long &x)
{
	char ch;int t=1;x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int bfs(int i,int x)
{
	memset(disit,127,sizeof(disit));
	while (!q.empty()) q.pop();
	q.push(i);
	disit[i]=1;
	while (!q.empty())
	{
		int v=q.front();
		q.pop();
		for (int j=1;j<=n;j++)
		if (a[v][j]&&vis[j])
		{
			if (disit[j]>disit[v]+1)
			disit[j]=disit[v]+1;
			if (j==sfc)
			return disit[j];
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	int cqh=100000000;
	for (int i=1;i<=m;i++)
	{
		read(x);read(y);read(z);
		if (a[x][y]!=0)
		{	
	    	a[x][y]=min(a[x][y],z);
	    	a[y][x]=a[x][y];
		}
		else a[x][y]=a[y][x]=z;
	}
	for (int i=1;i<=n;i++)
	{
		memset(dis,127,sizeof(dis));
		memset(vis,0,sizeof(vis));
		sum=0;
		for (int j=1;j<=n;j++)
		if (a[i][j]!=0) dis[j]=a[i][j];
		dis[i]=0;
		vis[i]=1;
		for (int j=1;j<n;j++)
		{
			int minn=100000000;
			for (int k=1;k<=n;k++)
			if (!vis[k]&&dis[k]<minn)
			{
				minn=dis[k];
				sfc=k;
			}
			dis[sfc]=0;vis[sfc]=1;		
			sum+=minn;
			xx=bfs(i,sfc);
			for (int k=1;k<=n;k++)
			{				
			    if (!vis[k]&&dis[k]>a[sfc][k]*xx&&a[sfc][k]!=0)
			    {
			        dis[k]=a[sfc][k]*xx;    
				}
			}
		}
		if (sum<cqh) cqh=sum;
	}
	printf("%d\n",cqh);
	return 0;
}

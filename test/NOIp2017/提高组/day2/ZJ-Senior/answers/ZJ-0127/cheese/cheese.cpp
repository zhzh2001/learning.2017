#include<bits/stdc++.h>
using namespace std;
struct rrsb
{
	long long a,b,c;
}a[10000];
double dis[2000][2000],dd[10000];
queue<int> q;
int sum,T,vis[10000],h,r,bo,n,tot;
long long sqr(long long x)
{
	return x*x;
}
int read(int &x)
{
	char ch;int t=1;x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int read(long long &x)
{
	char ch;int t=1;x=0;
	ch=getchar();
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') t=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x=x*t;
}
int main() 
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--)
	{
	    while (!q.empty()) 
		q.pop();
		sum=0;bo=0;tot=0;
		memset(dd,0,sizeof(dd));
		memset(dis,0,sizeof(dis));
		memset(vis,0,sizeof(vis));
		memset(a,0,sizeof(a));
		read(n);read(h);read(r);
		for (int i=1;i<=n;i++)
		{
			read(a[i].a);
			read(a[i].b);
			read(a[i].c);
		}
		for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
		{
			dis[i][j]=dis[j][i]=sqrt(sqr(a[i].a-a[j].a)+sqr(a[i].b-a[j].b)+sqr(a[i].c-a[j].c));	        
		}
		for (int i=1;i<=n;i++)
		{
			dd[i]=a[i].c;
			if (dd[i]>r) sum++;
			dis[0][i]=dis[i][0]=dd[i];
		}
		q.push(0);
		vis[0]=1;
		if (sum==n) 
		{
			printf("No\n");
			continue;
		}
		while (!q.empty())
		{
			int v=q.front();
			q.pop();
			vis[v]=1;
			if (a[v].c+r>=h) 
			{
				printf("Yes\n");
				bo=1;
				break;
			}
			for (int i=1;i<=n;i++)
			if (!vis[i]&&dis[v][i]<=r*2)
			q.push(i);
		}
		if (!bo) 
		printf("No\n");
	}
	return 0;
}

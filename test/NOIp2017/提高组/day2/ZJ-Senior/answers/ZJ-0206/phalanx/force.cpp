#include<cstdio>
#include<cstring>
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void Read(int& x){
	char c=nc();
	for(;c<'0'||c>'9';c=nc());
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
char ss[30];
int Len;
#define ll long long
inline void Print(ll x){
	for(Len=0;x;x/=10)ss[++Len]=x%10;
	while(Len)putchar(ss[Len--]+48);putchar('\n');
}
#define N 1010
#define M 5500010
int i,j,k,n,m,num,x,y,q;
ll a[N][N];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("f.out","w",stdout);
	Read(n);Read(m);Read(q);
	for(i=1;i<=n;i++)
	for(j=1;j<=m;j++)a[i][j]=1ll*(i-1)*m+j;
	while(q--){
		Read(x);Read(y);
		ll t=a[x][y];
		Print(a[x][y]);
		for(i=y;i<m;i++)a[x][i]=a[x][i+1];
		for(i=x;i<n;i++)a[i][m]=a[i+1][m];
		a[n][m]=t;
	}
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void Read(int& x){
	char c=nc();
	for(;c<'0'||c>'9';c=nc());
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
char ss[30];
int Len;
#define ll long long
inline void Print(ll x){
	for(Len=0;x;x/=10)ss[++Len]=x%10;
	while(Len)putchar(ss[Len--]+48);putchar('\n');
}
#define N 300010
#define M 5500010
vector<int>g[N],f,a[N];
int c[N];
int i,j,k,n,m,num,x,y,q;
bool b[N];
ll Ans,t;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Read(n);Read(m);Read(q);
//	if(q<=500){
		for(i=1;i<=n;i++)c[i]=1ll*i*m;
		while(q--){
			Read(x);Read(y);
			if(!b[x])for(i=b[x]=1;i<=m;i++)a[x].push_back(1ll*(x-1)*m+i);
			if(y==m)Print(c[x]);else Print(a[x][y-1]);
			if(x==n&&y==m)continue;
			t=a[x][y-1];
			for(j=y-1;j<m-1;j++)a[x][j]=(j==m-2?c[x]:a[x][j+1]);
			for(j=x;j<n;j++)c[j]=c[j+1];
			c[n]=t;
		}
//	}
return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void Read(int& x){
	char c=nc();
	for(;c<'0'||c>'9';c=nc());
	for(x=0;c>='0'&&c<='9';x=(x<<3)+(x<<1)+c-48,c=nc());
}
char ss[30];
int Len;
#define ll long long
inline void Print(ll x){
	for(Len=0;x;x/=10)ss[++Len]=x%10;
	while(Len)putchar(ss[Len--]+48);putchar('\n');
}
#define N 300010
#define M 5500010
vector<int>g[N],f;
int c[M],ls[M],rs[M],mx[M];
int i,j,k,n,m,num,x,y,q,Mx,Rt[N],rt;
bool b[N];
ll Ans,t;
inline int Get_max(int x,int l,int r,int y){
	if(!x)return 0;
	if(l==r)return mx[x];
	int Mid=l+r>>1;
	int t=max(y,Get_max(ls[x],l,Mid,y));
	if(t<=Mid)return t;
	return max(t,Get_max(rs[x],Mid+1,r,t));
}
inline int Get_sum(int x,int l,int r,int y){
	if(!x)return 0;
	if(l==r)return c[x];
	int Mid=l+r>>1;
	if(y<=Mid)return Get_sum(ls[x],l,Mid,y);else return c[ls[x]]+Get_sum(rs[x],Mid+1,r,max(y,mx[ls[x]]));
}
inline void Up(int x,int l,int r){
	int Mid=l+r>>1;
	c[x]=c[ls[x]]+c[rs[x]];
	if(mx[ls[x]]>=r)mx[x]=max(mx[ls[x]],mx[rs[x]]);else
	if(mx[ls[x]]<=Mid)mx[x]=mx[ls[x]];else
	mx[x]=max(mx[ls[x]],Get_max(rs[x],Mid+1,r,mx[ls[x]]));
}
inline void Update(int& x,int l,int r,int y){
	if(!x)x=++num;
	if(l==r){
		c[x]++;mx[x]=l+c[x];
		return;
	}
	int Mid=l+r>>1;
	if(y<=Mid)Update(ls[x],l,Mid,y);else Update(rs[x],Mid+1,r,y);
	Up(x,l,r);
}
inline int Query(int x,int l,int r,int L,int R){
	if(l>R||r<L||!x)return 0;
	if(l>=L&&r<=R)return c[x];
	int Mid=l+r>>1;
	return Query(ls[x],l,Mid,L,R)+Query(rs[x],Mid+1,r,L,R);
}
inline int Query2(int x,int l,int r,int L,int R){
	if(l>R||r<L||!x)return 0;
	if(l>=L&&r<=R){
		if(Mx<l)return 0;
		int t=Get_sum(x,l,r,Mx);
		Mx=max(Mx,Get_max(x,l,r,Mx));
		return t;
	}
	int Mid=l+r>>1;
	return Query2(ls[x],l,Mid,L,R)+Query2(rs[x],Mid+1,r,L,R);
}
inline ll Get(int x,int y){
	if(y<m){
		Mx=y;
		int t=Query(Rt[x],1,m,1,y-1)+Query2(Rt[x],1,m,y,m);
		if(y+t<=m)return 1ll*(x-1)*m+y+t;
		return g[x][y+t-m-1];
	}
	Mx=x;
	int t=Query(rt,1,n,1,x-1)+Query2(rt,1,n,x,n);
	if(x+t<=n)return 1ll*(x+t)*m;
	return f[x+t-n-1];
}
int main(){
	freopen("phalanx2.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Read(n);Read(m);Read(q);
	while(q--){
		Read(x);Read(y);
		Ans=Get(x,y);
		Print(Ans);
		if(x==n&&y==m)continue;
		if(x==n)t=Ans;else t=Get(x+1,m);
		g[x].push_back(t);f.push_back(Ans);
		Update(Rt[x],1,m,y);Update(rt,1,n,x);
	}
}

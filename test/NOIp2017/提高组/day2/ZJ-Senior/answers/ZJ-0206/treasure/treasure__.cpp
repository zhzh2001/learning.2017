#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<map>
#include<vector>
using namespace std;
#define N 15
#define INF 1e9
#define ll long long
#define It map<ll,int>::iterator
const ll Mo=2333333333333;
const int P=23333;
struct Node{
	int t,w;
	Node(int t=0,int w=0):t(t),w(w){}
	bool operator < (Node a)const{
		return w<a.w;
	}
};
vector<Node>g[N];
ll p[N];
int i,j,k,n,m,Ans=INF;
int d[N][N],x,y,z,q[N],l,r,D[N],cnt,last;
bool f;
map<ll,int>M;
inline ll Hash(int y){
	ll Res=0;bool f=1;
	for(int i=1;i<=n;i++){
		Res=(Res+D[i]*p[i])%Mo;
		if(!D[i])f=0;
	}
	if(f)Ans=min(Ans,y);
	return Res;
}
inline void Dfs(int y){
	ll x=Hash(y);
	It t=M.find(x);
	if(t!=M.end()&&y>=(*t).second)return;
	M[x]=y;
	for(int i=1;i<=n;i++)
	if(!D[i])
	for(int j=0;j<g[i].size();j++){
		int v=g[i][j].t;
		if(!D[v])continue;
		D[i]=D[v]+1;
		Dfs(y+D[v]*g[i][j].w);
		D[i]=0;
	}
}
inline void Solve(int x){
	memset(D,0,sizeof(D));
	q[r=1]=x;l=0;D[x]=1;
	while(++l<=r){
		x=q[l];
		for(int i=1;i<=n;i++)
		if(d[i][x]<INF&&!D[i])
		D[i]=D[x]+1,q[++r]=i;
	}
	int Res=0;
	for(int i=1;i<=n;i++)Res+=D[i]-1;
	Ans=min(Ans,Res);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++)
	for(j=1;j<=n;j++)d[i][j]=INF;
	for(i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if(i>1&&z!=last)f=1;
		if(z<d[x][y])d[x][y]=d[y][x]=z;
		last=z;
	}
	for(i=1;i<=n;i++){
		for(j=1;j<=n;j++)
		if(d[i][j]<INF)g[i].push_back(Node(j,d[i][j]));
		sort(g[i].begin(),g[i].end());
	}
	if(!f){
		for(i=1;i<=n;i++)Solve(i);
		cout<<Ans*last<<endl;
		return 0;
	}
	m<<=1;
	for(p[0]=i=1;i<=n;i++)p[i]=p[i-1]*P%Mo;
	for(i=1;i<=n;i++){
		D[i]=1;
		Dfs(0);
		D[i]=0;
	}
	cout<<Ans<<endl;
	return 0;
}

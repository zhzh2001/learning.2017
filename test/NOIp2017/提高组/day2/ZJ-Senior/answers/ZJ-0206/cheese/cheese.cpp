#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define N 1010
#define M 2000010
int i,j,k,n,m,p,s,t,H,r;
int a[N],b[N],c[N],h[N],TT;
bool v[N];
int nx[M],T[M],num;
inline bool Check(int x,int y){
	long long d=1ll*(a[x]-a[y])*(a[x]-a[y])+1ll*(b[x]-b[y])*(b[x]-b[y])+1ll*(c[x]-c[y])*(c[x]-c[y]);
	return d<=4ll*r*r;
}
inline void Dfs(int x){
	if(v[x])return;
	v[x]=1;
	for(int i=h[x];i;i=nx[i])Dfs(T[i]);
}
inline void Add(int x,int y){
	T[++num]=y;nx[num]=h[x];h[x]=num;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&TT);
	while(TT--){
		memset(h,0,sizeof(h));
		scanf("%d%d%d",&n,&H,&r);
		s=0;t=n+1;
		for(i=1;i<=n;i++){
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			if(c[i]<=r)Add(s,i);
			if(c[i]+r>=H)Add(i,t);
		}
		for(i=1;i<n;i++)
		for(j=i+1;j<=n;j++)
		if(Check(i,j))Add(i,j),Add(j,i);
		memset(v,0,sizeof(v));
		Dfs(s);
		if(v[t])printf("Yes\n");else printf("No\n");
	}
	return 0;
}

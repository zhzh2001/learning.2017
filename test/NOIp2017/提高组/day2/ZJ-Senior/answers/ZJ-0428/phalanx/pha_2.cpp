#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<set>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
}
inline ll rd() {
	char c = getchar(); ll x = 0, f = 1;
	while(c<'0'||c>'9') {
		if(c=='-') f = -1;
		c = getchar();
	}
	while(c>='0'&&c<='9') x = x*10+c-'0', c = getchar();
	return x*f;
}
int n, m, q;
int a[1010][1010];
ll b[1010000];
int cnt, pl, pr, v;
ll sum[5010000], lz[5010000];
set<ll> S;
inline void lzy(int k, int l, int r) {
	if(lz[k]) {
		int mid = (l+r)>>1;
		lz[k<<1] += lz[k];
		lz[k<<1|1] += lz[k];
		sum[k<<1] += 1ll*lz[k]*(mid-l+1);
		sum[k<<1|1] += 1ll*lz[k]*(r-mid);
		lz[k] = 0;
	}
}
inline void upd(int k, int l, int r) {
	if(l>=pl&&r<=pr) {
		sum[k]+=1ll*v*(r-l+1), lz[k]+=v;
		return;
	}
	lzy(k,l,r);
	int mid = (l+r)>>1;
	if(pl<=mid) upd(k<<1,l,mid);
	if(pr>mid) upd(k<<1|1,mid+1,r);
	sum[k] = sum[k<<1]+sum[k<<1|1];
}
inline ll getsum(int k, int l, int r) {
	if(l>=pl&&r<=pr) return sum[k];
	lzy(k,l,r);
	int mid = (l+r)>>1; ll ans = 0;
	if(pl<=mid) ans+=getsum(k<<1,l,mid);
	if(pr>mid) ans+=getsum(k<<1|1,mid+1,r);
	sum[k] = sum[k<<1]+sum[k<<1|1];
	return ans;
}
int main() {
	ju();
	n = rd(), m = rd(), q = rd();
	if(n<=1000&&m<=1000) {
		for(int i = 1; i <= n; ++i)
			for(int j = 1; j <= m; ++j)
				a[i][j] = (i-1)*m+j;
		while(q--) {
			int x = rd(), y = rd();
			printf("%d\n", a[x][y]); int t = a[x][y];
			for(int i = y; i <= m-1; ++i) a[x][i] = a[x][i+1];
			for(int i = x; i <= n-1; ++i) a[i][m] = a[i+1][m];
			a[n][m] = t;
		}
		return 0;
	}
	if(n!=1&&q<=500) {
		cnt = n; for(int i = 1; i <= n; ++i) b[i] = 1ll*i*m;
		while(q--) {
			int x = rd(), y = rd();
			while(y<m&&S.count(1ll*(x-1)*m+y)) ++y;
			if(y<m) {
				S.insert(1ll*(x-1)*m+y); printf("%lld\n", 1ll*(x-1)*m+y);
				b[++cnt] = 1ll*(x-1)*m+y;
				pl=x,pr=x,v=1; upd(1,1,n);
			}
			else {
				pl=1,pr=x; int t = x+getsum(1,1,n);
				printf("%lld\n", b[t]); b[++cnt] = b[t];
				pl=x,pr=x,v=1; upd(1,1,n);
				pl=x+1,pr=n,v=-1; upd(1,1,n);
			}
		}
		return 0;
	}
	if(n==1) {
		cnt = m; for(int i = 1; i <= m; ++i) b[i] = i;
	}
	else {
		cnt = n+m-1;
		for(int i = 1; i <= m; ++i) b[i] = i;
		for(int i = m+1; i <= cnt; ++i) b[i] = 1ll*(i-m+1)*m;
	}
	//
	while(q--) {
		int x = rd(), y = rd();
		pl=1,pr=y; int t = y+getsum(1,1,m);
		printf("%lld\n", b[t]); b[++cnt] = b[t];
		pl=y,pr=y,v=1; upd(1,1,m);
		pl=y+1,pr=m,v=-1; upd(1,1,m);
	}
	return 0;
}

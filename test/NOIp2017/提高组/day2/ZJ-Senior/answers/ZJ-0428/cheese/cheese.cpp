#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
}
inline ll rd() {
	char c = getchar(); ll x = 0, f = 1;
	while(c<'0'||c>'9') {
		if(c=='-') f = -1;
		c = getchar();
	}
	while(c>='0'&&c<='9') x = x*10+c-'0', c = getchar();
	return x*f;
}
const int N = 1010;
int T, n, h, r;
bool g[N][N], vis[N], gg;
int q[N];
struct bl{
	int x, y, z;
} a[N];
inline ll sqr(int x) {return 1ll*x*x;}
int main() {
	ju();
	T = rd();
	while(T--) {
		gg = 0;
		memset(g, 0, sizeof g);
		memset(vis, 0, sizeof vis); vis[0] = 1;
		n = rd(), h = rd(), r = rd();
		for(int i = 1; i <= n; ++i) a[i] = (bl){rd(),rd(),rd()};
		//for(int i = 1; i <= n; ++i) printf("%d %d %d\n", a[i].x, a[i].y, a[i].z);
		for(int i = 1; i <= n; ++i) {
			if(a[i].z<=r) g[0][i] = 1;
			if(a[i].z>=h-r) g[i][n+1] = 1;
		}
		for(int i = 1; i <= n; ++i)
			for(int j = i+1; j <= n; ++j)
				if(sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z))<=(r<<1))
					 g[i][j] = g[j][i] = 1;
		int l = 0, r = 1; q[1] = 0;
		while(l<r) {
			int u = q[++l];
			for(int i = 1; i <= n+1; ++i)
				if(g[u][i]&&!vis[i]) {
					if(i==n+1) {gg = 1; break;}
					vis[i] = 1; q[++r] = i;
				}
			if(gg) break;
		}
		if(gg) puts("Yes"); else puts("No");
	}
	return 0;
}

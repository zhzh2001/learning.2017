#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long ll;
inline void ju() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
}
inline ll rd() {
	char c = getchar(); ll x = 0, f = 1;
	while(c<'0'||c>'9') {
		if(c=='-') f = -1;
		c = getchar();
	}
	while(c>='0'&&c<='9') x = x*10+c-'0', c = getchar();
	return x*f;
}
const int N = 15, M = 1010, INF = 0x3f3f3f3f;
int n, m;
int g[N][N], a[N], dep[N], sum=0, ans=INF;
bool b[N];
inline void dfs(int t) {
	if(sum>=ans) return;
	if(t>n) {
		if(sum<ans) ans = sum; return;
	}
	for(int i = 1; i <= n; ++i)
		if(!b[i]) {
			b[i] = 1;
			for(int j = 1; j <= n; ++j)
				if(b[j]&&g[i][j]) {
					dep[i] = dep[j]+1, sum += dep[j]*g[i][j];
					dfs(t+1);
					sum -= dep[j]*g[i][j];
				}
			b[i] = 0;
		}
}
int main() {
	ju();
	n = rd(), m = rd();
	for(int i = 1; i <= m; ++i) {
		int x = rd(), y = rd(), z = rd();
		if(x!=y) {
			if(!g[x][y]||z<g[x][y]) g[x][y] = z;
			g[y][x] = g[x][y];
		}
	}
	for(int i = 1; i <= n; ++i) {
		memset(b, 0, sizeof b);
		a[1] = i; dep[i] = 1; b[i] = 1; sum = 0; dfs(2);
	}
	printf("%d\n", ans);
	return 0;
}

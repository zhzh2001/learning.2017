#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),c<'0');
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>='0');
}
const int N=13;
int n,m,G[N][N],dp[1<<N][N][N],Mi[1<<N][N][N];
void Check(int &a,int b){
	if(a==-1||b<a)a=b;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	Rd(n),Rd(m);
	memset(G,-1,sizeof(G));
	while(m--){
		int a,b,c;
		Rd(a),Rd(b),Rd(c);
		a--,b--;
		Check(G[a][b],c);
		Check(G[b][a],c);
	}
	memset(dp,-1,sizeof(dp));
	memset(Mi,-1,sizeof(Mi));
	for(int i=0;i<n;i++)
		for(int j=1;j<=n;j++)
			dp[1<<i][i][j]=0;
	for(int S=1;S<(1<<n);S++){
		for(int i=0;i<n;i++){
			for(int j=n;j>=1;j--){
				for(int s=(S-1)&S;;s=(s-1)&S){
					if(~Mi[S^s][j+1][i]&&~dp[s][i][j])Check(dp[S][i][j],dp[s][i][j]+Mi[S^s][j+1][i]);
					if(!s)break;
				}
			}
		}
		for(int i=1;i<=n;i++)
			for(int j=0;j<n;j++)
				for(int k=0;k<n;k++)
					if(~dp[S][j][i]&&~G[j][k])Check(Mi[S][i][k],dp[S][j][i]+(i-1)*G[j][k]);
	}
	int rs=-1;
	for(int i=0;i<n;i++)
		if(~dp[(1<<n)-1][i][1])Check(rs,dp[(1<<n)-1][i][1]);
	printf("%d\n",rs);
	return 0;
}

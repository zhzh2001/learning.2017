#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<vector>
#define ll long long
using namespace std;
const int M=1005;
struct Node{
	int x,y,z;
}A[M];
bool mark[M];
int n,h,r,Head[M],tot;
struct node{int to,nxt;}Edge[2*(M*M+2*M)];
inline void Addedge(int a,int b){
	Edge[tot]=(node){b,Head[a]};Head[a]=tot++;
	Edge[tot]=(node){a,Head[b]};Head[b]=tot++;
}
void dfs(int x){
	for(int i=Head[x];~i;i=Edge[i].nxt){
		int to=Edge[i].to;
		if(mark[to])continue;
		mark[to]=true;
		dfs(to);
	}
}
ll Sqr(int x){
	return (ll)x*x;
}
ll Dist(int x,int y){
	return Sqr(A[x].x-A[y].x)+Sqr(A[x].y-A[y].y)+Sqr(A[x].z-A[y].z);
}
void solve(){
	scanf("%d %d %d",&n,&h,&r);
	for(int i=0;i<=n+1;i++)Head[i]=-1,mark[i]=false;tot=0;
	for(int i=1;i<=n;i++)
		scanf("%d %d %d",&A[i].x,&A[i].y,&A[i].z);
	for(int i=1;i<=n;i++){
		if(abs(A[i].z)<=r)Addedge(0,i);
		if(abs(h-A[i].z)<=r)Addedge(n+1,i);
		for(int j=1;j<=n;j++){
			if(i==j)continue;
			if(Sqr(2*r)>=Dist(i,j))Addedge(i,j);
		}
	}
	mark[0]=true;
	dfs(0);
	puts(mark[n+1]?"Yes":"No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--)solve();
	return 0;
}

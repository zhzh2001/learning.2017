#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
inline void Rd(int &res){
	char c;res=0;
	while(c=getchar(),c<'0');
	do{
		res=(res<<1)+(res<<3)+(c^48);
	}while(c=getchar(),c>='0');
}
void Pn(ll x){
	if(!x)return;
	Pn(x/10);
	putchar(x%10^48);
}
void Pf(ll x){
	Pn(x);
	putchar('\n');
}
const int S1N=1005;
const int M=(int)3e5+5;
int n,m,q;
struct Subtask1{
	int A[S1N][S1N];
	void solve(){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(i-1)*m+j;
		while(q--){
			int x,y;
			Rd(x),Rd(y);
			int tmp=A[x][y];
			for(int i=y;i<m;i++)
				A[x][i]=A[x][i+1];
			for(int i=x;i<n;i++)
				A[i][m]=A[i+1][m];
			printf("%d\n",A[n][m]=tmp);
		}
	}
}P30;
struct SegmentTree{
	#define lson (p<<1)
	#define rson (p<<1|1)
	int Tree[M<<3];
	void Up(int p){
		Tree[p]=Tree[lson]+Tree[rson];
	}
	void Build(int L,int R,int k,int p){
		if(L==R){
			Tree[p]=(L<=k);
			return;
		}
		int mid=(L+R)>>1;
		Build(L,mid,k,lson);
		Build(mid+1,R,k,rson);
		Up(p);
	}
	void Update(int L,int R,int x,int p){
		if(L==R){
			Tree[p]^=1;
			return;
		}
		int mid=(L+R)>>1;
		if(x<=mid)Update(L,mid,x,lson);
		else Update(mid+1,R,x,rson);
		Up(p);
	}
	int Query(int L,int R,int k,int p){
		if(L==R)return L;
		int mid=(L+R)>>1;
		if(Tree[lson]>=k)return Query(L,mid,k,lson);
		else return Query(mid+1,R,k-Tree[lson],rson);
	}
};
struct Subtask2{
	int A[M<<1],sz;
	SegmentTree T;
	void solve(){
		for(int i=1;i<=m;i++)A[i]=++sz;
		T.Build(1,m+q,m,1);
		for(int i=1;i<=q;i++){
			int x,y;Rd(x),Rd(y);
			int pos=T.Query(1,m+q,y,1);
			printf("%d\n",A[pos]);
			T.Update(1,m+q,pos,1);
			A[++sz]=A[pos];
			T.Update(1,m+q,sz,1);
		}
	}
}P50;
struct Node{
	int x,y;
}Q[M];
struct Subtask3{
	ll A[505][50005],Ed[50005];
	int X[505],sz,Pos[50005];
	void solve(){
		for(int i=1;i<=q;i++){
			Rd(Q[i].x),Rd(Q[i].y);
			X[++sz]=Q[i].x;
		}
		sort(X+1,X+sz+1);
		sz=unique(X+1,X+sz+1)-X-1;
		for(int i=1;i<=sz;i++)Pos[X[i]]=i;
		for(int i=1;i<=sz;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(ll)(X[i]-1)*m+j;
		for(int i=1;i<=n;i++)Ed[i]=(ll)i*m;
		for(int i=1;i<=q;i++){
			int x=Q[i].x,y=Q[i].y;
			if(y==m){
				ll tmp=Ed[x];
				for(int i=x;i<n;i++)
					Ed[i]=Ed[i+1];
				Pf(Ed[n]=tmp);
			}else{
				ll tmp=A[Pos[x]][y];
				for(int i=y;i<m-1;i++)
					A[Pos[x]][i]=A[Pos[x]][i+1];
				A[Pos[x]][m-1]=Ed[x];
				for(int i=x;i<n;i++)
					Ed[i]=Ed[i+1];
				Pf(Ed[n]=tmp);
			}
		}
	}
}P70;
struct Subtask4{
	SegmentTree T;
	ll A[M<<1],Que[M<<1];
	int sz;
	void solve(){
		for(int i=1;i<m;i++)A[++sz]=i;
		for(int i=1;i<=n;i++)Que[i]=(ll)i*m;
		int L=1,R=n;
		T.Build(1,m+q,m-1,1);
		for(int i=1;i<=q;i++){
			int x,y;
			Rd(x),Rd(y);
			if(y==m){
				ll tmp;
				Pf(tmp=Que[L++]);
				Que[++R]=tmp;
			}else{
				int pos=T.Query(1,m+q,y,1);
				Pf(A[pos]);
				T.Update(1,m+q,pos,1);
				A[++sz]=Que[L++];
				T.Update(1,m+q,sz,1);
				Que[++R]=A[pos];
			}
		}
	}
}P80;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Rd(n),Rd(m),Rd(q);
	if(n<=1000&&m<=1000&&q<=500)P30.solve();
	else if(n==1)P50.solve();
	else if(n<=50000&&m<=50000&&q<=500)P70.solve();
	else P80.solve();
	return 0;
}

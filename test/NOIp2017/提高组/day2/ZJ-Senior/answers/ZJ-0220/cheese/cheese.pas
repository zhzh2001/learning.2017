var t,cas,n,h,r,i,j,head,tail:longint;x1,x2,y1,y2,z1,z2:int64;dis:double;
    mat:array[0..1010,0..1010]of longint;flag:boolean;
    x,y,z,q,vis:array[0..100000]of longint;
begin
   assign(input,'cheese.in');reset(input);
   assign(output,'cheese.out');rewrite(output);
   readln(t);
   for cas:=1 to t do
   begin
      readln(n,h,r);
      for i:=0 to n+1 do
         for j:=0 to n+1 do mat[i][j]:=0;
      for i:=1 to n do readln(x[i],y[i],z[i]);
      for i:=1 to n do if z[i]<=r then mat[0][i]:=1;
      for i:=1 to n do if h-z[i]<=r then mat[i][n+1]:=1;
      for i:=1 to n do
         for j:=i+1 to n do
         begin
            x1:=x[i];x2:=x[j];y1:=y[i];y2:=y[j];z1:=z[i];z2:=z[j];
            dis:=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2);
            dis:=sqrt(dis);
            if dis<=r+r then begin mat[i][j]:=1;mat[j][i]:=1;end;
         end;
      for i:=1 to n+1 do vis[i]:=0;
      head:=0;tail:=1;q[1]:=0;vis[0]:=1;flag:=false;
      while head<tail do
      begin
         inc(head);
         for i:=1 to n+1 do
            if(mat[q[head]][i]=1)and(vis[i]=0)then
            begin
               inc(tail);q[tail]:=i;
               vis[i]:=1;
               if i=n+1 then begin flag:=true;break;end;
            end;
         if flag then break;
      end;
      if flag then writeln('Yes') else writeln('No');
   end;
   close(input);close(output);
end.
var oo,n,m,i,j,x,y,z,sta,cur,head,tail,start,mini,k,num:longint;ans:int64;
    d,q,st,deep:array[0..100000]of longint;flag:boolean;
    dis:array[0..12,0..12,0..5000]of longint;
    mat:array[0..15,0..15]of longint;
    f:array[0..5000]of int64;
    vis,a:array[0..12]of longint;
    tong:array[0..500000]of longint;
function min(a,b:int64):int64;
begin
   if a<b then exit(a);
   exit(b);
end;
procedure bfs(x,arr,sta:longint);
var i,u:longint;flag:boolean;
begin
   fillchar(vis,sizeof(vis),0);
   head:=0;tail:=1;q[1]:=x;d[1]:=1;num:=0;vis[x]:=1;flag:=false;
   for i:=1 to n do if(sta or(1<<(i-1))=sta)then begin inc(num);a[num]:=i;end;
   while head<tail do
   begin
      inc(head);
      u:=q[head];
      for i:=1 to num do
         if(mat[u][a[i]]<oo)and(vis[a[i]]=0)then
         begin
            inc(tail);q[tail]:=a[i];
            vis[a[i]]:=1;
            d[tail]:=d[head]+1;
            if a[i]=arr then
            begin
               flag:=true;
               dis[x][arr][sta]:=d[tail];
               break;
            end;
         end;
      if flag then break;
   end;
end;
procedure bfs1(x:longint);
var u,i:longint;res:int64;
begin
   fillchar(vis,sizeof(vis),0);
   head:=0;tail:=1;q[1]:=x;vis[x]:=1;deep[1]:=1;res:=0;
   while head<tail do
   begin
      inc(head);
      u:=q[head];
      for i:=1 to n do
         if(vis[i]=0)and(mat[u][i]<oo)then
         begin
            inc(tail);q[tail]:=i;
            vis[i]:=1;res:=res+mat[u][i]*deep[head];
            deep[tail]:=deep[head]+1;
         end;
   end;
   if res<ans then ans:=res;
end;
begin
   assign(input,'treasure.in');reset(input);
   assign(output,'treasure.out');rewrite(output);
   oo:=1000000000;
   readln(n,m);
   for i:=1 to n do
   begin
      for j:=1 to n do mat[i][j]:=oo;
      mat[i][i]:=0;
   end;
   for i:=1 to m do
   begin
      readln(x,y,z);
      if z<mat[x][y] then
      begin
         mat[x][y]:=z;
         mat[y][x]:=z;
      end;
      inc(tong[z]);
   end;

   for i:=1 to n do
      for j:=1 to n do
         for sta:=1 to 1<<n-1 do
         begin
            if(sta or (1<<(i-1))<>sta)or(sta or(1<<(j-1))<>sta)then continue;
            if i=j then dis[i][j][sta]:=1 else bfs(i,j,sta);
         end;

   {for i:=1 to n do
      for j:=1 to n do
         for sta:=1 to 1<<n-1 do if dis[i][j][sta]<oo then writeln(i,' ',j,' ',sta,' ',dis[i][j][sta]);}
   flag:=false;
   for i:=1 to 5000 do if tong[i]=m then flag:=true;
   if flag then
   begin
      ans:=oo;
      for start:=1 to n do
      begin
         bfs1(start);
      end;
      writeln(ans);
   end else
   begin
      ans:=oo;
      for start:=1 to n do
      begin
         for sta:=1 to 1<<n-1 do f[sta]:=oo;
         f[1<<(start-1)]:=0;
         for sta:=1<<(start-1) to 1<<n-1 do
            if f[sta]<>oo then
            begin
               for cur:=1 to n do
                  if(sta or (1<<(cur-1))<>sta)then
                  begin
                     mini:=oo;
                     for i:=1 to n do
                        if(sta or (1<<(i-1))=sta)and(mat[i][cur]<mini)then begin k:=i;mini:=mat[i][cur];end;
                     if mini<oo then f[sta or (1<<(cur-1))]:=min(f[sta or (1<<(cur-1))],f[sta]+mini*dis[start][k][sta]);
                  end;
            end;
         ans:=min(ans,f[1<<n-1]);
         //writeln(ans);
      end;
      writeln(ans);
   end;
   close(input);close(output);
end.

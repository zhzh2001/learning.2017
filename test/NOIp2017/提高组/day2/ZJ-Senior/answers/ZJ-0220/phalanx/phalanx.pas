var n,m,q,i,j,cas,x,y,t,cnt,s,x1,x2,delta:longint;
    a:array[0..2010,0..2010]of longint;
    b,belong,start,ending,addi:array[0..500000]of longint;
begin
   assign(input,'phalanx.in');reset(input);
   assign(output,'phalanx.out');rewrite(output);
   readln(n,m,q);
   if(n<=2000)and(m<=2000)then
   begin
      for i:=1 to n do
         for j:=1 to m do a[i][j]:=(i-1)*m+j;
      for cas:=1 to q do
      begin
         readln(x,y);
         writeln(a[x][y]);
         t:=a[x][y];
         for i:=y+1 to m do a[x][i-1]:=a[x][i];
         for i:=x+1 to n do a[i-1][m]:=a[i][m];
         a[n][m]:=t;
      end;
   end else if(n=1)then
   begin
      cnt:=trunc(sqrt(m));
      s:=1;
      for i:=1 to m do
      begin
         belong[i]:=s;
         if i mod cnt=0 then inc(s);
         if s>cnt then s:=cnt;
      end;
      for i:=1 to m do
      begin
         if belong[i]<>belong[i-1] then start[belong[i]]:=i;
         ending[belong[i]]:=i;
      end;
      for i:=1 to m do b[i]:=i;
      for cas:=1 to q do
      begin
         readln(x,y);
         x1:=b[y]+addi[belong[y]];
         x2:=b[m]+addi[belong[m]];
         writeln(x1);
         for i:=belong[y]+1 to belong[m-1]-1 do addi[i]:=addi[i]+1;
         for i:=y to start[belong[y]+1]-1 do inc(b[i]);
         for i:=ending[belong[m-1]-1]+1 to m-1 do inc(b[i]);
         delta:=x2-x1;
         b[m]:=b[m]-delta;
      end;
   end else
   begin
      for cas:=1 to q do
      begin
         readln(x,y);
         writeln(x*(m-1)+y+cas-1);
      end;
   end;
   close(input);close(output);
end.
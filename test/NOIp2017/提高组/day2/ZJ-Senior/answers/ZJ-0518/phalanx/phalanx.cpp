#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>
#include <set>
#include <algorithm>
#define rep(i,a,b) for(int i=a,i##_END_=b;i<i##_END_;++i)
#define per(i,a,b) for(int i=(b)-1,i##_BEGIN_=a;i>=i##_BEGIN_;--i)
#define dig(...) fprintf(stderr,__VA_ARGS__)
#define se second
using namespace std;
const int N=50000;
typedef long long ll;
typedef pair<int,ll> pil;
int n,m,q,c;
int id[505];
ll num[505][N+5];
set<pil>st;
set<pil>::iterator it;
const int M=300000;
struct BIT
{
	int A[M*2+5];
	inline int lowbit(int x)
	{
		return x&(-x);
	}
	int sum(int x)
	{
		int res=0;
		while(x>0)
		{
			res+=A[x];
			x-=lowbit(x);
		}
		return res;
	}
	void add(int x,int w)
	{
		while(x<=600000)
		{
			A[x]+=w;
			x+=lowbit(x);
		}
	}
}bit;
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	int x,y;
	if(q<=500)
	{
		int cnt=0;
		rep(i,1,n+1) num[0][i]=(ll)m*i;
		rep(i,0,q)
		{
			scanf("%d%d",&x,&y);
			int c=id[x];
			if(c==0)
			{
				c=id[x]=++cnt;
				rep(j,1,m)
				{
					num[c][j]=(ll)m*(x-1)+j;
				}
			}
			ll tmp;
			if(y!=m) tmp=num[c][y];
			else tmp=num[0][x];
			printf("%lld\n",tmp);
			rep(j,y+1,m)
			{
				num[c][j-1]=num[c][j];
			}
			if(y<m) num[c][m-1]=num[0][x];
			rep(j,x+1,n+1)
			{
				num[0][j-1]=num[0][j];
			}
			num[0][n]=tmp;
		}
	}
	else
	{
		rep(i,1,m+1) st.insert(pil(i,i));
		rep(i,2,n+1) st.insert(pil(i+m-1,(ll)i*m));
		c=n+m-1;
		rep(i,1,c+1) bit.add(i,1);
		rep(i,0,q)
		{
			scanf("%d%d",&x,&y);
			int L=1,R=c,res=1;
			while(L<=R)
			{
				int mid=(L+R)>>1;
				if(bit.sum(mid)>=y) res=mid,R=mid-1;
				else L=mid+1;
			}
			bit.add(res,-1);
			it=st.lower_bound(pil(res,0));
			ll tmp=(*it).se;
			printf("%lld\n",tmp);
			st.erase(it);
			c++;
			st.insert(pil(c,tmp));
			bit.add(c,1);
		}
	}
	return 0;
}

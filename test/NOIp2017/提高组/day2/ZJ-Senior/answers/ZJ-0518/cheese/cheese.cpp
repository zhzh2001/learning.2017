#include <cstdio>
#include <cstring>
#include <vector>
#include <cmath>
#include <iostream>
#include <algorithm>
#define rep(i,a,b) for(int i=a,i##_END_=b;i<i##_END_;++i)
#define per(i,a,b) for(int i=(b)-1,i##_BEGIN_=a;i>=i##_BEGIN_;--i)
#define dig(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
const int M=1000;
int st[M+5],ed[M+5];
bool mp[M+5][M+5];
int a[M+5],b[M+5],c[M+5];
bool vis[M+5];
int T,n,h,r;
double sqr(int x)
{
	return (double)x*x;
}
bool judge(int x,int y)
{
	return (sqrt(sqr(a[x]-a[y])+sqr(b[x]-b[y])+sqr(c[x]-c[y]))<=2*r);
}
bool dfs(int x)
{
	if(ed[x]) return 1;
	if(vis[x]) return 0;
	vis[x]=1;
	rep(i,1,n+1) if(mp[x][i])
	{
		if(dfs(i)) return 1;
	}
	return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%d%d",&n,&h,&r);
		rep(i,1,n+1)
		{
			vis[i]=0;
			st[i]=ed[i]=0;
			rep(j,1,n+1) mp[i][j]=(i==j);
		}
		rep(i,1,n+1)
		{
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
		}
		rep(i,1,n+1)
		{
			if(c[i]<=r) st[i]=1;
			if(h-c[i]<=r) ed[i]=1;
			rep(j,1,n+1)
			{
				if(i!=j) mp[i][j]=judge(i,j);
			}
		}
		bool f=0;
		rep(i,1,n+1)
		{
			if(!st[i]) continue;
			if(dfs(i))
			{
				f=1;
				break;
			}
		}
		if(f) puts("Yes");
		else puts("No");
	}
	return 0;
}

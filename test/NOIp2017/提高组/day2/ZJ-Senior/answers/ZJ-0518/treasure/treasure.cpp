#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>
#include <algorithm>
#define rep(i,a,b) for(int i=a,i##_END_=b;i<i##_END_;++i)
#define per(i,a,b) for(int i=(b)-1,i##_BEGIN_=a;i>=i##_BEGIN_;--i)
#define dig(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
typedef pair<int,int> pii;
typedef long long ll;
const int M=12;
const int INF=0x7fffffff;
int mp[M+5][M+5],dis[1<<12|7][M+5];
int que[5000000];
int n,m;
ll dp[1<<13],ans=1LL<<50;
void BFS(int x)
{
	int s=1<<x;
	rep(i,0,n)
	{
		if(x==i) dis[s][i]=1;
		else dis[s][i]=INF;
	}
	int qt=0,qh=0;
	que[qh++]=s;
	rep(i,0,1<<n)
	{
		dp[i]=INF;
	}
	dp[s]=0;
	while(qh>qt)
	{
		s=que[qt++];
		rep(i,0,n)
		{
			if(s&(1<<i))
			{
				rep(j,0,n)
				{
					if((s&(1<<j))==0)
					{
						if(mp[i][j]==INF) continue;
						int ns=s|(1<<j);
//						dig("%d(%lld) -> %d(%lld)   :   %d %d\n",s,dp[s],ns,dp[ns],dis[s][i],mp[i][j]);
						if(dp[ns]>=dp[s]+dis[s][i]*mp[i][j])
						{
							dp[ns]=dp[s]+dis[s][i]*mp[i][j];
							rep(k,0,n)
							{
								dis[ns][k]=dis[s][k];
							}
							dis[ns][j]=dis[s][i]+1;
							que[qh++]=ns;
						}
					}
				}
			}
		}
	}
	ans=min(ans,dp[(1<<n)-1]);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	int a,b,c;
	rep(i,0,n)
	{
		rep(j,0,n) mp[i][j]=INF;
		mp[i][i]=0;
	}
	rep(i,0,m)
	{
		scanf("%d%d%d",&a,&b,&c);
		a--;b--;
		mp[a][b]=min(mp[a][b],c);
		mp[b][a]=min(mp[b][a],c);
	}
	rep(i,0,n) BFS(i);
	cout<<ans<<endl;
	return 0;
}

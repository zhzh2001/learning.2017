#include <cmath>
#include <cctype>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxn=1e3+5, maxm=1e6+5;

class Graph{
public:
	struct Edge{
		int to, next; Graph* bel;
		Edge& operator++(){
			return *this=bel->edge[next]; }
		inline int operator *(){ return to; }
	};
	void reset(){
		cntedge=0; memset(fir, 0, sizeof(fir)); }
	void addedge(int x, int y){ 
		Edge &e=edge[++cntedge];
		e.to=y; e.next=fir[x];
		e.bel=this; fir[x]=cntedge;
	}
	Edge& getlink(int x){ return edge[fir[x]]; }
private:
	int cntedge, fir[maxn];
	Edge edge[maxm*2];
}g;

double sqr(long long x){ return x*x; }

struct Ball{
	int x, y, z; 
	void set(int t1, int t2, int t3){
		x=t1, y=t2; z=t3; }
}ball[maxn];

int T, n, h, r, visit[maxn]; 
bool flag;

bool link(const Ball &a, const Ball &b){
	return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z))<=2*r;
}

int getint(){
	int re=0; char c;
	for (c=getchar(); !isgraph(c); c=getchar());
	for (re=c-48; c=getchar(),isgraph(c); re=re*10+c-48);
	return re;
}

void dfs(int now){
	visit[now]=1; Graph::Edge e;
	for (e=g.getlink(now); *e; ++e)
		if (!visit[*e]) dfs(*e); 
}

int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &T); int x, y, z;
	while (T--){
		g.reset(); memset(visit, 0, sizeof(visit));
		scanf("%d%d%d", &n, &h, &r);
		for (int i=2; i<=n+1; ++i){
			scanf("%d%d%d", &x, &y, &z);
			if (z<=r){
				g.addedge(1, i); g.addedge(i, 1); }
			if (z>=h-r){
				g.addedge(n+2, i); g.addedge(i, n+2); }
			ball[i].set(x, y, z);
		}
		for (int i=2; i<=n+1; ++i)
		for (int j=2; j<i; ++j)
			if (link(ball[i], ball[j])){
				g.addedge(i, j);
				g.addedge(j, i); }
		n+=2; dfs(1);
		if (visit[n]) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

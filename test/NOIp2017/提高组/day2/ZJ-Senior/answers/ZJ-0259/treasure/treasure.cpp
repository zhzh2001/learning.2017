#include <cstdio>
#include <algorithm>
using namespace std;

const int maxn=15, INF=1e9;
int n, m, cnt, map[maxn][maxn], use[maxn];
int ans, pl[maxn], dis[maxn];

void solve(){
	for (int i=1; i<=n; ++i) dis[i]=-1;
	dis[pl[1]]=0; cnt=0;
	int now, minm, tmp, edge;
	for (int i=2; i<=n; ++i){
		now=pl[i]; //当前枚举的节点 
		minm=INF;
		for (int j=1; j<=n; ++j){ //前一个节点 
			if (dis[j]==-1||j==now) continue;
			if (map[now][j]==INF) continue;
			tmp=dis[j]+1; //最短路 
			if (tmp<minm){
				minm=tmp; edge=map[now][j]; }
		}
		if (minm==INF) return;
		dis[now]=minm; cnt+=dis[now]*edge;
	}
	ans=min(ans, cnt);
}

void dfs(int now){
	if (now==n+1){
		solve(); return; }
	for (int i=1; i<=n; ++i){
		if (use[i]) continue;
		use[i]=1; pl[now]=i;
		dfs(now+1); use[i]=0; 
	}
}

//40假算法 
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m); int x, y, z;
	for (int i=1; i<=n; ++i)
	for (int j=1; j<=n; ++j) map[i][j]=INF;
	for (int i=1; i<=m; ++i){
		scanf("%d%d%d", &x, &y, &z);
		map[x][y]=min(map[x][y], z);
		map[y][x]=min(map[y][x], z);
	}
	ans=INF; dfs(1); 
	printf("%d\n", ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

#include <cstdio>
using namespace std;

const int maxq=1e5+5;
int map[1005][1005];
int n, m, q, ans[maxq];

class Ta{
public:
	int lowbit(int x){ 
		return x&(-x); }
	void add(int x, int v){
		while (x<=m){
			data[x]+=v;
			x+=lowbit(x); } }
	int query(int x){
		int re=0;
		while (x>0){
			re+=data[x];
			x-=lowbit(x); }
		return re; }
	int data[maxq];
private:
}ta;

int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if (n==1){
		for (int i=1; i<=m; ++i) ta.add(i, 1);
		int x, y, t;
		for (int i=1; i<=q; ++i){
			scanf("%d%d", &x, &y);
			t=ta.query(y);
			printf("%d\n", t);
			ta.add(y, 1); 
			ta.data[m]=t-ta.query(m-1);
		}
		fclose(stdin); fclose(stdout);
		return 0;
	}
	for (int i=1; i<=n; ++i)
	for (int j=1; j<=m; ++j) 
		map[i][j]=(i-1)*m+j;
	int x, y, tmp;
	for (int i=0; i<q; ++i){
		scanf("%d%d", &x, &y);
		tmp=map[x][y]; printf("%d\n", tmp);
		for (int j=y; j<m; ++j) map[x][j]=map[x][j+1];
		for (int j=x; j<n; ++j) map[j][m]=map[j+1][m];
		map[n][m]=tmp;
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

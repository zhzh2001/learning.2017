#include<bits/stdc++.h>
#define N 15
#define inf 1000000000
using namespace std;
int n,m,dis[N][N],bit[N],dp[8193][N][N];
int cnt,s[N];
void upd(int &x,int y)
{
	//CNT++;
	if (y<x) x=y;
}
void dfs(int u,int p,int i,int j,int k)
{
	if (u>cnt)
	{
		if (p==i) return;
		for (int q=1;q<=n;q++)
			if (p&bit[q-1])
			{
				//dp[i][j][k]
				//dp[i^p][j][k]
				//dp[p][q][k+1]
				if (dis[j][q]<inf)
				{
					//cout<<i<<j<<k<<p<<q<<endl;
					upd(dp[i][j][k],dp[i^p][j][k]+dp[p][q][k+1]+k*dis[j][q]);
				}
			}
	}
	else
	{
		dfs(u+1,p,i,j,k);
		dfs(u+1,p|bit[s[u]-1],i,j,k);
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=n;j++)
			dis[i][j]=i==j?0:inf;
	}
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if (dis[x][y]>z)
			dis[x][y]=dis[y][x]=z;
	}
	bit[0]=1;
	for (int i=1;i<=n;i++)
		bit[i]=bit[i-1]<<1;
	memset(dp,0x3f,sizeof dp);
	for (int i=1;i<bit[n];i++)
	{
		for (int j=1;j<=n;j++)
			if (i&bit[j-1])
			{
				int tmp=i^bit[j-1];cnt=0;
				for(int t=1;tmp;tmp>>=1,t++)
					if (tmp&1) s[++cnt]=t;
				for (int k=1;k<=n-cnt+1;k++)
				{
					if (i==bit[j-1]) dp[i][j][k]=0;
					else
					{
						dfs(1,0,i,j,k);
					}
				}
			}
	}
	int Ans=inf;
	for (int i=1;i<=n;i++)
	{
		upd(Ans,dp[bit[n]-1][i][1]);
	}
	printf("%d\n",Ans);
}

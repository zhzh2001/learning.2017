#include<bits/stdc++.h>
#define N 2005
#define ll long long
using namespace std;
int n,vis[N];ll h,r,x[N],y[N],z[N];
int to[N*N*2],nxt[N*N*2],fst[N],l;
void link(int x,int y)
{
	//cout<<x<<' '<<y<<endl;
	to[++l]=y;nxt[l]=fst[x];fst[x]=l;
	to[++l]=x;nxt[l]=fst[y];fst[y]=l;
}
void dfs(int x)
{
	vis[x]=1;
	for (int i=fst[x];i;i=nxt[i])
		if (!vis[to[i]]) dfs(to[i]);
}
#define sqr(x) ((x)*(x))
bool dis(int i,int j,ll d)
{
	return sqr(x[i]-x[j])+sqr(y[i]-y[j])<=d*d-sqr(z[i]-z[j]);
}
void work()
{
	memset(vis,0,sizeof vis);
	memset(fst,0,sizeof fst);l=0;
	scanf("%d%lld%lld",&n,&h,&r);
	for (int i=1;i<=n;i++)
		scanf("%lld%lld%lld",&x[i],&y[i],&z[i]); 
	int s=n+1,t=n+2;
	for (int i=1;i<=n;i++)
	{
		if (z[i]-r<=r) link(s,i);
		if (z[i]+r>=h) link(t,i);
	}
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
		if (dis(i,j,r*2))
			link(i,j);
	dfs(s);
	if (vis[t]) puts("Yes");
	else puts("No");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--)
		work();
}

#include<bits/stdc++.h>
#define N 300005
#define M 30000005
#define ll long long
using namespace std;
int n,m,q,Rt[N],sz[N],S;
int ls[M],rs[M],size[M],tg[M],cnt;
int nd()
{
	cnt++;
	//if (cnt==M)cerr<<"ERROR";
	ls[cnt]=rs[cnt]=size[cnt]=tg[cnt]=0;
	return cnt;
}
void mk(int &k,int l,int r)
{
	if (!k) k=nd();
	size[k]=r-l+1;tg[k]=1;
}
void dn(int k,int l,int r)
{
	if (!tg[k]) return;
	int mid=l+r>>1;
	mk(ls[k],l,mid);
	mk(rs[k],mid+1,r);
	tg[k]=0;
}
void up(int k)
{
	size[k]=size[ls[k]]+size[rs[k]];
}
void mdy(int &k,int l,int r,int x,int y)
{
	if (!k) k=nd();
	if (x<=l&&r<=y)
	{
		mk(k,l,r);
		return;
	}
	int mid=l+r>>1;
	if (x<=mid) mdy(ls[k],l,mid,x,y);
	if (y>mid) mdy(rs[k],mid+1,r,x,y);
	up(k);
}
int qry(int k,int l,int r,int x)
{
	if (l==r) return l;
	int mid=l+r>>1;dn(k,l,r);
	if (x<=size[ls[k]]) return qry(ls[k],l,mid,x);
	return qry(rs[k],mid+1,r,x-size[ls[k]]);
}
void clr(int k,int l,int r,int x)
{
	if (l==r)
	{
		size[k]=0;
		return;
	}
	int mid=l+r>>1;dn(k,l,r);
	if (x<=size[ls[k]]) clr(ls[k],l,mid,x);
	else clr(rs[k],mid+1,r,x-size[ls[k]]);
	up(k);
}
vector<ll>v[N];
ll qry(int x,int y)
{
	y=qry(Rt[x],1,S,y);
	if (x==n+1)
	{
		if (y<=n) return (ll)y*m;
		return v[x][y-n-1];
	}
	else
	{
		if (y<=m-1) return (ll)(x-1)*m+y;
		return v[x][y-m];
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	S=n+m+q;
	for (int i=1;i<=n;i++)
	{
		mdy(Rt[i],1,S,1,m-1);
		sz[i]=m-1;
	}
	mdy(Rt[n+1],1,S,1,n);sz[n+1]=n;
	while(q--)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		ll tmp,t1;
		if (y==m)
		{
			t1=qry(n+1,x);
			printf("%lld\n",t1);
			clr(Rt[n+1],1,S,x);
			sz[n+1]++;mdy(Rt[n+1],1,S,sz[n+1],sz[n+1]);v[n+1].push_back(t1);
		}
		else
		{
			tmp=qry(x,y);t1=qry(n+1,x);
			printf("%lld\n",tmp);
			clr(Rt[x],1,S,y);
			clr(Rt[n+1],1,S,x);
			sz[x]++;mdy(Rt[x],1,S,sz[x],sz[x]);v[x].push_back(t1);
			sz[n+1]++;mdy(Rt[n+1],1,S,sz[n+1],sz[n+1]);v[n+1].push_back(tmp);
		}
	}
//	cerr<<cnt<<endl;
//	while(1);
}

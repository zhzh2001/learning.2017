var
  n,h,r,i,j,k,t,p:longint;
  x1,y1,z1:int64;
  a:array[0..1100] of longint;
  x,y,z:array[0..1000] of int64;
function g(x:longint):longint;
  begin
    if a[x]=x
      then g:=x
      else
        begin
          a[x]:=g(a[x]);
          g:=a[x];
        end;
  end;
procedure f(x,y:longint);
  var i,j:longint;
  begin
    i:=g(x);
    j:=g(y);
    a[i]:=j;
  end;
begin
  assign(input,'cheese.in');
  reset(input);
  assign(output,'cheese.out');
  rewrite(output);
  readln(t);
  for i:=1 to t do
    begin
      readln(n,h,r);
      for j:=0 to n+1 do
        a[j]:=j;
      for j:=1 to n do
        begin
          readln(x[j],y[j],z[j]);
          for k:=1 to j-1 do
            begin
              x1:=x[j]-x[k];
              y1:=y[j]-y[k];
              z1:=z[j]-z[k];
              if sqrt(x1*x1+y1*y1+z1*z1)<=2*r
                then f(j,k);
            end;
          if z[j]<=r
            then f(0,j);
          if h-z[j]<=r
            then f(n+1,j);
        end;
      if g(0)=g(n+1)
        then writeln('Yes')
        else writeln('No');
    end;
  close(input);
  close(output);
end.

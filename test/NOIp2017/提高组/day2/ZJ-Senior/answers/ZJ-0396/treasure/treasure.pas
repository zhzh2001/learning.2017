var
  n,m,i,j,k,t,sum,x,y,z,p:longint;
  minx:int64;
  a,fa:array[1..12,1..12] of longint;
  tt:array[1..12] of longint;
function min(x,y:longint):longint;
  begin
    if x>y
      then exit(y)
      else exit(x);
  end;
procedure dfs(step,x:longint);
  var i,j:longint;
  begin
    k:=k+1;
    if k>10000000
      then
        begin
          writeln(minx);
          close(input);
          close(output);
          halt;
        end;
    for i:=1 to n do
    if (tt[i]=0)and(a[i,x]<maxlongint)and(sum+a[i,x]<minx)
      then
        begin
          sum:=sum+a[i,x]*step;
          tt[i]:=1;
          fa[x,i]:=1;
          dfs(step+1,i);
          tt[i]:=0;
          sum:=sum-a[i,x]*step;
          fa[x,i]:=0;
        end;
    t:=0;
    for i:=1 to n do
      if tt[i]=0
        then
          begin
            t:=1;
            break;
          end;
    if t=0
      then minx:=min(minx,sum);
    if (step>1)and(t=1)
     then
       for i:=1 to n do
         if fa[i,x]=1
           then dfs(step-1,i);
  end;
begin
  assign(input,'treasure.in');
  reset(input);
  assign(output,'treasure.out');
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      a[i,j]:=maxlongint;
  for i:=1 to m do
    begin
      readln(x,y,z);
      a[x,y]:=min(a[x,y],z);
      a[y,x]:=a[x,y];
    end;
  minx:=maxlongint;
  for i:=1 to n do
    begin
      tt[i]:=1;
      dfs(1,i);
      tt[i]:=0;
    end;
  writeln(minx);
  close(input);
  close(output);
end.

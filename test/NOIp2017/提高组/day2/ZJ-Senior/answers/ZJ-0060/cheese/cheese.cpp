#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
//by piano
template<typename tp> inline void read(tp &x) {
	x = 0; char c = getchar();int f = 0;
	for(; c < '0' || c > '9'; f |= (c == '-'), c = getchar());
	for(; c >= '0' && c <= '9'; x = (x << 3) + (x << 1) + c - '0', c = getchar());
	if(f) x = -x;
}
namespace one {
	static const int N = 1100;
	struct Point {
		ll x, y, z;
		inline void in() {
			read(x), read(y), read(z);
		}
	}p[N];
	
	struct E {
		int to, nxt;
	}e[N << 2];
	int head[N], cnt = 0, vis[N];
	inline void add(int u, int v) {
		e[++ cnt] = (E) {v, head[u]};head[u] = cnt;
	}
	
	inline void dfs(int u) {
		vis[u] = 1;
		for(int i = head[u]; i; i = e[i].nxt) {
			int v = e[i].to;
			if(!vis[v]) dfs(v);
		}
	}
	int n;
	ll h, r;
	#define sqr(a) ((a) * (a))
	#define A(a) ((a) > 0 ? (a) : (-(a)))
	inline bool Cross(Point a, Point b) {
		return  sqrt((sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z)) * 1.0) <= 2.0 * r;
	}
	
	inline void init() {
		#define cc(a) memset(a, 0, sizeof(a))
		cc(head);cnt = 0;cc(vis);cc(p);
		#undef cc
	}
	
	int main() {
		init();
		read(n), read(h), read(r);
		int down = 1, up = n + 2;
		for(int i = 2; i <= n + 1; i ++) p[i].in();
		for(int i = 2; i <= n + 1; i ++) if(A(p[i].z - 0) <= r) add(down, i);
		for(int i = 2; i <= n + 1; i ++) if(A(p[i].z - h) <= r) add(i, up);
		for(int i = 2; i <= n + 1; i ++) {
			for(int j = 2; j <= n + 1; j ++) if(i != j) {
				if(Cross(p[i], p[j])) {
					add(i, j);
				}
			}
		}
		dfs(down);
		if(vis[up]) cout << "Yes\n";
		else cout << "No\n";
	}
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T; for(read(T); T --; ) one :: main();
}

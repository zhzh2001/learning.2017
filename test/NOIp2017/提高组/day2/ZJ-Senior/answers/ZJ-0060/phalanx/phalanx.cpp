#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
//by piano
template<typename tp> inline void read(tp &x) {
	x = 0; char c = getchar();int f = 0;
	for(; c < '0' || c > '9'; f |= (c == '-'), c = getchar());
	for(; c >= '0' && c <= '9'; x = (x << 3) + (x << 1) + c - '0', c = getchar());
	if(f) x = -x;
}
const int N = 3e5 + 10 ;
int tr[N], cnt = 0;
inline void U(int u, int v) {
	for(; u <= N - 10; u += u & -u) tr[u] += v;
}

inline int Query(int u, int ans = 0) {
	for(; u >= 1; u -= u & -u) {
		ans += tr[u];
	}
	return ans;
}
int n, m, Q, a[1010][1010], b[N];
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n), read(m);
	if(n <= 1000 && m <= 1000) {
		for(int i = 1; i <= n; i ++) 
			for(int j = 1; j <= m; j ++)
				a[i][j] = ++ cnt;	
		for(read(Q); Q --;) {
			int x, y;
			read(x), read(y);
			int tmp;
			cout << (tmp = a[x][y]) << "\n";
			a[x][y] = 0;
			for(int i = y + 1; i <= m; i ++) a[x][i - 1] = a[x][i];
			for(int i = x + 1; i <= n; i ++) a[i - 1][m] = a[i][m];
			a[n][m] = tmp; 
		}
	}
	else if(n == 1) {
		swap(n, m);cnt = n;
		for(int i = 1; i <= n; i ++) b[i] = i, U(i, 1);
		for(read(Q); Q --;) {
			int x, y;
			read(x), read(x);
			int l = 1, r = cnt;
			while(l < r) {
				int mid = l + (r - l) / 2;
				if(Query(mid) < x) l = mid + 1;
				else r = mid;
			}
//			cout << Query(4) << "\n";
			U(r, -1);
			cout << b[r] << "\n";
			
			b[++ cnt] = b[r];b[r] = 0;
			U(cnt, 1);
		}
	}
	
}
/* 
1 3 1000
1 2
1 2
1 2
1 2
*/

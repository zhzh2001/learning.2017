#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
//by piano
template<typename tp> inline void read(tp &x) {
	x = 0; char c = getchar();int f = 0;
	for(; c < '0' || c > '9'; f |= (c == '-'), c = getchar());
	for(; c >= '0' && c <= '9'; x = (x << 3) + (x << 1) + c - '0', c = getchar());
	if(f) x = -x;
}
int n, m, base;
const int N = 12;
namespace f__k {
	// .. long long ..
	// f[i][j][s] fa - dep - set
	const int SETS = (1 << 12) + 233, inf = 1e8;// bigger
	int  a[N][N], base, ans = inf;
	// f[a][b][c][d][e][f][g][h]+
	int Pow[233];
	int f[(1 << 24) + 23];
	inline int Set(int a, int b) {
		return Pow[a] * b;
	}
	
	inline int Q(int S, int i) {
		return (S % Pow[i + 1]) / Pow[i];
	}
	inline void dfs(int S, int ct) {
		if(ct == n) {
			ans = min(ans, f[S]);
			return ;
		}
		int t[N], b[N], tp1, tp2;
		tp1 = 0, tp2 = 0;
		for(int i = 0; i < n; i ++) if(!Q(S, i)) t[tp1 ++] = i;
		for(int i = 0; i < n; i ++) if(Q(S, i))  b[tp2 ++] = i;
		for(int u = 0; u < tp1; u ++) {
			for(int v = 0; v < tp2; v ++) {
				int i = t[u], j = b[v];
	//			cout << i << ' ' << j << "\n";
				int p = Q(S, j);
				int st = S + Set(i, p + 1);
				f[st] = min(f[st], f[S] + a[i][j] * p);
				dfs(st, ct + 1);
			}
		}
	}
	
	int main() {
		Pow[0] = 1;for(int i = 1; i <= n; i ++) Pow[i] = Pow[i - 1] * n;
		for(int i = 0; i < n; i ++) for(int j = 0; j < n; j ++) a[i][j] = (i == j ? 0 : inf);
		for(int i = 1; i <= m; i ++) {
			int u, v, w;
			read(u), read(v), read(w);u --, v --;
			a[u][v] = a[v][u] = min(a[u][v], w);
		}
		memset(f, 38, sizeof(f));
		for(int i = 0; i < n; i ++) f[Set(i, 1)] = 0, dfs(Set(i, 1), 1);
		cout << ans << "\n";
	}
}

struct E {
	int to, nxt, w;
}e[N * N * 2];
int head[N], cnt = 0, vis[N];
inline void add(int u, int v, int w) {
	e[++ cnt] = (E) {v, head[u], w};head[u] = cnt;
}
int res = 0;
inline void D(int u, int fa, int dep) {
	for(int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		if(v != fa) res += dep * e[i].w, D(v, u, dep + 1);
	}
}
int ans = 1e8;
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	read(n), read(m);base = (1 << n) - 1;
	if(m == n - 1) {
		for(int i = 1; i <= m; i ++) {
			int u, v, w;
			read(u), read(v), read(w);
			add(u, v, w), add(v, u, w);
		}
		for(int i = 1; i <= n; i ++) res = 0, D(i, 0, 1), ans = min(ans, res);
		cout << ans << "\n";
		return 0;
	}
	f__k :: main();
	
}

#include<bits\stdc++.h>
using namespace std;
typedef long long ll;
int n,m,q,t,i,j,x,y,hed,lat,xx,yy,cnt;
int f[1010][1010],len[775*2],nxt[775*2],pre[775*2],a[600010];
ll s,v[775*2][775];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void solve1()
{
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			f[i][j]=(i-1)*m+j;
	for (i=1;i<=q;i++)
	{
		x=read();y=read();t=f[x][y];
		for (j=y;j<m;j++) f[x][j]=f[x][j+1];
		for (j=x;j<n;j++) f[j][m]=f[j+1][m];
		f[n][m]=t;
		printf("%d\n",t);
	}
}
void find(int x)
{
	xx=hed;int sum=0;
	while (sum+len[xx]<x) sum+=len[xx],xx=nxt[xx];
	yy=x-sum;
}
void add(ll x)
{
	if (len[lat]+1>m)
	{
		for (int i=1;i<=2*775;i++)
			if (len[i]==0)
			{
				nxt[lat]=i;
				pre[i]=lat;
				len[i]=1;v[i][1]=x;
				lat=i;break;
			}
	}
	else
	{
		len[lat]+1;
		v[lat][len[lat]]=x;
	}
}
void bg(int x,int y)
{
	for (int i=1;i<=len[y];i++) v[x][len[x]+i]=v[y][i];
	len[x]+=len[y];len[y]=0;nxt[x]=nxt[y];
	if (y=lat) lat=x;
}
void bi(int x)
{
	if (x!=hed&&len[x]+len[pre[x]]<=m) bg(pre[x],x);
	if (x!=lat&&len[x]+len[nxt[x]]<=m) bg(x,nxt[x]);
}
void del(int x,int y)
{
	for (int i=y;i<len[x];i++) v[x][i]=v[x][i+1];
	len[x]--;
	bi(x);
}
void solve2()
{
	for (i=1;i<=m;i++) a[i]=i;
	for (i=2;i<=n;i++) a[m+i-1]=i*m;
	n=n+m-1;m=sqrt(n);cnt=n/m+(int)(n%m!=0);
	for (i=1;i<=cnt;i++)
	{		
		pre[i]=i-1;nxt[i]=i+1;
		if (i==cnt) len[i]=n%m;
		else len[i]=m;
		for (j=1;j<=len[i];j++) v[i][j]=a[(i-1)*m+j];
	}
	hed=1;lat=cnt;
	while (q--)
	{
		x=read();y=read();
		find(y);s=v[xx][yy];del(xx,yy);add(s);
		printf("%lld\n",s);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if (n<=1000&&m<=1000) solve1();
	else solve2();
	return 0;
}

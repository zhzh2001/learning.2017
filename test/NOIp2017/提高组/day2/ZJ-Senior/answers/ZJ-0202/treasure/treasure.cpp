#include<bits\stdc++.h>
using namespace std;
typedef long long ll;
int n,m,i,j,k,l,x,y,z,mini,minj;
ll ans,minm,minn,di[15],fi[15],f[15][15];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void sc(int w,ll m)
{
	if (m>=minn) return;
	if (w==0){minn=m;return;}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (fi[i]==1&&fi[j]==0&&f[i][j]!=2e9)
			{
				fi[j]=1;
				di[j]=di[i]+1;
				sc(w-1,m+di[i]*f[i][j]);
				fi[j]=0;
			}	
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++) f[i][j]=2e9;
	for (i=1;i<=m;i++)
	{
		x=read();y=read();z=read();
		f[x][y]=min(f[x][y],(ll)z);f[y][x]=f[x][y];
	}
	minn=2e9;
	for (k=1;k<=n;k++)
	{
		memset(fi,0,sizeof(fi));
		fi[k]=1;di[k]=1;ans=0;
		for (l=1;l<n;l++)
		{
			minm=2e9;
			for (int i=1;i<=n;i++)
				for (int j=1;j<=n;j++)
					if (fi[i]==1&&fi[j]==0&&f[i][j]!=2e9&&(di[i]*f[i][j]<minm||di[i]*f[i][j]==minm&&di[i]<di[mini]))
					{
						minm=di[i]*f[i][j];mini=i;minj=j;
					}
			ans+=minm;fi[minj]=1;di[minj]=di[mini]+1;
		}
		minn=min(minn,ans);
	}
	memset(fi,0,sizeof(fi));
	memset(di,0,sizeof(di));
	if (n<11)
	{		
		for (i=1;i<=n;i++)
		{
			fi[i]=1;di[i]=1;		
			sc(n-1,0);
			fi[i]=0;
		}
	}
	printf("%lld\n",minn);
	return 0;
}

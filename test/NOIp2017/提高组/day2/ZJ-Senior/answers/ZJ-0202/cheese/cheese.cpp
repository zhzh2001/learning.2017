#include<bits\stdc++.h>
using namespace std;
typedef long long ll;
int T,n,h,i,l,r,ff,f[1010],fi[1010],a[1010];
ll R,x[1010],y[1010],z[1010];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
ll dis(int i,int j)
{
	return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while (T--)
	{
		n=read();h=read();R=read();
		l=1;r=0;ff=0;
		memset(f,0,sizeof(f));memset(fi,0,sizeof(fi));
		for (i=1;i<=n;i++)
		{
			x[i]=read();y[i]=read();z[i]=read();
			if (z[i]<=R) a[++r]=i,fi[i]=1;
			if (h-z[i]<=R) f[i]=1;
		}
		while (l<=r)
		{
			for (i=1;i<=n;i++)
				if (!fi[i]&&dis(a[l],i)<=2*R)
				{
					a[++r]=i;fi[i]=1;
					if (f[i]==1) ff=1;
				}
			if (ff) break;
			l++;
		}
		if (ff) puts("Yes"); else puts("No");
	}
	return 0;
}

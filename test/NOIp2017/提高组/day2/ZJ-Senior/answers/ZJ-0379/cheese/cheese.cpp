#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstdio>
using namespace std;
int T,n,ans[50];
long long h,r;
long long x[1007],y[1007],z[1007];
bool f,vis[1007];
struct da
{
	int to,next;
	} edge[1000007];
int first[1007],iter;
void addedge(int x,int y)
{
	edge[++iter].next=first[x]; edge[iter].to=y; first[x]=iter;
	edge[++iter].next=first[y]; edge[iter].to=x; first[y]=iter;
	}
void dfs(int u)
{
	vis[u]=true;
	for (int i=first[u];i!=0;i=edge[i].next)
	{
		int v=edge[i].to;
		if (v==n+1) f=true;
		if (!vis[v]) 
		{
			dfs(v);
			}
		}
	}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>T;
	for (int cnt=1;cnt<=T;cnt++)
	{
		iter=0;
		f=false;
		memset(x,0,sizeof(x));
		memset(y,0,sizeof(y));
		memset(z,0,sizeof(z));
		memset(first,0,sizeof(first));
		memset(edge,0,sizeof(edge));
		memset(vis,false,sizeof(vis));
		cin>>n>>h>>r;
		for (int i=1;i<=n;i++)
		{
			cin>>x[i]>>y[i]>>z[i];
			}
		for (int i=1;i<=n;i++)
		{
			if (z[i]<=r) addedge(0,i);
			if ((z[i]+r)>=h) addedge(i,n+1);
			}
		for (int i=1;i<=n-1;i++)
			for (int j=i+1;j<=n;j++)
			{
				if ((r+r)*(r+r)>=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])) addedge(i,j);
				}
		dfs(0);
		if (f) ans[cnt]=1;
			else ans[cnt]=0;
		}
	for (int i=1;i<=T;i++)
	{
		if (ans[i]==1) cout<<"Yes"<<endl;
			else cout<<"No"<<endl;
		}
	fclose(stdin);
	fclose(stdout);
	return 0;
	}
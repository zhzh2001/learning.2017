#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstdio>
#include<queue>
using namespace std;
const int INF=1000007;
int n,m,ans,minn,a,b,c;
int dep[21],map[21][21],fa[21];
bool vis[21],rd[21][21];
queue <int> q;
struct da
{
	int p1,p2,w;
	} edge[1007];
int iter;
void addedge(int x,int y,int z)
{
	edge[++iter].p1=x; edge[iter].p2=y; edge[iter].w=z;
	}
int find(int x)
{
	if (fa[x]!=x) return find(fa[x]);
	else return x;
	}	
void unionn(int a,int b)
{
	fa[a]=find(fa[b]);
	}
bool comp(da a,da b)
{
	return a.w<b.w;
	}
void bfs(int k)
{
	memset(vis,false,sizeof(vis));
	memset(dep,0,sizeof(dep));
	dep[k]=1;
	q.push(k);
	vis[k]=true;
	while (!q.empty())
	{
		int u;
		u=q.front();
		q.pop();
		for (int i=1;i<=n;i++)
		{
			if (map[u][i]!=INF&&!vis[i])
			{
				q.push(i);
				vis[i]=true;
				if (fa[i]==0) fa[i]=u;
				else if (dep[fa[i]]+1>dep[u]+1)
					fa[i]=u;
				dep[i]=dep[fa[i]]+1;
				addedge(u,i,(dep[u])*map[u][i]);
				}
			}
		}
	return;
	}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			map[i][j]=INF;
	for (int i=1;i<=m;i++)
	{
		cin>>a>>b>>c;
		if (a==b) continue;
		addedge(a,b,c);
		if (map[a][b]>c)
		{
			map[a][b]=c;
			map[b][a]=c;
			}
		}
	minn=INF;
	for (int t=1;t<=n;t++)
	{
		memset(edge,0,sizeof(edge));
		iter=0;
		ans=0;
		for (int i=1;i<=n;i++)
			fa[i]=0;
		fa[t]=t;
		bfs(t);
		for (int i=1;i<=n;i++)
			fa[i]=i;
		sort(edge+1,edge+m+1,comp);
		for (int i=1;i<=m;i++)
			{
				if (find(edge[i].p1)!=find(edge[i].p2)) 
				{
					ans+=edge[i].w;
					unionn(edge[i].p1,edge[i].p2);
					}
				}
		minn=min(minn,ans);
		}
	cout<<minn;
	fclose(stdin);
	fclose(stdout);
	return 0;
	}
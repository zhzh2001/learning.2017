#include <cstdio>
#include <cmath>
#include <queue>
#include <cstring>
#define C (c=nc())
using namespace std;

queue<int>que;
vector<int>map[1005];
bool b[1005],top[1005],ans;
int T,n,h,r;
int x[1005],y[1005],z[1005];

inline char nc(void)
{
	static char ch[100010],*p1=ch,*p2=ch;
	return p1==p2&&(p2=(p1=ch)+fread(ch,1,100010,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &n)
{
	static char c;int f=1;n=0;C;
	while (c<'0'||c>'9') c=='-'?f=-1,C:C;
	while (c>='0'&&c<='9') n=(n<<3)+(n<<1)+c-48,C;
	return (void)(n*=f);
}

double sqr(int x){return x*x;}

void BFS(void)
{
	register int i;
	while (!que.empty())
	{
		if (ans) break;
		int x=que.front();
		que.pop();
		for (i=0;i<map[x].size();++i)
		{
			if (b[map[x][i]]) continue;
			if (top[map[x][i]])
			{
				ans=1;
				break;
			} else que.push(map[x][i]),b[map[x][i]]=1;
		}
	}
}

int main(void)
{
	freopen ("cheese.in","r",stdin);
	freopen ("cheese.out","w",stdout);
	read(T);
	while (T--)
	{
		ans=0;
		read(n),read(h),read(r);
		register int i,j;
		for (i=1;i<=n;++i) read(x[i]),read(y[i]),read(z[i]);
		for (i=1;i<=n;++i)
		{
			for (j=i;j<=n;++j)
			{
				if (i!=j&&sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))<=2.0*r)
				{
					map[i].push_back(j);
					map[j].push_back(i);
				}
			}
		}
		for (i=1;i<=n;++i)
		{
			if (z[i]+r>=h) top[i]=1;
			if (z[i]<=r) b[i]=1,que.push(i);
		}
		BFS();
		if (ans) puts("Yes"); else puts("No");
		for (i=1;i<=n;++i) map[i].clear();
		while (!que.empty()) que.pop();
		memset(top,0,sizeof(top));
		memset(b,0,sizeof(b));
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include <cstdio>
#include <queue>
#include <cstring>
#define C (c=nc())
#define LL long long
using namespace std;

int b[15],lnk[2005],nxt[2005],to[2005],val[2005],n,m,q;
LL ans;

struct wjnsb
{
	int x;
	LL val;
	int l;
	bool operator< (const wjnsb& i) const
	{
		return val>i.val;
	}
};

priority_queue<wjnsb>que;

inline char nc(void)
{
	static char ch[100010],*p1=ch,*p2=ch;
	return p1==p2&&(p2=(p1=ch)+fread(ch,1,100010,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &n)
{
	static char c;int f=1;n=0;C;
	while (c<'0'||c>'9') c=='-'?f=-1,C:C;
	while (c>='0'&&c<='9') n=(n<<3)+(n<<1)+c-48,C;
	return (void)(n*=f);
}

LL bfs(int root)
{
	LL sum=0;
	que.push((wjnsb){root,0,0});
	while (1)
	{
		while (b[que.top().x]==root&&!que.empty()) que.pop();
		if (que.empty()) break;
		wjnsb x=que.top();que.pop();
		b[x.x]=root;
		sum+=x.val;
		for (register int i=lnk[x.x];~i;i=nxt[i])
		{
			if (b[to[i]]==root) continue;
			que.push((wjnsb){to[i],val[i]*(x.l+1),x.l+1});
		}
	}
	return sum;
}

int main(void)
{
	freopen ("treasure.in","r",stdin);
	freopen ("treasure.out","w",stdin);
	read(n),read(m);
	ans=2139062143;
	memset(nxt,0xff,sizeof(nxt));
	memset(lnk,0xff,sizeof(lnk));
	register int i;
	for (i=1;i<=m*2;i+=2)
	{
		read(q),read(to[i]),read(val[i]);
		nxt[i]=lnk[q],lnk[q]=i;
		to[i+1]=q,val[i+1]=val[i];
		nxt[i+1]=lnk[to[i]],lnk[to[i]]=i+1;
	}
	for (i=1;i<=n;++i) ans=min(ans,bfs(i));
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

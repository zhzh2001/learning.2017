#include <bits/stdc++.h>
using namespace std;

const int inf = 0x3f3f3f3f;
int n,m;
int dis[12][12];
int f[1<<12][12];
int zy[1<<12][12];
int zw[1<<12][12];
int g[1<<12];
int gp[1<<12];

int main() {
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  cin.tie(0);
  cin.sync_with_stdio(false);
  cin>>n>>m;
  memset(dis,0x3f,sizeof dis);
  for(int i = 0; i < m; ++i) {
    int u,v,w; cin>>u>>v>>w;
    --u;--v;
    if(w<dis[u][v]) dis[u][v]=w;
  }
  for(int i = 0; i < n; ++i) {
    f[1<<i][i]=0;
  }
  int nn = 1<<n;
  for(int y = 1; y < nn; ++y) {
    g[y]=inf;
    for(int x = 0; x < n; ++x) {
      if(~y&(1<<x)) {
        f[y][x] = inf; continue;
      }
      f[y][x] = g[y^(1<<x)];
      zy[y][x] = y^(1<<x);
      zw[y][x] = gp[y^(1<<x)];
      int p = y^(1<<x);
      for(int z = p; z; (--z)&=p) {
        for(int w = 0; w < n; ++w) {
          if(~z&(1<<w)) continue;
          int res = f[y^z][x] + f[z][w];
          if(res>=inf) continue;
          res += dis[x][w];
          if(res>=inf) continue;
          if(res<f[y][x]) {
            f[y][x] = res;
            zy[y][x] = z;
            zw[y][x] = w;
          }
        }
      }
      if(f[y][x]<g[y]) {
        g[y]=f[y][x];
        gp[y]=x;
      }
    }
  }
  return 0;
}

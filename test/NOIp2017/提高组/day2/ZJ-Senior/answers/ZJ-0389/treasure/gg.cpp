#include <bits/stdc++.h>
using namespace std;

const int inf = 0x3f3f3f3f;
int n,m;
int dis[12][12];
int f[1<<12][12];
int minsz(int y,int x) {
  if(~y&(1<<x)) return inf;
  if((1<<x)==y) return 0;
  if( f[y][x] != inf) return f[y][x];
  int &res=f[y][x];
  for(int z = (y^(1<<x)); z; z = (z-1)&(y^(1<<x))) {
    // 最后一个子树 = z
    for(int w = 0; w < n; ++w) {
      if(~z&(1<<w)) continue;
      int ts = minsz(y^z,x);
      if(ts>=inf) continue;
      ts += minsz(z,w);
      if(ts>=inf) continue;
      ts += dis[x][w];
      if(ts>=inf) continue;
      if(ts<res) {
        res=ts;
      }
    }
  }
  return res;
}
int ans[1<<12][12];
int getans(int y,int x) {
  if(~y&(1<<x)) return inf;
  if((1<<x)==y) return 0;
  if( ans[y][x] != inf) return ans[y][x];
  int &res=ans[y][x];
  for(int z = (y^(1<<x)); z; z = (z-1)&(y^(1<<x))) {
    // 最后一个子树 = z
    for(int w = 0; w < n; ++w) {
      if(~z&(1<<w)) continue;
      int ts = getans(y^z,x);
      if(ts>=inf) continue;
      ts += getans(z,w);
      if(ts>=inf) continue;
      ts += minsz(z,w);
      if(ts>=inf) continue;
      ts += dis[x][w];
      if(ts>=inf) continue;
      if(ts<res) {
        res=ts;
      }
    }
  }
  return res;
}

namespace dfss {
  int res,ans;
  int vis[12];
  void dfs(int x) {
    if(res>=ans) return;
    if(x==n) {
      ans=res;return;
    }
    for(int i = 0; i < n; ++i) {
      if(!vis[i]) continue;
      for(int j = 0; j < n; ++j) {
        if(vis[j]) continue;
        if(dis[i][j] >= 0x3f3f3f3f) continue;
        vis[j] = vis[i] + 1;
        res += vis[i] * dis[i][j];
        dfs(x+1);
        res -= vis[i] * dis[i][j];
        vis[j] = 0;
      }
    }
  }

}

int main() {
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  cin.tie(0);
  cin.sync_with_stdio(false);
  cin>>n>>m;
  memset(dis,0x3f,sizeof dis);
  for(int i = 0; i < m; ++i) {
    int u,v,w; cin>>u>>v>>w;
    --u;--v;
    if(w<dis[u][v]) dis[u][v]=w;
    if(w<dis[v][u]) dis[v][u]=w;
  }
  if(n>8) {
    memset(f,0x3f,sizeof f);
    memset(ans,0x3f,sizeof ans);
    int nn = 1<<n;
    int anss = inf;
    for(int i = 0; i < n; ++i) {
      if(getans(nn-1,i) < anss) {
        anss = getans(nn-1,i);
      }
    }
    printf("%d\n", anss);
  } else {
    dfss::ans=0x3f3f3f3f;
    for(int i = 0; i < n; ++i) {
      dfss::vis[i] = 1;
      dfss::res = 0;
      dfss::dfs(1);
      dfss::vis[i] = 0;
    }
    printf("%d\n", dfss::ans);
  }
  return 0;
}


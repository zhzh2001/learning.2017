#include <bits/stdc++.h>
using namespace std;

const int inf = 0x3f3f3f3f;
int n,m;
int dis[12][12];
int f[1<<12][12];
int ans[1<<12][12];

int main() {
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  cin.tie(0);
  cin.sync_with_stdio(false);
  cin>>n>>m;
  memset(dis,0x3f,sizeof dis);
  for(int i = 0; i < m; ++i) {
    int u,v,w; cin>>u>>v>>w;
    --u;--v;
    if(w<dis[u][v]) dis[u][v]=w;
    if(w<dis[v][u]) dis[v][u]=w;
  }
  memset(f,0x3f,sizeof f);
  memset(f[0],0,sizeof f[0]);
  memset(ans,0x3f,sizeof ans);
  for(int i = 0; i < n; ++i) {
    f[1<<i][i]=0;
    ans[1<<i][i]=0;
  }
  int nn = 1<<n;
  for(int y = 1; y < nn; ++y) {
    for(int x = 0; x < n; ++x) {
      if(~y&(1<<x)) {
        continue;
      }
      int p = y^(1<<x);
      for(int z = p; z; (--z)&=p) {
        for(int w = 0; w < n; ++w) {
          if(~z&(1<<w)) continue;
          int res = f[y^z][x] + f[z][w];
          if(res>=inf) continue;
          res += dis[x][w];
          if(res>=inf) continue;
          if(res<f[y][x]) {
            f[y][x] = res;
            int t = ans[y^z][x] + ans[z][w] 
                  + f[z][w]     + dis[x][w];
            if(t < ans[y][x]) ans[y][x] = t;
          }
        }
      }
    }
  }
  int anss = inf;
  for(int i = 0; i < n; ++i) {
    if(ans[nn-1][i] < anss) {
      anss = ans[nn-1][i];
    }
  }
  printf("%d\n", anss);
  return 0;
}

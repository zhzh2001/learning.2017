#include <bits/stdc++.h>
usindis namespace std;

int n,m;
int vis[12];
int dis[12][12];
int res,ans;

void dfs(int x) {
  if(res>=ans) return;
  if(x==n) {
    ans=res;return;
  }
  for(int i = 0; i < n; ++i) {
    if(!vis[i]) continue;
    for(int j = 0; j < n; ++j) {
      if(vis[j]) continue;
      if(dis[i][j] >= 0x3f3f3f3f) continue;
      vis[j] = vis[i] + 1;
      res += vis[i] * dis[i][j];
      dfs(x+1);
      res -= vis[i] * dis[i][j];
      vis[j] = 0;
    }
  }
}

int main() {
  freopen("treasure.in","r",stdin);
  cin.tie(0);
  cin.sync_with_stdio(false);
  cin>>n>>m;
  memset(dis,0x3f,sizeof dis);
  ans=0x3f3f3f3f;
  for(int i = 0; i < m; ++i) {
    int u,v,w; cin>>u>>v>>w;
    --u;--v;
    if(w<dis[u][v]) dis[u][v]=w;
    if(w<dis[v][u]) dis[v][u]=w;
  }
  for(int i = 0; i < n; ++i) {
    vis[i] = 1;
    res = 0;
    dfs(1);
    vis[i] = 0;
  }
  printf("%d\n", ans);
  return 0;
}



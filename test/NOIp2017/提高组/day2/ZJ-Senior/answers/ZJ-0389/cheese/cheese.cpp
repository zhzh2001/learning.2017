#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct pt {
  ll x,y,z;
  bool operator<(const pt &a) const {
    return z<a.z;
  }
};
// 0 : 下表面
// n+1 : 上表面
int n;
ll h,r;
pt ps[23333];
ll diss(const pt &a, const pt &b) {
  #define sq(x) ((x)*(x))
  return sq(a.x-b.x)+sq(a.y-b.y)+sq(a.z-b.z);
  #undef sq
}
bool jiao(int x,int y) {
  if(x==y) return true;
  if(x>y) swap(x,y);
  if(x==0) {
    if(y==n+1) {
      return false;
    } else {
      return abs(ps[y].z-0) <= r;
    }
  } else {
    if(y==n+1) {
      return abs(ps[x].z-h) <= r;
    } else {
      return diss(ps[x],ps[y]) <= 4 * r * r;
    }
  }
}
bool vis[23333];
bool dfs(int x) {
  if(vis[x]) return false;
  vis[x] = true;
  if(x==n+1) return true;
  for(int i = n + 1; i >= 0; --i) {
    if(jiao(x,i)) {
      if(dfs(i)) return true;
    }
  }
  return false;
}

int main() {
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  cin.sync_with_stdio(false);
  cin.tie(0);
  int T; cin>>T;
  while(T--) {
    memset(vis,0,sizeof vis);
    cin>>n>>h>>r;
    for(int i = 1; i <= n; ++i) {
      cin>>ps[i].x>>ps[i].y>>ps[i].z;
    }
    sort(ps+1,ps+n+1);
    dfs(0);
    if(vis[n+1]) {
      puts("Yes");
    } else {
      puts("No");
    }
  }
  return 0;
}

#include <bits/stdc++.h>
using namespace std;
namespace rng{
  typedef unsigned int ui;
  ui x=998244353,y=233666233,z=613613613;
  ui rr() {
    ui t=x;t^=(t<<13); t^=(t>>5); t^=(t<<1);
    x=y;y=z;z=t^x^y;
    return t;
  }
} using rng::rr;

void gi(int &a) {
  a=0; int c=getchar();
  while(!(c>='0'&&c<='9')) c=getchar();
  while ((c>='0'&&c<='9')) a=a*10+c-'0', c=getchar();
}

typedef long long ll;
const int maxn = 333333;

void *mymemalloc(size_t);
#define zs(x) ((x)?(x)->sz:0)
struct node {
  node *l,*r;
  int sz;
  ll x, y;
  void *operator new(size_t sz) {return mymemalloc(sz);}
  node(ll x):l(0),r(0),sz(1),x(x),y(x) {}
  node(ll x, ll y):l(0),r(0),sz(y-x+1),x(x),y(y) {}
  node(node *l,node *r,ll x,ll y):l(l),r(r),sz(y-x+1+zs(l)+zs(r)), x(x),y(y) {}
};
#undef zs
#define sz(x) ((x)?(x)->sz:0)
inline node *adjust(node *x) {
  x->sz = x->y - x->x + 1 + sz(x->l) + sz(x->r);
  return x;
}
pair<node*, node*> esplit(node *x,ll m) {
  if(!x) return make_pair(x,x);
  if(m<=0) {
    node *l = x->l;
    x->l=0;
    return make_pair(l,adjust(x));
  }
  ll p = x->y - x->x + 1;
  if(m>=p) {
    node *r = x->r;
    x->r=0;
    return make_pair(adjust(x),r);
  }
  node *nx = new node(0,x->r,x->x + m,x->y);
  x->y = x->x+m-1; x->r = 0;
  return make_pair(adjust(x),nx);
}
node *merge(node *x, node *y) {
  if(!x) return y; if(!y) return x;
  if(rr() % (sz(x)+sz(y)) < sz(x)) {
    x->r=merge(x->r,y);
    return adjust(x);
  } else {
    y->l=merge(x,y->l);
    return adjust(y);
  }
}
pair<node*,node*> split(node *x,int k) {
  if(!x) return make_pair(x,x);
  int pl = sz(x->l), pr = sz(x->r);
  if(k<=pl) {
    pair<node*,node*> p=split(x->l,k);
    x->l=p.second;
    return make_pair(p.first,adjust(x));
  } else if(k <= sz(x) - pr) {
    return esplit(x,k-pl);
  } else {
    pair<node*,node*> p=split(x->r,k-(sz(x) - pr));
    x->r=p.first;
    return make_pair(adjust(x),p.second);
  }
}

int n,m,q;
node *hang[maxn], *lie;
#define xy(x,y) (((x)-1)*(ll)m+(y))
int main() { 
  freopen("phalanx.in","r",stdin);
  freopen("phalanx.out","w",stdout);
  gi(n); gi(m); gi(q);
  for(int i = 1; i <= n; ++i) {
    hang[i] = new node(xy(i,1),xy(i,m-1));
    lie = merge(lie,new node(xy(i,m),xy(i,m)));
  }
  for(int i = 1; i <= q; ++i) {
    int x,y; gi(x); gi(y);
    pair<node*,node*> p1;
    node *A,*B,*C,*D,*E,*F;
    if(y != m) {
      p1 = split(hang[x],y-1);
      A = p1.first;
      p1 = split(p1.second, 1);
      B = p1.first, C = p1.second;
      p1 = split(lie,x-1);
      D = p1.first;
      p1 = split(p1.second, 1);
      E = p1.first, F = p1.second;
      printf("%lld\n", B->x);
      hang[x] = merge(A,merge(C,E));
      lie = merge(D,merge(F,B));
    } else {
      p1 = split(lie,x-1);
      D = p1.first;
      p1 = split(p1.second, 1);
      E = p1.first, F = p1.second;
      printf("%lld\n", E->x);
      lie = merge(D,merge(F,E));
    }
  }
  return 0;
}
char pol[333<<20]; // 333M
char *pis=pol;
void *mymemalloc(size_t s) {
  return (pis+=s)-s;
}



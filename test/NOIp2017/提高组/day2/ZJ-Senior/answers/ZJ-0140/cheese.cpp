#include<cstdio>
#include<cstring>
int T,n,l,r,res,q[1010],bo[1010],tmp;
unsigned long long x[1010],y[1010],z[1010],h,R;
unsigned long long sqr(unsigned long long x) {return(x*x);}
unsigned long long dis(int a,int b)
{
	return(sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b]));
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%lld%lld",&n,&h,&R);
		R=R*R*4;
		l=1;r=0;res=0;
		memset(q,0,sizeof(q));
		memset(bo,0,sizeof(bo));
		for (int i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if (sqr(z[i])<=R/4)
			{
				r++;
				q[r]=i;
				if (sqr(h-z[i])<=R/4) res=1;
				bo[i]=1;
			}
		}
		if (res==1) {printf("Yes\n");continue;}
		while(l<=r&&res==0)
		{
			tmp=q[l];
			l++;
			for (int i=1;i<=n;i++)
			if (bo[i]==0&&dis(tmp,i)<=R)
			{
				bo[i]=1;
				r++;
				q[r]=i;
				if (sqr(h-z[i])<=R/4) res=1;
			}
		}
		if (res==1) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

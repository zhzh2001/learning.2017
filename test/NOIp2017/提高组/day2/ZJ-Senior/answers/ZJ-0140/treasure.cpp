#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
long long oo,n,m,x,y,z,a[100][100];
long long f[5000],g[5000][20],res,N,dis;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	oo=1000000000;
	scanf("%lld%lld",&n,&m);
	for (long long i=1;i<=n;i++)
		for (long long j=1;j<=n;j++) a[i][j]=oo;
	for (long long i=1;i<=m;i++)
	{
		scanf("%lld%lld%lld",&x,&y,&z);
		a[x][y]=min(a[x][y],z);
		a[y][x]=a[x][y];
	}
	N=1<<n;res=oo;
	for (long long i=1;i<=n;i++)
	{
		for (long long j=0;j<N;j++) f[j]=oo;
		memset(g,0,sizeof(g));
		f[1<<(i-1)]=0;
		g[1<<(i-1)][i]=1;
		for (long long j=(1<<(i-1));j<N;j++)
		if (f[j]!=oo)
			for (long long k=1;k<=n;k++)
			if ((j&(1<<(k-1)))==0)
			{
				y=j+(1<<(k-1));
				for (long long l=1;l<=n;l++)	
				if (((j&(1<<(l-1)))!=0)&&(a[l][k]!=oo))
				{
					dis=f[j]+a[l][k]*g[j][l];
					if (dis<f[y])
					{
						f[y]=dis;
						for (long long L=1;L<=n;L++) g[y][L]=g[j][L];
						g[y][k]=g[j][l]+1;
					}
				}
			}
		res=min(res,f[N-1]);
	}
	printf("%lld\n",res);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll n,m,Q,mp[1010][1010];
int main(){
	freopen("phalanx.in","r",stdin);
//	freopen("phalanx.out","w",stdout);
	n=read();	m=read();	Q=read();
	For(i,1,n)	For(j,1,m)	mp[i][j]=(i-1)*m+j;
	while(Q--){
		ll x=read(),y=read(),id=mp[x][y];
		writeln(mp[x][y]);
		for(ll t=y;t<=m;++t)	mp[x][t]=mp[x][t+1];
		for(ll t=x;t<=n;++t)	mp[t][m]=mp[t+1][m];
		mp[n][m]=id;
/*		For(i,1,n){
			For(j,1,m)	printf("%lld ",mp[i][j]);
			puts("");
		}puts("----------");*/
	}
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
ll read(){	ll x=0;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar());	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x;	}
void write(ll x){	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=12100000,M=2410010;
int ls[N],rs[N],siz[N],val[N],tr[M];
ll len[M],S[M],zyy[M],n,m,Q,cnt,Len;
ll query(int &x,int l,int r,int v){
	if (!x)	x=++cnt;	siz[x]++;
	if (l==r)	return l<m?l:val[x];
	ll mid=(l+r)>>1,left=mid-l+1-siz[ls[x]];
	return left>=v?query(ls[x],l,mid,v):query(rs[x],mid+1,r,v-left);
}
void insert(int &x,int l,int r,int v,int valu){
	if (!x)	x=++cnt;
	if (l==r)	return val[x]=valu,void(0);
	ll mid=(l+r)>>1;
	v<=mid?insert(ls[x],l,mid,v,valu):insert(rs[x],mid+1,r,v,valu);
}
void build(ll l,ll r,ll p){
	if (l==r){
		if (l<=n)	S[p]=1,zyy[p]=m*l;
		return;
	}ll mid=(l+r)>>1;
	build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
	S[p]=S[p<<1]+S[p<<1|1];
}
ll QQQ(ll l,ll r,ll p,ll v){
	S[p]--;
	if (l==r)	return zyy[p];
	ll mid=(l+r)>>1,left=S[p<<1];
	return left>=v?QQQ(l,mid,p<<1,v):QQQ(mid+1,r,p<<1|1,v-left);
}
void Insert(ll l,ll r,ll p,ll x,ll v){
	S[p]++;
	if (l==r)	return zyy[p]=v,void(0);
	ll mid=(l+r)>>1;
	x<=mid?Insert(l,mid,p<<1,x,v):Insert(mid+1,r,p<<1|1,x,v);
}
inline ll QQQ(ll x,ll id){//求这一行最后一个 
	ll ID=QQQ(1,n+Q,1,x);
	if (!id)	id=ID;
	Insert(1,n+Q,1,++Len,id);
	return ID;
}
inline void work(ll x,ll y){
	ll id;
	if (y<m){
		id=(ll)query(tr[x],1,m+Q,y)+(x-1)*m;
		writeln(id);
		len[x]++;
		insert(tr[x],1,m+Q,len[x],(int)(QQQ(x,id)-(x-1)*m));
	}else	writeln(QQQ(x,0LL));
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();	m=read();	Q=read();
	For(i,1,n)	len[i]=m-1;
	build(1,n+Q,1);	Len=n;
	For(hh,1,Q){	ll x=read(),y=read();	work(x,y);	}
}

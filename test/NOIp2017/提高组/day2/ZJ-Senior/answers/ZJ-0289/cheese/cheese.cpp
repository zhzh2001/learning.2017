#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
const ll N=1010;
ll q[N],n,h,r,vis[N],mp[N][N];
struct data{	ll x,y,z;	}p[N];
inline ll sqr(ll x){	return x*x;	}
bool bfs(){
	ll he=0,ta=0;	memset(vis,0,sizeof vis);
	For(i,1,n)	if (p[i].z<=r)	vis[q[++ta]=i]=1;
	while(he!=ta){
		ll x=q[++he];	if (p[x].z>=h-r)	return 1;
		For(i,1,n)	if (mp[x][i]&&!vis[i])	vis[q[++ta]=i]=1;
	}return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	h=read();	r=read();
		For(i,1,n)	p[i].x=read(),p[i].y=read(),p[i].z=read();
		For(i,1,n)	For(j,i+1,n)	mp[i][j]=mp[j][i]=(sqr(p[i].x-p[j].x)+sqr(p[i].y-p[j].y))<=(sqr(2*r)-sqr(p[i].z-p[j].z));
		puts(bfs()?"Yes":"No");
	}
}
/*
20yi^2
20yi^2*2
*/

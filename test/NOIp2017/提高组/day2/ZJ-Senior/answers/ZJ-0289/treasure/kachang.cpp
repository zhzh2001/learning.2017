#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<ctime>
using namespace std;
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll zyy[1<<12|111],f[1<<12|111][14][14],mp[14][14],ct[1<<12|111],n,m,answ=1e18;
void add(ll &x,ll y){	if (y<x)	x=y;	}
int main(){
	freopen("treasure.in","r",stdin);
	memset(f,60,sizeof f);
	memset(mp,60,sizeof mp);
	n=read();	m=read();
	For(i,1,m){
		ll x=read()-1,y=read()-1,w=read();
		mp[x][y]=mp[y][x]=min(mp[x][y],w);
	}
	For(i,0,n-1)	For(has_go,1,n)	f[1<<i][i][has_go]=0;
	For(i,0,n-1)	zyy[1<<i]=i;
	For(i,1,(1<<n)-1)	ct[i]=ct[i-(i&-i)]+1;
	FOr(has_go,n,1){
		For(a,0,n-1)	For(b,0,n-1)	if (mp[a][b]!=mp[n][n])
		For(S,0,(1<<n)-1)	if (S>>a&1)	if (ct[S]>=has_go)	for(ll T=S;T;T=(T-1)&S)	if (T>>b&1)
			add(f[S][a][has_go],f[T][b][has_go+1]+f[S^T][a][has_go]+mp[a][b]*has_go);
		writeln(clock());
	}
	For(i,1,n)	answ=min(answ,f[(1<<n)-1][i][1]);
	writeln(answ);
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<ctime>
using namespace std;
#define ll int
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll zyy[1<<12|111],f[1<<12|111][14],g[1<<12|111][14],mp[14][14],ct[1<<12|111],n,m,answ=1e9;
void add(ll &x,ll y){	if (y<x)	x=y;	}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(f,60,sizeof f);
	memset(mp,60,sizeof mp);
	n=read();	m=read();
	For(i,1,m){
		ll x=read()-1,y=read()-1,w=read();
		mp[x][y]=mp[y][x]=min(mp[x][y],w);
	}
	For(i,0,n-1)	g[1<<i][i]=0;
	For(i,0,n-1)	zyy[1<<i]=i;
	For(i,1,(1<<n)-1)	ct[i]=ct[i-(i&-i)]+1;
	FOr(has_go,n,1){
		ll to=0;
		memset(f,60,sizeof f);
		For(i,0,n-1)	f[1<<i][i]=0;
		For(S,0,(1<<n)-1)	if (n-ct[S]>=has_go-1)For(now,0,n-1)	if (S>>now&1)
		for(ll T=S;T;T=(T-1)&S)	if (!(T&(1<<now)))for(ll t=T;t;t-=t&-t){
			to=zyy[t&-t];
			if (mp[now][to]!=mp[n][n])	add(f[S][now],g[T][to]+f[S^T][now]+mp[now][to]*has_go);
		}
		memcpy(g,f,sizeof f);
	}
	For(i,1,n)	answ=min(answ,g[(1<<n)-1][i]);
	writeln(answ);
}

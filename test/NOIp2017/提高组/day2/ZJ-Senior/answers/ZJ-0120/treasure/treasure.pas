uses math;
var
  n,m,i,k,x,y,l,ansans,ans,origin,l_t:longint;
  bcj:array[1..20]of longint;
  a:array[1..20,1..20]of longint;
  low:array[1..20]of longint;
  cost:array[1..20,1..20]of longint;
procedure init;
begin
  assign(input,'treasure.in'); reset(input);
  assign(output,'treasure.out'); rewrite(output);
end;
procedure over;
begin
  close(input); close(output);
end;
function father(x:longint):longint;
begin
  if bcj[x]=x then exit(x);
  father:=father(bcj[x]);
  bcj[x]:=father;
end;
procedure update(x:longint);
var
  p:longint;
begin
  for p:=1 to n do
  if p<>x then
  if a[x,p]<>-1 then
  begin
    cost[x,p]:=min(cost[x,p],low[x]*a[x,p]);
    cost[p,x]:=cost[x,p];
  end;
end;
procedure choose;
var
  i,k,m,x,y:longint;
begin
  m:=maxlongint;
  for i:=1 to n-1 do
  for k:=n downto i+1 do
  if a[i,k]<>-1 then
  if father(i)<>father(k) then
  if cost[i,k]<m then
  begin
    x:=i; y:=k;
    m:=cost[i,k];
  end;
  inc(ans,m);

  low[y]:=min(low[y],low[x]+1);
  low[x]:=min(low[x],low[y]+1);
  update(x);
  update(y);
  bcj[father(x)]:=father(y);
end;
begin
  init;
  readln(n,m);
  for i:=1 to n do
  for k:=1 to n do
  a[i,k]:=-1;

  l_t:=-1;
  for i:=1 to m do
  begin
    readln(x,y,l);

        if l_t=-1 then
        l_t:=l
        else if l_t<>l then
        l_t:=-2;

    if a[x,y]=-1 then
      a[x,y]:=l
    else
    begin
      a[x,y]:=min(a[x,y],l);
    end;
    a[y,x]:=a[x,y];
  end;

  //when l_t=-2 please be careful

  ansans:=maxlongint;
  for origin:=1 to n do
  begin
    ans:=0;
    for i:=1 to n do
    begin
      low[i]:=maxlongint shr 2;
      bcj[i]:=i;
    end;
    for i:=1 to n do
    for k:=1 to n do
      cost[i,k]:=maxlongint;

    low[origin]:=1;
    update(origin);
    for i:=1 to n-1 do
    choose;
    ansans:=min(ansans,ans);
  end;
  writeln(ansans);
  over;
end.

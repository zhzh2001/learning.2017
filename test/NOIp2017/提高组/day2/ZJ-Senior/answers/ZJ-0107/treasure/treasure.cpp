#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
int read()
{int t=0,f;char c;
   c=getchar();
   while(!((c>='0' && c<='9')||c=='-')) c=getchar();
   if(c=='-') f=-1,c=getchar();else f=1;
   while(c>='0' && c<='9')
   {
   	t=t*10+c-48;c=getchar();
   }
   return t*f;
}
int n,m,s,ans;
int dis[15][15],x,y,z;
int line,front[30],to[30],next[30];
bool boo[15];
int pre[15],dep[15];
int f[15];
void insert(int x,int y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
	line++;to[line]=x;next[line]=front[y];front[y]=line;
}
int ss[15],t,w;
void getans()
{int i,sx=0;
	line=-1;memset(front,-1,sizeof(front));
	memset(boo,false,sizeof(boo));boo[s]=true;
	memset(dep,0,sizeof(dep));
	for(i=1;i<=n;i++) if(i!=s) insert(pre[i],i);
	t=w=1;ss[t]=s;
    while(t<=w)
      {
      	int x=ss[t];
      	for(i=front[x];i!=-1;i=next[i])
      	  if(!boo[to[i]])
      	    {
      	      dep[to[i]]=dep[x]+1;
      	      sx+=dep[to[i]]*dis[x][to[i]];
      	      boo[to[i]]=true;ss[++w]=to[i];
			}
		t++;
	  }
	ans=min(ans,sx);
}
int Ask(int x)
{
	if(f[x]==x) return x;
	else return Ask(f[x]);
}
void dfs(int x)
{int i;
	if(x>n) 
	{
	  getans();
	  return;
	}
	if(x==s) 
	 {
	  dfs(x+1);
	  return;
	}
	for(i=1;i<=n;i++)
	   if(dis[x][i]!=-1 && x!=i)
	    if(Ask(x)!=Ask(i)) 
	  {
	   f[x]=i;
	   pre[x]=i;
	   dfs(x+1);
	   f[x]=x;
      }
}
int main()
{int i,j,k;
    freopen("treasure.in","r",stdin);
    freopen("treasure.out","w",stdout);
	n=read();m=read();ans=1000000000;
	memset(dis,-1,sizeof(dis));
	for(i=1;i<=m;i++)
	  {
	  	x=read();y=read();z=read();
	  	if(dis[x][y]==-1) dis[x][y]=dis[y][x]=z;
	  	else dis[x][y]=min(dis[x][y],z),dis[y][x]=min(dis[y][x],z);
	  }
	for(i=1;i<=n;i++) f[i]=i;
	for(i=1;i<=n;i++) s=i,dfs(1);
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 1005
#define ll long long
using namespace std;
ll read()
{ll t=0,f;char c;
   c=getchar();
   while(!((c>='0' && c<='9')||c=='-')) c=getchar();
   if(c=='-') f=-1,c=getchar();else f=1;
   while(c>='0' && c<='9')
   {
   	t=t*10+c-48;c=getchar();
   }
   return t*f;
}
ll n,m,t,r;
ll front[N],to[N*N],next[N*N],line,s,e,h;
struct node{ll x,y,z;};
node a[N];
void insert(int x,int y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
	line++;to[line]=x;next[line]=front[y];front[y]=line;
}
ll dis(node a,node b)
{
	return (a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
}
bool b[N];
ll dl[N],tt,ww;
int main()
{ll i,j,k;
   freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
   t=read();
   for(;t;t--)
     {
     	n=read();h=read();r=read();
     	for(i=1;i<=n;i++) a[i].x=read(),a[i].y=read(),a[i].z=read();
     	s=0;e=n+1;
     	line=-1;memset(front,-1,sizeof(front));
     	for(i=1;i<=n;i++) if(a[i].z-r<=0 && a[i].z+r>=0) insert(s,i);
		for(i=1;i<n;i++)
		  for(j=i+1;j<=n;j++)
		    if(dis(a[i],a[j])<=4*r*r)	insert(i,j);
		for(i=1;i<=n;i++)
		  if(a[i].z-r<=h && a[i].z+r>=h) insert(i,e);
		memset(b,false,sizeof(b));b[0]=true;
		tt=ww=1;
		while(tt<=ww)
		  {
		  	ll x=dl[tt];
		  	for(i=front[x];i!=-1;i=next[i])
		  	  if(!b[to[i]])
		  	    {
		  	      b[to[i]]=true;dl[++ww]=to[i];	
				  }
			tt++;
		  }
		if(b[e]) printf("Yes\n");else printf("No\n");
	 }
	return 0;
}

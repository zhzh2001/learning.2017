#include<cstdio>
#include<algorithm>
#include<cstring>
#include<iostream>
using namespace std;

int n,m,q,f[600008],tree[1048579],g[508][3];

int lowbit(int x){
	return x&(-x);
}

int find(int x){
	int i=x,res=0;
	while(i>0){
		res+=tree[i];
		i=i-lowbit(i);
	}
	return res;
}

void add(int x){
	while(x<=1048576){
		tree[x]++;
		x=x+lowbit(x);
	}
}

void del(int x){
	while(x<=1048576){
		tree[x]--;
		x=x+lowbit(x);
	}
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	scanf("%d%d%d",&n,&m,&q);
	if(q<=500){
		for(int i=1;i<=q;i++){
			scanf("%d%d",&g[i][1],&g[i][2]);
			int x=g[i][1],y=g[i][2];
			for(int j=i-1;j>0;j--){
				if(g[j][1]==x&&y<g[j][2]) continue;
				if(g[j][1]>x) continue;
				if(g[j][1]==x&&y>=g[j][2]){
					if(y==m){
						x++;
					}else{
						y++;
					}
				}else{
					if(y==m){
						if(x==n) x=g[j][1],y=g[j][2];
						else x++;
					}
				}
			}
			printf("%lld\n",(long long)1*(x-1)*m+(long long)y);
		}
	}else{
		if(n==1){
			for(int i=1;i<=m;i++){
				f[i]=f[i-1]+1;
				add(i);
			}
			for(int i=1;i<=q;i++){
				int x,y;
				scanf("%d%d",&x,&y);
				int l=y,r=i+m-1;
				while(l<r){
					int mid=(l+r+1)>>1;
					if(find(mid)<=y){
						l=mid;
					}else{
						r=mid-1;
					}
				}
				while(f[l]==0)
					l--;
				printf("%d\n",f[l]);
				f[i+m]=f[l];
				f[l]=0;
				del(l);
				add(i+m);
			}
		}
	}
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#include<iostream>
using namespace std;

int f[18][18],n,m,head[10008],vet[10008],next[10008],tot;
int num[18],boo[18],flag[18],len[10008];
long long ans;

void add(int x,int y,long long z){
	tot++;
	next[tot]=head[x];
	head[x]=tot;
	vet[tot]=y;
	len[tot]=z;
}

void dfs(int u,long long sum,int k){
	if(sum>=ans){
		return;
	}
	if(k==n){
		ans=sum;
		return;
	}
	flag[u]=1;
	for(int i=head[u];i;i=next[i]){
		int y=vet[i];
		boo[y]++;
	}
	for(int i=1;i<=n;i++)
	if(boo[i]&&flag[i]==0){
		for(int j=head[i];j;j=next[j]){
			int y=vet[j];
			if(flag[y]==1){
				num[i]=(num[y]+1);
				dfs(i,sum+len[j]*num[i],k+1);
				num[i]=0;
			}
		}
	}
	for(int i=head[u];i;i=next[i]){
		int y=vet[i];
		boo[y]--;
	}
	flag[u]=0;
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	ans=100000000000000008;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			f[i][j]=10000008;
	for(int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		f[x][y]=min(f[x][y],z);
		f[y][x]=min(f[y][x],z);
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(f[i][j]!=10000008){
				add(i,j,(long long)f[i][j]);
			}
	for(int i=1;i<=n;i++){
		memset(boo,0,sizeof(boo));
		memset(num,0,sizeof(num));
		dfs(i,0,1);
	}
	printf("%lld",ans);
}

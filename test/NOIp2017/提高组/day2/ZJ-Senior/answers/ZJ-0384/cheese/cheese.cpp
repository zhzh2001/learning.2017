#include<cstdio>
#include<algorithm>
#include<cstring>
#include<iostream>
using namespace std;

int t,n,m,fa[1008];
long long r;
struct node{
	long long x,y,z;
}h[1008];

int find(int xx){
	if(fa[xx]==xx) return xx;
	return fa[xx]=find(fa[xx]);
}

void merge(int xx,int yy){
	int ii=find(xx);
	int jj=find(yy);
	if(ii!=jj){
		fa[jj]=ii;
	}
}

bool check(int xx,int yy){
	long long ans=(h[xx].x-h[yy].x)*(h[xx].x-h[yy].x)+(h[xx].y-h[yy].y)*(h[xx].y-h[yy].y)+(h[xx].z-h[yy].z)*(h[xx].z-h[yy].z);
	return ans<=((long long)4*r*r);
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	scanf("%d",&t);
	for(int j=1;j<=t;j++){
		scanf("%d%d%lld",&n,&m,&r);
		for(int i=1;i<=n+2;i++)
			fa[i]=i;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&h[i].x,&h[i].y,&h[i].z);
			if(h[i].z<=r){
				merge(i,n+1);
			}
			if((m-h[i].z)<=r){
				merge(i,n+2);
			}
		}
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(check(i,j)){
					merge(i,j);
				}
		if(find(n+1)==find(n+2)){
			printf("Yes\n");
		}else{
			printf("No\n");
		}
	}
}

#include <bits/stdc++.h>
using namespace std;

int a[1010][1010],n,m,ans=100000000;
bool vis[1010];

void dfs(int k,int aa,int bb,int ds)
{
	if (k==n)
	{
		if (aa<ans) ans=aa;
		return;
	}
	for(int i=1;i<=n;i++) if (!vis[i] && a[bb][i]<600000)
	{
		vis[i]=true;
		dfs(k+1,aa+a[bb][i]*ds,bb,ds);
		dfs(k+1,aa+a[bb][i]*ds,i,ds+1);
		vis[i]=false;
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x7f,sizeof(a));
	for(int i=1;i<=m;i++)
	{
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		if (v<a[x][y]) 
		{
			a[x][y]=v;
			a[y][x]=v;
		}
	}
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		vis[i]=true;
		dfs(1,0,i,1);
		vis[i]=false;
	}
	printf("%d",ans);
}

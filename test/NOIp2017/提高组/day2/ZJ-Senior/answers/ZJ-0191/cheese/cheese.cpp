#include <bits/stdc++.h>
using namespace std;

struct hole
{
	int x,y,z;
}ho[1010];
bool cmp(hole a,hole b)
{
	return a.z<b.z;
}
double jl(hole a,hole b)
{
	return sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2)+pow(a.z-b.z,2));
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	for(int tt=1;tt<=t;tt++)
	{
		int n,h,r,p;
		scanf("%d%d%d",&n,&h,&r);
		p=h;
		for(int i=1;i<=n;i++) scanf("%d%d%d",&ho[i].x,&ho[i].y,&ho[i].z);
		sort(ho+1,ho+1+n,cmp);
		int que[1010],head=0,tail=0;
		bool vis[1010],find=false;
		memset(vis,false,sizeof(vis));
		while(tail+1<=n && ho[tail+1].z<=r) 
		{
			tail++;	
			que[tail]=tail;
		}
		if (tail==0)
		{
			printf("No\n");
			break;
		}
		while(head<tail)
		{
			head++;
			if ((ho[que[head]].z+r)>=p) 
			{
				printf("Yes\n");
				find=true;
				break;
			}
			for(int it=que[head]+1;it<=n;it++) if (!vis[it] && (jl(ho[it],ho[que[head]])<=2*r))
			{
				vis[it]=true;
				tail++;
				que[tail]=it;
			}
		}
		if (!find) printf("No\n");
	}
}

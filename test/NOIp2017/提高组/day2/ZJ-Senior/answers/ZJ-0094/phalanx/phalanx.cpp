#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define p1 p<<1
#define p2 (p<<1)|1
using namespace std;
int n,m,Q,tot,mark[505],s[600005*4];
ll q[505][50005],a[600005];
ll getnum(int i,int j){
	return (ll)((i-1)*m+j);
}
void solve1(){
	tot=1;
	int cnt=0;
	for(int i=1;i<=n;i++) q[1][i]=getnum(i,m);
	for(int x,y,I=1;I<=Q;I++){
		scanf("%d%d",&x,&y);
		if(mark[x]==0){
			mark[x]=++tot;
			for(int i=1;i<m;i++) q[mark[x]][i]=getnum(x,i);
			q[mark[x]][m]=q[1][x];
		}
		q[mark[x]][m]=q[1][x];
		ll t=q[mark[x]][y];
		printf("%lld\n",t);cnt++;
		for(int i=y;i<m;i++) q[mark[x]][i]=q[mark[x]][i+1];
		q[1][n+1]=t,q[mark[x]][m]=q[1][x+1];
		for(int i=x;i<=n;i++) q[1][i]=q[1][i+1];
	}
}
int find(int l,int r,int l1,int r1,int p){
	if(l==l1&r==r1) return s[p];
	int mid=(l+r)>>1;
	if(r1<=mid) 
		return find(l,mid,l1,r1,p1);
	else if(l1>=mid+1)
		return find(mid+1,r,l1,r1,p2);
	else
		return find(l,mid,l1,mid,p1)+find(mid+1,r,mid+1,r1,p2);
}
void add(int l,int r,int x,int d,int p){
	if(l==r){s[p]+=d;return;}
	int mid=(l+r)>>1;
	if(x<=mid) add(l,mid,x,d,p1); else add(mid+1,r,x,d,p2);
	s[p]=s[p1]+s[p2];
}
void solve2(){
	for(int i=1;i<=m;i++)
		a[i]=getnum(1,i);
	for(int i=2;i<=n;i++)
		a[i+m-1]=getnum(i,m);
	int N=n+m-1;
	for(int x,y,i=1;i<=Q;i++){
		scanf("%d%d",&x,&y);
		int t=find(1,N,1,y,1);
		printf("%lld\n",a[(y+t-1)%N+1]);
		add(1,N,y,1,1); add(1,N,N,y-N,1);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&Q);
	if(Q<=500) solve1(); else solve2();
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdio>
#include<cmath>
#define ll long long
using namespace std;
const int MAXN=1010,MAXM=2000005;
int head[MAXN],vet[MAXM],Next[MAXM],vis[MAXN],n,cas,tot,S,T;
ll r,x[MAXN],y[MAXN],z[MAXN],h;
ll sqr(ll x){
	x=abs(x);
	return x*x;
}
ll getlen(int i,int j){
	return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
}
void add(int u,int v){
	Next[++tot]=head[u],head[u]=tot,vet[tot]=v;
}
void dfs(int u,int pre){
	vis[u]=1;
	for(int e=head[u];e!=0;e=Next[e]){
		int v=vet[e];
		if(v==pre) continue;
		if(!vis[v]) dfs(v,u);
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&cas);
	while(cas--){
		scanf("%d%lld%lld",&n,&h,&r),S=n+1,T=n+2;
		memset(head,0,sizeof(head));
		memset(vis,0,sizeof(vis));
		tot=0;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if(z[i]<=r) add(S,i);
			if(h-z[i]<=r) add(i,T);
		}
		r=sqr(2*r);
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++){
				ll t=getlen(i,j);
				if(t<=r) add(i,j),add(j,i);
			}
		dfs(S,-1);
		if(vis[T]) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

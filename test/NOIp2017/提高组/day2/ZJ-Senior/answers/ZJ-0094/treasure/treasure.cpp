#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#define ll long long
using namespace std;
const int inf=7*1e8,MAXN=15,MAXM=5000;
int L[MAXN][MAXN],head[MAXN],vet[MAXN*MAXN*2],Next[MAXN*MAXN*2],f[MAXN][MAXN][MAXM],vis[MAXN][MAXM],n,m,M,ans,tot;
void add(int u,int v){
	Next[++tot]=head[u],head[u]=tot,vet[tot]=v;
}
void dfs(int u,int sta,int len){
	if(vis[u][sta]) return;
	vis[u][sta]=1;
	f[u][len][1<<(u-1)]=0;
	for(int e=head[u];e!=0;e=Next[e]){
		int v=vet[e];
		if(((sta>>(v-1))&1)==1) continue;
		dfs(v,sta+(1<<(v-1)),len+1);
		for(int i=0;i<=M;i++){
			if((i>>(u-1))&1!=1) continue;
			int j=(i-1)&i;
			while(j!=0){
				f[u][len][i]=min(f[u][len][i],f[v][len+1][i-j]+f[u][len][j]+len*L[u][v]);
				j=(j-1)&i;
			}
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m),M=(1<<n)-1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			L[i][j]=-1;
	for(int u,v,c,i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&c);
		if(L[u][v]==-1) add(u,v),add(v,u),L[u][v]=L[v][u]=c; else L[u][v]=L[v][u]=min(L[u][v],c);
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=0;k<=M;k++)
				f[i][j][k]=inf;
	ans=inf;
	for(int i=1;i<=n;i++){
		dfs(i,1<<(i-1),1);
		ans=min(ans,f[i][1][(1<<n)-1]);
	}
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
using namespace std;
int T;
int N;
double H,R;
int mark[1001];
struct node{
	double X,Y,Z;
}	Data[1001];
int Queue[1001];
int Head,Tail;
int k;
double X1,Y1,Z1,X2,Y2,Z2;
double c;
void Wide_Search(){
	Head=Tail=0;
	for(int i=1;i<=N;i++){
		if(Data[i].Z<0)c=-Data[i].Z;else c=Data[i].Z;
		if(c<=R){
			Queue[++Tail]=i,mark[i]=1;
			if(H-Data[i].Z<0)c=Data[i].Z-H;else c=H-Data[i].Z;
			if(c<=R){
				printf("Yes\n");
				return;
			}
		}
	}
	while(Head<Tail){
		Head++;
		k=Queue[Head];
		X1=Data[k].X;
		Y1=Data[k].Y;
		Z1=Data[k].Z;
		for(int i=1;i<=N;i++){
			if(mark[i])continue;
			X2=Data[i].X;Y2=Data[i].Y;Z2=Data[i].Z;
			c=sqrt((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)+(Z1-Z2)*(Z1-Z2));
			if(c<=2*R){
				if(abs(H-Data[i].Z)<=R){
					printf("Yes\n");
					return;
				}
				mark[i]=1;
				Queue[++Tail]=i;
			}
		}
	}
	printf("No\n");
}
void Work(){
	scanf("%d%lf%lf",&N,&H,&R);
	memset(mark,0,sizeof(mark));
	for(int i=1;i<=N;i++)scanf("%lf%lf%lf",&Data[i].X,&Data[i].Y,&Data[i].Z);
	Wide_Search();
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)Work();
	fclose(stdin);fclose(stdout);
	return 0;
}

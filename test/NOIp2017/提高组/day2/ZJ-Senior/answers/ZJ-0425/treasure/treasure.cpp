//B.B.K.K.B.K.K.
//Bass Bass Kick Kick Bass Kick Kick
//I'm Emiya Myuma
#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<algorithm>
using namespace std;
int N,M;
int D[15][15];
int mark[15];
int Depth[15];
int res=1e9;
struct node{
	int Next,to,dis;
}	E[2005];int tot=0;
int Head[15];
struct Matrix{
	int H,y,dist;
}	Q[2005];int Tail;
void ADD(int x,int y,int z){
	tot++;
	E[tot].to=y;
	E[tot].dis=z;
	E[tot].Next=Head[x];
	Head[x]=tot;
}
void INIT(){
	memset(Head,-1,sizeof(Head));
	int x,y,z;
	scanf("%d%d",&N,&M);
	for(int i=1;i<=N;i++)
	 	for(int j=1;j<=N;j++)D[i][j]=1e8;
	for(int i=1;i<=N;i++)D[i][i]=0;
	for(int i=1;i<=M;i++){
		scanf("%d%d%d",&x,&y,&z);
			D[x][y]=D[y][x]=min(D[x][y],z);
	}
	for(int i=1;i<=N;i++)
		for(int j=1;j<=N;j++)
			if(D[i][j]!=0){
				ADD(i,j,D[i][j]);
				ADD(j,i,D[i][j]);
			}
}
void Shift_up(int x){
	int i=x,a,b;
	while(i>1){
		a=Q[i].dist,b=Q[i/2].dist;
		if(a<b)swap(Q[i],Q[i/2]);else break;
		i/=2;
	}
}
void Shift_down(int x){
	int i=x,j;
	while(i*2<=Tail){
		if(i*2+1>Tail)j=i*2;
		else j=(Q[i*2].dist<Q[i*2+1].dist?(i*2):(i*2+1));
		if(Q[i].dist>Q[j].dist)swap(Q[i],Q[j]),i=j;else break;
	}
}
void Push(int x,int t,int z){
	Tail++;
	Q[Tail].H=x;
	Q[Tail].dist=z;
	Q[Tail].y=t;
	Shift_up(Tail);
}
void Pop(){
	Q[1]=Q[Tail--];
	Shift_down(1);
}
Matrix Top(){
	return Q[1];
}
int Work(int x){
	int ans=0;Tail=0;
	memset(mark,0,sizeof(mark));
	mark[x]=1;
	Depth[x]=1;
	for(int i=Head[x];~i;i=E[i].Next){
		int m=E[i].to,d=E[i].dis;
		Push(x,m,d);
	}
	for(int i=1;i<N;i++){
		Matrix Y=Top();
		while(mark[Y.y])Pop(),Y=Top();
		ans+=Y.dist;
		mark[Y.y]=1;
		Depth[Y.y]=Depth[Y.H]+1;
		//cout<<Y.H<<' '<<Y.y<<' '<<Y.dist<<endl;
		//cout<<Y.y<<' '<<Depth[Y.y]<<endl;
		for(int j=Head[Y.y];~j;j=E[j].Next){
			int z=E[j].to,k=E[j].dis;
			if(mark[z])continue;
			Push(Y.y,z,k*Depth[Y.y]);
		}
	}//cout<<x<<' '<<ans<<endl;
	return ans;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	INIT();
	for(int i=1;i<=N;i++)res=min(res,Work(i));
	printf("%d\n",res);
	fclose(stdin);fclose(stdout);
	return 0;
}

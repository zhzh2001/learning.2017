#include<cstdio>
#include<cstring>
#include<cmath>
#include<cctype>
#include<cstdlib>
#include<algorithm>
using namespace std;
#define D isdigit(c=getchar())
inline int getint(){
	int x,c;
	for(;!D;);for(x=0;x=x*10+c-'0',D;);
	return x;
}
int buf[100];
inline void putint(long long x){
	int i;
	for(i=0;x;buf[i++]=x%10,x/=10);
	for(;i;putchar(buf[--i]+'0'));
	putchar(10);
}

const int N=700000;
const int M=2000000;
int k,sm[N*4],q1;
int rt[N],cnt,cnt2;
int b[N*3],n,m,q;
void bt(int o,int l,int r){
	if(l==r){
		sm[o]=1;return;
	}
	int mid=(l+r)>>1;
	bt(o<<1,l,mid);bt(o<<1|1,mid+1,r);
	sm[o]=sm[o<<1]+sm[o<<1|1];
}
void qry(int o,int l,int r){
	if(l==r){k=l;return;}
	int mid=(l+r)>>1;
	if(q1<=sm[o<<1])return qry(o<<1,l,mid);
	q1-=sm[o<<1];
	qry(o<<1|1,mid+1,r);
}
void modify(int o,int l,int r){
	if(l==r){sm[o]=0;return;}
	int mid=(l+r)>>1;
	if(q1<=mid)modify(o<<1,l,mid);
	else modify(o<<1|1,mid+1,r);
	sm[o]=sm[o<<1]+sm[o<<1|1];
}
struct data{
	int sz;
	long long L_data;
	data(int sz=0,long long L_data=0):sz(sz),L_data(L_data){}
}a[M];
int ch[M][2],fa[M],sz[M];
void ps(int o){
	sz[o]=a[o].sz;
	sz[o]+=sz[ch[o][0]]+sz[ch[o][1]];
}
void rot(int x){
	int o=fa[x],k=ch[fa[x]][0]==x;
	fa[ch[o][!k]=ch[x][k]]=o;
	fa[x]=fa[o];
	if(fa[o])ch[fa[o]][ch[fa[o]][1]==o]=x;
	fa[ch[x][k]=o]=x;ps(o);
}
void spy(int z,int x){
	for(;fa[x];rot(x)){
		if(fa[fa[x]])rot(ch[fa[x]][0]==x^ch[fa[fa[x]]][0]==fa[x]?x:fa[x]);
//		printf("%d\n",x);
	}
	ps(x);rt[z]=x;
}

void kth(int z,int o,int s){
//	printf("sz[%d]=%d sz[%d]=%d sz[%d]=%d %d\n",o,a[o].sz,ch[o][0],sz[ch[o][0]],ch[o][1],sz[ch[o][1]],s);
	if(s<=sz[ch[o][0]])return kth(z,ch[o][0],s);
	s-=sz[ch[o][0]];
	if(s<=a[o].sz){
		int asf=a[o].sz-s;
		putint(a[o].L_data+s-1);
		q1=z;qry(1,1,n+q);int AK_YE=b[k];
		b[++cnt2]=a[o].L_data+s-1;
		q1=k;modify(1,1,n+q);
		spy(z,o);
		int p1=++cnt,p2=++cnt;
		fa[p2]=p1;rt[z]=p1;
		fa[ch[p1][0]=ch[o][0]]=p1;fa[ch[p2][1]=ch[o][1]]=p2;ch[p1][1]=p2;
		a[p1]=data(s-1,a[o].L_data);
		a[p2]=data(asf,a[o].L_data+s);
		ps(p2);ps(p1);
		int df=p1;
		for(;ch[df][1];df=ch[df][1])sz[df]++;
		int p3=++cnt;
		a[p3]=data(1,AK_YE);fa[p3]=df;ch[df][1]=p3;sz[p3]=1;spy(z,p3);
		return;	
	}
	s-=a[o].sz;
	return kth(z,ch[o][1],s);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=getint();m=getint();q=getint();
	for(int i=1;i<=n;i++)b[i]=(long long)i*m;cnt2=n;
	for(int i=1;i<=n;i++){
		rt[i]=++cnt;sz[i]=m-1;
		a[cnt]=data(m-1,(long long)(i-1)*m+1);
	}
	bt(1,1,n+q);
	for(int i=1;i<=q;i++){
		int x=getint();int y=getint();
		if(y==m){
			q1=x;k=0;qry(1,1,n+q);
			putint(b[k]);
			b[++cnt2]=b[k];
			q1=k;modify(1,1,n+q);
		}else{
			kth(x,rt[x],y);
		}
	}
	return 0;
}


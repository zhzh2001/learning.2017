#include<cstdio>
#include<cstring>
#include<cmath>
#include<cctype>
#include<cstdlib>
#include<algorithm>

using namespace std;
const int N=1025;
struct Pt{
	long long x,y,z;
	Pt(long long x=0,long long y=0,long long z=0):x(x),y(y),z(z){}
	Pt operator -(const Pt&a)const{return Pt(x-a.x,y-a.y,z-a.z);}
	long long dt(){return x*x+y*y+z*z;}
}a[N];
bool G[N][N],vis[N];
int n,T;
long long h,r;
int q[N];
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		for(int i=1;i<=n;i++)scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		memset(G,0,sizeof(G));
		for(int i=1;i<=n;i++)for(int j=i+1;j<=n;j++)
			if((a[i]-a[j]).dt()<=4*r*r){
				G[i][j]=G[j][i]=1;
			}
		for(int i=1;i<=n;i++){
			if(a[i].z<=r&&a[i].z>=-r)G[i][n+1]=G[n+1][i]=1;
			if(a[i].z>=h-r&&a[i].z<=h+r)G[n+2][i]=G[i][n+2]=1;
		}
		int t=0,w=1;q[1]=n+1;
		memset(vis,0,sizeof(vis));vis[n+1]=1;
		while(t<w){
			int x=q[++t];
			for(int i=1;i<=n+2;i++)if(G[x][i]&&!vis[i]){
				vis[i]=1;q[++w]=i;
			}
		}
		if(vis[n+2])puts("Yes");else puts("No");
	}
	return 0;
}


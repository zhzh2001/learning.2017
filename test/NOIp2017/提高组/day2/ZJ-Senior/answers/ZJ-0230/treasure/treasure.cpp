#include<cstdio>
#include<cstring>
#include<cmath>
#include<cctype>
#include<cstdlib>
#include<algorithm>

using namespace std;
const int N=13;
int dp[N][N][1<<N],cz[1<<N][N];
int G[N][N],n,m;
inline void dw(int&x,const int&y){x>y?x=y:0;}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)G[i][j]=100000000;
	for(int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if(z<G[x][y]){
			G[x][y]=G[y][x]=z;
		}
	}
	for(int i=1;i<(1<<n);i++)
		for(int j=1;j<=n;j++)if((1<<j-1)&i)cz[i][++cz[i][0]]=j;
	memset(dp,0x3f,sizeof(dp));
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)dp[i][j][(1<<j-1)]=0;
	for(int i=n-1;i>=1;i--){
		for(int j=1;j<(1<<n);j++){
			for(int k=1;k<=n;k++)if((1<<k-1)&j){
			//	printf("dp[%d][%d][%d]=%d\n",i,k,j,dp[i][k][j]);
				if(dp[i][k][j]>=100000000)continue;
				int S=(1<<n)-j-1;
				int df=dp[i][k][j];
				int*fas=G[k];
				for(int l=S;l;l=l-1&S){
					int&z=dp[i][k][j|l];
					int*a=cz[l];
					for(int q=1;q<=a[0];q++)
						dw(z,df+dp[i+1][a[q]][l]+i*fas[a[q]]);
				}
			}
		}
	}
	int ans=100000000;
	for(int i=1;i<=n;i++)dw(ans,dp[1][i][(1<<n)-1]);
	printf("%d\n",ans);
	return 0;
}


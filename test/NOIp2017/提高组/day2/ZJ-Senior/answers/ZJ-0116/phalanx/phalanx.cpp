#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<set>
using namespace std;
int n,m,q,f[600100];
struct node{int x,y;}a[300100];
struct point {int x,rank;};
struct cmp
{
	bool operator () (point a,point b) { return a.rank<b.rank;}
};
set<point,cmp>Q,S;
void solve()
{
	for (int i=1;i<=q;i++)
	{
		scanf("%d%d",&a[i].x,&a[i].y);
		int x=a[i].x,y=a[i].y;
		for (int j=i-1;j>=1;j--)
		{
			if (x==n&&y==m) { x=a[j].x;y=a[j].y;continue;}
			if (x<a[j].x&&y<a[j].y) continue;
			if (x==a[j].x&&y>=a[j].y)
			{
				y++;
				if (y>m) x++,y=m;
				continue;
			}
			if (y==m&&x>=a[j].x) x++;
		}
		printf("%d\n",(x-1)*m+y);
	}
}
int lowbit(int x) { return x&(-x);}
void add(int x) { for (int i=x;i<=m+q;i+=lowbit(i)) f[i]++;}
int get(int x)
{
	int ans=0;
	for (int i=x;i>=1;i-=lowbit(i)) ans+=f[i];
	return ans;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (q<=1000)
	{
		solve();
		return 0;
	}
	int now=m;
	for (int i=1;i<=m;i++) S.insert((point){i,i});
	for (int i=2;i<=n;i++) S.insert((point){(i-1)*m+m,m+i-1});
	for (int i=1;i<=q;i++)
	{
		scanf("%d%d",&a[i].x,&a[i].y);
		int last=0,id=a[i].y;
		while (get(id)!=last)
		{
			int x=get(id);
			id+=x-last;
			last=x;
		}
		set<point>::iterator it=S.lower_bound((point){0,id});
		point s=*it;
		printf("%d\n",s.x);
		S.insert((point){s.x,++now});
		S.erase(it);
		add(s.rank);
	}
	return 0;
}

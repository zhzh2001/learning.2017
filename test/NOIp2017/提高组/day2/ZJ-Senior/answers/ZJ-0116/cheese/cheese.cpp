#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
int n,f[1050];
ll h,r;
struct node{ll x,y,z;}a[1010];
int get(int x)
{
	if (f[x]==x) return x;else f[x]=get(f[x]);
	return f[x];
}
bool calc(int i,int j)
{
	ll X=(a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z);
	if (sqrt(X)<=(double)r*2) return true;
	return false;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		for (int i=1;i<=n+2;i++) f[i]=i;
		for (int i=1;i<=n;i++)
		{
			if (a[i].z<=r)
			{
				int x=get(i),y=get(n+1);
				if (x!=y) f[x]=y;
			}
			if (h-a[i].z<=r)
			{
				int x=get(i),y=get(n+2);
				if (x!=y) f[x]=y;
			}
		}
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			if (calc(i,j)) 
			{
				int x=get(i),y=get(j);
				if (x!=y) f[x]=y;
			}
		if (get(n+1)==get(n+2)) puts("Yes");else puts("No");
	}
	return 0;
}

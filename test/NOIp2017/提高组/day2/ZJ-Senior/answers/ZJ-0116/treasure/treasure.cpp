#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
const int N=15,M=1020;
int n,m,tot,f[N],dis[N][N];
bool p[N];
ll Min;
void dfs(int u,ll s,int t)
{
	if (s>Min) return;
	if (t==n) { Min=min(Min,s);return;}
	for (int i=1;i<=n;i++)
	if (!p[i])
	{
		for (int j=1;j<=n;j++)
		if (p[j]&&dis[i][j]<=500000)
		{
			f[i]=f[j]+1;
			p[i]=1;
			dfs(u,s+(ll)dis[i][j]*f[i],t+1);
			p[i]=0;
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dis,0x3f,sizeof(dis));
	for (int i=1;i<=m;i++)
	{
		int x,y,z; 
		scanf("%d%d%d",&x,&y,&z);
		dis[x][y]=min(dis[x][y],z);
		dis[y][x]=dis[x][y];
	} 
	Min=1LL<<60;
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=n;j++) f[j]=0,p[j]=0;
		f[i]=0;p[i]=1;
		dfs(i,0,1);
	}
	printf("%lld\n",Min);
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstring>
using namespace std;
#define ll long long
#define N 1020
#define M 2000100
int he[N],ne[M],e[M],cnt;
bool vis[N];
int Q[N],n,S,T;
ll r,h;
struct P{
	ll x,y,z;
}p[N];
inline int read(){
	char ch=getchar(); int x=0,f=1;
	for (;ch>'9' ||ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

void add(int u,int v){ne[++cnt]=he[u];he[u]=cnt;e[cnt]=v;}
bool check(int i,int j){
	double x=sqrt(1LL*(p[i].x-p[j].x)*(p[i].x-p[j].x)+1LL*(p[i].y-p[j].y)*(p[i].y-p[j].y)+1LL*(p[i].z-p[j].z)*(p[i].z-p[j].z));
	return x<=2LL*r;
}
bool BFS(int S,int T){
	memset (vis,0,sizeof(vis));int h=0,t;
	vis[S]=1; Q[t=1]=S;
	while (h!=t){
		int x=Q[++h];
		for (int i=he[x];i;i=ne[i])
			if (!vis[e[i]]) vis[e[i]]=1,Q[++t]=e[i];
	}
	return vis[T];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int TT=read();
	while (TT--){
		n=read(),h=read(),r=read();
		S=0,T=n+1;
		memset(he,0,sizeof(he)); cnt=0;
		for (int i=1;i<=n;i++){
			p[i].x=read(),p[i].y=read(),p[i].z=read();
			if (p[i].z<=r) add(S,i);
			if (h-p[i].z<=r) add(i,T);
		}
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
				if (check(i,j)) add(i,j),add(j,i);
		if (BFS(S,T)) puts("Yes"); else puts("No");
	}
	return 0;
}

#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstring>
using namespace std;
#define N 300010
#define ll long long
inline int read(){
	char ch=getchar(); int x=0,f=1;
	for (;ch>'9' ||ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int bit[N];
int top,TT,n,m,Q,id[1010][1010],flag[50010];
ll Id[N<<1],p[501][50010],tot;
void add(int x,int k){for (;x<=m;x+=x&-x) bit[x]+=k;}
int query(int x){int res=0;for (;x;x-=x&-x) res+=bit[x];return res;}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),Q=read();
	if (n<=1000 && m<=1000){
		for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) id[i][j]=(i-1)*n+j;
		while (Q--){
			int x=read(),y=read();
			printf("%d\n",id[x][y]);
			int tmp=id[x][y];
			for (int j=y;j<m;j++) id[x][j]=id[x][j+1];
			for (int i=x;i<n;i++) id[i][m]=id[i+1][m];
			id[n][m]=tmp;
		}
		return 0;
	}
	if (n<=50000 && m<=50000){
		memset(flag,0,sizeof(flag));
		for (int i=1;i<=n;i++) Id[i]=1LL*i*m;
		while (Q--){
			int x=read(),y=read();
			if (y==m) {
				printf("%lld\n",Id[x]); ll tmp=Id[x];
				for (int i=x;i<n;i++) Id[i]=Id[i+1];
				Id[n]=tmp;
				continue;
			}
			if (!flag[x]){
				printf("%lld\n",1LL*(x-1)*m+y); ll tmp=1LL*(x-1)*m+y;
				flag[x]=++tot;
				for (int i=1;i<y;i++) p[tot][i]=1LL*(x-1)*m+i;
				for (int i=y;i<m;i++) p[tot][i]=1LL*(x-1)*m+i+1;
				p[tot][m-1]=Id[x]; if (x==n) p[tot][m]=tmp; else
				p[tot][m]=Id[x+1];
				for (int i=x;i<n;i++) Id[i]=Id[i+1];
				Id[n]=tmp;
			}else{
				int tt=flag[x];
				printf("%lld\n",p[tt][y]); ll tmp=p[tt][y];
				for (int i=y;i<m;i++) p[tt][i]=p[tt][i+1];
				p[tt][m-1]=Id[x];
				if (x==n) p[tt][m]=tmp; else p[tt][m]=Id[x+1];
				for (int i=x;i<n;i++) Id[i]=Id[i+1];
				Id[n]=tmp;
			}
	//		for (int i=1;i<=m;i++) printf("%d ",p[flag[5]][i]); puts("");
		}
		return 0;
	} 
/*	if (n==1){
		for (int i=1;i<=m;i++) add(i,1);
		TT=m;
		while (Q--){
			int x=read(),y=read();
			if (y<=TT){
				int tmp=query(y);
				printf("%d\n",tmp);
				TT--; int ttt=query(y+1)-tmp;
				add(y,ttt);		
				St[++top]=tmp;
				printf("%d:::",TT); for (int i=1;i<=m;i++) printf("%d ",query(i)); puts("");
			} else{
				int tmp=find(y-TT);
				while (St[tmp]==0) tmp++;
				printf("%d\n",St[tmp]);
				add2(tmp+1,1);
				St[++top]=St[tmp];
				St[tmp]=0;
			}
		}
		return 0;
	}*/
	if (n==1){
		for (int i=1;i<=m;i++) add(i,1);
		for (int i=1;i<=m;i++) Id[i]=i;
		int tot=m;
		while (Q--){
			int x=read(),y=read();int tmp=query(y),t=Id[tmp];
			printf("%d\n",t);
			add(y,1);
			Id[tmp]=0;
			Id[++tot]=t;
		//	for (int i=1;i<=m;i++) printf("%d ",query(i)-query(i-1)); puts("");
		//	for (int i=1;i<=m;i++) printf("%d ",query(i)); puts("");
		//	for (int i=1;i<=m;i++) printf("%d ",Id[query(i)]); puts("");
		//	for (int i=1;i<=tot;i++) printf("%d ",Id[i]); puts("");
		}
		return 0;
	}
	return 0;
}
				

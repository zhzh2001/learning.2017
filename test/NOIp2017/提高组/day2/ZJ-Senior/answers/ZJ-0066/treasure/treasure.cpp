#include <cstdio>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstring>
using namespace std;
#define N 13
#define M 1010
int he[N],he2[N],ne[M],ne2[M],e[M],e2[M],va[M],va2[M],cnt,cnt2;
int f[N][N],dep[N],sum,ans,n,m,time;
int U[M],V[M],W[M],fa[N],a[N],id[M],U1[M],V1[M],W1[M];
inline int read(){
	char ch=getchar(); int x=0,f=1;
	for (;ch>'9' ||ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void add(int u,int v,int w){
	if (f[u][v]!=0) {
		if (va[f[u][v]]>w) {
			va[f[u][v]]=w;
			W[f[u][v]/2+1]=w;
		}
	}
	else {
		f[u][v]=++cnt;ne[cnt]=he[u];he[u]=cnt;e[cnt]=v;va[cnt]=w;
		U[f[u][v]/2+1]=u;V[f[u][v]/2+1]=v;W[f[u][v]/2+1]=w;
	}
	if (f[v][u]!=0){
		if (va[f[v][u]]>w) va[f[v][u]]=w;
	}else{
		f[v][u]=++cnt;ne[cnt]=he[v];he[v]=cnt;e[cnt]=u;va[cnt]=w;
	}
}
void add2(int u,int v,int w){
	ne2[++cnt2]=he2[u];he2[u]=cnt2;e2[cnt2]=v;va2[cnt2]=w;
}
void dfs(int x,int fa){
	if (sum>=ans) return;
	dep[x]=dep[fa]+1;
	for (int i=he[x];i;i=ne[i])
		if (e[i]!=fa){sum+=dep[x]*va[i];dfs(e[i],x);}
}
void dfs2(int x,int fa){
	if (sum>=ans) return;
	dep[x]=dep[fa]+1;
	for (int i=he2[x];i;i=ne2[i])
		if (e2[i]!=fa){sum+=dep[x]*va2[i];dfs2(e2[i],x);}
}
int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
void check(){
	memset(he2,0,sizeof(he2)); cnt2=0;
	for (int i=1;i<n;i++){
		add2(U[a[i]],V[a[i]],W[a[i]]);
		add2(V[a[i]],U[a[i]],W[a[i]]);
	}
	for (int i=1;i<=n;i++){
		sum=0; dfs2(i,0);
		ans=min(ans,sum);
	}
	if (n>8) time++;
	if (time>=10000000){printf("%d\n",ans); exit(0);}
}
void dfs2(int t,int la,int aim){
	int tmp[13];
	if (t==aim+1) {check(); return;}
	for (int i=la;i<=cnt/2;i++){
		int u=find(U[i]),v=find(V[i]);
		if (u!=v){
			memcpy(tmp,fa,sizeof(fa));
			fa[u]=v; a[t]=i;
			dfs2(t+1,i+1,aim);
			memcpy(fa,tmp,sizeof(tmp));
		}
	}
}
bool cmp(const int &a,const int &b){
	return W[a]<W[b];
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read(),w=read();
		if (u>v) swap(u,v); if (u==v) continue;
		add(u,v,w);
	}
	ans=2e9;
	if (m==n-1){
		for (int i=1;i<=n;i++){
			sum=0;
			dfs(i,0); ans=min(ans,sum);
		}
		printf("%d\n",ans);
		return 0;
	}
	for (int i=1;i<=n;i++) fa[i]=i;
	for (int i=1;i<=cnt/2;i++) id[i]=i;
	sort(id+1,id+cnt/2+1,cmp);
	memcpy(U1,U,sizeof(U)); memcpy(V1,V,sizeof(V)); memcpy(W1,W,sizeof(W));
	for (int i=1;i<=cnt/2;i++) {
		U[i]=U1[id[i]];
		V[i]=V1[id[i]];
		W[i]=W1[id[i]];
	}
	//for (int i=1;i<=cnt/2;i++) printf("%d %d %d\n",U[i],V[i],W[i]);
	dfs2(1,1,n-1);
	printf("%d\n",ans);
	return 0;
}
	
	


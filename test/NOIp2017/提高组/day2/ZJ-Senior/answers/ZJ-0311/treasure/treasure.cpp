#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define fi first
#define se second
#define FO(i,a,b) for(int i=a;i<=b;++i)
#define FD(i,a,b) for(int i=a;i>=b;--i)
#define oo 0x7f7f7f7f

inline int read(void){
	int x,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1; 
	for(x=0;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

int n,m,ans,ret,f[13];
int a[13][13],x,y,z;
bool ch[13][13];

void dfs1(int x,int dep,int fa){
	FO(i,1,n) if (ch[x][i]&&i!=fa) {
		ans+=a[x][i]*dep;
		dfs1(i,dep+1,x);
	}
}
inline void calc(){ FO(i,1,n) { ans=0; dfs1(i,1,-1); ret=min(ret,ans); } }
int find(int x){ return f[x]==x?x:find(f[x]); }

bool flag;
int si,ff[13][13],p,rettt,rett;

bool cut(){
	FO(i,1,n) ff[i][0]=0;
	FO(i,1,n) { p=++ff[find(f[i])][0];ff[find(f[i])][p]=i; }
	
	rettt=0;
	FO(i,1,n) if (ff[i][0]){
		rett=oo;
		FO(j,1,ff[i][0]) { ans=0; dfs1(ff[i][j],1,-1); rett=min(rett,ans); }
		rettt+=rett;
		if (rettt>=ret) return true;
	}
	return false;
}
pair<int,int> cc[13][13];
void dfs(int x){
	if (cut()) return ;
	if (x==n-1) { ret=rettt; return ; }
	FO(i,1,n) {
		FO(jj,1,n) { int j=cc[i][jj].se; if(j<=i) continue; if (!ch[i][j]&&a[i][j]!=oo){
			int x1=find(i),x2=find(j),t;
			if (x1==x2) continue;
			t=f[x1];f[x1]=x2;ch[i][j]=ch[j][i]=1;
			dfs(x+1);
			ch[i][j]=ch[j][i]=0;f[x1]=t;
		}}
	}
}

int main(void){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();ret=oo;flag=true;si=-1;
	FO(i,1,n) f[i]=i;
	FO(i,1,n) FO(j,1,n) a[i][j]=oo;
	FO(i,1,m) {
		x=read();y=read();z=read();
		a[x][y]=min(a[x][y],z);a[y][x]=a[x][y];
		if (si==-1) si=a[x][y];
		if (a[x][y]!=si) flag=false; 
	}
	FO(i,1,n)FO(j,1,n) cc[i][j]=mp(a[i][j],j);
	FO(i,1,n) sort(cc[i]+1,cc[i]+1+n);
	dfs(0);
	printf("%d\n",ret);
	return 0;
}


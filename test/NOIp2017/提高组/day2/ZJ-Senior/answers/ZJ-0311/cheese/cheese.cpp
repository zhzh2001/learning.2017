#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define fi first
#define se second
#define FO(i,a,b) for(int i=a;i<=b;++i)
#define FD(i,a,b) for(int i=a;i>=b;--i)

inline int read(void){
	int x,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1; 
	for(x=0;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

typedef long long LL;
LL h,r1,r,x[1005],y[1005],z[1005];
int T,n,hd[1005],v[1005],nxt[1005*1005],fall[1005*1005],cnt;

inline LL sqr(LL x){ return x*x; }
inline void ae(int x,int y){ ++cnt;nxt[cnt]=hd[x];fall[cnt]=y;hd[x]=cnt; }

queue<int> Q;
void bfs(int x){
	while (!Q.empty()) Q.pop();
	Q.push(x);v[x]=1;
	while (!Q.empty()){
		int x=Q.front();Q.pop();
		for (int i=hd[x];i;i=nxt[i]){
			int to=fall[i];
			if (v[to]) continue;
			v[to]=1;Q.push(to);
			if (to==n+2) return ;
		}
	}
}

inline bool ck(double x1,double x2,double x3,double r){ return x1*x1+x2*x2+x3*x3-r<=1e-7; }

int main(void){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while (T--){
		n=read();h=read();r1=read();r=4*r1*r1;
		FO(i,1,n+2) hd[i]=v[i]=0;cnt=0;
		FO(i,1,n) x[i]=read(),y[i]=read(),z[i]=read();
		FO(i,1,n) FO(j,i+1,n)
		if (ck(x[i]-x[j],y[i]-y[j],z[i]-z[j],r)) ae(i,j),ae(j,i);
		FO(i,1,n) if (z[i]<=r1) ae(n+1,i),ae(i,n+1);
		FO(i,1,n) if (h-z[i]<=r1) ae(i,n+2),ae(n+2,i);
		bfs(n+1);puts(v[n+2]?"Yes":"No");		
	}
	return 0;
}


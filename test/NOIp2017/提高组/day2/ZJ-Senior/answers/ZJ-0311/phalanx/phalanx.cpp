#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define fi first
#define se second
#define FO(i,a,b) for(int i=a;i<=b;++i)
#define FD(i,a,b) for(int i=a;i>=b;--i)
typedef long long LL;

inline int read(void){
	int x,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1; 
	for(x=0;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

int n,m,q,a[1005][1005];

void bf(){
	int cnt=0;
	FO(i,1,n)FO(j,1,m) a[i][j]=++cnt;
	while (q--){
		int x,y;x=read();y=read();
		printf("%d\n",cnt=a[x][y]);
		FO(i,y,m-1) a[x][i]=a[x][i+1];
		FO(i,x,n-1) a[i][m]=a[i+1][m];
		a[n][m]=cnt;
	}
}

int e[501][50001];
int d[50001],c[50001];
//int e[101][100001];
//int d[100001],c[100001];
void bf1(){
	int cnt=0,cnt1=0,now;
	FO(i,1,n) c[i]=i*m;
	FO(i,1,n) d[i]=-1;
	while (q--){
		int x,y;x=read();y=read();
		if (y==m){
			printf("%d\n",cnt=c[x]);
			FO(i,x,n-1) c[i]=c[i+1];
			c[n]=cnt;
		}else{
			if(d[x]==-1){
				d[x]=++cnt1;
				FO(i,1,m-1) e[cnt1][i]=(x-1)*m+i;
			}
			now=d[x];printf("%d\n",cnt=e[now][y]);
			FO(i,y,m-2) e[now][i]=e[now][i+1];
			e[now][m-1]=c[x];
			FO(i,x,n-1) c[i]=c[i+1];
			c[n]=cnt;
		}
	}
}

int T[300005*2*4];

#define LS x<<1
#define RS x<<1|1
#define MID (l+r>>1)
int cnt1,ccnt,x[300005],y[300005];
LL b[300005*3];
void build(int x,int l,int r){
	if (l==r) { T[x]=l<=cnt1?1:0; return ; }
	build(LS,l,MID);build(RS,MID+1,r);
	T[x]=T[LS]+T[RS];
}
void update(int x,int l,int r,int pos,int v){
	if (l==r) { T[x]=v; return ; }
	if (pos<=MID) update(LS,l,MID,pos,v);
	else update(RS,MID+1,r,pos,v);
	T[x]=T[LS]+T[RS];
}
int find(int x,int l,int r,int v){
	if (l==r) return l;
	if (T[LS]>=v) return find(LS,l,MID,v);
	else return find(RS,MID+1,r,v-T[LS]);
}

void doo(){
	int cnt=0;
	FO(i,1,m) b[++cnt]=i;
	FO(i,2,n) b[++cnt]=(LL)m*i;
	cnt1=cnt;cnt+=q;
	build(1,1,cnt);
	FO(i,1,q){
		ccnt=find(1,1,cnt,y[i]);
		printf("%lld\n",b[ccnt]);
		update(1,1,cnt,ccnt,0);
		update(1,1,cnt,++cnt1,1);
		b[cnt1]=b[ccnt];b[ccnt]=0;
	}
}

int main(void){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if (n<=1000&&m<=1000&&q<=500) { bf(); return 0; }
//	if (1){ bf1(); return 0; } 
	if (n<=50000&&m<=50000&&q<=500){ bf1(); return 0; } 
	bool flag=1;
	FO(i,1,q){ x[i]=read();y[i]=read(); flag=flag&&x[i]==1; }
	if(flag) doo(); else puts("GG");
	return 0;
}


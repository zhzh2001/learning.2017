#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll;
const int LEN = 1e5 + 5;
int i, j, k, n, m, s, t, ans, h, r, tot, src, tar;
int head[LEN];
bool flag[LEN];
struct edge {
	int vet, next;
} E[2000005];
struct node {
	int x, y, z;
} a[LEN];
void add(int u, int v) {
	E[++tot] = (edge){v, head[u]};
	head[u] = tot;
}
void join(int u, int v) {
	add(u, v);
	add(v, u);
}
bool dfs(int u) {
	flag[u] = true;
	if (u == tar) {
		return true;
	}
	for (int e = head[u]; e != -1; e = E[e].next) {
		int v = E[e].vet;
		if (flag[v]) {
			continue;
		}
		bool t = dfs(v);
		if (t) {
			return true;
		}
	}
	return false;
}
bool check1(const node &x) {
	if (x.z - r <= 0) {
		return true;
	} else {
		return false;
	}
}
bool check2(const node &x) {
	if (x.z + r >= h) {
		return true;
	} else {
		return false;
	}
}
ll sqr(ll x) {
	return x * x;
}
bool check(const node &x, const node &y) {
	if (sqr(x.x - y.x) + sqr(x.y - y.y) + sqr(x.z - y.z) <= 4LL * sqr(r)) {
		return true;
	} else {
		return false;
	}
}
int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--) {
		scanf("%d %d %d", &n, &h, &r);
		tot = -1;
		src = n + 1;
		tar = n + 2;
		memset(head, -1, sizeof(head));
		memset(flag, false, sizeof(flag));
		for (int i = 1; i <= n; i++) {
			scanf("%d %d %d", &a[i].x, &a[i].y, &a[i].z);
			if (check1(a[i])) {
				join(src, i);
			}
			if (check2(a[i])) {
				join(i, tar);
			}
		}
		for (int i = 1; i < n; i++) {
			for (int j = i + 1; j <= n; j++) {
				if (check(a[i], a[j])) {
					join(i, j);
				}
			}
		}
		if (dfs(src)) {
			puts("Yes");
		} else {
			puts("No");
		}
	}
	return 0;
}
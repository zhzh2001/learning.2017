#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
const int oo = 1e8;
int U = CLOCKS_PER_SEC;
int i, j, k, n, m, s, t, ans, tot, cnt, now, yz;
int a[15][15];
int dep[1005];
int root, all;
struct node {
	int x, y, l;
} c[10005];
int head[105], fa[105], du[105];
struct edge {
	int vet, next, len;
} E[1005];
void add(int u, int v, int c) {
	E[++tot] = (edge){v, head[u], c};
	head[u] = tot;
}
namespace D1 {
	void dfs(int s, int cost) {
		if (cost >= ans) {
			return;
		}
		if (s == all) {
			ans = cost;
			return;
		}
		for (int u = 1; u <= n; u++) {
			if ((1 << (u - 1)) & s) {
				continue;
			}
			for (int e = head[u]; e != -1; e = E[e].next) {
				int v = E[e].vet;
				if ((1 << (v - 1)) & s) {
					dep[u] = dep[v] + 1;
					dfs(s | (1 << (u - 1)), cost + dep[u] * E[e].len);
				}
			}
		}
	}
	void DO() {
		tot = -1;
		for (int i = 1; i <= n; i++) {
			head[i] = -1;
		}
		for (int i = 1; i <= cnt; i++) {
			add(c[i].x, c[i].y, c[i].l);
			add(c[i].y, c[i].x, c[i].l);
		}
		all = (1 << n) - 1;
		for (int i = 1; i <= n; i++) {
			root = i;
			dep[root] = 0;
			dfs(1 << (i - 1), 0);
		}
		printf("%d\n", ans);
	}
}
namespace D2 {
	int nowall;
	int Cost(int s) {
		int ret = 0;
		for (int i = 1; i <= n; i++) {
			if (((1 << (i - 1)) & s) == 0) {
				int t = oo;
				for (int e = head[i]; e != -1; e = E[e].next) {
					int v = E[e].vet;
					if ((1 << (v - 1)) & s) {
						t = min(t, E[e].len * (dep[v] + 1));
					}
				}
				ret += t;
			}
		}
		return ret;
	}
	void dfs(int s, int cost, int num) {
		ans = min(ans, cost + Cost(s));
		if (cost >= ans) {
			return;
		}
		if (num >= 5) {
			return;
		}
		for (int u = 1; u <= n; u++) {
			if ((1 << (u - 1)) & s) {
				continue;
			}
			for (int e = head[u]; e != -1; e = E[e].next) {
				int v = E[e].vet;
				if ((1 << (v - 1)) & s) {
					dep[u] = dep[v] + 1;
					dfs(s | (1 << (u - 1)), cost + dep[u] * E[e].len, num + 1);
				}
			}
		}
	}
	void DO() {
		tot = -1;
		for (int i = 1; i <= n; i++) {
			head[i] = -1;
		}
		for (int i = 1; i <= cnt; i++) {
			add(c[i].x, c[i].y, c[i].l);
			add(c[i].y, c[i].x, c[i].l);
		}
		all = (1 << n) - 1;
		for (int i = 1; i <= n; i++) {
			root = i;
			dep[root] = 0;
			dfs(1 << (i - 1), 0, 1);
		}
		printf("%d\n", ans);
	}
}
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= m; i++) {
		int x, y, z;
		scanf("%d %d %d", &x, &y, &z);
		if (x == y) {
			continue;
		}
		if (a[x][y] == 0) {
			a[x][y] = a[y][x] = z;
		} else {
			if (z < a[x][y]) {
				a[x][y] = a[y][x] = z;
			}
		}
	}
	for (int i = 1; i < n; i++) {
		for (int j = i + 1; j <= n; j++) {
			if (a[i][j] > 0) {
				c[++cnt] = (node){i, j, a[i][j]};
			}
		}
	}
	ans = oo;
	if (n <= 8) {
		D1 :: DO();
	} else {
    	D2 :: DO();
	}
	return 0;
}
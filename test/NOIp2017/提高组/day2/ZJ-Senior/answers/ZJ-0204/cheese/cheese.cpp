#include <cstdio>
#include <cstring>
#include <algorithm>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;
typedef long long ll;
const int N = 1005;

int T;
ll n, h, r;
bool vis[N];

struct point
{
	ll x, y, z;
}P[N];

bool check(int i, int j, ll R)
{
	if(i > j) swap(i, j);
	if(i == 0 && j == n + 1) return false;
	else if(i == 0) return R >= P[j].z;
	else if(j == n + 1) return R >= h - P[i].z;
	else return 4 * R * R >= (P[i].x - P[j].x) * (P[i].x - P[j].x) +
		(P[i].y - P[j].y) * (P[i].y - P[j].y) + (P[i].z - P[j].z) * (P[i].z - P[j].z);
}

void dfs(int x)
{
	vis[x] = true;
	rep(i, 1, n + 1)
		if(!vis[i] && check(x, i, r)) dfs(i);
}

void init()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d", &T);
	while(T--)
	{
		scanf("%lld%lld%lld", &n, &h, &r);
		rep(i, 1, n)
			scanf("%lld%lld%lld",  &P[i].x, &P[i].y, &P[i].z);
		memset(vis, false, sizeof(vis));
		dfs(0);
		if(vis[n + 1]) puts("Yes");
		else puts("No");
	}
	return 0;
}

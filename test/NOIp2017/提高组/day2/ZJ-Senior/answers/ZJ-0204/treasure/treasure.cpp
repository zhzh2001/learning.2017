#include <cstdio>
#include <cstring>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;
const int N = 5000;
const int INF = 0x3f3f3f3f;

int n, m;
int w[20][20], dis[20], Q[100];

int bfs(int u)
{
	int f = 1, l = 1, ans = 0;
	Q[1] = u;
	while(f <= l)
	{
		int rt = Q[f++];
		rep(i, 1, n) if(w[rt][i] != -1 && dis[i] == INF)
		{
			dis[i] = dis[rt] + 1;
			ans += w[rt][i] * dis[rt];
			Q[++l] = i;
		}
	}
	return ans;
}

template <typename T>
T min(T a, T b)
{return a < b ? a : b;}

void init()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
}

int main()
{
	init();
	scanf("%d%d", &n, &m);
	int val = 0;
	bool flag = true;
	memset(w, -1, sizeof(w));
	rep(i, 1, m)
	{
		int a, b, c;
		scanf("%d%d%d", &a, &b, &c);
		w[a][b] = w[b][a] = c;
		if(!val) val = c;
		else if(val != c) flag = false;
	}
	if(flag)
	{
		int fans = INF;
		rep(i, 1, n)
		{
			memset(dis, 0x3f, sizeof(dis));
			dis[i] = 1;
			fans = min(bfs(i), fans);
		}
		printf("%d\n", fans);
	}
	else
	{
		printf("0\n");
	}
	return 0;
}

#include <bits/stdc++.h>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;

int main()
{
	freopen("phalanx.in", "w", stdout);
	srand(time(NULL));
	const int n = 1000, m = 999, q = 1000;
	printf("%d %d %d\n", n, m, q);
	rep(i, 1, q)
		printf("1 %d\n", rand() % m + 1);
	return 0;
}

#include <cstdio>
#define rep(i, a, b) for(int i = (a); i <= (b); i++)
using namespace std;
const int N = 1005;
const int M = 300005;
typedef long long ll;

int n, m, q;
int map[N][N];
ll a[M << 3];

void init()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
}

struct que
{
	int x, y;
}Q[M];

namespace sgt
{
	int sum[M << 4];
	void build(int rt, int l, int r)
	{
		int lc = rt << 1, rc = lc + 1, mid = (l + r >> 1);
		if(l == r)
		{
			sum[rt] = l < (n + m);
			return;
		}
		build(lc, l, mid);
		build(rc, mid + 1, r);
		sum[rt] = sum[lc] + sum[rc];
	}
	
	int query(int rt, int l, int r, int rnk)
	{
		int lc = rt << 1, rc = lc + 1, mid = (l + r >> 1);
		sum[rt]--;
		if(l == r) return l;
		if(rnk <= sum[lc]) return query(lc, l, mid, rnk);
		else return query(rc, mid + 1, r, rnk - sum[lc]);
	}
	
	void add(int rt, int l, int r, int rnk)
	{
		int lc = rt << 1, rc = lc + 1, mid = (l + r >> 1);
		sum[rt]++;
		if(l == r) return;
		if(rnk <= mid) add(lc, l, mid, rnk);
		else add(rc, mid + 1, r, rnk);
	}
}
using namespace sgt;

int main()
{
	init();
	scanf("%d%d%d", &n, &m, &q);
	bool flag = true;
	rep(i, 1, q) scanf("%d%d", &Q[i].x, &Q[i].y), flag &= (Q[i].x == 1);
	if(n <= 1000 && m <= 1000)
	{
		map[0][1] = -m + 1;
		rep(i, 1, n)
		{
			map[i][1] = map[i - 1][1] + m;
			rep(j, 2, m) map[i][j] = map[i][j - 1] + 1;
		}
		rep(i, 1, q)
		{
			int x, y, t;
			x = Q[i].x, y = Q[i].y;
			t = map[x][y];
			printf("%d\n", t);
			rep(j, y, m) map[x][j] = map[x][j + 1];
			rep(k, x, n) map[k][m] = map[k + 1][m];
			map[n][m] = t;
		}
	}
	else if(flag)
	{
		rep(i, 1, m) a[i] = i;
		ll id = 2 * m;
		rep(j, 2, n) a[n + j - 1] = id, id += m;
		int len = n + m - 1 + q, cur = n + m - 1;
		build(1, 1, len);
		rep(i, 1, q)
		{
			ll t = a[query(1, 1, len, Q[i].y)];
			printf("%lld\n", t);
			a[++cur] = t;
			add(1, 1, len, cur);
		}
	}
	return 0;
}

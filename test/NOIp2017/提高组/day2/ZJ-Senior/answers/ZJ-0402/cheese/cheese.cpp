#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<climits>
#include<algorithm>
#include<cstring>
#include<string>
#include<vector>
#include<queue>
#include<map>

using namespace std;

struct data
{
	double x,y,z;
	data(double a=0,double b=0,double c=0):x(a),y(b),z(c){}
}a[1010];
int T,n;double h,r;
vector<int> b[1010];
const double eps=1e-9;
bool v[1010];

double Sqr(double x){return x*x;}

double Dist(data x,data y)
{
	return sqrt(Sqr(x.x-y.x)+Sqr(x.y-y.y)+Sqr(x.z-y.z));
}

bool check()
{
	queue<int> q;
	memset(v,0,sizeof(v));
	q.push(0);v[0]=1;
	while (!q.empty())
	{
		int x=q.front();q.pop();
		if (x==n+1) return true;
		for (int i=0;i<b[x].size();i++)
			if (!v[b[x][i]]) q.push(b[x][i]),v[b[x][i]]=1;
	}
	return false;
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%lf%lf",&n,&h,&r);
		for (int i=1;i<=n;i++)
			scanf("%lf%lf%lf",&a[i].x,&a[i].y,&a[i].z),b[i].clear();
		b[0].clear();b[n+1].clear();
		double x;
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			{
				x=Dist(a[i],a[j]);
				if (x<2.0*r-eps||abs(x-2.0*r)<eps) 
					b[i].push_back(j),b[j].push_back(i);
			}
		for (int i=1;i<=n;i++)
		{
			if (a[i].z<r-eps||abs(a[i].z-r)<eps) b[0].push_back(i);
			if (a[i].z+r>h+eps||abs(a[i].z+r-h)<eps) b[i].push_back(n+1);
		}
		if (check()) puts("Yes");else puts("No");
	}
	return 0;
}

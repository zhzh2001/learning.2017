#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cmath>
#include<climits>
#include<algorithm>
#include<cstring>
#include<string>
#include<vector>
#include<queue>
#include<map>
#define LL long long

using namespace std;

int n,m;
struct data
{
	int x,y;LL z;
}a[1010];
LL f[4100][20],inf=600000000;
bool v[4100];

LL solve(int x)
{
	for (int i=0;i<=((1<<n)-1);i++)
	{
		f[i][0]=inf;
		for (int j=1;j<=n;j++)
			f[i][j]=-1;
	}
	memset(v,0,sizeof(v));
	f[1<<(x-1)][0]=0,f[1<<(x-1)][x]=1,v[1<<(x-1)]=1;
	queue<int>q;q.push(1<<(x-1));
	while(!q.empty())
	{
		int y=q.front();q.pop();LL z;
		for (int i=1;i<=m;i++)
			if (f[y][a[i].x]>0&&f[y][a[i].y]<0)
			{
				z=a[i].z*f[y][a[i].x];
				if (z+f[y][0]<f[y|(1<<(a[i].y-1))][0])
				{
					f[y|(1<<(a[i].y-1))][0]=z+f[y][0];
//		printf("%d %lld\n",a[i].y,f[y|(1<<(a[i].y-1))][0]);
					for (int j=1;j<=n;j++)
						f[y|(1<<(a[i].y-1))][j]=f[y][j];
					f[y|(1<<(a[i].y-1))][a[i].y]=f[y|(1<<(a[i].y-1))][a[i].x]+1;
					if (!v[y|(1<<(a[i].y-1))]) q.push(y|(1<<(a[i].y-1))),v[y|(1<<(a[i].y-1))]=1;
				}
			}
			else if (f[y][a[i].x]<0&&f[y][a[i].y]>0)
			{
				z=a[i].z*f[y][a[i].y];
				if (z+f[y][0]<f[y|(1<<(a[i].x-1))][0])
				{
					f[y|(1<<(a[i].x-1))][0]=z+f[y][0];
//		printf("%d %lld\n",a[i].x,f[y|(1<<(a[i].x-1))][0]);
					for (int j=1;j<=n;j++)
						f[y|(1<<(a[i].x-1))][j]=f[y][j];
					f[y|(1<<(a[i].x-1))][a[i].x]=f[y|(1<<(a[i].x-1))][a[i].y]+1;
					if (!v[y|(1<<(a[i].x-1))]) q.push(y|(1<<(a[i].x-1))),v[y|(1<<(a[i].x-1))]=1;
				}
			}
		v[y]=0;
	}
	return f[(1<<n)-1][0];
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		scanf("%d%d%lld",&a[i].x,&a[i].y,&a[i].z);
	LL ans=solve(1);
	for (int i=2;i<=n;i++)
		ans=min(ans,solve(i));
	printf("%lld\n",ans);
	return 0;
}

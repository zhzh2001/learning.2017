#include<iostream>
#include<cstdio>
using namespace std;
inline int read(){
	char ch=getchar(); int x=0; bool pos=1;
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
inline void write(long long x){
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
const int maxq=505,maxn=300005;
int n,m,q,x[maxq],y[maxq];
inline long long ask(int xx,int yy,int id){
	for(int i=id;i;i--){
		if(yy==m){
			if(xx==n){
				xx=x[i]; yy=y[i];
			}else
			if(xx>=x[i])xx++;
		}else
		if(xx==x[i]&&yy>=y[i])yy++;
	}
	return (long long)m*(xx-1)+yy;
}
long long a[maxn<<1],que[maxn<<1];
int tree[maxn<<3];
int query(int l,int r,int s,int nod){
	if(l==r)return r;
	int mid=(l+r)>>1;
	if(tree[nod<<1]>=s)return query(l,mid,s,nod<<1); else return query(mid+1,r,s-tree[nod<<1],nod<<1|1);
}
void insert(int l,int r,int i,int s,int nod){
	if(l==r){
		tree[nod]+=s; return;
	}
	int mid=(l+r)>>1;
	if(i<=mid)insert(l,mid,i,s,nod<<1);
	else insert(mid+1,r,i,s,nod<<1|1);
	tree[nod]=tree[nod<<1]+tree[nod<<1|1];
}
void build(int l,int r,int nod){
	if(l==r){
		if(l<=m)tree[nod]=1; return;
	}
	int mid=(l+r)>>1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	tree[nod]=tree[nod<<1]+tree[nod<<1|1];
}
int main(){
	freopen("phalanx.in","r",stdin); freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if(q<=500){
		for(int i=1;i<=q;i++){
			x[i]=read(); y[i]=read();
			write(ask(x[i],y[i],i-1)); puts("");
		}
	}else{
		int sum=m+q,l=0,r=0;
		build(1,sum,1);
		for(int i=2;i<=n;i++)que[++r]=(long long)i*m;
		for(int i=1;i<=m;i++)a[i]=i;
		for(int i=1;i<=q;i++){
			x[i]=read(); y[i]=read();
			int t=query(1,sum,y[i],1);
			insert(1,sum,t,-1,1);
			insert(1,sum,i+m,1,1);
			que[++r]=a[t]; a[i+m]=que[++l];
			write(a[t]); puts(""); 
		}
	}
}

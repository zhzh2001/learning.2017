#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=1005;
int s,t,flag,edge[N][N],vis[N],T,n,h,r,x[N],y[N],z[N];
void dfs(int p){
	if(p==t||flag){
		flag=1; return;
	}
	if(vis[p])return; else vis[p]=1;
	for(int i=1;i<=t;i++)if(p!=i&&edge[p][i])dfs(i);
}
int main(){
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r); long long zs=(long long)r*r; flag=0; s=n+1; t=n+2;
		for(int i=1;i<=n;i++)scanf("%d%d%d",&x[i],&y[i],&z[i]); memset(vis,0,sizeof(vis)); memset(edge,0,sizeof(edge));
		for(int i=1;i<=n;i++)if((long long)z[i]*z[i]<=zs)edge[s][i]=1;
		for(int i=1;i<=n;i++)if((long long)(h-z[i])*(h-z[i])<=zs)edge[i][t]=1;
		unsigned long long zz=zs; zz<<=2;
		for(int i=1;i<=n;i++)for(int j=1;j<=n;j++){
			unsigned long long a=(long long)(x[i]-x[j])*(x[i]-x[j]),b=(long long)(y[i]-y[j])*(y[i]-y[j]),c=(long long)(z[i]-z[j])*(z[i]-z[j]);
			if(a+b+c<=zz)edge[i][j]=1;
		}
		dfs(s);
		if(flag)puts("Yes"); else puts("No");
	}
	return 0;
}

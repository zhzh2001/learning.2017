#include<iostream>
#include<cstdio>
using namespace std;
const int N=20;
int ans=1e9,n,vis[N],que[N][N],edge[N][N],m;
void dfs(int p,int sum){
	if(sum>ans)return;
	if(p==n+1){
		ans=sum; return;
	}
	int lastmin=1e9;
	for(int i=1;i<=n;i++)if(!vis[i]){
		for(int j=1;j<=n&&que[j][0];j++){
			int mmin=edge[que[j][1]][i];
			for(int k=2;k<=que[j][0];k++){
				mmin=min(mmin,edge[que[j][k]][i]); 
			}
			if(mmin==1e9||mmin*j>=lastmin)continue;
			mmin*=j; 
			que[j+1][++que[j+1][0]]=i; vis[i]=1;
			dfs(p+1,sum+mmin); vis[i]=0; que[j+1][0]--;
			lastmin=min(lastmin,mmin);
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)edge[i][j]=1e9;
	while(m--){
		int s1,s2,s3;
		scanf("%d%d%d",&s1,&s2,&s3);
		edge[s1][s2]=edge[s2][s1]=min(edge[s1][s2],s3);
	}
	que[1][0]=1;
	for(int i=1;i<=n;i++){que[1][1]=i; vis[i]=1; dfs(2,0); vis[i]=0;}
	cout<<ans<<endl;
	return 0;
}

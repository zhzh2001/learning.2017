#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#define INF 1008208820

using namespace std;

struct kk{
	int x,y;
};

int read()
{
	int x=0;
	char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x;
}

int n,m,ans,maxx=INF;
int a[13][13];
int f[13];
bool h[13];
int b[13];

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();
	m=read();
	for (int i=1;i<=m;++i){
		int x,y,z;
		x=read();y=read();z=read();
		a[x][y]=z;
		a[y][x]=z;
	}
	for (int i=1;i<=n;++i){
		for (int j=1;j<=n;++j)
			if (a[i][j]!=0) f[j]=a[i][j];
				else f[j]=INF;
		memset(h,true,sizeof(h));
		h[i]=false;
		for (int j=1;j<=n;++j)
			b[j]=1;
		b[i]=0;
		int p,o;
		ans=0;
		for (int kkk=1;kkk<n;++kkk){
			o=INF;
		for (int j=1;j<=n;++j)
			if (h[j]) if (f[j]*b[j]<o) o=b[j]*f[j],p=j;
		h[p]=false;
		ans+=o/b[p];
		for (int j=1;j<=n;++j)
			if (a[j][p]!=0&&h[j]) if (f[j]>(b[p]+1)*a[j][p])
				f[j]=(b[p]+1)*a[j][p],b[j]=b[p]+1;
		}
		if (ans<maxx) maxx=ans;
	} 
	printf("%d",maxx);
	return 0;
}

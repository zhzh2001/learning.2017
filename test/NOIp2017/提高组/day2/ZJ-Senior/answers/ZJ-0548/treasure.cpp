#include<bits/stdc++.h>
using namespace std;
long long n,m,a[20][20],dp[13][5000],j,k,l;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)a[i][j]=2147483647/2;
	for(int i=1;i<=m;i++)
	{
		scanf("%d %d %d",&j,&k,&l);
		a[j][k]=min(a[j][k],l); a[k][j]=a[j][k];
	}
	for(int i=1;i<=(1<<n)-1;i++)for(int j=1;j<=n;j++)dp[j][i]=2147483647/2;
	for(int i=1;i<=n;i++)dp[1][1<<(i-1)]=0;
	for(int i=2;i<=n;i++)
	for(int j=1;j<=(1<<n)-1;j++)
	for(int k=j;k>=1;k=(k-1)&j)
	if((dp[i-1][k]<2147483647/2)and((j&k)==k))
	{
		long long tot=0;
		for(int ii=1;ii<=n;ii++)if((j-k)&(1<<(ii-1)))
		{
			long long mi=2147483647/2;
			for(int jj=1;jj<=n;jj++)if(k&(1<<(jj-1)))mi=min(mi,a[jj][ii]);
			tot=tot+mi;
		}
		if(tot<2147483647/2)dp[i][j]=min(dp[i][j],dp[i-1][k]+(i-1)*tot);
	}
	long long ans=2147483647/2; for(int i=1;i<=n;i++)ans=min(ans,dp[i][(1<<n)-1]);
	printf("%lld\n",ans);
}
		

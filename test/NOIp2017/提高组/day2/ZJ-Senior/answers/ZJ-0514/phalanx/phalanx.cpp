#include<cstdio>
using namespace std;
const int maxn=1010;
int a[maxn][maxn],n,m,q;
void read(int &x)
{
	char ch=getchar();
	for (x=0;ch<'0' || ch>'9';ch=getchar());
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-48;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int i,j,tem,x,y;
	for (i=1,read(n),read(m),read(q);i<=n;++i)
	  for (j=1;j<=m;++j) a[i][j]=(i-1)*m+j;
	for (;q--;a[n][m]=tem)
	  {
	  	for (read(x),read(y),printf("%d\n",tem=a[x][y]),j=y;j<m;++j) a[x][j]=a[x][j+1];
	  	for (i=x;i<n;++i) a[i][m]=a[i+1][m];
	  }
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
using namespace std;
const int maxn=15,maxe=70,inf=1061109567;
struct rec
         {
         	int x,y,z;
		 }a[maxe];
int dep[maxn],fther[maxn],g[maxn][maxn],id[maxn],lnk[maxn],nxt[maxn<<1],sn[maxn<<1],w[maxn<<1],n,m,e,sum,tot,ans=inf;
void read(int &x)
{
	char ch=getchar();
	for (x=0;ch<'0' || ch>'9';ch=getchar());
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-48;
}
int qcomp(const rec &x,const rec &y)
{
	return x.z<y.z;
}
int getfather(int x)
{
	return fther[x]==x?x:fther[x]=getfather(fther[x]);
}
void add(int x,int y,int z)
{
	w[++tot]=z;sn[tot]=y;nxt[tot]=lnk[x];lnk[x]=tot;
}
void cal(int x)
{
	int j=lnk[x];
	for (;j;j=nxt[j])
	  if (dep[sn[j]]==0)
	      {
	      	if ((sum+=w[j]*dep[x])>=ans) return;
	      	dep[sn[j]]=dep[x]+1;cal(sn[j]);
		  }
}
void dfs(int x,int step)
{
	int i;
	if (step==n-1)
	    {
	    	for (i=1;i<=n;++i) fther[i]=i;
	    	memset(lnk,0,sizeof(lnk));
	    	for (i=1,tot=0;i<n;++i) fther[getfather(a[id[i]].x)]=getfather(a[id[i]].y),add(a[id[i]].x,a[id[i]].y,a[id[i]].z),add(a[id[i]].y,a[id[i]].x,a[id[i]].z);
	    	int x;
	    	for (i=2,x=getfather(1);i<=n;++i)
	    	  if (getfather(i)!=x) return;
	    	for (i=1;i<=n;++i)
	    	  {
	    	  	memset(dep,0,sizeof(dep));
	    	  	dep[i]=1;sum=0;cal(i);ans=min(ans,sum);
			  }
			if (n>8) {printf("%d\n",ans);fclose(stdin);fclose(stdout);exit(0);}
			return;
		}
	for (i=x;i<=e-n+step+2;++i) id[step+1]=i,dfs(i+1,step+1);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j,x,y,z;
	memset(g,63,sizeof(g));
	for (i=1,read(n),read(m);i<=m;++i) read(x),read(y),read(z),g[y][x]=g[x][y]=min(g[x][y],z);
	for (i=1;i<n;++i)
	  for (j=i+1;j<=n;++j)
	    if (g[i][j]<inf) a[++e].x=i,a[e].y=j,a[e].z=g[i][j];
	sort(a+1,a+e+1,qcomp);dfs(1,0);printf("%d\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

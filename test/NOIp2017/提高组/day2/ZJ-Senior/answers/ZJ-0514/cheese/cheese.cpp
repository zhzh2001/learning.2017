#include<cstdio>
using namespace std;
const int maxn=1010;
struct rec
         {
         	int x,y,z;
		 }a[maxn];
int fther[maxn],T,n,h,r;
void read(int &x)
{
	int k=1;
	char ch=getchar();
	for (x=0;ch<'0' || ch>'9';ch=getchar())
	  if (ch=='-') k=-1;
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-48;
	x*=k;
}
int getfather(int x)
{
	return fther[x]==x?x:fther[x]=getfather(fther[x]);
}
void merge(int x,int y)
{
	if ((x=getfather(x))!=(y=getfather(y))) fther[x]=y;
}
long long f(int x)
{
	return (long long)x*x;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int i,j;
	for (read(T);T--;)
	  {
	  	for (i=0,read(n),read(h),read(r);i<=n+1;++i) fther[i]=i;
	  	for (i=1;i<=n;++i)
	  	  {
	  	  	read(a[i].x);read(a[i].y);read(a[i].z);
	  	  	if (a[i].z>=-r && a[i].z<=r) merge(0,i);
	  	  	if (a[i].z-h>=-r && a[i].z-h<=r) merge(i,n+1);
	  	  }
	  	for (i=1;i<n;++i)
	  	  {
	  	  	for (j=i+1;j<=n;++j)
	  	  	  if (f(a[i].x-a[j].x)+f(a[i].y-a[j].y)<=4*f(r)-f(a[i].z-a[j].z)) merge(i,j);
	  	  	if (getfather(0)==getfather(n+1)) break;
	  	  }
	  	puts(getfather(0)==getfather(n+1)?"Yes":"No");
	  }
	fclose(stdin);fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define MAX 15
#define INF 0x3f3f3f3f
using namespace std;
int n, m, u, v, w, y, tot, ans=INF, k[MAX], dp[4100], dis[MAX][MAX];

int read()
{
	int x=0,f=1;
	char c=getchar();
	while(!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

void Work(int s)
{
	int x=1<<(s-1);
	for (int i = 1;i<=n;i++)
		if (i!=s){
			for (int j = 1;j<tot;j++){
				if (dp[j]==INF) continue;
				if (!(x&j)) continue;
				for (int q = 1;q<=n;q++)
					if (k[q]<=n){
					if (dis[i][q]!=INF&&(j&(1<<(q-1)))){
						if (dp[j|(1<<(i-1))]>dp[j]+dis[i][q]*(k[q]+1)){
							dp[j|(1<<(i-1))]=dp[j]+dis[i][q]*(k[q]+1);
							if (k[i]>k[q]+1) k[i]=k[q]+1;
							Work(i);
						}
					}
				}
			}
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for (int i = 0;i<=n+1;i++)
		for (int j = 0;j<=n+1;j++)
			dis[i][j]=INF;
	for (int i = 1;i<=m;i++){
		u=read(),v=read(),w=read();
		if (w<dis[u][v]){
			dis[u][v]=w;
			dis[v][u]=w;
		}		
	}
	tot=1<<n;
	for (int s = 1;s<=n;s++){
		for (int i = 1;i<=n;i++)
			k[i]=n+1;
		memset(dp,INF,sizeof(dp));
		
		dp[1<<(s-1)]=0;
		k[s]=0;
		Work(s);
		if (dp[tot-1]<ans){
			ans=dp[tot-1];
		}
		
	}
	cout << ans <<'\n';
	fclose(stdin),fclose(stdout);
	return 0;
}

#include <algorithm> 
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define MAX 2010
#define LL long long
using namespace std;
int T, n, h, r, l, d;
struct DATA{
	int x, y, z;
}a[MAX];
LL Now, rr, dis;
bool flag, vis[MAX], yes[MAX];

bool cmp(DATA aa,DATA bb)
{
	return aa.z<bb.z;
}

int read()
{
	int x=0,f=1;
	char c=getchar();
	while(!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

bool init()
{
	l=1;
	Now=a[l].z-r;
	while(Now<=0){
		vis[l]=true;
		l++;
		Now=a[l].z-r;
	}
	l--;
	if (!vis[1]) return true;
	d = n;
	Now=a[d].z+r;
	LL hh=h;
	while(Now>=hh){
		yes[d]=true;
		d--;
		Now=a[d].z+r;
	}
	if (!yes[n]) return true;
	return false;	
}

bool check(int i,int j)
{
	dis=(a[i].x-a[j].x)*(a[i].x-a[j].x);
	dis+=(a[i].y-a[j].y)*(a[i].y-a[j].y);
	dis+=(a[i].z-a[j].z)*(a[i].z-a[j].z);
	if (dis<=rr) return true;
	return false;	
}

void Work()
{
	for (int i = l+1;i<=n;i++){
		for (int j = i-1;j;j--){
			if (vis[j]&&check(i,j)){
				vis[i]=true;
				break;
			}
		}
		if (yes[i]&&vis[i]){
			printf("Yes\n");
			return;			
		}
	}
	printf("No\n");
	return;	
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--)
	{
		memset(vis,0,sizeof(vis));
		memset(yes,0,sizeof(yes));
		
		n=read(),h=read(),r=read();
		for (int i = 1;i<=n;i++)
			a[i].x=read(),a[i].y=read(),a[i].z=read();
		sort(a+1,a+1+n,cmp);
		if (init()){
			printf("No\n");
			continue;
		}
		rr=(r*r)<<2;
		Work();
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#define MAX 6010
using namespace std;
int m, n, tmp, q, x, y, a[MAX][MAX];

int read()
{
	int x=0,f=1;
	char c=getchar();
	while(!isdigit(c)){
		if (c=='-') f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	for (int i = 1;i<=n;i++)
		for (int j = 1;j<=m;j++)
			a[i][j]=(i-1)*n+j;
	
	for (int i = 1;i<=q;i++){
		x=read(),y=read();
		cout << a[x][y] << '\n';
		tmp=a[x][y];
			for (int k = y;k<m;k++){
				a[x][k]=a[x][k+1];
			}
			for (int j = x;j<n;j++)
				a[j][m]=a[j+1][m];
		a[n][m]=tmp;
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

var i,j,k,n,m,s,Times,h,tot,t,w,v:longint;
    x,y,z,g:array[0..1007]of longint;
    head,b,next:array[0..2000007]of longint;
    p:array[0..1007]of boolean;
    ans:boolean; r:extended;
function dist:extended;
var dx,dy,dz:int64;
begin
  dx:=x[i]-x[j];
  dy:=y[i]-y[j];
  dz:=z[i]-z[j];
  if dx*dx+dy*dy+dz*dz=0 then exit(0);
  exit(sqrt(dx*dx+dy*dy+dz*dz));
end;
procedure add(x,y:longint);
begin
  inc(tot);
  b[tot]:=y;
  next[tot]:=head[x];
  head[x]:=tot;
end;
begin
  assign(input,'cheese.in');
  reset(input);
  assign(output,'cheese.out');
  rewrite(output);

  readln(Times);
  for k:=1 to Times do
   begin
   tot:=0;
   fillchar(head,sizeof(head),0);
   fillchar(next,sizeof(next),0);
   fillchar(b,sizeof(b),0);
   fillchar(g,sizeof(g),0);
   fillchar(p,sizeof(p),false);
   readln(n,h,r); s:=0;
   for i:=1 to n do readln(x[i],y[i],z[i]);
   for i:=1 to n-1 do
    for j:=i+1 to n do
     if dist<=2*r then
      begin add(i,j); add(j,i); end;
   for i:=1 to n do
    begin
    if z[i]<=r then add(0,i);
    if (h-z[i])<=r then add(i,n+1);
    end;
   t:=0; w:=1; g[1]:=0; p[0]:=true; ans:=false;
   while t<=w do
     begin
     inc(t);
     if g[t]=n+1 then begin ans:=true; break; end;
     i:=head[g[t]];
     while i<>0 do
       begin
       v:=b[i];
       if not p[v] then begin inc(w); g[w]:=v; p[v]:=true; end;
       i:=next[i];
       end;
     end;
   if ans then writeln('Yes') else writeln('No');
   end;

  close(input);
  close(output);
end.

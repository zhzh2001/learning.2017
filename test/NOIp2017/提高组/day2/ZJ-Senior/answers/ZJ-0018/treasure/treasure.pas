var i,j,k,n,m,x,y,z,s,oo,t,w,u,anst,ans,maxv,nn,new:longint;
    map,house:array[0..17,0..17]of longint;
    g,dis:array[0..17]of longint;
    p:array[0..17]of boolean;
    one:array[0..4096]of longint;
    f:array[0..17,0..4096]of longint;
function min(a,b:longint):longint;
begin
  if a<b then exit(a); exit(b);
end;
function mindis:longint;
var ii,jj,x,ans:longint; s:string;
begin
  x:=j; s:='';
  while x<>0 do
   begin
   if x mod 2=0 then s:=s+'0' else s:=s+'1';
   x:=x div 2;
   end;
  ans:=maxlongint;
  for ii:=1 to length(s) do
   if (s[ii]='1')and(map[ii][k]<>oo)and(house[u][ii]<>oo) then
    ans:=min(ans,house[u][ii]*map[ii][k]);
  exit(ans);
end;
begin
  assign(input,'treasure.in');
  reset(input);
  assign(output,'treasure.out');
  rewrite(output);

  readln(n,m);
  oo:=maxlongint;
  for i:=1 to n do
   for j:=1 to n do
    begin
    map[i][j]:=oo;
    house[i][j]:=oo;
    end;
  for i:=1 to m do
   begin
   readln(x,y,z);
   map[x][y]:=min(map[x][y],z);
   map[y][x]:=map[x][y];
   end;
  for i:=1 to n do
   begin
   fillchar(p,sizeof(p),false);
   t:=0; w:=1; g[1]:=i; dis[1]:=1; p[i]:=true;
   while t<=w do
     begin
     inc(t); u:=g[t];
     house[i][u]:=dis[t];
     for j:=1 to n do
      if (not p[j])and(map[u][j]<>oo) then
       begin inc(w); g[w]:=j; p[j]:=true; dis[w]:=dis[t]+1; end;
     end;
   end;

  fillchar(f,sizeof(f),$7f);
  nn:=1<<n;
  for i:=1 to nn-1 do
   begin
   x:=i;
   while x<>0 do
     begin
     if x mod 2=1 then inc(one[i]);
     x:=x div 2;
     end;
   end;

  for i:=0 to n-1 do f[i+1][1<<i]:=0;
  for u:=1 to n do
   for i:=1 to n-1 do
    for j:=1 to nn-1 do
     if one[j]=i then
      for k:=1 to n do
        begin
        new:=j or (1<<(k-1));
        if one[new]=i+1 then
          begin
          anst:=mindis;
          if anst<>maxlongint then
            f[u][new]:=min(f[u][new],f[u][j]+anst);
          end;
        end;

  ans:=maxlongint;
  for i:=1 to n do
   ans:=min(ans,f[i][nn-1]);
  writeln(ans);

  close(input);
  close(output);
end.

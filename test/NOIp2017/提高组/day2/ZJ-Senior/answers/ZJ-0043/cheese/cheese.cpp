#include <bits/stdc++.h>
#define U unsigned long long
using namespace std;
int n,p,q,f[1005],T; long long x[1005],y[1005],z[1005],r,h; U R;
U dis(int i,int j){
	return (U)((x[i]-x[j])*(x[i]-x[j]))+(U)((y[i]-y[j])*(y[i]-y[j]))+(U)((z[i]-z[j])*(z[i]-z[j]));
}
int find(int x){
	return f[x]=(f[x]!=x?find(f[x]):x);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%lld%lld",&n,&h,&r); R=4*r*r;
		for (int i=1;i<=n+2;++i) f[i]=i;
		for (int i=1;i<=n;++i) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=1;i<=n;++i)
		for (int j=1;j<i;++j)
		if (dis(i,j)<=R){
			p=find(i); q=find(j); f[p]=q;
		}
		for (int i=1;i<=n;++i){
			if (z[i]<=r){
				p=find(i); q=find(n+1); f[p]=q;
			}
			if (z[i]+r>=h){
				p=find(i); q=find(n+2); f[p]=q;
			}
		}
		p=find(n+1); q=find(n+2);
		if (p==q) puts("Yes"); else puts("No");
	}
	return 0;
}

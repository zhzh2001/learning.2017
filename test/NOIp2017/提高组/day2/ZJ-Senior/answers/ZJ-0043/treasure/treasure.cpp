#include <bits/stdc++.h>
using namespace std;
const int inf=100000000;
int n,m,a[14][14],A[14][4100][14],H[15],id[4100],sz[4100],x,y,z,ans,f[14][14][4100];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
	for (int j=1;j<=n;++j) a[i][j]=inf;
	for (int i=1;i<=m;++i){
		scanf("%d%d%d",&x,&y,&z);
		if (x==y) continue;
		a[x][y]=a[y][x]=min(a[x][y],z);
	}
	id[1]=H[1]=1;
	for (int i=2;i<=n+1;++i) H[i]=H[i-1]<<1,id[H[i]]=i;
	for (int i=1;i<H[n+1];++i) sz[i]=sz[i^(i&-i)]+1;
	for (int i=1;i<=n;++i)
	for (int j=1;j<=n;++j)
	for (int s=0;s<H[n+1];++s) f[i][j][s]=inf;
	
	for (int i=1;i<=n;++i)
	for (int j=1;j<=n;++j)
		f[i][j][H[i]]=0;
	for (int s=1;s<H[n+1];++s){
		for (int o=s;o;o^=o&-o)
		for (int j=1;j<=n;++j)
			if (a[id[o&-o]][j]!=inf)
			A[j][s][++A[j][s][0]]=id[o&-o];
	}
	for (int s=1;s<H[n+1];++s){
		if (id[s]) continue;
		for (int i=1;i<=n;++i)
		if (H[i]&s){
			int S=s^H[i];
			for (int j=1;j<=n-sz[S];++j){
				for (int o=S;o;(--o)&=S)
				if (f[i][j][s^o]!=inf)
					for (int K=1,k;K<=A[i][o][0];++K){
						k=A[i][o][K];
						f[i][j][s]=min(f[i][j][s],a[i][k]*j+f[k][j+1][o]+f[i][j][s^o]);
					}
			}
		}
	}
	ans=inf;
	for (int i=1;i<=n;++i) ans=min(ans,f[i][1][H[n+1]-1]);
	printf("%d\n",ans);
	return 0;
}

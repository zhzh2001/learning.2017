#include <bits/stdc++.h>
using namespace std;
#define LL long long 
const int N = 400000;
const int M = 2000000;
int lc[M], rc[M], lb[M], rb[M], siz[M], tot;
LL va[M];
int fa[M], V[M];
int X[N], Y[N];
vector <int> P[N], Q[N];
int n, m, q, id;
void rebuild(int n)
{
    for (int i = 1; i <= tot; ++ i)
        lc[i] = rc[i] = lb[i] = rb[i] = siz[i] = 0;
    tot = 1;
    lb[1] = 1; rb[1] = n; siz[1] = n;
}
void psdw(int t)
{
    if (!lc[t])
    {
        lc[t] = ++ tot;
        rc[t] = ++ tot;
        lb[lc[t]] = lb[t]; rb[lc[t]] = (lb[t] + rb[t]) / 2; 
        siz[lc[t]] = rb[lc[t]] - lb[lc[t]] + 1;
        lb[rc[t]] = (lb[t] + rb[t]) / 2 + 1; rb[rc[t]] = rb[t];
        siz[rc[t]] = rb[rc[t]] - lb[rc[t]] + 1;
    }
}
int query(int t, int v)
{
    if (lb[t] == rb[t])
    {
        siz[t] --;
        return lb[t];
    }
    psdw(t);
    siz[t] --;
    if (v <= siz[lc[t]]) return query(lc[t], v);
    else return query(rc[t], v - siz[lc[t]]);
}
void get_f(int t)
{
    if (va[t]) return;
    else 
    {
        get_f(fa[t]);
        va[t] = va[fa[t]];
    }
}
int main()
{
    freopen("phalanx.in", "r", stdin);
    freopen("phalanx.out", "w", stdout);
    scanf("%d%d%d", &n, &m, &q);
    id = q;
    for (int i = 1; i <= q; ++ i)
    {
        scanf("%d%d", &X[i], &Y[i]);
        if (Y[i] != m)
        {
            fa[i] = id + 1;
            P[X[i]].push_back(++ id); V[id] = Y[i];
            Q[X[i]].push_back(++ id);
            fa[id] = id + 1;
            P[n + 1].push_back(++ id); V[id] = X[i];
            Q[n + 1].push_back(++ id);
            fa[id] = i;
        }
        else 
        {
            fa[i] = id + 1;
            P[n + 1].push_back(++ id); V[id] = X[i];
            Q[n + 1].push_back(++ id);
            fa[id] = i;
        }
    }
    // cerr << id << endl;
    for (int i = 1; i <= n + 1; ++ i)
    {
        rebuild(((i == n + 1)? n: (m - 1)) + P[i].size());
        for (int j = 0; j < P[i].size(); ++ j)
        {
            int w = query(1, V[P[i][j]]);
            if (i == n + 1)
            {
                if (w <= n) va[P[i][j]] = 1ll * (w - 1) * m + m;
                else fa[P[i][j]] = Q[i][w - n - 1];
            }
            else 
            {
                if (w <= m - 1) va[P[i][j]] = 1ll * (i - 1) * m + w;
                else fa[P[i][j]] = Q[i][w - m];
            }
        }
    }
    for (int i = 1; i <= q; ++ i)
    {
        get_f(i);
        printf("%lld\n", va[i]);
    }
}

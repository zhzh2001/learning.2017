#include <bits/stdc++.h>
using namespace std;
int n, m, q;
long long V[10100][10100];
int main()
{
    cin >> n >> m >> q;
    for (int i = 1; i <= n; ++ i)
        for (int j = 1; j <= m; ++ j)
            V[i][j] = 1ll * (i - 1) * m + j;
    for (int i = 1; i <= q; ++ i)
    {
        int x, y;
        cin >> x >> y;
        int nw = V[x][y];
        cout << nw << endl;
        for (int i = y; i < m; ++ i) V[x][i] = V[x][i + 1];
        for (int i = x; i < n; ++ i) V[i][m] = V[i + 1][m];
        V[n][m] = nw;
    }
}

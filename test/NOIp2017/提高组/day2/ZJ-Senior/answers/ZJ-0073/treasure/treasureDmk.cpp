#include <bits/stdc++.h>
using namespace std;
const int I = 500000;
const int N = 20;
int fa[N];
int get_f(int t)
{
    return fa[t] = (fa[t] == t? t: get_f(fa[t]));
}
int main()
{
    srand(time(0) * clock());
    int n = 12, m = 1000 - n + 1;
    m += n - 1;
    cout << n << " " << m << endl;
    for (int i = 1; i <= n; ++ i) fa[i] = i;
    for (int i = 1; i < n; ++ i)
    {
        int a, b;
        do a = rand() % n + 1, b = rand() % n + 1;
        while (get_f(a) == get_f(b));
        fa[get_f(a)] = get_f(b);
        cout << a << " " << b << " " << I << endl;
    }
    for (int i = n; i <= m; ++ i)
    {
        int a = rand() % n + 1, b = rand() % n + 1;
        cout << a << " " << b << " " << I << endl;
    }
    
}

#include <bits/stdc++.h>
using namespace std;
#define int long long
int n, m;
const int N = 20;
const int INF = 1e9;
int M[N][N], V[N], ans;
void dfs(int t)
{
    if (t == n + 1)
    {
        int c = -1;
        for (int i = 1; i <= n; ++ i)
            if (V[i] == 0)
            {
                if (c != -1) return;
                else c = i;
            }
        if (c == -1) return;
        int res = 0;
        for (int i = 1; i <= n; ++ i) if (i != c)
        {
            int v = INF;
            for (int j = 1; j <= n; ++ j)
                if (V[i] == V[j] + 1)
                    v = min(v, M[i][j]);
            res += v * V[i];
        }
        ans = min(ans, res);
        return;
    }
    for (int i = 0; i < n; ++ i)
    {
        V[t] = i;
        dfs(t + 1);
    }
}
signed main()
{
    cin >> n >> m;
    for (int i = 1; i <= n; ++ i)
        for (int j = 1; j <= n; ++ j)
            M[i][j] = INF;
    for (int i = 1; i <= m; ++ i)
    {
        int a, b, c;
        cin >> a >> b >> c;
        M[a][b] = M[b][a] = min(M[a][b], c);
    }
    ans = INF;
    dfs(1);
    cout << ans << endl;
}

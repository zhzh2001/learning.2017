#include <bits/stdc++.h>
using namespace std;
int n, m;
const int N = 20;
const int P = 5000;
const int INF = 1e9;
int M[N][N], V[P], A[P][N], B[P][P], F[N][P], ans;
int main()
{
    freopen("treasure.in", "r", stdin);
    freopen("treasure.out", "w", stdout);
    ans = INF;
    cin >> n >> m;
    for (int i = 1; i <= n; ++ i)
        for (int j = 1; j <= n; ++ j)
            M[i][j] = INF;
    for (int i = 1; i <= m; ++ i)
    {
        int a, b, c;
        cin >> a >> b >> c;
        M[a][b] = M[b][a] = min(M[a][b], c);
    }
    for (int i = 1; i <= n; ++ i)
        V[1 << (i - 1)] = i;
    for (int i = 1; i < (1 << n); ++ i)
        for (int j = 1; j <= n; ++ j) if (!((1 << (j - 1)) & i))
        {
            A[i][j] = INF;
            for (int k = 1; k <= n; ++ k) if (((1 << (k - 1)) & i))
                A[i][j] = min(A[i][j], M[j][k]);
        }
    for (int i = 1; i < (1 << n); ++ i)
        for (int j = 1; j < (1 << n); ++ j)
            if ((i & j) == 0)
                B[i][j] = min(INF, B[i][j - (j & (-j))] + A[i][V[j & (-j)]]);
    for (int i = 1; i <= n; ++ i)
        for (int j = 0; j < (1 << n); ++ j)
            F[i][j] = INF;
    for (int i = 1; i <= n; ++ i)
        F[1][1 << (i - 1)] = 0;
    for (int i = 2; i <= n; ++ i)
    {
        for (int j = 1; j < (1 << n); ++ j)
            for (int k = j & (j - 1); k; k = j & (k - 1))
                if (1ll * (i - 1) * B[k][j ^ k] <= INF)
                    F[i][j] = min(F[i][j], F[i - 1][k] + (i - 1) * B[k][j ^ k]);
    }
    for (int i = 1; i <= n; ++ i)
        ans = min(ans, F[i][(1 << n) - 1]);
    cout << ans << endl;
}

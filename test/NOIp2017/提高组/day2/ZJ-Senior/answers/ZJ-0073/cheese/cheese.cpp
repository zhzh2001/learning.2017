#include <bits/stdc++.h>
using namespace std;
int T;
const int N = 1010;
int n, h, r;
int xi[N], yi[N], zi[N];    
int M[N][N], V[N];
void dfs(int t)
{
    V[t] = 1;
    for (int i = 1; i <= n + 2; ++ i)
        if (M[t][i] && !V[i])
            dfs(i);
}
int main()
{
    freopen("cheese.in", "r", stdin);
    freopen("cheese.out", "w", stdout);
    scanf("%d", &T);
    while (T --)
    {
        scanf("%d%d%d", &n, &h, &r);
        for (int i = 1; i <= n; ++ i)
            scanf("%d%d%d", &xi[i], &yi[i], &zi[i]);
        for (int i = 1; i <= n + 2; ++ i)
            for (int j = 1; j <= n + 2; ++ j)
                M[i][j] = 0;
        for (int i = 1; i <= n; ++ i)
            if (zi[i] - r <= 0)
                M[n + 1][i] = 1;
        for (int i = 1; i <= n; ++ i)
            if (zi[i] + r >= h)
                M[i][n + 2] = 1;
        for (int i = 1; i <= n; ++ i)
            for (int j = 1; j <= n; ++ j)
                if (1ll * (xi[i] - xi[j]) * (xi[i] - xi[j]) + 1ll * (yi[i] - yi[j]) * (yi[i] - yi[j])
                 <= 1ll * (2 * r) * (2 * r) - 1ll * (zi[i] - zi[j]) * (zi[i] - zi[j]))
                    M[i][j] = 1;
        for (int i = 1; i <= n + 2; ++ i) V[i] = 0;
        dfs(n + 1);
        if (V[n + 2]) puts("Yes"); else puts("No");
    }
    
}

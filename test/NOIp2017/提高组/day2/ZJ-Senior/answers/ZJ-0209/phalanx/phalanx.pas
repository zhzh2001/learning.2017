const maxn=10000;
var map:array[0..maxn,0..maxn]of longint;
  n,m,q,i,j,x,y,t:longint;

begin
  assign(input,'phalanx.in');
  assign(output,'phalanx.out');
  reset(input);
  rewrite(output);

  readln(n,m,q);
  for i:=1 to n do
    for j:=1 to m do
      map[i,j]:=(i-1)*m+j;
  for i:=1 to q do
    begin
      readln(x,y);
      writeln(map[x,y]);
      t:=map[x,y];
      for j:=y to m-1 do map[x,j]:=map[x,j+1];
      for j:=x to n-1 do map[j,m]:=map[j+1,m];
      map[n,m]:=t;
    end;

  close(input);
  close(output);
end.
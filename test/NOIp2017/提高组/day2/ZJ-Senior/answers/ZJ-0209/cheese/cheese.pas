const maxn=2000;
var map:array[0..maxn,0..maxn]of longint;
  x,y,z:array[0..maxn]of int64;
  vis:array[0..maxn] of boolean;
  q:array[0..maxn*10]of longint;
  t,n,h:longint;
  r:int64;

function dist(u,v:longint):int64;
var t:int64;
begin
  t:=(x[u]-x[v])*(x[u]-x[v])+(y[u]-y[v])*(y[u]-y[v])+(z[u]-z[v])*(z[u]-z[v]);
  exit(t)
end;

function bfs():boolean;
var i,j,now,l,r:longint;
begin
 fillchar(q,sizeof(q),0);
 fillchar(vis,sizeof(vis),false);
 q[1]:=0;vis[0]:=true;
 l:=1;r:=1;
 while l<=r do
   begin
     now:=q[l];
     for i:=1 to map[now,0] do
       if not vis[map[now,i]] then
         begin
           inc(r);
           q[r]:=map[now,i];
           vis[map[now,i]]:=true;
           if map[now,i]=n+1 then exit(true)
         end;
     inc(l);
   end;
 exit(false);
end;

procedure main();
var i,j,k,l:longint;
begin
  readln(t);
  for l:=1 to t do
    begin
      fillchar(map,sizeof(map),0);
      fillchar(x,sizeof(x),0);
      y:=x;z:=y;

      readln(n,h,r);
      for i:=1 to n do
        begin
          readln(x[i],y[i],z[i]);
          for j:=1 to i-1 do
            if dist(i,j)<=(4*r*r) then
              begin
                inc(map[i,0]);map[i,map[i,0]]:=j;
                inc(map[j,0]);map[j,map[j,0]]:=i;
              end;
        end;
      for i:=1 to n do
        if z[i]<=r then
          begin
            inc(map[0,0]);map[0,map[0,0]]:=i;
          end
        else if h-z[i]<=r then
          begin
            inc(map[i,0]);map[i,map[i,0]]:=n+1;
          end;

      if bfs() then writeln('Yes') else writeln('No');
    end;
end;

begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);

  main();

  close(input);
  close(output);
end.
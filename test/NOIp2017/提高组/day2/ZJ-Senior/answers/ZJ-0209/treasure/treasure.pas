const maxn=50000;
  max=5000000;
var f:array[0..max]of longint;
  map:array[0..12,0..12]of longint;
  dist:array[0..12]of longint;
  n,m:longint;

procedure init();
var i,j,u,v,w:longint;
begin
  readln(n,m);
  fillchar(map,sizeof(map),$7f);
  for i:=1 to m do
    begin
      readln(u,v,w);
      if w<map[u,v] then map[u,v]:=w;
      if w<map[v,u] then map[v,u]:=w;
    end;
end;


procedure dp();
var i,j,t,last,now,t1,t2:longint;
  tmp:array[0..12]of longint;
begin
  fillchar(f,sizeof(f),$7f);
  fillchar(dist,sizeof(dist),0);
  f[0]:=0;dist[0]:=0;
  for i:=1 to (1<<n)-1 do
    begin
      t:=i;
      if t=(t and -t) then
        begin
          dist[i]:=1;
          f[i]:=0;
          continue;
        end;
      tmp:=dist;
      while t<>0 do
        begin
          now:=t and -t;
          now:=round(ln(now)/ln(2))+1;
          for j:=0 to n-1 do
            if ((1<<j) and i<>0) and (j+1<>now) then
              begin
                if map[j+1,now]>max then continue;
                last:=(i xor (t and -t));
                if (1<<j)<>(t and -t) then last:=last xor (1<<j);
                if f[i]>f[last]+(dist[j+1]+1)*map[j+1,now] then
                  begin
                    f[i]:=f[last]+(dist[j+1]+1)*map[j+1,now];
                    tmp[now]:=dist[j+1]+1;
                  end;
              end;
          t:=t-(t and -t)
        end;
      dist:=tmp;
    end;
end;

begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);

  init();
  dp();
  writeln(f[(1<<n)-1]);

  close(input);
  close(output);
end.

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=1050;
bool vis[N],w[N][N];
int x[N],y[N],z[N],q[N],n,H,R,l,r;

bool check()
{
/*
	for (int i=0;i<=n+1;i++)
		for (int j=0;j<=n+1;j++)
			printf("%d%c",w[i][j]," \n"[j==n+1]);
*/
	memset(vis,false,sizeof(vis));
	l=r=0;
	q[r++]=0; vis[0]=true;
	while (l<r)
	{
		int u=q[l++];
		if (u==n+1) return true;
		for (int i=0;i<=n+1;i++)
			if (w[u][i] && !vis[i])
				q[r++]=i,vis[i]=true;		
	}
	return false;	
}
void solve()
{
	memset(w,false,sizeof(w));
	scanf("%d%d%d",&n,&H,&R);
	for (int i=1;i<=n;i++)
	{
		scanf("%d%d%d",&x[i],&y[i],&z[i]);
		if (z[i]<=R)
			w[0][i]=w[i][0]=true;
		if (H-z[i]<=R)
			w[n+1][i]=w[i][n+1]=true;
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=i;j++)
			if ((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])<=4*R*R)
				w[i][j]=w[j][i]=true;
	if (check()) puts("Yes");
	else puts("No");	
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--) solve();	
	return 0;
}

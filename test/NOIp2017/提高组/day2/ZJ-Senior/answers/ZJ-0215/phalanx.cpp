#include<bits/stdc++.h>
using namespace std;
const int N=1050;
int id[N][N],n,m,q,cnt=0;
int c[N],a[N*2];

void solve1()
{
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			id[i][j]=++cnt;
	while (q--)
	{
		int x,y,tmp;
		scanf("%d%d",&x,&y);
		tmp=id[x][y];
		printf("%d\n",id[x][y]);
		for (int i=y+1;i<=m;i++)
			id[x][i-1]=id[x][i];
		for (int i=x+1;i<=n;i++)
			id[i-1][m]=id[i][m];
		id[n][m]=tmp;
	}	
}




namespace my{
	const int N=300000+100;
int a[N*2],c[N],d[N],cnt[N],Head[N],ans[N];
int ques_tot,tim,n,m,q;

struct Edge{
	int v,next;
}edge[N];
struct QUES{
	int x,y;
}ques[N];

vector<int>t[N],b[N];


inline void BITclear() {tim++;}
void add(int x,int v)
{
	for (int i=x;i<N;i+=i&-i)
	{
		if (d[i]!=tim) 
			d[i]=tim,c[i]=0;
		c[i]+=v;
	}
}
int sum(int x)
{
	int tot=0;
	for (int i=min(x,N-1);i;i-=i&-i)
		if (d[i]!=tim) d[i]=tim,c[i]=0;
		else tot+=c[i];
	return tot;
}
void addques(int x,int y)
{
	edge[++ques_tot]=(Edge){y,Head[x]};
	Head[x]=ques_tot;
}
int BS2(int v)
{
	int l=v,r=2*N;
	while (l<r)
	{
		int mid=(l+r+1)>>1;
		if (v+sum(mid)<=mid) r=mid-1;
		else l=mid;
	}
	return l+1;
}
void solve()
{
	BITclear();
	int len=n,pos;
	for (int i=1;i<=n;i++) a[i]=i*m;
	for (int i=1;i<=q;i++)
	{
		int x=ques[i].x,y=ques[i].y;
		cnt[x]++;
		if (y==m)
		{
			pos=x+sum(x);	
			ans[i]=a[pos];
			a[++len]=ans[i];
			add(x,1);
			continue;
		}
		pos=t[x][cnt[x]];
//		pos=y+t[x][cnt[x]];
		if (pos<=m-1) ans[i]=(x-1)*m+pos;
		else ans[i]=b[x][pos-(m-1)];
		a[++len]=ans[i];
//		pos=x+sum(x);
		pos=BS2(x);
		if (pos>len) {puts("Error!"); return;}
		b[x].push_back(a[pos]);
		add(x,1);		
	}	
}
int BS(int v)
{
	int l=v-1,r=2*N;
	while (l<r)
	{
		int mid=(l+r+1)>>1;
		if (v+sum(mid)<=mid) r=mid-1;
		else l=mid;
	}
	return l+1;
}
void prepare() //prepare t[row][i];
{
	for (int i=1;i<=n;i++) t[i].push_back(0);
	for (int i=1;i<=n;i++)
	{
		int cnt2=0;
		for (int j=Head[i];j;j=edge[j].next)
		{
			int v=edge[j].v;
			cnt2++;
//			t[i].push_back(sum(v));
			t[i].push_back(BS(v));
			add(v,1);
		}
		BITclear();
	}
}
void lalala()
{
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=q;i++)
		scanf("%d%d",&ques[i].x,&ques[i].y);
	for (int i=q;i>=1;i--)
		addques(ques[i].x,ques[i].y);
	prepare();
/*
	for (int i=1;i<=n;i++)
	{
		printf("%d %d\n",i,(int)t[i].size());
		for (int j=0;j<(int)t[i].size();j++)
			printf("%d ",t[i][j]);
		printf("\n");
	}
*/
	solve();
	for (int i=1;i<=q;i++)
		printf("%d\n",ans[i]);		
}
}



int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000 && m<=1000) solve1();
	else my::lalala();
	return 0;
}

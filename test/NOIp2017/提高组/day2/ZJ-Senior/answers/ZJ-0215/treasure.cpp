#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=15;
int w[N][N],n,pow[N],m,f[50000000],num[N],ans=0x3f3f3f3f;

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(w,0x3f,sizeof(w));
	memset(f,0x3f,sizeof(f));	
	scanf("%d%d",&n,&m);
	pow[0]=1;
	for (int i=1;i<=n;i++) 
		pow[i]=pow[i-1]*9;
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		w[x][y]=min(w[x][y],z);
		w[y][x]=w[x][y];
	}
	for (int i=1;i<=n;i++)
		f[pow[i-1]]=0;
	for (int i=1;i<pow[n];i++)
	{
//		printf("%d %d\n",i,f[i]);
		if (f[i]==0x3f3f3f3f) continue;
		int cnt=0;
		for (int j=1;j<=n;j++)
		{
			num[j]=i/pow[j-1]%9;
			if (num[j]) cnt++;
		}	
			
		for (int j=1;j<=n;j++)
			if (num[j])
				for (int k=1;k<=n;k++)
					if (j!=k && !num[k] && w[j][k]!=0x3f3f3f3f)
					{
						int sta=i+(num[j]+1)*pow[k-1];
						f[sta]=min(f[sta],f[i]+num[j]*w[j][k]);
						if (cnt+1==n) ans=min(ans,f[sta]);
					}
	}
	printf("%d\n",ans);	
	return 0;
}

#include<bits/stdc++.h>
#define min(x,y) ((x)<(y)?(x):(y))
int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')k=-1,c=getchar();
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return k*kk;
}using namespace std;
int n,m,x,y,z,a[100][100],f[13][13][5000],ans;
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	n=read();m=read();memset(a,6,sizeof(a));memset(f,6,sizeof(f));
	for (int i=1;i<=m;i++){
		x=read();y=read();z=read();
		a[x][y]=min(a[x][y],z);a[y][x]=min(a[y][x],z);
	}for (int j=n;j>=1;j--){
		for (int i=1;i<=n;i++){
			f[i][j][1<<(i-1)]=0;
			for (int l=1;l<=n;l++)
				for (int k=(1<<(i-1));k<1<<n;k++)
					if (((k&(1<<(l-1)))==0)&&f[i][j][k]<=1e8){
						int d=((1<<n)-1)^k;
						for (int kk=d;kk;kk=(kk-1)&d)
							f[i][j][k^kk]=min(f[i][j][k^kk],f[i][j][k]+f[l][j+1][kk]+a[i][l]*j);
					}
		}
	}ans=1e8;
	for (int i=1;i<=n;i++)ans=min(ans,f[i][1][(1<<n)-1]);
	cout<<ans<<endl;
}

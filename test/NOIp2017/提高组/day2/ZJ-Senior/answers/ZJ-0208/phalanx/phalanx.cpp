#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>

using namespace std;

typedef pair<int,int> par;

const int N=300010;

int n,m,q;
int x[N],y[N];
int nn;

int cnt,a[N<<2],b[N<<2];

int M[1010][1010];

inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void rea(int &x){
	char c=nc(); x=0;
	for(;c>'9'||c<'0';c=nc());for(;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
}

inline bool cmp(const int &a,const int &b){
	return y[a]<y[b];
}


inline int Query(int x){
	int ret=0;
	for(;x;x-=x&-x) ret+=b[x];
	return ret;
}

inline void Add(int x,int y){
	for(;x<=nn;x+=x&-x) b[x]+=y;
}

inline void solve1(){
	for(int i=1;i<=m;i++) a[++cnt]=i;
	for(int i=2;i<=n;i++) a[++cnt]=i*m;
	nn=cnt+q;
	for(int i=1;i<=cnt;i++) Add(i,1);
	for(int i=1;i<=q;i++){
		int L=y[i],R=cnt,mid,cur;
		while(L<=R) Query(mid=(L+R)>>1)<y[i]?L=mid+1:R=(cur=mid)-1;
		printf("%d\n",a[cur]);
		a[++cnt]=a[cur]; Add(cur,-1);
	}
}

inline void solve2(){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			M[i][j]=(i-1)*m+j;
	for(int i=1;i<=q;i++){
		int cur=M[x[i]][y[i]];
		printf("%d\n",cur);
		for(int j=y[i];j<m;j++)
			M[x[i]][j]=M[x[i]][j+1];
		for(int j=x[i];j<n;j++){
			M[j][m]=M[j+1][m];
		}
		M[n][m]=cur;
	}
}


int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	rea(n); rea(m); rea(q);
	int ok=1;
	for(int i=1;i<=q;i++){
		rea(x[i]); rea(y[i]);
		if(x[i]!=1) ok=0;
	}
	if(ok) solve1();
	else solve2();
	return 0;
}

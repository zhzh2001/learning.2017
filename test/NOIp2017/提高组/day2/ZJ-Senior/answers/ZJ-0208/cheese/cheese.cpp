#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long ll;

const int N=1010;

int t,n,fa[N],sz[N];
int S,T,h,r,a[N],b[N],c[N];

int Gfat(int x){
	return x==fa[x]?x:fa[x]=Gfat(fa[x]);
}

inline void iswap(int &x,int &y){
	int z=x; x=y; y=z;
}

inline void Unn(int x,int y){
	x=Gfat(x); y=Gfat(y);
	if(x==y) return ;
	if(sz[x]>sz[y]) iswap(x,y);
	sz[y]+=sz[x]; fa[x]=y;
}

inline ll sqr(int x){
	return 1LL*x*x;
}

inline bool bana(int x,int y){
	ll cur=sqr(a[x]-a[y])+sqr(b[x]-b[y]);
	return cur<=4LL*r*r-sqr(c[x]-c[y]);
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d%d",&n,&h,&r);
		S=n+1; T=n+2;
		for(int i=1;i<=n+2;i++) fa[i]=i,sz[i]=1;
		for(int i=1;i<=n;i++){
			scanf("%d%d%d",&a[i],&b[i],&c[i]);
			if(c[i]<=r) Unn(i,S);
			if(c[i]+r>=h) Unn(i,T);
		}
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				if(bana(i,j)) Unn(i,j);
		if(Gfat(S)==Gfat(T)) puts("Yes");
		else puts("No");
	}
	return 0;
}

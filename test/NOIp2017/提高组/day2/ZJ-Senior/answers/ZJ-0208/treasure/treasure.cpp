#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

int n,m;
int lk[15][15],pw[15],val[15];
int f[15][531445];
int ttt[531445],lst[4050],ttt1[4050],sz[531445];

inline int iinn(int x,int y){
	return (x/pw[y-1])%3;
}

inline int cst(int S){
	int ret=0;
	for(;S;S-=(1<<(lst[S]-1))){
		if(val[lst[S]]==(1<<30)) return 1<<30;
		ret+=val[lst[S]];
	}
	return ret;
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) lk[i][j]=1<<30;
	for(int i=1,x,y,z;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		lk[x][y]=lk[y][x]=min(lk[x][y],z);
	}
	pw[0]=1; for(int i=1;i<=n;i++) pw[i]=3*pw[i-1];
	for(int i=1;i<=n;i++)
		for(int j=0;j<pw[n];j++) f[i][j]=1<<30;
		
	for(int i=0;i<pw[n];i++)
		for(int j=1;j<=n;j++)
			if(!iinn(i,j)) ttt[i]|=1<<(j-1);
			else sz[i]++;
	
	for(int i=1;i<(1<<n);i++)
		for(int j=1;j<=n;j++)
			if((i>>(j-1))&1) ttt1[i]+=pw[j-1]*2;
	
	lst[1]=1;
	for(int i=2;i<(1<<n);i++)
		lst[i]=lst[i>>1]+1;
	
	for(int i=1;i<=n;i++) f[1][pw[i-1]*2]=0;
	int ans=1<<30;
	for(int i=1;i<=n;i++)
		for(int S=0;S<pw[n];S++){
			if(f[i][S]>=ans) continue;
			if(sz[S]==n) ans=min(ans,f[i][S]);
			for(int j=1;j<=n;j++)
				if(!iinn(S,j)){
					val[j]=1<<30;
					for(int k=1;k<=n;k++)
						if(iinn(S,k)==2) val[j]=min(val[j],lk[k][j]);
				}
			int nxt=0,SS=ttt[S];
			for(int j=1;j<=n;j++)
				if(iinn(S,j)) nxt+=pw[j-1];
			for(int ss=SS;ss;ss=(ss-1)&SS){
				int cur=cst(ss);
				if(cur==(1<<30)) continue;
				f[i+1][nxt+ttt1[ss]]=min(f[i+1][nxt+ttt1[ss]],f[i][S]+cur*i);
			}
		}
	printf("%d\n",ans);
	return 0;
}

#include <stdio.h>
void arcPhi(int *,int*,int,int);

int main(void){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q;
	scanf("%d %d %d",&n,&m,&q);
	int i,j;
	int trs[q][2];
	for(i=0;i<q;i++){
		scanf("%d %d",&trs[i][0],&trs[i][1]);
		int ans;
		int tem[2];
		tem[0]=trs[i][0];
		tem[1]=trs[i][1];
		for(j=i-1;j>=0;j--){
			arcPhi(tem,trs[j],n,m);
		}
		ans=(tem[0]-1)*m+tem[1];
		printf("%d\n",ans);
	}
	return 0;
}
void arcPhi(int *p,int *t,int in,int im){
	if(p[1]==im){
		if(p[0]==in){
			p[0]=t[0];
		}else if(p[0]>=t[0]){
			p[0]++;
		}
	}
	if(p[0]==t[0]){
		if(p[1]==im){
			p[1]=t[1];
		}else if(p[1]>=t[1]){
			p[1]++;
		}
	}
}
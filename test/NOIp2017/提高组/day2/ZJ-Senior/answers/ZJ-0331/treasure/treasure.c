#include <stdio.h>

int main(void){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d %d",&n,&m);
	int ways[n+1][n+1];
	int ps[n+1];
	int i,j,k;
	for(i=1;i<=n;i++){
		for(j=1;j<=n;j++){
			ways[i][j]=-1;
		}
	}
	
	
	for(i=0;i<m;i++){
		int temx,temy,temv;
		scanf("%d %d",&temx,&temy);
		scanf("%d",&temv);
		if(ways[temx][temy]==-1||ways[temx][temy]>temv){
			ways[temx][temy]=temv;
			ways[temy][temx]=temv;
		}
	}
	int tw=-1;
	for(i=1;i<=n;i++){/////////////////////////
		for(j=1;j<=n;j++){
			ps[j]=-1;
		}
		ps[i]=1;
		int count=1;
		int w=0;
		while(count<n){
			int min=-1;
			int mins[2];
			for(j=1;j<=n;j++){
				if(ps[j]==-1) continue;
				for(k=1;k<=n;k++){
					if(ways[j][k]==-1||ps[k]!=-1) continue;
					if(min==-1||ways[j][k]*ps[j]<min){
						min=ps[j]*ways[j][k];
						mins[0]=j;
						mins[1]=k;
					}
				}
			}
			ps[mins[1]]=ps[mins[0]]+1;
			w+=min;
			count++;
		}
		if(tw==-1||tw>w){
			tw=w;
		}
	}
	printf("%d",tw);
	return 0;
}
#include <stdio.h>
#define DELTA 0.001

double msqrt(double);
void qsor(int [][3],int *,int);
double mdist(int*,int *);

int main(void){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	int qwe;
	for(qwe=0;qwe<T;qwe++){
	int i;
	int n,h,r;
	scanf("%d %d %d",&n,&h,&r);
	int roles[n][3];
	int ways[n];
	int tobs[n];
	for(i=0;i<n;i++){
		scanf("%d %d %d",&roles[i][0],&roles[i][1],&roles[i][2]);
		ways[i]=i;
		tobs[i]=i;
	}
	for(i=0;i<n;i++){
		if(roles[i][2]<=r){
			ways[i]=-1;
		}else if(roles[i][2]>=(h-r)){
			ways[i]=-2;
		}
	}
	qsor(roles,tobs,n);
	for(i=0;i<n;i++){
		int tz=roles[tobs[i]][2];
		if(tz<=r){
			continue;
		}
		
		int j;
		for(j=0;j<i;j++){
			if(mdist(roles[tobs[i]],roles[tobs[j]])<=2*r+DELTA){
				ways[tobs[i]]=ways[tobs[j]];
				break;
			}
		}
	}
	
	int flag=0;///////////////////////////////////////
	for(i=0;i<n;i++){
		int k=ways[tobs[i]];
	}
	for(i=0;i<n;i++){
		int k=ways[tobs[i]];
		if(k<0) continue;
		
		while(k>=0){
			if(k==ways[k]){
				flag=1;
				break;
			}
			k=ways[k];
		}
		if(flag) break;
	}
	for(i=0;i<n-1;i++){
		int k=ways[tobs[i+1]];
		if(k!=ways[tobs[i]]){
			flag=1;
			break;
		}
	}
	
	if(!flag){
		printf("Yes\n");
	}else{
		printf("No\n");
	}
}
	return 0;
}

double msqrt(double n){///////OK
	double a=n/2;
	double wa=a*a-n;
	while((wa>DELTA/10)||(wa<-DELTA/10)){
		a=(a+n/a)/2;
		wa=a*a-n;
	}
	return a;
}
void qsor(int dat[][3],int * ns,int len){///OK
	if(len<=1){
		return;
	}
	int flag=0;
	int qi=0;
	int qj=len-1;
	while(qi!=qj){
		if(dat[ns[qi]][2]>dat[ns[qj]][2]){
			int ker=ns[qi];
			ns[qi]=ns[qj];
			ns[qj]=ker;
			flag=1-flag;
		}
		if(flag==0){
			qi++;
		}else{
			qj--;
		}
	}
	qsor(dat,ns,qi);
	qsor(dat,ns+qi+1,len-qi-1);
}
double mdist(int * a,int * b){
	double x0=a[0]-b[0];
	double y0=a[1]-b[1];
	double z0=a[2]-b[2];
	x0*=x0;
	y0*=y0;
	z0*=z0;
	return msqrt(x0+y0+z0);
}
#include<cstdio>
#include<cstring>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=1005;
struct node{
	ll x,y,z;
}a[maxn];
int n;
ll h,r;
inline void init(){
	n=read(); h=read(); r=read();
	for (int i=1;i<=n;i++){
		a[i].x=read();
		a[i].y=read();
		a[i].z=read();
	}
}
struct edge{
	int link,next;
}e[maxn<<2];
int tot,head[maxn];
inline void insert(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void ins(int u,int v){
	insert(u,v); insert(v,u);
}
inline ll sqr(ll x){
	return x*x;
}
const ll inf=1e4;
inline double dis(int i,int j){
	return 1.0*sqr(a[i].x-a[j].x)/inf+1.0*sqr(a[i].y-a[j].y)/inf+1.0*sqr(a[i].z-a[j].z)/inf;
}
bool vis[maxn];
inline void dfs(int u){
	vis[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (!vis[v]){
			dfs(v);
		}
	}
}
inline bool judge(int i,int j,ll limit){
	double lim=1.0*limit/inf;
	return dis(i,j)<=lim;
}
inline void solve(){
	ll lim=sqr(r);
	for (int i=1;i<=n;i++){
		ll temp1=sqr(a[i].z-0);
		ll temp2=sqr(a[i].z-h);
		if (temp1<=lim){
			ins(i,0);
		}
		if (temp2<=lim){
			ins(i,n+1);
		}
	}
	ll limit=4*lim;
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			if (judge(i,j,limit)){
				ins(i,j);
			}
		}
	}
	dfs(0);
	if (vis[n+1]){
		puts("Yes");
	}else{
		puts("No");
	}
}
inline void clean(){
	memset(vis,0,sizeof(vis));
	memset(head,0,sizeof(head));
	tot=0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while (T--){
		clean();
		init();
		solve();
	}
	return 0;
}

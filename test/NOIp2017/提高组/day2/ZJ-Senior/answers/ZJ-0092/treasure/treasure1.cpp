#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int maxn=13;
const ll inf=1e18;
int n,m;
ll a[maxn][maxn],dis[maxn][maxn];
inline void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			a[i][j]=a[j][i]=inf;
			dis[i][j]=dis[j][i]=inf;
		} 
	}
	for (int i=1;i<=m;i++){
		int u,v; ll w;
		scanf("%d%d%lld",&u,&v,&w);
		a[u][v]=a[v][u]=min(a[u][v],w);
		dis[u][v]=dis[v][u]=1;
	}
}
int all;
ll dp[maxn][1<<12];
inline ll DP(int start,int s){
	if (dp[start][s]!=-1){
		return dp[start][s];
	}
	if (s==all){
		return 0; 
	}
	ll &ans=dp[start][s];
	ans=inf;
	for (int i=1;i<=n;i++){
		if (s&(1<<(i-1))){
			for (int j=1;j<=n;j++){
				if (!(s&(1<<(j-1)))){
					if (a[i][j]!=inf){
						ans=min(ans,DP(start,s|(1<<(j-1)))+1ll*a[i][j]*(dis[start][i]+1));
					} 
				}
			}
		}
	}
	return ans;
}
inline void solve(){
	for (int i=1;i<=n;i++){
		dis[i][i]=0;
	}
	for (int k=1;k<=n;k++){
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				dis[i][j]=min(dis[i][k]+dis[k][j],dis[i][j]);
			}
		}
	}
	ll ans=inf; all=(1<<n)-1;
	memset(dp,-1,sizeof(dp));
	for (int i=1;i<=n;i++){
		ans=min(ans,DP(i,(1<<(i-1))));
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout); 
	init();
	solve();
	return 0;
}

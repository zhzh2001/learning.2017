#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long ll;
const int maxn=13;
const ll inf=1e18;
int n,m;
ll a[maxn][maxn];
inline void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			a[i][j]=a[j][i]=inf;
		} 
	}
	for (int i=1;i<=m;i++){
		int u,v; ll w;
		scanf("%d%d%lld",&u,&v,&w);
		a[u][v]=a[v][u]=min(a[u][v],w);
	}
}
ll ans;
int tot,start;
bool vis[maxn],cho[maxn][maxn];
void dfs(int x,ll sum){
	if (x==n){
		tot++;
		ans=min(ans,sum);
		if (tot==150000){
			printf("%d\n",ans);
			exit(0);
		}
		return ;
	}
	int dis[maxn][maxn];
	memset(dis,127/3,sizeof(dis));
	for (int i=1;i<=n;i++){
		dis[i][i]=0;
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (cho[i][j]){
				dis[i][j]=dis[j][i]=1;
			}
		}
	}
	for (int k=1;k<=n;k++){
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				dis[i][j]=min(dis[i][k]+dis[k][j],dis[i][j]);
			}
		}
	}
	for (int i=1;i<=n;i++){
		if (vis[i]){
			for (int j=1;j<=n;j++){
				if (!vis[j]){
					if (a[i][j]==inf) continue;
					vis[j]=1;
					cho[i][j]=cho[j][i]=1;
					dfs(x+1,sum+(dis[start][i]+1)*a[i][j]);
					cho[i][j]=cho[j][i]=0;
					vis[j]=0;
				}
			}
		}
	}
}
inline void solve(){
	ans=inf;
	for (int i=1;i<=n;i++){
		memset(vis,0,sizeof(vis));
		start=i; vis[i]=1; dfs(1,0);
	}
	printf("%d\n",ans);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout); 
	init();
	solve();
	return 0;
}

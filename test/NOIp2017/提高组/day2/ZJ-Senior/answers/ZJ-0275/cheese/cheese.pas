var
  tt,n,gao,r,i,j,h,t,v,k:longint;
  x,y,z:array[1..1005] of longint;
  c:array[0..1005] of longint;
  b:array[1..1005,1..1005] of boolean;
  way:array[1..1005,1..1005] of longint;
  visit:array[1..1005] of boolean;
  f:array[1..20000] of longint;
  dist:extended;
  p:boolean;
begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input); rewrite(output);
  read(tt);
  while tt>0 do
   begin
    dec(tt);
    read(n,gao,r);

    for i:=1 to n do for j:=1 to n do b[i,j]:=false;
    fillchar(c,sizeof(c),0);
    for i:=1 to n do
     for j:=1 to n do
      if i<>j then
       begin
        dist:=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]));
        if dist<=2*r then
         begin
          b[i,j]:=true;
          b[j,i]:=true;
          inc(c[i]); way[i,c[i]]:=j;
          inc(c[j]); way[j,c[j]]:=i;
         end;
       end;




    for i:=1 to n do
     if z[i]<=r then
      begin
       for j:=1 to n do visit[j]:=false;
       for j:=1 to t do f[j]:=0;
       h:=0; t:=1; f[1]:=i; visit[i]:=true; p:=false;
       while (h<=t) and (p=false) do
        begin
         inc(h);
         v:=f[h];
         for j:=1 to c[v] do
          if visit[way[v,j]]=false then
           begin
            visit[way[v,j]]:=true;
            inc(t);
            f[t]:=way[v,j];
            if z[way[v,j]]+r>=gao then begin p:=true; break; end;
           end;
        end;
       if p then break;
      end;
    if p then writeln('Yes') else writeln('No');
   end;
  close(input); close(output);
end.


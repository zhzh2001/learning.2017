var
  n,m,i,x,y,z,min,start,mmin,j,fei,num,ans,max,eend,k:longint;
  b:array[1..100,1..100] of boolean;
  way:array[1..100,1..100] of longint;
  f,deep:array[1..10000] of longint;
  has:array[1..1000] of boolean;
begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input); rewrite(output);
  read(n,m);
  for i:=1 to n do for j:=1 to n do way[i,j]:=999999;
  for i:=1 to m do
   begin
    read(x,y,z);
    b[x,y]:=true;
    b[y,x]:=true;
    if z<way[x,y] then begin way[x,y]:=z; way[y,x]:=z; end;
   end;

  min:=maxlongint;
  for start:=1 to n do
   begin
   fillchar(f,sizeof(f),0);
   fillchar(deep,sizeof(deep),0);
   fillchar(has,sizeof(has),false);
   num:=1; f[1]:=start; ans:=0; deep[start]:=1;has[start]:=true;
   while num<n do
    begin
     mmin:=maxlongint;
      for i:=1 to num do
      for j:=1 to n do
       if (b[f[i],j]) and (has[j]=false) then
        begin
         deep[j]:=deep[f[i]]+1;
         fei:=deep[f[i]]*way[f[i] ,j];
         if fei<mmin then begin mmin:=fei; eend:=j; end;
        end;
     ans:=ans+mmin;
     inc(num);
     f[num]:=eend;
     has[eend]:=true;


    end;
   if ans<min then begin min:=ans; k:=start; end;
   end;
  writeln(min);
  close(input); close(output);
end.

#include<cstdio>
#include<cstring>

int T,n,fa[1001];
long long di,x[1001],y[1001],z[1001],hh,rr,dd;
bool bo[1001],bb[1001],boo;

bool chec(int i,int j)
{
	di=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
	if (di<=dd) return 1; else return 0;
}

int askk(int x) {return (fa[x]==x)?(x):(fa[x]=askk(fa[x]));}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d",&n); scanf("%lld%lld",&hh,&rr); dd=4; dd*=rr*rr;
		for (int i=1; i<=n; i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		memset(bo,0,sizeof(bo));
		memset(bb,0,sizeof(bb));
		for (int i=1; i<=n; i++)
		{
			if (z[i]<=rr) bo[i]=1;
			if (hh-z[i]<=rr) bb[i]=1;
			fa[i]=i;
		}
		for (int i=1; i<n; i++)
			for (int j=i+1; j<=n; j++)
				if (chec(i,j)) fa[askk(i)]=askk(j);
		boo=0;
		for (int i=1; i<=n; i++)
			if (bo[i])
			{
				for (int j=1; j<=n; j++)
					if (bb[j]) 
						if (askk(i)==askk(j)) {boo=1; break;}
				if (boo) break;
			}
		if (boo) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<cstring>
#include<cmath>
using namespace std;
int head[1010],map[1010],Next[100005],to[100005],tot,t,n,h,r;
struct node{
	int x,y,z;
}a[1005];
double dist(int u,int v){
	int xx=abs(a[u].x-a[v].x);
	int yy=abs(a[u].y-a[v].y);
	int zz=abs(a[u].z-a[v].z);
	return sqrt(xx*xx+yy*yy+zz*zz);
}
void add(int a,int b){
	Next[tot]=head[a];
	head[a]=tot;
	to[tot]=b;
	tot++;
}
void dfs(int x){
	for (int i=head[x]; i!=-1; i=Next[i]){
		int y=to[i];
		if (map[y]==0){
			map[y]=1;
			dfs(y);
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		tot=0;
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1; i<=n+2; i++) head[i]=-1;
		for (int i=2; i<=n+1; i++) scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		for (int i=2; i<=n+1; i++){
			if (a[i].z-r<=0) {
				add(1,i);
				add(i,1);
			}
			if (a[i].z+r>=h){
				add(i,n+2);
				add(n+2,i);
			}
			for (int j=2; j<=n+1; j++){
				if (i!=j){
					if (dist(i,j)<=(double)(2*r)) {
						add(i,j);
						add(j,i);
					}
				}
			}
		}
		memset(map,0,sizeof(map));
		dfs(1);
		if (map[n+2]){
			printf("YES\n");
		}
		else {
			printf("NO\n");
		}
	}
	return 0;
}
 

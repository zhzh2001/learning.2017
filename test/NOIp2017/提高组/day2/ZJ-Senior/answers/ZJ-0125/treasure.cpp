#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int dis[200][200],b[200],n,m,len[200],f[505][5005];
struct node{
	int x,y,v;
}q[20005];
void dfs(int x,int l){
	for (int i=1; i<=n; i++){
		if (dis[x][i]!=1000000000 && b[i]==0){
			len[i]=min(l+1,len[i]);
			b[i]=1;
			dfs(i,l+1);
			b[i]=0;
		}	
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout); 
	scanf("%d%d",&n,&m);
	int res=1000000000;
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++)
			dis[i][j]=1000000000;
	for (int i=1; i<=m; i++){
		int x,y,l;
		scanf("%d%d%d",&x,&y,&l);
		dis[x][y]=min(dis[x][y],l);
		dis[y][x]=min(dis[y][x],l);
	}
	for (int p=1; p<=n; p++){
		memset(b,0,sizeof(b));
		for (int i=1; i<=n; i++) len[i]=1000000000;
		len[p]=1;
		b[p]=1;
		dfs(p,1);
		m=0;
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++)
				if (dis[i][j]!=1000000000){
					m++;
					q[m].x=i,q[m].y=j,q[m].v=dis[i][j]*len[i];				
				}
		for (int i=0; i<=m; i++)
			for (int j=(1<<n)-1; j>=0; j--) f[i][j]=1000000000;
		f[0][1<<(p-1)]=0;
		for (int i=1; i<=m; i++){
			for (int j=0; j<=(1<<n)-1; j++){
				f[i][j]=f[i-1][j]; 
				if (j&(1<<(q[i].x-1)) && j&(1<<(q[i].y-1)))
					f[i][j]=min(f[i][j],f[i-1][j-(1<<(q[i].y-1))]+q[i].v);
			}
		}
		res=min(res,f[m][(1<<n)-1]);
	}
	printf("%d",res);
	return 0;
}

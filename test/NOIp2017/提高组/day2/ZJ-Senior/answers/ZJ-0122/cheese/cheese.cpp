#include<cstdio>
#include<cstring>
#include<queue>
#include<iostream>
#include<algorithm>
#define ll long long
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
using namespace std;

void judge(){
	 freopen("cheese.in","r",stdin);
	 freopen("cheese.out","w",stdout);
}

const int maxn = 1005;

struct Graph{
	int cnt;
	int head[maxn << 1],nxt[maxn << 1],to[maxn << 1];
	void addedge(int st,int ed){
		to[++cnt] = ed;nxt[cnt] = head[st];head[st] = cnt;
		to[++cnt] = st;nxt[cnt] = head[ed];head[ed] = cnt;
	}
}e;

struct Node{
	ll x,y,z,id;
}a[maxn];
int t,n,st,ed;
ll h,r;

ll calc(ll a,ll b,ll c,ll d,ll e,ll f){
	return (a - d) * (a - d) + (b - e) * (b - e) + (c - f) * (c - f);
}

bool vis[maxn];
bool ck;
void bfs(){
	memset(vis,0,sizeof(vis));
	queue<int >q;
	q.push(n + 1);vis[n + 1] = true;
	while(!q.empty()){
		int t = q.front();q.pop();
		if (t == n + 2){ck = true;puts("Yes");break;}
		for (int i = e.head[t];i;i = e.nxt[i]){
			int v = e.to[i];
			if (!vis[v]) q.push(v);
		}
		vis[t] = true;
	}
}

int main()
{
	judge();
	scanf("%d",&t);
	while(t--){
		ck = false;
		e.cnt = 0;
		memset(e.nxt,0,sizeof(e.nxt));
		memset(e.to,0,sizeof(e.to));
		memset(e.head,0,sizeof(e.head));
		scanf("%d%lld%lld",&n,&h,&r);
		rpt(i,1,n){
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
			a[i].id = i;
		}
		st = n + 1;ed = n + 2;
		rpt(i,1,n) if (a[i].z - r <= 0){
			e.addedge(st,a[i].id);
			//cout << "1" << endl;
		}
		rpt(i,1,n) if (a[i].z + r >= h) {
			e.addedge(a[i].id,ed);
			//cout << "2" << endl; 
		}
		rpt(i,1,n) rpt(j,i + 1,n)
			if (calc(a[i].x,a[i].y,a[i].z,a[j].x,a[j].y,a[j].z) <= 4 * r * r) e.addedge(a[i].id,a[j].id);
		bfs();
		if (!ck) puts("No");
	}
	return 0;
}


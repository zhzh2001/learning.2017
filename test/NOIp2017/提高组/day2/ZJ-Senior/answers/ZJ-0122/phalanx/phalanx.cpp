#include<cstdio>
#include<algorithm>
#define ll long long
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
using namespace std;

void judge(){
	 freopen("phalanx.in","r",stdin);
	 freopen("phalanx.out","w",stdout);
}

const int maxn = 1005;

int n,m,q;
int mat[maxn][maxn];
int mm[300005];
int cl(int x,int y){
	return (x - 1) * m + y;
}
int main()
{
	judge();
	scanf("%d%d%d",&n,&m,&q);
	if (n == 1){
		rpt(i,1,m) mm[i] = i;
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			int tmp = mm[y];
			printf("%d\n",tmp);
			rpt(i,y + 1,m) mm[i - 1] = mm[i];
			mm[m] = tmp;
		}
	}
	else{
		rpt(i,1,n) rpt(j,1,m) mat[i][j] = cl(i,j);
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			int tmp = mat[x][y];
			printf("%d\n",tmp);
			//if (x == n && y == m) continue;
			rpt(i,y + 1,m) mat[x][i - 1] = mat[x][i];
			rpt(i,x + 1,n) mat[i - 1][m] = mat[i][m];
			mat[m][m] = tmp;  
		}
	}
	return 0;
}


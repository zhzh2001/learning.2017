#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define rpt(i,a,b) for (int i = (a);i <= (b);++i)
#define rpd(i,a,b) for (int i = (a);i >= (b);--i)
using namespace std;

void judge(){
	 freopen("treasure.in","r",stdin);
	 freopen("treasure.out","w",stdout);
}
int n,m,ans,sum;
int dep[20];
int mat[20][20];
const int maxn = 20;
int mnn(int x,int y){return x < y ? x : y;}
struct Graph{
	int cnt;
	int head[maxn << 1],nxt[maxn << 1],to[maxn << 1],val[maxn << 1];
	void addedge(int st,int ed,int v){
		to[++cnt] = ed;nxt[cnt] = head[st];val[cnt] = v;head[st] = cnt;
		to[++cnt] = st;nxt[cnt] = head[ed];val[cnt] = v;head[ed] = cnt;
	}
}e;

void dfs(int x,int f){
	dep[x] = dep[f] + 1;
	for (int i = e.head[x];i;i = e.nxt[i]){
		int v = e.to[i];
		if (v != f){
			sum += e.val[i] * dep[x];
			dfs(v,x);
		}
	}
}
bool vis[12];

void dfss(int x,int f){


}

int main()
{
	//judge();
	scanf("%d%d",&n,&m);
	ans = 0x3f3f3f;
	if (m == n - 1){
		rpt(i,1,m){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			e.addedge(x,y,z);
		}
		rpt(i,1,n){
			sum = 0;memset(dep,0,sizeof(dep));dfs(i,i);ans = min(ans,sum);
		}
		printf("%d\n",ans);
	}
	else{
		bool ck = false;
		int tmp = -1;
		rpt(i,1,m){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			if (tmp != -1 && tmp != z) ck = true;
			tmp = z;
			mat[x][y] = mnn(mat[x][y],z);
			mat[y][x] = mnn(mat[y][x],z);
		}
		if (!ck){
			rpt(i,1,n){
				dfss(i,i);
				rpt(i,1,n) sum += dep[i];
				ans = min(ans,tmp * dep[i]);
			}
		}
	}
	return 0;
}


#include <bits/stdc++.h>
#define lowbit(x) (x&-x)
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}

int n,m,ee,F[6000][13],Ans;
int mps[13][13],tot[6000];
queue <int> q;
inline int check(int x,int y,int tt)
{
	int ret=0;
	for(int i=y;i;i-=i&-i)
	{
		int ts=0x3fffffff;
		for(int j=x;j;j-=j&-j)
			ts=min(ts,mps[tot[i&-i]][tot[j&-j]]);
		if(ts==1061109567) return -1;
		ret+=ts*tt;
	}
	return ret;
}
inline int dfs(int state,int pre)
{
	if(!state) return 0;
	if(F[state][pre]!=1061109567) return F[state][pre];
	for(int i=state;i;i=(i-1)&state)
	{
		int bs=check(((1<<n)-1)^state,i,pre);
		if(bs==-1) continue;
		F[state][pre]=min(F[state][pre],bs+dfs(state^i,pre+1));
	}
	return F[state][pre];
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	for(int i=1;i<=13;i++) tot[(1<<(i-1))]=i;
	n=read(); m=read();
	memset(mps,0x3f,sizeof mps);
	for(int i=1;i<=m;i++) 
	{
		int u=read(), v=read(), w=read();
		mps[u][v]=mps[v][u]=min(mps[u][v],w);
	}
	Ans=0x3fffffff;
	for(int i=1;i<=n;i++)
	{
		memset(F,0x3f,sizeof F);
		Ans=min(Ans,dfs(((1<<n)-1)^(1<<(i-1)),1));
		//cout << i << " " << F[((1<<n)-1)^(1<<(i-1))][1] << endl;
	}
	cout << Ans << endl;
	return 0;
}
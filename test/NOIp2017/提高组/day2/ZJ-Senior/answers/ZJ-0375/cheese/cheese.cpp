#include <bits/stdc++.h>
#define N 1010
#define int long long
using namespace std;

inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int t,n,h,r,X[N],Y[N],Z[N];
int vis[N];
inline int sqr(int x){return x*x;}
inline int dis(int x,int y)
{
	return sqr(X[x]-X[y])+sqr(Y[x]-Y[y])+sqr(Z[x]-Z[y]);
}
queue <int> q;
signed main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read(); while(t--)
	{
		n=read(); h=read(); r=read();
		for(int i=1;i<=n;i++)
			X[i]=read(), Y[i]=read(), Z[i]=read();
		memset(vis,0,sizeof vis);
		while(!q.empty()) q.pop();
		vis[n+1]=1; q.push(n+1); int fg=0;
		while(!q.empty())
		{
			int lx=q.front(); q.pop();
			if(lx==n+2) {fg=1; break;}
			if(lx==n+1)
			{
				for(int i=1;i<=n;i++)
				{
					if(Z[i]-r<=0) 
						if(!vis[i]) vis[i]=1,q.push(i);
				}
			}
			else 
			{
				for(int i=1;i<=n;i++) 
					if(dis(i,lx)<=4*r*r) if(!vis[i]) vis[i]=1,q.push(i);
				if(Z[lx]+r>=h) if(!vis[n+2]) vis[n+2]=1,q.push(n+2);
			}
		}
		if(fg) puts("Yes");
		else puts("No");
	}
}

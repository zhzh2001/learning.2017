#include <bits/stdc++.h>
using namespace std;
#define blk 600
inline int read()
{
	int x=0,f=1; char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
int n,m,q,size[2000],nxt[2000];
int nx[600010],pr[600010],st[2000],las;
vector <long long> V[50000];
int ss[50000];
long long ls[50000],A[600010];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if(q<=500)
	{
		for(int i=1;i<=n;i++) ls[i]=1ll*(i-1)*m+m;
		for(int i=1;i<=q;i++)
		{
			//cerr << i << endl;
			int x=read(), y=read();
			if(!ss[x]) 
			{
				for(int j=1;j<m;j++) 
				V[x].push_back(1ll*(x-1)*m+j);
				ss[x]=1;
			}
			if(y==m) 
			{
				printf("%lld\n",ls[x]);
				int tmp=ls[x];
				for(int j=x;j<n;j++) ls[j]=ls[j+1];
				ls[n]=tmp;
			}
			else 
			{
				printf("%lld\n",V[x][y-1]);
				int tmp=V[x][y-1];
				for(int j=y-1;j<V[x].size()-1;j++)
					V[x][j]=V[x][j+1];
				V[x][V[x].size()-1]=ls[x];
				for(int j=x;j<n;j++) ls[j]=ls[j+1];
				ls[n]=tmp;
			}
		}
		return 0;
	}
	else 
	{
		for(int i=1;i<=m;i++) A[i]=i; las=n+m-1;
		for(int i=2;i<=n;i++) A[m+i-1]=1ll*(i-1)*m+m;
		int mx=0; for(int i=1;i<n+m;i++) 
			nx[i]=i+1,size[i/blk+bool(i%blk)]++,
			mx=max(mx,i/blk+bool(i%blk)),pr[i]=i-1;
		for(int i=1;i<=mx;i++) nxt[i]=i*blk+1,st[i]=(i-1)*blk+1;
		for(int g=1;g<=q;g++)
		{
			int x=read(), y=read();
			int ps=x+y-1,al=0,i;
			for(i=1;al+size[i]<ps;al+=size[i],i++);
			int pp=st[i];
			for(;al+1<ps;pp=nx[pp],al++); al++;
			printf("%lld\n",A[pp]);
			if(pp==st[i]) st[i]=nx[pp]; size[i]--;
			nx[pr[pp]]=nx[pp]; pr[nx[pp]]=pr[pp];
			nx[las]=pp; pr[pp]=las; las=pp; nx[pp]=0;
			if(size[mx]<blk) size[mx]++;
			else 
			{
				mx++; st[mx]=pp; size[mx]++;
			}
			//for(int ps=st[1];ps;ps=nx[ps]) printf("%d ",A[ps]); puts("");
		}
	}
	return 0;
}

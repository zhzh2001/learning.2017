#include<cstdio>
#include<vector>
#include<cctype>
int n,m,q,x,y;
namespace T1{
	const int N=50005;
	std::vector<int> a[N];
	int b[N];
	inline void mai(){
		int t,i,j;
		for(i=1,j=m;i<=n;++i)b[i]=j,j+=m;
		while(q--){
			scanf("%d%d",&x,&y);
			if(y==m){
				for(t=b[x],i=x+1;i<=m;++i)b[i-1]=b[i];printf("%d\n",t);
				b[m]=t;continue;
			}
			if(a[x].size()==0){
				a[x].resize(n+2);
				for(i=1,j=(x-1)*m+1;i<n;++i)a[x][i]=j++;
			}
			for(t=a[x][y],i=y+1;i<n;++i)a[x][i-1]=a[x][i];a[x][n-1]=b[x];
			for(i=x+1;i<=m;++i)b[i-1]=b[i];printf("%d\n",b[m]=t);
		}
	}
}
namespace T2{
	inline void read(int&x){
		static char c;
		for(c=getchar();!isdigit(c);c=getchar());
		for(x=0;isdigit(c);c=getchar())x=x*10+c-48;
	}
	const int N=300005;
	typedef unsigned int ui;
	ui seed=19260817;
	inline unsigned int nint(){return seed^=seed>>5,seed^=seed<<13,seed^=seed>>17,seed^=seed<<19;}
	struct treap{
		struct node{
			int l,r,v,sz;
			ui p;
		}a[N];
		int xb,rt;
		inline void maintain(int x){
			a[x].sz=a[a[x].l].sz+a[a[x].r].sz+1;
		}
		inline void lturn(int&x){
			int k=a[x].l;
			a[x].l=a[k].r;a[k].r=x;maintain(x);maintain(k);x=k;
		}
		inline void rturn(int&x){
			int k=a[x].r;
			a[x].r=a[k].l;a[k].l=x;maintain(x);maintain(k);x=k;
		}
		inline void pb(int&x,int v){
			if(!x)x=++xb,a[x].v=v,a[x].sz=1,a[x].p=nint();
				else{
					pb(a[x].r,v);
					if(a[a[x].r].p>a[x].p)rturn(x);
					maintain(x);
				}
		}
		inline int kth(int k){
			for(int i=rt;i;)
				if(k<=a[a[i].l].sz)i=a[i].l;
					else if(k==a[a[i].l].sz+1)return a[i].v;
							else k-=a[a[i].l].sz+1,i=a[i].r;
		}
		inline void del(int&i,int k){
			if(i){
				if(k==-1 || k==a[a[i].l].sz+1){
					if(!a[i].l)i=a[i].r;
						else if(!a[i].r)i=a[i].l;
								else if(a[a[i].l].p>a[a[i].r].p)lturn(i),del(a[i].r,-1);
										else rturn(i),del(a[i].l,-1);
				}else if(k<=a[a[i].l].sz)del(a[i].l,k);else del(a[i].r,k-a[a[i].l].sz-1);
				if(i)maintain(i);
			}
		}
	}t;
	inline void mai(){
		int i,x,y;
		for(i=1;i<=m;++i)t.pb(t.rt,i);
		while(q--){
			read(x),read(y);
			i=t.kth(y);
			t.del(t.rt,y);t.pb(t.rt,i);printf("%d\n",i);
		}
	}
}
int main(){
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=50000 && q<=500)T1::mai();
		else if(n==1)T2::mai();
	return 0;
}

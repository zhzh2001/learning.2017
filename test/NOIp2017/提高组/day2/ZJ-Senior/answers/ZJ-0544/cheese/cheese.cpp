#include<cstdio>
typedef long long ll;
const int N=1005;
int T,n,i,f[N],j;
ll x[N],y[N],z[N],h,r,s,R;
inline int gfa(int x){return x==f[x]?x:f[x]=gfa(f[x]);}
inline ll sqr(ll x){return x*x;}
int main(){
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);R=sqr(r*2);
		for(i=1;i<=n;++i)scanf("%lld%lld%lld",x+i,y+i,z+i);
		for(i=1;i<=n+2;++i)f[i]=i;
		for(i=1;i<n;++i)
			for(j=i+1;j<=n;++j)
				if(gfa(i)!=gfa(j)){
					s=sqr(x[i]-x[j]);
					if(s>R)continue;
					s+=sqr(y[i]-y[j]);
					if(s>R)continue;
					s+=sqr(z[i]-z[j]);
					if(s<=R)f[gfa(i)]=gfa(j);
				}
		for(i=1;i<=n;++i){
			if(z[i]<=r)f[gfa(i)]=gfa(n+1);
			if(z[i]>=h-r)f[gfa(i)]=gfa(n+2);
		}
		puts(gfa(n+1)==gfa(n+2)?"Yes":"No");
	}
	return 0;
}

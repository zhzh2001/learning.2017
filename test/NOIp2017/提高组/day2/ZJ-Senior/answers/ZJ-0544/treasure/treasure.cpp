#include<cstdio>
#include<queue>
#include<cstring>
const int N=14;
int n,m,a[N][N],u,v,w,lst;
bool bb;
namespace T1{
	inline void mai(){
		int q[N],t,w,d[N],s,ans=1<<30,i,j,k;
		for(i=1;i<=n;++i){
			t=0;q[w=1]=i;for(j=1;j<=n;++j)if(i!=j)d[j]=100;else d[j]=0;
			while(t<w){
				j=q[++t];
				for(k=1;k<=n;++k)if(~a[j][k] && d[k]>d[j]+1)d[k]=d[j]+1,q[++w]=k;
			}
			for(j=1,s=0;j<=n;++j)s+=d[j];
			if(s<ans)ans=s;
		}
		printf("%d\n",ans*lst);
	}
}
namespace T2{
	const int mo=1000003,L=3000000;
	struct node{
		char a[N];int v;// zhu yi huan hui char !!!!!!!!!!!!!!!!!!!
		inline int va(){
			static int i,s;
			for(i=1,s=0;i<=n;++i)s=(s*17+a[i])%mo;return s;
		}
		inline bool operator==(const node&rhs)const{
			for(register int i=1;i<=n;++i)if(a[i]!=rhs.a[i])return 0;return 1;
		}
	};
	int ans=1<<30,t,w;
	node q[L];
	struct hashe{
		node a[L];
		int nxt[L],h[mo],xb,id[L];
		inline bool tryins(const node&rhs,int z){
			for(int i=h[z];i;i=nxt[i])if(a[i]==rhs)return (a[i].v>rhs.v?q[id[i]].v=a[i].v=rhs.v:0),0;
			a[++xb]=rhs;
			nxt[xb]=h[z];
			h[z]=xb;id[xb]=w+1;
			return 1;
		}
	}h;
	node x,y;
	inline void mai(){
		register int i,j,cnt;
		for(i=1;i<=n;++i){
			for(j=1;j<=n;++j)if(i!=j)x.a[j]=20;else x.a[j]=0;
			q[++w]=x;h.tryins(x,x.va());
		}
		while(t<w && w<L-1000){
			x=q[++t];
			for(i=1,cnt=0;i<=n;++i)
				if(x.a[i]<20)
					for(j=1;j<=n;++j)if(~a[i][j] && x.a[j]==20){
						y=x;y.a[j]=x.a[i]+1;
						y.v=x.v+a[i][j]*y.a[j];
						if(h.tryins(y,y.va()))q[++w]=y;cnt=1;
					}
			if(!cnt && x.v<ans)
				ans=x.v;
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);memset(a,-1,sizeof a);for(int i=1;i<=n;++i)a[i][i]=0;
	while(m--){
		scanf("%d%d%d",&u,&v,&w);bb|=lst && lst!=w;lst=w;
		if(!~a[u][v] || w<a[u][v])a[u][v]=a[v][u]=w;
	}
	if(!bb)T1::mai();
		else T2::mai();
	return 0;
}

var
    ok,map:array[0..15,0..15]of longint;
    vis:array[0..15]of boolean;
    dis:array[0..15]of longint;
    ans,n,m,i,x,y,v,INF:longint;

procedure asson;
 begin
       assign(input,'treasure.in'); reset(input);
       assign(output,'treasure.out'); rewrite(output);
 end;

procedure assoff;
 begin
       close(input); close(output);
 end;

procedure dfs(viss,sum,dep:longint);
 var deps,j,k:longint;
 begin
       if (sum>=ans) then exit;
       if (viss=n) then
       begin
             if (sum<ans) then ans:=sum;
             exit;
       end;

       deps:=dep+1;
       ok[deps]:=ok[dep];
       inc(ok[deps,0]);
       for j:=1 to ok[dep,0] do
       begin
             for k:=1 to n do
              if (not vis[k] and (map[ok[dep,j],k]<>INF)) then
              begin
                    vis[k]:=true;
                    dis[k]:=dis[ok[dep,j]]+1;
                    ok[deps,ok[deps,0]]:=k;
                    dfs(viss+1,sum+map[ok[dep,j],k]*dis[k],deps);
                    vis[k]:=false;
              end;
       end;
 end;

begin
      asson;

      fillchar(map,sizeof(map),127);
      INF:=map[0,0];

      read(n,m);
      for i:=1 to m do
      begin
            read(x,y,v);
            if (map[x,y]>v) then
            begin
                  map[x,y]:=v;
                  map[y,x]:=v;
            end;
      end;

      ans:=maxlongint;
      for i:=1 to n do
      begin
            ok[0,0]:=1;
            ok[0,1]:=i;
            fillchar(vis,sizeof(vis),0);
            dis[i]:=0;
            vis[i]:=true;
            dfs(1,0,0);
      end;

      writeln(ans);

      assoff;
end.
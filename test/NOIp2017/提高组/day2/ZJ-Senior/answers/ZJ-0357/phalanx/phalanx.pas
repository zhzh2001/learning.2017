var
    a:array[0..1005,0..1005]of longint;
    n,m,q,i,j,x,y,tmp0,upk,k:longint;
    b,c:array[0..300005]of longint;

procedure asson;
 begin
       assign(input,'phalanx.in'); reset(input);
       assign(output,'phalanx.out'); rewrite(output);
 end;

procedure assoff;
 begin
       close(input); close(output);
 end;

 {
procedure print;
 var i,j:longint;
 begin
       writeln(' PHALANX');
       for i:=1 to n do
       begin
             for j:=1 to m do write(a[i,j],' ');
             writeln;
       end;
       writeln;
 end;}

procedure special30;
 begin
       for i:=1 to n do
        for j:=1 to m do a[i,j]:=(i-1)*m+j;
       for i:=1 to q do
       begin
             read(x,y);
             tmp0:=a[x,y];
             for j:=y+1 to m do a[x,j-1]:=a[x,j];
             for j:=x+1 to n do a[j-1,m]:=a[j,m];
             writeln(tmp0);
             a[n,m]:=tmp0;
       end;
 end;

procedure special20;
 begin
       for i:=1 to m do b[i]:=i;
       for i:=1 to q do
       begin
             read(x,y);
             tmp0:=b[y];
             for j:=y+1 to m do b[j-1]:=b[j];
             writeln(tmp0);
             b[m]:=tmp0;
       end;
 end;

procedure special10;
 begin
       for i:=1 to m do b[i]:=i;
       for i:=1 to n do c[i]:=i*m;
       k:=1;
       for i:=1 to q do
       begin
             read(x,y);
             if (y<>m) then
             begin
                   tmp0:=b[y];
                   for j:=y+1 to m do b[j-1]:=b[j];
                   upk:=k mod n+1;
                   b[m]:=c[upk];
                   c[k]:=tmp0;
             end
              else tmp0:=c[k];
             k:=k mod n+1;

             writeln(tmp0);
       end;
 end;

begin
      asson;

      read(n,m,q);

      if (n=1) then special20
       else if (n<=1000) then special30
        else special10;

      assoff;
end.

var
    h,r,r2,dr,ubl:real;
    n,i,lb,ub,k,j,l,lbi,t,ti:longint;
    x,y,z:array[0..1005]of real;
    fa,num:array[0..1005]of longint;

procedure asson;
 begin
       assign(input,'cheese.in'); reset(input);
       assign(output,'cheese.out'); rewrite(output);
 end;

procedure assoff;
 begin
       close(input); close(output);
 end;

function find(p:longint):longint;
 begin
       if (fa[p]=p) then exit(p);
       fa[p]:=find(fa[p]);
       exit(fa[p]);
 end;

procedure union(x1,y1:longint);
 var f1,f2:longint;
 begin
       f1:=find(x1); f2:=find(y1);
       if (f1<>f2) then fa[f1]:=fa[f2];
 end;

procedure sort(l,r:longint);
 var i,j,temp,mid:longint;
 begin
       i:=l; j:=r; mid:=num[(l+r) >> 1];
       repeat
              while (z[num[i]]<z[mid]) do inc(i);
              while (z[mid]<z[num[j]]) do dec(j);
              if (i<=j) then
              begin
                    temp:=num[i]; num[i]:=num[j]; num[j]:=temp;
                    inc(i); dec(j);
              end;
       until i>j;
       if (l<j) then sort(l,j);
       if (i<r) then sort(i,r);
 end;

{
function cmp2(i1,j1:longint):boolean;
 begin
       if (sqr(x[i1]-x[j1])+sqr(y[i1]-y[j1])<=r2) then exit(true)
        else exit(false);
 end;}

function cmp3(i1,j1:longint):boolean;
 begin
       if (sqr(x[i1]-x[j1])+sqr(y[i1]-y[j1])+sqr(z[i1]-z[j1])<=r2) then exit(true)
        else exit(false);
 end;

procedure work;
 begin
       read(n,h,r);
       //Set Check-Set , n+1 means z=0 , n+2 means z=h
       for i:=1 to n+2 do fa[i]:=i;
       lb:=n+1; ub:=n+2; ubl:=h-r; r2:=4.0*sqr(r); dr:=2.0*r;
       for i:=1 to n do
       begin
             read(x[i],y[i],z[i]);
             num[i]:=i;
             if (z[i]<=r) then union(i,lb);
             if (z[i]>=ubl) then union(i,ub);
       end;

       if (find(lb)=find(ub)) then
       begin
             writeln('Yes');
             exit;
       end;

       sort(1,n);
       for i:=1 to n do
        if (z[num[i]]>r) then
        begin
              k:=i;
              break;
        end;
       if (k>n) then
       begin
             writeln('No');
             exit;
       end;
       lbi:=1; i:=k; num[n+1]:=n+1; num[0]:=0; z[n+1]:=-1000.0; z[0]:=-1000000000.0;
       while (i<=n) do
       begin
             k:=i;
             while ((i<=n) and (z[num[i+1]]-z[num[k]]<dr)) do inc(i);
             for j:=k to i-1 do
              for l:=j+1 to i do
               if cmp3(num[j],num[l]) then union(num[j],num[l]);
             for j:=k to i do
              for l:=lbi to k-1 do
               if cmp3(num[j],num[l]) then union(num[j],num[l]);
             if (find(lb)=find(ub)) then
             begin
                   writeln('Yes');
                   exit;
             end;
             lbi:=k;
             inc(i);
       end;
       if (find(lb)=find(ub)) then writeln('Yes')
        else writeln('No');
 end;

begin
      asson;

      read(t);
      for ti:=1 to t do work;

      assoff;
end.
#include<bits/stdc++.h>
#define inf 0x3f3f3f3f
using namespace std;
int n,m,cost,fa[14],rudu[14],root,vis[14],timesp;
struct edge{int from,to;}bian[14];
vector<int> lj[14];

int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
inline bool cmp(edge a,edge b){return rudu[a.from]!=rudu[b.from]?rudu[a.from]>rudu[b.from]:a.from<b.from;}

void fileopen()
{
freopen("treasure.in","r",stdin);
freopen("treasure.out","w",stdout);
}

void fileclose()
{
fclose(stdin);
fclose(stdout);
}

void kruskal()
{
int i,sum=0;
for (i=1;i<=n;i++) fa[i]=i;
for (i=1;sum<n-1;i++)
  {
  int u=bian[i].from,v=bian[i].to;
  if (find(u)==find(v)) continue;
  sum++;
  lj[u].push_back(v);
  lj[v].push_back(u);
  u=find(u),v=find(v);
  fa[u]=v;
  }
}

int dfs(int u)
{
if (u==root) return 0;
vis[u]=timesp;
int sum=inf;
for (int i=0;i<lj[u].size();i++)
  {
  int v=lj[u][i];
  if (vis[v]==timesp) continue;
  sum=min(sum,dfs(v)+1);
  }
return sum;
}

inline int read()
{
int x=0;char c=getchar();
for (;!isdigit(c);c=getchar());
for (;isdigit(c);c=getchar()) x=x*10+c-'0';
return x;
}

int main()
{
fileopen();
n=read(),m=read();
int i,j;
for (i=1;i<=m;++i)
  {
  int u=read(),v=read(),w=read();
  if (u>v) swap(u,v);
  rudu[u]++,rudu[v]++;
  bian[i]=edge{u,v};cost=w;
  }
kruskal();
root=bian[1].from;
//printf("%d\n",root);
//for (i=1;i<=n;i++,puts("")) for (j=0;j<lj[i].size();j++) printf("%d ",lj[i][j]);
//for (i=1;i<=m;i++) printf("%d %d\n",bian[i].from,bian[i].to);
int s=0;
for (i=1;i<=n;i++) timesp++,s+=dfs(i)*cost;
printf("%d",s);  
fileclose();
return 0;
}

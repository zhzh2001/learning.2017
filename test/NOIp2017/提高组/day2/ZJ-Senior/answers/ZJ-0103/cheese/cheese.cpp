//#pragma GCC optimize(3)
#include<bits/stdc++.h>
#define boss 1000
#define inf 0x3f3f3f3f
#define lowbit(x) (x&-x)
#define sqr(x) ((x)*(x))
using namespace std;
typedef long long ll;
int n,t,vis[boss+10];ll r,h;bool nico;
struct hole{double x,y,z;int id;}ball[boss+10];

void fileopen()
{
freopen("cheese.in","r",stdin);
freopen("cheese.out","w",stdout);
}

void fileclose()
{
fclose(stdin);
fclose(stdout);
}

void niconiconi()
{
freopen("test.in","r",stdin);
freopen("test.out","w",stdout);
}

//int query(int x){int sum=0;for(;x;x-=lowbit(x))sum+=c[x];return sum;}
//void add(int x,int k){for (;x<=n;x+=lowbit(x)) c[x]+=k;}
inline double dist(hole a,hole b){return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z));}
inline int qieorjiao(hole a,hole b){return dist(a,b)<=2*r;}
inline int xiajiao(hole a){return a.z-r<=0;}
inline int shangjiao(hole a){return a.z+r>=h;}

bool bfs()
{
memset(vis,0,sizeof vis);
queue<hole> q;
for (int i=1;i<=n;i++) if (xiajiao(ball[i])) q.push(ball[i]);
for (hole now;!q.empty();)
  {
  now=q.front(),q.pop();vis[now.id]=1;
//  printf("%d\n",now.id);
  if (shangjiao(now)) return true;
  for (int i=1;i<=n;++i) if (qieorjiao(ball[i],now)&&!vis[i])
    {
    q.push(ball[i]);
    }
  }
return false;
}

inline ll read()
{
ll x=0;int f=1;char c=getchar();
for (;!isdigit(c);c=getchar()) if (c=='-') f=-1;
for (;isdigit(c);c=getchar()) x=x*10+c-'0';
return x*f;
}

int main()
{
fileopen();
//niconiconi();
for (t=read();t--;)
  {
  n=read(),h=read(),r=read();
  int i;
  for (i=1;i<=n;++i) 
    {
    ll x=read(),y=read(),z=read();
    ball[i]=hole{(double)x,(double)y,(double)z,i};
//    printf("%.3lf %.3lf %.3lf\n",ball[i].x,ball[i].y,ball[i].z);
	}
  puts(bfs()?"Yes":"No");
  }
fileclose();
return 0;
}

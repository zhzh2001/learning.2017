#include<bits/stdc++.h>
#define boss 300000
using namespace std;
typedef long long ll;
int n,m,q;ll a[2020][2020];

void fileopen()
{
freopen("phalanx.in","r",stdin);
freopen("phalanx.out","w",stdout);
}

void fileclose()
{
fclose(stdin);
fclose(stdout);
}

void niconiconi()
{
freopen("test.in","r",stdin);
freopen("test.out","w",stdout);
}

inline int read()
{
int x=0;char c=getchar();
for (;!isdigit(c);c=getchar());
for (;isdigit(c);c=getchar()) x=x*10+c-'0';
return x;
}

void kanqi(int x,int y)
{
int i,j;ll t=a[x][y];
for (i=y;i<m;i++) a[x][i]=a[x][i+1];
for (i=x;i<n;i++) a[i][m]=a[i+1][m];
a[n][m]=t;
}

int main()
{
fileopen();
//niconiconi();
n=read(),m=read(),q=read();
int i,j;
for (i=1;i<=n;i++) for (j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
for (i=1;i<=q;i++)
  {
  int x=read(),y=read();
  printf("%lld\n",a[x][y]);
  kanqi(x,y);
  } 
fileclose();
return 0;
}

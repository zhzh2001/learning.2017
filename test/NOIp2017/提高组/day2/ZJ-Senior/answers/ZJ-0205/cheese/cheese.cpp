#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#define ld long double
#define inf (1e-8)
using namespace std;
struct aaa{
	int x,y,z;
}a[233333];
int T,n,H,r,h,t,q[233333];
bool vis[233333];
ld dis(int x,int y)
{
	ld d=pow(pow(a[x].x-a[y].x,2)+pow(a[x].y-a[y].y,2)+pow(a[x].z-a[y].z,2),0.5);
	return d-inf;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>T;
	while(T--)
	{
		scanf("%d%d%d",&n,&H,&r),h=t=0;
		memset(vis,0,sizeof(vis));
		int cc=r*2; //cout<<cc<<endl;
		for(int i=1;i<=n;i++)
		scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
		for(int i=1;i<=n;i++)
		if(a[i].z<=r)q[++t]=i,vis[i]=1;
		//cout<<endl;
		bool flag=0;
		while(h<t)
		{
			int u=q[++h];
			int z=a[u].z;
			if(H-z<=r)
			{
				printf("Yes\n"),flag=1; break;
			}
			for(int i=1;i<=n;i++)
			{
				//cout<<i<<" "<<u<<" "<<dis(i,u)<<" "<<(dis(i,u)<=cc)<<endl;
				if(!vis[i]&&dis(i,u)<=cc)
				{
					//cout<<i<<endl;
					q[++t]=i,vis[i]=1;
					if(H-a[i].z<=r)
					{
						printf("Yes\n"),flag=1; break;
					}
				}
			}
			if(flag)break;
		}
		if(!flag)printf("No\n");
	}
}

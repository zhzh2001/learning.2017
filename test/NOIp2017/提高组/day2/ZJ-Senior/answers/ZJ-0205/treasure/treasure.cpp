#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<vector>
#define inf (0x7f7f7f7f)
#define N (23333)
using namespace std;
struct sss{
	int Si;
	bool con[13][13];
}q[N];
int n,m,E,Ans,Root;
int fi[N],ne[N],b[N],c[N],val[N],dep[N];
bool vis[N];
void add(int x,int y,int z)
{
	ne[++E]=fi[x],fi[x]=E;
	b[E]=y,c[E]=z;
}
void bfs(int S)
{
	int h=0,t=0,ans=0,q[N];
	q[++t]=S,vis[S]=1,dep[S]=1;
	while(h<t)
	{
		int u=q[++h];
		for(int i=fi[u];i;i=ne[i])
		{
			int v=b[i]; if(vis[v])continue; 
			dep[v]=dep[u]+1,ans+=c[i]*dep[u],q[++t]=v,vis[v]=1;
		}
	}
	Ans=min(Ans,ans);
}
void solve1()
{
	Ans=inf;
	for(int i=1;i<=n;i++)
	memset(vis,0,sizeof(vis)),bfs(i);
	printf("%d\n",Ans);
}
void tx(int u)
{
	int cnt=1,ans=0;
	while(cnt<=n)
	{
		int Min=inf,u;
		for(int i=1;i<=n;i++)
		if(val[i]<Min&&!vis[i])Min=val[i],u=i;
		ans+=Min,vis[u]=1;
		for(int i=fi[u];i;i=ne[i])
		{
			int v=b[i];
			if(dep[u]*c[i]<val[v])
			{
				val[v]=dep[u]*c[i];
				dep[v]=dep[u]+1;
			}
		}
		cnt++;
	}
	Ans=min(Ans,ans);
}
void solve2()
{
	Ans=inf;
	for(int i=1;i<=n;i++)
	{
		memset(vis,0,sizeof(vis));
		memset(val,63,sizeof(val));
		memset(dep,0,sizeof(dep));
		val[i]=0,dep[i]=1,tx(i);
	}
	printf("%d\n",Ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m; bool flag=1; int pre;
	for(int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z),add(y,x,z);
		if(i==1)pre=z;
		else if(z!=pre)flag=0;
	}
	if(flag)solve1();
	else solve2();
}

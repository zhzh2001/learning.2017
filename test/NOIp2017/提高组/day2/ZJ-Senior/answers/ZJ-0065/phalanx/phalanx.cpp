#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,q,x,y;
int a[1001][1001];

inline int read(){
	char x=getchar();
	while(x<'0' || x>'9')	x=getchar();
	int res=0;
	while(x>='0' && x<='9'){
		res=(res<<1)+(res<<3)+x-'0';
		x=getchar();
	}
	return res;
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=(i-1)*m+j;
	while(q--){
		x=read();y=read();
		int tmp=a[x][y];
		for(int i=y;i<m;i++)	a[x][i]=a[x][i+1];
		for(int i=x;i<n;i++)	a[i][m]=a[i+1][m];
		a[n][m]=tmp;
		cout<<a[n][m]<<endl;
	}
	return 0;
}

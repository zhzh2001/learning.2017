#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#define N 13
#define Inf 0x3f3f3f3f
using namespace std;
int n,m,x,y,z,res,ans=Inf;
int dis[N][N];
int fa[N],p[N];
int l[N],r[N];

int get(int x){
	return fa[x]==x?x:fa[x]=get(fa[x]);
}

void merge(int x,int y){
	fa[get(x)]=get(y);
}

bool ask(int x,int y){
	return get(x)==get(y);
}

void calc(int x,int cnt){
	if(r[x]){
		res+=cnt*dis[p[x]][r[x]];
		calc(r[x],cnt);
	}
	if(l[x]){
		res+=(cnt+1)*dis[x][l[x]];
		calc(l[x],cnt+1);
	}
	return;
}

void check(int root){
	memset(l,0,sizeof(l));
	memset(r,0,sizeof(r));
	for(int i=1;i<=n;i++){
		if(i==root)	continue;
		r[i]=l[p[i]];
		l[p[i]]=i;
	}
	res=0;
	calc(root,0);
	ans=min(ans,res);
}

void dfs(int x,int root){
	if(x==root){
		p[x]=x;
		dfs(x+1,root);
		return;
	}
	if(x==n+1){
		check(root);
		return;
	}
	for(int i=1;i<=n;i++){
		if(dis[x][i]<Inf && ask(x,i)==0){
			int tmp[N];
			for(int j=1;j<=n;j++)	tmp[j]=fa[j];
			p[x]=i;
			merge(x,i);
			dfs(x+1,root);
			for(int j=1;j<=n;j++)	fa[j]=tmp[j];
		}
	}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dis,0x3f,sizeof(dis));
	for(int i=1;i<=n;i++)	fa[i]=i;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		dis[x][y]=min(dis[x][y],z);
		dis[y][x]=dis[x][y];
	}
	for(int i=1;i<=n;i++)	dfs(1,i);
	cout<<ans;
	return 0;
}

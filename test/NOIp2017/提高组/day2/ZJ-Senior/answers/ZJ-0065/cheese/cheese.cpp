#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define N 1001
using namespace std;
typedef long long LL;
LL T,n,h,r;
LL x[N],y[N],z[N];
int fa[N];
double dis(LL i,LL j){
	return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
}

int get(int x){
	return fa[x]==x?x:fa[x]=get(fa[x]);
}

void merge(int x,int y){
	fa[get(x)]=get(y);
}

bool ask(int x,int y){
	return get(x)==get(y);
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>T;
	while(T--){
		cin>>n>>h>>r;
		for(int i=0;i<=n+1;i++)	fa[i]=i;
		for(int i=1;i<=n;i++)	cin>>x[i]>>y[i]>>z[i];
		for(int i=1;i<=n;i++){
			if(z[i]-r<=0)	merge(0,i);
			if(z[i]+r>=h)	merge(i,n+1);
			for(int j=i;j<=n;j++)
				if(dis(i,j)<=2*r)	merge(i,j);
		}
		if(ask(0,n+1))	cout<<"Yes"<<endl;
		else cout<<"No"<<endl;	
	}
	return 0;
}

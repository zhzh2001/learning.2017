#include <cstdio>
#include <cstring>
#define file_io(xxx)\
	do\
	{\
		freopen(xxx".in", "r", stdin);\
		freopen(xxx".out", "w", stdout);\
	}\
	while (0)
#define INF 0xffffffff
typedef long long ll;
inline void read(int &a)
{
	char c;
	while ((c = getchar()) >> 5 != 1);
	bool neg = 0;
	if (c == '-')
	{
		neg = 1;
		c = getchar();
	}
	a = 0;
	do
		a = a * 10 + c - '0';
	while ((c = getchar()) >> 4 == 3);
	if (neg)
		a = -a;
}
#define N 8
unsigned adj[N][N];
bool usd[N * (N + 1) / 2];
int map[N * (N - 1) / 2][2], cnt_map;
int n;
struct info
{
	unsigned sum, ans;
	info() {}
	info(const unsigned _sum, const unsigned _ans) : sum(_sum), ans(_ans) {}
};
struct edge
{
	int v, w, nxt;
} e[N * 2];
int head[N], cnt_e;
void add_edge(const int u, const int v, const int w)
{
	{
		edge &ee = e[++cnt_e];
		ee.v = v;
		ee.w = w;
		ee.nxt = head[u];
		head[u] = cnt_e;
	}
	{
		edge &ee = e[++cnt_e];
		ee.v = u;
		ee.w = w;
		ee.nxt = head[v];
		head[v] = cnt_e;
	}
}
bool vis[N];
void dfs_vis(const int u, const int fr)
{
	vis[u] = 1;
	for (int i = head[u]; i != 0; i = e[i].nxt)
		if (e[i].v != fr)
		{
			if (vis[e[i].v])
				return;
			dfs_vis(e[i].v, u);
		}
}
info dp_down[N];
info dfs_down(const int u, const int fr)
{
	unsigned sum = 0, ans = 0;
	for (int i = head[u]; i != 0; i = e[i].nxt)
		if (e[i].v != fr)
		{
			const info tmp = dfs_down(e[i].v, u);
			sum += tmp.sum + e[i].w;
			ans += tmp.ans;
		}
	return dp_down[u] = info(sum, ans + sum);
}
info dp_up[N];
void dfs_up(const int u, const int fr)
{
	unsigned w = 0;
	for (int i = head[u]; i != 0; i = e[i].nxt)
		w += e[i].w;
	const unsigned ans_d = dp_down[u].ans - dp_down[u].sum;
	for (int i = head[u]; i != 0; i = e[i].nxt)
		if (e[i].v != fr)
		{
			dp_up[e[i].v].sum = dp_down[0].sum - dp_down[e[i].v].sum;
			dp_up[e[i].v].ans = dp_up[e[i].v].sum + dp_up[u].ans
				+ (ans_d - dp_down[e[i].v].ans) + w - e[i].w;
			dfs_up(e[i].v, u);
		}
}
unsigned ans = INF;
void calc()
{
	memset(head, 0, sizeof head);
	cnt_e = 0;
	for (int i = cnt_map - 1; i >= 0; --i)
		if (usd[i])
		{
			const int u = map[i][0], v = map[i][1];
			add_edge(u, v, adj[u][v]);
		}
	memset(vis, 0, sizeof vis);
	dfs_vis(0, 0);
	for (int i = 0; i < n; ++i)
		if (!vis[i])
			return;
	dfs_down(0, 0);
	dp_up[0].sum = 0;
	dp_up[0].ans = 0;
	dfs_up(0, 0);
	for (int i = 0; i < n; ++i)
	{
		const unsigned tmp = dp_up[i].ans + dp_down[i].ans;
		if (ans > tmp)
			ans = tmp;
	}
}
void select(int n, const int m)
{
	if (m == -1)
	{
		calc();
		return;
	}
	while (n >= m)
	{
		usd[n] = 1;
		select(n - 1, m - 1);
		usd[n] = 0;
		--n;
	}
}
int main()
{
	file_io("treasure");
	memset(adj, -1, sizeof adj);
	int m;
	read(n);
	read(m);
	while (m--)
	{
		int u, v, w;
		read(u);
		read(v);
		read(w);
		--u;
		--v;
		if (adj[u][v] > (unsigned)w)
		{
			adj[u][v] = w;
			adj[v][u] = w;
		}
	}
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < i; ++j)
			if (adj[i][j] != INF)
			{
				map[cnt_map][0] = i;
				map[cnt_map][1] = j;
				++cnt_map;
			}
	select(cnt_map - 1, n - 2);
	printf("%u\n", ans);
	return 0;
}

#include <cstdio>
#define file_io(xxx)\
	do\
	{\
		freopen(xxx".in", "r", stdin);\
		freopen(xxx".out", "w", stdout);\
	}\
	while (0)
typedef long long ll;
inline void read(int &a)
{
	char c;
	while ((c = getchar()) >> 5 != 1);
	bool neg = 0;
	if (c == '-')
	{
		neg = 1;
		c = getchar();
	}
	a = 0;
	do
		a = a * 10 + c - '0';
	while ((c = getchar()) >> 4 == 3);
	if (neg)
		a = -a;
}
inline ll sqr(const int a)
{
	return ll(a) * a;
}
#define N 1005
struct pt
{
	int x, y, z;
} p[N];
int f[N + 2];
void init(const int n)
{
	for (int i = n - 1; i >= 0; --i)
		f[i] = i;
}
int find(const int a)
{
	return f[a] == a ? a : f[a] = find(f[a]);
}
void unite(const int a, const int b)
{
	f[find(a)] = find(b);
}
int main()
{
	file_io("cheese");
	int t;
	read(t);
	while (t--)
	{
		int n, h, r;
		read(n);
		read(h);
		read(r);
		init(n + 2);
		const ll sqr_r = sqr(r << 1);
		const int lower = n, upper = n + 1;
		for (int i = 0; i < n; ++i)
		{
			int x, y, z;
			read(x);
			read(y);
			read(z);
			if (z - r <= 0)
				unite(i, lower);
			if (z + r >= h)
				unite(i, upper);
			for (int j = 0; j < i; ++j)
				if (sqr(x - p[j].x) + sqr(y - p[j].y) + sqr(z - p[j].z)
					 <= sqr_r)
					unite(i, j);
			p[i].x = x;
			p[i].y = y;
			p[i].z = z;
		}
		puts(find(upper) == find(lower) ? "Yes" : "No");
	}
	return 0;
}

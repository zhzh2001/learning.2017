#include <cstdio>
#include <vector>
#define file_io(xxx)\
	do\
	{\
		freopen(xxx".in", "r", stdin);\
		freopen(xxx".out", "w", stdout);\
	}\
	while (0)
#define INF 0xffffffff
typedef long long ll;
inline void read(int &a)
{
	char c;
	while ((c = getchar()) >> 5 != 1);
	bool neg = 0;
	if (c == '-')
	{
		neg = 1;
		c = getchar();
	}
	a = 0;
	do
		a = a * 10 + c - '0';
	while ((c = getchar()) >> 4 == 3);
	if (neg)
		a = -a;
}
void print(int a)
{
	char buff[10], *ptr = buff + 9;
	*ptr = '\0';
	do
		*--ptr = a % 10 + '0';
	while (a /= 10);
	puts(ptr);
}
int mat[1000][1000];
std::vector <int> mat_1;
int main()
{
	file_io("phalanx");
	int n, m, q;
	read(n);
	read(m);
	read(q);
	if (n <= 1000 && m <= 1000)
	{
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < m; ++j)
				mat[i][j] = i * m + j + 1;
		while (q--)
		{
			int i, j;
			read(i);
			read(j);
			--i;
			const int tmp = mat[i][j - 1];
			print(tmp);
			int *const mat_i = mat[i];
			for (; j < m; ++j)
				mat_i[j - 1] = mat_i[j];
			--j;
			for (++i; i < n; ++i)
				mat[i - 1][j] = mat[i][j];
			mat[i - 1][j] = tmp;
		}
	}
	else if (n == 1)
	{
		for (int j = 1; j <= m; ++j)
			mat_1.push_back(j);
		while (q--)
		{
			int i, j;
			read(i);
			read(j);
			const int tmp = mat_1[j - 1];
			print(tmp);
			mat_1.erase(mat_1.begin() + j - 1);
			mat_1.push_back(tmp);
		}
	}
	return 0;
}

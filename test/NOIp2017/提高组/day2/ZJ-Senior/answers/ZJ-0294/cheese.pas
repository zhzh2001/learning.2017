type zb=record x,y,z:int64; end;
var a:array[1..1000]of zb;
    b,e:array[1..1000]of boolean;
    q:array[1..1000]of longint;
    n,x,i,j,t,tt,head,tail:longint;
    ans:boolean;
    h,r:int64;
function dis(x,y:longint):int64;
begin
  dis:=(a[x].x-a[y].x)*(a[x].x-a[y].x)+
       (a[x].y-a[y].y)*(a[x].y-a[y].y)+
       (a[x].z-a[y].z)*(a[x].z-a[y].z);
end;
begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  read(tt);
  for t:=1 to tt do
  begin
    read(n,h,r);
    fillchar(b,sizeof(b),false);
    fillchar(e,sizeof(e),false);
    head:=0;tail:=0;
    for i:=1 to n do
    begin
      read(a[i].x,a[i].y,a[i].z);
      if a[i].z<=r then
      begin
        inc(tail);
        q[tail]:=i;
        b[i]:=true;
      end;
      if a[i].z>=h-r then e[i]:=true;
    end;
    ans:=false;
    while head<tail do
    begin
      inc(head);
      x:=q[head];
      for i:=1 to n do
      if (not b[i])and(dis(i,x)<=4*r*r) then
      begin
        if e[i] then begin ans:=true; break; end;
        inc(tail);
        q[tail]:=i;
        b[i]:=true;
      end;
      if ans then break;
    end;
    if ans then writeln('Yes') else writeln('No');
  end;
  close(input);
  close(output);
end.

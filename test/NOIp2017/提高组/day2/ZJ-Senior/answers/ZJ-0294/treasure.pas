var a:array[1..12,1..12]of qword;
    b:array[1..12]of boolean;
    d:array[1..12,0..1]of qword;
    n,m,i,j,x,y,z,min,sum,ans:longint;
procedure dfs(x:longint);
var i,j:longint;
begin
  b[x]:=true;
  for i:=1 to n do
  if (b[i]=false)and(a[x,i]<>maxlongint) then
  begin
    for j:=1 to n do
    if (a[i,j]<>maxlongint)and(b[j]) then
    begin
      if (d[j,0]+1)*a[i,j]<d[i,0]*a[i,d[i,1]] then begin d[i,0]:=d[j,0]+1;d[i,1]:=j; end;
    end;
    dfs(i);
  end;
end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  read(n,m);
  for i:=1 to n do
    for j:=1 to n do
    a[i,j]:=maxlongint;
  for i:=1 to m do
  begin
    read(x,y,z);
    if x=y then continue;
    if z<a[x,y] then a[x,y]:=z;
    if z<a[y,x] then a[y,x]:=z;
  end;
    ans:=maxlongint;
    for i:=1 to n do
    begin
      fillchar(b,sizeof(b),false);
      for j:=1 to n do
      begin
        d[j,0]:=maxlongint;
        d[j,1]:=j;
      end;
      d[i,0]:=0;
      dfs(i);
      sum:=0;
      for j:=1 to n do
      if j<>i then sum:=sum+d[j,0]*a[j,d[j,1]];
      if sum<ans then ans:=sum;
    end;
  write(ans);
  close(input);
  close(output);
end.

#include<cstdio>
#include<vector>
#include<algorithm>
#define ll long long
using namespace std;
const int N=3e5+5,P=63*N;
int n,m,Q,nr,nt;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
int nd,top[N],ls[P],rs[P],sum[P];
void add(int px,int x){
	if (!top[px]) top[px]=++nd;
	for (int p=top[px],l=1,r=nr;;){
		sum[p]++;if (l==r) return;int m=l+r>>1; 
		if (x<=m){if (!ls[p]) ls[p]=++nd;p=ls[p];r=m;}
		else{if (!rs[p]) rs[p]=++nd;p=rs[p];l=m+1;}
	}
}
int ask(int p,int k){
	p=top[p];
	for (int l=1,r=nr;;){
		if (l==r) return l;
		int m=l+r>>1,lz=m-l+1-sum[ls[p]];
		if (k<=lz) p=ls[p],r=m;
		else k-=lz,p=rs[p],l=m+1;
	}
}
vector<ll> a[N];
void work(){
	int x=read(),y=read(),px;ll k;
	px=ask(nt,x);k=px<=n?(ll)px*m:a[nt][px-n-1];
	a[x].push_back(k);add(x,ask(x,m));	
	px=ask(x,y);
	k=px<=m?(ll)(x-1)*m+px:a[x][px-m-1];
	write(k);putchar('\n');
	add(x,px);a[nt].push_back(k);
	add(nt,ask(nt,x));
	px=ask(nt,x);
	k=px<=n?(ll)px*m:a[nt][px-n-1];
	a[x].push_back(k);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();Q=read();
	nr=max(n+Q,m+2*Q);nt=n+1;
	for (;Q--;) work();
}

#include<cstdio>
#include<algorithm>
#define ll long long
#define ull unsigned long long
using namespace std;
const int N=1005,M=N*N;
int n,m,r,S,T,i,j;ull nr;
struct arr{int x,y,z;}a[N];
int read(){
	char c=getchar();int k=0,p=0;
	for (;c<48||c>57;c=getchar()) if (c=='-') p=1;
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;
	return p?-k:k;
}
int et,he[N];bool vis[N];
struct edge{int l,to;}e[M];
void add(int x,int y){
	e[++et].l=he[x];he[x]=et;e[et].to=y;
}
ull dis(arr A,arr B){
	int dx=A.x-B.x,dy=A.y-B.y,dz=A.z-B.z;
	ull px=(ll)dx*dx,py=(ll)dy*dy,pz=(ll)dz*dz;
	return px+py+pz;
}
void dfs(int x){
	vis[x]=1;
	for (int i=he[x];i;i=e[i].l)
		if (!vis[e[i].to]) dfs(e[i].to);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (int TEST=read();TEST--;){
		n=read();m=read();r=read();
		nr=4ll*r*r;S=n+1;T=n+2;et=0;
		for (i=1;i<=T;i++) he[i]=0,vis[i]=0;
		for (i=1;i<=n;i++){
			a[i]=(arr){read(),read(),read()};
			if (a[i].z-r<=0) add(S,i);
			if (a[i].z+r>=m) add(i,T);
		}
		for (i=2;i<=n;i++) for (j=1;j<i;j++)
			if (dis(a[i],a[j])<=nr) add(i,j),add(j,i);
		dfs(S);
		if (vis[T]) puts("Yes"); else puts("No");
	}
}

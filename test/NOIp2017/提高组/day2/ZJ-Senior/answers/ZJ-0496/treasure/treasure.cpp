#include<cstdio>
#include<algorithm>
using namespace std;
const int N=20,E=2005,M=1<<12,inf=1e8;
int n,m,nm,i,j,k,d[N],g[M][M],f[M];
int et,he[N];
struct edge{int l,to,v;}e[E];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);nm=1<<n;
	for (i=1;i<=m;i++){
		int x,y,v;scanf("%d%d%d",&x,&y,&v);
		e[++et].l=he[x];he[x]=et;e[et].to=y;e[et].v=v;
		e[++et].l=he[y];he[y]=et;e[et].to=x;e[et].v=v;
	}
	for (k=1;k<nm;k++){
		for (i=1;i<=n;i++) d[i]=inf;
		for (int x=1;x<=n;x++) if (k&(1<<x-1))
			for (i=he[x];i;i=e[i].l)
				d[e[i].to]=min(d[e[i].to],e[i].v);
		for (i=k;i<nm;i=(i+1)|k){
			int sum=0;
			for (j=0;j<n;j++)
				if ((i&(1<<j))&&!(k&(1<<j)))
					sum+=d[j+1];
			g[k][i]=sum>inf?inf:sum;
		}
	}
	for (i=0;i<nm;i++) f[i]=inf;
	for (i=1;i<=n;i++) f[1<<i-1]=0;
	for (i=1;i<n;i++){
		for (j=nm;j--;) if (f[j]!=inf)
			for (k=j;k<nm;k=(k+1)|j)
				f[k]=min(f[k],f[j]+g[j][k]*i);
	}
	printf("%d",f[nm-1]);
}

#include <cmath>
#include <cstdio>
#include <vector>
#include <cstring>
using std::vector;

const int kMaxN = 1e3 + 5;

int T;
int n, h, r;
bool vis[kMaxN];
struct Ball {
	int x, y, z;
}ball[kMaxN];
inline double Dis(const Ball &x, const Ball &y) {
	return sqrt(pow(x.x - y.x, 2) + pow(x.y - y.y, 2)
							+ pow(x.z - y.z, 2)) - 1e-9;
}

vector<int> dst[kMaxN];
inline void Init() {
	memset(vis, 0, sizeof vis);
	for (int i = 0; i <= n + 1; i++)
		dst[i].clear();
}
inline void AddEdge(const int &u, const int &v) {
	dst[u].push_back(v);
	dst[v].push_back(u);
}

void DFS(const int &u) {
	const int size = dst[u].size();
	for (int i = 0; i < size; i++) {
		if (!vis[dst[u][i]]) {
			vis[dst[u][i]] = 1;
			DFS(dst[u][i]);
		}
	} 
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		scanf("%d%d%d", &n, &h, &r);
		Init();
		for (int i = 1; i <= n; i++) {
			scanf("%d%d%d", &ball[i].x, &ball[i].y, &ball[i].z);
			if (ball[i].z <= r)
				AddEdge(0, i);
			if (h - ball[i].z <= r)
				AddEdge(n + 1, i);
		}
		r <<= 1;
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j < i; j++) {
				if (Dis(ball[i], ball[j]) <= r)
					AddEdge(i, j);
			}
		}
		vis[n + 1] = 1;
		DFS(n + 1);
		puts(vis[0] ? "Yes" : "No");
	}
	return 0;
}

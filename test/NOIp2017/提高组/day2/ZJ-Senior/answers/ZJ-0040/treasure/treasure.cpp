#include <cstdio>
#include <vector>
#include <cstring>
using std::vector;

inline int min(const int &x, const int &y) {
	return x < y ? x : y;
}

const int kMaxN = 12 + 5;
const int inf = 0x3f3f3f3f;

int ans;
int n, m;
int dis[kMaxN][kMaxN];

struct Edge {
	int dst, val;
};
vector<Edge> G[kMaxN * kMaxN];
inline int code(const int x, const int y) {
	return n * y + x - 1;
}
inline int decode(const int x) {
	return x % n + 1;
}
void AddEdge(const int &u, const int &v, const int &w) {
	G[u].push_back((Edge){v, w});
}

bool vis[kMaxN];
int prim_dis[kMaxN], deep[kMaxN];
int GetAns(int root) {
	int ret = 0;
	memset(vis, 0, sizeof vis);
	memset(prim_dis, 0x3f, sizeof prim_dis);
	vis[root] = 1;
	deep[root] = 0;
	prim_dis[root] = 0;
	int u = code(root, 0);
	for (int i = G[u].size() - 1; i >= 0; i--) {
		const int v = decode(G[u][i].dst);
		prim_dis[v] = G[u][i].val;
		deep[v] = 1;
	}
	for (int i = 2; i <= n; i++) {
		int min_dis = inf, min_id = 0;
		for (int i = 1; i <= n; i++) {
			if (!vis[i] && prim_dis[i] < min_dis) {
				min_id = i;
				min_dis = prim_dis[i];
			}
		} 
		ret += min_dis;
		vis[min_id] = 1;
		u = code(min_id, deep[min_id]);
		for (int i = G[u].size() - 1; i >= 0; i--) {
			const int v = decode(G[u][i].dst);
			if (prim_dis[v] > G[u][i].val) {
				deep[v] = deep[min_id] + 1;
				prim_dis[v] = G[u][i].val;
			}
		}
	}
	return ret;
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	ans = inf;
	memset(dis, 0x3f, sizeof dis);
	scanf("%d%d", &n, &m);
	for (int i = 1, u, v, w; i <= m; i++) {
		scanf("%d%d%d", &u, &v, &w);
		dis[u][v] = min(dis[u][v], w);
		dis[v][u] = dis[u][v];
	}
	for (int i = 1; i <= n; i++) {
			for (int k = 1; k <= n; k++) {
				if (i != k && dis[i][k] != inf) {
					for (int j = 0; j < n - 1; j++)
						AddEdge(code(i, j), code(k, j + 1), (j + 1) * dis[i][k]);
			}
		}
	}
	for (int i = 1; i <= n; i++)
		ans = min(ans, GetAns(i));
	printf("%d\n", ans);
	return 0;
}

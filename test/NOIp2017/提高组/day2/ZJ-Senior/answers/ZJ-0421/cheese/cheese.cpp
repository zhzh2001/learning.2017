#include<bits/stdc++.h>
#define Fi first
#define Se second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int N=1010,INF=0x3f3f3f3f;
template<class T>inline bool chkmin(T &A,T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,T B){return A<B?A=B,1:0;}
int n,h,r,x[N],y[N],z[N];
bool vis[N];
inline bool check(int a,int b){
	if(a==0)return b!=n+1&&z[b]<=r;
	if(b==n+1)return a!=0&&h-z[a]<=r;
	ll tot=0;
	tot+=1ll*(x[a]-x[b])*(x[a]-x[b]);
	tot+=1ll*(y[a]-y[b])*(y[a]-y[b]);
	tot+=1ll*(z[a]-z[b])*(z[a]-z[b]);
	return 4ll*r*r>=tot;
}
bool dfs(int now){
	vis[now]=true;
	if(now==n+1)return true;
	FOR(i,1,n+1)if(!vis[i])
		if(check(now,i)&&dfs(i))return true;
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--){
		memset(vis,false,sizeof(vis));
		scanf("%d%d%d",&n,&h,&r);
		FOR(i,1,n)scanf("%d%d%d",&x[i],&y[i],&z[i]);
		if(dfs(0))puts("Yes");
		else puts("No");
	}
	return 0;
}

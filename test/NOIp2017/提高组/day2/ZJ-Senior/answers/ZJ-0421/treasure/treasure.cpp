#include<bits/stdc++.h>
#define Fi first
#define Se second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int N=20,INF=0x3f3f3f3f;
template<class T>inline bool chkmin(T &A,T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,T B){return A<B?A=B,1:0;}
int n,m,ans=INF,mp[N][N];
struct SHUI1{
	int dep[N],to[N][N];
	int Find(int x,int par[N]){
		if(par[x]==x)return x;
		return par[x]=Find(par[x],par);
	}
	int solve(int x,int f){
		int ans=0;
		FOR(i,1,n)if(i!=f&&to[i][x]){
			ans+=(dep[x]+1)*mp[i][x];
			dep[i]=dep[x]+1;
			ans+=solve(i,x);
		}
		return ans;
	}
	int calc(){
		int res=INF;
		FOR(i,1,n){
			dep[i]=0;
			chkmin(res,solve(i,i));
		}
		return res;
	}
	void dfs(int x,int par[N]){
		int tmp[N];
		if(x==n)chkmin(ans,calc());
		FOR(i,1,n){
			int tt1=Find(i,par),tt2=Find(x,par);
			if(mp[i][x]!=INF&&tt1!=tt2){
				memcpy(tmp,par,sizeof(tmp));
				tmp[tt1]=tt2;
				to[x][i]=to[i][x]=true;
				dfs(x+1,tmp);
				to[x][i]=to[i][x]=false;
			}
		}
	}
	void Main(){
		int par[N];
		FOR(i,1,n)par[i]=i;
		dfs(1,par);
		printf("%d\n",ans);
	}
}P1;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int x,y,z;
	scanf("%d%d",&n,&m);
	memset(mp,63,sizeof(mp));
	FOR(i,1,m){
		scanf("%d%d%d",&x,&y,&z);
		chkmin(mp[x][y],z);
		chkmin(mp[y][x],z);
	}
	P1.Main();
	return 0;
}

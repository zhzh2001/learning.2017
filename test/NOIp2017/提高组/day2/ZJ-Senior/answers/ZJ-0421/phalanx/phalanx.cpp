#include<bits/stdc++.h>
#define Fi first
#define Se second
#define FOR(a,b,c) for(int a=(b),a##_end=(c);a<=a##_end;++a)
#define ROF(a,b,c) for(int a=(b),a##_end=(c);a>=a##_end;--a)
using namespace std;
typedef long long ll;
typedef pair<int,int> PII;
const int N=3e5+10,INF=0x3f3f3f3f;
template<class T>inline bool chkmin(T &A,T B){return B<A?A=B,1:0;}
template<class T>inline bool chkmax(T &A,T B){return A<B?A=B,1:0;}
int n,m,q,x[N],y[N];
struct SHUI1{
	static const int Maxn=1010;
	bool check(){return max(n,m)<Maxn;}
	ll A[Maxn][Maxn];
	void Main(){
		FOR(i,1,n)FOR(j,1,m)A[i][j]=(i-1)*m+j;
		FOR(i,1,q){
			int X=x[i],Y=y[i];
			ll tmp=A[X][Y];
			printf("%lld\n",tmp);
			FOR(j,Y+1,m)A[X][j-1]=A[X][j];
			FOR(j,X+1,n)A[j-1][m]=A[j][m];
			A[n][m]=tmp;
		}
	}
}P1;
struct SHUI2{
	bool check(){return q<=500;}
	vector<ll>E[N];
	void Main(){
		FOR(i,1,n)E[i].push_back(1ll*i*m);
		FOR(i,1,q){
			int X=x[i],Y=y[i];
			int Sz=E[X].size();
			ROF(j,m-Sz,Y)E[X].push_back(1ll*m*(X-1)+j);
			ll tmp=E[X][m-Y];//下标从0开始
			printf("%lld\n",tmp);
			E[X].erase(E[X].begin()+m-Y);
			FOR(i,X+1,n){
				ll tt=E[i][0];
				E[i-1].insert(E[i-1].begin(),tt);
				E[i].erase(E[i].begin());
			}
			E[n].insert(E[n].begin(),tmp);
		}
	}
}P2;
struct SHUI3{
	bool check(){
		FOR(i,1,q)if(x[i]!=1)return false;
		return true;
	}
	vector<ll>E;
	void Main(){
		FOR(i,1,m)E.push_back(i);
		FOR(i,2,n)E.push_back(1ll*i*m);
		FOR(i,1,q){
			int Y=y[i];
			ll tmp=E[Y-1];
			printf("%lld\n",tmp);
			E.erase(E.begin()+Y-1);
			E.push_back(tmp);
		}
	}
}P3;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	FOR(i,1,q)scanf("%d%d",&x[i],&y[i]);
	if(P1.check())P1.Main();
	else if(P2.check())P2.Main();
	else P3.Main();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<ctime>
#include<stdlib.h>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<cmath>
#include<iostream>
#include<algorithm>
#define M 1005
#define ll long long 
#define db double
#define oo 1000000000
using namespace std;
int n,h,r,ans;
db eps=1e-7;
struct node{
	int x,y,z;
}A[M];
bool mark[M][M],vis[M];
bool check(int a,int b){
	int w1=A[b].x-A[a].x;
	int w2=A[b].y-A[a].y;
	int w3=A[b].z-A[a].z;
	db dis=sqrt(1LL*w1*w1+1LL*w2*w2+1LL*w3*w3);
//	printf("dis==%.4f\n",dis);
	if(dis<=2*r)return 1;
	return 0;
}	
void dfs(int now){
	//printf("now%d\n",now);
	vis[now]=1;
	if(now==n+1){
		return ;
	}
	for(int i=0;i<=n+1;i++){
		if(mark[now][i]&&!vis[i]){
			dfs(i);
		}
	}
}
int main(){
	int i,j,T;
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>T;
	while(T--){
		memset(mark,0,sizeof(mark));
		memset(vis,0,sizeof(vis));
		scanf("%d %d %d",&n,&h,&r);
		for(int i=1;i<=n;i++){
			scanf("%d %d %d",&A[i].x,&A[i].y,&A[i].z);
			if(A[i].z-r<=0){
			//	printf("aoe==%d\n",i);
				mark[0][i]=mark[i][0]=1;
			}
			if(A[i].z+r>=h){
				//printf("aoe=%d %d\n",i,n+1);
				mark[i][n+1]=mark[n+1][i]=1;
			}
		}
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				if(check(i,j))mark[i][j]=mark[j][i]=1;
			}
		}	
		dfs(0);
		if(vis[n+1]){
			puts("Yes");
		}else puts("No");
	}
	return 0;
}
/*
3
2 4 1
0 0 1
0 0 3
2 5 1
0 0 1
0 0 4
1
2 5 2
0 0 2
2 0 4
*/

#include<cstdio>
#include<cstring>
#include<ctime>
#include<stdlib.h>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<cmath>
#include<iostream>
#include<algorithm>
#define M 1005
#define ll long long 
#define db double
#define oo 1000000000
using namespace std;
int n,m;
struct node{
	int to,v;
};
vector<node>vec[M];
struct P1{
	int ans,res;
	void dfs(int now,int f,int d){
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			if(tmp.to==f)continue;
			dfs(tmp.to,now,d+1);
			res+=tmp.v*(d+1);
		}
	}
	void solve(){
		res=0;
		ans=oo;
		for(int i=1,a,b,c;i<=m;i++){
			scanf("%d %d %d",&a,&b,&c);
			vec[a].push_back((node){b,c});
			vec[b].push_back((node){a,c});
		}
		for(int i=1;i<=n;i++){
			res=0;
			dfs(i,0,0);
			if(res<ans)ans=res;
		}
		printf("%d\n",ans);
	}
}bao;
struct P2{
	int val[20][20],res,ans,dep[20],cnt;
	bool mark[20];
	void chk(int &a,int b){
		if(a>b)a=b;
	}
	void dfs(int now){
		if(now==n+1){
			if(res<ans)ans=res;
			return;
		}
		for(int i=1;i<=n;i++){
			if(mark[i]){
				for(int j=1;j<=n;j++){
					if(!mark[j]&&val[i][j]<=oo){
						mark[j]=1;
						dep[j]=dep[i]+1;
						res+=val[i][j]*dep[j];
						dfs(now+1);
						res-=val[i][j]*dep[j];
						mark[j]=0;
					}
				}
			}
		}
	}
	void solve(){
		memset(dep,0,sizeof(dep));
		memset(val,63,sizeof(val));
		res=0;ans=oo;
		for(int i=1,a,b,c;i<=m;i++){
			scanf("%d %d %d",&a,&b,&c);
			chk(val[a][b],c);
			chk(val[b][a],c);
		}
		for(int i=1;i<=n;i++){
			dep[i]=0;
			mark[i]=1; 
			dfs(2);
			mark[i]=0;
		}
		printf("%d\n",ans);
	}
}da;
struct P3{
	int dp[20][4200],f[4200][4200],g[20][4200];
	int val[20][20],res,ans;
	bool mark[20];
	void chk(int &a,int b){
		if(a==-1||a>b)a=b;
	}
	void solve(){
		ans=-1;
		memset(val,-1,sizeof(val));
		for(int i=1,a,b,c;i<=m;i++){
			scanf("%d %d %d",&a,&b,&c);
			chk(val[a][b],c);
			chk(val[b][a],c);
		}
		int w=1<<n;
		for(int i=0;i<w;i++){
			for(int j=1;j<=n;j++){
				g[j][i]=-1;
				if((1<<(j-1))&i)continue;
				for(int q=1;q<=n;q++){
					if((1<<(q-1))&i){
						if(~val[q][j])chk(g[j][i],val[q][j]);
					}
				}
			//	printf("aoe==%d %d %d\n",j,i,g[j][i]);
			}
		}
		for(int i=0;i<w;i++){
			for(int j=0;j<w;j++){
				f[i][j]=-1;
				if(i&j)continue;
				int sum=0;
				for(int q=1;q<=n;q++){
					if(j&(1<<(q-1))){
						if(~g[q][i]){
							sum+=g[q][i];
						}
						else {
							sum=-1;
							break;
						}
					}
				}
				f[i][j]=sum;
			}
		}
		//printf("%d\n",f[1][14]);
		memset(dp,-1,sizeof(dp));
		for(int i=0;i<n;i++){
			dp[0][1<<i]=0;
		}
		for(int i=1;i<=n;i++){
			for(int j=0;j<w;j++){
				if(~dp[i-1][j]){
					for(int q=0;q<w;q++){
						if((j|q)==q){
							if(f[j][q-j])chk(dp[i][q],dp[i-1][j]+f[j][q-j]*i); 
						}
					}
				}
			}
			if(~dp[i][w-1])chk(ans,dp[i][w-1]);
		}
		printf("%d\n",ans);
	}
}qing;
int main(){
	int i,j;
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	if(n==m+1){
		bao.solve();
	}
	else if(n<=6)da.solve();
	else qing.solve();
	return 0;
}
/*
4 3
1 2 1
1 3 3
1 4 1
*/

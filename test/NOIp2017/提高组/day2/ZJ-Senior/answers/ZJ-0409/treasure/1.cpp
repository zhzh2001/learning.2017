#include<cstdio>
#include<cstring>
#include<ctime>
#include<stdlib.h>
#include<vector>
#include<queue>
#include<map>
#include<set>
#include<cmath>
#include<iostream>
#include<algorithm>
#define M 1005
#define ll long long 
#define db double
#define oo 1000000000
using namespace std;
int n,m;
struct node{
	int to,v;
};
vector<node>vec[M];
struct P1{
	int ans,res;
	void dfs(int now,int f,int d){
		for(int i=0;i<vec[now].size();i++){
			node tmp=vec[now][i];
			if(tmp.to==f)continue;
			dfs(tmp.to,now,d+1);
			res+=tmp.to*(d+1);
		}
	}
	void solve(){
		res=0;
		ans=oo;
		for(int i=1,a,b,c;i<=m;i++){
			scanf("%d %d %d",&a,&b,&c);
			vec[a].push_back((node){b,c});
			vec[b].push_back((node){a,c});
		}
		for(int i=1;i<=n;i++){
			res=0;
			dfs(i,0,0);
			if(res<ans)ans=res;
		}
	}
}bao;
struct P2{
	int val[20][20],res,ans,dep[20],cnt;
	bool mark[20];
	void chk(int &a,int b){
		if(a>b)a=b;
	}
	void dfs(int now){
		if(now==n+1){
			if(res<ans)ans=res;
			return;
		}
		for(int i=1;i<=n;i++){
			if(mark[i]){
				for(int j=1;j<=n;j++){
					if(!mark[j]&&val[i][j]<=oo){
						mark[j]=1;
						dep[j]=dep[i]+1;
						res+=val[i][j]*dep[j];
						dfs(now+1);
						res-=val[i][j]*dep[j];
						mark[j]=0;
					}
				}
			}
		}
	}
	void solve(){
		memset(dep,0,sizeof(dep));
		memset(val,63,sizeof(val));
		res=0;ans=oo;
		for(int i=1,a,b,c;i<=m;i++){
			scanf("%d %d %d",&a,&b,&c);
			chk(val[a][b],c);
			chk(val[b][a],c);
		}
		for(int i=1;i<=n;i++){
			dep[i]=0;
			mark[i]=1; 
			dfs(2);
			mark[i]=0;
		}
		printf("%d\n",ans);
	}
}da;
int main(){
	int i,j;
	freopen("1.in","r",stdin);
	freopen("1.out","w",stdout);
	cin>>n>>m;
	if(n==m+1){
		bao.solve();
	}
	else if(n<=8)da.solve();
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<climits>
#include<queue>
using namespace std;
#define N 500050
queue<int>q;
int n,m,rt,val;
int u,v,w;
int d[100][100];
int s[100],vis[100];
int maxx;
int du[100];
int head[100],tot;
int res,ans;
int depth[100];
int son[100];
struct Edge{
	int to,next,w;
}edge[N*2];

void add(int u,int v,int w){
	tot++;	
	edge[tot].next=head[u];head[u]=tot;
	edge[tot].w=w;edge[tot].to=v;
}

void dfs(int pre,int x){
	depth[x]=depth[pre]+1;
	for (int i=head[x];i;i=edge[i].next)
		if (!vis[edge[i].to]){
			res+=edge[i].w*depth[x];
			vis[edge[i].to]=1;
			dfs(x,edge[i].to);
		}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(d,0x3f3f,sizeof(d));
	maxx=1061109567;
	for (int i=1;i<=n;i++)d[i][i]=0;
	memset(du,0,sizeof(du));
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		if (u!=v){
			d[u][v]=min(d[u][v],w);
			d[v][u]=min(d[v][u],w);
		}
		du[u]++;
		du[v]++;
	}
	tot=0;
	for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++){
			if (d[i][j]!=maxx){
				add(i,j,d[i][j]);
				add(j,i,d[i][j]);
				val=d[i][j];
			}	
		}
	ans=INT_MAX;
	if (m==n-1){
		for (int i=1;i<=n;i++){
		rt=i;
		memset(vis,0,sizeof(vis));
		memset(depth,0,sizeof(depth));
		depth[rt]=1;
		res=0;
		for (int j=head[rt];j;j=edge[j].next){
			vis[edge[j].to]=1;
			res+=depth[rt]*edge[j].w;
			dfs(rt,edge[j].to);
		}
		ans=min(res,ans);
		}
	}
	else {
		for (int i=1;i<=n;i++){
			rt=i;
			memset(vis,0,sizeof(vis));
			memset(depth,0,sizeof(depth));
			depth[rt]=1;
			res=0;
			while (!q.empty())q.pop();
			q.push(rt);vis[rt]=1;int pre=1;tot=0;
			memset(son,0,sizeof(son));
			while (!q.empty()){
				int x=q.front();q.pop();pre--;
				for (int i=head[x];i;i=edge[i].next){
					if (!vis[edge[i].to]){
						son[x]++;
						vis[edge[i].to]=1;
						depth[edge[i].to]=depth[x]+1;
						q.push(edge[i].to);tot++;
					}
				}
				if (pre==0){
					pre=tot;
					tot=0;
				}
			}
		
			int mdeep=0;
			for (int i=1;i<=n;i++)
				mdeep=max(mdeep,depth[i]);
			for (int i=1;i<=n;i++)
				if (depth[i]!=mdeep)res+=depth[i]*val*son[i];
			ans=min(res,ans);
		}
	}
	printf("%d",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
#define N 10000
#define ll long long
int t;
struct NO{
	ll x,y,z;
}a[N];
bool b[N][N],flag;
bool vis[N];
ll n,r,h;

ll dis(NO u,NO v){
	ll dx,dy,dz;
	if (u.x<v.x)dx=v.x-u.x;
	else dx=u.x-v.x;
	if (u.y<v.y)dy=v.y-u.y;
	else dy=u.y-v.y;
	if (u.z<v.z)dz=v.z-u.z;
	else dz=u.z-v.z;
	return (dx*dx)+(dy*dy)+(dz*dz);	
}

void dfs(int x){
	if (flag) return;
	if (x==n+1){
		flag=true;
		return;
	}
	for (int i=1;i<=n+1;i++)
		if (!vis[i]&&b[x][i]){
			vis[i]=1;
			dfs(i);
		}	
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%lld%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++)
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		memset(b,0,sizeof(b));
		memset(vis,0,sizeof(vis));
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (dis(a[i],a[j])<=(4*r*r)){
					b[i][j]=1;
					b[j][i]=1;
				}
		for (int i=1;i<=n;i++){
			if (a[i].z<=r&&a[i].z>=-r){
				b[0][i]=1;b[i][0]=1;
			}
			if ((a[i].z>=h-r)&&(a[i].z<=h+r)){
				b[i][n+1]=1;b[n+1][i]=1;
			}
		}
		flag=false;
		vis[0]=1;
		dfs(0);
		if (flag)printf("%s\n","Yes");
		else printf("%s\n","No");
	}
	return 0;
}

#include <bits/stdc++.h>

using namespace std;

const int N = 15;

int g[N][N], dist[N], cost[N], n, e, ans;
void dfs(int sum) {
	if (sum >= ans) return;
	bool ok = 1;
	for (int i = 1; i <= n; i++)
		if (dist[i] == -1) ok = 0;
	if (ok == 1)
		return ans = sum, void();
	for (int i = 1; i <= n; i++)
		if (dist[i] != -1)
			for (int j = 1; j <= n; j++)
				if (g[i][j] != g[0][0]&&dist[j] == -1) {
					dist[j] = dist[i] + 1;
					dfs(sum + dist[i] * g[i][j]);
					dist[j] = -1;
				}
}
void prim(int o) {
	int res = 0; memset(cost, 63, sizeof cost), cost[o] = 0, dist[o] = 1;
	for (int i = 1; i <= n; i++) {
		int k = 0;
		for (int j = 1; j <= n; j++)
			if (cost[j] != -1&&cost[j] < cost[k]) k = j;
		res += cost[k]; cost[k] = -1;
		for (int j = 1; j <= n; j++)
			if (g[k][j] != g[0][0]&&dist[k] * g[k][j] < cost[j])
				cost[j] = dist[k] * g[k][j], dist[j] = dist[k] + 1;
	}
	ans = min(ans, res);
}
int main() {
	freopen("treasure.in", "r", stdin), freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &e), memset(g, 63, sizeof g);
	for (int a, b, c, i = 1; i <= e; i++)
		scanf("%d%d%d", &a, &b, &c), g[a][b] = g[b][a] = min(g[a][b], c);
	ans = 1 << 30;
	if (n <= 10) {
		for (int i = 1; i <= n; i++)
			memset(dist, 255, sizeof dist), dist[i] = 1, dfs(0);
	} else {
		for (int i = 1; i <= n; i++) prim(i);
	}
	return printf("%d\n", ans), 0;
}

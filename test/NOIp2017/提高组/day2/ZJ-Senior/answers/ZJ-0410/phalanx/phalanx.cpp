#include <bits/stdc++.h>

#define M ((L + R) >> 1)
#define pb push_back

using namespace std;

typedef long long ll;

const int N = 6E5 + 10;

int n, m, q;

namespace subtask1 {


vector <ll> erased[N], inserted[N], lst;

int solve() {
	for (ll i = 1; i <= n; i++)
		lst.pb((i - 1) * m + m), erased[i].pb(0), erased[i].pb(m);
	while (q--) {
		ll x, y; scanf("%lld%lld", &x, &y);
		if (y != m) {
			bool printed = 0; int cnt = 0;
			vector <ll> :: iterator it = erased[x].begin()+1;
			for (int j = 1; j <= m; j++) {
				if (it == erased[x].end()||*it != j) cnt++;
						 						 else it++;
				if (cnt == y) {
					printf("%lld\n", m * (x - 1) + j), printed = 1;
					erased[x].insert(it, j);
					inserted[x].push_back(lst[x - 1]);
					lst.erase(lst.begin() + x - 1);
					lst.push_back(m * (x - 1) + j);
					break;
				}
			}
			if (!printed) {
				printf("%lld\n", inserted[x][y - cnt - 1]);
				lst.push_back(inserted[x][y - cnt - 1]);
				inserted[x].erase(inserted[x].begin() + y - cnt - 1);
				inserted[x].push_back(lst[x - 1]);
				lst.erase(lst.begin() + x - 1);
			}
		} else {
			printf("%lld\n", lst[x - 1]);
			lst.push_back(lst[x - 1]);
			lst.erase(lst.begin() + x - 1);
		}
/*		for (int i = 1; i <= n; i++) {
			for (int j = 0; j < erased[i].size(); j++)
				printf("%d ", erased[i][j]);
			puts("");
		}

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++)
				if (*lower_bound(erased[i].begin(), erased[i].end(), j) != j) printf("%d ", (i - 1) * m + j);
			for (int j = 0; j < inserted[i].size(); j++)
				printf("%d ", inserted[i][j]);
			printf("%d ", lst[i - 1]);
			puts("");
		}
*/
	}
	return 0;
}
}

namespace subtask2 {

class segment_tree {
	private :
		int sum[N * 4];
		void insert(int o, int L, int R, int p, int v) {
			if (p < L||R < p) return;
			if (L == R)
				return sum[o] += v, void();
			insert(o << 1 | 1, M + 1, R, p, v);
			insert(o << 1, L, M, p, v);
			sum[o] = sum[o << 1] + sum[o << 1 | 1];
		}
		int query(int o, int L, int R, int v) {
			if (L == R) return M;
			if (sum[o << 1] >= v) return query(o << 1, L, M, v);
			return query(o << 1 | 1, M + 1, R, v - sum[o << 1]);
		}
	public :
		void insert(int p, int v) {
			insert(1, 1, 6E5, p, v);
		}
		int query(int v) {
			return query(1, 1, 6E5, v);
		}
} ST;
int n, m, q, top;
ll a[N];

int solve() {
	for (int i = 1; i <= n; i++) a[i] = i, ST.insert(i, 1);
	for (int i = n + 1; i <= n * 2 - 1; i++)
		a[i] = (i - n + 1) * m, ST.insert(i, 1);
	top = n + n - 1;
	while (q--) {
		int y; scanf("%*d%d", &y); int p = ST.query(y);
		printf("%lld\n", a[++top] = a[p]);
		ST.insert(top, 1);
		ST.insert(p, -1);
	}
	return 0;
}
}
int main() {
	freopen("phalanx.in", "r", stdin), freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if ((ll)n * q + (ll)m * q <= 50000000) subtask1 :: solve();
								 	  else subtask2 :: solve();
	return 0;
}

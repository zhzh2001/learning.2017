#include <bits/stdc++.h>

using namespace std;

const int N = 1005;

int X[N], Y[N], Z[N], T, n, h, r;
bool visited[N], ok;

double sqr(double x) {
	return x * x;
}
double getdist(int a, int b) {
	return sqrt(sqr(X[a] - X[b]) + sqr(Y[a] - Y[b]) + sqr(Z[a] - Z[b]));
}
void dfs(int u) {
	if (visited[u]) return; visited[u] = 1;
	for (int i = 1; i <= n; i++)
		if (getdist(u, i) <= 2 * r) dfs(i);
}
int main() {
	freopen("cheese.in", "r", stdin), freopen("cheese.out", "w", stdout);
	for (scanf("%d", &T); T--;) {
		scanf("%d%d%d", &n, &h, &r);
		for (int i = 1; i <= n; i++)
			scanf("%d%d%d", X + i, Y + i, Z + i);
		memset(visited, ok = 0, sizeof visited);
		for (int i = 1; i <= n; i++)
			if (Z[i] <= r) dfs(i);
		for (int i = 1; i <= n; i++)
			if (visited[i]&&h - Z[i] <= r) ok = 1;
		puts(ok ? "Yes" : "No");
	}
	return 0;
}

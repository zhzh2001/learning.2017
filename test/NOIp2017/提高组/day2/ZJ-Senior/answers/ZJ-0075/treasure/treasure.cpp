#include<bits/stdc++.h>
#define sight(c) ('0'<=c&&c<='9')
#define inf 1000000007
using namespace std;
inline void read(int &x){
	static char c;
	for(c=getchar();!sight(c);c=getchar());
	for(x=0;sight(c);c=getchar()) x=x*10+c-48;
}
int n,m,f[17][17],x,y,co,Ans,ans,usd[17],fa[17],val[17],g,mi,top,dep;
int main () {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n); read(m);
	memset(f,127,sizeof f);
	while (m--) {
		read(x); read(y); read(co);
		f[x][y]=min(f[x][y],co);
		f[y][x]=f[x][y];
	}
	Ans=inf;
	for (int i=1;i<=n;i++) {
		for(int j=1;j<=n;j++) usd[j]=0,fa[i]=0,val[j]=inf;  ans=0; 
		usd[i]=1; g=n-1;  
		for (int j=1;j<=n;j++) if (f[i][j]<inf) val[j]=f[i][j],fa[j]=1;
		while (g--) {
		mi=inf;
		for (int j=1;j<=n;j++)
		if (!usd[j]&&val[j]<mi) {mi=val[j];top=j;}
		ans+=mi; usd[top]=1; dep=fa[top]+1;
		for (int j=1;j<=n;j++)
		if (!usd[j]&&f[top][j]<inf&&(val[j]>dep*f[top][j]||
		(val[j]==dep*f[top][j]&&fa[j]<dep)))val[j]=dep*f[top][j],fa[j]=dep;
		}
	   if (ans<Ans) Ans=ans;
	}
	printf("%d\n",Ans);
	return 0;
}

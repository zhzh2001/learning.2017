#include<bits/stdc++.h>
#define sight(c) ('0'<=c&&c<='9')
#define sqr(x) ((x)*(x))
#define N 1017
#define M 1011007
#define eho(x) for(int i=head[x];i;i=net[i])
using namespace std;
int head[N],net[M],fall[M],tot,T,n,h,r,R,x[N],y[N],z[N],X;
queue<int> Q;
int in[N];
inline void read(int &x){
	static char c;static int b;
	for(c=getchar(),b=1;!sight(c);c=getchar())if (c=='-') b=-1;
	for(x=0;sight(c);c=getchar()) x=x*10+c-48;
	x*=b;
}
inline void add(int x,int y){
	fall[++tot]=y; net[tot]=head[x]; head[x]=tot; 
}
int main () {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--) {
		memset(head,0,sizeof head); tot=0; memset(in,0,sizeof in);
		read(n); read(h); read(r);R=4*r*r;
		for (int i=1;i<=n;i++) read(x[i]),read(y[i]),read(z[i]);
	    for (int i=1;i<=n;i++) {
	    	if (z[i]<=r) add(0,i);
	    	if (z[i]+r>=h) add(i,n+1);
	    	for (int j=i+1;j<=n;j++)
	    	 if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=R) add(i,j),add(j,i);
		}
	   in[0]=1; Q.push(0);
	   while (!Q.empty()) {
	   	X=Q.front(); 
		Q.pop();
	   	eho(X) if (!in[fall[i]]) {in[fall[i]]=1;Q.push(fall[i]);
		  }
	   }
	   if (in[n+1]) puts("Yes"); else puts("No");
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
#define rt register int
#define r read()
using namespace std;
int i,j,k,m,n,x,y,z,cnt,h;ll R;
ll read()
{
	ll x=0;short zf=1;char ch=getchar();
	while(ch!='-'&&(ch<'0'||ch>'9'))ch=getchar();
	if(ch=='-')zf=-1,ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x*zf;
}
struct ppp{
	ll x,y,z;
}a[3001];
int fa[3001];
inline bool cmp(ppp x,ppp y)
{
	return x.x<y.x;
}
int ask(int x)
{
	if(fa[x]!=x)fa[x]=ask(fa[x]);
	return fa[x];
}
inline void uni(int x,int y)
{
	int p=ask(x),q=ask(y);
	if(p!=q)fa[q]=p;
}
inline ll pf(ll x)
{
	return x*x;
}
inline ll diss(int x,int y)
{
	return pf(a[x].x-a[y].x)+pf(a[x].y-a[y].y)+pf(a[x].z-a[y].z);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
    for(rt t=r;t;t--)
    {
    	n=r;h=r;R=r;
    	for(rt i=1;i<=n;i++)
    	a[i].x=r,a[i].y=r,a[i].z=r;
    	sort(a+1,a+n+1,cmp);
    	for(rt i=1;i<=n+2;i++)fa[i]=i;
    	for(rt i=1;i<=n;i++)
    	{
    		if(a[i].z<=R)uni(i,n+1);
    		if(a[i].z+R>=h)uni(n+2,i);
		}
		if(ask(n+1)==ask(n+2))
		{
			printf("Yes\n");
			continue;
		}
		for(rt i=1;i<=n-1;i++)
		for(rt j=i+1;j<=n;j++)
		{
			if(a[j].x-a[i].x>R*2)break;
			if(diss(i,j)<=pf(R)*4)uni(i,j);
			//else cout<<i<<' '<<j<<' '<<diss(i,j)<<' '<<pf(R)<<endl;
		}
		if(ask(n+1)==ask(n+2))printf("Yes\n");
		else printf("No\n");
	}
}


#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#define ll long long
#define rt register int
#define r read()
using namespace std;
int i,j,k,m,n,x,y,z,cnt,maxans=999999999;
int dis[15][15];
int sl[15],q[15];bool vis[15];
bool b[15][15],pd[16],cu1,cu2;
inline ll read()
{
	ll x=0;short zf=1;char ch=getchar();
	while(ch!='-'&&(ch<'0'||ch>'9'))ch=getchar();
	if(ch=='-')zf=-1,ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-48,ch=getchar();
	return x*zf;
}

inline void solve()
{
	for(rt i=1;i<=n;i++)
	{
		memset(sl,0,sizeof(sl));
		rt h=0,t=1,ans=0;q[1]=i;
		while(h<t)
		{
		    x=q[++h];
			for(rt j=1;j<=n;j++)if(b[x][j]&&!sl[j]&&j!=i)
			{
				sl[j]=sl[x]+1;q[++t]=j;
				ans+=sl[j]*dis[x][j];
			}
		}
		if(t!=n)return;
		if(ans<maxans)maxans=ans;
	}
}
void dfs(rt x,rt y,rt z,rt num)
{
	//cout<<num<<' '<<z<<endl;
	if(z==n-1)
	{
		//if(num!=n)return;
		solve();
		return ;
	}
	if(x>n)return;
	if(y>n)
	{
	    dfs(x+1,x+2,z,num);
	    return;
	}
	if(dis[x][y])
	{
		rt g=!pd[x]+!pd[y];bool cu1=pd[x],cu2=pd[y];
		num+=g;pd[x]=pd[y]=1;rt p=3;
		if(n>8)p=2;if(n<8)p=5;
		if(num!=z+1&&num-z<=p)
	    b[x][y]=b[y][x]=1,dfs(x,y+1,z+1,num);
	    num-=g;pd[x]=cu1;pd[y]=cu2;
	    
	}
	b[x][y]=b[y][x]=0,dfs(x,y+1,z,num);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
    n=r;m=r;
    for(rt i=1;i<=m;i++)
    {
    	x=r;y=r;z=r;
    	if(!dis[x][y]||dis[x][y]>z)
		dis[x][y]=z,dis[y][x]=z;
	}
	if(n>9)
	{
		for(rt i=1;i<=n;i++)
		{
			int ans=0;
			for(rt j=1;j<=n;j++)if(j!=i)ans+=dis[i][j];
			if(ans<maxans)maxans=ans;
		}
		cout<<maxans;
		return 0;
	}
	dfs(1,2,0,0);
	cout<<maxans;
}


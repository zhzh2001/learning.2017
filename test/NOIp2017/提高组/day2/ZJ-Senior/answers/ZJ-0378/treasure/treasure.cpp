#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define cl(x) memset(x,0,sizeof(x))
using namespace std;

#define read(x) scanf("%d",&(x))

const int N=15;

int n,m;
int d[N][N];

struct edge{
  int u,v,w,next;
}G[N<<1];
int head[N],inum;
inline void add(int u,int v,int w,int p){
  G[p].u=u; G[p].v=v; G[p].w=w; G[p].next=head[u]; head[u]=p;
}
inline void link(int u,int v,int w){
  add(u,v,w,++inum); add(v,u,w,++inum);
}
#define V G[p].v

int cur;

inline void dfs(int u,int fa,int d){
  for (int p=head[u];p;p=G[p].next)
    if (V!=fa)
      cur+=G[p].w*d,dfs(V,u,d+1);
}

int Ans=1<<30;

inline int solve(){
  for (int i=1;i<=n;i++)
    cur=0,dfs(i,0,1),Ans=min(Ans,cur);
}

int a[N];
int vst[N],clk;
int ins[N];

inline void search(int t){
  if (t==n-1){
    cl(head); inum=0;
    int flag=0; cl(ins);
    for (int i=1;i<=n-2;i++){
      ++clk;
      for (int j=i;j<=n-2;j++)
	vst[a[j]]=clk;
      for (int j=1;j<=n;j++)
	if (!ins[j] && vst[j]!=clk){
	  if (d[j][a[i]]==1<<30)
	    flag=1;
	  else
	    ins[j]=1,link(j,a[i],d[j][a[i]]);
	  break;
	}
    }
    int x=0;
    for (int i=1;i<=n;i++)
      if (!ins[i])
	if (!x)
	  x=i;
	else{
	  if (d[x][i]==1<<30)
	    flag=1;
	  else
	    link(x,i,d[x][i]);
	}
    if (!flag)
      solve();
    return;
  }
  for (int i=1;i<=n;i++)
    a[t]=i,search(t+1);
}

int main(){
  int u,v,w;
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  read(n); read(m);
  for (int i=1;i<=n;i++) for (int j=1;j<=n;j++) d[i][j]=1<<30;
  for (int i=1;i<=m;i++) read(u),read(v),read(w),d[u][v]=d[v][u]=min(d[u][v],w);
  if (n==1){
    printf("0\n");
  }else{
    search(1);
    printf("%d\n",Ans);
  }
  return 0;
}

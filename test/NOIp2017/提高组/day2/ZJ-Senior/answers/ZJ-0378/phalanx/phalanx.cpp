#include<cstdio>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long ll;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline void read(int &x){
  char c=nc(),b=1;
  for (;!(c>='0' && c<='9');c=nc()) if (c=='-') b=-1;
  for (x=0;c>='0' && c<='9';x=x*10+c-'0',c=nc()); x*=b;
}

const int M=2000005;
const int N=300005;

struct node{
  node *l,*r;
  ll L,R,sum; int fix;
  node(){ }
  ll len(){
    return R-L+1;
  }
  void push(){
    sum=R-L+1;
    if (l) sum+=l->sum;
    if (r) sum+=r->sum;
  }
}pool[M];
int ncnt;
inline node *newnode(ll _l,ll _r){
  if (_l>_r) return NULL; 
  node *x=pool+(++ncnt);
  x->L=_l; x->R=_r; x->sum=_r-_l+1; x->fix=rand()%((int)1e9);
  return x;
}

inline ll Sum(node *x){
  return !x?0:x->sum;
}

inline node *Merge(node *A,node *B){
  if (!A||!B) return A?A:B;
  if (A->fix<B->fix){
    A->r=Merge(A->r,B);
    A->push();
    return A;
  }else{
    B->l=Merge(A,B->l);
    B->push();
    return B;
  }
}
typedef pair<node*,node*> Droot;
inline Droot Split(node *A,int k){
  if (!A) return Droot(NULL,NULL);
  if (k<=Sum(A->l)+A->len()){
    Droot x=Split(A->l,k);
    A->l=x.second; A->push();
    x.second=A;
    return x;
  }else{
    Droot x=Split(A->r,k-Sum(A->l)-A->len());
    A->r=x.first; A->push();
    x.first=A;
    return x;
  }
}

inline Droot SplitT(node *A,int k){
  if (!A) return Droot(NULL,NULL);
  if (k<=((bool)A->l)){
    Droot x=SplitT(A->l,k);
    A->l=x.second; A->push();
    x.second=A;
    return x;
  }else{
    Droot x=SplitT(A->r,k-1);
    A->r=x.first; A->push();
    x.first=A;
    return x;
  }
}


int n,m;
node *rt[N],*rtc;

#define P(x,y) (((ll)(x)-1)*m+(y))

inline void Build(){
  for (int i=1;i<=n;i++){
    rt[i]=newnode(P(i,1),P(i,m-1));
    rtc=Merge(rtc,newnode(P(i,m),P(i,m)));
  }
}

inline void Work(int X,int Y){
  if (Y==m){
    Droot x=Split(rtc,X);
    Droot y=SplitT(x.second,1);
    printf("%lld\n",y.first->L);
    rtc=Merge(Merge(x.first,y.second),y.first);
  }else{
    Droot x=Split(rt[X],Y);
    Droot y=SplitT(x.second,1);
    ll L=y.first->L,R=y.first->R,dd=y.first->L+Y-Sum(x.first)-1;
    printf("%lld\n",dd);
    Droot t1=Split(rtc,X);
    Droot t2=SplitT(t1.second,1);
    rtc=Merge(Merge(t1.first,t2.second),newnode(dd,dd));

    node *n1=newnode(L,dd-1),*n2=newnode(dd+1,R),*nn;
    if (n1==NULL)
      nn=n2;
    else
      n1->r=n2,n1->push(),nn=n1;
    
    rt[X]=Merge(Merge(Merge(x.first,nn),y.second),t2.first);
  }
}

int main(){
  int Q,x,y;
  freopen("phalanx.in","r",stdin);
  freopen("phalanx.out","w",stdout);
  read(n); read(m); read(Q);
  Build();
  while (Q--){
    read(x); read(y);
    Work(x,y);
  }
  return 0;
}

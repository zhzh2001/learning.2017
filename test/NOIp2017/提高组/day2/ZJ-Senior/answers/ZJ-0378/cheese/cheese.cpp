#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define cl(x) memset(x,0,sizeof(x))
using namespace std;
typedef long long ll;

#define read(x) scanf("%d",&(x))

const int N=1105;

int n,h,R,S,T;
int x[N],y[N],z[N];

inline ll sqr(int x){
  return (ll)x*x;
}

inline bool pj(int i,int j){
  return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=4LL*sqr(R);
}

bool d[N][N];

int Q[N],l,r;
int ins[N];

inline bool bfs(){
  l=r=-1; cl(ins);
  Q[++r]=S; ins[S]=1;
  while (l<r){
    int u=Q[++l];
    for (int i=1;i<=T;i++)
      if (d[u][i] && !ins[i]){
	Q[++r]=i; ins[i]=1;
	if (i==T) return 1;
      }
  }
  return 0;
}

inline void Solve(){
  read(n); read(h); read(R);
  for (int i=1;i<=n;i++) read(x[i]),read(y[i]),read(z[i]);
  S=n+1; T=n+2;
  for (int i=1;i<=n;i++)
    for (int j=i+1;j<=n;j++)
      d[i][j]=d[j][i]=pj(i,j);
  for (int i=1;i<=n;i++)
    d[S][i]=(z[i]<=R),d[i][T]=(h-z[i]<=R);
  if (bfs())
    printf("Yes\n");
  else
    printf("No\n");
}

int main(){
  int T;
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  read(T);
  while (T--)
    Solve();
  return 0;
}

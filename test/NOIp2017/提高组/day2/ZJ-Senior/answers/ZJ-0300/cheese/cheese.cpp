#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;
int T,n,p[10000];
int tot,head[1000100],Next[2000100],to[2000100];
ll h,r,x[10000],y[10000],z[10000];
ll yl(ll x1,ll x2)
{
	return (x1-x2)*(x1-x2);
}
void add(int x1,int x2)
{
	tot++;
	Next[tot]=head[x1];
	head[x1]=tot;
	to[tot]=x2;
}
void dfs(int x1)
{
	p[x1]=1;
	for (int i=head[x1];i!=-1;i=Next[i])
		if (p[to[i]]==0)
			dfs(to[i]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++)
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=0;i<=n+1;i++) head[i]=-1;
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
				if (yl(x[i],x[j])+yl(y[i],y[j])+yl(z[i],z[j])<=4LL*r*r)
				  add(i,j),add(j,i);	
		for (int i=1;i<=n;i++)
		{
			if (z[i]<=r) add(0,i),add(i,0);	
			if (h-z[i]<=r) add(n+1,i),add(i,n+1);
		}
		for (int i=0;i<=n+1;i++) p[i]=0;
		dfs(0);
		if (p[n+1]==1) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

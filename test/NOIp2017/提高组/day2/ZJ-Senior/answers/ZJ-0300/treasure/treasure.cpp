#include<cstdio>
#include<iostream>
using namespace std;
int n,m,x,y,z,d[15][15];
int f[10000],ans;
int judge(int a,int b)
{
	for (int j=1;j<=n;j++)
		if ((a&(1<<(j-1)))!=0&&(b&(1<<(j-1)))==0) return 0;
	return 1;	
}
void dfs(int x1,int x2,int x3)
{
	for (int i=(1<<n)-1;i>=x2+1;i--)
		if (judge(x2,i)==1)
		{
			int s=f[x2];y=0;
			for (int j=1;j<=n;j++)
				if ((i&(1<<(j-1)))!=0&&(x2&(1<<(j-1)))==0)
				{
					x=99999999;
					for (int k=1;k<=n;k++)
						if ((x1&(1<<(k-1)))!=0&&d[k][j]!=-1)
							x=min(x,d[k][j]*x3);
					if (x==99999999) {y=1;break;}
					s+=x;		
				}
			if (y==1) continue;	
			if (s<f[i]&&s<ans)
			{
				f[i]=s;
				dfs(i-x2,i,x3+1);
			}	
		}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) d[i][j]=-1;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (d[x][y]==-1) d[x][y]=z,d[y][x]=d[x][y];
			else d[x][y]=min(d[x][y],z),d[y][x]=d[x][y];
	}
	ans=999999999;
	for (int ii=1;ii<=n;ii++)
	{
		for (int i=1;i<=(1<<n)-1;i++) f[i]=999999999;
		f[(1<<(ii-1))]=0;
		dfs((1<<(ii-1)),(1<<(ii-1)),1);
		ans=min(ans,f[(1<<n)-1]);
	}
	printf("%d\n",ans);
	return 0;
}

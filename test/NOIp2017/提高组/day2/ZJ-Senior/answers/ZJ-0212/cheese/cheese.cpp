#include<bits/stdc++.h>
using namespace std;
const int maxn=1000+5;
struct node
{
	long long x,y,z;
	long long len,q[maxn];
}a[maxn];
long long t,n,h,r;
int f[maxn+1];
/*void dfs(int x)
{
	if(x==n+1)
	{
		flag=1;
		return;
	}
	int lentg=a[x].len;
	for(int i=1;i<=lentg;i++)
	{
		if(flag)return;
		if(!f[a[x].q[i]])
		{
			f[a[x].q[i]]=1;
			dfs(a[x].q[i]);
			f[a[x].q[i]]=0;
		}
	}
}*/
long long fin(int x)
{
	while(f[x]!=x)
			x=f[x];
		return f[x];
}
int  main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++)
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);	
		for(int i=0;i<=n+1;i++)
			f[i]=i;
			for(int i=1;i<=n;i++)
				for(int j=i+1;j<=n;j++)
				{
					if(j!=i)
					{
						float dis=sqrt((a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z));
						if(dis<=(r<<1))
							{
								f[i]=fin(i);
								f[j]=fin(j);
								f[f[i]]=f[j];
							}
					}
				}
			for(int i=1;i<=n;i++)
			{
				if(a[i].z<=r)
				{
					f[i]=fin(i);
					f[0]=fin(0);
					f[f[i]]=f[0];
				}
				if(a[i].z+r>=h)
				{
					f[i]=fin(i);
					f[n+1]=fin(n+1);
					f[f[i]]=f[n+1];
				}	
			}
			if(fin(0)==fin(n+1))printf("Yes\n");
			else printf("No\n");
	}
	return 0;
}

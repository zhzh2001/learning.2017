program treasure;
var n,m,ans,a,b,c,i,j,head,tail,cur,cost,k,tmp,start,end1:longint;
    l,now:array[1..15,1..15]of longint;
    p:array[1..15]of longint;
    input,output:text;
begin
 assign(input,'treasure.in');
 reset(input);
 ans:=-1;
 readln(input,n,m);
 for i:=1 to n do
 begin
  for j:=1 to n do
   l[i,j]:=-1;
  l[i,i]:=0;
 end;
 for i:=1 to m do
 begin
  readln(input,a,b,c);
  if (l[a,b]=-1) or (c<l[a,b]) then
  begin
   l[a,b]:=c;
   l[b,a]:=c;
  end;
 end;
 close(input);
 for i:=1 to n do
 begin
  for j:=1 to n do
   p[j]:=j;
  tmp:=1;
  for j:=1 to n do
   for k:=1 to n do
    if j<>k then now[j,k]:=-1
            else now[j,k]:=0;
  for j:=1 to n do
   if (l[i,j]>0) then now[i,j]:=1;
  cost:=-1;
  cur:=0;
  while tmp<n do
  begin
   for j:=1 to n do
    for k:=1 to n do
     if (p[j]=i) and (p[k]<>i) and (now[j,k]>0) then
      if (cost=-1) or (cost>(now[j,k]*l[j,k])) then
      begin
       cost:=now[j,k]*l[j,k];
       start:=j;
       end1:=k;
      end;
    cur:=cur+cost;
    cost:=-1;
    p[end1]:=i;
    for j:=1 to n do
     if (p[j]<>i) and (l[end1,j]<>-1) then
      now[end1,j]:=now[start,end1]+1;
   inc(tmp);
  end;
  if (ans=-1) or (cur<ans) then ans:=cur;
 end;
 assign(output,'treasure.out');
 rewrite(output);
 writeln(output,ans);
 close(output);
end.

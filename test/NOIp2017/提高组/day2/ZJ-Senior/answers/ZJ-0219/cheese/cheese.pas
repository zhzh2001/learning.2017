program cheese;
const ep=1e-8;
var tot,t,n,h,r,num,i,j,k,tmp:longint;
    x,y,z,lower,higher,a:array[1..2000]of longint;
    bl,bl2:boolean;
    input,output:text;
function max(xx,yy:longint):longint;
begin
 if xx>yy then exit(xx)
          else exit(yy);
end;
function min(xx,yy:longint):longint;
begin
 if xx<yy then exit(xx)
          else exit(yy);
end;
function find(xx:longint):longint;
begin
 if a[xx]=xx then exit(xx)
 else
 begin
  a[xx]:=find(a[xx]);
  exit(a[xx]);
 end;
end;
begin
 assign(input,'cheese.in');
 assign(output,'cheese.out');
 reset(input);
 rewrite(output);
 readln(input,tot);
 t:=0;
 repeat
  inc(t);
  readln(input,n,h,r);
  num:=0;
  bl2:=false;
  for i:=1 to n do
  begin
   a[i]:=i;
   read(input,x[i],y[i],z[i]);
   if not bl2 then
   begin
    bl:=true;
    for j:=1 to i-1 do
     if sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))-ep<=2*r then
     begin
      bl:=false;
      if a[i]=i then
      begin
       a[i]:=find(j);
       lower[a[i]]:=min(lower[a[i]],z[i]-r);
       higher[a[i]]:=max(higher[a[i]],z[i]+r);
      end
      else if find(a[i])<>find(a[j]) then
      begin
       tmp:=find(a[i]);
       k:=find(a[j]);
       lower[k]:=min(lower[tmp],lower[k]);
       higher[k]:=max(higher[tmp],higher[k]);
       a[tmp]:=k;
      end;
     end;
    if bl then
    begin
     lower[i]:=z[i]-r;
     higher[i]:=z[i]+r;
    end;
    if (lower[find(a[i])]<=0) and (higher[find(a[i])]>=h) then bl2:=true;
   end;
  end;
  if bl2 then writeln(output,'Yes')
         else writeln(output,'No');
 until t=tot;
 close(input);
 close(output);
end.

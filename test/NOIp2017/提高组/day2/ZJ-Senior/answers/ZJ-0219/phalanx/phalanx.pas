program phalanx;
var
 a:array[1..1000,1..1000]of longint;
 i,j,x,n,m,q,y,t:longint;
 input,output:text;
begin
 assign(input,'phalanx.in');
 assign(output,'phalanx.out');
 reset(input);
 rewrite(output);
 readln(input,n,m,q);
 for i:=1 to n do
  for j:=1 to m do
   a[i,j]:=(i-1)*n+j;
 for i:=1 to q do
 begin
  readln(input,x,y);
  t:=a[x,y];
  writeln(output,t);
  for j:=y to m-1 do
   a[x,j]:=a[x,j+1];
  for j:=x to n-1 do
   a[j,m]:=a[j+1,m];
  a[n,m]:=t;
 end;
 close(input);
 close(output);
end.

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define inf 100000000
using namespace std;
ll sd[100];
ll fa[100];
bool fb[100];
ll ans,mi,x,y,n,m;
ll a[100][100];
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	return x*f;
}
int getfa(int x)
{
	if(fb[x])return sd[x]+1;
	fb[x]=true;
	sd[x]=getfa(fa[x]);
	return sd[x]+1;
}
inline void lzq(int g)
{
	For(i,1,n)fb[i]=false,sd[i]=inf;
	sd[g]=0;
	fb[g]=true;
	For(i,1,n)
	{
		if(fb[i])continue;
		sd[i]=getfa(i)-1;
		if(sd[i]==inf)return;
	}
	ans=0;
	For(i,1,n)ans+=(sd[i]*a[fa[i]][i]);
	if(ans<mi)mi=ans;
	
}
inline void dfs(int x,int g)
{
	if(x==n+1)
	{
		lzq(g);
		return;
	}
	if(x==g)
	{
		dfs(x+1,g);
		return;
	}
	For(i,1,n)
	{
		if(i==x)continue;
		if(a[i][x]==inf)continue;
		fa[x]=i;
		dfs(x+1,g);
	}
	return;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure2.out","w",stdout);
	n=read();m=read();
	For(i,1,n)For(j,1,m)a[i][j]=inf;
	For(i,1,m)
	{
		x=read();y=read();
		a[x][y]=a[y][x]=min(read(),a[x][y]);
	}
	mi=1000000000000;
	For(i,1,n)
	{
		fa[i]=0;
		dfs(1,i);
	}
	cout<<mi<<endl;
	return 0;
}

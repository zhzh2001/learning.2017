#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<queue>
#include<vector>
#define maxn 20
#define maxm 30000
#define maxx 10000000
using namespace std;
int n,m,cnt;
queue<int>q;
bool vis[maxn];
int dep[maxn],g[maxn][maxn],dis[maxn];
void bfs(int root)
{
  for (int i=1;i<=n;i++) dis[i]=maxx,vis[i]=false,dep[i]=n+n;
  dep[root]=1;dis[root]=0; vis[root]=true;q.push(root);
  while (!q.empty())
  {
  	int now=q.front();q.pop();
  	for (int y=1;y<=n;y++)
  	if (y!=now)
  	{
  	  if ((dep[now]*g[now][y])<dis[y])
  	  {
  	    dis[y]=dep[now]*g[now][y];
  	    dep[y]=dep[now]+1;
  	    //cout<<now<<" "<<y<<" "<<dis[y]<<" "<<dep[now]<<" "<<g[now][y]<<endl;
  	    if (!vis[y])
  	    {
  	      vis[y]=true;
		  q.push(y);	
		}
	  }
	  if ((dep[now]*g[now][y])==dis[y]) dep[y]=min(dep[y],dep[now]+1);
	}
	vis[now]=false;
  }
}
int main()
{
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  scanf("%d%d",&n,&m);
  int x,y,z;
  for (int i=1;i<=n;i++)
  for (int j=1;j<=n;j++)
  if (i!=j) g[i][j]=maxx;
  else g[i][j]=0;
  for (int i=1;i<=m;i++)
  {
  	scanf("%d%d%d",&x,&y,&z);
  	g[x][y]=min(g[x][y],z);
  	g[y][x]=min(g[y][x],z);
  }
  int ans=maxx;
  for (int i=1;i<=n;i++)
  {
  	bfs(i);
  	int sum=0;
  	for (int j=1;j<=n;j++) sum+=dis[j];
  	//for (int j=1;j<=n;j++) cout<<dis[j]<<endl;
  	ans=min(ans,sum);
  }
  printf("%d\n",ans);
  return 0;
}

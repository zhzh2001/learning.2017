#include<iostream>
#include<cstring>
#include<algorithm>
#include<cstdio>
#include<algorithm>
#include<queue>
#include<string>
#include<cmath>
#include<cstdlib>
#define maxn 2009
using namespace std;
struct ding{
  long long x,y,z;
}a[maxn];
string ans;
queue<int>q;
int t,n;
long long h,r;
bool vis[maxn],dis[maxn];
bool cmp(ding u,ding v){return u.z<v.z;}
bool check(int u,int v)
{
  double sum=(a[u].x-a[v].x)*(a[u].x-a[v].x);
  sum+=(a[u].y-a[v].y)*(a[u].y-a[v].y);
  sum+=(a[u].z-a[v].z)*(a[u].z-a[v].z);
  sum=sqrt(sum);
   //cout<<u<<" "<<v<<" "<<sum<<endl;
  if (sum<=r+r) return true;
  return false;
}
int main()
{
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  scanf("%d",&t);
  while (t--)
  {
  	scanf("%d%lld%lld",&n,&h,&r);
  	for (int i=1;i<=n;i++)
  	scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
  	sort(a+1,a+1+n,cmp);
  	for (int i=1;i<=n;i++) vis[i]=dis[i]=false;
    for (int i=1;i<=n;i++) if (a[i].z<=r) vis[i]=dis[i]=true,q.push(i);
    else break;
    while (!q.empty())
    {
      int now=q.front(); q.pop();
      for (int i=now+1;i<=n;i++) if (check(now,i)) 
	  {
	   dis[i]=true;
	   if (!vis[i])	q.push(i),vis[i]=true; 
      }
	}
	ans="No";
	for (int i=1;i<=n;i++) if ((dis[i])&&(h-a[i].z<=r)) ans="Yes";
	cout<<ans<<endl;
  }
  return 0;
}

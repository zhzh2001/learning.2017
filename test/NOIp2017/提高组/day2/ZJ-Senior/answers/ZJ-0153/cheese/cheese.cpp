#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <math.h>
#include <memory.h>
#include <stack>
using namespace std;
int n,h,r;
int tezhen[1001];
vector<int> edge[1001];
int disable[1001];
bool dist(int x1,int y1,int z1,int x2,int y2,int z2)
{
	return sqrt(pow(x1-x2,2)+pow(y1-y2,2)+pow(z1-z2,2))<=2*r;
}
bool dfs(int i,int pre)
{
	if(tezhen[i]==-1) return 1;
	bool flag=false;
	vector<int>::iterator it=edge[i].begin();
	for(;it!=edge[i].end();it++)
	{
		if(pre==*it) continue;
		if(disable[*it]) continue;
		if(dfs(*it,i)) flag=true;
	}
	if(!flag)disable[i]=1;
	return flag;
}
long int pos[1001][3];
void prog()
{
	scanf("%d%d%d",&n,&h,&r);
	bool flag=false;
	vector<int> top,down;
	memset(tezhen,0,sizeof(tezhen));
	memset(disable,0,sizeof(disable));
	for(int i=0;i<n;i++)
	{
		edge[i].erase(edge[i].begin(),edge[i].end());
		scanf("%ld%ld%ld",&pos[i][0],&pos[i][1],&pos[i][2]);
		if(pos[i][2]<=r) 
			tezhen[i]=1,down.push_back(i);
		if(h-pos[i][2]<=r) 
			tezhen[i]=-1,top.push_back(i);
		if(pos[i][2]<=r&&h-pos[i][2]<=r) flag=true;
		for(int j=0;j<i;j++)
		{
			if(abs(pos[j][0]-pos[i][0])>2*r||abs(pos[j][1]-pos[i][1])>2*r||abs(pos[j][2]-pos[i][2])>2*r) continue;
			if(dist(pos[i][0],pos[i][1],pos[i][2],pos[j][0],pos[j][1],pos[j][2]))
			{
				edge[i].push_back(j);
				edge[j].push_back(i);
			}
		}
	}
	if(flag) {
		printf("Yes\n");
		return;
	}
	vector<int>::iterator it=down.begin();
	for(;it!=down.end();it++)
	{
		if(dfs(*it,*it))
		{
			printf("Yes\n");
			return;
		}
	}
	printf("No\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t)
	{
		if(t==2)
		{
			int a=2;
			a++;
			a=2;
		}
		prog();
		t--;
	}
}

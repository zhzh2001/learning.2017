#include <stdio.h>
#include <queue>
#include <memory.h>
using namespace std;
int cost[12][12];
queue<int>q;
int lenth[12];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=0;i<m;i++)
	{
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		a--,b--;
		if(cost[a][b]==0||cost[a][b]>c)
		cost[a][b]=cost[b][a]=c;
	}
	long int ans=0x7fffffff;
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++) lenth[j]=0;
		lenth[i]=1;
		long int toacost=0;
		for(int t=1;t<n;t++)
		{
			int minx,miny,mincost=0x7fFFFFFF;
			for(int j=0;j<n;j++)
			{
				if(lenth[j]==0) continue;
				for(int k=0;k<n;k++)
				{
					if(cost[j][k]==0) continue;
					if(lenth[k]) continue;
					if(lenth[j]*cost[j][k]<mincost)
					{
						minx=j,miny=k,mincost=lenth[j]*cost[j][k];
					}
				}
			}
			lenth[miny]=lenth[minx]+1;
			toacost+=mincost;
		}
		if(toacost<ans) ans=toacost;
	}
	printf("%ld",ans);
}

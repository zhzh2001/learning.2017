#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(Ll &x,Ll &y){x=RR();y=RR();}

const Ll N=12+5;
Ll g[N][N],d[N],t[N],ans;
bool vi[N];
Ll n,m,x,y,z;
void check(Ll k){
	for(Ll i=0;i<=n;i++)t[i]=1e5,d[i]=1e9;
	t[k]=0;d[k]=0;
	memset(vi,0,sizeof vi);
	Ll sum=0;
	for(Ll k=1;k<=n;k++){
		Ll o=0;
		for(Ll i=1;i<=n;i++)if(!vi[i]&&d[o]>d[i])o=i;
		sum+=d[o];vi[o]=1;
		for(Ll i=1;i<=n;i++)if(!vi[i])
			if(d[i]>(t[o]+1)*g[i][o])
				d[i]=(t[o]+1)*g[i][o],t[i]=t[o]+1;
	}ans=min(ans,sum);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n,m);
	memset(g,63,sizeof g);
	for(Ll i=1;i<=m;i++){
		read(x,y);
		g[x][y]=min(g[x][y],RR());
		g[y][x]=g[x][y];
	}
	ans=1e9;
	for(Ll i=1;i<=n;i++)check(i);
	W(ans);
}


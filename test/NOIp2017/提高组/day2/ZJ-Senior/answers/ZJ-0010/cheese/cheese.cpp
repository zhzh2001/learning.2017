#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline Ll RR(){Ll v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(Ll x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(Ll x){W(x);puts("");}
inline void read(Ll &x,Ll &y){x=RR();y=RR();}

const Ll N=1e3+5;
Ll x[N],y[N],z[N];
bool g[N][N],A[N],B[N],vi[N],ans;
Ll n,h,t,R;
void dfs(Ll x){
	vi[x]=1;
	if(B[x])ans=1;
	if(ans)return;
	for(Ll i=1;i<=n;i++)if(g[i][x]&&!vi[i])dfs(i);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=RR();
	while(t--){
		memset(g,0,sizeof g);
		memset(A,0,sizeof A);
		memset(B,0,sizeof B);
		memset(vi,0,sizeof vi);
		read(n,h);R=RR();
		for(Ll i=1;i<=n;i++)read(x[i],y[i]),z[i]=RR();
		for(Ll i=1;i<=n;i++){
			if(z[i]<=R)A[i]=1;
			if(h-z[i]<=R)B[i]=1;
		}
		R=R*2;R=R*R;
		for(Ll i=1;i<=n-1;i++)
			for(Ll j=i+1;j<=n;j++)
				if((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])<=R)g[i][j]=g[j][i]=1;	
		ans=0;
		for(Ll i=1;i<=n;i++)if(A[i])dfs(i);
		if(ans)cout<<"Yes"<<endl;else cout<<"No"<<endl;
	}
}

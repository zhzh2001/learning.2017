#include<bits/stdc++.h>
#define Ll long long
using namespace std;
inline int RR(){int v=0,k=1;char c=0;while('0'>c||c>'9'){if(c=='-')k=-1;c=getchar();}while('0'<=c&&c<='9')v=v*10+c-48,c=getchar();return v*k;}
inline void W(int x){if(x<0)x=-x,putchar('-');if(x>9)W(x/10);putchar(x%10+48);}
inline void WW(int x){W(x);puts("");}
inline void read(int &x,int &y){x=RR();y=RR();}

const int N=5e4+5;
int a[N],b[N],X[N],Y[N],ans[N];
int n,m,q,x,y;
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n,m);q=RR();
	for(int p=1;p<=q;p++){
		read(X[p],Y[p]);
		x=X[p];y=Y[p];
		for(int i=y;i<=m;i++)a[i]=(x-1)*m+i;
		for(int i=x;i<=n;i++)b[i]=i*m;
		for(int j=1;j<p;j++){
			int xx=X[j],yy=Y[j];
			for(int i=y;i<m;i++)
				if(xx==x&&yy<=i)a[i]=a[i+1];
			for(int i=x;i<n;i++)
				if(xx<=i)b[i]=b[i+1];
			b[n]=ans[j];
			a[m]=b[x];
		}
		ans[p]=a[y];
		WW(ans[p]);
	}
}


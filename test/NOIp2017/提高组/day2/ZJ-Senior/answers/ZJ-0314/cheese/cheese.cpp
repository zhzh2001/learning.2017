	#include<iostream>
	#include<cstdio>
	#include<algorithm>
	#include<cstring>
	#include<cmath>
	using namespace std;
	double h,r,a[1005],b[1005],c[1005];
	int n,head,tail,q[1005];
	bool ans,flag[1005];
	double dist(int x,int y)
	{
		return sqrt((a[x]-a[y])*(a[x]-a[y])+(b[x]-b[y])*(b[x]-b[y])+(c[x]-c[y])*(c[x]-c[y]));
	}
	int main()
	{
		freopen("cheese.in","r",stdin);
		freopen("cheese.out","w",stdout);
		int cas=0;
		scanf("%d",&cas);
		while (cas--)
		{
			scanf("%d%lf%lf",&n,&h,&r);
			for (int i=1;i<=n;i++)
				scanf("%lf%lf%lf",&a[i],&b[i],&c[i]);
			for (int i=1;i<=n;i++)
				flag[i]=false;
			head=1;tail=0;
			for (int i=1;i<=n;i++)
				if (abs(c[i])<=r)
				{
					flag[i]=true;
					tail++;
					q[tail]=i;
				}
			while (head<=tail)
			{
				int u=q[head];
				for (int v=1;v<=n;v++)
					if (flag[v]==false && dist(u,v)<=r+r)
					{
						flag[v]=true;
						tail++;
						q[tail]=v;
					}
				head++;
			}
			ans=false;
			for (int i=1;i<=n;i++)
				if (flag[i]==true && abs(h-c[i])<=r)
				{
					ans=true;
					break;
				}
			if (ans==true) printf("Yes\n");
			else printf("No\n");
		}
		return 0;
	}

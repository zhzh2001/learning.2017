	#include<iostream>
	#include<cstdio>
	#include<algorithm>
	#include<cstring>
	#include<cmath>
	using namespace std;
	int n,m,q,a[1005][1005];
	int tree1[2400005],tree2[2400005];
	
	void build(int v,int l,int r)
	{
		if (l==r)
		{
			if (l<=m)
				tree1[v]=tree2[v]=1;
			return;
		}
		int mid=(l+r)>>1;
		build(v+v,l,mid);
		build(v+v+1,mid+1,r);
		tree1[v]=tree1[v+v]+tree1[v+v+1];
		tree2[v]=tree2[v+v]+tree2[v+v+1];
		return;
	}
	int find(int v,int l,int r,int w)
	{
		if (tree2[v]==1 && w==1 && l==r) return r;
		int mid=(l+r)>>1;
		if (tree2[v+v]<w) return find(v+v+1,mid+1,r,w-tree2[v+v]);
		else return find(v+v,l,mid,w);
	}
	int sum(int v,int l,int r,int x,int y)
	{
		if (x>y) return 0;
		if (x==l && r==y) return tree1[v];
		if (l==r) return tree1[v];
		int mid=(l+r)>>1;
		if (y<=mid) return sum(v+v,l,mid,x,y);
		else if (x>mid) return sum(v+v+1,mid+1,r,x,y);
		else return sum(v+v,l,mid,x,mid)+sum(v+v+1,mid+1,r,mid+1,y);
	}
	void modify1(int v,int l,int r,int x,int y)
	{
		if (l==r)
		{
			tree1[v]=y;
			return;
		}
		int mid=(l+r)>>1;
		if (x<=mid) modify1(v+v,l,mid,x,y);
		else modify1(v+v+1,mid+1,r,x,y);
		tree1[v]=tree1[v+v]+tree1[v+v+1];
		return;
	}
	void modify2(int v,int l,int r,int x,int y)
	{
		if (l==r)
		{
			tree2[v]=y;
			return;
		}
		int mid=(l+r)>>1;
		if (x<=mid) modify2(v+v,l,mid,x,y);
		else modify2(v+v+1,mid+1,r,x,y);
		tree2[v]=tree2[v+v]+tree2[v+v+1];
		return;
	}
	int main()
	{
		freopen("phalanx.in","r",stdin);
		freopen("phalanx.out","w",stdout);
		scanf("%d%d%d",&n,&m,&q);
		if (n==1)
		{
			n=m+q;
			build(1,1,n);
			while (q--)
			{
				int x,y;
				scanf("%d%d",&x,&y);
				int k=find(1,1,n,y);
				printf("%d\n",sum(1,1,n,1,k));
				modify2(1,1,n,k,0);
				m++;
				modify2(1,1,n,m,1);
				modify1(1,1,n,m,-sum(1,1,n,k+1,m-1));
			}
			return 0;
		}
		if (n<=1000 && m<=1000)
		{
			for (int i=1;i<=n;i++)
				for (int j=1;j<=m;j++)
					a[i][j]=(i-1)*m+j;
			while (q--)
			{
				int x,y,z;
				scanf("%d%d",&x,&y);
				z=a[x][y];
				printf("%d\n",z);
				for (int j=y;j<m;j++)
					a[x][j]=a[x][j+1];
				for (int i=x;i<n;i++)
					a[i][m]=a[i+1][m];
				a[n][m]=z;
			}
			return 0;
		}
		return 0;
	}

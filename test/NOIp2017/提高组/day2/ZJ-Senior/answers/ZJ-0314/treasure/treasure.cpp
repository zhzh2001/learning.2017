	#include<iostream>
	#include<cstdio>
	#include<algorithm>
	#include<cstring>
	#include<cmath>
	using namespace std;
	int n,m,adj[15][15],q[105],num[105],ans,head,tail,s;
	bool flag[105];
	int main()
	{
		freopen("treasure.in","r",stdin);
		freopen("treasure.out","w",stdout);
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				adj[i][j]=1000000000;
		for (int i=1;i<=m;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			adj[x][y]=min(adj[x][y],z);
			adj[y][x]=min(adj[y][x],z);
		}
		ans=1000000000;
		for (int i=1;i<=n;i++)
		{
			for (int j=1;j<=n;j++)
				flag[j]=false;
			head=1;tail=1;
			q[head]=i;flag[i]=true;num[i]=1;
			s=0;
			while (head<=tail)
			{
				int u=q[head];
				for (int v=1;v<=n;v++)
					if (flag[v]==false && adj[u][v]!=1000000000)
					{
						tail++;
						flag[v]=true;
						q[tail]=v;
						num[v]=num[u]+1;
						s+=adj[u][v]*num[u];
					}
				head++;
			}
			ans=min(ans,s);
		}
		printf("%d\n",ans);
		return 0;
	}

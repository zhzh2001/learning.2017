#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std ;
#define lb(x) ((x)&(-(x)))
#define rep(i,a,b) for(int i=a;i<=b;i++)
const int inf=0x3f3f3f3f ;
int n,m,val,mp[15],f[5000][15],bt[5000],c[15],dd[5000],v[15][15],sum[5000][15] ;
bool ok(int msk) {
	if (f[msk][dd[lb(msk)]]<inf) return 1 ; return 0 ;
}
void dfs(int t,int msk,int Msk,int rt) {
	if (msk==0) {
		int res=0,ree=0 ;
		rep(i,1,t-1) {
			int tmp=inf,atm=0 ;
			rep(j,0,n-1) if (mp[rt]&(1<<j)) if (c[i]&(1<<j)) {
				int S=f[c[i]][j+1]+sum[c[i]][j+1]+v[rt][j+1] ;
				if (tmp>S) tmp=S,atm=sum[c[i]][j+1]+v[rt][j+1] ;
					else if (tmp==S) atm=min(atm,sum[c[i]][j+1]+v[rt][j+1]) ;
			}
			res+=tmp ;
			ree+=atm ;
		}
		if (f[Msk][rt]>res) f[Msk][rt]=res,sum[Msk][rt]=ree ; else
			if (f[Msk][rt]==res) sum[Msk][rt]=min(sum[Msk][rt],ree) ;
		return ;
	}
	for (int i=msk;i;i=(i-1)&msk) if (i&mp[rt]) if (ok(i)) {
		c[t]=i ;
		dfs(t+1,msk^i,Msk,rt) ;
	}
}
int main() {
	freopen("treasure.in","r",stdin) ;
	freopen("treasure.out","w",stdout) ;
	scanf("%d%d",&n,&m) ;
	rep(i,1,m) {
		int x,y ; scanf("%d%d%d",&x,&y,&val) ;
		if (v[x][y]!=0) v[x][y]=min(v[x][y],val) ; else v[x][y]=val ;
		v[y][x]=v[x][y] ;
		mp[x]|=1<<y-1 ;
		mp[y]|=1<<x-1 ;
	}
	rep(i,1,(1<<n)-1) rep(j,1,n) f[i][j]=inf ;
	rep(i,0,n-1) dd[1<<i]=i+1 ;
//	rep(i,0,n-1) f[(1<<i)][i+1]=0 ;
	bt[0]=0 ;
	rep(i,1,(1<<n)-1) bt[i]=bt[i-lb(i)]+1 ;
	rep(i,1,(1<<n)-1) {
		rep(j,0,n-1) if (((1<<j)&(i))>0) {
			dfs(1,i^(1<<j),i,j+1) ;
//			printf("%d %d : %d\n",i,j+1,f[i][j+1]) ;
		}
	}
	int ans=inf ; //printf("%d %d\n",f[0],ans) ;
	rep(i,1,n) ans=min(ans,f[(1<<n)-1][i]) ;
	printf("%d\n",ans) ;
	return 0 ;
}

#include <cstdio>
#include <algorithm>
using namespace std ;
#define rep(i,a,b) for(int i=a;i<=b;i++)
const int N=50050 ;
int n,m,Q ;
int x[N],y[N],mp[N] ;
unsigned int a[505][N],b[N] ;

int A[1005][1005] ;
int main() {
//	printf("%d\n",sizeof(a)/1024/1024) ;
	freopen("phalanx.in","r",stdin) ;
	freopen("phalanx.out","w",stdout) ;
	scanf("%d%d%d",&n,&m,&Q) ;
	if (n<=1000 && m<=1000) {
		rep(i,1,n) rep(j,1,m) A[i][j]=(i-1)*m+j ;
		while ((Q--)>0) {
			int xx,yy ; scanf("%d%d",&xx,&yy) ;
			int ans=A[xx][yy] ;
			printf("%d\n",ans) ;
			rep(i,yy,m-1) A[xx][i]=A[xx][i+1] ;
			rep(i,xx,n-1) A[i][m]=A[i+1][m] ;
			A[n][m]=ans ;
		}
	}
	else {
		rep(i,1,Q) scanf("%d%d",x+i,y+i) ;
		int num=0 ;
		rep(i,1,Q) if (!mp[x[i]]) {
			mp[x[i]]=++num ;
			rep(j,1,m-1) a[num][j]=(x[i]-1)*(long long)m+j ;
		}
		rep(i,1,n) b[i]=i*(long long)m ;
		rep(i,1,Q) {
			int t=mp[x[i]] ;
			long long ans ;
			if (y[i]==m) ans=b[x[i]] ; else ans=a[t][y[i]] ;
			rep(j,y[i],m-2) a[t][j]=a[t][j+1] ;
			if (y[i]<m) a[t][m-1]=b[x[i]] ;
			rep(j,x[i],n-1) b[j]=b[j+1] ; b[n]=ans ;
			printf("%lld\n",ans) ;
		}
	}
	return 0 ;
}

#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std ;
#define rep(i,a,b) for(int i=a;i<=b;i++)
int n,H,R,x[1050],y[1050],z[1050],fa[1050] ;
int efind(int t) {
	if (fa[t]==t) return t ; else {
		fa[t]=efind(fa[t]) ; return fa[t] ;
	}
}
void uni(int x,int y) {
	x=efind(x),y=efind(y) ;
	fa[x]=y ;
}
int main() {
	freopen("cheese.in","r",stdin) ;
	freopen("cheese.out","w",stdout) ;
	int cas ; scanf("%d",&cas) ;
	while ((cas--)>0) {
		scanf("%d%d%d",&n,&H,&R) ;
		rep(i,1,n) scanf("%d%d%d",x+i,y+i,z+i) ;
		rep(i,0,n+1) fa[i]=i ;
		rep(i,1,n) {
			if (z[i]<=R) uni(0,i) ;
			if (z[i]>=H-R) uni(i,n+1) ;
		}
		rep(i,1,n) rep(j,i+1,n) {
			double dx=x[i]-x[j],dy=y[i]-y[j],dz=z[i]-z[j] ;
			double tmp=sqrt(dx*dx+dy*dy+dz*dz) ;
			if (tmp<=R*2) uni(i,j) ;
		}
		if (efind(0)==efind(n+1)) puts("Yes") ; else puts("No") ;
	}
	return 0 ;
}

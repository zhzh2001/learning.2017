#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

inline void FO() {
    freopen("treasure.in", "r", stdin);
    freopen("treasure.out", "w", stdout);
    return;
}


const int N = 13, P = 12, INF = 0x3f3f3f3f;

inline void Cmin(int &a, int b) {
    if (a > b) a = b;
    return;
}
inline void Add(int &a, int b) {
    if (a == INF || b == INF) a = INF;
    else a += b;
    return;
}

int n, m;
int cost[N][N], temp[N], a[N], b[N];
int upc[1 << P][1 << P];
int f[N][1 << P];

int main() {
    FO();
    scanf("%d%d", &n, &m);
    memset(cost, 0x3f, sizeof(cost));
    for (int i = 1; i <= m; ++i) {
        int u, v, w;
        scanf("%d%d%d", &u, &v, &w);
        Cmin(cost[u][v], w);
        Cmin(cost[v][u], w);
    }
    for (int mask = 0; mask < (1 << n); ++mask) {
        int size1 = 0, size2 = 0;
        for (int i = 0; i < n; ++i)
            if (mask & (1 << i)) a[++size1] = i + 1;
            else b[++size2] = i + 1;
        memset(temp, 0x3f, sizeof(temp));
        for (int i = 1; i <= size1; ++i)
            for (int j = 1; j <= size2; ++j)
                Cmin(temp[b[j]], cost[a[i]][b[j]]);
        for (int j = mask ^ ((1 << n) - 1), i = j; i; i = (i - 1) & j)
            for (int k = 0; k < n; ++k)
                if (i & (1 << k)) Add(upc[mask][i], temp[k + 1]);
    }
    memset(f, 0x3f, sizeof(f));
    for (int i = 0; i < n; ++i)
        f[0][1 << i] = 0;
    for (int dep = 1; dep < n; ++dep)
        for (int mask = 0; mask < (1 << n); ++mask)
            if (f[dep - 1][mask] < INF) {
                for (int j = mask ^ ((1 << n) - 1), i = j; i; i = (i - 1) & j)
                    if (upc[mask][i] < INF) Cmin(f[dep][mask ^ i], f[dep - 1][mask] + upc[mask][i] * dep);
            }
    int res = INF;
    for (int i = 0; i < n; ++i)
        Cmin(res, f[i][(1 << n) - 1]);
    printf("%d\n", res);
    return 0;
}

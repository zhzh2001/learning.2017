#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>

using namespace std;

inline void FO() {
    freopen("cheese.in", "r", stdin);
    freopen("cheese.out", "w", stdout);
    return;
}

inline double Sqr(double x) {
    return x * x;
}

const int N = 1010;
const double EPS = 1E-10;

inline int Sgn(double x) {
    return (x < -EPS) ? (-1) : (x > EPS);
}

inline bool LE(double x, double y) {
    return !Sgn(fabs(x - y)) || x < y;
}

int n;
double h, r;
double x[N], y[N], z[N];
int f[N];
int Gf(int k) {
    return (f[k] != k) ? (f[k] = Gf(f[k])) : (k);
}
void Union(int i, int j) {
    int k1 = Gf(i), k2 = Gf(j);
    if (k1 != k2) f[k2] = k1;
    return;
}

double Dis(int i, int j) {
    return Sqr(x[i] - x[j]) + Sqr(y[i] - y[j]) + Sqr(z[i] - z[j]);
}

void Solve() {
    scanf("%d%lf%lf", &n, &h, &r);
    for (int i = 0; i <= n + 1; ++i)
        f[i] = i;
    for (int i = 1; i <= n; ++i) {
        scanf("%lf%lf%lf", &x[i], &y[i], &z[i]);
        if (z[i] <= r) Union(i, 0);
        if (z[i] + r >= h) Union(i, n + 1);
        for (int j = 1; j < i; ++j)
            if (LE(Dis(i, j), Sqr(r * 2))) Union(i, j);
    }
    if (Gf(0) != Gf(n + 1)) puts("No");
    else puts("Yes");
    return;
}

int main() {
    FO();
    int t;
    scanf("%d", &t);
    while (t--) Solve();
    return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

inline void FO() {
    freopen("phalanx.in", "r", stdin);
    freopen("phalanx.out", "w", stdout);
    return;
}

const int N = 300010, M = 300010, Q = 300010, SIZE = 15000000;

int n, m, q;

int tot, root[N];
struct Node {
    int l, r, lc, rc, sum, key;
} p[SIZE];

inline void Update(int k) {
    p[k].sum = p[p[k].lc].sum + p[p[k].rc].sum;
    return;
}

int Build(int l, int r) {
    int k = ++tot;
    p[k].l = l;
    p[k].r = r;
    if (l == r) {
        if (l < m) {
            p[k].sum = 1;
            p[k].key = l;
        }
        return k;
    }
    int m = (l + r) >> 1;
    p[k].lc = Build(l, m);
    p[k].rc = Build(m + 1, r);
    Update(k);
    return k;
}
int Insert(int k, int x, int ex, int key) {
    int K = ++tot;
    p[K] = p[k];
    if (p[K].l == p[K].r) {
        p[K].sum = ex;
        p[K].key = key;
        return K;
    }
    if (x <= p[p[K].lc].sum) p[K].lc = Insert(p[K].lc, x, ex, key);
    else p[K].rc = Insert(p[K].rc, x - p[p[K].lc].sum, ex, key);
    Update(K);
    return K;
}
int Query(int k, int x) {
    if (p[k].l == p[k].r) return p[k].key;
    if (x <= p[p[k].lc].sum) return Query(p[k].lc, x);
    return Query(p[k].rc, x - p[p[k].lc].sum);
}

namespace BIT {
    int bit[M + Q], key[M + Q];
    inline int Lowbit(int x) {
        return x & -x;
    }
    void Modify(int x, int k) {
        for (; x <= m + q; x += Lowbit(x))
            bit[x] += k;
        return;
    }
    int Query(int x) {
        --x;
        int k = 0;
        for (int i = 20; ~i; --i) {
            int j = k + (1 << i);
            if (j <= m + q && x >= bit[j]) x -= bit[k = j];
        }
        return k + 1;
    }
    void Init() {
        for (int i = 1; i <= m; ++i) {
            Modify(i, 1);
            key[i] = (i - 1) * n + m;
        }
        return;
    }
}

int cnt[N];

int main() {
    FO();
    scanf("%d%d%d", &n, &m, &q);
    BIT::Init();
    root[1] = Build(1, m - 1);
    for (int i = 2; i <= n; ++i)
        root[i] = root[1];
    cnt[0] = n;
    for (int i = 1; i <= n; ++i)
        cnt[i] = m - 1;
    while (q--) {
        int x, y;
        scanf("%d%d", &x, &y);
        if (y != m) {
            int res = Query(root[x], y) + (x - 1) * m;
            printf("%d\n", res);
            root[x] = Insert(root[x], y, 0, 0);
            int np = BIT::Query(x);
            root[x] = Insert(root[x], ++cnt[x], 1, BIT::key[np] - (x - 1) * m);
            BIT::Modify(np, -1);
            BIT::Modify(++cnt[0], 1);
            BIT::key[cnt[0]] = res;
        } else {
            int np = BIT::Query(x), res = BIT::key[np];
            printf("%d\n", res);
            BIT::Modify(np, -1);
            BIT::Modify(++cnt[0], 1);
            BIT::key[cnt[0]] = res;
        }
    }
    return 0;
}

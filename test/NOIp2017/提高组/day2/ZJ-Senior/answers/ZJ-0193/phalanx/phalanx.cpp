#include<cstdio>
#include<cstring>
#include<iostream>
#include<cmath>
#include<cstdlib>
#include<string>
#include<vector>
#include<set>
#include<queue>
using namespace std;
#define ll long long
#define x0 adjhfzgs
#define x1 sdkhfas
#define y1 sfkhgsss
#define y0 sihgvssss
inline void rd(int &res){
	res=0;char c;
	while(c=getchar(),c<48);
	do res=(res<<1)+(res<<3)+(c^48);
	while(c=getchar(),c>=48);
}
void pt(int x){
	if(!x)return ;
	pt(x/10);
	putchar((x%10)^48);
}
void sc(int x){
	if(x<0){putchar('-');x=-x;}
	if(x)pt(x);
	else putchar('0');
	putchar('\n');
}
inline void Max(int &x,int y){if(x<y)x=y;}
inline void Min(int &x,int y){if(x>y)x=y;}
int m,n,q;
namespace P1{
	const int M=1005;
	int id[M][M];
	void solve(){
		int c,i,j,k,a,b;
		for(i=1;i<=n;i++)
			for(j=1;j<=m;j++)id[i][j]=(i-1)*m+j;
		while(q--){
			rd(a),rd(b);
			c=id[a][b];
			printf("%d\n",c);
			for(j=b;j<m;j++)id[a][j]=id[a][j+1];
			for(i=a;i<n;i++)id[i][m]=id[i+1][m];
			id[n][m]=c;
		}
	}
}
namespace P2{
	const int M=5e5+5;
	int x[M],y[M];
	void solve(){
		int i,j,k,a,b;
		for(i=1;i<=q;i++){
			rd(x[i]),rd(y[i]);
			a=x[i],b=y[i];
			for(j=i-1;j>=1;j--){
				if(a==x[j]&&b>=y[j]){
					if(b!=m){
						b=b+1;
					}
					else if(a==n)a=x[j],b=y[j];
					else a=a+1,b=m;
				}
				else if(b==m&&a>x[j]){
					if(a!=n)a++;
					else a=x[j],b=y[j];
				}
			}
			printf("%d\n",(a-1)*m+b);
		}
	}
}
namespace P3{
	const int M=5e5+5;
	int A[M],B[M];
	void add(int x,int t){
		while(x<=2*m){
			B[x]+=t;
			x+=x&-x;
		}
	}//
	int Sum(int x){
		int res=0;
		while(x){
			res+=B[x];
			x-=x&-x;
		}
		return res;
	}
	void solve(){
		int i,j,k,x,y;
		for(i=1;i<=m;i++)A[i]=i,add(i,1);
		int tot=m;
//		printf("%d\n",1);
		while(q--){
			rd(x);rd(y);
			int L=1,R=tot,res=1;
			while(L<=R){
				int mid=L+R>>1;
				if(Sum(mid)<y){
					L=mid+1;
				}
				else res=mid,R=mid-1;
			}
			sc(A[res]);
			add(res,-1);
			A[++tot]=A[res];A[res]=0;
			add(tot,1);
		}
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	rd(n),rd(m),rd(q);
	if(n<=1000&&m<=1000&&q<=20000)
	P1::solve();
	else if(1ll*q*q<=20000000)
	P2::solve();
	else P3::solve();
	return 0;
}

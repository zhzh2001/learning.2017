#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
using namespace std;
int n,m;
long long Vmin=0x9f,vnow;
int v [1010] [1010];
int node [1010] [1010];
int tail,head;
int p [10100];
int vis [1010];

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	int a,b,c;
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			v [i] [j]=0x9f;
	for(int i = 1; i <= m; i++){
		scanf("%d%d%d",&a,&b,&c);
		v [a] [b]=v [b] [a]=c;
		node [a] [++node [a] [0]]=i;
	}
	for(int i = 1; i <= n; i++){
		int tot = 1;memset(p,0,sizeof(p));tail=1;head=1;vnow=0;
		int now=p [head++]=i;
		while(tot<=n&&tail>=head){
			for(int j = 1; j <= node [now] [0]; j++){
				if(!vis [node [now] [j]]){
					p [++tail]=node [now] [j];
					vnow+=tot*v [now] [node [now] [j]];
					vis [node [now] [j]]=1;
					tot++;
				}
			}
		}
		if(vnow>Vmin)Vmin=vnow;
	}
	printf("%lld",vnow);
	
	return 0;
}

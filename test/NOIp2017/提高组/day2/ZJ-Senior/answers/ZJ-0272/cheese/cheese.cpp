#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<cmath>
#define MAXN 1000 + 10
using namespace std;
int t,flag1,flag2,zmin,zmax;
long long x [MAXN],y [MAXN],z [MAXN];
double dis [MAXN] [MAXN];
int d [MAXN] [MAXN];

double dist(int a,int b)
{
	return sqrt((x [a]-x [b])*(x [a]-x [b])*1.0+(y [a]-y [b])*(y [a]-y [b])*1.0+(z [a]-z [b])*(z [a]-z [b])*1.0);
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for(int k = 1; k <= t; k++)
	{
		flag2=flag1=0;
		int n,h,r;
		scanf("%d%d%d",&n,&h,&r);
		for(int i = 1; i <= n; i++){
			x [i]=y [i]=z [i]=0;
			for(int j = 1; j <= n; j++){
				dis [i] [j]=0x9fff;d [i] [j]=0;
			}
		}
		for(int i = 1; i <= n; i++){
			int a,b,c;
			scanf("%d%d%d",&a,&b,&c);
			if((c-r)<=0)flag1=1;
			if((c+r)>=h)flag2=1;
			x [i]=a;y [i]=b;z [i]=c; 
		}
		if(!flag1&&flag2)printf("No");
		else{
			for(int i = 1; i < n; i++)
				for(int j =i+1; j <= n; j++){
					dis [i] [j]=dis [j] [i]=dist(i,j);
					if(dis [i] [j] <=2*r)d [i] [j]=d [j] [i] =1;
				}
				int max=-1,min=0x9fff;
			for(int i = 1; i <= n; i++){
				if((z [i]-r)<=0&&z [i] <= min){
					zmin=i;
					min = z [i];
				}
				if((z [i]+r)>=h&&z [i] >= max){
					zmax=i;
					max=z [i];	
				}
			}
			for(int m = 1; m <= n; m++)
				for(int i =1; i <= n; i++)
				 	for(int j = 1; j <= n; j++)
				 		d [i] [j]=d [i] [j]||(d [i] [m]+d [m] [j]);
			if(d [zmax] [zmin])printf("Yes");else printf("No");
		}
	}	
	
	return 0;
}

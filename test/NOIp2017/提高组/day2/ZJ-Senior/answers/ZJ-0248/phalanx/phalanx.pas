program phalanx(input,output);
var
  n,m,q,p,i,j,qaq,x,y,k,t:longint;
  a:array[0..1005,0..1005] of longint;
begin
  assign(input,'phalanx.in');reset(input);
  assign(output,'phalanx.out');rewrite(output);
  readln(n,m,q);
  t:=0;
  for i:=1 to n do
    for j:=1 to m do
      begin
        inc(t);
        a[i,j]:=t;
      end;
  for k:=1 to q do
    begin
      readln(x,y);
      qaq:=a[x,y];
      writeln(qaq);
      for j:=y to m-1 do
        a[x,j]:=a[x,j+1];
      for i:=x to n-1 do
        a[i,m]:=a[i+1,m];
      a[n,m]:=qaq;
    end;
  close(input);close(output);
end.
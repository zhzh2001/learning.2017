program cheese(input,output) ;
var
  i,j,n,m,h,st,fi,t,k:longint;
  r:int64;
  pd:boolean;
  x,y,z:array[0..1005] of int64;
  father,start,finish:array[0..1005] of longint;
  f:array[0..1005,0..1005] of longint;
procedure link;
var
  i,j,k:longint;
  dis:int64;
begin
  for i:=1 to n do
    for j:=1 to n do
      begin
        dis:=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
        if dis<=sqr(2*r) then f[i,j]:=1;
      end;
end;

function getfather(xx:longint):longint;
begin
  if father[xx]=xx then exit(xx);
  father[xx]:=getfather(father[xx]);
  exit(father[xx]);
end;

procedure merge(x,y:longint);
var
  xx,yy:longint;
begin
  xx:=getfather(x);
  yy:=getfather(y);
  father[yy]:=xx;
end;

begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  readln(t);
  for k:=1 to t do
    begin
      readln(n,h,r);
      for i:=1 to n do readln(x[i],y[i],z[i]);
      for i:=1 to n do
        for j:=1 to n do
          f[i,j]:=0;
      link;
      for i:=1 to n do start[i]:=0;
      for i:=1 to n do finish[i]:=0;
      st:=0;fi:=0;
      for i:=1 to n do
        begin
          if z[i]<=r then begin inc(st);start[st]:=i;end;
          if z[i]+r>=h then begin inc(fi);finish[fi]:=i;end;
        end;
     // for i:=1 to n do writeln(start[i],' ',finish[i]);

      for i:=1 to n do father[i]:=i;
      for i:=1 to n do
        for j:=1 to n do
          if f[i,j]>0 then merge(i,j);

     { for i:=1 to n do begin
        for j:=1 to n do
          write(f[i,j],' ');writeln;end;   }

      pd:=false;
      for i:=1 to st do
        for j:=1 to fi do
          if father[start[i]]=father[finish[j]] then pd:=true;
//      for i:=1 to n do writeln(father[i]);

      if pd then writeln('Yes') else writeln('No');
    end;

  close(input);close(output);
end.
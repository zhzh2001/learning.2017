program treasure(input,output);
var
  w,sum:array[0..20] of longint;
  flag:array[0..20] of boolean;
  map:array[0..20,0..20] of longint;
  i,j,k,n,m,ans,x,y,v,mini,bj:longint;
function min(a,b:longint):longint;
begin
  if a<b then exit(a) else exit(b);
end;
procedure dfs(p,k:longint);
var
  i,j:longint;
begin
  w[p]:=min(w[p],k);
  for i:=p+1 to n do
    if (not flag[i]) and (map[p,i]<>500005) then dfs(i,k+1);
end;
//-----{main}-----
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      map[i,j]:=500005;
  for i:=1 to m do
    begin
      readln(x,y,v);
      map[x,y]:=min(map[x,y],v);
      map[y,x]:=min(map[y,x],v);
    end;
{  for i:=1 to n do   begin
    for j:=1 to n do
      write(map[i,j],' ');writeln;end;    }
  mini:=500005;
  for i:=1 to n do
    begin
      for j:=1 to n do w[j]:=50000;
      for j:=1 to n do flag[j]:=false;
      flag[i]:=true;
      dfs(i,0);
      for j:=1 to n do
        if j<>i then sum[i]:=sum[i]+w[j];
      if sum[i]<mini then begin mini:=sum[i];bj:=i;end;
     // for j:=1 to n do writeln(w[j]);writeln;writeln(sum[i]);writeln;
    end;
  ans:=mini*v;
  writeln(ans);
  close(input);close(output);
end.

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define reg register int
#define rep(i, a, b) for (reg i = a; i <= b; i++)
#define per(i, a, b) for (reg i = a; i >= b; i--)
#define travel(i, u) for (reg i = head[u]; i; i = edge[i].next)
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}
const int N = 1200000, QQ = 500, mN = 1000, M = 50000;
int n, m, q;
ll F[mN + 2][M + 2];
ll Last[N / 4 + 2];
int Pos[N / 4 + 2];
ll Pos2[N / 4 + 2];
struct node
{
	int x, y, id;
}Q[QQ + 2], T[QQ + 2];

int cmp(node X, node Y)
{
	return X.x < Y.x;
}
ll Calc(int x, int y, int z)
{
	if (!z) return F[x][y] ? F[x][y] : (ll)(x - 1) * m + y;
	else if (y != m) return F[z][y] ? F[z][y] : (ll)(x - 1) * m + y;
	else return Last[x] ? Last[x] : (ll)x * m;
}
void Solve1()
{
	while (q--)
	{
		int x = read(), y = read();
		ll t = Calc(x, y, 0);
		for (int j = y; j < m; j++)
			F[x][j] = Calc(x, j + 1, 0);
		for (int i = x; i < n; i++)
			F[i][m] = Calc(i + 1, m, 0);
		F[n][m] = t;
		printf("%lld\n", t);
	}
}
void Solve2()
{
	rep(i, 1, q) Q[i].x = read(), Q[i].y = read(), Q[i].id = i;
	sort(Q + 1, Q + q + 1, cmp);
	int now = 0;
	rep(i, 1, q)
	{
		if (Q[i].x != Q[i - 1].x) now++;
		T[Q[i].id].x = Q[i].x, T[Q[i].id].y = Q[i].y, T[Q[i].id].id = now;
	}
	rep(i, 1, n) Last[i] = (ll)i * m;
	rep(i, 1, q)
	{
		ll t = Calc(T[i].x, T[i].y, T[i].id);
		for (int j = T[i].y; j < m - 1; j++) F[T[i].id][j] = Calc(T[i].x, j + 1, T[i].id);
		if (T[i].y != m) F[T[i].id][m - 1] = Last[T[i].x];
		for (int j = T[i].x; j < n; j++) Last[j] = Last[j + 1];
		Last[n] = t;
		printf("%lld\n", t);
	}
}

struct Node
{
	int Sum[N + 2];
	void up(int u)
	{
		Sum[u] = Sum[u + u] + Sum[u + u + 1];
	}
	void Build(int u, int l, int r)
	{
		if (l == r)
		{
			if (l <= m) Sum[u] = 1;
			return;
		}
		int mid = (l + r) >> 1;
		Build(u + u, l, mid);
		Build(u + u + 1, mid + 1, r);
		up(u);
	}
	void modify(int u, int l, int r, int t)
	{
		if (l == r)
		{
			Sum[u] = 1;
			return;
		}
		int mid = (l + r) >> 1;
		if (t > mid) modify(u + u + 1, mid + 1, r, t);
		else modify(u + u, l, mid, t);
		up(u);
	}
	int query(int u, int l, int r, int t)
	{
		if (l == r)
		{
			Sum[u]--;
			return l;
		}
		int mid = (l + r) >> 1, TT;
		if (t > Sum[u + u]) TT = query(u + u + 1, mid + 1, r, t - Sum[u + u]);
		else TT = query(u + u, l, mid, t);
		up(u);
		return TT;
	}
}Segment_tree;
void Solve3()
{
	Segment_tree.Build(1, 1, m + q);
	rep(i, 1, m) Pos[i] = i;
	rep(i, 1, q)
	{
		int x = read(), y = read();
		int pos = Segment_tree.query(1, 1, m + q, y);
		Pos[m + i] = Pos[pos];
		printf("%d\n", Pos[pos]);
		Segment_tree.modify(1, 1, m + q, m + i);
	}
}

void Solve4()
{
	Segment_tree.Build(1, 1, m + q);
	rep(i, 1, m) Pos2[i] = (ll)i;
	rep(i, 1, n) Last[i] = (ll)i * m;
	rep(i, 1, q)
	{
		int x = read(), y = read();
		int pos = Segment_tree.query(1, 1, m + q, y);
		Pos2[m + i] = Last[i + 1];
		Last[n + i] = Pos2[pos];
		printf("%lld\n", Pos2[pos]);
		Segment_tree.modify(1, 1, m + q, m + i);
	}
}

int sqz()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	n = read(), m = read(), q = read();
	if (q <= 500 && n <= 1000) Solve1();
	else if (q <= 500) Solve2();
	else if (n == 1) Solve3();
	else Solve4();
	return 0;
}

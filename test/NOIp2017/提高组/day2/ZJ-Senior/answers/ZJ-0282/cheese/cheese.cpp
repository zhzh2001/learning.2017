#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define reg register int
#define rep(i, a, b) for (reg i = a; i <= b; i++)
#define per(i, a, b) for (reg i = a; i >= b; i--)
#define travel(i, u) for (reg i = head[u]; i; i = edge[i].next)
const int N = 1000;
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}

int head[N + 5], flag[N + 5];
int n, h, r, num = 0;
struct node
{
	int vet, next;
}edge[2000005];
struct Node
{
	int x, y, z;
}Point[N + 5];
void addedge(int u, int v)
{
	edge[++num].vet = v;
	edge[num].next = head[u];
	head[u] = num;
}

int Calc(Node X, Node Y)
{
	ll now = (ll)r * r * 4 - (ll)(X.x - Y.x) * (X.x - Y.x);
	if (now < 0) return 0;
	now = now - (ll)(X.y - Y.y) * (X.y - Y.y);
	if (now < 0) return 0;
	now = now - (ll)(X.z - Y.z) * (X.z - Y.z);
	if (now < 0) return 0;
	return 1;
}
void dfs(int u)
{
	flag[u] = 1;
	travel(i, u)
	{
		int v = edge[i].vet;
		if (flag[v]) continue;
		dfs(v);
	}
}

int sqz()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int H_H = read();
	while (H_H--)
	{
		n = read(), h = read(), r = read();
		rep(i, 0, n + 1) head[i] = 0, flag[i] = 0;
		rep(i, 1, n)
			Point[i].x = read(), Point[i].y = read(), Point[i].z = read();
		rep(i, 1, n)
			if (Point[i].z - r <= 0)
			{
				addedge(0, i);
				addedge(i, 0);
			}
		rep(i, 1, n)
			if (Point[i].z + r >= h)
			{
				addedge(n + 1, i);
				addedge(i, n + 1);
			}
		rep(i, 1, n - 1)
			rep(j, i + 1, n)
				if (Calc(Point[i], Point[j]))
				{
					addedge(i, j);
					addedge(j, i);
				}
		dfs(0);
		if (flag[n + 1]) puts("Yes");
		else puts("No");
	}
	return 0;
}

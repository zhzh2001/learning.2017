#include<cmath>
#include<queue>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define sqz main
#define ll long long
#define reg register int
#define rep(i, a, b) for (reg i = a; i <= b; i++)
#define per(i, a, b) for (reg i = a; i >= b; i--)
#define travel(i, u) for (reg i = head[u]; i; i = edge[i].next)
ll read()
{
	ll x = 0; int zf = 1; char ch;
	while (ch != '-' && (ch < '0' || ch > '9')) ch = getchar();
	if (ch == '-') zf = -1, ch = getchar();
	while (ch >= '0' && ch <= '9') x = x * 10 + ch - '0', ch = getchar();
	return x * zf;
}
void write(ll y)
{
	if (y < 0) putchar('-'), y = -y;
	if (y > 9) write(y / 10);
	putchar(y % 10 + '0');
}
const int N = 20, M = 1000;

int num = 0, now, head[N + 5], deep[N + 5], flag[N + 5];
queue<int> Q;
struct node
{
	int vet, next, val;
}edge[2 * M + 5];
void addedge(int u, int v, int w)
{
	edge[++num].vet = v;
	edge[num].next = head[u];
	edge[num].val = w;
	head[u] = num;
	swap(u, v);
	edge[++num].vet = v;
	edge[num].next = head[u];
	edge[num].val = w;
	head[u] = num;
}
void dfs(int u, int fa)
{
	deep[u] = deep[fa] + 1;
	travel(i, u)
	{
		int v = edge[i].vet;
		if (v == fa) continue;
		now += deep[u] * edge[i].val;
		dfs(v, u);
	}
}
int bfs(int s)
{
	memset(flag, 0, sizeof flag);
	int T = 0;
	Q.push(s);
	flag[s] = 1;
	while (!Q.empty())
	{
		int u = Q.front();
		Q.pop();
		travel(i, u)
		{
			int v = edge[i].vet;
			if (!flag[v])
			{
				T += flag[u];
				flag[v] = flag[u] + 1;
				Q.push(v);
			}
		}
	}
	return T;
}

int sqz()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int n = read(), m = read();
	if (m == n - 1)
	{
		rep(i, 1, m)
		{
			int u = read(), v = read(), w = read();
			addedge(u, v, w);
		}
		int ans = 1e9;
		rep(i, 1, n)
		{
			now = 0;
			deep[0] = 0;
			dfs(i, 0);
			ans = min(ans, now);
		}
		printf("%d\n", ans);
	}
	else
	{
		int x, ans = 1e9;
		rep(i, 1, m)
		{
			int u = read(), v = read(), w = read();
			addedge(u, v, w);
			x = w;
		}
		rep(i, 1, n)
			ans = min(ans, bfs(i) * x);
		printf("%d\n", ans);
	}
	return 0;
}

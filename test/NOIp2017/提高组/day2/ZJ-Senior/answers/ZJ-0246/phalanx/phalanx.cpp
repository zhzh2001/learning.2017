#include<cstdio>
const int Q=300010,N=300010;
int n,m,q,x[Q],y[Q],a[1010][1010],b[N*2],f[N*2],c[N*2],l,r,mid,ans,d[501][50001],pos[50001];
inline int read(){int d=0,f=1; char c=getchar(); while (c<'0'||c>'9') c=getchar(); while (c>='0'&&c<='9') d=d*10+c-48,c=getchar(); return d*f;}
inline int query(int x){int res=0;for (int i=x;i;i-=i&-i) res+=f[i]; return res;}
inline void update(int x,int y){for (int i=x;i<=m+q;i+=i&-i) f[i]+=y;}
int W[20];
inline void write(int x){if (!x) putchar('0');int t=0;for (;x;x/=10) W[++t]=x%10;for (int i=t;i;i--) putchar((char)(W[i]+48));}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	for (int i=1;i<=q;i++) x[i]=read(),y[i]=read();
	if (n<=1000&&m<=1000)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				a[i][j]=(i-1)*m+j;
		for (int i=1;i<=q;i++)
		{
			int ans=a[x[i]][y[i]];
			printf("%d\n",ans);
			for (int j=y[i];j<m;j++) a[x[i]][j]=a[x[i]][j+1];
			for (int j=x[i];j<n;j++) a[j][m]=a[j+1][m];
			a[n][m]=ans;
		}
		return 0;
	}
	bool bo=1;
	for (int i=1;i<=q;i++) if (x[i]!=1) bo=0;
	if (bo)
	{
		if (n==1)
		{
			int t=m;
			for (int i=1;i<=m;i++) b[i]=i,update(i,1);
			for (int i=1;i<=q;i++)
			{
				l=y[i]; r=t; ans=0;
				while (l<=r)
				{
					mid=(l+r)>>1;
					if (query(mid)>=y[i]) ans=mid,r=mid-1; else l=mid+1;
				}
				write(b[ans]); puts("");
				b[++t]=b[ans]; update(ans,-1); update(t,1);
			}
		}
		else
		{
			int t=m,tt=n-1,h=1;
			for (int i=1;i<=m;i++) b[i]=i,update(i,1);
			for (int i=1;i<n;i++) c[i]=(i+1)*m;
			for (int i=1;i<=q;i++)
			{
				l=y[i]; r=t; ans=0;
				while (l<=r)
				{
					mid=(l+r)>>1;
					if (query(mid)>=y[i]) ans=mid,r=mid-1; else l=mid+1;
				}
				write(b[ans]); puts("");
				b[++t]=c[h++]; update(ans,-1); update(t,1); c[++tt]=b[ans];
			}
		}
		return 0;
	}
	int t=0;
	for (int i=1;i<=q;i++)
		if (!pos[x[i]])
		{
			pos[x[i]]=++t; d[t][0]=x[i];
			for (int j=1;j<=m;j++) d[t][j]=(x[i]-1)*m+j;
		}
	for (int i=1;i<=n;i++) b[i]=i*m;
	for (int i=1;i<=q;i++)
	{
		ans=d[pos[x[i]]][y[i]];
		write(ans); puts("");
		for (int j=y[i];j<m;j++) d[pos[x[i]]][j]=d[pos[x[i]]][j+1];
		for (int j=x[i];j<n;j++) b[j]=b[j+1];
		b[n]=ans;
		for (int j=1;j<=t;j++) d[j][m]=b[d[j][0]];
	}
	return 0;
}

#include<cstdio>
#include<algorithm>
using namespace std;
const int N=13,inf=1000000000;
struct E{int x,y,z;};
struct F{int x,y;}f[N];
int n,m,ans=inf,dep[N],a[N][N],id,q[N],p,s,mn[N];
bool b[N];
inline bool cmp(E a,E b){return a.x<b.x;}
inline void dfs(int k,int sum)
{
	if (sum>ans) return;
	if (k==n) {if (sum<ans) ans=sum; return;}
	int t=0; E c[n-k+1]; F tmp[N];
	for (p=1;p<=n;p++) tmp[p]=f[p];
	for (p=1;p<=n;p++) if (!b[p]&&f[p].y!=inf) c[++t].x=f[p].x,c[t].y=f[p].y,c[t].z=p;
	sort(c+1,c+1+t,cmp);
	for (int i=1;i<=t;i++)
	{
		id=c[i].z;
		dep[id]=dep[c[i].y]+1;
		b[id]=1;
		for (p=1;p<=n;p++)
			if (!b[p]&&a[id][p]!=-1&&(dep[id]+1)*a[id][p]<f[p].x)
			{
				f[p].x=(dep[id]+1)*a[id][p];
				f[p].y=id;
			}
		dfs(k+1,sum+c[i].x);
		b[c[i].z]=0;
		for (p=1;p<=n;p++) f[p]=tmp[p];
	}
	for (int i=1;i<=n;i++) if (b[i])
		for (int j=1;j<=n;j++) if (!b[i]&&f[j].y!=i&&a[i][j]!=-1)
		{
			dep[j]=dep[i]+1;
			b[j]=1;
			for (p=1;p<=n;p++) if (!b[p]&&a[j][p]!=-1&&(dep[j]+1)*a[j][p]<f[p].x)
			{
				f[p].x=(dep[j]+1)*a[j][p];
				f[p].y=j;
			}
			dfs(k+1,sum+dep[j]*a[i][j]);
			b[j]=0;
			for (p=1;p<=n;p++) f[p]=tmp[p];
		}
}
void bfs(int S)
{
	for (int i=1;i<=n;i++) b[i]=0;
	int h=1,t=1; q[1]=S; b[S]=1; dep[S]=0; f[S].x=0;
	while (h<=t)
	{
		int u=q[h++];
		for (int v=1;v<=n;v++) if (a[u][v]!=-1)
			if (!b[v]) b[v]=1,dep[v]=dep[u]+1,f[v].x=dep[v]*a[u][v],q[++t]=v;
	}
	int sum=0;
	for (int i=1;i<=n;i++) sum+=f[i].x;
	if (sum<ans) ans=sum;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			a[i][j]=-1;
	bool ljj=1; int V=-1;
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if (V==-1) V=z;
		else if (z!=V) ljj=0;
		if (a[x][y]==-1||a[x][y]>z) a[x][y]=a[y][x]=z;
	}
	if (ljj)
	{
		for (int i=1;i<=n;i++) bfs(i);
		printf("%d",ans);
		return 0;
	}
	for (int i=1;i<=n;i++)
	{
		for (int j=1;j<=n;j++) b[j]=0,f[j].x=inf;
		dep[i]=0; b[i]=1;
		for (int j=1;j<=n;j++) if (a[i][j]!=-1) f[j].x=a[i][j],f[j].y=i;
		dfs(1,0);
	}
	printf("%d",ans);
	return 0;
}

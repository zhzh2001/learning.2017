#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>

using namespace std;

#define ll long long

const int we=4005;
const int qw=2000006;

ll t,h,r,n,r0;
ll ecnt;
ll head,tail;
ll nxt[qw],adj[qw],go[qw];
bool rc[qw],vst[qw];
ll que[we];
bool bo;

struct node{
	ll x,y,z;
}a[we];

void init(){//!!!!!!
	for(ll i=1;i<=n*(n-1);i++){
		adj[i]=0;go[i]=0;nxt[i]=0;
	}
	for(ll i=0;i<=n+1;i++){
		rc[i]=false;vst[i]=false;
	}
	ecnt=1;
	head=1;tail=0;
	bo=false;
}

void add(ll u,ll v){
	nxt[++ecnt]=adj[u];adj[u]=ecnt;go[ecnt]=v;
	nxt[++ecnt]=adj[v];adj[v]=ecnt;go[ecnt]=u;
}

bool o_k(node a,node b){
	if(((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z))<=(r0*4))return true;
	return false;
}

void pd(){
	for(ll i=1;i<=n;i++){
		for(ll j=i+1;j<=n;j++){
			if(o_k(a[i],a[j]))add(i,j);
		}
	}

}

void bfs(){
	ll u,v;
	while(head<=tail){
		u=que[head];
		if(rc[u]==1){
			bo=true;
			return;
		}
		
		for(ll e=adj[u];e;e=nxt[e]){
			v=go[e];
			if(vst[v]==0){
				vst[v]=1;
				que[++tail]=v;
				if(rc[v]==1){
					bo=true;
					return;
				}
			}
		}
		head++;
	}
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&t);
	while(t>0){
		t--;
		scanf("%lld%lld%lld",&n,&h,&r);
		r0=r*r;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		}
		init();//!!!!!!!!!!
		pd();
		for(ll i=1;i<=n;i++){
			if(a[i].z+r>=h)rc[i]=true;
			if(a[i].z-r<=0){
				que[++tail]=i;vst[i]=true;
			}
		}
		bfs();
		if(bo){
			printf("Yes\n");
		}
		else printf("No\n");
	}
	return 0;
}

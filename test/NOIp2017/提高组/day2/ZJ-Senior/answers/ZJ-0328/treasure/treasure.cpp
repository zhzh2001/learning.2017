#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>

using namespace std;

const int inf=0x7f7f7f7f;

const int qw=50;

int dis[qw][qw];
bool bo[qw];
int vst[qw];
int n,m;
int dep[qw];
int ans=0,tot=inf;

void init(){
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)dis[i][j]=inf;
		dis[i][i]=0;
	}
}

void dfs(int rt,int sum){
	if(ans>=tot)return;// cut
	if(sum==n){
		tot=min(ans,tot);
		return;
	}
	for(int i=1;i<=n;i++){
		if(vst[i]>0||dis[i][rt]==inf)continue;//||i==rt
		vst[i]=rt;
	}
	for(int j=1;j<=n;j++){
		if(vst[j]==0||j==rt||bo[j]==1)continue;
		for(int i=1;i<=n;i++){
			if(bo[i]==0||dis[i][j]==inf||i==j)continue;
			dep[j]=dep[i]+1;bo[j]=1;
			ans+=dep[i]*dis[i][j];
			dfs(j,sum+1);
			ans-=dep[i]*dis[i][j];bo[j]=0;
		}
	}
	for(int i=1;i<=n;i++){
		if(vst[i]==rt)vst[i]=0;
	}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	init();
	int u,v,w;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		dis[u][v]=min(dis[u][v],w);
		dis[v][u]=min(dis[v][u],w);
	}
	ans=0;
	for(int i=1;i<=n;i++){//vst[qw]  can reach
	  for(int j=1;j<=n;j++){dep[j]=0;vst[j]=0;bo[j]=0;}
	  ans=0;
	  dep[i]=1;vst[i]=n+1;
		bo[i]=true;//can go
		dfs(i,1);// rt  当前有几个节点 
		bo[i]=false;
	}
	printf("%d",tot);
	return 0;
}

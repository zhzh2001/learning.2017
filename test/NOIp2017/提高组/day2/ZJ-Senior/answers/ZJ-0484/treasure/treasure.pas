var n,m,i,j,x,y,z:longint;sum,ans:int64;
dist:array[0..1000,0..1000]of longint;
procedure prim(x:longint);
var i,j,mini:longint;min:int64;
deep,a:array[0..1000]of int64;
flag:array[0..1000]of boolean;
begin
  for i:=1 to n do
  begin
    flag[i]:=false;
    a[i]:=dist[x,i];
    deep[i]:=1;
  end;
  flag[x]:=true;
  deep[x]:=0;
  for i:=2 to n do
  begin
    min:=a[1];mini:=1;
    for j:=1 to n do
      if a[j]<min then
      begin
        min:=a[j];
        mini:=j;
      end;
    flag[mini]:=true;
    sum:=sum+a[mini];
    for j:=1 to n do
      if (not flag[j])and(a[j]>dist[mini,j]*(deep[mini]+1)) then
      begin
        a[j]:=dist[mini,j]*(deep[mini]+1);
        deep[j]:=deep[mini]+1;
      end;
    a[mini]:=2100000000;
  end;
end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      dist[i,j]:=2100000000;
  for i:=1 to m do
  begin
    readln(x,y,z);
    if dist[x,y]>z then dist[x,y]:=z;
    if dist[y,x]>z then dist[y,x]:=z;
  end;
  ans:=-1;
  for i:=1 to n do
  begin
    sum:=0;
    prim(i);
    if (sum<ans)or(ans=-1) then ans:=sum;
  end;
  writeln(ans);
  close(input);close(output);
end.
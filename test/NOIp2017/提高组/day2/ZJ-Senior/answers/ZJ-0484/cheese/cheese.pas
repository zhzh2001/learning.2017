var n,h,r,bo,tt:longint;po:boolean;
x,y,z,flag:array[0..1010]of longint;
a:array[0..1010,0..1010]of boolean;
function pd(p1,p2:longint):boolean;
var dist:extended;
begin
  dist:=sqrt(sqr(x[p1]-x[p2])+sqr(y[p1]-y[p2])+sqr(z[p1]-z[p2]));
  if dist>2*r then exit(false) else exit(true);
end;
procedure add(p1,p2:longint);
begin
  a[p1,p2]:=po;
  a[p2,p1]:=po;
end;
procedure dij(p3:longint);
var i,j,k:longint;
begin
  for i:=1 to n do flag[i]:=0;
  flag[p3]:=1;
  for i:=1 to n do
  begin
    for j:=1 to n do
      if flag[j]=1 then break;
    if flag[j]=1 then
    begin
    k:=j;
    flag[k]:=2;
    for j:=1 to n do
      if a[k,j] and(flag[j]=0) then begin flag[j]:=1; end;
    if h-z[k]<=r then
    begin
      bo:=1;
      exit;
    end;
    end else exit;
  end;
end;
procedure work;
var i,j:longint;
begin
  readln(n,h,r);
  for i:=1 to n do
  begin
    readln(x[i],y[i],z[i]);
    for j:=1 to i-1 do
    begin
      po:=pd(i,j);
      add(i,j);
      add(j,i);
    end;
  end;
  bo:=0;
  for i:=1 to n do
  begin
    if z[i]<=r then dij(i);
    if bo<>0 then break;
  end;
  if bo=1 then writeln('Yes') else writeln('No');
end;
begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  readln(tt);
  while tt>0 do
  begin
    dec(tt);
    work;
  end;
  close(input);close(output);
end.

#include<cstdio>
#include<cctype>
#include<cstring>
#include<cmath>
#define eps 1e-10
const char write[][5]={"No","Yes"};
int n,h,r,d,head[1007],q[800005],X;
bool vis[1007];
struct ball{
	int x,y,z;
}a[1007];
struct edge{
	int to,nxt;
}e[2300060];
inline long long sqr(int a){return(long long)(a)*(long long)a;}
inline int readint(){
	char c=getchar();
	bool b=false;
	for(;!isdigit(c);c=getchar())b=c=='-';
	int d=0;
	for(;isdigit(c);c=getchar())
	d=(d<<3)+(d<<1)+(c^'0');
	return b?-d:d;
}
int bfs(){
	memset(vis,0,sizeof vis);
	vis[0]=true;
	int l=0,r=1;
	q[1]=0;
	while(l!=r){
		int u=q[l=l%800000+1];
		for(int i=head[u];i!=-1;i=e[i].nxt){
			int v=e[i].to;
			if(!vis[v]){
				if(v==X)return 1;
				vis[v]=true;
				q[r=r%800000+1]=v;
			}
		}
	}
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(int T=readint();T--;){
		memset(head,-1,sizeof head);
		n=readint(),h=readint(),r=readint();
		d=r<<1;
		for(int i=1;i<=n;++i)
		a[i].x=readint(),a[i].y=readint(),a[i].z=readint();
		int cnt=0;
		X=n+1;
		for(int i=1;i<=n;++i){
			for(int j=i+1;j<=n;++j){
				double dis=sqrt((long long)sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z));
				if(d-dis>=-eps){
					e[++cnt]=(edge){j,head[i]};
					head[i]=cnt;
					e[++cnt]=(edge){i,head[j]};
					head[j]=cnt;
				}
			}
			if(a[i].z<=r){
				e[++cnt]=(edge){i,head[0]};
				head[0]=cnt;
				e[++cnt]=(edge){0,head[i]};
				head[i]=cnt;
			}
			if(a[i].z+r>=h){
				e[++cnt]=(edge){i,head[X]};
				head[X]=cnt;
				e[++cnt]=(edge){X,head[i]};
				head[i]=cnt;
			}
		}
		puts(write[bfs()]);
	}
}

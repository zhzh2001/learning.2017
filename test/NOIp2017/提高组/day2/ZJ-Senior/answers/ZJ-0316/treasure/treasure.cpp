#include<cstdio>
#include<cctype>
#include<cstring>
int n,m,head[13],sz;
int ans,now,cnt,fa[13],h[13];
int dis[13][13],use[13];
struct edge{
	int from,to,dis,nxt;
}e[300],e2[300];
inline int readint(){
	char c=getchar();
	for(;!isdigit(c);c=getchar());
	int d=0;
	for(;isdigit(c);c=getchar())
	d=(d<<3)+(d<<1)+(c^'0');
	return d;
}
int find(int x){return fa[x]==x?x:(fa[x]=find(fa[x]));}
int pd(int now,int pre,int f){
	int p=0;
	for(int i=h[now];i;i=e2[i].nxt)
	if(e2[i].to!=pre){
		p+=pd(e2[i].to,now,f+1);
		p+=e2[i].dis*f;
		if(p>ans)return 0x3f3f3f3f;
	}
	return p;
}
void dfs(int t,int pre){
	int fj[13];
	memcpy(fj,fa,sz);
	if(t==n){
		int cnt=0;
		memset(h,0,sz);
		for(int i=1;i<n;++i){
			e2[++cnt]=(edge){e[use[i]].from,e[use[i]].to,e[use[i]].dis,h[e[use[i]].from]};
			h[e[use[i]].from]=cnt;
			e2[++cnt]=(edge){e[use[i]].to,e[use[i]].from,e[use[i]].dis,h[e[use[i]].to]};
			h[e[use[i]].to]=cnt;
		}
		for(int i=1;i<=n;++i){
			int p=pd(i,0,1);
			if(p<ans)ans=p;
		}
	}else
	for(int i=pre+1,ff=cnt;i<=ff;i+=2){
		int a=find(e[i].from),b=find(e[i].to);
		if(a!=b){
			fa[b]=a;
			use[t]=i;
			dfs(t+1,i);
			memcpy(fa,fj,sz);
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=readint(),m=readint();
	sz=sizeof fa;
	memset(dis,0x3f,sizeof dis);
	memset(head,0,sz);
	while(m--){
		int u=readint(),v=readint(),t=readint();
		if(dis[u][v]>t)dis[u][v]=dis[v][u]=t;
	}
	cnt=0;
	for(int i=1;i<=n;++i)
	for(int j=i+1;j<=n;++j)
	if(dis[i][j]<0x3f3f3f3f){
		e[++cnt]=(edge){i,j,dis[i][j],head[i]};
		head[i]=cnt;
		e[++cnt]=(edge){j,i,dis[j][i],head[j]};
		head[j]=cnt;
	}
	ans=0x3f3f3f3f,now=0;
	for(int i=1;i<=n;++i)fa[i]=i;
	dfs(1,0);
	printf("%d\n",ans);
	return 0;
}

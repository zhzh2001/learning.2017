#include<cstdio>
#include<cctype>
int n,m,q,a[1005][1005];
inline int readint(){
	char c=getchar();
	for(;!isdigit(c);c=getchar());
	int d=0;
	for(;isdigit(c);c=getchar())
	d=(d<<3)+(d<<1)+(c^'0');
	return d;
}
inline void putint(int a){
	int w=1;
	for(;w<=a;w*=10);
	for(w/=10;w;w/=10)putchar((a/w)^'0'),a%=w;
	putchar('\n');
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=readint(),m=readint(),q=readint();
	if(n<=1000&&m<=1000){
		for(int i=1;i<=n;++i)
		for(int j=1;j<=m;++j)a[i][j]=(i-1)*m+j;
		while(q--){
			int x=readint(),y=readint();
			int p=a[x][y];
			putint(p);
			for(int i=y+1;i<=m;++i)a[x][i-1]=a[x][i];
			for(int i=x+1;i<=n;++i)a[i-1][m]=a[i][m];
			a[n][m]=p;
		}
	}
	return 0;
}

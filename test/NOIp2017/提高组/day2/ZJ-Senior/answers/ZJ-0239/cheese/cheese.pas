var x,y,z,flag:array[1..1100]of longint;
    head,vet,next:array[1..2100000]of longint;
    cas,v,n,i,j,tot,h,r:longint;
    s:double;

function dis(x1,y1,z1,x2,y2,z2:double):double;
begin
 exit(sqrt(sqr(x1-x2)+sqr(y1-y2)+sqr(z1-z2)));
end;

procedure add(a,b:longint);
begin
 inc(tot);
 next[tot]:=head[a];
 vet[tot]:=b;
 head[a]:=tot;
end;

procedure dfs(u:longint);
var e,v:longint;
begin
 flag[u]:=1;
 e:=head[u];
 while e<>0 do
 begin
  v:=vet[e];
  if flag[v]=0 then dfs(v);
  e:=next[e];
 end;
end;

begin
 assign(input,'cheese.in'); reset(input);
 assign(output,'cheese.out'); rewrite(output);
 readln(cas);
 for v:=1 to cas do
 begin
  readln(n,h,r);
  for i:=1 to n do readln(x[i],y[i],z[i]);
  tot:=0;
  fillchar(head,sizeof(head),0);
  for i:=1 to n do
   for j:=1 to n do
    if i<>j then
    begin
     s:=dis(x[i],y[i],z[i],x[j],y[j],z[j]);
     if s-2*r<=1e-8 then
     begin
      add(i,j); add(j,i);
     end;
    end;
  for i:=1 to n do
   if (z[i]+r>=0)and(z[i]-r<=0) then add(n+1,i);
  for i:=1 to n do
   if (z[i]+r>=h)and(z[i]-r<=h) then add(i,n+2);
  fillchar(flag,sizeof(flag),0);
  dfs(n+1);
  if flag[n+2]=1 then writeln('Yes')
   else writeln('No');
 end;
 close(input);
 close(output);
end.

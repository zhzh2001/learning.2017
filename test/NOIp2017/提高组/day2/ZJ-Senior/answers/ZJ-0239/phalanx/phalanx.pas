var n,m,q,tot:longint;


procedure do1;
var f:array[1..1000,1..1000]of longint;
    i,j,tmp,x1,y1:longint;

begin
 for i:=1 to n do
  for j:=1 to m do f[i,j]:=m*(i-1)+j;
 for i:=1 to q do
 begin
  readln(x1,y1); tmp:=f[x1,y1];
  writeln(tmp);
  for j:=y1 to m-1 do f[x1,j]:=f[x1,j+1];
  f[x1,m]:=0;
  for j:=x1 to n-1 do f[j,m]:=f[j+1,m];
  f[n,m]:=tmp;
 end;

end;

procedure do2;
var f:array[1..10000000]of longint;
    head,vet1,vet2,vet3,next:array[1..3100000]of longint;
    i,j,tmp,x1,y1:longint;

procedure add(a,b,c:longint);
begin
 inc(tot);
 next[tot]:=head[a];
 vet1[tot]:=b;
 vet2[tot]:=c;
 vet3[tot]:=tot;
 head[a]:=tot;
end;

function hash(x,y:int64):longint;
var t,e,v1,v2:int64;
begin
 t:=(x*13796482+y*14796482) mod 3000017+1;
 e:=head[t];
 while e<>0 do
 begin
  v1:=vet1[e]; v2:=vet2[e];
  if (v1=x)and(v2=y) then exit(vet3[e]);
  e:=next[e];
 end;
 add(t,x,y);
 exit(tot);
end;

begin
 for i:=1 to n do
  for j:=1 to m do f[hash(i,j)]:=m*(i-1)+j;
  for i:=1 to q do
  begin
   readln(x1,y1); tmp:=f[hash(x1,y1)];
   writeln(tmp);
   for j:=y1 to m-1 do f[hash(x1,j)]:=f[hash(x1,j+1)];
   f[hash(x1,m)]:=0;
   for j:=x1 to n-1 do f[hash(j,m)]:=f[hash(j+1,m)];
   f[hash(n,m)]:=tmp;
  end;
end;

procedure do3;
var pre,next,num:array[0..710000]of longint;
    i,x1,y1,k,j,tmp,t1,t2,tot,t:longint;

begin

 for i:=1 to m do
 begin
  pre[i]:=i-1;
  next[i]:=i+1;
  num[i]:=i;
 end;
 next[0]:=1; next[m]:=-1; tot:=m;
 for i:=1 to q do
 begin
  readln(x1,y1);
  k:=0;
  for j:=1 to y1 do k:=next[k];
  tmp:=num[k]; writeln(tmp);
  t1:=pre[k]; t2:=next[k];
  pre[t2]:=t1;
  next[t1]:=t2; next[k]:=-1; pre[k]:=-1; num[k]:=-1;
  k:=0;
  while next[k]>0 do k:=next[k];
  inc(tot); pre[tot]:=k; next[k]:=tot; num[tot]:=tmp;
 end;

end;

begin
 assign(input,'phalanx.in'); reset(input);
 assign(output,'phalanx.out'); rewrite(output);
 readln(n,m,q);
 if (n<=1000)and(m<=1000) then do1
  else if n>1 then do2
   else do3;


 close(input);
 close(output);
end.


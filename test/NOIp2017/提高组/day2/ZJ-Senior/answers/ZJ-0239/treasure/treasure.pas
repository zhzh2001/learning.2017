const max=500001;
var dp,dis:array[0..5000,0..20]of int64;
    f:array[1..20,1..20]of int64;
    n,m,i,j,x,y,z,k,maxs,v,v1:longint;
    s,ans:int64;

function min(x,y:int64):int64;
begin
 if x<y then exit(x);
 exit(y);
end;

begin
 assign(input,'treasure.in'); reset(input);
 assign(output,'treasure.out'); rewrite(output);
 readln(n,m);
 for i:=1 to n do
  for j:=1 to n do f[i,j]:=1<<60;
 for i:=1 to n do f[i,i]:=0;
 for i:=1 to m do
 begin
  readln(x,y,z);
  f[x,y]:=min(f[x,y],z);
  f[y,x]:=min(f[y,x],z);
 end;
 maxs:=(1<<n)-1;
 for i:=1 to maxs do
  for j:=1 to n do
   if i and (1<<(j-1))=0 then
   begin
    dis[i,j]:=1<<60;
    for k:=1 to n do
     if (j<>k)and(i and (1<<(k-1))>0) then dis[i,j]:=min(dis[i,j],f[j,k]);
   end;

 m:=maxs;
 for i:=1 to maxs do
  for j:=0 to n+1 do dp[i,j]:=1<<60;
 for i:=1 to n do dp[1<<(i-1),0]:=0;
 for i:=0 to n do
  for j:=1 to maxs do
  begin
   v:=j xor m; v1:=v;
   while v>0 do
   begin
    s:=0;
    for k:=1 to n do
     if v and (1<<(k-1))>0 then
     begin
      s:=s+dis[j,k];
      if s>=(1<<60) then break;
     end;
    if s<(1<<60) then
     dp[j or v,i+1]:=min(dp[j or v,i+1],dp[j,i]+s*(i+1));
    v:=v1 and (v-1);
   end;
  end;
 ans:=1<<60;
 for i:=1 to n+1 do ans:=min(ans,dp[maxs,i]);
 if n=1 then ans:=0;
 writeln(ans);


 close(input);
 close(output);
end.

#include<cstdio>
#define ll long long
using namespace std;
int n,m,q,PoS[300005][2];
int ReaD(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int main(){
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	n=ReaD();
	m=ReaD();
	q=ReaD();
	for(int i=1;i<=q;i++){PoS[i][0]=ReaD();PoS[i][1]=ReaD();}
	for(int i=q-1;i>0;i--){
		for(int j=q;j>i;j--){
			if(PoS[j][0]<PoS[i][0]||PoS[j][1]<PoS[i][1])continue;
			if(PoS[j][0]==n&&PoS[j][1]==m){
				PoS[j][0]=PoS[i][0];
				PoS[j][1]=PoS[i][1];
				continue;
			}
			if(PoS[j][1]==m)PoS[j][0]++;
			if(PoS[j][0]==PoS[i][0])PoS[j][1]++;
		}
	}
	for(int i=1;i<=q;i++)printf("%d\n",(PoS[i][0]-1)*m+PoS[i][1]);
	return 0;
}

#include<cstdio>
#include<cstring>
#define MiN(x,y) ((x)<(y)?(x):(y))
using namespace std;
int MaP[15][15],n,m,DiS[15],DeEp[15],CoSt,MiNi,AnS=-1;
bool b[15];
int ReaD(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void PiRm(const int &st){
	memset(DiS,-1,sizeof(DiS));
	memset(DeEp,-1,sizeof(DeEp));
	memset(b,0,sizeof(b));
	CoSt=0;
	DiS[st]=0;
	DeEp[st]=1;
	b[st]=1;
	for(int i=1;i<=n;i++){
		if(i==st)continue;
		DiS[i]=MaP[st][i];
		if(DiS[i]!=-1)DeEp[i]=2;
	}
	for(int t=1;t<n;t++){
		MiNi=0;
		for(int i=1;i<=n;i++){
			if(b[i]||DiS[i]==-1)continue;
			if((!MiNi)||(DiS[i]<DiS[MiNi]))MiNi=i;
		}
		CoSt+=DiS[MiNi];
		b[MiNi]=1;
		for(int i=1;i<=n;i++){
			if(b[i])continue;
			if(MaP[MiNi][i]==-1)continue;
			if(DiS[i]!=-1&&DiS[i]<=MaP[MiNi][i]*DeEp[MiNi])continue;
			DiS[i]=MaP[MiNi][i]*DeEp[MiNi];
			DeEp[i]=DeEp[MiNi]+1;
		}
	}
	AnS=(AnS==-1?CoSt:MiN(AnS,CoSt));
}
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	n=ReaD();m=ReaD();
	memset(MaP,-1,sizeof(MaP));
	for(int i=0;i<m;i++){
		int x,y,TmP;
		x=ReaD();y=ReaD();TmP=ReaD();
		MaP[x][y]=(MaP[x][y]==-1?TmP:MiN(MaP[x][y],TmP));
		MaP[y][x]=(MaP[y][x]==-1?TmP:MiN(MaP[y][x],TmP));
	}
	for(int i=1;i<=n;i++)PiRm(i);
	printf("%d",AnS);
	return 0;
}

#include<cstdio>
#define ll long long
using namespace std;
struct Hp{
	ll x,y,z;
}Node[1005];
ll h,r;
int n,T;
bool b[1005],Flag;
ll ReaD(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void Dfs(const int &x){
	if(h-Node[x].z<=r){Flag=1;return;}
	b[x]=1;
	for(int i=0;i<n;i++)if(!b[i]){
		if((unsigned ll)((Node[x].x-Node[i].x)*(Node[x].x-Node[i].x))
		+(unsigned ll)((Node[x].y-Node[i].y)*(Node[x].y-Node[i].y))
		+(unsigned ll)((Node[x].z-Node[i].z)*(Node[x].z-Node[i].z))
		<=(unsigned ll)(r)*(unsigned ll)(r)*(unsigned ll)(4))Dfs(i);
		if(Flag)return;
	}
}
int main(){
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	T=ReaD();
	while(T-->0){	
		n=ReaD();h=ReaD();r=ReaD();Flag=0;
		for(int i=0;i<n;i++){
			Node[i].x=ReaD();
			Node[i].y=ReaD();
			Node[i].z=ReaD();
			b[i]=0;
		}
		for(int i=0;i<n;i++)if((!Flag)&&Node[i].z<=r&&(!b[i]))Dfs(i);
		printf(Flag?"Yes\n":"No\n");
	}
	return 0;
}

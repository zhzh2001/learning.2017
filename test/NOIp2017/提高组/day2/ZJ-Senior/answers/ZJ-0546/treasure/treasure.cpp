#include<bits/stdc++.h>
using namespace std;
#define forup(i,a,b)  for(int i=(a);i<=(b);i++)
#define fordown(i,a,b) for(int i=(a);i>=b;i--)
#define pb push_back
#define maxn 105
#define INF 100000007
typedef   long long ll;
int n,m;
int g[maxn][maxn];
bool vis[maxn];
int cost;
int cnt;
int ans=INF;
int a[maxn]; 
int dep[maxn];
int flag=1;
void dfs(int x)
{if(cost>ans) return;
 if(x==n+1)  {ans=min(ans,cost);  /*cout<<x<<' '<<cost<<endl;*/	return;}
  flag=0;
  forup(i,1,x-1) if(g[a[i]][a[x]]<INF)
  {flag=1;
   cost+=(dep[a[i]])*g[a[i]][a[x]];
   dep[a[x]]=dep[a[i]]+1;
   dfs(x+1);
   cost-=(dep[a[i]])*g[a[i]][a[x]];
   dep[a[x]]=0;
  }
  if(flag==0)  return; 
}
int main()
{freopen("treasure.in","r",stdin);
 freopen("treasure.out","w",stdout);
 cin>>n>>m;
 forup(i,1,n) forup(j,1,n)  g[i][j]=INF;
 forup(i,1,m)
 { int x,y,z; scanf("%d %d %d",&x,&y,&z);
   g[x][y]=min(g[x][y],z);g[y][x]=min(g[y][x],z);
 }
  forup(i,1,n) a[i]=i;
  do
  {dep[a[1]]=1;
   cost=0;
   flag=1;//按照这个顺序能否联通 
   dfs(2);
   dep[a[1]]=0;
   //if(flag==1) ans=min(ans,cost);
  }while(next_permutation(a+1,a+n+1));
  cout<<ans<<endl;
 fclose(stdin);fclose(stdout);	
	
return 0;	
} 

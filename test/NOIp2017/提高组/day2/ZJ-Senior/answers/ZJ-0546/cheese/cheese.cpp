#include<bits/stdc++.h>
using namespace std;
#define forup(i,a,b)  for(int i=(a);i<=(b);i++)
#define fordown(i,a,b) for(int i=(a);i>=b;i--)
#define int ll
#define pb push_back
#define maxn 3005
typedef   long long ll;
#define lb long double
int h,r,n;
struct Node{int x,y,z;}node[maxn];
vector<int> g[maxn];
void add(int x,int y){g[x].pb(y);g[y].pb(x);}
lb cal_dis(int a,int b)
{lb x=fabs((lb)(node[a].x)-(lb)(node[b].x));
 lb y=fabs((lb)(node[a].y)-(lb)(node[b].y));
 lb z=fabs((lb)(node[a].z)-(lb)(node[b].z)); 
 cout<<x*x+y*y+z*z<<endl;
 return x*x+y*y+z*z;
}
bool vis[maxn];
queue<int> q;
void bfs()
{forup(i,0,n+1) vis[i]=0;
 vis[0]=1; q.push(0); 
 while(!q.empty())
  {int x=q.front();q.pop();
   for(int i=0;i<g[x].size();i++)
    {int u=g[x][i];
     if(!vis[u])
      {vis[u]=1; q.push(u);}
	}
  }
if(vis[n+1]==1) cout<<"Yes"<<endl;
else            cout<<"No"<<endl;
}
void clearall(){forup(i,0,n+1) g[i].clear();}
signed main()
{
 freopen("cheese.in","r",stdin);
 freopen("cheese.out","w",stdout);
 int T; cin>>T;
 while(T--)
 {cin>>n>>h>>r;
  forup(i,1,n)
  {cin>>node[i].x>>node[i].y>>node[i].z;}
  forup(i,1,n) if(abs(node[i].z)<=r) {add(0,i);}
  forup(i,1,n) if(abs(node[i].z-h)<=r) add(i,n+1);
  forup(i,1,n)
   forup(j,i+1,n)
    {if(cal_dis(i,j)<=(lb)r*2*(lb)r*2)  add(i,j);}
  bfs();
  clearall();
 }
	
 fclose(stdin);fclose(stdout);	
	
return 0;	
}

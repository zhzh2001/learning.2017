#include<bits/stdc++.h>
#define maxn 15
#define maxm 1050
#define llINF 2e13
using namespace std;
inline int read()
{
	int num=0;
	char c=' ';
	bool flag=true;
	for(;c>'9'||c<'0';c=getchar())
	if(c=='-')flag=false;
	for(;c>='0'&&c<='9';num=num*10+c-48,c=getchar());
	return flag ? num : -num;
}

namespace graph
{
	int head[maxn],top;
	struct node
	{
		int dot_order,next_location,road_val;
	}a[maxm*2];
	struct edge
	{
		int x,y,val;
	}e[maxm];
	
	int n,m;
	void init()
	{
		n=read();
		m=read();
		for(int i=1;i<=m;i++)
		{
			int x=read();
			int y=read();
			int z=read();
			top++;
			a[top].dot_order=y;
			a[top].next_location=head[x];
			a[top].road_val=z;
			head[x]=top;
			
			top++;
			a[top].dot_order=x;
			a[top].next_location=head[y];
			a[top].road_val=z;
			head[y]=top;
			
			e[i].val=z;
			e[i].x=x;
			e[i].y=y;
		}
	}
}using namespace graph;

namespace tree_
{
	int depth[maxn],fa[maxn];
	void dfs(int u)
	{
		for(int i=head[u];i;i=a[i].next_location)
		{
			int to=a[i].dot_order;
			if(to==fa[u])continue;
			fa[to]=u;
			depth[to]=depth[u]+1;
			dfs(to);
		}
	}
	void work_tree()
	{
		long long sum=llINF;
		for(int i=1;i<=n;i++)
		{
			memset(depth,0,sizeof(depth));
			memset(fa,0,sizeof(fa));
			depth[i]=1;
			dfs(i);
			long long t=0;
			for(int j=1;j<=m;j++)
			{
				int f;
				if(fa[e[j].x]==e[j].y)
				f=e[j].y;
				else
				f=e[j].x;
				t=t+(depth[f]*e[j].val);
			}
			sum=min(sum,t);
		}
		printf("%lld\n",sum);
	}
}

namespace Bcj
{
	int father[maxn];
	int getfather(int k)
	{
		if(father[k]==k)return k;
		father[k]=getfather(father[k]);
		return father[k];
	}
	void un(int k,int l)
	{
		int fx=getfather(k);
		int fy=getfather(l);
		father[fx]=fy;
	}
	bool judge(int k,int l)
	{
		int fx=getfather(k);
		int fy=getfather(l);
		return fx==fy;
	}
}using namespace Bcj;

namespace Floyd
{
	int dis[maxn][maxn];
	void floyd()
	{
		for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
	}
}using namespace Floyd;

namespace Kruscal
{
	int head_sc[maxn];
	struct sc_tree
	{
		int to,nxt,val;
	}tree[maxn];
	int sc_tot=0;
	struct sc_edge
	{
		int x,y,v;
	}sc_e[maxn];
	int hy=0;
	bool mycmp(edge a,edge b)
	{
		return a.val<b.val;
	}
	void build_tree()
	{
		sort(e+1,e+1+m,mycmp);
		for(int i=1;i<=m;i++)
		{
			int x=e[i].x;
			int y=e[i].y;
			int v=e[i].val;
			if(!judge(x,y))
			{
				hy++;
				sc_e[hy].x=x;
				sc_e[hy].y=y;
				sc_e[hy].v=v;
				dis[x][y]=dis[y][x]=1;
				un(x,y);
				sc_tot++;
				tree[sc_tot].to=y;
				tree[sc_tot].val=v;
				tree[sc_tot].nxt=head_sc[x];
				head_sc[x]=sc_tot;
				
				sc_tot++;
				tree[sc_tot].to=x;
				tree[sc_tot].val=v;
				tree[sc_tot].nxt=head_sc[y];
				head_sc[y]=sc_tot;
			}
		}
	}
}using namespace Kruscal;

namespace mfsf
{
	void work_final()
	{
		long long ans=llINF;
		for(int i=1;i<=n;i++)
		{
			long long t=0;
			for(int j=1;j<=n-1;j++)
			{
				int x=sc_e[j].x;
				int y=sc_e[j].y;
				int v=sc_e[j].v;
				int f;
				if(dis[i][x]<dis[i][y])
				f=x;
				else
				f=y;
				t=t+(dis[i][f]+1)*v;
			}
			ans=min(ans,t);
		}
		printf("%lld\n",ans);
	}
}using namespace mfsf;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	init();
	if(m==n-1)
	{
		using namespace tree_;
		work_tree();
		return 0;
	}
	for(int i=1;i<=n;i++)
	father[i]=i;
	memset(dis,10,sizeof(dis));
	for(int i=1;i<=n;i++)
	dis[i][i]=0;
	build_tree();
	floyd();
	work_final();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

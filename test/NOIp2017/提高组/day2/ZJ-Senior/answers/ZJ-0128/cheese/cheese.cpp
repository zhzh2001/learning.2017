#include<bits/stdc++.h>
#define maxn 1020
using namespace std;
inline int read()
{
	int num=0;
	char c=' ';
	bool flag=true;
	for(;c>'9'||c<'0';c=getchar())
	if(c=='-')flag=false;
	for(;c>='0'&&c<='9';num=num*10+c-48,c=getchar());
	return flag ? num : -num;
}
int n,h,r,T;
int x[maxn],y[maxn],z[maxn];
void init()
{
	n=read();
	h=read();
	r=read();
	for(int i=1;i<=n;i++)
	{
		x[i]=read();
		y[i]=read();
		z[i]=read();
	} 
}
void work1()
{
	int cc=0;
	if(z[1]<=r)cc++;
	if(h-z[1]<=r)cc++;
	if(cc==2)printf("Yes\n");
	else printf("No\n");
}

namespace Bcj
{
	int father[maxn];
	int getfather(int k)
	{
		if(father[k]==k)return k;
		father[k]=getfather(father[k]);
		return father[k];
	}
	void un(int k,int l)
	{
		int fx=getfather(k);
		int fy=getfather(l);
		father[fx]=fy;
	}
	bool judge(int k,int l)
	{
		int fx=getfather(k);
		int fy=getfather(l);
		return fx==fy;
	}
}using namespace Bcj;

namespace mfsf
{
	double dis(int k,int l)
	{
		return sqrt((double)(x[k]-x[l])*(x[k]-x[l]) + (y[k]-y[l])*(y[k]-y[l]) + (z[k]-z[l])*(z[k]-z[l]));
	}
	void work2()
	{
		for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++)
		{
			double d=dis(i,j);
			if(d<=2.0*r)
			un(i,j);
		}
		for(int i=1;i<=n;i++)
		if(z[i]<=r)
		for(int j=1;j<=n;j++)
		if(h-z[j]<=r)
		{
			if(judge(i,j))
			{
				printf("Yes\n");
				return;
			}
		}
		printf("No\n");
	}
}using namespace mfsf;

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--)
	{
		init();
		for(int i=1;i<=n;i++)
		father[i]=i;
		if(n==1)work1();
		else work2();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

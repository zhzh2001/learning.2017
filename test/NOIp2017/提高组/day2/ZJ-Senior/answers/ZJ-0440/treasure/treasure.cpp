#include<ctime>
#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int INF=1000000007;
const int N=15;
int n,m,edgenum;
int miniedge[N][N];
struct PRE_Edge{
	int a,b,v;
}Edge[N*N];
bool use_edge[N*N];
bool mark[N];
struct CALC_Edge{
	int to,v;
};
vector<CALC_Edge>edge[N];
int ans,cnt;
void dfs_tree(int p,int d){
	mark[p]=true;
	for(int i=0;i<edge[p].size();i++){
		int to=edge[p][i].to;
		int v=edge[p][i].v;
		if(!mark[to]){
			cnt+=(d+1)*v;
			dfs_tree(to,d+1);
		}
	}
}
void solve(){
	for(int i=1;i<=n;i++)edge[i].clear();
	for(int i=1;i<=edgenum;i++){
		if(use_edge[i]){
			int a=Edge[i].a;
			int b=Edge[i].b;
			int v=Edge[i].v;
			edge[a].push_back((CALC_Edge){b,v});
			edge[b].push_back((CALC_Edge){a,v});
		}
	}
	for(int i=1;i<=n;i++){
		cnt=0;
		memset(mark,0,sizeof(mark));
		dfs_tree(i,0);
		bool flag=true;
		for(int j=1;j<=n;j++)
			if(!mark[j])flag=false;
		if(flag)ans=min(ans,cnt);
	}
}
void dfs(int num,int L,int R){//ȫ���� 
	int len=R-L+1;
	if(num>len)return;
	if(num==len){
		for(int i=L;i<=R;i++)use_edge[i]=true;
		solve();
		return;
	}
	if(num>0){
		use_edge[L]=true;
		dfs(num-1,L+1,R);
	}
	use_edge[L]=false;
	dfs(num,L+1,R);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			miniedge[i][j]=INF;
	for(int i=1;i<=m;i++){
		int a,b,v;
		scanf("%d%d%d",&a,&b,&v);
		if(a>b)swap(a,b);
		miniedge[a][b]=min(miniedge[a][b],v);
	}
	edgenum=0;
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(miniedge[i][j]!=INF){
				Edge[++edgenum]=(PRE_Edge){i,j,miniedge[i][j]};
			}
		}
	}
	ans=INF;dfs(n-1,1,edgenum);
	printf("%d\n",ans);
	return 0;
}

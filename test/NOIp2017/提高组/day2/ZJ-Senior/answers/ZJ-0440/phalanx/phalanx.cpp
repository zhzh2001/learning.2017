#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;
const int N=1005;
int n,m,q;
int num[N][N];
void solve_1(){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			num[i][j]=(i-1)*m+j;
	for(int i=1;i<=q;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		int val=num[x][y];
		printf("%d\n",val);
		for(int j=y+1;j<=m;j++)
			num[x][j-1]=num[x][j];
		for(int j=x+1;j<=n;j++)
			num[j-1][m]=num[j][m];
		num[n][m]=val;
	}
}
int tot,num2[900005];
struct Segment_Tree{
	struct node{
		int L,R,sum,p;
	}tree[900005<<2];
	void up(int p){
		tree[p].sum=tree[p<<1].sum+tree[p<<1|1].sum;
	}
	void build(int L,int R,int p){
		tree[p].L=L,tree[p].R=R;
		if(L==R){
			if(L<tot)tree[p].sum=1;
			else tree[p].sum=0;
			return;
		}
		int mid=L+R>>1;
		build(L,mid,p<<1);
		build(mid+1,R,p<<1|1);
		up(p);
	}
	void update(int x,int p){
		if(tree[p].L==tree[p].R){
			tree[p].sum=1-tree[p].sum;
			return;
		}
		int mid=tree[p].L+tree[p].R>>1;
		if(x<=mid)update(x,p<<1);
		else update(x,p<<1|1);
		up(p);
	}
	int query(int p,int num){
		int L=tree[p].L,R=tree[p].R;
		if(L==R)return L;
		int mid=L+R>>1;
		int numL=tree[p<<1].sum;
		if(num<=numL)return query(p<<1,num);
		else return query(p<<1|1,num-numL);
	}
}T;
void solve_2(){
	tot=0;
	for(int i=1;i<=m;i++)num2[++tot]=i;
	for(int i=2;i<=n;i++)num2[++tot]=i*m;
	T.build(1,900002,1);
	for(int i=1;i<=q;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		int id=T.query(1,y);
		printf("%d\n",num2[id]);
		num2[++tot]=num2[id];
		num2[id]=0;
		T.update(tot,1);
		T.update(id,1);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<N&&m<N){
		solve_1();
	}else{
		solve_2();
	}
	return 0;
}

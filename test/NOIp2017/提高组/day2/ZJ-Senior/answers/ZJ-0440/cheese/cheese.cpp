#include<math.h>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<algorithm>
using namespace std;
const int N=1005;
typedef long long ll; 
int T,n,h,r;
struct node{
	int x,y,z;
}num[N];
bool edge[N][N];
bool mark[N];
bool judge(int a,int b){
	if(abs(num[a].x-num[b].x)>2*r)return false;
	if(abs(num[a].y-num[b].y)>2*r)return false;
	if(abs(num[a].z-num[b].z)>2*r)return false;
	ll dis=1LL*(num[a].x-num[b].x)*(num[a].x-num[b].x)
			+1LL*(num[a].y-num[b].y)*(num[a].y-num[b].y)
			+1LL*(num[a].z-num[b].z)*(num[a].z-num[b].z);
	ll res=4LL*r*r;
	if(dis<=res)return true;
	return false;
}
bool check_top(int p){
	if(num[p].z+r>=h)return true;
	return false;
}
bool check_low(int p){
	if(num[p].z<=r)return true;
	return false;
}
void dfs(int p){
	mark[p]=1;
	for(int i=1;i<=n;i++)
		if(edge[p][i]&&!mark[i])dfs(i);
}
bool solve(){
	for(int i=0;i<N;i++)
		num[i].x=num[i].y=num[i].z=0;
	memset(edge,0,sizeof(edge));
	memset(mark,0,sizeof(mark));
	scanf("%d%d%d",&n,&h,&r);
	for(int i=1;i<=n;i++)
		scanf("%d%d%d",&num[i].x,&num[i].y,&num[i].z);
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(judge(i,j)){
				edge[i][j]=1;
				edge[j][i]=1;
			}
		}
	}
	for(int i=1;i<=n;i++)
		if(check_low(i)&&!mark[i])dfs(i);
	for(int i=1;i<=n;i++)
		if(mark[i]&&check_top(i))return true;
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		if(solve())puts("Yes");
		else puts("No");
	}
	return 0;
}

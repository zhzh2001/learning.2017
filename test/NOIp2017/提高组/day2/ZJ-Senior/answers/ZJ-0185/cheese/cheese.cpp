#include<iostream>
#include<cstdio>
#include<algorithm>
#define N 11005
using namespace std;
#define ll long long
struct note{
	ll x,y,z;
}point[N];
ll sqr(ll x)
{
	return x*x;
}
ll n,h,r;
ll rep[N];
ll getrep(ll x)
{
	if(rep[x]==x) return x;
	return rep[x]=getrep(rep[x]);
}
void Union(ll x,ll y)
{	
	rep[getrep(x)]=getrep(y);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	ll cas;
	scanf("%lld",&cas);
	while(cas--)
	{
		scanf("%lld %lld %lld",&n,&h,&r);
		ll S=n+1,T=n+2;
		for(ll i=1;i<=n+2;i++) rep[i]=i;
		for(ll i=1;i<=n;i++)
		{
			scanf("%lld %lld %lld",&point[i].x,&point[i].y,&point[i].z);
		}
		for(ll i=1;i<=n;i++)
		{
			if(point[i].z-r<=0) Union(S,i);
			if(point[i].z+r>=h) Union(i,T);
		}
		for(ll i=1;i<=n;i++)
			for(ll j=i+1;j<=n;j++)
			if(getrep(i)!=getrep(j))
			{
				if(sqr(point[i].x-point[j].x)+sqr(point[i].y-point[j].y)+sqr(point[i].z-point[j].z)<=r*r*4ll)
				Union(i,j);
			}	
		if(getrep(S)==getrep(T))
		{
			puts("Yes");
		}else puts("No");	
	}
}

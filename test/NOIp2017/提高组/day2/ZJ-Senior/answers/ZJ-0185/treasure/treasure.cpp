#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cstdlib>
#define maxn 1005
#define ll long long
using namespace std;
ll tot=0,ans;
ll head[maxn],next[maxn<<1],val[maxn<<1],to[maxn<<1];
ll map[maxn][maxn];
bool vis[maxn];
ll cnt,n,m,num;
ll Inf;
struct note{
	ll u,v,c;
	friend bool operator<(note a,note b)
	{
		return a.c<b.c;
	}
}edge[maxn];
void dfs(ll u,ll len)
{
	vis[u]=1;
	
	for(ll edge=head[u];edge;edge=next[edge])
	if(!vis[to[edge]]) 
	{
		tot+=len*val[edge];
		dfs(to[edge],len+1);
	}
}
void add(ll u,ll v,ll c)
{
	to[++num]=v;
	val[num]=c;
	next[num]=head[u];
	head[u]=num;
}
void clear_edgeset()
{
	memset(head,0,sizeof(head));
	num=0;
}
int rep[maxn];
void clear_rep()
{
	for(int i=1;i<=n;i++)
	rep[i]=i;
}
int getrep(int x)
{
	if(x==rep[x]) return x;
	return rep[x]=getrep(rep[x]);
}
void Union(int x,int y)
{
	rep[x]=y;
}
void solve()
{
	for(ll i=1;i<=m;i++)
	{
		ll u,v,c;
		scanf("%lld %lld %lld",&u,&v,&c);
		map[u][v]=min(map[u][v],c);
		map[v][u]=min(map[v][u],c);
	}
	for(ll i=1;i<=n;i++)
		for(ll j=i+1;j<=n;j++)
		if(map[i][j]!=Inf) 
		{
			edge[++cnt]=(note){i,j,map[i][j]};
		}
	for(ll i=1;i<=cnt;i++)	
	{
		add(edge[i].u,edge[i].v,edge[i].c);
		add(edge[i].v,edge[i].u,edge[i].c);
	}
	if(n==m+1)
	{
		for(ll i=1;i<=n;i++)
		{
			memset(vis,0,sizeof(vis));
			tot=0;
			vis[i]=1;
			dfs(i,1);
			ans=min(ans,tot);
		}
	}else
	{
		
		int t=0;
		while(t<=200005)
		{
			clear_edgeset();
			clear_rep();
			for(int i=1;i<n;i++)
			{
				int tmp=rand()%cnt+1;
				if(getrep(edge[tmp].u)!=getrep(edge[tmp].v)) 
				{
					add(edge[tmp].u,edge[tmp].v,edge[tmp].c);
					add(edge[tmp].v,edge[tmp].u,edge[tmp].c);
					Union(getrep(edge[tmp].u),getrep(edge[tmp].v));
				}else i--;
			}
			for(ll i=1;i<=n;i++)
			{
				memset(vis,0,sizeof(vis));
				tot=0;
				vis[i]=1;
				dfs(i,1);
				ans=min(ans,tot);
			}
			t++;
		}
	}
	printf("%lld\n",ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	srand(123456);
	ans=0x7FFFFFFF;
	memset(map,127,sizeof(map));
	Inf=map[1][1];
	scanf("%lld %lld",&n,&m);
	solve();
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<vector>
#define ll long long
using namespace std;
ll n,m,q,map[1005][1005];
vector<ll> vec;
void solve1()
{
	for(ll i=1;i<=n;i++)
		for(ll j=1;j<=m;j++)
		map[i][j]=((i-1)*m+j);
	while(q--)
	{
		ll x,y,pos;
		scanf("%lld %lld",&x,&y);
		printf("%lld\n",pos=map[x][y]);
		for(ll i=y;i<=m-1;i++) map[x][i]=map[x][i+1];
		for(ll i=x;i<=n-1;i++) map[i][m]=map[i+1][m];
		map[n][m]=pos;
	}	
}
void solve2()
{
	for(ll i=1;i<=m;i++)
	vec.push_back(i);
	while(q--)
	{
		ll x,y,pos;
		scanf("%lld %lld",&x,&y);
		printf("%lld\n",pos=vec[y-1]);
		vec.erase(vec.begin()+y-1,vec.begin()+y);
		vec.push_back(pos);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%lld %lld %lld",&n,&m,&q);
	if(n==1) solve2();else solve1();
}

program cheese;
 var
  f,b,x,y,z:array[0..1001] of longint;
  h,i,i1,j,m,n,o,p,q,t,u:longint;
 begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i1:=1 to t do
   begin
    fillchar(f,sizeof(f),0);
    readln(n,h,q);
    p:=0;
    for i:=1 to n do
     begin
      readln(x[i],y[i],z[i]);
      if z[i]-q<=0 then
       begin
        inc(p);
        b[p]:=i;
        f[i]:=1;
       end;
     end;
    o:=0;
    u:=0;
    repeat
     inc(o);
     m:=b[o];
     for j:=1 to n do
      if (f[j]=0) and (sqrt(sqr(x[m]-x[j])+sqr(y[m]-y[j])+sqr(z[m]-z[j]))<=2*q) then
       begin
        inc(p);
        b[p]:=j;
        f[j]:=1;
        if z[j]+q>=h then u:=1;
       end;
    until o=p;
    if u=1 then writeln('Yes')
           else writeln('No');
   end;
  close(input);
  close(output);
 end.

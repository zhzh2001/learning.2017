#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxp=1005;
int t,n,h,r;
struct point
{
	int x,y,z;
}p[maxp];
void read(int &x)
{
	x=0;char ch=getchar();bool flag=0;
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') flag=1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	if(flag) x=-x;
}
double dist(point p1,point p2)
{
	return sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z));
}
bool pcomp(point p1,point p2)
{
	return p1.z<p2.z;
}
bool vis[maxp],end;
void dfs(int x)
{
	register int i;
	if(end) return;
	if(p[x].z+r>=h) end=1;
	else
	{
		vis[x]=1;
		for(i=x+1;p[i].z<=p[x].z+2*r;i++)
		{
			if(dist(p[i],p[x])<=2*r&&!vis[i])
			{
				dfs(i);
			}
		}
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	register int i,j;
	read(t);
	for(i=1;i<=t;i++)
	{
		read(n);read(h);read(r);
		for(j=1;j<=n;j++)
		{
			read(p[j].x);read(p[j].y);read(p[j].z);
		}
		sort(p+1,p+n+1,pcomp);
		if(p[0].z>r)
		{
			printf("No\n");
		}
		else
		{
			j=1;end=0;
			memset(vis,0,sizeof(vis));
			while(p[j].z<=r&&!end)
			{
				dfs(j);
				j++;
			}
			if(end) printf("Yes\n");
			else printf("No\n");
		}
	}
	return 0;
}

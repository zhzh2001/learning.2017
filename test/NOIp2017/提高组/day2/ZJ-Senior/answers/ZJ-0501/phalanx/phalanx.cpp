#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxq=300005;
void read(int &x)
{
	x=0;char ch=getchar();bool flag=0;
	while(ch<'0'||ch>'9')
	{
		if(ch=='-') flag=1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	if(flag) x=-x;
}
int n,m,q;
int x[maxq],y[maxq];
int nx[maxq],ny[maxq],p[maxq];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	register int i,j;
	int t;
	int mode;
	read(n);read(m);read(q);
	for(i=1;i<=q;i++)
	{
		read(x[i]);read(y[i]);
	}
	for(i=1;i<=q;i++)
	{
		t=0;mode=0;
		for(j=1;j<=i-1;j++)
		{
			if((x[i]>=x[j]&&y[i]==m))
			{
				t+=m;
			}
			else if((x[i]==x[j]&&y[i]>=y[j]))
			{
				t++;
			}
			if(x[i]==nx[j]&&y[i]==ny[j])
			{
				mode=j;
				break;
			}
		}
		if(mode)
		{
			printf("%d\n",p[mode]);p[i]=p[mode];
		} 
		else
		{
			printf("%d\n",(x[i]-1)*m+y[i]+t);p[i]=(x[i]-1)*m+y[i]+t;
		} 
		nx[i]=n;ny[i]=m;
		for(j=1;j<=i-1;j++)
		{
			if(j==mode)
			{
				nx[mode]=n;
				ny[mode]=m;
			}
			else
			{
				if((nx[j]==x[i]&&ny[j]>=y[i]))
				{
					ny[j]--;
				}
				else if((nx[j]>=x[i]&&ny[j]==m))
				{
					nx[j]--;
				}
			}
		}
	}
	return 0;
}

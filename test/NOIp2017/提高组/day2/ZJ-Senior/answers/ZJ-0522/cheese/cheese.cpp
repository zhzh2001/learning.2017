#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn=1015;
int _test,n,fa[maxn];
LL H,R,a[maxn],b[maxn],c[maxn];
int getfa(int x){ return fa[x]==x?x:fa[x]=getfa(fa[x]); }
void merge(int x,int y){ x=getfa(x); y=getfa(y); if(x!=y) fa[x]=y; }
bool check(int x,int y){
	if((double)(a[x]-a[y])*(a[x]-a[y])+(double)(b[x]-b[y])*(b[x]-b[y])+(double)(c[x]-c[y])*(c[x]-c[y])>4e18+5) return false;
	return (a[x]-a[y])*(a[x]-a[y])+(b[x]-b[y])*(b[x]-b[y])+(c[x]-c[y])*(c[x]-c[y])<=R*R*4;
}
int _abs(int x){ return x<0?-x:x; }
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&_test);
	while(_test--){
		scanf("%d%lld%lld",&n,&H,&R);
		for(int i=1;i<=n;i++) scanf("%lld%lld%lld",&a[i],&b[i],&c[i]);
		for(int i=1;i<=n+2;i++) fa[i]=i;
		for(int i=1;i<=n;i++){
			if(_abs(c[i])<=R) merge(i,n+1);
			if(_abs(H-c[i])<=R) merge(i,n+2);
		}
		for(int i=1;i<=n;i++)
		 for(int j=i+1;j<=n;j++) if(check(i,j)) merge(i,j);
		if(getfa(n+1)==getfa(n+2)) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

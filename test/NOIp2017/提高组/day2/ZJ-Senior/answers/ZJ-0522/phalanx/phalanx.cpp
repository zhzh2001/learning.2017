#include<cstdio>
#include<cctype>
#include<cstdlib>
#include<algorithm>
using namespace std;
typedef long long LL;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int getint(){
	char ch=gc(); int res=0;
	while(!isdigit(ch)) ch=gc();
	while(isdigit(ch)) res=(res<<3)+(res<<1)+ch-'0', ch=gc();
	return res;
}
const int maxn=500005,HMOD=10000007,max_nq=25050005;
int n,m,Q;
LL rdm[maxn];
inline int getF(int x,int y){ return (rdm[x]*51+rdm[y])%HMOD; }
unsigned int Hshtab[HMOD+5],nxt[max_nq],H_x[max_nq],H_y[max_nq],val[max_nq],tot;
inline int hsh_find(unsigned int x,unsigned int y){
	int _F=getF(x,y);
	for(int j=Hshtab[_F];j;j=nxt[j]) if(H_x[j]==x&&H_y[j]==y) return j;
	H_x[++tot]=x; H_y[tot]=y; val[tot]=(x-1)*((unsigned int)m)+y;
	nxt[tot]=Hshtab[_F]; Hshtab[_F]=tot;
	return tot;
}
void Solve1(){
	srand(2333);
	for(int i=1;i<=500000;i++) rdm[i]=(((LL)rand())<<14)+rand();
	while(Q--){
		int x=getint(),y=getint();
		int res=val[hsh_find(x,y)]; 
		for(int i=y+1;i<=m;i++) val[hsh_find(x,i-1)]=val[hsh_find(x,i)];
		for(int i=x+1;i<=n;i++) val[hsh_find(i-1,m)]=val[hsh_find(i,m)];
		val[hsh_find(n,m)]=res;
		printf("%d\n",res);
	}
}
void Updata(int x,int y){
	for(;x<=m+Q;x+=(x&(-x))) H_x[x]+=y;
}
int Query(int x){
	int res=0;
	for(;x;x-=(x&(-x))) res+=H_x[x];
	return res;
}
void Solve2(){
	for(int i=1;i<=m;i++) Updata(i,1), val[i]=i; 
	int now_len=m;
	for(int ii=1;ii<=Q;ii++){
		int x=getint(),y=getint();
		int L=1,R=m+Q,pos;
		while(L<=R){
			int mid=(L+R)>>1;
			if(Query(mid)>=y) pos=mid, R=mid-1; else L=mid+1;
		}
		printf("%d\n",val[pos]);
		Updata(pos,-1); val[++now_len]=pos; Updata(now_len,1); 
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=getint(); m=getint(); Q=getint();
	if((LL)m*Q+n<25050005){
		Solve1();
		return 0;
	}
	Solve2();
	return 0;
}

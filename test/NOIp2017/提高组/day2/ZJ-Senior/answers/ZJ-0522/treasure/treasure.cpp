#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
int n,m,f[14][531445],w[15][15],INF,ans,pw[20],w_m[14][(1<<12)+5];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	pw[0]=1; for(int i=1;i<=15;i++) pw[i]=pw[i-1]*3;
	scanf("%d%d",&n,&m);
	memset(w,63,sizeof(w)); INF=w[0][0];
	for(int i=1;i<=m;i++){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		w[x][y]=min(w[x][y],z); w[y][x]=w[x][y];
	}
	memset(w_m,63,sizeof(w_m));
	for(int i=1;i<=n;i++)
	 for(int s=0;s<=(1<<n)-1;s++)
	  for(int j=1;j<=n;j++) if((s>>j-1)&1) w_m[i][s]=min(w_m[i][s],w[i][j]); 
	memset(f,63,sizeof(f)); 
	for(int i=1;i<=n;i++) f[1][2*pw[i-1]]=0;
	for(int i=1;i<=n-1;i++)
	 for(int s=0;s<=pw[n]-1;s++) if(f[i][s]<INF){
	 	int now_s=0; for(int j=1;j<=n;j++) if((s/pw[j-1])%3==2) now_s|=(1<<j-1);
	 	for(int k=1;k<=n;k++) if((s/pw[k-1])%3==0)
	 	 if(w_m[k][now_s]<INF) f[i][s+pw[k-1]]=min(f[i][s+pw[k-1]],f[i][s]+w_m[k][now_s]*i);
	 	int _t=s; for(int j=1;j<=n;j++) if((_t/pw[j-1])%3==1) _t+=pw[j-1];
	 	f[i+1][_t]=min(f[i+1][_t],f[i][s]);	 
	 }
	ans=INF;
	for(int i=1;i<=n-1;i++) ans=min(ans,f[i][pw[n]-1]);
	printf("%d\n",ans);
	return 0;
}

var
 i,j,ti:longint;
 t,n,h,r,distr,max,min,ans,blank:int64;
 f:array[0..1010]of longint;
 a:array[0..1010,0..2]of longint;
 b:array[0..1010]of longint;
 procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2,2];
         repeat
           while a[i,2]<x do
            inc(i);
           while x<a[j,2] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i,0];
                a[i,0]:=a[j,0];
                a[j,0]:=y;
                y:=a[i,1];
                a[i,1]:=a[j,1];
                a[j,1]:=y;
                y:=a[i,2];
                a[i,2]:=a[j,2];
                a[j,2]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
function dist(x,y:longint):longint;
 begin
  blank:=(a[x,0]-a[y,0])*(a[x,0]-a[y,0])+(a[x,1]-a[y,1])*(a[x,1]-a[y,1])+(a[x,2]-a[y,2])*(a[x,2]-a[y,2]);
  exit(blank);
 end;
function ff(x:longint):longint;
 begin
  if f[x]=x then exit(x)
            else begin
                 f[x]:=ff(f[x]);
                 exit(f[x]);
                 end;
 end;
begin
 assign(input,'cheese.in');
 assign(output,'cheese.out');
 reset(input);
 rewrite(output);
 read(t);
 for ti:=1 to t do
 begin
  read(n,h,r);
  fillchar(f,sizeof(f),0);
  fillchar(a,sizeof(a),0);
  fillchar(b,sizeof(b),0);
  distr:=4*r*r;{max,min}
  for i:=1 to n do
   read(a[i,0],a[i,1],a[i,2]);
  sort(1,n);
  a[0,2]:=-1;
  for i:=n downto 0 do
   if a[i,2]<=r then begin
                     max:=i;
                     break;
                     end;
  for i:=1 to n do
   f[i]:=i;
  a[n+1,2]:=maxlongint;
  for i:=1 to n+1 do
   if a[i,2]>=(h-r) then begin
                         min:=i;
                         break;
                         end;
  for i:=max+1 to n do
  begin
   j:=i-1;
   while (j>0)and((a[i,2]-a[j,2])<=2*r) do
   begin
    if dist(i,j)<=distr then begin
                             if a[ff(i),2]>a[ff(j),2] then f[ff(i)]:=ff(j)
                                                      else f[ff(j)]:=ff(i);
                             end;
    dec(j);
   end;
  end;
  ans:=0;
  for i:=min to n do
   if f[i]<=max then begin
                     ans:=1;
                     break;
                     end;
  if ans=0 then writeln('No')
           else writeln('Yes');
 end;
 close(input);
 close(output);
end.



#include<cstdio>
#include<cstring>
const int maxn=1e4+5;
const int maxm=6e5+5;
int map[maxn][maxn],line[maxm],cf2[30005],n,m,q;
int lowbit(int x){return x&(-x);}
int change(int *c,int l,int p,int dx){
	while (p<=l) {
		c[p]+=dx;
		p+=lowbit(p);
	}
	return 0;
}
int getsum(int *c,int l,int p){
	int sum=0;
	while (p>0){
		sum+=c[p];
		p-=lowbit(p);
	}
	return sum;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	int qt=q;
	if (n==1){
		for (int i=1;i<=m;i++) line[i]=i;
		for (int x,y,t;q>0;q--){
			scanf("%d%d",&x,&y);
			t=line[y];
			printf("%d\n",t);
			for (int i=y;i<=m;i++) line[i]=line[i+1];
			line[m]=t;
		}
	}
	else{
		for (int i=1;i<=n;i++) for (int j=1;j<=n;j++) map[i][j]=(i-1)*m+j;
		for (int x,y,t;q>0;q--){
			scanf("%d%d",&x,&y);
			t=map[x][y];
			printf("%d\n",t);
			for (int i=y;i<m;i++) map[x][i]=map[x][i+1];
			for (int i=x;i<n;i++) map[i][m]=map[i+1][m];
			map[n][m]=t;
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;
const int maxn=1005;
int T,n,x[maxn],y[maxn],z[maxn],h,r,re[1000],pbegin=0,pend=0;
bool visit[maxn],able;
bool can(int a,int b,int r2){
	return ((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b])<=r2);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	for (;T>0;T--){
		able=false;
		memset(re,0,sizeof(re));
		memset(visit,0,sizeof(visit));
		scanf("%d%d%d",&n,&h,&r);
		for (int i=0;i<n;i++){
			scanf("%d%d%d",x+i,y+i,z+i);
			if (z[i]<=r) {visit[i]=true; re[pend]=i; pend++;}
		}
		for (pbegin=0;pbegin<pend;pbegin++)
			for (int j=0;j<n;j++){
				if (visit[j]) continue;
				if (can(re[pbegin],j,r*r*4)) {
					re[pend]=j;
					visit[j]=true;
					pend++;
					if (z[j]+r>=h) {able=true; pend=0; break;}
				}
			}
		if (able) printf("Yes\n"); else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

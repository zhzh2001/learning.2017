#include<cstdio>
#include<cstring>
const int INF=5e5+5;
int n,m,map[20][20],ans=0x7fffffff;
int min(int a,int b);
int min(int a,int b){return (a<b)?a:b;}
int qsort(int *a,int *b,int *c,int l,int r);
int qsort(int *a,int *b,int *c,int l,int r){
	int i=l,j=r,m=c[i],t;
	while (i<=j){
		while (c[i]<m) i++;
		while (c[j]>m) j--;
		if (i<=j){
			t=a[i]; a[i]=a[j]; a[j]=t;
			t=b[i]; b[i]=b[j]; b[j]=t;
			t=c[i]; c[i]=c[j]; c[j]=t;
			i++; j--;
		}
	}
	if (j>l) qsort(a,b,c,l,j);
	if (i<r) qsort(a,b,c,i,r);
	return 0;
}
int find(int start){
	int sum=0,pbegin=0,pend=0;
	bool visit[15];
	int depth[15],dis[10005],a[10005],b[10005];
	memset(visit,0,sizeof(visit));
	memset(depth,0,sizeof(depth));
	memset(a,0,sizeof(a));
	memset(b,0,sizeof(b));
	memset(dis,0,sizeof(dis));
	visit[start]=true;
	for (int j=1;j<=n;j++){
		if (map[start][j]==INF) continue;
		a[pend]=start; b[pend]=j; dis[pend]=map[start][j]; pend++;
	}
	for (int i=2;i<=n;i++){
		qsort(a,b,dis,pbegin,pend-1);
		while(!(visit[a[pbegin]]&&(!visit[b[pbegin]]))) pbegin++;
		sum+=dis[pbegin];
		int p=b[pbegin];
		visit[p]=true;
		depth[p]=depth[a[pbegin]]+1;
		for (int j=1;j<=n;j++){
			if (visit[j]||map[p][j]==INF) continue;
			a[pend]=p; b[pend]=j; dis[pend]=(depth[p]+1)*map[p][j]; pend++;
		}
	}
	return sum;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	for (int i=0;i<20;i++) for (int j=0;j<20;j++) map[i][j]=INF;
	scanf("%d %d",&n,&m);
	for (int a,b,v,i=0;i<m;i++){
		scanf("%d%d%d",&a,&b,&v);
		map[a][b]=min(map[a][b],v);
		map[b][a]=map[a][b];
	}
	for (int t,i=1;i<=n;i++)
		ans=min(ans,find(i));
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

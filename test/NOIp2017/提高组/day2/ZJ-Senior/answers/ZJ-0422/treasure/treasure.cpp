#include <cstdio>
#include <algorithm>
using namespace std;
long long Zero[4500][15],dp[15][4500],ini[4500][15],n,m,ans,Map[20][20];
void dfs(int i,int j,int sta,int t,long long sum)
{
	if (t>Zero[j][0])
	{
		dp[i+1][sta]=min(dp[i+1][sta],dp[i][j]+sum*i);
		return;
	}
	dfs(i,j,sta,t+1,sum);
	dfs(i,j,sta|(1<<(Zero[j][t]-1)),t+1,sum+ini[j][t]);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
			Map[i][j]=Map[j][i]=1e9;
	for (int i=1;i<=m;i++)
	{
		int u,v;
		long long c;
		scanf("%d%d%lld",&u,&v,&c);
		Map[u][v]=Map[v][u]=min(Map[u][v],c);
	}
	for (int j=0;j<(1<<n);j++)
		for (int k=1;k<=n;k++)
			if ((j&(1<<(k-1)))==0) Zero[j][++Zero[j][0]]=k;
	for (int j=0;j<(1<<n);j++)
		for (int k=1;k<=n;k++)
			ini[j][k]=1e9;
	for (int j=0;j<(1<<n);j++)
		for (int k=1;k<=Zero[j][0];k++)
			for (int l=1;l<=n;l++)
				if ((j&(1<<(l-1)))!=0)
					ini[j][k]=min(ini[j][k],Map[Zero[j][k]][l]);
	for (int i=1;i<=n;i++)
		for (int j=0;j<(1<<n);j++)
			dp[i][j]=1e9;
	for (int i=1;i<=n;i++)
		dp[1][1<<(i-1)]=0;
	for (int i=1;i<=n;i++)
		for (int j=0;j<(1<<n);j++)
		{
			if (dp[i][j]==1e9) continue;
			dfs(i,j,j,1,0);
		}
	ans=1e9;
	for (int i=1;i<=n;i++) ans=min(ans,dp[i][(1<<n)-1]);
	printf("%lld\n",ans);
	return 0;
}

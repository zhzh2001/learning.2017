#include <cstdio>
#include <cstring>
int val[1500010],cnt,next[1500010],head[1050],q[1050],T,n,m;
long long h,r,x[1050],y[1050],z[1050];
bool vis[1050];
long long abs(long long x)
{
	return (x<0)?-x:x;
}
void add(int a,int b)
{
	val[++cnt]=b;
	next[cnt]=head[a];
	head[a]=cnt;
}
void bfs()
{
	int hd=1,tl=1;
	q[1]=n+1;
	vis[n+1]=true;
	while (hd<=tl)
	{
		int u=q[hd++];
		for (int i=head[u];i;i=next[i])
		{
			int v=val[i];
			if (!vis[v])
			{
				vis[v]=true;
				q[++tl]=v;
			}
		}
	}
	if (!vis[n+2]) printf("No\n");
	else printf("Yes\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		cnt=0;
		memset(head,0,sizeof(head));
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			{
				long long now=2LL*r*2LL*r;
				if (now-(x[i]-x[j])*(x[i]-x[j])<0) continue;
				now-=(x[i]-x[j])*(x[i]-x[j]);
				if (now-(y[i]-y[j])*(y[i]-y[j])<0) continue;
				now-=(y[i]-y[j])*(y[i]-y[j]);
				if (now-(z[i]-z[j])*(z[i]-z[j])<0) continue;
				add(i,j),add(j,i);
			}
		for (int i=1;i<=n;i++)
			if (abs(z[i])<=r) add(n+1,i);
		for (int i=1;i<=n;i++)
			if (abs(h-z[i])<=r) add(i,n+2);
		for (int i=1;i<=n+2;i++) vis[i]=false;
		bfs();
	}
	return 0;
}

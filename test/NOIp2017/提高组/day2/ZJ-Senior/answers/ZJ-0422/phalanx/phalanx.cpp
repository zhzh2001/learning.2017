#include <cstdio>
#include <cmath>
#include <queue>
#include <algorithm>
using namespace std;
deque<long long> que[600],que1[505][230];
int q,num,all,blo[600],opx[510],opy[510],pos[300010],size,num1,all1,blo1[600],wh[600];
long long n,m,tmp[600];
bool vis[300010];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%lld%lld%d",&n,&m,&q);
	if (n==1)
	{
		num=floor(sqrt(m));
		for (int i=1;;i++)
		{
			int st=(i-1)*num+1;
			int ed=min(m,(long long)i*num);
			if (st>ed) break;
			all=i;
			for (int j=st;j<=ed;j++)
				que[i].push_back(j),blo[j]=i;
		}
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			int BLO=blo[y];
			int st=(BLO-1)*num+1;
			for (int i=1;i<=y-st;i++)
				tmp[i]=que[BLO].front(),que[BLO].pop_front();
			long long ans=que[BLO].front();
			que[BLO].pop_front();
			printf("%lld\n",ans);
			for (int i=y-st;i>=1;i--)
				que[BLO].push_front(tmp[i]);
			for (int i=BLO;i<all;i++)
			{
				que[i].push_back(que[i+1].front());
				que[i+1].pop_front();
			}
			que[all].push_back(ans);
		}
		return 0;
	}
	if (q<=500)
	{
		for (int i=1;i<=q;i++)
			scanf("%d%d",&opx[i],&opy[i]),vis[opx[i]]=true;
		num=floor(sqrt(m));
		for (int lin=1;lin<=n;lin++)
			if (vis[lin])
			{
				pos[lin]=++size;
				wh[size]=lin;
				for (int i=1;;i++)
				{
					int st=(i-1)*num+1;
					int ed=min(m,(long long)i*num);
					if (st>ed) break;
					all=i;
					for (int j=st;j<=ed;j++)
						que1[pos[lin]][i].push_back((long long)(lin-1)*m+j),blo[j]=i;
				}
			}
		num1=floor(sqrt(n));
		for (int i=1;;i++)
		{
			int st=(i-1)*num1+1;
			int ed=min(n,(long long)i*num1);
			if (st>ed) break;
			all1=i;
			for (int j=st;j<=ed;j++)
				que[i].push_back(j*m),blo1[j]=i;
		}
		for (int cas=1;cas<=q;cas++)
		{
			int POS=pos[opx[cas]];
			int BLO=blo[opy[cas]];
			int st=(BLO-1)*num+1;
			for (int i=1;i<=opy[cas]-st;i++)
				tmp[i]=que1[POS][BLO].front(),que1[POS][BLO].pop_front();
			long long ans=que1[POS][BLO].front();
			que1[POS][BLO].pop_front();
			printf("%lld\n",ans);
			for (int i=opy[cas]-st;i>=1;i--)
				que1[POS][BLO].push_front(tmp[i]);
			for (int i=BLO;i<all;i++)
			{
				que1[POS][i].push_back(que1[POS][i+1].front());
				que1[POS][i+1].pop_front();
			}
			que1[POS][all].push_back(ans);
			BLO=blo1[opx[cas]];
			st=(BLO-1)*num1+1;
			for (int i=1;i<=opx[cas]-st;i++)
				tmp[i]=que[BLO].front(),que[BLO].pop_front();
			que[BLO].pop_front();
			for (int i=opx[cas]-st;i>=1;i--)
				que[BLO].push_front(tmp[i]);
			for (int i=BLO;i<all1;i++)
			{
				que[i].push_back(que[i+1].front());
				que[i+1].pop_front();
			}
			que[all1].push_back(ans);
			for (int i=1;i<=size;i++)
			{
				if (wh[i]<opx[cas]) continue;
				BLO=blo1[wh[i]];
				int st=(BLO-1)*num1+1;
				int ed=min(m,(long long)BLO*num1);
				if (wh[i]-st<=ed-wh[i])
				{
					for (int j=1;j<=wh[i]-st;j++)
						tmp[j]=que[BLO].front(),que[BLO].pop_front();
					que1[pos[wh[i]]][all].pop_back();
					que1[pos[wh[i]]][all].push_back(que[BLO].front());
					for (int j=wh[i]-st;j>=1;j--)
						que[BLO].push_front(tmp[j]);
				}
				else
				{
					for (int j=1;j<=ed-wh[i];j++)
						tmp[j]=que[BLO].back(),que[BLO].pop_back();
					que1[pos[wh[i]]][all].pop_back();
					que1[pos[wh[i]]][all].push_back(que[BLO].back());
					for (int j=ed-wh[i];j>=1;j--)
						que[BLO].push_back(tmp[j]);
				}
			}
		}
		return 0;
	}
	return 0;
}

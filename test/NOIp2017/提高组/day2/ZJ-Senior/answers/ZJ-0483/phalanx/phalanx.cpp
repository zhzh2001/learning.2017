#include <cstdio>

int matrix[1001][1001];

void print_matrix(int n, int m) {
	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			fprintf(stderr, "%d ", matrix[i][j]);
		}
		fprintf(stderr, "\n");
	}

}

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	int n, m, q;
	scanf("%d%d%d", &n, &m, &q);

	for (int i = 1; i <= n; ++i) {
		for (int j = 1; j <= m; ++j) {
			matrix[i][j] = (i - 1) * m + j;
		}
	}
	
//	print_matrix(n, m);
	
	for (int i = 0; i < q; ++i) {
		int x, y;
		scanf("%d%d", &x, &y);
		int t = matrix[x][y];


		for (int j = y + 1; j <= m; ++j) {
			matrix[x][j - 1] = matrix[x][j];
		}


		for (int j = x + 1; j <= n; ++j) {
			matrix[j - 1][n] = matrix[j][n];
		}

		
		matrix[n][m] = t;
//		print_matrix(n, m);
		printf("%d\n", t);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

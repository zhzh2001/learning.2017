#include <cstdio>
#include <cstring>
#include <cmath>
#include <queue>
#include <algorithm>
#include <stdint.h>

using std::queue;
using std::min;

#define nullptr NULL
#define N 13

struct Node {
	Node(){}
	Node(int _dst, int _len, Node* _next = nullptr) {
		dst = _dst; next = _next; len = _len;
	}
	int dst;
	Node *next;
	int len;
};

struct NodeIndex {
	Node *head;
	Node *tail;
};

NodeIndex node_index[N];

void add_edge(int src, int dst, int len) {
	if (node_index[src].head == nullptr) {
		node_index[src].head = node_index[src].tail = new Node(dst, len);
	}
	else {
		node_index[src].tail = node_index[src].tail->next = new Node(dst, len);
	}
}

void clean_edge(int n) {
	for (int i = 0; i < n; ++i) {
		Node *p = node_index[i].head;

		while (p != nullptr) {
			Node *tp = p->next;
			delete p;
			p = tp;
		}
	}
}

uint16_t all_visited;

struct BfsState {
	BfsState(){}
	BfsState(int _value, uint16_t _visited, uint32_t _all_depth) {
		value = _value;
		visited = _visited;
		all_depth = _all_depth;
	}
	
	int value;
	uint16_t visited;
	uint32_t all_depth;
};

BfsState cache[(1 << (N + 1))];

void bfs(int src, int n, int &vmin) {
	queue<BfsState> q;

	memset(cache, 0, sizeof(cache));
	
	q.push(BfsState(0, 1 << src, 0));
	
	while (!q.empty()) {
		BfsState cur_state = q.front();
		
			register Node *p;
			int t_depth, cur_value, ii, t;
			unsigned long p_dst_mask;
			uint16_t new_visited;
			for (int i = 1; i <= n; ++i) {
				if (!(cur_state.visited & (1 << i))) continue;
				p = node_index[i].head;
				ii = i - 1;
				while (p != nullptr) {
					t_depth =  (cur_state.all_depth >> (ii * 4)) & 0b1111;
					cur_value = cur_state.value + (t_depth + 1) * p->len;
					p_dst_mask = 1 << p->dst;
					new_visited = cur_state.visited | p_dst_mask;
					

					if (!(p_dst_mask & cur_state.visited) && cur_value < vmin) {

//					if (cache[new_visited].value != 0) {
//						q.push(cache[new_visited]);
//					}
//					else {
						t = p->dst - 1;
						if (new_visited == all_visited)
							vmin = min(vmin, cur_value);
						else
							q.push(BfsState(cur_value, new_visited, cur_state.all_depth & (~(0b1111 << (t * 4))) | ((t_depth + 1) << (t * 4))));
//							q.push(cache[new_visited] = BfsState(cur_value, new_visited, cur_state.all_depth & (~(0b1111 << (t * 4))) | ((t_depth + 1) << (t * 4))));
					}
					p = p->next;
				}
			}
		
		q.pop();
	}
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	
	int n, m;
	scanf("%d%d", &n, &m);
	
	int len[N][N];
	memset(len, 0x3f, sizeof(len));
	
	for (int i = 0; i < m; ++i) {
		int src, dst, v;
		scanf("%d%d%d", &src, &dst, &v);
		len[src][dst] = len[dst][src] = min(len[dst][src], v);
	}
	int real_edge_num = 0;
	for (int i = 1; i <= n; ++i) {
		for (int j = i + 1; j <= n; ++j) {
			if (len[i][j] != 0x3f3f3f3f) {
				++real_edge_num;
				add_edge(i, j, len[i][j]);
				add_edge(j, i, len[i][j]);
			}
		}
	}
	
//	fprintf(stderr, "read ok, real_edge_num = %d\n", real_edge_num);

	all_visited = 0;
	for (int i = 1; i <= n; ++i)
		all_visited |= (1 << i);
	
	
	int vmin = 0x7fffffff;
	for (int i = 1; i <= n; ++i) {
		bfs(i, n, vmin);
//		fprintf(stderr, "vmin_%d = %d\n", i, vmin);
	}
	printf("%d\n", vmin);
	
	
	

	fclose(stdin);
	fclose(stdout);
	return 0;
}

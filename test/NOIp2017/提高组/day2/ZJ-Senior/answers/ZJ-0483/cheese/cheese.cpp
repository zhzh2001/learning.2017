#include <cstdio>
#include <cstring>
#include <cmath>
#include <queue>

using std::queue;

#define nullptr NULL
#define N 1001

struct Hole {
	int x; int y; int z;
} holes[N];

struct Node {
	Node(){}
	Node(int _dst, Node* _next = nullptr) {
		dst = _dst; next = _next;
	}
	int dst;
	Node *next;
};

struct NodeIndex {
	Node *head;
	Node *tail;
};

NodeIndex node_index[N];

void add_edge(int src, int dst) {
	if (node_index[src].head == nullptr) {
		node_index[src].head = node_index[src].tail = new Node(dst);
	}
	else {
		node_index[src].tail = node_index[src].tail->next = new Node(dst);
	}
}

void clean_edge(int n) {
	for (int i = 0; i < n; ++i) {
		Node *p = node_index[i].head;
		
		while (p != nullptr) {
			Node *tp = p->next;
			delete p;
			p = tp;
		}
	}
}

inline double dist(const Hole &src, const Hole &dst) {
	return sqrt((src.x - dst.x) * (src.x - dst.x) + (src.y - dst.y) * (src.y - dst.y) + (src.z - dst.z) * (src.z - dst.z));
}

bool bfs(int h, int n, int r) {
	queue<int> q;
	bool visited[N];
	memset(visited, 0, sizeof(visited));
//	fprintf(stderr, "avail head i = ");
	for (int i = 0; i < n; ++i) {
		if (holes[i].z <= r) {
//			fprintf(stderr, "%d ", i);
			q.push(i);
		}
	}
//	fprintf(stderr, "\n");
	
	while (!q.empty()) {
		int cur_pos = q.front();
		
		if (holes[cur_pos].z >= h - r) {
			return true;
		}
		
		Node* p = node_index[cur_pos].head;
		
		while (p != nullptr) {
			if (!visited[p->dst]) {
				q.push(p->dst);
				visited[p->dst] = true;
			}
			p = p->next;
		}
		
		q.pop();
	}
	return false;
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int t;
	scanf("%d", &t);
	
	for (int ti = 0; ti < t; ++ti) {
		memset(node_index, 0, sizeof(node_index));
		int n, h, r;
		scanf("%d%d%d", &n, &h, &r);
		for (int i = 0; i < n; ++i) {
			scanf("%d%d%d", &holes[i].x, &holes[i].y, &holes[i].z);
		}
		
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				if (dist(holes[i], holes[j]) <= 2 * r) {
					add_edge(i, j);
					add_edge(j, i);
				}
			}
		}
		
		printf("%s\n", ((bfs(h, n, r))? "Yes":"No"));
		
		clean_edge(n);
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

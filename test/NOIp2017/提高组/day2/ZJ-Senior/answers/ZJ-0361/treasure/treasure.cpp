#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;

const int N = 15;
int dis[N][N] , n , m , x , y, z , ans = 2e9 , dep[N] , res = 0;

void dfs(int x) {
	if(x == n + 1) {
		ans = min (ans , res);
		return;
	}
	if(res >= ans) return;
	for(int i = 1;i <= n;++ i) if(!dep[i]) {
		for(int j = 1;j <= n;++ j) if(dep[j] && dis[i][j] <= 1e9){
			dep[i] = dep[j] + 1; res += dep[j] * dis[i][j];
			dfs(x + 1);
			dep[i] = 0; res -= dep[j] * dis[i][j];
		}
	}
}

int main(void) {
	freopen("treasure.in" , "r" , stdin);
	freopen("treasure.out" , "w" , stdout);
	scanf("%d%d" , &n , &m);
	for(int i = 1;i <= n;++ i) {
		for(int j = 1;j <= n; ++ j) {
			dis[i][j] = dis[j][i] = 2e9;
		}
	}
	for(int i = 1;i <= m;++ i) {
		scanf("%d%d%d" , &x , &y , &z);
		dis[y][x] = dis[x][y] = min(dis[x][y] , z);
	}
	for(int i = 1;i <= n;++ i) {
		res = 0;
		dep[i] = 1;
		dfs(2);
		dep[i] = 0;
	}
	printf("%d\n" , ans);
}

#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <queue>
using namespace std;

const long long N = 5e4 + 5;

long long last[N][505] , n , m , q , x , y , ty;

vector< pair<long long,long long> >opt[N];

long long query(long long x , long long y , long long wh) {
	for(long long j = 1;j <= x - 1;++ j) last[j][wh] = last[j][wh - 1];
	for(long long j = x;j <= n;++ j) {
		last[j][wh] = last[j + 1][wh - 1];
	}
	if(y == m) {
		return last[n][wh] = last[x][wh - 1];
	}
	long long nx = x , ny = y , res = 0;
	for(long long i = (long long) opt[x].size() - 1;i >= 0; --i) {
		long long a = opt[x][i].first , b = opt[x][i].second;
		if(a <= ny + 1) ny ++;
		if(ny == m) {
			res = last[nx][b - 1];
			last[n][wh] = res;
			return res;
		}
	}
	res = (nx - 1) * 1ll * m + ny;
	return last[n][wh] = res;
}

const int N2 = 3e5 * 4;

int S[N2] , a[N2];

#define lowbit(x) (x & (-x))

void add(int x , int v) {
	for(int i = x;i < N2;i += lowbit(i)) S[i] += v;
}

int sum(int x) {
	int res = 0;
	for(int i = x;i;i -= lowbit(i)) res += S[i];
	return res;
}

void get20(void) {
	int tot = 0;
	for(int i = 1;i <= m ;++ i) a[++ tot] = i , add(i , 1);
	for(int i = 1;i <= q;++ i) {
		int x , y;
		scanf("%d%d" , &x , &y);
		int l = 1 , r = tot , res = 0;
		while(l <= r) {
			int mid = (l + r) >> 1;
			if(sum(mid) >= y) {
				res = mid; r = mid - 1;
			}
			else l = mid + 1;
		}
		printf("%d\n" , a[res]);
		a[++ tot] = a[res];
		add(res , -1); add(tot , 1);
	}
}

char ch = ' ';

void read(int &x) {
	ch = getchar(); while(!isdigit(ch)) ch = getchar();
	x = 0;
	while(isdigit(ch)) {
		x *= 10; x = x + ch - '0'; ch = getchar();
	}
}

long long b[N2];

void get10(void) {
	int tot = 0;
	for(int i = 1;i <= m; ++ i) b[++ tot] = i , add(tot , 1);
	for(int i = 2;i <= n; ++ i) b[++ tot] = i * 1ll * m , add(tot , 1);
	for(int i = 1;i <= q;++ i) {
		int x , y;
		read(x); read(y);
		int l = 1 , r = tot , res = 0;
		while(l <= r) {
			int mid = (l + r) >> 1;
			if(sum(mid) >= y) {
				res = mid; r = mid - 1;
			}
			else l = mid + 1;
		}
		printf("%lld\n" , b[res]);
		b[++ tot] = b[res];
		add(res , -1); add(tot , 1);
	}
}

int main(void) {
	freopen("phalanx.in" , "r" , stdin);
	freopen("phalanx.out" , "w" , stdout);
	scanf("%lld%lld%lld" , &n , &m , &q);
	if(n == 1) {
		get20();
		return 0;
	}
	else if(q > 1000) {
		get10();
		return 0;
	}
	for(long long i = 1;i <= n;++ i) {
		last[i][0] = i *  1ll * m;
	}
	for(long long i = 1;i <= q;++ i) {
		scanf("%lld%lld" , &x , &y);
		printf("%lld\n" , query(x , y , i));
		opt[x].push_back(make_pair(y + 1 , i));
	}
}

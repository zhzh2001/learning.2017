#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <queue>
using namespace std;

const int N = 1e4 + 5;
const int M = 2e6 + 5;

int fir[N] , ne[M] , to[M]  , cnt  ,  s , t , n;
bool vis[N];
queue<int>q;

const double eps = 1e-8;

double r , h;

struct NODE {
	double x , y , z;
	void input(void) {
		scanf("%lf%lf%lf" , &x ,&y ,&z);
	}
}P[N];

double sqr(double x) {
	return x * x;
}

double dis(NODE xxx , NODE yyy) {
	return sqrt(sqr(xxx.x - yyy.x) + sqr(xxx.y - yyy.y) + sqr(xxx.z - yyy.z));
}

void add(int x , int y) {
	ne[++ cnt] = fir[x]; fir[x] = cnt; to[cnt] = y;
}

void link(int x , int y) {
	add(x , y); add(y , x);
}

#define Foreachson(i , x) for(int i = fir[x];i;i = ne[i])

int main(void) {
	freopen("cheese.in" , "r" , stdin);
	freopen("cheese.out" , "w" , stdout);
	int ttt;
	cin >> ttt;
	for(int kkk = 1;kkk <= ttt;++ kkk) {
		cin >> n >> h >> r;
		for(int i = 1;i <= n;++ i) P[i].input();
		memset(fir , 0 , sizeof(fir));
		memset(vis , 0 , sizeof(vis));
		s = 0; t = n + 1; cnt = 0; while(!q.empty()) q.pop();
		vis[s] = vis[t] = 0;
		for(int i = 1;i <= n;++ i) {
			if(P[i].z + r >= h - eps) link(i , t);
			if(P[i].z - r <= eps) link(s , i);
			for(int j = i + 1;j <= n;++ j) {
				if(dis(P[i] , P[j]) <= 2 * r + eps) link(i , j);
			}
		}
		q.push(s); vis[s] = 1;
		while(!q.empty()) {
			int ind = q.front(); q.pop();
			Foreachson(i , ind) {
				int V = to[i];
				if(vis[V]) continue;
				vis[V] = 1;
				if(vis[t]) break;
				q.push(V);
			}
			if(vis[t]) {
				while(!q.empty()) q.pop();
			}
		}
		if(vis[t]) puts("Yes");
		else puts("No");
	}
}

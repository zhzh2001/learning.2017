#include<stdio.h>
#include<algorithm>
#include<iostream>
#define N 300005
#define M 300005
#define ll long long
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
struct opr{
	int x,y;
}Q[M];
int n,m,q;
struct P1{
	int A[1005][1005];
	void solve(){
		for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		A[i][j]=(i-1)*m+j;
		for(int i=1;i<=q;i++){
			int x=Q[i].x,y=Q[i].y,res=A[x][y];
			Pt(res),putchar('\n');
			for(int j=y;j<m;j++)
			A[x][j]=A[x][j+1];
			for(int j=x;j<n;j++)
			A[j][m]=A[j+1][m];
			A[n][m]=res;
		}
	}
}P1;
struct P2{
	#define UP 600000
	int tot;
	int A[UP+5],BIT[UP+5];
	void add(int x,int a){
		int i=x;
		while(i<=UP){
			BIT[i]+=a;
			i+=i&-i;
		}
	}
	int sum(int x){
		int i=x,res=0;
		while(i){
			res+=BIT[i];
			i-=i&-i;
		}
		return res;
	}
	int find(int k){
		int L=1,R=tot,res=-1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(sum(mid)>=k){
				res=mid;
				R=mid-1;
			}else L=mid+1;
		}
		return res;
	}
	void solve(){
		for(int i=1;i<=m;i++){
			add(i,1);
			A[i]=i;
		}
		tot=m;
		for(int i=1;i<=q;i++){
			int pos=find(Q[i].y);
			Pt(A[pos]);
			putchar('\n');
			add(pos,-1);
			A[++tot]=A[pos];
			add(tot,1);
		}
	}
}P2;
struct P3{
	#define Up 900000
	ll A[Up+5];
	int BIT[Up+5];
	void add(int x,int a){
		int i=x;
		while(i<=Up){
			BIT[i]+=a;
			i+=i&-i;
		}
	}
	int sum(int x){
		int i=x,res=0;
		while(i){
			res+=BIT[i];
			i-=i&-i;
		}
		return res;
	}
	int find(int k){
		int L=1,R=tot,res=-1;
		while(L<=R){
			int mid=(L+R)>>1;
			if(sum(mid)>=k){
				res=mid;
				R=mid-1;
			}else L=mid+1;
		}
		return res;
	}
	int tot;
	void solve(){
		for(int i=1;i<=m;i++){
			A[i]=i;
			add(i,1);
		}
		for(int i=2;i<=n;i++){
			A[m+i-1]=1LL*i*m;
			add(m+i-1,1);
		}
		tot=n+m-1;
		for(int i=1;i<=q;i++){
			int pos=find(Q[i].y);
			Pt(A[pos]),putchar('\n');
			add(pos,-1);
			A[++tot]=A[pos];
			add(tot,1);
		}
	}
}P3;
bool chk1(){
	for(int i=1;i<=q;i++)
	if(Q[i].x!=1)return false;
	return true;
}
//long long 2s 512M
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Rd(n);Rd(m);Rd(q);
	for(int i=1;i<=q;i++){
		Rd(Q[i].x);Rd(Q[i].y);
	}
	if(n<=1000&&m<=1000&&q<=500)P1.solve();
	else if(n==1)P2.solve();
	else if(chk1())P3.solve();
	return 0;
}

#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 15
#define M 1005
#define INF 1061109567
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
struct edge{
	int v,w,nxt;
}e[M<<1];
int n,m;
int cost[N][N];
struct P1{
	int dep[N],ans,tim;
	void dfs(int cnt,int res){
		tim++;
		if(tim>1e8)return;
		if(ans<res)return;
		if(cnt==n){
			if(res<ans)ans=res;
			return;
		}
		for(int i=1;i<=n;i++)
		if(dep[i]==-1){
			for(int j=1;j<=n;j++)
			if(~dep[j]&&cost[j][i]!=INF){
				dep[i]=dep[j]+1;
				dfs(cnt+1,res+dep[j]*cost[j][i]);
				dep[i]=-1;
			}
		}
	}
	void solve(){
		tim=0;
		memset(dep,-1,sizeof(dep));
		ans=INF;
		for(int i=1;i<=n;i++){
			dep[i]=1;
			dfs(1,0);
			dep[i]=-1;
		}
		Pt(ans);
		putchar('\n');
	}
}P1;
//long long
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int a,b,c;
	Rd(n);Rd(m);
	memset(cost,63,sizeof(cost));
	for(int i=1;i<=m;i++){
		Rd(a);Rd(b);Rd(c);
		if(c<cost[a][b])cost[a][b]=cost[b][a]=c;
	}
	P1.solve();
	return 0;
}

#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream>
#define N 1005
#define ll long long
using namespace std;
template <class T>
inline void Rd(T &res){
	char c;res=0;int k=1;
	while(c=getchar(),c<48&&c!='-');
	if(c=='-'){k=-1;c='0';}
	do{
		res=(res<<3)+(res<<1)+(c^48);
	}while(c=getchar(),c>=48);
	res*=k;
}
template <class T>
inline void Pt(T res){
	if(res<0){
		putchar('-');
		res=-res;
	}
	if(res>=10)Pt(res/10);
	putchar(res%10+48);
}
struct node{
	int x,y,z; 
}s[N];
bool mark[N];
int n,h,r;
bool check(int i,int j){
	int x=s[i].x-s[j].x;
	int y=s[i].y-s[j].y;
	int z=s[i].z-s[j].z;
	return 1LL*x*x+1LL*y*y<=4LL*r*r-1LL*z*z;
}
void dfs(int x){
	mark[x]=1;
	for(int i=1;i<=n;i++)
	if(!mark[i]&&check(x,i))dfs(i);
}
int T;
//long long
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	Rd(T);
	while(T--){
		Rd(n);Rd(h);Rd(r);
		for(int i=1;i<=n;i++){
			Rd(s[i].x);Rd(s[i].y);Rd(s[i].z);
		}
		memset(mark,0,sizeof(mark));
		for(int i=1;i<=n;i++)
		if(h-s[i].z<=r&&!mark[i])dfs(i);
		bool f=0;
		for(int i=1;i<=n;i++)
		if(s[i].z<=r&&mark[i])f=1;
		puts(f?"Yes":"No");
	}
	return 0;
}

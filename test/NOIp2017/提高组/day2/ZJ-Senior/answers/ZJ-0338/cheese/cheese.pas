var
  t,aaa,n,h,r,head,tail,i,j:longint;
  xbm,sbm,ccc:boolean;
  a:array[0..100000,1..3] of longint;
  b:array[0..1000,0..1000] of boolean;
  ss:array[0..1000] of boolean;
  q:array[0..100000] of longint;

function dist(x,y:longint):longint;
var
  t1,t2,t3:longint;
begin
  t1:=a[x,1]-a[y,1];
  t2:=a[x,2]-a[y,2];
  t3:=a[x,3]-a[y,3];
  dist:=trunc(sqrt(t1*t1+t2*t2+t3*t3));
end;

begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);

  readln(t);
  for aaa:=1 to t do
    begin
      readln(n,h,r);
      fillchar(b,sizeof(b),false);
      head:=0;
      xbm:=false;sbm:=false;
      for i:=1 to n do
        begin
          readln(a[i,1],a[i,2],a[i,3]);
          if a[i,3]+r>=h then
            begin
              sbm:=true;
              ss[i]:=true;
            end;
          if a[i,3]-r<=0 then
            begin
              xbm:=true;
              inc(head);
              q[head]:=i;
            end;
        end;
      if (not sbm) or (not xbm) then
        begin
          writeln('No');
          continue;
        end;
      for i:=1 to n do
        for j:=i+1 to n do
          if dist(i,j)<=2*r then
            begin
              b[i,j]:=true;
              b[j,i]:=true;
            end;
      tail:=head;ccc:=false;
      while head<=tail do
        begin
          for i:=1 to n do
            if b[q[head],i] then
              begin
                if ss[i] then
                  begin
                    writeln('Yes');
                    ccc:=true;
                    break;
                  end;
                inc(tail);
                q[tail]:=i;
              end;
          if ccc then break;
          inc(head);
        end;
      if not ccc then writeln('No');
    end;

  close(input);
  close(output);
end.

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
#define N 1010
using namespace std;
struct node
{
	int x,y,z,flag,g;
}data[N];
int id[N];
int t,n,h,r;


inline int read()
{
	char c=getchar();int f=1,x=0;
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

int cmp(int x,int y)
{
	return data[x].z<data[y].z;
}

int judge(int a,int b)
{
	int x1=data[a].x,y1=data[a].y,z1=data[a].z;
	int x2=data[b].x,y2=data[b].y,z2=data[b].z;
	int tmp=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
	if (tmp<=r*2) return 1;
	return 0;
}

void work()
{
	n=read();h=read();r=read();
	memset(data,0,sizeof(data));
	for (int i=1;i<=n;i++)
	{
		data[i].x=read();data[i].y=read();data[i].z=read();
		id[i]=i;
	}
	sort(id+1,id+n+1,cmp);
	for (int ii=1;ii<=n;ii++)
	{
		int i=id[ii];
		if (data[i].z<=r) data[i].flag=1;
		else break;
	}
	for (int ii=n;ii>=1;ii--)
	{
		int i=id[ii];
		if (data[i].z+r>=h) data[i].g=1;
		else break;
	}
	for (int ii=1;ii<=n;ii++)
	{
		int i=id[ii];
		if (data[i].flag!=1) continue;
		for (int jj=ii+1;jj<=n;jj++)
		{
			int j=id[jj];
			if (data[j].flag==1) continue;
			if (judge(i,j)) data[j].flag=1;
		}
	}
	for (int i=1;i<=n;i++)
		if (data[i].flag==1&&data[i].g==1)
		{
			printf("Yes\n");
			return;
		}
	printf("No\n");
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	while (t--) work();
	return 0;
}

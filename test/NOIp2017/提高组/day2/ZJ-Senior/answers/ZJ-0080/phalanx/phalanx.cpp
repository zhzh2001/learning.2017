#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
#define N 1010
using namespace std;
int n,m,q;
int a[N][N];

inline int read()
{
	char c=getchar();int f=1,x=0;
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

void special()
{
	int p[m+q+1];
	memset(p,0,sizeof(p));
	for (int i=1;i<=m;i++) p[i]=i;
	int tail=m;
	for (int i=1;i<=q;i++)
	{
		int x=read(),y=read();
		int j=1,k=0;
		while (k<y)
		{
			while (p[j]==0) j++;
			k++;j++;
		}
		j--;
		while (p[j]==0) j--;
		printf("%d\n",p[j]);
		p[++tail]=p[j];
		p[j]=0;
	}
}

void work()
{
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a[i][j]=(i-1)*m+j;
	for (int i=1;i<=q;i++)
	{
		int x=read(),y=read();
		printf("%d\n",a[x][y]);
		int tmp=a[x][y];
		for (int j=y;j<=m-1;j++) a[x][j]=a[x][j+1];
		for (int i=x;i<=n;i++) a[i][m]=a[i+1][m];
		a[n][m]=tmp;
	}
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if (n==1) special();
	else work();
	return 0;
}

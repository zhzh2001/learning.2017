#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define inf 0x3f3f3f3f
#define N 15
#define M 1010
using namespace std;
int map[N][N];
int n,m;
int t[N],v[N];
ll ans,d[N];

inline int read()
{
	char c=getchar();int f=1,x=0;
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}

void query(int x)
{
	for (int i=1;i<=n;i++) d[i]=inf;
	memset(v,0,sizeof(v));
	d[x]=0;t[x]=1;v[x]=1;
	for (int timet=1;timet<=n-1;timet++)
	{
		ll minn=0ll;
		int minp,fr;
		for (int i=1;i<=n;i++)
		{
			if (v[i]!=1) continue;
			for (int j=1;j<=n;j++)
			{
				if (v[j]==1) continue;
				if (map[i][j]==inf) continue;
				if ((ll)map[i][j]*t[i]<minn||minn==0)
				{
					minn=(ll)map[i][j]*t[i];
					minp=j;fr=i;
				}
			}
		}
		if (minn<d[minp]&&minn!=0)
		{
			d[minp]=minn;
			t[minp]=t[fr]+1;
			v[minp]=1;
		}
	}
	ll sum=0ll;
	for (int i=1;i<=n;i++) sum+=d[i];
	if (ans!=0) ans=min(ans,sum);
	else ans=sum;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (i==j) map[i][j]=0;
			else map[i][j]=inf;
	for (int i=1;i<=m;i++)
	{
		int x=read(),y=read(),k=read();
		map[x][y]=min(map[x][y],k);
		map[y][x]=map[x][y];
	}
	ans=0ll;
	for (int i=1;i<=n;i++) query(i);
	printf("%lld\n",ans);
	return 0;
}

var n, m, x, y, v, sum, ans, i, j, k: longint;
    a: array[1..20, 1..20] of longint;
begin
    assign(input, 'treasure.in');
    assign(output, 'treasure.out');
    reset(input);
    rewrite(output);

    readln(n, m);
    for i := 1 to m do
    begin
        readln(x, y, v);
        a[x, y] := 1;
        a[y, x] := 1;
    end;
    for i := 1 to n do
    for k := 1 to n do
    for j := 1 to n do
        if (i <> k) and (k <> j) and (i <> j) then
        begin
            if (a[i, k] > 0) and (a[k, j] > 0) then
                if a[i, j] = 0 then
                begin
                    a[i, j] := a[i, k] + a[k, j];
                    a[j, i] := a[i, j];
                end else
                    if a[i, j] > a[i, k] + a[k, j] then
                    begin
                        a[i, j] := a[i, k] + a[k, j];
                        a[j, i] := a[i, j];
                    end;
        end;

    ans := maxlongint;
    for i := 1 to n do
    begin
        sum := 0;
        for j := 1 to n do
            sum := sum + a[i, j];
        if sum < ans then
            ans := sum;
    end;
    writeln(ans * v);

    close(input);
    close(output);
end.
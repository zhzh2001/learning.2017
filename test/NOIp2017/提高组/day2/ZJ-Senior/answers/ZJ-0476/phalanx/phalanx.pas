var qq, q, n, m, max, i, j, xx: longint;
    x, y, shu, mm: int64;
    a: array[1..1003000] of longint;
begin
    assign(input, 'phalanx.in');
    assign(output, 'phalanx.out');
    reset(input);
    rewrite(output);

    readln(n, m, q);
    max := 1003000;
    mm := n * m;
    if mm > max then
        for i := 1 to max do
            a[i] := i else
            for i := 1 to mm do
                a[i] := i;
    for qq := 1 to q do
    begin
        readln(x, y);
        shu := (x - 1) * m + y;
        if shu > max then
            writeln(shu) else
            begin
                writeln(a[shu]);
                xx := a[shu];
                for i := (x - 1) * m + y to x * m - 1 do
                    a[i] := a[i + 1];
                for j := x to n - 1 do
                    a[j * m] := a[(j + 1) * m];
                a[n * m] := xx;
            end;
    end;

    close(input);
    close(output);
end.

var t, tt, n, h, i, j: longint;
    r: int64;
    xbm, sbm, p: boolean;
    dist: extended;
    x, y, z, sf, dd: array[1..1005] of int64;
procedure sort(l, r: longint);
var
    i, j, a, b: longint;
begin
    i := l;
    j := r;
    a := z[(l + r) div 2];
    repeat
        while z[i] > a do
            inc(i);
        while a > z[j] do
            dec(j);
        if not(i > j) then
        begin
            b := z[i];
            z[i] := z[j];
            z[j] := b;
            b := x[i];
            x[i] := x[j];
            x[j] := b;
            b := y[i];
            y[i] := y[j];
            y[j] := b;
            inc(i);
            j := j - 1;
        end;
    until i > j;
    if l < j then
        sort(l, j);
    if i < r then
        sort(i, r);
end;
begin
    assign(input, 'cheese.in');
    assign(output, 'cheese.out');
    reset(input);
    rewrite(output);

    readln(t);
    for tt := 1 to t do
    begin
        fillchar(sf, sizeof(sf), 0);
        fillchar(dd, sizeof(dd), 0);
        xbm := false;
        sbm := false;
        readln(n, h, r);
        for i := 1 to n do
        begin
            readln(x[i], y[i], z[i]);
            if r >= z[i] then
                xbm := true;
            if z[i] + r >= h then
                sbm := true;
        end;
        if (xbm = false) or (sbm = false) then
            writeln('No') else
            begin
                sort(1, n);
                for i := 1 to n do
                begin
                    if r >= z[i] then
                        dd[i] := 1;
                    if z[i] + r >= h then
                        sf[i] := 1;
                end;
                p := false;
                for i := 1 to n do
                if (p = true) then
                    break else
                    begin
                        if (dd[i] = 1) and (sf[i] = 1) then
                        begin
                            p := true;
                            break;
                        end;
                        if sf[i] = 1 then
                        begin
                            for j := i + 1 to n do
                            if abs(z[i] - z[j]) <= 2 * r then
                            begin
                                dist := sqrt(abs(x[i]-x[j])*abs(x[i]-x[j])+abs(y[i]-y[j])*abs(y[i]-y[j])+abs(z[i]-z[j])*abs(z[i]-z[j]));
                                if dist <= 2 * r then
                                    sf[j] := 1;
                            end;
                        end;
                    end;
                if p = true then
                    writeln('Yes') else
                    writeln('No');
            end;
    end;

    close(input);
    close(output);
end.

#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;
const long double eps=1e-12;
unsigned long long h,r,x[2100],y[2100],z[2100],k;
long double t;
int n,head,tail,vis[2100];
int f[2100][2100],q[2100];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while (T--)
	{
		cin>>n>>h>>r;
		for (int i=1;i<=n;i++)
			cin>>x[i]>>y[i]>>z[i];
		for (int i=0;i<=n+1;i++)
			for (int j=0;j<=n+1;j++)
				f[i][j]=0;
	//cout<<T<<endl;
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
			{
				k=(x[j]-x[i])*(x[j]-x[i])+(y[j]-y[i])*(y[j]-y[i])+(z[j]-z[i])*(z[j]-z[i]);
				t=sqrt(k);
				if (t-2*r<eps) f[i][j]=f[j][i]=1;
				}
		for (int i=1;i<=n;i++)
		{
			if (z[i]<=r) f[0][i]=f[i][0]=1;
			if (z[i]>=h-r) f[n+1][i]=f[i][n+1]=1;
			}	
		//	cout<<T<<endl;
		head=tail=1; q[tail]=0;
		for (int i=0;i<=n+1;i++) vis[i]=0;
		vis[0]=1;
		while (head<=tail)
		{
			int u=q[head];
			for (int i=1;i<=n+1;i++)
				if (!vis[i]&&f[u][i])
				{
					tail++;
					q[tail]=i;
					vis[i]=1;
				}
			head++;
		}
		if (vis[n+1]) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

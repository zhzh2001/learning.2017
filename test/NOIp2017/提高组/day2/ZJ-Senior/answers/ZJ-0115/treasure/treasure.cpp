#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,num;
long long g[210][210];
long long f[21][11000];
long long p[210];
int q[210];
long long minn;
void dfs(int x,long long sum,int dep,int sta,int stt)
{
	if (x>num)
	{
		f[dep+1][stt|sta]=min(f[dep+1][stt|sta],f[dep][sta]+sum*(dep+1));
		return;
	}
	dfs(x+1,sum,dep,sta,stt);
	dfs(x+1,sum+p[x],dep,sta,stt|(1<<(q[x]-1)));
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			g[i][j]=1e15;
	for (int i=1;i<=m;i++)
	{
		int x,y;
		long long z;
		scanf("%d%d%lld",&x,&y,&z);
		g[x][y]=min(g[x][y],z);
		g[y][x]=min(g[y][x],z);
	}
	for (int i=0;i<=n;i++)
		for (int j=0;j<=(1<<n)-1;j++)
			f[i][j]=1e15;
	for (int i=0;i<n;i++) f[0][1<<i]=0;
	for (int dep=0;dep<=n-2;dep++)
		for (int sta=1;sta<=(1<<n)-1;sta++)
		if (f[dep][sta]<1e15)
		{
			num=0;
			for (int i=0;i<n;i++)
				if (!(sta&(1<<i)))
				{
					q[++num]=i+1; 
					p[num]=1e15;
					for (int j=0;j<n;j++)
						if (sta&(1<<j))
							p[num]=min(p[num],g[i+1][j+1]);
				}
			dfs(1,0,dep,sta,0);
		}
	minn=1e15;
	for (int i=1;i<=n-1;i++)
		minn=min(minn,f[i][(1<<n)-1]);
	printf("%lld\n",minn);
}

#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,q,d,num,sum;
int f[1100][1100],p[51000],q1[1100000];
int tree[1100000],t[1100000],t1[510][51000];
int lowbit(int x)
{
	return x&-x;
}
void build(int x,int y)
{
	for (int i=x;i<=d;i+=lowbit(i))
		tree[i]+=y;
}
int query(int x)
{
	int ans=0;
	for (int i=x;i;i-=lowbit(i))
		ans+=tree[i];
	return ans;
}
int check(int x,int y)
{
	//printf("%d\n",query(x-1));
	return m-query(x-1)>=m-y+1;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n==1)
	{
		num=m;
		d=m+q;
		for (int i=1;i<=m;i++) t[i]=i,build(i,1);
		//for (int i=1;i<=m;i++) printf("%d\n",tree[i]);
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			int l=1;
			int r=num;
			while (l<r)
			{
				int mid=(l+r+1)>>1;
				if (check(mid,y)) l=mid;
				else r=mid-1;
			}
			printf("%d\n",t[r]);
			t[++num]=t[r];
			build(r,-1);
			build(num,1);
		}
		return 0;
	}
	if (n<=1000&&m<=1000&&q<=500)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				f[i][j]=(i-1)*m+j;
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			int t=f[x][y];
			for (int i=y+1;i<=m;i++) f[x][i-1]=f[x][i];
			for (int i=x+1;i<=n;i++) f[i-1][m]=f[i][m];
			f[n][m]=t;
			printf("%d\n",t);
		}
		return 0;
	}
	if (q<=500)
	{
		for (int i=1;i<=n;i++) t1[0][i]=i*m;
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			if (!p[x])
			{
				p[x]=++sum;
				for (int i=1;i<=m;i++) t1[sum][i]=(x-1)*m+i;
			} 
			t1[p[x]][m]=t1[0][x];
			int t=t1[p[x]][y];
			for (int i=y+1;i<=m;i++) t1[p[x]][i-1]=t1[p[x]][i];
			t1[p[x]][m]=t1[0][x];
			for (int i=x+1;i<=n;i++) t1[0][i-1]=t1[0][i];
			t1[0][n]=t;
			printf("%d\n",t);
		}
		return 0;
	}
	{
		num=m;
		d=m+q;
		for (int i=1;i<=m;i++) t[i]=i,build(i,1);
		for (int i=1;i<=n;i++) q1[i]=i*m;
		int head,tail;
		head=1; tail=n;
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			int l=1;
			int r=num;
			while (l<r)
			{
				int mid=(l+r+1)>>1;
				if (check(mid,y)) l=mid;
				else r=mid-1;
			}
			printf("%d\n",t[r]);
			t[++num]=q1[head];
			head++; tail++;
			q1[tail]=t[r];
			build(r,-1);
			build(num,1);
		}
		return 0;
	}
}

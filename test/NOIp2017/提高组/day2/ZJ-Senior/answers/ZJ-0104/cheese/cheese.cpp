#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<vector>
#include<queue>
#include<cstring>
#define open(s) ios::sync_with_stdio(false); freopen(s".in","r",stdin); freopen(s".out","w",stdout);
#define close fclose(stdin);fclose(stdout);
using namespace std;
vector<int> st,ed,g[1010];
int t,p[1010],x[1010],y[1010],z[1010],n,h,r,done[1010];
int find(int x){
	return p[x]==x ? x: find(p[x]);
}
double dist(int a,int b){
	return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]));
}
void doit(){
	st.clear();
	ed.clear();
	queue<int> q;
	cin>>n>>h>>r;
	for(int i=1;i<=n;i++){
		g[i].clear();
		cin>>x[i]>>y[i]>>z[i];
		p[i]=i;
		if(z[i]-r<=0) st.push_back(i);
		if(z[i]+r>=h) ed.push_back(i);
		for(int j=1;j<i;j++){
			//cout<<dist(i,j)<<" i="<<i<<" j="<<j<<endl;
			if(dist(i,j)<=2*r){
				g[i].push_back(j);
				g[j].push_back(i);
			}
		}
	}
	/*for(int i=0;i<st.size();i++) cout<<st[i]<<" ";
	cout<<endl;
	for(int j=0;j<ed.size();j++) cout<<ed[j]<<" ";
	cout<<endl;
	for(int i=1;i<=n;i++){
		cout<<" i="<<i<<" ";
		for(int j=0;j<g[i].size();j++) cout<<g[i][j]<<" ";
		cout<<endl;
	}*/
	for(int i=0;i<st.size();i++){
		int x=find(st[i]);
		for(int j=0;j<g[st[i]].size();j++){
			int y=find(g[st[i]][j]);
			if(x!=y){
				p[x]=y;
				q.push(g[st[i]][j]);
				//cout<<"p"<<x<<"="<<y<<endl;
			}
		}
	}
	while(!q.empty()){
		int x=q.front();
		q.pop();
		int xx=find(x);
		for(int i=0;i<g[x].size();i++){
			int yy=find(g[x][i]);
			if(xx!=yy){
				p[xx]=yy;
				q.push(g[x][i]);
			}	
		}
	}
	for(int i=0;i<st.size();i++){
		for(int j=0;j<ed.size();j++){
			if(find(st[i])==find(ed[j])){
				cout<<"Yes"<<endl;
				return;
			}
		}
	}
	cout<<"No"<<endl;
}
int main(){
	open("cheese");
	cin>>t;
	while(t--){
		doit();
	}
	close;
	return 0;
}

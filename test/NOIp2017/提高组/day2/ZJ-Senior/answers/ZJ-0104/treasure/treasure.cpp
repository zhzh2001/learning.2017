#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<vector>
#include<queue>
#include<cstring>
#define open(s) ios::sync_with_stdio(false); freopen(s".in","r",stdin); freopen(s".out","w",stdout);
#define close fclose(stdin);fclose(stdout);
using namespace std;
class edge{
public:
	int from,to,dist;
	edge(int a=0,int b=0,int c=0): from(a),to(b),dist(c){}
	bool operator < (const edge& b) const{
		return dist>b.dist;
	}
};
int n,m,ans=2147483647;
vector<edge> edges,edges_bak;
vector<int> g[20];
int cmp(edge a,edge b){
	return a.dist<b.dist;
}
int search_(int x){
	int depth[20],ans=0,side=0;
	bool done[20];
	memset(done,0,sizeof(done));
	priority_queue<edge> q;
	depth[x]=1;
	done[x]=1;
	for(int i=0;i<g[x].size();i++){
		edge& e=edges[g[x][i]];
		if(!done[e.to]){
			q.push(e);
		}
	}
	while(!q.empty()){
		/*for(int i=1;i<=n;i++){
			cout<<"depth "<<i<<" = "<<depth[i]<<" done="<<(int)done[i]<<" ans="<<ans<<endl;
		}*/
		edge x=q.top();
		q.pop();
		if(!done[x.to]){
			done[x.to]=1;
			side++;
			ans+=x.dist;
			depth[x.to]=depth[x.from]+1;
			for(int j=0;j<g[x.to].size();j++){
				edge& e=edges[g[x.to][j]];
				if(!done[e.to]){
					e.dist*=depth[x.to];
					q.push(e);
				}
			}
		}
		if(side==n-1) break;
	}
	return ans;
}
int main(){
	open("treasure");
	cin>>n>>m;
	for(int i=0;i<m;i++){
		int x,y,z;
		cin>>x>>y>>z;
		edges_bak.push_back(edge(x,y,z));
		edges_bak.push_back(edge(y,x,z));
		g[x].push_back(edges_bak.size()-2);
		g[y].push_back(edges_bak.size()-1);
	}
	/*for(int i=1;i<=n;i++){
		cout<<"g"<<i<<" = ";
		for(int j=0;j<g[i].size();j++) cout<<edges[g[i][j]].to<<" "<<edges[g[i][j]].dist<<",";
		cout<<endl;
	}*/
	for(int i=1;i<=n;i++){
		edges.clear();
		for(int j=0;j<edges_bak.size();j++){
			edges.push_back(edges_bak[j]);
		}
		int t=search_(i);
		//cout<<"i="<<i<<" t="<<t<<endl;
		if(t<ans) ans=t;
	}
	cout<<ans<<endl;
	close;
	return 0;
}

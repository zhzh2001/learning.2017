#include<bits/stdc++.h>
using namespace std;

#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;i++)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;i--)
#define LL long long
#define INF 0x3f3f3f3f
#define inf 1e15
#define Sz(a) sizeof(a)
#define mcl(a,b) memset(a,b,Sz(a))

#define N 15
#define M 1005

int n,m;
struct node{
	int to;
	LL cost;
};
vector<node>E[N];
int dis[N][N],num;

struct p40{
	
	void solve(){
		REP(k,1,n)
			REP(i,1,n)
				REP(j,1,n)
					dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
		LL ans=inf;
		REP(i,1,n){
			LL sum=0;
			int f=1;
			REP(j,1,n){
				if(i==j)continue;
				if(dis[i][j]==INF){
					f=0;
					break;
				}
				sum+=dis[i][j];
			}
			if(f)ans=min(ans,sum);
		}
		printf("%lld\n",1LL*ans*num);
	}
}p40;

//struct pwater{
//	void solve(){
//		
//	}
//}pwater;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	int f=1;
	mcl(dis,63);
	REP(i,1,m){
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		E[a].push_back((node){b,c});
		E[b].push_back((node){a,c});
		dis[a][b]=dis[b][a]=1;
		if(num!=c){
			if(i==1)num=c;
			else f=0;
		}
	}
	if(f)p40.solve();
//	else pwater.solve();
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;i++)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;i--)
#define LL long long
#define INF 0x3f3f3f3f
#define Sz(a) sizeof(a)
#define mcl(a,b) memset(a,b,Sz(a))


#define N 1005

int n;
LL h,r;

struct node{
	LL x,y,z;
}A[N];

vector<int>E[N];
bool vis[N]; 

bool check_dis(int id1,int id2){
	return sqrt( (A[id1].x-A[id2].x)*(A[id1].x-A[id2].x) + (A[id1].y-A[id2].y)*(A[id1].y-A[id2].y) + (A[id1].z-A[id2].z)*(A[id1].z-A[id2].z) ) <=2*r; 
}

void Init(){
	REP(i,1,n){
		REP(j,i+1,n){
			if(check_dis(i,j)){
				E[i].push_back(j);
				E[j].push_back(i); 
			}		
		}
	}
}

bool check_up(LL x){
	return x+r>=h; 
}
bool check_down(LL x){
	return x-r<=0;
}

bool bfs(){
	queue<int>Q;
	while(!Q.empty())Q.pop();
	
	mcl(vis,0);
	
	REP(i,1,n)if(check_down(A[i].z))Q.push(i);
	if(Q.empty())return 0;
	while(!Q.empty()){
		int x=Q.front();Q.pop();
		if(check_up(A[x].z)){
			return 1;
		} 
		if(vis[x])continue;
		vis[x]=1;
		REP(i,0,E[x].size()-1){
			int y=E[x][i];
			if(!vis[y]){
				Q.push(y);
			}	
		} 
	}
	return 0;
}


int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;cin>>T;
	while(T--){
		cin>>n>>h>>r;
		REP(i,1,n)scanf("%lld%lld%lld",&A[i].x,&A[i].y,&A[i].z);
		Init();
		int f=bfs();
		if(f)puts("Yes");
		else puts("No"); 
		REP(i,1,n)E[i].clear();
	}
	return 0;
}

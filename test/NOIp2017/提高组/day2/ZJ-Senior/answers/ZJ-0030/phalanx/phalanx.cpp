#include<bits/stdc++.h>
using namespace std;

#define REP(i,f,t) for(int i=(f),i##_end_=(t);i<=i##_end_;i++)
#define DREP(i,f,t) for(int i=(f),i##_end_=(t);i>=i##_end_;i--)
#define LL long long
#define INF 0x3f3f3f3f
#define Sz(a) sizeof(a)
#define mcl(a,b) memset(a,b,Sz(a))

#define N 5005
 
int n,m,q;

struct p30{
	int mp[N][N];
	void Init(){
		int cnt=0;
		REP(i,1,n)REP(j,1,m)mp[i][j]=++cnt;
	}
	void work(int x,int y){
		int t=mp[x][y];
		REP(j,y,m-1)mp[x][j]=mp[x][j+1];
		REP(i,x,n-1)mp[i][m]=mp[i+1][m];
		mp[n][m]=t;
//		REP(i,1,n){
//			REP(j,1,m){
//				printf("%d ",mp[i][j]);
//			}
//			puts("");
//		}
//		puts("!!!");
	}
	void solve(){
		Init();
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%d\n",mp[x][y]);
			work(x,y);	
		}
	}
}p30;
struct p20{
	#define M 100005
	
	#define lson L,mid,p<<1
	#define rson mid+1,R,p<<1|1 
	#define family tree[p],tree[p<<1],tree[p<<1|1]
	struct Tree{
		int L,R;
		int sum,add;
	}tree[M<<2];
	void Down(Tree &A,Tree &L,Tree &R){
		int &t=A.add;
		if(!t)return;
		L.add+=A.add;
		R.add+=A.add;
		t=0;
	}
	void build(int L,int R,int p){
		tree[p].L=L,tree[p].R=R;
		tree[p].add=tree[p].sum=0;
		if(L==R){
			tree[p].sum=L;
			return;
		}
		int mid=(L+R)>>1;
		build(lson),build(rson);
	} 
	void update(int L,int R,int p){
//		printf("L=%d R=%d\n",tree[p].L,tree[p].R);
		if(tree[p].L==L && tree[p].R==R){
//			printf("L=%d R=%d\n",L,R);
			tree[p].add++;
			return;
		}
		Down(family);
		int mid=(tree[p].L+tree[p].R)>>1;
		if(R<=mid)update(L,R,p<<1);
		else if(L>mid)update(L,R,p<<1|1);
		else update(lson),update(rson);
	}
	int query(int p,int x){
		if(tree[p].L==tree[p].R)return tree[p].sum;
		int mid=(tree[p].L+tree[p].R)>>1;
		Down(family);
		if(x<=mid)return tree[p].add+query(p<<1,x);
		else return tree[p].add+query(p<<1|1,x); 
	}
	void Find(int p,int res){
		if(tree[p].L==tree[p].R){
			tree[p].sum=res;
			tree[p].add=0;
			return;
		}
		int mid=(tree[p].L+tree[p].R)>>1;
		if(m<=mid)Find(p<<1,res);
		else Find(p<<1|1,res);
	}
	void solve(){
		build(1,m,1);
		REP(i,1,q){
			int x,y;
			scanf("%d%d",&x,&y);
			if(y<m)update(y,m-1,1);
			int t=query(1,y);
			if(y<m)Find(1,t);
			printf("%d\n",t);
		}	
	}
}p20;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout) ;
	cin>>n>>m>>q;
	if(n<=5000 && m<=5000)p30.solve();
	else if(n==1)p20.solve();
		
	return 0;
}

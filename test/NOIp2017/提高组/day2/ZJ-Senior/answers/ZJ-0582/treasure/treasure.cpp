#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int num[200],n,m,dep[200],x,y,z,mp[20][20],ans=2100000000,X[200],Y[200],Z[200];
bool bo[21];
int main()
{
	srand(19260817);
	freopen("treasure.in","r",stdin),freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; i++)
		for (int j=1; j<=n; j++)
			if(i!=j)mp[i][j]=2100000000;
	for (int i=1; i<=m; i++)
		scanf("%d%d%d",&x,&y,&z),mp[x][y]=mp[y][x]=min(z,mp[x][y]);
	int top=0;
	for (int i=1; i<=n; i++)
		for (int j=i+1; j<=n; j++)
			if(i!=j&&mp[i][j]!=2100000000)
				top++,X[top]=i,Y[top]=j,Z[top]=mp[i][j];

	m=top;
	for (int i=1; i<=m; i++)num[i]=i;
	int TOT=15000000/m;
	int ans=2100000000,now=0;
	for (int ii=1; ii<=TOT; ii++)
	{
		random_shuffle(num+1,num+1+m);
		memset(bo,0,sizeof(bo)),memset(dep,0,sizeof(dep)),bo[X[num[1]]]=true,bo[Y[num[1]]]=true,now=Z[num[1]],dep[X[num[1]]]=0,dep[Y[num[1]]]=1;
		for (int i=2; i<=m; i++)
			if(bo[X[num[i]]]+bo[Y[num[i]]]==1)
			{
				if(bo[Y[num[i]]])swap(X[num[i]],Y[num[i]]);
				bo[Y[num[i]]]=true,dep[Y[num[i]]]=dep[X[num[i]]]+1,now+=dep[Y[num[i]]]*Z[num[i]];
				if(now>ans)break;
			}
		bool ok=true;
		for (int i=1; i<=n; i++)
			if(!bo[i])
			{
				ok=false;
				break;
			}
		if(ok)ans=min(ans,now);


		memset(bo,0,sizeof(bo)),memset(dep,0,sizeof(dep)),bo[X[num[1]]]=true,bo[Y[num[1]]]=true,now=Z[num[1]],dep[X[num[1]]]=1,dep[Y[num[1]]]=0;
		for (int i=2; i<=m; i++)
			if(bo[X[num[i]]]+bo[Y[num[i]]]==1)
			{
				if(bo[Y[num[i]]])swap(X[num[i]],Y[num[i]]);
				bo[Y[num[i]]]=true,dep[Y[num[i]]]=dep[X[num[i]]]+1,now+=dep[Y[num[i]]]*Z[num[i]];
				if(now>ans)break;
			}
		ok=true;
		for (int i=1; i<=n; i++)
			if(!bo[i])
			{
				ok=false;
				break;
			}
		if(ok)ans=min(ans,now);
	}
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<cmath>
#include<cstring>
using namespace std;
long long n,i,j,T;
long long h,r,x[10000],y[10000],z[10000];
long long top,to[1000100],first[1000100],next[1000100],last[1000100],q[100000],head,tail;
bool bo[100000],ok[101000],incheese[100010];
long long dis(long long a,long long b)
{
	return (x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]);
}
void add(long long a,long long b)
{
	top++,to[top]=b;
	if(first[a]==0)first[a]=top;
	else next[last[a]]=top;
	last[a]=top;
}
long long ABS(long long x)
{
	if(x<0)return -x;else return x;
}
int main()
{
	freopen("cheese.in","r",stdin),freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	while (T--)
	{
		scanf("%lld%lld%lld",&n,&h,&r);memset(incheese,0,sizeof(incheese));
		for (i=1; i<=n; i++)
		{
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if(z[i]+r>=0||z[i]-r>=h)incheese[i]=true;
		}
		top=0,memset(bo,0,sizeof(bo)),memset(ok,0,sizeof(ok)),memset(first,0,sizeof(first)),memset(next,0,sizeof(next)),memset(last,0,sizeof(last)),memset(to,0,sizeof(to)),memset(q,0,sizeof(q));
		for (i=1; i<=n; i++)
			for (j=i+1; j<=n; j++)
			if(incheese[i]&&incheese[j])
				if(dis(i,j)<=4ll*r*r)
				{
					add(i,j),add(j,i);
				}
		head=0,tail=0;
		for (i=1; i<=n; i++)
			if(ABS(z[i])<=r)
			{
				bo[i]=true;
				head++;q[head]=i;
				ok[i]=true;
			}
		while(head>=tail)
		{
			tail++;
			for (i=first[q[tail]]; i; i=next[i])
			{
				ok[to[i]]=true;
				if(!bo[to[i]])
				{
					head++,q[head]=to[i];
					bo[to[i]]=true;
				}
			}
		}
		bool ans=false;
		for (i=1; i<=n; i++)
			if(ABS(z[i]-h)<=r&&ok[i])
			{
				ans=true;
			}
		if(ans)puts("Yes");else puts("No");
	}

	return 0;
}

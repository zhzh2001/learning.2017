#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,mp[13][13],oi;
long long f[10000][13];
bool ok[13];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=12;i++)
	 for(int j=1;j<=12;j++)
	  if(i!=j)mp[i][j]=mp[j][i]=500001;
	for(int i=1,a,b,c;i<=m;i++)scanf("%d%d%d",&a,&b,&c),mp[a][b]=min(mp[a][b],c),mp[b][a]=mp[a][b];
	for(int i=2;i<=((1<<(n+1))-2);i=i+2)f[i][0]=5000000000000000000;
	for(int i=2;i<=((1<<(n+1))-2);i=i+2)
	{
		memset(ok,0,sizeof ok),oi=0;
		for(int j=1;j<=12;j++)
		 if((1<<j)&i)oi++,ok[j]=1;
		if(oi==1){f[i][0]=0;continue;}
		for(int j=1;j<=12;j++)
		 if(ok[j])
		  for(int k=1;k<=12;k++)
		   if(ok[k]&&j!=k&&mp[j][k]!=500001&&f[i][0]>f[i^(1<<j)][0]+(f[i^(1<<j)][k]+1)*mp[j][k])
		   {
		   	  f[i][0]=f[i^(1<<j)][0]+(f[i^(1<<j)][k]+1)*mp[j][k];
		   	  f[i][j]=f[i^(1<<j)][k]+1;
		   	  for(int q=1;q<=12;q++)
		   	   if(q!=j)f[i][q]=f[i^(1<<j)][q];
		   }
	}
	printf("%lld",f[(1<<(n+1))-2][0]);
	return 0;
}

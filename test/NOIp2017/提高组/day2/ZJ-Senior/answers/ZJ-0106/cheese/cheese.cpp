#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
using namespace std;
int n,h;
long long mp[1010][3],r;
bool sh[1010],xia[1010],e[1010][1010],vis[1010],ok;
void dfs(int x)
{
	if(vis[1005])return ;
	if(e[x][1005]){vis[1005]=1;return ;}
	for(int i=1;i<=n;i++)
	 if(x!=i&&!vis[i]&&e[x][i])vis[i]=1,dfs(i);
}
int main()
{
	int t;
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
	   memset(e,0,sizeof e),memset(vis,0,sizeof vis),memset(xia,0,sizeof xia),memset(sh,0,sizeof sh),ok=0;
	   scanf("%d%d%d",&n,&h,&r);
	   for(int i=1,a,b,c;i<=n;i++)scanf("%d%d%d",&a,&b,&c),mp[i][0]=a,mp[i][1]=b,mp[i][2]=c;
	   for(int i=1;i<=n;i++)
	    if((mp[i][2]-r)<=0)xia[i]=1,e[i][0]=e[0][i]=1;
	   for(int i=1;i<=n;i++)
	    if((h-mp[i][2])<=r)sh[i]=1,e[i][1005]=e[1005][i]=1;
	   for(int i=1;i<=n;i++)
	    if(sh[i]&&xia[i]){printf("Yes\n");ok=1;break;}
	   if(ok){ok=0;continue;}
	   for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++)
	     if(i!=j&&(mp[i][0]-mp[j][0])*(mp[i][0]-mp[j][0])+(mp[i][1]-mp[j][1])*(mp[i][1]-mp[j][1])+(mp[i][2]-mp[j][2])*(mp[i][2]-mp[j][2])-4*r*r<=0&&!e[i][j])
	      e[i][j]=e[j][i]=1;
       vis[0]=1;dfs(0);
       if(vis[1005])printf("Yes\n");
       else printf("No\n");
	}
    return 0;
}

#include<bits/stdc++.h>
#define LL long long 
#define INF 0x3f3f3f3f
#define lowbit(x) (x & (-x))
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 300000 + 5;

int n, m, q;
int X[N], Y[N];

namespace CASEI {
	
	const int M = 50000 + 5;
	
	int vis[M], cnt;
	LL L[M];
	LL A[505][M];
	int id[M];
	
	LL getval(int x, int y) {
		return 1LL * (x - 1) * m + y; 
	}
	
	void add(int x) {
		id[x] = ++cnt;
		for (int i = 1; i <= m - 1; i++) A[cnt][i] = getval(x, i);
	}
	
	void solve() {
		cnt = 0;
		for (int i = 1; i <= n; i++) vis[i] = 0;
		for (int i = 1; i <= n; i++) L[i] = getval(i, m);
		for (int i = 1; i <= q; i++) {
			if (!vis[X[i]]) {
				vis[X[i]] = 1;
				add(X[i]);
			}
			int p = id[X[i]];
			LL ans = Y[i] <= m - 1 ? A[p][Y[i]] : L[X[i]];
			if (Y[i] <= m - 1) {
				for (int j = Y[i]; j < m - 1; j++) A[p][j] = A[p][j + 1];
				A[p][m - 1] = L[X[i]];
			}
			for (int j = X[i]; j <= n; j++) L[j] = L[j + 1];
			L[n] = ans;
			printf("%lld\n", ans);
		}
	}
	
}

namespace CASEII {
	
	const int M = 900000 + 5;

	#define lc (x << 1)
	#define rc (x << 1 | 1)

	int sum[M << 2];
	LL val[M];

	LL getval(int x, int y) {
		return 1LL * (x - 1) * m + y; 
	}
	
	void upd(int x) {
		sum[x] = sum[lc] + sum[rc];
	}
	
	void build(int x, int l, int r) {
		if (l == r) {
			sum[x] = val[l] ? 1 : 0;
			return;
		}
		int m = (l + r) >> 1;
		build(lc, l, m);
		build(rc, m + 1, r);
		upd(x);
	}

	int getkth(int x, int l, int r, int k) {
		if (l == r) {
			sum[x] = 0;
			return l;
		}
		int m = (l + r) >> 1, ans = 0;
		if (sum[lc] >= k) ans = getkth(lc, l, m, k);
		else ans = getkth(rc, m + 1, r, k - sum[lc]);
		upd(x);
		return ans;
	}

	void modify(int x, int l, int r, int pos) {
		if (l == r) {
			sum[x] = 1;
			return;
		}
		int m = (l + r) >> 1;
		if (pos <= m) modify(lc, l, m, pos);
		else modify(rc, m + 1, r, pos);
		upd(x);
	}

	void solve() {
		for (int i = 1; i <= m; i++) val[i] = getval(1, i);
		for (int i = 2; i <= n; i++) val[m + i - 1] = getval(i, m);
		int num = n + m + q;
		int cnt = n + m - 1;
		build(1, 1, num);
		for (int i = 1; i <= q; i++) {
			if (X[i] != 1) {
				printf("F**K YOU!\n");
				return;
			}
			int ans = getkth(1, 1, num, Y[i]);
			printf("%lld\n", val[ans]);
			val[++cnt] = val[ans];
			modify(1, 1, num, cnt);
		}	
	}
}
	
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n), read(m), read(q);
	for (int i = 1; i <= q; i++) {
		read(X[i]), read(Y[i]);
	}
	if (q <= 500) CASEI::solve();
		else CASEII::solve();
	return 0;
}

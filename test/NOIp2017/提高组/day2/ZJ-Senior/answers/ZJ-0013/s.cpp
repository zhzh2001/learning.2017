#include<bits/stdc++.h>
#define LL long long 
#define INF 0x3f3f3f3f
#define lowbit(x) (x & (-x))
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 300000 + 5;

int n, m, q;
int X[N], Y[N];

namespace CASEI {
	
	const int M = 100 + 5;
	
	int vis[M], cnt;
	LL L[M];
	LL A[105][M];
	int id[M];
	
	LL getval(int x, int y) {
		return 1LL * (x - 1) * m + y; 
	}
	
	void add(int x) {
		id[x] = ++cnt;
		for (int i = 1; i <= m - 1; i++) A[cnt][i] = getval(x, i);
	}
	
	void solve() {
		cnt = 0;
		for (int i = 1; i <= n; i++) vis[i] = 0;
		for (int i = 1; i <= n; i++) L[i] = getval(i, m);
		for (int i = 1; i <= q; i++) {
			if (!vis[X[i]]) {
				vis[X[i]] = 1;
				add(X[i]);
			}
			int p = id[X[i]];
			LL ans = Y[i] <= m - 1 ? A[p][Y[i]] : L[X[i]];
			if (Y[i] <= m - 1) {
				for (int j = Y[i]; j < m - 1; j++) A[p][j] = A[p][j + 1];
				A[p][m - 1] = L[X[i]];
			}
			for (int j = X[i]; j <= n; j++) L[j] = L[j + 1];
			L[n] = ans;
			printf("%lld\n", ans);
		}
	}
	
}
	
int main() {
	freopen("d.in", "r", stdin);
	read(n), read(m), read(q);
	printf("%d %d %d\n", n, m, q);
	for (int i = 1; i <= q; i++) {
		read(X[i]), read(Y[i]);
		// printf("%d %d %d\n", i, X[i], Y[i]);
	}
	CASEI::solve();
	return 0;
}

#include<bits/stdc++.h>
#define LL long long 
#define INF 0x3f3f3f3f
#define lowbit(x) (x & (-x))
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 12;

int n, m;
int g[N][N];
int v[N][1 << N];
int pos[1 << N];
int f[N][1 << N];

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	read(n), read(m);
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < n; j++) g[i][j] = INF;
	for (int i = 0; i < n; i++) g[i][i] = 0;
	for (int i = 1, x, y, z; i <= m; i++) {
		read(x), read(y), read(z); x--, y--;
		g[x][y] = min(g[x][y], z);
		g[y][x] = min(g[y][x], z);
	}
	for (int i = 0; i < n; i++) v[i][0] = INF;
	for (int i = 0; i < n; i++) pos[1 << i] = i;
	for (int i = 0; i < n; i++)
		for (int j = 1; j < (1 << n); j++) 
			v[i][j] = min(v[i][j - lowbit(j)], g[i][pos[lowbit(j)]]);
	for (int d = 0; d < n; d++) 	
		for (int i = 0; i < (1 << n); i++) f[d][i] = INF;
	for (int i = 0; i < n; i++) f[0][1 << i] = 0;
	for (int d = 1; d < n; d++) 
		for (int i = 1; i < (1 << n); i++) {
			int s = (i - 1) & i;
			while (s) {
				int t = i - s, add = 0;
				while (t) {
					int p = pos[lowbit(t)];
					if (v[p][s] == INF) {
						add = INF;
						break;
					} else add += v[p][s];
					t -= lowbit(t);
				}
				if (add != INF && f[d - 1][s] != INF) f[d][i] = min(f[d][i], f[d - 1][s] + add * d);
				s = (s - 1) & i;
			}
		}
	int ans = INF;
	for (int d = 0; d < n; d++) ans = min(ans, f[d][(1 << n) - 1]);
	printf("%d\n", ans);
	return 0;	
}

#include<bits/stdc++.h>
#define LL long long 
#define LDB long double
using namespace std;

template<class T> inline 
void read(T &x) {
	x = 0; int f = 1; char ch = getchar();
	while (ch < '0' || ch > '9')   {if (ch == '-') f = -1; ch = getchar();}
	while ('0' <= ch && ch <= '9') {x = x * 10 + ch - '0'; ch = getchar();}
	x *= f;
}

const int N = 1000 + 5;

int n;
LL h, r;
int f[N];

struct node {
	LL x, y, z;
} p[N];

int find(int x) {return f[x] == x ? x : f[x] = find(f[x]);}

void merge(int x, int y) {
	x = find(x), y = find(y);
	if (x == y) return;
	f[y] = x;
}

double sqr(LL x) {
	return (double) 1.0 * x * x;
}

double dis(int x, int y) {
	double ans = 0;
	ans = sqrt(sqr(p[x].x - p[y].x) + sqr(p[x].y - p[y].y) + sqr(p[x].z - p[y].z));
	return ans;
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int cas;
	read(cas);
	while (cas--) {
		read(n), read(h), read(r); int S = n + 1, T = n + 2; 
		for (int i = 1; i <= T; i++) f[i] = i;
		for (int i = 1; i <= n; i++) read(p[i].x), read(p[i].y), read(p[i].z);
		for (int i = 1; i <= n; i++) if (p[i].z - r <= 0) merge(S, i);
		for (int i = 1; i <= n; i++) if (p[i].z + r >= h) merge(T, i);
		for (int i = 1; i <= n; i++) 
			for (int j = i + 1; j <= n; j += 3) {
				if (j + 0 <= n && dis(i, j + 0) <= (double)2.0 * r) merge(i, j + 0);
				if (j + 1 <= n && dis(i, j + 1) <= (double)2.0 * r) merge(i, j + 1);
				if (j + 2 <= n && dis(i, j + 2) <= (double)2.0 * r) merge(i, j + 2);
			}
		if (find(S) == find(T)) {
			printf("Yes\n");
		} else printf("No\n");
	}
	return 0;
}

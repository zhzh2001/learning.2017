Var
 n,m,q,i,GSize:Longint;
 Leave:Int64;
 X,Y:Array[0..300005]Of Longint;    {Querys}

Function max(a,b:Longint):Longint;
Begin if a>b then exit(a); exit(b) end;

vAR                                 {Solve1}
 Las:Array[0..300005]Of Int64;
 H:Array[0..505,0..50005]Of Int64;
 G:Array[0..505]Of Longint;

Procedure Solve1;
Var i,j,Row,qx,qy:Longint; tmp:Int64;
Begin
 For i:=1 to n Do Las[i]:=Int64(i)*m;
 For i:=1 to Q Do Begin
  qx:=x[I];
  qy:=y[I];
  If qy=m Then
   Leave:=Las[qx]
  Else Begin
   Row:=0; For j:=1 to GSize Do If G[j]=qx Then Begin Row:=j; Break End;
   If Row=0 Then Begin
    Inc(GSize);
    G[GSize]:=qx;
    Row:=GSize;
    tmp:=int64(qx-1)*m;
    For j:=1 to m-1 Do H[GSize,j]:=tmp+j
   End;
   Leave:=H[Row,qy];
   For j:=qy to m-2 Do H[Row,j]:=H[Row,j+1];
   H[Row,m-1]:=Las[qx]
  End;
  WriteLn(Leave);
  For j:=qx to n-1 Do Las[j]:=Las[j+1];
  Las[n]:=Leave
 End
End;

Function Judge2:Boolean;
Var i:Longint;
Begin
 For i:=1 to Q Do If X[i]<>1 Then Exit(False);
 Exit(True)
End;

VAR                                 {Solve2}
 Root,top:Longint;
 f,c,sta:Array[0..300005]Of Longint;
 va:Array[0..300005]Of Int64;
 rv:Array[0..300005]of Boolean;
 s:Array[0..300005,0..1]Of Longint;

 Procedure sw(Var a,b:Longint);
 Var c:Longint; Begin c:=a; a:=b; b:=c end;

 Procedure Rev(k:Longint);
 Begin
  If k<>0 Then
   Rv[k]:=Not Rv[k]
 End;

 Procedure pd(k:Longint);
 Begin
  If rv[k] Then Begin
   Rev(s[k,0]);
   Rev(s[k,1]);
   Sw(s[k,0],s[k,1]);
   rv[k]:=False
  End
 End;

 Procedure pu(k:Longint);
 Begin
  c[k]:=c[s[k,0]]+c[s[k,1]]+1
 End;

 Procedure Rotate(x:Longint;Var K:Longint);
 Var y,z,p:Longint;
 Begin
  y:=f[x]; z:=f[y]; p:=Ord(S[y,1]=x);
  If y=k Then k:=x Else s[z,Ord(s[z,1]=y)]:=x;
  f[x]:=z; f[y]:=x; f[s[x,1-p]]:=y;
  s[y,p]:=s[x,1-p]; s[x,1-p]:=y;
  pu(y)
 End;

 Procedure Splay(x:Longint;Var k:Longint);
 Var y,z:Longint;
 Begin{
  top:=1; Sta[1]:=x; y:=x;
  While y<>k Do Begin y:=f[y]; inc(top); sta[top]:=y End;
  For top:=top Downto 1 Do Pd(Sta[top]);}
  While x<>k Do Begin
   y:=f[x]; z:=f[y];
   If y<>k Then
   If (s[y,0]=x)Xor(s[z,0]=y) Then Rotate(x,k)
                              Else Rotate(y,k);
   Rotate(x,k)
  End;
  pu(x)
 End;

 Function sk(k,x:Longint):Longint;
 Var t:Longint;
 Begin
  Pd(k);
  t:=c[s[k,0]]+1;
  if x=t Then Exit(k);
  If x<t Then Exit(sk(s[k,0],x));
  Exit(sk(s[k,1],x-t))
 End;

 Function Bd(L,R:Longint):Longint;
 Var tmp:Longint;
 Begin
  If L>R Then Exit(0);
  Bd:=(L+R)>>1;
  c[Bd]:=1;
  va[Bd]:=Bd-1;
  rv[Bd]:=False;
  tmp:=Bd(L,Bd-1); f[tmp]:=Bd; S[Bd,0]:=tmp;
  tmp:=Bd(1+Bd,R); f[tmp]:=Bd; S[Bd,1]:=tmp;
  pu(Bd)
 End;

 Procedure RevTag(L,R:Longint);
 Begin
  If L>=R Then Exit;
  SPlay(sk(Root,R+2),Root);
  SPlay(sk(Root,L),s[Root,0]);
  Rev(s[s[Root,0],1])
 End;

Procedure Solve2;
Var i,qy,poi:Longint;
Begin
 For i:=1 to n Do Las[i]:=int64(i)*m;
 poi:=1;
 Root:=Bd(1,m+2);
 For i:=1 to Q Do Begin
  qy:=Y[i];
  Leave:=Va[sk(Root,Qy+1)];
  WriteLn(Leave);
  RevTag(qy,m);
  RevTag(qy,m-1);
  Las[poi]:=Leave;
  poi:=poi+1; If poi>n Then poi:=1;
  va[Sk(Root,m+1)]:=Las[poi];
 End
End;

Begin
 Assign(input,'phalanx.in'); Reset(Input);
 Assign(Output,'phalanx.out'); Rewrite(Output);
 Read(N,m,q);
 For i:=1 to Q Do Read(X[i],Y[i]);
 If Judge2 Then Solve2 Else Solve1;
 Close(Input); Close(Output)
End.
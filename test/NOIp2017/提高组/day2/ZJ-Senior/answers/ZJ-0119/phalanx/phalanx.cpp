#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<map>
#include<queue>

#define REP(i,l,r) for (int i=(l);i<=(r);++i)
#define rep(i,l,r) for (int i=(l);i<(r);++i)
#define fr(a) freopen(a,"r",stdin)
#define fw(a) freopen(a,"w",stdout)
#define mem(a) memset(a,0,sizeof(a))
#define dbg(a) cout<<#a<<" = "<<a<<endl
#define tpn typename

using namespace std;

typedef long long ll;
typedef double db;
typedef long double ldb;

template <tpn A> inline void read(A &x){
	A neg;
	char c;
	neg=1;
	do{
		c=getchar();
		if (c=='-') neg=-1;
	}while (c<'0'||c>'9');
	x=0;
	do{
		x=x*10+c-48;
		c=getchar();
	}while (c>='0'&&c<='9');
	x*=neg;
}
template <tpn A,tpn B> inline void read(A &a,B &b){
	read(a),read(b);
}
template <tpn A,tpn B,tpn C> inline void read(A &a,B &b,C &c){
	read(a),read(b),read(c);
}
template <tpn A,tpn B,tpn C,tpn D> inline void read(A &a,B &b,C &c,D &d){
	read(a),read(b),read(c),read(d);
}
const int p=100000007;
int seed=19260817;
inline int Random(){
	return seed=1LL*seed*seed%p*1LL*19890603%p;
}
int n,m,q;
namespace task1{
	int a[1001][1001],tmp,x,y;
	inline void solve(){
		REP(i,1,n)
		  REP(j,1,m) a[i][j]=(i-1)*m+j;
		REP(ii,1,q){
			read(x,y);
			printf("%d\n",a[x][y]);
			tmp=a[x][y];
			REP(i,y,m-1) a[x][i]=a[x][i+1];
			REP(i,x,n-1) a[i][m]=a[i+1][m];
			a[n][m]=tmp;
		}
	}
}
namespace task2{
	int x[501],y[501];
	ll a[501][50001],lac[50001],tmp;
	int totl,tob[50001],tos[501],xx,yy;
	bool b[50001];
	inline void solve(){
		REP(i,1,q) read(x[i],y[i]),b[x[i]]=1;
		REP(i,1,n) if (b[i]) ++totl,tob[i]=totl,tos[totl]=i;
		REP(i,1,totl)
		  REP(j,1,m) a[i][j]=1LL*(tos[i]-1)*m+j;
		REP(i,1,n) lac[i]=1LL*i*m;
		REP(i,1,q){
			xx=tob[x[i]];
			yy=y[i];
			tmp=a[xx][yy];
			printf("%lld\n",tmp);
			REP(j,yy,m-1) a[xx][j]=a[xx][j+1];
			REP(j,x[i],n-1){
				lac[j]=lac[j+1];
				if (b[j]) a[tob[j]][m]=lac[j];
			}
			lac[n]=tmp;
			if (b[n]) a[tob[n]][m]=lac[n];
		}
	}
}
namespace task3{
	struct TREAPS{
		struct TREAP{
			ll num;
			int rnd,siz;
			TREAP *le,*ri;
			TREAP(ll x=0){
				rnd=Random();
				num=x;
				le=ri=0;
				siz=1;
			}
		}*root,*le,*ri,*p;
		ll tmp;
		TREAPS(){
			root=le=ri=p=0;
		}
		inline int size(TREAP *root){
			return root?root->siz:0;
		}
		inline void maintain(TREAP *root){
			root->siz=size(root->le)+size(root->ri)+1;
		}
		inline void spilit(TREAP *root,TREAP *&le,TREAP *&ri,int k){
			if (!root){
				le=ri=0;
				return;
			}
			int lsize=size(root->le);
			if (lsize>=k){
				ri=root;
				spilit(root->le,le,ri->le,k);
				maintain(ri);
			}else{
				le=root;
				spilit(root->ri,le->ri,ri,k-lsize-1);
				maintain(le);
			}
		}
		inline TREAP *merge(TREAP *le,TREAP *ri){
			if (!le) return ri;
			if (!ri) return le;
			if (le->rnd<ri->rnd){
				le->ri=merge(le->ri,ri);
				maintain(le);
				return le;
			}else{
				ri->le=merge(le,ri->le);
				maintain(ri);
				return ri;
			}
		}
		inline void insert(ll x){
			p=new TREAP(x);
			root=merge(root,p);
		}
		inline ll ask_and_del(int wei){
			spilit(root,le,ri,wei-1);
			spilit(ri,p,ri,1);
			root=merge(le,ri);
			return p->num;
		}
	}tree;
	ll tmp;
	int x,y;
	inline void solve(){
		REP(i,1,m){
			tree.insert(i);
		}
		REP(i,2,n) tree.insert(1LL*i*m);
		REP(ii,1,q){
			read(x,y);
			tmp=tree.ask_and_del(y);
			printf("%lld\n",tmp);
			tree.insert(tmp);
		}
	}
}
int main(){
	fr("phalanx.in");
	fw("phalanx.out");
	read(n,m,q);
	if (n<=1000&&m<=1000&&q<=500) task1::solve();
	else if (n<=50000&&m<=50000&&q<=500) task2::solve();
	else task3::solve();
	return 0;
}

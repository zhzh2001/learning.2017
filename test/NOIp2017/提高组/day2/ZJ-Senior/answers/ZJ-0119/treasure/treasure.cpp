#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<map>
#include<queue>

#define REP(i,l,r) for (int i=(l);i<=(r);++i)
#define rep(i,l,r) for (int i=(l);i<(r);++i)
#define fr(a) freopen(a,"r",stdin)
#define fw(a) freopen(a,"w",stdout)
#define mem(a) memset(a,0,sizeof(a))
#define dbg(a) cout<<#a<<" = "<<a<<endl
#define tpn typename

using namespace std;

typedef long long ll;
typedef double db;
typedef long double ldb;
typedef pair<int,int> pii;

template <tpn A> inline void read(A &x){
	A neg;
	char c;
	neg=1;
	do{
		c=getchar();
		if (c=='-') neg=-1;
	}while (c<'0'||c>'9');
	x=0;
	do{
		x=x*10+c-48;
		c=getchar();
	}while (c>='0'&&c<='9');
	x*=neg;
}
template <tpn A,tpn B> inline void read(A &a,B &b){
	read(a),read(b);
}
template <tpn A,tpn B,tpn C> inline void read(A &a,B &b,C &c){
	read(a),read(b),read(c);
}
template <tpn A,tpn B,tpn C,tpn D> inline void read(A &a,B &b,C &c,D &d){
	read(a),read(b),read(c),read(d);
}
int n,m,a[13][13],u,v,z;
bool b[13];
int dist[13],nowval,mini,tmp;
queue <int> q;
namespace task1{
	inline void solve(){
	mini=0x7fffffff;
	REP(i,1,n){
		q.push(i);
		mem(b);
		mem(dist);
		b[i]=1;
		nowval=0;
		while (!q.empty()){
			u=q.front();
			q.pop();
			for (v=1;v<=n;++v) if (a[u][v]!=a[0][0]&&!b[v]){
				b[v]=1;
				dist[v]=dist[u]+1;
				nowval+=a[u][v]*dist[v];
				q.push(v);
			}
		}
		if (nowval<mini) mini=nowval;
	}
	cout<<mini;
	}
}
inline bool check(){
	int y;
	REP(i,1,n)
	  REP(j,1,n) if (a[i][j]!=a[0][0]){
	  	y=a[i][j];
	  	break;
	  }
	REP(i,1,n)
	  REP(j,1,n) if (a[i][j]!=a[0][0]&&a[i][j]!=y) return 0;
	return 1;
}
int main(){
	fr("treasure.in");
	fw("treasure.out");
	read(n,m);
	memset(a,0x3f,sizeof(a));
	REP(i,1,m){
		read(u,v,z);
		if (z<a[u][v]) a[u][v]=a[v][u]=z;
	}
	if (check()){
		task1::solve();
		return 0;
	}
	mini=0x7fffffff;
	REP(i,1,n){
		mem(b);
		mem(dist);
		b[i]=1;
		nowval=0;
		REP(jj,1,n-1){
			tmp=0x7fffffff;
			u=v=0;
			REP(j,1,n)
			if (b[j]){
				REP(k,1,n){
					if (!b[k]&&a[j][k]!=a[0][0])
					  if ((dist[j]+1)*a[j][k]<tmp) tmp=a[j][k]*(dist[j]+1),u=j,v=k;
				}
			}
			nowval+=tmp;
			dist[v]=dist[u]+1;
			b[v]=1;
		}
		if (nowval<mini) mini=nowval;
	}
	cout<<mini;
	return 0;
}

#include<iostream>
#include<cstdio>
#include<string>
#include<cstring>
#include<algorithm>
#include<vector>
#include<set>
#include<map>
#include<queue>

#define REP(i,l,r) for (int i=(l);i<=(r);++i)
#define rep(i,l,r) for (int i=(l);i<(r);++i)
#define fr(a) freopen(a,"r",stdin)
#define fw(a) freopen(a,"w",stdout)
#define mem(a) memset(a,0,sizeof(a))
#define dbg(a) cout<<#a<<" = "<<a<<endl
#define tpn typename

using namespace std;

typedef unsigned long long ull;
typedef long long ll;
typedef double db;
typedef long double ldb;

template <tpn A> inline void read(A &x){
	A neg;
	char c;
	neg=1;
	do{
		c=getchar();
		if (c=='-') neg=-1;
	}while (c<'0'||c>'9');
	x=0;
	do{
		x=x*10+c-48;
		c=getchar();
	}while (c>='0'&&c<='9');
	x*=neg;
}
template <tpn A,tpn B> inline void read(A &a,B &b){
	read(a),read(b);
}
template <tpn A,tpn B,tpn C> inline void read(A &a,B &b,C &c){
	read(a),read(b),read(c);
}
template <tpn A,tpn B,tpn C,tpn D> inline void read(A &a,B &b,C &c,D &d){
	read(a),read(b),read(c),read(d);
}
const int maxn=1010;
int T,n,h,fa[maxn];
ll r,d,x[maxn],y[maxn],z[maxn];
ull xx,yy,zz;
inline int ask(int x){
	return fa[x]==x?x:fa[x]=ask(fa[x]);
}
inline void set_merge(int x,int y){
	x=ask(x),y=ask(y);
	if (x!=y) fa[x]=y;
}
#define sqr(a) ((a)*(a))
int main(){
	fr("cheese.in");
	fw("cheese.out");
	read(T);
	while (T--){
		read(n,h,r);
		d=sqr(2*r);
		REP(i,1,n+2) fa[i]=i;
		REP(i,1,n){
			read(x[i],y[i],z[i]);
			rep(j,1,i){
				xx=sqr(x[i]-x[j]);
				yy=sqr(y[i]-y[j]);
				zz=sqr(z[i]-z[j]);
				if (xx+yy+zz<=d) set_merge(j+1,i+1);
			}
			if (z[i]<=r) set_merge(1,i+1);
			if (z[i]+r>=h) set_merge(i+1,n+2);
		}
		if (ask(1)==ask(n+2)) puts("Yes");
		else puts("No");
	}
	return 0;
}

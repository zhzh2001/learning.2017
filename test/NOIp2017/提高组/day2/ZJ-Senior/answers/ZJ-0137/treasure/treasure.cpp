#include<cstdio>
#include<iostream>
using namespace std;

int n,m;
int minn=999999999;
int sum=0;
int f[18][18],a[18],b[18],c[18];

void dfs(int k,int w)
{
	if (w>minn) return;
	if (k==n) 
	{
		minn=min(minn,w);
		return;
	}
	for (int i=1;i<=sum;i++)
	for (int j=1;j<=n;j++)
	if (f[a[i]][j]!=500008&b[j]==0)
	{
		b[j]=1;
		a[++sum]=j;
		c[j]=c[a[i]]+1;
		dfs(k+1,w+c[a[i]]*f[a[i]][j]);
		b[j]=0;
		sum--;
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
	b[i]=0;
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
	f[i][j]=500008;
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		f[x][y]=f[y][x]=min(f[x][y],z);
	}
	/*
	for (int i=1;i<=8;i++)
	{
		for (int j=1;j<=8;j++)
		printf("%d ",f[i][j]);
		printf("\n");
	}
	*/
	for (int i=1;i<=n;i++)
	{
		b[i]=1;
		a[++sum]=i;
		c[i]=1;
		dfs(1,0);
		b[i]=0;
		sum--;
	}
	printf("%d\n",minn);
	
	fclose(stdout);
	fclose(stdin);
	return 0;
}

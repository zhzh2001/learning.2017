#include<cstdio>
#include<iostream>
using namespace std;

int f[1008][1008];
int d[300008];
long long a[300008],b[300008];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	int n,m,q;
	scanf("%d%d%d",&n,&m,&q);
	if (n==1)
	{
		int t;
		for (int i=1;i<=m;i++)
		d[i]=i;
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%d\n",f[y]);
			t=d[y];
			for (int i=y;i<=m-1;i++)
			d[i]=d[i+1];
			d[m]=t;
		}
	}
	else if (n<=1000&m<=1000)
	{
		int t;
		for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
		f[i][j]=(i-1)*m+j;
		while (q--)
		{	
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%d\n",f[x][y]);
			t=f[x][y];
			for (int i=y;i<=m-1;i++)
			f[x][i]=f[x][i+1];
			for (int i=x;i<=n-1;i++)
			f[i][m]=f[i+1][m];
			f[n][m]=t;
		}
	}
	else
	{
		long long t;
		for (int i=1;i<=n;i++)
		b[i]=i*m;
		for (int i=1;i<=m;i++)
		a[i]=i;
		while (q--)
		{
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%lld\n",a[y]);
			t=a[y];
			for (int i=y;i<=m-1;i++)
			a[i]=a[i+1];
			a[m]=b[2];
			for (int i=x;i<=n-1;i++)
			b[i]=b[i+1];
			b[n]=t;
		}
	}
	
	fclose(stdout);
	fclose(stdin);
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
#define sqr(x) ((x) * (x))
#define N (100005)
typedef long long ll;
using namespace std;
int q[N], vis[N], h, t, n, flag;
ll x[N], y[N], z[N], High, r;
inline double dis(int i, int j){
	return sqrt(sqr(x[i] - x[j]) + sqr(y[i] - y[j]) + sqr(z[i] - z[j]));
}
inline bool check(int i, int j){
	double d = dis(i, j);
	if(d < (double) 2 * r) return 1;
	if(fabs(d - 2 * r) < 0.0000001) return 1;
	return 0;
}
inline void push(int u){
	if(vis[u]) return;
	if(z[u] + r >= High) flag = 1;
	q[++t] = u, vis[u] = 1;
}
inline void solve(){
	memset(vis, 0, sizeof(vis));
	h = 0, t = 0, flag = 0;
	scanf("%d%lld%lld", &n, &High, &r);
	for(int i = 1; i <= n; i++) 
		scanf("%lld%lld%lld", &x[i], &y[i], &z[i]);
	for(int i = 1; i <= n; i++) if(z[i] - r <= 0) push(i);
	while(h < t){
		if(flag) return (void) (puts("Yes"));
		int u = q[++h];
		//printf("%d\n", u);
		for(int i = 1; i <= n; i++)
			if(!vis[i] && check(u, i)) push(i);
	}
	if(flag) puts("Yes"); else puts("No");
}
int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T = 0; scanf("%d", &T);
	while(T--) solve();
	return 0;
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
int a[505][50005], row[50005], rev[50005], num[50005], n, m, q, cnt;
struct Point{int x, y;} qu[100005];
int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	for(int i = 1; i <= q; i++){
		int x, y;
		scanf("%d%d", &x, &y);
		qu[i] = (Point){x, y};
		if(!num[x]) num[x] = ++cnt, rev[cnt] = x;
	}
	for(int i = 1; i <= cnt; i++) 
		for(int j = 1; j <= m; j++) a[i][j] = (rev[i] - 1) * m + j;
	for(int i = 1; i <= n; i++) row[i] = i * m;
	for(register int j = 1; j <= q; j++){
		int x = qu[j].x, y = qu[j].y;
		for(register int i = 1; i <= cnt; i++) a[i][m] = row[rev[i]];
		int tmp = a[num[x]][y]; printf("%d\n", a[num[x]][y]);
		for(register int i = y; i <= m - 1; i++) a[num[x]][i] = a[num[x]][i+1];
		for(register int i = x; i <= n - 1; i++) row[i] = row[i+1]; row[n] = tmp;
		/*for(int i = 1; i <= n; i++){
				for(int j = 1; j <= m; j++) printf("%d", a[i][j]);	
			printf("\n");
		}*/
	}

}

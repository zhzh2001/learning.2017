#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF (0x7f7f7f7f)
#define Max(a, b) ((a) > (b) ? (a) : (b))
#define Min(a, b) ((a) < (b) ? (a) : (b))
typedef long long ll;
using namespace std;
int f[(1<<12)+100][20][20], s[20][20], vis[20], n, m, ans;
inline void solve(int st){
	//printf("-----%d-----\n", st);
	memset(f, 127 / 3, sizeof(f));
	f[1<<(st-1)][st][1] = 0;
	for(int i = 1; i < (1 << n); i++)
		for(int j = 1; j <= n; j++){
			if(!((1 << j - 1) & i)) continue;
 			for(int k = 1; k <= n; k++){
				if(f[i][j][k] > 7000000) continue;
				for(int u = 0; u < n; u++) if(!(i & (1 << u))){
					if(s[u+1][j] == INF) continue;
					//printf("%d %d %d\n", i^(1<<u), u+1, k-1);
					int val = s[u+1][j] * k;
					f[i|(1<<u)][u+1][k+1] = Min(f[i|(1<<u)][u+1][k+1], f[i][j][k] + val);
					f[i|(1<<u)][j][k] = Min(f[i][j][k] + val, f[i|(1<<u)][j][k]); 	
				}
				//printf("%d %d %d %d\n", i, j, k, f[i][j][k]);	
			}
		}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++) 
			ans = Min(ans, f[(1<<n)-1][i][j]); //printf("%d %d %d\n", i, j, f[(1<<n)-1][i][j]); 
}
inline int dfs(int u, int k){
	int tmp = 0; vis[u] = 1;
	for(int j = 1; j <= n; j++) 
		if(!vis[j] && s[u][j] != INF) tmp += (k + 1)* s[u][j] + dfs(j, k + 1);
	//printf("%d %d %d\n", u, k, tmp);
	return tmp;
}
inline void subtask(){
	memset(vis, 0, sizeof(vis));
	//printf("OK this subtask\n");
	for(int i = 1; i <= n; i++){
		memset(vis, 0, sizeof(vis));
		int val = dfs(i, 0);
		ans = Min(ans, val);
		//printf("%d %d\n", i, dfs(i, 0));
	}
	cout << ans;
}
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int Edge = 0; ans = INF;
	memset(s, 127, sizeof(s));
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= m; i++){
		int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		s[x][y] = Min(s[x][y], z);
		s[y][x] = Min(s[y][x], z);
	}
	for(int i = 1; i <= n; i++)
		for(int j = i + 1; j <= n; j++) if(s[i][j] != INF) Edge++;
	if(Edge == n - 1) {subtask(); return 0;}
	for(int i = 1; i <= n; i++) solve(i);
	cout << ans;
	return 0;
}

#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<string>
#include<algorithm>
#define MAXN 15
#define MAXS 5002
#define MAXM 2005
using namespace std;
typedef pair<int, int> PII;

inline void getint(int &num){
	char ch;
	while((ch = getchar()) < '0' || ch > '9');
	num = ch - '0';
	while((ch = getchar()) >= '0' && ch <= '9') num = (num << 1) + (num << 3) + ch - '0';
}

int n, m, ans = 0x3f3f3f3f, S;
int f[MAXS];
int st[MAXS][MAXN];

struct tEdge{
	int np, val;
	tEdge *nxt;
};

struct Graph{
	tEdge E[MAXM], *V[MAXN];
	int tope;
	
	inline void clear() {tope = 0, memset(V, 0, sizeof(V));}
	
	inline void addedge(int u, int v, int w){
		E[++tope].np = v, E[tope].val = w;
		E[tope].nxt = V[u], V[u] = &E[tope];
	}
} G;

inline void DP(int s){
	memset(f, 0x3f, sizeof(f));
	f[1 << (s - 1)] = 0, st[1 << (s - 1)][s] = 1;
	for(register int i = 1; i <= S; i++){
		if(f[i] == 0x3f3f3f3f) continue;
		for(register int j = 1; j <= n; j++)
			if(i & (1 << (j - 1))){
				for(register tEdge *ne = G.V[j]; ne; ne = ne->nxt){
					if(i & (1 << (ne->np - 1))) continue;
					int newS = i | (1 << (ne->np - 1));
					if(f[i] + st[i][j] * ne->val < f[newS]){
						f[newS] = f[i] + st[i][j] * ne->val;
						for(register int k = 1; k <= n; k++)
							st[newS][k] = st[i][k];
						st[newS][ne->np] = st[i][j] + 1;
					}
				}
			}
	}
}

int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	
	getint(n), getint(m); S = (1 << n) - 1;
	G.clear();
	for(register int i = 1; i <= m; i++){
		int u, v, w;
		getint(u), getint(v), getint(w);
		G.addedge(u, v, w), G.addedge(v, u, w);
	}
	for(register int i = 1; i <= n; i++){
		DP(i);
		ans = min(ans, f[(1 << n) - 1]);
	}
	printf("%d\n", ans);
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

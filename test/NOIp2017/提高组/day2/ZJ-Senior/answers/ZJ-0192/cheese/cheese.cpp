#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<iostream>
#include<string>
#include<algorithm>
#define MAXV 1005
#define MAXE 1100005
using namespace std;
typedef long long ll;

template<typename T>
inline void getint(T &num){
	char ch;
	bool neg = 0;
	while((ch = getchar()) < '0' || ch > '9') if(ch == '-') neg = 1;
	num = ch - '0';
	while((ch = getchar()) >= '0' && ch <= '9') num = (num << 1) + (num << 3) + ch - '0';
	if(neg) num = -num; 
}

int T, n;
ll h, r, mxd;

struct tCoord{
	ll x, y, z;
} c[MAXV];

struct tEdge{
	int np;
	tEdge *nxt;
};

struct Graph{
	tEdge E[MAXE], *V[MAXV];
	int tope;
	
	inline void clear() {tope = 0, memset(V, 0, sizeof(V));}
	
	inline void addedge(int u, int v){
		E[++tope].np = v;
		E[tope].nxt = V[u], V[u] = &E[tope];
	}
} G;

#define sqr(x) ((x) * (x))

inline void try_linking(int u, int v){
	ll sqr_d = sqr(c[u].x - c[v].x) + sqr(c[u].y - c[v].y) + sqr(c[u].z - c[v].z);
	if(sqr_d <= mxd) G.addedge(u, v), G.addedge(v, u);
}

bool vis[MAXV];

inline bool dfs(int u){
	if(u == n + 1) return 1;
	for(register tEdge *ne = G.V[u]; ne; ne = ne->nxt){
		if(vis[ne->np]) continue;
		vis[ne->np] = 1;
		if(dfs(ne->np)) return 1;
	}
	return 0;
}

int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	
	getint(T);
	while(T--){
		getint(n), getint(h), getint(r);
		mxd = r * r * 4;
		for(register int i = 1; i <= n; i++)
			getint(c[i].x), getint(c[i].y), getint(c[i].z);
		G.clear();
		for(register int i = 1; i <= n; i++){
			if(r >= c[i].z) G.addedge(0, i);
			if(r >= h - c[i].z) G.addedge(i, n + 1);
		}
		for(register int i = 1; i < n; i++)
			for(register int j = i + 1; j <= n; j++)
				try_linking(i, j);
		memset(vis, 0, sizeof(vis));
		if(dfs(0)) puts("Yes");
		else puts("No");
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

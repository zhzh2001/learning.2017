#include <cstdio>
#include <algorithm>
using namespace std;
#define REP(a,b,c) for(int a=b;a<=c;++a)
#define RREP(a,b,c) for(int a=b;a>=c;--a)
#define LL long long

struct node{LL s; int l,r,f,sz;}x[4000000];
int y[400000][2],z,tmp,q;
LL n,m;

void rot(int &zz,int a) { int x1,x2,y,z; y=x[a].f; z=x[y].f; x1=x[y].l==a?0:1; x2=x[z].l==y?0:1;
	if(x1==0) {x[y].sz-=1+x[x[a].l].sz; x[y].l=x[a].r; x[x[a].r].f=y; x[y].f=a; x[a].r=y; }
	else {x[y].sz-=1+x[x[a].r].sz; x[y].r=x[a].l; x[x[a].l].f=y; x[y].f=a; x[a].l=y;} x[a].sz=1+x[x[a].l].sz+x[x[a].r].sz; 
	if(x2==0) {x[a].f=z; x[z].l=a;} else {x[a].f=z; x[z].r=a;} if(x[a].f==0) zz=a; 
}
void splay(int a,int &zz) { int x1,x2,y,z;
	while(zz!=a) {if(x[a].f!=zz) {y=x[a].f; z=x[y].f; x1=x[y].l==a?0:1; x2=x[z].l==y?0:1; if(x1==x2) rot(zz,y); else rot(zz,a);} rot(zz,a);}
}
void add(int &z,int a) { if(z==0) {z=a; return ;} int b=z; 
	while(1) {++x[b].sz; if(x[a].s<x[b].s) {if(x[b].l==0) break; else b=x[b].l;} else {if(x[b].r==0) break; else b=x[b].r;}}
	if(x[b].s>x[a].s) x[b].l=a; else x[b].r=a; x[a].f=b; splay(a,z); 
}
void ad(int &z,int a) {
	if(z==0) {z=a; return ;} int b=z; while(x[b].r) b=x[b].r; splay(b,z); x[a].l=b,x[b].f=a,x[a].sz+=x[b].sz,z=a;
}
void de(int &z,int a) { if(x[z].sz==1) {z=0; return ;} int b=z;
	while(1) {if(x[x[b].l].sz>=a) b=x[b].l; else if(x[x[b].l].sz+1==a) break; else a-=x[x[b].l].sz+1,b=x[b].r;}
	splay(b,z);  if(x[b].l==0) {z=x[b].r,x[z].f=0; return ;} if(x[b].r==0) {z=x[b].l,x[z].f=0; return ;}
	int c=x[b].l; b=x[b].r; z=b; x[b].f=0; while(x[b].l) b=x[b].l; splay(b,z); x[b].l=c,x[c].f=b,x[b].sz+=x[c].sz; 
}
LL get(int b,int a) {while(1) {if(x[x[b].l].sz>=a) b=x[b].l; else if(x[x[b].l].sz+1==a) return x[b].s; else a-=x[x[b].l].sz+1,b=x[b].r;}}
LL solve(LL a,LL b,LL c) { int d;
	if(m-1-x[y[a][0]].sz>=b) {
		int e=y[a][0],f=0; while(e) {if(x[e].s-m*(a-1)-f-x[x[e].l].sz-1<b) f+=x[x[e].l].sz+1,e=x[e].r; else e=x[e].l;}
		d=m*(a-1)+f+b; x[++tmp]=(node){d,0,0,0,1}; add(y[a][0],tmp); x[++tmp]=(node){c,0,0,0,1}; ad(y[a][1],tmp);
	} else {b-=m-1-x[y[a][0]].sz; d=get(y[a][1],b); de(y[a][1],b); x[++tmp]=(node){c,0,0,0,1}; ad(y[a][1],tmp);} return d;
}
int main() {
	freopen("phalanx.in","r",stdin); freopen("phalanx.out","w",stdout);
	scanf("%lld%lld%d",&n,&m,&q); LL a,b,c,d; REP(i,1,n) {x[++tmp]=(node){i*m,0,0,0,1}; ad(z,tmp);} 
	while(q--) { scanf("%lld%lld",&a,&b); 
		if(b<m) {c=get(z,a); de(z,a);  d=solve(a,b,c); printf("%lld\n",d); x[++tmp]=(node){d,0,0,0,1}; ad(z,tmp); }
		else {c=get(z,a); de(z,a); printf("%lld\n",c); x[++tmp]=(node){c,0,0,0,1}; ad(z,tmp);}
	} return 0;
}

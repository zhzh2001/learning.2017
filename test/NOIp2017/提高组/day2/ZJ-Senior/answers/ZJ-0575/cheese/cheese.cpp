#include <cstdio>
#include <algorithm>
using namespace std;
#define REP(a,b,c) for(int a=b;a<=c;++a)
#define RREP(a,b,c) for(int a=b;a>=c;--a)
#define LL long long

const int N=1005;
int T,a,b,c,n;
LL x[N],y[N],z[N];
int bel[N];
LL h,r;

inline LL sq(LL a) {return a*a;}
inline int fa(int a) {return bel[a]==a?a:bel[a]=fa(bel[a]);}
inline void bind(int a,int b) {bel[fa(a)]=fa(b);}
	

int main() {
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--) {
		scanf("%d%lld%lld",&n,&h,&r);
		REP(i,1,n) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]); REP(i,0,n+1) bel[i]=i;
		REP(i,1,n) {if(z[i]<=r) bind(0,i); if(z[i]+r>=h) bind(i,n+1);} r=r*r*4;
		REP(i,1,n) REP(j,i+1,n) if(sq(x[i]-x[j])+sq(y[i]-y[j])<=r-sq(z[i]-z[j])) bind(i,j);
		if(fa(0)==fa(n+1)) puts("Yes"); else puts("No");
	} return 0;
}

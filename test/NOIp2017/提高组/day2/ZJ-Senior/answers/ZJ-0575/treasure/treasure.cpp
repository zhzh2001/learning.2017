#include <cstdio>
#include <algorithm>
using namespace std;
#define REP(a,b,c) for(int a=b;a<=c;++a)
#define RREP(a,b,c) for(int a=b;a>=c;--a)
#define LL long long

const int N=20,maxn=40000000;
int a,b,c,d,e,f,n,m;
int x[N][N],y[1<<12],z[1<<12][1<<12],q[1<<12][N],p[1<<12];

inline int cmin(int a,int b) {return a>b?b:a;}

int main() {
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	REP(i,0,11) p[1<<i]=i+1; scanf("%d%d",&n,&m); d=(1<<n)-1; 
	REP(i,1,n) REP(j,1,n) x[i][j]=i==j?0:maxn; REP(i,0,d) y[i]=maxn; REP(i,1,n) y[1<<(i-1)]=0;
	while(m--) {scanf("%d%d%d",&a,&b,&c); x[a][b]=x[b][a]=cmin(x[a][b],c);} 
	REP(j,1,n) q[0][j]=maxn; REP(i,1,d) REP(j,1,n) q[i][j]=cmin(q[i^(i&(-i))][j],x[p[i&(-i)]][j]);
	REP(i,1,d) REP(f,1,d) if((i&f)==0) {z[i][f]=cmin(z[i][f^(f&(-f))]+q[i][p[f&(-f)]],maxn);}
	REP(i,1,n-1) RREP(j,d,0) if(y[j]<maxn) for(f=e=d^j;f;f=(f-1)&e) {y[j|f]=cmin(y[j|f],y[j]+z[j][f]*i);}
	printf("%d\n",y[d]); return 0;
}
